Summary: iMeeting php support
Name: iic-php
Version: @@@VERSION@@@
Release: 1
License: Sitescape
Group: Applications/Communication
URL: http://www.sitescape.com
Source0: %{name}-%{version}.tar.gz
Source1: @@@PHP_TARBALL@@@
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Requires: iic-apache

%description
This package contains the PHP support for iMeeting web services

%prep
%setup -q
%setup -q -D -a 1
( cd @@@PHP_BASE@@@ ; ./configure --enable-shared --with-apxs2=/usr/local/apache2/bin/apxs --prefix=$RPM_BUILD_ROOT/usr/local )

%build
( cd @@@PHP_BASE@@@ ; make )

%install
rm -rf $RPM_BUILD_ROOT
( cd @@@PHP_BASE@@@ ; make install; mkdir -p $RPM_BUILD_ROOT/usr/local/apache2/modules ; cp /usr/local/apache2/modules/libphp4* $RPM_BUILD_ROOT/usr/local/apache2/modules )

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/usr/local/apache2/modules/libphp4.so
/usr/local/bin/pear
/usr/local/bin/php
/usr/local/bin/php-config
/usr/local/bin/phpize
/usr/local/etc/pear.conf
/usr/local/include/php
/usr/local/lib/php
/usr/local/bin/peardev
/usr/local/bin/pecl
/usr/local/man/man1

%doc

%changelog
* Thu Oct  7 2004 root <root@dbs.homedns.org> 1.0.0-1
- Initial build.



