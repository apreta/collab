Summary: ipunity web app
Name: iic-zon-ha
Version: @@@VERSION@@@
Release: 1
License: Sitescape
Group: Applications/Communication
URL: http://www.sitescape.com
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
#Requires: heartbeat

%description
This package contains configuration for Zon Lite High Availability

%prep
%setup -q

%build

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/etc/ha.d/resource.d
mkdir -p $RPM_BUILD_ROOT/var/lib/heartbeat/crm
cp iic/conf/ha.d/authkeys $RPM_BUILD_ROOT/etc/ha.d/
cp iic/conf/ha.d/ha.cf.in $RPM_BUILD_ROOT/etc/ha.d/
cp -r iic/conf/ha.d/resource.d $RPM_BUILD_ROOT/etc/ha.d/
cp iic/conf/ha.d/crm/cib.xml.in $RPM_BUILD_ROOT/var/lib/heartbeat/crm/

mkdir -p $RPM_BUILD_ROOT/opt/iic/conf
cp iic/conf/config_ha.sh $RPM_BUILD_ROOT/opt/iic/conf/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc
/etc/ha.d
/var/lib/heartbeat/crm
/opt/iic/conf/config_ha.sh

%post

%postun

%changelog
* Thu Oct  7 2004 root <root@dbs.homedns.org> 1.0.0-1
- Initial build.



