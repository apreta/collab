Summary: libx264 built for use in iMeeting
Name: iic-x264
Version: @@@VERSION@@@
Release: 1
License: GPL
Group: Applications/Communication
URL: http://www.sitescape.com
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot

%description
This package contains the H.264 codec.

%if "%{_vendor}" == "suse"
%debug_package
%endif

%prep
%setup -q
( cd x264; ./configure --enable-debug --prefix=/opt/iic --extra-ldflags=-lmp3lame --disable-cli --enable-shared )

%build
( cd x264; make )

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT
( cd x264; make DESTDIR=$RPM_BUILD_ROOT install )

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/opt/iic/include/x264.h
/opt/iic/include/x264_config.h
/opt/iic/lib/libx264.so
/opt/iic/lib/libx264.so.115
/opt/iic/lib/pkgconfig/x264.pc

%post
if ! grep --silent /opt/iic/lib /etc/ld.so.conf ; then
    echo "/opt/iic/lib" >> /etc/ld.so.conf
fi
/sbin/ldconfig

%postun
if [ "$1" = 0 ]; then
  grep -v /opt/iic/lib /etc/ld.so.conf > /tmp/xxx
  mv /tmp/xxx /etc/ld.so.conf
fi
/sbin/ldconfig

%changelog
* Thu Oct  7 2004 root <root@dbs.homedns.org> 1.0.0-1
- Initial build.



