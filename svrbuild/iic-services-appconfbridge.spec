Summary: Kablink conferencing Asterisk/appconference bridge
Name: iic-services-appconfbridge
Version: @@@VERSION@@@
Release: 1
License: Sitescape
Group: Applications/Communication
URL: http://www.novell.com
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Requires: iic-xmlrpc 
BuildRequires: iic-xmlrpc

%description
This package contains the service that connects to an Asterisk-based conferencing bridge / media server
running the appconference Asterisk module.


%if "%{_vendor}" == "suse"
%debug_package
%endif

%prep
%setup -q
mkdir -p iic/lib
( cd iic/jcomponent; ./configure --enable-debug )
( cd iic/services; ./configure --enable-debug )
( cd iic/services; make -i depend )


%build
( cd iic/jcomponent; make )
( cd astxx; ./pm --prefix=/opt/iic libastxx_manager )
( cd iic/services/voice; make ../../bin/appconfvoiced )

%install
rm -rf $RPM_BUILD_ROOT
( cd astxx; ./pm --DESTDIR=$RPM_BUILD_ROOT --prefix=/opt/iic install_libastxx_manager )
( cd iic; make DESTDIR=$RPM_BUILD_ROOT install-appconfvoiced )

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/opt/iic/conf/init.d/iicvoice
/opt/iic/conf/appconfconfig.xml.in
/opt/iic/bin/appconfvoiced
/opt/iic/lib

%post
if ! grep --silent /opt/iic/lib /etc/ld.so.conf ; then
    echo "/opt/iic/lib" >> /etc/ld.so.conf
fi
/sbin/ldconfig

%postun
if [ "$1" -eq 0 ]; then
    grep -v /opt/iic/lib /etc/ld.so.conf > /tmp/xxx
    mv /tmp/xxx /etc/ld.so.conf
fi
/sbin/ldconfig
startup_dir=/etc/init.d
[ -f $startup_dir/iicvoice ] && /sbin/chkconfig --del iicvoice

%changelog
* Mon Oct 6 2008 mcrane <mcrane@novell.com> 1.0.0-1
- Initial build.
