Summary: iMeeting stub bridge
Name: iic-services-stubbridge
Version: @@@VERSION@@@
Release: 1
License: Sitescape
Group: Applications/Communication
URL: http://www.sitescape.com
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
BuildRequires: iic-xmlrpc
Requires: iic-xmlrpc

%description
This package contains the XML router and all the (non-web)
services connected to it.

%if "%{_vendor}" == "suse"
%debug_package
%endif

%prep
%setup -q
mkdir -p iic/lib
( cd iic/jcomponent; ./configure --enable-debug )
( cd iic/services; ./configure --enable-debug )
( cd iic/services; make -i depend )

%build
( cd iic/jcomponent; make )
( cd iic/services/voice; make ../../bin/stubvoiced )

%install
rm -rf $RPM_BUILD_ROOT
( cd iic; make DESTDIR=$RPM_BUILD_ROOT install-stubvoiced )

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/opt/iic/conf/init.d/iicvoice
/opt/iic/bin/stubvoiced

%postun
startup_dir=/etc/init.d
[ -f $startup_dir/iicvoice ] && /sbin/chkconfig --del iicvoice

%changelog
* Thu Oct  7 2004 root <root@dbs.homedns.org> 1.0.0-1
- Initial build.



