Summary: iMeeting Apache server
Name: iic-apache
Version: @@@VERSION@@@
Release: 1
License: Sitescape
Group: Applications/Communication
URL: http://www.sitescape.com
Source0: %{name}-%{version}.tar.gz
Source1: @@@HTTPD_TARBALL@@@
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot

%description
This package contains the web based system components

%prep
%setup -q
%setup -q -D -a 1
( rm -rf /usr/local/apache2 )
( cd @@@HTTPD_BASE@@@ ; ./buildconf )

%build
cd @@@HTTPD_BASE@@@

function mpmbuild()
{
    mpm=$1; shift
    mkdir -p $mpm; pushd $mpm
    ../configure --with-mpm=$mpm --enable-mods-shared=all --enable-deflate --enable-ssl --with-ldap --enable-ldap --enable-auth-ldap
    make
    popd
}

mpmbuild prefork
#mpmbuild worker

# Verify that the same modules were built into the two httpd binaries
#./prefork/httpd -l | grep -v prefork > prefork.mods
#./worker/httpd -l | grep -v worker > worker.mods
#if ! diff -u prefork.mods worker.mods; then
#  : Different modules built into httpd binaries, will not proceed
#  exit 1
#fi

%install
rm -rf $RPM_BUILD_ROOT
cd @@@HTTPD_BASE@@@

pushd prefork
make DESTDIR=$RPM_BUILD_ROOT install
popd

#install -m 755 worker/.libs/httpd $RPM_BUILD_ROOT/usr/local/apache2/bin/httpd.worker

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/usr/local/apache2
%doc

%post
cp /usr/local/apache2/conf/httpd.conf  /usr/local/apache2/conf/httpd-std.conf

grep -v '^Listen' /usr/local/apache2/conf/httpd-std.conf > /tmp/httpd.conf
echo "Listen @@@PORTAL_IP@@@:80" >> /tmp/httpd.conf
mv /tmp/httpd.conf /usr/local/apache2/conf

%changelog
* Thu Oct  7 2004 root <root@dbs.homedns.org> 1.0.0-1
- Initial build.
