#!/bin/bash

function usage
{
    echo "usage: $0 [--clean]"
    echo "--clean"
    echo "    uninstalls any previous version before installing current version"
}

function parse_cmdline
{
    clean="false"
    while [ $# -gt 0 ] ; do
	case $1 in
	    --help|-h)
		usage
		exit -1
		;;
	    --clean)
		clean="true"
		shift
		;;
	    *)
		echo "Unknown argument '$1' seen"
		usage
		exit -1
		;;
	esac
    done
}

function find_upgrades
{
    local excl_bridge;
    
    rpm -qa --queryformat '%{NAME}::%{VERSION}\n' | \
	sort \
    > /tmp/zoninst-all.txt

    rpm --query --queryformat '%{NAME}::%{VERSION}\n' --package *.rpm | \
	sort \
    > /tmp/zoninst-this.txt

    # this may not strictly be true since older packages in the -this.txt list will be in upgraded.txt too
    comm -13 /tmp/zoninst-all.txt /tmp/zoninst-this.txt | sed -e 's+::+-+' | sed -e 's+$+*.rpm+' > /tmp/zoninst-upgraded.txt

    sed -e 's+::.*$++' /tmp/zoninst-all.txt > /tmp/zoninst-all-name.txt
    sed -e 's+::.*$++' /tmp/zoninst-this.txt > /tmp/zoninst-this-name.txt

    # find the packages that exist on the machine now
    comm -12 /tmp/zoninst-all-name.txt /tmp/zoninst-this-name.txt > /tmp/zoninst-exist-name.txt
}

function install
{
    if [ "$clean" == "true" ] ; then
	echo -n "Removing prior installed packages..."
	( rpm -e `cat /tmp/zoninst-exist-name.txt | grep -v w3c-libwww` ) &> install.log && echo "Ok" || echo "Done"
	rm -rf /opt/iic
    fi

    echo -n "Installing RPM's..."
    if [ "$clean" == "true" ] ; then
	# it's clean; install everything
	local non_bridge_rpm=$( ls *.rpm | grep -v 'iic-services-.*bridge' | grep -v 'iic-asterisk' )
	rpm -Uhv --force $non_bridge_rpm && echo "Ok" || ( echo "FAILED; see install.log for details" ; exit -1 )
    else
	local non_bridge_rpm=$( cat /tmp/zoninst-upgraded.txt | grep -v 'iic-services-.*bridge' | grep -v 'iic-asterisk' )
	if [ ! -z "$non_bridge_rpm" ] ; then
	    # just upgrade those packages that are upgraded; except bridge.  that gets done
	    # during config when voice_provider is known
	    rpm -Uhv --force $non_bridge_rpm && echo "Ok" || ( echo "FAILED; see install.log for details" ; exit -1 )
	fi
    fi

    cp /tmp/zoninst-this.txt /opt/iic
}

parse_cmdline $*
find_upgrades
install
