#!/bin/bash

. ./utils.sh

config="conferencing"
version=`cat version.txt`

#notlame_rpm=notlame-3.96.1-1.i686.rpm
#libwww_rpm=w3c-libwww-5.4.0-5.i386.rpm
#neon_rpm=neon-0.24.7-2.1.wbel3.lpt.i386.rpm  only need for <RHEL4

package_release ${config}-svr-$version $config

echo "Created ${config}-svr-$version.tar.gz"
