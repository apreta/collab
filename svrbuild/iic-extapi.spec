Summary: iMeeting external component API
Name: iic-extapi
Version: @@@VERSION@@@
Release: 1
License: Sitescape
Group: Applications/Communication
URL: http://www.sitescape.com
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
BuildRequires: iic-xmlrpc, @@@HTTPD_PKGNAME@@@
Requires: iic-xmlrpc, @@@HTTPD_PKGNAME@@@

%if "%{_vendor}" == "suse"
Requires: libapr-util1 apache2-mod_php5
%endif

%description
This package contains the Apache module that implements the external API.

%prep
%setup -q
( cd iic/jcomponent; ./configure --enable-debug )
( cd iic/mod_jabapi; ./configure --enable-debug )

%build
( mkdir -p iic/lib )
( cd iic/jcomponent; make )
( cd iic/mod_jabapi; make )

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/opt/iic/conf/init.d
mkdir -p $RPM_BUILD_ROOT/opt/iic/conf/sysconfig
mkdir -p $RPM_BUILD_ROOT/opt/iic/httpd/modules

( cd iic/mod_jabapi; ./configure; make DESTDIR=$RPM_BUILD_ROOT install )

chmod 755 $RPM_BUILD_ROOT/opt/iic/httpd/modules/mod_jabapi.so

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/opt/iic/httpd/modules/mod_jabapi.so
/opt/iic/conf/init.d/iicextapi
/opt/iic/conf/sysconfig/iicextapi

%doc

%postun
rm -f \
	/opt/iic/httpd/conf.d/zon-extapi.conf \
	/opt/iic/http/conf.d/httpd-extapi.conf
/sbin/chkconfig --del iicextapi

%changelog
* Thu Oct  7 2004 root <builder@imidio.com> 1.0.0-1
- Initial build.
