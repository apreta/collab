#!/bin/bash

. /opt/iic/conf/global-config

function usage
{
    echo "usage: $0 {nms|appconf|stub|ext}"
}

installed=$(rpm -qa | grep 'iic-services-nmsbridge|iic-services-appconfbridge|iic-services-stubbridge')
if [ ! -z "$installed" ]; then
    rpm -e $installed
fi

if [ "$1" == "ext" ]; then
    brtype="stub"
else
    brtype=$1
fi

rpm -Uhv --force iic-services-${brtype}bridge*.rpm

# TODO: make sure we install asterisk on right server
# if [ "$1" == "appconf" ]; then
#	rpm -Uhv --force iic-asterisk*.rpm
# fi
