Summary: iMeeting XML rpc library
Name: iic-xmlrpc
Version: @@@VERSION@@@
Release: 1
License: Sitescape
Group: Applications/Communication
URL: http://www.sitescape.com
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot

%description
This package provides the XMLRPC shared library used by system components

%if "%{_vendor}" == "suse"
%debug_package
%endif

%prep
%setup -q
( cd xmlrpc2; ./configure --prefix=/opt/iic --disable-libwww-client --disable-cplusplus )

%build
( cd xmlrpc2; make )

%install
rm -rf $RPM_BUILD_ROOT

( cd xmlrpc2; make DESTDIR=$RPM_BUILD_ROOT install; make install-compat-hdr )

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc
/opt/iic/bin
/opt/iic/include
/opt/iic/lib

%post
if ! grep --silent /opt/iic/lib /etc/ld.so.conf; then
    echo "/opt/iic/lib" >> /etc/ld.so.conf
    /sbin/ldconfig
fi
# ( cd /opt/iic/lib ; ln -s libxmlrpc.so.3.3.0 libxmlrpc.so )

%postun
rm -f /opt/iic/lib/libxmlrpc.so
if [ "$1" = 0 ]; then
  grep -v /opt/iic/lib /etc/ld.so.conf > /tmp/xxx
  mv /tmp/xxx /etc/ld.so.conf
  /sbin/ldconfig
fi

%changelog
* Thu Oct  7 2004 root <root@dbs.homedns.org> 1.0.0-1
- Initial build.



