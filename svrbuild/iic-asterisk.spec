#
# spec file for package asterisk (Version 1.4.21.2.kablink)
#
#

Name:           iic-asterisk
BuildRequires:  curl-devel expat gcc-c++ wget 
BuildRequires: bison
BuildRequires: m4
BuildRequires: ncurses-devel
BuildRequires: openssl-devel
BuildRequires: autoconf 
PreReq:      coreutils sed grep


URL:            http://www.asterisk.org
Summary:        The Asterisk Open Source PBX
Version:        @@@VERSION@@@
Release:        1
License:        BSD License and BSD-like, GNU General Public License (GPL)
Group:          Productivity/Telephony/Servers
Source0:	%{name}-%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-buildroot



%description
Asterisk is an Open Source PBX and telephony development platform that
can both replace a conventional PBX and act as a platform for developing
custom telephony applications for delivering dynamic content over a
telephone similarly to how one can deliver dynamic content through a
web browser using CGI and a web server.

Asterisk provides voicemail services with directory, call conferencing,
interactive voice response, and call queuing. It has support for
three-way calling, caller ID services, ADSI, SIP, and H.323 (as both
client and gateway).

This specific Asterisk package has been customized to support the needs
for audio conference mixing for Novell Conferencing and Kablink Conferencing.
Changes include a G.726 codec that works and the addition of the appconference
module.  For more information on Kablink, please visit http://www.kablink.org.

Asterisk documentation is available on the Asterisk home page
(http://www.asterisk.org) and on the Asterisk wiki
(http://www.voip-info.org/wiki-Asterisk).



Authors:
--------
    Mark Spencer <markster@digium.com>


%if "%{_vendor}" == "suse"
%debug_package
%endif

%prep
%setup -q
( cd asterisk; ./configure --without-newt --libdir=/usr/lib64 )
%build
( cd asterisk; make )
( cd appconference; make )

%install
rm -rf $RPM_BUILD_ROOT

( cd asterisk; make install DESTDIR=%{buildroot} )
( cd asterisk; install -D -p -m 0755 contrib/init.d/rc.%{_vendor}.asterisk %{buildroot}%{_initrddir}/asterisk )
( cd appconference; make install DESTDIR=%{buildroot} )

rm -f %{buildroot}%{_localstatedir}/lib/asterisk/moh/.asterisk-moh-freeplay-wav 2>/dev/null
rm -f %{buildroot}%{_localstatedir}/lib/asterisk/moh/.asterisk-moh-freeplay-alaw 2>/dev/null
rm -f %{buildroot}%{_localstatedir}/lib/asterisk/moh/.asterisk-moh-freeplay-ulaw 2>/dev/null
rm -f %{buildroot}%{_localstatedir}/lib/asterisk/sounds/.asterisk-core-sounds-en-gsm-1.4.9 2>/dev/null
rm -f %{buildroot}%{_localstatedir}/lib/asterisk/sounds/.asterisk-core-sounds-en-alaw-1.4.9 2>/dev/null
rm -f %{buildroot}%{_localstatedir}/lib/asterisk/sounds/.asterisk-core-sounds-en-ulaw-1.4.9 2>/dev/null

cp asterisk/iic/* %{buildroot}%{_sysconfdir}/asterisk

%post
/sbin/chkconfig --add asterisk
mkdir -p -m 0775 /var/run/asterisk

%preun
%stop_on_removal
if [ $1 -eq 0 ]; then
    /sbin/service asterisk stop &> /dev/null || :
    /sbin/chkconfig --del asterisk
fi

%postun
%{?insserv_cleanup:%{insserv_cleanup}}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)

%{_initrddir}/asterisk

%dir %{_libdir}/asterisk
%dir %{_libdir}/asterisk/modules
%{_libdir}/asterisk/modules/*

%{_localstatedir}/lib/asterisk/moh/*
%{_localstatedir}/lib/asterisk/static-http/*
%{_localstatedir}/lib/asterisk/agi-bin/*
%{_localstatedir}/lib/asterisk/firmware/*
%{_localstatedir}/lib/asterisk/images/*
%{_localstatedir}/lib/asterisk/keys/*
%{_localstatedir}/lib/asterisk/sounds/*

%{_sbindir}/asterisk
%{_sbindir}/astgenkey
%{_sbindir}/autosupport
%{_sbindir}/muted
%{_sbindir}/rasterisk
%{_sbindir}/safe_asterisk
%{_sbindir}/smsq
%{_sbindir}/stereorize
%{_sbindir}/streamplayer

%{_mandir}/man8/asterisk.8*
%{_mandir}/man8/astgenkey.8*
%{_mandir}/man8/autosupport.8*
%{_mandir}/man8/safe_asterisk.8*

# /var/spool/asterisk/voicemail/*

%dir %{_sysconfdir}/asterisk
%config(noreplace) %{_sysconfdir}/asterisk/*

%changelog
* Wed Jan 21 2009 - mcrane@novell.com
- Original OBS RPM spec file changed for use in Novell/Kablink Conferencing
