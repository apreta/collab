#!/bin/bash

libical_base="libical-0.24"
libical_tarball="$libical_base.RC4.tar.gz"

SVRSRCROOT=`pwd`/..

. ./utils.sh

if [ $# -eq 1 -a ! "$1" == "--retry" ]; then
    echo $1 > version.txt
    retry="no"
elif [ $# -eq 1 -a "$1" == "--retry" ]; then
    retry="yes"
else
    retry="no"
fi

tag="local"
version=`cat version.txt`
product="conferencing"

for skip in skip-* ; do
    if [ "$skip" != "skip-*" ]; then
	label=$( echo $skip | sed -e 's+skip-++' )
	echo "Skipping build of iic-$label rpm"
    fi
done

[ ! -e $libical_tarball ]   && echo "WARNING: $libical_tarball must be present to build services"

echo -n "Removing any installed iic packages..."
clean_system && echo "Ok" || echo "FAILED"

rm -rf $package_dir/BUILD/*
rm -rf $package_dir/SOURCES/*

if [ $retry != "yes" ]; then
    touch .startdatetime
fi
build_libical_package $tag $version
build_json_package $tag $version
build_xmlrpc_package $tag $version
build_extbridge_devel_package $tag $version
build_mailer_package $tag $version
build_x264_package $tag $version
build_ffmpeg_package $tag $version
build_services_package $tag $version
build_services_stubbridge_package $tag $version
build_services_appconfbridge_package $tag $version
build_asterisk_package $tag $version
build_web_packages $tag $version
build_zon_ha_package $tag $version

./package-release.sh "conferencing"
