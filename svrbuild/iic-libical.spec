Summary: libical for Zon
Name: iic-libical
Version: @@@VERSION@@@
Release: 1
License: Sitescape
Group: Applications/Communication
URL: http://www.sitescape.com
Source0: %{name}-%{version}.tar.gz
Source1: @@@LIBICAL_TARBALL@@@
Patch0: libical-folding.patch
Patch1: libical-newline.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot

%description
This package contains the ical support for iic-mailer

%if "%{_vendor}" == "suse"
%debug_package
%endif

%prep
%setup -q
%setup -q -D -a 1
%patch0 -p1
%patch1 -p1
( cd @@@LIBICAL_BASE@@@ ; ./configure --prefix=/opt/iic )

%build
( cd @@@LIBICAL_BASE@@@ ; make )

%install
rm -rf $RPM_BUILD_ROOT
( cd @@@LIBICAL_BASE@@@ ; make DESTDIR=$RPM_BUILD_ROOT install )

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/opt/iic/share
/opt/iic/lib
/opt/iic/include

%doc

%changelog
* Thu Oct  7 2004 root <root@dbs.homedns.org> 1.0.0-1
- Initial build.
