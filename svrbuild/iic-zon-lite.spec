Summary: ipunity web app
Name: iic-zon-lite
Version: @@@VERSION@@@
Release: 1
License: Sitescape
Group: Applications/Communication
URL: http://www.sitescape.com
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Requires: php

%description
This package contains the meeting invitation processing for iMeeting

%prep
%setup -q

%build

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/var/iic/htdocs/imidio
cp -r iic/webcontent/imidio/lite $RPM_BUILD_ROOT/var/iic/htdocs/imidio

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc
/var/iic/htdocs/imidio/lite

%post

%postun

%changelog
* Thu Oct  7 2004 root <root@dbs.homedns.org> 1.0.0-1
- Initial build.



