Summary: Zon NMS-based bridge
Name: iic-services-nmsbridge
Version: @@@VERSION@@@
Release: 1
License: Sitescape
Group: Applications/Communication
URL: http://www.sitescape.com
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Requires: iic-xmlrpc, nmsnabase = 4.13-Beta, nmscnf = 2.12-Beta, nmsnacore = 4.13-Beta, nmsnaimport = 4.13-Beta
BuildRequires: iic-xmlrpc, resip, nmsnabase = 4.13-Beta, nmscnf = 2.12-Beta, nmsnacore = 4.13-Beta, nmsnaimport = 4.13-Beta

%description
This package contains the XML router and all the (non-web)
services connected to it.

%if "%{_vendor}" == "suse"
%debug_package
%endif

%prep
%setup -q
mkdir -p iic/lib
( cd iic/jcomponent; ./configure --enable-debug )
( cd iic/util/asynccomm; ./configure --enable-debug )
( cd iic/services; ./configure --enable-debug )
( cd iic/services/voice; make -k depend )
( cd iaxclient/iaxclient/lib/libiax2; ./configure --enable-static --disable-shared )

%build
( cd iic/jcomponent; make )
( cd iic/util/asynccomm; make )
( cd iic/services/voice; make ../../bin/nmsvoiced )
( cd iaxclient/iaxclient/lib/libiax2; make; make install )
( cd iic/iaxgw; make -k depend; make )

%install
rm -rf $RPM_BUILD_ROOT
( cd iic; make DESTDIR=$RPM_BUILD_ROOT install-nmsvoiced )

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/opt/iic/conf/init.d/iicvoice
/opt/iic/conf/voiceconfig.xml.in
/opt/iic/bin/nmsvoiced
/opt/iic/prompt
/opt/iic/bin/clean-bridge-recordings.sh
/opt/iic/conf/init.d/iicvoipgw
/opt/iic/bin/iaxgwd
/opt/iic/conf/nms
/opt/iic/conf/privkey.pem

%postun
startup_dir=/etc/rc.d
[ -f $startup_dir/iicvoice ] && /sbin/chkconfig --del iicvoice
[ -f $startup_dir/iicvoipgw ] && /sbin/chkconfig --del iicvoipgw

%changelog
* Thu Oct  7 2004 root <root@dbs.homedns.org> 1.0.0-1
- Initial build.



