Summary: Zon voice bridge developers kit
Name: iic-extbridge-devel
Version: @@@VERSION@@@
Release: 1
License: Sitescape
Group: Applications/Communication
URL: http://www.sitescape.com
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
BuildRequires: iic-xmlrpc
Requires: iic-xmlrpc

%description
This package contains libraries and example source for
developing interfaces to external (non-Zon) voice bridges.

%prep
%setup -q
( cd iic/jcomponent; ./configure --enable-debug )
( cd iic/services; ./configure --enable-debug )

%build
mkdir -p iic/lib
( cd iic/jcomponent; make )
( cd iic/services/voice; make -f Makefile.extvoiced )

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/opt/iic/lib
mkdir -p $RPM_BUILD_ROOT/opt/iic/bin
( cd iic/jcomponent ; make DESTDIR=$RPM_BUILD_ROOT install )
( cd iic/services/voice ; make DESTDIR=$RPM_BUILD_ROOT -f Makefile.extvoiced dist )


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/opt/iic/bin/extvoiced
/opt/iic/lib/libjcomponent.a
/opt/iic/lib/libjcomponentsl.a
/opt/iic/include/common/common.h
/opt/iic/include/common/strings.h
/opt/iic/include/jcomponent/jcomponent.h
/opt/iic/include/jcomponent/jcomponent_ext.h
/opt/iic/include/jcomponent/mio/mio.h
/opt/iic/include/jcomponent/rpc.h
/opt/iic/include/jcomponent/util/log.h
/opt/iic/include/jcomponent/util/nadutils.h
/opt/iic/include/jcomponent/util/rpcresult.h
/opt/iic/include/jcomponent/util/util.h
/opt/iic/include/jcomponent/xmlparse/xmlparse.h
/opt/iic/include/util/iobject.h
/opt/iic/include/util/istring.h
/opt/iic/include/util/ithread.h
/opt/iic/src/extvoiced/README.extvoiced
/opt/iic/src/extvoiced/Makefile.extvoiced
/opt/iic/src/extvoiced/extvoiced.cpp

%changelog
* Thu Oct  7 2004 root <root@dbs.homedns.org> 1.0.0-1
- Initial build.
