#!/bin/bash

function usage
{
    echo "usage: $0 [--initdb]"
}

initdb="false"
function parse_cmdline
{
    while [ $# -gt 0 ] ; do
	case $1 in
	    --initdb)
		initdb="true"
		shift
		;;
	    --help|-h)
		usage
		exit -1
		;;
	    *)
		echo "Unknown argument '$1' seen"
		usage
		exit -1
		;;
	esac
    done
}

. ./inst-utils.sh

parse_cmdline $*

[ installers.tar.gz -nt /opt/iic/conf/installers.tar.gz ] && \
    cp installers.tar.gz /opt/iic/conf

pushd /opt/iic/conf

. ./global-config
. ./utils.sh

ctl_services stop &> /dev/null

if [ -e ./config_db.sh -a "$initdb" == "true" ] ; then
    ./config_db.sh --use-cached-config
fi
[ -e ./questions-*.sh ]     && ./questions-*.sh
[ -e ./config_jabber.sh ]   && ./config_jabber.sh
[ -e ./config_jcomp.sh ]    && ./config_jcomp.sh
[ -e ./config_sharemgr.sh ] && ./config_sharemgr.sh
./config_init.sh
./config_web.sh

popd

chown -R $iic_user /opt/iic /var/iic /var/log/iic

ctl_services start
