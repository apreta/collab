Summary: iMeeting XML router and and all non-web services
Name: iic-services
Version: @@@VERSION@@@
Release: 1
License: Sitescape
Group: Applications/Communication
URL: http://www.sitescape.com
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
BuildRequires: iic-xmlrpc iic-mailer iic-libical
Requires: iic-xmlrpc iic-libical

%description
This package contains the XML router and all the (non-web)
services connected to it.

%package xmlrouter
Group: Applications/Communication
Summary: iMeeting XML router service

%description xmlrouter
iMeeting XML router service

%package xmlrouter-services
Group: Applications/Communication
Summary: iMeeting XML Zon services connected to the XML router

%description xmlrouter-services
iMeeting XML Zon services connected to the XML router

%package appshare
Group: Applications/Communication
Summary: iMeeting application/desktop sharing

%description appshare
iMeeting XML application/desktop sharing

%package sharemgr
Group: Applications/Communication
Summary: iMeeting daemons etc. for "share lite"

%description sharemgr
iMeeting daemons etc. for "share lite"

%package db
Group: Applications/Communication
Summary: iMeeting database

%description db
iMeeting database

%if "%{_vendor}" == "suse"
%debug_package
%endif

%prep
%setup -q
( cd jabberd; ./configure --enable-debug )
( cd jadc2s; ./configure --enable-debug --enable-ssl )
( cd iic; ./configure --enable-debug )
( cd iic/services; make -i depend )
( cd vnc2mov; ./configure )

%build
( cd jabberd; make )
( cd jadc2s; make )
mkdir -p iic/lib
( cd iic; make )
( cd vnc2mov; make )

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/opt/iic/lib
( cd jabberd; make DESTDIR=$RPM_BUILD_ROOT/opt/iic/jabberd install )
( cd iic; make DESTDIR=$RPM_BUILD_ROOT install )
#( cd vncmov; make DESTDIR=$RPM_BUILD_ROOT install )
mkdir -p $RPM_BUILD_ROOT/var/run/iic
mkdir -p $RPM_BUILD_ROOT/var/iic/cdr
mkdir -p $RPM_BUILD_ROOT/var/iic/mtgspool/active
mkdir -p $RPM_BUILD_ROOT/var/iic/mtgspool/converting
mkdir -p $RPM_BUILD_ROOT/var/iic/mtgspool/done

# Get rid of not related to run-time stuff
rm -rf $RPM_BUILD_ROOT/opt/iic/include

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/opt/iic/conf/utils.sh
/opt/iic/conf/configure.sh
/opt/iic/conf/control-services.sh
/opt/iic/bin/nanny
/opt/iic/bin/logger
#/opt/iic/bin/rpminject
#
/opt/iic/conf/init.d/iicproxy
/opt/iic/bin/proxyd
#
/opt/iic/bin/vnc2mov
/opt/iic/conf/config_init.sh
/opt/iic/conf/zon-rt-user
/opt/iic/conf/zon-rt-call
/opt/iic/conf/zon-rt-rsrv
/opt/iic/conf/config_batch.sh
/opt/iic/conf/config_web.sh
/opt/iic/conf/config_dist_sles10.sh
/opt/iic/conf/config_dist_rhel4.sh
/opt/iic/conf/config_dist_rhel5.sh
/opt/iic/conf/zonportal.in
/opt/iic/conf/zonextapi.in
/opt/iic/conf/removeworker.awk
/opt/iic/conf/init.d/iicportal
/opt/iic/conf/init.d/iicutils
/opt/iic/conf/webcontent
/opt/iic/webcontent/imidio
/opt/iic/conf/iicversion.txt
/opt/iic/bin/maintain-archives.sh
/opt/iic/bin/maintain-docshare.sh
%dir /var/log/iic
/opt/iic/locale

%files db
%defattr(-,root,root,-)
/opt/iic/conf/init.d/iicpgpid
/opt/iic/conf/database.sql.in
/opt/iic/conf/systemusers.sql.in
/opt/iic/lib/libsqldb.so
/opt/iic/conf/config_db.sh
/opt/iic/conf/pg_hba.conf.in
/opt/iic/conf/postgresql.conf
/opt/iic/bin/db-backup.sh
/opt/iic/conf/db_upgrade.sh
/opt/iic/conf/db_upgrade_23.sql

%files appshare
%defattr(-,root,root,-)
/opt/iic/conf/init.d/iicshare
/opt/iic/conf/init.d/iictransmitter
/opt/iic/bin/reflectord
/opt/iic/bin/transmitterd
/opt/iic/conf/hosts
/opt/iic/conf/passwd
/opt/iic/bin/clean-appshare-recordings.sh

%files sharemgr
%defattr(-,root,root,-)
/opt/iic/conf/config-ipunity.defaults
/opt/iic/conf/init.d/iicsharemgr
/opt/iic/conf/sysconfig/imidio_iic_ipunity.in
/opt/iic/bin/sharemgrd
/opt/iic/conf/sharemgr.xml.in
/opt/iic/conf/sharemgr_share_map.xml.in
/opt/iic/conf/sharemgr/mime.types
/opt/iic/conf/sharemgr/sharemgr-http.conf.in
%dir /var/run/iic
%dir /opt/iic/sharemgr/htdocs

%files xmlrouter
%defattr(-,root,root,-)
# system-wide configuration variables for daemons
/opt/iic/conf/sysconfig/imidio_iic_zon.in
# the following are c2s related
/opt/iic/conf/init.d/iicjadc2s
/opt/iic/bin/jadc2s
# the following are used to configure jabber according to cached config
/opt/iic/conf/config_jabber.sh
/opt/iic/jabberd/jabber.xml.in
/opt/iic/jabberd/service.xml.in
/opt/iic/jabberd/service_map.xml.in
/opt/iic/jabberd/jadc2s.xml.in
/opt/iic/jabberd/controller_map.xml.in
/opt/iic/jabberd/clogger.xml
# the following are jabberd related
/opt/iic/conf/init.d/iicjabber
/opt/iic/jabberd/jabberd/jabberd
/opt/iic/jabberd/dnsrv/dnsrv.so
/opt/iic/jabberd/xdb_sql/xdb_sql.so
/opt/iic/jabberd/xdb_sql/xdb_sql.xml.in
/opt/iic/jabberd/xdb_file/xdb_file.so
/opt/iic/jabberd/conference-0.4/conference.so
/opt/iic/jabberd/dialback/dialback.so
#/opt/iic/jabberd/jabberd/pth-1.4.0/.libs/libpth.so
/opt/iic/jabberd/jsm/jsm.so
/opt/iic/jabberd/pthsock/pthsock_client.so
/opt/iic/jabberd/jabber-lite.xml.in
/opt/iic/jabberd/spool/share/share.xml
%dir /var/run/iic


%files xmlrouter-services
%defattr(-,root,root,-)
# default configuration for the jabber-based system
/opt/iic/conf/config.defaults
# configures the jcomponents of the system given the cached config
/opt/iic/conf/config_jcomp.sh
# The following are Zon address book (i.e. database interface) related
/opt/iic/conf/init.d/iicaddress
/opt/iic/bin/addressd
/opt/iic/bin/event_rdr
/opt/iic/bin/ldap-sync.sh
/opt/iic/bin/ldap-sync.xml
/opt/iic/conf/new-user-template.default
/opt/iic/conf/meeting-summary-template.default
/opt/iic/conf/sqldb.xml.in
/opt/iic/conf/client_upgrade_file.imi.in
/opt/iic/conf/ldap.xml
# The following are Zon meeting controller related items
/opt/iic/conf/init.d/iiccontroller
/opt/iic/bin/controllerd
/opt/iic/conf/invitation-template.default
/opt/iic/conf/transport.xml
/opt/iic/conf/dialing.xml
/opt/iic/bin/clean-appshare-recordings.sh
/opt/iic/bin/clean-cdr-records.sh
# The following are Zon mailer related items
%dir /var/iic/cdr
/opt/iic/conf/init.d/iicmailer
/opt/iic/bin/mailerd
# The following are meeting archiver related items
/opt/iic/conf/init.d/iicmtgarchiver
/opt/iic/bin/mtgarchiverd
/opt/iic/bin/clean-mtgspool.sh
%dir /var/iic/mtgspool/active
%dir /var/iic/mtgspool/converting
%dir /var/iic/mtgspool/done
/opt/iic/conf/dummy2.html.in
/opt/iic/conf/dummy.html.in
/opt/iic/conf/dummy.mp3
/opt/iic/conf/new-mtgarchive-template.default
# The following are inputs to the script conf/config_jcomp.sh
/opt/iic/conf/config_begin.xml.in
/opt/iic/conf/jcomp_begin.xml.in
/opt/iic/conf/controller.xml.in
/opt/iic/conf/controller_share_svrs.xml.in
/opt/iic/conf/controller_voice_svrs.xml.in
/opt/iic/conf/voice.xml.in
/opt/iic/conf/mtgarchiver.xml.in
/opt/iic/conf/addressbk.xml.in
/opt/iic/conf/jadc2s.xml.in
/opt/iic/conf/jadc2s_ssl.xml.in
/opt/iic/conf/extapi.xml.in
/opt/iic/conf/extportal.xml.in
/opt/iic/conf/mailer.xml.in
/opt/iic/conf/jcomp_end.xml.in
/opt/iic/conf/config_end.xml.in
# The following are used for ssl configuration
/opt/iic/conf/make_root_ca.sh
/opt/iic/conf/make_server_cert.sh
/opt/iic/conf/make_wserver_cert.sh
# Converter service
/opt/iic/bin/converter
/opt/iic/conf/converter.xml.in
/opt/iic/conf/init.d/iicconverter

%post
if ! grep --silent /opt/iic/lib /etc/ld.so.conf ; then
    echo "/opt/iic/lib" >> /etc/ld.so.conf
fi
/sbin/ldconfig
usermod -c "Zon services" -d /opt/iic zon || useradd -m -d /opt/iic zon

%postun
if [ "$1" -eq 0 ]; then
    grep -v /opt/iic/lib /etc/ld.so.conf > /tmp/xxx
    mv /tmp/xxx /etc/ld.so.conf
fi
/sbin/ldconfig
startup_dir=/etc/init.d
[ -f $startup_dir/iicmtgarchiver ] && /sbin/chkconfig --del iicmtgarchiver
[ -f $startup_dir/iicmailer ] && /sbin/chkconfig --del iicmailer
[ -f $startup_dir/iiccontroller ] && /sbin/chkconfig --del iiccontroller
[ -f $startup_dir/iicaddress ] && /sbin/chkconfig --del iicaddress
[ -f $startup_dir/iicjabber ] && /sbin/chkconfig --del iicjabber
[ -f $startup_dir/iicjadc2s ] && /sbin/chkconfig --del iicjadc2s
[ -f $startup_dir/iicportal ] && /sbin/chkconfig --del iicportal
[ -f $startup_dir/iicshare ] && /sbin/chkconfig --del iicshare
[ -f $startup_dir/iictransmitter ] && /sbin/chkconfig --del iictransmitter
[ -f $startup_dir/iicpgpid ] && /sbin/chkconfig --del iicpgpid
[ -f $startup_dir/iicproxy ] && /sbin/chkconfig --del iicproxy
[ -f $startup_dir/iicconverter ] && /sbin/chkconfig --del iicconverter
test 0

%changelog
* Thu Oct  7 2004 root <root@dbs.homedns.org> 1.0.0-1
- Initial build.
