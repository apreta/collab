#!/bin/bash

vservers=/vservers

if [ $# -ne 2 ]; then
    echo "Usage: $0 <servername> <user>"
    exit -1
fi

echo "Entering server $1..."

export CVSROOT=:pserver:$2@jabber.homedns.org:/opt/cvsroot

chroot $vservers/$1 sudo -u $2 bash --login
