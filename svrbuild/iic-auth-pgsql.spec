Summary: Apache module for authentication with pgsql
Name: iic-auth-pgsql
Version: @@@VERSION@@@
Release: 1
License: Sitescape
Group: Applications/Communication
URL: http://www.sitescape.com
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
BuildRequires: httpd
Requires: httpd

%description
This package contains the Apache module for repository authentication.

%prep
%setup -q

%build
( mkdir -p iic/lib )
( cd iic/mod_auth_pgsql; make )

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/opt/iic/conf/init.d
mkdir -p $RPM_BUILD_ROOT/opt/iic/conf/sysconfig

( cd iic/mod_auth_pgsql; make DESTDIR=$RPM_BUILD_ROOT install )

mkdir -p $RPM_BUILD_ROOT/opt/iic/httpd/modules
cp /opt/iic/httpd/modules/mod_auth_pgsql.so $RPM_BUILD_ROOT/opt/iic/httpd/modules
chmod 755 $RPM_BUILD_ROOT/opt/iic/httpd/modules/mod_auth_pgsql.so

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/opt/iic/httpd/modules/mod_auth_pgsql.so

%doc

%post

%postun

%changelog
* Thu Oct  7 2004 root <builder@imidio.com> 1.0.0-1
- Initial build.
