Summary: iMeeting json library
Name: iic-json
Version: @@@VERSION@@@
Release: 1
License: Sitescape
Group: Applications/Communication
URL: http://novell.com
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot

%description
This package provides the json shared library used by system components

%if "%{_vendor}" == "suse"
%debug_package
%endif

%prep
%setup -q
( cd json-c; ./configure --prefix=/opt/iic )

%build
( cd json-c; make )

%install
rm -rf $RPM_BUILD_ROOT

( cd json-c; make DESTDIR=$RPM_BUILD_ROOT install )

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc
/opt/iic/include
/opt/iic/lib

%post
if ! grep --silent /opt/iic/lib /etc/ld.so.conf; then
    echo "/opt/iic/lib" >> /etc/ld.so.conf
    /sbin/ldconfig
fi
# ( cd /opt/iic/lib ; ln -s libjson.so.3.3.0 libjson.so )

%postun
# rm -f /opt/iic/lib/libjson.so
if [ "$1" = 0 ]; then
  grep -v /opt/iic/lib /etc/ld.so.conf > /tmp/xxx
  mv /tmp/xxx /etc/ld.so.conf
  /sbin/ldconfig
fi

%changelog
* Fri Aug 8 2008 root <root@dbs.homedns.org> 1.0.0-1
- Initial build.

