Summary: iMeeting XML router and and all non-web services
Name: iic-mailer
Version: @@@VERSION@@@
Release: 1
License: Sitescape
Group: Applications/Communication
URL: http://www.sitescape.com
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot

%description
This package contains the shared libraries for e-mail support

%if "%{_vendor}" == "suse"
%debug_package
%endif

%prep
%setup -q
( cd jwsmtp; mkdir build )

%build
( cd jwsmtp; make lib )

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/opt/iic/lib
( cd jwsmtp; make PREFIX=$RPM_BUILD_ROOT/opt/iic/ libinstall )

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/opt/iic/include/jwsmtp/compat.h
/opt/iic/include/jwsmtp/jwsmtp.h
/opt/iic/lib/libjwsmtp.a
/opt/iic/lib/libjwsmtp.so.1.0.0

%post
if ! grep --silent /opt/iic/lib /etc/ld.so.conf ; then
    echo "/opt/iic/lib" >> /etc/ld.so.conf
fi
/sbin/ldconfig

%postun
if [ "$1" = 0 ]; then
  grep -v /opt/iic/lib /etc/ld.so.conf > /tmp/xxx
  mv /tmp/xxx /etc/ld.so.conf
fi
/sbin/ldconfig

%changelog
* Thu Oct  7 2004 root <root@dbs.homedns.org> 1.0.0-1
- Initial build.



