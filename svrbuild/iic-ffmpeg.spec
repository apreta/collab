Summary: FFMPEG built for use in iMeeting
Name: iic-ffmpeg
Version: @@@VERSION@@@
Release: 1
License: GPL
Group: Applications/Communication
URL: http://www.sitescape.com
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot

%description
This package contains video processing libraries.

%if "%{_vendor}" == "suse"
%debug_package
%endif

%prep
%setup -q
( cd ffmpeg; ./configure --enable-debug --prefix=/opt/iic --enable-shared --enable-libx264 --enable-libmp3lame --enable-libfaac --enable-gpl --enable-nonfree --extra-cflags="-I /opt/iic/include" --extra-ldflags="-L /opt/iic/lib" )

%build
( cd ffmpeg; make )

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT
( cd ffmpeg; make DESTDIR=$RPM_BUILD_ROOT install )

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/opt/iic/bin/ffmpeg
/opt/iic/bin/ffprobe
/opt/iic/bin/ffserver
/opt/iic/include/libavcodec
/opt/iic/include/libavdevice
/opt/iic/include/libavfilter
/opt/iic/include/libavformat
/opt/iic/include/libavutil
/opt/iic/include/libpostproc
/opt/iic/include/libswscale
/opt/iic/lib/libavcodec.a
/opt/iic/lib/libavcodec.so
/opt/iic/lib/libavcodec.so.53
/opt/iic/lib/libavcodec.so.53.6.0
/opt/iic/lib/libavdevice.a
/opt/iic/lib/libavdevice.so
/opt/iic/lib/libavdevice.so.53
/opt/iic/lib/libavdevice.so.53.0.0
/opt/iic/lib/libavfilter.a
/opt/iic/lib/libavfilter.so
/opt/iic/lib/libavfilter.so.2
/opt/iic/lib/libavfilter.so.2.10.0
/opt/iic/lib/libavformat.a
/opt/iic/lib/libavformat.so
/opt/iic/lib/libavformat.so.53
/opt/iic/lib/libavformat.so.53.2.0
/opt/iic/lib/libavutil.a
/opt/iic/lib/libavutil.so
/opt/iic/lib/libavutil.so.51
/opt/iic/lib/libavutil.so.51.2.1
/opt/iic/lib/libpostproc.a
/opt/iic/lib/libpostproc.so
/opt/iic/lib/libpostproc.so.51
/opt/iic/lib/libpostproc.so.51.2.0
/opt/iic/lib/libswscale.a
/opt/iic/lib/libswscale.so
/opt/iic/lib/libswscale.so.0
/opt/iic/lib/libswscale.so.0.14.0
/opt/iic/lib/pkgconfig
/opt/iic/share/ffmpeg

%post
if ! grep --silent /opt/iic/lib /etc/ld.so.conf ; then
    echo "/opt/iic/lib" >> /etc/ld.so.conf
fi
/sbin/ldconfig

%postun
if [ "$1" = 0 ]; then
  grep -v /opt/iic/lib /etc/ld.so.conf > /tmp/xxx
  mv /tmp/xxx /etc/ld.so.conf
fi
/sbin/ldconfig

%changelog
* Thu Oct  7 2004 root <root@dbs.homedns.org> 1.0.0-1
- Initial build.



