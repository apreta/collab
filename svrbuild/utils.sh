
if [ -f ./settings.sh ]; then
	. ./settings.sh
fi

if [ -f /etc/SuSE-release ]; then
	sles_install="y"
else
	sles_install=""
fi

os_arch=`arch`

if [ "$sles_install" ]; then
	package_dir="/usr/src/packages"
	package_arch="i586"
	httpd_pkgname=apache2
else
	package_dir="/root/rpmbuild"
	if [ "$os_arch" == "x86_64" ]; then
		package_arch="x86_64"
	else
		package_arch="i386"
	fi
	httpd_pkgname=httpd
fi

function clean_system
{
    local pkgs=$( rpm -qa | grep '^iic' )
    [ ! -z "$pkgs" ] && rpm -e $pkgs

    return 0;
}

function needs_update
{
    local fn=$1

    if [ ! -e $fn ] || [ $fn -ot .startdatetime ]; then
	return 0
    else
	return 255
    fi
}

function check_product
{
    local fn=$1

    if [ -e $fn ]; then
	[ $fn -ot .startdatetime ] && echo "WARNING: $fn wasn't built during the last build"
    else
	echo "WARNING: $fn doesn't exist after the last build"
	return 255
    fi

    return 0
}

function copy_module
{
    local SRC_ROOT=$1
    local module=$2
    local DEST_ROOT=$PWD

    pushd $SRC_ROOT >/dev/null

    tar -c --exclude="CVS*" --exclude="*.a" --exclude="*.la" --exclude="*.o" \
	   --exclude="*.lo" --exclude="*.so" --exclude="*.spec" --exclude=".deps" \
	   --exclude="*.so.*"  --exclude="*.gch" --exclude=".svn" \
	   $module | \
	   tar -x -C $DEST_ROOT

    popd >/dev/null
}

function export_module
{
    local tag=$1
    local module=$2

	if [ -d $SVRSRCROOT/$module ] ; then
	    echo -n "Copying $module from $SVRSRCROOT ..."
	    (copy_module $SVRSRCROOT $module; (cd $module; make -k clean) ; echo ) >& ../${module}-copy.log
	else
	    echo -n "Missing source for module $module"
	fi

    return $?
}

function make_json_tarball
{
    tag=$1
    version=$2

    rm -rf iic-json-$version
    mkdir -p iic-json-$version

    ( cd iic-json-$version
	export_module $tag json-c && echo "Ok" || echo "FAILED" )

    tar zcf $package_dir/SOURCES/iic-json-$version.tar.gz \
	iic-json-$version

    rm -rf iic-json-$version
}

function make_json_specfile
{
    sed \
	-e "s/@@@VERSION@@@/$version/g" \
	iic-json.spec > $package_dir/SPECS/iic-json-$version-1.spec
}

function make_json_rpm
{
    echo -n "Building iic-json rpm..."
    rpmbuild -ba $package_dir/SPECS/iic-json-$version-1.spec \
	&>  json-rpmbuild.log && echo "Ok" || echo "FAILED"
}


function build_json_package
{
    tag=$1
    version=$2

    [ ! -e skip-json ] && \
	needs_update $package_dir/RPMS/$package_arch/iic-json-$version-1.$package_arch.rpm  && \
	make_json_tarball $tag $version && \
	make_json_specfile $version && \
	make_json_rpm $version
}

function package_json
{
    if check_product $package_dir/RPMS/$package_arch/iic-json-$version-1.$package_arch.rpm ;  then
	cp $package_dir/RPMS/$package_arch/iic-json-$version-1.$package_arch.rpm $1
    fi
}

function make_xmlrpc_tarball
{
    tag=$1
    version=$2

    rm -rf iic-xmlrpc-$version
    mkdir -p iic-xmlrpc-$version

    ( cd iic-xmlrpc-$version
	export_module $tag xmlrpc2 && echo "Ok" || echo "FAILED" )

    tar zcf $package_dir/SOURCES/iic-xmlrpc-$version.tar.gz \
	iic-xmlrpc-$version

    rm -rf iic-xmlrpc-$version
}

function make_xmlrpc_specfile
{
    sed \
	-e "s/@@@VERSION@@@/$version/g" \
	iic-xmlrpc.spec > $package_dir/SPECS/iic-xmlrpc-$version-1.spec
}

function make_xmlrpc_rpm
{
    echo -n "Building iic-xmlrpc rpm..."
    rpmbuild -ba $package_dir/SPECS/iic-xmlrpc-$version-1.spec \
	&>  xmlrpc-rpmbuild.log && echo "Ok" || echo "FAILED"
}

function build_xmlrpc_package
{
    tag=$1
    version=$2

    [ ! -e skip-xmlrpc ] && \
	needs_update $package_dir/RPMS/$package_arch/iic-xmlrpc-$version-1.$package_arch.rpm  && \
	make_xmlrpc_tarball $tag $version && \
	make_xmlrpc_specfile $version && \
	make_xmlrpc_rpm $version
}

function package_xmlrpc
{
    if check_product $package_dir/RPMS/$package_arch/iic-xmlrpc-$version-1.$package_arch.rpm ;  then
	cp $package_dir/RPMS/$package_arch/iic-xmlrpc-$version-1.$package_arch.rpm $1
    fi
}

function build_mailer_package
{
    tag=$1
    version=$2

    [ ! -e skip-mailer ] && \
	needs_update $package_dir/RPMS/$package_arch/iic-mailer-$version-1.$package_arch.rpm  && \
	make_mailer_tarball $tag $version && \
	make_mailer_specfile $version && \
	make_mailer_rpm $version
}

function make_mailer_tarball
{
    tag=$1
    version=$2

    rm -rf iic-mailer-$version
    mkdir -p iic-mailer-$version

    ( cd iic-mailer-$version
	export_module $tag jwsmtp && echo "Ok" || echo "FAILED" )

    tar zcf $package_dir/SOURCES/iic-mailer-$version.tar.gz \
	iic-mailer-$version

    rm -rf iic-mailer-$version
}

function make_mailer_specfile
{
    sed \
	-e "s/@@@VERSION@@@/$version/g" \
	iic-mailer.spec > $package_dir/SPECS/iic-mailer-$version-1.spec
}

function make_mailer_rpm
{
    echo -n "Building iic-mailer rpm..."
    rpmbuild -ba $package_dir/SPECS/iic-mailer-$version-1.spec \
	&>  mailer-rpmbuild.log && echo "Ok" || echo "FAILED"
}

function package_mailer
{
    if check_product $package_dir/RPMS/$package_arch/iic-mailer-$version-1.$package_arch.rpm ;  then
	cp $package_dir/RPMS/$package_arch/iic-mailer-$version-1.$package_arch.rpm $1
    fi
}

function build_libical_package
{
    tag=$1
    version=$2

    [ ! -e skip-libical ] && \
	needs_update $package_dir/RPMS/$package_arch/iic-libical-$version-1.$package_arch.rpm  && \
	make_libical_tarball $tag $version && \
	make_libical_specfile $version && \
	make_libical_rpm $version
}

function make_libical_tarball
{
    tag=$1
    version=$2

    rm -rf iic-libical-$version
    mkdir -p iic-libical-$version

    tar zcf $package_dir/SOURCES/iic-libical-$version.tar.gz \
	iic-libical-$version

    cp $libical_tarball $package_dir/SOURCES
    cp libical*patch $package_dir/SOURCES

    rm -rf iic-libical-$version
}

function make_libical_specfile
{
    sed \
	-e "s/@@@VERSION@@@/$version/g" \
	-e "s/@@@LIBICAL_BASE@@@/$libical_base/g" \
	-e "s/@@@LIBICAL_TARBALL@@@/$libical_tarball/g" \
	iic-libical.spec > $package_dir/SPECS/iic-libical-$version-1.spec
}

function make_libical_rpm
{
    echo -n "Building iic-libical rpm..."
    rpmbuild -ba $package_dir/SPECS/iic-libical-$version-1.spec \
	&>  libical-rpmbuild.log && echo "Ok" || echo "FAILED"
}

function package_libical
{
    if check_product $package_dir/RPMS/$package_arch/iic-libical-$version-1.$package_arch.rpm ;  then
	cp $package_dir/RPMS/$package_arch/iic-libical-$version-1.$package_arch.rpm $1
    fi
}

#++++

function build_x264_package
{
    tag=$1
    version=$2

    [ ! -e skip-x264 ] && \
	needs_update $package_dir/RPMS/$package_arch/iic-x264-$version-1.$package_arch.rpm  && \
	make_x264_tarball $tag $version && \
	make_x264_specfile $version && \
	make_x264_rpm $version
}

function make_x264_tarball
{
    tag=$1
    version=$2

    rm -rf iic-x264-$version
    mkdir -p iic-x264-$version

    ( cd iic-x264-$version
	export_module $tag x264 && echo "Ok" || echo "FAILED" )

    tar zcf $package_dir/SOURCES/iic-x264-$version.tar.gz \
	iic-x264-$version

    rm -rf iic-x264-$version
}

function make_x264_specfile
{
    sed \
	-e "s/@@@VERSION@@@/$version/g" \
	iic-x264.spec > $package_dir/SPECS/iic-x264-$version-1.spec
}

function make_x264_rpm
{
    echo -n "Building iic-x264 rpm..."
    rpmbuild -ba $package_dir/SPECS/iic-x264-$version-1.spec \
	&>  x264-rpmbuild.log && echo "Ok" || echo "FAILED"
}

function package_x264
{
    if check_product $package_dir/RPMS/$package_arch/iic-x264-$version-1.$package_arch.rpm ;  then
	cp $package_dir/RPMS/$package_arch/iic-x264-$version-1.$package_arch.rpm $1
    fi
}

#++++

function build_ffmpeg_package
{
    tag=$1
    version=$2

    [ ! -e skip-ffmpeg ] && \
	needs_update $package_dir/RPMS/$package_arch/iic-ffmpeg-$version-1.$package_arch.rpm  && \
	rpm -Uhv $package_dir/RPMS/$package_arch/iic-x264-$version-1.$package_arch.rpm && \
	make_ffmpeg_tarball $tag $version && \
	make_ffmpeg_specfile $version && \
	make_ffmpeg_rpm $version && \
	rpm -e iic-x264
}

function make_ffmpeg_tarball
{
    tag=$1
    version=$2

    rm -rf iic-ffmpeg-$version
    mkdir -p iic-ffmpeg-$version

    ( cd iic-ffmpeg-$version
	export_module $tag ffmpeg && echo "Ok" || echo "FAILED" )

    tar zcf $package_dir/SOURCES/iic-ffmpeg-$version.tar.gz \
	iic-ffmpeg-$version

    rm -rf iic-ffmpeg-$version
}

function make_ffmpeg_specfile
{
    sed \
	-e "s/@@@VERSION@@@/$version/g" \
	iic-ffmpeg.spec > $package_dir/SPECS/iic-ffmpeg-$version-1.spec
}

function make_ffmpeg_rpm
{
    echo -n "Building iic-ffmpeg rpm..."
    rpmbuild -ba $package_dir/SPECS/iic-ffmpeg-$version-1.spec \
	&>  ffmpeg-rpmbuild.log && echo "Ok" || echo "FAILED"
}

function package_ffmpeg
{
    if check_product $package_dir/RPMS/$package_arch/iic-ffmpeg-$version-1.$package_arch.rpm ;  then
	cp $package_dir/RPMS/$package_arch/iic-ffmpeg-$version-1.$package_arch.rpm $1
    fi
}

#++++

function make_meeting_invite_tarball
{
    rm -rf iic-meeting-invite-$version
    mkdir iic-meeting-invite-$version

    ( cd iic-meeting-invite-$version
	export_module $tag iic && echo "Ok" || echo "FAILED" )

    ( tar zcf $package_dir/SOURCES/iic-meeting-invite-$version.tar.gz \
	    iic-meeting-invite-$version/iic/webcontent/imidio/invite )

    rm -rf iic-meeting-invite-$version
}

function make_meeting_invite_specfile
{
    sed \
	-e "s/@@@VERSION@@@/$version/g" \
	iic-meeting-invite.spec > \
    $package_dir/SPECS/iic-meeting-invite-$version-1.spec
}

function make_meeting_invite_rpm
{
    echo -n "Building iic-meeting-invite rpm..."

    rpmbuild -ba $package_dir/SPECS/iic-meeting-invite-$version-1.spec \
	&>  meeting-invite-rpmbuild.log && echo "Ok" || echo "FAILED"
}

function make_console_tarball
{
    rm -rf iic-console-$version
    mkdir iic-console-$version

    ( cd iic-console-$version ; \
	export_module $tag iic && echo "Ok" || echo "FAILED" )

    ( tar zcf $package_dir/SOURCES/iic-console-$version.tar.gz \
	    iic-console-$version/iic/console )

    rm -rf iic-console-$version
}

function make_console_specfile
{
    sed \
	-e "s/@@@VERSION@@@/$version/g" \
	iic-console.spec \
    > $package_dir/SPECS/iic-console-$version-1.spec
}

function make_console_rpm
{
    echo -n "Building iic-console rpm..."

    rpmbuild -ba $package_dir/SPECS/iic-console-$version-1.spec \
	&>  console-rpmbuild.log && echo "Ok" || echo "FAILED"
}

function make_web_base_tarball
{
    rm -rf iic-web-base-$version
    mkdir iic-web-base-$version

    ( cd iic-web-base-$version
	export_module $tag iic && echo "Ok" || echo "FAILED" )

    ( cd iic-web-base-$version
	export_module $tag docs && echo "Ok" || echo "FAILED" )

    if [ "$oss_build" == "y" ]; then
        docpath="docs/open"
    else
        docpath="docs/beta/manual"
    fi

    ( tar zcf $package_dir/SOURCES/iic-web-base-$version.tar.gz \
	iic-web-base-$version/iic/webcontent \
	iic-web-base-$version/$docpath/HTML \
	iic-web-base-$version/$docpath/pdfs )

    rm -rf iic-web-base-$version
}

function make_web_base_specfile
{
    sed \
	-e "s/@@@VERSION@@@/$version/g" \
	-e "s/@@@HTTPD_PKGNAME@@@/$httpd_pkgname/" \
	iic-web-base.spec > \
    $package_dir/SPECS/iic-web-base-$version-1.spec
}

function make_web_base_rpm
{
    echo -n "Building iic-web-base rpm..."

    rpmbuild -ba $package_dir/SPECS/iic-web-base-$version-1.spec \
	&>  web-base-rpmbuild.log && echo "Ok" || echo "FAILED"
}

function build_zon_ha_package
{
    tag=$1
    version=$2

    [ ! -e skip-zon-ha ] && \
	needs_update $package_dir/RPMS/$package_arch/iic-zon-ha-$version-1.$package_arch.rpm  && \
	make_zon_ha_tarball $tag $version && \
	make_zon_ha_specfile $version && \
	make_zon_ha_rpm $version
}

function make_zon_ha_tarball
{
    rm -rf iic-zon-ha-$version
    mkdir iic-zon-ha-$version

    ( cd iic-zon-ha-$version
	export_module $tag iic && echo "Ok" || echo "FAILED" )

    ( tar zcf $package_dir/SOURCES/iic-zon-ha-$version.tar.gz \
        iic-zon-ha-$version/iic/conf/config_ha.sh \
	iic-zon-ha-$version/iic/conf/ha.d iic-zon-ha-$version/iic/conf/ha.d/resource.d )

    rm -rf iic-zon-ha-$version
}

function make_zon_ha_specfile
{
    sed \
	-e "s/@@@VERSION@@@/$version/g" \
	iic-zon-ha.spec \
    > $package_dir/SPECS/iic-zon-ha-$version-1.spec
}

function make_zon_ha_rpm
{
    echo -n "Building iic-zon-ha rpm..."

    rpmbuild -ba $package_dir/SPECS/iic-zon-ha-$version-1.spec \
	&>  zon-ha-rpmbuild.log && echo "Ok" || echo "FAILED"
}

function package_zon_ha
{
    if check_product $package_dir/RPMS/$package_arch/iic-zon-ha-$version-1.$package_arch.rpm ;  then
	cp $package_dir/RPMS/$package_arch/iic-zon-ha-$version-1.$package_arch.rpm $1

	cp $heartbeat_rpm \
	   $heartbeat_stonith_rpm \
	   $heartbeat_pils_rpm \
	   $1
    fi
}

function make_extapi_tarball
{
    tag=$1
    version=$2

    rm -rf iic-extapi-$version
    mkdir -p iic-extapi-$version

    ( cd iic-extapi-$version
	export_module $tag iic && echo "Ok" || echo "FAILED" )

    tar zcf $package_dir/SOURCES/iic-extapi-$version.tar.gz \
	iic-extapi-$version

    rm -rf iic-extapi-$version
}

function make_extapi_specfile
{
     sed \
	-e "s/@@@VERSION@@@/$version/g" \
	-e "s/@@@HTTPD_PKGNAME@@@/$httpd_pkgname/" \
	iic-extapi.spec \
    > $package_dir/SPECS/iic-extapi-$version-1.spec
}

function make_extapi_rpm
{
    echo -n "Building iic-extapi rpm..."

    rpmbuild -ba $package_dir/SPECS/iic-extapi-$version-1.spec \
	&> extapi-rpmbuild.log && echo "Ok" || echo "FAILED"
}

function package_zon_web_services
{
    if check_product $package_dir/RPMS/$package_arch/iic-web-base-$version-1.$package_arch.rpm && \
	check_product $package_dir/RPMS/$package_arch/iic-extapi-$version-1.$package_arch.rpm && \
	check_product $package_dir/RPMS/$package_arch/iic-console-$version-1.$package_arch.rpm && \
	check_product $package_dir/RPMS/$package_arch/iic-meeting-invite-$version-1.$package_arch.rpm; then \

	cp  $package_dir/RPMS/$package_arch/iic-web-base-$version-1.$package_arch.rpm \
	    $package_dir/RPMS/$package_arch/iic-extapi-$version-1.$package_arch.rpm \
	    $package_dir/RPMS/$package_arch/iic-console-$version-1.$package_arch.rpm \
	    $package_dir/RPMS/$package_arch/iic-meeting-invite-$version-1.$package_arch.rpm \
	    $1
    fi
	
}

function build_web_packages
{
    tag=$1
    version=$2


    rpm -Uhv \
	$package_dir/RPMS/$package_arch/iic-xmlrpc-$version-1.$package_arch.rpm 

    [ ! -e skip-web-base ] && \
	needs_update $package_dir/RPMS/$package_arch/iic-web-base-$version-1.$package_arch.rpm && \
	make_web_base_tarball $tag $version && \
	make_web_base_specfile $version && \
	make_web_base_rpm $version

    [ ! -e skip-extapi ] && \
	needs_update $package_dir/RPMS/$package_arch/iic-extapi-$version-1.$package_arch.rpm && \
	make_extapi_tarball $tag $version && \
	make_extapi_specfile $version && \
	make_extapi_rpm $version

    [ ! -e skip-meeting-invite ] && \
	needs_update $package_dir/RPMS/$package_arch/iic-meeting-invite-$version-1.$package_arch.rpm && \
	make_meeting_invite_tarball $tag $version && \
	make_meeting_invite_specfile $version && \
	make_meeting_invite_rpm $version

    [ ! -e skip-console ] && \
	needs_update $package_dir/RPMS/$package_arch/iic-console-$version-1.$package_arch.rpm && \
	make_console_tarball $tag $version && \
	make_console_specfile $version && \
	make_console_rpm $version

    rpm -e iic-xmlrpc

    # Get rid of any post-install created files
    # MATT-TODO:  Figure out what files wind up getting sprinkled about on the build system and delete them
}

function make_services_tarball
{
    tag=$1
    version=$2

    rm -rf iic-services-$version
    mkdir -p iic-services-$version

    ( cd iic-services-$version
	export_module $tag iic && echo "Ok" || echo "FAILED"
	export_module $tag jabberd && echo "Ok" || echo "FAILED"
	export_module $tag jadc2s && echo "Ok" || echo "FAILED"
	export_module $tag vnc2mov && echo "Ok" || echo "FAILED"
    )

    tar zcf $package_dir/SOURCES/iic-services-$version.tar.gz \
	iic-services-$version

    rm -rf iic-services-$version
}

function make_services_specfile
{
    sed \
	-e "s/@@@VERSION@@@/$version/g" \
	iic-services.spec > $package_dir/SPECS/iic-services-$version-1.spec
}

function make_services_rpms
{
    echo -n 'Building iic-services-* rpms...'

	# use RPM macros instead; need to get OSS flag to iic/services/configure
	if [ "$oss_build" != "y" ]; then
		export BUILD_OSS=n
	fi

    rpmbuild -ba $package_dir/SPECS/iic-services-$version-1.spec \
	&> services-rpmbuild.log && echo "Ok" || echo "FAILED"
}

function make_services_stubbridge_tarball
{
    tag=$1
    version=$2

    rm -rf iic-services-stubbridge-$version
    mkdir -p iic-services-stubbridge-$version

    ( cd iic-services-stubbridge-$version
	export_module $tag iic && echo "Ok" || echo "FAILED"
    )

    tar zcf $package_dir/SOURCES/iic-services-stubbridge-$version.tar.gz \
	iic-services-stubbridge-$version

    rm -rf iic-services-stubbridge-$version
}

function make_services_stubbridge_specfile
{
    sed \
	-e "s/@@@VERSION@@@/$version/g" \
	iic-services-stubbridge.spec > $package_dir/SPECS/iic-services-stubbridge-$version-1.spec
}

function make_services_stubbridge_rpm
{
    echo -n 'Building iic-services-stubbridge rpm...'

    rpmbuild -ba $package_dir/SPECS/iic-services-stubbridge-$version-1.spec \
	&> services-stubbridge-rpmbuild.log && echo "Ok" || echo "FAILED"
}

function make_services_nmsbridge_tarball
{
    tag=$1
    version=$2

    rm -rf iic-services-nmsbridge-$version
    mkdir -p iic-services-nmsbridge-$version

    ( cd iic-services-nmsbridge-$version
	export_module $tag iic && echo "Ok" || echo "FAILED"
	export_module $tag iaxclient && echo "Ok" || echo "FAILED"
    )

    tar zcf $package_dir/SOURCES/iic-services-nmsbridge-$version.tar.gz \
	iic-services-nmsbridge-$version

    rm -rf iic-services-nmsbridge-$version
}

function make_services_nmsbridge_specfile
{
    sed \
	-e "s/@@@VERSION@@@/$version/g" \
	iic-services-nmsbridge.spec > $package_dir/SPECS/iic-services-nmsbridge-$version-1.spec
}

function make_services_nmsbridge_rpm
{
    echo -n 'Building iic-services-nmsbridge rpm...'

	# use RPM macros instead; need to get OSS flag to iic/services/configure
	if [ "$oss_build" != "y" ]; then
		export BUILD_OSS=n
	fi

    rpmbuild -ba $package_dir/SPECS/iic-services-nmsbridge-$version-1.spec \
	&> services-nmsbridge-rpmbuild.log && echo "Ok" || echo "FAILED"
}

function make_services_appconfbridge_tarball
{
    tag=$1
    version=$2

    rm -rf iic-services-appconfbridge-$version
    mkdir -p iic-services-appconfbridge-$version

    ( cd iic-services-appconfbridge-$version
	export_module $tag iic && echo "Ok" || echo "FAILED"
	export_module $tag astxx && echo "Ok" || echo "FAILED"
    )

    tar zcf $package_dir/SOURCES/iic-services-appconfbridge-$version.tar.gz \
	iic-services-appconfbridge-$version

    rm -rf iic-services-appconfbridge-$version

}

function make_services_appconfbridge_specfile
{
    sed \
	-e "s/@@@VERSION@@@/$version/g" \
	iic-services-appconfbridge.spec > $package_dir/SPECS/iic-services-appconfbridge-$version-1.spec

}

function make_services_appconfbridge_rpm
{
    echo -n 'Building iic-services-appconfbridge rpm...'

    rpmbuild -ba $package_dir/SPECS/iic-services-appconfbridge-$version-1.spec \
	&> services-appconfbridge-rpmbuild.log && echo "Ok" || echo "FAILED"
}

function package_zon_services
{
    if check_product $package_dir/RPMS/$package_arch/iic-services-$version-1.$package_arch.rpm && \
	check_product $package_dir/RPMS/$package_arch/iic-services-db-$version-1.$package_arch.rpm && \
	check_product $package_dir/RPMS/$package_arch/iic-services-stubbridge-$version-1.$package_arch.rpm && \
	check_product $package_dir/RPMS/$package_arch/iic-services-appconfbridge-$version-1.$package_arch.rpm && \
	check_product $package_dir/RPMS/$package_arch/iic-services-appshare-$version-1.$package_arch.rpm && \
	check_product $package_dir/RPMS/$package_arch/iic-services-xmlrouter-$version-1.$package_arch.rpm && \
	check_product $package_dir/RPMS/$package_arch/iic-services-xmlrouter-services-$version-1.$package_arch.rpm && \
	check_product $package_dir/RPMS/$package_arch/iic-console-$version-1.$package_arch.rpm && \
	check_product $package_dir/RPMS/$package_arch/iic-services-sharemgr-$version-1.$package_arch.rpm 
	then

	cp  $package_dir/RPMS/$package_arch/iic-services-$version-1.$package_arch.rpm \
	    $package_dir/RPMS/$package_arch/iic-services-db-$version-1.$package_arch.rpm \
	    $package_dir/RPMS/$package_arch/iic-services-appshare-$version-1.$package_arch.rpm \
	    $package_dir/RPMS/$package_arch/iic-services-stubbridge-$version-1.$package_arch.rpm \
	    $package_dir/RPMS/$package_arch/iic-services-appconfbridge-$version-1.$package_arch.rpm \
	    $package_dir/RPMS/$package_arch/iic-services-xmlrouter-$version-1.$package_arch.rpm \
	    $package_dir/RPMS/$package_arch/iic-services-xmlrouter-services-$version-1.$package_arch.rpm \
	    $package_dir/RPMS/$package_arch/iic-console-$version-1.$package_arch.rpm \
	    $package_dir/RPMS/$package_arch/iic-services-sharemgr-$version-1.$package_arch.rpm \
	    $1

	if [ "$oss_build" != "y" ]; then
	    [ -e $package_dir/RPMS/$package_arch/iic-services-nmsbridge-$version-1.$package_arch.rpm ] && \
	        check_product $package_dir/RPMS/$package_arch/iic-services-nmsbridge-$version-1.$package_arch.rpm && \
	        cp $package_dir/RPMS/$package_arch/iic-services-nmsbridge-$version-1.$package_arch.rpm $1 ||
	    echo "WARNING: didn't package the NMS bridge"

	fi

	[[ `uname -r` < 2.6 ]] && cp $neon_rpm $1

	else
		echo "*** Package error: RPMs missing"
    fi
}

function build_services_package
{
    tag=$1
    version=$2

    [ ! -e skip-services ] && \
	needs_update $package_dir/RPMS/$package_arch/iic-services-$version-1.$package_arch.rpm && \
	make_services_tarball $tag $version && \
	make_services_specfile $version && \
	( cd $package_dir/RPMS/$package_arch ; \
		rpm -Uhv iic-json-$version-1.$package_arch.rpm iic-xmlrpc-$version-1.$package_arch.rpm \
		iic-mailer-$version-1.$package_arch.rpm iic-libical-$version-1.$package_arch.rpm \
		iic-x264-$version-1.$package_arch.rpm iic-ffmpeg-$version-1.$package_arch.rpm ) && \
	make_services_rpms $version && \
	rpm -e iic-json iic-xmlrpc iic-mailer iic-libical iic-x264 iic-ffmpeg
}

function build_services_stubbridge_package
{
    tag=$1
    version=$2

    [ ! -e skip-services-stubbridge ] && \
	needs_update $package_dir/RPMS/$package_arch/iic-services-stubbridge-$version-1.$package_arch.rpm && \
	make_services_stubbridge_tarball $tag $version && \
	make_services_stubbridge_specfile $version && \
	rpm -Uhv $package_dir/RPMS/$package_arch/iic-json-$version-1.$package_arch.rpm \
	$package_dir/RPMS/$package_arch/iic-xmlrpc-$version-1.$package_arch.rpm && \
	make_services_stubbridge_rpm $version && \
	rpm -e iic-json iic-xmlrpc
}

function build_services_nmsbridge_package
{
    tag=$1
    version=$2

    [ ! -e skip-services-nmsbridge ] && \
	needs_update $package_dir/RPMS/$package_arch/iic-services-nmsbridge-$version-1.$package_arch.rpm && \
	make_services_nmsbridge_tarball $tag $version && \
	make_services_nmsbridge_specfile $version && \
	rpm -Uhv $package_dir/RPMS/$package_arch/iic-json-$version-1.$package_arch.rpm \
	$package_dir/RPMS/$package_arch/iic-xmlrpc-$version-1.$package_arch.rpm && \
	make_services_nmsbridge_rpm $version && \
	rpm -e iic-json iic-xmlrpc
}

function build_services_appconfbridge_package
{
    tag=$1
    version=$2

    [ ! -e skip-services-appconfbridge ] && \
	needs_update $package_dir/RPMS/$package_arch/iic-services-appconfbridge-$version-1.$package_arch.rpm && \
	make_services_appconfbridge_tarball $tag $version && \
	make_services_appconfbridge_specfile $version && \
	rpm -Uhv $package_dir/RPMS/$package_arch/iic-json-$version-1.$package_arch.rpm \
	$package_dir/RPMS/$package_arch/iic-xmlrpc-$version-1.$package_arch.rpm && \
	make_services_appconfbridge_rpm $version && \
	rpm -e iic-json iic-xmlrpc
}

function make_extbridge_devel_tarball
{
    tag=$1
    version=$2

    rm -rf iic-extbridge-devel-$version
    mkdir -p iic-extbridge-devel-$version

    ( cd iic-extbridge-devel-$version
	export_module $tag iic && echo "Ok" || echo "FAILED" )

    tar zcf $package_dir/SOURCES/iic-extbridge-devel-$version.tar.gz \
	iic-extbridge-devel-$version

    rm -rf iic-extbridge-devel-$version
}

function make_extbridge_devel_specfile
{
    sed \
	-e "s/@@@VERSION@@@/$version/g" \
	iic-extbridge-devel.spec > $package_dir/SPECS/iic-extbridge-devel-$version-1.spec
}

function make_extbridge_devel_rpm
{
    echo -n "Building iic-extbridge-devel rpm..."
    rpmbuild -ba $package_dir/SPECS/iic-extbridge-devel-$version-1.spec \
	&>  extbridge-devel-rpmbuild.log && echo "Ok" || echo "FAILED"
}

function build_extbridge_devel_package
{
    tag=$1
    version=$2

    [ ! -e skip-extbridge-devel ] && \
	needs_update $package_dir/RPMS/$package_arch/iic-extbridge-devel-$version-1.$package_arch.rpm && \
	make_extbridge_devel_tarball $tag $version && \
	make_extbridge_devel_specfile $version && \
	rpm -Uhv $package_dir/RPMS/$package_arch/iic-json-$version-1.$package_arch.rpm \
	$package_dir/RPMS/$package_arch/iic-xmlrpc-$version-1.$package_arch.rpm && \
	make_extbridge_devel_rpm $version && \
	rpm -e iic-json iic-xmlrpc
}


function make_asterisk_tarball
{
    tag=$1
    version=$2

    rm -rf iic-asterisk-$version
    mkdir -p iic-asterisk-$version

    ( cd iic-asterisk-$version
	export_module $tag asterisk && echo "Ok" || echo "FAILED" )

    ( cd iic-asterisk-$version
	export_module $tag appconference && echo "Ok" || echo "FAILED" )

    tar zcf $package_dir/SOURCES/iic-asterisk-$version.tar.gz \
	iic-asterisk-$version

    rm -rf iic-asterisk-$version
}

function make_asterisk_specfile
{
    sed \
	-e "s/@@@VERSION@@@/$version/g" \
	iic-asterisk.spec > $package_dir/SPECS/iic-asterisk-$version-1.spec
}

function make_asterisk_rpm
{
    echo -n "Building iic-asterisk rpm..."
    rpmbuild -ba $package_dir/SPECS/iic-asterisk-$version-1.spec \
	&>  asterisk-rpmbuild.log && echo "Ok" || echo "FAILED"
}

function build_asterisk_package
{
    tag=$1
    version=$2

    [ ! -e skip-asterisk ] && \
	needs_update $package_dir/RPMS/$package_arch/iic-asterisk-$version-1.$package_arch.rpm && \
	make_asterisk_tarball $tag $version && \
	make_asterisk_specfile $version && \
	make_asterisk_rpm $version
}

function package_asterisk
{
    if check_product $package_dir/RPMS/$package_arch/iic-asterisk-$version-1.$package_arch.rpm ;  then
      mkdir $1/asterisk
	  cp $package_dir/RPMS/$package_arch/iic-asterisk-$version-1.$package_arch.rpm $1/asterisk
    fi
}

function package_release
{
    mkdir -p $1
    local config=$2

    package_json $1
    package_xmlrpc $1
    package_mailer $1
    package_libical $1
    package_x264 $1
    package_ffmpeg $1
    package_zon_services $1
    package_zon_web_services $1
    package_asterisk $1
    # package_zon_ha $1
    cp install.sh $1
    cp install-bridge.sh $1
    cp cleanup.sh $1
    chmod +x $1/install.sh
    chmod +x $1/install-bridge.sh
    chmod +x $1/cleanup.sh

    tar zcf $1.tar.gz $1

    rm -rf $1
}

