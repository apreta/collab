Summary: iMeeting base config for all web services
Name: iic-web-base
Version: @@@VERSION@@@
Release: 1
License: Sitescape
Group: Applications/Communication
URL: http://www.sitescape.com
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Requires: @@@HTTPD_PKGNAME@@@

%description
This package contains the base configuration on which to add additional web services

%prep
%setup -q

%build

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/var/iic/htdocs/imidio
cp -r iic/webcontent/imidio/images     $RPM_BUILD_ROOT/var/iic/htdocs/imidio
cp -r iic/webcontent/imidio/downloads  $RPM_BUILD_ROOT/var/iic/htdocs/imidio
cp -r iic/webcontent/imidio/css        $RPM_BUILD_ROOT/var/iic/htdocs/imidio
cp -r iic/webcontent/imidio/js         $RPM_BUILD_ROOT/var/iic/htdocs/imidio

if [ -d docs/beta/manual ]; then
    cp -r 'docs/beta/manual/HTML'      $RPM_BUILD_ROOT/var/iic/htdocs/imidio/client-manual
    cp -r 'docs/beta/manual/pdfs'      $RPM_BUILD_ROOT/var/iic/htdocs/imidio/quickstart
else
    cp -r 'docs/open/HTML'          $RPM_BUILD_ROOT/var/iic/htdocs/imidio/client-manual
    cp -r 'docs/open/pdfs'          $RPM_BUILD_ROOT/var/iic/htdocs/imidio/quickstart
fi

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc
/var/iic/htdocs/imidio

%post

%postun

%changelog
* Thu Oct  7 2004 root <root@dbs.homedns.org> 1.0.0-1
- Initial build.



