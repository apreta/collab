#!/bin/bash

vservers=/vservers

if [ $# -ne 2 ]; then
   echo "Usage: $0 <servername> <user>"
   exit -1
fi

if [ ! -d $vservers ]; then
   mkdir $vservers
fi

server=$vservers/$1

if [ -e $server ]; then
   echo "$server exists"
   exit -1
fi

echo "Copying files to new file system."
mkdir $server
cp -ax /sbin /bin /etc /usr  /lib /dev $server
cp -ax /var/lib/rpm $server/var/lib/
if [ -d /opt/gnome ]; then
    mkdir -p $server/opt/gnome/lib
    mkdir -p $server/opt/gnome/include
    cp -ax /opt/gnome/lib $server/opt/gnome/
    cp -ax /opt/gnome/include $server/opt/gnome/
fi
if [ -d /opt/nms ]; then
    mkdir -p /opt/nms
    cp -ax /opt/nms $server/opt/
fi
mkdir $server/root
mkdir $server/tmp
mkdir -p $server/home/$2
chown $2 $server/home/$2
