#!/bin/sh

make=0

if uname | grep -i "MINGW32"; then
    prefix=/gnutls
elif uname | grep -i "DARWIN"; then
    echo "Yay! A Mac"
    prefix=/opt/conferencing
    SUDO=
    # glibtool doesn't pass -headerpad parameter through on link, so add
    # to gcc command itself to make sure it takes affect
    old_CC="$CC"
    if [ -z $CC ]; then
    	CC=gcc
    fi
    export CC="$CC -headerpad_max_install_names"
#	flags are set up in build-client.sh
#    arch_flags="-arch i386"
#    gcc_flags="-mmacosx-version-min=10.5"
#    export CFLAGS="$arch_flags $gcc_flags"
#    export CXXFLAGS="$arch_flags $gcc_flags"
#    export CPPFLAGS="$arch_flags $gcc_flags"
#    export LDFLAGS="$arch_flags $gcc_flags"
#    export OBJCFLAGS="$arch_flags"
#    export OBJCXXFLAGS="$arch_flags"
else
    echo "Linux!"
    SUDO=sudo
    prefix=/opt/conferencing/client
fi

until [ -z "$1" ]
  do
    case $1 in
     --make)  make=1 ;;
     *) echo "Unknown option $1"; exit -1 ;;
    esac
    shift
  done  

if ! echo $PATH | grep "$prefix"; then
    echo "Adding bin directory to path"
    export PATH=$prefix/bin:$PATH
fi

echo "Building libtasn1"
cd libtasn1-2.7
if (( make == 0 )); then
	glibtoolize --force
    ./configure --prefix=$prefix
fi
if make && $SUDO make install; then
  echo "Ok"
else
  echo "Failed"; exit -1
fi

echo "Building libpgp-error"
# Editted w32-gettext.c and remove duplicate case statements (mismatch in a few sub-language defs, don't think they will effect us)
# Removed tests subdir from build
cd ../libgpg-error-1.9
if (( make == 0 )); then
    ./configure --prefix=$prefix
fi
if make && $SUDO make install; then
  echo "Ok"
else
  echo "Failed"; exit -1
fi

echo "Building libgcrypt"
# Commented out sys/times.h header
cd ../libgcrypt-1.4.6
if (( make == 0 )); then
#    autoreconf --force --install
    ./configure --prefix=$prefix --disable-asm
    make clean
fi
if make && $SUDO make install; then
  echo "Ok"
else
  echo "Failed"; exit -1
fi

echo "Building gnutls"
# Editted gl/gai_strerror.c and #if out the body; its not needed
cd ../gnutls-2.8.6
if (( make == 0 )); then
    ./configure --prefix=$prefix --with-included-opencdk --disable-cxx --with-libtasn1-prefix=$prefix
    make clean
fi
if make && $SUDO make install; then
  echo "Ok"
else
  echo "Failed"; exit -1
fi

export CC="$old_CC"