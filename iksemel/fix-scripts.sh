#!/bin/sh

files="build.sh configure config.guess config.sub missing install-sh"

for file in $files; do
	chmod +x $file
done
