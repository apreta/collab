/* iksemel (XML parser for Jabber)
** Copyright (C) 2000-2004 Gurer Ozen <madcat@e-kolay.net>
** This code is free software; you can redistribute it and/or
** modify it under the terms of GNU Lesser General Public License.
*/

#include "common.h"
#include "iksemel.h"

#include <netlib/log.h>

#ifdef HAVE_GNUTLS
#include <gnutls/gnutls.h>
//#include <gnutls/extra.h>
#include <gnutls/x509.h>
#include <gcrypt.h>
#endif

#ifdef USE_NETLIB
#include <netlib/net.h>
#endif

#define SF_FOREIGN 1
#define SF_TRY_SECURE 2
#define SF_SECURE 4

struct stream_data {
	iksparser *prs;
	ikstack *s;
	ikstransport *trans;
	char *name_space;
	void *user_data;
	const char *server;
	iksStreamHook *streamHook;
	iksLogHook *logHook;
	iks *current;
	char *buf;
	int sock;
	int state;
	unsigned int flags;
	char *auth_username;
	char *auth_pass;
	int read_timeout;
    int data_sent;
#ifdef HAVE_GNUTLS
	gnutls_session sess;
	gnutls_certificate_credentials cred;
#endif
};

#ifdef HAVE_GNUTLS

#ifndef MAX_PATH
#define MAX_PATH 1024
#endif

static int enable_security = 0;
static char x509_cafile[MAX_PATH] = "";

static char gNewCertBuf[ 6 * MAX_PATH ] = "";

static gnutls_certificate_credentials xcred;
static int x509ctype = GNUTLS_X509_FMT_PEM;;

static int g_global_tls_inited = 0;

#define PRI_MAX 16
#define SU(x) (x!=NULL?x:"Unknown")

int protocol_priority[PRI_MAX] =
    { GNUTLS_TLS1_1, GNUTLS_TLS1_0, GNUTLS_SSL3, 0 };

int kx_priority[PRI_MAX] =
    { GNUTLS_KX_DHE_RSA, GNUTLS_KX_DHE_DSS, GNUTLS_KX_RSA,
    GNUTLS_KX_SRP_RSA, GNUTLS_KX_SRP_DSS, GNUTLS_KX_SRP,
    /* Do not use anonymous authentication, unless you know what that means */
    GNUTLS_KX_RSA_EXPORT, GNUTLS_KX_ANON_DH, 0
};

int cipher_priority[PRI_MAX] =
    { GNUTLS_CIPHER_AES_256_CBC, GNUTLS_CIPHER_AES_128_CBC,
    GNUTLS_CIPHER_3DES_CBC, GNUTLS_CIPHER_ARCFOUR_128,
    GNUTLS_CIPHER_ARCFOUR_40, 0
};

int comp_priority[PRI_MAX] = { GNUTLS_COMP_ZLIB, GNUTLS_COMP_NULL, 0 };

int mac_priority[PRI_MAX] =
    { GNUTLS_MAC_SHA1, GNUTLS_MAC_MD5, GNUTLS_MAC_RMD160, 0 };

int cert_type_priority[PRI_MAX] =
    { GNUTLS_CRT_X509, 0 };

int lock_init (void **priv);
int lock_destroy (void **lock);
int lock_lock (void **lock);
int lock_unlock (void **lock);


struct gcry_thread_cbs gcry_threads_apr = { 0,
  NULL, lock_init, lock_destroy,
  lock_lock, lock_unlock };


static void addToNewCert( char *ptag, char *pdata )
{
    strcat( gNewCertBuf, "<" );
    strcat( gNewCertBuf, ptag );
    strcat( gNewCertBuf, ">" );
    strcat( gNewCertBuf, pdata );
    strcat( gNewCertBuf, "</" );
    strcat( gNewCertBuf, ptag );
    strcat( gNewCertBuf, ">\n" );
}


int iks_get_new_cert_len( )
{
    return strlen( gNewCertBuf );
}


int iks_get_new_cert( int bufLen, char *buf )
{
    int iLen = strlen( gNewCertBuf );
    if( bufLen < (iLen + 1) )
        return 0;

    buf[0] = 0;
    strcpy( buf, gNewCertBuf );
    return iLen;
}


/* Hex encodes the given data.
 */
const char *raw_to_string(const unsigned char *raw, size_t raw_size)
{
	static char buf[1024];
	size_t i;
	if (raw_size == 0)
		return NULL;

	if (raw_size * 3 + 1 >= sizeof(buf))
		return NULL;

	for (i = 0; i < raw_size; i++) {
		sprintf(&(buf[i * 3]), "%02X ", raw[i]);
	}
	buf[sizeof(buf) - 1] = '\0';

	return buf;
}

static const char *format_ctime(const time_t * tv)
{
	static char buf[256];
	struct tm *tp;

	if (((tp = localtime(tv)) == NULL) || (!strftime(buf, sizeof buf, "%a %b %e %H:%M:%S %Z %Y\n", tp)))
		strcpy(buf, "Unknown");	/* make sure buf text isn't garbage */

	return buf;

}

static void tls_log_func(int level, const char *str)
{
    debugf("|<%d>| %s", level, str);
}

/* A callback function to be used at the certificate selection time.
 */
static int tls_cert_callback(gnutls_session session,
			 const gnutls_datum * req_ca_rdn, int nreqs,
			 const gnutls_pk_algorithm * sign_algos,
			 int sign_algos_length, gnutls_retr_st * st)
{
    char issuer_dn[256];
    int i, ret;
    size_t len;

    /* Print the server's trusted CAs
     */
    if (nreqs > 0)
    {
      debugf("- Server's trusted authorities:");
    }
    else
    {
      debugf("- Server did not send us any trusted authorities names.");
    }

    /* print the names (if any) */
    for (i = 0; i < nreqs; i++) {
      len = sizeof(issuer_dn);
      ret = gnutls_x509_rdn_get(&req_ca_rdn[i], issuer_dn, &len);
      if (ret >= 0) {
        debugf("   [%d]: ", i);
        debugf("%s", issuer_dn);
      }
   }
   return 0;
}


/* initialize security stuff (certificate, etc.)
 */
static void init_tls_globals(void)
{
    int ret;
    int log = 2;

    if (g_global_tls_inited)
        return;

    gcry_control (GCRYCTL_SET_THREAD_CBS, &gcry_threads_apr);

    if ((ret = gnutls_global_init()) < 0) {
      errorf("global_init: %s", gnutls_strerror(ret));
      return;
    }
    debugf("SSL lib initialized");

    if (getenv("GNUTLS_DEBUG") != NULL)
        log = 9;

    gnutls_global_set_log_function(tls_log_func);
    gnutls_global_set_log_level(log);

/*    if ((ret = gnutls_global_init_extra()) < 0) {
      errorf("global_init_extra: %s", gnutls_strerror(ret));
      return;
    }
*/
    /* X509 stuff */
    if (gnutls_certificate_allocate_credentials(&xcred) < 0) {
      errorf("Certificate allocation memory error");
      return;
    }

    /* there are some CAs that have a v1 certificate *%&@#*%&
     */
    gnutls_certificate_set_verify_flags(xcred,
					GNUTLS_VERIFY_ALLOW_X509_V1_CA_CRT);

    if (strlen(x509_cafile) > 0)
    {
      ret =  gnutls_certificate_set_x509_trust_file(xcred,
          x509_cafile, (gnutls_x509_crt_fmt_t)x509ctype);

      if( ret < 0  ) {
        errorf("Error setting the x509 trust file");
		return;
      } else {
        debugf( "Processed %d CA certificate(s).", ret);
      }
    }

	g_global_tls_inited = 1;
}

/* initializes a gnutls_session with some defaults.
 */
static gnutls_session init_tls_session(const char *hostname)
{
    gnutls_session session;

	gnutls_init(&session, GNUTLS_CLIENT);

	gnutls_cipher_set_priority(session, cipher_priority);
	gnutls_compression_set_priority(session, comp_priority);
	gnutls_kx_set_priority(session, kx_priority);
	gnutls_protocol_set_priority(session, protocol_priority);
	gnutls_mac_set_priority(session, mac_priority);

	gnutls_dh_set_prime_bits(session, 512);

	gnutls_credentials_set(session, GNUTLS_CRD_CERTIFICATE, xcred);

	gnutls_certificate_client_set_retrieve_function(xcred, tls_cert_callback);
    return session;
}


BOOL isCertInFile( FILE *pf, char *inBuf )
{
    char    line[ MAX_PATH+1 ];
    char    *pBuf;

    pBuf = inBuf;
    while( TRUE )
    {
        while( fgets( line, MAX_PATH, pf ) )
        {
            int     iCmp    = strlen( line );
            if( iCmp  >  strlen( pBuf ) )
                break;
            if( strncmp( line, pBuf, iCmp )  !=  0 )
                break;
            pBuf += iCmp;
            if( *pBuf  ==  '\n' )
                pBuf++;
            if( *pBuf  ==  '\r' )
                pBuf++;
            if( *pBuf  ==  '\n' )
                pBuf++;
            if( strstr( line, "END CERTIFICATE" )  !=  0 )
            {
                return TRUE;
            }
        }
        if( feof( pf ) )
        {
            return FALSE;
        }
        // read past end of this cert
        while( strstr( line, "END CERTIFICATE" )  ==  0 )
        {
            if( !fgets( line, MAX_PATH, pf ) )
            {
                return FALSE;
            }
        }
        pBuf = inBuf;
    }

    // Shouldn't get here
    return FALSE;
}


BOOL cert_copy_found( gnutls_session session )
{
    gnutls_x509_crt crt;
    const gnutls_datum *cert_list;
    int cert_list_size = 0, ret;

    errorf("see if cert matches one manually downloaded by the user");

    cert_list = gnutls_certificate_get_peers(session, (unsigned int *)&cert_list_size);

    if( cert_list_size  ==  0 )
    {
        errorf("No certificates found!");
        return FALSE;
    }

    gnutls_x509_crt_init( &crt );
    ret = gnutls_x509_crt_import(crt, &cert_list[0], GNUTLS_X509_FMT_DER);
    if (ret < 0)
    {
        errorf("Decoding error: %s", gnutls_strerror(ret));
        return FALSE;
    }

    //  Get the cert from the server into a char buffer
    char buffer[5 * 1024];
    size_t size = sizeof( buffer );

    ret = gnutls_x509_crt_export( crt, GNUTLS_X509_FMT_PEM, buffer, &size );
    if( ret  <  0 )
    {
        return FALSE;
    }

    gnutls_x509_crt_deinit( crt );

    //  Open the .pem file and loop over its certs trying to match one to the server's
    FILE    *pf;
    BOOL    fRet;
    pf = fopen( x509_cafile, "r" );
    if( pf  !=  NULL )
    {
        fRet = isCertInFile( pf, buffer );
        fclose( pf );
        if( fRet )
            return TRUE;
    }


    return FALSE;
}


void print_x509_info(gnutls_session session, const char *hostname)
{
	gnutls_x509_crt crt;
	const gnutls_datum *cert_list;
	int cert_list_size = 0, ret;
	char digest[20];
	char serial[40];
	char dn[256];
	size_t dn_size;
	size_t digest_size = sizeof(digest);
	unsigned int j;
	size_t serial_size = sizeof(serial);
	const char *print;
	time_t expiret, activet;

	cert_list = gnutls_certificate_get_peers(session, (unsigned int *)&cert_list_size);

	if (cert_list_size == 0) {
		errorf("No certificates found!");
		return;
	}

	debugf(" - Got a certificate list of %d certificates.", cert_list_size);

	for (j = 0; j < (unsigned int) cert_list_size; j++) {

		gnutls_x509_crt_init(&crt);
		ret = gnutls_x509_crt_import(crt, &cert_list[j],
				   GNUTLS_X509_FMT_DER);
		if (ret < 0) {
			errorf("Decoding error: %s", gnutls_strerror(ret));
			return;
		}

		debugf(" - Certificate[%d] info:", j);

		{
			char buffer[5 * 1024];
			size_t size = sizeof(buffer);

			ret = gnutls_x509_crt_export(crt,
					   GNUTLS_X509_FMT_PEM, buffer, &size);
			if (ret < 0) {
				errorf("Encoding error: %s", gnutls_strerror(ret));
				return;
			}
            debugf("%s", buffer);
            gNewCertBuf[ 0 ] = 0;
            strcat( gNewCertBuf, "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n<newcert>\n" );

            addToNewCert( "certificate", buffer );
		}

		if (j == 0 && hostname != NULL) {	/* Check the hostname of the first certificate
						 * if it matches the name of the host we
						 * connected to.
						 */
			if (gnutls_x509_crt_check_hostname(crt, hostname) == 0) {
				errorf (" # The hostname in the certificate does NOT match '%s'.", hostname);
			} else {
				errorf (" # The hostname in the certificate matches '%s'.", hostname);
			}
		}


		expiret = gnutls_x509_crt_get_expiration_time(crt);
		activet = gnutls_x509_crt_get_activation_time(crt);

		debugf(" # valid since: %s", format_ctime(&activet));
		debugf(" # expires at: %s", format_ctime(&expiret));
        addToNewCert( "issued",  (char *) format_ctime(&activet) );
        addToNewCert( "expires", (char *) format_ctime(&expiret) );


		/* Print the serial number of the certificate.
		 */
		if (gnutls_x509_crt_get_serial(crt, serial, &serial_size) >= 0) {
			print = raw_to_string((const unsigned char *)serial, serial_size);
            if (print != NULL)
            {
				debugf(" # serial number: %s", print);
                addToNewCert( "serial", (char *) print );
            }
		}

		/* Print the fingerprint of the certificate */
		digest_size = sizeof(digest);
		if ((ret = gnutls_x509_crt_get_fingerprint(crt, GNUTLS_DIG_MD5, digest, &digest_size)) < 0) {
			errorf("Error in fingerprint calculation: %s", gnutls_strerror(ret));
		} else {
			print = raw_to_string((const unsigned char *)digest, digest_size);
            if (print != NULL)
            {
                debugf(" # fingerprint: %s", print);
                addToNewCert( "md5", (char *) print );
            }
		}


		dn_size = sizeof(dn);
		ret = gnutls_x509_crt_get_dn(crt, dn, &dn_size);
        if (ret >= 0)
        {
            debugf(" # Subject's DN: %s", dn);
            addToNewCert( "issto", (char *) dn );
        }

		dn_size = sizeof(dn);
		ret = gnutls_x509_crt_get_issuer_dn(crt, dn, &dn_size);
        if (ret >= 0)
        {
            debugf(" # Issuer's DN: %s", dn);
            addToNewCert( "issby", (char *) dn );
        }

        strcat( gNewCertBuf, "</newcert>\n" );

		gnutls_x509_crt_deinit(crt);
	}
}

void print_cert_info(gnutls_session session, const char *hostname)
{
	debugf("- Certificate type: ");
	switch (gnutls_certificate_type_get(session)) {
	case GNUTLS_CRT_X509:
		debugf("X.509");
		print_x509_info(session, hostname);
		break;
    default:
        debugf("Unsupported certicate type");
        break;
	}
}

int cert_verify(gnutls_session session)
{
	int status;
	status = gnutls_certificate_verify_peers(session);

	if (status == GNUTLS_E_NO_CERTIFICATE_FOUND) {
		errorf("- Peer did not send any certificate.");
		return IKS_NO_CERTIFICATE_FOUND;
	}
	if (status < 0) {
		errorf("- Could not verify certificate (err: %s)",
		   gnutls_strerror(status));
		return IKS_UNABLE_VERIFY_CERTIFICATE;
	}

	if (gnutls_certificate_type_get(session) == GNUTLS_CRT_X509) {
        if (status & GNUTLS_CERT_SIGNER_NOT_FOUND) {
            if( !cert_copy_found( session ) )
            {
                errorf("- Peer's certificate issuer is unknown");
                return IKS_CERT_SIGNER_NOT_FOUND;
            }
            else
                errorf("- Peer's certificate matches one manually downloaded by user");
        } else if (status & GNUTLS_CERT_INVALID) {
			errorf("- Peer's certificate is NOT trusted");
			return IKS_CERT_INVALID;
		} else {
			errorf("- Peer's certificate is trusted");
		}
	} else {
		if (status & GNUTLS_CERT_SIGNER_NOT_FOUND) {
			errorf("- Could not find a signer of the peer's key");
			return IKS_CERT_SIGNER_NOT_FOUND;
		}
		if (status & GNUTLS_CERT_INVALID) {
			errorf("- Peer's key is invalid");
			return IKS_CERT_INVALID;
		}
		else {
			errorf("- Peer's key is valid");
		}
	}
	return IKS_OK;
}

int print_info(gnutls_session session, const char *hostname)
{
	const char *tmp;
	gnutls_credentials_type cred;
	gnutls_kx_algorithm kx;


	kx = gnutls_kx_get(session);

	cred = gnutls_auth_get_type(session);
	if (cred == GNUTLS_CRD_CERTIFICATE)
	{
		char dns[256];
		size_t dns_size = sizeof(dns);
		unsigned int type;

		/* This fails in client side */
		if (gnutls_server_name_get(session, dns, &dns_size, &type, 0) == 0) {
			debugf("- Given server name[%d]: %s", type, dns);
		}

		print_cert_info(session, hostname);
	}

	tmp = SU(gnutls_protocol_get_name(gnutls_protocol_get_version(session)));
	debugf("- Version: %s", tmp);

	tmp = SU(gnutls_kx_get_name(kx));
	debugf("- Key Exchange: %s", tmp);

	tmp = SU(gnutls_cipher_get_name(gnutls_cipher_get(session)));
	debugf("- Cipher: %s", tmp);

	tmp = SU(gnutls_mac_get_name(gnutls_mac_get(session)));
	debugf("- MAC: %s", tmp);

	tmp = SU(gnutls_compression_get_name(gnutls_compression_get(session)));
	debugf("- Compression: %s", tmp);

	return 0;
}

static ssize_t
tls_push (iksparser *prs, const char *buffer, size_t len)
{
	struct stream_data *data = iks_user_data (prs);
    int     iRet    = -1;

#ifndef USE_NETLIB
    iRet = data->trans->send (data->sock, buffer, len);
    if( iRet )
        return -1;
#else  // ICE
    if(!data || !buffer)
        return(-1);
    if( net_checkwriteable(data->sock, IKS_DEFAULT_TIMEOUT/1000) > 0)
    {
        iRet = net_send(data->sock, (char*)buffer, len);
        if (iRet < 0)
        {
            debugf("net_send error: %d [%d]", iRet, _error);
            errno = EIO;
        }
        else
            data->data_sent += iRet;
    }
#endif
	return iRet;
}

static ssize_t
tls_pull (iksparser *prs, char *buffer, size_t len)
{
	struct stream_data *data = iks_user_data (prs);
	int iRet     = -1;

#ifndef USE_NETLIB
    iRet = data->trans->recv (data->sock, buffer, len, -1);
    if( iRet == -1 )
        return -1;
#else  // ICE
    if( (iRet = net_checkreadable(data->sock, data->read_timeout))  <  0 )
    {
        debugf("net_checkreadable error: %d [%d]", iRet, _error);
        errno = EIO;
        return -1;
    }
    else if (iRet == 1)
    {
        iRet = net_recv(data->sock, buffer, len);
        if (iRet < 0)
        {
            debugf("net_recv error: %d [%d]", iRet, _error);
            errno = EIO;
            return -1;
        }
    }
    else // iRet == 0, no data
    {
        errno = EAGAIN;
        return -1;
    }

#endif
	return iRet;
}

static int
handshake (struct stream_data *data)
{
	int ret;

    data->sess = init_tls_session(data->server);
    data->cred = xcred;

	gnutls_transport_set_push_function (data->sess, (gnutls_push_func) tls_push);
	gnutls_transport_set_pull_function (data->sess, (gnutls_pull_func) tls_pull);
	gnutls_transport_set_ptr (data->sess, data->prs);

	ret = gnutls_handshake (data->sess);
	if (ret != 0) {
        debugf("tls handshake error: %d [%d]", ret, _error);
		gnutls_deinit (data->sess);
//		gnutls_certificate_free_credentials (data->cred);
#ifdef USE_NETLIB
        // We seem to have connected and we may want to retry with SSL off
        if (data->data_sent > 0)
            net_set_retry_last();
#endif
		return IKS_NET_TLSFAIL;
	}

	data->flags &= (~SF_TRY_SECURE);
	data->flags |= SF_SECURE;

	print_info(data->sess, data->server);

	if ((ret=cert_verify(data->sess))!=IKS_OK) {
		return ret;
	}

	iks_send_header (data->prs, data->server);

	return IKS_OK;
}
#endif

static void
insert_attribs (iks *x, char **atts)
{
	if (atts) {
		int i = 0;
		while (atts[i]) {
			iks_insert_attrib (x, atts[i], atts[i+1]);
			i += 2;
		}
	}
}

#define CNONCE_LEN 4

static void
iks_sasl_challenge (struct stream_data *data, iks *challenge)
{
	char *b;
	iks *x;

	b = iks_cdata (iks_child (challenge));
	if (!b) return;

	b = iks_base64_decode (b);

	if (strstr (b, "rspauth")) {
		x = iks_new("response");
	} else {
		char *realm, *nonce, *charset = NULL, *algorithm = NULL;
		char *realm_end, *nonce_end, cnonce[CNONCE_LEN*8 + 1];
		unsigned char a1_h[16], a1[33], a2[33], response_value[33];
		char *response, *response_coded;
		iksmd5 *md5;
		int i;

		realm = strstr (b, "realm");
		if (realm) {
			realm += 7;
			do {
				realm_end = strchr(realm, '"');
			} while (*(realm_end - 1) == '\\');
		} else {
			realm_end = 0;
			realm = (char *) data->server;
		}
		nonce = strstr (b, "nonce");
		if (nonce) {
			nonce += 7;
			do {
				nonce_end = strchr (nonce, '"');
			} while (*(nonce - 1) == '\\');
		} else {
			iks_free (b);
			return;
		}
		charset = strstr (b, "charset");
		if (charset) charset += 8;
		algorithm = strstr (b, "algorithm");
		if (algorithm) algorithm += 10;
		if (realm_end) *realm_end = 0;
		*nonce_end = 0;

		for (i = 0; i < CNONCE_LEN; ++i)
			sprintf (cnonce + i*8, "%08x", rand());

		md5 = iks_md5_new();
		iks_md5_hash (md5, data->auth_username, iks_strlen (data->auth_username), 0);
		iks_md5_hash (md5, ":", 1, 0);
		iks_md5_hash (md5, realm, iks_strlen (realm), 0);
		iks_md5_hash (md5, ":", 1, 0);
		iks_md5_hash (md5, data->auth_pass, iks_strlen (data->auth_pass), 1);
		iks_md5_digest (md5, a1_h);
		iks_md5_reset (md5);
		iks_md5_hash (md5, a1_h, 16, 0);
		iks_md5_hash (md5, ":", 1, 0);
		iks_md5_hash (md5, nonce, iks_strlen (nonce), 0);
		iks_md5_hash (md5, ":", 1, 0);
		iks_md5_hash (md5, cnonce, iks_strlen (cnonce), 1);
		iks_md5_print (md5, a1);
		iks_md5_reset (md5);
		iks_md5_hash (md5, "AUTHENTICATE:xmpp/", 18, 0);
		iks_md5_hash (md5, data->server, iks_strlen (data->server), 1);
		iks_md5_print (md5, a2);
		iks_md5_reset (md5);
		iks_md5_hash (md5, a1, 32, 0);
		iks_md5_hash (md5, ":", 1, 0);
		iks_md5_hash (md5, nonce, iks_strlen (nonce), 0);
		iks_md5_hash (md5, ":00000001:", 10, 0);
		iks_md5_hash (md5, cnonce, iks_strlen (cnonce), 0);
		iks_md5_hash (md5, ":auth:", 6, 0);
		iks_md5_hash (md5, a2, 32, 1);
		iks_md5_print (md5, response_value);
		iks_md5_delete (md5);

		i = iks_strlen (data->auth_username) + iks_strlen (realm) +
			iks_strlen (nonce) + iks_strlen (data->server) +
			CNONCE_LEN*8 + 136;
		response = iks_malloc (i);
		sprintf (response, "username=\"%s\",realm=\"%s\",nonce=\"%s\""
			",cnonce=\"%s\",nc=00000001,qop=auth,digest-uri=\""
			"xmpp/%s\",response=%s,charset=utf-8",
			data->auth_username, realm, nonce, cnonce,
			data->server, response_value);
		response_coded = iks_base64_encode (response, 0);

		x = iks_new ("response");
		iks_insert_cdata (x, response_coded, 0);

		iks_free (response);
		iks_free (response_coded);
	}
	iks_insert_attrib (x, "xmlns", IKS_NS_XMPP_SASL);
	iks_send (data->prs, x);
	iks_delete (x);
	iks_free (b);
}

static int
tagHook (struct stream_data *data, char *name, char **atts, int type)
{
	iks *x;
	int err;

	switch (type) {
		case IKS_OPEN:
		case IKS_SINGLE:
#ifdef HAVE_GNUTLS
			if (data->flags & SF_TRY_SECURE) {
				if (strcmp (name, "proceed") == 0) {
					err = handshake (data);
					return err;
				} else if (strcmp (name, "failure") == 0){
#ifdef USE_NETLIB
                    net_set_retry_last();
#endif
					return IKS_NET_TLSFAIL;
				}
			}
#endif
			if (data->current) {
				x = iks_insert (data->current, name);
				insert_attribs (x, atts);
			} else {
				x = iks_new (name);
				insert_attribs (x, atts);
				if (iks_strcmp (name, "stream:stream") == 0) {
					// ICE - Got stream header from remote, assume good connection.
#ifdef USE_NETLIB
                    net_set_connected();
#endif
					err = data->streamHook (data->user_data, IKS_NODE_START, x);
					if (err != IKS_OK) return err;
					break;
				}
			}
			data->current = x;
			if (IKS_OPEN == type) break;
		case IKS_CLOSE:
			x = data->current;
			if (NULL == x) {
				err = data->streamHook (data->user_data, IKS_NODE_STOP, NULL);
				if (err != IKS_OK) return err;
				break;
			}
			if (NULL == iks_parent (x)) {
				data->current = NULL;
				if (iks_strcmp (name, "challenge") == 0)
					iks_sasl_challenge(data, x);
				else if (iks_strcmp (name, "stream:error") == 0) {
					// ICE - Got stream header from remote, assume good connection.
#ifdef USE_NETLIB
                    net_set_connected();
#endif
					err = data->streamHook (data->user_data, IKS_NODE_ERROR, x);
					if (err != IKS_OK) return err;
				} else {
					err = data->streamHook (data->user_data, IKS_NODE_NORMAL, x);
					if (err != IKS_OK) return err;
				}
				break;
			}
			data->current = iks_parent (x);
	}
	return IKS_OK;
}

static int
cdataHook (struct stream_data *data, char *cdata, size_t len)
{
	if (data->current) iks_insert_cdata (data->current, cdata, len);
	return IKS_OK;
}

static void
deleteHook (struct stream_data *data)
{
#ifdef HAVE_GNUTLS
	if (data->flags & SF_SECURE) {
		gnutls_bye (data->sess, GNUTLS_SHUT_WR);
		gnutls_deinit (data->sess);
//		gnutls_certificate_free_credentials (data->cred);
	}
#endif
#ifndef USE_NETLIB
	if (data->trans) data->trans->close ((void*)data->sock);
	data->trans = NULL;
#else
	if(data->sock > 0) {
	    SOCKET s = data->sock;
		data->sock = 0;
        net_close(s, TRUE);
	}
#endif
	if (data->current) iks_delete (data->current);
	data->current = NULL;
	data->flags = 0;
}

iksparser *
iks_stream_new (char *name_space, void *user_data, iksStreamHook *streamHook)
{
	ikstack *s;
	struct stream_data *data;

	s = iks_stack_new (DEFAULT_STREAM_CHUNK_SIZE, 0);
	if (NULL == s) return NULL;
	data = iks_stack_alloc (s, sizeof (struct stream_data));
	memset (data, 0, sizeof (struct stream_data));
	data->s = s;
	data->prs = iks_sax_extend (s, data, (iksTagHook *)tagHook, (iksCDataHook *)cdataHook, (iksDeleteHook *)deleteHook);
	data->name_space = name_space;
	data->user_data = user_data;
	data->streamHook = streamHook;

	return data->prs;
}

void *
iks_stream_user_data (iksparser *prs)
{
	struct stream_data *data = iks_user_data (prs);

	return data->user_data;
}

void
iks_set_log_hook (iksparser *prs, iksLogHook *logHook)
{
	struct stream_data *data = iks_user_data (prs);

	data->logHook = logHook;
}

int
iks_connect_tcp (iksparser *prs, const char *server, int port)
{
#ifdef USE_DEFAULT_IO
	return iks_connect_with (prs, server, port, server, &iks_default_transport);
#else
	return IKS_NET_NOTSUPP;
#endif
}

int
iks_connect_via (iksparser *prs, const char *server, int port, const char *server_name)
{
#ifdef USE_DEFAULT_IO
	return iks_connect_with (prs, server, port, server_name, &iks_default_transport);
#else
	return IKS_NET_NOTSUPP;
#endif
}

int
iks_connect_with (iksparser *prs, const char *server, int port, const char *server_name, ikstransport *trans)
{
	struct stream_data *data = iks_user_data (prs);
	int ret;

	if (!trans->connect) return IKS_NET_NOTSUPP;

	if (!data->buf) {
		data->buf = iks_stack_alloc (data->s, NET_IO_BUF_SIZE);
		if (NULL == data->buf) return IKS_NOMEM;
	}

	ret = trans->connect (prs, (void*)&data->sock, server, port);
	if (ret) return ret;

	data->trans = trans;

	return iks_send_header (prs, server_name);
}

int
iks_connect_async (iksparser *prs, const char *server, int port, void *notify_data, iksAsyncNotify *notify_func)
{
#ifdef USE_DEFAULT_IO
	return iks_connect_async_with (prs, server, port, server, &iks_default_transport, notify_data, notify_func);
#else
	return IKS_NET_NOTSUPP;
#endif
}

int
iks_connect_async_with (iksparser *prs, const char *server, int port, const char *server_name, ikstransport *trans, void *notify_data, iksAsyncNotify *notify_func)
{
	struct stream_data *data = iks_user_data (prs);
	int ret;

	if (NULL == trans->connect_async)
		return IKS_NET_NOTSUPP;

	if (!data->buf) {
		data->buf = iks_stack_alloc (data->s, NET_IO_BUF_SIZE);
		if (NULL == data->buf) return IKS_NOMEM;
	}

	ret = trans->connect_async (prs, (void*)&data->sock, server, server_name, port, notify_data, notify_func);
	if (ret) return ret;

	data->trans = trans;
	data->server = server_name;

	return IKS_OK;
}

int
iks_connect_fd (iksparser *prs, int fd)
{
#ifdef USE_DEFAULT_IO
	struct stream_data *data = iks_user_data (prs);

	if (!data->buf) {
		data->buf = iks_stack_alloc (data->s, NET_IO_BUF_SIZE);
		if (NULL == data->buf) return IKS_NOMEM;
	}

	data->sock = fd;
	data->flags |= SF_FOREIGN;
	data->trans = &iks_default_transport;

	return IKS_OK;
#else
	return IKS_NET_NOTSUPP;
#endif
}

int
iks_fd (iksparser *prs)
{
	struct stream_data *data = iks_user_data (prs);

	return data->sock;
}

int
iks_recv (iksparser *prs, int timeout)
{
	struct stream_data *data = iks_user_data (prs);
	int len=0, ret;

	if(data->sock <= 0) return(0);

    data->read_timeout = timeout;

	while (1) {
#ifdef HAVE_GNUTLS
		if (data->flags & SF_SECURE) {
			len = gnutls_record_recv (data->sess, data->buf, NET_IO_BUF_SIZE - 1);
		} else
#endif
		{
#ifndef USE_NETLIB
			len = data->trans->recv (data->sock, data->buf, NET_IO_BUF_SIZE - 1, timeout);
#else	// ICE
			// Timeout for check calls below is in seconds.
			if (timeout > 1000) timeout = timeout / 1000;

			switch(data->state)
			{
			case 0:
				// Initial connect timeout is fixed
				if (net_checkwriteable(data->sock, IKS_DEFAULT_TIMEOUT/1000) > 0)
				{
					//net_set_nonblock((SOCKET)data->sock, 0);
					data->state = 1;

					if (enable_security && (data->flags & SF_SECURE)==0) {
                        ret = handshake(data);
						return ret;
					} else {
						iks_send_header (data->prs, data->server);
					}
				}
				if(data->state == 0) return IKS_TIMEOUT;	// timeout with no connection

			case 1:
				if ((len = net_checkreadable(data->sock, timeout)) > 0)
				{
					len = net_recv(data->sock, data->buf, 4095);
				}
			}
#endif
		}

        if (len == GNUTLS_E_AGAIN)
            return IKS_NO_DATA;
        if (len < 0)
            return IKS_NET_RWERR;
        if (len == 0)
            return IKS_NO_DATA;
		data->buf[len] = '\0';
		if (data->logHook) data->logHook (data->user_data, data->buf, len, 1);
		ret = iks_parse (prs, data->buf, len, 0);
		if (ret != IKS_OK) return ret;
#ifndef USE_NETLIB
		if (!data->trans) {
			/* stream hook called iks_disconnect */
			return IKS_NET_NOCONN;
		}
#else
        if (data->sock <= 0) {
			return IKS_NET_NOCONN;
		}
#endif
		timeout = 0;
	}
	return IKS_OK;
}

int
iks_send_header (iksparser *prs, const char *to)
{
	struct stream_data *data = iks_user_data (prs);
	char *msg;
	int len, err;

	len = 91 + strlen (data->name_space) + 6 + strlen (to) + 16 + 1;
	msg = iks_malloc (len);
	if (!msg) return IKS_NOMEM;
	sprintf (msg, "<?xml version='1.0'?>"
		"<stream:stream xmlns:stream='http://etherx.jabber.org/streams' xmlns='"
		"%s' to='%s' version='1.0'>", data->name_space, to);
	err = iks_send_raw (prs, msg);
	iks_free (msg);
	if (err) return err;
	data->server = to;
	return IKS_OK;
}

int
iks_send (iksparser *prs, iks *x)
{
	return iks_send_raw (prs, iks_string (iks_stack (x), x));
	/*char * xmlstr = iks_string (NULL, x);
	int ret = iks_send_raw (prs, xmlstr);
	iks_free(xmlstr);
	return ret;	*/
}

int
iks_send_raw (iksparser *prs, const char *xmlstr)
{
	struct stream_data *data = iks_user_data (prs);
	int ret;

	if(!data || !xmlstr) return IKS_OK;

#ifdef HAVE_GNUTLS
	if (data->flags & SF_SECURE) {
		/* Don't exceed gnutls maximum record size
		*/
		int len = strlen(xmlstr);
		int max = gnutls_record_get_max_size(data->sess);
		int i;

		for (i=0; i<len; )
		{
			int send = len - i;
			if (send > max)
				send = max;
			if (gnutls_record_send (data->sess, xmlstr+i, send) < 0) return IKS_NET_RWERR;
			i += send;
		}
	} else
#endif
	{
#ifndef USE_NETLIB
		ret = data->trans->send (data->sock, xmlstr, strlen (xmlstr));
		if (ret) return ret;
#else
		ret = net_send(data->sock, (char*)xmlstr, strlen(xmlstr));
		if (ret < 0) return IKS_NET_RWERR;
#endif
	}
	if (data->logHook) data->logHook (data->user_data, xmlstr, strlen (xmlstr), 0);
	return IKS_OK;
}

void
iks_disconnect (iksparser *prs)
{
	iks_parser_reset (prs);
	iks_interrupt(prs);
}

/*****  tls api  *****/

int
iks_has_tls (void)
{
#ifdef HAVE_GNUTLS
	return 1;
#else
	return 0;
#endif
}

int
iks_is_secure (iksparser *prs)
{
#ifdef HAVE_GNUTLS
	struct stream_data *data = iks_user_data (prs);

	return data->flags & SF_SECURE;
#else
	return 0;
#endif
}

int
iks_start_tls (iksparser *prs)
{
#ifdef HAVE_GNUTLS
	int ret;
	struct stream_data *data = iks_user_data (prs);

	ret = iks_send_raw (prs, "<starttls xmlns='urn:ietf:params:xml:ns:xmpp-tls'/>");
	if (ret) return ret;
	data->flags |= SF_TRY_SECURE;
	return IKS_OK;
#else
	return IKS_NET_NOTSUPP;
#endif
}

/*****  sasl  *****/

int
iks_start_sasl (iksparser *prs, enum ikssasltype type, char *username, char *pass)
{
	iks *x;

	x = iks_new ("auth");
	iks_insert_attrib (x, "xmlns", IKS_NS_XMPP_SASL);
	switch (type) {
		case IKS_SASL_PLAIN: {
			int len = iks_strlen (username) + iks_strlen (pass) + 2;
			char *s = iks_malloc (80+len);
			char *base64;

			iks_insert_attrib (x, "mechanism", "PLAIN");
			sprintf (s, "%c%s%c%s", 0, username, 0, pass);
			base64 = iks_base64_encode (s, len);
			iks_insert_cdata (x, base64, 0);
			iks_free (base64);
			iks_free (s);
			break;
		}
		case IKS_SASL_DIGEST_MD5: {
			struct stream_data *data = iks_user_data (prs);

			iks_insert_attrib (x, "mechanism", "DIGEST-MD5");
			data->auth_username = username;
			data->auth_pass = pass;
			break;
		}
		default:
			iks_delete (x);
			return IKS_NET_NOTSUPP;
	}
	iks_send (prs, x);
	iks_delete (x);
	return IKS_OK;
}

/***** ICE added APIs *****/

#ifdef USE_NETLIB
int
iks_connect_netlib (iksparser *prs, const char *server, const char *target_server, int port, int proxy_only)
{
    int native_err;
	struct stream_data *data = iks_user_data (prs);

	if(data->sock > 0) {
		iks_disconnect(prs);
		return IKS_FAILED;
	}
	if(!server) return(IKS_FAILED);

	if(target_server == NULL) target_server = server;
	if(port == 0) port = IKS_JABBER_PORT;

	data->server = strdup(server);
	if(!data->server) {
		iks_disconnect(prs);
		return IKS_FAILED;
	}
	data->state = 0;
	data->sock = net_socket();
	if(data->sock <= 0) {
		iks_disconnect(prs);
		return IKS_FAILED;
	}

	data->buf = iks_stack_alloc (data->s, NET_IO_BUF_SIZE);
	if(!data->buf) {
		iks_disconnect(prs);
		return IKS_NOMEM;
	}

#ifdef HAVE_GNUTLS
    /* initializes random number generator, takes a while */
    if (enable_security) {
        init_tls_globals();
    }
#endif

	net_set_server(DETECT_TYPE, target_server);

	native_err = net_hunt(data->sock, target_server, port, proxy_only);

	net_set_nonblock(data->sock, 1);	// restore it, network funcs may have changed it

	if (native_err != IERR_NET_SUCCESS) {
        iks_disconnect(prs);
	}
	if (native_err == IERR_NET_PROXYAUTHFAILED) {
		return IKS_PROXYAUTHFAILED;
	}
	if (native_err == IERR_NET_PROXYAUTHREQUIRED) {
		return IKS_PROXYAUTHREQUIRED;
	}

	return native_err==IERR_NET_SUCCESS?IKS_OK:IKS_FAILED;
}

void iks_interrupt(iksparser *prs)
{
	struct stream_data *data;
	if(!prs) return;

	data = iks_user_data (prs);
	if(!data) return;

	if(data->sock > 0) {
	    SOCKET s = data->sock;
		data->sock = 0;
        net_close(s, TRUE);
	}
}

void iks_set_security(int enable, const char *trust_ca)
{
  enable_security = enable;
  strcpy(x509_cafile, trust_ca);
}

void iks_set_option(const char *option, const char *value)
{
    if (strcasecmp(option, "http") == 0) {
        net_set_http(strcasecmp(value, "yes") == 0);
    } else if (strcasecmp(option, "path") == 0) {
        net_initialize(value);
    } else if (strcasecmp(option, "reset") == 0) {
        net_reset_hunt();
    } else if (strcasecmp(option, "primaryport") == 0) {
        net_enable_primary_port(strcasecmp(value, "yes") == 0);
    } else if (strcasecmp(option, "disablehunt") == 0) {
        net_enable_port_hunting(strcasecmp(value, "yes") != 0);
    }
}

const char *iks_get_option(const char *option)
{
    if (strcasecmp(option, "usinghttp") == 0) {
        return net_get_http() ? "yes" : "no";
    }
    return NULL;
}

// ToDo:  Set proxy server.
// Type = NONE, HTTP, SOCKS4, SOCKS5, DETECT
int iks_set_proxy(iksparser *prs, char *type, char *target_server, char *proxy_server, int port)
{
	proxy_types p_type = NO_PROXY;

	if (strcasecmp(type, "NONE") == 0)
	{
	}
	else if (strcasecmp(type, "HTTP") == 0)
	{
		p_type = HTTP_TYPE;
	}
	else if (strcasecmp(type, "SOCKS4") == 0)
	{
		p_type = SOCKS4_TYPE;
	}
	else if (strcasecmp(type, "SOCKS5") == 0)
	{
		p_type = SOCKS5_TYPE;
	}
	else if (strcasecmp(type, "DETECT") == 0)
	{
		p_type = DETECT_TYPE;
	}
	else
		return -1;

	net_set_server(p_type, target_server);

	return 0;
}

// Set proxy server authentication
// Type = NONE, BASIC, DIGEST, NTLM, NEGOTIATE (probably should use negotiate in most cases)
// Current user = use credentials of logged in user for NTLM authentication
int iks_set_auth(iksparser *prs, char *type, int current_user, char *user, char *password)
{
	auth_types a_type = AUTH_NONE;

	if (strcasecmp(type, "NONE") == 0)
	{
	}
	else if (strcasecmp(type, "BASIC") == 0)
	{
		a_type = AUTH_BASIC;
	}
	else if (strcasecmp(type, "DIGEST") == 0)
	{
		a_type = AUTH_DIGEST;
	}
	else if (strcasecmp(type, "NTLM") == 0)
	{
		a_type = AUTH_NTLM;
	}
	else if (strcasecmp(type, "NEGOTIATE") == 0)
	{
		a_type = AUTH_NEGOTIATE;
	}
	else
		return -1;

	net_set_auth(a_type, current_user, user, password);

	return 0;
}

#endif // USE_NETLIB
