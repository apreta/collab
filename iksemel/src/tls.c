
#define _NO_OLDNAMES
#include <apr.h>
#include <apr_thread_mutex.h>

static apr_pool_t *tls_pool = NULL;

int lock_init (void **lock)
{
    if (tls_pool == NULL)
        apr_pool_create(&tls_pool, NULL);

    apr_thread_mutex_t ** mutex = (apr_thread_mutex_t **) lock;
    return apr_thread_mutex_create(mutex, APR_THREAD_MUTEX_NESTED, tls_pool);
}

int lock_destroy (void **lock)
{
    apr_thread_mutex_t * mutex = (apr_thread_mutex_t *) *lock;
    apr_thread_mutex_destroy(mutex);
	return 0;
}

int lock_lock (void **lock)
{
    apr_thread_mutex_t * mutex = (apr_thread_mutex_t *) *lock;
    return apr_thread_mutex_lock(mutex);
}

int lock_unlock (void **lock)
{
    apr_thread_mutex_t * mutex = (apr_thread_mutex_t *) *lock;
    return apr_thread_mutex_unlock(mutex);
}
