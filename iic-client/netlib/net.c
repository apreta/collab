/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <errno.h>
#include <ctype.h>

#include <fcntl.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>

#ifndef WIN32

#include <unistd.h>
#include <netdb.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "proxy.h"

#else

#define WIN32_LEAN_AND_MEAN	1
#include <windows.h>
#include <winsock2.h>
#include <Winbase.h>
#include <Wininet.h>
#include "wininetdefs.h"
#include <io.h>

#endif

#include "net.h"
#include "net_internal.h"
#include "proxydetect.h"
#include "digcalc.h"
#include "httpauth.h"
#include "log.h"

#define HD_AUTHENTICATE         "Proxy-Authenticate:"
#define HD_LENGTH               "Content-Length:"
#define HD_CONNECTION           "Connection:"
#define HD_PROXYCONNECTION      "Proxy-Connection:"

char g_szProxyFileName[512] = "proxy.iic";

static char* ErrString[] =
{
	"Connected",	// starts from IERR_NET_SUCCESS
	"Malloc failed, maybe out of memory.",
	"Failed to open socket, or send/recv data.",
	"Server is not reachable.",
	"Connection to the server is blocked.",
	"Authentication with the server is failed.",
	"Server requires authentication.",
	"Authetication with the proxy server is failed.",
	"Proxy server requires authentication.",
	"Response from the server is not valid."
	"Failed to initialize security module.",
	"Access denied.",
	"Host not found.",
	"Connection timed out.",
};

int tcp_read_time_out = 10; //seconds, used when negotiating proxy tunnel
int tcp_connect_time_out = 20;

unsigned proxy_ip;

proxy_types proxy_type = NO_PROXY;
auth_types proxy_auth = AUTH_NONE;
BOOL net_http_mode = FALSE;
BOOL enable_nagle_algorithm = TRUE;
BOOL enable_primary_port = FALSE;
BOOL enable_port_hunting = TRUE;
BOOL main_thread = FALSE;
void (*polling_hook)() = NULL;

char target_server[256];
char proxy_user[256] = {0};
char proxy_password[256] = {0};
int proxy_current_user;		// non-zero to use current user to authenticate to proxy

typedef struct _PROXY {
	unsigned type;		// PROXY, SOCKS
	char* server;
	unsigned port;
	struct _PROXY* next;
} PROXY;

PROXY* proxy_list = NULL;


//
// Internal function decls
//
#ifdef USE_SSPI
static int NegotiateSSPIAuthentication(SOCKETINFO* socket_info, in_addr_t ip, in_port_t	port, PSTR pchUserName, PSTR pchPassword, PSTR pszPref);
#endif
static int timed_connect(SOCKET socket, const struct sockaddr *addr, int len);

BOOL is_proxy_configured();
int proxy_detect(char *server_name);
int net_hunt_disabled(SOCKET net_socket, const char *server, int default_port);


static void encode_base_64(char* src,char* dest,int max_len)
{
	static const char base64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	int n,l,i;
	l=strlen(src);
	max_len=(max_len-1)/4;
	for ( i=0;i<max_len;i++,src+=3,l-=3)
	{
		switch (l) {
		case 0:
			break;
		case 1:
			n=src[0] << 16;
			*dest++=base64[(n >> 18) & 077];
			*dest++=base64[(n >> 12) & 077];
			*dest++='=';
			*dest++='=';
			break;
		case 2:
			n=src[0] << 16 | src[1] << 8;
			*dest++=base64[(n >> 18) & 077];
			*dest++=base64[(n >> 12) & 077];
			*dest++=base64[(n >> 6) & 077];
			*dest++='=';
			break;
		default:
			n=src[0] << 16 | src[1] << 8 | src[2];
			*dest++=base64[(n >> 18) & 077];
			*dest++=base64[(n >> 12) & 077];
			*dest++=base64[(n >> 6) & 077];
			*dest++=base64[n & 077];
		}
		if (l<3) break;
	}
	*dest++=0;
}

static int write_n_bytes(int fd,char *buff,size_t size)
{
	int i=0;
	unsigned wrote=0;
	for(;;)
	{
		i=send(fd,&buff[wrote],size-wrote,0);
		if(i<=0)
			return i;
		wrote+=i;
		if(wrote==size)
			return wrote;
	}
}

#if UNUSED
static int read_line(SOCKET fd, char *buff, size_t size)
{
	int ready;
	unsigned i;
	fd_set fds;
	struct timeval tv;

    if (fd < 0)
        return -1;

	tv.tv_sec = tcp_read_time_out;
	tv.tv_usec = 0;

	for(i=0;i<size-1;i++)
	{
		FD_ZERO(&fds);
		FD_SET(fd, &fds);

		ready = select(fd + 1, &fds, NULL, NULL, &tv);
		if(ready!=1 || !(FD_ISSET(fd, &fds)) || 1!=recv(fd,&buff[i],1,0))
			return -1;
		else if(buff[i]=='\n')
		{
			buff[i+1]=0;
			return (i+1);
		}
	}

	return -1;
}
#endif

static int read_n_bytes(SOCKET fd,char *buff, size_t size)
{
	int ready;
	unsigned i;
	fd_set fds;
	struct timeval tv;

    if (fd < 0)
        return -1;

	tv.tv_sec = tcp_read_time_out;
	tv.tv_usec = 0;

	for(i=0;i<size;i++)
	{
		FD_ZERO(&fds);
		FD_SET(fd, &fds);

		ready = select(fd + 1, &fds, NULL, NULL, &tv);
		if(ready!=1 || !(FD_ISSET(fd, &fds)) || 1!=recv(fd,&buff[i],1,0))
			return -1;
	}

	return size;
}

int timed_connect(SOCKET sock, const struct sockaddr *addr, int len)
{
	int ret;
	fd_set wfds;
	fd_set efds;
	struct timeval tv;

	tv.tv_sec = tcp_connect_time_out;
	tv.tv_usec = 0;

	_set_nonblock(sock, 1);

	// Disable Nagle algorithm
	if (!enable_nagle_algorithm) {
	    _set_nodelay(sock, 1);
	}

	ret = connect(sock, addr, len);
	if(ret==-1 && (CHECK_ERR == _EWOULDBLOCK || CHECK_ERR == _EINPROGRESS))
   	{
		FD_ZERO(&wfds);
		FD_SET(sock, &wfds);
		FD_ZERO(&efds);
		FD_SET(sock, &efds);

		ret = select(sock + 1, NULL, &wfds, &efds, &tv);
      	if(ret==1)
		{
			if (FD_ISSET(sock, &efds))
				ret = IERR_NET_FAILED;
			if (FD_ISSET(sock, &wfds))
				ret = IERR_NET_SUCCESS;
        }
		else if (ret == 0)
			ret=IERR_NET_CONNECTTIMEDOUT;
		else
			ret=IERR_NET_FAILED;
	}
	else if (ret==0)
		ret=IERR_NET_SUCCESS;
	else
		ret=IERR_NET_FAILED;

	_set_nonblock(sock, 0);

	return ret;
}

static int GetResponse(int sock, char* buff, int _len)
{
	int len=0 ;
	char* p;

	debugf("GetResponse");

	memset(buff,0,_len);

	// read header byte by byte.
	while(len<_len)
	{
		if(1==read_n_bytes(sock,buff+len,1)){
			len++;
		} else {
			errorf("Failed to read response. error=%d", CHECK_ERR);
			return IERR_NET_SOCKET_FAIL;
		}

		if (len > 4     &&
			buff[len-1]=='\n'  &&
			buff[len-2]=='\r'  &&
			buff[len-3]=='\n'  &&
			buff[len-4]=='\r')
			break;
	}

	debugf("Response Body: %s", buff);

	p = strstr(buff, HD_LENGTH);
	if (p!=NULL)
	{
		char szLen[16];
		int count=0;
		int cLen=0;
		memset(szLen, 0, 16);
		p += 15;
		while(*p==' ') p++;
		while(isdigit(*p)) szLen[count++]=*p++;
		cLen = atoi(szLen);

        while(cLen>0)
        {
			if(1==read_n_bytes(sock,buff+len,1)) {
				if (len<(_len-1)) len++;	// don't want buffer over run
			}
			else break;
			cLen--;
        }
	}

	return len<_len?IERR_NET_SUCCESS:IERR_NET_BLOCKED;
}

static int ParseResponse(char* _buff, int* auth_code,
						 char* realm,
						 char* nonce,
						 char* opaque,
						 char* algorithm,
						 BOOL* fKeepAlive)
{
	char* tok;
	int last_auth_code = *auth_code;

	debugf("ParseResponse");

	*auth_code = AUTH_NONE;
	*fKeepAlive = FALSE;

	if (strncmp(&_buff[9], "200", 3)==0)
		return IERR_NET_SUCCESS;

	if (strncmp(&_buff[9], "407", 3)==0)	// proxy requires authentication
	{
		char* buff = _buff;
		while (TRUE)
		{
			char* p = strstr(buff, HD_AUTHENTICATE);
			if (p!=NULL)
			{
				char* next_p;
				p+=19;
				next_p = p;
				while(*p==' '){++p;};	// trim left
				if (strncasecmp(p, "Basic", 5)==0) {
					*auth_code = AUTH_BASIC;
				}
				else if (strncasecmp(p, "Digest", 6)==0) {
					*auth_code = AUTH_DIGEST;
					p = strstr(buff, "realm=");
					if (p) {
						p+=6;
						tok = strchr(p+1, '\"');
						strncpy(realm, p+1, (tok-p-1));
					}
					p = strstr(buff, "nonce=");
					if (p) {
						p+=6;
						tok = strchr(p+1, '\"');
						strncpy(nonce, p+1, (tok-p-1));
					}
					p = strstr(buff, "opaque=");
					if (p) {
						p+=7;
						tok = strchr(p+1, '\"');
						strncpy(opaque, p+1, (tok-p-1));
					}
					p = strstr(buff, "algorithm=");
					if (p) {
						p+=10;
						tok = strchr(p+1, '\"');
						strncpy(algorithm, p+1, (tok-p-1));
					}
                } else if (strncasecmp(p, "NTLM", 4)==0) {
                    //  NTLM is only supported on Windows
#ifdef WIN32
                    *auth_code = AUTH_NTLM;
#else
                    p = strstr(next_p, HD_AUTHENTICATE);
                    if (p!=NULL) {
                        buff = p;
                        continue;
                    }
#endif
				} else {
					*auth_code = AUTH_NEGOTIATE;
				}

				if (*auth_code == last_auth_code)	// see if there is an alternative method
				{
					p = strstr(next_p, HD_AUTHENTICATE);
					if (p!=NULL) {
						buff = p;
						continue;
					}
				}

				*fKeepAlive = FALSE;
				p = strstr(_buff, HD_CONNECTION);
				if (p!=NULL)
				{
					p+=11;
					while(*p==' '){++p;};	// trim left
					if ( !strncasecmp(p, "Keep-Alive", sizeof("Keep-Alive")-1 ) )
						*fKeepAlive = TRUE;
				}

				return IERR_NET_PROXYAUTHREQUIRED;
			}
		}
	}

	return IERR_NET_BLOCKED;
}

#ifdef USE_SSPI

static void SkipWhite(PSTR *ppS )
{
    PSTR pS = *ppS;

    while ( *pS && isspace(*pS) )
        ++pS;

    *ppS = pS;
}


static void SkipNonWhite(PSTR *ppS )
{
    PSTR pS = *ppS;

    while ( *pS && !isspace(*pS) )
        ++pS;

    *ppS = pS;
}


static int NegotiateSSPIAuthentication(
	SOCKETINFO*	socket_info,
	in_addr_t	ip,
	in_port_t	port,
	PSTR		pchUserName,
	PSTR		pchPassword,
	PSTR		pszPref
    )
{
    char               ReceiveBuffer[8*1024];
    BYTE               Request[1024];
    int                RequestSize;
    DWORD              cMaxBuffer;
    char               CrLf[] = "\r\n";
    BOOL               fKeepAlive = FALSE;
    int                cRec;
    DWORD              cLen;
    BOOL               fInHeader;
    PSTR               pchAuthData = NULL;
    BOOL               fServerKeepAlive = FALSE;
    BOOL               fNeedMoreData;
    BOOL               fNeedAuthenticate = TRUE;
    PSTR               pH;
    PSTR               pN;
    BOOL               fStatusLine;
    int                Status = -1;
    DWORD              cToRead;
	int				   err = IERR_NET_SUCCESS;
	int				   cSent;

    cMaxBuffer = SSPIInitialize(pchUserName, pchPassword, NULL);
    if (cMaxBuffer <= 0)
    {
        errorf("SSPI: Failed to initialize context [%d]", GetLastError());
        err = IERR_NET_SOCKET_FAIL;
        goto ex;
    }

    //
    // Connect to the server
    //
again:
    if ( socket_info->sock == INVALID_SOCKET )
    {
        socket_info->sock = socket(AF_INET, SOCK_STREAM, 0);

        if (socket_info->sock == INVALID_SOCKET)
        {
			errorf("SSPI: Failed to open socket");
            err = IERR_NET_SOCKET_FAIL;
            goto ex;
        }

		err = timed_connect(socket_info->sock, &socket_info->addr, socket_info->len);
		if (err != IERR_NET_SUCCESS)
			goto ex;
    }

    //
    // Send the client request
    //
	sprintf(Request,"CONNECT %s:%d HTTP/1.0\r\nUser-Agent: "
		"iic 1.0\r\n",
		inet_ntoa( * (struct in_addr *) &ip),
		ntohs(port));

    if ( fKeepAlive )
    {
        strcat(Request, "Connection: Keep-Alive\r\n" );
        strcat(Request, "Proxy-Connection: Keep-Alive\r\n" );
    }

    strcat(Request, "Proxy-Authorization: NTLM" );
    if (SSPIAuthenicate( pchAuthData, Request + strlen(Request), 512, &fNeedMoreData ))
    {
		errorf("SSPI: Add authorization header failed");
        err = IERR_NET_PROXYAUTHFAILED;
        goto ex;

    }

    strcat(Request, CrLf);
    strcat(Request, CrLf);

    RequestSize = (int)strlen(Request);

	debugf("SSPI: %s", Request);

	cSent = send(socket_info->sock, Request, RequestSize, 0);
    if ( cSent != RequestSize)
    {
		errorf("SSPI: Send request failed, error=%d", CHECK_ERR);
        err = IERR_NET_SOCKET_FAIL;
        goto ex;
    }

    //
    // Receive request and parse
    //
    cLen = (DWORD)-1;
    fInHeader = TRUE;
    fServerKeepAlive = FALSE;
    fNeedAuthenticate = FALSE;

    for ( pH = ReceiveBuffer, fStatusLine = TRUE ; fInHeader ; )
    {
        cRec = recv( socket_info->sock, pH, (int)(ReceiveBuffer+sizeof(ReceiveBuffer)-pH), 0 );
        if ( cRec <= 0 )
        {
            _close( socket_info->sock );
            socket_info->sock = INVALID_SOCKET;
            break;
        }
        pH[ cRec ] = '\0';

        // Iterate on header fields
        while ( (pN = strstr(pH, "\r\n" )) )
        {
            *pN = '\0';

			debugf("%s", pH);

            if ( fStatusLine )
            {
                // This is the status line, decode status
                SkipNonWhite( &pH );
                SkipWhite( &pH );
                Status = atoi( pH );
				if ( Status == 200 )
				{
				    return IERR_NET_SUCCESS;
				}
                if ( Status == 407 )
                {
                    fNeedAuthenticate = TRUE;
                }
                fStatusLine = FALSE;
            }
            else if ( pN == pH )    // end of header fields
            {
                cLen -= (int)(ReceiveBuffer+cRec-pH-2 );
                fInHeader = FALSE;
                break;
            }
            else if ( !strnicmp( pH, HD_AUTHENTICATE, sizeof(HD_AUTHENTICATE)-1 ) )
            {
                SkipNonWhite( &pH );
                SkipWhite( &pH );

                // store pointer to authenticate blob

                SkipNonWhite( &pH );
                SkipWhite( &pH );
                pchAuthData = pH;
            }
            else if ( !strnicmp( pH, HD_LENGTH, sizeof(HD_LENGTH)-1 ) )
            {
                // get content length

                SkipNonWhite( &pH );
                SkipWhite( &pH );
                cLen = atoi( pH );
            }
            else if ( !strnicmp( pH, HD_CONNECTION, sizeof(HD_CONNECTION)-1 ) )
            {
                // check for keep-alive flag

                SkipNonWhite( &pH );
                SkipWhite( &pH );
                if ( !strnicmp( pH, "Keep-Alive", sizeof("Keep-Alive")-1 ) )
                    fServerKeepAlive = TRUE;
            }
            else if ( !strnicmp( pH, HD_PROXYCONNECTION, sizeof(HD_PROXYCONNECTION)-1 ) )
            {
                // check for keep-alive flag

                SkipNonWhite( &pH );
                SkipWhite( &pH );
                if ( !strnicmp( pH, "Keep-Alive", sizeof("Keep-Alive")-1 ) )
                    fServerKeepAlive = TRUE;
            }

            pH = pN + 2;
        }
    }

    // read message body

    if ( socket_info->sock != INVALID_SOCKET )
    {
        while(cLen>0)
        {
            if ( (cToRead = sizeof(ReceiveBuffer)) > cLen )
                cToRead = cLen;
            cRec = recv( socket_info->sock, ReceiveBuffer, cToRead, 0 );

            if ( cRec <= 0 )
            {
				warnf("SSPI: No enough bytes read from the message body, expected:%d recv'd:%d", cToRead, cRec);
//                _close( socket_info->sock );
//                socket_info->sock = INVALID_SOCKET;
                break;
            }

            cLen -= cRec;
        }
    }

    if ( !fServerKeepAlive )
    {
        errorf("SSPI: Authentication rejected by server" );

        fNeedAuthenticate = FALSE;  // authentication failed

        err = IERR_NET_PROXYAUTHFAILED;

        _close( socket_info->sock );
        socket_info->sock = INVALID_SOCKET;
    }

    if ( fNeedAuthenticate )
    {
        fKeepAlive = TRUE;
        goto again;
    }

ex:
    return err;
}

#endif


static int SendRequestWithoutAuthentication(SOCKETINFO* socket_info, in_addr_t ip, in_port_t port,
											char* buff, int buff_size)
{
  debugf("SendRequestWithoutAuthentication");

	int len;
	sprintf(buff,"CONNECT %s:%d HTTP/1.0\r\nUser-Agent: iic 1.0\r\n"
		"Connection: Keep-Alive\r\n"
		"Proxy-Connection: Keep-Alive\r\n"
		"\r\n",
		inet_ntoa( * (struct in_addr *) &ip),
		ntohs(port));

	len=strlen(buff);

	debugf("None: %s", buff);

	if(len!=send(socket_info->sock,buff,len,0)) {
		errorf("Failed to send request. error=%d", CHECK_ERR);
		return IERR_NET_SOCKET_FAIL;
	}

	return GetResponse(socket_info->sock, buff, buff_size);
}

static int NegotiateBasicAuthentication(SOCKETINFO* socket_info, in_addr_t ip, in_port_t port,
			char* buff, int buff_size, const char* user, const char* pass)
{
	int len;
	int err;

	debugf("NegotiateBasicAuthentication");

    if ( socket_info->sock == INVALID_SOCKET )
    {
        socket_info->sock = socket(AF_INET, SOCK_STREAM, 0);

        if (socket_info->sock == INVALID_SOCKET)
        {
			errorf("Digest: Failed to open socket");
            return IERR_NET_SOCKET_FAIL;
        }

		err = timed_connect(socket_info->sock, &socket_info->addr, socket_info->len);
		if (err != IERR_NET_SUCCESS)
			return err;
    }

	sprintf(buff,"CONNECT %s:%d HTTP/1.0\r\nUser-Agent: iic 1.0\r\n"
		"Connection: Keep-Alive\r\n"
		"Proxy-Connection: Keep-Alive\r\n",
		inet_ntoa( * (struct in_addr *) &ip),
		ntohs(port));

	{
		char src[256];
		char dst[512];
		strcpy(src,user);
		strcat(src,":");
		strcat(src,pass);
		encode_base_64(src,dst,512);
		strcat(buff,"Proxy-Authorization: Basic ");
		strcat(buff,dst);
		strcat(buff,"\r\n\r\n");
	}

	len=strlen(buff);

	debugf("Basic: %s", buff);

	if(len!=send(socket_info->sock,buff,len,0)) {
		errorf("Basic: Send Basic authentication response failed, error=%d", CHECK_ERR);
		return IERR_NET_SOCKET_FAIL;
	}

	return GetResponse(socket_info->sock, buff, buff_size);
}

static int NegotiateDigestAuthentication(SOCKETINFO* socket_info, in_addr_t ip, in_port_t port,
		char* buff, int buff_size, const char* user, const char* pass,
		const char* realm, const char* nonce, const char* opaque, const char* algorithm, BOOL fKeepAlive)
{
	int len;

	debugf("NegotiateDigestAthentication");

	char * qop = "auth";					// Quality of Protection
	char * cnonce = "0a4f113b";
	char nonce_count[9] = "00000001";
	char * pszMethod = "CONNECT";
	char   pszURI[256]="";
	HASHHEX HA1;
	HASHHEX HA2 = "";
	HASHHEX Response;
	int err;

	memset(buff, 0, buff_size);
	sprintf(pszURI, "%s:%d", inet_ntoa( * (struct in_addr *) &ip), ntohs(port));

    if ( socket_info->sock == INVALID_SOCKET )
    {
        socket_info->sock = socket(AF_INET, SOCK_STREAM, 0);

        if (socket_info->sock == INVALID_SOCKET)
        {
			errorf("Digest: Failed to open socket");
            return IERR_NET_SOCKET_FAIL;
        }

		err = timed_connect(socket_info->sock, &socket_info->addr, socket_info->len);
		if (err != IERR_NET_SUCCESS)
			return err;
    }

	DigestCalcHA1(algorithm, user, realm, pass, nonce, cnonce, HA1);
	DigestCalcResponse(HA1, nonce, nonce_count, cnonce, qop,
	  pszMethod, pszURI, HA2, Response);

	sprintf(buff,"CONNECT %s:%d HTTP/1.0\r\nUser-Agent: "
		"iic 1.0\r\n"
		"Proxy-Authorization: Digest username=\"%s\","
		"realm=\"%s\",nonce=\"%s\",qop=auth,nc=00000001,cnonce=\"%s\",uri=\"%s\",response=\"%s\"",
		inet_ntoa( * (struct in_addr *) &ip), ntohs(port),
		user, realm, nonce, cnonce, pszURI, Response);

	if (opaque[0]!=0) {
		strcat(buff, ",opaque=\"");
		strcat(buff, opaque);
		strcat(buff, "\"");
	}
    if ( fKeepAlive )
    {
        strcat(buff, "\r\nConnection: Keep-Alive" );
        strcat(buff, "\r\nProxy-Connection: Keep-Alive" );
    }
	strcat(buff, "\r\n\r\n");
	len=strlen(buff);

	debugf("Digest: %s", buff);

	if(len!=send(socket_info->sock,buff,len,0))
	{
		errorf("Digest: Send Digest authentication response failed");
		return IERR_NET_SOCKET_FAIL;
	}

	return GetResponse(socket_info->sock, buff, buff_size);
}

static int Socks4Authentication(SOCKETINFO* socket_info, in_addr_t ip, in_port_t port,char *user)
{
	char buff[256];
	int len, err;

	debugf("Socks4Authentication");

	if ( socket_info->sock == INVALID_SOCKET )
    {
        socket_info->sock = socket(AF_INET, SOCK_STREAM, 0);

        if (socket_info->sock == INVALID_SOCKET)
        {
			errorf("SOCKS v4: Failed to open socket");
            return IERR_NET_SOCKET_FAIL;
        }

		err = timed_connect(socket_info->sock, &socket_info->addr, socket_info->len);
		if (err != IERR_NET_SUCCESS)
			return err;
    }

	memset(buff,0,sizeof(buff));
	buff[0]=4; // socks version
	buff[1]=1; // connect command
	memcpy(&buff[2],&port,2); // dest port
	memcpy(&buff[4],&ip,4); // dest host
	len=strlen(user)+1; // username
	if(len>1)
		strcpy(&buff[8],user);
	if((len+8)!=write_n_bytes(socket_info->sock,buff,(8+len))) {
		errorf("SOCKS v4: Failed to write SOCKS v4 request, error=%d", CHECK_ERR);
		return IERR_NET_SOCKET_FAIL;
	}

	if(8!=read_n_bytes(socket_info->sock,buff,8)) {
		errorf("SOCKS v4: Failed to read SOCKS v4 response, error=%d", CHECK_ERR);
		return IERR_NET_SOCKET_FAIL;
	}

	if (buff[0]!=0||buff[1]!=90) {
		warnf("SOCKS v4: autentication was blocked");
		return IERR_NET_BLOCKED;
	}

    return IERR_NET_SUCCESS;
}

static int Socks5Authentication(SOCKETINFO* socket_info, in_addr_t ip, in_port_t port, char *user, char *pass)
{
	char buff[256];
	int len, err;
	debugf("Socks5Authentication");
	if ( socket_info->sock == INVALID_SOCKET )
    {
        socket_info->sock = socket(AF_INET, SOCK_STREAM, 0);

        if (socket_info->sock == INVALID_SOCKET)
        {
			errorf("SOCKS v4: Failed to open socket");
            return IERR_NET_SOCKET_FAIL;
        }

		err = timed_connect(socket_info->sock, &socket_info->addr, socket_info->len);
		if (err != IERR_NET_SUCCESS)
			return err;
    }

	if(user)
	{
		buff[0]=5;   //version
		buff[1]=2;	//nomber of methods
		buff[2]=0;   // no auth method
		buff[3]=2;  /// auth method -> username / password
		if(4!=write_n_bytes(socket_info->sock,buff,4)){
			errorf("SOCKS v5: Failed to send SOCKS v5 initial request. error=%d", CHECK_ERR);
			return IERR_NET_SOCKET_FAIL;
		}
	}
	else
	{
		buff[0]=5;   //version
		buff[1]=1;	//nomber of methods
		buff[2]=0;   // no auth method
		if(3!=write_n_bytes(socket_info->sock,buff,3)){
			errorf("SOCKS v5: Failed to send SOCKS v5 initial request. error=%d", CHECK_ERR);
			return IERR_NET_SOCKET_FAIL;
		}
	}

	memset(buff,0,sizeof(buff));

	if(2!=read_n_bytes(socket_info->sock,buff,2)){
		errorf("SOCKS v5: Failed to read SOCKS v5 initial response. error=%d", CHECK_ERR);
		return IERR_NET_SOCKET_FAIL;
	}

	if (buff[0]!=5||(buff[1]!=0&&buff[1]!=2))
    {
		if((buff[0]==5)&&(buff[1]==(char)0xFF)){
			errorf("SOCKS v5: authentication was blocked.");
			return IERR_NET_BLOCKED;
		} else {
			errorf("SOCKS v5: authentication was failed.");
			return IERR_NET_SOCKET_FAIL;
		}
	}

	if (buff[1]==2)
	{
		// authentication
		char in[2];
		char out[515]; char* cur=out;
		int c;
		*cur++=1; // version
		c=strlen(user);
		*cur++=c;
		strncpy(cur,user,c);
		cur+=c;
		c=strlen(pass);
		*cur++=c;
		strncpy(cur,pass,c);
		cur+=c;

		if((cur-out)!=write_n_bytes(socket_info->sock,out,cur-out)) {
			errorf("SOCKS v5: Failed to send SOCKS v5 second request. error=%d", CHECK_ERR);
			return IERR_NET_SOCKET_FAIL;
		}


		if(2!=read_n_bytes(socket_info->sock,in,2)) {
			errorf("SOCKS v5: Failed to read SOCKS v5 second response. error=%d", CHECK_ERR);
			return IERR_NET_SOCKET_FAIL;
		}
		if(in[0]!=1||in[1]!=0)
		{
			if(in[0]!=1) {
				errorf("SOCKS v5: authentication was failed.");
				return IERR_NET_SOCKET_FAIL;
			} else if (user[0]!=0) {
				errorf("SOCKS v5: authentication was failed, username/password maybe wrong.");
				return IERR_NET_PROXYAUTHFAILED;
			} else {
				errorf("SOCKS v5: authentication was failed, no user name.");
				return IERR_NET_PROXYAUTHREQUIRED;
			}
		}
	}

	buff[0]=5;       // version
	buff[1]=1;       // connect
	buff[2]=0;       // reserved
	buff[3]=1;       // ip v4

	memcpy(&buff[4],&ip,4); // dest host
	memcpy(&buff[8],&port,2); // dest port

	if(10!=write_n_bytes(socket_info->sock,buff,10)) {
		errorf("SOCKS v5: Send connect command to SOCKS v5 failed. error=%d", CHECK_ERR);
		return IERR_NET_SOCKET_FAIL;
	}

	if(4!=read_n_bytes(socket_info->sock,buff,4)) {
		errorf("SOCKS v5: Failed to read connect response. error=%d", CHECK_ERR);
		return IERR_NET_SOCKET_FAIL;
	}

	if (buff[0]!=5||buff[1]!=0) {
		errorf("SOCKS v5: Invalid connect response.");
		return IERR_NET_SOCKET_FAIL;
	}

	switch (buff[3])
	{
	case 1: len=4;  break;
	case 4: len=16; break;
	case 3: len=0;
		if(1!=read_n_bytes(socket_info->sock,(char*)&len,1)) {
			errorf("SOCKS v5: Failed to read the data length of the connect response. error=%d", CHECK_ERR);
			return IERR_NET_SOCKET_FAIL;
		}
		break;
	default:
		errorf("SOCKS v5: Invalid connect response.");
		return IERR_NET_SOCKET_FAIL;
	}

	if((len+2)!=read_n_bytes(socket_info->sock,buff,(len+2))) {
		errorf("SOCKS v5: Failed to read SOCKS v5 final response. error=%d", CHECK_ERR);
		return IERR_NET_SOCKET_FAIL;
	}

	return IERR_NET_SUCCESS;
}

/* Create SSL tunnel through proxy server to target server */
static int tunnel_to(SOCKETINFO* socket_info, in_addr_t ip, in_port_t port, proxy_types pt,char *user, char *pass)
{
	static int auth_code = AUTH_NONE;
	static char realm[256], nonce[256], opaque[256], algorithm[32];
	static BOOL fKeepAlive = FALSE;
	debugf("tunnel_to");
	char buff[BUFF_SIZE];
	int err;

	switch(pt)
	{
	case HTTP_TYPE:
		{
			switch(auth_code)
			{
			case AUTH_NONE:
				infof("First try without authentication.");
				err = SendRequestWithoutAuthentication(socket_info, ip, port, buff, sizeof(buff));
				if (err!=IERR_NET_SUCCESS) return err;

				memset(realm,0,sizeof(realm));
				memset(nonce,0,sizeof(nonce));
				memset(opaque,0,sizeof(opaque));
				memset(algorithm,0,sizeof(algorithm));

				err = ParseResponse(buff, &auth_code, realm, nonce, opaque, algorithm, &fKeepAlive);
				if (err == IERR_NET_PROXYAUTHREQUIRED && (user[0] != 0 || auth_code == AUTH_NTLM)) {
					errorf("Authentication is required, auth code=%d", auth_code);
					if (!fKeepAlive) {
						debugf("Server does not want to keep the connect, close it.");
						_close( socket_info->sock );
						socket_info->sock = INVALID_SOCKET;
					}
				}	// pass through
				else
					break;
			default:
				switch(auth_code)
				{
				case AUTH_BASIC:
					infof("Perform Basic authentication.");
					err = NegotiateBasicAuthentication(socket_info, ip, port, buff, sizeof(buff), user, pass);
					if (err == IERR_NET_SUCCESS){
						err = ParseResponse(buff, &auth_code, realm, nonce, opaque, algorithm, &fKeepAlive);
					}
					break;
				case AUTH_DIGEST:
					infof("Perform Digest authentication.");
					err = NegotiateDigestAuthentication(socket_info, ip, port, buff, sizeof(buff), user, pass, realm, nonce, opaque, algorithm, fKeepAlive);
					if (err == IERR_NET_SUCCESS) {
						memset(realm,0,sizeof(realm));
						memset(nonce,0,sizeof(nonce));
						memset(opaque,0,sizeof(opaque));
						memset(algorithm,0,sizeof(algorithm));
						err = ParseResponse(buff, &auth_code, realm, nonce, opaque, algorithm, &fKeepAlive);
						if (err == IERR_NET_PROXYAUTHREQUIRED && user[0]!=0 && auth_code!=AUTH_DIGEST){
							// the proxy server may support multiple methods of authentication,
							// if one failed, try a different one
							debugf("Digest authentication was failed, server also supports NTLM.");
							if (!fKeepAlive) {
								debugf("Server does not want to keep the connect, close it.");
								_close( socket_info->sock );
								socket_info->sock = INVALID_SOCKET;
							}
						}	// pass through -- try NTLM or Negotiate
						else
							break;
					}
					else
						break;
#ifdef USE_SSPI	/* Win32 only */
				case AUTH_NTLM:
					if (auth_code == AUTH_NTLM) {
						infof("Perform NTLM authentication.");
						err = NegotiateSSPIAuthentication(socket_info, ip, port, user, pass, "NTLM");
						if (err != IERR_NET_PROXYAUTHFAILED) {
							auth_code = AUTH_NONE;	// don't reset it if we want to try later
						}
						fKeepAlive = FALSE;
						break;
					}
					// else pass through
/*				default:
					infof("Negotiate authentication with server.");
					err = NegotiateSSPIAuthentication(socket_info, ip, port, user, pass, "Negotiate");
					auth_code = AUTH_NONE;
					fKeepAlive = FALSE;
					break;
*/
#endif
				default:
					infof("Unable to authenticate with proxy.");
					auth_code = AUTH_NONE;
					err = IERR_NET_AUTHFAILED;
					break;
				}
			}
		}

		break;

	case SOCKS4_TYPE:
		err = Socks4Authentication(socket_info, ip, port, user);
		break;

	case SOCKS5_TYPE:
		err = Socks5Authentication(socket_info, ip, port, user, pass);
		break;

	case SOCKS_TYPE:
		debugf("Perform SOCKS authentication.");
		err = Socks5Authentication(socket_info, ip, port, user, pass);
		if (err != IERR_NET_SUCCESS) {
			_close( socket_info->sock );
			socket_info->sock = INVALID_SOCKET;
			err = Socks4Authentication(socket_info, ip, port, user);
		}

	default:
		break;
	}

	return err;
}

static int connect_to_proxy(SOCKETINFO* socket_info, in_addr_t ip, in_port_t port)
{
	struct sockaddr_in addr;
	struct hostent *hp;
	int retcode;
	PROXY* proxy;

	memset(&addr,0,sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(proxy_ip);

	proxy = proxy_list;
	while (proxy)
	{
		addr.sin_port = htons(proxy->port);
		if ((addr.sin_addr.s_addr = inet_addr(proxy->server)) == INADDR_NONE)
		{
			hp = gethostbyname (proxy->server);
			if (hp == NULL || hp->h_addrtype != AF_INET)
			{
				errorf("Host is not reachable. %s:%d", proxy->server, proxy->port);
				retcode = IERR_NET_NOTREACHABLE;
				proxy = proxy->next;
				continue;
			}
			memcpy (&addr.sin_addr, hp->h_addr, hp->h_length);
		}

		if ( socket_info->sock == INVALID_SOCKET )
		{
			socket_info->sock = socket(AF_INET, SOCK_STREAM, 0);

			if (socket_info->sock == INVALID_SOCKET)
			{
				errorf("Failed to open a new socket");
				return IERR_NET_SOCKET_FAIL;
			}
		}

		// tcp connect to proxy
		retcode = timed_connect(socket_info->sock, (struct sockaddr*)&addr, sizeof(addr));
		if (IERR_NET_SUCCESS != retcode)
		{
			errorf("Could not connect to proxy: %s:%d [%d]", proxy->server, proxy->port, retcode);
			_close(socket_info->sock);
			socket_info->sock = INVALID_SOCKET;
			proxy = proxy->next;
			continue;
		}

		// save proxy address info
		memcpy(&socket_info->addr, (struct sockaddr*)&addr, sizeof(addr));
		socket_info->len  = sizeof(addr);

		// tunnel to target
		retcode = tunnel_to(socket_info, ip, port, proxy->type, proxy_user, proxy_password);
		switch(retcode)
		{
		case IERR_NET_SUCCESS:
			break;
		case IERR_NET_BLOCKED:
		case IERR_NET_SOCKET_FAIL:
			_close(socket_info->sock);
			socket_info->sock = INVALID_SOCKET;
			proxy = proxy->next;
			continue;
		case IERR_NET_PROXYAUTHREQUIRED:
		case IERR_NET_PROXYAUTHFAILED:
			if (proxy->next!=NULL)	// keep trying other
			{
				_close(socket_info->sock);
				socket_info->sock = INVALID_SOCKET;
				proxy = proxy->next;
				continue;
			}
			return retcode;
		}

		return retcode;
	}

	return IERR_NET_FAILED;
}

#define MAX_NET_SOCKETS 64
SOCKETINFO *sockets[MAX_NET_SOCKETS] = { 0 };
int last_sock = 0;

SOCKET net_socket()
{
	int net_socket = ++last_sock;

	while (++net_socket != last_sock)
	{
		if (net_socket >= MAX_NET_SOCKETS)
			net_socket = 1;

		if (sockets[net_socket] == NULL)
			break;
	}

	// No empty slots
	if (sockets[net_socket] != NULL)
		return -1;

	SOCKETINFO *si = calloc(1, sizeof(SOCKETINFO));
	si->sock = -1;

	sockets[net_socket] = si;
	last_sock = net_socket;

	return net_socket;
}

SOCKETINFO * find_socket(SOCKET net_socket)
{
	if (net_socket <= 0 || net_socket > MAX_NET_SOCKETS)
		return NULL;
	return sockets[net_socket];
}

void setup_socket(SOCKETINFO *si, BOOL use_http)
{
	if (!si->is_http && si->sock != -1)
	{
		_close(si->sock);
		si->sock = -1;
	}

	if (!use_http)
	{
		si->sock = socket(AF_INET, SOCK_STREAM, 0);
		if (si->sock < 0)
		{
			errorf("Unexpected error creating socket [%d]", CHECK_ERR);
		}
	}
	else
	{
		si->is_http = use_http;
		HttpInit(&si->http);
		HttpSetThreadMode(si->http, main_thread, polling_hook);
	}
}

int net_connect(SOCKET net_socket, const char * server, const struct sockaddr *addr, int len, int direct)
{
	debugf("net_connect");
	SOCKETINFO *socket_info = find_socket(net_socket);

	if (socket_info == NULL)
		return -1;

	if (socket_info->sock == -1 && !socket_info->is_http)
		setup_socket(socket_info, FALSE);

	if (!socket_info->is_http)
	{
		int socktype=0,ret=0;
		int optlen=0;
		struct sockaddr_in *in_addr = (struct sockaddr_in *)addr;

		memcpy(&socket_info->addr, addr, len);
		socket_info->len = len;

		if (proxy_type == NO_PROXY || direct)
			return timed_connect(socket_info->sock,&socket_info->addr,socket_info->len);
		else if (proxy_type == DETECT_TYPE && *target_server) {
			debugf("Detect proxy server settings.");
			if (proxy_detect(target_server)!=0) {
				errorf("Failed to get proxy server settings.");
				return IERR_NET_FAILED;
			}
		}

		optlen = sizeof(socktype);
		getsockopt(socket_info->sock, SOL_SOCKET, SO_TYPE, (char *)&socktype, &optlen);
		if (!(socket_info->addr.sa_family == AF_INET  && socktype == SOCK_STREAM))
		{
			// Disable Nagle algorithm
			if (!enable_nagle_algorithm) {
				_set_nodelay(socket_info->sock, 1);
			}
			return connect(socket_info->sock,&socket_info->addr,socket_info->len);
		}

		ret = connect_to_proxy(socket_info, in_addr->sin_addr.s_addr, in_addr->sin_port);
		if(ret != 0)
			SET_ERR(_ENETUNREACH);

		return ret;
	}
	else
	{
		return HttpConnect(socket_info->http, server);
	}
}

static void delete_proxies()
{
	while (proxy_list!=NULL)
	{
		PROXY* proxy = proxy_list;
		proxy_list = proxy->next;
		free(proxy->server);
		free(proxy);
	}
}

#ifdef WIN32

static void parse_proxy_settings(char* settings)
{
	char* p = settings;
	char* token = NULL;
	PROXY* proxy;

	if (settings == NULL)
		return;

	infof("Parsing proxy settings: %s", settings);

	delete_proxies();	// get rid of the old ones

	while (TRUE)
	{
		token = strchr(p, ' ');
		if (token == NULL)
			break;

		proxy = malloc(sizeof(PROXY));
		memset(proxy, 0, sizeof(PROXY));

		*token = '\0';
		if (stricmp(p, "PROXY")==0) {
			proxy->type = HTTP_TYPE;
		} else if (strnicmp(p, "SOCKS", 5)==0) {
			proxy->type = SOCKS_TYPE;	// no version specified, assume v4
		} else {
			free(proxy);
			break;
		}

		p = token + 1;
		token = strchr(p, ':');
		if (token != NULL) {

			*token = '\0';
			proxy->server = strdup(p);	// server name
			p = token + 1;
			token = strchr(p, ';');
			if (token!=NULL) {
				*token = '\0';
			}
			proxy->port = atoi(p);	// port

			if (proxy_list==NULL) {	// add to list
				proxy_list = proxy;
			} else {
				proxy_list->next = proxy;
			}

			if (token == NULL) break;
			p = token + 1;
		}
		else
		{
			free(proxy);
			break;
		}
	}
}

static int parse_ie_proxy_settings(INTERNET_PROXY_INFO* settings)
{
	char* p = (char*)settings->lpszProxy;
	char* token = NULL;
	char* c = NULL;
	PROXY* proxy;

	if (settings == NULL)
		return -1;

	delete_proxies();	// get rid of the old ones

	infof("Parsing IE proxy settings: %s", p);

	token = strtok(p, "; ");
	while (token!=NULL)
	{
		proxy = malloc(sizeof(PROXY));
		memset(proxy, 0, sizeof(PROXY));

		proxy->type = HTTP_TYPE;
		if (strnicmp(token, "socks=", 6)==0) {
			proxy->type = SOCKS_TYPE;
			token+=6;
		} else if (strnicmp(token, "http=", 5)==0) {
			proxy->type = HTTP_TYPE;
			token+=5;
		} else if (strchr(token, '=') == NULL) {
			// No token, assume http (formated this way if one proxy is configured for all protocols)
			proxy->type = HTTP_TYPE;
		} else {
			free(proxy);
			token = strtok(NULL, "; ");
			continue;
		}

		c = strchr(token, ':');
		if (c != NULL) {
			*c = '\0';
			proxy->server = strdup(token);	// server name
			token = c + 1;
			proxy->port = atoi(token);	// port

			debugf("Adding: %s:%d", proxy->server, proxy->port);

			if (proxy_list==NULL) {	// add to list
				proxy_list = proxy;
			} else {
				proxy_list->next = proxy;
			}

			token = strtok(NULL, "; ");
		}
		else
		{
			free(proxy);
			break;
		}
	}

	return proxy_list==NULL?-1:0;
}

int get_internet_connection_flags()
{
	INTERNET_PER_CONN_OPTION_LIST list;
	INTERNET_PER_CONN_OPTION option;
    DWORD   dwBufSize = sizeof(list);

	list.dwSize = sizeof(list);
	list.pszConnection = NULL;
	list.dwOptionCount = 1;
	list.pOptions = &option;
	option.dwOption = INTERNET_PER_CONN_FLAGS;

	if (InternetQueryOption(NULL,INTERNET_OPTION_PER_CONNECTION_OPTION, &list, &dwBufSize))
	{
		return list.pOptions[0].Value.dwValue;
	}

	return PROXY_TYPE_DIRECT;
}

BOOL is_proxy_configured()
{
	return get_internet_connection_flags() != PROXY_TYPE_DIRECT;
}

int proxy_detect(char *server)
{
	char url[1024];
	char proxy_server[1024];
	int status=-1;

	// For IE, get the manual proxy settings first
	INTERNET_PER_CONN_OPTION_LIST list;
	INTERNET_PER_CONN_OPTION option;
    DWORD   dwBufSize = sizeof(list);

	list.dwSize = sizeof(list);
	list.pszConnection = NULL;
	list.dwOptionCount = 1;
	list.pOptions = &option;
	option.dwOption = INTERNET_PER_CONN_FLAGS;

	if (InternetQueryOption(NULL,INTERNET_OPTION_PER_CONNECTION_OPTION, &list, &dwBufSize))
	{
		if (list.pOptions[0].Value.dwValue & PROXY_TYPE_PROXY)	// retrieve proxy settings
		{
			DWORD dwSize=0;
			HGLOBAL hData;
			char* lpszData;
			INTERNET_PROXY_INFO* pInfo;

			//This call determines the buffer size needed.
			InternetQueryOption(NULL,INTERNET_OPTION_PROXY ,NULL,&dwSize);
			if (dwSize>0)
			{
				//Allocate the necessary memory.
				hData = GlobalAlloc(GHND, dwSize);
				lpszData = GlobalLock(hData);
				//Call InternetQueryOption again with the buffer provided.
				if (InternetQueryOption(NULL, INTERNET_OPTION_PROXY,lpszData,&dwSize))
				{
					pInfo = (INTERNET_PROXY_INFO*)lpszData;
					if (pInfo->dwAccessType == INTERNET_OPEN_TYPE_PROXY)
					{
						status = parse_ie_proxy_settings(pInfo);
					}
				}
				GlobalUnlock(hData);
				//Free the allocated memory.
				GlobalFree(lpszData);
			}

			if (status==0 && proxy_user[0]==0)
			{
				//This call determines the buffer size needed.
                dwSize = 0;
                InternetQueryOption(NULL,INTERNET_OPTION_PROXY_USERNAME ,NULL,&dwSize);
				if (dwSize>0)
				{
					//Allocate the necessary memory.
					hData = GlobalAlloc(GHND, dwSize);
					lpszData = GlobalLock(hData);
					//Call InternetQueryOption again with the buffer provided.
					InternetQueryOption(NULL, INTERNET_OPTION_PROXY_USERNAME,lpszData,&dwSize);
					strcpy(proxy_user, lpszData);
					GlobalUnlock(hData);
					//Free the allocated memory.
					GlobalFree(lpszData);
				}

				//This call determines the buffer size needed.
                dwSize = 0;
				InternetQueryOption(NULL,INTERNET_OPTION_PROXY_PASSWORD ,NULL,&dwSize);
				if (dwSize > 0)
				{
					//Allocate the necessary memory.
					hData = GlobalAlloc(GHND, dwSize);
					lpszData = GlobalLock(hData);
					//Call InternetQueryOption again with the buffer provided.
					InternetQueryOption(NULL, INTERNET_OPTION_PROXY_PASSWORD,lpszData,&dwSize);
					strcpy(proxy_password, lpszData);
					GlobalUnlock(hData);
					//Free the allocated memory.
					GlobalFree(lpszData);
				}
			}

			if (status==0)
				return status;
		}
	}

	// We're going to open an SSL tunnel, so detect proxy for HTTPS protocol
	sprintf(url, "https://%s", server);

	status = DetectProxy(url, proxy_server, sizeof(proxy_server));
	if (status == 0)
	{
		// Parse & handle setting (e.g. "PROXY x.x.x.x:p; DIRECT")
		parse_proxy_settings(proxy_server);
	}

	return status;
}

#elif defined(LINUX) || defined(MACOSX)

BOOL is_proxy_configured( )
{
	return proxy_is_proxy_configured( );
}

int proxy_detect(char *server)
{
	char url[1024];

    if( !is_proxy_configured( ) )
        return NOPROXY;

    delete_proxies();	// get rid of the old ones

	// We're going to open an SSL tunnel, so detect proxy for HTTPS protocol
	sprintf(url, "https://%s", server);

    HttpProxyInfo       proxy_info;
    int                 fRet;
    fRet = proxy_for_uri( url, &proxy_info );
    if( fRet )
    {
        PROXY   *proxy;
        proxy = malloc( sizeof(PROXY) );
        memset( proxy, 0, sizeof(PROXY) );

        //  check for other types of connection?
        proxy->type   = HTTP_TYPE;

        proxy->server = strdup( proxy_info.host );	// server name
        proxy->port   = proxy_info.port;	// port

        // add to list
        if( proxy_list  ==  NULL )
        {   // add to list
            proxy_list = proxy;
        }
        else
        {
            proxy_list->next = proxy;
        }

        if( proxy_info.username  !=  NULL )
            strcpy( proxy_user,     proxy_info.username );
        if( proxy_info.password  !=  NULL )
            strcpy( proxy_password, proxy_info.password );
    }

    return( (fRet) ? 0 : NOPROXY );
}

#else

BOOL is_proxy_configured()
{
    return FALSE;
}

int proxy_detect(char *server)
{
    return NOPROXY;
}

#endif

void net_set_server(proxy_types type, const char *target)
{
	proxy_type = type;
	*target_server = '\0';
	strncat(target_server, target, sizeof(target_server));
}

void net_set_auth(auth_types type, int current_user, const char *user, const char *passwd)
{
	proxy_auth = type;
	proxy_current_user = current_user;
	*proxy_user = '\0';
	strncat(proxy_user, user, sizeof(proxy_user));
	*proxy_password = '\0';
	strncat(proxy_password, passwd, sizeof(proxy_password));
}

void net_set_http(BOOL flag)
{
  net_http_mode = flag;
}

BOOL net_get_http()
{
  return net_http_mode;
}

char* net_err_string(int err)
{
	if (err < 0)
		return "Unknown error.";

	return ErrString[err];
}

int net_recv(SOCKET net_socket, char *buffer, int len)
{
  SOCKETINFO *socket_info = find_socket(net_socket);
  int res;

  if (socket_info == NULL)
    return -1;

  if (!socket_info->is_http)
  {
      res = recv(socket_info->sock, buffer, len, 0);
  }
  else
  {
      res = HttpRecv(socket_info->http, buffer, len);
  }
/*
  if (res < 0 && CHECK_ERR != _EWOULDBLOCK)
  {
      errorf("Connection error [%d]", CHECK_ERR);
  }
*/
  return res;
}

int net_send(SOCKET net_socket, char *buffer, int len )
{
	SOCKETINFO *socket_info = find_socket(net_socket);
	int res;

	if (socket_info == NULL)
		return -1;

	if (!socket_info->is_http)
	{
		res = send(socket_info->sock, buffer, len, 0);
	}
	else
	{
		res = HttpSend(socket_info->http, buffer, len );
	}

	if (res < 0 && CHECK_ERR != _EWOULDBLOCK)
	{
	  errorf("Connection error [%d]", CHECK_ERR);
	}

	return res;
}

// Check/wait for readability.
// Timeout is in seconds.
int net_checkreadable(SOCKET net_socket, int timeout)
{
	SOCKETINFO *socket_info = find_socket(net_socket);
	int res;

	if (socket_info == NULL)
		return -1;

	if (!socket_info->is_http)
	{
        if (socket_info->sock < 0)
            return -1;
            
		fd_set fds;
		struct timeval tv;
		tv.tv_sec = timeout;
		tv.tv_usec = 0;

		FD_ZERO(&fds);
		FD_SET(socket_info->sock, &fds);

		res = select(socket_info->sock + 1, &fds, NULL, NULL, &tv);
		if (res == 0)
			return 0;
		else if (res > 0)
			return 1;
	}
	else
	{
		res = HttpCheckReadable(socket_info->http, timeout);
	}
	if (res < 0 && CHECK_ERR != _EWOULDBLOCK)
	  errorf("Connection error [%d]", CHECK_ERR);

	return res;
}

// Check/wait for writeability.
// Timeout is in seconds.  HTTP version always returns 1.
int net_checkwriteable(SOCKET net_socket, int timeout)
{
	SOCKETINFO *socket_info = find_socket(net_socket);
	int res;

	if (socket_info == NULL)
		return -1;

	if (!socket_info->is_http)
	{
        if (socket_info->sock < 0)
            return -1;

		fd_set fds;
		struct timeval tv;
		tv.tv_sec = timeout;
		tv.tv_usec = 0;

		FD_ZERO(&fds);
		FD_SET(socket_info->sock, &fds);

		res = select(socket_info->sock + 1, NULL, &fds, NULL, &tv);
		if (res == 0)
			return 0;
		else if (res > 0)
			return 1;
	}
	else
	{
		res = HttpCheckWriteable(socket_info->http, timeout);
	}
	if (res < 0 && CHECK_ERR != _EWOULDBLOCK)
		errorf("Connection error [%d]", CHECK_ERR);

	return res;
}

int net_close(SOCKET net_socket, BOOL clean_shutdown)
{
	SOCKETINFO *socket_info = find_socket(net_socket);
	int res = -1;

	if (socket_info == NULL)
		return -1;

	socket_info->interrupt = TRUE;

	infof("Closing network connection");

	if (!socket_info->is_http)
	{
		SOCKET s = socket_info->sock;
		socket_info->sock = -1;
		if (s != -1)
			res = _close(s);
	}
	else
	{
		res = HttpClose(socket_info->http, clean_shutdown);
	}
    sleep_millis(200);

	sockets[net_socket] = NULL;
	free(socket_info);

	return res;
}

void net_set_nonblock(SOCKET net_socket, int act)
{
	SOCKETINFO *socket_info = find_socket(net_socket);

	if (socket_info == NULL)
		return;

	if (!socket_info->is_http)
	{
	    _set_nonblock(socket_info->sock, act);
	}
	else
	{
		HttpSetNonBlock(socket_info->http, act != 0);
	}
}

// If you change the hunt sequence (esp. to remove an entry), change version also
#define HUNT_VERSION 2

HUNTINFO normal_map[] =	// N
{
	{ -1, CONNECT_DIRECT },		/* default service port, skipped if use primary port is off */
	{ 1270, CONNECT_DIRECT },	/* webex port */
	{ 443, CONNECT_DIRECT },
	{ 443, CONNECT_PROXY },
	{ 80, CONNECT_HTTP },		/* use WinInet */
	{ 0, CONNECT_DIRECT }
};

HUNTINFO proxy_only_map[] =	// P
{
	{ 443, CONNECT_PROXY },
	{ 80, CONNECT_HTTP },		/* if proxy is configured, WinInet should find and use */
	{ 0, CONNECT_DIRECT }
};

HUNTINFO http_only_map[] = // H
{
	{ 80, CONNECT_HTTP },		/* if proxy is configured, WinInet should find and use */
	{ 0, CONNECT_DIRECT }
};


static BOOL config_read = FALSE;
static int last_attempt = -1;
static int last_time = 0;
static char last_map = 'X';
static int last_success = 0;	// number of seconds to retry port after successful connection

const char *get_my_address()
{
	char name[256] = "";
	static char ip_addr[16] = "";

	if (gethostname(name, sizeof(name)) == 0)
	{
		struct hostent *host = gethostbyname(name);
		if(host != NULL)
		{
			strcpy(ip_addr, inet_ntoa(*(struct in_addr *)host->h_addr));
		}
	}

	return ip_addr;
}

BOOL read_config(char *last_map, int *last_pos, int *last_time)
{
	int vers, pos, time;
	char map;
	char ip[16];
	FILE *f;

	f = fopen(g_szProxyFileName, "r");
	if (f != NULL)
	{
		if (fscanf(f, "%d %16s %c %d %d", &vers, ip, &map, &pos, &time) == 5)
		{
			if (strcmp(ip, get_my_address()) == 0 && vers == HUNT_VERSION)
			{
				*last_pos = pos;
				*last_time = time;
				*last_map = map;
				fclose(f);
				infof("Read connection config for %s: %c %d", ip, map, pos);
				return TRUE;
			}
		}
		fclose(f);
	}
	return FALSE;
}

void write_config(char map, int pos, int time)
{
	FILE *f;

	f = fopen(g_szProxyFileName, "w");
	if (f != NULL)
	{
		fprintf(f, "%d %s %c %d %d\n", HUNT_VERSION, get_my_address(), map, pos, time);
		fclose(f);
		infof("Wrote connection config: %c %d", map, pos);
	}
}

// Resolve server name.
BOOL net_resolve_hostname(const char *server, struct sockaddr_in *sin)
{
	struct hostent *host;

    debugf("Resolving hostname %s", server);
	if ((sin->sin_addr.s_addr = inet_addr(server)) != INADDR_NONE)
	{
		sin->sin_family = AF_INET;
	}
	else
	{
		host = gethostbyname(server);
		if(host != NULL)
		{
			memcpy(&sin->sin_addr, host->h_addr, host->h_length);
			sin->sin_family = host->h_addrtype;
		}
		else
		{
			/* Perhaps WinInet can resolve name for use...we can't */
			errorf("Unable to resolve hostname %s", server);
			return FALSE;
		}
	}
	return TRUE;
}

// enable or disable nagle algorithm
void net_set_nagle_algorithm(SOCKET sock, BOOL enable)
{
	int nodelayval = enable?0:1;

	// Disable Nagle algorithm
	if (enable_nagle_algorithm!=enable && ((int)sock)>0) {
		SOCKETINFO *socket_info = find_socket(sock);
		_set_nodelay(socket_info->sock, nodelayval);
	}

	enable_nagle_algorithm = enable;
}

// Remove hunting configuration and reset to initial hunting state.
void net_reset_hunt()
{
	unlink(g_szProxyFileName);
	last_attempt = -1;
	last_time = 0;
	last_map = 'X';
	last_success = 0;
}

void net_enable_primary_port(BOOL enable)
{
	enable_primary_port = enable;
}

void net_enable_port_hunting(BOOL enable)
{
	enable_port_hunting = enable;
}

// The net_hunt() won't hunt to next port if the calling process is restarted
// since the process may exit on connection errors, it may cause endless loop.
// call this function to manually set the hunt port, so that the restarted process can
// still keep hunting, and avoid stucking at a certain port
int net_set_next_hunt_port()
{
	int pos = last_attempt + 1;
	HUNTINFO * map;
	switch(last_map)
	{
	case 'P':
	{
		map = proxy_only_map;
		break;
	}
	case 'H':
	{
		map = http_only_map;
		break;
	}
	default:
		map = normal_map;
		break;
	}

	if (map[pos].port == 0)
	{
		pos = 0;
		if (!enable_primary_port && map[pos].port == -1)
			pos ++;
	}

	last_attempt = pos;
	last_time = time(NULL);

	write_config(last_map, last_attempt, last_time);

	errorf("Skipping port due to error (%d)", pos);

	return 0;
}

// Write out current configuration as successful.
// Call after protocol handshake indicates we've probably got a good connection.
void net_set_connected()
{
	infof("Protocol active, saving configuration (%c %d)", last_map, last_attempt);

	// Update stored config
	write_config(last_map, last_attempt, last_time);

	// Reset retry counter--if we fail after this point, we want to retry
	// same port
	last_success = RETRY_AFTER_CONNECT;
}

void net_set_retry_last()
{
	infof("Retry same port, saving configuration (%c %d)", last_map, last_attempt);

	// Update stored config
	write_config(last_map, last_attempt, last_time);

	// Reset retry counter--if we fail after this point, we want to retry
	// same port
	if (last_success <= 0)
        last_success = 1;
}

// Search through available connection mechanisms to find route to server.
// If connection succeeds, caller should also call net_set_connected so the
// same port is used for the next connection.
int net_hunt(SOCKET net_socket, const char *server, int default_port, BOOL proxy_only)
{
	SOCKETINFO *socket_info = find_socket(net_socket);
	HUNTINFO * map;
	struct sockaddr_in sin;
	struct sockaddr *addr = (struct sockaddr *)&sin;
	BOOL resolved_name = FALSE;
	int err;
	int pos;
	int proxy_auth_err = 0;
	char map_flag;
    int now = time(NULL);

    if (!enable_port_hunting)
    {
        return net_hunt_disabled(net_socket, server, default_port);
    }

    debugf("net_hunt");

	if (socket_info == NULL)
		return -1;

	/* Figure out which hunt map to use and where to start */
	if (net_http_mode)
	{
		infof("HTTP-only option selected");
		map = http_only_map;
		map_flag = 'H';
		last_success = RETRY_AFTER_CONNECT;
	}
	else
	{
	    /* Even if a proxy is configured in the OS, we try the normal hunt sequence. */
		map = proxy_only==FALSE ? normal_map : proxy_only_map;
		map_flag = proxy_only==FALSE ? 'N' : 'P';

		infof("net_hunt - Using %c hunt map.", map_flag );

		if (!config_read)
		{
			/* Read config from last time we ran, or from other process running before us. */
			if (read_config(&last_map, &last_attempt, &last_time))
			{
				if (now - last_time < STORED_CONFIG_TIME)
				{
					/* Last time required HTTP only map, so switch this time also */
					if (last_map == 'H')
					{
						map = http_only_map;
						map_flag = 'H';
					}
					else if (last_map == 'P')
					{
						map = proxy_only_map;
						map_flag = 'P';
					}

					/* Force use of same strategy in test below */
					last_time = 0;
					last_success = RETRY_AFTER_CONNECT;
				}
				else
					last_attempt = -1;
			}
			config_read = TRUE;
		}
		else
		{
			/* We had switched to HTTP or proxy map last time, so keep using that map */
			if (last_success > 0)
			{
				if (last_map == 'H')
				{
					map = http_only_map;
					map_flag = 'H';
				}
				else if (last_map == 'P')
				{
					map = proxy_only_map;
					map_flag = 'P';
				}
			}
		}
	}

	/* Fallback to HTTP if we can't resolve server name and have no record of a
	   successful connection */
	if (map_flag != 'H')
	{
		resolved_name = net_resolve_hostname(server, &sin);
		if (!resolved_name && last_success <= 0)
		{
			map = http_only_map;
			map_flag = 'H';
		}
	}

	/* If last connection seems to have been successful, try same strategy again */
	if (last_attempt >= 0 && map_flag == last_map && last_success > 0)
	{
		pos = last_attempt;
		if (map[pos].port == 0)
			pos = 0;
	}
	/* Otherwise, if last try failed, skip to next port (as long as we are still
	   using the same map) */
	else if (last_attempt >= 0 && map_flag == last_map && last_success <= 0)
	{
		pos = last_attempt + 1;
		if (map[pos].port == 0)
			pos = 0;
	}
	/* Otherwise stop from top */
	else
	{
		pos = 0;

		/* Don't use primary port unless enabled */
		if (!enable_primary_port && map[pos].port == -1)
			pos++;
	}

	while (map[pos].port)
	{
		int port = map[pos].port != -1 ? map[pos].port : default_port;

		if (socket_info->interrupt)
			return IERR_NET_FAILED;

		/* Store in globals, upper levels will re-create socket so we can't store there */
		last_attempt = pos;
		last_time = time(NULL);
		last_map = map_flag;

		/* Try to resolve the server name */
		err = 0;
		if (!resolved_name && map[pos].connect_type != CONNECT_HTTP)
		{
			resolved_name = net_resolve_hostname(server, &sin);
			if (!resolved_name)
				err = IERR_NET_HOSTNOTFOUND;
		}

		sin.sin_port = htons(port);

		if (!err)
		{
			switch (map[pos].connect_type)
			{
			case CONNECT_DIRECT:
				infof("Attempting direct connection to %s:%d", server, port);
				setup_socket(socket_info, FALSE);
				err = net_connect(net_socket, server, addr , sizeof(sin), TRUE);
				break;

			case CONNECT_PROXY:
				infof("Attempting connection to %s:%d via proxy", server, port);
				setup_socket(socket_info, FALSE);
				err = net_connect(net_socket, server, addr, sizeof(sin), FALSE);
				break;

			case CONNECT_HTTP:
				infof("Attempting HTTP connection to %s:%d", server, port);
                setup_socket(socket_info, TRUE);
                err = net_connect(net_socket, server, addr, sizeof(sin), FALSE);
				break;
			}
		}

		/* Decrement retry after success timer by number of seconds in this attempt */
		last_success -= time(NULL) - last_time;

		switch (err)
		{
		case IERR_NET_SUCCESS:
			infof("Connected.");
			// Caller should call net_set_connected() if this connection seems good
			last_success -= 2;	// guestimate how long we'll spend trying to send data
			return err;
		case IERR_NET_PROXYAUTHFAILED:
			errorf("Cannot connect to %s -- %s", server, "Proxy authentication failed.");
			last_attempt = -1;
			proxy_auth_err = err;
			break;
		case IERR_NET_PROXYAUTHREQUIRED:
			errorf("Cannot connect to %s -- %s", server, "Proxy required authentication.");
			last_attempt = -1;
			proxy_auth_err = err;
			break;
		default:
			errorf("Cannot connect to %s:%d [%d:%d]", server, port, err, CHECK_ERR);
			last_attempt = -1;
		}

		/* We keep retry on a "known good" connection multiple times before continuing
		   with the hunt process (but not if we need a password to continue) */
		if (last_success <= 0 || proxy_auth_err)
			pos++;
		else
		{
			last_success -= 2;
			sleep_millis(2000);
		}
	}

	/* ran out of alternatives */
	last_attempt = -1;
	return proxy_auth_err != 0 ? proxy_auth_err : IERR_NET_FAILED;
}

int net_hunt_disabled(SOCKET net_socket, const char *server, int default_port)
{
	SOCKETINFO *socket_info = find_socket(net_socket);
	struct hostent *host;
	struct sockaddr_in sin;
	struct sockaddr *addr = (struct sockaddr *)&sin;
	int err;

	if (socket_info == NULL)
		return -1;

	if ((sin.sin_addr.s_addr = inet_addr(server)) != INADDR_NONE)
	{
		sin.sin_family = AF_INET;
	}
	else
	{
		host = gethostbyname(server);
		if(host != NULL)
		{
			memcpy(&sin.sin_addr, host->h_addr, host->h_length);
			sin.sin_family = host->h_addrtype;
		}
		else
		{
			/* Perhaps WinInet can resolve name for use...we can't */
			errorf("Unable to resolve hostname %s", server);
			return -1;
		}
	}


	sin.sin_port = htons(default_port);

	infof("Attempting direct connection to %s:%d", server, default_port);
	setup_socket(socket_info, FALSE);
	err = net_connect(net_socket, server, addr , sizeof(sin), TRUE);

	return err;
}

// Set underlying network read/write timeouts.
// Timeout is in millis.
BOOL net_set_timeout(SOCKET socket, int timeout)
{
	SOCKETINFO *si = find_socket(socket);

	if (socket == -1)
		return FALSE;

	infof("Setting connection timeout to %d", timeout);

	if (!si->is_http)
	{
		if (si->sock != -1)
		{
			infof("Setting socket timeout to %d", timeout);
			if (setsockopt(si->sock, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout)) == SOCKET_ERROR)
			{
				return FALSE;
			}
			if (setsockopt(si->sock, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout, sizeof(timeout)) == SOCKET_ERROR)
			{
				return FALSE;
			}
		}
	}
	else
	{
		HttpSetTimeout(si->http, timeout);
	}

	return TRUE;
}

void net_set_thread_mode(BOOL _main_thread, void (*_polling_hook)())
{
    main_thread = _main_thread;
    polling_hook = _polling_hook;
}


int net_initialize(const char *config_path)
{
    strcpy(g_szProxyFileName, config_path);
    strcat(g_szProxyFileName, "/proxy.iic");

#if defined(LINUX) /*|| defined(MACOSX)*/
    proxy_init( );
#endif

    return 0;
}


