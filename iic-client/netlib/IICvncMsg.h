/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef	IICvncMsg_h
#define IICvncMsg_h

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __x86_64__
typedef unsigned int DWORD;
#else
typedef unsigned long DWORD;	// may be 64 bit
#endif
typedef void *PVOID;

typedef unsigned short USHORT;
typedef unsigned int UINT;
typedef unsigned int *UINT_PTR;
typedef long *LONG_PTR;
typedef unsigned int WPARAM;
typedef LONG_PTR LPARAM;

typedef int HWND;
typedef char * LPCTSTR;

typedef long LONG;
typedef struct tagPOINT
{
    LONG x;
    LONG y;
} POINT, *PPOINT;


/*	windows message compatibility defs */

/*
 * Window Messages
 */

#define WM_CREATE                       0x0001
#define WM_DESTROY                      0x0002
#define WM_MOVE                         0x0003
#define WM_SIZE                         0x0005
#define WM_CLOSE                        0x0010
#define	WM_COPYDATA                     0x004A	// 74

/*
 * lParam of WM_COPYDATA message points to...
 */
typedef struct tagCOPYDATASTRUCT {
    DWORD dwData;
    DWORD cbData;
    PVOID lpData;
} COPYDATASTRUCT, *PCOPYDATASTRUCT;


// Callback event codes
#ifndef SHAREAPI_H
enum {
	CB_Connected = 0,
	CB_Disconnected,
	CB_ConnectFailed,
	CB_SessionEnded,
	CB_InternalError,
	CB_Connecting,      // Viewer
    CB_GetEncryptKey,
	CB_Max
};
#endif

// This will go after I switch to existing one.
typedef enum {              // COPYDATASTRUCT dwData field = operation code
    SetPassword = 0,        // use existing codes
    SetPorts,
    SetCallbackInfo,
    ShareAddlApps,
    EnableDisableTransparent,
    SetPwrPointWndHandle,
    SetColorDeduction,
    EnableDisableRecording,
    ShareAllInstances,
    AddClientByName,        // 9, skipped in CDC_<op> version ?
    SetAgentMode,
    SetDebugMode,
    SetNagleAlgorithm,
    GetEncryptKey,
} CopyDataOpCodeT;

enum {
	CDC_SetPassword,
	CDC_SetPorts,
	CDC_Setcallbackinfo,
	CDC_ShareApplications,
	CDC_EnableTransparent,
	CDC_SetPPTHWND,
	CDC_ReduceColor,
	CDC_EnableRecording,
	CDC_AllApplicationInstances,
	// 9 is add client by name ?
	CDC_SetAgentMode = 10,
	CDC_SetDebugMode = 11,
	CDC_SetNagleAlgorithm = 12,
    CDC_GetEncryptKey = 13,
};

typedef struct ClientInfo { USHORT port; char name[256]; char key[256]; } ClientInfoT;  // used with AddClientByName


typedef enum {
    IIC_Meeting = 2,        // skip 1 so we don't get accidental match
    IIC_vncServer,
    IIC_vncViewer
} IIC_ID;  // window IDs
typedef int IIC_id;         // contains IIC_ID but want to allow conversion from int

//  queue name, use 'm' for char.  One queue and id indicates dest.
// id provided to open_queue() or create_queue() determines what messages will be got,
//  to field of send_message() sends to that id.

#define IIC_MeetingQ    "IIC_MeetinQ"
#define IIC_MeetingChar 'm'

typedef struct CallbackInfo {
    IIC_id  callback;       //HWND callback;
    UINT msg;
    char proxyAuthName[64];
    char proxyPassword[32];
    char meetingId[65];
    char sessiontoken[128];
    char participantId[128];
}   CallbackInfoT;

void MsgQueueInit(IIC_id IICid);
void MsgQueueCreate(IIC_id IICid);          // called by top level program, clears queue by create
void MsgQueueOpen(IIC_id IICid);

// I could use a macro GetMessage(a,b,c,d)  GetMessage(&a,&b,&c,&d)
void GetMessage(IIC_id *id, DWORD *wmCmd, DWORD *wParam, void **lParam);
int PostMessage(IIC_id id, DWORD wmCmd, DWORD wParam, void *lParam);

void DelMessage(DWORD wmCmd, DWORD wParam, void*lParam);

unsigned int RegisterWindowMessage(const char *pString);
#define RegisterWindowMessageA RegisterWindowMessage
#define SendMessage PostMessage

// externs for C code to use since C compile won't allow
// uint msgT = RegisterWindowMessage(<text>);
 extern const UINT msgConnectError;
 extern const UINT msgViewerInternalError;
 extern const UINT msgNetworkError;
 extern const UINT msgViewerCreated;
 extern const UINT msgEnableRemoteControl;
 extern const UINT msgEnableFullScreen;
 extern const UINT msgViewerDestroyed;
 extern const UINT msgObtainedControl;
 extern const UINT msgCloseSession;
 extern const UINT msgEnableDebugMode;
 extern const UINT msgGetEncryptKey;

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
