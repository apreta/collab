/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

/*
    IICvncMsgs  module to provide Linux replacement for Windows SendMessage and receipt of messages.
    supports WM_COPYDATA msg, =code 74, using COPYDATASTRUCT, including what that points to.
    IICvncMsg.h supplies the defines and structs needed, mostly as in winuser.h
    RegisterWindowsMessage() returns an int code based on table index for already definted messages,
    it could easily be made to handle new messages and be machine global.
    #define TEST_MAIN to include a test main() to list the messages and test the messaging.
    To get messages to right destination, use :
    1 queue, name IIC_Meeting, using "ICEcorps" as the id string.
    3 window Ids: IIC_Meeting, IIC_vncServer, IIC_vncViewer
    table of the messages we send between them

    PostMessage() checks if wmCmd == WM_COPYDATA and sends a copy of COPYDATASTRUCT and what it points to.

    Usage:

    Top level or parent program calls
    MsgQueueCreate(dest id)
    Lower level or child program calls
    MsgQueueOpen(dest id)

    PostMessage(dest id, wmCmd, wParam, lParam)

    GetMessage(from-id, wmCmd, wParam, lParam)
    if lParam not null, when done with what it points to:
    DelMessage(lParam)

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "IICvncMsg.h"
#include "queue.h"

#ifndef linux
#define	LogDebug printf
#define	CHECK_DUPES
#endif

#ifdef TEST_MAIN
#include <unistd.h>         // for fork()
#endif

// use printf for LogDebug
#define LogDebug printf
//#define TEST_MAIN         // originally only handled RegisterWindowMessage(), not testing messaging yet.

#define	_T(a)	a
#define RegisterWindowMessageA	RegisterWindowMessage

typedef unsigned int UINT;

static	bool	QueueOpened = false;
static MsgQueue *pMQ = NULL;


#ifdef __cplusplus
extern "C" {
#endif

// MsgQueueCreate called by top level program to create the queue
void MsgQueueCreate(IIC_id IICid)
{
    int success = -2;
    if(pMQ != NULL)
    {
        // Read and dump messages in queue
        int From, MsgSize, Command;
        while (pMQ->get_message(From, MsgSize, Command, false) == 0)
            ;

        return;
    }
    pMQ = new MsgQueue(IIC_MeetingQ);
    success = pMQ->create_queue(IICid, IIC_MeetingChar);
    QueueOpened = true;
}

// MsgQueueOpen called by child programs to open an existing queue
void MsgQueueOpen(IIC_id IICid)
{
    int success = -2;
    if(pMQ != NULL)
    {
        // Read and dump messages in queue
        int From, MsgSize, Command;
        while (pMQ->get_message(From, MsgSize, Command, false) == 0)
            ;

        return;
    }
    pMQ = new MsgQueue(IIC_MeetingQ);
    success = pMQ->open_queue(IICid, IIC_MeetingChar);
    QueueOpened = true;
}


/* COPYDATASTRUCT used for WM_COPYDATA msg
  Not good to look inside what's being sent, BUT if wmCmd == WM_COPYDATA and lParam != 0
  then it's a COPYDTASTRUCT *
  If so then grab what it points to, lpData[cbData], add to message and pass along for other end to point to
  To get COPYDATASTRUCT across, ptrs aren't meaningfull, be careful about them!
*/

// example calls:
//PostMessage(dest, 0, CB_Connected, 0L)
//PostMessage(dest, WM_COPYDATA, 0, &copydatastruct)    and copydatastruct points to lpData[cbData]

// Don't need separate SendMessage()

typedef struct { DWORD Params[2]; COPYDATASTRUCT cds; char Rest[]; } CopyDataMsgT;

#define COPYDATA_OFFSET ((int)(long)&(((CopyDataMsgT*)0)->cds))

int	PostMessage(/*int wnd*/ IIC_id id, DWORD wmCmd, DWORD wParam, void *lParam)
{
    if(QueueOpened)
    {
        // Handle COPYDATASTRUCT, copy the struct plus what it points to
        if((wmCmd == WM_COPYDATA) && (lParam != 0))
        {
            COPYDATASTRUCT * pCDS = (COPYDATASTRUCT*)lParam;
            int MsgSize = sizeof(CopyDataMsgT) + pCDS->cbData;
            char CopyMsg[MsgSize];
            CopyDataMsgT *pCDM = (CopyDataMsgT *)CopyMsg;
            pCDM->Params[0] = wParam;
            pCDM->Params[1] = (DWORD)(long)lParam;           // meaningless (dangerous) if not 0, keep for consistency
            pCDM->cds = *pCDS;                      // copy the struct
            memcpy(&pCDM->Rest[0],pCDS->lpData,pCDS->cbData);
            pMQ->send_message((int)id, (char*)pCDM, MsgSize, wmCmd);
        }
        else
        {   // short message
            DWORD Msg[2];
            Msg[0] = wParam;
            Msg[1] = (DWORD)(long)lParam;    // probably 0 but maybe not
            pMQ->send_message((int)id, (char*)Msg, sizeof(Msg), wmCmd);
        }
    }
    return 0;
}


void GetMessage(IIC_id *idFrom, DWORD *wmCmd, DWORD *wParam, void **lParam)
{
    int Success, MsgSize, Command;
    int From;

    Success = pMQ->get_message(From, MsgSize, Command);

    CopyDataMsgT *pCDM = (CopyDataMsgT*) calloc(1, MsgSize);

    Success = pMQ->get_message_data((char*)pCDM, MsgSize);

    *wmCmd = Command;
    *wParam = pCDM->Params[0];
    *lParam = (void*)pCDM->Params[1];
    *idFrom = (IIC_id)From;

    // if wmCmd is WM_COPYDATA and lParam not null then we have a COPYDATASTRUCT and data it points to.
    //&Msg[2] pts to copydatastruct, &Msg[5] pts to Rest[]
    // Keep the copydatastruct and Rest[] in Msg, simpler and more efficient, just set the pointers
    // only trick is correcting the pointer when deleting mem in DelMessage()
    if((*wmCmd == WM_COPYDATA)  && (*lParam != NULL))
    {
        // make sure msg size has room for copydatastruct and what it points to
        // Note:  seems like a good idea, but doesn't seem to work for the 1 byte messages we send
        if(MsgSize < sizeof(CopyDataMsgT))
        {
            LogDebug("GetMessage ERROR; msg too small for COPYDATASTRUCT, MsgSize=%d\n", MsgSize);
			*lParam = NULL;
			free(pCDM);
		}
        else
        {
            COPYDATASTRUCT *pCDS = &pCDM->cds;
            pCDS->lpData = pCDM->Rest;
            *lParam = pCDS;                     // pass ptr to COPYDATASTRUCT back to caller, it has ptr to Rest[]
            //printf("GetMsg wmCmd:%d, wParam:%d, lParam:%08X\n",*wmCmd, *wParam, *lParam);
        }
    }
    else
    {
        // IF lParam is supposed to point to anything other than COPYDATASTRUCT, HANDLE IT
        free(pCDM);
    }
}

// Since we give caller of GetMessage() pointer into Msg[], pointing at copydatastruct, have to corfect the
//  ptr before deleting the mem.
void DelMessage(DWORD wmCmd, DWORD wParam, void *lParam)
{
    if(wmCmd == WM_COPYDATA && lParam != NULL)
    {
    	free((char*)lParam - COPYDATA_OFFSET);
	}
}

//	Windows has UINT RegisterWindowMessage(LPCTSTR lpString);


// initially 26 entries, from searching code
// Changing, need rest of msgs also.
const char *IICvncMsgTable[] = {
 "unused",								// so 0 isn't valid index
 "IICAppShareCallback",
 "IICServer.Callback",
 "IICServer.Properties.SetPassword",
 "IICServer.AddClient.Message",
 "IICServer.RemoveClients.Message",

 "IICViewer.Refresh",
 "IICViewer.ConnectError",
 "IICViewer.InternalError",
 "IICViewer.NetworkError",
 "IICViewer.WindowCreated",
 "IICViewer.EnableRemoteControl",
 "IICViewer.EnableFullScreen",
 "IICViewer.WindowDestroyed",
 "IICViewer.ObtainedControl",
 "IICViewer.CloseSession",
 "IICViewer.EnableDebugMode",
 "IICViewer.GetEncryptKey",
};

int	IICvncMsgTableSize = sizeof(IICvncMsgTable) / sizeof(char *);
const int vncMsgOffset = 	0xC000;								// on Windows user msgs start at this value

// Try putting msg values here
 //UINT msgServerError = RegisterWindowMessage(_T("IICServerError"));
 UINT msgCallbackInfo = RegisterWindowMessage("IICAppShareCallback");
 UINT msgServerCallback                = RegisterWindowMessage(_T("IICServer.Callback"));
 UINT CALLBACK_MSG             = RegisterWindowMessageA("IICServer.Callback");
 UINT MENU_SETPASSWORD_MSG    = RegisterWindowMessageA("IICServer.Properties.SetPassword"); // IIC
 UINT MENU_ADD_CLIENT_MSG     = RegisterWindowMessageA("IICServer.AddClient.Message");
 UINT MENU_REMOVE_CLIENTS_MSG = RegisterWindowMessageA("IICServer.RemoveClients.Message");  // REalVNc 336

 //UINT msgViewExitFullScreen	= (msgAppBase + 1);	//::RegisterWindowMessage(_T("IICViewExitFullScreen"));
 //UINT msgVNCRefresh			= (msgAppBase + 5);	//::RegisterWindowMessage(_T("IICViewer.Refresh"));
 const UINT msgVNCRefresh			= RegisterWindowMessage(_T("IICViewer.Refresh"));
 const UINT msgConnectError					= RegisterWindowMessage(_T("IICViewer.ConnectError"));
 const UINT msgViewerInternalError           = RegisterWindowMessage(_T("IICViewer.InternalError"));
 const UINT msgNetworkError					= RegisterWindowMessage(_T("IICViewer.NetworkError"));
 const UINT msgViewerCreated					= RegisterWindowMessage(_T("IICViewer.WindowCreated"));
 const UINT msgEnableRemoteControl			= RegisterWindowMessage(_T("IICViewer.EnableRemoteControl"));
 const UINT msgEnableFullScreen				= RegisterWindowMessage(_T("IICViewer.EnableFullScreen"));
 const UINT msgViewerDestroyed				= RegisterWindowMessage(_T("IICViewer.WindowDestroyed"));
 const UINT msgObtainedControl				= RegisterWindowMessage(_T("IICViewer.ObtainedControl"));
 const UINT msgCloseSession					= RegisterWindowMessage(_T("IICViewer.CloseSession"));
 const UINT msgEnableDebugMode				= RegisterWindowMessage(_T("IICViewer.EnableDebugMode"));
 const UINT msgGetEncryptKey				= RegisterWindowMessage(_T("IICViewer.GetEncryptKey"));


unsigned int RegisterWindowMessage(const char *pString)
{
    int Index = 0;
	for(int i=1; i<IICvncMsgTableSize; i++)
	{
		if(0 == strcmp(IICvncMsgTable[i],pString))
		{
			Index = i;
			break;
		}
	}
#ifdef	CHECK_DUPES
	// LogDebug("RegWinMsg[%d] = '%s'\n",Index,pString);
	for(int j=Index+1; j<IICvncMsgTableSize; j++)
		if(0 == strcmp(IICvncMsgTable[j],pString))
			LogDebug("\a RegWinMsg DUPE [%d]='%s'\n",j,pString);
#endif
	if(Index == 0)												// error, log it
    {
		LogDebug("RegisterWindowMessage error - unknown msg: '%s'\n",pString);
		return(0);
    }
	else
		return(Index + vncMsgOffset);
}

#ifdef __cplusplus
}   // extern "C"
#endif

#ifdef  TEST_MAIN

int main(int argc, char *argv[])
{
	printf("Table size %d  includes unused[0]\n",IICvncMsgTableSize);

	RegisterWindowMessage("no match");

// Test messaging, using 2 threads
#define NUM_MESSAGES    1000

IIC_id id = IIC_Meeting, From;
unsigned int wmCmd, wParam;
void *lParam;
MsgQueueCreate(id);
// spawn thread for sender
int ForkRet = fork();
if(ForkRet == 0)
{   //child
    IIC_id to=IIC_Meeting;
    for(int s=0; s<NUM_MESSAGES; s++)    // send 4 messages, 300 times
    {
        PostMessage(to, WM_COMMAND, CB_Connected, 0L);
        uint msgT = RegisterWindowMessage("IICAppShareCallback");
        PostMessage(to, msgT, CB_Connected, 0L);

        ClientInfoT client = { 2182 };
        //COPYDATASTRUCT cds = {(long unsigned int*)9, sizeof(client), &client};
        COPYDATASTRUCT cds = {9, sizeof(client), &client};
        PostMessage(to, WM_COPYDATA, 0, &cds);
        PostMessage(to, WM_COPYDATA, 0, 0); //&cds);    // good test althoug illegal
    }
}
else if(ForkRet == -1)
{   // error
    printf("ERROR, couldn't fork()\n");
}
else
{   // parent

    for(int r=0; r<(NUM_MESSAGES*4); r++)
    {
        GetMessage(&From,&wmCmd,&wParam,&lParam);
        printf("GotMessage From:%d wmCmd:%d wParam:%d lParam:%08X; ",From,wmCmd,wParam,lParam);
        if(lParam != NULL)
        {
            COPYDATASTRUCT *p = (COPYDATASTRUCT*)lParam;
            printf(" dwData %d, cbData %d, lpData%08X, ",p->dwData,p->cbData,p->lpData);
            printf(" port: %d; ",((ClientInfoT*)(p->lpData))->port);
            DelMessage(wmCmd,wParam,lParam);
        }
        printf("\tRECEIVED %d\n",r+1);
    }
}
}   // end fork parent

#endif	//def TEST_MAIN


