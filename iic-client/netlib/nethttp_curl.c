/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

// nethttp.c: implementation of our cross-platform HTTP API.
//    The Windows implementation (nethttp_win32.c) uses WinINet while the Linux
//    implementation uses LibCurl
//
//////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <openssl/ssl.h>


#include "net.h"
#include "nethttp.h"
#include "log.h"
#include "proxy.h"


/** Cache proxy servers detected for each server accessed */
static PROXY *proxy_list = NULL;

int         giMaxBytes2Send      = 1024;
int         giAuthCount;

/** The last error code returned from the curl API */
int g_last_curl_error = CURLE_OK;

/** The last error text returned from the curl API */
char g_last_curl_error_text[CURL_ERROR_SIZE+1];

/** openssl locking globals */
static pthread_mutex_t *g_lockarray;

/** A simple mapping between curl errors and out own brand of error */
typedef struct curl_to_conf_map
{
  CURLcode curlCode;
  int confCode;
}CURL_TO_CONF_MAP;

static CURL_TO_CONF_MAP curl_to_conf[] = {
  { CURLE_OK, 0 },
  { CURLE_UNSUPPORTED_PROTOCOL,          ERROR_INTERNET_PROTOCOL_NOT_FOUND },  /* 1 */
  { CURLE_FAILED_INIT,                   INTERNET_ERROR_BASE },                /* 2 */
  { CURLE_URL_MALFORMAT,                 ERROR_INTERNET_INVALID_URL },            /* 3 */
  { CURLE_URL_MALFORMAT_USER,            INTERNET_ERROR_BASE },               /* 4 (NOT USED) */
  { CURLE_COULDNT_RESOLVE_PROXY,   INTERNET_ERROR_BASE },               /* 5 */
  { CURLE_COULDNT_RESOLVE_HOST,    INTERNET_ERROR_BASE },               /* 6 */
  { CURLE_COULDNT_CONNECT,         INTERNET_ERROR_BASE },               /* 7 */
  { CURLE_FTP_WEIRD_SERVER_REPLY,  INTERNET_ERROR_BASE },               /* 8 */
  { CURLE_FTP_ACCESS_DENIED,       INTERNET_ERROR_BASE },               /* 9 a service was denied by the FTP server due to lack of access - when login fails this is not returned. */
  { CURLE_FTP_USER_PASSWORD_INCORRECT, INTERNET_ERROR_BASE },           /* 10 */
  { CURLE_FTP_WEIRD_PASS_REPLY,    INTERNET_ERROR_BASE },               /* 11 */
  { CURLE_FTP_WEIRD_USER_REPLY,    INTERNET_ERROR_BASE },               /* 12 */
  { CURLE_FTP_WEIRD_PASV_REPLY,    INTERNET_ERROR_BASE },               /* 13 */
  { CURLE_FTP_WEIRD_227_FORMAT,    INTERNET_ERROR_BASE },               /* 14 */
  { CURLE_FTP_CANT_GET_HOST,       INTERNET_ERROR_BASE },               /* 15 */
  { CURLE_FTP_CANT_RECONNECT,      INTERNET_ERROR_BASE },               /* 16 */
  { CURLE_FTP_COULDNT_SET_BINARY,  INTERNET_ERROR_BASE },               /* 17 */
  { CURLE_PARTIAL_FILE,            INTERNET_ERROR_BASE },               /* 18 */
  { CURLE_FTP_COULDNT_RETR_FILE,   INTERNET_ERROR_BASE },               /* 19 */
  { CURLE_FTP_WRITE_ERROR,         INTERNET_ERROR_BASE },               /* 20 */
  { CURLE_FTP_QUOTE_ERROR,         INTERNET_ERROR_BASE },               /* 21 */
  { CURLE_HTTP_RETURNED_ERROR,     INTERNET_ERROR_BASE },               /* 22 */
  { CURLE_WRITE_ERROR,             INTERNET_ERROR_BASE },               /* 23 */
  { CURLE_MALFORMAT_USER,          INTERNET_ERROR_BASE },               /* 24 - NOT USED */
  { CURLE_FTP_COULDNT_STOR_FILE,   INTERNET_ERROR_BASE },               /* 25 - failed FTP upload */
  { CURLE_READ_ERROR,              INTERNET_ERROR_BASE },               /* 26 - could open/read from file */
  { CURLE_OUT_OF_MEMORY,           INTERNET_ERROR_BASE },               /* 27 */
  { CURLE_OPERATION_TIMEOUTED,     ERROR_INTERNET_TIMEOUT },            /* 28 - the timeout time was reached */
  { CURLE_FTP_COULDNT_SET_ASCII,   INTERNET_ERROR_BASE },               /* 29 - TYPE A failed */
  { CURLE_FTP_PORT_FAILED,         INTERNET_ERROR_BASE },               /* 30 - FTP PORT operation failed */
  { CURLE_FTP_COULDNT_USE_REST,    INTERNET_ERROR_BASE },               /* 31 - the REST command failed */
  { CURLE_FTP_COULDNT_GET_SIZE,    INTERNET_ERROR_BASE },               /* 32 - the SIZE command failed */
  { CURLE_HTTP_RANGE_ERROR,        INTERNET_ERROR_BASE },               /* 33 - RANGE "command" didn't work */
  { CURLE_HTTP_POST_ERROR,         INTERNET_ERROR_BASE },               /* 34 */
  { CURLE_SSL_CONNECT_ERROR,       INTERNET_ERROR_BASE },               /* 35 - wrong when connecting with SSL */
  { CURLE_BAD_DOWNLOAD_RESUME,     INTERNET_ERROR_BASE },               /* 36 - couldn't resume download */
  { CURLE_FILE_COULDNT_READ_FILE,  INTERNET_ERROR_BASE },               /* 37 */
  { CURLE_LDAP_CANNOT_BIND,        INTERNET_ERROR_BASE },               /* 38 */
  { CURLE_LDAP_SEARCH_FAILED,      INTERNET_ERROR_BASE },               /* 39 */
  { CURLE_LIBRARY_NOT_FOUND,       INTERNET_ERROR_BASE },               /* 40 */
  { CURLE_FUNCTION_NOT_FOUND,      INTERNET_ERROR_BASE },               /* 41 */
  { CURLE_ABORTED_BY_CALLBACK,     INTERNET_ERROR_BASE },               /* 42 */
  { CURLE_BAD_FUNCTION_ARGUMENT,   INTERNET_ERROR_BASE },               /* 43 */
  { CURLE_BAD_CALLING_ORDER,       INTERNET_ERROR_BASE },               /* 44 - NOT USED */
  { CURLE_INTERFACE_FAILED,        INTERNET_ERROR_BASE },               /* 45 - CURLOPT_INTERFACE failed */
  { CURLE_BAD_PASSWORD_ENTERED,    INTERNET_ERROR_BASE },               /* 46 - NOT USED */
  { CURLE_TOO_MANY_REDIRECTS ,     INTERNET_ERROR_BASE },               /* 47 - catch endless re-direct loops */
  { CURLE_UNKNOWN_TELNET_OPTION,   INTERNET_ERROR_BASE },               /* 48 - User specified an unknown option */
  { CURLE_TELNET_OPTION_SYNTAX ,   INTERNET_ERROR_BASE },               /* 49 - Malformed telnet option */
  { CURLE_OBSOLETE,                INTERNET_ERROR_BASE },               /* 50 - NOT USED */
  { CURLE_SSL_PEER_CERTIFICATE,    INTERNET_ERROR_BASE },               /* 51 - peer's certificate wasn't ok */
  { CURLE_GOT_NOTHING,             INTERNET_ERROR_BASE },               /* 52 - when this is a specific error */
  { CURLE_SSL_ENGINE_NOTFOUND,     INTERNET_ERROR_BASE },               /* 53 - SSL crypto engine not found */
  { CURLE_SSL_ENGINE_SETFAILED,    INTERNET_ERROR_BASE },               /* 54 - can not set SSL crypto engine as default */
  { CURLE_SEND_ERROR,              INTERNET_ERROR_BASE },               /* 55 - failed sending network data */
  { CURLE_RECV_ERROR,              INTERNET_ERROR_BASE },               /* 56 - failure in receiving network data */
  { CURLE_SHARE_IN_USE,            INTERNET_ERROR_BASE },               /* 57 - share is in use */
  { CURLE_SSL_CERTPROBLEM,         INTERNET_ERROR_BASE },               /* 58 - problem with the local certificate */
  { CURLE_SSL_CIPHER,              INTERNET_ERROR_BASE },               /* 59 - couldn't use specified cipher */
  { CURLE_SSL_CACERT,              ERROR_INTERNET_INVALID_CA },         /* 60 - problem with the CA cert (path?) */
  { CURLE_BAD_CONTENT_ENCODING,    INTERNET_ERROR_BASE },               /* 61 - Unrecognized transfer encoding */
  { CURLE_LDAP_INVALID_URL,        INTERNET_ERROR_BASE },               /* 62 - Invalid LDAP URL */
  { CURLE_FILESIZE_EXCEEDED,       INTERNET_ERROR_BASE },               /* 63 - Maximum file size exceeded */
  { CURLE_FTP_SSL_FAILED,          INTERNET_ERROR_BASE },               /* 64 - Requested FTP SSL level failed */
  { CURLE_SEND_FAIL_REWIND,        INTERNET_ERROR_BASE },               /* 65 - Sending the data requires a rewind that failed */
  { CURLE_SSL_ENGINE_INITFAILED,   INTERNET_ERROR_BASE },               /* 66 - failed to initialise ENGINE */
  { CURLE_LOGIN_DENIED,            ERROR_INTERNET_LOGIN_FAILURE },      /* 67 - user, password or similar was not accepted and we failed to login */
  CURL_LAST,                     /* END OF LIST MARKER */
};

void SetLastCurlError( HREQUEST pReq, const char *pszContext );
bool ExtractCertificateInfo(HREQUEST pReq, X509 *cert, const char *pem_cert);

#ifdef DEBUG_LOG
void dump_string( const char *pszLabel, const char *s, unsigned long len )
{
  if( len == 0 && s != NULL)
  {
      len = strlen(s);
  }

  debugf("%s: %.*s", pszLabel, len, s);
}
#endif


//*****************************************************************************
int curl_handler_debugcallback (CURL *curl, curl_infotype type, char *pInfo, size_t infoSize, void *pUser)
{
  char szBuf[513];
  bool truncated = false;

  size_t alloc_size = infoSize;
  if( alloc_size > 512 )
    {
      truncated = true;
      alloc_size = 512;
    }

  memset(szBuf,0,513);
  strncpy( szBuf, pInfo, alloc_size );
  szBuf[alloc_size] = '\0';

  char *pszLabel = "";
  switch( type )
    {
    case CURLINFO_TEXT: //The data is informational text.
      pszLabel = "";
      break;
    case CURLINFO_HEADER_IN: //The data is header (or header-like) data received from the peer.
      pszLabel = "HDR IN";
      break;
    case CURLINFO_HEADER_OUT: //The data is header (or header-like) data sent to the peer.
      pszLabel = "HDR OUT";
      break;
    case CURLINFO_DATA_IN: //The data is protocol data received from the peer.
      pszLabel = "DATA IN";
      break;
    case CURLINFO_DATA_OUT: //The data is protocol data sent to the peer.
      pszLabel = "DATA OUT";
      break;
    default:
      pszLabel = "UNKNOWN";
      break;
    }
  dump_string( pszLabel, szBuf, alloc_size );
  if( truncated )
    {
      debugf( "NOTE: The actual data size was %d, only the first 512 bytes are shown.", infoSize );
    }
  return 0;
}

//*****************************************************************************
// Setup locking for OpenSSL
static void lock_callback(int mode, int type, char *file, int line)
{
    (void)file;
    (void)line;
    if (mode & CRYPTO_LOCK) {
        pthread_mutex_lock(&(g_lockarray[type]));
    }
    else {
        pthread_mutex_unlock(&(g_lockarray[type]));
    }
}

static unsigned long thread_id(void)
{
    unsigned long ret;
    
    ret=(unsigned long)pthread_self();
    return(ret);
}

static void init_locks(void)
{
    int i;
    
    g_lockarray=(pthread_mutex_t *)OPENSSL_malloc(CRYPTO_num_locks() *
                                                sizeof(pthread_mutex_t));
    for (i=0; i<CRYPTO_num_locks(); i++) {
        pthread_mutex_init(&(g_lockarray[i]),NULL);
    }
    
    CRYPTO_set_id_callback((unsigned long (*)())thread_id);
    CRYPTO_set_locking_callback((void (*)())lock_callback);
}

static void kill_locks(void)
{
    int i;
    
    CRYPTO_set_locking_callback(NULL);
    for (i=0; i<CRYPTO_num_locks(); i++)
        pthread_mutex_destroy(&(g_lockarray[i]));
    
    OPENSSL_free(g_lockarray);
}


//*****************************************************************************
void netInitializeLibrary()
{
    curl_global_init( CURL_GLOBAL_ALL );
    init_locks();
}


//*****************************************************************************
void netReleaseLibrary()
{
    curl_global_cleanup();
    kill_locks();
}


//*****************************************************************************
HSESSION netHttpOpen( const char *appName, int iTimeout, bool fUseNTLM )
{
    HSESSION sess = calloc(sizeof(SESSION), 1);
    sess->use_ntlm = fUseNTLM;
    sess->timeout = iTimeout;
    return sess;
}


//*****************************************************************************
bool netSetSecurityOptions( HSESSION aSession, const char *aCAFile)
{
    free(aSession->cafile);
    aSession->cafile = strdup(aCAFile);
#ifdef DEBUG_LOG
    debugf("CURLOPT_CAINFO set to %s", aCAFile);
#endif

    return true;
}


//*****************************************************************************
PROXY *find_proxy_info(const char *aUrl)
{
    // See if we've already looked up a proxy for this URL
    
    PROXY *proxy = proxy_list;
    while (proxy != NULL)
    {
        if (strcmp(proxy->url, aUrl) == 0)
            return proxy;
        proxy = proxy->next;
    }

    // Nope, create a new one (server name is blank if none are defined)

    HttpProxyInfo info;

    proxy = calloc(1, sizeof(PROXY));
    strcpy(proxy->url, aUrl);
    proxy->next = proxy_list;
    proxy_list = proxy;
    
    if (proxy_for_uri(aUrl, &info))
    {
        strcpy(proxy->server, info.host);
        strcpy(proxy->username, info.username);
        strcpy(proxy->password, info.password);
        proxy->port = info.port;
    }
        
    return proxy;
}


//*****************************************************************************
HCONNECTION netHttpConnect( HSESSION aSession, const char *aUrl, const char *aHost,
                        unsigned short aPort, const char *aName, const char *aPass )
{
#ifdef DEBUG_LOG
    debugf("entering netHttpConnect( )  --  aUrl is *%s*  --  aHost is *%s*", aUrl, aHost );
#endif

    HCONNECTION connect = calloc(sizeof(CONNECTION), 1);
    connect->session = aSession;
    connect->host = strdup(aHost);
    connect->port = aPort;
    connect->proxy_auth = 0;
    connect->auth = 0;

    if (aName && *aName && aPass && *aPass)
    {
        connect->auth = calloc(strlen(aName) + strlen(aPass) + 2, 1);
        sprintf(connect->auth, "%s:%s", aName, aPass);
    }

    // See if a proxy is configured at the OS level

    char url[512];
    sprintf(url, "%s://%s", aPort == 443 ? "https" : "http", aHost); 

    PROXY *proxy = find_proxy_info(url);
    connect->proxy = proxy;

    if (proxy && *proxy->server)
    {
        connect->proxy_auth = calloc(strlen(proxy->username) + strlen(proxy->password) + 2, 1);
        sprintf(connect->proxy_auth, "%s:%s", proxy->username, proxy->password);
    }
    
    return connect;
}


//*****************************************************************************
HREQUEST new_request( HCONNECTION aConn, const char *aVerb, unsigned long flags )
{
    // Allocate and prepare a request
    HREQUEST req = calloc(sizeof(REQUEST), 1);
    if( req )
    {
      req->connection = aConn;
      req->verb = aVerb ? strdup(aVerb):0;
      req->flags = flags;
      req->retCode = CURLE_OK;
      req->szErrorText[0] = '\0';
      req->m_pBUFFERRecv = 0;
      req->totalBytesReceived = 0;
      req->expectedContentLength = 0;
      req->fRequestComplete = false;
      req->statusCode = 0;
      req->statusCodeText = 0;
      req->m_pBUFFERSend = 0;
      req->sendBuffer = 0;
      req->sendLength = 0;
      pthread_mutex_init(&req->m_mutex, NULL);
    }

    return req;
}

void delete_request( HREQUEST req )
{
    if( req->verb )
    {
      free( req->verb );
      req->verb = 0;
    }

    if( req->statusCodeText )
    {
      free( req->statusCodeText );
      req->statusCodeText = 0;
    }

    pthread_mutex_destroy(&req->m_mutex);
    free( req );
}

void clear_request( HREQUEST req )
{
    if( req )
    {
      req->retCode = CURLE_OK;
      req->szErrorText[0] = '\0';
      req->m_pBUFFERRecv = 0;
      req->totalBytesReceived = 0;
      req->expectedContentLength = 0;
      req->fRequestComplete = false;
      req->statusCode = 0;
      req->statusCodeText = 0;
      req->m_pBUFFERSend = 0;
      req->sendBuffer = 0;
      req->sendLength = 0;
    }
}

int setup_curl( HREQUEST req )
{
    CURLcode ret;
    HCONNECTION connect = req->connection;
    HSESSION sess = connect->session;
    req->hcurl = curl_easy_init( );

#ifdef DEBUG_LOG
#ifdef DEBUG_LOG_VERBOSE
    curl_easy_setopt( req->hcurl, CURLOPT_VERBOSE, 1 );
    curl_easy_setopt( req->hcurl, CURLOPT_DEBUGFUNCTION, curl_handler_debugcallback );
#endif
#endif

    curl_easy_setopt( req->hcurl, CURLOPT_ERRORBUFFER, req->szErrorText );
    curl_easy_setopt( req->hcurl, CURLOPT_NOSIGNAL, (long)1 );

    if (sess->timeout  >  0)
    {
        //EXP1 No Timeout
        //curl_easy_setopt(req->hcurl, CURLOPT_TIMEOUT, (long)iTimeout);
    }

    curl_easy_setopt( req->hcurl, CURLOPT_CAINFO, sess->cafile);
    curl_easy_setopt( req->hcurl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_easy_setopt( req->hcurl, CURLOPT_SSL_VERIFYHOST, 0);

    if (connect->proxy && *connect->proxy->server)
    {
        debugf("CURLOPT_PROXY set to %s", connect->proxy->server);
        curl_easy_setopt(req->hcurl, CURLOPT_PROXY, connect->proxy->server);
        curl_easy_setopt(req->hcurl, CURLOPT_PROXYPORT, (long)connect->proxy->port);
        curl_easy_setopt(req->hcurl, CURLOPT_PROXYAUTH, (long)CURLAUTH_ANY);        
        ret = curl_easy_setopt(req->hcurl, CURLOPT_PROXYUSERPWD, connect->proxy_auth);
    }

#ifdef DEBUG_LOG
    debugf("CURLOPT_USERPWD = %s", connect->auth);
#endif
    ret = curl_easy_setopt(req->hcurl, CURLOPT_USERPWD, connect->auth);
    if( ret != CURLE_OK )
    {
#ifdef DEBUG_LOG
        debugf("CURLOPT_USERPWD %s Error: %d", connect->auth, (int)ret );
#endif
    }
       
    return 0;
}

//*****************************************************************************
HREQUEST netHttpOpenRequest( HCONNECTION aConn, const char *aVerb, const char *aObjName, const char *aVersion, unsigned long flags )
{
#ifdef DEBUG_LOG
  debugf("netHttpOpenRequest - Start");
  dump_string("        aVerb", aVerb, 0 );
  dump_string("  aConn->host", aConn->host, 0 );
  dump_string("     aObjName", aObjName, 0 );
  dump_string("     aVersion", aVersion, 0 );
#endif

    HREQUEST req = new_request( aConn, aVerb, flags );
    setup_curl(req);

    char *scheme = (flags & INTERNET_FLAG_SECURE) ? "https" : "http";

    req->url = calloc(strlen(aObjName) + strlen(aConn->host) + 16, 1);

    if( strncmp( aConn->host, "http://", 7 ) == 0 )
    {
      sprintf(req->url, "%s://%s%s", scheme, &aConn->host[7], aObjName );
    }else
    {
      sprintf(req->url, "%s://%s%s", scheme, aConn->host, aObjName);
    }

#ifdef DEBUG_LOG
    debugf( "netHttpOpenRequest: CURLOPT_URL = %s", req->url );
#endif

    curl_easy_setopt( req->hcurl, CURLOPT_CUSTOMREQUEST, NULL );

    if( strcmp( aVerb, "POST" ) == 0 )
    {
      curl_easy_setopt( req->hcurl, CURLOPT_POST, 1 );
    }else if( strcmp( aVerb, "GET" ) == 0 )
    {
      curl_easy_setopt( req->hcurl, CURLOPT_HTTPGET, 1 );
    }else if( strcmp( aVerb, "PUT" ) == 0 )
    {
      curl_easy_setopt( req->hcurl, CURLOPT_PUT, 1 );
    }else
    {
      curl_easy_setopt( req->hcurl, CURLOPT_CUSTOMREQUEST, req->verb );
    }

    req->retCode = curl_easy_setopt(req->hcurl, CURLOPT_URL, req->url);
    SetLastCurlError( req, "netHttpOpenRequest" );

#ifdef DEBUG_LOG
    debugf( "netHttpOpenRequest: Done" );
#endif
    return req;
}


//****************************************************************************
void SetLastCurlError( HREQUEST pReq, const char *pszContext )
{
    g_last_curl_error = pReq->retCode;
    if( pReq->retCode != CURLE_OK )
    {
      strcpy( g_last_curl_error_text, pReq->szErrorText );

      debugf( "%s Error: %d, %s", (pszContext) ? pszContext:"Curl", g_last_curl_error, g_last_curl_error_text );
    }
}


//*****************************************************************************
int GetLastError( )
{
  // Attempt to translate curl errors into our errors
  int curl_error = g_last_curl_error;
  int conf_error = 0;
  int i;
  for( i = 0; curl_to_conf[i].curlCode != CURL_LAST; ++i )
  {
    int iCurlError = (int)curl_to_conf[i].curlCode;
    if( iCurlError == curl_error )
      {
	conf_error = curl_to_conf[i].confCode;
	break;
      }
  }
  return conf_error;
}


//*****************************************************************************
bool netInternetGetLastResponseInfo( unsigned long *pdwError, char *szBuf, unsigned long *pdwLength )
{
    // TODO: implement
    debugf( "W A R N I N G !  netInternalGetLastResponseInfo is not implemented.");
    debugf( "entering netInternetGetLastResponseInfo( )" );
    return false;
}


//*****************************************************************************
bool netHttpEndRequest( HREQUEST pSMsg )
{
    // TODO: implement
    debugf( "W A R N I N G !  netHttpEndRequest is not implemented.");
    debugf( "entering netHttpEndRequest( )" );
    return true;
}


//*****************************************************************************
unsigned long netInternetErrorDlg( void *pWindow, HREQUEST pSMsg, unsigned long dwError )
{
    HCONNECTION connection = pSMsg->connection;

    debugf( "entering netInternetErrorDlg( )" );

    // TODO: handle other securiry/certificate issues

    if (pSMsg->statusCode == 407 && !connection->proxy_auth)
    {
        PROXY *proxy = connection->proxy;
        if (proxy && *proxy->username && *proxy->password)
        {
            connection->proxy_auth = calloc(strlen(proxy->username) + strlen(proxy->password) + 2, 1);
            sprintf(connection->proxy_auth, "%s:%s", proxy->username, proxy->password);

            CURLcode ret = curl_easy_setopt(pSMsg->hcurl, CURLOPT_PROXYUSERPWD, connection->proxy_auth);
            if( ret == CURLE_OK )
            {
                // Drop response, most likely a pretty error message from proxy
                ReadBuffer(&pSMsg->m_pBUFFERRecv, NULL, pSMsg->totalBytesReceived);
                return ERROR_INTERNET_FORCE_RETRY;
            }
            else
            {
#ifdef DEBUG_LOG
              debugf("netInternetErrorDlg - CURLOPT_PROXYUSERPWD Error: %d", (int)ret);
#endif
            }
        }
    }

    return ERROR_CANCELLED;
}


//*****************************************************************************
bool netInternetCloseConnection( HCONNECTION aConnection )
{
    if( !aConnection )
    {
#ifdef DEBUG_LOG
        debugf("netInternetCloseConnection( HCONNECTION ) - NULL Parameter. Returning now.");
#endif
        return true;
    }

    if( aConnection->host )
    {
        free(aConnection->host);
    }

    if( aConnection->auth )
    {
        free(aConnection->auth);
    }

    if( aConnection->proxy_auth )
    {
        free(aConnection->proxy_auth);
    }

    free( aConnection );

    return true;
}


//*****************************************************************************
bool netInternetCloseHandle( HREQUEST pSMsg )
{
    if( pSMsg  ==  NULL )
    {
#ifdef DEBUG_LOG
        debugf( "netInternetCloseHandle( HREQUEST pSMsg = NULL )  --  RETURNING immediately" );
#endif
        return true;
    }
    else
    {
#ifdef DEBUG_LOG
        debugf( "netInternetCloseHandle( HREQUEST pMsg = 0x%8.8x )", pSMsg );
#endif
    }

    pthread_mutex_lock( &pSMsg->m_mutex );

    BUFFER  *cur;
    cur = pSMsg->m_pBUFFERRecv;
    while( cur  !=  NULL )
    {
        cur = RemoveBuffer( &pSMsg->m_pBUFFERRecv );
    }

    pthread_mutex_unlock( &pSMsg->m_mutex );

    if( pSMsg->headers )
    {
        curl_slist_free_all( pSMsg->headers );
        pSMsg->headers = 0;
    }

    if ( pSMsg->hcurl != NULL )
    {
        curl_easy_cleanup( pSMsg->hcurl );
    }
    
    delete_request( pSMsg );

#ifdef DEBUG_LOG
    debugf("netInternetCloseHandle(HREQUEST) Done");
#endif

    return true;
}


//*****************************************************************************
// Process data received from server
size_t curl_handler_writefunction( void *ptr, size_t size, size_t nmemb, void *stream)
{
    size_t nBytesHandled = nmemb * size;

    HREQUEST pReq = (HREQUEST)stream;

#ifdef DEBUG_LOG
    debugf( "curl_handler_writefunction size = %d, nmemb = %d, bytes = %d", size, nmemb, nBytesHandled );
#ifdef DEBUG_LOG_VERBOSE
    if( nBytesHandled )
    {
        dump_string("HREQUEST->m_pBUFFERRecv += ", (char *)ptr, nBytesHandled );
    }
#endif
#endif

    FillBuffer( &pReq->m_pBUFFERRecv, ptr, nBytesHandled );
    pReq->totalBytesReceived += nBytesHandled;

    return nBytesHandled;
}


// Process an HTTP header
size_t curl_handler_headerfunction( void *ptr, size_t size, size_t nmemb, void *stream)
{
    size_t nBytesHandled = nmemb * size;
    char *hdr = (char *)ptr;
    char szHdr[512];

    HREQUEST pReq = (HREQUEST)stream;

    // The curl docs state that the incoming ptr may not contain a nul terminator, so we make a nul terminated copy.
    // It's a buit crude, but will do for what we need from the headers.
    if( nBytesHandled > (sizeof(szHdr) - 1) )
    {
        memcpy( szHdr, hdr, sizeof(szHdr)-2 );
        szHdr[sizeof(szHdr)-1] = '\0';
    } 
    else
    {
        memcpy( szHdr, hdr, nBytesHandled );
        szHdr[nBytesHandled] = '\0';
    }

#ifdef DEBUG_LOG
    debugf( "curl_handler_headerfunction (%d bytes)", nBytesHandled );
    dump_string("   Header", szHdr, 0 );
#endif

    // NOTICE! strtok() is not multi thread safe. - Potential for crashes

    char *szHTTP_SLASH = "HTTP/";
    char *szContent_Length = "Content-Length:";

    // Grab the text associated with the status code
    if( nBytesHandled > strlen(szHTTP_SLASH)  && strncmp( szHTTP_SLASH, szHdr, strlen(szHTTP_SLASH) ) == 0 )
    {
        pReq->statusCodeText = calloc( nBytesHandled+1, 1 );

        char *p = strchr(szHdr, ' ');  // Past HTTP/1.1
        if (p)
            p = strchr(p+1, ' ');      // Past status code
        if (p)
            strcat( pReq->statusCodeText, p+1 );

#ifdef DEBUG_LOG
        debugf( "   Cached status text \"%s\"", pReq->statusCodeText );
#endif
    }

    // Grab the expected Content-Length
    if( nBytesHandled > strlen(szContent_Length) && strncmp( szContent_Length, szHdr, strlen(szContent_Length) ) == 0 )
    {
        char *p = strchr(szHdr, ' ');
        if (p)
            pReq->expectedContentLength = atol( p+1 );
        
#ifdef DEBUG_LOG
        debugf( "  Cached content length: %d", pReq->expectedContentLength );
#endif
    }

    return nBytesHandled;
}


// Send data to server
size_t curl_handler_readfunction( void *ptr, size_t size, size_t nmemb, void *stream)
{
    size_t nBytesHandled;
    size_t nOutSize = nmemb * size;
    HREQUEST pReq = (HREQUEST)stream;

    nBytesHandled = ReadBuffer( &pReq->m_pBUFFERSend, ptr, nOutSize );

#ifdef DEBUG_LOG
    debugf("Sent %d bytes", nBytesHandled);
#endif
    return nBytesHandled;
}


// Seek to new position in buffer
int curl_handler_seekfunction( void *stream, curl_off_t offset, int origin)
{
    debugf("Seeking to %d in write buffer (%d)", (int)offset, origin);
    
    HREQUEST pReq = (HREQUEST)stream;
    if (origin == SEEK_SET && offset == 0 && pReq->sendLength > 0)
    {
        ClearBuffer(&pReq->m_pBUFFERSend);
        FillBuffer(&pReq->m_pBUFFERSend, pReq->sendBuffer, pReq->sendLength);
        return CURL_SEEKFUNC_OK;
    }
    return CURL_SEEKFUNC_CANTSEEK;
}


// See if specified cert is in cert file
BOOL check_for_cert(const char *cert_file, char *inBuf)
{
    char    line[512];
    char    *pBuf;
    FILE    *pf;

    // ToDo: cache cert (at least matching one) in memory
    pf = fopen( cert_file, "r" );
    if( pf == NULL )
    {
        return FALSE;
    }

    pBuf = inBuf;
    while( TRUE )
    {
        while( fgets( line, sizeof(line), pf ) )
        {
            int     iCmp    = strlen( line );
            if( iCmp  >  strlen( pBuf ) )
                break;
            if( strncmp( line, pBuf, iCmp )  !=  0 )
                break;
            pBuf += iCmp;
            if( *pBuf  ==  '\n' )
                pBuf++;
            if( *pBuf  ==  '\r' )
                pBuf++;
            if( *pBuf  ==  '\n' )
                pBuf++;
            if( strstr( line, "END CERTIFICATE" )  !=  0 )
            {
                fclose(pf);
                return TRUE;
            }
        }
        if( feof( pf ) )
        {
            fclose(pf);
            return FALSE;
        }
        while( strstr( line, "END CERTIFICATE" )  ==  0 )
        {
            if( !fgets( line, MAX_PATH, pf ) )
            {
                fclose(pf);
                return FALSE;
            }
        }
        pBuf = inBuf;
    }
    if( *inBuf  ==  '\n' )
        inBuf++;
    if( *inBuf  ==  '\r' )
        inBuf++;
    if( *inBuf  ==  '\n' )
        inBuf++;
    if( strlen( inBuf )  >  0 )
    {
        fclose(pf);
        return FALSE;
    }

    fclose(pf);
    return TRUE;
}


// Called by openssl to verify peer cert
static int openssl_verify_callback(X509_STORE_CTX *ctx, void *arg)
{
    int ok;
    HREQUEST pReq = (HREQUEST)arg;
    char *cert;
    char zero = 0;

    if (!(ok = X509_verify_cert(ctx)) && ctx->cert)
    {
        // Normal verification failed, see if we have allowed this cert before
        BIO *mem = BIO_new(BIO_s_mem());
        PEM_write_bio_X509(mem, ctx->cert);
        BIO_write(mem, &zero, 1);
        BIO_get_mem_data(mem, &cert);
        debugf("Verifying cert: %s", cert);

        if (check_for_cert(pReq->connection->session->cafile, cert))
        {
            ok = 1;
        }
        else
        {
            ExtractCertificateInfo(pReq, ctx->cert, cert);
        }

        BIO_free(mem);
    }

    return(ok);
}


// Curl SSL initialization callback
static CURLcode curl_sslctxfun(CURL * curl, void * sslctx, void * parm)
{
//  sslctxparm * p = (sslctxparm *) parm;
  SSL_CTX * ctx = (SSL_CTX *) sslctx ;

/* Save for future reference.  If I'd known about adding certs this way
   early, I could have avoided the certificate concatenating code in meeting.

  if (!SSL_CTX_use_certificate(ctx,p->usercert)) {
    BIO_printf(p->errorbio, "SSL_CTX_use_certificate problem"); goto err;
  }
  if (!SSL_CTX_use_PrivateKey(ctx,p->pkey)) {
    BIO_printf(p->errorbio, "SSL_CTX_use_PrivateKey"); goto err;
  }

  if (!SSL_CTX_check_private_key(ctx)) {
    BIO_printf(p->errorbio, "SSL_CTX_check_private_key"); goto err;
  }

  SSL_CTX_set_quiet_shutdown(ctx,1);
  SSL_CTX_set_cipher_list(ctx,"RC4-MD5");
  SSL_CTX_set_mode(ctx, SSL_MODE_AUTO_RETRY);

  X509_STORE_add_cert(ctx->cert_store,sk_X509_value(p->ca,
                                                    sk_X509_num(p->ca)-1));
*/
  SSL_CTX_set_verify_depth(ctx,2);

  SSL_CTX_set_verify(ctx,SSL_VERIFY_PEER,0);

  SSL_CTX_set_cert_verify_callback(ctx, openssl_verify_callback, parm);

  return CURLE_OK ;

/*
err:
  ERR_print_errors(p->errorbio);
  return CURLE_SSL_CERTPROBLEM;
*/
}


//*****************************************************************************
bool netHttpSendRequest( HREQUEST pReq, HSESSION pSession, const char * aHeaders, unsigned long dwHeadersLen,
                         const char *aOptional, unsigned long dwOptionalLen )
{
#ifdef DEBUG_LOG
    debugf( "netHttpSendRequest() Start" );
    debugf( "    aHeaders: (%d Bytes) *%s*", dwHeadersLen, aHeaders ? aHeaders:"[NULL]" );
    dump_string("   aOptional:", aOptional, dwOptionalLen );
#endif

    CURL *hcurl = pReq->hcurl;

    if (aHeaders)
        netHttpAddRequestHeaders( pReq, aHeaders, dwHeadersLen);

    // Keep libcurl from adding "expect: 100-continue" header
    char *pszReqHeaders = "Expect:";
    netHttpAddRequestHeaders( pReq, pszReqHeaders, strlen(pszReqHeaders) );

    // Keep copy of send buffer so we can reset position if needed (e.g. proxy auth requires resend)
    pReq->sendBuffer = (char *)aOptional;
    pReq->sendLength = dwOptionalLen;

    pReq->retCode = curl_easy_setopt(hcurl, CURLOPT_HTTPHEADER, pReq->headers);
    SetLastCurlError( pReq, "netHttpSendRequest - CURLOPT_HTTPHEADER" );

    if ( strcmp(pReq->verb, "POST") == 0) {
        // Hack, should get this from caller, but we only use POST requests with HTTP tunneling
        pszReqHeaders = "Content-type: application/octet-stream";
        netHttpAddRequestHeaders( pReq, pszReqHeaders, strlen(pszReqHeaders) );

        curl_easy_setopt( hcurl, CURLOPT_POSTFIELDSIZE, dwOptionalLen );
        curl_easy_setopt( hcurl, CURLOPT_POSTFIELDS, aOptional );
    }
    else if ( strcmp( pReq->verb, "PUT") == 0 )
    {
        FillBuffer( &pReq->m_pBUFFERSend, (char *)aOptional, dwOptionalLen );
        curl_easy_setopt( hcurl, CURLOPT_INFILESIZE, dwOptionalLen );
        curl_easy_setopt( hcurl, CURLOPT_READFUNCTION, curl_handler_readfunction );
        curl_easy_setopt( hcurl, CURLOPT_READDATA, pReq );
        curl_easy_setopt( hcurl, CURLOPT_SEEKFUNCTION, curl_handler_seekfunction );
        curl_easy_setopt( hcurl, CURLOPT_SEEKDATA, pReq );
    }
    else if ( strcmp( pReq->verb, "GET") != 0 )
    {
        if (dwOptionalLen > 0)
        {
            curl_easy_setopt( hcurl, CURLOPT_PUT, 1 );
            FillBuffer( &pReq->m_pBUFFERSend, (char *)aOptional, dwOptionalLen );
            curl_easy_setopt( hcurl, CURLOPT_INFILESIZE, dwOptionalLen );
            curl_easy_setopt( hcurl, CURLOPT_READFUNCTION, curl_handler_readfunction );
            curl_easy_setopt( hcurl, CURLOPT_READDATA, pReq );
            curl_easy_setopt( hcurl, CURLOPT_SEEKFUNCTION, curl_handler_seekfunction );
            curl_easy_setopt( hcurl, CURLOPT_SEEKDATA, pReq );
            curl_easy_setopt( hcurl, CURLOPT_CUSTOMREQUEST, pReq->verb );
        }
        else
        {
            curl_easy_setopt( hcurl, CURLOPT_HTTPGET, 1 );
            curl_easy_setopt( hcurl, CURLOPT_CUSTOMREQUEST, pReq->verb );
        }
    }
    
    // TODO: need to implement callback for CURLOPT_SEEKFUNCTION to allow proxy server authentication
    // Libcurl needs to back up in the data stream to re-send the request after proxy auth request.
    
    curl_easy_setopt( hcurl, CURLOPT_WRITEFUNCTION, curl_handler_writefunction );
    curl_easy_setopt( hcurl, CURLOPT_WRITEDATA, pReq );

    curl_easy_setopt( hcurl, CURLOPT_HEADERFUNCTION, curl_handler_headerfunction );
    curl_easy_setopt( hcurl, CURLOPT_HEADERDATA, pReq );

	curl_easy_setopt( hcurl, CURLOPT_SSL_CTX_FUNCTION, curl_sslctxfun)  ;
	curl_easy_setopt( hcurl, CURLOPT_SSL_CTX_DATA, pReq)  ;

    // Process the request. This call blocks until don
    pReq->retCode = curl_easy_perform( hcurl );

    SetLastCurlError( pReq, "netHttpSendRequest - curl_easy_perform" );
    pReq->fRequestComplete = true;

    // Get the response code
    /*
      CURLINFO_RESPONSE_CODE - Pass a pointer to a long to receive the last received HTTP or FTP code.
      This option was known as CURLINFO_HTTP_CODE in libcurl 7.10.7 and earlier.
      This will be zero if no server response code has been received.
      CURLINFO_HTTP_CONNECTCODE - only for HTTPS tunnels through proxy.
     */
    curl_easy_getinfo( hcurl, CURLINFO_RESPONSE_CODE, &pReq->statusCode );
    if (pReq->statusCode == 0)
    {
        curl_easy_getinfo( hcurl, CURLINFO_HTTP_CONNECTCODE, &pReq->statusCode );

        // HACK: There must be a better way to detect this case, but if we appear to be creating
        // an SSL tunnel through a proxy server, set status code to OK so caller knows to retry.
        if (pReq->statusCode == 407)
            pReq->retCode = CURLE_OK;
    }

/*    if ( pReq->retCode == CURLE_OK && pReq->statusCode != 200 )
    {
        // Set error code if request was not successful
        pReq->retCode = CURLE_HTTP_RETURNED_ERROR;
        SetLastCurlError( pReq, "netHttpSendRequest - bad status code" );
    }
*/

    if( pReq->headers )
    {
        curl_slist_free_all( pReq->headers );
        pReq->headers = 0;
    }

    debugf( "netHttpSendRequest( ) - Finish. Bytes Received: %d, Status code %d.", pReq->totalBytesReceived, pReq->statusCode );

    return (pReq->retCode == CURLE_OK);
}


//*****************************************************************************
bool netHttpSendRequestEx( HREQUEST pSMsg, HSESSION pSession, LPINTERNET_BUFFERS Output, LPINTERNET_BUFFERS Input )
{
    bool    fRet    = true;

    fRet = netHttpSendRequest( pSMsg, pSession, "", 0, Output->lpvBuffer, Output->dwBufferTotal );

    return fRet;
}


//*****************************************************************************
bool netHttpQueryResultsReady( HREQUEST aReq, bool main_thread )
{
  return aReq->fRequestComplete;
  //return (aReq->totalBytesReceived > 0);
}


//*****************************************************************************
bool netHttpQueryInfo( HREQUEST aReq, unsigned long dwFlags, char *pStatusCode, unsigned long *pdwBufferLength, unsigned long *pdwIndex )
{
    bool    fRet    = false;

    if( dwFlags  ==  HTTP_QUERY_STATUS_CODE )
    {
      sprintf( pStatusCode, "%ld", aReq->statusCode );
      *pdwBufferLength = strlen(pStatusCode);
      fRet = true;
    }
    else if( dwFlags  ==  HTTP_QUERY_STATUS_TEXT )
    {
      if( aReq->statusCodeText )
	{
	  strcpy( pStatusCode, aReq->statusCodeText );
	}else
	{
	  strcpy( pStatusCode, "[none]" );
	}
      *pdwBufferLength = strlen(pStatusCode);
      fRet = true;
    }
    else if( dwFlags  ==  HTTP_QUERY_CONTENT_LENGTH )
    {
      // curl_easy_perfom() blocks until the data is received, so what we have now is all there is.
#ifdef DEBUG_LOG
      debugf( "entering newHttpQueryInfo() -- REQUEST.m_pBUFFERRecv = %08X, Received length = %d",
	      aReq->m_pBUFFERRecv, (aReq->m_pBUFFERRecv) ? aReq->m_pBUFFERRecv->len : 0 );
      debugf( "HTTP_QUERY_CONTENT_LENGTH to be returned = %d", aReq->totalBytesReceived );
#endif

      //  format number of bytes read and return that
      sprintf( pStatusCode, "%d", (int)aReq->totalBytesReceived );
      *pdwBufferLength = strlen(pStatusCode);
      fRet = true;
    }else
    {
      errorf( "netHttpQueryInfo - Error: Unsupported dwFlags value %d", dwFlags );
    }


    return fRet;
}


//*****************************************************************************
bool netInternetReadFileExA( HREQUEST pSMsg, INTERNET_BUFFERSA *IB )
{
    pthread_mutex_lock( &pSMsg->m_mutex );

    int     bytesRead;
    bytesRead = ReadBuffer( &pSMsg->m_pBUFFERRecv, IB->lpvBuffer, IB->dwBufferLength );
    IB->dwBufferLength = bytesRead;

#ifdef DEBUG_LOG
    debugf( "netInternetReadFileEx - Read %d bytes.", bytesRead );
#endif
    pthread_mutex_unlock( &pSMsg->m_mutex );

    return true;
}


//*****************************************************************************
bool netHttpAddRequestHeaders( HREQUEST aReq, const char *szHeader, unsigned long dwHeaderLen )
{
    if( dwHeaderLen  ==  0 )
        return false;

#ifdef DEBUG_LOG
    debugf( "netHttpAddRequestHeaders: dwHeaderLen = %d, szHeader = %s", dwHeaderLen, szHeader );
#endif

    char buff[512];
    const char *pos = szHeader;
    char *end = strstr(pos, "\n");
    while (end)
    {
        int len = end - pos;
        strncat(buff, pos, len);

        aReq->headers = curl_slist_append(aReq->headers, buff);

        pos = end + 2;
        end = strstr(pos, "\n");
    }

    // In case last header string isn't terminated with <cr><lf>
    if (*pos)
    {
        aReq->headers = curl_slist_append(aReq->headers, pos);
    }

    return true;
}


//*****************************************************************************
bool netInternetWriteFile( HREQUEST pSMsg, const void *pvData, unsigned long dwOut, unsigned long *pdwBytesWritten )
{
    //  Here's the plan...
    //  See the explanation in the function netHttpSendRequestEx( ).

    *pdwBytesWritten = dwOut;

    return true;
}


//**********************************************************************************
bool netHttpClose( HSESSION pSession, HCONNECTION pConnection )
{

    if( pSession )
    {
        // TODO: probably should free memory...
    }

    return true;
}


//**********************************************************************************
static void addToCert(char *buffer, const char *ptag, const char *pdata)
{
    strcat( buffer, "<" );
    strcat( buffer, ptag );
    strcat( buffer, ">" );
    strcat( buffer, pdata );
    strcat( buffer, "</" );
    strcat( buffer, ptag );
    strcat( buffer, ">\n" );
}


static const char *convert_time(ASN1_TIME *time, char *buff)
{
    char *str;
    int size;

    BIO *mem = BIO_new(BIO_s_mem());

    ASN1_TIME_print(mem, time);

    size = BIO_get_mem_data(mem, &str);
    strncpy(buff, str, size);
    buff[size] = '\0';

    BIO_free(mem);

    return buff;
}


static const char *raw_to_string(const unsigned char *raw, size_t raw_size, char *buff, int buff_size)
{
	size_t i;
	if (raw_size == 0)
		return NULL;

	if (raw_size * 3 + 1 >= buff_size)
		return NULL;

	for (i = 0; i < raw_size; i++) {
		sprintf(&(buff[i * 3]), "%02X ", raw[i]);
	}
	buff[i * 3] = '\0';

	return buff;
}


bool ExtractCertificateInfo(HREQUEST pReq, X509 *cert, const char *pem_cert)
{
    char *buffer = pReq->szCertText;
    char temp[1024];
    unsigned int  md_size;
    unsigned char md[EVP_MAX_MD_SIZE];
    
    strcpy(buffer, "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n<newcert>\n");

    addToCert(buffer, "certificate", pem_cert);

    addToCert(buffer, "issued", convert_time(X509_get_notBefore(cert), temp));
    addToCert(buffer, "expires", convert_time(X509_get_notAfter(cert), temp));

    long serial = ASN1_INTEGER_get(X509_get_serialNumber(cert));
    addToCert(buffer, "serial", raw_to_string((const unsigned char *)&serial, sizeof(serial), temp, sizeof(temp)));

	X509_digest(cert, EVP_md5(), md, &md_size);
	raw_to_string(md, md_size, temp, sizeof(temp));
	addToCert(buffer, "md5", temp);

    X509_NAME_oneline(X509_get_subject_name(cert), temp, sizeof(temp));
    addToCert(buffer, "issto", temp);

    X509_NAME_oneline(X509_get_issuer_name(cert), temp, sizeof(temp));
    addToCert(buffer, "issby", temp);

		/* Print the fingerprint of the certificate */
/*		digest_size = sizeof(digest);
		if ((ret = gnutls_x509_crt_get_fingerprint(crt, GNUTLS_DIG_MD5, digest, &digest_size)) < 0) {
			errorf("Error in fingerprint calculation: %s", gnutls_strerror(ret));
		} else {
			print = raw_to_string((const unsigned char *)digest, digest_size);
            if (print != NULL)
            {
                debugf(" # fingerprint: %s", print);
                addToNewCert( "md5", (char *) print );
            }
		}
*/

    strcat(buffer, "</newcert>\n");

    return true;
}


const char *netGetServerCertificate(HREQUEST pReq)
{
    return pReq->szCertText;
}
