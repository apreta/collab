/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

// nethttp_win32.c: implementation of our cross-platform HTTP API.
//    The Windows implementation uses WinINet while the Linux
//    implementation (nethttp.c) uses LibCurl.
//
//////////////////////////////////////////////////////////////////////


#include <stdio.h>

#include "nethttp.h"


HINTERNET netHttpOpen( LPCSTR appName, int iTimeout, bool fUseNTLM )
{
    return( InternetOpenA( appName, INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0) );
}


void netHttpClose( HINTERNET hSession, HINTERNET hConnection )
{
    if( hConnection )
        InternetCloseHandle( hConnection );
    if( hSession )
        InternetCloseHandle( hSession );
}


HINTERNET netHttpConnect( HINTERNET hSession, LPCSTR aUrl, LPCSTR aHost, unsigned short aPort, LPCSTR aName,
                          LPCSTR aPass )
{
    return( InternetConnectA( hSession, aHost, aPort, aName, aPass, INTERNET_SERVICE_HTTP, 0, 0 ));
}


HINTERNET netHttpOpenRequest( HINTERNET hConnection, const char *aVerb, const char *aObjName, const char *aVersion, DWORD dwFlags )
{
    return( HttpOpenRequestA( hConnection, aVerb, aObjName, aVersion, NULL, NULL, dwFlags, 0) );
}


bool netHttpSendRequest( HINTERNET hRequest, HINTERNET hSession, const char *aHeaders, DWORD dwHeadersLen, const char *aOptional, DWORD dwOptionalLen )
{
    return( HttpSendRequestA( hRequest, aHeaders, dwHeadersLen, (void *) aOptional, dwOptionalLen ) );

}

bool netHttpQueryResultsReady( HINTERNET hRequest, bool main_thread )
{
    return true;
}

bool netHttpQueryInfo( HINTERNET hRequest, DWORD dwFlags, LPVOID pStatusCode, LPDWORD pdwBufferLength, LPDWORD pdwIndex )
{
    return( HttpQueryInfoA( hRequest, dwFlags, pStatusCode, pdwBufferLength, pdwIndex ) );
}

bool netInternetReadFileExA( HINTERNET hRequest, INTERNET_BUFFERSA *pOutput )
{
    unsigned long   dwContext;
    return( InternetReadFileExA( hRequest, pOutput, IRF_NO_WAIT, dwContext ) );
}


DWORD netInternetErrorDlg( void *hParent, HINTERNET hRequest, DWORD dwError )
{
    HWND    hWnd    = hParent ? (HWND)hParent : GetForegroundWindow( );
    DWORD   dwFlags = FLAGS_ERROR_UI_FILTER_FOR_ERRORS | FLAGS_ERROR_UI_FLAGS_GENERATE_DATA | FLAGS_ERROR_UI_FLAGS_CHANGE_OPTIONS;

    return( InternetErrorDlg( hWnd, hRequest, dwError, dwFlags, NULL ) );
}

bool netInternetCloseHandle( HINTERNET hRequest )
{
    if( hRequest )
        return InternetCloseHandle( hRequest );
    else
        return true;
}

bool netInternetCloseConnection( HINTERNET hConnection )
{
    if( hConnection )
        return InternetCloseHandle( hConnection );
    else
        return true;
}

bool netHttpAddRequestHeaders( HINTERNET hRequest, const char *szHeader, DWORD dwHeaderLen )
{
    return HttpAddRequestHeadersA( hRequest, szHeader, dwHeaderLen, HTTP_ADDREQ_FLAG_ADD );
}


bool netHttpEndRequest( HINTERNET hRequest )
{
    return HttpEndRequest( hRequest, NULL, 0, 0 );
}

bool netInternetGetLastResponseInfo( LPDWORD pdwError, char *szBuf, LPDWORD pdwLength )
{
    return InternetGetLastResponseInfoA( pdwError, szBuf, pdwLength );
}


bool netHttpSendRequestEx( HINTERNET hRequest, HINTERNET hSession, LPINTERNET_BUFFERS Output, LPINTERNET_BUFFERS Input )
{
    return HttpSendRequestEx( hRequest, Output, Input, HSR_INITIATE, 0 );
}


bool netInternetWriteFile( HINTERNET hRequest, LPCVOID pvData, DWORD dwOut, LPDWORD pdwBytesWritten )
{
    return InternetWriteFile( hRequest, pvData, dwOut, pdwBytesWritten );
}

