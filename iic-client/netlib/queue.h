/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef QUEUE_H
#define QUEUE_H

#define MAX_MSG_SIZE 8000 // under standard default (8192)
#define MAX_CLIENTS 100
#define MAX_QUEUE_BYTES 500000

struct msgbuffer
{
    long to;              // message destination (0 is server)
    long msg_size;
    long total_size;      // if larger then msg_size, message was split and more segments follow
    long from;            // message source
    long command;
    char data[MAX_MSG_SIZE];
};

#define MSG_HEADER_SIZE (sizeof(long)*5)
#define MSG_CMD_NORMAL 0
#define MSG_CMD_ERROR 1

class MsgQueue
{
    int queue_id;
    char *queue_name;
    int client_id;

    msgbuffer send_buffer;

    msgbuffer recv_buffer;

public:
    MsgQueue(const char * name);

    int create_queue(int client_id, char key);
    int open_queue(int client_id, char key);

    int send_message(int to_id, const char *msg_data, int msg_size, int command);

    int get_message(int & from_id, int & total_size, int & command, bool wait=true);
    int get_message_data(char *msg_data, int msg_size);

};


#endif
