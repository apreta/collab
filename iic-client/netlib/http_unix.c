
#ifdef LINUX

#include <stdio.h>
#include <time.h>
#include <assert.h>

#include "http.h"
#include "httpbuffer.h"
#include "log.h"

/*

Protocol:

 Connect URL: /connect
	Returns <host id>/<check>

 Close URL: /close/<host#>/<check>

 Data exchangel URL: /data/<host#>/<check>/<in-sequence>/<out-sequence>

 in-sequence = position in "up" stream of first byte in this request
 out-sequence = position in "down" stream that client expects to receive
                (= position in "down" stream of last byte in last response + 1)

 If client does not receive a response, it will re-send the same request with
 same sequence numbers again.  The server will ignore the request data if it
 has already sent data to host; and will generate a new response (which may
 include more data if more is pending for client).

Poll strategy:
  If data was received, try again immediately
  If no data, start counter and use fast poll interval
  If no data and counter exceeded, use slow poll interval

Improvements:
  Use async functions to stream data better (might not work well for all proxies)
  Hash sequence values to make protocol harder to spoof (minimally check source address is unchanged)

*/

struct _HTTPINFO
{
//	HINTERNET hInternetSession;
//	HINTERNET hConnection;

	BOOL non_blocking;
	BOOL closing;
	int read_timeout;	/* 0 = no timeout */

	long last;

	BOOL sending;
	BOOL fast_poll;
	int empty_count;

	char server[512];
	char id[32];

	BUFFER *recv_queue;
	BUFFER *send_queue;

	long in_sequence;
	long out_sequence;

//	CRITICAL_SECTION lock;

};

#if 0

static int CheckStatus(HINTERNET hURL);
static int SendRequest(HTTPINFO * info);
static int BufferResult(HTTPINFO * info, HINTERNET hURL);
static BOOL RetryRequest(DWORD error);

#endif

void HttpInit(HTTPINFO ** info)
{
    *info = calloc(sizeof(HTTPINFO),1);
//	InitializeCriticalSection(&(*info)->lock);
}


void HttpSetNonBlock(HTTPINFO * info, BOOL flag)
{
	info->non_blocking = flag;
}

void HttpSetTimeout(HTTPINFO * info, int timeout)
{
	info->read_timeout = timeout;
}

// Open HTTP tunnel to server.
// Will block until suceeds or times out.

int HttpConnect(HTTPINFO * info, const char *server)
{
#if 0
	HINTERNET hURL = NULL;
	char object[128];
	char out[128];
	DWORD dwBytes;
	DWORD dwError;
	BOOL result;

	strcpy(info->server, server);


	// Make internet connection.
	info->hInternetSession = InternetOpenA(
					  "Microsoft Internet Explorer",
					  INTERNET_OPEN_TYPE_PRECONFIG,
					  NULL,
					  NULL,
					  0);

	if (info->hInternetSession == NULL)
	{
		errorf("InternetOpen failed [%d]", (int)GetLastError());
		return -1;
	}

	sprintf(object, "/connect");

	// Open connection to server.
	info->hConnection = InternetConnectA(
				info->hInternetSession,
				server,
				INTERNET_DEFAULT_HTTP_PORT,
				NULL, NULL,
				INTERNET_SERVICE_HTTP, 0, 0);
	if (info->hConnection == NULL)
	{
		errorf("InternetConnect failed [%s, %d]", server, (int)GetLastError());
		goto failed;
	}

	// Make connection to desired page.
	hURL = HttpOpenRequestA(
				info->hConnection,
				"POST", object,
				"HTTP/1.1",
				NULL, NULL,
				INTERNET_FLAG_NO_CACHE_WRITE|INTERNET_FLAG_PRAGMA_NOCACHE|INTERNET_FLAG_RELOAD|INTERNET_FLAG_KEEP_CONNECTION, 0);

	if (hURL == NULL)
	{
		errorf("HttpOpenRequest failed [%d]", (int)GetLastError());
		goto failed;
	}

	while (TRUE)
	{
		HWND hWnd;

		result = HttpSendRequest(
					hURL,
					NULL, 0,
					NULL, 0);
		if (result == FALSE)
		{
			errorf("HttpSendRequest failed [%d]", (int)GetLastError());
			goto failed;
		}

		hWnd = GetForegroundWindow() ? GetForegroundWindow() : GetDesktopWindow();

		dwError = InternetErrorDlg(hWnd,
						hURL, GetLastError(),
						FLAGS_ERROR_UI_FILTER_FOR_ERRORS |
						FLAGS_ERROR_UI_FLAGS_CHANGE_OPTIONS |
						FLAGS_ERROR_UI_FLAGS_GENERATE_DATA,
						NULL);
		if (dwError == ERROR_INTERNET_FORCE_RETRY)
			continue;

		break;
	}

	// Check status
	if (CheckStatus(hURL) < 0)
		goto failed;

	// Get result.
	result = InternetReadFile(
				hURL,
				out,
				sizeof(out),
				&dwBytes);
	if (result == FALSE)
	{
		errorf("InternetReadFile failed [%d]", (int)GetLastError());
		goto failed;
	}
	if (dwBytes <= 0)
	{
		errorf("InternetReadFile no data returned from connection");
		goto failed;
	}

	strncat(info->id, out, dwBytes < sizeof(info->id) ? dwBytes : sizeof(info->id));
	errorf("Connect HTTP session %s", info->id);

	InternetCloseHandle(hURL);

	return 0;

failed:
	if (hURL != NULL)
		InternetCloseHandle(hURL);

	if (info->hConnection != NULL)
	{
		InternetCloseHandle(info->hConnection);
		info->hConnection = NULL;
	}
#endif
	return -1;
}


// Send data
// Currently just adds to outgoing buffer list for sending during a receive or
// check readable operation.

int HttpSend(HTTPINFO * info, char * data, int len)
{
#if 0
	BOOL was_empty = info->send_queue == NULL;

	EnterCriticalSection(&info->lock);
	int status = FillBuffer(&info->send_queue, data, len);
	LeaveCriticalSection(&info->lock);

	debugf("Prepared %d bytes", len);

	if (len > 0 && was_empty && !info->fast_poll)
	{
		/* Set to fast poll so receive/check on another thread will send soon */
		info->empty_count = 0;
		info->last = clock() / (CLOCKS_PER_SEC/1000);
		info->fast_poll = TRUE;
	}

	return status;
#endif
    return -1;
}


// Receive data
// Non-blocking symantics not quite the same as regular socket as we will
// block for a bit to send a request off to the server.

int HttpRecv(HTTPINFO * info, char * data, int len)
{
#if 0
	int status;
	int read;

	if (!info->non_blocking)
	{
		// Connection timeout keeps blocking calls from waiting indefinitely.
		int timeout = (info->read_timeout == 0) ? -1 : info->read_timeout/1000;

		status = HttpCheckReadable(info, timeout);
		if (status < 0)
			return status;
	}

	if (info->closing)
		return -1;

	// Check for buffered data
	EnterCriticalSection(&info->lock);
	read = ReadBuffer(&info->recv_queue, data, len);
	LeaveCriticalSection(&info->lock);
	if (read > 0)
	{
		debugf("Unbuffered %d bytes", read);
		return read;
	}

	status = SendRequest(info);
	if (status < 0)
		return status;

	// Read buffer.
	EnterCriticalSection(&info->lock);
	read = ReadBuffer(&info->recv_queue, data, len);
	LeaveCriticalSection(&info->lock);

	debugf("Unbuffered %d bytes", read);
	return read;
#endif
    return -1;
}


// See if there is data to read.
// If timeout is non-zero, will wait for data to become available.
// -1 waits forever.

int HttpCheckReadable(HTTPINFO * info, int timeout)
{
#if 0
	BUFFER * cur;
	long now = clock() / (CLOCKS_PER_SEC/1000);
	long expires = now + (timeout*1000) + 1;	// make sure we get through once
	int status;

	/* Already data in queue, return immediately */
	cur = info->recv_queue;
	if (cur != NULL)
		return 1;

	while (!info->closing && (timeout == -1 || now < expires))
	{
		/* Use poll interval to determine when to send request */
		if (now - info->last > (info->fast_poll ? HTTP_FAST_POLL_TIME : HTTP_SLOW_POLL_TIME))
		{
			status = SendRequest(info);
			if (status < 0)
				return status;

			/* Didn't send all outbound data, send another request */
			if (info->send_queue != NULL)
			{
				info->last = 0;
				continue;
			}
		}
		else
			Sleep(100);

		cur = info->recv_queue;
		if (cur != NULL)
			return 1;

		now = clock() / (CLOCKS_PER_SEC/1000);
	}
#endif
	return 0;
}


// See if we can write data.
// Since we just write into buffer, this always returns true.

int HttpCheckWriteable(HTTPINFO * info, int timeout)
{
	// *** check number bytes waiting?
	return 1;
}


// Try to perform clean shutdown of tunnel.
// Server will also disconnect us if it hasn't received any recent requests.

int HttpClose(HTTPINFO * info, BOOL clean_shutdown)
{
#if 0
	HINTERNET hURL;
	char object[128];
	BUFFER * cur;

	if (info->closing)
		return 0;

	// Not the best logic, but try to force blocking receive to exit.
	info->closing = TRUE;

	if (*info->id && clean_shutdown)
	{
		sprintf(object, "/close/%s", info->id);

		// Open connection to server.
		if (info->hConnection == NULL)
		{
			info->hConnection = InternetConnectA(
						info->hInternetSession,
						info->server,
						INTERNET_DEFAULT_HTTP_PORT,
						NULL, NULL,
						INTERNET_SERVICE_HTTP, 0, 0);
			if (info->hConnection == NULL)
			{
				errorf("InternetConnect failed [%d]", (int)GetLastError());
				return -1;
			}
		}

		// Try to cleanly shutdown session.
		hURL = HttpOpenRequestA(
					info->hConnection,
					"POST", object,
					"HTTP/1.1",
					NULL, NULL,
					INTERNET_FLAG_NO_CACHE_WRITE|INTERNET_FLAG_PRAGMA_NOCACHE|INTERNET_FLAG_RELOAD|INTERNET_FLAG_KEEP_CONNECTION, 0);

		if (hURL != NULL)
		{
			HttpSendRequest(
						hURL,
						NULL, 0,
						NULL, 0);

			// Ignore result.

			InternetCloseHandle(hURL);
		}
	}

	// Clean up on this end.
	if (info->hConnection != NULL)
		InternetCloseHandle(info->hConnection);

	if (info->hInternetSession)
		InternetCloseHandle(info->hInternetSession);

	EnterCriticalSection(&info->lock);
	cur = info->recv_queue;
	while (cur != NULL)
	{
		cur = RemoveBuffer(&info->recv_queue);
	}
	LeaveCriticalSection(&info->lock);

	DeleteCriticalSection(&info->lock);

    free(info);
#endif

	return 0;
}

#if 0
// Send data request to server.
static int SendRequest(HTTPINFO * info)
{
	HINTERNET hURL = NULL;
	char object[128];
	char out[SEND_BUFF_SIZE];
	int len;
	int retries = 0;
	BOOL result;
	DWORD timeout = HTTP_RECV_TIMEOUT;
	static long counter = 0;

	assert(!info->sending);

	// Get any outbound data.
	EnterCriticalSection(&info->lock);
	len = ReadBuffer(&info->send_queue, out, sizeof(out));
	LeaveCriticalSection(&info->lock);
	if (len < 0)
		len = 0;

	debugf("Sending %d bytes", len);

retry:
	// Generate request URL from current stream position and unique id.
	// The unique id keeps the response from being cached.
	sprintf(object, "/data/%s/%ld/%ld/%ld", info->id, info->in_sequence, info->out_sequence, ++counter);

	// Close connections on retry
	if (retries++)
	{
		InternetCloseHandle(hURL);
		InternetCloseHandle(info->hConnection);
		info->hConnection = NULL;
	}

	// Open connection to server.
	if (info->hConnection == NULL)
	{
		info->hConnection = InternetConnectA(
					info->hInternetSession,
					info->server,
					INTERNET_DEFAULT_HTTP_PORT,
					NULL, NULL,
					INTERNET_SERVICE_HTTP, 0, 0);
		if (info->hConnection == NULL)
		{
			errorf("InternetConnect failed [%d]", (int)GetLastError());
			return -1;
		}
	}

	// Make connection to desired page.
	hURL = HttpOpenRequestA(
				info->hConnection,
				"POST", object,
				"HTTP/1.1",
				NULL, NULL,
				INTERNET_FLAG_NO_CACHE_WRITE|INTERNET_FLAG_PRAGMA_NOCACHE|INTERNET_FLAG_RELOAD|INTERNET_FLAG_KEEP_CONNECTION, 0);

	if (hURL == NULL)
	{
		errorf("HttpOpenRequest failed [%d]", (int)GetLastError());
		return -1;
	}

	info->sending = TRUE;

	// Set timeout...may not work on all versions of wininet, but I don't expect
	// remove server to hang, I expect packet loss, in which case we should get
	// a connection error back.
	InternetSetOption(hURL, INTERNET_OPTION_RECEIVE_TIMEOUT, (LPVOID)&timeout, sizeof(DWORD));

	result = HttpSendRequest(
				hURL,
				NULL, 0,
				out, len);
	if (result == FALSE)
	{
		DWORD err = GetLastError();
		errorf("HttpSendRequest failed [%d]", (int)err);
		if (retries < HTTP_REQUEST_RETRIES && RetryRequest(err))
		{
			Sleep(500);
			goto retry;
		}

		goto failed;
	}

	// Check status
	if (CheckStatus(hURL) < 0)
		goto failed;

	// Buffer any result
	if (BufferResult(info, hURL) < 0)
	{
		DWORD err = GetLastError();
		if (retries < HTTP_REQUEST_RETRIES && RetryRequest(err))
		{
			goto retry;
		}

		goto failed;
	}

	// Keep track of our position
	info->in_sequence += len;

	InternetCloseHandle(hURL);

	debugf("Sent %d outgoing bytes", len);

	info->sending = FALSE;
	return 0;

failed:
	if (hURL)
		InternetCloseHandle(hURL);
	info->sending = FALSE;
	return -1;
}

static void DumpHeaders(HINTERNET hURL)
{
	LPVOID lpOutBuffer=NULL;
	DWORD dwSize = 0;

	for (;;)
	{
		// This call will fail on the first pass, because
		// no buffer is allocated.
		if(!HttpQueryInfo(hURL,HTTP_QUERY_RAW_HEADERS_CRLF,
					      (LPVOID)lpOutBuffer,&dwSize,NULL))
		{
			if (GetLastError()==ERROR_HTTP_HEADER_NOT_FOUND)
			{
				// Code to handle the case where the header isn't available.
				debugf("HTTP headers not found");
				return;
			}
			else
			{
				// Check for an insufficient buffer.
				if (GetLastError()==ERROR_INSUFFICIENT_BUFFER)
				{
					// Allocate the necessary buffer.
					lpOutBuffer = calloc(dwSize,1);
					continue;
				}
				else
				{
					// Error handling code.
					debugf("HTTP headers not fetched");
					return;
				}
			}
		}
		break;
	}

	debugf("HTTP headers: %s", (char *)lpOutBuffer);
	free(lpOutBuffer);
}

// Check status from HTTP response:
//   200 for good connection
//   <anything else> if connection failed
static int CheckStatus(HINTERNET hURL)
{
	DWORD dwCode, dwSize;
	dwSize = sizeof (DWORD);

	debugf("Checking status");

	if (!HttpQueryInfo(
			hURL,
			HTTP_QUERY_STATUS_CODE | HTTP_QUERY_FLAG_NUMBER,
			&dwCode, &dwSize, NULL))
	{
		errorf("HttpQueryInfo failed [%d]", (int)GetLastError());
		return -1;
	}

	if (dwCode < 200 || dwCode > 299)
	{
		errorf("Invalid status in HTTP response: %d", (int)dwCode);
		DumpHeaders(hURL);
		return -1;
	}
	if (dwCode != 200)
	{
		debugf("Unexpected HTTP status code %d, continuing", (int)dwCode);
		DumpHeaders(hURL);
	}

	return 0;
}


static int GetLength(HINTERNET hURL)
{
	DWORD dwLen, dwSize;
	dwSize = sizeof (DWORD);

	if (!HttpQueryInfo(
			hURL,
			HTTP_QUERY_CONTENT_LENGTH | HTTP_QUERY_FLAG_NUMBER,
			&dwLen, &dwSize, NULL))
	{
		errorf("HttpQueryInfo for Content-Length failed [%d]", (int)GetLastError());
		return 0;
	}

	return dwLen;
}


// Read response data and add to response queue.
static int BufferResult(HTTPINFO * info, HINTERNET hURL)
{
	char out[HTTP_BUFF_SIZE];
	DWORD dwBytes = 0;
	BOOL result;
	int numRead = 0;
	int readLen = 0;

	debugf("Reading response");

	// Read response length (we only handle response with explicit length)
	readLen = GetLength(hURL);

	while (numRead < readLen)
	{
		int remaining = readLen - numRead;

		// Get result.
		result = InternetReadFile(
					hURL,
					out,
					remaining > sizeof(out) ? sizeof(out) : remaining,
					&dwBytes);
		if (result == FALSE)
		{
			errorf("InternetReadFile failed [%d]", (int)GetLastError());
			return -1;
		}
		if (dwBytes > 0)
		{
			BUFFER * buff = calloc(1, sizeof(BUFFER));

			EnterCriticalSection(&info->lock);

			memcpy(buff->data, out, dwBytes);
			buff->len = dwBytes;
			InsertBuffer(&info->recv_queue, buff);

			LeaveCriticalSection(&info->lock);

			// Got data, reset counters
			info->empty_count = 0;
			info->last = 0;
			info->fast_poll = TRUE;

			numRead += dwBytes;

			debugf("Received %d bytes", (int)dwBytes);
		}
	}

	if (numRead == 0)
	{
		// No data, start counters
		info->last = clock() / (CLOCKS_PER_SEC/1000);
		if (++info->empty_count >= HTTP_START_SLOW_COUNT)
		{
			info->fast_poll = FALSE;
		}
	}

	info->out_sequence += numRead;

	debugf("Received %d bytes from server", numRead);

	return 0;
}

static BOOL RetryRequest(DWORD error)
{
	switch (error)
	{
	case ERROR_INTERNET_TIMEOUT:
	case ERROR_INTERNET_CANNOT_CONNECT:
	case ERROR_INTERNET_CONNECTION_ABORTED:
	case ERROR_INTERNET_CONNECTION_RESET:
		return TRUE;
	}

	return FALSE;
}

#endif

#endif
