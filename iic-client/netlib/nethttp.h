/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

// nethttp.h: platform agnostic interface for HTTP commands.
//
//////////////////////////////////////////////////////////////////////

#ifndef _NETHTTP_H
#define _NETHTTP_H

//////////////////////////////////////////////////////////////////////
// Uncomment the DEBUG_LOG definition below in order to enable
// diagnostics output.
#define DEBUG_LOG

//////////////////////////////////////////////////////////////////////
// Uncomment the DEBUG_LOG_VERBOSE definition below for additional diagnostics output.
// Note: This flag is only used when DEBUG_LOG is also defined.
//#define DEBUG_LOG_VERBOSE

#ifdef __cplusplus
extern "C" {
#endif    //  #ifdef __cplusplus


#ifdef WIN32

#include <stdio.h>
#include <stdlib.h>

#include "wininet.h"

#include <stdbool.h>

typedef HINTERNET HREQUEST;

HINTERNET netHttpOpen( LPCSTR appName, int iTimeout, bool fUseNTLM );
void netHttpClose( HINTERNET hSession, HINTERNET hConnection );
HINTERNET netHttpConnect( HINTERNET hSession, LPCSTR aUrl, LPCSTR aHost, unsigned short aPort, LPCSTR aName,
                          LPCSTR aPass );
HINTERNET netHttpOpenRequest( HINTERNET hConnection, const char *aVerb, const char *aObjName, const char *aVersion, DWORD dwFlags );
bool netHttpSendRequest( HINTERNET hRequest, HINTERNET hSession, const char *aHeaders, DWORD dwHeadersLen, const char *aOptional, DWORD dwOptionalLen );
bool netHttpQueryResultsReady( HINTERNET hRequest, bool main_thread );
bool netHttpQueryInfo( HINTERNET hRequest, DWORD dwFlags, LPVOID pStatusCode, LPDWORD pdwBufferLength, LPDWORD pdwIndex );
bool netInternetReadFileExA( HINTERNET hRequest, INTERNET_BUFFERSA *pOutput );
DWORD netInternetErrorDlg( void *hParent, HINTERNET hRequest, DWORD dwError );
bool netInternetCloseHandle( HINTERNET hRequest );
bool netInternetCloseConnection( HINTERNET hConnection );
bool netHttpAddRequestHeaders( HINTERNET hRequest, const char *szHeader, DWORD dwHeaderLen );
bool netHttpEndRequest( HINTERNET hRequest );
bool netInternetGetLastResponseInfo( LPDWORD pdwError, char *szBuf, LPDWORD pdwLength );
bool netHttpSendRequestEx( HINTERNET hRequest, HINTERNET hSession, LPINTERNET_BUFFERS Output, LPINTERNET_BUFFERS Input );
bool netInternetWriteFile( HINTERNET hRequest, LPCVOID pvData, DWORD dwOut, LPDWORD pdwBytesWritten );



#else       //*****************************************************************************

// Linux and Mac OS X

#include "httpbuffer.h"

#include <time.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/tcp.h>

#include <stdbool.h>
#include <string.h>

#include <curl/curl.h>
#include <errno.h>

#include <pthread.h>

    
#define CERT_TEXT_SIZE 4096
#define PROXY_TEXT_SIZE 512

typedef struct _PROXY
{
    unsigned        type;		// PROXY, SOCKS
    char            url[PROXY_TEXT_SIZE];
    char            server[PROXY_TEXT_SIZE];
    unsigned        port;
    char            username[PROXY_TEXT_SIZE];
    char            password[PROXY_TEXT_SIZE];
    struct _PROXY   *next;
} PROXY;
    
typedef struct _SESSION
{
    int use_ntlm;
    int timeout;
    char *cafile;
} SESSION;
typedef struct _SESSION * HSESSION;

typedef struct _CONNECTION
{
    HSESSION session;
    char *host;
    int  port;
    char *auth;
    char *proxy_auth;
    PROXY *proxy;
} CONNECTION;
typedef struct _CONNECTION * HCONNECTION;

typedef struct _REQUEST
{
  HCONNECTION connection;
  CURL *hcurl;
  char *verb;
  char *url;
  int flags;
  struct curl_slist * headers;
  CURLcode retCode;                       // Error code returned by curl operations
  char szErrorText[CURL_ERROR_SIZE+1];    // Error text returned by curl operations
  pthread_mutex_t m_mutex;                // Used to protect access to m_pBUFFERRecv
  BUFFER *m_pBUFFERRecv;                  // Ptr. to the buffer that will receive downloaded data
  size_t totalBytesReceived;              // The total number of bytes written to m_pBUFFERRecv
  size_t expectedContentLength;           // The expected content length (From the HTTP header)
  bool fRequestComplete;                  // Flag: The request is complete ( curl_easy_perform() call returned )
  long statusCode;                        // The HTTP status code
  char *statusCodeText;                   // The text associated with the HTTP status code
  BUFFER *m_pBUFFERSend;                  // Ptr. to buffer of data to be sent
  void *sendBuffer;
  int sendLength;
  char szCertText[CERT_TEXT_SIZE];
} REQUEST;
typedef struct _REQUEST * HREQUEST;


typedef struct _INTERNET_BUFFERSA
{
    unsigned long       dwStructSize;
    void                *Next;
    char                *lpvBuffer;
    unsigned long       dwBufferLength;
    unsigned long       dwBufferTotal;
}   INTERNET_BUFFERSA, *LPINTERNET_BUFFERSA;


typedef struct _INTERNET_BUFFERS
{
    unsigned long       dwStructSize;
    void                *Next;
    void                *lpvBuffer;
    unsigned long       dwBufferLength;
    unsigned long       dwBufferTotal;
}   INTERNET_BUFFERS, *LPINTERNET_BUFFERS;


#define LPVOID                  void *
#define S_OK                    0
#define E_ABORT                 ((long) 0x80004004L)
#define E_FAIL                  ((unsigned long) 0x80004005L)


#define MAX_PATH                260

#define HTTP_QUERY_CONTENT_LENGTH   5
#define HTTP_QUERY_STATUS_CODE      19
#define HTTP_QUERY_STATUS_TEXT      20
#define HTTP_QUERY_RAW_HEADERS_CRLF 22

#define INTERNET_DEFAULT_HTTP_PORT      80
#define INTERNET_DEFAULT_HTTPS_PORT     443

#define ERROR_INSUFFICIENT_BUFFER   122L
#define ERROR_CANCELLED             1223L

#define INTERNET_FLAG_RELOAD                    0x80000000
#define HTTP_QUERY_FLAG_NUMBER                  0x20000000
#define INTERNET_FLAG_NO_CACHE_WRITE            0x04000000
#define INTERNET_FLAG_DONT_CACHE                INTERNET_FLAG_NO_CACHE_WRITE
#define INTERNET_FLAG_SECURE                    0x00800000
#define INTERNET_FLAG_KEEP_CONNECTION           0x00400000
#define INTERNET_FLAG_IGNORE_CERT_DATE_INVALID  0x00002000
#define INTERNET_FLAG_IGNORE_CERT_CN_INVALID    0x00001000
#define INTERNET_FLAG_PRAGMA_NOCACHE            0x00000100


#define INTERNET_ERROR_BASE                     12000
#define ERROR_INTERNET_OUT_OF_HANDLES           (INTERNET_ERROR_BASE+1)
#define ERROR_INTERNET_TIMEOUT                  (INTERNET_ERROR_BASE+2)
#define ERROR_INTERNET_EXTENDED_ERROR           (INTERNET_ERROR_BASE+3)
#define ERROR_INTERNET_INTERNAL_ERROR           (INTERNET_ERROR_BASE+4)
#define ERROR_INTERNET_INVALID_URL              (INTERNET_ERROR_BASE+5)
#define ERROR_INTERNET_UNRECOGNIZED_SCHEME      (INTERNET_ERROR_BASE+6)
#define ERROR_INTERNET_NAME_NOT_RESOLVED        (INTERNET_ERROR_BASE+7)
#define ERROR_INTERNET_PROTOCOL_NOT_FOUND       (INTERNET_ERROR_BASE+8)
#define ERROR_INTERNET_INVALID_OPTION           (INTERNET_ERROR_BASE+9)
#define ERROR_INTERNET_BAD_OPTION_LENGTH        (INTERNET_ERROR_BASE+10)
#define ERROR_INTERNET_OPTION_NOT_SETTABLE      (INTERNET_ERROR_BASE+11)
#define ERROR_INTERNET_SHUTDOWN                 (INTERNET_ERROR_BASE+12)
#define ERROR_INTERNET_INCORRECT_USER_NAME      (INTERNET_ERROR_BASE+13)
#define ERROR_INTERNET_INCORRECT_PASSWORD       (INTERNET_ERROR_BASE+14)
#define ERROR_INTERNET_LOGIN_FAILURE            (INTERNET_ERROR_BASE+15)
#define ERROR_INTERNET_INVALID_OPERATION        (INTERNET_ERROR_BASE+16)
#define ERROR_INTERNET_OPERATION_CANCELLED      (INTERNET_ERROR_BASE+17)
#define ERROR_INTERNET_INCORRECT_HANDLE_TYPE    (INTERNET_ERROR_BASE+18)
#define ERROR_INTERNET_INCORRECT_HANDLE_STATE   (INTERNET_ERROR_BASE+19)
#define ERROR_INTERNET_NOT_PROXY_REQUEST        (INTERNET_ERROR_BASE+20)
#define ERROR_INTERNET_REGISTRY_VALUE_NOT_FOUND (INTERNET_ERROR_BASE+21)
#define ERROR_INTERNET_BAD_REGISTRY_PARAMETER   (INTERNET_ERROR_BASE+22)
#define ERROR_INTERNET_NO_DIRECT_ACCESS         (INTERNET_ERROR_BASE+23)
#define ERROR_INTERNET_NO_CONTEXT               (INTERNET_ERROR_BASE+24)
#define ERROR_INTERNET_NO_CALLBACK              (INTERNET_ERROR_BASE+25)
#define ERROR_INTERNET_REQUEST_PENDING          (INTERNET_ERROR_BASE+26)
#define ERROR_INTERNET_INCORRECT_FORMAT         (INTERNET_ERROR_BASE+27)
#define ERROR_INTERNET_ITEM_NOT_FOUND           (INTERNET_ERROR_BASE+28)
#define ERROR_INTERNET_CANNOT_CONNECT           (INTERNET_ERROR_BASE+29)
#define ERROR_INTERNET_CONNECTION_ABORTED       (INTERNET_ERROR_BASE+30)
#define ERROR_INTERNET_CONNECTION_RESET         (INTERNET_ERROR_BASE+31)
#define ERROR_INTERNET_FORCE_RETRY              (INTERNET_ERROR_BASE+32)
#define ERROR_INTERNET_INVALID_PROXY_REQUEST    (INTERNET_ERROR_BASE+33)
#define ERROR_INTERNET_NEED_UI                  (INTERNET_ERROR_BASE+34)
#define ERROR_INTERNET_HANDLE_EXISTS            (INTERNET_ERROR_BASE+36)
#define ERROR_INTERNET_SEC_CERT_DATE_INVALID    (INTERNET_ERROR_BASE+37)
#define ERROR_INTERNET_SEC_CERT_CN_INVALID      (INTERNET_ERROR_BASE+38)
#define ERROR_INTERNET_HTTP_TO_HTTPS_ON_REDIR   (INTERNET_ERROR_BASE+39)
#define ERROR_INTERNET_HTTPS_TO_HTTP_ON_REDIR   (INTERNET_ERROR_BASE+40)
#define ERROR_INTERNET_MIXED_SECURITY           (INTERNET_ERROR_BASE+41)
#define ERROR_INTERNET_CHG_POST_IS_NON_SECURE   (INTERNET_ERROR_BASE+42)
#define ERROR_INTERNET_POST_IS_NON_SECURE       (INTERNET_ERROR_BASE+43)
#define ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED  (INTERNET_ERROR_BASE+44)
#define ERROR_INTERNET_INVALID_CA               (INTERNET_ERROR_BASE+45)
#define ERROR_INTERNET_CLIENT_AUTH_NOT_SETUP    (INTERNET_ERROR_BASE+46)
#define ERROR_INTERNET_ASYNC_THREAD_FAILED      (INTERNET_ERROR_BASE+47)
#define ERROR_INTERNET_REDIRECT_SCHEME_CHANGE   (INTERNET_ERROR_BASE+48)
#define ERROR_HTTP_HEADER_NOT_FOUND             (INTERNET_ERROR_BASE+150)
#define ERROR_INTERNET_SECURITY_CHANNEL_ERROR   (INTERNET_ERROR_BASE+157)

/**
 * Initialize the interface library (curl).<br>
 * This function should be called one time at application startup.
 */
void netInitializeLibrary();

/**
 * Cleanup the interface library (curl).<br>
 * This function shluld be called one time at application shutdown.
 */
void netReleaseLibrary();

HSESSION netHttpOpen( const char *appName, int iTimeout, bool fUseNTLM );
bool     netSetSecurityOptions( HSESSION aSession, const char *aCAFile);
HCONNECTION netHttpConnect( HSESSION aSession, const char *aUrl, const char *aHost,
                            unsigned short aPort, const char *aName, const char *aPass );
HREQUEST netHttpOpenRequest( HCONNECTION aConnection, const char *aVerb, const char *aObjName,
                            const char *aVersion, unsigned long flags );
bool    netHttpSendRequest( HREQUEST pMsg, HSESSION pSession, const char * aHeaders, unsigned long dwHeadersLen,
                         const char *aOptional, unsigned long dwOptionalLen );
bool    netHttpSendRequestEx( HREQUEST pMsg, HSESSION pSession, LPINTERNET_BUFFERS Output, LPINTERNET_BUFFERS Input );
bool    netHttpEndRequest( HREQUEST pMsg );
bool    netInternetCloseHandle( HREQUEST aRequest );
bool    netInternetCloseConnection( HCONNECTION aConnection );
bool    netHttpAddRequestHeaders( HREQUEST aReq, const char *szHeader, unsigned long dwHeaderLen );
bool    netHttpClose( HSESSION pSession, HCONNECTION pConnection );
bool    netInternetReadFileExA( HREQUEST pMsg, INTERNET_BUFFERSA *IB );
bool    netHttpQueryInfo( HREQUEST aReq, unsigned long dwFlags, char *pStatusCode, unsigned long *pdwBufferLength, unsigned long *pdwIndex );
bool    netHttpQueryResultsReady( HREQUEST aReq, bool main_thread );
int     GetLastError( );
bool    netInternetWriteFile( HREQUEST pMsg, const void *pvData, unsigned long dwOut, unsigned long *pdwBytesWritten );
bool    netInternetGetLastResponseInfo( unsigned long *pdwError, char *szBuf, unsigned long *pdwLength );
unsigned long netInternetErrorDlg( void *hWin, HREQUEST pMsg, unsigned long dwError );
const char *netGetServerCertificate(HREQUEST pReq);

HREQUEST new_request( HCONNECTION aConn, const char *aVerb, unsigned long flags );
void delete_request( HREQUEST req );
void clear_request( HREQUEST req );

#endif      //  #ifdef WIN32

#ifdef __cplusplus
}
#endif      //  #ifdef __cplusplus

#endif      //  #ifndef _NETHTTP_H
