/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

char *logdir = NULL;
static FILE *logf = NULL;

// The number of log files to keep before deleting the oldest
static unsigned history_len = 10;

// If the current log is >= kMaxLogFileSize, rotate the current log
// into the stack of historical files and create a new log file.
static int kMaxLogFileSize = 100 * 1024;

#define MAX_LOG_FILENAME    (512)

static void log_rotate_if_needed( const char *logFN, int iAlwaysRotate )
{
    char newerfn[MAX_LOG_FILENAME];
    char olderfn[MAX_LOG_FILENAME];
    int i;
    long    sz      = -1;

    struct stat myStats;
    if( stat( logFN, &myStats )  ==  0 )
        sz = myStats.st_size;

    if( (sz < kMaxLogFileSize)  &&  !iAlwaysRotate )
        return;

    sprintf( newerfn, "%s.%u", logFN, history_len );
    unlink( newerfn );
    for( i = history_len-1; i > 0; i--)
    {
        strcpy( olderfn, newerfn );
        sprintf( newerfn, "%s.%u", logFN, i );
        rename( newerfn, olderfn );
    }
    strcpy(olderfn, newerfn);
    strcpy(newerfn, logFN);
    rename( newerfn, olderfn );
}

void net_write_stamp(int level, const char *file, int line)
{
    char buff[256];

    if (logf != NULL)
    {
        time_t now = time(NULL);
        struct tm *now_tm = localtime(&now);
        int pos;

        const char *f = strrchr(file, '\\');
        if (f == NULL)
            f = strrchr(file, '/');
        if (f == NULL)
            f = file;
        else
            ++f;

        pos = strftime(buff, sizeof(buff), /*"%m/%d/%y "*/ "%H:%M:%S", now_tm);
        sprintf(buff + pos, " %s:%d ", f, line);
        fwrite(buff, strlen(buff), 1, logf);
    }
}

void net_write_log(const char *format,...)
{
    char buff[1024];

    if (logf != NULL)
    {
        va_list args;

        va_start(args, format);

        vsnprintf(buff, sizeof(buff), format, args);
        fprintf(logf, "%s\n", buff);
        fflush(logf);

        va_end(args);
    }
}

void net_init_log(const char *directory, int log_to_stdout)
{
    logdir = calloc(strlen(directory) + 20, 1);

    if (log_to_stdout)
    {
        sprintf(logdir, "%s/session.log", directory);

        log_rotate_if_needed( logdir, 0 );

        logf = fopen(logdir, "a");
        net_write_log("\r\n");
        net_write_log("Opening log\r\n");
    }
    else
    {
        logf = stdout;
    }
}
