/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

// Auto detect proxy server
// Detect location of JS configuration file, download, and execute.

// Returns strings of the following form:
// PROXY 10.0.1.1:8080
// DIRECT
// SOCKS 10.0.1.1:8080
// or list of these strings
// PROXY 1.1.1.1:8080; DIRECT

#ifdef WIN32

#define WIN32_LEAN_AND_MEAN	1
#include <windows.h>
#include <wininet.h>
//#include <urlmon.h>
#include <stdio.h>
#include <winsock2.h>
#include <ws2tcpip.h>

#include "autoproxy.h"
#include "proxydetect.h"
#include "log.h"

/* ==================================================================
                            HELPER FUNCTIONS
   ================================================================== */

/////////////////////////////////////////////////////////////////////
//  ResolveHostName                               (a helper function)
/////////////////////////////////////////////////////////////////////
DWORD __stdcall ResolveHostName( LPSTR   lpszHostName,
                                 LPSTR   lpszIPAddress,
                                 LPDWORD lpdwIPAddressSize )
{
  DWORD dwIPAddressSize;
  DWORD error;
  struct in_addr ip_addr;
  struct hostent *hp;
  char *szAddress;

  if ((ip_addr.S_un.S_addr = inet_addr(lpszHostName)) == INADDR_NONE)
  {
    hp = gethostbyname (lpszHostName);
    if (hp == NULL || hp->h_addrtype != AF_INET)
	{
      error = ERROR_INTERNET_NAME_NOT_RESOLVED;
	  goto quit;
	}

	memcpy (&ip_addr, hp->h_addr, hp->h_length);
  }

  szAddress = inet_ntoa(ip_addr);

  dwIPAddressSize = strlen(szAddress);
  if( ( *lpdwIPAddressSize < dwIPAddressSize ) ||
      ( lpszIPAddress == NULL ) )
  {
    *lpdwIPAddressSize = dwIPAddressSize + 1;
    error = ERROR_INSUFFICIENT_BUFFER;
    goto quit;
  }

  strcpy(lpszIPAddress, szAddress);

  error = 0;

quit:
  return( error );
}


/////////////////////////////////////////////////////////////////////
//  IsResolvable                                  (a helper function)
/////////////////////////////////////////////////////////////////////
BOOL __stdcall IsResolvable( LPSTR lpszHost )
{
  char szDummy[255];
  DWORD dwDummySize = sizeof(szDummy) - 1;

  if( ResolveHostName( lpszHost, szDummy, &dwDummySize ) )
    return( FALSE );
  return TRUE;
}


/////////////////////////////////////////////////////////////////////
//  GetIPAddress                                  (a helper function)
/////////////////////////////////////////////////////////////////////
DWORD __stdcall GetIPAddress( LPSTR   lpszIPAddress,
                              LPDWORD lpdwIPAddressSize )
{
  char szHostBuffer[255];

  if( gethostname( szHostBuffer, sizeof(szHostBuffer) - 1 ) != ERROR_SUCCESS )
    return( ERROR_INTERNET_INTERNAL_ERROR );
  return( ResolveHostName( szHostBuffer, lpszIPAddress, lpdwIPAddressSize ) );
}


/////////////////////////////////////////////////////////////////////
//  IsInNet                                       (a helper function)
/////////////////////////////////////////////////////////////////////
BOOL __stdcall IsInNet( LPSTR lpszIPAddress, LPSTR lpszDest, LPSTR lpszMask )
{
  DWORD dwDest;
  DWORD dwIpAddr;
  DWORD dwMask;

  dwIpAddr = inet_addr( lpszIPAddress );
  dwDest   = inet_addr( lpszDest );
  dwMask   = inet_addr( lpszMask );

  if( ( dwDest == INADDR_NONE ) ||
      ( dwIpAddr == INADDR_NONE ) ||
      ( ( dwIpAddr & dwMask ) != dwDest ) )
    return( FALSE );

  return( TRUE );
}


/* ==================================================================
                        Detect Proxy Settings
   ================================================================== */
int DetectProxy(char *url, char *lpszProxy, unsigned dwProxyLen)
{
#ifdef USE_AUTOPROXY
  char host[256] = {0};
  char WPADLocation[1024]= "";
  char TempPath[MAX_PATH];
  char TempFile[MAX_PATH];
  char proxyBuffer[1024];
  char* proxy = proxyBuffer;
  ZeroMemory( proxy, 1024 );
  DWORD dwProxyHostNameLength = 1024;
  DWORD returnVal;
  HMODULE hModJS;
  URL_COMPONENTSA urlComponents;

  // Declare and populate an AutoProxyHelperVtbl structure, and then
  // place a pointer to it in a containing AutoProxyHelperFunctions
  // structure, which will be passed to InternetInitializeAutoProxyDll:
  AutoProxyHelperVtbl Vtbl =
  {
    IsResolvable,
    GetIPAddress,
    ResolveHostName,
    IsInNet
  };
  AutoProxyHelperFunctions HelperFunctions = { &Vtbl };

  // Declare function pointers for the three autoproxy functions
  pfnInternetInitializeAutoProxyDll    pInternetInitializeAutoProxyDll;
  pfnInternetDeInitializeAutoProxyDll  pInternetDeInitializeAutoProxyDll;
  pfnInternetGetProxyInfo              pInternetGetProxyInfo;


  if( !( hModJS = LoadLibraryA( "jsproxy.dll" ) ) )
    return FAIL;

  if( !( pInternetInitializeAutoProxyDll = (pfnInternetInitializeAutoProxyDll)
                GetProcAddress( hModJS, "InternetInitializeAutoProxyDll" ) ) ||
      !( pInternetDeInitializeAutoProxyDll = (pfnInternetDeInitializeAutoProxyDll)
                GetProcAddress( hModJS, "InternetDeInitializeAutoProxyDll" ) ) ||
      !( pInternetGetProxyInfo = (pfnInternetGetProxyInfo)
                GetProcAddress( hModJS, "InternetGetProxyInfo" ) ) )
    return FAIL;

  if( !DetectAutoProxyUrl( WPADLocation, sizeof(WPADLocation),
                           PROXY_AUTO_DETECT_TYPE_DHCP |
                             PROXY_AUTO_DETECT_TYPE_DNS_A ) )
    return NOPROXY;

  debugf("WPAD Location is: %s", WPADLocation );

  GetTempPathA( sizeof(TempPath)/sizeof(TempPath[0]), TempPath );
  GetTempFileNameA( TempPath, NULL, 0, TempFile );
  URLDownloadToFileA( NULL, WPADLocation, TempFile, NULL, NULL );

  if( !( returnVal = pInternetInitializeAutoProxyDll( 0, TempFile, NULL,
                                                      &HelperFunctions, NULL ) ) )
    return FAIL;

  debugf( "InternetInitializeAutoProxyDll returned: %d", returnVal);

  // Delete the temporary file
  // (or, to examine the auto-config script, comment out the
  // file delete and substitute the following printf call)
  // printf( "\n  The auto-config script temporary file is:\n    %s\n", TempFile );
  DeleteFileA( TempFile );

  // Get host name from URL
  memset(&urlComponents, 0, sizeof(urlComponents));
  urlComponents.dwStructSize = sizeof(urlComponents);
  urlComponents.lpszHostName = host;
  urlComponents.dwHostNameLength = sizeof(host);
  if( !InternetCrackUrlA( url, 0, 0, &urlComponents) )
	return BADURL;

  if( !pInternetGetProxyInfo( (LPSTR) url,  strlen( url),
                              (LPSTR) host, strlen( host),
                              &proxy, &dwProxyHostNameLength ) )
  {
      DWORD     dwT = GetLastError( );
      debugf("Unable to determine proxy info [%d]", dwT );
      return NOPROXY;
  }

  debugf("Proxy is: %s", proxy );

  strncpy(lpszProxy, proxy, dwProxyLen - 1);

  if( !pInternetDeInitializeAutoProxyDll( NULL, 0 ) )
    return FAIL;

  return( 0 );

#else
    return NOPROXY;
#endif
}

#endif
