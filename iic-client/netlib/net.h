/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef NET_H
#define NET_H

#include "port.h"

#ifdef __cplusplus
extern "C" {
#endif

/*error codes*/
typedef enum
{
	IERR_NET_FAILED=-1,
	IERR_NET_SUCCESS=0,
	IERR_NET_MEMORY_FAIL,        // malloc failed
	IERR_NET_SOCKET_FAIL,        // look errno for more
	IERR_NET_NOTREACHABLE,
	IERR_NET_BLOCKED,
	IERR_NET_AUTHFAILED,
	IERR_NET_AUTHREQUIRED,
	IERR_NET_PROXYAUTHFAILED,
	IERR_NET_PROXYAUTHREQUIRED,
	IERR_NET_INVALIDRESPONSE,
	IERR_NET_FAILEDTOINITSECURITYMODULE,
	IERR_NET_ACCESSDENIED,
	IERR_NET_HOSTNOTFOUND,
	IERR_NET_CONNECTTIMEDOUT,
} ERR_CODE;

char* net_err_string(int err);

typedef enum {NO_PROXY,HTTP_TYPE,SOCKS4_TYPE,SOCKS5_TYPE,SOCKS_TYPE,DETECT_TYPE} proxy_types;
typedef enum {AUTH_NONE,AUTH_BASIC,AUTH_DIGEST,AUTH_NTLM,AUTH_NEGOTIATE} auth_types;

// Initialize network lib (proxy.iic tracking file is written to config_path)
int net_initialize(const char *config_path);

// Create a network lib socket (the returned SOCKET is not compatible with OS sockets and must be used with net_xxx functions)
SOCKET net_socket();

// Resolve host name (may block)
BOOL net_resolve_hostname(const char *server, struct sockaddr_in *sin);

// Hunt for connection to specified server
int net_hunt(SOCKET socket, const char *server, int default_port, BOOL proxy_only);

// Connect to server; doesn't hunt for port or mode, but can be set into http mode
int net_connect(SOCKET socket, const char * server, const struct sockaddr *addr, int len, int direct);

// Enable use of proxy server (should be called before net_hunt or net_connect are called)
void net_set_server(proxy_types type, const char *target_server);

// Set authentication for proxy server
void net_set_auth(auth_types type, int current_user, const char *user, const char *passwd);

// Set global "always use HTTP tunneling" flag
void net_set_http(BOOL flag);

// Get global "always use HTTP tunneling" flag
BOOL net_get_http();

// Skip to next port in hunt sequence for next connect attempt
int  net_set_next_hunt_port();

// Remember this as a successful connection (call after exchange data with server, e.g. protocol headers)
void net_set_connected();

// Retry same port on next connection attempt
void net_set_retry_last();

// Reset hunt sequence to start
void net_reset_hunt();

// Enable/disable TCP delay
void net_set_nagle_algorithm(SOCKET sock, BOOL enable);

// Enable/disable use of primary port
void net_enable_primary_port(BOOL enable);

// Enable port hunitng
void net_enable_port_hunting(BOOL enable);

// Replacements for standard IO functions (read, write, select, etc.)
int net_recv(SOCKET socket, char * buffer, int len);
int net_send(SOCKET socket, char * buffer, int len);
int net_checkreadable(SOCKET socket, int timeout);
int net_checkwriteable(SOCKET socket, int timeout);
int net_close(SOCKET socket, BOOL clean_shutdown);
void net_set_nonblock(SOCKET socket, int enable);
BOOL net_set_timeout(SOCKET socket, int timeout);

// Set threading mode--even if socket is set non-blocking, HTTP requests will block for a bit until the response
// is received.  To let application main loop run during these periods, use this function.
// Default is not running on the main thread, and no polling hook.
void net_set_thread_mode(BOOL main_thread, void (*polling_hook)());

#ifdef __cplusplus
}
#endif

#endif
