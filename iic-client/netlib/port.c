/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include <stdio.h>

#include "port.h"

void _set_nonblock(int fd, int act)
{
#if defined(LINUX) || defined(MACOSX)
	int flags;

	flags = fcntl(fd, F_GETFL, 0);
	if(flags < 0) return;

	if(act)
		flags |= O_NONBLOCK;
	else
		flags &= ~O_NONBLOCK;

	fcntl(fd, F_SETFL, flags);
#else // _WIN32
	unsigned long flag = act;
	ioctlsocket(fd, FIONBIO, &flag);
#endif
}

void _set_nodelay(int fd, int act)
{
    setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (const char *) &act, sizeof(int));
}

int _gettimeofday(struct timeval * tv)
{
#if defined(LINUX) || defined(MACOSX)
	struct timezone tz;
	return gettimeofday(tv, &tz);
#else
	time_t now;

	time(&now);

	tv->tv_sec = now;
	tv->tv_usec = 0;
	return 0;
#endif
}

#if defined(LINUX) || defined(MACOSX)
// Return microseconds of elapsed time
long _clock()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * 1000000 + tv.tv_usec;
}

int sleep_millis(int ms)
{
	struct timespec tv = { ms / 1000, ms % 1000 * 1000000 };
	nanosleep(&tv, NULL);
	return 0;
}
#endif

#ifdef _WIN32
int inet_aton(const char * cp, struct in_addr * ip)
{
	unsigned long l_ip;

	l_ip = inet_addr(cp);
	if (l_ip == INADDR_NONE)
		return -1;
	ip->S_un.S_addr = l_ip;
	return 0;
}

const char * local_strerror(int error_code)
{
	static char buffer[1024];

	buffer[0] = '\0';

	FormatMessage(
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		error_code,
		0, // Default language
		(LPTSTR) buffer,
		sizeof(buffer),
		NULL
	);

	return buffer;
}

#endif
