
/* Read system configured proxy. */

#ifndef HTTP_PROXY_H
#define HTTP_RROXY_H

#define MAX_PROXY_STRING 512

typedef struct {
    char url[MAX_PROXY_STRING];
    
	char host[MAX_PROXY_STRING];
	unsigned port;
	
	char username[MAX_PROXY_STRING];
	char password[MAX_PROXY_STRING];
	
} HttpProxyInfo;


int proxy_for_uri  ( const char *uri, HttpProxyInfo *proxy_info );

void proxy_init     ( void );
void proxy_shutdown ( void );

int proxy_is_proxy_configured( );

#endif
