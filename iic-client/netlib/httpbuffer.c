/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */


#include <stdlib.h>
#include <memory.h>

#include "httpbuffer.h"

// Insert new buffer at tail of queue.
void InsertBuffer(BUFFER ** head, BUFFER * new_buff)
{
	BUFFER *cur = *head;

	if (*head == NULL)
	{
		*head = new_buff;
		return;
	}

	while (cur->next != NULL)
		cur = cur->next;

	cur->next = new_buff;
}


// Remove buffer from head of queue, and returns new head.
BUFFER * RemoveBuffer(BUFFER ** head)
{
	BUFFER *cur = *head;
	*head = cur->next;
	free(cur);

	return *head;
}


// Store data in buffer queue.
int FillBuffer(BUFFER ** head, char * data, int len)
{
	BUFFER * cur;
	int copied = 0;
	int remaining_in_buff;

	if( len <= 0 )
	  return 0;

	// Get to last buffer
	cur = *head;
	while (cur != NULL && cur->next != NULL)
		cur = cur->next;

	while (copied < len)
	{
		if (cur == NULL)
		{
			cur = calloc(1, sizeof(BUFFER));
			InsertBuffer(head, cur);
		}

		remaining_in_buff = sizeof(cur->data) - cur->len;
		if (remaining_in_buff > 0)
		{
			int num_to_copy = remaining_in_buff > len - copied ?
							  len - copied : remaining_in_buff;

			memcpy(cur->data + cur->len, data + copied, num_to_copy);
			copied += num_to_copy;
			cur->len += num_to_copy;
		}
		else
			cur = NULL;
	}

	return copied;
}


// Copy buffered data from queue to supplied buffer.
int ReadBuffer(BUFFER ** head, char * data, int len)
{
	BUFFER * cur;
	int copied = 0;

	cur = *head;
	while (cur != NULL && copied < len)
	{
		int remaining_in_buff = cur->len - cur->pos;
		if (remaining_in_buff > 0)
		{
			int num_to_copy = remaining_in_buff > len - copied ?
							  len - copied : remaining_in_buff;

            if (data)
                memcpy(data + copied, cur->data + cur->pos, num_to_copy);
			copied += num_to_copy;
			cur->pos += num_to_copy;

			if (cur->pos == cur->len)
				cur = RemoveBuffer(head);
		}
	}

	return copied;
}


void ClearBuffer(BUFFER ** head)
{
    while (*head)
        RemoveBuffer(head);
}
