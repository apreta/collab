
//#include <arpa/inet.h>
//#include <assert.h>
//#include <errno.h>
//#include <netdb.h>
#include <netinet/in.h>
//#include <stdio.h>
//#include <stdlib.h>
//#include <sys/socket.h>
//#include <unistd.h>

#include <CoreServices/CoreServices.h>
#include <SystemConfiguration/SystemConfiguration.h>

#include "proxy.h"
#include "log.h"


#define kPrivateRunLoopMode CFSTR("org.apreta.ProxyConfig")


static HttpProxyInfo *proxy_info;


static void CFQRelease(CFTypeRef cf)
{
	if (cf != NULL) {
		CFRelease(cf);
	}
}

static void CopyCFString(CFStringRef str, char *buffer, int bufflen)
{
	Boolean			success;
	
	assert(str != NULL);

    buffer[0] = '\0';
	
	success = CFStringGetCString(str, buffer, bufflen, kCFStringEncodingUTF8);
	assert(success);
}


// Callback for CFNetworkExecuteProxyAutoConfigurationURL.  client is a 
// pointer to a CFTypeRef.  This stashes either error or proxies in that 
// location.
static void ResultCallback(void * client, CFArrayRef proxies, CFErrorRef error)
{
	CFTypeRef *		resultPtr;
	
	assert( (proxies != NULL) == (error == NULL) );
	
	resultPtr = (CFTypeRef *) client;
	assert( resultPtr != NULL);
	assert(*resultPtr == NULL);
	
	if (error != NULL) {
		*resultPtr = CFRetain(error);
	} else {
		*resultPtr = CFRetain(proxies);
	}
	CFRunLoopStop(CFRunLoopGetCurrent());
}


void SetProxyAuth(char *proxy, unsigned port, SecProtocolType proto)
{
    OSErr status;
    SecKeychainItemRef itemRef;
    UInt32 passwordLen;
    char *password;
    UInt32 proxyLen = strlen(proxy);

    status = SecKeychainFindInternetPassword( NULL, proxyLen, proxy, 0, NULL, 0, NULL, 0, NULL,
                    port, proto, kSecAuthenticationTypeAny, NULL, NULL, &itemRef);
        
    if (status == noErr) {
        SecKeychainAttributeList list;
        SecKeychainAttribute attr;
            
        list.count = 1;
        list.attr = &attr;
        attr.tag = kSecAccountItemAttr;
        status = SecKeychainItemCopyContent(itemRef, NULL, &list, NULL, NULL);

        if (status == noErr) {
            snprintf(proxy_info->username, attr.length + 1, "%s", (char *)attr.data);
        }
                    
        SecKeychainItemFreeContent(&list, NULL);
    }

    if (*proxy_info->username) {
        status = SecKeychainFindInternetPassword(NULL, proxyLen, proxy, 0, NULL, 0, NULL, 0, NULL,
                        port, proto, kSecAuthenticationTypeAny, &passwordLen, (void **)&password, NULL);
            
        if (status == noErr) {
            snprintf(proxy_info->password, passwordLen + 1, "%s", password);
            SecKeychainItemFreeContent(NULL, password);
        }
    }
    
}


static OSStatus SetProxyInfo(CFURLRef url, CFDictionaryRef thisProxy)
{
    OSStatus            err;
    Boolean             success;
    CFStringRef         proxyHost;
    CFNumberRef         proxyPortNum;
    struct sockaddr_in  proxyAddr;

    // Create a (struct sockaddr_in) from the proxy host and port.
    err = noErr;
    proxyHost = CFDictionaryGetValue(thisProxy, kCFProxyHostNameKey);
    if (proxyHost == NULL) {
        err = coreFoundationUnknownErr;
    } else {
        assert(CFGetTypeID(proxyHost) == CFStringGetTypeID());
        CopyCFString(proxyHost, proxy_info->host, sizeof(proxy_info->host));
    }
    if (err == noErr) {
        proxyPortNum = CFDictionaryGetValue(thisProxy, kCFProxyPortNumberKey);
        if (proxyPortNum == NULL) {
            proxy_info->port = 8080;  // If there's no port, default to port 8080.
        } else {
            assert(CFGetTypeID(proxyPortNum) == CFNumberGetTypeID());
            
            success = CFNumberGetValue(proxyPortNum, kCFNumberSInt32Type, &proxy_info->port);
            if ( ! success ) {
                err = coreFoundationUnknownErr;
            }
        }
    }

    return err;
}

static OSStatus CreateProxyListWithExpandedPACProxies(
    CFURLRef        url, 
    CFArrayRef      proxies, 
    CFArrayRef *    expandedProxiesPtr
)
{
    OSStatus			err;
	CFMutableArrayRef	expandedProxies;
	CFIndex				proxyCount;
	CFIndex				proxyIndex;
	
    assert(proxies != NULL);
    assert( expandedProxiesPtr != NULL);
    assert(*expandedProxiesPtr == NULL);

	expandedProxies = NULL;
	
    // Start with an empty results array.
    
    err = noErr;
	expandedProxies = CFArrayCreateMutable(NULL, 0, &kCFTypeArrayCallBacks);
	if (expandedProxies == NULL) {
		err = coreFoundationUnknownErr;
	}
    
    // For each incoming proxy, if it's not a PAC-based proxy, just add the proxy 
    // to the results array.  If it /is/ a PAC-based proxy, run the PAC script 
    // and append its results to the array.
    
	if (err == noErr) {
		proxyCount = CFArrayGetCount(proxies);
		
		for (proxyIndex = 0; proxyIndex < proxyCount; proxyIndex++) {
			CFDictionaryRef		thisProxy;
			CFStringRef			proxyType;
		
			thisProxy = (CFDictionaryRef) CFArrayGetValueAtIndex(proxies, proxyIndex);
			assert(thisProxy != NULL);
			assert(CFGetTypeID(thisProxy) == CFDictionaryGetTypeID());
			
			proxyType = (CFStringRef) CFDictionaryGetValue(thisProxy, kCFProxyTypeKey);
			assert(proxyType != NULL);
			assert(CFGetTypeID(proxyType) == CFStringGetTypeID());
			
			if ( ! CFEqual(proxyType, kCFProxyTypeAutoConfigurationURL) ) {
				// If it's not a PAC proxy, just copy it across.
				
				CFArrayAppendValue(expandedProxies, thisProxy);
			} else {
				CFRunLoopSourceRef		rls;
				CFURLRef				scriptURL;
				CFTypeRef				result;
				CFStreamClientContext	context = { 0, &result, NULL, NULL, NULL };
				
				// If it is a PAC proxy, expand it and copy the results across.
				
				result = NULL;
				
				scriptURL = CFDictionaryGetValue(thisProxy, kCFProxyAutoConfigurationURLKey);
				assert(scriptURL != NULL);
				assert(CFGetTypeID(scriptURL) == CFURLGetTypeID());
				
                // We don't need to work around <rdar://problem/5530166> because 
                // we know that our caller has called CFNetworkCopyProxiesForURL.
                
				rls = CFNetworkExecuteProxyAutoConfigurationURL(scriptURL, url, ResultCallback, &context);
				if (rls == NULL) {
					err = coreFoundationUnknownErr;
				}

                // Despite the fact that CFNetworkExecuteProxyAutoConfigurationURL has 
                // neither a "Create" nor a "Copy" in the name, we are required to 
                // release the return CFRunLoopSourceRef <rdar://problem/5533931>.

				if (err == noErr) {
					CFRunLoopAddSource(CFRunLoopGetCurrent(), rls, kPrivateRunLoopMode);
					
					CFRunLoopRunInMode(kPrivateRunLoopMode, 1.0e10, false);
					
					CFRunLoopRemoveSource(CFRunLoopGetCurrent(), rls, kPrivateRunLoopMode);
					
					// Once the runloop returns, we should have either an error result or a 
					// proxies array result.  Do the appropriate thing with that result.
					
					assert(result != NULL);
					
					if ( CFGetTypeID(result) == CFErrorGetTypeID() ) {
						if ( CFEqual(CFErrorGetDomain( (CFErrorRef) result ), kCFErrorDomainOSStatus) ) {
							err = CFErrorGetCode( (CFErrorRef) result );
						} else {
							err = coreFoundationUnknownErr;
						}
					} else if ( CFGetTypeID(result) == CFArrayGetTypeID() ) {
						CFArrayAppendArray(expandedProxies, result, CFRangeMake(0, CFArrayGetCount(result)));
					} else {
						assert(false);
						err = kernelTimeoutErr;
					}
				}
				
				CFQRelease(result);
				CFQRelease(rls);
			}
		}
	}
	
	// Clean up.
	
	if (err == noErr) {
		*expandedProxiesPtr = expandedProxies;
	} else {
		CFQRelease(expandedProxies);
	}
    
    assert( (err == noErr) == (*expandedProxiesPtr != NULL) );
    
    return err;
}


static OSStatus GetProxyForURL(const char *urlStr)
{
	OSStatus            err;
	CFURLRef            url;
	CFDictionaryRef     proxySettings;
	CFArrayRef          proxies;
    CFArrayRef          proxiesToTry;
	
	url           = NULL;
	proxySettings = NULL;
	proxies       = NULL;
    proxiesToTry  = NULL;
	
	// Create a URL from the argument C string, and verify that it's an plain ol' 
    // HTTP request.
	
	err = noErr;
	url = CFURLCreateWithBytes(NULL, (const UInt8 *) urlStr, strlen(urlStr), kCFStringEncodingUTF8, NULL);
	if (url == NULL) {
		err = coreFoundationUnknownErr;
	}
    if (err == noErr) {
        CFStringRef     scheme;
        
        scheme = CFURLCopyScheme(url);
        assert(scheme != NULL);

        CFQRelease(scheme);
    }
	
	// Get the default proxies dictionary from CF.
	
	if (err == noErr) {
		proxySettings = SCDynamicStoreCopyProxies(NULL);
		if (proxySettings == NULL) {
			err = coreFoundationUnknownErr;
		}
	}
	
	// Call CFNetworkCopyProxiesForURL to get the proxy list.  Then expand 
    // any PAC-based proxies.
	
	if (err == noErr) {
		proxies = CFNetworkCopyProxiesForURL(url, proxySettings);
		if (proxies == NULL) {
			err = coreFoundationUnknownErr;
		}
	}
    if (err == noErr) {
        err = CreateProxyListWithExpandedPACProxies(url, proxies, &proxiesToTry);
    }
    
    // Finally, try to do get the resource using the various proxies we have 
    // available.
    
    if (err == noErr) {
        CFIndex     proxyCount;
        CFIndex     proxyIndex;
        
        proxyCount = CFArrayGetCount(proxiesToTry);
        
        for (proxyIndex = 0; proxyIndex < proxyCount; proxyIndex++) {
            CFDictionaryRef     thisProxy;
            CFStringRef         proxyType;
            
            thisProxy = (CFDictionaryRef) CFArrayGetValueAtIndex(proxiesToTry, proxyIndex);
            assert(thisProxy != NULL);
            assert(CFGetTypeID(thisProxy) == CFDictionaryGetTypeID());
            
            proxyType = CFDictionaryGetValue(thisProxy, kCFProxyTypeKey);
            assert(proxyType != NULL);
            assert(CFGetTypeID(proxyType) == CFStringGetTypeID());
            
            if ( CFEqual(proxyType, kCFProxyTypeNone) ) {
                debugf("No proxy configured");
            } else if ( CFEqual(proxyType, kCFProxyTypeHTTP) ) {
                debugf("HTTP proxy configured");
                SetProxyInfo(url, thisProxy);
                SetProxyAuth(proxy_info->host, proxy_info->port, kSecProtocolTypeHTTPProxy);
            } else if ( CFEqual(proxyType, kCFProxyTypeHTTPS) ) {
                debugf("HTTPS proxy configured");
                SetProxyInfo(url, thisProxy);
                SetProxyAuth(proxy_info->host, proxy_info->port, kSecProtocolTypeHTTPSProxy);
            } else {
                debugf("Unknown proxy configured");
                err = unimpErr;
            }

            if (err == noErr) {
                break;
            } else if (err != unimpErr) {
                errorf("No proxy info found");
            }
        }
        
        snprintf(proxy_info->url, sizeof(proxy_info->url), "%s", urlStr);
    }

	// Clean up.
	
    CFQRelease(proxiesToTry);
	CFQRelease(proxies);
	CFQRelease(proxySettings);
	CFQRelease(url);
    
    return err;
}


// Only checks if HTTP or HTTPS proxy is enabled.  If only auto proxy is configured, this will return false. 
Boolean IsProxyConfigured(const char * urlStr)
{
    Boolean             enabled = FALSE;
	OSStatus            err;
	CFURLRef            url;
	CFDictionaryRef     proxySettings;
	CFArrayRef          proxies;
    Boolean             secure = FALSE;
	
	url           = NULL;
	proxySettings = NULL;
	proxies       = NULL;
	
	// Create a URL from the argument C string, and verify that it's an plain ol' 
    // HTTP request.
	
	err = noErr;
	url = CFURLCreateWithBytes(NULL, (const UInt8 *) urlStr, strlen(urlStr), kCFStringEncodingUTF8, NULL);
	if (url == NULL) {
		err = coreFoundationUnknownErr;
	}
    if (err == noErr) {
        CFStringRef     scheme;
        
        scheme = CFURLCopyScheme(url);
        assert(scheme != NULL);

        if (CFStringCompare(scheme, CFSTR("https"), kCFCompareCaseInsensitive) == 0) {
            secure = TRUE;
        }

        CFQRelease(scheme);
    }
	
	if (err == noErr) {
        CFNumberRef number;
        
		proxySettings = SCDynamicStoreCopyProxies(NULL);
		if (proxySettings == NULL) {
			err = coreFoundationUnknownErr;
		}
        
        if (secure)
            number = CFDictionaryGetValue(proxySettings, kSCPropNetProxiesHTTPSEnable);
        else
            number = CFDictionaryGetValue(proxySettings, kSCPropNetProxiesHTTPEnable);

        if (number != NULL) {
            SInt32 enabledNum;
            Boolean success = CFNumberGetValue(number, kCFNumberSInt32Type, &enabledNum);
            if ( success && enabledNum ) {
                enabled = TRUE;
            }
        }        
	}

    CFQRelease(proxies);
	CFQRelease(proxySettings);
	CFQRelease(url);
    
    return enabled;
}

//*****************************************************************************
int proxy_for_uri( const char *uri, HttpProxyInfo *out_proxy_info )
{
    int ret;
    OSStatus err;

    memset(out_proxy_info, 0, sizeof(*out_proxy_info));
    proxy_info = out_proxy_info;

    debugf("Checking proxy for %s", uri);
    err = GetProxyForURL(uri);
    
    if (err == noErr)
        return *proxy_info->host != 0;
    return 0;
}


//*****************************************************************************
void proxy_init(void)
{
}


//*****************************************************************************
void proxy_shutdown(void)
{
}


//*****************************************************************************
int proxy_is_proxy_configured( )
{
    return 1;
}


