/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef PORT_H
#define PORT_H

#if defined(LINUX) || defined(MACOSX)
#include <time.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/tcp.h>
#else
#include <winsock2.h>
#include <time.h>
#endif

#ifdef __cplusplus
extern "C"
{
#endif

// Abstractions for some network functions

#if defined(LINUX) | defined(MACOSX)

#ifndef BOOL
#define BOOL int
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#define SOCKET int
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1

void _set_nonblock(int fd, int act);
void _set_nodelay(int fd, int act);
int _gettimeofday(struct timeval * tv);
int sleep_millis(int ms);
long _clock();

#define _write(f,b,l) write(f,b,l)
#define _read(f,b,l) read(f,b,l)
#define _error errno
#define _close close
#define _strerror strerror

#define CLEAR_ERR (errno = 0)
#define CHECK_ERR errno
#define SET_ERR(_err) errno = _err

#define _EAGAIN EAGAIN
#define _EWOULDBLOCK EWOULDBLOCK  // really the same as EAGAIN
#define _EINTR EINTR
#define _EINPROGRESS EINPROGRESS
#define _ENETUNREACH ENETUNREACH

#endif

#ifdef _WIN32

typedef int socklen_t;

void _set_nonblock(int fd, int act);
void _set_nodelay(int fd, int act);
int _gettimeofday(struct timeval * tv);

#define _write(f,b,l) send(f,b,l,0)
#define _read(f,b,l) recv(f,b,l,0)
#define _error WSAGetLastError()
#define _close closesocket
#define _strerror local_strerror
#define _clock clock

#define CLEAR_ERR
#define CHECK_ERR GetLastError()
#define SET_ERR(_err) SetLastError(_err)

#define _EAGAIN WSAEWOULDBLOCK
#define _EWOULDBLOCK WSAEWOULDBLOCK
#define _EINTR WSAEINTR
#define _EINPROGRESS WSAEINPROGRESS
#define _ENETUNREACH WSAENETUNREACH

extern void __declspec(dllimport) __stdcall Sleep(unsigned long millis);
#define sleep(sec) Sleep(sec * 1000)
#define sleep_millis(ms) Sleep(ms)
int inet_aton(const char * cp, struct in_addr * ip);
const char * local_strerror(int error_code);

#ifndef strcasecmp
  #define strcasecmp stricmp
#endif
#ifndef strncasecmp
  #define strncasecmp strnicmp
#endif

#endif

#ifdef __cplusplus
} // extern "C"
#endif

#endif
