#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "reportgen.h"

char *readLine( char *pszOut, int num, FILE *fp )
{
    char *pRet = fgets( pszOut, num, fp );

    // Strip trailing line terminators
    int len = pRet ? strlen(pszOut) : 0;
    if( len )
    {
        if( pszOut[len-1] == '\r' )
        {
            pszOut[len-1] = '\0';
            --len;
        }
        if( pszOut[len-1] == '\n' )
        {
            pszOut[len-1] = '\0';
        }
    }
    return pRet;
}

int readWord(FILE *fp, char *word, int n)
{
	int c;
	int k= 0;
	while((c= getc(fp)) != EOF)
	{
		if(c<'!' || c>'~'){
			if(k>0)
				break;
		}else if(--n<=0)
			break;
		else{
			++k;
			*word++= c;
		}
	}
	*word++= 0;

	return c!=EOF;
}

int isHex32num(char *cp)
{
	int k;
    char *psz0X = "0x";

	if( !cp )
        return 0;

	if( strlen( cp ) != 10 )
        return 0;

    if( strncmp( psz0X, cp, 2 ) != 0 )
        return 0;

	for(k= 2; cp[k]; k++)
		if(!isxdigit(cp[k]))
			return 0;

	return k==10;
}

unsigned long hexToLong( char *cp )
{
    char *endc;
    char *zeroX = "0x";
    int iOff = 0;
    if( strncmp(zeroX,cp,2) == 0 )
    {
        iOff = 2;
    }
    unsigned long lRet = strtoul( &cp[iOff], &endc, 16 );
    return lRet;
}

int GetMapFileName( char *pszPath, char *pszFileName, int nlen )
{
    int i;

    if( !pszPath || !pszPath[0] || !pszFileName )
    {
        return 0;
    }

    // Look for the last directory separator slash char in the string
    int iSlash = -1;
    char cSlash;
#ifdef WIN32
    cSlash = '\\';
#else
    cSlash = '/';
#endif
    for( i = strlen( pszPath ) - 1; i >= 0; --i )
    {
        if( pszPath[i] == cSlash )
        {
            iSlash = i;
            break;
        }
    }
    if( iSlash != -1 )
    {
        strncpy( pszFileName, &pszPath[iSlash+1], nlen );
    }else
    {
        strncpy( pszFileName, pszPath, nlen );
    }

    // Look for the trailing '.' before the file type
    int iDot = -1;
    for( i = strlen(pszFileName)-1; i >= 0; --i )
    {
        if( pszFileName[i] == '.' )
        {
            iDot = i;
            pszFileName[iDot] = '\0';
            strcat( pszFileName, ".map" );
            break;
        }
    }
    if( iDot == -1 )
    {
        strcat( pszFileName, ".map" );
    }
    return strlen( pszFileName );
}

int GetSymbolFileName( char *pszPath, char *pszFileName, int nlen )
{
    int i;

    if( !pszPath || !pszPath[0] || !pszFileName )
    {
        return 0;
    }

    // Look for the last directory separator slash char in the string
    int iSlash = -1;
    char cSlash;
#ifdef WIN32
    cSlash = '\\';
#else
    cSlash = '/';
#endif
    for( i = strlen( pszPath ) - 1; i >= 0; --i )
    {
        if( pszPath[i] == cSlash )
        {
            iSlash = i;
            break;
        }
    }
    if( iSlash != -1 )
    {
        strncpy( pszFileName, &pszPath[iSlash+1], nlen );
    }else
    {
        strncpy( pszFileName, pszPath, nlen );
    }

    // Look for the trailing '.' before the file type
    int iDot = -1;
    for( i = strlen(pszFileName)-1; i >= 0; --i )
    {
        if( pszFileName[i] == '.' )
        {
            iDot = i;
            pszFileName[iDot] = '\0';
            strcat( pszFileName, ".symbols" );
            break;
        }
    }
    if( iDot == -1 )
    {
        strcat( pszFileName, ".symbols" );
    }
    return strlen( pszFileName );
}
