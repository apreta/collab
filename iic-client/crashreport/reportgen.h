#ifndef _REPORTGEN_H_
#define _REPORTGEN_H_

#include "config.h"

enum CRASHREPORT_LIMITS
{
    MAX_NAME_LEN = 1024,
    MAX_LINE_LEN = 2048,
    MAX_SYMBOL_LEN = 512

};

/** Read to the end of the current line.<br>
  * Trailing line termnator characters are stripped.
  */
char *readLine( char *pszOut, int num, FILE *fp );

/** Read the next whitespace delimited "word" from the file. */
int readWord(FILE *fp, char *word, int n);

/** Return TRUE if the specified string is a 32 Bit Hex encoded number.<br>
  * Like "0x12345678"
  */
int isHex32num(char *cp);

/** Convert 0x12345678 to unsigned long */
unsigned long hexToLong( char *cp );

/** Get the name of a map file from it's associated module name */
int GetMapFileName( char *pszPath, char *pszFileName, int nlen );

/** Get the name of a symbol file from one of it's associated file names. (Either xxxxx.map/.exe/,dll ... ) */
int GetSymbolFileName( char *pszPath, char *pszFileName, int nlen );

#ifdef WIN32

int Generate_gnu_win32_report( FILE *fpIn, FILE *fpOut );

#define isHexNumber(S) isHex32num(S)
#define translateMap( A, M, O ) trmap_gnu_win32(A, M, O)
#define GenerateCrashReport(F_IN,F_OUT) Generate_gnu_win32_report(F_IN,F_OUT)

#endif //WIN32

#ifdef LINUX

int Generate_gnu_linux_report( FILE *fpIn, FILE *fpOut );

#define GenerateCrashReport(F_IN,F_OUT) Generate_gnu_linux_report(F_IN,F_OUT)

#endif // LINUX


#endif // _REPORTGEN_H_
