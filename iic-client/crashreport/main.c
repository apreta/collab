#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "reportgen.h"

static char *pszProgram = "crashreport";
static char *pszVersion = "1.0";

void ShowUsage();

/**
  * Return the next argument starting from argv[ndx] in pszArg.
  * Increments ndx, gets the next string and places it in pszArg.
  * Returns the new ndx value.
  */
int NextArg( int ndx, int argc, char **argv, char *pszArg );

char cOutputOption = 'o';

FILE *fpIn = 0;
FILE *fpOut = 0;
FILE *fpMap = 0;
char szOutputFileName[MAX_NAME_LEN] = "";
char szInputFileName[MAX_NAME_LEN] = "";
char szMapFileName[MAX_NAME_LEN] = "";
char szSymbolFileName[MAX_NAME_LEN] = "";

int main( int argc, char **argv )
{
    int i = 0;

    printf("%s - Version %s\n", pszProgram, pszVersion );

    if(0)
    {
        // Debug mode.
        printf("WARNING: RUNNING IN DEBUG DIAGNOSTICS MODE (See main.c)\n\n");
        strcpy( szInputFileName, "c:\\src\\iic\\clients\\bin\\meeting.zon.crash" );
        strcpy( szOutputFileName, "c:\\src\\iic\\clients\\bin\\meeting.zon.crash.rpt" );
    }else
    {
        if( argc <= 1 )
        {
            printf("Missing parameters\n");
            ShowUsage();
            return 1;
        }

        for( i = 1; i < argc; ++i )
        {
            char *pszArg = argv[i];
            // If this argument is an option (-x or /x)
            if( pszArg && (pszArg[0] == '-' || pszArg[0] == '/') && (strlen(pszArg) == 2) )
            {
                if( pszArg[1] == cOutputOption )
                {
                    i = NextArg( i, argc, argv, szOutputFileName );
                }
            }else
            {
                strncpy( szInputFileName, pszArg, MAX_NAME_LEN );
            }
        }

        if( !szInputFileName[0] && !szMapFileName[0] )
        {
            printf("No input file name or map file name specified.");
            ShowUsage();
            return 1;
        }
    }

    printf("Processing input file: %s\n",szInputFileName);

    if( !szOutputFileName[0] )
    {
        fpOut = stderr;
    }else
    {
        fpOut = fopen( szOutputFileName, "w" );
        if( !fpOut )
        {
            printf("Cannot open the output file: %s\n",szOutputFileName);
            return 2;
        }
    }

    // Open
    fpIn = fopen( szInputFileName, "r" );
    if( !fpIn )
    {
        printf("Cannot open the input file: %s\n", szInputFileName );
        return 2;
    }

    // Process the input file
    int nRet = GenerateCrashReport( fpIn, fpOut );

    // Cleanup
    if( fpIn )
    {
        fclose(fpIn);
    }
    if( szOutputFileName[0] && fpOut )
    {
        fclose(fpOut);
    }

    return nRet;
}

int NextArg( int ndx, int argc, char **argv, char *pszArg )
{
    ++ndx;

    if( ndx == argc )
    {
        pszArg[0] = '\0';
        return ndx;
    }

    strncpy( pszArg, argv[ndx], MAX_NAME_LEN );

    return ndx;
}

void ShowUsage()
{
    printf("Usage:\n");
    printf("To process a crash report:\n");
    printf("%s input_file [-o output_file]\n",pszProgram);
    printf("\n");
}
