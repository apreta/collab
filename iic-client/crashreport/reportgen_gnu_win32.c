#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h>
#include "reportgen.h"
#include "crashlib.h"

extern char szMapFileName[];
extern char szSymbolFileName[];

typedef struct
{
    unsigned long address;
    unsigned long size;
    char *pszName;
}SYMBOL_SECTION_ENTRY;

#define MAX_SYMBOL_SECTION_ENTRIES 20000

static SYMBOL_SECTION_ENTRY symbol_section[MAX_SYMBOL_SECTION_ENTRIES];
static int symbol_index[MAX_SYMBOL_SECTION_ENTRIES];
static int nSymbolSectionEntries = 0;

char *trmap_gnu_win32(char * pszSymbolFileName, unsigned long addr, unsigned long *offset);
void ClearSymbolData();
int CreateSymbolData(char *pszMapFileName, char *pszSymbolFileName );

int SectionCompare( int ndx1, int ndx2 );
void SectionSwap( int ndx1, int ndx2 );

int AddTextSection( char *address, char *size, char *name );
int AddSymbol( char *address, char *symbol );


int Generate_gnu_win32_report( FILE *fpIn, FILE *fpOut )
{
/*
    rprintf(_T("0x%08lX "),StackFrame.AddrPC.Offset );
    rprintf(_T("0x%08lX "),lastImageBaseAddress );
    rprintf(_T("0x%08lX "),hModule);
    rprintf(_T("0x%08lX "),lastAddressAdjustment);
    rprintf(_T("0x%08lX "),lastSectionAlignment);
*/
    int nRet = 0;
    int callStackFound = 0;
    char szLine[MAX_LINE_LEN+1];
    char szTag[MAX_LINE_LEN+1];
    char szAddress[MAX_LINE_LEN+1];             // The crash stack frame address
    char szImageBaseAddress[MAX_LINE_LEN+1];    // The image base address
    char szModuleAddress[MAX_LINE_LEN+1];       // The module address
    char szAddressAdjustment[MAX_LINE_LEN+1];   // The stack frame address adjustment
    char szSectionAlignment[MAX_LINE_LEN+1];    // The section alignment
    char szSectionStart[MAX_LINE_LEN+1];        // The section start address
    char szSection[MAX_LINE_LEN+1];             // The section number
    char szOffset[MAX_LINE_LEN+1];              // The offset within the section
    char szSectionEnd[MAX_LINE_LEN+1];          // The section end address

    char *pszCALLSTACK = ".CALLSTACK:";
    char *pszCOMMENT = "#";


    while( !feof(fpIn) )
    {
        if( readWord( fpIn, szTag, MAX_LINE_LEN ) )
        {
            if( strncmp( CRASHLIB_MAPNAME, szTag, strlen(CRASHLIB_MAPNAME) ) == 0 )
            {
                if( readLine( szLine, MAX_LINE_LEN, fpIn ) )
                {
                    // Don't care
                }
                continue;
            }

            if( strncmp( CRASHLIB_MAPPATH, szTag, strlen(CRASHLIB_MAPPATH) ) == 0 )
            {
                if( readLine( szLine, MAX_LINE_LEN, fpIn ) )
                {
                    // Don't care
                }
                continue;
            }

            if( strncmp( CRASHLIB_DATE, szTag, strlen(CRASHLIB_DATE) ) == 0 )
            {
                if( readLine( szLine, MAX_LINE_LEN, fpIn ) )
                {
                    fprintf(fpOut,"Date: %s\n", szLine );
                }
                continue;
            }

            if( strncmp( CRASHLIB_NAME, szTag, strlen(CRASHLIB_NAME) ) == 0 )
            {
                if( readLine( szLine, MAX_LINE_LEN, fpIn ) )
                {
                    fprintf( fpOut, "Name: %s\n",szLine );
                }
                continue;
            }

            if( strncmp( CRASHLIB_EXCEPTION, szTag, strlen(CRASHLIB_EXCEPTION) ) == 0 )
            {
                if( readLine( szLine, MAX_LINE_LEN, fpIn ) )
                {
                    fprintf( fpOut, "Exception: %s\n", szLine );
                }
                continue;
            }

            if( strncmp( CRASHLIB_LOCATION, szTag, strlen(CRASHLIB_LOCATION) ) == 0 )
            {
                if( readLine( szLine, MAX_LINE_LEN, fpIn ) )
                {
                    fprintf( fpOut, "At location: %s\n", szLine );
                }
                continue;
            }

            if( strncmp( CRASHLIB_READINGLOC, szTag, strlen(CRASHLIB_READINGLOC) ) == 0 )
            {
                if( readLine( szLine, MAX_LINE_LEN, fpIn ) )
                {
                    fprintf( fpOut, "While reading from location: %s\n", szLine );
                }
                continue;
            }

            if( strncmp( CRASHLIB_WRITINGLOC, szTag, strlen(CRASHLIB_WRITINGLOC) ) == 0 )
            {
                if( readLine( szLine, MAX_LINE_LEN, fpIn ) )
                {
                    fprintf( fpOut, "While writing to location: %s\n", szLine );
                }
                continue;
            }

            if( strncmp( CRASHLIB_REGISTERS, szTag, strlen(CRASHLIB_REGISTERS) ) == 0 )
            {
                if( readLine( szLine, MAX_LINE_LEN, fpIn ) )
                {
                    fprintf( fpOut, "Registers: %s\n", szLine );
                }
                continue;
            }

            if( strncmp( CRASHLIB_CONTEXTFLAGS, szTag, strlen(CRASHLIB_CONTEXTFLAGS) ) == 0 )
            {
                if( readLine( szLine, MAX_LINE_LEN, fpIn ) )
                {
                    fprintf( fpOut, "Context flags: %s\n", szLine );
                }
                continue;
            }

            if( strncmp( CRASHLIB_CONTEXTSEGS, szTag, strlen(CRASHLIB_CONTEXTSEGS) ) == 0 )
            {
                if( readLine( szLine, MAX_LINE_LEN, fpIn ) )
                {
                    fprintf( fpOut, "Context segs: %s\n", szLine );
                }
                continue;
            }

            if( strncmp( CRASHLIB_CALLSTACK, szTag, strlen(CRASHLIB_CALLSTACK) ) == 0 )
            {
                // NOTE: The following readWord calls must by in synch with
                // /src/iic/clients/crashlib/crashlib.c output.
                if( readWord( fpIn, szAddress, MAX_LINE_LEN ) )
                {
                    if( strncmp( pszCOMMENT, szAddress, strlen(pszCOMMENT)) == 0 )
                    {
                        readLine( szLine, MAX_LINE_LEN, fpIn );
                        continue; // Ignore comment lines
                    }

                    readWord( fpIn, szImageBaseAddress, MAX_LINE_LEN );
                    readWord( fpIn, szModuleAddress, MAX_LINE_LEN );
                    readWord( fpIn, szAddressAdjustment, MAX_LINE_LEN );
                    readWord( fpIn, szSectionAlignment, MAX_LINE_LEN );
                    readWord( fpIn, szSectionStart, MAX_LINE_LEN );
                    readWord( fpIn, szSection, MAX_LINE_LEN );
                    readWord( fpIn, szOffset, MAX_LINE_LEN );
                    readWord( fpIn, szSectionEnd, MAX_LINE_LEN );
                    readLine( szLine, MAX_LINE_LEN, fpIn );

                    unsigned long address = hexToLong( szAddress );
                    unsigned long imageBaseAddress = hexToLong( szImageBaseAddress );
                    unsigned long moduleAddress = hexToLong( szModuleAddress );
                    unsigned long addressAdjustment = hexToLong( szAddressAdjustment );
                    //unsigned long sectionAlignment = hexToLong( szSectionAlignment );

                    unsigned long displacement = 0L;
                    unsigned long adjustedAddress = address;
                    if( addressAdjustment )
                    {
                        adjustedAddress = (address - moduleAddress) + imageBaseAddress;
                    }

                    // Get the appropriate map file name from the module information in szLine
                    int mapLen = GetMapFileName( szLine, szMapFileName, MAX_NAME_LEN );
                    if( mapLen )
                    {
                        int symbolLen = GetSymbolFileName( szMapFileName, szSymbolFileName, MAX_NAME_LEN );
                        if( symbolLen )
                        {
                            CreateSymbolData( szMapFileName, szSymbolFileName );
                        }else
                        {
                            fprintf(stderr,"Cannot create symbol file name from \"%s\"\n",szMapFileName);
                        }
                    }else
                    {
                        fprintf(stderr,"Cannot create map file name from \"%s\"\n", szLine);
                    }

                    char *pszSymbol = trmap_gnu_win32( szSymbolFileName, adjustedAddress, &displacement );

                    fprintf(fpOut, "\nModule: %s\n", szLine );
                    if( pszSymbol )
                    {
                        if( displacement )
                        {
                            fprintf(fpOut,"Address %s (%lx) - %s ( + %lx )\n",szAddress, adjustedAddress, pszSymbol, displacement );
                        }else
                        {
                            fprintf(fpOut,"Address: %s (%lx) - %s\n",szAddress, adjustedAddress, pszSymbol );
                        }
                    }else
                    {
                        fprintf(fpOut,"%s (%lx) - Unknown symbol?\n",szAddress, adjustedAddress);
                    }
                    fprintf(fpOut,"\n");
                } // end if read szAddress from CALLSTACK
            } // end if szTag is CALLSTACK
        } // end if read szTag
    }

    return nRet;
}

// Defunct code......
char *trmap_gnu_win32(char *pszSymbolFileName, unsigned long addr, unsigned long *offset)
{
    unsigned long thisAddress = 0;
    unsigned long lastAddress = 0;

    char szAddress[MAX_SYMBOL_LEN+1];
    char szSize[MAX_SYMBOL_LEN+1];

    char szLastLine[MAX_LINE_LEN+1];
    char szLine[MAX_LINE_LEN+1];

    static char szSymbol[MAX_SYMBOL_LEN+1];
    FILE *fp = 0;


    // Clear
    szSymbol[0] = '\0';

    if( !pszSymbolFileName || !pszSymbolFileName[0] )
    {
       fprintf(stderr,"Warning: trmap_gnu_win32 Missing pszSymbolFileName parameter!\n");
       return 0;
    }

    fp = fopen( pszSymbolFileName, "rt" );
    if( !fp )
    {
        fprintf(stderr,"Warning: Cannot open symbol file %s for input\n",pszSymbolFileName);
        return 0;
    }

    while( readWord( fp, szAddress, MAX_SYMBOL_LEN ) )
    {
        if( readWord( fp, szSize, MAX_SYMBOL_LEN ) && readLine( szLine, MAX_LINE_LEN, fp ) )
        {
            thisAddress = hexToLong( szAddress );
            if( thisAddress > addr )
            {
                strcpy(szSymbol,szLastLine);
                if( offset )
                {
                    *offset = addr - lastAddress;
                }
                break;
            }
        }else
        {
            fprintf(stderr,"Warning: Symbol file %s is corrupted.\n",pszSymbolFileName);
            break;
        }
        // Remember the current address/symbol
        lastAddress = thisAddress;
        strcpy( szLastLine, szLine );
    }

    if( fp )
    {
        fclose( fp );
    }

    return szSymbol[0] ? szSymbol : 0;
}

void ClearSymbolData()
{
    // Clear the decks
    int i;
    nSymbolSectionEntries = 0;
    for( i = 0; i < MAX_SYMBOL_SECTION_ENTRIES; ++i )
    {
        symbol_section[i].pszName = 0;
        symbol_section[i].address = 0;
        symbol_section[i].size = 0;
        symbol_index[i] = i;
    }
}

int CreateSymbolData( char * pszMapFileName, char *pszSymbolFileName )
{
    int nRet = 0;

    char szLine[MAX_LINE_LEN+1];
	char word[MAX_SYMBOL_LEN+1];
	char text_section_address[MAX_SYMBOL_LEN+1];
	char text_section_size[MAX_SYMBOL_LEN+1];
	char object_file_name[MAX_SYMBOL_LEN+1];
	char symbol_name[MAX_SYMBOL_LEN+1];
	FILE *fpMap = 0;
	FILE *fpSymbol = 0;
    int flushData = 1;
    char *pszDotText = ".text";
    int createSymbolFile = 0;

    struct stat mapStat;
    struct stat symbolStat;

    // No pointers = No Joy
    if( !pszMapFileName || !pszSymbolFileName )
        return -1;

    // No map file = No Joy
    int nMapStat = stat( pszMapFileName, &mapStat );
    if( nMapStat != 0 )
    {
        fprintf(stderr,"Warning: Cannot retrieve file attrs for Map file %s.\n",pszMapFileName);
        return -1;
    }

    int nSymbolStat = stat( pszSymbolFileName, &symbolStat );
    if( nSymbolStat != 0 )
    {
        createSymbolFile = 1;
    }else if( nMapStat == 0 && nSymbolStat == 0 )
    {
        // stat.st_mtime is the last modification time
        // This compare should work for GNU tools
        if( symbolStat.st_mtime < mapStat.st_mtime )
        {
            createSymbolFile = 1;
        }
    }

    if( !createSymbolFile )
    {
        return 0;
    }

    fprintf(stderr,"DEBUG: Creating symbol file %s\n",pszSymbolFileName);

    ClearSymbolData();

    /***********************************************************************************************
    Search pattern for GNU C,C++, ld map file:
    ------------------------------------------
    .text hex-start-address hex-length Object file name
          0x12345678 Symbol name
          0x12345678 Symbol name

    .text$_MangledName
          0x12345678 Symbol name

    NOTE: hex-start-address + hex-length = The start address of the next test section.
    ************************************************************************************************/

	fpMap = fopen(pszMapFileName, "rt");

	if(!fpMap)
	{
        fprintf(stderr,"Warning: Cannot open the mapfile: %s for input\n",szMapFileName);
        return 2;
    }

    fpSymbol = fopen(pszSymbolFileName, "wt" );
    if( !fpSymbol )
    {
        fclose(fpMap);
        fprintf(stderr,"Warning: Cannot create the symbol file: %s for output\n",szSymbolFileName);
        return 2;
    }

	while(readWord(fpMap, word, sizeof(word)))
	{
	    if( flushData )
	    {
            // If we just read a .text section
	        if( strcmp( word, pszDotText ) == 0 )
	        {
	            // Grab the base address of the .text section
	            readWord(fpMap,text_section_address,MAX_SYMBOL_LEN);

	            // Grab the length of the .text section
	            readWord(fpMap,text_section_size,MAX_SYMBOL_LEN);

	            // Grab the name of the .text section's object file
	            readLine( object_file_name, MAX_SYMBOL_LEN, fpMap );
                if( strcmp( object_file_name, "*(.init)" ) == 0 ||
                    strcmp( object_file_name, " *(.init)" ) == 0 )
                    continue; // Ignore this one

                AddTextSection( text_section_address, text_section_size, object_file_name );
                flushData = 0;
	        }else
	        {
	            // Flush the remainder of this line
                readLine( szLine, MAX_LINE_LEN, fpMap );
	        }
	    }else
	    {
            if(isHexNumber(word))
            {
                readLine( symbol_name, MAX_SYMBOL_LEN, fpMap );

                AddSymbol( word, symbol_name );

            }else
            {
                // Flush
                readLine(szLine, MAX_LINE_LEN, fpMap );

                flushData = 1;
            }
	    }
	}

	if( fpMap )
	{
        fclose(fpMap);
	}

    // Use a quick & dirty shell sort on the data
    int gap, i, j;
    for(gap = nSymbolSectionEntries/2; gap > 0; gap = gap/2)
    {
        for(i = gap; i < nSymbolSectionEntries; i++)
        {
            for(j = i-gap; j >= 0 && SectionCompare( j, j+gap ) > 0; j=j-gap)
            {
                SectionSwap(j, j+gap);
            }
        }
    }


    if( fpSymbol )
    {
        for( i = 0; i < nSymbolSectionEntries; ++i )
        {
            char szaddress[MAX_SYMBOL_LEN];
            char szsize[MAX_SYMBOL_LEN];
            ltoa( symbol_section[symbol_index[i]].address, szaddress, 16 );
            ltoa( symbol_section[symbol_index[i]].size, szsize, 16 );
            fprintf(fpSymbol,"%s %s %s\n",szaddress,szsize,symbol_section[symbol_index[i]].pszName);
        }

        fclose(fpSymbol);
    }

    return nRet;
}

int AddTextSection( char *address, char *size, char *name )
{
    int ndx = nSymbolSectionEntries;
    if( nSymbolSectionEntries < MAX_SYMBOL_SECTION_ENTRIES )
    {
        ++nSymbolSectionEntries;
        symbol_section[ndx].address = hexToLong( address );
        symbol_section[ndx].size = hexToLong( size );
        symbol_section[ndx].pszName = malloc( strlen(name) + 1 );
        if( symbol_section[ndx].pszName )
        {
            strcpy(symbol_section[ndx].pszName, name );
        }else
        {
            fprintf(stderr,"Warning: Out of heap space at symbol entry %d\n",nSymbolSectionEntries);
        }
        return nSymbolSectionEntries;
    }
    return -1;
}

int AddSymbol( char *address, char *symbol )
{
    int ndx = nSymbolSectionEntries;
    if( nSymbolSectionEntries < MAX_SYMBOL_SECTION_ENTRIES )
    {
        ++nSymbolSectionEntries;
        symbol_section[ndx].address = hexToLong( address );
        symbol_section[ndx].size = 0;
        symbol_section[ndx].pszName = malloc( strlen(symbol) + 1 );
        if( symbol_section[ndx].pszName )
        {
            strcpy(symbol_section[ndx].pszName, symbol );
        }else
        {
            fprintf(stderr,"Warning: Out of heap space at symbol entry %d\n",nSymbolSectionEntries);
        }
        return nSymbolSectionEntries;
    }
    return -1;
}

int SectionCompare( int ndx1, int ndx2 )
{
    if( symbol_section[symbol_index[ndx1]].address > symbol_section[symbol_index[ndx2]].address ) return 1;
    else if( symbol_section[symbol_index[ndx1]].address < symbol_section[symbol_index[ndx2]].address ) return -1;
    return 0;
}

void SectionSwap( int ndx1, int ndx2 )
{
    int temp = symbol_index[ndx1];
    symbol_index[ndx1] = symbol_index[ndx2];
    symbol_index[ndx2] = temp;
}
