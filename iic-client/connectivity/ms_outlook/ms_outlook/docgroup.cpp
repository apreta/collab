// DocGroup.cpp: implementation of the CDocGroup class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "iic.h"
#include "docgroup.h"
#include "iicdoc.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDocGroup::CDocGroup()
{
    m_pSearch = NULL;
}

CDocGroup::CDocGroup(CIICDoc *aDocument, CDocDirectory *pDirectory, const CString &aGroupId, const CString &aName, int aType)
{
    m_pDocument = aDocument;
    m_pDirectory = pDirectory;
    m_GroupId = aGroupId;
    m_Name = aName;
    m_Type = aType;
    m_pSearch = NULL;
}

CDocGroup::CDocGroup(CIICDoc *aDocument, CDocDirectory *pDirectory, const CString &aName)
{
    m_pDocument = aDocument;
    m_pDirectory = pDirectory;
    m_Name = aName;
    m_Type = kGroupTypeOther;
    m_pSearch = NULL;
}

CDocGroup::~CDocGroup()
{
    CSingleLock singleLock(&m_CriticalSection);
    singleLock.Lock();  
    {
        if (m_pSearch != NULL)
        {
//            delete m_pSearch;
            m_pSearch = NULL;
        }
    }
    singleLock.Unlock();
}

//////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// GetId
//
void CDocGroup::GetGroupId(CString &aGroupId)
{
    aGroupId = m_GroupId;
}

//////////////////////////////////////////////////////////////////////
// GetDirectory
//
CDocDirectory *CDocGroup::GetDirectory()
{
    return m_pDirectory;
}

//////////////////////////////////////////////////////////////////////
// GetDirectoryId
//
void CDocGroup::GetDirectoryId(CString &aDirectoryId)
{
    m_pDirectory->GetId(aDirectoryId);
}

//////////////////////////////////////////////////////////////////////
// GetName
//
void CDocGroup::GetName(CString &aName)
{
   aName = m_Name;
}

//////////////////////////////////////////////////////////////////////
// SetName
//
void CDocGroup::SetName(const CString &aName)
{
   m_Name = aName;
}

//////////////////////////////////////////////////////////////////////
// GetFullyQualifiedName
//
void CDocGroup::GetFullyQualifiedName(CString &aName)
{
    CString strDirectoryName;
    m_pDirectory->GetName(strDirectoryName);

    aName = m_Name + _T(" (") + strDirectoryName + _T(")");
}

//////////////////////////////////////////////////////////////////////
// GetType
//
int CDocGroup::GetType()
{
    return m_Type;
}

//////////////////////////////////////////////////////////////////////
// IsReadOnly
//
BOOL CDocGroup::IsReadOnly()
{
    return m_pDirectory->IsReadOnly();
}

//////////////////////////////////////////////////////////////////////
// IsGlobalAddressBook
//
BOOL CDocGroup::IsGlobalAddressBook()
{
    return m_pDirectory->IsGlobal();
}

//////////////////////////////////////////////////////////////////////
// IsAllGroup
//
BOOL CDocGroup::IsAllGroup()
{
    return m_Type == kGroupTypeAll;
}

//////////////////////////////////////////////////////////////////////
// IsSearchGroup
//
BOOL CDocGroup::IsSearchGroup()
{
    bool bIsSearch = false;

    CSingleLock singleLock(&m_CriticalSection);
    singleLock.Lock();  
    {
        bIsSearch = m_pSearch != NULL;
    }
    singleLock.Unlock();

    return bIsSearch;
}

//////////////////////////////////////////////////////////////////////
// IsBuddyGroup
//
BOOL CDocGroup::IsBuddyGroup()
{
    return m_Type == kGroupTypeBuddies;
}

//////////////////////////////////////////////////////////////////////
// IsInDirectory
//
BOOL CDocGroup::IsInDirectory(const CString &aDirectoryId)
{
    CString dirId;
    m_pDirectory->GetId(dirId);
    return (aDirectoryId == dirId);
}

//////////////////////////////////////////////////////////////////////
// IsUserInGroup
//
BOOL CDocGroup::IsUserInGroup(const CString &aUserId)
{
	return FALSE;
}

//////////////////////////////////////////////////////////////////////
// AddChildGroups
//
void CDocGroup::AddChildGroups(CStringArray &aChildGroupsArray)
{
    m_ChildGroups.Copy(aChildGroupsArray);
}

//////////////////////////////////////////////////////////////////////
// HasChildGroups
//
BOOL CDocGroup::HasChildGroups()
{
    return (m_ChildGroups.GetSize() != 0);
}

//////////////////////////////////////////////////////////////////////
// GetChildGroups
//
void CDocGroup::GetChildGroups(CStringArray &aChildGroupsArray)
{
    aChildGroupsArray.Copy(m_ChildGroups);
}

//////////////////////////////////////////////////////////////////////
// AddUser
//
void CDocGroup::AddUser(const CString &aUserId)
{
	m_Members.Add(aUserId);
}

//////////////////////////////////////////////////////////////////////
// RemoveUser
//
void CDocGroup::RemoveUser(const CString &aUserId)
{

}


