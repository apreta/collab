// ms_outlook.h : main header file for the ms_outlook DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

#include <list>
#include <string>


#ifdef WIN32
#ifdef MS_OUTLOOK_EXPORTS
#define MS_OUTLOOK_API __declspec(dllexport)
#else
#define MS_OUTLOOK_API __declspec(dllimport)
#endif

#else       //  #ifdef WIN32

#ifdef MS_OUTLOOK_EXPORTS
#define MS_OUTLOOK_API
#else
#define MS_OUTLOOK_API SAL_DLLPUBLIC_EXPORT
#endif

#endif       //  #ifdef WIN32

using namespace std;


#define STATUS_OK 0
#define STATUS_FAIL -1
#define STATUS_OVERFLOW -2

 MS_OUTLOOK_API int  __stdcall connGetInfo( char *pC, int iMaxLen, long *lFlags );

 MS_OUTLOOK_API int  __stdcall connInitialize( const char *userName, const char *configDir );

 MS_OUTLOOK_API void __stdcall connShutdown( );

 MS_OUTLOOK_API BOOL __stdcall connStartImport( );

 MS_OUTLOOK_API void __stdcall connStopImport( );

 MS_OUTLOOK_API BOOL __stdcall connIsReady( );

 MS_OUTLOOK_API BOOL __stdcall connIsConfigured( );

 MS_OUTLOOK_API BOOL __stdcall connConfigureDialog( void *windowHandle );

 MS_OUTLOOK_API int  __stdcall connConfigureItem( const char *pC );

 MS_OUTLOOK_API void __stdcall connClearState( );

 MS_OUTLOOK_API int  __stdcall connFetchFolders( );

 MS_OUTLOOK_API int  __stdcall connGetFolder( int iIndex, char *pC, int iMaxLen );

// MS_OUTLOOK_API int  connGetAB( int iIndex, char *pC );

 MS_OUTLOOK_API int  __stdcall connGetContactCount( );

 MS_OUTLOOK_API int  __stdcall connGetContactInfo( int iIndex, const char *pname, char *pdata, int iMaxLen );

 MS_OUTLOOK_API int  __stdcall connGetGroupCount( );

 MS_OUTLOOK_API int  __stdcall connGetGroupInfo( int iIndex, int *iGroupSize, char *pGroupId, int iIdLen, char *pdata, int iMaxLen );

 MS_OUTLOOK_API int  __stdcall connGetGroupMember( int iIndex, int iMember, char *pdata, int iMaxLen );


// Cms_outlookApp
// See ms_outlook.cpp for the implementation of this class
//

class Cms_outlookApp : public CWinApp
{
public:
	Cms_outlookApp();

// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};

extern CStringArray    garrFolders;
extern CStringArray    garrABs;
