// DocContact.cpp: implementation of the CDocContact class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "iic.h"
#include "doccontact.h"
#include "doccontacts.h"
#include "iicdoc.h"
//#include "utils.h"

#include <string>
using namespace std;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//extern CIICApp theApp;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDocContact::CDocContact()
{
    m_bDeleted = FALSE;
    m_bIsInterCommunity = false;
}

CDocContact::CDocContact(CIICDoc *aDocument)
{
    m_bDeleted = FALSE;
    m_bIsInterCommunity = false;
    m_pDocument = aDocument;
}

CDocContact::CDocContact(CIICDoc *aDocument, CDocContact *aContact)
{
    m_bDeleted = FALSE;
    m_bIsInterCommunity = false;
    m_pDocument = aDocument;

    // Copy the field values  
    int fieldcount = m_pDocument->m_Contacts.m_FieldNames.GetSize();
    CString strTemp;

    for (int loop=0; loop < fieldcount; loop++)
    {
        strTemp = "";
        TCHAR *fieldname = m_pDocument->m_Contacts.m_FieldNames[loop];

        if (aContact->GetData(fieldname, strTemp))
        {
            this->SetData(fieldname, strTemp);
        }
    }
}

CDocContact::~CDocContact()
{

}

//////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// DumpDataValue
//
void CDocContact::DumpDataValue(const CString &aIICName)
{
    CString strValue = _T("");
    TRACE("Value: %s=", aIICName);

    if (m_Data.Lookup(aIICName, strValue))
    {
        TRACE("'%s'\n", strValue);
    }
    else
    {
        TRACE("-- No Value --\n");
    }
}

//////////////////////////////////////////////////////////////////////
// CopyFields
//
void CDocContact::CopyFields(CDocContact* aContact)
{
    CString strFieldKey;
    CString strFieldValue;

    POSITION position = aContact->m_Data.GetStartPosition();

    while (position != NULL)
    {
        aContact->m_Data.GetNextAssoc(position, strFieldKey, strFieldValue);
        this->SetDataRaw(strFieldKey, strFieldValue);
    }
}

//////////////////////////////////////////////////////////////////////
// SetId
//
void CDocContact::SetId(int aId)
{
    m_Id = aId;
}

//////////////////////////////////////////////////////////////////////
// GetId
//
int CDocContact::GetId()
{
    return m_Id;
}

//////////////////////////////////////////////////////////////////////
// SetData
//
void CDocContact::SetData(const CString &aIICName, const CString &aValue)
{
    CString strTemp;

    if (aIICName == kContactDirectory)
    {
        m_pDocument->m_Directories.GetIdFromName(aValue, strTemp);
    }
    else
    {
        strTemp = aValue;
    }

    m_Data.SetAt(aIICName, strTemp);
}

//////////////////////////////////////////////////////////////////////
// SetDataRaw
//
void CDocContact::SetDataRaw(const CString &aIICName, const CString &aValue)
{
    m_Data.SetAt(aIICName, aValue);
}

//////////////////////////////////////////////////////////////////////
// GetData
//
BOOL CDocContact::GetData(const CString &aIICName, CString &aValue)
{
    CString strTemp;
    BOOL result = m_Data.Lookup(aIICName, strTemp);

    if (result)
    {
        if (aIICName == kContactDirectory)
        {
            m_pDocument->m_Directories.GetNameFromId(strTemp, aValue);
        }
        else
        {
            aValue = strTemp;
        }
    }
    else 
    {
        aValue = _T(""); //.LoadString( IDS_IIC_ELLIPSIS );
    }

    return result;
}

//////////////////////////////////////////////////////////////////////
// GetDataRaw
//
BOOL CDocContact::GetDataRaw(const CString &aIICName, CString &aValue)
{
    return m_Data.Lookup(aIICName, aValue);
}

