/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef LINUX
#include "windows.h"
#endif
#include "utf8_utils.h"

#pragma hdrstop

//#include "../network/logger.h"

//////////////////////////////////////////////////////////////////////////////

#ifdef UNICODE

//****************************************************************************
//      _CUTF8String class
//****************************************************************************
_CUTF8String::_CUTF8String(const wchar_t* aStr)
{
	m_buffer = NULL;
	m_error = false;
	if (aStr!=NULL)
	{
		m_length = WideCharToMultiByte( CP_UTF8, 0, aStr, -1, m_buffer, 0, NULL, NULL );
		if (m_length > 0)
		{
			m_buffer = new char[m_length + 1];
			WideCharToMultiByte( CP_UTF8, 0, aStr, -1, m_buffer, m_length+1, NULL, NULL );
		}
	}
}

//****************************************************************************
_CUTF8String::_CUTF8String(const wchar_t* aStr, char *out, int outlen)
{
	m_buffer = NULL;
	m_error = false;
	if (aStr!=NULL)
	{
		int res = WideCharToMultiByte( CP_UTF8, 0, aStr, -1, out, outlen-1, NULL, NULL );
		out[outlen-1] = 0;
		if (res == 0)
		{
			out[0] = 0;
			m_error = true;
		}
	}
}

//****************************************************************************
_CUTF8String::~_CUTF8String()
{
	if (m_buffer)
		delete[] m_buffer;
}


//****************************************************************************
char* _CUTF8String::c_str()
{
	return m_buffer;
}


//****************************************************************************
_CUTF8String::operator char*( )
{
	return m_buffer;
}



//****************************************************************************
//      _CUNICODEString class
//****************************************************************************
_CUNICODEString::_CUNICODEString(const char* aStr)
{
	m_buffer = NULL;
	if (aStr!=NULL)
	{
		m_length = MultiByteToWideChar( CP_UTF8, 0, aStr, -1, m_buffer, 0 );
		if (m_length > 0)
		{
			m_buffer = new wchar_t[m_length + 1];
			MultiByteToWideChar( CP_UTF8, 0, aStr, -1, m_buffer, m_length+1 );
		}
	}
}


//****************************************************************************
_CUNICODEString::~_CUNICODEString()
{
    if (m_buffer)
        delete[] m_buffer;
}


//****************************************************************************
wchar_t* _CUNICODEString::c_str()
{
    return m_buffer;
}


//****************************************************************************
_CUNICODEString::operator wchar_t*( )
{
    return m_buffer;
}



//****************************************************************************
//      _CUNICODEStringFromWide class
//****************************************************************************
//****************************************************************************
//
//  NOTE:  This version of the constructor accepts a wide character string.
//    The presumption is that we are receiving a wide character string
//    (or CString) that contains a UTF-8 encoded string that needs to
//    be converted to UNICODE.  Several places exist in the code where
//    strings received from the server are UTF-8 encoded, but are passed
//    up the food chain from the low-level libraries as CStrings.
//
//****************************************************************************
_CUNICODEStringFromWide::_CUNICODEStringFromWide(const wchar_t *aStr)
{
    m_buffer = NULL;
    if( aStr != NULL )
    {
        //  Convert string from UTF8 (stored in a CStringT) to UNICODE
        BOOL        fAlreadyReportedABug = FALSE;
        char        *pBuf       = NULL;
        char        *pP;
        int         i;
        int         iLen        = wcslen( aStr );
        wchar_t     wChar;

        //  First, convert from wide chars to single chars.  Since the source string
        //    is UTF-8 each source wide-char SHOULD have all zeros in the upper byte.
        //    As a precaution, we check each char as we convert it and dump a message
        //    to the log if we find any offensive characters.
        pBuf = new char[ iLen + 1 ];
        pP = pBuf;

        for( i=0; i<iLen; i++, pP++ )
        {
            wChar = *( aStr + i );
            if( 0xFFFFFF00 & wChar )
            {
                //  We should never get here if the input string is really UTF-8!
                if( !fAlreadyReportedABug )
                {
//                    infof( "Unexpected character encountered in UTF-8 string at position %d, char value 0x%x = %d:  %S", i, wChar, wChar, aStr );
                    fAlreadyReportedABug = TRUE;    //  only report one problem per string...
                }
            }
            *pP = ( 0xFF & wChar );
        }

        *pP = 0;        //  Don'f forget the terminator

        //  Finally, convert the single byte, UTF-8 encoded string we just created to Unicode
        m_length = MultiByteToWideChar( CP_UTF8, 0, pBuf, -1, m_buffer, 0 );
        if( m_length > 0 )
        {
            m_buffer = new wchar_t[ m_length + 1 ];
            MultiByteToWideChar( CP_UTF8, 0, pBuf, -1, m_buffer, m_length+1 );
        }

        delete pBuf;
        pBuf = NULL;
    }
}


//****************************************************************************
_CUNICODEStringFromWide::~_CUNICODEStringFromWide()
{
	if (m_buffer)
		delete[] m_buffer;
}


//****************************************************************************
wchar_t* _CUNICODEStringFromWide::c_str()
{
	return m_buffer;
}


//****************************************************************************
_CUNICODEStringFromWide::operator wchar_t*( )
{
	return m_buffer;
}



//****************************************************************************
//      _CMBCSToWideNoXlate class
//
//      Simply convert a multi-byte string to a Wide Character string by
//      copying bytes - not translating from UTF8 to wind character.
//****************************************************************************
_CMBCSToWideNoXlate::_CMBCSToWideNoXlate(const char* aStr)
{
    m_buffer = NULL;
    if( aStr != NULL )
    {
        wchar_t     *pP;
        int         i;
        int         iLen        = strlen( aStr );

        m_buffer = new wchar_t[ iLen + 1 ];
        pP = m_buffer;

        for( i=0; i<iLen; i++, pP++ )
        {
            *pP = 0x00ff & *( aStr + i );
        }

        *pP = 0;        //  Don'f forget the terminator
    }
}


//****************************************************************************
_CMBCSToWideNoXlate::~_CMBCSToWideNoXlate()
{
    if (m_buffer)
        delete[] m_buffer;
}


//****************************************************************************
wchar_t* _CMBCSToWideNoXlate::c_str()
{
    return m_buffer;
}


//****************************************************************************
_CMBCSToWideNoXlate::operator wchar_t*( )
{
    return m_buffer;
}

#endif      //  #ifdef UNICODE
