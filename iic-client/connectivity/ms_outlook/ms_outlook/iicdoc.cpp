// IICDoc.cpp : implementation of the CIICDoc class
//

#include "stdafx.h"
//#include "IIC.h"
#include <process.h>

#include "iicdoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


bool g_debug = false;


/////////////////////////////////////////////////////////////////////////////
// CIICDoc

//IMPLEMENT_DYNCREATE(CIICDoc, CDocument)

BEGIN_MESSAGE_MAP(CIICDoc, CDocument)
	//{{AFX_MSG_MAP(CIICDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
//	ON_COMMAND(ID_FILE_SEND_MAIL, OnFileSendMail)
//	ON_UPDATE_COMMAND_UI(ID_FILE_SEND_MAIL, OnUpdateFileSendMail)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIICDoc construction/destruction

CIICDoc::CIICDoc()
{
    // Get the windows version...
    m_bIsWindowsNTOrLater = FALSE;
    OSVERSIONINFO osversioninfo;
    osversioninfo.dwOSVersionInfoSize = sizeof(osversioninfo);

    if (GetVersionEx(&osversioninfo))
    {
        m_bIsWindowsNTOrLater = (osversioninfo.dwPlatformId == VER_PLATFORM_WIN32_NT);
    }

    // Set locale
    //   Was already done in CIICApp::InitInstance( )
//    CString cstrT;
//    cstrT.LoadString( IDS_IICDOC_LOCALE_ENGLISH );
////    setlocale(LC_ALL, cstrT );
//    setlocale(LC_ALL, "English" );

    // Initialize TimeZone
    _tzset();

    // Initialize members...
    m_hwndMain = NULL;
    
    m_bIsInitialized = FALSE;
}

CIICDoc::~CIICDoc()
{
	POSITION pos;
	for( pos = m_ErrorList.GetHeadPosition(); pos != NULL; )
	{
		CString* err = (CString*)m_ErrorList.GetNext(pos);
		delete err;
	}
	m_ErrorList.RemoveAll();
}

BOOL CIICDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CIICDoc serialization

void CIICDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CIICDoc diagnostics

#ifdef _DEBUG
void CIICDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CIICDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CIICDoc Implementation
/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// Initialize
//
void CIICDoc::Initialize(CWnd *aMainWnd)
{
    if (m_bIsInitialized == FALSE)
    {
        m_bIsInitialized = TRUE;
        m_hwndMain = aMainWnd->m_hWnd;

        CString strScreenName;

		m_Directories.Initialize(this);
        m_Groups.Initialize(this);
        m_Contacts.Initialize(this);
    }
}

/////////////////////////////////////////////////////////////////////////////
// DumpContacts
//
void CIICDoc::DumpContacts()
{
    TRACE("=======================\n=== DUMP CONTACTS ===\n");
    m_Contacts.DumpContacts();
}

////////////////////////////////////////////////////////////////////////////
// Clear
//
void CIICDoc::Clear()
{
    m_Groups.ClearAll();
    m_Contacts.ClearAll();
    m_Directories.ClearAll();
}

/////////////////////////////////////////////////////////////////////////////
// CIICDoc commands

void CIICDoc::OnCloseDocument() 
{
//	m_Server.ServerExitThread();
	
	CDocument::OnCloseDocument();
}

void CIICDoc::LogError(CString& aErr)
{
    CSingleLock singleLock(&m_ErrorCriticalSection);
    singleLock.Lock();  

	m_ErrorList.AddHead(new CString(aErr));

	if (m_ErrorList.GetCount()>100)	// TODO: make it configurable
	{
		CString* err = (CString*)m_ErrorList.RemoveTail();
		delete err;
	}

    singleLock.Unlock();
}

void CIICDoc::GetErrors(CListBox& aListCtrl)
{
    CSingleLock singleLock(&m_ErrorCriticalSection);
    singleLock.Lock();  

	POSITION pos;
	for( pos = m_ErrorList.GetHeadPosition(); pos != NULL; )
	{
		CString* err = (CString*)m_ErrorList.GetNext(pos);

		aListCtrl.AddString(*err);
	}

    singleLock.Unlock();
}



