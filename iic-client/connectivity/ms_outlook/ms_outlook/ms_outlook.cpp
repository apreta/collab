// ms_outlook.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "ms_outlook.h"

#include "utf8_utils.h"
#include "importmapi.h"

#include "dialogconfiguresubcription.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// Cms_outlookApp

BEGIN_MESSAGE_MAP(Cms_outlookApp, CWinApp)
END_MESSAGE_MAP()

bool			gInitialized = false;
bool			gConfigured = true;

CDialogConfigureSubcription	*gConfigureDlg = NULL;

CIICDoc			*giicDoc = NULL;
CImportMAPI     gMapi;
CString			gConfigDir;

CStringArray    garrFolders;
CStringArray    garrABs;

CStringArray    garrStateFolders;
CStringArray    garrStateABs;

CPtrArray           garrContacts;
CStringArray        gfolders;
CStringArray        gbooks;

CPtrArray           garrGroups;

char strName[] = {"Outlook"};

Cms_outlookApp  theApp;

FILE *glogf = NULL;

void writeStateInfo( );


void debugf( char *p, ... )
{
	if (!glogf)
		return;

    char tbuffer [9];
    _strtime( tbuffer );
	fprintf(glogf, "%s: ", tbuffer); 

	va_list argptr;
	va_start(argptr, p);

	vfprintf( glogf, p, argptr );
	fflush(glogf);
}


//*****************************************************************************
void connStateToFolders( CStringArray &folders )
{
    folders.RemoveAll();
    folders.Copy( garrStateFolders);
}

void connStateToABs( CStringArray &books )
{
    books.RemoveAll();
    books.Copy( garrStateABs);
}

void connFoldersToState( CStringArray &folders )
{
    garrStateFolders.RemoveAll();
    garrStateFolders.Copy( folders );
}

void connABsToState( CStringArray &books )
{
    garrStateABs.RemoveAll();
    garrStateABs.Copy( books );
}


//*****************************************************************************
bool ConfigureOutlook( HWND hWnd )
{
	bool    importNeeded = false;

	if (gConfigureDlg)
	{
		gConfigureDlg->SetForegroundWindow();
		return false;
	}

	CDialogConfigureSubcription     dlg( CWnd::FromHandle( hWnd ) );
	gConfigureDlg = &dlg;

    connStateToFolders( dlg.m_Folders );
    connStateToABs( dlg.m_AddressBooks );

    if( dlg.DoModal( ) == IDOK )
    {
        CStringArray folders, books;

        connStateToFolders( folders );
        connStateToABs( books );

        connFoldersToState( dlg.m_Folders );
        connABsToState( dlg.m_AddressBooks );

        // now figure out if they are changed;
        if( folders.GetSize()!=dlg.m_Folders.GetSize() ||
              books.GetSize()!=dlg.m_AddressBooks.GetSize())
        {
            importNeeded = TRUE;
        }

        if( importNeeded==FALSE && folders.GetSize()==dlg.m_Folders.GetSize() )
        {
            for (int i=0; i<folders.GetSize(); i++)
            {
                for (int j=0; j<dlg.m_Folders.GetSize(); j++)
                {
                    if (folders.GetAt(i)!=dlg.m_Folders.GetAt(j))
                    {
                        importNeeded = TRUE;
                    }
                }

                if (importNeeded) break;
            }
        }

        if (importNeeded==FALSE && books.GetSize()==dlg.m_AddressBooks.GetSize())
        {
            for (int i=0; i<books.GetSize(); i++)
            {
                for (int j=0; j<dlg.m_AddressBooks.GetSize(); j++)
                {
                    if (books.GetAt(i)!=dlg.m_AddressBooks.GetAt(j))
                    {
                        importNeeded = TRUE;
                    }
                }

                if (importNeeded) break;
            }
        }

		writeStateInfo();

		gConfigured = true;
    }

	gConfigureDlg = NULL;
    return importNeeded;
}


//*****************************************************************************
// Cms_outlookApp construction
Cms_outlookApp::Cms_outlookApp()
{
}


//*****************************************************************************
// Cms_outlookApp initialization
BOOL Cms_outlookApp::InitInstance()
{
	CWinApp::InitInstance();

	return TRUE;
}


//*****************************************************************************
// Friendly name for plugin
MS_OUTLOOK_API int __stdcall connGetInfo( char *pC, int iMaxLen, long *lFlags )
{
    strncpy( pC, strName, iMaxLen - 1 );
    pC[ iMaxLen - 1 ] = 0;

	if ((int)strlen(strName) >= iMaxLen)
		return STATUS_OVERFLOW;
	return STATUS_OK;
}


//*****************************************************************************
int parseStateInfo( CString S2 )
{
    CCSV    CSV;
    CString Field, type;

	CSV.Set( &Field, S2 );

    // Get all fields for this line
    if( CSV.GetNextField( ) )
    {
        type = Field;
        if( CSV.GetNextField( ) )
        {
            //  Line contained two strings
            if( type.CompareNoCase( _T( "folder" ) )  ==  0 )
            {
                garrStateFolders.Add( Field );
                return garrStateFolders.GetCount( );
            }
            else if( type.CompareNoCase( _T( "addressbook" ) )  ==  0 )
            {
                garrStateABs.Add( Field );
                return garrStateABs.GetCount( );
            }
        }
    }

    return 0;
}


//*****************************************************************************
void writeStateInfo( )
{
	CString configFile = CString(gConfigDir) + _T("\\ms_outlook.cfg");
	CFile File;
	if (File.Open(configFile, CFile::modeCreate|CFile::modeWrite|CFile::shareDenyWrite)) 
	{
		CArchive arStore(&File, CArchive::store);

		for (int i=0; i<garrStateFolders.GetCount(); i++)
		{
			arStore.WriteString(_T("folder,"));

			CString folder = garrStateFolders[i];
			CString out;

			folder.Replace(_T("\""), _T("\"\""));
			out.Format(_T("\"%s\"\n"), folder);
			arStore.WriteString(out);
		}

		for (int i=0; i<garrStateABs.GetCount(); i++)
		{
			arStore.WriteString(_T("addressbook,"));

			CString folder = garrStateABs[i];
			CString out;

			folder.Replace(_T("\""), _T("\"\""));
			out.Format(_T("\"%s\"\n"), folder);
			arStore.WriteString(out);
		}

		arStore.Close();
	}
}


//*****************************************************************************
MS_OUTLOOK_API int __stdcall connInitialize( const char *userName, const char *configDir )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	int stat = 0;

	if ( !gInitialized )
	{
		giicDoc = new CIICDoc();
		giicDoc->SetAuthUserId(CString(userName));
		gConfigDir = configDir;

		CString logFile = CString(configDir) + _T("\\ms_outlook.log");
		glogf = _wfopen(logFile, _T("w"));

		CString configFile = CString(configDir) + _T("\\ms_outlook.cfg");
		CFile File;
		if (File.Open(configFile, CFile::modeRead|CFile::shareDenyNone)) 
		{
			CArchive Ar(&File, CArchive::load);
			CString S;
			while(Ar.ReadString(S)) 
			{ 
				parseStateInfo( S );
			}
		}

		gInitialized = true;
	}

	return stat;
}


//*****************************************************************************
MS_OUTLOOK_API void __stdcall connShutdown( )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if ( gInitialized )
	{
	    gMapi.Shutdown();

		debugf( "MAPI shutdown\n" );

		gInitialized = false;
	}
}


//*****************************************************************************
// Massage groups from importmapi class into easier form for returning to caller
int convertGroups(CMapStringToString& mapGroups, CMapStringToPtr& mapMembers)
{
	debugf( "Converting groups\n" );

	if( mapGroups.GetCount( )  >  0 )
    {
        CMapStringToString::CPair*  pCurVal;

        pCurVal= mapGroups.PGetFirstAssoc( );
        while( pCurVal  !=  NULL )
        {
            debugf( "Group %S: %S\n", (LPCTSTR) pCurVal->key , (LPCTSTR) pCurVal->value );
			CDocGroup *pGroup = new CDocGroup(giicDoc, NULL, pCurVal->key, pCurVal->value, kGroupTypeOther);
			garrGroups.Add(pGroup);

			CStringArray *pMembers = NULL;
			
			if (mapMembers.Lookup(pCurVal->key, (void*&) pMembers))
			{
				for (int i=0; i<pMembers->GetCount(); i++)
				{
					CString member = pMembers->ElementAt(i);
					pGroup->AddUser( member );
				}
			}

            pCurVal= mapGroups.PGetNextAssoc( pCurVal );
        }
    }

	debugf( "Converting groups completed\n" );
	return 0;
}


//*****************************************************************************
// ToDo: convert to async by importing in a background thread
MS_OUTLOOK_API BOOL __stdcall connStartImport( )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CMapStringToString fieldDefaults;
    fieldDefaults.SetAt(kContactDirectory,      CDocDirectories::kOutlookDirectory);
    fieldDefaults.SetAt(kContactGroup,          CDocGroups::kOutlookGroup);
    fieldDefaults.SetAt(kContactUserType,       kUserTypeContact);
    fieldDefaults.SetAt(kContactServer,         _T(""));
    fieldDefaults.SetAt(kContactProfileId,      kDefaultProfileId);
    fieldDefaults.SetAt(kContactUserId,         _T(""));
    fieldDefaults.SetAt(kContactPassword,       _T(""));
    fieldDefaults.SetAt(kContactUserName,       _T(""));
    fieldDefaults.SetAt(kContactPrivMaxPriority,    kDefaultMaxPriority);
    fieldDefaults.SetAt(kContactPrivPriorityType,   kDefaultPriorityType);
    fieldDefaults.SetAt(kContactPrivEnabled,    kDefaultEnabled);
    fieldDefaults.SetAt(kContactPrivDisabled,   kDefaultDisabled);

    garrContacts.RemoveAll( );
	garrGroups.RemoveAll( );
    gfolders.RemoveAll( );
    gbooks.RemoveAll( );

    gfolders.Copy( garrStateFolders );
    gbooks.Copy( garrStateABs );

	CMapStringToString  mapGroups;
	CMapStringToPtr     mapMembers;
	CMapStringToPtr     mapGroupsInGroups;

    int iRet;
    iRet = gMapi.GetContacts( giicDoc,
                             fieldDefaults,
                             garrContacts,
                             true,             //  use default folder
                             true,             //  we want distribution list info
                             mapGroups,
                             mapMembers,
                             gfolders,
                             gbooks,
                             mapGroupsInGroups );

	convertGroups( mapGroups, mapMembers );
	
	return true;
}

//*****************************************************************************
// ToDo: convert to async by importing in a background thread
MS_OUTLOOK_API void __stdcall connStopImport( )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if ( gInitialized )
	{
	    gMapi.Stop();

		debugf( "MAPI stopped\n" );
	}
}

//*****************************************************************************
MS_OUTLOOK_API BOOL __stdcall connIsReady( )
{
//	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	return true;
}


//*****************************************************************************
MS_OUTLOOK_API BOOL __stdcall connIsConfigured( )
{
//	AFX_MANAGE_STATE(AfxGetStaticModuleState());
	
	return garrStateFolders.GetCount() + garrStateABs.GetCount() > 0;
}


//*****************************************************************************
MS_OUTLOOK_API BOOL __stdcall connConfigureDialog( void *windowHandle )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	HWND hWnd = (HWND)windowHandle;

    if( !ConfigureOutlook( hWnd ) )
        return FALSE;

	return TRUE;
}


//*****************************************************************************
MS_OUTLOOK_API void __stdcall connClearState( )
{
//	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	garrFolders.RemoveAll( );
    garrABs.RemoveAll( );
    garrStateFolders.RemoveAll( );
    garrStateABs.RemoveAll( );
    garrContacts.RemoveAll( );
    garrGroups.RemoveAll( );
    gfolders.RemoveAll( );
    gbooks.RemoveAll( );
}


//*****************************************************************************
MS_OUTLOOK_API int __stdcall connConfigureItem( const char *pC )
{
//	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CCSV    CSV;
    CString Field, type;
    CString S2;
	int stat = STATUS_FAIL;

    S2 = CUNICODEString( pC );
    CSV.Set( &Field, S2 );

    // Get all fields for this line
    if( CSV.GetNextField( ) )
    {
        type = Field;
        if( CSV.GetNextField( ) )
        {
            //  Line contained two strings
            if( type.CompareNoCase( _T( "folder" ) )  ==  0 )
            {
                garrStateFolders.Add( Field );
				stat = 0;
            }
            else if( type.CompareNoCase( _T( "addressbook" ) )  ==  0 )
            {
                garrStateABs.Add( Field );
				stat = 0;
            }
        }
    }

	if (!stat)
	{
		writeStateInfo();
		gConfigured = true;
	}

    return stat;
}


//*****************************************************************************
MS_OUTLOOK_API int __stdcall connGetFolder( int iIndex, char *pC, int iMaxLen )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if( iIndex < 0  ||  iIndex > garrFolders.GetCount( ) )
        return STATUS_FAIL;

	CString strT = garrFolders.ElementAt( iIndex );
	_CUTF8String encT( strT, pC, iMaxLen );

	if (encT.IsComplete())
		return STATUS_OK;
	return STATUS_OVERFLOW;
}


//*****************************************************************************
MS_OUTLOOK_API int __stdcall connFetchFolders( )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	garrFolders.RemoveAll( );
    garrABs.RemoveAll( );

    gMapi.GetFoldersAndAddressBooks( giicDoc, garrFolders, garrABs );

    int iNumFolders = (int) garrFolders.GetCount( );
    int iNumABs = (int) garrABs.GetCount( );

	return iNumFolders;
}

#if 0
//*****************************************************************************
MS_OUTLOOK_API int connGetAB( int iIndex, char *pC, int iLen )
{
    CString     strT    = garrABs.ElementAt( iIndex );
    strcpy( pC, CUTF8String( strT ) );
    return strlen( pC );
}
#endif


//*****************************************************************************
MS_OUTLOOK_API int __stdcall connGetGroupCount( )
{
//	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	return garrGroups.GetCount( );
}


//*****************************************************************************
MS_OUTLOOK_API int __stdcall connGetGroupInfo( int iIndex, int *iGroupSize, char *pId, int iIdLen, char *pdata, int iMaxLen )
{
//	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if( iIndex < 0  ||  iIndex >= garrGroups.GetSize( ) )
	{
		debugf("Failed to fetch group %d\n", iIndex);
        return STATUS_FAIL;
	}

	CDocGroup *pGroup = (CDocGroup *) garrGroups.GetAt(iIndex);
	*iGroupSize = pGroup->GetUserCount();

    CString strT;

	// Copy group id
	pGroup->GetGroupId(strT);
	_CUTF8String encId( strT, pId, iIdLen );

	// Copy group name
	pGroup->GetName( strT );
	_CUTF8String encT( strT, pdata, iMaxLen );

	debugf("Group info for %i: %s %d %s\n", iIndex, pdata, *iGroupSize, pId);

	if (encId.IsComplete() && encT.IsComplete())
		return STATUS_OK;
	return STATUS_OVERFLOW;
}


//*****************************************************************************
MS_OUTLOOK_API int __stdcall connGetGroupMember( int iIndex, int iMember, char *pdata, int iMaxLen )
{
//	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if( iIndex < 0  ||  iIndex >= garrGroups.GetSize( ) )
	{
		debugf("Failed to fetch group %d\n", iIndex);
        return STATUS_FAIL;
	}

	CDocGroup *pGroup = (CDocGroup *) garrGroups.GetAt(iIndex);
	if ( iMember < 0 || iMember >= pGroup->GetUserCount() )
	{
		debugf("Failed to fetch group member %d, %d\n", iIndex, iMember);
		return STATUS_FAIL;
	}

	CString strT = pGroup->GetUser( iMember );

	_CUTF8String encT( strT, pdata, iMaxLen );

	if (encT.IsComplete())
		return STATUS_OK;
	return STATUS_OVERFLOW;
}

#if 0
//*****************************************************************************
void dumpContacts( )
{
    debugf1( "\n* * * CONTACTS * * * - number of contacts returned = %d\n", (void *) garrContacts.GetCount( ) );
    CString strT;
    for( int loop=0; loop < (int) garrContacts.GetSize(); loop++ )
    {
        CDocContact     *pC = (CDocContact *) garrContacts.GetAt( loop );

        pC->GetData( kContactFirst   , strT );
        debugf2( "category:data is:  %s  --  %s", CUTF8String( kContactFirst ), CUTF8String( strT ) );
        pC->GetData( kContactLast    , strT );
        debugf2( "category:data is:  %s  --  %s", CUTF8String( kContactLast ), CUTF8String( strT ) );
        debugf( " " );
    }
}


//*****************************************************************************
void dumpStrArr( CStringArray &arr, CString &cstrName )
{
    char    cBuf[ 10 ];
    debugf2( "\n* * * %s * * * - number of elements in array = %s",
             CUTF8String( cstrName ), _itoa( arr.GetCount( ), cBuf, 10 ) );
    CString strT;
    for( int loop=0; loop < (int) arr.GetSize(); loop++ )
    {
        debugf( CUTF8String( arr.ElementAt( loop ) ) );
    }
    debugf( " " );
}


//*****************************************************************************
void dumpMapStrStr( CMapStringToString &map, CString &cstrName )
{
    char    cBuf[ 10 ];
    _itoa( map.GetCount( ), cBuf, 10 );
    debugf2( "\n* * * %s * * * - number of elements in map = %s",
             CUTF8String( cstrName ), cBuf );

    if( map.GetCount( )  >  0 )
    {
        CMapStringToString::CPair*  pCurVal;

        pCurVal= map.PGetFirstAssoc( );
        while( pCurVal  !=  NULL )
        {
            debugf2( "Current key value at %s: %s\n", CUTF8String( pCurVal->key ), CUTF8String( pCurVal->value ) );
            pCurVal= map.PGetNextAssoc( pCurVal );
        }
    }
    debugf( " " );
}


//*****************************************************************************
void dumpMapStrPtr( CMapStringToPtr &map, CString &cstrName )
{
    debugf( "entering dumpMapStrPtr( )" );
    char    cBuf[ 10 ];
    _itoa( map.GetCount( ), cBuf, 10 );
    debugf2( "\n* * * %s * * * - number of elements in map = %s",
             CUTF8String( cstrName ), cBuf );

    CStringArray    *pUserArray;
    CString         key;
    POSITION        pos;
    pos = map.GetStartPosition( );
    while( pos  !=  NULL )
    {
        map.GetNextAssoc( pos, key, (void *&) pUserArray );
        debugf2( "Current key value is %s: number of assoc strings is %s\n",
                 CUTF8String( key ), _itoa( pUserArray->GetCount( ), cBuf, 10 ) );
    }

    debugf( " " );
}
#endif


//*****************************************************************************
MS_OUTLOOK_API int __stdcall connGetContactCount( )
{
//	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	return garrContacts.GetCount( );
}


//*****************************************************************************
MS_OUTLOOK_API int __stdcall connGetContactInfo( int iIndex, const char *pname, char *pdata, int iMaxLen )
{
//	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	if( iIndex < 0  ||  iIndex >= garrContacts.GetSize( ) )
        return STATUS_FAIL;

    CDocContact *pC  = (CDocContact *) garrContacts.GetAt( iIndex );
    if( !pC )
        return STATUS_FAIL;

    CString     strT;
    CString     cstrName1( pname );
    BOOL        fRet = pC->GetData( cstrName1, strT );

	_CUTF8String encT( strT, pdata, iMaxLen );

	if (encT.IsComplete())
		return STATUS_OK;
	return STATUS_OVERFLOW;
}
