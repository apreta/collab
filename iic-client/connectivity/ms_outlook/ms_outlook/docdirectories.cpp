// DocDirectories.cpp: implementation of the CDocDirectories class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "iic.h"
#include "docdirectories.h"
#include "docdirectory.h"
#include "iicdoc.h"
//#include "utils.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Statics
//////////////////////////////////////////////////////////////////////

// static to indicate that m_OutlookIndex has not been set
int CDocDirectories::kDirectoryNone = -1;

// static for Outlook directoryid
TCHAR *CDocDirectories::kOutlookDirectory = _T("1");

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDocDirectories::CDocDirectories()
{
    m_IsSubscriptionChanged = FALSE;
    m_OutlookIndex = kDirectoryNone;
    m_MessengerIndex = kDirectoryNone;
    m_YahooIndex = kDirectoryNone;
    m_AOLIndex = kDirectoryNone;
}

CDocDirectories::~CDocDirectories()
{
    CDocDirectory *pDirectory = NULL;
    int count = m_Directories.GetSize();

    for (int loop=0; loop < count; loop++)
    {
        pDirectory = (CDocDirectory *) m_Directories.GetAt(loop);
        delete pDirectory;
    }
}

//////////////////////////////////////////////////////////////////////
// Implementation 
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Initialize
//
void CDocDirectories::Initialize(CIICDoc *aDocument)
{
    m_pDocument = aDocument;
}

//////////////////////////////////////////////////////////////////////
// Add
//
void CDocDirectories::Add(const CString &aId, const CString &aName, BOOL aIsSubscribed, int aType)
{
    CSingleLock singleLock(&m_DirectoriesCriticalSection);
    singleLock.Lock();  

    if (aType == dtGlobal)
    {
        aIsSubscribed = FALSE;
    }

    CDocDirectory *pDirectory = new CDocDirectory(m_pDocument, aId, aName, aIsSubscribed, aType);

    int index = m_Directories.Add(pDirectory);
    m_MapNameToIndex.SetAt(aName, index);

    if (aType == dtOutlook)
    {
        this->m_OutlookIndex = index;
    }
    else if (aType == dtMessenger)
    {
        this->m_MessengerIndex = index;
    }
    else if (aType == dtYahoo)
    {
        this->m_YahooIndex = index;
    }
    else if (aType == dtAOL)
    {
        this->m_AOLIndex = index;
    }

    singleLock.Unlock();  

}

//////////////////////////////////////////////////////////////////////
// Clear
//
void CDocDirectories::ClearAll()
{
    m_MapNameToIndex.RemoveAll();
    m_SubscribedDirectories.RemoveAll();
    m_ServerDirectories.RemoveAll();
    m_AllServerDirectories.RemoveAll();

    int count = m_Directories.GetSize();
    for (int loop=0; loop < count; loop++)
    {
        CDocDirectory *pDirectory = (CDocDirectory *) m_Directories.GetAt(loop);
        delete pDirectory;
    }
    m_Directories.RemoveAll();

    m_IsSubscriptionChanged = FALSE;
    m_OutlookIndex = kDirectoryNone;
    m_MessengerIndex = kDirectoryNone;
    m_YahooIndex = kDirectoryNone;
    m_AOLIndex = kDirectoryNone;
    m_SearchIndex = kDirectoryNone;
}

//////////////////////////////////////////////////////////////////////
// GetDirCount
//
int CDocDirectories::GetDirCount()
{
    int count;

    CSingleLock singleLock(&m_DirectoriesCriticalSection);
    singleLock.Lock();  

    count = m_Directories.GetSize();

    singleLock.Unlock();  

    return count;
}

//////////////////////////////////////////////////////////////////////
// GetDirFromIndex
//
CDocDirectory *CDocDirectories::GetDirFromIndex(int aIndex)
{
    return (CDocDirectory *)m_Directories.GetAt(aIndex);
}

//////////////////////////////////////////////////////////////////////
// GetDirFromId
//
CDocDirectory *CDocDirectories::GetDirFromId(const CString &aId)
{
    CSingleLock singleLock(&m_DirectoriesCriticalSection);
    singleLock.Lock();  

    CDocDirectory *pDirectory = NULL;

    int count = m_Directories.GetSize();
    CString strId;

    for (int loop=0; loop < count; loop++)
    {
        ((CDocDirectory *)m_Directories[loop])->GetId(strId);

        if (strId == aId)
        {
            pDirectory = (CDocDirectory *) m_Directories[loop];
            break;
        }
    }

    singleLock.Unlock();  

    return pDirectory;
}

//////////////////////////////////////////////////////////////////////
// GetDirFromGroupById
//
CDocDirectory *CDocDirectories::GetDirFromGroupById(const CString &aGroupById)
{
    CDocDirectory *pDirectory = NULL;

    if (aGroupById.Find(kDirectoryPrefix) == 0)
    {
        CString strDirectoryId = aGroupById.Mid(kDirectoryPrefixLen);
        pDirectory = GetDirFromId(strDirectoryId);
    }

    return pDirectory;
}

//////////////////////////////////////////////////////////////////////
// GetDirFromName 
//
CDocDirectory *CDocDirectories::GetDirFromName(const CString &aName)
{
    CSingleLock singleLock(&m_DirectoriesCriticalSection);
    singleLock.Lock();  

    CDocDirectory *pDirectory = NULL;
    int index = 0;

    if (m_MapNameToIndex.Lookup(aName, index))
    {
        pDirectory = (CDocDirectory *)m_Directories.GetAt(index);
    }

    // let it unlock when this goes out of scope
    // singleLock.Unlock();  

    return pDirectory;
}

//////////////////////////////////////////////////////////////////////
// GetDirOfType
//
CDocDirectory *CDocDirectories::GetDirOfType(enum DirectoryType aDirectoryType)
{
    CDocDirectory *result = NULL;
    CDocDirectory *pDirectory = NULL;

    CSingleLock singleLock(&m_DirectoriesCriticalSection);
    singleLock.Lock();  

    int count = m_Directories.GetSize();

    for (int loop=0; loop < count; loop++)
    {
        pDirectory = (CDocDirectory *)m_Directories[loop];

        if (pDirectory->GetType() == aDirectoryType)
        {
            result = pDirectory;
            break;
        }
    }

    singleLock.Unlock();  

    return result;
}

//////////////////////////////////////////////////////////////////////
// GetNameFromId
//
void CDocDirectories::GetNameFromId(const CString &aId, CString &aName)
{
    CSingleLock singleLock(&m_DirectoriesCriticalSection);
    singleLock.Lock();  

    int count = m_Directories.GetSize();
    CString strId;

    for (int loop=0; loop < count; loop++)
    {
        ((CDocDirectory *)m_Directories[loop])->GetId(strId);

        if (strId == aId)
        {
            ((CDocDirectory *)m_Directories[loop])->GetName(aName);
            break;
        }
    }

    singleLock.Unlock();  
}

//////////////////////////////////////////////////////////////////////
// GetNameFromIndex
//
void CDocDirectories::GetNameFromIndex(int aIndex, CString &aName)
{
    CSingleLock singleLock(&m_DirectoriesCriticalSection);
    singleLock.Lock();  

    ((CDocDirectory *)m_Directories[aIndex])->GetName(aName);

    singleLock.Unlock();  
}

//////////////////////////////////////////////////////////////////////
// GetIdFromName
//
void CDocDirectories::GetIdFromName(const CString &aName, CString &aId)
{
    CSingleLock singleLock(&m_DirectoriesCriticalSection);
    singleLock.Lock();  

    int index = -1;
    m_MapNameToIndex.Lookup(aName, index);
    if (index == -1)
    {
        aId = aName;
    }
    else
    {
        ((CDocDirectory *)m_Directories.GetAt(index))->GetId(aId);
    }

    singleLock.Unlock();  
}
