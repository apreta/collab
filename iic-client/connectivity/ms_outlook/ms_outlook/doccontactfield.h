// DocContactField.h: interface for the CDocContactField class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DOCCONTACTFIELD_H__C4CF1188_40E2_406C_821D_929B0B810DA6__INCLUDED_)
#define AFX_DOCCONTACTFIELD_H__C4CF1188_40E2_406C_821D_929B0B810DA6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CDocContactField : public CObject  
{
    // This map contains key/value pairs of available
    // options for this field.  Key is the value that 
    // is actually placed in the record (and database),
    // value is the text used to display the option to 
    // the user.
    CMapStringToString m_OptionList;

    // For options, we also maintain an array of just the
    // descriptions.  This is easier to use that processing
    // the above map, and it preserves the order of the items
    CStringArray m_OptionDescriptionArray;

public:
	CDocContactField();
	virtual ~CDocContactField();

    enum EditTypes 
    {
        kEditTypeString = 0, 
        kEditTypeStringValidNumeric, 
        kEditTypeOptionList,
        kEditTypePrivilegeFlag
    };

    int     m_ContactId;
    CString m_ContactColumnId;       // Should be set to one of the kContact char constants

    BOOL m_bIsDataField;             // True if field was in m_FieldNames (is an actual database field)
    BOOL m_bContactEncryptData;      // Basically just for password
    BOOL m_bContactIsDownloadable;   // True if was a member of m_ContactFieldsOLD...will not be downloaded if this is not true
    BOOL m_bContactCopyOnUpdate;     // True to copy this field value during an update

	BOOL m_bAlwaysDownload;          // True to download regardless of user selection

    BOOL m_bSearchShowInResults;     // True if this field should be displayed in the search results
    BOOL m_bSearchAllowSearch;       // True if this field should be selected as a search field
    BOOL m_bSearchInitialSelect;     // True if this field should be automatically selected
    
    BOOL      m_bEditAllowUser;      // True if User's can edit this field
    BOOL      m_bEditAllowAdmin;     // True if Admin's can edit this field
    EditTypes m_EditType;            // Type of field, string or option list
    int       m_EditMin;             // Min allowed value for numeric fields
    int       m_EditMax;             // Max allowed value for numeric fields
    bool      m_EditIsReadOnly;      // For disabled fields...

    void    AddOption(const CString &aOptionValue, const CString &aOptionDescription);
    CString GetOptionValueFromDescription(const CString &aDescription);
    CString GetOptionDescriptionFromValue(const CString &aValue);
    void    GetOptionDescriptions(CStringArray &aDescriptionArray);
};

#endif // !defined(AFX_DOCCONTACTFIELD_H__C4CF1188_40E2_406C_821D_929B0B810DA6__INCLUDED_)
