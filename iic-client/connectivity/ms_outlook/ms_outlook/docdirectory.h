// DocDirectory.h: interface for the CDocDirectory class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DOCDIRECTORY_H__8F651E35_B660_441C_ADA2_CCB8E0D6AEA8__INCLUDED_)
#define AFX_DOCDIRECTORY_H__8F651E35_B660_441C_ADA2_CCB8E0D6AEA8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Forward reference for our main document object
class CIICDoc;
class CDocGroup;

class CDocDirectory : public CObject  
{
    CIICDoc *m_pDocument;

    char *  m_IdAsChar;
    CString m_Id;
    CString m_Name;
    BOOL    m_IsSubscribed;
    BOOL    m_IsDownloaded;
    BOOL    m_IsServiceAvailable;
    int     m_Type;          // 1 = IIC GAL, 2 = Outlook, Other = Personal

    // DirectoryState m_LoadState;
    // int            m_LoadPosition;

	CDocDirectory();

public:
	CDocDirectory(CIICDoc *aDocument, const CString &aId, const CString &aName, BOOL aIsSubscribed, int aType);
	virtual ~CDocDirectory();

    void GetId(CString &aId);
    void GetGroupById(CString &aId);
    void GetName(CString &aName);
    int  GetType();

    BOOL IsSubscribed();
    BOOL IsDownloaded();
    BOOL IsService();
    BOOL IsReadOnly();
    BOOL IsGlobal();
    BOOL IsPersonal();

    CDocGroup *GetAllGroup();

    // aDirectoryType parameter is a kDirectoryType constant 
    // from CDocDirectories.h
    BOOL IsDirectoryType(const CString &aDirectoryType);

    char *GetIdBuffer();
};

#endif // !defined(AFX_DOCDIRECTORY_H__8F651E35_B660_441C_ADA2_CCB8E0D6AEA8__INCLUDED_)
