// IICDoc.h : interface of the CIICDoc class
//
/////////////////////////////////////////////////////////////////////////////


#if !defined(AFX_IICDOC_H__FB53B436_51A5_4C6E_9916_B950EEB1A162__INCLUDED_)
#define AFX_IICDOC_H__FB53B436_51A5_4C6E_9916_B950EEB1A162__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DocContacts.h"
#include "DocDirectories.h"
#include "DocGroups.h"


#include "../../../../iic/services/common/common.h"
#include "resource.h"
#include "utf8_utils.h"


#define kDocUserTypeContact 0
#define kDocUserTypeUser 1
#define kDocUserTypeAdmin 2

#define MAX_SCREEN_WIDTH 1024

#define SUPPRESS_BUDDY_GROUP true

class CIICDoc : public CDocument
{
    BOOL m_bIsInitialized;
    HWND m_hwndMain;
    BOOL m_bIsWindowsNTOrLater;
	
	CString m_strAuthUserId;

    CCriticalSection m_ErrorCriticalSection;
	CPtrList m_ErrorList;

public:
    CCriticalSection m_SessionCriticalSection;

    void Initialize(CWnd *aMainWnd);
    void StartupParameters();
    void CheckStartup();
    void DumpContacts();

	void SetAuthUserId(const CString &str)	{ m_strAuthUserId = str; }
	void GetAuthUserId(CString& str)		{ str = m_strAuthUserId; }

    void Clear();

	void LogError(CString& aErr);
	void GetErrors(CListBox& aListCtrl);

	void Sort(int aItemType);

protected: // create from serialization only
public:
	CIICDoc();
//	DECLARE_DYNCREATE(CIICDoc)

    void ClearProfiles();

public:
	virtual ~CIICDoc();

    CString     m_cstrBrandInitial;
    CString     m_cstrBrandName;

    const CString&  GetBrandInitial( )  { return m_cstrBrandInitial; }
    const CString&  GetBrandName( )     { return m_cstrBrandName; }

	CDocContacts    m_Contacts;
    CDocDirectories m_Directories;
    CDocGroups      m_Groups;

	CPtrArray       m_Profiles;  // CDocProfiles
    CString GetProfileDisplayFromValue(const CString &aProfileId);
    CString GetProfileValueFromDisplay(const CString &aProfileName);
    CString GetProfileOptions();
    CString GetProfileDefaultName();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIICDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual void OnCloseDocument();
	//}}AFX_VIRTUAL

#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

// Generated message map functions
protected:
	//{{AFX_MSG(CIICDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IICDOC_H__FB53B436_51A5_4C6E_9916_B950EEB1A162__INCLUDED_)
