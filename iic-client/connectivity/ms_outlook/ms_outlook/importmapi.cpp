// ImportMAPI.cpp: implementation of the CImportMAPI class.
//
//////////////////////////////////////////////////////////////////////

#pragma warning(disable:4786)

#include "stdafx.h"
//#include "iic.h"

#include "importmapi.h"

#include <mapiutil.h>
#include <mapiguid.h>
#include <mspst.h>
#include <string>
#include <list>
#include <map>
using namespace std;
#include "mapi_utils.h"
//#include "..\network\logger.h"
//#include "utils.h"

#include "doccontact.h"
#include "doccontacts.h"
#include "utf8_utils.h"
#include "resource.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

// Define MAPI guids we need.
// Don't want to statically link against MAPI library in case it's missing.
GUID LOCAL_IID_IDistList =
	{ 0x0002030E, 0x0000, 0x0000,
		{ 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46 }
	};
GUID LOCAL_IID_IMailUser =
	{ 0x0002030A, 0x0000, 0x0000,
		{ 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46 }
	};
GUID LOCAL_IID_IMAPIContainer = 
	{ 0x0002030B, 0x0000, 0x0000,
		{ 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46 }
	};

// Stores contact field configuration.
struct MAPIField
{
	MAPIField()	{ m_PropId = 0; m_PropSet = 0; m_ABPropId = 0; }
	CString	m_strIICName;
	CString	m_strMAPIName;
	DWORD	m_PropId;
	int		m_PropSet;
	DWORD	m_ABPropId;
};

// GUIDs for accessing outlook named properties.
// Outlook contacts only use property set 3.
GUID MAPIGUIDs[] = 
{ 
	{0},	// Prop set 1
	{0},	// Prop set 2
	{ 0x00062004, 0x0000, 0x0000,		// Prop set 3
		{ 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x46 }
	},
	{0}		// Prop set 4
};

// Outlook distribution list format
// Stored in multivalued binary custom property
// Each entry has this format (derived empirically):
//   <contactId><Member Entry Id>
//   <addressId><entryId>

// Named property ids for outlook distribution lists.
#define TAG_DISTLIST_ENTRIES 0x8055 // Binary encoding of entry ids in list
#define TAG_DISTLIST_DESCRIP 0x8054 // Binary encoding of member descriptor structure

// UIDs for identifying type of distribution list entry
BYTE contactId[] =  { 0x00, 0x00, 0x00, 0x00, 0xC0, 0x91, 0xAD, 0xD3, 0x51, 0x9D, 0xCF, 
					  0x11, 0xA4, 0xA9, 0x00, 0xAA, 0x00, 0x47, 0xFA, 0xA4, 0xC3 };
BYTE contactId2[] = { 0x00, 0x00, 0x00, 0x00, 0xC0, 0x91, 0xAD, 0xD3, 0x51, 0x9D, 0xCF, 
					  0x11, 0xA4, 0xA9, 0x00, 0xAA, 0x00, 0x47, 0xFA, 0xA4, 0xC2 };

BYTE addressId[] =	{ 0x00, 0x00, 0x00, 0x00, 0xC0, 0x91, 0xAD, 0xD3, 0x51, 0x9D, 0xCF, 
					  0x11, 0xA4, 0xA9, 0x00, 0xAA, 0x00, 0x47, 0xFA, 0xA4, 0xB5 };

BYTE distListId[] = { 0x00, 0x00, 0x00, 0x00, 0xC0, 0x91, 0xAD, 0xD3, 0x51, 0x9D, 0xCF,
					  0x11, 0xA4, 0xA9, 0x00, 0xAA, 0x00, 0x47, 0xFA, 0xA4, 0xB4 };

// Length of distribution list type identifier
#define DISTLIST_ID_LEN 21

// Number of rows to fetch in one operation
#define ROWSET_CHUNK 50

// Time to yield (in msecs) between rowsets
#define SLEEP_TIME 70


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CImportMAPI::CImportMAPI()
{
	m_propTags = NULL;
	m_addrTags = NULL;
	m_dlistTags = NULL;
	m_groupId = 0;
    doCancelOutlookDownload = FALSE;
	m_MAPIInit = FALSE;
}

CImportMAPI::~CImportMAPI()
{
	FreePropertyTags();
}

//////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////

BOOL CImportMAPI::Initialize()
{
    return TRUE;
}


BOOL CImportMAPI::Shutdown()
{
	CancelDownload();

    CSingleLock singleLock(&m_lock);
    singleLock.Lock();  

	if (m_MAPIInit)
	{
		mapi_EnsureFinished();
		m_MAPIInit = FALSE;
		debugf("MAPI shut down\n");
	}
		
    return TRUE;
}

void CImportMAPI::Stop()
{
	CancelDownload();
	Sleep(500);
}

//////////////////////////////////////////////////////////////////////
// GetContactNames
//
int CImportMAPI::GetContacts(IMAPIFolder *pFolder, IABContainer *pBook, 
                             const CMapStringToString &aDefaultValues, 
                             CPtrArray &aContactArray,
                             BOOL aUseDefault,
                             BOOL aGetDistributionLists,
                             CMapStringToString &aGroups,
                             CMapStringToPtr &aMembers)
{
	// Make two passes...first one collects all contacts & users, second
	// collects distribution lists.
	for (int pass = 0; pass < 2; pass++)
	{
		// Iterate through all contacts in folder
		if (pFolder != NULL)
		{
			IMAPITable *contents;
			HRESULT hr = pFolder->GetContentsTable(0,&contents);
			if (hr==S_OK)
			{ 
				SizedSPropTagArray(3,foldcols) = {3, {PR_ENTRYID,PR_MESSAGE_CLASS,PR_SUBJECT} };
				SRowSet *frows;
				contents->SetColumns((SPropTagArray*)&foldcols, 0);
				bool done = false;
				while (!done && doCancelOutlookDownload == FALSE)
				{
                    Sleep(SLEEP_TIME);
					hr = contents->QueryRows(ROWSET_CHUNK, 0, &frows);
					//hr = pHrQueryAllRows(contents,(SPropTagArray*)&foldcols,NULL,NULL,0,&frows);
					if (hr==S_OK)
					{ 
						for (unsigned int m=0; m<frows->cRows && doCancelOutlookDownload == FALSE; m++)
						{ 
							// Open each contact in folder and get data
							mapi_TEntryid eid; 
							string subj, msgclass;
							if (frows->aRow[m].lpProps[0].ulPropTag==PR_ENTRYID) eid=&frows->aRow[m].lpProps[0];
							if (frows->aRow[m].lpProps[1].ulPropTag==PR_MESSAGE_CLASS) msgclass=CUTF8String(frows->aRow[m].lpProps[1].Value.LPSZ);
							if (frows->aRow[m].lpProps[2].ulPropTag==PR_SUBJECT) subj=CUTF8String(frows->aRow[m].lpProps[2].Value.LPSZ);
							if (!eid.isempty())
							{ 
								IMessage *imsg; ULONG msgtype;
								hr = pFolder->OpenEntry(eid.size, eid.ab, NULL, 0, &msgtype, (IUnknown**)&imsg);
								if (hr==S_OK)
								{ 
									if (msgtype==MAPI_MESSAGE)
									{ 
										debugf("Importing contact %s\n", subj.c_str());
										if (pass == 0 && strcmp(msgclass.c_str(), "IPM.Contact")==0)
										{
											AddContact(pFolder, imsg, aDefaultValues, aContactArray);
										}
										else if (pass == 1 && strcmp(msgclass.c_str(), "IPM.DistList")==0 && aGetDistributionLists)
										{
											AddDistList(pFolder, imsg, aDefaultValues, aContactArray, aGroups, aMembers);
										}
									}
									imsg->Release();
								}
							}
						}
						if (frows->cRows == 0)
							done = true;
						pFreeProws(frows);
					}
				}
				contents->Release();
			}

		}

		// Iterate through address entries in address list.
		if (pBook != NULL)
		{
			IMAPITable *contents;
			HRESULT hr = pBook->GetContentsTable(0,&contents);
			if (hr==S_OK)
			{ 
				SizedSPropTagArray(2,foldcols) = {2, {PR_ENTRYID,PR_DISPLAY_NAME} };
				SRowSet *frows;
				contents->SetColumns((SPropTagArray*)&foldcols, 0);
				bool done = false;
				while (!done && doCancelOutlookDownload == FALSE)
				{
                    Sleep(SLEEP_TIME);
					hr = contents->QueryRows(ROWSET_CHUNK, 0, &frows);
					//hr = pHrQueryAllRows(contents,(SPropTagArray*)&foldcols,NULL,NULL,0,&frows);
					if (hr==S_OK)
					{ 
						for (unsigned int m=0; m<frows->cRows; m++)
						{ 
							// Open each address in address book.
							mapi_TEntryid eid; 
							CString name;
							if (frows->aRow[m].lpProps[0].ulPropTag==PR_ENTRYID) eid=&frows->aRow[m].lpProps[0];
							if (frows->aRow[m].lpProps[1].ulPropTag==PR_DISPLAY_NAME) name=frows->aRow[m].lpProps[1].Value.LPSZ;
							if (!eid.isempty())
							{ 
								IMailUser *iuser; ULONG usertype;
								hr = pBook->OpenEntry(eid.size, eid.ab, NULL, 0, &usertype, (IUnknown**)&iuser);
								if (hr==S_OK)
								{ 
									debugf("Importing entry %S\n", (LPCTSTR)name);
									if (pass == 0 && usertype == MAPI_MAILUSER)
									{ 
										AddABEntry(iuser, aDefaultValues, aContactArray);
									}
									else if (pass == 1 && usertype == MAPI_DISTLIST)
									{
										AddABDistList((IDistList*)iuser, name, aDefaultValues, aContactArray, aGroups, aMembers);
									}
									iuser->Release();
								}
							}
						}
						if (frows->cRows == 0)
							done = true;
						pFreeProws(frows);
					}
				}
				contents->Release();
			}
		}
	}

	return 0;
}


//*****************************************************************************
int CImportMAPI::GetContacts(CIICDoc *aDocument, 
                             const CMapStringToString &aDefaultValues, 
                             CPtrArray &aContactArray,
                             BOOL aUseDefault,
                             BOOL aGetDistributionLists,
                             CMapStringToString &aGroups,
                             CMapStringToPtr &aMembers,
							 CStringArray &aFolderArray, 
							 CStringArray &aAddressBookArray,
                             CMapStringToPtr &aGroupsInGroups)
{
    CSingleLock singleLock(&m_lock);
    singleLock.Lock();  

    doCancelOutlookDownload = FALSE;

	debugf("Starting MAPI import\n");

    CString cstrBlank( "" );
    if (!LoadMapFromCsv(aDocument, cstrBlank))
    {
	    singleLock.Unlock();
        return IDS_IIC_OUTLOOKFAILEDMAP;
    }

    CleanupMaps();
	aContactArray.RemoveAll();
	m_pDocument = aDocument;

	mapi_Folders.clear();
	mapi_AddressBooks.clear();

	if (!m_MAPIInit)
	{
		mapi_EnsureLibraries();
		if (mapi_Libraries.size()>0)
		{ 
			mapi_EnsureStores(mapi_Libraries.front().path, true);
		}
		else
		{
			debugf("No MAPI libraries available\n");
			return IDS_IIC_OUTLOOKIMPORTSUCCEEDED;
		}
		m_MAPIInit = TRUE;
	}
	

	// Open mail box and address books
	debugf("Openning MAPI folders\n");
	mapi_EnsureFolders(NULL);
	if (mapi_logonCanceled)
	{
		singleLock.Unlock();
		debugf("User cancelled logon\n");
		return IDS_IIC_OUTLOOKFAILEDDEFAULT;
	}

	debugf("Openning MAPI addrss books\n");
	mapi_EnsureAddressBooks(NULL);
	if (mapi_logonCanceled)
	{
		singleLock.Unlock();
		debugf("User cancelled logon\n");
		return IDS_IIC_OUTLOOKFAILEDDEFAULT;
	}

	// Extract contacts from configured folders
	for (list<mapi_TFolderInfo>::const_iterator i=mapi_Folders.begin(); i!=mapi_Folders.end(); i++)
	{
		for (int nFolder=0; nFolder<aFolderArray.GetSize(); nFolder++)
		{
			if (strcmp(i->name.c_str(), CUTF8String(aFolderArray.GetAt(nFolder))) == 0)
			{
				debugf("Importing from contact folder %s\n", i->name.c_str());

				IMAPIFolder * ifolder;
				ULONG ftype;
				HRESULT hr = mapi_msgstore->OpenEntry(i->eid.size,i->eid.ab, NULL,0,&ftype,(IUnknown**)&ifolder);
				if (hr==S_OK)
				{ 
					if (ftype==MAPI_FOLDER)
					{ 
						IMAPIFolder* pFolder = ifolder;
						
						GetContacts(pFolder, NULL, aDefaultValues, 
												 aContactArray,
												 aUseDefault,
												 aGetDistributionLists,
												 aGroups,
												 aMembers);

						if (pFolder != NULL)
						{
							pFolder->Release();
							pFolder = NULL;
						}

						aFolderArray.RemoveAt(nFolder);
						break;
					}
					ifolder->Release();
				}
			}
		}
	}

	// And also from configured address books
	for (list<mapi_TAddressBookInfo>::const_iterator i2=mapi_AddressBooks.begin(); i2!=mapi_AddressBooks.end(); i2++)
	{ 
		for (int nBook=0; nBook<aAddressBookArray.GetSize(); nBook++)
		{
			if (strcmp(i2->name.c_str(), CUTF8String(aAddressBookArray.GetAt(nBook))) == 0)
			{
				debugf("Importing from address book %s\n", i2->name.c_str());

				IABContainer * icontainer;
				ULONG ftype;
				HRESULT hr = mapi_addrbook->OpenEntry(i2->eid.size,i2->eid.ab, NULL,0,&ftype,(IUnknown**)&icontainer);
				if (hr==S_OK)
				{ 
					if (ftype==MAPI_ABCONT)
					{ 
						IABContainer *pBook = icontainer;
						GetContacts(NULL, pBook, aDefaultValues, 
												 aContactArray,
												 aUseDefault,
												 aGetDistributionLists,
												 aGroups,
												 aMembers);

						if (pBook != NULL)
						{
							pBook->Release();
							pBook = NULL;
						}
						
						aAddressBookArray.RemoveAt(nBook);
						break;
					}
					icontainer->Release();
				}
			}
		}
	}

    ResolveGroupsInGroups(aGroupsInGroups);

    CleanupMaps();

    singleLock.Unlock();
	
	debugf("MAPI import complete\n");
    return IDS_IIC_OUTLOOKIMPORTSUCCEEDED;
}


//*****************************************************************************
void CImportMAPI::AddGroupInGroup(CString &aGroupEntryId, CString &aGroupMemberId)
{
	CStringArray *pGroupInGroupArray;

	debugf("Adding group in group: %S - %S\n", (LPCTSTR)aGroupEntryId, (LPCTSTR)aGroupMemberId);

	if (m_mapGroupsInGroups.Lookup(aGroupEntryId, (void *&)pGroupInGroupArray))
	{
		TRACE("Adding to existing array\n");
		pGroupInGroupArray->Add(aGroupMemberId);
	}
	else
	{   
		TRACE("Creating new array\n");
		pGroupInGroupArray = new CStringArray();
		pGroupInGroupArray->Add(aGroupMemberId);
		m_mapGroupsInGroups.SetAt(aGroupEntryId, pGroupInGroupArray);
	}
}


//*****************************************************************************
// Resolve Group In Group map to return a map containing
// our generated GroupIds (right now, they are mapped 
// by EntryIds, which was necessary until we were sure to
// have generated GroupIds for all distlists)
void CImportMAPI::ResolveGroupsInGroups(CMapStringToPtr &aGroupsInGroups)
{
    CString strGroupEntryId;
    CString strGroupId;
    CStringArray *pGroupInGroupArray;
    CString strGroupInGroupId;

    // Iterate through EntryId-based map...
    POSITION position = m_mapGroupsInGroups.GetStartPosition();

    while (position != NULL)
    {
        m_mapGroupsInGroups.GetNextAssoc(position, strGroupEntryId, (void *&)pGroupInGroupArray);

        // Try to find GroupId for the containing group...
        if (m_mapEntryIdsToGroupIds.Lookup(strGroupEntryId, strGroupId))
        {
            // Now create an array of contained groupids...
            CStringArray *pGroupInGroupIdArray = new CStringArray();

            // Iterate through the contained entryids
            for (int loop=0; loop < pGroupInGroupArray->GetSize(); loop++)
            {
                // Try to find GroupId for the contained entryid...
                if (m_mapEntryIdsToGroupIds.Lookup(pGroupInGroupArray->GetAt(loop), strGroupInGroupId))
                {
                    // Add the contained GroupId
                    pGroupInGroupIdArray->Add(strGroupInGroupId);
                }
            }

            // Add to our GroupId-based map
            aGroupsInGroups.SetAt(strGroupId, pGroupInGroupIdArray);
        }
    }
}


//*****************************************************************************
void CImportMAPI::CleanupMaps()
{
	m_mapEntryId.RemoveAll();
    m_mapEntryIdsToGroupIds.RemoveAll();

    CString strGroupEntryId;
    CStringArray *pGroupInGroupArray;
    POSITION position = m_mapGroupsInGroups.GetStartPosition();

    while (position != NULL)
    {
        m_mapGroupsInGroups.GetNextAssoc(position, strGroupEntryId, (void *&)pGroupInGroupArray);
        delete pGroupInGroupArray;
    }

    m_mapGroupsInGroups.RemoveAll();
}


//*****************************************************************************
// Add contact to output array.	
BOOL CImportMAPI::AddContact(IMAPIFolder * pFolder, IMessage * pMsg, const CMapStringToString &aDefaultValues, CPtrArray &aContactArray)
{
	if (m_propTags == NULL)
	{
		m_propTags = GetContactPropertyTags(pMsg);
		if (m_propTags == NULL)
			return FALSE;
	}

	SPropValue *values;
	ULONG numValues;

	HRESULT hRes = pMsg->GetProps(m_propTags, 0, &numValues, &values);
	if (SUCCEEDED(hRes))
	{
		CDocContact *pContact = new CDocContact(m_pDocument);

		// Get the mapped Outlook values
		MAPIField *field;
		POSITION pos;
		CString key;
		int i = 1;	// property 0 is entry id

		for( pos = m_Map.GetStartPosition(); pos != NULL; i++)
		{
			m_Map.GetNextAssoc(pos, key, (void*&)field);

			// Some properties may be type PT_ERROR, skip those
			if (PROP_TYPE(values[i].ulPropTag) == PT_TSTRING)
				pContact->SetData(field->m_strIICName, values[i].Value.LPSZ);
			else
				pContact->SetData(field->m_strIICName, _T(""));
		}

		// Add empty values for any unmapped fields
		for (int loop=0; loop < m_arrayNotMapped.GetSize(); loop++)
		{
			pContact->SetData(m_arrayNotMapped.GetAt(loop), _T(""));
		}

		// Set the given default values
		CString strFieldIIC;
		CString strValue;
		pos = aDefaultValues.GetStartPosition();
		while (NULL != pos)
		{
			aDefaultValues.GetNextAssoc(pos, strFieldIIC, strValue);
			pContact->SetData(strFieldIIC, strValue);
		}

		// Validate the name info was provided
		CString strFullName;
		CString strFirstName;
		CString strLastName;
		pContact->GetDataRaw(kContactOutlookFullName, strFullName);
		pContact->GetDataRaw(kContactFirst, strFirstName);
		pContact->GetDataRaw(kContactLast, strLastName);

		if (strFirstName.IsEmpty() && strLastName.IsEmpty())
		{
			if (!strFullName.IsEmpty())
			{
				pContact->SetData(kContactFirst, strFullName);
			}
		}

		// Add the contact to the result array and
		// get the next Outlook contact...
		int index = aContactArray.Add(pContact);

		// Set its local id if necessary....
		CString strEntryId;

		ConvertEntryID(&values[0].Value.bin, strEntryId);
		pContact->SetData(kContactUserId, strEntryId);

		m_mapEntryId.SetAt(strEntryId, strEntryId);

		pMAPIFreeBuffer(values);
	}

	return TRUE;
}


//*****************************************************************************
// Add distribution list from contact to output arrays.
BOOL CImportMAPI::AddDistList(IMAPIFolder * pFolder, IMessage * pMsg,
			const CMapStringToString &aDefaultValues, CPtrArray &aContactArray,
			CMapStringToString &aGroups, CMapStringToPtr &aMembers)
{
	if (m_dlistTags == NULL)
	{
		m_dlistTags = GetDistListPropertyTags(pMsg);
		if (m_dlistTags == NULL)
			return FALSE;
	}

    CString strAuthUserId;
	m_pDocument->GetAuthUserId(strAuthUserId);

	SPropValue *values;
	ULONG numValues;

	HRESULT hRes = pMsg->GetProps(m_dlistTags, 0, &numValues, &values);
	if (SUCCEEDED(hRes))
	{
        // Get the entry id for this distlist...
		CString strGroupEntryId;
        CString strLookup;
		ConvertEntryID(&values[0].Value.bin, strGroupEntryId);

        // Get the name...
		CString strName;
		CString strGroupId;

		if (values[1].ulPropTag != PR_SUBJECT)
			return FALSE;

		strName = values[1].Value.LPSZ;

        // Add the distribution list as a group...
        strGroupId.Format(_T("%s.%d"), strAuthUserId, ++m_groupId);
        aGroups.SetAt(strGroupId, strName);

		debugf("Adding group %S - %S\n", (LPCTSTR)strName, (LPCTSTR)strGroupId );

        // Add EntryId/Group combo to our map if not already there...
        // Used later to resolve "Groups in Groups")
        if (!m_mapEntryIdsToGroupIds.Lookup(strGroupEntryId, strLookup))
        {
            debugf("Adding group map %S - %S %S\n", (LPCTSTR)strName, (LPCTSTR)strGroupEntryId, (LPCTSTR)strGroupId);
            m_mapEntryIdsToGroupIds.SetAt(strGroupEntryId, strGroupId);
        }

        CStringArray *pUserArray;

		if (PROP_TYPE(values[2].ulPropTag) == PT_MV_BINARY)
		{
			SBinaryArray *array = &values[2].Value.MVbin;

			for (int i=0; i<(int)array->cValues; i++)
			{
				if (array->lpbin[i].cb < DISTLIST_ID_LEN)
					continue;

				bool is_contact = memcmp(array->lpbin[i].lpb, contactId, DISTLIST_ID_LEN) == 0 ||
					memcmp(array->lpbin[i].lpb, contactId2, DISTLIST_ID_LEN) == 0;
				bool is_address = memcmp(array->lpbin[i].lpb, addressId, DISTLIST_ID_LEN) == 0;
				bool is_distList = memcmp(array->lpbin[i].lpb, distListId, DISTLIST_ID_LEN) == 0;

				if (is_contact || is_address)
				{
					CString strEntryId, strUserId;
					ConvertEntryID(&array->lpbin[i], strEntryId, DISTLIST_ID_LEN);

					if (!m_mapEntryId.Lookup(strEntryId, strUserId))
					{
						// Maybe address is from an address book we didn't download--
						// try to open and add now.
						if (is_address)
						{
							IMailUser * iuser;
							ULONG ftype;
							HRESULT hr = mapi_addrbook->OpenEntry(array->lpbin[i].cb - DISTLIST_ID_LEN,
								(ENTRYID*)(array->lpbin[i].lpb + DISTLIST_ID_LEN), 
								NULL,0,&ftype,(IUnknown**)&iuser);
							if (SUCCEEDED(hr))
							{
								if (ftype == MAPI_MAILUSER)
								{
									AddABEntry(iuser, aDefaultValues, aContactArray);
								}
								iuser->Release();
							}
						}
						else if (is_contact)
						{
							// >>> Not working if contact is in a different message store
							// (e.g. public folders)--try against each message store? <<<
							IMessage *icontact;
							ULONG ftype;
							HRESULT hr = mapi_msgstore->OpenEntry(array->lpbin[i].cb - DISTLIST_ID_LEN,
								(ENTRYID*)(array->lpbin[i].lpb + DISTLIST_ID_LEN), 
								NULL,0,&ftype,(IUnknown**)&icontact);
							if (SUCCEEDED(hr))
							{
								if (ftype == MAPI_MESSAGE)
								{
									AddContact(NULL, icontact, aDefaultValues, aContactArray);
								}
								icontact->Release();
							}
						}
					}

					// Keep track of group membership.
					if (m_mapEntryId.Lookup(strEntryId, strUserId))
					{
						if (aMembers.Lookup(strGroupId, (void *&)pUserArray))
						{
							pUserArray->Add(strUserId);
						}
						else
						{   
							pUserArray = new CStringArray();
							pUserArray->Add(strUserId);
							aMembers.SetAt(strGroupId, pUserArray);
						}
					}
				}
				else if (is_distList)
				{
                    // Group In Group case...

                    // Get the distlist object
					IMessage *idistlist;
					ULONG ftype;
					HRESULT hr = mapi_msgstore->OpenEntry(array->lpbin[i].cb - DISTLIST_ID_LEN,
						(ENTRYID*)(array->lpbin[i].lpb + DISTLIST_ID_LEN), 
						NULL,0,&ftype,(IUnknown**)&idistlist);
					if (SUCCEEDED(hr))
					{
						if (ftype == MAPI_MESSAGE)
						{
							SPropValue *value;
							CString strGroupInGroupEntryId;
							HRESULT hRes = pHrGetOneProp(idistlist, PR_ENTRYID, &value);
							if (SUCCEEDED(hRes))
							{
								ConvertEntryID(&value->Value.bin, strGroupInGroupEntryId);

								// Keep track of group within groups
								AddGroupInGroup(strGroupEntryId, strGroupInGroupEntryId); 

								pMAPIFreeBuffer(value);
							}
						}

						idistlist->Release();
					}
                }  // is_distlist
			}
		}

		pMAPIFreeBuffer(values);
	}

	return TRUE;
}


//*****************************************************************************
// Add address entry from address book.
BOOL CImportMAPI::AddABEntry(IMailUser * pUser, const CMapStringToString &aDefaultValues, 
				CPtrArray &aContactArray)
{
	if (m_addrTags == NULL)
	{
		m_addrTags = GetABEntryPropertyTags(pUser);
		if (m_addrTags == NULL)
			return FALSE;
	}

	SPropValue *values;
	ULONG numValues;

	HRESULT hRes = pUser->GetProps(m_addrTags, 0, &numValues, &values);
	if (SUCCEEDED(hRes))
	{
		CDocContact *pContact = new CDocContact(m_pDocument);

		// Get the mapped Outlook values
		MAPIField *field;
		POSITION pos;
		CString key;
		int i = 1;	// property 0 is entry id

		for( pos = m_Map.GetStartPosition(); pos != NULL; )
		{
			m_Map.GetNextAssoc(pos, key, (void*&)field);

			if (field->m_ABPropId != 0)
			{
				// Some properties may be type PT_ERROR, skip those
				// Also check email for '@' to make sure its an SMTP address not an Exchange address
				if (PROP_TYPE(values[i].ulPropTag) == PT_TSTRING &&
						(values[i].ulPropTag != PR_EMAIL_ADDRESS || _tcschr(values[i].Value.LPSZ, _T('@')) != 0))
					pContact->SetData(field->m_strIICName, values[i].Value.LPSZ);
				else
					pContact->SetData(field->m_strIICName, _T(""));
				i++;
			}
			else
				pContact->SetData(field->m_strIICName, _T(""));
		}

		// Add empty values for any unmapped fields
		for (int loop=0; loop < m_arrayNotMapped.GetSize(); loop++)
		{
			pContact->SetData(m_arrayNotMapped.GetAt(loop), _T(""));
		}

		// Set the given default values
		CString strFieldIIC;
		CString strValue;
		pos = aDefaultValues.GetStartPosition();
		while (NULL != pos)
		{
			aDefaultValues.GetNextAssoc(pos, strFieldIIC, strValue);
			pContact->SetData(strFieldIIC, strValue);
		}

		// Validate the name info was provided
		CString strFullName;
		CString strFirstName;
		CString strLastName;
		pContact->GetDataRaw(kContactOutlookFullName, strFullName);
		pContact->GetDataRaw(kContactFirst, strFirstName);
		pContact->GetDataRaw(kContactLast, strLastName);

		if (strFirstName.IsEmpty() && strLastName.IsEmpty())
		{
			if (!strFullName.IsEmpty())
			{
				pContact->SetData(kContactFirst, strFullName);
			}
		}

		// Add the contact to the result array and get the next Outlook contact...
		int index = aContactArray.Add(pContact);

		// Set its local id if necessary....
		CString strEntryId;

		ConvertEntryID(&values[0].Value.bin, strEntryId);
		pContact->SetData(kContactUserId, strEntryId);

		m_mapEntryId.SetAt(strEntryId, strEntryId);

		pMAPIFreeBuffer(values);
	}

	return TRUE;
}


//*****************************************************************************
// Add distribution list from address book.
BOOL CImportMAPI::AddABDistList(IDistList * pDist, CString &aGroupName,
				const CMapStringToString &aDefaultValues, CPtrArray &aContactArray,
				CMapStringToString &aGroups, CMapStringToPtr &aMembers)
{
    CString strAuthUserId;
	m_pDocument->GetAuthUserId(strAuthUserId);

    // Add the distribution list as a group...
	CString strGroupId;
    strGroupId.Format(_T("%s.%d"), strAuthUserId, ++m_groupId);
    aGroups.SetAt(strGroupId, aGroupName);

	debugf("Adding group %S - %S\n", (LPCTSTR)aGroupName , (LPCTSTR)strGroupId );

	SPropValue *value;
	CString strGroupEntryId;
	HRESULT hRes = pHrGetOneProp(pDist, PR_ENTRYID, &value);
	if (SUCCEEDED(hRes))
	{
		ConvertEntryID(&value->Value.bin, strGroupEntryId);
		pMAPIFreeBuffer(value);
	}

    // Add EntryId/Group combo to our map if not already there...
    // Used later to resolve "Groups in Groups")
    CString strLookup;
    if (!m_mapEntryIdsToGroupIds.Lookup(strGroupEntryId, strLookup))
    {
        debugf("Adding group map %S - %S %S\n", (LPCTSTR)aGroupName, (LPCTSTR)strGroupEntryId, (LPCTSTR)strGroupId);
        m_mapEntryIdsToGroupIds.SetAt(strGroupEntryId, strGroupId);
    }

	IMAPITable *contents;
	HRESULT hr = pDist->GetContentsTable(0,&contents);
	if (hr==S_OK)
	{ 
		SizedSPropTagArray(2,foldcols) = {2, {PR_ENTRYID,PR_DISPLAY_NAME} };
		SRowSet *frows;
		contents->SetColumns((SPropTagArray*)&foldcols, 0);
		bool done = false;
		while (!done && doCancelOutlookDownload == FALSE)
		{
            Sleep(SLEEP_TIME);
			hr = contents->QueryRows(ROWSET_CHUNK, 0, &frows);
			// hr = pHrQueryAllRows(contents,(SPropTagArray*)&foldcols,NULL,NULL,0,&frows);
			if (hr==S_OK)
			{ 
				for (unsigned int m=0; m<frows->cRows; m++)
				{ 
					mapi_TEntryid eid; 
					string name;
					if (frows->aRow[m].lpProps[0].ulPropTag==PR_ENTRYID) eid=&frows->aRow[m].lpProps[0];
					if (frows->aRow[m].lpProps[1].ulPropTag==PR_DISPLAY_NAME) name=CUTF8String(frows->aRow[m].lpProps[1].Value.LPSZ);
					if (!eid.isempty())
					{ 
						IMailUser *iuser; ULONG usertype;
						hr = pDist->OpenEntry(eid.size, eid.ab, NULL, 0, &usertype, (IUnknown**)&iuser);
						if (hr==S_OK)
						{ 
							debugf("Importing member %s\n", name.c_str());
							if (usertype == MAPI_MAILUSER)
							{ 
								AddABDistListMember(iuser, strGroupId, aDefaultValues, aContactArray, aGroups, aMembers);
							}
							else if (usertype == MAPI_DISTLIST)
							{
								// Handle group in group

								SPropValue *value;
								CString strGroupInGroupEntryId;
								HRESULT hRes = pHrGetOneProp(iuser, PR_ENTRYID, &value);
								if (SUCCEEDED(hRes))
								{
									ConvertEntryID(&value->Value.bin, strGroupInGroupEntryId);

									// Keep track of group within groups
									AddGroupInGroup(strGroupEntryId, strGroupInGroupEntryId); 

									pMAPIFreeBuffer(value);
								}
							}
							iuser->Release();
						}
					}
				}
				if (frows->cRows == 0)
					done = true;
				pFreeProws(frows);
			}
		}
		contents->Release();
	}
	return TRUE;
}


//*****************************************************************************
// Add each address from address book distribution list to group.
BOOL CImportMAPI::AddABDistListMember(IMailUser * pDist, CString &aGroupId,
				const CMapStringToString &aDefaultValues, CPtrArray &aContactArray,
				CMapStringToString &aGroups, CMapStringToPtr &aMembers)
{
	SPropValue *value;
    CStringArray *pUserArray;

	HRESULT hRes = pHrGetOneProp(pDist, PR_ENTRYID, &value);
	if (SUCCEEDED(hRes))
	{
		if (value->ulPropTag == PR_ENTRYID)
		{
			CString strEntryId, strUserId;
			ConvertEntryID(&value->Value.bin, strEntryId);

			if (!m_mapEntryId.Lookup(strEntryId, strUserId))
			{
				// Maybe address is from an address book we didn't download--try to 
				// open and add now.
				IMailUser * iuser;
				ULONG ftype;
				HRESULT hr = mapi_addrbook->OpenEntry(value->Value.bin.cb,(ENTRYID*)value->Value.bin.lpb, NULL,0,&ftype,(IUnknown**)&iuser);
				if (SUCCEEDED(hr) && ftype == MAPI_MAILUSER)
				{
					AddABEntry(iuser, aDefaultValues, aContactArray);
				}
			}

            if (m_mapEntryId.Lookup(strEntryId, strUserId))
            {
                if (aMembers.Lookup(aGroupId, (void *&)pUserArray))
                {
                    pUserArray->Add(strUserId);
                }
                else
                {   
                    pUserArray = new CStringArray();
                    pUserArray->Add(strUserId);
                    aMembers.SetAt(aGroupId, pUserArray);
                }
            }
		}

		pMAPIFreeBuffer(value);
	}

	return TRUE;
}


//////////////////////////////////////////////////////////////////////
// LoadMapFromCsv
//
// Loads the map decribing which import fields map to
// which contact columns.
//
BOOL CImportMAPI::LoadMapFromCsv(CIICDoc *aDocument, const CString &aCsvFile)
{
    debugf("Loading map file %S\n", (LPCTSTR)aCsvFile);

    // Get the run path for this application 
	TCHAR szBuffer[_MAX_PATH]; 
	VERIFY(::GetModuleFileName(AfxGetInstanceHandle(), szBuffer, _MAX_PATH));
	CString sPath = (CString)szBuffer;
	sPath = sPath.Left(sPath.ReverseFind('\\'));

    CString strMapFile = sPath + _T("\\OutlookImport.csv");

    FILE    *hFile = fopen( CUTF8String( strMapFile ), "r" );
    if( hFile )
    {
        char    buffer[ MAX_PATH ];

        CString S;

        FreePropertyTags();

        while( fgets( buffer, MAX_PATH-1, hFile ))      //For all lines of the file
        {
            buffer[MAX_PATH-1] = '\0';
            //  fgets a line including the newline char
            char    *p = strchr(buffer, '\n');
            if( p )
                *p = '\0';

            S = CUNICODEString( buffer );

            MAPIField * mapiField = new MAPIField;
            int currentField = 1;

            CCSV CSV;
            CString Field;
            CSV.Set(&Field,S);

            while(CSV.GetNextField())  //for all Fields of the Record
            { 
                if (currentField == 1)
                {
                    mapiField->m_strIICName = Field;
                }
                else if (currentField == 2)
                {
                    _stscanf(Field, _T("%x"), &mapiField->m_PropId);
                }
                else if (currentField == 3)
                {
                    mapiField->m_strMAPIName = Field;
                }
                else if (currentField == 4)
                {
                    mapiField->m_PropSet = _ttoi(Field);
                }
                else if (currentField == 5)
                {
                    _stscanf(Field, _T("%x"), &mapiField->m_ABPropId);
                }

                currentField++;
            } 

            if (!mapiField->m_strIICName.IsEmpty())
                if (!mapiField->m_strMAPIName.IsEmpty())
                {
                    m_Map.SetAt(mapiField->m_strIICName, mapiField);
                }
                else 
                {
                    m_arrayNotMapped.Add(mapiField->m_strIICName);
                    m_listNotMapped.AddTail(mapiField->m_strIICName);
                    delete mapiField;
                }
        }

        fclose( hFile );
    }

    // Add any unaccounted for fields to the lists 
    // of unmapped fields...

    int count = aDocument->m_Contacts.m_FieldNames.GetSize();

    CString strField;
	MAPIField * mapiField;

    for (int loop=0; loop < count; loop++)
    {
        strField = aDocument->m_Contacts.m_FieldNames.GetAt(loop);

        if ((m_listNotMapped.Find(strField) == NULL) &&
            (!m_Map.Lookup(strField, (void*&)mapiField)))
        {
            m_arrayNotMapped.Add(strField);
            m_listNotMapped.AddTail(strField);
        }
    }

    return TRUE;
}


//*****************************************************************************
// Get property list from configuration, and resolve real ids for
// named properties.
SPropTagArray *CImportMAPI::GetContactPropertyTags(IMAPIProp * pProps)
{
	SPropTagArray * output = NULL;
	MAPIField *field;
	POSITION pos;
	CString key;

	SPropTagArray * namedProps;
	MAPINAMEID customNames[40];
	MAPINAMEID * customNamePtrs[40];
	int count = 0;

	// Get real property ids for named properties
	for( pos = m_Map.GetStartPosition(); pos != NULL && count < 40; )
	{
		m_Map.GetNextAssoc(pos, key, (void*&)field);

		if (field->m_PropSet > 0 && field->m_PropSet < 4)
		{
			customNames[count].lpguid = &MAPIGUIDs[field->m_PropSet - 1];
			customNames[count].ulKind = MNID_ID;
			customNames[count].Kind.lID = field->m_PropId;
			customNamePtrs[count] = &customNames[count];
			count++;
		}
	}

	HRESULT hRes = pProps->GetIDsFromNames(count, customNamePtrs, 0, &namedProps);
	if (SUCCEEDED(hRes))
	{
		int numProps = m_Map.GetCount() + 1;
		// Put together list of all properties we need
		output = (SPropTagArray*)malloc(CbNewSPropTagArray(numProps));

		// Always get long term entry id
		output->aulPropTag[0] = PR_ENTRYID;

		// then add rest of configured fields.
		int allPos = 1, namedPos = 0;
		for( pos = m_Map.GetStartPosition(); pos != NULL; )
		{
			m_Map.GetNextAssoc(pos, key, (void*&)field);

			if (field->m_PropSet > 0 && field->m_PropSet < 4)
			{
				if (PROP_TYPE(namedProps->aulPropTag[namedPos]) != PT_ERROR)
				{
					output->aulPropTag[allPos++] = PROP_TAG(PT_TSTRING, PROP_ID(namedProps->aulPropTag[namedPos]));
				}
				else
				{
					// Oops! Can't read this property, handle as unmapped field.
					m_Map.RemoveKey(key);
					m_arrayNotMapped.Add(field->m_strIICName);
					m_listNotMapped.AddTail(field->m_strIICName);
					delete field;
				}
				namedPos++;
			}
			else
			{
				output->aulPropTag[allPos++] = PROP_TAG(PT_TSTRING, field->m_PropId);
			}
		}

		output->cValues = allPos;
	}
	
	return output;
}


//*****************************************************************************
SPropTagArray *CImportMAPI::GetABEntryPropertyTags(IMAPIProp * pProps)
{
	SPropTagArray * output = NULL;
	MAPIField *field;
	POSITION pos;
	CString key;

	// Put together list of all properties we need
	int numProps = m_Map.GetCount() + 1;
	output = (SPropTagArray*)malloc(CbNewSPropTagArray(numProps));

	// Always get long term entry id
	output->aulPropTag[0] = PR_ENTRYID;

	// then add rest of configured fields.
	int idx = 1;
	for( pos = m_Map.GetStartPosition(); pos != NULL; )
	{
		m_Map.GetNextAssoc(pos, key, (void*&)field);

		if (field->m_ABPropId != 0)
			output->aulPropTag[idx++] = PROP_TAG(PT_TSTRING, field->m_ABPropId);
	}

	output->cValues = idx;
	
	return output;
}


//*****************************************************************************
// Retrieve fixed set of properties for distribution list.
SPropTagArray *CImportMAPI::GetDistListPropertyTags(IMAPIProp * pProps)
{
	SPropTagArray * output = NULL;

	SPropTagArray * namedProps;
	MAPINAMEID customNames[1];
	MAPINAMEID * customNamePtrs[1];
	int count = 0;

	customNames[count].lpguid = &MAPIGUIDs[2];
	customNames[count].ulKind = MNID_ID;
	customNames[count].Kind.lID = TAG_DISTLIST_ENTRIES;
	customNamePtrs[count] = &customNames[count];
	count++;

	HRESULT hRes = pProps->GetIDsFromNames(count, customNamePtrs, 0, &namedProps);
	if (SUCCEEDED(hRes))
	{
		int numProps = 3;
		// Put together list of all properties we need
		output = (SPropTagArray*)malloc(CbNewSPropTagArray(numProps));

		// Always get long term entry id
		output->aulPropTag[0] = PR_ENTRYID;
		output->aulPropTag[1] = PR_SUBJECT;
		output->aulPropTag[2] = PROP_TAG(PT_MV_BINARY, PROP_ID(namedProps->aulPropTag[0]));
		output->cValues = 3;
	}
	
	return output;
}


//*****************************************************************************
void CImportMAPI::FreePropertyTags()
{
	if (m_propTags)
	{
		free(m_propTags);
		m_propTags = NULL;
	}
	if (m_addrTags)
	{
		free(m_addrTags);
		m_addrTags = NULL;
	}
	if (m_dlistTags)
	{
		free(m_dlistTags);
		m_dlistTags = NULL;
	}
	m_arrayNotMapped.RemoveAll();
	m_listNotMapped.RemoveAll();
	m_Map.RemoveAll();
}


//*****************************************************************************
BOOL CImportMAPI::ConvertEntryID(SBinary * prop, CString & str, int offset)
{	
	static char chars[] = "0123456789ABCDEF";

	str.Empty();
	char bit[3] = { 0,0,0 };

	for (int i = offset; i< (int) prop->cb; i++)
	{
		bit[0] = chars[prop->lpb[i] >> 4];
		bit[1] = chars[prop->lpb[i] & 0xF];
		str += bit;
	}

	return TRUE;
}


//*****************************************************************************
/*
void dumpBinary(SBinaryArray * array)
{
	static char chars[] = "0123456789ABCDEF";
	CString str;

	for (int i=0; i<array->cValues; i++)
	{
		SBinary * bin = &array->lpbin[i];

		str.Empty();
		char bit[4] = { 0,0,' ', 0 };

		for (int i = 0; i< bin->cb; i++)
		{
			bit[0] = chars[bin->lpb[i] >> 4];
			bit[1] = chars[bin->lpb[i] & 0xF];
			str += bit;
		}

		TRACE(str + "\n");
	}
}
*/


//*****************************************************************************
int CImportMAPI::GetFoldersAndAddressBooks(CIICDoc *aDocument, 
                    CStringArray &aFolderArray, CStringArray &aAddressBookArray)
{
    CSingleLock singleLock(&m_lock);
    singleLock.Lock();  

	debugf("Loading MAPI folders and address books\n");

	aFolderArray.RemoveAll();
	aAddressBookArray.RemoveAll();

	mapi_Folders.clear();
	mapi_AddressBooks.clear();

	m_pDocument = aDocument;

	mapi_EnsureLibraries();
	if (mapi_Libraries.size()>0)
	{ 
		mapi_EnsureStores(mapi_Libraries.front().path, true);
	
		// Add folders
		mapi_EnsureFolders(NULL);
		if (mapi_logonCanceled)
	    {
		    singleLock.Unlock();
			debugf("User cancelled logon\n");
			return IDS_IIC_OUTLOOKFAILEDDEFAULT;
		}

        debugf( "About to add the following Folders " );
		for (list<mapi_TFolderInfo>::const_iterator i=mapi_Folders.begin(); i!=mapi_Folders.end(); i++)
        {
            debugf((char *) i->name.c_str());
			aFolderArray.Add(CUNICODEString(i->name.c_str()));
		}

		// Add address books.
		mapi_EnsureAddressBooks(NULL);
		if (mapi_logonCanceled)
	    {
		    singleLock.Unlock();
			debugf("User cancelled logon\n");
			return IDS_IIC_OUTLOOKFAILEDDEFAULT;
		}

        debugf( "\nAbout to add the following ABs " );
		for (list<mapi_TAddressBookInfo>::const_iterator i2=mapi_AddressBooks.begin(); i2!=mapi_AddressBooks.end(); i2++)
		{
            debugf((char *) i2->name.c_str());
			aAddressBookArray.Add(CUNICODEString(i2->name.c_str()));
		}
	}    

	singleLock.Unlock();
	
	debugf("\nFinished loading folders\n");
	return IDS_IIC_OUTLOOKIMPORTSUCCEEDED;
}


//*****************************************************************************
void CImportMAPI::CancelDownload()
{
	debugf("MAPI import stopped\n");
    doCancelOutlookDownload = TRUE;
}
