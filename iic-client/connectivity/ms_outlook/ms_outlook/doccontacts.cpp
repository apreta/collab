// DocContacts.cpp: implementation of the CDocContacts class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "iic.h"
#include "doccontacts.h"
#include "doccontactfield.h"
#include "iicdoc.h"
//#include "utils.h"
//#include "..\network\logger.h"

#include "resource.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif



//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDocContacts::CDocContacts()
{
    m_pDocument = NULL;

    m_FieldNames.Add(kContactUserId);
    m_FieldNames.Add(kContactUserName); 
    m_FieldNames.Add(kContactDirectory);
    m_FieldNames.Add(kContactTitle);
    m_FieldNames.Add(kContactFirst);
    m_FieldNames.Add(kContactMiddle);
    m_FieldNames.Add(kContactLast);
    m_FieldNames.Add(kContactSuffix);
    m_FieldNames.Add(kContactCompany);
    m_FieldNames.Add(kContactJobTitle);
    m_FieldNames.Add(kContactAddress1);
    m_FieldNames.Add(kContactAddress2);
    m_FieldNames.Add(kContactState);
    m_FieldNames.Add(kContactCountry);
    m_FieldNames.Add(kContactPostalCode);
    m_FieldNames.Add(kContactIICScreen);
/*    m_FieldNames.Add(kContactAimScreen); kContactAimScreen suppressed for now */
    m_FieldNames.Add(kContactEmail1);
    m_FieldNames.Add(kContactEmail2);
    m_FieldNames.Add(kContactEmail3);
    m_FieldNames.Add(kContactBusinessPhone);
    m_FieldNames.Add(kContactHomePhone);
    m_FieldNames.Add(kContactMobilePhone);
	m_FieldNames.Add(kContactOtherPhone);
	m_FieldNames.Add(kContactExtension); 
    m_FieldNames.Add(kContactUser1);
    m_FieldNames.Add(kContactUser2);
    m_FieldNames.Add(kContactUser3);
    m_FieldNames.Add(kContactUser4);
    m_FieldNames.Add(kContactUser5);
    m_FieldNames.Add(kContactUserType);
    m_FieldNames.Add(kContactPassword);
    m_FieldNames.Add(kContactServer);
    m_FieldNames.Add(kContactProfileId);
    m_FieldNames.Add(kContactPrivMaxPriority);
    m_FieldNames.Add(kContactPrivPriorityType);
    m_FieldNames.Add(kContactPrivEnabled);
    m_FieldNames.Add(kContactPrivDisabled);

    ResetFieldDownloadChanged();
}

CDocContacts::~CDocContacts()
{
    for (int loop=0; loop < m_Contacts.GetSize(); loop++)
    {
        delete (CDocContact*)m_Contacts[loop];
    }

    for (int loop2=0; loop2 < m_ContactFields.GetSize(); loop2++)
    {
        delete ((CDocContactField *)m_ContactFields.GetAt(loop2));
    }
}   

//////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// DumpContacts
//
void CDocContacts::DumpContacts()
{
    CSingleLock singleLock(&m_ContactCriticalSection);
    singleLock.Lock();  
    
    int contactCount = m_Contacts.GetSize();
    TRACE("SIZE=%d\n", contactCount);

	int loop;
    for (loop=0; loop < contactCount; loop++)
    {
        TRACE("==============\nloop=%d\n", loop);
        CDocContact *pContact = (CDocContact *) m_Contacts[loop];
        TRACE("id=%d\n", pContact->GetId());

        pContact->DumpDataValue(kContactUserId);
        pContact->DumpDataValue(kContactDirectory);
        pContact->DumpDataValue(kContactGroup);
        pContact->DumpDataValue(kContactTitle);
        pContact->DumpDataValue(kContactFirst);
        pContact->DumpDataValue(kContactMiddle);
        pContact->DumpDataValue(kContactLast);
        pContact->DumpDataValue(kContactSuffix);
        pContact->DumpDataValue(kContactCompany);
        pContact->DumpDataValue(kContactJobTitle);
        pContact->DumpDataValue(kContactAddress1);
        pContact->DumpDataValue(kContactAddress2);
        pContact->DumpDataValue(kContactState);
        pContact->DumpDataValue(kContactCountry);
        pContact->DumpDataValue(kContactPostalCode);
        pContact->DumpDataValue(kContactIICScreen);
        pContact->DumpDataValue(kContactAimScreen); 
        pContact->DumpDataValue(kContactEmail1);
        pContact->DumpDataValue(kContactEmail2);
        pContact->DumpDataValue(kContactEmail3);
        pContact->DumpDataValue(kContactBusinessPhone);
        pContact->DumpDataValue(kContactHomePhone);
        pContact->DumpDataValue(kContactMobilePhone);
        pContact->DumpDataValue(kContactOtherPhone);
        pContact->DumpDataValue(kContactExtension);
        pContact->DumpDataValue(kContactUser1);
        pContact->DumpDataValue(kContactUser2);
        pContact->DumpDataValue(kContactUser3);
        pContact->DumpDataValue(kContactUser4);
        pContact->DumpDataValue(kContactUser5);
    }

    TRACE("DONE == SIZE=%d, LOOP=%d\n", contactCount, loop);

    singleLock.Unlock();  
}

//////////////////////////////////////////////////////////////////////
// Initialize
//
void CDocContacts::Initialize(CIICDoc *aDocument)
{
    m_pDocument = aDocument;

    InitializeContactFields();

    ApplyDownloadFields();
}


//////////////////////////////////////////////////////////////////////
// InitializeContactFields
//
void CDocContacts::InitializeContactFields()
{

    CDocContactField *pContactField = NULL;

	CString strTitle;
//    strTitle = m_pDocument->m_SystemPreferences.GetBrandName();
    strTitle = m_pDocument->GetBrandName();

    //
    // kContactFieldPreferred
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldPreferred, pContactField);
    pContactField->m_ContactId               = kContactFieldPreferred;
    pContactField->m_ContactColumnId         = kContactPreferred;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_PREFCONTACT );

    //
    // kContactFieldFirstLast
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldFirstLast, pContactField);
    pContactField->m_ContactId               = kContactFieldFirstLast;
    pContactField->m_ContactColumnId         = kContactFirstLast;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_FIRSTLAST );

    //
    // kContactFieldLastFirst
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldLastFirst, pContactField);
    pContactField->m_ContactId               = kContactFieldLastFirst;
    pContactField->m_ContactColumnId         = kContactLastFirst;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_LASTFIRST );

    //
    // kContactFieldNone
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldNone, pContactField);
    pContactField->m_ContactId               = kContactFieldNone;
    pContactField->m_ContactColumnId         = kContactNone;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_BLANKIFNOTCONT );

    //
    // kContactFieldUserId
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldUserId, pContactField);
    pContactField->m_ContactId               = kContactFieldUserId;
    pContactField->m_ContactColumnId         = kContactUserId;
//    pContactField->m_ContactDisplayHeader    = "";
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = FALSE;
    pContactField->m_bSearchShowInResults    = FALSE;
    pContactField->m_bSearchAllowSearch      = FALSE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = FALSE;
    pContactField->m_bEditAllowAdmin         = FALSE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 
	pContactField->m_bAlwaysDownload         = TRUE;

    //
    // kContactFieldDirectory
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldDirectory, pContactField);
    pContactField->m_ContactId               = kContactFieldDirectory;
    pContactField->m_ContactColumnId         = kContactDirectory;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_ADDRBOOKS );
    pContactField->m_bIsDataField            = TRUE;    // TODO true?
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = FALSE;  // Is selected in a different control than the other search fields, so false
    pContactField->m_bSearchInitialSelect    = TRUE;
    pContactField->m_bEditAllowUser          = TRUE;   // TODO is this true?
    pContactField->m_bEditAllowAdmin         = TRUE;   // TODO is this true?
    pContactField->m_EditType                = CDocContactField::kEditTypeOptionList; 
	pContactField->m_bAlwaysDownload         = TRUE;
    // TODO the options for this field are set when the Address Books are fetched!

    //
    // kContactFieldGroup
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldGroup, pContactField);
    pContactField->m_ContactId               = kContactFieldGroup;
    pContactField->m_ContactColumnId         = kContactGroup;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_GROUP );
    pContactField->m_bIsDataField            = FALSE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = FALSE;
    pContactField->m_bSearchAllowSearch      = FALSE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = FALSE;
    pContactField->m_bEditAllowAdmin         = FALSE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 
    // Options for this field are handled through CDocGroups

    //
    // kContactFieldUserName
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldUserName, pContactField);
    pContactField->m_ContactId               = kContactFieldUserName;
    pContactField->m_ContactColumnId         = kContactUserName;
//    pContactField->m_ContactDisplayHeader    = "";
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = FALSE;
    pContactField->m_bSearchAllowSearch      = FALSE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = FALSE;
    pContactField->m_bEditAllowAdmin         = FALSE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 
	pContactField->m_bAlwaysDownload         = TRUE;

    //
    // kContactFieldTitle
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldTitle, pContactField);
    pContactField->m_ContactId               = kContactFieldTitle;
    pContactField->m_ContactColumnId         = kContactTitle;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_TITLE );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 

    //
    // kContactFieldFirst
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldFirst, pContactField);
    pContactField->m_ContactId               = kContactFieldFirst;
    pContactField->m_ContactColumnId         = kContactFirst;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_FIRSTNAME );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = TRUE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 

    //
    // kContactFieldMiddle
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldMiddle, pContactField);
    pContactField->m_ContactId               = kContactFieldMiddle;
    pContactField->m_ContactColumnId         = kContactMiddle;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_MIDDLENAME );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 

    //
    // kContactFieldLast
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldLast, pContactField);
    pContactField->m_ContactId               = kContactFieldLast;
    pContactField->m_ContactColumnId         = kContactLast;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_LASTNAME );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = TRUE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 

    //
    // kContactFieldSuffix
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldSuffix, pContactField);
    pContactField->m_ContactId               = kContactFieldSuffix;
    pContactField->m_ContactColumnId         = kContactSuffix;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_SUFFIX );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 

    //
    // kContactFieldCompany
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldCompany, pContactField);
    pContactField->m_ContactId               = kContactFieldCompany;
    pContactField->m_ContactColumnId         = kContactCompany;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_COMPANY );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 
	pContactField->m_bAlwaysDownload         = TRUE;

    //
    // kContactFieldJobTitle
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldJobTitle, pContactField);
    pContactField->m_ContactId               = kContactFieldJobTitle;
    pContactField->m_ContactColumnId         = kContactJobTitle;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_JOBTITLE );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 

    //
    // kContactFieldAddress1
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldAddress1, pContactField);
    pContactField->m_ContactId               = kContactFieldAddress1;
    pContactField->m_ContactColumnId         = kContactAddress1;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_ADDR1 );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 

    //
    // kContactFieldAddress2
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldAddress2, pContactField);
    pContactField->m_ContactId               = kContactFieldAddress2;
    pContactField->m_ContactColumnId         = kContactAddress2;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_ADDR2 );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 

    //
    // kContactFieldState
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldState, pContactField);
    pContactField->m_ContactId               = kContactFieldState;
    pContactField->m_ContactColumnId         = kContactState;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_STATE );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 

    //
    // kContactFieldCountry
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldCountry, pContactField);
    pContactField->m_ContactId               = kContactFieldCountry;
    pContactField->m_ContactColumnId         = kContactCountry;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_COUNTRY );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 

    //
    // kContactFieldPostalCode
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldPostalCode, pContactField);
    pContactField->m_ContactId               = kContactFieldPostalCode;
    pContactField->m_ContactColumnId         = kContactPostalCode;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_POSTCODE );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 

    //
    // kContactFieldIICScreen
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldIICScreen, pContactField);
    pContactField->m_ContactId               = kContactFieldIICScreen;
    pContactField->m_ContactColumnId         = kContactIICScreen;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_SCRNNAME );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 
	pContactField->m_bAlwaysDownload         = TRUE;

    //
    // kContactFieldAimScreen
    //
    /* Disabled for now
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldAimScreen, pContactField);
    pContactField->m_ContactId               = kContactFieldAimScreen;
    pContactField->m_ContactColumnId         = kContactAIMScreen;
    pContactField->m_ContactDisplayHeader    = "AIM Screen Name";
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 
    */

    //
    // kContactFieldEmail1
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldEmail1, pContactField);
    pContactField->m_ContactId               = kContactFieldEmail1;
    pContactField->m_ContactColumnId         = kContactEmail1;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_EMAIL );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 
	pContactField->m_bAlwaysDownload         = TRUE;

    //
    // kContactFieldEmail2
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldEmail2, pContactField);
    pContactField->m_ContactId               = kContactFieldEmail2;
    pContactField->m_ContactColumnId         = kContactEmail2;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_EMAIL2 );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 
	pContactField->m_bAlwaysDownload         = TRUE;

    //
    // kContactFieldEmail3
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldEmail3, pContactField);
    pContactField->m_ContactId               = kContactFieldEmail3;
    pContactField->m_ContactColumnId         = kContactEmail3;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_EMAIL3 );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 
	pContactField->m_bAlwaysDownload         = TRUE;

    //
    // kContactFieldBusinessPhone
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldBusinessPhone, pContactField);
    pContactField->m_ContactId               = kContactFieldBusinessPhone;
    pContactField->m_ContactColumnId         = kContactBusinessPhone;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_BUSPHONE );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 
	pContactField->m_bAlwaysDownload         = TRUE;

    //
    // kContactFieldHomePhone
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldHomePhone, pContactField);
    pContactField->m_ContactId               = kContactFieldHomePhone;
    pContactField->m_ContactColumnId         = kContactHomePhone;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_HOMEPHONE );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 
	pContactField->m_bAlwaysDownload         = TRUE;

    //
    // kContactFieldMobilePhone
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldMobilePhone, pContactField);
    pContactField->m_ContactId               = kContactFieldMobilePhone;
    pContactField->m_ContactColumnId         = kContactMobilePhone;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_MOBILEPHONE );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 
	pContactField->m_bAlwaysDownload         = TRUE;

    //
    // kContactFieldOtherPhone
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldOtherPhone, pContactField);
    pContactField->m_ContactId               = kContactFieldOtherPhone;
    pContactField->m_ContactColumnId         = kContactOtherPhone;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_OTHERPHONE );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 
	pContactField->m_bAlwaysDownload         = TRUE;

	//
    // kContactFieldExtension
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldExtension, pContactField);
    pContactField->m_ContactId               = kContactFieldExtension;
    pContactField->m_ContactColumnId         = kContactExtension;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_EXTENSION );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 
	pContactField->m_bAlwaysDownload         = TRUE;

    //
    // kContactFieldUser1
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldUser1, pContactField);
    pContactField->m_ContactId               = kContactFieldUser1;
    pContactField->m_ContactColumnId         = kContactUser1;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_MSNSCREEN );  // User 1
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 
	pContactField->m_bAlwaysDownload         = TRUE;

    //
    // kContactFieldUser2
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldUser2, pContactField);
    pContactField->m_ContactId               = kContactFieldUser2;
    pContactField->m_ContactColumnId         = kContactUser2;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_YAHOODISPNAME );  // User 2
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 
	pContactField->m_bAlwaysDownload         = TRUE;

    //
    // kContactFieldUser3
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldUser3, pContactField);
    pContactField->m_ContactId               = kContactFieldUser3;
    pContactField->m_ContactColumnId         = kContactUser3;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_AIMSCRN );  // User 3
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 
	pContactField->m_bAlwaysDownload         = TRUE;

    //
    // kContactFieldUser4
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldUser4, pContactField);
    pContactField->m_ContactId               = kContactFieldUser4;
    pContactField->m_ContactColumnId         = kContactUser4;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_USER1 );  // User 4
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 

    //
    // kContactFieldUser5
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldUser5, pContactField);
    pContactField->m_ContactId               = kContactFieldUser5;
    pContactField->m_ContactColumnId         = kContactUser5;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_USER2 );  // User 5
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = TRUE;
    pContactField->m_bSearchAllowSearch      = TRUE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = TRUE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 

    //
    // kContactFieldDefPhone
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldDefPhone, pContactField);
    pContactField->m_ContactId               = kContactFieldDefPhone;
    pContactField->m_ContactColumnId         = kContactDefPhone;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_DEFPHONE );
    pContactField->m_bIsDataField            = FALSE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = FALSE;
    pContactField->m_bContactCopyOnUpdate    = FALSE;
    pContactField->m_bSearchShowInResults    = FALSE;
    pContactField->m_bSearchAllowSearch      = FALSE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = FALSE;
    pContactField->m_bEditAllowAdmin         = FALSE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 
	pContactField->m_bAlwaysDownload         = TRUE;

    //
    // kContactFieldDefEmail
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldDefEmail, pContactField);
    pContactField->m_ContactId               = kContactFieldDefEmail;
    pContactField->m_ContactColumnId         = kContactDefEmail;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_DEFEMAIL );
    pContactField->m_bIsDataField            = FALSE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = FALSE;
    pContactField->m_bContactCopyOnUpdate    = FALSE;
    pContactField->m_bSearchShowInResults    = FALSE;
    pContactField->m_bSearchAllowSearch      = FALSE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = FALSE;
    pContactField->m_bEditAllowAdmin         = FALSE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 
	pContactField->m_bAlwaysDownload         = TRUE;

    //
    // kContactFieldUserType
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldUserType, pContactField);
    pContactField->m_ContactId               = kContactFieldUserType;
    pContactField->m_ContactColumnId         = kContactUserType;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_USERTYPE );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = FALSE;
    pContactField->m_bSearchAllowSearch      = FALSE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = FALSE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeOptionList; 
	pContactField->m_bAlwaysDownload         = TRUE;

    //
    // kContactFieldPassword
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldPassword, pContactField);
    pContactField->m_ContactId               = kContactFieldPassword;
    pContactField->m_ContactColumnId         = kContactPassword;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_PASSWORD );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = TRUE;
    pContactField->m_bContactIsDownloadable  = FALSE;  // Doesn't actually exist on the database record
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = FALSE;
    pContactField->m_bSearchAllowSearch      = FALSE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = FALSE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 

    //
    // kContactFieldServer
    //
    // TODO Check Edit?
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldServer, pContactField);
    pContactField->m_ContactId               = kContactFieldServer;
    pContactField->m_ContactColumnId         = kContactServer;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_SERVER );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = FALSE;
    pContactField->m_bSearchAllowSearch      = FALSE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = FALSE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 

    //
    // kContactFieldProfileId
    //
    pContactField = new CDocContactField();
    m_ContactFields.SetAtGrow(kContactFieldProfileId, pContactField);
    pContactField->m_ContactId               = kContactFieldProfileId;
    pContactField->m_ContactColumnId         = kContactProfileId;
//    pContactField->m_ContactDisplayHeader.LoadString( IDS_CONTDISPHDR_POLICY );
    pContactField->m_bIsDataField            = TRUE;
    pContactField->m_bContactEncryptData     = FALSE;
    pContactField->m_bContactIsDownloadable  = TRUE;
    pContactField->m_bContactCopyOnUpdate    = TRUE;
    pContactField->m_bSearchShowInResults    = FALSE;
    pContactField->m_bSearchAllowSearch      = FALSE;
    pContactField->m_bSearchInitialSelect    = FALSE;
    pContactField->m_bEditAllowUser          = FALSE;
    pContactField->m_bEditAllowAdmin         = TRUE;
    pContactField->m_EditType                = CDocContactField::kEditTypeString; 

}


//////////////////////////////////////////////////////////////////////
// InitializeContacts
//
// For now, loads up test data
//
void CDocContacts::InitializeContacts()
{
    /*
    Not any more
    */
}


/////////////////////////////////////////////////////////////////////////////
// ApplyDownloadFields
//
// Called after the set of fields to download has been changed
// by the user.  Figures out what fields need to fetched from
// the server.
//
void CDocContacts::ApplyDownloadFields()
{
    CStringList listDownloadFields; // To aid in lookup
    int loop;
    CString strField;
    BOOL bNeedPreferred = FALSE;

    CSingleLock singleLock(&m_ContactCriticalSection);
    singleLock.Lock();  

    m_FieldsToDownload.RemoveAll();

	// Set up fields that are always downloaded
    for (loop=0; loop < m_ContactFields.GetSize(); loop++)
    {
        CDocContactField * pF = (CDocContactField *)m_ContactFields.GetAt(loop);
		if (pF->m_bAlwaysDownload)
		{
			listDownloadFields.AddTail(pF->m_ContactColumnId);
		    m_FieldsToDownload.Add(pF->m_ContactColumnId);
		}
    }

    for (loop=0; loop < m_FieldsToDownload.GetSize(); loop++)
    {
        CString strTest = m_FieldsToDownload.GetAt(loop);
    }

    singleLock.Unlock();  
}

/////////////////////////////////////////////////////////////////////////////
// AddDownloadField
//
// Private helper function for ApplyDownloadFields
//
// If the field is the Preferred Contact Name field, it is 
// not added to the download list and this function returns
// TRUE
//
BOOL CDocContacts::AddDownloadField(CStringList &aDownloadList, const CString &aField)
{
    BOOL bResult = FALSE;

    // If we haven't already downloaded this field...
    if (m_DownloadedFields.Find(aField) == NULL)
    {
        // If we don't already have this marked for download...
        if (aDownloadList.Find(aField) == NULL)
        {
            // If its the Preferred Contact Name, we handle it specially
            if (aField == kContactPreferred)
            {
                bResult = TRUE;
            }
            else
            {
                // Mark this field for download...
                aDownloadList.AddTail(aField);
                m_FieldsToDownload.Add(aField);
            }
        }
    }

    return bResult;
}

/////////////////////////////////////////////////////////////////////////////
// SetDownloadedFields
//
// Called when download is completed.  Marks the fields as
// having been downloaded.
//
void CDocContacts::SetDownloadedFields()
{
    CSingleLock singleLock(&m_ContactCriticalSection);
    singleLock.Lock();  

    int fieldcount = m_FieldsToDownload.GetSize();

    for (int loop=0; loop < fieldcount; loop++)
    {
        m_DownloadedFields.AddTail(m_FieldsToDownload.GetAt(loop));
    }

    singleLock.Unlock();  
}

/////////////////////////////////////////////////////////////////////////////
// IsFieldDownloadChanged
//
BOOL CDocContacts::IsFieldDownloadChanged()
{
    return m_IsFieldDownloadChanged;
}

/////////////////////////////////////////////////////////////////////////////
// ResetFieldDownloadChanged
//
void CDocContacts::ResetFieldDownloadChanged()
{
    m_IsFieldDownloadChanged = FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// GetContact
//
// Returns a CDocContact pointer for the given contact ID
//
CDocContact *CDocContacts::GetContact(int aContactId)
{
    CDocContact *pContact = NULL;
    int contactcount = m_Contacts.GetSize();

    CSingleLock singleLock(&m_ContactCriticalSection);
    singleLock.Lock();  

    if (contactcount > 0 && aContactId >= 0 && aContactId < contactcount)
    {
         pContact = (CDocContact *) m_Contacts[aContactId];
    }

    singleLock.Unlock();  

    return pContact;
}

/////////////////////////////////////////////////////////////////////////////
// GetContactCount
//
// Warning this gives the total contact count, including deleted contacts
//
int CDocContacts::GetContactCount()
{
    int result = 0;

    CSingleLock singleLock(&m_ContactCriticalSection);
    singleLock.Lock();  

    result = m_Contacts.GetSize();

    singleLock.Unlock();  

    return result;
}

/////////////////////////////////////////////////////////////////////////////
// GetContactFromUserId
//
// Returns a CDocContact pointer for the given UserId
//
CDocContact *CDocContacts::GetContactFromUserId(const CString &aUserId)
{
    CSingleLock singleLock(&m_ContactCriticalSection);
    singleLock.Lock();  

    CDocContact *pContact = NULL;

    int index = -1;
    if (!m_MapUserIds.Lookup(aUserId, index))
    {
        TRACE("Can't find UserId map!\n");
    }
    else
    {
        pContact = (CDocContact *) m_Contacts[index];
    }

    singleLock.Unlock();  

    return pContact;
}

/////////////////////////////////////////////////////////////////////////////
// GetContactFromIICScreenName
//
//  TODO: Not used any more?
//
CDocContact *CDocContacts::GetContactFromIICScreenName(const CString &aIICScreenName)
{
    CDocContact *result = NULL;
    int count = GetContactCount();
    CString strIICScreenName;

    for (int loop=0; (result == NULL) && (loop < count); loop++)
    {
        CDocContact *pContact = GetContact(loop);

        if (NULL != pContact)
        {
            if (pContact->GetData(kContactIICScreen, strIICScreenName))
            {
                if (!strIICScreenName.IsEmpty() && (strIICScreenName == aIICScreenName))
                {
                    result = pContact;
                }
            }
        }
    }

    return result;
}

/////////////////////////////////////////////////////////////////////////////
// ClearAll
//
void CDocContacts::ClearAll()
{
    CSingleLock singleLock(&m_ContactCriticalSection);
    singleLock.Lock();  

    ResetFieldDownloadChanged();

    CDocContact *pContact = NULL;

    for (int loop=0; loop < m_Contacts.GetSize(); loop++)
    {
        (CDocContact *)pContact = (CDocContact *)m_Contacts[loop];
        m_Contacts[loop] = NULL;
        delete pContact;
    }

    m_Contacts.RemoveAll();
    m_MapUserIds.RemoveAll();
    m_DownloadedFields.RemoveAll();

    singleLock.Unlock();  

    this->ApplyDownloadFields();
}

/////////////////////////////////////////////////////////////////////////////
// GetGroupBy
//
void CDocContacts::GetGroupBy(int aGroupById, CString &aGroupBy)
{
    CSingleLock singleLock(&m_ContactCriticalSection);
    singleLock.Lock();  

    aGroupBy = m_GroupBy[aGroupById];

    singleLock.Unlock();
}

//////////////////////////////////////////////////////////////////////
// FindContactByUserName
//
// Tries to find a contacts in the local contact list using the 
// given username.  If a user with that username cannot be found,
// it will parse out the IIC screenname and attempt to find the 
// contact that way.  
//
// Returns NULL if the no matching contact is found.
//
CDocContact *CDocContacts::FindContactByUserName(const CString &aUserName)
{
    CDocContact *pContact = NULL;

    int  delimiter_position = aUserName.Find(_T("@"));   // Search for UserName delimiter "@"
    bool bIsUserName = (delimiter_position != -1 );  // True if we found the UserName delimiter

    // Search for a match on username...
    //
    // To find a match, aIICScreen will contain a string such as "screen@iic.homedns.org
    // which we will try to match to a UserName on the contact record.
    //
    if (bIsUserName)
    {
        pContact = m_pDocument->m_Contacts.FindFirstContact(FALSE, kContactUserName, aUserName);
    }

    // If we STILL didn't find a contact and the CAB download is disabled by policy,
    // then we should look for a normal contact with a match on just the IICScreenName
    //
    // To find a match, aIICScreen will contain a string such as "screen@iic.homedns.org".
    // We will parse out the "screen" portion and look for a match.
    //
    // If for some reason the '@' delimiter is not found, just use the whole string...
    //
    if (NULL == pContact)
    {
        CString strScreen;

        if (bIsUserName)
        {
            strScreen = aUserName.Left(delimiter_position);
        }
        else
        {
            strScreen = aUserName;
        }

        if (!strScreen.IsEmpty())
        {
            pContact = m_pDocument->m_Contacts.FindFirstContact(FALSE, kContactIICScreen, strScreen);
        }
    }

    return pContact;
}

//////////////////////////////////////////////////////////////////////
// FindFirstContact
//
// Returns NULL if no User with matching search is found.
//
CDocContact *CDocContacts::FindFirstContact(BOOL aUsersOnly, const CString &aSearchField, const CString &aSearchValue)
{
    BOOL bFound = FALSE;
    CDocContact *pContact = NULL;
    CString strContactValue;
    int count = GetContactCount();

    for (int loop = 0; loop < count && !bFound; loop++)
    {
        pContact = GetContact(loop);

        if ( /*!pContact->IsDeleted() && */ (!aUsersOnly /*|| pContact->IsInNetwork()*/))
        {
            if (pContact->GetData(aSearchField, strContactValue) &&
                aSearchValue == strContactValue)
            {
                bFound = TRUE;
            }
        }
    }

    if (!bFound)
    {
        pContact = NULL;
    }

    return pContact;
}

//////////////////////////////////////////////////////////////////////
// FindAllContacts
//
BOOL CDocContacts::FindAllContacts(BOOL aUsersOnly, const CString &aSearchField, const CString &aSearchValue, CPtrList &aSearchResults)
{
    CDocContact *pContact = NULL;
    CString strContactValue;
    int count = GetContactCount();

    for (int loop = 0; loop < count; loop++)
    {
        pContact = GetContact(loop);

        if (/*!pContact->IsDeleted() &&*/ (!aUsersOnly /*|| pContact->IsInNetwork()*/))
        {
            if (pContact->GetData(aSearchField, strContactValue) &&
                aSearchValue == strContactValue)
            {
                aSearchResults.AddTail(pContact);
            }
        }
    }

    return (!aSearchResults.IsEmpty());
}


