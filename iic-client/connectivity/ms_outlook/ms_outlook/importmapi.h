// ImportMAPI.h: interface for the CImportMAPI class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IMPORTMAPI_H__29AD1AA7_8E2C_4952_B4FD_10C6BC149E5A__INCLUDED_)
#define AFX_IMPORTMAPI_H__29AD1AA7_8E2C_4952_B4FD_10C6BC149E5A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "iicdoc.h"
#include "csv.h"

#include <mapix.h>

//class CIICDoc;
class mapi_TEntryid;


static BOOL doCancelOutlookDownload;


class CImportMAPI : public CObject  
{
	CIICDoc * m_pDocument;
	CMapStringToPtr m_Map;
    CStringArray m_arrayNotMapped;
    CStringList  m_listNotMapped;
	SPropTagArray * m_propTags;
	SPropTagArray * m_dlistTags;
	SPropTagArray * m_addrTags;

    CMapStringToString m_mapEntryId;
	int m_groupId;

    // Key is EntryId of a DistList
    // Value is the generated GroupId for that DistList
    // Allows lookup of a Group using the related DistList EntryId
    CMapStringToString m_mapEntryIdsToGroupIds;

    // Key is an EntryId of a DistList
    // Value is an CStringArray* containing EntryIds for child DistLists 
    // Allows us to track Groups contained in other Groups
    CMapStringToPtr m_mapGroupsInGroups;

	void AddGroupInGroup(CString &aGroupEntryId, CString &aGroupMemberId);
    void ResolveGroupsInGroups(CMapStringToPtr &aGroupsInGroups);
    void CleanupMaps();

	BOOL LoadMapFromCsv(CIICDoc *aDocument, const CString &aCsvFile);
	SPropTagArray *GetContactPropertyTags(IMAPIProp * pProps);
	SPropTagArray *GetDistListPropertyTags(IMAPIProp * pProps);
	SPropTagArray *GetABEntryPropertyTags(IMAPIProp * pProps);
	void FreePropertyTags();
	BOOL ConvertEntryID(SBinary * prop, CString & str, int offset=0);

	// Handle items from contacts folder
	BOOL AddContact(IMAPIFolder * pFolder, IMessage * pMsg,
				const CMapStringToString &aDefaultValues, CPtrArray &aContactArray);
	BOOL AddDistList(IMAPIFolder * pFolder, IMessage * pMsg,
				const CMapStringToString &aDefaultValues, CPtrArray &aContactArray,
				CMapStringToString &aGroups, CMapStringToPtr &aMembers);

	// Handle items from address book
	BOOL AddABEntry(IMailUser * pUser, const CMapStringToString &aDefaultValues, 
				CPtrArray &aContactArray);
	BOOL AddABDistList(IDistList * pDist, CString &aGroupName, 
				const CMapStringToString &aDefaultValues, CPtrArray &aContactArray,
				CMapStringToString &aGroups, CMapStringToPtr &aMembers);
	BOOL AddABDistListMember(IMailUser * pDist, CString &aGroupid,
				const CMapStringToString &aDefaultValues, CPtrArray &aContactArray,
				CMapStringToString &aGroups, CMapStringToPtr &aMembers);

	BOOL m_MAPIInit;
	CCriticalSection m_lock;

public:
	CImportMAPI();
	virtual ~CImportMAPI();

    BOOL Initialize();
	BOOL Shutdown();
	void Stop();

    int GetContacts(CIICDoc *aDocument, 
                    const CMapStringToString &aDefaultValues, 
                    CPtrArray &aContactArray,
                    BOOL aUseDefault,
                    BOOL aGetDistributionLists,
                    CMapStringToString &aGroups,
                    CMapStringToPtr &aMembers,
                    CStringArray& aFolderArray,
                    CStringArray& aAddressBooksArray,
                    CMapStringToPtr &aGroupsInGroups
					);
	
	int GetContacts(IMAPIFolder *pFolder, IABContainer *pBook,
                             const CMapStringToString &aDefaultValues, 
                             CPtrArray &aContactArray,
                             BOOL aUseDefault,
                             BOOL aGetDistributionLists,
                             CMapStringToString &aGroups,
                             CMapStringToPtr &aMembers);

    int GetFoldersAndAddressBooks(CIICDoc *aDocument, 
                    CStringArray& aFolderArray,
                    CStringArray& aAddressBooksArray
                    );

    static void CancelDownload();
};

#endif // !defined(AFX_IMPORTMAPI_H__29AD1AA7_8E2C_4952_B4FD_10C6BC149E5A__INCLUDED_)
