#if !defined(AFX_DIALOGCONFIGURESUBCRIPTION_H__151E61BA_8A9B_42ED_AF15_061F3070B09B__INCLUDED_)
#define AFX_DIALOGCONFIGURESUBCRIPTION_H__151E61BA_8A9B_42ED_AF15_061F3070B09B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DialogConfigureSubcription.h : header file
//


//class CIICDoc;


/////////////////////////////////////////////////////////////////////////////
// CDocOutlookFoldersInfo

class CDocOutlookFoldersInfo : public CObject  
{
    public:
        CDocOutlookFoldersInfo();
        virtual ~CDocOutlookFoldersInfo();

    // Message will be posted to this window handle
    // when the contact import process has completed.
    // It must remain valid
    //
//    HWND m_ResultListener;

        CStringArray m_Folders;
        CStringArray m_AddressBooks;
};


/////////////////////////////////////////////////////////////////////////////
// CDialogConfigureSubcription dialog

class CDialogConfigureSubcription : public CDialog
{
//	CIICDoc* m_pDocument;

// Construction
public:
	CDialogConfigureSubcription( CWnd* pParent = NULL );   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDialogConfigureSubcription)
	enum { IDD = IDD_DIALOG_SUB_CONFIGURE };
	CCheckListBox	m_ListFolders;
	CCheckListBox	m_ListAddrBooks;
	//}}AFX_DATA

	CStringArray m_Folders;
	CStringArray m_AddressBooks;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDialogConfigureSubcription)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDialogConfigureSubcription)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
    LRESULT     OutlookFoldersLookupCompleted( CDocOutlookFoldersInfo *pFoldersInfo );
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALOGCONFIGURESUBCRIPTION_H__151E61BA_8A9B_42ED_AF15_061F3070B09B__INCLUDED_)
