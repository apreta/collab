// DocGroup.h: interface for the CDocGroup class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DOCGROUP_H__590C0AC7_320B_4992_A994_F0FC64EFE7CF__INCLUDED_)
#define AFX_DOCGROUP_H__590C0AC7_320B_4992_A994_F0FC64EFE7CF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DocDirectory.h"

/*
template<> inline UINT AFXAPI HashKey<CString> (CString strKey)
{
    LPCTSTR key = strKey;
    UINT nHash = 0;
    while (*key)
        nHash = (nHash<<5) + nHash + *key++;
    return nHash;
}
*/
// Forward reference for our main document object
class CIICDoc;
class CDocSearch;

static int kGroupTypeAll     = 1;
static int kGroupTypeBuddies = 2;
static int kGroupTypeOther   = 3;

class CDocGroup : public CObject  
{
	CDocGroup();

    CCriticalSection m_CriticalSection;

    CIICDoc *m_pDocument;
    CDocDirectory *m_pDirectory;

    CString  m_GroupId;
    CString  m_Name;
    int      m_Type;    // 1 = All, 2 = Other 

	CStringArray m_Members;
    CStringArray m_ChildGroups;

    CDocSearch *m_pSearch;  // Set if the group corresponds to a contact search

public:
    CDocGroup(CIICDoc *aDocument, CDocDirectory *pDirectory, const CString &aGroupId, const CString &aName, int aType);
	CDocGroup(CIICDoc *aDocument, CDocDirectory *pDirectory, const CString &aName);
	virtual ~CDocGroup();

    void GetGroupId(CString &aGroupId);
    CDocDirectory *GetDirectory();
    void GetDirectoryId(CString &aDirectoryId);
    void GetName(CString &aName);
    void SetName(const CString &aName);
    void GetFullyQualifiedName(CString &aName);
    int GetType();

    BOOL IsReadOnly();
    BOOL IsAllGroup();
    BOOL IsSearchGroup();
    BOOL IsBuddyGroup();
    BOOL IsInDirectory(const CString &aDirectoryId);
    BOOL IsUserInGroup(const CString &aUserId);
    BOOL IsGlobalAddressBook();

    void AddUser(const CString &aUserId);
    void RemoveUser(const CString &aUserId);

	int GetUserCount()	{ return m_Members.GetCount(); }
	CString GetUser(int i)  { return  m_Members.GetAt(i); }

    void AddChildGroups(CStringArray &aChildGroupsArray);
    BOOL HasChildGroups();
    void GetChildGroups(CStringArray &aChildGroupsArray);
};

#endif // !defined(AFX_DOCGROUP_H__590C0AC7_320B_4992_A994_F0FC64EFE7CF__INCLUDED_)
