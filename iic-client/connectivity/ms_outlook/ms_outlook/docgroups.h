// DocGroups.h: interface for the CDocGroups class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DOCGROUPS_H__EED50DD0_3258_4053_95EA_A69BBCAB84FE__INCLUDED_)
#define AFX_DOCGROUPS_H__EED50DD0_3258_4053_95EA_A69BBCAB84FE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DocGroup.h"
#include "DocDirectory.h"

// Forward reference for our main document object
class CIICDoc;

// Statics
static char *kGroupAll = "All";

// Class CDocGroups
class CDocGroups : public CObject  
{
public:
    // static for Outlook groupid
    static TCHAR *kOutlookGroup;

protected:
    CCriticalSection m_CriticalSection;

    CIICDoc *m_pDocument;

    CMapStringToPtr  m_Groups;
    CMapStringToPtr  m_UserGroups;    // maintains list of all groups a user is in
    CPtrArray        m_DeletedGroups; 

public:
	CDocGroups();
	virtual ~CDocGroups();

    void Initialize(CIICDoc *aDocument);

    void ClearAll();
    void PrepareForRefresh();

    void RemoveGroup(const CString &aGroupId);
    void RemoveUserFromGroup(const CString &aGroupId, const CString &aUserId);
    void RemoveUserFromAllGroups(const CString &aUserId);

    int  GetUserGroupCount(const CString &aUserId);
    BOOL GetUserGroupNameAt(const CString &aUserId, int aIndex, CString &aGroupName);
    CDocGroup *GetUserGroupAt(const CString &aUserId, int aIndex);
    BOOL GetAllGroupId(const CString &aDirectoryId, CString &aGroupId);
    void GetGroupsInDirectory(CPtrArray &aGroupArray, const CString &aDirectoryId);

    CDocGroup *GetGroupFromId(const CString &aGroupId);
    BOOL GetGroupNameFromId(const CString &aGroupId, CString &aGroupName);

};

#endif // !defined(AFX_DOCGROUPS_H__EED50DD0_3258_4053_95EA_A69BBCAB84FE__INCLUDED_)
