// DocDirectories.h: interface for the CDocDirectories class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DOCDIRECTORIES_H__B1427CB8_690E_46F8_BBD7_1FF718B39F3B__INCLUDED_)
#define AFX_DOCDIRECTORIES_H__B1427CB8_690E_46F8_BBD7_1FF718B39F3B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <afxtempl.h>

#include "DocDirectory.h"

// Forward reference for our main document object
class CIICDoc;

// template needed for CMap
template<> inline UINT AFXAPI HashKey<CString> (CString strKey)
{
    LPCTSTR key = strKey;
    UINT nHash = 0;
    while (*key)
        nHash = (nHash<<5) + nHash + *key++;
    return nHash;
}

//
// Constants to identify different directory types
//
// These are actual directory types
static char *kDirectoryTypeActiveDirectory  = "active";
static char *kDirectoryTypeOutlook          = "outlook";
static char *kDirectoryTypeIIC              = "iic";
static char *kDirectoryTypeServiceImbedded  = "imbedded";
static char *kDirectoryTypeServiceMessenger = "messenger";
static char *kDirectoryTypeServiceAOL       = "aol";
static char *kDirectoryTypeServiceYahoo     = "yahoo";
static char *kDirectoryTypeSearch           = "search";

// These are special directory type that have meaning
// in the context of "hunting" for user information
static char *kDirectoryTypeNone             = "none";
static char *kDirectoryTypeDefault          = "default";
static char *kDirectoryTypeIICInNetwork     = "iicnetwork";

static const CString kDirectoryPrefix = _T("Dir.");
static const int     kDirectoryPrefixLen = kDirectoryPrefix.GetLength();

//
// class CDocDirectories 
//
class CDocDirectories : public CObject  
{ 
public:
    // static to indicate that m_OutlookIndex has not been set
    static int kDirectoryNone;

    // static for Outlook directoryid
    static TCHAR *kOutlookDirectory;

protected:
    // Lock object to protect multithreaded access
    CCriticalSection m_DirectoriesCriticalSection;

    // Reference to the document object
    CIICDoc *m_pDocument;

    // Hold the pointers to the CDirectory objects.  We 
    // use an array of objects because we care about the 
    // order of the directories.
    CPtrArray m_Directories;  

    // Map to allow quick lookup of a CDirectory by name.
    // Returns index into m_Directories array.
    CMap <CString, CString, int, int> m_MapNameToIndex;

    CArray <char *, char *> m_SubscribedDirectories;
    CArray <char *, char *> m_AllServerDirectories;   // Return all known server directories
    CArray <char *, char *> m_ServerDirectories;      // Excludes Global if community download is disabled

    // Index of the Outlook directory (if there 
    // is one, so we can quickly get to it...
    int m_OutlookIndex;
    int m_MessengerIndex;
    int m_YahooIndex;
    int m_AOLIndex;
    int m_SearchIndex;

public:
	CDocDirectories();
	virtual ~CDocDirectories();

    void Initialize(CIICDoc *aDocument);
    void InitializeSearchDirectory();

    void Add(const CString &aId, const CString &aName, BOOL aIsSubscribed, int aType);
    void ClearAll();
    void PrepareForRefresh();

    int GetDirCount();

    CDocDirectory *GetDirFromIndex(int aIndex);
    CDocDirectory *GetDirFromId(const CString &aId);
    CDocDirectory *GetDirFromGroupById(const CString &aGroupById);
    CDocDirectory *GetDirFromName(const CString &aName);

	void GetNameFromIndex (int aIndex, CString &aName);
    void GetNameFromId (const CString &aId, CString &aName);
    void GetIdFromName (const CString &aName, CString &aId);
    CDocDirectory *GetDirOfType(enum DirectoryType aDirectoryType);

    BOOL m_IsSubscriptionChanged;
    BOOL IsOutlookSubscribed();
    BOOL IsMessengerSubscribed();
    BOOL IsYahooSubscribed();
    BOOL IsAOLSubscribed();
};

#endif // !defined(AFX_DOCDIRECTORIES_H__B1427CB8_690E_46F8_BBD7_1FF718B39F3B__INCLUDED_)
