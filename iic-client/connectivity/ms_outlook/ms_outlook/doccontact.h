// DocContact.h: interface for the CDocContact class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DOCCONTACT_H__F5B12C58_7547_4C3E_8C56_9D1F87DBEAFB__INCLUDED_)
#define AFX_DOCCONTACT_H__F5B12C58_7547_4C3E_8C56_9D1F87DBEAFB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "docdirectories.h"

// Forward reference for our main document object
class CIICDoc;

#define kPrivSystemDefault -1
#define kPrivEnabled        0
#define kPrivDisabled       1


const static int kPresenceUnavailable = 0;
const static int kPresenceAvailable = 1;
const static int kPresenceAway = 2;



enum ServiceType {
	ServiceTypeNone      = 0,
	ServiceTypeIIC       = 1,
	ServiceTypeImbedded  = 2,
	ServiceTypeMessenger = 4,
	ServiceTypeAOL       = 8,
	ServiceTypeYahoo     = 16
};

class CDocContact : public CObject  
{
public:
	CDocContact(CIICDoc *aDocument);
	CDocContact(CIICDoc *aDocument, CDocContact *aContact);
	virtual ~CDocContact();

    void DumpDataValue(const CString &aIICName);

    void CopyFields(CDocContact* aContact);
    void SetId(int aId);
    int  GetId();

    void SetData(const CString &aIICName, const CString &aValue);
    void SetDataRaw(const CString &aIICName, const CString &aValue);
    BOOL GetData(const CString &aIICName, CString &aValue);
    BOOL GetDataRaw(const CString &aIICName, CString &aValue);

private:
	CDocContact();

    CIICDoc *m_pDocument;

    int m_Id;           // This is our local id for this contact
    int m_bDeleted;     // Indicate if the record has been deleted

    bool m_bIsInterCommunity;  // Indicates if this contact record is for a buddy
                               // in another community

protected:
    // This holds the rest of the user's data
    // Key=IICName Value=Data Value
    //
    CMapStringToString m_Data;  

    // Hunt
    CStringArray m_HuntValues;
    CMapStringToString m_NormalizedHuntData;  

    // Privileges
    int PrivGet(UINT aPriviledge);
    void PrivSet(UINT aPrivilege, UINT aValue);
};

#endif // !defined(AFX_DOCCONTACT_H__F5B12C58_7547_4C3E_8C56_9D1F87DBEAFB__INCLUDED_)
