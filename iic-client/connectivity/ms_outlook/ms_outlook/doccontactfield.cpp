// DocContactField.cpp: implementation of the CDocContactField class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "iic.h"
#include "doccontactfield.h"
#include "doccontacts.h"
//#include "docprofile.h"

#include "iicdoc.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDocContactField::CDocContactField()
{
//    m_ContactDisplayHeader = "";
    m_ContactColumnId = "";

    m_bIsDataField = FALSE;
    m_bContactEncryptData = FALSE;
    m_bContactIsDownloadable = FALSE;
    m_bContactCopyOnUpdate = FALSE;

	m_bAlwaysDownload = FALSE;

    m_bSearchShowInResults = FALSE;
    m_bSearchAllowSearch = FALSE;
    m_bSearchInitialSelect = FALSE;

    m_bEditAllowUser = FALSE;
    m_bEditAllowAdmin = FALSE;
    m_EditType = CDocContactField::kEditTypeString; 
    m_EditMin = -1;
    m_EditMax = -1;
    m_EditIsReadOnly = false;
}

CDocContactField::~CDocContactField()
{

}

//////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////
// AddOption
//
void CDocContactField::AddOption(const CString &aOptionValue, const CString &aOptionDescription)
{
    m_OptionList.SetAt(aOptionValue, aOptionDescription);
    m_OptionDescriptionArray.Add(aOptionDescription);
}

//////////////////////////////////////////////////////////////////////
// GetOptionValueFromDescription
//
// Returns empty string if Description not found
//
CString CDocContactField::GetOptionValueFromDescription(const CString &aDescription)
{
    CString strResult = _T("");
    CString strValue;
    CString strDescription;
    POSITION position = m_OptionList.GetStartPosition();

    while (NULL != position && strResult.IsEmpty())
    {
        m_OptionList.GetNextAssoc(position, strValue, strDescription);

        if (strDescription == aDescription)
        {
            strResult = strValue;
        }
    }

    return strResult;
}

//////////////////////////////////////////////////////////////////////
// GetOptionDescriptionFromValue
//
// Returns empty string if Value not found
//
CString CDocContactField::GetOptionDescriptionFromValue(const CString &aValue)
{
    CString strDescription = _T("");
    m_OptionList.Lookup(aValue, strDescription);
    return strDescription;
}

//////////////////////////////////////////////////////////////////////
// GetOptionDescriptions
//
void CDocContactField::GetOptionDescriptions(CStringArray &aDescriptionArray)
{
    aDescriptionArray.Copy(m_OptionDescriptionArray);
}
