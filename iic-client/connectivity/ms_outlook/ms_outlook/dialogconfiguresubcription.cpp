// DialogConfigureSubcription.cpp : implementation file
//

#include "stdafx.h"
//#include "iic.h"

#include "ms_outlook.h"

#include "iicdoc.h"
#include "dialogconfiguresubcription.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif




/////////////////////////////////////////////////////////////////////////////
// CDocOutlookFoldersInfo

CDocOutlookFoldersInfo::CDocOutlookFoldersInfo()
{
}

CDocOutlookFoldersInfo::~CDocOutlookFoldersInfo()
{
}


/////////////////////////////////////////////////////////////////////////////
// CDialogConfigureSubcription dialog


CDialogConfigureSubcription::CDialogConfigureSubcription( CWnd* pParent /*=NULL*/ )
	: CDialog(CDialogConfigureSubcription::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDialogConfigureSubcription)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
//	m_pDocument = pDocument;
}


void CDialogConfigureSubcription::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialogConfigureSubcription)
	DDX_Control(pDX, IDC_LIST_FOLDERS, m_ListFolders);
	DDX_Control(pDX, IDC_LIST_ADDRBOOKS, m_ListAddrBooks);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDialogConfigureSubcription, CDialog)
	//{{AFX_MSG_MAP(CDialogConfigureSubcription)
	//}}AFX_MSG_MAP
//    ON_REGISTERED_MESSAGE(msgOutlookFoldersLookupCompleted, OnOutlookFoldersLookupCompleted)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CDialogConfigureSubcription message handlers

BOOL CDialogConfigureSubcription::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CDocOutlookFoldersInfo* pFoldersInfo = new CDocOutlookFoldersInfo();

    int iNumFolders = connFetchFolders( );

    pFoldersInfo->m_Folders.Copy( garrFolders );
    pFoldersInfo->m_AddressBooks.Copy( garrABs );

    OutlookFoldersLookupCompleted( pFoldersInfo );

    return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

//*****************************************************************************
void CDialogConfigureSubcription::OnOK() 
{
	int i;

	m_Folders.RemoveAll();
	m_AddressBooks.RemoveAll();

	for (i=0; i<m_ListFolders.GetCount(); i++)
	{
		if (m_ListFolders.GetCheck(i))
		{
			CString temp;
			m_ListFolders.GetText(i, temp);
			m_Folders.Add(temp);
		}
	}
	
	for (i=0; i<m_ListAddrBooks.GetCount(); i++)
	{
		if (m_ListAddrBooks.GetCheck(i))
		{
			CString temp;
			m_ListAddrBooks.GetText(i, temp);
			m_AddressBooks.Add(temp);
		}
	}

	CDialog::OnOK();
}

//*****************************************************************************
LRESULT CDialogConfigureSubcription::OutlookFoldersLookupCompleted( CDocOutlookFoldersInfo *pFoldersInfo )
{
	int i,j;

//	CDocOutlookFoldersInfo* pFoldersInfo = (CDocOutlookFoldersInfo*)lParam;
	if (pFoldersInfo==NULL)
		return -1;

	int nFolders = pFoldersInfo->m_Folders.GetSize();
	for (i=0; i<nFolders; i++)
	{
		CString folder = pFoldersInfo->m_Folders.GetAt(i);
		int index = m_ListFolders.AddString(folder);

		for (j=0; j<m_Folders.GetSize(); j++)
		{
			if (folder == m_Folders.GetAt(j))
			{
				m_ListFolders.SetCheck(index, true);
				m_Folders.RemoveAt(j);
				break;
			}
		}
	}

	// in case online only folders
	for (i=0; i<m_Folders.GetSize(); i++)
	{
		int index = m_ListFolders.AddString(m_Folders.GetAt(i));
		m_ListFolders.SetCheck(index, true);
	}

	int nAddrBooks = pFoldersInfo->m_AddressBooks.GetSize();
	for (i=0; i<nAddrBooks; i++)
	{
		CString name = pFoldersInfo->m_AddressBooks.GetAt(i);
		int index = m_ListAddrBooks.AddString(name);

		for (int j=0; j<m_AddressBooks.GetSize(); j++)
		{
			if (name == m_AddressBooks.GetAt(j))
			{
				m_ListAddrBooks.SetCheck(index, true);
				m_AddressBooks.RemoveAt(j);
				break;
			}
		}
	}

	// in case online only folders
	for (i=0; i<m_AddressBooks.GetSize(); i++)
	{
		int index = m_ListAddrBooks.AddString(m_AddressBooks.GetAt(i));
		m_ListAddrBooks.SetCheck(index, true);
	}

	delete pFoldersInfo;

	return 0;
}