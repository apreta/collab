// DocGroups.cpp: implementation of the CDocGroups class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "iic.h"
#include "docgroups.h"
#include "iicdoc.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Statics
//////////////////////////////////////////////////////////////////////

TCHAR * CDocGroups::kOutlookGroup = _T("1");

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDocGroups::CDocGroups()
{
    m_pDocument = NULL;
}

CDocGroups::~CDocGroups()
{
    ClearAll();
}

//////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Initialize
//
void CDocGroups::Initialize(CIICDoc *aDocument)
{
    m_pDocument = aDocument;
}

//////////////////////////////////////////////////////////////////////
// ClearAll
//
void CDocGroups::ClearAll()
{
    CSingleLock singleLock(&m_CriticalSection);
    singleLock.Lock();  

    POSITION position;

    //
    // Delete all active CDocGroup objects
    //
    CString strGroupId;
    CDocGroup *pGroup;

    position = m_Groups.GetStartPosition();

    while (position != NULL)
    {
        m_Groups.GetNextAssoc(position, strGroupId, (void *&)pGroup);
        delete pGroup;
    }

    m_Groups.RemoveAll();

    //
    // Delete all deleted CDocGroup objects
    //
    int deletedcount = m_DeletedGroups.GetSize();

    for (int loop=0; loop < deletedcount; loop++)
    {
        pGroup = (CDocGroup *)m_DeletedGroups.GetAt(loop);
        delete pGroup;
    }

    m_DeletedGroups.RemoveAll();

    //
    // Delete all CStringArrays maintained for the users
    //
    CString strUserId;
    CStringArray *pStringArray;

    position = m_UserGroups.GetStartPosition();

    while (position != NULL)
    {
        m_UserGroups.GetNextAssoc(position, strUserId, (void *&)pStringArray);
        delete pStringArray;
    }

    m_UserGroups.RemoveAll();

    singleLock.Unlock();  
}

//////////////////////////////////////////////////////////////////////
// PrepareForRefresh
//
// NOTE: does not notify views...
//
void CDocGroups::PrepareForRefresh()
{
    CSingleLock singleLock(&m_CriticalSection);
    singleLock.Lock();  

    //
    // Move all groups to the Deleted map...
    //
    CString strGroupId;
    CDocGroup *pGroup;

    POSITION position = m_Groups.GetStartPosition();

    while (position != NULL)
    {
        m_Groups.GetNextAssoc(position, strGroupId, (void *&)pGroup);
        m_DeletedGroups.Add(pGroup);
    }

    m_Groups.RemoveAll();

    //
    // Delete all CStringArrays maintained for the user's group membership
    //
    CString strUserId;
    CStringArray *pStringArray;

    position = m_UserGroups.GetStartPosition();

    while (position != NULL)
    {
        m_UserGroups.GetNextAssoc(position, strUserId, (void *&)pStringArray);
        delete pStringArray;
    }

    m_UserGroups.RemoveAll();

    singleLock.Unlock();  
}

//////////////////////////////////////////////////////////////////////
// RemoveGroup
//
void CDocGroups::RemoveGroup(const CString &aGroupId)
{
    CDocGroup *pGroup = NULL;

    CSingleLock singleLock(&m_CriticalSection);
    singleLock.Lock();  

    if (m_Groups.Lookup(aGroupId, (void *&)pGroup))
    {
        m_Groups.RemoveKey(aGroupId);
        m_DeletedGroups.Add(pGroup);
    }

    singleLock.Unlock();  
}

//////////////////////////////////////////////////////////////////////
// RemoveUserFromGroup
//
void CDocGroups::RemoveUserFromGroup(const CString &aGroupId, const CString &aUserId)
{
    CStringArray *pGroups = NULL;
    int index = -1;

    CSingleLock singleLock(&m_CriticalSection);
    singleLock.Lock();  

    // m_UserGroups tracks all groups that a user is a 
    // member of.  Remove this group for this user...
    if (m_UserGroups.Lookup(aUserId, (void *&)pGroups))
    {
        for (int loop=0; (index == -1) && (loop < pGroups->GetSize()); loop++)
        {
            if (aGroupId == pGroups->GetAt(loop))
            {
                index = loop;
            }
        }

        if (index != -1)
        {
            pGroups->RemoveAt(index);
        }
    }

    singleLock.Unlock();  

    // Now remove the user from the group
    CDocGroup *pGroup = this->GetGroupFromId(aGroupId);

    if (pGroup != NULL)
    {
        pGroup->RemoveUser(aUserId);
    }
}

//////////////////////////////////////////////////////////////////////
// RemoveUserFromAllGroups
//
void CDocGroups::RemoveUserFromAllGroups(const CString &aUserId)
{
    CStringArray *pGroups = NULL;
    CStringArray arrayGroups;

    CSingleLock singleLock(&m_CriticalSection);
    singleLock.Lock();  

    // m_UserGroups tracks all groups that a user is a 
    // member of.  Remove this group for this user...
    if (m_UserGroups.Lookup(aUserId, (void *&)pGroups))
    {
        arrayGroups.Copy(*pGroups);
        pGroups->RemoveAll();
    }

    singleLock.Unlock();  

    for (int loop=0; loop < arrayGroups.GetSize(); loop++)
    {
        CString strGroupId = arrayGroups.GetAt(loop);

        CDocGroup *pGroup = this->GetGroupFromId(strGroupId);

        if (pGroup != NULL)
        {
            pGroup->RemoveUser(aUserId);
        }
    }
}

//////////////////////////////////////////////////////////////////////
// GetUserGroupCount
//
// Returns the number of groups that contain the specified user
//
int CDocGroups::GetUserGroupCount(const CString &aUserId)
{
    int returnValue = 0;
    CStringArray *pGroups = NULL;

    CSingleLock singleLock(&m_CriticalSection);
    singleLock.Lock();  

    if (m_UserGroups.Lookup(aUserId, (void *&)pGroups))
    {
        returnValue = pGroups->GetSize();
    }

    singleLock.Unlock();  

    return returnValue;
}

//////////////////////////////////////////////////////////////////////
// GetUserGroupNameAt
//
// Returns the group name at the given index for the given user.
// 
// Used with GetUserGroupCount, this function allows consumers 
// to iterate over the groups that contain the specified user.
//
// Returns true if the specified user and index are valid.  Note
// that they may not be valid if another thread has modified the list of
// groups, which may result in a return value of false, in which 
// the resulting groupid should be ignored
//
BOOL CDocGroups::GetUserGroupNameAt(const CString &aUserId, int aIndex, CString &aGroupName)
{
    BOOL returnValue = FALSE;
    CStringArray *pGroups = NULL;

    CSingleLock singleLock(&m_CriticalSection);
    singleLock.Lock();  

    if (m_UserGroups.Lookup(aUserId, (void *&)pGroups))
    {
        if (aIndex < pGroups->GetSize())
        {
            CString strGroupId = pGroups->GetAt(aIndex);
            CDocGroup *pGroup = NULL;

            if (m_Groups.Lookup(strGroupId, (void *&)pGroup))
            {
                pGroup->GetName(aGroupName);
                returnValue = TRUE;
            }
        }
    }

    singleLock.Unlock();  

    return returnValue;
}

//////////////////////////////////////////////////////////////////////
// GetUserGroupAt
//
// Returns the group at the given index for the given user.
// 
// Used with GetUserGroupCount, this function allows consumers 
// to iterate over the groups that contain the specified user.
//
// Returns true if the specified user and index are valid.  Note
// that they may not be valid if another thread has modified the list of
// groups, which may result in a return value of false, in which 
// the resulting group point is NULL.
//
CDocGroup *CDocGroups::GetUserGroupAt(const CString &aUserId, int aIndex)
{
    CDocGroup *returnGroup = NULL;
    CStringArray *pGroups = NULL;

    CSingleLock singleLock(&m_CriticalSection);
    singleLock.Lock();  

    if (m_UserGroups.Lookup(aUserId, (void *&)pGroups))
    {
        if (aIndex < pGroups->GetSize())
        {
            CString strGroupId = pGroups->GetAt(aIndex);
            CDocGroup *pGroup = NULL;

            if (m_Groups.Lookup(strGroupId, (void *&)pGroup))
            {
                returnGroup = pGroup;
            }
        }
    }

    singleLock.Unlock();  

    return returnGroup;
}

//////////////////////////////////////////////////////////////////////
// GetAllGroupId
//
BOOL CDocGroups::GetAllGroupId(const CString &aDirectoryId, CString &aGroupId)
{
    CSingleLock singleLock(&m_CriticalSection);
    singleLock.Lock();  

    BOOL bFound = FALSE;
    CString strGroupId;
    CDocGroup *pGroup;

    POSITION position = m_Groups.GetStartPosition();

    while (position != NULL && (!bFound))
    {
        m_Groups.GetNextAssoc(position, strGroupId, (void *&)pGroup);

        if (pGroup->IsInDirectory(aDirectoryId))
        {
            if (pGroup->IsAllGroup())
            {
                bFound = TRUE;
                aGroupId = strGroupId;
            }
        }
    }

    singleLock.Unlock();  

    return bFound;
}

//////////////////////////////////////////////////////////////////////
// GetGroupsInDirectory
//
void CDocGroups::GetGroupsInDirectory(CPtrArray &aGroupArray, const CString &aDirectoryId)
{

    CSingleLock singleLock(&m_CriticalSection);
    singleLock.Lock();  

    CString strGroupId;
    CDocGroup *pGroup;

    POSITION position = m_Groups.GetStartPosition();

    while (position != NULL)
    {
        m_Groups.GetNextAssoc(position, strGroupId, (void *&)pGroup);

        if (pGroup->IsInDirectory(aDirectoryId))
        {
            aGroupArray.Add(pGroup);
        }
    }

    singleLock.Unlock();  
}

//////////////////////////////////////////////////////////////////////
// GetGroupFromId
//
CDocGroup *CDocGroups::GetGroupFromId(const CString &aGroupId)
{
    CSingleLock singleLock(&m_CriticalSection);
    singleLock.Lock();  

    CDocGroup *pGroup = NULL;
    m_Groups.Lookup(aGroupId, (void *&)pGroup);

    singleLock.Unlock();
    
    return pGroup;
}

//////////////////////////////////////////////////////////////////////
// GetGroupNameFromId
//
BOOL CDocGroups::GetGroupNameFromId(const CString &aGroupId, CString &aGroupName)
{
    BOOL result = FALSE;
    aGroupName = "";

    CSingleLock singleLock(&m_CriticalSection);
    singleLock.Lock();  

    CDocGroup *pGroup = NULL;

    if (m_Groups.Lookup(aGroupId, (void *&)pGroup))
    {
        result = TRUE;
        pGroup->GetName(aGroupName);
    }

    singleLock.Unlock();
    
    return result;
}

