// DocContacts.h: interface for the CDocContacts class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DOCCONTACTS_H__A73818D1_E1EB_4762_A6D4_F233D0952C30__INCLUDED_)
#define AFX_DOCCONTACTS_H__A73818D1_E1EB_4762_A6D4_F233D0952C30__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "DocContact.h"

#include <afxcoll.h>

// Forward reference for our main document object
class CIICDoc;


// Available CContactDisplayDefinition types
#define kDisplayTypeView 0
#define kDisplayTypeEdit 1

// User admin priviledge values
static TCHAR *kSystemDefault           = _T("-1");
static TCHAR *kUserPrioritySoft        = _T("0");
static TCHAR *kUserPriorityHard        = _T("1");

// User Type values
static TCHAR *kUserTypeContact        = _T("0");
static TCHAR *kUserTypeUser           = _T("1");
static TCHAR *kUserTypeAdmin          = _T("2");

// User Admin Defaults
static TCHAR *kDefaultUserType     = kUserTypeContact;
static TCHAR *kDefaultProfileId    = _T("0");
static TCHAR *kDefaultMaxSize      = _T("5");
static TCHAR *kDefaultMaxPriority  = _T("0");
static TCHAR *kDefaultPriorityType = kUserPrioritySoft;
static TCHAR *kDefaultEnabled      = _T("0");
static TCHAR *kDefaultDisabled     = _T("0");

// Special Contact values
#define kContactNone        _T("None")
#define kContactPreferred   _T("Pref")

static TCHAR *kContactFirstLast = _T("FL");
static TCHAR *kContactLastFirst = _T("LF");
static TCHAR *kContactBlank     = _T("N");

static TCHAR *kContactOutlookFullName = _T("OutlookFullName");
static TCHAR *kContactInMeetingName   = _T("displayname");
static TCHAR *kContactDescription     = _T("description");

// Available Contact column ids
static TCHAR *kContactUserId           = _T("userid");
static TCHAR *kContactDirectory        = _T("directoryid");
static TCHAR *kContactGroup            = _T("group");
static TCHAR *kContactUserName         = _T("username");
static TCHAR *kContactTitle            = _T("title");
static TCHAR *kContactFirst            = _T("first");
static TCHAR *kContactMiddle           = _T("middle");
static TCHAR *kContactLast             = _T("last");
static TCHAR *kContactSuffix           = _T("suffix");
static TCHAR *kContactCompany          = _T("company");
static TCHAR *kContactJobTitle         = _T("jobtitle");
static TCHAR *kContactAddress1         = _T("address1");
static TCHAR *kContactAddress2         = _T("address2");
static TCHAR *kContactState            = _T("state");
static TCHAR *kContactCountry          = _T("country");
static TCHAR *kContactPostalCode       = _T("postalcode");
static TCHAR *kContactIICScreen        = _T("screenname");
static TCHAR *kContactAimScreen        = _T("aimname");
static TCHAR *kContactEmail1           = _T("email");
static TCHAR *kContactEmail2           = _T("email2");
static TCHAR *kContactEmail3           = _T("email3");
static TCHAR *kContactBusinessPhone    = _T("busphone");
static TCHAR *kContactHomePhone        = _T("homephone");
static TCHAR *kContactMobilePhone      = _T("mobilephone");
static TCHAR *kContactOtherPhone       = _T("otherphone");
static TCHAR *kContactExtension        = _T("extension");
static TCHAR *kContactUser1            = _T("user1");
static TCHAR *kContactUser2            = _T("user2");
static TCHAR *kContactUser3            = _T("user3");
static TCHAR *kContactUser4            = _T("user4");
static TCHAR *kContactUser5            = _T("user5");
static TCHAR *kContactDefPhone         = _T("defphone");
static TCHAR *kContactDefEmail         = _T("defemail");

// Only set by admins
static TCHAR *kContactUserType         = _T("type");
static TCHAR *kContactPassword         = _T("password");
static TCHAR *kContactServer           = _T("server");
static TCHAR *kContactProfileId        = _T("profileid");
static TCHAR *kContactPrivMaxPriority  = _T("maxpriority");
static TCHAR *kContactPrivPriorityType = _T("prioritytype");
static TCHAR *kContactPrivEnabled      = _T("enabledfeatures");
static TCHAR *kContactPrivDisabled     = _T("disabledfeatures");


enum ContactFields {
    // Special display only fields
    kContactFieldPreferred = 0,
    kContactFieldFirstLast,
    kContactFieldLastFirst,
    kContactFieldNone,

    // Standard Contact Fields
    kContactFieldUserId,
    kContactFieldDirectory,
    kContactFieldGroup,
    kContactFieldUserName,
    kContactFieldTitle,
    kContactFieldFirst,
    kContactFieldMiddle,
    kContactFieldLast,
    kContactFieldSuffix,
    kContactFieldCompany,
    kContactFieldJobTitle,
    kContactFieldAddress1,
    kContactFieldAddress2,
    kContactFieldState,
    kContactFieldCountry,
    kContactFieldPostalCode,
    kContactFieldIICScreen,
    // kContactFieldAimScreen,
    kContactFieldEmail1,
    kContactFieldEmail2,
    kContactFieldEmail3,
    kContactFieldBusinessPhone,
    kContactFieldBusinessDirect,
    kContactFieldHomePhone,
    kContactFieldHomeDirect,
    kContactFieldMobilePhone,
    kContactFieldMobileDirect,
    kContactFieldOtherPhone,
    kContactFieldOtherDirect,
    kContactFieldExtension,
    kContactFieldUser1,
    kContactFieldUser2,
    kContactFieldUser3,
    kContactFieldUser4,
    kContactFieldUser5,
    kContactFieldDefPhone,
    kContactFieldDefEmail,

    // Contact fields that are
    // only set by admins
    kContactFieldUserType,
    kContactFieldPassword,
    kContactFieldServer,
    kContactFieldProfileId,
    kContactFieldPrivEnabled,
    kContactFieldPrivDisabled,

};



class CDocContacts : public CObject  
{
public:

    CPtrArray m_ContactFields;

    // Lists the fields that actually correspond to field data in the server database
    TCHAR   m_tC;
    CArray <TCHAR *, TCHAR *> m_FieldNames;

    // This map provides a list of fields that are valid to download
    // as contact information.  If the field is not listed in this 
    // map, it will never make the list of fields to download...
    //
    // An example of this is Password, which is not added to the list
    // because we never want to try to download this as contact info
    // (it doesn't actually exist on the contact record!)
    // CMapStringToString m_ContactFieldsOLD;  

	CDocContacts();
	virtual ~CDocContacts();
    void DumpContacts();
    void Initialize(CIICDoc *aDocument);
    void InitializeView();
    void InitializeContactFields();
    void InitializeContacts();

    CDocContact *FindContactByUserName(const CString &aUserName);

    CDocContact *FindFirstContact(BOOL aUsersOnly, const CString &aSearchField, const CString &aSearchValue);
    BOOL FindAllContacts(BOOL aUsersOnly, const CString &aSearchField, const CString &aSearchValue, CPtrList &aSearchResults);

    void PrepareForRefresh();
    void ApplyDownloadFields();
    void SetDownloadedFields();
    BOOL IsFieldDownloadChanged();
    void ResetFieldDownloadChanged();

    CDocContact *GetContact(int aContactId); 
    int          GetContactCount(); 
    CDocContact *GetContactFromUserId(const CString &aUserId);
    CDocContact *GetContactFromIICScreenName(const CString &aIICScreenName);

    void ClearAll();

    void GetGroupBy(int aGroupById, CString &aGroupBy);

private:
    CCriticalSection m_ContactCriticalSection;
    CCriticalSection m_ListenerCriticalSection;

    CIICDoc *m_pDocument;

    CStringList  m_DownloadedFields;
    CStringArray m_FieldsToDownload;
    BOOL m_IsFieldDownloadChanged;

    CPtrArray m_Contacts;
    CMap <CString, CString, int, int> m_MapUserIds; // Map Server Userid to index in m_Contacts array
    CPtrArray m_ContactListeners;

    CStringArray m_GroupBy;

    BOOL AddDownloadField(CStringList &aDownloadList, const CString &aField);
};

#endif // !defined(AFX_DOCCONTACTS_H__A73818D1_E1EB_4762_A6D4_F233D0952C30__INCLUDED_)
