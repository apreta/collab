// DocDirectory.cpp: implementation of the CDocDirectory class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
//#include "iic.h"
#include "docdirectory.h"
//#include "utils.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include "iicdoc.h"
#include "docgroup.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDocDirectory::CDocDirectory()
{
    m_pDocument = NULL;
}

CDocDirectory::CDocDirectory(CIICDoc *aDocument, const CString &aId, const CString &aName, BOOL aIsSubscribed, int aType)
{
    m_pDocument = aDocument;
    m_Id = aId;
    m_Name = aName;
    m_IsSubscribed = aIsSubscribed;
    m_IsDownloaded = FALSE;
    m_IsServiceAvailable = FALSE;
    m_Type = aType;

    m_IdAsChar = new char[50];
    strcpy(m_IdAsChar, CUTF8String(m_Id));
}

CDocDirectory::~CDocDirectory()
{
    delete m_IdAsChar;
}

//////////////////////////////////////////////////////////////////////
// Implementation 
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// GetId
//
void CDocDirectory::GetId(CString &aId)
{
    aId = m_Id;
}

//////////////////////////////////////////////////////////////////////
// GetIdBuffer
//
char *CDocDirectory::GetIdBuffer()
{
    return m_IdAsChar;
}

//////////////////////////////////////////////////////////////////////
// GetGroupById
//
void CDocDirectory::GetGroupById(CString &aId)
{
    aId = kDirectoryPrefix + m_Id;
}

//////////////////////////////////////////////////////////////////////
// GetName
//
void CDocDirectory::GetName(CString &aName)
{
    aName = m_Name;
}

//////////////////////////////////////////////////////////////////////
// GetType
//
int CDocDirectory::GetType()
{
    return m_Type;
}   

//////////////////////////////////////////////////////////////////////
// GetAllGroup
//
CDocGroup *CDocDirectory::GetAllGroup()
{
    CDocGroup *pGroup = NULL;
    CString strGroupId;

    m_pDocument->m_Groups.GetAllGroupId(m_Id, strGroupId);
    pGroup = m_pDocument->m_Groups.GetGroupFromId(strGroupId);

    return pGroup;
}

//////////////////////////////////////////////////////////////////////
// IsSubscribed
//
BOOL CDocDirectory::IsSubscribed()
{
    return m_IsSubscribed;
}

//////////////////////////////////////////////////////////////////////
// IsDownloaded
//
BOOL CDocDirectory::IsDownloaded()
{
    return m_IsDownloaded;
}

//////////////////////////////////////////////////////////////////////
// IsService
//
BOOL CDocDirectory::IsService()
{
    return ((m_Type == dtMessenger) ||
            (m_Type == dtYahoo) ||
            (m_Type == dtAOL));
}

//////////////////////////////////////////////////////////////////////
// IsReadOnly
//
BOOL CDocDirectory::IsReadOnly()
{
	return TRUE;
}

//////////////////////////////////////////////////////////////////////
// IsGlobal
//
BOOL CDocDirectory::IsGlobal()
{
    return (m_Type == dtGlobal);
}

//////////////////////////////////////////////////////////////////////
// IsPersonal
//
BOOL CDocDirectory::IsPersonal()
{
    return (m_Type == dtPersonal);
}

//////////////////////////////////////////////////////////////////////
// IsDirectoryType
//
// aDirectoryType parameter is a kDirectoryType constant 
// from CDocDirectories.h
//
BOOL CDocDirectory::IsDirectoryType(const CString &aDirectoryType)
{
    BOOL bResult = FALSE;

    if (aDirectoryType == kDirectoryTypeActiveDirectory)
    {
        bResult = (m_Type == dtLDAP);
    }
    else if (aDirectoryType == kDirectoryTypeOutlook)
    {
        bResult = (m_Type == dtOutlook);
    }
    else if (aDirectoryType == kDirectoryTypeIIC)
    {
        bResult = (m_Type == dtGlobal) || (m_Type == dtPersonal);
    }

    return bResult;
}

