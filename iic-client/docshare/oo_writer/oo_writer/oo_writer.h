// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the OO_WRITER_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// OO_WRITER_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.


#ifdef WIN32
#ifdef OO_WRITER_EXPORTS
#define OO_WRITER_API __declspec(dllexport)
#else
#define OO_WRITER_API __declspec(dllimport)
#endif

#else       //  #ifdef WIN32

#ifdef OO_WRITER_EXPORTS
#define OO_WRITER_API
#else
#define OO_WRITER_API SAL_DLLPUBLIC_EXPORT
#endif

#endif       //  #ifdef WIN32

// This class is exported from the oo_writer.dll

extern "C" OO_WRITER_API int dsGetRelPri( void );
extern "C" OO_WRITER_API int dsGetPriLen( void );
extern "C" OO_WRITER_API int dsGetPriTypes( char *pC, int iMaxLen );
extern "C" OO_WRITER_API int dsGetSecLen( void );
extern "C" OO_WRITER_API int dsGetSecTypes( char *pC, int iMaxLen );

extern "C" OO_WRITER_API int dsGetStatus( void );
extern "C" OO_WRITER_API int dsGetPreLoadExeCmdLen( void );
extern "C" OO_WRITER_API int dsGetPreLoadExeCmd( char *pC, int iMaxLen );
extern "C" OO_WRITER_API int dsGetPostLoadKillCmdLen( void );
extern "C" OO_WRITER_API int dsGetPostLoadKillCmd( char *pC, int iMaxLen );
extern "C" OO_WRITER_API int dsPreLoad( );
extern "C" OO_WRITER_API int dsLoad( char *pFN, unsigned long ulHWnd );
extern "C" OO_WRITER_API int dsPostLoad( );
extern "C" OO_WRITER_API int dsGetNumPages( void );
extern "C" OO_WRITER_API int dsGetTempPath( char *pC, int iMaxLen );

extern "C" OO_WRITER_API void dsAbort( );
