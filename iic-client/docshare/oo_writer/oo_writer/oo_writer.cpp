//
// oo_writer.cpp : Defines the entry point for the OOo Writer DLL
//

#ifdef WIN32
#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <windows.h>

#else
//#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <unistd.h>     //  for unlink( )

#endif      // #ifdef WIN32

#include "oo_writer.h"

#include <stdio.h>
#include <wchar.h>

#ifdef WIN32
#include <direct.h>
#include <io.h>
#include <process.h>

#define WNT
#define CPPU_ENV	msci
#endif      // #ifdef WIN32

#include <cppuhelper/bootstrap.hxx>

#include <osl/file.hxx>
#include <rtl/process.h>
#include <rtl/strbuf.hxx>

#include <com/sun/star/bridge/XUnoUrlResolver.hpp>

#ifdef _COM_SUN_STAR_BRIDGE_XUNOURLRESOLVER_HPP_

#include <com/sun/star/frame/XComponentLoader.hpp>
#include <com/sun/star/beans/XPropertySet.hpp>
#include <com/sun/star/beans/PropertyValue.hpp>
#include <com/sun/star/drawing/XDrawPagesSupplier.hpp>
#include <com/sun/star/document/XDocumentInfoSupplier.hpp>
#include <com/sun/star/document/XExporter.hpp>
#include <com/sun/star/document/XFilter.hpp>
#include <com/sun/star/drawing/XDrawPages.hpp>
#include <com/sun/star/drawing/XDrawPage.hpp>
#include <com/sun/star/frame/XController.hpp>
#include <com/sun/star/frame/XStorable.hpp>
#include <com/sun/star/frame/FrameSearchFlag.hpp>
#include <com/sun/star/lang/XMultiServiceFactory.hpp>
#include <com/sun/star/lang/SystemDependent.hpp>

#ifndef _COM_SUN_STAR_AWT_XTOOLKIT_HPP_
#include <com/sun/star/awt/XToolkit.hpp>
#endif

#include <com/sun/star/awt/WindowAttribute.hpp>
#include <com/sun/star/awt/XSystemChildFactory.hpp>

#include <string.h>

// handy cause OUStrings are very often used in the API
#define createStr(x)            (OUString::createFromAscii(x))   

//*****************************************************************************

using namespace rtl;
using namespace com::sun::star::uno;
using namespace com::sun::star::lang;
using namespace com::sun::star::beans;
using namespace com::sun::star::bridge;
using namespace com::sun::star::frame;
using namespace com::sun::star::registry;
using namespace com::sun::star::drawing;
using namespace com::sun::star::document;
using namespace com::sun::star::awt;


using com::sun::star::registry::XSimpleRegistry;
using com::sun::star::beans::XPropertySet;
using com::sun::star::beans::PropertyValue;
using com::sun::star::bridge::XUnoUrlResolver;
using com::sun::star::lang::XMultiServiceFactory;
using com::sun::star::awt::XToolkit;
using com::sun::star::awt::XSystemChildFactory;


// Function declarations


//  Globals
Reference< XToolkit >   mxToolkit;

Reference <XMultiComponentFactory> xMCFClient;
Reference< XComponent > gcomponent;

int             dsRelPri    = 2000;
int             dsStatus    = 0;

unsigned long   gulWnd;
char            *gpFN       = NULL;
int             gNumPages   = 0;
int             gPriLen     = 0;
int             gSecLen     = 0;
char            goBuf[ 512 ];
bool            gfAbort     = false;
bool            gf4x        = true;

Reference <XComponentContext> oomgrContext;
Reference <XMultiComponentFactory> oomgr;
Reference <XWindowPeer> gmyWinPeer;
Reference <XWindow>     gmyWin      = NULL;
Reference <XComponentContext> bootContext;
Reference <XMultiServiceFactory> myMSF;

char    strPriTypes[]       = {"OpenDocument Text (*.odt)|*.odt|OpenDocument Text Template (*.ott)|*.ott|OpenOffice.org 1.0 Text Document (*.sxw)|*.sxw|OpenOffice.org 1.0 Text Document Template (*.stw)|*.stw"};
char    strSecTypes[]       = {"MS Word 97/2000/XP (*.doc)|*.doc|MS Word 97/2000/XP Template (*.dot)|*.dot|MS Word 6.0/95 (*.doc)|*.doc|MS Word 95 Template (*.dot)|*.dot"};
char    strPreLoadExeCmd[]  = {"soffice -headless \"-accept=socket,host=localhost,port=2083;urp;StarOffice.ServiceManager\""};
char    strPostLoadKillCmd[]= {"soffice.bin"};

#if 0
static void myMsg( char *p )
{
    FILE *pf;
    pf = fopen( "/home/jim/log.txt", "a" );
    if( pf )
    {
        fprintf( pf, "%s\n", p );
        fclose( pf );
    }
}

static void myMsgO( OUString ouIn )
{
    int     i;
    char    myBuf[ 1000 ];
    for( i=0; i<ouIn.getLength( ); i++ )
        myBuf[ i ] = (char) ouIn.pData->buffer[ i ];
    myBuf[ i ] = 0;
    myMsg( myBuf );
}
#endif



//*****************************************************************************
int gsPDF2PNG( char *pPDF )
{

    char    bufInName[ 1024 ];
    char    bufOutName[ 1024 ];
    int     idx     = 0;
    int     iRet    = 0;
    if( gf4x )
    {
#ifdef WIN32
        sprintf( bufInName, "\"%s%s\"", goBuf+8, pPDF );
        sprintf( bufOutName, "-o \"%sPage-%s06dhi.png\"", goBuf+8, "%" );
        iRet = (int) spawnl( _P_WAIT, "gswin32c.exe", "gswin32c.exe", "-I.;./gs;./gs/lib", "-sDEVICE=png16m", "-r384x384", bufOutName, bufInName, NULL );
#else
        sprintf( bufInName, "%s%s", goBuf+7, pPDF );
        sprintf( bufOutName, "-sOutputFile=%sPage-%s06dhi.png", goBuf+7, "%" );
        gchar  *argv[ 15 ];
        argv[ idx++ ] = "gs";
        argv[ idx++ ] = "gs";
        argv[ idx++ ] = "-q";
        argv[ idx++ ] = "-dBATCH";
        argv[ idx++ ] = "-dNOPAUSE";
        argv[ idx++ ] = "-sDEVICE=png16m";
        argv[ idx++ ] = "-r384x384";
        argv[ idx++ ] = bufOutName;
        argv[ idx++ ] = bufInName;
        argv[ idx++ ] = NULL;

        int         iExit   = 0;
        gboolean    gbRet;
        gchar       *buf_out, *buf_err;
        GError      *pgErr  = NULL;
        gbRet = g_spawn_sync( NULL, argv, NULL, (GSpawnFlags)((G_SPAWN_SEARCH_PATH) | (G_SPAWN_FILE_AND_ARGV_ZERO)),
                              NULL, NULL, &buf_out, &buf_err, &iExit, &pgErr );
#endif
    }
    else
    {
#ifdef WIN32
        sprintf( bufInName, "\"%s%s\"", goBuf+8, pPDF );
        sprintf( bufOutName, "-o \"%sPage-%s06d.png\"", goBuf+8, "%" );
        iRet = (int) spawnl( _P_WAIT, "gswin32c.exe", "gswin32c.exe", "-I.;./gs;./gs/lib", "-sDEVICE=png16m", "-r192x192", bufOutName, bufInName, NULL );
#else
        sprintf( bufInName, "%s%s", goBuf+7, pPDF );
        sprintf( bufOutName, "-sOutputFile=%sPage-%s06d.png", goBuf+7, "%" );
        gchar  *argv[ 15 ];
        argv[ idx++ ] = "gs";
        argv[ idx++ ] = "gs";
        argv[ idx++ ] = "-q";
        argv[ idx++ ] = "-dBATCH";
        argv[ idx++ ] = "-dNOPAUSE";
        argv[ idx++ ] = "-sDEVICE=png16m";
        argv[ idx++ ] = "-r192x192";
        argv[ idx++ ] = bufOutName;
        argv[ idx++ ] = bufInName;
        argv[ idx++ ] = NULL;
        gboolean    gbRet;
        gbRet = g_spawn_sync( NULL, argv, NULL, (GSpawnFlags)(G_SPAWN_SEARCH_PATH | G_SPAWN_FILE_AND_ARGV_ZERO), NULL, NULL, NULL, NULL, NULL, NULL );
#endif
    }

    return iRet;   //  Zero return means SUCCESS
}


#ifdef WIN32

//*****************************************************************************
bool APIENTRY DllMain(HINSTANCE hinstDLL, unsigned long fdwReason, LPVOID lpvReserved)
{
    gPriLen = (int) strlen( strPriTypes );
    gSecLen = (int) strlen( strSecTypes );
    gfAbort = false;

    switch (fdwReason)
    {
        case DLL_PROCESS_ATTACH:
            // attach to process
            // return FALSE to fail DLL load
            break;

        case DLL_PROCESS_DETACH:
            // detach from process
            break;

        case DLL_THREAD_ATTACH:
            // attach to thread
            break;

        case DLL_THREAD_DETACH:
            // detach from thread
            break;
    }
    return TRUE; // succesful
}

#endif      //  #ifdef WIN32


//*****************************************************************************
//  Sets the internal abort flag which we check as frequently as possible
OO_WRITER_API void dsAbort( )
{
    gfAbort = true;
}

//*****************************************************************************
OO_WRITER_API int dsGetStatus( void )
{
    return dsStatus;
}


//*****************************************************************************
OO_WRITER_API int dsGetRelPri( void )
{
    return dsRelPri;
}


//*****************************************************************************
OO_WRITER_API int dsGetPriLen( void )
{
    return (int) strlen( strPriTypes );
}


//*****************************************************************************
OO_WRITER_API int dsGetSecLen( void )
{
    return (int) strlen( strSecTypes );
}


//*****************************************************************************
OO_WRITER_API int dsGetPreLoadExeCmdLen( void )
{
    return (int) strlen( strPreLoadExeCmd );
}


//*****************************************************************************
OO_WRITER_API int dsGetPostLoadKillCmdLen( void )
{
    return (int) strlen( strPostLoadKillCmd );
}


//*****************************************************************************
OO_WRITER_API int dsGetNumPages( void )
{
    return gNumPages;
}


//*****************************************************************************
OO_WRITER_API int dsGetPriTypes( char *pC, int iMaxLen )
{
    int iLen = (int) strlen( strPriTypes );
    if( iLen > iMaxLen )
    {
        strncpy( pC, strPriTypes, iMaxLen - 1 );
        pC[ iMaxLen - 1 ] = 0;
        return -iLen;
    }

    strcpy( pC, strPriTypes );
    pC[ iLen ] = 0;
    return iLen;
}


//*****************************************************************************
OO_WRITER_API int dsGetSecTypes( char *pC, int iMaxLen )
{
    int iLen = (int) strlen( strSecTypes );
    if( iLen > iMaxLen )
    {
        strncpy( pC, strSecTypes, iMaxLen - 1 );
        pC[ iMaxLen - 1 ] = 0;
        return -iLen;
    }

    strcpy( pC, strSecTypes );
    pC[ iLen ] = 0;
    return iLen;
}


//*****************************************************************************
OO_WRITER_API int dsGetPreLoadExeCmd( char *pC, int iMaxLen )
{
    int iLen = (int) strlen( strPreLoadExeCmd );
    if( iLen > iMaxLen )
    {
        strncpy( pC, strPreLoadExeCmd, iMaxLen - 1 );
        pC[ iMaxLen - 1 ] = 0;
        return -iLen;
    }

    strcpy( pC, strPreLoadExeCmd );
    pC[ iLen ] = 0;
    return iLen;
}


//*****************************************************************************
OO_WRITER_API int dsGetPostLoadKillCmd( char *pC, int iMaxLen )
{
    int iLen = (int) strlen( strPostLoadKillCmd );
    if( iLen > iMaxLen )
    {
        strncpy( pC, strPostLoadKillCmd, iMaxLen - 1 );
        pC[ iMaxLen - 1 ] = 0;
        return -iLen;
    }

    strcpy( pC, strPostLoadKillCmd );
    pC[ iLen ] = 0;
    return iLen;
}


//*****************************************************************************
OO_WRITER_API int dsGetTempPath( char *pC, int iMaxLen )
{
    int iLen = (int) strlen( goBuf );
    if( iLen > iMaxLen )
    {
        strncpy( pC, goBuf, iMaxLen - 1 );
        pC[ iMaxLen - 1 ] = 0;
        return -iLen;
    }

    strcpy( pC, goBuf );
    pC[ iLen ] = 0;
    return iLen;
}


//*****************************************************************************
static void exportAllPagesAsPNG( )
{
    int         i;
    sal_Int16   startpage = 0;
    sal_Bool    bF = sal_False;
    sal_Bool    bT = sal_True;

    OUString    sNewTemp;
    osl_getProcessWorkingDir( &sNewTemp.pData );
    sNewTemp += createStr( "/dspages/" );

    for( i=0; i<sNewTemp.getLength( ); i++ )
        goBuf[ i ] = (char) sNewTemp.pData->buffer[ i ];
    goBuf[ i ] = 0;

    //  Delete all files from our temp output folder
    char    findBuf[ 540 ];
#ifdef WIN32
    strcpy( findBuf, goBuf+8 );     //  The +8 skips over the "file:///" prefix
    strcat( findBuf, "*.*" );
    struct _finddata_t  findData;
    long                lFind;
    lFind = (long) _findfirst( findBuf, &findData );
    if( lFind != -1L )
    {
        char    delBuf[ 540 ];
        strcpy( delBuf, goBuf+8 );
        strcat( delBuf, findData.name );
        unlink( delBuf );
        while( _findnext( lFind, &findData )  ==  0 )
        {
            strcpy( delBuf, goBuf+8 );
            strcat( delBuf, findData.name );
            unlink( delBuf );
        }
        _findclose( lFind );
    }
#else
    strcpy( findBuf, goBuf+7 );     //  The +7 skips over the "file:///" prefix
    GDir    *pDir;
    pDir = g_dir_open( findBuf, 0, NULL );
    if( pDir )
    {
        char    delBuf[ 540 ];
        const char  *pName;
        while( (pName = g_dir_read_name( pDir ))  != NULL )
        {
            strcpy( delBuf, findBuf );
            strcat( delBuf, pName );
            g_unlink( delBuf );
        }
        g_dir_close( pDir );
    }
#endif      //  #ifdef WIN32

    //*****************
    Reference <XStorable> xStorable( gcomponent, UNO_QUERY );
    if( xStorable == NULL || !xStorable.is( ) )
        return;

    //  Isolate just the name of the file from the full path we were passed
    char    bufName[ 256 ];
    char    *pNameOnly  = strrchr( gpFN, '/' );
    char    *pExt       = NULL;
    if( !pNameOnly )
        pNameOnly = strrchr( gpFN, '\\' );
    if( pNameOnly )
    {
        pNameOnly++;    //  Skip over the slash character
        pExt = strrchr( pNameOnly, '.' );
    }
    if( pExt )
    {
        strncpy( bufName, pNameOnly, pExt - pNameOnly );
        bufName[ pExt - pNameOnly ] = 0;
    }
    else
        strcpy( bufName, "outfile" );
    strcat( bufName, ".PDF" );

    //  sURL = Full path of the PDF file to be created
    OUString sURL;
    sURL = sNewTemp + createStr( bufName );

    //  Exporting to PDF consists of giving the proper filter name in the
    //  property "FilterName" With only this, the document will be exported
    //  using the existing PDF export settings (the one used the last time,
    //  or the default if the first time)
    Sequence<PropertyValue> aProps1( 1 );
    aProps1[0].Name = createStr( "FilterName" );
    aProps1[0].Value = makeAny( createStr( "writer_pdf_Export" ) );

    xStorable->storeToURL( sURL, aProps1 );

    int     iRet;

    //  Convert the PDF to a series of high-resolution PNGs - one for each page
    gf4x = true;
    iRet = gsPDF2PNG( bufName );

    //  Count the number of PNGs that were just created
    gNumPages   = 0;
#ifdef WIN32
    findBuf[ strlen( findBuf ) - 1 ] = 0;
    strcat( findBuf, "png" );
    lFind = (long) _findfirst( findBuf, &findData );
    if( lFind != -1L )
    {
        gNumPages++;
        while( _findnext( lFind, &findData )  ==  0 )
            gNumPages++;
        _findclose( lFind );
    }
#else
    pDir = g_dir_open( findBuf, 0, NULL );
    if( pDir )
    {
        const char  *pName;
        while( (pName = g_dir_read_name( pDir ))  != NULL )
        {
            if( strcmp( pName + (strlen( pName ) - 4), ".png" ) == 0 )
                gNumPages++;
        }
        g_dir_close( pDir );
    }
#endif      //  #ifdef WIN32

    //  Convert the PDF to a series of medium-resolution PNGs - one for each page
    gf4x = false;
    iRet = gsPDF2PNG( bufName );
    if( iRet  !=  0 )
    {
        gfAbort = true;
        return;
    }

    if( !gfAbort )
    {
        //  Create the index file
        OStringBuffer   indexText;
        indexText.append( "<index>\n" );

        int     i;
        for( i=0; i<gNumPages; i++ )
        {
            char    bufPNGhi[ 100 ];
            sprintf( bufPNGhi, "Page-%06dhi.png", i+1 );
            char    bufPNGlo[ 100 ];
            sprintf( bufPNGlo, "Page-%06d.png", i+1 );

            char    bufT[ 300 ];
            sprintf( bufT, "<page title=\"Slide %d\" file=\"%s\" altfile=\"%s\" scale=\"2.0\" altscale=\"1.0\" fittowidth=\"T\"/>\n", i, bufPNGhi, bufPNGlo );
            indexText.append( bufT );

            if( gfAbort )
                break;
        }

        indexText.append( "</index>\n" );

        if( !gfAbort )
        {
            FILE    *f;
#ifndef WIN32
            strcpy( findBuf, goBuf+7 );     //  The +7 skips over the "file:///" prefix
            strcat( findBuf, "index.xml" );
            f = fopen( findBuf, "w" );
#else
            strcpy( findBuf, goBuf+8 );     //  The +8 skips over the "file:///" prefix
            strcat( findBuf, "index.xml" );
            f = fopen( findBuf, "w" );
#endif
            fputs( indexText, f );
            fclose( f );
        }
    }

    return;
}


//*****************************************************************************
static void unloadDS( )
{
    //  Dispose of the local service manager
    try
    {
        Reference< XComponent >::query( xMCFClient )->dispose( );
    }
    catch ( Exception )
    {
//        return;
    }

    mxToolkit.clear( );
    xMCFClient.clear( );
    gcomponent.clear( );
    oomgrContext.clear( );
    oomgr.clear( );
    gmyWinPeer.clear( );
    gmyWin.clear( );
    myMSF.clear( );

    mxToolkit = NULL;
    xMCFClient = NULL;
    gcomponent = NULL;
    oomgrContext = NULL;
    oomgr = NULL;
    gmyWinPeer = NULL;
    gmyWin = NULL;
    myMSF = NULL;

    return;
}

#ifdef WIN32
#define THREAD_RET(x) return (x);
#else
#define THREAD_RET(x) return NULL;
#endif

//*****************************************************************************
#ifdef WIN32
unsigned long WINAPI ThreadFunc( LPVOID lpParam )
#else
gpointer ThreadFunc( gpointer lpParam )
#endif
{
    int     i;

    bootContext = ::cppu::defaultBootstrap_InitialComponentContext( );

    xMCFClient = bootContext->getServiceManager( );

    // Create an instance of a component which supports the services specified by the factory.
    OUString sUURString(RTL_CONSTASCII_USTRINGPARAM("com.sun.star.bridge.UnoUrlResolver"));
    Reference <XInterface> xInterface;
    xInterface = xMCFClient->createInstanceWithContext( sUURString, bootContext );

    Reference <XUnoUrlResolver> resolver(xInterface, UNO_QUERY);
    if( resolver == NULL )
    {
        dsStatus = 666;
        THREAD_RET( 1 );
    }
    if( !resolver.is( ) )
    {
        dsStatus = 666;
        THREAD_RET( 1 );
    }

    // Resolve the component context from the office using the fixed URL
    OUString sConnectionString( RTL_CONSTASCII_USTRINGPARAM("uno:socket,host=localhost,port=2083;urp;StarOffice.ServiceManager"));
    try
    {
        xInterface = Reference <XInterface>( resolver->resolve( sConnectionString ), UNO_QUERY );
    }
    catch ( Exception )
    {
        dsStatus = 666;
        THREAD_RET( 1 );
    }

    if( gfAbort )
    {
        dsStatus = 666;
        THREAD_RET( 1 );
    }
    myMSF = Reference <XMultiServiceFactory>(xInterface, UNO_QUERY);

    //  This is the "Legal Solution" described on page 466 of the Developer's Guide
    OUString strTK( RTL_CONSTASCII_USTRINGPARAM( "com.sun.star.awt.Toolkit" ));
    Reference <XToolkit> myToolkit( myMSF->createInstance( strTK ), UNO_QUERY );

    if( gfAbort  ||  !myToolkit.is( ) )
    {
        dsStatus = 666;
        THREAD_RET( 1 );
    }

    ::com::sun::star::awt::WindowDescriptor aDescriptor;
    aDescriptor.Type = ::com::sun::star::awt::WindowClass_TOP ;
    aDescriptor.WindowServiceName = createStr("window");
    aDescriptor.ParentIndex = -1;
    aDescriptor.Parent = NULL;
    ::com::sun::star::awt::Rectangle rect(0,0,0,0);
    aDescriptor.Bounds = rect;
    aDescriptor.WindowAttributes = ::com::sun::star::awt::WindowAttribute::BORDER |
                                    ::com::sun::star::awt::WindowAttribute::MOVEABLE |
                                    ::com::sun::star::awt::WindowAttribute::SIZEABLE |
                                    ::com::sun::star::awt::WindowAttribute::CLOSEABLE ;

    try
    {
        gmyWinPeer = myToolkit->createWindow( aDescriptor );
    }
    catch ( Exception )
    {
        dsStatus = 666;
        THREAD_RET( 33 );
    }

    if( gfAbort  ||  !gmyWinPeer.is( ) )
    {
        dsStatus = 666;
        THREAD_RET( 4 );
    }

    gmyWin = Reference <XWindow>( gmyWinPeer, UNO_QUERY );
    if( !gmyWin.is( ) )
    {
        dsStatus = 666;
        THREAD_RET( 5 );
    }

    gmyWinPeer->setBackground( 0x00808080 );
    gmyWin->setVisible( sal_True );

#ifdef WIN32
    RECT    aRect;
    GetClientRect( (HWND) gulWnd, &aRect );
    gmyWin->setPosSize( 0, 0, aRect.right, aRect.bottom, 15 );
#endif

    OUString strXFrame( RTL_CONSTASCII_USTRINGPARAM( "com.sun.star.frame.Frame" ));
    Reference<XFrame>         xFrame( myMSF->createInstance( strXFrame ), UNO_QUERY );

    xFrame->initialize( gmyWin );

    // get frames supplier
    OUString    strDesktop( RTL_CONSTASCII_USTRINGPARAM("com.sun.star.frame.Desktop" ) );
    Reference< XFramesSupplier > xFramesSupplier( myMSF->createInstance( strDesktop ), UNO_QUERY );
    if( gfAbort  ||  !xFramesSupplier.is() )
    {
        dsStatus = 666;
        THREAD_RET( sal_False );
    }

    // get frames
    Reference <XFrames> xFrames( xFramesSupplier->getFrames(), UNO_QUERY );
    if ( !xFrames.is() )
    {
        dsStatus = 666;
        THREAD_RET( sal_False );
    }

    // append m_xFrame to m_xFrames
    xFrames->append( xFrame );

    OUString  sOldName = xFrame->getName( );
    OUString  sNewName( RTL_CONSTASCII_USTRINGPARAM( "myNewFrame" ) );
    xFrame->setName( sNewName );
    OUString  sCheckName = xFrame->getName( );

    // gets the server component context as property of the office component factory
    oomgrContext = ::cppu::defaultBootstrap_InitialComponentContext( );
    Reference <XPropertySet> mgrPropSet( xInterface, UNO_QUERY );
    mgrPropSet->getPropertyValue( createStr("DefaultContext") ) >>= oomgrContext;

    // gets the service manager from the office
    oomgr = oomgrContext->getServiceManager( );

    //**********************
    //**** CONNECTED !! ****
    //**********************
    Reference <XComponentLoader> comploader(oomgr->createInstanceWithContext(
        strDesktop,
        oomgrContext ), UNO_QUERY );

    //  Loads a component specified by an URL into the specified new or existing frame.
    OUString    sAbsoluteDocUrl, sWorkingDir, sDocPathUrl;
    osl_getProcessWorkingDir( &sWorkingDir.pData );

    if( osl::FileBase::getFileURLFromSystemPath( OUString::createFromAscii( gpFN ), sDocPathUrl)  !=  osl::FileBase::E_None )
    {
        dsStatus = 666;
        THREAD_RET( -2 );
    }

    if( gfAbort  ||  sDocPathUrl.getLength( ) <=  0 )
    {
        dsStatus = 666;
        THREAD_RET( -3 );
    }

    if( osl::FileBase::getAbsoluteFileURL( sWorkingDir, sDocPathUrl, sAbsoluteDocUrl)  !=  osl::FileBase::E_None )
    {
        dsStatus = 666;
        THREAD_RET( -4 );
    }

    if( sAbsoluteDocUrl.getLength( ) <=  0 )
    {
        dsStatus = 666;
        THREAD_RET( -5 );
    }

    for( i=0; i<sAbsoluteDocUrl.getLength( ); i++ )
        goBuf[ i ] = (char) sAbsoluteDocUrl.pData->buffer[ i ];
    goBuf[ i ] = 0;

#ifndef WIN32
    char    myBuf[ 1000 ];
    for( i=0; i<sAbsoluteDocUrl.getLength( ); i++ )
        myBuf[ i ] = (char) sAbsoluteDocUrl.pData->buffer[ i ];
    myBuf[ i ] = 0;

    OUString    sTemp1( RTL_CONSTASCII_USTRINGPARAM( myBuf+7 ) );
    OUString    sTemp2( createStr( myBuf+7 ) );
    OUString    sOutPath( sAbsoluteDocUrl );
    sOutPath += sAbsoluteDocUrl;
#endif

    try
    {
#ifndef WIN32
        if( osl_searchFileURL( sAbsoluteDocUrl.pData, NULL, &sOutPath.pData)  !=  osl_File_E_None )
#else
        if( osl_searchFileURL( sAbsoluteDocUrl.pData, sWorkingDir.pData, &sAbsoluteDocUrl.pData)  !=  osl_File_E_None )
#endif
        {
#ifdef WIN32
            MessageBox( NULL, goBuf, "! ! ! NOT - Found File - NOT ! ! !", MB_OK );
#endif
//            dsStatus = 666;
//            THREAD_RET( -6 );
        }
    }
    catch ( Exception )
    {
        dsStatus = 666;
        THREAD_RET( -7 );
    }

    try
    {
        gcomponent = comploader->loadComponentFromURL(
            sAbsoluteDocUrl, sNewName, com::sun::star::frame::FrameSearchFlag::GLOBAL,
            Sequence < ::com::sun::star::beans::PropertyValue >() );
    }
    catch ( Exception )
    {
        dsStatus = 666;
        THREAD_RET( -7 );
    }
    if( gfAbort  ||  gcomponent == NULL  ||  !gcomponent.is( ) )
    {
        dsStatus = 666;
        THREAD_RET( 1 );
    }

    //  Export each page as a separate PNG image
    exportAllPagesAsPNG( );

    //  Dispose of the local service manager
    unloadDS( );

#ifdef WIN32
    PostMessage( (HWND) gulWnd, WM_CLOSE, 0, 0 );
#endif

    dsStatus = 666;
    THREAD_RET( i );
}


//*****************************************************************************
OO_WRITER_API int dsPreLoad( )
{
    gfAbort = false;
    dsStatus = 666;     //  Indicate we have completed our work
    return 0;
}


//*****************************************************************************
OO_WRITER_API int dsLoad( char *pFN, unsigned long ulWnd )
{
    dsStatus = 0;
    gfAbort = false;
    gulWnd = ulWnd;
    if( gpFN )
        free( gpFN );
    gpFN = (char *) malloc( strlen( pFN ) + 1 );
    strcpy( gpFN, pFN );

#ifdef WIN32
    unsigned long   dwThreadParam   = 1;
    unsigned long   dwThreadID      = 1;
    CreateThread( NULL, 0, ThreadFunc, &dwThreadParam, 0, &dwThreadID );

#else
    unsigned long   dwThreadParam   = 1;

    g_thread_create( ThreadFunc, &dwThreadParam, false, NULL );

#endif      //  #ifdefWIN32

    return 1;
}


//*****************************************************************************
OO_WRITER_API int dsPostLoad( )
{
    gfAbort = false;
    if( gpFN )
        free( gpFN );
    gpFN = NULL;

    dsStatus = 666;     //  Indicate we have completed our work
    return 0;
}

#else       //  #ifdef _COM_SUN_STAR_BRIDGE_XUNOURLRESOLVER_HPP_

#error "No connection to UNO possible, necessary XUnoUrlResolver not included properly"

#endif
