erase .\oo_writer.urd

"%OO_SDK_HOME%/windows/bin/idlc" -I. -I"%OO_SDK_HOME%/idl" -O. ./oo_writer.idl

erase oo_writer.rdb

"%OO_SDK_HOME%/windows/bin/regmerge" oo_writer.rdb /UCR ./oo_writer.urd
"%OO_SDK_HOME%/windows/bin/regmerge" oo_writer.rdb / "%OFFICE_HOME%\program\types.rdb"
"%OO_SDK_HOME%/windows/bin/regmerge" oo_writer.rdb / "%OFFICE_HOME%\program\services.rdb"

set JEP0=-O./inc
set JEP1=-Tcom.sun.star.lang.XMultiServiceFactory -Tcom.sun.star.lang.XComponent -Tcom.sun.star.beans.XPropertySet -Tcom.sun.star.document.XExporter -Tcom.sun.star.document.XFilter
set JEP2=-Tcom.sun.star.bridge.UnoUrlResolver -Tcom.sun.star.bridge.XUnoUrlResolver -Tcom.sun.star.frame.XComponentLoader -Tcom.sun.star.lang.XMultiComponentFactory -Tcom.sun.star.frame.FrameSearchFlag -Tcom.sun.star.frame.XStorable
set JEP3=-Tcom.sun.star.container.XHierarchicalNameAccess -Tcom.sun.star.registry.XSimpleRegistry -Tcom.sun.star.lang.SystemDependent

set JEP4=-Tcom.sun.star.presentation.XPresentation -Tcom.sun.star.presentation.XPresentationSupplier -Tcom.sun.star.presentation.PresentationView
set JEP5=-Tcom.sun.star.container.XNamed -Tcom.sun.star.drawing.* -Tcom.sun.star.document.XDocumentInfoSupplier -Tcom.sun.star.awt.XMessageBox
set JEP6=-Tcom.sun.star.awt.XToolkit -Tcom.sun.star.awt.XSystemChildFactory -Tcom.sun.star.awt.Toolkit -Tcom.sun.star.awt.WindowAttribute

"%OO_SDK_HOME%\windows\bin\cppumaker" -Gc -BUCR %JEP0% %JEP1% %JEP2% %JEP3% %JEP4% %JEP5% %JEP6% oo_writer.rdb
