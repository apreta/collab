//#include <wx/wx.h>
//
//
//#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
//#include <windows.h>
//
//#ifdef BUILD_DLL
//#define DLL_EXPORT __declspec(dllexport)
//#else
//#define DLL_EXPORT
//#endif



enum        //  wdDocumentDirection
{
    wdLeftToRight   = 0,  // Left to right.
    wdRightToLeft   = 1   // Right to left.
};

enum        //  wdOpenFormat
{
    wdOpenFormatAuto                    =  0,  // The existing format.
    wdOpenFormatDocument                =  1,  // Word format.
    wdOpenFormatDocument97              =  1,  // Microsoft Word 97 document format.
    wdOpenFormatTemplate97              =  2,  // Word 97 template format.
    wdOpenFormatTemplate                =  2,  // As a Word template.
    wdOpenFormatRTF                     =  3,  // Rich text format (RTF).
    wdOpenFormatText                    =  4,  // Unencoded text format.
    wdOpenFormatEncodedText             =  5,  // Encoded text format.
    wdOpenFormatUnicodeText             =  5,  // Unicode text format.
    wdOpenFormatAllWord                 =  6,  // A Microsoft Office Word format that is backward compatible with earlier versions of Word.
    wdOpenFormatWebPages                =  7,  // HTML format.
    wdOpenFormatXML                     =  8,  // XML format.
    wdOpenFormatXMLDocument             =  9,  // XML document format.
    wdOpenFormatXMLDocumentMacroEnabled = 10,  // XML document format with macros enabled.
    wdOpenFormatXMLTemplate             = 11,  // XML template format.
    wdOpenFormatXMLTemplateMacroEnabled = 12,  // XML template format with macros enabled.
    wdOpenFormatAllWordTemplates        = 13   // Word template format.
};

enum        //  WdPrintOutItem
{
    wdPrintDocumentContent     = 0,  // Current document content.
    wdPrintProperties          = 1,  // Properties in the current document.
    wdPrintMarkup              = 2,  // Markup in the current document.
    wdPrintComments            = 2,  // Comments in the current document.
    wdPrintStyles              = 3,  // Styles in the current document.
    wdPrintAutoTextEntries     = 4,  // Autotext entries in the current document.
    wdPrintKeyAssignments      = 5,  // Key assignments in the current document.
    wdPrintEnvelope            = 6,  // An envelope.
    wdPrintDocumentWithMarkup  = 7   // Current document content including markup.
};

enum        //  WdPrintOutPages
{
    wdPrintAllPages        = 0,  // All pages.
    wdPrintOddPagesOnly    = 1,  // Odd-numbered pages only.
    wdPrintEvenPagesOnly   = 2   // Even-numbered pages only.
};

enum        //  WdPrintOutRange
{
    wdPrintAllDocument  = 0,  // The entire document.
    wdPrintSelection    = 1,  // The current selection.
    wdPrintCurrentPage  = 2,  // The current page.
    wdPrintFromTo       = 3,  // A specified range.
    wdPrintRangeOfPages = 4   // A specified range of pages.
};


enum        //  WdSaveFormat
{
    wdFormatDocument                =  0, //  Microsoft Office Word format.
    wdFormatDocument97              =  0, //  Microsoft Word 97 document format.
    wdFormatTemplate                =  1, //  Word template format.
    wdFormatTemplate97              =  1, //  Word 97 template format.
    wdFormatText                    =  2, //  Microsoft Windows text format.
    wdFormatTextLineBreaks          =  3, //  Windows text format with line breaks preserved.
    wdFormatDOSText                 =  4, //  Microsoft DOS text format.
    wdFormatDOSTextLineBreaks       =  5, //  Microsoft DOS text with line breaks preserved.
    wdFormatRTF                     =  6, //  Rich text format (RTF).
    wdFormatEncodedText             =  7, //  Encoded text format.
    wdFormatUnicodeText             =  7, //  Unicode text format.
    wdFormatHTML                    =  8, //  Standard HTML format.
    wdFormatWebArchive              =  9, //  Web archive format.
    wdFormatFilteredHTML            = 10, //  Filtered HTML format.
    wdFormatXML                     = 11, //  Extensible Markup Language (XML) format.
    wdFormatXMLDocument             = 12, //  XML document format.
    wdFormatXMLDocumentMacroEnabled = 13, //  XML document format with macros enabled.
    wdFormatXMLTemplate             = 14, //  XML template format.
    wdFormatXMLTemplateMacroEnabled = 15, //  XML template format with macros enabled.
    wdFormatDocumentDefault         = 16, //  Word default document file format. For Microsoft Office Word 2007, this is the DOCX format.
    wdFormatPDF                     = 17, //  PDF format.
    wdFormatXPS                     = 18 //  XPS format.
};


// parameter types: by value VTs
#define VTS_I2              "\x02"      // a 'short'
#define VTS_I4              "\x03"      // a 'long'
#define VTS_R4              "\x04"      // a 'float'
#define VTS_R8              "\x05"      // a 'double'
#define VTS_CY              "\x06"      // a 'CY' or 'CY*'
#define VTS_DATE            "\x07"      // a 'DATE'
#define VTS_WBSTR           "\x08"      // an 'LPCOLESTR'
#define VTS_DISPATCH        "\x09"      // an 'IDispatch*'
#define VTS_SCODE           "\x0A"      // an 'SCODE'
#define VTS_BOOL            "\x0B"      // a 'VARIANT_BOOL'
#define VTS_VARIANT         "\x0C"      // a 'const VARIANT&' or 'VARIANT*'
#define VTS_UNKNOWN         "\x0D"      // an 'IUnknown*'
#if defined(_UNICODE)
#define VTS_BSTR            VTS_WBSTR// an 'LPCOLESTR'
#define VT_BSTRT            VT_BSTR
#else
#define VTS_BSTR            "\x0E"  // an 'LPCSTR'
#define VT_BSTRA            14
#define VT_BSTRT            VT_BSTRA
#endif
#define VTS_I1              "\x10"      // a 'signed char'
#define VTS_UI1             "\x11"      // a 'BYTE'
#define VTS_UI2             "\x12"      // a 'WORD'
#define VTS_UI4             "\x13"      // a 'DWORD'
#define VTS_I8              "\x14"      // a 'LONGLONG'
#define VTS_UI8             "\x15"      // a 'ULONGLONG'


// parameter types: by reference VTs
#define VTS_PI2             "\x42"      // a 'short*'
#define VTS_PI4             "\x43"      // a 'long*'
#define VTS_PR4             "\x44"      // a 'float*'
#define VTS_PR8             "\x45"      // a 'double*'
#define VTS_PCY             "\x46"      // a 'CY*'
#define VTS_PDATE           "\x47"      // a 'DATE*'
#define VTS_PBSTR           "\x48"      // a 'BSTR*'
#define VTS_PDISPATCH       "\x49"      // an 'IDispatch**'
#define VTS_PSCODE          "\x4A"      // an 'SCODE*'
#define VTS_PBOOL           "\x4B"      // a 'VARIANT_BOOL*'
#define VTS_PVARIANT        "\x4C"      // a 'VARIANT*'
#define VTS_PUNKNOWN        "\x4D"      // an 'IUnknown**'
#define VTS_PI1             "\x50"      // a 'signed char*'
#define VTS_PUI1            "\x51"      // a 'BYTE*'
#define VTS_PUI2            "\x52"      // a 'WORD*'
#define VTS_PUI4            "\x53"      // a 'DWORD*'
#define VTS_PI8             "\x54"      // a 'LONGLONG*'
#define VTS_PUI8            "\x55"      // a 'ULONGLONG*'

// special VT_ and VTS_ values
#define VTS_NONE            NULL        // used for members with 0 params
#define VT_MFCVALUE         0xFFF       // special value for DISPID_VALUE
#define VT_MFCBYREF         0x40        // indicates VT_BYREF type
#define VT_MFCMARKER        0xFF        // delimits named parameters (INTERNAL USE)




//***********************************************************************
// helper(s) for reliable and small QueryInterface calls
//  from oleimpl2.h
//LPUNKNOWN AFXAPI _AfxQueryInterface(LPUNKNOWN lpUnknown, REFIID riid);
#define QUERYINTERFACE(lpUnknown, iface) \
    (iface*)_AfxQueryInterface(lpUnknown, IID_##iface)


//***********************************************************************
// helper for reliable and small Release calls
//  from oleimpl2.h
//DWORD AFXAPI _AfxRelease(LPUNKNOWN* plpUnknown);
DWORD _AfxRelease(LPUNKNOWN* plpUnknown);
#ifndef _DEBUG
// generate smaller code in release build
#define RELEASE(lpUnk) _AfxRelease((LPUNKNOWN*)&lpUnk)
#else
// generate larger but typesafe code in debug build
#define RELEASE(lpUnk) do \
                           { if ((lpUnk) != NULL) { (lpUnk)->Release(); (lpUnk) = NULL; } } while (0)
#endif


//***********************************************************************
typedef LPVOID* LPLP;           //  from oleimpl2.h




//***********************************************************************
class myDispatchDriver
{
public:
    myDispatchDriver( )     { m_lpDispatch = NULL; m_bAutoRelease = false; };
    myDispatchDriver( LPDISPATCH lpDispatch, bool bAutoRelease = true);
//    myDispatchDriver( const myDispatchDriver &dispatchSrc );


public:
//    bool    CreateDispatch( LPCTSTR lpszProgID, COleException* pError);

    void    InvokeHelperV( DISPID dwDispID, WORD wFlags,
                           VARTYPE vtRet, void* pvRet, const BYTE* pbParamInfo, va_list argList);
    void    InvokeHelper( DISPID dwDispID, WORD wFlags,
                          VARTYPE vtRet, void* pvRet, const BYTE* pbParamInfo, ...);
//    bool    CreateDispatch( REFCLSID clsid, COleException* pError );
//    bool    CreateDispatch( LPCTSTR lpszProgID, COleException* pError = NULL );


// Attributes
    LPDISPATCH  m_lpDispatch;
    BOOL        m_bAutoRelease;

// Operations
    bool    CreateDispatch2( REFCLSID clsid );
    bool    CreateDispatch( LPCTSTR lpszProgID );

    void    ReleaseDispatch( );

};


//*****************************************************************************
// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

// CApplication wrapper class

class _Application : public myDispatchDriver
{
public:
	_Application(){} // Calls myDispatchDriver default constructor
//	_Application(LPDISPATCH pDispatch) : myDispatchDriver(pDispatch) {}
//	_Application(const CApplication& dispatchSrc) : myDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// _Application methods
public:
/*
    LPDISPATCH get_Application()
	{
		LPDISPATCH result;
		InvokeHelper(0x3e8, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	long get_Creator()
	{
		long result;
		InvokeHelper(0x3e9, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Parent()
	{
		LPDISPATCH result;
		InvokeHelper(0x3ea, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	CString get_Name()
	{
		CString result;
		InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
    */
	LPDISPATCH get_Documents()
	{
		LPDISPATCH result;
		InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
    }
    /*
	LPDISPATCH get_Windows()
	{
		LPDISPATCH result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_ActiveDocument()
	{
		LPDISPATCH result;
		InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_ActiveWindow()
	{
		LPDISPATCH result;
		InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Selection()
	{
		LPDISPATCH result;
		InvokeHelper(0x5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_WordBasic()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_RecentFiles()
	{
		LPDISPATCH result;
		InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_NormalTemplate()
	{
		LPDISPATCH result;
		InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_System()
	{
		LPDISPATCH result;
		InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_AutoCorrect()
	{
		LPDISPATCH result;
		InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_FontNames()
	{
		LPDISPATCH result;
		InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_LandscapeFontNames()
	{
		LPDISPATCH result;
		InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_PortraitFontNames()
	{
		LPDISPATCH result;
		InvokeHelper(0xd, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Languages()
	{
		LPDISPATCH result;
		InvokeHelper(0xe, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Assistant()
	{
		LPDISPATCH result;
		InvokeHelper(0xf, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Browser()
	{
		LPDISPATCH result;
		InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_FileConverters()
	{
		LPDISPATCH result;
		InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_MailingLabel()
	{
		LPDISPATCH result;
		InvokeHelper(0x12, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Dialogs()
	{
		LPDISPATCH result;
		InvokeHelper(0x13, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_CaptionLabels()
	{
		LPDISPATCH result;
		InvokeHelper(0x14, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_AutoCaptions()
	{
		LPDISPATCH result;
		InvokeHelper(0x15, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_AddIns()
	{
		LPDISPATCH result;
		InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_Visible()
	{
		BOOL result;
		InvokeHelper(0x17, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
    */
	void put_Visible(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x17, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
    }
    wxString get_Version()
	{
		wxString result;
		InvokeHelper(0x18, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
    /*
	BOOL get_ScreenUpdating()
	{
		BOOL result;
		InvokeHelper(0x1a, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_ScreenUpdating(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x1a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_PrintPreview()
	{
		BOOL result;
		InvokeHelper(0x1b, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_PrintPreview(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x1b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_Tasks()
	{
		LPDISPATCH result;
		InvokeHelper(0x1c, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_DisplayStatusBar()
	{
		BOOL result;
		InvokeHelper(0x1d, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_DisplayStatusBar(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x1d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_SpecialMode()
	{
		BOOL result;
		InvokeHelper(0x1e, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	long get_UsableWidth()
	{
		long result;
		InvokeHelper(0x21, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_UsableHeight()
	{
		long result;
		InvokeHelper(0x22, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	BOOL get_MathCoprocessorAvailable()
	{
		BOOL result;
		InvokeHelper(0x24, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_MouseAvailable()
	{
		BOOL result;
		InvokeHelper(0x25, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	VARIANT get_International(long Index)
	{
		VARIANT result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x2e, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, parms, Index);
		return result;
	}
	CString get_Build()
	{
		CString result;
		InvokeHelper(0x2f, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	BOOL get_CapsLock()
	{
		BOOL result;
		InvokeHelper(0x30, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_NumLock()
	{
		BOOL result;
		InvokeHelper(0x31, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	CString get_UserName()
	{
		CString result;
		InvokeHelper(0x34, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_UserName(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x34, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_UserInitials()
	{
		CString result;
		InvokeHelper(0x35, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_UserInitials(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x35, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_UserAddress()
	{
		CString result;
		InvokeHelper(0x36, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_UserAddress(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x36, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_MacroContainer()
	{
		LPDISPATCH result;
		InvokeHelper(0x37, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_DisplayRecentFiles()
	{
		BOOL result;
		InvokeHelper(0x38, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_DisplayRecentFiles(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x38, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_CommandBars()
	{
		LPDISPATCH result;
		InvokeHelper(0x39, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_SynonymInfo(LPCTSTR Word, VARIANT * LanguageID)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_PVARIANT ;
		InvokeHelper(0x3b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms, Word, LanguageID);
		return result;
	}
	LPDISPATCH get_VBE()
	{
		LPDISPATCH result;
		InvokeHelper(0x3d, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	CString get_DefaultSaveFormat()
	{
		CString result;
		InvokeHelper(0x40, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_DefaultSaveFormat(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x40, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_ListGalleries()
	{
		LPDISPATCH result;
		InvokeHelper(0x41, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
    */
    wxString get_ActivePrinter( )
	{
        wxString result;
		InvokeHelper(0x42, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
    }
	void put_ActivePrinter(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x42, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
    /*
	LPDISPATCH get_Templates()
	{
		LPDISPATCH result;
		InvokeHelper(0x43, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_CustomizationContext()
	{
		LPDISPATCH result;
		InvokeHelper(0x44, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void put_CustomizationContext(LPDISPATCH newValue)
	{
		static BYTE parms[] = VTS_DISPATCH ;
		InvokeHelper(0x44, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_KeyBindings()
	{
		LPDISPATCH result;
		InvokeHelper(0x45, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_KeysBoundTo(long KeyCategory, LPCTSTR Command, VARIANT * CommandParameter)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_PVARIANT ;
		InvokeHelper(0x46, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms, KeyCategory, Command, CommandParameter);
		return result;
	}
	LPDISPATCH get_FindKey(long KeyCode, VARIANT * KeyCode2)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_I4 VTS_PVARIANT ;
		InvokeHelper(0x47, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms, KeyCode, KeyCode2);
		return result;
	}
	CString get_Caption()
	{
		CString result;
		InvokeHelper(0x50, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_Caption(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x50, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_Path()
	{
		CString result;
		InvokeHelper(0x51, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	BOOL get_DisplayScrollBars()
	{
		BOOL result;
		InvokeHelper(0x52, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_DisplayScrollBars(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x52, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_StartupPath()
	{
		CString result;
		InvokeHelper(0x53, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_StartupPath(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x53, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_BackgroundSavingStatus()
	{
		long result;
		InvokeHelper(0x55, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_BackgroundPrintingStatus()
	{
		long result;
		InvokeHelper(0x56, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_Left()
	{
		long result;
		InvokeHelper(0x57, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Left(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x57, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_Top()
	{
		long result;
		InvokeHelper(0x58, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Top(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x58, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_Width()
	{
		long result;
		InvokeHelper(0x59, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Width(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x59, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_Height()
	{
		long result;
		InvokeHelper(0x5a, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Height(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x5a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_WindowState()
	{
		long result;
		InvokeHelper(0x5b, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_WindowState(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x5b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_DisplayAutoCompleteTips()
	{
		BOOL result;
		InvokeHelper(0x5c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_DisplayAutoCompleteTips(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x5c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_Options()
	{
		LPDISPATCH result;
		InvokeHelper(0x5d, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	long get_DisplayAlerts()
	{
		long result;
		InvokeHelper(0x5e, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_DisplayAlerts(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x5e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_CustomDictionaries()
	{
		LPDISPATCH result;
		InvokeHelper(0x5f, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	CString get_PathSeparator()
	{
		CString result;
		InvokeHelper(0x60, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_StatusBar(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x61, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_MAPIAvailable()
	{
		BOOL result;
		InvokeHelper(0x62, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_DisplayScreenTips()
	{
		BOOL result;
		InvokeHelper(0x63, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_DisplayScreenTips(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x63, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_EnableCancelKey()
	{
		long result;
		InvokeHelper(0x64, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_EnableCancelKey(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x64, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_UserControl()
	{
		BOOL result;
		InvokeHelper(0x65, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_FileSearch()
	{
		LPDISPATCH result;
		InvokeHelper(0x67, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	long get_MailSystem()
	{
		long result;
		InvokeHelper(0x68, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	CString get_DefaultTableSeparator()
	{
		CString result;
		InvokeHelper(0x69, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_DefaultTableSeparator(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x69, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_ShowVisualBasicEditor()
	{
		BOOL result;
		InvokeHelper(0x6a, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_ShowVisualBasicEditor(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x6a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_BrowseExtraFileTypes()
	{
		CString result;
		InvokeHelper(0x6c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_BrowseExtraFileTypes(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x6c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_IsObjectValid(LPDISPATCH Object)
	{
		BOOL result;
		static BYTE parms[] = VTS_DISPATCH ;
		InvokeHelper(0x6d, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, parms, Object);
		return result;
	}
	LPDISPATCH get_HangulHanjaDictionaries()
	{
		LPDISPATCH result;
		InvokeHelper(0x6e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_MailMessage()
	{
		LPDISPATCH result;
		InvokeHelper(0x15c, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_FocusInMailHeader()
	{
		BOOL result;
		InvokeHelper(0x182, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void Quit(VARIANT * SaveChanges, VARIANT * OriginalFormat, VARIANT * RouteDocument)
	{
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x451, DISPATCH_METHOD, VT_EMPTY, NULL, parms, SaveChanges, OriginalFormat, RouteDocument);
	}
	void ScreenRefresh()
	{
		InvokeHelper(0x12d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void PrintOutOld(VARIANT * Background, VARIANT * Append, VARIANT * Range, VARIANT * OutputFileName, VARIANT * From, VARIANT * To, VARIANT * Item, VARIANT * Copies, VARIANT * Pages, VARIANT * PageType, VARIANT * PrintToFile, VARIANT * Collate, VARIANT * FileName, VARIANT * ActivePrinterMacGX, VARIANT * ManualDuplexPrint)
	{
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x12e, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Background, Append, Range, OutputFileName, From, To, Item, Copies, Pages, PageType, PrintToFile, Collate, FileName, ActivePrinterMacGX, ManualDuplexPrint);
	}
	void LookupNameProperties(LPCTSTR Name)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x12f, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Name);
	}
	void SubstituteFont(LPCTSTR UnavailableFont, LPCTSTR SubstituteFont)
	{
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x130, DISPATCH_METHOD, VT_EMPTY, NULL, parms, UnavailableFont, SubstituteFont);
	}
	BOOL Repeat(VARIANT * Times)
	{
		BOOL result;
		static BYTE parms[] = VTS_PVARIANT ;
		InvokeHelper(0x131, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, Times);
		return result;
	}
	void DDEExecute(long Channel, LPCTSTR Command)
	{
		static BYTE parms[] = VTS_I4 VTS_BSTR ;
		InvokeHelper(0x136, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Channel, Command);
	}
	long DDEInitiate(LPCTSTR App, LPCTSTR Topic)
	{
		long result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x137, DISPATCH_METHOD, VT_I4, (void*)&result, parms, App, Topic);
		return result;
	}
	void DDEPoke(long Channel, LPCTSTR Item, LPCTSTR Data)
	{
		static BYTE parms[] = VTS_I4 VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x138, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Channel, Item, Data);
	}
	CString DDERequest(long Channel, LPCTSTR Item)
	{
		CString result;
		static BYTE parms[] = VTS_I4 VTS_BSTR ;
		InvokeHelper(0x139, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms, Channel, Item);
		return result;
	}
	void DDETerminate(long Channel)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x13a, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Channel);
	}
	void DDETerminateAll()
	{
		InvokeHelper(0x13b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	long BuildKeyCode(long Arg1, VARIANT * Arg2, VARIANT * Arg3, VARIANT * Arg4)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x13c, DISPATCH_METHOD, VT_I4, (void*)&result, parms, Arg1, Arg2, Arg3, Arg4);
		return result;
	}
	CString KeyString(long KeyCode, VARIANT * KeyCode2)
	{
		CString result;
		static BYTE parms[] = VTS_I4 VTS_PVARIANT ;
		InvokeHelper(0x13d, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms, KeyCode, KeyCode2);
		return result;
	}
	void OrganizerCopy(LPCTSTR Source, LPCTSTR Destination, LPCTSTR Name, long Object)
	{
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 ;
		InvokeHelper(0x13e, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Source, Destination, Name, Object);
	}
	void OrganizerDelete(LPCTSTR Source, LPCTSTR Name, long Object)
	{
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_I4 ;
		InvokeHelper(0x13f, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Source, Name, Object);
	}
	void OrganizerRename(LPCTSTR Source, LPCTSTR Name, LPCTSTR NewName, long Object)
	{
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 ;
		InvokeHelper(0x140, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Source, Name, NewName, Object);
	}
	void AddAddress(SAFEARRAY * * TagID, SAFEARRAY * * Value)
	{
		static BYTE parms[] = VTS_UNKNOWN VTS_UNKNOWN ;
		InvokeHelper(0x141, DISPATCH_METHOD, VT_EMPTY, NULL, parms, TagID, Value);
	}
	CString GetAddress(VARIANT * Name, VARIANT * AddressProperties, VARIANT * UseAutoText, VARIANT * DisplaySelectDialog, VARIANT * SelectDialog, VARIANT * CheckNamesDialog, VARIANT * RecentAddressesChoice, VARIANT * UpdateRecentAddresses)
	{
		CString result;
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x142, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms, Name, AddressProperties, UseAutoText, DisplaySelectDialog, SelectDialog, CheckNamesDialog, RecentAddressesChoice, UpdateRecentAddresses);
		return result;
	}
	BOOL CheckGrammar(LPCTSTR String)
	{
		BOOL result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x143, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, String);
		return result;
	}
	BOOL CheckSpelling(LPCTSTR Word, VARIANT * CustomDictionary, VARIANT * IgnoreUppercase, VARIANT * MainDictionary, VARIANT * CustomDictionary2, VARIANT * CustomDictionary3, VARIANT * CustomDictionary4, VARIANT * CustomDictionary5, VARIANT * CustomDictionary6, VARIANT * CustomDictionary7, VARIANT * CustomDictionary8, VARIANT * CustomDictionary9, VARIANT * CustomDictionary10)
	{
		BOOL result;
		static BYTE parms[] = VTS_BSTR VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x144, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, Word, CustomDictionary, IgnoreUppercase, MainDictionary, CustomDictionary2, CustomDictionary3, CustomDictionary4, CustomDictionary5, CustomDictionary6, CustomDictionary7, CustomDictionary8, CustomDictionary9, CustomDictionary10);
		return result;
	}
	void ResetIgnoreAll()
	{
		InvokeHelper(0x146, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	LPDISPATCH GetSpellingSuggestions(LPCTSTR Word, VARIANT * CustomDictionary, VARIANT * IgnoreUppercase, VARIANT * MainDictionary, VARIANT * SuggestionMode, VARIANT * CustomDictionary2, VARIANT * CustomDictionary3, VARIANT * CustomDictionary4, VARIANT * CustomDictionary5, VARIANT * CustomDictionary6, VARIANT * CustomDictionary7, VARIANT * CustomDictionary8, VARIANT * CustomDictionary9, VARIANT * CustomDictionary10)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x147, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Word, CustomDictionary, IgnoreUppercase, MainDictionary, SuggestionMode, CustomDictionary2, CustomDictionary3, CustomDictionary4, CustomDictionary5, CustomDictionary6, CustomDictionary7, CustomDictionary8, CustomDictionary9, CustomDictionary10);
		return result;
	}
	void GoBack()
	{
		InvokeHelper(0x148, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void Help(VARIANT * HelpType)
	{
		static BYTE parms[] = VTS_PVARIANT ;
		InvokeHelper(0x149, DISPATCH_METHOD, VT_EMPTY, NULL, parms, HelpType);
	}
	void AutomaticChange()
	{
		InvokeHelper(0x14a, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void ShowMe()
	{
		InvokeHelper(0x14b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void HelpTool()
	{
		InvokeHelper(0x14c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	LPDISPATCH NewWindow()
	{
		LPDISPATCH result;
		InvokeHelper(0x159, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void ListCommands(BOOL ListAllCommands)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x15a, DISPATCH_METHOD, VT_EMPTY, NULL, parms, ListAllCommands);
	}
	void ShowClipboard()
	{
		InvokeHelper(0x15d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void OnTime(VARIANT * When, LPCTSTR Name, VARIANT * Tolerance)
	{
		static BYTE parms[] = VTS_PVARIANT VTS_BSTR VTS_PVARIANT ;
		InvokeHelper(0x15e, DISPATCH_METHOD, VT_EMPTY, NULL, parms, When, Name, Tolerance);
	}
	void NextLetter()
	{
		InvokeHelper(0x15f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	short MountVolume(LPCTSTR Zone, LPCTSTR Server, LPCTSTR Volume, VARIANT * User, VARIANT * UserPassword, VARIANT * VolumePassword)
	{
		short result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BSTR VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x161, DISPATCH_METHOD, VT_I2, (void*)&result, parms, Zone, Server, Volume, User, UserPassword, VolumePassword);
		return result;
	}
	CString CleanString(LPCTSTR String)
	{
		CString result;
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x162, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms, String);
		return result;
	}
	void SendFax()
	{
		InvokeHelper(0x164, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void ChangeFileOpenDirectory(LPCTSTR Path)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x165, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Path);
	}
	void RunOld(LPCTSTR MacroName)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x166, DISPATCH_METHOD, VT_EMPTY, NULL, parms, MacroName);
	}
	void GoForward()
	{
		InvokeHelper(0x167, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void Move(long Left, long Top)
	{
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0x168, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Left, Top);
	}
	void Resize(long Width, long Height)
	{
		static BYTE parms[] = VTS_I4 VTS_I4 ;
		InvokeHelper(0x169, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Width, Height);
	}
	float InchesToPoints(float Inches)
	{
		float result;
		static BYTE parms[] = VTS_R4 ;
		InvokeHelper(0x172, DISPATCH_METHOD, VT_R4, (void*)&result, parms, Inches);
		return result;
	}
	float CentimetersToPoints(float Centimeters)
	{
		float result;
		static BYTE parms[] = VTS_R4 ;
		InvokeHelper(0x173, DISPATCH_METHOD, VT_R4, (void*)&result, parms, Centimeters);
		return result;
	}
	float MillimetersToPoints(float Millimeters)
	{
		float result;
		static BYTE parms[] = VTS_R4 ;
		InvokeHelper(0x174, DISPATCH_METHOD, VT_R4, (void*)&result, parms, Millimeters);
		return result;
	}
	float PicasToPoints(float Picas)
	{
		float result;
		static BYTE parms[] = VTS_R4 ;
		InvokeHelper(0x175, DISPATCH_METHOD, VT_R4, (void*)&result, parms, Picas);
		return result;
	}
	float LinesToPoints(float Lines)
	{
		float result;
		static BYTE parms[] = VTS_R4 ;
		InvokeHelper(0x176, DISPATCH_METHOD, VT_R4, (void*)&result, parms, Lines);
		return result;
	}
	float PointsToInches(float Points)
	{
		float result;
		static BYTE parms[] = VTS_R4 ;
		InvokeHelper(0x17c, DISPATCH_METHOD, VT_R4, (void*)&result, parms, Points);
		return result;
	}
	float PointsToCentimeters(float Points)
	{
		float result;
		static BYTE parms[] = VTS_R4 ;
		InvokeHelper(0x17d, DISPATCH_METHOD, VT_R4, (void*)&result, parms, Points);
		return result;
	}
	float PointsToMillimeters(float Points)
	{
		float result;
		static BYTE parms[] = VTS_R4 ;
		InvokeHelper(0x17e, DISPATCH_METHOD, VT_R4, (void*)&result, parms, Points);
		return result;
	}
	float PointsToPicas(float Points)
	{
		float result;
		static BYTE parms[] = VTS_R4 ;
		InvokeHelper(0x17f, DISPATCH_METHOD, VT_R4, (void*)&result, parms, Points);
		return result;
	}
	float PointsToLines(float Points)
	{
		float result;
		static BYTE parms[] = VTS_R4 ;
		InvokeHelper(0x180, DISPATCH_METHOD, VT_R4, (void*)&result, parms, Points);
		return result;
	}
	void Activate()
	{
		InvokeHelper(0x181, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	float PointsToPixels(float Points, VARIANT * fVertical)
	{
		float result;
		static BYTE parms[] = VTS_R4 VTS_PVARIANT ;
		InvokeHelper(0x183, DISPATCH_METHOD, VT_R4, (void*)&result, parms, Points, fVertical);
		return result;
	}
	float PixelsToPoints(float Pixels, VARIANT * fVertical)
	{
		float result;
		static BYTE parms[] = VTS_R4 VTS_PVARIANT ;
		InvokeHelper(0x184, DISPATCH_METHOD, VT_R4, (void*)&result, parms, Pixels, fVertical);
		return result;
	}
	void KeyboardLatin()
	{
		InvokeHelper(0x190, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void KeyboardBidi()
	{
		InvokeHelper(0x191, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void ToggleKeyboard()
	{
		InvokeHelper(0x192, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	long Keyboard(long LangId)
	{
		long result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x1be, DISPATCH_METHOD, VT_I4, (void*)&result, parms, LangId);
		return result;
	}
	CString ProductCode()
	{
		CString result;
		InvokeHelper(0x194, DISPATCH_METHOD, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH DefaultWebOptions()
	{
		LPDISPATCH result;
		InvokeHelper(0x195, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void DiscussionSupport(VARIANT * Range, VARIANT * cid, VARIANT * piCSE)
	{
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x197, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Range, cid, piCSE);
	}
	void SetDefaultTheme(LPCTSTR Name, long DocumentType)
	{
		static BYTE parms[] = VTS_BSTR VTS_I4 ;
		InvokeHelper(0x19e, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Name, DocumentType);
	}
	CString GetDefaultTheme(long DocumentType)
	{
		CString result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x1a0, DISPATCH_METHOD, VT_BSTR, (void*)&result, parms, DocumentType);
		return result;
	}
	LPDISPATCH get_EmailOptions()
	{
		LPDISPATCH result;
		InvokeHelper(0x185, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	long get_Language()
	{
		long result;
		InvokeHelper(0x187, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_COMAddIns()
	{
		LPDISPATCH result;
		InvokeHelper(0x6f, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_CheckLanguage()
	{
		BOOL result;
		InvokeHelper(0x70, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_CheckLanguage(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x70, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_LanguageSettings()
	{
		LPDISPATCH result;
		InvokeHelper(0x193, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_Dummy1()
	{
		BOOL result;
		InvokeHelper(0x196, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_AnswerWizard()
	{
		LPDISPATCH result;
		InvokeHelper(0x199, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	long get_FeatureInstall()
	{
		long result;
		InvokeHelper(0x1bf, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_FeatureInstall(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x1bf, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void PrintOut2000(VARIANT * Background, VARIANT * Append, VARIANT * Range, VARIANT * OutputFileName, VARIANT * From, VARIANT * To, VARIANT * Item, VARIANT * Copies, VARIANT * Pages, VARIANT * PageType, VARIANT * PrintToFile, VARIANT * Collate, VARIANT * FileName, VARIANT * ActivePrinterMacGX, VARIANT * ManualDuplexPrint, VARIANT * PrintZoomColumn, VARIANT * PrintZoomRow, VARIANT * PrintZoomPaperWidth, VARIANT * PrintZoomPaperHeight)
	{
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x1bc, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Background, Append, Range, OutputFileName, From, To, Item, Copies, Pages, PageType, PrintToFile, Collate, FileName, ActivePrinterMacGX, ManualDuplexPrint, PrintZoomColumn, PrintZoomRow, PrintZoomPaperWidth, PrintZoomPaperHeight);
	}
	VARIANT Run(LPCTSTR MacroName, VARIANT * varg1, VARIANT * varg2, VARIANT * varg3, VARIANT * varg4, VARIANT * varg5, VARIANT * varg6, VARIANT * varg7, VARIANT * varg8, VARIANT * varg9, VARIANT * varg10, VARIANT * varg11, VARIANT * varg12, VARIANT * varg13, VARIANT * varg14, VARIANT * varg15, VARIANT * varg16, VARIANT * varg17, VARIANT * varg18, VARIANT * varg19, VARIANT * varg20, VARIANT * varg21, VARIANT * varg22, VARIANT * varg23, VARIANT * varg24, VARIANT * varg25, VARIANT * varg26, VARIANT * varg27, VARIANT * varg28, VARIANT * varg29, VARIANT * varg30)
	{
		VARIANT result;
		static BYTE parms[] = VTS_BSTR VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x1bd, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms, MacroName, varg1, varg2, varg3, varg4, varg5, varg6, varg7, varg8, varg9, varg10, varg11, varg12, varg13, varg14, varg15, varg16, varg17, varg18, varg19, varg20, varg21, varg22, varg23, varg24, varg25, varg26, varg27, varg28, varg29, varg30);
		return result;
	}
	void PrintOut(VARIANT * Background, VARIANT * Append, VARIANT * Range, VARIANT * OutputFileName, VARIANT * From, VARIANT * To, VARIANT * Item, VARIANT * Copies, VARIANT * Pages, VARIANT * PageType, VARIANT * PrintToFile, VARIANT * Collate, VARIANT * FileName, VARIANT * ActivePrinterMacGX, VARIANT * ManualDuplexPrint, VARIANT * PrintZoomColumn, VARIANT * PrintZoomRow, VARIANT * PrintZoomPaperWidth, VARIANT * PrintZoomPaperHeight)
	{
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x1c0, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Background, Append, Range, OutputFileName, From, To, Item, Copies, Pages, PageType, PrintToFile, Collate, FileName, ActivePrinterMacGX, ManualDuplexPrint, PrintZoomColumn, PrintZoomRow, PrintZoomPaperWidth, PrintZoomPaperHeight);
	}
	long get_AutomationSecurity()
	{
		long result;
		InvokeHelper(0x1c1, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_AutomationSecurity(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x1c1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_FileDialog(long FileDialogType)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x1c2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, parms, FileDialogType);
		return result;
	}
	CString get_EmailTemplate()
	{
		CString result;
		InvokeHelper(0x1c3, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_EmailTemplate(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x1c3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_ShowWindowsInTaskbar()
	{
		BOOL result;
		InvokeHelper(0x1c4, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_ShowWindowsInTaskbar(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x1c4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_NewDocument()
	{
		LPDISPATCH result;
		InvokeHelper(0x1c6, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_ShowStartupDialog()
	{
		BOOL result;
		InvokeHelper(0x1c7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_ShowStartupDialog(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x1c7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_AutoCorrectEmail()
	{
		LPDISPATCH result;
		InvokeHelper(0x1c8, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_TaskPanes()
	{
		LPDISPATCH result;
		InvokeHelper(0x1c9, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_DefaultLegalBlackline()
	{
		BOOL result;
		InvokeHelper(0x1cb, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_DefaultLegalBlackline(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x1cb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL Dummy2()
	{
		BOOL result;
		InvokeHelper(0x1ca, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_SmartTagRecognizers()
	{
		LPDISPATCH result;
		InvokeHelper(0x1cc, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_SmartTagTypes()
	{
		LPDISPATCH result;
		InvokeHelper(0x1cd, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_XMLNamespaces()
	{
		LPDISPATCH result;
		InvokeHelper(0x1cf, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void PutFocusInMailHeader()
	{
		InvokeHelper(0x1d0, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	BOOL get_ArbitraryXMLSupportAvailable()
	{
		BOOL result;
		InvokeHelper(0x1d1, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
*/
	// _Application properties
public:

};





// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

// Documents wrapper class

class Documents : public myDispatchDriver
{
    public:
        Documents(){} // Calls myDispatchDriver default constructor
        Documents(LPDISPATCH pDispatch) : myDispatchDriver(pDispatch) {}
//        Documents(const CDocuments& dispatchSrc) : myDispatchDriver(dispatchSrc) {}

    // Attributes
    public:

    // Operations
    public:


    // Documents methods
    public:
/*
        LPUNKNOWN get__NewEnum()
        {
            LPUNKNOWN result;
            InvokeHelper(0xfffffffc, DISPATCH_PROPERTYGET, VT_UNKNOWN, (void*)&result, NULL);
            return result;
        }
        long get_Count()
        {
            long result;
            InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
            return result;
        }
        LPDISPATCH get_Application()
        {
            LPDISPATCH result;
            InvokeHelper(0x3e8, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
            return result;
        }
        long get_Creator()
        {
            long result;
            InvokeHelper(0x3e9, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
            return result;
        }
        LPDISPATCH get_Parent()
        {
            LPDISPATCH result;
            InvokeHelper(0x3ea, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
            return result;
        }
        LPDISPATCH Item(VARIANT * Index)
        {
            LPDISPATCH result;
            static BYTE parms[] = VTS_PVARIANT ;
            InvokeHelper(0x0, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Index);
            return result;
        }
        void Close(VARIANT * SaveChanges, VARIANT * OriginalFormat, VARIANT * RouteDocument)
        {
            static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
            InvokeHelper(0x451, DISPATCH_METHOD, VT_EMPTY, NULL, parms, SaveChanges, OriginalFormat, RouteDocument);
        }
        LPDISPATCH AddOld(VARIANT * Template, VARIANT * NewTemplate)
        {
            LPDISPATCH result;
            static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT ;
            InvokeHelper(0xb, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Template, NewTemplate);
            return result;
        }
        LPDISPATCH OpenOld( VARIANT *FileName,              VARIANT *ConfirmConversions, VARIANT *ReadOnly, VARIANT *AddToRecentFiles,
                            VARIANT *PasswordDocument,      VARIANT *PasswordTemplate,   VARIANT *Revert,   VARIANT *WritePasswordDocument,
                            VARIANT *WritePasswordTemplate, VARIANT *Format )
        {
            LPDISPATCH result;
            static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;

            InvokeHelper( 0xc, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
                          FileName,              ConfirmConversions, ReadOnly, AddToRecentFiles,
                          PasswordDocument,      PasswordTemplate,   Revert,   WritePasswordDocument,
                          WritePasswordTemplate, Format );
            return result;
        }
        void Save(VARIANT * NoPrompt, VARIANT * OriginalFormat)
        {
            static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT ;
            InvokeHelper(0xd, DISPATCH_METHOD, VT_EMPTY, NULL, parms, NoPrompt, OriginalFormat);
        }
        LPDISPATCH Add(VARIANT * Template, VARIANT * NewTemplate, VARIANT * DocumentType, VARIANT * Visible)
        {
            LPDISPATCH result;
            static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
            InvokeHelper(0xe, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Template, NewTemplate, DocumentType, Visible);
            return result;
        }
        LPDISPATCH Open2000( VARIANT *FileName,              VARIANT *ConfirmConversions, VARIANT *ReadOnly, VARIANT *AddToRecentFiles,
                             VARIANT *PasswordDocument,      VARIANT *PasswordTemplate,   VARIANT *Revert,   VARIANT *WritePasswordDocument,
                             VARIANT *WritePasswordTemplate, VARIANT *Format,             VARIANT *Encoding, VARIANT *Visible )
        {
            LPDISPATCH result;
            static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;

            InvokeHelper( 0xf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
                          FileName,              ConfirmConversions, ReadOnly, AddToRecentFiles,
                          PasswordDocument,      PasswordTemplate,   Revert,   WritePasswordDocument,
                          WritePasswordTemplate, Format,             Encoding, Visible);
            return result;
        }
        void CheckOut(LPCTSTR FileName)
        {
            static BYTE parms[] = VTS_BSTR ;
            InvokeHelper(0x10, DISPATCH_METHOD, VT_EMPTY, NULL, parms, FileName);
        }
        BOOL CanCheckOut(LPCTSTR FileName)
        {
            BOOL result;
            static BYTE parms[] = VTS_BSTR ;
            InvokeHelper(0x11, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, FileName);
            return result;
        }

        LPDISPATCH Open2002( VARIANT *FileName,              VARIANT *ConfirmConversions, VARIANT *ReadOnly,        VARIANT *AddToRecentFiles,
                             VARIANT *PasswordDocument,      VARIANT *PasswordTemplate,   VARIANT *Revert,          VARIANT *WritePasswordDocument,
                             VARIANT *WritePasswordTemplate, VARIANT *Format,             VARIANT *Encoding,        VARIANT *Visible,
                             VARIANT *OpenAndRepair,         VARIANT *DocumentDirection,  VARIANT *NoEncodingDialog )
        {
            LPDISPATCH result;
            static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
            InvokeHelper( 0x12, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
                          FileName,              ConfirmConversions, ReadOnly,        AddToRecentFiles,
                          PasswordDocument,      PasswordTemplate,   Revert,          WritePasswordDocument,
                          WritePasswordTemplate, Format,             Encoding,        Visible,
                          OpenAndRepair,         DocumentDirection,  NoEncodingDialog);
            return result;
        }

        LPDISPATCH Open( VARIANT *FileName,              VARIANT *ConfirmConversions, VARIANT *ReadOnly,         VARIANT *AddToRecentFiles,
                         VARIANT *PasswordDocument,      VARIANT *PasswordTemplate,   VARIANT *Revert,           VARIANT *WritePasswordDocument,
                         VARIANT *WritePasswordTemplate, VARIANT *Format,             VARIANT *Encoding,         VARIANT *Visible,
                         VARIANT *OpenAndRepair,         VARIANT *DocumentDirection,  VARIANT *NoEncodingDialog, VARIANT *XMLTransform )
        {
            LPDISPATCH result;
            static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT
                                  VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT
                                  VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT
                                  VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;

            InvokeHelper( 0x13, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
                          FileName,              ConfirmConversions, ReadOnly,         AddToRecentFiles,
                          PasswordDocument,      PasswordTemplate,   Revert,           WritePasswordDocument,
                          WritePasswordTemplate, Format,             Encoding,         Visible,
                          OpenAndRepair,         DocumentDirection,  NoEncodingDialog, XMLTransform);
            return result;
        }
 */

        //*****************************************************************************
        LPDISPATCH myOpenOld( LPCTSTR FileName )
        {
            LPDISPATCH result;
            static BYTE parms[] = VTS_BSTR VTS_I4   VTS_I4 VTS_I4
                                  VTS_BSTR VTS_BSTR VTS_I4 VTS_BSTR
                                  VTS_BSTR VTS_I4;

            long    lFalse      = 0;
            long    lTrue       = 1;
//            long    l50001      = 50001;
            TCHAR   strBlank[]  = { L"" };
            InvokeHelper( 0xf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
                          FileName, lFalse,           lTrue,  lFalse,
                          strBlank, strBlank,         lFalse, strBlank,
                          strBlank, wdOpenFormatAuto );
            return result;
        }

        //*****************************************************************************
        LPDISPATCH myOpen2000( LPCTSTR FileName )
        {
            LPDISPATCH result;
            static BYTE parms[] = VTS_BSTR VTS_I4   VTS_I4 VTS_I4
                                  VTS_BSTR VTS_BSTR VTS_I4 VTS_BSTR
                                  VTS_BSTR VTS_I4   VTS_I4 VTS_I4;

            long    lFalse      = 0;
            long    lTrue       = 1;
            long    l50001      = 50001;
//            char    *strBlank   = { "" };
            TCHAR   strBlank[]  = { L"" };
            InvokeHelper( 0xf, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
                          FileName, lFalse,           lTrue,  lFalse,
                          strBlank, strBlank,         lFalse, strBlank,
                          strBlank, wdOpenFormatAuto, l50001, lTrue );
            return result;
        }

        //*****************************************************************************
        LPDISPATCH myOpen2002( LPCTSTR FileName )
        {
            LPDISPATCH result;
            static BYTE parms[] = VTS_BSTR VTS_I4   VTS_I4 VTS_I4
                                  VTS_BSTR VTS_BSTR VTS_I4 VTS_BSTR
                                  VTS_BSTR VTS_I4   VTS_I4 VTS_I4
                                  VTS_I4   VTS_I4   VTS_I4 ;

            long    lFalse      = 0;
            long    lTrue       = 1;
            long    l50001      = 50001;
//            char    *strBlank   = { "" };
            TCHAR   strBlank[]  = { L"" };
            InvokeHelper( 0x12, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
                          FileName, lFalse,           lTrue,  lFalse,
                          strBlank, strBlank,         lFalse, strBlank,
                          strBlank, wdOpenFormatAuto, l50001, lTrue,
                          lFalse,   wdLeftToRight,    lTrue );
            return result;
        }

        //*****************************************************************************
        LPDISPATCH myOpen( LPCTSTR FileName )
        {
            LPDISPATCH result;
            static BYTE parms[] = VTS_BSTR VTS_I4   VTS_I4 VTS_I4
                                  VTS_BSTR VTS_BSTR VTS_I4 VTS_BSTR
                                  VTS_BSTR VTS_I4   VTS_I4 VTS_I4
                                  VTS_I4   VTS_I4   VTS_I4 ;

            long    lFalse      = 0;
            long    lTrue       = 1;
            long    l50001      = 50001;
//            char    *strBlank   = { "" };
            TCHAR   strBlank[]  = { L"" };
            InvokeHelper( 0x13, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms,
                          FileName, lFalse,           lTrue,  lFalse,
                          strBlank, strBlank,         lFalse, strBlank,
                          strBlank, wdOpenFormatAuto, l50001, lTrue,
                          lFalse,   wdLeftToRight,    lTrue );
            return result;
        }

    // Documents properties
    public:

};




// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard

// _Document wrapper class

class _Document : public myDispatchDriver
{
public:
	_Document(){} // Calls myDispatchDriver default constructor
	_Document(LPDISPATCH pDispatch) : myDispatchDriver(pDispatch) {}
//	_Document(const CDocument0& dispatchSrc) : myDispatchDriver(dispatchSrc) {}

	// Attributes
public:

	// Operations
public:


	// _Document methods
public:
/*
    CString get_Name()
	{
		CString result;
		InvokeHelper(0x0, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Application()
	{
		LPDISPATCH result;
		InvokeHelper(0x1, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	long get_Creator()
	{
		long result;
		InvokeHelper(0x3e9, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Parent()
	{
		LPDISPATCH result;
		InvokeHelper(0x3ea, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_BuiltInDocumentProperties()
	{
		LPDISPATCH result;
		InvokeHelper(0x3e8, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_CustomDocumentProperties()
	{
		LPDISPATCH result;
		InvokeHelper(0x2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	CString get_Path()
	{
		CString result;
		InvokeHelper(0x3, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Bookmarks()
	{
		LPDISPATCH result;
		InvokeHelper(0x4, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Tables()
	{
		LPDISPATCH result;
		InvokeHelper(0x6, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Footnotes()
	{
		LPDISPATCH result;
		InvokeHelper(0x7, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Endnotes()
	{
		LPDISPATCH result;
		InvokeHelper(0x8, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Comments()
	{
		LPDISPATCH result;
		InvokeHelper(0x9, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	long get_Type()
	{
		long result;
		InvokeHelper(0xa, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	BOOL get_AutoHyphenation()
	{
		BOOL result;
		InvokeHelper(0xb, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_AutoHyphenation(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_HyphenateCaps()
	{
		BOOL result;
		InvokeHelper(0xc, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_HyphenateCaps(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0xc, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_HyphenationZone()
	{
		long result;
		InvokeHelper(0xd, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_HyphenationZone(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_ConsecutiveHyphensLimit()
	{
		long result;
		InvokeHelper(0xe, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_ConsecutiveHyphensLimit(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0xe, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_Sections()
	{
		LPDISPATCH result;
		InvokeHelper(0xf, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Paragraphs()
	{
		LPDISPATCH result;
		InvokeHelper(0x10, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Words()
	{
		LPDISPATCH result;
		InvokeHelper(0x11, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Sentences()
	{
		LPDISPATCH result;
		InvokeHelper(0x12, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Characters()
	{
		LPDISPATCH result;
		InvokeHelper(0x13, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Fields()
	{
		LPDISPATCH result;
		InvokeHelper(0x14, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_FormFields()
	{
		LPDISPATCH result;
		InvokeHelper(0x15, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Styles()
	{
		LPDISPATCH result;
		InvokeHelper(0x16, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Frames()
	{
		LPDISPATCH result;
		InvokeHelper(0x17, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_TablesOfFigures()
	{
		LPDISPATCH result;
		InvokeHelper(0x19, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Variables()
	{
		LPDISPATCH result;
		InvokeHelper(0x1a, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_MailMerge()
	{
		LPDISPATCH result;
		InvokeHelper(0x1b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Envelope()
	{
		LPDISPATCH result;
		InvokeHelper(0x1c, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	CString get_FullName()
	{
		CString result;
		InvokeHelper(0x1d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Revisions()
	{
		LPDISPATCH result;
		InvokeHelper(0x1e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_TablesOfContents()
	{
		LPDISPATCH result;
		InvokeHelper(0x1f, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_TablesOfAuthorities()
	{
		LPDISPATCH result;
		InvokeHelper(0x20, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_PageSetup()
	{
		LPDISPATCH result;
		InvokeHelper(0x44d, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void put_PageSetup(LPDISPATCH newValue)
	{
		static BYTE parms[] = VTS_DISPATCH ;
		InvokeHelper(0x44d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_Windows()
	{
		LPDISPATCH result;
		InvokeHelper(0x22, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_HasRoutingSlip()
	{
		BOOL result;
		InvokeHelper(0x23, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_HasRoutingSlip(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x23, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_RoutingSlip()
	{
		LPDISPATCH result;
		InvokeHelper(0x24, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_Routed()
	{
		BOOL result;
		InvokeHelper(0x25, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_TablesOfAuthoritiesCategories()
	{
		LPDISPATCH result;
		InvokeHelper(0x26, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Indexes()
	{
		LPDISPATCH result;
		InvokeHelper(0x27, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_Saved()
	{
		BOOL result;
		InvokeHelper(0x28, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_Saved(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x28, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_Content()
	{
		LPDISPATCH result;
		InvokeHelper(0x29, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_ActiveWindow()
	{
		LPDISPATCH result;
		InvokeHelper(0x2a, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	long get_Kind()
	{
		long result;
		InvokeHelper(0x2b, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_Kind(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x2b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_ReadOnly()
	{
		BOOL result;
		InvokeHelper(0x2c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Subdocuments()
	{
		LPDISPATCH result;
		InvokeHelper(0x2d, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_IsMasterDocument()
	{
		BOOL result;
		InvokeHelper(0x2e, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	float get_DefaultTabStop()
	{
		float result;
		InvokeHelper(0x30, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_DefaultTabStop(float newValue)
	{
		static BYTE parms[] = VTS_R4 ;
		InvokeHelper(0x30, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_EmbedTrueTypeFonts()
	{
		BOOL result;
		InvokeHelper(0x32, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_EmbedTrueTypeFonts(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x32, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_SaveFormsData()
	{
		BOOL result;
		InvokeHelper(0x33, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_SaveFormsData(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x33, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_ReadOnlyRecommended()
	{
		BOOL result;
		InvokeHelper(0x34, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_ReadOnlyRecommended(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x34, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_SaveSubsetFonts()
	{
		BOOL result;
		InvokeHelper(0x35, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_SaveSubsetFonts(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x35, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_Compatibility(long Type)
	{
		BOOL result;
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x37, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, parms, Type);
		return result;
	}
	void put_Compatibility(long Type, BOOL newValue)
	{
		static BYTE parms[] = VTS_I4 VTS_BOOL ;
		InvokeHelper(0x37, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, Type, newValue);
	}
	LPDISPATCH get_StoryRanges()
	{
		LPDISPATCH result;
		InvokeHelper(0x38, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_CommandBars()
	{
		LPDISPATCH result;
		InvokeHelper(0x39, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_IsSubdocument()
	{
		BOOL result;
		InvokeHelper(0x3a, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	long get_SaveFormat()
	{
		long result;
		InvokeHelper(0x3b, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_ProtectionType()
	{
		long result;
		InvokeHelper(0x3c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Hyperlinks()
	{
		LPDISPATCH result;
		InvokeHelper(0x3d, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Shapes()
	{
		LPDISPATCH result;
		InvokeHelper(0x3e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_ListTemplates()
	{
		LPDISPATCH result;
		InvokeHelper(0x3f, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Lists()
	{
		LPDISPATCH result;
		InvokeHelper(0x40, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_UpdateStylesOnOpen()
	{
		BOOL result;
		InvokeHelper(0x42, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_UpdateStylesOnOpen(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x42, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	VARIANT get_AttachedTemplate()
	{
		VARIANT result;
		InvokeHelper(0x43, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
		return result;
	}
	void put_AttachedTemplate(VARIANT * newValue)
	{
		static BYTE parms[] = VTS_PVARIANT ;
		InvokeHelper(0x43, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_InlineShapes()
	{
		LPDISPATCH result;
		InvokeHelper(0x44, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Background()
	{
		LPDISPATCH result;
		InvokeHelper(0x45, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void put_Background(LPDISPATCH newValue)
	{
		static BYTE parms[] = VTS_DISPATCH ;
		InvokeHelper(0x45, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_GrammarChecked()
	{
		BOOL result;
		InvokeHelper(0x46, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_GrammarChecked(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x46, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_SpellingChecked()
	{
		BOOL result;
		InvokeHelper(0x47, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_SpellingChecked(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x47, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_ShowGrammaticalErrors()
	{
		BOOL result;
		InvokeHelper(0x48, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_ShowGrammaticalErrors(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x48, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_ShowSpellingErrors()
	{
		BOOL result;
		InvokeHelper(0x49, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_ShowSpellingErrors(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x49, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_Versions()
	{
		LPDISPATCH result;
		InvokeHelper(0x4b, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_ShowSummary()
	{
		BOOL result;
		InvokeHelper(0x4c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_ShowSummary(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x4c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_SummaryViewMode()
	{
		long result;
		InvokeHelper(0x4d, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_SummaryViewMode(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x4d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_SummaryLength()
	{
		long result;
		InvokeHelper(0x4e, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_SummaryLength(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x4e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_PrintFractionalWidths()
	{
		BOOL result;
		InvokeHelper(0x4f, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_PrintFractionalWidths(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x4f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_PrintPostScriptOverText()
	{
		BOOL result;
		InvokeHelper(0x50, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_PrintPostScriptOverText(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x50, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_Container()
	{
		LPDISPATCH result;
		InvokeHelper(0x52, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_PrintFormsData()
	{
		BOOL result;
		InvokeHelper(0x53, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_PrintFormsData(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x53, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_ListParagraphs()
	{
		LPDISPATCH result;
		InvokeHelper(0x54, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void put_Password(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x55, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void put_WritePassword(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x56, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_HasPassword()
	{
		BOOL result;
		InvokeHelper(0x57, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	BOOL get_WriteReserved()
	{
		BOOL result;
		InvokeHelper(0x58, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	CString get_ActiveWritingStyle(VARIANT * LanguageID)
	{
		CString result;
		static BYTE parms[] = VTS_PVARIANT ;
		InvokeHelper(0x5a, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, parms, LanguageID);
		return result;
	}
	void put_ActiveWritingStyle(VARIANT * LanguageID, LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_PVARIANT VTS_BSTR ;
		InvokeHelper(0x5a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, LanguageID, newValue);
	}
	BOOL get_UserControl()
	{
		BOOL result;
		InvokeHelper(0x5c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_UserControl(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x5c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_HasMailer()
	{
		BOOL result;
		InvokeHelper(0x5d, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_HasMailer(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x5d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_Mailer()
	{
		LPDISPATCH result;
		InvokeHelper(0x5e, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_ReadabilityStatistics()
	{
		LPDISPATCH result;
		InvokeHelper(0x60, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_GrammaticalErrors()
	{
		LPDISPATCH result;
		InvokeHelper(0x61, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_SpellingErrors()
	{
		LPDISPATCH result;
		InvokeHelper(0x62, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_VBProject()
	{
		LPDISPATCH result;
		InvokeHelper(0x63, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_FormsDesign()
	{
		BOOL result;
		InvokeHelper(0x64, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	CString get__CodeName()
	{
		CString result;
		InvokeHelper(0x80010000, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put__CodeName(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x80010000, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_CodeName()
	{
		CString result;
		InvokeHelper(0x106, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	BOOL get_SnapToGrid()
	{
		BOOL result;
		InvokeHelper(0x12c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_SnapToGrid(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x12c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_SnapToShapes()
	{
		BOOL result;
		InvokeHelper(0x12d, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_SnapToShapes(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x12d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_GridDistanceHorizontal()
	{
		float result;
		InvokeHelper(0x12e, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_GridDistanceHorizontal(float newValue)
	{
		static BYTE parms[] = VTS_R4 ;
		InvokeHelper(0x12e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_GridDistanceVertical()
	{
		float result;
		InvokeHelper(0x12f, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_GridDistanceVertical(float newValue)
	{
		static BYTE parms[] = VTS_R4 ;
		InvokeHelper(0x12f, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_GridOriginHorizontal()
	{
		float result;
		InvokeHelper(0x130, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_GridOriginHorizontal(float newValue)
	{
		static BYTE parms[] = VTS_R4 ;
		InvokeHelper(0x130, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	float get_GridOriginVertical()
	{
		float result;
		InvokeHelper(0x131, DISPATCH_PROPERTYGET, VT_R4, (void*)&result, NULL);
		return result;
	}
	void put_GridOriginVertical(float newValue)
	{
		static BYTE parms[] = VTS_R4 ;
		InvokeHelper(0x131, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_GridSpaceBetweenHorizontalLines()
	{
		long result;
		InvokeHelper(0x132, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_GridSpaceBetweenHorizontalLines(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x132, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_GridSpaceBetweenVerticalLines()
	{
		long result;
		InvokeHelper(0x133, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_GridSpaceBetweenVerticalLines(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x133, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_GridOriginFromMargin()
	{
		BOOL result;
		InvokeHelper(0x134, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_GridOriginFromMargin(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x134, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_KerningByAlgorithm()
	{
		BOOL result;
		InvokeHelper(0x135, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_KerningByAlgorithm(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x135, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_JustificationMode()
	{
		long result;
		InvokeHelper(0x136, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_JustificationMode(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x136, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_FarEastLineBreakLevel()
	{
		long result;
		InvokeHelper(0x137, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_FarEastLineBreakLevel(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x137, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_NoLineBreakBefore()
	{
		CString result;
		InvokeHelper(0x138, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_NoLineBreakBefore(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x138, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_NoLineBreakAfter()
	{
		CString result;
		InvokeHelper(0x139, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_NoLineBreakAfter(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x139, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_TrackRevisions()
	{
		BOOL result;
		InvokeHelper(0x13a, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_TrackRevisions(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x13a, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_PrintRevisions()
	{
		BOOL result;
		InvokeHelper(0x13b, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_PrintRevisions(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x13b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_ShowRevisions()
	{
		BOOL result;
		InvokeHelper(0x13c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_ShowRevisions(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x13c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void Close(VARIANT * SaveChanges, VARIANT * OriginalFormat, VARIANT * RouteDocument)
	{
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x451, DISPATCH_METHOD, VT_EMPTY, NULL, parms, SaveChanges, OriginalFormat, RouteDocument);
	}
    */
    void Close( )
    {
        static BYTE parms[] = VTS_I4 VTS_I4 VTS_I4 ;
        InvokeHelper(0x451, DISPATCH_METHOD, VT_EMPTY, NULL, parms, 0, 0, 0 );
    }
/*
	void SaveAs2000(VARIANT * FileName, VARIANT * FileFormat, VARIANT * LockComments, VARIANT * Password, VARIANT * AddToRecentFiles, VARIANT * WritePassword, VARIANT * ReadOnlyRecommended, VARIANT * EmbedTrueTypeFonts, VARIANT * SaveNativePictureFormat, VARIANT * SaveFormsData, VARIANT * SaveAsAOCELetter)
	{
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x66, DISPATCH_METHOD, VT_EMPTY, NULL, parms, FileName, FileFormat, LockComments, Password, AddToRecentFiles, WritePassword, ReadOnlyRecommended, EmbedTrueTypeFonts, SaveNativePictureFormat, SaveFormsData, SaveAsAOCELetter);
	}
	void Repaginate()
	{
		InvokeHelper(0x67, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void FitToPages()
	{
		InvokeHelper(0x68, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void ManualHyphenation()
	{
		InvokeHelper(0x69, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void Select()
	{
		InvokeHelper(0xffff, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void DataForm()
	{
		InvokeHelper(0x6a, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void Route()
	{
		InvokeHelper(0x6b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void Save()
	{
		InvokeHelper(0x6c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void PrintOutOld( VARIANT * Background, VARIANT * Append,   VARIANT * Range, VARIANT * OutputFileName,
                      VARIANT * From,       VARIANT * To,       VARIANT * Item,  VARIANT * Copies,
                      VARIANT * Pages,      VARIANT * PageType, VARIANT * PrintToFile, VARIANT * Collate,
                      VARIANT * ActivePrinterMacGX, VARIANT * ManualDuplexPrint)
	{
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT
                              VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT
                              VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT
                              VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper( 0x6d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
                      Background, Append, Range, OutputFileName,
                      From, To, Item, Copies,
                      Pages, PageType, PrintToFile, Collate,
                      ActivePrinterMacGX, ManualDuplexPrint);
	}
    */
        void myPrintToFileOld( LPCTSTR FileName )
//    VARIANT * Background,           VARIANT * Append,               VARIANT * Range,            VARIANT * OutputFileName,
//    VARIANT * From,                 VARIANT * To,                   VARIANT * Item,             VARIANT * Copies,
//    VARIANT * Pages,                VARIANT * PageType,             VARIANT * PrintToFile,      VARIANT * Collate,
//    VARIANT * ActivePrinterMacGX,   VARIANT * ManualDuplexPrint,    VARIANT * PrintZoomColumn,  VARIANT * PrintZoomRow,
//    VARIANT * PrintZoomPaperWidth,  VARIANT * PrintZoomPaperHeight)
    {
//        static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
//        InvokeHelper( 0x1be, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
//                      Background,           Append,             Range,              OutputFileName,
//                      From,                 To,                 Item,               Copies,
//                      Pages,                PageType,           PrintToFile,        Collate,
//                      ActivePrinterMacGX,   ManualDuplexPrint,  PrintZoomColumn,    PrintZoomRow,
//                      PrintZoomPaperWidth,  PrintZoomPaperHeight    );

        static BYTE parms[] = VTS_I4    VTS_I4   VTS_I4 VTS_BSTR
                              VTS_BSTR  VTS_BSTR VTS_I4 VTS_I4
                              VTS_BSTR  VTS_I4   VTS_I4 VTS_I4 ;
//                              VTS_SCODE VTS_I4 ;

        long    lFalse      = 0;
        long    lTrue       = 1;
        long    lOne        = 1;
        char    *strBlank   = { "" };

        InvokeHelper( 0x6d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
                      lFalse,    lFalse,           wdPrintAllDocument,   FileName,
                      strBlank, strBlank,        wdPrintDocumentContent, lOne,
                      strBlank, wdPrintAllPages, lTrue,                  lFalse );
//                      (long)DISP_E_PARAMNOTFOUND, lFalse );
    }

        void myPrintPageToFileOld( LPCTSTR FileName, LPCTSTR PageNum )
        {
            static BYTE parms[] = VTS_I4    VTS_I4   VTS_I4 VTS_BSTR
                                  VTS_BSTR  VTS_BSTR VTS_I4 VTS_I4
                                  VTS_BSTR  VTS_I4   VTS_I4 VTS_I4 ;
//                              VTS_SCODE VTS_I4 ;

            long    lFalse      = 0;
            long    lTrue       = 1;
            long    lOne        = 1;
            char    *strBlank   = { "" };

            InvokeHelper( 0x6d, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
                          lFalse,   lFalse,          wdPrintFromTo,          FileName,
                          PageNum,  PageNum,         wdPrintDocumentContent, lOne,
                          strBlank, wdPrintAllPages, lTrue,                  lFalse );
//                      (long)DISP_E_PARAMNOTFOUND, lFalse );
        }
/*
	void SendMail()
	{
		InvokeHelper(0x6e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	LPDISPATCH Range(VARIANT * Start, VARIANT * End)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x7d0, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Start, End);
		return result;
	}
	void RunAutoMacro(long Which)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x70, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Which);
	}
	void Activate()
	{
		InvokeHelper(0x71, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void PrintPreview()
	{
		InvokeHelper(0x72, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	LPDISPATCH GoTo(VARIANT * What, VARIANT * Which, VARIANT * Count, VARIANT * Name)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x73, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, What, Which, Count, Name);
		return result;
	}
	BOOL Undo(VARIANT * Times)
	{
		BOOL result;
		static BYTE parms[] = VTS_PVARIANT ;
		InvokeHelper(0x74, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, Times);
		return result;
	}
	BOOL Redo(VARIANT * Times)
	{
		BOOL result;
		static BYTE parms[] = VTS_PVARIANT ;
		InvokeHelper(0x75, DISPATCH_METHOD, VT_BOOL, (void*)&result, parms, Times);
		return result;
	}
	long ComputeStatistics(long Statistic, VARIANT * IncludeFootnotesAndEndnotes)
	{
		long result;
		static BYTE parms[] = VTS_I4 VTS_PVARIANT ;
		InvokeHelper(0x76, DISPATCH_METHOD, VT_I4, (void*)&result, parms, Statistic, IncludeFootnotesAndEndnotes);
		return result;
	}
    */
    long CountPages( )
    {
        long result;
        static BYTE parms[] = VTS_I4 VTS_I4 ;
        InvokeHelper(0x76, DISPATCH_METHOD, VT_I4, (void*)&result, parms, 2, 0 );
        return result;
    }
/*
	void MakeCompatibilityDefault()
	{
		InvokeHelper(0x77, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void Protect2002(long Type, VARIANT * NoReset, VARIANT * Password)
	{
		static BYTE parms[] = VTS_I4 VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x78, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Type, NoReset, Password);
	}
	void Unprotect(VARIANT * Password)
	{
		static BYTE parms[] = VTS_PVARIANT ;
		InvokeHelper(0x79, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Password);
	}
	void EditionOptions(long Type, long Option, LPCTSTR Name, VARIANT * Format)
	{
		static BYTE parms[] = VTS_I4 VTS_I4 VTS_BSTR VTS_PVARIANT ;
		InvokeHelper(0x7a, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Type, Option, Name, Format);
	}
	void RunLetterWizard(VARIANT * LetterContent, VARIANT * WizardMode)
	{
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x7b, DISPATCH_METHOD, VT_EMPTY, NULL, parms, LetterContent, WizardMode);
	}
	LPDISPATCH GetLetterContent()
	{
		LPDISPATCH result;
		InvokeHelper(0x7c, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void SetLetterContent(VARIANT * LetterContent)
	{
		static BYTE parms[] = VTS_PVARIANT ;
		InvokeHelper(0x7d, DISPATCH_METHOD, VT_EMPTY, NULL, parms, LetterContent);
	}
	void CopyStylesFromTemplate(LPCTSTR Template)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x7e, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Template);
	}
	void UpdateStyles()
	{
		InvokeHelper(0x7f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void CheckGrammar()
	{
		InvokeHelper(0x83, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void CheckSpelling(VARIANT * CustomDictionary, VARIANT * IgnoreUppercase, VARIANT * AlwaysSuggest, VARIANT * CustomDictionary2, VARIANT * CustomDictionary3, VARIANT * CustomDictionary4, VARIANT * CustomDictionary5, VARIANT * CustomDictionary6, VARIANT * CustomDictionary7, VARIANT * CustomDictionary8, VARIANT * CustomDictionary9, VARIANT * CustomDictionary10)
	{
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x84, DISPATCH_METHOD, VT_EMPTY, NULL, parms, CustomDictionary, IgnoreUppercase, AlwaysSuggest, CustomDictionary2, CustomDictionary3, CustomDictionary4, CustomDictionary5, CustomDictionary6, CustomDictionary7, CustomDictionary8, CustomDictionary9, CustomDictionary10);
	}
	void FollowHyperlink(VARIANT * Address, VARIANT * SubAddress, VARIANT * NewWindow, VARIANT * AddHistory, VARIANT * ExtraInfo, VARIANT * Method, VARIANT * HeaderInfo)
	{
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x87, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Address, SubAddress, NewWindow, AddHistory, ExtraInfo, Method, HeaderInfo);
	}
	void AddToFavorites()
	{
		InvokeHelper(0x88, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void Reload()
	{
		InvokeHelper(0x89, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	LPDISPATCH AutoSummarize(VARIANT * Length, VARIANT * Mode, VARIANT * UpdateProperties)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x8a, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, Length, Mode, UpdateProperties);
		return result;
	}
	void RemoveNumbers(VARIANT * NumberType)
	{
		static BYTE parms[] = VTS_PVARIANT ;
		InvokeHelper(0x8c, DISPATCH_METHOD, VT_EMPTY, NULL, parms, NumberType);
	}
	void ConvertNumbersToText(VARIANT * NumberType)
	{
		static BYTE parms[] = VTS_PVARIANT ;
		InvokeHelper(0x8d, DISPATCH_METHOD, VT_EMPTY, NULL, parms, NumberType);
	}
	long CountNumberedItems(VARIANT * NumberType, VARIANT * Level)
	{
		long result;
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x8e, DISPATCH_METHOD, VT_I4, (void*)&result, parms, NumberType, Level);
		return result;
	}
	void Post()
	{
		InvokeHelper(0x8f, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void ToggleFormsDesign()
	{
		InvokeHelper(0x90, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void Compare2000(LPCTSTR Name)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x91, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Name);
	}
	void UpdateSummaryProperties()
	{
		InvokeHelper(0x92, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	VARIANT GetCrossReferenceItems(VARIANT * ReferenceType)
	{
		VARIANT result;
		static BYTE parms[] = VTS_PVARIANT ;
		InvokeHelper(0x93, DISPATCH_METHOD, VT_VARIANT, (void*)&result, parms, ReferenceType);
		return result;
	}
	void AutoFormat()
	{
		InvokeHelper(0x94, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void ViewCode()
	{
		InvokeHelper(0x95, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void ViewPropertyBrowser()
	{
		InvokeHelper(0x96, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void ForwardMailer()
	{
		InvokeHelper(0xfa, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void Reply()
	{
		InvokeHelper(0xfb, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void ReplyAll()
	{
		InvokeHelper(0xfc, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void SendMailer(VARIANT * FileFormat, VARIANT * Priority)
	{
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0xfd, DISPATCH_METHOD, VT_EMPTY, NULL, parms, FileFormat, Priority);
	}
	void UndoClear()
	{
		InvokeHelper(0xfe, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void PresentIt()
	{
		InvokeHelper(0xff, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void SendFax(LPCTSTR Address, VARIANT * Subject)
	{
		static BYTE parms[] = VTS_BSTR VTS_PVARIANT ;
		InvokeHelper(0x100, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Address, Subject);
	}
	void Merge2000(LPCTSTR FileName)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x101, DISPATCH_METHOD, VT_EMPTY, NULL, parms, FileName);
	}
	void ClosePrintPreview()
	{
		InvokeHelper(0x102, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void CheckConsistency()
	{
		InvokeHelper(0x103, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	LPDISPATCH CreateLetterContent(LPCTSTR DateFormat, BOOL IncludeHeaderFooter, LPCTSTR PageDesign, long LetterStyle, BOOL Letterhead, long LetterheadLocation, float LetterheadSize, LPCTSTR RecipientName, LPCTSTR RecipientAddress, LPCTSTR Salutation, long SalutationType, LPCTSTR RecipientReference, LPCTSTR MailingInstructions, LPCTSTR AttentionLine, LPCTSTR Subject, LPCTSTR CCList, LPCTSTR ReturnAddress, LPCTSTR SenderName, LPCTSTR Closing, LPCTSTR SenderCompany, LPCTSTR SenderJobTitle, LPCTSTR SenderInitials, long EnclosureNumber, VARIANT * InfoBlock, VARIANT * RecipientCode, VARIANT * RecipientGender, VARIANT * ReturnAddressShortForm, VARIANT * SenderCity, VARIANT * SenderCode, VARIANT * SenderGender, VARIANT * SenderReference)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_BOOL VTS_BSTR VTS_I4 VTS_BOOL VTS_I4 VTS_R4 VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x104, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, DateFormat, IncludeHeaderFooter, PageDesign, LetterStyle, Letterhead, LetterheadLocation, LetterheadSize, RecipientName, RecipientAddress, Salutation, SalutationType, RecipientReference, MailingInstructions, AttentionLine, Subject, CCList, ReturnAddress, SenderName, Closing, SenderCompany, SenderJobTitle, SenderInitials, EnclosureNumber, InfoBlock, RecipientCode, RecipientGender, ReturnAddressShortForm, SenderCity, SenderCode, SenderGender, SenderReference);
		return result;
	}
	void AcceptAllRevisions()
	{
		InvokeHelper(0x13d, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void RejectAllRevisions()
	{
		InvokeHelper(0x13e, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void DetectLanguage()
	{
		InvokeHelper(0x97, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void ApplyTheme(LPCTSTR Name)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x142, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Name);
	}
	void RemoveTheme()
	{
		InvokeHelper(0x143, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void WebPagePreview()
	{
		InvokeHelper(0x145, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void ReloadAs(long Encoding)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x14b, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Encoding);
	}
	CString get_ActiveTheme()
	{
		CString result;
		InvokeHelper(0x21c, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	CString get_ActiveThemeDisplayName()
	{
		CString result;
		InvokeHelper(0x21d, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Email()
	{
		LPDISPATCH result;
		InvokeHelper(0x13f, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Scripts()
	{
		LPDISPATCH result;
		InvokeHelper(0x140, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_LanguageDetected()
	{
		BOOL result;
		InvokeHelper(0x141, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_LanguageDetected(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x141, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_FarEastLineBreakLanguage()
	{
		long result;
		InvokeHelper(0x146, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_FarEastLineBreakLanguage(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x146, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_Frameset()
	{
		LPDISPATCH result;
		InvokeHelper(0x147, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	VARIANT get_ClickAndTypeParagraphStyle()
	{
		VARIANT result;
		InvokeHelper(0x148, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
		return result;
	}
	void put_ClickAndTypeParagraphStyle(VARIANT * newValue)
	{
		static BYTE parms[] = VTS_PVARIANT ;
		InvokeHelper(0x148, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_HTMLProject()
	{
		LPDISPATCH result;
		InvokeHelper(0x149, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_WebOptions()
	{
		LPDISPATCH result;
		InvokeHelper(0x14a, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	long get_OpenEncoding()
	{
		long result;
		InvokeHelper(0x14c, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	long get_SaveEncoding()
	{
		long result;
		InvokeHelper(0x14d, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_SaveEncoding(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x14d, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_OptimizeForWord97()
	{
		BOOL result;
		InvokeHelper(0x14e, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_OptimizeForWord97(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x14e, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_VBASigned()
	{
		BOOL result;
		InvokeHelper(0x14f, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void PrintOut2000(VARIANT * Background, VARIANT * Append, VARIANT * Range, VARIANT * OutputFileName, VARIANT * From, VARIANT * To, VARIANT * Item, VARIANT * Copies, VARIANT * Pages, VARIANT * PageType, VARIANT * PrintToFile, VARIANT * Collate, VARIANT * ActivePrinterMacGX, VARIANT * ManualDuplexPrint, VARIANT * PrintZoomColumn, VARIANT * PrintZoomRow, VARIANT * PrintZoomPaperWidth, VARIANT * PrintZoomPaperHeight)
	{
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x1bc, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Background, Append, Range, OutputFileName, From, To, Item, Copies, Pages, PageType, PrintToFile, Collate, ActivePrinterMacGX, ManualDuplexPrint, PrintZoomColumn, PrintZoomRow, PrintZoomPaperWidth, PrintZoomPaperHeight);
	}
	void sblt(LPCTSTR s)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x1bd, DISPATCH_METHOD, VT_EMPTY, NULL, parms, s);
	}
	void ConvertVietDoc(long CodePageOrigin)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x1bf, DISPATCH_METHOD, VT_EMPTY, NULL, parms, CodePageOrigin);
	}
	void PrintOut(VARIANT * Background, VARIANT * Append, VARIANT * Range, VARIANT * OutputFileName, VARIANT * From, VARIANT * To, VARIANT * Item, VARIANT * Copies, VARIANT * Pages, VARIANT * PageType, VARIANT * PrintToFile, VARIANT * Collate, VARIANT * ActivePrinterMacGX, VARIANT * ManualDuplexPrint, VARIANT * PrintZoomColumn, VARIANT * PrintZoomRow, VARIANT * PrintZoomPaperWidth, VARIANT * PrintZoomPaperHeight)
	{
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x1be, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Background, Append, Range, OutputFileName, From, To, Item, Copies, Pages, PageType, PrintToFile, Collate, ActivePrinterMacGX, ManualDuplexPrint, PrintZoomColumn, PrintZoomRow, PrintZoomPaperWidth, PrintZoomPaperHeight);
	}
    */
    void myPrintToFile( LPCTSTR FileName )
//    VARIANT * Background,           VARIANT * Append,               VARIANT * Range,            VARIANT * OutputFileName,
//    VARIANT * From,                 VARIANT * To,                   VARIANT * Item,             VARIANT * Copies,
//    VARIANT * Pages,                VARIANT * PageType,             VARIANT * PrintToFile,      VARIANT * Collate,
//    VARIANT * ActivePrinterMacGX,   VARIANT * ManualDuplexPrint,    VARIANT * PrintZoomColumn,  VARIANT * PrintZoomRow,
//    VARIANT * PrintZoomPaperWidth,  VARIANT * PrintZoomPaperHeight)
    {
//        static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
//        InvokeHelper( 0x1be, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
//                      Background,           Append,             Range,              OutputFileName,
//                      From,                 To,                 Item,               Copies,
//                      Pages,                PageType,           PrintToFile,        Collate,
//                      ActivePrinterMacGX,   ManualDuplexPrint,  PrintZoomColumn,    PrintZoomRow,
//                      PrintZoomPaperWidth,  PrintZoomPaperHeight    );

        static BYTE parms[] = VTS_I4   VTS_I4 VTS_I4 VTS_BSTR
                              VTS_I4   VTS_I4 VTS_I4 VTS_I4
                              VTS_BSTR VTS_I4 VTS_I4 VTS_I4
                              VTS_I4   VTS_I4 VTS_I4 VTS_I4
                              VTS_I4   VTS_I4 ;

        long    lFalse       = 0;
        long    lTrue        = 1;
        char    *strBlank   = { "" };

        InvokeHelper( 0x1be, DISPATCH_METHOD, VT_EMPTY, NULL, parms,
                      lFalse, lFalse, wdPrintAllDocument, FileName,
                      lFalse, lFalse, wdPrintDocumentContent, lTrue,
                      strBlank, wdPrintAllPages, lTrue, lFalse,
                      lFalse, lFalse, lTrue, lTrue,
                      12240, 15840 );
    }
/*
	LPDISPATCH get_MailEnvelope()
	{
		LPDISPATCH result;
		InvokeHelper(0x150, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_DisableFeatures()
	{
		BOOL result;
		InvokeHelper(0x151, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_DisableFeatures(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x151, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_DoNotEmbedSystemFonts()
	{
		BOOL result;
		InvokeHelper(0x152, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_DoNotEmbedSystemFonts(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x152, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_Signatures()
	{
		LPDISPATCH result;
		InvokeHelper(0x153, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	CString get_DefaultTargetFrame()
	{
		CString result;
		InvokeHelper(0x154, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_DefaultTargetFrame(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x154, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_HTMLDivisions()
	{
		LPDISPATCH result;
		InvokeHelper(0x156, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	long get_DisableFeaturesIntroducedAfter()
	{
		long result;
		InvokeHelper(0x157, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_DisableFeaturesIntroducedAfter(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x157, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_RemovePersonalInformation()
	{
		BOOL result;
		InvokeHelper(0x158, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_RemovePersonalInformation(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x158, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_SmartTags()
	{
		LPDISPATCH result;
		InvokeHelper(0x15a, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	void Compare2002(LPCTSTR Name, VARIANT * AuthorName, VARIANT * CompareTarget, VARIANT * DetectFormatChanges, VARIANT * IgnoreAllComparisonWarnings, VARIANT * AddToRecentFiles)
	{
		static BYTE parms[] = VTS_BSTR VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x159, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Name, AuthorName, CompareTarget, DetectFormatChanges, IgnoreAllComparisonWarnings, AddToRecentFiles);
	}
	void CheckIn(BOOL SaveChanges, VARIANT * Comments, BOOL MakePublic)
	{
		static BYTE parms[] = VTS_BOOL VTS_PVARIANT VTS_BOOL ;
		InvokeHelper(0x15d, DISPATCH_METHOD, VT_EMPTY, NULL, parms, SaveChanges, Comments, MakePublic);
	}
	BOOL CanCheckin()
	{
		BOOL result;
		InvokeHelper(0x15f, DISPATCH_METHOD, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void Merge(LPCTSTR FileName, VARIANT * MergeTarget, VARIANT * DetectFormatChanges, VARIANT * UseFormattingFrom, VARIANT * AddToRecentFiles)
	{
		static BYTE parms[] = VTS_BSTR VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x16a, DISPATCH_METHOD, VT_EMPTY, NULL, parms, FileName, MergeTarget, DetectFormatChanges, UseFormattingFrom, AddToRecentFiles);
	}
	BOOL get_EmbedSmartTags()
	{
		BOOL result;
		InvokeHelper(0x15b, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_EmbedSmartTags(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x15b, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_SmartTagsAsXMLProps()
	{
		BOOL result;
		InvokeHelper(0x15c, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_SmartTagsAsXMLProps(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x15c, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_TextEncoding()
	{
		long result;
		InvokeHelper(0x165, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_TextEncoding(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x165, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_TextLineEnding()
	{
		long result;
		InvokeHelper(0x166, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_TextLineEnding(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x166, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void SendForReview(VARIANT * Recipients, VARIANT * Subject, VARIANT * ShowMessage, VARIANT * IncludeAttachment)
	{
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x161, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Recipients, Subject, ShowMessage, IncludeAttachment);
	}
	void ReplyWithChanges(VARIANT * ShowMessage)
	{
		static BYTE parms[] = VTS_PVARIANT ;
		InvokeHelper(0x162, DISPATCH_METHOD, VT_EMPTY, NULL, parms, ShowMessage);
	}
	void EndReview()
	{
		InvokeHelper(0x164, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	LPDISPATCH get_StyleSheets()
	{
		LPDISPATCH result;
		InvokeHelper(0x168, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	VARIANT get_DefaultTableStyle()
	{
		VARIANT result;
		InvokeHelper(0x16d, DISPATCH_PROPERTYGET, VT_VARIANT, (void*)&result, NULL);
		return result;
	}
	CString get_PasswordEncryptionProvider()
	{
		CString result;
		InvokeHelper(0x16f, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	CString get_PasswordEncryptionAlgorithm()
	{
		CString result;
		InvokeHelper(0x170, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	long get_PasswordEncryptionKeyLength()
	{
		long result;
		InvokeHelper(0x171, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	BOOL get_PasswordEncryptionFileProperties()
	{
		BOOL result;
		InvokeHelper(0x172, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void SetPasswordEncryptionOptions(LPCTSTR PasswordEncryptionProvider, LPCTSTR PasswordEncryptionAlgorithm, long PasswordEncryptionKeyLength, VARIANT * PasswordEncryptionFileProperties)
	{
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_I4 VTS_PVARIANT ;
		InvokeHelper(0x169, DISPATCH_METHOD, VT_EMPTY, NULL, parms, PasswordEncryptionProvider, PasswordEncryptionAlgorithm, PasswordEncryptionKeyLength, PasswordEncryptionFileProperties);
	}
	void RecheckSmartTags()
	{
		InvokeHelper(0x16b, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void RemoveSmartTags()
	{
		InvokeHelper(0x16c, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void SetDefaultTableStyle(VARIANT * Style, BOOL SetInTemplate)
	{
		static BYTE parms[] = VTS_PVARIANT VTS_BOOL ;
		InvokeHelper(0x16e, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Style, SetInTemplate);
	}
	void DeleteAllComments()
	{
		InvokeHelper(0x173, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void AcceptAllRevisionsShown()
	{
		InvokeHelper(0x174, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void RejectAllRevisionsShown()
	{
		InvokeHelper(0x175, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void DeleteAllCommentsShown()
	{
		InvokeHelper(0x176, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void ResetFormFields()
	{
		InvokeHelper(0x177, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void SaveAs(VARIANT * FileName, VARIANT * FileFormat, VARIANT * LockComments, VARIANT * Password, VARIANT * AddToRecentFiles, VARIANT * WritePassword, VARIANT * ReadOnlyRecommended, VARIANT * EmbedTrueTypeFonts, VARIANT * SaveNativePictureFormat, VARIANT * SaveFormsData, VARIANT * SaveAsAOCELetter, VARIANT * Encoding, VARIANT * InsertLineBreaks, VARIANT * AllowSubstitutions, VARIANT * LineEnding, VARIANT * AddBiDiMarks)
	{
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x178, DISPATCH_METHOD, VT_EMPTY, NULL, parms, FileName, FileFormat, LockComments, Password, AddToRecentFiles, WritePassword, ReadOnlyRecommended, EmbedTrueTypeFonts, SaveNativePictureFormat, SaveFormsData, SaveAsAOCELetter, Encoding, InsertLineBreaks, AllowSubstitutions, LineEnding, AddBiDiMarks);
	}
    */
    void SaveAs( LPCTSTR FileName )
    {
        static BYTE     parms[]     = VTS_BSTR VTS_I4 ;
        long            lSaveType   = wdFormatDocument;
        InvokeHelper( 0x178, DISPATCH_METHOD, VT_EMPTY, NULL, parms, FileName, lSaveType );
    }
    void SaveAsPdf( LPCTSTR FileName )
    {
        static BYTE     parms[]     = VTS_BSTR VTS_I4 ;
        long            lSaveType   = wdFormatPDF;
        InvokeHelper( 0x178, DISPATCH_METHOD, VT_EMPTY, NULL, parms, FileName, lSaveType );
    }
/*
	BOOL get_EmbedLinguisticData()
	{
		BOOL result;
		InvokeHelper(0x179, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_EmbedLinguisticData(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x179, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_FormattingShowFont()
	{
		BOOL result;
		InvokeHelper(0x1c0, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_FormattingShowFont(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x1c0, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_FormattingShowClear()
	{
		BOOL result;
		InvokeHelper(0x1c1, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_FormattingShowClear(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x1c1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_FormattingShowParagraph()
	{
		BOOL result;
		InvokeHelper(0x1c2, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_FormattingShowParagraph(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x1c2, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_FormattingShowNumbering()
	{
		BOOL result;
		InvokeHelper(0x1c3, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_FormattingShowNumbering(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x1c3, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_FormattingShowFilter()
	{
		long result;
		InvokeHelper(0x1c4, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_FormattingShowFilter(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x1c4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void CheckNewSmartTags()
	{
		InvokeHelper(0x17a, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	LPDISPATCH get_Permission()
	{
		LPDISPATCH result;
		InvokeHelper(0x1c5, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_XMLNodes()
	{
		LPDISPATCH result;
		InvokeHelper(0x1cc, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_XMLSchemaReferences()
	{
		LPDISPATCH result;
		InvokeHelper(0x1cd, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_SmartDocument()
	{
		LPDISPATCH result;
		InvokeHelper(0x1ce, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_SharedWorkspace()
	{
		LPDISPATCH result;
		InvokeHelper(0x1cf, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH get_Sync()
	{
		LPDISPATCH result;
		InvokeHelper(0x1d2, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_EnforceStyle()
	{
		BOOL result;
		InvokeHelper(0x1d7, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_EnforceStyle(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x1d7, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_AutoFormatOverride()
	{
		BOOL result;
		InvokeHelper(0x1d8, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_AutoFormatOverride(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x1d8, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_XMLSaveDataOnly()
	{
		BOOL result;
		InvokeHelper(0x1d9, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_XMLSaveDataOnly(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x1d9, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_XMLHideNamespaces()
	{
		BOOL result;
		InvokeHelper(0x1dd, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_XMLHideNamespaces(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x1dd, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_XMLShowAdvancedErrors()
	{
		BOOL result;
		InvokeHelper(0x1de, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_XMLShowAdvancedErrors(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x1de, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_XMLUseXSLTWhenSaving()
	{
		BOOL result;
		InvokeHelper(0x1da, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_XMLUseXSLTWhenSaving(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x1da, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	CString get_XMLSaveThroughXSLT()
	{
		CString result;
		InvokeHelper(0x1db, DISPATCH_PROPERTYGET, VT_BSTR, (void*)&result, NULL);
		return result;
	}
	void put_XMLSaveThroughXSLT(LPCTSTR newValue)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x1db, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	LPDISPATCH get_DocumentLibraryVersions()
	{
		LPDISPATCH result;
		InvokeHelper(0x1dc, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	BOOL get_ReadingModeLayoutFrozen()
	{
		BOOL result;
		InvokeHelper(0x1e1, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_ReadingModeLayoutFrozen(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x1e1, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	BOOL get_RemoveDateAndTime()
	{
		BOOL result;
		InvokeHelper(0x1e4, DISPATCH_PROPERTYGET, VT_BOOL, (void*)&result, NULL);
		return result;
	}
	void put_RemoveDateAndTime(BOOL newValue)
	{
		static BYTE parms[] = VTS_BOOL ;
		InvokeHelper(0x1e4, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	void SendFaxOverInternet(VARIANT * Recipients, VARIANT * Subject, VARIANT * ShowMessage)
	{
		static BYTE parms[] = VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x1d0, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Recipients, Subject, ShowMessage);
	}
	void TransformDocument(LPCTSTR Path, BOOL DataOnly)
	{
		static BYTE parms[] = VTS_BSTR VTS_BOOL ;
		InvokeHelper(0x1f4, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Path, DataOnly);
	}
	void Protect(long Type, VARIANT * NoReset, VARIANT * Password, VARIANT * UseIRM, VARIANT * EnforceStyleLock)
	{
		static BYTE parms[] = VTS_I4 VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x1d3, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Type, NoReset, Password, UseIRM, EnforceStyleLock);
	}
	void SelectAllEditableRanges(VARIANT * EditorID)
	{
		static BYTE parms[] = VTS_PVARIANT ;
		InvokeHelper(0x1d4, DISPATCH_METHOD, VT_EMPTY, NULL, parms, EditorID);
	}
	void DeleteAllEditableRanges(VARIANT * EditorID)
	{
		static BYTE parms[] = VTS_PVARIANT ;
		InvokeHelper(0x1d5, DISPATCH_METHOD, VT_EMPTY, NULL, parms, EditorID);
	}
	void DeleteAllInkAnnotations()
	{
		InvokeHelper(0x1df, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	void AddDocumentWorkspaceHeader(BOOL RichFormat, LPCTSTR Url, LPCTSTR Title, LPCTSTR Description, LPCTSTR ID)
	{
		static BYTE parms[] = VTS_BOOL VTS_BSTR VTS_BSTR VTS_BSTR VTS_BSTR ;
		InvokeHelper(0x1e2, DISPATCH_METHOD, VT_EMPTY, NULL, parms, RichFormat, Url, Title, Description, ID);
	}
	void RemoveDocumentWorkspaceHeader(LPCTSTR ID)
	{
		static BYTE parms[] = VTS_BSTR ;
		InvokeHelper(0x1e3, DISPATCH_METHOD, VT_EMPTY, NULL, parms, ID);
	}
	void Compare(LPCTSTR Name, VARIANT * AuthorName, VARIANT * CompareTarget, VARIANT * DetectFormatChanges, VARIANT * IgnoreAllComparisonWarnings, VARIANT * AddToRecentFiles, VARIANT * RemovePersonalInformation, VARIANT * RemoveDateAndTime)
	{
		static BYTE parms[] = VTS_BSTR VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT VTS_PVARIANT ;
		InvokeHelper(0x1e5, DISPATCH_METHOD, VT_EMPTY, NULL, parms, Name, AuthorName, CompareTarget, DetectFormatChanges, IgnoreAllComparisonWarnings, AddToRecentFiles, RemovePersonalInformation, RemoveDateAndTime);
	}
	void RemoveLockedStyles()
	{
		InvokeHelper(0x1e7, DISPATCH_METHOD, VT_EMPTY, NULL, NULL);
	}
	LPDISPATCH get_ChildNodeSuggestions()
	{
		LPDISPATCH result;
		InvokeHelper(0x1e6, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	LPDISPATCH SelectSingleNode(LPCTSTR XPath, LPCTSTR PrefixMapping, BOOL FastSearchSkippingTextNodes)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BOOL ;
		InvokeHelper(0x1e8, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, XPath, PrefixMapping, FastSearchSkippingTextNodes);
		return result;
	}
	LPDISPATCH SelectNodes(LPCTSTR XPath, LPCTSTR PrefixMapping, BOOL FastSearchSkippingTextNodes)
	{
		LPDISPATCH result;
		static BYTE parms[] = VTS_BSTR VTS_BSTR VTS_BOOL ;
		InvokeHelper(0x1e9, DISPATCH_METHOD, VT_DISPATCH, (void*)&result, parms, XPath, PrefixMapping, FastSearchSkippingTextNodes);
		return result;
	}
	LPDISPATCH get_XMLSchemaViolations()
	{
		LPDISPATCH result;
		InvokeHelper(0x1ea, DISPATCH_PROPERTYGET, VT_DISPATCH, (void*)&result, NULL);
		return result;
	}
	long get_ReadingLayoutSizeX()
	{
		long result;
		InvokeHelper(0x1eb, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_ReadingLayoutSizeX(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x1eb, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
	long get_ReadingLayoutSizeY()
	{
		long result;
		InvokeHelper(0x1ec, DISPATCH_PROPERTYGET, VT_I4, (void*)&result, NULL);
		return result;
	}
	void put_ReadingLayoutSizeY(long newValue)
	{
		static BYTE parms[] = VTS_I4 ;
		InvokeHelper(0x1ec, DISPATCH_PROPERTYPUT, VT_EMPTY, NULL, parms, newValue);
	}
 */
	// _Document properties
public:

};
