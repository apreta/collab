//
// ms_word.cpp : Defines the entry point for the MS Word DLL
//


#include <wx/wx.h>
#include <wx/dir.h>

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <windows.h>
#include <winspool.h>
#include <process.h>

#include <wx/app.h>
#include <wx/msw/registry.h>

#include <wx/file.h>
#include <wx/filename.h>
#include <wx/msgdlg.h>
#include <wx/metafile.h>
#include <wx/process.h>

#include "../../sharelib/procutil.h"

#ifdef MS_WORD_EXPORTS
#define MS_WORD_API __declspec(dllexport)
#else
#define MS_WORD_API __declspec(dllimport)
#endif

#include "msword.h"


extern "C" MS_WORD_API int dsGetRelPri( void );
extern "C" MS_WORD_API int dsGetPriLen( void );
extern "C" MS_WORD_API int dsGetPriTypes( char *pC, int iMaxLen );
extern "C" MS_WORD_API int dsGetSecLen( void );
extern "C" MS_WORD_API int dsGetSecTypes( char *pC, int iMaxLen );

extern "C" MS_WORD_API int dsGetStatus( void );
extern "C" MS_WORD_API int dsGetPreLoadExeCmdLen( void );
extern "C" MS_WORD_API int dsGetPreLoadExeCmd( char *pC, int iMaxLen );
extern "C" MS_WORD_API int dsGetPostLoadKillCmdLen( void );
extern "C" MS_WORD_API int dsGetPostLoadKillCmd( char *pC, int iMaxLen );
extern "C" MS_WORD_API int dsPreLoad( );
extern "C" MS_WORD_API int dsLoad( char *pFN, unsigned long ulHWnd );
extern "C" MS_WORD_API int dsPostLoad( );
extern "C" MS_WORD_API int dsGetNumPages( void );
extern "C" MS_WORD_API int dsGetTempPath( char *pC, int iMaxLen );

extern "C" MS_WORD_API void dsAbort( );


int             dsRelPri    = 1000;
int             dsStatus    = 0;
int             gNumPages   = 0;
bool            gfAbort     = false;
wxString        goBuf;              //  Just the path to the folder
wxString        gFN;

bool            gf4x        = true;

char    strPriTypes[] = {"MS Word documents(*.doc)|*.doc|Document templates(*.dot)|*.dot"};
char    strSecTypes[] = {""};
char    strPreLoadExeCmd[]  = {""};
char    strPostLoadKillCmd[]= {"WINWORD.EXE"};


//  This code implements the PDF-to-PNG conversion using the Ghostscript API
////*****************************************************************************
////  Example of using GS DLL as a ps2pdf converter.
////  Copied from GhostScript file \doc\API.htm
////*****************************************************************************
//
//#if defined(_WIN32) && !defined(_Windows)
//# define _Windows
//#endif
//
//#ifdef _Windows
//// add this source to a project with gsdll32.dll, or compile it directly with:
////    cl -D_Windows -Isrc -Febin\ps2pdf.exe ps2pdf.c bin\gsdll32.lib
//
////# include <windows.h>
//# define GSDLLEXPORT __declspec(dllimport)
//#endif
//
//#include "ierrors.h"
//#include "iapi.h"
//
//void *minst;


//*****************************************************************************
//static int GSDLLCALL gsPoll( void *pVoid )
//{
//    return( (gfAbort) ? 1 : 0 );
//}


//*****************************************************************************
//static int GSDLLCALL gsStdOut( void *pVoid, const char *str, int iLen )
//{
//    wxString    strRest;
//    wxString    strT( str, wxConvUTF8 );
//    strT.Truncate( iLen );
//    if( strT.StartsWith( wxT("Page"), &strRest ) )
//    {
//        long        lValue;
//        strRest.Trim( true );
//        strRest.Trim( false );
//        if( strRest.ToLong( &lValue ) )
//            wxLogDebug( wxT("Entering test_stdout( ) - found page number %d"), lValue );
//    }
//    return iLen;
//}


//*****************************************************************************
int gsPDF2PNG( wxString &strPDF, wxString &strTarget )
{
    wxString    strPercent( wxT("\%") );
    wxString    strFormatLo( wxT("gswin32c.exe -I.;./gs;./gs/lib -sDEVICE=png16m -r192x192 -o \"%sPage-%s06d.png\" \"%s\"") );
    wxString    strFormatHi( wxT("gswin32c.exe -I.;./gs;./gs/lib -sDEVICE=png16m -r384x384 -o \"%sPage-%s06dhi.png\" \"%s\"") );
    wxString    strArgcLo = wxString::Format( strFormatLo, strTarget.wc_str( ), strPercent.wc_str( ), strPDF.wc_str( ) );
    wxString    strArgcHi = wxString::Format( strFormatHi, strTarget.wc_str( ), strPercent.wc_str( ), strPDF.wc_str( ) );

    long        lRet;
    if( gf4x )
        lRet = ::wxExecute( strArgcHi, wxEXEC_SYNC );
    else
        lRet = ::wxExecute( strArgcLo, wxEXEC_SYNC );


    //  This code implements the PDF-to-PNG conversion using the Ghostscript API
//    int         code, code1;
//    const char  *gsargv[ 10 ];
//    int         gsargc;
//    char        outFmt[ 600 ];
//    char        inPDF[ 600 ];
//
////    sprintf( inPDF, "%s", strPDF.mb_str( wxConvUTF8 ) );
//    strcpy( inPDF, strPDF.mb_str( wxConvUTF8 ) );
//
////    sprintf( outFmt, "-o %s\\Page-\%06d.png", strTarget );
//    strcpy( outFmt, "-sOutputFile=" );
//    strcat( outFmt, strTarget.mb_str( wxConvUTF8 ) );
//    if( gf4x )
//        strcat( outFmt, "Page-%06dhi.png" );
//    else
//        strcat( outFmt, "Page-%06d.png" );
//
//    gsargv[ 0 ] = "gswin32c";     /* actual value doesn't matter */
//    gsargv[ 1 ] = "-dNOPAUSE";
//    gsargv[ 2 ] = "-dBATCH";
//    gsargv[ 3 ] = "-sDEVICE=png16m";
//
//    if( gf4x )
//        gsargv[ 4 ] = "-r384x384";
//    else
//        gsargv[ 4 ] = "-r192x192";
//
//    gsargv[ 5 ] = outFmt;
//    gsargv[ 6 ] = "-I.;./gs;./gs/lib";
//    gsargv[ 7 ] = inPDF;
//    gsargc= 8;
//
//    code = gsapi_new_instance( &minst, NULL );
//    if( code  <  0 )
//        return 1;
//
//    code = gsapi_set_poll( minst, gsPoll );
//
//    code = gsapi_set_stdio( minst, NULL, gsStdOut, NULL );
//
//    code = gsapi_init_with_args( minst, gsargc, (char **) gsargv );
//    code1 = gsapi_exit( minst );
//    if( code == 0  || code == e_Quit )
//        code = code1;
//
//    gsapi_delete_instance( minst );
//
//    if( code == 0  ||  code == e_Quit )
//        return 0;

    return 0;   //  Zero return means SUCCESS
}


//*****************************************************************************
BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    gfAbort = false;
    switch (fdwReason)
    {
        case DLL_PROCESS_ATTACH:
            // attach to process
            // return FALSE to fail DLL load
            break;
        
        case DLL_PROCESS_DETACH:
            // detach from process
            break;
        
        case DLL_THREAD_ATTACH:
            // attach to thread
            break;
        
        case DLL_THREAD_DETACH:
            // detach from thread
            break;
    }
    return TRUE; // succesful
}


//*****************************************************************************
//  Sets the internal abort flag which we check as frequently as possible
MS_WORD_API void dsAbort( )
{
    gfAbort = true;
}


//*****************************************************************************
//  This contains the current status/progress of the DS.
MS_WORD_API int dsGetStatus( void )
{
    return dsStatus;
}


//*****************************************************************************
MS_WORD_API int dsGetRelPri( void )
{
    return dsRelPri;
}


//*****************************************************************************
MS_WORD_API int dsGetPriLen( void )
{
    return strlen( strPriTypes );
}


//*****************************************************************************
MS_WORD_API int dsGetSecLen( void )
{
    return strlen( strSecTypes );
}


//*****************************************************************************
MS_WORD_API int dsGetPreLoadExeCmdLen( void )
{
    return strlen( strPreLoadExeCmd );
}


//*****************************************************************************
MS_WORD_API int dsGetPostLoadKillCmdLen( void )
{
    return strlen( strPostLoadKillCmd );
}


//*****************************************************************************
MS_WORD_API int dsGetNumPages( void )
{
    return gNumPages;
}


//*****************************************************************************
MS_WORD_API int dsGetPriTypes( char *pC, int iMaxLen )
{
    int iLen = strlen( strPriTypes );
    if( iLen > iMaxLen )
    {
        strncpy( pC, strPriTypes, iMaxLen - 1 );
        pC[ iMaxLen - 1 ] = 0;
        return -iLen;
    }

    strcpy( pC, strPriTypes );
    pC[ iLen ] = 0;
    return iLen;
}


//*****************************************************************************
MS_WORD_API int dsGetSecTypes( char *pC, int iMaxLen )
{
    int iLen = strlen( strSecTypes );
    if( iLen > iMaxLen )
    {
        strncpy( pC, strSecTypes, iMaxLen - 1 );
        pC[ iMaxLen - 1 ] = 0;
        return -iLen;
    }

    strcpy( pC, strSecTypes );
    pC[ iLen ] = 0;
    return iLen;
}


//*****************************************************************************
MS_WORD_API int dsGetPreLoadExeCmd( char *pC, int iMaxLen )
{
    int iLen = strlen( strPreLoadExeCmd );
    if( iLen > iMaxLen )
    {
        strncpy( pC, strPreLoadExeCmd, iMaxLen - 1 );
        pC[ iMaxLen - 1 ] = 0;
        return -iLen;
    }

    strcpy( pC, strPreLoadExeCmd );
    pC[ iLen ] = 0;
    return iLen;
}


//*****************************************************************************
MS_WORD_API int dsGetPostLoadKillCmd( char *pC, int iMaxLen )
{
    int iLen = strlen( strPostLoadKillCmd );
    if( iLen > iMaxLen )
    {
        strncpy( pC, strPostLoadKillCmd, iMaxLen - 1 );
        pC[ iMaxLen - 1 ] = 0;
        return -iLen;
    }

    strcpy( pC, strPostLoadKillCmd );
    pC[ iLen ] = 0;
    return iLen;
}


//*****************************************************************************
MS_WORD_API int dsGetTempPath( char *pC, int iMaxLen )
{
    int     iLen = goBuf.Len( );
    if( iLen > iMaxLen )
    {
        strncpy( pC, goBuf.mb_str( wxConvUTF8 ), iMaxLen - 1 );
        pC[ iMaxLen - 1 ] = 0;
        return -iLen;
    }

    strcpy( pC, goBuf.mb_str( wxConvUTF8 ) );
    pC[ iLen ] = 0;
    return iLen;
}


//*****************************************************************************
MS_WORD_API int dsPreLoad( )
{
    gfAbort = false;
    dsStatus = 666;     //  Indicate we have completed our work
    return 0;
}


//*****************************************************************************
//  MS Word version 9 is, I believe, Word 2000
static bool versionBefore9( )
{
    wxString    strTitle( _("Upload Error"), wxConvUTF8 );
    wxString    strMsg( _("We are upable to upload documents using versions\nof MS Word older than Word 2003."), wxConvUTF8 );
    wxMessageDialog  msg( NULL, strMsg, strTitle );
    msg.ShowModal( );
    return false;
}


//*****************************************************************************
//  MS Word version 9 is, I believe, Word 2000
static bool version9( )
{
    wxString    strTitle( _("Upload Error"), wxConvUTF8 );
    wxString    strMsg( _("We are upable to upload documents using versions\nof MS Word older than Word 2003."), wxConvUTF8 );
    wxMessageDialog  msg( NULL, strMsg, strTitle );
    msg.ShowModal( );
    return false;
}


//*****************************************************************************
//  MS Word version 10 is, I believe, Word 2002/Word XP
static bool version10( )
{
    wxString    strTitle( _("Upload Error"), wxConvUTF8 );
    wxString    strMsg( _("We are upable to upload documents using versions\nof MS Word older than Word 2003."), wxConvUTF8 );
    wxMessageDialog  msg( NULL, strMsg, strTitle );
    msg.ShowModal( );
    return false;
}


//*****************************************************************************
//  MS Word version 11 is Word 2003
//  Strategy:  Use the "MS Document Image Writer" printer driver to create TIFF images
//    of each page then convert each TIFF to a high resolution PNG.
//  Upload both the TIFF and the PNG to the server.
static bool version11( _Application *pApp, wxString &strOutPath, wxString &strFN )
{
    wxString    strImage( wxT("Microsoft Office Document Image Writer") );
    pApp->put_ActivePrinter( strImage.wc_str( ) );

    Documents   documents   = pApp->get_Documents( );
    _Document   document    = documents.myOpen( strFN.wc_str( ) );

    gNumPages = document.CountPages( );

    wxFileName  fn( strFN );
    strOutPath.Append( wxT("\\") );

    //  Create separate TIFF images for each page of the document
    if( !gfAbort )
    {
        int     i;
        for( i=0; i<gNumPages; i++ )
        {
            if( gfAbort )
                break;

            wxString    strOutTiff( strOutPath );
            strOutTiff.Append( wxString::Format( wxT("Page-%06d.tiff"), i ) );

            wxString    strT;
            strT = wxString::Format( wxT("%d"), i+1 );
            document.myPrintPageToFileOld( strOutTiff.wc_str( ), strT.wc_str( ) );
        }
    }

    //  Print-to-file opens the MS Document Imaging app - kill it
    wxString    strViewerName( wxT("MSPVIEW.EXE") );
    KillNamedProcess( strViewerName.mb_str( wxConvUTF8 ) );

    // Close document
    document.Close( );
    document.ReleaseDispatch( );
    documents.ReleaseDispatch( );

    if( !gfAbort )
    {
        //  Create the index file
        wxString    indexText;
        indexText += wxT("<index>\n");

        //  Open each of the TIFF files and create a high resolution PNG of each
        int     i;
        for( i=0; i<gNumPages; i++ )
        {
            wxString    strTiff( wxString::Format( wxT("Page-%06d.tiff"), i ) );
            wxString    strInFile( strOutPath );
            strInFile.Append( strTiff );
            wxImage     image( strInFile, wxBITMAP_TYPE_TIF );

            wxString    strPng( wxString::Format( wxT("Page-%06d.png"), i ) );
            wxString    strOutFile( strOutPath );
            strOutFile.Append( strPng );

            if( gfAbort )
                break;

            image.SaveFile( strOutFile, wxBITMAP_TYPE_PNG );

            wxString    page;
            page = wxString::Format( wxT("<page title=\"Slide %d\" file=\"%s\" altfile=\"%s\" scale=\"1.0\" altscale=\"1.0\" fittowidth=\"T\"/>\n"), i, strTiff.c_str( ), strPng.c_str( ) );
            indexText += page;

            if( gfAbort )
                break;
        }

        indexText += wxT("</index>\n");

        wxString    strOutIndex( strOutPath );
        strOutIndex.Append( wxT("index.xml") );

        if( !gfAbort )
        {
            wxFile  indexFile( strOutIndex, wxFile::write );
            indexFile.Write( indexText );
            indexFile.Close( );
        }
    }
    return true;
}


//*****************************************************************************
//  MS Word version 12 is Word 2007
//  Strategy:  Save the document as a PDF then convert each page to medium and high resolution PNGs.
//  Upload both of the PNG images to the server
static bool version12( _Application *pApp, wxString &strOutPath, wxString &strFN )
{
    Documents   documents   = pApp->get_Documents( );
    _Document   document    = documents.myOpen( strFN.wc_str( ) );

    gNumPages = document.CountPages( );

    wxFileName  fn( strFN );
    strOutPath.Append( wxT("\\") );

    wxString    strPDF( fn.GetName( ) );
    strPDF.Append( wxT(".PDF") );
    wxString    strOutFile( strOutPath );
    strOutFile.Append( strPDF );

    //  Save the document as a PDF
    if( !gfAbort )
        document.SaveAsPdf( strOutFile.wc_str( ) );

    //  The save operation opens the MS Document Imaging app - kill it
    wxString    strViewerName( wxT("MSPVIEW.EXE") );
    KillNamedProcess( strViewerName.mb_str( wxConvUTF8 ) );

    //  Close the document
    document.Close( );
    document.ReleaseDispatch( );
    documents.ReleaseDispatch( );

    int     iRet;

    //  Convert the PDF to a series of high-resolution PNGs - one for each page
    gf4x = true;
    iRet = gsPDF2PNG( strOutFile, strOutPath );

    //  Convert the PDF to a series of medium-resolution PNGs - one for each page
    gf4x = false;
    iRet = gsPDF2PNG( strOutFile, strOutPath );
    if( iRet  !=  0 )
    {
        gfAbort = true;
        return false;
    }

    if( !gfAbort )
    {
        //  Create the index file
        wxString    indexText;
        indexText += wxT("<index>\n");

        int     i;
        for( i=0; i<gNumPages; i++ )
        {
            wxString    strPNGhi( wxString::Format( wxT("Page-%06dhi.png"), i+1 ) );
            wxString    strPNG( wxString::Format( wxT("Page-%06d.png"), i+1 ) );

            wxString    page;
            page = wxString::Format( wxT("<page title=\"Slide %d\" file=\"%s\" altfile=\"%s\" scale=\"2.0\" altscale=\"1.0\" fittowidth=\"T\"/>\n"), i, strPNGhi.c_str( ), strPNG.c_str( ) );
            indexText += page;

            if( gfAbort )
                break;
        }

        indexText += wxT("</index>\n");

        wxString    strIndex( goBuf, wxConvUTF8 );
        strIndex.Append( wxT("\\index.xml") );

        if( !gfAbort )
        {
            wxFile  indexFile( strIndex, wxFile::write );
            indexFile.Write( indexText );
            indexFile.Close( );
        }
    }

    //  Remove the original PDF file
//    ::wxRemoveFile( strOutFile );

    return true;
}


//*****************************************************************************
unsigned long WINAPI ThreadFunc( LPVOID lpParam )
{
    bool        fRet;

    //  Find our temp output folder - creating it if necessary - and empty it
    wxString    strOutPath;
    strOutPath = wxGetCwd( );
    strOutPath.Append( wxT("\\dspages") );

    if( !wxDir::Exists( strOutPath ) )
        if( !::wxMkdir( strOutPath ) )
        {
            //  Folder doesn't exist and can't be created - time to admit defeat
            wxLogDebug( wxT("Cannot create folder for temp images during upload") );
            dsStatus = 666;     //  Indicate we have completed our work
            return 0;
        }

    goBuf.Empty( );
    goBuf.Append( strOutPath );

    if( !gfAbort )
    {
        wxString    fileSpec( wxT("*.*") );
        wxString    fileName;
        wxDir       dir( strOutPath );
        if( dir.IsOpened( ) )
        {
            bool fMore = dir.GetFirst( &fileName, fileSpec, wxDIR_FILES );
            while( fMore )
            {
                wxString    strFull = strOutPath + wxT("\\") + fileName;
                ::wxRemoveFile( strFull );
                fMore = dir.GetNext( &fileName );
            }
        }
    }

    CoInitializeEx( NULL, COINIT_APARTMENTTHREADED );

    // Start Word
    _Application    app;
    if( !app.CreateDispatch( wxT("Word.Application") ) )
    {
        wxLogDebug( wxT("Unknown error returned by app.CreateDispatch( )") );
        dsStatus = 666;     //  Indicate we have completed our work
        return 0;
    }

    double      dVersion;
    wxString    strVersion;
    strVersion = app.get_Version( );
    fRet = strVersion.ToDouble( &dVersion );
    if( fRet )
    {
        int iVersion =  ((int) dVersion);
        if( iVersion  <  9 )
            fRet = versionBefore9( );
        else if( iVersion  ==  9 )
            fRet = version9( );
        else if( iVersion  ==  10 )
            fRet = version10( );
        else if( iVersion  ==  11 )
            fRet = version11( &app, strOutPath, gFN );
        else if( iVersion  >=  12 )
            fRet = version12( &app, strOutPath, gFN );
    }

    app.ReleaseDispatch( );

    dsStatus = 666;     //  Indicate we have completed our work
    return 1;
}


//*****************************************************************************
MS_WORD_API int dsLoad( char *pFN, unsigned long ulWnd )
{
    gfAbort = false;
    dsStatus = 0;

    wxString    strFN( pFN, wxConvUTF8 );
    gFN.Empty( );
    gFN.Append( strFN );

    unsigned long   dwThreadParam   = 1;
    unsigned long   dwThreadID      = 1;

    CreateThread( NULL, 0, ThreadFunc, &dwThreadParam, 0, &dwThreadID );

    return 1;
}


//*****************************************************************************
MS_WORD_API int dsPostLoad( )
{
    gfAbort = false;
    dsStatus = 666;     //  Indicate we have completed our work
    return 0;
}
