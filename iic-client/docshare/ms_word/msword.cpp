// Machine generated IDispatch wrapper class(es) created with ClassWizard

#include <wx/wx.h>

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <windows.h>
#include <winspool.h>
#include <process.h>
#include <objbase.h>

#include <wx/app.h>
#include <wx/msw/registry.h>

#include <wx/filename.h>

#include <wx/debug.h>


#ifdef MS_WORD_EXPORTS
#define MS_WORD_API __declspec(dllexport)
#else
#define MS_WORD_API __declspec(dllimport)
#endif

#include "msword.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#define _countof(array) (sizeof(array)/sizeof(array[0]))

inline LPCOLESTR T2COLE(LPCTSTR lp) { return lp; }



//***********************************************************************
static void AfxBSTR2wxString( wxString *pStr, BSTR bstr )
{
    wxASSERT( pStr != NULL);
#if defined(_UNICODE)
    wxString    strT( bstr );
    pStr->Empty( );
    pStr->Append( strT );
#else
    wxASSERT( false );
#endif
}


//***********************************************************************
DWORD _AfxRelease(LPUNKNOWN* lplpUnknown)
{
    wxASSERT(lplpUnknown != NULL);
    if (*lplpUnknown != NULL)
    {
        DWORD dwRef = (*lplpUnknown)->Release();
        *lplpUnknown = NULL;
        return dwRef;
    }
    return 0;
}


//***********************************************************************
HRESULT AfxGetClassIDFromString(LPCTSTR lpsz, LPCLSID lpClsID)
{
//    USES_CONVERSION;

    HRESULT hr;
    if (lpsz[0] == '{')
        hr = CLSIDFromString((LPOLESTR)T2COLE(lpsz), lpClsID);
    else
        hr = CLSIDFromProgID(T2COLE(lpsz), lpClsID);
    return hr;
}


//***********************************************************************
static void AfxVariantInit(LPVARIANT pVar)
{
    memset(pVar, 0, sizeof(*pVar));
}


#ifdef _DEBUG

//***********************************************************************
static LPCTSTR AfxGetSeverityString(SCODE sc)
{
    static const TCHAR* rgszSEVERITY[] =
    {
        _T("SEVERITY_SUCCESS"),
        _T("SEVERITY_ERROR"),
    };
    return rgszSEVERITY[SCODE_SEVERITY(sc)];
}


//***********************************************************************
static LPCTSTR AfxGetFacilityString(SCODE sc)
{
    static const TCHAR* rgszFACILITY[] =
    {
        _T("FACILITY_NULL"),
        _T("FACILITY_RPC"),
        _T("FACILITY_DISPATCH"),
        _T("FACILITY_STORAGE"),
        _T("FACILITY_ITF"),
        _T("FACILITY_0x05"),
        _T("FACILITY_0x06"),
        _T("FACILITY_WIN32"),
        _T("FACILITY_WINDOWS"),
    };
    if( SCODE_FACILITY(sc)  >=  (int) _countof(rgszFACILITY))
        return _T("<Unknown Facility>");

    return rgszFACILITY[SCODE_FACILITY(sc)];
}


//***********************************************************************
static LPCTSTR AfxGetScodeRangeString(SCODE sc)
{
    struct RANGE_ENTRY
    {
        SCODE scFirst;
        SCODE scLast;
        LPCTSTR lpszName;
    };
#define MAKE_RANGE_ENTRY(scRange) \
    { scRange##_FIRST, scRange##_LAST, \
    _T(#scRange) _T("_FIRST...") _T(#scRange) _T("_LAST") }

    static const RANGE_ENTRY scRangeTable[] =
    {
        MAKE_RANGE_ENTRY(CACHE_E),
        MAKE_RANGE_ENTRY(CACHE_S),
        MAKE_RANGE_ENTRY(CLASSFACTORY_E),
        MAKE_RANGE_ENTRY(CLASSFACTORY_S),
        MAKE_RANGE_ENTRY(CLIENTSITE_E),
        MAKE_RANGE_ENTRY(CLIENTSITE_S),
        MAKE_RANGE_ENTRY(CLIPBRD_E),
        MAKE_RANGE_ENTRY(CLIPBRD_S),
        MAKE_RANGE_ENTRY(CONVERT10_E),
        MAKE_RANGE_ENTRY(CONVERT10_S),
        MAKE_RANGE_ENTRY(CO_E),
        MAKE_RANGE_ENTRY(CO_S),
        MAKE_RANGE_ENTRY(DATA_E),
        MAKE_RANGE_ENTRY(DATA_S),
        MAKE_RANGE_ENTRY(DRAGDROP_E),
        MAKE_RANGE_ENTRY(DRAGDROP_S),
        MAKE_RANGE_ENTRY(ENUM_E),
        MAKE_RANGE_ENTRY(ENUM_S),
        MAKE_RANGE_ENTRY(INPLACE_E),
        MAKE_RANGE_ENTRY(INPLACE_S),
        MAKE_RANGE_ENTRY(MARSHAL_E),
        MAKE_RANGE_ENTRY(MARSHAL_S),
        MAKE_RANGE_ENTRY(MK_E),
        MAKE_RANGE_ENTRY(MK_S),
        MAKE_RANGE_ENTRY(OLEOBJ_E),
        MAKE_RANGE_ENTRY(OLEOBJ_S),
        MAKE_RANGE_ENTRY(OLE_E),
        MAKE_RANGE_ENTRY(OLE_S),
        MAKE_RANGE_ENTRY(REGDB_E),
        MAKE_RANGE_ENTRY(REGDB_S),
        MAKE_RANGE_ENTRY(VIEW_E),
        MAKE_RANGE_ENTRY(VIEW_S),
    };
#undef MAKE_RANGE_ENTRY

    // look for it in the table
    for( int i = 0; i < (int) _countof(scRangeTable); i++ )
    {
        if (sc >= scRangeTable[i].scFirst && sc <= scRangeTable[i].scLast)
            return scRangeTable[i].lpszName;
    }
    return NULL;    // not found
}


//***********************************************************************
static LPCTSTR AfxGetScodeString(SCODE sc)
{
	struct SCODE_ENTRY
	{
		SCODE sc;
		LPCTSTR lpszName;
	};
	#define MAKE_SCODE_ENTRY(sc)    { sc, _T(#sc) }
	static const SCODE_ENTRY scNameTable[] =
	{
		MAKE_SCODE_ENTRY(S_OK),
		MAKE_SCODE_ENTRY(S_FALSE),

		MAKE_SCODE_ENTRY(CACHE_S_FORMATETC_NOTSUPPORTED),
		MAKE_SCODE_ENTRY(CACHE_S_SAMECACHE),
		MAKE_SCODE_ENTRY(CACHE_S_SOMECACHES_NOTUPDATED),
		MAKE_SCODE_ENTRY(CONVERT10_S_NO_PRESENTATION),
		MAKE_SCODE_ENTRY(DATA_S_SAMEFORMATETC),
		MAKE_SCODE_ENTRY(DRAGDROP_S_CANCEL),
		MAKE_SCODE_ENTRY(DRAGDROP_S_DROP),
		MAKE_SCODE_ENTRY(DRAGDROP_S_USEDEFAULTCURSORS),
		MAKE_SCODE_ENTRY(INPLACE_S_TRUNCATED),
		MAKE_SCODE_ENTRY(MK_S_HIM),
		MAKE_SCODE_ENTRY(MK_S_ME),
		MAKE_SCODE_ENTRY(MK_S_MONIKERALREADYREGISTERED),
		MAKE_SCODE_ENTRY(MK_S_REDUCED_TO_SELF),
		MAKE_SCODE_ENTRY(MK_S_US),
		MAKE_SCODE_ENTRY(OLE_S_MAC_CLIPFORMAT),
		MAKE_SCODE_ENTRY(OLE_S_STATIC),
		MAKE_SCODE_ENTRY(OLE_S_USEREG),
		MAKE_SCODE_ENTRY(OLEOBJ_S_CANNOT_DOVERB_NOW),
		MAKE_SCODE_ENTRY(OLEOBJ_S_INVALIDHWND),
		MAKE_SCODE_ENTRY(OLEOBJ_S_INVALIDVERB),
		MAKE_SCODE_ENTRY(OLEOBJ_S_LAST),
		MAKE_SCODE_ENTRY(STG_S_CONVERTED),
		MAKE_SCODE_ENTRY(VIEW_S_ALREADY_FROZEN),

		MAKE_SCODE_ENTRY(E_UNEXPECTED),
		MAKE_SCODE_ENTRY(E_NOTIMPL),
		MAKE_SCODE_ENTRY(E_OUTOFMEMORY),
		MAKE_SCODE_ENTRY(E_INVALIDARG),
		MAKE_SCODE_ENTRY(E_NOINTERFACE),
		MAKE_SCODE_ENTRY(E_POINTER),
		MAKE_SCODE_ENTRY(E_HANDLE),
		MAKE_SCODE_ENTRY(E_ABORT),
		MAKE_SCODE_ENTRY(E_FAIL),
		MAKE_SCODE_ENTRY(E_ACCESSDENIED),

		MAKE_SCODE_ENTRY(CACHE_E_NOCACHE_UPDATED),
		MAKE_SCODE_ENTRY(CLASS_E_CLASSNOTAVAILABLE),
		MAKE_SCODE_ENTRY(CLASS_E_NOAGGREGATION),
		MAKE_SCODE_ENTRY(CLIPBRD_E_BAD_DATA),
		MAKE_SCODE_ENTRY(CLIPBRD_E_CANT_CLOSE),
		MAKE_SCODE_ENTRY(CLIPBRD_E_CANT_EMPTY),
		MAKE_SCODE_ENTRY(CLIPBRD_E_CANT_OPEN),
		MAKE_SCODE_ENTRY(CLIPBRD_E_CANT_SET),
		MAKE_SCODE_ENTRY(CO_E_ALREADYINITIALIZED),
		MAKE_SCODE_ENTRY(CO_E_APPDIDNTREG),
		MAKE_SCODE_ENTRY(CO_E_APPNOTFOUND),
		MAKE_SCODE_ENTRY(CO_E_APPSINGLEUSE),
		MAKE_SCODE_ENTRY(CO_E_BAD_PATH),
		MAKE_SCODE_ENTRY(CO_E_CANTDETERMINECLASS),
		MAKE_SCODE_ENTRY(CO_E_CLASS_CREATE_FAILED),
		MAKE_SCODE_ENTRY(CO_E_CLASSSTRING),
		MAKE_SCODE_ENTRY(CO_E_DLLNOTFOUND),
		MAKE_SCODE_ENTRY(CO_E_ERRORINAPP),
		MAKE_SCODE_ENTRY(CO_E_ERRORINDLL),
		MAKE_SCODE_ENTRY(CO_E_IIDSTRING),
		MAKE_SCODE_ENTRY(CO_E_NOTINITIALIZED),
		MAKE_SCODE_ENTRY(CO_E_OBJISREG),
		MAKE_SCODE_ENTRY(CO_E_OBJNOTCONNECTED),
		MAKE_SCODE_ENTRY(CO_E_OBJNOTREG),
		MAKE_SCODE_ENTRY(CO_E_OBJSRV_RPC_FAILURE),
		MAKE_SCODE_ENTRY(CO_E_SCM_ERROR),
		MAKE_SCODE_ENTRY(CO_E_SCM_RPC_FAILURE),
		MAKE_SCODE_ENTRY(CO_E_SERVER_EXEC_FAILURE),
		MAKE_SCODE_ENTRY(CO_E_SERVER_STOPPING),
		MAKE_SCODE_ENTRY(CO_E_WRONGOSFORAPP),
		MAKE_SCODE_ENTRY(CONVERT10_E_OLESTREAM_BITMAP_TO_DIB),
		MAKE_SCODE_ENTRY(CONVERT10_E_OLESTREAM_FMT),
		MAKE_SCODE_ENTRY(CONVERT10_E_OLESTREAM_GET),
		MAKE_SCODE_ENTRY(CONVERT10_E_OLESTREAM_PUT),
		MAKE_SCODE_ENTRY(CONVERT10_E_STG_DIB_TO_BITMAP),
		MAKE_SCODE_ENTRY(CONVERT10_E_STG_FMT),
		MAKE_SCODE_ENTRY(CONVERT10_E_STG_NO_STD_STREAM),
		MAKE_SCODE_ENTRY(DISP_E_ARRAYISLOCKED),
		MAKE_SCODE_ENTRY(DISP_E_BADCALLEE),
		MAKE_SCODE_ENTRY(DISP_E_BADINDEX),
		MAKE_SCODE_ENTRY(DISP_E_BADPARAMCOUNT),
		MAKE_SCODE_ENTRY(DISP_E_BADVARTYPE),
		MAKE_SCODE_ENTRY(DISP_E_EXCEPTION),
		MAKE_SCODE_ENTRY(DISP_E_MEMBERNOTFOUND),
		MAKE_SCODE_ENTRY(DISP_E_NONAMEDARGS),
		MAKE_SCODE_ENTRY(DISP_E_NOTACOLLECTION),
		MAKE_SCODE_ENTRY(DISP_E_OVERFLOW),
		MAKE_SCODE_ENTRY(DISP_E_PARAMNOTFOUND),
		MAKE_SCODE_ENTRY(DISP_E_PARAMNOTOPTIONAL),
		MAKE_SCODE_ENTRY(DISP_E_TYPEMISMATCH),
		MAKE_SCODE_ENTRY(DISP_E_UNKNOWNINTERFACE),
		MAKE_SCODE_ENTRY(DISP_E_UNKNOWNLCID),
		MAKE_SCODE_ENTRY(DISP_E_UNKNOWNNAME),
		MAKE_SCODE_ENTRY(DRAGDROP_E_ALREADYREGISTERED),
		MAKE_SCODE_ENTRY(DRAGDROP_E_INVALIDHWND),
		MAKE_SCODE_ENTRY(DRAGDROP_E_NOTREGISTERED),
		MAKE_SCODE_ENTRY(DV_E_CLIPFORMAT),
		MAKE_SCODE_ENTRY(DV_E_DVASPECT),
		MAKE_SCODE_ENTRY(DV_E_DVTARGETDEVICE),
		MAKE_SCODE_ENTRY(DV_E_DVTARGETDEVICE_SIZE),
		MAKE_SCODE_ENTRY(DV_E_FORMATETC),
		MAKE_SCODE_ENTRY(DV_E_LINDEX),
		MAKE_SCODE_ENTRY(DV_E_NOIVIEWOBJECT),
		MAKE_SCODE_ENTRY(DV_E_STATDATA),
		MAKE_SCODE_ENTRY(DV_E_STGMEDIUM),
		MAKE_SCODE_ENTRY(DV_E_TYMED),
		MAKE_SCODE_ENTRY(INPLACE_E_NOTOOLSPACE),
		MAKE_SCODE_ENTRY(INPLACE_E_NOTUNDOABLE),
		MAKE_SCODE_ENTRY(MEM_E_INVALID_LINK),
		MAKE_SCODE_ENTRY(MEM_E_INVALID_ROOT),
		MAKE_SCODE_ENTRY(MEM_E_INVALID_SIZE),
		MAKE_SCODE_ENTRY(MK_E_CANTOPENFILE),
		MAKE_SCODE_ENTRY(MK_E_CONNECTMANUALLY),
		MAKE_SCODE_ENTRY(MK_E_ENUMERATION_FAILED),
		MAKE_SCODE_ENTRY(MK_E_EXCEEDEDDEADLINE),
		MAKE_SCODE_ENTRY(MK_E_INTERMEDIATEINTERFACENOTSUPPORTED),
		MAKE_SCODE_ENTRY(MK_E_INVALIDEXTENSION),
		MAKE_SCODE_ENTRY(MK_E_MUSTBOTHERUSER),
		MAKE_SCODE_ENTRY(MK_E_NEEDGENERIC),
		MAKE_SCODE_ENTRY(MK_E_NO_NORMALIZED),
		MAKE_SCODE_ENTRY(MK_E_NOINVERSE),
		MAKE_SCODE_ENTRY(MK_E_NOOBJECT),
		MAKE_SCODE_ENTRY(MK_E_NOPREFIX),
		MAKE_SCODE_ENTRY(MK_E_NOSTORAGE),
		MAKE_SCODE_ENTRY(MK_E_NOTBINDABLE),
		MAKE_SCODE_ENTRY(MK_E_NOTBOUND),
		MAKE_SCODE_ENTRY(MK_E_SYNTAX),
		MAKE_SCODE_ENTRY(MK_E_UNAVAILABLE),
		MAKE_SCODE_ENTRY(OLE_E_ADVF),
		MAKE_SCODE_ENTRY(OLE_E_ADVISENOTSUPPORTED),
		MAKE_SCODE_ENTRY(OLE_E_BLANK),
		MAKE_SCODE_ENTRY(OLE_E_CANT_BINDTOSOURCE),
		MAKE_SCODE_ENTRY(OLE_E_CANT_GETMONIKER),
		MAKE_SCODE_ENTRY(OLE_E_CANTCONVERT),
		MAKE_SCODE_ENTRY(OLE_E_CLASSDIFF),
		MAKE_SCODE_ENTRY(OLE_E_ENUM_NOMORE),
		MAKE_SCODE_ENTRY(OLE_E_INVALIDHWND),
		MAKE_SCODE_ENTRY(OLE_E_INVALIDRECT),
		MAKE_SCODE_ENTRY(OLE_E_NOCACHE),
		MAKE_SCODE_ENTRY(OLE_E_NOCONNECTION),
		MAKE_SCODE_ENTRY(OLE_E_NOSTORAGE),
		MAKE_SCODE_ENTRY(OLE_E_NOT_INPLACEACTIVE),
		MAKE_SCODE_ENTRY(OLE_E_NOTRUNNING),
		MAKE_SCODE_ENTRY(OLE_E_OLEVERB),
		MAKE_SCODE_ENTRY(OLE_E_PROMPTSAVECANCELLED),
		MAKE_SCODE_ENTRY(OLE_E_STATIC),
		MAKE_SCODE_ENTRY(OLE_E_WRONGCOMPOBJ),
		MAKE_SCODE_ENTRY(OLEOBJ_E_INVALIDVERB),
		MAKE_SCODE_ENTRY(OLEOBJ_E_NOVERBS),
		MAKE_SCODE_ENTRY(REGDB_E_CLASSNOTREG),
		MAKE_SCODE_ENTRY(REGDB_E_IIDNOTREG),
		MAKE_SCODE_ENTRY(REGDB_E_INVALIDVALUE),
		MAKE_SCODE_ENTRY(REGDB_E_KEYMISSING),
		MAKE_SCODE_ENTRY(REGDB_E_READREGDB),
		MAKE_SCODE_ENTRY(REGDB_E_WRITEREGDB),
		MAKE_SCODE_ENTRY(RPC_E_ATTEMPTED_MULTITHREAD),
		MAKE_SCODE_ENTRY(RPC_E_CALL_CANCELED),
		MAKE_SCODE_ENTRY(RPC_E_CALL_REJECTED),
		MAKE_SCODE_ENTRY(RPC_E_CANTCALLOUT_AGAIN),
		MAKE_SCODE_ENTRY(RPC_E_CANTCALLOUT_INASYNCCALL),
		MAKE_SCODE_ENTRY(RPC_E_CANTCALLOUT_INEXTERNALCALL),
		MAKE_SCODE_ENTRY(RPC_E_CANTCALLOUT_ININPUTSYNCCALL),
		MAKE_SCODE_ENTRY(RPC_E_CANTPOST_INSENDCALL),
		MAKE_SCODE_ENTRY(RPC_E_CANTTRANSMIT_CALL),
		MAKE_SCODE_ENTRY(RPC_E_CHANGED_MODE),
		MAKE_SCODE_ENTRY(RPC_E_CLIENT_CANTMARSHAL_DATA),
		MAKE_SCODE_ENTRY(RPC_E_CLIENT_CANTUNMARSHAL_DATA),
		MAKE_SCODE_ENTRY(RPC_E_CLIENT_DIED),
		MAKE_SCODE_ENTRY(RPC_E_CONNECTION_TERMINATED),
		MAKE_SCODE_ENTRY(RPC_E_DISCONNECTED),
		MAKE_SCODE_ENTRY(RPC_E_FAULT),
		MAKE_SCODE_ENTRY(RPC_E_INVALID_CALLDATA),
		MAKE_SCODE_ENTRY(RPC_E_INVALID_DATA),
		MAKE_SCODE_ENTRY(RPC_E_INVALID_DATAPACKET),
		MAKE_SCODE_ENTRY(RPC_E_INVALID_PARAMETER),
		MAKE_SCODE_ENTRY(RPC_E_INVALIDMETHOD),
		MAKE_SCODE_ENTRY(RPC_E_NOT_REGISTERED),
		MAKE_SCODE_ENTRY(RPC_E_OUT_OF_RESOURCES),
		MAKE_SCODE_ENTRY(RPC_E_RETRY),
		MAKE_SCODE_ENTRY(RPC_E_SERVER_CANTMARSHAL_DATA),
		MAKE_SCODE_ENTRY(RPC_E_SERVER_CANTUNMARSHAL_DATA),
		MAKE_SCODE_ENTRY(RPC_E_SERVER_DIED),
		MAKE_SCODE_ENTRY(RPC_E_SERVER_DIED_DNE),
		MAKE_SCODE_ENTRY(RPC_E_SERVERCALL_REJECTED),
		MAKE_SCODE_ENTRY(RPC_E_SERVERCALL_RETRYLATER),
		MAKE_SCODE_ENTRY(RPC_E_SERVERFAULT),
		MAKE_SCODE_ENTRY(RPC_E_SYS_CALL_FAILED),
		MAKE_SCODE_ENTRY(RPC_E_THREAD_NOT_INIT),
		MAKE_SCODE_ENTRY(RPC_E_UNEXPECTED),
		MAKE_SCODE_ENTRY(RPC_E_WRONG_THREAD),
		MAKE_SCODE_ENTRY(STG_E_ABNORMALAPIEXIT),
		MAKE_SCODE_ENTRY(STG_E_ACCESSDENIED),
		MAKE_SCODE_ENTRY(STG_E_CANTSAVE),
		MAKE_SCODE_ENTRY(STG_E_DISKISWRITEPROTECTED),
		MAKE_SCODE_ENTRY(STG_E_EXTANTMARSHALLINGS),
		MAKE_SCODE_ENTRY(STG_E_FILEALREADYEXISTS),
		MAKE_SCODE_ENTRY(STG_E_FILENOTFOUND),
		MAKE_SCODE_ENTRY(STG_E_INSUFFICIENTMEMORY),
		MAKE_SCODE_ENTRY(STG_E_INUSE),
		MAKE_SCODE_ENTRY(STG_E_INVALIDFLAG),
		MAKE_SCODE_ENTRY(STG_E_INVALIDFUNCTION),
		MAKE_SCODE_ENTRY(STG_E_INVALIDHANDLE),
		MAKE_SCODE_ENTRY(STG_E_INVALIDHEADER),
		MAKE_SCODE_ENTRY(STG_E_INVALIDNAME),
		MAKE_SCODE_ENTRY(STG_E_INVALIDPARAMETER),
		MAKE_SCODE_ENTRY(STG_E_INVALIDPOINTER),
		MAKE_SCODE_ENTRY(STG_E_LOCKVIOLATION),
		MAKE_SCODE_ENTRY(STG_E_MEDIUMFULL),
		MAKE_SCODE_ENTRY(STG_E_NOMOREFILES),
		MAKE_SCODE_ENTRY(STG_E_NOTCURRENT),
		MAKE_SCODE_ENTRY(STG_E_NOTFILEBASEDSTORAGE),
		MAKE_SCODE_ENTRY(STG_E_OLDDLL),
		MAKE_SCODE_ENTRY(STG_E_OLDFORMAT),
		MAKE_SCODE_ENTRY(STG_E_PATHNOTFOUND),
		MAKE_SCODE_ENTRY(STG_E_READFAULT),
		MAKE_SCODE_ENTRY(STG_E_REVERTED),
		MAKE_SCODE_ENTRY(STG_E_SEEKERROR),
		MAKE_SCODE_ENTRY(STG_E_SHAREREQUIRED),
		MAKE_SCODE_ENTRY(STG_E_SHAREVIOLATION),
		MAKE_SCODE_ENTRY(STG_E_TOOMANYOPENFILES),
		MAKE_SCODE_ENTRY(STG_E_UNIMPLEMENTEDFUNCTION),
		MAKE_SCODE_ENTRY(STG_E_UNKNOWN),
		MAKE_SCODE_ENTRY(STG_E_WRITEFAULT),
		MAKE_SCODE_ENTRY(TYPE_E_AMBIGUOUSNAME),
		MAKE_SCODE_ENTRY(TYPE_E_BADMODULEKIND),
		MAKE_SCODE_ENTRY(TYPE_E_BUFFERTOOSMALL),
		MAKE_SCODE_ENTRY(TYPE_E_CANTCREATETMPFILE),
		MAKE_SCODE_ENTRY(TYPE_E_CANTLOADLIBRARY),
		MAKE_SCODE_ENTRY(TYPE_E_CIRCULARTYPE),
		MAKE_SCODE_ENTRY(TYPE_E_DLLFUNCTIONNOTFOUND),
		MAKE_SCODE_ENTRY(TYPE_E_DUPLICATEID),
		MAKE_SCODE_ENTRY(TYPE_E_ELEMENTNOTFOUND),
		MAKE_SCODE_ENTRY(TYPE_E_INCONSISTENTPROPFUNCS),
		MAKE_SCODE_ENTRY(TYPE_E_INVALIDSTATE),
		MAKE_SCODE_ENTRY(TYPE_E_INVDATAREAD),
		MAKE_SCODE_ENTRY(TYPE_E_IOERROR),
		MAKE_SCODE_ENTRY(TYPE_E_LIBNOTREGISTERED),
		MAKE_SCODE_ENTRY(TYPE_E_NAMECONFLICT),
		MAKE_SCODE_ENTRY(TYPE_E_OUTOFBOUNDS),
		MAKE_SCODE_ENTRY(TYPE_E_QUALIFIEDNAMEDISALLOWED),
		MAKE_SCODE_ENTRY(TYPE_E_REGISTRYACCESS),
		MAKE_SCODE_ENTRY(TYPE_E_SIZETOOBIG),
		MAKE_SCODE_ENTRY(TYPE_E_TYPEMISMATCH),
		MAKE_SCODE_ENTRY(TYPE_E_UNDEFINEDTYPE),
		MAKE_SCODE_ENTRY(TYPE_E_UNKNOWNLCID),
		MAKE_SCODE_ENTRY(TYPE_E_UNSUPFORMAT),
		MAKE_SCODE_ENTRY(TYPE_E_WRONGTYPEKIND),
		MAKE_SCODE_ENTRY(VIEW_E_DRAW),
	};
	#undef MAKE_SCODE_ENTRY

	// look for it in the table
	for( int i = 0; i < (int) _countof(scNameTable); i++)
	{
		if (sc == scNameTable[i].sc)
			return scNameTable[i].lpszName;
	}
	return NULL;    // not found
}


//***********************************************************************
static LPCTSTR AfxGetFullScodeString(SCODE sc)
{
    static TCHAR szBuf[128] = _T("<scode string too long>");

    LPCTSTR lpsz;
    if ((lpsz = AfxGetScodeString(sc)) != NULL)
    {
        // found exact match
        static const TCHAR szFormat[] = _T("%s ($%08lX)");
        if( lstrlen(lpsz) + _countof(szFormat) - 2 /*%s*/ - 6 /*$%08lX*/ + 8 /*size of long value*/ < _countof(szBuf) )
        {
            wsprintf(szBuf, szFormat, lpsz, sc);
        }
    }
    else if ((lpsz = AfxGetScodeRangeString(sc)) != NULL)
    {
        // found suitable range
        static const TCHAR szFormat[] = _T("range: %s ($%08lX)");
        if( lstrlen(lpsz) + _countof(szFormat) - 2 /*%s*/ - 6 /*$%08lX*/ + 8 /*size of long value*/ < _countof(szBuf) )
        {
            wsprintf(szBuf, szFormat, lpsz, sc);
        }
    }
    else
    {
        // not found at all -- split it up into its parts
        LPCTSTR lpszSeverity = AfxGetSeverityString(sc);
        LPCTSTR lpszFacility = AfxGetFacilityString(sc);
        static const TCHAR szFormat[] = _T("severity: %s, facility: %s ($%08lX)");

        if( lstrlen(lpszSeverity) + lstrlen(lpszFacility) + _countof(szFormat) - 2 /*%s*/ - 6 /*$%08lX*/ + 8 /*size of long value*/ < _countof(szBuf) )
        {
            wsprintf(szBuf, szFormat, lpszSeverity, lpszFacility, sc);
        }
    }
    return szBuf;
}
#endif      //  #ifdef _DEBUG


//***********************************************************************
static void AfxThrowOleException(SCODE sc)
{
#ifdef _DEBUG
    wxLogDebug( /*traceOle, 0,*/ wxT("Warning: constructing COleException, scode = %s.\n"),
          AfxGetFullScodeString(sc));
#endif
//    COleException* pException = new COleException;
//    pException->m_sc = sc;
//    THROW(pException);
}


//***********************************************************************
void myDispatchDriver::InvokeHelperV( DISPID dwDispID, WORD wFlags,
                                      VARTYPE vtRet, void* pvRet, const BYTE* pbParamInfo, va_list argList)
{
//    USES_CONVERSION;

    if (m_lpDispatch == NULL)
    {
//        TRACE(traceOle, 0, "Warning: attempt to call Invoke with NULL m_lpDispatch!\n");
        return;
    }

    DISPPARAMS dispparams;
    memset(&dispparams, 0, sizeof dispparams);

    // determine number of arguments
    if (pbParamInfo != NULL)
        dispparams.cArgs = lstrlenA((LPCSTR)pbParamInfo);

    DISPID dispidNamed = DISPID_PROPERTYPUT;
    if (wFlags & (DISPATCH_PROPERTYPUT|DISPATCH_PROPERTYPUTREF))
    {
        wxASSERT(dispparams.cArgs > 0);
        dispparams.cNamedArgs = 1;
        dispparams.rgdispidNamedArgs = &dispidNamed;
    }

    if (dispparams.cArgs != 0)
    {
        // allocate memory for all VARIANT parameters
        VARIANT* pArg = new VARIANT[dispparams.cArgs];
        wxASSERT(pArg != NULL);   // should have thrown exception
        dispparams.rgvarg = pArg;
        memset(pArg, 0, sizeof(VARIANT) * dispparams.cArgs);

        // get ready to walk vararg list
        const BYTE* pb = pbParamInfo;
        pArg += dispparams.cArgs - 1;   // params go in opposite order

        while (*pb != 0)
        {
            wxASSERT(pArg >= dispparams.rgvarg);

            pArg->vt = *pb; // set the variant type
            if (pArg->vt & VT_MFCBYREF)
            {
                pArg->vt &= ~VT_MFCBYREF;
                pArg->vt |= VT_BYREF;
            }
            switch (pArg->vt)
            {
                case VT_UI1:
//                    pArg->bVal = va_arg(argList, BYTE);
                    wxASSERT( FALSE );
                    break;
                case VT_UI2:
//                    pArg->uiVal = va_arg(argList, USHORT);
                    wxASSERT( FALSE );
                    break;
                case VT_UI4:
                    pArg->ulVal = va_arg(argList, ULONG);
                    break;
                case VT_UI8:
                    pArg->ullVal = va_arg(argList, ULONGLONG);
                    break;
                case VT_I1:
//                    pArg->cVal = va_arg(argList, char);
                    wxASSERT( FALSE );
                    break;
                case VT_I2:
//                    pArg->iVal = va_arg(argList, short);
                    wxASSERT( FALSE );
                    break;
                case VT_I4:
                    pArg->lVal = va_arg(argList, long);
                    break;
                case VT_I8:
                    pArg->llVal = va_arg(argList, LONGLONG);
                    break;
                case VT_R4:
                    pArg->fltVal = (float)va_arg(argList, double);
                    break;
                case VT_R8:
                    pArg->dblVal = va_arg(argList, double);
                    break;
                case VT_DATE:
                    pArg->date = va_arg(argList, DATE);
                    break;
                case VT_CY:
                    pArg->cyVal = *va_arg(argList, CY*);
                    break;
                case VT_BSTR:
                {
                    LPCOLESTR lpsz = va_arg(argList, LPOLESTR);
                    pArg->bstrVal = ::SysAllocString(lpsz);
//                    if (lpsz != NULL && pArg->bstrVal == NULL)
//                        AfxThrowMemoryException();
                }
                break;
#if !defined(_UNICODE)
                case VT_BSTRA:
                {
                    LPCSTR lpsz = va_arg(argList, LPSTR);
                    pArg->bstrVal = ::SysAllocString(T2COLE(lpsz));
                    if (lpsz != NULL && pArg->bstrVal == NULL)
                        AfxThrowMemoryException();
                    pArg->vt = VT_BSTR;
                }
                break;
#endif
                case VT_DISPATCH:
                    pArg->pdispVal = va_arg(argList, LPDISPATCH);
                    break;
                case VT_ERROR:
                    pArg->scode = va_arg(argList, SCODE);
                    break;
                case VT_BOOL:
                    V_BOOL(pArg) = (VARIANT_BOOL)(va_arg(argList, BOOL) ? -1 : 0);
                    break;
                case VT_VARIANT:
                    *pArg = *va_arg(argList, VARIANT*);
                    break;
                case VT_UNKNOWN:
                    pArg->punkVal = va_arg(argList, LPUNKNOWN);
                    break;

                case VT_UI1|VT_BYREF:
                    pArg->pbVal = va_arg(argList, BYTE*);
                    break;
                case VT_UI2|VT_BYREF:
                    pArg->puiVal = va_arg(argList, USHORT*);
                    break;
                case VT_UI4|VT_BYREF:
                    pArg->pulVal = va_arg(argList, ULONG*);
                    break;
//                case VT_UI8|VT_BYREF:
//                    pArg->pullVal = va_arg(argList, ULONGLONG*);
//                    break;
                case VT_I1|VT_BYREF:
                    pArg->pcVal = va_arg(argList, char*);
                    break;
                case VT_I2|VT_BYREF:
                    pArg->piVal = va_arg(argList, short*);
                    break;
                case VT_I4|VT_BYREF:
                    pArg->plVal = va_arg(argList, long*);
                    break;
//                case VT_I8|VT_BYREF:
//                    pArg->pllVal = va_arg(argList, LONGLONG*);
//                    break;
                case VT_R4|VT_BYREF:
                    pArg->pfltVal = va_arg(argList, float*);
                    break;
                case VT_R8|VT_BYREF:
                    pArg->pdblVal = va_arg(argList, double*);
                    break;
                case VT_DATE|VT_BYREF:
                    pArg->pdate = va_arg(argList, DATE*);
                    break;
                case VT_CY|VT_BYREF:
                    pArg->pcyVal = va_arg(argList, CY*);
                    break;
                case VT_BSTR|VT_BYREF:
                    pArg->pbstrVal = va_arg(argList, BSTR*);
                    break;
                case VT_DISPATCH|VT_BYREF:
                    pArg->ppdispVal = va_arg(argList, LPDISPATCH*);
                    break;
                case VT_ERROR|VT_BYREF:
                    pArg->pscode = va_arg(argList, SCODE*);
                    break;
                case VT_BOOL|VT_BYREF:
                {
                    // coerce BOOL into VARIANT_BOOL
                    BOOL* pboolVal = va_arg(argList, BOOL*);
                    *pboolVal = *pboolVal ? MAKELONG(0xffff, 0) : 0;
                    pArg->pboolVal = (VARIANT_BOOL*)pboolVal;
                }
                break;
                case VT_VARIANT|VT_BYREF:
                    pArg->pvarVal = va_arg(argList, VARIANT*);
                    break;
                case VT_UNKNOWN|VT_BYREF:
                    pArg->ppunkVal = va_arg(argList, LPUNKNOWN*);
                    break;

                default:
                    wxASSERT(FALSE);  // unknown type!
                    break;
            }

            --pArg; // get ready to fill next argument
            ++pb;
        }
    }

    // initialize return value
    VARIANT* pvarResult = NULL;
    VARIANT vaResult;
    AfxVariantInit(&vaResult);
    if (vtRet != VT_EMPTY)
        pvarResult = &vaResult;

    // initialize EXCEPINFO struct
    EXCEPINFO excepInfo;
    memset(&excepInfo, 0, sizeof excepInfo);

    UINT nArgErr = (UINT)-1;  // initialize to invalid arg

    // make the call
    SCODE sc = m_lpDispatch->Invoke(dwDispID, IID_NULL, 0, wFlags,
                                    &dispparams, pvarResult, &excepInfo, &nArgErr);

    // cleanup any arguments that need cleanup
    if (dispparams.cArgs != 0)
    {
        VARIANT* pArg = dispparams.rgvarg + dispparams.cArgs - 1;
        const BYTE* pb = pbParamInfo;
        while (*pb != 0)
        {
            switch ((VARTYPE)*pb)
            {
#if !defined(_UNICODE)
                case VT_BSTRA:
#endif
                case VT_BSTR:
                    VariantClear(pArg);
                    break;
            }
            --pArg;
            ++pb;
        }
    }
    delete[] dispparams.rgvarg;

    // throw exception on failure
    if (FAILED(sc))
    {
        VariantClear(&vaResult);
        if (sc != DISP_E_EXCEPTION)
        {
            // non-exception error code
            AfxThrowOleException(sc);
        }

        // make sure excepInfo is filled in
        if (excepInfo.pfnDeferredFillIn != NULL)
            excepInfo.pfnDeferredFillIn(&excepInfo);

#if 0
        // allocate new exception, and fill it
        COleDispatchException* pException =
                                           new COleDispatchException(NULL, 0, excepInfo.wCode);
        wxASSERT(pException->m_wCode == excepInfo.wCode);
        if (excepInfo.bstrSource != NULL)
        {
            pException->m_strSource = excepInfo.bstrSource;
            SysFreeString(excepInfo.bstrSource);
        }
        if (excepInfo.bstrDescription != NULL)
        {
            pException->m_strDescription = excepInfo.bstrDescription;
            SysFreeString(excepInfo.bstrDescription);
        }
        if (excepInfo.bstrHelpFile != NULL)
        {
            pException->m_strHelpFile = excepInfo.bstrHelpFile;
            SysFreeString(excepInfo.bstrHelpFile);
        }
        pException->m_dwHelpContext = excepInfo.dwHelpContext;
        pException->m_scError = excepInfo.scode;

        // then throw the exception
        THROW(pException);
#endif
        wxASSERT(FALSE);  // not reached
    }

    if (vtRet != VT_EMPTY)
    {
        // convert return value
        if (vtRet != VT_VARIANT)
        {
            SCODE sc = VariantChangeType(&vaResult, &vaResult, 0, vtRet);
            if (FAILED(sc))
            {
//                TRACE(traceOle, 0, "Warning: automation return value coercion failed.\n");
                VariantClear(&vaResult);
//                AfxThrowOleException(sc);
            }
            wxASSERT(vtRet == vaResult.vt);
        }

        // copy return value into return spot!
        switch (vtRet)
        {
            case VT_UI1:
                *(BYTE*)pvRet = vaResult.bVal;
                break;
            case VT_UI2:
                *(USHORT*)pvRet = vaResult.uiVal;
                break;
            case VT_UI4:
                *(ULONG*)pvRet = vaResult.ulVal;
                break;
            case VT_UI8:
                *(ULONGLONG*)pvRet = vaResult.ullVal;
                break;
            case VT_I1:
                *(char*)pvRet = vaResult.cVal;
                break;
            case VT_I2:
                *(short*)pvRet = vaResult.iVal;
                break;
            case VT_I4:
                *(long*)pvRet = vaResult.lVal;
                break;
            case VT_I8:
                *(LONGLONG*)pvRet = vaResult.llVal;
                break;
            case VT_R4:
                *(float*)pvRet = vaResult.fltVal;
                break;
            case VT_R8:
                *(double*)pvRet = vaResult.dblVal;
                break;
            case VT_DATE:
                *(double*)pvRet = *(double*)&vaResult.date;
                break;
            case VT_CY:
                *(CY*)pvRet = vaResult.cyVal;
                break;
            case VT_BSTR:
//                AfxBSTR2CString((CString*)pvRet, vaResult.bstrVal);
                AfxBSTR2wxString( (wxString *) pvRet, vaResult.bstrVal);
                SysFreeString(vaResult.bstrVal);
                break;
            case VT_DISPATCH:
                *(LPDISPATCH*)pvRet = vaResult.pdispVal;
                break;
            case VT_ERROR:
                *(SCODE*)pvRet = vaResult.scode;
                break;
            case VT_BOOL:
                *(BOOL*)pvRet = (V_BOOL(&vaResult) != 0);
                break;
            case VT_VARIANT:
                *(VARIANT*)pvRet = vaResult;
                break;
            case VT_UNKNOWN:
                *(LPUNKNOWN*)pvRet = vaResult.punkVal;
                break;

            default:
                wxASSERT(FALSE);  // invalid return type specified
        }
    }
}


//***********************************************************************
void myDispatchDriver::InvokeHelper( DISPID dwDispID, WORD wFlags,
                                               VARTYPE vtRet, void* pvRet, const BYTE* pbParamInfo, ...)
{
    va_list argList;
    va_start(argList, pbParamInfo);

    InvokeHelperV( dwDispID, wFlags, vtRet, pvRet, pbParamInfo, argList );

    va_end(argList);
}


//***********************************************************************
static LPUNKNOWN _AfxQueryInterface(LPUNKNOWN lpUnknown, REFIID iid)
{
    wxASSERT(lpUnknown != NULL);

    LPUNKNOWN lpW = NULL;
    if (lpUnknown->QueryInterface(iid, (LPLP)&lpW) != S_OK)
        return NULL;

    return lpW;
}


//***********************************************************************
bool myDispatchDriver::CreateDispatch2( REFCLSID clsid )
{
    wxASSERT( m_lpDispatch == NULL );

    m_bAutoRelease = TRUE;  // good default is to auto-release

    // create an instance of the object
    LPUNKNOWN   lpUnknown   = NULL;
    SCODE       sc          = CoCreateInstance(clsid, NULL, CLSCTX_ALL | CLSCTX_REMOTE_SERVER,
                                IID_IUnknown, (LPLP)&lpUnknown);
    if( sc == E_INVALIDARG )
    {
        // may not support CLSCTX_REMOTE_SERVER, so try without
        sc = CoCreateInstance(clsid, NULL, CLSCTX_ALL & ~CLSCTX_REMOTE_SERVER,
                              IID_IUnknown, (LPLP)&lpUnknown);
    }
    if (FAILED(sc))
        goto Failed;

    // make sure it is running
    sc = OleRun( lpUnknown );
    if( FAILED(sc) )
        goto Failed;

    // query for IDispatch interface
    m_lpDispatch = QUERYINTERFACE(lpUnknown, IDispatch);
    if (m_lpDispatch == NULL)
        goto Failed;

    lpUnknown->Release();
    wxASSERT(m_lpDispatch != NULL);
    return TRUE;

Failed:
    RELEASE(lpUnknown);
//    if (pError != NULL)
//        pError->m_sc = sc;

#ifdef _DEBUG
    TRACE(traceOle, 0, _T("Warning: CreateDispatch returning scode = %s.\n"),
          AfxGetFullScodeString(sc));
#endif

    return FALSE;
}


//***********************************************************************
bool myDispatchDriver::CreateDispatch( LPCTSTR lpszProgID )
{
    wxASSERT(m_lpDispatch == NULL);

    // map prog id to CLSID
    CLSID   clsid;
    SCODE   sc      = AfxGetClassIDFromString( lpszProgID, &clsid );
    if( FAILED( sc ) )
    {
//        if( pError  !=  NULL )
//            pError->m_sc = sc;
        return false;
    }

    // create with CLSID
//    return CreateDispatch( clsid, pError );
    return CreateDispatch2( clsid );
}


//***********************************************************************
void myDispatchDriver::ReleaseDispatch( )
{
    if( m_lpDispatch != NULL )
    {
        if( m_bAutoRelease )
        {
            try
            {
                m_lpDispatch->Release( );
            }
            catch( int )
            {
            }
        }
        m_lpDispatch = NULL;
    }
}


//***********************************************************************
myDispatchDriver::myDispatchDriver( LPDISPATCH lpDispatch, bool bAutoRelease )
{
    m_lpDispatch = lpDispatch;
    m_bAutoRelease = bAutoRelease;
}


//***********************************************************************
//myDispatchDriver::myDispatchDriver( const myDispatchDriver &dispatchSrc )
//{
//    wxASSERT(this != &dispatchSrc);   // constructing from self?
//
//    m_lpDispatch = dispatchSrc.m_lpDispatch;
//    if( m_lpDispatch != NULL )
//        m_lpDispatch->AddRef( );
//    m_bAutoRelease = true;
//}
