//
// main.cpp : Defines the entry point for the Mac PDF importer
//

#include <wx/wx.h>
#include <wx/file.h>
#include <wx/filename.h>
#include <wx/dir.h>

#include "mac_pdf.h"

#define DOCSHARE_API

extern "C" DOCSHARE_API int  dsGetRelPri( void );
extern "C" DOCSHARE_API int  dsGetPriLen( void );
extern "C" DOCSHARE_API int  dsGetPriTypes( char *pC, int iMaxLen );
extern "C" DOCSHARE_API int  dsGetSecLen( void );
extern "C" DOCSHARE_API int  dsGetSecTypes( char *pC, int iMaxLen );

extern "C" DOCSHARE_API int  dsGetStatus( void );
extern "C" DOCSHARE_API int  dsGetPreLoadExeCmdLen( void );
extern "C" DOCSHARE_API int  dsGetPreLoadExeCmd( char *pC, int iMaxLen );
extern "C" DOCSHARE_API int  dsGetPostLoadKillCmdLen( void );
extern "C" DOCSHARE_API int  dsGetPostLoadKillCmd( char *pC, int iMaxLen );
extern "C" DOCSHARE_API int  dsPreLoad( );
extern "C" DOCSHARE_API int  dsLoad( char *pFN, unsigned long ulHWnd );
extern "C" DOCSHARE_API int  dsPostLoad( );
extern "C" DOCSHARE_API int  dsGetNumPages( void );

extern "C" DOCSHARE_API int  dsSetTempPath( const char *pC );
extern "C" DOCSHARE_API int  dsGetTempPath( char *pC, int iMaxLen );

extern "C" DOCSHARE_API void  dsAbort( );


int             dsRelPri    = 3000;
int             dsStatus    = 0;
int             gNumPages   = 0;
bool            gfAbort     = false;
wxString		goBuf;
wxString		gFN;
pthread_t 		gThread		= NULL;


char    strPriTypes[] = {"PDF Documents(*.pdf)|*.pdf"};
char    strSecTypes[] = {""};
char    strPreLoadExeCmd[]  = {""};
char    strPostLoadKillCmd[]= {""};


//*****************************************************************************
//  Sets the internal abort flag which we check as frequently as possible
DOCSHARE_API void  dsAbort( )
{
    gfAbort = true;
}


//*****************************************************************************
static void BuildIndex( wxString &aDirectory )
{
    wxString    strDirectory = aDirectory;
    if( !strDirectory.EndsWith( wxT("/") ) )
        strDirectory += wxT("/");

    wxString    fileName;
    wxString    fileSpec( wxT("*.PNG") );
    wxString    baseName;
    wxDir       dir( strDirectory );
    if( dir.IsOpened( ) )
    {
        //  Create the index file
        bool        fRet;
        wxString    indexText;
        indexText += _T("<index>\n");

        //  Find each image file in directory
        //    They were created as: xxxx1.PNG, xxx2.PNG, ...
        int     iIndex  = 1;
        bool    fMore = dir.GetFirst( &fileName, fileSpec, wxDIR_FILES );
        if( fMore )
        {
            int i = fileName.Find(wxT('.'), true);
            while (i>0 && isdigit(fileName[i-1]))
                --i;
            baseName = fileName.Left(i);
        }
        
        if (baseName.Len() > 0)
        {
            while (true)
            {
                wxString iterName = wxString::Format(wxT("%s%d"), baseName.c_str(), iIndex);
                wxString imageName = iterName + wxT(".PNG");

                if (wxFile::Exists(strDirectory + imageName))
                {
                    wxString altName = iterName + wxT(".PNG");
                    wxString strT = wxString::Format( wxT("<page title=\"Page %d\" file=\"%s\" altfile=\"%s\" scale=\"1.0\" altscale=\"1.0\" fittowidth=\"F\"/>\n"),
                                            iIndex++, imageName.c_str( ), altName.c_str( ) );
                    indexText += strT;
                }
                else
                    break;
            }
        }

        indexText += wxT("</index>\n");
        wxString    strOut( strDirectory );
        strOut.Append( wxT("/index.xml") );

        wxFile  indexFile( strOut, wxFile::write );
        fRet = indexFile.Write( indexText );
        indexFile.Close( );
    }
    return;
}


//*****************************************************************************
//  This contains the current status/progress of the DS.
DOCSHARE_API int  dsGetStatus( void )
{
    return dsStatus;
}


//*****************************************************************************
//  This contains the current status/progress of the DS.
DOCSHARE_API int  dsGetRelPri( void )
{
    return dsRelPri;
}


//*****************************************************************************
DOCSHARE_API int  dsGetPriLen( void )
{
    return strlen( strPriTypes );
}


//*****************************************************************************
DOCSHARE_API int  dsGetSecLen( void )
{
    return strlen( strSecTypes );
}


//*****************************************************************************
DOCSHARE_API int  dsGetPreLoadExeCmdLen( void )
{
    return strlen( strPreLoadExeCmd );
}


//*****************************************************************************
DOCSHARE_API int  dsGetPostLoadKillCmdLen( void )
{
    return strlen( strPostLoadKillCmd );
}


//*****************************************************************************
DOCSHARE_API int  dsGetNumPages( void )
{
    return gNumPages;
}


//*****************************************************************************
DOCSHARE_API int  dsGetPriTypes( char *pC, int iMaxLen )
{
    int iLen = strlen( strPriTypes );
    if( iLen > iMaxLen )
    {
        strncpy( pC, strPriTypes, iMaxLen - 1 );
        pC[ iMaxLen - 1 ] = 0;
        return -iLen;
    }

    strcpy( pC, strPriTypes );
    pC[ iLen ] = 0;
    return iLen;
}


//*****************************************************************************
DOCSHARE_API int  dsGetSecTypes( char *pC, int iMaxLen )
{
    int iLen = strlen( strSecTypes );
    if( iLen > iMaxLen )
    {
        strncpy( pC, strSecTypes, iMaxLen - 1 );
        pC[ iMaxLen - 1 ] = 0;
        return -iLen;
    }

    strcpy( pC, strSecTypes );
    pC[ iLen ] = 0;
    return iLen;
}


//*****************************************************************************
DOCSHARE_API int  dsGetPreLoadExeCmd( char *pC, int iMaxLen )
{
    int iLen = strlen( strPreLoadExeCmd );
    if( iLen > iMaxLen )
    {
        strncpy( pC, strPreLoadExeCmd, iMaxLen - 1 );
        pC[ iMaxLen - 1 ] = 0;
        return -iLen;
    }

    strcpy( pC, strPreLoadExeCmd );
    pC[ iLen ] = 0;
    return iLen;
}


//*****************************************************************************
DOCSHARE_API int  dsGetPostLoadKillCmd( char *pC, int iMaxLen )
{
    int iLen = strlen( strPostLoadKillCmd );
    if( iLen > iMaxLen )
    {
        strncpy( pC, strPostLoadKillCmd, iMaxLen - 1 );
        pC[ iMaxLen - 1 ] = 0;
        return -iLen;
    }

    strcpy( pC, strPostLoadKillCmd );
    pC[ iLen ] = 0;
    return iLen;
}


//*****************************************************************************
DOCSHARE_API int  dsSetTempPath( const char *pC )
{
    goBuf = wxString(pC, wxConvUTF8);
    return 0;
}


//*****************************************************************************
DOCSHARE_API int  dsGetTempPath( char *pC, int iMaxLen )
{
    int     iLen = goBuf.Len( );
    if( iLen > iMaxLen )
    {
        strncpy( pC, goBuf.mb_str( wxConvUTF8 ), iMaxLen - 1 );
        pC[ iMaxLen - 1 ] = 0;
        return -iLen;
    }

    strcpy( pC, goBuf.mb_str( wxConvUTF8 ) );
    pC[ iLen ] = 0;
    return iLen;
}


//*****************************************************************************
DOCSHARE_API int  dsPreLoad( )
{
    gfAbort = false;
    dsStatus = 1;     //  Indicate we have completed our work
    return 0;
}


//*****************************************************************************
// Convert each page of PDF to a PNG.  (The same mechanism could also
// be used to convert to an SVG.)
void * ThreadFunc( void * lpParam )
{
    bool        fRet;

    wxString    strTarget;
    if (!goBuf.IsEmpty())
        strTarget = goBuf;
    else
        strTarget = wxGetCwd();
        
    strTarget.Append( wxT("/dspages") );

    wxFileName::Mkdir( strTarget, 0777,  wxPATH_MKDIR_FULL);

    if( !gfAbort )
    {
        //  Delete all files from our temp output folder
        wxString    fileSpec( wxT("*") );
        wxString    fileName;

        wxDir       dir( strTarget );
        if( dir.IsOpened( ) )
        {
            bool fMore;
            while( (fMore = fMore = dir.GetFirst( &fileName, fileSpec, wxDIR_FILES )) == true )
            {
                wxString    strFull;
                strFull = strTarget + wxT("/") + fileName;

                ::wxRemoveFile( strFull );

                fRet = fMore;
            }
        }
    }

    if( !gfAbort )
    {
        if (OpenPDF(gFN.mb_str( wxConvUTF8 )))
        {
            dsStatus = -1;
            return NULL;
        }
        gNumPages = GetPageCount();
    }
    
    for (int i = 1; i <= gNumPages && !gfAbort; i++)
    {
     	if (ExportPage(i, strTarget.mb_str( wxConvUTF8 ), 1000, 0))
        {
            dsStatus = -1;
            ClosePDF();
            return NULL;
        }
    }

    goBuf = strTarget;

    ClosePDF();
    
    // Build index file; also compress EMF files
    BuildIndex( strTarget );
    
    dsStatus = 1;
    return NULL;
}


//*****************************************************************************
DOCSHARE_API int  dsLoad( char *pFN, unsigned long ulWnd )
{
    gFN = wxString( pFN, wxConvUTF8 );

    dsStatus = 0;
    gfAbort = false;

    pthread_create( &gThread, NULL, ThreadFunc, NULL );

    return 1;
}


//*****************************************************************************
DOCSHARE_API int  dsPostLoad( )
{
    gfAbort = false;
    dsStatus = 1;
    return 0;
}
