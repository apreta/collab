/*
 *  mac_pdf.cpp
 *  mac_pdf
 *
 *  Created by Mike on 11/19/10.
 *  Copyright 2010 Michael Tinglof. All rights reserved.
 *
 */

#include <string>
#include <sstream>

using std::string;
using std::stringstream;

#include "mac_pdf.h"

static CGPDFDocumentRef document = NULL;
static int pageCount = 0;


CGPDFDocumentRef GetDocumentRef (const char *filename)
{
    CFStringRef path;
    CFURLRef url;
    CGPDFDocumentRef document;
    
    path = CFStringCreateWithCString (NULL, filename, kCFStringEncodingUTF8);
    url = CFURLCreateWithFileSystemPath (NULL, path, kCFURLPOSIXPathStyle, 0);
    CFRelease (path);

    document = CGPDFDocumentCreateWithURL (url);
    CFRelease(url);
    
    return document;
}

CGContextRef CreateBitmapContext (int pixelsWide, int pixelsHigh)
{
    CGContextRef    context = NULL;
    CGColorSpaceRef colorSpace;
    void *          bitmapData;
    int             bitmapByteCount;
    int             bitmapBytesPerRow;
    
    bitmapBytesPerRow   = (pixelsWide * 4);
    bitmapByteCount     = (bitmapBytesPerRow * pixelsHigh);
    
    colorSpace = CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB);
    bitmapData = malloc( bitmapByteCount );
    if (bitmapData == NULL)
    {
        fprintf (stderr, "Memory not allocated\n");
        return NULL;
    }
    context = CGBitmapContextCreate (bitmapData,
                                     pixelsWide,
                                     pixelsHigh,
                                     8,      // bits per component
                                     bitmapBytesPerRow,
                                     colorSpace,
                                     kCGImageAlphaNoneSkipFirst);
    if (context == NULL)
    {
        free (bitmapData);
        fprintf (stderr, "Context not created\n");
        return NULL;
    }
    CGColorSpaceRelease( colorSpace );
    
    CGContextSetRGBFillColor(context, 1.0, 1.0, 1.0, 1.0);
    CGContextFillRect(context, CGContextGetClipBoundingBox(context));
    
    return context;
}

CGContextRef DisplayPDFPage (CGPDFDocumentRef document, size_t pageNumber, int width, int height)
{
    CGContextRef context;
    CGPDFPageRef page;
    CGRect rect = CGRectMake(0, 0, width, height);
    
    page = CGPDFDocumentGetPage (document, pageNumber);
    if (page == NULL)
        return NULL;

    CGRect pageRect = CGPDFPageGetBoxRect (page, kCGPDFMediaBox);
    // printf("%f %f %f %f\n", pageRect.origin.x, pageRect.origin.y, pageRect.size.width, pageRect.size.height);

    float scale;

    if (height == 0)
    {
        float ratio = pageRect.size.height / pageRect.size.width;
        scale = width / pageRect.size.width;
        height = width * ratio;
    }
    else
    {
        float xscale = width / pageRect.size.width;
    	float yscale = height / pageRect.size.height;
    	scale = xscale < yscale ? xscale : yscale;
    }
    
    context = CreateBitmapContext (width, height);
    if (context != NULL)
    {
        CGContextSaveGState (context);
        CGContextScaleCTM (context, scale, scale);

        // This is supposed to work, but wouldn't scale image larger then original
        // CGAffineTransform m;
        // m = CGPDFPageGetDrawingTransform (page, kCGPDFMediaBox, rect, 0, true);
        // CGContextConcatCTM (context, m);
        // CGContextClipToRect (context, CGPDFPageGetBoxRect (page, kCGPDFMediaBox));

        CGContextDrawPDFPage (context, page);
        CGContextRestoreGState (context);
    }
    
    return context;
}

void SaveJPEGImage (CGImageRef imageRef, const char *filename)
{
    CFStringRef path;
    CFURLRef url;
    float quality = 0.5;
    
    CFMutableDictionaryRef mSaveMetaAndOpts = CFDictionaryCreateMutable (NULL, 0,
                                                  &kCFTypeDictionaryKeyCallBacks,  &kCFTypeDictionaryValueCallBacks);
    CFDictionarySetValue (mSaveMetaAndOpts, kCGImageDestinationLossyCompressionQuality, 
                         CFNumberCreate(NULL, kCFNumberFloatType, &quality));

    path = CFStringCreateWithCString (NULL, filename, kCFStringEncodingUTF8);
    url = CFURLCreateWithFileSystemPath (NULL, path, kCFURLPOSIXPathStyle, 0);
    CFRelease (path);
    
    CGImageDestinationRef dr = CGImageDestinationCreateWithURL ( url, CFSTR("public.jpeg") , 1, NULL);
    CGImageDestinationAddImage(dr, imageRef, mSaveMetaAndOpts);
    CGImageDestinationFinalize(dr);
}

void SavePNGImage (CGImageRef imageRef, const char *filename)
{
    CFStringRef path;
    CFURLRef url;
    
    path = CFStringCreateWithCString (NULL, filename, kCFStringEncodingUTF8);
    url = CFURLCreateWithFileSystemPath (NULL, path, kCFURLPOSIXPathStyle, 0);
    CFRelease (path);

	CGImageDestinationRef dr = CGImageDestinationCreateWithURL ( url, CFSTR("public.png") , 1, NULL);
	CGImageDestinationAddImage (dr, imageRef, NULL);
	CGImageDestinationFinalize (dr);
    
    CFRelease (url);
    CFRelease (dr);
}

int OpenPDF (const char *filename)
{
	document = GetDocumentRef(filename);
    if (document == NULL)
        return -1;
    
    pageCount = CGPDFDocumentGetNumberOfPages (document);
    if (pageCount == 0) {
        fprintf(stderr, "`%s' has no content\n", filename);
        return -1;
    }
    
    return 0;
}

int GetPageCount ()
{
    return pageCount;
}

int ExportPage (int page, const char *outputdir, int width, int height)
{
    string dir = outputdir;
    
    if (document == NULL)
        return -1;
    
    CGContextRef context;
    
    context = DisplayPDFPage (document, page, width, height);
    if (context == NULL)
        return -1;

    CGImageRef image;
    image = CGBitmapContextCreateImage (context);
    
    stringstream ss;
    ss << dir << "/Page" << page << ".PNG";
    
    SavePNGImage (image, ss.str().c_str());
    // SaveJPEGImage (image, name);
    
    CGImageRelease (image);
    CGContextRelease (context);
    
    return 0;
}

void ClosePDF ()
{
    CGPDFDocumentRelease (document);
}
