/*
 *  mac_pdf.h
 *  mac_pdf
 *
 *  Created by Mike on 11/19/10.
 *  Copyright 2010 Michael Tinglof. All rights reserved.
 *
 */

#include <Carbon/Carbon.h>

int OpenPDF(const char *filename);
int GetPageCount();
int ExportPage(int page, const char *outputdir, int width, int height);
void ClosePDF();
