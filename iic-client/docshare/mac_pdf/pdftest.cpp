/*
 *  pdftest.c
 *  mac_pdf
 *
 *  Created by Mike on 11/19/10.
 *  Copyright 2010 Apreta. All rights reserved.
 *
 */

#include <wx/init.h>

extern "C" 
{
int  dsLoad( char *pFN, unsigned long ulHWnd );
int  dsSetTempPath( const char *pC );
int  dsGetStatus( void );
int  dsGetNumPages( );
}


int main(int argc, char *argv[])
{
	printf("Exporting...\n");

    wxEntryStart(argc, argv);
    
	dsSetTempPath(".");    
    dsLoad((char *)argv[1], 0);
    
    int stat;
    while ((stat = dsGetStatus()) == 0)
        sleep(1);
    
    printf("Export %s\n", stat == 1 ? "succeeded" : "failed");
    printf("%d pages\n", dsGetNumPages());
    
    return 0;
}