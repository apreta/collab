//#include <wx/wx.h>
//
//
//#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
//#include <windows.h>
//
//#ifdef BUILD_DLL
//#define DLL_EXPORT __declspec(dllexport)
//#else
//#define DLL_EXPORT
//#endif


// _Application wrapper class
#define ppSaveAsPresentation		1
#define ppSaveAsPowerPoint7			2
#define ppSaveAsPowerPoint4			3
#define	ppSaveAsPowerPoint3			4
#define ppSaveAsTemplate			5
#define ppSaveAsRTF					6
#define ppSaveAsShow				7
#define ppSaveAsAddIn				8
#define ppSaveAsPowerPoint4FarEast	10
#define ppSaveAsDefault				11
#define ppSaveAsHTML				12
#define ppSaveAsHTMLv3				13
#define ppSaveAsHTMLDual			14
#define ppSaveAsMetaFile			15
#define ppSaveAsGIF					16
#define ppSaveAsJPG					17
#define ppSaveAsPNG					18
#define ppSaveAsBMP					19
#define ppSaveAsWebArchive			20
#define ppSaveAsTIF					21
#define ppSaveAsPresForReview		22
#define ppSaveAsEMF					23



// parameter types: by value VTs
#define VTS_I2              "\x02"      // a 'short'
#define VTS_I4              "\x03"      // a 'long'
#define VTS_R4              "\x04"      // a 'float'
#define VTS_R8              "\x05"      // a 'double'
#define VTS_CY              "\x06"      // a 'CY' or 'CY*'
#define VTS_DATE            "\x07"      // a 'DATE'
#define VTS_WBSTR           "\x08"      // an 'LPCOLESTR'
#define VTS_DISPATCH        "\x09"      // an 'IDispatch*'
#define VTS_SCODE           "\x0A"      // an 'SCODE'
#define VTS_BOOL            "\x0B"      // a 'VARIANT_BOOL'
#define VTS_VARIANT         "\x0C"      // a 'const VARIANT&' or 'VARIANT*'
#define VTS_UNKNOWN         "\x0D"      // an 'IUnknown*'
#if defined(_UNICODE)
#define VTS_BSTR            VTS_WBSTR// an 'LPCOLESTR'
#define VT_BSTRT            VT_BSTR
#else
#define VTS_BSTR            "\x0E"  // an 'LPCSTR'
#define VT_BSTRA            14
#define VT_BSTRT            VT_BSTRA
#endif
#define VTS_I1              "\x10"      // a 'signed char'
#define VTS_UI1             "\x11"      // a 'BYTE'
#define VTS_UI2             "\x12"      // a 'WORD'
#define VTS_UI4             "\x13"      // a 'DWORD'
#define VTS_I8              "\x14"      // a 'LONGLONG'
#define VTS_UI8             "\x15"      // a 'ULONGLONG'


// parameter types: by reference VTs
#define VTS_PI2             "\x42"      // a 'short*'
#define VTS_PI4             "\x43"      // a 'long*'
#define VTS_PR4             "\x44"      // a 'float*'
#define VTS_PR8             "\x45"      // a 'double*'
#define VTS_PCY             "\x46"      // a 'CY*'
#define VTS_PDATE           "\x47"      // a 'DATE*'
#define VTS_PBSTR           "\x48"      // a 'BSTR*'
#define VTS_PDISPATCH       "\x49"      // an 'IDispatch**'
#define VTS_PSCODE          "\x4A"      // an 'SCODE*'
#define VTS_PBOOL           "\x4B"      // a 'VARIANT_BOOL*'
#define VTS_PVARIANT        "\x4C"      // a 'VARIANT*'
#define VTS_PUNKNOWN        "\x4D"      // an 'IUnknown**'
#define VTS_PI1             "\x50"      // a 'signed char*'
#define VTS_PUI1            "\x51"      // a 'BYTE*'
#define VTS_PUI2            "\x52"      // a 'WORD*'
#define VTS_PUI4            "\x53"      // a 'DWORD*'
#define VTS_PI8             "\x54"      // a 'LONGLONG*'
#define VTS_PUI8            "\x55"      // a 'ULONGLONG*'

// special VT_ and VTS_ values
#define VTS_NONE            NULL        // used for members with 0 params
#define VT_MFCVALUE         0xFFF       // special value for DISPID_VALUE
#define VT_MFCBYREF         0x40        // indicates VT_BYREF type
#define VT_MFCMARKER        0xFF        // delimits named parameters (INTERNAL USE)




//***********************************************************************
// helper(s) for reliable and small QueryInterface calls
//  from oleimpl2.h
//LPUNKNOWN AFXAPI _AfxQueryInterface(LPUNKNOWN lpUnknown, REFIID riid);
#define QUERYINTERFACE(lpUnknown, iface) \
    (iface*)_AfxQueryInterface(lpUnknown, IID_##iface)


//***********************************************************************
// helper for reliable and small Release calls
//  from oleimpl2.h
//DWORD AFXAPI _AfxRelease(LPUNKNOWN* plpUnknown);
DWORD _AfxRelease(LPUNKNOWN* plpUnknown);
#ifndef _DEBUG
// generate smaller code in release build
#define RELEASE(lpUnk) _AfxRelease((LPUNKNOWN*)&lpUnk)
#else
// generate larger but typesafe code in debug build
#define RELEASE(lpUnk) do \
                           { if ((lpUnk) != NULL) { (lpUnk)->Release(); (lpUnk) = NULL; } } while (0)
#endif


//***********************************************************************
typedef LPVOID* LPLP;           //  from oleimpl2.h




//***********************************************************************
class myDispatchDriver
{
public:
    myDispatchDriver( )     { m_lpDispatch = NULL; m_bAutoRelease = true; };
    myDispatchDriver( LPDISPATCH lpDispatch, bool bAutoRelease = true);
//    myDispatchDriver( const myDispatchDriver &dispatchSrc );


public:
//    bool    CreateDispatch( LPCTSTR lpszProgID, COleException* pError);

    void    InvokeHelperV( DISPID dwDispID, WORD wFlags,
                           VARTYPE vtRet, void* pvRet, const BYTE* pbParamInfo, va_list argList);
    void    InvokeHelper( DISPID dwDispID, WORD wFlags,
                          VARTYPE vtRet, void* pvRet, const BYTE* pbParamInfo, ...);
//    bool    CreateDispatch( REFCLSID clsid, COleException* pError );
//    bool    CreateDispatch( LPCTSTR lpszProgID, COleException* pError = NULL );


// Attributes
    LPDISPATCH  m_lpDispatch;
    BOOL        m_bAutoRelease;

// Operations
    bool    CreateDispatch2( REFCLSID clsid );
    bool    CreateDispatch( LPCTSTR lpszProgID );

    void    ReleaseDispatch( );
    
    bool    IsOk()  { return m_lpDispatch != NULL; }

};


//class _Application : public COleDispatchDriver
class _Application : public myDispatchDriver
{
public:
	_Application() {}		// Calls COleDispatchDriver default constructor
//	_Application(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
//	_Application(const _Application& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetPresentations();
/*	LPDISPATCH GetWindows();
	LPDISPATCH GetActiveWindow();
	LPDISPATCH GetActivePresentation();
	LPDISPATCH GetSlideShowWindows();
	LPDISPATCH GetCommandBars();
	CString GetPath();
	CString GetName();
	CString GetCaption();
	void SetCaption(LPCTSTR lpszNewValue);
	LPDISPATCH GetAssistant();
	LPDISPATCH GetFileSearch();
	LPDISPATCH GetFileFind();
	CString GetBuild();
*/
//    CString GetVersion();
/*	CString GetOperatingSystem();
	CString GetActivePrinter();
	long GetCreator();
	LPDISPATCH GetAddIns();
	LPDISPATCH GetVbe();
	void Help(LPCTSTR HelpFile, long ContextID);
*/	void Quit();
	// method 'Run' not emitted because of invalid return type or parameter type
/*	float GetLeft();
	void SetLeft(float newValue);
	float GetTop();
	void SetTop(float newValue);
	float GetWidth();
	void SetWidth(float newValue);
	float GetHeight();
	void SetHeight(float newValue);
	long GetWindowState();
	void SetWindowState(long nNewValue);
	long GetVisible();
*/	void SetVisible(long nNewValue);
/*	long GetActive();
	void Activate();
	LPDISPATCH GetAnswerWizard();
	LPDISPATCH GetCOMAddIns();
	CString GetProductCode();
	LPDISPATCH GetDefaultWebOptions();
	LPDISPATCH GetLanguageSettings();
	long GetShowWindowsInTaskbar();
	void SetShowWindowsInTaskbar(long nNewValue);
	long GetFeatureInstall();
	void SetFeatureInstall(long nNewValue);
*/
};

/////////////////////////////////////////////////////////////////////////////
// SlideShowWindow wrapper class

class SlideShowWindow : public myDispatchDriver
{
public:
	SlideShowWindow() {}		// Calls COleDispatchDriver default constructor
//	SlideShowWindow(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
//	SlideShowWindow(const SlideShowWindow& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
/*	LPDISPATCH GetApplication();
	LPDISPATCH GetParent();
*/	LPDISPATCH GetView();
/*	LPDISPATCH GetPresentation();
	long GetIsFullScreen();
	float GetLeft();
	void SetLeft(float newValue);
	float GetTop();
	void SetTop(float newValue);
	float GetWidth();
	void SetWidth(float newValue);
	float GetHeight();
	void SetHeight(float newValue);
	long GetActive();
	void Activate();
*/
};

/////////////////////////////////////////////////////////////////////////////
// SlideShowView wrapper class

class SlideShowView : public myDispatchDriver
{
public:
	SlideShowView() {}		// Calls COleDispatchDriver default constructor
//	SlideShowView(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
//	SlideShowView(const SlideShowView& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
/*	LPDISPATCH GetApplication();
	LPDISPATCH GetParent();
	long GetZoom();
*/	LPDISPATCH GetSlide();
	long GetPointerType();
	void SetPointerType(long nNewValue);
/*	long GetState();
	void SetState(long nNewValue);
	long GetAcceleratorsEnabled();
	void SetAcceleratorsEnabled(long nNewValue);
	float GetPresentationElapsedTime();
	float GetSlideElapsedTime();
	void SetSlideElapsedTime(float newValue);
	LPDISPATCH GetLastSlideViewed();
*/	long GetAdvanceMode();
/*	LPDISPATCH GetPointerColor();
	long GetIsNamedShow();
	CString GetSlideShowName();
	void DrawLine(float BeginX, float BeginY, float EndX, float EndY);
*/	void EraseDrawing();
/*	void First();
	void Last();
	void Next();
	void Previous();
*/	void GotoSlide(long index, long ResetSlide);
/*	void GotoNamedShow(LPCTSTR SlideShowName);
	void EndNamedShow();
	void ResetSlideTime();
*/	void Exit();
	long GetCurrentShowPosition();
};

/////////////////////////////////////////////////////////////////////////////
// SlideShowSettings wrapper class

class SlideShowSettings : public myDispatchDriver
{
public:
	SlideShowSettings() {}		// Calls COleDispatchDriver default constructor
//	SlideShowSettings(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
//	SlideShowSettings(const SlideShowSettings& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
/*
	LPDISPATCH GetApplication();
	LPDISPATCH GetParent();
	LPDISPATCH GetPointerColor();
	LPDISPATCH GetNamedSlideShows();
	long GetStartingSlide();
	void SetStartingSlide(long nNewValue);
	long GetEndingSlide();
	void SetEndingSlide(long nNewValue);
	long GetAdvanceMode();
	void SetAdvanceMode(long nNewValue);
*/	LPDISPATCH Run();
/*	long GetLoopUntilStopped();
	void SetLoopUntilStopped(long nNewValue);
	long GetShowType();
*/	void SetShowType(long nNewValue);
/*	long GetShowWithNarration();
	void SetShowWithNarration(long nNewValue);
	long GetShowWithAnimation();
	void SetShowWithAnimation(long nNewValue);
	CString GetSlideShowName();
	void SetSlideShowName(LPCTSTR lpszNewValue);
	long GetRangeType();
	void SetRangeType(long nNewValue);
*/
};
/////////////////////////////////////////////////////////////////////////////
// Presentations wrapper class

class Presentations : public myDispatchDriver
{
public:
	Presentations() {}		// Calls COleDispatchDriver default constructor
	Presentations( LPDISPATCH pDispatch ) : myDispatchDriver( pDispatch ) {}
//	Presentations(const Presentations& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long GetCount();
//	LPDISPATCH GetApplication();
//	LPDISPATCH GetParent();
//	LPDISPATCH Item(const VARIANT& index);
//	LPDISPATCH Add(long WithWindow);
	LPDISPATCH Open(LPCTSTR FileName, long ReadOnly, long Untitled, long WithWindow);
};
/////////////////////////////////////////////////////////////////////////////
// Slides wrapper class

class Slides : public myDispatchDriver
{
public:
	Slides() {}		// Calls COleDispatchDriver default constructor
	Slides(LPDISPATCH pDispatch) : myDispatchDriver(pDispatch) {}
//	Slides(const Slides& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	long GetCount();
/*	LPDISPATCH GetApplication();
	LPDISPATCH GetParent();
*/	LPDISPATCH Item(const VARIANT& index);
/*	LPDISPATCH FindBySlideID(long SlideID);
	LPDISPATCH Add(long index, long Layout);
	long InsertFromFile(LPCTSTR FileName, long index, long SlideStart, long SlideEnd);
	LPDISPATCH Range(const VARIANT& index);
	LPDISPATCH Paste(long index);
*/
};
/////////////////////////////////////////////////////////////////////////////
// _Slide wrapper class

class _Slide : public myDispatchDriver
{
public:
	_Slide() {}		// Calls COleDispatchDriver default constructor
	_Slide(LPDISPATCH pDispatch) : myDispatchDriver(pDispatch) {}
//	_Slide(const _Slide& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:
/*
// Operations
public:
	LPDISPATCH GetApplication();
	LPDISPATCH GetParent();
*/	LPDISPATCH GetShapes();
/*	LPDISPATCH GetHeadersFooters();
	LPDISPATCH GetSlideShowTransition();
	LPDISPATCH GetColorScheme();
	void SetColorScheme(LPDISPATCH newValue);
	LPDISPATCH GetBackground();
*/
//    CString GetName();
/*	void SetName(LPCTSTR lpszNewValue);
	long GetSlideID();
	long GetPrintSteps();
*/	void Select();
/*	void Cut();
	void Copy();
	long GetLayout();
	void SetLayout(long nNewValue);
	LPDISPATCH Duplicate();
*/	void Delete();
/*	LPDISPATCH GetTags();
	long GetSlideIndex();
	long GetSlideNumber();
	long GetDisplayMasterShapes();
	void SetDisplayMasterShapes(long nNewValue);
	long GetFollowMasterBackground();
	void SetFollowMasterBackground(long nNewValue);
	LPDISPATCH GetNotesPage();
	LPDISPATCH GetMaster();
	LPDISPATCH GetHyperlinks();
	void Export(LPCTSTR FileName, LPCTSTR FilterName, long ScaleWidth, long ScaleHeight);
	LPDISPATCH GetScripts();
*/
};
/////////////////////////////////////////////////////////////////////////////
// _Presentation wrapper class

class _Presentation : public myDispatchDriver
{
public:
	_Presentation() {}		// Calls COleDispatchDriver default constructor
	_Presentation(LPDISPATCH pDispatch) : myDispatchDriver(pDispatch) {}
//	_Presentation(const _Presentation& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
/*	LPDISPATCH GetApplication();
	LPDISPATCH GetParent();
	LPDISPATCH GetSlideMaster();
	LPDISPATCH GetTitleMaster();
	long GetHasTitleMaster();
	LPDISPATCH AddTitleMaster();
	void ApplyTemplate(LPCTSTR FileName);
	CString GetTemplateName();
	LPDISPATCH GetNotesMaster();
	LPDISPATCH GetHandoutMaster();
*/	LPDISPATCH GetSlides();
	LPDISPATCH GetPageSetup();
/*	LPDISPATCH GetColorSchemes();
	LPDISPATCH GetExtraColors();
*/	LPDISPATCH GetSlideShowSettings();
/*	LPDISPATCH GetFonts();
	LPDISPATCH GetWindows();
	LPDISPATCH GetTags();
	LPDISPATCH GetDefaultShape();
	LPDISPATCH GetBuiltInDocumentProperties();
	LPDISPATCH GetCustomDocumentProperties();
	LPDISPATCH GetVBProject();
	long GetReadOnly();
	CString GetFullName();
	CString GetName();
	CString GetPath();
*/	long GetSaved();
	void SetSaved(long nNewValue);
/*	long GetLayoutDirection();
	void SetLayoutDirection(long nNewValue);
	LPDISPATCH NewWindow();
	void FollowHyperlink(LPCTSTR Address, LPCTSTR SubAddress, BOOL NewWindow, BOOL AddHistory, LPCTSTR ExtraInfo, long Method, LPCTSTR HeaderInfo);
	void AddToFavorites();
*/	LPDISPATCH GetPrintOptions();
	void PrintOut(long From, long To, LPCTSTR PrintToFile, long Copies, long Collate);
	void Save();
	void SaveAs(LPCTSTR FileName, long FileFormat, long EmbedTrueTypeFonts);
/*	void SaveCopyAs(LPCTSTR FileName, long FileFormat, long EmbedTrueTypeFonts);
	void Export(LPCTSTR Path, LPCTSTR FilterName, long ScaleWidth, long ScaleHeight);
*/	void Close();
/*	LPDISPATCH GetContainer();
	long GetDisplayComments();
	void SetDisplayComments(long nNewValue);
	long GetFarEastLineBreakLevel();
	void SetFarEastLineBreakLevel(long nNewValue);
	CString GetNoLineBreakBefore();
	void SetNoLineBreakBefore(LPCTSTR lpszNewValue);
	CString GetNoLineBreakAfter();
	void SetNoLineBreakAfter(LPCTSTR lpszNewValue);
	void UpdateLinks();
	LPDISPATCH GetSlideShowWindow();
	long GetFarEastLineBreakLanguage();
	void SetFarEastLineBreakLanguage(long nNewValue);
	void WebPagePreview();
	long GetDefaultLanguageID();
	void SetDefaultLanguageID(long nNewValue);
	LPDISPATCH GetCommandBars();
	LPDISPATCH GetPublishObjects();
	LPDISPATCH GetWebOptions();
	LPDISPATCH GetHTMLProject();
	void ReloadAs(long cp);
	long GetEnvelopeVisible();
	void SetEnvelopeVisible(long nNewValue);
	long GetVBASigned();
*/
};

/////////////////////////////////////////////////////////////////////////////
// Shapes wrapper class

class Shapes : public myDispatchDriver
{
public:
	Shapes() {}		// Calls COleDispatchDriver default constructor
	Shapes(LPDISPATCH pDispatch) : myDispatchDriver(pDispatch) {}
//	Shapes(const Shapes& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
/*	LPDISPATCH GetApplication();
	long GetCreator();
	LPDISPATCH GetParent();
*/	long GetCount();
	LPDISPATCH Item(const VARIANT& index);
/*	LPUNKNOWN Get_NewEnum();
	LPDISPATCH AddCallout(long Type, float Left, float Top, float Width, float Height);
	LPDISPATCH AddConnector(long Type, float BeginX, float BeginY, float EndX, float EndY);
	LPDISPATCH AddCurve(const VARIANT& SafeArrayOfPoints);
	LPDISPATCH AddLabel(long Orientation, float Left, float Top, float Width, float Height);
	LPDISPATCH AddLine(float BeginX, float BeginY, float EndX, float EndY);
	LPDISPATCH AddPicture(LPCTSTR FileName, long LinkToFile, long SaveWithDocument, float Left, float Top, float Width, float Height);
	LPDISPATCH AddPolyline(const VARIANT& SafeArrayOfPoints);
	LPDISPATCH AddShape(long Type, float Left, float Top, float Width, float Height);
	LPDISPATCH AddTextEffect(long PresetTextEffect, LPCTSTR Text, LPCTSTR FontName, float FontSize, long FontBold, long FontItalic, float Left, float Top);
	LPDISPATCH AddTextbox(long Orientation, float Left, float Top, float Width, float Height);
	LPDISPATCH BuildFreeform(long EditingType, float X1, float Y1);
*/	void SelectAll();
/*	LPDISPATCH Range(const VARIANT& index);
*/	long GetHasTitle();
/*	LPDISPATCH AddTitle();
*/	LPDISPATCH GetTitle();
/*	LPDISPATCH GetPlaceholders();
	LPDISPATCH AddOLEObject(float Left, float Top, float Width, float Height, LPCTSTR ClassName, LPCTSTR FileName, long DisplayAsIcon, LPCTSTR IconFileName, long IconIndex, LPCTSTR IconLabel, long Link);
	LPDISPATCH AddComment(float Left, float Top, float Width, float Height);
	LPDISPATCH AddPlaceholder(long Type, float Left, float Top, float Width, float Height);
	LPDISPATCH AddMediaObject(LPCTSTR FileName, float Left, float Top, float Width, float Height);
	LPDISPATCH Paste();
	LPDISPATCH AddTable(long NumRows, long NumColumns, float Left, float Top, float Width, float Height);
*/
};
/////////////////////////////////////////////////////////////////////////////
// Shape wrapper class

class Shape : public myDispatchDriver
{
public:
	Shape() {}		// Calls COleDispatchDriver default constructor
	Shape(LPDISPATCH pDispatch) : myDispatchDriver(pDispatch) {}
//	Shape(const Shape& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
/*	LPDISPATCH GetApplication();
	long GetCreator();
	LPDISPATCH GetParent();
	void Apply();
*/	void Delete();
/*	void Flip(long FlipCmd);
	void IncrementLeft(float Increment);
	void IncrementRotation(float Increment);
	void IncrementTop(float Increment);
	void PickUp();
	void RerouteConnections();
	void ScaleHeight(float Factor, long RelativeToOriginalSize, long fScale);
	void ScaleWidth(float Factor, long RelativeToOriginalSize, long fScale);
	void SetShapesDefaultProperties();
	LPDISPATCH Ungroup();
	void ZOrder(long ZOrderCmd);
	LPDISPATCH GetAdjustments();
	long GetAutoShapeType();
	void SetAutoShapeType(long nNewValue);
	long GetBlackWhiteMode();
	void SetBlackWhiteMode(long nNewValue);
	LPDISPATCH GetCallout();
	long GetConnectionSiteCount();
	long GetConnector();
	LPDISPATCH GetConnectorFormat();
	LPDISPATCH GetFill();
	LPDISPATCH GetGroupItems();
	float GetHeight();
	void SetHeight(float newValue);
	long GetHorizontalFlip();
	float GetLeft();
	void SetLeft(float newValue);
	LPDISPATCH GetLine();
	long GetLockAspectRatio();
	void SetLockAspectRatio(long nNewValue);
*/
//    CString GetName();
/*	void SetName(LPCTSTR lpszNewValue);
	LPDISPATCH GetNodes();
	float GetRotation();
	void SetRotation(float newValue);
	LPDISPATCH GetPictureFormat();
	LPDISPATCH GetShadow();
	LPDISPATCH GetTextEffect();
*/	LPDISPATCH GetTextFrame();
/*	LPDISPATCH GetThreeD();
	float GetTop();
	void SetTop(float newValue);
*/	long GetType();
/*	long GetVerticalFlip();
	VARIANT GetVertices();
	long GetVisible();
	void SetVisible(long nNewValue);
	float GetWidth();
	void SetWidth(float newValue);
	long GetZOrderPosition();
	LPDISPATCH GetOLEFormat();
	LPDISPATCH GetLinkFormat();
	LPDISPATCH GetPlaceholderFormat();
	LPDISPATCH GetAnimationSettings();
	LPDISPATCH GetActionSettings();
	LPDISPATCH GetTags();
	void Cut();
	void Copy();
	void Select(long Replace);
	LPDISPATCH Duplicate();
	long GetMediaType();
*/	long GetHasTextFrame();
/*	LPDISPATCH GetScript();
	CString GetAlternativeText();
	void SetAlternativeText(LPCTSTR lpszNewValue);
	long GetHasTable();
	LPDISPATCH GetTable();
*/
};

/////////////////////////////////////////////////////////////////////////////
// TextFrame wrapper class

class TextFrame : public myDispatchDriver
{
public:
	TextFrame() {}		// Calls COleDispatchDriver default constructor
	TextFrame(LPDISPATCH pDispatch) : myDispatchDriver(pDispatch) {}
//	TextFrame(const TextFrame& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
/*	LPDISPATCH GetApplication();
	long GetCreator();
	LPDISPATCH GetParent();
	float GetMarginBottom();
	void SetMarginBottom(float newValue);
	float GetMarginLeft();
	void SetMarginLeft(float newValue);
	float GetMarginRight();
	void SetMarginRight(float newValue);
	float GetMarginTop();
	void SetMarginTop(float newValue);
	long GetOrientation();
	void SetOrientation(long nNewValue);
*/	long GetHasText();
	LPDISPATCH GetTextRange();
/*	LPDISPATCH GetRuler();
	long GetHorizontalAnchor();
	void SetHorizontalAnchor(long nNewValue);
	long GetVerticalAnchor();
	void SetVerticalAnchor(long nNewValue);
	long GetAutoSize();
	void SetAutoSize(long nNewValue);
	long GetWordWrap();
	void SetWordWrap(long nNewValue);
	void DeleteText();
*/
};
/////////////////////////////////////////////////////////////////////////////
// TextRange wrapper class

class TextRange : public myDispatchDriver
{
public:
	TextRange() {}		// Calls COleDispatchDriver default constructor
	TextRange(LPDISPATCH pDispatch) : myDispatchDriver(pDispatch) {}
//	TextRange(const TextRange& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
/*	long GetCount();
	LPDISPATCH GetApplication();
	LPDISPATCH GetParent();
	LPDISPATCH GetActionSettings();
	long GetStart();
	long GetLength();
	float GetBoundLeft();
	float GetBoundTop();
	float GetBoundWidth();
	float GetBoundHeight();
	LPDISPATCH Paragraphs(long Start, long Length);
	LPDISPATCH Sentences(long Start, long Length);
	LPDISPATCH Words(long Start, long Length);
	LPDISPATCH Characters(long Start, long Length);
	LPDISPATCH Lines(long Start, long Length);
	LPDISPATCH Runs(long Start, long Length);
	LPDISPATCH TrimText();
*/
    wxString GetText();
/*	void SetText(LPCTSTR lpszNewValue);
	LPDISPATCH InsertAfter(LPCTSTR NewText);
	LPDISPATCH InsertBefore(LPCTSTR NewText);
	LPDISPATCH InsertDateTime(long DateTimeFormat, long InsertAsField);
	LPDISPATCH InsertSlideNumber();
	LPDISPATCH InsertSymbol(LPCTSTR FontName, long CharNumber, long Unicode);
	LPDISPATCH GetFont();
	LPDISPATCH GetParagraphFormat();
	long GetIndentLevel();
	void SetIndentLevel(long nNewValue);
	void Select();
	void Cut();
	void Copy();
	void Delete();
	LPDISPATCH Paste();
	void ChangeCase(long Type);
	void AddPeriods();
	void RemovePeriods();
	LPDISPATCH Find(LPCTSTR FindWhat, long After, long MatchCase, long WholeWords);
	LPDISPATCH Replace(LPCTSTR FindWhat, LPCTSTR ReplaceWhat, long After, long MatchCase, long WholeWords);
	void RotatedBounds(float* X1, float* Y1, float* X2, float* Y2, float* X3, float* Y3, float* x4, float* y4);
	long GetLanguageID();
	void SetLanguageID(long nNewValue);
	void RtlRun();
	void LtrRun();
*/
};
/////////////////////////////////////////////////////////////////////////////
// PageSetup wrapper class

class PageSetup : public myDispatchDriver
{
public:
	PageSetup() {}		// Calls COleDispatchDriver default constructor
//	PageSetup(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
//	PageSetup(const PageSetup& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
/*	LPDISPATCH GetApplication();
	LPDISPATCH GetParent();
	long GetFirstSlideNumber();
	void SetFirstSlideNumber(long nNewValue);
	float GetSlideHeight();
*/	void SetSlideHeight(float newValue);
/*	float GetSlideWidth();
*/	void SetSlideWidth(float newValue);
/*	long GetSlideSize();
	void SetSlideSize(long nNewValue);
	long GetNotesOrientation();
	void SetNotesOrientation(long nNewValue);
	long GetSlideOrientation();
	void SetSlideOrientation(long nNewValue);
*/};
/////////////////////////////////////////////////////////////////////////////
// PrintOptions wrapper class

class PrintOptions : public myDispatchDriver
{
public:
	PrintOptions() {}		// Calls COleDispatchDriver default constructor
	PrintOptions(LPDISPATCH pDispatch) : myDispatchDriver(pDispatch) {}
//	PrintOptions(const PrintOptions& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	LPDISPATCH GetApplication();
	long GetPrintColorType();
	void SetPrintColorType(long nNewValue);
	long GetCollate();
	void SetCollate(long nNewValue);
	long GetFitToPage();
	void SetFitToPage(long nNewValue);
	long GetFrameSlides();
	void SetFrameSlides(long nNewValue);
	long GetNumberOfCopies();
	void SetNumberOfCopies(long nNewValue);
	long GetOutputType();
	void SetOutputType(long nNewValue);
	LPDISPATCH GetParent();
	long GetPrintHiddenSlides();
	void SetPrintHiddenSlides(long nNewValue);
	long GetPrintInBackground();
	void SetPrintInBackground(long nNewValue);
	long GetRangeType();
	void SetRangeType(long nNewValue);
	LPDISPATCH GetRanges();
	long GetPrintFontsAsGraphics();
	void SetPrintFontsAsGraphics(long nNewValue);
//	CString GetSlideShowName();
	void SetSlideShowName(LPCTSTR lpszNewValue);
//	CString GetActivePrinter();
	void SetActivePrinter(LPCTSTR lpszNewValue);
	long GetHandoutOrder();
	void SetHandoutOrder(long nNewValue);
	long GetPrintComments();
	void SetPrintComments(long nNewValue);
};
