//
// ms_ppt.cpp : Defines the entry point for the MS PowerPoint DLL
//


#include <wx/wx.h>
#include <wx/dir.h>

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <windows.h>
#include <winspool.h>
#include <process.h>

#include <wx/app.h>
#include <wx/msw/registry.h>

#include <wx/filename.h>
//#include <wx/metafile.h>
#include <wx/file.h>
#include <wx/wfstream.h>
#include <wx/zstream.h>

#ifdef MS_PPT_EXPORTS
#define MS_PPT_API __declspec(dllexport)
#else
#define MS_PPT_API __declspec(dllimport)
#endif

#include "msppt9.h"


extern "C" MS_PPT_API int WINAPI dsGetRelPri( void );
extern "C" MS_PPT_API int WINAPI dsGetPriLen( void );
extern "C" MS_PPT_API int WINAPI dsGetPriTypes( char *pC, int iMaxLen );
extern "C" MS_PPT_API int WINAPI dsGetSecLen( void );
extern "C" MS_PPT_API int WINAPI dsGetSecTypes( char *pC, int iMaxLen );

extern "C" MS_PPT_API int WINAPI dsGetStatus( void );
extern "C" MS_PPT_API int WINAPI dsGetPreLoadExeCmdLen( void );
extern "C" MS_PPT_API int WINAPI dsGetPreLoadExeCmd( char *pC, int iMaxLen );
extern "C" MS_PPT_API int WINAPI dsGetPostLoadKillCmdLen( void );
extern "C" MS_PPT_API int WINAPI dsGetPostLoadKillCmd( char *pC, int iMaxLen );
extern "C" MS_PPT_API int WINAPI dsPreLoad( );
extern "C" MS_PPT_API int WINAPI dsLoad( char *pFN, unsigned long ulHWnd );
extern "C" MS_PPT_API int WINAPI dsPostLoad( );
extern "C" MS_PPT_API int WINAPI dsGetNumPages( void );

extern "C" MS_PPT_API int WINAPI dsSetTempPath( const char *pC );
extern "C" MS_PPT_API int WINAPI dsGetTempPath( char *pC, int iMaxLen );

extern "C" MS_PPT_API void WINAPI dsAbort( );


int             dsRelPri    = 3000;
int             dsStatus    = 0;
int             gNumPages   = 0;
bool            gfAbort     = false;
wxString        goBuf;
wxString        gFN;


char    strPriTypes[] = {"PowerPoint Presentations(*.ppt;*.pps;*.pptx)|*.ppt;*.pps;*.pptx|PowerPoint Design Templates(*.pot)|*.pot|PowerPoint Add-Ins(*.ppa)|*.ppa"};
char    strSecTypes[] = {""};
char    strPreLoadExeCmd[]  = {""};
char    strPostLoadKillCmd[]= {""};


//*****************************************************************************
BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    gfAbort = false;
    switch (fdwReason)
    {
        case DLL_PROCESS_ATTACH:
            // attach to process
            // return FALSE to fail DLL load
            break;

        case DLL_PROCESS_DETACH:
            // detach from process
            break;

        case DLL_THREAD_ATTACH:
            // attach to thread
            break;

        case DLL_THREAD_DETACH:
            // detach from thread
            break;
    }
    return TRUE; // succesful
}


//*****************************************************************************
//  Sets the internal abort flag which we check as frequently as possible
MS_PPT_API void WINAPI dsAbort( )
{
    gfAbort = true;
}


//*****************************************************************************
static void CompressFile(wxString& aDir, wxString& aIn, wxString& aOut)
{
    wxFileInputStream inS(aDir + aIn);
    wxFileOutputStream outS(aDir + aOut);
    wxZlibOutputStream outZ(outS, 2, wxZLIB_GZIP);
    
    outZ.Write(inS);
}

//*****************************************************************************
static void BuildIndex( wxString &aDirectory )
{
    wxString    strDirectory = aDirectory;
    if( !strDirectory.EndsWith( wxT("\\") ) )
        strDirectory += wxT("\\");

    wxString    fileName;
    wxString    fileSpec( wxT("*.PNG") );
    wxString    baseName;
    wxDir       dir( strDirectory );
    if( dir.IsOpened( ) )
    {
        //  Create the index file
        bool        fRet;
        wxString    indexText;
        indexText += _T("<index>\n");

        int     iIndex  = 1;
        bool    fMore = dir.GetFirst( &fileName, fileSpec, wxDIR_FILES );
        if( fMore )
        {
            int i = fileName.Find(wxT('.'), true);
            while (i>0 && isdigit(fileName[i-1]))
                --i;
            baseName = fileName.Left(i);
        }
        
        if (baseName.Len() > 0)
        {
            while (true)
            {
                wxString iterName = wxString::Format(wxT("%s%d"), baseName.c_str(), iIndex);
                wxString imageName = iterName + wxT(".EMF");
                wxString compressName = iterName + wxT(".EMZ");
                if (wxFile::Exists(strDirectory + imageName))
                {
                    CompressFile(strDirectory, imageName, compressName);
                    wxRemoveFile(strDirectory + imageName);
                } 
                else 
                {
                    compressName = iterName + wxT(".PNG");
                }
                    
                wxString altName = iterName + wxT(".PNG");
                if (wxFile::Exists(strDirectory + altName))
                {
                    wxString strT = wxString::Format( wxT("<page title=\"Slide %d\" file=\"%s\" altfile=\"%s\" scale=\"1.0\" altscale=\"4.0\" fittowidth=\"F\"/>\n"),
                                            iIndex++, compressName.c_str( ), altName.c_str( ) );
                    indexText += strT;
                }
                else
                    break;
            }
        }

        indexText += wxT("</index>\n");
        wxString    strOut( strDirectory );
        strOut.Append( wxT("\\index.xml") );

        wxFile  indexFile( strOut, wxFile::write );
        fRet = indexFile.Write( indexText );
        indexFile.Close( );
    }
    return;
}


//*****************************************************************************
//  This contains the current status/progress of the DS.
MS_PPT_API int WINAPI dsGetStatus( void )
{
    return dsStatus;
}


//*****************************************************************************
//  This contains the current status/progress of the DS.
MS_PPT_API int WINAPI dsGetRelPri( void )
{
    return dsRelPri;
}


//*****************************************************************************
MS_PPT_API int WINAPI dsGetPriLen( void )
{
    return strlen( strPriTypes );
}


//*****************************************************************************
MS_PPT_API int WINAPI dsGetSecLen( void )
{
    return strlen( strSecTypes );
}


//*****************************************************************************
MS_PPT_API int WINAPI dsGetPreLoadExeCmdLen( void )
{
    return strlen( strPreLoadExeCmd );
}


//*****************************************************************************
MS_PPT_API int WINAPI dsGetPostLoadKillCmdLen( void )
{
    return strlen( strPostLoadKillCmd );
}


//*****************************************************************************
MS_PPT_API int WINAPI dsGetNumPages( void )
{
    return gNumPages;
}


//*****************************************************************************
MS_PPT_API int WINAPI dsGetPriTypes( char *pC, int iMaxLen )
{
    int iLen = strlen( strPriTypes );
    if( iLen > iMaxLen )
    {
        strncpy( pC, strPriTypes, iMaxLen - 1 );
        pC[ iMaxLen - 1 ] = 0;
        return -iLen;
    }

    strcpy( pC, strPriTypes );
    pC[ iLen ] = 0;
    return iLen;
}


//*****************************************************************************
MS_PPT_API int WINAPI dsGetSecTypes( char *pC, int iMaxLen )
{
    int iLen = strlen( strSecTypes );
    if( iLen > iMaxLen )
    {
        strncpy( pC, strSecTypes, iMaxLen - 1 );
        pC[ iMaxLen - 1 ] = 0;
        return -iLen;
    }

    strcpy( pC, strSecTypes );
    pC[ iLen ] = 0;
    return iLen;
}


//*****************************************************************************
MS_PPT_API int WINAPI dsGetPreLoadExeCmd( char *pC, int iMaxLen )
{
    int iLen = strlen( strPreLoadExeCmd );
    if( iLen > iMaxLen )
    {
        strncpy( pC, strPreLoadExeCmd, iMaxLen - 1 );
        pC[ iMaxLen - 1 ] = 0;
        return -iLen;
    }

    strcpy( pC, strPreLoadExeCmd );
    pC[ iLen ] = 0;
    return iLen;
}


//*****************************************************************************
MS_PPT_API int WINAPI dsGetPostLoadKillCmd( char *pC, int iMaxLen )
{
    int iLen = strlen( strPostLoadKillCmd );
    if( iLen > iMaxLen )
    {
        strncpy( pC, strPostLoadKillCmd, iMaxLen - 1 );
        pC[ iMaxLen - 1 ] = 0;
        return -iLen;
    }

    strcpy( pC, strPostLoadKillCmd );
    pC[ iLen ] = 0;
    return iLen;
}


//*****************************************************************************
MS_PPT_API int WINAPI dsSetTempPath( const char *pC )
{
    goBuf = wxString(pC, wxConvUTF8);
    return 0;
}


//*****************************************************************************
MS_PPT_API int WINAPI dsGetTempPath( char *pC, int iMaxLen )
{
    int     iLen = goBuf.Len( );
    if( iLen > iMaxLen )
    {
        strncpy( pC, goBuf.mb_str( wxConvUTF8 ), iMaxLen - 1 );
        pC[ iMaxLen - 1 ] = 0;
        return -iLen;
    }

    strcpy( pC, goBuf.mb_str( wxConvUTF8 ) );
    pC[ iLen ] = 0;
    return iLen;
}


//*****************************************************************************
MS_PPT_API int WINAPI dsPreLoad( )
{
    gfAbort = false;
    dsStatus = 1;     //  Indicate we have completed our work
    return 0;
}


//*****************************************************************************
//  Strategy:  Save each slide of the presentation as a separate EMF file,
//    then convert each EMF image to a high resolution PNG.
//  Upload both the EMF and the PNG to the server.
unsigned long WINAPI ThreadFunc( LPVOID lpParam )
{
    bool        fRet;

    CoInitializeEx( NULL, COINIT_APARTMENTTHREADED );

    // Start PowerPoint.
    _Application    app;
    if( !app.CreateDispatch( wxT("Powerpoint.Application") ) )
    {
        wxLogDebug( wxT("Unknown error returned by app.CreateDispatch( )") );
        dsStatus = -1;
        return 0;
    }
    //  It must be visible to create the page images ... NOT if the open has the right flags
    // app.SetVisible( true );

    Presentations   presentations   = app.GetPresentations( );
    _Presentation   presentation    = presentations.Open( gFN.wc_str( ), 1, 0, 0 );

    if (!presentation.IsOk())
    {
        wxLogDebug( wxT("Unknown error returned by app.CreateDispatch( )") );
        dsStatus = -1;
        return 0;        
    }

    wxString    strTarget;
    if (!goBuf.IsEmpty())
        strTarget = goBuf;
    else
        strTarget = wxGetCwd();
        
    strTarget.Append( wxT("\\dspages") );

    wxFileName::Mkdir( strTarget, 0777,  wxPATH_MKDIR_FULL);

    if( !gfAbort )
    {
        //  Delete all files from our temp output folder
        wxString    fileSpec( wxT("*.*") );
        wxString    fileName;

        wxDir       dir( strTarget );
        if( dir.IsOpened( ) )
        {
            bool fMore = dir.GetFirst( &fileName, fileSpec, wxDIR_FILES );
            while( fMore )
            {
                wxString    strFull;
                strFull = strTarget + wxT("\\") + fileName;

                ::wxRemoveFile( strFull );

                fMore = dir.GetNext( &fileName );
                fRet = fMore;
            }
        }
    }

    if( !gfAbort )
    {
        Slides slides = presentation.GetSlides( );
        gNumPages = slides.GetCount( );
        slides.ReleaseDispatch( );
    }

#ifdef USE_METAFILE
    if( !gfAbort )
        presentation.SaveAs( strTarget.wc_str( ), ppSaveAsEMF, 0 );
#endif

    if( !gfAbort )
        presentation.SaveAs( strTarget.wc_str( ), ppSaveAsPNG, 0 );
        
    goBuf.Empty( );
    goBuf.Append( strTarget );

    // Close PowerPoint
    presentation.Close( );
    presentation.ReleaseDispatch( );
    presentations.ReleaseDispatch( );
    
    app.ReleaseDispatch( );

    // Build index file; also compress EMF files
    BuildIndex( strTarget );

    dsStatus = 1;
    return 1;
}


//*****************************************************************************
MS_PPT_API int WINAPI dsLoad( char *pFN, unsigned long ulWnd )
{
    wxString    strFN( pFN, wxConvUTF8 );
    gFN.Empty( );
    gFN.Append( strFN );

    dsStatus = 0;
    gfAbort = false;

    unsigned long   dwThreadParam   = 1;
    unsigned long   dwThreadID      = 1;

    CreateThread( NULL, 0, ThreadFunc, &dwThreadParam, 0, &dwThreadID );
    return 1;
}


//*****************************************************************************
MS_PPT_API int WINAPI dsPostLoad( )
{
    gfAbort = false;
    dsStatus = 1;
    return 0;
}
