#include "blowfish.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// ECB Test data
typedef struct
{
  const char *key;
  const char *clear;
  const char *cipher;
}ECB_TEST_DATA;

/** Test the blowfish implementation using standard ECB test data */
class CTestBlowFish
{
 public:
  CTestBlowFish();

  /** Run all tests. <br>
   * All output is via stdout.
   * @return int Returns 0 if all tests pass. Returns non-zero on failure.
   */
  int runTests();

 protected:

   /**
    * Convert a standard nul terminated hex sstring into a BYTE array
    */
  DWORD pszHexToByteArray( const char *psz, BYTE *pszArray, DWORD len );

  /**
   * Print a BYTE buffer as a hex string to stdout
   */
  void printHex( BYTE *szBuf, DWORD len );

  /**
   * Compare two buffers and return true if they are indentical.
   */
  int isIdentical( BYTE *sz1, BYTE* sz2, DWORD len );

  /**
   * Run the specified ecb test.
   */
  int runTest( ECB_TEST_DATA * pecb );

 private:
  /** The blowfish instance */
  CBlowFish m_bf;

};

