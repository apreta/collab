#include "testblowfish.h"


#define MAX_KEY_BYTES 48
#define MAX_TEST_BYTES 8

ECB_TEST_DATA ecb[] =
  {
    // Key                 Clear text         Cipher
    // ----------------    ----------------   ----------------
    { "0000000000000000", "0000000000000000","4EF997456198DD78" },
    { "FFFFFFFFFFFFFFFF", "FFFFFFFFFFFFFFFF","51866FD5B85ECB8A" },
    { "3000000000000000", "1000000000000001","7D856F9A613063F2" },
    { "1111111111111111", "1111111111111111","2466DD878B963C9D" },
    { "0123456789ABCDEF", "1111111111111111","61F9C3802281B096" },
    { "1111111111111111", "0123456789ABCDEF","7D0CC630AFDA1EC7" },
    { "0000000000000000", "0000000000000000","4EF997456198DD78" },
    { "FEDCBA9876543210", "0123456789ABCDEF","0ACEAB0FC6A0A28D" },
    { "7CA110454A1A6E57", "01A1D6D039776742","59C68245EB05282B" },
    { "0131D9619DC1376E", "5CD54CA83DEF57DA","B1B8CC0B250F09A0" },
    { "07A1133E4A0B2686", "0248D43806F67172","1730E5778BEA1DA4" },
    { "3849674C2602319E", "51454B582DDF440A","A25E7856CF2651EB" },
    { "04B915BA43FEB5B6", "42FD443059577FA2","353882B109CE8F1A" },
    { "0113B970FD34F2CE", "059B5E0851CF143A","48F4D0884C379918" },
    { "0170F175468FB5E6", "0756D8E0774761D2","432193B78951FC98" },
    { "43297FAD38E373FE", "762514B829BF486A","13F04154D69D1AE5" },
    { "07A7137045DA2A16", "3BDD119049372802","2EEDDA93FFD39C79" },
    { "04689104C2FD3B2F", "26955F6835AF609A","D887E0393C2DA6E3" },
    { "37D06BB516CB7546", "164D5E404F275232","5F99D04F5B163969" },
    { "1F08260D1AC2465E", "6B056E18759F5CCA","4A057A3B24D3977B" },
    { "584023641ABA6176", "004BD6EF09176062","452031C1E4FADA8E" },
    { "025816164629B007", "480D39006EE762F2","7555AE39F59B87BD" },
    { "49793EBC79B3258F", "437540C8698F3CFA","53C55F9CB49FC019" },
    { "4FB05E1515AB73A7", "072D43A077075292","7A8E7BFA937E89A3" },
    { "49E95D6D4CA229BF", "02FE55778117F12A","CF9C5D7A4986ADB5" },
    { "018310DC409B26D6", "1D9D5C5018F728C2","D1ABB290658BC778" },
    { "1C587F1C13924FEF", "305532286D6F295A","55CB3774D13EF201" },
    { "0101010101010101", "0123456789ABCDEF","FA34EC4847B268B2" },
    { "1F1F1F1F0E0E0E0E", "0123456789ABCDEF","A790795108EA3CAE" },
    { "E0FEE0FEF1FEF1FE", "0123456789ABCDEF","C39E072D9FAC631D" },
    { "0000000000000000", "FFFFFFFFFFFFFFFF","014933E0CDAFF6E4" },
    { "FFFFFFFFFFFFFFFF", "0000000000000000","F21E9A77B71C49BC" },
    { "0123456789ABCDEF", "0000000000000000","245946885754369A" },
    { "FEDCBA9876543210", "FFFFFFFFFFFFFFFF","6B5C5A9C5D9E0A5A" },
    { 0,0,0 }
  };

/**
Test data block 2:
Longest key = 48 Bytes == 5*8 Bytes == 48 * 8 = 384 Bit key
**/
ECB_TEST_DATA ecb2[] = {
  // Key                                                    Clear text          Cipher
  // --------------------------------------------------     ----------------    ----------------
  ///1234567890123456789012345678901234567890
  { "F0",                                                  "FEDCBA9876543210", "F9AD597C49DB005E" },
  { "F0E1",                                                "FEDCBA9876543210", "E91D21C1D961A6D6" },
  { "F0E1D2",                                              "FEDCBA9876543210", "E9C2B70A1BC65CF3" },
  { "F0E1D2C3",                                            "FEDCBA9876543210", "BE1E639408640F05" },
  { "F0E1D2C3B4",                                          "FEDCBA9876543210", "B39E44481BDB1E6E" },
  { "F0E1D2C3B4A5",                                        "FEDCBA9876543210", "9457AA83B1928C0D" },
  { "F0E1D2C3B4A596",                                      "FEDCBA9876543210", "8BB77032F960629D" },
  { "F0E1D2C3B4A59687",                                    "FEDCBA9876543210", "E87A244E2CC85E82" },
  { "F0E1D2C3B4A5968778",                                  "FEDCBA9876543210", "15750E7A4F4EC577" },
  { "F0E1D2C3B4A596877869",                                "FEDCBA9876543210", "122BA70B3AB64AE0" },
  { "F0E1D2C3B4A5968778695A",                              "FEDCBA9876543210", "3A833C9AFFC537F6" },
  { "F0E1D2C3B4A5968778695A4B",                            "FEDCBA9876543210", "9409DA87A90F6BF2" },
  { "F0E1D2C3B4A5968778695A4B3C",                          "FEDCBA9876543210", "884F80625060B8B4" },
  { "F0E1D2C3B4A5968778695A4B3C2D",                        "FEDCBA9876543210", "1F85031C19E11968" },
  { "F0E1D2C3B4A5968778695A4B3C2D1E",                      "FEDCBA9876543210", "79D9373A714CA34F" },
  { "F0E1D2C3B4A5968778695A4B3C2D1E0F",                    "FEDCBA9876543210", "93142887EE3BE15C" },
  { "F0E1D2C3B4A5968778695A4B3C2D1E0F00",                  "FEDCBA9876543210", "03429E838CE2D14B" },
  { "F0E1D2C3B4A5968778695A4B3C2D1E0F0011",                "FEDCBA9876543210", "A4299E27469FF67B" },
  { "F0E1D2C3B4A5968778695A4B3C2D1E0F001122",              "FEDCBA9876543210", "AFD5AED1C1BC96A8" },
  { "F0E1D2C3B4A5968778695A4B3C2D1E0F00112233",            "FEDCBA9876543210", "10851C0E3858DA9F" },
  { "F0E1D2C3B4A5968778695A4B3C2D1E0F0011223344",          "FEDCBA9876543210", "E6F51ED79B9DB21F" },
  { "F0E1D2C3B4A5968778695A4B3C2D1E0F001122334455",        "FEDCBA9876543210", "64A6E14AFD36B46F" },
  { "F0E1D2C3B4A5968778695A4B3C2D1E0F00112233445566",      "FEDCBA9876543210", "80C7D7D45A5479AD" },
  { "F0E1D2C3B4A5968778695A4B3C2D1E0F0011223344556677",    "FEDCBA9876543210", "05044B62FA52D080" },
  ///000000000111111111122222222223333333333444444444
  ///123456789012345678901234567890123456789012345678
  {0,0,0}
};


/**
 * Convert a standard nul terminated hex sstring into a QByteArray
 */
DWORD CTestBlowFish::pszHexToByteArray( const char *psz, BYTE *pszArray, DWORD len )
{
  int offset = 0;
  char digit;
  char szDigit[3];
  char *pszEnd;

  int i = 0;
  offset = 0;
  while( offset < strlen(psz) )
  {
    szDigit[0] = psz[offset++];
    szDigit[1] = psz[offset++];
    szDigit[2] = '\0';
    digit = (char)strtol( szDigit, &pszEnd, 16 );
    pszArray[i++] = digit;
  }
  return i;
}

/**
 * Print a byte buffer as a hex string to stdout
 */
void CTestBlowFish::printHex( BYTE *szBuf, DWORD len )
{
  int i;
  for( i = 0; i < len; ++i )
  {
    BYTE c = szBuf[i];
    fprintf(stdout,"%02X",c);
  }
}

/**
 * Compare two buffers and return true if they are indentical.
 */
int CTestBlowFish::isIdentical( BYTE *sz1, BYTE* sz2, DWORD len )
{
  int i = 0;
  while( i < len )
  {
    if( sz1[i] != sz2[i] )
      return 0;
    ++i;
  }
  return 1;
}

/**
 * Run the specified ecb test.
 */
int CTestBlowFish::runTest( ECB_TEST_DATA * pecb )
{
  BYTE szKeyBytes[ MAX_KEY_BYTES+1 ];
  BYTE szClearText[ MAX_TEST_BYTES+1 ];
  BYTE szCipherText[ MAX_TEST_BYTES+1 ];
  BYTE szTestCipherText[ MAX_TEST_BYTES+1 ];
  BYTE szTestClearText[ MAX_TEST_BYTES+1 ];
  DWORD keylen = 0;

  keylen = pszHexToByteArray( pecb->key, szKeyBytes, MAX_KEY_BYTES );
  pszHexToByteArray( pecb->clear, szClearText, MAX_TEST_BYTES );
  pszHexToByteArray( pecb->cipher, szCipherText, MAX_TEST_BYTES );

  CBlowFish bf1;
  //bf1.SetKey( pecb->key );
  bf1.Initialize( szKeyBytes, strlen(pecb->key)/2 );

  bf1.Encode( szClearText, szTestCipherText, MAX_TEST_BYTES );

  CBlowFish bf2;
  //bf2.SetKey( pecb->key );
  bf2.Initialize( szKeyBytes, strlen(pecb->key)/2 );
  bf2.Decode( szTestCipherText, szTestClearText, MAX_TEST_BYTES );

  fprintf(stdout, "Key: %s,  Clr: %s,  Cipher: %s,", pecb->key, pecb->clear, pecb->cipher );

  fprintf(stdout,"  Test cipher: ");
  printHex(szTestCipherText, MAX_TEST_BYTES);
  fprintf(stdout,",");

  fprintf(stdout,"  Test clr:");
  printHex(szTestClearText, MAX_TEST_BYTES);

  int CipherOK = isIdentical( szCipherText, szTestCipherText, MAX_TEST_BYTES );
  int DecipherOK = isIdentical( szClearText, szTestClearText, MAX_TEST_BYTES );

  if( CipherOK && DecipherOK )
  {
    fprintf(stdout," PASS");
  }else
  {
    if( !CipherOK )
    {
      fprintf(stdout," Cipher failed! ");
    }else
    {
      fprintf(stdout," Cipher Passed. ");
    }
    if( !DecipherOK )
    {
      fprintf(stdout," Decipher failed! ");
    }else
    {
      fprintf(stdout," Decipher passed. ");
    }
  }
  fprintf(stdout,"\n");

  return (CipherOK && DecipherOK) ? 1 : 0;
}

CTestBlowFish::CTestBlowFish()
{
}

int CTestBlowFish::runTests()
{
  int ret = 0;
  int i = 0;
  int totalTests = 0;
  int totalPassed = 0;
  int totalFailed = 0;
  DWORD keylen;

  fprintf( stdout, "Running all BlowFish tests\n");

#ifdef ORDER_ABCD
  fprintf(stdout,"Blowfish using ORDER_ABCD - Big Endian\n");
#endif

#ifdef ORDER_DCBA
  fprintf(stdout,"Blowfish using ORDER_DCBA - Little Endian\n");
#endif

#ifdef ORDER_BADC
  fprintf(stdout,"Blowfish useing ORDER_BACD - Vax\n");
#endif


#if 0
  fprintf(stdout,"Test tools validation:\n");
  i = 0;
  while( ecb[i].key )
  {
    pszHexToByteArray( ecb[i].key, szKeyBytes, MAX_TEST_BYTES );
    fprintf(stdout,"%s  ",ecb[i].key);
    printHex(szKeyBytes, MAX_TEST_BYTES);
    fprintf(stdout,"\n");
    ++i;
  }
  fprintf(stdout,"\n\n");
#endif

  fprintf(stdout,"Known values tests:\n");
  //fprintf(stdout,"Key               Clear text        Cipher            Test Cipher       Test Clear\n");
  //fprintf(stdout,"----------------  ----------------  ----------------  ----------------  ----------------\n");

  i = 0;
  while( ecb[i].key )
  {
    if( runTest( &ecb[i] ) )
      ++totalPassed;
    else
      ++totalFailed;
    ++totalTests;
    ++i;
  }

  i = 0;
  while( ecb2[i].key )
  {
    if( runTest( &ecb2[i] ) )
      ++totalPassed;
    else
      ++totalFailed;
    ++totalTests;
    ++i;

  }

  fprintf(stdout,"Total ECB tests: %d. Passed: %d , Failed: %d\n", totalTests, totalPassed, totalFailed );

  return ret;
}

#ifdef BUILD_TEST_APP

int main( int argc, char **argv )
{
  CTestBlowFish tests;

  fprintf(stderr,"cryptlib blowfish test\n\n");

  tests.runTests();

  return 0;
}

#endif
