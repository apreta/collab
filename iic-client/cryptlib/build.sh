#!/bin/bash
bakefile -f autoconf cryptlib.bkl
if [ ! -e configure ]
then
    bakefilize --copy && aclocal && autoconf
fi
./configure --prefix=$conferencing
make clean
make
# Copy and preserve links (cp --preserve=ALL n/a on Mac OS X)
ls lib*dylib* | cpio -p ../iic/clients/bin


