#!/bin/bash
echo "Cleaning up previous config info "
rm -f Makefile
rm -f Makefile.in
rm -f config.*
rm -f configure
rm -f aclocal.m4
rm -f autoconf_inc.m4
rm -r -f autom4te.cache
rm -f libcrypt*
rm -f cryptlib_test
rm -f cryptlib_test.log
rm -f cryptlib*.o
rm -f -r .deps
rm -f .DS_Store
rm -f bk-deps
echo "Config cleanup done"
