#!/bin/bash
bakefile -f autoconf cryptlib_test.bkl
if [ ! -e configure ]
then
    bakefilize --copy && aclocal && autoconf
fi
./configure --prefix=$conferencing
make clean
make
if [ -e cryptlib_test ]
then
    echo cryptlib_test application built
    ./cryptlib_test >cryptlib_test.log
    cat cryptlib_test.log
    echo Results saved in cryptlib_test.log
else
    echo The cryptlib_test program failed to build
    exit 1
fi

