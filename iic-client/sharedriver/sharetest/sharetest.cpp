// sharetest.cpp : Defines the entry point for the console application.
//

#ifdef WIN32
#include "stdafx.h"
#include <Windows.h>
#else 
#include <unistd.h>
#endif
#include <time.h>

#include "../../sharelib/share_api.h"

int main(int argc, char* argv[])
{
	char buff[512];
	
	getcwd(buff, sizeof(buff));
	
	share_initialize(buff, "1001", "server::2234", "master", "1001", "123", "aeadad", true, time(NULL));

	MonitorInfo* info = share_get_display_at(0);

	share_desktop();

#ifdef WIN32
	Sleep(5000);
#else
	sleep(15);
#endif

	share_end();

	return 0;
}

