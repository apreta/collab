/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef UNIXVIEWER_H_INCLUDED
#define UNIXVIEWER_H_INCLUDED

#include <string>

#include "share_api.h"
#include "../netlib/IICvncMsg.h"

typedef struct _GtkWidget      GtkWidget;
typedef struct _GtkWindow      GtkWindow;

class UnixViewer
{
    std::string m_ExecPath;

    share_event_handler m_Callback;
    void *m_CallbackData;

    std::string m_ReflectorAddress;
    std::string m_ReflectorPassword;
    bool        m_fEnableSecurity;
    std::string m_EncryptKey;
    std::string m_MeetingId;
    std::string m_PartId;
    std::string m_Token;
    std::string m_command;      // command to launch viewer

    int m_xwinid;
    void *m_hParent;
	bool m_bEnableControl;
	bool m_bRelaunch;

    GtkWidget* m_pizza;
    GtkWidget* m_scrolled_win;
    GtkWidget* m_viewer;

    GtkWidget* m_frame;
    bool m_FullScreen;

    static std::string s_command;

    static volatile bool s_ViewerWanted;
    static volatile bool s_VncThreadRunning;
    static volatile bool s_RunVncThread;
    static volatile bool s_QuitVncThread;

public:
    ServerStatus m_ViewerStatus;

    UnixViewer(const char *aExecPath);
    int Initialize(const char *aAddress, const char *aPassword, const char *aEncryptKey, const char *aMeetingId, const char *aPartId, const char *aToken, const bool fEnableSecurity);
    void SetCallbackHandler(void *user, share_event_handler);
    int OnClientCreated(WPARAM wParam, LPARAM lParam);
    int OnClientDestroyed(WPARAM wParam, LPARAM lParam);
	int LaunchViewer(void * hParent, bool aEnableControl, bool aRestart, int aOption);
	int ReLaunchViewer(void * hParent, bool aEnableControl, bool aRestart, int aOption);
	int ShutdownViewer(bool bWait = false);
	void MoveViewer(int x, int y, int width, int height);
	void EnableRemoteControl(bool aEnable);
	void EnableFullScreen(bool bEnable);
	int HandleEvent(int id);
    void OnGetEncryptKey( );

protected:
    int DoFullScreen();
    int DoEmbedded();
    int ReparentWindow();
    void RemoveWindow();
    static void *RunVnc(void *p);

private:
    void log_rotate_if_needed( char *logFN, bool fAlwaysRotate );

};


#endif // UNIXVIEWER_H_INCLUDED
