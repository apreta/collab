/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
// PaneAppShare.cpp : implementation file
//

#include <windows.h>
#include <tchar.h>
#include <time.h>
#include <stdio.h>

#include <process.h>

#include "share_api.h"
#include "vncviewer.h"
#include "flag.h"
#include "procutil.h"
#include <log.h>

extern bool g_debug;

extern VncViewer *g_Viewer;

volatile static bool ViewerWanted = false;

const UINT msgEnableRemoteControl			= RegisterWindowMessage(_T("IICViewer.EnableRemoteControl"));
const UINT msgEnableFullScreen				= RegisterWindowMessage(_T("IICViewer.EnableFullScreen"));
const UINT msgEnableDebugMode				= RegisterWindowMessage(_T("IICViewer.EnableDebugMode"));


/////////////////////////////////////////////////////////////////////////////
// VncViewer


VncViewer::VncViewer()
{
    m_ExecPath = new TCHAR[MAX_EXEC_PATH];
    m_Callback = NULL;
    m_CallbackData = NULL;
    
    // Get the run path for this application
	::GetModuleFileName(NULL, m_ExecPath, MAX_EXEC_PATH);
	TCHAR *p = _tcsrchr(m_ExecPath, _T('\\'));
	if (p)
        *(p+1) = _T('\0');

	m_hViewer = NULL;
	m_bEnableControl = FALSE;
	m_ShareStarted = 0;
	m_nErrorCount = 0;
	m_bNetworkError = false;
	m_nShareOption = 0;
	m_bConnected = false;
	m_bLaunching = false;

	m_nVerticalOffset = 0;
	m_bShowToolbar = FALSE;
	m_bHasControl = FALSE;

	m_bLockUpdate = false;

    m_hThread   = NULL;
}

VncViewer::~VncViewer()
{
}

void VncViewer::SetCallbackHandler(void *user, share_event_handler handler)
{
    m_Callback = handler;
    m_CallbackData = user;
}

void VncViewer::MoveViewer(int x, int y, int cx, int cy)
{
	if (m_bLockUpdate)
		return;

	// Move viewer window to match
	BOOL ok = ::MoveWindow(m_hViewer, 0, 0, cx, cy, FALSE);
	if (!ok)
	{
	    int err = GetLastError();
	    errorf("Move window error %d", err);
	}

	InvalidateRect(m_hViewer, NULL, TRUE);
}

void VncViewer::EraseBkgnd(int x, int y, int cx, int cy)
{
	if (m_bLockUpdate)
	{
		ValidateRect(m_hParent, NULL);
		return;
	}

	RECT rect;
	GetClientRect(m_hParent, &rect);
	ValidateRect(m_hParent, &rect);

	// We may need to repaint some portion of the window being drawn by the external
	// application.
	if (m_hViewer != NULL)
	{
		// Viewer displays itself at top left of window so update rectangle
		// will be same
        SetRect(&rect, x, y, x+cx, y+cy);

        ::RedrawWindow(m_hViewer, &rect, NULL, RDW_FRAME|RDW_INVALIDATE);
	}

}

int VncViewer::Initialize(const TCHAR *aAddress, const TCHAR *aPassword, const TCHAR *aEncryptKey, const TCHAR *aMeetingId, const TCHAR *aPartId, const TCHAR *aToken)
{
    m_ReflectorAddress = aAddress ? aAddress : "";
    m_ReflectorPassword = aPassword ? aPassword : "";
    m_MeetingId = aMeetingId;
    m_PartId = aPartId;
    m_Token = aToken ? aToken : "";
    if( aEncryptKey )
        m_EncryptKey = aEncryptKey;

    return 0;
}


//*****************************************************************************
int VncViewer::LaunchViewer(HWND hwnd, bool aEnableControl, bool aRestart, int aOption)
{
	m_nShareOption = aOption;

	// Prevent re-entrancy
	Flag flag(m_bLaunching);
	if (!flag.check())
		return 0;

	ShutdownViewer(true);	// always stop the current instance

    m_hParent = hwnd;

	m_hViewer = NULL;
	m_bHasControl = FALSE;

	if (m_ReflectorAddress.empty())
		return SHARE_NO_SERVER;

	// kill the previous instance if there is
#if !defined(TESTCLIENT)
	KillProcessByName(VNC_CLIENT_NAME);

	// Wait 10 seconds, then signal user that we've got a stuck process
	int retry = 20;
	while (IsAppRunning(VNC_CLIENT_NAME))
	{
		if (--retry)
			Sleep(500);
		else
			return SHARE_REBOOT;
	}
#endif

	m_bEnableControl = aEnableControl;

	// Reset error count if this is a new session
	if (!aRestart)
	{
		m_nErrorCount = 0;
		m_bNetworkError = false;
	}
	m_ShareStarted = time(NULL);

	TCHAR szHandle[10];
	_stprintf(szHandle, _T("%08X"), (UINT)m_hParent);

    TCHAR vnc[MAX_EXEC_PATH];
    _tcscpy(vnc, m_ExecPath);
    _tcscat(vnc, VNC_CLIENT_NAME);

    TCHAR   cmdLine[MAX_EXEC_PATH + MAX_EXEC_PATH];
    sprintf( cmdLine, _T("/window %s /server %s /password %s /notoolbar /nostatus /nocursorshape /quickoption 100"),
            szHandle, m_ReflectorAddress.c_str( ), m_ReflectorPassword.c_str( ) );

	if (!aEnableControl)
        _tcscat( cmdLine, _T(" /viewonly") );

	if ((m_nShareOption & ViewFullScreen)!=0)
        _tcscat( cmdLine, _T(" /fullscreen") );

	if (!m_ProxyUserName.empty())
	{
        _tcscat( cmdLine, _T(" /proxyauthname ") );
        _tcscat( cmdLine, m_ProxyUserName.c_str( ) ); // iic proxy server
	}
	if (!m_ProxyPassword.empty())
	{
        _tcscat( cmdLine, _T(" /proxypassword ") );
        _tcscat( cmdLine, m_ProxyPassword.c_str( ) );   // iic proxy port
	}

    _tcscat( cmdLine, _T(" /meetingid ") );
    _tcscat( cmdLine, m_MeetingId.c_str( ) ); // iic meeting id

    _tcscat( cmdLine, _T(" /partid ") );
    _tcscat( cmdLine, m_PartId.c_str( ) );

    _tcscat( cmdLine, _T(" /token ") );
    _tcscat( cmdLine, m_Token.c_str( ) );

	if (m_nShareOption & ViewAutoDetect)	// only when presenter enables auto detect
	{
        _tcscat( cmdLine, _T(" /detectslowconn") );
	}

	if (g_debug)
	{
        _tcscat( cmdLine, _T(" /loglevel") );
        _tcscat( cmdLine, _T(" 10") );
        _tcscat( cmdLine, _T(" /logfile") );
        _tcscat( cmdLine, _T(" \\IICShare.log") );
	}

#if 0
	TCHAR szPPTOffset[10];
	if (m_bShowToolbar)
	{
		_stprintf(szPPTOffset, _T("%d"), m_nVerticalOffset);

		args[argc++] = _T("/ppt");
		args[argc++] = szPPTOffset;
	}
#endif

	if (!m_EncryptKey.empty())
	{
        _tcscat( cmdLine, _T(" /key ") );
        _tcscat( cmdLine, m_EncryptKey.c_str( ) );
	}

#ifdef TESTCLIENT
	if (m_pDocument->m_StartupNoViewer)
	{
        _tcscat( cmdLine, _T(" /testmode") );
	}
#endif

    m_viewerParams._this = this;
    m_viewerParams.vnc[0] = '0';
    _tcscpy( m_viewerParams.vnc, vnc );
    m_viewerParams.cmdLine[0] = '0';
    _tcscpy( m_viewerParams.cmdLine, cmdLine );

    m_bConnected = false;
	ViewerWanted = true;

    debugf("Starting share viewer thread");

    DWORD   threadID;
    HANDLE  hThread;
    hThread = CreateThread( NULL, 0, StartThread, (PVOID) &m_viewerParams, 0, (LPDWORD) &threadID );
    if( hThread  ==  NULL )
    {
        //int err = GetLastError();
        return SHARE_FAILED_LAUNCH;
    }
    else
        m_hThread = hThread;

	return 0;
}


//*****************************************************************************
int VncViewer::ShutdownViewer(bool bWait)
{
    debugf("Stopping share viewer");
    
	ViewerWanted = false;
	m_bHasControl = FALSE;

    if( m_hViewer != NULL )
	{
		::PostMessage(m_hViewer, WM_CLOSE, 0, 0);
		m_hViewer = NULL;

        // There seems to be some unwanted interaction between having the parent window in this process and the child in
        // the viewer...the process doesn't want to exit until we destroy our window, which we don't necessarily want to
        // do.  So instead, summarily execute the viewer process.
		Sleep(500);
		KillProcessByName(VNC_CLIENT_NAME);
    }
	//	make sure kill the instances
	else
	{
#if !defined(TESTCLIENT)
		KillProcessByName(VNC_CLIENT_NAME);
#endif
	}

#if !defined(TESTCLIENT)
	// Wait 10 seconds, then signal user that we've got a stuck process
	if (bWait)
	{
		int retry = 20;
		while (IsAppRunning(VNC_CLIENT_NAME))
		{
			if (--retry)
				Sleep(500);
			else
				return SHARE_REBOOT;
		}
	}
#endif

    if( m_hThread  !=  NULL )
    {
        m_hThread = NULL;
    }

	return 0;
}

void VncViewer::EnableRemoteControl(bool aEnable)
{
	int count = 200;
	while (count-- > 0)
	{
		if (m_hViewer!=NULL) break;
		MsgWait(NULL, 20, 1);
	}

	if (m_hViewer != NULL && m_bEnableControl != aEnable)
	{
		m_bEnableControl = aEnable;

		// N.B. enable flag is actually implemented in vnc viewer as a "view only"
		// flag, so we negate setting here before sending.
		debugf(_T("%s app share session control"), aEnable ? _T("Enabling") : _T("Disabling"));
		::PostMessage(m_hViewer, msgEnableRemoteControl, 0, (LPARAM)(BOOL)!aEnable);
	}
}

void VncViewer::EnableFullScreen(bool aEnable)
{
	if (m_hViewer != NULL)
	{
	    if (aEnable)
            m_nShareOption |= ViewFullScreen;
        else
            m_nShareOption &= ~ViewFullScreen;

		::PostMessage(m_hViewer, msgEnableFullScreen, 0, (LPARAM)(BOOL)aEnable);
		
        if (m_Callback)
            m_Callback(m_CallbackData, aEnable ? CB_DisplayFullScreen : CB_DisplayNormal);
	}
}

void VncViewer::EnableDebugMode(bool aEnable)
{
	if (m_hViewer != NULL)
	{
		::PostMessage(m_hViewer, msgEnableDebugMode, (WPARAM)(BOOL)aEnable, (LPARAM)(BOOL)aEnable);
	}
}

void VncViewer::Refresh()
{
    debugf("Share viewer refresh");
	//LaunchViewer(m_hParent, m_bEnableControl, true, m_nShareOption);
	KillProcessByName(VNC_CLIENT_NAME);
}

int VncViewer::HandleEvent(int id)
{
    if (!ViewerWanted)
        return 0;

    switch(id)
    {
        case CB_Disconnected: 
            debugf("CB_Disconnected");
            if (ViewerWanted) 
            {
                MsgWait(NULL, 1000, 5);
                LaunchViewer(m_hParent, m_bEnableControl, true, m_nShareOption);
                return 1;
            }
            break;
    }
    return 0;
}

LRESULT VncViewer::OnClientCreated(WPARAM wParam, LPARAM lParam)
{
    debugf("Share viewer window handle received [%X]", lParam);
    
	if (m_Callback)
        m_Callback(m_CallbackData, CB_Connected);

	m_hViewer = (HWND)lParam;

	m_nErrorCount = 0;
	m_bNetworkError = false;
	m_bConnected = true;
	m_bHasControl = FALSE;

	RECT rect;
	GetClientRect(m_hParent, &rect);

	// Move viewer window to match
	MoveViewer(0, 0, rect.right-rect.left, rect.bottom-rect.top);

	return 0;
}

LRESULT VncViewer::OnClientDestroyed(WPARAM wParam, LPARAM lParam)
{
    debugf("Share viewer client destroyed");
	return 0;
}

LRESULT VncViewer::OnConnectError(WPARAM wParam, LPARAM lParam)
{
    debugf("Share viewer connect error");
    
	// N.B. We won't have a viewer window yet if this error occurs as it
	// is only created if the connection succeeds.

	// Close failed viewer session.
	ShutdownViewer(true);

	if (m_Callback)
        m_Callback(m_CallbackData, CB_Connecting);

	MsgWait(NULL, CONNECTING_INTERVAL, 5);

	LaunchViewer(m_hParent, m_bEnableControl, true, m_nShareOption);

	return 0;
}

LRESULT VncViewer::OnNetworkError(WPARAM wParam, LPARAM lParam)
{
    debugf("Share viewer network error");

	m_bHasControl = FALSE;

    // Network error is handled within the vncviwer process (it does not exit) so
    // we don't need to do anything but report it.

	if (m_hViewer != NULL)
	{
		m_bNetworkError = true;

        if (m_Callback)
            m_Callback(m_CallbackData, CB_Connecting);
    }

	return 0;
}

LRESULT VncViewer::OnObtainedControl(WPARAM wParam, LPARAM lParam)
{
	m_bHasControl = (BOOL)lParam;
    debugf("Share view %s control", m_bHasControl ? "has" : "lost");
	return 0;
}

LRESULT VncViewer::OnCloseSession(WPARAM wParam, LPARAM lParam)
{
	return 0;
}

LRESULT VncViewer::OnViewerInternalError(WPARAM wParam, LPARAM lParam)
{
	return 0;
}


//*****************************************************************************
DWORD WINAPI VncViewer::StartThread( LPVOID cmdLine )
{
    VncviewerParams     *params;
    params = (VncviewerParams *) cmdLine;

    debugf("Launching %s", params->cmdLine);

    if (g_Viewer->m_Callback)
        g_Viewer->m_Callback(g_Viewer->m_CallbackData, CB_Connecting);

    spawnl( _P_WAIT, params->vnc, VNC_CLIENT_NAME, params->cmdLine, NULL );

    debugf("Share viewer process ended");

    if (g_Viewer->m_Callback)
        g_Viewer->m_Callback(g_Viewer->m_CallbackData, CB_Disconnected);

    return 0;
}
