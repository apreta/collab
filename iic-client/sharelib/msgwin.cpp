/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include <windows.h>
#include <tchar.h>
#include "vncserver.h"

extern HINSTANCE g_hinstance;
static bool g_InitMsgWin = false;
static VncServer *g_VncServer = NULL;
static volatile HWND g_hMsgWnd = NULL;

// vnc server messages
const UINT msgServerCallback                = RegisterWindowMessage(_T("IICServer.Callback"));


static LRESULT CALLBACK MainWndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
        case WM_CREATE:
            // Initialize the window.
            return 0;

        case WM_PAINT:
            // Paint the window's client area.
            return 0;

        case WM_SIZE:
            // Set the size and position of the window.
            return 0;

        case WM_DESTROY:
            // Clean up window-specific data objects.
            return 0;

        default:
            if (uMsg == msgServerCallback)
            {
                g_VncServer->OnCallbackMsg(wParam);
            }
            else
                return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }
    return 0;
}

static BOOL InitMsgWindow()
{
    WNDCLASSEX wcx;

    // Fill in the window class structure with parameters
    // that describe the main window.

    wcx.cbSize = sizeof(wcx);          // size of structure
    wcx.style = CS_HREDRAW |
        CS_VREDRAW;                    // redraw if size changes
    wcx.lpfnWndProc = MainWndProc;     // points to window procedure
    wcx.cbClsExtra = 0;                // no extra class memory
    wcx.cbWndExtra = 0;                // no extra window memory
    wcx.hInstance = g_hinstance;       // handle to instance
    wcx.hIcon = NULL;                  // predefined app. icon
    wcx.hCursor = NULL;                // predefined arrow
    wcx.hbrBackground = NULL;
    wcx.lpszMenuName =  NULL;          // name of menu resource
    wcx.lpszClassName = "MsgWinClass"; // name of window class
    wcx.hIconSm = NULL;

    // Register the window class.

    return RegisterClassEx(&wcx);
}

HWND CreateMsgWindowInternal()
{
	HWND hwnd = CreateWindow(
        "MsgWinClass",        // name of window class
        "Sample",            // title-bar string
        WS_OVERLAPPEDWINDOW, // top-level window
        CW_USEDEFAULT,       // default horizontal position
        CW_USEDEFAULT,       // default vertical position
        CW_USEDEFAULT,       // default width
        CW_USEDEFAULT,       // default height
        (HWND) NULL,         // no owner window
        (HMENU) NULL,        // use class menu
        g_hinstance,           // handle to application instance
        (LPVOID) NULL);      // no window-creation data

	return hwnd;
}

DWORD WINAPI ThreadFunction(LPVOID lpParam)
{
	g_hMsgWnd = CreateMsgWindowInternal();

	MSG msg;
	while(GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	
	return 0;
}

HWND CreateMsgWindow(VncServer *server)
{
    HWND hwnd;

	g_hMsgWnd = NULL;
    g_VncServer = server;

    if (!g_InitMsgWin)
    {
        InitMsgWindow();
        g_InitMsgWin = true;
    }

    // Create the main window.

#ifndef SHARE_DRIVER
    hwnd = CreateMsgWindowInternal();
#else
	// Create message window in background thread as main thread will be
	// blocked reading native messages from Chrome.
	// N.B.: Haven't tested this in share driver for npapi plugin yet.
	CreateThread(NULL, 0, ThreadFunction, NULL, 0, NULL); 
	while (g_hMsgWnd == NULL)
		Sleep(200);
	hwnd = g_hMsgWnd;
#endif

    return hwnd;
}
