/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#if !defined(AFX_PANEAPPSHARE_H__097D5695_6E3C_4954_B1A7_EE896458BB96__INCLUDED_)
#define AFX_PANEAPPSHARE_H__097D5695_6E3C_4954_B1A7_EE896458BB96__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "share_api.h"

#include <string>

#define MAX_EXEC_PATH 1024
#define MAXPWLEN 128

#define CONNECTING_INTERVAL	6000
#define VNC_CLIENT_NAME _T("mtgviewer.exe")


class VncViewer;


//*****************************************************************************
typedef struct _VncviewerParams
{
    VncViewer   *_this;
    TCHAR       vnc[MAX_EXEC_PATH];
    TCHAR       cmdLine[MAX_EXEC_PATH+MAX_EXEC_PATH];
}   VncviewerParams;


//*****************************************************************************
class VncViewer
{
    TCHAR * m_ExecPath;

    share_event_handler m_Callback;
    void *m_CallbackData;

    std::string m_ReflectorAddress;
    std::string m_ReflectorPassword;
    std::string m_EncryptKey;
    std::string m_MeetingId;
    std::string m_PartId;
    std::string m_Token;

    std::string m_ProxyUserName;
    std::string m_ProxyPassword;

    HWND m_hParent;
	HWND m_hViewer;
	bool m_bEnableControl;
	int  m_nShareOption;
	unsigned m_ShareStarted;
	int m_nErrorCount;
	bool m_bNetworkError;
	bool m_bConnected;
	bool m_bLaunching;

	int		    m_nVerticalOffset;
	BOOL		m_bShowToolbar;
	BOOL		m_bHasControl;
	int			m_nShareOptions;

	bool		m_bLockUpdate;

    VncviewerParams     m_viewerParams;

public:
	VncViewer();

    int Initialize(const TCHAR *aAddress, const TCHAR *aPassword, const TCHAR *aEncryptKey,
                   const TCHAR *aMeetingId, const TCHAR *aPartId, const TCHAR *aToken);
    void SetCallbackHandler(void *user, share_event_handler);

    void MoveViewer(int x, int y, int cx, int cy);
    void EraseBkgnd(int x, int y, int cx, int cy);

	int LaunchViewer(HWND hParent, bool aEnableControl, bool aRestart, int aOption);
	int ShutdownViewer(bool bWait = false);
	void SetPPTMode(int aShareOptions);
	void EnableRemoteControl(bool aEnable);
	void EnableFullScreen(bool aEnable);
	void Refresh();
    BOOL IsConnected() { return m_bConnected; }
	void EnableDebugMode(bool aEnable);
	int HandleEvent(int id);

	void LockUpdate(bool aLock) { m_bLockUpdate = aLock; }

	//}}AFX_VIRTUAL

// Implementation
	virtual ~VncViewer();

	LRESULT OnClientCreated(WPARAM wParam, LPARAM lParam);
	LRESULT OnClientDestroyed(WPARAM wParam, LPARAM lParam);
	LRESULT OnConnectError(WPARAM wParam, LPARAM lParam);
	LRESULT OnNetworkError(WPARAM wParam, LPARAM lParam);
	LRESULT OnObtainedControl(WPARAM wParam, LPARAM lParam);
	LRESULT OnCloseSession(WPARAM wParam, LPARAM lParam);
	LRESULT OnViewerInternalError(WPARAM wParam, LPARAM lParam);

private:
    HANDLE  m_hThread;

    static DWORD WINAPI StartThread( LPVOID cmdLine );
};



#endif // !defined(AFX_PANEAPPSHARE_H__097D5695_6E3C_4954_B1A7_EE896458BB96__INCLUDED_)
