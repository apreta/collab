/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include <windows.h>
#include <tchar.h>
#include <string.h>
#include <time.h>
#include <stdio.h>
#include <process.h>
#include <tlhelp32.h>

#ifdef WIN32
#include "vncserver.h"
#else
#include "unixserver.h"
#endif

#include "vncviewer.h"
#include "procutil.h"

BOOL KillProcessByName(const TCHAR* aName)
{
    HANDLE         hProcessSnap = NULL;
    BOOL           bRet      = FALSE;
    PROCESSENTRY32 pe32      = {0};

    //  Take a snapshot of all processes in the system.

    hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

    if (hProcessSnap == INVALID_HANDLE_VALUE)
        return (FALSE);

    //  Fill in the size of the structure before using it.

    pe32.dwSize = sizeof(PROCESSENTRY32);

    //  Walk the snapshot of the processes, and for each process,
    //  display information.

    if (Process32First(hProcessSnap, &pe32))
    {
		do
		{
			if (_tcsicmp(pe32.szExeFile, aName)==0)
			{
				HANDLE hProcess = OpenProcess (PROCESS_ALL_ACCESS,
					FALSE, pe32.th32ProcessID);

				// Get the process name.
				if (NULL != hProcess )
				{
					TerminateProcess(hProcess, (UINT)-1);

					CloseHandle( hProcess );
				}
			}

		} while (Process32Next(hProcessSnap, &pe32));

		bRet = TRUE;
    }
    else
        bRet = FALSE;    // could not walk the list of processes

    // Do not forget to clean up the snapshot object.

    CloseHandle (hProcessSnap);
    return (bRet);
}

BOOL IsAppRunning(const TCHAR* aName)
{
    HANDLE         hProcessSnap = NULL;
    PROCESSENTRY32 pe32      = {0};

	BOOL running = FALSE;

    //  Take a snapshot of all processes in the system.

    hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

    if (hProcessSnap == INVALID_HANDLE_VALUE)
        return (FALSE);

    //  Fill in the size of the structure before using it.

    pe32.dwSize = sizeof(PROCESSENTRY32);

    //  Walk the snapshot of the processes, and for each process,
    //  display information.

    if (Process32First(hProcessSnap, &pe32))
    {
		do
		{
			if (_tcsicmp(pe32.szExeFile, aName)==0)
			{
				running = TRUE;
				break;
			}

		} while (Process32Next(hProcessSnap, &pe32));

    }

    // Do not forget to clean up the snapshot object.

    CloseHandle (hProcessSnap);
    return (running);
}

//******************************************************
// 0	--- Window Me, 98, 95, 3.1
// 1	--- NT
// 2	--- 2000, XP

int GetOSVersion()
{
	DWORD dwVersion = GetVersion();

	if (dwVersion < 0x80000000) {
		// Get the Windows version.
		DWORD dwWindowsMajorVersion =  (DWORD)(LOBYTE(LOWORD(dwVersion)));
		DWORD dwWindowsMinorVersion =  (DWORD)(HIBYTE(LOWORD(dwVersion)));

		if (dwWindowsMajorVersion < 0x80)              // Windows NT/2000, Whistler
		{
			if (dwWindowsMinorVersion >= 5)
				return 2;
			return 1;
		}
	}
	return 0;
}

BOOL MsgWait(HWND hWnd, int aTimeout, int aCount)
{
    DWORD start = GetTickCount();

	while (aCount-- >= 0)
	{
	    int remaining = aTimeout - (int)(GetTickCount() - start);
	    if (remaining <= 0)
            return TRUE;

		DWORD res = MsgWaitForMultipleObjects(0, NULL, FALSE, remaining, QS_ALLEVENTS);
		if (res == WAIT_FAILED)
		{
			break;
		}

		if (res != WAIT_TIMEOUT)
		{
			MSG msg;
			while(PeekMessage(&msg, hWnd, 0, 0, PM_REMOVE))
			{
				if (msg.message == WM_QUIT)
				{
					PostQuitMessage(0);
					return FALSE;
				}
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
	}

	return TRUE;
}


void KillClientProcess( )
{
    KillProcessByName( VNC_CLIENT_NAME );
}


void KillServerProcess( )
{
    KillProcessByName( VNC_SERVER_NAME );
}


void KillSofficeProcess( )
{
    KillProcessByName( SOFFICE_PROCESS_NAME );
}


void KillNamedProcess( const char *pName )
{
    KillProcessByName( pName );
}
