/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <stdlib.h>
#include <wx/gtk/win_gtk.h>
#include <string.h>

#include "unixviewer.h"
#include "util_linux.h"
#include "../netlib/IICvncMsg.h"
#include <log.h>

#include <glib.h>
#include <glib/gstdio.h>

extern void StartMessageServer();
extern int  FindWindow(IIC_id id, void *b);

#define MENU_CLASS_NAME IIC_vncViewer

extern UnixViewer *g_Viewer;

std::string UnixViewer::s_command;

volatile bool UnixViewer::s_ViewerWanted = false;
volatile bool UnixViewer::s_VncThreadRunning = false;         // outside of UnixServer class so RunVnc() can access
// RunVnc loops because when it quit after return from system() call then successive pthread_create() calls would lead
//  to whole program going away.
// The reason for having the system() call that launches viewer done by a separate thread is to have it wait for return,
//  so we know if it quits early and when it's done.
volatile bool UnixViewer::s_RunVncThread = false;          // tell thread fn to do another command
volatile bool UnixViewer::s_QuitVncThread = false;         // to tell thread fn to quit


void *UnixViewer::RunVnc(void *p)
{
    while(!s_QuitVncThread)
    {
        s_VncThreadRunning = true;
        debugf("RunVnc viewer executing: %s\n", s_command.c_str());

        g_Viewer->m_ViewerStatus = Started;
        if (g_Viewer->m_Callback)
            g_Viewer->m_Callback(g_Viewer->m_CallbackData, CB_Connecting);

        system(s_command.c_str());

        s_RunVncThread = false;
        
        g_Viewer->m_ViewerStatus = NotRunning;
        if (g_Viewer->m_Callback)
            g_Viewer->m_Callback(g_Viewer->m_CallbackData, CB_SessionEnded);

        debugf("\tRunVnc viewer returning\n");
        while(!s_RunVncThread)
            sleep_ms(100);
    }
    s_VncThreadRunning = false;
    return 0;
}

#if 0
static int
socket_realized(GtkWidget *w, gpointer data)
{
	printf("Realized\n");
//	gtk_socket_steal(GTK_SOCKET((GtkSocket*)data), xwin_id);
	return FALSE;
}

static void
size_request(GtkWidget      *widget,
                            GtkRequisition *req,
                            gpointer        user_data)
{
    req->width = 400;
    req->height = 400;
}
#endif

//#define LOGXWININFO
#define GXW(w)  GDK_WINDOW_XID(w->window)

#ifdef LOGXWININFO
void LogXwininfo(int id, char *label)
{
    char temp[128];
    sprintf(temp," 0x%x ",id);
    std::string s = "echo ";
    s = s + temp + label + " >>xwin.txt";
    system(s.c_str());
    s = "xwininfo -id ";
    s = s + temp + " >>xwin.txt; xwininfo -id " + temp + " -tree >>xwin.txt" ;
    system(s.c_str());
}
#endif

UnixViewer::UnixViewer(const char *aExecPath)
{
    m_ExecPath = aExecPath;
    m_Callback = NULL;
    m_CallbackData = NULL;
    m_pizza = NULL;
    m_scrolled_win = NULL;
    m_viewer = NULL;
    m_frame = NULL;
    m_FullScreen = false;
    m_hParent = NULL;
    m_ViewerStatus = NotRunning;
    m_bRelaunch = false;
    m_fEnableSecurity = false;
}

int UnixViewer::Initialize(const char *aAddress, const char *aPassword, const char *aEncryptKey, const char *aMeetingId, const char *aPartId, const char *aToken, const bool fEnableSecurity)
{
    m_ReflectorAddress = aAddress;
    m_ReflectorPassword = aPassword;
    m_MeetingId = aMeetingId;
    m_PartId = aPartId;
    m_Token = aToken;
    m_fEnableSecurity = fEnableSecurity;
    if (aEncryptKey)
        m_EncryptKey = aEncryptKey;
    StartMessageServer();
    return 0;
}

void UnixViewer::SetCallbackHandler(void *user, share_event_handler handler)
{
    m_Callback = handler;
    m_CallbackData = user;
}

#if 0
static int
ignore_x_error(Display* d, XErrorEvent* e)
{
	fprintf(stderr, "ignore_x_error called\n");
	return 0;
}

static int
get_window_id(Window win, char *title, Window *wid)
{
	Window root_return;
	Window parent_return;
	Window *children;
	unsigned int nchildren;
	unsigned int i;
	char *tit;
	int ret = FALSE;
	int titlelen = strlen(title);
	int (*oldErrorHandler)(Display*, XErrorEvent*);

	XQueryTree(GDK_DISPLAY(),
		   win,
		   &root_return,
		   &parent_return,
		   &children,
		   &nchildren);

	oldErrorHandler = XSetErrorHandler(ignore_x_error);
	for(i=0;i<nchildren;i++) {
		if (!XFetchName(GDK_DISPLAY(),
				children[i],
				&tit)) {
			continue;
		}
		if(tit) {
			if(strncmp(tit,title,titlelen)==0) {
				XFree(tit);
				*wid = children[i];
				ret = TRUE;
				break;
			}
			XFree(tit);
		}
	}
	XSetErrorHandler(oldErrorHandler);
	for(i=0;!ret && i<nchildren;i++)
		ret=get_window_id(children[i],title,wid);
	if(children)
		XFree(children);
	return ret;
}
#endif

int UnixViewer::DoFullScreen()
{
    GtkWidget* sock;

    if (m_frame != NULL)
    {
        gtk_widget_destroy(m_frame);
        m_frame = NULL;
    }
    
    m_frame = gtk_window_new(GTK_WINDOW_TOPLEVEL);  // _POPUP
    gtk_window_set_title(GTK_WINDOW(m_frame), "Meeting Viewer");

    sock = gtk_socket_new ();
    gtk_widget_set_usize (sock, 1600, 1200);  // need real size from viewer
    gtk_container_add(GTK_CONTAINER(m_frame), sock);
    gtk_widget_realize(sock);
    gtk_socket_steal(GTK_SOCKET(sock), m_xwinid);

    gtk_window_fullscreen(GTK_WINDOW(m_frame));
    gtk_widget_show_all(m_frame);    
    
    return 0;
}

int UnixViewer::DoEmbedded()
{
    GtkWidget* parent = (GtkWidget*)m_hParent;
    GtkWidget* sock;
    GtkBin *cnt = GTK_BIN (parent);
    GtkRequisition req;
    gint width, height;

    RemoveWindow();

    // Our parent is a GtkScrolledWindow; it's child is a GtkPizza.  We'll insert our windows as
    // a child of that.
    m_pizza = gtk_bin_get_child(cnt);

    gtk_widget_size_request (parent, &req);
    width = req.width;
    height = req.height;

    m_scrolled_win = gtk_scrolled_window_new (NULL, NULL);
    gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(m_scrolled_win),
                        GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

    gtk_pizza_put (GTK_PIZZA(m_pizza), m_scrolled_win, 0, 0, width, height);

    sock = gtk_socket_new ();
    gtk_widget_set_usize (sock, 1600, 1200);  // need real size from viewer

    gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (m_scrolled_win), sock);

    gtk_viewport_set_shadow_type (GTK_VIEWPORT (GTK_BIN(m_scrolled_win)->child), GTK_SHADOW_NONE);
    gtk_widget_set_usize (GTK_BIN(m_scrolled_win)->child, width, height);

    gtk_widget_realize(sock);

    gtk_socket_steal (GTK_SOCKET (sock), m_xwinid);

    gtk_widget_show_all (m_pizza);

    m_viewer = sock;

    debugf("WINDOWS parent %x, win %x, scrolled_win %x, sock %x\n",
        GXW(parent),GXW(m_pizza),GXW(m_scrolled_win),GXW(sock));

    return 0;
}

int UnixViewer::ReparentWindow()
{
    if (m_FullScreen)
    {
        DoFullScreen();
    }
    else
    {
        DoEmbedded();

        if (m_frame != NULL)
        {
            gtk_widget_destroy(m_frame);
            m_frame = NULL;
        }
    }

    return 0;
}

void UnixViewer::RemoveWindow()
{
    GtkWidget* parent = (GtkWidget*)m_hParent;
    GtkBin *cnt = GTK_BIN (parent);
    GtkWidget *pizza = gtk_bin_get_child(cnt);

	GList *children = gtk_container_get_children(GTK_CONTAINER(pizza));

	GList *next = g_list_first(children);
	while (next)
	{
		gtk_container_remove(GTK_CONTAINER(pizza), (GtkWidget*)next->data);
		next = g_list_next(next);
	}

	g_list_free(children);

	m_scrolled_win = 0;
	m_viewer = 0;
}

int UnixViewer::OnClientCreated(WPARAM wParam, LPARAM lParam)
{
    debugf("OnClientCreated wParam %d, lParam %ld\n",wParam,lParam);  //wParam is is viewer window id
    m_xwinid = wParam;
#ifdef LOGXWININFO
    GtkWidget *parent = (GtkWidget*)m_hParent;
    LogXwininfo(GXW(parent),"ClientConnected");
#endif
	if (m_Callback)
        m_Callback(m_CallbackData, CB_Connected);
    return 0;
}

int UnixViewer::OnClientDestroyed(WPARAM wParam, LPARAM lParam)
{
    if (m_Callback)
        m_Callback(m_CallbackData, CB_Disconnected);
    return 0;
}

int UnixViewer::HandleEvent(int id)
{
    if (!s_ViewerWanted)
        return 0;

    switch(id)
    {
        case CB_Connecting:
            debugf("CB_Connecting\n");
            break;

        case CB_Connected:
            debugf("CB_Connected\n");
            sleep_ms(100);
            ReparentWindow();       // Has to be done from main thread
            break;

        case CB_Disconnected:       // destroy old windows as soon as viewer is gone, don't relaunch yet
            debugf("CB_Disconnected\n");
            break;

        case CB_SessionEnded:       // This happens only, or after Disconnected.
            debugf("CB_SessionEnded\n");
            if (s_ViewerWanted)
            {
                sleep_ms(1000);
                LaunchViewer(m_hParent, m_bEnableControl, false, false);
                return 1;
            }
            break;

        default:
            debugf("Viewer::HandleEvent unknown id %d\n",id);
            break;
    }
    return 0;
}


// The number of log files to keep before deleting the oldest
static unsigned history_len = 10;

// If the current log is >= kMaxLogFileSize, rotate the current log
// into the stack of historical files and create a new log file.
static int kMaxLogFileSize = 100 * 1024;

# define MAX_LOG_FILENAME    (256)

void UnixViewer::log_rotate_if_needed( char *logFN, bool fAlwaysRotate )
{
    char newerfn[MAX_LOG_FILENAME];
    char olderfn[MAX_LOG_FILENAME];
    int i;
    long    sz      = -1;

    struct stat myStats;
    i = stat( logFN, &myStats );
    if( i  ==  0 )
        sz = myStats.st_size;

    if( (sz < kMaxLogFileSize)  &&  !fAlwaysRotate )
        return;

    sprintf( newerfn, "%s.%u", logFN, history_len );
    unlink( newerfn );
    for( i = history_len-1; i > 0; i--)
    {
        strcpy( olderfn, newerfn );
        sprintf( newerfn, "%s.%u", logFN, i );
        if( rename(newerfn, olderfn)  ==  0)
        {
//            errNo = GetLastError( );
        }
    }
    strcpy(olderfn, newerfn);
    strcpy(newerfn, logFN);
    if (rename(newerfn, olderfn)  ==  0)
    {
//        errNo = GetLastError( );
    }
}


int UnixViewer::LaunchViewer(void * hParent, bool aEnableControl, bool aRestart, int aOption)
{
    m_hParent = hParent;
    m_bEnableControl = aEnableControl;

    char    logFN[ MAX_LOG_FILENAME ];
    logFN[ 0 ] = 0;
    strcat( logFN, g_get_home_dir( ) );
    strcat( logFN, "/.meeting/mtgview.log" );
    log_rotate_if_needed( logFN, false );

    std::string path = m_ExecPath + "/mtgviewer";
    m_command = path + " -embed -passwd " + m_ReflectorPassword + " " + m_ReflectorAddress + " " + m_MeetingId +
                " " + m_PartId + " " + m_Token;

    if( m_fEnableSecurity )
        m_command = m_command + " yes";
    else
        m_command = m_command + " no";

    if(!m_bEnableControl)
        m_command = m_command + " -viewonly" ;

    m_xwinid = 0; //xwinid will be returned from viewer in its CB_Connected msg.

    m_command = m_command + " >>~/.meeting/mtgview.log 2>&1"; //
    s_command = m_command;  // save global copy for other thread

    s_ViewerWanted = true;
    sleep_ms(500); // we appear to get the share started event a bit before server is ready to accept listeners

    if(!s_VncThreadRunning)
    {
        pthread_t vnc_thread;
        int ThreadRet = pthread_create(&vnc_thread, NULL, RunVnc, NULL);
        if(ThreadRet == -1)  // error
        {
            debugf("ERROR creating vnc thread\n");
        }
    }
    else
        s_RunVncThread = true;

    return 0;
}


int UnixViewer::ReLaunchViewer(void * hParent, bool aEnableControl, bool aRestart, int aOption)
{
    return 0;
}


int UnixViewer::ShutdownViewer(bool bWait)
{
    s_ViewerWanted = false;
    debugf("Shutting down viewer\n");

    //RemoveWindow();
    m_hParent = NULL;

    if (m_frame)
    {
        gtk_widget_destroy(m_frame);
        m_frame = NULL;
	}

	HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
	if (vncMenu != 0)
	{
		UINT cmd = WM_CLOSE;

		::PostMessage(vncMenu, cmd, 0, 0L);
		sleep_ms(100);

		if (bWait)
		{
			int retry = 20;
            while(::FindWindow(MENU_CLASS_NAME,NULL))
			{
				if (--retry)
					sleep_ms(100);
				else
				{
				    debugf("Viewer didn't exit cleanly, killing");
		            system("pgrep mtgviewer | xargs kill");
					return SHARE_REBOOT;
				}
			}
		}
	}

    return 0;
}

void UnixViewer::MoveViewer(int x, int y, int width, int height)
{
    if (m_pizza != NULL)
        gtk_pizza_set_size( GTK_PIZZA(m_pizza), m_scrolled_win, x, y, width, height);
}

void UnixViewer::EnableRemoteControl(bool aEnable)
{
	if (m_xwinid != 0)
	{
		m_bEnableControl = aEnable;

		// N.B. previously, enable flag is actually implemented in vnc viewer as a "view only" flag.
		//  Now we send enable flag.
		debugf("%s app share session control\n", aEnable ? ("Enabling") : ("Disabling"));
		PostMessage(IIC_vncViewer, msgEnableRemoteControl, aEnable, 0);
	}
}

void UnixViewer::EnableFullScreen(bool bEnable)
{
    if (m_FullScreen != bEnable)
    {
        m_FullScreen = bEnable;

        if (bEnable)
        {
            DoFullScreen();
        }
        else
        {
            if (m_viewer)
            {
                // Use existing socket if we have it; otherwise viewer is shutdown when
                // we destroy old socket and create a new one
                gtk_socket_steal(GTK_SOCKET(m_viewer), m_xwinid);
            }
            else
            {
                DoEmbedded();
            }
            if (m_frame != NULL)
            {
                gtk_widget_destroy(m_frame);
                m_frame = NULL;
            }
        }
    }
}

void UnixViewer::OnGetEncryptKey( )
{
    //  Send EncryptKey
    HWND    vncMenu     = 0;
    int     iNumSleeps  = 0;
    while( true )
    {
        vncMenu = ::FindWindow( MENU_CLASS_NAME, NULL );
        if( vncMenu )
            break;
        if( iNumSleeps  >  1000 )
            break;
        sleep_ms( 10 );
        iNumSleeps++;
    }

    if( vncMenu  !=  0 )
    {
        struct EncryptKey { char key[256]; };
        EncryptKey  eKey;
        if (m_fEnableSecurity)
        {
            strcpy( eKey.key, m_EncryptKey.c_str( ) );
        }
        else
        {
            memset( eKey.key, 0, sizeof( eKey.key ) );
        }
        COPYDATASTRUCT command3 = {CDC_GetEncryptKey, sizeof( eKey ), &eKey };
        SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command3);
        debugf("Sent encryption key\n");
    }
}
