/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include <windows.h>

#include <string>
#include "AppList.h"
#include "share_api.h"
#include <log.h>

//  AppList object
AppList::AppList(const char *_display)
{
	windows = 0;
	display = strdup(_display);
}

AppList::~AppList()
{
    // delete strings for windows entries
	if(windows > 0)
    for(int i=0; i<=windows; i++)
    {	// be safe about it, even though there's no way the pointers should be null for <windows> entries
        if(xwinEntry[i].id)
			delete xwinEntry[i].id;
        if(xwinEntry[i].name)
			delete xwinEntry[i].name;
    }
    windows = 0;
    free(display);
}

BOOL IsChildWindow(HWND hWnd)
{
	return GetAncestor(hWnd, GA_ROOTOWNER)!=hWnd;
}

#if 0

// ToDo: Add icons to app list

typedef DWORD (WINAPI *PGMFN)( HANDLE, HMODULE, LPTSTR, DWORD);
PGMFN pGetModuleFileNameEx = NULL;

HICON GetWindowIcon(HWND hWnd, BOOL& bNeedDestroy)
{
	bNeedDestroy = FALSE;

	HICON hIcon = (HICON)::SendMessage(hWnd, WM_GETICON, ICON_BIG, 0);
	if (hIcon != NULL)
		return hIcon;

	if (pGetModuleFileNameEx == NULL)
	{
		HMODULE hDLL = LoadLibraryA("psapi");
		pGetModuleFileNameEx =
			(PGMFN) GetProcAddress(hDLL, "GetModuleFileNameExA");
	}

	TCHAR       n[512];
	HINSTANCE   hIns = (HINSTANCE)GetWindowLong (hWnd, GWL_HINSTANCE);
	DWORD processId;
	GetWindowThreadProcessId (hWnd, &processId);
	HANDLE process = OpenProcess (PROCESS_ALL_ACCESS, FALSE, processId);
	if (process == NULL) return NULL;
	if (pGetModuleFileNameEx != NULL)
	{
		if (pGetModuleFileNameEx(process, hIns, n, 512) == 0)
		{
			UINT err = GetLastError();

			CloseHandle(process);
			return NULL;
		}
	}
	CloseHandle(process);

	ExtractIconEx((LPCTSTR)n, 0, &hIcon, NULL, 1);

	bNeedDestroy = TRUE;

	return hIcon;
}

#endif

void AppList::FindNamedIdsWith(char *Property, char *Value)
{
    MonitorInfo * info = share_get_display(display);
    RECT dispRect;
    SetRect(&dispRect, info->tx, info->ty, info->bx, info->by);
    
	HWND hWnd = GetWindow(GetForegroundWindow(), GW_HWNDLAST);

	while (hWnd != NULL)
	{
		if (IsWindowVisible(hWnd) && IsChildWindow(hWnd)==FALSE)
		{
		    RECT rect, intersect;
            wchar_t title[1024] = L"";
            WINDOWPLACEMENT wp = { sizeof(WINDOWPLACEMENT) };

            // Use normal position to figure out which display app is on--if it's really maximized it will still
            // be on the same screen, and if it's minimized the share server will restore it.
            GetWindowRect(hWnd, &rect);
            GetWindowPlacement(hWnd, &wp);
            rect = wp.rcNormalPosition;

            if (IntersectRect(&intersect, &dispRect, &rect) && 
                intersect.bottom - intersect.top > 9 &&
                intersect.right - intersect.left > 9)
            {
                wchar_t wclass[512];

                GetClassNameW(hWnd, wclass, 512);
                
                GetWindowTextW(hWnd, title, sizeof(title));
                if (*title && wcscmp(wclass, L"Progman"))
                {
                    DWORD procID;
                    GetWindowThreadProcessId(hWnd, &procID);
                    char buffer[20];
                    itoa( procID, buffer, 10 );

                    //debugf("%S - %d,%d - %d,%d", title, intersect.left, intersect.top, intersect.right, intersect.bottom);

                    xwinEntry[windows].name = new wstring(title);
                    xwinEntry[windows].id = new string(buffer);
                    xwinEntry[windows].Matches = true;

                    if (++windows >= MAX_WINS)
                        break;
                }
            }
		}

		hWnd = GetNextWindow(hWnd, GW_HWNDPREV);
	}
}

