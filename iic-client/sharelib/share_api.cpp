/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include "share_api.h"

#include <windows.h>
#include <tchar.h>

#include "msgwin.h"
#include "viewerwin.h"
#include "vncserver.h"
#include "vncviewer.h"
#include <log.h>


bool g_debug = false;
bool g_fullScreen = false;
bool g_remoteControl = false;
HWND g_parentWindow = NULL;
HWND g_serverListener = NULL;
HWND g_viewerListener = NULL;
VncServer *g_Server = NULL;
VncViewer *g_Viewer = NULL;
std::list<MonitorInfo> g_monitors;

static void get_monitor_info();


void share_initialize(const char *exec_path, const char *meeting_id, const char *share_server, const char *password, const char *encrypt_key, const char *part_id,
                      const char *sessionToken, bool fEnableSecurity, int start_time)
{
#ifdef SHARE_DRIVER
	// TODO: log init
#endif

    if (g_Server == NULL)
    {
        g_Server = new VncServer(exec_path);
    }
    g_Server->SetReflectorAddress(share_server);
    g_Server->SetReflectorPassword(password);
    g_Server->SetEncryptKey(encrypt_key);
    g_Server->SetEnableSecurity(fEnableSecurity);
    g_Server->SetMeetingId(meeting_id);
    g_Server->SetParticipantId(part_id);
    g_Server->SetToken(sessionToken);
    g_Server->SetStartTime(start_time);
    g_Server->SetAgentMode(false);
    g_Server->SetCallbackHandler(NULL,NULL);
    g_Server->SetPPTHWND(NULL,NULL);
    g_Server->EnableTransparent(true);
    g_Server->SetDisplayDevice(NULL);
    if (g_Viewer == NULL)
    {
        g_Viewer = new VncViewer();
    }
    g_Viewer->Initialize(share_server, password, encrypt_key, meeting_id, part_id, sessionToken);
}

void share_set_event_handler(void *user, share_event_handler _handler)
{
    if (g_Server) g_Server->SetCallbackHandler(user, _handler);
    if (g_Viewer) g_Viewer->SetCallbackHandler(user, _handler);
}

int share_enable_recording(bool enable)
{
    if (g_Server)
        return g_Server->EnableRecording(enable ? TRUE : FALSE);
    return 0;
}

int share_set_options(const char *option, const char *value)
{
    if (strcmp(option, "transparent") == 0) {
        bool enable = stricmp(value, "enable") == 0;
        if (g_Server)
            return g_Server->EnableTransparent(enable);
    }
    if (strcmp(option, "directx") == 0) {
        bool enable = stricmp(value, "enable") == 0;
        if (g_Server)
            return g_Server->EnableDirectX(enable);
    }
    else if (strcmp(option, "display") == 0) {
        if (g_Server)
            g_Server->SetDisplayDevice(value);
    }
    return -1;
}

int share_refresh()
{
    if (g_viewerListener)
        g_Viewer->Refresh();
    if (g_Server)
        g_Server->Refresh();

    return 0;
}

std::list<MonitorInfo> share_get_displays()
{
    get_monitor_info();
    return g_monitors;
}

MonitorInfo* share_get_display(const char * display_name)
{
    if (g_monitors.size() == 0)
        get_monitor_info();
        
    if (display_name == NULL || *display_name == 0)
    {
        return &(*g_monitors.begin());
    }
    
    std::list<MonitorInfo>::iterator iter;
    for (iter = g_monitors.begin(); iter != g_monitors.end(); iter++)
    {
        MonitorInfo* info = &(*iter);
        if (info->deviceName == display_name)
            return info;
    }
    return NULL;
}

int share_get_number_displays() 
{
	get_monitor_info();
	return g_monitors.size();
}

MonitorInfo* share_get_display_at(int index)
{
    if (g_monitors.size() == 0)
        get_monitor_info();

    std::list<MonitorInfo>::iterator iter;
    for (iter = g_monitors.begin(); iter != g_monitors.end(); iter++)
    {
        MonitorInfo* info = &(*iter);
        if (index-- == 0)
            return info;
    }
    return NULL;
}

int share_desktop()
{
    if (g_serverListener == NULL)
        g_serverListener = CreateMsgWindow(g_Server);
    return g_Server->LaunchServer(g_serverListener, NULL, FALSE, FALSE, FALSE);
}

int share_applications(std::string &application, BOOL share_all_instances)
{
    std::string apps = application + ",";
    if (g_serverListener == NULL)
        g_serverListener = CreateMsgWindow(g_Server);
    return g_Server->LaunchServer(g_serverListener, apps.c_str(), FALSE, share_all_instances, FALSE);
}

int share_presentation(void * present_window, void * parent_window, bool agent_mode)
{
    if (g_serverListener == NULL)
        g_serverListener = CreateMsgWindow(g_Server);

    g_Server->SetPPTHWND((HWND)present_window, (HWND)parent_window);
    g_Server->SetAgentMode(agent_mode);

    int res = g_Server->LaunchServer(g_serverListener, NULL, TRUE, FALSE, FALSE);

    return res;
}

int share_end()
{
    if (g_Server)
        return g_Server->ShutdownServer();
    return 0;
}

int share_get_start_time()
{
    return g_Server->GetStartTime();
}

int share_view(void * hParentWnd)
{
    g_fullScreen = false;

    // We create a listener window to insert between parent (wxWidgets) window and the
    // VNC viewer window
    g_parentWindow = (HWND)hParentWnd;
    g_viewerListener = CreateViewerWindow((HWND)hParentWnd, g_Viewer);
    return g_Viewer->LaunchViewer(g_viewerListener, g_remoteControl, FALSE, 0);
}

void share_enable_fullscreen(bool enable)
{
    g_fullScreen = enable;
    if (enable)
    {
        /* remove viewer window from frame and enlarge to screen */
        FullScreenViewerWindow(g_viewerListener);
    }
    else
    {
        /* put viewer back into meeting frame */
        NormalViewerWindow(g_viewerListener, g_parentWindow, true);
    }
    g_Viewer->EnableFullScreen(enable);
}


void share_enable_control(bool enable)
{
    g_remoteControl = enable;
    if (g_viewerListener)
        g_Viewer->EnableRemoteControl(enable);
}

bool share_is_control_enabled()
{
    return g_remoteControl;
}

void share_move_viewer(int x, int y, int width, int height)
{
    if (!g_fullScreen && g_viewerListener != NULL)
    {
        MoveWindow(g_viewerListener, x, y, width, height, TRUE);
    }
}

void share_erase_background(int x, int y, int width, int height)
{
    g_Viewer->EraseBkgnd(x, y, width, height);
}

void share_get_buffer(void*& buffer, int& width, int& height)
{
    buffer = NULL;
    width = 0;
    height = 0;
}

void share_view_end()
{
    if (g_Viewer)
        g_Viewer->ShutdownViewer(false);

    if (g_viewerListener)
    {
        DestroyWindow(g_viewerListener);
        g_viewerListener = NULL;
    }
}

int share_handle_event(int code)
{
    static bool switched = false;
    switch (code) 
    {
        case CB_Connected: 
            if (g_fullScreen)
                FullScreenViewerWindow(g_viewerListener);
            switched = false;
            break;
        
        case CB_Disconnected:
            if (g_fullScreen && !switched) 
            {
                NormalViewerWindow(g_viewerListener, g_parentWindow, false);
                switched = true;
            }
            break;
    }
    if (g_Viewer)
        return g_Viewer->HandleEvent(code);
   return 0;
}

void share_request_control()
{
}

void share_remote_mouse_move(int x, int y)
{
}

void share_remote_button(int button, bool pressed, int x, int y)
{
}

void share_remote_key(int key, bool pressed)
{
}


/*
 * Utility functions
 */

/* Display device "state" */
#define DISPLAY_DEVICE_ATTACHED_TO_DESKTOP 0x00000001
#define DISPLAY_DEVICE_MULTI_DRIVER        0x00000002
#define DISPLAY_DEVICE_PRIMARY_DEVICE      0x00000004
#define DISPLAY_DEVICE_MIRRORING_DRIVER    0x00000008

/* Child device "state" (a.k.a. monitors) */
#define DISPLAY_DEVICE_ACTIVE              0x00000001
#define DISPLAY_DEVICE_ATTACHED            0x00000002


static BOOL CALLBACK MonitorEnumProc(
    HMONITOR hMonitor,
    HDC hdcMonitor,
    LPRECT lprcMonitor,
    LPARAM dwData)
{
    MONITORINFOEX info;
    info.cbSize = sizeof(info);
    
    GetMonitorInfo(hMonitor, &info);

    debugf("Monitor: %s (%d %d %d %d)", info.szDevice, info.rcMonitor.top, info.rcMonitor.left, info.rcMonitor.bottom, info.rcMonitor.right);
    
    MonitorInfo mon = { info.szDevice, "", info.rcMonitor.left, info.rcMonitor.top, info.rcMonitor.right, info.rcMonitor.bottom };
    g_monitors.push_back(mon);
        
    return TRUE;
}

static void get_monitor_info(MonitorInfo * info)
{
    DISPLAY_DEVICE dd;
	ZeroMemory(&dd, sizeof(dd));
    dd.cb = sizeof(dd);

    int j = 0;
    while (EnumDisplayDevices(info->deviceName.c_str(), j, &dd, 0))
    {
        if (dd.StateFlags & DISPLAY_DEVICE_ATTACHED)
        {
            debugf("Monitor info: %s, %s, %s", dd.DeviceName, dd.DeviceString, dd.DeviceID);
            info->description = dd.DeviceString;
        }
        ++j;
    }
}
 
static void get_monitor_info()
{
    g_monitors.clear();
    
    EnumDisplayMonitors(NULL, NULL, MonitorEnumProc, NULL);
    
    for (std::list<MonitorInfo>::iterator iter = g_monitors.begin(); iter != g_monitors.end(); iter++)
    {
        get_monitor_info(&(*iter));
    }
}
