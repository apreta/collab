/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include <string.h>

#include "unixserver.h"
#include "util_linux.h"

#include "../netlib/IICvncMsg.h"
#include <log.h>
#include <pthread.h>

extern void StartMessageServer();
extern int  FindWindow(IIC_id id, void *b);

extern "C" {
#include "d3des.h"
}

#include <glib.h>
#include <glib/gstdio.h>


int EncryptPasswd(char *passwd, char *encryptedPasswd);

IIC_id MENU_CLASS_NAME = IIC_vncServer;

// VNC password encryption key
unsigned char fixedkey[8] = {23,82,107,6,35,78,88,7};

const UINT MENU_SETPASSWORD_MSG    = RegisterWindowMessageA("IICServer.Properties.SetPassword");
const UINT MENU_ADD_CLIENT_MSG     = RegisterWindowMessageA("IICServer.AddClient.Message");
const UINT MENU_REMOVE_CLIENTS_MSG = RegisterWindowMessageA("IICServer.RemoveClients.Message");

const UINT CALLBACK_MSG             = RegisterWindowMessageA("IICServer.Callback");

extern UnixServer *g_Server;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

UnixServer::UnixServer(/*const*/ char *aExecPath)
{
    m_ExecPath = aExecPath;
    m_ServerStatus = NotRunning;


    m_MeetingId = NULL;
    m_ReflectorAddress = ""; //NULL;
    m_ReflectorPassword = NULL;

    m_Callback = NULL;
    m_CallbackData = NULL;

    m_hMsgWin = 0;
	m_hWndPPT = 0;
	m_bEnableRecording = false;
	m_bLaunching = false;
	m_bAgentMode = false;
	m_bNagleAlgorithm = true;
	m_bEnableSecurity = false;
	m_bReducedColor = false;
	m_EncryptKey = 0;
	
	m_iStartTime = 0;
}

UnixServer::~UnixServer()
{
    if (m_MeetingId)
        free((void*)m_MeetingId);
    if (m_PartId)
        free((void*)m_PartId);
    if (m_Token)
        free((void*)m_Token);
    if (m_ReflectorPassword)
        free((void*)m_ReflectorPassword);
    debugf("UnixServer dtor\n");
}

int UnixServer::Initialize(const char *aAddress, const char *aPassword, const char *aEncryptKey, const char *aMeetingId, const char *aPartId, const char *aToken, const bool fEnableSecurity)
{
    StartMessageServer();
    std::string S(aAddress);    // have to replace :: with : for x11vnc server, viewer uses :: to specify port
    S.replace( S.find("::"), 2, ":");
    m_ReflectorAddress = strdup((char*)S.c_str());  // use strdup() ?  or would that lead to mem leak ?
    debugf("reflector: %s\n",m_ReflectorAddress);
    m_ReflectorPassword = strdup(aPassword);
    m_MeetingId = strdup(aMeetingId);
    m_PartId = strdup(aPartId);
    m_Token = strdup(aToken);
    debugf("m_MeetingId=%s\n",m_MeetingId);
    m_bEnableSecurity = fEnableSecurity;
    if (aEncryptKey)
        m_EncryptKey = strdup(aEncryptKey);

    return 0;
}

void UnixServer::SetCallbackHandler(void *user, share_event_handler handler)
{
    m_Callback = handler;
    m_CallbackData = user;
}

/*
const char *winvncShareApps   = "-shareapps";
const char *winvncSharePPT    = "-shareppt";
const char *winvncStartTime   = "-starttime";
const char *winvncRunAsUserApp= "-run";
const char *winvncRecording   = "-recording";
const char *winvncAllInstances= "-allinstances";
*/

std::string UnixServer::s_command;
volatile bool UnixServer::s_VncThreadRunning = false;         // outside of UnixServer class so RunVnc() can access
// RunVnc loops because when it quit after return from system() call then successive pthread_create() calls would lead
//  to whole program going away.
// The reason for having the system() call that launches x11vnc done by a separate thread is to have it wait for return,
//  so we know if it quits early and when it's done.
volatile bool UnixServer::s_RunVncThread = false;          // tell thread fn to do another command
volatile bool UnixServer::s_QuitVncThread = false;         // to tell thread fn to quit

// This routine could automatically launch server on exit but it shouldn't just be looped.
// "caller" will set RunVncThread when he wants server launched again.
void * UnixServer::RunVnc(void *p)
{
    s_VncThreadRunning = true;
    while(!s_QuitVncThread)
    {
        debugf("RunVnc Executing: %s\n", s_command.c_str());
        g_Server->m_ServerStatus = Started;
        system(s_command.c_str());
        g_Server->m_ServerStatus = NotRunning;
        debugf("\tRunVnc returning\n");
        // If we still want server, relaunch it
        if(g_Server->m_bServerWanted && g_Server->m_bRestart)
            s_RunVncThread = true;
        else
            s_RunVncThread = false;
        while(!s_RunVncThread)
            sleep_ms(100);
    }
    s_VncThreadRunning = false;
    return 0;
}

// The number of log files to keep before deleting the oldest
static unsigned history_len = 10;

// If the current log is >= kMaxLogFileSize, rotate the current log
// into the stack of historical files and create a new log file.
static int kMaxLogFileSize = 100 * 1024;

# define MAX_LOG_FILENAME    (256)

void UnixServer::log_rotate_if_needed( char *logFN, bool fAlwaysRotate )
{
    char newerfn[MAX_LOG_FILENAME];
    char olderfn[MAX_LOG_FILENAME];
    int i;
    long    sz      = -1;

    struct stat myStats;
    i = stat( logFN, &myStats );
    if( i  ==  0 )
        sz = myStats.st_size;

    if( (sz < kMaxLogFileSize)  &&  !fAlwaysRotate )
        return;

    sprintf( newerfn, "%s.%u", logFN, history_len );
    unlink( newerfn );
    for( i = history_len-1; i > 0; i--)
    {
        strcpy( olderfn, newerfn );
        sprintf( newerfn, "%s.%u", logFN, i );
        if( rename(newerfn, olderfn) )
        {
            debugf("Unable to rotate logfile %s\n", newerfn);
        }
    }
    strcpy(olderfn, newerfn);
    strcpy(newerfn, logFN);
    if (rename(newerfn, olderfn)  !=  0)
    {
        debugf("Unable to rotate logfile %s\n", newerfn);
    }
}

int UnixServer::LaunchServer(std::string &aAppName, bool aRestart)  // for now
{
    debugf("m_ServerStatus=%d\n", m_ServerStatus);
    if(m_ServerStatus != NotRunning)
    {
        // error, kill it
        ShutdownServer(true);       // kill it and wait for death cert, also make sure RunVnc not running.
    }
    // Save these for relaunch
    m_bServerWanted = true;
    m_aAppName = aAppName;
    m_bRestart = aRestart;

    char    logFN[ MAX_LOG_FILENAME ];
    logFN[ 0 ] = 0;
    strcat( logFN, g_get_home_dir( ) );
    strcat( logFN, "/.meeting/mtgshare.log" );
    log_rotate_if_needed( logFN, false );

    char buff[32];
    sprintf(buff, "%d", m_iStartTime);

    s_command = m_ExecPath + "/mtgshare";   //m_ExecPath is src/iic/clients/bin
    if(aAppName != "")
        s_command = s_command + " -winid " + aAppName;
    debugf("m_MeetingId=%s\n",m_MeetingId);

    s_command = s_command + " -nodragging -solid steelblue";

    if( m_bEnableSecurity )
        s_command = s_command + " -useEncryptkey yes";
    else
        s_command = s_command + " -useEncryptkey no";

    if (m_Token != NULL)
        s_command = s_command + " -token " + m_Token;

    if (m_PartId != NULL)
        s_command = s_command + " -partid " + m_PartId;

    s_command = s_command + " -connect_or_exit " + m_ReflectorAddress + " -meeting " + m_MeetingId +
              " -starttime " + buff + " -recording " + (m_bEnableRecording?"1":"0") + " >>~/.meeting/mtgshare.log 2>&1";

    // PROBLEM:  using thread, works 1st time, program quits 2nd time, or 3rd or 4th...
    debugf("VncThreadRunning %d\n",s_VncThreadRunning);     // should be 0
    if(!s_VncThreadRunning)
    {
        pthread_t vnc_thread;
        int ThreadRet = pthread_create(&vnc_thread, NULL, RunVnc, NULL);
        if(ThreadRet == -1)  // error
        {
            debugf("ERROR creating vnc thread\n");
        }
    }
    else
        s_RunVncThread = true;

    return 0;
}

int UnixServer::ShutdownServer(bool aWait, bool aRestart)
{
    m_bServerWanted = false;
    debugf("Shutting down server\n");

	HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
	if (vncMenu != 0)
	{
		UINT cmd = WM_CLOSE;

		::PostMessage(vncMenu, cmd, 0, 0L);
		sleep_ms(100);

		if (aWait)
		{
			int retry = 20;
            while(::FindWindow(MENU_CLASS_NAME,NULL))
			{
				if (--retry)
					sleep_ms(100);
				else
				{
				    debugf("Server didn't exit cleanly, killing");
		            system("pgrep mtgshare | xargs kill");
					return SHARE_REBOOT;
				}
			}
		}
	}

    // Either it wasn't running or it quit before we timed out.
    m_ServerStatus = NotRunning;
    debugf("\tserver shut down\n");

	return 0;
}

int UnixServer::ShareApplication(LPCTSTR aAppName, BOOL aShareAllApplicationInstances)
{
    // Use IIC_id IIC_vncServer
    IIC_id vncMenu = IIC_vncServer;
	COPYDATASTRUCT command0 = {CDC_AllApplicationInstances, aShareAllApplicationInstances, &aShareAllApplicationInstances };
	PostMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command0);

    COPYDATASTRUCT command = {CDC_ShareApplications, strlen(aAppName), (void*)aAppName };
	PostMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command);

	return 0;
}

int UnixServer::EnableTransparent(BOOL bEnable)
{
	//IIC_id vncMenu = IIC_vncServer; //
	HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
	if (vncMenu != 0)
	{
		COPYDATASTRUCT command = {CDC_EnableTransparent, bEnable, &bEnable };
		PostMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command);
	}

	return 0;
}

bool UnixServer::IsRunning()
{
    int pgrepReturn;
    pgrepReturn = system("pgrep mtgshare");
    if(pgrepReturn == 256)
    {
        m_ServerStatus = NotRunning;
        return false;
    }
    else
    {
        m_ServerStatus = Started;
        return true;
    }
}

HWND UnixServer::FindWindow(int aRetries)
{
#if 1
	HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
	while (vncMenu == 0 && aRetries--)
	{
		sleep_ms(500);
		vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
	}

	return vncMenu;
#else
    return IIC_vncServer;
#endif
}

void UnixServer::SetMeetingId(const char * id)
{
	m_MeetingId = strdup(id);
}

void UnixServer::SetReflectorAddress(const char * address)
{
	m_ReflectorAddress = strdup(address);
}

void UnixServer::SetReflectorPassword(const char * password)
{
	m_ReflectorPassword = strdup(password);
}

int UnixServer::ConnectReflector()
{
	HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
	if (vncMenu == 0)
		return -1;

	SendPassword(vncMenu, m_ReflectorPassword);

    char address[512];
	UINT port = VNC_REFLECTOR_PORT;

    strcpy(address, m_ReflectorAddress);
	char *port_str = strstr(address, "::");
	if (port_str != NULL)
	{
		port = atoi(port_str + 2);
		*port_str = '\0';
	}

	if (strlen(address) == 0)
	{
		return -1;
	}

	// Construct callback and proxy info
	struct CallbackInfo {
	  HWND callback;
	  UINT msg;
	  char proxyAuthName[64];
	  char proxyPassword[32];
	  char meetingId[65];
	  char sessionToken[128];
	  char participantId[128];
	} cbInfo;

    // We never get this info anymore; we rely on wininet and tunnelling
    const char *proxy_auth_name = "";
    const char *proxy_password = "";

	cbInfo.callback = m_hMsgWin;
	cbInfo.msg = CALLBACK_MSG;

	strncpy(cbInfo.proxyAuthName, proxy_auth_name, sizeof(cbInfo.proxyAuthName));
	strncpy(cbInfo.proxyPassword, proxy_password, sizeof(cbInfo.proxyPassword));

	strcpy(cbInfo.meetingId, m_MeetingId);

	strcpy(cbInfo.sessionToken, "");
	strcpy(cbInfo.participantId, "");

	debugf("Connecting to reflector at %s::%d\n", address, port);

	// Set callback window to receive message when vnc server has connected
	COPYDATASTRUCT command = {2, sizeof(CallbackInfo), (void*)&cbInfo };
	SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command);

	// Set reduced color (uses cbBytes as flag, pointer is ignored)
	COPYDATASTRUCT command2 = {6, m_bReducedColor, &m_bReducedColor};
	SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command2);

	// Send command to connect to reflector
	struct ClientInfo { USHORT port; char name[256]; char key[256]; };
	ClientInfo client = { port };
	strcpy(client.name, address);
	if (m_bEnableSecurity) {
		strcpy(client.key, m_EncryptKey);
	} else {
		memset(client.key, 0, sizeof(client.key));
	}
	COPYDATASTRUCT command3 = {9, sizeof(client), &client};
	SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command3);

    //SetDebugMode(TRUE);

	return 0;
}

int UnixServer::SendPassword(HWND wnd, const char * aPassword)
{
	char encrypted[MAXPWLEN+1];

	EncryptPasswd((char *)aPassword, encrypted);
	encrypted[MAXPWLEN] = '\0';

	// Send password to vnc server
	COPYDATASTRUCT command = {0, MAXPWLEN, (void*)encrypted };
	SendMessage(wnd, WM_COPYDATA, (WPARAM)wnd, (LPARAM)&command);

	return 0;
}

int UnixServer::SendPort(HWND wnd, UINT aRfbPort)
{
	struct PortInfo { UINT rfbPort; BOOL httpEnable; UINT httpPort; } ports = { aRfbPort, FALSE, 0 };

	// Set port password
	COPYDATASTRUCT command = {1, sizeof(PortInfo), (void*)&ports };
	SendMessage(wnd, WM_COPYDATA, (WPARAM)wnd, (LPARAM)&command);

	return 0;
}

int UnixServer::SetPPTHWND(HWND hWnd, HWND hMeetingWnd)
{
	if (hWnd != 0)
	{
		m_hWndPPT = hWnd;	// allow re-set window handler
		m_hMeetingWnd = hMeetingWnd;

		return 0;
	}

	if (m_hWndPPT!=0)
	{
		HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
		if (vncMenu != 0)
		{
			struct handles { HWND hPPT; HWND hMeeting; } wnds = { m_hWndPPT, m_hMeetingWnd };
			COPYDATASTRUCT command = {CDC_SetPPTHWND, sizeof(handles), (void*)&wnds };
			SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command);
		}
	}

	return 0;
}

int UnixServer::EnableRecording(BOOL bEnable)
{
/*
	m_bEnableRecording = bEnable==TRUE;

	HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
	if (vncMenu != 0)
	{
		COPYDATASTRUCT command = {CDC_EnableRecording, bEnable, &bEnable };
		SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command);
	}
*/
	return 0;
}

int UnixServer::SetAgentMode(BOOL bEnable)
{
	m_bAgentMode = bEnable==TRUE;

	HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
	if (vncMenu != 0)
	{
		COPYDATASTRUCT command = {CDC_SetAgentMode, bEnable, &bEnable };
		SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command);
	}

	return 0;
}

int UnixServer::SetDebugMode(BOOL bEnable)
{
	HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
	if (vncMenu != 0)
	{
		COPYDATASTRUCT command = {CDC_SetDebugMode, bEnable, &bEnable };
		SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command);
	}

	return 0;
}

int UnixServer::SetNagleAlgorithm(bool bEnable)
{
	if (m_bNagleAlgorithm != bEnable)
	{
		m_bNagleAlgorithm = bEnable;

		HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
		if (vncMenu != 0)
		{
			COPYDATASTRUCT command = {CDC_SetNagleAlgorithm, bEnable, &bEnable };
			SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command);
		}
	}

	return 0;
}

void UnixServer::OnCallbackMsg(int code)
{
    if( code  ==  CB_GetEncryptKey )
    {
//        debugf("Sending EncryptKey from UnixServer::OnCallbackMsg( )\n" );
        //  Send EncryptKey
        HWND    vncMenu     = 0;
        int     iNumSleeps  = 0;
        while( true )
        {
            vncMenu = ::FindWindow( MENU_CLASS_NAME, NULL );
            if( vncMenu )
                break;
            if( iNumSleeps  >  1000 )
                break;
            sleep_ms( 10 );
            iNumSleeps++;
        }

        if( vncMenu  !=  0 )
        {
            struct EncryptKey { char key[256]; };
            EncryptKey  eKey;
            if (m_bEnableSecurity)
            {
                strcpy( eKey.key, m_EncryptKey );
            }
            else
            {
                memset( eKey.key, 0, sizeof( eKey.key ) );
            }
            COPYDATASTRUCT command3 = {CDC_GetEncryptKey, sizeof( eKey ), &eKey };
            SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command3);
        }
    }
    else
    {
        if (m_Callback)
            m_Callback(m_CallbackData, code);
    }
}


int EncryptPasswd(char *passwd, char *encryptedPasswd)
{
    int i;
    int len = strlen(passwd);

    for (i = 0; i < MAXPWLEN; i++) {
        if (i < len) {
            encryptedPasswd[i] = passwd[i];
        } else {
            encryptedPasswd[i] = 0;
        }
    }

    deskey(fixedkey, EN0);
    des((unsigned char*)encryptedPasswd, (unsigned char *)encryptedPasswd);

    return 8;
}
