/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef UNIXSERVER_H_INCLUDED
#define UNIXSERVER_H_INCLUDED

#include "share_api.h"
#include "../netlib/IICvncMsg.h"

#define FALSE false
#define TRUE true
// UnixServer.h: interface for the UnixServer class.
//
//////////////////////////////////////////////////////////////////////

#ifndef UnixServer_H
#define UnixServer_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "share_api.h"

#define VNC_REFLECTOR_PORT 2182
#define VNC_SERVER_NAME _T("mtgshare")

#define MAX_EXEC_PATH 1024
#define MAXPWLEN 128


// Launch vnc server on client, and direct it to connect to
// reflector.

// Current flow:
// - user starts data share
// - VNC server is launched on client
// - VNC server is directed to connect to reflector
// - Controller API is called to start app share
// - Other clients connect to reflector when event is received

// Configuration:
// - Our address
// - Share server "reverse" port
// - Share server client port


class UnixServer
{
    std::string m_ExecPath;
    std::string m_aAppName;

    bool m_bEnableSecurity;
    const char *m_MeetingId;
    const char *m_PartId;
    const char *m_Token;
    const char *m_EncryptKey;
    bool       m_fEnableSecurity;

	const char *m_ReflectorAddress;
	const char *m_ReflectorPassword;

    share_event_handler m_Callback;
    void *m_CallbackData;

	// Send info to attached VNC server
	int SendPassword(HWND wnd, const char * aPassword);
	int SendPort(HWND wnd, UINT aPort);

	int ConnectReflector();

	HWND FindWindow(int aRetries);

    void log_rotate_if_needed( char *logFN, bool fAlwaysRotate );

    HWND m_hMsgWin;
	HWND m_hWndPPT;
	HWND m_hMeetingWnd;

	int m_iStartTime;
	bool m_bEnableRecording;
	bool m_bLaunching;
	bool m_bAgentMode;
	bool m_bNagleAlgorithm;
	bool m_bReducedColor;

    bool m_bServerWanted;
    bool m_bRestart;

    static std::string s_command;
    static volatile bool s_VncThreadRunning;
    static volatile bool s_RunVncThread;
    static volatile bool s_QuitVncThread;

    static void * RunVnc(void *p);

public:

	ServerStatus m_ServerStatus;
	UnixServer(char *aExecPath);
	virtual ~UnixServer();

    void SetCallbackHandler(void *user, share_event_handler);

    int Initialize(const char *aAddress, const char *aPassword, const char *aEncryptKey, const char *aMeetingId, const char *aPartId, const char *aToken, const bool fEnableSecurity);
//	int LaunchServer(HWND hMsgWin, LPCTSTR aAppName, BOOL aSharePowerPoint, BOOL aShareAllApplicationInstances/*=FALSE*/, BOOL aRestart/*=FALSE*/);
	int LaunchServer(std::string &aAppName, bool aRestart);
	int ShutdownServer(bool aWait = false, bool aRestart = false);
//	int ShutdownServer(bool bWait = false);

	int ShareApplication(LPCTSTR aAppName, BOOL aShareAllApplicationInstances=FALSE);

	bool IsRunning();

	const char * GetMeetingId()	        { return m_MeetingId; }
	void SetMeetingId(const char * id);

	const char * GetReflectorAddress()	{ return m_ReflectorAddress; }
	void SetReflectorAddress(const char * address);

	const char * GetReflectorPassword()	{ return m_ReflectorPassword; }
	void SetReflectorPassword(const char * password);

	// Additional methods to change options
	int EnableTransparent(BOOL bEnable);

	int SetPPTHWND(HWND hWnd, HWND hMeetingWnd);

	void SetStartTime(int start_time)   { m_iStartTime = start_time; }
	int GetStartTime() { return m_iStartTime; }

	int EnableRecording(BOOL bEnable);

	int SetAgentMode(BOOL aEnable);

	int SetDebugMode(BOOL bEnable);

	int SetNagleAlgorithm(bool bEnable);

	void OnCallbackMsg(int code);
};


#endif


#endif // UNIXSERVER_H_INCLUDED
