/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include <unistd.h>
#include <stdarg.h>

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include <sys/types.h>
#include <sys/sysctl.h>

#include <CoreServices/CoreServices.h>
//#include </Developer/Headers/FlatCarbon/Files.h>
//#include </Developer/Headers/FlatCarbon/Folders.h>

#include <log.h>

void sleep_ms(int ms)
{
    struct timeval tv = { 0, ms * 1000 };
    select(0, NULL, NULL, NULL, &tv);
}

const char *get_log_dir()
{
	FSRef dir;
	static char name[1024];
	
	FSFindFolder(kUserDomain, kApplicationSupportFolderType, TRUE, &dir);
	FSRefMakePath(&dir, (UInt8*)name, sizeof(name));
	strcat(name, "/meeting/");
	return name;
}

int get_processes(struct kinfo_proc *&kp)
{
    int mib[4];
    int maxproc;
    size_t len;
    
    kp = NULL;
    
    mib[0] = CTL_KERN;
    mib[1] = KERN_MAXPROC;
    mib[2] = 0;
    len = sizeof(maxproc);
    if (sysctl(mib, 2, &maxproc, &len, NULL, 0) == -1)
    {
        debugf("Error getting max processes");
        return 0;
    }
    
	len = maxproc * sizeof(struct kinfo_proc);
	kp = (struct kinfo_proc*)malloc(len);
    
	mib[0] = CTL_KERN;
	mib[1] = KERN_PROC;
	mib[2] = KERN_PROC_ALL;
	mib[3] = 0;
    
	if (sysctl(mib, 3, kp, &len, NULL, 0) == -1)
    {
		debugf("Error getting process info");
        free(kp);
        return 0;
    }
    
	return len / sizeof(struct kinfo_proc);    
}

int IsAppRunning(const char *name)
{
    int len = strlen(name);
    int numproc;    
    struct kinfo_proc *kp;
    int pid = -1;
    
    numproc = get_processes(kp);
    for (int i = 0; i < numproc; i++)
    {
        debugf("Checking %s", kp[i].kp_proc.p_comm);
        if (strncmp(kp[i].kp_proc.p_comm, name, len) == 0)
        {
            pid = kp[i].kp_proc.p_pid;
            break;
        }
    }
    free(kp);
    debugf("Process %s is %s", name, pid > 0 ? "running" : "not running");
    return pid;
}    

int KillProcessByName(const char *name)
{
    int len = strlen(name);
    int numproc;    
    struct kinfo_proc *kp;
    int pid = -1;
    
    numproc = get_processes(kp);
    for (int i = 0; i < numproc; i++)
    {
        if (strncmp(kp[i].kp_proc.p_comm, name, len) == 0)
        {
            pid = kp[i].kp_proc.p_pid;
            debugf("Killing %d", pid);
            if (kill(pid, SIGKILL))
            {
                debugf("Kill process %d failed: %d", pid, errno);
            }
        }
    }
    free(kp);
    return 0;
}
