/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include <sys/stat.h>

#include "macosxserver.h"
#include "util_macosx.h"

#include <iiclib/ipc.h>
#include <log.h>
#include <pthread.h>

#include <CoreServices/CoreServices.h>
//#include <Gestalt.h>

extern "C" {
#include "d3des.h"
}

extern int StartMessageServer();
extern int StopMessageServer();
extern void SendMessage(const char *src, const char *message);
extern void SendMessage(const char *src, const char *message, const char *format, ...);

int EncryptPasswd(char *passwd, char *encryptedPasswd);

// VNC password encryption key
unsigned char fixedkey[8] = {23,82,107,6,35,78,88,7};

extern MacOSXServer *g_Server;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

MacOSXServer::MacOSXServer(const char *aExecPath, const char *ipcName)
{
    m_ExecPath = aExecPath;
    m_ServerStatus = NotRunning;

    m_ServerName = VNC_SERVER_NAME;

    SInt32 ver;
    Gestalt(gestaltSystemVersion, &ver);
    if ((ver & 0xFFF0) == 0x1050)
        m_ServerName += "-10.5";

    m_MeetingId = NULL;
    m_ReflectorAddress = ""; //NULL;
    m_ReflectorPassword = NULL;
    m_DisplayDevice = NULL;

    m_Callback = NULL;
    m_CallbackData = NULL;

    //m_hMsgWin = 0;
	//m_hWndPPT = 0;
	m_bEnableRecording = false;
	m_bLaunching = false;
	m_bAgentMode = false;
	m_bNagleAlgorithm = true;
	m_bEnableSecurity = false;
	m_bReducedColor = false;
 
	m_ipcName = ipcName;
 
    StartMessageServer();
}

MacOSXServer::~MacOSXServer()
{
    if (m_MeetingId)
        free((void*)m_MeetingId);
    if (m_PartId)
        free((void*)m_PartId);
    if (m_Token)
        free((void*)m_Token);
    if (m_ReflectorPassword)
        free((void*)m_ReflectorPassword);
    if (m_DisplayDevice)
        free((void*)m_DisplayDevice);
    debugf("MacOSXServer destroyed");
}

int MacOSXServer::Initialize(const char *aAddress, const char *aPassword, const char *aEncryptKey, const char *aMeetingId, const char *aPartId, const char *aToken, const bool fEnableSecurity)
{
    std::string S(aAddress);    // have to replace :: with : for x11vnc server, viewer uses :: to specify port
    S.replace( S.find("::"), 2, ":");
    m_ReflectorAddress = strdup((char*)S.c_str());  // use strdup() ?  or would that lead to mem leak ?
    m_ReflectorPassword = strdup(aPassword);
    m_MeetingId = strdup(aMeetingId);
    m_PartId = strdup(aPartId);
    m_Token = strdup(aToken);
    m_bEnableSecurity = fEnableSecurity;
    if (aEncryptKey)
        m_EncryptKey = strdup(aEncryptKey);

    return 0;
}

void MacOSXServer::SetCallbackHandler(void *user, share_event_handler handler)
{
    m_Callback = handler;
    m_CallbackData = user;
}


static pthread_t vnc_thread;

static  std::string command;
static  volatile bool VncThreadRunning = false;         // outside of MacOSXServer class so RunVnc() can access
// RunVnc loops because when it quit after return from system() call then successive pthread_create() calls would lead
//  to whole program going away.
// The reason for having the system() call that launches x11vnc done by a separate thread is to have it wait for return,
//  so we know if it quits early and when it's done.
static  volatile bool RunVncThread = false;          // tell thread fn to do another command
static  volatile bool QuitVncThread = false;         // to tell thread fn to quit

// This routine could automatically launch server on exit but it shouldn't just be looped.
// "caller" will set RunVncThread when he wants server launched again.
void * MacOSXServer::RunVnc(void *p)
{
    VncThreadRunning = true;
    while(!QuitVncThread)
    {
        debugf("Mac OS X - RunVnc Executing: %s\n", command.c_str());

        g_Server->m_ServerStatus = Started;
        if (g_Server->m_Callback)
            g_Server->m_Callback(g_Server->m_CallbackData, CB_Connecting);

        system(command.c_str());

        g_Server->m_ServerStatus = NotRunning;
        if (g_Server->m_Callback)
            g_Server->m_Callback(g_Server->m_CallbackData, CB_Disconnected);

        debugf("\tRunVnc returning\n");
        if(g_Server->m_bServerWanted)
            RunVncThread = true;
        else
            RunVncThread = false;
        while(!RunVncThread)
            sleep_ms(100);
    }
    VncThreadRunning = false;
    return 0;
}

// The number of log files to keep before deleting the oldest
static unsigned history_len = 10;

// If the current log is >= kMaxLogFileSize, rotate the current log
// into the stack of historical files and create a new log file.
static int kMaxLogFileSize = 100 * 1024;

# define MAX_LOG_FILENAME    (512)

void MacOSXServer::log_rotate_if_needed( const char *logFN, bool fAlwaysRotate )
{
    char newerfn[MAX_LOG_FILENAME];
    char olderfn[MAX_LOG_FILENAME];
    int i;
    long    sz      = -1;

    struct stat myStats;
    i = stat( logFN, &myStats );
    if( i  ==  0 )
        sz = myStats.st_size;

    if( (sz < kMaxLogFileSize)  &&  !fAlwaysRotate )
        return;

    sprintf( newerfn, "%s.%u", logFN, history_len );
    unlink( newerfn );
    for( i = history_len-1; i > 0; i--)
    {
        strcpy( olderfn, newerfn );
        sprintf( newerfn, "%s.%u", logFN, i );
        if( rename(newerfn, olderfn)  ==  0)
        {
//            errNo = GetLastError( );
        }
    }
    strcpy(olderfn, newerfn);
    strcpy(newerfn, logFN);
    if (rename(newerfn, olderfn)  ==  0)
    {
//        errNo = GetLastError( );
    }
}

int MacOSXServer::LaunchServer(std::string &aAppName, bool aRestart)  // for now
{
    if (IsRunning())
    {
        // error, kill it
        ShutdownServer(true);       // kill it and wait for death cert, also make sure RunVnc not running.
    }

    // Save these for relaunch
    m_bServerWanted = true;
    m_aAppName = aAppName;

    std::string log_dir = get_log_dir();
	std::string log_name = log_dir + "/mtgshare.log";
	
	debugf("Viewer log file: %s", log_name.c_str());
    log_rotate_if_needed(log_name.c_str(), false);
    
    char buff[32];
    sprintf(buff, "%d", m_iStartTime);

    SInt32 ver;
    Gestalt(gestaltSystemVersion, &ver);

    command = m_ExecPath + "/" + m_ServerName; 
    if(aAppName != "")
        command = command + " -winid " + aAppName;

    if (m_DisplayDevice && *m_DisplayDevice)
        command = command + " -macdisplay " + m_DisplayDevice;

    // need osx equivalent to remove wallpaper
    // command = command + " -nodragging -solid steelblue";
    command = command + " -macnosaver -macnosleep -macnowait ";
    command = command + " -nonap -xd_area 0 -wait 10 -defer 10 ";

    if( m_bEnableSecurity )
        command = command + " -useEncryptkey yes";
    else
        command = command + " -useEncryptkey no";

    if (m_Token != NULL)
        command = command + " -token " + m_Token;

    if (m_PartId != NULL)
        command = command + " -partid " + m_PartId;

    command = command + " -connect_or_exit " + m_ReflectorAddress + " -meeting " + m_MeetingId +
              " -starttime " + buff + " -recording " + (m_bEnableRecording?"1":"0");

	command = command + " -ipc " + m_ipcName;

	debugf("Viewer launch: %s", command.c_str());
	
    command = command + " >>\"" + log_name +"\" 2>&1"; 
    
    // PROBLEM:  using thread, works 1st time, program quits 2nd time, or 3rd or 4th...
    debugf("VncThreadRunning %d\n", VncThreadRunning);     // should be 0
    if(!VncThreadRunning)
    {
        int ThreadRet = pthread_create(&vnc_thread, NULL, RunVnc, NULL);
        if(ThreadRet == -1)  // error
        {
            debugf("ERROR creating vnc thread\n");
        }
    }
    else
        RunVncThread = true;

    return 0;
}

#if 0   // will need to do this !!!
int MacOSXServer::LaunchServer(HWND hMsgWin, LPCTSTR aAppName, BOOL aSharePowerPoint,
							 BOOL aShareAllApplicationInstances, BOOL aRestart)
{
	// Prevent re-entrancy
	Flag flag(m_bLaunching);
	if (!flag.check())
		return 0;

    m_hMsgWin = hMsgWin;

	// See if server is already running
	// kill the previous instance if there is
    if (IsRunning())
    {
        ShutdownServer(true);
    }

	KillProcessByName(m_ServerName.c_str());

	// Wait 10 seconds, then signal user that we've got a stuck process
	int retry = 20;
	while (IsAppRunning(m_ServerName.c_str()) >= 0)
	{
		if (--retry)
			Sleep(500);
		else
			return SHARE_REBOOT;
	}

	{
		TCHAR buff[32];
		m_iStartTime = time(NULL);
		_itot(m_iStartTime, buff, 10);

		if (GetOSVersion()==2)	// for 2000+, workaround bring up app to front issue
		{
			DWORD recipients = BSM_APPLICATIONS;
			BroadcastSystemMessage(BSF_ALLOWSFW, &recipients, 0, 0, 0);
		}

		// Not running, launch it.
		TCHAR vnc[MAX_EXEC_PATH];
		_tcscpy(vnc, m_ExecPath);
		_tcscat(vnc, m_ServerName.c_str());
		if (aSharePowerPoint)
		{
			if (spawnl(_P_DETACH, vnc, m_ServerName.c_str(), winvncStartTime, buff,
				winvncRecording, (m_bEnableRecording?_T("1"):_T("0")), winvncSharePPT, NULL) == -1)
			{
				//int err = GetLastError();
				return SHARE_FAILED_LAUNCH;
			}
		}
		else if (aAppName != NULL && _tcslen(aAppName)>0)
		{
			if (spawnl(_P_DETACH, vnc, m_ServerName.c_str(), winvncStartTime, buff,
                winvncRecording, (m_bEnableRecording?_T("1"):_T("0")),
                winvncAllInstances, (aShareAllApplicationInstances?_T("1"):_T("0")),
                winvncShareApps, aAppName, NULL) == -1)
			{
				//int err = GetLastError();
				return SHARE_FAILED_LAUNCH;
			}
		}
		else
		{
			if (spawnl(_P_DETACH, vnc, m_ServerName.c_str(), winvncStartTime, buff,
				winvncRecording, (m_bEnableRecording?_T("1"):_T("0")), winvncRunAsUserApp, NULL) == -1)
			{
				//int err = GetLastError();
				return SHARE_FAILED_LAUNCH;
			}
		}
	}

	// Give process a second to start
	HWND vncMenu = FindWindow();
	if (vncMenu == NULL)
	{
		errorf("Error waiting for share process to start");
		return SHARE_FAILED_LAUNCH;
	}

    // Connect to share server.
    if (ConnectReflector())
    {
        errorf("Error connecting to share server");
        return SHARE_FAILED_LAUNCH;
    }

	SetAgentMode(m_bAgentMode);
	SetNagleAlgorithm(m_bNagleAlgorithm);

	return 0;
}
#endif


int MacOSXServer::ShutdownServer(bool aWait)
{
    m_bServerWanted = false;
    debugf("Shutting down server\n");

	SendMessage("server", "shutdown");
	
	if (aWait)
	{
        sleep_ms(100);
		int retry = 20;
		while(IsAppRunning(m_ServerName.c_str()) >= 0)
		{
			if (--retry)
			{
				sleep_ms(100);
			}
			else
			{
				debugf("Server didn't exit cleanly, killing");
				KillProcessByName(m_ServerName.c_str());
				return SHARE_REBOOT;
			}
		}
	}

    // Either it wasn't running or it quit before we timed out.
    m_ServerStatus = NotRunning;
    debugf("\tserver shut down\n");

	return 0;
}

void MacOSXServer::Refresh()
{
    debugf("Share server refresh");
    
    if (m_Callback)
        m_Callback(m_CallbackData, CB_Disconnected);
        
    // An orderly shutdown takes a bit, so kill process instead
    KillProcessByName(m_ServerName.c_str());
}

int MacOSXServer::ShareApplication(const char * aAppName, BOOL aShareAllApplicationInstances)
{

	return 0;
}

int MacOSXServer::EnableTransparent(BOOL bEnable)
{

	return 0;
}

bool MacOSXServer::IsRunning()
{
    int res = IsAppRunning(m_ServerName.c_str());
    if (res >= 0)
    {
        m_ServerStatus = Started;
        return true;
    }
    else
    {
        m_ServerStatus = NotRunning;
        return false;
    }
}

void MacOSXServer::SetMeetingId(const char * id)
{
	m_MeetingId = strdup(id);
}

void MacOSXServer::SetReflectorAddress(const char * address)
{
	m_ReflectorAddress = strdup(address);
}

void MacOSXServer::SetReflectorPassword(const char * password)
{
	m_ReflectorPassword = strdup(password);
}

int MacOSXServer::ConnectReflector()
{
#if 0
	SendPassword(vncMenu, m_ReflectorPassword);

    char address[512];
	UINT port = VNC_REFLECTOR_PORT;

    strcpy(address, m_ReflectorAddress);
	char *port_str = strstr(address, "::");
	if (port_str != NULL)
	{
		port = atoi(port_str + 2);
		*port_str = '\0';
	}

	if (strlen(address) == 0)
	{
		return -1;
	}

	// Construct callback and proxy info
	struct CallbackInfo {
	  HWND callback;
	  UINT msg;
	  char proxyAuthName[64];
	  char proxyPassword[32];
	  char meetingId[65];
	  char sessionToken[128];
	  char participantId[128];
	} cbInfo;

    // We never get this info anymore; we rely on wininet and tunnelling
    const char *proxy_auth_name = "";
    const char *proxy_password = "";

	cbInfo.callback = m_hMsgWin;
	cbInfo.msg = CALLBACK_MSG;

	strncpy(cbInfo.proxyAuthName, proxy_auth_name, sizeof(cbInfo.proxyAuthName));
	strncpy(cbInfo.proxyPassword, proxy_password, sizeof(cbInfo.proxyPassword));

	strcpy(cbInfo.meetingId, m_MeetingId);

	strcpy(cbInfo.sessionToken, "");
	strcpy(cbInfo.participantId, "");

	debugf("Connecting to reflector at %s::%d\n", address, port);

	// Set callback window to receive message when vnc server has connected
	COPYDATASTRUCT command = {2, sizeof(CallbackInfo), (void*)&cbInfo };
	SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command);

	// Set reduced color (uses cbBytes as flag, pointer is ignored)
	COPYDATASTRUCT command2 = {6, m_bReducedColor, &m_bReducedColor};
	SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command2);

	// Send command to connect to reflector
	struct ClientInfo { USHORT port; char name[256]; char key[256]; };
	ClientInfo client = { port };
	strcpy(client.name, address);
	if (m_bEnableSecurity) {
		strcpy(client.key, m_EncryptKey);
	} else {
		memset(client.key, 0, sizeof(client.key));
	}
	COPYDATASTRUCT command3 = {9, sizeof(client), &client};
	SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command3);
#endif
    //SetDebugMode(TRUE);

	return 0;
}

int MacOSXServer::SendPassword(const char * aPassword)
{
	char encrypted[MAXPWLEN+1];

	EncryptPasswd((char *)aPassword, encrypted);
	encrypted[MAXPWLEN] = '\0';

	// Send password to vnc server

	return 0;
}

int MacOSXServer::SendPort(unsigned aRfbPort)
{

	return 0;
}

/*
int MacOSXServer::SetPPTHWND(HWND hWnd, HWND hMeetingWnd)
{

	return 0;
}
*/

int MacOSXServer::EnableRecording(BOOL bEnable)
{
	m_bEnableRecording = bEnable==TRUE;

	return 0;
}

int MacOSXServer::SetAgentMode(BOOL bEnable)
{
	m_bAgentMode = bEnable==TRUE;

	return 0;
}

int MacOSXServer::SetDebugMode(BOOL bEnable)
{

	return 0;
}

int MacOSXServer::SetNagleAlgorithm(bool bEnable)
{
	if (m_bNagleAlgorithm != bEnable)
	{
		m_bNagleAlgorithm = bEnable;

		// Send to vnc server
	}

	return 0;
}

void MacOSXServer::SetDisplayDevice( const char *display )
{
    if ( m_DisplayDevice )
        free( (void*)m_DisplayDevice );
    m_DisplayDevice = display ? strdup( display ) : NULL;
}

void MacOSXServer::OnCallbackMsg(int code)
{
    debugf("Share server callback: %d", code);

#if 0    
    if (code  ==  CB_GetEncryptKey)
    {
        //  Send EncryptKey
        HWND    vncMenu     = 0;
        int     iNumSleeps  = 0;
        while( true )
        {
            vncMenu = FindWindow();
            if( vncMenu )
                break;
            if( iNumSleeps  >  1000 )
                break;
            sleep_ms( 10 );
            iNumSleeps++;
        }

        if( vncMenu  !=  0 )
        {
            struct EncryptKey { char key[256]; };
            EncryptKey  eKey;
            if (m_bEnableSecurity)
            {
                strcpy( eKey.key, m_EncryptKey );
            }
            else
            {
                memset( eKey.key, 0, sizeof( eKey.key ) );
            }
            COPYDATASTRUCT command3 = {CDC_GetEncryptKey, sizeof( eKey ), &eKey };
            SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command3);
        }
    }
    else
#endif		
    {
        if (m_Callback)
            m_Callback(m_CallbackData, code);
    }
}

int EncryptPasswd(char *passwd, char *encryptedPasswd)
{
    int i;
    int len = strlen(passwd);

    for (i = 0; i < MAXPWLEN; i++) {
        if (i < len) {
            encryptedPasswd[i] = passwd[i];
        } else {
            encryptedPasswd[i] = 0;
        }
    }

    deskey(fixedkey, EN0);
    des((unsigned char*)encryptedPasswd, (unsigned char *)encryptedPasswd);

    return 8;
}
