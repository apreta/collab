/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include "share_api.h"
#include <stdlib.h>         // for system()
#include <unistd.h>         // fork()
#include <stdio.h>          // printf
#include <string>
#include <sstream>

#include "macosxviewer.h"
#include "macosxserver.h"
#include "util_linux.h"

#include <iiclib/ipc.h>
#include <log.h>

#include <CoreServices/CoreServices.h>
#include <ApplicationServices/ApplicationServices.h>

bool g_debug = false;
bool g_fullScreen = false;
bool g_remoteControl = false;
//HWND g_parentWindow = 0;
MacOSXViewer *g_Viewer = NULL;
MacOSXServer *g_Server = NULL;
queue_t ipc_q = NULL;
std::list<MonitorInfo> g_monitors;

#ifdef SHARE_DRIVER
const char * ipcName = "shared";
#else
const char * ipcName = "shareq";
#endif

static void get_monitor_info();


void share_initialize(const char *exec_path, const char *meeting_id, const char *share_server, const char *password, const char *encrypt_key, const char *part_id, 
                      const char *token, const bool fEnableSecurity, int start_time)
{
	ipc_init();
	
    if (g_Viewer == NULL)
    {
        g_Viewer = new MacOSXViewer(exec_path);
    }
    g_Viewer->Initialize(share_server, password, encrypt_key, meeting_id, part_id, token, fEnableSecurity);

    if (g_Server == NULL)
        g_Server = new MacOSXServer(exec_path, ipcName);
    g_Server->SetStartTime(start_time);        
    g_Server->Initialize(share_server, password, encrypt_key, meeting_id, part_id, token, fEnableSecurity);
    g_Server->SetDisplayDevice(NULL);
}

void share_set_event_handler(void *user, share_event_handler _handler)
{
    g_Server->SetCallbackHandler(user, _handler);
    g_Viewer->SetCallbackHandler(user, _handler);
}

int share_enable_recording(bool enable)
{
    if (g_Server)
        return g_Server->EnableRecording(enable ? TRUE : FALSE);
    return 0;
}

int share_set_options(const char *option, const char *value)
{
    if (strcmp(option, "display") == 0) {
        if (g_Server)
            g_Server->SetDisplayDevice(value);
    }
    return -1;
}

int share_refresh()
{
    if (g_Viewer)
        g_Viewer->Refresh();
    if (g_Server)
        g_Server->Refresh();

    return 0;
}

std::list<MonitorInfo> share_get_displays()
{
    if (g_monitors.size() == 0)
        get_monitor_info();
    return g_monitors;
}

MonitorInfo* share_get_display(const char * display_name)
{
   if (g_monitors.size() == 0)
        get_monitor_info();
        
    if (display_name == NULL || *display_name == 0)
    {
        return &(*g_monitors.begin());
    }
    
    std::list<MonitorInfo>::iterator iter;
    for (iter = g_monitors.begin(); iter != g_monitors.end(); iter++)
    {
        MonitorInfo* info = &(*iter);
        if (info->deviceName == display_name)
            return info;
    }
    return NULL;
}

int share_get_number_displays() 
{
	get_monitor_info();
	return g_monitors.size();
}

MonitorInfo* share_get_display_at(int index)
{
    if (g_monitors.size() == 0)
        get_monitor_info();

    std::list<MonitorInfo>::iterator iter;
    for (iter = g_monitors.begin(); iter != g_monitors.end(); iter++)
    {
        MonitorInfo* info = &(*iter);
        if (index-- == 0)
            return info;
    }
    return NULL;
}

int share_desktop()
{
    std::string null("");
    if(g_Server != NULL)
        g_Server->LaunchServer(null,true);

    return 0;
}

int share_applications(std::string &application, BOOL share_all_instances)
{
    if(g_Server != NULL)
        g_Server->LaunchServer(application, true);

    return 0;
}

int share_presentation(void * present_window, void * parent_window, bool agent_mode)
{
    return -1;
}

int share_end()
{
	if (g_Server)
		g_Server->ShutdownServer();     //  ?? SHould I delete server so it's created to get meeting id ?
    return 0;
}

int share_get_start_time()
{
	if (g_Server)
		return g_Server->GetStartTime();
	return -1;
}

int share_view(void * hParentWnd)
{
	if (g_Viewer)
		return g_Viewer->LaunchViewer(hParentWnd, g_remoteControl, FALSE, 0);
	return -1;
}

void share_enable_fullscreen(bool enable)
{
	if (g_Viewer)
		g_Viewer->EnableFullScreen(enable);
}

void share_enable_control(bool enable)
{
    if( g_remoteControl != enable )
        debugf("g_remoteControl = %d\n", enable);
    g_remoteControl = enable;
    if(g_Viewer)
        g_Viewer->EnableRemoteControl(enable);
}

bool share_is_control_enabled()
{
    return g_remoteControl;
}

void share_move_viewer(int x, int y, int width, int height)
{
	if (g_Viewer)
		g_Viewer->MoveViewer(x, y, width, height);
}

void share_erase_background(int x, int y, int width, int height)
{
}

void share_view_end()
{
	if (g_Viewer)
		g_Viewer->ShutdownViewer(false);
}

void share_get_buffer(void*& buffer, int& width, int& height)
{
	if (g_Viewer)
		g_Viewer->GetBuffer(buffer, width, height);
}

int share_handle_event(int code)
{
    if (g_Viewer)
        return g_Viewer->HandleEvent(code);
    return 0;
}

void share_request_control()
{
    if (g_Viewer)
        g_Viewer->RequestRemoteControl();
}

void share_remote_mouse_move(int x, int y)
{
    if (g_Viewer)
        g_Viewer->RemoteMouseMove(x, y);
}

void share_remote_button(int button, bool pressed, int x, int y)
{
    if (g_Viewer)
        g_Viewer->RemoteMouseButton(button, pressed, x, y);
}

void share_remote_key(int key, bool pressed)
{
    if (g_Viewer)
        g_Viewer->RemoteKey(key, pressed);
}


//
// IPC for vnc components
//

void SendMessage(const char *src, const char *message)
{
	datablock_t blk = datablock_create(0);
	datablock_add_string(blk, src, "");
	datablock_add_string(blk, message, "");
	int stat = queue_send(ipc_q, blk);
	if (stat)
	{
		errorf("Error sending ipc: %s, %s", src, message);
	}
	datablock_destroy(blk);
}

void SendMessage(const char *src, const char *message, const char *format, ...)
{
	va_list ap;
	int d;
	char *s;
	datablock_t blk = datablock_create(0);

	datablock_add_string(blk, src, "");
	datablock_add_string(blk, message, "");

	va_start(ap, format);
	while (*format)
	{
		switch(*format++) 
		{
		   case 's':                       /* string */
				   s = va_arg(ap, char *);
				   datablock_add_string(blk, s, "");
				   break;
		   case 'd':                       /* int */
				   d = va_arg(ap, int);
				   datablock_add_int(blk, d);
				   break;
		}
	}

	int stat = queue_send(ipc_q, blk);
	if (stat)
	{
		errorf("Error sending ipc: %s, %s", src, message);
	}
	datablock_destroy(blk);

	va_end(ap);
}

void QueueHandler(void *user, datablock_t blk)
{
	std::string src = datablock_get_string(blk);
	std::string cmd = datablock_get_string(blk);
	debugf("ipc: %s, %s", src.c_str(), cmd.c_str());

	if (src == "viewer") {
		if (cmd == "connected") {
			g_Viewer->OnClientCreated();
		}
		else if (cmd == "flush") {
			g_Viewer->OnDisplayFlush();
		}
		else if (cmd == "resize") {
			int width = datablock_get_int(blk);
			int height = datablock_get_int(blk);
			g_Viewer->OnResize(width, height);
		}
        else if (cmd == "control") {
            int granted = datablock_get_int(blk);
            g_Viewer->OnControlGranted(granted != 0);
        }
	} else if (src == "server") {
		if (cmd == "connected") {
			g_Server->OnCallbackMsg(CB_Connected);
		}
	}
		
	datablock_destroy(blk);
}

int StartMessageServer()
{
    if (ipc_q == NULL)
    {
        ipc_q = queue_create(ipcName);
        queue_server(ipc_q);
        queue_set_event_handler(ipc_q, QueueHandler, NULL);

        queue_run(ipc_q);
    }

    return 0;
}

int StopMessageServer()
{
    if (ipc_q != NULL)
    {
        queue_destroy(ipc_q);
        ipc_q = NULL;
    }
    
    return 0;
}

/*
 * Utility functions
 */

static void get_monitor_info()
{
    g_monitors.clear();
	unsigned int ndisp = 0;

    CGDirectDisplayID disp[64];

    if (CGGetActiveDisplayList(64, disp, &ndisp) == kCGErrorSuccess) 
    {
        unsigned i;
        for (i = 0; i < ndisp; i++)
        {
            CGDirectDisplayID d = disp[i];
            int w = CGDisplayPixelsWide(d);
            int h = CGDisplayPixelsHigh(d);
            int m = CGDisplayIsMain(d);
            int a = CGDisplayIsActive(d);
            int b = CGDisplayIsBuiltin(d);
            unsigned int vendor = CGDisplayVendorNumber(d);
            CGSize sz = CGDisplayScreenSize(d);
            CGRect bd = CGDisplayBounds(d);
            debugf("%5d ID: 0x%08x width: %4d (%3d mm) height: %4d (%3d mm) at (%d, %d)\nMain=%d Builtin=%d Active=%d Vendor: 0x%x%s",
                i, d, w, (int) sz.width, h, (int) sz.height, (int) bd.origin.x, (int) bd.origin.y, m, b, a, vendor, vendor == 0x610 ? " (Apple)" : "");

            std::stringstream id;
            id << i;
            
            std::stringstream desc;
            desc << "Display " << i << " " << w << "x" << h << (b ? " built-in" : (m ? " primary" : " secondary"));
            
            MonitorInfo mon = { id.str(), desc.str(), bd.origin.x, bd.origin.y, bd.origin.x + w, bd.origin.y + h };
            g_monitors.push_back(mon);
        }
    }
}
