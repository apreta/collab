/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include <stdlib.h>
#include <sys/stat.h>

#include "macosxviewer.h"
#include "util_macosx.h"
#include <iiclib/ipc.h>
#include <log.h>

extern int StartMessageServer();
extern int StopMessageServer();
extern void SendMessage(const char *src, const char *message);
extern void SendMessage(const char *src, const char *message, const char *format, ...);

volatile static bool ViewerWanted = false;
extern MacOSXViewer *g_Viewer;
std::string g_command;

static pthread_t vnc_thread;

static  volatile bool VncThreadRunning = false;         // outside of UnixServer class so RunVnc() can access
// RunVnc loops because when it quit after return from system() call then successive pthread_create() calls would lead
//  to whole program going away.
// The reason for having the system() call that launches viewer done by a separate thread is to have it wait for return,
//  so we know if it quits early and when it's done.
// TODO: this was true on Linux, perhaps on Mac we can let the thread exit.
static  volatile bool RunVncThread = false;          // tell thread fn to do another command
static  volatile bool QuitVncThread = false;         // to tell thread fn to quit


void *MacOSXViewer::RunVnc(void *p)
{
    bool restart = false;
    
    while(!QuitVncThread)
    {
        VncThreadRunning = true;
        debugf("MacOSXViewer::RunVnc viewer executing: %s\n", g_command.c_str());

        g_Viewer->m_ViewerStatus = Started;
        if (g_Viewer->m_Callback)
            g_Viewer->m_Callback(g_Viewer->m_CallbackData, CB_Connecting);

        if (restart)
            sleep_ms(2000);
        
        system(g_command.c_str());

        g_Viewer->m_ViewerStatus = NotRunning;
        if (g_Viewer->m_Callback)
            g_Viewer->m_Callback(g_Viewer->m_CallbackData, CB_Disconnected);

        debugf("\tRunVnc viewer returning\n");
        RunVncThread = false;
        while(!RunVncThread)
            sleep_ms(100);
        
        restart = true;
    }
    VncThreadRunning = false;
    return 0;
}

MacOSXViewer::MacOSXViewer(const char *aExecPath)
{
    m_ExecPath = aExecPath;
    m_Callback = NULL;
    m_CallbackData = NULL;
    m_FullScreen = false;
    m_hParent = NULL;
    m_ViewerStatus = NotRunning;
    m_bRelaunch = false;
    m_fEnableSecurity = false;
	m_DisplayHandle = 0;
	m_DisplayMemory = NULL;
	m_Width = 0;
	m_Height = 0;
}

int MacOSXViewer::Initialize(const char *aAddress, const char *aPassword, const char *aEncryptKey, const char *aMeetingId, const char *aPartId, const char *aToken, const bool fEnableSecurity)
{
    m_ReflectorAddress = aAddress;
    m_ReflectorPassword = aPassword;
    m_MeetingId = aMeetingId;
    m_PartId = aPartId;
    m_Token = aToken;
    m_fEnableSecurity = fEnableSecurity;
    if (aEncryptKey)
        m_EncryptKey = aEncryptKey;
    StartMessageServer();
    return 0;
}

void MacOSXViewer::SetCallbackHandler(void *user, share_event_handler handler)
{
    m_Callback = handler;
    m_CallbackData = user;
}

int MacOSXViewer::OnClientCreated()
{
    debugf("OnClientCreated");

    if (m_Callback)
        m_Callback(m_CallbackData, CB_Connected);
    return 0;
}

int MacOSXViewer::OnClientDestroyed()
{
    if (m_Callback)
        m_Callback(m_CallbackData, CB_Disconnected);
    return 0;
}

int MacOSXViewer::OnDisplayFlush()
{
    if (m_Callback)
        m_Callback(m_CallbackData, CB_DisplayUpdated);
    return 0;
}

int MacOSXViewer::OnResize(int width, int height)
{
	 debugf("Share viewer resize (%d, %d)", width, height);
	 m_Width = width;
	 m_Height = height;
    return 0;
}

int MacOSXViewer::OnControlGranted(bool granted)
{
    debugf("Share viewer control %s", granted ? "granted" : "revoked");
    if (m_Callback)
        m_Callback(m_CallbackData, granted ? CB_GrantControl : CB_RevokeControl);
    return 0;
}

int MacOSXViewer::HandleEvent(int id)
{
    if (!ViewerWanted)
        return 0;

    switch(id)
    {
        case CB_Connecting:
            debugf("CB_Connecting\n");
            break;

        case CB_Connected:
            debugf("CB_Connected\n");
            break;

        case CB_Disconnected: 
            debugf("CB_Disconnected\n");
            if (ViewerWanted)
            {
                LaunchViewer(m_hParent, m_bEnableControl, true, false);
                return 1;
            }
			else if (m_DisplayMemory)
            {
                m_DisplayMemory = NULL;
                shm_destroy(m_DisplayHandle);
            }
            break;

        case CB_SessionEnded:       // This happens only, or after Disconnected.
            debugf("CB_SessionEnded\n");

        default:
            debugf("Viewer::HandleEvent unknown id %d\n",id);
            break;
    }
    return 0;
}


// The number of log files to keep before deleting the oldest
static unsigned history_len = 10;

// If the current log is >= kMaxLogFileSize, rotate the current log
// into the stack of historical files and create a new log file.
static int kMaxLogFileSize = 100 * 1024;

# define MAX_LOG_FILENAME    (512)

void MacOSXViewer::log_rotate_if_needed(const char *logFN, bool fAlwaysRotate)
{
    char newerfn[MAX_LOG_FILENAME];
    char olderfn[MAX_LOG_FILENAME];
    int i;
    long    sz      = -1;

    struct stat myStats;
    i = stat( logFN, &myStats );
    if( i  ==  0 )
        sz = myStats.st_size;

    if( (sz < kMaxLogFileSize)  &&  !fAlwaysRotate )
        return;

    sprintf( newerfn, "%s.%u", logFN, history_len );
    unlink( newerfn );
    for( i = history_len-1; i > 0; i--)
    {
        strcpy( olderfn, newerfn );
        sprintf( newerfn, "%s.%u", logFN, i );
        if( rename(newerfn, olderfn)  ==  0)
        {
//            errNo = GetLastError( );
        }
    }
    strcpy(olderfn, newerfn);
    strcpy(newerfn, logFN);
    if (rename(newerfn, olderfn)  ==  0)
    {
//        errNo = GetLastError( );
    }
}


int MacOSXViewer::LaunchViewer(void * hParent, bool aEnableControl, bool aRestart, int aOption)
{
    m_hParent = hParent;
    m_bEnableControl = aEnableControl;

	ShutdownViewer(true);
    
	std::string log_dir = get_log_dir();
	std::string log_name = log_dir + "/mtgview.log";
	
	debugf("Viewer log file: %s", log_name.c_str());
    log_rotate_if_needed(log_name.c_str(), false);

    std::string path = m_ExecPath + "/" + VNC_CLIENT_NAME;
    m_command = path + " -ipassword " + m_ReflectorPassword;
    if (m_fEnableSecurity)
        m_command += " -key " + m_EncryptKey;
    m_command += " -meetingid " + m_MeetingId + " -partid " + m_PartId;
	m_command += " -ipc shareq -f r8b8g8p8";
	m_command += " -logdir \"" + std::string(get_log_dir()) + "\" -d -d ";
	m_command += m_ReflectorAddress;
     
	debugf("Viewer launch: %s", m_command.c_str());
	
    m_command = m_command + " >>\"" + log_name +"\" 2>&1";
    g_command = m_command;  // save global copy for other thread

    ViewerWanted = true;
    sleep_ms(500); // we appear to get the share started event a bit before server is ready to accept listeners

    if(!VncThreadRunning)
    {
        int ThreadRet = pthread_create(&vnc_thread, NULL, RunVnc, NULL);
        if(ThreadRet == -1)  // error
        {
            debugf("ERROR creating vnc thread\n");
        }
    }
    else
        RunVncThread = true;

    return 0;
}


int MacOSXViewer::ReLaunchViewer(void * hParent, bool aEnableControl, bool aRestart, int aOption)
{
    return 0;
}


int MacOSXViewer::ShutdownViewer(bool bWait)
{
    ViewerWanted = false;
    debugf("Shutting down viewer\n");

    if (IsAppRunning(VNC_CLIENT_NAME) < 0)
        return 0;
    
	SendMessage("viewer", "shutdown");

    if (bWait)
    {
        sleep_ms(100);
        int retry = 20;
        while (IsAppRunning(VNC_CLIENT_NAME) >= 0)
        {
            if (--retry)
            {
                sleep_ms(100);
            }
            else 
            {
                debugf("Viewer didn't exit cleanly, killing process\n");
                KillProcessByName(VNC_CLIENT_NAME);
                return 0;
            }
        }
    }
  
    return 0;
}

void MacOSXViewer::Refresh()
{
    debugf("Share viewer refresh");

    if (m_Callback)
        m_Callback(m_CallbackData, CB_Connecting);

    ClearBuffer();
    OnDisplayFlush();
    
    // An orderly shutdown takes a bit, so kill process instead
	//LaunchViewer(m_hParent, m_bEnableControl, true, 0 /*aShareOption*/);
    KillProcessByName(VNC_CLIENT_NAME);
}

void MacOSXViewer::MoveViewer(int x, int y, int width, int height)
{
}

void MacOSXViewer::RequestRemoteControl()
{
	if (m_bEnableControl)
	{
		SendMessage("viewer", "control");
	}
}

void MacOSXViewer::RemoteMouseMove(int x, int y)
{
	if (m_bEnableControl)
	{
		SendMessage("viewer", "mouse", "dd", x, y);		
	}
}

void MacOSXViewer::RemoteMouseButton(int button, bool pressed, int x, int y)
{
	static const char *names[] = { "left", "middle", "right" };

	if (m_bEnableControl)
	{
		SendMessage("viewer", "button", "ssdd", 
			names[button],
			pressed ? "down" : "up",
			x, y);		
	}	
}

void MacOSXViewer::RemoteKey(int key, bool pressed)
{
	if (m_bEnableControl)
	{
		SendMessage("viewer", "key", "ds",
			key,
			pressed ? "down" : "up");
	}	
}

void MacOSXViewer::EnableRemoteControl(bool aEnable)
{
    m_bEnableControl = aEnable;

	/*	// N.B. previously, enable flag is actually implemented in vnc viewer as a "view only" flag.
		//  Now we send enable flag.
		debugf("%s app share session control\n", aEnable ? ("Enabling") : ("Disabling"));
		PostMessage(IIC_vncViewer, msgEnableRemoteControl, aEnable, 0);
	 */

    if (g_Viewer->m_Callback)
        g_Viewer->m_Callback(g_Viewer->m_CallbackData, aEnable ? CB_EnableControl : CB_DisableControl);
}

void MacOSXViewer::EnableFullScreen(bool bEnable)
{
    if (bEnable)
    {
        if (m_Callback)
            m_Callback(m_CallbackData, CB_DisplayFullScreen);
    }
    else
    {
        if (m_Callback)
            m_Callback(m_CallbackData, CB_DisplayNormal);
    }
    m_FullScreen = bEnable;
}

int MacOSXViewer::GetBuffer(void*& buffer, int& width, int& height)
{
	width = m_Width;
	height = m_Height;
	
	if (m_DisplayMemory == NULL)
	{
		m_DisplayHandle = shm_create("shareq", 20000000, 1);
		if (m_DisplayHandle == NULL) 
		{
			errorf("Failed to open shared memory");
			return -1;
		}
		
		m_DisplayMemory = shm_get_buffer(m_DisplayHandle);
	}
	
	buffer = m_DisplayMemory;
	
	return 0;
}

void MacOSXViewer::ClearBuffer()
{
    if (m_DisplayMemory)
    {
        int size = m_Width * m_Height * 8;
        memset(m_DisplayMemory, 0, size);
    }
}

void MacOSXViewer::OnGetEncryptKey()
{
    //  Send EncryptKey
/*    HWND    vncMenu     = 0;
    int     iNumSleeps  = 0;
    while( true )
    {
        vncMenu = ::FindWindow( MENU_CLASS_NAME, NULL );
        if( vncMenu )
            break;
        if( iNumSleeps  >  1000 )
            break;
        sleep_ms( 10 );
        iNumSleeps++;
    }

    if( vncMenu  !=  0 )
    {
        struct EncryptKey { char key[256]; };
        EncryptKey  eKey;
        if (m_fEnableSecurity)
        {
            strcpy( eKey.key, m_EncryptKey.c_str( ) );
        }
        else
        {
            memset( eKey.key, 0, sizeof( eKey.key ) );
        }
        COPYDATASTRUCT command3 = {CDC_GetEncryptKey, sizeof( eKey ), &eKey };
        SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command3);
    }
	 */
}
