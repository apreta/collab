/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef SHAREAPI_H
#define SHAREAPI_H

#include <string>
#include <list>

/*
    Define API for sharing desktop or application.
    This API should remain cross platform.
*/

#ifndef SHARE_API 
	#ifdef _MSC_VER
		#define SHARE_API __declspec(dllimport)
	#else
		#define SHARE_API
	#endif
#endif

typedef int BOOL;

#define SHARE_REBOOT -1
#define SHARE_FAILED_LAUNCH -2
#define SHARE_NO_SERVER -3

// Callback event codes
enum {
	CB_Connected = 0,
	CB_Disconnected,
	CB_ConnectFailed,
	CB_SessionEnded,
	CB_InternalError,
	CB_Connecting,

	CB_GetEncryptKey,

    CB_DisplayUpdated,
    CB_DisplayFullScreen,
    CB_DisplayNormal,

    CB_EnableControl,
    CB_DisableControl,
    CB_GrantControl,
    CB_RevokeControl,

    CB_Max
};

enum {
    ViewFullScreen = 1,
    ViewAutoDetect = 2
};

typedef enum {
    NotRunning=0, 
    Started, 
    Connected
} ServerStatus;

struct MonitorInfo {
    std::string deviceName;     // Internal name for display
    std::string description;    // Human readable name
    int tx, ty, bx, by;         // Position of this display on virtual desktop
};


// Event callback.
// N.B.: the event handler may be called from a backgroup thread.
typedef void (*share_event_handler)(void *user, int callback_event);

SHARE_API void share_initialize(const char *exec_path, const char *share_server, const char *password, const char *encrypt_key,
                      const char *meeting_id, const char *part_id, const char *sessionToken, const bool fEnableSecurity,
                      int start_time);
SHARE_API void share_set_event_handler(void *user, share_event_handler _handler);
SHARE_API int share_set_options(const char *option, const char *value);

SHARE_API int share_refresh();

SHARE_API int share_get_number_displays();
SHARE_API std::string share_get_display(int index);

// Get information about connected displays.  
// Use share_set_option("display", display_name) to select display to share.
SHARE_API std::list<MonitorInfo> share_get_displays();
SHARE_API MonitorInfo* share_get_display(const char * display_name);
SHARE_API MonitorInfo* share_get_display_at(int index);

// APIs for presenter
SHARE_API int share_desktop();
SHARE_API int share_applications(std::string &application, BOOL share_all_instances);
SHARE_API int share_presentation(void * present_window, void * parent_window, bool agent_mode);
SHARE_API int share_enable_recording(bool enable);
SHARE_API int share_get_start_time();
SHARE_API int share_end();

// APIs for viewer
SHARE_API int share_view(void * hParentWbd);
SHARE_API void share_enable_fullscreen(bool enable);
SHARE_API void share_enable_control(bool enable);
SHARE_API bool share_is_control_enabled();
SHARE_API void share_move_viewer(int x, int y, int width, int height);
SHARE_API void share_erase_background(int x, int y, int width, int height);
SHARE_API void share_get_buffer(void*& buffer, int& width, int& height);
SHARE_API void share_view_end();

// APIs for viewer remote control
// Keycods are as defined by RFB, which is derived from x11 keysym.
enum {
	RemoteLeftButton = 0,
	RemoteMiddleButton = 1,
	RemoteRightButton = 2
};
SHARE_API void share_request_control();
SHARE_API void share_remote_mouse_move(int x, int y);
SHARE_API void share_remote_button(int button, bool pressed, int x, int y);
SHARE_API void share_remote_key(int key, bool pressed);

// For synchronization purposes, events dispatched to the callback handler set above should be 
// marshalled to the main thread, then passed back to this API.
SHARE_API int share_handle_event(int event);

#endif
