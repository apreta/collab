/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include "share_api.h"
#include <stdlib.h>         // for system()
#include <unistd.h>         // fork()
#include <stdio.h>          // printf

#include "unixviewer.h"
#include "unixserver.h"
#include "util_linux.h"

#include <log.h>

bool g_debug = false;
bool g_fullScreen = false;
bool g_remoteControl = false;
HWND g_parentWindow = 0;
HWND g_serverListener = 0;
HWND g_viewerListener = 0;
UnixViewer *g_Viewer = NULL;
UnixServer *g_Server = NULL;

/*  These are defined in IICvncMsg.cpp
const UINT msgEnableRemoteControl			= RegisterWindowMessage(_T("IICViewer.EnableRemoteControl"));
const UINT msgEnableFullScreen				= RegisterWindowMessage(_T("IICViewer.EnableFullScreen"));
const UINT msgEnableDebugMode				= RegisterWindowMessage(_T("IICViewer.EnableDebugMode"));
const UINT msgGetEncryptKey				= RegisterWindowMessage("IICViewer.GetEncryptKey");
*/

void share_initialize(const char *exec_path, const char *meeting_id, const char *share_server, const char *password, const char *encrypt_key, const char *part_id, const char *token, const bool fEnableSecurity, int start_time)
{
    if (g_Viewer == NULL)
    {
        g_Viewer = new UnixViewer(exec_path);
    }
    g_Viewer->Initialize(share_server, password, encrypt_key, meeting_id, part_id, token, fEnableSecurity);

    if (g_Server == NULL)
        g_Server = new UnixServer((char*)exec_path);
    g_Server->SetStartTime(start_time);
    g_Server->Initialize(share_server, password, encrypt_key, meeting_id, part_id, token, fEnableSecurity);
}

void share_set_event_handler(void *user, share_event_handler _handler)
{
    g_Server->SetCallbackHandler(user, _handler);
    g_Viewer->SetCallbackHandler(user, _handler);
}

int share_enable_recording(bool enable)
{
    if (g_Server)
        return g_Server->EnableRecording(enable ? TRUE : FALSE);
    return 0;
}

int share_set_options(const char *option, const char *value)
{
    return -1;
}

int share_refresh()
{
/*    if (g_viewerListener)
        g_Viewer->Refresh();
    if (g_Server)
        g_Server->Refresh();
*/
    return 0;
}

int share_desktop()
{
    std::string null("");
    if(g_Server != NULL)
        g_Server->LaunchServer(null,true);

    return 0;
}

int share_applications(std::string &application, BOOL share_all_instances)
{
    if(g_Server != NULL)
        g_Server->LaunchServer(application, true);

    return 0;
}

int share_presentation(void * present_window, void * parent_window, bool agent_mode)
{

    return -1;
}

int share_end()
{
    if (g_Server)
        g_Server->ShutdownServer();
    return 0;
}

int share_get_start_time()
{
    return g_Server->GetStartTime();
}

int share_view(void * hParentWnd)
{
    g_fullScreen = false;
    return g_Viewer->LaunchViewer(hParentWnd, g_remoteControl, 0, 0);
}

void share_enable_fullscreen(bool enable)
{
    g_fullScreen = enable;
    g_Viewer->EnableFullScreen(enable);
}

void share_enable_control(bool enable)
{
    if( g_remoteControl != enable )
        debugf( "g_remoteControl = %d\n",enable );
    g_remoteControl = enable;
    if(g_Viewer)    // g_ViewerListener ?
        g_Viewer->EnableRemoteControl(enable);

}

bool share_is_control_enabled()
{
    return g_remoteControl;
}

void share_move_viewer(int x, int y, int width, int height)
{
    g_Viewer->MoveViewer(x, y, width, height);
}

void share_erase_background(int x, int y, int width, int height)
{
}

void share_view_end()
{
    if (g_Viewer)
        g_Viewer->ShutdownViewer(false);
    //DestroyWindow(g_viewerListener);
    g_viewerListener = 0;
}

int share_handle_event(int code)
{
    if (g_Viewer)
        return g_Viewer->HandleEvent(code);
    return 0;
}

void share_request_control()
{
}

void share_remote_mouse_move(int x, int y)
{
//    if (g_Viewer)
//        return g_Viewer->RemoteMouseMove(x, y);
}

void share_remote_button(int button, bool pressed, int x, int y)
{
//    if (g_Viewer)
//        return g_Viewer->RemoteMouseButton(button, pressed, x, y);
}

void share_remote_key(int key, bool pressed)
{
}

// IPC classes
// ToDo: replace with newer IPC code in iiclib

static bool MessageServerRunning = false;


// ::FindWindow since it's used a bunch of places, see if x11vnc or vncviewer is running, return it's IIC_id IIC_vncServer if so.
HWND FindWindow(IIC_id id, void *)
{
    const char *cmd;

    if(id == IIC_vncServer)
        cmd = "pgrep mtgshare";
    else if(id == IIC_vncViewer)
        cmd = "pgrep mtgviewer";
    else
        return 0;

    int pgrepReturn = system(cmd);
    if(pgrepReturn == 256)
        return 0;
    else
        return id;
}

/*
    MessageServer   handles callback messages from x11vnc and vncviewer
*/

void * MessageServer(void *p)
{
    bool MessageServerRun = true;            // cleared when we get message to quit

    debugf("create MsgQueue %d\n",IIC_Meeting);
    MsgQueueCreate(IIC_Meeting);             // open queue and provide my from id

    MessageServerRunning = true;

    while(MessageServerRun)
    {
		IIC_id id;
		unsigned int wmCmd,wParam;
		void *lParam;

        GetMessage(&id, &wmCmd, &wParam, &lParam);
        debugf("GetMessage from:%d wmCmd:%d wParam:%d lParam:%d\n",id,wmCmd,wParam,lParam);

        // Messages are only from vnc so call callback
        // wmCmd identifies the message type, what do I pass back?  wParam ?
        if(id == IIC_vncServer)
            g_Server->OnCallbackMsg(wParam);
        else if(id == IIC_vncViewer)
        {
            if(wmCmd == msgViewerCreated)
                g_Viewer->OnClientCreated(wParam,(long int*)lParam);
            else if(wmCmd == msgViewerDestroyed)
                g_Viewer->OnClientDestroyed(wParam,(long int*)lParam);
            else if(wmCmd == msgConnectError)
                ;
            else if(wmCmd == msgNetworkError)
                ;
            else if(wmCmd == msgObtainedControl)
                ;
            else if(wmCmd == msgCloseSession)
                ;
            else if(wmCmd == msgViewerInternalError)
                ;
            else if(wmCmd == msgGetEncryptKey)
            {
                g_Viewer->OnGetEncryptKey( );
            }
        }
        DelMessage(wmCmd, wParam, lParam);      // it will delete what it should ??? Getting 0177
        sleep_ms(25);                           // in case we get flurry of msgs, let others in
    }
    MessageServerRunning = false;
    return(0);      // exit this thread
}

static  pthread_t MsgServer_thread;

int StartMessageServer()
{   int Delay = 0;
    if(!MessageServerRunning)
    {
        int ThreadRet = pthread_create(&MsgServer_thread, NULL, MessageServer, NULL);
        if(ThreadRet == -1)  // error
        {
            debugf("ERROR creating MessageServer thread\n");
        }
        else
        {
            while(!MessageServerRunning)
            {   // seeing delay of 1-4 usually
                sleep_ms(20);
                Delay++;
            }
            debugf("MessageServerRunning, Delay %d\n",Delay);
        }
    }
    return 0;
}

