/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
// VncWrapper.cpp: implementation of the VncServer class.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32

#include <windows.h>
#include <tchar.h>
#include <string.h>
#include <time.h>
#include <stdio.h>
#include <process.h>
#include <tlhelp32.h>

#include "vncserver.h"
#include "msgwin.h"
#include "flag.h"
#include "procutil.h"

#include <log.h>

extern "C" {
#include "d3des.h"
}

volatile bool VncServer::m_fThreadShouldBeRunning = false;

int EncryptPasswd(char *passwd, char *encryptedPasswd);


// Copied from vnc source
TCHAR mutexname []     = _T("IICServer_Win32_Instance_Mutex");
const TCHAR *MENU_CLASS_NAME = _T("IICServerTrayIcon");

// VNC password encryption key
unsigned char fixedkey[8] = {23,82,107,6,35,78,88,7};

const UINT MENU_SETPASSWORD_MSG    = RegisterWindowMessageA("IICServer.Properties.SetPassword");
const UINT MENU_ADD_CLIENT_MSG     = RegisterWindowMessageA("IICServer.AddClient.Message");
const UINT MENU_REMOVE_CLIENTS_MSG = RegisterWindowMessageA("IICServer.RemoveClients.Message");

const UINT CALLBACK_MSG             = RegisterWindowMessageA("IICServer.Callback");

enum {
	CDC_SetPassword,
	CDC_SetPorts,
	CDC_Setcallbackinfo,
	CDC_ShareApplications,
	CDC_EnableTransparent,
	CDC_SetPPTHWND,
	CDC_ReduceColor,
	CDC_EnableRecording,
	CDC_AllApplicationInstances,
	CDC_SetAgentMode = 10,
	CDC_SetDebugMode = 11,
	CDC_SetNagleAlgorithm = 12,
	CDC_Close = 13,
};

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

VncServer::VncServer(const char *execPath)
{
    m_ExecPath = new TCHAR[MAX_EXEC_PATH];

    // Get the run path for this application
#ifdef SHARE_DRIVER
	_tcscpy(m_ExecPath, execPath);
#else
	::GetModuleFileName(NULL, m_ExecPath, MAX_EXEC_PATH);
#endif
	TCHAR *p = _tcsrchr(m_ExecPath, _T('\\'));
	if (p)
        *(p+1) = _T('\0');

    m_MeetingId = NULL;
    m_PartId = NULL;
    m_token = NULL;
    m_ReflectorAddress = NULL;
    m_ReflectorPassword = NULL;
    m_DisplayDevice = NULL;

    m_Callback = NULL;
    m_CallbackData = NULL;

    m_hMsgWin = NULL;
	m_hWndPPT = NULL;
	m_bEnableRecording = false;
	m_bLaunching = false;
	m_bAgentMode = false;
	m_bNagleAlgorithm = true;
	m_bEnableSecurity = false;
	m_bReducedColor = false;
	m_bEnableTransparent = true;
	m_bEnableDirectX = false;

    m_hThread   = NULL;
    m_fThreadShouldBeRunning = false;
}

VncServer::~VncServer()
{
    if (m_MeetingId)
        free((void*)m_MeetingId);
    if (m_PartId)
        free((void*)m_PartId);
    if (m_token)
        free((void*)m_token);
    if (m_ReflectorAddress)
        free((void*)m_ReflectorAddress);
    if (m_ReflectorPassword)
        free((void*)m_ReflectorPassword);
    if (m_DisplayDevice)
        free((void*)m_DisplayDevice);
}

void VncServer::SetCallbackHandler(void *user, share_event_handler handler)
{
    m_Callback = handler;
    m_CallbackData = user;
}


const TCHAR *winvncShareApps   = _T("-shareapps");
const TCHAR *winvncSharePPT    = _T("-shareppt");
const TCHAR *winvncStartTime   = _T("-starttime");
const TCHAR *winvncRunAsUserApp= _T("-run");
const TCHAR *winvncRecording   = _T("-recording");
const TCHAR *winvncAllInstances= _T("-allinstances");

#ifndef BSF_ALLOWSFW
#define BSF_ALLOWSFW 0x00000080
#endif



//*****************************************************************************
int VncServer::LaunchServer(HWND hMsgWin, LPCTSTR aAppName, BOOL aSharePowerPoint,
							 BOOL aShareAllApplicationInstances, BOOL aRestart)
{
    // Prevent re-entrancy
	Flag flag(m_bLaunching);
	if (!flag.check())
		return 0;

    if (!m_ReflectorAddress || *m_ReflectorAddress == 0)
        return SHARE_NO_SERVER;

    m_hMsgWin = hMsgWin;

	// See if server is already running
	// kill the previous instance if there is
    if (IsRunning())
    {
        ShutdownServer(true, aRestart==TRUE);
    }

	KillProcessByName(VNC_SERVER_NAME);

	// Wait 10 seconds, then signal user that we've got a stuck process
	int retry = 20;
	while (IsAppRunning(VNC_SERVER_NAME))
	{
		if (--retry)
			Sleep(500);
		else
			return SHARE_REBOOT;
	}

    // N.B. mode parameter (e.g. -shareapps) should be last
	{
		TCHAR buff[32];
		if (m_iStartTime == 0)
            m_iStartTime = time(NULL);
		_itot(m_iStartTime, buff, 10);

		if (GetOSVersion()==2)	// for 2000+, workaround bring up app to front issue
		{
			DWORD recipients = BSM_APPLICATIONS;
			BroadcastSystemMessage(BSF_ALLOWSFW, &recipients, 0, 0, 0);
		}

		// Not running, launch it.
		TCHAR vnc[MAX_EXEC_PATH];
		_tcscpy(vnc, m_ExecPath);
		_tcscat(vnc, VNC_SERVER_NAME);
		if (aSharePowerPoint)
		{
            TCHAR   cmdLine[MAX_EXEC_PATH + MAX_EXEC_PATH];
            sprintf( cmdLine, _T("-starttime %s -recording %s %s -shareppt"), buff, (m_bEnableRecording?_T("1"):_T("0")), (m_bEnableDirectX?_T("-directx":_T(""))) );

            m_serverParams._this = this;
            m_serverParams.vnc[0] = '0';
            _tcscpy( m_serverParams.vnc, vnc );
            m_serverParams.cmdLine[0] = '0';
            _tcscpy( m_serverParams.cmdLine, cmdLine );

            debugf("Share server starting thread [PPT]");

            DWORD   threadID;
            HANDLE  hThread;
            hThread = CreateThread( NULL, 0, VncServer::StartThread, (PVOID) &m_serverParams, 0, (LPDWORD) &threadID );
            if( hThread  ==  NULL )
            {
                //int err = GetLastError();
                return SHARE_FAILED_LAUNCH;
            }
            else
                m_hThread = hThread;
		}
		else if (aAppName != NULL && _tcslen(aAppName)>0)
		{
            TCHAR   cmdLine[MAX_EXEC_PATH + MAX_EXEC_PATH];
            sprintf( cmdLine, _T("-starttime %s -recording %s -allinstances %s -display %s %s -shareapps %s "), buff,
                        (m_bEnableRecording?_T("1"):_T("0")), 
                        (aShareAllApplicationInstances?_T("1"):_T("0")),
                        (m_DisplayDevice ? m_DisplayDevice : _T("default")),
                        (m_bEnableDirectX?_T("-directx":_T(""))),
                        aAppName );

            m_serverParams._this = this;
            m_serverParams.vnc[0] = '0';
            _tcscpy( m_serverParams.vnc, vnc );
            m_serverParams.cmdLine[0] = '0';
            _tcscpy( m_serverParams.cmdLine, cmdLine );

            debugf("Share server starting thread [Apps]");

            DWORD   threadID;
            HANDLE  hThread;
            hThread = CreateThread( NULL, 0, VncServer::StartThread, (PVOID) &m_serverParams, 0, (LPDWORD) &threadID );
            if( hThread  ==  NULL )
			{
				//int err = GetLastError();
				return SHARE_FAILED_LAUNCH;
            }
            else
                m_hThread = hThread;
		}
		else
		{
            TCHAR   cmdLine[MAX_EXEC_PATH + MAX_EXEC_PATH];
            sprintf( cmdLine, _T("-starttime %s -recording %s -display %s %s -run"), buff, 
                        (m_bEnableRecording?_T("1"):_T("0")), 
                        (m_DisplayDevice ? m_DisplayDevice : _T("default")),
                        (m_bEnableDirectX?_T("-directx":_T(""))) );

            m_serverParams._this = this;
            m_serverParams.vnc[0] = '0';
            _tcscpy( m_serverParams.vnc, vnc );
            m_serverParams.cmdLine[0] = '0';
            _tcscpy( m_serverParams.cmdLine, cmdLine );

            debugf("Share server starting thread [Desk]");

            DWORD   threadID;
            HANDLE  hThread;
            hThread = CreateThread( NULL, 0, VncServer::StartThread, (PVOID) &m_serverParams, 0, (LPDWORD) &threadID );
            if( hThread  ==  NULL )
            {
                //int err = GetLastError();
                return SHARE_FAILED_LAUNCH;
            }
            else
                m_hThread = hThread;
		}
	}

    return 0;
}


//*****************************************************************************
int VncServer::ShutdownServer(bool aWait, bool aRestart)
{
    if( m_hThread  !=  NULL )
    {
        debugf("Stopping share server");
        
        //  Server thread is running - Terminate then close it
        m_fThreadShouldBeRunning = false;
        m_hThread = NULL;

        HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
        if (vncMenu != NULL)
        {
			/*bool bClose = true;
			COPYDATASTRUCT command = {CDC_Close, bClose, &bClose };
			SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command);
			*/

            UINT cmd = WM_CLOSE;
            ::PostMessage(vncMenu, cmd, 0, 0L);
            Sleep(1000);

            if (aWait)
            {
                int retry = 20;
                while (IsAppRunning(VNC_SERVER_NAME))
                {
                    if (--retry)
                        Sleep(500);
                    else
                        return SHARE_REBOOT;
                }
            }
        }
    }

    return 0;
}


void VncServer::Refresh()
{
    if (m_fThreadShouldBeRunning)
    {
        debugf("Share server refresh");
        
        HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
        if (vncMenu != NULL)
        {
            UINT cmd = WM_CLOSE;
            ::PostMessage(vncMenu, cmd, 0, 0L);
        }
    }
}

//*****************************************************************************
int VncServer::ShareApplication(LPCTSTR aAppName, BOOL aShareAllApplicationInstances)
{
	if (GetOSVersion()==2)	// for 2000+, workaround bring up app to front issue
	{
		DWORD recipients = BSM_APPLICATIONS;
		BroadcastSystemMessage(BSF_ALLOWSFW, &recipients, 0, 0, 0);
	}

	HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
	if (vncMenu != NULL)
	{
		COPYDATASTRUCT command0 = {CDC_AllApplicationInstances, aShareAllApplicationInstances, &aShareAllApplicationInstances };
		SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command0);

        COPYDATASTRUCT command = {CDC_ShareApplications, _tcslen(aAppName), (void*)aAppName };
		SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command);
	}

	return 0;
}


//*****************************************************************************
int VncServer::EnableTransparent(BOOL bEnable)
{
    debugf("Share server %s transparent mode", bEnable ? "enable" : "disable");

    m_bEnableTransparent = bEnable;

	HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
	if (vncMenu != NULL)
	{
		COPYDATASTRUCT command = {CDC_EnableTransparent, bEnable, &bEnable };
		SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command);
	}

	return 0;
}

//*****************************************************************************
int VncServer::EnableDirectX(BOOL bEnable)
{
    debugf("Share server %s directx", bEnable ? "enable" : "disable");
    m_bEnableDirectX = bEnable;
	return 0;
}

bool VncServer::IsRunning( )
{
    return m_fThreadShouldBeRunning;
}

HWND VncServer::FindWindow(int aRetries)
{
	HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
	while (vncMenu == NULL && aRetries--)
    {
		Sleep(500);
		vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
	}

	return vncMenu;
}

void VncServer::SetMeetingId(const char * id)
{
	m_MeetingId = strdup(id);
}

void VncServer::SetParticipantId(const char * id)
{
	m_PartId = strdup(id);
}

void VncServer::SetToken(const char * token)
{
	m_token = strdup(token);
}

void VncServer::SetReflectorAddress(const char * address)
{
	m_ReflectorAddress = strdup(address);
}

void VncServer::SetReflectorPassword(const char * password)
{
	m_ReflectorPassword = strdup(password);
}

void VncServer::SetEncryptKey( const char *key )
{
    m_EncryptKey = strdup( key );
}

void VncServer::SetDisplayDevice( const char *display )
{
    if ( m_DisplayDevice )
        free( (void*)m_DisplayDevice );
    m_DisplayDevice = display ? strdup( display ) : NULL;
}

int VncServer::ConnectReflector()
{
	HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
	if (vncMenu == NULL)
		return -1;

	SendPassword(vncMenu, m_ReflectorPassword);

    char address[512];
	UINT port = VNC_REFLECTOR_PORT;

    strcpy(address, m_ReflectorAddress);
	char *port_str = strstr(address, "::");
	if (port_str != NULL)
	{
		port = atoi(port_str + 2);
		*port_str = '\0';
	}

	if (strlen(address) == 0)
	{
		return -1;
	}

	// Construct callback and proxy info
	struct CallbackInfo {
	  HWND callback;
	  UINT msg;
	  char proxyAuthName[64];
	  char proxyPassword[32];
	  char meetingId[65];
	  char sessionToken[128];
	  char participantId[128];
	} cbInfo;

    // We never get this info anymore; we rely on wininet and tunnelling
    const char *proxy_auth_name = "";
    const char *proxy_password = "";

	cbInfo.callback = m_hMsgWin;
	cbInfo.msg = CALLBACK_MSG;

	strncpy(cbInfo.proxyAuthName, proxy_auth_name, sizeof(cbInfo.proxyAuthName));
	strncpy(cbInfo.proxyPassword, proxy_password, sizeof(cbInfo.proxyPassword));

	strcpy(cbInfo.meetingId, m_MeetingId);
	strcpy(cbInfo.participantId, m_PartId);
	strcpy(cbInfo.sessionToken, m_token);

	debugf("Connecting to reflector at %s::%d", address, port);

	// Set callback window to receive message when vnc server has connected
	COPYDATASTRUCT command = {2, sizeof(CallbackInfo), (void*)&cbInfo };
	SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command);

	// Set reduced color (uses cbBytes as flag, pointer is ignored)
	COPYDATASTRUCT command2 = {6, m_bReducedColor, &m_bReducedColor};
	SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command2);

	// Send command to connect to reflector
	struct ClientInfo { USHORT port; char name[256]; char key[256]; };
	ClientInfo client = { port };
	strcpy(client.name, address);

	if (m_bEnableSecurity) {
		strcpy(client.key, m_EncryptKey);
	} else {
		memset(client.key, 0, sizeof(client.key));
	}
	COPYDATASTRUCT command3 = {9, sizeof(client), &client};
	SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command3);

	// Set transparent mode.   The default is true (which means transparent windows are ignored), so we only
	// send the message at startup if it should be disabled. 
	if (m_bEnableTransparent == false)
	{
        COPYDATASTRUCT command4 = {4, m_bEnableTransparent, &m_bEnableTransparent};
        SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command4);	    
	}

    SetDebugMode(TRUE);

	return 0;
}

int VncServer::SendPassword(HWND wnd, const char * aPassword)
{
	char encrypted[MAXPWLEN+1];

	EncryptPasswd((char *)aPassword, encrypted);
	encrypted[MAXPWLEN] = '\0';

	// Send password to vnc server
	COPYDATASTRUCT command = {0, MAXPWLEN, (void*)encrypted };
	SendMessage(wnd, WM_COPYDATA, (WPARAM)wnd, (LPARAM)&command);

	return 0;
}

int VncServer::SendPort(HWND wnd, UINT aRfbPort)
{
	struct PortInfo { UINT rfbPort; BOOL httpEnable; UINT httpPort; } ports = { aRfbPort, FALSE, 0 };

	// Set port password
	COPYDATASTRUCT command = {1, sizeof(PortInfo), (void*)&ports };
	SendMessage(wnd, WM_COPYDATA, (WPARAM)wnd, (LPARAM)&command);

	return 0;
}

int VncServer::SetPPTHWND(HWND hWnd, HWND hMeetingWnd)
{
    m_hWndPPT = hWnd;
    m_hMeetingWnd = hMeetingWnd;

	return 0;
}

int VncServer::EnableRecording(BOOL bEnable)
{
    debugf("Share server %s recording", bEnable ? "enable" : "disable");

	m_bEnableRecording = bEnable==TRUE;

	/*HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
	if (vncMenu != NULL)
	{
		COPYDATASTRUCT command = {CDC_EnableRecording, bEnable, &bEnable };
		SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command);
	}*/

	return 0;
}

int VncServer::SetAgentMode(BOOL bEnable)
{
    debugf("Share server %s agent mode", bEnable ? "enable" : "disable");

	m_bAgentMode = bEnable==TRUE;

	HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
	if (vncMenu != NULL)
	{
		COPYDATASTRUCT command = {CDC_SetAgentMode, bEnable, &bEnable };
		SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command);
	}

	return 0;
}

int VncServer::SetDebugMode(BOOL bEnable)
{
	HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
	if (vncMenu != NULL)
	{
		COPYDATASTRUCT command = {CDC_SetDebugMode, bEnable, &bEnable };
		SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command);
	}

	return 0;
}

int VncServer::SetNagleAlgorithm(bool bEnable)
{
	if (m_bNagleAlgorithm != bEnable)
	{
		m_bNagleAlgorithm = bEnable;

		HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
		if (vncMenu != NULL)
		{
			COPYDATASTRUCT command = {CDC_SetNagleAlgorithm, bEnable, &bEnable };
			SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command);
		}
	}

	return 0;
}

void VncServer::OnCallbackMsg(int code)
{
    if (m_Callback)
        m_Callback(m_CallbackData, code);
}


int EncryptPasswd(char *passwd, char *encryptedPasswd)
{
    int i;
    int len = strlen(passwd);

    for (i = 0; i < MAXPWLEN; i++) {
        if (i < len) {
            encryptedPasswd[i] = passwd[i];
        } else {
            encryptedPasswd[i] = 0;
        }
    }

    deskey(fixedkey, EN0);
    des((unsigned char*)encryptedPasswd, (unsigned char *)encryptedPasswd);

    return 8;
}

void VncServer::SendPPTHandle()
{
	if (m_hWndPPT != NULL)
	{
        Sleep(1000);

		HWND vncMenu = ::FindWindow(MENU_CLASS_NAME, NULL);
		if (vncMenu != NULL)
		{
            debugf("Sending PPT handle [%08X]", m_hWndPPT);
        
			struct handles { HWND hPPT; HWND hMeeting; } wnds = { m_hWndPPT, m_hMeetingWnd };
			COPYDATASTRUCT command = {CDC_SetPPTHWND, sizeof(handles), (void*)&wnds };
			SendMessage(vncMenu, WM_COPYDATA, (WPARAM)vncMenu, (LPARAM)&command);
		}
	}
}


//*****************************************************************************
DWORD WINAPI VncServer::StartThread( LPVOID cmdLine )
{
    bool    fRestarting = false;

    VncserverParams     *params;
    params = (VncserverParams *) cmdLine;

    m_fThreadShouldBeRunning = true;

    while(m_fThreadShouldBeRunning)
    {
        if (fRestarting)
        {
            errorf("Share server restarting");
            Sleep(2000);
        }
        
        debugf("Share server start: %s", params->cmdLine);
        
        HANDLE  hThreadConnect;
        DWORD   threadIDConnect;
        hThreadConnect = CreateThread( NULL, 0, VncServer::StartConnect, cmdLine, 0, (LPDWORD) &threadIDConnect );

        spawnl( _P_WAIT, params->vnc, VNC_SERVER_NAME, params->cmdLine, NULL );
        fRestarting = true;
    }
    
    debugf("Share server thread exit");
    return 0;
}


//*****************************************************************************
DWORD WINAPI VncServer::StartConnect( LPVOID cmdLine )
{
    VncserverParams     *params;
    params = (VncserverParams *) cmdLine;

    // Give process a second to start
    HWND vncMenu = params->_this->FindWindow(20);
    if (vncMenu == NULL)
    {
        errorf("Error waiting for share process to start");
        return 1;
    }

    // Connect to share server.
    if( params->_this->ConnectReflector( ) )
    {
        errorf("Error connecting to share server");
        return 1;
    }

    params->_this->SetAgentMode( params->_this->m_bAgentMode );
    params->_this->SetNagleAlgorithm( params->_this->m_bNagleAlgorithm );
    
    params->_this->SendPPTHandle();
    
    return 0;
}

#endif
