/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include "vncviewer.h"

extern HINSTANCE g_hinstance;
static bool g_InitMsgWin = false;
static VncViewer *g_VncViewer = NULL;

// vnc viewer messages
const UINT msgConnectError					= RegisterWindowMessage(_T("IICViewer.ConnectError"));
const UINT msgViewerInternalError           = RegisterWindowMessage(_T("IICViewer.InternalError"));
const UINT msgNetworkError					= RegisterWindowMessage(_T("IICViewer.NetworkError"));
const UINT msgViewerCreated					= RegisterWindowMessage(_T("IICViewer.WindowCreated"));
const UINT msgViewerDestroyed				= RegisterWindowMessage(_T("IICViewer.WindowDestroyed"));
const UINT msgObtainedControl				= RegisterWindowMessage(_T("IICViewer.ObtainedControl"));
const UINT msgCloseSession					= RegisterWindowMessage(_T("IICViewer.CloseSession"));


static LRESULT CALLBACK MainWndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
        case WM_CREATE:
            // Initialize the window.
            //LPCREATESTRUCT pCreate = (LPCREATESTRUCT)lParam;
            return 0;

        case WM_SIZE:
        {
            // Set the size and position of the window.
            int width = LOWORD(lParam);
            int height = HIWORD(lParam);
            g_VncViewer->MoveViewer(0,0,width,height);
            return 0;
        }

        case WM_PAINT:
            return DefWindowProc(hwnd, uMsg, wParam, lParam);
        
        default:
            if (uMsg == msgConnectError)
            {
                return g_VncViewer->OnConnectError(wParam, lParam);
            }
            else if (uMsg == msgViewerInternalError)
            {
                return g_VncViewer->OnViewerInternalError(wParam, lParam);
            }
            else if (uMsg == msgNetworkError)
            {
                return g_VncViewer->OnNetworkError(wParam, lParam);
            }
            else if (uMsg == msgViewerCreated)
            {
                return g_VncViewer->OnClientCreated(wParam, lParam);
            }
            else if (uMsg == msgViewerDestroyed)
            {
                return g_VncViewer->OnClientDestroyed(wParam, lParam);
            }
            else if (uMsg == msgObtainedControl)
            {
                return g_VncViewer->OnObtainedControl(wParam, lParam);
            }
            else if (uMsg == msgCloseSession)
            {
                return g_VncViewer->OnCloseSession(wParam, lParam);
            }
            else
                return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }
    return 0;
}

static BOOL InitViewerWindow()
{
    WNDCLASSEX wcx;

    // Fill in the window class structure with parameters
    // that describe the main window.

    wcx.cbSize = sizeof(wcx);          // size of structure
    wcx.style = CS_HREDRAW |
        CS_VREDRAW;  // redraw if size changes
    wcx.lpfnWndProc = MainWndProc;     // points to window procedure
    wcx.cbClsExtra = 0;                // no extra class memory
    wcx.cbWndExtra = 0;                // no extra window memory
    wcx.hInstance = g_hinstance;       // handle to instance
    wcx.hIcon = NULL;                  // predefined app. icon
    wcx.hCursor = NULL;                // predefined arrow
    wcx.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
    wcx.lpszMenuName =  NULL;          // name of menu resource
    wcx.lpszClassName = "ViewerWinClass"; // name of window class
    wcx.hIconSm = NULL;

    // Register the window class.

    return RegisterClassEx(&wcx);
}

HWND CreateViewerWindow(HWND hParent, VncViewer *viewer)
{
    HWND hwnd;

    g_VncViewer = viewer;

    if (!g_InitMsgWin)
    {
        InitViewerWindow();
        g_InitMsgWin = true;
    }

    // Create the main window.

    hwnd = CreateWindow(
        "ViewerWinClass",        // name of window class
        "Zon Viewer",            // title-bar string
        WS_CHILD|WS_VISIBLE,           // child win
        0,       // default horizontal position
        0,       // default vertical position
        CW_USEDEFAULT,       // default width
        CW_USEDEFAULT,       // default height
        (HWND) hParent,         // no owner window
        (HMENU) NULL,        // use class menu
        g_hinstance,           // handle to application instance
        (LPVOID) NULL);      // no window-creation data
    if (hwnd == NULL)
    {
        int err = GetLastError();
        printf("Create viewer window failed: %d\n", err);
    }

    return hwnd;
}

void NormalViewerWindow(HWND viewerListener, HWND parentWindow, bool setActive)
{
    /* put viewer back into meeting frame */
    LONG style = GetWindowLong(viewerListener, GWL_STYLE);
    style = (style & ~WS_POPUPWINDOW) | WS_CHILD;
    SetWindowLong(viewerListener, GWL_STYLE, style);
    SetParent(viewerListener, parentWindow);
    UpdateWindow(viewerListener);

    /* bring meeting frame to top */
    if (setActive) 
    {
        HWND frame = GetAncestor(parentWindow, GA_ROOT);
        SetWindowPos(frame, HWND_TOP, 0, 0, 0, 0, SWP_NOSIZE|SWP_NOMOVE);
        SetActiveWindow(frame);
    }
}

void FullScreenViewerWindow(HWND viewerListener)
{
    /* remove viewer window from frame and enlarge to screen */
    int cx = GetSystemMetrics(SM_CXSCREEN);
    int cy = GetSystemMetrics(SM_CYSCREEN);
    LONG style = GetWindowLong(viewerListener, GWL_STYLE);
    style = (style & ~WS_CHILD) | WS_POPUPWINDOW;
    SetWindowLong(viewerListener, GWL_STYLE, style);
    SetParent(viewerListener, NULL);
    SetWindowPos(viewerListener, HWND_TOP, 0, 0, cx, cy, 0);
    UpdateWindow(viewerListener);    
}
