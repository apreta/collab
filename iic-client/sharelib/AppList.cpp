/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include <string.h>

#include <sys/time.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>

#include "AppList.h"

//#define XLSCLIENTS      // use xlsclients = simpler but not looking good,
//  else longer method to parse xwininfo output - takes a few seconds.
//#define	PAREN_DELIMS	// finds name between ()'s, else find between ""'s.

using namespace std;

#ifdef linux
#include "util_linux.h"
#endif

#include <log.h>

Display *gpDisplay   = NULL;


double dnow( )
{
    double          t_now;
    struct timeval  now;
    gettimeofday( &now, NULL );
    t_now = now.tv_sec + ( (double) now.tv_usec/1000000. );
    return t_now;
}


//  AppList object
AppList::AppList(const char *display)
{
    // make initial list in ctor, gives user more control over when it happens.
    if ( gpDisplay  ==  NULL )
        gpDisplay = XOpenDisplay( "" );

#ifdef linux
#ifdef XLSCLIENTS
    system("xlsclients -l >xls-l.t");
#else
//    system("xwininfo -root -tree >~/.meeting/xwi.t");
    //printf(" use existing xwi.t\n");
#endif
    windows = 0;
#else  //not linux
    windows = 0;
#endif  // def linux
}

AppList::~AppList()
{
    // delete strings for windows entries
    if (windows > 0)
        for (int i=0; i<=windows; i++)
        {
            // be safe about it, even though there's no way the pointers should be null for <windows> entries
            if (xwinEntry[i].id)
                delete xwinEntry[i].id;
            if (xwinEntry[i].name)
                delete xwinEntry[i].name;
        }
    windows = 0;
}
/*
    xwininfo -root -tree        //output:
xwininfo: Window id: 0x3e (the root window) (has no name)

  Root window id: 0x3e (the root window) (has no name)
  Parent window id: 0x0 (none)
     66 children:
     0x10002c4 "<unknown>": ("<unknown>" "Gnome-panel")  202x21+125+751  +125+751
        1 child:
        0x10002c5 (has no name): ()  1x1+-1+-1  +124+750
...
     0x3482f49 "codeblocks": ("codeblocks" "Codeblocks")  276x410+230+43  +230+43
xwininfo -id 0x3482f49 | grep 'Map State'
  Map State: IsUnMapped
*/
//#define TRY_XPROP     // avoids apps being gone if minimized
//                      if defined, then look for 'Client accepts input' 'True'

void AppList::FindNamedIdsWith(char *Property, char *Value)     // just find top level window from xlsclients
{
#ifdef linux
#ifdef XLSCLIENTS
    /*  parse output of xlsclients -l, example here, I want Window and Command:
    Window 0x3400001:
      Machine:  SLED10VM
      Name:  codeblocks
      Icon Name:  codeblocks
      Command:  codeblocks
      Instance/Class:  codeblocks/Codeblocks
    */
    int i=0, retVal, numMatching=0, numWindows=0;
    char *cretVal;
    debugf("Finding ");
    string label, value;
    //string id,name;
    string buffer;      // for system() command

    ifstream in("xls-l.t");
    if (in.is_open())
    {
        while ((i < MAX_WINS) && in )
        {
            xwinEntry[i].id = new string("");   //only initialize slots I'm using
            xwinEntry[i].name = new string("");
            xwinEntry[i].Matches = false;
            label = "";
            value = "0";
            while ((label != "Window") && (in >> label))
                if (!in)
                    break;
            in >> value;
            *xwinEntry[i].id = value;
            while ((label != "Command:") && (in >> label))
                if (!in)
                    break;
            in >> value;
            *xwinEntry[i].name = value;
            //cout << i << ", " << *xwinEntry[i].id << ", " << *xwinEntry[i].name << endl;
            debugf("%d id: %s, name: %s\n",i,xwinEntry[i].id->c_str(),xwinEntry[i].name->c_str());
            i++;
        }
    }
    windows = i-1;     // last attempt to fill slot gets end of file
#else

    struct
    {
        char id[256];
        char name[256];
        char MapState[256];     // IsViewable or not
        bool Matches;           // should indicate apps with windows
    } xwin;

    double          start = dnow( );
    int             id=0, numMatching=0, i;
    int	            iActualFormat;
    int	            iRet, rc, scr;
    unsigned int    ui, ui2;
    unsigned long   ulNitems, ulBytesAfter;

    Atom            atomNET_WM_STATE, atomNET_WM_WINDOW_TYPE, atomNET_WM_WINDOW_TYPE_NORMAL, atomNET_FRAME_WINDOW;
    Atom            atomActualType, *pAtom;
    Display         *dpy;
    Status          stat;
    Window		    *list, *list2;
    Window          r, rootwin, w;
    XTextProperty   tProp, tName2;
    XWindowAttributes	xwAttr;
    XClassHint      xwClass;

    dpy = XOpenDisplay( NULL );
    scr = DefaultScreen( dpy );
    rootwin = RootWindow( dpy, scr );

    atomNET_WM_STATE    = XInternAtom( dpy, "_NET_WM_STATE", False );
    atomNET_WM_WINDOW_TYPE    = XInternAtom( dpy, "_NET_WM_WINDOW_TYPE", False );
    atomNET_WM_WINDOW_TYPE_NORMAL   = XInternAtom( dpy, "_NET_WM_WINDOW_TYPE_NORMAL", False );
    atomNET_FRAME_WINDOW    = XInternAtom( dpy, "_NET_FRAME_WINDOW", False );
    
    list = NULL;
    list2 = NULL;
    rc = XQueryTree( dpy, rootwin, &r, &w, &list, &ui );
    debugf( "Finding sharable apps - number of top-level windows = %d\n", ui );
    for ( i=0; i<(int)ui; i++ )
    {
        //  Top-level window - get attributes
        stat = XGetWindowAttributes( dpy, list[ i ], &xwAttr );
        if ( stat == 0 )
        {
//            debugf( "ERROR getting XWinAttr( ) for list[%d] - 0X%8.8x  -- SKIPPING\n", i, list[i] );
            continue;
        }

        // Class hints should be more reliable (not translated)
        stat = XGetClassHint( dpy, list[i], &xwClass );
        if (stat && (( xwClass.res_name, "gnome-main-menu") == 0 || strcasecmp(xwClass.res_name, "desktop_window") == 0 || 
                    strcasecmp(xwClass.res_name, "gnome-panel") == 0) )
        {
            continue;
        }
        
        //  Top-level window - get name
        stat = XGetWMName( dpy, list[ i ], &tProp );
        if ( stat == 0 )
        {
//            debugf( "  after XGetWMName1( 0x%8.8x ) - stat = %d  --  IT FAILED - NO NAME - NO PROBLEM\n", list[i], stat );
        }
        else if ( strstr((char *)tProp.value,"gnome-panel")  ||  strstr((char *)tProp.value,"gnome-main-menu")  ||  strstr((char *)tProp.value,"desktop_window") )
        {
//            debugf( "  after XGetWMName1( 0x%8.8x )  --  SKIPPING  --  stat = %d, *value = 0x%8.8x  ==  ***%s***,  atomEncoding=%d, format=%d, nitems=%d\n",
//                list[i], stat, tProp.value, tProp.value, tProp.encoding, tProp.format, tProp.nitems );
            continue;
        }
        else
        {
            // If we're running compwiz then the top-level window is the main application window, so take the window title and id
            // from this level.

            iRet = XGetWindowProperty( dpy, list[ i ], atomNET_WM_WINDOW_TYPE, 0, 1, False, AnyPropertyType /* atomTypeAtom */,
                                       &atomActualType, &iActualFormat, &ulNitems, &ulBytesAfter, (unsigned char **) &pAtom );
            if ( iRet  !=  0 )
            {
                continue;
            }
            else if ( atomActualType  ==  0 || *pAtom != atomNET_WM_WINDOW_TYPE_NORMAL )
            {
                continue;
            }

            iRet = XGetWindowProperty( dpy, list[ i ], atomNET_FRAME_WINDOW, 0, 1, False, AnyPropertyType /* atomTypeAtom */,
                                       &atomActualType, &iActualFormat, &ulNitems, &ulBytesAfter, (unsigned char **) &pAtom );
            if ( iRet  !=  0 )
            {
                continue;
            }
            else if ( atomActualType  ==  0 )
            {
                continue;
            }

            sprintf( xwin.id, "0x%8.8lX", list[i] );
            strcpy( xwin.name, (char *) tProp.value );
            debugf("id %d of %d, = %s name= %s\n", id, i, xwin.id, xwin.name );
            xwin.Matches = true;
            numMatching++;
            xwinEntry[id].id = new string(xwin.id);
            xwinEntry[id].name = new string(xwin.name);
            id++;

            continue;
        }

        // If we're running without compiz, then the top level windows are window manager frames and are unnamed.
        // Get the name from the children of the frame windows.
  
        //  Get list2 (containing child windows) of this top-level window
        rc = XQueryTree( dpy, list[ i ], &r, &w, &list2, &ui2 );
        if ( rc != 0  &&  list2  &&  list2[0] )
        {
            stat = XGetClassHint( dpy, list[i], &xwClass );
            if (stat && (strcasecmp( xwClass.res_name, "gnome-main-menu") == 0 || strcasecmp(xwClass.res_name, "desktop_window") == 0 || 
                        strcasecmp(xwClass.res_name, "gnome-panel") == 0) )
            {
                continue;
            }

            //  Second-level window - get name
            stat = XGetWMName( dpy, list2[ 0 ], &tName2 );
            if ( stat == 0 )
            {
//                debugf( "  after XGetWMName2( 0x%8.8x ) - stat = %d  --  IT FAILED - NO NAME - SKIPPING\n", list2[0], stat );
                continue;
            }
            else if ( strstr((char *)tName2.value,"gnome-panel")  ||  strstr((char *)tName2.value,"gnome-main-menu")  ||  strstr((char *)tName2.value,"desktop_window") )
            {
//                debugf( "  after XGetWMName2( 0x%8.8x )  --  SKIPPING  --  stat = %d, *value = 0x%8.8x  ==  ***%s***,  atomEncoding=%d, format=%d, nitems=%d\n",
//                    list2[0], stat, tName2.value, tName2.value, tName2.encoding, tName2.format, tName2.nitems );
                continue;
            }
            else
            {
//                debugf( "  after XGetWMName2( 0x%8.8x )  --  stat = %d, *value = 0x%8.8x  ==  ***%s***\n",
//                    list2[0], stat, tName2.value, tName2.value );
            }

            //  Second-level window - get _NET_WM_STATE prop value
            iRet = XGetWindowProperty( dpy, list2[ 0 ], atomNET_WM_STATE, 0, 1, False, AnyPropertyType /* atomTypeAtom */,
                                       &atomActualType, &iActualFormat, &ulNitems, &ulBytesAfter, (unsigned char **) &pAtom );
            if ( iRet  !=  0 )
            {
//                debugf( "    ERROR getting XWinProp( NET_WM_STATE ) for list2[ 0 ] - 0X%8.8x\n", i, list2[ 0 ] );
                continue;
            }
            else if ( atomActualType  ==  0 )
            {
//                debugf( "    after XWinProp( list2[0] = 0x%8.8x, _NET_WM_STATE ) - NOTHING RETURNED - SKIPPING 1 - actualType = %d (should be %d) actFormat = %d, nItems = %d, pAtom = 0x%8.8x\n",
//                    list2[0], atomActualType, atomTypeAtom, iActualFormat, ulNitems, pAtom );
                continue;
            }
            else
            {
//                debugf( "    after XWinProp( list2[0] = 0x%8.8x, NET_WM_STATE ) - actualType = %d (should be %d) actFormat = %d, nItems = %d, pAtom = 0x%8.8x == %d\n",
//                    list2[0], atomActualType, atomTypeAtom, iActualFormat, ulNitems, pAtom, *pAtom );

                sprintf( xwin.id, "0x%8.8lX", list2[0] );
                strcpy( xwin.name, (char *) tName2.value );
                debugf("id %d of %d, = %s name= %s\n", id, i, xwin.id, xwin.name );
                xwin.Matches = true;
                numMatching++;
                xwinEntry[id].id = new string(xwin.id);
                xwinEntry[id].name = new string(xwin.name);
                id++;
            }
            if ( list2 )
                XFree( list2 );
            list2 = NULL;
        }
    }

    if ( list )
        XFree( list );

    XCloseDisplay( dpy );

    windows = id;

    double  dNow = dnow( );
    debugf("%d matching, %d windows,  elapsed = %.4f\n", numMatching, ui, dNow - start );
#endif
#else   //not linux
    // now have list of viewable ones.
    debugf("%d matching\n",windows);
#endif
}

// end AppList object
