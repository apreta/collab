/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef MACOSXVIEWER_H_INCLUDED
#define MACOSXVIEWER_H_INCLUDED

#include <string>

#include "share_api.h"
//#include "../netlib/IICvncMsg.h"
#include <iiclib/ipc.h>

#define VNC_CLIENT_NAME "mtgviewer"


class MacOSXViewer
{
    std::string m_ExecPath;

    share_event_handler m_Callback;
    void *m_CallbackData;

    std::string m_ReflectorAddress;
    std::string m_ReflectorPassword;
    bool        m_fEnableSecurity;
    std::string m_EncryptKey;
    std::string m_MeetingId;
    std::string m_PartId;
    std::string m_Token;
    std::string m_command;      // command to launch viewer

    void *m_hParent;
	queue_t m_queue;
    bool m_bEnableControl;
    bool m_bRelaunch;

    bool m_FullScreen;
	
	shm_t m_DisplayHandle;
	void *m_DisplayMemory;
	int m_Width;
	int m_Height;

public:
    ServerStatus m_ViewerStatus;

    MacOSXViewer(const char *aExecPath);
    int Initialize(const char *aAddress, const char *aPassword, const char *aEncryptKey, const char *aMeetingId, const char *aPartId, const char *aToken, const bool fEnableSecurity);
    void SetCallbackHandler(void *user, share_event_handler);
    int OnClientCreated();
    int OnClientDestroyed();
	int OnDisplayFlush();
	int OnResize(int width, int height);
	int OnControlGranted(bool granted);
	int LaunchViewer(void * hParent, bool aEnableControl, bool aRestart, int aOption);
	int ReLaunchViewer(void * hParent, bool aEnableControl, bool aRestart, int aOption);
	int ShutdownViewer(bool bWait = false);
    void Refresh();
	void MoveViewer(int x, int y, int width, int height);
    void RequestRemoteControl();
	void RemoteMouseMove(int x, int y);
	void RemoteMouseButton(int button, bool pressed, int x, int y);
	void RemoteKey(int key, bool pressed);
	void EnableRemoteControl(bool aEnable);
	void EnableFullScreen(bool bEnable);
	int GetBuffer(void*& buffer, int& width, int& height);
    void ClearBuffer();
	int HandleEvent(int id);
    void OnGetEncryptKey();

protected:
    static void *RunVnc(void *p);

private:
    void log_rotate_if_needed(const char *logFN, bool fAlwaysRotate);

};


#endif // UNIXVIEWER_H_INCLUDED
