#ifndef SEARCHCONTACT_H
#define SEARCHCONTACT_H

#include <wx/wxprec.h>

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

//(*Headers(SearchContact)
#include <wx/checkbox.h>
#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
//*)

class SearchContact: public wxDialog
{
	public:

		SearchContact(wxWindow* parent,wxWindowID id = -1);
		virtual ~SearchContact();

		//(*Identifiers(SearchContact)
		//*)

	protected:

		//(*Handlers(SearchContact)
		//*)

    public:

		//(*Declarations(SearchContact)
		wxCheckBox* GlobalCheck;
		wxCheckBox* PersonalCheck;
		wxTextCtrl* Field1Edit;
		wxTextCtrl* Cond1Edit;
		wxTextCtrl* Value1Edit;
		wxTextCtrl* Field2Edit;
		wxTextCtrl* Cond2Edit;
		wxTextCtrl* Value2Edit;
		//*)

	private:

		DECLARE_EVENT_TABLE()
};

#endif
