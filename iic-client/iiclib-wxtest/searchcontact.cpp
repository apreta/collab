#include "searchcontact.h"

//(*InternalHeaders(SearchContact)
#include <wx/bitmap.h>
#include <wx/button.h>
#include <wx/font.h>
#include <wx/fontenum.h>
#include <wx/fontmap.h>
#include <wx/image.h>
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(SearchContact)
//*)

BEGIN_EVENT_TABLE(SearchContact,wxDialog)
	//(*EventTable(SearchContact)
	//*)
END_EVENT_TABLE()

SearchContact::SearchContact(wxWindow* parent,wxWindowID id)
{
	//(*Initialize(SearchContact)
	wxXmlResource::Get()->LoadObject(this,parent,_T("SearchContact"),_T("wxDialog"));
	GlobalCheck = (wxCheckBox*)FindWindow(XRCID("ID_CHECKBOX1"));
	PersonalCheck = (wxCheckBox*)FindWindow(XRCID("ID_CHECKBOX2"));
	Field1Edit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL1"));
	Cond1Edit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL2"));
	Value1Edit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL3"));
	Field2Edit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL4"));
	Cond2Edit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL5"));
	Value2Edit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL6"));
	//*)
}

SearchContact::~SearchContact()
{
	//(*Destroy(SearchContact)
	//*)
}

