#ifndef CREATEMEETING_H
#define CREATEMEETING_H

#include <wx/wxprec.h>

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

//(*Headers(CreateMeeting)
#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
//*)

class CreateMeeting: public wxDialog
{
	public:

		CreateMeeting(wxWindow* parent,wxWindowID id = -1);
		virtual ~CreateMeeting();

		//(*Identifiers(CreateMeeting)
		//*)

	protected:

		//(*Handlers(CreateMeeting)
		//*)

    public:
		//(*Declarations(CreateMeeting)
		wxTextCtrl* UserIDEdit;
		wxTextCtrl* NameEdit;
		wxTextCtrl* EmailEdit;
		wxTextCtrl* PhoneEdit;
		//*)

	private:

		DECLARE_EVENT_TABLE()
};

#endif
