#ifndef MOVEPARTICIPANT_H
#define MOVEPARTICIPANT_H

#include <wx/wxprec.h>

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

//(*Headers(MoveParticipant)
#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
//*)

class MoveParticipant: public wxDialog
{
	public:

		MoveParticipant(wxWindow* parent,wxWindowID id = -1);
		virtual ~MoveParticipant();

		//(*Identifiers(MoveParticipant)
		//*)

	protected:

		//(*Handlers(MoveParticipant)
		//*)

    public:

		//(*Declarations(MoveParticipant)
		wxTextCtrl* PartIdEdit;
		wxTextCtrl* SubIdEdit;
		//*)

	private:

		DECLARE_EVENT_TABLE()
};

#endif
