#include "meetingid.h"

//(*InternalHeaders(MeetingId)
#include <wx/bitmap.h>
#include <wx/button.h>
#include <wx/font.h>
#include <wx/fontenum.h>
#include <wx/fontmap.h>
#include <wx/image.h>
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(MeetingId)
//*)

BEGIN_EVENT_TABLE(MeetingId,wxDialog)
	//(*EventTable(MeetingId)
	//*)
END_EVENT_TABLE()

MeetingId::MeetingId(wxWindow* parent,wxWindowID id)
{
	//(*Initialize(MeetingId)
	wxXmlResource::Get()->LoadObject(this,parent,_T("MeetingId"),_T("wxDialog"));
	MeetingID = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL1"));
	StaticText2 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT2"));
	PartID = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL2"));
	//*)
}

MeetingId::~MeetingId()
{
}

