#ifndef MEETINGID_H
#define MEETINGID_H

#include <wx/wxprec.h>

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

//(*Headers(MeetingId)
#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
//*)

class MeetingId: public wxDialog
{
	public:

		MeetingId(wxWindow* parent,wxWindowID id = -1);
		virtual ~MeetingId();

		//(*Identifiers(MeetingId)
		//*)

	protected:

		//(*Handlers(MeetingId)
		//*)

    public:
		//(*Declarations(MeetingId)
		wxTextCtrl* MeetingID;
		wxStaticText* StaticText2;
		wxTextCtrl* PartID;
		//*)

	private:

		DECLARE_EVENT_TABLE()
};

#endif
