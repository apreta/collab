#ifndef CREATEPROFILE_H
#define CREATEPROFILE_H

#include <wx/wxprec.h>

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

//(*Headers(CreateProfile)
#include <wx/checkbox.h>
#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
//*)

class CreateProfile: public wxDialog
{
	public:

		CreateProfile(wxWindow* parent,wxWindowID id = -1);
		virtual ~CreateProfile();

		//(*Identifiers(CreateProfile)
		//*)

	protected:

		//(*Handlers(CreateProfile)
		//*)

    public:

		//(*Declarations(CreateProfile)
		wxTextCtrl* ProfileIdEdit;
		wxTextCtrl* NameEdit;
		wxTextCtrl* PINLenEdit;
		wxTextCtrl* MeetingIdLenEdit;
		wxTextCtrl* MaxBuddiesEdit;
		wxCheckBox* SaveCheck;
		//*)

	private:

		DECLARE_EVENT_TABLE()
};

#endif
