#include "moveparticipant.h"

//(*InternalHeaders(MoveParticipant)
#include <wx/bitmap.h>
#include <wx/button.h>
#include <wx/font.h>
#include <wx/fontenum.h>
#include <wx/fontmap.h>
#include <wx/image.h>
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(MoveParticipant)
//*)

BEGIN_EVENT_TABLE(MoveParticipant,wxDialog)
	//(*EventTable(MoveParticipant)
	//*)
END_EVENT_TABLE()

MoveParticipant::MoveParticipant(wxWindow* parent,wxWindowID id)
{
	//(*Initialize(MoveParticipant)
	wxXmlResource::Get()->LoadObject(this,parent,_T("MoveParticipant"),_T("wxDialog"));
	PartIdEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL2"));
	SubIdEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL1"));
	//*)
}

MoveParticipant::~MoveParticipant()
{
}

