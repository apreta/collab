#include "searchmeeting.h"

//(*InternalHeaders(SearchMeeting)
#include <wx/bitmap.h>
#include <wx/button.h>
#include <wx/font.h>
#include <wx/fontenum.h>
#include <wx/fontmap.h>
#include <wx/image.h>
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(SearchMeeting)
//*)

BEGIN_EVENT_TABLE(SearchMeeting,wxDialog)
	//(*EventTable(SearchMeeting)
	//*)
END_EVENT_TABLE()

SearchMeeting::SearchMeeting(wxWindow* parent,wxWindowID id)
{
	//(*Initialize(SearchMeeting)
	wxXmlResource::Get()->LoadObject(this,parent,_T("SearchMeeting"),_T("wxDialog"));
	TitleEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL1"));
	FirstEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL2"));
	StaticText3 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT3"));
	LastEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL3"));
	NumDaysEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL4"));
	//*)
}

SearchMeeting::~SearchMeeting()
{
	//(*Destroy(SearchMeeting)
	//*)
}

