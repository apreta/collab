#include "createmeeting.h"

//(*InternalHeaders(CreateMeeting)
#include <wx/bitmap.h>
#include <wx/button.h>
#include <wx/font.h>
#include <wx/fontenum.h>
#include <wx/fontmap.h>
#include <wx/image.h>
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(CreateMeeting)
//*)

BEGIN_EVENT_TABLE(CreateMeeting,wxDialog)
	//(*EventTable(CreateMeeting)
	//*)
END_EVENT_TABLE()

CreateMeeting::CreateMeeting(wxWindow* parent,wxWindowID id)
{
	//(*Initialize(CreateMeeting)
	wxXmlResource::Get()->LoadObject(this,parent,_T("CreateMeeting"),_T("wxDialog"));
	UserIDEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL4"));
	NameEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL1"));
	EmailEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL2"));
	PhoneEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL3"));
	//*)
}

CreateMeeting::~CreateMeeting()
{
}

