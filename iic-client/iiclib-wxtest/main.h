#ifndef MAIN_H
#define MAIN_H

#include "wx/defs.h"
#include "wx/event.h"

#include "app.h"
#include "session_api.h"
#include "controller.h"
#include "addressbk.h"
#include "schedule.h"
#include "events.h"
#include "mainpanel.h"
#include "options.h"


class MyFrame: public wxFrame
{
    MainPanel *panel;
    session_t session;
    controller_t controller;
    addressbk_t book;
    schedule_t sched;
    directory_t cab;
    options_t options;
    group_t group;

    wxString screenName;
    wxString userId;
    wxString displayName;
    meeting_details_t instantMeeting;

	public:
		MyFrame(wxFrame *frame, const wxString& title);
		~MyFrame();
	private:
		void OnQuit(wxCommandEvent& event);
		void OnAbout(wxCommandEvent& event);
		void OnSignOn(wxCommandEvent& event);
		void OnStartMeeting(wxCommandEvent& event);
		void OnJoinMeeting(wxCommandEvent& event);
		void OnEndMeeting(wxCommandEvent& event);
		void OnScheduleMeeting(wxCommandEvent& event);
		void OnRemoveMeeting(wxCommandEvent& event);
		void OnCreateMeeting(wxCommandEvent& event);
        void OnAddParticipant(wxCommandEvent& event);
        void OnAddSubmeeting(wxCommandEvent& event);
        void OnMoveParticipant(wxCommandEvent& event);
        void OnMeetingOptions(wxCommandEvent& event);
        void OnMeetMany(wxCommandEvent& event);
        void OnSearchMeetings(wxCommandEvent& event);
        void OnSearchContacts(wxCommandEvent& event);
        void OnAddContact(wxCommandEvent& event);
        void OnAddProfile(wxCommandEvent& event);
        void OnAddGroup(wxCommandEvent& event);
		void OnConnectEvent(wxCommandEvent& event);
		void OnSessionError(SessionErrorEvent& event);
        void OnRegisteredEvent(wxCommandEvent& event);
        void OnAuthEvent(wxCommandEvent& event);
        void OnScheduleFetched(wxCommandEvent& event);
        void OnScheduleUpdated(wxCommandEvent& event);
        void OnAddressUpdated(wxCommandEvent& event);
        void OnMeetingFetched(wxCommandEvent& event);
        void OnDirectoryFetched(wxCommandEvent& event);
        void OnMeetingEvent(MeetingEvent& event);
		DECLARE_EVENT_TABLE();

		static void on_error(void *, int err, const char *msg, int code, const char *id, const char *resource);
		static void on_connected(void *);
		static void on_controller_registered(void *);
		static void on_meeting_event(void *u, meeting_event_t evt);
		static void on_addressbk_auth(void *u);
		static void on_schedule_fetched(void *u, const char *opid, meeting_list_t list);
        static void on_meeting_fetched(void *u, const char *opid, meeting_details_t mtg);
        static void on_directory_fetched(void *u, const char *opid, directory_t dir);
		static void on_schedule_updated(void *u, const char *opid, int status);
		static void on_address_updated(void *u, const char *opid, const char *itemid);
		static void on_meet_many(void *u, meeting_details_t mtg, int options);

        void DumpMeeting(meeting_details_t mtg);

};

#endif // MAIN_H
