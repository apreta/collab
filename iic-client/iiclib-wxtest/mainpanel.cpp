#include "mainpanel.h"

#include <map>

//(*InternalHeaders(MainPanel)
#include <wx/bitmap.h>
#include <wx/font.h>
#include <wx/fontenum.h>
#include <wx/fontmap.h>
#include <wx/image.h>
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(MainPanel)
//*)

BEGIN_EVENT_TABLE(MainPanel,wxPanel)
	//(*EventTable(MainPanel)
	//*)
END_EVENT_TABLE()

MainPanel::MainPanel(wxWindow* parent,wxWindowID id)
{
	//(*Initialize(MainPanel)
	wxXmlResource::Get()->LoadObject(this,parent,_T("MainPanel"),_T("wxPanel"));
	tree = (wxTreeListCtrl*)FindWindow(XRCID("ID_TREELISTCTRL1"));
	text = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL1"));
	//*)
    tree->AddColumn(_("Participant"));
    tree->AddColumn(_("Status"));
}

MainPanel::~MainPanel()
{
}

// Quick wrapper to make char * work in STL container
class cstring
{
    const char *value;

public:
    cstring(const char *s) : value(s)
    {
    }
    cstring(const cstring& c) : value(c.value)
    {
    }

    cstring& operator=(const cstring& rhs)
    {
        value = rhs.value;
        return *this;
    }

    bool operator<(const cstring& rhs) const
    {
        return (strcmp(value,rhs.value) < 0);
    }

    bool operator==(const cstring& rhs) const
    {
        return (strcmp(value,rhs.value) == 0);
    }
};

typedef std::map<cstring,wxTreeItemId> ItemMap;


void MainPanel::BuildTree(meeting_t meeting)
{
    ItemMap map;

    tree->DeleteRoot();

    wxTreeItemId root = tree->AddRoot(_T("root"));

    meeting_lock(meeting);

    submeeting_t sm;
    participant_t p;

    for (sm = meeting_next_submeeting(meeting,NULL); sm != NULL; sm = meeting_next_submeeting(meeting,sm))
    {
        wxString title = wxString(sm->title, wxConvUTF8) + _T("[") +  wxString(sm->sub_id, wxConvUTF8) + _T("]");
        wxTreeItemId id = tree->AppendItem(root, title);
        map.insert(ItemMap::value_type(sm->sub_id, id));
    }

    for (p = meeting_next_participant(meeting,NULL); p != NULL; p = meeting_next_participant(meeting,p))
    {
        ItemMap::iterator iter = map.find(p->sub_id);
        if (iter != map.end())
        {
            wxString label = wxString(p->name, wxConvUTF8) + _T("[") + wxString(p->part_id, wxConvUTF8) + _T("]");
            wxTreeItemId id = (*iter).second;
            tree->AppendItem(id, label);
        }
    }

    tree->ExpandAll(root);

    meeting_unlock(meeting);
}

