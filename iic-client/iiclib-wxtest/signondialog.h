#ifndef SIGNONDIALOG_H
#define SIGNONDIALOG_H

#include <wx/wxprec.h>

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

//(*Headers(SignOnDialog)
#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
//*)

class SignOnDialog: public wxDialog
{
	public:

		SignOnDialog(wxWindow* parent,wxWindowID id = -1);
		virtual ~SignOnDialog();

		//(*Identifiers(SignOnDialog)
		//*)

	protected:

		//(*Handlers(SignOnDialog)
		//*)

   public:

		//(*Declarations(SignOnDialog)
		wxTextCtrl* Screen;
		wxTextCtrl* Password;
		//*)

	private:

		DECLARE_EVENT_TABLE()
};

#endif
