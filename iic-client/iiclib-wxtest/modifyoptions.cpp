#include "modifyoptions.h"

//(*InternalHeaders(ModifyOptions)
#include <wx/bitmap.h>
#include <wx/button.h>
#include <wx/font.h>
#include <wx/fontenum.h>
#include <wx/fontmap.h>
#include <wx/image.h>
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(ModifyOptions)
//*)

BEGIN_EVENT_TABLE(ModifyOptions,wxDialog)
	//(*EventTable(ModifyOptions)
	//*)
END_EVENT_TABLE()

ModifyOptions::ModifyOptions(wxWindow* parent,wxWindowID id)
{
	//(*Initialize(ModifyOptions)
	wxXmlResource::Get()->LoadObject(this,parent,_T("ModifyOptions"),_T("wxDialog"));
	MaskEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL1"));
	ValueEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL2"));
	//*)
}

ModifyOptions::~ModifyOptions()
{
}

