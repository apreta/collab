#ifndef EVENTS_H_INCLUDED
#define EVENTS_H_INCLUDED

#include "wx/defs.h"
#include "wx/event.h"

// Custom event types
DECLARE_LOCAL_EVENT_TYPE(wxEVT_SESSION_ERROR, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_MEETING_EVENT, -1)

// Event types using wxCommandEvent
DECLARE_LOCAL_EVENT_TYPE(wxEVT_SESSION_CONNECTED, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_CONTROLLER_REGISTERED, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_ADDRESSBK_AUTH, -1)

DECLARE_LOCAL_EVENT_TYPE(wxEVT_SCHEDULE_FETCHED, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_MEETING_FETCHED, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_DIRECTORY_FETCHED, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_SCHEDULE_UPDATED, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_ADDRESS_UPDATED, -1)

// Custom wxEvent for Session Error.
class SessionErrorEvent : public wxEvent
{
    int m_error;
    wxString m_message;
    wxString m_rpcId;

public:
    SessionErrorEvent(int id, int err, const char *msg, const char *rpcid) :
        wxEvent(id,wxEVT_SESSION_ERROR), m_error(err), m_message(msg, wxConvUTF8), m_rpcId(rpcid, wxConvUTF8)
        {
        }
    SessionErrorEvent(const SessionErrorEvent& copy) :
        wxEvent(copy), m_error(copy.m_error), m_message(copy.m_message), m_rpcId(copy.m_rpcId)
        {
        }

    SessionErrorEvent * Clone() const
        {
            return new SessionErrorEvent(*this);
        }

    int GetErrorCode() const            { return m_error; }
    const wxString& GetMessage() const  { return m_message; }
    const wxString& GetRpcId() const    { return m_rpcId; }
};

typedef void (wxEvtHandler::*SessionErrorEventFunction)(SessionErrorEvent&);

#define EVT_SESSION_ERROR(id, fn) \
    DECLARE_EVENT_TABLE_ENTRY(wxEVT_SESSION_ERROR, id, wxID_ANY, (wxObjectEventFunction) (wxEventFunction) \
        wxStaticCastEvent( SessionErrorEventFunction, & fn ), (wxObject *) NULL ),


class MeetingEvent : public wxEvent
{
    int m_event;
    wxString m_meeting_id;
    wxString m_sub_id;
    wxString m_part_id;

public:
    MeetingEvent(int id, int event, const char *mid, const char *subid, const char *partid) :
        wxEvent(id,wxEVT_MEETING_EVENT), m_event(event), m_meeting_id(mid, wxConvUTF8),
                m_sub_id(subid, wxConvUTF8), m_part_id(partid, wxConvUTF8)
        {
        }
    MeetingEvent(const MeetingEvent& copy) :
        wxEvent(copy),  m_event(copy.m_event), m_meeting_id(copy.m_meeting_id), m_sub_id(copy.m_sub_id), m_part_id(copy.m_part_id)
        {
        }

    MeetingEvent * Clone() const
        {
            return new MeetingEvent(*this);
        }

    int GetEvent()                      { return m_event; }
    const wxString& GetMeetingId()      { return m_meeting_id; }
    const wxString& GetSubId()          { return m_sub_id; }
    const wxString& GetParticipantId()  { return m_part_id; }
};

typedef void (wxEvtHandler::*MeetingEventFunction)(MeetingEvent&);

#define EVT_MEETING_EVENT(id, fn) \
    DECLARE_EVENT_TABLE_ENTRY(wxEVT_MEETING_EVENT, id, wxID_ANY, (wxObjectEventFunction) (wxEventFunction) \
        wxStaticCastEvent( MeetingEventFunction, & fn ), (wxObject *) NULL ),


#endif // EVENTS_H_INCLUDED
