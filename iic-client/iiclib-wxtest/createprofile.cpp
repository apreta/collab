#include "createprofile.h"

//(*InternalHeaders(CreateProfile)
#include <wx/bitmap.h>
#include <wx/button.h>
#include <wx/font.h>
#include <wx/fontenum.h>
#include <wx/fontmap.h>
#include <wx/image.h>
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(CreateProfile)
//*)

BEGIN_EVENT_TABLE(CreateProfile,wxDialog)
	//(*EventTable(CreateProfile)
	//*)
END_EVENT_TABLE()

CreateProfile::CreateProfile(wxWindow* parent,wxWindowID id)
{
	//(*Initialize(CreateProfile)
	wxXmlResource::Get()->LoadObject(this,parent,_T("CreateProfile"),_T("wxDialog"));
	ProfileIdEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL4"));
	NameEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL5"));
	PINLenEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL1"));
	MeetingIdLenEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL2"));
	MaxBuddiesEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL3"));
	SaveCheck = (wxCheckBox*)FindWindow(XRCID("ID_CHECKBOX1"));
	//*)
}

CreateProfile::~CreateProfile()
{
	//(*Destroy(CreateProfile)
	//*)
}

