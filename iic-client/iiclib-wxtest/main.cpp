#include "main.h"

#include "signondialog.h"
#include "meetingid.h"
#include "createmeeting.h"
#include "addparticipant.h"
#include "moveparticipant.h"
#include "modifyoptions.h"
#include "searchmeeting.h"
#include "searchcontact.h"
#include "createcontact.h"
#include "createprofile.h"
#include "creategroup.h"

#include "../../services/common/common.h"

#include <wx/stdpaths.h>

// RPC request messages
struct rpc_method_st
{
    const char * method;
    session_rpc_request_handler request_handler;
};
extern struct rpc_method_st c_method_list[];


//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

int idMenuQuit = wxNewId();
int idMenuAbout = wxNewId();
int idMenuSignOn = wxNewId();
int idMenuStartMeeting = wxNewId();
int idMenuJoinMeeting = wxNewId();
int idMenuEndMeeting = wxNewId();
int idMenuSchedule = wxNewId();
int idMenuRemoveMeeting = wxNewId();
int idMenuRefresh = wxNewId();
int idMenuCreateMeeting = wxNewId();
int idMenuAddParticipant = wxNewId();
int idMenuAddSubmeeting = wxNewId();
int idMenuMoveParticipant = wxNewId();
int idMenuMeetingOptions = wxNewId();
int idMenuMeetMany = wxNewId();
int idMenuSearchMeetings = wxNewId();
int idMenuSearchContacts = wxNewId();
int idMenuAddContact = wxNewId();
int idMenuAddProfile = wxNewId();
int idMenuAddGroup = wxNewId();
int idConnectEvent = wxNewId();
int idSessionError = wxNewId();
int idControllerRegistered = wxNewId();
int idAddressBkAuth = wxNewId();
int idScheduleFetched = wxNewId();
int idDirectoryFetched = wxNewId();
int idMeetingFetched = wxNewId();
int idMeetingEvent = wxNewId();
int idScheduleUpdated = wxNewId();
int idAddressUpdated = wxNewId();

BEGIN_EVENT_TABLE(MyFrame, wxFrame)
    EVT_MENU(idMenuQuit, MyFrame::OnQuit)
    EVT_MENU(idMenuAbout, MyFrame::OnAbout)
    EVT_MENU(idMenuSignOn, MyFrame::OnSignOn)
    EVT_MENU(idMenuStartMeeting, MyFrame::OnStartMeeting)
    EVT_MENU(idMenuJoinMeeting, MyFrame::OnJoinMeeting)
    EVT_MENU(idMenuEndMeeting, MyFrame::OnEndMeeting)
    EVT_MENU(idMenuSchedule, MyFrame::OnScheduleMeeting)
    EVT_MENU(idMenuRemoveMeeting, MyFrame::OnRemoveMeeting)
    EVT_MENU(idMenuCreateMeeting, MyFrame::OnCreateMeeting)
    EVT_MENU(idMenuAddParticipant, MyFrame::OnAddParticipant)
    EVT_MENU(idMenuAddSubmeeting, MyFrame::OnAddSubmeeting)
    EVT_MENU(idMenuMoveParticipant, MyFrame::OnMoveParticipant)
    EVT_MENU(idMenuMeetingOptions, MyFrame::OnMeetingOptions)
    EVT_MENU(idMenuMeetMany, MyFrame::OnMeetMany)
    EVT_MENU(idMenuSearchMeetings, MyFrame::OnSearchMeetings)
    EVT_MENU(idMenuSearchContacts, MyFrame::OnSearchContacts)
    EVT_MENU(idMenuAddContact, MyFrame::OnAddContact)
    EVT_MENU(idMenuAddProfile, MyFrame::OnAddProfile)
    EVT_MENU(idMenuAddGroup, MyFrame::OnAddGroup)
    EVT_COMMAND(idConnectEvent, wxEVT_SESSION_CONNECTED, MyFrame::OnConnectEvent)
    EVT_COMMAND(idControllerRegistered, wxEVT_CONTROLLER_REGISTERED, MyFrame::OnRegisteredEvent)
    EVT_COMMAND(idAddressBkAuth, wxEVT_ADDRESSBK_AUTH, MyFrame::OnAuthEvent)
    EVT_COMMAND(idScheduleFetched, wxEVT_SCHEDULE_FETCHED, MyFrame::OnScheduleFetched)
    EVT_COMMAND(idMeetingFetched, wxEVT_MEETING_FETCHED, MyFrame::OnMeetingFetched)
    EVT_COMMAND(idDirectoryFetched, wxEVT_DIRECTORY_FETCHED, MyFrame::OnDirectoryFetched)
    EVT_COMMAND(idScheduleUpdated, wxEVT_SCHEDULE_UPDATED, MyFrame::OnScheduleUpdated)
    EVT_COMMAND(idAddressUpdated, wxEVT_ADDRESS_UPDATED, MyFrame::OnAddressUpdated)
    EVT_SESSION_ERROR(idSessionError, MyFrame::OnSessionError)
    EVT_MEETING_EVENT(idMeetingEvent, MyFrame::OnMeetingEvent)
END_EVENT_TABLE()

MyFrame::MyFrame(wxFrame *frame, const wxString& title)
    : wxFrame(frame, -1, title)
{
    session = NULL;
    controller = NULL;

    wxString userDir = wxStandardPaths::Get().GetUserDataDir();

    session_initialize(userDir.mb_str());

    panel = new MainPanel(this);
//    text = new wxTextCtrl(this, -1, wxT(""), wxDefaultPosition, wxDefaultSize, wxTE_MULTILINE);

#if wxUSE_MENUS
    // create a menu bar
    wxMenuBar* mbar = new wxMenuBar();
    wxMenu* fileMenu = new wxMenu(_T(""));
    fileMenu->Append(idMenuSignOn, _("&Sign On"), _("Sign on"));
    fileMenu->Append(idMenuStartMeeting, _("Start &Meeting"), _("Start meeting"));
    fileMenu->Append(idMenuJoinMeeting, _("&Join Meeting"), _("Join meeting"));
    fileMenu->Append(idMenuEndMeeting, _("&End Meeting"), _("End meeting"));
    fileMenu->Append(idMenuSchedule, _("&Schedule Meeting"), _("Schedule meeting"));
    fileMenu->Append(idMenuRemoveMeeting, _("&Remove Meeting"), _("Remove meeting"));
    fileMenu->Append(idMenuAddParticipant, _("Add Participant"), _("Add participant"));
    fileMenu->Append(idMenuCreateMeeting, _("&Create Meeting"), _("Create meeting"));
    fileMenu->Append(idMenuAddSubmeeting, _("Add Submeeting"), _("Add submeeting"));
    fileMenu->Append(idMenuMoveParticipant, _("Move Participant"), _("Move participant"));
    fileMenu->Append(idMenuMeetingOptions, _("Meeting Options"), _("Meeting options"));
    fileMenu->Append(idMenuMeetMany, _("Meet Many"), _("Meet many"));
    fileMenu->Append(idMenuSearchMeetings, _("Search Meetings"), _("Search meetings"));
    fileMenu->Append(idMenuSearchContacts, _("Search Contacts"), _("Search contacts"));
    fileMenu->Append(idMenuAddContact, _("Add Contact"), _("Add contact"));
    fileMenu->Append(idMenuAddProfile, _("Add Profile"), _("Add profile"));
    fileMenu->Append(idMenuAddGroup, _("Add Group"), _("Add group"));
    fileMenu->Append(idMenuRefresh, _("&Refresh"), _("Refresh"));
    fileMenu->Append(idMenuQuit, _("&Quit\tAlt-F4"), _("Quit the application"));
    mbar->Append(fileMenu, _("&File"));

    wxMenu* helpMenu = new wxMenu(_T(""));
    helpMenu->Append(idMenuAbout, _("&About\tF1"), _("Show info about this application"));
    mbar->Append(helpMenu, _("&Help"));

    SetMenuBar(mbar);
#endif // wxUSE_MENUS

#if wxUSE_STATUSBAR
    // create a status bar with some information about the used wxWidgets version
    CreateStatusBar(2);
    SetStatusText(_("Hello Code::Blocks user !"),0);
    SetStatusText(wxbuildinfo(short_f),1);
#endif // wxUSE_STATUSBAR
}

MyFrame::~MyFrame()
{
}

void MyFrame::OnQuit(wxCommandEvent& event)
{
    Close();
}

void MyFrame::OnAbout(wxCommandEvent& event)
{
    wxString msg = wxbuildinfo(long_f);
    wxMessageBox(msg, _("Welcome to..."));
}

void MyFrame::OnConnectEvent(wxCommandEvent& event)
{
    panel->text->AppendText(_("Connected.\n"));

    addressbk_authenticate(book, screenName.mb_str(wxConvUTF8), "");
}

void MyFrame::OnSessionError(SessionErrorEvent& event)
{
    panel->text->AppendText(_("Session error: ") + event.GetMessage() + _T("\n"));
}

void MyFrame::OnAuthEvent(wxCommandEvent& event)
{
    panel->text->AppendText(_("Authenticated.\n"));

    auth_info_t info = addressbk_get_auth_info(book);
    profile_t prof = addressbk_get_profile(book);

    displayName = wxString(info->first, wxConvUTF8),  + _T(" ") + wxString(info->last, wxConvUTF8);

    panel->text->AppendText( wxString::Format(_T("  Enabled features: %u\n"), prof->enabled_features) );
    panel->text->AppendText( wxString::Format(_T("  PIN length: %d\n"), prof->pin_length) );

    addressbk_fetch_profiles(book, "profiles");

    controller_register(controller, info, FALSE);

    options_set_system(options, "lastuser", screenName.mb_str(wxConvUTF8));
    options_write(options);
}

void MyFrame::OnRegisteredEvent(wxCommandEvent& event)
{
    panel->text->AppendText(_("Registered.\n"));

    schedule_set_authentication(sched, addressbk_get_auth_info(book));

    schedule_find_my_meetings(sched, "mymeetings");
//    schedule_find_public_meetings(sched, "pubmeetings");
    addressbk_fetch_contacts(book, "cab", dtGlobal);
    addressbk_fetch_contacts(book, "pab", dtPersonal);
}

void MyFrame::OnScheduleFetched(wxCommandEvent& event)
{
    meeting_list_t list = (meeting_list_t)event.GetClientData();
    meeting_info_t info;
    meeting_iterator_t iter;

    if (event.GetString() == _T("mymeetings"))
    {
        panel->text->AppendText(_("My Meetings:\n"));
        schedule_find_meeting_details(sched, "instant", schedule_get_my_instant_meeting(sched));
//        schedule_find_meeting_details(sched, "normal", "600576");
        addressbk_get_recording_list(this->book, "recordings", schedule_get_my_instant_meeting(sched));
    }
    else if (event.GetString().Left(6) == _T("search"))
    {
        panel->text->AppendText(_T("Search Results:\n"));
    }
    else
        panel->text->AppendText(_("Public Meetings:\n"));

    iter = meeting_list_iterate(list);
    while ((info = meeting_list_next(list, iter)) != NULL)
    {
        wxString id(info->meeting_id, wxConvUTF8);
        wxString title(info->title, wxConvUTF8);
        panel->text->AppendText(_T("  ") + id + _T("  ") + title + _T("\n"));
    }

    meeting_list_destroy(list);
}

void MyFrame::OnScheduleUpdated(wxCommandEvent& event)
{
    panel->text->AppendText(_("Meeting scheduled\n"));
}

void MyFrame::OnAddressUpdated(wxCommandEvent& event)
{
    panel->text->AppendText(wxString::Format(_("Contact updated: %s\n"), event.GetString().c_str()));

    wxString opid = event.GetString();
    if (opid.Left(7) == _T("addgrp:"))
    {
        wxString memberid = opid.Mid(7);

        addressbk_group_add_member(book, "addmem", group, memberid.mb_str());
    }
}

void MyFrame::OnMeetingFetched(wxCommandEvent& event)
{
    meeting_details_t mtg = (meeting_details_t)event.GetClientData();
    if (event.GetString() == _T("instant"))
        instantMeeting = mtg;
    DumpMeeting(mtg);
}

void MyFrame::OnDirectoryFetched(wxCommandEvent& event)
{
    directory_t dir = (directory_t)event.GetClientData();
    contact_iterator_t iter;
    contact_t cnt;

    if (event.GetString().Left(8) == _T("contacts"))
        panel->text->AppendText(_T("Search results:\n"));
    else if (dir->type == dtGlobal)
        panel->text->AppendText(_("CAB fetched:\n"));
    else
        panel->text->AppendText(_("PAB fechted:\n"));

    iter = directory_contact_iterate(dir);

    while ((cnt = directory_contact_next(dir, iter)) != NULL)
    {
        panel->text->AppendText(_T("  ") + wxString(cnt->userid, wxConvUTF8) + _T(" ") + wxString(cnt->first, wxConvUTF8) + _T(" ") + wxString(cnt->last, wxConvUTF8) + _T(", ") +
                        wxString(cnt->email, wxConvUTF8) + _T("\n"));

    }

    int num_all = directory_group_get_size(dir->all_group);
    if (num_all > 0)
    {
        const char *userid = directory_group_get_member(dir->all_group, 0);
        //cnt = directory_find_contact(dir, userid);
        cnt = directory_find_contact_by_field(dir, "userid", userid);

        panel->text->AppendText( wxString::Format(_("%d contacts in all group, first is %s %s\n"),
                        num_all, wxString(cnt->first, wxConvUTF8).c_str(), wxString(cnt->last, wxConvUTF8).c_str()) );

    }
    else
    {
        panel->text->AppendText( _("No contacts in all group\n"));
    }
}


void MyFrame::OnMeetingEvent(MeetingEvent& event)
{
    //    panel->text->AppendText(_("Meeting event: ") + wxString::Format(_T("%d"),event.GetEvent()) + _T("\n"));
    int     iEv;
    iEv = event.GetEvent( );
    wxString    strT( c_method_list[ iEv+1 ].method, wxConvUTF8 );
    panel->text->AppendText(_("Meeting event: ") + wxString::Format(_T("%d = "),iEv) + strT + _T("\n"));
    static BOOL once = true;

    meeting_t mtg = controller_find_meeting(controller, event.GetMeetingId().mb_str(wxConvUTF8));
    if (mtg != NULL)
    {
        meeting_lock(mtg);
        panel->BuildTree(mtg);

        participant_t part = meeting_next_participant(mtg, NULL);

        if (once && part)
        {
            meeting_details_t details = controller_get_meeting_details(controller, mtg, 1, &part);
            DumpMeeting(details);
            meeting_details_destroy(details);
            once = false;
        }

        meeting_unlock(mtg);
    }
}

void MyFrame::OnSignOn(wxCommandEvent& event)
{
    SignOnDialog dlg(this);
    int res = dlg.ShowModal();
    if (res == wxID_OK)
    {
        screenName = dlg.Screen->GetValue();
        wxString pass = dlg.Password->GetValue();

        wxString sysDir = wxStandardPaths::Get().GetDataDir();
        wxString userDir = wxStandardPaths::Get().GetUserDataDir();

        options = options_create("zon", screenName.mb_str(wxConvUTF8), userDir.mb_str(wxConvUTF8), sysDir.mb_str(wxConvUTF8));
        if (options_read(options))
        {
            wxMessageBox(_("Unable to read application preferences file"), _("Error!"), wxICON_ERROR);
            return;
        }

        wxString server(options_get_system(options, "server", "unknown"), wxConvUTF8);

        session = session_create(server.mb_str(wxConvUTF8), screenName.mb_str(wxConvUTF8), pass.mb_str(wxConvUTF8), "wx",
                                        FALSE, "", FALSE);
        session_set_connected_handler(session, this, on_connected);
        session_set_error_handler(session, this, on_error);
        session_connect(session, FALSE);

        controller = controller_create(session);
        controller_set_registered_handler(controller, this, on_controller_registered);
        controller_set_meeting_event_handler(controller, this, on_meeting_event);
        controller_set_get_params_handler(controller, this, on_meet_many);

        book = addressbk_create(session);
        addressbk_set_auth_handler(book, this, on_addressbk_auth);
        addressbk_set_directory_fetched_handler(book, this, on_directory_fetched);
        addressbk_set_updated_handler(book, this, on_address_updated);

        const char *fields[] = { "userid", "first", "last", "email" };

        addressbk_set_field_list(book, fields, 4);

        sched = schedule_create(session);
        schedule_set_list_handler(sched, this, on_schedule_fetched);
        schedule_set_meeting_handler(sched, this, on_meeting_fetched);
        schedule_set_updated_handler(sched, this, on_schedule_updated);
    }
}

void MyFrame::OnStartMeeting(wxCommandEvent& event)
{
    MeetingId dlg(this);
    int res = dlg.ShowModal();
    if (res == wxID_OK)
    {
        wxString meetingid = dlg.MeetingID->GetValue();
        controller_meeting_start(controller, meetingid.mb_str(wxConvUTF8));
    }
}

void MyFrame::OnJoinMeeting(wxCommandEvent& event)
{
    MeetingId dlg(this);
    int res = dlg.ShowModal();
    if (res == wxID_OK)
    {
        wxString meetingid = dlg.MeetingID->GetValue();
        wxString partid = dlg.PartID->GetValue();
        controller_meeting_join(controller, meetingid.mb_str(wxConvUTF8), partid.mb_str(wxConvUTF8), "");
    }
}

void MyFrame::OnEndMeeting(wxCommandEvent& event)
{
    MeetingId dlg(this);
    int res = dlg.ShowModal();
    if (res == wxID_OK)
    {
        wxString meetingid = dlg.MeetingID->GetValue();
        controller_meeting_end(controller, meetingid.mb_str(wxConvUTF8));
    }
}

void MyFrame::OnScheduleMeeting(wxCommandEvent& event)
{
    CreateMeeting dlg(this);

    if (dlg.ShowModal() == wxID_OK)
    {
        wxString userid = dlg.UserIDEdit->GetValue();
        wxString name = dlg.NameEdit->GetValue();
        wxString email = dlg.EmailEdit->GetValue();
        wxString phone = dlg.PhoneEdit->GetValue();

        meeting_details_t mtg = meeting_details_new(this->sched);
        invitee_t invt = meeting_details_add(mtg);
        invt->user_id = meeting_details_string(mtg, userid.mb_str(wxConvUTF8));
        invt->name = meeting_details_string(mtg, name.mb_str(wxConvUTF8));
        invt->email = meeting_details_string(mtg, email.mb_str(wxConvUTF8));
        invt->phone = meeting_details_string(mtg, phone.mb_str(wxConvUTF8));

        schedule_update_meeting(sched, "create", mtg, SendEmailAll);

        meeting_details_destroy(mtg);
    }
}

void MyFrame::OnRemoveMeeting(wxCommandEvent& event)
{
    MeetingId dlg(this);
    int res = dlg.ShowModal();
    if (res == wxID_OK)
    {
        wxString meetingid = dlg.MeetingID->GetValue();

        schedule_remove_meeting(sched, "remove", meetingid.mb_str(wxConvUTF8));
    }
}

void MyFrame::OnCreateMeeting(wxCommandEvent& event)
{
    CreateMeeting dlg(this);

    if (dlg.ShowModal() == wxID_OK)
    {
        wxString userid = dlg.UserIDEdit->GetValue();
        wxString name = dlg.NameEdit->GetValue();
        wxString email = dlg.EmailEdit->GetValue();
        wxString phone = dlg.PhoneEdit->GetValue();

        meeting_details_t mtg = meeting_details_copy(sched, instantMeeting);

        invitee_t invt = meeting_details_add(mtg);
        invt->user_id = meeting_details_string(mtg, userid.mb_str(wxConvUTF8));
        invt->name = meeting_details_string(mtg, name.mb_str(wxConvUTF8));
        invt->email = meeting_details_string(mtg, email.mb_str(wxConvUTF8));
        invt->phone = meeting_details_string(mtg, phone.mb_str(wxConvUTF8));

        controller_meeting_create(controller, mtg);

        meeting_details_destroy(mtg);
    }
}

void MyFrame::OnAddParticipant(wxCommandEvent& event)
{
    AddParticipant dlg(this);

    if (dlg.ShowModal() == wxID_OK)
    {
        wxString userid = dlg.UserIDEdit->GetValue();
        wxString partid = dlg.PartIDEdit->GetValue();
        wxString name = dlg.NameEdit->GetValue();
        wxString email = dlg.EmailEdit->GetValue();
        wxString phone = dlg.PhoneEdit->GetValue();

#ifdef TEST_PART_UPDATE
/*
int controller_participant_update(controller_t cnt, const char * meeting_id, const char * sub_id,
                                const char *part_id, const char *user_id, const char *username, int role,
                                const char *name, int selphone, const char *phone, int selemail, const char *email,
                                const char *screenname, int notify_type, int call_options);
*/
        controller_participant_update(controller, instantMeeting->meeting_id, NULL,
                partid.mb_str(wxConvUTF8),
                userid.mb_str(wxConvUTF8),
                NULL,
                ParticipantNormal,
                name.mb_str(wxConvUTF8),
                SelPhoneThis, phone.mb_str(wxConvUTF8),
                SelEmailThis, email.mb_str(wxConvUTF8),
                NULL, NotifyEmail, 0
                );
#else

        meeting_t mtg = controller_find_meeting(controller, instantMeeting->meeting_id);
        meeting_details_t details = controller_get_meeting_details(controller, mtg, 0, NULL);
        invitee_t invt = meeting_details_add(details);

        invt->participant_id = meeting_details_string(details, partid.mb_str(wxConvUTF8));
        invt->user_id = meeting_details_string(details, userid.mb_str(wxConvUTF8));
        invt->name = meeting_details_string(details, name.mb_str(wxConvUTF8));
        invt->selphone = SelPhoneThis;
        invt->phone = meeting_details_string(details, phone.mb_str(wxConvUTF8));
        invt->selemail = SelEmailThis;
        invt->email = meeting_details_string(details, email.mb_str(wxConvUTF8));

        controller_meeting_modify(controller, details, TRUE);

#endif

    }
}

void MyFrame::OnAddSubmeeting(wxCommandEvent& event)
{

    controller_submeeting_add(controller, instantMeeting->meeting_id, "addsub");
}

void MyFrame::OnMoveParticipant(wxCommandEvent& event)
{
    MoveParticipant dlg(this);

    if (dlg.ShowModal() == wxID_OK)
    {
        wxString partid = dlg.PartIdEdit->GetValue();
        wxString subid = dlg.SubIdEdit->GetValue();

        controller_participant_move(controller, instantMeeting->meeting_id,
                    partid.mb_str(wxConvUTF8), subid.mb_str(wxConvUTF8));
    }
}

void MyFrame::OnMeetingOptions(wxCommandEvent& event)
{
    ModifyOptions dlg(this);

    if (dlg.ShowModal() == wxID_OK)
    {
        long mask, flag;

        dlg.MaskEdit->GetValue().ToLong(&mask);
        dlg.ValueEdit->GetValue().ToLong(&flag);

        controller_meeting_modify_options(controller, instantMeeting->meeting_id, mask, flag);
    }
}

void MyFrame::OnMeetMany(wxCommandEvent& event)
{
    // Cheat: re-using dialog, enter token as part id in dialog.
    MeetingId dlg(this);
    int res = dlg.ShowModal();
    if (res == wxID_OK)
    {
        wxString meetingid = dlg.MeetingID->GetValue();
        wxString token = dlg.PartID->GetValue();

        controller_get_meeting_params(controller, meetingid.mb_str(wxConvUTF8), token.mb_str(wxConvUTF8));
    }
}

void MyFrame::OnSearchMeetings(wxCommandEvent& event)
{
    static int search = 0;

    wxString opid = wxString::Format(_T("search:%d"), ++search);

    SearchMeeting dlg(this);

    if (dlg.ShowModal() == wxID_OK)
    {
        wxString title = dlg.TitleEdit->GetValue();
        wxString first = dlg.FirstEdit->GetValue();
        wxString last = dlg.LastEdit->GetValue();
        int within = atoi(dlg.NumDaysEdit->GetValue().mb_str());

        int start = MIN_DATE;
        int end = MAX_DATE;
        if (within > 0)
        {
            start = time(NULL);
            end = start + (within * 60 * 60 * 24);
        }

        schedule_search_meetings(sched, opid.mb_str(), NULL, title.mb_str(), first.mb_str(), last.mb_str(), FALSE,
                start, end, 0, 50);
    }
}

void MyFrame::OnSearchContacts(wxCommandEvent& event)
{
    static int search = 0;

    wxString opid = wxString::Format(_T("contacts:%d"), ++search);

    SearchContact dlg(this);

    if (dlg.ShowModal() == wxID_OK)
    {
        wxString fld1 = dlg.Field1Edit->GetValue();
        wxString fld2 = dlg.Field2Edit->GetValue();
        wxString cond1 = dlg.Cond1Edit->GetValue();
        wxString cond2 = dlg.Cond2Edit->GetValue();
        wxString val1 = dlg.Value1Edit->GetValue();
        wxString val2 = dlg.Value2Edit->GetValue();

        BOOL pab = dlg.PersonalCheck->IsChecked() ? TRUE : FALSE;
        BOOL cab = dlg.GlobalCheck->IsChecked() ? TRUE : FALSE;

        contact_condition_t c = contact_condition_new();
        if (cab)
            contact_condition_add_directory(c, dtGlobal);
        if (pab)
            contact_condition_add_directory(c, dtPersonal);
        contact_condition_add_field(c, fld1.mb_str(), cond1.mb_str(), val1.mb_str());
        contact_condition_add_field(c, fld2.mb_str(), cond2.mb_str(), val2.mb_str());

        addressbk_search_contacts(book, opid.mb_str(), c, "first", 0, 50);

        contact_condition_destroy(c);
    }
}

void MyFrame::OnAddContact(wxCommandEvent& event)
{
    static int add = 0;

    wxString opid = wxString::Format(_T("add:%d"), ++add);

    CreateContact dlg(this);

    if (dlg.ShowModal() == wxID_OK)
    {
        int dir_type;
        contact_t contact;
        directory_t dir;

        wxString userid = dlg.UserIdEdit->GetValue();
        wxString screen = dlg.ScreenEdit->GetValue();
        wxString password = dlg.PasswordEdit->GetValue();
        wxString first = dlg.FirstEdit->GetValue();
        wxString last = dlg.LastEdit->GetValue();
        wxString email = dlg.EmailEdit->GetValue();

        if (dlg.UserCheck->IsChecked())
            dir_type = dtGlobal;
        else
            dir_type = dtPersonal;
        dir = addressbk_get_directory(book, dir_type);

        contact = directory_find_contact(dir, userid.mb_str());
        if (contact == NULL)
        {
            contact = directory_contact_new(dir);
            userid = _T("");
        }

        contact->screenname = directory_string(dir, screen.mb_str());
        contact->first = directory_string(dir, first.mb_str());
        contact->last = directory_string(dir, last.mb_str());
        contact->email = directory_string(dir, email.mb_str());
        if (dlg.UserCheck->IsChecked())
            contact->type = UserTypeUser;
        else
            contact->type = UserTypeContact;

        profile_iterator_t iter = profiles_iterate(book);
        profile_t profile = profiles_next(book, iter);
        contact->profileid = directory_string(dir, profile->profile_id);

        addressbk_contact_update(book, opid.mb_str(), dir_type, contact, password.mb_str());
    }
}

void MyFrame::OnAddProfile(wxCommandEvent& event)
{
    static int add = 0;

    wxString opid = wxString::Format(_T("addprof:%d"), ++add);

    CreateProfile dlg(this);

    if (dlg.ShowModal() == wxID_OK)
    {
        profile_t profile;

        wxString profid = dlg.ProfileIdEdit->GetValue();
        wxString name = dlg.NameEdit->GetValue();
        wxString strPinLen = dlg.PINLenEdit->GetValue();
        wxString strIdLen = dlg.MeetingIdLenEdit->GetValue();
        wxString strMaxBuddies = dlg.MaxBuddiesEdit->GetValue();
        long savePass = dlg.SaveCheck->IsChecked() ? ProfileUserSavePassword : 0;
        long maxBuddies, pinLen, idLen;
        strMaxBuddies.ToLong(&maxBuddies);
        strPinLen.ToLong(&pinLen);
        strIdLen.ToLong(&idLen);

        profile = profiles_find_profile(book, profid.mb_str());
        if (profile == NULL)
        {
            profile = profiles_new(book);
            profid = _T("");
        }

        profile->profile_id = profiles_string(book, profid.mb_str());
        profile->name = profiles_string(book, name.mb_str());
        profile->max_presence_monitors = maxBuddies;
        profile->pin_length = pinLen;
        profile->meeting_id_length = idLen;
        profile->enabled_features = (profile->enabled_features & ~ProfileUserSavePassword) | savePass;

        addressbk_profile_update(book, opid.mb_str(), profile);
    }
}

void MyFrame::OnAddGroup(wxCommandEvent& event)
{
    CreateGroup dlg(this);

    if (dlg.ShowModal() == wxID_OK)
    {
        int dir_type;

        wxString GroupName = dlg.GroupNameEdit->GetValue();
        wxString opid = wxString::Format(_T("addgrp:%s"), dlg.MemberEdit->GetValue().c_str());

        directory_t dir;
        if (dlg.GlobalCheck->IsChecked())
            dir_type = dtGlobal;
        else
            dir_type = dtPersonal;
        dir = addressbk_get_directory(book, dir_type);

        group = directory_group_new(dir, GroupName.mb_str());

        addressbk_group_update(book, opid.mb_str(), dir_type, group);
    }
}

// Convert session events to wxEvents...
// AddPendingEvent is thread safe; the event will get serialied back to the UI thread.

void MyFrame::on_error(void *u, int err, const char *msg, int code, const char *id, const char *resource)
{
    MyFrame *frame = (MyFrame*)u;
    SessionErrorEvent ev(idSessionError, err, msg, id);
    frame->AddPendingEvent(ev);
}

void MyFrame::on_connected(void *u)
{
    MyFrame *frame = (MyFrame*)u;
    wxCommandEvent ev(wxEVT_SESSION_CONNECTED, idConnectEvent);
    frame->AddPendingEvent(ev);
}

void MyFrame::on_controller_registered(void *u)
{
    MyFrame *frame = (MyFrame*)u;
    wxCommandEvent ev(wxEVT_CONTROLLER_REGISTERED, idControllerRegistered);
    frame->AddPendingEvent(ev);
}

void MyFrame::on_meeting_event(void *u, meeting_event_t evt)
{
    MyFrame *frame = (MyFrame*)u;
    MeetingEvent ev(idMeetingEvent, evt->event, evt->meeting_id, evt->sub_id, evt->participant_id);
    frame->AddPendingEvent(ev);
}

void MyFrame::on_addressbk_auth(void *u)
{
    MyFrame *frame = (MyFrame*)u;
    wxCommandEvent ev(wxEVT_ADDRESSBK_AUTH, idAddressBkAuth);
    frame->AddPendingEvent(ev);
}

void MyFrame::on_schedule_fetched(void *u, const char *opid, meeting_list_t list)
{
    MyFrame *frame = (MyFrame*)u;
    wxCommandEvent ev(wxEVT_SCHEDULE_FETCHED, idScheduleFetched);
    ev.SetClientData(list);
    ev.SetString(wxString(opid,wxConvUTF8));
    frame->AddPendingEvent(ev);
}

void MyFrame::on_meeting_fetched(void *u, const char *opid, meeting_details_t mtg)
{
    MyFrame *frame = (MyFrame*)u;
    wxCommandEvent ev(wxEVT_MEETING_FETCHED, idMeetingFetched);
    ev.SetClientData(mtg);
    ev.SetString(wxString(opid,wxConvUTF8));
    frame->AddPendingEvent(ev);
}

void MyFrame::on_directory_fetched(void *u, const char *opid, directory_t dir)
{
    MyFrame *frame = (MyFrame*)u;
    wxCommandEvent ev(wxEVT_DIRECTORY_FETCHED, idDirectoryFetched);
    ev.SetClientData(dir);
    ev.SetString(wxString(opid,wxConvUTF8));
    frame->AddPendingEvent(ev);
}

void MyFrame::on_schedule_updated(void *u, const char *opid, int status)
{
    MyFrame *frame = (MyFrame*)u;
    wxCommandEvent ev(wxEVT_SCHEDULE_UPDATED, idScheduleUpdated);
    frame->AddPendingEvent(ev);
}

void MyFrame::on_address_updated(void *u, const char *opid, const char *itemid)
{
    MyFrame *frame = (MyFrame*)u;
    wxCommandEvent ev(wxEVT_ADDRESS_UPDATED, idAddressUpdated);
    ev.SetString(wxString(opid,wxConvUTF8));
    frame->AddPendingEvent(ev);
}

void MyFrame::on_meet_many(void *u, meeting_details_t mtg, int options)
{
    MyFrame *frame = (MyFrame*)u;
    wxCommandEvent ev(wxEVT_MEETING_FETCHED, idMeetingFetched);
    ev.SetClientData(mtg);
    ev.SetString(_T("meetmany"));
    frame->AddPendingEvent(ev);
}


void MyFrame::DumpMeeting(meeting_details_t mtg)
{
    invitee_iterator_t iter;
    invitee_t invt;

    iter = meeting_details_iterate(mtg);

    panel->text->AppendText(_("Meeting details: ") + wxString(mtg->meeting_id, wxConvUTF8) +
                        _T(" ") + wxString(mtg->title,wxConvUTF8) + _T("\n"));

    while ((invt = meeting_details_next(mtg, iter)) != NULL)
    {
        panel->text->AppendText(_T("  ") + wxString(invt->participant_id, wxConvUTF8) + _T(": ") +
                        wxString(invt->name,wxConvUTF8) + _T("\n"));
    }
}
