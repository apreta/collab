#ifndef SEARCHMEETING_H
#define SEARCHMEETING_H

#include <wx/wxprec.h>

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

//(*Headers(SearchMeeting)
#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
//*)

class SearchMeeting: public wxDialog
{
	public:

		SearchMeeting(wxWindow* parent,wxWindowID id = -1);
		virtual ~SearchMeeting();

		//(*Identifiers(SearchMeeting)
		//*)

	protected:

		//(*Handlers(SearchMeeting)
		//*)

    public:

		//(*Declarations(SearchMeeting)
		wxTextCtrl* TitleEdit;
		wxTextCtrl* FirstEdit;
		wxStaticText* StaticText3;
		wxTextCtrl* LastEdit;
		wxTextCtrl* NumDaysEdit;
		//*)

	private:

		DECLARE_EVENT_TABLE()
};

#endif
