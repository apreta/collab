#include "signondialog.h"

//(*InternalHeaders(SignOnDialog)
#include <wx/bitmap.h>
#include <wx/button.h>
#include <wx/font.h>
#include <wx/fontenum.h>
#include <wx/fontmap.h>
#include <wx/image.h>
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(SignOnDialog)
//*)


BEGIN_EVENT_TABLE(SignOnDialog,wxDialog)
	//(*EventTable(SignOnDialog)
	//*)
END_EVENT_TABLE()

SignOnDialog::SignOnDialog(wxWindow* parent,wxWindowID id)
{
	//(*Initialize(SignOnDialog)
	wxXmlResource::Get()->LoadObject(this,parent,_T("SignOnDialog"),_T("wxDialog"));
	Screen = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL2"));
	Password = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL3"));
	//*)
}

SignOnDialog::~SignOnDialog()
{
}

