#ifndef MODIFYOPTIONS_H
#define MODIFYOPTIONS_H

#include <wx/wxprec.h>

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

//(*Headers(ModifyOptions)
#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
//*)

class ModifyOptions: public wxDialog
{
	public:

		ModifyOptions(wxWindow* parent,wxWindowID id = -1);
		virtual ~ModifyOptions();

		//(*Identifiers(ModifyOptions)
		//*)

	protected:

		//(*Handlers(ModifyOptions)
		//*)

    public:

		//(*Declarations(ModifyOptions)
		wxTextCtrl* MaskEdit;
		wxTextCtrl* ValueEdit;
		//*)

	private:

		DECLARE_EVENT_TABLE()
};

#endif
