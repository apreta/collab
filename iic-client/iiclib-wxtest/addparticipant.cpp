#include "addparticipant.h"

//(*InternalHeaders(AddParticipant)
#include <wx/bitmap.h>
#include <wx/button.h>
#include <wx/font.h>
#include <wx/fontenum.h>
#include <wx/fontmap.h>
#include <wx/image.h>
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(AddParticipant)
//*)

BEGIN_EVENT_TABLE(AddParticipant,wxDialog)
	//(*EventTable(AddParticipant)
	//*)
END_EVENT_TABLE()

AddParticipant::AddParticipant(wxWindow* parent,wxWindowID id)
{
	//(*Initialize(AddParticipant)
	wxXmlResource::Get()->LoadObject(this,parent,_T("AddParticipant"),_T("wxDialog"));
	UserIDEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL4"));
	PartIDEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL5"));
	NameEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL1"));
	EmailEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL2"));
	PhoneEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL3"));
	NotifyEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL6"));
	//*)
}

AddParticipant::~AddParticipant()
{
}

