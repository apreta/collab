#include "app.h"
#include "main.h"

//(*AppHeaders
#include <wx/xrc/xmlres.h>
#include <wx/image.h>
//*)
#include "wx/xh_treelist.h"


IMPLEMENT_APP(MyApp);

bool MyApp::OnInit()
{
    wxXmlResource::Get()->AddHandler(new wxTreeListCtrlXmlHandler());

	//(*AppInitialize
	bool wxsOK = true;
	wxInitAllImageHandlers();
	wxXmlResource::Get()->InitAllHandlers();
	wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("mainpanel.xrc"));
	wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("meetingid.xrc"));
	wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("signondialog.xrc"));
	wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("createmeeting.xrc"));
	wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("addparticipant.xrc"));
	wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("moveparticipant.xrc"));
	wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("modifyoptions.xrc"));
	wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("searchmeeting.xrc"));
	wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("searchcontact.xrc"));
	wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("createcontact.xrc"));
	wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("createprofile.xrc"));
	wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("creategroup.xrc"));
	//*)

	MyFrame* frame = new MyFrame(0L, _("wxWidgets Application Template"));
	frame->Show();
	return true;
}
