#ifndef MAINPANEL_H
#define MAINPANEL_H

#include <wx/wxprec.h>

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

//(*Headers(MainPanel)
#include <wx/panel.h>
#include <wx/sizer.h>
#include <wx/textctrl.h>
#include <wx/treelistctrl.h>
//*)

#include "../iiclib/controller.h"


class MainPanel: public wxPanel
{
	public:

		MainPanel(wxWindow* parent,wxWindowID id = -1);
		virtual ~MainPanel();

        void BuildTree(meeting_t meeting);

		//(*Identifiers(MainPanel)
		//*)

	protected:

		//(*Handlers(MainPanel)
		//*)

    public:
		//(*Declarations(MainPanel)
		wxTreeListCtrl* tree;
		wxTextCtrl* text;
		//*)

	private:

		DECLARE_EVENT_TABLE()
};

#endif
