#ifndef CREATECONTACT_H
#define CREATECONTACT_H

#include <wx/wxprec.h>

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

//(*Headers(CreateContact)
#include <wx/checkbox.h>
#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
//*)

class CreateContact: public wxDialog
{
	public:

		CreateContact(wxWindow* parent,wxWindowID id = -1);
		virtual ~CreateContact();

		//(*Identifiers(CreateContact)
		//*)

	protected:

		//(*Handlers(CreateContact)
		//*)

    public:

		//(*Declarations(CreateContact)
		wxTextCtrl* UserIdEdit;
		wxTextCtrl* ScreenEdit;
		wxTextCtrl* FirstEdit;
		wxTextCtrl* LastEdit;
		wxTextCtrl* EmailEdit;
		wxTextCtrl* PasswordEdit;
		wxCheckBox* UserCheck;
		//*)

	private:

		DECLARE_EVENT_TABLE()
};

#endif
