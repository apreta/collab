#include "createcontact.h"

//(*InternalHeaders(CreateContact)
#include <wx/bitmap.h>
#include <wx/button.h>
#include <wx/font.h>
#include <wx/fontenum.h>
#include <wx/fontmap.h>
#include <wx/image.h>
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(CreateContact)
//*)

BEGIN_EVENT_TABLE(CreateContact,wxDialog)
	//(*EventTable(CreateContact)
	//*)
END_EVENT_TABLE()

CreateContact::CreateContact(wxWindow* parent,wxWindowID id)
{
	//(*Initialize(CreateContact)
	wxXmlResource::Get()->LoadObject(this,parent,_T("CreateContact"),_T("wxDialog"));
	UserIdEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL1"));
	ScreenEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL2"));
	FirstEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL3"));
	LastEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL4"));
	EmailEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL5"));
	PasswordEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL6"));
	UserCheck = (wxCheckBox*)FindWindow(XRCID("ID_CHECKBOX1"));
	//*)
}

CreateContact::~CreateContact()
{
	//(*Destroy(CreateContact)
	//*)
}

