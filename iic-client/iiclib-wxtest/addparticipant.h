#ifndef ADDPARTICIPANT_H
#define ADDPARTICIPANT_H

#include <wx/wxprec.h>

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

//(*Headers(AddParticipant)
#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
//*)

class AddParticipant: public wxDialog
{
	public:

		AddParticipant(wxWindow* parent,wxWindowID id = -1);
		virtual ~AddParticipant();

		//(*Identifiers(AddParticipant)
		//*)

	protected:

		//(*Handlers(AddParticipant)
		//*)

    public:
		//(*Declarations(AddParticipant)
		wxTextCtrl* UserIDEdit;
		wxTextCtrl* PartIDEdit;
		wxTextCtrl* NameEdit;
		wxTextCtrl* EmailEdit;
		wxTextCtrl* PhoneEdit;
		wxTextCtrl* NotifyEdit;
		//*)

	private:

		DECLARE_EVENT_TABLE()
};

#endif
