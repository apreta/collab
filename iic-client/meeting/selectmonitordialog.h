#ifndef SELECTMONITORDIALOG_H
#define SELECTMONITORDIALOG_H

//(*Headers(SelectMonitorDialog)
#include <wx/panel.h>
#include <wx/dialog.h>
//*)
#include "monitorlabelframe.h"

#include "sharelib/share_api.h"

class SelectMonitorDialog: public wxDialog
{
	public:
		SelectMonitorDialog(wxWindow* parent, std::list<MonitorInfo> & monitors);
		virtual ~SelectMonitorDialog();

		//(*Declarations(SelectMonitorDialog)
		wxPanel* m_Panel;
		//*)
		
		wxString GetDisplay()
		{
		    return m_Selected;
		}

		void CloseLabels();

	protected:
        wxRadioBox* m_Displays;
        wxString m_Selected;
        std::list<MonitorLabelFrame*> m_Labels;
        
		//(*Identifiers(SelectMonitorDialog)
		//*)
		
		bool Validate();
		
	private:
        std::list<MonitorInfo> m_Monitors;

		//(*Handlers(SelectMonitorDialog)
		void OnInit(wxInitDialogEvent& event);
		//*)

		DECLARE_EVENT_TABLE()
};

#endif
