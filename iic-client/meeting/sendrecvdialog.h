/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#if !defined(AFX_DIALOGSENDRECV_H__C77F6910_A39D_4E62_B6FD_14CE6AF28577__INCLUDED_)
#define AFX_DIALOGSENDRECV_H__C77F6910_A39D_4E62_B6FD_14CE6AF28577__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DialogSendRecv.h : header file
//

#include "wx/dialog.h"
#include "wx/gauge.h"

#if defined(LINUX) || defined(MACOSX)
typedef int BOOL;
#endif

/////////////////////////////////////////////////////////////////////////////
// SendRecvDialog dialog

class SendRecvDialog : public wxDialog
{
// Construction
public:
    SendRecvDialog( wxWindow* pParent = NULL);   // standard constructor

    void    SetTitle(const wxString& aTitle);
	void    SetLabelTitle(const wxString& aLabel);
	void    SetLabelTitle2(const wxString& aLabel);
	void    SetProgressRange(int aMax);
	void    SetProgressPos(int aPos);
	void    SetProgressRange2(int aMax);
	void    SetProgressPos2(int aPos);
	BOOL    fCanceled( )                { return m_fCancel; }
	void    Cancel( )                   { m_fCancel = TRUE; }
	void    StartAutoProgress(int aTimeout);
	void    StopAutoProgress();
	void    SetCancel(bool fCanCancel);

// Dialog Data
	//{{AFX_DATA(CDialogSendRecv)
//	enum { IDD = IDD_DIALOG_SENDRECV };
//	CProgressCtrl	m_ctlProgress2;
//	CProgressCtrl	m_ctlProgress;
    //}}AFX_DATA
    wxGauge         *m_pGauge1;
    wxGauge         *m_pGauge2;
    wxStaticText    *m_pStatic1;
    wxStaticText    *m_pStatic2;

    bool            m_fCancel;

// Overrides
	// ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CDialogSendRecv)
protected:
//	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
    void        OnOK( wxCommandEvent& event );
    void        OnCancel( wxCommandEvent& event );

	// Generated message map functions
	//{{AFX_MSG(CDialogSendRecv)
//	virtual void OnOK();
//	afx_msg void OnTimer(UINT nIDEvent);
//	virtual BOOL OnInitDialog();
//	afx_msg void OnClose();
//	afx_msg void OnDestroy();
	//}}AFX_MSG
//	DECLARE_MESSAGE_MAP()



    DECLARE_EVENT_TABLE()
};


#endif // !defined(AFX_DIALOGSENDRECV_H__C77F6910_A39D_4E62_B6FD_14CE6AF28577__INCLUDED_)
