/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 * 
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */
#ifndef APPSHAREPANEL_H
#define APPSHAREPANEL_H

#include <wx/scrolwin.h>
#include <wx/panel.h>
#include <wx/html/htmlwin.h>

#include "events.h"

class MeetingMainFrame;
class ShareControl;
class AppSharePanel;

// Scrollable bitmap viewer
class BitmapView: public wxScrolledWindow
{
public:
    BitmapView(wxWindow* parent,wxWindowID id,wxBitmap& bitmap,ShareControl *control);
    virtual ~BitmapView();
    
protected:
    void OnEraseBackground(wxEraseEvent& event);
    void OnPaint(wxPaintEvent& event);
	void OnMove(wxMouseEvent& event);
	void OnButton(wxMouseEvent& event);
    void OnKey(wxKeyEvent& event);
    
public:
    void StartShare();
    void SetScrolling();
    void SetRemoteControl(bool enabled);
    void SetControlGranted(bool granted);
        
    wxString m_Status;
    wxBitmap& m_Bitmap;
    int m_Width;
    int m_Height;
    bool m_ControlGranted;
    bool m_ControlEnabled;
    
private:
	ShareControl* m_pShareControl;
    wxCursor m_CursorControl;
    wxCursor m_CursorGranted;
    wxCursor m_CursorNormal;
    
    DECLARE_EVENT_TABLE()
};


// Frame window for full screen app share viewer
class FullScreenFrame: public wxFrame
{
public:
    FullScreenFrame(wxWindow* parent, wxBitmap& bitmap);
    virtual ~FullScreenFrame();
    
protected:
    void OnPaint(wxPaintEvent& event);

    BitmapView* m_BitmapView;

private:
    DECLARE_EVENT_TABLE()
};


class AppSharePanel: public wxWindow
{
	public:
		AppSharePanel(wxWindow* parent,wxWindowID id = -1,ShareControl *control = NULL);
		virtual ~AppSharePanel();

    protected:

		void OnEraseBackground(wxEraseEvent& event);
		void OnMeetingEvent(MeetingEvent& event);
        void OnShareTimer(wxTimerEvent& event);
		void OnShareEvent(wxCommandEvent& event);

        virtual void DoSetSize(int x, int y,
                           int width, int height,
                           int sizeFlags = wxSIZE_AUTO);

        void SetInfoText();
        void NormalDisplay();
        void FullScreen();

        MeetingMainFrame* m_pMMF;
		wxHtmlWindow* m_ShareHTML;
		ShareControl* m_pShareControl;
		wxSize m_ViewerSize;
        wxTimer m_ShareTimer;

        wxBitmap m_ShareBitmap;
        BitmapView* m_BitmapView;
        FullScreenFrame* m_FullScreen;
    
	private:

		DECLARE_EVENT_TABLE()
};

#endif
