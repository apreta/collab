#ifndef FILEMANAGER_H
#define FILEMANAGER_H

#include "webdav.h"
#include "progress1dialog.h"

#include <vector>

#define MEETING_PATH    wxT("/repository/SharedDocuments/$mid/")
#define USER_PATH       wxT("/repository/user/$user/")


namespace FileManagerNS
{
    class ProcessThread;
}

class FileCollection;
class FileDetails;


/**
 * Handle file upload/download/caching
 */
class FileManager
{
    friend class FileManagerNS::ProcessThread;
    
public:
    typedef bool (FileManager::*FuncPtr)(const wxString& name);

    enum { Create = 1, ShowErrors = 2, InMeeting = 4 };

    FileManager(const wxString& repoPath, int options = Create);
    virtual ~FileManager();

    static void SetUploadURL(wxString url);
    
    // Upload a single file
    bool UploadFile(const wxString& remoteName, const wxString& data, bool isFileName);

    // Download a single file
    // If saveToFile is true, outData has fileName; otherwise it has data
    bool DownloadFile(const wxString& remoteName, wxString& outData, bool saveToFile);

    // Load collection of files
    bool LoadCollection(const wxString& name);

    // Store collection of files (upload new files as needed)
    bool StoreCollection(const wxString& name);

    // Copy collection to new location
    long CopyCollection(const wxString& name, const wxString& fromPath, bool fDeleteSource = true);
    
    bool LoadAsync(const wxString& name, wxWindow* parent, bool wait);

    bool StoreAsync(const wxString& name, wxWindow* parent, bool wait);
    
    int GetFileCount();
    FileDetails* GetFile(int index);
    void AddFile(int index, const wxString& localFile, const wxString& title);
    void RemoveFile(int index);
    void MoveFile(int index, int dir);
    bool Exists(const wxString& compareFile);
    void SetDirty() { m_Dirty = true; }
    bool IsDirty()  { return m_Dirty; }
    
    wxString& GetTempPath() { return m_TempDir; }
    wxString GetTempFile(const wxString& ext);

    enum { OK = 0, DownloadError, UploadError, FileNotFound };
    int GetLastError()  { return m_Status; }
    
    static wxString GetPathForUser(const wxString& userName);
    static wxString GetPathForMeeting(const wxString& meetingId);
    static long ComputeChecksum(wxString fn);
    
protected:
    long makeDir(const wxString& aTargetUrl, const wxString& aTargetPath,
                 wxString& aStatusCode, wxString& aStatusText);
    long sendFile(const wxString& aTargetUrl, const wxString& aTargetPath,
                 const wxString& aIn, bool aSendFromFile, wxString& aStatusCode, wxString& aStatusText);
    long getFile(const wxString& aTargetUrl, const wxString& aTargetPath, 
                 wxString& aOut, bool aSaveToFile, wxString& aStatusCode, wxString& aStatusText);
    long deleteFile(const wxString& aTargetUrl, const wxString& aTargetPath,
                    wxString& aStatusCode, wxString& aStatusText);
    bool checkStatus(const wxString& status);
    void showProgress();
    void hideProgress();
    wxString generateMeta(const wxString& base);
    long parseMeta(const wxString& meta);
    bool processAsync(const wxString& name, const wxString& caption, wxWindow* parent, bool wait, FuncPtr op);

private:
    static wxString m_UploadURL;

    int m_Options;
    wxString m_RepoPath;
    wxString m_TempDir;

    wxString m_UserName;
    wxString m_Password;

    wxWindow* m_pParent;
    Progress1Dialog* m_pDialogProgress;

    int m_Status;

    bool m_InProgress;
    CWebDavUtils m_WebDavUtils;

    std::vector<FileDetails*> m_Files;
    bool m_Dirty;

    static int m_TempCount;
};


class FileDetails
{
public:
    FileDetails(const wxString& title, const wxString& localPath) :
        m_Title(title), m_LocalPath(localPath)
    {
        wxFileName fn(localPath);
        m_Type = fn.GetExt();
    }

    FileDetails(const wxString& title, const wxString& name, const wxString& type) :
         m_Name(name), m_Type(type), m_Title(title)
    {
        
    }
 
    wxString m_Name;
    wxString m_Type; // (jpg, etc.)
    wxString m_Title;
    wxString m_LocalPath;
};


namespace FileManagerNS {
    class ProcessThread : public wxThread
    {
        wxString name;
        FileManager* manager;
        FileManager::FuncPtr op;

    public:
        ProcessThread(FileManager* manager, FileManager::FuncPtr op, const wxString& name) :
            wxThread(wxTHREAD_JOINABLE)
        {
            this->manager = manager;
            this->op = op;
            this->name = name;
        }
        void* Entry();
    };
}

#endif // FILEMANAGER_H
