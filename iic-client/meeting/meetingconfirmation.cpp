/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "htmlgen.h"
#include "meetingconfirmation.h"
#include "services/common/common.h"

//(*InternalHeaders(MeetingConfirmation)
#include <wx/xrc/xmlres.h>
//*)
#include <wx/sizer.h>

//(*IdInit(MeetingConfirmation)
//*)

BEGIN_EVENT_TABLE(MeetingConfirmation,wxDialog)
	//(*EventTable(MeetingConfirmation)
	//*)
END_EVENT_TABLE()

MeetingConfirmation::MeetingConfirmation(wxWindow* parent,wxWindowID id):
m_nNoteCount(0),
m_nDisplayMask(0),
m_fStartingMeeting(false)
{
	//(*Initialize(MeetingConfirmation)
	wxXmlResource::Get()->LoadObject(this,parent,_T("MeetingConfirmation"),_T("wxDialog"));
	m_HeaderMsg = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT2"));
	m_SendToAll = (wxRadioButton*)FindWindow(XRCID("ID_SENDTOALL_RADIO"));
	m_SendToNew = (wxRadioButton*)FindWindow(XRCID("ID_SENDTONEW_RADIO"));
	m_SendToNone = (wxRadioButton*)FindWindow(XRCID("ID_SENDTONONE_RADIO"));
	m_HTML = (wxHtmlWindow*)FindWindow(XRCID("ID_HTML"));
	m_Panel = (wxPanel*)FindWindow(XRCID("ID_PANEL1"));
	StaticText1 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT1"));
	//*)

	// Setup the various notes
	m_strPleaseNote   =         _("Please note:");
	m_strEmailsDontSend =       _("Emails will not be sent to invitees with <b>Don't Send</b> selected.");
	m_strNonSysModerator    =   _("One or more invited moderators is not a system user.  While they can join your meeting they cannot start it.");
	m_strTimeUnspecified =      _("Your scheduled meeting time has past or is unspecified.");
	m_strStartSendReminders =   _("Your meeting settings are configured to send new reminder invitations.  Starting the meeting will:");
	m_strStartWillCall =        _("Call participants");
	m_strStartWillEmail =       _("Email invitations");
    m_strStartWillIM =          _("IM invitations");
	m_strLargeMeeting =         _("You have invited a large number of people. ");
	m_strLargeMeetingContact =  _("You have invited (and are about to call) a large number of people. ");
	//m_strClickOK      =         _("Click OK to continue, or click Cancel to review the meeting settings before proceeding.");

#ifdef __WXMAC__
	Centre();
#endif

}
/*
    LTEXT           "Emails will not be sent to invitees with ""Don't Send"" selected.",
                    IDC_STATIC_EMAILNOTE,18,83,167,20,NOT WS_VISIBLE
    LTEXT           "o",IDC_STATIC_EMAILBULLET,8,82,8,8,NOT WS_VISIBLE
*/
MeetingConfirmation::~MeetingConfirmation()
{
}



int MeetingConfirmation::GetConfirmation( bool fStarting, int nDisplayMask )
{
    int nRet = -1;
    bool fShowDlg = true;
    int nResp = wxID_OK;

    m_fStartingMeeting = fStarting;
    fShowDlg = nDisplayMask != 0;

    if( fShowDlg )
    {
        // Flesh out the mask a bit more:
        nDisplayMask |= MCM_NeedWarning;

        if( nDisplayMask & (MCM_NotifyCall|MCM_NotifyEmail|MCM_NotifyIM) )
        {
            if( fStarting )
            {
                nDisplayMask |= MCM_NeedNotify;
            }else
            {
                if( nDisplayMask & MCM_NotifyEmail )
                {
                    nDisplayMask |= MCM_EmailOptions|MCM_EmailNote;
                }
            }
        }

        m_nDisplayMask = nDisplayMask;

        if( !PrepareDisplay( ) )
        {
            //  Nothing was displayed in the confirmation dlg - time to get out of Dodge
            return SendEmailNone;
        }

        nResp = ShowModal();

        if( nResp == wxID_OK )
        {
            if( m_SendToAll->GetValue() )
            {
                nRet = SendEmailAll;
            }else if( m_SendToNew->GetValue() )
            {
                nRet = SendEmailNew;
            }else
            {
                nRet = SendEmailNone;
            }
        }
    }else
    {
        // Default return for no confirmation required
        nRet = SendEmailNone;
    }
    return nRet;
}


bool MeetingConfirmation::PrepareDisplay()
{
    bool    fRet    = false;

    // Show or hide the email radio buttons
    if( m_nDisplayMask & MCM_EmailOptions )
    {
        m_SendToAll->SetValue( true );
    }
    else
    {
		m_HeaderMsg->Hide();
		m_SendToAll->Hide();
		m_SendToNew->Hide();
		m_SendToNone->Hide();
    }

    // Start building the HTML content
    HtmlGen r;
    r.clear( false );
    r.startHtml();

    r.bold( m_strPleaseNote );
    r.linebreak();
    r.linebreak();

    if( m_nDisplayMask & MCM_EmailNote )
    {
        r.line( m_strEmailsDontSend );
        fRet = true;
    }

    if( m_nDisplayMask & MCM_NeedNotify )
    {
        r.line( m_strStartSendReminders );
        r.startUnorderedList( HtmlGen::Bullet_disc );

        if( m_nDisplayMask & MCM_NotifyCall )
        {
            r.listItem( m_strStartWillCall );
        }
        if( m_nDisplayMask & MCM_NotifyEmail )
        {
            r.listItem( m_strStartWillEmail );
        }
        if( m_nDisplayMask & MCM_NotifyIM )
        {
            r.listItem( m_strStartWillIM );
        }
        r.endUnorderedList();
        r.linebreak();
        fRet = true;
    }

    if( m_nDisplayMask & (MCM_LargeMeeting|MCM_ModeratorExternal|MCM_InvalidTime) )
    {
        r.startUnorderedList( HtmlGen::Bullet_disc );

        if( m_nDisplayMask & MCM_LargeMeeting )
        {
            if( m_nDisplayMask & MCM_NotifyCall )
            {
                r.listItem( m_strLargeMeetingContact );
            }else
            {
                r.listItem( m_strLargeMeeting );
            }
        }

        if( m_nDisplayMask & MCM_ModeratorExternal )
        {
            r.listItem( m_strNonSysModerator );
        }

        if( m_nDisplayMask & MCM_InvalidTime )
        {
            r.listItem( m_strTimeUnspecified );
        }

        r.endUnorderedList();
        fRet = true;
    }

    //r.linebreak();
    //r.line(m_strClickOK); // Using static text for this now

    r.endHtml();

    // Set the HTML content and size the control to fit the content
    m_HTML->SetPage( r.buf() );
    m_HTML->SetSize(m_HTML->GetInternalRepresentation()->GetWidth(),
    m_HTML->GetInternalRepresentation()->GetHeight());

    GetSizer()->Fit(this);
    
    m_Panel->Layout();
    m_Panel->Refresh();
    m_Panel->Update();

    return fRet;
}
