/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"
#include "wxzonoptions.h"
#include <wx/stdpaths.h>
#include <wx/tokenzr.h>
#include "utils.h"

wxZonOptions::wxZonOptions():
m_fInitialized(false)
{

}

void wxZonOptions::Initialize( const wxString & baseName, const wxString & screenName, const wxString & userDir, wxString & globalDir )
{
    if( !m_fInitialized )
    {
        m_options = options_create( baseName.mb_str(wxConvUTF8), screenName.mb_str(wxConvUTF8), userDir.mb_str(wxConvUTF8), globalDir.mb_str(wxConvUTF8) );
        m_fInitialized = true;
    }
}

void wxZonOptions::Initialize( const wxString & screenName )
{
    wxString sysDir = wxStandardPaths::Get().GetDataDir();
    wxString userDir = wxStandardPaths::Get().GetUserDataDir();
    Initialize( wxT("zon"), screenName, userDir, sysDir );
}

bool wxZonOptions::SetScreenName( const wxString & screenName )
{
    wxString encName( screenName );
    CUtils::EncodeStringFully( encName );
    wxString userDir = wxStandardPaths::Get().GetUserDataDir();
    int nRet = options_set_screen_name( m_options, encName.mb_str(wxConvUTF8), userDir.mb_str(wxConvUTF8) );
    return (nRet == 0);
}

bool wxZonOptions::Read()
{
	int nRet = options_read( m_options );

	return (nRet == 0);
}

bool wxZonOptions::Write()
{
	int nRet = options_write( m_options );

	return (nRet == 0);
}

const char * wxZonOptions::GetUser(const char *name, const char *def)
{
	return options_get_user( m_options, name, def );
}

void wxZonOptions::SetUser(const char *name, const char *value)
{
	options_set_user( m_options, name, value);
}

void wxZonOptions::ClrUser(const char *name)
{
    options_clr_user( m_options, name );
}


const char *wxZonOptions::GetSystem(const char *name, const char *def)
{
	return options_get_system( m_options, name, def);
}


int wxZonOptions::GetSystemInt( const char *name, int nDefault )
{
    int nRet = nDefault;
    wxString strDefault;
    strDefault = wxString::Format(wxT("%d"),nDefault);
    wxCharBuffer def = strDefault.mb_str();

    const char *value = GetSystem( name, def );
    if( value )
    {
        wxString strValue( value, wxConvUTF8 );
        long n;
        if( strValue.ToLong( &n, 10 ) )
        {
            nRet = n;
        }
    }
    return nRet;
}


bool wxZonOptions::GetSystemBool( const char *name, bool fDefault )
{
    bool fRet = fDefault;
    const char *defaultNo("No");
    const char *defaultYes("Yes");
    const char *value = GetSystem( name, (fDefault) ? defaultYes : defaultNo );
    if( value )
    {
        char c = tolower(value[0]);
        if( c == 'y' || c == '1' || c == 't' )
            fRet = true;
        else
            fRet = false;
    }
    return fRet;
}

void wxZonOptions::SetSystem(const char *name, const char *value)
{
	options_set_system( m_options, name, value);
}

void wxZonOptions::SetSystemInt( const char *name, int iValue )
{
    wxString strValue;
    strValue = wxString::Format( wxT("%d"),iValue );
    SetSystem( name, strValue.mb_str() );
}

bool wxZonOptions::SaveScreenMetrics(  const char *name ,const wxWindow & window )
{
    wxSize size;
    wxPoint point;
    long style;

    if( !window.IsShown() )
        return false;

#if defined(_WXMSW_)
    long ignoreThis = wxMAXIMIZE|wxMINIMIZE;
#else
    long ignoreThis = 0;
#endif
    style = window.GetWindowStyleFlag();
    if( style & ignoreThis )
        return false;

    size  = window.GetSize();
    point = window.GetPosition();

    if( ((size.x + point.x) < 0)  ||  ((size.y + point.y) < 0) )
        return false;

    return SaveScreenMetrics( name, size, point, style );
}

bool wxZonOptions::SaveScreenMetrics( const char *name, const wxSize & size, const wxPoint & point, long style )
{
    bool fOK = false;
    wxString value;

    value = wxString::Format( wxT("%d,%d,%d,%d,%u"),
        size.GetWidth(), size.GetHeight(),
        point.x, point.y,
        style);

    SetUser( name, value.mb_str(wxConvUTF8) );

    fOK = true;

    return fOK;
}

bool wxZonOptions::LoadScreenMetrics( const char *name, wxWindow & window )
{
    bool fOK = false;
    wxString strValue;
    wxString strDef(wxT(""));

    strValue = GetUserString( name, strDef );
    if( strValue.length() == 0 )
        return fOK;

    wxRect rect;
    long style;

    int i = 0;
    long numVal = 0;

    wxStringTokenizer tkz( strValue, wxT(","));

    fOK = true;

    while ( fOK && tkz.HasMoreTokens() )
    {
        wxString token = tkz.GetNextToken();
        fOK = token.ToLong( &numVal, 10 );
        if( fOK )
        {
            switch (i++)
            {
            case 0: rect.SetWidth( (int)numVal ); break;
            case 1: rect.SetHeight( (int)numVal ); break;
            case 2: rect.SetX( (int)numVal ); break;
            case 3: rect.SetY( (int)numVal ); break;
            case 4: style = numVal; break;
            default: fOK = false; break; // Too many tokens!
            }
        }
    }
    // TODO: Validate the window size and position

    // No errors and 5 tokens is good
    if( fOK && i == 5 )
    {
        wxLogDebug(wxT("wxZonOptions::LoadScreenMetrics name=x=%d , y=%d , w=%d , h=%d"),
            rect.GetX(), rect.GetY(), rect.GetWidth(), rect.GetHeight() );

        window.SetSize( rect );
        window.SetWindowStyleFlag( style );
    }
    return fOK;
}

bool wxZonOptions::GetUserBool( const char *name, bool fDefault )
{
    bool fRet = fDefault;
    const char *defaultNo("No");
    const char *defaultYes("Yes");
    const char *value = GetUser( name, (fDefault)?defaultYes:defaultNo );
    if( value )
    {
        char c = tolower(value[0]);
        if( c == 'y' || c == '1' || c == 't' )
            fRet = true;
        else
            fRet = false;
    }
    return fRet;
}

void wxZonOptions::SetUserBool( const char *name, bool fValue )
{
    const char *No("No");
    const char *Yes("Yes");
    SetUser( name, (fValue)?Yes:No );
}


int wxZonOptions::GetUserInt( const char *name, int nDefault )
{
    int nRet = nDefault;
    wxString strDefault;
    strDefault = wxString::Format(wxT("%d"),nDefault);
    wxCharBuffer def = strDefault.mb_str();

    const char *value = GetUser( name, def );
    if( value )
    {
        wxString strValue( value, wxConvUTF8 );
        long n;
        if( strValue.ToLong( &n, 10 ) )
        {
            nRet = n;
        }
    }
    return nRet;
}

void wxZonOptions::SetUserInt( const char *name, int nValue )
{
    wxString strValue;
    strValue = wxString::Format( wxT("%d"),nValue );
    SetUser( name, strValue.mb_str() );
}

wxFont wxZonOptions::GetUserFont( const char *name, const wxFont & defaultFont )
{
    wxFont retFont;    wxString strDefaultFont;
    strDefaultFont = CUtils::FontToString( defaultFont );
    wxCharBuffer def = strDefaultFont.mb_str();
    const char *value = GetUser( name, def );
    wxString strValue( value, wxConvUTF8 );
    retFont = CUtils::FontFromString( strValue );
    return retFont;
}

void wxZonOptions::SetUserFont( const char *name, const wxFont & font )
{
    wxString strFont;
    strFont = CUtils::FontToString( font );
    SetUser( name, strFont.mb_str() );
}

wxColour wxZonOptions::GetUserColour( const char *name, const wxColour & defaultColour )
{
    wxColour retColour;
    wxString strDefaultColour;
    strDefaultColour = CUtils::ColourToString( defaultColour );
    wxCharBuffer def = strDefaultColour.mb_str();
    const char *value = GetUser( name, def );
    wxString strValue( value, wxConvUTF8 );
    retColour = CUtils::ColourFromString( strValue );
    return retColour;
}

void wxZonOptions::SetUserColour( const char *name, const wxColour & colour )
{
    wxString strColour;
    if (colour.IsOk())
    {
        strColour = CUtils::ColourToString( colour );
        SetUser( name, strColour.mb_str() );
    }
}

wxString wxZonOptions::GetUserString( const char *name, const wxString & strDefault )
{
    wxString ret;
    wxCharBuffer def = strDefault.mb_str(wxConvUTF8);
    ret = wxString::FromAscii( GetUser( name, def ) );
    return ret;
}

void wxZonOptions::SetUserString( const char *name, const wxString & strValue )
{
    SetUser( name, strValue.mb_str(wxConvUTF8) );
}

void wxZonOptions::SetUserArrayInt( const char *name, const wxArrayInt & array )
{
    wxString strEncodedArray(wxT(""));
    wxString delimiter(wxT("|"));

    for( size_t i = 0; i < array.GetCount(); ++i )
    {
        wxString value;
        value = wxString::Format(wxT("%d"),array[i]);
        strEncodedArray += value;
        if( i < (array.GetCount()-1) )
        {
            strEncodedArray += delimiter;
        }
    }
    SetUserString( name, strEncodedArray );
}

bool wxZonOptions::GetUserArrayInt( const char *name, const wxArrayInt & defaultArray, wxArrayInt & array )
{
    bool fDefault = false;
    wxString blank(wxT(""));
    wxString value(wxT(""));
    wxString delimiter(wxT("|"));

    value = GetUserString( name, blank );

    if( value == blank )
    {
        fDefault = true;
        array.Clear();
        for( size_t i = 0; i < defaultArray.GetCount(); ++i )
        {
            array.Add( defaultArray[i] );
        }
    }else
    {
        // value is: "n0|n1|n2..."
        array.Clear();

        wxStringTokenizer tkz( value, delimiter );
        while ( tkz.HasMoreTokens() )
        {
            wxString token = tkz.GetNextToken();
            long long_value = 0;
            if( token.ToLong( &long_value, 10 ) )
            {
                array.Add( long_value );
            }
        }

    }
    return fDefault;
}
