#ifndef INVITEDIALOG_H
#define INVITEDIALOG_H

//(*Headers(InviteDialog)
#include <wx/panel.h>
#include <wx/dialog.h>
#include <wx/html/htmlwin.h>
//*)
#include "controller.h"

class InviteDialog: public wxDialog
{
	public:

		InviteDialog(wxWindow* parent);
		virtual ~InviteDialog();

        bool ShowInvite(meeting_invite_t invite);

		//(*Declarations(InviteDialog)
		wxPanel* Panel1;
		wxHtmlWindow* m_HtmlWindow;
		//*)

	protected:

		//(*Identifiers(InviteDialog)
		//*)
        void OnInviteAccepted(wxCommandEvent& event);
        void OnInviteRejected(wxCommandEvent& event);

	private:

		//(*Handlers(InviteDialog)
		//*)

		DECLARE_EVENT_TABLE()
};

#endif
