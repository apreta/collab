/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef _CONTACTSEARCH_H_
#define _CONTACTSEARCH_H_

#include <wx/arrstr.h>

class wxComboBox;

class ContactSearch
{
public:
    ContactSearch();

    enum GroupMask
    {
        Groups_None =     0x0000000,  /**< Search no particular groups */
        Groups_Global =   0x0000001,  /**< Search global groups */
        Groups_Personal = 0x0000002,  /**< Search personal groups */
        Groups_All =      0x7FFFFFF   /**< Search all groups */
    };

    /**
      * Encode the content of this instance into a wxArrayString
      */
    void Encode( wxArrayString & data ) const;

    /**
      * Encode the content of this instance into a wxString
      */
    void Encode( wxString & data ) const;

    /**
      * Decode a wxArrayString into the content of this instance.
      */
    void Decode( const wxArrayString & data );

    /**
      * Decode a wxString into the content of this instance.
      */
    void Decode( const wxString & data );

    /** Assignment */
    ContactSearch & operator=( const ContactSearch & rhs );

    /** Utility function used to populate db field selection Combo Boxes
      */
    void PopulateDatabaseFieldCombo( wxComboBox *pCombo );

    /** Return the display string for a db field index.
      * @param ndx The field index.
      */
    wxString DatabaseDisplayField( int ndx );

    /** Return the field string for a db field index.
      * @param ndx The field index.
      */
    wxString DatabaseField( int ndx );

private:
    /** Initialize the contact record fields */
    void InitializeContactFields();

    /** Append a new dbField / displayField name pair */
    void AddField( const wxString & dbField, const wxString & displayField );

public:
    /**
      * Format a search results title for this criteria.
      * @return wxString Return the formatted text.
      */
    wxString FormatDescription() const;

    /** The name of this search critera:<br>
      * NOTE: This name string is NOT included in the Encode() / Decode()
      * operations.
      */
    wxString m_strSearchName;

    bool m_fFirstName;          /**< Search by first name */
    wxString m_strFirstName;    /**< The first name text */

    bool m_fLastName;           /**< Search by last name */
    wxString m_strLastName;     /**< The last name text */

    bool m_fEmailAddresses;     /**< Search by email addresse */
    wxString m_strEmailAddress; /**< The email addresses text */

    bool m_fPhoneNumbers;       /**< Search by phone numbers */
    wxString m_strPhoneNumbers; /**< The phone number text */

    bool m_fMore1;              /**< Include additional criteria 1 */
    int m_nField1;              /**< Criteria 1 field name */
    wxString m_strField1Text;   /**< Criteria 1 text */

    bool m_fMore2;              /**< Include additional criteria 2 */
    int m_nField2;              /**< Criteria 2 field name */
    wxString m_strField2Text;   /**< Criteria 2 text */

    bool m_fMore3;              /**< Include additional criteria 3 */
    int m_nField3;              /**< Criteria 3 field name */
    wxString m_strField3Text;   /**< Criteria 3 text */

    int m_nSelectedGroups;      /**< The selected group mask */

    int m_nMaxResults;          /**< The number of results */

private:
    wxArrayString m_dbField;
    wxArrayString m_displayField;
};

#endif
