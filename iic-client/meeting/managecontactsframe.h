/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef MANAGECONTACTSFRAME_H
#define MANAGECONTACTSFRAME_H

//(*Headers(ManageContactsFrame)
#include <wx/panel.h>
#include <wx/treelistctrl.h>
#include <wx/button.h>
#include <wx/frame.h>
//*)

#include "contacttreelist.h"
#include "wx/propgrid/propgrid.h"

class ManageContactsFrame: public wxFrame
{
	public:
		ManageContactsFrame(wxWindow* parent, ManageContactsFrame **ppThis, wxWindowID id = -1);
		virtual ~ManageContactsFrame();

        void DisplayContacts();
        void ConnectionReconnected();

    public:
        /** Popup menu id values */
        enum MenuID
        {
            Help_About = wxID_ABOUT,

            Options_SignOff = wxID_HIGHEST,
            // Options_Preferences,  // wxID_PREFERENCES


            ///////////////////////////////////////////////////
            Search_New = wxID_HIGHEST + 100,
            Search_Continue,
            Search_Remove,
            Search_Edit,
            ////////////////////////////////////////////////////
            User_New,
            Contact_New,
            Group_New,
            Contact_Refresh,
            Profiles_Manage,
            ////////////////////////////////////////////////////
            Buddies_Add,
            Buddies_AddByScreen,
            Buddies_Remove,
            IM_Send,
            ///////////////////////////////////////////////////
            Edit_Copy,
            Edit_Paste,
            Edit_Delete,
            Edit_Edit,
            Edit_RemoveFromGroup,
            Edit_EmailAccountInfo,
            ///////////////////////////////////////////////////
            Meeting_Center,
            Meeting_StartInstant,
            Meeting_Schedule,
            ///////////////////////////////////////////////////
            Help_Contents
        };

        /**
          * Command mask values:<br>
          * Some simple processing rules:<br>
          *
          */
        enum CommandMaskValues
        {
            Cmd_Search_Continue         = 0x00000001,
            Cmd_Search_Remove           = 0x00000002,
            Cmd_Search_Edit             = 0x00000004,
            Cmd_User_New                = 0x00000008,
            Cmd_Contact_New             = 0x00000010,
            Cmd_Group_New               = 0x00000020,
            Cmd_Buddies                 = 0x00000040,
            Cmd_Buddies_Add             = 0x00000080,
            Cmd_Buddies_AddByScreen     = 0x00000100,
            Cmd_Buddies_Remove          = 0x00000200,
            Cmd_Edit_Edit               = 0x00000400,
            Cmd_Edit_Copy               = 0x00000800,
            Cmd_Edit_Paste              = 0x00001000,
            Cmd_Edit_Delete             = 0x00002000,
            Cmd_Edit_RemoveFromGroup    = 0x00004000,
            Cmd_Edit_EmailAccountInfo   = 0x00008000
        };

        /**
          * StateFlags: m_nState is a bitmask made up of these values.
          */
        enum StateFlags
        {
            State_Idle =                0x00000000, /**< No state */
            State_Sorting =             0x00000001, /**< Sort is in progress */
            State_HaveCopiedContacts =  0x00000002, /**< Contacts have been copied */
            State_WaitingForUpdate =    0x00000004  /**< Waiting for address book update */
        };

        /**
          * Return true if the specified state flag(s) are set.
          * @param flags The State_Xxxxx mask.
          * @return bool Returns true if one or more of the flags bits are set in m_nState.
          */
        bool IsState( int flags )
        {
            return (m_nState & flags) != 0;
        }

        /**
          * Clear the specified state flag(s).
          * @param flags The State_Xxxxx mask.
          */
        void ClearState( int flags )
        {
            m_nState &= ~flags;
        }
        /**
          * Set the specified state flag(s).
          * @param flags The State_Xxxxx mask.
          */
        void SetState( int flags )
        {
            m_nState |= flags;
        }

		//(*Identifiers(ManageContactsFrame)
		//*)

        // Non wxSmith handlers

        /** OK/Cancel processing */
        void OnOK(wxCommandEvent& event);

        /** Server results processing */
        void OnDirectoryStarted(wxCommandEvent& event);
        void OnDirectoryFetched(wxCommandEvent& event);
        void OnScheduleError(wxCommandEvent& event);
        void OnSessionError(SessionErrorEvent& event);
        void OnAddressbkUpdateFailed(wxCommandEvent& event);
        void OnAddressUpdated(wxCommandEvent& event);
        void OnAddressDeleted(wxCommandEvent& event);

        /** Search menu processing */
        void OnSearchNew( wxCommandEvent& event );
        void OnSearchContinue( wxCommandEvent& event );
        void OnSearchEdit( wxCommandEvent& event );
        void OnSearchRemove( wxCommandEvent& event );

        /** Sort control */
        void OnContactListColumnLeftClick(wxListEvent& event);
        void SortContacts( const wxTreeItemId & item, bool fSortParent = false );
        void SortContacts();

        /** New menu processing */
        void OnNewButtonClicked(wxCommandEvent& event);
        void OnUserNew(wxCommandEvent& event);
        void OnContactNew(wxCommandEvent& event);
        void OnGroupNew(wxCommandEvent& event);
        void OnContactsRefresh(wxCommandEvent& event);

        /** Edit menu processing */
        void OnEditEdit(wxCommandEvent& event);
        void OnEditCopy(wxCommandEvent& event);
        void OnEditPaste(wxCommandEvent& event);
        void OnEditDelete(wxCommandEvent& event);
        void OnEditRemoveFromGroup(wxCommandEvent& event);
        void OnEditEmailAccountInfo(wxCommandEvent& event);

        /** Buddies menu processing */
        void OnBuddiesButtonClicked(wxCommandEvent& event);
        void OnBuddiesAdd(wxCommandEvent& event);
        void OnBuddiesAddByScreen(wxCommandEvent& event);
        void OnBuddiesRemove(wxCommandEvent& event);
        void OnIMSend(wxCommandEvent& event);

        /** Meeting menu */
        void OnMeetingCenter(wxCommandEvent& event);
        void OnMeetingStart(wxCommandEvent& event);
        void OnMeetingSchedule(wxCommandEvent& event);

        /** Help menu processing */
        void OnHelpContents(wxCommandEvent& event);
        void OnHelpAbout(wxCommandEvent& event);
        void OnHelpContext(wxCommandEvent& event);

        /** Admin menu processing */
        void OnProfileManage(wxCommandEvent& event);

        /** Options menu processing */
        void OnSignOffEvent(wxCommandEvent& event);

        /** Close processing */
        void OnClose( wxCloseEvent& event );
        void OnMenuClose( wxCommandEvent& event );
        void OnExit( wxCommandEvent& event );

        /** Size tracking */
        void OnSize( wxSizeEvent& event );
        void OnMove( wxMoveEvent& event );

	protected:

		//(*Handlers(ManageContactsFrame)
		void OnSearchButtonClick(wxCommandEvent& event);
		void OnContactsTreeListCtrlItemRightClick(wxTreeEvent& event);
		void OnContactsTreeListCtrlSelectionChanged(wxTreeEvent& event);
		//*)
        void OnProfileFetched(wxCommandEvent& event);

		//(*Declarations(ManageContactsFrame)
		wxButton* m_EditButton;
		wxPanel* m_ContactsPanel;
		wxButton* m_SearchButton;
		wxTreeListCtrl* m_ContactsTreeListCtrl;
		wxButton* m_NewButton;
		wxButton* m_DeleteButton;
		//*)

    protected:
        /** Initialize the GUI.<br>
          * This is called right after the GUI is loaded from disk.
          */
        void Initialize();

        /** Reload contact tree from address book. */
        void RefreshContacts();

        /** Set the UI feedback based on the current m_nCmdMask value */
        void SetUIFeedback();

        /** Set the available command mask based on the current selections in the tree.<br>
          * The m_nCmdMask member is populated with the CommandMaskValues Cmd_Xxxxx values that
          * indicate what menu items and/or buttons should be enabled.<br>
          * The SetUIFeedback() method is called once the command mask is defined.
          */
        void SetAvailableCommands();

        /** See if the current user can delete all of the selected
          * contact records.
          * @param selections An array of selected nodes.
          * @return bool Returns true if the user is allowed to delete one or more of the selected records.
          */
        bool CanDeleteSomeContacts( wxArrayTreeItemIds & selections );


        /** See if the current user can delete all of the selected
          * groups.
          * @param selections An array of selected nodes.
          * @return bool Returns true if the user is allowed to delete one or more of the selected groups.
          */
        bool CanDeleteSomeGroups( wxArrayTreeItemIds & selections );

        /** See if the current user can delete the specified contact
          * @param item The tree item id
          * @return bool Returns true if the user is allowed to delete the contact record.
          */
        bool CanDeleteContact( wxTreeItemId & item );

        /** See if the current user can delete the specified group
          * @param item The tree item id
          * @return bool Returns true if the user is allowed to delete the group.
          */
        bool CanDeleteGroup( wxTreeItemId & item );

        /** Delete the specified item
          * @param item The tree item to be deleted.
          * @return int Returns zero if successful. Returns non-zero on failure.
          */
        int DeleteItem( wxTreeItemId & item );

        /**
          * Check the currently selected tree items and determine if
          * a delete operation is allowed.<br>
          * If the delete operation is allowed then the user is asked to confirm the delete.
          * @return bool Returns true if a delete operation is allowed.
          */
        bool ConfirmDelete( bool fShowErrorMsg = false );

        /**
          * Check the currently selected tree items and determine if
          * a delete operation is allowed. If the delete operation is not
          * allowed then an optional error message can be displayed.<br>
          * @param fShowErrorMsg If true and a delete operation is not allowed then an appropriate error message is displayed.
          * @return bool Returns true if a delete operation is allowed.
          */
        bool DeleteAllowed( bool fShowErrorMsg = false );

        /** Create a new contact in the specified directory.<br>
          * The new contact record is created in the "All" folder
          * of the specified dir.
          * @param dir The target directory for the new contact record.
          * @param type The type of contact record to be created (UserTypeContact,UserTypeUser,UserTypeAdmin)
          * @param node Output: The ContactTreeList node where the contact was inserted.
          * @return int Returns zero if successful. Returns non-zero on failure.
          */
        int CreateNewContact( directory_t dir, int type, wxTreeItemId & node );

        /** Attempt to edit the currently selected node.
          */
        int EditSelectedNode();

        /** Add a contact record to the specified group.<br>
          * If the
          * @param groupId The id of the group
          */
        int AddGroupMember( group_t group, contact_t contact );

        /** Remove a contact record from the specified group.<br>
          * If the
          * @param groupId The id of the group
          */
        int RemoveGroupMember( group_t group, contact_t contact );

        /** Add contact to personal address book and make it a buddy.
          */
        int AddBuddy( contact_t contact );

        /** Remove buddy contact.
          */
        int RemoveBuddy( contact_t contact );

        /** Invite/schedule contact to meeting.
          */
        int InviteItem(const wxChar *command, wxTreeItemId & item);

    private:
        /**
          * Wait for state flags to clear.<br>
          * If the operation times out, then the flags are cleared.
          * @param flags The State flags to wait for.
          * @param nSeconds The number of seconds to wait.
          * @param errStr The error message string if the wait times out (Optional)
          * @return int Returns zero if the flags were cleared prior to timeout. Returns -1 on timeout.
          */
        int WaitForClear( int flags, int nSeconds, const wxString & errMsg );

        void ClearDatabaseError()
        {
            SetDatabaseError( 0, wxT("") );
        }

        void SetDatabaseError( int nDatabaseError, const wxString & strDatabaseError, bool fShowError = false )
        {
            m_nDatabaseError = nDatabaseError;
            m_strDatabaseError = strDatabaseError;
            if( fShowError )
            {
                ShowDatabaseError();
            }
        }

        void ShowDatabaseError();

        void goModeless();
        void goModal();

        /**
          * Save the specified contact.<br>
          * @param dir The directory that contains the contact record.
          * @param group_node The tree node that is to own the new contact
          * @param contact The contact record.
          */
        int SaveContact( directory_t dir, wxTreeItemId & group_node, contact_t contact, wxString & strPassword );

        /**
          * Start a new search.
          */
        void StartNewSearch();

        pstring     allocField( directory_t dir, const wxString & strField );
        pstring     allocField( directory_t dir, const char *strField );

	private:
        wxString m_strCaption;              /**< The caption for all message boxes */
        ContactTreeList m_ContactTree;      /**< The contact tree list handler */
        wxTreeItemId m_CommunityNode;       /**< The Top level community contacts node */
        wxTreeItemId m_PersonalNode;        /**< The Top level personal node */
        wxTreeItemId m_SearchNode;          /**< The Top level search node */
        int m_nCmdMask;                     /**< The current command mask */
        int m_nState;                       /**< The current run state */
        wxMenuBar *m_MainMenuBar;           /**< The main menu */
        wxMenu *m_ManageContactsPopup;      /**< The manage contacts menu */
        wxMenu *m_EditPopup;                /**< The Edit menu */
        wxMenu *m_AdminPopup;               /**< The Admin menu */
        wxMenu *m_OptionsMenu;              /**< The options menu */
        wxMenu *m_SearchMenu;               /**< The menu bar search menu */
        wxMenu *m_MeetingsMenu;             /**< The menu bar meetings menu */
        wxMenu *m_HelpMenu;                 /**< The menu bar help menu */
        wxMenu *m_SearchPopup;              /**< The search node context menu */
        wxMenu *m_MainPopup;                /**< The standard node context menu */
        wxMenu *m_NewPopup;                 /**< The New button popup */
        wxMenu *m_BuddiesPopup;             /**< The Buddies button popup */
        wxString m_strDatabaseError;        /**< The last known database error message */
        int m_nDatabaseError;               /**< The last known database error code */
        ContactTreeListData m_Clipboard;    /**< The internal clipboard */
        ManageContactsFrame **m_ppThis;     /**< Ptr. to the Ptr. holding "this" pointer. *ppThis is cleared on exit. */

	private:
        directory_t     m_Dir;              /**< Dir of the contact being edited.  */
        wxArrayString   m_arrPolicyNames;

		DECLARE_EVENT_TABLE()
};

#endif
