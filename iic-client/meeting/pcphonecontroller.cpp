/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 *
 *
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */
#include "app.h"
#include <wx/stdpaths.h>
#include <wx/xrc/xmlres.h>
#include "pcphonecontroller.h"
#include "meetingmainframe.h"
#include "meetingcentercontroller.h"
#include "VoipService.h"
#include "pcphonepanel.h"

/**
  * Notes:
  * Accessing meeting_details_t from within this class:

  MeetingMainFrame *pMeetingMainFrame = (MeetingMainFrame *)m_parent;
  wxString strMID;
  strMID = pMeetingMainFrame->GetMeetingID();

  meeting_t mtg = controller_find_meeting( CONTROLLER.controller, strMID.mb_str(wxConvUTF8) );
  if (mtg != NULL)
  {
    meeting_details_t   details         = controller_get_meeting_details(CONTROLLER.controller, mtg, 0, NULL);
  }

    details-> pstring bridge_phone;
	details-> system_url;

  */
PCPhoneController::PCPhoneController( wxWindow * parent ):
m_parent(parent),
m_pPCPhoneDialDialog(0),
m_fVoipServerInitiated(false),
m_timer(this)
{
    m_strStatusIdle = _("Not connected");
	m_strStatusConnecting = _("Connecting");
	m_strStatusConnected = _("Connected");
	m_strRinging = _("Ringing");
	m_strStatusReleasing = _("Releasing");
    m_strStatusInitFail = _("Unable to initialize VoIP Service.  Ensure network and audio devices are functioning properly.");
    m_strStatusInitNoMic = _("Unable to initialize VoIP Service.  No Microphone found.");
	m_strStatusFailed = _("Unable to establish audio connection to conference server.\nPlease ask you network administrator to allow outbound UDP traffic on port 4569.");
	m_strStatusTimeout = _("Timeout waiting to establish audio connection to conference server.\nPlease ask you network administrator to allow outbound UDP traffic on port 4569.");
	m_strStatusIncoming = _("Incoming web call from %s");
	m_strVoipStatus = wxT("");

    m_voip = new CVoipService( this ); // Initialize w/o external events
    m_voip->MuteMicrophone(FALSE);
    m_voip->MuteSpeaker(FALSE);
}

PCPhoneController::~PCPhoneController()
{
    if( m_voip )
    {
        m_voip->Shutdown();
        delete m_voip;
        m_voip = 0;
    }
}

void PCPhoneController::ConnectPCPhonePanel()
{
    MeetingMainFrame *pMMF = (MeetingMainFrame *)m_parent;
    PCPhonePanel *pPanel = pMMF->PhonePanel();
    if( !pPanel )
    {
        wxLogDebug(wxT("PCPhoneController - ConnectPCPhonePanel - No PCPhonePanel found"));
        return;
    }

    // Connect the Connect/Disconnect button
    pPanel->Connect( XRCID("ID_CONNECTDISCONNECT_BUTTON"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&PCPhoneController::OnReverseConnection, 0, this );

    // Connect the Dial button
    pPanel->Connect( XRCID("ID_DIAL_BUTTON"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&PCPhoneController::OnDialPad, 0, this );

    // Connect the audio properties button
    pPanel->Connect( XRCID("ID_AUDIOPROPERTIES_BUTTON"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&PCPhoneController::OnAudioProperties, 0, this );

    pPanel->Connect( XRCID("ID_MUTE_CHECK"), wxEVT_COMMAND_CHECKBOX_CLICKED, (wxObjectEventFunction)&PCPhoneController::OnMicrophoneMuteClick, 0, this );
    pPanel->Connect( XRCID("ID_SPEAKER_MUTE"), wxEVT_COMMAND_CHECKBOX_CLICKED, (wxObjectEventFunction)&PCPhoneController::OnSpeakerMuteClick, 0, this );

    Connect( wxID_ANY, wxEVT_VOIP_STATUS, (wxObjectEventFunction)&PCPhonePanel::UpdateVoipStatus, 0, pPanel );
    Connect( wxID_ANY, wxEVT_VOIP_LEVELS, (wxObjectEventFunction)&PCPhonePanel::UpdateVoipLevels, 0, pPanel );

    Connect( wxID_ANY, wxEVT_TIMER, wxTimerEventHandler(PCPhoneController::OnTimer) );

    wxLogDebug(wxT("PCPhoneController - ConnectPCPhonePanel - Done"));
}

void PCPhoneController::OnConnectToMeeting(wxCommandEvent& event)
{
    ConnectToMeeting();
}

void PCPhoneController::OnDisconnect(wxCommandEvent& event )
{
    Disconnect();
}


void PCPhoneController::OnMicrophoneMuteClick(wxCommandEvent& event)
{
    if (event.IsChecked())
        m_voip->MuteMicrophone(TRUE);
    else
        m_voip->MuteMicrophone(FALSE);
}

void PCPhoneController::OnSpeakerMuteClick(wxCommandEvent& event)
{
    if (event.IsChecked())
        m_voip->MuteSpeaker(TRUE);
    else
        m_voip->MuteSpeaker(FALSE);
}

void PCPhoneController::OnReverseConnection(wxCommandEvent& event)
{
    if( IsInCall() )
    {
        Disconnect();
    }else
    {
        ConnectToMeeting();
    }
}

bool PCPhoneController::Disconnect()
{
    HangupCall();

    return true;
}

bool PCPhoneController::ConnectToMeeting()
{
    bool fOK = false;

/**
NOTES: The aDirect flag is derived from the
invitee

enum InviteType {
	InviteNormal = 0,
	InvitePC*Phone,
	InvitePCPhoneDirect <<<<<< Set aDirectFlag to true if

I don't know where this information is stored in the new code ?

};
**/

    MakeVoipCall( false, true );

    return fOK;
}

void PCPhoneController::MakeVoipCall(bool aDirectFlag, bool aServerInviteFlag)
{
    wxLogDebug(wxT("PCPhoneController::MakeVoipCall"));

    // NOTE: All of the calls made to m_voip->SetOption() must be in synch with the
    // related code in iiciax.c in the iaxlib project ( See VoipSetOption() )

    // Get to the current meeting details
    MeetingMainFrame *pMeetingMainFrame = (MeetingMainFrame *)m_parent;
    wxString strMID;
    strMID = pMeetingMainFrame->GetMeetingID();

    wxString strPID;
    strPID = pMeetingMainFrame->GetParticipantID();

    wxString strBridgeAddress;
    meeting_t mtg = controller_find_meeting( CONTROLLER.controller, strMID.mb_str(wxConvUTF8) );
    if (mtg != NULL)
    {
        strBridgeAddress = wxString( mtg->bridge_address, wxConvUTF8 );
    }
    else
    {
        wxLogDebug(wxT("PCPhoneController::MakeVoipCall - Cannot find meeting_t"));
    }

    if( PREFERENCES.m_nAudioSubsystem == Voip_Audio_Internal_PA )
        m_voip->SetOption("audiosubsystem", "pa" );
    else if( PREFERENCES.m_nAudioSubsystem == Voip_Audio_Internal_Alsa )
        m_voip->SetOption("audiosubsystem", "alsa");

    wxString userDir = wxStandardPaths::Get().GetUserDataDir();
    wxString dataDir = wxStandardPaths::Get().GetDataDir();

    int stat = m_voip->Initialize(userDir.mb_str());
    if (stat != IICVOIP_OK)
    {
        wxLogDebug( wxT("PC Phone initialization failure"));
        EmitStatus(PCPhoneStatus_Failed, stat == -2 ? m_strStatusInitNoMic : m_strStatusInitFail);
        m_voip->Shutdown();
        return;
    }

    if (CONTROLLER.IsVOIPEncrypted())
    {
        wxString certFile = dataDir + wxT("/cert.pem");

        m_voip->SetOption("certfile", certFile.mbc_str());
        m_voip->SetOption("encrypt", "blowfish");
    }

    m_fVoipServerInitiated = aServerInviteFlag;

    wxString strPin;
    strPin = (aDirectFlag) ? wxT("super") : wxT("pin");
    strPin += strPID;

    wxString lowCodec(OPTIONS.GetSystem("LowCodec", "ilbc"), wxConvUTF8);
    m_voip->SetOption("lowcodec", lowCodec.mb_str(wxConvUTF8) );

    wxString medCodec(OPTIONS.GetSystem("MediumCodec", "g726"), wxConvUTF8);
    m_voip->SetOption("mediumcodec", medCodec.mb_str(wxConvUTF8) );

    wxString voipBandwidth;
    if( PREFERENCES.m_nPCPhoneBandwidth == 0 )
        voipBandwidth = wxT("auto");
    else if( PREFERENCES.m_nPCPhoneBandwidth == 1 )
        voipBandwidth = wxT("low");
    else if( PREFERENCES.m_nPCPhoneBandwidth == 2 )
        voipBandwidth = wxT("medium");
    else
        voipBandwidth = wxT("high");
    m_voip->SetOption("bandwidth", voipBandwidth.mb_str(wxConvUTF8) );

    wxString voipVAD;
    if( PREFERENCES.m_nPCPhoneVAD == 0 )
        voipVAD = wxT("low");
    else if( PREFERENCES.m_nPCPhoneVAD == 1 )
        voipVAD = wxT("medium");
    else
        voipVAD = wxT("high");
    m_voip->SetOption("speechprobability", voipVAD.mb_str(wxConvUTF8) );

    wxString maxJitter( wxString::Format( _T("%d"), PREFERENCES.m_nMaxJitter) );
    m_voip->SetOption("maxjitter", maxJitter.mb_str(wxConvUTF8) );

    wxString silenceThreshold( wxString::Format( _T("%d"), PREFERENCES.m_nSilenceThreshold) );
    m_voip->SetOption("silencethreshold", silenceThreshold.mb_str(wxConvUTF8) );

    wxString suppressionThreshold = wxString::Format( _T("%f"), ((double)PREFERENCES.m_nSuppressionThreshold) / 100.0);
    m_voip->SetOption("suppressionthreshold", suppressionThreshold.mb_str(wxConvUTF8) );

    m_voip->SetOption("echocanceller", PREFERENCES.m_fEnableEchoCancellation ? "1" : "0");

    m_voip->SetOption("noisefilter", PREFERENCES.m_fEnableNoiseFilter ? "1" : "0");

    // Call bridge using VoIP phone
    wxCharBuffer pin = strPin.mb_str();
    wxCharBuffer address = strBridgeAddress.mb_str( wxConvUTF8 );

    if (m_voip->MakeCall( pin, address ) == IICVOIP_OK)
    {
        EmitStatus( PCPhoneStatus_Connecting, m_strStatusConnecting );

        m_timer.Start( DefaultConnectionTimeout, wxTIMER_ONE_SHOT );
    }
    else
    {
        EmitStatus( PCPhoneStatus_Failed, m_strStatusFailed );
    }
}

bool PCPhoneController::IsInCall()
{
    if( m_voip->IsInCall() )
        return true;

    return false;
}

void PCPhoneController::SetMuted(bool muted)
{
    m_voip->MuteMicrophone(muted);

    // Also notify panel so it can change button state if needed
    MeetingMainFrame *pMMF = (MeetingMainFrame *)m_parent;
    PCPhonePanel *pPanel = pMMF->PhonePanel();
    if( pPanel )
        pPanel->SetMuted(muted);
}

void PCPhoneController::HangupCall()
{
	if (m_voip->IsInCall())
	{
		m_voip->Hangup();
		if (m_strVoipStatus != m_strStatusIdle)
			EmitStatus( PCPhoneStatus_Releasing, m_strStatusReleasing );

		// In case call was still connecting and was made at server request, let server know we are done with it
		if (m_fVoipServerInitiated)
		{
            //m_pDocument->m_Meeting.HangupParticipant(strMyId);
		}
	}

}

void PCPhoneController::OnTimer( wxTimerEvent& event )
{
    if (event.GetInterval() == ShutdownTimer)
    {
        if (!IsInCall())
        {
            wxLogDebug( wxT("Shutting down voip client") );
            m_voip->Shutdown();
        }
    }
    else
    {
        wxLogDebug( wxT("PC Phone connection timeout"));
        EmitStatus( PCPhoneStatus_Failed, m_strStatusFailed );
    }
}

void PCPhoneController::OnRinging(int)
{
    EmitStatus( PCPhoneStatus_Ringing, m_strRinging );
}

void PCPhoneController::OnEstablished(int)
{
    m_timer.Stop();
    EmitStatus( PCPhoneStatus_Connected, m_strStatusConnected );
}

void PCPhoneController::OnCleared(int)
{
    m_timer.Stop();
    EmitStatus( PCPhoneStatus_Idle, m_strStatusIdle );

    m_timer.Start( ShutdownTimer, wxTIMER_ONE_SHOT );
}

void PCPhoneController::OnLevels(float input, float output)
{
    // Mic level: -32 to 0
	int mic_level = (int)input;
	if( mic_level < -32 ) mic_level = -32;
	else if( mic_level > 0 ) mic_level = 0;

    // Vol level: -40 to 0
	int vol_level = (int)output;
	if( vol_level < -40 ) vol_level = -40;
	else if( vol_level > 0 ) vol_level = 0;

    wxCommandEvent ev(wxEVT_VOIP_LEVELS, wxID_ANY);
    ev.SetInt( vol_level );
    ev.SetExtraLong( mic_level );
    AddPendingEvent(ev);
}

void PCPhoneController::EmitStatus( PCPhoneStatus status, const wxString & msg )
{
    m_strVoipStatus = msg;

    wxString strLog;

    strLog = wxT("PCPhoneController - ");
    strLog += msg;
    wxLogDebug( strLog );

    wxCommandEvent ev(wxEVT_VOIP_STATUS, wxID_ANY);
    ev.SetInt( (int)status );
    ev.SetString( msg );
    AddPendingEvent(ev);
}

void PCPhoneController::On_TT1( wxCommandEvent& event )
{
    Dial("1");
}

void PCPhoneController::On_TT2( wxCommandEvent& event )
{
    Dial("2");
}

void PCPhoneController::On_TT3( wxCommandEvent& event )
{
    Dial("3");
}

void PCPhoneController::On_TT4( wxCommandEvent& event )
{
    Dial("4");
}

void PCPhoneController::On_TT5( wxCommandEvent& event )
{
    Dial("5");
}

void PCPhoneController::On_TT6( wxCommandEvent& event )
{
    Dial("6");
}

void PCPhoneController::On_TT7( wxCommandEvent& event )
{
    Dial("7");
}

void PCPhoneController::On_TT8( wxCommandEvent& event )
{
    Dial("8");
}

void PCPhoneController::On_TT9( wxCommandEvent& event )
{
    Dial("9");
}

void PCPhoneController::On_TT0( wxCommandEvent& event )
{
    Dial("0");
}

void PCPhoneController::On_TTStar( wxCommandEvent& event )
{
    Dial("*");
}

void PCPhoneController::On_TTPound( wxCommandEvent& event )
{
    Dial("#");
}


void PCPhoneController::Dial( const char *pszText )
{
    if( IsInCall() )
    {
        //TODO: Play a sound. (TT Tone would be nice, click will do) PLF
        wxString str( pszText, wxConvUTF8 );
        wxLogDebug(wxT("Dialing %s"), str.c_str() );

        m_voip->Dial( pszText );
    }
}

void PCPhoneController::OnAudioProperties( wxCommandEvent& event )
{
    wxLogDebug(wxT("Volume properties"));

#ifdef WIN32
    bool isVista = false;
	DWORD dwVersion = GetVersion();

	if (dwVersion < 0x80000000) {
		// Get the Windows version.
		DWORD dwWindowsMajorVersion =  (DWORD)(LOBYTE(LOWORD(dwVersion)));

		if (dwWindowsMajorVersion < 0x80 && dwWindowsMajorVersion >= 6)              // Windows NT/2000, Whistler
            isVista = true;
	}

    if (isVista)
    {
        wxExecute( wxT("control MMSYS.CPL") );
    }
    else
    {
        wxExecute( wxT("rundll32 MMSYS.CPL,ShowAudioPropertySheet") );
    }
#endif

#ifdef LINUX
    wxExecute( wxT("gnome-volume-control") );
#endif

}

void PCPhoneController::OnDialPad(wxCommandEvent& event)
{
    if( !m_pPCPhoneDialDialog )
    {
            m_pPCPhoneDialDialog = new PCPhoneDialDialog( m_parent, wxID_ANY );

            // Connect the GUI
            m_pPCPhoneDialDialog->Connect( wxID_OK, wxEVT_COMMAND_BUTTON_CLICKED,  (wxObjectEventFunction)&PCPhoneController::OnPCPhoneDialDialogOK, 0, this );

            m_pPCPhoneDialDialog->Connect( XRCID("ID_TTPAD0_BUTTON"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&PCPhoneController::On_TT0, 0, this );
            m_pPCPhoneDialDialog->Connect( XRCID("ID_TTPAD1_BUTTON"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&PCPhoneController::On_TT1, 0, this );
            m_pPCPhoneDialDialog->Connect( XRCID("ID_TTPAD2_BUTTON"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&PCPhoneController::On_TT2, 0, this );
            m_pPCPhoneDialDialog->Connect( XRCID("ID_TTPAD3_BUTTON"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&PCPhoneController::On_TT3, 0, this );
            m_pPCPhoneDialDialog->Connect( XRCID("ID_TTPAD4_BUTTON"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&PCPhoneController::On_TT4, 0, this );
            m_pPCPhoneDialDialog->Connect( XRCID("ID_TTPAD5_BUTTON"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&PCPhoneController::On_TT5, 0, this );
            m_pPCPhoneDialDialog->Connect( XRCID("ID_TTPAD6_BUTTON"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&PCPhoneController::On_TT6, 0, this );
            m_pPCPhoneDialDialog->Connect( XRCID("ID_TTPAD7_BUTTON"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&PCPhoneController::On_TT7, 0, this );
            m_pPCPhoneDialDialog->Connect( XRCID("ID_TTPAD8_BUTTON"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&PCPhoneController::On_TT8, 0, this );
            m_pPCPhoneDialDialog->Connect( XRCID("ID_TTPAD9_BUTTON"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&PCPhoneController::On_TT9, 0, this );
            m_pPCPhoneDialDialog->Connect( XRCID("ID_TTPADSTAR_BUTTON"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&PCPhoneController::On_TTStar, 0, this );
            m_pPCPhoneDialDialog->Connect( XRCID("ID_TTPADPOUND_BUTTON"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&PCPhoneController::On_TTPound, 0, this );
    }
    if( !m_pPCPhoneDialDialog )
    {
        return;
    }
    m_pPCPhoneDialDialog->Show(true);

}

void PCPhoneController::OnPCPhoneDialDialogOK(wxCommandEvent& event)
{
    if( m_pPCPhoneDialDialog )
    {
        m_pPCPhoneDialDialog->Show(false);
    }
}

