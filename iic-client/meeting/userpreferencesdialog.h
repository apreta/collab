/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef USERPREFERENCESDIALOG_H
#define USERPREFERENCESDIALOG_H

//(*Headers(UserPreferencesDialog)
#include <wx/notebook.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/checkbox.h>
#include <wx/listbox.h>
#include <wx/radiobut.h>
#include <wx/panel.h>
#include <wx/button.h>
#include <wx/dialog.h>
#include <wx/combobox.h>
//*)

#include "controls/wx/wxTextCtrlEx.h"
#include "controls/wx/wxTogBmpBtn.h"
#include "userpreferences.h"
#include "contacttreelist.h"

class UserPreferencesDialog: public wxDialog
{
	public:

		UserPreferencesDialog(wxWindow* parent,wxWindowID id = -1);
		virtual ~UserPreferencesDialog();

		//(*Identifiers(UserPreferencesDialog)
		//*)

        /**
          * Edit user preferences.
          * @param prefs The current preferences
          * @return int Returns wxID_OK if the preferences were changed.
          */
        int EditPreferences( UserPreferences & prefs );

        /**
          * Edit only the language panel of the user preferences.
          * @return int Returns wxID_OK if the preferences were changed.
          */
        int EditLanguageOnly( );

        /**
          * Is a contact refresh needed?
          */
        bool ContactRefreshNeeded( )    { return m_refreshContacts; }


    protected:
        /** Set up language select tab */
        void InitLanguages();

        /** Apply the settings in the current user's profile to the UI */
        void ApplyCurrentProfileSettings();

        /** Display current prefs */
        void Value_to_GUI();

        /** Gather changed prefs */
        void GUI_to_Value();

        /** Update the app font sample text */
        void UpdateAppFontSample();

		//(*Handlers(UserPreferencesDialog)
		void OnAppFontBtnClick(wxCommandEvent& event);
		void OnSavePasswordCheckClick(wxCommandEvent& event);
		void OnAutoSignOnCheckClick(wxCommandEvent& event);
		void OnMoveToDisplayedButtonClick(wxCommandEvent& event);
		void OnMoveToAvailableButtonClick(wxCommandEvent& event);
		void OnMoveColumnsUpButtonClick(wxCommandEvent& event);
		void OnMoveColumnsDownButtonClick(wxCommandEvent& event);
		void OnConfigureOutlookButtonClick(wxCommandEvent& event);
		void OnDownloadOutlookCheckClick(wxCommandEvent& event);
		//*)

        /// Font Panel Handlers ////////////////////////////////////////////////////////////
        void OnClickAppFont( wxCommandEvent & event );
        void OnClickFore( wxCommandEvent & event );
        void OnClickBack( wxCommandEvent & event );
        void OnClickSmaller( wxCommandEvent & event );
        void OnClickNorm( wxCommandEvent & event );
        void OnClickLarger( wxCommandEvent & event );
        void OnClickBold( wxCommandEvent & event );
        void OnClickItalics( wxCommandEvent & event );
        void OnClickUline( wxCommandEvent & event );
        void OnClickHelp(wxCommandEvent& event);

        /** Size tracking */
        void OnSize( wxSizeEvent& event );
        void OnMove( wxMoveEvent& event );

		//(*Declarations(UserPreferencesDialog)
		wxRadioButton* m_MediumBandwidthRadio;
		wxCheckBox* m_DownloadCABCheck;
		wxCheckBox* m_CacheABCheck;
		wxPanel* m_Sounds;
		wxCheckBox* m_SavePasswordCheck;
		wxCheckBox* m_EnablePrivateMeetings;
		wxCheckBox* m_AutoSignOnCheck;
		wxComboBox* m_AudioSubsystemCombo;
		wxCheckBox* m_StartOnComputerStartCheck;
		wxRadioButton* m_HighBandwidthRadio;
		wxCheckBox* m_BuiltInIMCheck;
		wxTogBmpBtn* m_buttonSetItalics;
		wxListBox* m_LanguageList;
		wxPanel* m_Font;
		wxCheckBox* m_ShowInstantMeetingCheck;
		wxCheckBox* m_DisableInMeetingSoundsCheck;
		wxStaticText* StaticText2;
		wxStaticText* m_ChatFontLabel;
		wxStaticText* m_AudioSubsystemLabel;
		wxStaticText* m_ChatSoundsLabel;
		wxCheckBox* m_EnableNoiseFilterCheck;
		wxPanel* m_ContactDisplay;
		wxRadioButton* m_LowVadRadio;
		wxComboBox* m_ChatSoundsCombo;
		wxCheckBox* m_ReduceColorCheck;
		wxCheckBox* m_UsePowerPointConverterCheck;
		wxPanel* m_Language;
		wxRadioButton* m_MedVadRadio;
		wxPanel* m_DataShare;
		wxListBox* m_DisplayedColumnsListBox;
		wxTogBmpBtn* m_buttonSetSmaller;
		wxStaticText* StaticText1;
		wxStaticText* StaticText3;
		wxCheckBox* m_fKeepTryingConnectionCheck;
		wxRadioButton* m_HighVadRadio;
		wxCheckBox* m_AutoDetectSlowConnectionCheck;
		wxRadioButton* m_AutoBandwidthRadio;
		wxCheckBox* m_MuteAllSoundsCheck;
		wxCheckBox* m_EnableEchoCancellationCheck;
		wxCheckBox* m_PlayChatInvtSoundCheck;
		wxTextCtrlEx* m_ChatSampleEdit;
		wxButton* m_AppFontBtn;
		wxPanel* m_General;
		wxNotebook* m_Notebook;
		wxButton* m_MoveToAvailableButton;
		wxButton* m_ConfigureOutlookButton;
		wxListBox* m_AvailableColumnsListBox;
		wxButton* m_MoveColumnsUpButton;
		wxStaticText* StaticText5;
		wxPanel* m_PCPhone;
		wxButton* m_MoveToDisplayedButton;
		wxTogBmpBtn* m_buttonSetBold;
		wxTogBmpBtn* m_buttonSetNorm;
		wxTogBmpBtn* m_buttonSetFore;
		wxTextCtrl* m_FontSampleEdit;
		wxTogBmpBtn* m_buttonSetUline;
		wxTogBmpBtn* m_buttonSetLarger;
		wxTogBmpBtn* m_buttonSetBack;
		wxRadioButton* m_LowBandwidthRadio;
		wxButton* m_MoveColumnsDownButton;
		wxStaticText* m_InMeetingSoundsLabel;
		wxComboBox* m_InMeetingSoundsCombo;
		wxTextCtrl* m_MaxJitterEdit;
		wxCheckBox* m_UseDirectXCaptureCheck;
		wxStaticText* m_NetworkBandwidthLabel;
		wxCheckBox* m_DownloadPublicMeetingsCheck;
		wxCheckBox* m_HidePresenterMeetingWindowCheck;
		wxCheckBox* m_DownloadOutlookCheck;
		//*)

	private:
        /** The preferences being edited */
        UserPreferences m_prefs;

        /** The profile.enabled_features flags */
        unsigned m_enabled_features;

        /** The contact display settings controller */
        ContactDisplayManager m_ContactDisplayManager;

        /** Contacts need to be refreshed on exit */
        bool m_refreshContacts;

		DECLARE_EVENT_TABLE()
};

#endif
