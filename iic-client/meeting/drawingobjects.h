/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
// DrawingObjects.h: interface for the CDrawingObjects class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(DRAWINGOBJECTS_H)
#define DRAWINGOBJECTS_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "wx/window.h"

#include "iicdoc.h"

#include <list>
#include "iksemel.h"
#include "base64.h"

#include "dashline.h"

#define SZ_RESIZEBOX	6
#define SZ_RESIZEBORDER	4

class DocShareView;
class CBaseDrawingObject;
class CAction;
class CDashLine;


class IICRect
{
public:
    IICRect( wxPoint& ul, wxPoint& lr )         { left = ul.x; top = ul.y; right = lr.x; bottom = lr.y; }
    IICRect( wxPoint& ul, wxSize sz )           { left = ul.x; top = ul.y; right = left + sz.GetWidth( ); bottom = top + sz.GetHeight( ); }
    IICRect( int l, int t, int r, int b )   { left = l; top = t; right = r; bottom = b; }
    IICRect( )      { left = top = right = bottom = 0; }

    ~IICRect( )     { };

    int     Height( ) const  { return bottom - top; };
    int     Width( )  const  { return right - left; };

    void    SetRectEmpty( )     { left = top = right = bottom = 0; }
    bool    IsRectEmpty( ) const;

    void    NormalizeRect( );
    void    InflateRect( int x, int y );
    void    InflateRect( double dblZoom );
    void    ScaleRect( double dblZoom );
    bool    EqualRect( IICRect& r1 );
    bool    UnionRect( IICRect& r1, IICRect& r2 );
    bool    PtInRect( const wxPoint& pt );

    wxPoint     TopLeft( )  const   { return wxPoint( left, top ); }
    wxSize      Size( )     const   { return wxSize( Width( ), Height( ) ); }

    IICRect operator+(const wxPoint& pt);
    void    operator+=(const wxPoint& pt);
    void    operator+=(const wxSize& sz);
    void    operator-=(const wxPoint& pt);

    int     left, top, right, bottom;
};


// Style
enum {
    ST_NONE		= 0,
	ST_TRANSPARENT	= 1,
};

// Mouse position
enum {
    NO_SIDE = 0,
	RIGHT_SIDE,
	LEFT_SIDE,
	TOP_SIDE,
	BOTTOM_SIDE,
	TOPLEFT_SIDE,
	BOTTOMLEFT_SIDE,
	TOPRIGHT_SIDE,
	BOTTOMRIGHT_SIDE,
	HORZ_SIDE,
	LINE_START,
	LINE_END
};

// Mode
enum {
	BOX_MODE_IDLE		= 0,
	BOX_MODE_SELECTED	= 1,
	BOX_MODE_RESIZEING	= 2,
	BOX_MODE_DRAGGING	= 4,
	BOX_MULTI_SELECTION	= 8,
	BOX_MODE_DELETING	= 16
};

extern const wxString DOType_Line;
extern const wxString DOType_TextBox;
extern const wxString DOType_Rectangle;
extern const wxString DOType_Ellipse;
extern const wxString DOType_Rectangle_Opaque;
extern const wxString DOType_Ellipse_Opaque;
extern const wxString DOType_ArrowLine;
extern const wxString DOType_Image;
extern const wxString DOType_CheckMark;
extern const wxString DOType_RadioMark;
extern const wxString DOType_CurveLine;
extern const wxString DOType_HighLite;

typedef	std::list<wxPoint> POINTLIST;
typedef	std::list<BYTE> BYTELIST;
typedef std::list<CBaseDrawingObject*> OBJLIST;
typedef std::list<CAction*> ACTIONLIST;


class CDrawingObjects
{
public:
	CDrawingObjects();
	virtual ~CDrawingObjects();

	void Paint(wxDC* pDC, IICRect& rect, wxPoint& ptOffset);
	void GetXML(wxString& xml);
	void PaintSelection(wxDC* pDC, IICRect& rect, wxPoint ptOffset);

	static int  m_nGrid;

public:
	void SelectItem(CBaseDrawingObject* obj);
	bool UnSelect(IICRect& rect);
	void SelectInRegion(IICRect rect);
	void UnSelect();
	void DeleteAllItems();
	void GetSelectedRegion(IICRect& rect, bool bUnselect=FALSE);

	void OnEditCopy();
	void OnEditPaste(wxPoint& point);
	void OnEditDelete(bool aNotifyServer=true);
	void OnEditUndo(IICRect& rect);
	void OnEditRedo(IICRect& rect);
	void OnEditChange();
	bool CanUndo() { return m_listUndo.size()!=0; }
	bool CanRedo() { return m_listRedo.size()!=0; }
	void AddAction(CAction* action, bool aNotifyServer=true, bool fCompact=true);	// fCompact==TRUE, not to send image data
	void ClearClipboard();
	void ClearUndoActions();
	void ClearRedoActions();

	int Add(CBaseDrawingObject* obj, bool fCanUndoRedo=true);	// append to the end of the list
	void Delete(CBaseDrawingObject* obj);
	CBaseDrawingObject* Delete(const wxString& aID);
	CBaseDrawingObject* Find(const wxString& aID);
	CBaseDrawingObject* HitTest(const wxPoint point, int& flags);
	void SetOwner(DocShareView* pView, CIICDoc* pDocument)
	{
		m_pOwner = pView;
		m_pDocument = pDocument;
	}
    DocShareView* GetOwner() { if (m_pOwner) return m_pOwner; return NULL;}

	void BringToFront(IICRect& rect);
	void SendBack(IICRect& rect);
	void BringToFront(const wxString& strID);
	void SendBack(const wxString& strID);
	wxSize GetExtent( wxDC& dc );
	void MoveSelection(wxPoint ptOffset);
	void SendActionXML(bool aNotifyServer, const wxString& actionXML);
	void ResetPasteOffset();

	// attributes
public:
	bool IsEmpty() {return m_listObj.size()==0;}
	int GetSize() {return m_listObj.size();}
	int GetSelCount()
	{
		return m_listSelected.size();
	}
	OBJLIST& GetSelectedItems() { return m_listSelected; }
	bool IsClipboardEmpty() { return m_listClipboard.size()==0; }

	bool IsMeetingInProgress() { return m_pDocument->m_Meeting.IsInProgress(); }

protected:
    DocShareView*   m_pOwner;
	CIICDoc*        m_pDocument;

	OBJLIST		m_listObj;
	OBJLIST		m_listSelected;

	OBJLIST		m_listClipboard;
	ACTIONLIST	m_listUndo;
	ACTIONLIST	m_listRedo;
};


class CBaseDrawingObject
{
public:
	CBaseDrawingObject();
	CBaseDrawingObject(IICRect&);
	CBaseDrawingObject(CBaseDrawingObject&, bool aPreAction, bool fReuseID);
	virtual ~CBaseDrawingObject();
	int AddRef();
	int Release();

// attributes
protected:
	CDrawingObjects*	m_pOwner;

	wxString	m_strID;	// Unique ID
	POINTLIST	m_points;	// keeps all the end points
	POINTLIST	m_pointsCommitted;	// keeps all the end points
	wxString	m_strType;	// the element type
	wxString	m_strText;	// Text box
	IICRect		m_rcObj;
	int			m_nStyle;
	wxString	m_strTextCommitted;	// Text box
	IICRect		m_rcObjCommitted;
	int			m_nStyleCommitted;

	wxString	m_strAuthor;

    wxFontData	m_logFont;
    wxFontData  m_logFontCommitted;
	// Font attributes
	wxString	m_strFontFace;
	int			m_nFontSize;
	wxString	m_strFontStyle;
	int			m_nAlignment;	// DT_LEFT, DT_CENTER, DT_RIGHT, DT_TOP, DT_VCENTER, DT_BOTTOM
	wxString	m_strFontFaceCommitted;
	int			m_nFontSizeCommitted;
	wxString	m_strFontStyleCommitted;
	int			m_nAlignmentCommitted;	// DT_LEFT, DT_CENTER, DT_RIGHT, DT_TOP, DT_VCENTER, DT_BOTTOM

	// Color attributes
	wxColour	m_crColor;		// border color
	wxColour	m_crBkgnd;		// Background color
	wxColour	m_crTextColor;	// Text color
	wxColour	m_crColorCommitted;		// border color
	wxColour	m_crBkgndCommitted;		// Background color
	wxColour	m_crTextColorCommitted;	// Text color

	// Operational attributes
	IICRect		m_rcOrigRect;	//used for dragging
	IICRect		m_rcOrigRectCommitted;
	IICRect		m_rcLastRect;	//used for dragging
	bool		m_bDragging;
	int			m_nMode;

	wxPoint		m_ptPasteOffset;	// used for pasting

	// Line attribute
	UINT		m_nLineSize;
	UINT		m_nLineStyle;
	UINT		m_nLineSizeCommitted;
	UINT		m_nLineStyleCommitted;

	wxPoint* 	m_pts;
    POINTLIST	m_pathPoints;
    BYTELIST	m_pathTypes;
	wxPoint		m_ptOffset;

public:
	virtual CBaseDrawingObject* Clone(bool aPreAction=false, bool fReuseID=true) = 0;
	virtual int Paint(wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset){return 0;}
	virtual void GetXML(wxString& xml, bool fCompact=false);		// only include basic attributes -- ex. no image data
	virtual void SetAttributes(iks *y);
	virtual wxString GetType(){return m_strType;}
	virtual void GetText(wxString& txt){txt = m_strText;}
	virtual void SetText(wxString& txt){m_strText = txt;}
	virtual wxSize GetExtent(wxDC* pDC);

	virtual CBaseDrawingObject* HitTest(const wxPoint pt, int& flags);
	virtual int OnSide(const wxPoint &point, IICRect rect);      //returns side if point is over a side
								//or 0 if not on a side

	void Drag(wxDC* pDC, wxPoint& point, wxPoint offset, double dblZoom );
	void Resize(wxDC* pDC, wxPoint& p, int Side , wxPoint offset, double dblZoom );
	virtual void Drop(wxDC* pDC, wxPoint& point, wxPoint offset);
	virtual void EndResize(wxDC* pDC, wxPoint& p, int Side, wxPoint offset);
	virtual void Move(wxPoint ptOffset);

	int operator < ( const CBaseDrawingObject& o ) const;
	void Assign(const CBaseDrawingObject& o );

	static void DrawBox ( wxDC* pDC, IICRect& rect, double dblZoom = 1.0, wxPoint ptOffset=wxPoint(0,0) );
	static void DrawBox ( wxDC* pDC, IICRect& rect, wxBrush* br, wxPoint ptOffset=wxPoint(0,0), double dblZoom = 1.0);
//    static void DrawBox2( wxDC* pDC, IICRect rect );
    static void DrawTracker2(wxDC* pDC, IICRect rect, CBaseDrawingObject* pBox);
    static void DrawLine(wxDC* pDC, IICRect& rect, double dblZoom = 1.0, wxPoint ptOffset=wxPoint(0,0));

    void DrawTracker(wxDC* pDC, int state, IICRect& toDraw, wxPoint ptOffset=wxPoint(0,0));
	IICRect GetBorder(int nHandle, IICRect rect);
	static wxPoint GetHandle(int nHandle, IICRect rect);

	virtual void SetOwner(CDrawingObjects* pOwner)
	{
		m_pOwner = pOwner;
	}
	virtual void SetMode(int mode)
	{
		m_nMode = mode;
	}

	wxString& GetID() { return m_strID; }
	void SetID(wxString& aID) { m_strID = aID; }
	wxString& GetAuther() { return m_strAuthor; };
	void SetAuthor(wxString& author) { m_strAuthor = author; }
	IICRect GetRect() { return IICRect(m_rcObj.left, m_rcObj.top, m_rcObj.right, m_rcObj.bottom); }
	void SetRect(IICRect rect);
	IICRect& GetOrigRect() { return m_rcOrigRect; }
	IICRect& GetLastRect() { return m_rcLastRect; }
	virtual bool IsMovable() { return true; }
	virtual bool IsCurveLine() { return false; }
	virtual bool IsVResizable() { return true; }
	virtual bool IsHResizable() { return true; }
    wxFontData& GetLogFont() { return m_logFont; }
	wxColour GetForegroundColor() { return m_crColor; }
	wxColour GetBackgroundColor() { return m_crBkgnd; }
	wxColour GetTextColor() { return m_crTextColor; }
	const wxString& GetFontFace() { return m_strFontFace; }
	const wxString& GetFontStyle() { return m_strFontStyle;}
	int GetFontSize() { return m_nFontSize; }
	virtual void SetFontFace(const wxString& strFontFace, bool bSetDefault) { m_strFontFace = strFontFace; }
	virtual void SetFontStyle(const wxString& strFontStyle, bool bSetDefault) { m_strFontStyle = strFontStyle; }
	virtual void SetFontSize(int nFontSize, bool bSetDefault) { m_nFontSize = nFontSize; }
    virtual void SetLogFont( wxFontData &_font, bool bSetDefault) { m_logFont.SetChosenFont( _font.GetChosenFont( ) ); }
	virtual void SetForegroundColor(wxColour color, bool bSetDefault) { m_crColor = color; }
	virtual void SetBackgroundColor(wxColour color, bool bSetDefault) { m_crBkgnd = color; }
	virtual void SetTextColor(wxColour color, bool bSetDefault) { m_crTextColor = color; }
	virtual void SetLineSize(UINT aSize, bool bSetDefault) { m_nLineSize = aSize; }
	virtual void SetLineStyle(UINT aStyle, bool bSetDefault) { m_nLineStyle = aStyle; }
	virtual void ToggleTransparency(bool bTransparent, bool bSetDefault);
	bool IsSelected() { return m_nMode==BOX_MODE_SELECTED; }
	bool IsDeleting() { return m_nMode==BOX_MODE_DELETING; }
	virtual void SetLineSize(UINT aSize) { m_nLineSize = aSize; }
	virtual void SetLineStyle(UINT aStyle) { m_nLineStyle = aStyle; }
	int GetLineSize() { return m_nLineSize; }
	int GetLineStyle() { return m_nLineStyle; }
	virtual bool AcceptText() { return true; }
	virtual bool AcceptBkgndColor() { return true; }
	wxPoint GetPasteOffset();
	void ResetPasteOffset() { m_ptPasteOffset = wxPoint(6,6); }
	bool IsTransparent() { return (m_nStyle & ST_TRANSPARENT)==ST_TRANSPARENT;}
	IICRect UnionRect(IICRect rect);
	int GetStyle() { return m_nStyle; }
	void SetStyle(int nStyle) { m_nStyle = nStyle; }

    void    BeginPath( );
    void    EndPath( );
    void    StrokePath( wxDC *pDC );
	void CleanupPath();
protected:
	void BeginPaint(wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset);
	void EndPaint(wxDC* pDC);

	void DrawPathOutline(bool fRound, wxDC* pDC, wxPoint ptOffset, bool fStorePath, int aLineSize);
	void DrawLineEx(wxDC* pDC, const IICRect& rect, wxPoint ptOffset);
	void DrawRectangle(wxDC* pDC, const IICRect& rect, wxPoint ptOffset, bool fill=TRUE);

    int  CalcLineWrapping( wxDC *pDC, wxString &strText, IICRect &calc, wxSize &textSize, wxArrayInt &arrLineStarts, wxArrayInt &arrLineEnds );

	wxPoint	m_ptWindowOrg;
	int		m_nRefCnt;
};

class CLine : public CBaseDrawingObject
{
public:
	CLine(): CBaseDrawingObject() {m_strType = DOType_Line;}
	CLine(IICRect& rect);
	CLine(CLine&, bool aPreAction, bool fReuseID);
	virtual ~CLine();

	virtual CBaseDrawingObject* Clone(bool aPreAction=false, bool fReuseID=true);
	virtual int Paint(wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset);
	virtual void GetXML(wxString& xml, bool fCompact=false);
	virtual CBaseDrawingObject* HitTest(const wxPoint pt, int& flags);
	virtual wxSize GetExtent(wxDC* pDC);
	virtual bool IsVResizable() { return false; }
	virtual bool AcceptText() { return false; }
	virtual bool AcceptBkgndColor() { return false; }

	virtual void SetForegroundColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crColorDefault =color; m_crColor = color; }
	virtual void SetBackgroundColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crBkgndDefault = color; m_crBkgnd = color; }
	virtual void SetTextColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crTextColorDefault = color; m_crTextColor = color; }
	virtual void SetLineSize(UINT aSize, bool bSetDefault) { if (bSetDefault) m_nLineSizeDefault = aSize; m_nLineSize = aSize; }
	virtual void SetLineStyle(UINT aStyle, bool bSetDefault) { if (bSetDefault) m_nLineStyleDefault = aStyle; m_nLineStyle = aStyle; }
	virtual void ToggleTransparency(bool bTransparent, bool bSetDefault) {CBaseDrawingObject::ToggleTransparency(bTransparent, bSetDefault); if (bSetDefault) m_nDefaultStyle=m_nStyle;}
    virtual void SetLogFont( wxFontData &_font, bool bSetDefault)
    {
        m_logFont = _font;
        if (bSetDefault) m_fontDefault = _font;
	}
	virtual void SetFontFace(const wxString& strFontFace, bool bSetDefault) { if (bSetDefault) m_strFontFaceDefault = strFontFace; m_strFontFace = strFontFace; }
	virtual void SetFontStyle(const wxString& strFontStyle, bool bSetDefault) { if (bSetDefault) m_strFontStyleDefault = strFontStyle; m_strFontStyle = strFontStyle; }
	virtual void SetFontSize(int nFontSize, bool bSetDefault) { if (bSetDefault) m_nFontSizeDefault = nFontSize; m_nFontSize = nFontSize; }

	void DrawDragPoints(wxDC* pDC, IICRect rect);

	static wxColour	m_crColorDefault;
	static wxColour	m_crBkgndDefault;
	static wxColour	m_crTextColorDefault;
	static UINT		m_nLineSizeDefault;
	static UINT		m_nLineStyleDefault;
	static int		m_nDefaultStyle;
    static wxFontData	m_fontDefault;
	static wxString	m_strFontFaceDefault;
	static int		m_nFontSizeDefault;
	static wxString	m_strFontStyleDefault;

};

class CCurveLine : public CLine
{
public:
	CCurveLine(): CLine() {m_strType = DOType_CurveLine;}
	CCurveLine(IICRect& rect);
	CCurveLine(CCurveLine&, bool aPreAction, bool fReuseID);
	virtual ~CCurveLine();

	void AddPoint(wxPoint point, bool endpoint);

	virtual CBaseDrawingObject* Clone(bool aPreAction=false, bool fReuseID=true);
	virtual int Paint(wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset);
	virtual CBaseDrawingObject* HitTest(const wxPoint pt, int& flags);
	virtual bool IsCurveLine() { return true; }
	virtual bool IsVResizable() { return m_rcOrigRect.Height()>0; }
	virtual bool IsHResizable() { return m_rcOrigRect.Width()>0; }
	virtual bool AcceptText() { return false; }
	virtual bool AcceptBkgndColor() { return false; }

	virtual void SetForegroundColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crColorDefault =color; m_crColor = color; }
	virtual void SetBackgroundColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crBkgndDefault = color; m_crBkgnd = color; }
	virtual void SetTextColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crTextColorDefault = color; m_crTextColor = color; }
	virtual void SetLineSize(UINT aSize, bool bSetDefault) { if (bSetDefault) m_nLineSizeDefault = aSize; m_nLineSize = aSize; }
	virtual void SetLineStyle(UINT aStyle, bool bSetDefault) { if (bSetDefault) m_nLineStyleDefault = aStyle; m_nLineStyle = aStyle; }
	virtual void ToggleTransparency(bool bTransparent, bool bSetDefault) {CBaseDrawingObject::ToggleTransparency(bTransparent, bSetDefault); if (bSetDefault) m_nDefaultStyle=m_nStyle;}
    virtual void SetLogFont( wxFontData &_font, bool bSetDefault)
    {
        m_logFont = _font;
        if (bSetDefault)
            m_fontDefault = _font;
	}
	virtual void SetFontFace(const wxString& strFontFace, bool bSetDefault) { if (bSetDefault) m_strFontFaceDefault = strFontFace; m_strFontFace = strFontFace; }
	virtual void SetFontStyle(const wxString& strFontStyle, bool bSetDefault) { if (bSetDefault) m_strFontStyleDefault = strFontStyle; m_strFontStyle = strFontStyle; }
	virtual void SetFontSize(int nFontSize, bool bSetDefault) { if (bSetDefault) m_nFontSizeDefault = nFontSize; m_nFontSize = nFontSize; }

	static wxColour	m_crColorDefault;
	static wxColour	m_crBkgndDefault;
	static wxColour	m_crTextColorDefault;
	static UINT		m_nLineSizeDefault;
	static UINT		m_nLineStyleDefault;
	static int		m_nDefaultStyle;
    static wxFontData	m_fontDefault;
	static wxString	m_strFontFaceDefault;
	static int		m_nFontSizeDefault;
	static wxString	m_strFontStyleDefault;

};

class CHighLite : public CCurveLine
{
public:
	CHighLite(): CCurveLine() {m_strType = DOType_HighLite;}
	CHighLite(IICRect& rect);
	CHighLite(CHighLite&, bool aPreAction, bool fReuseID);
	virtual ~CHighLite();

	virtual CBaseDrawingObject* Clone(bool aPreAction=false, bool fReuseID=true);

	virtual bool IsMovable() { return FALSE; }
	virtual bool IsVResizable() { return FALSE; }
	virtual bool IsHResizable() { return FALSE; }

	virtual void SetForegroundColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crColorDefault =color; m_crColor = color; }
	virtual void SetBackgroundColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crBkgndDefault = color; m_crBkgnd = color; }
	virtual void SetTextColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crTextColorDefault = color; m_crTextColor = color; }
	virtual void SetLineSize(UINT aSize, bool bSetDefault) { if (bSetDefault) m_nLineSizeDefault = aSize; m_nLineSize = aSize; }
	virtual void SetLineStyle(UINT aStyle, bool bSetDefault) { if (bSetDefault) m_nLineStyleDefault = aStyle; m_nLineStyle = aStyle; }
	virtual void ToggleTransparency(bool bTransparent, bool bSetDefault) {CBaseDrawingObject::ToggleTransparency(bTransparent, bSetDefault); if (bSetDefault) m_nDefaultStyle=m_nStyle;}
    virtual void SetLogFont( wxFontData &_font, bool bSetDefault)
    {
        m_logFont = _font;
        if (bSetDefault)
            m_fontDefault = _font;
	}
	virtual void SetFontFace(const wxString& strFontFace, bool bSetDefault) { if (bSetDefault) m_strFontFaceDefault = strFontFace; m_strFontFace = strFontFace; }
	virtual void SetFontStyle(const wxString& strFontStyle, bool bSetDefault) { if (bSetDefault) m_strFontStyleDefault = strFontStyle; m_strFontStyle = strFontStyle; }
	virtual void SetFontSize(int nFontSize, bool bSetDefault) { if (bSetDefault) m_nFontSizeDefault = nFontSize; m_nFontSize = nFontSize; }

	static wxColour	m_crColorDefault;
	static wxColour	m_crBkgndDefault;
	static wxColour	m_crTextColorDefault;
	static UINT		m_nLineSizeDefault;
	static UINT		m_nLineStyleDefault;
	static int		m_nDefaultStyle;
    static wxFontData	m_fontDefault;
	static wxString	m_strFontFaceDefault;
	static int		m_nFontSizeDefault;
	static wxString	m_strFontStyleDefault;

};

class CArrowLine : public CLine
{
    wxPoint m_ptValues[3];

	void ArrowCoords(int x1, int y1, int x2, int y2, int x3, int y3);
	void CalcCoords(int index, int x, int y, double dist, double dirn);
	void CalcValuesQuad(int x1, int y1, int x2, int y2);
	void CalcValues(int x1, int y1, int x2, int y2);

public:
	CArrowLine(): CLine() {m_strType = DOType_ArrowLine;}
	CArrowLine(IICRect& rect);
	CArrowLine(CArrowLine&, bool aPreAction, bool fReuseID);
	virtual ~CArrowLine();

	virtual CBaseDrawingObject* Clone(bool aPreAction=false, bool fReuseID=true);
	virtual int Paint(wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset);
	virtual bool AcceptText() { return false; }
	virtual bool AcceptBkgndColor() { return false; }

	virtual void SetForegroundColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crColorDefault =color; m_crColor = color; }
	virtual void SetBackgroundColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crBkgndDefault = color; m_crBkgnd = color; }
	virtual void SetTextColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crTextColorDefault = color; m_crTextColor = color; }
	virtual void SetLineSize(UINT aSize, bool bSetDefault) { if (bSetDefault) m_nLineSizeDefault = aSize; m_nLineSize = aSize; }
	virtual void SetLineStyle(UINT aStyle, bool bSetDefault) { if (bSetDefault) m_nLineStyleDefault = aStyle; m_nLineStyle = aStyle; }
	virtual void ToggleTransparency(bool bTransparent, bool bSetDefault) {CBaseDrawingObject::ToggleTransparency(bTransparent, bSetDefault); if (bSetDefault) m_nDefaultStyle=m_nStyle;}
    virtual void SetLogFont( wxFontData &_font, bool bSetDefault)
    {
        m_logFont = _font;
        if (bSetDefault)
            m_fontDefault = _font;
	}
	virtual void SetFontFace(const wxString& strFontFace, bool bSetDefault) { if (bSetDefault) m_strFontFaceDefault = strFontFace; m_strFontFace = strFontFace; }
	virtual void SetFontStyle(const wxString& strFontStyle, bool bSetDefault) { if (bSetDefault) m_strFontStyleDefault = strFontStyle; m_strFontStyle = strFontStyle; }
	virtual void SetFontSize(int nFontSize, bool bSetDefault) { if (bSetDefault) m_nFontSizeDefault = nFontSize; m_nFontSize = nFontSize; }

	static wxColour	m_crColorDefault;
	static wxColour	m_crBkgndDefault;
	static wxColour	m_crTextColorDefault;
	static UINT		m_nLineSizeDefault;
	static UINT		m_nLineStyleDefault;
	static int		m_nDefaultStyle;
    static wxFontData	m_fontDefault;
	static wxString	m_strFontFaceDefault;
	static int		m_nFontSizeDefault;
	static wxString	m_strFontStyleDefault;

};

class CRectangle : public CBaseDrawingObject
{
public:
	CRectangle(): CBaseDrawingObject() {m_strType = DOType_Rectangle;}
	CRectangle(IICRect& rect);
	CRectangle(CRectangle&, bool aPreAction, bool fReuseID);
	virtual ~CRectangle();

	virtual CBaseDrawingObject* Clone(bool aPreAction=false, bool fReuseID=true);
	virtual int Paint(wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset);
	virtual void GetXML(wxString& xml, bool fCompact=false);
	virtual wxSize GetExtent(wxDC* pDC);

	virtual void SetForegroundColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crColorDefault =color; m_crColor = color; }
	virtual void SetBackgroundColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crBkgndDefault = color; m_crBkgnd = color; }
	virtual void SetTextColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crTextColorDefault = color; m_crTextColor = color; }
	virtual void SetLineSize(UINT aSize, bool bSetDefault) { if (bSetDefault) m_nLineSizeDefault = aSize; m_nLineSize = aSize; }
	virtual void SetLineStyle(UINT aStyle, bool bSetDefault) { if (bSetDefault) m_nLineStyleDefault = aStyle; m_nLineStyle = aStyle; }
	virtual void ToggleTransparency(bool bTransparent, bool bSetDefault) {CBaseDrawingObject::ToggleTransparency(bTransparent, bSetDefault); if (bSetDefault) m_nDefaultStyle=m_nStyle;}
    virtual void SetLogFont( wxFontData &_font, bool bSetDefault)
    {
        m_logFont.SetChosenFont( _font.GetChosenFont( ) );
        if (bSetDefault)
            m_fontDefault.SetChosenFont( _font.GetChosenFont( ) );
	}
	virtual void SetFontFace(const wxString& strFontFace, bool bSetDefault) { if (bSetDefault) m_strFontFaceDefault = strFontFace; m_strFontFace = strFontFace; }
	virtual void SetFontStyle(const wxString& strFontStyle, bool bSetDefault) { if (bSetDefault) m_strFontStyleDefault = strFontStyle; m_strFontStyle = strFontStyle; }
	virtual void SetFontSize(int nFontSize, bool bSetDefault) { if (bSetDefault) m_nFontSizeDefault = nFontSize; m_nFontSize = nFontSize; }

	static wxColour	m_crColorDefault;
	static wxColour	m_crBkgndDefault;
	static wxColour	m_crTextColorDefault;
	static UINT		m_nLineSizeDefault;
	static UINT		m_nLineStyleDefault;
	static int		m_nDefaultStyle;
    static wxFontData	m_fontDefault;
	static wxString	m_strFontFaceDefault;
	static int		m_nFontSizeDefault;
	static wxString	m_strFontStyleDefault;
};

class CEllipse : public CBaseDrawingObject
{
	wxPoint     m_EllipsePts[13];
	IICRect     m_EllipseRect;
	wxRegion    *m_pEllipseRgn;
	wxRegion    *m_pEllipseFillRgn;

public:
	CEllipse(): CBaseDrawingObject() {m_strType = DOType_Ellipse; m_pEllipseRgn = NULL; m_pEllipseFillRgn = NULL; }
	CEllipse(IICRect& rect);
	CEllipse(CEllipse&, bool aPreAction, bool fReuseID);
	virtual ~CEllipse();

	void RecreateEllipse(IICRect rect);
	void UpdateEllipseRgn( wxDC* pDC, CDashLine& Line );

	virtual CBaseDrawingObject* Clone(bool aPreAction=false, bool fReuseID=true);
	virtual int Paint(wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset);
	virtual void GetXML(wxString& xml, bool fCompact=false);
	virtual wxSize GetExtent(wxDC* pDC);

	virtual void SetForegroundColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crColorDefault =color; m_crColor = color; }
	virtual void SetBackgroundColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crBkgndDefault = color; m_crBkgnd = color; }
	virtual void SetTextColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crTextColorDefault = color; m_crTextColor = color; }
	virtual void SetLineSize(UINT aSize, bool bSetDefault) { if (bSetDefault) m_nLineSizeDefault = aSize; m_nLineSize = aSize; }
	virtual void SetLineStyle(UINT aStyle, bool bSetDefault) { if (bSetDefault) m_nLineStyleDefault = aStyle; m_nLineStyle = aStyle; }
	virtual void ToggleTransparency(bool bTransparent, bool bSetDefault) {CBaseDrawingObject::ToggleTransparency(bTransparent, bSetDefault); if (bSetDefault) m_nDefaultStyle=m_nStyle;}
    virtual void SetLogFont( wxFontData &_font, bool bSetDefault)
    {
        m_logFont.SetChosenFont( _font.GetChosenFont( ) );
        if (bSetDefault)
            m_fontDefault.SetChosenFont( _font.GetChosenFont( ) );
	}
	virtual void SetFontFace(const wxString& strFontFace, bool bSetDefault) { if (bSetDefault) m_strFontFaceDefault = strFontFace; m_strFontFace = strFontFace; }
	virtual void SetFontStyle(const wxString& strFontStyle, bool bSetDefault) { if (bSetDefault) m_strFontStyleDefault = strFontStyle; m_strFontStyle = strFontStyle; }
	virtual void SetFontSize(int nFontSize, bool bSetDefault) { if (bSetDefault) m_nFontSizeDefault = nFontSize; m_nFontSize = nFontSize; }

	static wxColour	m_crColorDefault;
	static wxColour	m_crBkgndDefault;
	static wxColour	m_crTextColorDefault;
	static UINT		m_nLineSizeDefault;
	static UINT		m_nLineStyleDefault;
	static int		m_nDefaultStyle;
    static wxFontData	m_fontDefault;
	static wxString	m_strFontFaceDefault;
	static int		m_nFontSizeDefault;
	static wxString	m_strFontStyleDefault;
};


class CTextBox : public CBaseDrawingObject
{

public:
	CTextBox(): CBaseDrawingObject() {m_strType = DOType_TextBox;}
	CTextBox(IICRect rect);
	CTextBox(CTextBox&, bool aPreAction, bool fReuseID);
	virtual ~CTextBox();

	virtual CBaseDrawingObject* Clone(bool aPreAction=false, bool fReuseID=true);
	virtual int Paint(wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset);
	virtual void GetXML(wxString& xml, bool fCompact=false);
	virtual wxSize GetExtent(wxDC* pDC);

	virtual void SetForegroundColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crColorDefault =color; m_crColor = color; }
	virtual void SetBackgroundColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crBkgndDefault = color; m_crBkgnd = color; }
	virtual void SetTextColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crTextColorDefault = color; m_crTextColor = color; }
	virtual void SetLineSize(UINT aSize, bool bSetDefault) { if (bSetDefault) m_nLineSizeDefault = aSize; m_nLineSize = aSize; }
	virtual void SetLineStyle(UINT aStyle, bool bSetDefault) { if (bSetDefault) m_nLineStyleDefault = aStyle; m_nLineStyle = aStyle; }
	virtual void ToggleTransparency(bool bTransparent, bool bSetDefault) {CBaseDrawingObject::ToggleTransparency(bTransparent, bSetDefault); if (bSetDefault) m_nDefaultStyle=m_nStyle;}
    virtual void SetLogFont( wxFontData &_font, bool bSetDefault)
    {
        m_logFont.SetChosenFont( _font.GetChosenFont( ) );
        if (bSetDefault)
            m_fontDefault.SetChosenFont( _font.GetChosenFont( ) );
	}
	virtual void SetFontFace(const wxString& strFontFace, bool bSetDefault) { if (bSetDefault) m_strFontFaceDefault = strFontFace; m_strFontFace = strFontFace; }
	virtual void SetFontStyle(const wxString& strFontStyle, bool bSetDefault) { if (bSetDefault) m_strFontStyleDefault = strFontStyle; m_strFontStyle = strFontStyle; }
	virtual void SetFontSize(int nFontSize, bool bSetDefault) { if (bSetDefault) m_nFontSizeDefault = nFontSize; m_nFontSize = nFontSize; }

	static wxColour	m_crColorDefault;
	static wxColour	m_crBkgndDefault;
	static wxColour	m_crTextColorDefault;
	static UINT		m_nLineSizeDefault;
	static UINT		m_nLineStyleDefault;
	static int		m_nDefaultStyle;
    static wxFontData	m_fontDefault;
	static wxString	m_strFontFaceDefault;
	static int		m_nFontSizeDefault;
	static wxString	m_strFontStyleDefault;
};


class CImage : public CBaseDrawingObject
{
protected:
    wxImage     m_image;
	wxString    m_strImageData;
    CBase64     m_encoder;	// for converting binary data

	void GetImageData(wxString& strData);

public:
	CImage(): CBaseDrawingObject() {m_strType = DOType_Image;}
	CImage(IICRect rect);
	CImage(CImage&, bool aPreAction, bool fReuseID);
	virtual ~CImage();

	virtual CBaseDrawingObject* Clone(bool aPreAction=false, bool fReuseID=true);
	virtual int Paint(wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset);
	virtual void GetXML(wxString& xml, bool fCompact=false);
	virtual void SetAttributes(iks *y);
	virtual bool AcceptText() { return false; }
	virtual bool AcceptBkgndColor() { return false; }

	virtual void SetForegroundColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crColorDefault =color; m_crColor = color; }
	virtual void SetBackgroundColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crBkgndDefault = color; m_crBkgnd = color; }
	virtual void SetTextColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crTextColorDefault = color; m_crTextColor = color; }
	virtual void SetLineSize(UINT aSize, bool bSetDefault) { if (bSetDefault) m_nLineSizeDefault = aSize; m_nLineSize = aSize; }
	virtual void SetLineStyle(UINT aStyle, bool bSetDefault) { if (bSetDefault) m_nLineStyleDefault = aStyle; m_nLineStyle = aStyle; }
	virtual void ToggleTransparency(bool bTransparent, bool bSetDefault) {CBaseDrawingObject::ToggleTransparency(bTransparent, bSetDefault); if (bSetDefault) m_nDefaultStyle=m_nStyle;}
    virtual void SetLogFont( wxFontData &_font, bool bSetDefault)
    {
        m_logFont = _font;
        if (bSetDefault)
            m_fontDefault = _font;
	}
	virtual void SetFontFace(const wxString& strFontFace, bool bSetDefault) { if (bSetDefault) m_strFontFaceDefault = strFontFace; m_strFontFace = strFontFace; }
	virtual void SetFontStyle(const wxString& strFontStyle, bool bSetDefault) { if (bSetDefault) m_strFontStyleDefault = strFontStyle; m_strFontStyle = strFontStyle; }
	virtual void SetFontSize(int nFontSize, bool bSetDefault) { if (bSetDefault) m_nFontSizeDefault = nFontSize; m_nFontSize = nFontSize; }

	void SetBitmap( wxBitmap bitmap );
	void SetBitmap( const char* pData );
	unsigned GetImageSize( );      //  WAS... { return m_image.GetImageSize(); }

	static wxColour	m_crColorDefault;
	static wxColour	m_crBkgndDefault;
	static wxColour	m_crTextColorDefault;
	static UINT		m_nLineSizeDefault;
	static UINT		m_nLineStyleDefault;
	static int		m_nDefaultStyle;
    static wxFontData	m_fontDefault;
	static wxString	m_strFontFaceDefault;
	static int		m_nFontSizeDefault;
	static wxString	m_strFontStyleDefault;

};

class CCheckMark : public CBaseDrawingObject
{
public:
	CCheckMark(): CBaseDrawingObject() {m_strType = DOType_CheckMark;}
	CCheckMark(IICRect& rect);
	CCheckMark(CCheckMark&, bool aPreAction, bool fReuseID);
	virtual ~CCheckMark();

	virtual CBaseDrawingObject* Clone(bool aPreAction=false, bool fReuseID=true);
	virtual int Paint(wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset);
	virtual void GetXML(wxString& xml, bool fCompact=false);
	virtual bool IsVResizable() { return true; }
	virtual bool IsHResizable() { return true; }
	virtual bool AcceptText() { return false; }
	virtual bool AcceptBkgndColor() { return false; }

	virtual void SetForegroundColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crColorDefault =color; m_crColor = color; }
	virtual void SetBackgroundColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crBkgndDefault = color; m_crBkgnd = color; }
	virtual void SetTextColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crTextColorDefault = color; m_crTextColor = color; }
	virtual void SetLineSize(UINT aSize, bool bSetDefault) { if (bSetDefault) m_nLineSizeDefault = aSize; m_nLineSize = aSize; }
	virtual void SetLineStyle(UINT aStyle, bool bSetDefault) { if (bSetDefault) m_nLineStyleDefault = aStyle; m_nLineStyle = aStyle; }
	virtual void ToggleTransparency(bool bTransparent, bool bSetDefault) {CBaseDrawingObject::ToggleTransparency(bTransparent, bSetDefault); if (bSetDefault) m_nDefaultStyle=m_nStyle;}
    virtual void SetLogFont( wxFontData &_font, bool bSetDefault)
    {
        m_logFont = _font;
        if (bSetDefault)
            m_fontDefault = _font;
	}
	virtual void SetFontFace(const wxString& strFontFace, bool bSetDefault) { if (bSetDefault) m_strFontFaceDefault = strFontFace; m_strFontFace = strFontFace; }
	virtual void SetFontStyle(const wxString& strFontStyle, bool bSetDefault) { if (bSetDefault) m_strFontStyleDefault = strFontStyle; m_strFontStyle = strFontStyle; }
	virtual void SetFontSize(int nFontSize, bool bSetDefault) { if (bSetDefault) m_nFontSizeDefault = nFontSize; m_nFontSize = nFontSize; }

	static wxColour	m_crColorDefault;
	static wxColour	m_crBkgndDefault;
	static wxColour	m_crTextColorDefault;
	static UINT		m_nLineSizeDefault;
	static UINT		m_nLineStyleDefault;
	static int		m_nDefaultStyle;
    static wxFontData	m_fontDefault;
	static wxString	m_strFontFaceDefault;
	static int		m_nFontSizeDefault;
	static wxString	m_strFontStyleDefault;

};

class CRadioMark : public CBaseDrawingObject
{
	static CRadioMark* m_singleton;

private:
	CRadioMark(IICRect& rect);
	CRadioMark(CRadioMark&, bool aPreAction, bool fReuseID);

public:
	static CRadioMark* GetRadioMark();
	static void Clear();

	virtual ~CRadioMark();

	virtual CBaseDrawingObject* Clone(bool aPreAction=false, bool fReuseID=true);
	virtual int Paint(wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset);
	virtual void GetXML(wxString& xml, bool fCompact=false);
	virtual bool IsVResizable() { return true; }
	virtual bool IsHResizable() { return true; }
	virtual bool AcceptText() { return false; }
	virtual bool AcceptBkgndColor() { return false; }

	virtual void SetForegroundColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crColorDefault =color; m_crColor = color; }
	virtual void SetBackgroundColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crBkgndDefault = color; m_crBkgnd = color; }
	virtual void SetTextColor(wxColour color, bool bSetDefault) { if (bSetDefault) m_crTextColorDefault = color; m_crTextColor = color; }
	virtual void SetLineSize(UINT aSize, bool bSetDefault) { if (bSetDefault) m_nLineSizeDefault = aSize; m_nLineSize = aSize; }
	virtual void SetLineStyle(UINT aStyle, bool bSetDefault) { if (bSetDefault) m_nLineStyleDefault = aStyle; m_nLineStyle = aStyle; }
	virtual void ToggleTransparency(bool bTransparent, bool bSetDefault) {CBaseDrawingObject::ToggleTransparency(bTransparent, bSetDefault); if (bSetDefault) m_nDefaultStyle=m_nStyle;}
    virtual void SetLogFont( wxFontData &_font, bool bSetDefault)
    {
        m_logFont = _font;
        if (bSetDefault)
            m_fontDefault = _font;
	}
	virtual void SetFontFace(const wxString& strFontFace, bool bSetDefault) { if (bSetDefault) m_strFontFaceDefault = strFontFace; m_strFontFace = strFontFace; }
	virtual void SetFontStyle(const wxString& strFontStyle, bool bSetDefault) { if (bSetDefault) m_strFontStyleDefault = strFontStyle; m_strFontStyle = strFontStyle; }
	virtual void SetFontSize(int nFontSize, bool bSetDefault) { if (bSetDefault) m_nFontSizeDefault = nFontSize; m_nFontSize = nFontSize; }

	static wxColour	m_crColorDefault;
	static wxColour	m_crBkgndDefault;
	static wxColour	m_crTextColorDefault;
	static UINT		m_nLineSizeDefault;
	static UINT		m_nLineStyleDefault;
	static int		m_nDefaultStyle;
    static wxFontData	m_fontDefault;
	static wxString	m_strFontFaceDefault;
	static int		m_nFontSizeDefault;
	static wxString	m_strFontStyleDefault;

};

///////////////////////////////////////////////////////////////////////////////
class CAction
{
protected:
	CIICDoc* m_pDocument;

	void SendXML(const wxString& actionXML);

public:
	CAction(CIICDoc* pDocument);
	virtual ~CAction();

	enum {AT_NONE, AT_CREATE, AT_PASTE, AT_DELETE, AT_CHANGE, AT_BRINGTOFRONT, AT_SENDBACK, AT_ASSIGN};

	virtual void Do(CDrawingObjects& src, bool aPreAction=false);
	virtual void Undo(CDrawingObjects& src, IICRect& rect)	= 0;
	virtual void Redo(CDrawingObjects& src, IICRect& rect) = 0;
	virtual void SendActionXML(bool bUndo=false, bool fCompact=false, bool aNotifyServer=true, bool aUndoRedoAction=true) = 0;

	bool IsMeetingInProgress() { return m_pDocument->m_Meeting.IsInProgress(); }
protected:
	int m_nType;
	OBJLIST m_listObj;

};

class CCreateAction : public CAction
{
public:
	CCreateAction(CIICDoc* pDocument);

	virtual void Undo(CDrawingObjects& src, IICRect& rect);
	virtual void Redo(CDrawingObjects& src, IICRect& rect);
	virtual void SendActionXML(bool bUndo=false, bool fCompact=false, bool aNotifyServer=true, bool aUndoRedoAction=true);
};

class CPasteAction : public CAction
{
public:
	CPasteAction(CIICDoc* pDocument);
	~CPasteAction();

	virtual void Undo(CDrawingObjects& src, IICRect& rect);
	virtual void Redo(CDrawingObjects& src, IICRect& rect);
	virtual void SendActionXML(bool bUndo=false, bool fCompact=false, bool aNotifyServer=true, bool aUndoRedoAction=true);
};

class CDeleteAction : public CAction
{
public:
	CDeleteAction(CIICDoc* pDocument);

	virtual void Undo(CDrawingObjects& src, IICRect& rect);
	virtual void Redo(CDrawingObjects& src, IICRect& rect);
	virtual void SendActionXML(bool bUndo=false, bool fCompact=true, bool aNotifyServer=true, bool aUndoRedoAction=true);
};

class CAssignAction : public CAction
{
public:
	CAssignAction(CIICDoc* pDocument);

	virtual void Undo(CDrawingObjects& src, IICRect& rect);
	virtual void Redo(CDrawingObjects& src, IICRect& rect);
	virtual void SendActionXML(bool bUndo=false, bool fCompact=true, bool aNotifyServer=true, bool aUndoRedoAction=true);
};

class CChangeAction : public CAction
{
	CAction* m_pPreAction;
	CAction* m_pPostAction;

public:
	CChangeAction(CIICDoc* pDocument);
	virtual ~CChangeAction();

	virtual void Undo(CDrawingObjects& src, IICRect& rect);
	virtual void Redo(CDrawingObjects& src, IICRect& rect);
	virtual void SendActionXML(bool bUndo=false, bool fCompact=true, bool aNotifyServer=true, bool aUndoRedoAction=true);
	void PreAction(CDrawingObjects& src);
	void PostAction(CDrawingObjects& src);

protected:
};

class CReOrderAction : public CAction
{
public:
	CReOrderAction(CIICDoc* pDocument, int nType);

	virtual void Undo(CDrawingObjects& src, IICRect& rect);
	virtual void Redo(CDrawingObjects& src, IICRect& rect);
	virtual void SendActionXML(bool bUndo=false, bool fCompact=true, bool aNotifyServer=true, bool aUndoRedoAction=true);
};

extern void snap_to_grid(wxPoint& pt);

#endif // !defined(DRAWINGOBJECTS_H)
