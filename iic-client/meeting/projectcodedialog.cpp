#include "app.h"
#include "projectcodedialog.h"
#include "meetingcentercontroller.h"
#include "controller.h"

//(*InternalHeaders(ProjectCodeDialog)
#include <wx/xrc/xmlres.h>
//*)
#include <wx/button.h>
#include <wx/validate.h>
#include <wx/valtext.h>

//(*IdInit(ProjectCodeDialog)
//*)

BEGIN_EVENT_TABLE(ProjectCodeDialog,wxDialog)
	//(*EventTable(ProjectCodeDialog)
	//*)
END_EVENT_TABLE()

ProjectCodeDialog::ProjectCodeDialog(wxWindow* parent)
{
	//(*Initialize(ProjectCodeDialog)
	wxXmlResource::Get()->LoadObject(this,parent,_T("ProjectCodeDialog"),_T("wxDialog"));
	m_ProjectCodeEntry = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL_PROJECTCODE"));
	
	Connect(XRCID("ID_TEXTCTRL_PROJECTCODE"),wxEVT_COMMAND_TEXT_UPDATED,(wxObjectEventFunction)&ProjectCodeDialog::OnProjectCodeEntryText);
	//*)
	
    EnableButtons();

#ifdef __WXMAC__
	Centre();
#endif
	
}

ProjectCodeDialog::~ProjectCodeDialog()
{
	//(*Destroy(ProjectCodeDialog)
	//*)
}

int ProjectCodeDialog::GetProjectCode(wxString & code)
{
    int res = wxID_OK;
    code.Empty();
    
    if (CONTROLLER.RequestMeetingProjectCode())
    {
        int res = ShowModal();
        if (res == wxID_OK)
        {
            code = m_ProjectCodeEntry->GetValue();
        }
    }
    
    return res;
}

void ProjectCodeDialog::OnProjectCodeEntryText(wxCommandEvent& event)
{
    EnableButtons();
}

void ProjectCodeDialog::EnableButtons()
{
    wxWindow *pWindow = wxWindow::FindWindowById( wxID_OK, this );
    if( pWindow )
    {
        wxButton *pButton = wxDynamicCast( pWindow, wxButton );
        if( pButton )
        {
            pButton->Enable(!m_ProjectCodeEntry->IsEmpty());
        }
    }

    pWindow = wxWindow::FindWindowById( wxID_CANCEL, this );
    if( pWindow )
    {
        wxButton *pButton = wxDynamicCast( pWindow, wxButton );
        if( pButton )
        {
            pButton->Enable(m_ProjectCodeEntry->IsEmpty());
        }
    }
}
