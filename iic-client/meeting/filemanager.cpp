#include "app.h"
#include "filemanager.h"
#include "meetingcentercontroller.h"

#include <wx/stdpaths.h>
#include <wx/wfstream.h>
#include <wx/uri.h>
#include <json.h>

#ifndef FAILED
  #define FAILED(x) 	(x<0)
#endif

/*
    Upload/download profile image per user

    Upload/download images per meeting
    
    User picture can go in /repository/user/<user>/
        File name "profile.jpg"

    Meeting-related pictures can go in /repository/SharedDocuments/<meeting>/Pictures113636/ (copy profile over)
        File "meta" lists contents of collection

    Cache file locally using same name, remove "picture*.jpg" at meeting end
*/

int FileManager::m_TempCount = 0;
wxString FileManager::m_UploadURL;


FileManager::FileManager(const wxString& repoPath, int options)
{
    // May want to use doc share server url instead...
    m_TempDir = wxStandardPaths::Get().GetTempDir() + wxT("/");

    if (m_UploadURL.IsEmpty())
        m_UploadURL = wxString(wxGetApp().Options().GetSystem("UploadURL", ""), wxConvUTF8);

    m_RepoPath = repoPath;
    m_Options = options;
    m_InProgress = false;
    m_pDialogProgress = NULL;
    m_pParent = NULL;
    m_Status = 0;
    m_Dirty = false;
    
    // Get auth info
    wxString community;
    CONTROLLER.GetSignOnInfo(m_UserName, m_Password, community);
    if (CONTROLLER.AnonymousMode())
        CONTROLLER.GetControllerUsername(m_UserName);
    
    m_RepoPath.Replace(wxT("$user"), m_UserName);
    
    m_WebDavUtils.SetShowErrors(false);
    if ((options & InMeeting) == 0)
        m_WebDavUtils.AddHeader(wxT("X-iic-host: true"));
}

FileManager::~FileManager()
{
}

void FileManager::SetUploadURL(wxString url)
{
    m_UploadURL = url;
}

// Make directory on webdav server.
// URL = http://server, path = /repository/...
long FileManager::makeDir(const wxString& aTargetUrl, const wxString& aTargetPath,
                          wxString& aStatusCode, wxString& aStatusText)
{
    long hr = S_OK;
    int len = 0;
    wxString lockToken;
    
    hr = m_WebDavUtils.SendRequest(wxT("MKCOL"), aTargetUrl, aTargetPath, m_UserName, m_Password, NULL, 0, aStatusCode,
                              aStatusText, false, lockToken, m_pParent, &len);
    if (m_pDialogProgress && m_pDialogProgress->IsCancelled())
    {
        hr = E_ABORT;
    }

    return hr;
}


// Send local file to webdav server.
// URL = http://server, path = /repository/...
long FileManager::sendFile(const wxString& aTargetUrl, const wxString& aTargetPath,
                           const wxString& aIn, bool aSendFromFile, wxString& aStatusCode, wxString& aStatusText)
{
    long hr = S_OK;
    wxString lockToken;
    wxFile hfile;
    unsigned char* lpBuf;
    long iBytesRead;
    
    if (aSendFromFile)
    {
        if (wxFile::Exists(aIn))
            hfile.Open(aIn, wxFile::read);
        if (hfile.IsOpened())
        {
            unsigned long dwFileSize = hfile.Length();
            lpBuf = new unsigned char[ dwFileSize+1 ];
            iBytesRead = hfile.Read(lpBuf, dwFileSize);
            hfile.Close();
        }
        else
        {
            wxLogDebug(wxT("Unable to send file %s"), aIn.wc_str());
            hr = E_FAIL;
            iBytesRead = 0;
        }
    }
    else
    {
        lpBuf = new unsigned char[ aIn.Len() * sizeof(wxChar) ];
        strcpy((char*)lpBuf, aIn.utf8_str());
        iBytesRead = strlen((char*)lpBuf);
    }

    if (iBytesRead > 0)
    {
        int len = 0;
        
        hr = m_WebDavUtils.SendRequest(wxT("PUT"), aTargetUrl, aTargetPath, m_UserName, m_Password, lpBuf, iBytesRead, aStatusCode,
                                  aStatusText, false, lockToken, m_pParent, &len);
        if (m_pDialogProgress && m_pDialogProgress->IsCancelled())
        {
            hr = E_ABORT;
        }
    }

    delete [] lpBuf;

    return hr;
}

long FileManager::getFile(const wxString& aTargetUrl, const wxString& aTargetPath,
                          wxString& aOut, bool aSaveInFile, wxString& aStatusCode, wxString& aStatusText)
{
    long hr = S_OK;

    hr = m_WebDavUtils.RetrieveFile(aTargetUrl, aTargetPath, aOut, m_UserName, m_Password, aStatusCode, aStatusText, aSaveInFile, m_pParent);

    if (m_pDialogProgress && m_pDialogProgress->IsCancelled())
    {
        hr = E_ABORT;
    }
    
    return hr;
}

long FileManager::deleteFile(const wxString& aTargetUrl, const wxString& aTargetPath,
                             wxString& aStatusCode, wxString& aStatusText)
{
    long hr = S_OK;
    int len = 0;

    hr = m_WebDavUtils.SendRequest(wxT("DELETE"), aTargetUrl, aTargetPath, m_UserName, m_Password, NULL, 0, aStatusCode,
                              aStatusText, false, wxT(""), m_pParent, &len);

    if (m_pDialogProgress && m_pDialogProgress->IsCancelled())
    {
        hr = E_ABORT;
    }
    
    return hr;
}

bool FileManager::checkStatus(const wxString& status)
{
    return status.Left(1) == wxT("2");
}

bool FileManager::UploadFile(const wxString& remoteName, const wxString& data, bool isFileName)
{
    wxURI url(m_UploadURL);
    wxString docServer = url.GetScheme() + wxT("://") + url.GetServer();
    wxString path = m_RepoPath;

    wxString statusCode, statusText;

    long hr = makeDir(docServer, path, statusCode, statusText);
    if (!FAILED(hr) && (!m_pDialogProgress || !m_pDialogProgress->IsCancelled()))
    {
        path += remoteName;
        hr = sendFile(docServer, path, data, isFileName, statusCode, statusText);
    }
    
    if (FAILED(hr) || checkStatus(statusCode) == false)
    {
        if (m_Options & ShowErrors)
        {
            wxString errmsg = wxString(_("Unable to upload files.")) + wxT("\r\n");
            long    lCode;
            statusCode.ToLong( &lCode );
            CUtils::ReportDAVError(NULL, lCode, statusText, wxT("%s\n%s\n%s"), errmsg, hr);
        }

        hideProgress();
        m_Status = UploadError;
        return false;
    }
    
    wxLogDebug(wxT("Upload result: %s"), statusCode.wc_str());
    m_Status = OK;
    return true;
}

bool FileManager::DownloadFile(const wxString& remoteName, wxString& data, bool storeToFile)
{
    wxURI url(m_UploadURL);
    wxString docServer = url.GetScheme() + wxT("://") + url.GetServer();
    wxString path = m_RepoPath + remoteName;

    wxString statusCode, statusText;

    long hr = getFile(docServer, path, data, storeToFile, statusCode, statusText);
    
    if ((FAILED(hr) || checkStatus(statusCode) == false))
    {
        if (m_Options & ShowErrors)
        {
            wxString errmsg = wxString(_("Unable to download files.")) + wxT("\r\n");
            long    lCode;
            statusCode.ToLong( &lCode );
            CUtils::ReportDAVError(NULL, lCode, statusText, wxT("%s\n%s\n%s"), errmsg, hr);
        }

        hideProgress();
        m_Status = statusCode == wxT("404") ? FileNotFound : DownloadError;
        return false;
    }
    
    wxLogDebug(wxT("Download result: %s"), statusCode.wc_str());
    m_Status = OK;
    return true;;
}

void FileManager::showProgress()
{
    if (m_pDialogProgress != NULL && !m_pDialogProgress->IsCancelled())
        m_pDialogProgress->Commands().Add(wxT("pulse"));
}

void FileManager::hideProgress()
{
    if (m_pDialogProgress != NULL)
        m_pDialogProgress->Commands().Add(wxT("hide"));
}

// Store collection of files (upload new files as needed)
bool FileManager::StoreCollection(const wxString& name)
{
    wxURI url(m_UploadURL);
    wxString docServer = url.GetScheme() + wxT("://") + url.GetServer();
    wxString path = m_RepoPath;

    wxString statusCode, statusText;

    wxString metaData = generateMeta(name);
    
    long hr = makeDir(docServer, path, statusCode, statusText);
    if (!FAILED(hr) && (!m_pDialogProgress || !m_pDialogProgress->IsCancelled()))
    {
        path = path + name + wxT("/");
        hr = makeDir(docServer, path, statusCode, statusText);
    }
    
    if (!FAILED(hr) && (!m_pDialogProgress || !m_pDialogProgress->IsCancelled()))
    {
        wxString metaPath = path + wxT("meta");
        hr = sendFile(docServer, metaPath, metaData, false, statusCode, statusText);
    }

    for (int i=0; i < GetFileCount() && !FAILED(hr) && checkStatus(statusCode); i++)
    {
        FileDetails* f = GetFile(i);
        wxString filePath = path + f->m_Name;
        hr = sendFile(docServer, filePath, f->m_LocalPath, true, statusCode, statusText);
    }
    
    if (FAILED(hr) || checkStatus(statusCode) == false)
    {
        if (m_Options & ShowErrors)
        {
            wxString errmsg = wxString(_("Unable to upload files.")) + wxT("\r\n");
            long    lCode;
            statusCode.ToLong( &lCode );
            CUtils::ReportDAVError(NULL, lCode, statusText, wxT("%s\n%s\n%s"), errmsg, hr);
        }

        hideProgress();
        m_Status = OK;
        return false;
    }
    
    wxLogDebug(wxT("Upload result: %s"), statusCode.wc_str()); 
    m_Status = UploadError;
    return true;
}

wxString FileManager::generateMeta(const wxString& base)
{
    json_object * array = json_object_new_array();
    
    for (int i=0; i<GetFileCount(); i++)
    {
        FileDetails* f = GetFile(i);
        f->m_Name = wxString(base) << wxT("-") << i << wxT(".") << f->m_Type;
        json_object *f_obj = json_object_new_object();
        json_object_object_add(f_obj, (char*)"name", json_object_new_string((char*)(const char*)f->m_Name.utf8_str()));
        json_object_object_add(f_obj, (char*)"title", json_object_new_string((char*)(const char*)f->m_Title.utf8_str()));
        json_object_object_add(f_obj, (char*)"type", json_object_new_string((char*)(const char*)f->m_Type.utf8_str()));
        json_object_array_add(array, f_obj);
    }
   
    wxString ret = wxString::FromUTF8(json_object_to_json_string(array));
    json_object_put(array);

    wxLogDebug(wxT("Upload meta: %s"), ret.c_str());
    
    return ret;
}

bool FileManager::LoadCollection(const wxString& name)
{
    wxURI url(m_UploadURL);
    wxString docServer = url.GetScheme() + wxT("://") + url.GetServer();
    wxString path = m_RepoPath + name + wxT("/");

    wxString statusCode, statusText;
    wxString data;

    wxString metaPath = path + wxT("meta");
    long hr = getFile(docServer, metaPath, data, false, statusCode, statusText);
    
    if (statusCode == wxT("404"))
    {
        hideProgress();
        m_Status = FileNotFound;
        return false;
    }
    
    if (checkStatus(statusCode))
    {
        hr = parseMeta(data);
        if (FAILED(hr))
            wxLogDebug(wxT("Unable to parse metadata from %d"), path.c_str());
    }

    for (int i=0; i < GetFileCount() && !FAILED(hr) && checkStatus(statusCode); i++)
    {
        FileDetails* f = GetFile(i);
        wxString filePath = path + f->m_Name;
        f->m_LocalPath = GetTempFile(f->m_Type);
        hr = getFile(docServer, filePath, f->m_LocalPath, true, statusCode, statusText);
    }
    
    if (FAILED(hr) || checkStatus(statusCode) == false)
    {
        if (m_Options & ShowErrors)
        {
            wxString errmsg = wxString(_("Unable to download files.")) + wxT("\r\n");
            long    lCode;
            statusCode.ToLong( &lCode );
            CUtils::ReportDAVError(NULL, lCode, statusText, wxT("%s\n%s\n%s"), errmsg, hr);
        }

        hideProgress();
        m_Status = DownloadError;
        return false;
    }
    
    wxLogDebug(wxT("Download result: %s"), statusCode.wc_str());
    m_Status = OK;
    return true;
}

long FileManager::parseMeta(const wxString& meta)
{
    struct json_object *obj;
    const char *data = strdup(meta.utf8_str());

    obj = json_tokener_parse((char*)data);
    if (json_is_error(obj))
        return E_FAIL;
        
    for (int i=0; i < json_object_array_length(obj); i++)
    {
        struct json_object *file_obj = json_object_array_get_idx(obj, i);
        
        const char *name = json_object_get_string(json_object_object_get(file_obj, (char*)"name"));
        const char *title = json_object_get_string(json_object_object_get(file_obj, (char*)"title"));
        const char *type = json_object_get_string(json_object_object_get(file_obj, (char*)"type"));
        
        if (name == NULL || title == NULL || type == NULL)
            return E_FAIL;
        
        FileDetails* f = new FileDetails(wxString::FromUTF8(title), wxString::FromUTF8(name), wxString::FromUTF8(type));
        m_Files.push_back(f);
    }
 
    return 0;
}

long FileManager::CopyCollection(const wxString& name, const wxString& fromPath, bool fDeleteSource)
{
    wxURI url(m_UploadURL);
    wxString docServer = url.GetScheme() + wxT("://") + url.GetServer();
    wxString path = m_RepoPath;

    wxString statusCode, statusText;
    long hr = 0;

    hr = makeDir(docServer, path, statusCode, statusText);
    if (!FAILED(hr) && (!m_pDialogProgress || !m_pDialogProgress->IsCancelled()))
    {
        path = path + name + wxT("/");
        hr = makeDir(docServer, path, statusCode, statusText);
    }

    if (!FAILED(hr) && (!m_pDialogProgress || !m_pDialogProgress->IsCancelled()))
    {
        std::string xml;
        wxString targetURL = docServer + path;
        
        hr = m_WebDavUtils.DAVCopyFolder(m_pParent, docServer, fromPath, targetURL, m_UserName, m_Password, xml, statusCode, statusText);
    }

    //  Time to cleanup - empty out the temp folder
    if (fDeleteSource)
    {
        deleteFile(docServer, fromPath, statusCode, statusText);
    }

    if (FAILED(hr) || !checkStatus(statusCode))
    {
        wxString  errmsg( _("Unable to move uploaded files to new meeting. Please try uploading again."), wxConvUTF8 );
        long    lCode;
        statusCode.ToLong(&lCode);
        CUtils::ReportDAVError(m_pParent, lCode, statusText, wxT("%s\n%s\n%s"), errmsg, hr);
        hideProgress();
    }

    return hr;
}

bool FileManager::LoadAsync(const wxString& name, wxWindow* parent, bool wait)
{
    m_pParent = parent;
    return processAsync(name, _("Downloading pictures"), parent, wait, &FileManager::LoadCollection);
}

bool FileManager::StoreAsync(const wxString& name, wxWindow* parent, bool wait)
{
    m_pParent = parent;
    return processAsync(name, _("Uploading pictures"), parent, wait, &FileManager::StoreCollection);    
}

bool FileManager::processAsync(const wxString& name, const wxString& caption, wxWindow* parent, bool wait, FuncPtr op)
{
    if (m_InProgress)
        return false;
        
    if (m_UploadURL.IsEmpty())
    {
        wxLogDebug(wxT("Upload server not set"));
        return false;        
    }

    m_InProgress = true;
 
    FileManagerNS::ProcessThread* thread = new FileManagerNS::ProcessThread(this, op, name);
    thread->Create();

    if (!wait)
    {
        thread->Run();
        return true;
    }
 
    wxString title = caption;
    wxString prompt = _("Please wait until operation completes.");
    
    Progress1Dialog dlg(title, prompt, 100, parent, wxPD_APP_MODAL|wxPD_CAN_ABORT);
    dlg.SetReturnCode(wxID_CANCEL);
    m_pDialogProgress = &dlg;

    thread->Run();

    while (dlg.IsShown())
    {
        showProgress();
        
        int iLC = 0;
        ::wxMilliSleep(10);
#ifdef WIN32
        while (wxGetApp().Pending() && iLC < 20)
        {
            iLC++;
            wxGetApp().Dispatch();
        }
#else
        while (wxGetApp().Pending())
        {
            iLC++;
            wxGetApp().Dispatch();
        }
#ifdef LINUX
        if (g_main_context_iteration(NULL, false))
            ;
#endif
#endif
    }
    
    bool res = thread->Wait() != NULL;
    
    m_InProgress = false;
    return res;
}

int FileManager::GetFileCount()
{
    return m_Files.size();
}

FileDetails* FileManager::GetFile(int index)
{
    return m_Files[index];
}

void FileManager::AddFile(int index, const wxString& localFile, const wxString& title)
{
    FileDetails* info = new FileDetails(title, localFile);
    if (index < 0)
        m_Files.push_back(info);
    else
        m_Files.insert(m_Files.begin() + index, info);
    m_Dirty = true;
}

void FileManager::RemoveFile(int index)
{
    FileDetails* f = m_Files[index];
    m_Files.erase(m_Files.begin() + index);
    delete f;
    m_Dirty = true;
}

void FileManager::MoveFile(int index, int dir)
{
    FileDetails* f = m_Files[index];
    if (dir < 0)
    {
        if (index > 0)
        {
            m_Files.erase(m_Files.begin() + index);
            m_Files.insert(m_Files.begin() + index - 1, f);
        }
    }
    else 
    {
        if (index < m_Files.size() - 1)
        {
            m_Files.erase(m_Files.begin() + index);
            m_Files.insert(m_Files.begin() + index + 1, f);
        }
    }
    m_Dirty = true;
}

bool FileManager::Exists(const wxString& compareName)
{
    wxFileName check(compareName);
    
    for (int i=0; i<GetFileCount(); i++)
    {
        wxFileName file(GetFile(i)->m_LocalPath);
        if (check.GetSize() == file.GetSize()) 
        {
            return ComputeChecksum(compareName) == ComputeChecksum(GetFile(i)->m_LocalPath);
        }
    }
    return false;
}

wxString FileManager::GetTempFile(const wxString& ext)
{
    return wxString::Format(wxT("%smtgtemp%d.%s"), GetTempPath().c_str(), ++m_TempCount, ext.c_str());
}

wxString FileManager::GetPathForUser(const wxString& userName)
{
    wxString path(USER_PATH);
    path.Replace(wxT("$user"), userName);
    return path;
}

wxString FileManager::GetPathForMeeting(const wxString& meetingId)
{
    wxString path(MEETING_PATH);
    path.Replace(wxT("$mid"), meetingId);
    return path;  
}

long FileManager::ComputeChecksum(wxString fn)
{
    long checksum = 0;
    char buff[8192];
    wxFile f(fn);
    if (f.IsOpened() && !f.Eof())
    {
        int bytes = f.Read(buff,sizeof(buff));
        for (int i=0; i<bytes; i++)
            checksum += *(buff + i);
    }
    return checksum;
}

void* FileManagerNS::ProcessThread::Entry()
{
    if (manager->m_pDialogProgress && manager->m_pDialogProgress->IsCancelled())
        return NULL;

    bool res = (manager->*op)(name);

    if (manager->m_pDialogProgress)
    {
        manager->m_pDialogProgress->Commands().Add(wxT("hide"));
    }
    else if (manager->m_pParent)
    {
        wxCommandEvent ev(wxEVT_FILEMANAGER_DONE, wxID_ANY);
        manager->m_pParent->AddPendingEvent(ev);
        manager->m_pParent = NULL;
    }
    manager->m_InProgress = false;

    return res ? (void*)1 : NULL;
}

