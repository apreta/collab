/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "waitforstartdialog.h"

//(*InternalHeaders(WaitForStartDialog)
#include <wx/bitmap.h>
#include <wx/button.h>
#include <wx/font.h>
#include <wx/fontenum.h>
#include <wx/fontmap.h>
#include <wx/image.h>
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(WaitForStartDialog)
//*)

#include "app.h"
#include "meetingcentercontroller.h"
#include "session_api.h"
#include "events.h"


BEGIN_EVENT_TABLE(WaitForStartDialog,wxDialog)
	//(*EventTable(WaitForStartDialog)
	//*)
END_EVENT_TABLE()


//*****************************************************************************
WaitForStartDialog::WaitForStartDialog(wxWindow* parent,wxWindowID id)
{
	//(*Initialize(WaitForStartDialog)
	wxXmlResource::Get()->LoadObject(this,parent,_T("waitforstartdialog"),_T("wxDialog"));
	StaticText1 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT1"));
	StaticText2 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT2"));
    //*)
}


//*****************************************************************************
WaitForStartDialog::~WaitForStartDialog()
{
}


//*****************************************************************************
void WaitForStartDialog::OnUpdateStatus( wxCommandEvent& event )
{
    wxLogDebug( wxT("entering WaitForStartDialog::OnUpdateStatus( )") );
    int     iStatus = event.GetInt( );
    if( iStatus  ==  ERR_SUCCESS )
    {
        wxLogDebug( wxT("entering WaitForStartDialog::OnUpdateStatus( ) -- status is:  RECONNECTED!!!") );
    }
    else if( (iStatus == ERR_NOTCONNECTED)   ||
             (iStatus == ERR_CONNECTFAILED)  ||
             (iStatus == ERR_CONNECTLOST)       )
    {
        wxLogDebug( wxT("entering WaitForStartDialog::OnUpdateStatus( ) -- status is:  we are not connected (%d)"), iStatus );
    }
}


//*****************************************************************************
void WaitForStartDialog::SetupForSecondCancel( )
{
    wxLogDebug( wxT("entering WaitForStartDialog::SetupForSecondCancel( )") );

    wxString    strT( _("Waiting for host or moderator to start meeting"), wxConvUTF8 );
    StaticText1->SetLabel( strT );
    strT.Clear( );
    StaticText2->SetLabel( strT );

    wxButton    *pBtn;
    pBtn = (wxButton*)FindWindow(XRCID("wxID_OK"));
    if( pBtn )
    {
        pBtn->Hide( );
    }
    pBtn = (wxButton*)FindWindow(XRCID("wxID_CANCEL"));
    if( pBtn )
    {
        wxString    strT2( _("Cancel"), wxConvUTF8 );
        pBtn->SetLabel( strT2 );
    }
}
