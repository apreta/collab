/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef MEETINGCONFIRMATION_H
#define MEETINGCONFIRMATION_H

#include "schedule.h"

//(*Headers(MeetingConfirmation)
#include <wx/stattext.h>
#include <wx/radiobut.h>
#include <wx/panel.h>
#include <wx/dialog.h>
#include <wx/html/htmlwin.h>
//*)

class MeetingConfirmation: public wxDialog
{
	public:

		MeetingConfirmation(wxWindow* parent,wxWindowID id = -1);
		virtual ~MeetingConfirmation();

		//(*Identifiers(MeetingConfirmation)
		//*)

        /** Misc. constants and control values */
        enum MiscValues
        {
            LargeMeetingThreshold = 10,     /**< Meetings having more than this numner of invitees are large */
        };

        /**
          * Prepare the display according to the mask bits in m_nDisplayMask.
          */
        bool PrepareDisplay();

        /**
          * Get confirmation to schedule or start a meeting.<br>
          * <br>
          * The meeting details are analyzed to determine if a confirmation
          * dialog is required. If no confirmation dialog is displayed then SendEmailNone is retunred.<br>
          * <br>
          * When a confirmation dialog is displayed then:<br>
          * If the user clicks OK, then the selected
          * SendEmailOptions value is returned (0 .. n)<br>
          * If the user clicks Cancel, then -1 is returned.
          * @param fStarting Set this to true if you are about to start the meeting.
          * @param mtg Ptr. to the meeting details.
          * @return int The SendEmailOption on OK, -1 on Cancel.
          */
        int GetConfirmation( bool fStarting, int nDisplayMask );

        enum MeetingConfirmationMask
        {
            MCM_EmailOptions =      0x00000001,      /**< Email options radio buttons are displayed */
            MCM_EmailNote    =      0x00000002,      /**< Display m_strEmailsDontSend */
            MCM_NeedWarning  =      0x00000004,      /**< Display m_strPleaseNote */

            MCM_NeedNotify   =      0x00000008,      /**< Display m_strStartSendReminders */
            MCM_NotifyCall   =      0x00000010,          /**< Display m_strStartWillCall if NeedNotify is set */
            MCM_NotifyEmail  =      0x00000020,          /**< Display m_strStartWillEmail if NeedNotify is set*/
            MCM_NotifyIM     =      0x00000040,          /**< Display m_strStartWillIM if NeedNotify is set*/

            MCM_LargeMeeting =      0x00000080,      /**< Display (NotifyCall) ? m_strLargeMeetingContact : m_strLargeMeeting */
            MCM_ModeratorExternal = 0x00000100,      /**< Display m_strNonSysModerator */
            MCM_InvalidTime =       0x00000200,      /**< Display m_strTimeUnspecified */
            MCM_ClickOK =           0x00000400       /**< Display m_strClickOK */
        };


	protected:
		//(*Handlers(MeetingConfirmation)
		//*)

		//(*Declarations(MeetingConfirmation)
		wxRadioButton* m_SendToNew;
		wxStaticText* m_HeaderMsg;
		wxStaticText* StaticText1;
		wxRadioButton* m_SendToAll;
		wxRadioButton* m_SendToNone;
		wxPanel* m_Panel;
		wxHtmlWindow* m_HTML;
		//*)

	private:

        // The various notes that can be displayed and the associated control flags
        int  m_nNoteCount;
        int  m_nDisplayMask;
        bool m_fStartingMeeting;

        wxString m_strPleaseNote;
        wxString m_strEmailsDontSend;
        wxString m_strNonSysModerator;
        wxString m_strTimeUnspecified;
        wxString m_strStartSendReminders;
        wxString m_strStartWillCall;
        wxString m_strStartWillEmail;
        wxString m_strStartWillIM;
        wxString m_strLargeMeetingContact;
        wxString m_strLargeMeeting;
        //wxString m_strClickOK;

        wxString m_strDisplay;

		DECLARE_EVENT_TABLE()
};

#endif
