/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef SELECTCONTACTDIALOG_H
#define SELECTCONTACTDIALOG_H

class wxGrid;

//(*Headers(SelectContactDialog)
#include <wx/treelistctrl.h>
#include <wx/button.h>
#include <wx/dialog.h>
//*)

DECLARE_LOCAL_EVENT_TYPE(wxEVT_INVITE_SELECTED_CONTACTS, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_HIDE_CONTACT_LIST, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_SEARCH_CONTACTS, -1);

class SelectContactDialog: public wxDialog
{
	public:

		SelectContactDialog(wxWindow* parent,wxWindowID id = -1);
		virtual ~SelectContactDialog();

		//(*Identifiers(SelectContactDialog)
		//*)

	protected:
        /**
          * StateFlags: m_nState is a bitmask made up of these values.
          */
        enum StateFlags
        {
            State_Idle =                0x00000000, /**< No state */
            State_Sorting =             0x00000001  /**< Sort is in progress */
        };

        /**
          * Return true if the specified state flag(s) are set.
          * @param flags The State_Xxxxx mask.
          * @return bool Returns true if one or more of the flags bits are set in m_nState.
          */
        bool IsState( int flags )
        {
            return (m_nState & flags) != 0;
        }

        /**
          * Clear the specified state flag(s).
          * @param flags The State_Xxxxx mask.
          */
        void ClearState( int flags )
        {
            m_nState &= ~flags;
        }
        /**
          * Set the specified state flag(s).
          * @param flags The State_Xxxxx mask.
          */
        void SetState( int flags )
        {
            m_nState |= flags;
        }

		//(*Handlers(SelectContactDialog)
		//*)

		//(*Declarations(SelectContactDialog)
		wxButton* m_HideButton;
		wxTreeListCtrl* m_ContactTreeList;
		wxButton* mSearchButton;
		wxButton* m_InviteButton;
		//*)

        void OnSize( wxSizeEvent& event );
        void OnMove( wxMoveEvent& event );


	private:
        int m_nState;   /**< The current run state */

		DECLARE_EVENT_TABLE()
};

#endif
