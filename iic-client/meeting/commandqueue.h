/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef COMMANDQUEUE_H_INCLUDED
#define COMMANDQUEUE_H_INCLUDED

#include "commandqueuelist.h"

/**
  * This class contains a simple list of commands that are
  * to be executed in FIFO order.
  */
class CommandQueue
{
public:
    /** Constructor */
    CommandQueue();

    /** Add a command
      * @param name The name of the command.
      * @param p0 Parameter 1
      * @param p1 Parameter 2
      * @param p2 Parameter 3
      * @param p3 Parameter 4
      * @param p4 Parameter 5
      * @return int Returns the position in the queue (0..n)
      */
    int Add( const wxString &name );
    int Add( const wxString &name, const wxString & p0 );
    int Add( const wxString &name, const wxString & p0, const wxString & p1 );
    int Add( const wxString &name, const wxString & p0, const wxString & p1, const wxString & p2 );
    int Add( const wxString &name, const wxString & p0, const wxString & p1, const wxString & p2, const wxString & p3 );
    int Add( const wxString &name, const wxString & p0, const wxString & p1, const wxString & p2, const wxString & p3, const wxString & p4 );

    /**
      * Remove the next command from the queue and return it's data.<br>
      * The first node is removed from the list, and the caller of this method
      * owns the CommandQueueElement memory.
      * @return CommandQueueElement * Return a pointer the next command if there is one. Return zeo if the queue is empty.
      */
    CommandQueueElement * GetNextCommand();

    /** Return a ref to the list */
    const CommandQueueList & GetList();

    /** Append a new command to the end of the list */
    int AppendCommand( CommandQueueElement *pCmd );

    /** Append a parameter to the last command in the queue */
    int AppendParameter( const wxString & pX );

private:
    /** Return a pointer to the last command in the queue */
    CommandQueueElement * LastCmd();

    /** The list of commands */
    CommandQueueList m_list;

};


#endif // COMMANDQUEUE_H_INCLUDED
