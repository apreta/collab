/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef PROFILEMANAGERDIALOG_H
#define PROFILEMANAGERDIALOG_H

#include "schedule.h"
#include "addressbk.h"
#include "wx/propgrid/propgrid.h"
#include "services/common/common.h"

//(*Headers(ProfileManagerDialog)
#include <wx/stattext.h>
#include <wx/panel.h>
#include <wx/button.h>
#include <wx/dialog.h>
#include <wx/combobox.h>
//*)

class SessionErrorEvent;

/**
  */
class CProfile
{
public:
    CProfile();

    /** Mutators */
    CProfile & operator=(profile_t rhs);
    CProfile & operator=(const CProfile & rhs);


    void Set( profile_t profile );

    /** Accessors */
    void Get( profile_t profile );

    /** Equality operators */
    bool operator == (profile_t rhs);
    bool operator == (const CProfile & rhs);
    bool operator!=(profile_t rhs);
    bool operator!=(const CProfile & rhs);

    /**
      * @return bool Return true if the specified profile contains the same data as this instance.
      */
    bool IsIdentical( profile_t profile );

public:
    wxString options;
    wxString profile_id;
    wxString name;
    unsigned    enabled_features;           // see ProfileFlags enum in common.h
    int         max_voice_size;             // currently not used
    int         max_data_size;              // currently not used
    int         max_presence_monitors;
    int         max_scheduled_meetings;
    int         pin_length;
    int         meeting_id_length;
    int         password_length;
    int         max_idle_minutes;
    int         max_connect_minutes;
};

/**
  */
class ProfileManagerDialog: public wxDialog
{
	public:

		ProfileManagerDialog(wxWindow* parent,wxWindowID id = -1);
		virtual ~ProfileManagerDialog();

		//(*Identifiers(ProfileManagerDialog)
		//*)

        /**
          * The main interface.
          * @return int Returns wxID_OK or wxID_CANCEL
          */
        int ManageProfiles();

	protected:
        /** Fetch the existing profile records from the server.
          * @return int Returns zero if all wen well. Returns non-zero on failure.
          */
        int FetchProfiles();

        /** If the active profile has been changed, then as the user if they wish to save
          * it's content to the database.
          * @return bool Return true if the save was successful.
          */
        bool QuerySaveChanges();

        /** Save the active profile to the database.
          * @return int Returns zero if successful, returns non-zero on failure.
          */
        int SaveActiveProfile();

        /** Save the specified profile to the database.
          * @param profile The profile record to be saved.
          * @return int Returns zero if successful, returns non-zero on failure.
          */
        int SaveProfile( profile_t profile );

        /** Delete the specified profile from the database.
          * @param profile The profile record to be removed.
          * @return int Returns zero if successful, returns non-zero on failure.
          */
        int DeleteProfile( profile_t profile );

        /** Select the specified profile by index.<br>
          * The specified profile is selected in the combo box.<br>
          * The m_profile, m_active_profile, and m_original_profile members are populated.<br>
          * The GUI is updated to display the profile content.
          @param ndx The target profile index.
          @param name The target profile name.
          */
        bool SelectProfile( int ndx );
        bool SelectProfile( const wxString &name );


        /** Event processing: The OK button was clicked */
        void OnOkClicked(wxCommandEvent& event);

        /** Event processing: The Cancel button was clicked */
        void OnCancelClicked(wxCommandEvent& event );

        /** Event processing: The Cancel button was clicked */
        void OnApplylicked(wxCommandEvent& event );

        /** Event processing: Profile records have been fetched */
        void OnProfileFetched(wxCommandEvent& event);

        /** Event processing: Error from server */
        void OnSessionError(SessionErrorEvent& event);

        /** Event processing: Successful update */
        void OnAddressUpdated(wxCommandEvent& event);

        /** Event processing: Successful delete */
        void OnAddressDeleted(wxCommandEvent& event);

        /** Event processing: The property grid has changed.
          */
        void OnPropertyGridChanged( wxPropertyGridEvent& event );

        /** Transfer m_profile to the display. */
        void Value_to_GUI();

        /** Transfer the displayed values to m_profile. */
        void GUI_to_Value();

        /** Create a new profile record using the specified name
          * @param newName The profile_t.name
          * @param fromProfile Optional profile template.
          * @return int Returns zero if successful, returns non-zero on error
          */
        int CreateNewProfile( wxString & newName, profile_t fromProfile = 0 );

        /** Check for duplicate names.
          * @param name The name to look for.
          * @return bool Returns true if the name is in use.
          */
        bool IsDuplicateProfileName( const wxString & name );

        /** Prompt the user for a unique name
          * @param initialDefault The starting name to display
          * @return wxString Returns the name.
          */
        wxString AskForUniqueName( const wxString & initialDefault );

		//(*Handlers(ProfileManagerDialog)
		void OnPolicyComboSelect(wxCommandEvent& event);
		void OnNewPolicyButtonClick(wxCommandEvent& event);
		void OnCopyPolicyButtonClick(wxCommandEvent& event);
		void OnDeletePolicyButtonClick(wxCommandEvent& event);
		//*)

        void OnClickHelp(wxCommandEvent& event);

		//(*Declarations(ProfileManagerDialog)
		wxStaticText* StaticText2;
		wxButton* m_NewPolicyButton;
		wxStaticText* StaticText1;
		wxComboBox* m_PolicyCombo;
		wxButton* m_CopyPolicyButton;
		wxPanel* m_PropGridPanel;
		wxButton* m_DeletePolicyButton;
		//*)

	private:
        /**
          * StateFlags: m_nState is a bitmask made up of these values.
          */
        enum StateFlags
        {
            State_Idle =                0x00000000, /**< No state */
            State_WaitingForUpdate =    0x00000001  /**< Waiting for address book update */
        };

        /**
          * Return true if the specified state flag(s) are set.
          * @param flags The State_Xxxxx mask.
          * @return bool Returns true if one or more of the flags bits are set in m_nState.
          */
        bool IsState( int flags )
        {
            return (m_nState & flags) != 0;
        }

        /**
          * Clear the specified state flag(s).
          * @param flags The State_Xxxxx mask.
          */
        void ClearState( int flags )
        {
            m_nState &= ~flags;
        }

        /**
          * Set the specified state flag(s).
          * @param flags The State_Xxxxx mask.
          */
        void SetState( int flags )
        {
            m_nState |= flags;
        }

        void ClearDatabaseError()
        {
            SetDatabaseError( 0, wxT("") );
        }

        /**
          * Wait for state flags to clear.<br>
          * If the operation times out, then the flags are cleared.
          * @param flags The State flags to wait for.
          * @param nSeconds The number of seconds to wait.
          * @param errStr The error message string if the wait times out (Optional)
          * @return int Returns zero if the flags were cleared prior to timeout. Returns -1 on timeout.
          */
        int WaitForClear( int flags, int nSeconds, const wxString & errMsg );

        void SetDatabaseError( int nDatabaseError, const wxString & strDatabaseError, bool fShowError = false )
        {
            m_nDatabaseError = nDatabaseError;
            m_strDatabaseError = strDatabaseError;
            if( fShowError )
            {
                ShowDatabaseError();
            }
        }

        void ShowDatabaseError();

        void goModeless();
        void goModal();

        /** Test a m_profile.enabled_features flag.
          * @param flag The flag bits to test.
          * @return 1 if the specified flag is set. Return 0 if the specified flag is clear.
          */
        int IsSet( unsigned int flag )
        {
            return (m_profile && (m_active_profile.enabled_features & flag) != 0) ? 1:0;
        }
        /** Test a m_profile.enabled_options flag.
          * @param flag The flag bits to test.
          * @return 1 if the specified flag is clear. Return 0 if the specified flag is set
          */
        int IsClr( unsigned int flag )
        {
            return (!(m_profile && (m_active_profile.enabled_features & flag) != 0)) ? 1:0;
        }

        /** Clear the specified m_profile.enabled_features flag if the target property is enabled.
          * @param flag The flag bits to be set;
          * @param id The property ID.
          * @return none;
          */
        void ClrFlag( unsigned int flag, wxPGId & id )
        {
            if( m_profile && m_Grid )
            {
                int value = m_Grid->GetPropertyValueAsInt( id );
                if( value )
                {
                    m_active_profile.enabled_features &= ~flag;
                }else
                {
                    m_active_profile.enabled_features |= flag;
                }
            }
        }

        /** Set the specified m_profile.enabled_features flag if the target property is enabled.
          * @param flag The flag bits to be set;
          * @param id The property ID.
          * @return none;
          */
        void SetFlag( unsigned int flag, wxPGId & id )
        {
            if( m_profile && m_Grid )
            {
                int value = m_Grid->GetPropertyValueAsInt( id );
                if( value )
                {
                    m_active_profile.enabled_features |= flag;
                }else
                {
                    m_active_profile.enabled_features &= ~flag;
                }
            }
        }

private:
        wxString m_strCaption;      /**< The standard caption */
        wxPropertyGrid *m_Grid;     /**< The property grid */

        ///////////////////////////////////////////////////////////////////
        // Property sheet ID numbers
        wxPGId m_id_savepassword;
        wxPGId m_id_zonim;
        wxPGId m_id_dldpublicmtg;
        wxPGId m_id_dldcab;
        wxPGId m_id_sharedesktop;
        wxPGId m_id_shareapp;
        wxPGId m_id_shareppt;
        wxPGId m_id_sharedoc;
        wxPGId m_id_sharewhiteboard;
        wxPGId m_id_remotecontrol;
        wxPGId m_id_npresencemon;
        wxPGId m_id_invtpinlen;
        wxPGId m_id_usermtgidlen;
        wxPGId m_id_maxschedmetg;
        wxPGId m_id_locktimout;
        wxPGId m_id_sesstimeout;
        wxPGId m_id_projectcode;

        /** The active profile record:<br>
          * This is the record currently selected in the m_PolicyCombo.<br>
          * The values displayed on the GUI always map to this record.<br>
          * Value_to_GUI() and GUI_to_Value() always act on this record.<br>
          */
        int m_nActiveProfile;
        profile_t m_profile;
        CProfile m_active_profile;

        /** The original active profile record:<br>
          * This is a copy of the record currently selected in the m_PolicyCombo.<br>
          * It is used to detect changes between the original settings and the currently
          * displayed settings.
          */
        CProfile m_original_profile;

        int m_nState;                       /**< The current run state */
        wxString m_strDatabaseError;        /**< The last known database error message */
        int m_nDatabaseError;               /**< The last known database error code */

		DECLARE_EVENT_TABLE()
};

#endif
