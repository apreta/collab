/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"
#include "meetingcentercontroller.h"
#include "docexplorerdialog.h"
#include "iicdoc.h"
#include "docshareview.h"

//(*InternalHeaders(DocExplorerDialog)
#include <wx/xrc/xmlres.h>
//*)

#include "addressbk.h"

#include "iksemel.h"

#include <vector>

//(*IdInit(DocExplorerDialog)
//*)

#ifndef __WXMSW__

/* Definitions should be moved into a portability header */
#define FAILED(x)       (x < 0)
#define SUCCEEDED(x)    (x >= 0)
#endif

static bool gbGotHref = false;
static std::string buffer;

typedef std::vector<std::string> STRINGVECTOR;
STRINGVECTOR    gArray;


BEGIN_EVENT_TABLE(DocExplorerDialog,wxDialog)
	//(*EventTable(DocExplorerDialog)
	//*)
END_EVENT_TABLE()

DocExplorerDialog::DocExplorerDialog( wxWindow* parent, CIICDoc *pDocument, wxString strMtgID, wxString strDocID, bool fInMeeting )
    : m_strDocID( strDocID ), 
      m_fErrorInitializing( false ), 
      m_pDocument( pDocument ), 
      m_strMtgID( strMtgID ), 
      m_fInMeeting( fInMeeting )
{
	//(*Initialize(DocExplorerDialog)
	wxXmlResource::Get()->LoadObject(this,parent,_T("DocExplorerDialog"),_T("wxDialog"));
	m_textRecsFor = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT1"));
	m_list = (wxListCtrl*)FindWindow(XRCID("ID_LISTCTRL1"));
	m_btnOK = (wxButton*)FindWindow(XRCID("ID_BTN_OK"));
	m_btnCancel = (wxButton*)FindWindow(XRCID("ID_BTN_CANCEL"));
	m_btnUpload = (wxButton*)FindWindow(XRCID("ID_BTN_UPLOAD"));
	m_btnDelete = (wxButton*)FindWindow(XRCID("ID_BTN_DELETE"));
	
	Connect(XRCID("ID_LISTCTRL1"),wxEVT_COMMAND_LIST_ITEM_SELECTED,(wxObjectEventFunction)&DocExplorerDialog::OnItemSelected);
	Connect(XRCID("ID_BTN_OK"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&DocExplorerDialog::OnBtnOKClick);
	Connect(XRCID("ID_BTN_CANCEL"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&DocExplorerDialog::OnBtnCancelClick);
	Connect(XRCID("ID_BTN_UPLOAD"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&DocExplorerDialog::OnBtnUploadClick);
	Connect(XRCID("ID_BTN_DELETE"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&DocExplorerDialog::OnBtnDeleteClick);
	//*)

    wxString strT;
    if( m_strMtgID.IsNumber( ) )
    {
        strT = _("Document list for meeting: ");
        strT += m_strMtgID;
    }
    m_textRecsFor->SetLabel( strT );

    if ( !fInMeeting )
    {
        m_btnOK->Show( false );
        m_btnCancel->SetLabel( _("Done") );
    }

    wxString    strTargetFileURL( m_pDocument->m_Meeting.GetDocShareServer( ) );

    wxString    strTargetFilePath( m_pDocument->m_Meeting.GetDocSharePath( ) );
    strTargetFilePath += m_strMtgID;
    strTargetFilePath += wxT("/");

    wxString        xml;
    wxString        statusCode, strStatusText;
    wxString        errmsg;

    Cleanup( );

    int     hr = RetrieveDAVInfo(strTargetFileURL, strTargetFilePath, xml, statusCode, strStatusText);
    if( SUCCEEDED(hr) )
    {
        if (statusCode == _T("207") && xml.length()>0)
        {
            ParseResponse( xml.mb_str( wxConvUTF8 ) );
        }
        else if (statusCode == _T("404"))	// maybe a new meeting
        {
            // continue
        }
        else
        {
            long lCode;
            statusCode.ToLong(&lCode);
            CUtils::ReportDAVError(this, lCode, strStatusText,
                                   errmsg + _T("\n%s\n%s\n%s"),
                                   strTargetFileURL, 0);
            m_fErrorInitializing = true;
        }
    }
    else
    {
        wxString    errmsg( _("Failed to retrieve document list from server, please try again later."), wxConvUTF8 );
        long lCode;
        statusCode.ToLong(&lCode);
        CUtils::ReportDAVError(this, lCode, strStatusText,
                               errmsg + _T("\n%s\n%s\n%s"),
                               strTargetFileURL, hr);
        m_fErrorInitializing = true;
    }

    m_btnOK->Enable( false );
    m_btnDelete->Enable( false );
}


//*****************************************************************************
DocExplorerDialog::~DocExplorerDialog()
{
	//(*Destroy(DocExplorerDialog)
    //*)

//    Cleanup( );
}


//*****************************************************************************
int DocExplorerDialog::RetrieveDAVInfo( const wxString& aUrl, const wxString& aUrlPath, wxString& outXML, wxString& statusCode, wxString& strStatusText )
{
    wxString    userName, passwd, iicServer;
    m_pDocument->GetSessionInfo( userName, passwd, iicServer );
    CWebDavUtils dav;
    if (!m_fInMeeting)
        dav.AddHeader( wxT("X-iic-host: true") );
    return dav.DAVRetrieveInfo( this, aUrl, aUrlPath, userName, passwd, outXML, statusCode, strStatusText );
}


//*****************************************************************************
static void pr_tag_start (void *udata, char *name, char **atts)
{
    if (strcmp(name, "D:href")==0)
    {
        gbGotHref = TRUE;
        buffer.clear();
    }
}


//*****************************************************************************
static int pr_tag (void *udata, char *name, char **atts, int type)
{
    if (gbGotHref) 
    {
        gbGotHref = false;
        gArray.push_back( buffer.c_str() );
        buffer.clear();
    }
    if (type == 0)
        pr_tag_start(udata, name, atts);

    return 0;
}


//*****************************************************************************
static int pr_cdata (void *udata, char *data, size_t len)
{
    if (gbGotHref)
    {
        buffer += std::string(data, len);
    }
    return 0;
}


//*****************************************************************************
int DocExplorerDialog::ParseResponse( const char *aXml )
{
    int         i;
    std::string strLine;
    iksparser   *p;
    p = iks_sax_new( NULL, pr_tag, pr_cdata );       // expat broke the udata

    int     hr = iks_parse (p, (char*)aXml, strlen(aXml), 1);
    switch( hr )
    {
        case IKS_OK:
            for( i=0; i<(int)gArray.size(); i++)
            {
                strLine = gArray.at( i );
            }
            RefillList( );
            break;
        case IKS_NOMEM:
            wxLogDebug( wxT("Not enough memory") );
            break;
        case IKS_BADXML:
            wxLogDebug( wxT("XML document is not well-formed") );
            break;
        case IKS_HOOK:
            wxLogDebug( wxT("Our hooks did not like something") );
            break;
    }
    iks_parser_delete( p );

    return hr;
}


//*****************************************************************************
void DocExplorerDialog::RefillList( )
{
    int         i, iLen;
    std::string strBaseFolder;
    std::string strLine;
    std::string strFolder;

    //  Delete everything in the list and start over

    strBaseFolder = gArray.at( 0 );
    iLen = strBaseFolder.length( );

    for( i=1; i<(int)gArray.size(); i++ )
    {
        strLine = gArray.at( i );
        strFolder = strLine.substr( iLen );
        wxString    strT( strFolder.c_str( ), wxConvUTF8 );
        CUtils::DecodeStringFully( strT );
        wxString    strT2( strT );
        strT.EndsWith( wxT("/"), &strT2 );
        if( !strT2.Contains( wxT("TempoUpload") ) && !strT2.Contains( wxT("meetingPictures") ) )
            m_list->InsertItem( i-1, strT2 );
    }
}


//*****************************************************************************
void DocExplorerDialog::Cleanup( )
{
    m_list->ClearAll( );
    gArray.clear( );
}


//*****************************************************************************
//
//  Event handlers start here
//
//*****************************************************************************
void DocExplorerDialog::OnBtnOKClick(wxCommandEvent& event)
{
    wxLogDebug( wxT("entering DocExplorerDialog::OnBtnOKClick( )") );

    m_strDocID.Empty( );
    if( m_list->GetSelectedItemCount( )  ==  1 )
    {
        long    lSel = -1;
        lSel = m_list->GetNextItem( lSel, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED );
        if( lSel != -1 )
        {
            m_strDocID = m_list->GetItemText( lSel );
            EndModal( wxID_OK );
            return;
        }
    }

    EndModal( wxID_OK );
}


//*****************************************************************************
void DocExplorerDialog::OnBtnCancelClick(wxCommandEvent& event)
{
    wxLogDebug( wxT("entering DocExplorerDialog::OnBtnCancelClick( )") );
    EndModal( wxID_CANCEL );
}


//*****************************************************************************
void DocExplorerDialog::OnBtnUploadClick(wxCommandEvent& event)
{
//    wxLogDebug( wxT("entering DocExplorerDialog::OnBtnUploadClick( )") );
//    EndModal( wxID_UP );

    DocShareView *pView = (DocShareView*)m_pDocument->GetFirstView();
    bool fRet;

    Enable( false );
    fRet = pView->Upload( m_strDocID );
    Enable( true );
    if ( fRet )
    {
        if ( m_fInMeeting )
            EndModal( wxID_OK );
        else
        {
            SetFocus( );
            RefreshList( );
        }
    }
    
}


//*****************************************************************************
void DocExplorerDialog::OnBtnDeleteClick(wxCommandEvent& event)
{
    wxLogDebug( wxT("entering DocExplorerDialog::OnBtnDeleteClick( )") );

    if( m_list->GetSelectedItemCount( )  ==  1 )
    {
        long    lSel = -1;
        lSel = m_list->GetNextItem( lSel, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED );
        if( lSel != -1 )
        {
            wxString    strCaption( _("Please Confirm"), wxConvUTF8 );
            wxString    strMsg( _("Are you sure that you want to delete the selected document?"), wxConvUTF8 );
            wxMessageDialog dlg( NULL, strMsg, strCaption, wxYES_NO | wxICON_QUESTION );
            if( dlg.ShowModal( )  !=  wxID_YES )
                return;

            m_strDocID = m_list->GetItemText( lSel );

            wxString    strStatusCode, strStatusText;

            wxString    strTargetFileURL( m_pDocument->m_Meeting.GetDocShareServer( ) );

            wxString    strTargetFilePath(m_pDocument->m_Meeting.GetDocSharePath());
            strTargetFilePath += m_strMtgID;
            strTargetFilePath += wxT("/");

            wxString    strT( m_strDocID );
            CUtils::EncodeStringFully( strT );
            strTargetFilePath += strT;
            strTargetFilePath += wxT('/');

            wxBeginBusyCursor( );

            DocShareView *pView = (DocShareView*)m_pDocument->GetFirstView();

            long  hr = pView->AsyncSendRequest( wxT("DELETE"), strTargetFileURL,
                                                strTargetFilePath, NULL, 0, strStatusCode, strStatusText, 
                                                false, wxT("") );
            if( FAILED(hr)  ||  strStatusCode != wxT("204") )
            {
                wxString    errMsg( _T("Failed to delete selection."), wxConvUTF8 );
                long    lCode;
                strStatusCode.ToLong( &lCode );
                wxEndBusyCursor( );
                CUtils::ReportDAVError( this, lCode, strStatusText,
                                       wxT("%s\n%s\n%s"), errMsg, hr );
                return;
            }
            else
            {
                m_btnDelete->Enable( false );
            }

            //  We just successfully deleted a document - refresh the list
            RefreshList( );
            
            wxEndBusyCursor( );
       }
    }
}


//*****************************************************************************
void DocExplorerDialog::OnItemSelected( wxListEvent& event )
{
    bool    fEnable     = (m_list->GetSelectedItemCount( ) == 1) ? true : false;

    m_btnOK->Enable( fEnable );
    m_btnDelete->Enable( fEnable );
}


//*****************************************************************************
void DocExplorerDialog::RefreshList( )
{
    wxBeginBusyCursor( );
    
    Cleanup( );

    wxString strTargetFileURL;
    wxString  strTargetFilePath;
    wxString statusCode;
    wxString strStatusText;
    wxString xml;
    
    gbGotHref = FALSE;
    strTargetFileURL = m_pDocument->m_Meeting.GetDocShareServer( );
    strTargetFilePath = m_pDocument->m_Meeting.GetDocSharePath( );
    strTargetFilePath += m_strMtgID;
    strTargetFilePath += _T("/");

    if( RetrieveDAVInfo( strTargetFileURL, strTargetFilePath, xml, statusCode, strStatusText ) == S_OK )
    {
        wxEndBusyCursor( );
        if( statusCode == wxT("207")  &&  xml.length( ) > 0 )
        {
            wxBeginBusyCursor( );
            ParseResponse( xml.mb_str( wxConvUTF8 ) );
        }
    }
    wxEndBusyCursor( );    
}
