/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
// Base64.cpp: implementation of the CBase64 class.
// Author: Wes Clyburn (clyburnw@enmu.edu)
//
// This code was practically stolen from:
// Dr. Dobb's Journal, September 1995,
// "Mime and Internet Mail", by Tim Kientzle
//////////////////////////////////////////////////////////////////////

//#include "stdafx.h"
#include "wx_pch.h"
#include "base64.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#endif


// Static Member Initializers

// The 7-bit alphabet used to encode binary information
char CBase64::m_sBase64Alphabet[] =
"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

int CBase64::m_nMask[] = { 0, 1, 3, 7, 15, 31, 63, 127, 255 };

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBase64::CBase64()
{
	m_nBitsRemaining = 0;
    m_lBitStorage = 0;
}

CBase64::~CBase64()
{
}

char* CBase64::Encode(const char *szEncoding, int nSize)
{
	m_nBitsRemaining = 0;
    m_lBitStorage = 0;

    char* sOutput = NULL;
	char* sOutputPtr = NULL;
	int nNumBits = 6;
	unsigned nDigit;
	int lp = 0;

	if( szEncoding == NULL )
		return sOutput;

	sOutput = new char[(2*nSize)+3];
	memset(sOutput, 0, (2*nSize)+3);
	sOutputPtr = sOutput;

	m_szInput = szEncoding;
	m_nInputSize = nSize;

	m_nBitsRemaining = 0;
	nDigit = read_bits( nNumBits, &nNumBits, lp );
	int totalRead = 0;
	while( nNumBits > 0 )
	{
		totalRead++;
		*sOutput++ = m_sBase64Alphabet[ (int)nDigit ];
		nDigit = read_bits( nNumBits, &nNumBits, lp );
	}
	// Pad with '=' as per RFC 1521
	while( totalRead % 4 != 0 )
	{
		totalRead++;
		*sOutput++ = '=';
	}
	return sOutputPtr;
}

// The size of the output buffer must not be less than
// 3/4 the size of the input buffer. For simplicity,
// make them the same size.
int CBase64::Decode(const char *szDecoding, char *szOutput)
{
	m_nBitsRemaining = 0;
    m_lBitStorage = 0;

//    char* sInput = NULL;
    int c, lp =0;
	int nDigit;
    int nDecode[ 256 ];

	if( szOutput == NULL )
		return 0;
	if( szDecoding == NULL )
		return 0;
	int size = strlen(szDecoding);
	if( size == 0 )
		return 0;

	// Build Decode Table
    //
    int     i;
	for( i=0; i < 256; i++ )
		nDecode[i] = -2; // Illegal digit
	for( i=0; i < 64; i++ )
	{
		nDecode[ (int) m_sBase64Alphabet[ i ] ] = i;
		nDecode[ (int) m_sBase64Alphabet[ i ] | 0x80 ] = i; // Ignore 8th bit
		nDecode[ (int) ('=') ] = -1;
		nDecode[ (int) ('=' | 0x80) ] = -1; // Ignore MIME padding char
    }

	// Clear the output buffer
	memset( szOutput, 0, size + 1 );

	// Decode the Input
	for( lp = 0, i = 0; lp < size; lp++ )
	{
		c = szDecoding[ lp ];
		nDigit = nDecode[ c & 0x7F ];
		if( nDigit < -1 )
		{
			return 0;
		}
		else if( nDigit >= 0 )
		{
			// i (index into output) is incremented by write_bits()
			write_bits( nDigit & 0x3F, 6, szOutput, i );
		}
    }
	return i;
}

unsigned CBase64::read_bits(int nNumBits, int * pBitsRead, int& lp)
{
    unsigned lScratch;
    while( ( m_nBitsRemaining < nNumBits ) &&
		   ( lp < m_nInputSize ) )
	{
		int c = m_szInput[ lp++ ];
        m_lBitStorage <<= 8;
        m_lBitStorage |= (c & 0xff);
		m_nBitsRemaining += 8;
    }
    if( m_nBitsRemaining < nNumBits )
	{
		lScratch = m_lBitStorage << ( nNumBits - m_nBitsRemaining );
		*pBitsRead = m_nBitsRemaining;
		m_nBitsRemaining = 0;
    }
	else
	{
		lScratch = m_lBitStorage >> ( m_nBitsRemaining - nNumBits );
		*pBitsRead = nNumBits;
		m_nBitsRemaining -= nNumBits;
    }
    return (unsigned)lScratch & m_nMask[nNumBits];
}


void CBase64::write_bits(unsigned nBits,
						 int nNumBits,
						 char *szOutput,
						 int& i)
{
	unsigned nScratch;

	m_lBitStorage = (m_lBitStorage << nNumBits) | nBits;
	m_nBitsRemaining += nNumBits;
	while( m_nBitsRemaining > 7 )
	{
		nScratch = m_lBitStorage >> (m_nBitsRemaining - 8);
		szOutput[ i++ ] = nScratch & 0xFF;
		m_nBitsRemaining -= 8;
	}
}

