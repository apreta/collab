/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"
#include "meetingmainframe.h"
#include "mainpanel.h"
#include "participantpanel.h"
#include "pcphonepanel.h"
#include "chatpanel.h"
#include "appsharepanel.h"
#include "meetingcentercontroller.h"
#include "sharecontrol.h"
#include "pcphonecontroller.h"

//(*InternalHeaders(MeetingMainFrame)
#include <wx/xrc/xmlres.h>
//*)

//(*InternalHeaders(MeetingMainFrame)
//*)

//#include "meetingsetupdialog.h"
#include "aboutdialog.h"

int idMenuQuit = wxNewId();


static int kMinWidth = 20;


//(*IdInit(MeetingMainFrame)
//*)

IMPLEMENT_CLASS(MeetingMainFrame, wxFrame)

BEGIN_EVENT_TABLE(MeetingMainFrame,wxFrame)
	//(*EventTable(MeetingMainFrame)
	//*)
    EVT_MENU(idMenuQuit, MeetingMainFrame::OnQuit)
    EVT_MENU(XRCID("IDC_DLG_MTG_MENU_CONTENTS"), MeetingMainFrame::OnContents)
    EVT_MENU(XRCID("IDC_DLG_MTG_MENU_ABOUT"), MeetingMainFrame::OnAbout)
    EVT_MENU(XRCID("IDC_DLG_MTG_MENU_EXITMEETING"), MeetingMainFrame::OnQuit)
    EVT_MENU(XRCID("IDC_DLG_MTG_MENU_RESTOREVIEW"), MeetingMainFrame::OnRestoreView)
END_EVENT_TABLE()


//*****************************************************************************
MeetingMainFrame::MeetingMainFrame(wxWindow* parent,wxWindowID id)
{
    m_fCloseAppOnMeetingEnd = false;
    m_fMeetingEnded = false;
    m_fChatFocused = false;
    
    //(*Initialize(MeetingMainFrame)
    wxXmlResource::Get()->LoadObject(this,parent,_T("MeetingMainFrame"),_T("wxFrame"));
    //*)
	
    SetFont(wxGetApp().GetAppDefFont());

    // ToDo: save/load these settings
#ifdef TESTCLIENT
    int w = wxSystemSettings::GetMetric(wxSYS_SCREEN_X) * 50 /100;
    int h = wxSystemSettings::GetMetric(wxSYS_SCREEN_Y) * 50 /100;
#elif defined(__WXMAC__)
    int w = wxSystemSettings::GetMetric(wxSYS_SCREEN_X) * 90 /100;
    int h = wxSystemSettings::GetMetric(wxSYS_SCREEN_Y) * 85 /100;	
#else
    int w = wxSystemSettings::GetMetric(wxSYS_SCREEN_X) * 90 /100;
    int h = wxSystemSettings::GetMetric(wxSYS_SCREEN_Y) * 90 /100;
#endif
    SetSize(w, h);

    //  Load the menubar from the XRC file and attach it
    wxMenuBar   *pSrcBar;
    pSrcBar = wxXmlResource::Get()->LoadMenuBar( NULL, _T("MeetingMenuBar"));

    SetMenuBar( pSrcBar );

    if ( CONTROLLER.IsVOIPAvailable() )
    {
        m_pPCPhoneController = new PCPhoneController( this );

        bool fUsePCPhonePanel = true;
        if( fUsePCPhonePanel )
        {
            m_pPCPhonePanel = new PCPhonePanel( this, -1 );
        }else
        {
            m_pPCPhonePanel = 0;
        }
        m_pPCPhoneController->ConnectPCPhonePanel();
        wxLogDebug(wxT("PCPhoneController - Created"));
    }
    else
    {
        wxLogDebug(wxT("PCPhoneController - No controller created. CONTROLLER.IsVoipAvailable() returned false"));
        m_pPCPhoneController = 0;
        m_pPCPhonePanel = 0;
    }

    // create a status bar
    m_pStatusBar = CreateStatusBar(2);

    m_auiMgr = new wxAuiManager(this);

    m_ShareControl = new ShareControl( this );
    m_pMainPanel = new MainPanel( this, -1, m_ShareControl );
	m_auiMgr->AddPane(m_pMainPanel, wxAuiPaneInfo().Name(wxT("Share")).CenterPane());
    m_pPartPanel = new ParticipantPanel( this, -1, m_ShareControl );
    m_pChatPanel = new ChatPanel( this );

    m_pPartPanel->Hide( );
    m_pPartPanel->SetSize( kMMFSplitterWidth, 300 );

    if( m_pPCPhonePanel )
    {
        m_pPCPhonePanel->Hide();
        m_pPCPhonePanel->SetSize( kMMFSplitterWidth, 80  );
    }

    m_pChatPanel->Hide( );
    m_pChatPanel->SetSize( kMMFSplitterWidth, 300 );

    m_auiMgr->Update( );

    Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( MeetingMainFrame::OnClose ) );
    Connect( wxEVT_ACTIVATE, wxActivateEventHandler( MeetingMainFrame::OnActivate ) );
    CONTROLLER.Connect( wxEVT_SHARE_EVENT, wxCommandEventHandler( MeetingMainFrame::OnShareEvent ), NULL, this );

    Connect(wxID_ANY, wxEVT_COMMAND_HTML_LINK_CLICKED, wxHtmlLinkEventHandler( MeetingMainFrame::OnLinkClicked ) );

    CenterOnScreen( );
}


//*****************************************************************************
MeetingMainFrame::~MeetingMainFrame()
{
    wxLogDebug( wxT("entering MeetingMainFrame DESTRUCTOR") );
	
    if (!m_fMeetingEnded)
        EndMeeting( );

    if( m_pPCPhoneController )
    {
        delete m_pPCPhoneController;
        m_pPCPhoneController = 0;
    }

    /*if( m_fCloseAppOnMeetingEnd )
	{
        CONTROLLER.Close( );
	}*/

	CONTROLLER.Disconnect( wxEVT_SHARE_EVENT, wxCommandEventHandler( MeetingMainFrame::OnShareEvent ), NULL, this );

	// Try to dispatch comamnds, a command may have been waiting for this meeting to exit.
	CONTROLLER.ExecutePendingCommands(true);
}


//*****************************************************************************
void MeetingMainFrame::OnClose(wxCloseEvent& event)
{
    wxCommandEvent ev( wxEVT_COMMAND_MENU_SELECTED, XRCID("IDC_DLG_MTG_MENU_EXITMEETING") );
    AddPendingEvent( ev );
}

//*****************************************************************************
void MeetingMainFrame::OnActivate(wxActivateEvent& event)
{
    // Skip default handling of activate event to avoid incorrect tab switch
    // (wx was saving last active control as this window lost focus to the progress dialog,
    // then sometimes switching back to that tab when the progress dialog closed.)
    if (!event.GetActive())
    {
        m_fChatFocused = IsChatActive();
    }
    else 
    {
        if (m_fChatFocused) 
        {
            CUtils::SetFocus(m_pChatPanel->GetChatControl());
            m_fChatFocused = false;
        }
    }

#ifdef __WXMAC__
	// Well, we need this part of the default processing or Mac menus don't work.
	if (m_frameMenuBar != NULL)
	{
		m_frameMenuBar->MacInstallMenuBar();
	}
#endif

}

//*****************************************************************************
void MeetingMainFrame::OnShareEvent(wxCommandEvent& event)
{
    if (m_ShareControl)
        m_ShareControl->OnCallback(event.GetInt());
	event.Skip();
}

//*****************************************************************************
void MeetingMainFrame::PostShareEvent(int code)
{
    wxCommandEvent ev(wxEVT_SHARE_EVENT, wxID_ANY);
    ev.SetInt(code);

    // Had to dispatch through meeting controller; was not getting events on GTK
    // when adding to this window
    CONTROLLER.AddPendingEvent(ev);
}

//*****************************************************************************
void MeetingMainFrame::OnQuit(wxCommandEvent& event)
{
    Show( false );
}

//*****************************************************************************
void MeetingMainFrame::OnContents(wxCommandEvent& event)
{
    wxString help( wxGetApp().Options().GetSystem( "HelpPath", "" ),  wxConvUTF8);
    help += LOCALE->GetCanonicalName( );
    help += wxT("/");
    help += wxT("running_meetings.htm");
    wxLaunchDefaultBrowser( help );
}

//*****************************************************************************
void MeetingMainFrame::OnAbout(wxCommandEvent& event)
{
    AboutDialog about(this);
	about.ShowModal();
}

//*****************************************************************************
void MeetingMainFrame::OnLinkClicked( wxHtmlLinkEvent& event )
{
    wxString eventid = event.GetLinkInfo().GetHref();
    wxCommandEvent ev(wxEVT_COMMAND_MENU_SELECTED, wxXmlResource::GetXRCID(eventid));
    AddPendingEvent(ev);
}


//*****************************************************************************
void MeetingMainFrame::EndMeeting( )
{
    if (!PREFERENCES.m_fUseDirectX)
        CUtils::RestoreDWM();

    session_send_presence(CONTROLLER.session, NULL, PRES_AVAILABLE, "iic:");

    if (m_pPCPhoneController)
        m_pPCPhoneController->HangupCall();

    m_pChatPanel->EndMeeting( );
    m_pPartPanel->EndMeeting( );
    m_pPCPhonePanel->EndMeeting( );
    m_pMainPanel->EndMeeting( );

    m_ShareControl->ShareEnd( );

    m_fMeetingEnded = true;
    
    CUtils::CloseDialogs( this );

    if( m_fCloseAppOnMeetingEnd )
	{
        CONTROLLER.Close( );
	}
	else
	{
		Destroy( );
	}
}


//*****************************************************************************
void MeetingMainFrame::StartMeeting( const wxString &strParticipantID, const wxString &strParticipantName, const wxString &strMeetingID, const wxString &strMeetingTitle, const wxString &strPassword, bool fJoining )
{
    if (!PREFERENCES.m_fUseDirectX)
        CUtils::DisableDWM();
    
    session_send_presence(CONTROLLER.session, NULL, PRES_AVAILABLE, "iic:meeting");

    wxLogDebug(wxT("MeetingMainFrame::StartMeeting"));

    m_strParticipantID = strParticipantID;
    m_strParticipantName = strParticipantName;
    m_strMeetingID = strMeetingID;
    m_strMeetingTitle = strMeetingTitle;
    m_strPassword = strPassword;

    m_strParticipantID.StartsWith( wxT("part:"), &m_strParticipantID);

#ifdef TESTCLIENT
    wxString    strT;
    strT = wxString::Format( wxT("%s [%s]"), m_strMeetingTitle.c_str( ), strParticipantName.c_str( ) );
    SetTitle( strT );
#else
    wxString    strT;
    wxString dash(wxT(" - "));
    wxString id(_("ID: "));
    strT = m_strMeetingTitle  + dash + id + strMeetingID;
    SetTitle( strT );
#endif

    m_pMainPanel->StartMeeting( m_strParticipantID, strParticipantName, strMeetingID, strMeetingTitle, fJoining );
    m_pPartPanel->StartMeeting( m_strParticipantID, strParticipantName, strMeetingID, strMeetingTitle, strPassword, fJoining );

    if( m_pPCPhonePanel )
    {
        m_pPCPhonePanel->StartMeeting( m_strParticipantID, strParticipantName, strMeetingID, strMeetingTitle, strPassword, fJoining );
    }

    m_pChatPanel->StartMeeting( m_strParticipantID, strParticipantName, strMeetingID, strMeetingTitle, fJoining );
}

//*****************************************************************************
// We optionally make window transparent so that is it not visible during a desktop share
void MeetingMainFrame::DoSetTransparent( bool flag )
{
    int     iAlpha  = 255;
    if ( flag )
    {
        if( PREFERENCES.m_fHidePresenterMeetingWindow )
            iAlpha = 254;
    }
    bool    fCan;
    fCan = CanSetTransparent( );
    if( fCan )
    {
        SetTransparent( iAlpha );
    }
}

//*****************************************************************************
void MeetingMainFrame::SetParticipantID( const wxString &strParticipantID )
{
    wxLogDebug(wxT("MeetingMainFrame::SetParticipantID"));

    m_strParticipantID = strParticipantID;

    m_pMainPanel->SetParticipantID( strParticipantID );
    if( m_pPCPhonePanel )
    {
        m_pPCPhonePanel->SetParticipantID( strParticipantID );
    }
    m_pChatPanel->SetParticipantID( strParticipantID );
}


//*****************************************************************************
void MeetingMainFrame::ShowChatAndPart( bool fShow )
{
    wxLogDebug( wxT("entering MeetingMainFrame::ShowChatAndPart( %d )"), fShow );

    wxAuiPaneInfo paneInfo;

    if( fShow  &&  !m_auiMgr->GetPane( m_pChatPanel ).IsOk( ) )
    {
        // The side panels show up in GTK in reversed order, so we reverse the order of creation
        // so it matches windows
#if defined(__WXGTK__) || defined(__WXMAC__)
        paneInfo = wxAuiPaneInfo().Name(wxT("chatpanel"))
            .CloseButton( false )
            .MinimizeButton( true )
            .Caption(_("Chat"))
            .Left();
        paneInfo.dock_proportion = 800;
        m_auiMgr->AddPane(m_pChatPanel, paneInfo);

        if( m_pPCPhonePanel )
        {
            paneInfo = wxAuiPaneInfo().Name(wxT("pcphone"))
                       .CloseButton( false )
                       .MinimizeButton( true )
                       .Caption( _("Web Phone") )
            .Left();
            paneInfo.dock_proportion = 650;
            m_auiMgr->AddPane(m_pPCPhonePanel, paneInfo );
        }

        paneInfo = wxAuiPaneInfo().Name(wxT("partPanel"))
                   .CloseButton( false )
                   .MinimizeButton( true )
                   .Caption(_("Invitees"))
                   .Left();
        paneInfo.dock_proportion = 1000;
        m_auiMgr->AddPane(m_pPartPanel, paneInfo );
#else
        paneInfo = wxAuiPaneInfo().Name(wxT("partPanel"))
                   .CloseButton( false )
                   .MinimizeButton( true )
                   .Caption(_("Invitees"))
                   .Left();
        paneInfo.dock_proportion = 1000;
        m_auiMgr->AddPane(m_pPartPanel, paneInfo );

        if( m_pPCPhonePanel )
        {
            paneInfo = wxAuiPaneInfo().Name(wxT("pcphone"))
                       .CloseButton( false )
                       .MinimizeButton( true )
                       .Caption( _("Web Phone") )
                       .Left();
            paneInfo.dock_proportion = 550;
            m_auiMgr->AddPane(m_pPCPhonePanel, paneInfo );
        }

        paneInfo = wxAuiPaneInfo().Name(wxT("chatpanel"))
                   .CloseButton( false )
                   .MinimizeButton( true )
                   .Caption(_("Chat"))
                   .Left();
        paneInfo.dock_proportion = 800;
        m_auiMgr->AddPane(m_pChatPanel, paneInfo);
#endif
    }

    paneInfo = m_auiMgr->GetPane( m_pChatPanel );
    if( !paneInfo.IsOk( ) )
        wxLogDebug( wxT("  auiMgr->GetPane( Chat ) FAILED") );
    paneInfo.Show( fShow );

    if( m_pPCPhonePanel )
    {
        paneInfo = m_auiMgr->GetPane( m_pPCPhonePanel );
        if( !paneInfo.IsOk( ) )
            wxLogDebug( wxT("  auiMgr->GetPane( PC phone ) FAILED") );
        paneInfo.Show( fShow );
    }

    paneInfo = m_auiMgr->GetPane( m_pPartPanel );
    if( !paneInfo.IsOk( ) )
        wxLogDebug( wxT("  auiMgr->GetPane( Part ) FAILED") );

    paneInfo.Show( fShow );

    m_auiMgr->Update();

    if ( fShow && m_strInitialLayout.IsEmpty() ) 
    {
        m_strInitialLayout = m_auiMgr->SavePerspective( );
        wxLogDebug( wxT("Initial layout: %s"), m_strInitialLayout.c_str() );
    }
}


//*****************************************************************************
//  Minimize in this case means "make them really narrow but leave enough width
//  for the user to grab the splitter bar and open them up manually".
//*****************************************************************************
void MeetingMainFrame::MinimizeChatAndPart( bool fShow )
{
    wxLogDebug( wxT("entering MeetingMainFrame::MinimizeChatAndPart( %d )"), fShow );

    wxString    strT;
    int         iMPw, iMPh, iThisw, iThish;

    m_pMainPanel->GetSize( &iMPw, &iMPh );
    this->GetSize( &iThisw, &iThish );

    int     iLoc;
    if( ((iThisw - iMPw)  >  kMinWidth)  &&  !fShow )
    {
        strT = m_auiMgr->SavePerspective( );
        iLoc = strT.Find( wxT("dock_size(4,0,0)=") );
        if( iLoc  !=  wxNOT_FOUND )
        {
            strT.Truncate( iLoc + 17 );
            strT.Append( wxT("25|") );
            m_auiMgr->LoadPerspective( strT, true );
        }
    }
    else if( ((iThisw - iMPw - 32)  <  kMinWidth)  &&  fShow )
    {
        strT = m_auiMgr->SavePerspective( );
        iLoc = strT.Find( wxT("dock_size(4,0,0)=") );
        if( iLoc  !=  wxNOT_FOUND )
        {
            strT.Truncate( iLoc + 17 );
            strT.Append( wxT("300|") );
            m_auiMgr->LoadPerspective( strT, true );
        }
    }

    m_auiMgr->Update();
}

//*****************************************************************************
void MeetingMainFrame::OnRestoreView(wxCommandEvent& event)
{
    if ( !m_strInitialLayout.IsEmpty() )
        m_auiMgr->LoadPerspective( m_strInitialLayout, true );

    int w,h;
    int init_w = wxSystemSettings::GetMetric(wxSYS_SCREEN_X) * 90 /100;
    int init_h = wxSystemSettings::GetMetric(wxSYS_SCREEN_Y) * 90 /100;

    GetSize(&w, &h);
    if (w < init_w) w = init_w;
    if (h < init_h) h = init_h;
    SetSize(w, h);
}

//*****************************************************************************
void MeetingMainFrame::ConnectionLost( )
{
    wxLogDebug( wxT("entering MeetingMainFrame::ConnectionLost( )") );
    m_pStatusBar->SetStatusText( _("Connection lost..."));

    m_pChatPanel->ConnectionLost( );
    m_pPartPanel->ConnectionLost( );
    m_pMainPanel->ConnectionLost( );
}


//*****************************************************************************
void MeetingMainFrame::ConnectionReconnected( )
{
    wxLogDebug( wxT("entering MeetingMainFrame::ConnectionReconnected( )") );
    m_pStatusBar->SetStatusText( _("Connection reconnected..."));

    m_pChatPanel->ConnectionReconnected( );
    m_pPartPanel->ConnectionReconnected( );
    m_pMainPanel->ConnectionReconnected( );
}


//*****************************************************************************
bool MeetingMainFrame::IsChatActive()
{
    return FindFocus() == m_pChatPanel->GetChatControl();
}
