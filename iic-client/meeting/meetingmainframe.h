/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef MEETINGMAINFRAME_H
#define MEETINGMAINFRAME_H

//(*Headers(MeetingMainFrame)
#include <wx/frame.h>
//*)
#include <wx/aui/aui.h>
#include <wx/menu.h>
#include <wx/layout.h>
#include <wx/msgdlg.h>

//#include "sharecontrol.h"

class ChatPanel;
class ParticipantPanel;
class MainPanel;
class ShareControl;
class PCPhonePanel;
class PCPhoneController;


const static int kMMFSplitterWidth      = 325;  //  Width of Name col in PartPanel + 5 for frame widths


class MeetingMainFrame: public wxFrame
{
public:

    MeetingMainFrame(wxWindow* parent,wxWindowID id = -1);
    virtual ~MeetingMainFrame();

    //(*Identifiers(MeetingMainFrame)
    //*)

public:
    void        StartMeeting( const wxString &strParticipantID, const wxString &strParticipantName, const wxString &strMeetingID, const wxString &strMeetingTitle, const wxString &strPassword, bool fJoining = false );
    void        DoSetTransparent( bool flag );
    void        SetParticipantID( const wxString &strParticipantID );
    void        EndMeeting( );
    void        ConnectionLost( );
    void        ConnectionReconnected( );

    ParticipantPanel*   GetParticipantPanel( ) const { return m_pPartPanel; }
    ShareControl*       GetShareControl( )     const { return m_ShareControl; }

    MainPanel*  GetMainPanel( )  { return m_pMainPanel; }
    void        ShowChatAndPart( bool fShow = true );
    void        MinimizeChatAndPart( bool fShow = true );
    void        CloseAppOnMeetingEnd( )     { m_fCloseAppOnMeetingEnd = true; }
    void        RestoreLayout( );

    const wxString& GetMeetingID( ) { return m_strMeetingID; }
    const wxString& GetParticipantID() { return m_strParticipantID; }

    PCPhoneController *PhoneController() { return m_pPCPhoneController; }
    PCPhonePanel *PhonePanel() { return m_pPCPhonePanel; }

    void PostShareEvent(int code);

    bool IsChatActive();
    
protected:
    //(*Handlers(MeetingMainFrame)
    //*)
    void OnClose(wxCloseEvent& event);
    void OnActivate(wxActivateEvent& event);
    void OnShareEvent(wxCommandEvent& event);
    void OnQuit(wxCommandEvent& event);
    void OnContents(wxCommandEvent& event);
    void OnAbout(wxCommandEvent& event);
    void OnLinkClicked(wxHtmlLinkEvent& event);
    void OnRestoreView(wxCommandEvent& event);
    
    //(*Declarations(MeetingMainFrame)
    //*)

private:
    MainPanel               *m_pMainPanel;
    ParticipantPanel        *m_pPartPanel;
    PCPhonePanel            *m_pPCPhonePanel;
    ChatPanel               *m_pChatPanel;
    wxStatusBar             *m_pStatusBar;
    wxAuiManager            *m_auiMgr;
    PCPhoneController       *m_pPCPhoneController;

    // When we support multiple meetings, each meeting will have it's own share controller
    ShareControl            *m_ShareControl;

    wxString    m_strParticipantID;
    wxString    m_strParticipantName;
    wxString    m_strMeetingID;
    wxString    m_strMeetingTitle;
    wxString    m_strPassword;
    bool        m_fCloseAppOnMeetingEnd;
    bool        m_fMeetingEnded;
    bool        m_fChatFocused;
    wxString    m_strInitialLayout;

    DECLARE_CLASS(MeetingMainFrame)
    DECLARE_EVENT_TABLE()
};

#endif
