/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"
#include "meetingcentercontroller.h"
#include "recordingdialog.h"

//(*InternalHeaders(RecordingDialog)
#include <wx/xrc/xmlres.h>
//*)

#include "addressbk.h"

#include <vector>

//(*IdInit(RecordingDialog)
//*)


static int sImageCollapsed		= 1;
static int sImageExpanded		= 2;
static int sImageMP3	        = 3; 
static int sImageChat	        = 4; 
static int sImageFlash   		= 5;
static int sImageSummary   		= 6;


BEGIN_EVENT_TABLE(RecordingDialog,wxDialog)
	//(*EventTable(RecordingDialog)
	//*)
END_EVENT_TABLE()

RecordingDialog::RecordingDialog( wxWindow* parent, wxString strMtgID )
    : m_strMtgID( strMtgID )
{
	//(*Initialize(RecordingDialog)
	wxXmlResource::Get()->LoadObject(this,parent,_T("RecordingDialog"),_T("wxDialog"));
	m_textRecsFor = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT1"));
	m_tree = (wxTreeListCtrl*)FindWindow(XRCID("ID_TREELISTCTRL1"));
	m_btnView = (wxButton*)FindWindow(XRCID("ID_BTN_VIEW"));
	m_btnDelete = (wxButton*)FindWindow(XRCID("ID_BTN_DELETE"));
	m_btnRefresh = (wxButton*)FindWindow(XRCID("ID_BTN_REFRESH"));
	m_btnCancel = (wxButton*)FindWindow(XRCID("ID_BTN_CANCEL"));
	
	Connect(XRCID("ID_BTN_VIEW"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&RecordingDialog::OnBtnViewClick);
	Connect(XRCID("ID_BTN_DELETE"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&RecordingDialog::OnBtnDeleteClick);
	Connect(XRCID("ID_BTN_REFRESH"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&RecordingDialog::OnBtnRefreshClick);
	Connect(XRCID("ID_BTN_CANCEL"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&RecordingDialog::OnBtnCancelClick);
	//*)

    Connect(XRCID("ID_TREELISTCTRL1"),wxEVT_COMMAND_TREE_ITEM_ACTIVATED,(wxObjectEventFunction)&RecordingDialog::OnRecordingListItemActivated);
    Connect(XRCID("ID_TREELISTCTRL1"),wxEVT_COMMAND_TREE_ITEM_COLLAPSED,(wxObjectEventFunction)&RecordingDialog::OnRecordingListItemCollapsed);
    Connect(XRCID("ID_TREELISTCTRL1"),wxEVT_COMMAND_TREE_ITEM_EXPANDED,(wxObjectEventFunction)&RecordingDialog::OnRecordingListItemExpanded);
    Connect(XRCID("ID_TREELISTCTRL1"),wxEVT_COMMAND_TREE_ITEM_RIGHT_CLICK,(wxObjectEventFunction)&RecordingDialog::OnRecordingListItemRightClick);
    Connect(XRCID("ID_TREELISTCTRL1"),wxEVT_COMMAND_TREE_SEL_CHANGED,(wxObjectEventFunction)&RecordingDialog::OnRecordingListSelectionChanged);
    Connect(XRCID("ID_TREELISTCTRL1"),wxEVT_COMMAND_TREE_KEY_DOWN,(wxObjectEventFunction)&RecordingDialog::OnRecordingListKeyDown);
    Connect(XRCID("ID_TREELISTCTRL1"),wxEVT_COMMAND_TREE_ITEM_MENU,(wxObjectEventFunction)&RecordingDialog::OnRecordingListItemMenu);
    Connect(XRCID("ID_TREELISTCTRL1"),wxEVT_COMMAND_LIST_COL_CLICK,(wxObjectEventFunction)&RecordingDialog::OnRecordingListColClick);

    CONTROLLER.Connect(wxID_ANY, wxEVT_RECORDINGS_FETCHED, wxCommandEventHandler( RecordingDialog::OnRecordingsFetched), 0, this );

    int iRet;
    iRet = addressbk_get_recording_list( CONTROLLER.AddressBook( ), "recordings", m_strMtgID.mb_str( wxConvUTF8 ) );
    if( iRet  !=  0 )
    {
        wxLogDebug( wxT("error returned from addressbk_get_recording_list( ) -- %d"), iRet );
        return;
    }

    // Create the image lists for the tree control
    m_imageList.Create( 21, 21, true, 7 );

    wxColour    colourMask( 255, 255, 255 );
    wxString    strFN( wxGetApp().GetResourceFile(wxT("res/bitmap_r.bmp")) );
    wxBitmap    bmpU( strFN, wxBITMAP_TYPE_BMP );
    iRet = m_imageList.Add( bmpU, colourMask );
    wxLogDebug( wxT("in RecordDialog CONSTRUCTOR - bmpU.IsOk( ) returned %d  --  m_imageList.Add( ) returned %d"), bmpU.IsOk( ), iRet  );

    m_tree->AddColumn( wxT(""), 250 );
    m_tree->SetImageList( &m_imageList );
    m_tree->SetIndent( 26 );

    m_tree->SetSortColumn( 0 );
    m_tree->SetSortAscending( 0, false );

    wxString    strT( _("Recordings for meeting: "), wxConvUTF8 );
    strT += m_strMtgID;
    m_textRecsFor->SetLabel( strT );

}


//*****************************************************************************
RecordingDialog::~RecordingDialog()
{
	//(*Destroy(RecordingDialog)
    //*)

    CONTROLLER.Disconnect(wxID_ANY, wxEVT_RECORDINGS_FETCHED, wxCommandEventHandler( RecordingDialog::OnRecordingsFetched), 0, this );

}


//*****************************************************************************
void RecordingDialog::ParseFolderName( const std::string &aFolder, wxString &aDate )
{
    int meetingID;
    int year;
    int month;
    int day;
    int hour;
    int minute;
    int second;

    if( sscanf( aFolder.c_str( ), "%d-%4d-%2d-%2d-%2d-%2d-%2d", &meetingID, &year, &month, &day, &hour, &minute, &second) == 7 )
    {
        aDate = wxString::Format( _T("%02d/%02d/%04d %02d:%02d:%02d"), month, day, year, hour, minute, second);
    }
    else
    {
        aDate.Empty( );
        aDate.Append( wxString( aFolder.c_str( ), wxConvUTF8 ) );
    }
}


//*****************************************************************************
void RecordingDialog::FreeANode( wxTreeItemId aItem)
{
    if( aItem  &&  aItem.IsOk( ) )
    {
        wxTreeItemData  *link = m_tree->GetItemData( aItem );
        delete link;
        m_tree->SetItemData( aItem, NULL );

        if( m_tree->HasChildren( aItem ) )
        {
            wxTreeItemIdValue   cookie;
            wxTreeItemId        idChild     = m_tree->GetFirstChild( aItem, cookie );
            while( idChild  &&  idChild.IsOk( ) )
            {
                FreeANode( idChild );
                idChild = m_tree->GetNextChild( idChild, cookie );
            }
        }
    }
}


//*****************************************************************************
void RecordingDialog::Cleanup( )
{
    m_tree->DeleteRoot( );

}


//*****************************************************************************
//
//  Event handlers start here
//
//*****************************************************************************
void RecordingDialog::OnBtnViewClick(wxCommandEvent& event)
{
    wxTreeItemId    selectedItem    = m_tree->GetSelection( );
    if( selectedItem.IsOk( ) )
    {
        RecordingTreeItemData   *pRTID  = (RecordingTreeItemData *) m_tree->GetItemData( selectedItem );
        if( pRTID )
            wxLaunchDefaultBrowser( pRTID->m_str );
    }
}


//*****************************************************************************
void RecordingDialog::OnBtnDeleteClick(wxCommandEvent& event)
{
    wxTreeItemId    selectedItem    = m_tree->GetSelection( );
    if( selectedItem.IsOk( ) )
    {
        RecordingTreeItemData   *pRTID  = (RecordingTreeItemData *) m_tree->GetItemData( selectedItem );
        if( pRTID )
        {
            wxString    strT( pRTID->m_str );
            if( addressbk_recording_remove( CONTROLLER.AddressBook( ), "rec_remove", m_strMtgID.mb_str( wxConvUTF8 ), strT.mb_str( wxConvUTF8 ) )  ==  0 )
            {
                OnBtnRefreshClick( event );
//                m_tree->DeleteChildren( selectedItem );
//                m_tree->Delete( selectedItem );
            }
        }
    }
}


//*****************************************************************************
void RecordingDialog::OnBtnRefreshClick(wxCommandEvent& event)
{
    int iRet;
    iRet = addressbk_get_recording_list( CONTROLLER.AddressBook( ), "recordings", m_strMtgID.mb_str( wxConvUTF8 ) );
    if( iRet  !=  0 )
    {
        wxLogDebug( wxT("error returned from addressbk_get_recording_list( ) -- %d"), iRet );
    }
}


//*****************************************************************************
void RecordingDialog::OnBtnCancelClick(wxCommandEvent& event)
{
    Cleanup( );

    EndModal( wxID_CANCEL );
}


//*****************************************************************************
void RecordingDialog::OnRecordingListItemActivated(wxTreeEvent& event)
{
    wxTreeItemId    selectedItem    = event.GetItem( );
    if( selectedItem.IsOk( ) )
    {
        RecordingTreeItemData   *pRTID  = (RecordingTreeItemData *) m_tree->GetItemData( selectedItem );
        if( pRTID )
            wxLaunchDefaultBrowser( pRTID->m_str );
    }
}


//*****************************************************************************
void RecordingDialog::OnRecordingListItemCollapsed(wxTreeEvent& event)
{
    m_tree->SetItemImage( event.GetItem( ), sImageCollapsed );
}


//*****************************************************************************
void RecordingDialog::OnRecordingListItemExpanded(wxTreeEvent& event)
{
    m_tree->SetItemImage( event.GetItem( ), sImageExpanded );
}


//*****************************************************************************
void RecordingDialog::OnRecordingListItemRightClick(wxTreeEvent& event)
{
}


//*****************************************************************************
void RecordingDialog::OnRecordingListSelectionChanged(wxTreeEvent& event)
{
    bool    fCanView = !m_tree->HasChildren( event.GetItem( ) );
    m_btnView->Enable( fCanView );
    m_btnDelete->Enable( !fCanView );
}


//*****************************************************************************
void RecordingDialog::OnRecordingListKeyDown(wxTreeEvent& event)
{
}


//*****************************************************************************
void RecordingDialog::OnRecordingListItemMenu(wxTreeEvent& event)
{
}


//*****************************************************************************
void RecordingDialog::OnRecordingListColClick( wxListEvent& event )
{
    wxTreeItemId        root    = m_tree->GetRootItem( );

    m_tree->ReverseSortOrder( );
    m_tree->SortChildren( root );

    wxTreeItemIdValue   cookie;
    for( wxTreeItemId child = m_tree->GetFirstChild( root, cookie );
         child.IsOk();
         child = m_tree->GetNextChild( root, cookie ) )
    {
        m_tree->SortChildren( child );
    }
}


//*****************************************************************************
void RecordingDialog::OnRecordingsFetched( wxCommandEvent& event )
{
    //  Delete everything in the tree and start over
    Cleanup( );
    wxTreeItemId    root = m_tree->AddRoot( _T("root") );

    recording_info_t    recInfo;
    recording_list_t    recList;
    recList = (recording_list_t) event.GetClientData( );
    wxString            strT;
    strT = event.GetString( );

    int     i;
    int     iLen;
    iLen = recording_list_get_size( recList );
    for( i=0; i<iLen; i++ )
    {
        recInfo = recording_list_get_recording( recList, i );
        AddRecToTree( recInfo );
    }

    m_tree->ExpandAll( root );

    m_tree->SetColumnImage( 0, 0 );
    m_tree->SortChildren( root );                       //Descending

    wxTreeItemIdValue   cookie;
    wxTreeItemId        parent;
    parent = m_tree->GetFirstChild( root, cookie );
    if( parent.IsOk( ) )
    {
        m_tree->SelectItem( parent );
        bool    fCanView = !m_tree->HasChildren( parent );
        m_btnView->Enable( fCanView );
        m_btnDelete->Enable( !fCanView );
    }

    recording_list_destroy( recList );
}



//*****************************************************************************
void RecordingDialog::AddRecToTree( recording_info_t recInfo )
{
    RecordingTreeItemData   *pRTID;
    wxTreeItemId            root        = m_tree->GetRootItem( );
    wxTreeItemId            parent;
    wxTreeItemId            newNode;

    wxString    cstrT;
    wxString    strDate( recInfo->time_string, wxConvUTF8 );
    wxString    strT( recInfo->folder_file, wxConvUTF8 );
    pRTID = new RecordingTreeItemData( strT );
    parent = m_tree->AppendItem( root, strDate, sImageExpanded, -1, pRTID );

    if( recInfo->chat_file )
    {
        wxString    strT( recInfo->chat_file, wxConvUTF8 );
        pRTID = new RecordingTreeItemData( strT );
        cstrT = _("Chat Transcript");
        newNode = m_tree->AppendItem( parent, cstrT, sImageChat, -1, (wxTreeItemData *) pRTID );
    }

    if( recInfo->data_file )
    {
        wxString    strT( recInfo->data_file, wxConvUTF8 );
        pRTID = new RecordingTreeItemData( strT );
        cstrT = _("Data Recording");
        newNode = m_tree->AppendItem( parent, cstrT, sImageFlash, -1, (wxTreeItemData *) pRTID );
    }

    if( recInfo->summary_file )
    {
        wxString    strT( recInfo->summary_file, wxConvUTF8 );
        pRTID = new RecordingTreeItemData( strT );
        cstrT = _("Summary");
        newNode = m_tree->AppendItem( parent, cstrT, sImageSummary, -1, (wxTreeItemData *) pRTID );
    }

    if( recInfo->voice_file )
    {
        wxString    strT( recInfo->voice_file, wxConvUTF8 );
        pRTID = new RecordingTreeItemData( strT );
        cstrT = _("Voice Recording");
        newNode = m_tree->AppendItem( parent, cstrT, sImageMP3, -1, (wxTreeItemData *) pRTID );
    }

}
