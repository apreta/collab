/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef DOCEXPLORERDIALOG_H
#define DOCEXPLORERDIALOG_H

//(*Headers(DocExplorerDialog)
#include <wx/listctrl.h>
#include <wx/stattext.h>
#include <wx/button.h>
#include <wx/dialog.h>
//*)


class CIICDoc;


class DocExplorerDialog: public wxDialog
{
	public:

		DocExplorerDialog( wxWindow* parent, CIICDoc *pDocument, wxString strMtgID, wxString strDocID, bool fCloseOnUpload );
		virtual ~DocExplorerDialog();

		//(*Identifiers(DocExplorerDialog)
		//*)

    public:
        wxString        m_strDocID;
        bool            m_fErrorInitializing;

	protected:

		//(*Handlers(DocExplorerDialog)
		void OnBtnOKClick(wxCommandEvent& event);
		void OnBtnCancelClick(wxCommandEvent& event);
		void OnBtnUploadClick(wxCommandEvent& event);
		void OnBtnDeleteClick(wxCommandEvent& event);
		void OnItemSelected(wxListEvent& event);
		//*)

		//(*Declarations(DocExplorerDialog)
		wxButton* m_btnUpload;
		wxButton* m_btnDelete;
		wxListCtrl* m_list;
		wxButton* m_btnOK;
		wxButton* m_btnCancel;
		wxStaticText* m_textRecsFor;
		//*)

    private:
        void            RefillList( );
        void            Cleanup( );
        int             RetrieveDAVInfo( const wxString& aUrl, const wxString& aUrlPath, wxString& outXML, wxString& statusCode, wxString& strStatusText );
        int             ParseResponse( const char *aXml );
        void            RefreshList( );

    private:
        CIICDoc         *m_pDocument;
        wxString        m_strMtgID;
        bool            m_fInMeeting;

		DECLARE_EVENT_TABLE()
};

#endif
