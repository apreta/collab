/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
// Line.cpp : Defines the class behaviors for CDashLine
//
//	The Bresenham function in this file is derived from code from 
//			Jean-Claude Lanz mailto:Jclanz@bluewin.ch
//		and he presumably shares copyright to it
//	Otherwise the copyright belongs to Llew S. Goodstadt 
//		http://www.lg.ndirect.co.uk    mailto:lg@ndirect.co.uk
//		who hereby grants you fair use and distribution rights of the code 
//		in both commercial and non-commercial applications.

#include "wx_pch.h"
#include "dashline.h"
#include "bezier.h"


/////////////////////////////////////////////////////////////////////////////
// helper

/////////////////////////////////////////////////////////////////////////////
// CDashLine

CDashLine::CDashLine( wxDC& dc, unsigned* pattern, unsigned count)
	:m_DC(dc), m_CurPat(0), m_Count(0), m_Pattern(0)
{
	SetPattern(pattern, count);
}

CDashLine::~CDashLine()
{
	// destroy GDI objects
	delete[] m_Pattern;
}

void CDashLine::SetPattern(unsigned* pattern, unsigned count)
{
	// Must be an even number of dash and gaps
    wxASSERT(count >=0);
    wxASSERT(!(count % 2));

	m_Count = count;
	delete[] m_Pattern;
	if (m_Count)
	{
		m_Pattern = new UINT[count];
		memcpy(m_Pattern, pattern, count * sizeof(UINT));
	}	
	else
		m_Pattern = 0;
	Reset();
}

void CDashLine::Reset()
{
	m_CurPat = 0;
	if (m_Count)
		m_CurStretch = m_Pattern[0];
}

// use linear interpolation to compute next position
void CDashLine::Bresenham( long x, long y, POINTLIST& pathPoints, BYTELIST& pathTypes )
{

	// Setup Bresenham
	long dx = x - m_CurPos.x;
	long dy = y - m_CurPos.y;

	long *p1, *p2, *pd1, *pd2;
	if (abs(dx) >= abs(dy))
	{
		p1 = (long *) &m_CurPos.x; 
		p2 = (long *) &m_CurPos.y; 
		pd1 = &dx; 
		pd2 = &dy; 
	}
	else
	{
		p1 = (long *) &m_CurPos.y; 
		p2 = (long *) &m_CurPos.x; 
		pd1 = &dy; 
		pd2 = &dx;
	}

	int max = abs(*pd1);
	int dec = abs(*pd2);
	int s1 = (*pd1 >= 0) ? 1: -1;
	int s2 = (*pd2 >= 0) ? 1: -1;
	int val = max;

	// count past correct number of pixels in current segment
	// 		or until end of this line
	for (int i = 0; i < max; i++)
	{
		val -= dec;
		if (val <= 0) 
		{	
			*p2 += s2; 
			val += max; 
		}
		*p1 += s1;

		--m_CurStretch;
		if (!m_CurStretch)
		{
			// use next pattern for next segment
			m_CurPat = (m_CurPat + 1) % m_Count;
			m_CurStretch = m_Pattern[m_CurPat];

			// draw segment or skip gap
            if (m_CurPat % 2)
            {
//                m_DC.LineTo(m_CurPos);
                m_DC.DrawLine( m_PriPos.x, m_PriPos.y, m_CurPos.x, m_CurPos.y );
                pathPoints.push_back( wxPoint( m_PriPos.x, m_PriPos.y ) );
                pathTypes.push_back( (BYTE) PT_MOVETO );
                pathPoints.push_back( wxPoint( m_CurPos.x, m_CurPos.y ) );
                pathTypes.push_back( (BYTE) PT_LINETO );
            }
            else
            {
//				m_DC.MoveTo(m_CurPos);
                m_PriPos = m_CurPos;
            }
		
			// if last point, return
			if (i == max - 1) 
				return;
		}
	}

	// draw to last point if necessary
    if (!(m_CurPat % 2))
    {
//		m_DC.LineTo(m_CurPos);
        m_DC.DrawLine( m_PriPos.x, m_PriPos.y, m_CurPos.x, m_CurPos.y );
        pathPoints.push_back( wxPoint( m_PriPos.x, m_PriPos.y ) );
        pathTypes.push_back( (BYTE) PT_MOVETO );
        pathPoints.push_back( wxPoint( m_CurPos.x, m_CurPos.y ) );
        pathTypes.push_back( (BYTE) PT_LINETO );
    }
}

void CDashLine::MoveTo(int x, int y)
{
	// with MoveTo, reset parameters
	Reset();

	// save current position
	m_CurPos.x = x; 
	m_CurPos.y = y;
    m_PriPos = m_CurPos;

	// move to position
//	m_DC.MoveTo(m_CurPos);
}


void CDashLine::LineTo( int x, int y, POINTLIST& pathPoints, BYTELIST& pathTypes )
{
	// line type and color
	if (!m_Count)
	{
//		m_DC.LineTo(x, y);
        m_CurPos.x = x; m_CurPos.y = y;
        m_DC.DrawLine( m_PriPos.x, m_PriPos.y, m_CurPos.x, m_CurPos.y );
        m_PriPos = m_CurPos;
        pathPoints.push_back( wxPoint( x, y ) );
        pathTypes.push_back( (BYTE) PT_LINETO );
		return;
	}

	// calculate and draw next points
	Bresenham(x, y, pathPoints, pathTypes );
}


// use linear interpolation to compute next position
void CDashLine::BezierTo( wxPoint* dest, POINTLIST& pathPoints, BYTELIST& pathTypes )
{
	if (!m_Count)
	{
//		m_DC.PolyBezierTo(dest, 3);
        m_DC.DrawSpline( 3, dest );
		m_CurPos = dest[2];
        pathPoints.push_back( wxPoint( dest[0].x, dest[0].y ) );
        pathTypes.push_back( (BYTE) PT_LINETO );
        pathPoints.push_back( wxPoint( dest[0].x, dest[0].y ) );
        pathTypes.push_back( (BYTE) PT_MOVETO );
        pathPoints.push_back( wxPoint( dest[1].x, dest[1].y ) );
        pathTypes.push_back( (BYTE) PT_LINETO );
        pathPoints.push_back( wxPoint( dest[1].x, dest[1].y ) );
        pathTypes.push_back( (BYTE) PT_MOVETO );
        pathPoints.push_back( wxPoint( dest[2].x, dest[2].y ) );
        pathTypes.push_back( (BYTE) PT_LINETO );
        return;
	}	


	// Setup Bezier representing curve
	LBezier currentBez;
	currentBez.p[0] = m_CurPos;
	for (int i = 0; i < 3; ++i)
		currentBez.p[i+1] = dest[i];

	LBezier drawBezier;
    wxPoint Output[4];

	//for (int ii = 0; ii < 100; ++ii)
	while(1)
    {
//        wxLogDebug( wxT("another loop in BezierTo( )") );
		// split off segment corresponding to current stretch, ie drawBezier
		currentBez.TSplit(drawBezier, currentBez.TAtLength(m_CurStretch));
		drawBezier.GetCPoint(Output);

		m_CurPos = Output[3];

//        pathPoints.push_back( Output[ 0 ] );
//        pathTypes.push_back( (BYTE) PT_MOVETO );
//        pathPoints.push_back( Output[ 1 ] );
//        pathTypes.push_back( (BYTE) PT_LINETO );
//        pathPoints.push_back( Output[ 1 ] );
//        pathTypes.push_back( (BYTE) PT_MOVETO );
//        pathPoints.push_back( Output[ 2 ] );
//        pathTypes.push_back( (BYTE) PT_LINETO );
//        pathPoints.push_back( Output[ 2 ] );
//        pathTypes.push_back( (BYTE) PT_MOVETO );
//        pathPoints.push_back( Output[ 3 ] );
//        pathTypes.push_back( (BYTE) PT_LINETO );

        if (m_CurPat % 2)
        {
//            m_DC.MoveTo(m_CurPos);
            pathPoints.push_back( m_CurPos );
            pathTypes.push_back( (BYTE) PT_MOVETO );
            m_PriPos = m_CurPos;
        }
        else
        {
//            m_DC.PolyBezierTo(Output+1, 3);
            pathPoints.push_back( m_CurPos );
            pathTypes.push_back( (BYTE) PT_LINETO );
            m_DC.DrawSpline( 3, Output+1 );
        }

		// break if some part of segment did not fit on bezier
		if (!m_CurStretch)
		{
			// use next pattern for next segment
			m_CurPat = (m_CurPat + 1) % m_Count;
			m_CurStretch = m_Pattern[m_CurPat];
		}	
		else
			break;		
	}		
}


// Returns count of elements (dash/dot and gaps)
// You must be careful to pass in enough memory for pattern
// It is probably safest to always have an array of [8]
unsigned CDashLine::GetPattern(unsigned* pattern, bool round, unsigned penSize, unsigned style)
{
    wxASSERT(style <= DL_DASHDOTDOTDOT_GAP);
	int gapLen	= round	?	penSize * 2 : penSize;
	int dotLen	= round	?	1			: penSize;
	int dashLen	= round	?	penSize * 2 : penSize * 3;
	if (style >= DL_DASH_GAP)
		gapLen *= 2;
    if (style == DL_SOLID)
    {
//        pattern[0] = dashLen;
//        pattern[1] = dashLen;
        return 0;
    }
	
	switch (style)
	{
		case DL_DASH:
		case DL_DASH_GAP:
				pattern[0] = dashLen;
				pattern[1] = gapLen;
				return 2;
		case DL_DOT:
		case DL_DOT_GAP:
				pattern[0] = dotLen;
				pattern[1] = gapLen;
				return 2;
		case DL_DASHDOT:
		case DL_DASHDOT_GAP:
				pattern[0] = dashLen;
				pattern[2] = dotLen;
				pattern[1] = 
				pattern[3] = gapLen;
				return 4;
		case DL_DASHDOTDOT:
		case DL_DASHDOTDOT_GAP:
				pattern[0] = dashLen;
				pattern[2] =
				pattern[4] = dotLen;
				pattern[1] =
				pattern[3] =
				pattern[5] = gapLen;
				return 6;
		case DL_DASHDOTDOTDOT:
		case DL_DASHDOTDOTDOT_GAP:
		default:
				pattern[0] = dashLen;
				pattern[2] =
				pattern[4] = 
				pattern[6] = dotLen;
				pattern[1] =
				pattern[3] =
				pattern[5] = 
				pattern[7] = gapLen;
				return 8;
		
	}	
		
}	
