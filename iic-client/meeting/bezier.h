/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//	Copyright Llew S. Goodstadt 1998
//		http://www.lg.ndirect.co.uk    mailto:lg@ndirect.co.uk
//		you are hereby granted the right tofair use and distribution
//		including in both commercial and non-commercial applications.

//#include <afxwin.h>
#include <math.h>


class IICRect;


void EllipseToBezier( IICRect& r, wxPoint* cCtlPt );
struct LDPoint
{
	double x, y;	

	// Constructors
		LDPoint(){}
		LDPoint(double _x, double _y): x(_x), y(_y)	{}
		LDPoint(const wxPoint& p): x(p.x), y(p.y)	{}
		LDPoint(const LDPoint& p): x(p.x), y(p.y)	{}
//		LDPoint(const POINT& p): x(p.x), y(p.y)		{}

	// equality operators
		bool		operator ==(const LDPoint& other) const;
		bool		operator !=(const LDPoint& other) const;

	// Functions/binary-operators that return points or sizes
		LDPoint		OffsetBy(double dx, double dy) const;
		LDPoint		OffsetBy(const LDPoint sz) const;			//		should be LDSize
		LDPoint		operator +(const LDPoint& other) const;		//		should be LDSize
		LDPoint		operator -(const LDPoint& other) const;		//		should be LDSize * 2
		LDPoint		operator -() const;
		double		DistFrom2(const LDPoint&) const;
		double		DistFrom(const LDPoint&) const;
		LDPoint		ScaledBy(double f) const;
		LDPoint		operator*(double f) const;
        wxPoint		GetCPoint() const;


	// Functions/assignement-operators that modify this point
		LDPoint&	Offset(double dx, double dy);
		LDPoint&	Offset(const LDPoint& sz);					//		should be LDSize
		LDPoint&	operator +=(const LDPoint& other);			//		should be LDSize
		LDPoint&	operator -=(const LDPoint& other);			//		should be LDSize
		LDPoint&	Scale(double f);
		LDPoint&	operator*=(double f);
};


struct LBezier
{
	LDPoint	p[4];
	void GetCPoint(wxPoint* out);
	void TSplit(LBezier& Output, double t);
	void Split(LBezier& Left, LBezier& Right) const;
	double TAtLength(unsigned int& len) const;
	double TAtLength(double& len, double error = 0.5) const;
	double Length(double error = 0.5) const;
};

// equality operators
inline bool	LDPoint::operator ==(const LDPoint& other) const
	{	return other.x == x && other.y == y;}
inline bool LDPoint::operator !=(const LDPoint& other) const
	{	return other.x != x || other.y != y;}

	// Functions/binary-operators that return points or sizes
inline LDPoint LDPoint::OffsetBy(double dx, double dy) const
	{	return LDPoint(dx+x, dy+y);}
inline LDPoint LDPoint::OffsetBy(const LDPoint sz) const
	{	return LDPoint(sz.x + x, sz.y + y);}
inline LDPoint LDPoint::operator +(const LDPoint& sz) const
	{	return LDPoint(sz.x + x, sz.y + y);}
inline LDPoint LDPoint::operator -(const LDPoint& other) const
	{	return LDPoint(x-other.x, y-other.y);}
inline LDPoint LDPoint::operator -() const
	{	return LDPoint(-x, -y);}
inline double LDPoint::DistFrom2(const LDPoint& o) const
	{	return ((x - o.x) * (x - o.x) + (y - o.y) * (y - o.y));}
inline double LDPoint::DistFrom(const LDPoint& o) const
	{	return(sqrt(DistFrom2(o)));}
inline LDPoint LDPoint::ScaledBy(double f) const
	{return LDPoint(x * f, y * f);}
inline LDPoint LDPoint::operator*(double f) const
	{return LDPoint(x * f, y * f);}
inline wxPoint LDPoint::GetCPoint() const
	{	return wxPoint(static_cast<int>(x + 0.5), static_cast<int>(y + 0.5));}

	// Functions/assignement-operators that modify this point
inline LDPoint&	LDPoint::Offset(double dx, double dy)
	{x += dx; y += dy; return *this;}
inline LDPoint&	LDPoint::Offset(const LDPoint& sz)
	{x += sz.x; y += sz.y; return *this;}
inline LDPoint&	LDPoint::operator +=(const LDPoint& sz)
	{x += sz.x; y += sz.y; return *this;}
inline LDPoint&	LDPoint::operator -=(const LDPoint& sz)
	{x -= sz.x; y -= sz.y; return *this;}
inline LDPoint&	LDPoint::Scale(double f)
	{x *= f; y *= f; return *this;}
inline LDPoint&	LDPoint::operator*=(double f)
	{x *= f; y *= f; return *this;}
