/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"
#include "chatmainframe.h"
#include "participantpanel.h"
#include "chatpanel.h"
#include "meetingcentercontroller.h"

//(*InternalHeaders(ChatMainFrame)
#include <wx/bitmap.h>
#include <wx/font.h>
#include <wx/fontenum.h>
#include <wx/fontmap.h>
#include <wx/image.h>
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/textdlg.h>
#include <wx/xrc/xmlres.h>
//*)

//(*InternalHeaders(ChatMainFrame)
//*)

#include "aboutdialog.h"

int idCDMenuQuit = wxNewId();


//static int kMinWidth = 20;


//(*IdInit(ChatMainFrame)
//*)

IMPLEMENT_CLASS(ChatMainFrame, wxFrame)

BEGIN_EVENT_TABLE(ChatMainFrame,wxFrame)
	//(*EventTable(ChatMainFrame)
	//*)
    EVT_MENU(idCDMenuQuit, ChatMainFrame::OnQuit)
    EVT_MENU(XRCID("IDC_DLG_CHAT_CONTENTS"), ChatMainFrame::OnContents)
    EVT_MENU(XRCID("IDC_DLG_CHAT_ABOUT"), ChatMainFrame::OnAbout)
    EVT_MENU(XRCID("IDC_DLG_CHAT_CLOSE"), ChatMainFrame::OnQuit)
END_EVENT_TABLE()


//*****************************************************************************
ChatMainFrame::ChatMainFrame(wxWindow* parent, ChatMainFrame **ppThis)
{
    //(*Initialize(ChatMainFrame)
    wxXmlResource::Get()->LoadObject(this,parent,_T("ChatMainFrame"),_T("wxFrame"));
    //*)

    m_ppThis = ppThis;

    SetFont(wxGetApp().GetAppDefFont());

    wxString title;
    title = PRODNAME + _T(" - ") + _T("IM");
    this->SetTitle( title );

    // ToDo: save/load these settings
    int w = wxSystemSettings::GetMetric(wxSYS_SCREEN_X) * 35 /100;
    int h = wxSystemSettings::GetMetric(wxSYS_SCREEN_Y) * 35 /100;
    SetSize(w, h);

    //  Load the menubar from the XRC file and attach it
    wxMenuBar   *pSrcBar;
    pSrcBar = wxXmlResource::Get()->LoadMenuBar( NULL, _T("IDC_DLG_CHAT_MENUBAR"));

    SetMenuBar(pSrcBar);

    // create a status bar
    m_pStatusBar = CreateStatusBar(2);

    m_auiMgr = new wxAuiManager(this);

    m_pNotebook = CreateNotebook();

    m_auiMgr->AddPane(m_pNotebook, wxAuiPaneInfo().Name(wxT("notebook")).CenterPane());

    m_auiMgr->Update();

    Connect(wxEVT_CLOSE_WINDOW, wxCloseEventHandler( ChatMainFrame::OnClose));

    CenterOnScreen();
}


//*****************************************************************************
ChatMainFrame::~ChatMainFrame()
{
    EndChat();
}

//*****************************************************************************
void ChatMainFrame::SetDisplayName(const wxString& name)
{
    m_strDisplayName = name;
}


//*****************************************************************************
void ChatMainFrame::OnClose(wxCloseEvent& event)
{
    *m_ppThis = NULL;
    Destroy();
}

//*****************************************************************************
void ChatMainFrame::OnQuit(wxCommandEvent& event)
{
    *m_ppThis = NULL;
    Destroy();
}

//*****************************************************************************
void ChatMainFrame::OnContents(wxCommandEvent& event)
{
    wxString help( wxGetApp().Options().GetSystem( "HelpPath", "" ),  wxConvUTF8);
    help += LOCALE->GetCanonicalName( );
    help += wxT("/");
    help += wxT("running_meetings.htm");
    wxLaunchDefaultBrowser( help );
}

//*****************************************************************************
void ChatMainFrame::OnAbout(wxCommandEvent& event)
{
    AboutDialog about(this);
	about.ShowModal();
}

//*****************************************************************************
void ChatMainFrame::EndChat( )
{
    Destroy( );
}

//*****************************************************************************
void ChatMainFrame::AddChat( const wxString &name)
{
    ChatPanel *panel;
    wxString displayName(name);
    CUtils::DecodeScreenName(displayName);
    
    panel = new ChatPanel( this );
    panel->SetSize( kCDSplitterWidth, 300 );
    m_pNotebook->AddPage( panel, displayName, true );

    panel->StartIM(m_strDisplayName, name);

    int     iAlpha  = 255;
    if( PREFERENCES.m_fHidePresenterMeetingWindow )
        iAlpha = 254;
    bool    fCan;
    fCan = CanSetTransparent( );
    if( fCan )
    {
        SetTransparent( iAlpha );
    }
}

//*****************************************************************************
int ChatMainFrame::FindChat( const wxString &name, bool create)
{
    wxString displayName(name);
    CUtils::DecodeScreenName(displayName);
    
    for (int i=0; i<m_pNotebook->GetPageCount(); i++)
    {
        if (m_pNotebook->GetPageText(i) == displayName)
            return i;
    }
    if (create)
    {
        AddChat(name);
        return FindChat(name, false);
    }

    return -1;
}

//*****************************************************************************
void ChatMainFrame::StartIM(const wxString &strFrom)
{
    wxLogDebug(wxT("ChatMainFrame::StartIM"));

    int idx = FindChat(strFrom, true);
    m_pNotebook->SetSelection(idx);
}

//*****************************************************************************
void ChatMainFrame::IncomingIM(const wxString &strFrom, const wxString &strMessage)
{
    wxLogDebug(wxT("ChatMainFrame::IncomingIM"));

    int idx = FindChat(strFrom, true);

    if (idx >= 0)
    {
        ChatPanel *panel = dynamic_cast<ChatPanel*>(m_pNotebook->GetPage(idx));

        panel->DisplayIM(strFrom,strMessage);

        // TODO: indicate tab has text
    }
    else
        wxLogDebug(wxT("Chat window for %s not found"), strFrom.wc_str());
}

//*****************************************************************************
void ChatMainFrame::IncomingComposing(const wxString &strFrom, bool composing)
{
    // TODO: show/clear composing status
}

//*****************************************************************************
void ChatMainFrame::StartChat( const wxString &strParticipantID, const wxString &strParticipantName, const wxString &strChatID, const wxString &strChatTitle, const wxString &strPassword, bool fJoining )
{

    wxLogDebug(wxT("ChatMainFrame::StartChat"));

//    m_strParticipantID = strParticipantID;
//    m_strParticipantName = strParticipantName;
//    m_strChatID = strChatID;
//    m_strChatTitle = strChatTitle;
//    m_strPassword = strPassword;

//    m_strParticipantID.StartsWith( wxT("part:"), &m_strParticipantID);

    wxString    strChatRoomName( _("New Chat"), wxConvUTF8 );
    SetTitle( strChatRoomName );

    wxTextEntryDialog   dlg( this, _("Enter names, separated by commas: "), _("Invite to new Chat room"), _T(""), wxOK | wxCANCEL );    if( dlg.ShowModal( )  !=  wxID_OK )
        return;

    wxString    strInvite;
    strInvite = dlg.GetValue( );
/*
    m_pChatPanel = new ChatPanel( this );
    m_pChatPanel->SetSize( kCDSplitterWidth, 300 );
    m_pNotebook->AddPage( m_pChatPanel, strInvite, true );

    char    CID[ 100 ], CTitle[ 200 ], PID[ 100 ], PName[ 200 ];
    strcpy( CID, "CID" );
    strcpy( CTitle, "CTitle" );
    strcpy( PID,   "PID" );
    strcpy( PName, m_strParticipantName.mb_str( wxConvUTF8 ) );

    strcpy( PName, m_strParticipantName.mb_str( wxConvUTF8 ) );
    strcpy( CID, m_strChatID.mb_str( wxConvUTF8 ) );
    strcpy( CTitle, m_strChatTitle.mb_str( wxConvUTF8 ) );
    strcpy( PID,   m_strParticipantID.mb_str( wxConvUTF8 ) );

    char jid[512];
    sprintf(jid, "%s@%s", screen, zact->servername);
    strInvite.Append( wxT("@dbs.homedns.org") );
        //int session_make_rpc_call(session_t sess, const char *id, const char *service, const char *method, const char *format, ...);
    session_make_rpc_call(CONTROLLER.session, "rpc_iminvite", strInvite.mb_str( wxConvUTF8 ), "im.invite", "(ssssss)",
                          CID, CTitle, CTitle,
                          "you are invited",                        //  message
                          PID, PName );

    wxString    strBlank;
    m_pChatPanel->StartMeeting( strBlank, strParticipantName, strBlank, strBlank, false );
//    m_pChatPanel->StartMeeting( m_strParticipantID, strParticipantName, strChatID, strChatTitle, fJoining );

    int     iAlpha  = 255;
    if( PREFERENCES.m_fHidePresenterMeetingWindow )
        iAlpha = 254;
    bool    fCan;
    fCan = CanSetTransparent( );
    if( fCan )
    {
        SetTransparent( iAlpha );
    }
*/
}


//*****************************************************************************
void ChatMainFrame::SetParticipantID( const wxString &strParticipantID )
{
    wxLogDebug(wxT("ChatMainFrame::SetParticipantID"));

    //m_pChatPanel->SetParticipantID( strParticipantID );
}


//*****************************************************************************
void ChatMainFrame::ConnectionLost( )
{
    wxLogDebug( wxT("entering ChatMainFrame::ConnectionLost( )") );
    m_pStatusBar->SetStatusText( _("Connection lost..."));

    //m_pChatPanel->ConnectionLost( );
}


//*****************************************************************************
void ChatMainFrame::ConnectionReconnected( )
{
    wxLogDebug( wxT("entering ChatMainFrame::ConnectionReconnected( )") );
    m_pStatusBar->SetStatusText( _("Connection reconnected..."));

    //m_pChatPanel->ConnectionReconnected( );
}


//*****************************************************************************
wxAuiNotebook *ChatMainFrame::CreateNotebook( )
{
    // create the notebook off-window to avoid flicker
    //wxSize client_size = GetClientSize();

    wxAuiNotebook *ctrl = new wxAuiNotebook( this, wxID_ANY,
                                             wxPoint(-1,-1),
                                             wxSize(-1,-1),
                                             wxAUI_NB_TOP | wxAUI_NB_TAB_SPLIT | wxAUI_NB_TAB_MOVE | wxAUI_NB_SCROLL_BUTTONS | wxAUI_NB_CLOSE_ON_ALL_TABS | wxNO_BORDER);

    wxBitmap page_bmp = wxArtProvider::GetBitmap( wxART_NORMAL_FILE, wxART_OTHER, wxSize(16,16) );

    return ctrl;
}


//*****************************************************************************
void ChatMainFrame::AddIM( wxString strPerson )
{
}


//*****************************************************************************
void ChatMainFrame::CreateChatRoom( wxString strRoomName, wxString strPerson )
{
}


//*****************************************************************************
void ChatMainFrame::AddChatRoom( wxString strRoomName, wxString strPerson )
{
}
