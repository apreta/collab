/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "selectapplication.h"

//(*InternalHeaders(SelectApplication)
#include <wx/bitmap.h>
#include <wx/button.h>
#include <wx/font.h>
#include <wx/fontenum.h>
#include <wx/fontmap.h>
#include <wx/image.h>
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/xrc/xmlres.h>
//*)
#include "../sharelib/AppList.h"

//(*IdInit(SelectApplication)
//*)

BEGIN_EVENT_TABLE(SelectApplication,wxDialog)
	//(*EventTable(SelectApplication)
	//*)
END_EVENT_TABLE()

SelectApplication::SelectApplication(wxWindow* parent, const wxString display) :
m_Display(display), m_MyAppList( NULL )
{
    wxBusyCursor    wait;
	//(*Initialize(SelectApplication)
	wxXmlResource::Get()->LoadObject(this,parent,_T("SelectApplication"),_T("wxDialog"));
	AppShareListBox1 = (wxListBox*)FindWindow(XRCID("ID_LISTBOX1"));
	Connect(XRCID("ID_LISTBOX1"),wxEVT_COMMAND_LISTBOX_SELECTED,(wxObjectEventFunction)&SelectApplication::OnAppShareListBox1Select);
	//*)

    Connect(XRCID("wxID_NO"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&SelectApplication::refresh);

    Selected = -1;              // none yet

    wxCommandEvent  event;
    refresh( event );
}

SelectApplication::~SelectApplication()
{
    if( m_MyAppList )
        free( m_MyAppList );
	//(*Destroy(SelectApplication)
	//*)
}


void SelectApplication::OnAppShareListBox1Select(wxCommandEvent& event)
{
    // how do I get selected item ?
    wxArrayInt selections;
    int NumSelected = AppShareListBox1->GetSelections(selections);
    if( NumSelected  >  0 )
        Selected = selections[0];
    else
        Selected = -1;
}


int SelectApplication::GetSelectedApps( std::string &strApps )
{
    wxArrayInt  selections;
    int         NumSelected = AppShareListBox1->GetSelections( selections );
    if( NumSelected  <=  0 )
        return 0;

    strApps.clear( );
    std::string     strComma( "," );
    int             i;
    for( i=0; i<NumSelected; i++ )
    {
        std::string     id = *m_MyAppList->xwinEntry[ selections[ i ] ].id;
        strApps.append( id );
        strApps.append( strComma );
    }
    return NumSelected;
}


void SelectApplication::refresh( wxCommandEvent &event )
{
    if( m_MyAppList )
        free( m_MyAppList );

    AppShareListBox1->Set( 0, NULL );

    m_MyAppList = new AppList(m_Display.mb_str());
    m_MyAppList->FindNamedIdsWith("Map State","IsViewable");      // find visible named windows
    for(int i=0; i<m_MyAppList->windows; i++)
    {
        wxString label(m_MyAppList->xwinEntry[i].name->c_str(),wxConvUTF8);    // GOTTA be an easier way !
        AppShareListBox1->InsertItems(1,&label,i);
    }
}
