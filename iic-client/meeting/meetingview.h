/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef _MEETINGVIEW_H_
#define _MEETINGVIEW_H_

#include <wx/event.h>
#include "schedule.h"
#include "events.h"
#include "services/common/common.h"
#include "htmlgen.h"

class MeetingView : wxEvtHandler
{
public:
	MeetingView();
    virtual ~MeetingView();

    /** Display the window.
      * This method will query the server to obtain the meeting details and immediatly return
      * to the caller. A short time later the meeting details window will be displayed as a modal dialog.
      */
    void Display(  wxWindow * parent, const wxString & meeting_id );

    /** Create the report */
    static void CreateReport( meeting_details_t mtg, wxString & strHTML, wxWindow *parent, wxString &strPin, wxString strModerator, wxString strStatusMsg );

    /** Add a report table row */
    static void addRow( HtmlGen & r, const wxString & label, const wxString & value );

    /** Find host name */
    static void GetHostName( meeting_details_t mtg, wxString & strHostName );

private:
    /** Process the meeting details.<br>
      * This method creates the simple HTML content for the display.
      */
    void OnMeetingFetched(wxCommandEvent& event);       /**< Received after meeting details are received */

    /** Show the report */
    void DisplayHTML( const wxString & strHTML );

    /** The parent window */
    wxWindow *m_parent;

    /** The meeting ID */
    wxString m_meeting_id;

    /** Flag moderator */
    wxString m_moderator;
};
#endif

