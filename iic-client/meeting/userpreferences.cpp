/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"
#include "userpreferences.h"
#include <wx/settings.h>

#include "base64.h"

#define DEF_CHAT_FONT 8

UserPreferences::UserPreferences():
m_fSavePassword(true),
m_fAutoSignOn(false),
m_fKeepTryingConnection(true),
m_fStartOnComputerStart(false),
m_fIMClient(false),
m_fDownloadPublicMeetings(true),
m_fDownloadCAB(true),
m_fCacheAB(false),
m_fDownloadOutlook(false),
m_fShowInstantMeeting(true),
m_fEnablePrivateMeetings(true),
m_nChatFontSize(DEF_CHAT_FONT),
m_fChatBold(false),
m_fChatItalic(false),
m_fChatUnderline(false),
m_fMuteAllSounds(false),
m_nChatSounds(1),
m_fPlayChatInvtSound(true),
m_nInMeetingSounds(0),
m_fDisableInMeetingSounds(true),
m_fHidePresenterMeetingWindow(true),
m_fAutoDetectSlowConnection(false),
m_fReduceColor(false),
m_fUsePowerPoint(false),
m_fUseDirectX(false),
m_nPCPhoneBandwidth(0),
m_nPCPhoneVAD(1),
m_fEnableEchoCancellation(false),
m_fEnableNoiseFilter(false),
m_nAudioSubsystem(1), // Voip_Audio_Internal_PA ( TODO: Include the Reqd. header )
m_nMaxJitter(2000),
m_nSuppressionThreshold(500)
{
    m_AppFont = s_AppDefFont;
#ifdef WIN32
	// GetColour call was hanging on Ubuntu; probably called too early
	// in the initialization process
    m_ChatBG = wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOW );
    m_ChatFG = wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOWTEXT );
#endif
}

void UserPreferences::SetDefaultFont(wxFont &defFont)
{
    s_AppDefFont = defFont;
    m_AppFont = defFont;
}

UserPreferences & UserPreferences::operator=( const UserPreferences & rhs )
{
    // General panel
    m_strPassword = rhs.m_strPassword;
    m_fSavePassword = rhs.m_fSavePassword;
    m_fAutoSignOn = rhs.m_fAutoSignOn;
    m_fKeepTryingConnection = rhs.m_fKeepTryingConnection;
    m_fStartOnComputerStart = rhs.m_fStartOnComputerStart;
    m_fDownloadPublicMeetings = rhs.m_fDownloadPublicMeetings;
    m_fDownloadCAB = rhs.m_fDownloadCAB;
    m_fCacheAB = rhs.m_fCacheAB;
    m_fDownloadOutlook = rhs.m_fDownloadOutlook;
    m_fIMClient = rhs.m_fIMClient;
    m_fShowInstantMeeting = rhs.m_fShowInstantMeeting;
    m_fEnablePrivateMeetings = rhs.m_fEnablePrivateMeetings;

    // Font panel
    m_AppFont = rhs.m_AppFont;
    m_ChatBG = rhs.m_ChatBG;
    m_ChatFG = rhs.m_ChatFG;
    m_nChatFontSize = rhs.m_nChatFontSize;
    m_fChatBold = rhs.m_fChatBold;
    m_fChatItalic = rhs.m_fChatItalic;
    m_fChatUnderline = rhs.m_fChatUnderline;

    // Sounds panel
    m_fMuteAllSounds = rhs.m_fMuteAllSounds;
    m_nChatSounds = rhs.m_nChatSounds;
    m_fPlayChatInvtSound = rhs.m_fPlayChatInvtSound;
    m_nInMeetingSounds = rhs.m_nInMeetingSounds;
    m_fDisableInMeetingSounds = rhs.m_fDisableInMeetingSounds;

    // Data share panel
    m_fHidePresenterMeetingWindow = rhs.m_fHidePresenterMeetingWindow;
    m_fAutoDetectSlowConnection = rhs.m_fAutoDetectSlowConnection;
    m_fReduceColor = rhs.m_fReduceColor;
    m_fUsePowerPoint = rhs.m_fUsePowerPoint;
    m_fUseDirectX = rhs.m_fUseDirectX;
        
    // PC phone panel
    m_nPCPhoneBandwidth = rhs.m_nPCPhoneBandwidth;
    m_nPCPhoneVAD = rhs.m_nPCPhoneVAD;
    m_fEnableEchoCancellation = rhs.m_fEnableEchoCancellation;
    m_fEnableNoiseFilter = rhs.m_fEnableNoiseFilter;
    m_nAudioSubsystem = rhs.m_nAudioSubsystem;
    m_nMaxJitter = rhs.m_nMaxJitter;
    m_nSuppressionThreshold = rhs.m_nSuppressionThreshold;
    m_nSilenceThreshold = rhs.m_nSilenceThreshold;

    // Internal communications
    m_nVersion = rhs.m_nVersion;
    m_strLastJID = rhs.m_strLastJID;

    return *this;
}

void UserPreferences::Load()
{
    wxZonOptions & opt = wxGetApp().Options();
    const char *blank = "";

    ///// Version number /////////////////////////////////////////////////////////////////////////////
    m_nVersion = opt.GetUserInt( "Version", 0 );

    ///// General Panel /////////////////////////////////////////////////////////////////////////////
    char        pwdDecoded[ 128 ];
    int         iLenDecoded;
    memset( pwdDecoded, 0, 128 );

    //  Passwords are Base64 encoded starting with file version 5
    if( m_nVersion > 4 )
    {
        CBase64     base64;
        iLenDecoded = base64.Decode( OPTIONS.GetUser( "SignOnPassword", blank ), pwdDecoded );
        pwdDecoded[ iLenDecoded ] = 0;
        m_strPassword = wxString::FromAscii( pwdDecoded );
    }
    else
        m_strPassword = wxString::FromAscii( OPTIONS.GetUser( "SignOnPassword", blank ) );

    m_fSavePassword = opt.GetUserBool( "SignOnSavePassword", true );
    m_fAutoSignOn = opt.GetUserBool( "SignOnAutoSignOn", false );
    m_fKeepTryingConnection = opt.GetUserBool( "SignOnAutoRetry", true );
    m_fStartOnComputerStart = opt.GetUserBool( "StartupOnComputerStart", false );
    m_fDownloadPublicMeetings = opt.GetUserBool( "DownloadPublicMeetings", true );
    m_fDownloadCAB = opt.GetUserBool( "DownloadCAB", true );
    m_fCacheAB = opt.GetUserBool( "CacheAB", false );
    m_fDownloadOutlook = opt.GetUserBool( "DownloadOutlook", false );
    m_fIMClient = opt.GetUserBool( "IMClient", false );
    m_fShowInstantMeeting = opt.GetUserBool( "ShowInstantMeeting", true );

    bool defEnablePrivate = !wxGetApp().Options().GetSystemBool( "DisablePrivate", false );
    m_fEnablePrivateMeetings = opt.GetUserBool( "EnablePrivateMeetings", defEnablePrivate );

    ////// Contact display Panel /////////////////////////////////////////////////////////////////
    wxArrayInt defaultList;
    wxArrayInt newList;
    opt.GetUserArrayInt( "ContactListColumns", defaultList, m_DisplayedContactColumns );

    ////// Font Panel ////////////////////////////////////////////////////////////////////////////
    wxColour defaultFG;
    wxColour defaultBG;

    defaultBG = wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOW );
    defaultFG = wxSystemSettings::GetColour( wxSYS_COLOUR_WINDOWTEXT );

    m_AppFont = opt.GetUserFont( "ApplicationFont", s_AppDefFont );
    m_ChatBG = opt.GetUserColour( "ChatBGColour",  defaultBG );
    m_ChatFG = opt.GetUserColour( "ChatFGColour",  defaultFG );
    m_nChatFontSize = opt.GetUserInt( "ChatRelFontSize", DEF_CHAT_FONT );
    m_fChatBold = opt.GetUserBool( "ChatFontBold", false );
    m_fChatItalic = opt.GetUserBool( "ChatFontItalic", false );
    m_fChatUnderline = opt.GetUserBool( "CharFontUnderline", false  );

    ///// Sounds Panel /////////////////////////////////////////////////////////////////////////
    m_fMuteAllSounds = opt.GetUserBool( "MuteAllSounds", false );
    m_nChatSounds = opt.GetUserInt( "ChatSounds", 1 );
    m_fPlayChatInvtSound = opt.GetUserBool( "PlayChatInvtSound", true );
    m_nInMeetingSounds = opt.GetUserInt( "InMeetingSounds", 0 );
    m_fDisableInMeetingSounds = opt.GetUserBool( "DisableInMeetingSounds", true );

    ///// Data share Panel /////////////////////////////////////////////////////////////////////
    m_fHidePresenterMeetingWindow = opt.GetUserBool( "HidePresenterMeetingWindow", true );
    m_fAutoDetectSlowConnection = opt.GetUserBool( "AutoDetectSlowConnection", false );
    m_fReduceColor = opt.GetUserBool( "ReduceColor", false );
    m_fUsePowerPoint = opt.GetUserBool( "UsePowerPointConverter", false );
    m_fUseDirectX = opt.GetUserBool( "UseDirectX", false );
    
    ///// PC phone Panel //////////////////////////////////////////////////////////////////////
    m_nPCPhoneBandwidth = opt.GetUserInt( "PCPhoneBandwidth", 0 );
    m_nPCPhoneVAD = opt.GetUserInt( "PCPhoneVAD", 1 );
    m_fEnableEchoCancellation = opt.GetUserBool( "EnableEchoCancellation", false );
    m_fEnableNoiseFilter = opt.GetUserBool( "EnableNoiseFilter", false );
    m_nAudioSubsystem = opt.GetUserInt( "AudioSubsystem", 1 );
    m_nMaxJitter = opt.GetUserInt( "MaxJitter", 2000 );
    m_nSuppressionThreshold = opt.GetUserInt( "SuppressionThreshold", 500 );
    m_nSilenceThreshold = opt.GetUserInt( "SilenceThreshold", 1 );

    ///// Internal Communications /////////////////////////////////////////////////////////////////////////////
    m_strLastJID = wxString::FromAscii( opt.GetUser( "LastJID", blank ) );

    // Updates...force change to high bandwidth for current users
    if (m_nVersion < 2)
    {
        m_nPCPhoneBandwidth = 0;
    }
    if (m_nVersion < 3)
    {
        m_nSuppressionThreshold = 500;
    }
    if (m_nVersion < 4)
    {
        m_nAudioSubsystem = 1;      // switch back to OSS
    }
    m_nVersion = 5;

    // Verifications
    if (m_nPCPhoneBandwidth < 0 || m_nPCPhoneBandwidth > 3)
        m_nPCPhoneBandwidth = 0;
    if (m_nPCPhoneVAD < 0 || m_nPCPhoneVAD > 2)
        m_nPCPhoneVAD = 1;
}

void UserPreferences::Save()
{
    wxZonOptions & opt = wxGetApp().Options();

    ///// General Panel /////////////////////////////////////////////////////////////////////////////
    if( m_fSavePassword )
    {
        CBase64     base64;
        char        *pwdEncoded;
        pwdEncoded = base64.Encode( m_strPassword.mb_str(wxConvUTF8), m_strPassword.Len( ) );
        opt.SetUser( "SignOnPassword", pwdEncoded );
        free( pwdEncoded );
    }else
    {
        wxString blank_password;
        blank_password = wxT("");
        opt.SetUser( "SignOnPassword", blank_password.mb_str(wxConvUTF8) );
    }
    opt.SetUserBool( "SignOnSavePassword", m_fSavePassword );
    opt.SetUserBool( "SignOnAutoSignOn",   m_fAutoSignOn );
    opt.SetUserBool( "SignOnAutoRetry",    m_fKeepTryingConnection );
    opt.SetUserBool( "StartupOnComputerStart", m_fStartOnComputerStart );
    opt.SetUserBool( "DownloadPublicMeetings", m_fDownloadPublicMeetings );
    opt.SetUserBool( "DownloadCAB", m_fDownloadCAB );
    opt.SetUserBool( "CacheAB", m_fCacheAB );
    opt.SetUserBool( "DownloadOutlook", m_fDownloadOutlook );
    opt.SetUserBool( "IMClient", m_fIMClient );
    opt.SetUserBool( "ShowInstantMeeting", m_fShowInstantMeeting );
    opt.SetUserBool( "EnablePrivateMeetings", m_fEnablePrivateMeetings );

    ////// Contact display Panel //////////////////////////////////////////////////////////////////
    opt.SetUserArrayInt( "ContactListColumns", m_DisplayedContactColumns );

    ////// Font Panel /////////////////////////////////////////////////////////////////////////////
    opt.SetUserFont( "ApplicationFont", m_AppFont );
    opt.SetUserColour( "ChatBGColour", m_ChatBG );
    opt.SetUserColour( "ChatFGColour", m_ChatFG );
    opt.SetUserInt( "ChatRelFontSize", m_nChatFontSize );
    opt.SetUserBool( "ChatFontBold", m_fChatBold );
    opt.SetUserBool( "ChatFontItalic", m_fChatItalic );
    opt.SetUserBool( "CharFontUnderline", m_fChatUnderline );

    ///// Sounds Panel ////////////////////////////////////////////////////////////////////////////
    opt.SetUserBool( "MuteAllSounds", m_fMuteAllSounds );
    opt.SetUserInt( "ChatSounds", m_nChatSounds );
    opt.SetUserBool( "PlayChatInvtSound", m_fPlayChatInvtSound );
    opt.SetUserInt( "InMeetingSounds", m_nInMeetingSounds );
    opt.SetUserBool( "DisableInMeetingSounds", m_fDisableInMeetingSounds );

    ///// Data share Panel /////////////////////////////////////////////////////////////////////
    opt.SetUserBool( "HidePresenterMeetingWindow", m_fHidePresenterMeetingWindow );
    opt.SetUserBool( "AutoDetectSlowConnection", m_fAutoDetectSlowConnection );
    opt.SetUserBool( "ReduceColor", m_fReduceColor );
    opt.SetUserBool( "UsePowerPointConverter", m_fUsePowerPoint );
    opt.SetUserBool( "UseDirectX", m_fUseDirectX );

    ///// PC phone Panel //////////////////////////////////////////////////////////////////////
    opt.SetUserInt( "PCPhoneBandwidth", m_nPCPhoneBandwidth );
    opt.SetUserInt( "PCPhoneVAD", m_nPCPhoneVAD );
    opt.SetUserBool( "EnableEchoCancellation", m_fEnableEchoCancellation );
    opt.SetUserBool( "EnableNoiseFilter", m_fEnableNoiseFilter );
    opt.SetUserInt( "AudioSubsystem", m_nAudioSubsystem );
    opt.SetUserInt( "MaxJitter", m_nMaxJitter );
    opt.SetUserInt( "SuppressionThreshold", m_nSuppressionThreshold );
    opt.SetUserInt( "SilenceThreshold", m_nSilenceThreshold );

    ///// Internal Communications /////////////////////////////////////////////////////////////////////////////
    opt.SetUserInt( "Version", m_nVersion );
    opt.SetUser( "LastJID", m_strLastJID.mb_str(wxConvUTF8) );
}

wxFont UserPreferences::s_AppDefFont;
