/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"

#include "signondlg.h"
#include <wx/dir.h>
#include <wx/stdpaths.h>
#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <wx/valtext.h>

#include "services/common/common.h"
#include "meetingcentercontroller.h"
#include "logupload.h"

#include "base64.h"
#include "cryptlib/cryptlib.h"


//(*InternalHeaders(SignOnDlg)
#include <wx/xrc/xmlres.h>
//*)

SignOnParams & SignOnParams::operator=(const SignOnParams & rhs)
{
    m_strServer = rhs.m_strServer;
    m_strScreen = rhs.m_strScreen;
    m_strPassword = rhs.m_strPassword;
    m_strPrevScreen = rhs.m_strPrevScreen;
    m_strPrevPassword = rhs.m_strPrevPassword;
    m_strMeetingPIN = rhs.m_strMeetingPIN;
    m_strFullName = rhs.m_strFullName;
    m_strPhone = rhs.m_strPhone;
    m_strEmail = rhs.m_strEmail;
    m_fSavePassword = rhs.m_fSavePassword;
    m_fAutoSignOn = rhs.m_fAutoSignOn;
    m_fKeepTryingConnection = rhs.m_fKeepTryingConnection;
    m_fAutoHunt = rhs.m_fAutoHunt;
    m_fEnableSSL = rhs.m_fEnableSSL;
    m_fDebugging = rhs.m_fDebugging;
    m_fAnonymousUser = rhs.m_fAnonymousUser;
    m_fHideServerEntry = rhs.m_fHideServerEntry;
    m_fTestConnection = rhs.m_fTestConnection;

    return *this;
}


//(*IdInit(SignOnDlg)
//*)

IMPLEMENT_DYNAMIC_CLASS(SignOnDlg,wxDialog)
BEGIN_EVENT_TABLE(SignOnDlg,wxDialog)
	//(*EventTable(SignOnDlg)
    //*)
EVT_COMBOBOX(XRCID("ID_SCREEN_COMBO"), SignOnDlg::OnScreenComboBox)
EVT_TEXT(XRCID("ID_SCREEN_COMBO"), SignOnDlg::OnScreenText)
END_EVENT_TABLE()

SignOnDlg::SignOnDlg(wxWindow* parent, wxWindowID id):
m_parent(parent)
{
	//(*Initialize(SignOnDlg)
	wxXmlResource::Get()->LoadObject(this,parent,_T("SignOnDlg"),_T("wxDialog"));
	m_LogoBitmap = (wxStaticBitmap*)FindWindow(XRCID("ID_LOGO_BITMAP"));
	m_RegisteredUserCheck = (wxCheckBox*)FindWindow(XRCID("ID_REGISTEREDUSER_CHECK"));
	m_ServerLabel = (wxStaticText*)FindWindow(XRCID("ID_SERVER_LABEL"));
	m_ServerEdit = (wxTextCtrl*)FindWindow(XRCID("ID_SERVER_EDIT"));
	m_FullNameLabel = (wxStaticText*)FindWindow(XRCID("ID_FULLNAME_LABEL"));
	m_FullNameEdit = (wxTextCtrl*)FindWindow(XRCID("ID_FULLNAME_EDIT"));
	m_ScreenLabel = (wxStaticText*)FindWindow(XRCID("ID_SCREEN_LABEL"));
	m_ScreenCombo = (wxComboBox*)FindWindow(XRCID("ID_SCREEN_COMBO"));
	m_PasswordLabel = (wxStaticText*)FindWindow(XRCID("ID_PASSWORD_LABEL"));
	m_PasswordEdit = (wxTextCtrl*)FindWindow(XRCID("ID_PASSWORD_EDIT"));
	m_MeetingPinLabel = (wxStaticText*)FindWindow(XRCID("ID_MEETINGPIN_LABEL"));
	m_AnonPinLabel = (wxStaticText*)FindWindow(XRCID("ID_ANONPIN_LABEL"));
	m_MeetingPinEdit = (wxTextCtrl*)FindWindow(XRCID("ID_MEETINGPIN_EDIT"));
	m_PhoneLabel = (wxStaticText*)FindWindow(XRCID("ID_PHONE_LABEL"));
	m_PhoneEdit = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL1"));
	m_EmailLabel = (wxStaticText*)FindWindow(XRCID("ID_EMAIL_LABEL"));
	m_EmailEdit = (wxTextCtrl*)FindWindow(XRCID("ID_EMAIL_EDIT"));
	m_SavePasswordCheck = (wxCheckBox*)FindWindow(XRCID("ID_SAVEPASSWORD_CHECK"));
	m_AutoSignOnCheck = (wxCheckBox*)FindWindow(XRCID("ID_AUTOSIGNON_CHECK"));
	m_KeepTryingCheck = (wxCheckBox*)FindWindow(XRCID("ID_KEEPTRYING_CHECK"));
	m_HttpCheck = (wxCheckBox*)FindWindow(XRCID("ID_CHECKBOX_HTTP"));
	m_TestLink = (wxHyperLink*)FindWindow(XRCID("ID_TEST_LINK"));
	
	Connect(XRCID("ID_REGISTEREDUSER_CHECK"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&SignOnDlg::OnRegisteredUserClick);
	Connect(XRCID("ID_SAVEPASSWORD_CHECK"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&SignOnDlg::OnSavePasswordClick);
	Connect(XRCID("ID_AUTOSIGNON_CHECK"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&SignOnDlg::OnAutoSignOnClick);
	//*)
	m_TestLink->Connect(XRCID("ID_TEST_LINK"),wxEVT_LEFT_DOWN,wxMouseEventHandler(SignOnDlg::OnTest), NULL, this);
	Connect(wxID_ANY,wxEVT_INIT_DIALOG,(wxObjectEventFunction)&SignOnDlg::OnInit);
    Connect( wxID_ANY, wxEVT_SIZE, wxSizeEventHandler(SignOnDlg::OnSize) );
    
    // Can't use numeric filter as it allows decimals, e notation, etc.
    wxString allowed[] = { wxT("0"), wxT("1"), wxT("2"), wxT("3"), wxT("4"), wxT("5"), wxT("6"), wxT("7"), wxT("8"), wxT("9") };
    wxTextValidator vd(wxFILTER_INCLUDE_CHAR_LIST);
    vd.SetIncludes(wxArrayString(10, allowed));
#ifndef __WXMAC__
    // Setting this validator on OSX blocks cut & paste.  Being able to cut and paste the pin from an invite is
    // important.
    m_MeetingPinEdit->SetValidator(vd);
#endif
    m_MeetingPinEdit->SetMaxLength(18);
}

bool SignOnDlg::GetSignOnInformation( SignOnParams & params )
{
    m_params = params;

    wxString strTitle = PRODNAME + _(" - Sign On");
    this->SetTitle(strTitle);

    m_SavePasswordCheck->SetValue( m_params.m_fSavePassword );
    m_AutoSignOnCheck->SetValue( m_params.m_fAutoSignOn );
    m_KeepTryingCheck->SetValue( m_params.m_fKeepTryingConnection );
    m_HttpCheck->SetValue( !m_params.m_fAutoHunt );

    // Shrink to fit
    // GetSizer()->Fit(this);

    // Locate the OK button.
    wxWindow *pOKWindow = wxWindow::FindWindowById( wxID_OK, this );
    if( pOKWindow )
    {
        wxButton *pOKButton = wxDynamicCast( pOKWindow, wxButton );
        if( pOKButton )
        {
            pOKButton->SetDefault();
        }
    }

    m_ServerEdit->SetValue( m_params.m_strServer );

    wxString strScreen(m_params.m_strScreen);
    CUtils::DecodeScreenName( strScreen );
    
    wxString    strT, strB, strA;
    strA = strScreen.AfterFirst( wxT(':') );
    if( !strA.IsEmpty( ) )
    {
        strB = strScreen.BeforeFirst( wxT(':') );
        strT.Append( strB.Lower( ) );
        strT.Append( wxT(':') );
        strT.Append( strA );
    }
    else
        strT.Append( strScreen.Lower( ) );

    // Don't put anonymous screen name/password into dialog
    if ( strT != wxT(ANONYMOUS_NAME) )
    {
        m_ScreenCombo->SetValue( strT );
        m_PasswordEdit->SetValue( m_params.m_strPassword );
    }

    m_FullNameEdit->SetValue( m_params.m_strFullName );
    m_MeetingPinEdit->SetValue( m_params.m_strMeetingPIN );

    bool previousScreens = PopulateScreenNamesCombo( m_params.m_strScreen );

    if (!previousScreens && CheckZonPassword())
        previousScreens = true;

    // If we've never had a sign on before (no saved screens) and we have a pin,
    // or screen name has been set to anonymous, set dialog up for anonymous sign on.
    if ( (!previousScreens && !m_params.m_strMeetingPIN.IsEmpty()) || m_params.m_strScreen == wxT(ANONYMOUS_NAME) )
    {
        SetUnregistered();
    }
    else
    {
        m_params.m_fAnonymousUser = false;
    }

    m_RegisteredUserCheck->SetValue( !m_params.m_fAnonymousUser );
    SetUIFeedback();

    Center();

    while (true)
    {
        int nRet = ShowModal();
        if( nRet == wxID_OK )
        {
            if (!Validate())
                continue;

            m_params.m_strServer = m_ServerEdit->GetValue();

            m_params.m_strMeetingPIN = m_MeetingPinEdit->GetValue();

            if( !m_RegisteredUserCheck->GetValue() )
            {
                m_params.m_strFullName = m_FullNameEdit->GetValue();
                m_params.m_strPhone = m_PhoneEdit->GetValue();
                m_params.m_strEmail = m_EmailEdit->GetValue();

                //  Move to the click event handler for the registered checkbox
                m_params.m_fAnonymousUser = true;
            }
            else
            {
                m_params.m_strScreen = m_ScreenCombo->GetValue( );
                m_params.m_strPassword = m_PasswordEdit->GetValue();
                m_params.m_fAnonymousUser = false;
            }

            m_params.m_fSavePassword = m_SavePasswordCheck->GetValue();
            m_params.m_fAutoSignOn = m_AutoSignOnCheck->GetValue();
            m_params.m_fKeepTryingConnection = m_KeepTryingCheck->GetValue();

            if (m_params.m_fAutoHunt != !m_HttpCheck->GetValue())
            {
                session_reset_hunt(CONTROLLER.session);

                m_params.m_fAutoHunt = !m_HttpCheck->GetValue();
            }
            m_params.m_fDebugging = false;

            // Return the settings to the caller
            params = m_params;

            return true;
        }
        return false;
    }
}

void SignOnDlg::SetPin(wxString& strPin)
{
    m_MeetingPinEdit->SetValue(strPin);
}

SignOnDlg::~SignOnDlg()
{
}


bool SignOnDlg::PopulateScreenNamesCombo( wxString &strScreen )
{
    bool screenPresent = false;

    wxString fileSpec( wxT("*.opt") );
    wxString fileName;
    wxDir dir( wxStandardPaths::Get().GetUserDataDir() );
    if( !dir.IsOpened() )
        return false;

    bool fMore = dir.GetFirst( &fileName, fileSpec, wxDIR_FILES );
    while( fMore )
    {
        int dot_opt = fileName.Find( wxT(".opt") );
        if( dot_opt != wxNOT_FOUND )
        {
            fileName = fileName.Left( dot_opt );
        }
        if( fileName.Len( ) > 0  &&  (fileName != wxT(ANONYMOUS_NAME) && fileName != wxT("temp") && fileName != wxT("zon")) )
        {
            wxString screen( fileName );
            CUtils::DecodeStringFully( screen );

            wxString displayScreen( screen );
            CUtils::DecodeScreenName( displayScreen );

            wxString    *pPass  = NULL;
            OPTIONS.SetScreenName( screen );
            if( OPTIONS.Read( ) )
            {
                int     iVersion = OPTIONS.GetUserInt( "Version", 0 );
                wxString    strT2;

                //  Passwords are Base64 encoded starting with file version 5
                if( iVersion > 4 )
                {
                    CBase64     base64;
                    char        pwdDecoded[ 128 ];
                    int         iLenDecoded;
                    const char *blank   = "";
                    memset( pwdDecoded, 0, 128 );
                    iLenDecoded = base64.Decode( OPTIONS.GetUser( "SignOnPassword", blank ), pwdDecoded );
                    pwdDecoded[ iLenDecoded ] = 0;
                    wxString    strT( pwdDecoded, wxConvUTF8 );
                    strT2.Append( strT );
                    pPass = new wxString( strT2 );
                }
                else
                {
                    const char *blank   = "";
                    strT2 = wxString::FromAscii( OPTIONS.GetUser( "SignOnPassword", blank ) );
                    pPass = new wxString( strT2 );
                }

                SN_t    pSN_t  = new SN_st;
                pSN_t->strSN.Append( displayScreen );
                pSN_t->strPass.Append( strT2 );
                pSN_t->fSavePass = OPTIONS.GetUserBool("SignOnSavePassword", true );
                pSN_t->fAutoLogin = OPTIONS.GetUserBool("SignOnAutoSignOn", false );
                m_arrSN_t.Add( pSN_t );
            }

            m_ScreenCombo->Append( displayScreen, (void *) pPass );
            screenPresent = true;
        }

        fMore = dir.GetNext( &fileName );
    }

    OPTIONS.SetScreenName( strScreen );
    OPTIONS.Read( );

    return screenPresent;
}

void SignOnDlg::OnSavePasswordClick(wxCommandEvent& event)
{
    if( !m_SavePasswordCheck->GetValue() )
    {
        m_AutoSignOnCheck->SetValue(false);
    }
}

void SignOnDlg::OnAutoSignOnClick(wxCommandEvent& event)
{
    if( m_AutoSignOnCheck->GetValue() )
    {
        if( !m_SavePasswordCheck->GetValue() )
        {
            m_SavePasswordCheck->SetValue(true);
        }
    }
}

void SignOnDlg::SetUnregistered()
{
    m_params.m_strPrevScreen   = m_params.m_strScreen;
    m_params.m_strPrevPassword = m_params.m_strPassword;
    m_params.m_strScreen = wxT(ANONYMOUS_NAME);
    m_params.m_strPassword = wxT(ANONYMOUS_PASSWORD);
    m_params.m_fAnonymousUser = true;
}

void SignOnDlg::OnRegisteredUserClick(wxCommandEvent& event)
{
    bool registered = m_RegisteredUserCheck->GetValue();
    if( registered )
    {
        m_params.m_strScreen   = m_params.m_strPrevScreen;
        m_params.m_strPassword = m_params.m_strPrevPassword;
        m_params.m_fAnonymousUser = false;
        m_ScreenCombo->SetFocus();
    }
    else
    {
        SetUnregistered();
        m_FullNameEdit->SetFocus();
    }
    SetUIFeedback();
}

void SignOnDlg::SetUIFeedback()
{
    bool registered = m_RegisteredUserCheck->GetValue();

	m_FullNameLabel->Show( !registered );
	m_FullNameEdit->Show( !registered );

	m_ServerLabel->Show( !m_params.m_fHideServerEntry );
	m_ServerEdit->Show( !m_params.m_fHideServerEntry );
	m_ScreenLabel->Show( registered );
	m_ScreenCombo->Show( registered );
	m_PasswordLabel->Show( registered );
	m_PasswordEdit->Show( registered );

	// Always have a PIN
    m_MeetingPinLabel->Show( registered );
    m_AnonPinLabel->Show( !registered );

	m_PhoneLabel->Show( !registered );
	m_PhoneEdit->Show( !registered );
	m_EmailLabel->Show( !registered );
	m_EmailEdit->Show( !registered );

	m_SavePasswordCheck->Show( registered );
	m_AutoSignOnCheck->Show( registered );
	m_KeepTryingCheck->Show( registered );

    Resize();
}

void SignOnDlg::Resize()
{
#ifdef __WXGTK__
    // A simple Fit doesn't work under wxGTK--the dialog may or may not be resized appropriately, and child
    // controls often end up overlayed.  This approach seems to work.
    int x,y;
    GetPosition(&x, &y);
    wxSize minSize = GetSizer()->GetMinSize( );
    printf("Min size %d, %d\n", minSize.GetWidth(), minSize.GetHeight());
    SetMinSize(minSize);
    DoSetSize(x, y, minSize.GetWidth(), minSize.GetHeight(), 0);
#else
    GetSizer()->Fit(this);
#endif
}

void SignOnDlg::OnSize(wxSizeEvent& event)
{
#ifdef __WXGTK__
    //printf("Resizing to %d,%d\n", event.GetSize().GetWidth(), event.GetSize().GetHeight());
    Layout( );
#endif
    event.Skip();
}

bool SignOnDlg::Validate()
{
    bool ok = true;

    if (!m_params.m_fTestConnection)
    {
        if ( !m_RegisteredUserCheck->GetValue() )
        {
            if ( m_MeetingPinEdit->IsEmpty() )
            {
                wxMessageDialog msg(this, _("You must enter a PIN number.  Please enter the PIN from your meeting invitation to continue."),
                        _("Sign On error"),
                        wxOK|wxICON_ERROR);
                msg.ShowModal();

                ok = false;
            }
           
            if ( m_FullNameEdit->IsEmpty() )
            {
                wxMessageDialog msg(this, _("You must enter a name.  Please enter your name as you would like it to appear to other meeting participants."),
                        _("Sign On error"),
                        wxOK|wxICON_ERROR);
                msg.ShowModal();

                ok = false;
            }
        }
        else
        {
            if ( m_ScreenCombo->GetValue().IsEmpty() || m_PasswordEdit->IsEmpty() )
            {
                wxMessageDialog msg(this, _("You must enter a screen name and password to continue."),
                        _("Sign On error"),
                        wxOK|wxICON_ERROR);
                msg.ShowModal();

                ok = false;
            }
        }
        
        wxString pin = m_MeetingPinEdit->GetValue();
        for (unsigned i = 0; i < pin.Len(); i++)
        {
            if ((pin[i] < wxT('0') || pin[i] > wxT('9')) && pin[i] != wxT(' '))
            {
                wxMessageDialog msg(this, _("The PIN number should only contain numbers."),
                                    _("Sign On error"),
                                    wxOK|wxICON_ERROR);
                msg.ShowModal();
                
                ok = false;
                break;
            }
        }
    }

    return ok;
}


void SignOnDlg::UpdateCurrentScreenName( const wxString &newSN )
{
    wxString    strT;
    int         iFirstPart  = -1;
    int         newLen      = newSN.Len( );
    SN_t        SN_tT       = NULL;
    if( newLen > 0  &&  newSN.CmpNoCase( m_strPriorSN ) != 0 )
    {
        int         i;
        for( i=0; i<(int)m_arrSN_t.GetCount( ); i++ )
        {
            SN_tT = m_arrSN_t.Item( i );
            if( newSN.CmpNoCase( SN_tT->strSN )  ==  0 )
            {
                //  We have an exact match - fill in password and return
                strT = SN_tT->strPass;
                m_PasswordEdit->SetValue( strT );
                m_SavePasswordCheck->SetValue( SN_tT->fSavePass );
                m_AutoSignOnCheck->SetValue( SN_tT->fAutoLogin );
                m_strPriorSN.Empty( );
                m_strPriorSN.Append( newSN );
                return;
            }
            if( iFirstPart == -1  &&  SN_tT->strSN.StartsWith( newSN.wc_str( wxConvUTF8 ) ) )
            {
                iFirstPart = i;
            }
        }
    }
    if( iFirstPart  ==  -1 )
    {
        m_PasswordEdit->Clear( );
        m_strPriorSN.Empty( );
        m_strPriorSN.Append( newSN );
    }
    else
    {
        //  We have a partial match - display password for first SN that is a candidate and display SN, selecting all text after current text insertion point
        SN_tT = m_arrSN_t.Item( iFirstPart );
        if( SN_tT )
        {
            m_ScreenCombo->SetSelection( iFirstPart );
            strT = SN_tT->strSN;
            m_ScreenCombo->SetSelection( newSN.Len( ), strT.Len( ) );
            strT = SN_tT->strPass;
            m_PasswordEdit->SetValue( strT );
            m_SavePasswordCheck->SetValue( SN_tT->fSavePass );
            m_AutoSignOnCheck->SetValue( SN_tT->fAutoLogin );
        }
        m_strPriorSN.Empty( );
        m_strPriorSN.Append( newSN );
    }
}


void SignOnDlg::OnScreenComboBox( wxCommandEvent &event )
{
    //  The selected line in the dropdown just changed...
    //  BUT this also triggers OnScreenText( ), so let it handle it...
}


void SignOnDlg::OnScreenText( wxCommandEvent &event )
{
    //  The text in the combobox just changed...
    wxString    newSN       = m_ScreenCombo->GetValue( );
    UpdateCurrentScreenName( newSN );
}

void SignOnDlg::OnInit(wxInitDialogEvent& event)
{
    CUtils::StealFocus(this);
    if (m_params.m_fAnonymousUser)
        m_FullNameEdit->SetFocus();
    else
        m_ScreenCombo->SetFocus();
}

wxString SignOnDlg::ReadOption(wxString file, wxString search)
{
    wxString out;
    
    wxFileInputStream ins(file);
    wxTextInputStream ints(ins);
    ints.SetStringSeparators(wxString(wxT(",")));

    search = wxT("\"") + search + wxT("\"");
    
    while (ins.IsOk() && !ins.Eof())
    {
        wxString prop = ints.ReadWord().Trim().Trim(false);
        wxString val = ints.ReadWord().Trim().Trim(false);
     
        if (prop == search)
        {
            out = val.AfterFirst(wxT('"'));
            out = out.BeforeLast(wxT('"'));
            break;
        }
    }
    
    return out;
}

// See if user has a saved Zon screenname/password and import
bool SignOnDlg::CheckZonPassword()
{
#ifdef WIN32
    wxString docDir = wxStandardPaths::Get().GetDocumentsDir() + wxT("\\imidio");
    wxString sysOpt = docDir + wxT("\\IIC.opt");
    wxString lastUser;
    wxString password;
    wxString savePassword;

    lastUser = ReadOption(sysOpt, wxT("LastUser"));

    if (!lastUser.IsEmpty())
    {
        wxLogDebug(wxT("Zon last user: %s"), lastUser.c_str());

        password = ReadOption(docDir + wxT("\\") + lastUser + wxT(".opt"), wxT("SignonAuth"));

        CBase64 base64;
        
        char encrypted[512];
        base64.Decode(password.mb_str(), encrypted);
        
        char decrypted[512];
        CBlowFish   blowfish;
        blowfish.Initialize( BLOWFISH_KEY, sizeof(BLOWFISH_KEY) );
        blowfish.Decode( (unsigned char*) encrypted, (unsigned char*) decrypted, sizeof(decrypted)-1 );
        
        password = wxString(decrypted, wxConvUTF8);
        
        savePassword = ReadOption(docDir + wxT("\\") + lastUser + wxT(".opt"), wxT("SignonSavePassword"));

        m_ScreenCombo->SetValue( lastUser );
        if ( !password.IsEmpty() && savePassword )
        {
            m_PasswordEdit->SetValue( password );
            m_SavePasswordCheck->SetValue(true);
        }

        wxLogDebug(wxT("Zon password: %s, save: %s"), password.c_str(), savePassword.c_str());
        return true;
    }
#endif       
    return false;
}

void SignOnDlg::OnTest(wxMouseEvent& event)
{
    if (event.AltDown()) 
    {
        wxString msg = _("Selecting this command with the ALT key pressed submits your log files for analysis.  Wou you like to continue?");
        int stat = ::wxMessageBox(msg, _("Meeting Center"), wxICON_INFORMATION|wxYES_NO);
        if (stat != wxYES)
            return;

        LogUpload upload;
        upload.UploadLogs(this);
    }
    else 
    {
        m_params.m_fTestConnection = true;
        EndModal(wxID_OK);
    }
}
