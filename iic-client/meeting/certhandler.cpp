
#include "app.h"
#include "certhandler.h"
#include <wx/ffile.h>

CertHandler CertHandler::s_handler;

CertHandler::CertHandler()
{
    //ctor
}

CertHandler::~CertHandler()
{
    //dtor
}

void CertHandler::Initialize(const wxString &caFile)
{
    m_caFile = caFile;
}

void CertHandler::AddCertFile(const wxString &filename)
{
    m_files.Add(filename);
}

void CertHandler::AddCertToFile(const wxString &cert, int fileIdx)
{
    wxString strAcceptedCA = m_files[fileIdx];

    // Add new certificate to our cache
    wxFFile out(strAcceptedCA.c_str(), wxT("a"));
    out.SeekEnd();
    out.Write(cert.mb_str(wxConvUTF8), cert.Len());
    out.Close();
}

void CertHandler::AddTempCert(const wxString &cert)
{
    m_temps.Add(cert);
}

bool CertHandler::WriteCerts()
{
    wxFFile out(m_caFile, wxT("w"));

    for (int i=0; i<m_files.GetCount(); i++)
    {
        wxString buff;

        wxFFile in(m_files[i], wxT("r"));

        if (in.ReadAll(&buff))
        {
            out.Write(buff);
        }
        else
        {
            wxLogDebug(wxT("Error reading certificate from %s"), m_files[i].c_str());
        }
    }

    for (int i=0; i<m_temps.GetCount(); i++)
    {
        out.Write(m_temps[i]);
    }

    out.Close();
    return true;
}
