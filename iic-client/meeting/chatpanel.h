/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef CHATPANEL_H
#define CHATPANEL_H

//(*Headers(ChatPanel)
#include <wx/stattext.h>
#include <wx/panel.h>
#include <wx/combobox.h>
//*)
#include <wx/bmpbuttn.h>
#include <wx/textctrl.h>
//#include <wx/richtext/richtextstyles.h>
//#include <wx/richtext/richtextctrl.h>

//#include "controls/wx/things/toggle.h"
#include "controls/wx/wxTextCtrlEx.h"
#include "controls/wx/wxTogBmpBtn.h"

#include "events.h"
#include "controller.h"

class MeetingMainFrame;
class ChatMainFrame;


class ChatPanel: public wxPanel
{
public:

    ChatPanel( wxWindow *parent, wxWindowID id = -1 );
    virtual ~ChatPanel();

public:
    void        StartMeeting( const wxString &strParticipantID, const wxString &strParticipantName, const wxString &strMeetingID, const wxString &strMeetingTitle, bool fJoining = false );
    void        SetParticipantID( const wxString &strParticipantID )    { m_strParticipantID = strParticipantID; }
    void        EndMeeting( );
    void        ConnectionLost( );
    void        ConnectionReconnected( );

    void        StartIM( const wxString &strDisplayName, const wxString &strIMFrom );
    void        DisplayIM( const wxString &strFrom, const wxString& message);

    wxWindow*   GetChatControl()    { return TextCtrlSend; }

    //(*Identifiers(ChatPanel)
    //*)

protected:

    //(*Handlers(ChatPanel)
    void OnSendTextEnter(wxCommandEvent& event);
    void OnTextCtrlSendText(wxCommandEvent& event);
    void OnTextCtrlSendFocus(wxFocusEvent& event);
    void OnTextCtrlSendTextEnter(wxCommandEvent& event);
    void OnTextCtrlSendChar(wxKeyEvent& event);
    //*)

    void OnTextCtrlRecvTextURL( wxTextUrlEvent& event );
    void OnProcessCustom( wxCommandEvent &event );
    void OnInitDialog( wxInitDialogEvent &event );

    void OnClickFore(wxCommandEvent& event);
    void OnClickBack(wxCommandEvent& event);
    void OnClickSmaller(wxCommandEvent& event);
    void OnClickNorm(wxCommandEvent& event);
    void OnClickLarger(wxCommandEvent& event);
    void OnClickBold(wxCommandEvent& event);
    void OnClickItalics(wxCommandEvent& event);
    void OnClickUline(wxCommandEvent& event);
    void OnClickSend(wxCommandEvent& event);
	void OnMeetingEvent(MeetingEvent& event);
    void OnMeetingChatEvent(MeetingChatEvent& event);

    //(*Declarations(ChatPanel)
    wxTogBmpBtn* m_buttonSend;
    wxTogBmpBtn* m_buttonSetItalics;
    wxTextCtrlEx* TextCtrlSend;
    wxTogBmpBtn* m_buttonSetSmaller;
    wxTextCtrlEx* TextCtrlRecv;
    wxStaticText* StaticText1;
    wxTogBmpBtn* m_buttonSetBold;
    wxTogBmpBtn* m_buttonSetNorm;
    wxTogBmpBtn* m_buttonSetFore;
    wxComboBox* m_chatTargetCombo;
    wxTogBmpBtn* m_buttonSetUline;
    wxTogBmpBtn* m_buttonSetLarger;
    wxTogBmpBtn* m_buttonSetBack;
    //*)

private:
    void InsertCueText();
    void ClearCueText();
    void EnableChats();
    void SelectChatType();
    void SendIMMessage( wxString strMessage );
    void SendMeetingChat( wxString strMessage );

private:
    MeetingMainFrame    *m_pParent;
    ChatMainFrame       *m_pParentChat;

    wxString    m_strParticipantID;
    wxString    m_strParticipantName;
    wxString    m_strMeetingID;
    wxString    m_strMeetingTitle;
    wxString    m_strIMFrom;
    
    bool        m_fSendTextCue;

    participant_t m_participantMe;
    int         m_iRole;
    int         m_iChatMode;

    DECLARE_EVENT_TABLE()
};

#endif
