/* (c) 2009 Michael Tinglof */

#include "app.h"
#include "contactservice.h"
#include "services/common/common.h"

#include <wx/dynlib.h>
#include <wx/stdpaths.h>
#include <wx/thread.h>
#include <algorithm>
#include <functional>

#ifndef WIN32
#define WINAPI
#endif

// Plug-in API -- move into separate header
typedef int  (WINAPI *CONNGETINFO)( char *pC, int iMaxLen, long *lFlags );
typedef int  (WINAPI *CONNINITIALIZE)( const char *userName, const char *configDir );
typedef void (WINAPI *CONNSHUTDOWN)( void );
typedef void (WINAPI *CONNCLEARSTATE)( void );
typedef BOOL (WINAPI *CONNSTARTIMPORT)( );
typedef void (WINAPI *CONNSTOPIMPORT)( );
typedef BOOL (WINAPI *CONNISREADY)( );
typedef BOOL (WINAPI *CONNISCONFIGURED)( );
typedef BOOL (WINAPI *CONNCONFIGUREDIALOG)( void * hwnd );
typedef int  (WINAPI *CONNCONFIGUREITEM)( const char *item );
typedef int  (WINAPI *CONNFETCHFOLDERS)( );
typedef int  (WINAPI *CONNGETFOLDER)( int iIndex, char *pC, int iMax );
typedef int  (WINAPI *CONNGETCONTACTCOUNT)( void );
typedef int  (WINAPI *CONNGETCONTACTINFO)( int, const char *, char *, int maxlen );
typedef int  (WINAPI *CONNGETGROUPCOUNT)( void );
typedef int  (WINAPI *CONNGETGROUPINFO)( int iIndex, int *iGroupSize, char *id, int idlen, char *name, int maxlen );
typedef int  (WINAPI *CONNGETGROUPMEMBER)( int iIndex, int iMember, char *, int maxlen );

#define MAX_PLUGIN_NAME 64


struct ContactPlugIn
{
    wxDynamicLibrary *dynLib;
    wxString name;
    
    CONNINITIALIZE      connInitialize;
    CONNSHUTDOWN        connShutdown;
    CONNSTARTIMPORT     connStartImport;
    CONNSTOPIMPORT      connStopImport;
    CONNISREADY         connIsReady;
    CONNISCONFIGURED    connIsConfigured;
    CONNCONFIGUREDIALOG connConfigureDialog;
    CONNCONFIGUREITEM   connConfigureItem;
    CONNCLEARSTATE      connClearState;
    CONNFETCHFOLDERS    connFetchFolders;
    CONNGETFOLDER       connGetFolder;
    CONNGETCONTACTCOUNT connGetContactCount;
    CONNGETCONTACTINFO  connGetContactInfo;
    CONNGETGROUPCOUNT   connGetGroupCount;
    CONNGETGROUPINFO    connGetGroupInfo;
    CONNGETGROUPMEMBER  connGetGroupMember;
};


class ImportThread : public wxThread
{
    ContactService *m_service;
    
public:
    ImportThread(ContactService *service)
    {
        m_service = service;
    }
    void *Entry()
    {
        m_service->DoImport();
        return NULL;
    }
};


template<class T>
struct find_name : std::binary_function<T*, wxString, bool>
{
public:
    bool operator()(T *obj, wxString name) const
    {
        return obj->name == name;
    }
};


ContactService::ContactService(ContactEvents & handler) : m_handler(handler)
{
    m_fieldCount = 0;
    m_downloadedCount = 0;
    m_running = false;
    m_workingPath = ::wxGetCwd();
}

ContactService::~ContactService()
{
    //dtor
}

void ContactService::SetFields(const char * names[], int count)
{
    m_fieldNames = names;
    m_fieldCount = count;
}

void ContactService::SetBook(addressbk_t book)
{
    m_book = book;
}

void ContactService::SetUser(wxString userName)
{
    m_user = userName;
}

wxString& ContactService::GetLibraryPath()
{
    if (m_libraryPath.IsEmpty())
    {
        wxString    strExePath( wxStandardPaths::Get().GetExecutablePath() );
        wxFileName  wxFN( strExePath );
        m_libraryPath.Append( wxFN.GetPath( ) );
        m_libraryPath.Append( wxT("/connectivity") );
    }
    return m_libraryPath;
}

ContactPlugIn * ContactService::LoadPlugin(const wxString name)
{
    char plugName[256];
    bool success;
    long flags = 0;
    int res = 0;
    wxString userDir = wxStandardPaths::Get().GetUserDataDir();

    PlugInList::iterator iter = std::find_if(m_plugIns.begin(), m_plugIns.end(), std::bind2nd(find_name<ContactPlugIn>(),name));

    if (iter != m_plugIns.end())
        return *iter;

    wxString dllname(GetLibraryPath());
    dllname.Append(wxT("/"));
    dllname.Append(name);

    wxDynamicLibrary *dynLib =  new wxDynamicLibrary();
    dynLib->Load(dllname);

    if (!dynLib->IsLoaded())
        return NULL;

    ContactPlugIn *plugin = new ContactPlugIn;
    plugin->dynLib = dynLib;

    wxDYNLIB_FUNCTION( CONNGETINFO, connGetInfo, *dynLib );
    if (pfnconnGetInfo == NULL)
        goto fail;
        
    pfnconnGetInfo(plugName, sizeof(plugName), &flags);

    plugin->name = wxString::FromUTF8(plugName);
    
#define LOAD_FUNCTION(type, name) \
    plugin->name = (type) dynLib->GetSymbol(wxT(#name), &success); \
    if (!success)       \
        goto fail;

    LOAD_FUNCTION(CONNINITIALIZE, connInitialize)
    LOAD_FUNCTION(CONNSHUTDOWN, connShutdown)
    LOAD_FUNCTION(CONNSTARTIMPORT, connStartImport)
    LOAD_FUNCTION(CONNSTOPIMPORT, connStopImport)
    LOAD_FUNCTION(CONNISREADY, connIsReady)
    LOAD_FUNCTION(CONNISCONFIGURED, connIsConfigured)
    LOAD_FUNCTION(CONNCONFIGUREDIALOG, connConfigureDialog)
    LOAD_FUNCTION(CONNCONFIGUREITEM, connConfigureItem)
    LOAD_FUNCTION(CONNCLEARSTATE, connClearState)
    LOAD_FUNCTION(CONNFETCHFOLDERS, connFetchFolders)
    LOAD_FUNCTION(CONNGETFOLDER, connGetFolder)
    LOAD_FUNCTION(CONNGETCONTACTCOUNT, connGetContactCount)
    LOAD_FUNCTION(CONNGETCONTACTINFO, connGetContactInfo)
    LOAD_FUNCTION(CONNGETGROUPCOUNT, connGetGroupCount);
    LOAD_FUNCTION(CONNGETGROUPINFO, connGetGroupInfo);
    LOAD_FUNCTION(CONNGETGROUPMEMBER, connGetGroupMember);

    res = plugin->connInitialize(m_user.mb_str(), userDir.mb_str());
    if (res)
    {
        wxLogDebug(wxT("Error initializing contact plugin %s: %d"), name.c_str(), res);
        goto fail;
    }

    m_plugIns.push_back(plugin);

    ::wxSetWorkingDirectory(m_workingPath);

    return plugin;
    
fail:
    wxLogDebug(wxT("Failed to load contact plugin %s"), name.c_str());
    delete plugin;
    dynLib->Unload();
    return NULL;    
}

void ContactService::ResetDirectories()
{
    m_directoryNames.clear();
}

void ContactService::ImportDirectory(const wxString name)
{
    DirectoryList::iterator iter = std::find_if(m_directoryNames.begin(), m_directoryNames.end(), std::bind2nd(std::equal_to<wxString>(),name));
    if (iter == m_directoryNames.end())
        m_directoryNames.push_back(name);
}

void ContactService::StartImport()
{
    m_downloadedCount = 0;
    
    ImportThread *thread = new ImportThread(this);
    thread->Create();
    thread->Run();

}

void ContactService::StopImport()
{
    wxLogDebug(wxT("Stopping contact services"));

    PlugInList::iterator iter;
    
    for (iter = m_plugIns.begin(); iter != m_plugIns.end(); iter++)
    {
        (*iter)->connStopImport();
    }    
}

void ContactService::Shutdown()
{
    wxLogDebug(wxT("Shutting down contact services"));

    PlugInList::iterator iter;
    
    for (iter = m_plugIns.begin(); iter != m_plugIns.end(); iter++)
    {
        (*iter)->connShutdown();
    }
}

void ContactService::DoImport()
{
    char userId[1024];
    char data[1024];
    char id[1024];
    int stat;
    
    m_running = true;

    // Give main thread a chance to continue startup 
    wxSleep(1);
    
    DirectoryList::iterator iter;
   
    for (iter = m_directoryNames.begin(); iter != m_directoryNames.end(); iter++)
    {
        wxString name = *iter;
        wxLogDebug(wxT("Importing directory %s"), name.c_str());
        
        ContactPlugIn * plugin = LoadPlugin(name);
        if (plugin == NULL)
        {
            ::wxSetWorkingDirectory(m_workingPath);
            ++m_downloadedCount;
            continue;
        }

/*        if (!plugin->connIsConfigured())
        {
            wxLogDebug(wxT("Directory %s not configured"));
            ++m_downloadedCount;
            continue;
        }
*/        
        
        directory_t dir = directory_store(m_book, plugin->name.mb_str());
        
        m_handler.OnDirectoryStarted(dir);

        plugin->connStartImport();

        int numContacts = plugin->connGetContactCount();
        
        sprintf(userId, "%s:all", (const char *)plugin->name.mb_str());
        group_t all_group = directory_group_store(dir, userId, "All", dtAll);

        // Read contact data into address book.
        for(int i=0; i < numContacts; i++)
        {
            contact_t contact;
            
            stat = plugin->connGetContactInfo(i, "userid", data, sizeof(data)-MAX_PLUGIN_NAME);
            if (stat)
                continue;

            wxLogDebug(wxT("Importing contact %hs"), data);

            sprintf(userId, "%s:%s", (const char *)name.mb_str(), data);

            contact = directory_contact_store(dir, userId);

            // Also add contacts to all group
            directory_group_add_member(all_group, userId);  

            for (int j=0; j < m_fieldCount; j++)
            {
                stat = plugin->connGetContactInfo(i, m_fieldNames[j], data, sizeof(data));
                if (!stat)
                {
                    directory_contact_set_field(contact, m_fieldNames[j], data);
                }
            }
        }

        // Read group data
        int numGroups = plugin->connGetGroupCount();
        for (int i=0; i < numGroups; i++)
        {
            int numMembers = 0;
            
            stat = plugin->connGetGroupInfo(i, &numMembers, id, sizeof(id)-MAX_PLUGIN_NAME, data, sizeof(data));
            if (stat)
                continue;
            
            wxLogDebug(wxT("Importing group %hs"), data);

            sprintf(userId, "%s:%s", (const char *)name.mb_str(), id);
            
            group_t group = directory_group_store(dir, userId, data, dtOther);
            
            for (int j=0; j < numMembers; j++)
            {
                int stat = plugin->connGetGroupMember(i, j, id, sizeof(id)-MAX_PLUGIN_NAME);
                if (!stat)
                {
                    sprintf(userId, "%s:%s", (const char *)name.mb_str(), id);
                    directory_group_add_member(group, userId);
                }
            }
        }

        dir->enabled = 1;
        wxLogDebug(wxT("Downloaded %d contacts from %s"), numContacts, name.c_str());

        m_handler.OnDirectoryReady(dir);

        ::wxSetWorkingDirectory(m_workingPath);      
        ++m_downloadedCount;
    }

    m_running = false;
}

bool ContactService::Configure(const wxString name, wxWindow *parent)
{
    ContactPlugIn * plugin = LoadPlugin(name);
    if (plugin == NULL)
    {
        wxLogDebug(wxT("Plugin for %s not found or loaded"), name.c_str());
        return false;
    }

    bool changed = plugin->connConfigureDialog(parent ? parent->GetHandle(): NULL);
    
    wxLogDebug(wxT("Configured %s"), name.c_str());

    ::wxSetWorkingDirectory(m_workingPath);
    
    return changed;
}

bool ContactService::IsConfigured(const wxString name)
{
    ContactPlugIn * plugin = LoadPlugin(name);
    if (plugin == NULL)
    {
        wxLogDebug(wxT("Plugin for %s not found or loaded"), name.c_str());
        return false;
    }

    bool configed = plugin->connIsConfigured();
    
    ::wxSetWorkingDirectory(m_workingPath);
    
    return configed;
}

bool ContactService::IsImporting()
{
    return m_running;
}

directory_t ContactService::GetDirectoryFirst()
{
    m_dirIter = directory_iterate(m_book);
    return directory_next(m_book, m_dirIter);
}

directory_t ContactService::GetDirectoryNext()
{
    return directory_next(m_book, m_dirIter);    
}

contact_t ContactService::FindContact(const wxString userId)
{
    return addressbk_find_contact(m_book, userId.mb_str(wxConvUTF8));
}
