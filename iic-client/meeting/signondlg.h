/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef SIGNONDLG_H
#define SIGNONDLG_H

//(*Headers(SignOnDlg)
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/checkbox.h>
#include <wx/statbmp.h>
#include <wx/dialog.h>
#include <wx/combobox.h>
//*)
#include <wx/hyperlink.h>

class SignOnParams
{
    public:
    SignOnParams():
    m_fSavePassword(true),
    m_fAutoSignOn(false),
    m_fKeepTryingConnection(true),
    m_fAnonymousUser(false),
    m_fAutoHunt(true),
    m_fEnableSSL(false),
    m_fDebugging(false),
    m_fHideServerEntry(false),
    m_fSignOnCancel(false),
    m_fTestConnection(false)
    {
    }

    SignOnParams & operator = (const SignOnParams & rhs);

    public:
    wxString m_strServer;       /**< Server address (Registered users only) */
    wxString m_strScreen;       /**< Screen name    (Registered users only) */
    wxString m_strPassword;     /**< Password       (Registered users only) */
    wxString m_strPrevScreen;   /**< Previous Screen name - saved when user unchecks registered user */
    wxString m_strPrevPassword; /**< Previous Password    - saved when user unchecks registered user */
    wxString m_strMeetingPIN;   /**< The meeting PIN */
    wxString m_strCommunity;    /**< Name of community superuser is logging into */

    wxString m_strFullName;     /**< Full user name (Unregistered users only) */
    wxString m_strPhone;        /**< Phone number   (Unregistered users only) */
    wxString m_strEmail;        /**< Email address  (Unregistered users only) */

    bool m_fSavePassword;       /**< Flag: Save password (Required if Auto sign on is used) */
    bool m_fAutoSignOn;         /**< Flag: Auto sign on  (Requires Save password) */
    bool m_fKeepTryingConnection; /**< Flag: Keep trying the connection until successful */
    bool m_fAnonymousUser;       /**< Flag: Anonymous user (no password required, no access to contact or meeting info) */

    // Advanced settings
    /////////////////////////////////////////////////////////////////////////////
    /** <<Flag: Automatically hunt for the best connection to the server.
      * If this is false, then the hunt process is skipped and only HTTP is used.
      */
    bool m_fAutoHunt;           /**< Flag: disable port hunting when connecting to server */
    bool m_fEnableSSL;          /**< Use SSL when connecting to server */
    bool m_fDebugging;          /**< Turn on debugging for this session */
    bool m_fHideServerEntry;    /**< Flag: hide server entry field in connection dialog */
    bool m_fSignOnCancel;       /**< Flag: last sign on attempt was cancelled */
    bool m_fTestConnection;     /**< Flag: test connection to server */
};


struct SN_st
{
    wxString    strSN;
    wxString    strPass;
    bool        fSavePass;
    bool        fAutoLogin;
};
typedef struct SN_st * SN_t;

WX_DEFINE_ARRAY_PTR(SN_t, SN_tArray);


class SignOnDlg: public wxDialog
{
	public:

		SignOnDlg(wxWindow* parent = 0,wxWindowID id = -1);
		virtual ~SignOnDlg();

		//(*Identifiers(SignOnDlg)
		//*)

        /**
          * Retrieve the sign on information from the user.<br>
          * This method displays the sign on dialog as a model dialog.<br>
          * The dialog fields are initialized with the parameters (server,screen, and password)<br>
          * If the user clicks OK, then the server, screen, and password paramters are are updated
          * with the user entered data.
          * @param server Reference to the current server address.
          * @param screen Reference to the current screen name.
          * @param password Reference to the current password.
          * @return bool Returns true if the user clicked OK.
          */
        bool GetSignOnInformation( SignOnParams & params );

        void SetPin( wxString& pin );

	protected:
        /** Populate the screen names combo */
        bool PopulateScreenNamesCombo( wxString &strScreen );

        /** Set the registered user Vs. non-registered user screen feedback */
        void SetUIFeedback();

        /** Handle resizing window after option changes */
        void Resize();

        void SetUnregistered();

        bool Validate();
        
        wxString ReadOption(wxString file, wxString search);
        bool CheckZonPassword();

		//(*Handlers(SignOnDlg)
		void OnSavePasswordClick(wxCommandEvent& event);
		void OnAutoSignOnClick(wxCommandEvent& event);
		void OnRegisteredUserClick(wxCommandEvent& event);
		void OnInit(wxInitDialogEvent& event);
		//*)
        void OnSize( wxSizeEvent& event );
        void OnScreenComboBox( wxCommandEvent &event );
        void OnScreenText( wxCommandEvent &event );
        void OnTest( wxMouseEvent &event );

		//(*Declarations(SignOnDlg)
		wxTextCtrl* m_ServerEdit;
		wxStaticText* m_PhoneLabel;
		wxCheckBox* m_SavePasswordCheck;
		wxTextCtrl* m_PasswordEdit;
		wxCheckBox* m_AutoSignOnCheck;
		wxHyperLink* m_TestLink;
		wxTextCtrl* m_EmailEdit;
		wxCheckBox* m_RegisteredUserCheck;
		wxCheckBox* m_KeepTryingCheck;
		wxCheckBox* m_HttpCheck;
		wxComboBox* m_ScreenCombo;
		wxStaticText* m_FullNameLabel;
		wxStaticText* m_AnonPinLabel;
		wxTextCtrl* m_FullNameEdit;
		wxTextCtrl* m_PhoneEdit;
		wxStaticText* m_MeetingPinLabel;
		wxTextCtrl* m_MeetingPinEdit;
		wxStaticText* m_ServerLabel;
		wxStaticText* m_ScreenLabel;
		wxStaticText* m_EmailLabel;
		wxStaticBitmap* m_LogoBitmap;
		wxStaticText* m_PasswordLabel;
		//*)

        wxWindow * m_parent;
        SignOnParams m_params;  /**< The data store for the sign on parameters */
    private:
        void UpdateCurrentScreenName( const wxString &strSN );

        SN_tArray       m_arrSN_t;
        wxString        m_strPriorSN;

        DECLARE_DYNAMIC_CLASS(SignOnDlg)
		DECLARE_EVENT_TABLE()
};

#endif
