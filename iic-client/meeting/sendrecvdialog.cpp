/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
// SendRecvDialog.cpp : implementation file
//

//#include "stdafx.h"
//#include "iic.h"
#include "wx_pch.h"
#include "sendrecvdialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



//BEGIN_MESSAGE_MAP(SendRecvDialog, CDialog)
//    //{{AFX_MSG_MAP(SendRecvDialog)
//ON_WM_TIMER()
//ON_WM_CLOSE()
//ON_WM_DESTROY()
//    //}}AFX_MSG_MAP
//END_MESSAGE_MAP()

BEGIN_EVENT_TABLE(SendRecvDialog,wxDialog)
    //(*EventTable(SendRecvDialog)
    //*)
END_EVENT_TABLE()

/////////////////////////////////////////////////////////////////////////////
// SendRecvDialog dialog


//   IDD_DIALOG_SENDRECV DIALOG  0, 0, 282, 80
//   STYLE DS_SETFONT | DS_MODALFRAME | WS_POPUP | WS_CAPTION | WS_SYSMENU
//   CAPTION "Uploading..."
//   FONT 8, "Tahoma"
//   BEGIN
//      DEFPUSHBUTTON   "Cancel",IDOK,114,61,50,14
//      LTEXT           "File:",IDC_STATIC_LABEL2,  7,7,268,8
//      CONTROL         "Progress2",IDC_PROGRESS2,"msctls_progress32",WS_BORDER, 7,17,268,14
//      LTEXT           "Total:",IDC_STATIC_LABEL,  7,33,162,8
//      CONTROL         "Progress1",IDC_PROGRESS1,"msctls_progress32",WS_BORDER, 7,42,268,14
//   END



//*****************************************************************************
SendRecvDialog::SendRecvDialog( wxWindow *pParent )
: wxDialog( pParent, wxID_ANY, wxString(_T("")) )
{
    m_fCancel = false;

    wxBoxSizer  *sizer  = new wxBoxSizer( wxVERTICAL );
    wxGridSizer *grid   = new wxGridSizer( 2, 2, 3, 3 );

//    m_pStatic1   = new wxStaticText( this, XRCID("IDS_SENDRECV_TEXT1"), _("File:") );
    m_pStatic1   = new wxStaticText( this, wxID_ANY, _("File:") );
    grid->Add( m_pStatic1, 1, wxALL, 5 );
//    m_pGauge1    = wxGauge( this, XRCID("IDS_SENDRECV_GAUGE1"), 100 );
    m_pGauge1    = new wxGauge( this, wxID_ANY, 100 );
    grid->Add( m_pGauge1, 1, wxALL, 5 );
//    m_pStatic2   = new wxStaticText( this, XRCID("IDS_SENDRECV_TEXT2"), _("Total:") );
    m_pStatic2   = new wxStaticText( this, wxID_ANY, _("Total:") );
    grid->Add( m_pStatic2, 1, wxALL, 5 );
//    m_pGauge2    = wxGauge( this, XRCID("IDS_SENDRECV_GAUGE2"), 100 );
    m_pGauge2    = new wxGauge( this, wxID_ANY, 100 );
    grid->Add( m_pGauge2, 1, wxALL, 5 );

    sizer->Add( grid, 1, wxALL, 5 );





    wxStdDialogButtonSizer *btnsizer = new wxStdDialogButtonSizer( );
    wxButton *cancel = NULL;

    cancel = new wxButton( this, wxID_CANCEL );
    btnsizer->AddButton( cancel );
    cancel->SetDefault( );
    cancel->SetFocus( );
    btnsizer->Realize( );
    sizer->Add( btnsizer, wxSizerFlags().Border(wxALL,10).Center() );





//    wxStdDialogButtonSizer  *pBtnSizer   = CreateStdDialogButtonSizer( wxCANCEL );
//    sizer->Add( pBtnSizer, 1, wxALL, 5 );

    this->SetSizer( sizer );
    sizer->Fit( this );

    Show( );

    Connect( wxID_CANCEL, wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&SendRecvDialog::OnCancel, 0, this);
    Connect( wxID_ANY,    wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&SendRecvDialog::OnCancel, 0, this);
}


#if 0
//*****************************************************************************
void SendRecvDialog::DoDataExchange(CDataExchange* pDX)
{
	wxDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(SendRecvDialog)
	DDX_Control(pDX, IDC_PROGRESS2, m_ctlProgress2);
	DDX_Control(pDX, IDC_PROGRESS1, m_ctlProgress);
	//}}AFX_DATA_MAP
}
#endif


//*****************************************************************************
//
// SendRecvDialog message handlers
//
//*****************************************************************************
void SendRecvDialog::OnOK( wxCommandEvent& event )
{
	// TODO: Add extra validation here
	m_fCancel = true;
}


//*****************************************************************************
void SendRecvDialog::OnCancel( wxCommandEvent& event )
{
    // TODO: Add extra validation here
    m_fCancel = true;
}


//*****************************************************************************
void SendRecvDialog::SetTitle( const wxString& aTitle)
{
    SetLabel( aTitle );
}


//*****************************************************************************
void SendRecvDialog::SetLabelTitle2(const wxString& aLabel)
{
    m_pStatic2->SetLabel( aLabel );
}


//*****************************************************************************
void SendRecvDialog::SetLabelTitle(const wxString& aLabel)
{
    m_pStatic1->SetLabel( aLabel );
}


//*****************************************************************************
void SendRecvDialog::SetProgressRange(int aMax)
{
	m_fCancel = false;
	m_pGauge1->SetRange( aMax );
}


//*****************************************************************************
void SendRecvDialog::SetProgressPos(int aPos)
{
    m_pGauge1->SetValue( aPos );
}


//*****************************************************************************
void SendRecvDialog::SetProgressRange2(int aMax)
{
	m_fCancel = false;
    m_pGauge2->SetRange( aMax );
}


//*****************************************************************************
void SendRecvDialog::SetProgressPos2(int aPos)
{
    m_pGauge2->SetValue( aPos );
}


//*****************************************************************************
void SendRecvDialog::StartAutoProgress(int aTimeout)
{
#if 0
    CString     cstrT;
    cstrT.LoadString( IDS_DLG_SENDREC_CONV_WAIT );
	SetDlgItemText(IDC_STATIC_LABEL, cstrT );

	GetDlgItem(IDC_STATIC_LABEL2)->ShowWindow(SW_HIDE);
	m_ctlProgress2.ShowWindow(SW_HIDE);

	m_ctlProgress.SetRange(0, 5);

	SetTimer(1, aTimeout, NULL);
#endif
}


//*****************************************************************************
void SendRecvDialog::StopAutoProgress()
{
#if 0
	KillTimer(1);	// stop the auto pregress
	m_ctlProgress.SetPos(0);
	GetDlgItem(IDC_STATIC_LABEL2)->ShowWindow(SW_SHOW);
	m_ctlProgress2.ShowWindow(SW_SHOW);

    CString     cstrT;
    cstrT.LoadString( IDS_DLG_SENDREC_TOTAL );
    SetDlgItemText(IDC_STATIC_LABEL, cstrT );
#endif
}


#if 0
//*****************************************************************************
void SendRecvDialog::OnTimer(UINT nIDEvent)
{
	int pos = m_ctlProgress.GetPos() + 1;
	if (pos > 5)
		pos = 0;
	m_ctlProgress.SetPos(pos);

    CDialog::OnTimer(nIDEvent);
}
#endif


#if 0
//*****************************************************************************
bool SendRecvDialog::OnInitDialog()
{
//	CDialog::OnInitDialog();

//	CMenu* pMenu = GetSystemMenu(FALSE);
//	if (pMenu)
//	{
//		pMenu->RemoveMenu(pMenu->GetMenuItemCount()-1, MF_BYPOSITION);
//	}

	return true;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
#endif


//*****************************************************************************
void SendRecvDialog::SetCancel( bool fCanCancel)
{
//	GetDlgItem(IDOK)->EnableWindow(fCanCancel);
}


#if 0
//*****************************************************************************
void SendRecvDialog::OnClose()
{
	m_fCancel = true;

//    CDialog::OnClose();
}


//*****************************************************************************
void SendRecvDialog::OnDestroy()
{
//	CDialog::OnDestroy();

	m_fCancel = true;

    Destroy( );
}
#endif
