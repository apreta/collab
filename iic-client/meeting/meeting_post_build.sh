#!/bin/bash

# Copy a few files into place so we can run meeting directly from
# the bin directory.

echo 'Meeting - Meeting Center Controller Post Processing script'
echo '=========================================================='

cp *.xrc ../bin

if [ ! -d ../bin/res ]; then
	mkdir ../bin/res
fi

cp res/* ../bin/res
cp -R locale ../bin/locale

if [ ! -d ../bin/SharedSupport ]; then
	mkdir ../bin/SharedSupport
fi

cp *.pem ../bin/SharedSupport
cp ../bin/zon-def.opt ../bin/SharedSupport

