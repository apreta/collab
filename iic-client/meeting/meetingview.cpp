/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"
#include "meetingview.h"
#include "meetingcentercontroller.h"
#include <wx/statline.h>
#include <wx/html/htmlwin.h>

MeetingView::MeetingView()
{
}

MeetingView::~MeetingView()
{
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_MEETING_FETCHED,wxCommandEventHandler( MeetingView::OnMeetingFetched), 0, this );
}

void MeetingView::Display( wxWindow * parent, const wxString & meeting_id )
{
    // Make sure we are connected
    CONTROLLER.Connect(wxID_ANY, wxEVT_MEETING_FETCHED,wxCommandEventHandler( MeetingView::OnMeetingFetched), 0, this );

    m_parent = parent;
    m_meeting_id = meeting_id;
    CONTROLLER.FetchMeetingDetails( MeetingCenterController::OP_ViewMeeting, m_meeting_id );
}

void MeetingView::OnMeetingFetched(wxCommandEvent& event)
{
    if( !CONTROLLER.IsOpcode( event.GetString(), MeetingCenterController::OP_ViewMeeting ) )
    {
        // This event is not for us
        event.Skip();
        return;
    }

    wxLogDebug( wxT("in MeetingView::OnMeetingFetched( )") );

    wxString strPin;
    wxString    strModerator;
    wxString    strYes( _("yes"), wxConvUTF8 );
    wxString    strNo( _("no"), wxConvUTF8 );


    // Grab the meeting detail pointer
    meeting_details_t mtg = (meeting_details_t)event.GetClientData();

    invitee_iterator_t iter;
    invitee_t invt;

    // Find my pin in this meeting, if any
    iter = meeting_details_iterate(mtg);

    while ((invt = meeting_details_next(mtg, iter)) != NULL)
    {
        wxString userid( invt->user_id, wxConvUTF8 );

        if ( CONTROLLER.GetCurrentUserId() == userid )
        {
            strPin = wxString( invt->participant_id, wxConvUTF8 );
            strModerator.Append( (invt->role == RollModerator) ? strYes : strNo );
            break;
        }
    }

    wxString    strStatusMsg;
    wxString    strHTML;
    CreateReport( mtg, strHTML, m_parent, strPin, strModerator, strStatusMsg );

    meeting_details_destroy( mtg );

    DisplayHTML( strHTML );
}

void MeetingView::GetHostName( meeting_details_t mtg, wxString & strHostName )
{
    invitee_iterator_t iter;
    invitee_t invt;

    iter = meeting_details_iterate(mtg);

    while ((invt = meeting_details_next(mtg, iter)) != NULL)
    {
        wxString contact( invt->name, wxConvUTF8 );
        if( invt->role == RollHost )
        {
            strHostName = wxString( invt->name, wxConvUTF8 );
            break;
        }
    }
}


void MeetingView::CreateReport( meeting_details_t mtg, wxString & strHTML, wxWindow *parent, wxString & strPin, wxString strModerator, wxString strStatusMsg )
{
    wxColor bg( parent->GetBackgroundColour() );

//    wxString WaitingToStart( _("Waiting for meeting to start") );
    wxString Meeting_Details( _("Meeting details") );
    wxString Description( _("Description:") );
    wxString Invite_Message( _("Invite Message:") );


    wxString    strTitle;
    wxString    strHostName;
    wxString    strDialInNone( _("none"), wxConvUTF8 );
    wxString    strDialInNumber;
    bool        fVoiceAllowed   = false;
    wxString    strMeetingID;
    wxString    strPassword;
    wxString    strDateTime;
    wxString    strPrivacy;
    wxString    strDesc;
    wxString    strInvite;
    wxString    strYes( _("yes"), wxConvUTF8 );
    wxString    strNo( _("no"), wxConvUTF8 );


    if( mtg )
    {
        strTitle.Append( wxString( mtg->title, wxConvUTF8 ) );
        GetHostName( mtg, strHostName );
        strDialInNumber.Append( wxString( mtg->bridge_phone, wxConvUTF8 ) );
        fVoiceAllowed = (mtg->options & InvitationDataOnly) ? false : true;
        strMeetingID.Append( wxString( mtg->meeting_id, wxConvUTF8 ) );
        strPassword.Append( wxString( mtg->password, wxConvUTF8 ) );
        strDateTime.Append( wxString( CONTROLLER.DateTimeDecode( mtg->start_time, MeetingCenterController::LongDateTime ), wxConvUTF8 ) );
        strPrivacy.Append( wxString( CONTROLLER.privacyText( mtg->privacy ), wxConvUTF8 ) );
        strDesc.Append( wxString( mtg->description, wxConvUTF8 ) );
        strInvite.Append( wxString( mtg->invite_message, wxConvUTF8 ) );
    }


    HtmlGen r;
    r.clear( false );

    r.startHtml();

    r.image( wxGetApp().GetResourceFile(_("res/Z_OpenSource_n.png")) );
    r.linebreak( );

    if( !strStatusMsg.IsEmpty( ) )
    {
        r.hline( );
        r.h2( strStatusMsg );
        r.hline( );
    }

    r.h3( Meeting_Details );

    //startTable( wxColor & bg, const wxString & width, int border = 0, int cellspacing = 2, int cellpadding = 1 );
    r.startTable( bg, _T("85%") );

    addRow( r, _("Title:"),     strTitle );
    addRow( r, _("Host:"),      strHostName );
//    addRow( r, _("Dial-in #:"), _("none") ); // Todo: Where does this come from ?
    addRow( r, _("Dial-in #:"), (fVoiceAllowed) ? strDialInNumber : strDialInNone );
    addRow( r, _("Meeting ID:"), strMeetingID );
    addRow( r, _("My PIN #:"),  strPin );

    addRow( r, _("Password:"), strPassword.IsEmpty() ? _("none") : _("yes") );
    addRow( r, _("Date/Time:"), strDateTime );
    wxString status;

   	//PLF-TODO: Need the meeting_info_t record for the status:
   	//if( mtg->status == StatusNotStarted ) status = _("Not started");
	//else if( mtg->status == StatusInProgress ) status = _("In progress");
	//else if( mtg->status == StatusCompleted ) status = _("Completed");

	addRow( r, _("Privacy:"), strPrivacy );
	addRow( r, _("Moderator:"), strModerator ); // Is the host always the moderator ??

	//PLF-TODO: Need to call fetch_meeting_for_user() to get this information
	//addRow( r, _("Last Start:"), CONTROLLER.DateTimeDecode( mtg->start_time, MeetingCenterController::LongDateTime ) );
	//addRow( r, _("Last End:"), CONTROLLER.DateTimeDecode( mtg->end_time, MeetingCenterController::LongDateTime ) );

    r.endTable();

    r.h3( Description );
    r.simpletext( strDesc );

    r.h3( Invite_Message );
    r.simpletext( strInvite );
    r.endHtml();

    strHTML = r.buf();
}

void MeetingView::addRow( HtmlGen & r, const wxString & label, const wxString & value )
{
    r.startTableRow();
    r.tableDataCell( label );
    r.tableDataCell( value );
    r.endTableRow();
}

void MeetingView::DisplayHTML( const wxString & strHTML )
{
    wxBoxSizer *topsizer;
    wxHtmlWindow *html;
    wxDialog dlg(m_parent, wxID_ANY, wxString(_("Meeting Information")));
    topsizer = new wxBoxSizer(wxVERTICAL);
    html = new wxHtmlWindow(&dlg, wxID_ANY, wxDefaultPosition,
    wxSize(380, 320), wxHW_SCROLLBAR_NEVER);
    html->SetBorders(0);
    html->SetPage( strHTML );
    // Fit the HTML window to the size of its contents
    html->SetSize(html->GetInternalRepresentation()->GetWidth(),
    html->GetInternalRepresentation()->GetHeight());
    topsizer->Add(html, 1, wxALL, 10);
    topsizer->Add(new wxStaticLine(&dlg, wxID_ANY), 0, wxEXPAND | wxLEFT |
    wxRIGHT, 10);
    wxButton *but = new wxButton(&dlg, wxID_OK, _("OK"));
    but->SetDefault();
    topsizer->Add(but, 0, wxALL | wxALIGN_RIGHT, 15);
    dlg.SetSizer(topsizer);
    topsizer->Fit(&dlg);
#ifdef __WXMAC__
	dlg.Centre();
#endif	
    dlg.ShowModal();
}

