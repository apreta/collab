/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef FINDMEETINGSDIALOG_H
#define FINDMEETINGSDIALOG_H

//(*Headers(FindMeetingsDialog)
#include <wx/button.h>
#include <wx/checkbox.h>
#include <wx/combobox.h>
#include <wx/datectrl.h>
#include <wx/dialog.h>
#include <wx/radiobut.h>
#include <wx/sizer.h>
#include <wx/spinctrl.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
//*)

#include "meetingsearch.h"
#include "presetdata.h"

class FindMeetingsDialog: public wxDialog
{
	public:

		FindMeetingsDialog(wxWindow* parent,wxWindowID id = -1);
		virtual ~FindMeetingsDialog();

		//(*Identifiers(FindMeetingsDialog)
		//*)

        /**
          * Preset the search criteria to be edited.<br>
          * NOTE: You MUST call this method before calling EditSearchCriteria()
          * @param presetName The name of the critera. This will be selected in the combo box.
          * @param criteria The content for the screen.
          */
        void PresetSearchCriteria( const wxString & presetName, const MeetingSearch & criteria );

        /** Get search criteria
          * @return int Returns wxID_OK, wxID_CANCEL, Etc.
          */
        int EditSearchCriteria();

        /** Return the selected search criteria
          */
        const MeetingSearch & GetCriteria()
        {
            return m_Criteria;
        }

	protected:
        /** m_Criteria => GUI */
        void Value_to_GUI();

        /** GUI => m_Criteria */
        void GUI_to_Value();

        /** Setup the UI state to match the current criteria */
        void SetUIFeedback();

		//(*Handlers(FindMeetingsDialog)
		void OnSearchNameComboSelect(wxCommandEvent& event);
		void OnScheduledWithinRadioSelect(wxCommandEvent& event);
		void OnScheduledBetweenRadioSelect(wxCommandEvent& event);
		void OnSearchAllDatesRadioSelect(wxCommandEvent& event);
		void OnMeetingTitleContainsCheckClick(wxCommandEvent& event);
		void OnHostsFirstNameContainsCheckClick(wxCommandEvent& event);
		void OnHostsLastNameContainsCheckClick(wxCommandEvent& event);
		void OnMeetingIdCheckClick(wxCommandEvent& event);
		void OnSaveSearchAsBtnClick(wxCommandEvent& event);
		void OnDeleteSearchBtnClick(wxCommandEvent& event);
		//*)

        void OnClickHelp(wxCommandEvent& event);

		//(*Declarations(FindMeetingsDialog)
		wxStaticText* m_SearchNameLabel;
		wxComboBox* m_SearchNameCombo;
		wxButton* m_SaveSearchAsBtn;
		wxButton* m_DeleteSearchBtn;
		wxRadioButton* m_ScheduledWithinRadio;
		wxComboBox* m_NextOrLastCombo;
		wxSpinCtrl* m_NextOrLastSpin;
		wxRadioButton* m_ScheduledBetweenRadio;
		wxDatePickerCtrl* m_FromDatePicker;
		wxStaticText* m_AndLabel;
		wxDatePickerCtrl* m_ToDatePicker;
		wxRadioButton* m_SearchAllDatesRadio;
		wxCheckBox* m_MeetingTitleContainsCheck;
		wxTextCtrl* m_MeetingTitleContainsEdit;
		wxCheckBox* m_HostsFirstNameContainsCheck;
		wxTextCtrl* m_HostsFirstNameContainsEdit;
		wxCheckBox* m_HostsLastNameContainsCheck;
		wxTextCtrl* m_HostsLastNameContainsEdit;
		wxCheckBox* m_MeetingIdCheck;
		wxTextCtrl* m_MeetingIdEdit;
		wxCheckBox* m_OnlyShowMeetingInProgressCheck;
		wxStaticText* m_NumberOfResultsLabel;
		wxSpinCtrl* m_NumberOfResultsSpin;
		//*)

	private:
        /** The current search criteria */
        MeetingSearch m_Criteria;

        /** The Preset Data processor */
        PresetData m_PresetData;

		DECLARE_EVENT_TABLE()
};

#endif
