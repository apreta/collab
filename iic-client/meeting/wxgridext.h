/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef _WXGRIDEXT_H_
#define _WXGRIDEXT_H_

#include <wx/grid.h>

#include <wx/sheet/sheet.h>
#include <wx/sheet/sheetedt.h>
#include <wx/sheet/sheetren.h>

#include <wx/laywin.h>
#include <wx/htmllbox.h>

#ifdef __WXMSW__
    typedef LRESULT WINAPI (*DefSubclassProcPtr)(HWND,UINT,WPARAM,LPARAM);
    typedef BOOL WINAPI (*SetWindowSubclassPtr)(HWND,SUBCLASSPROC,UINT_PTR,DWORD_PTR);
#endif

class CImageListComposite;
class GridCellComboIntEditor;
class GridCellTextEditor;
class wxImageList;

/**
  * This class processes combo box selections for the GridCellComboIntEditor class.
  */
class GridCellComboIntEditorEvtHandler : public wxEvtHandler
{
public:
    GridCellComboIntEditorEvtHandler();
    void Connect( GridCellComboIntEditor *pEditor );
    /**
      * Process user selections from the combo box<br>
      * If the SetupCustomEntry() method is used, then this selection event looks for the
      * selection of the trigger value. See SetupCustomEntry() for additional details.
      */
    void OnComboBoxSelect( wxCommandEvent & event );

    /**
      * Process text changes from the combo box<br>
      * If the user entered data via an editable combo
      * then the inChoices association is updates as
      * required.
      */
    void OnComboBoxTextUpdated( wxCommandEvent & event );

    GridCellComboIntEditor *m_pEditor;
};

///////////////////////////////////////////////////////////////////////////////
/**
  * This class extends the wxGridCellChoiceEditor by maintaining
  * an array of values that map 1:1 with the string values.<br>
  * The SetupCustomEntry() method also enables a "Enter Xxxx..." entry in the item list
  * to trigger a query for a new choice list item. When the user selects the trigger item
  * from the choice list, a simple dialog is displayed asking the user for a new entry.
  *
  */
class GridCellComboIntEditor : public wxSheetCellChoiceEditorRefData
{
public:
    GridCellComboIntEditor(size_t count = 0, const wxString choices[] = NULL, bool allowOthers = false);
    GridCellComboIntEditor(const wxArrayString& choices, const wxArrayInt & intChoices, bool allowOthers = false);
    virtual void CreateEditor(wxWindow* parent, wxWindowID id,wxEvtHandler* evtHandler, wxSheet* sheet);
    virtual void BeginEdit(const wxSheetCoords& coords, wxSheet* sheet);
    virtual void StartingKey(wxKeyEvent& event);
    virtual bool EndEdit(const wxSheetCoords& coords, wxSheet* sheet);
    virtual void SetSize(const wxRect& rect, const wxSheetCellAttr &attr);
    virtual void Show(bool show, const wxSheetCellAttr &attr);

    const wxArrayString & Choices()
    {
        return m_choices;
    }

    const wxArrayInt & IntChoices()
    {
        return m_intChoices;
    }
    
    /**
      * Remove all choices in combo.
      */
    void Reset();

    /**
      * Append a new choice.<br>
      * If the strValue is already in the current string choices, then intValue is assosiciated
      * with the existing choice. If the strValue is not in the current string choices, then a new
      * string/int pair are appended to the end of the list.
      * @param strValue The string value.
      * @param intValue The associated int value.
      */
    void Append( const wxString & strValue, int intValue );

    /**
      * Return the intValue for the specified string.
      * @param strValue The target value
      * @return int The integer value associated with the string if found. intDefault is returned on strValue is not found.
      */
    int GetIntValue( const wxString & strValue, int intDefault = -1 );

    /**
      * Set the initial value based on it's integer value.
      */
    bool SetValue( int intValue );

    /**
      * Return the start value.
      */
    wxString StartValue()
    {
        return m_startValue;
    }

    void SetCustomValue(wxString & value);

    wxString GetCustomValue();
    
    const wxString & GetCustomEntryCaption()
    {
        return m_strCustomEntryCaption;   /**< The custom entry caption text */
    }

    const wxString & GetCustomEntryPrompt()
    {
        return m_strCustomEntryPrompt;    /**< The custom entry prompt text */
    }
    int GetCustomEntryTrigger()
    {
        return m_nCustomEntryTrigger;          /**< The custom entry trigger value. This is one of the m_intChoices values */
    }
    int GetCustomEntryIntValue()
    {
        return m_nCustomEntryIntValue;         /**< The custom entry integer value. */
    }
    wxWindow * GetParentCustomEntry()
    {
        return m_parentCustomEntry;      /**< The parent window for the custom entry dialog */
    }
    wxSheet * Grid()
    {
        return m_grid;
    }
    const wxSheetCoords & GetCoords()
    {
        return m_coords;
    }

    /**
      * Setup the data for a custom entry dialog.<br>
      * The custom entry dialog is a data entry dialog that is triggered when the users selects
      * an entry from the combo box.<br>
      * Custom entry process:<br>
      * The user selects the Custom Entry Trigger item from the combo box.<br>
      * The Custom Entry Dialog is displayed.<br>
      * The user enters the requested text.<br>
      * The new text is added to the combo box and associated with the Custom Entry Int Value.<br>
      */
    void SetupCustomEntry( wxWindow *parent, const wxString & caption, const wxString & prompt, int nTrigger, int nValue );

    /**
      * Process user selections from the combo box<br>
      * If the SetupCustomEntry() method is used, then this selection event looks for the
      * selection of the trigger value. See SetupCustomEntry() for additional details.
      */
    void OnComboBoxSelect( wxCommandEvent & event );


    bool Copy(const GridCellComboIntEditor& other);
    DECLARE_SHEETOBJREFDATA_COPY_CLASS(GridCellComboIntEditor,
                                       wxSheetCellChoiceEditorRefData)

private:
    wxSheetCoords m_coords;
    wxSheet *m_grid;

private:
    wxArrayInt m_intChoices;            /**< The array of int values that map to the string choices */
    wxWindow *m_parentCustomEntry;      /**< The parent window for the custom entry dialog */
    wxString m_strCustomEntryCaption;   /**< The custom entry caption text */
    wxString m_strCustomEntryPrompt;    /**< The custom entry prompt text */
    int m_nCustomEntryTrigger;          /**< The custom entry trigger value. This is one of the m_intChoices values */
    int m_nCustomEntryIntValue;         /**< The custom entry integer value. */
    wxPoint m_pointActivate;            /**< The coordinates the user clicked on when the editor was activated */
    GridCellComboIntEditorEvtHandler m_comboHandler;
};

//////////////////////////////////////////////////////////////////////////////////////////////
typedef wxListBox GridCellTextPopup;    // also trying wxSimpleHtmlListBox, but can't get border to display

class GridCellTextEditorEvtHandler : public wxEvtHandler
{
public:
    GridCellTextEditorEvtHandler();
    void Connect(GridCellTextEditor *editor, wxSheet *sheet, GridCellTextPopup *list);

    void OnListChar(wxKeyEvent& event);
    void OnListSelected(wxCommandEvent& event);
    void OnTimer(wxTimerEvent& event);
    
    GridCellTextEditor *m_pEditor;
    GridCellTextPopup *m_autoList;
    wxSheet *m_sheet;
};

/**
 * Custom text entry for wxSheet implementing autocomplete.
 */
class GridCellTextEditor : public wxSheetCellTextEditorRefData
{
    const wxArrayString* m_autoNames;
    GridCellTextPopup *m_autoList;

public:
    GridCellTextEditor() : m_autoNames(NULL), m_autoList(NULL), m_timer(&m_listHandler) {}

    virtual void CreateEditor(wxWindow* parent, wxWindowID id,
                              wxEvtHandler* evtHandler, wxSheet* sheet);

    virtual void DestroyControl();
    virtual void BeginEdit(const wxSheetCoords& coords, wxSheet* sheet);
    virtual bool EndEdit(const wxSheetCoords& coords, wxSheet* sheet);
    virtual bool OnKeyDown(wxKeyEvent& event);
    virtual bool OnChar(wxKeyEvent& event);
/*
    virtual void ResetValue();
    virtual void StartingKey(wxKeyEvent& event);
    virtual void HandleReturn(wxKeyEvent& event);
    virtual bool OnChar(wxKeyEvent& event);
*/
    bool Copy(const GridCellTextEditor& other)
        { return wxSheetCellTextEditorRefData::Copy(other); }
    DECLARE_SHEETOBJREFDATA_COPY_CLASS(GridCellTextEditor,
                                       wxSheetCellTextEditorRefData)

    void AutoComplete(const wxArrayString& choices);
    void ShowAutoList();
    void DestroyAutoList();
    void FillMatches(const wxString& value);

#ifdef __WXMSW__
    static LRESULT CALLBACK TextEditSubClass(HWND hWnd, UINT uMsg, WPARAM wParam,
                                             LPARAM lParam, UINT_PTR uIdSubclass, DWORD_PTR dwRefData);

    static bool m_LoadedFuncs;
    static DefSubclassProcPtr m_DefSubclassProc;
    static SetWindowSubclassPtr m_SetWindowSubclass;
#endif

    GridCellTextEditorEvtHandler m_listHandler;
    wxTimer m_timer;
};


//////////////////////////////////////////////////////////////////////////////////////////////
class GridCellBoolEditor : public wxSheetCellBoolEditorRefData
{
public:
    void CreateEditor(wxWindow* parent, wxWindowID id,
                              wxEvtHandler* evtHandler, wxSheet* sheet);

    bool Copy(const GridCellBoolEditor& other)
        { return wxSheetCellBoolEditorRefData::Copy(other); }
    DECLARE_SHEETOBJREFDATA_COPY_CLASS(GridCellBoolEditor,
                                       wxSheetCellBoolEditorRefData)
};

//////////////////////////////////////////////////////////////////////////////////////////////
class GridCellImageRenderer : public wxSheetCellBitmapRendererRefData
{
public:
    GridCellImageRenderer() {};

    GridCellImageRenderer(CImageListComposite * pImageList, int align = 0 );

    /**
      * Selects the correct image from the source and sets the resulting
      * bitmap. Rendering is passed on to the base class.
      */
    virtual void Draw(wxSheet& sheet, const wxSheetCellAttr& attr,
                      wxDC& dc, const wxRect& rect,
                      const wxSheetCoords& coords, bool isSelected);

    /**
      * Returns the size of the image.
      */
    virtual wxSize GetBestSize(wxSheet& sheet, const wxSheetCellAttr& attr,
                               wxDC& dc, const wxSheetCoords& coords);


    bool Copy(const GridCellImageRenderer& other)
    {
         m_pImageListSrc = other.m_pImageListSrc;
         m_pImageList = other.m_pImageList;
         return wxSheetCellBitmapRendererRefData::Copy(other);
    }
    DECLARE_SHEETOBJREFDATA_COPY_CLASS(GridCellImageRenderer,wxSheetCellBitmapRendererRefData)

    /**
      * Set image to display from associated image list.
      */
    void SetImage(int image);

private:
    /**< Pointer to the image list */
    CImageListComposite *m_pImageListSrc;

    /**< Pointer to the active image list */
    wxImageList * m_pImageList;

    /**< Current status image to display */
    int m_nImage;
};


#endif
