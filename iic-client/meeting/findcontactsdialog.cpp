/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "findcontactsdialog.h"

//(*InternalHeaders(FindContactsDialog)
#include <wx/bitmap.h>
#include <wx/font.h>
#include <wx/fontenum.h>
#include <wx/fontmap.h>
#include <wx/image.h>
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(FindContactsDialog)
//*)

BEGIN_EVENT_TABLE(FindContactsDialog,wxDialog)
	//(*EventTable(FindContactsDialog)
	//*)
END_EVENT_TABLE()

FindContactsDialog::FindContactsDialog(wxWindow* parent,wxWindowID id):
m_PresetData("FindContacts")
{
	//(*Initialize(FindContactsDialog)
	wxXmlResource::Get()->LoadObject(this,parent,_T("FindContactsDialog"),_T("wxDialog"));
	m_SearchNameLabel = (wxStaticText*)FindWindow(XRCID("ID_SEARCHNAME_LABEL"));
	m_SearchNameCombo = (wxComboBox*)FindWindow(XRCID("ID_SEARCHNAME_COMBO"));
	m_SaveSearchAsBtn = (wxButton*)FindWindow(XRCID("ID_SAVESEARCHAS_BTN"));
	m_DeleteSearchBtn = (wxButton*)FindWindow(XRCID("ID_DELETESEARCH_BTN"));
	m_FirstNameContainsCheck = (wxCheckBox*)FindWindow(XRCID("ID_FIRSTNAMECONTAINS_CHECK"));
	m_FirstNameContainsEdit = (wxTextCtrl*)FindWindow(XRCID("ID_FIRSTNAMECONTAINS_EDIT"));
	m_LastNameContainsCheck = (wxCheckBox*)FindWindow(XRCID("ID_LASTNAMECONTAINS_CHECK"));
	m_LastNameContainsEdit = (wxTextCtrl*)FindWindow(XRCID("ID_LASTNAMECONTAINS_EDIT"));
	m_EmailAddressesContainsCheck = (wxCheckBox*)FindWindow(XRCID("ID_EMAILADDRESSESCONTAINS_CHECK"));
	m_EmailAddressesContainEdit = (wxTextCtrl*)FindWindow(XRCID("ID_EMAILADDRESSESCONTAIN_EDIT"));
	m_PhoneNumbersContainCheck = (wxCheckBox*)FindWindow(XRCID("ID_PHONENUMBERSCONTAIN_CHECK"));
	m_PhoneNumbersContainEdit = (wxTextCtrl*)FindWindow(XRCID("ID_PHONENUMBERSCONTAIN_EDIT"));
	m_More1Check = (wxCheckBox*)FindWindow(XRCID("ID_MORE1_CHECK"));
	m_More1Combo = (wxComboBox*)FindWindow(XRCID("ID_MORE1_COMBO"));
	m_More1Edit = (wxTextCtrl*)FindWindow(XRCID("ID_MORE1_EDIT"));
	m_More2Check = (wxCheckBox*)FindWindow(XRCID("ID_MORE2_CHECK"));
	m_More2Combo = (wxComboBox*)FindWindow(XRCID("ID_MORE2_COMBO"));
	m_More2Edit = (wxTextCtrl*)FindWindow(XRCID("ID_MORE2_EDIT"));
	m_More3Check = (wxCheckBox*)FindWindow(XRCID("ID_MORE3_CHECK"));
	m_More3Combo = (wxComboBox*)FindWindow(XRCID("ID_MORE3_COMBO"));
	m_More3Edit = (wxTextCtrl*)FindWindow(XRCID("ID_MORE3_EDIT"));
	m_SearchSelectedGroupsLabel = (wxStaticText*)FindWindow(XRCID("ID_SEARCHSELECTEDGROUPS_LABEL"));
	m_SearchSelectedGroupsCombo = (wxComboBox*)FindWindow(XRCID("ID_SEARCHSELECTEDGROUPS_COMBO"));
	m_NumberOfResultsLabel = (wxStaticText*)FindWindow(XRCID("ID_NUMBEROFRESULTS_LABEL"));
	m_NumberOfResultsSpin = (wxSpinCtrl*)FindWindow(XRCID("ID_NUMBEROFRESULTS_SPIN"));
	Connect(XRCID("ID_SEARCHNAME_COMBO"),wxEVT_COMMAND_COMBOBOX_SELECTED,(wxObjectEventFunction)&FindContactsDialog::OnSearchNameComboSelect);
	Connect(XRCID("ID_SAVESEARCHAS_BTN"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&FindContactsDialog::OnSaveSearchAsBtnClick);
	Connect(XRCID("ID_DELETESEARCH_BTN"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&FindContactsDialog::OnDeleteSearchBtnClick);
	Connect(XRCID("ID_FIRSTNAMECONTAINS_CHECK"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&FindContactsDialog::OnFirstNameContainsCheckClick);
	Connect(XRCID("ID_LASTNAMECONTAINS_CHECK"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&FindContactsDialog::OnLastNameContainsCheckClick);
	Connect(XRCID("ID_EMAILADDRESSESCONTAINS_CHECK"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&FindContactsDialog::OnEmailAddressesContainsCheckClick);
	Connect(XRCID("ID_PHONENUMBERSCONTAIN_CHECK"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&FindContactsDialog::OnPhoneNumbersContainCheckClick);
	Connect(XRCID("ID_MORE1_CHECK"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&FindContactsDialog::OnMore1CheckClick);
	Connect(XRCID("ID_MORE2_CHECK"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&FindContactsDialog::OnMore2CheckClick);
	Connect(XRCID("ID_MORE3_CHECK"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&FindContactsDialog::OnMore3CheckClick);
	//*)

	m_Criteria.PopulateDatabaseFieldCombo( m_More1Combo );
	m_Criteria.PopulateDatabaseFieldCombo( m_More2Combo );
	m_Criteria.PopulateDatabaseFieldCombo( m_More3Combo );

	m_SearchSelectedGroupsCombo->Append( _("None") );
	m_SearchSelectedGroupsCombo->Append( _("Global") );
	m_SearchSelectedGroupsCombo->Append( _("Personal") );
	m_SearchSelectedGroupsCombo->Append( _("All") );

    m_PresetData.Load( );
    m_PresetData.PopulateComboBox( m_SearchNameCombo );

    // Locate the OK button.
    wxWindow *pOKWindow = wxWindow::FindWindowById( wxID_OK, this );
    if( pOKWindow )
    {
        wxButton *pOKButton = wxDynamicCast( pOKWindow, wxButton );
        if( pOKButton )
        {
            pOKButton->SetDefault();
        }
    }
	
#ifdef __WXMAC__
	Centre();
#endif

}

FindContactsDialog::~FindContactsDialog()
{
	//(*Destroy(FindContactsDialog)
	//*)
}

void FindContactsDialog::PresetSearchCriteria( const wxString & presetName, const ContactSearch & criteria )
{
    m_SearchNameCombo->SetStringSelection( presetName );
    m_Criteria = criteria;
    m_Criteria.m_strSearchName = presetName;
}

int FindContactsDialog::EditSearchCriteria( bool fModal )
{
    int nRet = wxID_CANCEL;

    Value_to_GUI();

    if( !fModal )
    {
        Show();
    }else
    {
        nRet = ShowModal();
    }
    return nRet;
}

void FindContactsDialog::SaveSearchCriteria()
{
    GUI_to_Value();
    // There is no UNDO on the changes to the search criteria
    // so let's pretend the user meant what they did
    m_PresetData.Save();
}

void FindContactsDialog::Value_to_GUI()
{
    m_FirstNameContainsCheck->SetValue( m_Criteria.m_fFirstName );
    m_FirstNameContainsEdit->SetValue( m_Criteria.m_strFirstName );

    m_LastNameContainsCheck->SetValue( m_Criteria.m_fLastName );
    m_LastNameContainsEdit->SetValue( m_Criteria.m_strLastName );

    m_EmailAddressesContainsCheck->SetValue( m_Criteria.m_fEmailAddresses );
    m_EmailAddressesContainEdit->SetValue( m_Criteria.m_strEmailAddress );

    m_PhoneNumbersContainCheck->SetValue( m_Criteria.m_fPhoneNumbers );
    m_PhoneNumbersContainEdit->SetValue( m_Criteria.m_strPhoneNumbers );

    m_More1Check->SetValue( m_Criteria.m_fMore1 );
    m_More2Check->SetValue( m_Criteria.m_fMore2 );
    m_More3Check->SetValue( m_Criteria.m_fMore3 );

    m_More1Combo->SetStringSelection( m_Criteria.DatabaseDisplayField( m_Criteria.m_nField1 ) );
    m_More2Combo->SetStringSelection( m_Criteria.DatabaseDisplayField( m_Criteria.m_nField2 ) );
    m_More3Combo->SetStringSelection( m_Criteria.DatabaseDisplayField( m_Criteria.m_nField3 ) );

    m_More1Edit->SetValue( m_Criteria.m_strField1Text );
    m_More2Edit->SetValue( m_Criteria.m_strField2Text );
    m_More3Edit->SetValue( m_Criteria.m_strField3Text );

    if( m_Criteria.m_nSelectedGroups == 0 )
    {
        m_SearchSelectedGroupsCombo->SetSelection(0);
    }else if( m_Criteria.m_nSelectedGroups == ContactSearch::Groups_Global )
    {
        m_SearchSelectedGroupsCombo->SetSelection(1);
    }else if( m_Criteria.m_nSelectedGroups == ContactSearch::Groups_Personal )
    {
        m_SearchSelectedGroupsCombo->SetSelection(2);
    }else
    {
        m_SearchSelectedGroupsCombo->SetSelection(3);
    }

    m_NumberOfResultsSpin->SetValue( m_Criteria.m_nMaxResults );

    SetUIFeedback();
}

void FindContactsDialog::SetUIFeedback()
{
    m_FirstNameContainsEdit->Enable( m_Criteria.m_fFirstName );
    m_LastNameContainsEdit->Enable( m_Criteria.m_fLastName );
    m_EmailAddressesContainEdit->Enable( m_Criteria.m_fEmailAddresses );
    m_PhoneNumbersContainEdit->Enable( m_Criteria.m_fPhoneNumbers );

    m_More1Combo->Enable( m_Criteria.m_fMore1 );
    m_More1Edit->Enable( m_Criteria.m_fMore1 );

    m_More2Combo->Enable( m_Criteria.m_fMore2 );
    m_More2Edit->Enable( m_Criteria.m_fMore2 );

    m_More3Combo->Enable( m_Criteria.m_fMore3 );
    m_More3Edit->Enable( m_Criteria.m_fMore3 );

}

void FindContactsDialog::GUI_to_Value()
{
    m_Criteria.m_fFirstName = m_FirstNameContainsCheck->GetValue();
    m_Criteria.m_strFirstName = m_FirstNameContainsEdit->GetValue();

    m_Criteria.m_fLastName = m_LastNameContainsCheck->GetValue();
    m_Criteria.m_strLastName = m_LastNameContainsEdit->GetValue();

    m_Criteria.m_fEmailAddresses = m_EmailAddressesContainsCheck->GetValue();
    m_Criteria.m_strEmailAddress = m_EmailAddressesContainEdit->GetValue();

    m_Criteria.m_fPhoneNumbers = m_PhoneNumbersContainCheck->GetValue();
    m_Criteria.m_strPhoneNumbers = m_PhoneNumbersContainEdit->GetValue();

    m_Criteria.m_fMore1 = m_More1Check->GetValue();
    m_Criteria.m_fMore2 = m_More2Check->GetValue();
    m_Criteria.m_fMore3 = m_More3Check->GetValue();

    m_Criteria.m_nField1 = m_More1Combo->GetSelection();
    m_Criteria.m_nField2 = m_More2Combo->GetSelection();
    m_Criteria.m_nField3 = m_More3Combo->GetSelection();

    m_Criteria.m_strField1Text = m_More1Edit->GetValue();
    m_Criteria.m_strField2Text = m_More2Edit->GetValue();
    m_Criteria.m_strField3Text = m_More3Edit->GetValue();

    int nGroup = m_SearchSelectedGroupsCombo->GetSelection();
    if( nGroup == 0 )
        m_Criteria.m_nSelectedGroups = ContactSearch::Groups_None;
    else if( nGroup == 1 )
        m_Criteria.m_nSelectedGroups = ContactSearch::Groups_Global;
    else if( nGroup == 2 )
        m_Criteria.m_nSelectedGroups = ContactSearch::Groups_Personal;
    else
        m_Criteria.m_nSelectedGroups = ContactSearch::Groups_All;

    m_Criteria.m_nMaxResults = m_NumberOfResultsSpin->GetValue();
}


void FindContactsDialog::OnSearchNameComboSelect(wxCommandEvent& event)
{
    wxArrayString newData;
    if( m_PresetData.HandleNewSelection( m_SearchNameCombo, newData ) )
    {
        // Retain the search name for the caller
        m_Criteria.m_strSearchName = m_SearchNameCombo->GetStringSelection();

        // Update the GUI with the new data
        m_Criteria.Decode( newData );
        Value_to_GUI();
    }
}

void FindContactsDialog::OnSaveSearchAsBtnClick(wxCommandEvent& event)
{
    GUI_to_Value();
    wxArrayString data;
    m_Criteria.Encode( data );
    wxString caption;
    wxString prompt;

    caption = _("Find Contacts");
    prompt  = _("Search name:");

    m_PresetData.HandleSaveAs( this, m_SearchNameCombo, caption, prompt, data );
}

void FindContactsDialog::OnDeleteSearchBtnClick(wxCommandEvent& event)
{
    m_PresetData.RemoveCurrentPreset( m_SearchNameCombo );
}

void FindContactsDialog::OnFirstNameContainsCheckClick(wxCommandEvent& event)
{
    m_Criteria.m_fFirstName = m_FirstNameContainsCheck->GetValue();
    m_FirstNameContainsEdit->Enable( m_Criteria.m_fFirstName );
}

void FindContactsDialog::OnLastNameContainsCheckClick(wxCommandEvent& event)
{
    m_Criteria.m_fLastName = m_LastNameContainsCheck->GetValue();
    m_LastNameContainsEdit->Enable( m_Criteria.m_fLastName );
}

void FindContactsDialog::OnEmailAddressesContainsCheckClick(wxCommandEvent& event)
{
    m_Criteria.m_fEmailAddresses = m_EmailAddressesContainsCheck->GetValue();
    m_EmailAddressesContainEdit->Enable( m_Criteria.m_fEmailAddresses );
}

void FindContactsDialog::OnPhoneNumbersContainCheckClick(wxCommandEvent& event)
{
    m_Criteria.m_fPhoneNumbers = m_PhoneNumbersContainCheck->GetValue();
    m_PhoneNumbersContainEdit->Enable( m_Criteria.m_fPhoneNumbers );
}

void FindContactsDialog::OnMore1CheckClick(wxCommandEvent& event)
{
    m_Criteria.m_fMore1 = m_More1Check->GetValue();
    m_More1Combo->Enable( m_Criteria.m_fMore1 );
    m_More1Edit->Enable( m_Criteria.m_fMore1 );
}

void FindContactsDialog::OnMore2CheckClick(wxCommandEvent& event)
{
    m_Criteria.m_fMore2 = m_More2Check->GetValue();
    m_More2Combo->Enable( m_Criteria.m_fMore2 );
    m_More2Edit->Enable( m_Criteria.m_fMore2 );
}

void FindContactsDialog::OnMore3CheckClick(wxCommandEvent& event)
{
    m_Criteria.m_fMore3 = m_More3Check->GetValue();
    m_More3Combo->Enable( m_Criteria.m_fMore3 );
    m_More3Edit->Enable( m_Criteria.m_fMore3 );
}
