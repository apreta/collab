/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 *
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */
#include "app.h"
#include "appsharepanel.h"
#include "sharecontrol.h"
#include "meetingcentercontroller.h"
#include "meetingmainframe.h"

#include "controls/wx/wxTogBmpBtn.h"       //  for gTransColour


BEGIN_EVENT_TABLE(BitmapView, wxScrolledWindow)
    EVT_PAINT(BitmapView::OnPaint)
	EVT_MOTION(BitmapView::OnMove)
    EVT_LEFT_DOWN(BitmapView::OnButton)
    EVT_LEFT_DCLICK(BitmapView::OnButton)
    EVT_LEFT_UP(BitmapView::OnButton)
    EVT_RIGHT_DOWN(BitmapView::OnButton)
    EVT_RIGHT_DCLICK(BitmapView::OnButton)
    EVT_RIGHT_UP(BitmapView::OnButton)
    EVT_KEY_DOWN(BitmapView::OnKey)
    EVT_KEY_UP(BitmapView::OnKey)
END_EVENT_TABLE()

BitmapView::BitmapView(wxWindow* parent,wxWindowID id,wxBitmap& bitmap,ShareControl *control)
    : wxScrolledWindow(parent, id), m_Bitmap(bitmap), m_pShareControl(control)
{
    m_Status = _("Loading...");
    m_Width = 0;
    m_Height = 0;
    m_ControlEnabled = false;
    m_ControlGranted = false;
    
    /* Cursor from other platforms is too large for Mac
    wxMask      *pMask;
    wxImage     imgT;
    wxBitmap    bmpT;
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/cursor.bmp")), wxBITMAP_TYPE_BMP );
    pMask = new wxMask( bmpT, *wxWHITE );
    bmpT.SetMask( pMask );
    imgT = bmpT.ConvertToImage( );
    imgT.SetOption( wxIMAGE_OPTION_CUR_HOTSPOT_X, 15 );
    imgT.SetOption( wxIMAGE_OPTION_CUR_HOTSPOT_Y, 15 );
    m_CursorControl = wxCursor( imgT );
    */
    
    m_CursorControl = wxCursor(wxCURSOR_CROSS);
    m_CursorNormal = wxCursor(wxCURSOR_ARROW);
    m_CursorGranted = wxCursor(wxCURSOR_BULLSEYE);
    
}

BitmapView::~BitmapView()
{
}

void BitmapView::OnPaint(wxPaintEvent &WXUNUSED(event))
{
    wxPaintDC dc( this );
    PrepareDC( dc );
    
    dc.DrawText( m_Status, 30, 10 );
	dc.DrawBitmap(m_Bitmap, 0, 0);
}

void BitmapView::OnMove(wxMouseEvent& event)
{
    if (!m_ControlGranted)
        return;

    int x,y;
    wxPoint point = event.GetPosition( );
	CalcUnscrolledPosition(point.x, point.y, &x, &y);
    wxLogDebug(wxT("Viewer mouse move to %d,%d"), x, y);

    if (x < 0) x = 0; else if (x >= m_Width) x = m_Width - 1;
    if (y < 0) y = 0; else if (y >= m_Height) y = m_Height - 1;

	m_pShareControl->ViewerMouseMove(x, y);
}

void BitmapView::OnButton(wxMouseEvent& event)
{
    event.Skip();
    
    if (!m_ControlGranted) 
    {
        if (m_ControlEnabled)
            m_pShareControl->ViewerRequestControl();
        return;
    }

	int x,y;
    wxPoint point = event.GetPosition( );
	CalcUnscrolledPosition(point.x, point.y, &x, &y);
    wxLogDebug(wxT("Viewer button at %d,%d"), x, y);
    
    int button;
    switch (event.GetButton())
    {
        case wxMOUSE_BTN_LEFT:
            button = RemoteLeftButton;
            break;
        case wxMOUSE_BTN_MIDDLE:
            button = RemoteMiddleButton;
            break;
        case wxMOUSE_BTN_RIGHT:
            button = RemoteRightButton;
            break;
    }

    bool pressed = false;
    if (event.ButtonDown() || event.ButtonDClick())
        pressed = true;

    if (event.ButtonDown())
    {
        CaptureMouse();
    }
    else if (event.ButtonUp() && HasCapture())
    {
        ReleaseMouse();
    }

    m_pShareControl->ViewerMouseButton(button, pressed, x, y);
}

void BitmapView::OnKey(wxKeyEvent& event)
{
    if (!m_ControlGranted)
        return;

    int key = event.GetKeyCode();
    bool pressed = event.GetEventType() == wxEVT_KEY_DOWN;
    wxLogDebug(wxT("Key event %d"), event.GetKeyCode());

    // Convert to RFB keycodes (which are the same as X11 keysyms)
    if (key >= 'A' && key <= 'Z' && !event.ShiftDown())
    {
        key += 'a' - 'A';
    }
    else if (key < ' ' || key >= 0x7f)
    {
        switch (key) 
        {
            case WXK_BACK:              key = 0xff08;       break;
            case WXK_TAB:               key = 0xff09;       break;
            case WXK_RETURN:            key = 0xff0d;       break;
            case WXK_ESCAPE:            key = 0xff1b;       break;
            case WXK_SPACE:             key = ' ';          break;
            case WXK_DELETE:            key = 0xffff;       break;
            case WXK_CLEAR:             key = 0xff69;       break;
            case WXK_SHIFT:             key = 0xffe1;       break;
            case WXK_ALT:               key = 0xffe9;       break;
            case WXK_CONTROL:           key = 0xffe3;       break;   
            case WXK_MENU:              key = 0xff67;       break;
            case WXK_PAUSE:             key = 0xff13;       break;
            case WXK_END:               key = 0xff57;       break;
            case WXK_HOME:              key = 0xff50;       break;
            case WXK_LEFT:              key = 0xff51;       break;
            case WXK_UP:                key = 0xff52;       break;
            case WXK_RIGHT:             key = 0xff53;       break;
            case WXK_DOWN:              key = 0xff54;       break;
            case WXK_SELECT:            key = 0xff60;       break;
            case WXK_PRINT:             key = 0xff61;       break;
            case WXK_EXECUTE:           key = 0xff62;       break;
            case WXK_INSERT:            key = 0xff63;       break;
            case WXK_HELP:              key = 0xff6a;       break;
            case WXK_NUMPAD0:           key = 0xffb0;       break;
            case WXK_NUMPAD1:           key = 0xffb1;       break;
            case WXK_NUMPAD2:           key = 0xffb2;       break;
            case WXK_NUMPAD3:           key = 0xffb3;       break;
            case WXK_NUMPAD4:           key = 0xffb4;       break;
            case WXK_NUMPAD5:           key = 0xffb5;       break;
            case WXK_NUMPAD6:           key = 0xffb6;       break;
            case WXK_NUMPAD7:           key = 0xffb7;       break;
            case WXK_NUMPAD8:           key = 0xffb8;       break;
            case WXK_NUMPAD9:           key = 0xffb9;       break;
            case WXK_MULTIPLY:          key = 0xffaa;       break;
            case WXK_ADD:               key = 0xffab;       break;
            case WXK_SEPARATOR:         key = 0xffac;       break;
            case WXK_SUBTRACT:          key = 0xffad;       break;
            case WXK_DECIMAL:           key = 0xffae;       break;
            case WXK_DIVIDE:            key = 0xffaf;       break;
            case WXK_F1:                key = 0xffbe;       break;
            case WXK_F2:                key = 0xffbf;       break;
            case WXK_F3:                key = 0xffc0;       break;
            case WXK_F4:                key = 0xffc1;       break;
            case WXK_F5:                key = 0xffc2;       break;
            case WXK_F6:                key = 0xffc3;       break;
            case WXK_F7:                key = 0xffc4;       break;
            case WXK_F8:                key = 0xffc5;       break;
            case WXK_F9:                key = 0xffc6;       break;
            case WXK_F10:               key = 0xffc7;       break;
            case WXK_F11:               key = 0xffc8;       break;
            case WXK_F12:               key = 0xffc9;       break;
            case WXK_F13:               key = 0xffca;       break;
            case WXK_F14:               key = 0xffcb;       break;
            case WXK_F15:               key = 0xffcc;       break;
            case WXK_F16:               key = 0xffcd;       break;
            case WXK_F17:               key = 0xffce;       break;
            case WXK_F18:               key = 0xffcf;       break;
            case WXK_F19:               key = 0xffd0;       break;
            case WXK_F20:               key = 0xffd1;       break;
            case WXK_F21:               key = 0xffd2;       break;
            case WXK_F22:               key = 0xffd3;       break;
            case WXK_F23:               key = 0xffd4;       break;
            case WXK_F24:               key = 0xffd5;       break;
            case WXK_NUMLOCK:           key = 0xff7f;       break;
            case WXK_SCROLL:            key = 0xff14;       break;
            case WXK_PAGEUP:            key = 0xff55;       break;
            case WXK_PAGEDOWN:          key = 0xff56;       break;
            case WXK_NUMPAD_SPACE:      key = 0xff80;       break;
            case WXK_NUMPAD_TAB:        key = 0xff89;       break;
            case WXK_NUMPAD_ENTER:      key = 0xff8d;       break;
            case WXK_NUMPAD_F1:         key = 0xff91;       break;
            case WXK_NUMPAD_F2:         key = 0xff92;       break;
            case WXK_NUMPAD_F3:         key = 0xff93;       break;
            case WXK_NUMPAD_F4:         key = 0xff94;       break;
            case WXK_NUMPAD_HOME:       key = 0xff95;       break;
            case WXK_NUMPAD_LEFT:       key = 0xff96;       break;
            case WXK_NUMPAD_UP:         key = 0xff97;       break;
            case WXK_NUMPAD_RIGHT:      key = 0xff98;       break;
            case WXK_NUMPAD_DOWN:       key = 0xff99;       break;
            case WXK_NUMPAD_PAGEUP:     key = 0xff9a;       break;
            case WXK_NUMPAD_PAGEDOWN:   key = 0xff9b;       break;
            case WXK_NUMPAD_END:        key = 0xff9c;       break;
            case WXK_NUMPAD_BEGIN:      key = 0xff9d;       break;
            case WXK_NUMPAD_INSERT:     key = 0xff9e;       break;
            case WXK_NUMPAD_DELETE:     key = 0xff9f;       break;
            case WXK_NUMPAD_EQUAL:      key = 0xffbd;       break;
            case WXK_NUMPAD_MULTIPLY:   key = 0xffaa;       break;
            case WXK_NUMPAD_ADD:        key = 0xffab;       break;
            case WXK_NUMPAD_SEPARATOR:  key = 0xffac;       break;
            case WXK_NUMPAD_SUBTRACT:   key = 0xffad;       break;
            case WXK_NUMPAD_DECIMAL:    key = 0xffae;       break;
            case WXK_NUMPAD_DIVIDE:     key = 0xffaf;       break;

            case WXK_LBUTTON:
            case WXK_RBUTTON:
            case WXK_CANCEL:
            case WXK_MBUTTON:
            case WXK_CAPITAL:
            case WXK_SNAPSHOT:
            
            // the following key codes are only generated under Windows currently
            case WXK_WINDOWS_LEFT:
            case WXK_WINDOWS_RIGHT:
            case WXK_WINDOWS_MENU:
            case WXK_COMMAND:
            default:
                wxLogDebug(wxT("Key code is unknown, ignoring"));
                return;
        }
    }
    
    m_pShareControl->ViewerKey(key, pressed);
}

void BitmapView::StartShare()
{
    m_ControlGranted = false;
    SetRemoteControl(share_is_control_enabled());
}

void BitmapView::SetScrolling()
{
    m_Width = m_Bitmap.GetWidth();
    m_Height = m_Bitmap.GetHeight();
    wxLogDebug(wxT("Resetting viewer size to %d,%d"), m_Width, m_Height);
    SetVirtualSize(m_Width, m_Height);
    
    // Set up or hide scrollbars depending on client window size
    wxSize size = GetSize();
    wxLogDebug(wxT("Window size %d,%d"), size.GetWidth(), size.GetHeight());
    if (size.GetWidth() >= m_Width && size.GetHeight() >= m_Height)
        SetScrollRate(0, 0);
    else
        SetScrollRate(10, 10);
}

void BitmapView::SetRemoteControl(bool enabled)
{
    wxLogDebug(wxT("Remote control %s"), enabled ? wxT("enabled") : wxT("disabled"));
    m_ControlEnabled = enabled;
    if (enabled)
        SetCursor(m_CursorControl);
    else
        SetCursor(m_CursorNormal);
}

void BitmapView::SetControlGranted(bool granted)
{
    wxLogDebug(wxT("Remote control %s"), granted ? wxT("granted") : wxT("revoked"));
    m_ControlGranted = granted;
    if (granted)
        SetCursor(m_CursorGranted);
    else if (m_ControlEnabled)
        SetCursor(m_CursorControl);
    else
        SetCursor(m_CursorNormal);
}


BEGIN_EVENT_TABLE(FullScreenFrame, wxFrame)
END_EVENT_TABLE()

FullScreenFrame::FullScreenFrame(wxWindow* parent, wxBitmap& bitmap) :
    wxFrame(parent, -1, wxT(""), wxDefaultPosition, wxDefaultSize, wxFRAME_FLOAT_ON_PARENT)
{
    ShowFullScreen(true);
#ifdef __WXMAC__
    // wxWidgets is sizing the window correctly, but leaving it positioned as if the top menubar
    // is still on the screen.  So, we simply need to move it up.
    Move(0,0);
#endif
}

FullScreenFrame::~FullScreenFrame()
{
    ShowFullScreen(false);
}

void FullScreenFrame::OnPaint( wxPaintEvent &WXUNUSED(event) )
{
}


BEGIN_EVENT_TABLE(AppSharePanel, wxWindow)
END_EVENT_TABLE()

AppSharePanel::AppSharePanel(wxWindow* parent, wxWindowID id, ShareControl*control) :
	wxWindow(parent, id), m_ShareTimer(this)
{
	//m_Status = _("Connecting...");
	m_pShareControl = control;
    m_pMMF = wxDynamicCast(wxGetTopLevelParent(this), MeetingMainFrame);

    m_FullScreen = NULL;

    wxGetApp( ).m_pMC->Connect(wxID_ANY, wxEVT_MEETING_EVENT, MeetingEventHandler(AppSharePanel::OnMeetingEvent), 0, this );
    Connect( wxID_ANY, wxEVT_TIMER, wxTimerEventHandler(AppSharePanel::OnShareTimer) );

	m_ShareHTML = new wxHtmlWindow( this, wxID_ANY, wxDefaultPosition,
								    wxSize(400, 200), wxHW_SCROLLBAR_NEVER );
    m_ShareHTML->SetBorders( 10 );

    m_ShareHTML->SetPage(
            _("<HTML><BODY>"
              "<h3>No application share in progress.</h3>"
              "</BODY></HTML>")
        );
    m_ShareHTML->Show(true);

#ifdef __WXMAC__
    m_BitmapView = new BitmapView( this, -1, m_ShareBitmap, m_pShareControl );
    m_BitmapView->Show(false);
#else
    m_BitmapView = NULL;
#endif

	Layout( );

    CONTROLLER.Connect( wxEVT_SHARE_EVENT, wxCommandEventHandler( AppSharePanel::OnShareEvent ), NULL, this );

}

AppSharePanel::~AppSharePanel()
{
    wxGetApp( ).m_pMC->Disconnect(wxID_ANY, wxEVT_MEETING_EVENT, MeetingEventHandler(AppSharePanel::OnMeetingEvent), 0, this );

	CONTROLLER.Disconnect( wxEVT_SHARE_EVENT, wxCommandEventHandler( AppSharePanel::OnShareEvent ), NULL, this );
}

void AppSharePanel::OnMeetingEvent(MeetingEvent& event)
{
    event.Skip();

    if( m_pMMF->GetMeetingID() != event.GetMeetingId( ) )
    {
        //  Wrong meeting!!
        return;
    }
        
    if (event.GetEvent() == AppShareStarted)
    {
        if (!m_pShareControl->IsPresenter())
        {
            // Use timer to launch share: in the case sharing is already in progess when
            // we join the meeting, this gives the rest of the meeting window a chance
            // to initialize first.
            m_ShareTimer.Start( 200, wxTIMER_ONE_SHOT);
        }
        else if (m_pShareControl->IsDesktopShared())
        {
            // We're sharing our desktop, change help text.
            SetInfoText();
        }
        else 
        {
            // We are the presenter, and the server thinks app sharing is running, but we're not
            // sharing our desktop, so tell server app share session is ended.
            m_pShareControl->ServerAppShareEnd();
			SetInfoText();
        }
    }
    else if (event.GetEvent() == AppShareEnded)
    {
        NormalDisplay();
        m_pShareControl->ShareEnd();
        SetInfoText();
    }
    else if (event.GetEvent() == MeetingStarted)
    {
        SetInfoText();
    }
    else if (event.GetEvent() == MeetingEnded)
    {
        m_pShareControl->ShareEnd();
        SetInfoText();
    }
    else if (event.GetEvent() == PresenterGranted)
    {
        SetInfoText();
    }
    else if (event.GetEvent() == PresenterRevoked)
    {
        SetInfoText();
    }
}

void AppSharePanel::OnEraseBackground(wxEraseEvent& event)
{
    wxCoord x,y,height,width;

    wxDC *dc = event.GetDC();
    dc->GetClippingBox(&x, &y, &width, &height);
    m_pShareControl->ViewerErase(x, y, width, height);

	event.Skip();
}

void AppSharePanel::DoSetSize(int x, int y, int width, int height, int sizeFlags)
{
    wxWindow::DoSetSize(x, y, width, height, sizeFlags);
    m_pShareControl->ViewerMoved(0, 0, width, height);
    if (m_BitmapView && m_BitmapView->GetParent() == this)
        m_BitmapView->SetSize(x, y, width, height);
    wxLogDebug(_T("Moved viewer (%d,%d)"), width, height);
}

void AppSharePanel::SetInfoText()
{
    bool IsSharing = m_pShareControl->IsViewing();
    bool IsPresenting = m_pShareControl->IsDesktopShared();
    bool IsPresenter = m_pShareControl->IsPresenter();
    bool IsModerator = m_pShareControl->IsModerator();

    if (IsPresenting)
    {
        m_ShareHTML->SetPage(
            _("<HTML><BODY>"
              "<h3>Your desktop is shared</h3>"
              "<p><a href='IDC_DLG_MTG_MENU_STOPSHARING'>Stop sharing</a>"
              "</BODY></HTML>")
        );
        m_ShareHTML->Show(true);
        if (m_BitmapView)
            m_BitmapView->Show(false);
    }
    else if (IsSharing)
    {
        m_ShareHTML->Show(false);
        if (m_BitmapView)
            m_BitmapView->Show(true);
    }
    else
    {
        if (IsPresenter)
            m_ShareHTML->SetPage(
                _("<HTML><BODY>"
                  "<h3>No application share in progress.</h3>"
                  "<p><a href='IDC_DLG_MTG_MENU_SHAREDESKTOP'>Share my desktop</a><br>"
                  "<a href='IDC_DLG_MTG_MENU_SHAREAPPLICATION'>Share an application</a>"
                  "</BODY></HTML>")
            );
        else if (IsModerator)
            m_ShareHTML->SetPage(
                _("<HTML><BODY>"
                  "<h3>No application share in progress.</h3>"
                  "<p><a href='IDC_DLG_MTG_MENU_MAKEMEPRESENTER'>Make Me Presenter</a><br>"
                  "</BODY></HTML>")
            );            
        else 
            m_ShareHTML->SetPage(
                _("<HTML><BODY>"
                  "<h3>No application share in progress.</h3>"
                  "</BODY></HTML>")
            );
        m_ShareHTML->Show(true);
        if (m_BitmapView)
            m_BitmapView->Show(false);
    }
    Layout();
}

void AppSharePanel::OnShareTimer(wxTimerEvent& event)
{
    m_pShareControl->ShareViewer(GetHandle());
	SetInfoText();
}

void AppSharePanel::NormalDisplay()
{
    if (m_BitmapView != NULL && m_FullScreen != NULL)
    {
        m_BitmapView->Reparent(this);
        GetParent()->Layout();
        m_BitmapView->SetScrolling();
        
        m_FullScreen->Destroy();
        m_FullScreen = NULL;
    }
    else
    {
        int width, height;
        GetSize(&width, &height);
        m_pShareControl->ViewerMoved(0, 0, width, height);
        GetParent()->Refresh();
    }
}

void AppSharePanel::FullScreen()
{
    if (m_BitmapView != NULL && m_FullScreen == NULL)
    {
        m_FullScreen = new FullScreenFrame(m_pMMF, m_ShareBitmap);
        m_BitmapView->Reparent(m_FullScreen);
        m_FullScreen->Show();
        m_BitmapView->SetScrolling();
    }    
}

void AppSharePanel::OnShareEvent(wxCommandEvent& event)
{
	event.Skip();
    if (!m_pShareControl->IsViewing())
        return;
    
    switch (event.GetInt())
    {
        case CB_Connected:
        {
            //m_Status = _("Loading...");
            Refresh();
            m_pShareControl->ViewerMoved(0, 0, GetSize().GetWidth(), GetSize().GetHeight());
            if (m_BitmapView)
                m_BitmapView->StartShare();
            break;
        }
        case CB_DisplayUpdated:
        {
            if (m_BitmapView != NULL)
            {
                wxImage image;
                m_pShareControl->ViewerGetImage(image);

                m_ShareBitmap = wxBitmap(image);

                if (m_ShareBitmap.GetWidth() != m_BitmapView->m_Width || m_ShareBitmap.GetHeight() != m_BitmapView->m_Height)
                    m_BitmapView->SetScrolling();

                m_BitmapView->SetBackgroundColour(*wxBLACK);
                m_BitmapView->SetForegroundColour(*wxWHITE);

                m_BitmapView->Refresh();
                
                wxLogDebug(wxT("Viewer updated"));
            }
            break;
        }
        case CB_DisplayNormal:
            NormalDisplay();
            break;

        case CB_DisplayFullScreen:
            FullScreen();
            break;
        
        case CB_EnableControl:
            if (m_BitmapView != NULL)
                m_BitmapView->SetRemoteControl(true);
            break;
            
        case CB_DisableControl:
            if (m_BitmapView != NULL)
                m_BitmapView->SetRemoteControl(false);
            break;
        
        case CB_GrantControl:
            if (m_BitmapView != NULL)
                m_BitmapView->SetControlGranted(true);
            break;
            
        case CB_RevokeControl:
            if (m_BitmapView != NULL)
                m_BitmapView->SetControlGranted(false);
            break;
    }
}
