/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef APP_H
#define APP_H

#include "wx_pch.h"
#include <wx/font.h>
#include <wx/log.h>
#include <wx/snglinst.h>
#include <wx/html/htmlwin.h>

#include "startupparams.h"
#include "wxzonoptions.h"
#include "userpreferences.h"

class MeetingCenterController;
class DocShareView;
class CIICDoc;

// Common accessors
#define STARTUP    (wxGetApp().m_S)
#define CONTROLLER (*wxGetApp().m_pMC)
#define OPTIONS    (wxGetApp().m_Options)
#define PREFERENCES (wxGetApp().m_UserPreferences)
#define PRODNAME    (wxGetApp().m_strProductName)
#define LOCALE      (wxGetApp().m_pLocale)

#define MAX_STRING 2048

#include <wx/ipc.h>
#include <wx/docview.h>

#include <ipc.h>


//*****************************************************************************
// Server class, for listening to connection requests
class stServer : public wxServer
{
public:
    wxConnectionBase *OnAcceptConnection(const wxString& topic);
};


//*****************************************************************************
// Connection class, for use by both communicating instances
class stConnection : public wxConnection
{
public:
    stConnection() {}
    ~stConnection() {}
    bool OnExecute(const wxString& topic, wxChar*data, int size,
                   wxIPCFormat format);
};


//*****************************************************************************
// Client class, to be used by subsequent instances in OnInit
class stClient : public wxClient
{
    public:
        stClient() {};
        wxConnectionBase *OnMakeConnection() { return new stConnection; }
};



//*****************************************************************************
//  MyLogWindow class
class MyLogWindow : public wxLogWindow
{
public:
    MyLogWindow( wxFrame *parent, const wxChar *title, bool fShow = true, bool fPassToOld = true ) : wxLogWindow( parent, title, fShow, fPassToOld ) { }

    bool OnFrameClose( wxFrame *frame );
    void OnFrameDelete( wxFrame *frame );
};


//*****************************************************************************
//  MyApp class
class MyApp : public wxApp
{
public:
	/** Clear members */
	void Clear();

	/** Clear all startup parameters */
	void ClearStartupParameters();

	/** Application initialization.<br>
	 ** This method is called before any windows are displayed.<br>
	 ** @return bool Returns true if it's OK to start the
	 ** application.
	 **/
	virtual bool OnInit( );

    /** Application exit */
    virtual int OnExit();

    /** OnAssertFailure */
    void        OnAssertFailure( const wxChar *file, int line, const wxChar *func, const wxChar *cond, const wxChar *msg );

	/** Parse the command line parameters. The input parameters
	 ** determine what window will be displayed.
	 **/
	virtual void ParseCommandLineParameters();

    /** Return the Options interface */
    wxZonOptions & Options() { return m_Options; }

    /** Return a reference to the default font */
	wxFont          &GetAppDefFont( )    { return m_AppDefFont; };

	/** Reset the default font */
	void            SetAppDefFont( const wxFont &font ) { m_AppDefFont = font; }

    /** Return resource file name */
    wxString        GetResourceFile( wxString res )
    {
        return m_strResourceDir + wxT("/") + res;
    }

    void            LoadMenuResources( );
    void            DecodeCommandLineParameters( );
    void            ProcessQueueCommand( );

    void SendQueueInvite(wxString& strMeetingID, wxString& strTitle, wxString& strDescrip, wxString& strMessage,
                         wxString& strPartID, wxString& strPartName, wxString& strSystem, wxString& strScreen);
    void SendQueueStatus(wxString status);

    /** Open debug log window */
    void OpenDebugWindow( );

public:
    MyLogWindow     *m_pMyLogWindow;

    wxDocManager    *m_pDocManager;

    UserPreferences m_UserPreferences;  /**< The current user preferences. This is loaded by the meeting center controller. */
	StartupParams   m_S;                /**< The command line startup parameters */
	MeetingCenterController *m_pMC;     /**< Ptr. to the meeting center controller */
    wxZonOptions m_Options;

    wxLocale        *m_pLocale;

    stServer        *m_server;
    wxArrayString   m_argArray;
    wxString        m_strExecDir;
    wxString        m_strResourceDir;
    wxString        m_strDataDir;
    wxString        m_strProductName;
    wxString        m_strWebPhoneName;
    wxString        m_strUseWebPhone;

private:
    wxFont          m_AppDefFont;
    bool            m_fShowMCC;
    bool            m_fExitBeforeStart;

    wxSingleInstanceChecker     *m_pSingleInstance;

    queue_t         m_queue;        /**< Communication channel with external IM program. */

    static void queue_handler(void *u, datablock_t blk);
    void        OnQueueEvent(wxCommandEvent& event);
    void        SendQueueReply(wxString msg);
    int         GetLocaleFromSys( );
    bool        IsControllerVisible( int iDisplay );
	void		MakeForeground( );
    int         MeetingLookup(const wxString& mid, const wxString& pid);
    void        log_rotate_if_needed( wxString &logFN, bool fAlwaysRotate );
    wxString    GetOptionName(const char *name, const char *defValue);

};

DECLARE_APP(MyApp)

#endif // APP_H
