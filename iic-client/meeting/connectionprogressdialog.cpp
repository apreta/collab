/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"
#include "connectionprogressdialog.h"

//(*InternalHeaders(ConnectionProgressDialog)
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(ConnectionProgressDialog)
//*)

BEGIN_EVENT_TABLE(ConnectionProgressDialog,wxDialog)
	//(*EventTable(ConnectionProgressDialog)
	//*)
END_EVENT_TABLE()

ConnectionProgressDialog::ConnectionProgressDialog(wxWindow* parent,wxWindowID id):
m_parent(parent)
{
	//(*Initialize(ConnectionProgressDialog)
	wxXmlResource::Get()->LoadObject(this,parent,_T("ConnectionProgressDialog"),_T("wxDialog"));
	m_InfoText = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT1"));
	m_ConnectGauge = (wxGauge*)FindWindow(XRCID("ID_CONNECT_GAUGE"));
	m_MsgEdit = (wxTextCtrl*)FindWindow(XRCID("ID_MSG_EDIT"));
	m_CloseDlgBtn = (wxButton*)FindWindow(XRCID("ID_CLOSEDLG_BTN"));
	//*)
    Connect(wxID_ANY,wxEVT_INIT_DIALOG,(wxObjectEventFunction)&ConnectionProgressDialog::OnInit); 
    
    m_ConnectGauge->SetRange(3);

#ifdef __WXMAC__
	Centre();
#endif
}

ConnectionProgressDialog::~ConnectionProgressDialog()
{
}

void ConnectionProgressDialog::OnInit(wxInitDialogEvent& event)
{
    CUtils::StealFocus(this);
    Raise();
}

void ConnectionProgressDialog::SetComplete(const wxString & str)
{
    wxString    strT( _("Testing complete."), wxConvUTF8 );
    m_InfoText->SetLabel( strT );
    m_ConnectGauge->SetValue(3);
    m_MsgEdit->SetValue( str );
    
    m_CloseDlgBtn->SetLabel( _("OK"));
}

