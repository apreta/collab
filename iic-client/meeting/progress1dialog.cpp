/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "progress1dialog.h"

////(*InternalHeaders(Progress1Dialog)
//#include <wx/bitmap.h>
//#include <wx/button.h>
//#include <wx/font.h>
//#include <wx/fontenum.h>
//#include <wx/fontmap.h>
//#include <wx/image.h>
//#include <wx/intl.h>
//#include <wx/settings.h>
//#include <wx/xrc/xmlres.h>
////*)
//
////(*IdInit(Progress1Dialog)
////*)

#include "app.h"
#include "meetingcentercontroller.h"
#include "session_api.h"
#include "events.h"


//BEGIN_EVENT_TABLE(Progress1Dialog,wxDialog)
//	//(*EventTable(Progress1Dialog)
//	//*)
//END_EVENT_TABLE()


//*****************************************************************************
Progress1Dialog::Progress1Dialog( const wxString &strTitle, const wxString &strPrompt, int iMax, wxWindow *pParent, int iStyle )
    :wxProgressDialog( strTitle, strPrompt, iMax, pParent, iStyle ), m_PendingTimer(this)
{
    m_fCancelled = false;

    Connect( wxID_ANY, wxEVT_TIMER,                   wxTimerEventHandler(Progress1Dialog::OnExecutePendingTimer), 0, this );

    m_PendingTimer.Start( 5, wxTIMER_ONE_SHOT );
}


//*****************************************************************************
Progress1Dialog::~Progress1Dialog()
{
    m_PendingTimer.Stop( );
    Disconnect( wxID_ANY, wxEVT_TIMER,                   wxTimerEventHandler(Progress1Dialog::OnExecutePendingTimer), 0, this );
}


//*****************************************************************************
void Progress1Dialog::OnExecutePendingTimer( wxTimerEvent& event )
{
//    wxLogDebug( wxT("Progress1Dialog::OnExecutePendingTimer") );

    CommandQueueElement *pCmd           = Commands().GetNextCommand();
    while( pCmd )
    {
        Internal_ExecuteCommand( pCmd );

        // Done with the memory now
        delete pCmd;

        pCmd = Commands().GetNextCommand();
    }

    while( wxGetApp( ).Pending( ) )
    {
        wxGetApp( ).Dispatch( );
    }

    m_PendingTimer.Start( 5, wxTIMER_ONE_SHOT );
}


//*****************************************************************************
void Progress1Dialog::Internal_ExecuteCommand( CommandQueueElement *pCmd )
{
//    wxLogDebug( wxT("entering Progress1Dialog::OnUpdateStatus( )  -- command is %s"), pCmd->GetTaskName().c_str() );

    if( pCmd->IsTaskName(wxT("hide")) )
    {
        Hide( );
        m_fCancelled = true;
    }
    else if( pCmd->IsTaskName(wxT("OK")) )
    {
        SetReturnCode( wxID_OK );
    }
    else if( pCmd->IsTaskName(wxT("value")) )
    {
        wxString    strValue = pCmd->GetParam( 0 );
        long        lValue;
        if( strValue.ToLong( &lValue ) )
        {
            if( !Update( (int) lValue ) )
                m_fCancelled = true;
        }
    }
    else if( pCmd->IsTaskName(wxT("pulse")) )
    {
        if ( !Pulse( ) )
            m_fCancelled = true;
    }
}
