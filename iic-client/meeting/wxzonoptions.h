/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef _WXZONOPTIONS_H_
#define _WXZONOPTIONS_H_

#include "options.h"
#include "utils.h"

/**
  * This class is wrapper around the C language options.h/options.cpp interface.<br>
  * This wrapper makes it more convenient to use from within wxWidgets applications.
  */
class wxZonOptions
{
public:
	/** Constructor */
	wxZonOptions();

	/** Initialize the options interface.<br>
     ** This method sets up the full path names for the Global, System, and User settings files.
	 ** @param baseName The base name used to create the options flavors
	 ** @param screenName The users screen name
	 ** @parame userDir The folder where user options are to be stored.
	 ** @param globalDir The folder where global options are to be stored.
	 ** @returns none.
	 **/
	void Initialize( const wxString & baseName, const wxString & screenName, const wxString & userDir, wxString & globalDir );

    /** Set the name of the user options file. */
    bool SetScreenName( const wxString & screenName );

	/** Initialize the options interface.
     ** This method sets up the full path names for the Global, System, and User settings files.
     ** The 4 parameter Initialize() method is called using the following default values:<br>
     ** baseName = wxT("zon")<br>
     ** sysDir = wxStandardPaths::Get().GetDataDir()<br>
     ** userDir = wxStandardPaths::Get().GetUserDataDir()<br>
     ** <br>
     ** WARNING: session_initialize() from iiclib MUST BE CALLED BEFORE THIS METHOD. If you don't do this
     ** then yer App will SEGFAULT.
	 ** @param screenName The users screen name
	 ** @returns none.
	 **/
	void Initialize( const wxString & screenName );

	/**
	 ** Read in the options files.
	 ** @return bool Returns true if successful.
	 **/
	bool Read();

	/**
	 ** Write the options files.
	 ** @return bool Returns true if successful.
	 **/
	bool Write();

	/** Get a user value.
	 ** @param name Ptr. to the nul terminated option name.
	 ** @param def Ptr. to the nul terminated default value.\
	 ** @return const char * Returns a pointer to the nul terminated option value.
	 **/
	const char *GetUser(const char *name, const char *def);

	/**
	 ** Set a user value
	 ** @param name Ptr. to the nul terminated option name.
	 ** @param value Ptr. to the nul terminated option value.
	 ** @returns none.
	 **/
	void SetUser(const char *name, const char *value);

    /** Clear a user value
      * @param name Ptr. to the nul terminated option name.
      */
    void ClrUser(const char *name);

	/** Get a system value.
	 ** @param name Ptr. to the nul terminated option name.
	 ** @param def Ptr. to the nul terminated default value.\
	 ** @return const char * Returns a pointer to the nul terminated option value.
	 **/
	const char *GetSystem(const char *name, const char *def);

    /**
      * Get system bool.
      * @param name  Ptr. to the Nul terminated storage key name
      * @param fDefault The default value returned if the value was not found.
      * @return bool Returns the value if found.
      */
    bool GetSystemBool( const char *name, bool fDefault );

    /**
      * Get system int.
      * @param name  Ptr. to the Nul terminated storage key name
      * @param iDefault The default value returned if the value was not found.
      * @return int Returns the value if found.
      */
    int GetSystemInt( const char *name, int iDefault );

	/**
	 ** Set a user value
	 ** @param name Ptr. to the nul terminated option name.
	 ** @param value Ptr. to the nul terminated option value.
	 ** @returns none.
	 **/
	void SetSystem(const char *name, const char *value);

    /**
	 ** Set a system value
	 ** @param name Ptr. to the nul terminated option name.
	 ** @param value Ptr. to the nul terminated option value.
	 ** @returns none.
	 **/
    void SetSystemInt( const char *name, int ivalue );

    /**
      * Save screen size, position and style in the user options file.
      * @param name  Ptr. to the Nul terminated storage key name
      * @param window Reference to the target wxWindow
      * @return bool Returns true if successful
      */
    bool SaveScreenMetrics( const char *name, const wxWindow & window );

    /**
      * Load screen size, position, and style from the user options file.
      * @param name  Ptr. to the Nul terminated storage key name
      * @param window Reference to the target wxWindow
      */
    bool LoadScreenMetrics( const char *name, wxWindow & window );

    /**
      * Save some window metrics
      */
    bool SaveScreenMetrics(  const char *name, const wxSize & size, const wxPoint & point, long style );

    /**
      * Get user string.
      * @param name  Ptr. to the Nul terminated storage key name
      * @param fDefault The default value returned if the value was not found.
      * @return bool Returns the value if found.
      */
    wxString GetUserString( const char *name, const wxString & strDefault );

    /**
      * Set user bool
      * @param name  Ptr. to the Nul terminated storage key name
      * @param strValue The output value
      */
    void SetUserString( const char *name, const wxString & strValue );

    /**
      * Get user bool.
      * @param name  Ptr. to the Nul terminated storage key name
      * @param fDefault The default value returned if the value was not found.
      * @return bool Returns the value if found.
      */
    bool GetUserBool( const char *name, bool fDefault );

    /**
      * Set user bool
      * @param name  Ptr. to the Nul terminated storage key name
      * @param fDefault The default value returned if the value was not found.
      */
    void SetUserBool( const char *name, bool fValue );

    /**
      * Get user int.
      * @param name  Ptr. to the Nul terminated storage key name
      * @param nDefault The default value returned if the value was not found.
      * @return int Returns the value if found.
      */
    int GetUserInt( const char *name, int nDefault );

    /**
      * Set user int
      * @param name  Ptr. to the Nul terminated storage key name
      * @param nValue The value to store
      */
    void SetUserInt( const char *name, int nValue );

    /**
      * Get user wxFont
      * @param name  Ptr. to the Nul terminated storage key name
      * @param fDefault The default value returned if the value was not found.
      * @return bool Returns the value if found.
      */
    wxFont GetUserFont( const char *name, const wxFont & defaultFont );

    /**
      * Set user wxFont
      * @param name  Ptr. to the Nul terminated storage key name
      * @param fDefault The default value returned if the value was not found.
      */
    void SetUserFont( const char *name, const wxFont & font );

    /**
      * Get user wxColour
      * @param name  Ptr. to the Nul terminated storage key name
      * @param fDefault The default value returned if the value was not found.
      * @return bool Returns the value if found.
      */
    wxColour GetUserColour( const char *name, const wxColour & defaultColour );

    /**
      * Set user wxColour
      * @param name  Ptr. to the Nul terminated storage key name
      * @param color The color value to be written.
      */
    void SetUserColour( const char *name, const wxColour & colour );

    /**
      * Set user integer array.
      * @param array Ref to to be written.
      */
    void SetUserArrayInt( const char *name, const wxArrayInt & array );

    /**
      * Get user interger array.
      * @param defaultArray The default array to be returned if the name is not found.
      * @param array Output: The returned array of values.
      * @param bool Returns true if the array is loaded from storage. Returns false if the default values are returned.
      */
    bool GetUserArrayInt( const char *name, const wxArrayInt & defaultArray, wxArrayInt & array );

private:
	/** Ptr. to the options control structure created by the Initialize()
	 ** method.
	 **/
	options_t m_options;

	/** Flag: The options interface has been initialized */
	bool m_fInitialized;
};

#endif
