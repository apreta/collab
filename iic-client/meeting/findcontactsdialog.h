/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef FINDCONTACTSDIALOG_H
#define FINDCONTACTSDIALOG_H

//(*Headers(FindContactsDialog)
#include <wx/button.h>
#include <wx/checkbox.h>
#include <wx/combobox.h>
#include <wx/dialog.h>
#include <wx/sizer.h>
#include <wx/spinctrl.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
//*)

#include "contactsearch.h"
#include "presetdata.h"

class FindContactsDialog: public wxDialog
{
	public:

		FindContactsDialog(wxWindow* parent,wxWindowID id = -1);
		virtual ~FindContactsDialog();

		//(*Identifiers(FindContactsDialog)
		//*)

	protected:

		//(*Handlers(FindContactsDialog)
		void OnSearchNameComboSelect(wxCommandEvent& event);
		void OnSaveSearchAsBtnClick(wxCommandEvent& event);
		void OnDeleteSearchBtnClick(wxCommandEvent& event);
		void OnFirstNameContainsCheckClick(wxCommandEvent& event);
		void OnLastNameContainsCheckClick(wxCommandEvent& event);
		void OnEmailAddressesContainsCheckClick(wxCommandEvent& event);
		void OnPhoneNumbersContainCheckClick(wxCommandEvent& event);
		void OnMore1CheckClick(wxCommandEvent& event);
		void OnMore2CheckClick(wxCommandEvent& event);
		void OnMore3CheckClick(wxCommandEvent& event);
		//*)

		//(*Declarations(FindContactsDialog)
		wxStaticText* m_SearchNameLabel;
		wxComboBox* m_SearchNameCombo;
		wxButton* m_SaveSearchAsBtn;
		wxButton* m_DeleteSearchBtn;
		wxCheckBox* m_FirstNameContainsCheck;
		wxTextCtrl* m_FirstNameContainsEdit;
		wxCheckBox* m_LastNameContainsCheck;
		wxTextCtrl* m_LastNameContainsEdit;
		wxCheckBox* m_EmailAddressesContainsCheck;
		wxTextCtrl* m_EmailAddressesContainEdit;
		wxCheckBox* m_PhoneNumbersContainCheck;
		wxTextCtrl* m_PhoneNumbersContainEdit;
		wxCheckBox* m_More1Check;
		wxComboBox* m_More1Combo;
		wxTextCtrl* m_More1Edit;
		wxCheckBox* m_More2Check;
		wxComboBox* m_More2Combo;
		wxTextCtrl* m_More2Edit;
		wxCheckBox* m_More3Check;
		wxComboBox* m_More3Combo;
		wxTextCtrl* m_More3Edit;
		wxStaticText* m_SearchSelectedGroupsLabel;
		wxComboBox* m_SearchSelectedGroupsCombo;
		wxStaticText* m_NumberOfResultsLabel;
		wxSpinCtrl* m_NumberOfResultsSpin;
		//*)

public:
        /**
          * Preset the search criteria to be edited.<br>
          * NOTE: You MUST call this method before calling EditSearchCriteria()
          * @param presetName The name of the critera. This will be selected in the combo box.
          * @param criteria The content for the screen.
          */
        void PresetSearchCriteria( const wxString & presetName, const ContactSearch & criteria );

        /** Initialize the search crtieria and display the modeless dialog.
          * @param fModal If you want a modal dialog then pass fModal as true.
          * @return int Returns the wxID_OK/wxID_CANCEL if fModal is true, returns wxID_CANCEL if fModal is false.
          */
        int EditSearchCriteria( bool fModal = false );

        /** Save the search criteria
          */
        void SaveSearchCriteria();

        /** Return the selected search criteria
          */
        const ContactSearch & GetCriteria()
        {
            return m_Criteria;
        }

protected:
        /** m_Criteria => GUI */
        void Value_to_GUI();

        /** GUI => m_Criteria */
        void GUI_to_Value();

        /** Setup the UI state to match the current criteria */
        void SetUIFeedback();

	private:
        /** The current search criteria */
        ContactSearch m_Criteria;

        /** The Preset Data processor */
        PresetData m_PresetData;

		DECLARE_EVENT_TABLE()
};

#endif
