/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "wx_pch.h"
#include "meetingsearch.h"
#include "utils.h"
#include <wx/tokenzr.h>

MeetingSearch::MeetingSearch():
m_nDateRange(WithinNDays),
m_fNextNDays(false),
m_nDays(7),
m_nStartDate(0),
m_nEndDate(0),
m_fTitle(false),
//m_strTitle
m_fHostFirstName(false),
//m_strHostFirst
m_fHostLastName(false),
//m_strHostLast
m_fMeetingId(false),
//m_strMeetingId
m_fOnlyInProgress(false),
m_nMaxResults(100)
{

}

void MeetingSearch::Encode( wxArrayString & data ) const
{
    data.Clear();
    /*  0 */ data.Add( wxString::Format(wxT("%d"),m_nDateRange) );
    /*  1 */ data.Add( wxString::Format(wxT("%d"),m_fNextNDays) );
    /*  2 */ data.Add( wxString::Format(wxT("%d"),m_nDays) );
    /*  3 */ data.Add( wxString::Format(wxT("%d"),m_nStartDate) );
    /*  4 */ data.Add( wxString::Format(wxT("%d"),m_nEndDate) );
    /*  5 */ data.Add( wxString::Format(wxT("%d"),m_fTitle) );
    /*  6 */ data.Add( m_strTitle );
    /*  7 */ data.Add( wxString::Format(wxT("%d"),m_fHostFirstName) );
    /*  8 */ data.Add( m_strHostFirst );
    /*  9 */ data.Add( wxString::Format(wxT("%d"),m_fHostLastName) );
    /* 10 */ data.Add( m_strHostLast );
    /* 11 */ data.Add( wxString::Format(wxT("%d"),m_fMeetingId) );
    /* 12 */ data.Add( m_strMeetingId );
    /* 13 */ data.Add( wxString::Format(wxT("%d"),m_fOnlyInProgress) );
    /* 14 */ data.Add( wxString::Format(wxT("%d"),m_nMaxResults) );
    //Add furutre parameters here
}

void MeetingSearch::Encode( wxString & data ) const
{
    wxArrayString array;
    Encode( array );
    size_t i;
    data = wxT("");
    for( i = 0; i < array.GetCount(); ++i )
    {
        data += array[i];
        if( i < (array.GetCount()-1) )
        {
            data += wxT("|");
        }
    }
}

void MeetingSearch::Decode( const wxString & data )
{
    if( data.IsEmpty() )
        return;
    if( data.length() < 1 )
        return;

    wxArrayString arrayData;

    wxStringTokenizer tkz( data, wxT("|"));

    while ( tkz.HasMoreTokens() )
    {
        wxString token = tkz.GetNextToken();
        arrayData.Add( token );
    }
    Decode( arrayData );
}

void MeetingSearch::Decode( const wxArrayString & data )
{
    bool fOK = true;
    long val;
    for( size_t i = 0; fOK && i < data.GetCount(); ++i )
    {
        switch(i)
        {
            case 0: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_nDateRange = val;
                    break;

            case 1: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_fNextNDays = (val) ? true:false;
                    break;

            case 2: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_nDays = val;
                    break;

            case 3: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_nStartDate = val;
                    break;

           case 4: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_nEndDate = val;
                    break;

            case 5: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_fTitle = (val) ? true:false;
                    break;

            case 6: m_strTitle = data[i];
                    break;

            case 7: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_fHostFirstName = (val) ? true:false;
                    break;

            case 8: m_strHostFirst = data[i];
                    break;

            case 9: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_fHostLastName = (val) ? true:false;
                    break;

            case 10: m_strHostLast = data[i];
                    break;

            case 11: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_fMeetingId = (val) ? true:false;
                    break;

            case 12: m_strMeetingId = data[i];
                    break;

            case 13: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_fOnlyInProgress = (val) ? true:false;
                    break;

            case 14: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_nMaxResults = val;
                    break;

            // Add future parameters here
            default: break;
        }
    }
}

MeetingSearch & MeetingSearch::operator=( const MeetingSearch & rhs )
{
    // Ref data
    m_strSearchName = rhs.m_strSearchName;

    // Core data
    m_nDateRange = rhs.m_nDateRange;
    m_fNextNDays = rhs.m_fNextNDays;
    m_nDays = rhs.m_nDays;
    m_nStartDate = rhs.m_nStartDate;
    m_nEndDate = rhs.m_nEndDate;
    m_fTitle = rhs.m_fTitle;
    m_strTitle = rhs.m_strTitle;
    m_fHostFirstName = rhs.m_fHostFirstName;
    m_strHostFirst = rhs.m_strHostFirst;
    m_fHostLastName = rhs.m_fHostLastName;
    m_strHostLast = rhs.m_strHostLast;
    m_fMeetingId = rhs.m_fMeetingId;
    m_strMeetingId = rhs.m_strMeetingId;
    m_fOnlyInProgress = rhs.m_fOnlyInProgress;
    m_nMaxResults = rhs.m_nMaxResults;

    return *this;
}

wxString MeetingSearch::FormatDescription() const
{
    wxString str(wxT(""));
    wxString sep(wxT(","));

    if( m_nDateRange == WithinNDays )
    {
        if( m_fNextNDays )
        {
            str += wxString::Format(_("Within the next %d days"),m_nDays);
        }else
        {
            str += wxString::Format(_("Within the previous %d days"),m_nDays);
        }
    }else if( m_nDateRange == BetweenDates )
    {
        wxString start;
        wxString end;
        start = CUtils::FormatShortDate( m_nStartDate );
        end = CUtils::FormatShortDate( m_nEndDate );

        str += _("Dates between ");
        str += start;
        str += _(" and ");
        str += end;
    }else
    {
        str += _("All dates");
    }

    if( m_fTitle )
    {
        str += sep;
        str += _("Title:");
        str += m_strTitle;
    }
    if( m_fHostFirstName )
    {
        str += sep;
        str += _("First:");
        str += m_strHostFirst;
    }
    if( m_fHostLastName )
    {
        str += sep;
        str += _("Last:");
        str+= m_strHostLast;
    }
    if( m_fMeetingId )
    {
        str += sep;
        str += _("MID:");
        str += m_strMeetingId;
    }
    if( m_fOnlyInProgress )
    {
        str += sep;
        str += _("In progress");
    }
    return str;
}

