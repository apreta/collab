#!/bin/sh

# Merge translatable strings into per-language message catalogs

for dir in locale/*; do
	if [ -d $dir ]; then
    	echo "Updating $dir"
		pushd $dir >/dev/null
		if ! msgmerge meeting.po ../../meeting.pot -o meeting.po.upd; then
			echo "Failed"
			exit -1;
		fi
		mv meeting.po meeting.po.old
		mv meeting.po.upd meeting.po
		popd >/dev/null
	fi
done
