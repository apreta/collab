/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef MAINPANEL_H
#define MAINPANEL_H

//(*Headers(MainPanel)
#include <wx/panel.h>
//*)
#include <wx/aui/auibook.h>
#include <wx/artprov.h>
#include <wx/wxhtml.h>
#include <wx/scrolwin.h>
#include <memory>

#include "events.h"
#include "controller.h"

class ShareControl;
class AppSharePanel;
class WaitForStartDialog;
class DocShareCanvas;
class FileManager;
class FileDetails;


class MainPanel: public wxPanel
{
public:

    MainPanel(wxWindow* parent,wxWindowID id = -1, ShareControl *control = NULL);
    virtual ~MainPanel();

    AppSharePanel   *GetAppSharePanel()     { return m_pAppSharePanel; }
    wxHtmlWindow    *GetHTMLPanel( )        { return m_HTMLWindow; }
    DocShareCanvas  *GetDocSharePanel()     { return m_pDocSharePanel; }
    wxToolBar       *GetDocShareToolbar()   { return m_wndToolBar; }
    wxToolBar       *GetDocShareToolbar2()  { return m_wndToolBar2; }
    wxStaticText    *GetPositionLabel()     { return m_wndPositionLabel; }
    void             DisableDocShareToolbar( );
    void            StartMeeting( const wxString &strParticipantID, const wxString &strParticipantName, const wxString &strMeetingID, const wxString &strMeetingTitle, bool fJoining = false );
    void            SetParticipantID( const wxString &strParticipantID )    { m_strParticipantID = strParticipantID; }
    void            EndMeeting( );
    void            ConnectionLost( );
    void            ConnectionReconnected( );
    void            DisplayPage( int iPage );
    bool            IsHighlighted( wxString& tab, wxRect& pos );

    //(*Identifiers(MainPanel)
    //*)

protected:

    //(*Handlers(MainPanel)
    //*)
    void OnMeetingEvent(MeetingEvent& event);
    void OnMeetingError(MeetingError& event);
    void OnDocshareEvent(DocshareEvent& event);

    //(*Declarations(MainPanel)
    //*)
    wxAuiNotebook*      m_pNotebook;
    AppSharePanel*      m_pAppSharePanel;
    wxToolBar*          m_wndToolBar;
    wxToolBar*          m_wndToolBar2;
    wxStaticText*       m_wndPositionLabel;
    DocShareCanvas      *m_pDocSharePanel;
    ShareControl*       m_pShareControl;
    std::auto_ptr<FileManager> m_fileManager;

private:
    void                OnMeetingFetched( wxCommandEvent& event );
    void                OnWaitDialogOK( wxCommandEvent& event );
    void                OnWaitDialogCancel( wxCommandEvent& event );
    void                OnLinkClicked( wxHtmlLinkEvent& event );
    void                OnHTMLClicked( wxHtmlCellEvent& event );
    void                OnStopSharing( wxCommandEvent& event );
    void                OnFileManagerDone( wxCommandEvent& event );
    void                OnTabChange( wxAuiNotebookEvent& event );
    void                OnTimer(wxTimerEvent& event);

    wxHtmlWindow        *CreateHTMLCtrl( wxWindow* parent );
    wxAuiNotebook       *CreateNotebook( );
    wxToolBar           *CreateDocShareToolbar(wxWindow* parent);
    wxToolBar           *CreateDocShareToolbar2(wxWindow* parent);
    void                DisplayMeetingSummary( meeting_t mtg, int iStatus );

    void                FetchPictures( wxString mtgId );
    void                DisplayMeetingPictures( wxString& html );
    void                DisplayPicture( FileDetails * file, wxString& html, int index );

    void                OnGetPassword( wxCommandEvent& event );



private:
    WaitForStartDialog  *m_pWaitDlg;
    wxString            m_strParticipantID;
    wxString            m_strParticipantName;
    wxString            m_strMeetingID;
    wxString            m_strMeetingTitle;
    wxString            m_strHostName;
    wxHtmlWindow        *m_HTMLWindow;
    bool                m_fMeetingHasStarted;
    wxString            m_strHTML;
    wxString            m_strPictureArg;
    bool                m_fMeetingFetched;
    bool                m_fPicturesFetched;

    bool                m_fDocShareInProgress;
    int                 m_docShareHightlight;
    wxRect              m_docShareTabPos;
    wxTimer             m_timer;

    DECLARE_EVENT_TABLE()
};

#endif
