/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"
#include "sendrecvprogressdialog.h"

//(*InternalHeaders(SendRecvProgressDialog)
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(SendRecvProgressDialog)
//*)

BEGIN_EVENT_TABLE(SendRecvProgressDialog,wxDialog)
	//(*EventTable(SendRecvProgressDialog)
	//*)
END_EVENT_TABLE()

SendRecvProgressDialog::SendRecvProgressDialog(wxWindow* parent,wxWindowID id):
    m_fReceiveMode(false),
    m_AutoTimer(this),
    m_fTimerAborted( false )
{
	//(*Initialize(SendRecvProgressDialog)
	wxXmlResource::Get()->LoadObject(this,parent,_T("SendRecvProgressDialog"),_T("wxDialog"));
	m_pStaticText1 = (wxStaticText*)FindWindow(XRCID("ID_SENDRECVPROG_STATICTEXT1"));
	m_pGauge1 = (wxGauge*)FindWindow(XRCID("ID_SENDRECVPROG_GAUGE1"));
	m_pStaticText2 = (wxStaticText*)FindWindow(XRCID("ID_SENDRECVPROG_STATICTEXT2"));
	m_pGauge2 = (wxGauge*)FindWindow(XRCID("ID_SENDRECVPROG_GAUGE2"));
	//*)

    Connect( wxID_ANY,    wxEVT_TIMER, wxTimerEventHandler(SendRecvProgressDialog::OnExecuteAutoTimer) );
    Connect( wxID_CANCEL, wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&SendRecvProgressDialog::OnCancel);
}

SendRecvProgressDialog::~SendRecvProgressDialog()
{
	//(*Destroy(SendRecvProgressDialog)
	//*)
}


//*****************************************************************************
bool SendRecvProgressDialog::Show( bool show )
{
    if (show && !IsVisible())
    {
        if (m_fReceiveMode && GetParent() != NULL)
        {
            wxPoint pos = GetParent()->GetScreenRect().GetBottomRight();
            SetSize(pos.x - 300, pos.y - 160, wxDefaultCoord, wxDefaultCoord);
        }
        else
            CenterOnParent();
            
        int     iAlpha  = 254;
        if( CanSetTransparent( ) )
        {
            SetTransparent( iAlpha );
        }
    }
    return wxDialog::Show(show);
}


//*****************************************************************************
void SendRecvProgressDialog::SetReceiveMode( bool fMode )
{
    m_fReceiveMode = fMode;
}


//*****************************************************************************
void SendRecvProgressDialog::SetTitle( const wxString& aTitle)
{
    SetLabel( aTitle );
}


//*****************************************************************************
void SendRecvProgressDialog::SetLabelTitle2(const wxString& aLabel)
{
    m_pStaticText2->SetLabel( aLabel );
}


//*****************************************************************************
void SendRecvProgressDialog::SetLabelTitle(const wxString& aLabel)
{
    m_pStaticText1->SetLabel( aLabel );
}


//*****************************************************************************
void SendRecvProgressDialog::SetProgressRange(int aMax)
{
    m_fCancel = false;
    m_pGauge1->SetRange( aMax );
}


//*****************************************************************************
void SendRecvProgressDialog::IncrementProgressPos( )
{
    int iPos    = m_pGauge1->GetValue( ) + 1;
    int iMax    = m_pGauge1->GetRange( );
    if( iPos  >  iMax )
        iPos = iMax;
    m_pGauge1->SetValue( iPos );
}


//*****************************************************************************
void SendRecvProgressDialog::SetProgressPos(int aPos)
{
    int iMax = m_pGauge1->GetRange( );
    if( aPos  >  iMax )
        aPos = iMax;
    m_pGauge1->SetValue( aPos );
}


//*****************************************************************************
void SendRecvProgressDialog::SetProgressRange2(int aMax)
{
    m_fCancel = false;
    m_pGauge2->SetRange( aMax );
    m_pGauge2->Show( aMax != 0 );
}


//*****************************************************************************
void SendRecvProgressDialog::SetProgressPos2(int aPos)
{
    int iMax = m_pGauge2->GetRange( );
    if( aPos  >  iMax )
        aPos = iMax;
    m_pGauge2->SetValue( aPos );
}


//*****************************************************************************
void SendRecvProgressDialog::StartAutoProgress(int aTimeout)
{
    m_fCancel = false;
    m_fTimerAborted = false;
    wxString    strLabel1( _("Converting files...please wait"), wxConvUTF8 );
    SetLabelTitle( strLabel1 );

    //m_pStaticText2->Show( false );

    SetProgressRange( 5 );
    SetProgressPos( 1 );

    m_AutoTimer.Start( aTimeout, wxTIMER_ONE_SHOT );
}


//*****************************************************************************
void SendRecvProgressDialog::StopAutoProgress( )
{
    m_fTimerAborted = true;
    m_AutoTimer.Stop( );

    SetProgressPos( 0 );
    m_pStaticText2->Show( true );
}


//*****************************************************************************
void SendRecvProgressDialog::OnExecuteAutoTimer( wxTimerEvent &event )
{
    if( !m_fTimerAborted )
    {
        int pos = m_pGauge1->GetValue( ) + 1;
        if( pos  >  5 )
            pos = 0;
        m_pGauge1->SetValue( pos );

        m_AutoTimer.Start( -1, wxTIMER_ONE_SHOT );
    }
}


//*****************************************************************************
void SendRecvProgressDialog::OnCancel( wxCommandEvent& event )
{
    m_fCancel = true;
}
