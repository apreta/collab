/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef _PCPHONECONTROLLER_H_
#define _PCPHONECONTROLLER_H_

#include <wx/wxprec.h>
#include "pcphonedialdialog.h"
#include "VoipService.h"

/** Status mnemonics */
enum PCPhoneStatus
{
    PCPhoneStatus_Idle,
    PCPhoneStatus_Connecting,
    PCPhoneStatus_Connected,
    PCPhoneStatus_Ringing,
    PCPhoneStatus_Releasing,
    PCPhoneStatus_Failed,
    PCPhoneStatus_Timeout,
    PCPhoneStatus_Incoming
};

class PCPhoneController : public wxEvtHandler, public CVoipEvents
{
public:
    /** Some constants */
    enum PCPhoneSettings
    {
        DefaultConnectionTimeout = 60000,   /**< The default time to wait for a connection in ms */
        ShutdownTimer = 250,                /**< The time to wait to shutdown voip service after call is disconnected */
    };

    /** Constructor: Just remembers the parent window. */
	PCPhoneController( wxWindow * parent );

    /** Destructor: Delete the dialog, and disconnects from the VOIP server. */
    ~PCPhoneController();

    /** Connect the PCPhonePanel */
    void ConnectPCPhonePanel();

    /** Connect VOIP to the specified meeting */
    bool ConnectToMeeting();

    /** Disconnect from the VOIP server */
    bool Disconnect();

    /** Make a call */
    void MakeVoipCall(bool aDirectFlag, bool aServerInviteFlag);

    /** Hangup a call */
    void HangupCall();

    /** Is a call active.
      * @return bool Returns true if a call is active.
      */
    bool IsInCall();

    /** Set call muted */
    void SetMuted(bool mute);

    /////////////////////////////////////////////////////////////////
	virtual void OnRinging(int status);
	virtual void OnEstablished(int status);
	virtual void OnCleared(int status);
	virtual void OnLevels(float input, float output);

private:
    /** Dial the spcified text.
      */
    void Dial( const char *pszText );

public:
    /** Processing for the OK button (Hides the PC phone dialog) */
    void OnPCPhoneDialDialogOK(wxCommandEvent& event);

    /** Processing for the Connect to meeting button */
    void OnConnectToMeeting(wxCommandEvent& event);

    /** Processing for the Disconnect button */
    void OnDisconnect(wxCommandEvent& event );

    /** Processing for mute checkbox */
    void OnMicrophoneMuteClick(wxCommandEvent& event );
    void OnSpeakerMuteClick(wxCommandEvent& event );
    
    /** ReverseConnection - Connect or Disconnect as required */
    void OnReverseConnection(wxCommandEvent& event);

    /** On Dial pad requested */
    void OnDialPad(wxCommandEvent& event);

    /** Connection timeout */
    void OnTimer( wxTimerEvent& event );

    // Touch Tone Pad processing
    void On_TT1( wxCommandEvent& event );
    void On_TT2( wxCommandEvent& event );
    void On_TT3( wxCommandEvent& event );
    void On_TT4( wxCommandEvent& event );
    void On_TT5( wxCommandEvent& event );
    void On_TT6( wxCommandEvent& event );
    void On_TT7( wxCommandEvent& event );
    void On_TT8( wxCommandEvent& event );
    void On_TT9( wxCommandEvent& event );
    void On_TT0( wxCommandEvent& event );
    void On_TTStar( wxCommandEvent& event );
    void On_TTPound( wxCommandEvent& event );

    /** Audio proerties button processing */
    void OnAudioProperties( wxCommandEvent& event );

private:
    /** Emit a status event */
    void EmitStatus( PCPhoneStatus status, const wxString & msg );

public:
	wxWindow *m_parent;     /**< The GUI parent window */
	PCPhoneDialDialog *m_pPCPhoneDialDialog; /**< Ptr. to the PC Phone Dial Pad Dialog */
	bool m_fVoipServerInitiated;  /**< VOIP session initiated by the server */
	CVoipService *m_voip;   /**< Ptr. to the VOIP server instance */
	wxTimer m_timer;        /**< Connection timout timer */


	wxString m_strStatusIdle;
	wxString m_strStatusConnecting;
	wxString m_strStatusConnected;
	wxString m_strRinging;
	wxString m_strStatusReleasing;
    wxString m_strStatusInitFail;
    wxString m_strStatusInitNoMic;
	wxString m_strStatusFailed;
	wxString m_strStatusTimeout;
	wxString m_strStatusIncoming;
	wxString m_strTimeout;

	wxString m_strVoipStatus;

};


#endif
