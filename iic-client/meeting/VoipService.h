/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#pragma once


#include "VoipApi.h"

/*
   Wrapper for VoIP DLL
*/

/** iaxclient initialization - Audio source types.
  * These values were cloned from the iaxclient library in order eliminate the need for
  * clients to inlcude the iaxclient headers.
  */
enum VoipAudioSupport
{
    Voip_Audio_Internal = 0,
    Voip_Audio_Internal_PA = 1,
    Voip_Audio_Internal_File = 2,
    Voip_Audio_Internal_Alsa = 3,
    Voip_Audio_External = 99
};

class CVoipEvents
{
public:
	virtual void OnRinging(int) { };
	virtual void OnEstablished(int) { };
	virtual void OnCleared(int) { };
	virtual void OnLevels(float input, float output) { };
};


class CVoipService
{
	static void OnRingingHandler(int);
	static void OnEstablishedHandler(int);
	static void OnClearedHandler(int);
	static void Logger(int, const char *);
	static void OnLevels(float, float);

	static CVoipEvents * s_events;

public:
	CVoipService(CVoipEvents * events);
	~CVoipService(void);

	int Initialize(const char *dir);
	int Shutdown();
	int SetOption(const char * option, const char * value);
	int MakeCall(const char * from_user, const char * to_address);
	int Hangup();
	int Dial(const char * str);
	int MuteMicrophone(bool muted);
	int MuteSpeaker(bool muted);
	bool IsInCall();
	const wxString & GetErrorText( int errorCode );

	wxString m_strLastErrorText;
};
