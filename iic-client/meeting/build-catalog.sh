#!/bin/sh

echo "Generating strings from XRC files"
wxrc --gettext *.xrc >xrc_strings.cpp

echo "Building source file list"
find . -name "*.cpp" -o -name "*.h" >files.lst

echo "Generating message file"
rm meeting.pot
xgettext -k_ -kwxGetTranslation -dmeeting -omeeting.pot -ffiles.lst
