#ifndef MONITORLABELFRAME_H
#define MONITORLABELFRAME_H

//(*Headers(MonitorLabelFrame)
#include <wx/stattext.h>
#include <wx/frame.h>
//*)

class MonitorLabelFrame: public wxFrame
{
public:

    MonitorLabelFrame(wxWindow* parent, wxString& label, int x, int y);
    virtual ~MonitorLabelFrame();

    //(*Identifiers(MonitorLabelFrame)
    //*)

public:
 
    
protected:
    //(*Handlers(MonitorLabelFrame)
    //*)
    
    //(*Declarations(MonitorLabelFrame)
    wxStaticText* m_DisplayLabel;
    //*)

private:
    DECLARE_CLASS(MonitorLabelFrame)
    DECLARE_EVENT_TABLE()
};

#endif
