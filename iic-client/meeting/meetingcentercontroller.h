/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef MEETINGCENTERCONTROLLER_H
#define MEETINGCENTERCONTROLLER_H

//(*Headers(MeetingCenterController)
#include <wx/panel.h>
#include <wx/treelistctrl.h>
#include <wx/button.h>
#include <wx/frame.h>
#include <wx/html/htmlwin.h>
//*)
#include <wx/taskbar.h>
#include <wx/timer.h>

#include "session_api.h"
#include "controller.h"
#include "addressbk.h"
#include "schedule.h"
#include "events.h"
#include "services/common/common.h"
#include <wx/imaglist.h>
#include "meetingview.h"
#include "signondlg.h"
#include "commandqueue.h"
#include "meetingsearch.h"
#include "presencemanager.h"
#include "certhandler.h"
#include "contactservice.h"

class ConnectionProgressDialog;
class MeetingSetupDialog;
class ReconnectDialog;
class MeetingMainFrame;
class MeetingCenterController;
class ManageContactsFrame;
class InstallController;
class Progress1Dialog;
class ChatMainFrame;
class ContactService;


/**
  * Each item stored in the m_meetingList TreeListCtrl has one of these
  * data nodes attached to it.<br>
  *
  * Root nodes usually have a simple CommonTreeItemData attachment.
  */
class CommonTreeItemData : public wxTreeItemData
{
public:
    CommonTreeItemData( int nType ):
    m_nType( nType )
    {
    }
    int m_nType;
};

/**

  */
class MeetingTreeItemData : public CommonTreeItemData
{
public:

    /** Constructor for meeting nodes
      */
    MeetingTreeItemData( int nType, meeting_info_t info  );

    /** Constructor for root nodes
      */
    MeetingTreeItemData( int nType );

public:
    /** Meeting type: TypeInstant or TypeScheduled or ?? */
    int  m_meetingType;

    /** Meeting role: RollNormal,RollModerator,RollHost */
    int  m_meetingRole;

    /** The meeting ID */
    wxString m_meetingId;

    /** The participant ID */
    wxString m_participantId;

    /** The participant name */
    wxString m_participantName;

    /** The meeting title */
    wxString m_title;

    /** The meeting options */
    int m_options;

    /** The meeting start time */
    int m_start_time;
};

/**
  * Each search results node has an instance of this class.
  */
class SearchResultsTreeItemData : public CommonTreeItemData
{
public:
    SearchResultsTreeItemData( int nType )
    :CommonTreeItemData(nType),
    m_nSearchNumber(-1),
    m_list(0),
    m_nStartingRow(0),
    m_nTotalRetrieved(0),
    m_fComplete(false)
    {
    }

public:
    /**
      * The search critera name.
      */
    wxString m_strSearchCriteriaName;

    /**
      * The search critera used to populate the node.<br>
      * This is created by MeetingSearch::Encode( wxString )
      */
    wxString m_strSearchCriteria;

    /**
      * The search number used to populate this node.
      */
    int m_nSearchNumber;

    /**
      * The current meeting list. This instance is required
      * until a search is marked as complete.
      */
    meeting_list_t m_list;

    /**
      * The starting row used for incremental searches.
      */
    int m_nStartingRow;

    /**
      * The toal number of items retrieved.
      */
    int m_nTotalRetrieved;

    /**
      * The search is complete.
      */
    bool m_fComplete;
};

//*****************************************************************************
class TaskBarIcon : public wxTaskBarIcon
{
    MeetingCenterController *m_pMMC;

public:
    TaskBarIcon(MeetingCenterController * pMMC);
    wxMenu* CreatePopupMenu();
    void OnLeftClick(wxTaskBarIconEvent& event);
};


//*****************************************************************************
class MeetingCenterController: public wxFrame, public ContactEvents
{
public:
    /** NodeTypes */
    enum NodeTypes
    {
        Node_Root = 0,           /**< The node is one of the roots */
        Node_SearchResults = 1,  /**< The node is a search results root */
        Node_Public = 2,         /**< Meeting: The current user has nothing to do with the meeting */
        Node_OwnsMeeting = 3,    /**< Meeting: The current user owns this meeting */
        Node_InvitedTo = 4       /**< Meeting: The current user is invited to this meeting */
    };

    /** Meeting list image index values */
    enum MeetingListImages
    {
        Meeting_Img_Offline = 0,    /**< The meeting is currently offline */
        Meeting_Img_Online  = 1,    /**< The meeting is currently online */
        Meeting_Img_Unknown = 2     /**< The meeting status is unknown */
    };

    /** Default meeting display columns */
    enum DefaultMeetingDisplayColumns
    {
        Col_Title = 0,      /**< The meeting title text */
        Col_Id,             /**< The meeding ID */
        Col_PIN,            /**< The PIN number */
        Col_Host,           /**< The hose name ( First Last ) */
        Col_DateTime,       /**< The meeting start time */
        Col_Privacy,        /**< Privacy type */
        Col_Moderator       /**< Moderator Y or N */
    };

    /** State flags<br>
      * The m_ControllerState member is a bit mask that uses these
      * values.
      */
    enum StateMask
    {
        State_Idle =                0x00000000, /**< No state */
        State_Connected =           0x00000001, /**< The session is connected to the server */
        State_Authenticated =       0x00000002, /**< The user has been authenticated by the server */
        State_Registered =          0x00000004, /**< The user has been regsitered */
        State_HaveInstantMID =      0x00000008, /**< The users instant meeting ID has been retrieved */
        State_HaveMyMeetings =      0x00000010, /**< The users scheduled meetings have been loaded */
        State_HaveInvited =         0x00000020, /**< The users invited meetings have been loaded */
        State_HavePublic =          0x00000040, /**< The public meetings have been loaded */
        State_Closing =             0x00000080, /**< The application is closing */
        State_Sorting =             0x00000100, /**< The meeting list is being sorted */
        State_AdministratorMode =   0x00000200, /**< An administrator is signed on */
        State_WaitingForUpdate =    0x00000400, /**< Waiting for address book update */
        State_WaitingForReconnect = 0x08000000, /**< Connection has been lost - waiting for reconnection */
        State_WaitingForSignOn =    0x10000000, /**< The SignOn process is running */
        State_AnonymousMode =       0x20000000, /**< This session is for an anonymous user */
        State_Downloading =         0x40000000, /**< Addressbook data is being downloaded */
        State_WaitingForSetup =     0x80000000, /**< Waiting for meeting setup dialog */
    };

    /** Constructor
      * @param parent Ptr. to the parent window. Null is OK.
      * @param id Optional window ID. Defaults to -1
      */
    MeetingCenterController(wxWindow* parent_window, wxWindowID id = -1);

    virtual ~MeetingCenterController();

    /** Create the meeting center window.
      * @return wxFrame * Returns a pointer to the meeting center frame window.
      */
    wxFrame * CreateMeetingCenterWindow();

    /** Show meeting center window.
      */
    void ShowMeetingCenterWindow();

    /** Retrieve the last known preferences.
      * @return bool Returns true if all went well.
      */
    bool ReadPreferences();

    /** Retrieve the user's preferences based on entered screen name.
      * @return bool Returns true if all went well.
      */
    bool ReadUserPreferences();

    /** Refresh the meetings list.
      * @return none
      */
    void RefreshMeetingsList();

    /** Load profile image for current user (if available).
      */
    wxString FetchProfileImage();

    /** Menu ID numbers */
    enum MenuIDS
    {
        Help_About = wxID_ABOUT,

        Options_SignOff  = wxID_HIGHEST,
        Options_ProfilePhoto,
        //Options_Preferences, // wxID_PREFERENCES

        Meeting_Start = wxID_HIGHEST + 200,
        Meeting_Join,
        Meeting_JoinBy,
        Meeting_New,
        Meeting_View,
        Meeting_Refresh,
        Meeting_Edit,
        Meeting_Copy,
        Meeting_Delete,
        Meeting_Center,
        Documents_Recordings,
        Documents_Shared,
        //Search_FindMeeting, // wxID_FIND is used for this menu ID
        Search_Continue,
        Search_Remove,
        Search_Edit,
        Contacts_Manage,
        Contacts_EditMyContact,
        Contacts_Fetch,
        Help_Contents,
        Help_Debug,
        Help_Upload,
        Tools_ExecutePendingCommands

    };


    /** Timer ID numbers */
    enum TimerIDS
    {
        Ping_Timer      = wxID_HIGHEST,
        Pending_Timer
    };

    /** State flags: These mask values determine which command events are active
      * based on the current meeingList selection.
      */
    enum CommandMaskValues
    {
        Mtg_Start       = 0x00000001, /**< Start meeting enabled */
        Mtg_Join        = 0x00000002, /**< Join meeting enabled */
        Mtg_Create      = 0x00000004, /**< New meeting enabled */
        Mtg_View        = 0x00000008, /**< View meeting enabled */
        Mtg_Edit        = 0x00000010, /**< Edit meeting enabled */
        Mtg_Copy        = 0x00000020, /**< Delete meeting enabled */
        Mtg_Recordings  = 0x00000040, /**< Documents Recordings enabled */
        Mtg_Shared      = 0x00000080, /**< Documents Shared enabled */
        Mtg_Delete      = 0x00000100, /**< Delete enabled */
        Mtg_Preferences = 0x00000200, /**< Options - Preferences enabled */
        Srch_FindMeeting= 0x00000400, /**< Search find meetings */
        Srch_Continue   = 0x00000800, /**< Search continue enabled */
        Srch_Remove     = 0x00001000, /**< Search remove enabled */
        Srch_Edit       = 0x00002000, /**< Search edit enabled */
        Cnt_Manage      = 0x00004000, /**< Manage Contacts enabled */
        Cnt_EditMyNOT   = 0x00008000  /**< Edit my contact info DISABLED */
    };

    /**
      * Server Op Codes.<br>
      * Most of the functions in IICLIB that talk to the server have an opid parameter.<br>
      * These codes are used to create the opid strings sent to the server and to identify the originator of the
      * server call when the results are returned from the server.
      *
      */
    enum ServerOpcodes
    {
        OP_Instant =            1000,      /**< Meeting fetched during initial retrieval of the one instant meeting */
        OP_EditMeeting =        1001,      /**< MeetingSetupDialog originated - Edit or New operation */
        OP_MeetingMainFrame =   1002,      /**< MeetingMainFrame originalted */
        OP_DeleteMeeting =      1003,      /**< Meeting removed operation */
        OP_ViewMeeting =        1004,      /**< Meeting view operation */
        OP_MainPanel =          1005,      /**< MainPanel within MeetingMainFrame - fetch meeting details for display */
        OP_MCC_Public =         1006,      /**< MeetingCenterController: Public meetings fetch */
        OP_MCC_Invited =        1007,      /**< MeetingCenterController: Invited meetings fetch */
        OP_MCC_Scheduled =      1008,      /**< MeetingCenterController: Scheduled meetings fetch */
        OP_MCC_Search =         1009,      /**< MeetingCenterController: Find community meetings. (Uses OPCODE:SUBOP format) */
        OP_MCC_PAB =            1010,      /**< MeetingCenterController: Fetch Personal Address Book */
        OP_MCC_CAB =            1011,      /**< MeetingCenterController: Fetch Community Address Book */
        OP_EditMeeting_Search = 1012,      /**< MeetingSetupDialog search for contacts. (Uses OPCODE:SUBOP format) */
        OP_EditMeeting_Contact= 1013,      /**< MeetingSetupDialog add contacts */
        OP_ManageContacts =     1014,      /**< ManageContactsDialog procesing */
        OP_ManageProfiles =     1015,      /**< ManageProfiles processing */
        OP_EditMyContact =      1016,      /**< Edit my contact information */
        OP_FindMyPINAndJoin =   1017       /**< Find user's PIN so we can then join the indicated meeting */
    };

    /** Encode a MeetingFetchOpcode into a string
      * @param nOpcode The integer op code.
      * @return wxString Returns the encoded op code.
      */
    static  wxString opcode( int nOpcode )
    {
        wxString ret;
        ret = wxString::Format(wxT("%d"),nOpcode);
        return ret;
    }

    /** Encode a two part MeetingFetchOpcode into a string.<br>
      * NOTE: If nSubOp is less than zero, a sub op code is generated from the current system
      * time. This should always result in a unique sub op code for the session.
      * @param nOpCode The integer op code
      * @param nSubOpCode The sub op code
      * @return wxString Returns the encoded op code.
      */
    static wxString opcode( int nOpcode, int nSubOp );

    /** Compare an encoded opcode to a MeetingFecthOpcode value.<br>
      * NOTE: Two part op codes are formatted as "OOOO:SSSS"<br>
      * Where:<br>
      * OOOO Is the numeric op code.<br>
      * SSSS Is the numeric sup-op code.<br>
      * If fExactMatch is false, then only the op code (OOOO) is used in the compare.
      * @param strOpcode Ref to the opcode string.
      * @param int nOpcode The value to test.
      * @param fExactMatch If true then the entire strOpcode string is compared.
      * @return bool Returns true if the values match.
      */
    bool IsOpcode( const wxString & strOpcode, int nOpcode, bool fExactMatch = true )
    {
        if( fExactMatch )
        {
            wxString strNum;
            strNum = strOpcode;
            long nVal = -1;
            if( strNum.ToLong( &nVal, 10 ) )
            {
                if( nVal == (long)nOpcode )
                    return true;
            }
            return false;
        }
        else
        {
            // Match only the prefix
            wxString strCheckCode;
            wxString strLeftSide;
            strCheckCode = wxString::Format(wxT("%d"),nOpcode);
            strLeftSide = strOpcode.Left( strCheckCode.length() );
            if( strCheckCode.CmpNoCase( strLeftSide ) == 0 )
            {
                return true;
            }
            return false;
        }
    }

    /** Setup the m_nCmdMask value based on the current state of the m_meetingList selection.
      */
    void SetupCommandMask( MeetingTreeItemData *pData = 0, SearchResultsTreeItemData *pSrch = 0 );

    /** Update the UI feedback.
      * The current value of m_nCmdMask is used to enable and disable UI features.
      */
    void SetUIFeedback( MeetingTreeItemData *pData = 0, SearchResultsTreeItemData *pSrch = 0 );

    /** Returns true if the specified CommandMaskValues are active
      * in the m_nCmdMask.
      * @param mask The mask bits to test.
      * @return bool Returns true if one or more mask bits is set.
      */
    bool IsEnabled( unsigned int mask )
    {
        return (mask & m_nCmdMask) != 0;
    }

    /** If there is a single selected meeting, then return it's ID.
      * @return wxString Return the meeging ID.
      */
    wxString GetSingleSelectedMeeting();

    /** Return meeting IDs of all selected meetings.
      * @param arrStr The wxArrayString of meeting IDs of all selected meetings.
      * @return int Return the number of meeting IDs in arrStr.
      */
    int GetAllSelectedMeetings( wxArrayString &arrStr );

    /** Initialize the controller:<br>
      * 1) Read preferences
      * 2) Initialize the server session
      */
    void Initialize();

    /** Destroy all meeting frame windows.
      */
    void CloseAllMeetings();

    /** See if there is a frame window for this meeting.
      */
    bool IsMeetingActive(const wxString & mtgId);

    /** See if any meeting is active or meeting setup dialog is open.
     */
    bool IsMeetingActive();

    /** Find first meeting window (if any).
      */
    MeetingMainFrame * FindMeetingWindow();

    /** Find meeting window (if any) associated with this meeting.
      */
    MeetingMainFrame * FindMeetingWindow(const wxString & mtgId);

    /** Raise main window for application, depending on current state.
     */
    wxWindow * ShowAppWindow();
    
    /** Display text on the status line.
      * @param msg The text to be displayed.
      * @return none.
      */
    void showStatus( const wxString & msg );

    /** Create a session and sign on.<br>
      * This method is called from the OnSignOn() event processor.<br>
      */
    void CreateSession( const wxString & server, const wxString & targetServer, const wxString & screen, const wxString & password, bool autoHunt, bool secure, const wxString &lastJID );

    /**
      * Get the display string for a Privacy code.
      * @param privacy The integer privacy code.
      * @return wxString The text privacy code.
      */
    wxString privacyText( int privacy );

    /**
      * Get the display string for a meeting Type code.
      * @param type The integer type code
      * @return wxStrign The text type code
      */
    wxString typeText( int type );

    /** Date/Time Format values */
    enum DateDecodeFormat
    {
        LongDateTime = 0,
        ShortDateTime,
        ShortDate
    };

    /** Decode the specfied date/time value into a display string.
      * @param aTime The date/time in standard time_t format.
      */
    wxString DateTimeDecode(int aTime, int aDateDecodeFormat = LongDateTime);

    /** Fetch meeting details for the specified OP code.
      * @param meetingId Ref to the target meeting ID.
      * @return None.
      */
    void FetchMeetingDetails( int nOpcode, const wxString & meetingId );

    /** Return the address book handle */
    addressbk_t AddressBook()
    {
        return book;
    }

    /** Return the current user's profile record */
    profile_t UserProfile()
    {
        return m_profile;
    }

    /** Profile state access: */
    bool CanShareDeskTop()
    {
        if( m_profile && (m_profile->enabled_features & ProfileDisableShareDesktop) != 0 )
            return false;
        return true;
    }

    bool CanShareApplication()
    {
        if( m_profile && (m_profile->enabled_features & ProfileDisableShareApplication) != 0 )
            return false;
        return true;
    }

    bool CanSharePowerpoint()
    {
        if( m_profile && (m_profile->enabled_features & ProfileDisableSharePowerpoint) != 0 )
            return false;
        return true;
    }

    bool CanShareDocument()
    {
        if( m_profile && (m_profile->enabled_features & ProfileDisableShareDocument) != 0 )
            return false;
        return true;
    }

    bool CanShareWhiteboard()
    {
        if( m_profile && (m_profile->enabled_features & ProfileDisableShareWhiteboard) != 0 )
            return false;
        return true;
    }

    bool CanShareRemoteControl()
    {
        if( m_profile && (m_profile->enabled_features & ProfileDisableShareRemoteControl) != 0 )
            return false;
        return true;
    }

    bool RequestMeetingProjectCode()
    {
        if( m_profile && (m_profile->enabled_features & ProfileRequestProjectCode) != 0 )
            return true;
        return false;
    }
        
    bool IsTelephonyAvailable();

    bool IsVOIPAvailable();

    bool IsVOIPEncrypted();

    /** Return true if an administrator is currently signed on */
    bool AdministratorMode()
    {
        return (m_ControllerState & State_AdministratorMode) != 0;
    }

    /** Return true if an anonymous user is currently signed on */
    bool AnonymousMode()
    {
        return (m_ControllerState & State_AnonymousMode) != 0;
    }

    /** Return true if we are registered as an IM client */
    bool IMClientMode();

    /** Wait for contact download to complete.
      */
    bool WaitContactsLoaded(wxWindow * waiter = NULL, bool bShowStatus = false);

    /** Refresh downloaded contact information.
      */
    int RefreshContacts( bool fFlush = true, bool fWait = true );

    /** Check to see if contact information has been downloaded.<br>
      * If contact information is supposed to be downloaded, then
      * this call answers the question: Is it done yet.
      * @return bool Returns true if contact information has been downloaded.
      */
    bool ContactInformationDownloaded();

    /** Lookup contact information in the Community Address Book.
      * @param useruid Ref to the target user id.
      * @return contact_t Pointer to the contact record if found. Zero is returned if the user id is not found.
      */
    contact_t LookupCABContact( const wxString & userid )
    {
        const char *pszuserid = userid.mb_str( wxConvUTF8 );
        return LookupCABContact( pszuserid );
    }


    /** Lookup contact information in the Community Address Book.
      * @param userid Ptr. to the Nul terminated user id string.
      * @return contact_t Pointer to the contact record if found. Zero is returned if the user id is not found.
      */
    contact_t LookupCABContact( const char * userid )
    {
        contact_t ret = 0;
        if( m_cab && userid )
        {
            ret = directory_find_contact( m_cab, userid );
        }
        return ret;
    }

    /** Lookup contact information in the Personal Address Book.
      * @param useruid Ref to the target user id.
      * @return contact_t Pointer to the contact record if found. Zero is returned if the user id is not found.
      */
    contact_t LookupPABConntact( const wxString & userid )
    {
        const char *pszuserid = userid.mb_str( wxConvUTF8 );
        return LookupPABContact( pszuserid );
    }

    /** Lookup contact information in the Personal Address Book.
      * @param userid Ptr. to the Nul terminated user id string.
      * @return contact_t Pointer to the contact record if found. Zero is returned if the user id is not found.
      */
    contact_t LookupPABContact( const char * userid )
    {
        contact_t ret = 0;
        if( m_pab && userid )
        {
            ret = directory_find_contact( m_pab, userid );
        }
        return ret;
    }

    /** Return the personal address book */
    directory_t GetPAB()
    {
        return m_pab;
    }

    /** Return the community address book */
    directory_t GetCAB()
    {
        return m_cab;
    }

    /** Event handler for directory import started */
    void OnDirectoryStarted(directory_t dir)
    {
        wxCommandEvent ev(wxEVT_DIRECTORY_STARTED, wxID_ANY);
        ev.SetClientData(dir);
        ev.SetString(wxString("import",wxConvUTF8));
        AddPendingEvent(ev);
    }
    
    /** Event handler for directory import complete */
    void OnDirectoryReady(directory_t dir)
    {
        wxCommandEvent ev(wxEVT_DIRECTORY_FETCHED, wxID_ANY);
        ev.SetClientData(dir);
        ev.SetString(wxString("import",wxConvUTF8));
        AddPendingEvent(ev);
    }

    /** Return user id of signed on user
     */
    wxString& GetCurrentUserId()
    {
        return m_strUserId;
    }

    void GetSignOnInfo(wxString &strScreen, wxString &strPassword, wxString &strCommunity)
    {
        strScreen = m_SignOnParams.m_strScreen;
        strPassword = m_SignOnParams.m_strPassword;
        strCommunity = m_SignOnParams.m_strCommunity;
    }

    /** Return screen name of signed on user
     */
    wxString GetCurrentScreenName()
    {
        return m_SignOnParams.m_strScreen;
    }

    const wxString& GetDisplayName()
    {
        return m_displayName;
    }
    
    void GetControllerUsername(wxString &username);

    void SetWaitingForSetup();
    void ClearWaitingForSetup();
    void RegisterMeetingSetup(MeetingSetupDialog* pDlg);
    void UnregisterMeetingSetup(MeetingSetupDialog* pDlg);
    MeetingSetupDialog* GetMeetingSetup();

    /** Lookup a meeting ID
      * @param mid The meeting ID
      * @return MeetingTreeItemData Returns a pointer to the meeting node data if found.
      */
    MeetingTreeItemData * LookupMeetingId( const wxString & mid );
    MeetingTreeItemData * LookupMeetingId( const wxTreeItemId & node, const wxString & mid );

    /** Lookup a meeting PIN
      * @param mid The meeting ID
      * @return MeetingTreeItemData Returns a pointer to the meeting node data if found.
      */
    MeetingTreeItemData * LookupMeetingPIN( const wxString & pin );
    MeetingTreeItemData * LookupMeetingPIN( const wxTreeItemId & node, const wxString & pin );

    /** Update the displayed status of a meeting */
    bool UpdateMeetingStatus( const wxString & mid, int newStatus );

    /** Update the displayed presence for a user */
    void UpdatePresence( const wxString system, const wxString screen, const wxString presence, const wxString status );

    void PresenceUpdated( const PresenceInfo& info );

    /** Return the schedule interfact handle */
    schedule_t Schedule()
    {
        return sched;
    }

    /** Return the contact manager service */
    ContactService * Directories()
    {
        return m_pContactService;
    }

    /** Return the presence manager */
    PresenceManager & Presence()
    {
        return m_PresenceManager;
    }

    /**
      * Return a reference to the command queue.
      */
    CommandQueue & Commands()
    {
        return m_Commands;
    }

    /**
      * Execute pending commands.
      * NOTE: This method is called as part of the SignOn() process.<br>
      * This call places a Tools_ExecutePendingCommands wxCOMMAND_EVENT into the queue
      * and returns.
      */
    void ExecutePendingCommands(bool wait=false);

    /**
      * Are all meetings reloaded?  Used after reconnection.
      */
    bool MeetingsAreReloaded( ) { return (m_fPublicMeetingsAreLoaded && m_fInvitedMeetingsAreLoaded && m_fMyMeetingsAreLoaded); };

private:
    /** Add columns and parent nodes to meeting center window */
    void SetupMeetingList();
    
    /** Called by OnExecutePendingCommands()
      * Do not call directly.
      */
    void Internal_ExecutePendingCommands();

    /** Called by Internal_ExecutePendingCommands()
      * Do not call directly.
      * @param pCmd Ptr. to the command
      * @param fWaitingForInput Returned as true if the command requires additional input to execute.
      * @return bool Returns true if the command was handled. (i.e. The command name was recognized).
      */
    bool Internal_ExecuteCommand( CommandQueueElement *pCmd, bool &fWaitingForInput );

    /** Internal join meeting */
    void JoinMeeting( MeetingTreeItemData *pData );

    /** Generate single certicate file with all allowed certificates */
    void ProcessCertificates(const wxString& userDataDir);

    /** Add new certificate */
    void AddCertificate(const wxString &strCert, bool bAccept);

    /** Show received certificate to user */
    bool ShowCertificate(wxString &details);

    //(*Identifiers(MeetingCenterController)
    //*)

protected:
    //(*Handlers(MeetingCenterController)
    void OnStartMeeting(wxCommandEvent& event);
    void OnJoinMeeting(wxCommandEvent& event);
    void OnScheduleNewMeeting(wxCommandEvent& event);
    void OnEditMeetingClick(wxCommandEvent& event);
    void OnViewMeeting(wxCommandEvent& event);
    void OnFindMeeting(wxCommandEvent& event);
    void OnMeetingListSelectionChanged(wxTreeEvent& event);
    void OnMeetingListItemMenu(wxTreeEvent& event);
    void OnMeetingListColumnLeftClick(wxListEvent& event);
    void OnMeetingListItemRightClick(wxTreeEvent& event);
    void OnMeetingListItemCollapsed(wxTreeEvent& event);
    void OnMeetingListItemExpanded(wxTreeEvent& event);
    void OnMeetingListKeyDown(wxTreeEvent& event);
    //*)

    // Non-wxSmith handlers
    void OnClose( wxCloseEvent& event );
    void OnActivate( wxActivateEvent& event );
    void OnMenuClose( wxCommandEvent& event );
    void OnExit( wxCommandEvent& event );
    void OnJoinMeetingByPin( wxCommandEvent& event );
    void OnRefreshMeetings( wxCommandEvent& event );
    void OnCopyMeeting( wxCommandEvent& event );
    void OnDeleteMeeting( wxCommandEvent& event );
    void OnRecordings( wxCommandEvent& event );
    void OnShared( wxCommandEvent& event );
    void OnSignOffEvent( wxCommandEvent& event );
    void OnConnectionProgreesDone(wxCommandEvent& event); // The user clicked the OK button
    void OnPreferences( wxCommandEvent& event );
    void OnSize( wxSizeEvent& event );
    void OnExecutePendingCommands( wxCommandEvent& event );
    void OnExecutePendingTimer( wxTimerEvent& event );
    void OnExecutePingTimer( wxTimerEvent& event );
    void OnReconnectCancelClick( wxCommandEvent& event );
    void OnReconnectCancelOKClick( wxCommandEvent& event );
    void OnReconnectCancelCancelClick( wxCommandEvent& event );
    void EnableWindows( bool fEnable, bool fFocus = false );
    void OnSearchContinue( wxCommandEvent& event );
    void OnSearchEdit( wxCommandEvent& event );
    void OnSearchRemove( wxCommandEvent& event );
    void OnMeetingCenter( wxCommandEvent& event );
    void OnContactsManage( wxCommandEvent& event );
    void ContactsManage( );
    void OnContactsFetch( wxCommandEvent& event );
    void OnContactsEditMyContact( wxCommandEvent& event );
    void OnContents( wxCommandEvent& event );
    void OnUploadLogs( wxCommandEvent& event );
    void OnProfilePhoto( wxCommandEvent& event );
    void OnDebug( wxCommandEvent& event );
    void OnAbout( wxCommandEvent& event );
    void OnTaskIconDblClick( wxTaskBarIconEvent& event );

    void OnMeetingListColClick( wxListEvent& event );
    void OnEditMeeting(wxCommandEvent& event);

    void deleteSelectedMeeting();

public:
    void SessionIsReconnected( );

   /**
     * Sign on to the server.<br>
     * If there is already a session conntected then this call does nothing.
     */
   void SignOn();

   /**
     * Sign off. Disconnect from the server.
     */
   void SignOff( bool fClose = true );

   /** Return true the one or more of the specified state flags are set
     * @param mask The state flags.
     */
   bool HasState( int mask )
   {
       return (m_ControllerState & mask) != 0;
   }

   /**
     * Returns the login state.<br>
     *
     * @return bool Returns true if this controller is signed on.
     */
   bool AlreadySignedOn(bool fAllowAnonymous = false)
   {
       if( m_ControllerState & State_Connected )
       {
           if( m_ControllerState & State_Authenticated || fAllowAnonymous )
           {
               if( m_ControllerState & State_Registered )
               {
                   return true;
               }
           }
       }
       return false;
   }

    /**
      * Given a MeetingSearch instance, initiate a search.<br>
      * If an existing node is located that matches the search
      * criteria, then it is repopulated with new results. If no
      * matching nodes are found, then a new one is created.
      */
    void StartMeetingSearch( const MeetingSearch & criteria );

    /**
      * Given a search node from the meeting list, initiate a search.<br>
      */
    void StartMeetingSearch( wxTreeItemId parent );

    /**
      * Return a SearchResultsTreeItemData pointer for the
      * currently selected node.
      */
    SearchResultsTreeItemData *GetSearchNode( wxTreeItemId & parent_node );
    SearchResultsTreeItemData *GetSearchNode();

    /**
      * Sort the meeting list.<br>
      */
    void SortMeetingList();

    /**
      * Sort the meeting list for a particular node.<br>
      * This method locks out user triggered sort changes until it is complete.
      */
    void SortMeetingList( const wxTreeItemId & item );

    /**
      * Given a tree item ID, return it's MeetingTreeItemData.
      * @return MeetingTreeItemData * Returns the data pointer if found, or zero if not found.
      */
    MeetingTreeItemData * GetMeetingItemData( const wxTreeItemId & item )
    {
        CommonTreeItemData *pCommonData = (CommonTreeItemData *)m_meetingList->GetItemData(item);
        if( pCommonData->m_nType == Node_Root )
            return 0;
        if( pCommonData->m_nType == Node_SearchResults )
            return 0;
        MeetingTreeItemData *pData = (MeetingTreeItemData *)pCommonData;
        return pData;
    }

    bool IsUpdateRequired( wxString installerUrl, int clientUpgradeFound );

private:
    // iic callback functioins - A 'C' to 'C++'/wxWidgets event conversion.
    // These static callback functions convert session api events into wxWidgets events.
    // The void * u parameters in these callbacks are the "this" pointers for the MeetingCenterController
    // class. The void *u parameter is static cast to MeetingCenterController *.
    static void on_session_error(void *, int err, const char *msg, int code, const char *id, const char *resource);
    static void on_connected(void *);
    static void on_controller_registered(void *);
    static void on_controller_ping(void *);
    static void on_controller_invite_lookup( void *u, meeting_invite_t invite );
    static void on_controller_invite( void *u, meeting_invite_t invite );
    static void on_meeting_event(void *u, meeting_event_t evt);
    static void on_meeting_error(void *u, meeting_error_t evt);
    static void on_meeting_chat(void *u, meeting_chat_t evt);
    static void on_submeeting_add(void *u, const char *opid, const char *sub_id);
    static void on_get_params(void *u, meeting_details_t details, int formum_options);
    static void on_addressbk_auth(void *u);
    static void on_addressbk_progress(void *u, const char *opid, int num_fetched, int total);
    static void on_schedule_fetched(void *u, const char *opid, meeting_list_t list);
    static void on_schedule_progress(void *u, const char *opid, int num_fetched, int total );
    static void on_schedule_updated(void  *u, const char *opid, int status, const char *meetingid );
    static void on_meeting_fetched(void *u, const char *opid, meeting_details_t mtg);
    static void on_directory_fetched(void *u, const char *opid, directory_t dir);
    static void on_schedule_error(void *u, const char *opid, int code, const char *msg);
    static void on_addressbk_update_failed( void *u, const char *opid, int status, const char *msg);
    static void on_address_updated(void *u, const char *opid, const char *itemid);
    static void on_address_deleted(void *u, const char *opid, const char *itemid);
    static void on_addressbk_version_check( void *u, int upgrade_type, const char *url );
    static void on_docshare_event(void *u, docshare_event_t evt);
    static void on_profile_fetched(void *u, const char *opid);
    static void on_controller_im_notify( void *u, const char *mid, participant_t part );
    static void on_addressbk_recording_list(void *u, const char *opid, recording_list_t reclist );
    static void on_addressbk_document_list(void *u, const char *opid, document_list_t reclist );
    static void on_im_event(void *, messaging_im_t msg);
    static void on_presence_event(void *, messaging_presence_t pres);
    static void on_im_invite(void *user, const char *id, const char *method, resultset_t result);
    static void on_conversion_result(void *user, const char *id, resultset_t result);
    static void on_conversion_error(void *user, const char *id, int code, const char *msg);
    static void on_conversion_event(void *user, const char *id, const char *method, resultset_t result);

    // iic interface events
    void OnConnectEvent(wxCommandEvent& event);
    void OnSessionError(SessionErrorEvent& event);
    void OnAuthEvent(wxCommandEvent& event);
    void OnAddressbkProgressEvent(wxCommandEvent& event);
    void OnRegisteredEvent(wxCommandEvent& event);
    void OnControllerInviteLookupEvent(wxCommandEvent& event); /**< Received when we look up a pin */
    void OnControllerInviteEvent(wxCommandEvent& event); /**< Received when someone else invites us to a meeting */
    void OnScheduleFetched(wxCommandEvent& event);      /**< Received after scheduled meetings are received */
    void OnMeetingFetched(wxCommandEvent& event);       /**< Received after meeting details are received */
    void OnDirectoryFetched(wxCommandEvent& event);
    void OnMeetingEvent(MeetingEvent& event);           /**< Received when meeting status has changed */
    void OnMeetingError(MeetingError& event);           /**< Received when user signs on elsewhere */
    void OnMeetingChatEvent(MeetingChatEvent& event);
    void OnScheduleProgress(wxCommandEvent& event);
    void OnScheduleUpdated(wxCommandEvent& event);
    void OnScheduleError(wxCommandEvent& event);
    void OnAddressbkUpdateFailed(wxCommandEvent& event);
    void OnAddressUpdated(wxCommandEvent& event);
    void OnAddressDeleted(wxCommandEvent& event);
    void OnAddressVersionCheck(wxCommandEvent& event);
    void OnProfileFetched(wxCommandEvent& event);
    void OnPresenceEvent(PresenceEvent& event);
    void OnControllerIMNotify(wxCommandEvent& event); /**< Received when we need to send an IM invite to someone */
    void OnControllerPingSucceeded( wxCommandEvent& event );
    void OnControllerPingFailed( );
    void OnIMChatEvent(IMChatEvent& event);
    void OnIMInviteEvent(IMInviteEvent& event);
    void OnLinkClicked(wxHtmlLinkEvent& event);
    void OnHTMLClicked(wxHtmlCellEvent& event);
    
private:
    //(*Declarations(MeetingCenterController)
    wxButton* m_ButtonJoin;
    wxButton* m_ButtonFind;
    wxTreeListCtrl* m_meetingList;
    wxButton* m_ButtonView;
    wxButton* m_ButtonEdit;
    wxPanel* m_Panel;
    wxButton* m_ButtonNew;
    wxHtmlWindow* m_HtmlText;
    wxButton* m_ButtonStart;
    //*)

    //////////////////////////////////////////////////
    wxMenuBar* m_MainMenuBar;   /**< The main menu bar */
    wxMenu*     m_ContactsMenu;
    wxMenu*     m_MeetingsMenu;
    wxMenu*     m_DocumentsMenu;
    wxMenu*     m_OptionsMenu;
    wxMenu*     m_SearchMenu;
    wxMenu*     m_HelpMenu;
    wxMenu*     m_MeetingPopup;
    wxMenu*     m_SearchPopup;

public:
    /** The session API handle. This handle is established by the Sign On process. */
    session_t session;

    /** The controller API handle. Set in CreateSession() */
    controller_t controller;

    /** The address book API handle. Set in CreateSession() */
    addressbk_t book;

    /** The meeting schedule handle. Set in CreateSession() */
    schedule_t sched;

    /** The state flag */
    int m_ControllerState;

    /** The participant name. This is passed to the MeetingMainFrame when
      * a meeting is started or joined.
      */
    wxString m_strParticipantName;

    /** The community address book (CAB).
      * This pointer is set in the OnDirectoryFetched() event handler when the
      * directory type is dtGlobal.
      */
    directory_t m_cab;

    /** The personal address book (PAB).
      * This pointer is set in the OnDirectoryFetched() event handler when the
      * directory type is dtPersonal.
      */
    directory_t m_pab;

    SignOnParams m_SignOnParams;            /**< The last sign on parameters */
    SignOnParams m_SavedSignOnParams;       /**< Sign on parameters to be restored after test connection. */
    wxString m_displayName;                 /**< The user display name. This is set by the OnAuthEvent() method. */
    wxWindow *parent;                       /**< The parent window (Can't use m_parent due to wxSmith code generator) */
    wxSize   m_lastSize;                    /**< The last known size */
    wxPoint  m_lastPosition;                /**< The last known position */
    ReconnectDialog *m_pReconnectDialog;    /**< The Reconnect dialog */
    meeting_invite_t m_join_inv;            /**< The meeting invitation we received when attempting to join a meeting */
    wxDialog   *m_pDialogInvite;            /**< Dialog showing meeting invitation */
    bool       m_fPublicMeetingsAreLoaded;  /**< Reload progress - used for reconnect */
    bool       m_fInvitedMeetingsAreLoaded; /**< Reload progress - used for reconnect */
    bool       m_fMyMeetingsAreLoaded;      /**< Reload progress - used for reconnect */
    bool       m_fIMClientMode;             /**< Using internal client IM */
    bool       m_fInstantMeeting;           /**< Show instant meeting */

    // m_meetingList info
    wxTreeItemId m_RootId;              /**< The root node of the m_meetingList */
    wxTreeItemId m_PublicMeetingsId;    /**< The public meeting node */
    wxTreeItemId m_MyMeetingsId;        /**< The my meetings node */
    wxTreeItemId m_PastMeetingsId;      /**< The my meetings node */
    wxTreeItemId m_UnscheduledMeetingsId; /**< The my meetings node */
    wxTreeItemId m_MyInstantMeetingId;  /**< The my instant meetings node */
    wxTreeItemId m_InvitedMeetingsId;   /**< The invited meetings node */
    wxTreeItemId m_CommunitySearchId;   /**< The community search results node */

    bool m_fPublicMeetingsExpanded;     /**< Flag: The Public meetings node is expanded */
    bool m_fMyMeetingsExpanded;         /**< Flag: The My Meetings node is expanded */
    bool m_fPastMeetingsExpanded;       /**< Flag: The My Meetings node is expanded */
    bool m_fUnscheduledMeetingsExpanded;/**< Flag: The My Meetings node is expanded */
    bool m_fMyInstantMeetingExpanded;   /**< Flag: The My Instant Meeting node is expanded */
    bool m_fInvitedMeetingsExpanded;    /**< Flag: The Invited meetings node is expanded */
    bool m_fCommunitySearchExpanded;    /**< Flag: The community search results node is expanded */

    wxString m_strLastMeetingId;        /**< The last selected meetingId */
    wxString m_strInstantMID;           /**< The current user's instant meeting ID */
    wxString m_strUserId;               /**< The current user's ID */
    profile_t m_profile;                /**< The current user's profile */
    int m_nCmdMask;                     /**< The available command mask. This value is set by SetupCommandMask() */
    bool m_fHaveGUI;                    /**< The meeting center controller GUI is active */
    int m_nSearchNumber;                /**< The current meeting search number */
    long m_nLastSortColumn;             /**< The last known sort column */
    bool m_fLastSortAscending;          /**< The ascending/descending state for m_nLastSortColumn */
    MeetingView m_MeetingView;          /**< The meeting view interface */
    int m_nDatabaseError;               /**< The last database error */
    wxString m_strDatabaseError;        /**< The last database error text */

    /** The connection progress dialog.<br>
      * Created at the start of the connection process.<br>
      * Closed on successful registration, or by the user after an error.
      */
    ConnectionProgressDialog *m_pConnectionProgressDialog;

    /** Pointer to meeting setup dialog that is currently displayed.<br>
      * Set by MeetingSetupDialog whenever it is displayed.
      */
    MeetingSetupDialog *m_pMeetingSetupDialog;

    wxTimer m_PendingTimer;
    wxTimer m_PingTimer;
    int     m_iPingsSent;
    int     m_iPingsReceived;

    /** The Manage Contacts dialog.<br>
      * This pointer is allocated when the ManageContactsFrame is created.<br>
      * The ManageContactsFrame clears the pointer when it closes.
      */
    ManageContactsFrame *m_pManageContactsFrame;

    /** The command queue.
      */
    CommandQueue m_Commands;

    /** Presence manager.
     */
    PresenceManager m_PresenceManager;

    /** Client upgrade downloader
      */
    InstallController   *m_pInstallController;
    bool                m_bLaunchInstallerAtExit;
    bool                m_fCheckVersionCompleted;

    bool                m_fForceApplicationExit;

    Progress1Dialog     *m_pProgressOfContactLoad;
    int                 m_iAnonSignOnTries;

    SignOnDlg           *m_pSignOnDlg;

    ChatMainFrame       *m_pChatMainFrame;

    ContactService      *m_pContactService;

    TaskBarIcon         *m_pTaskBarIcon;

	DECLARE_EVENT_TABLE()
};

/**
  * Specialized sort comparator for the meeting list.
  */
class MeetingListComparator : public wxTreeListCtrlComparator
{
public:
    MeetingListComparator( MeetingCenterController *pController, wxTreeListCtrl * pTree );
    virtual ~MeetingListComparator( ) {  }  /* avoid GCC warning */

    /**
      * Specialized comparator function:<br>
      * The comparator is setup to sort time_t time columns correctly.
      */
    virtual int CompareItems(const wxTreeItemId& item1,const wxTreeItemId& item2);

    /**
      * Access the controller.
      */
    MeetingCenterController * Controller()
    {
        return m_pController;
    }

private:
    MeetingCenterController *m_pController;
};


#endif
