/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "wx_pch.h"
#include "commandqueue.h"

CommandQueue::CommandQueue()
{
}

const CommandQueueList & CommandQueue::GetList()
{
    return m_list;
}

CommandQueueElement * CommandQueue::LastCmd()
{
    if( m_list.GetCount() )
    {
        wxCommandQueueListNode *last = m_list.GetLast();
        return last->GetData();
    }
    return 0;
}

int CommandQueue::AppendParameter( const wxString & pX )
{
    CommandQueueElement *pLastCmd = LastCmd();
    if( pLastCmd )
    {
        return pLastCmd->Append( pX );
    }
    return 0;
}

int CommandQueue::AppendCommand( CommandQueueElement *pCmd )
{
    m_list.Append( pCmd );
    return m_list.GetCount() - 1;
}

int CommandQueue::Add( const wxString &name )
{
    m_list.Append( new CommandQueueElement( name ) );
    return m_list.GetCount() - 1;
}

int CommandQueue::Add( const wxString &name, const wxString & p0 )
{
    Add( name );
    LastCmd()->Append(p0);
    return m_list.GetCount() - 1;
}

int CommandQueue::Add( const wxString &name, const wxString & p0, const wxString & p1 )
{
    Add( name );
    LastCmd()->Append(p0);
    LastCmd()->Append(p1);
    return m_list.GetCount() - 1;
}

int CommandQueue::Add( const wxString &name, const wxString & p0, const wxString & p1, const wxString & p2 )
{
    Add( name );
    LastCmd()->Append(p0);
    LastCmd()->Append(p1);
    LastCmd()->Append(p2);
    return m_list.GetCount() - 1;
}

int CommandQueue::Add( const wxString &name, const wxString & p0, const wxString & p1, const wxString & p2, const wxString & p3 )
{
    Add( name );
    LastCmd()->Append(p0);
    LastCmd()->Append(p1);
    LastCmd()->Append(p2);
    LastCmd()->Append(p3);
    return m_list.GetCount() - 1;
}

int CommandQueue::Add( const wxString &name, const wxString & p0, const wxString & p1, const wxString & p2, const wxString & p3, const wxString & p4 )
{
    Add( name );
    LastCmd()->Append(p0);
    LastCmd()->Append(p1);
    LastCmd()->Append(p2);
    LastCmd()->Append(p3);
    LastCmd()->Append(p4);
    return m_list.GetCount() - 1;
}

CommandQueueElement * CommandQueue::GetNextCommand()
{
    if( m_list.GetCount() )
    {
        wxCommandQueueListNode *pNode = m_list.GetFirst();
        CommandQueueElement *pCommand = pNode->GetData();
        pNode->SetData( 0 );
        m_list.DeleteNode( pNode );
        return pCommand;
    }
    return 0;
}
