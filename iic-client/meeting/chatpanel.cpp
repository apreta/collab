/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"
#include "chatpanel.h"
#include "meetingcentercontroller.h"
#include "meetingmainframe.h"
#include "participantpanel.h"
#include "sharecontrol.h"
#include "chatmainframe.h"

#include <wx/colordlg.h>
#include <wx/tooltip.h>
#include <wx/richtext/richtextstyles.h>

#include "utils.h"


//DEFINE_EVENT_TYPE(wxEVT_MY_TEXTCTRLEX_ONCHAR)


// it may also be convenient to define an event table macro for this event type
#define EVT_MY_TEXTCTRLEX_ONCHAR(id, fn) \
    DECLARE_EVENT_TABLE_ENTRY( \
        wxEVT_MY_TEXTCTRLEX_ONCHAR, id, wxID_ANY, \
        (wxObjectEventFunction)(wxEventFunction) wxStaticCastEvent( wxCommandEventFunction, &fn ), \
        (wxObject *) NULL \
        ),


//(*InternalHeaders(ChatPanel)
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(ChatPanel)
//*)

BEGIN_EVENT_TABLE(ChatPanel,wxPanel)
	//(*EventTable(ChatPanel)
	//*)
EVT_BUTTON(XRCID("IDC_DLG_MTG_BTN_SETFORE"),ChatPanel::OnClickFore)
EVT_BUTTON(XRCID("IDC_DLG_MTG_BTN_SETBACK"),ChatPanel::OnClickBack)
EVT_BUTTON(XRCID("IDC_DLG_MTG_BTN_SETSMALLER"),ChatPanel::OnClickSmaller)
EVT_BUTTON(XRCID("IDC_DLG_MTG_BTN_SETNORM"),ChatPanel::OnClickNorm)
EVT_BUTTON(XRCID("IDC_DLG_MTG_BTN_SETLARGER"),ChatPanel::OnClickLarger)
EVT_BUTTON(XRCID("IDC_DLG_MTG_BTN_SETBOLD"),ChatPanel::OnClickBold)
EVT_BUTTON(XRCID("IDC_DLG_MTG_BTN_SETITALICS"),ChatPanel::OnClickItalics)
EVT_BUTTON(XRCID("IDC_DLG_MTG_BTN_SETULINE"),ChatPanel::OnClickUline)
EVT_BUTTON(XRCID("IDC_DLG_MTG_BTN_SEND"),ChatPanel::OnClickSend)

EVT_TEXT_ENTER(XRCID("ID_CUSTOM1"),ChatPanel::OnTextCtrlSendTextEnter)
EVT_CHAR(ChatPanel::OnTextCtrlSendChar)
EVT_MY_TEXTCTRLEX_ONCHAR(wxID_ANY, ChatPanel::OnProcessCustom)
EVT_INIT_DIALOG(ChatPanel::OnInitDialog)

EVT_TEXT_URL(XRCID("ID_CUSTOM2"),ChatPanel::OnTextCtrlRecvTextURL)

END_EVENT_TABLE()


//*****************************************************************************
//  Constructore and Destructors
//*****************************************************************************
ChatPanel::ChatPanel( wxWindow *parent, wxWindowID id )
{
    m_pParent       = NULL;
    m_pParentChat   = NULL;
    m_iRole         = RollNormal;
    m_iChatMode     = ChatAll;

    if( parent->IsKindOf(CLASSINFO(MeetingMainFrame)) )
        m_pParent = (MeetingMainFrame *) parent;
    else if( parent->IsKindOf(CLASSINFO(ChatMainFrame)) )
        m_pParentChat = (ChatMainFrame *) parent;

	//(*Initialize(ChatPanel)
	wxXmlResource::Get()->LoadObject(this,parent,_T("ChatPanel"),_T("wxPanel"));
	TextCtrlRecv = (wxTextCtrlEx*)FindWindow(XRCID("ID_CUSTOM2"));
	m_buttonSetFore = (wxTogBmpBtn*)FindWindow(XRCID("IDC_DLG_MTG_BTN_SETFORE"));
	m_buttonSetBack = (wxTogBmpBtn*)FindWindow(XRCID("IDC_DLG_MTG_BTN_SETBACK"));
	m_buttonSetSmaller = (wxTogBmpBtn*)FindWindow(XRCID("IDC_DLG_MTG_BTN_SETSMALLER"));
	m_buttonSetNorm = (wxTogBmpBtn*)FindWindow(XRCID("IDC_DLG_MTG_BTN_SETNORM"));
	m_buttonSetLarger = (wxTogBmpBtn*)FindWindow(XRCID("IDC_DLG_MTG_BTN_SETLARGER"));
	m_buttonSetBold = (wxTogBmpBtn*)FindWindow(XRCID("IDC_DLG_MTG_BTN_SETBOLD"));
	m_buttonSetItalics = (wxTogBmpBtn*)FindWindow(XRCID("IDC_DLG_MTG_BTN_SETITALICS"));
	m_buttonSetUline = (wxTogBmpBtn*)FindWindow(XRCID("IDC_DLG_MTG_BTN_SETULINE"));
	m_buttonSend = (wxTogBmpBtn*)FindWindow(XRCID("IDC_DLG_MTG_BTN_SEND"));
	StaticText1 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT1"));
	m_chatTargetCombo = (wxComboBox*)FindWindow(XRCID("ID_COMBOBOX1"));
	TextCtrlSend = (wxTextCtrlEx*)FindWindow(XRCID("ID_CUSTOM1"));
	//*)

    long    lWinStyle;
    lWinStyle = GetWindowStyle( );
    lWinStyle |= wxWANTS_CHARS;
    SetWindowStyle( lWinStyle );

    //  Set up Text control for Receive
    wxTextAttr  tAttr;
    bool        bRet;
    wxFont      wFont;

    tAttr = TextCtrlRecv->GetDefaultStyle( );
    tAttr.SetFlags( wxTEXT_ATTR_FONT );
    bRet = TextCtrlRecv->SetDefaultStyle( tAttr );

    //  Set up Text control for Send
/*
    tAttr = TextCtrlSend->GetDefaultStyle( );
    wFont = tAttr.GetFont( );
    if( wFont.IsOk( ) )
    {
        tAttr.SetFlags( wxTEXT_ATTR_FONT );
        bRet = TextCtrlSend->SetDefaultStyle( tAttr );
        tAttr = TextCtrlSend->GetDefaultStyle( );
        wFont = tAttr.GetFont( );
    }
    else
    {
        wxLogMessage( wxT("In ChatPanel constructor : (before) default font IS INVALID!!!\n\r") );
        wFont = wxGetApp( ).GetAppDefFont( );
        tAttr.SetFont( wFont );
        tAttr.SetFlags( wxTEXT_ATTR_FONT );
        bRet = TextCtrlSend->SetDefaultStyle( tAttr );
        tAttr = TextCtrlSend->GetDefaultStyle( );
        wFont = tAttr.GetFont( );
    }

    bRet = TextCtrlSend->GetStyleU( TextCtrlSend->GetInsertionPoint( ), tAttr );
    wFont = tAttr.GetFont( );
*/
    //  ALWAYS set the current font to the application default.  The control is created with a BAD current font.  Sigh...
    wFont = wxGetApp( ).GetAppDefFont( );
    tAttr.SetFont( wFont );

    tAttr.SetFlags( wxTEXT_ATTR_FONT );
    bRet = TextCtrlSend->SetStyle( TextCtrlSend->GetInsertionPoint( ), TextCtrlSend->GetInsertionPoint( ), tAttr );

/*
    bRet = TextCtrlSend->GetStyleU( TextCtrlSend->GetInsertionPoint( ), tAttr );

    wFont = tAttr.GetFont( );
    if( !wFont.IsOk( ) )
        wxLogMessage( wxT("In ChatPanel constructor : (after) Current font IS INVALID!!\n\r") );
*/
    m_buttonSetFore->SetBackgroundColour( GetBackgroundColour( ) );
    m_buttonSetBack->SetBackgroundColour( GetBackgroundColour( ) );
    m_buttonSetSmaller->SetBackgroundColour( GetBackgroundColour( ) );
    m_buttonSetNorm->SetBackgroundColour( GetBackgroundColour( ) );
    m_buttonSetLarger->SetBackgroundColour( GetBackgroundColour( ) );
    m_buttonSetBold->SetBackgroundColour( GetBackgroundColour( ) );
    m_buttonSetItalics->SetBackgroundColour( GetBackgroundColour( ) );
    m_buttonSetUline->SetBackgroundColour( GetBackgroundColour( ) );
    m_buttonSend->SetBackgroundColour( GetBackgroundColour( ) );

    wxToolTip::Enable( true );
    wxToolTip::SetDelay( 200 );

    TextCtrlRecv->SetIgnoreOnCharEntry( true );
    TextCtrlRecv->InitializeForChat( _("Tahoma"), 2, wxColour( 255, 0, 0 ), wxColour( 255, 255, 255 ), true, false, false );
    TextCtrlSend->InitializeForChat( _("Tahoma"), 2, wxColour( 0, 0, 0 ), wxColour( 255, 255, 255 ),  false, false, false );

    TextCtrlSend->Connect(wxEVT_SET_FOCUS, wxFocusEventHandler(ChatPanel::OnTextCtrlSendFocus), NULL, this);

    InsertCueText();

    // iic session API
    if( m_pParent  &&  wxGetApp( ).m_pMC )
    {
        wxGetApp( ).m_pMC->Connect(wxID_ANY, wxEVT_MEETING_CHAT, MeetingChatEventHandler(ChatPanel::OnMeetingChatEvent), 0, this );
        wxGetApp( ).m_pMC->Connect(wxID_ANY, wxEVT_MEETING_EVENT, MeetingEventHandler(ChatPanel::OnMeetingEvent), 0, this );
    } 
    
    if( m_pParentChat )
    {
        StaticText1->Show( false );
        m_chatTargetCombo->Show( false );
    }
}


//*****************************************************************************
ChatPanel::~ChatPanel()
{
    if( m_pParent  &&  wxGetApp( ).m_pMC )
    {
        wxGetApp( ).m_pMC->Disconnect(wxID_ANY, wxEVT_MEETING_CHAT, MeetingChatEventHandler(ChatPanel::OnMeetingChatEvent), 0, this );
        wxGetApp( ).m_pMC->Disconnect(wxID_ANY, wxEVT_MEETING_EVENT, MeetingEventHandler(ChatPanel::OnMeetingEvent), 0, this );
    }
}

//*****************************************************************************
void ChatPanel::InsertCueText()
{
    TextCtrlSend->SelectAll();
    TextCtrlSend->ClearContents();

    m_fSendTextCue = true;
    wxColor color(*wxLIGHT_GREY);
    TextCtrlSend->FormatForegroundColor( color );
    if (TextCtrlSend->IsEnabled())
        TextCtrlSend->WriteText( _("Type chat text here") );
    else
        TextCtrlSend->WriteText( _("Chat is disabled") );
}

//*****************************************************************************
void ChatPanel::ClearCueText()
{
    if (m_fSendTextCue)
    {
        m_fSendTextCue = false;
        TextCtrlSend->SelectAll();
        TextCtrlSend->ClearContents();
        
        // Initialize the Chat panel from the user preferences
        TextCtrlSend->SelectAll();
        TextCtrlSend->FormatForegroundColor( PREFERENCES.m_ChatFG );
        TextCtrlSend->FormatBackgroundColor( PREFERENCES.m_ChatBG );
        TextCtrlSend->FormatFontSize( PREFERENCES.m_nChatFontSize );
        TextCtrlSend->FormatBold( PREFERENCES.m_fChatBold );
        m_buttonSetBold->SetDown( PREFERENCES.m_fChatBold );
        TextCtrlSend->FormatItalic( PREFERENCES.m_fChatItalic );
        m_buttonSetItalics->SetDown( PREFERENCES.m_fChatItalic );
        TextCtrlSend->FormatUnderline( PREFERENCES.m_fChatUnderline );
        m_buttonSetUline->SetDown( PREFERENCES.m_fChatUnderline );
        TextCtrlSend->SetSelection(0,0);
    }
}

//*****************************************************************************
void ChatPanel::EnableChats()
{
    bool enableSend;
    
    if (m_iRole == RollNormal) {
        if (m_iChatMode == ChatModerators)
            m_chatTargetCombo->SetSelection(1);
        m_chatTargetCombo->Enable(m_iChatMode == ChatAll);
        enableSend = m_iChatMode != ChatNone;
    } else {
        m_chatTargetCombo->Enable(true);
        enableSend = true;
    }
    if (!enableSend) {
        TextCtrlSend->ClearContents( );
        TextCtrlSend->Enable(enableSend); 
        InsertCueText();
    } else if (!TextCtrlSend->IsEnabled()) {
        TextCtrlSend->Enable(enableSend);
        InsertCueText();
    } else {
        TextCtrlSend->Enable(enableSend); 
    }
    m_buttonSend->Enable(enableSend);
	m_buttonSetFore->Enable(enableSend);
	m_buttonSetBack->Enable(enableSend);
	m_buttonSetSmaller->Enable(enableSend);
	m_buttonSetNorm->Enable(enableSend);
	m_buttonSetLarger->Enable(enableSend);
	m_buttonSetBold->Enable(enableSend);
	m_buttonSetItalics->Enable(enableSend);
	m_buttonSetUline->Enable(enableSend);
}

//*****************************************************************************
void ChatPanel::SelectChatType()
{
    //int iSel = m_chatTargetCombo->GetCurrentSelection( );

    if (m_iRole == RollHost) {
        // Switch to all attendees
        m_chatTargetCombo->SetSelection(0);
    } else {
        // Switch to hosts and moderators
        m_chatTargetCombo->SetSelection(1);
    }
}

//*****************************************************************************
void ChatPanel::OnMeetingEvent(MeetingEvent& event)
{
    event.Skip();

    wxString    strMID( event.GetMeetingId( ) );
    wxString    strSubID( event.GetSubId( ) );
    wxString    strPartID( event.GetParticipantId( ) );

    if (event.GetEvent() == MeetingStarted || event.GetEvent() == MeetingModified || event.GetEvent() == ParticipantState)
    {
        meeting_t mtg = controller_find_meeting( CONTROLLER.controller, strMID.mb_str(wxConvUTF8) );
        if( mtg  !=  NULL )
        {
            participant_t p = meeting_find_me(mtg);
            if (p != NULL) {
                m_participantMe = p;
                m_iRole = p->roll;
            }
            m_iChatMode = mtg->invitee_chat_options;
        }
        EnableChats();
        if (event.GetEvent() == MeetingStarted)
            SelectChatType();
    }
    else if (event.GetEvent() == MeetingEnded)
    {
    }
}

//*****************************************************************************
void ChatPanel::OnMeetingChatEvent(MeetingChatEvent& event)
{
    wxTextAttr  tAttr;
    tAttr = TextCtrlRecv->GetTextAttr( );
    tAttr.SetBackgroundColour( wxColour( 255, 255, 255 ) );
    tAttr.SetTextColour( wxColour( 255, 0, 0 ) );
    tAttr.SetFlags( wxTEXT_ATTR_FONT | wxTEXT_ATTR_TEXT_COLOUR | wxTEXT_ATTR_BACKGROUND_COLOUR );

    wxString    strMsg;
    strMsg = event.GetMessage( );
    CUtils::DecodeXMLString( strMsg );

    wxString    strDisplayName = event.GetFromName( );
    if( event.GetRole( )  ==  RollHost )
    {
        strDisplayName.Append( _(" [Host]") );
    }
    else if( event.GetRole( )  ==  RollModerator )
    {
        strDisplayName.Append( _(" [Moderator]") );
    }

    if( event.GetSentTo( )  !=  ToAllParticipants )
    {
        strDisplayName.Append( _(" (whisper)") );
    }

    TextCtrlRecv->DisplayMessage( tAttr, strDisplayName, strMsg, true );

    if( m_strParticipantName.Cmp( event.GetFromName( ) )  !=  0 )
    {
        //  Don't display the NewChat status =message in the meeting sharing controller if we are the originator
        MeetingMainFrame    *pMMF = m_pParent;
        if( pMMF )
        {
            ShareControl    *pShare = pMMF->GetShareControl( );
            if( pShare )
            {
                pShare->NewChat( );
            }
        }
    }
}

//*****************************************************************************
void ChatPanel::StartIM( const wxString &strDisplayName, const wxString &strIMFrom )
{
    m_strParticipantName = strDisplayName;
    m_strIMFrom = strIMFrom;
}


//*****************************************************************************
void ChatPanel::StartMeeting( const wxString &strParticipantID, const wxString &strParticipantName, const wxString &strMeetingID, const wxString &strMeetingTitle, bool fJoining )
{
    m_strParticipantID = strParticipantID;
    m_strParticipantName = strParticipantName;
    m_strMeetingID = strMeetingID;
    m_strMeetingTitle = strMeetingTitle;

    SetName( m_strMeetingTitle );
}


//*****************************************************************************
void ChatPanel::EndMeeting( )
{
    if( m_pParent  &&  wxGetApp( ).m_pMC )
    {
        wxGetApp( ).m_pMC->Disconnect(wxID_ANY, wxEVT_MEETING_CHAT, MeetingChatEventHandler(ChatPanel::OnMeetingChatEvent), 0, this );
    }
}


//*****************************************************************************
//  Event Handlers
//*****************************************************************************
void ChatPanel::OnSendTextEnter(wxCommandEvent& event)
{
//    wxLogDebug( wxT("entering ChatPanel::OnSendTextEnter( )\n\r") );
}


//*****************************************************************************
void ChatPanel::OnClickFore(wxCommandEvent& event)
{
    ClearCueText();

    wxColourData    wData;
    wData.SetChooseFull( true );
    wxColour        wCol;
    TextCtrlSend->GetForeground( wCol );
    wData.SetColour( wCol );

    wxColourDialog  wColourDlg( this, &wData );
    wColourDlg.SetTitle( _("Font colour") );
    if( wColourDlg.ShowModal( )  ==  wxID_OK )
    {
        wxColourData    wRet;
        wRet = wColourDlg.GetColourData( );
        TextCtrlSend->FormatForegroundColor( wRet.GetColour( ) );
    }

    TextCtrlSend->SetFocus();
}


//*****************************************************************************
void ChatPanel::OnClickBack(wxCommandEvent& event)
{
    ClearCueText();

    wxColourData    wData;
    wData.SetChooseFull( true );
    wxColour        wCol;
    TextCtrlSend->GetBackground( wCol );
    wData.SetColour( wCol );

    wxColourDialog  wColourDlg( this, &wData );
    wColourDlg.SetTitle( _("Background colour") );
    if( wColourDlg.ShowModal( )  ==  wxID_OK )
    {
        wxColourData    wRet;
        wRet = wColourDlg.GetColourData( );
        TextCtrlSend->FormatBackgroundColor( wRet.GetColour( ) );
    }

    TextCtrlSend->SetFocus();
}


//*****************************************************************************
void ChatPanel::OnClickSmaller(wxCommandEvent& event)
{
    ClearCueText();

    TextCtrlSend->FormatFontSizeDecrease();
    TextCtrlSend->SetFocus();
}


//*****************************************************************************
void ChatPanel::OnClickNorm(wxCommandEvent& event)
{
    ClearCueText();

    TextCtrlSend->FormatFontSizeNormal();
    TextCtrlSend->SetFocus();
}


//*****************************************************************************
void ChatPanel::OnClickLarger(wxCommandEvent& event)
{
    ClearCueText();

    TextCtrlSend->FormatFontSizeIncrease();
    TextCtrlSend->SetFocus();
}


//*****************************************************************************
void ChatPanel::OnClickBold(wxCommandEvent& event)
{
    ClearCueText();

    TextCtrlSend->FormatBold( m_buttonSetBold->IsDown( ) );
    TextCtrlSend->SetFocus( );
}


//*****************************************************************************
void ChatPanel::OnClickItalics(wxCommandEvent& event)
{
    ClearCueText();

    TextCtrlSend->FormatItalic( m_buttonSetItalics->IsDown( ) );
    TextCtrlSend->SetFocus( );
}


//*****************************************************************************
void ChatPanel::OnClickUline(wxCommandEvent& event)
{
    ClearCueText();

    TextCtrlSend->FormatUnderline( m_buttonSetUline->IsDown( ) );
    TextCtrlSend->SetFocus( );
}


//*****************************************************************************
void ChatPanel::OnClickSend(wxCommandEvent& event)
{
    if (!TextCtrlSend->IsEnabled())
        return;

    ClearCueText();

    OnTextCtrlSendTextEnter( event );
}


//*****************************************************************************
//
//  This handler is not triggered if user types Shift-Enter.
//
void ChatPanel::OnTextCtrlSendTextEnter( wxCommandEvent& event )
{
//    wxLogDebug( wxT("entering ChatPanel::OnTextCtrlSendTextEnter( ) \n\r") );

    if (!TextCtrlSend->IsEnabled())
        return;

    // Send the message
    wxString strMessage;

    m_buttonSend->SetFocus();
    TextCtrlSend->GetTextAsMarkup( strMessage );

    // Reset the scrollbar for send box
    TextCtrlSend->ShowPosition( 0 );

    if (!strMessage.IsEmpty())
    {
        strMessage.Replace(_T("FACE=\"Symbol\""), _T("FACE=\"\""));

        CUtils::EncodeXMLString(strMessage);

        if( m_pParent )
            SendMeetingChat( strMessage );
        else
            SendIMMessage( strMessage );
    }
}


//*****************************************************************************
void ChatPanel::SendIMMessage( wxString strMessage )
{
    wxLogDebug( wxT("entering ChatPanel::SendIMMessage( ) \n\r") );

    session_send_im( CONTROLLER.session, m_strIMFrom.mb_str( wxConvUTF8 ), m_strIMFrom.mb_str( wxConvUTF8 ), strMessage.mb_str( wxConvUTF8 ), false );

    DisplayIM(this->m_strParticipantName, strMessage);

    // Clear the send edit control
    TextCtrlSend->ClearContents( );
    TextCtrlSend->SetFocus( );
}


//*****************************************************************************
void ChatPanel::SendMeetingChat( wxString strMessage )
{
    // Send the message and its intended recipients to the
    // main window for processing...
    controller_t    pC;
    pC = wxGetApp( ).m_pMC->controller;

    wxString    strT;
    strT.Append( m_strParticipantID );
    if( !strT.StartsWith( wxT("part:") ) )
        strT.Prepend( wxT("part:") );

    int     iSel;
    iSel = m_chatTargetCombo->GetCurrentSelection( );
    if( iSel  ==  0 ) //  "All Attendees"
    {
        controller_meeting_chat( pC, m_strMeetingID.mb_str(wxConvUTF8), ToAllParticipants,
                                 strMessage.mb_str(wxConvUTF8), strT.mb_str(wxConvUTF8), "" );
    }
    else if( iSel  ==  1 ) // "Host and Moderators"
    {
        controller_meeting_chat( pC, m_strMeetingID.mb_str(wxConvUTF8), ToModeratorParticipants,
                                 strMessage.mb_str(wxConvUTF8), strT.mb_str(wxConvUTF8), "" );
    }
    else if( iSel  ==  3 ) // not used, was to host
    {
        controller_meeting_chat( pC, m_strMeetingID.mb_str(wxConvUTF8), ToHostParticipant,
                                 strMessage.mb_str(wxConvUTF8), strT.mb_str(wxConvUTF8), "" );
    }
    
    if ( (iSel == 2) ||
         (iSel == 1 && m_iRole != RollModerator && m_iRole != RollHost) ) 
    {
        //  Don't forget to send the message to yourself so it appears in your own chat log
        controller_meeting_chat( pC, m_strMeetingID.mb_str(wxConvUTF8), ToOneParticipant,
                                 strMessage.mb_str(wxConvUTF8), strT.mb_str(wxConvUTF8), strT.mb_str(wxConvUTF8) );
    }

    //  Loop over the selected participants and individually send them the message...
    if( iSel == 2 )
    {
        int                 iNumSelected;
        wxArrayTreeItemIds  selectedItems;
        wxTreeListCtrl      *pTree;
        ParticipantPanel    *pPart;
        if( m_pParent )
            pPart = m_pParent->GetParticipantPanel( );
        else if( m_pParentChat )
            pPart = m_pParentChat->GetParticipantPanel( );
        pTree = pPart->GetParticipantTree( );
        iNumSelected = pTree->GetSelections( selectedItems );

        ParticipantTreeItemData *pPTID;
        for( int i=0; i<iNumSelected; i++ )
        {
            if( pTree->HasChildren( selectedItems.Item( i ) ) )
            {
                wxLogDebug( wxT("Selected node has CHILDREN!!\n") );
                wxTreeItemIdValue   cookie;
                wxTreeItemId        idT;
                idT = pTree->GetFirstChild( selectedItems.Item( i ), cookie );
                while( idT.IsOk( ) )
                {
                    pPTID = (ParticipantTreeItemData *) pTree->GetItemData( idT );
                    if( pPTID && pPTID->m_p != m_participantMe )
                    {
                        controller_meeting_chat( pC, m_strMeetingID.mb_str(wxConvUTF8), ToOneParticipant,
                            strMessage.mb_str(wxConvUTF8), strT.mb_str(wxConvUTF8), pPTID->m_p->part_id );
                    }
                    idT = pTree->GetNextChild( selectedItems.Item( i ), cookie );
                }
            }
            else
            {
                pPTID = (ParticipantTreeItemData *) pTree->GetItemData( selectedItems.Item( i ) );
                if( pPTID && pPTID->m_p != m_participantMe )
                {
                    controller_meeting_chat( pC, m_strMeetingID.mb_str(wxConvUTF8), ToOneParticipant,
                                             strMessage.mb_str(wxConvUTF8), strT.mb_str(wxConvUTF8), pPTID->m_p->part_id );
                }
            }
        }
    }

    // Clear the send edit control
    TextCtrlSend->ClearContents( );
    TextCtrlSend->SetFocus( );
}


//*****************************************************************************
void ChatPanel::OnTextCtrlRecvTextURL( wxTextUrlEvent& event )
{
    wxMouseEvent    mouseEv;
    mouseEv = event.GetMouseEvent( );

    if( mouseEv.LeftDown( ) )
    {
        long        lStart  = event.GetURLStart( );
        long        lEnd    = event.GetURLEnd( );
        wxString    strT( TextCtrlRecv->GetRange( lStart, lEnd ) );

        wxLaunchDefaultBrowser( strT );
    }
}


//*****************************************************************************
void ChatPanel::OnTextCtrlSendFocus(wxFocusEvent& event)
{
     ClearCueText();
}


//*****************************************************************************
void ChatPanel::OnTextCtrlSendChar(wxKeyEvent& event)
{
//    wxLogDebug( wxT("entering ChatPanel::OnTextCtrlSendChar( )\n\r") );
}


//*****************************************************************************
void ChatPanel::OnTextCtrlSendText(wxCommandEvent& event)
{
//    wxLogDebug( wxT("entering ChatPanel::OnTextCtrlSendText( )\n\r") );
}


//*****************************************************************************
//
//  OnProcessCustom
//
//    This handler keeps the state of the Bold/Italic/Uline bottons current when
//    the user is changing position or selection within the TextCtrlSend control.
//    This event handler is fired for each keystroke into the TextCtrlSend control.
//    It is also fired on Left Button Up when text is selected via the mouse.
//
//*****************************************************************************
void ChatPanel::OnProcessCustom( wxCommandEvent& WXUNUSED(event) )
{
    wxTextAttr  tAttr;
    bool        fRet;

    if( m_fSendTextCue )
        return;
        
    fRet = TextCtrlSend->GetStyleU( TextCtrlSend->GetInsertionPoint( ), tAttr );
    if( fRet )
    {
        wxFont  wFont = tAttr.GetFont( );

        if( wFont.IsOk( ) )
        {
            bool    fUline = wFont.GetUnderlined( );
            int     iFW = wFont.GetWeight( );
            long    lItalic = wFont.GetStyle( );

            m_buttonSetBold->SetDown( iFW  ==  wxFONTWEIGHT_BOLD );
            m_buttonSetItalics->SetDown( lItalic  ==  wxFONTSTYLE_ITALIC );
            m_buttonSetUline->SetDown( fUline );
        }
    }
}


//*****************************************************************************
void ChatPanel::OnInitDialog( wxInitDialogEvent &event )
{
//    wxLogDebug( wxT("Entering ChatPanel::OnInitDialog()\n\r") );
//    wxLogDebug( wxT("  leaving ChatPanel::OnInitDialog()\n\r") );
}


//*****************************************************************************
void ChatPanel::ConnectionLost( )
{
//    wxLogDebug( wxT("entering ChatPanel::ConnectionLost( )") );
}


//*****************************************************************************
void ChatPanel::ConnectionReconnected( )
{
//    wxLogDebug( wxT("entering ChatPanel::ConnectionReconnected( )") );
}


//*****************************************************************************
void ChatPanel::DisplayIM(const wxString& strFrom, const wxString &strMsg)
{
    wxTextAttr  tAttr;
    tAttr = TextCtrlRecv->GetTextAttr( );
    tAttr.SetBackgroundColour( wxColour( 255, 255, 255 ) );
    tAttr.SetTextColour( wxColour( 255, 0, 0 ) );
    tAttr.SetFlags( wxTEXT_ATTR_FONT | wxTEXT_ATTR_TEXT_COLOUR | wxTEXT_ATTR_BACKGROUND_COLOUR );

    wxString msg(strMsg);
    CUtils::DecodeXMLString( msg );

    wxString displayName( strFrom );
    CUtils::DecodeScreenName( displayName );

    TextCtrlRecv->DisplayMessage( tAttr, displayName, msg, true );
}
