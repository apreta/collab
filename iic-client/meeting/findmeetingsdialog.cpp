/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"
#include "findmeetingsdialog.h"

//(*InternalHeaders(FindMeetingsDialog)
#include <wx/bitmap.h>
#include <wx/font.h>
#include <wx/fontenum.h>
#include <wx/fontmap.h>
#include <wx/image.h>
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(FindMeetingsDialog)
//*)

BEGIN_EVENT_TABLE(FindMeetingsDialog,wxDialog)
	//(*EventTable(FindMeetingsDialog)
	//*)

EVT_BUTTON( wxID_HELP, FindMeetingsDialog::OnClickHelp)

END_EVENT_TABLE()

FindMeetingsDialog::FindMeetingsDialog(wxWindow* parent,wxWindowID id):
m_PresetData("FindMeetings")
{
	//(*Initialize(FindMeetingsDialog)
	wxXmlResource::Get()->LoadObject(this,parent,_T("FindMeetingsDialog"),_T("wxDialog"));
	m_SearchNameLabel = (wxStaticText*)FindWindow(XRCID("ID_SEARCHNAME_LABEL"));
	m_SearchNameCombo = (wxComboBox*)FindWindow(XRCID("ID_SEARCHNAME_COMBO"));
	m_SaveSearchAsBtn = (wxButton*)FindWindow(XRCID("ID_SAVESEARCHAS_BTN"));
	m_DeleteSearchBtn = (wxButton*)FindWindow(XRCID("ID_DELETESEARCH_BTN"));
	m_ScheduledWithinRadio = (wxRadioButton*)FindWindow(XRCID("ID_SCHEDULEDWITHIN_RADIO"));
	m_NextOrLastCombo = (wxComboBox*)FindWindow(XRCID("ID_NEXTORLAST_COMBO"));
	m_NextOrLastSpin = (wxSpinCtrl*)FindWindow(XRCID("ID_NEXTORLAST_SPIN"));
	m_ScheduledBetweenRadio = (wxRadioButton*)FindWindow(XRCID("ID_SCHEDULEDBETWEEN_RADIO"));
	m_FromDatePicker = (wxDatePickerCtrl*)FindWindow(XRCID("ID_FROMDATE_PICKER"));
	m_AndLabel = (wxStaticText*)FindWindow(XRCID("ID_AND_LABEL"));
	m_ToDatePicker = (wxDatePickerCtrl*)FindWindow(XRCID("ID_TODATE_PICKER"));
	m_SearchAllDatesRadio = (wxRadioButton*)FindWindow(XRCID("ID_SEARCHALLDATES_RADIO"));
	m_MeetingTitleContainsCheck = (wxCheckBox*)FindWindow(XRCID("ID_MEETINGTITLECONTAINS_CHECK"));
	m_MeetingTitleContainsEdit = (wxTextCtrl*)FindWindow(XRCID("ID_MEETINGTITLECONTAINS_EDIT"));
	m_HostsFirstNameContainsCheck = (wxCheckBox*)FindWindow(XRCID("ID_HOSTSFIRSTNAMECONTAINS_CHECK"));
	m_HostsFirstNameContainsEdit = (wxTextCtrl*)FindWindow(XRCID("ID_HOSTSFIRSTNAMECONTAINS_EDIT"));
	m_HostsLastNameContainsCheck = (wxCheckBox*)FindWindow(XRCID("ID_HOSTSLASTNAMECONTAINS_CHECK"));
	m_HostsLastNameContainsEdit = (wxTextCtrl*)FindWindow(XRCID("ID_HOSTSLASTNAMECONTAINS_EDIT"));
	m_MeetingIdCheck = (wxCheckBox*)FindWindow(XRCID("ID_MEETINGID_CHECK"));
	m_MeetingIdEdit = (wxTextCtrl*)FindWindow(XRCID("ID_MEETINGID_EDIT"));
	m_OnlyShowMeetingInProgressCheck = (wxCheckBox*)FindWindow(XRCID("ID_ONLYSHOWMEETINGSINPROGRESS_CHECK"));
	m_NumberOfResultsLabel = (wxStaticText*)FindWindow(XRCID("ID_NUMBEROFRESULTS_LABEL"));
	m_NumberOfResultsSpin = (wxSpinCtrl*)FindWindow(XRCID("ID_NUMBEROFRESULTS_SPIN"));
	Connect(XRCID("ID_SEARCHNAME_COMBO"),wxEVT_COMMAND_COMBOBOX_SELECTED,(wxObjectEventFunction)&FindMeetingsDialog::OnSearchNameComboSelect);
	Connect(XRCID("ID_SAVESEARCHAS_BTN"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&FindMeetingsDialog::OnSaveSearchAsBtnClick);
	Connect(XRCID("ID_DELETESEARCH_BTN"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&FindMeetingsDialog::OnDeleteSearchBtnClick);
	Connect(XRCID("ID_SCHEDULEDWITHIN_RADIO"),wxEVT_COMMAND_RADIOBUTTON_SELECTED,(wxObjectEventFunction)&FindMeetingsDialog::OnScheduledWithinRadioSelect);
	Connect(XRCID("ID_SCHEDULEDBETWEEN_RADIO"),wxEVT_COMMAND_RADIOBUTTON_SELECTED,(wxObjectEventFunction)&FindMeetingsDialog::OnScheduledBetweenRadioSelect);
	Connect(XRCID("ID_SEARCHALLDATES_RADIO"),wxEVT_COMMAND_RADIOBUTTON_SELECTED,(wxObjectEventFunction)&FindMeetingsDialog::OnSearchAllDatesRadioSelect);
	Connect(XRCID("ID_MEETINGTITLECONTAINS_CHECK"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&FindMeetingsDialog::OnMeetingTitleContainsCheckClick);
	Connect(XRCID("ID_HOSTSFIRSTNAMECONTAINS_CHECK"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&FindMeetingsDialog::OnHostsFirstNameContainsCheckClick);
	Connect(XRCID("ID_HOSTSLASTNAMECONTAINS_CHECK"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&FindMeetingsDialog::OnHostsLastNameContainsCheckClick);
	Connect(XRCID("ID_MEETINGID_CHECK"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&FindMeetingsDialog::OnMeetingIdCheckClick);
	//*)

	m_NextOrLastCombo->Append( _("Last") );
	m_NextOrLastCombo->Append( _("Next") );

    m_PresetData.Load();

    m_PresetData.PopulateComboBox( m_SearchNameCombo );

#ifdef __WXMAC__
	Centre();
#endif
	
}

FindMeetingsDialog::~FindMeetingsDialog()
{
}

void FindMeetingsDialog::PresetSearchCriteria( const wxString & presetName, const MeetingSearch & criteria )
{
    m_SearchNameCombo->SetStringSelection( presetName );
    m_Criteria = criteria;
    m_Criteria.m_strSearchName = presetName;
}

int FindMeetingsDialog::EditSearchCriteria()
{
    Value_to_GUI();
    int nResp = ShowModal();
    if( nResp == wxID_OK )
    {
        GUI_to_Value();
    }

    // There is no UNDO on the changes to the search criteria
    // so let's pretend the user meant what they did
    m_PresetData.Save();

    return nResp;
}

void FindMeetingsDialog::Value_to_GUI()
{
    // The radio buttons:
    if( m_Criteria.m_nDateRange == MeetingSearch::WithinNDays )
    {
        m_ScheduledWithinRadio->SetValue( true );
        m_ScheduledBetweenRadio->SetValue( false );
        m_SearchAllDatesRadio->SetValue( false );
    }else if( m_Criteria.m_nDateRange == MeetingSearch::BetweenDates )
    {
        m_ScheduledWithinRadio->SetValue( false );
        m_ScheduledBetweenRadio->SetValue( true );
        m_SearchAllDatesRadio->SetValue( false );
    }else // MeetingSearch::AllDates
    {
        m_ScheduledWithinRadio->SetValue( false );
        m_ScheduledBetweenRadio->SetValue( false );
        m_SearchAllDatesRadio->SetValue( true );
    }

    // The next/prev & spin box
    m_NextOrLastCombo->SetSelection( m_Criteria.m_fNextNDays ? 0:1 );
    m_NextOrLastSpin->SetValue( m_Criteria.m_nDays );

    // Dates: From / To
    time_t ticks = (m_Criteria.m_nStartDate) ? m_Criteria.m_nStartDate:wxDateTime::Now().GetTicks();
    wxDateTime theTime;
    theTime.Set( ticks );
    // There is a bogus ASSERT in wxDateTime::GetValue()
    // This hack bypasses the ASSERT:
    // Setting the HH:MM:SS.ms values to zero for the date picker
    theTime.SetHour(0);
    theTime.SetMinute(0);
    theTime.SetSecond(0);
    theTime.SetMillisecond(0);
    m_FromDatePicker->SetValue( theTime );

    ticks = (m_Criteria.m_nEndDate) ? m_Criteria.m_nEndDate:wxDateTime::Now().GetTicks();
    theTime.Set( ticks );
    theTime.SetHour(0);
    theTime.SetMinute(0);
    theTime.SetSecond(0);
    theTime.SetMillisecond(0);
    m_FromDatePicker->SetValue( theTime );

    // The check box / text pairs
    m_MeetingTitleContainsCheck->SetValue( m_Criteria.m_fTitle );
    m_MeetingTitleContainsEdit->SetValue( m_Criteria.m_strTitle );

    m_HostsFirstNameContainsCheck->SetValue( m_Criteria.m_fHostFirstName );
    m_HostsFirstNameContainsEdit->SetValue( m_Criteria.m_strHostFirst );

    m_HostsLastNameContainsCheck->SetValue( m_Criteria.m_fHostLastName );
    m_HostsLastNameContainsEdit->SetValue( m_Criteria.m_strHostLast );

    m_MeetingIdCheck->SetValue( m_Criteria.m_fMeetingId );
    m_MeetingIdEdit->SetValue( m_Criteria.m_strMeetingId );

    // The last bits
    m_OnlyShowMeetingInProgressCheck->SetValue( m_Criteria.m_fOnlyInProgress );
    m_NumberOfResultsSpin->SetValue( m_Criteria.m_nMaxResults );

    SetUIFeedback();
}

void FindMeetingsDialog::SetUIFeedback()
{
    bool fWithinNDays = false;
    bool fBetweenDates = false;
    if( m_Criteria.m_nDateRange == MeetingSearch::WithinNDays )
    {
        fWithinNDays = true;
    }else if( m_Criteria.m_nDateRange == MeetingSearch::BetweenDates )
    {
        fBetweenDates = true;
    }
    m_NextOrLastCombo->Enable(fWithinNDays);
    m_NextOrLastSpin->Enable(fWithinNDays);
	m_FromDatePicker->Enable(fBetweenDates);
	m_AndLabel->Enable(fBetweenDates);
	m_ToDatePicker->Enable(fBetweenDates);

	m_MeetingTitleContainsEdit->Enable( m_Criteria.m_fTitle );
	m_HostsFirstNameContainsEdit->Enable( m_Criteria.m_fHostFirstName );
	m_HostsLastNameContainsEdit->Enable( m_Criteria.m_fHostLastName );
	m_MeetingIdEdit->Enable( m_Criteria.m_fMeetingId );
}

void FindMeetingsDialog::GUI_to_Value()
{
    if( m_ScheduledWithinRadio->GetValue() )
    {
        m_Criteria.m_nDateRange = MeetingSearch::WithinNDays;
    }else if( m_ScheduledBetweenRadio->GetValue() )
    {
        m_Criteria.m_nDateRange = MeetingSearch::BetweenDates;
    }else
    {
        m_Criteria.m_nDateRange = MeetingSearch::AllDates;
    }

    // The next/prev & spin box
    m_Criteria.m_fNextNDays = m_NextOrLastCombo->GetSelection() ? true:false;
    m_Criteria.m_nDays = m_NextOrLastSpin->GetValue();

    // Dates: From/To
    wxDateTime theDate;
    theDate = m_FromDatePicker->GetValue();
    m_Criteria.m_nStartDate = theDate.GetTicks();

    theDate = m_ToDatePicker->GetValue();
    m_Criteria.m_nEndDate = theDate.GetTicks();

    // The check box / text pairs
    m_Criteria.m_fTitle = m_MeetingTitleContainsCheck->GetValue();
    m_Criteria.m_strTitle = m_MeetingTitleContainsEdit->GetValue();

    m_Criteria.m_fHostFirstName  = m_HostsFirstNameContainsCheck->GetValue();
    m_Criteria.m_strHostFirst = m_HostsFirstNameContainsEdit->GetValue();

    m_Criteria.m_fHostLastName = m_HostsLastNameContainsCheck->GetValue();
    m_Criteria.m_strHostLast = m_HostsLastNameContainsEdit->GetValue();

    m_Criteria.m_fMeetingId = m_MeetingIdCheck->GetValue();
    m_Criteria.m_strMeetingId = m_MeetingIdEdit->GetValue();

    // The last bits
    m_Criteria.m_fOnlyInProgress  = m_OnlyShowMeetingInProgressCheck->GetValue();
    m_Criteria.m_nMaxResults = m_NumberOfResultsSpin->GetValue();
}

void FindMeetingsDialog::OnSearchNameComboSelect(wxCommandEvent& event)
{
    wxArrayString newData;
    if( m_PresetData.HandleNewSelection( m_SearchNameCombo, newData ) )
    {
        // Retain the search name for the caller
        m_Criteria.m_strSearchName = m_SearchNameCombo->GetStringSelection();

        // Update the GUI with the new data
        m_Criteria.Decode( newData );
        Value_to_GUI();
    }
}

void FindMeetingsDialog::OnScheduledWithinRadioSelect(wxCommandEvent& event)
{
    m_ScheduledWithinRadio->SetValue( true );
    m_ScheduledBetweenRadio->SetValue( false );
    m_SearchAllDatesRadio->SetValue( false );
    m_Criteria.m_nDateRange = MeetingSearch::WithinNDays;
    SetUIFeedback();
}

void FindMeetingsDialog::OnScheduledBetweenRadioSelect(wxCommandEvent& event)
{
    m_ScheduledWithinRadio->SetValue( false );
    m_ScheduledBetweenRadio->SetValue( true );
    m_SearchAllDatesRadio->SetValue( false );
    m_Criteria.m_nDateRange = MeetingSearch::BetweenDates;
    SetUIFeedback();
}

void FindMeetingsDialog::OnSearchAllDatesRadioSelect(wxCommandEvent& event)
{
    m_ScheduledWithinRadio->SetValue( false );
    m_ScheduledBetweenRadio->SetValue( false );
    m_SearchAllDatesRadio->SetValue( true );
    m_Criteria.m_nDateRange = MeetingSearch::AllDates;
    SetUIFeedback();
}

void FindMeetingsDialog::OnMeetingTitleContainsCheckClick(wxCommandEvent& event)
{
   	m_Criteria.m_fTitle = m_MeetingTitleContainsCheck->GetValue();
 	m_MeetingTitleContainsEdit->Enable(m_Criteria.m_fTitle);
}

void FindMeetingsDialog::OnHostsFirstNameContainsCheckClick(wxCommandEvent& event)
{
	m_Criteria.m_fHostFirstName = m_HostsFirstNameContainsCheck->GetValue();
	m_HostsFirstNameContainsEdit->Enable(m_Criteria.m_fHostFirstName);
}

void FindMeetingsDialog::OnHostsLastNameContainsCheckClick(wxCommandEvent& event)
{
	m_Criteria.m_fHostLastName = m_HostsLastNameContainsCheck->GetValue();
	m_HostsLastNameContainsEdit->Enable(m_Criteria.m_fHostLastName);
}

void FindMeetingsDialog::OnMeetingIdCheckClick(wxCommandEvent& event)
{
	m_Criteria.m_fMeetingId = m_MeetingIdCheck->GetValue();
	m_MeetingIdEdit->Enable(m_Criteria.m_fMeetingId);
}


void FindMeetingsDialog::OnSaveSearchAsBtnClick(wxCommandEvent& event)
{
    GUI_to_Value();
    wxArrayString data;
    m_Criteria.Encode( data );
    wxString caption;
    wxString prompt;

    caption = _("Find Meetings");
    prompt  = _("Search name:");

    m_PresetData.HandleSaveAs( this, m_SearchNameCombo, caption, prompt, data );
}

void FindMeetingsDialog::OnDeleteSearchBtnClick(wxCommandEvent& event)
{
    m_PresetData.RemoveCurrentPreset( m_SearchNameCombo );
}


//*****************************************************************************
void FindMeetingsDialog::OnClickHelp(wxCommandEvent& event)
{
    wxString help( wxGetApp().Options().GetSystem( "HelpPath", "" ),  wxConvUTF8);
    help += LOCALE->GetCanonicalName( );
    help += wxT("/");
    help += wxT("searchingforcommunitymeetin.htm");
    wxLaunchDefaultBrowser( help );
}
