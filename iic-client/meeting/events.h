/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef EVENTS_H_INCLUDED
#define EVENTS_H_INCLUDED

#include "wx/defs.h"
#include "wx/event.h"

DECLARE_LOCAL_EVENT_TYPE(wxEVT_SESSION_ERROR, -1)           // Uses SessionErrorEvent
DECLARE_LOCAL_EVENT_TYPE(wxEVT_MEETING_EVENT, -1)           // Uses MeetingEvent
DECLARE_LOCAL_EVENT_TYPE(wxEVT_MEETING_ERROR, -1)           // Uses MeetingEvent
DECLARE_LOCAL_EVENT_TYPE(wxEVT_MEETING_CHAT, -1)            // Uses MeetingChatEvent
DECLARE_LOCAL_EVENT_TYPE(wxEVT_DOCSHARE_EVENT, -1)          // Uses DocshareEvent
DECLARE_LOCAL_EVENT_TYPE(wxEVT_PRESENCE_EVENT, -1)          // Uses PresenceEvent
DECLARE_LOCAL_EVENT_TYPE(wxEVT_IM_CHAT, -1)                 // Uses IMChatEvent
DECLARE_LOCAL_EVENT_TYPE(wxEVT_IM_INVITE, -1)               // Uses IMInviteEvent

// Custom wxCommandEvent types
DECLARE_LOCAL_EVENT_TYPE(wxEVT_SESSION_CONNECTED, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_CONTROLLER_REGISTERED, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_CONTROLLER_INVITE, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_CONTROLLER_INVITE_LOOKUP, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_ADDRESSBK_AUTH, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_ADDRESSBK_PROGRESS, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_SUBMEETING_ADDED, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_CONTROLLER_IM_NOTIFY, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_CONTROLLER_PING, -1)

DECLARE_LOCAL_EVENT_TYPE(wxEVT_SCHEDULE_FETCHED, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_SCHEDULE_PROGRESS, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_SCHEDULE_UPDATED, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_SCHEDULE_ERROR, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_MEETING_FETCHED, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_DIRECTORY_FETCHED, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_ADDRESSBK_UPDATE_FAILED, -1);
DECLARE_LOCAL_EVENT_TYPE(wxEVT_ADDRESS_UPDATED, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_ADDRESS_DELETED, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_ADDRESS_VERSION_CHECK, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_PROFILE_FETCHED, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_RECORDINGS_FETCHED, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_DOCUMENTS_FETCHED, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_DIRECTORY_STARTED, -1)

DECLARE_LOCAL_EVENT_TYPE(wxEVT_EXECUTE_PENDING_COMMANDS, -1);
DECLARE_LOCAL_EVENT_TYPE(wxEVT_UPDATE_RECONNECT_STATUS, -1);
//DECLARE_LOCAL_EVENT_TYPE(wxEVT_UPDATE_PROGRESS1_STATUS, -1);

DECLARE_LOCAL_EVENT_TYPE(wxEVT_SHARE_TOOLBAR_STATUS, -1);

DECLARE_LOCAL_EVENT_TYPE(wxEVT_QUEUE_EVENT, -1)

DECLARE_LOCAL_EVENT_TYPE(wxEVT_VOIP_STATUS, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_VOIP_LEVELS, -1)

DECLARE_LOCAL_EVENT_TYPE(wxEVT_MEETING_EDIT_MEETING, -1)
DECLARE_LOCAL_EVENT_TYPE(wxEVT_GET_PASSWORD, -1)

DECLARE_LOCAL_EVENT_TYPE(wxEVT_SHARE_EVENT, -1)

DECLARE_LOCAL_EVENT_TYPE(wxEVT_CONVERSION_EVENT, -1)

DECLARE_LOCAL_EVENT_TYPE(wxEVT_FILEMANAGER_DONE, -1)

//*****************************************************************************
// Custom wxEvent for Session Error.
class SessionErrorEvent : public wxEvent
{
    int m_error;
    wxString m_message;
    wxString m_rpcId;
    wxString m_strDetails;

public:
    SessionErrorEvent(int id, int err, const char *msg, const char *rpcid, const char *details) :
        wxEvent(id,wxEVT_SESSION_ERROR), m_error(err), m_message(msg, wxConvUTF8), m_rpcId(rpcid, wxConvUTF8), m_strDetails(details, wxConvUTF8)
        {
        }
    SessionErrorEvent(const SessionErrorEvent& copy) :
        wxEvent(copy), m_error(copy.m_error), m_message(copy.m_message), m_rpcId(copy.m_rpcId), m_strDetails(copy.m_strDetails)
        {
        }

    SessionErrorEvent * Clone() const
        {
            return new SessionErrorEvent(*this);
        }

    int GetErrorCode() const            { return m_error; }
    const wxString& GetMessage() const  { return m_message; }
    const wxString& GetRpcId() const    { return m_rpcId; }
    const wxString& GetDetails() const  { return m_strDetails; }
    wxString GetOpid() const
    {
        wxString opid(wxT(""));
        int ncolon = m_rpcId.Find(':');
        if( ncolon != wxNOT_FOUND )
        {
            opid = m_rpcId.Mid( ncolon + 1 );
        }
        return opid;
    }
};

typedef void (wxEvtHandler::*SessionErrorEventFunction)(SessionErrorEvent&);

#define EVT_SESSION_ERROR(id, fn) \
    DECLARE_EVENT_TABLE_ENTRY(wxEVT_SESSION_ERROR, id, wxID_ANY, (wxObjectEventFunction) (wxEventFunction) \
        wxStaticCastEvent( SessionErrorEventFunction, & fn ), (wxObject *) NULL ),

#define SessionErrorEventHandler(func) \
    (wxObjectEventFunction)(wxEventFunction)wxStaticCastEvent(SessionErrorEventFunction, &func)


//*****************************************************************************
// Custom wxEvent for Meeting Events.
class MeetingEvent : public wxEvent
{
    int m_event;
    wxString m_meeting_id;
    wxString m_sub_id;
    wxString m_part_id;

public:
    MeetingEvent(int id, int event, const char *mid, const char *subid, const char *partid) :
        wxEvent(id,wxEVT_MEETING_EVENT), m_event(event)
    {
        if( mid )
            m_meeting_id = wxString::FromAscii( mid );
        if( subid )
            m_sub_id = wxString::FromAscii( subid );
        if( partid )
            m_part_id = wxString::FromAscii( partid );
    }
    MeetingEvent(const MeetingEvent& copy) :
        wxEvent(copy),  m_event(copy.m_event), m_meeting_id(copy.m_meeting_id), m_sub_id(copy.m_sub_id), m_part_id(copy.m_part_id)
        {
        }

    MeetingEvent * Clone() const
        {
            return new MeetingEvent(*this);
        }

    int GetEvent()                      { return m_event; }
    const wxString& GetMeetingId()      { return m_meeting_id; }
    const wxString& GetSubId()          { return m_sub_id; }
    const wxString& GetParticipantId()  { return m_part_id; }
};

typedef void (wxEvtHandler::*MeetingEventFunction)(MeetingEvent&);

#define EVT_MEETING_EVENT(id, fn) \
    DECLARE_EVENT_TABLE_ENTRY(wxEVT_MEETING_EVENT, id, wxID_ANY, (wxObjectEventFunction) (wxEventFunction) \
        wxStaticCastEvent( MeetingEventFunction, & fn ), (wxObject *) NULL ),

#define MeetingEventHandler(func) \
    (wxObjectEventFunction)(wxEventFunction)wxStaticCastEvent(MeetingEventFunction, &func)


//*****************************************************************************
// Custom wxEvent for Meeting Errors.
class MeetingError : public wxEvent
{
    int         m_error;
    wxString    m_meeting_id;
    wxString    m_part_id;
    wxString    m_type;
    wxString    m_message;

    public:
        MeetingError(int id, int error, const char *mid, const char *partid, const char *type, const char *message) :
            wxEvent(id,wxEVT_MEETING_ERROR), m_error(error)
        {
            if( mid )
                m_meeting_id = wxString::FromAscii( mid );
            if( partid )
                m_part_id = wxString::FromAscii( partid );
            if( type )
                m_type = wxString::FromAscii( type );
            if( message )
                m_message = wxString::FromAscii( message );
        }
        MeetingError(const MeetingError& copy) :
            wxEvent(copy),  m_error(copy.m_error), m_meeting_id(copy.m_meeting_id), m_part_id(copy.m_part_id), m_type(copy.m_type), m_message(copy.m_message)
        {
        }

        MeetingError * Clone() const
        {
            return new MeetingError(*this);
        }

        int             GetError()          { return m_error; }
        const wxString& GetMeetingId()      { return m_meeting_id; }
        const wxString& GetParticipantId()  { return m_part_id; }
        const wxString& GetType()           { return m_type; }
        const wxString& GetMessage()        { return m_message; }
};

typedef void (wxEvtHandler::*MeetingErrorFunction)(MeetingError&);

#define EVT_MEETING_ERROR(id, fn) \
    DECLARE_EVENT_TABLE_ENTRY(wxEVT_MEETING_ERROR, id, wxID_ANY, (wxObjectEventFunction) (wxEventFunction) \
                              wxStaticCastEvent( MeetingErrorFunction, & fn ), (wxObject *) NULL ),

#define MeetingErrorHandler(func) \
    (wxObjectEventFunction)(wxEventFunction)wxStaticCastEvent(MeetingErrorFunction, &func)


//*****************************************************************************
// Custom wxEvent for Meeting Chat.
class MeetingChatEvent : public wxEvent
{
    wxString    m_strMeetingID;
    wxString    m_strMessage;
    int         m_iSentTo;
    wxString    m_strFromID;
    wxString    m_strFromName;
    int         m_iRole;
    int         m_iTime;

    public:
        MeetingChatEvent(int id, const char *msgID, const char *msg, int iSentTo, const char *fromID,
                  const char *fromName, int iRole, int iTime) :
            wxEvent(id,wxEVT_MEETING_CHAT), m_iSentTo(iSentTo), m_iRole(iRole), m_iTime(iTime)
        {
            if( msgID )
                m_strMeetingID = wxString::FromAscii( msgID );
            if( msg )
                m_strMessage = wxString::FromUTF8( msg ); 
            if( fromID )
                m_strFromID = wxString::FromAscii( fromID );
            if( fromName )
                m_strFromName = wxString::FromUTF8( fromName );
        }

        MeetingChatEvent(const MeetingChatEvent& copy) :
            wxEvent(copy),  m_strMeetingID(copy.m_strMeetingID), m_strMessage(copy.m_strMessage), m_iSentTo(copy.m_iSentTo), m_strFromID(copy.m_strFromID), m_strFromName(copy.m_strFromName), m_iRole(copy.m_iRole), m_iTime(copy.m_iTime)
        {
        }

        MeetingChatEvent * Clone() const
        {
            return new MeetingChatEvent(*this);
        }

        const wxString& GetMeetingId()      { return m_strMeetingID; }
        const wxString& GetMessage()        { return m_strMessage; }
        const wxString& GetFromID()         { return m_strFromID; }
        const wxString& GetFromName()       { return m_strFromName; }
        int GetSentTo( )                    { return m_iSentTo; }
        int GetRole( )                      { return m_iRole; }
        int GetTime( )                      { return m_iTime; }
};

typedef void (wxEvtHandler::*MeetingChatEventFunction)(MeetingChatEvent&);

#define EVT_MEETING_CHAT(id, fn) \
    DECLARE_EVENT_TABLE_ENTRY(wxEVT_MEETING_CHAT, id, wxID_ANY, (wxObjectEventFunction) (wxEventFunction) \
                              wxStaticCastEvent( MeetingChatEventFunction, & fn ), (wxObject *) NULL ),

#define MeetingChatEventHandler(func) \
    (wxObjectEventFunction)(wxEventFunction)wxStaticCastEvent(MeetingChatEventFunction, &func)


//*****************************************************************************
// Custom wxEvent for Docshare Events.
class DocshareEvent : public wxEvent
{
    int         m_event;
    wxString    m_meeting_id;
    wxString    m_presenter_id;
    wxString    m_doc_id;
    wxString    m_page_id;
    wxString    m_drawing;
    int         m_options;

    public:
        DocshareEvent(int id, int event, const char *mid, const char *presid, const char *docid, const char *pageid, const char *drawing, int options) :
            wxEvent(id,wxEVT_DOCSHARE_EVENT), m_event(event), m_options(options)
        {
            if( mid )
                m_meeting_id = wxString::FromAscii( mid );
            if( presid )
                m_presenter_id = wxString::FromAscii( presid );
            if( docid )
                m_doc_id = wxString::FromUTF8( docid );
            if( pageid )
                m_page_id = wxString::FromUTF8( pageid );
            if( drawing )
                m_drawing = wxString::FromUTF8( drawing );
        }
        DocshareEvent(const DocshareEvent& copy) :
            wxEvent(copy),  m_event(copy.m_event), m_meeting_id(copy.m_meeting_id), m_presenter_id(copy.m_presenter_id), m_doc_id(copy.m_doc_id), m_page_id(copy.m_page_id), m_drawing(copy.m_drawing), m_options(copy.m_options)
        {
        }

        DocshareEvent * Clone() const
        {
            return new DocshareEvent(*this);
        }

        DocshareEvent& operator=(const DocshareEvent&);

        int GetEvent()                      { return m_event; }
        const wxString& GetMeetingId()      { return m_meeting_id; }
        const wxString& GetPresenterId()    { return m_presenter_id; }
        const wxString& GetDocId()          { return m_doc_id; }
        const wxString& GetPageId()         { return m_page_id; }
        const wxString& GetDrawing()        { return m_drawing; }
        int GetOptions( )                   { return m_options; }
        void SetDrawing( wxString& strIn )  { m_drawing = strIn; }
};

typedef void (wxEvtHandler::*DocshareEventFunction)(DocshareEvent&);

#define EVT_DOCSHARE_EVENT(id, fn) \
    DECLARE_EVENT_TABLE_ENTRY(wxEVT_DOCSHARE_EVENT, id, wxID_ANY, (wxObjectEventFunction) (wxEventFunction) \
                              wxStaticCastEvent( DocshareEventFunction, & fn ), (wxObject *) NULL ),

#define DocshareEventHandler(func) \
    (wxObjectEventFunction)(wxEventFunction)wxStaticCastEvent(DocshareEventFunction, &func)


//*****************************************************************************
// Custom wxEvent for Presence Events.
class PresenceEvent : public wxEvent
{
    wxString m_system;
    wxString m_screen;
    int m_presence;
    wxString m_status;

public:
    PresenceEvent(int id, const wxString& system, const wxString& screen, int presence, const wxString& status) :
        wxEvent(id,wxEVT_PRESENCE_EVENT),
        m_system(system),
        m_screen(screen),
        m_presence(presence),
        m_status(status)
    {
    }
    PresenceEvent(const PresenceEvent& copy) :
        wxEvent(copy),  m_screen(copy.m_screen), m_presence(copy.m_presence), m_status(copy.m_status)
        {
        }

    PresenceEvent * Clone() const
        {
            return new PresenceEvent(*this);
        }

    const wxString& GetSystem()         { return m_system; }
    const wxString& GetScreen()         { return m_screen; }
    int GetPresence()                   { return m_presence; }
    const wxString& GetStatus()         { return m_status; }
};

typedef void (wxEvtHandler::*PresenceEventFunction)(PresenceEvent&);

#define EVT_PRESENCE_EVENT(id, fn) \
    DECLARE_EVENT_TABLE_ENTRY(wxEVT_PRESENCE_EVENT, id, wxID_ANY, (wxObjectEventFunction) (wxEventFunction) \
        wxStaticCastEvent( PresenceEventFunction, & fn ), (wxObject *) NULL ),

#define PresenceEventHandler(func) \
    (wxObjectEventFunction)(wxEventFunction)wxStaticCastEvent(PresenceEventFunction, &func)


//*****************************************************************************
// Custom wxEvent for IM Chat.
class IMChatEvent : public wxEvent
{
    wxString    m_strUser;
    wxString    m_strServer;
    wxString    m_strResource;
    wxString    m_strBody;
    int         m_iType;

public:
    enum { Normal, Composing, ClearComposing };

    IMChatEvent( int id, const char *user, const char *server, const char *resource,
                 const char *body, int composing ) :
        wxEvent(id,wxEVT_IM_CHAT)
    {
        if( user )
            m_strUser = wxString::FromUTF8( user );
        if( server )
            m_strServer = wxString::FromAscii( server );
        if( resource )
            m_strResource = wxString::FromAscii( resource );
        if( body )
        {
            m_strBody = wxString::FromUTF8( body );
            m_iType = Normal;
        }
        else 
        { 
            if ( composing )
                m_iType = Composing;
            else
                m_iType = ClearComposing;
        }
            
    }

    IMChatEvent(const IMChatEvent& copy) :
        wxEvent(copy),  m_strUser(copy.m_strUser), m_strServer(copy.m_strServer), m_strResource(copy.m_strResource), m_strBody(copy.m_strBody), m_iType(copy.m_iType)
    {
    }

    IMChatEvent * Clone() const
    {
        return new IMChatEvent(*this);
    }

    const wxString& GetUser()       { return m_strUser; }
    const wxString& GetServer()     { return m_strServer; }
    const wxString& GetResource()   { return m_strResource; }
    const wxString& GetBody()       { return m_strBody; }
    int GetType( )                  { return m_iType; }
};

typedef void (wxEvtHandler::*IMChatEventFunction)(IMChatEvent&);

#define EVT_IM_CHAT(id, fn) \
    DECLARE_EVENT_TABLE_ENTRY(wxEVT_IM_CHAT, id, wxID_ANY, (wxObjectEventFunction) (wxEventFunction) \
                              wxStaticCastEvent( IMChatEventFunction, & fn ), (wxObject *) NULL ),

#define IMChatEventHandler(func) \
    (wxObjectEventFunction)(wxEventFunction)wxStaticCastEvent(IMChatEventFunction, &func)


//*****************************************************************************
// Custom wxEvent for IM Invite.
class IMInviteEvent : public wxEvent
{
    wxString    m_strMID;
    wxString    m_strTitle;
    wxString    m_strDesc;
    wxString    m_strMessage;
    wxString    m_strPID;
    wxString    m_strName;

    public:
        IMInviteEvent( int id, const char *mid, const char *title, const char *desc,
                       const char *message, const char *pid, const char *name ) :
            wxEvent(id,wxEVT_IM_INVITE)
        {
            if( mid )
                m_strMID = wxString::FromAscii( mid );
            if( title )
                m_strTitle = wxString::FromUTF8( title );
            if( desc )
                m_strDesc = wxString::FromUTF8( desc );
            if( message )
                m_strMessage = wxString::FromUTF8( message );
            if( pid )
                m_strPID = wxString::FromAscii( pid );
            if( name )
                m_strName = wxString::FromUTF8( name );
        }

        IMInviteEvent(const IMInviteEvent& copy) :
            wxEvent(copy),  m_strMID(copy.m_strMID), m_strTitle(copy.m_strTitle), m_strDesc(copy.m_strDesc), m_strMessage(copy.m_strMessage), m_strPID(copy.m_strPID), m_strName(copy.m_strName)
        {
        }

        IMInviteEvent * Clone() const
        {
            return new IMInviteEvent(*this);
        }

        const wxString& GetMID()        { return m_strMID; }
        const wxString& GetTitle()      { return m_strTitle; }
        const wxString& GetDesc()       { return m_strDesc; }
        const wxString& GetMessage()    { return m_strMessage; }
        const wxString& GetPID()        { return m_strPID; }
        const wxString& GetName()       { return m_strName; }
};

typedef void (wxEvtHandler::*IMInviteEventFunction)(IMInviteEvent&);

#define EVT_IM_INVITE(id, fn) \
    DECLARE_EVENT_TABLE_ENTRY(wxEVT_IM_INVITE, id, wxID_ANY, (wxObjectEventFunction) (wxEventFunction) \
                              wxStaticCastEvent( IMInviteEventFunction, & fn ), (wxObject *) NULL ),

#define IMInviteEventHandler(func) \
    (wxObjectEventFunction)(wxEventFunction)wxStaticCastEvent(IMInviteEventFunction, &func)

#endif // EVENTS_H_INCLUDED
