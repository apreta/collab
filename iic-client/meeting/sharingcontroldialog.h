/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef SHARINGCONTROLDIALOG_H
#define SHARINGCONTROLDIALOG_H

//(*Headers(SharingControlDialog)
#include <wx/stattext.h>
#include <wx/bmpbuttn.h>
#include <wx/statbmp.h>
#include <wx/button.h>
#include <wx/frame.h>
//*)

#include <wx/minifram.h>


class ShareControl;


class SharingControlDialog: public wxFrame
{
	public:

		SharingControlDialog(wxWindow* parent,wxWindowID id = -1);
		virtual ~SharingControlDialog();

		//(*Identifiers(SharingControlDialog)
		//*)

    public:
        void        SetShareControl( ShareControl *pSC )        { m_pSC = pSC; }
        void        PostUpdateStatus( int iNewStatus ); /* Handle status update from background thread */
        void        UpdateStatus( int iNewStatus );
        void        Handup( wxString strName, bool fHandup );
        void        NewChat( );
        void        ShowToolbar( bool fMeetingVisible );
        void        HideToolbar( );
        void        Recording( bool fRecording );
        void        Holding( bool fHolding );
        void        Moderator( bool fMod );
        void        SetCommand( int ID, bool fCheck );

	protected:
		//(*Handlers(SharingControlDialog)
		void OnBtnViewMeetingClick(wxCommandEvent& event);
		void OnBtnStopSharingClick(wxCommandEvent& event);
		void OnBtnRefreshClick(wxCommandEvent& event);
		void OnBtnOptionsClick(wxCommandEvent& event);
		//*)
        void OnBtnStatusClick(wxCommandEvent& event);
        void OnUpdateStatus(wxCommandEvent& event);

        void        OnCaptureLost( wxMouseCaptureLostEvent& event );
        void        OnLButtonDown( wxMouseEvent& event );
        void        OnLButtonUp( wxMouseEvent& event );
        void        OnMotion( wxMouseEvent& event );
        void        OnTimer( wxTimerEvent& event );

		//(*Declarations(SharingControlDialog)
		wxBitmapButton* m_BtnOptions2;
		wxButton* m_BtnOptions;
		wxButton* m_ButtonStatus;
		wxButton* m_BtnStopSharing;
		wxStaticBitmap* StaticBitmap1;
		wxButton* m_BtnViewMeeting;
		wxButton* m_ButtonHidden;
		wxStaticText* m_StaticStatus;
		wxButton* m_BtnRefresh;
		//*)

    private:
        void        FirstDisplay( );
        void        ShowStatus( bool fShow, bool fDramatic = false );
        void        SetLabel(wxButton *button, wxString label);

    private:
        wxTimer         m_timer;
        int             m_iFlashDelay;
        int             m_iMinHeight;       //  Min height of dialog - no status showing
        int             m_iStatusHeight;    //  Height with status text showing
        int             m_iMaxHeight;       //  Height with status button showing
        int             m_iNormalWidth;     //  This doesn't change - just cache it to avoid looking it up again
        bool            m_fMtgIsVisible;
        bool            m_fPresenting;
        bool            m_fFirstDisplay;
        int             m_iPosX;
        int             m_iPosY;
        ShareControl    *m_pSC;
        wxMenu          *m_pPopupMenu;
        wxWindow        *m_pMeetingWindow;

        bool    m_fRecording;
        bool    m_fHolding;
        bool    m_fDragging;
        bool    m_fOpeningStatus;
        bool    m_fLeaveStatusClosed;
        int     m_iOffsetX;
        int     m_iOffsetY;

		DECLARE_EVENT_TABLE()
};

#endif
