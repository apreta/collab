/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"
#include "docshareindexdialog.h"

//(*InternalHeaders(DocShareIndexDialog)
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(DocShareIndexDialog)
//*)

BEGIN_EVENT_TABLE(DocShareIndexDialog,wxDialog)
	//(*EventTable(DocShareIndexDialog)
	//*)
END_EVENT_TABLE()

DocShareIndexDialog::DocShareIndexDialog(wxWindow* parent, PAGEVECTOR &saPages, bool fReadOnly )
    : m_saPages(saPages), m_fReadOnly(fReadOnly), m_timer( this )
{
	//(*Initialize(DocShareIndexDialog)
	wxXmlResource::Get()->LoadObject(this,parent,_T("DocShareIndexDialog"),_T("wxDialog"));
	m_listCtrl = (wxListCtrl*)FindWindow(XRCID("ID_DLG_DSINDEX_LIST_CTRL"));
	m_btnOK = (wxButton*)FindWindow(XRCID("ID_DLG_DSINDEX_BTN_OK"));
	m_btnCancel = (wxButton*)FindWindow(XRCID("ID_DLG_DSINDEX_BTN_CANCEL"));
	m_btnMoveUp = (wxButton*)FindWindow(XRCID("ID_DLG_DSINDEX_BTN_MOVEUP"));
	m_btnMoveDown = (wxButton*)FindWindow(XRCID("ID_DLG_DSINDEX_BTN_MOVEDOWN"));
	m_btnRename = (wxButton*)FindWindow(XRCID("ID_DLG_DSINDEX_BTN_RENAME"));
	m_btnDelete = (wxButton*)FindWindow(XRCID("ID_DLG_DSINDEX_BTN_DELETE"));
	
	Connect(XRCID("ID_DLG_DSINDEX_LIST_CTRL"),wxEVT_COMMAND_LIST_END_LABEL_EDIT,(wxObjectEventFunction)&DocShareIndexDialog::OnEndLabelEdit);
	Connect(XRCID("ID_DLG_DSINDEX_BTN_OK"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&DocShareIndexDialog::OnClickOK);
	Connect(XRCID("ID_DLG_DSINDEX_BTN_CANCEL"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&DocShareIndexDialog::OnClickCancel);
	Connect(XRCID("ID_DLG_DSINDEX_BTN_MOVEUP"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&DocShareIndexDialog::OnClickMoveUp);
	Connect(XRCID("ID_DLG_DSINDEX_BTN_MOVEDOWN"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&DocShareIndexDialog::OnClickMoveDown);
	Connect(XRCID("ID_DLG_DSINDEX_BTN_RENAME"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&DocShareIndexDialog::OnClickRename);
	Connect(XRCID("ID_DLG_DSINDEX_BTN_DELETE"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&DocShareIndexDialog::OnClickDelete);
	//*)

    m_fDirty = false;

    wxString    strColumnHeader( _("Name"), wxConvUTF8 );
    m_listCtrl->InsertColumn( 0, strColumnHeader );

    for( int i=0; i<(int)m_saPages.size(); i++ )
    {
        m_listCtrl->InsertItem( i, m_saPages.at(i).m_strTitle );
    }

    m_listCtrl->SetItemState( 0, wxLIST_STATE_SELECTED, wxLIST_STATE_SELECTED );

    Connect( wxID_ANY, wxEVT_TIMER, wxTimerEventHandler(DocShareIndexDialog::OnTimer) );

    m_timer.Start( 500, wxTIMER_ONE_SHOT );
}


DocShareIndexDialog::~DocShareIndexDialog()
{
    m_timer.Stop( );
    Disconnect( wxID_ANY, wxEVT_TIMER, wxTimerEventHandler(DocShareIndexDialog::OnTimer) );
	//(*Destroy(DocShareIndexDialog)
	//*)
}


void DocShareIndexDialog::OnTimer( wxTimerEvent& event )
{
    int     count = m_listCtrl->GetSelectedItemCount( );
    if( count == 1 )
    {
        int     index = m_listCtrl->GetNextItem( -1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED );
        count = m_listCtrl->GetItemCount( );
        m_btnMoveUp->  Enable( (index == 0 || m_fReadOnly) ? false : true );
        m_btnMoveDown->Enable( (index == (count-1) || m_fReadOnly) ? false : true );
        m_btnRename->  Enable( m_fReadOnly ? false : true );
        m_btnDelete->  Enable( (index == -1 || count <= 1 || m_fReadOnly) ? false : true );
    }

    m_timer.Start( 500, wxTIMER_ONE_SHOT );
}


void DocShareIndexDialog::OnClickOK(wxCommandEvent& event)
{
    for( int i=0; i<(int)m_saPages.size( ); i++ )
    {
        CDocPage    docPage = m_saPages.at( i );
        wxLogDebug( wxT("Page %d has title of %s"), i, docPage.m_strTitle.wc_str( ) );
    }
    EndModal( wxID_OK );
}

void DocShareIndexDialog::OnClickCancel(wxCommandEvent& event)
{
    EndModal( wxID_CANCEL );
}

void DocShareIndexDialog::OnClickMoveUp(wxCommandEvent& event)
{
    int     count = m_listCtrl->GetSelectedItemCount( );
    if( count == 1 )
    {
        int     index = m_listCtrl->GetNextItem( -1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED );
        CDocPage    docPage = m_saPages.at( index );
        m_saPages.erase( m_saPages.begin( ) + index );
        m_listCtrl->DeleteItem( index );

        m_listCtrl->InsertItem( --index, docPage.m_strTitle );
        m_saPages.insert( m_saPages.begin( ) + index, docPage );

        m_listCtrl->SetItemState( index, wxLIST_STATE_SELECTED, wxLIST_STATE_SELECTED );
        m_listCtrl->EnsureVisible( index );

        m_fDirty = true;
    }
}

void DocShareIndexDialog::OnClickMoveDown(wxCommandEvent& event)
{
    int     count = m_listCtrl->GetSelectedItemCount( );
    if( count == 1 )
    {
        int index = m_listCtrl->GetNextItem( -1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED );
        CDocPage    docPage = m_saPages.at( index );
        m_saPages.erase( m_saPages.begin( ) + index );
        m_listCtrl->DeleteItem( index );

        m_listCtrl->InsertItem( ++index, docPage.m_strTitle );
        m_saPages.insert( m_saPages.begin( ) + index, docPage );

        m_listCtrl->SetItemState( index, wxLIST_STATE_SELECTED, wxLIST_STATE_SELECTED );
        m_listCtrl->EnsureVisible( index );

        m_fDirty = true;
    }
}

void DocShareIndexDialog::OnClickRename(wxCommandEvent& event)
{
    int     count = m_listCtrl->GetSelectedItemCount( );
    if( count == 1 )
    {
        int index = m_listCtrl->GetNextItem( -1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED );
        m_listCtrl->SetFocus( );
        m_listCtrl->EditLabel( index );
    }
}

void DocShareIndexDialog::OnClickDelete(wxCommandEvent& event)
{
    int     count = m_listCtrl->GetSelectedItemCount( );
    if( count == 1  &&  m_listCtrl->GetItemCount( ) > 1 )
    {
        wxString    strTitle( _("Please Confirm"), wxConvUTF8 );
        wxString    strMessage( _("Are you sure that you want to delete the selected page?"), wxConvUTF8 );
        wxMessageDialog     dlg( this, strMessage, strTitle, wxYES_NO | wxICON_QUESTION );
        if( dlg.ShowModal( )  !=  wxID_YES )
            return;

        int index = m_listCtrl->GetNextItem( -1, wxLIST_NEXT_ALL, wxLIST_STATE_SELECTED );
//        CDocPage    docPage = m_saPages.at( index );
        m_saPages.erase( m_saPages.begin( ) + index );
        m_listCtrl->DeleteItem( index );
        if( index  ==  m_listCtrl->GetItemCount( ) )
        {
            index = m_listCtrl->GetItemCount( ) - 1;
        }

        m_listCtrl->SetItemState( index, wxLIST_STATE_SELECTED, wxLIST_STATE_SELECTED );
        m_listCtrl->EnsureVisible( index );

        m_fDirty = true;
    }
}



void DocShareIndexDialog::SetReadOnly( bool fReadOnly )
{
    m_fReadOnly = fReadOnly;
}

void DocShareIndexDialog::OnEndLabelEdit(wxListEvent& event)
{
    int         index   = event.GetIndex( );
    wxString    strNew  = event.GetLabel( );

    if( strNew.Len( ) > 0 )
    {
        // Make sure name is unique
        for( int i=0; i<(int)m_saPages.size(); i++ )
        {
            if( i  !=  index )
            {
                CDocPage    &page = m_saPages.at( i );
                if( page.m_strTitle.Cmp( strNew )  ==  0 )
                {
                    wxString    strTitle( _("Please Note"), wxConvUTF8 );
                    wxString    strMessage( _("Name is duplicated with another page name."), wxConvUTF8 );
                    wxMessageDialog dlg( this, strMessage, strTitle, wxOK | wxICON_ERROR );
                    dlg.ShowModal( );

                    page = m_saPages.at( index );
                    m_listCtrl->SetItemText( index, page.m_strTitle );
                    return;
                }
            }
        }

        CDocPage    &docPage = m_saPages.at( index );
        docPage.m_strTitle.Empty( );
        docPage.m_strTitle.Append( strNew );
//        m_saPages.at( index, docPage );
        m_listCtrl->SetItemText( index, strNew );

        m_fDirty = true;
    }
}
