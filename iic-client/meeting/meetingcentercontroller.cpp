/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 *
 *
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */
#include "app.h"
#include "meetingcentercontroller.h"
#include "meetingsetupdialog.h"
#include "meetingmainframe.h"
#include "connectionprogressdialog.h"
#include "reconnectdialog.h"
#include "htmlgen.h"
#include "findmeetingsdialog.h"
#include "aboutdialog.h"
#include "progress1dialog.h"
#include "chatmainframe.h"
#include "badauthcertdialog.h"

//(*InternalHeaders(MeetingCenterController)
#include <wx/xrc/xmlres.h>
//*)

#include <wx/stdpaths.h>
#include <wx/datetime.h>
#include <wx/statline.h>
#include <wx/html/htmlwin.h>
#include <wx/defs.h>
#include <wx/process.h>
#include <wx/mstream.h>
#include <wx/stopwatch.h>
#include <wx/dynlib.h>

#include "schedule.h"
#include "managecontactsframe.h"
#include "createcontactdialog.h"
#include "installcontroller.h"
#include "recordingdialog.h"
#include "invitedialog.h"
#include "pcphonecontroller.h"
#include "webdav.h"
#include "userpreferencesdialog.h"
#include "iicdoc.h"
#include "docshareview.h"
#include "logupload.h"
#include "imageuploaddialog.h"
#include "filemanager.h"

#include "fileversion.h"
#include "version.h"
#include "base64.h"

#ifdef MACOSX
#include <wx/imaglist.h>
#endif



MeetingTreeItemData::MeetingTreeItemData( int nType, meeting_info_t info ):
CommonTreeItemData(nType)
{
    if( info )
    {
        m_meetingId = wxString(info->meeting_id, wxConvUTF8);
        m_title = wxString(info->title, wxConvUTF8);
        m_participantId = wxString( info->my_participant_id, wxConvUTF8 );
        m_meetingType = info->type;
        m_meetingRole = info->my_role;
        m_options = info->options;
        m_start_time = info->start_time;
    }
    else
    {
        wxASSERT(info);
    }
}

MeetingTreeItemData::MeetingTreeItemData( int nType ):
CommonTreeItemData(nType),
m_meetingType(-1),
m_meetingRole(-1),
m_options(0)
{
    m_meetingId = wxT("");
    m_participantId = wxT("");
}


//  Turn this into an option
int kPingInterval         = 40000;  //  Make it 40 seconds for now
int kAnonSignOnTriesLimit = 3;      //  Avoid infinite loop - Limit the number of signon retries for Anon users


TaskBarIcon::TaskBarIcon(MeetingCenterController * pMMC)
{
    m_pMMC = pMMC;
    Connect(wxID_ANY, wxEVT_TASKBAR_LEFT_DOWN, wxTaskBarIconEventHandler(TaskBarIcon::OnLeftClick));
}

wxMenu * TaskBarIcon::CreatePopupMenu()
{
    wxMenu *menu = new wxMenu();

    menu->Append( MeetingCenterController::Meeting_Center, _("Show Meetings..."), _("Show meeting center."), wxITEM_NORMAL );
    menu->Append( MeetingCenterController::Contacts_Manage, _("Show Contacts..."), _("Show contact and buddy list."), wxITEM_NORMAL );
    menu->AppendSeparator();
    menu->Append( wxID_EXIT, _("Exit"), _("Exit the meeting client"), wxITEM_NORMAL );

    if (!m_pMMC->AlreadySignedOn())
    {
        menu->Enable(MeetingCenterController::Meeting_Center, false);
        menu->Enable(MeetingCenterController::Contacts_Manage, false);
    }
    return menu;
}

void TaskBarIcon::OnLeftClick(wxTaskBarIconEvent& event)
{
    PopupMenu(CreatePopupMenu());
}


//(*IdInit(MeetingCenterController)
//*)

BEGIN_EVENT_TABLE(MeetingCenterController,wxFrame)
	//(*EventTable(MeetingCenterController)
	//*)
END_EVENT_TABLE()

MeetingCenterController::MeetingCenterController(wxWindow* parent_window,wxWindowID id):
m_MainMenuBar(0),
m_ContactsMenu(0),
m_MeetingsMenu(0),
m_DocumentsMenu(0),
m_OptionsMenu(0),
m_SearchMenu(0),
m_HelpMenu(0),
m_MeetingPopup(0),
m_SearchPopup(0),
session(0),
controller(0),
book(0),
sched(0),
m_ControllerState(State_Idle),
m_cab(0),
m_pab(0),
parent(parent_window),
m_pReconnectDialog(0),
m_join_inv(0),
m_pDialogInvite(0),
m_fPublicMeetingsAreLoaded( false ),
m_fInvitedMeetingsAreLoaded( false ),
m_fMyMeetingsAreLoaded( false ),
m_fIMClientMode( false ),
m_fInstantMeeting( false ),
m_fPublicMeetingsExpanded(false),
m_fMyMeetingsExpanded(true),
m_fPastMeetingsExpanded(false),
m_fUnscheduledMeetingsExpanded(true),
m_fMyInstantMeetingExpanded(true),
m_fInvitedMeetingsExpanded(true),
m_fCommunitySearchExpanded(false),
m_profile(0),
m_nCmdMask(0),
m_fHaveGUI(false),
m_nSearchNumber(0),
m_nLastSortColumn(wxNOT_FOUND),
m_fLastSortAscending(true),
m_nDatabaseError(0),
m_pConnectionProgressDialog(0),
m_pMeetingSetupDialog(0),
m_PendingTimer(this,Pending_Timer),
m_PingTimer(this,Ping_Timer),
m_iPingsSent(0),
m_iPingsReceived(0),
m_pManageContactsFrame(0),
m_pInstallController(0),
m_bLaunchInstallerAtExit( false ),
m_fCheckVersionCompleted( false ),
m_fForceApplicationExit( false ),
m_pProgressOfContactLoad(0),
m_iAnonSignOnTries(0),
m_pSignOnDlg(0),
m_pChatMainFrame(0),
m_pContactService(0)
{
    // NOTE: The GUI construction code has been move to the DisplayMeetingCenterWindow()
    // method. This was done to handle the cases where the primary UI is NOT the meeting center window.

    Initialize();
}

MeetingCenterController::~MeetingCenterController()
{
    wxLogDebug( wxT("entering MeetingCenterController DESTRUCTOR") );

    if( m_pProgressOfContactLoad )
        delete m_pProgressOfContactLoad;    //  This should already be Destroyed

    if( m_pInstallController )
    {
        delete m_pInstallController;
        m_pInstallController = NULL;
    }

    m_PresenceManager.Disconnect();
    
    if( m_pContactService )
    {
        delete m_pContactService;
    }
}

void MeetingCenterController::Initialize()
{
    m_SignOnParams.m_fAnonymousUser = false;

    wxString userDir = wxStandardPaths::Get().GetUserDataDir();

    session_initialize(userDir.mb_str()); // THIS MUST BE FIRST

    if (!ReadPreferences())
    {
        wxMessageBox(_("Error reading system configuration file.\nRe-installing the application may fix this problem."), wxT("Meeting Center"), wxOK | wxICON_EXCLAMATION);
    }

    ProcessCertificates(userDir);

    m_PresenceManager.Connect( new PresenceHandlerPtr<MeetingCenterController>(this, &MeetingCenterController::PresenceUpdated) );

    // iic session API
    Connect(wxID_ANY, wxEVT_SESSION_CONNECTED,       wxCommandEventHandler( MeetingCenterController::OnConnectEvent) );
    Connect(wxID_ANY, wxEVT_CONTROLLER_REGISTERED,   wxCommandEventHandler( MeetingCenterController::OnRegisteredEvent) );
    Connect(wxID_ANY, wxEVT_CONTROLLER_INVITE_LOOKUP, wxCommandEventHandler( MeetingCenterController::OnControllerInviteLookupEvent) );
    Connect(wxID_ANY, wxEVT_CONTROLLER_INVITE,       wxCommandEventHandler( MeetingCenterController::OnControllerInviteEvent) );
    Connect(wxID_ANY, wxEVT_ADDRESSBK_AUTH,          wxCommandEventHandler( MeetingCenterController::OnAuthEvent) );
    Connect(wxID_ANY, wxEVT_ADDRESSBK_PROGRESS,      wxCommandEventHandler( MeetingCenterController::OnAddressbkProgressEvent) );
    Connect(wxID_ANY, wxEVT_SCHEDULE_FETCHED,        wxCommandEventHandler( MeetingCenterController::OnScheduleFetched) );
    Connect(wxID_ANY, wxEVT_MEETING_FETCHED,         wxCommandEventHandler( MeetingCenterController::OnMeetingFetched) );
    Connect(wxID_ANY, wxEVT_DIRECTORY_FETCHED,       wxCommandEventHandler( MeetingCenterController::OnDirectoryFetched) );
    Connect(wxID_ANY, wxEVT_SESSION_ERROR,           SessionErrorEventHandler(MeetingCenterController::OnSessionError) );
    Connect(wxID_ANY, wxEVT_MEETING_EVENT,           MeetingEventHandler(MeetingCenterController::OnMeetingEvent) );
    Connect(wxID_ANY, wxEVT_MEETING_ERROR,           MeetingErrorHandler(MeetingCenterController::OnMeetingError) );
    Connect(wxID_ANY, wxEVT_MEETING_CHAT,            MeetingChatEventHandler(MeetingCenterController::OnMeetingChatEvent) );
    Connect(wxID_ANY, wxEVT_SCHEDULE_PROGRESS,       wxCommandEventHandler(MeetingCenterController::OnScheduleProgress) );
    Connect(wxID_ANY, wxEVT_SCHEDULE_UPDATED,        wxCommandEventHandler(MeetingCenterController::OnScheduleUpdated) );
    Connect(wxID_ANY, wxEVT_SCHEDULE_ERROR,          wxCommandEventHandler(MeetingCenterController::OnScheduleError) );
    Connect(wxID_ANY, wxEVT_ADDRESSBK_UPDATE_FAILED, wxCommandEventHandler(MeetingCenterController::OnAddressbkUpdateFailed) );
    Connect(wxID_ANY, wxEVT_ADDRESS_UPDATED,         wxCommandEventHandler(MeetingCenterController::OnAddressUpdated) );
    Connect(wxID_ANY, wxEVT_ADDRESS_DELETED,         wxCommandEventHandler(MeetingCenterController::OnAddressDeleted) );
    Connect(wxID_ANY, wxEVT_ADDRESS_VERSION_CHECK,   wxCommandEventHandler(MeetingCenterController::OnAddressVersionCheck) );
    Connect(wxID_ANY, wxEVT_CONTROLLER_IM_NOTIFY,    wxCommandEventHandler(MeetingCenterController::OnControllerIMNotify) );
    Connect(wxID_ANY, wxEVT_CONTROLLER_PING,         wxCommandEventHandler(MeetingCenterController::OnControllerPingSucceeded) );
    Connect(wxID_ANY, wxEVT_IM_CHAT,                 IMChatEventHandler(MeetingCenterController::OnIMChatEvent) );

    Connect( Tools_ExecutePendingCommands, wxEVT_EXECUTE_PENDING_COMMANDS, wxCommandEventHandler(MeetingCenterController::OnExecutePendingCommands) );
    Connect( Pending_Timer, wxEVT_TIMER, wxTimerEventHandler(MeetingCenterController::OnExecutePendingTimer) );
    Connect( Ping_Timer,    wxEVT_TIMER, wxTimerEventHandler(MeetingCenterController::OnExecutePingTimer) );

    m_pTaskBarIcon = new TaskBarIcon(this);

#if defined(WIN32)
    wxIcon icon( wxGetApp().GetResourceFile(_T("res/zon.ico")), wxBITMAP_TYPE_ICO );
    m_pTaskBarIcon->SetIcon( icon );
#elif defined(LINUX)
    wxIcon icon( wxGetApp().GetResourceFile(_T("res/zon.xpm")), wxBITMAP_TYPE_XPM );
    m_pTaskBarIcon->SetIcon( icon );
#endif
	// OSX will use icon from bundle automatically
	
    m_pTaskBarIcon->Connect( Meeting_Center, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnMeetingCenter), NULL, this );
    m_pTaskBarIcon->Connect( Contacts_Manage, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnContactsManage), NULL, this );
    m_pTaskBarIcon->Connect( wxID_EXIT, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnExit), NULL, this );
    m_pTaskBarIcon->Connect( wxID_ANY, wxEVT_TASKBAR_LEFT_DCLICK, wxTaskBarIconEventHandler(MeetingCenterController::OnTaskIconDblClick), NULL, this );

    m_pContactService = new ContactService(*this);
}

bool MeetingCenterController::ReadPreferences()
{
    bool fOK = false;

    // NOTE: m_lastScreen is not known right now.
    // The system level preferences are read in first to find the last screen name.
    wxString dummyScreen(wxT("temp"));

    wxGetApp().Options().Initialize( dummyScreen );
    if( wxGetApp().Options().Read() )
    {
        int     iVersion = OPTIONS.GetSystemInt( "Version", 0 );

        //  lastpwd is Base64 encoded starting with file version 5
        if( iVersion > 4 )
        {
            CBase64     base64;
            int         iLenDecoded;
            char        pwdDecoded[ 128 ];
            memset( pwdDecoded, 0, 128 );
            iLenDecoded = base64.Decode( wxGetApp().Options().GetSystem( "lastpwd", "" ), pwdDecoded );
            pwdDecoded[ iLenDecoded ] = 0;
            m_SignOnParams.m_strPassword = wxString::FromAscii( pwdDecoded );
        }
        else
        {
            m_SignOnParams.m_strPassword = wxString::FromAscii( wxGetApp().Options().GetSystem( "lastpwd", "" ) );
        }

        m_SignOnParams.m_strServer = wxString::FromAscii( wxGetApp().Options().GetSystem( "lastserver", "" ) );
        m_SignOnParams.m_strServer = wxString::FromAscii( wxGetApp().Options().GetSystem( "lastserver", "" ) );
        m_SignOnParams.m_strScreen = wxString::FromUTF8( wxGetApp().Options().GetSystem( "lastuser", "" ) );

        if (m_SignOnParams.m_strServer.IsEmpty())
        {
            m_SignOnParams.m_strServer = wxString::FromAscii( wxGetApp().Options().GetSystem( "server", "" ) );
        }
        m_SignOnParams.m_fHideServerEntry = wxGetApp().Options().GetSystemBool( "HideServerEntry", false );

        // If we have a screen name from a previous session
        if( !m_SignOnParams.m_strScreen.IsEmpty() )
        {
            // Attempt to retrieve the screen named preferences
            wxGetApp().Options().SetScreenName( m_SignOnParams.m_strScreen );
            if( !wxGetApp().Options().Read() )
            {
                // No one around to report this error to yet
            }

            // Restore the user preferences
            PREFERENCES.Load();

            // Set the app font
            wxGetApp().SetAppDefFont( PREFERENCES.m_AppFont );

            // Restore user sign on data.
            // NOTE: m_fSavePassword, m_fAutoSignOn, and m_fKeepTryingConnection are stored
            // in both the UserPreferences and SignOnParams classes for convenience.
            m_SignOnParams.m_fSavePassword = wxGetApp().Options().GetUserBool("SignOnSavePassword", true );
            m_SignOnParams.m_fAutoSignOn  = wxGetApp().Options().GetUserBool("SignOnAutoSignOn",   false );
            m_SignOnParams.m_fKeepTryingConnection = wxGetApp().Options().GetUserBool("SignOnAutoRetry",    true );
            m_SignOnParams.m_fAutoHunt = wxGetApp().Options().GetUserBool("SignOnAutoHunt", true );
        }
        fOK = true;
    }
    return fOK;
}

bool MeetingCenterController::ReadUserPreferences()
{
    bool fOK = true;

    if( !m_SignOnParams.m_strScreen.IsEmpty() )
    {
        // Attempt to retrieve the screen named preferences
        wxGetApp().Options().SetScreenName( m_SignOnParams.m_strScreen );
        if( !wxGetApp().Options().Read() )
        {
            // May not be any preferences to load yet...
        }

        // Restore the user preferences
        PREFERENCES.Load();

        // Set the app font
        wxGetApp().SetAppDefFont( PREFERENCES.m_AppFont );
    }

    return fOK;
}

void MeetingCenterController::ShowMeetingCenterWindow( )
{
    if (m_fHaveGUI && !AnonymousMode())
        CUtils::RestoreWindow(this);
}

wxFrame * MeetingCenterController::CreateMeetingCenterWindow( )
{
    if (m_fHaveGUI)
    {
        return this;
    }

	//(*Initialize(MeetingCenterController)
	wxXmlResource::Get()->LoadObject(this,parent,_T("MeetingCenterController"),_T("wxFrame"));
	m_HtmlText = (wxHtmlWindow*)FindWindow(XRCID("ID_HTMLWINDOW1"));
	m_meetingList = (wxTreeListCtrl*)FindWindow(XRCID("ID_MEETINGS_LIST"));
	m_ButtonStart = (wxButton*)FindWindow(XRCID("ID_BUTTON_START"));
	m_ButtonJoin = (wxButton*)FindWindow(XRCID("ID_BUTTON_JOIN"));
	m_ButtonNew = (wxButton*)FindWindow(XRCID("ID_BUTTON_NEW"));
	m_ButtonEdit = (wxButton*)FindWindow(XRCID("ID_BUTTON_EDIT"));
	m_ButtonView = (wxButton*)FindWindow(XRCID("ID_BUTTON_VIEW"));
	m_ButtonFind = (wxButton*)FindWindow(XRCID("ID_BUTTON_FIND"));
	m_Panel = (wxPanel*)FindWindow(XRCID("ID_PANEL1"));
	
	Connect(XRCID("ID_MEETINGS_LIST"),wxEVT_COMMAND_TREE_ITEM_COLLAPSED,(wxObjectEventFunction)&MeetingCenterController::OnMeetingListItemCollapsed);
	Connect(XRCID("ID_MEETINGS_LIST"),wxEVT_COMMAND_TREE_ITEM_EXPANDED,(wxObjectEventFunction)&MeetingCenterController::OnMeetingListItemExpanded);
	Connect(XRCID("ID_MEETINGS_LIST"),wxEVT_COMMAND_TREE_ITEM_RIGHT_CLICK,(wxObjectEventFunction)&MeetingCenterController::OnMeetingListItemRightClick);
	Connect(XRCID("ID_MEETINGS_LIST"),wxEVT_COMMAND_TREE_SEL_CHANGED,(wxObjectEventFunction)&MeetingCenterController::OnMeetingListSelectionChanged);
	Connect(XRCID("ID_MEETINGS_LIST"),wxEVT_COMMAND_TREE_KEY_DOWN,(wxObjectEventFunction)&MeetingCenterController::OnMeetingListKeyDown);
	Connect(XRCID("ID_MEETINGS_LIST"),wxEVT_COMMAND_TREE_ITEM_MENU,(wxObjectEventFunction)&MeetingCenterController::OnMeetingListItemMenu);
	Connect(XRCID("ID_BUTTON_START"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&MeetingCenterController::OnStartMeeting);
	Connect(XRCID("ID_BUTTON_JOIN"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&MeetingCenterController::OnJoinMeeting);
	Connect(XRCID("ID_BUTTON_NEW"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&MeetingCenterController::OnScheduleNewMeeting);
	Connect(XRCID("ID_BUTTON_EDIT"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&MeetingCenterController::OnEditMeeting);
	Connect(XRCID("ID_BUTTON_VIEW"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&MeetingCenterController::OnViewMeeting);
	Connect(XRCID("ID_BUTTON_FIND"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&MeetingCenterController::OnFindMeeting);
	//*)
    Connect(XRCID("ID_MEETINGS_LIST"),wxEVT_COMMAND_LIST_COL_CLICK,(wxObjectEventFunction)&MeetingCenterController::OnMeetingListColClick);

    Connect(wxID_ANY, wxEVT_MEETING_EDIT_MEETING, wxCommandEventHandler(MeetingCenterController::OnEditMeeting));

    Connect(wxID_ANY, wxEVT_COMMAND_HTML_LINK_CLICKED, wxHtmlLinkEventHandler(MeetingCenterController::OnLinkClicked));
    Connect(wxID_ANY, wxEVT_COMMAND_HTML_CELL_CLICKED, wxHtmlCellEventHandler(MeetingCenterController::OnHTMLClicked));

    CreateStatusBar( 1 );

    wxIcon icon( wxGetApp().GetResourceFile(_T("res/zon.ico")), wxBITMAP_TYPE_ICO );
    SetIcon( icon );

	this->SetFont( wxGetApp().GetAppDefFont() );
    m_meetingList->SetFont( wxGetApp().GetAppDefFont() );

    // Setup the custom sort comparator
    m_meetingList->SetSortComparator( new MeetingListComparator( this, m_meetingList ) );

    // Events that should be supported by wxSmith (YGWYPF)
    Connect( wxEVT_SIZE, (wxObjectEventFunction)&MeetingCenterController::OnSize );

    // Load and size window header
    wxString strName( wxGetApp().GetResourceFile(wxT("res/header.html")) );
    if (wxGetApp().m_pLocale  !=  NULL)
    {
        wxString strT(wxGetApp().m_pLocale->GetCanonicalName());
        strT.Prepend(wxT("locale/"));
        strT.Append(wxT("/header.html"));
        strT =  wxGetApp().GetResourceFile(strT);
        if( ::wxFileExists(strT))
        {
            strName.Empty();
            strName.Append(strT);
        }
    }
    wxFileName FName(strName);
    m_HtmlText->SetBorders(0);
    //m_HtmlText->GetInternalRepresentation()->SetIndent(0, wxHTML_INDENT_ALL);
    m_HtmlText->LoadFile(FName);
    m_HtmlText->Layout();
    
    wxSize size(m_HtmlText->GetInternalRepresentation()->GetWidth()+10,
                m_HtmlText->GetInternalRepresentation()->GetHeight());
    m_HtmlText->SetSize(size);
    m_HtmlText->SetMinSize(size);
	// Fit(); keeps size from being restored
    Layout();
    //m_HtmlText->GetInternalRepresentation()->SetBackgroundColour(m_Panel->GetBackgroundColour());

    // Restore the window metrics
    if (!wxGetApp().Options().LoadScreenMetrics( "MeetingCenterController", *this ))
    {
        SetSize(800,500);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////
   	m_MainMenuBar = new wxMenuBar( 0 );

   	// Meetings menu
	m_MeetingsMenu = new wxMenu();
	m_MeetingsMenu->Append( Meeting_Start, _("Start..."), _("Start the selected meeting"), wxITEM_NORMAL );
	m_MeetingsMenu->Append( Meeting_Join,  _("Join...") , _("Join the selected meeting"), wxITEM_NORMAL );
	m_MeetingsMenu->Append( Meeting_JoinBy,_("Join by meeting ID or PIN..."), _("Join a meeting that matches an ID or PIN"), wxITEM_NORMAL );
	m_MeetingsMenu->Append( Meeting_New, _("Schedule new..."), _("Schedule a new meeting"), wxITEM_NORMAL );
	m_MeetingsMenu->Append( Meeting_View, _("View..."), _("View the details of the selected meeting"), wxITEM_NORMAL );
	m_MeetingsMenu->Append( Meeting_Refresh, _("Refresh"), _("Refresh the current meeing list"), wxITEM_NORMAL );
	m_MeetingsMenu->AppendSeparator();
	m_MeetingsMenu->Append( Meeting_Edit, _("Edit..."), _("Edit the selected meeting"), wxITEM_NORMAL );
	m_MeetingsMenu->Append( Meeting_Copy, _("Copy"), _("Make a copy of the selected meeting"), wxITEM_NORMAL );
	m_MeetingsMenu->Append( Meeting_Delete, _("Delete"), _("Delete the selected meeting"), wxITEM_NORMAL );
	m_MeetingsMenu->AppendSeparator();
	if ( IMClientMode() )
        m_MeetingsMenu->Append( wxID_CLOSE, _("Close meeting center"), _("Close the meeting center window"), wxITEM_NORMAL );
    m_MeetingsMenu->Append( wxID_EXIT, _("Exit"), _("Exit the application"), wxITEM_NORMAL );
	m_MainMenuBar->Append( m_MeetingsMenu, _("Meetings") );

    // Documents menu
	m_DocumentsMenu = new wxMenu();
	m_DocumentsMenu->Append( Documents_Recordings, _("Recordings..."), wxEmptyString, wxITEM_NORMAL );
	m_DocumentsMenu->Append( Documents_Shared, _("Uploaded presentations..."), wxEmptyString, wxITEM_NORMAL );
	m_MainMenuBar->Append( m_DocumentsMenu, _("Documents") );

    // Options menu
	m_OptionsMenu = new wxMenu();
	m_OptionsMenu->Append( Options_SignOff, _("Sign off"), wxEmptyString, wxITEM_NORMAL );
	m_OptionsMenu->AppendSeparator();
	m_OptionsMenu->Append( Options_ProfilePhoto, _("Set profile picture..."), wxEmptyString, wxITEM_NORMAL );
	m_OptionsMenu->Append( wxID_PREFERENCES, _("Preferences..."), wxEmptyString, wxITEM_NORMAL );
	m_MainMenuBar->Append( m_OptionsMenu, _("Options") );

    // Search menu
    m_SearchMenu = new wxMenu();
    m_SearchMenu->Append( wxID_FIND, _("Find meetings...\tCtrl+F"), _("Search the community meetings list") );
    m_SearchMenu->Append( Search_Continue, _("Find more"), _("Search for additional meetings"), wxITEM_NORMAL );
    m_SearchMenu->Append( Search_Remove, _("Remove search"), _("Remove these search results"), wxITEM_NORMAL );
    m_SearchMenu->AppendSeparator();
    m_SearchMenu->Append( Search_Edit, _("Edit search criteria"), _("Change the search critera and start the search again"), wxITEM_NORMAL );
	m_MainMenuBar->Append( m_SearchMenu, _("Search") );

    // Contacts menu
    m_ContactsMenu = new wxMenu();
    m_ContactsMenu->Append( Contacts_Manage, _("Manage contacts and buddies..."), _("Add, change, and delete contact information. Change contact display preferences."), wxITEM_NORMAL );
    m_ContactsMenu->Append( Contacts_EditMyContact, _("Edit my contact information..."), _("Edit your own contact information."), wxITEM_NORMAL );
//    m_ContactsMenu->AppendSeparator();
//    m_ContactsMenu->Append( Contacts_Fetch, _("Fetch contacts"), _("Refresh contact information."), wxITEM_NORMAL );
    m_MainMenuBar->Append( m_ContactsMenu, _("Contacts") );

    // Help menu
	m_HelpMenu = new wxMenu();
	m_HelpMenu->Append( Help_Contents, _("Contents..."), _("View user manual in browser"), wxITEM_NORMAL );
    m_HelpMenu->AppendSeparator();
    if ( LogUpload::IsAvailable() )
        m_HelpMenu->Append( Help_Upload, _("Submit logs for analysis"), wxEmptyString, wxITEM_NORMAL );
#ifdef WIN32 // The log is written to from background threads, so displaying in a window is only safe on Windows
	m_HelpMenu->Append( Help_Debug, _("Open log window..."), wxEmptyString, wxITEM_NORMAL );
    m_HelpMenu->AppendSeparator();
#endif
	m_HelpMenu->Append( Help_About, _("About..."), wxEmptyString, wxITEM_NORMAL );
	m_MainMenuBar->Append( m_HelpMenu, _("Help") );

	this->SetMenuBar( m_MainMenuBar );

    // Create the pop up menu for the meeting list
    m_MeetingPopup = new wxMenu();
	m_MeetingPopup->Append( Meeting_Start, _("Start..."), _("Start the selected meeting"), wxITEM_NORMAL );
	m_MeetingPopup->Append( Meeting_Join,  _("Join...") , _("Join the selected meeting"), wxITEM_NORMAL );
	m_MeetingPopup->Append( Meeting_View, _("View..."), _("View the details of the selected meeting"), wxITEM_NORMAL );
	m_MeetingPopup->AppendSeparator();
	m_MeetingPopup->Append( Meeting_Edit, _("Edit..."), _("Edit the selected meeting"), wxITEM_NORMAL );
	m_MeetingPopup->Append( Meeting_Copy, _("Copy"), _("Make a copy of the selected meeting"), wxITEM_NORMAL );
	m_MeetingPopup->Append( Meeting_Delete, _("Delete"), _("Delete the selected meeting"), wxITEM_NORMAL );
	m_MeetingPopup->AppendSeparator();
	m_MeetingPopup->Append( Documents_Recordings, _("Recordings..."), wxEmptyString, wxITEM_NORMAL );
	m_MeetingPopup->Append( Documents_Shared, _("Uploaded presentations..."), wxEmptyString, wxITEM_NORMAL );

    // Create the popup menu for the search nodes
    m_SearchPopup = new wxMenu();
    m_SearchPopup->Append( Search_Continue, _("Find more"), _("Search for additional meetings"), wxITEM_NORMAL );
    m_SearchPopup->Append( Search_Remove, _("Remove search"), _("Remove these search results"), wxITEM_NORMAL );
    m_SearchPopup->AppendSeparator();
    m_SearchPopup->Append( Search_Edit, _("Edit search criteria"), _("Change the search critera and start the search again"), wxITEM_NORMAL );

    // Map the menu events.
    Connect( Meeting_Start, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnStartMeeting) );
    Connect( Meeting_Join, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnJoinMeeting) );
    Connect( Meeting_JoinBy, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnJoinMeetingByPin) );
    Connect( Meeting_New, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnScheduleNewMeeting) );
    Connect( Meeting_View, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnViewMeeting) );
    Connect( Meeting_Refresh, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnRefreshMeetings) );
    Connect( Meeting_Edit, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnEditMeeting) );
    Connect( Meeting_Copy, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnCopyMeeting) );
    Connect( Meeting_Delete, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnDeleteMeeting) );
    Connect( wxID_CLOSE, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnMenuClose) );
    Connect( wxID_EXIT, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnExit) );
    Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( MeetingCenterController::OnClose ) );
    Connect( wxEVT_ACTIVATE, wxActivateEventHandler( MeetingCenterController::OnActivate ) );

    // Documents menu
    Connect( Documents_Recordings, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnRecordings) );
    Connect( Documents_Shared, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnShared) );

    // Options menu
    Connect( Options_SignOff,wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnSignOffEvent) );
    Connect( Options_ProfilePhoto,wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnProfilePhoto) );
    Connect( wxID_PREFERENCES,wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnPreferences) );

    // Search menu
	Connect( wxID_FIND,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&MeetingCenterController::OnFindMeeting);
    Connect( Search_Continue,wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnSearchContinue) );
    Connect( Search_Edit, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnSearchEdit) );
    Connect( Search_Remove, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnSearchRemove) );

    // Contacts menu
    Connect( Contacts_Manage, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnContactsManage) );
    Connect( Contacts_EditMyContact, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnContactsEditMyContact) );
    Connect( Contacts_Fetch, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnContactsFetch) );

    // Help menu
    Connect( Help_Contents, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnContents) );
    Connect( Help_Upload, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnUploadLogs) );
    Connect( Help_Debug, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnDebug) );
    Connect( Help_About, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingCenterController::OnAbout) );

    // Load the images for the meeting list
    wxImageList *pImageList = new wxImageList( 16, 18 );

    wxBitmap offline( wxGetApp().GetResourceFile(wxT("res/status_meeting_offline.png")), wxBITMAP_TYPE_PNG );
    pImageList->Add( offline );
    wxBitmap online( wxGetApp().GetResourceFile(wxT("res/status_meeting_online.png")), wxBITMAP_TYPE_PNG );
    pImageList->Add( online );

    wxBitmap unknown( wxGetApp().GetResourceFile(wxT("res/status_meeting_unknown.png")), wxBITMAP_TYPE_PNG );
    pImageList->Add( unknown );

    // Hand off the image list to the meetingList - The list owns the memory now.
    m_meetingList->AssignImageList( pImageList );

    // Set the standard wxAccelrators for this window
    CUtils::UseStandardAccelerators( *this );

    SetupMeetingList();

    SetUIFeedback();

    m_fHaveGUI = true;

    return this;
}

void MeetingCenterController::SetupMeetingList()
{
    if( m_meetingList->GetColumnCount() == 0 )
    {
        m_meetingList->AddColumn( _("Title") );
        m_meetingList->SetColumnWidth( Col_Title, 200 );
        m_meetingList->AddColumn( _("Meeting ID") );
        m_meetingList->AddColumn( _("My PIN") );
        m_meetingList->AddColumn( _("Host") );
        m_meetingList->AddColumn( _("Date/Time") );
        m_meetingList->SetColumnWidth( Col_DateTime, 180 );
        m_meetingList->AddColumn( _("Privacy") );
        m_meetingList->AddColumn( _("Moderator") );
    }

    // The Root node exists for the life of the window.
    m_RootId = m_meetingList->AddRoot( _("Meetings"), -1, -1, new MeetingTreeItemData( Node_Root ) );

    // Add a node for My Instant Meeting...we should always have an instant meeting, and first seems most convenient.
    if ( m_fInstantMeeting )
        m_MyInstantMeetingId = m_meetingList->AppendItem( m_RootId, _("My instant meeting"), -1, -1, new MeetingTreeItemData( Node_Root ) );
    else
        m_MyInstantMeetingId = 0;

    // Add a new node to the meetings tree for My Meetings
    m_MyMeetingsId = m_meetingList->AppendItem( m_RootId, _("My upcoming meetings"), -1, -1, new MeetingTreeItemData( Node_Root ) );
    m_PastMeetingsId = m_meetingList->AppendItem( m_RootId, _("My past meetings"), -1, -1, new MeetingTreeItemData( Node_Root ) );
    m_UnscheduledMeetingsId = m_meetingList->AppendItem( m_RootId, _("My unscheduled meetings"), -1, -1, new MeetingTreeItemData( Node_Root ) );

    // Add a new node to the meetings tree for Invited
    m_InvitedMeetingsId = m_meetingList->AppendItem( m_RootId, _("Invited"), -1, -1, new MeetingTreeItemData( Node_Root ) );

    if( PREFERENCES.m_fDownloadPublicMeetings )
    {
        // Add a new node to the meetings tree
        m_PublicMeetingsId = m_meetingList->AppendItem( m_RootId, _("Public"), -1, -1, new MeetingTreeItemData( Node_Root ) );
    }

    // Add the community search node
    m_CommunitySearchId = m_meetingList->AppendItem( m_RootId, _("Community search results"), -1, -1, new MeetingTreeItemData( Node_Root ) );   
}

void MeetingCenterController::OnMenuClose(wxCommandEvent& event)
{
    Close();
}

void MeetingCenterController::OnExit(wxCommandEvent& event)
{
    if (m_pTaskBarIcon)
    {
        m_pTaskBarIcon->RemoveIcon();
        m_pTaskBarIcon = 0;
    }

    // Mostly to make sure we close Outlook/MAPI store if open
    if (m_pContactService)
        m_pContactService->Shutdown();

    // launch installer if needed
    if( m_bLaunchInstallerAtExit )
    {
        bool    result;
        m_bLaunchInstallerAtExit = false;
        if( m_pInstallController )
            result = m_pInstallController->ExecInstaller();

        // A little harsh, but the normal close is too slow, the installer is trying to update meeting.exe
        // before we can clean up.
        exit(0);
    }

    m_ControllerState |= State_Closing;

    // TODO: fix...but if we're not connected, signon dialog is probably up
    if ((m_ControllerState & State_Connected) == 0)
        exit(0);

    Close();
}

void MeetingCenterController::CloseAllMeetings()
{
    wxWindowListNode* node = wxTopLevelWindows.GetFirst();
    while (node)
    {
        wxWindow* win = node->GetData();
        if (win->IsKindOf(CLASSINFO(MeetingMainFrame)))
            win->Destroy();
        node = node->GetNext();
    }
}

bool MeetingCenterController::IsMeetingActive(const wxString & mtgID)
{
    wxWindowListNode* node = wxTopLevelWindows.GetFirst();
    while (node)
    {
        wxWindow* win = node->GetData();
        if (win->IsKindOf(CLASSINFO(MeetingMainFrame)))
        {
            return true;

            /* Short circuit for now since we can only support one meeting
            MeetingMainFrame* frame = dynamic_cast<MeetingMainFrame*>(win);
            if (frame->GetMeetingID() == mtgId)
                return true;
            */
        }
        node = node->GetNext();
    }
    return false;
}

bool MeetingCenterController::IsMeetingActive()
{
    if (m_pMeetingSetupDialog != NULL)
        return true;

    wxWindowListNode* node = wxTopLevelWindows.GetFirst();
    while (node)
    {
        wxWindow* win = node->GetData();
        if (win->IsKindOf(CLASSINFO(MeetingMainFrame)))
        {
            return true;
        }
        node = node->GetNext();
    }
    return false;
}

MeetingMainFrame * MeetingCenterController::FindMeetingWindow()
{
    wxWindowListNode* node = wxTopLevelWindows.GetFirst();
    while (node)
    {
        wxWindow* win = node->GetData();
        if (win->IsKindOf(CLASSINFO(MeetingMainFrame)))
        {
            MeetingMainFrame* frame = dynamic_cast<MeetingMainFrame*>(win);
            return frame;
        }
        node = node->GetNext();
    }
    return NULL;
}

MeetingMainFrame * MeetingCenterController::FindMeetingWindow(const wxString & mtgId)
{
    wxWindowListNode* node = wxTopLevelWindows.GetFirst();
    while (node)
    {
        wxWindow* win = node->GetData();
        if (win->IsKindOf(CLASSINFO(MeetingMainFrame)))
        {
            MeetingMainFrame* frame = dynamic_cast<MeetingMainFrame*>(win);
            if (frame->GetMeetingID() == mtgId)
                return frame;
        }
        node = node->GetNext();
    }
    return NULL;
}

void MeetingCenterController::OnClose(wxCloseEvent& event )
{
    wxLogDebug( wxT("entering MeetingCenterController::OnClose( )") );

    if ((m_ControllerState & State_Closing) == 0 && IMClientMode())
    {
        Hide();
        return;
    }

    int meetings = 0;

    // Close any meetings/meeting windows
    if (IsVisible())
    {
        wxWindowListNode* node = wxTopLevelWindows.GetFirst();
        while (node)
        {
            wxWindow* win = node->GetData();
            if (win->IsKindOf(CLASSINFO(MeetingMainFrame))) ++meetings;
            node = node->GetNext();
        }

        if (meetings)
        {
            wxMessageDialog dlg(this, _("Closing this window will exit your active meeting.  Continue anyway?"), _("Warning"), wxOK | wxCANCEL | wxICON_EXCLAMATION);
            if( !m_fForceApplicationExit  &&  dlg.ShowModal() != wxID_OK )
            {
                m_ControllerState &= ~State_Closing;
                event.Veto();
                return;
            }

            CloseAllMeetings();
        }
    }
    else
    {
        CloseAllMeetings();
    }

    // Close contact window
    if (m_pManageContactsFrame)
        m_pManageContactsFrame->Close();

    // Close any still remaining dialogs
    CUtils::CloseDialogs(this);

    // Shutdown contact service
    if (m_pContactService)
        m_pContactService->Shutdown();

    if (m_pTaskBarIcon)
    {
        m_pTaskBarIcon->RemoveIcon();
        m_pTaskBarIcon = 0;
    }

    // Shut down this timer so we won't receive events after the handler has been destroyed
    m_PingTimer.Stop( );
    m_iPingsSent     = 0;
    m_iPingsReceived = 0;

    m_fHaveGUI = false;

    // Save the options for this window
    wxGetApp().Options().SaveScreenMetrics( "MeetingCenterController", m_lastSize, m_lastPosition, GetWindowStyleFlag() );
    wxGetApp().Options().SetUserBool( "fPublicMeetingsExpanded", m_fPublicMeetingsExpanded );
    wxGetApp().Options().SetUserBool( "fMyMeetingsExpanded", m_fMyMeetingsExpanded );
    wxGetApp().Options().SetUserBool( "fPastMeetingsExpanded", m_fPastMeetingsExpanded );
    wxGetApp().Options().SetUserBool( "fUnscheduledMeetingsExpanded", m_fUnscheduledMeetingsExpanded );
    wxGetApp().Options().SetUserBool( "fMyInstantMeetingExpanded", m_fMyInstantMeetingExpanded );
    wxGetApp().Options().SetUserBool( "fInvitedMeetingsExpanded", m_fInvitedMeetingsExpanded );
    m_nLastSortColumn = m_meetingList->GetSortColumn();
    m_fLastSortAscending = m_meetingList->GetSortAscending();
    wxGetApp().Options().SetUserInt( "nLastSortColumn",  m_nLastSortColumn );
    wxGetApp().Options().SetUserBool( "fLastSortAscending", m_fLastSortAscending );

    wxGetApp().Options().Write();

    // Set a flag to indicate that we are shutting down.
    // This is done to suppress warnings about server connections being lost.
    m_ControllerState |= State_Closing;

    // Disconnect from server
    session_disconnect(session, TRUE);

#ifndef __WXMAC__
    // On Mac we may still have pending events to process, so completely removing the window
    // now can lead to crashes.
    Destroy();
#endif

    CUtils::MsgWait( 20, 10 );

    if (sched)
        schedule_destroy(sched);
    if (book)
        addressbk_destroy(book);
    if (controller)
        controller_destroy(controller);
    if (session)
        session_destroy(session);

    sched = 0; book = 0; controller = 0; session = 0;

    wxLogDebug( wxT("Exitting main loop") );
    wxGetApp().ExitMainLoop();
}

void MeetingCenterController::OnActivate(wxActivateEvent& event)
{
#ifdef __WXMAC__
	// Well, we need this part of the default processing or Mac menus don't work.
	if (m_frameMenuBar != NULL)
	{
		m_frameMenuBar->MacInstallMenuBar();
	}
#endif
}

void MeetingCenterController::OnStartMeeting(wxCommandEvent& event)
{
    // See what's been selected
    wxTreeItemId selId;
    selId = m_meetingList->GetSelection();
    if( !selId.IsOk() )
    {
        return; // No selection!
    }
    
    if (IsMeetingActive())
    {
        wxMessageBox(_("You may only participate in one meeting at a time.\nPlease exit the current meeting if you wish to start or join a new meeting"), wxT("Meeting Center"), wxOK | wxICON_EXCLAMATION);
        return;
    }

    MeetingTreeItemData *pData = (MeetingTreeItemData *)m_meetingList->GetItemData(selId); // Const cast s/b safe enough
    if( pData->m_meetingId.IsEmpty() ||
        (pData->m_participantId.IsEmpty() && !AdministratorMode()) )
    {
        return; // No data
    }

    SetWaitingForSetup();

    if( !WaitContactsLoaded( ) )
    {
        ClearWaitingForSetup();
        return;
    }

    Commands().Add( wxT("start-meeting"),
        pData->m_participantId,
        m_strParticipantName,
        pData->m_meetingId,
        pData->m_title );

    ExecutePendingCommands();
}

void MeetingCenterController::OnJoinMeeting(wxCommandEvent& event)
{
    // See what's been selected
    wxTreeItemId selId;
    selId = m_meetingList->GetSelection();
    if( !selId.IsOk() )
    {
        return; // No selection!
    }

    if (IsMeetingActive())
    {
        wxMessageBox(_("You may only participate in one meeting at a time.\nPlease exit the current meeting if you wish to start or join a new meeting"), wxT("Meeting Center"), wxOK | wxICON_EXCLAMATION);
        return;
    }

    MeetingTreeItemData *pData = (MeetingTreeItemData *)m_meetingList->GetItemData(selId); // Const cast s/b safe enough
    if( pData->m_meetingId.IsEmpty() ) // OK to proceed without participant id, server can allocate one
    {
        return; // No data
    }

    JoinMeeting( pData );
}

void MeetingCenterController::JoinMeeting( MeetingTreeItemData *pData )
{
    if( !WaitContactsLoaded( ) )
        return;

    Commands().Add( wxT("join-meeting"),pData->m_participantId, m_strParticipantName, pData->m_meetingId, pData->m_title );

    ExecutePendingCommands();
}

void MeetingCenterController::OnScheduleNewMeeting(wxCommandEvent& event)
{
    SetWaitingForSetup();

    if( WaitContactsLoaded( ) )
    {
        // Create the dialog
        MeetingSetupDialog dlg(this);

        int nRet = dlg.NewMeeting();
        if( nRet == wxID_OK )
        {
            RefreshMeetingsList();
        }
    }

    ClearWaitingForSetup();
}

wxString MeetingCenterController::GetSingleSelectedMeeting()
{
    wxString ret(wxT(""));
    // See what's been selected
    wxTreeItemId selId;
    selId = m_meetingList->GetSelection();
   if( selId.IsOk() )
    {
        MeetingTreeItemData *pData = (MeetingTreeItemData *)m_meetingList->GetItemData(selId); // Const cast s/b safe enough
        if( !pData->m_meetingId.IsEmpty() )
        {
            ret = pData->m_meetingId;
        }
    }

    return ret;
}

int MeetingCenterController::GetAllSelectedMeetings( wxArrayString &arrStr )
{
    arrStr.Clear( );

    // See what's been selected
    wxArrayTreeItemIds  selIds;
    int                 iNumSels;
    iNumSels = m_meetingList->GetSelections( selIds );
    if( iNumSels  <=  0 )
        return 0;

    for( int i=0; i<iNumSels; i++ )
    {
        wxTreeItemId    selId = selIds.Item( i );
        if( selId.IsOk( ) )
        {
            MeetingTreeItemData *pData = (MeetingTreeItemData *) m_meetingList->GetItemData( selId ); // Const cast s/b safe enough
            if( !pData->m_meetingId.IsEmpty( ) )
            {
                arrStr.Add( pData->m_meetingId );
            }
        }
    }

    return (int) arrStr.Count( );
}

void MeetingCenterController::OnEditMeeting(wxCommandEvent& event)
{
    wxString meetingId;
    meetingId = GetSingleSelectedMeeting();
    if( meetingId.IsEmpty() )
        return;

    SetWaitingForSetup();
    
    if( WaitContactsLoaded( ) )
    {
        // Create the dialog
        MeetingSetupDialog *pDlg = new MeetingSetupDialog( this );

        int nRet = pDlg->EditMeeting( meetingId, true );
        if( nRet == wxID_OK )
        {
            RefreshMeetingsList( );
        }
        pDlg->Destroy( );
    }

    ClearWaitingForSetup();
}

void MeetingCenterController::OnEditMeetingClick(wxCommandEvent& event)
{
    /* Unfortunately, this doesn't work on Linux...GTK will not dispatch further pending events
       while one is being processed, and MeetingSetupDialog needs pending events to fetch
       meeting details from the server */

/*
    wxString meetingId;
    meetingId = GetSingleSelectedMeeting();
    if( meetingId.IsEmpty() )
        return;

    wxCommandEvent  ev( wxEVT_MEETING_EDIT_MEETING, wxID_ANY );
    ev.SetString( meetingId );
    this->AddPendingEvent( ev );
*/
}

void MeetingCenterController::OnViewMeeting(wxCommandEvent& event)
{
    wxString meetingId;
    meetingId = GetSingleSelectedMeeting();
    if( meetingId.IsEmpty() )
        return;

    m_MeetingView.Display( this, meetingId );
}

void MeetingCenterController::OnFindMeeting(wxCommandEvent& event)
{
    // Function n/a for now
    FindMeetingsDialog Dlg( this, wxID_ANY );

    int nResp = Dlg.EditSearchCriteria();
    if( nResp == wxID_OK )
    {
        MeetingSearch criteria;
        criteria = Dlg.GetCriteria();
        StartMeetingSearch( criteria );
    }
}

void MeetingCenterController::OnJoinMeetingByPin( wxCommandEvent& event )
{
    wxString defaultValue(wxT(""));
    wxString caption( _("Join Meeting"), wxConvUTF8 );
    wxString msg( _("Please enter the Meeting Id or PIN:"), wxConvUTF8 );
    wxString id_or_pin;

    wxTextEntryDialog AskForPin(this, msg, caption, defaultValue, wxOK|wxCANCEL );
    int nResp = AskForPin.ShowModal();
    if( nResp != wxID_OK )
    {
        return; // They said no
    }
    id_or_pin = AskForPin.GetValue();
    if( !id_or_pin.IsEmpty() )
    {
        // We need to queue up this request to start/join a meeting.
        // The meeting ID was entered by the user on the sign on screen.
        // We need to validate the Meeting Id.
        m_Commands.Add(wxT("join-meeting-by-pin"),id_or_pin);
        ExecutePendingCommands();
    }
}

void MeetingCenterController::OnRefreshMeetings( wxCommandEvent& event )
{
    RefreshMeetingsList();
}

void MeetingCenterController::OnCopyMeeting( wxCommandEvent& event )
{
    wxString meetingId;
    meetingId = GetSingleSelectedMeeting();
    if( meetingId.IsEmpty() )
        return;

    if( !WaitContactsLoaded( ) )
        return;

    // Create the dialog
    MeetingSetupDialog dlg(this);

    int nRet = dlg.CopyMeeting( meetingId );
    if( nRet == wxID_OK )
    {
        RefreshMeetingsList();
    }
}

void MeetingCenterController::deleteSelectedMeeting()
{
    wxString meetingId;
    wxArrayString   arrStr;
    int             iNumMeetings;
    iNumMeetings = GetAllSelectedMeetings( arrStr );
    if( iNumMeetings  <=  0 )
        return;

    wxMessageDialog dlg(this, _("Are you sure you want to delete the selected meeting?"), _("Warning"), wxOK | wxCANCEL | wxICON_EXCLAMATION);
    if (dlg.ShowModal() != wxID_OK)
        return;

    // PLF-TODO: Search for all m_meetingList search results nodes that match the meeting ID.
    // PLF-TODO: The deleted meetings need to removed from search results.

    for( int i=0; i<iNumMeetings; i++ )
    {
        schedule_remove_meeting( sched, opcode( OP_DeleteMeeting ).mb_str(wxConvUTF8), arrStr.Item( i ).mb_str(wxConvUTF8) );
    }

	// TODO: wait for API results
	// Since we're not waiting for the result from the above API calls, give the server a bit of time
	// to process the operations before starting a refresh.
    CUtils::MsgWait( 150, 3 );

    RefreshMeetingsList();
}

void MeetingCenterController::OnDeleteMeeting( wxCommandEvent& event )
{
    deleteSelectedMeeting();
}

void MeetingCenterController::OnRecordings( wxCommandEvent& event )
{
    wxString meetingId;
    meetingId = GetSingleSelectedMeeting();
    if( meetingId.IsEmpty() )
        return;

    RecordingDialog     dlg( this, meetingId );
    dlg.ShowModal( );
}

void MeetingCenterController::OnShared( wxCommandEvent& event )
{
    wxString meetingId;
    meetingId = GetSingleSelectedMeeting();
    if( meetingId.IsEmpty() )
        return;

    const char *pChar;
    pChar = controller_get_docshare_server( CONTROLLER.controller, (const char *) meetingId.mb_str( wxConvUTF8 ) );
    wxString docServer( pChar, wxConvUTF8 );

    wxString screenName, password, community;
    GetSignOnInfo( screenName, password, community );

    CIICDoc *pDoc;
    pDoc = (CIICDoc *) wxGetApp( ).m_pDocManager->CreateDocument( wxEmptyString, wxDOC_NEW );

    pDoc->SetSessionInfo( screenName, password, docServer );
    pDoc->m_Meeting.SetMeetingId( meetingId );

    DocShareView    *pView;
    pView = (DocShareView *) pDoc->GetFirstView();

    pView->InitializeInfo( pDoc, meetingId );

    wxPoint pos = GetPosition( );
    DocShareCanvas * pDocSharePanel = new DocShareCanvas( this, pos, wxSize( 1, 1 ), 0 );

    pView->Start( NULL, pDocSharePanel, false, true, false );

    pDoc->DeleteAllViews();
    pDocSharePanel->Destroy();
}

void MeetingCenterController::showStatus( const wxString & msg )
{
    wxStatusBar *pStatus = GetStatusBar();
    if( pStatus )
    {
        pStatus->SetStatusText( msg );
    }
}

void MeetingCenterController::OnSignOffEvent( wxCommandEvent& event )
{
    wxLogDebug( wxT("entering MeetingCenterController::OnSignOffEvent( )") );
    SignOff( false );
    SignOn();
}

void MeetingCenterController::SignOn()
{
    if( AlreadySignedOn(true) )
    {
        ExecutePendingCommands();
        return;
    }

    // Protect againt opening dialog twice, but also allow for user to click on second invite while previous sign on dialog is still shown.
    if ( m_pSignOnDlg )
    {
        if (STARTUP.m_fStartupProvided && !STARTUP.m_StartupParticipantId.IsEmpty())
            m_pSignOnDlg->SetPin(STARTUP.m_StartupParticipantId);
        return;
    }

    bool fAllowAutoSignOn = false;
    bool fCreateSession = false;

#ifdef TESTCLIENT
    if (!STARTUP.m_StartupTest.IsEmpty())
    {
        m_SignOnParams.m_strScreen   = STARTUP.m_StartupScreenName;
        m_SignOnParams.m_strServer   = STARTUP.m_StartupServer;
        m_SignOnParams.m_strPassword = STARTUP.m_StartupPassword;
        m_SignOnParams.m_strFullName = STARTUP.m_StartupFullName;
        m_SignOnParams.m_strMeetingPIN = STARTUP.m_StartupMeetingId;

        fAllowAutoSignOn = true;
        fCreateSession = true;
    }
    else
#endif
    if (STARTUP.m_fStartupProvided &&
        !STARTUP.m_StartupScreenName.IsEmpty() &&
        !STARTUP.m_StartupServer.IsEmpty() &&
        !STARTUP.m_StartupPassword.IsEmpty())
    {
        m_SignOnParams.m_strServer = STARTUP.m_StartupServer;
        m_SignOnParams.m_strScreen = STARTUP.m_StartupScreenName;
        m_SignOnParams.m_strPassword = STARTUP.m_StartupPassword;
        fAllowAutoSignOn = true;
        fCreateSession = true;
    }
    else if( STARTUP.m_fStartupProvided &&
             STARTUP.m_StartupScreenName.Cmp( wxT(ANONYMOUS_NAME) ) == 0 &&
             !STARTUP.m_StartupServer.IsEmpty() &&
             !STARTUP.m_StartupFullName.IsEmpty() &&
             STARTUP.m_StartupPassword.IsEmpty() )
    {
        m_SignOnParams.m_strServer = STARTUP.m_StartupServer;
        m_SignOnParams.m_strScreen = STARTUP.m_StartupScreenName;
        m_SignOnParams.m_strPassword = wxT(ANONYMOUS_PASSWORD);
        m_SignOnParams.m_strFullName = STARTUP.m_StartupFullName;
        m_SignOnParams.m_strMeetingPIN = STARTUP.m_StartupParticipantId.IsEmpty() ? STARTUP.m_StartupMeetingId : STARTUP.m_StartupParticipantId;
        m_SignOnParams.m_fAnonymousUser = true;
        fAllowAutoSignOn = true;
        fCreateSession = true;
        m_iAnonSignOnTries++;
    }
    else if( PREFERENCES.m_fAutoSignOn )
    {
        if( !m_SignOnParams.m_strServer.IsEmpty() &&
            !m_SignOnParams.m_strScreen.IsEmpty() &&
            !m_SignOnParams.m_strPassword.IsEmpty() )
        {
            fAllowAutoSignOn = true;
            fCreateSession = true;
        }
    }

    if( !fAllowAutoSignOn )
    {
        ////////////////////////////////////////////////////////////////////
        m_pSignOnDlg = new SignOnDlg(0);
        wxString lastScreen;
        lastScreen = m_SignOnParams.m_strScreen;

        if (STARTUP.m_fStartupProvided)
        {
            m_SignOnParams.m_strFullName = STARTUP.m_StartupFullName;
            m_SignOnParams.m_strMeetingPIN = STARTUP.m_StartupParticipantId.IsEmpty() ? STARTUP.m_StartupMeetingId : STARTUP.m_StartupParticipantId;
        }

        if( m_pSignOnDlg->GetSignOnInformation( m_SignOnParams ) )
        {
            if ( m_SignOnParams.m_fTestConnection )
            {
                m_SavedSignOnParams = m_SignOnParams;
                m_SignOnParams.m_strScreen = wxT(ANONYMOUS_NAME);
                m_SignOnParams.m_strPassword = wxT(ANONYMOUS_PASSWORD);
                m_SignOnParams.m_fAnonymousUser = true;
            }
            
            //  See if the user entered a screen name that includes a community name
            m_SignOnParams.m_strCommunity = m_SignOnParams.m_strScreen.AfterFirst( wxT(':') );
            if( !m_SignOnParams.m_strCommunity.IsEmpty( ) )
            {
                m_SignOnParams.m_strScreen = m_SignOnParams.m_strScreen.BeforeFirst( wxT(':') );
            }

            // Normalize into allowed screenname form
            m_SignOnParams.m_strScreen.Trim( true );
            m_SignOnParams.m_strScreen.Trim( false );
            CUtils::EncodeScreenName( m_SignOnParams.m_strScreen );

            // If the user has just switched screen names or we just learned about a screen name
            if( lastScreen.IsEmpty() || (m_SignOnParams.m_strScreen != lastScreen) )
            {
                // Save the new data
                if (!m_SignOnParams.m_fAnonymousUser)
                {
                    wxGetApp().Options().SetSystem( "server",   m_SignOnParams.m_strServer.mb_str(wxConvUTF8));
                    wxGetApp().Options().SetSystem( "lastuser", m_SignOnParams.m_strScreen.mb_str(wxConvUTF8));

                    PREFERENCES.m_fAutoSignOn = m_SignOnParams.m_fAutoSignOn;

                    PREFERENCES.m_fKeepTryingConnection = m_SignOnParams.m_fKeepTryingConnection;
                    PREFERENCES.Save();

                    wxGetApp().Options().Write();
                }

                // Set the current user name options file name
                wxGetApp().Options().SetScreenName( m_SignOnParams.m_strScreen );

                // Read this user's preferences without loosing just entered password.
                // Most of the preferences are stored in screen named options file.
                ReadUserPreferences();
            }

            m_fIMClientMode = PREFERENCES.m_fIMClient;
            m_fInstantMeeting = PREFERENCES.m_fShowInstantMeeting;

            fCreateSession = true;
        }
        else
        {
            wxCommandEvent dummy;
            OnExit(dummy);
        }

        delete m_pSignOnDlg;
        m_pSignOnDlg = NULL;
    }

    if( fCreateSession )
    {
        if (m_fIMClientMode)
        {
            Commands().Add( wxT("manage-contacts") );
        }

        if( !m_pConnectionProgressDialog )
        {
            m_pConnectionProgressDialog = new ConnectionProgressDialog(0);
            m_pConnectionProgressDialog->Connect( XRCID("ID_CLOSEDLG_BTN"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&MeetingCenterController::OnConnectionProgreesDone, 0, this);
            m_pConnectionProgressDialog->Connect( wxID_ANY, wxEVT_CLOSE_WINDOW, (wxObjectEventFunction)&MeetingCenterController::OnConnectionProgreesDone, 0, this);
       }
       if( m_pConnectionProgressDialog )
       {
           m_pConnectionProgressDialog->Show();
           EnableWindows(false);
       }

        m_SignOnParams.m_fEnableSSL = TRUE;

        CreateSession(
            m_SignOnParams.m_strServer,
            wxT(""),
            m_SignOnParams.m_strScreen,
            m_SignOnParams.m_strPassword,
            !m_SignOnParams.m_fAutoHunt,
            m_SignOnParams.m_fEnableSSL,
            PREFERENCES.m_strLastJID
            );

        wxString strPassword;
        if( !PREFERENCES.m_fSavePassword || m_SignOnParams.m_fAnonymousUser)
        {
            strPassword = wxT("");
        }
        else
        {
            strPassword = m_SignOnParams.m_strPassword;
        }

        CBase64     base64;
        char        *pwdEncoded;
        pwdEncoded = base64.Encode( strPassword.mb_str(wxConvUTF8), strPassword.Len( ) );
        wxGetApp().Options().SetSystem( "lastpwd", pwdEncoded );
        free( pwdEncoded );

        //  lastpwd is Base64 encoded starting with file version 5
        wxGetApp().Options().SetSystemInt( "Version", 5 );

        PREFERENCES.m_strPassword = strPassword;
        PREFERENCES.m_strLastJID.Empty( );
        PREFERENCES.m_strLastJID.Append( wxString( session_get_jid( session ), wxConvUTF8 ) );
        PREFERENCES.Save( );
    }
}

void MeetingCenterController::SignOff( bool fClose )
{
    wxLogDebug( wxT("entering MeetingCenterController::SignOff( )") );

    CloseAllMeetings( );

    if (m_pManageContactsFrame)
        m_pManageContactsFrame->Close();
    
    if( m_fHaveGUI )
    {
        Show(false);
        
        if( m_meetingList  &&  m_RootId.IsOk( ) )
        {
            wxTreeItemIdValue pos;
            wxTreeItemId item = m_meetingList->GetFirstChild(m_RootId, pos);
            while (item.IsOk())
            {
                m_meetingList->DeleteChildren(item);
                item = m_meetingList->GetNextChild(m_RootId, pos);
            }
        }
    }

    m_PingTimer.Stop( );
    m_iPingsSent     = 0;
    m_iPingsReceived = 0;

    // Shutdown contact service
    if (m_pContactService)
        m_pContactService->StopImport( );
        
    // Clear out previous session state
    m_cab = 0;
    m_pab = 0;

    session_flush(session);
    session_disconnect(session, TRUE);

    // Clear the state flags associated with SignOn()
    m_ControllerState &= ~(State_Connected|State_Authenticated|State_Registered|State_AdministratorMode|State_AnonymousMode);

    if( fClose )
    {
        bool res = Close();
        if (!res)
            return;
    }
}

void MeetingCenterController::CreateSession( const wxString & server, const wxString & targetServer, const wxString & screen, const wxString & password, bool autoHunt, bool secure, const wxString &lastJID )
{
    //  Do a sanity check on the lastJID - does the screen name match the passed screen name?
    wxString    strLastJID( lastJID );
    if( !strLastJID.StartsWith( screen ) )
        strLastJID.Empty( );

    m_ControllerState |= State_WaitingForSignOn;

    bool        fEnableSec      = secure;
    bool        fEnableExtAuth  = false; // no longer needed
    bool        fUsePrimaryPort = OPTIONS.GetSystemBool( "UsePrimaryPort", true );
    bool        fDisablePortHunting = OPTIONS.GetSystemBool( "DisablePortHunting", false );

    // "iic" resource signifies IM-able client, "mtg" signifies meeting client without IM
    wxString strResource = IMClientMode() ? _T("iic") : _T("mtg");

    if (!strLastJID.Contains(_T("/") + strResource))
        strLastJID.clear();

    session = session_create(server.mb_str(wxConvUTF8), screen.mb_str(wxConvUTF8), password.mb_str(wxConvUTF8), strResource.mb_str(wxConvUTF8),
                             fEnableSec, CertHandler::GetHandler().GetCAFile().mb_str( wxConvUTF8 ), fEnableExtAuth, strLastJID.mb_str(wxConvUTF8) );
    session_set_connected_handler(session, this, on_connected);
    session_set_error_handler(session, this, on_session_error);
    session_set_option(session, "http", autoHunt ? "yes" : "no");
    session_set_option(session, "primaryport", fUsePrimaryPort ? "yes" : "no");
    session_set_option(session, "disablehunt", fDisablePortHunting ? "yes" : "no");
    session_set_im_handler(session, this, on_im_event);
    session_set_presence_handler(session, this, on_presence_event);
    session_set_rpc_request_handler(session, this, "im.invite", on_im_invite);
    if (!targetServer.IsEmpty())
        session_set_target_server(session, targetServer.mb_str());
    session_connect(session, FALSE);

    controller = controller_create(session);
    controller_set_registered_handler(controller, this, on_controller_registered);
    controller_set_invite_lookup_handler(controller, this, on_controller_invite_lookup);
    controller_set_meeting_event_handler(controller, this, on_meeting_event);
    controller_set_meeting_error_handler(controller, this, on_meeting_error);
    controller_set_meeting_chat_handler(controller, this, on_meeting_chat);
    controller_set_submeeting_add_handler(controller, this, on_submeeting_add);
    controller_set_get_params_handler(controller, this, on_get_params);
    controller_set_invite_handler(controller, this, on_controller_invite);
    controller_set_docshare_event_handler(controller, this, on_docshare_event);
    controller_set_im_notify_handler(controller, this, on_controller_im_notify);
    controller_set_ping_handler(controller, this, on_controller_ping);

    wxString userDir = wxStandardPaths::Get().GetUserDataDir();

    book = addressbk_create(session);
    if (PREFERENCES.m_fCacheAB)
        addressbk_enable_cache(book, userDir.mb_str());
    addressbk_set_auth_handler(book, this, on_addressbk_auth);
    addressbk_set_directory_fetched_handler(book, this, on_directory_fetched);
    addressbk_set_update_failed_handler(book, this, on_addressbk_update_failed);
    addressbk_set_updated_handler(book, this, on_address_updated );
    addressbk_set_deleted_handler(book, this, on_address_deleted );
    addressbk_set_profiles_fetched_handler( book, this, on_profile_fetched );
    addressbk_set_version_check_handler( book, this, on_addressbk_version_check );
    addressbk_set_recording_list_handler( book, this, on_addressbk_recording_list );
    addressbk_set_progress_handler(book, this, on_addressbk_progress);

    static const char * fields[] =
    {
        "userid",
        "title",
        "first",
        "middle",
        "last",
        "suffix",
        "company",
        "jobtitle",
        "address1",
        "address2",
        //"city",
        "state",
        "country",
        "postalcode",
        "screenname",
        "email",
        "email2",
        "email3",
        "busphone",
        "homephone",
        "mobilephone",
        "otherphone",
        "aimscreen",
        "msnscreen",
        "yahooscreen",
        "user1",
        "user2",
        "defphone",
        "type",
        //"password",
        "profileid",
        //"enabledfeatures",
        //"disabledfeatures",
        0
    };
    int field_count = 0;
    while( fields[field_count] ) ++field_count;

    addressbk_set_field_list( book, fields, field_count );

    m_pContactService->SetBook( book );
    m_pContactService->SetFields( fields, field_count );
    m_pContactService->SetUser( screen );
    
    sched = schedule_create(session);
    schedule_set_list_handler(sched, this, on_schedule_fetched);
    schedule_set_meeting_handler(sched, this, on_meeting_fetched);
    schedule_set_progress_handler(sched, this, on_schedule_progress);
    schedule_set_updated_handler(sched, this, on_schedule_updated);
    schedule_set_error_handler( sched, this, on_schedule_error);
    
	session_set_rpc_handlers( CONTROLLER.session, this, "rpc_convert", on_conversion_result, on_conversion_error);
    session_set_rpc_request_handler( CONTROLLER.session, this, "ConversionEvent", on_conversion_event);
}

void MeetingCenterController::OnConnectEvent(wxCommandEvent& event)
{
    wxString screen(m_SignOnParams.m_strScreen);
    CUtils::DecodeScreenName(screen);
    
    wxString msg;
    msg = _("Connected");
    if (m_SignOnParams.m_fEnableSSL)
        msg += wxT(" (SSL) ");
    msg += wxT(" - ");
    msg += screen;
    if( !m_SignOnParams.m_strCommunity.IsEmpty( ) )
    {
        msg += wxT(":");
        msg += m_SignOnParams.m_strCommunity;
    }
    showStatus( msg );

    m_ControllerState |= State_Connected;

    // Todo: too soon???
    if( m_pConnectionProgressDialog )
    {
        m_pConnectionProgressDialog->SetConnected();
    }

    CWebDavUtils::SetEnableExternalAuth( session_is_plaintext_auth( session ) );
    CWebDavUtils::SetTrustedCA( CertHandler::GetHandler().GetCAFile() );

#ifdef __WXMSW__
    // Open the current executable's version information.
    wxFileVersion ver;
    ver.Open( );
    wxString    strVer = ver.GetFixedFileVersion( true );
#else
    wxString    strVer( wxT(VERSION) );
    strVer += wxT(".");
    strVer += wxT(BUILD_NUM);
#endif

    wxLogDebug(wxT("Checking version %s"), strVer.c_str());

    strVer.Replace( wxT("."), wxT(", ") );

    int     iRet;
    iRet = addressbk_check_version( book, strVer.mb_str( wxConvUTF8 ) );
}

//*****************************************************************************
// IsUpdateRequired  -- returns TRUE if installer is going to update the client
//
bool MeetingCenterController::IsUpdateRequired( wxString installerUrl, int clientUpgradeFound )
{
    if( NULL  !=  m_pInstallController )
    {
        delete m_pInstallController;
        m_pInstallController = NULL;
    }

    m_pInstallController = new InstallController( );
    m_pInstallController->SetMeetingIds( m_SignOnParams.m_strMeetingPIN, m_SignOnParams.m_strScreen );

    // set location of installer
    m_pInstallController->SetInstallerURL( installerUrl );

    // don't wait for installer to complete - we'll just exit
    m_pInstallController->SetWaitForInstallToComplete( false );

    bool        result      = false;
    wxString    updateStr;
    wxString    strTitle    = PRODNAME;

    //  If nothing to do let's get out of Dodge
    if( clientUpgradeFound  ==  UpgradeNotFound )
    {
        m_bLaunchInstallerAtExit = result;
        m_fCheckVersionCompleted = !result;
        return result;
    }

    // if URL version indicates mandatory update required
    if( clientUpgradeFound  ==  UpgradeMandatory )
    {
        updateStr = _("Update your client!  The version you are running must be updated to a new version to continue.\n\nSelecting OK will download and install the new version. \n\n");
        wxString    strT( _("The application will then be restarted.") );
        updateStr += strT;
        wxMessageDialog     dlg( NULL, updateStr, strTitle, wxOK | wxICON_EXCLAMATION );
        dlg.ShowModal( );
        result = true;
    }
    else if( clientUpgradeFound  ==  UpgradeOptional )
    {
        // confirm with user via dialog
        updateStr = _("Update your client!  An optional upgrade is available. \n\n Selecting OK will download and install the new version. \n\n");
        wxString    strT( _("The application will then be restarted.") );
        updateStr += strT;
        wxMessageDialog     dlg( NULL, updateStr, strTitle, wxOK | wxCANCEL | wxICON_EXCLAMATION );
        if( dlg.ShowModal( )  ==  wxID_OK )
            result = true;
    }

    // if we're doing the upgrade
    if( result  ==  true )
    {
        int     iFlags = wxPD_APP_MODAL|wxPD_CAN_ABORT;

        if (m_pConnectionProgressDialog)
            m_pConnectionProgressDialog->Show(false);
        if (m_pReconnectDialog)
            m_pReconnectDialog->Show(false);

        // save installer via progress dialog
        wxString    strT( _("Download installer") );
        wxString    strT2( _("Please wait while application update is downloaded.") );
        
        result = m_pInstallController->DownloadInstaller( strT, iFlags, strT2, this );
        wxLogDebug( wxT("  after DownloadInstaller( ) --  it returned %d"), result );

        // clean up open handles
        m_pInstallController->Cleanup( );
    }

    m_bLaunchInstallerAtExit = result;
    m_fCheckVersionCompleted = !result;

    if (!result && clientUpgradeFound  ==  UpgradeMandatory)
    {
        wxMessageDialog     dlg( NULL, _("The required upgrade could not be completed at this time.  Please try again later."), strTitle, wxOK | wxICON_EXCLAMATION );
        dlg.ShowModal( );
        return true;
    }

    return result;
}


//*****************************************************************************
void MeetingCenterController::EnableWindows( bool fEnable, bool fFocus )
{
    //  Enable/Disable any meetings that are running
    wxWindowListNode    *node       = wxTopLevelWindows.GetFirst( );
    while( node )
    {
        wxWindow    *win = node->GetData( );
        if( win->IsKindOf(CLASSINFO(MeetingMainFrame)) )
        {
            win->Enable( fEnable );
        }
        else if( win->IsKindOf(CLASSINFO(CreateContactDialog)) )
        {
            win->Enable( fEnable );
        }
        node = node->GetNext( );
    }

    //  If MeetingSetupDialog is open enable/disable it
    if (m_pMeetingSetupDialog != NULL)
    {
        m_pMeetingSetupDialog->Enable( fEnable );
    }

    //  If ManageContactsFrame is open enable/disable it
    if( m_pManageContactsFrame  !=  NULL )
    {
        m_pManageContactsFrame->Enable( fEnable );
    }

    if( m_fHaveGUI )
    {
        Enable( fEnable );
        if ( fFocus )
            SetFocus( );
    }
}


//*****************************************************************************
void MeetingCenterController::SessionIsReconnected( )
{
    wxLogDebug( wxT("entering MeetingCenterController::SessionIsReconnected( )") );

    if( m_ControllerState & State_WaitingForReconnect )
    {
        //  Notify all the running meetings that we are reconnected
        wxWindowListNode    *node       = wxTopLevelWindows.GetFirst( );
        while( node )
        {
            wxWindow    *win = node->GetData();
            if( win->IsKindOf(CLASSINFO(MeetingMainFrame)) )
            {
                ((MeetingMainFrame *) win)->ConnectionReconnected( );
            }
            else if( win->IsKindOf(CLASSINFO(CreateContactDialog)) )
            {
                wxDialog    *pDlg = (wxDialog *) node->GetData( );
                if( pDlg->IsModal( ) )
                    pDlg->EndModal( wxID_CANCEL );
            }

            node = node->GetNext( );
        }
        
        if( m_pManageContactsFrame )
        {
            m_pManageContactsFrame->ConnectionReconnected();
        }

        EnableWindows( true );
        m_ControllerState &= ~State_WaitingForReconnect;

        //  If MeetingSetupDialog was open when we were disconnected, close it so we don't * * CRASH * *
        if (m_pMeetingSetupDialog != NULL)
        {
            m_pMeetingSetupDialog->ForceNoSave( );
            m_pMeetingSetupDialog->EndModal( wxID_CANCEL );
        }
    }

    EnableWindows( true );
    m_ControllerState &= ~State_WaitingForReconnect;
}

void MeetingCenterController::ProcessCertificates(const wxString& userDir)
{
    CertHandler& handler = CertHandler::GetHandler();

    handler.Initialize(userDir + wxT("/allowedCA.pem"));

    wxString strSystemCA = wxStandardPaths::Get().GetDataDir() + wxT("/") +
                           wxString::FromAscii(wxGetApp().Options().GetSystem( "TrustedCA", "" ));
    wxString strAcceptedCA = wxStandardPaths::Get().GetUserDataDir() + wxT("/serverCA.pem");

    handler.AddCertFile(strSystemCA);
    handler.AddCertFile(strAcceptedCA);
    handler.WriteCerts();
}


void MeetingCenterController::AddCertificate(const wxString &strCert, bool bAccept)
{
    CertHandler& handler = CertHandler::GetHandler();

    if (bAccept)
    {
        handler.AddCertToFile(strCert, 1);
    }
    else
    {
        handler.AddTempCert(strCert);
    }

    handler.WriteCerts();
}

bool MeetingCenterController::ShowCertificate(wxString &details)
{
    bool accept = false;
    int         iStart, iEnd;
    wxString    strStart( wxT("<certificate>") );
    wxString    strEnd( wxT("</certificate>") );
    wxString    strCert;

    iStart = details.Find( strStart );
    iEnd = details.Find( strEnd );
    if( iStart != wxNOT_FOUND  &&  iEnd != wxNOT_FOUND )
    {
        iStart += strStart.Len( );
        strCert = details.Mid( iStart, iEnd - iStart );
    }

    details.Replace( wxT("\n"), wxT("") );

    char    *pBuf   = NULL;
    pBuf = (char *) malloc( details.Len( ) + 1 );
    strcpy( pBuf, details.mb_str( wxConvUTF8 ) );

    int                 iRet;
    wxMemoryInputStream iStream( pBuf, details.Len( ) );
    wxXmlDocument       doc( iStream );
    wxXmlNode           *pRoot;
    if( doc.IsOk( ) )
    {
        pRoot = doc.GetRoot( );
        if( pRoot )
        {
            BadAuthCertDialog   dlg( this, pRoot );
            iRet = dlg.ShowModal( );
            if( iRet == wxID_OK )
            {
                if( dlg.isRadioAccept( )  ||  dlg.isRadioTemp( ) )
                {
                    AddCertificate(strCert, dlg.isRadioAccept());
                    accept = true;
                }
            }
        }
    }
    free( pBuf );
    return accept;
}

//*****************************************************************************
void MeetingCenterController::OnSessionError(SessionErrorEvent& event)
{
    int error_code = event.GetErrorCode();

    wxString error_msg;
    wxString rpc_id;
    wxString opid;
    wxString details;
    error_msg = event.GetMessage();
    rpc_id = event.GetRpcId();
    opid = event.GetOpid();
    details = event.GetDetails();

    if( m_ControllerState & State_WaitingForSignOn && error_code == ERR_SSLTLS_HANDSHAKE && m_SignOnParams.m_fEnableSSL )
    {
        wxLogDebug(wxT("SSL handshake error, trying again with SSL off"));
        
        m_SignOnParams.m_fEnableSSL = false;

        // Try again with SSL off
        CreateSession(
            m_SignOnParams.m_strServer,
            wxT(""),
            m_SignOnParams.m_strScreen,
            m_SignOnParams.m_strPassword,
            !m_SignOnParams.m_fAutoHunt,
            m_SignOnParams.m_fEnableSSL,
            PREFERENCES.m_strLastJID
            );

        return;
    }

    if( m_ControllerState & State_WaitingForSignOn && error_code == ERR_WRONGSERVERNAME )
    {
        wxLogDebug(wxT("Wrong hostname used, retrying with %s"), details.wc_str());
        
        // Try again with the host name returned by the server
        CreateSession(
            details,
            m_SignOnParams.m_strServer,
            m_SignOnParams.m_strScreen,
            m_SignOnParams.m_strPassword,
            !m_SignOnParams.m_fAutoHunt,
            m_SignOnParams.m_fEnableSSL,
            PREFERENCES.m_strLastJID
            );
        return;
    }

    wxString xMsg( wxGetTranslation( error_msg ) );
    wxLogDebug( wxT("SESSION ERROR: code: %d,  msg: *%s*,  translated msg: *%s*,  rpd_id: *%s*,  opid: *%s*,  details: *%s*"),
                error_code, error_msg.wc_str( ), xMsg.wc_str( ), rpc_id.wc_str( ), opid.wc_str( ), details.wc_str( ) );
    showStatus( xMsg );

    wxLogDebug( event.GetMessage().c_str() );
    wxLogDebug( xMsg.c_str() );

    // If the user is not signed on yet, and the connection
    // progress screen is displayed, then let the connection progress screen
    // handle the error.
    if( m_ControllerState & State_WaitingForSignOn )
    {
        if( error_code  ==  ERR_CERT_SIGNER_NOT_FOUND )
        {
            if (ShowCertificate(details))
            {
                session_connect(session, FALSE);
                return;
            }
        }

        if( m_pConnectionProgressDialog )
        {
            m_pConnectionProgressDialog->SetError( xMsg );

            //  Call SignOff( ) to cleanup and prepare for next signon attempt
            SignOff( false );
            return;
        }
    }

    //  Stop the timer until we are reconnected
    m_PingTimer.Stop( );
    m_iPingsSent     = 0;
    m_iPingsReceived = 0;

    //  We just tried to reconnect and it failed; so just continue to wait...
    if( (m_ControllerState & State_WaitingForReconnect)  &&  
        (error_code == ERR_CONNECTLOST || error_code == ERR_CONNECTFAILED || error_code == ERR_CERT_SIGNER_NOT_FOUND) )
    {
        wxCommandEvent  event( wxEVT_UPDATE_RECONNECT_STATUS, wxID_ANY );
        event.SetInt( error_code );
        m_pReconnectDialog->AddPendingEvent( event );
    }

    //  If the user has logged in elsewhere warn them then shut down this session
    if( error_code  ==  ERR_SIGNEDONELSEWHERE )
    {
        wxString    strBrand = wxString::FromAscii( wxGetApp().Options().GetSystem( "BrandName", "" ) );

        CloseAllMeetings();
        CUtils::MsgWait( 200, 3 );

        SignOff( false );

        wxMessageDialog dlg( NULL, _("You have Signed On to a different machine or device and have been Signed Off here."), strBrand, wxOK | wxICON_WARNING );
        dlg.ShowModal( );

        // ToDo: make sure we don't auto-signon
        SignOn();

        return;
    }

    // If we are not shutting down and we have lost our connection to the server
    if ( (m_ControllerState & State_Closing) == 0 && error_code == ERR_CONNECTLOST)
    {
        // Inform user
        if( m_pReconnectDialog  ==  NULL )
        {
            m_pReconnectDialog = new ReconnectDialog( NULL, 0 );
            m_pReconnectDialog->Connect( XRCID("wxID_CANCEL"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&MeetingCenterController::OnReconnectCancelClick, 0, this);
        }

        //  NOTE:  This dialog is not modal because I don't want to block execution while
        //  we are attempting to reconnect.  Also, if connection is re-established, this
        //  dialog will be hidden by either SessionIsReconnected( ) or OnConnectEvent( ), above.
        EnableWindows( false );
        m_pReconnectDialog->Show( true );

        // Clear out previous session state
        m_cab = 0;
        m_pab = 0;

        session_flush( session );
        session_disconnect( session, TRUE );
        m_ControllerState |= State_WaitingForReconnect;

        wxCommandEvent  event( wxEVT_UPDATE_RECONNECT_STATUS, wxID_ANY );
        event.SetInt( error_code );
        m_pReconnectDialog->AddPendingEvent( event );
    }

    if( IsOpcode( opid, OP_EditMyContact, false ) )
    {
        m_ControllerState &= ~State_WaitingForUpdate;
        m_strDatabaseError = error_msg;
        m_nDatabaseError = error_code;
        return;
    }

    event.Skip();
}


//*****************************************************************************
void MeetingCenterController::OnReconnectCancelClick( wxCommandEvent& event )
{
    wxLogDebug( wxT("entering MeetingCenterController::OnReconnectCancelClick( )") );

    m_pReconnectDialog->Destroy( );
    m_pReconnectDialog = 0;

    EnableWindows( true );
    m_ControllerState &= ~State_WaitingForReconnect;

    SignOff(false);
    SignOn();
}


//*****************************************************************************
void MeetingCenterController::OnAddressbkProgressEvent(wxCommandEvent& event)
{
    wxString    opid    = event.GetString( );
    int         iNum    = event.GetInt( );

    wxLogDebug( wxT("entering MeetingCenterController::OnAddressbkProgressEvent( )  --  opid = %s  --  iNum = %d"), opid.wc_str( ), iNum );

    if( !(opid == opcode(OP_MCC_CAB) || opid == opcode(OP_MCC_PAB)) )
    {
        // Not from around here
        event.Skip();
        return;
    }
}


//*****************************************************************************
void MeetingCenterController::OnAuthEvent(wxCommandEvent& event)
{
    auth_info_t auth = addressbk_get_auth_info(book);

    // If auth->session_id is NULL, authenticate failed.
    if (auth->session_id == NULL)
    {
        //  Call SignOff( ) to cleanup and prepare for next signon attempt
        SignOff( false );

        if( m_pConnectionProgressDialog )
        {
            m_pConnectionProgressDialog->SetError( _("Unable to register with the server.  Please try again later.") );
        }

        return;
    }

    m_displayName = wxString(auth->first, wxConvUTF8),  + _T(" ") + wxString(auth->last, wxConvUTF8);

    wxString title;
    title = PRODNAME + _T(" - ");
    title += m_displayName;
    if( !m_SignOnParams.m_strCommunity.IsEmpty( ) )
    {
        title += wxT(":");
        title += m_SignOnParams.m_strCommunity;
    }
    this->SetTitle( title );

    // Store the sign on data as server settings.
    wxLogDebug( wxT("MeetingCenterController::OnAuthEvent - Server:%s , Screen:%s, Usertype:%d"),m_SignOnParams.m_strServer.c_str(), m_SignOnParams.m_strScreen.c_str(),auth->user_type );

    wxGetApp().Options().SetSystem( "lastserver",   m_SignOnParams.m_strServer.mb_str(wxConvUTF8));
    wxGetApp().Options().SetSystem( "lastuser", m_SignOnParams.m_strScreen.mb_str(wxConvUTF8));

    if( !m_SignOnParams.m_fSavePassword || m_SignOnParams.m_fAnonymousUser )
    {
        wxString    strBlank( wxT("") );
        wxGetApp().Options().SetSystem( "lastpwd", strBlank.mb_str(wxConvUTF8) );
    }
    else
    {
        CBase64     base64;
        char        *pwdEncoded;
        pwdEncoded = base64.Encode( m_SignOnParams.m_strPassword.mb_str(wxConvUTF8), m_SignOnParams.m_strPassword.Len( ) );
        wxGetApp().Options().SetSystem( "lastpwd",  pwdEncoded );
        free( pwdEncoded );
    }

    //  lastpwd is Base64 encoded starting with file version 5
    wxGetApp().Options().SetSystemInt( "Version", 5 );

    wxGetApp().Options().Write();

    wxGetApp().Options().SetScreenName( m_SignOnParams.m_strScreen );

    if ( !wxGetApp().Options().Read() )
    {
        wxMessageBox( _("Unable to read application preferences file\nafter authentication received"), _("Error!"), wxICON_ERROR, this);
        return;
    }

    // Now store user specific sign on data.
    wxGetApp().Options().SetUserBool("SignOnSavePassword", m_SignOnParams.m_fSavePassword );
    if( !m_SignOnParams.m_fSavePassword )
        wxGetApp().Options().SetUser("SignOnPassword", "" );
    wxGetApp().Options().SetUserBool("SignOnAutoSignOn",   m_SignOnParams.m_fAutoSignOn );
    wxGetApp().Options().SetUserBool("SignOnAutoRetry",    m_SignOnParams.m_fKeepTryingConnection );
    wxGetApp().Options().SetUserBool("SignOnAutoHunt",     m_SignOnParams.m_fAutoHunt );

    m_ControllerState |= State_Authenticated;

    if (auth->user_type == UserTypeAdmin)
    {
        m_ControllerState |= State_AdministratorMode;
    }
    m_strUserId = wxString( auth->user_id, wxConvUTF8 );

    if( m_pConnectionProgressDialog )
    {
        m_pConnectionProgressDialog->SetAuthenticated();
    }

    addressbk_fetch_profiles(book, "profiles");

    controller_register(controller, auth, FALSE);

    // Send presence to controller now so unavailable presence is sent later when we disconnect
    char controller_address[512];
    session_get_service_jid(session, "controller", controller_address);
    session_send_presence(session, controller_address, PRES_AVAILABLE, NULL);

    // Skip this event so the connection progress dialog and see it
    event.Skip();
}


//*****************************************************************************
void MeetingCenterController::OnControllerInviteEvent(wxCommandEvent& event)
{
    meeting_invite_t invite = (meeting_invite_t) event.GetClientData( );
    wxString    strMID( invite->meeting_id, wxConvUTF8 );
    wxString    strPID( invite->participant_id, wxConvUTF8 );
    wxString    strPName( invite->participant_name, wxConvUTF8 );
    wxString    strTitle( invite->title, wxConvUTF8 );

    MeetingMainFrame *mtgWindow = FindMeetingWindow( strMID );

#ifdef TESTCLIENT
    wxLogDebug( wxT("entering MCC::OnControllerInviteEvent( event )  --  TESTCLIENT mode") );
    if( STARTUP.m_StartupTest.CmpNoCase( wxT("accept") )  ==  0 )
    {
        m_pDialogInvite = NULL;
        //OnMeetingInviteAccepted( event );
        meeting_invite_destroy( invite );
        return;
    }
#endif

    wxLogDebug( wxT("Invited to meeting %s, pin %s"), strMID.c_str(), strPID.c_str() );

    if( m_pDialogInvite )
    {
        // Already are presenting an invitation, show again
        wxLogDebug( wxT("Previous invite still open, ignoring new one") );
        m_pDialogInvite->Raise( );
    }
    else
    {
        // Ignore if invited to meeting we're already in or already connected to via voip
        if ( mtgWindow && ( invite->invite_type  == InviteNormal || (mtgWindow->PhoneController() && mtgWindow->PhoneController()->IsInCall()) ) )
        {
            meeting_invite_destroy( invite );
            return;
        }

        InviteDialog *pDlg = new InviteDialog( this ); 
        m_pDialogInvite = pDlg;

        // Present invite to user
        bool accept = pDlg->ShowInvite(invite);
        if (accept)
        {
            // Join meeting
            if ( !mtgWindow )
            {
                Commands( ).Add( wxT("join-meeting"), strPID, strPName, strMID, strTitle );
            }

            if ( mtgWindow && (invite->invite_type == InvitePCPhone || invite->invite_type == InvitePCPhoneDirect) )
            {
                Commands( ).Add( wxT("connect-pcphone"), strMID );
            }

            MeetingMainFrame *activeWindow = FindMeetingWindow();

            if ( !mtgWindow && activeWindow )
            {
                // Close meeting first.
                activeWindow->EndMeeting( );
            }
            else
            {
                // Not in meeting, execute now.
                ExecutePendingCommands( true );
            }
        }

        delete m_pDialogInvite;
        m_pDialogInvite = NULL;
    }

    meeting_invite_destroy( invite );
}


//*****************************************************************************
void MeetingCenterController::OnControllerIMNotify(wxCommandEvent& event)
{
    participant_t part = (participant_t) event.GetClientData( );
    wxString mid(event.GetString( ));

    meeting_t mtg = controller_find_meeting(controller, mid.mb_str(wxConvUTF8));
    if (mtg != NULL)
    {
        /*** FUTURE WORK:
        // See what kind of notification to send to Pidgin:
        int nIM = (NotifyIIC|NotifyAIM|NotifyYahoo|NotifyMessenger|NotifyIMAny);
        bool fNotifyIM = (part->notify_type & nIM) != 0;
        bool fNotifyPhone = (part->notify_type & NotifyPhone) != 0;
        bool fPCPhoneNotify = !fNotifyIM && fNotifyPhone && (part->selphone == SelPhonePC);
        ***/

        const char *pid = strchr(part->part_id, ':') + 1;
        wxString    strDesc( mtg->description, wxConvUTF8 );
        wxString    strHost( mtg->host_id, wxConvUTF8 );
        wxString    strMID( mtg->meeting_id, wxConvUTF8 );
        wxString    strPID( pid, wxConvUTF8 );
        wxString    strPName( part->name, wxConvUTF8 );
        wxString    strTitle( mtg->title, wxConvUTF8 );
        wxString    strMessage( mtg->invite_message, wxConvUTF8 );
        wxString    strSystem( _T("zon") );
        wxString    strScreen( part->screenname, wxConvUTF8 );

        /***** FUTURE WORK
        if( fPCPhoneNotify )
        {
            wxLogDebug(wxT("OnControllerIMNotify: PC Phone Notify for %s"),strPName.c_str());
        }
        else
        {
            wxLogDebug(wxT("OnControllerIMNotify: IM Notify for %s"),strPName.c_str());
        }
        ***********/

        wxGetApp().SendQueueInvite(strMID, strTitle, strDesc, strMessage, strPID, strPName, strSystem, strScreen);
    }
}

void MeetingCenterController::OnRegisteredEvent(wxCommandEvent& event)
{
    m_fPublicMeetingsExpanded = wxGetApp().Options().GetUserBool( "fPublicMeetingsExpanded", false );
    m_fMyMeetingsExpanded = wxGetApp().Options().GetUserBool( "fMyMeetingsExpanded", true );
    m_fPastMeetingsExpanded = wxGetApp().Options().GetUserBool( "fPastMeetingsExpanded", false );
    m_fUnscheduledMeetingsExpanded = wxGetApp().Options().GetUserBool( "fUnscheduledMeetingsExpanded", true );
    m_fMyInstantMeetingExpanded = wxGetApp().Options().GetUserBool( "fMyInstantMeetingExpanded", true );
    m_fInvitedMeetingsExpanded = wxGetApp().Options().GetUserBool( "fInvitedMeetingsExpanded", true );
    m_nLastSortColumn = wxGetApp().Options().GetUserInt( "nLastSortColumn", Col_Title  );
    m_fLastSortAscending = wxGetApp().Options().GetUserBool( "fLastSortAscending", true );

    // Clear the waiting for sign on state
    m_ControllerState &= ~State_WaitingForSignOn;

    // We are connected
    m_ControllerState |= State_Registered;

    if( m_pConnectionProgressDialog )
    {
        m_pConnectionProgressDialog->SetRegistered();
        m_pConnectionProgressDialog->Terminate();
        m_pConnectionProgressDialog = 0;
        EnableWindows(true, true);

        // Meeting center may have been hidden by sign out, so make sure to restore here.
        if (!IMClientMode() && !AnonymousMode())
            ShowAppWindow();
    }

    SetUIFeedback();

    const char *pURL;
    pURL = controller_get_docshare_server( CONTROLLER.controller, "" );
    wxString docServer( pURL, wxConvUTF8 );
    FileManager::SetUploadURL( docServer );

    // Get the users profile and enforce any settings that have changed
    m_profile = addressbk_get_profile(book);
    bool fUpdatePreferences = false;

    // Enforce the profile settings.
    if( (m_profile->enabled_features & ProfileUserSavePassword) == 0 )
    {
        PREFERENCES.m_fSavePassword = false;
        PREFERENCES.m_fAutoSignOn = false;
        PREFERENCES.m_strPassword.Empty( );
        fUpdatePreferences = true;
        OPTIONS.SetUserBool("SignOnSavePassword", false );
        OPTIONS.SetUser("SignOnPassword", "" );
        OPTIONS.SetUserBool("SignOnAutoSignOn",   false );
        OPTIONS.SetSystem( "lastpwd",  "" );
        OPTIONS.Write( );
    }
    if( (m_profile->enabled_features & ProfileDisableMeetingDownload) != 0 )
    {
        if( PREFERENCES.m_fDownloadPublicMeetings )
        {
            PREFERENCES.m_fDownloadPublicMeetings = false;
            fUpdatePreferences = true;
        }
    }
    if( (m_profile->enabled_features & ProfileDisableCommunityDownload) != 0 )
    {
        if( PREFERENCES.m_fDownloadCAB )
        {
            PREFERENCES.m_fDownloadCAB = false;
            fUpdatePreferences = true;
        }
    }
    if( fUpdatePreferences )
    {
        PREFERENCES.Save();
    }

    // Skip this event so the connection progress dialog and see it
    event.Skip();

    if ( !AnonymousMode() )
    {
        schedule_set_authentication(sched, addressbk_get_auth_info(book));

        if ( m_fHaveGUI )
        {
            // Set the last known sort info
            if( m_nLastSortColumn < 0 )
            {
                m_nLastSortColumn = Col_Title;
                m_fLastSortAscending = true;
            }
            m_meetingList->SetSortColumn( m_nLastSortColumn );
            m_meetingList->SetSortAscending( m_nLastSortColumn, m_fLastSortAscending );
        }

        RefreshMeetingsList();
    }

    if( m_pConnectionProgressDialog )
    {
        m_pConnectionProgressDialog->Terminate();
        m_pConnectionProgressDialog = 0;
        EnableWindows(true, true);
    }

    if( AnonymousMode( ) )
    {
        // ToDo: streamline this case if we already have meeting id and other info.
        controller_lookup_invite( controller, m_SignOnParams.m_strMeetingPIN.mb_str() );
    }
    else
    {
        // If the sign on parameters includes a meeting ID
        if( !m_SignOnParams.m_strMeetingPIN.IsEmpty() )
        {
            // We need to queue up this request to start/join a meeting.
            // The meeting ID was entered by the user on the sign on screen.
            // We need to validate the Meeting Id.
            m_Commands.Add(wxT("join-meeting-by-pin"),m_SignOnParams.m_strMeetingPIN);
        }
    }

    // Notify anyone waiting to reconnect if we were reconnecting
    if (m_ControllerState & State_WaitingForReconnect)
    {
        SessionIsReconnected();
    }

    if (IMClientMode())
    {
        session_send_presence(session, NULL, PRES_AVAILABLE, (char *)"iic:");
    }

    if (!AnonymousMode() && IMClientMode() && m_pManageContactsFrame)
    {
        m_pManageContactsFrame->DisplayContacts();
    }
}

void MeetingCenterController::OnControllerInviteLookupEvent(wxCommandEvent& event)
{
    meeting_invite_t inv = (meeting_invite_t) event.GetClientData( );

    wxLogDebug( wxT("entering MeetingCenterController::OnControllerInviteLookupEvent( )") );

    wxString    strMID( inv->meeting_id, wxConvUTF8 );
    wxString    strPID( inv->participant_id, wxConvUTF8 );
    wxString    strPName( inv->participant_name, wxConvUTF8 );
    wxString    strTitle( inv->title, wxConvUTF8 );

    if ( strPID.IsEmpty( ) && strMID.IsEmpty( ) )
    {
        wxMessageBox(_("The entered PIN is not valid."), _("Meeting Center"), wxOK | wxICON_EXCLAMATION);        
        meeting_invite_destroy( inv );
    }
    else if( strPID.IsEmpty( )  &&  !strMID.IsEmpty( )  &&  !AnonymousMode( ) )
    {
        //  See if we can look up the user's PIN for this meeting
        m_join_inv = inv;
        FetchMeetingDetails( OP_FindMyPINAndJoin, strMID );
    }
    else
    {
        Commands( ).Add( wxT("join-meeting"), strPID, strPName, strMID, strTitle );
        ExecutePendingCommands( );

        meeting_invite_destroy( inv );
    }
}

void MeetingCenterController::OnConnectionProgreesDone(wxCommandEvent& event)
{
    wxLogDebug( wxT("MeetingCenterController::OnConnectionProgreesDone") );

    if( m_pConnectionProgressDialog )
    {
        m_pConnectionProgressDialog->Disconnect( wxID_ANY, wxEVT_CLOSE_WINDOW, (wxObjectEventFunction)&MeetingCenterController::OnConnectionProgreesDone, 0, this);
        m_pConnectionProgressDialog->Terminate();
        m_pConnectionProgressDialog = 0; // Already deleted.
        EnableWindows(true, true);

        if (m_SignOnParams.m_fTestConnection)
        {
            m_SignOnParams = m_SavedSignOnParams;
            m_SignOnParams.m_fTestConnection = false;
            SignOff( false );
            SignOn( );
        }
        else if( m_ControllerState & State_WaitingForSignOn )
        {
            // If auto sign on is currently ON, then turn it off
            m_SignOnParams.m_fAutoSignOn = false;
            PREFERENCES.m_fAutoSignOn = false;
            m_ControllerState &= ~State_WaitingForSignOn;
            m_SignOnParams.m_fSignOnCancel = true;

            session_disconnect(session, TRUE);

            // Try again.
            if( m_iAnonSignOnTries  <  kAnonSignOnTriesLimit )
            {
                SignOn( );
            }
            else
                Close( );
        }
    }
}

void MeetingCenterController::RefreshMeetingsList()
{
    wxLogDebug(wxT("MeetingCenterController::RefreshMeetingsList"));

    if ( AnonymousMode() )
        return;

    if( m_meetingList->GetColumnCount() > 0 )
    {
        m_meetingList->DeleteRoot();

        // Clear some state flags
        m_ControllerState &= ~(State_HaveInstantMID|State_HaveMyMeetings|State_HavePublic|State_HaveInvited);

        SetupMeetingList();
    }

    //////////////////////////////////////////////////////////////////////
    // Retrieve the list of meetings:
    // The OnScheduleFetched() handler will populate the m_meetingList
    /////////////////////////////////////////////////////////////////////

    m_fPublicMeetingsAreLoaded = false;
    m_fInvitedMeetingsAreLoaded = false;
    m_fMyMeetingsAreLoaded = false;

    if( PREFERENCES.m_fDownloadPublicMeetings )
    {
        schedule_find_public_meetings(sched, opcode(OP_MCC_Public).mb_str(wxConvUTF8) );
    }
    else
        m_fPublicMeetingsAreLoaded = true;

    schedule_find_invited_meetings(sched, opcode(OP_MCC_Invited).mb_str(wxConvUTF8) );

    static bool fSchedMyMtgs = true;
    if( fSchedMyMtgs )
        schedule_find_my_meetings(sched, opcode(OP_MCC_Scheduled).mb_str(wxConvUTF8) );

    // TODO: separate contact loading from meeting list.  Currently this is how contacts
    // are initially loaded when a user signs on so this can't simply be removed.
    int ret = 0;
    ret = RefreshContacts( false, false );

    SetUIFeedback();
}

wxString MeetingCenterController::FetchProfileImage()
{
    wxString profileImage;
    FileManager mgr(FileManager::GetPathForUser(m_SignOnParams.m_strScreen));
    mgr.DownloadFile(wxT("profilePicture.jpg"), profileImage, true);
    return profileImage;
}

void MeetingCenterController::OnScheduleFetched(wxCommandEvent& event)
{
    meeting_list_t list = (meeting_list_t)event.GetClientData();
    meeting_info_t info;
    meeting_iterator_t iter;
    wxTreeItemId parent_node;
    bool fMyMeetings = false;       // true == My Meetings. Used to pick the right root node in the tree
    bool fMyInstantMeeting = false; // true == Flag the one instant meeting in order to grab the meeting ID
    bool fInvitedMeetings = false;  // true == Invited meetings node
    bool fPublicMeetings = false;   // true == Public meetings node
    bool fFoundMeetings = false;    // true == Search results node
    bool fUnknownParent = false;    // true == Search results node, unable to locate the parent node.
    int nRecords = 0;
    SearchResultsTreeItemData *pSrch = 0;

    wxString opid;
    opid = event.GetString();

    wxLogDebug(wxT("MeetingCenterController::OnScheduleFetched: opid = %s"),opid.c_str() );

    if ( opid == opcode( OP_MCC_Scheduled ) )
    {
        // Get the meeting details for "My meetings"
        schedule_find_meeting_details(sched, opcode( OP_Instant ).mb_str(wxConvUTF8), schedule_get_my_instant_meeting(sched));
        fMyMeetings = true;
        m_fMyMeetingsAreLoaded = true;
        wxLogDebug(wxT("MeetingCenterController::OnScheduleFetched - My Meetings"));
    }
    else if( opid == opcode( OP_MCC_Public ) )
    {
        if( !PREFERENCES.m_fDownloadPublicMeetings )
        {
            //  Preference set to NOT download Public meetings  --  We shouldn't be here, so quitely return
            return;
        }
        else if( !m_PublicMeetingsId.IsOk( ) )
        {
            //  No Public meeting node - add one.
            m_PublicMeetingsId = m_meetingList->PrependItem( m_RootId, _("Public"), -1, -1, new MeetingTreeItemData( Node_Root ) );
        }
        parent_node = m_PublicMeetingsId;
        fPublicMeetings = true;
        m_fPublicMeetingsAreLoaded = true;
        wxLogDebug(wxT("MeetingCenterController::OnScheduleFetched - Public meetings"));
    }
    else if( opid == opcode( OP_MCC_Invited ) )
    {
        parent_node = m_InvitedMeetingsId;
        fInvitedMeetings = true;
        m_fInvitedMeetingsAreLoaded = true;
        wxLogDebug(wxT("MeetingCenterController::OnScheduleFetched - Invited meetings"));
    }
    else
    {
        wxString strSearchOp;
        strSearchOp = opcode( OP_MCC_Search );
        strSearchOp += wxT(":");

        if( opid.Left( strSearchOp.length() ) == strSearchOp )
        {
            // Get the search number
            wxString strSearchNumber;
            strSearchNumber = event.GetString().Mid( strSearchOp.length() );
            long nSearchNumber = -1;

            bool fOK = strSearchNumber.ToLong( &nSearchNumber, 10 );
            if( fOK )
            {
                fFoundMeetings = true;

                // Locate the parent node for this search number
                wxTreeItemIdValue cookie;
                fUnknownParent = true;
                for ( wxTreeItemId item = m_meetingList->GetFirstChild(m_CommunitySearchId, cookie);
                  item.IsOk();
                  item = m_meetingList->GetNextChild(m_CommunitySearchId, cookie) )
                {
                    SearchResultsTreeItemData *pData = (SearchResultsTreeItemData *)m_meetingList->GetItemData(item); // Const cast s/b safe enough
                    if( pData->m_nSearchNumber == nSearchNumber )
                    {
                        parent_node = item;
                        fUnknownParent = false;
                        pSrch = GetSearchNode( parent_node );
                        break;
                    }
                }
            }
            wxLogDebug(wxT("MeetingCenterController::OnScheduleFetched - Found meetings - Found Parent Node = %d"),!fUnknownParent);
        }
    }

    time_t now = time(NULL);
    
    iter = meeting_list_iterate(list);
    while ((info = meeting_list_next(list, iter)) != NULL)
    {
        ++nRecords;

        // If these are search results and we have already
        // displayed partial search results.
        if( fFoundMeetings && nRecords <= pSrch->m_nStartingRow )
            continue; // Skip this record...

        wxString meeting_id(info->meeting_id, wxConvUTF8);
        wxString host_id(info->host_id, wxConvUTF8);
        wxString title(info->title, wxConvUTF8);
        wxString type( typeText( info->type ) );
        wxString privacy( privacyText( info->privacy ) );
        wxString startTime;
        startTime = DateTimeDecode( info->start_time );
        //int     options;
        wxString my_participant_id( info->my_participant_id, wxConvUTF8 );
        wxString moderator;
        if( info->my_role == RollModerator  ||  info->my_role == RollHost )
        {
            moderator = _("Yes");
        }
        else
        {
            moderator = _("No");
        }
        wxString host_first_name( info->host_first_name, wxConvUTF8 );
        wxString host_last_name( info->host_last_name, wxConvUTF8 );
        wxString host = host_first_name;
        host += wxT(" ");
        host += host_last_name;

        int iImage = Meeting_Img_Unknown;
        if( info->status == StatusInProgress )
            iImage = Meeting_Img_Online;
        else
            iImage = Meeting_Img_Offline;

        // Point to the correct parent
        if( fMyMeetings )
        {
            // If this is the one instant meeting, then change parent nodes
            if( info->type == TypeInstant )
            {
                parent_node = m_MyInstantMeetingId;
                fMyInstantMeeting = true;
            }
            else if ( info->start_time == 0 )
            {
                parent_node = m_UnscheduledMeetingsId;
            }
            else if ( info->start_time + 30*60 < now )
            {
                parent_node = m_PastMeetingsId;
            }
            else
            {
                parent_node = m_MyMeetingsId;
            }
        }

        int nNodeType;
        if( fMyMeetings )
        {
            nNodeType = Node_OwnsMeeting;
        }
        else if( fInvitedMeetings )
        {
            nNodeType = Node_InvitedTo;
        }
        else if( fPublicMeetings )
        {
            nNodeType = Node_Public;
        }
        else if( fFoundMeetings )
        {
            // We need to find out the users relationship to this meeting
            MeetingTreeItemData *pData = LookupMeetingId( meeting_id );
            if( pData )
            {
                nNodeType = pData->m_nType;
            }
            else
            {
                // Must be public
                nNodeType = Node_Public;
            }

            wxASSERT(!fUnknownParent);
            if( fUnknownParent )
            {
                // No parent node ?
                wxLogDebug(wxT("   Unknown search results parent node. Aborting results dsiplay."));
                return;
            }
        }

        if ( parent_node )
        {
            wxTreeItemId idMeeting = m_meetingList->AppendItem( parent_node, title, iImage, iImage, new MeetingTreeItemData( nNodeType, info ) );
            m_meetingList->SetItemText( idMeeting, Col_Id,          meeting_id );
            m_meetingList->SetItemText( idMeeting, Col_PIN,         my_participant_id );
            m_meetingList->SetItemText( idMeeting, Col_Host,        host );
            m_meetingList->SetItemText( idMeeting, Col_DateTime,    startTime );
            m_meetingList->SetItemText( idMeeting, Col_Privacy,     privacy );
            m_meetingList->SetItemText( idMeeting, Col_Moderator,   moderator );
            if ( info->start_time >= now - 30*60 && info->start_time <= now + 60*60 )
                m_meetingList->SetItemBackgroundColour( idMeeting, wxColor(200,255,200) );
        }

        // SPECIAL CASE: Track the instant meeting ID and the current user's ID
        if( fMyInstantMeeting && m_fInstantMeeting )
        {
            m_strInstantMID = meeting_id;
            m_ControllerState |= State_HaveInstantMID;
        }
    }

    wxLogDebug(wxT("   %d meetings fetched."), nRecords);

    if( fMyMeetings )
    {
        if( m_fMyMeetingsExpanded )
            m_meetingList->Expand( m_MyMeetingsId );
        if( m_fPastMeetingsExpanded )
            m_meetingList->Expand( m_PastMeetingsId );
        if( m_fUnscheduledMeetingsExpanded )
            m_meetingList->Expand( m_UnscheduledMeetingsId );
        if( m_fMyInstantMeetingExpanded && m_MyInstantMeetingId )
            m_meetingList->Expand( m_MyInstantMeetingId );

        SortMeetingList( parent_node );
    }
    else if( fPublicMeetings )
    {
        if( m_fPublicMeetingsExpanded )
            m_meetingList->Expand( m_PublicMeetingsId );

        SortMeetingList( parent_node );
    }
    else if( fInvitedMeetings )
    {
        if( m_fInvitedMeetingsExpanded )
            m_meetingList->Expand( m_InvitedMeetingsId );

        SortMeetingList( parent_node );
    }
    else if( fFoundMeetings )
    {
        MeetingSearch criteria;
        criteria.Decode( pSrch->m_strSearchCriteria );

        // Track the list in case we need to clean it up later
        pSrch->m_list = list;

        if( pSrch->m_nSearchNumber == 0 )
        {
            // Make sure the outer search node is expanded if this is the first
            // search.
            m_meetingList->Expand( m_CommunitySearchId );
        }
        if( pSrch->m_nStartingRow == 0 )
        {
            // Make sure the search results node is expanded on the first chunk
            m_meetingList->Expand( parent_node );
            m_meetingList->EnsureVisible( parent_node );

            // Should I select the node as well ??????
        }

        SortMeetingList( parent_node );

        if( !pSrch->m_fComplete )
        {
            wxString searchTitlePrefix(wxT(""));
            wxString searchDesc;
            wxString searchTitle;

            searchDesc = criteria.FormatDescription();

            if( nRecords == 0 )
            {
                // Empty result set
                pSrch->m_fComplete = true;
                searchTitlePrefix = _("No meetings found");
            }
            else
            {
                if( nRecords < criteria.m_nMaxResults )
                {
                    // We got back fewer records than we asked for,
                    // must be the end of the data
                    pSrch->m_fComplete = true;
                    searchTitlePrefix = _("Search complete");
                }
                else if( nRecords == pSrch->m_nTotalRetrieved )
                {
                    // No change in the total indicates the end of the list
                    pSrch->m_fComplete = true;
                    searchTitlePrefix = _("Search complete");
                }
                else
                {
                    searchTitlePrefix = _("Partial search");
                }
            }

            // If the search is now complete
            if( pSrch->m_fComplete )
            {
                // TODO: add specific API to remove searches
                //meeting_list_destroy(list);
                //pSrch->m_list = 0;
            }
            SetUIFeedback( 0, pSrch );

            // Update the search results title as required
            searchTitle = searchTitlePrefix;
            searchTitle += wxT(": ");
            searchTitle += searchDesc;
            m_meetingList->SetItemText( parent_node, searchTitle );
        }
        // Track the total number of records found
        pSrch->m_nTotalRetrieved = nRecords;
        pSrch->m_nStartingRow = nRecords;
    }

    // Set some state flags
    if( fMyMeetings ) m_ControllerState |= State_HaveMyMeetings;
    if( fPublicMeetings ) m_ControllerState |= State_HavePublic;
    if( fInvitedMeetings ) m_ControllerState |= State_HaveInvited;

    // Give the command queue another go in case one of the commands
    // requires the instance meeting ID
    ExecutePendingCommands();
}

void MeetingCenterController::OnMeetingFetched(wxCommandEvent& event)
{
    if( IsOpcode( event.GetString(), OP_MeetingMainFrame ) )
    {
        // This event is not for us
        event.Skip();
        return;
    }

    wxString    strEvent( event.GetString( ) );
    bool        fJoin       = ( IsOpcode( event.GetString( ), OP_FindMyPINAndJoin ) );
    bool        fInstant    = ( IsOpcode( event.GetString( ), OP_Instant ) );
    bool        fJoined     = false;

    meeting_details_t mtg = (meeting_details_t)event.GetClientData();
    invitee_iterator_t iter;
    invitee_t invt;

    iter = meeting_details_iterate(mtg);

    while ((invt = meeting_details_next(mtg, iter)) != NULL)
    {
        if( fJoin )
        {
            wxString    strScreen( invt->screenname, wxConvUTF8 );
            if( m_SignOnParams.m_strScreen.CmpNoCase( strScreen )  ==  0 )
            {
                wxString    strMID(   m_join_inv->meeting_id, wxConvUTF8 );
                wxString    strPID(   invt->participant_id,   wxConvUTF8 );
                wxString    strPName( invt->name,             wxConvUTF8 );
                wxString    strTitle( m_join_inv->title,      wxConvUTF8 );

                Commands( ).Add( wxT("join-meeting"), strPID, strPName, strMID, strTitle );
                ExecutePendingCommands( );
                meeting_invite_destroy( m_join_inv );
                m_join_inv = 0;
                fJoined = true;
                break;
            }
        }
        // If this call came from the ??
        else if( fInstant )
        {
            m_strParticipantName = wxString(invt->name,wxConvUTF8);
            break;
        }
    }

    if( fJoin  &&  !fJoined )
    {
        //  User wants to join a meeting they were not invited to
        wxString    strMID(   m_join_inv->meeting_id,       wxConvUTF8 );
        wxString    strPID(   wxT(""),                      wxConvUTF8 );
        wxString    strPName( m_SignOnParams.m_strFullName, wxConvUTF8 );
        wxString    strTitle( m_join_inv->title,            wxConvUTF8 );

        Commands( ).Add( wxT("join-meeting"), strPID, strPName, strMID, strTitle );
        ExecutePendingCommands( );
        meeting_invite_destroy( m_join_inv );
        m_join_inv = 0;
    }

    meeting_details_destroy(mtg);
}


//*****************************************************************************
void MeetingCenterController::OnDirectoryFetched(wxCommandEvent& event)
{
    wxString opid;
    opid = event.GetString();

    if( !(opid == opcode(OP_MCC_CAB) || opid == opcode(OP_MCC_PAB)) )
    {
        // Not from around here
        event.Skip();
        return;
    }

    directory_t dir = (directory_t)event.GetClientData();

    wxLogDebug(wxT("MeetingCenterController::OnDirectoryFetched( )  --  opid: %s , dir->type: %d"), event.GetString().c_str(), dir->type);

    if (dir->type == dtGlobal)
    {
        m_cab = dir;
    }
    else if( dir->type == dtPersonal )
    {
        m_pab = dir;
    }

    //  Execute any commands that were deferred pending arrival of this data
    ExecutePendingCommands( );

    // Someone else may want to know about this
    event.Skip();
}


//*****************************************************************************
bool MeetingCenterController::WaitContactsLoaded(wxWindow *waiter, bool bShowStatus)
{
    if (m_ControllerState & (State_Closing|State_Downloading))
        return false;

    m_ControllerState |= State_Downloading;

    if (waiter == NULL)
        waiter = this;

    bool fHaveAB = (GetCAB() ? true:false) && (GetPAB() ? true:false);
    if( !fHaveAB )
    {
        wxStopWatch sw;
        long start_time = sw.Time();
        long timeout_ms = 3000;         //  delay before displaying cancel dialog

        while( GetCAB() == NULL || GetPAB() == NULL )
        {
            if( (sw.Time() - start_time) > timeout_ms || bShowStatus )
            {
                //  If we are a SuperAdmin and we are waiting for a PAB to download,
                //  assume there is no PAB so just create an empty one and continue.
                if( GetPAB( ) == NULL  &&  !m_SignOnParams.m_strCommunity.IsEmpty( ) )
                {
                    wxLogDebug(wxT("in MeetingCenterController::WaitContactsLoaded( ) - user is SuperAdmin and no PAB has arrived") );
                    directory_t dirT;
                    dirT = addressbk_get_directory( book, dtPersonal, true );
                    if( dirT )
                    {
                        m_pab = dirT;
                        wxLogDebug(wxT("    in MeetingCenterController::WaitContactsLoaded( ) - PAB was created") );
                        continue;
                    }
                }
                if( m_pProgressOfContactLoad  ==  NULL )
                {
                    wxString    strTitle( _("Fetching Contacts"), wxConvUTF8 );
                    wxString    strPrompt( _("Your contacts are being downloaded from the server.\nClick Cancel to stop the download and abort "), wxConvUTF8 );
#ifndef __WXMAC__
                    strPrompt.Append( PRODNAME );
                    strPrompt.Append( wxT(".") );
#else
                    strPrompt = strPrompt.BeforeFirst( wxT('\n') );
#endif
                    int         iFlags      = wxPD_APP_MODAL | wxPD_CAN_ABORT;

                    m_pProgressOfContactLoad = new Progress1Dialog( strTitle, strPrompt, 10, waiter, iFlags );
                    m_pProgressOfContactLoad->Hide( );
                    m_pProgressOfContactLoad->Fit( );
                }
                fHaveAB = (GetCAB() ? true:false) && (GetPAB() ? true:false);
                if( !fHaveAB  &&  !m_pProgressOfContactLoad->IsShown( ) )
                {
                    m_pProgressOfContactLoad->Show( );
                    EnableWindows( false );
                }
            }

            CUtils::MsgWait( 500, 1 );
            wxGetApp().Yield();

            if (m_pProgressOfContactLoad && !m_pProgressOfContactLoad->Pulse())
            {
                m_pProgressOfContactLoad->Hide( );
                m_pProgressOfContactLoad->Destroy( );
                m_pProgressOfContactLoad = NULL;

                wxLogDebug( wxT("User cancelled contact download, closing app") );
                m_ControllerState |= State_Closing;
                Close( );       //  User clicked the Cancel button.  We warned them we would close the app it they did that!
                return false;
            }
        }
    }

    if( m_pProgressOfContactLoad )
    {
        m_pProgressOfContactLoad->Hide( );
        m_pProgressOfContactLoad->Destroy( );
        m_pProgressOfContactLoad = NULL;
        EnableWindows( true );
        waiter->SetFocus( );
    }

    m_ControllerState &= ~State_Downloading;

    fHaveAB = (GetCAB() ? true:false) && (GetPAB() ? true:false);
    return fHaveAB;
}


//*****************************************************************************
int MeetingCenterController::RefreshContacts( bool fFlush, bool fWait )
{
    if ( (m_ControllerState & (State_Closing|State_Downloading)) != 0 || m_pContactService->IsImporting() )
        return -1;

    if( fFlush )
    {
        addressbk_flush( book );
        m_cab = 0;
        m_pab = 0;
    }
    int ret = 0;

    wxString userDir = wxStandardPaths::Get().GetUserDataDir();

    if( !GetPAB() )
    {
        ret = addressbk_fetch_contacts(book, opcode(OP_MCC_PAB).mb_str(wxConvUTF8), dtPersonal );
        wxLogDebug(wxT("addressbk_fetch_contacts(book, \"pab\", dtPersonal ) = %d"), ret );
    }

    if( !GetCAB() )
    {
        if( PREFERENCES.m_fDownloadCAB)
        {
            ret = addressbk_fetch_contacts(book, opcode(OP_MCC_CAB).mb_str(wxConvUTF8), dtGlobal);
            wxLogDebug(wxT("addressbk_fetch_contacts(book, \"cab\", dtGlobal) = %d"), ret);
        }
        else
        {
            ret = addressbk_fetch_buddies(book, opcode(OP_MCC_CAB).mb_str(wxConvUTF8));
            wxLogDebug(wxT("addressbk_fetch_buddies(book, \"cab\") = %d"), ret);
        }
    }

    // Avoid re-importing contacts if meeting list is being refreshed, unless we have not imported any contacts yet.
    if ( fFlush || m_pContactService->GetDirectoryFirst() == NULL ) 
    {
        wxLogDebug(wxT("Importing contacts"));
        m_pContactService->ResetDirectories();
        if ( PREFERENCES.m_fDownloadOutlook )
        {
            m_pContactService->ImportDirectory(wxT("ms_outlook"));
        }
        m_pContactService->StartImport( );
    }

    if( fWait )
    {
        if( WaitContactsLoaded( ) )
        {
            if( m_pManageContactsFrame )
                m_pManageContactsFrame->DisplayContacts();
        }
    }

    return ret;
}


bool MeetingCenterController::ContactInformationDownloaded()
{
    if( !GetCAB( ) )
        return false;

    if( !GetPAB( ) )
    {
        return false;
    }
    return true;
}

void MeetingCenterController::OnMeetingError(MeetingError& event)
{
    wxLogDebug( wxT("MeetingCenterController::OnMeetingError - error = %d  --  MID = %s  --  PID = %s  --  Type = %s  --  Message = ***%s***"),
                event.GetError( ), event.GetMeetingId( ).wc_str( ), event.GetParticipantId( ).wc_str( ), event.GetType( ).wc_str( ), event.GetMessage( ).wc_str( ) );

    if( event.GetError( )  ==  ERR_CONNECTLOST )
        OnControllerPingFailed( );
    else
        event.Skip();
}

void MeetingCenterController::OnMeetingEvent(MeetingEvent& event)
{
    wxLogDebug(wxT("MeetingCenterController::OnMeetingEvent( %d )"), event.GetEvent());

    if ((m_ControllerState & State_Closing) == 0)
    {
        meeting_t mtg = controller_find_meeting(controller, event.GetMeetingId().mb_str(wxConvUTF8));
        if (mtg != NULL)
        {
            if( event.GetEvent() == MeetingStarted )
            {
                UpdateMeetingStatus( event.GetMeetingId(), StatusInProgress );
            }
            else if( event.GetEvent() == MeetingEnded )
            {
                UpdateMeetingStatus( event.GetMeetingId(), StatusCompleted );
            }
            else if( event.GetEvent() == MeetingReset )
            {
                wxLogDebug(wxT("MeetingCenterController::OnMeetingEvent(MeetingReset)"));
                RefreshMeetingsList();
            }
        }
    }
    event.Skip( );
}


void MeetingCenterController::OnMeetingChatEvent(MeetingChatEvent& event)
{
    event.Skip( );
}

wxString MeetingCenterController::privacyText( int privacy )
{
    wxString ret;
    switch( privacy )
    {
        case PrivacyPublic:     ret = _("Public");     break;
        case PrivacyUnlisted:   ret = _("Unlisted");   break;
        case PrivacyPrivate:    ret = _("Private");    break;
        default:                ret = _("Unknown");    break;
    }
    return ret;
};

wxString MeetingCenterController::typeText( int type )
{
    wxString ret;
    switch( type )
    {
        case TypeInstant:   ret = _("Instant");    break;
        case TypeScheduled: ret = _("Scheduled");  break;
        default:            ret = _("Unknown");    break;
    }
    return ret;
}

wxString MeetingCenterController::DateTimeDecode(int aTime, int aDateDecodeFormat)
{
    wxString ret(wxT(""));

    if( aDateDecodeFormat == ShortDate )
    {
        ret = CUtils::FormatShortDate(aTime);
    }
    else if( aDateDecodeFormat == ShortDateTime )
    {
        ret = CUtils::FormatShortDateTime(aTime);
    }
    else //if( aDateDecodeFormat == LongDateTime )
    {
        ret = CUtils::FormatLongDateTime(aTime);
    }
    return ret;
}

void MeetingCenterController::SetupCommandMask( MeetingTreeItemData *pData, SearchResultsTreeItemData *pSrch )
{
    m_nCmdMask = 0;

    wxArrayTreeItemIds  selIds;
    int                 iNumSels;
    iNumSels = m_meetingList->GetSelections( selIds );

    m_nCmdMask |= Mtg_Create;

    if( AlreadySignedOn() )
    {
        m_nCmdMask |= Mtg_Preferences;
        if( AdministratorMode() )
        {
            m_nCmdMask |= Cnt_Manage;
            if( !m_SignOnParams.m_strCommunity.IsEmpty( ) )
                m_nCmdMask |= Cnt_EditMyNOT;
        }
    }

    // If there is only one meeting associated with this data
    if( iNumSels == 1  &&  pData  &&  !pData->m_meetingId.IsEmpty() )
    {
        // If meeting is visible to us, we should be able to join it
        m_nCmdMask |= Mtg_Join;

        m_nCmdMask |= Mtg_View;

        if( pData->m_nType == Node_OwnsMeeting )
        {
                m_nCmdMask |= Mtg_Start;
                m_nCmdMask |= Mtg_Recordings;
                m_nCmdMask |= Mtg_Shared;
                m_nCmdMask |= Mtg_Edit;
                if( pData->m_meetingType != TypeInstant )
                {
                    m_nCmdMask |= Mtg_Delete;
                    m_nCmdMask |= Mtg_Copy;
                }
        }
        else if ( AdministratorMode() )
        {
            m_nCmdMask |= Mtg_Start;
            m_nCmdMask |= Mtg_Edit;
        }
        else
        {
            if( pData->m_meetingRole == RollModerator )
            {
                m_nCmdMask |= Mtg_Start;
                
                if( pData->m_options & ModeratorMayEdit )
                    m_nCmdMask |= Mtg_Edit;
            }
        }
    }
    else if( iNumSels  >  1 )
    {
        bool    fDeletable  = true;
        for( int i=0; i<iNumSels; i++ )
        {
            wxTreeItemId    selId = selIds.Item( i );
            if( selId.IsOk( ) )
            {
                CommonTreeItemData *pCommonData = (CommonTreeItemData *) m_meetingList->GetItemData( selId );
                if( pCommonData->m_nType != Node_SearchResults  &&
                      !((MeetingTreeItemData *)pCommonData)->m_meetingId.IsEmpty( ) &&
                      ((MeetingTreeItemData *)pCommonData)->m_nType == Node_OwnsMeeting &&
                      ((MeetingTreeItemData *)pCommonData)->m_meetingType != TypeInstant )
                {
                    continue;
                }
                else
                {
                    fDeletable = false;
                    break;
                }
            }
            else
            {
                fDeletable = false;
                break;
            }
        }
        if( fDeletable )
            m_nCmdMask |= Mtg_Delete;
    }
    if( pSrch )
    {
        m_nCmdMask |= Srch_Remove;
        m_nCmdMask |= Srch_Edit;
        if( !pSrch->m_fComplete )
        {
            m_nCmdMask |= Srch_Continue;
        }
    }

    if (HasState(State_WaitingForSetup))
    {
        m_nCmdMask &= ~(Mtg_Create|Mtg_Start|Mtg_Edit);
    }

}

void MeetingCenterController::SetUIFeedback( MeetingTreeItemData *pData, SearchResultsTreeItemData *pSrch )
{
    if ( !m_fHaveGUI )
        return;

    // If nothing specified, see if we can find current selection ourselves
    if (pData == NULL && pSrch == NULL)
    {
        wxTreeItemId selId;
        selId = m_meetingList->GetSelection();
        if( selId.IsOk() )
        {
            CommonTreeItemData *pCommonData = (CommonTreeItemData *)m_meetingList->GetItemData(selId); // Const cast s/b safe enough

            if( pCommonData->m_nType == Node_SearchResults )
            {
                pSrch = (SearchResultsTreeItemData *)pCommonData;
            }
            else
            {
                pData = (MeetingTreeItemData *)pCommonData;
            }
        }
    }

    SetupCommandMask( pData, pSrch );

    // Update the main menu
    m_MeetingsMenu-> Enable( Meeting_Start,   IsEnabled( Mtg_Start ) );
    m_MeetingsMenu-> Enable( Meeting_Join,    IsEnabled( Mtg_Join ) );
    m_MeetingsMenu-> Enable( Meeting_New,     IsEnabled( Mtg_Create ) );
    m_MeetingsMenu-> Enable( Meeting_View,    IsEnabled( Mtg_View ) );
    m_MeetingsMenu-> Enable( Meeting_Edit,    IsEnabled( Mtg_Edit ) );
    m_MeetingsMenu-> Enable( Meeting_Copy,    IsEnabled( Mtg_Copy ) );
    m_MeetingsMenu-> Enable( Meeting_Delete,  IsEnabled( Mtg_Delete ) );
    m_DocumentsMenu->Enable( Documents_Recordings,    IsEnabled( Mtg_Recordings ) );
    m_DocumentsMenu->Enable( Documents_Shared,        IsEnabled( Mtg_Recordings ) );
    m_OptionsMenu->  Enable( wxID_PREFERENCES,        IsEnabled( Mtg_Preferences ) );
    m_ContactsMenu-> Enable( Contacts_EditMyContact, !IsEnabled( Cnt_EditMyNOT ) );

    // Update the popup
    m_MeetingPopup->Enable( Meeting_Start, IsEnabled( Mtg_Start ) );
    m_MeetingPopup->Enable( Meeting_Join,  IsEnabled( Mtg_Join ) );
    m_MeetingPopup->Enable( Meeting_View,  IsEnabled( Mtg_View ) );
    //-----------------------------------------------------------------
    m_MeetingPopup->Enable( Meeting_Edit,  IsEnabled( Mtg_Edit ) );
    m_MeetingPopup->Enable( Meeting_Copy,  IsEnabled( Mtg_Copy ) );
    m_MeetingPopup->Enable( Meeting_Delete, IsEnabled( Mtg_Delete ) );
    //------------------------------------------------------------------
    m_MeetingPopup->Enable( Documents_Recordings, IsEnabled( Mtg_Recordings ) );
    m_MeetingPopup->Enable( Documents_Shared, IsEnabled( Mtg_Recordings ) );

    // The buttons (These s/b a toolbar ?)
 	m_ButtonStart->Enable( IsEnabled( Mtg_Start ) );
	m_ButtonJoin->Enable( IsEnabled( Mtg_Join ) );
	m_ButtonNew->Enable( IsEnabled( Mtg_Create ) );
	m_ButtonEdit->Enable( IsEnabled( Mtg_Edit ) );
	m_ButtonView->Enable( IsEnabled( Mtg_View ) );
	//m_ButtonFind

    //----------------------------------------------------------------------
    m_SearchMenu->Enable( Search_Continue, IsEnabled( Srch_Continue ) );
    m_SearchMenu->Enable( Search_Remove,   IsEnabled( Srch_Remove ) );
    m_SearchMenu->Enable( Search_Edit,     IsEnabled( Srch_Edit ) );

    //----------------------------------------------------------------------
    // Update the search results popup
    m_SearchPopup->Enable( Search_Continue, IsEnabled( Srch_Continue ) );
    m_SearchPopup->Enable( Search_Remove,   IsEnabled( Srch_Remove ) );
    m_SearchPopup->Enable( Search_Edit,     IsEnabled( Srch_Edit ) );
}

void MeetingCenterController::OnMeetingListSelectionChanged(wxTreeEvent& event)
{
    // See what's been selected
    wxTreeItemId selId;
    selId = event.GetItem();
    CommonTreeItemData *pCommonData = (CommonTreeItemData *)m_meetingList->GetItemData(selId); // Const cast s/b safe enough
    MeetingTreeItemData *pData = 0;
    SearchResultsTreeItemData *pSrch = 0;

    if( pCommonData->m_nType == Node_SearchResults )
    {
        pSrch = (SearchResultsTreeItemData *)pCommonData;
    }
    else
    {
        pData = (MeetingTreeItemData *)pCommonData;
    }
    SetUIFeedback( pData, pSrch );
}

void MeetingCenterController::OnMeetingListItemCollapsed(wxTreeEvent& event)
{
    wxTreeItemId selId;
    selId = event.GetItem();

    if( selId == m_PublicMeetingsId ) m_fPublicMeetingsExpanded = false;
    else if( selId == m_MyMeetingsId ) m_fMyMeetingsExpanded = false;
    else if( selId == m_PastMeetingsId ) m_fPastMeetingsExpanded = false;
    else if( selId == m_UnscheduledMeetingsId ) m_fUnscheduledMeetingsExpanded = false;
    else if( selId == m_MyInstantMeetingId ) m_fMyInstantMeetingExpanded = false;

}

void MeetingCenterController::OnMeetingListItemExpanded(wxTreeEvent& event)
{
    wxTreeItemId selId;
    selId = event.GetItem();

    if( selId == m_PublicMeetingsId ) m_fPublicMeetingsExpanded = true;
    else if( selId == m_MyMeetingsId ) m_fMyMeetingsExpanded = true;
    else if( selId == m_PastMeetingsId ) m_fPastMeetingsExpanded = true;
    else if( selId == m_UnscheduledMeetingsId ) m_fUnscheduledMeetingsExpanded = true;
    else if( selId == m_MyInstantMeetingId ) m_fMyInstantMeetingExpanded = true;
}

void MeetingCenterController::OnMeetingListItemMenu(wxTreeEvent& event)
{
    // See what's been selected on the meeting list
    wxTreeItemId selId;
    selId = event.GetItem();
    MeetingTreeItemData *pData = (MeetingTreeItemData *)m_meetingList->GetItemData(selId); // Const cast s/b safe enough
    if( pData->m_meetingId.IsEmpty() )
        return; // No meeting ID == No popup menu

    // Show the popup menu
    wxPoint menuPos;
    menuPos = event.GetPoint();
    menuPos = m_meetingList->ClientToScreen(menuPos);
    menuPos = ScreenToClient(menuPos);
    PopupMenu( m_MeetingPopup, menuPos );
}

void MeetingCenterController::FetchMeetingDetails( int nOpcode, const wxString & meetingId )
{
    schedule_find_meeting_details(sched, opcode(nOpcode).mb_str(wxConvUTF8), meetingId.mb_str(wxConvUTF8) );
}

void MeetingCenterController::OnSize( wxSizeEvent& event )
{
    m_lastSize = GetSize();
    m_lastPosition = GetPosition();

    event.Skip();
}

void MeetingCenterController::OnMeetingListItemRightClick(wxTreeEvent& event)
{
    // See what's been selected on the meeting list
    wxTreeItemId selId;
    selId = event.GetItem();
    CommonTreeItemData *pCommonData = (CommonTreeItemData *)m_meetingList->GetItemData(selId);
    if( pCommonData->m_nType == Node_Root )
        return;

    wxPoint menuPos;
    menuPos = event.GetPoint();
    menuPos = m_meetingList->ClientToScreen(menuPos);
    menuPos = ScreenToClient(menuPos);

    if( pCommonData->m_nType != Node_SearchResults )
    {
        MeetingTreeItemData *pData = (MeetingTreeItemData *)pCommonData;
        if( pData->m_meetingId.IsEmpty() )
            return; // No meeting ID == No popup menu

        // Show the meeting popup menu
        PopupMenu( m_MeetingPopup, menuPos );
    }

    if( pCommonData->m_nType == Node_SearchResults )
    {
        // Show the search popup menu
        PopupMenu( m_SearchPopup, menuPos );
    }
}

void MeetingCenterController::OnScheduleProgress(wxCommandEvent& event)
{
    wxString opid = event.GetString();
    int num_fetched = event.GetInt();
    //int total = event.GetExtraLong();

    // NOTE: At present (03/28/07) the total value is Chunk Size + 1.
    // The Jabber query is not setup to retrieve all of the records ? Ask Mike T about this (PLF)

    wxString status;
    if( opid == opcode(OP_MCC_Public) )
    {
        status = wxString::Format(_("%d public meetings received"),num_fetched);
    }
    else if( opid == opcode(OP_MCC_Scheduled) )
    {
        status = wxString::Format(_("%d scheduled meetings received"),num_fetched);
    }
    else if( opid == opcode(OP_MCC_Invited) )
    {
        status = wxString::Format(_("%d meeting invitations received"),num_fetched);
    }
    showStatus( status );

    event.Skip();
}

void MeetingCenterController::OnScheduleUpdated(wxCommandEvent& event)
{
    wxString opid = event.GetString();
    int status = event.GetInt();

    wxLogDebug( wxT("MeetingCenterController::OnScheduleUpdated: opid=%s, status=%d"), opid.c_str(), status );

    event.Skip();
}

void MeetingCenterController::OnScheduleError(wxCommandEvent& event)
{
    wxLogDebug(wxT("MeetingCenterController::OnScheduleError"));

    wxString str;
    str = event.GetString();
    int nd = str.Find('|');
    wxString opid;
    wxString msg;
    if( nd >= 0 )
    {
        opid = str.Left(nd);
        msg =  str.Mid(nd+1);
    }
    else
    {
        opid = wxT("");
        msg = str;
    }
    wxLogDebug( wxT("Schedule error: Code=%d, %s %s"),
        event.GetInt(),
        opid.c_str(),
        msg.c_str());

    // ToDo: show error

    event.Skip();
}

void MeetingCenterController::OnAddressbkUpdateFailed(wxCommandEvent& event)
{
    wxString str;
    str = event.GetString();
    int nd = str.Find('|');
    wxString opid;
    wxString msg;
    if( nd >= 0 )
    {
        opid = str.Left(nd);
        msg =  str.Mid(nd+1);
    }
    else
    {
        opid = wxT("");
        msg = str;
    }
    wxLogDebug(wxT("Address Book Update Failed: Code=%d, %s %s"),
        event.GetInt(),
        opid.c_str(),
        msg.c_str());

    event.Skip();
}

void MeetingCenterController::OnAddressUpdated(wxCommandEvent& event)
{
    wxString str;
    wxString opid = event.GetString();

    if( IsOpcode( opid, OP_EditMyContact, false ) )
    {
        m_ControllerState &= ~State_WaitingForUpdate;
    }

    event.Skip();
}

void MeetingCenterController::OnAddressDeleted(wxCommandEvent& event)
{
    wxString str;
    str = event.GetString();
    int nd = str.Find('|');
    wxString opid;
    wxString itemid;
    if( nd >= 0 )
    {
        opid = str.Left(nd);
        itemid =  str.Mid(nd+1);
    }
    else
    {
        opid = wxT("");
        itemid = str;
    }
    wxLogDebug(wxT("Address Book Deleted: opid=%s, itemid=%s"),
        opid.c_str(), itemid.c_str() );

    event.Skip();
}

void MeetingCenterController::OnAddressVersionCheck(wxCommandEvent& event)
{
    int         iType;
    iType = event.GetInt( );
    wxString    strURL;
    strURL = event.GetString( );
    strURL.Replace( wxT("\n"), wxT("") );
    strURL.Replace( wxT("\r"), wxT("") );

#if defined(LINUX)
    if (sizeof(void*) == 8)
        strURL.Replace( wxT(".exe"), wxT("-64bit.rpm") );
    else
        strURL.Replace( wxT(".exe"), wxT(".rpm") );
#elif defined(MACOSX)
    strURL.Replace( wxT(".exe"), wxT(".dmg") );
#endif

    wxLogDebug( wxT("Address Book Version Check: upgrade_type = %d,   URL = %s"), iType, strURL.c_str( ) );

    bool        fRet;
    fRet = IsUpdateRequired( strURL, iType );
    if( fRet )
    {
        m_ControllerState = State_Closing;
        if (m_pConnectionProgressDialog)
            m_pConnectionProgressDialog->Terminate( );
        OnExit( event );
        return;
    }

    // Version is ok, continue with sign on sequence.

    if ( !m_SignOnParams.m_fAnonymousUser )
    {
        addressbk_authenticate(book, m_SignOnParams.m_strScreen.mb_str(wxConvUTF8), m_SignOnParams.m_strCommunity.mb_str(wxConvUTF8) );
    }
    else if ( m_SignOnParams.m_fTestConnection )
    {
        if (m_pConnectionProgressDialog) 
        {
            wxString info = _("Your computer is able to connect to the meeting server and the meeting software is up to date.\n\n"
                              "Follow the invitation instructions at meeting time to join the meeting.");
            if (strcmp(session_get_option(session, "usinghttp"),"yes") == 0)
            {
                info.append( _("\n\nHTTP mode was required to complete this connection.  This may reduce performance during the meeting.") );
            }
            m_pConnectionProgressDialog->SetComplete( info );
            return;
        }
    }
    else
    {
        m_ControllerState |= State_AnonymousMode;
        controller_register_anonymous(controller, m_SignOnParams.m_strMeetingPIN.mb_str(wxConvUTF8),
                    m_SignOnParams.m_strFullName.mb_str(wxConvUTF8));

        // Send presence to controller now so unavailable presence is sent later when we disconnect
        char controller_address[512];
        session_get_service_jid(session, "controller", controller_address);
        session_send_presence(session, controller_address, PRES_AVAILABLE, NULL);
    }

    if( m_ControllerState & State_WaitingForReconnect )
    {
        // ToDo: too soon?
        wxCommandEvent  event( wxEVT_UPDATE_RECONNECT_STATUS, wxID_ANY );
        event.SetInt( ERR_SUCCESS );
        m_pReconnectDialog->AddPendingEvent( event );
    }

    if( !m_PingTimer.IsRunning( ) )
    {
        m_iPingsSent     = 0;
        m_iPingsReceived = 0;
        wxLogDebug( wxT("in MeetingCenterController::OnConnectEvent( ) -- starting Ping timer") );
        m_PingTimer.Start( kPingInterval, wxTIMER_CONTINUOUS );
    }
}

void MeetingCenterController::OnProfileFetched(wxCommandEvent& event)
{
    wxString opid = event.GetString();

    wxLogDebug( wxT("MeetingCenterController::OnProfileFetched: opid=%s"), opid.c_str() );

    event.Skip();
}

void MeetingCenterController::OnPresenceEvent(PresenceEvent& event)
{
    event.Skip();
}

void MeetingCenterController::OnPreferences( wxCommandEvent& event )
{
    UserPreferencesDialog Dlg( this, wxID_ANY );

    bool oldIMClient            = PREFERENCES.m_fIMClient;
    wxFont  oldFont             = PREFERENCES.m_AppFont;
    int nResp = Dlg.EditPreferences( PREFERENCES );
    if( nResp == wxID_OK )
    {
        PREFERENCES.Save(); 

        // NOTE: Because these fonts haven't been created yet, the
        // operator != and operator == interface cannot be used to check for
        // changes.
        if( oldFont.GetFaceName() != PREFERENCES.m_AppFont.GetFaceName() ||
            oldFont.GetPointSize() != PREFERENCES.m_AppFont.GetPointSize() )
        {
            // Set the app font
            wxLogDebug(wxT("Setting the new font"));

            wxGetApp().SetAppDefFont( PREFERENCES.m_AppFont );

            // Inform user client must be restarted
            wxString caption;
            wxString msg;
            caption = _("Meeting Center");
            msg = _("The font changes that you requested will be used the next time you restart this program.");

            wxMessageDialog MsgDlg(this, msg, caption, wxOK | wxICON_INFORMATION);
            MsgDlg.ShowModal();
        }
        else if ( oldIMClient != PREFERENCES.m_fIMClient )
        {
            wxString caption;
            wxString msg;
            caption = _("Meeting Center");
            if ( PREFERENCES.m_fIMClient )
                msg = _("Conferencing IM will be enabled the next time you restart this program.");
            else
                msg = _("Conferencing IM will be disabled the next time you restart this program.");

            wxMessageDialog MsgDlg(this, msg, caption, wxOK | wxICON_INFORMATION);
            MsgDlg.ShowModal();
        }
        
        if( m_fInstantMeeting != PREFERENCES.m_fShowInstantMeeting )
        {
            m_fInstantMeeting = PREFERENCES.m_fShowInstantMeeting;
            RefreshMeetingsList();
        }

        if( Dlg.ContactRefreshNeeded( ) )
            RefreshContacts( );
    }
}

void MeetingCenterController::GetControllerUsername(wxString &username)
{
    wxString rtn_string(controller_get_username(controller), wxConvUTF8);
    username = rtn_string;
}

MeetingTreeItemData * MeetingCenterController::LookupMeetingId( const wxString & mid )
{
    MeetingTreeItemData *pData = 0;

    if( m_ControllerState & State_HaveInstantMID )
    {
        pData = LookupMeetingId( m_MyInstantMeetingId, mid );
        if( pData )
        {
            return pData;
        }
    }

    if( m_ControllerState & State_HaveMyMeetings )
    {
        pData = LookupMeetingId( m_MyMeetingsId, mid );
        if( pData )
            return pData;
        pData = LookupMeetingId( m_PastMeetingsId, mid );
        if( pData )
            return pData;
        pData = LookupMeetingId( m_UnscheduledMeetingsId, mid );
        if( pData )
            return pData;
    }

    if( m_ControllerState & State_HaveInvited )
    {
        pData = LookupMeetingId( m_InvitedMeetingsId, mid );
        if( pData )
        {
            return pData;
        }
    }

    if( m_ControllerState & State_HavePublic )
    {
        pData = LookupMeetingId( m_PublicMeetingsId, mid );
        if( pData )
        {
            return pData;
        }
    }

    return 0;
}

MeetingTreeItemData * MeetingCenterController::LookupMeetingId( const wxTreeItemId & node, const wxString & mid )
{
    MeetingTreeItemData *pRet = 0;
    wxTreeItemIdValue cookie;

    for ( wxTreeItemId item = m_meetingList->GetFirstChild(node, cookie);
      item.IsOk();
      item = m_meetingList->GetNextChild(node, cookie) )
    {
        MeetingTreeItemData *pData = (MeetingTreeItemData *)m_meetingList->GetItemData(item); // Const cast s/b safe enough
        if( pData->m_meetingId == mid )
        {
            pRet = pData;
            break;
        }
    }
    return pRet;
}

MeetingTreeItemData * MeetingCenterController::LookupMeetingPIN( const wxString & pin )
{
    MeetingTreeItemData *pData = 0;

    if( m_ControllerState & State_HaveInstantMID )
    {
        pData = LookupMeetingPIN( m_MyInstantMeetingId, pin );
        if( pData )
        {
            return pData;
        }
    }

    if( m_ControllerState & State_HaveMyMeetings )
    {
        pData = LookupMeetingPIN( m_MyMeetingsId, pin );
        if( pData )
            return pData;
        pData = LookupMeetingPIN( m_PastMeetingsId, pin );
        if( pData )
            return pData;
        pData = LookupMeetingPIN( m_UnscheduledMeetingsId, pin );
        if( pData )
            return pData;
    }

    if( m_ControllerState & State_HaveInvited )
    {
        pData = LookupMeetingPIN( m_InvitedMeetingsId, pin );
        if( pData )
        {
            return pData;
        }
    }
    return 0;
}

MeetingTreeItemData * MeetingCenterController::LookupMeetingPIN( const wxTreeItemId & node, const wxString & pin )
{
    MeetingTreeItemData *pRet = 0;
    wxTreeItemIdValue cookie;

    for ( wxTreeItemId item = m_meetingList->GetFirstChild(node, cookie);
      item.IsOk();
      item = m_meetingList->GetNextChild(node, cookie) )
    {
        MeetingTreeItemData *pData = (MeetingTreeItemData *)m_meetingList->GetItemData(item); // Const cast s/b safe enough
        if( pData->m_participantId == pin )
        {
            pRet = pData;
            break;
        }
    }
    return pRet;
}

bool MeetingCenterController::UpdateMeetingStatus( const wxString & mid, int newStatus )
{
    bool fOK = false;
    MeetingTreeItemData * pID = LookupMeetingId( mid );
    if( pID )
    {
        int iImage = Meeting_Img_Unknown;
        if( newStatus == StatusInProgress )
            iImage = Meeting_Img_Online;
        else
            iImage = Meeting_Img_Offline;

        m_meetingList->SetItemImage( pID->GetId(), iImage, wxTreeItemIcon_Normal );
        m_meetingList->SetItemImage( pID->GetId(), iImage, wxTreeItemIcon_Selected );

        wxLogDebug(wxT("UpdateMeetingStatus for mid=%s, status=%d, iImage=%d"), mid.c_str(), newStatus, iImage );
        fOK = true;
    }
    else
    {
        wxLogDebug(wxT("UpdateMeetingStatus for mid=%s, status=%d, mid not found!"), mid.c_str(), newStatus );
    }
    return fOK;
}

void MeetingCenterController::StartMeetingSearch( const MeetingSearch & criteria )
{
    wxLogDebug(wxT("MeetingCenterController::StartMeetingSearch"));

    wxString encodedCriteria;
    criteria.Encode( encodedCriteria );
    bool fFoundMatchingSearch = false;
    wxTreeItemId parent;
    int nSearchNumber;

    // Look for an existing search mith matching criteria:
    // If one is found then that node is repopulated, a new search results
    // node is created otherwise.
    wxTreeItemIdValue cookie;
    for ( wxTreeItemId item = m_meetingList->GetFirstChild(m_CommunitySearchId, cookie);
      item.IsOk();
      item = m_meetingList->GetNextChild(m_CommunitySearchId, cookie) )
    {
        SearchResultsTreeItemData *pData = (SearchResultsTreeItemData *)m_meetingList->GetItemData(item); // Const cast s/b safe enough
        if( pData->m_strSearchCriteria == encodedCriteria )
        {
            fFoundMatchingSearch = true;
            parent = item;
            m_meetingList->DeleteChildren( parent );
            nSearchNumber = pData->m_nSearchNumber;
            wxLogDebug(wxT("   Found existing search results node: Search Number = %d"), nSearchNumber );
            break;
        }
    }

    if( !fFoundMatchingSearch )
    {
        SearchResultsTreeItemData *pItemData = new SearchResultsTreeItemData( Node_SearchResults );
        pItemData->m_strSearchCriteriaName = criteria.m_strSearchName;
        pItemData->m_strSearchCriteria = encodedCriteria;
        nSearchNumber = pItemData->m_nSearchNumber = m_nSearchNumber++;

        parent = m_meetingList->AppendItem( m_CommunitySearchId, _("Searching..."), -1, -1, pItemData );
        wxLogDebug(wxT("   Creating new search results node: Search Number = %d"), nSearchNumber );
    }

    StartMeetingSearch( parent );
}

void MeetingCenterController::StartMeetingSearch( wxTreeItemId parent )
{
    SearchResultsTreeItemData *pSrch = GetSearchNode( parent );
    if( !pSrch )
        return;

    MeetingSearch criteria;
    criteria.Decode( pSrch->m_strSearchCriteria );

    // Begin the search
    //////////////////////////////////////////////////////////////////////////////
    int start_date = MIN_DATE;
    int end_date = MAX_DATE;
    if( criteria.m_nDateRange == MeetingSearch::WithinNDays )
    {
        if( criteria.m_fNextNDays )
        {
            start_date = time(NULL);
            end_date = start_date + (criteria.m_nDays * 24 * 60 * 60);
        }
        else
        {
            end_date = time(NULL);
            start_date = end_date - (criteria.m_nDays * 24 * 60 * 60);
        }
    }
    else if( criteria.m_nDateRange == MeetingSearch::BetweenDates )
    {
        start_date = criteria.m_nStartDate;
        end_date = criteria.m_nEndDate;
    }

    wxLogDebug(wxT("Searching: SearchNumber=%d, StartingRow=%d"),pSrch->m_nSearchNumber,pSrch->m_nStartingRow);
    wxLogDebug(wxT("   Title = |%s|, len=%d"),criteria.m_strTitle.c_str(),criteria.m_strTitle.length() );

// NOTE: Using ( bool ) ? wxString.mb_str() : NULL caused compiler errors due to
// ? operand mismatch. That's why the following NO_SEARCH define is used. (PLF)
#define NO_SEARCH ((char *)0)
    wxCharBuffer meeting_id = criteria.m_strMeetingId.mb_str(wxConvUTF8);;
    wxCharBuffer title = criteria.m_strTitle.mb_str(wxConvUTF8);
    wxCharBuffer first = criteria.m_strHostFirst.mb_str(wxConvUTF8);
    wxCharBuffer last = criteria.m_strHostLast.mb_str(wxConvUTF8);

    int err = schedule_search_meetings(
    sched,
    opcode( OP_MCC_Search, pSrch->m_nSearchNumber ).mb_str(wxConvUTF8),
    ( criteria.m_fMeetingId ) ?     meeting_id : NO_SEARCH,
    ( criteria.m_fTitle ) ?         title : NO_SEARCH,
    ( criteria.m_fHostFirstName ) ? first : NO_SEARCH,
    ( criteria.m_fHostLastName ) ?  last : NO_SEARCH,
    criteria.m_fOnlyInProgress ? TRUE:FALSE,
    start_date,
    end_date,
    pSrch->m_nStartingRow,
    criteria.m_nMaxResults );
    if( err != 0 )
    {
        wxString err_str;
        switch( err )
        {
        case ERR_RPCFAILED: err_str = _("RPC Failed"); break;
        default:            err_str = wxString::Format(wxT("Logic error: schedule_search_meetings returned %d"),err);
        }
        wxString msg;
        wxString caption;
        caption = _("Meeting Search");
        msg = wxString::Format(_("Unable to start the search:\n%s"), err_str.c_str() );
        ::wxMessageBox( msg, caption, wxOK, this );
    }
}

void MeetingCenterController::OnSearchContinue( wxCommandEvent& event )
{
    StartMeetingSearch( m_meetingList->GetSelection() );
}

void MeetingCenterController::OnSearchEdit( wxCommandEvent& event )
{
    SearchResultsTreeItemData *pSrch = GetSearchNode();
    if( !pSrch )
        return;

    wxTreeItemId searchNode = m_meetingList->GetSelection();

    // Function n/a for now
    FindMeetingsDialog Dlg( this, wxID_ANY );

    MeetingSearch criteria;
    criteria.Decode( pSrch->m_strSearchCriteria );

    Dlg.PresetSearchCriteria( pSrch->m_strSearchCriteriaName, criteria );

    int nResp = Dlg.EditSearchCriteria();

    if( nResp == wxID_OK )
    {
        // Clear the slate prior to starting the search again
        if( pSrch->m_list )
        {
            //meeting_list_destroy(pSrch->m_list);
            //pSrch->m_list = 0;
        }
        m_meetingList->DeleteChildren( searchNode );

        // Update the search criteria that's attached to the node
        MeetingSearch criteria;
        criteria = Dlg.GetCriteria();
        pSrch->m_strSearchCriteriaName = criteria.m_strSearchName;
        criteria.Encode( pSrch->m_strSearchCriteria );

        // Reset the results state
        pSrch->m_nStartingRow = 0;
        pSrch->m_nTotalRetrieved = 0;
        pSrch->m_fComplete = false;

        // Use a different search number
        pSrch->m_nSearchNumber = m_nSearchNumber++;

        SetUIFeedback( 0, pSrch );

        // Do it again
        StartMeetingSearch( searchNode );
    }
}

void MeetingCenterController::OnSearchRemove( wxCommandEvent& event )
{
    SearchResultsTreeItemData *pSrch = GetSearchNode();
    if( !pSrch )
        return;

    wxTreeItemId searchNode = m_meetingList->GetSelection();

    if( pSrch->m_list )
    {
        //meeting_list_destroy(pSrch->m_list);
        //pSrch->m_list = 0;
    }

    // Remove the children first to avoid EVT_TREE_DELETE_ITEM events
    // that we don't care about
    m_meetingList->DeleteChildren( searchNode );

    // Now remove the node
    m_meetingList->Delete( searchNode );

}

SearchResultsTreeItemData * MeetingCenterController::GetSearchNode()
{
    SearchResultsTreeItemData * ret = 0;
    wxTreeItemId selId;
    selId = m_meetingList->GetSelection();
    if( !selId.IsOk() )
    {
        return ret; // No selection!
    }
    ret = GetSearchNode( selId );
    return ret;
}

SearchResultsTreeItemData * MeetingCenterController::GetSearchNode( wxTreeItemId & parent_node )
{
    CommonTreeItemData *pCommonData = (CommonTreeItemData *)m_meetingList->GetItemData( parent_node );
    SearchResultsTreeItemData * ret = 0;
    if( pCommonData->m_nType == Node_SearchResults )
    {
        ret = (SearchResultsTreeItemData *)pCommonData;
    }
    return ret;
}

void MeetingCenterController::OnMeetingListKeyDown(wxTreeEvent& event)
{
    int keyPressed = event.GetKeyEvent().GetKeyCode();
    if( keyPressed == WXK_DELETE )
    {
        if( (m_nCmdMask & Mtg_Delete) != 0 )
        {
            deleteSelectedMeeting();
        }
        else
        {
            ::wxBell();
        }
    }
    else
    {
        event.Skip();
    }
}

void MeetingCenterController::OnMeetingListColumnLeftClick(wxListEvent& event)
{
    if( m_ControllerState & State_Sorting )
        return; // Not now! Prevent recursive sorts

    m_ControllerState |= State_Sorting;

    long new_sort_col = event.GetColumn();
    wxLogDebug(wxT("MeetingCenterController::OnMeetingListColumnLeftClick( col %d )"),new_sort_col);

    long current_sort_col = m_meetingList->GetSortColumn();

    if( new_sort_col == current_sort_col )
    {
        m_meetingList->ReverseSortOrder();
    }
    else
    {
        m_meetingList->SetSortColumn( new_sort_col );
    }
    SortMeetingList();

    m_ControllerState &= ~State_Sorting;
}

void MeetingCenterController::SortMeetingList( const wxTreeItemId & item )
{
    if( m_ControllerState & State_Sorting )
        return; // Not now! Prevent recursive sorts

    m_ControllerState |= State_Sorting;

    m_meetingList->SortChildren( item );

    m_ControllerState &= ~State_Sorting;
}

void MeetingCenterController::SortMeetingList()
{
    wxLogDebug(wxT("SortMeetingList - Start"));

    if( m_ControllerState & State_HavePublic )
    {
        m_meetingList->SortChildren( m_PublicMeetingsId );
    }
    if( m_ControllerState & State_HaveMyMeetings )
    {
        m_meetingList->SortChildren( m_MyMeetingsId );
        m_meetingList->SortChildren( m_PastMeetingsId );
        m_meetingList->SortChildren( m_UnscheduledMeetingsId );
    }
    if( m_ControllerState & State_HaveInvited )
    {
        m_meetingList->SortChildren( m_InvitedMeetingsId );
    }

    wxTreeItemIdValue cookie;
    for ( wxTreeItemId item = m_meetingList->GetFirstChild(m_CommunitySearchId, cookie);
      item.IsOk();
      item = m_meetingList->GetNextChild(m_CommunitySearchId, cookie) )
    {
        m_meetingList->SortChildren( item );
    }

    wxLogDebug(wxT("SortMeetingList - End"));
}


MeetingListComparator::MeetingListComparator( MeetingCenterController *pController, wxTreeListCtrl * pTree )
:wxTreeListCtrlComparator(pTree),
m_pController(pController)
{
}

int MeetingListComparator::CompareItems(const wxTreeItemId& item1,const wxTreeItemId& item2)
{
    int cmp = 0;
    long sort_col = GetTree()->GetSortColumn();

    if( sort_col != wxNOT_FOUND )
    {
        if( sort_col == MeetingCenterController::Col_DateTime )
        {
            cmp = 0;
            MeetingTreeItemData *d1 = Controller()->GetMeetingItemData(item1);
            MeetingTreeItemData *d2 = Controller()->GetMeetingItemData(item2);
            if( d1 && d2 )
            {
                cmp = d1->m_start_time - d2->m_start_time;
            }
        }
        else
        {
            cmp = GetTree()->GetItemText(item1,sort_col).CmpNoCase( GetTree()->GetItemText(item2,sort_col) );
        }
    }
    else
    {
        cmp = GetTree()->GetItemText(item1).CmpNoCase( GetTree()->GetItemText(item2) );
    }

    wxTreeListColumnInfo& info = GetTree()->GetColumn(sort_col);
    if( !info.GetSortAscending() )
    {
        cmp = -cmp;
    }
    return cmp;
}

void MeetingCenterController::OnMeetingCenter( wxCommandEvent& event )
{
    ShowMeetingCenterWindow();
}

void MeetingCenterController::OnContactsManage( wxCommandEvent& event )
{
    Commands().Add( wxT("manage-contacts") );
    ExecutePendingCommands();
}

void MeetingCenterController::ContactsManage( )
{
    if( !m_pManageContactsFrame )
    {
        m_pManageContactsFrame = new ManageContactsFrame( this, &m_pManageContactsFrame );
    }
    if( m_pManageContactsFrame )
    {
        if (m_pManageContactsFrame->IsIconized())
            m_pManageContactsFrame->Iconize(false);
        m_pManageContactsFrame->Raise();

        m_pManageContactsFrame->DisplayContacts();
    }
}

void MeetingCenterController::OnContactsFetch( wxCommandEvent& event )
{
    RefreshContacts( );
}

void MeetingCenterController::OnContents( wxCommandEvent& event )
{
    wxString help( wxGetApp().Options().GetSystem( "HelpPath", "" ),  wxConvUTF8);
    help += LOCALE->GetCanonicalName( );
    help += wxT("/");
    help += wxT("index.html");
    wxLaunchDefaultBrowser( help );
}

void MeetingCenterController::OnUploadLogs( wxCommandEvent& event )
{
    LogUpload log;
    log.UploadLogs(this);
}

void MeetingCenterController::OnProfilePhoto( wxCommandEvent& event )
{
    ImageUploadDialog dlg(this, _("Upload profile picture"), FileManager::GetPathForUser(m_SignOnParams.m_strScreen), wxT("profilePicture.jpg"), true);
    if (dlg.ShowDialog())
        FetchProfileImage();
}

void MeetingCenterController::OnDebug( wxCommandEvent& event )
{
    wxGetApp().OpenDebugWindow();
}

//*****************************************************************************
void MeetingCenterController::OnAbout( wxCommandEvent& event )
{
#if 0
    // Force a crash for testing.
    int *i = reinterpret_cast<int*>(0x45);
    *i = 5;  // crash!
#endif

	AboutDialog about(this);
	about.ShowModal();
}

wxString MeetingCenterController::opcode( int nOpcode, int nSubOp )
{
    wxString ret;

    if( nSubOp >= 0 )
    {
        ret = wxString::Format(wxT("%d:%d"),nOpcode, nSubOp);
    }
    else
    {
        time_t ticks = wxDateTime::Now().GetTicks();
        ret = wxString::Format(wxT("%d:%u"),nOpcode, ticks);
    }
    return ret;
}

void MeetingCenterController::OnContactsEditMyContact( wxCommandEvent& event )
{
    int err;

    if( !WaitContactsLoaded( ) )
        return;

    m_nDatabaseError = err = 0;
    m_strDatabaseError = wxT("");

    auth_info_t auth = addressbk_get_auth_info(book);

    wxLogDebug(wxT("MeetingCenterController::OnContactsEditMyContact"));
    contact_t current_user = directory_find_contact(m_cab,auth->user_id);
    if( !current_user )
        return;

    wxString strPassword( wxT("") );
    directory_t dir;
    dir = GetCAB();
    strPassword = m_SignOnParams.m_strPassword;

    CreateContactDialog Dlg( this, wxID_ANY );
    bool fChanged = Dlg.EditNewContact( dir, 0, current_user, strPassword, true );
    if( !fChanged )
        return;

    /// Save the contact information
    wxString strOperation;
    strOperation = wxT("Update a contact");

    m_ControllerState |= State_WaitingForUpdate;

    wxString strOpid;
    strOpid = MeetingCenterController::opcode( OP_EditMyContact, -1 );
    wxCharBuffer opid = strOpid.mb_str();
    wxCharBuffer pwd = strPassword.mb_str();

    err = addressbk_contact_update(AddressBook(), opid, dir->type, current_user, pwd);

    wxLogDebug(wxT("MeetingCenterController::OnContactsEditMyContact addressbk_contact_update returned: %d"),err);
    if( err != 0 )
    {
        m_ControllerState &= ~State_WaitingForUpdate;
        m_strDatabaseError = wxString::Format(_("Unable to update your contact information.\nError: %d"),err);
        m_nDatabaseError = err;
    }

    // Wait for it.........
    wxStopWatch sw;
    long start_time = sw.Time();
    int nSeconds = 20;
    long timeout_ms = (nSeconds * 1000);
    while( m_ControllerState & State_WaitingForUpdate )
    {
        CUtils::MsgWait( 500, 1 );
        wxGetApp().Yield();

        if( (sw.Time() - start_time) > timeout_ms )
        {
            m_ControllerState &= ~State_WaitingForUpdate;
            m_strDatabaseError = _("Timeout while waiting for your contact update.");
            m_nDatabaseError = err = -1;
            break;
        }
    }
    wxString msg;
    wxString caption;
    caption = _("Manage Contacts");

    if( m_nDatabaseError )
    {
        msg = wxString::Format( _("Your contact information could not be updated.\nError: %s (%d)"), m_strDatabaseError.c_str(), m_nDatabaseError );
    }
    else
    {
        msg = _("Your contact information has been updated.");
    }
    ::wxMessageBox( msg, caption, wxOK, this );
}

bool MeetingCenterController::IsTelephonyAvailable()
{
    return (controller_is_telephony_available(controller) & TelephonyAvailable) != 0;
}

bool MeetingCenterController::IsVOIPAvailable()
{
    return (controller_is_telephony_available(controller) & VOIPAvailable) != 0;
}

bool MeetingCenterController::IsVOIPEncrypted()
{
    return (controller_is_telephony_available(controller) & VOIPEncrypted) != 0;
}

void MeetingCenterController::SetWaitingForSetup()
{
    m_ControllerState |= State_WaitingForSetup;
    SetUIFeedback();    
}

void MeetingCenterController::ClearWaitingForSetup()
{
    if (m_ControllerState & State_WaitingForSetup)
    {
        m_ControllerState &= ~State_WaitingForSetup;
        SetUIFeedback();
    }    
}

void MeetingCenterController::RegisterMeetingSetup(MeetingSetupDialog* pDlg)
{
    SetWaitingForSetup();
    m_pMeetingSetupDialog = pDlg;
}

void MeetingCenterController::UnregisterMeetingSetup(MeetingSetupDialog* pDlg)
{
    ClearWaitingForSetup();
    m_pMeetingSetupDialog = NULL;
}

MeetingSetupDialog* MeetingCenterController::GetMeetingSetup()
{
    return m_pMeetingSetupDialog;
}

void MeetingCenterController::UpdatePresence( const wxString system, const wxString screen, const wxString presence, const wxString status )
{
    // N.B.:  this function is for presence from external sources (e.g. pidgin), so ignore these status updates
    // if we are using the internal IM client.
    if (IMClientMode() && system == wxT("zon"))
        return;

    int pres = PresenceInfo::PresenceUnavailable;
    int special = PresenceInfo::SpecialNone;

    if (presence == _T("reset"))
    {
        m_PresenceManager.ResetPresence();
    }
    else
    {
        if (presence == _T("available"))
            pres = PresenceInfo::PresenceAvailable;
        else if (presence == _T("away"))
            pres = PresenceInfo::PresenceAway;
        else if (presence == _T("unavailable"))
            pres = PresenceInfo::PresenceUnavailable;

        if (status.Left(4).Cmp(wxT("iic:")) == 0)
        {
            if (status.Find(wxT("voice")) != wxNOT_FOUND)
            {
                special |= PresenceInfo::SpecialPhone;
            }
            if (status.Find(wxT("meeting")) != wxNOT_FOUND)
            {
                special |= PresenceInfo::SpecialMeeting;
            }
        }

        m_PresenceManager.SetPresence(system.wc_str(), screen.wc_str(), pres, status.wc_str(), special);

        wxLogDebug( wxT("Set presence for %s - %s %d %d"), screen.wc_str(), presence.wc_str(), pres, special);
    }
}

void MeetingCenterController::PresenceUpdated( const PresenceInfo& info )
{
    PresenceEvent event(wxEVT_PRESENCE_EVENT, info.GetSystem(),       info.GetScreen(),       info.GetPresence(), info.GetStatus());
    AddPendingEvent(event);
}

void MeetingCenterController::OnMeetingListColClick( wxListEvent& event )
{
    long new_sort_col = event.GetColumn();
//    wxLogDebug(wxT("MeetingCenterController::OnMeetingListColClick( col %d )"), new_sort_col );

    long current_sort_col = m_meetingList->GetSortColumn( );

    if( new_sort_col == current_sort_col )
    {
        m_meetingList->ReverseSortOrder( );
    }
    else
    {
        m_meetingList->SetSortColumn( new_sort_col );
    }

    if( new_sort_col  ==  0 )
    {
        m_meetingList->SortChildren( m_RootId );
    }

    wxTreeItemIdValue   cookie;
    for( wxTreeItemId child = m_meetingList->GetFirstChild( m_RootId, cookie );
          child.IsOk();
          child = m_meetingList->GetNextChild( m_RootId, cookie ) )
    {
        m_meetingList->SortChildren( child );
    }
}


//*****************************************************************************
void MeetingCenterController::OnExecutePingTimer( wxTimerEvent& event )
{
//    wxLogDebug( wxT("MeetingCenterController::OnExecutePingTimer") );

    if( m_iPingsSent  !=  m_iPingsReceived )
    {
        wxLogDebug( wxT("ERROR: Last ping was not handled before next Ping is scheduled to be sent  --  calling OnControllerPingFailed( )") );
        OnControllerPingFailed( );
        return;
    }

    int     iRet;
    iRet = controller_ping_start( controller, "" );
    if( iRet  !=  0 )
    {
        //  We got back an error from the controller when we tried to send our ping.
        //  This is not good...
        //  We should tell someone about it.
        wxLogDebug( wxT("ERROR returned from call to controller_ping_start( )  --  calling OnControllerPingFailed( )") );
        OnControllerPingFailed( );
        return;
    }
    m_iPingsSent++;
}


//*****************************************************************************
void MeetingCenterController::OnControllerPingFailed( )
{
    wxLogDebug( wxT("entering MCC::OnControllerPingFailed( ) - creating event and passing it to OnSessionError( )") );

    m_PingTimer.Stop( );
    m_iPingsSent     = 0;
    m_iPingsReceived = 0;

    SessionErrorEvent   ev( wxID_ANY, ERR_CONNECTLOST, "Server ping failed.", "", "" );
    this->AddPendingEvent( ev );
}


//*****************************************************************************
void MeetingCenterController::OnControllerPingSucceeded( wxCommandEvent& event )
{
    m_iPingsReceived++;
}


//*****************************************************************************
void MeetingCenterController::OnIMChatEvent(IMChatEvent& event)
{
    wxLogMessage( wxT("entering MeetingCenterController::OnIMChatEvent") );

    if (this->m_pChatMainFrame != NULL)
    {
        if (event.GetType() == IMChatEvent::Normal)
            m_pChatMainFrame->IncomingIM(event.GetUser(), event.GetBody());
        else
            m_pChatMainFrame->IncomingComposing(event.GetUser(), event.GetType() == IMChatEvent::Composing);
    }
    else if (event.GetType() == IMChatEvent::Normal)
    {
        Commands().Add( wxT("start-im"), event.GetUser(), event.GetServer(), event.GetBody() );

        ExecutePendingCommands();
    }
}


//*****************************************************************************
void MeetingCenterController::OnIMInviteEvent(IMInviteEvent& event)
{
    wxLogMessage( wxT("entering MeetingCenterController::OnIMInviteEvent") );
}


//*****************************************************************************
bool MeetingCenterController::IMClientMode()
{
   return m_fIMClientMode;
}


//*****************************************************************************
wxWindow * MeetingCenterController::ShowAppWindow()
{
    if (m_pSignOnDlg)
    {
        m_pSignOnDlg->Raise();
        return m_pSignOnDlg;
    }
    else if ((m_ControllerState & State_Connected) != 0)
    {
        if (IMClientMode())
        {
            ContactsManage();
            return m_pManageContactsFrame;
        }
        else if (IsMeetingActive())
        {
            MeetingMainFrame * wnd = FindMeetingWindow();
            if (wnd) 
            {
                CUtils::RestoreWindow(wnd);
            }
            return wnd;
        }
        else if (!AnonymousMode())
        {
            ShowMeetingCenterWindow();
            return this;
        }
    }
    return NULL;
}


//*****************************************************************************
void MeetingCenterController::OnTaskIconDblClick(wxTaskBarIconEvent& event)
{
    ShowAppWindow();
}


//*****************************************************************************
void MeetingCenterController::OnLinkClicked( wxHtmlLinkEvent& event )
{
    wxLaunchDefaultBrowser( event.GetLinkInfo().GetHref() );
}

//*****************************************************************************
void MeetingCenterController::OnHTMLClicked( wxHtmlCellEvent& event )
{
    wxHtmlLinkInfo *info = event.GetCell()->GetLink();
    if (info != NULL)
        wxLaunchDefaultBrowser( info->GetHref() );
}
