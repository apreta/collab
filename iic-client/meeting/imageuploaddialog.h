#ifndef IMAGEUPLOADDIALOG_H
#define IMAGEUPLOADDIALOG_H

//(*Headers(ImageUploadDialog)
#include <wx/checkbox.h>
#include <wx/panel.h>
#include <wx/button.h>
#include <wx/dialog.h>
//*)


class FileManager;
class FileDetails;


class ImagePanel: public wxPanel
{
public:
    static const int ProfileWidth = 100;
    static const int ProfileHeight = 120;
    static const int RowSize = 4;

    ImagePanel(wxWindow* parent, wxSize size, wxString title, bool singleImage);
    virtual ~ImagePanel();

    void Initialize(FileManager* mgr);
    void AddFile(bool crop = true);
    void PopulateFiles();

protected:
    void OnLinkClicked(wxHtmlLinkEvent& event);
    
    void PopulateFile(FileDetails * file, wxString& html, int index);
    wxImage ResizeImage(wxImage& image, int width, int height, bool crop);

    wxString m_title;
    wxHtmlWindow* m_FileDisplay;
    bool m_singleImage;
    FileManager* m_fileManager;
};


class ImageUploadDialog: public wxDialog
{
public:
    ImageUploadDialog(wxWindow* parent, const wxString& caption, const wxString& repoPath, const wxString& storeName, bool singleImage);
    virtual ~ImageUploadDialog();
    
    void SetDefaultImage(const wxString& localFile);
    bool ShowDialog();

    //(*Declarations(ImageUploadDialog)
    wxButton* m_AddButton;
    wxPanel* m_ImageContainer;
    wxCheckBox* m_CropImageCheck;
    //*)


protected:

    //(*Identifiers(ImageUploadDialog)
    //*)

    void PopulateFile(FileDetails * file, wxString& html, int index);
    void PopulateFiles();
    wxImage ResizeImage(wxImage& image, int width, int height);
    bool LoadImages();
    bool StoreImages();
    
    
private:

    //(*Handlers(ImageUploadDialog)
    void OnAddButtonClick(wxCommandEvent& event);
    //*)
    
    DECLARE_EVENT_TABLE()
    
    bool m_singleImage;
    wxString m_repoPath;
    wxString m_storeName;
    ImagePanel* m_imagePanel;
    FileManager* m_fileManager;
};

#endif
