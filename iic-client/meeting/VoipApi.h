/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef VOIP_API_H
#define VOIP_API_H

#if 0
#ifdef IICVOIP_EXPORTS
#define DLL __declspec(dllexport)
#else
#define DLL __declspec(dllimport)
#endif
#else
#define DLL
#endif

#define IICVOIP_OK 0
#define IICVOIP_FAIL -1
#define IICVOIP_NOTINIT -2
#define IICVOIP_CALLACTIVE -3

#define IICVOIP_ERROR 0
#define IICVOIP_WARN  1
#define IICVOIP_INFO  2
#define IICVOIP_DEBUG 3

typedef void (*VoipCallback)(int param);
typedef void (*VoipLogger)(int level, const char *log);
typedef void (*VoipLevelCallback)(float input, float output);

void DLL VoipSetOnRinging(VoipCallback h);
void DLL VoipSetOnEstablished(VoipCallback h);
void DLL VoipSetOnCleared(VoipCallback h);
void DLL VoipSetLogger(VoipLogger h);
void DLL VoipSetLevelCallback(VoipLevelCallback h);

int DLL VoipInitialize(const char * dir = 0);
int DLL VoipShutdown();
int DLL VoipSetOption(const char * option, const char * value);
int DLL VoipMakeCall(const char * from_user, const char * to_address);
int DLL VoipHangup();
int DLL VoipDial(const char * str);
int DLL VoipMute(int muted);
int DLL VoipSpeakerMute(int muted);
bool DLL VoipIsInCall();

#endif
