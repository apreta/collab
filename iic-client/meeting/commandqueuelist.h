/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef _COMMANDQUEUELIST_H_
#define _COMMANDQUEUELIST_H_

#include <wx/list.h>

/**
  * An Open Zon task
  */
class CommandQueueElement
{
public:
	/** Simple constructor */
	CommandQueueElement( const wxString & taskName );
	virtual ~CommandQueueElement();

	/**
	  * Set the task name.<br>
	  * Think in terms of computer readable here. This is not
	  * intended to be a human friendly display item.
	  * @param taskName The name of the task.
	  */
	void SetTaskName( const wxString & taskName );

    /**
      * Return the task name.
      * @return const wxString & Returns a ref to the task name.
      */
    const wxString & GetTaskName()
    {
        return m_strTaskName;
    }

    /** Compare task names
      */
    bool IsTaskName( const wxString & name )
    {
        return m_strTaskName.CmpNoCase( name ) == 0;
    }
    bool IsTaskName( const wxChar * name )
    {
        return m_strTaskName.CmpNoCase( name ) == 0;
    }

	/**
	 ** Append a task parameter.
	 ** @param p The parameter.
	 ** @return int Returns the number of parameters.
	 **/
	int Append( const wxString & p );

	/**
	 ** Return the number of parameters.
	 ** @return int Returns the m_params count.
	 **/
	int GetParamCount()
	{
	    return m_params.Count();
	}

    /**
      * Return a ref to the parameter array.
      */
    const wxArrayString & GetParams()
    {
        return m_params;
    }

    /**
      * Return a parameter by index.
      * @param ndx The index value (0..n)
      * @return const wxString & Returns a ref to the parameter. A ref to wxEmpty string is returned if ndx is out of range.
      */
    wxString GetParam( int ndx )
    {
        wxString ret( wxT("") );
        if( ndx >= 0 && ndx < GetParamCount() )
        {
            ret = m_params[ ndx ];
        }
        return ret;
    }

private:
	/** The name of the task */
	wxString m_strTaskName;

	/** The array of parameters associated with the task */
	wxArrayString m_params;
};

WX_DECLARE_LIST( CommandQueueElement, CommandQueueList );

#endif
