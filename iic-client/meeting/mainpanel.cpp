/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"
#include "mainpanel.h"
#include "meetingcentercontroller.h"
#include "meetingmainframe.h"
#include "docshareview.h"
#include "waitforstartdialog.h"
//#include "../../services/common/eventnames.h"
#include "projectcodedialog.h"
#include "filemanager.h"

//(*InternalHeaders(MainPanel)
#include <wx/xrc/xmlres.h>
//*)
#include <wx/statline.h>
#include <wx/file.h>
#include <wx/html/htmlwin.h>

#include "appsharepanel.h"

#ifdef __WXMAC__
#include <Carbon/Carbon.h>
#endif

//(*IdInit(MainPanel)
//*)

BEGIN_EVENT_TABLE(MainPanel,wxPanel)
	//(*EventTable(MainPanel)
	//*)
END_EVENT_TABLE()


//*****************************************************************************
MainPanel::MainPanel(wxWindow* parent, wxWindowID id, ShareControl *control)
{
    m_fMeetingHasStarted = false;
    m_HTMLWindow         = NULL;
    m_pShareControl      = control;
    m_pWaitDlg           = NULL;
    m_fMeetingFetched    = false;
    m_fPicturesFetched    = false;
    m_fDocShareInProgress = false;
    m_wndToolBar = NULL;
    m_wndToolBar2 = NULL;

	//(*Initialize(MainPanel)
	wxXmlResource::Get()->LoadObject(this,parent,_T("MainPanel"),_T("wxPanel"));
	//*)
    CONTROLLER.Connect(wxID_ANY, wxEVT_MEETING_EVENT, MeetingEventHandler(MainPanel::OnMeetingEvent), 0, this );
    CONTROLLER.Connect(wxID_ANY, wxEVT_MEETING_ERROR, MeetingErrorHandler(MainPanel::OnMeetingError), 0, this );
    CONTROLLER.Connect(wxID_ANY, wxEVT_DOCSHARE_EVENT, DocshareEventHandler(MainPanel::OnDocshareEvent), 0, this );

    Connect(wxID_ANY, wxEVT_COMMAND_HTML_LINK_CLICKED, wxHtmlLinkEventHandler(MainPanel::OnLinkClicked));
    Connect(wxID_ANY, wxEVT_COMMAND_HTML_CELL_CLICKED, wxHtmlCellEventHandler(MainPanel::OnHTMLClicked));
    Connect(wxEVT_FILEMANAGER_DONE, wxCommandEventHandler(MainPanel::OnFileManagerDone));
    Connect(wxEVT_COMMAND_AUINOTEBOOK_PAGE_CHANGED, wxAuiNotebookEventHandler(MainPanel::OnTabChange));
    Connect(wxID_ANY, wxEVT_TIMER, wxTimerEventHandler(MainPanel::OnTimer));

    m_timer.SetOwner(this);

	m_pNotebook = CreateNotebook();
	GetSizer()->Add(m_pNotebook, 1, wxEXPAND, 0 );
	GetSizer()->Layout();
}


//*****************************************************************************
void MainPanel::OnGetPassword( wxCommandEvent& event )
{
//    wxLogDebug( wxT("entering MainPanel::OnGetPassword( wxCommandEvent& event )") );

    wxString password;
    wxString title;

    // Get the title truncated to 60 chars or less to prevent the dialog from looking silly
    title = m_strMeetingTitle;
    if( title.Len( )  >  60 )
    {
        title = title.Left( 60 );
    }

    wxString    caption( _("Password:"), wxConvUTF8 );
    caption += wxT(" ");
    caption += title;

    wxString                msg( _("Please enter the password:"), wxConvUTF8 );
    wxPasswordEntryDialog   AskForPassword( this, msg, caption );
    int nResp = AskForPassword.ShowModal( );
    if( nResp  !=  wxID_OK )
    {
        return;     // They cancelled out of the dialog
    }
    password = AskForPassword.GetValue( );

    int     iRet;
    iRet = controller_meeting_join( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), m_strParticipantID.mb_str(wxConvUTF8), password.mb_str(wxConvUTF8) );
}


//*****************************************************************************
MainPanel::~MainPanel()
{
    if( m_pWaitDlg )
        m_pWaitDlg->Destroy( );
    m_pWaitDlg = NULL;
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_MEETING_FETCHED,wxCommandEventHandler( MainPanel::OnMeetingFetched), 0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_MEETING_EVENT, MeetingEventHandler(MainPanel::OnMeetingEvent), 0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_MEETING_ERROR, MeetingErrorHandler(MainPanel::OnMeetingError), 0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_DOCSHARE_EVENT, DocshareEventHandler(MainPanel::OnDocshareEvent), 0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_GET_PASSWORD,  wxCommandEventHandler(MainPanel::OnGetPassword), 0, this );

    Disconnect(wxEVT_FILEMANAGER_DONE, wxCommandEventHandler(MainPanel::OnFileManagerDone));
    Disconnect(wxEVT_COMMAND_AUINOTEBOOK_PAGE_CHANGED, wxAuiNotebookEventHandler(MainPanel::OnTabChange));
    Disconnect(wxID_ANY, wxEVT_TIMER, wxTimerEventHandler(MainPanel::OnTimer));
}


//*****************************************************************************
wxHtmlWindow* MainPanel::CreateHTMLCtrl(wxWindow* parent)
{
    if (!parent)
        parent = this;

    m_HTMLWindow = new wxHtmlWindow(parent, wxID_ANY,
                                    wxDefaultPosition,
                                    wxSize(400,300));

    return m_HTMLWindow;
}


//*****************************************************************************
wxToolBar* MainPanel::CreateDocShareToolbar(wxWindow* parent)
{
    wxToolBar* wndToolBar = new wxToolBar(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTB_HORIZONTAL | wxNO_BORDER | wxTB_FLAT);
    wndToolBar->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_3DLIGHT ) );
    wxBitmap bmpT;
    wxToolBarToolBase *pTBTB;
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb1_01.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar->AddTool( XRCID("IDC_DOCSHARETB_NEW"), wxString(_("New"), wxConvUTF8), bmpT, wxString(_("New Document"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb1_02.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar->AddTool( XRCID("IDC_DOCSHARETB_OPEN"), wxString(_("Open"), wxConvUTF8), bmpT, wxString(_("Open Document"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb1_03.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar->AddTool( XRCID("IDC_DOCSHARETB_SAVE"), wxString(_("Save"), wxConvUTF8), bmpT, wxString(_("Save Document"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb1_04.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar->AddTool( XRCID("IDC_DOCSHARETB_SAVEAS"), wxString(_("Save As"), wxConvUTF8), bmpT, wxString(_("Save As Document"), wxConvUTF8), wxITEM_NORMAL );
    wndToolBar->AddSeparator( );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb1_05.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar->AddTool( XRCID("IDC_DOCSHARETB_UPLOADDOC"), wxString(_("Upload document"), wxConvUTF8), bmpT, wxString(_("Upload document"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb1_06.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar->AddTool( XRCID("IDC_DOCSHARETB_ORGANIZEPAGES"), wxString(_("Organize pages"), wxConvUTF8), bmpT, wxString(_("Organize pages"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb1_07.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar->AddTool( XRCID("IDC_DOCSHARETB_INSERTPAGE"), wxString(_("Insert new blank page"), wxConvUTF8), bmpT, wxString(_("Insert new blank page"), wxConvUTF8), wxITEM_NORMAL );
    wndToolBar->AddSeparator( );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb1_08.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar->AddTool( XRCID("IDC_DOCSHARETB_FIRSTPAGE"), wxString(_("First page"), wxConvUTF8), bmpT, wxString(_("First page"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb1_09.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar->AddTool( XRCID("IDC_DOCSHARETB_PREVIOUSPAGE"), wxString(_("Previous page"), wxConvUTF8), bmpT, wxString(_("Previous page"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb1_10.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar->AddTool( XRCID("IDC_DOCSHARETB_GOTOPAGE"), wxString(_("Goto page"), wxConvUTF8), bmpT, wxString(_("Goto page"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb1_11.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar->AddTool( XRCID("IDC_DOCSHARETB_NEXTPAGE"), wxString(_("Next page"), wxConvUTF8), bmpT, wxString(_("Next page"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb1_12.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar->AddTool( XRCID("IDC_DOCSHARETB_LASTPAGE"), wxString(_("Last page"), wxConvUTF8), bmpT, wxString(_("Last page"), wxConvUTF8), wxITEM_NORMAL );
    wndToolBar->AddSeparator( );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb1_13.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar->AddTool( XRCID("IDC_DOCSHARETB_CUT"), wxString(_("Cut"), wxConvUTF8), bmpT, wxString(_("Cut"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb1_14.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar->AddTool( XRCID("IDC_DOCSHARETB_COPY"), wxString(_("Copy"), wxConvUTF8), bmpT, wxString(_("Copy"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb1_15.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar->AddTool( XRCID("IDC_DOCSHARETB_PASTE"), wxString(_("Paste"), wxConvUTF8), bmpT, wxString(_("Paste"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb1_16.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar->AddTool( XRCID("IDC_DOCSHARETB_DELETESEL"), wxString(_("Delete the selection"), wxConvUTF8), bmpT, wxString(_("Delete the selection"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb1_17.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar->AddTool( XRCID("IDC_DOCSHARETB_CLEARPAGE"), wxString(_("Clear current page"), wxConvUTF8), bmpT, wxString(_("Clear current page"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb1_20.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar->AddTool( XRCID("IDC_DOCSHARETB_CLEARALL"), wxString(_("Clear all pages"), wxConvUTF8), bmpT, wxString(_("Clear all pages"), wxConvUTF8), wxITEM_NORMAL );
    wndToolBar->AddSeparator( );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb1_18.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar->AddTool( XRCID("IDC_DOCSHARETB_UNDO"), wxString(_("Undo"), wxConvUTF8), bmpT, wxString(_("Undo"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb1_19.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar->AddTool( XRCID("IDC_DOCSHARETB_REDO"), wxString(_("Redo"), wxConvUTF8), bmpT, wxString(_("Redo"), wxConvUTF8), wxITEM_NORMAL );
    wndToolBar->Realize( );
    return wndToolBar;
}


//*****************************************************************************
wxToolBar* MainPanel::CreateDocShareToolbar2(wxWindow* parent)
{
    wxToolBar* wndToolBar2 = new wxToolBar(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTB_HORIZONTAL | wxNO_BORDER | wxTB_FLAT);
    wndToolBar2->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_3DLIGHT ) );
    wxBitmap bmpT;
    wxToolBarToolBase *pTBTB;
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb2_00.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar2->AddTool( XRCID("IDC_DOCSHARETB_POINTER"), wxString(_("Show Pointer"), wxConvUTF8), bmpT, wxString(_("Share pointer position"), wxConvUTF8), wxITEM_CHECK );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb2_01.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar2->AddTool( XRCID("IDC_DOCSHARETB_SELECT"), wxString(_("Select"), wxConvUTF8), bmpT, wxString(_("Select objects"), wxConvUTF8), wxITEM_CHECK );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb2_02.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar2->AddTool( XRCID("IDC_DOCSHARETB_LINE"), wxString(_("Draw line"), wxConvUTF8), bmpT, wxString(_("Draw a straight line"), wxConvUTF8), wxITEM_CHECK );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb2_03.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar2->AddTool( XRCID("IDC_DOCSHARETB_ARROW"), wxString(_("Draw arrow line"), wxConvUTF8), bmpT, wxString(_("Draw a line with an arrowhead"), wxConvUTF8), wxITEM_CHECK );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb2_04.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar2->AddTool( XRCID("IDC_DOCSHARETB_SCRIBBLE"), wxString(_("Scribble"), wxConvUTF8), bmpT, wxString(_("Scribble!!"), wxConvUTF8), wxITEM_CHECK );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb2_05.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar2->AddTool( XRCID("IDC_DOCSHARETB_HIGHLIGHT"), wxString(_("Highlight"), wxConvUTF8), bmpT, wxString(_("Highlight"), wxConvUTF8), wxITEM_CHECK );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb2_06.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar2->AddTool( XRCID("IDC_DOCSHARETB_RECTANGLE"), wxString(_("Draw a rectangle"), wxConvUTF8), bmpT, wxString(_("Draw a rectangle"), wxConvUTF8), wxITEM_CHECK );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb2_07.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar2->AddTool( XRCID("IDC_DOCSHARETB_ELLIPSE"), wxString(_("Draw an ellipse"), wxConvUTF8), bmpT, wxString(_("Draw an ellipse"), wxConvUTF8), wxITEM_CHECK );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb2_08.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar2->AddTool( XRCID("IDC_DOCSHARETB_TEXTBOX"), wxString(_("Draw a textbox"), wxConvUTF8), bmpT, wxString(_("Draw a textbox"), wxConvUTF8), wxITEM_CHECK );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb2_09.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar2->AddTool( XRCID("IDC_DOCSHARETB_CHECKMARK"), wxString(_("Draw a check mark"), wxConvUTF8), bmpT, wxString(_("Draw a check mark"), wxConvUTF8), wxITEM_CHECK );
    wndToolBar2->AddSeparator( );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb2_10.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar2->AddTool( XRCID("IDC_DOCSHARETB_LINETHICKNESS"), wxString(_("Line thickness"), wxConvUTF8), bmpT, wxString(_("Line thickness"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb2_11.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar2->AddTool( XRCID("IDC_DOCSHARETB_LINESTYLE"), wxString(_("Line style"), wxConvUTF8), bmpT, wxString(_("Line style"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb2_12.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar2->AddTool( XRCID("IDC_DOCSHARETB_FONTSETTINGS"), wxString(_("Font settings"), wxConvUTF8), bmpT, wxString(_("Font settings"), wxConvUTF8), wxITEM_NORMAL );
    wndToolBar2->AddSeparator( );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb2_13.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar2->AddTool( XRCID("IDC_DOCSHARETB_LINECOLOR"), wxString(_("Line or border color"), wxConvUTF8), bmpT, wxString(_("Line or border color"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb2_14.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar2->AddTool( XRCID("IDC_DOCSHARETB_BGCOLOR"), wxString(_("Background color"), wxConvUTF8), bmpT, wxString(_("Background color"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb2_15.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar2->AddTool( XRCID("IDC_DOCSHARETB_TEXTCOLOR"), wxString(_("Text color"), wxConvUTF8), bmpT, wxString(_("Text color"), wxConvUTF8), wxITEM_NORMAL );
    wndToolBar2->AddSeparator( );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb2_16.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar2->AddTool( XRCID("IDC_DOCSHARETB_TOFRONT"), wxString(_("Bring to front"), wxConvUTF8), bmpT, wxString(_("Bring to front"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb2_17.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar2->AddTool( XRCID("IDC_DOCSHARETB_TOBACK"), wxString(_("Send to back"), wxConvUTF8), bmpT, wxString(_("Send to back"), wxConvUTF8), wxITEM_NORMAL );
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/ds_tb2_18.bmp")), wxBITMAP_TYPE_BMP );
    pTBTB = wndToolBar2->AddTool( XRCID("IDC_DOCSHARETB_TOGTRANS"), wxString(_("Toggle transparency"), wxConvUTF8), bmpT, wxString(_("Toggle transparency"), wxConvUTF8), wxITEM_NORMAL );
    wndToolBar2->Realize( );
    return wndToolBar2;
}


//*****************************************************************************
void MainPanel::DisableDocShareToolbar( )
{
    if (NULL != m_wndToolBar)
    {
        m_wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_NEW"),            false );
        m_wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_OPEN"),           false );
        m_wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_SAVE"),           false );
        m_wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_SAVEAS"),         false );
        m_wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_UPLOADDOC"),      false );
        m_wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_ORGANIZEPAGES"),  false );
        m_wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_INSERTPAGE"),     false );
        m_wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_FIRSTPAGE"),      false );
        m_wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_PREVIOUSPAGE"),   false );
        m_wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_GOTOPAGE"),       false );
        m_wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_NEXTPAGE"),       false );
        m_wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_LASTPAGE"),       false );
        m_wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_CUT"),            false );
        m_wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_COPY"),           false );
        m_wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_PASTE"),          false );
        m_wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_DELETESEL"),      false );
        m_wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_CLEARPAGE"),      false );
        m_wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_UNDO"),           false );
        m_wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_REDO"),           false );
    }
    if (NULL != m_wndToolBar2)
    {
        m_wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_POINTER"),       false );
        m_wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_SELECT"),        false );
        m_wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_LINE"),          false );
        m_wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_ARROW"),         false );
        m_wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_SCRIBBLE"),      false );
        m_wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_HIGHLIGHT"),     false );
        m_wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_RECTANGLE"),     false );
        m_wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_ELLIPSE"),       false );
        m_wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_TEXTBOX"),       false );
        m_wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_CHECKMARK"),     false );
        m_wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_LINETHICKNESS"), false );
        m_wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_LINESTYLE"),     false );
        m_wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_FONTSETTINGS"),  false );
        m_wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_LINECOLOR"),     false );
        m_wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_BGCOLOR"),       false );
        m_wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_TEXTCOLOR"),     false );
        m_wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_TOFRONT"),       false );
        m_wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_TOBACK"),        false );
        m_wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_TOGTRANS"),      false );
        m_wndToolBar2->ToggleTool( XRCID("IDC_DOCSHARETB_LINE"),          false );
        m_wndToolBar2->ToggleTool( XRCID("IDC_DOCSHARETB_SELECT"),        false );
        m_wndToolBar2->ToggleTool( XRCID("IDC_DOCSHARETB_ARROW"),         false );
        m_wndToolBar2->ToggleTool( XRCID("IDC_DOCSHARETB_SCRIBBLE"),      false );
        m_wndToolBar2->ToggleTool( XRCID("IDC_DOCSHARETB_HIGHLIGHT"),     false );
        m_wndToolBar2->ToggleTool( XRCID("IDC_DOCSHARETB_RECTANGLE"),     false );
        m_wndToolBar2->ToggleTool( XRCID("IDC_DOCSHARETB_ELLIPSE"),       false );
        m_wndToolBar2->ToggleTool( XRCID("IDC_DOCSHARETB_TEXTBOX"),       false );
        m_wndToolBar2->ToggleTool( XRCID("IDC_DOCSHARETB_CHECKMARK"),     false );
    }
}


class MainPanelTabArt : public wxAuiDefaultTabArt {
    MainPanel* m_pOwner;
    
public:
    MainPanelTabArt(MainPanel *owner) : m_pOwner(owner)
    { }
   
    wxAuiTabArt* Clone();
    void DrawTab(wxDC& dc,
                 wxWindow* wnd,
                 const wxAuiNotebookPage& page,
                 const wxRect& in_rect,
                 int close_button_state,
                 wxRect* out_tab_rect,
                 wxRect* out_button_rect,
                 int* x_extent);
};

wxAuiTabArt* MainPanelTabArt::Clone()
{
    wxAuiDefaultTabArt* art = new MainPanelTabArt(m_pOwner);
    art->SetNormalFont(m_normal_font);
    art->SetSelectedFont(m_selected_font);
    art->SetMeasuringFont(m_measuring_font);

    return art;
}

// wxAuiBlendColour is used by wxAuiStepColour
static unsigned char wxAuiBlendColour(unsigned char fg, unsigned char bg, double alpha)
{
    double result = bg + (alpha * (fg - bg));
    if (result < 0.0)
        result = 0.0;
    if (result > 255)
        result = 255;
    return (unsigned char)result;
}

// wxAuiStepColour() it a utility function that simply darkens
// or lightens a color, based on the specified percentage
// ialpha of 0 would be completely black, 100 completely white
// an ialpha of 100 returns the same colour
static wxColor wxAuiStepColour(const wxColor& c, int ialpha)
{
    if (ialpha == 100)
        return c;

    unsigned char r = c.Red(),
                  g = c.Green(),
                  b = c.Blue();
    unsigned char bg;

    // ialpha is 0..200 where 0 is completely black
    // and 200 is completely white and 100 is the same
    // convert that to normal alpha 0.0 - 1.0
    ialpha = wxMin(ialpha, 200);
    ialpha = wxMax(ialpha, 0);
    double alpha = ((double)(ialpha - 100.0))/100.0;

    if (ialpha > 100)
    {
        // blend with white
        bg = 255;
        alpha = 1.0 - alpha;  // 0 = transparent fg; 1 = opaque fg
    }
    else
    {
        // blend with black
        bg = 0;
        alpha += 1.0;         // 0 = transparent fg; 1 = opaque fg
    }

    r = wxAuiBlendColour(r, bg, alpha);
    g = wxAuiBlendColour(g, bg, alpha);
    b = wxAuiBlendColour(b, bg, alpha);

    return wxColour(r, g, b);
}

static void IndentPressedBitmap(wxRect* rect, int button_state)
{
    if (button_state == wxAUI_BUTTON_STATE_PRESSED)
    {
        rect->x++;
        rect->y++;
    }
}

static void DrawFocusRect(wxWindow* win, wxDC& dc, const wxRect& rect, int flags)
{
#ifdef __WXMSW__
    wxUnusedVar(win);
    wxUnusedVar(flags);

    RECT rc;
    wxCopyRectToRECT(rect, rc);

    ::DrawFocusRect(GetHdcOf(dc), &rc);

#elif defined(__WXGTK20__)
    GdkWindow* gdk_window = dc.GetGDKWindow();
    wxASSERT_MSG( gdk_window,
                  wxT("cannot draw focus rectangle on wxDC of this type") );

    GtkStateType state;
    //if (flags & wxCONTROL_SELECTED)
    //    state = GTK_STATE_SELECTED;
    //else
        state = GTK_STATE_NORMAL;

    gtk_paint_focus( win->m_widget->style,
                     gdk_window,
                     state,
                     NULL,
                     win->m_wxwindow,
                     NULL,
                     dc.LogicalToDeviceX(rect.x),
                     dc.LogicalToDeviceY(rect.y),
                     rect.width,
                     rect.height );
#elif (defined(__WXMAC__))

#if wxMAC_USE_CORE_GRAPHICS
    {
        CGRect cgrect = CGRectMake( rect.x , rect.y , rect.width, rect.height ) ;

#if 0
        Rect bounds ;
        win->GetPeer()->GetRect( &bounds ) ;

        wxLogDebug(wxT("Focus rect %d, %d, %d, %d"), rect.x, rect.y, rect.width, rect.height);
        wxLogDebug(wxT("Peer rect %d, %d, %d, %d"), bounds.left, bounds.top, bounds.right - bounds.left, bounds.bottom - bounds.top);
#endif

        HIThemeFrameDrawInfo info ;
        memset( &info, 0 , sizeof(info) ) ;

        info.version = 0 ;
        info.kind = 0 ;
        info.state = kThemeStateActive;
        info.isFocused = true ;

        CGContextRef cgContext = (CGContextRef) win->MacGetCGContextRef() ;
        wxASSERT( cgContext ) ;

        HIThemeDrawFocusRect( &cgrect , true , cgContext , kHIThemeOrientationNormal ) ;
    }
 #else
    {
        Rect r;
        r.left = rect.x; r.top = rect.y; r.right = rect.GetRight(); r.bottom = rect.GetBottom();
        wxTopLevelWindowMac* top = win->MacGetTopLevelWindow();
        if ( top )
        {
            wxPoint pt(0, 0) ;
            wxMacControl::Convert( &pt , win->GetPeer() , top->GetPeer() ) ;
            OffsetRect( &r , pt.x , pt.y ) ;
        }

        DrawThemeFocusRect( &r , true ) ;
    }
#endif
#else
    wxUnusedVar(win);
    wxUnusedVar(flags);

    // draw the pixels manually because the "dots" in wxPen with wxDOT style
    // may be short traits and not really dots
    //
    // note that to behave in the same manner as DrawRect(), we must exclude
    // the bottom and right borders from the rectangle
    wxCoord x1 = rect.GetLeft(),
            y1 = rect.GetTop(),
            x2 = rect.GetRight(),
            y2 = rect.GetBottom();

    dc.SetPen(*wxBLACK_PEN);

#ifdef __WXMAC__
    dc.SetLogicalFunction(wxCOPY);
#else
    // this seems to be closer than what Windows does than wxINVERT although
    // I'm still not sure if it's correct
    dc.SetLogicalFunction(wxAND_REVERSE);
#endif

    wxCoord z;
    for ( z = x1 + 1; z < x2; z += 2 )
        dc.DrawPoint(z, rect.GetTop());

    wxCoord shift = z == x2 ? 0 : 1;
    for ( z = y1 + shift; z < y2; z += 2 )
        dc.DrawPoint(x2, z);

    shift = z == y2 ? 0 : 1;
    for ( z = x2 - shift; z > x1; z -= 2 )
        dc.DrawPoint(z, y2);

    shift = z == x1 ? 0 : 1;
    for ( z = y2 - shift; z > y1; z -= 2 )
        dc.DrawPoint(x1, z);

    dc.SetLogicalFunction(wxCOPY);
#endif
}

// DrawTab() draws an individual tab.
//
// dc       - output dc
// in_rect  - rectangle the tab should be confined to
// caption  - tab's caption
// active   - whether or not the tab is active
// out_rect - actual output rectangle
// x_extent - the advance x; where the next tab should start

void MainPanelTabArt::DrawTab(wxDC& dc,
                                 wxWindow* wnd,
                                 const wxAuiNotebookPage& page,
                                 const wxRect& in_rect,
                                 int close_button_state,
                                 wxRect* out_tab_rect,
                                 wxRect* out_button_rect,
                                 int* x_extent)
{
    wxColor wxAuiStepColour(const wxColor& c, int percent);
    
    wxCoord normal_textx, normal_texty;
    wxCoord selected_textx, selected_texty;
    wxCoord texty;

    wxColor foreColor = dc.GetTextForeground();
    
    // if the caption is empty, measure some temporary text
    wxString caption = page.caption;
    if (caption.empty())
        caption = wxT("Xj");

    dc.SetFont(m_selected_font);
    dc.GetTextExtent(caption, &selected_textx, &selected_texty);

    dc.SetFont(m_normal_font);
    dc.GetTextExtent(caption, &normal_textx, &normal_texty);

    // figure out the size of the tab
    wxSize tab_size = GetTabSize(dc,
                                 wnd,
                                 page.caption,
                                 page.bitmap,
                                 page.active,
                                 close_button_state,
                                 x_extent);

    wxCoord tab_height = m_tab_ctrl_height - 3;
    wxCoord tab_width = tab_size.x;
    wxCoord tab_x = in_rect.x;
    wxCoord tab_y = in_rect.y + in_rect.height - tab_height;


    caption = page.caption;


    // select pen, brush and font for the tab to be drawn

    if (page.active)
    {
        dc.SetFont(m_selected_font);
        texty = selected_texty;
    }
     else
    {
        dc.SetFont(m_normal_font);
        texty = normal_texty;
    }


    // create points that will make the tab outline

    int clip_width = tab_width;
    if (tab_x + clip_width > in_rect.x + in_rect.width)
        clip_width = (in_rect.x + in_rect.width) - tab_x;

    dc.SetClippingRegion(tab_x, tab_y, clip_width+1, tab_height-3);

    wxPoint border_points[6];
    if (m_flags &wxAUI_NB_BOTTOM)
    {
       border_points[0] = wxPoint(tab_x,             tab_y);
       border_points[1] = wxPoint(tab_x,             tab_y+tab_height-6);
       border_points[2] = wxPoint(tab_x+2,           tab_y+tab_height-4);
       border_points[3] = wxPoint(tab_x+tab_width-2, tab_y+tab_height-4);
       border_points[4] = wxPoint(tab_x+tab_width,   tab_y+tab_height-6);
       border_points[5] = wxPoint(tab_x+tab_width,   tab_y);
    }
    else //if (m_flags & wxAUI_NB_TOP) {}
    {
       border_points[0] = wxPoint(tab_x,             tab_y+tab_height-4);
       border_points[1] = wxPoint(tab_x,             tab_y+2);
       border_points[2] = wxPoint(tab_x+2,           tab_y);
       border_points[3] = wxPoint(tab_x+tab_width-2, tab_y);
       border_points[4] = wxPoint(tab_x+tab_width,   tab_y+2);
       border_points[5] = wxPoint(tab_x+tab_width,   tab_y+tab_height-4);
    }
    // TODO: else if (m_flags &wxAUI_NB_LEFT) {}
    // TODO: else if (m_flags &wxAUI_NB_RIGHT) {}

    int drawn_tab_yoff = border_points[1].y;
    int drawn_tab_height = border_points[0].y - border_points[1].y;


    if (page.active)
    {
        // draw active tab

        // draw base background color
        wxRect r(tab_x, tab_y, tab_width, tab_height);
        dc.SetPen(m_base_colour_pen);
        dc.SetBrush(m_base_colour_brush);
        dc.DrawRectangle(r.x+1, r.y+1, r.width-1, r.height-4);

        // this white helps fill out the gradient at the top of the tab
        dc.SetPen(*wxWHITE_PEN);
        dc.SetBrush(*wxWHITE_BRUSH);
        dc.DrawRectangle(r.x+2, r.y+1, r.width-3, r.height-4);

        // these two points help the rounded corners appear more antialiased
        dc.SetPen(m_base_colour_pen);
        dc.DrawPoint(r.x+2, r.y+1);
        dc.DrawPoint(r.x+r.width-2, r.y+1);

        // set rectangle down a bit for gradient drawing
        r.SetHeight(r.GetHeight()/2);
        r.x += 2;
        r.width -= 2;
        r.y += r.height;
        r.y -= 2;

        // draw gradient background
        wxColor top_color = *wxWHITE;
        wxColor bottom_color = m_base_colour;
        dc.GradientFillLinear(r, bottom_color, top_color, wxNORTH);
    }
     else
    {
        // draw inactive tab

        wxRect r(tab_x, tab_y+1, tab_width, tab_height-3);

        if (m_pOwner->IsHighlighted(caption, r)) {
            dc.SetBrush(*wxBLACK_BRUSH);
            dc.DrawRectangle(r);
            dc.SetTextForeground(*wxWHITE);
        }
        else 
        {
            // start the gradent up a bit and leave the inside border inset
            // by a pixel for a 3D look.  Only the top half of the inactive
            // tab will have a slight gradient
            r.x += 3;
            r.y++;
            r.width -= 4;
            r.height /= 2;
            r.height--;

            // -- draw top gradient fill for glossy look
            wxColor top_color = m_base_colour;
            wxColor bottom_color = wxAuiStepColour(top_color, 160);
            dc.GradientFillLinear(r, bottom_color, top_color, wxNORTH);

            r.y += r.height;
            r.y--;

            // -- draw bottom fill for glossy look
            top_color = m_base_colour;
            bottom_color = m_base_colour;
            dc.GradientFillLinear(r, top_color, bottom_color, wxSOUTH);
        }
    }

    // draw tab outline
    dc.SetPen(m_border_pen);
    dc.SetBrush(*wxTRANSPARENT_BRUSH);
    dc.DrawPolygon(WXSIZEOF(border_points), border_points);

    // there are two horizontal grey lines at the bottom of the tab control,
    // this gets rid of the top one of those lines in the tab control
    if (page.active)
    {
       if (m_flags &wxAUI_NB_BOTTOM)
           dc.SetPen(wxPen(wxColour(wxAuiStepColour(m_base_colour, 170))));
       // TODO: else if (m_flags &wxAUI_NB_LEFT) {}
       // TODO: else if (m_flags &wxAUI_NB_RIGHT) {}
       else //for wxAUI_NB_TOP
           dc.SetPen(m_base_colour_pen);
        dc.DrawLine(border_points[0].x+1,
                    border_points[0].y,
                    border_points[5].x,
                    border_points[5].y);
    }


    int text_offset = tab_x + 8;
    int close_button_width = 0;
    if (close_button_state != wxAUI_BUTTON_STATE_HIDDEN)
    {
        close_button_width = m_active_close_bmp.GetWidth();
    }


    int bitmap_offset = 0;
    if (page.bitmap.IsOk())
    {
        bitmap_offset = tab_x + 8;

        // draw bitmap
        dc.DrawBitmap(page.bitmap,
                      bitmap_offset,
                      drawn_tab_yoff + (drawn_tab_height/2) - (page.bitmap.GetHeight()/2),
                      true);

        text_offset = bitmap_offset + page.bitmap.GetWidth();
        text_offset += 3; // bitmap padding
    }
     else
    {
        text_offset = tab_x + 8;
    }


    wxString draw_text = caption; /*wxAuiChopText(dc,
                          caption,
                          tab_width - (text_offset-tab_x) - close_button_width);*/

    // draw tab text
    dc.DrawText(draw_text,
                text_offset,
                drawn_tab_yoff + (drawn_tab_height)/2 - (texty/2) - 1);

    // draw close button if necessary
    if (close_button_state != wxAUI_BUTTON_STATE_HIDDEN)
    {
        wxBitmap bmp = m_disabled_close_bmp;

        if (close_button_state == wxAUI_BUTTON_STATE_HOVER ||
            close_button_state == wxAUI_BUTTON_STATE_PRESSED)
        {
            bmp = m_active_close_bmp;
        }

        wxRect rect(tab_x + tab_width - close_button_width - 1,
                    tab_y + (tab_height/2) - (bmp.GetHeight()/2),
                    close_button_width,
                    tab_height);
        IndentPressedBitmap(&rect, close_button_state);
        dc.DrawBitmap(bmp, rect.x, rect.y, true);

        *out_button_rect = rect;
    }

    *out_tab_rect = wxRect(tab_x, tab_y, tab_width, tab_height);

#ifndef __WXMAC__
    // draw focus rectangle
    if (page.active && (wnd->FindFocus() == wnd))
    {
        wxRect focusRectText(text_offset, (drawn_tab_yoff + (drawn_tab_height)/2 - (texty/2) - 1),
            selected_textx, selected_texty);

        wxRect focusRect;
        wxRect focusRectBitmap;

        if (page.bitmap.IsOk())
            focusRectBitmap = wxRect(bitmap_offset, drawn_tab_yoff + (drawn_tab_height/2) - (page.bitmap.GetHeight()/2),
                                            page.bitmap.GetWidth(), page.bitmap.GetHeight());

        if (page.bitmap.IsOk() && draw_text.IsEmpty())
            focusRect = focusRectBitmap;
        else if (!page.bitmap.IsOk() && !draw_text.IsEmpty())
            focusRect = focusRectText;
        else if (page.bitmap.IsOk() && !draw_text.IsEmpty())
            focusRect = focusRectText.Union(focusRectBitmap);

        focusRect.Inflate(2, 2);

        DrawFocusRect(wnd, dc, focusRect, 0);
    }
#endif

    dc.DestroyClippingRegion();
    dc.SetTextForeground(foreColor);
}


//*****************************************************************************
wxAuiNotebook* MainPanel::CreateNotebook()
{
    // create the notebook off-window to avoid flicker
    //wxSize client_size = GetClientSize();

	wxPanel * panel;
	wxBoxSizer * sizer;
	
    wxAuiNotebook* ctrl = new wxAuiNotebook(this, wxID_ANY,
                                            wxPoint(-1,-1),
                                            wxSize(-1,-1),
                                            wxAUI_NB_TOP | wxAUI_NB_TAB_SPLIT | wxAUI_NB_TAB_MOVE | wxAUI_NB_SCROLL_BUTTONS | wxNO_BORDER);
    
    MainPanelTabArt* art = new MainPanelTabArt(this);
    ctrl->SetArtProvider(art);

    wxBitmap page_bmp = wxArtProvider::GetBitmap(wxART_NORMAL_FILE, wxART_OTHER, wxSize(16,16));
    
    ctrl->AddPage( CreateHTMLCtrl( ctrl ), _("Meeting Info"), false, page_bmp);

	// App share panel
    panel = new wxPanel( ctrl, wxID_ANY );
    m_pAppSharePanel = new AppSharePanel( panel, wxID_ANY, m_pShareControl );
	m_pAppSharePanel->SetMinSize(wxSize(20,20));
	m_pAppSharePanel->SetBackgroundColour(*wxWHITE);
	m_pAppSharePanel->ClearBackground();
	sizer  = new wxBoxSizer( wxVERTICAL );
	sizer->Add( m_pAppSharePanel,  1, wxALL | wxEXPAND , 0 );
	panel->SetSizer( sizer );
	ctrl->AddPage( panel, _("Desktop/Application Share"), false, page_bmp );
	
    // Doc share panel
    panel = new wxPanel( ctrl, wxID_ANY );
    m_wndToolBar = CreateDocShareToolbar(panel);
    m_wndToolBar2 = CreateDocShareToolbar2(panel);
    DisableDocShareToolbar( );
    
    m_pDocSharePanel = new DocShareCanvas( panel, wxPoint(0, 0), wxSize( -1, -1 ), 0 );
	m_pDocSharePanel->SetMinSize(wxSize(20,20));
    m_pDocSharePanel->SetScrollbars( kDocSharePixelsPerUnitX, kDocSharePixelsPerUnitY, 50, 50 );
    m_pDocSharePanel->SetBackgroundColour(*wxWHITE);
    m_pDocSharePanel->ClearBackground();

    wxStaticLine *divider = new wxStaticLine(panel, wxID_ANY);

    wxSizer * tbwrapper = new wxBoxSizer( wxHORIZONTAL );

    wxSizer *tbsizer = new wxBoxSizer( wxVERTICAL );
    tbsizer->AddSpacer( 1 );
    tbsizer->Add( m_wndToolBar,  0, wxALIGN_LEFT, 0);
    tbsizer->Add( m_wndToolBar2, 0, wxTOP | wxALIGN_LEFT, 0 );

    wxSizer *col2sizer = new wxBoxSizer( wxVERTICAL );
    wxButton * button = new wxButton( panel, XRCID("IDC_DLG_MTG_MENU_STOPSHARING"), _("Stop Sharing") );
    m_wndPositionLabel = new wxStaticText( panel, wxID_ANY, wxT(""), wxDefaultPosition, wxSize(-1, -1), 0 /*wxALIGN_CENTRE*/ );
    col2sizer->Add( button, 0 );
    col2sizer->Add( m_wndPositionLabel, 0, wxEXPAND|wxTOP, 5 );

    tbwrapper->Add( tbsizer );
    tbwrapper->Add( 50, 1, 0 );
    tbwrapper->Add( col2sizer );

    sizer  = new wxBoxSizer( wxVERTICAL );
    sizer->AddSpacer( 1 );
    sizer->Add( tbwrapper );
    sizer->AddSpacer( 1 );
    sizer->Add( divider, 0, wxEXPAND, 0 );
    sizer->Add( m_pDocSharePanel,  1, wxBOTTOM | wxEXPAND , 0 );
    
    panel->SetSizer( sizer );
    ctrl->AddPage( panel, _("Presentation/Document Share"), false, page_bmp );

   return ctrl;
}


//*****************************************************************************
void MainPanel::StartMeeting( const wxString &strParticipantID, const wxString &strParticipantName, const wxString &strMeetingID, const wxString &strMeetingTitle, bool fJoining )
{
//    wxLogDebug( wxT("entering MainPanel::StartMeeting( )") );

    m_strParticipantID = strParticipantID;
    m_strParticipantName = strParticipantName;
    m_strMeetingID = strMeetingID;
    m_strMeetingTitle = strMeetingTitle;

    // Make sure we are connected
    CONTROLLER.Connect(wxID_ANY, wxEVT_MEETING_FETCHED,wxCommandEventHandler( MainPanel::OnMeetingFetched), 0, this );

    if (!CONTROLLER.AnonymousMode())
    {
        CONTROLLER.FetchMeetingDetails( MeetingCenterController::OP_MainPanel, m_strMeetingID );
    }
    else
    {
        // We can't fetch details for anonymous user, proceed with what we get from meeting events.
        m_fMeetingFetched = true;
    }
}


//*****************************************************************************
void MainPanel::EndMeeting( )
{
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_MEETING_FETCHED,wxCommandEventHandler( MainPanel::OnMeetingFetched), 0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_MEETING_EVENT, MeetingEventHandler(MainPanel::OnMeetingEvent), 0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_MEETING_ERROR, MeetingErrorHandler(MainPanel::OnMeetingError), 0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_DOCSHARE_EVENT, DocshareEventHandler(MainPanel::OnDocshareEvent), 0, this );
}

//*****************************************************************************
void MainPanel::OnMeetingFetched(wxCommandEvent& event)
{
    if( !CONTROLLER.IsOpcode( event.GetString(), MeetingCenterController::OP_MainPanel ) )
    {
        // This event is not for us
        event.Skip( );
        return;
    }

    // Grab the meeting detail pointer
    meeting_details_t   mtg     = (meeting_details_t) event.GetClientData();
    invitee_iterator_t  iter;
    invitee_t           invt;

    iter = meeting_details_iterate( mtg );

    wxString    strHostName;
    while( (invt = meeting_details_next(mtg, iter)) != NULL )
    {
        if( invt->role == RollHost )
        {
            m_strHostName.Empty( );
            m_strHostName.Append( wxString( invt->name, wxConvUTF8 ) );
            break;
        }
    }

    meeting_details_destroy( mtg );
    m_fMeetingFetched = true;
}


//*****************************************************************************
void MainPanel::OnMeetingEvent(MeetingEvent& event)
{
    int         iEv = event.GetEvent( );
    wxString    strMID( event.GetMeetingId( ) );

    event.Skip( );
    
    if (strMID != m_strMeetingID)
        return;
    
    if( iEv == AppShareStarted)
    {
        DisplayPage(1);
    }
    else if( iEv == AppShareEnded)
    {
        DisplayPage(0);
    }
    else if( iEv == MeetingWaiting )
    {
        wxLogDebug( wxT("in MainPanel::OnMeetingEvent( event ==  MeetingWaiting )") );

        // MeetingWaiting event not sent unless user is eligible to start meeting
        meeting_t mtg = controller_find_meeting( CONTROLLER.controller, strMID.mb_str(wxConvUTF8) );
        if( mtg  ==  NULL )
        {
            wxString    strStatus( event.GetSubId( ) );
            long        lStatus     = 0;
            strStatus.ToLong( &lStatus );

            DisplayMeetingSummary( mtg, lStatus );
        }
        else
        {
            participant_t       me              = meeting_find_me( mtg );
            if( !m_fMeetingHasStarted )
            {
                if( me  !=  NULL )
                {
                    if( m_pWaitDlg )
                        m_pWaitDlg->Destroy( );
                    m_pWaitDlg = new WaitForStartDialog( this, 0 );
                    m_pWaitDlg->SetMID( strMID );
                    m_pWaitDlg->Connect( XRCID("wxID_OK"),     wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&MainPanel::OnWaitDialogOK, 0, this);
                    m_pWaitDlg->Connect( XRCID("wxID_CANCEL"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&MainPanel::OnWaitDialogCancel, 0, this);

                    //  NOTE:  This dialog is not modal because I don't want to block execution while
                    //  we are attempting to reconnect.  Also, if connection is re-established, this
                    //  dialog will be hidden by either SessionIsReconnected( ) or OnConnectEvent( ), above.
                    m_pWaitDlg->Show( true );
                }
                wxString    strStatus( event.GetSubId( ) );
                long        lStatus     = 0;
                strStatus.ToLong( &lStatus );

                DisplayMeetingSummary( mtg, lStatus );
            }
        }
    }
    else if( iEv == MeetingJoined )
    {
        wxLogDebug( wxT("in MainPanel::OnMeetingEvent( event ==  MeetingJoined )") );

        meeting_t mtg = controller_find_meeting( CONTROLLER.controller, strMID.mb_str(wxConvUTF8) );
        DisplayMeetingSummary( mtg, 0 );
    }
    else if( iEv  ==  MeetingStarted )
    {
        wxLogDebug( wxT("in MainPanel::OnMeetingEvent( event ==  MeetingStarted )") );

        m_fMeetingHasStarted = true;
        ((MeetingMainFrame*)GetParent( ))->ShowChatAndPart( );

        if( m_HTMLWindow )
            m_HTMLWindow->SetPage( m_strHTML );
        if( m_pWaitDlg )
            m_pWaitDlg->Destroy( );
        m_pWaitDlg = NULL;

        FetchPictures( strMID );

        this->GetParent( )->Show( );
        this->GetParent( )->SetFocus( );

        meeting_t mtg = controller_find_meeting( CONTROLLER.controller, strMID.mb_str(wxConvUTF8) );
		participant_t p = meeting_find_me(mtg);
		bool isModerator = p && (p->roll == RollHost || p->roll == RollModerator);
		bool isPresenter = p && (p->status & StatusPresenter);
        m_pDocSharePanel->SetPresenter(isPresenter, isModerator);		
    }
    else if( iEv == PresenterGranted )
    {
        meeting_t mtg = controller_find_meeting( CONTROLLER.controller, strMID.mb_str(wxConvUTF8) );
		participant_t p = meeting_find_me(mtg);
		bool isModerator = p && (p->roll == RollHost || p->roll == RollModerator);
        m_pDocSharePanel->SetPresenter(true, isModerator);
    }
    else if( iEv == PresenterRevoked )
    {
        meeting_t mtg = controller_find_meeting( CONTROLLER.controller, strMID.mb_str(wxConvUTF8) );
		participant_t p = meeting_find_me(mtg);
		bool isModerator = p && (p->roll == RollHost || p->roll == RollModerator);
        m_pDocSharePanel->SetPresenter(false, isModerator);
    }

}


//*****************************************************************************
void MainPanel::OnDocshareEvent(DocshareEvent& event)
{
    event.Skip( );
    switch( event.GetEvent( ) )
    {
        case DocShareStarted:
            m_fDocShareInProgress = true;
            m_docShareHightlight = 0;
            break;
        case DocShareEnded:
            m_fDocShareInProgress = false;
            m_docShareHightlight = 0;
            break;
        case DocShareConfigure:
        case ParticipantJoined:
            break;
        case DocShareDownloadPage:
        case DocShareDrawing:
            m_fDocShareInProgress = true;
            if (m_pNotebook->GetSelection() != 2) 
            {
                m_docShareHightlight = 5;
                m_pNotebook->Refresh();
                m_timer.Start(1000);
            }
            break;
    }
}


//*****************************************************************************
void MainPanel::DisplayPage( int iPage )
{
    wxLogDebug( wxT("Selecting meeting page %d"), iPage );
    if( iPage >= 0  &&  iPage <= 2 )
    {
        m_pNotebook->SetSelection( iPage );
    }
}


//*****************************************************************************
bool MainPanel::IsHighlighted( wxString& tab, wxRect& pos )
{
    if (tab == _("Presentation/Document Share"))
    {
        m_docShareTabPos = pos;
        return m_fDocShareInProgress && ((m_docShareHightlight & 1) == 1);
    }
    return false;
}


//*****************************************************************************
void MainPanel::ConnectionLost( )
{
    wxLogDebug( wxT("entering MainPanel::ConnectionLost( )") );
}


//*****************************************************************************
void MainPanel::ConnectionReconnected( )
{
    wxLogDebug( wxT("entering MainPanel::ConnectionReconnected( )") );
}


//*****************************************************************************
void MainPanel::OnWaitDialogOK( wxCommandEvent& event )
{
    wxLogDebug( wxT("entering MainPanel::OnWaitDialogOK( )") );

    wxString strProjectCode;
    ProjectCodeDialog dlg(this);
    dlg.GetProjectCode(strProjectCode);

    wxString    strMID = m_pWaitDlg->GetMID( );
    int         iRet;
    iRet = controller_meeting_start( CONTROLLER.controller, strMID.mb_str( wxConvUTF8 ) );

    if( m_pWaitDlg )
        m_pWaitDlg->Destroy( );
    m_pWaitDlg = NULL;

    this->GetParent( )->Show( );

    if (!strProjectCode.IsEmpty())
    {
        wxString info = wxT("project: ") + strProjectCode;
        controller_log_event(CONTROLLER.controller, m_strMeetingID.mb_str(), info.mb_str(wxConvUTF8));
    }
}


//*****************************************************************************
void MainPanel::OnWaitDialogCancel( wxCommandEvent& event )
{
    wxLogDebug( wxT("entering MainPanel::OnWaitDialogCancel( )") );

    m_pWaitDlg->Disconnect( XRCID("wxID_OK"),     wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&MainPanel::OnWaitDialogOK,     0, this);
    m_pWaitDlg->Disconnect( XRCID("wxID_CANCEL"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&MainPanel::OnWaitDialogCancel, 0, this);

    if( m_pWaitDlg )
        m_pWaitDlg->Destroy( );
    m_pWaitDlg = NULL;

}


//*****************************************************************************
void MainPanel::OnMeetingError(MeetingError& event)
{
    wxLogDebug( wxT("entering MainPanel::OnMeetingError( ) - error = %d  --  MID = %s  --  PID = %s  --  Type = %s  --  Message = %s"),
                event.GetError( ), event.GetMeetingId( ).wc_str( ), event.GetParticipantId( ).wc_str( ), event.GetType( ).wc_str( ), event.GetMessage( ).wc_str( ) );

    event.Skip();

    // We'll assume something went wrong even if error code is 0
    int err = event.GetError();
    if (err == 0)
        err = Failed;

    if (event.GetError( )  ==  ERR_CONNECTLOST)
    {
        // Handled by meeting controller
        return;
    }

    //  Show the status message in the status bar or meeting info tab
    if (this->m_fMeetingHasStarted)
    {
        wxString msg;
        CUtils::GetServerStatusMsg( event.GetError(), msg );
        ((MeetingMainFrame *)GetParent( ))->SetStatusText( msg );
    }
    else
    {        
        if( event.GetError( )  ==  InvalidPassword )
        {
            wxCommandEvent  ev( wxEVT_GET_PASSWORD, wxID_ANY );
            CONTROLLER.AddPendingEvent( ev );
        }
        else
            DisplayMeetingSummary(NULL, event.GetError());
    }
}


//*****************************************************************************
void MainPanel::OnStopSharing( wxCommandEvent& event )
{

}


//*****************************************************************************
void MainPanel::OnFileManagerDone( wxCommandEvent& event )
{
    m_fPicturesFetched = true;

    wxLogDebug(wxT("MainPanel::OnFileManagerDone - meeting pictures loaded"));

    wxString pictures;
    DisplayMeetingPictures( pictures );
    m_strHTML.Replace( wxT("MEETING_PICTURES"), pictures );
    
    if( m_HTMLWindow && m_fMeetingHasStarted )
        m_HTMLWindow->SetPage( m_strHTML );
}


//*****************************************************************************
void MainPanel::OnTabChange( wxAuiNotebookEvent& event )
{
    wxLogDebug(wxT("Tab change"));
    if (event.GetSelection() == 2) 
    {
        m_docShareHightlight = 0;
    }
}


//*****************************************************************************
void MainPanel::OnTimer(wxTimerEvent& event)
{
    if (--m_docShareHightlight <= 1)
    {
        m_timer.Stop();
    }
    m_pNotebook->Refresh();  
}


//*****************************************************************************
void MainPanel::DisplayMeetingSummary( meeting_t mtg, int iStatus )
{
    wxString    strTitle;
    wxString    strHostName;
    wxString    strDialInNone( _("none"), wxConvUTF8 );
    wxString    strDialInNumber;
    bool        fVoiceAllowed   = false;
    wxString    strMeetingID;
    wxString    strPassword;
    wxString    strDateTime;
    wxString    strPrivacy;
    wxString    strModerator;
    wxString    strDesc;
    wxString    strInvite;
    wxString    strYes( _("yes"), wxConvUTF8 );
    wxString    strNo( _("no"), wxConvUTF8 );
    wxString    strHeaderLink = wxString::FromAscii( wxGetApp().Options().GetSystem( "HeaderLink", "http://www.apreta.org" ) );
    wxString    strRoll;
    
    wxString    strHTMLWaiting;
    wxString    strPin;

    wxString    strStatusMsg;

    if( iStatus != 0 )
        CUtils::GetServerStatusMsg( iStatus, strStatusMsg );

    wxString strPhoneInfo(OPTIONS.GetSystem("PhoneInfo", ""), wxConvUTF8);
    
    //  Read the HTML from disk
    wxString    strName( wxGetApp().GetResourceFile(wxT("res/mtgdesc.html")) );
    if( wxGetApp( ).m_pLocale  !=  NULL )
    {
        wxString    strT( wxGetApp( ).m_pLocale->GetCanonicalName( ) );
        strT.Prepend( wxT("locale/") );
        strT.Append( wxT("/mtgdesc.html") );
        strT = wxGetApp().GetResourceFile( strT );
        if( ::wxFileExists( strT ) )
        {
            strName.Empty( );
            strName.Append( strT );
        }
    }

    meeting_details_t   details     = NULL;

    if (mtg != NULL)
    {
        details = controller_get_meeting_details( CONTROLLER.controller, mtg, 0, NULL );
        participant_t       me              = meeting_find_me( mtg );
        if( me  !=  NULL )
        {
            strPin = wxString(me->part_id, wxConvUTF8);
            if (strPin.Find( wxT(':')) > 0)
                strPin = strPin.AfterFirst( wxT(':') );
            strModerator.Append( (me->roll == RollModerator || me->roll == RollHost) ? strYes : strNo );
			switch (me->roll) {
				case RollHost:	strRoll = _("Host"); break;
				case RollModerator: strRoll = _("Moderator"); break;
				default: strRoll = _("Guest"); break;
			}
        }

        strTitle.Append( wxString( details->title, wxConvUTF8 ) );
//        MeetingView::GetHostName( details, strHostName );

        strDialInNumber.Append( wxString( details->bridge_phone, wxConvUTF8 ) );
        fVoiceAllowed = (details->options & InvitationDataOnly) ? false : true;
        strMeetingID.Append( wxString( details->meeting_id, wxConvUTF8 ) );
        strPassword.Append( wxString( details->password, wxConvUTF8 ) );
        strDateTime.Append( wxString( CONTROLLER.DateTimeDecode( details->start_time, MeetingCenterController::LongDateTime ), wxConvUTF8 ) );
        strPrivacy.Append( wxString( CONTROLLER.privacyText( details->privacy ), wxConvUTF8 ) );
        strDesc.Append( wxString( details->description, wxConvUTF8 ) );
        strInvite.Append( wxString( details->invite_message, wxConvUTF8 ) );
        
        if (strDialInNumber.IsEmpty() || strDialInNumber == wxT("none")) {
            strDialInNumber = strDialInNone;
        }
        
        if (!strPhoneInfo.IsEmpty()) {
            strPhoneInfo.Replace(wxT("MEETING_ID"), strMeetingID);
            strDialInNumber.Append(wxT("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"));
            strDialInNumber.Append(strPhoneInfo);
        }
    }

    wxFile  fileHTML;
    if( !fileHTML.Open( strName ) )
    {
        MeetingView::CreateReport( details, m_strHTML, this, strPin, strModerator, strStatusMsg );
        if( !m_fMeetingHasStarted )
        {
            if( strStatusMsg.IsEmpty( ) )
                strStatusMsg.Append( wxString( _("Waiting for meeting to start"), wxConvUTF8 ) );
            MeetingView::CreateReport( details, strHTMLWaiting, this, strPin, strModerator, strStatusMsg );
        }
    }
    else
    {
        int     iLen;
        iLen = fileHTML.Length( );

        char*   lpBuf       = new char[ iLen+1 ];
        memset( lpBuf, 0, iLen+1 );
        int     iBytesRead;
        iBytesRead = fileHTML.Read( lpBuf, iLen );
        if( iBytesRead  <=  iLen )
            lpBuf[ iBytesRead ] = 0;
        else
            lpBuf[ iLen ] = 0;
        fileHTML.Close( );

        wxString    strRaw( (const char *) lpBuf, wxConvUTF8 );
        delete [] lpBuf;

        //  Placeholders embedded into the raw HTML
        //            HEADER_LINK
        //            WAITING2START
        //            MTG_TITLE
        //            HOST_NAME
        //            DIAL_IN_LABEL
        //            DIAL_IN_NUMBER
        //            MEETING_ID
        //            MY_PIN
        //            PASSWORD_VALUE
        //            TIME_DATE_STRING
        //            PRIVACY_VALUE
        //            MODERATOR_VALUE
        //            MEETING_DESC
        //            MEETING_INVITE

        m_strHTML.Clear( );
        m_strHTML.Append( strRaw );
        m_strHTML.Replace( wxT("MTG_TITLE"), strTitle );

        // Parse meeting pictures arguments (currently just background color) before 
        // entering message loop
        int pos = m_strHTML.find( wxT("MEETING_PICTURES(") );
        if (pos != wxString::npos)
        {
            pos += 16;
            int end = m_strHTML.find( wxT(")"), pos);
            if (end != wxString::npos)
            {
                m_strPictureArg = m_strHTML.substr(pos+1, end-pos-1);
                m_strHTML.erase(pos, pos-end+1);
            }
        }

        if( !m_fMeetingFetched )
        {
            wxStopWatch sw;
            long        start_time = sw.Time();
            long        timeout_ms = 5000;
            while( !m_fMeetingFetched )
            {
                CUtils::MsgWait( 200, 1 );
#ifdef WIN32
				// ToDo: find better fix
				// Prevents recursive call to Yield during startup on Linux
				// when meeting parameters are specified on command line.
                wxGetApp().Yield();
#endif
                if( (sw.Time() - start_time) > timeout_ms )
                {
                    wxLogDebug( wxT("MainPanel::DisplayMeetingSummary( ) -- Timeout while waiting for m_strHostName") );
                    break;
                }
            }
        }

        if ( mtg != NULL  && m_strHostName.IsEmpty() )
        {
            participant_t host_part = meeting_find_host(mtg);
            if( host_part != NULL )
            {
                m_strHostName = wxString(host_part->name, wxConvUTF8);
            }
        }

        m_strHTML.Replace( wxT("HEADER_LINK"), strHeaderLink );

        m_strHTML.Replace( wxT("HOST_NAME"), m_strHostName );

        wxString    strDialInLabel( _("Dial In #: "), wxConvUTF8 );
        m_strHTML.Replace( wxT("DIAL_IN_LABEL"), strDialInLabel );

        m_strHTML.Replace( wxT("DIAL_IN_NUMBER"), (fVoiceAllowed) ? strDialInNumber : strDialInNone );

        m_strHTML.Replace( wxT("MEETING_ID"), strMeetingID );
        m_strHTML.Replace( wxT("MY_PIN"), strPin );

        m_strHTML.Replace( wxT("PASSWORD_VALUE"), strPassword.IsEmpty( ) ? _("none") : _("Yes") );
        m_strHTML.Replace( wxT("TIME_DATE_STRING"), strDateTime );
        m_strHTML.Replace( wxT("PRIVACY_VALUE"), strPrivacy );

        m_strHTML.Replace( wxT("MODERATOR_VALUE"), strModerator );
        m_strHTML.Replace( wxT("ROLE"), strRoll );

        strDesc.Replace( wxT("\n\r"), wxT("<BR>") );
        strDesc.Replace( wxT("\r\n"), wxT("<BR>") );
        strDesc.Replace( wxT("\n"),   wxT("<BR>") );
        strDesc.Replace( wxT("\r"),   wxT("<BR>") );
        strDesc.Replace( wxT("<CR>"), wxT("<BR>") );        //  Classic Zon embeds these
        m_strHTML.Replace( wxT("MEETING_DESC"), strDesc );

        strInvite.Replace( wxT("\n\r"), wxT("<BR>") );
        strInvite.Replace( wxT("\r\n"), wxT("<BR>") );
        strInvite.Replace( wxT("\n"),   wxT("<BR>") );
        strInvite.Replace( wxT("\r"),   wxT("<BR>") );
        strInvite.Replace( wxT("<CR>"), wxT("<BR>") );        //  Classic Zon embeds these
        m_strHTML.Replace( wxT("MEETING_INVITE"), strInvite );
        
        if (m_fPicturesFetched)
        {
            wxString pictures;
            DisplayMeetingPictures( pictures );
            m_strHTML.Replace( wxT("MEETING_PICTURES"), pictures );
        }

        if( !m_fMeetingHasStarted )
        {
            strHTMLWaiting.Clear( );
            strHTMLWaiting.Append( m_strHTML );

            if( strStatusMsg.IsEmpty( ) )
                strStatusMsg.Append( wxString( _("Waiting for meeting to start"), wxConvUTF8 ) );
            strHTMLWaiting.Replace( wxT("WAITING2START"),
                                    wxString::Format( _("<tr><td colspan=\"7\" align=\"center\"><FONT face=\"Arial, Helvetica, sans-serif\" color=\"#FF0000\" size=\"5\"><strong>%s</strong></font></td></tr><tr><td colspan=\"7\"><hr /></td></tr>"),
                strStatusMsg.wc_str( ) ) );

            strHTMLWaiting.Replace(wxT("MEETING_PICTURES"), wxT(""));
        }

        m_strHTML.Replace( wxT("WAITING2START"), wxT("") );
        fileHTML.Close( );
    }

    if( mtg != NULL  &&  details != NULL )
    {
        meeting_details_destroy( details );
    }

    wxLogDebug( wxT("MainPanel::DisplayMeetingSummary( ) -- Updating meeting info") );

    if( m_HTMLWindow )
        m_HTMLWindow->SetPage( m_fMeetingHasStarted ? m_strHTML : strHTMLWaiting );
}


//*****************************************************************************
void MainPanel::FetchPictures( wxString mtgId )
{
    if (m_fileManager.get() == NULL)
    {
        m_fileManager.reset( new FileManager( FileManager::GetPathForMeeting(mtgId), FileManager::InMeeting ) );
        m_fileManager->LoadAsync( wxT("meetingPictures"), this, false );
    }    
}


//*****************************************************************************
void MainPanel::DisplayMeetingPictures( wxString& html )
{
    if (m_fileManager->GetFileCount() < 1)
    {
        html.Empty();
        return;
    }
    
    int i;
    html = wxT("<table width=100% cellspacing=4 bgcolor=BGCOLOR><tr>");

    for (i=0; i < m_fileManager->GetFileCount(); i++)
    {
        if (i > 1 && i % 5 == 0)
            html.Append(wxT("</tr><tr>"));
        FileDetails *file = m_fileManager->GetFile(i);
        DisplayPicture(file, html, i);
    }
    
    html.Append(wxT("<td width=10%>&nbsp;</td></tr></table>"));

    html.Replace(wxT("BGCOLOR"), m_strPictureArg);
}

//*****************************************************************************
void MainPanel::DisplayPicture( FileDetails * file, wxString& html, int index )
{
    wxString item = 
        _("<td width=18% align=center valign=top>"
            "<img align=center src='IMAGE_FILE'/>"
            "<p align=center><font size=3>TITLE</font></p>"
          "</td>");

    item.Replace(wxT("IMAGE_FILE"), file->m_LocalPath);
    item.Replace(wxT("TITLE"), file->m_Title);
    item.Replace(wxT("INDEX"), wxString::Format(wxT("%d"), index));
    
    html.Append(item);
}

//*****************************************************************************
void MainPanel::OnLinkClicked( wxHtmlLinkEvent& event )
{
    wxString href = event.GetLinkInfo().GetHref();
    if ( href.StartsWith(wxT("http")) )
        wxLaunchDefaultBrowser( event.GetLinkInfo().GetHref() ); 
    else
        event.Skip();
}

//*****************************************************************************
void MainPanel::OnHTMLClicked( wxHtmlCellEvent& event )
{
    wxHtmlLinkInfo *info = event.GetCell()->GetLink();
    if (info != NULL && info->GetHref().StartsWith(wxT("http")))
        wxLaunchDefaultBrowser( info->GetHref() );
    else
        event.Skip();
}
