/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef _CONTACTTREELIST_H_
#define _CONTACTTREELIST_H_

#include "imagelistcomposite.h"
#include "addressbk.h"
#include "wx/treelistctrl.h"
#include <wx/listbox.h>
#include "contactsearch.h"
#include "events.h"
#include "services/common/common.h"

///////////////////////////////////////////////////////////////

/**
  * Common node data.
  */
class ContactCommonTreeItemData : public wxTreeItemData
{
public:
    ContactCommonTreeItemData( int nType ):
    m_nType(nType),
    m_nStatus(0)
    {
    }
    ContactCommonTreeItemData( const ContactCommonTreeItemData & from ):
    m_nType(from.m_nType),
    m_nStatus(from.m_nStatus)
    {
    }

    int m_nType;            /**< One of the NodeTypes */
    int m_nStatus;          /**< One of the NodeStatus values */
};

/**
  * This class manages the user specified contact list display properties.<br>
  * There are two user defined display properties related to contacts:<br>
  * 1) The displayed columns.<br>
  * 2) The contact name display.<br>
  * <br>
  * NOTE: Column zero of the ContactTreeList will always be Name. This allows
  * the mix of Address Books, Groups, and Contacts to share a common descriptive column.
  */
class ContactTreeList;
class ContactDisplayManager : public wxEvtHandler
{
public:
    /** Constructor - Sets up the default display preferences */
    ContactDisplayManager();

    /** Contact record field index values */
    enum ContactFieldIndexValues
    {
        Contact_AddressBook = 0,
        Contact_Title,
        Contact_First,
        Contact_Middle,
        Contact_Last,
        Contact_Suffix,
        Contact_Company,
        Contact_JobTitle,
        Contact_Address1,
        Contact_Address2,
        Contact_State,
        Contact_Country,
        Contact_PostalCode,
        Contact_BusinessPhone,
        Contact_HomePhone,
        Contact_OtherPhone,
        Contact_Email1,
        Contact_Email2,
        Contact_Email3,
        Contact_MSNScreen,
        Contact_YIMScreen,
        Contact_AIMScreen,
        Contact_ScreenName,

        MaxContactFieldIndex    /**< This MUST BE LAST */
    };


    /**
      * GUI Helper function:<br>
      * Populate a wxListBox with the entries in the m_AvailableList.
      * @param listbox Ptr. to the target listbox.
      * @return none.
      */
    void PopulateAvailableListBox( wxListBox *listbox );

    /**
      * GUI Helper function:<br>
      * Populate a wxListBox with the entries in the m_DisplayList.
      * @param listbox Ptr. to the target listbox.
      * @return none.
      */
    void PopulateDisplayListBox( wxListBox *listbox );

    /**
      * GUI Helper function:<br>
      * Process the Move from displayed to available operation.
      * @param displayListbox The source list box.
      * @param availableListbox The destination list box.
      * @return bool Returns true if successful.
      */
    bool ProcessMoveFromDisplayed( wxListBox *displayedListbox, wxListBox *availableListbox );

    /**
      * GUI Helper function:<br>
      * Process the Move from displayed to available operation.
      * @param availableListbox The source list box.
      * @param displayListbox The destination list box.
      * @return bool Returns true if successful.
      */
    bool ProcessMoveToDisplayed( wxListBox *availableListbox, wxListBox *displayedListbox );

    /**
      * GUI Helper function:<br>
      * Process a move request.
      * @param displayListbox The displayed item listbox.
      * @param fMoveUp Flag: If true then move selected items up. If false then move selected items down.
      * @return bool Returns true if the selected items were moved.
      */
    bool ProcessMove( wxListBox * displayListbox, bool fMoveUp );

    /**
      * GUI Helper function:<br>
      * Update the column headers on a contact tree list.
      * @param treelist The target ContactTreeList instance.
      * @return bool Returns true if successful. Returns false on failure.
      */
    bool UpdateColumnHeadings( ContactTreeList & treelist );

    /**
      * GUI Helper function:<br>
      * Update the column text for the specified contact.
      */
    bool UpdateColumnDisplay( ContactTreeList & treelist, wxTreeItemId & item );

private:
    /** Initialize the display lists to a default state */
    void InitializeDisplayList();

    /** Setup some default values */
    void SetDefaultValues();

    /** Append a contact field definition */
    void AddField( size_t ndx, const wxString & strDisplayName, int nWidth = -1 );

    /**
      * Add a field to the Displayed field list.
      * @param ndx The field index ( 0...MaxContactFieldIndex-1 )
      * @return bool Returns true if the index was added.
      */
    bool AddToDisplayed( size_t ndx );

    /**
      * Remove a field from the Displayed field list.
      * @param ndx The field index ( 0...MaxContactFieldIndex-1 )
      * @return bool Returns true if the index was removed.
      */
    bool RemoveFromDisplayed( size_t ndx );

    /** Given a display list index value, return the master index,
      */
    size_t DisplayToMasterIndex( size_t iSel );

    /** Given an available list index value, return the master index.
      */
    size_t AvailableToMasterIndex( size_t iSel );

    /** The master list of field index values */
    wxArrayInt m_MasterList;

    /** The master list of column header names */
    wxArrayString m_MasterDisplayNames;

    /** The master available state (1 == Available, 0 == Displayed) */
    wxArrayInt m_MasterAvailable;

    /** The master column widths */
    wxArrayInt m_MasterColumnWidths;

    /** The list of display index values */
    wxArrayInt *m_pDisplayList;
};

/** Each node in the contact list that references a group
  * has one of these objects attached to it.
  */
class GroupTreeItemData : public ContactCommonTreeItemData
{
public:
    GroupTreeItemData( int nType, group_t group );
    GroupTreeItemData( const GroupTreeItemData & from );
public:
    /** The source group data */
    group_t m_group;
};

/**
  * Each node in the contact list that references a contact
  * has one of these objects attached to it.
  */
class ContactTreeItemData : public ContactCommonTreeItemData
{
public:
    /** Constructors
      * @param nType The ContactTreeList::NodeType
      * @param contact The associated contact record
      * @param password The user entered password
      */
    ContactTreeItemData( int nType, contact_t contact );
    ContactTreeItemData( int nType, contact_t contact, const wxString & password );
    ContactTreeItemData( const ContactTreeItemData & from );
public:

    /** The source contact data */
    contact_t m_contact;

    /** The user entered password */
    wxString m_strPassword;
};

/**
  * Each search results node has one of these
  * objects attached to it.
  */
class ContactSearchResultsItemData : public ContactCommonTreeItemData
{
public:
    ContactSearchResultsItemData( int nType )
    :ContactCommonTreeItemData(nType),
    m_nSearchNumber(-1),
    m_dir(0),
    m_nStartingRow(0),
    m_nTotalRetrieved(0),
    m_fComplete(false)
    {
    }

public:
    /**
      * The search critera name.
      */
    wxString m_strSearchCriteriaName;

    /**
      * The search critera used to populate the node.<br>
      * This is created by MeetingSearch::Encode( wxString )
      */
    wxString m_strSearchCriteria;

    /**
      * The search number used to populate this node.
      */
    int m_nSearchNumber;

    /**
      * The directory.
      */
    directory_t m_dir;

    /**
      * The starting row used for incremental searches.
      */
    int m_nStartingRow;

    /**
      * The toal number of items retrieved.
      */
    int m_nTotalRetrieved;

    /**
      * The search is complete.
      */
    bool m_fComplete;
};

/**
  * This class handles the majority of the processing for a wxTreeListCtrl
  * used to display contact records. There are at lease two places int he product
  * that require the display and manipulation of contact trees.
  * <br>
  * Each instance of this class has a single wxTreeListCtrl instance.<br>
  **/
class ContactTreeList : public wxEvtHandler
{
public:
    ContactTreeList( wxTreeListCtrl *pTreeListCtrl = 0 );
    ~ContactTreeList( );

    /**
      * Display columns.
      */
    enum DisplayColumns
    {
        Col_Name = 0,
        Col_ScreenName,
        Col_BusinessPhone
    };

    enum EditImageNames
    {
        EditImageGroup = 0,
        EditImageContact,
        EditImageUserOld,
        EditImageDirectory,
        EditImageUser,
        EditImageBuddy
    };

    /** Tree node types:<br>
      * Node_Root is the top level node and is always hidden from view.<br>
      * Node_Directory contains Node_Group and/or Node_Contact nodes.<br>
      * Node_Group is always a child of Node_Directory.<br>
      * Node_Contact is a child Node_Group, Node_Directory, or Node_SearchResults.<br>
      * Node_SearchResults is always a child of Node_SearchRoot.<br>
      * The NodeTypes also server another purpose:<br>
      * When the user selectes multiple items in the tree, the selected
      * items produce a mask that identifies what's been selected.
      */
    enum NodeTypes
    {
        Node_Root =                 0x00000001, /**< The root node */
        Node_GlobalDir =            0x00000002, /**< Directory root node: CAB */
        Node_PersonalDir =          0x00000004, /**< Directory root node: PAB */
        Node_UserDir =              0x00000008, /**< User imported directory */
        Node_Group =                0x00000200, /**< Group root node */
        Node_Contact =              0x00000400, /**< A contact node */
        Node_SearchRoot =           0x00000800, /**< Search results top level root node */
        Node_SearchResults =        0x00001000  /**< Search results root node */
    };

    /**
      * Node status flags.
      */
    enum NodeStatus
    {
        Node_Status_NoChange =             0x00000000, /**< The node has not been changed since it was created */
        Node_Status_New =                  0x00000001, /**< The node is new for this sesstion */
        Node_Status_Changed =              0x00000002  /**< The node has been changed */
    };

    /**
      * Connect the tree list control pointer.
      */
    void SetTree( wxTreeListCtrl *pTreeListCtrl, wxString & strTreeName )
    {
        m_pTree = pTreeListCtrl;
        m_strTreeName = strTreeName;
    }

    /**
      * Setup the appropriate image list
      */
    void SetUseEditorImageList( bool f )
    {
        m_fUseEditorImageList = f;
    }

    /**
      * Return the tree list control pointer.
      */
    wxTreeListCtrl * Tree()
    {
        return m_pTree;
    }


    /**
      * Remove contacts from tree.
      */
    void RemoveContacts();

    /**
      * Add a top level contact directory.
      * @param title The title text.
      * @param contact_dir The directory containing the contacts.
      * @return wxTreeItemId Returns the tree item ID for the new node.
      */
    wxTreeItemId AppendDirectoryNode( const wxString & title, directory_t contact_dir, int dirType, bool preview = false );

    /** Append the search results root node
      * @return wxTreeItemId Returns the tree item ID for the new node.
      */
    wxTreeItemId AppendSearchResultsRoot();

    /** Append a new contact node.
      * @param dir The directory that will contain the new node.
      * @param contact The contact record.
      * @param pContactData The node data for the new entry.
      * @param password The user enetered password
      * @return wxTreeItemId Return the id of the new contact node.
      */
    wxTreeItemId AppendContact( directory_t dir, contact_t contact );
    wxTreeItemId AppendContact( directory_t dir, contact_t contact, const wxString & password );
    wxTreeItemId AppendContact( directory_t dir, const wxTreeItemId & group_node, ContactTreeItemData *pContactData );

    /** Append a new group node
      * @param dir The cirectory that will contain the new group
      * @param group The group record.
      * @return wxTreeItemId Returns the id of the new group node.
      */
    wxTreeItemId AppendGroup( directory_t dir, group_t group );

    /**
      * Return the node type for the specified item
      * @param item The wxTreeItemId of the target item.
      * @return int Returns the NodeType of the item if successful. Returns wxNOT_FOUND on failure.
      */
    int GetNodeType( wxTreeItemId & item )
    {
        int nRet = wxNOT_FOUND;
        if( Tree() )
        {
            ContactCommonTreeItemData *pCommonData = (ContactCommonTreeItemData *)Tree()->GetItemData( item );
            nRet = pCommonData->m_nType;
        }
        return nRet;

    }

    /**
      * Set the status for a node.
      * @param id The node to change
      * @param status The new status
      */
    void SetNodeStatus( wxTreeItemId & item , int status )
    {
        ContactCommonTreeItemData *pData = (ContactCommonTreeItemData *)m_pTree->GetItemData( item );
        if( pData )
        {
            pData->m_nStatus = status;
        }
    }
    int GetNodeStatus( wxTreeItemId & item )
    {
        int nRet = Node_Status_NoChange;
        ContactCommonTreeItemData *pData = (ContactCommonTreeItemData *)m_pTree->GetItemData( item );
        if( pData )
        {
            nRet = pData->m_nStatus;
        }
        return nRet;
    }

    /**
      * Return true if the specified item is a directory root node.
      * @param item The wxTreeItemId of the target item.
      * @return bool Returns true if the item NodeType is Node_Directory. Returns false for all other items.
      */
    bool IsDirectory( wxTreeItemId & item )
    {
        if( !item.IsOk() )
            return false;

        int nType = GetNodeType( item );

        int nDirMask = Node_GlobalDir | Node_PersonalDir | Node_UserDir;

        if( nType & nDirMask )
        {
            return true;
        }
        return false;
    }

    /**
      * Return true if the specified item is a directory group node.
      * @param item The wxTreeItemId of the target item.
      * @return bool Returns true if the item NodeType is Node_Directory. Returns false for all other items.
      */
    bool IsGroup( wxTreeItemId & item )
    {
        if( !item.IsOk() )
            return false;

        int nType = GetNodeType( item );
        if( nType & Node_Group )
        {
            return true;
        }
        return false;
    }

    /**
      * Return true if the specified item is within the global directory.
      * @param item The wxTreeItemId of the target item.
      * @return bool Returns true if the item NodeType is Node_Directory. Returns false for all other items.
      */
    bool IsInGlobalDir( wxTreeItemId & item )
    {
        if( !item.IsOk() )
            return false;
        if( GetNodeType(item) == Node_Contact )
        {
            ContactTreeItemData *pCData = (ContactTreeItemData *)m_pTree->GetItemData( item );
            return (pCData->m_contact->directory->type == dtGlobal );
        }else if( GetNodeType(item) == Node_Group )
        {
            GroupTreeItemData *pGData = (GroupTreeItemData *)m_pTree->GetItemData( item );
            return (pGData->m_group->directory->type == dtGlobal);

        }
        return false;
    }

    /**
      * Return true if the specified item is within the personal directory.
      * @param item The wxTreeItemId of the target item.
      * @return bool Returns true if the item NodeType is Node_Directory. Returns false for all other items.
      */
    bool IsInPersonalDir( wxTreeItemId & item )
    {
        if( !item.IsOk() )
            return false;
        if( GetNodeType(item) == Node_Contact )
        {
            ContactTreeItemData *pCData = (ContactTreeItemData *)m_pTree->GetItemData( item );
            return (pCData->m_contact->directory->type == dtPersonal );
        }else if( GetNodeType(item) == Node_Group )
        {
            GroupTreeItemData *pGData = (GroupTreeItemData *)m_pTree->GetItemData( item );
            return (pGData->m_group->directory->type == dtPersonal);

        }
        return false;
    }

    /**
      * Return true if the specified item is within a search results set.
      * @param item The wxTreeItemId of the target item.
      * @return bool Returns true if the item is a Node_Contact within a Node_SearchResults set.
      */
    bool IsInSearchResults( wxTreeItemId & item )
    {
        bool fIsSearchResult = false;

        if( IsContact( item ) )
        {
            wxTreeItemId parent_id;
            parent_id = m_pTree->GetItemParent( item );

            if( GetNodeType( parent_id ) == Node_SearchResults )
            {
                fIsSearchResult = true;
            }
        }
        return fIsSearchResult;
    }

    /**
      * Return true if the specified node is a contact.
      */
    bool IsContact( wxTreeItemId & item )
    {
        if( GetNodeType( item ) & Node_Contact )
            return true;
        return false;
    }

    /**
      * Check to see if any contact nodes are selected.
      */
    bool HasContactsSelected();

    /**
      * Start a contact search.
      * @param criteria The search criteria to use.
      */
    void StartContactSearch( const ContactSearch & criteria );

    /**
      * Start a contact search.
      * @param id The tree item id of the search parent node.
      */
    void StartContactSearch( const wxTreeItemId & id );

    /**
      * Edit the currently selected search criteria.
      * @return bool Returns the prior flag state.
      */
    bool EditCurrentSearch( bool f );

    /**
      * Start a contact search at the currently selected node.
      */
    void StartContactSearch()
    {
        StartContactSearch( m_pTree->GetSelection() );
    }

    /**
      * Return the search node data for the currently selected node.
      * @return ContactSearchResultsItemData * Returns a pointer to the node data if found. Return zero on failure.
      */
    ContactSearchResultsItemData * GetSearchNode();

    /**
      * Return the search node data for the specified node.
      * @param id The tree item id.
      * @return ContactSearchResultsItemData * Returns a pointer to the node data if found. Return zero on failure.
      */
    ContactSearchResultsItemData * GetSearchNode( const wxTreeItemId & id );

    /** Process a directory fetched event */
    void ProcessOnDirectoryFetched(wxCommandEvent& event);

    /**
      * Return the next search number and update the count.
      */
    int NextSearchNumber()
    {
        return m_nSearchNumber++;
    }

    /**
      * Remove the current search node.
      */
    void RemoveSearch();

    /** Get tree selection information.
      * @param selections Output: Reference to the wxArrayTreeItemIds array that will hold the selections.
      * @param nSelections Output: Reference to the number of elements in the selections array.
      * @return int Returns the selection mask.
      */
    int GetSelectionInfo( wxArrayTreeItemIds & selections, size_t & nSelections );

    /** Count the number of Buddies in the specified selections.
      * @param selections Reference to the array of selected items.
      * @return size_t Returns the number of buddy list items in the selections array.
      */
    size_t GetBuddyCount( wxArrayTreeItemIds & selections );

    /** Attempt to locate the Community Address Book - All Group.
      * @return group_t Returns a pointer to the CAB - All Group if found. Returns zero if not found.
      */
    group_t GetCABAllGroup( wxTreeItemId & id );

    /** Attempt to locate the Personal Address Book - All Group.
      * @return group_t Returns a pointer to the CAB - All Group if found. Returns zero if not found.
      */
    group_t GetPABAllGroup( wxTreeItemId & id );

    /** Attempt to locate the users Buddies group.
      * @return group_t Returns a pointer to the buddies group if found. Returns zero if not found.
      */
    group_t GetBuddiesGroup( wxTreeItemId & id );

    /** Attempt to loacate a contact record within the buddies group.
      * @param contact Ptr. to the target contact record.
      * @return wxTreeItemId Returns the ID of the tree node if found. Returns an invalid Id if not found.
      */
    wxTreeItemId FindBuddyNode( contact_t contact );

    /** Attempt to locate a contact via it's userid
      * @param userid The target userid.
      * @return contact_t Returns a Ptr. to the contact record if foune. Returns zero if not found.
      */
    contact_t FindContact( const wxString & userid );

    /** Remove all of the nodes that reference a contact
      * @param parent The parent node to traverse.
      * @param contact Ptr. to the contact recrod.
      * @return int Returns the number of Node_Contact items removed.
      */
    int RemoveContactNodes( contact_t contact );
    void RemoveContactNodes( wxTreeItemId parent, contact_t contact, int & count );

    /** Refresh the display for the specified node.<br>
      * Called when:<br>
      * A new contact or group node has been added to the tree.<br>
      * After a contact or group node has been edited.<br>
      * <br>
      * NOTE: The UpdateDisplay methods having the contact parameter are used to
      * traverse the entire tree in order to update all instances of the contact record.
      *
      * @param item The node that was changed.
      * @param contact The contact record that was changed.
      * @param parent The parent node to be searched.
      * @return none.
      */
    void UpdateDisplay( wxTreeItemId & item );
    void UpdateDisplay( contact_t contact );
    void UpdateDisplay( wxTreeItemId parent, contact_t contact );

    /** Process Expanded/Collapsed nodes */
    void OnItemCollapsed(wxTreeEvent& event);
	void OnItemExpanded(wxTreeEvent& event);

    /** Construct an options key name from the specified node
      * @param item The target node
      * @return wxString Returns the node's key name
      */
    wxString GetNodeKeyName( wxTreeItemId & item );

    /** Expand the specified node if required */
    void ExpandNodeIfRequired( wxTreeItemId & item );

    /** Get list of user directories (e.g. outlook) that have been added to the tree. */
    const wxArrayTreeItemIds& GetUserDirectories( )
    {
        return m_DirectoryIds;
    }
    
    /** Find user directory with specified name */
    wxTreeItemId FindUserDirectory( const wxString& name );

private:
    /** Initialize the tree control */
    void Initialize();

    /** Handle presence event */
    void OnPresenceEvent( PresenceEvent& event );

    void SetPresence( wxTreeItemId parent );

    void PrepareSearchValue( wxString & strValue );


protected:
    /** Flag: The tree has been initialized */
    bool m_fInitialized;


    wxTreeListCtrl *m_pTree;            /**< The target tree list control */
    wxString m_strTreeName;             /**< The user specified tree name. This is used for saving state data */
    wxTreeItemId m_RootId;              /**< The root node of the m_pTree control */
    wxTreeItemId m_CABId;               /**< The Community Address Book root node */
    wxTreeItemId m_PABId;               /**< The Personal Address Book root node */
    wxTreeItemId m_CommunitySearchId;   /**< The search results parent node */
    int m_nSearchNumber;                /**< The last known search number */
    bool m_fEditingSearchCriteria;      /**< Flag: The current search criteria is being edited */

    wxArrayTreeItemIds m_DirectoryIds;

    /** Image lists for the tree */
    bool m_fUseEditorImageList;
    int m_StatusOnline;
    int m_StatusPhone;
    int m_StatusContact;
    CImageListComposite m_StatusImages;

    /** The display manager (Handles column headings, and data display) */
    ContactDisplayManager m_ContactDisplayManager;

}; // ContactTreeList

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
  * This class holds a reference to data copied from the ContactTreeList.<br>
  * Only Node_Contact records are copied.<br>
  * Copied contact records are stored as an encoded string of field values.<br>
  * Pasted contact records become new contact records witha type of UserTypeContact<br>
  */
class ContactTreeListData
{
public:
    /** Constructor */
    ContactTreeListData() {}

    /** Destructor: Clears the arrays */
    virtual ~ContactTreeListData();

    /** Copy selected data from the ContactTreeList.
      * @param src The ContactTreeList that has the data being copied.
      * @param dst The destination string array.
      * @return int Returns the number of items selected.
      */
    virtual int CopySelectedData( ContactTreeList & src );
    virtual int CopySelectedData( ContactTreeList & src, wxArrayString & dst );

    /** Get the number of contacts copied to this instance.
      * @return size_t Returns the number of entries in the m_EncodedContact array.
      */
    size_t GetContactCount()
    {
        return m_EncodedContact.GetCount();
    }

    /** Create a contact record from the specified encoded entry.
      * @param ndx The encoded contact index ( 0..GetContactCount()-1 )
      * @param src The string array containing the encoded contact records.
      * @return contact_t Returns the contact record created from the encoded data.
      */
    virtual contact_t CreateContact( directory_t dir, size_t ndx, bool fCreatingBuddy = false );
    virtual contact_t CreateContact( directory_t dir, wxArrayString src, size_t ndx, bool fCreatingBuddy = false );

    /**
      * See if there is any data available.
      * @return bool Returns true if there are any entries int the m_EncodedContact array.
      */
    bool HasClipboardData()
    {
        return (GetContactCount() > 0);
    }

private:
    /** Encode a contact record into a string of field values.<br>
      * Encoded format: "field|field>|...."<br>
      * Where field is the contact.field content
      * @param contact The source contact record.
      */
    wxString EncodeContactRecord( contact_t contact );

    /** Append another field to the end of the encoded string.
      * @param pfield Ptr. to the contact field
      * @param strEncoded Input/Output Ref to the encoded string.
      * @param fAppendDelimiter If true then the delimiter characater is appended after the field data.
      * @return none
      */
    void AppendField( pstring pfield, wxString & strEncoded, bool fAppendDelimiter = true );

    /** Decode a string of contact field values into a new contact record
      * @param strFields The encoded contact record.
      */
    virtual contact_t DecodeContactRecord( directory_t dir, const wxString & strEncoded, bool fCreatingBuddy = false );

    /** Allocate a pstring
      * @param strField The field value
      * @return pstring Returns a pointer to the new string value.
      */
    pstring allocField( directory_t dir, const wxString & strField );


public:
    /** An array of encoded contact_t records. */
    wxArrayString m_EncodedContact;
};

#endif
