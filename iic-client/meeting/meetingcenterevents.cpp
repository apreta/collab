/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"
#include "meetingcentercontroller.h"

#include "base64.h"
#include "iicdoc.h"
#include "cryptlib/cryptlib.h"


#include "services/common/eventnames.h"


////////////////////////////////////////////////////////////////////////////////////////
// Convert session events to wxEvents...
// AddPendingEvent is thread safe; the event will get serialied back to the UI thread.

void MeetingCenterController::on_session_error(void *u, int err, const char *msg, int code, const char *id, const char *resource)
{
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxString    strMsg( msg, wxConvUTF8 );
//    wxLogDebug( wxT("entering MeetingCenterController::on_session_error( ) - err: %d  --  msg:  ***%s***"), err, strMsg.wc_str( ) );
    SessionErrorEvent ev(wxID_ANY, err, msg, id, resource);
    frame->AddPendingEvent(ev);
}

void MeetingCenterController::on_connected(void *u)
{
//    wxLogDebug( wxT("entering MeetingCenterController::on_connected( )") );
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxCommandEvent ev(wxEVT_SESSION_CONNECTED, wxID_ANY);
    frame->AddPendingEvent(ev);
}

void MeetingCenterController::on_controller_registered(void *u)
{
//    wxLogDebug( wxT("entering MeetingCenterController::on_controller_registered( )") );
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxCommandEvent ev(wxEVT_CONTROLLER_REGISTERED, wxID_ANY);
    frame->AddPendingEvent(ev);
}

void MeetingCenterController::on_controller_ping(void *u)
{
//    wxLogDebug( wxT("entering MeetingCenterController::on_controller_ping( )") );
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxCommandEvent ev(wxEVT_CONTROLLER_PING, wxID_ANY);
    frame->AddPendingEvent(ev);
}

void MeetingCenterController::on_meeting_event(void *u, meeting_event_t evt)
{
//    wxLogDebug( wxT("entering MeetingCenterController::on_meeting_event( )") );
    MeetingCenterController *frame = (MeetingCenterController*)u;
    MeetingEvent ev(wxID_ANY, evt->event, evt->meeting_id, evt->sub_id, evt->participant_id);
    frame->AddPendingEvent(ev);
}

void MeetingCenterController::on_docshare_event(void *u, docshare_event_t evt)
{
    wxString    strT( event_names[ evt->event ], wxConvUTF8 );
    wxLogDebug( wxT("entering MeetingCenterController::on_docshare_event( )  --  event = %s (%d)  --  options = %d"), strT.wc_str( ), evt->event, evt->options );
    MeetingCenterController *frame = (MeetingCenterController*)u;

    std::string s       = evt->drawing;
    const char  *inbuf  = s.c_str( );
    int         length  = strlen( inbuf ) + 1;

    char* decodebuf = new char[ length ];
    char* output    = new char[ length ];

    memset( decodebuf, 0, length );
    memset( output, 0, length );

    // base 64 decode
    CBase64     base64Encoder;
    base64Encoder.Decode( inbuf, decodebuf );

    // decrypt results
    CBlowFish   blowfish;
    blowfish.Initialize( BLOWFISH_KEY, sizeof(BLOWFISH_KEY) );
    blowfish.Decode( (unsigned char *)decodebuf, (unsigned char *)output, length-1 );

    DocshareEvent       ev( wxID_ANY, evt->event, evt->meeting_id, evt->presenter_id, evt->doc_id, evt->page_id, output, evt->options );

    wxString    decryptedValue;
    decryptedValue = wxString( output, wxConvUTF8 );
    ev.SetDrawing( decryptedValue );

    frame->AddPendingEvent( ev );

    delete[] decodebuf;
    delete[] output;
}

void MeetingCenterController::on_meeting_error(void *u, meeting_error_t evt)
{
    wxString    strType( evt->type,    wxConvUTF8 );
    wxString    strMsg(  evt->message, wxConvUTF8 );
    wxLogDebug( wxT("entering MeetingCenterController::on_meeting_error( )  --  type = %s,   message = %s"), strType.wc_str( ), strMsg.wc_str( ) );
    MeetingCenterController *frame = (MeetingCenterController*)u;
    MeetingError ev(wxID_ANY, evt->error, evt->meeting_id, evt->participant_id, evt->type, evt->message);
    frame->AddPendingEvent(ev);
}


void MeetingCenterController::on_meeting_chat(void *u, meeting_chat_t evt)
{
//    wxLogDebug( wxT("entering MeetingCenterController::on_meeting_chat( )") );
    MeetingCenterController *frame = (MeetingCenterController*)u;
    MeetingChatEvent ev(wxID_ANY, evt->meeting_id, evt->message, evt->sent_to, evt->from_participant_id, evt->from_name, evt->from_role, evt->time );
    frame->AddPendingEvent(ev);
}


void MeetingCenterController::on_submeeting_add(void *u, const char *opid, const char *sub_id)
{
//    wxLogDebug( wxT("entering MeetingCenterController::on_submeeting_add( )") );
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxCommandEvent ev(wxEVT_SUBMEETING_ADDED, wxID_ANY);
    wxString    *pstrT;
    pstrT = new wxString( wxString(sub_id,wxConvUTF8));
    ev.SetClientData( pstrT );
    ev.SetString(wxString(opid,wxConvUTF8));
    frame->AddPendingEvent(ev);
}


void MeetingCenterController::on_get_params(void *u, meeting_details_t details, int forum_options)
{
    wxLogDebug( wxT("entering MeetingCenterController::on_get_params( ) - forum_options is %d"), forum_options );
    wxString    strMeetingID( details->meeting_id, wxConvUTF8 );
    wxString    strTitle( details->title, wxConvUTF8 );
    wxLogDebug( wxT("  meeting_id = ***%s***   title = ***%s***"), strMeetingID.wc_str( ), strTitle.wc_str( ) );


    invitee_iterator_t iter;
    invitee_t invt;

    iter = meeting_details_iterate( details );
    wxLogDebug( wxT("  ScreenName  --  Name  --  PartID  --  UserID") );
    while ((invt = meeting_details_next( details, iter)) != NULL)
    {
        wxString    strScreen( invt->screenname, wxConvUTF8 );
        wxString    strName( invt->name, wxConvUTF8 );
        wxString    strPID( invt->participant_id, wxConvUTF8 );
        wxString    strUserID( invt->user_id, wxConvUTF8 );
        wxLogDebug( wxT("  %s  --  %s  --  %s  --  %s"), strScreen.wc_str( ), strName.wc_str( ), strPID.wc_str( ), strUserID.wc_str( ) );
    }

    //  The real work happens here...
    //  The result of this event is the same as wxEVT_MEETING_FETCHED to just use that event.
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxCommandEvent ev(wxEVT_MEETING_FETCHED, wxID_ANY);
    ev.SetClientData( details );
    ev.SetInt( forum_options );

    wxString strOpid;
    strOpid = MeetingCenterController::opcode( MeetingCenterController::OP_EditMeeting );
    ev.SetString( strOpid );
    frame->AddPendingEvent( ev );

}

void MeetingCenterController::on_controller_invite_lookup( void *u, meeting_invite_t invite )
{
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxCommandEvent ev(wxEVT_CONTROLLER_INVITE_LOOKUP, wxID_ANY);
    ev.SetClientData( invite );
    frame->AddPendingEvent( ev );
}

void MeetingCenterController::on_controller_invite( void *u, meeting_invite_t invite )
{
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxCommandEvent ev(wxEVT_CONTROLLER_INVITE, wxID_ANY);
    ev.SetClientData( invite );
    frame->AddPendingEvent( ev );
}

void MeetingCenterController::on_controller_im_notify( void *u, const char *mid, participant_t part )
{
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxCommandEvent ev(wxEVT_CONTROLLER_IM_NOTIFY, wxID_ANY);
    ev.SetClientData( part );
    ev.SetString( wxString( mid, wxConvUTF8 ));
    frame->AddPendingEvent( ev );
}

void MeetingCenterController::on_addressbk_auth(void *u)
{
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxCommandEvent ev(wxEVT_ADDRESSBK_AUTH, wxID_ANY);
    frame->AddPendingEvent(ev);
}

void MeetingCenterController::on_addressbk_progress(void *u, const char *opid, int num_fetched, int total)
{
    MeetingCenterController *frame = (MeetingCenterController*) u;
    wxCommandEvent          ev( wxEVT_ADDRESSBK_PROGRESS, wxID_ANY );
    ev.SetString( wxString( opid, wxConvUTF8 ) );
    ev.SetInt( num_fetched );
    frame->AddPendingEvent( ev );
}

void MeetingCenterController::on_schedule_fetched(void *u, const char *opid, meeting_list_t list)
{
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxCommandEvent ev(wxEVT_SCHEDULE_FETCHED, wxID_ANY);
    ev.SetClientData(list);
    ev.SetString(wxString(opid,wxConvUTF8));
    frame->AddPendingEvent(ev);
}

void MeetingCenterController::on_meeting_fetched(void *u, const char *opid, meeting_details_t mtg)
{
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxCommandEvent ev(wxEVT_MEETING_FETCHED, wxID_ANY);
    ev.SetClientData(mtg);
    ev.SetString(wxString(opid,wxConvUTF8));
    frame->AddPendingEvent(ev);
}

void MeetingCenterController::on_directory_fetched(void *u, const char *opid, directory_t dir)
{
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxCommandEvent ev(wxEVT_DIRECTORY_FETCHED, wxID_ANY);
    ev.SetClientData(dir);
    ev.SetString(wxString(opid,wxConvUTF8));
    frame->AddPendingEvent(ev);
}

void MeetingCenterController::on_schedule_progress(void *u, const char *opid, int num_fetched, int total )
{
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxCommandEvent ev(wxEVT_SCHEDULE_PROGRESS, wxID_ANY);
    ev.SetInt( num_fetched );
    ev.SetExtraLong( total );
    ev.SetString(wxString(opid,wxConvUTF8));
    frame->AddPendingEvent(ev);
}

void MeetingCenterController::on_schedule_updated(void  *u, const char *opid, int status, const char *meetingid )
{
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxCommandEvent ev(wxEVT_SCHEDULE_UPDATED, wxID_ANY);

    wxString strOpcode( opid, wxConvUTF8 );
    wxString strId( meetingid, wxConvUTF8 );
    wxString str;
    str = strOpcode;
    str += wxT("|");
    str += strId;

    ev.SetInt( status );
    ev.SetString(str);

    frame->AddPendingEvent(ev);
}

void MeetingCenterController::on_schedule_error(void *u, const char *opid, int code, const char *msg )
{
//    wxLogDebug( wxT("entering MeetingCenterController::on_schedule_error( )") );
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxCommandEvent ev(wxEVT_SCHEDULE_ERROR, wxID_ANY);

    // Send: OPCODE|ERROR TEXT for now.
    // Need a new event type someday....PLF
    wxString strOpcode( opid, wxConvUTF8 );
    wxString strMsg( msg, wxConvUTF8 );
    wxString str;
    str = strOpcode;
    str += wxT("|");
    str += strMsg;

    ev.SetInt( code );
    ev.SetString(str);

    frame->AddPendingEvent(ev);
}

void MeetingCenterController::on_addressbk_update_failed( void *u, const char *opid, int status, const char *msg)
{
    MeetingCenterController *frame = (MeetingCenterController *)u;
    wxCommandEvent ev(wxEVT_ADDRESSBK_UPDATE_FAILED, wxID_ANY);

    // Send: OPCODE|ERROR TEXT for now.
    // Need a new event type someday....PLF
    wxString strOpcode( opid, wxConvUTF8 );
    wxString strMsg( msg, wxConvUTF8 );
    wxString str;
    str = strOpcode;
    str += wxT("|");
    str += strMsg;

    ev.SetInt( status );
    ev.SetString(str);

    frame->AddPendingEvent(ev);
}

void MeetingCenterController::on_address_updated(void *u, const char *opid, const char *itemid)
{
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxCommandEvent ev(wxEVT_ADDRESS_UPDATED, wxID_ANY);
    wxString Opcode( opid, wxConvUTF8 );
    wxString ItemId( itemid, wxConvUTF8 );
    wxString str;
    str = Opcode;
    str += wxT("|");
    str += ItemId;
    ev.SetString( str );
    frame->AddPendingEvent(ev);
}

void MeetingCenterController::on_address_deleted(void *u, const char *opid, const char *itemid)
{
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxCommandEvent ev(wxEVT_ADDRESS_DELETED, wxID_ANY);
    wxString Opcode( opid, wxConvUTF8 );
    wxString ItemId( itemid, wxConvUTF8 );
    wxString str;
    str = Opcode;
    str += wxT("|");
    str += ItemId;
    ev.SetString( str );
    frame->AddPendingEvent(ev);
}

void MeetingCenterController::on_profile_fetched(void *u, const char *opid)
{
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxCommandEvent ev(wxEVT_PROFILE_FETCHED, wxID_ANY);
    wxString Opcode( opid, wxConvUTF8 );
    ev.SetString( Opcode );
    frame->AddPendingEvent(ev);
}

void MeetingCenterController::on_addressbk_version_check( void *u, int upgrade_type, const char *url )
{
    MeetingCenterController *frame = (MeetingCenterController*) u;
    wxCommandEvent          ev(wxEVT_ADDRESS_VERSION_CHECK, wxID_ANY);
    wxString                strURL( url, wxConvUTF8 );

    ev.SetInt( upgrade_type );
    ev.SetString( strURL );
    frame->AddPendingEvent( ev );
}

void MeetingCenterController::on_addressbk_recording_list(void *u, const char *opid, recording_list_t reclist)
{
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxCommandEvent ev(wxEVT_RECORDINGS_FETCHED, wxID_ANY);
    ev.SetClientData(reclist);
    ev.SetString(wxString(opid,wxConvUTF8));
    frame->AddPendingEvent(ev);
}

void MeetingCenterController::on_addressbk_document_list(void *u, const char *opid, document_list_t reclist)
{
    MeetingCenterController *frame = (MeetingCenterController*)u;
    wxCommandEvent ev(wxEVT_DOCUMENTS_FETCHED, wxID_ANY);
    ev.SetClientData(reclist);
    ev.SetString(wxString(opid,wxConvUTF8));
    frame->AddPendingEvent(ev);
}

void MeetingCenterController::on_im_event( void *u, messaging_im_t msg )
{
    wxLogDebug( wxT("entering MeetingCenterController::on_im_event( )") );

    wxString    user( msg->user, wxConvUTF8 );
    wxString    body( msg->body, wxConvUTF8 );
    wxLogDebug( wxT("    user = %s  --  body = %s"), user.wc_str( ), body.wc_str( ) );

    IMChatEvent ev( wxID_ANY, msg->user, msg->server, msg->resource, msg->body, msg->composing );
    MeetingCenterController *frame = ( MeetingCenterController* ) u;
    frame->AddPendingEvent( ev );
    
    messaging_im_destroy(msg);
}

void MeetingCenterController::on_presence_event(void *u, messaging_presence_t pres)
{
    wxLogDebug( wxT("entering MeetingCenterController::on_presence_event( )") );

    wxString    user( pres->user, wxConvUTF8 );
    wxString    status( pres->status, wxConvUTF8 );
    wxLogDebug( wxT("    user = %s  --  presence = %d  --  status = %s"), user.wc_str( ), pres->presence, status.wc_str( ) );

    int special = PresenceInfo::SpecialNone;
    if (status.Left(4).Cmp(wxT("iic:")) == 0)
    {
        if (status.Find(wxT("voice")) != wxNOT_FOUND)
        {
            special |= PresenceInfo::SpecialPhone;
        }
        if (status.Find(wxT("meeting")) != wxNOT_FOUND)
        {
            special |= PresenceInfo::SpecialMeeting;
        }
    }

    // The presence manager can be called from a background thread
    MeetingCenterController *frame = ( MeetingCenterController* ) u;
    frame->Presence().SetPresence(_T("zon"), user.wc_str(), pres->presence, status.wc_str(), special);
    
    messaging_presence_destroy(pres);
}

void MeetingCenterController::on_im_invite(void *user, const char *id, const char *method, resultset_t result)
{
    wxLogDebug( wxT("entering MeetingCenterController::on_im_invite( )") );

    char *mid, *title, *descrip, *message, *pid, *name;
    resultset_get_output_values(result, "(ssssss)", &mid, &title, &descrip, &message, &pid, &name);

    IMInviteEvent   ev( wxID_ANY, mid, title, descrip, message, pid, name );
    MeetingCenterController *frame = ( MeetingCenterController* ) user;
    frame->AddPendingEvent( ev );
}

void MeetingCenterController::on_conversion_result(void *user, const char *id, resultset_t result)
{
    int status;
	resultset_get_output_values(result, "(i)", &status);

    wxCommandEvent ev( wxEVT_CONVERSION_EVENT, wxID_ANY );
    ev.SetString(wxT("ConversionComplete"));
    ev.SetInt(status);
    MeetingCenterController *frame = ( MeetingCenterController* ) user;
    frame->AddPendingEvent( ev );
}

void MeetingCenterController::on_conversion_error(void *user, const char *id, int code, const char *msg)
{
    wxCommandEvent ev( wxEVT_CONVERSION_EVENT, wxID_ANY );
    ev.SetString(wxT("ConversionFailed"));
    ev.SetInt(code);
    MeetingCenterController *frame = ( MeetingCenterController* ) user;
    frame->AddPendingEvent( ev );
}

void MeetingCenterController::on_conversion_event(void *user, const char *id, const char *method, resultset_t result)
{
    wxLogDebug( wxT("entering MeetingCenterController::on_conversion_event( )") );

    char *name;
    int arg1;
    resultset_get_output_values(result, "(si)", &name, &arg1);

    wxCommandEvent ev( wxEVT_CONVERSION_EVENT, wxID_ANY );
    ev.SetString(wxString(name, wxConvUTF8));
    ev.SetInt(arg1);
    MeetingCenterController *frame = ( MeetingCenterController* ) user;
    frame->AddPendingEvent( ev );
}
