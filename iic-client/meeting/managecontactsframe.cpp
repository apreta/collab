/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 *
 *
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */
#include "app.h"
#include "meetingcentercontroller.h"

#include "managecontactsframe.h"

#include "findcontactsdialog.h"
#include "createcontactdialog.h"
#include "profilemanagerdialog.h"
#include "aboutdialog.h"
#include "contactservice.h"

#include <wx/textdlg.h>

//(*InternalHeaders(ManageContactsFrame)
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(ManageContactsFrame)
//*)

BEGIN_EVENT_TABLE(ManageContactsFrame,wxFrame)
	//(*EventTable(ManageContactsFrame)
	//*)
END_EVENT_TABLE()

ManageContactsFrame::ManageContactsFrame(wxWindow* parent, ManageContactsFrame **ppThis, wxWindowID id):
m_nCmdMask(Cmd_Buddies_AddByScreen|Cmd_User_New|Cmd_Contact_New|Cmd_Group_New),
m_nState(0),
m_MainMenuBar(0),
m_EditPopup(0),
m_AdminPopup(0),
m_SearchMenu(0),
m_MeetingsMenu(0),
m_HelpMenu(0),
m_SearchPopup(0),
m_MainPopup(0),
m_NewPopup(0),
m_BuddiesPopup(0),
m_nDatabaseError(0),
m_ppThis(ppThis),
m_Dir(0)
{
	//(*Initialize(ManageContactsFrame)
	wxXmlResource::Get()->LoadObject(this,parent,_T("ManageContactsFrame"),_T("wxFrame"));
	m_ContactsTreeListCtrl = (wxTreeListCtrl*)FindWindow(XRCID("ID_CONTACTS_TREELISTCTRL"));
	m_NewButton = (wxButton*)FindWindow(XRCID("ID_NEW_BUTTON"));
	m_EditButton = (wxButton*)FindWindow(XRCID("ID_EDIT_BUTTON"));
	m_DeleteButton = (wxButton*)FindWindow(XRCID("ID_DELETE_BUTTON"));
	m_SearchButton = (wxButton*)FindWindow(XRCID("ID_SEARCH_BUTTON"));
	m_ContactsPanel = (wxPanel*)FindWindow(XRCID("ID_CONTACTS_PANEL"));
	
	Connect(XRCID("ID_CONTACTS_TREELISTCTRL"),wxEVT_COMMAND_TREE_ITEM_RIGHT_CLICK,(wxObjectEventFunction)&ManageContactsFrame::OnContactsTreeListCtrlItemRightClick);
	Connect(XRCID("ID_CONTACTS_TREELISTCTRL"),wxEVT_COMMAND_TREE_SEL_CHANGED,(wxObjectEventFunction)&ManageContactsFrame::OnContactsTreeListCtrlSelectionChanged);
	Connect(XRCID("ID_SEARCH_BUTTON"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&ManageContactsFrame::OnSearchButtonClick);
	//*)
    m_ContactsTreeListCtrl->SetFont( wxGetApp().GetAppDefFont() );

	Initialize();
}

ManageContactsFrame::~ManageContactsFrame()
{
	//(*Destroy(ManageContactsFrame)
	//*)

    CONTROLLER.Disconnect(wxID_ANY, wxEVT_SESSION_ERROR,           SessionErrorEventHandler(ManageContactsFrame::OnSessionError),       0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_ADDRESSBK_UPDATE_FAILED, wxCommandEventHandler(ManageContactsFrame::OnAddressbkUpdateFailed), 0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_ADDRESS_UPDATED,         wxCommandEventHandler(ManageContactsFrame::OnAddressUpdated),        0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_DIRECTORY_FETCHED,       wxCommandEventHandler(ManageContactsFrame::OnDirectoryFetched),      0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_DIRECTORY_STARTED,       wxCommandEventHandler(ManageContactsFrame::OnDirectoryStarted),      0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_ADDRESS_DELETED,         wxCommandEventHandler(ManageContactsFrame::OnAddressDeleted),        0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_PROFILE_FETCHED,         wxCommandEventHandler(ManageContactsFrame::OnProfileFetched),        0, this );
}

void ManageContactsFrame::Initialize()
{
    ::wxBeginBusyCursor( );

    if (CONTROLLER.IMClientMode())
    {
        m_strCaption = PRODNAME + _T(" - ");
        m_strCaption += CONTROLLER.GetDisplayName();
    }
    else if( CONTROLLER.AdministratorMode() )
    {
        m_strCaption = _("Manage Contacts and Users");
    }else
    {
        m_strCaption = _("Manage Contacts");
    }
    SetTitle( m_strCaption );

    CONTROLLER.Connect(wxID_ANY, wxEVT_SESSION_ERROR,           SessionErrorEventHandler(ManageContactsFrame::OnSessionError),       0, this );
    CONTROLLER.Connect(wxID_ANY, wxEVT_ADDRESSBK_UPDATE_FAILED, wxCommandEventHandler(ManageContactsFrame::OnAddressbkUpdateFailed), 0, this );
    CONTROLLER.Connect(wxID_ANY, wxEVT_ADDRESS_UPDATED,         wxCommandEventHandler(ManageContactsFrame::OnAddressUpdated),        0, this );
    CONTROLLER.Connect(wxID_ANY, wxEVT_DIRECTORY_FETCHED,       wxCommandEventHandler(ManageContactsFrame::OnDirectoryFetched),      0, this );
    CONTROLLER.Connect(wxID_ANY, wxEVT_DIRECTORY_STARTED,       wxCommandEventHandler(ManageContactsFrame::OnDirectoryStarted),      0, this );
    CONTROLLER.Connect(wxID_ANY, wxEVT_ADDRESS_DELETED,         wxCommandEventHandler(ManageContactsFrame::OnAddressDeleted),        0, this );
    CONTROLLER.Connect(wxID_ANY, wxEVT_PROFILE_FETCHED,         wxCommandEventHandler(ManageContactsFrame::OnProfileFetched),        0, this );

    //  Start the fetch of the Profiles, so we have their names when needed
    wxString strOpid;
    strOpid = MeetingCenterController::opcode( MeetingCenterController::OP_ManageContacts, -1 );
    wxCharBuffer opid = strOpid.mb_str();

    addressbk_fetch_profiles(CONTROLLER.AddressBook(),opid);

    // Initialize the contacts tree list control
    // m_ContactTree.SetUseEditorImageList( true );
    wxString strTreeName(wxT("MC"));
    m_ContactTree.SetTree( m_ContactsTreeListCtrl, strTreeName );
    m_ContactTree.Tree()->SetSortColumn(0);

    // Create the main frame menu
    m_MainMenuBar = new wxMenuBar();

    // Create a popup menu to hold the Exit menu item (wxWidgets won't allow menu items on the wxMenuBar)
    m_ManageContactsPopup = new wxMenu();
    m_ManageContactsPopup->Append( Contact_New, _("New contact..."), _("Create a new personal contact") );
    m_ManageContactsPopup->Append( Group_New, _("New group..."), _("Create a new group") );
	m_ManageContactsPopup->Append( Contact_Refresh, _("Refresh"), _("Refresh contact information.") );
    m_ManageContactsPopup->AppendSeparator();
    m_ManageContactsPopup->Append( Edit_Copy, _("Copy"), _("Copy the selected contacts to the clipboard") );
    m_ManageContactsPopup->Append( Edit_Paste, _("Paste"), _("Paste contacts on the clipboard into the selected list") );
    m_ManageContactsPopup->Append( Edit_Delete, _("Delete"), _("Delete the selected contacts or buddies") );
    m_ManageContactsPopup->Append( Edit_Edit, _("Edit selected..."), _("Edit the selected contact information.") );
    m_ManageContactsPopup->Append( Edit_RemoveFromGroup, _("Remove from group"), _("Remove the selected contacts form their associated group.") );
    m_ManageContactsPopup->AppendSeparator();
    m_ManageContactsPopup->Append( wxID_CLOSE, _("Close manage contacts"), _("Close the manage contacts window.") );
    if( CONTROLLER.IMClientMode() )
        m_ManageContactsPopup->Append( wxID_EXIT, _("Exit"), _("Exit the application"), wxITEM_NORMAL );
    m_MainMenuBar->Append( m_ManageContactsPopup, _("Contacts") );

    //////////////////////////////////////////////////////////////////////////
    m_AdminPopup = new wxMenu(); 
    m_AdminPopup->Append( User_New, _("New user..."), _("Create a new user") );
    m_AdminPopup->Append( Edit_EmailAccountInfo, _("Email account information") );
    m_AdminPopup->AppendSeparator();
    m_AdminPopup->Append( Profiles_Manage, _("Manage policies...") );
    if( CONTROLLER.AdministratorMode() )
    {
        m_MainMenuBar->Append( m_AdminPopup, _("Admin") );
    }

    //////////////////////////////////////////////////////////////////////////
    m_NewPopup = new wxMenu();
    if( CONTROLLER.AdministratorMode() )
    {
        m_NewPopup->Append( User_New, _("New user..."), _("Create a new user") );
    }
    m_NewPopup->Append( Contact_New, _("New contact..."), _("Create a new personal contact") );
    m_NewPopup->Append( Group_New, _("New group..."), _("Create a new group") );

    //////////////////////////////////////////////////////////////////////////
    m_BuddiesPopup = new wxMenu();
    m_BuddiesPopup->Append( Buddies_Add, _("Add to personal buddies"), _("Add selected contacts to you buddies list") );
    m_BuddiesPopup->Append( Buddies_AddByScreen, _("Add buddy by screen name..."), _("Add a buddy by entering the screen name") );
    m_BuddiesPopup->Append( Buddies_Remove, _("Remove from personal buddies"), _("Remove selected entries from your buddies list") );
    m_MainMenuBar->Append( m_BuddiesPopup, _("Buddies") );

    // Options menu
	m_OptionsMenu = new wxMenu();
	m_OptionsMenu->Append( Options_SignOff, _("Sign off"), wxEmptyString, wxITEM_NORMAL );
	m_OptionsMenu->AppendSeparator();
	m_OptionsMenu->Append( wxID_PREFERENCES, _("Preferences..."), wxEmptyString, wxITEM_NORMAL );
	m_MainMenuBar->Append( m_OptionsMenu, _("Options") );

    // Create the search menu for the menu bar
    m_SearchMenu = new wxMenu();
    m_SearchMenu->Append( Search_New, _("Find contacts..."), _("Start a new search of the community address book"), wxITEM_NORMAL );
    m_SearchMenu->Append( Search_Continue, _("Find more"), _("Search for additional meetings"), wxITEM_NORMAL );
    m_SearchMenu->Append( Search_Remove, _("Remove search"), _("Remove these search results"), wxITEM_NORMAL );
    m_SearchMenu->AppendSeparator();
    m_SearchMenu->Append( Search_Edit, _("Edit search criteria"), _("Change the search critera and start the search again"), wxITEM_NORMAL );
    m_MainMenuBar->Append( m_SearchMenu, _("Search") );

    if( CONTROLLER.IMClientMode() )
    {
        m_MeetingsMenu = new wxMenu();
        m_MeetingsMenu->Append( Meeting_Center, _("Meeting center"), _("Show the meeting center window"), wxITEM_NORMAL );
        m_MeetingsMenu->AppendSeparator();
        m_MeetingsMenu->Append( Meeting_StartInstant, _("Start instant meeting..."), _("Start my instant meeting"), wxITEM_NORMAL );
        m_MeetingsMenu->Append( Meeting_Schedule, _("Schedule meeting..."), _("Schedule a new meeting"), wxITEM_NORMAL );
        m_MainMenuBar->Append( m_MeetingsMenu, _("Meetings") );
    }

    m_HelpMenu = new wxMenu();
	m_HelpMenu->Append( Help_Contents, _("Contents..."), _("View user manual in browser"), wxITEM_NORMAL );
    m_HelpMenu->AppendSeparator();
	m_HelpMenu->Append( Help_About, _("About..."), wxEmptyString, wxITEM_NORMAL );
	m_MainMenuBar->Append( m_HelpMenu, _("Help") );

    // Create the popup menu for the search nodes and attach the events
    m_SearchPopup = new wxMenu();
    m_SearchPopup->Append( Search_Continue, _("Find more"), _("Search for additional meetings"), wxITEM_NORMAL );
    m_SearchPopup->Append( Search_Remove, _("Remove search"), _("Remove these search results"), wxITEM_NORMAL );
    m_SearchPopup->AppendSeparator();
    m_SearchPopup->Append( Search_Edit, _("Edit search criteria"), _("Change the search critera and start the search again"), wxITEM_NORMAL );
    Connect( Search_New, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnSearchNew) );
    Connect( Search_Continue,wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnSearchContinue) );
    Connect( Search_Edit, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnSearchEdit) );
    Connect( Search_Remove, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnSearchRemove) );

    // Create the popup menu for non search nodes
    m_MainPopup = new wxMenu();
    if( CONTROLLER.IMClientMode() )
    {
        m_MainPopup->Append( IM_Send, _("Send IM..."), _("Send IM to user") );
        m_MainPopup->Append( Meeting_StartInstant, _("Start instant meeting..."), _("Start my instant meeting"), wxITEM_NORMAL );
        m_MainPopup->Append( Meeting_Schedule, _("Schedule meeting..."), _("Schedule a new meeting"), wxITEM_NORMAL );
        m_MainPopup->AppendSeparator();
    }
    if( CONTROLLER.AdministratorMode() )
    {
        m_MainPopup->Append( User_New, _("New user..."), _("Create a new user") );
    }
    m_MainPopup->Append( Contact_New, _("New contact..."), _("Create a new personal contact") );
    m_MainPopup->Append( Group_New, _("New group..."), _("Create a new group") );
    m_MainPopup->AppendSeparator();
    m_MainPopup->Append( Buddies_Add, _("Add to personal buddies"), _("Add selected contacts to you buddies list") );
    m_MainPopup->Append( Buddies_AddByScreen, _("Add buddy by screen name..."), _("Add a buddy by entering the screen name") );
    m_MainPopup->Append( Buddies_Remove, _("Remove from personal buddies"), _("Remove selected entries from your buddies list") );
    m_MainPopup->Append( Edit_Edit, _("Edit..."), _("Edit the selected contact or buddy entry") );
    m_MainPopup->AppendSeparator();
    m_MainPopup->Append( Edit_Copy, _("Copy"), _("Copy the selected contacts to the clipboard") );
    m_MainPopup->Append( Edit_Paste, _("Paste"), _("Paste contacts on the clipboard into the selected list") );
    m_MainPopup->Append( Edit_Delete, _("Delete"), _("Delete the selected contacts or buddies") );
    m_MainPopup->Append( Edit_RemoveFromGroup, _("Remove from group"), _("Remove the selected contacts form their associated group.") );

	Connect(XRCID("ID_CONTACTS_TREELISTCTRL"),wxEVT_COMMAND_LIST_COL_CLICK, (wxObjectEventFunction)&ManageContactsFrame::OnContactListColumnLeftClick);

    // Theese buttons are directly connected
	Connect(XRCID("ID_COPY_BUTTON"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&ManageContactsFrame::OnEditCopy);
	Connect(XRCID("ID_PASTE_BUTTON"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&ManageContactsFrame::OnEditPaste);
	Connect(XRCID("ID_EDIT_BUTTON"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&ManageContactsFrame::OnEditEdit);
	Connect(XRCID("ID_DELETE_BUTTON"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&ManageContactsFrame::OnEditDelete);

    // The New button displays a popup menu
	Connect( XRCID("ID_NEW_BUTTON"),wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&ManageContactsFrame::OnNewButtonClicked);
    Connect( User_New, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnUserNew) );
    Connect( Contact_New, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnContactNew) );
    Connect( Group_New, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnGroupNew) );

    Connect( Contact_Refresh, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnContactsRefresh) );

    // The Buddies button displays a popup mennu
	Connect( XRCID("ID_BUDDIES_BUTTON"),wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&ManageContactsFrame::OnBuddiesButtonClicked);
    Connect( Buddies_Add, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnBuddiesAdd) );
    Connect( Buddies_AddByScreen, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnBuddiesAddByScreen) );
    Connect( Buddies_Remove, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnBuddiesRemove) );
    Connect( IM_Send, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnIMSend) );

    Connect( Edit_Copy, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnEditCopy) );
    Connect( Edit_Paste, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnEditPaste) );
    Connect( Edit_Edit, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnEditEdit) );
    Connect( Edit_EmailAccountInfo, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnEditEmailAccountInfo) );
	Connect( Edit_Delete, wxEVT_COMMAND_MENU_SELECTED,wxCommandEventHandler(ManageContactsFrame::OnEditDelete) );
	Connect( Edit_RemoveFromGroup, wxEVT_COMMAND_MENU_SELECTED,wxCommandEventHandler(ManageContactsFrame::OnEditRemoveFromGroup) );

    Connect( Options_SignOff,wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnSignOffEvent) );

    Connect( Profiles_Manage, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnProfileManage) );

    Connect( Meeting_Center, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnMeetingCenter) );
    Connect( Meeting_StartInstant, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnMeetingStart) );
    Connect( Meeting_Schedule, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnMeetingSchedule) );

    Connect( Help_About, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnHelpAbout) );
    Connect( Help_Contents, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnHelpContents) );

    Connect( wxEVT_CLOSE_WINDOW, wxCloseEventHandler( ManageContactsFrame::OnClose ) );
    Connect( wxID_CLOSE, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnMenuClose) );
    Connect( wxID_EXIT, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ManageContactsFrame::OnExit) );

   // Track size events
    Connect( wxEVT_SIZE, (wxObjectEventFunction)&ManageContactsFrame::OnSize );
    Connect( wxEVT_MOVE, (wxObjectEventFunction)&ManageContactsFrame::OnMove );

    if( !wxGetApp().Options().LoadScreenMetrics( "ManageContactsFrame", *this ) )
    {
        wxSize size(800,600);
        wxRect defaultSize( GetPosition(), size );
        SetSize( defaultSize );
    }

    SetMenuBar( m_MainMenuBar );

    SetAvailableCommands( );

    ::wxEndBusyCursor( );
}

void ManageContactsFrame::ConnectionReconnected()
{
    m_ContactTree.RemoveContacts();
}

void ManageContactsFrame::DisplayContacts()
{
    m_ContactTree.RemoveContacts();
    
    Show();
    Raise();

    if (!CONTROLLER.WaitContactsLoaded(this, true))
    {
        return;
    }

    if( CONTROLLER.GetCAB() )
    {
        m_CommunityNode = m_ContactTree.AppendDirectoryNode( _("Community"), CONTROLLER.GetCAB(), dtGlobal );
        if( m_CommunityNode.IsOk() )
        {
            SortContacts( m_CommunityNode );
        }
    }

    if( CONTROLLER.GetPAB() )
    {
        wxString    strScreenName, strPassword, strCommunity;
        CONTROLLER.GetSignOnInfo( strScreenName, strPassword, strCommunity );
        if( strCommunity.IsEmpty( ) )
        {
            m_PersonalNode = m_ContactTree.AppendDirectoryNode( _("Personal"), CONTROLLER.GetPAB(), dtPersonal );
            if( m_PersonalNode.IsOk() )
            {
                SortContacts( m_PersonalNode, true /* true = Sort the Personal node and then it's children */ );
            }
        }
    }

    // Add user imported directories
    directory_t dir = CONTROLLER.Directories()->GetDirectoryFirst();
    while (dir)
    {
        wxString dirName(dir->name, wxConvUTF8);
        wxTreeItemId id = m_ContactTree.AppendDirectoryNode( dirName, dir, dtUser );
        SortContacts( id, true );
        
        dir = CONTROLLER.Directories()->GetDirectoryNext();
    }

    if (!m_SearchNode)
    {
        m_SearchNode = m_ContactTree.AppendSearchResultsRoot();
    }
}

void ManageContactsFrame::RefreshContacts()
{
    CONTROLLER.RefreshContacts(true, false);
    DisplayContacts();
}

void ManageContactsFrame::SetAvailableCommands()
{
    // get the items currently selected, return the number of items,
    // create the selection mask.
    wxArrayTreeItemIds selections;
    size_t nSelected;
    int nSelectionMask = m_ContactTree.GetSelectionInfo( selections, nSelected );

    m_nCmdMask = 0;

    if( CONTROLLER.GetPAB( ) )
        m_nCmdMask |= Cmd_Buddies_AddByScreen;

    if( CONTROLLER.AdministratorMode() )
        m_nCmdMask |= Cmd_User_New;

    m_nCmdMask |= Cmd_Group_New;

    if( nSelected == 0 )
    {
        // Nothing else enabled
    }else if( nSelected == 1 )
    {
        wxTreeItemId item;
        item = selections[0];

        if( (m_nState && State_HaveCopiedContacts) )
        {
            if( nSelectionMask == ContactTreeList::Node_Group )
            {
                if( m_ContactTree.IsInGlobalDir( item ) )
                {
                    if( CONTROLLER.AdministratorMode() )
                    {
                        m_nCmdMask |= Cmd_Edit_Paste;
                    }
                }
                else if( m_ContactTree.IsInPersonalDir( item ) )
                {
                    m_nCmdMask |= Cmd_Edit_Paste;
                }
            }
        }
        if( nSelectionMask == ContactTreeList::Node_Contact )
        {
            m_nCmdMask |= Cmd_Edit_Copy;

            ContactTreeItemData     *pCData     = (ContactTreeItemData *)m_ContactTree.Tree()->GetItemData( item );
            contact_t               contact     = pCData->m_contact;
            if( contact  &&  (contact->type == UserTypeUser || contact->type == UserTypeAdmin) )
                m_nCmdMask |= Cmd_Edit_EmailAccountInfo;

            if( CONTROLLER.GetPAB( ) )
            {
                size_t nBuddyCount = m_ContactTree.GetBuddyCount( selections );

                if( !contact->readonly )
                {
                    if( nBuddyCount == nSelected )
                    {
                        m_nCmdMask |= Cmd_Buddies_Remove;
                    }
                    else if( nBuddyCount == 0 )
                    {
                        m_nCmdMask |= Cmd_Buddies_Add;
                    }
                }
            }

            wxTreeItemId parent_node;
            parent_node = m_ContactTree.Tree()->GetItemParent( item );
            if( m_ContactTree.GetNodeType( parent_node ) == ContactTreeList::Node_Group )
            {
                GroupTreeItemData *pGData = (GroupTreeItemData *)m_ContactTree.Tree()->GetItemData( parent_node );
                if( !pGData->m_group->readonly && ( pGData->m_group->type == dtOther || pGData->m_group->type == dtBuddies ) )
                {
                    m_nCmdMask |= Cmd_Edit_RemoveFromGroup;
                }
            }
            m_nCmdMask |= Cmd_Edit_Edit;
        }
        if( nSelectionMask == ContactTreeList::Node_SearchResults )
        {
            m_nCmdMask |= Search_Edit;
            ContactSearchResultsItemData *pSrch = m_ContactTree.GetSearchNode();
            if( pSrch )
            {
                m_nCmdMask |= Cmd_Search_Remove;
                if( !pSrch->m_fComplete )
                {
                    m_nCmdMask |= Cmd_Search_Continue;
                }
            }
        }
    }else // Multiple items selected
    {
        if( nSelectionMask == ContactTreeList::Node_Contact )
        {
            // Multiple contacts selected....
            m_nCmdMask |= Cmd_Edit_Copy;

/* ToDo: check all contacts selected are from CAB before enabling buddies add
            if( CONTROLLER.GetPAB( ) )
            {
                size_t nBuddyCount = m_ContactTree.GetBuddyCount( selections );

                if( nBuddyCount == nSelected )
                {
                    m_nCmdMask |= Cmd_Buddies_Remove;
                }
                else if( nBuddyCount == 0 )
                {
                    m_nCmdMask |= Cmd_Buddies_Add;
                }
            }
*/

            wxTreeItemId item;
            item = selections[0];

            // If all of the selected contacts are in the buddies group or
            // another group
            wxTreeItemId parent_node = m_ContactTree.Tree()->GetItemParent( item );
            GroupTreeItemData *pGData = (GroupTreeItemData *)m_ContactTree.Tree()->GetItemData( parent_node );

            size_t nInTheSameGroup = 0;
            for( size_t i = 0; i < nSelected; ++i )
            {
                wxTreeItemId item_i, check_parent;
                item_i = selections[i];
                check_parent = m_ContactTree.Tree()->GetItemParent( item_i );
                if( parent_node == check_parent )
                {
                    ++nInTheSameGroup;
                }
            }
            if( !pGData->m_group->readonly && nInTheSameGroup == nSelected )
            {
                m_nCmdMask |= Cmd_Edit_RemoveFromGroup;
            }
        }
    }

    if( DeleteAllowed( false /* false = No error message displayed */ ) )
    {
        m_nCmdMask |= Cmd_Edit_Delete;
    }

    //  SuperAdmins have a non-empty Community name
    wxString    strScreenName, strPassword, strCommunity;
    CONTROLLER.GetSignOnInfo( strScreenName, strPassword, strCommunity );
    if( strCommunity.IsEmpty( ) )
    {
        m_nCmdMask |= Cmd_Contact_New;
        m_nCmdMask |= Cmd_Buddies;
    }
    else
    {
        //  Turn OFF all Cmd_Buddies_* flags in the mask
        int     i;
        i = Cmd_Buddies_Add | Cmd_Buddies_AddByScreen | Cmd_Buddies_Remove;
        i = ~i;
        m_nCmdMask = m_nCmdMask & i;
    }

    SetUIFeedback();
}

void ManageContactsFrame::SetUIFeedback()
{
    int m = m_nCmdMask;

    m_NewButton->Enable( (m & Cmd_Contact_New) || (m & Cmd_Group_New) );

    m_EditButton->Enable( m & Cmd_Edit_Edit );
    m_DeleteButton->Enable( m & Cmd_Edit_Delete );

    //m_EmailAccountInfoButton->Enable( m & Cmd_Edit_EmailAccountInfo );

    int     iBudIdx = m_MainMenuBar->FindMenu( _("Buddies") );
    if( (m & Cmd_Buddies)  ==  0 )
        m_MainMenuBar->EnableTop( iBudIdx, false );
    else
    {
        m_MainMenuBar->EnableTop( iBudIdx, true );
        m_BuddiesPopup->Enable( Buddies_Add, (m & Cmd_Buddies_Add) );
        m_BuddiesPopup->Enable( Buddies_AddByScreen, (m & Cmd_Buddies_AddByScreen) );
        m_BuddiesPopup->Enable( Buddies_Remove, (m & Cmd_Buddies_Remove) );
    }

    m_MainPopup->Enable( Contact_New, (m & Cmd_Contact_New) );

    m_MainPopup->Enable( Buddies_Add, (m & Cmd_Buddies_Add) );
    m_MainPopup->Enable( Buddies_AddByScreen, (m & Cmd_Buddies_AddByScreen) );
    m_MainPopup->Enable( Buddies_Remove, (m & Cmd_Buddies_Remove) );

    m_MainPopup->Enable( Edit_Copy, (m & Cmd_Edit_Copy) );
    m_MainPopup->Enable( Edit_Paste, (m & Cmd_Edit_Paste) );
    m_MainPopup->Enable( Edit_Delete, (m & Cmd_Edit_Delete) );
    m_MainPopup->Enable( Edit_Edit, (m & Cmd_Edit_Edit) );
    m_MainPopup->Enable( Edit_RemoveFromGroup, (m & Cmd_Edit_RemoveFromGroup) );

    m_ManageContactsPopup->Enable( Contact_New, (m & Cmd_Contact_New) );

    m_ManageContactsPopup->Enable( Edit_Copy, (m & Cmd_Edit_Copy) );
    m_ManageContactsPopup->Enable( Edit_Paste, (m & Cmd_Edit_Paste) );
    m_ManageContactsPopup->Enable( Edit_Delete, (m & Cmd_Edit_Delete) );
    m_ManageContactsPopup->Enable( Edit_Edit, (m & Cmd_Edit_Edit) );
    m_ManageContactsPopup->Enable( Edit_RemoveFromGroup, (m & Cmd_Edit_RemoveFromGroup) );

    m_AdminPopup->Enable( Edit_EmailAccountInfo, (m & Cmd_Edit_EmailAccountInfo) );

    m_NewPopup->Enable( Contact_New, (m & Cmd_Contact_New) );

    m_SearchMenu->Enable( Search_Remove, (m & Cmd_Search_Remove) );
    m_SearchMenu->Enable( Search_Continue, (m & Cmd_Search_Continue) );
    m_SearchMenu->Enable( Search_Edit, (m & Cmd_Search_Edit) );

    //m_MainPopup->Enable( Edit_EmailAccountInfo, (m & Cmd_Edit_EmailAccountInfo) );

}

void ManageContactsFrame::OnSearchButtonClick(wxCommandEvent& event)
{
    StartNewSearch();
}

void ManageContactsFrame::StartNewSearch()
{
    goModeless();

    FindContactsDialog Dlg(this,wxID_ANY);
    int nRet = Dlg.EditSearchCriteria( true );

    goModal();

    if( nRet != wxID_OK )
        return;

    // Make sure the criteria is saved
    Dlg.SaveSearchCriteria();

    ContactSearch criteria;
    criteria = Dlg.GetCriteria();

    if( m_ContactTree.EditCurrentSearch(false) )
    {
        ContactSearchResultsItemData *pSrch = m_ContactTree.GetSearchNode();
        if( !pSrch )
            return;

        wxTreeItemId searchNode = m_ContactTree.Tree()->GetSelection();
        m_ContactTree.Tree()->DeleteChildren( searchNode );
/*
        // Clear the slate prior to starting the search again
        if( pSrch->m_list )
        {
            meeting_list_destroy(pSrch->m_list);
            pSrch->m_list = 0;
        }
*/
        pSrch->m_strSearchCriteriaName = criteria.m_strSearchName;
        criteria.Encode( pSrch->m_strSearchCriteria );

        // Reset the results state
        pSrch->m_nStartingRow = 0;
        pSrch->m_nTotalRetrieved = 0;
        pSrch->m_fComplete = false;

        // Use a different search number
        pSrch->m_nSearchNumber = m_ContactTree.NextSearchNumber();

        m_ContactTree.StartContactSearch();
    }else
    {
        m_ContactTree.StartContactSearch( criteria );
    }
}

void ManageContactsFrame::OnSearchNew( wxCommandEvent& event )
{
    StartNewSearch();
}

void ManageContactsFrame::OnSearchContinue( wxCommandEvent& event )
{
    m_ContactTree.StartContactSearch();
}

void ManageContactsFrame::OnSearchEdit( wxCommandEvent& event )
{
    ContactSearchResultsItemData *pSrch = m_ContactTree.GetSearchNode();
    if( !pSrch )
        return;

    wxTreeItemId searchNode = m_ContactTree.Tree()->GetSelection();

    goModeless();

    FindContactsDialog Dlg( this, wxID_ANY );

    ContactSearch criteria;
    criteria.Decode( pSrch->m_strSearchCriteria );
    m_ContactTree.EditCurrentSearch(true);
    Dlg.PresetSearchCriteria( pSrch->m_strSearchCriteriaName, criteria );
    int nResp = Dlg.EditSearchCriteria( true ); // Prep & Show dialog

    goModal();

    if( nResp == wxID_OK )
    {
/*
        // Clear the slate prior to starting the search again
        if( pSrch->m_list )
        {
            meeting_list_destroy(pSrch->m_list);
            pSrch->m_list = 0;
        }
*/
        // Reset the results state
        pSrch->m_nStartingRow = 0;
        pSrch->m_nTotalRetrieved = 0;
        pSrch->m_fComplete = false;
        pSrch->m_nSearchNumber = m_ContactTree.NextSearchNumber();

        ContactSearch criteria;
        Dlg.SaveSearchCriteria();
        criteria = Dlg.GetCriteria();

        pSrch->m_strSearchCriteriaName = criteria.m_strSearchName;
        criteria.Encode( pSrch->m_strSearchCriteria );

        m_ContactTree.Tree()->DeleteChildren( searchNode );
        m_ContactTree.StartContactSearch( searchNode );
/*
        // Update the search criteria that's attached to the node
        MeetingSearch criteria;
        criteria = Dlg.GetCriteria();
        pSrch->m_strSearchCriteriaName = criteria.m_strSearchName;
        criteria.Encode( pSrch->m_strSearchCriteria );


        // Use a different search number

        SetUIFeedback( 0, pSrch );

        // Do it again
        StartMeetingSearch( searchNode );
*/
    }
}

void ManageContactsFrame::OnSearchRemove( wxCommandEvent& event )
{
    m_ContactTree.RemoveSearch();
}

void ManageContactsFrame::OnDirectoryFetched(wxCommandEvent& event)
{
    event.Skip();    
    if (event.GetString() == wxT("import"))
    {
        // Imported directory (e.g. outlook).  Make sure tree has been initialized first.
        if (m_ContactTree.Tree() != NULL) {
            directory_t dir = (directory_t)event.GetClientData();
            wxString dirName(dir->name, wxConvUTF8);
            wxTreeItemId id = m_ContactTree.AppendDirectoryNode( dirName, dir, dtUser );
            SortContacts( id, true );
        }
    }
    else
    {
        // This must be the search results.
        m_ContactTree.ProcessOnDirectoryFetched( event );
    }
}

void ManageContactsFrame::OnDirectoryStarted(wxCommandEvent& event)
{
    if (event.GetString() == wxT("import"))
    {
        // Starting to imported directory (e.g. outlook)
        directory_t dir = (directory_t)event.GetClientData();
        wxString dirName(dir->name, wxConvUTF8);
        if (m_ContactTree.FindUserDirectory(dirName) == (wxTreeItemId)NULL) 
        {
            wxTreeItemId id = m_ContactTree.AppendDirectoryNode( dirName, dir, dtUser, true );
            SortContacts( id, true );
        }
        event.Skip();
    }
}

void ManageContactsFrame::OnSessionError(SessionErrorEvent& event)
{
    wxString error_msg;
    wxString rpc_id;
    wxString opid;
    error_msg = event.GetMessage();
    rpc_id = event.GetRpcId();
    opid = event.GetOpid();

    wxLogDebug(wxT("ManageContactsFrame::OnSessionError opid=%s msg=%s"),opid.c_str(),error_msg.c_str() );

    ClearState( State_WaitingForUpdate );

    event.Skip();
}

void ManageContactsFrame::OnAddressbkUpdateFailed(wxCommandEvent& event)
{
    wxString str;
    str = event.GetString();
    int nd = str.Find('|');
    wxString opid;
    wxString msg;
    if( nd >= 0 )
    {
        opid = str.Left(nd);
        msg =  str.Mid(nd+1);
    }else
    {
        opid = wxT("");
        msg = str;
    }

    // This one is ours
    wxLogDebug(wxT("ManageContactsFrame::OnAddressbkUpdateFailed opid=%s, msg=%s"),opid.c_str(),msg.c_str());

    if( !CONTROLLER.IsOpcode( opid, MeetingCenterController::OP_ManageContacts, false /* NOT exact match */ ) )
        event.Skip();

    ClearState( State_WaitingForUpdate );
    
    SetDatabaseError( event.GetInt(), _("Server operation failed"), true);
}

void ManageContactsFrame::OnAddressUpdated(wxCommandEvent& event)
{
    wxString str;
    str = event.GetString();
    int nd = str.Find('|');
    wxString opid;
    wxString itemid;
    if( nd >= 0 )
    {
        opid = str.Left(nd);
        itemid =  str.Mid(nd+1);
    }else
    {
        opid = wxT("");
        itemid = str;
    }
    wxLogDebug(wxT("ManageContactsFrame: Address Book Updated: opid=%s, itemid=%s"),
        opid.c_str(), itemid.c_str() );

    ClearState( State_WaitingForUpdate );

    contact_t   cnt;
    if( m_Dir )
        cnt = directory_find_contact_by_field( m_Dir, "userid", itemid.mb_str( wxConvUTF8 ) );
    m_Dir = 0;

    event.Skip();
}

void ManageContactsFrame::OnAddressDeleted(wxCommandEvent& event)
{
    wxString str;
    str = event.GetString();
    int nd = str.Find('|');
    wxString opid;
    wxString itemid;
    if( nd >= 0 )
    {
        opid = str.Left(nd);
        itemid =  str.Mid(nd+1);
    }else
    {
        opid = wxT("");
        itemid = str;
    }
    wxLogDebug(wxT("ManageContactsFrame: Address Book Deleted: opid=%s, itemid=%s"),
        opid.c_str(), itemid.c_str() );

    ClearState( State_WaitingForUpdate );

    event.Skip();
}

void ManageContactsFrame::OnContactsTreeListCtrlItemRightClick(wxTreeEvent& event)
{
    ContactSearchResultsItemData *pSrch = m_ContactTree.GetSearchNode();
    if( pSrch )
    {
        // Prep the menu:
        m_SearchPopup->Enable( Search_Continue, !pSrch->m_fComplete );
        //Search_Remove - Always OK
        //Search_Edit - Always OK
        PopupMenu( m_SearchPopup, event.GetPoint() );
    }else
    {
        PopupMenu( m_MainPopup, event.GetPoint() );
    }
}

void ManageContactsFrame::OnContactListColumnLeftClick(wxListEvent& event)
{
    if( m_nState & State_Sorting )
        return; // Not now! Prevent recursive sorts

    m_nState |= State_Sorting;

    long new_sort_col = event.GetColumn();
    wxLogDebug(wxT("MeetingCenterController::OnMeetingListColumnLeftClick( col %d )"),new_sort_col);

    long current_sort_col = m_ContactTree.Tree()->GetSortColumn();

    if( new_sort_col == current_sort_col )
    {
        m_ContactTree.Tree()->ReverseSortOrder();
    }else
    {
        m_ContactTree.Tree()->SetSortColumn( new_sort_col );
    }
    SortContacts();

    m_nState &= ~State_Sorting;
}

void ManageContactsFrame::SortContacts()
{
    if( m_CommunityNode.IsOk() )
        SortContacts( m_CommunityNode );

    if( m_PersonalNode.IsOk() )
        SortContacts( m_PersonalNode );

    if( m_SearchNode.IsOk() )
        SortContacts( m_SearchNode );
        
    const wxArrayTreeItemIds& dirs = m_ContactTree.GetUserDirectories( );
    for( unsigned int i=0; i<dirs.GetCount(); i++ )
        SortContacts( dirs[i] );
}

void ManageContactsFrame::SortContacts( const wxTreeItemId & item, bool fSortParent  )
{
    if( fSortParent )
    {
        m_ContactTree.Tree()->SortChildren( item );
    }

    wxTreeItemIdValue cookie;
    for ( wxTreeItemId child = m_ContactTree.Tree()->GetFirstChild(item, cookie);
      child.IsOk();
      child = m_ContactTree.Tree()->GetNextChild(item, cookie) )
    {
        m_ContactTree.Tree()->SortChildren( child );
    }
}


void ManageContactsFrame::OnContactsTreeListCtrlSelectionChanged(wxTreeEvent& event)
{
    SetAvailableCommands();
}


void ManageContactsFrame::OnNewButtonClicked(wxCommandEvent& event)
{
    wxPoint btn_pos;
    btn_pos = m_NewButton->GetPosition();

    wxSize btn_siz;
    btn_siz = m_NewButton->GetSize();

    btn_pos.y += btn_siz.GetHeight();

    PopupMenu( m_NewPopup, btn_pos );
}

////////////////////////////////////////////////////////////////
void ManageContactsFrame::OnUserNew(wxCommandEvent& event)
{
    wxArrayTreeItemIds selections;
    size_t nSelected;
    int nSelectionMask = m_ContactTree.GetSelectionInfo( selections, nSelected );
    wxTreeItemId allGroupId;
    wxTreeItemId selectedGroupId;

    // Determine the parent node(s) for the new user
    group_t allGroup = m_ContactTree.GetCABAllGroup(allGroupId);
    group_t selectedGroup = allGroup;
    if( nSelected == 1 )
    {
        selectedGroupId = selections[0];
        if( nSelectionMask == ContactTreeList::Node_Group )
        {
            // Also create in a new group
            GroupTreeItemData *pData = (GroupTreeItemData *)m_ContactTree.Tree()->GetItemData( selectedGroupId );
            selectedGroup = pData->m_group;
        }
    }

    wxTreeItemId node;
    int nRet = CreateNewContact( CONTROLLER.GetCAB(), UserTypeUser, node );

    if( nRet == 0 && node.IsOk() && selectedGroup != allGroup )
    {
        ContactTreeItemData *pCData = (ContactTreeItemData *)m_ContactTree.Tree()->GetItemData( node );
        if( !pCData )
        {
            wxLogDebug(wxT("ManageContactsFrame::OnUserNew - Logic error NULL ItemData for node"));
            return;
        }

        int err = AddGroupMember( selectedGroup, pCData->m_contact);
        if( err == 0 )
        {
            ContactTreeItemData *pCNewData = new ContactTreeItemData( *pCData );
            node = m_ContactTree.AppendContact( CONTROLLER.GetCAB(), selectedGroupId, pCNewData );
        }
    }

    // Select the new node and make sure it's visible.
    if( nRet == 0 && node.IsOk() )
    {
        m_ContactTree.Tree()->SelectItem( node, node, true );
        m_ContactTree.Tree()->EnsureVisible( node );
    }
}

////////////////////////////////////////////////////////////////
void ManageContactsFrame::OnContactNew(wxCommandEvent& event)
{
    wxArrayTreeItemIds selections;
    size_t nSelected;
    int nSelectionMask = m_ContactTree.GetSelectionInfo( selections, nSelected );
    wxTreeItemId allGroupId;
    wxTreeItemId selectedGroupId;

    // Determine the parent node(s) for the new contact
    group_t allGroup = m_ContactTree.GetPABAllGroup( allGroupId );
    group_t selectedGroup = allGroup;
    if( nSelected == 1 )
    {
        selectedGroupId = selections[0];
        if( !m_ContactTree.IsInGlobalDir(selectedGroupId) &&
            nSelectionMask == ContactTreeList::Node_Group )
        {
            GroupTreeItemData *pData = (GroupTreeItemData *)m_ContactTree.Tree()->GetItemData( selectedGroupId );
            selectedGroup = pData->m_group;
        }
    }

    wxTreeItemId node;
    int nRet = CreateNewContact( CONTROLLER.GetPAB(), UserTypeContact, node );
    if( nRet == 0 && node.IsOk() && selectedGroup != allGroup )
    {
        wxLogDebug(wxT("ManageContactsFrame::OnContactNew - New contact created. Attempting to add the contact to another group"));
        ContactTreeItemData *pCData = (ContactTreeItemData *)m_ContactTree.Tree()->GetItemData( node );
        int err = AddGroupMember( selectedGroup, pCData->m_contact);
        if( err == 0 )
        {
            ContactTreeItemData *pCNewData = new ContactTreeItemData( *pCData );
            node = m_ContactTree.AppendContact( CONTROLLER.GetPAB(), selectedGroupId, pCNewData );
        }
    }

    // Select the new node and make sure it's visible.
    if( nRet == 0 && node.IsOk() )
    {
        m_ContactTree.Tree()->SelectItem( node, node, true );
        m_ContactTree.Tree()->EnsureVisible( node );
    }
}

////////////////////////////////////////////////////////////////
void ManageContactsFrame::OnGroupNew(wxCommandEvent& event)
{
    wxArrayTreeItemIds selections;
    size_t nSelected;
    m_ContactTree.GetSelectionInfo( selections, nSelected );
    wxString strMsg;
    bool fCreateCABGroup = false;
    bool fError = false;
    if( nSelected != 1 )
    {
        strMsg = _("Please select an address book in which to create a new group.");
        fError = true;
    }else if( nSelected == 1 )
    {
        wxTreeItemId item = selections[0];
        if( m_ContactTree.GetNodeType(item) == ContactTreeList::Node_Group )
        {
            strMsg = _("You cannot create a group here.\nPlease select an address book in which to create a new group.");
            fError = true;
        }else
        {
            if( m_ContactTree.GetNodeType(item) == ContactTreeList::Node_GlobalDir )
            {
                if( !CONTROLLER.AdministratorMode() )
                {
                    strMsg = _("You cannot create community groups.\nPlease contact your administrator or select the personal address book if you wish to create a new group.");
                    fError = true;
                }
                fCreateCABGroup = true;
            }
        }
    }

    if( fError )
    {
        ::wxMessageBox( strMsg, m_strCaption, wxOK, this );
    }else
    {
        wxString message( _("Group name:"), wxConvUTF8 );
        wxString defaultValue( _("New group"), wxConvUTF8 );
        wxString groupName;

        wxTextEntryDialog AskForGroupName( this, message, m_strCaption, defaultValue );
        int nResp = AskForGroupName.ShowModal();
        if( nResp == wxID_OK )
        {
            directory_t dir = 0;
            int dir_type;
            if( fCreateCABGroup )
            {
                dir = CONTROLLER.GetCAB();
                dir_type = dtGlobal;
            }else
            {
                dir = CONTROLLER.GetPAB();
                dir_type = dtPersonal;
            }
            groupName = AskForGroupName.GetValue();

            group_t group = directory_group_new( dir, groupName.mb_str(wxConvUTF8) );
            group->type = dtOther;
            wxString strOpid;
            strOpid = MeetingCenterController::opcode( MeetingCenterController::OP_ManageContacts, -1 );


            int err = addressbk_group_update(CONTROLLER.book, strOpid.mb_str(), dir_type, group);
            wxLogDebug(wxT("ManageContactsFrame: addressbk_group_update returned %d"),err);
            if( err != 0 )
            {
                ClearState(State_WaitingForUpdate);
                SetDatabaseError( err, wxT(""), true );
                return;
            }

            // Wait for it.........
            err = WaitForClear( State_WaitingForUpdate, 20, _("Timeout while adding a new group") );
            if( err )
            {
                return;
            }
            m_ContactTree.AppendGroup( dir, group );
        }
    }
}

////////////////////////////////////////////////////////////////
void ManageContactsFrame::OnContactsRefresh(wxCommandEvent& event)
{
    RefreshContacts();
}

////////////////////////////////////////////////////////////////
void ManageContactsFrame::OnEditEdit(wxCommandEvent& event)
{
    EditSelectedNode();
}

////////////////////////////////////////////////////////////////
void ManageContactsFrame::OnEditCopy(wxCommandEvent& event)
{
    m_Clipboard.CopySelectedData( m_ContactTree );
    if( m_Clipboard.HasClipboardData() )
    {
        m_nState |= State_HaveCopiedContacts;
    }else
    {
        m_nState &= ~State_HaveCopiedContacts;
    }
    SetAvailableCommands();
}

////////////////////////////////////////////////////////////////
void ManageContactsFrame::OnEditPaste(wxCommandEvent& event)
{
    // Sanity check
    if( (m_nState & State_HaveCopiedContacts ) == 0 )
        return;

    wxArrayTreeItemIds selections;
    size_t nSelected;
    m_ContactTree.GetSelectionInfo( selections, nSelected );
    if( nSelected != 1 )
        return;

    int err = 0;
    directory_t     dir             = 0;
    wxTreeItemId    selected_group_node = selections[0];
    wxTreeItemId    all_group_node;
    group_t         all_group       = 0;
    bool            fAddingBuddy    = false;

    // Get the directory and "All" group information
    if( m_ContactTree.IsInGlobalDir( selected_group_node ) )
    {
        all_group = m_ContactTree.GetCABAllGroup( all_group_node );
        dir = CONTROLLER.GetCAB();
    }else
    {
        all_group = m_ContactTree.GetPABAllGroup( all_group_node );
        dir = CONTROLLER.GetPAB( );

        if( selected_group_node != all_group_node )
        {
            GroupTreeItemData   *pGData = (GroupTreeItemData *)m_ContactTree.Tree()->GetItemData( selected_group_node );
            if( pGData  &&  pGData->m_group  &&  pGData->m_group->type == dtBuddies )
            {
                fAddingBuddy = true;
                wxLogDebug( wxT("ManageContactsFrame::OnEditPaste - Pasting into Buddies group of the PAB") );
            }
        }
    }

    wxString strPassword(wxT(""));
    wxTreeItemId new_node;

    // For all clipboard entries
    for( size_t i = 0; i < m_Clipboard.GetContactCount(); ++i )
    {
        // Create the new contact record in the "All" group
        contact_t contact = m_Clipboard.CreateContact( dir, i, fAddingBuddy );
        if( !contact )
        {
            continue; // Something is wrong here!!!
        }

        err = SaveContact( dir, all_group_node, contact, strPassword );
        new_node = m_ContactTree.AppendContact( dir, contact, strPassword );
        if( err == 0 )
        {
            if( selected_group_node != all_group_node )
            {
                wxLogDebug(wxT("ManageContactsFrame::OnEditPaste - New contact created. Attempting to add the contact to another group"));
                ContactTreeItemData *pCData = (ContactTreeItemData *)m_ContactTree.Tree()->GetItemData( new_node );
                GroupTreeItemData   *pGData = (GroupTreeItemData *)m_ContactTree.Tree()->GetItemData( selected_group_node );
                int err = AddGroupMember( pGData->m_group, pCData->m_contact);
                if( err == 0 )
                {
                    ContactTreeItemData *pCNewData = new ContactTreeItemData( *pCData );
                    new_node = m_ContactTree.AppendContact( dir, selected_group_node, pCNewData );
                }
            }
        }else
        {
            // Error!
        }

    } // end for all clipboard entries
}

////////////////////////////////////////////////////////////////
void ManageContactsFrame::OnEditDelete(wxCommandEvent& event)
{
    int err = 0;
    wxArrayTreeItemIds selections;
    size_t nSelected;
    m_ContactTree.GetSelectionInfo( selections, nSelected );

    if( ConfirmDelete() )
    {
        for( size_t i = 0; i < nSelected; ++i )
        {
            wxTreeItemId item;
            item = selections[i];

            err = DeleteItem( item );
        }
    }
}

////////////////////////////////////////////////////////////////
bool ManageContactsFrame::DeleteAllowed( bool fShowErrorMsg )
{
    wxArrayTreeItemIds selections;
    size_t nSelected;
    int nSelectionMask = m_ContactTree.GetSelectionInfo( selections, nSelected );
    wxString strMsg;

    bool fError = false;

    if( nSelected == 0 )
    {
        fError = true;
        strMsg = wxT("");
    }else if( nSelected > 1 )
    {
        if( nSelectionMask == ContactTreeList::Node_Contact )
        {
            // More than one contact / user record is selected see if the user can delete at lease some
            // of the selected records.
            if( !CanDeleteSomeContacts( selections ) )
            {
                strMsg = _("You cannot delete these contacts.\nPlease contact your administrator for assistance.");
                fError = true;
            }
        }else if( nSelectionMask == ContactTreeList::Node_Group )
        {
            // More than one group is selected
            if( !CanDeleteSomeGroups( selections ) )
            {
                strMsg = _("You cannot delete these groups.\nPlease select another group or specific contacts that you wish to delete.");
                fError = true;
            }
        }else if( nSelectionMask == ContactTreeList::Node_GlobalDir  ||  nSelectionMask == ContactTreeList::Node_PersonalDir )
        {
            {
                strMsg = _("You cannot delete the Community or Personal directories.\nPlease select a group or specific contacts that you wish to delete.");
                fError = true;
            }
        }
    }else if( nSelected == 1 )
    {
        wxTreeItemId item = selections[0];
        if( nSelectionMask == ContactTreeList::Node_Group )
        {
            GroupTreeItemData *pGData = (GroupTreeItemData *)m_ContactTree.Tree()->GetItemData( item );
            if( pGData->m_group->directory->type == dtGlobal && !CONTROLLER.AdministratorMode() )
            {
                strMsg = _("You cannot delete this group.\nPlease contact your administrator for assistance.");
                fError = true;
            }else if( pGData->m_group->type == dtAll || pGData->m_group->type == dtBuddies || pGData->m_group->readonly )
            {
                strMsg = _("You cannot delete this group.\nPlease select another group or specific contacts that you wish to delete.");
                fError = true;
            }
        }else if( nSelectionMask == ContactTreeList::Node_Contact )
        {
            ContactTreeItemData *pCData = (ContactTreeItemData *)m_ContactTree.Tree()->GetItemData( item );
            if( pCData->m_contact->directory->type == dtGlobal && !CONTROLLER.AdministratorMode() )
            {
                strMsg = _("You cannot delete this contact.\nPlease contact your administrator for assistance.");
                fError = true;
            }else if ( pCData->m_contact->readonly )
            {
                strMsg = _("You cannot delete this contact.");
                fError = true;
            }
        }else if( nSelectionMask == ContactTreeList::Node_GlobalDir  ||  nSelectionMask == ContactTreeList::Node_PersonalDir )
        {
            strMsg = _("You cannot delete this directory.\nPlease select a group or specific contacts that you wish to delete.");
            fError = true;
        }else if( nSelectionMask == ContactTreeList::Node_SearchRoot )
        {

        }
    }

    if( fError && fShowErrorMsg && !strMsg.IsEmpty() )
    {
        ::wxMessageBox( strMsg, m_strCaption, wxOK, this );
    }

    return !fError;
}

////////////////////////////////////////////////////////////////
bool ManageContactsFrame::ConfirmDelete( bool fShowErrorMsg )
{
    bool fConfirmed = false;

    if( DeleteAllowed( fShowErrorMsg ) )
    {
        wxString strMsg;
        strMsg = _("The selected item(s) will be permanently deleted.\nContinue?");
        wxMessageDialog     dlg( this, strMsg, m_strCaption, wxYES_NO | wxICON_QUESTION );
        if( dlg.ShowModal( )  ==  wxID_YES )
        {
            fConfirmed = true;
        }
    }

    return fConfirmed;
}

////////////////////////////////////////////////////////////////
int ManageContactsFrame::DeleteItem( wxTreeItemId & item )
{
    int err = 0;
    wxString strOpid;
    strOpid = MeetingCenterController::opcode( MeetingCenterController::OP_ManageContacts, -1 );
    wxCharBuffer opid = strOpid.mb_str();

    switch ( m_ContactTree.GetNodeType( item ) )
    {
    case ContactTreeList::Node_UserDir:
        break;
    case ContactTreeList::Node_Group:
        if( CanDeleteGroup( item ) )
        {
            //int addressbk_group_delete(addressbk_t bk, const char *opid, group_t group);
            // Delete the contact
            GroupTreeItemData *pGData = (GroupTreeItemData *)m_ContactTree.Tree()->GetItemData( item );

            int err = addressbk_group_delete(CONTROLLER.AddressBook(), opid, pGData->m_group );

            wxLogDebug(wxT("ManageContactsFrame::DeleteItem addressbk_group_delete returned: %d"),err);
            if( err != 0 )
            {
                ClearState(State_WaitingForUpdate);
                if( err == ERR_NOTADMIN )
                {
                    SetDatabaseError( err, _("Not authorized"), true );
                }else
                {
                    SetDatabaseError( err, wxT(""), true );
                }
                return err;
            }

            // Wait for it.........
            err = WaitForClear( State_WaitingForUpdate, 20, _("Timeout while deleting a group") );
            if( !err && m_nDatabaseError )
            {
                err = m_nDatabaseError;
            }
            if( err == 0 )
            {
                m_ContactTree.Tree()->Delete( item );
                // Warning: pGData and item are no longer valid, they have been deleted from the tree as of now.
            }
        }
        break;
    case ContactTreeList::Node_Contact:
        if( CanDeleteContact( item ) )
        {
            if( m_ContactTree.IsInSearchResults( item ) )
            {
                // Just remove the node. If the search is incomplete and the user opts to get more results, the node
                // will show up again. This is a bit strange, but the user knows best. Right ?
                m_ContactTree.Tree()->Delete( item );
            }else
            {
                // Delete the contact
                ContactTreeItemData *pCData = (ContactTreeItemData *)m_ContactTree.Tree()->GetItemData( item );

                int err = addressbk_contact_delete(CONTROLLER.AddressBook(), opid, pCData->m_contact );

                wxLogDebug(wxT("ManageContactsFrame::DeleteItem addressbk_contact_delete returned: %d"),err);
                if( err != 0 )
                {
                    ClearState(State_WaitingForUpdate);
                    SetDatabaseError( err, wxT(""), true );
                    return err;
                }

                // Wait for it.........
                err = WaitForClear( State_WaitingForUpdate, 20, _("Timeout while deleting contact") );
                if( !err && m_nDatabaseError )
                {
                    err = m_nDatabaseError;
                }
                if( err == 0 )
                {
                    m_ContactTree.RemoveContactNodes( pCData->m_contact );
                    // Warning: pCData and item are no longer valid, they have been deleted from the tree as of now.
                }
            }
        }
        break;
    case ContactTreeList::Node_SearchRoot:
        break;
    case ContactTreeList::Node_SearchResults:
        break;
    };

    return err;
}


////////////////////////////////////////////////////////////////
void ManageContactsFrame::OnEditEmailAccountInfo(wxCommandEvent& event)
{
    wxArrayTreeItemIds  selections;
    size_t              nSelected;

    m_ContactTree.GetSelectionInfo( selections, nSelected );
    wxTreeItemId        item;
    item = selections[ 0 ];
    ContactTreeItemData     *pCData     = (ContactTreeItemData *)m_ContactTree.Tree()->GetItemData( item );
    contact_t               contact     = pCData->m_contact;

    if( contact  ==  NULL )
        return;
    if( strlen( contact->email )  ==  0 )
    {
        wxString            strErrTitle( _("Please Note"), wxConvUTF8 );
        wxString            strErrMessage( _("Cannot send email to user, email address is not specified."), wxConvUTF8 );
        wxMessageDialog     errDlg( this, strErrMessage, strErrTitle, wxOK | wxICON_EXCLAMATION );
        errDlg.ShowModal( );
        return;
    }

    wxString            strAskTitle( _("Please Confirm"), wxConvUTF8 );
    wxString            strAskMessage( _("Are you sure that you want to e-mail account information to the selected user?"), wxConvUTF8 );
    wxMessageDialog     askDlg( this, strAskMessage, strAskTitle, wxYES_NO | wxICON_QUESTION );
    if( askDlg.ShowModal( )  !=  wxID_YES )
    {
        return;
    }

    bool        fAskForPass = true;
    wxString    strNewPass;
    while( fAskForPass )
    {
        wxString            strAsk2Title( _("Reset Password"), wxConvUTF8 );
        wxString            strAsk2Message( _("Input a password for the user:"), wxConvUTF8 );
        wxString            strBlank;
        wxTextEntryDialog   ask2Dlg( this, strAsk2Message, strAsk2Title, strBlank, wxOK | wxCANCEL | wxICON_QUESTION );
        if( ask2Dlg.ShowModal( )  !=  wxID_OK )
        {
            return;
        }

        CUtils::MsgWait( 10, 1 );	// avoid stuck dialog bkgnd

        strNewPass = ask2Dlg.GetValue( );
        if( !strNewPass.IsEmpty( ) )
            fAskForPass = false;
    }

    int iRet;
    iRet = addressbk_email_account_info( CONTROLLER.AddressBook( ), contact, strNewPass.mb_str( wxConvUTF8 ) );
}


////////////////////////////////////////////////////////////////
void ManageContactsFrame::OnBuddiesButtonClicked(wxCommandEvent& event)
{
    //wxPoint btn_pos;
    //btn_pos = m_BuddiesButton->GetPosition();

    //wxSize btn_siz;
    //btn_siz = m_BuddiesButton->GetSize();

    //btn_pos.y += btn_siz.GetHeight();

    //PopupMenu( m_BuddiesPopup, btn_pos );
}

////////////////////////////////////////////////////////////////
void ManageContactsFrame::OnBuddiesAdd(wxCommandEvent& event)
{
    // It is assumed that all the entries in the selections array are valid potential
    // buddies.
    // This operation is identical to the Copy/Paste operation except we use a private "clipboard" array

    wxString strOpid;
    strOpid = MeetingCenterController::opcode( MeetingCenterController::OP_ManageContacts, -1 );
    wxCharBuffer opid = strOpid.mb_str();

    // Copy the data
    wxArrayString encodedData;
    m_Clipboard.CopySelectedData( m_ContactTree, encodedData );
    if( encodedData.GetCount() <= 0 )
        return;

    int err = 0;
    directory_t dir = CONTROLLER.GetPAB();

    wxTreeItemId selected_group_node;
    m_ContactTree.GetBuddiesGroup( selected_group_node );

    wxTreeItemId all_group_node;
    m_ContactTree.GetPABAllGroup( all_group_node );

    wxString strPassword(wxT(""));
    wxTreeItemId new_node;

    // For all clipboard entries
    for( size_t i = 0; i < encodedData.GetCount(); ++i )
    {
        // Create the new contact record in the "All" group
        contact_t contact = m_Clipboard.CreateContact( dir, encodedData, i, true );
        if( !contact )
        {
            continue; // Something is wrong here!!!
        }

        err = AddBuddy( contact );
        new_node = m_ContactTree.AppendContact( dir, contact, strPassword );
        if( err == 0 )
        {
            if( selected_group_node != all_group_node )
            {
                wxLogDebug(wxT("ManageContactsFrame::OnEditPaste - New contact created. Attempting to add the contact to another group"));
                ContactTreeItemData *pCData = (ContactTreeItemData *)m_ContactTree.Tree()->GetItemData( new_node );
                //GroupTreeItemData   *pGData = (GroupTreeItemData *)m_ContactTree.Tree()->GetItemData( selected_group_node );
                //int err = AddGroupMember( pGData->m_group, pCData->m_contact);
                //if( err == 0 )
                {
                    ContactTreeItemData *pCNewData = new ContactTreeItemData( *pCData );
                    new_node = m_ContactTree.AppendContact( dir, selected_group_node, pCNewData );
                }
            }
        }else
        {
            // Error!
        }

    } // end for all clipboard entries
}


////////////////////////////////////////////////////////////////
void ManageContactsFrame::OnBuddiesAddByScreen(wxCommandEvent& event)
{
    wxString strTitle( _("Add A Buddy"), wxConvUTF8 );
    wxString strMessage( _("Enter a buddy's screenname to add to your personal buddy list"), wxConvUTF8 );
    wxString strNewName;

    wxTextEntryDialog AskForBuddyName( this, strMessage, strTitle );
    int nResp = AskForBuddyName.ShowModal( );
    if( nResp == wxID_OK )
    {
        CUtils::MsgWait( 10, 1 );	// avoid stuck dialog bkgnd

        strNewName = AskForBuddyName.GetValue( );
        if( strNewName.IsEmpty( ) )
            return;
        CUtils::EncodeScreenName( strNewName );

        //  Does this screenname exist?
        directory_t dirCAB     = CONTROLLER.GetCAB( );
        directory_t dirPAB     = CONTROLLER.GetPAB( );
        contact_t   oldContact;

        oldContact = directory_find_contact_by_screenname( dirPAB, strNewName.mb_str( wxConvUTF8 ) );
        if( oldContact  !=  NULL )
        {
            //  Screen name already exists in the user PAB.  Do nothing and silently return.
            return;
        }

        oldContact = directory_find_contact_by_screenname( dirCAB, strNewName.mb_str( wxConvUTF8 ) );
        contact_t   newContact      = directory_contact_new( dirPAB );
        if( newContact  ==  NULL )
        {
            wxString        strErrTitle( _("Add Buddy Failed"), wxConvUTF8 );
            wxString        strErrMessage( _("Add Buddy failed: could not add new contact to Personal Address Book"), wxConvUTF8 );
            wxMessageDialog Err( this, strErrMessage, strErrTitle, wxOK | wxICON_ERROR );
            Err.ShowModal( );
            return;
        }
        if( oldContact  ==  NULL )
        {
            //  We could not find the indicated screen name in the CAB so create a new contact with the
            //  entered name as both the screenname and the first name
            newContact->screenname = allocField( dirPAB, strNewName );
            newContact->first = allocField( dirPAB, strNewName );
            newContact->type = UserTypeContact;
        }
        else
        {
            //  Existing screenname found in the CAB.  Copy just the name info and call it a day.
            newContact->screenname  = allocField( dirPAB, oldContact->screenname );
            newContact->title       = allocField( dirPAB, oldContact->title );
            newContact->first       = allocField( dirPAB, oldContact->first );
            newContact->middle      = allocField( dirPAB, oldContact->middle );
            newContact->last        = allocField( dirPAB, oldContact->last );
            newContact->suffix      = allocField( dirPAB, oldContact->suffix );
            newContact->type = UserTypeContact;
        }

        //  Add as a buddy
        int err = 0;

        wxTreeItemId selected_group_node;
        m_ContactTree.GetBuddiesGroup( selected_group_node );

        wxTreeItemId all_group_node;
        m_ContactTree.GetPABAllGroup( all_group_node );

        wxString strPassword(wxT(""));
        wxTreeItemId new_node;

        err = SaveContact( dirPAB, all_group_node, newContact, strPassword );
        new_node = m_ContactTree.AppendContact( dirPAB, newContact, strPassword );
        if( err == 0 )
        {
            if( selected_group_node != all_group_node )
            {
                wxLogDebug(wxT("ManageContactsDialog::OnBuddiesAddByScreen( ) - New contact created. Attempting to add the contact to another group"));
                ContactTreeItemData *pCData = (ContactTreeItemData *)m_ContactTree.Tree()->GetItemData( new_node );
                GroupTreeItemData   *pGData = (GroupTreeItemData *)m_ContactTree.Tree()->GetItemData( selected_group_node );
                int err = AddGroupMember( pGData->m_group, pCData->m_contact);
                if( err == 0 )
                {
                    ContactTreeItemData *pCNewData = new ContactTreeItemData( *pCData );
                    new_node = m_ContactTree.AppendContact( dirPAB, selected_group_node, pCNewData );
                }
            }
        }
        else
        {
            // Error!
        }
    }

}


////////////////////////////////////////////////////////////////
void ManageContactsFrame::OnBuddiesRemove(wxCommandEvent& event)
{
    // It is assumed that all the entries in the selections array are valid potential
    // buddies.

    // Gather the selections
    wxArrayTreeItemIds selections;
    size_t nSelected;
    m_ContactTree.GetSelectionInfo( selections, nSelected );

    // Get to the buddies node
    wxTreeItemId buddiesGroupId;
    group_t buddiesGroup = m_ContactTree.GetBuddiesGroup( buddiesGroupId );
    if( !buddiesGroup || !buddiesGroupId.IsOk() )
    {
        return;
    }

    wxTreeItemId buddyId;
    wxTreeItemId selectedId;

    for( size_t i = 0; i < nSelected; ++i )
    {
        selectedId = selections[i];

        if( m_ContactTree.GetNodeType( selectedId ) == ContactTreeList::Node_Contact )
        {
            ContactTreeItemData * pCData = (ContactTreeItemData *)m_ContactTree.Tree()->GetItemData( selectedId );
            contact_t contact = pCData->m_contact;
            
            if( contact->readonly )
                return;

            // First remove the tree node
            buddyId = m_ContactTree.FindBuddyNode( pCData->m_contact );
            if( buddyId.IsOk() )
            {
                m_ContactTree.Tree()->Delete( buddyId );
            }

            // Next remove the buddies entry
            //int err = RemoveGroupMember( buddiesGroup, pCData->m_contact);
            int err = RemoveBuddy( contact);
            if( err != 0 )
            {
                wxLogDebug(wxT("ManageContactsFrame::OnBuddiesRemove - RemoveBuddy returned %d"),err);
            }
        }
    }
}

///////////////////////////////////////////////////////////////
void ManageContactsFrame::OnIMSend(wxCommandEvent& event)
{
    // Gather the selections
    wxArrayTreeItemIds selections;
    size_t nSelected;
    m_ContactTree.GetSelectionInfo( selections, nSelected );

    wxTreeItemId selectedId;
    bool commandQueued = false;

    for( size_t i = 0; i < nSelected; ++i )
    {
        selectedId = selections[i];

        if( m_ContactTree.GetNodeType( selectedId ) == ContactTreeList::Node_Contact )
        {
            ContactTreeItemData * pCData = (ContactTreeItemData *)m_ContactTree.Tree()->GetItemData( selectedId );
            contact_t contact = pCData->m_contact;

            wxString screen( contact->screenname, wxConvUTF8 );
            if (!screen.IsEmpty())
            {
                CONTROLLER.Commands().Add( wxT("start-im"), screen);
                commandQueued = true;
            }
        }
    }

    if (commandQueued)
        CONTROLLER.ExecutePendingCommands();
}

///////////////////////////////////////////////////////////////
void ManageContactsFrame::OnEditRemoveFromGroup(wxCommandEvent& event)
{
    // Gather the selections
    wxArrayTreeItemIds selections;
    size_t nSelected;
    m_ContactTree.GetSelectionInfo( selections, nSelected );
    wxTreeItemId item;
    if( nSelected == 0 )
        return;

    item = selections[0];
    wxTreeItemId group_node;
    group_node = m_ContactTree.Tree()->GetItemParent( item );
    if( m_ContactTree.GetNodeType( group_node ) != ContactTreeList::Node_Group )
        return;
    GroupTreeItemData *pGData = (GroupTreeItemData *)m_ContactTree.Tree()->GetItemData( group_node );
    group_t group = pGData->m_group;

    for( size_t i = 0; i < nSelected; ++i )
    {
        item = selections[i];
        if( m_ContactTree.GetNodeType( item ) == ContactTreeList::Node_Contact )
        {
            ContactTreeItemData *pCData = (ContactTreeItemData *)m_ContactTree.Tree()->GetItemData( item );
            if( pCData->m_contact->readonly )
                return;

            // Remove the item from the tree
            m_ContactTree.Tree()->Delete( item );

            // Remove the item from the data
            int err = RemoveGroupMember( group, pCData->m_contact);
            if( err != 0 )
            {
                wxLogDebug(wxT("ManageContactsFrame::OnEditRemoveFromGroup - RemoveGroupMember returned %d"),err);
            }
        }
    }
}

////////////////////////////////////////////////////////////////
bool ManageContactsFrame::CanDeleteSomeContacts( wxArrayTreeItemIds & selections )
{
    bool fCanDelete = false;
    for( size_t i = 0; !fCanDelete && i < selections.GetCount(); ++i )
    {
        wxTreeItemId item = selections[i];
        if( CanDeleteContact( item ) )
        {
            fCanDelete = true;
            break;
        }
    }
    return fCanDelete;
}

bool ManageContactsFrame::CanDeleteSomeGroups( wxArrayTreeItemIds & selections )
{
    bool fCanDelete = false;
    for( size_t i = 0; !fCanDelete && i < selections.GetCount(); ++i )
    {
        wxTreeItemId item = selections[i];
        if( CanDeleteGroup( item ) )
        {
            fCanDelete = true;
            break;
        }
    }
    return fCanDelete;
}

bool ManageContactsFrame::CanDeleteContact( wxTreeItemId & item )
{
    if( m_ContactTree.GetNodeType( item ) != ContactTreeList::Node_Contact )
    {
        return false;
    }

    if( m_ContactTree.IsInSearchResults( item ) )
    {
        return true; // Always OK to delete search results
    }

    ContactTreeItemData *pCData = (ContactTreeItemData *)m_ContactTree.Tree()->GetItemData( item );
    if( pCData->m_contact->directory->type == dtGlobal )
    {
        if( m_ContactTree.IsInGlobalDir( item ) && !CONTROLLER.AdministratorMode() )
        {
            return false;
        }
    }
    if( pCData->m_contact->readonly)
    {
        return false;
    }

    return true;
}

bool ManageContactsFrame::CanDeleteGroup( wxTreeItemId & item )
{
    if( m_ContactTree.GetNodeType(item) != ContactTreeList::Node_Group )
    {
        return false;
    }

    GroupTreeItemData *pGData = (GroupTreeItemData *)m_ContactTree.Tree()->GetItemData( item );
    if( pGData->m_group->type == dtAll )
    {
        return false;
    }
    if( pGData->m_group->type == dtBuddies )
    {
        return false;
    }
    if( pGData->m_group->readonly)
    {
        return false;
    }
    if( m_ContactTree.IsInGlobalDir( item ) &&
        !CONTROLLER.AdministratorMode() )
    {
        return false;
    }

    wxTreeItemIdValue cookie;
    bool fCanDeleteAllMembers = true;
    for ( wxTreeItemId child = m_ContactTree.Tree()->GetFirstChild(item, cookie);
      child.IsOk();
      child = m_ContactTree.Tree()->GetNextChild(item, cookie) )
    {
        if( m_ContactTree.GetNodeType( child ) == ContactTreeList::Node_Contact )
        {
            if( !CanDeleteContact( child ) )
            {
                fCanDeleteAllMembers = false;
                break;
            }
        }
    }

    return fCanDeleteAllMembers;
}

////////////////////////////////////////////////////////////////
void ManageContactsFrame::goModeless()
{
#if 0 //def __WXGTK__
    /* Don't think this is necessary here as we aren't a modal dialog */
    /* Gtk disables all other windows when one is modal, including modeless dialogs created by that
       window.  For now, hack around this by setting ourselves non-modal while this dialog is up. */
    GtkWindow * gtkwin = (GtkWindow*) GetHandle();
    gtk_window_set_modal(gtkwin, false);
#endif
}

void ManageContactsFrame::goModal()
{
#if 0 //def __WXGTK__
    // Back to modal
    GtkWindow * gtkwin = (GtkWindow*) GetHandle();
    gtk_window_set_modal(gtkwin, true);
#endif
}

int ManageContactsFrame::CreateNewContact( directory_t dir, int type, wxTreeItemId & node )
{
    int err = 0;
    wxASSERT_MSG(dir != 0, wxT("ManageContactsFrame::CreateNewContact - directory_t is NULL") );
    if( !dir )
    {
        return -1;
    }

    // Create a new contact record
    contact_t contact = directory_contact_new( dir );

    contact->type = type;

    profile_iterator_t iter = profiles_iterate(CONTROLLER.AddressBook());
    profile_t profile = profiles_next(CONTROLLER.AddressBook(), iter);
    if( profile )
    {
        contact->profileid = directory_string( dir, profile->profile_id);
    }

    // Let the user have a go at the data
    goModeless();
    wxString strPassword(wxT(""));
    CreateContactDialog Dlg( this, wxID_ANY );
    bool fChanged = Dlg.EditNewContact( dir, 0, contact, strPassword );
    goModal();
    if( !fChanged )
    {
        return 0;
    }

    wxTreeItemId group_node;
    group_t group;

    // Append the node
    if( dir->type == dtGlobal )
    {
        group = m_ContactTree.GetCABAllGroup( group_node );
        if( group )
        {
            err = SaveContact( dir, group_node, contact, strPassword );
            node = m_ContactTree.AppendContact( dir, contact, strPassword );
        }
    }else
    {
        group = m_ContactTree.GetPABAllGroup( group_node );
        if( group )
        {
            err = SaveContact( dir, group_node, contact, strPassword );
            node = m_ContactTree.AppendContact( dir, contact );
        }
    }

    return err;
}

int ManageContactsFrame::EditSelectedNode()
{
    int err = 0;

    wxArrayTreeItemIds selections;
    size_t nSelected;
    int nSelectionMask = m_ContactTree.GetSelectionInfo( selections, nSelected );
    wxString strMsg;
    bool fError = false;
    wxTreeItemId selected_node;

    if( nSelected == 0 )
    {
        strMsg = _("Please select the contact that you wish to edit.");
        fError = true;
    }else if( nSelected > 1 )
    {
        strMsg = _("You cannot edit more than one item at a time.\nPlease select the contact that you wish to edit.");
        fError = true;
    }
    else // A single item is selected
    {
        selected_node = selections[0];

        m_ContactTree.IsInGlobalDir( selected_node );

        if( nSelectionMask != ContactTreeList::Node_Contact )
        {
            strMsg = _("You may only edit contacts.\nPlease select the contact that you wish to edit.");
            fError = true;
        }
#if 0
        else if( !CONTROLLER.AdministratorMode() && fFromCAB )
        {
            strMsg = _("You cannot edit this contact.\nPlease contact your administrator or select a contact from the personal contact to edit.");
            fError = true;
        }
#endif
    }
    if( fError )
    {
        ::wxMessageBox( strMsg, m_strCaption, wxOK, this );
    }else
    {
        // Let the user edit the selected node
        ContactTreeItemData *pData = (ContactTreeItemData *)m_ContactTree.Tree()->GetItemData( selected_node );
        contact_t contact = pData->m_contact;

        m_Dir = NULL;
        wxString strPassword( wxT("") );
        if( m_ContactTree.IsInSearchResults( selected_node ) )
        {
            //  Node is a search result - see if it is from the CAB or PAB and set the correct directory
            if( CONTROLLER.GetPAB() )
            {
                contact_t   testContact;
                testContact = directory_find_contact_by_field( CONTROLLER.GetPAB(), "userid", contact->userid );
                if( testContact )
                {
                    m_Dir = CONTROLLER.GetPAB( );
                }
            }
            if( m_Dir  ==  NULL )
                m_Dir = CONTROLLER.GetCAB();
            strPassword = pData->m_strPassword;
        }
        else if( m_ContactTree.IsInGlobalDir( selected_node ) )
        {
            m_Dir = CONTROLLER.GetCAB();
            strPassword = pData->m_strPassword;
        }
        else
        {
            m_Dir = CONTROLLER.GetPAB();
            if( m_Dir  ==  NULL )
                return 0;
        }

        goModeless();

        CreateContactDialog Dlg( this, wxID_ANY );
        bool fChanged = Dlg.EditNewContact( m_Dir, 0, pData->m_contact, pData->m_strPassword );

        goModal();

        if( !fChanged )
        {
            return 0;
        }

        wxTreeItemId group_node;
        group_t group;

        // Append the node
        if( m_Dir->type == dtGlobal )
        {
            group = m_ContactTree.GetCABAllGroup( group_node );
            if( group )
            {
                err = SaveContact( m_Dir, group_node, contact, pData->m_strPassword );
                m_ContactTree.UpdateDisplay( contact );
            }
        }else
        {
            group = m_ContactTree.GetPABAllGroup( group_node );
            if( group )
            {
                err = SaveContact( m_Dir, group_node, contact, strPassword );
                m_ContactTree.UpdateDisplay( contact );
            }
        }
    }

    err = (fError) ? -1:0;

    return err;
}

void ManageContactsFrame::ShowDatabaseError()
{
    wxString strMsg;
    if( m_nDatabaseError )
    {
        if( m_strDatabaseError.IsEmpty() )
        {
            m_strDatabaseError = _("Unknown error");
        }
        strMsg = wxString::Format(_("Unable to update contact information:\n%s"),m_strDatabaseError.c_str());
        ::wxMessageBox( strMsg, m_strCaption, wxOK, this );
    }
}

int ManageContactsFrame::WaitForClear( int flags, int nSeconds, const wxString & errMsg )
{
    // If there are pending contact records that we being processed
    wxStopWatch sw;
    long start_time = sw.Time();
    long timeout_ms = (nSeconds * 1000);

    while( IsState( flags ) )
    {
        CUtils::MsgWait( 500, 1 );
        wxGetApp().Yield();

        if( (sw.Time() - start_time) > timeout_ms )
        {
            ClearState( flags );
            if( !errMsg.IsEmpty() )
            {
                SetDatabaseError( -1, errMsg, true );
            }
            return -1;
            break;
        }
    }

    // All is well
    return 0;
}

void ManageContactsFrame::OnOK(wxCommandEvent& event)
{

    event.Skip();
}


int ManageContactsFrame::SaveContact( directory_t dir, wxTreeItemId & group_node, contact_t contact, wxString & strPassword )
{
    bool fNewContactRecord = !(contact->userid && *contact->userid);
    wxString strOperation;
    if( fNewContactRecord )
    {
        strOperation = wxT("Add a new contact");
    }else
    {
        strOperation = wxT("Update a contact");
    }

    ClearDatabaseError();
    SetState( State_WaitingForUpdate );

    wxLogDebug(wxT("ManageContactsFrame::SaveContact - %s"), strOperation.c_str() );

    wxString strOpid;
    strOpid = MeetingCenterController::opcode( MeetingCenterController::OP_ManageContacts, -1 );
    wxCharBuffer opid = strOpid.mb_str();
    wxCharBuffer pwd = strPassword.mb_str();

    int err = addressbk_contact_update(CONTROLLER.AddressBook(),opid,dir->type,contact,pwd);

    wxLogDebug(wxT("ManageContactsFrame::SaveContact addressbk_contact_update returned: %d"),err);
    if( err != 0 )
    {
        ClearState(State_WaitingForUpdate);
        SetDatabaseError( err, wxT(""), true );
        return err;
    }

    // Wait for it.........
    err = WaitForClear( State_WaitingForUpdate, 20, _("Timeout while adding a new contact") );
    if( err )
    {
        return err;
    }
    if( m_nDatabaseError )
    {
        return m_nDatabaseError;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    // Add the contact to the appropriate All group if this is was a new contact
    if( fNewContactRecord )
    {
        GroupTreeItemData *pGData = (GroupTreeItemData *)m_ContactTree.Tree()->GetItemData( group_node );
        group_t group = pGData->m_group;

        err = AddGroupMember( group, contact );
    }

    return err;
}

int ManageContactsFrame::AddGroupMember( group_t group, contact_t contact )
{
    // Add the new contact to the selected group
    wxString strOpid;
    strOpid = MeetingCenterController::opcode( MeetingCenterController::OP_ManageContacts, -1 );

    wxString group_name( group->name, wxConvUTF8 );
    wxString new_userid( contact->userid, wxConvUTF8 );
    wxString new_first( contact->first, wxConvUTF8 );
    wxString new_last( contact->last, wxConvUTF8 );

    ClearDatabaseError();
    SetState( State_WaitingForUpdate );

    int err = addressbk_group_add_member(CONTROLLER.book, strOpid.mb_str(), group, contact->userid);
    wxLogDebug(wxT("ManageContactsFrame::AddGroupMember - addressbk_group_add_member returned %d"),err);
    if( err != 0 )
    {
        ClearState(State_WaitingForUpdate);
        SetDatabaseError( err, wxT(""), true );
        return err;
    }

    // Wait for it.........
    err = WaitForClear( State_WaitingForUpdate, 20, _("Timeout while adding a new group member") );
    if( err )
    {
        return err;
    }

    wxLogDebug(wxT("ManageContactsFrame::AddGroupMember -  Contact Userid:%s  First:%s Last:%s added to group |%s| "),new_userid.c_str(),new_first.c_str(),new_last.c_str(), group_name.c_str() );

    return err;
}


int ManageContactsFrame::RemoveGroupMember( group_t group, contact_t contact )
{
    // Add the new contact to the selected group
    wxString strOpid;
    strOpid = MeetingCenterController::opcode( MeetingCenterController::OP_ManageContacts, -1 );

    wxString group_name( group->name, wxConvUTF8 );
    wxString the_userid( contact->userid, wxConvUTF8 );
    wxString the_first( contact->first, wxConvUTF8 );
    wxString the_last( contact->last, wxConvUTF8 );

    ClearDatabaseError();
    SetState( State_WaitingForUpdate );

    int err = addressbk_group_remove_member(CONTROLLER.book, strOpid.mb_str(), group, contact->userid);
    wxLogDebug(wxT("ManageContactsFrame::RemoveGroupMember - addressbk_group_remove_member returned %d"),err);
    if( err != 0 )
    {
        ClearState(State_WaitingForUpdate);
        SetDatabaseError( err, wxT(""), true );
        return err;
    }

    // Wait for it.........
    err = WaitForClear( State_WaitingForUpdate, 20, _("Timeout while removing a group member") );
    if( err )
    {
        return err;
    }

    wxLogDebug(wxT("ManageContactsFrame::RemoveGroupMember -  Contact Userid:%s  First:%s Last:%s removed from group |%s| "),the_userid.c_str(),the_first.c_str(),the_last.c_str(), group_name.c_str() );

    return err;
}

int ManageContactsFrame::AddBuddy( contact_t contact )
{
    wxString strOpid;
    strOpid = MeetingCenterController::opcode( MeetingCenterController::OP_ManageContacts, -1 );

    ClearDatabaseError();
    SetState( State_WaitingForUpdate );

    int err = addressbk_buddy_add(CONTROLLER.book, strOpid.mb_str(), contact, FALSE);
    wxLogDebug(wxT("ManageContactsFrame::AddBuddy - addressbk_buddy_add returned %d"),err);
    if( err != 0 )
    {
        ClearState(State_WaitingForUpdate);
        SetDatabaseError( err, wxT(""), true );
        return err;
    }

    // Wait for it.........
    err = WaitForClear( State_WaitingForUpdate, 20, _("Timeout while adding a new buddy") );
    if( err )
    {
        return err;
    }

    wxLogDebug(wxT("ManageContactsFrame::AddBuddy - added ok") );

    return err;
}

int ManageContactsFrame::RemoveBuddy( contact_t contact )
{
    wxString strOpid;
    strOpid = MeetingCenterController::opcode( MeetingCenterController::OP_ManageContacts, -1 );

    ClearDatabaseError();
    SetState( State_WaitingForUpdate );

    int err = addressbk_buddy_remove(CONTROLLER.book, strOpid.mb_str(), contact);
    wxLogDebug(wxT("ManageContactsFrame::RemoveBuddy - addressbk_buddy_remove returned %d"),err);
    if( err != 0 )
    {
        ClearState(State_WaitingForUpdate);
        SetDatabaseError( err, wxT(""), true );
        return err;
    }

    // Wait for it.........
    err = WaitForClear( State_WaitingForUpdate, 20, _("Timeout while removing buddy") );
    if( err )
    {
        return err;
    }

    wxLogDebug(wxT("ManageContactsFrame::RemoveBuddy - removed ok") );

    return err;
}

void ManageContactsFrame::OnSignOffEvent(wxCommandEvent& event)
{
    m_ContactTree.RemoveContacts();

    event.Skip();
}

void ManageContactsFrame::OnClose( wxCloseEvent& event )
{
    // Clear the pointer to this instance owned by the MeetingCenterController
    *m_ppThis = 0;

    // Seeee Ya
    Destroy();
}

void ManageContactsFrame::OnMenuClose( wxCommandEvent& event )
{
    Close();
}

void ManageContactsFrame::OnExit( wxCommandEvent& event )
{
    Close();
    event.Skip();  // Event percolates up to meeting center controller to complete exit
}

void ManageContactsFrame::OnSize( wxSizeEvent& event )
{
    wxGetApp().Options().SaveScreenMetrics( "ManageContactsFrame", *this );
    event.Skip();
}

void ManageContactsFrame::OnMove( wxMoveEvent& event )
{
    wxGetApp().Options().SaveScreenMetrics( "ManageContactsFrame", *this );
    event.Skip();
}

void ManageContactsFrame::OnMeetingCenter(wxCommandEvent& event)
{
    CONTROLLER.ShowMeetingCenterWindow();
}

int ManageContactsFrame::InviteItem(const wxChar *command, wxTreeItemId & item)
{
    int added = 0;

    switch ( m_ContactTree.GetNodeType(item))
    {
    case ContactTreeList::Node_UserDir:
        break;
    case ContactTreeList::Node_Group:
        // ToDo: invite individuals from group
        break;
    case ContactTreeList::Node_Contact:
    {
        ContactTreeItemData *pCData = (ContactTreeItemData *)m_ContactTree.Tree()->GetItemData( item );

        wxString userid(pCData->m_contact->userid, wxConvUTF8);
        wxString screen(pCData->m_contact->screenname, wxConvUTF8);

        wxString name(m_ContactTree.Tree()->GetItemText(item));

        CONTROLLER.Commands().Add(command, userid, screen, name);
        ++added;
        break;
    }
    case ContactTreeList::Node_SearchRoot:
        break;
    case ContactTreeList::Node_SearchResults:
        break;
    };

    return added;
}

void ManageContactsFrame::OnMeetingStart(wxCommandEvent& event)
{
    bool started = false;
    wxArrayTreeItemIds selections;
    size_t nSelected;
    m_ContactTree.GetSelectionInfo( selections, nSelected );

    for( size_t i = 0; i < nSelected; ++i )
    {
        wxTreeItemId item;
        item = selections[i];

        if (InviteItem(wxT("start-invite"), item))
            started = true;
    }

    // No one selected, just bring up dialog
    if (!started)
    {
        CONTROLLER.Commands().Add( wxT("start-invite") );
    }
    CONTROLLER.ExecutePendingCommands();
}

void ManageContactsFrame::OnMeetingSchedule(wxCommandEvent& event)
{
    bool started = false;
    wxArrayTreeItemIds selections;
    size_t nSelected;
    m_ContactTree.GetSelectionInfo( selections, nSelected );

    for( size_t i = 0; i < nSelected; ++i )
    {
        wxTreeItemId item;
        item = selections[i];

        if (InviteItem(wxT("schedule-invite"), item))
            started = true;
    }

    // No one selected, just bring up dialog
    if (!started)
    {
        CONTROLLER.Commands().Add( wxT("schedule-invite") );
    }
    CONTROLLER.ExecutePendingCommands();
}

void ManageContactsFrame::OnHelpContents(wxCommandEvent& event)
{
    wxString help( wxGetApp().Options().GetSystem( "HelpPath", "" ),  wxConvUTF8);
    help += LOCALE->GetCanonicalName( );
    help += wxT("/");
    help += wxT("contactbuddyconfiguration.htm");
    wxLaunchDefaultBrowser( help );
}

void ManageContactsFrame::OnHelpAbout(wxCommandEvent& event)
{
	AboutDialog about(this);
	about.ShowModal();
}

void ManageContactsFrame::OnHelpContext(wxCommandEvent& event)
{

}

void ManageContactsFrame::OnProfileManage(wxCommandEvent& event)
{
    goModeless();

    ProfileManagerDialog Dlg( this, wxID_ANY );
    Dlg.ManageProfiles();

    goModal();
}


void ManageContactsFrame::OnProfileFetched(wxCommandEvent& event)
{
    wxString opid;
    opid = event.GetString();

    wxLogDebug(wxT("ManageContactsFrame::OnProfileFetched opid=%s"),opid.c_str() );

    if( !CONTROLLER.IsOpcode( opid, MeetingCenterController::OP_ManageContacts, false /* Not exact match */ ) )
        event.Skip();

    // Populate the array of policy names
    m_arrPolicyNames.Clear( );

    profile_iterator_t      iter    = profiles_iterate( CONTROLLER.book );
    profile_t               profile = 0;
    while( (profile = profiles_next( CONTROLLER.book, iter ))  !=  0 )
    {
        wxString name( profile->name, wxConvUTF8 );
        m_arrPolicyNames.Add( name );
    }
}

pstring ManageContactsFrame::allocField( directory_t dir, const wxString & strField )
{
    if( !strField.IsEmpty() )
    {
        return directory_string( dir, strField.mb_str( wxConvUTF8 ) );
    }else
    {
        wxString blank(wxT(""));
        return directory_string( dir, blank.mb_str() );
    }
}


pstring ManageContactsFrame::allocField( directory_t dir, const char *strField )
{
    if( strField != NULL  &&  strlen( strField ) > 0 )
    {
        return directory_string( dir, strField );
    }
    else
    {
        wxString blank( wxT("") );
        return directory_string( dir, blank.mb_str() );
    }
}
