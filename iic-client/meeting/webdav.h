/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 *
 *
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */

#ifndef WEBDAV_H_INCLUDED
#define WEBDAV_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>

#ifdef __WXMSW__
#include "wininet.h"
#endif      //  #ifdef __WXMSW__

#include "../netlib/nethttp.h"

#ifdef MACOSX
#include <string>
#endif

class CWebDavUtils
{
#if __WXMSW__
    HINTERNET m_InternetSession;
    HINTERNET m_Connection;
#else
    HSESSION     m_InternetSession;
    HCONNECTION  m_Connection;
#endif      //  #ifdef __WXMSW__

    wxString     m_strHeaders;
    bool         m_bShowErrors;

    void SetInternetSecurityOptions( HREQUEST hRequest );
    bool SetAuthentication( HREQUEST hRequest, const wxString & userName, const wxString & password );

    static bool	  m_bIgnoreUnknownCA;
    static bool	  m_bIgnoreCertRev;
    static bool	  m_bEnableExternalAuth;
    static wxString m_strCAFile;
    static wxString m_strLastCert;

public:
    CWebDavUtils();
    ~CWebDavUtils();

    int    DAVRetrieveInfo(wxWindow* pWindow, const wxString& aUrl, const wxString& aUrlPath, const wxString& userName,
                            const wxString& passwd, wxString& outXML, wxString& strStatusCode, wxString& strStatusText);

    int    DAVLockUnlock(wxWindow* pWindow, const wxString& aUrl, const wxString& aUrlPath, const wxString& userName,
                          const wxString& passwd, const wxChar* lpLockToken, bool bLockUnlock, std::string& outXML,
                          wxString& strStatusCode, wxString& strStatusText);

    int    DAVCopyFolder(wxWindow* pWindow, const wxString& source, const wxString& aUrlPath, const wxString& destination,
                          const wxString& userName, const wxString& passwd,
                          std::string& outXML, wxString& strStatusCode, wxString& strStatusText);

    int    SendRequest( const wxString& szReq,
                         const wxString& strFileURL,
                         const wxString& aUrlPath,
                         const wxString& userName,
                         const wxString& passwd,
                         const unsigned char* pData,
                         unsigned long dwLength,
                         wxString& strStatusCode,
                         wxString& strStatusText,
                         bool bAddLockToken,
                         const wxString& strLockToken,
                         wxWindow* pWindow,
                         int* piChunk);

    int    RetrieveFile(const wxString& aUrl, const wxString& aUrlPath, wxString& out,
                         const wxString& userName, const wxString& passwd,
                         wxString& strStatusCode, wxString& strStatusText, bool aSaveToFile, wxWindow* pWindow);

    bool   HandleCertError(wxWindow* parent);

    void SetShowErrors(bool bFlag) 
    { 
        m_bShowErrors = bFlag; 
    }
    
    void   AddHeader(const wxString& aHeader)
    {
        m_strHeaders = aHeader;
    }

    static void SetEnableExternalAuth(bool aFlag)
    {
        m_bEnableExternalAuth = aFlag;
    }

    static void SetTrustedCA(const wxString& strTrustCA)
    {
        m_strCAFile = strTrustCA;
    }

private:
    bool OpenConnection(const wxString& aServerUrl, const wxString& userName, const wxString& passwd);

    void CloseConnection();

};


#endif // WEBDAV_H_INCLUDED
