/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"
#include "presetdata.h"
#include <wx/tokenzr.h>
#include <wx/combobox.h>
#include <wx/textdlg.h>

bool PresetData::Load()
{
    bool fOK = false;
    wxString str;
    wxString strName;
    wxString strData;
    wxString blank(wxT(""));

    m_Status.Clear();
    m_Data.Clear();
    m_Name.Clear();

    m_nLoadedEntries = 0;

    for( int ndx = 0; ndx < Max_Presets; ++ndx )
    {
        str = OPTIONS.GetUserString( Key(ndx).mb_str(wxConvUTF8), blank );
        if( str.IsEmpty() )
            break;

        // Split the name from the data
        int nSplit = str.Find('|');
        if( nSplit != wxNOT_FOUND )
        {
            strName = str.Left( nSplit );
            strData = str.Mid( nSplit + 1 );

            m_Name.Add( strName );
            m_Data.Add( strData );
            m_Status.Add( Preset_Loaded );

            ++m_nLoadedEntries;
        }
    }

    return fOK;
}


bool PresetData::Save()
{
    bool fOK = false;
    int ndx;

    // Wipe all previous entries:
    // All of the entries need to be trashed because we cannot handle
    // index values that are not sequential between 0..n
    for( int i = 0; i < GetCount(); ++i )
    {
        OPTIONS.ClrUser( Key(i).mb_str(wxConvUTF8) );
    }

    // Now save the new entries
    ndx = 0;
    for( int i = 0; i < GetCount(); ++i )
    {
        if( m_Status[i] != Preset_Removed )
        {
            wxString strOut;
            strOut = m_Name[i];
            strOut += wxT("|");
            strOut += m_Data[i];

            OPTIONS.SetUserString( Key(ndx).mb_str(wxConvUTF8), strOut );
            ++ndx;
        }
    }

    OPTIONS.Write();

    return fOK;
}

int PresetData::Lookup( const wxString & name )
{
    for( int ndx = 0; ndx < GetCount(); ++ndx )
    {
        if( name.CmpNoCase( m_Name[ndx] ) == 0 )
        {
            return ndx;
        }
    }
    return wxNOT_FOUND;
}

bool PresetData::Remove( int ndx )
{
    bool fOK = false;

    if( ndx >=0 && ndx < GetCount() )
    {
        m_Name[ndx] = wxT("");
        m_Status[ndx] = Preset_Removed;
        fOK = true;
    }

    return fOK;
}

void PresetData::Encode( const wxArrayString & data, wxString & strData )
{
    // Construct the output string
    strData = wxT("");
    for( size_t i = 0; i < data.GetCount(); ++i )
    {
        strData += data[i];
        if( i < (data.GetCount()-1) )
        {
            strData += wxT("|");
        }
    }
}

bool PresetData::Add( const wxString & name, const wxArrayString & data )
{
    bool fOK = false;
    wxString strData;

    Encode( data, strData );

    // See if the name already exists
    int ndx = Lookup( name );
    if( ndx != wxNOT_FOUND )
    {
        m_Status[ndx] = Preset_New;
        m_Data[ndx] = strData;
        fOK = true;
    }

    if( !fOK )
    {
        // Look for a free slot
        for( ndx = 0; ndx < GetCount(); ++ndx )
        {
            if( m_Status[ndx] == Preset_Removed )
            {
                m_Name[ndx] = name;
                m_Status[ndx] = Preset_New;
                m_Data[ndx] = strData;
                fOK = true;
                break;
            }
        }
    }

    if( !fOK )
    {
        // Append a new entry
        m_Name.Add( name );
        m_Status.Add( Preset_New );
        m_Data.Add( strData );
    }

    return fOK;
}

bool PresetData::DataHasChanged( const wxString & name, const wxArrayString & data )
{
    bool fDataHasChanged = false;

    int ndx = Lookup( name );
    if( ndx != wxNOT_FOUND )
    {
        wxString strData;
        Encode( data, strData );
        if( m_Data[ndx].CmpNoCase( strData ) != 0 )
            fDataHasChanged = true;
    }
    return fDataHasChanged;
}

bool PresetData::Retrieve( int ndx, wxArrayString & data  )
{
    bool fOK = false;

    if( ndx >= 0 && ndx < GetCount() )
    {
        wxString strData = m_Data[ndx];
        data.Clear();

        wxStringTokenizer tkz( strData, wxT("|"));

        while ( tkz.HasMoreTokens() )
        {
            wxString token = tkz.GetNextToken();
            data.Add( token );
        }
        fOK = true;
    }

    return fOK;
}

wxString PresetData::Key( int ndx )
{
    wxString ret;

    ret = wxString::Format(wxT("Preset-%s-%d"),m_strPrefix.c_str(),ndx);

    return ret;
}

void PresetData::PopulateComboBox( wxComboBox *pCombo )
{
    pCombo->Clear();
    for( int ndx = 0; ndx < GetCount(); ++ndx )
    {
        if( m_Status[ndx] != Preset_Removed )
        {
            pCombo->Append( m_Name[ndx] );
        }
    }
    // Always append a blank entry, and select it by default
    pCombo->Append(wxT(" "));
    pCombo->SetStringSelection(wxT(" "));
}

void PresetData::RemoveCurrentPreset( wxComboBox *pCombo )
{
    wxString strName;
    strName = pCombo->GetStringSelection();

    int ndx = Lookup( strName );
    if( ndx != wxNOT_FOUND )
    {
        // Remove the preset, repopulate the combo, select the blank entry.
        Remove( ndx );
        PopulateComboBox( pCombo );
    }
}

bool PresetData::HandleSaveAs( wxWindow *parent, wxComboBox *pCombo, const wxString & caption, const wxString & prompt, const wxArrayString & data )
{
    bool fSaved = false;
/*
 wxTextEntryDialog(wxWindow* parent, const wxString& message, const wxString& caption = "Please enter text",
 const wxString& defaultValue = "", long style = wxOK |  wxCANCEL |  wxCENTRE, const wxPoint& pos = wxDefaultPosition)
*/
    wxString strDefault;
    strDefault = pCombo->GetStringSelection();

    wxTextEntryDialog Dlg( parent, prompt, caption, strDefault );
    if( Dlg.ShowModal() == wxID_OK )
    {
        wxString strName;
        strName = Dlg.GetValue();
        if( !strName.IsEmpty() )
        {
            int ndx = Lookup( strName );
            Add( strName, data );
            if( ndx == wxNOT_FOUND )
            {
                PopulateComboBox( pCombo );
                pCombo->SetStringSelection( strName );
            }
            fSaved = true;
        }
    }
    return fSaved;
}

bool PresetData::HandleNewSelection( wxComboBox * pCombo, wxArrayString & data )
{
    bool fHaveNewData = false;
    wxString strName;
    strName = pCombo->GetStringSelection();
    int ndx = Lookup( strName );
    if( ndx != wxNOT_FOUND )
    {
        Retrieve( ndx, data );
        fHaveNewData = true;
    }
    return fHaveNewData;
}

