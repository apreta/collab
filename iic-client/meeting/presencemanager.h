/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef PRESENCEMANAGER_H_INCLUDED
#define PRESENCEMANAGER_H_INCLUDED

#include <string>
#include <map>

#include "funcptr.h"

class PresenceInfo
{
    std::wstring m_system;
    std::wstring m_screen;
    int m_presence;
    std::wstring m_status;
    int m_special;

public:
    enum
    {
        PresenceUnavailable = 0,
        PresenceAvailable,
        PresenceAway,
    };
    enum
    {
        SpecialNone = 0,
        SpecialPhone = 1,
        SpecialMeeting = 2,
    };

    PresenceInfo()
    {
        m_presence = 0;
    }

    PresenceInfo(const std::wstring& system, const std::wstring& screen, int pres, const std::wstring& status, int special) :
        m_system(system),
        m_screen(screen),
        m_presence(pres),
        m_status(status),
        m_special(special)
    {
    }

    PresenceInfo(const PresenceInfo& copy) :
        m_system(copy.m_system),
        m_screen(copy.m_screen),
        m_presence(copy.m_presence),
        m_status(copy.m_status),
        m_special(copy.m_special)
    {
    }

    int GetPresence() const                 { return m_presence; }
    const std::wstring& GetSystem() const   { return m_system; }
    const std::wstring& GetScreen() const   { return m_screen; }
    const std::wstring& GetStatus() const   { return m_status; }
    const int           GetSpecial() const  { return m_special; }
};

typedef std::map<std::wstring, PresenceInfo> PresenceList;


class PresenceHandler
{
public:
    virtual ~PresenceHandler() { }
    virtual void operator()(const PresenceInfo& info) = 0;
};

template<typename T> class PresenceHandlerPtr : public PresenceHandler
{
public:
    typedef void (T::*MemberFn)(const PresenceInfo& info);
    PresenceHandlerPtr(T *_obj, MemberFn _fn) : obj(_obj), fn(_fn) {  }
    void operator()(const PresenceInfo& info) { ((obj)->*(fn))(info); }
private:
    T *obj;
    MemberFn fn;
};


class PresenceManager
{
    PresenceList m_presenceList;
    std::auto_ptr<PresenceHandler> m_presenceCallback;

public:
    void Connect(PresenceHandler* callback);
    void Disconnect();

    void SetPresence(const std::wstring system, const std::wstring screen, int presence, const std::wstring status, int special);
    void RemovePresence(const std::wstring system, const std::wstring screen);
    void ResetPresence();

    PresenceInfo * GetPresence(const std::wstring system, const std::wstring screen);
};


#endif // PRESENCEMANAGER_H_INCLUDED
