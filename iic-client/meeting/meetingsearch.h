/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef _MEETINGSEARCH_H_
#define _MEETINGSEARCH_H_

#include <wx/arrstr.h>

class MeetingSearch
{
public:
    MeetingSearch();


    enum DateRanges
    {
        WithinNDays = 0,        /**< Search within the next/prev N days */
        BetweenDates,           /**< Search between dates */
        AllDates                /**< Seatch all dates */
    };

    /**
      * Encode the content of this instance into a wxArrayString
      */
    void Encode( wxArrayString & data ) const;

    /**
      * Encode the content of this instance into a wxString
      */
    void Encode( wxString & data ) const;

    /**
      * Decode a wxArrayString into the content of this instance.
      */
    void Decode( const wxArrayString & data );

    /**
      * Decode a wxString into the content of this instance.
      */
    void Decode( const wxString & data );

    /** Assignment */
    MeetingSearch & operator=( const MeetingSearch & rhs );

    /**
      * Format a search results title for this criteria.
      * @return wxString Return the formatted text.
      */
    wxString FormatDescription() const;

    /** The name of this search critera:<br>
      * NOTE: This name string is NOT included in the Encode() / Decode()
      * operations.
      */
    wxString m_strSearchName;

    int m_nDateRange;           /**< Date range: One of the DateRanges enums */
    bool m_fNextNDays;          /**< Next/Prev */
    int m_nDays;                /**< The number of days Next/Prev */
    int m_nStartDate;           /**< The start date */
    int m_nEndDate;             /**< The end date */

    bool m_fTitle;              /**< Search by title */
    wxString m_strTitle;        /**< The title text */

    bool m_fHostFirstName;      /**< Search by the host forst name */
    wxString m_strHostFirst;    /**< The host's first name */

    bool m_fHostLastName;       /**< Search by the host last name */
    wxString m_strHostLast;     /**< The host's last name */

    bool m_fMeetingId;          /**< Search by meeting id */
    wxString m_strMeetingId;    /**< The meeting id */

    bool m_fOnlyInProgress;     /**< Only show meeting in progress */

    int m_nMaxResults;          /**< The number of results */

};

#endif
