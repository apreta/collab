/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef SENDRECVPROGRESSDIALOG_H
#define SENDRECVPROGRESSDIALOG_H

//(*Headers(SendRecvProgressDialog)
#include <wx/stattext.h>
#include <wx/dialog.h>
#include <wx/gauge.h>
//*)
#include <wx/timer.h>


class SendRecvProgressDialog: public wxDialog
{
public:

    SendRecvProgressDialog(wxWindow* parent,wxWindowID id = -1);
    virtual ~SendRecvProgressDialog();

    //(*Identifiers(SendRecvProgressDialog)
    //*)
    bool    Show(bool show = true);
    void    SetReceiveMode(bool mode);
    void    SetTitle(const wxString& aTitle);
    void    SetLabelTitle(const wxString& aLabel);
    void    SetLabelTitle2(const wxString& aLabel);
    void    SetProgressRange(int aMax);
    void    IncrementProgressPos( );
    void    SetProgressPos(int aPos);
    void    SetProgressRange2(int aMax);
    void    SetProgressPos2(int aPos);
    bool    fCanceled( )                { return m_fCancel; }
    void    Cancel( )                   { m_fCancel = true; }
    void    ClearCancel( )              { m_fCancel = false; }
    void    StartAutoProgress(int aTimeout);
    void    StopAutoProgress();
    void    OnExecuteAutoTimer( wxTimerEvent &event );

protected:

    //(*Handlers(SendRecvProgressDialog)
    //*)
    void    OnCancel( wxCommandEvent& event );

    //(*Declarations(SendRecvProgressDialog)
    wxStaticText* m_pStaticText1;
    wxStaticText* m_pStaticText2;
    wxGauge* m_pGauge2;
    wxGauge* m_pGauge1;
    //*)

    bool            m_fReceiveMode;
    bool            m_fCancel;

private:
    wxTimer         m_AutoTimer;
    bool            m_fTimerAborted;

    DECLARE_EVENT_TABLE()
};

#endif
