/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 *
 *
 * All changes from ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */
// DrawingObjects.cpp: implementation of the CDrawingObjects class.
//
//////////////////////////////////////////////////////////////////////

#include "app.h"
#include "iicdoc.h"
#include "docshareview.h"

#include "drawingobjects.h"
#include "utils.h"
#include "bezier.h"
#include "dashline.h"

#include <wx/mstream.h>

typedef int wxRasterOperationMode;

const wxString DOType_Line( wxT("line") );
const wxString DOType_TextBox( wxT("textbox") );
const wxString DOType_Rectangle( wxT("rectangle") );
const wxString DOType_Ellipse( wxT("ellipse") );
const wxString DOType_Rectangle_Opaque( wxT("rectangle_opaque") );
const wxString DOType_Ellipse_Opaque( wxT("ellipse_opaque") );
const wxString DOType_ArrowLine (wxT("arrow_line") );
const wxString DOType_Image( wxT("image") );
const wxString DOType_CheckMark( wxT("check_mark") );
const wxString DOType_RadioMark( wxT("radio_mark") );
const wxString DOType_CurveLine( wxT("curve") );
const wxString DOType_HighLite( wxT("highlite") );


wxColour	CLine::m_crColorDefault = wxColour(0,0,0);
wxColour	CLine::m_crBkgndDefault(255,255,255);
wxColour	CLine::m_crTextColorDefault(0,0,0);
UINT		CLine::m_nLineSizeDefault	= 1;
UINT		CLine::m_nLineStyleDefault	= PS_SOLID;
int			CLine::m_nDefaultStyle		= ST_NONE;
wxFontData	CLine::m_fontDefault		= wxFontData( );
wxString	CLine::m_strFontFaceDefault;
int			CLine::m_nFontSizeDefault	= 8;
wxString	CLine::m_strFontStyleDefault;

wxColour	CCurveLine::m_crColorDefault(0,0,0);
wxColour	CCurveLine::m_crBkgndDefault(255,255,255);
wxColour	CCurveLine::m_crTextColorDefault(0,0,0);
UINT		CCurveLine::m_nLineSizeDefault	= 1;
UINT		CCurveLine::m_nLineStyleDefault	= PS_SOLID;
int			CCurveLine::m_nDefaultStyle		= ST_NONE;
wxFontData	CCurveLine::m_fontDefault		= wxFontData( );
wxString	CCurveLine::m_strFontFaceDefault;
int			CCurveLine::m_nFontSizeDefault	= 8;
wxString	CCurveLine::m_strFontStyleDefault;

wxColour	CHighLite::m_crColorDefault(255,255,0);
wxColour	CHighLite::m_crBkgndDefault(255,255,255);
wxColour	CHighLite::m_crTextColorDefault(0,0,0);
UINT		CHighLite::m_nLineSizeDefault	= 36;
UINT		CHighLite::m_nLineStyleDefault	= PS_SOLID;
int			CHighLite::m_nDefaultStyle		= ST_TRANSPARENT;
wxFontData	CHighLite::m_fontDefault		= wxFontData( );
wxString	CHighLite::m_strFontFaceDefault;
int			CHighLite::m_nFontSizeDefault	= 8;
wxString	CHighLite::m_strFontStyleDefault;

wxColour	CEllipse::m_crColorDefault(0,0,0);
wxColour	CEllipse::m_crBkgndDefault(0,192,192);
wxColour	CEllipse::m_crTextColorDefault(0,0,0);
UINT		CEllipse::m_nLineSizeDefault	= 1;
UINT		CEllipse::m_nLineStyleDefault	= PS_SOLID;
int			CEllipse::m_nDefaultStyle		= ST_NONE;
wxFontData	CEllipse::m_fontDefault			= wxFontData( );
wxString	CEllipse::m_strFontFaceDefault;
int			CEllipse::m_nFontSizeDefault	= 8;
wxString	CEllipse::m_strFontStyleDefault;

wxColour	CRectangle::m_crColorDefault(0,0,0);
wxColour	CRectangle::m_crBkgndDefault(0,192,192);
wxColour	CRectangle::m_crTextColorDefault(0,0,0);
UINT		CRectangle::m_nLineSizeDefault	= 1;
UINT		CRectangle::m_nLineStyleDefault	= PS_SOLID;
int			CRectangle::m_nDefaultStyle		= ST_NONE;
wxString	CRectangle::m_strFontFaceDefault;
int			CRectangle::m_nFontSizeDefault	= 8;
wxString	CRectangle::m_strFontStyleDefault;
wxFontData	CRectangle::m_fontDefault		= wxFontData( );

wxColour	CTextBox::m_crColorDefault(0,0,0);
wxColour	CTextBox::m_crBkgndDefault(255,255,255);
wxColour	CTextBox::m_crTextColorDefault(0,0,0);
UINT		CTextBox::m_nLineSizeDefault	= 1;
UINT		CTextBox::m_nLineStyleDefault	= PS_NULL;
int			CTextBox::m_nDefaultStyle		= ST_TRANSPARENT;
wxFontData	CTextBox::m_fontDefault			= wxFontData( );
wxString	CTextBox::m_strFontFaceDefault;
int			CTextBox::m_nFontSizeDefault	= 8;
wxString	CTextBox::m_strFontStyleDefault;

wxColour	CArrowLine::m_crColorDefault(0,0,0);
wxColour	CArrowLine::m_crBkgndDefault(255,255,255);
wxColour	CArrowLine::m_crTextColorDefault(0,0,0);
UINT		CArrowLine::m_nLineSizeDefault	= 1;
UINT		CArrowLine::m_nLineStyleDefault	= PS_SOLID;
int			CArrowLine::m_nDefaultStyle		= ST_NONE;
wxFontData	CArrowLine::m_fontDefault		= wxFontData( );
wxString	CArrowLine::m_strFontFaceDefault;
int			CArrowLine::m_nFontSizeDefault	= 8;
wxString	CArrowLine::m_strFontStyleDefault;

wxColour	CImage::m_crColorDefault(0,0,0);
wxColour	CImage::m_crBkgndDefault(255,255,255);
wxColour	CImage::m_crTextColorDefault(0,0,0);
UINT		CImage::m_nLineSizeDefault	= 1;
UINT		CImage::m_nLineStyleDefault	= PS_NULL;
int			CImage::m_nDefaultStyle		= ST_NONE;
wxFontData	CImage::m_fontDefault		= wxFontData( );
wxString	CImage::m_strFontFaceDefault;
int			CImage::m_nFontSizeDefault	= 8;
wxString	CImage::m_strFontStyleDefault;

wxColour	CCheckMark::m_crColorDefault(0,0,128);
wxColour	CCheckMark::m_crBkgndDefault(0,0,128);
wxColour	CCheckMark::m_crTextColorDefault(0,0,0);
UINT		CCheckMark::m_nLineSizeDefault	= 12;
UINT		CCheckMark::m_nLineStyleDefault	= PS_SOLID;
int			CCheckMark::m_nDefaultStyle		= ST_NONE;
wxFontData	CCheckMark::m_fontDefault		= wxFontData( );
wxString	CCheckMark::m_strFontFaceDefault;
int			CCheckMark::m_nFontSizeDefault	= 8;
wxString	CCheckMark::m_strFontStyleDefault;

wxColour	CRadioMark::m_crColorDefault(64,0,0);
wxColour	CRadioMark::m_crBkgndDefault(64,0,0);
wxColour	CRadioMark::m_crTextColorDefault(0,0,0);
UINT		CRadioMark::m_nLineSizeDefault	= 1;
UINT		CRadioMark::m_nLineStyleDefault	= PS_SOLID;
int			CRadioMark::m_nDefaultStyle		= ST_NONE;
wxFontData	CRadioMark::m_fontDefault		= wxFontData( );
wxString	CRadioMark::m_strFontFaceDefault;
int			CRadioMark::m_nFontSizeDefault	= 8;
wxString	CRadioMark::m_strFontStyleDefault;

int  CDrawingObjects::m_nGrid = 6;

#define ARROW_LENGTH	16
#define ARROW_WIDTH		6

double toRadians(double d)
{
	return d * 2 * 3.1415926 / 360;
}

double toDegrees(double r)
{
	return r * 360 / 2 / 3.1415926;
}


//*****************************************************************************
//*****************************************************************************
void IICRect::NormalizeRect( )
{
    long temp;
    if( left  >  right )
    {
        temp = left;
        left = right;
        right = temp;
    }
    if( top  >  bottom )
    {
        temp = top;
        top = bottom;
        bottom = temp;
    }
}


//*****************************************************************************
void IICRect::InflateRect( int x, int y )
{
    left -= x;
    top -= y;
    right += x;
    bottom += y;
}


//*****************************************************************************
void IICRect::InflateRect( double dblZoom )
{
    int iDelta;

    iDelta = (int)(( ((double) Width( )) * (1.0 - dblZoom) * 0.5) + 0.5);
    left += iDelta;
    right -= iDelta;

    iDelta = (int)(( ((double) Height( )) * (1.0 - dblZoom) * 0.5) + 0.5);
    top += iDelta;
    bottom -= iDelta;
}


//*****************************************************************************
void IICRect::ScaleRect( double dblZoom )
{
    left   = (int)((((double) left)   * dblZoom) + 0.5);
    top    = (int)((((double) top)    * dblZoom) + 0.5);
    right  = (int)((((double) right)  * dblZoom) + 0.5);
    bottom = (int)((((double) bottom) * dblZoom) + 0.5);
}


//*****************************************************************************
bool IICRect::EqualRect( IICRect& r1 )
{
    bool fRet = (left == r1.left  &&  top == r1.top  &&  right == r1.right  &&  bottom == r1.bottom );
    return fRet;
}


//*****************************************************************************
bool IICRect::UnionRect( IICRect& r1, IICRect& r2 )
{
    if( r1.IsRectEmpty( ) )
    {
        if( r2.IsRectEmpty( ) )
        {
            //  BOTH rectangles are empty!!
            left = top = right = bottom = 0;
            return false;
        }
        //  Only r1 is empty - copy r2
        left   = r2.left;
        top    = r2.top;
        right  = r2.right;
        bottom = r2.bottom;
        return true;
    }
    if( r2.IsRectEmpty( ) )
    {   //  Only r2 is empty - copy r1
        left   = r1.left;
        top    = r1.top;
        right  = r1.right;
        bottom = r1.bottom;
        return true;
    }
    //  Neither rectangle is empty
    r1.NormalizeRect( );
    r2.NormalizeRect( );
    left = MIN( r1.left, r2.left );
    top  = MIN( r1.top, r2.top );
    right = MAX( r1.right, r2.right );
    bottom = MAX( r1.bottom, r2.bottom );
    return true;
}


//*****************************************************************************
bool IICRect::PtInRect( const wxPoint& pt )
{
    bool    fRet = (pt.x >= left  &&  pt.x <= right  &&  pt.y >= top  &&  pt.y <= bottom );
    return fRet;
}


//*****************************************************************************
bool IICRect::IsRectEmpty( ) const
{
    return ( Width( ) <= 0  ||  Height( ) <= 0 );
}


//*****************************************************************************
IICRect IICRect::operator+(const wxPoint& pt)
{
    return IICRect( left + pt.x, top + pt.y, right + pt.x, bottom + pt.y );
}


//*****************************************************************************
void IICRect::operator+=(const wxPoint& pt)
{
    left   += pt.x;
    right  += pt.x;
    top    += pt.y;
    bottom += pt.y;
}


//*****************************************************************************
void IICRect::operator+=(const wxSize& sz)
{
    left   += sz.x;
    right  += sz.x;
    top    += sz.y;
    bottom += sz.y;
}


//*****************************************************************************
void IICRect::operator-=(const wxPoint& pt)
{
    left   -= pt.x;
    right  -= pt.x;
    top    -= pt.y;
    bottom -= pt.y;
}



//*****************************************************************************
//*****************************************************************************

CDrawingObjects::CDrawingObjects()
{
	m_pOwner = NULL;
}

void CDrawingObjects::Paint(wxDC* pDC, IICRect& rect, wxPoint& ptOffset)
{
	OBJLIST::iterator i;

    for (i =  m_listObj.begin(); i != m_listObj.end(); ++i)
	{
		CBaseDrawingObject* db = *i;

		if (db)
		{
			db->Paint(pDC, rect, ptOffset);
		}
	}
}

void CDrawingObjects::PaintSelection(wxDC* pDC, IICRect& rect, wxPoint ptOffset)
{
	OBJLIST::iterator i;
    for (i =  m_listSelected.begin(); i != m_listSelected.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;

		if (obj)
		{
			obj->SetMode(BOX_MODE_IDLE);
			obj->Paint(pDC, rect, ptOffset);
			obj->SetMode(BOX_MODE_SELECTED);
		}
	}
}

void CDrawingObjects::MoveSelection(wxPoint ptOffset)
{
	OBJLIST::iterator i;
    for (i =  m_listSelected.begin(); i != m_listSelected.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;

		if (obj && obj->IsMovable())
		{
			obj->Move(ptOffset);
		}
	}
}

wxSize CDrawingObjects::GetExtent( wxDC& dc )
{
	wxSize size(0,0);

	OBJLIST::iterator i;

    for( i = m_listObj.begin(); i != m_listObj.end(); ++i )
	{
		CBaseDrawingObject* db = *i;

		if( db )
		{
			IICRect rect = db->GetRect( );
			rect.NormalizeRect();
			if (rect.right > size.GetWidth( ))
                size.SetWidth( rect.right );
			if (rect.bottom > size.GetHeight( ))
                size.SetHeight( rect.bottom );
		}
	}

	return size;
}

void CDrawingObjects::GetXML(wxString& xml)
{
	xml += _T("<markup>\n");
	OBJLIST::iterator i;

    for (i =  m_listObj.begin(); i != m_listObj.end(); ++i)
	{
		CBaseDrawingObject* db = *i;

		if (db)
		{
			db->GetXML(xml);
		}
	}
	xml += _T("</markup>\n");
}

void CDrawingObjects::ResetPasteOffset()
{
	if (!IsClipboardEmpty())
	{
		OBJLIST::iterator i;
		for (i =  m_listClipboard.begin(); i != m_listClipboard.end(); ++i)
		{
			CBaseDrawingObject* obj = *i;
			if (obj)
			{
				obj->ResetPasteOffset();
			}
		}
	}
}

CDrawingObjects::~CDrawingObjects()
{
	DeleteAllItems();
}

void CDrawingObjects::UnSelect()
{
	OBJLIST::iterator i;

	if (m_listSelected.size()==0)
		return;

	for (i =  m_listSelected.begin(); i != m_listSelected.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;

		if (obj)
		{
			obj->SetMode(BOX_MODE_IDLE);
		}
	}

	m_listSelected.clear();
}

void CDrawingObjects::DeleteAllItems()
{
	m_listSelected.clear();

	OBJLIST::iterator i;

	for (i =  m_listObj.begin(); i != m_listObj.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;
		if (obj)
		{
			obj->Release();
		}
	}

	m_listObj.clear();

//	ClearClipboard();
	ClearUndoActions();
	ClearRedoActions();

	CRadioMark::Clear();
}


int CDrawingObjects::Add(CBaseDrawingObject* obj, bool fCanUndoRedo)
{
	if (Find(obj->GetID())==NULL)
	{
		m_listObj.push_back(obj);

		obj->SetOwner(this);
		wxString userId;
		m_pDocument->m_Meeting.GetMeetingUserID(userId);
		obj->SetAuthor(userId);

		if (fCanUndoRedo)
		{
			m_listSelected.clear();
			m_listSelected.push_back(obj);
			CAction * action = new CCreateAction(m_pDocument);
			action->Do(*this);
			AddAction(action, true, false);
		}
	}

	return 0;
}

void CDrawingObjects::Delete(CBaseDrawingObject *obj)
{
	m_listObj.remove(obj);	// NOTE: obj is not destroyed
	obj->SetMode(BOX_MODE_DELETING);

	obj->SetOwner(NULL);
}

CBaseDrawingObject* CDrawingObjects::Delete(const wxString& aID)
{
	CBaseDrawingObject * obj = Find(aID);
	if (obj)
	{
		obj->SetMode(BOX_MODE_DELETING);
		m_listObj.remove(obj);
		return obj;
	}

	return NULL;
}

CBaseDrawingObject* CDrawingObjects::Find(const wxString& aID)
{
	OBJLIST::iterator i;

	for (i = m_listObj.begin(); i != m_listObj.end(); ++i)
	{
		CBaseDrawingObject * obj = *i;

		if (obj->GetID() == aID)
		{
			return obj;
		}
	}

	return NULL;
}

void CDrawingObjects::BringToFront(IICRect& rect)
{
	OBJLIST::iterator i;

	rect.SetRectEmpty();

	if (m_listSelected.size()==0)
		return;

    for (i =  m_listSelected.begin(); i != m_listSelected.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;

		if (obj)
		{
			rect = obj->UnionRect(rect);

			m_listObj.remove(obj);
			m_listObj.push_back(obj);
		}
	}

	CAction* action = new CReOrderAction(m_pDocument, CAction::AT_BRINGTOFRONT);
	action->Do(*this);
	AddAction(action);
}

void CDrawingObjects::SendBack(IICRect& rect)
{
	OBJLIST::reverse_iterator i;

	rect.SetRectEmpty();

	if (m_listSelected.size()==0)
		return;

    for (i =  m_listSelected.rbegin(); i != m_listSelected.rend(); ++i)
	{
		CBaseDrawingObject* obj = *i;

		if (obj)
		{
			rect = obj->UnionRect(rect);

			m_listObj.remove(obj);
			m_listObj.push_front(obj);
		}
	}

	CAction* action = new CReOrderAction(m_pDocument, CAction::AT_SENDBACK);
	action->Do(*this);
	AddAction(action);
}

void CDrawingObjects::BringToFront(const wxString& strID)
{
	CBaseDrawingObject* obj = Find(strID);
	if (obj)
	{
		m_listObj.remove(obj);
		m_listObj.push_back(obj);
	}
}

void CDrawingObjects::SendBack(const wxString& strID)
{
	CBaseDrawingObject* obj = Find(strID);
	if (obj)
	{
		m_listObj.remove(obj);
		m_listObj.push_front(obj);
	}
}

void CDrawingObjects::GetSelectedRegion(IICRect& rect, bool bUnselect)
{
	rect.SetRectEmpty();

	OBJLIST::iterator i;

	if (m_listSelected.size()==0)
		return;

    for (i =  m_listSelected.begin(); i != m_listSelected.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;

		if (obj)
		{
			if (bUnselect)
			{
				obj->SetMode(BOX_MODE_IDLE);
			}
			rect = obj->UnionRect(rect);
		}
	}

	if (rect.Width() % 4)
	{
		rect.right += 4 - (rect.Width() % 4);
	}

	if (rect.Height() % 4)
	{
		rect.bottom += 4 - (rect.Width() % 4);
	}
}

bool CDrawingObjects::UnSelect(IICRect& rect)
{
	GetSelectedRegion(rect, TRUE);

	m_listSelected.clear();

	return TRUE;
}


void CDrawingObjects::SelectInRegion(IICRect rect)
{
	OBJLIST::iterator i;

	IICRect intersect;
	UnSelect(intersect);

	if (rect.IsRectEmpty())	// select all
	{
		for (i =  m_listObj.begin(); i != m_listObj.end(); ++i)
		{
			CBaseDrawingObject* obj = *i;

			if (obj)
			{
				SelectItem(obj);
			}
		}
		return;
	}

    for (i =  m_listObj.begin(); i != m_listObj.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;

		if (obj)
		{
			IICRect rc = obj->GetRect();

			rc.NormalizeRect();
			if (rc.Width()==0) rc.right = rc.left + 2;
			if (rc.Height()==0) rc.bottom = rc.top + 2;

			intersect.UnionRect(rect, rc);
			if (rect.EqualRect(intersect))
			{
				SelectItem(obj);
			}
		}
	}
}

void CDrawingObjects::SelectItem(CBaseDrawingObject *obj)
{
	if (obj)
	{
		OBJLIST::iterator i;

		for (i = m_listSelected.begin(); i != m_listSelected.end(); ++i)
		{
			CBaseDrawingObject * objSel = *i;

			if (objSel->GetID() == obj->GetID())
			{
				return;
			}
		}

		m_listSelected.push_back(obj);

		obj->SetMode(BOX_MODE_SELECTED);
	}
}

CBaseDrawingObject* CDrawingObjects::HitTest(const wxPoint point, int& flags)
{
	CBaseDrawingObject *obj = NULL;
	OBJLIST::reverse_iterator i;

    for (i =  m_listObj.rbegin(); i != m_listObj.rend(); ++i)
	{
		obj = *i;
		if (obj)
		{
			obj = obj->HitTest(point, flags);
			if (obj)
			{
				return obj;
			}
		}
	}

	return NULL;
}

void CDrawingObjects::OnEditCopy()
{
	OBJLIST::iterator i;

	ClearClipboard();

	for (i =  m_listSelected.begin(); i != m_listSelected.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;
		if (obj)
		{
			m_listClipboard.push_back(obj->Clone());
		}
	}

	ResetPasteOffset();
}

void CDrawingObjects::OnEditPaste(wxPoint& point)
{
	m_listSelected.clear();

	wxPoint offset(0x1000, 0x1000);
	OBJLIST::iterator i;

	if (point.x < 0 || point.y < 0)
	{
		point = offset;
	}
	else	// calculate the top-left corner position
	{
		for (i =  m_listClipboard.begin(); i != m_listClipboard.end(); ++i)
		{
			CBaseDrawingObject* obj = *i;
			if (obj)
			{
				IICRect rect = obj->GetRect();	// move the new object down a few points
				if (offset.x > rect.left)
				{
					offset.x = rect.left;
				}
				if (offset.y > rect.top)
				{
					offset.y = rect.top;
				}
			}
		}
	}

	for (i =  m_listClipboard.begin(); i != m_listClipboard.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;
		if (obj)
		{
			CBaseDrawingObject* newObj = obj->Clone(false, false);
			IICRect rect = newObj->GetRect();	// move the new object down a few points
			rect -= offset;
			rect += point;
			rect += obj->GetPasteOffset();
			newObj->SetRect(rect);
			newObj->SetMode(BOX_MODE_SELECTED);
			m_listSelected.push_back(newObj);	// Paste Action is relying on it
			m_listObj.push_back(newObj);
		}
	}

	CAction* action = new CPasteAction(m_pDocument);
	action->Do(*this);
	AddAction(action, true, false);
}

void CDrawingObjects::SendActionXML(bool aNotifyServer, const wxString& actionXML)
{
	if (aNotifyServer)
	{
		if (IsMeetingInProgress())
		{
			wxString meetingId;
			m_pDocument->m_Meeting.GetMeetingId(meetingId);
			m_pDocument->SendDrawOnWhiteBoard(meetingId, actionXML);
		}
	}
}

void CDrawingObjects::AddAction(CAction* action, bool aNotifyServer, bool fCompact)
{
	action->SendActionXML(false, fCompact, aNotifyServer, false);

	if (!aNotifyServer || !IsMeetingInProgress())
	{
		m_listUndo.push_back(action);
		m_listRedo.clear();
	}
	else
	{
		delete action;
	}
}

void CDrawingObjects::ClearClipboard()
{
	OBJLIST::iterator i;

	for (i =  m_listClipboard.begin(); i != m_listClipboard.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;
		if (obj)
		{
			obj->Release();
		}
	}

	m_listClipboard.clear();
}

void CDrawingObjects::ClearUndoActions()
{
	ACTIONLIST::iterator i;
	for (i =  m_listUndo.begin(); i != m_listUndo.end(); ++i)
	{
		CAction* action = *i;
		if (action)
		{
			delete action;
		}
	}

	m_listUndo.clear();
}

void CDrawingObjects::ClearRedoActions()
{
	ACTIONLIST::iterator i;
	for (i =  m_listRedo.begin(); i != m_listRedo.end(); ++i)
	{
		CAction* action = *i;
		if (action)
		{
			delete action;
		}
	}

	m_listRedo.clear();
}

void CDrawingObjects::OnEditDelete(bool aNotifyServer)
{
	CAction* action = new CDeleteAction(m_pDocument);
	action->Do(*this, true);	// use the committed attributes, in case one user is still dragging object
	AddAction(action, aNotifyServer);

	OBJLIST::iterator i;
    for (i =  m_listSelected.begin(); i != m_listSelected.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;

		if (obj)
		{
			if (!aNotifyServer || !IsMeetingInProgress())
			{
				Delete(obj);
				obj->Release();
			}
		}
	}

	m_listSelected.clear();
}

void CDrawingObjects::OnEditChange()
{
	CAction* action = new CChangeAction(m_pDocument);
	action->Do(*this);
	AddAction(action);
}

void CDrawingObjects::OnEditUndo(IICRect& rect)
{
	UnSelect(rect);

	if (m_listUndo.size()>0)
	{
		CAction* action = m_listUndo.back();
		if (action)
		{
			action->SendActionXML(true);

			m_listUndo.pop_back();
			action->Undo(*this, rect);
			m_listRedo.push_front(action);
		}
	}
}

void CDrawingObjects::OnEditRedo(IICRect& rect)
{
	UnSelect(rect);

	if (m_listRedo.size()>0)
	{
		CAction* action = m_listRedo.front();
		if (action)
		{
			action->SendActionXML();

			m_listRedo.pop_front();
			action->Redo(*this, rect);
			m_listUndo.push_back(action);
		}
	}
}

//*****************************************************************************
//*****************************************************************************

CBaseDrawingObject::CBaseDrawingObject()
{
    m_pOwner = 0;
	m_nAlignmentCommitted = m_nAlignment = DT_LEFT;

	m_ptPasteOffset.x = m_ptPasteOffset.y = 6;

	m_nRefCnt = 1;

	m_bDragging = false;
	m_nMode = BOX_MODE_IDLE;

	m_pts = NULL;
	m_pathPoints.clear( );
	m_pathTypes.clear( );
}

CBaseDrawingObject::CBaseDrawingObject(IICRect& rect)
{
    m_pOwner = 0;
	m_rcObj = rect;
	if (m_rcObj.left < 0)
		m_rcObj -= wxPoint(m_rcObj.left, 0);
	if (m_rcObj.top < 0)
		m_rcObj -= wxPoint(0, m_rcObj.top);

	m_rcObjCommitted = m_rcObj;
	m_rcOrigRectCommitted = m_rcOrigRect = m_rcObj;
	m_rcLastRect = m_rcObj;

	m_crColorCommitted = m_crColor = wxColour(0,0,0);
	m_crBkgndCommitted = m_crBkgnd = wxColour(255,255,255);
	m_crTextColorCommitted = m_crTextColor = wxColour(0,0,0);

	m_nStyleCommitted = m_nStyle = ST_NONE;

	m_nLineSizeCommitted = m_nLineSize = 1;
	m_nLineStyleCommitted = m_nLineStyle = PS_SOLID;

	m_nFontSizeCommitted = m_nFontSize = 12;
	m_nAlignmentCommitted = m_nAlignment = DT_LEFT;

	// generate UUID
	CUtils::GetUniqueID(m_strID);

	m_ptPasteOffset.x = m_ptPasteOffset.y = 6;

	m_nRefCnt = 1;

	m_bDragging = false;
	m_nMode = BOX_MODE_IDLE;

	m_pts = NULL;
	m_pathPoints.clear( );
	m_pathTypes.clear( );
}


CBaseDrawingObject::CBaseDrawingObject(CBaseDrawingObject& copy, bool aPreAction, bool fReuseID)
{
	m_pOwner	= copy.m_pOwner;
	if (fReuseID)
	{
		m_strID	= copy.m_strID;
	}
	else
	{
		CUtils::GetUniqueID(m_strID);
	}
	m_strType	= copy.m_strType;		// the element type
	m_strTextCommitted	= m_strText	= aPreAction?copy.m_strTextCommitted:copy.m_strText;		// Text box
	m_rcObjCommitted	= m_rcObj	= aPreAction?copy.m_rcOrigRectCommitted:copy.m_rcOrigRect;
	m_strAuthor = copy.m_strAuthor;
	m_nStyleCommitted	= m_nStyle	= aPreAction?copy.m_nStyleCommitted:copy.m_nStyle;

    m_logFont = copy.m_logFont;
	// Font attributes
	m_strFontFaceCommitted	= m_strFontFace	= aPreAction ? copy.m_strFontFaceCommitted  : copy.m_strFontFace;
	m_nFontSizeCommitted	= m_nFontSize	= aPreAction ? copy.m_nFontSizeCommitted    : copy.m_nFontSize;
	m_strFontStyleCommitted	= m_strFontStyle= aPreAction ? copy.m_strFontStyleCommitted : copy.m_strFontStyle;
	m_nAlignmentCommitted	= m_nAlignment	= aPreAction ? copy.m_nAlignmentCommitted   : copy.m_nAlignment;

	// Color attributes
	m_crColorCommitted		= m_crColor		= aPreAction ? copy.m_crColorCommitted     : copy.m_crColor;		// border color
	m_crBkgndCommitted		= m_crBkgnd		= aPreAction ? copy.m_crBkgndCommitted     : copy.m_crBkgnd;		// Background color
	m_crTextColorCommitted	= m_crTextColor	= aPreAction ? copy.m_crTextColorCommitted : copy.m_crTextColor;	// Text color

	// Operational attributes
	m_rcOrigRectCommitted	= m_rcOrigRect	= aPreAction ? copy.m_rcOrigRectCommitted : copy.m_rcOrigRect;	//used for dragging
	m_rcLastRect	        = aPreAction ? copy.m_rcOrigRectCommitted : copy.m_rcOrigRect;	//used for dragging

	m_nLineSizeCommitted	=m_nLineSize	= aPreAction ? copy.m_nLineSizeCommitted  : copy.m_nLineSize;
	m_nLineStyleCommitted	= m_nLineStyle	= aPreAction ? copy.m_nLineStyleCommitted : copy.m_nLineStyle;

	m_ptPasteOffset.x = m_ptPasteOffset.y = 6;

	m_points.assign(aPreAction ? copy.m_pointsCommitted.begin() : copy.m_points.begin(),
		aPreAction ? copy.m_pointsCommitted.end() : copy.m_points.end());
	m_pointsCommitted.assign(aPreAction ? copy.m_pointsCommitted.begin() : copy.m_points.begin(),
		aPreAction ? copy.m_pointsCommitted.end() : copy.m_points.end());

	m_nRefCnt = 1;
	m_bDragging = false;
	m_nMode = BOX_MODE_IDLE;

	m_pts = NULL;
	m_pathPoints.clear( );
	m_pathTypes.clear( );
}

void CBaseDrawingObject::CleanupPath()
{
	if (m_pts)
	{
		delete[] m_pts;
		m_pts = NULL;
	}
	if( !m_pathPoints.empty( ) )
	{
		m_pathPoints.clear( );
	}
	if( !m_pathTypes.empty( ) )
	{
		m_pathTypes.clear( );
	}
}

CBaseDrawingObject::~CBaseDrawingObject()
{
	CleanupPath();
}

int CBaseDrawingObject::AddRef()
{
	return ++m_nRefCnt;
}

int CBaseDrawingObject::Release()
{
	if (--m_nRefCnt==0)
	{
		delete this;
		return 0;
	}
	else
	{
		return m_nRefCnt;
	}
}

void CBaseDrawingObject::StrokePath( wxDC *pDC )
{

    POINTLIST::iterator i;
    BYTELIST::iterator  j;
    wxPoint             ptPrior;
    bool                fFirstMove  = true;

    for( i =  m_pathPoints.begin(), j =  m_pathTypes.begin(); i != m_pathPoints.end(); ++i, ++j)
    {
        wxPoint pt      = *i;
        BYTE    type    = *j;
        switch( type )
        {
            case PT_MOVETO:
                if( m_nLineStyle != PS_SOLID  ||  fFirstMove )
                {
                    ptPrior = pt;
                    fFirstMove = false;
                    break;
                }

            case PT_LINETO:
                pDC->DrawLine( ptPrior.x, ptPrior.y, pt.x, pt.y );
                ptPrior = pt;
                break;
            default:
                break;
        }
    }
}

void CBaseDrawingObject::DrawPathOutline(bool fRound, wxDC* pDC, wxPoint ptOffset, bool fStorePath, int aLineSize)
{
    if( m_pathPoints.empty( ) && fStorePath)
    {
//        wxLogDebug( wxT("in CBaseDrawingObject::DrawPathOutline( )  --  m_pathPoints list is already populated") );
	}

	// make pen and stroke path
/*    int alpha = 255;
    
    if (m_nStyle&ST_TRANSPARENT)
        alpha = 50;
        
    wxColour color(m_crColor.Red(), m_crColor.Green(), m_crColor.Blue(), alpha);
*/
    wxPen Pen( m_crColor, aLineSize, (m_nLineStyle!=PS_NULL ? wxSOLID : wxTRANSPARENT) );
    wxPen OldPen = pDC->GetPen( );
    pDC->SetPen( Pen );
    StrokePath( pDC );
	pDC->SetPen( OldPen );
}

IICRect CBaseDrawingObject::UnionRect(IICRect rect)
{
	IICRect rect2;
	IICRect rect1 = GetOrigRect();
	rect1.NormalizeRect();
	rect1.InflateRect(GetLineSize(),GetLineSize());	// vert, horz line does not have any size
	if (m_strType==DOType_ArrowLine)
	{
		rect1.InflateRect(ARROW_WIDTH, ARROW_WIDTH);
	}
	rect2.UnionRect(rect, rect1);
	return rect2;
}

wxPoint CBaseDrawingObject::GetPasteOffset()
{
	wxPoint pt = m_ptPasteOffset;
	m_ptPasteOffset.x += 6;
	m_ptPasteOffset.y += 6;
	return pt;
}

void CBaseDrawingObject::SetRect(IICRect rect)
{
	CleanupPath();

	m_rcObj = rect;

	// recalc the points
	if (m_points.size()>0)
	{
		POINTLIST pts;
		pts.assign(m_points.begin(), m_points.end());
		m_points.clear();
		POINTLIST::iterator ip;
		wxPoint offset = m_rcObj.TopLeft() - m_rcOrigRect.TopLeft();
		for (ip =  pts.begin(); ip != pts.end(); ++ip)
		{
			m_points.push_back(*ip + offset);
		}
	}

	m_rcOrigRect	= m_rcObj;
	m_rcLastRect	= m_rcObj;
}

void CBaseDrawingObject::Move(wxPoint ptOffset)
{
	CleanupPath();

	m_rcObj	+= ptOffset;
	if (m_rcObj.left < 0)
		m_rcObj -= wxPoint(m_rcObj.left, 0);
	if (m_rcObj.top < 0)
		m_rcObj -= wxPoint(0, m_rcObj.top);

	// recalc the points
	if (m_points.size()>0)
	{
		POINTLIST pts;
		pts.assign(m_points.begin(), m_points.end());
		m_points.clear();
		POINTLIST::iterator ip;
		wxPoint offset = m_rcObj.TopLeft() - m_rcOrigRect.TopLeft();
		for (ip =  pts.begin(); ip != pts.end(); ++ip)
		{
			m_points.push_back(*ip + offset);
		}
	}

	m_rcOrigRect	= m_rcObj;
	m_rcLastRect	= m_rcObj;
}

CBaseDrawingObject* CBaseDrawingObject::HitTest(const wxPoint pt, int& flags)
{
	IICRect rc = m_rcOrigRect;
	rc.bottom += 1;

	if (IsVResizable() || IsHResizable())
	{
		flags = OnSide(pt, rc);
		if (flags)
			return this;
	}

	int lineSize = m_nLineSize/2;
	if (lineSize < 4) lineSize = 4;

	if( m_pathPoints.empty( ) )	//  || IsSelected())
	{
		rc.InflateRect(lineSize, lineSize);
		if (rc.PtInRect(pt))
			return this;
	}
	else
    {
        POINTLIST::iterator iterPt;
        iterPt   =  m_pathPoints.begin( );

        wxPoint         ptPrior;
        ptPrior = *iterPt;
        ++iterPt;
        IICRect         fullRect( ptPrior, ptPrior );
        int             iCount = 0;
        for( ; iterPt != m_pathPoints.end(); ++iterPt )
        {
            iCount++;
            wxPoint ptCurr  = *iterPt;

            IICRect rect( ptPrior, ptCurr );
            rect -= m_ptOffset;
            rect.NormalizeRect( );
            rect.InflateRect( lineSize, lineSize );
            fullRect.UnionRect( fullRect, rect );
//            if( rect.PtInRect( pt ) )
//                return this;
            if( fullRect.PtInRect( pt ) )
                return this;
            ptPrior = ptCurr;
        }

	}

	return NULL;
}

wxSize CBaseDrawingObject::GetExtent(wxDC* pDC)
{
	return m_rcOrigRect.Size();
}

void CBaseDrawingObject::BeginPaint(wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset)
{
}

void CBaseDrawingObject::EndPaint(wxDC* pDC)
{
}

void snap_to_grid(wxPoint& pt)
{
	int x1, x2, y1, y2;
	int x, y;
	int dis, min;

	x1 = pt.x - (pt.x%CDrawingObjects::m_nGrid);
	x2 = (pt.x + CDrawingObjects::m_nGrid -1) / CDrawingObjects::m_nGrid * CDrawingObjects::m_nGrid;

	y1 = pt.y - (pt.y%CDrawingObjects::m_nGrid);
	y2 = (pt.y + CDrawingObjects::m_nGrid -1) / CDrawingObjects::m_nGrid * CDrawingObjects::m_nGrid;


	x = x1;
	y = y1;
	min = abs((pt.x - x1) * (pt.x - x1)) + abs((pt.y - y1) * (pt.y - y1));
	dis = abs((pt.x - x1) * (pt.x - x1)) + abs((pt.y - y2) * (pt.y - y2));
	if (min > dis)
	{
		min = dis;
		x = x1;
		y = y2;
	}

	dis = abs((pt.x - x2) * (pt.x - x2)) + abs((pt.y - y1) * (pt.y - y1));
	if (min > dis)
	{
		min = dis;
		x = x2;
		y = y1;
	}

	dis = abs((pt.x - x2) * (pt.x - x2)) + abs((pt.y - y2) * (pt.y - y2));
	if (min > dis)
	{
		min = dis;
		x = x2;
		y = y2;
	}

	pt.x = x;
	pt.y = y;
}

void CBaseDrawingObject::Drag( wxDC* pDC, wxPoint& pIn, wxPoint offset, double dblZoom )
{
    wxPen   pen( *wxBLACK, 1, wxDOT );
    IICRect rcDD = m_rcLastRect;

    wxRasterOperationMode rop = pDC->GetLogicalFunction( );
    pDC->SetLogicalFunction( wxEQUIV );
    int BkMode = pDC->GetBackgroundMode( );
    pDC->SetBackgroundMode( wxTRANSPARENT );
    wxPen   OldPen = pDC->GetPen( );
    pDC->SetPen( pen );

    if(m_bDragging)
	{
        if( GetType() == DOType_Line  ||  GetType() == DOType_ArrowLine )
            DrawLine( pDC, rcDD, dblZoom );
        else
            DrawBox( pDC, rcDD, dblZoom );
	}

    wxPoint p( pIn );
    p -= offset;

	if( p.x < 0 ) p.x = 0;
	if( p.y < 0 ) p.y = 0;

	// snap to grid
	snap_to_grid( p );

	IICRect     New( p, m_rcObj.Size( ) );
	rcDD = New;

	if( GetType() == DOType_Line  ||  GetType() == DOType_ArrowLine )
		DrawLine( pDC, rcDD, dblZoom );
    else
        DrawBox( pDC, rcDD, dblZoom );

    pDC->SetPen( OldPen );
    pDC->SetBackgroundMode( BkMode );
    pDC->SetLogicalFunction( rop );

	m_rcLastRect = New;
	m_rcObj = New;
	m_bDragging = true;
}

void CBaseDrawingObject::Drop(wxDC* pDC, wxPoint& p, wxPoint offset)
{
	if (m_pOwner->GetOwner())
	{
		IICRect rc = m_rcOrigRect;
		rc.NormalizeRect();

		rc.InflateRect( SZ_RESIZEBOX+GetLineSize(), SZ_RESIZEBOX+GetLineSize() );

        m_pOwner->GetOwner( )->InvalidateSelection( rc, false );

        wxLogDebug( wxT("in Drop( ) - invalidating rect (%d, %d) - (%d, %d)"),
                    rc.left, rc.top, rc.right, rc.bottom );
	}

	p.x -= offset.x;
	p.y -= offset.y;

	if (p.x < 0) p.x = 0;
	if (p.y < 0) p.y = 0;

	// snap to grid
	snap_to_grid(p);

	p -= m_rcObj.TopLeft();

	Move(p);

	if (!m_pOwner->IsMeetingInProgress())
	{
		m_rcObjCommitted = m_rcOrigRectCommitted = m_rcObj;
	}

	m_bDragging = false;
}


void CBaseDrawingObject::Resize(wxDC* pDC, wxPoint& point, int Side, wxPoint offset, double dblZoom )
{
    wxPen pen( *wxBLACK, 1, wxDOT );

    IICRect rcDD = m_rcLastRect;

	IICRect temp = m_rcLastRect;

	if(m_bDragging)
	{
       	wxRasterOperationMode rop = pDC->GetLogicalFunction( );
        pDC->SetLogicalFunction( wxEQUIV );
        int BkMode = pDC->GetBackgroundMode( );
        pDC->SetBackgroundMode( wxTRANSPARENT );
        wxPen   OldPen = pDC->GetPen( );
        pDC->SetPen( pen );

		if(GetType()==DOType_Line||GetType()==DOType_ArrowLine)
			DrawLine( pDC,rcDD, dblZoom );
		else
			DrawBox( pDC, rcDD, dblZoom );

        pDC->SetPen( OldPen );
        pDC->SetBackgroundMode( BkMode );
        pDC->SetLogicalFunction( rop );
	}


	point.x -= offset.x;
	point.y -= offset.y;

	if (point.x < 0) point.x = 0;
	if (point.y < 0) point.y = 0;

	// snap to grid
	snap_to_grid(point);

	switch (Side)
	{
		case BOTTOMLEFT_SIDE:
			if (point.x < m_rcObj.right)
				temp.left = point.x;
			if (point.y > m_rcObj.top)
				temp.bottom = point.y;
			break;

		case TOPLEFT_SIDE:
			if (point.x < m_rcObj.right)
				temp.left = point.x;
			if (point.y < m_rcObj.bottom)
				temp.top = point.y;
			break;

		case LEFT_SIDE:
			if (point.x < m_rcObj.right)
				temp.left = point.x;
			break;

		case TOPRIGHT_SIDE:
			if (point.x > m_rcObj.left)
				temp.right = point.x;
			if (point.y <  m_rcObj.bottom)
				temp.top = point.y;
			break;

		case BOTTOMRIGHT_SIDE:
			if (point.x > m_rcObj.left)
				temp.right = point.x;
			if (point.y > m_rcObj.top)
				temp.bottom = point.y;
			break;

		case RIGHT_SIDE:
			if (point.x > m_rcObj.left)
				temp.right = point.x;
			break;

		case TOP_SIDE:
			if (point.y < m_rcObj.bottom)
				temp.top = point.y;
			break;

		case BOTTOM_SIDE:
			if (point.y > m_rcObj.top)
				temp.bottom = point.y;
			break;
		case LINE_START:
			temp.left = point.x;
			temp.top = point.y;
			if (abs(temp.left-temp.right)<SZ_RESIZEBOX)
				temp.left = temp.right;
			if (abs(temp.top-temp.bottom)<SZ_RESIZEBOX)
				temp.top = temp.bottom;
			break;
		case LINE_END:
			temp.right = point.x;
			temp.bottom = point.y;
			if (abs(temp.left-temp.right)<3)
				temp.right = temp.left;
			if (abs(temp.top-temp.bottom)<3)
				temp.bottom = temp.top;
			break;
	}

    rcDD = temp;

    wxRasterOperationMode rop = pDC->GetLogicalFunction( );
    pDC->SetLogicalFunction( wxEQUIV );
    int BkMode = pDC->GetBackgroundMode( );
    pDC->SetBackgroundMode( wxTRANSPARENT );
    wxPen   OldPen = pDC->GetPen( );
    pDC->SetPen( pen );

	if(GetType()==DOType_Line||GetType()==DOType_ArrowLine)
		DrawLine( pDC,rcDD, dblZoom );
	else
		DrawBox( pDC, rcDD, dblZoom );

    pDC->SetPen( OldPen );
    pDC->SetBackgroundMode( BkMode );
    pDC->SetLogicalFunction( rop );

	m_bDragging = true;
	m_rcLastRect = temp;
	m_rcObj = temp;

}

void CBaseDrawingObject::EndResize(wxDC* pDC, wxPoint& point, int Side, wxPoint offset)
{
	CleanupPath();

	if (m_pOwner->GetOwner())
	{
		IICRect rc = m_rcOrigRect;
		rc.NormalizeRect();
		rc.InflateRect(SZ_RESIZEBOX+GetLineSize(),SZ_RESIZEBOX+GetLineSize());
        m_pOwner->GetOwner( )->InvalidateSelection( rc, false );
	}

	IICRect temp = m_rcLastRect;

	point.x -= offset.x;
	point.y -= offset.y;

	if (point.x < 0) point.x = 0;
	if (point.y < 0) point.y = 0;
	// snap to grid
	snap_to_grid(point);

	switch (Side)
	{
		case BOTTOMLEFT_SIDE:
			if (point.x < m_rcObj.right)
				temp.left = point.x;
			if (point.y > m_rcObj.top)
				temp.bottom = point.y;
			break;

		case TOPLEFT_SIDE:
			if (point.x < m_rcObj.right)
				temp.left = point.x;
			if (point.y < m_rcObj.bottom)
				temp.top = point.y;
			break;

		case LEFT_SIDE:
			if (point.x < m_rcObj.right)
				temp.left = point.x;
			break;

		case TOPRIGHT_SIDE:
			if (point.x > m_rcObj.left)
				temp.right = point.x;
			if (point.y <  m_rcObj.bottom)
				temp.top = point.y;
			break;

		case BOTTOMRIGHT_SIDE:
			if (point.x > m_rcObj.left)
				temp.right = point.x;
			if (point.y > m_rcObj.top)
				temp.bottom = point.y;
			break;

		case RIGHT_SIDE:
			if (point.x > m_rcObj.left)
				temp.right = point.x;
			break;

		case TOP_SIDE:
			if (point.y < m_rcObj.bottom)
				temp.top = point.y;
			break;

		case BOTTOM_SIDE:
			if (point.y > m_rcObj.top)
				temp.bottom = point.y;
			break;

		case LINE_START:
			temp.left = point.x;
			temp.top = point.y;
			if (abs(temp.left-temp.right)<SZ_RESIZEBOX)
				temp.left = temp.right;
			if (abs(temp.top-temp.bottom)<SZ_RESIZEBOX)
				temp.top = temp.bottom;
			break;
		case LINE_END:
			temp.right = point.x;
			temp.bottom = point.y;
			if (abs(temp.left-temp.right)<SZ_RESIZEBOX)
				temp.right = temp.left;
			if (abs(temp.top-temp.bottom)<SZ_RESIZEBOX)
				temp.bottom = temp.top;
			break;
	}

	// recalc the points
	if (m_points.size()>0)
	{
		POINTLIST pts;
		pts.assign(m_points.begin(), m_points.end());
		m_points.clear();
		POINTLIST::iterator ip;
		double zoomX = 1.0;
		double zoomY = 1.0;
		if (m_rcOrigRect.Width()!=0) zoomX = (double)temp.Width() / m_rcOrigRect.Width();
		if (m_rcOrigRect.Height()!=0) zoomY = (double)temp.Height() / m_rcOrigRect.Height();
		for (ip =  pts.begin(); ip != pts.end(); ++ip)
		{
			int x = (int)(((*ip).x - m_rcOrigRect.left) * zoomX + temp.left);
			int y = (int)(((*ip).y - m_rcOrigRect.top) * zoomY + temp.top);
			m_points.push_back(wxPoint(x, y));
		}
	}

	m_rcObj = temp;
	m_rcLastRect = m_rcObj;
	m_rcOrigRect = m_rcObj;
	m_bDragging = false;

	if (!m_pOwner->IsMeetingInProgress())
	{
		m_rcObjCommitted = m_rcOrigRectCommitted = m_rcObj;
	}
}

#define SIZE_THRESHOLD 1


int CBaseDrawingObject::OnSide(const wxPoint &P, IICRect rc)
{
	int side = NO_SIDE;

	int nHandleCount = 8;
	for (int nHandle = nHandleCount; nHandle >= 1; nHandle --)
	{
		IICRect rect = GetBorder(nHandle,rc);
		if(rect.PtInRect(P))
		{
			switch(nHandle)
			{
				case 1: side = TOPLEFT_SIDE;
					if(!IsVResizable()) side = LEFT_SIDE;
					break;
				case 2:side = TOP_SIDE;
					if(!IsVResizable()) side = NO_SIDE;
					break;
				case 3:	side = TOPRIGHT_SIDE;
					if(!IsVResizable()) side = RIGHT_SIDE;
					break;
				case 4: side = RIGHT_SIDE;
					break;
				case 5:	side = BOTTOMRIGHT_SIDE;
					if(!IsVResizable()) side = RIGHT_SIDE;
					break;
				case 6: side = BOTTOM_SIDE;
					if(!IsVResizable()) side = NO_SIDE;
					break;
				case 7:	side = BOTTOMLEFT_SIDE;
					if(!IsVResizable()) side = LEFT_SIDE;
					break;
				case 8:	side = LEFT_SIDE;
					break;
				default:side = NO_SIDE;
					break;
			}
			break;
		}
	}
	return side;
}

// returns logical coords of center of handle
wxPoint CBaseDrawingObject::GetHandle(int nHandle, IICRect rcDD)
{
	int x, y, xCenter, yCenter;

	// this gets the center regardless of left/right and top/bottom ordering
	xCenter = rcDD.left + rcDD.Width() / 2;
	yCenter = rcDD.top + rcDD.Height() / 2;

	switch (nHandle)
	{
	default:
	case 1:
		x = rcDD.left - SZ_RESIZEBOX/2;
		y = rcDD.top - SZ_RESIZEBOX/2;
		break;

	case 2:
		x = xCenter-SZ_RESIZEBOX/2;
		y = rcDD.top - SZ_RESIZEBOX/2;
		break;

	case 3:
		x = rcDD.right-SZ_RESIZEBOX/2;
		y = rcDD.top-SZ_RESIZEBOX/2;
		break;

	case 4:
		x = rcDD.right-SZ_RESIZEBOX/2;
		y = yCenter-SZ_RESIZEBOX/2;
		break;

	case 5:
		x = rcDD.right-SZ_RESIZEBOX/2;
		y = rcDD.bottom-SZ_RESIZEBOX/2;
		break;

	case 6:
		x = xCenter-SZ_RESIZEBOX/2;
		y = rcDD.bottom-SZ_RESIZEBOX/2;
		break;

	case 7:
		x = rcDD.left-SZ_RESIZEBOX/2;
		y = rcDD.bottom-SZ_RESIZEBOX/2;
		break;

	case 8:
		x = rcDD.left-SZ_RESIZEBOX/2;
		y = yCenter-SZ_RESIZEBOX/2;
		break;
	}

	return wxPoint(x, y);
}

// returns logical coords of center of handle
IICRect CBaseDrawingObject::GetBorder(int nHandle, IICRect rc)
{
	int x=0, y=0, w=0, h=0;
	int xCenter, yCenter;

	// this gets the center regardless of left/right and top/bottom ordering
	xCenter = rc.left + rc.Width() / 2;
	yCenter = rc.top + rc.Height() / 2;

	w = SZ_RESIZEBOX+1;
	h = SZ_RESIZEBOX+1;

	switch (nHandle)
	{
	default:
	case 1:
		x = rc.left - SZ_RESIZEBOX/2;
		y = rc.top - SZ_RESIZEBOX/2;
		break;

	case 2:
		x = xCenter-(SZ_RESIZEBOX+1)/2;
		y = rc.top - SZ_RESIZEBOX/2;
		break;

	case 3:
		x = rc.right-SZ_RESIZEBOX/2;
		y = rc.top - SZ_RESIZEBOX/2;
		break;

	case 4:
		x = rc.right-SZ_RESIZEBOX/2;
		y = yCenter-(SZ_RESIZEBOX+1)/2;
		break;

	case 5:
		x = rc.right-SZ_RESIZEBOX/2;
		y = rc.bottom-SZ_RESIZEBOX/2;
		break;

	case 6:
		x = xCenter-(SZ_RESIZEBOX+1)/2;
		y = rc.bottom-SZ_RESIZEBOX/2;
		break;

	case 7:
		x = rc.left-SZ_RESIZEBOX/2;
		y = rc.bottom-SZ_RESIZEBOX/2;
		break;

	case 8:
		x = rc.left-SZ_RESIZEBOX/2;
		y = yCenter-(SZ_RESIZEBOX+1)/2;
		break;
	}

	return IICRect(x, y, x+w, y+h);
}

int CBaseDrawingObject::operator < ( const CBaseDrawingObject& o ) const
{
	int nRet = FALSE;
	if(m_rcObj.top != o.m_rcObj.top)
		nRet = (o.m_rcObj.top > m_rcObj.top);
	else if(m_rcObj.left != o.m_rcObj.left)
		nRet = (o.m_rcObj.left > m_rcObj.left);
	else
	{
		nRet = (o.m_rcObj.Width() > m_rcObj.Width());
	}
	return nRet;
}

void CBaseDrawingObject::Assign(const CBaseDrawingObject& copy )
{
	m_strTextCommitted	= m_strText	= copy.m_strTextCommitted;		// Text box
	m_rcObjCommitted	= m_rcObj	= copy.m_rcOrigRectCommitted;
	m_nStyleCommitted	= m_nStyle	= copy.m_nStyleCommitted;

	// Font attributes
	m_strFontFaceCommitted	= m_strFontFace	= copy.m_strFontFaceCommitted;
	m_nFontSizeCommitted	= m_nFontSize	= copy.m_nFontSizeCommitted;
	m_strFontStyleCommitted	= m_strFontStyle= copy.m_strFontStyleCommitted;
	m_nAlignmentCommitted	= m_nAlignment	= copy.m_nAlignmentCommitted;

	// Color attributes
	m_crColorCommitted		= m_crColor		= copy.m_crColorCommitted;		// border color
	m_crBkgndCommitted		= m_crBkgnd		= copy.m_crBkgndCommitted;		// Background color
	m_crTextColorCommitted	= m_crTextColor	= copy.m_crTextColorCommitted;	// Text color

	// Operational attributes
	m_rcOrigRectCommitted	= m_rcOrigRect	= copy.m_rcOrigRectCommitted;	//used for dragging
	m_rcLastRect	= copy.m_rcOrigRectCommitted;	//used for dragging

	m_nLineSizeCommitted	=m_nLineSize	= copy.m_nLineSizeCommitted;
	m_nLineStyleCommitted	= m_nLineStyle	= copy.m_nLineStyleCommitted;

	m_ptPasteOffset.x = m_ptPasteOffset.y = 6;

	m_points.clear();
	m_pointsCommitted.clear();
	CleanupPath();
	m_points.assign(copy.m_pointsCommitted.begin(), copy.m_pointsCommitted.end());
	m_pointsCommitted.assign(copy.m_pointsCommitted.begin(), copy.m_pointsCommitted.end());

	m_nRefCnt = 1;
	m_bDragging = false;
	m_nMode = BOX_MODE_IDLE;
}

void CBaseDrawingObject::DrawBox(wxDC* pDC, IICRect& rectIn, double dblZoom, wxPoint ptOffset )
{
    IICRect rect( rectIn );
    rect.ScaleRect( dblZoom );
    pDC->DrawLine( rect.left, rect.top, rect.right, rect.top );
    pDC->DrawLine( rect.right, rect.top, rect.right, rect.bottom );
    pDC->DrawLine( rect.right, rect.bottom, rect.left, rect.bottom );
    pDC->DrawLine( rect.left, rect.bottom, rect.left, rect.top );
}

void CBaseDrawingObject::DrawBox(wxDC* pDC, IICRect& rectIn, wxBrush* br, wxPoint ptOffset, double dblZoom )
{
    wxBrush     Old = pDC->GetBrush( );
    pDC->SetBrush( *br );
    IICRect rect( rectIn );
    rect.ScaleRect( dblZoom );
    pDC->DrawLine( rect.left, rect.top, rect.right, rect.top );
    pDC->DrawLine( rect.right, rect.top, rect.right, rect.bottom );
    pDC->DrawLine( rect.right, rect.bottom, rect.left, rect.bottom );
    pDC->DrawLine( rect.left, rect.bottom, rect.left, rect.top );
    pDC->SetBrush( Old );
}

//void CBaseDrawingObject::DrawBox2(wxDC* pDC, IICRect rect)
//{
//	DrawBox(pDC, rect);
//}

void Draw3dBox(wxDC* pDC, IICRect rect)
{
    wxLogDebug( wxT("ENTERING Draw3dBox( )  --  NEED TO IMPLEMENT THIS IF IT IS ACTUALLY USED") );
}

void CBaseDrawingObject::DrawTracker2(wxDC* pDC, IICRect rect, CBaseDrawingObject* pBox)
{
    wxPoint     handle      = GetHandle(1, rect);
    wxBrush     OldBrush    = pDC->GetBrush( );
    wxBrush     NewBrush( wxColour( 127, 127, 127 ) );
    pDC->SetBrush( NewBrush );
    pDC->DrawRectangle( handle.x, handle.y, SZ_RESIZEBOX, SZ_RESIZEBOX );
	handle = GetHandle(5, rect);
    pDC->DrawRectangle( handle.x, handle.y, SZ_RESIZEBOX, SZ_RESIZEBOX );

	handle = GetHandle(3, rect);
    pDC->DrawRectangle( handle.x, handle.y, SZ_RESIZEBOX, SZ_RESIZEBOX );
	handle = GetHandle(7, rect);
    pDC->DrawRectangle( handle.x, handle.y, SZ_RESIZEBOX, SZ_RESIZEBOX );

	if (pBox->IsVResizable())
	{
		handle = GetHandle(2, rect);
        pDC->DrawRectangle( handle.x, handle.y, SZ_RESIZEBOX, SZ_RESIZEBOX );

		handle = GetHandle(6, rect);
        pDC->DrawRectangle( handle.x, handle.y, SZ_RESIZEBOX, SZ_RESIZEBOX );
	}

	if (pBox->IsHResizable())
	{
		handle = GetHandle(4, rect);
        pDC->DrawRectangle( handle.x, handle.y, SZ_RESIZEBOX, SZ_RESIZEBOX );

		handle = GetHandle(8, rect);
        pDC->DrawRectangle( handle.x, handle.y, SZ_RESIZEBOX, SZ_RESIZEBOX );
	}
    pDC->SetBrush( OldBrush );
}

void CBaseDrawingObject::DrawLine(wxDC* pDC, IICRect& rectIn, double dblZoom, wxPoint ptOffset )
{
    IICRect rect( rectIn );
    rect.ScaleRect( dblZoom );
    pDC->DrawLine( rect.left, rect.top, rect.right, rect.bottom );
}


void CBaseDrawingObject::DrawTracker(wxDC* pDC, int state, IICRect& toDraw, wxPoint ptOffset)
{
	BeginPaint(pDC, toDraw, ptOffset);

	if (state == BOX_MODE_SELECTED)
	{
		IICRect rcDD = m_rcObj + toDraw.TopLeft();
		switch (state)
		{
		default:
			break;

		case BOX_MODE_SELECTED:
			{
				CBaseDrawingObject::DrawTracker2( pDC, rcDD, this);

				break;
			}

		}
	}

	EndPaint(pDC);
}

void CBaseDrawingObject::GetXML(wxString& xml, bool fCompact)
{
	wxString temp;

	xml += _T("<id>");
	xml += m_strID;
	xml += _T("</id>\n");

	xml += _T("<type>");
	xml += m_strType;
	xml += _T("</type>\n");

	xml += _T("<author>");
	xml += m_strAuthor;
	xml += _T("</author>\n");

	xml += _T("<note>");
	temp = m_strText;
	CUtils::EncodeXMLString(temp);
	xml += temp;
	xml += _T("</note>\n");

	temp = wxString::Format(_T("<style>%d</style>\n"), m_nStyle);
	xml += temp;

	temp = wxString::Format(_T("<x1>%d</x1>\n"),m_rcOrigRect.left);
	xml += temp;
	temp = wxString::Format(_T("<y1>%d</y1>\n"),m_rcOrigRect.top);
	xml += temp;
	temp = wxString::Format(_T("<x2>%d</x2>\n"),m_rcOrigRect.right);
	xml += temp;
	temp = wxString::Format(_T("<y2>%d</y2>\n"),m_rcOrigRect.bottom);
	xml += temp;

    int     iVal;
    iVal = (m_crColor.Blue( )<<16) + (m_crColor.Green( )<<8) + m_crColor.Red( );
    temp = wxString::Format(_T("<forecolor>%d</forecolor>\n"), iVal );
	xml += temp;

    iVal = (m_crBkgnd.Blue( )<<16) + (m_crBkgnd.Green( )<<8) + m_crBkgnd.Red( );
    temp = wxString::Format(_T("<bkcolor>%d</bkcolor>\n"), iVal );
	xml += temp;

    iVal = (m_crTextColor.Blue( )<<16) + (m_crTextColor.Green( )<<8) + m_crTextColor.Red( );
    temp = wxString::Format(_T("<textcolor>%d</textcolor>\n"), iVal );
	xml += temp;

	xml += _T("<font-face>");
	xml += m_strFontFace;
	xml += _T("</font-face>\n");

	xml += _T("<font-style>");
	xml += m_strFontStyle;
	xml += _T("</font-style>\n");

	temp = wxString::Format(_T("<font-size>%d</font-size>\n"),m_nFontSize);
	xml += temp;

	temp = wxString::Format(_T("<align>%d</align>\n"),m_nAlignment);
	xml += temp;

	temp = wxString::Format(_T("<line-size>%d</line-size>\n"),m_nLineSize);
	xml += temp;
	temp = wxString::Format(_T("<line-style>%d</line-style>\n"),m_nLineStyle);
	xml += temp;

	xml += _T("<points>\n");
	if (m_points.size() > 0)
	{
		POINTLIST::iterator ip;
		for (ip=m_points.begin(); ip!=m_points.end(); ip++)
		{
			xml += _T("<point>\n");
			temp = wxString::Format(_T("<x>%d</x>\n"),(*ip).x);
			xml += temp;
			temp = wxString::Format(_T("<y>%d</y>\n"),(*ip).y);
			xml += temp;
			xml += _T("</point>\n");
		}
	}
	xml += _T("</points>\n");
}

void CBaseDrawingObject::SetAttributes(iks *y)
{
    m_strID = wxString( iks_find_cdata (y, "id"), wxConvUTF8 );
    m_strAuthor = wxString( iks_find_cdata (y, "author"), wxConvUTF8 );
    m_strText = wxString( iks_find_cdata (y, "note"), wxConvUTF8 );
	CUtils::DecodeXMLString(m_strText);
	m_strTextCommitted = m_strText;
	int x1, y1, x2, y2;
	x1 = atoi(iks_find_cdata (y, "x1"));
	y1 = atoi(iks_find_cdata (y, "y1"));
	x2 = atoi(iks_find_cdata (y, "x2"));
	y2 = atoi(iks_find_cdata (y, "y2"));
	m_rcObjCommitted = m_rcObj =  IICRect(x1,y1,x2,y2);
    m_rcOrigRectCommitted = m_rcOrigRect = m_rcLastRect = m_rcObj;

    int     iVal = atoi(iks_find_cdata (y, "bkcolor"));
    m_crBkgnd.Set( (0xFF & iVal), (0xFF & iVal >> 8), (0xFF & iVal >> 16) );
    m_crBkgndCommitted = m_crBkgnd;
    iVal = atoi(iks_find_cdata (y, "forecolor"));
    m_crColor.Set( (0xFF & iVal), (0xFF & iVal >> 8), (0xFF & iVal >> 16) );
    m_crColorCommitted = m_crColor;
    char* value = iks_find_cdata (y, "textcolor");
    if( value )
    {
        iVal = atoi( value );
        m_crTextColor.Set( (0xFF & iVal), (0xFF & iVal >> 8), (0xFF & iVal >> 16) );
        m_crTextColorCommitted = m_crTextColor;
    }
    else
    {
        m_crTextColorCommitted = m_crTextColor = m_crColor;
    }
	m_nStyleCommitted = m_nStyle = atoi(iks_find_cdata (y, "style"));
	m_nLineStyleCommitted = m_nLineStyle = atoi(iks_find_cdata (y, "line-style"));
	m_nLineSizeCommitted = m_nLineSize = atoi(iks_find_cdata (y, "line-size"));
    m_strFontFaceCommitted = m_strFontFace = wxString( iks_find_cdata (y, "font-face"), wxConvUTF8 );
    m_strFontStyleCommitted = m_strFontStyle = wxString( iks_find_cdata (y, "font-style"), wxConvUTF8 );
	m_nFontSizeCommitted = m_nFontSize = atoi(iks_find_cdata (y, "font-size"));

	m_points.clear();
	CleanupPath();

	y = iks_find(y, "points");
	if (y)
	{
		y = iks_child(y);
		while (y)
		{
			if (iks_type (y) == IKS_TAG && strcmp (iks_name (y), "point") == 0)
			{
				x1 = atoi(iks_find_cdata (y, "x"));
				y1 = atoi(iks_find_cdata (y, "y"));
				m_points.push_back(wxPoint(x1,y1));
			}
			y = iks_next(y);
		}
	}
	m_pointsCommitted.clear();
	m_pointsCommitted.assign(m_points.begin(), m_points.end());
}

void CBaseDrawingObject::ToggleTransparency(bool bTransparent, bool bSetDefault)
{
	if (bTransparent)
		m_nStyle = m_nStyle | ST_TRANSPARENT;
	else
		m_nStyle = m_nStyle ^ ST_TRANSPARENT;
}


///////////////////////////////////////////////////////////////////////////////
CLine::CLine(IICRect& rect)
	: CBaseDrawingObject(rect)
{
	m_strType = DOType_Line;

	m_nStyleCommitted		= m_nStyle		= CLine::m_nDefaultStyle;
	m_nLineStyleCommitted	= m_nLineStyle	= CLine::m_nLineStyleDefault;
	m_nLineSizeCommitted	= m_nLineSize	= CLine::m_nLineSizeDefault;

	m_crColorCommitted		= m_crColor		= CLine::m_crColorDefault;
	m_crBkgndCommitted		= m_crBkgnd		= CLine::m_crBkgndDefault;
	m_crTextColorCommitted	= m_crTextColor	= CLine::m_crTextColorDefault;

}

CLine::CLine(CLine& copy, bool aPreAction, bool fReuseID)
	: CBaseDrawingObject(copy, aPreAction, fReuseID)
{
}

CLine::~CLine()
{
}

wxSize CLine::GetExtent(wxDC* pDC)
{
	return CBaseDrawingObject::GetExtent(pDC);
}

void CLine::DrawDragPoints(wxDC* pDC, IICRect rect)
{
	if (m_nMode == BOX_MODE_SELECTED)
	{
        wxBrush     OldBrush    = pDC->GetBrush( );
        wxBrush     NewBrush( wxColour( 127, 127, 127 ) );
        pDC->SetBrush( NewBrush );

		wxPoint handle = wxPoint(rect.left-SZ_RESIZEBOX/2, rect.top-SZ_RESIZEBOX/2);
        pDC->DrawRectangle( handle.x, handle.y, SZ_RESIZEBOX, SZ_RESIZEBOX );
		handle = wxPoint(rect.right-SZ_RESIZEBOX/2, rect.bottom-SZ_RESIZEBOX/2);;
        pDC->DrawRectangle( handle.x, handle.y, SZ_RESIZEBOX, SZ_RESIZEBOX );
        pDC->SetBrush( OldBrush );
	}
}

int CLine::Paint(wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset)
{
	BeginPaint(pDC, toDraw, ptOffset);

    wxRasterOperationMode rop = pDC->GetLogicalFunction( );
    pDC->SetLogicalFunction( (m_nStyle&ST_TRANSPARENT) ? wxAND : wxCOPY );
    int BkMode = pDC->GetBackgroundMode( );
    pDC->SetBackgroundMode( wxTRANSPARENT );

	IICRect rc = m_rcObj + toDraw.TopLeft();

	DrawLineEx(pDC, rc, toDraw.TopLeft());

	// restore DC settings
    pDC->SetBackgroundMode( BkMode );
    pDC->SetLogicalFunction( rop );

	DrawDragPoints(pDC, rc);

	EndPaint(pDC);
	return 0;
}

void CLine::GetXML(wxString& xml, bool fCompact)
{
	wxString temp;

	xml += _T("<item>\n");
	CBaseDrawingObject::GetXML(xml, fCompact);

	xml += _T("</item>\n");
}

CBaseDrawingObject* CLine::HitTest(const wxPoint pt, int& flags)
{
	flags = NO_SIDE;
	bool irregular=true;
	int extended = SZ_RESIZEBORDER + m_nLineSize/2;

	if ((m_rcOrigRect.left < m_rcOrigRect.right && m_rcOrigRect.top < m_rcOrigRect.bottom) ||
		(m_rcOrigRect.left > m_rcOrigRect.right && m_rcOrigRect.top > m_rcOrigRect.bottom))
	{
		irregular = false;
	}

	IICRect rcOrigRect = m_rcOrigRect;
	IICRect rc = IICRect(rcOrigRect.right - extended,
		rcOrigRect.bottom - extended, rcOrigRect.right + extended,
		rcOrigRect.bottom + extended);
	if (rc.PtInRect(pt))
	{
		flags = LINE_END;
		return this;
	}
	rc = IICRect(rcOrigRect.left - extended,
		rcOrigRect.top - extended, rcOrigRect.left + extended,
		rcOrigRect.top + extended);
	if (rc.PtInRect(pt))
	{
		flags = LINE_START;
		return this;
	}

	rcOrigRect.NormalizeRect();
	if (rcOrigRect.Width()<=extended||rcOrigRect.Height()<=extended)
	{
		rc = IICRect(rcOrigRect.left - extended,
			rcOrigRect.top - extended, rcOrigRect.right + extended,
			rcOrigRect.bottom + extended);
		if (rc.PtInRect(pt))
		{
			return this;
		}
		return NULL;
	}

	if (rcOrigRect.PtInRect(pt))
	{
		int y1 = (pt.x - rcOrigRect.left - extended) * rcOrigRect.Height() / rcOrigRect.Width();
		if (irregular)
			y1 = rcOrigRect.bottom - y1;
		else
			y1 += rcOrigRect.top;
		int y2 = (pt.x - rcOrigRect.left + extended) * rcOrigRect.Height() / rcOrigRect.Width();
		if (irregular)
		{
			y2 = rcOrigRect.bottom - y2;
			if (pt.y>=y2 && pt.y<=y1)
			{
				return this;
			}
		}
		else
		{
			y2 += rcOrigRect.top;
			if (pt.y>=y1 && pt.y<=y2)
			{
				return this;
			}
		}
	}

	return NULL;
}

CBaseDrawingObject* CLine::Clone(bool aPreAction, bool fReuseID)
{
	CLine* clone = new CLine(*this, aPreAction, fReuseID);
	return clone;
}

///////////////////////////////////////////////////////////////////////////////
CCurveLine::CCurveLine(IICRect& rect)
	: CLine(rect)
{
	m_strType = DOType_CurveLine;

	m_nStyleCommitted		= m_nStyle		= CCurveLine::m_nDefaultStyle;
	m_nLineStyleCommitted	= m_nLineStyle	= CCurveLine::m_nLineStyleDefault;
	m_nLineSizeCommitted	= m_nLineSize	= CCurveLine::m_nLineSizeDefault;

	m_crColorCommitted		= m_crColor		= CCurveLine::m_crColorDefault;
	m_crBkgndCommitted		= m_crBkgnd		= CCurveLine::m_crBkgndDefault;
	m_crTextColorCommitted	= m_crTextColor	= CCurveLine::m_crTextColorDefault;

	m_points.push_back(rect.TopLeft());
	m_pointsCommitted.push_back(rect.TopLeft());
}

CCurveLine::CCurveLine(CCurveLine& copy, bool aPreAction, bool fReuseID)
	: CLine(copy, aPreAction, fReuseID)
{
}

CCurveLine::~CCurveLine()
{
}

int CCurveLine::Paint(wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset)
{
	BeginPaint(pDC, toDraw, ptOffset);

    wxRasterOperationMode rop = pDC->GetLogicalFunction( );
    pDC->SetLogicalFunction( (m_nStyle&ST_TRANSPARENT) ? wxAND : wxCOPY );
    int BkMode = pDC->GetBackgroundMode( );
    pDC->SetBackgroundMode( wxTRANSPARENT );

	IICRect rc = m_rcObj + toDraw.TopLeft();

	unsigned Type[8];
	int Pattern = CDashLine::GetPattern(Type, true, m_nLineSize, m_nLineStyle);

	CDashLine Line(*pDC, Type, Pattern);

	// do drawing inside path for win95/8 compatibility
	int i;
	int size = m_points.size();
	if (((size-1)%3) != 0)
	{
		size += 3 - ((size-1)%3);
	}

	if (m_pts == NULL)
	{
		m_pts = new wxPoint[size];
		POINTLIST::iterator ip;
		for (i=0, ip =  m_points.begin(); ip != m_points.end(); ++ip, i++)
		{
			m_pts[i] = *ip + toDraw.TopLeft();
		}
		for (int j=i;j<size;j++)
		{
			m_pts[j] = m_pts[i-1];
		}
	}

    wxPen pen( *wxBLACK, 0, wxTRANSPARENT );
    wxPen   OldPen = pDC->GetPen( );
    pDC->SetPen( pen );

    BeginPath( );
	Line.MoveTo( m_pts[0].x, m_pts[0].y );
    m_pathPoints.push_back( m_pts[0] );
    m_pathTypes.push_back( (BYTE) PT_MOVETO );
	for( i = 1; i < size; i+=3 )
		Line.BezierTo( m_pts+i, m_pathPoints, m_pathTypes );
    EndPath( );

    pDC->SetPen( OldPen );

	// make pen and stroke path
	DrawPathOutline(true, pDC, toDraw.TopLeft(), TRUE, m_nLineSize);

	// restore DC settings
    pDC->SetBackgroundMode( BkMode );
    pDC->SetLogicalFunction( rop );

	if (!m_bDragging)
	{
		DrawTracker(pDC, m_nMode, toDraw);
	}

	EndPaint(pDC);
	return 0;
}

CBaseDrawingObject* CCurveLine::HitTest(const wxPoint pt, int& flags)
{
	flags = NO_SIDE;
	return CBaseDrawingObject::HitTest(pt, flags);
}

CBaseDrawingObject* CCurveLine::Clone(bool aPreAction, bool fReuseID)
{
	CCurveLine* clone = new CCurveLine(*this, aPreAction, fReuseID);
	return clone;
}

void CCurveLine::AddPoint(wxPoint point, bool endpoint)
{
	if (point.x < 0) point.x = 0;
	if (point.y < 0) point.y = 0;

	if (point.x < m_rcObj.left) m_rcObj.left = point.x;
	if (point.x > m_rcObj.right) m_rcObj.right = point.x;
	if (point.y < m_rcObj.top) m_rcObj.top = point.y;
	if (point.y > m_rcObj.bottom) m_rcObj.bottom = point.y;

	m_rcOrigRect = m_rcObj;
	m_rcLastRect = m_rcObj;

	if (!m_pOwner->IsMeetingInProgress())
	{
		m_rcObjCommitted = m_rcOrigRectCommitted = m_rcObj;
	}

	if (endpoint || m_points.size()==0)
	{
		m_points.push_back(point);
		if (!m_pOwner->IsMeetingInProgress())
		{
			m_pointsCommitted.push_back(point);
		}
		CleanupPath();
	}
	else
	{
		wxPoint last = m_points.back();
		int dis = abs(point.x-last.x) * abs(point.x-last.x) + abs(point.y-last.y) * abs(point.y-last.y);
		if (dis > 900)
		{
			m_points.push_back(point);
			if (!m_pOwner->IsMeetingInProgress())
			{
				m_pointsCommitted.push_back(point);
			}
			CleanupPath();
		}
	}

	m_bDragging = !endpoint;
}

///////////////////////////////////////////////////////////////////////////////
CHighLite::CHighLite(IICRect& rect)
	: CCurveLine(rect)
{
	m_strType = DOType_HighLite;

	m_nStyleCommitted		= m_nStyle		= CHighLite::m_nDefaultStyle;
	m_nLineStyleCommitted	= m_nLineStyle	= CHighLite::m_nLineStyleDefault;
	m_nLineSizeCommitted	= m_nLineSize	= CHighLite::m_nLineSizeDefault;

	m_crColorCommitted		= m_crColor		= CHighLite::m_crColorDefault;
	m_crBkgndCommitted		= m_crBkgnd		= CHighLite::m_crBkgndDefault;
	m_crTextColorCommitted	= m_crTextColor	= CHighLite::m_crTextColorDefault;

	m_points.push_back(rect.TopLeft());
}

CHighLite::CHighLite(CHighLite& copy, bool aPreAction, bool fReuseID)
	: CCurveLine(copy, aPreAction, fReuseID)
{
}

CHighLite::~CHighLite()
{
}

CBaseDrawingObject* CHighLite::Clone(bool aPreAction, bool fReuseID)
{
	CHighLite* clone = new CHighLite(*this, aPreAction, fReuseID);
	return clone;
}

//////////////////////////////////////////////////////////////////////////////
// Assume caller has created a pen with size=1 && right color && style
// and rect is normailized
void CBaseDrawingObject::DrawLineEx(wxDC* pDC, const IICRect& rect, wxPoint ptOffset)
{
	unsigned Type[8];
	int Pattern = CDashLine::GetPattern(Type, true, m_nLineSize, m_nLineStyle);

	// save the current pen before using DashLine
    wxPen   OldPen = pDC->GetPen( );
	CDashLine Line(*pDC, Type, Pattern);

	// do drawing inside path for win95/8 compatibility
    BeginPath( );
    Line.MoveTo(rect.left,  rect.top);
    m_pathPoints.push_back( wxPoint( rect.left,  rect.top ) );
    m_pathTypes.push_back( (BYTE) PT_MOVETO );
	Line.LineTo(rect.right, rect.bottom, m_pathPoints, m_pathTypes );
    EndPath( );

	// make pen and stroke path
	DrawPathOutline(true, pDC, ptOffset, FALSE, m_nLineSize);

    pDC->SetPen( OldPen );
}

void CBaseDrawingObject::DrawRectangle(wxDC* pDC, const IICRect& rect, wxPoint ptOffset, bool fill)
{
	if (rect.IsRectEmpty())
		return;

	if (fill)
	{
        wxPen pen( *wxBLACK, 0, wxTRANSPARENT );	// Cannot use FillRect no transparent
        wxPen   OldPen = pDC->GetPen( );
        pDC->SetPen( pen );

		IICRect rc = rect;
		rc.right +=1;
		rc.bottom += 1;
        pDC->DrawRectangle( rc.left, rc.top, rc.Width( ), rc.Height( ) );

        pDC->SetPen( OldPen );
	}

	unsigned Type[8];
	int Pattern = CDashLine::GetPattern(Type, true, m_nLineSize, m_nLineStyle);

    wxPen pen( *wxBLACK, 0, wxTRANSPARENT );	// Cannot use FillRect no transparent
    wxPen   OldPen = pDC->GetPen( );
    pDC->SetPen( pen );

    CDashLine Line(*pDC, Type, Pattern);

	// do drawing inside path for win95/8 compatibility
    BeginPath( );
	Line.MoveTo(rect.left,  rect.bottom );
    m_pathPoints.push_back( wxPoint( rect.left,  rect.bottom ) );
    m_pathTypes.push_back( (BYTE) PT_MOVETO );
	Line.LineTo(rect.right, rect.bottom, m_pathPoints, m_pathTypes );
	Line.LineTo(rect.right, rect.top,    m_pathPoints, m_pathTypes );
	Line.LineTo(rect.left,  rect.top,    m_pathPoints, m_pathTypes );
	Line.LineTo(rect.left,  rect.bottom, m_pathPoints, m_pathTypes );
    EndPath( );

    pDC->SetPen( OldPen );

	// make pen and stroke path
	DrawPathOutline(true, pDC, ptOffset, FALSE, m_nLineSize);
}

void CBaseDrawingObject::BeginPath( )
{
    m_pathPoints.clear( );
    m_pathTypes.clear( );
}

void CBaseDrawingObject::EndPath( )
{
}



///////////////////////////////////////////////////////////////////////////////
CArrowLine::CArrowLine(IICRect& rect)
	: CLine(rect)
{
	m_strType = DOType_ArrowLine;

	m_nStyleCommitted		= m_nStyle		= CArrowLine::m_nDefaultStyle;
	m_nLineStyleCommitted	= m_nLineStyle	= CArrowLine::m_nLineStyleDefault;
	m_nLineSizeCommitted	= m_nLineSize	= CArrowLine::m_nLineSizeDefault;
	m_crColorCommitted		= m_crColor		= CArrowLine::m_crColorDefault;
	m_crBkgndCommitted		= m_crBkgnd		= CArrowLine::m_crBkgndDefault;
	m_crTextColorCommitted	= m_crTextColor	= CArrowLine::m_crTextColorDefault;
}

CArrowLine::CArrowLine(CArrowLine& copy, bool aPreAction, bool fReuseID)
	: CLine(copy, aPreAction, fReuseID)
{
}

CArrowLine::~CArrowLine()
{
}

// ARROW COORDS: Load x-y value arrays */
void CArrowLine::ArrowCoords(int x1, int y1, int x2, int y2, int x3, int y3)
{
	m_ptValues[0].x = x1;
	m_ptValues[0].y = y1;
	m_ptValues[1].x = x2;
	m_ptValues[1].y = y2;
	m_ptValues[2].x = x3;
	m_ptValues[2].y = y3;
}

/* CALCULATE COORDINATES: Determine new x-y coords given a start x-y and
a distance and direction */
void CArrowLine::CalcCoords(int index, int x, int y, double dist, double dirn)
{
	while(dirn < 0.0)   dirn = 360.0+dirn;
	while(dirn > 360.0) dirn = dirn-360.0;

	// North-East
	if (dirn <= 90.0)
	{
		m_ptValues[index].x = x + (int) (sin(toRadians(dirn))*dist);
		m_ptValues[index].y = y - (int) (cos(toRadians(dirn))*dist);
		return;
	}
	// South-East
	if (dirn <= 180.0)
	{
		m_ptValues[index].x = x + (int) (cos(toRadians(dirn-90))*dist);
		m_ptValues[index].y = y + (int) (sin(toRadians(dirn-90))*dist);
		return;
	}
	// South-West
	if (dirn <= 90.0)
	{
		m_ptValues[index].x = x - (int) (sin(toRadians(dirn-180))*dist);
		m_ptValues[index].y = y + (int) (cos(toRadians(dirn-180))*dist);
	}
	// Nort-West
	else
	{
		m_ptValues[index].x = x - (int) (cos(toRadians(dirn-270))*dist);
		m_ptValues[index].y = y - (int) (sin(toRadians(dirn-270))*dist);
	}
}

/* CALCULATE VALUES QUADRANTS: Calculate x-y values where direction is not
parallel to eith x or y axis. */
void CArrowLine::CalcValuesQuad(int x1, int y1, int x2, int y2)
{
	double arrowAng = toDegrees (atan((double) ARROW_WIDTH/(double) ARROW_LENGTH));
	double dist = sqrt((double)(ARROW_LENGTH*ARROW_LENGTH + ARROW_WIDTH * ARROW_WIDTH));
	double lineAng = toDegrees(atan(((double) abs(x1-x2))/
							((double) abs(y1-y2))));

	// Adjust line angle for quadrant
	if (x1 > x2)
	{
		// South East
		if (y1 > y2) lineAng = 180.0-lineAng;
	}
	else
	{
		// South West
		if (y1 > y2) lineAng = 180.0+lineAng;
		// North West
		else lineAng = 360.0-lineAng;
	}

	// Calculate coords

	m_ptValues[0].x = x2;
	m_ptValues[0].y = y2;
	CalcCoords(1,x2,y2,dist,lineAng-arrowAng);
	CalcCoords(2,x2,y2,dist,lineAng+arrowAng);
}

void CArrowLine::CalcValues(int x1, int y1, int x2, int y2)
{
	// North or south
	if (x1 == x2)
	{
		// North
		if (y2 < y1) ArrowCoords(x2,y2,x2-ARROW_WIDTH,y2+ARROW_LENGTH,x2+ARROW_WIDTH,y2+ARROW_LENGTH);
		// South
		else ArrowCoords(x2,y2,x2-ARROW_WIDTH,y2-ARROW_LENGTH,x2+ARROW_WIDTH,y2-ARROW_LENGTH);
			return;
	}
	// East or West
	if (y1 == y2)
	{
		// East
		if (x2 > x1) ArrowCoords(x2,y2,x2-ARROW_LENGTH,y2-ARROW_WIDTH,x2-ARROW_LENGTH,y2+ARROW_WIDTH);
		// West
		else ArrowCoords(x2,y2,x2+ARROW_LENGTH,y2-ARROW_WIDTH,x2+ARROW_LENGTH,y2+ARROW_WIDTH);
			return;
	}
	// Calculate quadrant

	CalcValuesQuad(x1,y1,x2,y2);
}


int CArrowLine::Paint(wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset)
{
	BeginPaint(pDC, toDraw, ptOffset);

    wxPen pen( m_crColor );
    wxPen   OldPen = pDC->GetPen( );
    pDC->SetPen( pen );
    wxRasterOperationMode rop = pDC->GetLogicalFunction( );
    pDC->SetLogicalFunction( (m_nStyle&ST_TRANSPARENT) ? wxAND : wxCOPY );
    wxBrush     OldBrush;
    wxBrush     brush( m_crColor );
    OldBrush = pDC->GetBrush( );
    pDC->SetBrush( brush );

    int BkMode = pDC->GetBackgroundMode( );
    pDC->SetBackgroundMode( wxTRANSPARENT );

	IICRect rc = m_rcObj + toDraw.TopLeft();

	if (rc.left!=rc.right || rc.top != rc.bottom)
	{
		DrawLineEx(pDC, rc, toDraw.TopLeft());

        pDC->SetPen( OldPen );

        wxPen pen2( m_crColor );
        OldPen = pDC->GetPen( );
        pDC->SetPen( pen2 );

		CalcValues(rc.left, rc.top, rc.right, rc.bottom);
        pDC->DrawPolygon( 3, m_ptValues );
	}

	// restore DC settings
    pDC->SetBackgroundMode( BkMode );
    pDC->SetBrush( OldBrush );
    pDC->SetPen( OldPen );

    pDC->SetLogicalFunction( rop );

	DrawDragPoints(pDC, rc);

	EndPaint(pDC);
	return 0;
}

CBaseDrawingObject* CArrowLine::Clone(bool aPreAction, bool fReuseID)
{
	CArrowLine* clone = new CArrowLine(*this, aPreAction, fReuseID);
	return clone;
}
///////////////////////////////////////////////////////////////////////////////
CRectangle::CRectangle(IICRect& rect)
	: CBaseDrawingObject(rect)
{
	m_strType = DOType_Rectangle;

	m_nStyleCommitted		= m_nStyle		= m_nDefaultStyle;
	m_nLineStyleCommitted	= m_nLineStyle	= m_nLineStyleDefault;
	m_nLineSizeCommitted	= m_nLineSize	= m_nLineSizeDefault;

	m_crColorCommitted		= m_crColor		= CRectangle::m_crColorDefault;
	m_crBkgndCommitted		= m_crBkgnd		= CRectangle::m_crBkgndDefault;
	m_crTextColorCommitted	= m_crTextColor	= CRectangle::m_crTextColorDefault;

    m_logFont = m_fontDefault;
	m_strFontFaceCommitted	= m_strFontFace	= CRectangle::m_strFontFaceDefault;
	m_nFontSizeCommitted	= m_nFontSize	= CRectangle::m_nFontSizeDefault;
	m_strFontStyleCommitted	= m_strFontStyle= CRectangle::m_strFontStyleDefault;
}

CRectangle::CRectangle(CRectangle& copy, bool aPreAction, bool fReuseID)
	: CBaseDrawingObject(copy, aPreAction, fReuseID)
{
}

CRectangle::~CRectangle()
{
}

wxSize CRectangle::GetExtent(wxDC* pDC)
{
	return CBaseDrawingObject::GetExtent(pDC);
}


//*****************************************************************************
int CBaseDrawingObject::CalcLineWrapping( wxDC *pDC, wxString &strText, IICRect &calc, wxSize &textSize, wxArrayInt &arrLineStarts, wxArrayInt &arrLineEnds )
{
    int         iMaxWidth   = calc.Width( ) - 10;
    textSize = pDC->GetTextExtent( strText );
    int         iSingleLineHeight = textSize.GetHeight( );

    //  Let's start this out on the right foot...
    arrLineStarts.Add( 0 );

    //  Do a little sanity checking...
    wxString    strT( wxT("W") );
    wxSize      minSize = pDC->GetTextExtent( strT );
    if( iMaxWidth < minSize.GetWidth( ) ||  calc.Height( ) < minSize.GetHeight( ) )
    {
        arrLineEnds.Add( 0 );
        return iSingleLineHeight;
    }


    bool    fManuallyWrap = false;
    if( strText.Find( wxT("\n") )  !=  wxNOT_FOUND )
        fManuallyWrap = true;

    if( fManuallyWrap  ||  (iMaxWidth > 10  &&  textSize.GetWidth( ) > iMaxWidth) )
    {
        //  Text string is too wide so we need to do some wrapping
        //          - - - - OR - - - -
        //  Text string contains NewLine and/or Carriage Returns char(s) and we need to manually wrap it
        wxArrayInt  widths;
        if( pDC->GetPartialTextExtents( strText, widths ) )
        {
            int     iLen        = strText.Len( );
            int     iLoc        = 0;
            int     iLastWhite  = 0;
            int     iFirstLoc   = 0;
            wxChar  c;
            for( iLoc = 0; iLoc < iLen; iLoc++ )
            {
                c = strText.GetChar( iLoc );
                if( c  ==  wxT('\0') )
                {
                    arrLineEnds.Add( iLoc );
                    break;
                }
                if( c == wxT('\n') )
                {
                    arrLineEnds.Add( iLoc );
                    iLastWhite = 0;
                    if( iLoc + 1  >=  iLen )
                        break;
                    arrLineStarts.Add( iLoc + 1 );
                    iFirstLoc = iLoc + 1;
                }

                if( (widths.Item( iLoc ) - widths.Item( iFirstLoc ))  >= iMaxWidth )
                {
                    //  We need to break the line...
                    if( iLastWhite  == 0 )
                    {
                        //  No white space in this line - break it before this character...
                        arrLineEnds.Add( iLoc - 1 );
                        iLastWhite = 0;
                        arrLineStarts.Add( iLoc - 1 );
                        iFirstLoc = iLoc - 1;
                    }
                    else
                    {
                        arrLineEnds.Add( iLastWhite );
                        arrLineStarts.Add( iLastWhite + 1 );
                        iFirstLoc = iLastWhite + 1;
                        iLastWhite = 0;
                    }
                }

                if( c == wxT(' ')  ||  c == wxT('\t') )
                    iLastWhite = iLoc;
            }
        }
        else
        {
            wxLogDebug( wxT("pDC->GetPartialTextExtents( ) failed!!") );
        }

        //  Adjust the height based on the number of lines we just counted
        textSize.SetHeight( arrLineStarts.GetCount( ) * iSingleLineHeight );
    }

    if( arrLineStarts.GetCount( ) > arrLineEnds.GetCount( ) )
        arrLineEnds.Add( strText.Len( ) );

    return iSingleLineHeight;
}


//*****************************************************************************
int CRectangle::Paint(wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset)
{
	BeginPaint(pDC, toDraw, ptOffset);

    wxBrush     brush( m_crBkgnd );
    wxBrush OldBrush;
    OldBrush = pDC->GetBrush( );
    pDC->SetBrush( brush );
    wxRasterOperationMode rop = pDC->GetLogicalFunction( );
    pDC->SetLogicalFunction( (m_nStyle&ST_TRANSPARENT) ? wxAND : wxCOPY );

    pDC->SetTextForeground( m_crTextColor );
    int BkMode = pDC->GetBackgroundMode( );
    pDC->SetBackgroundMode( wxTRANSPARENT );

	IICRect rc = m_rcObj + toDraw.TopLeft();

	DrawRectangle(pDC, rc, toDraw.TopLeft());

    bool    fULine  = false;
    int     iStyle  = wxFONTSTYLE_NORMAL;
    int     iWeight = wxFONTWEIGHT_NORMAL;
    if( m_strFontStyle.Find( wxT("bold|") ) != wxNOT_FOUND )
        iWeight = wxFONTWEIGHT_BOLD;
    if( m_strFontStyle.Find( wxT("italic")) != wxNOT_FOUND )
        iStyle = wxFONTSTYLE_ITALIC;
    if( m_strFontStyle.Find( wxT("underline")) != wxNOT_FOUND )
        fULine = true;
    wxFont      cfont( m_nFontSize, wxFONTFAMILY_SWISS, iStyle, iWeight, fULine, m_strFontFace );

    wxFont  OldFont;
    OldFont = pDC->GetFont( );
    pDC->SetFont( cfont );

    IICRect calc = rc;

    //  These arrays store the character location in the string where each line starts and ends
    wxArrayInt  arrLineStarts;
    wxArrayInt  arrLineEnds;
    wxSize      textSize;
    int         iSingleLineHeight;

    iSingleLineHeight = CalcLineWrapping( pDC, m_strText, calc, textSize, arrLineStarts, arrLineEnds );

    if( textSize.GetHeight( )  <  rc.Height( ) )
	{
		calc += wxPoint((rc.Width()-calc.Width())/2, (rc.Height()-textSize.GetHeight( ))/2);
	}
	else
	{
		calc += wxPoint((rc.Width()-calc.Width())/2, 0);
		calc.bottom = rc.bottom;
	}
	if (calc.Width()>rc.Width())
	{
		calc.left = rc.left;
		calc.right = rc.right;
	}

    int         i;
    int         iTop    = calc.top;
    wxString    strT;
    for( i=0; i< (int)arrLineStarts.GetCount( ); i++ )
    {
        if( (iTop + iSingleLineHeight)  >  calc.bottom )
            break;
        strT = m_strText.Mid( arrLineStarts.Item( i ), arrLineEnds.Item( i ) - arrLineStarts.Item( i ) );
        wxSize  sizeThisLine = pDC->GetTextExtent( strT );
        int     iXOffset = ( calc.Width( ) - sizeThisLine.GetWidth( ) ) / 2;
        pDC->DrawText( strT, calc.left + 3 + iXOffset, iTop );
        iTop += iSingleLineHeight;
    }


	// restore DC settings
    pDC->SetBackgroundMode( BkMode );
    pDC->SetBrush( OldBrush );
    pDC->SetFont( OldFont );

	DrawTracker(pDC, m_nMode, toDraw);

    pDC->SetLogicalFunction( rop );
	EndPaint(pDC);
	return 0;
}

void CRectangle::GetXML(wxString& xml, bool fCompact)
{
	wxString temp;

	xml += _T("<item>\n");
	CBaseDrawingObject::GetXML(xml, fCompact);
	xml += _T("</item>\n");
}

CBaseDrawingObject* CRectangle::Clone(bool aPreAction, bool fReuseID)
{
	CRectangle* clone = new CRectangle(*this, aPreAction, fReuseID);
	return clone;
}


//*****************************************************************************
//*****************************************************************************
///////////////////////////////////////////////////////////////////////////////
CEllipse::CEllipse(IICRect& rect)
	: CBaseDrawingObject(rect)
{
	m_strType = DOType_Ellipse;

	m_nStyleCommitted		= m_nStyle		= m_nDefaultStyle;
	m_nLineStyleCommitted	= m_nLineStyle	= m_nLineStyleDefault;
	m_nLineSizeCommitted	= m_nLineSize	= m_nLineSizeDefault;

	m_crColorCommitted		= m_crColor		= CEllipse::m_crColorDefault;
	m_crBkgndCommitted		= m_crBkgnd		= CEllipse::m_crBkgndDefault;
	m_crTextColorCommitted	= m_crTextColor = CEllipse::m_crTextColorDefault;

    m_logFont = m_fontDefault;
	m_strFontFaceCommitted	= m_strFontFace = CEllipse::m_strFontFaceDefault;
	m_nFontSizeCommitted	= m_nFontSize	= CEllipse::m_nFontSizeDefault;
	m_strFontStyleCommitted	= m_strFontStyle= CEllipse::m_strFontStyleDefault;

    m_pEllipseRgn = NULL;
    m_pEllipseFillRgn = NULL;
}


//*****************************************************************************
CEllipse::CEllipse(CEllipse& copy, bool aPreAction, bool fReuseID)
	: CBaseDrawingObject(copy, aPreAction, fReuseID)
{
    m_pEllipseRgn = NULL;
    m_pEllipseFillRgn = NULL;
}


//*****************************************************************************
CEllipse::~CEllipse()
{
}


//*****************************************************************************
wxSize CEllipse::GetExtent(wxDC* pDC)
{
	return CBaseDrawingObject::GetExtent(pDC);
}

//***************************************************
//	RecreateEllipse()
//		Rotates Cnt number of points radians around c
//
//***************************************************
void CEllipse::RecreateEllipse(IICRect rect)
{
	IICRect ellipseR(rect);

	// Create an axis aligned ellipse dimensions  = ellipseR
	EllipseToBezier( ellipseR, m_EllipsePts);

	// Get bounding rect of ellipse
	// Note this does NOT include the pen width
	wxPoint tl = m_EllipsePts[0];
	wxPoint br = tl;
	for (int i = 1; i < 13; ++i)
	{
		if(m_EllipsePts[i].x < tl.x)
			tl.x = m_EllipsePts[i].x;
		else if (m_EllipsePts[i].x > br.x)
			br.x = m_EllipsePts[i].x;
		if(m_EllipsePts[i].y < tl.y)
			tl.y = m_EllipsePts[i].y;
		else if (m_EllipsePts[i].y > br.y)
			br.y = m_EllipsePts[i].y;
	}
	m_EllipseRect = IICRect(tl, br);
}

//***************************************************
//	UpdateEllipseRgn()
//
//***************************************************
void CEllipse::UpdateEllipseRgn( wxDC* pDC, CDashLine& Line )
{
	// Get fill area of ellipse
    BeginPath( );

    Line.MoveTo( m_EllipsePts[0].x, m_EllipsePts[0].y );
    m_pathPoints.push_back( m_EllipsePts[0] );
    m_pathTypes.push_back( (BYTE) PT_MOVETO );
    for( int i = 1; i < 12; i+=3 )
    {
        Line.BezierTo( m_EllipsePts+i, m_pathPoints, m_pathTypes );
    }
    EndPath( );

//    wxLogDebug( wxT("(1) after 4 calls to BezierTo( ) m_pathPoints.size( ) returned %d"), m_pathPoints.size( ) );

    if( m_pEllipseFillRgn )
    {
        m_pEllipseFillRgn->Clear( );
        delete m_pEllipseFillRgn;
    }

    double  dX, dY;
#ifndef __WXMAC__   // TODO: test on linux
    pDC->GetUserScale( &dX, &dY );
#else
    dX = dY = 1.0;
#endif

    wxPoint     *pPts;
    pPts = new wxPoint[ m_pathPoints.size( ) ];

    int                 i       = 0;
    POINTLIST::iterator ip;
    wxPoint             offset  = m_rcObj.TopLeft() - m_rcOrigRect.TopLeft();
    for( ip = m_pathPoints.begin(); ip != m_pathPoints.end(); ++ip )
    {
        wxPoint Pt    = *ip;
        Pt.x = (int)(((double)Pt.x * dX) + 0.5 );
        Pt.y = (int)(((double)Pt.y * dY) + 0.5 );
        pPts[ i++ ] = Pt;
    }
    m_pEllipseFillRgn = new wxRegion( i, pPts );
    delete pPts;
}


//*****************************************************************************
int CEllipse::Paint(wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset)
{
	BeginPaint(pDC, toDraw, ptOffset);

    wxBrush     brush( m_crBkgnd );
    wxBrush OldBrush;
    OldBrush = pDC->GetBrush( );
    pDC->SetBrush( brush );
    wxRasterOperationMode rop = pDC->GetLogicalFunction( );
    pDC->SetLogicalFunction( (m_nStyle&ST_TRANSPARENT) ? wxAND : wxCOPY );
    pDC->SetTextForeground( m_crTextColor );
    int BkMode = pDC->GetBackgroundMode( );
    pDC->SetBackgroundMode( wxTRANSPARENT );

    bool    fULine  = false;
    int     iStyle  = wxFONTSTYLE_NORMAL;
    int     iWeight = wxFONTWEIGHT_NORMAL;
    if( m_strFontStyle.Find( wxT("bold|") ) != wxNOT_FOUND )
        iWeight = wxFONTWEIGHT_BOLD;
    if( m_strFontStyle.Find( wxT("italic")) != wxNOT_FOUND )
        iStyle = wxFONTSTYLE_ITALIC;
    if( m_strFontStyle.Find( wxT("underline")) != wxNOT_FOUND )
        fULine = true;
    wxFont      cfont( m_nFontSize, wxFONTFAMILY_SWISS, iStyle, iWeight, fULine, m_strFontFace );

    wxFont  Oldfont = pDC->GetFont( );
    pDC->SetFont( cfont );

	IICRect rc = m_rcObj + toDraw.TopLeft();

	unsigned Type[8];
    int Pattern = CDashLine::GetPattern(Type, true, m_nLineSize, m_nLineStyle);
    if( m_nLineStyle  ==  PS_SOLID )
    {
        Type[ 0 ] = 2;
        Type[ 1 ] = 1;
        Pattern = 2;
    }

	CDashLine Line(*pDC, Type, Pattern);

	RecreateEllipse( rc );          //  calcs the 12 line segment approximation of the ellipse

    wxPen pen2( *wxBLACK, 0, wxTRANSPARENT );	// Cannot use FillRect no transparent
    pDC->SetPen( pen2 );
	UpdateEllipseRgn( pDC, Line );  //  creates the m_pEllipseFillRgn

    int     xOff, yOff;
    xOff = pDC->LogicalToDeviceX( 0 );
    yOff = pDC->LogicalToDeviceY( 0 );

    wxPoint xyOffset( xOff, yOff );
    m_pEllipseFillRgn->Offset( xyOffset );
//    wxLogDebug( wxT("m_pEllipseFillRgn->Offset( ptOffset ) returned %d"), fRet );

    pDC->SetClippingRegion( *m_pEllipseFillRgn );

	IICRect rc2 = m_EllipseRect;
	rc2.right +=1;
    rc2.bottom += 1;
    //  Fill the ellipse by drawing a filled rectangle which is clipped to the appropriate size and shape
    pDC->DrawRectangle( rc2.left, rc2.top, rc2.Width( ), rc2.Height( ) );
    pDC->DestroyClippingRegion( );

	// do drawing inside path for win95/8 compatibility
    int     i;
	//  Draw the outline of the ellipse using the info saved in m_pathPoints and m_pathTypes
    DrawPathOutline( true, pDC, toDraw.TopLeft(), false, m_nLineSize );

    IICRect     calc = rc;

    //  These arrays store the character location in the string where each line starts and ends
    wxArrayInt  arrLineStarts;
    wxArrayInt  arrLineEnds;
    wxSize      textSize;
    int         iSingleLineHeight;

    iSingleLineHeight = CalcLineWrapping( pDC, m_strText, calc, textSize, arrLineStarts, arrLineEnds );

    if( textSize.GetHeight( ) < rc.Height())
	{
		calc += wxPoint((rc.Width()-calc.Width())/2, (rc.Height()-textSize.GetHeight( ))/2);
	}
	else
	{
		calc += wxPoint((rc.Width()-calc.Width())/2, 0);
		calc.bottom = rc.bottom;
	}
	if (calc.Width()>rc.Width())
	{
		calc.left = rc.left;
		calc.right = rc.right;
	}

    int         iTop    = calc.top;
    wxString    strT;
    for( i=0; i< (int)arrLineStarts.GetCount( ); i++ )
    {
        if( (iTop + iSingleLineHeight)  >  calc.bottom )
            break;
        strT = m_strText.Mid( arrLineStarts.Item( i ), arrLineEnds.Item( i ) - arrLineStarts.Item( i ) );
        wxSize  sizeThisLine = pDC->GetTextExtent( strT );
        int     iXOffset = ( calc.Width( ) - sizeThisLine.GetWidth( ) ) / 2;
        pDC->DrawText( strT, calc.left + 3 + iXOffset, iTop );
        iTop += iSingleLineHeight;
    }

	// restore DC settings
    pDC->SetBackgroundMode( BkMode );
    pDC->SetBrush( OldBrush );
    pDC->SetFont( Oldfont );

	DrawTracker(pDC, m_nMode, toDraw);

    pDC->SetLogicalFunction( rop );

	EndPaint(pDC);
	return 0;
}


//*****************************************************************************
void CEllipse::GetXML(wxString& xml, bool fCompact)
{
	wxString temp;

	xml += _T("<item>\n");
	CBaseDrawingObject::GetXML(xml, fCompact);
	xml += _T("</item>\n");
}


//*****************************************************************************
CBaseDrawingObject* CEllipse::Clone(bool aPreAction, bool fReuseID)
{
	CEllipse* clone = new CEllipse(*this, aPreAction, fReuseID);
	return clone;
}


//*****************************************************************************
//*****************************************************************************
///////////////////////////////////////////////////////////////////////////////
CTextBox::CTextBox(IICRect rect)
	: CBaseDrawingObject(rect)
{
	m_strType = DOType_TextBox;

	m_nStyleCommitted		= m_nStyle		= CTextBox::m_nDefaultStyle;
	m_nLineSizeCommitted	= m_nLineSize	= CTextBox::m_nLineSizeDefault;
	m_nLineStyleCommitted	= m_nLineStyle	= CTextBox::m_nLineStyleDefault;

	m_crColorCommitted		= m_crColor		= CTextBox::m_crColorDefault;
	m_crBkgndCommitted		= m_crBkgnd		= CTextBox::m_crBkgndDefault;
	m_crTextColorCommitted	= m_crTextColor = CTextBox::m_crTextColorDefault;

    m_logFont = m_fontDefault;
	m_strFontFaceCommitted	= m_strFontFace = CTextBox::m_strFontFaceDefault;
	m_nFontSizeCommitted	= m_nFontSize	= CTextBox::m_nFontSizeDefault;
	m_strFontStyleCommitted = m_strFontStyle= CTextBox::m_strFontStyleDefault;
}

CTextBox::CTextBox(CTextBox& copy, bool aPreAction, bool fReuseID)
	: CBaseDrawingObject(copy, aPreAction, fReuseID)
{
}

CTextBox::~CTextBox()
{
}

wxSize CTextBox::GetExtent(wxDC* pDC)
{
    CBaseDrawingObject::GetExtent(pDC);

    wxSize  sRet;
    sRet = pDC->GetMultiLineTextExtent( m_strText );

    return sRet;
}

int CTextBox::Paint(wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset)
{
	BeginPaint(pDC, toDraw, ptOffset);

    wxBrush     brush( m_crBkgnd );
    wxBrush OldBrush;
    OldBrush = pDC->GetBrush( );
    pDC->SetBrush( brush );
    int BkMode = pDC->GetBackgroundMode( );
    pDC->SetBackgroundMode( wxTRANSPARENT );

	IICRect rc = m_rcObj + toDraw.TopLeft();

    wxRasterOperationMode rop = pDC->GetLogicalFunction( );
    pDC->SetLogicalFunction( (m_nStyle&ST_TRANSPARENT) ? wxAND : wxCOPY );

	DrawRectangle(pDC, rc, toDraw.TopLeft());

    pDC->SetLogicalFunction( rop );


    bool    fULine  = false;
    int     iStyle  = wxFONTSTYLE_NORMAL;
    int     iWeight = wxFONTWEIGHT_NORMAL;
    if( m_strFontStyle.Find( wxT("bold|") ) != wxNOT_FOUND )
        iWeight = wxFONTWEIGHT_BOLD;
    if( m_strFontStyle.Find( wxT("italic")) != wxNOT_FOUND )
        iStyle = wxFONTSTYLE_ITALIC;
    if( m_strFontStyle.Find( wxT("underline")) != wxNOT_FOUND )
        fULine = true;
    wxFont      cfont( m_nFontSize, wxFONTFAMILY_SWISS, iStyle, iWeight, fULine, m_strFontFace );

    wxFont  Oldfont = pDC->GetFont( );
    pDC->SetFont( cfont );

    pDC->SetTextForeground( m_crTextColor );

	wxString temp = m_strText;
    if( temp.IsEmpty( ) )
        temp = wxString( _("Click to Edit") );

	int ybias = m_nLineSize/2;
	rc.InflateRect(0, -ybias);

    IICRect     calc            = rc;
    wxArrayInt  arrLineStarts;
    wxArrayInt  arrLineEnds;
    wxSize      textSize;
    int         iSingleLineHeight;

    iSingleLineHeight = CalcLineWrapping( pDC, temp, calc, textSize, arrLineStarts, arrLineEnds );

    int         iTop    = calc.top;
    wxString    strT;
    for( int i=0; i< (int)arrLineStarts.GetCount( ); i++ )
    {
        if( (iTop + iSingleLineHeight)  >  calc.bottom )
            break;
        strT = temp.Mid( arrLineStarts.Item( i ), arrLineEnds.Item( i ) - arrLineStarts.Item( i ) );
        pDC->DrawText( strT, calc.left + 3, iTop );
        iTop += iSingleLineHeight;
    }

	// restore DC settings
    pDC->SetBackgroundMode( BkMode );
    pDC->SetBrush( OldBrush );
    pDC->SetFont( Oldfont );

	DrawTracker(pDC, m_nMode, toDraw);

	EndPaint(pDC);
	return 0;
}


void CTextBox::GetXML(wxString& xml, bool fCompact)
{
	wxString temp;

	xml += _T("<item>\n");
	CBaseDrawingObject::GetXML(xml, fCompact);
	xml += _T("</item>\n");
}

CBaseDrawingObject* CTextBox::Clone(bool aPreAction, bool fReuseID)
{
	CTextBox* clone = new CTextBox(*this, aPreAction, fReuseID);
	return clone;
}

//////////////////////////////////////////////////////////////////////////////
CImage::CImage(IICRect rect)
	: CBaseDrawingObject(rect)
{
    wxLogDebug( wxT("entering CImage default CONSTRUCTOR") );

    m_strType = DOType_Image;
    wxString    strOption( wxT("quality") );
    m_image.SetOption( wxIMAGE_OPTION_QUALITY, 75 );

	m_nStyleCommitted		= m_nStyle		= CImage::m_nDefaultStyle;
	m_nLineSizeCommitted	= m_nLineSize	= CImage::m_nLineSizeDefault;
	m_nLineStyleCommitted	= m_nLineStyle	= CImage::m_nLineStyleDefault;
    m_crColorCommitted		= m_crColor		= CImage::m_crColorDefault;
}


CImage::CImage( CImage& copy, bool aPreAction, bool fReuseID )
	: CBaseDrawingObject( copy, aPreAction, fReuseID )
{
    wxLogDebug( wxT("entering CImage copy CONSTRUCTOR") );
    if( copy.m_image.IsOk( ) )
	{
        m_image = copy.m_image.Copy( );
	}
    m_strImageData = copy.m_strImageData;
}

CImage::~CImage()
{
}

CBaseDrawingObject* CImage::Clone(bool aPreAction, bool fReuseID)
{
	CImage* clone = new CImage(*this, aPreAction, fReuseID);
	return clone;
}

int CImage::Paint( wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset )
{
//    wxLogDebug( wxT("entering CImage::Paint( )") );

	BeginPaint(pDC, toDraw, ptOffset);

	IICRect rc = m_rcObj + toDraw.TopLeft( );

    wxRasterOperationMode rop = pDC->GetLogicalFunction( );
    pDC->SetLogicalFunction( (m_nStyle&ST_TRANSPARENT) ? wxAND : wxCOPY );
    if( m_image.IsOk( ) )
    {
        wxLogDebug( wxT("  m_image.IsOk( ) returned TRUE") );
        wxBitmap    scaled;
        scaled = wxBitmap( m_image.Scale( rc.Width( ), rc.Height( ) ) );
        wxLogDebug( wxT("  scaled image IsOk( ) returned %d"), scaled.IsOk( ) );
        pDC->DrawBitmap( scaled, rc.left, rc.top, false );
    }
    else
    {
        wxLogDebug( wxT("  m_image.IsOk( ) returned FALSE --  stored image is BAD!!!") );
    }

    rc.InflateRect( m_nLineSize/2, m_nLineSize/2 );
	DrawRectangle( pDC, rc, toDraw.TopLeft(), false );

	DrawTracker( pDC, m_nMode, toDraw );

    pDC->SetLogicalFunction( rop );

	EndPaint( pDC );
	return 0;
}

unsigned int CImage::GetImageSize( )
{
//    wxLogDebug( wxT("entering CImage::GetImageSize( )  --  NEED TO IMPLEMENT THIS?!?!") );
#if 0
	if (m_dwSizeSrcBits>0)
		return m_dwSizeSrcBits;

	if (m_lpSrcBits!=NULL)
	{
		TCHAR temp[MAX_PATH], tempFile[MAX_PATH];
		GetTempPath(MAX_PATH, temp);
		GetTempFileName(temp, _T("IIC"), 0, tempFile);

		if (SaveImage(tempFile))
		{
			FILE * f = fopen(_bstr_t(tempFile), "rb");
			if (f)
			{
				fseek(f, 0, SEEK_END);
				unsigned int num = ftell(f);
				fclose(f);
//				DeleteFile(tempFile);
                ::wxRemoveFile( tempFile );

				return num;
			}
		}
	}
#endif
	return 0;
}

void CImage::GetImageData( wxString& strData )
{
//    wxLogDebug( wxT("entering CImage::GetImageData( wxString& strData )") );

    if( !m_strImageData.IsEmpty( ) )
	{
		strData += m_strImageData;
		return;
	}

	if( m_image.IsOk( ) )
    {
        wxMemoryOutputStream    oStream;

        if( m_image.SaveFile( oStream, wxBITMAP_TYPE_JPEG ) )
		{
            int     num         = oStream.GetLength( );
            BYTE    *pImageData = new BYTE[ num ];
            if( pImageData )
            {
                oStream.CopyTo( pImageData, num );
                strData += wxString( m_encoder.Encode( (const char *)pImageData, num ), wxConvUTF8 );
                delete[] pImageData;
            }
		}
    }
}

void CImage::GetXML( wxString& xml, bool fCompact )
{
	wxString temp;

	xml += _T("<item>\n");
	CBaseDrawingObject::GetXML( xml, fCompact );

    if( !fCompact )	// upload image file
	{
		temp = _T("<image>");

		GetImageData( temp );

		temp += _T("</image>");
		xml += temp;
	}

	xml += _T("</item>\n");
}

void CImage::SetAttributes( iks *y )
{
    char    *pData = iks_find_cdata( y, "image" );
	if( pData )
	{
		SetBitmap( pData );
	}

	// SetBitmap will reset the object boundary
	CBaseDrawingObject::SetAttributes( y );
}

void CImage::SetBitmap( wxBitmap bitmap)
{
//    wxLogDebug( wxT("entering CImage::SetBitmap( wxBitmap bitmap )") );

    m_image = bitmap.ConvertToImage( );

    m_rcObj.bottom = m_rcObj.top + m_image.GetHeight( );
    m_rcObj.right = m_rcObj.left + m_image.GetWidth( );

    m_rcOrigRect = m_rcObj;
    m_rcLastRect = m_rcObj;
}

void CImage::SetBitmap( const char* pData )
{
//    wxLogDebug( wxT("entering CImage::SetBitmap( const char* pData )") );

	m_strImageData = wxString( pData, wxConvUTF8 );
    if( m_strImageData.IsEmpty( ) )
		return;

    int     num = strlen( pData );
	char    *pImageData = new char[ num+1 ];
	if( pImageData )
	{
        int     iLen    = m_encoder.Decode( pData, pImageData );

        wxMemoryInputStream     iStream( pImageData, iLen );

		// ImageBufLen is the size of the image buffer
        if( m_image.LoadFile( iStream, wxBITMAP_TYPE_JPEG ) )
		{
			m_rcObj.bottom = m_rcObj.top + m_image.GetHeight( );
			m_rcObj.right = m_rcObj.left + m_image.GetWidth( );

			m_rcOrigRect = m_rcObj;
			m_rcLastRect = m_rcObj;
        }
        free( pImageData );
    }
}

//////////////////////////////////////////////////////////////////////////////
CCheckMark::CCheckMark(IICRect& rect)
	: CBaseDrawingObject(rect)
{
	m_strType = DOType_CheckMark;

	m_nStyleCommitted		= m_nStyle		= CCheckMark::m_nDefaultStyle;
	m_nLineSizeCommitted	= m_nLineSize	= CCheckMark::m_nLineSizeDefault;
	m_nLineStyleCommitted	= m_nLineStyle	= CCheckMark::m_nLineStyleDefault;

	m_crColorCommitted		= m_crColor		= CCheckMark::m_crColorDefault;
	m_crBkgndCommitted		= m_crBkgnd		= CCheckMark::m_crBkgndDefault;
	m_crTextColorCommitted	= m_crTextColor = CCheckMark::m_crTextColorDefault;
}

CCheckMark::CCheckMark(CCheckMark& copy, bool aPreAction, bool fReuseID)
	: CBaseDrawingObject(copy, aPreAction, fReuseID)
{
	m_strType = DOType_CheckMark;
}

CCheckMark::~CCheckMark()
{
}

CBaseDrawingObject* CCheckMark::Clone(bool aPreAction, bool fReuseID)
{
	CCheckMark* clone = new CCheckMark(*this, aPreAction, fReuseID);
	return clone;
}

int CCheckMark::Paint(wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset)
{
	BeginPaint(pDC, toDraw, ptOffset);

    wxRasterOperationMode rop = pDC->GetLogicalFunction( );
    pDC->SetLogicalFunction( (m_nStyle&ST_TRANSPARENT) ? wxAND : wxCOPY );
    int BkMode = pDC->GetBackgroundMode( );
    pDC->SetBackgroundMode( wxTRANSPARENT );

	IICRect rc = m_rcObj + toDraw.TopLeft();
	unsigned Type[8];
	int Pattern = CDashLine::GetPattern(Type, true, m_nLineSize+5, m_nLineStyle);

    wxPen pen( *wxBLACK, 0, wxTRANSPARENT );	// Cannot use FillRect no transparent
    wxPen   OldPen2 = pDC->GetPen( );
    pDC->SetPen( pen );

	CDashLine Line(*pDC, Type, Pattern);

    BeginPath( );
	int L1 = (rc.Width()-2) / 3;
	int y = rc.top + rc.Height() / 2;
	Line.MoveTo(rc.left+1,  y);
    m_pathPoints.push_back( wxPoint( rc.left+1, y ) );
    m_pathTypes.push_back( (BYTE) PT_MOVETO );
	int y1 = y + L1;
    if (y1 > rc.bottom)
        y1 = rc.bottom;
	Line.LineTo(rc.left + L1, y1, m_pathPoints, m_pathTypes );
    Line.MoveTo(rc.left + L1, y1 );
    m_pathPoints.push_back( wxPoint( rc.left + L1, y1 ) );
    m_pathTypes.push_back( (BYTE) PT_MOVETO );
	int y2 = y1 - L1 * 2;
    if (y2 < rc.top)
        y2 = rc.top;
	Line.LineTo(rc.right - 3, y2, m_pathPoints, m_pathTypes );
    EndPath( );

    pDC->SetPen( OldPen2 );

	// make pen and stroke path
	DrawPathOutline(true, pDC, toDraw.TopLeft(), TRUE, m_nLineSize+5);

	// restore DC settings
    pDC->SetBackgroundMode( BkMode );

    pDC->SetLogicalFunction( rop );

	DrawTracker(pDC, m_nMode, toDraw);

	EndPaint(pDC);

	return 0;
}

void CCheckMark::GetXML(wxString& xml, bool fCompact)
{
	xml += _T("<item>\n");
	CBaseDrawingObject::GetXML(xml, fCompact);
	xml += _T("</item>\n");
}

//////////////////////////////////////////////////////////////////////////////
CRadioMark* CRadioMark::m_singleton = NULL;

CRadioMark::CRadioMark(IICRect& rect)
	: CBaseDrawingObject(rect)
{
	m_strType = DOType_RadioMark;

	m_nStyleCommitted		= m_nStyle		= CRadioMark::m_nDefaultStyle;
	m_nLineSizeCommitted	= m_nLineSize	= CRadioMark::m_nLineSizeDefault;
	m_nLineStyleCommitted	= m_nLineStyle	= CRadioMark::m_nLineStyleDefault;

	m_crColorCommitted		= m_crColor		= CRadioMark::m_crColorDefault;
	m_crBkgndCommitted		= m_crBkgnd		= CRadioMark::m_crBkgndDefault;
	m_crTextColorCommitted	= m_crTextColor = CRadioMark::m_crTextColorDefault;
}

CRadioMark::CRadioMark(CRadioMark& copy, bool aPreAction, bool fReuseID)
	: CBaseDrawingObject(copy, aPreAction, fReuseID)
{
	m_strType = DOType_RadioMark;
}

CRadioMark::~CRadioMark()
{
}

CRadioMark* CRadioMark::GetRadioMark()
{
	if (m_singleton == NULL)
	{
		IICRect rect(10,10,10,10);
		m_singleton = new CRadioMark(rect);
	}

	m_singleton->AddRef();

	return m_singleton;
}

void CRadioMark::Clear()
{
	if (m_singleton!=NULL)
	{
		m_singleton->Release();
		m_singleton = NULL;
	}
}

CBaseDrawingObject* CRadioMark::Clone(bool aPreAction, bool fReuseID)
{
	CRadioMark* clone = GetRadioMark();
	return clone;
}

int CRadioMark::Paint(wxDC* pDC, IICRect& toDraw, wxPoint& ptOffset)
{
	BeginPaint(pDC, toDraw, ptOffset);

    wxPen pen( m_crColor );
    wxPen   OldPen = pDC->GetPen( );
    pDC->SetPen( pen );
    wxBrush     brush( m_crBkgnd );
    wxBrush OldBrush;
    OldBrush = pDC->GetBrush( );
    pDC->SetBrush( brush );
    wxRasterOperationMode rop = pDC->GetLogicalFunction( );
    pDC->SetLogicalFunction( (m_nStyle&ST_TRANSPARENT) ? wxAND : wxCOPY );
    int BkMode = pDC->GetBackgroundMode( );
    pDC->SetBackgroundMode( wxTRANSPARENT );

	IICRect rc = m_rcObj + toDraw.TopLeft();

    pDC->DrawEllipse( rc.left, rc.top, rc.Width( ), rc.Height( ) );

	// restore DC settings
    pDC->SetBackgroundMode( BkMode );
    pDC->SetPen( OldPen );
    pDC->SetBrush( OldBrush );

    pDC->SetLogicalFunction( rop );

	DrawTracker(pDC, m_nMode, toDraw);

	EndPaint(pDC);

	return 0;
}

void CRadioMark::GetXML(wxString& xml, bool fCompact)
{
	xml += _T("<item>\n");
	CBaseDrawingObject::GetXML(xml, fCompact);
	xml += _T("</item>\n");
}

///////////////////////////////////////////////////////////////////////////////
CAction::CAction(CIICDoc* pDocument)
	:m_pDocument(pDocument)
{
	m_nType = AT_NONE;
}

CAction::~CAction()
{
	OBJLIST::iterator i;
	for (i =  m_listObj.begin(); i != m_listObj.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;
		if (obj)
		{
			obj->Release();
		}
	}
	m_listObj.clear();
}

void CAction::Do(CDrawingObjects& src, bool aPreAction)
{
	if (m_nType == AT_DELETE)	// use reverse order for deleting
	{
		OBJLIST::reverse_iterator i;
		OBJLIST& listSelected = src.GetSelectedItems();

		for (i =  listSelected.rbegin(); i != listSelected.rend(); ++i)
		{
			CBaseDrawingObject* obj = *i;

			if (obj && !obj->IsDeleting())
			{
				m_listObj.push_back(obj->Clone(aPreAction, true));
			}
		}
	}
	else
	{
		OBJLIST::iterator i;
		OBJLIST& listSelected = src.GetSelectedItems();

		for (i =  listSelected.begin(); i != listSelected.end(); ++i)
		{
			CBaseDrawingObject* obj = *i;

			if (obj && !obj->IsDeleting())
			{
				m_listObj.push_back(obj->Clone(aPreAction, true));
			}
		}
	}
}

void CAction::SendXML(const wxString& actionXML)
{
	if (IsMeetingInProgress())
	{
		wxString meetingId;
		m_pDocument->m_Meeting.GetMeetingId(meetingId);

		m_pDocument->LockDocShareEvent();

		m_pDocument->SendDrawOnWhiteBoard(meetingId, actionXML);
	}
}

///////////////////////////////////////////////////////////////////////////////
CCreateAction::CCreateAction(CIICDoc* pDocument)
	: CAction(pDocument)
{
	m_nType = AT_CREATE;
}

void CCreateAction::Undo(CDrawingObjects& src, IICRect& rect)
{
    wxLogDebug( wxT("entering CCreateAction::Undo( )") );
	OBJLIST::iterator i;

    for (i =  m_listObj.begin(); i != m_listObj.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;

		if (obj)
		{
			rect = obj->UnionRect(rect);
            wxLogDebug( wxT("  found an object (0x%8.8x) - its (union) rect is (%d, %d) - (%d, %d)"), obj, rect.left, rect.top, rect.right, rect.bottom );

			obj = src.Delete(obj->GetID());
			if (obj)
			{
                wxLogDebug( wxT("      src.Delete returned an object - (0x%8.8x)"), obj );
				rect = obj->UnionRect(rect);
				obj->Release();
			}
		}
	}
}

void CCreateAction::Redo(CDrawingObjects& src, IICRect& rect)
{
	OBJLIST::iterator i;

    for (i =  m_listObj.begin(); i != m_listObj.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;

		if (obj)
		{
			rect = obj->UnionRect(rect);

			src.Add(obj->Clone(), false);
		}
	}
}

void CCreateAction::SendActionXML(bool bUndo, bool fCompact, bool aNotifyServer, bool aUndoRedoAction)
{
//	xml += _T("<?xml version=\"1.0\" encoding=\"UTF8\"?>\n");
	if (!aNotifyServer || !IsMeetingInProgress())
		return;

	wxString userId, temp;
	m_pDocument->m_Meeting.GetMeetingUserID(userId);


	OBJLIST::iterator i;
    for (i =  m_listObj.begin(); i != m_listObj.end(); ++i)
	{
		wxString xml;
		if (bUndo)
		{
			temp = wxString::Format(_T("<action type=\"%s\" actor=\"%s\" undoredo=\"%d\">\n"), _T("delete"), userId.wc_str( ), aUndoRedoAction?1:0);
			fCompact = true;
		}
		else
		{
			temp = wxString::Format(_T("<action type=\"%s\" actor=\"%s\" undoredo=\"%d\">\n"), _T("create"), userId.wc_str( ), aUndoRedoAction?1:0);
			fCompact = false;
		}

		xml += temp;

		CBaseDrawingObject* obj = *i;
		if (obj)
		{
			obj->GetXML(xml, fCompact);
		}
		xml += _T("</action>\n");

		SendXML(xml);
	}
}

///////////////////////////////////////////////////////////////////////////////
CPasteAction::CPasteAction(CIICDoc* pDocument)
	: CAction(pDocument)
{
	m_nType = AT_PASTE;
}

CPasteAction::~CPasteAction()
{
}

void CPasteAction::Undo(CDrawingObjects& src, IICRect& rect)
{
	OBJLIST::iterator i;

    for (i =  m_listObj.begin(); i != m_listObj.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;

		if (obj)
		{
			rect = obj->UnionRect(rect);

			obj = src.Delete(obj->GetID());
			if (obj)
			{
				rect = obj->UnionRect(rect);
				obj->Release();
			}
		}
	}
}

void CPasteAction::Redo(CDrawingObjects& src, IICRect& rect)
{
	OBJLIST::iterator i;

    for (i =  m_listObj.begin(); i != m_listObj.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;

		if (obj)
		{
			rect = obj->UnionRect(rect);

			src.Add(obj->Clone(), false);
		}
	}
}

void CPasteAction::SendActionXML(bool bUndo, bool fCompact, bool aNotifyServer, bool aUndoRedoAction)
{
//	xml += _T("<?xml version=\"1.0\" encoding=\"UTF8\"?>\n");
	if (!aNotifyServer || !IsMeetingInProgress())
		return;

	wxString userId, temp;
	m_pDocument->m_Meeting.GetMeetingUserID(userId);

	OBJLIST::iterator i;
    for (i =  m_listObj.begin(); i != m_listObj.end(); ++i)
	{
		wxString xml;
		if (bUndo)
		{
			temp = wxString::Format(_T("<action type=\"%s\" actor=\"%s\" undoredo=\"%d\">\n"), _T("delete"), userId.wc_str( ), aUndoRedoAction?1:0);
			fCompact = true;
		}
		else
		{
			temp = wxString::Format(_T("<action type=\"%s\" actor=\"%s\" undoredo=\"%d\">\n"), _T("paste"), userId.wc_str( ), aUndoRedoAction?1:0);
			fCompact = false;
		}
		xml += temp;

		CBaseDrawingObject* obj = *i;

		if (obj)
		{
			obj->GetXML(xml, fCompact);
		}
		xml += _T("</action>\n");
		SendXML(xml);
	}
}

///////////////////////////////////////////////////////////////////////////////
CDeleteAction::CDeleteAction(CIICDoc* pDocument)
	: CAction(pDocument)
{
	m_nType = AT_DELETE;
}

void CDeleteAction::Undo(CDrawingObjects& src, IICRect& rect)
{
	OBJLIST::iterator i;

    for (i =  m_listObj.begin(); i != m_listObj.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;

		if (obj)
		{
			rect = obj->UnionRect(rect);

			src.Add(obj->Clone(), false);
		}
	}
}

void CDeleteAction::Redo(CDrawingObjects& src, IICRect& rect)
{
	OBJLIST::iterator i;

    for (i =  m_listObj.begin(); i != m_listObj.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;

		if (obj)
		{
			rect = obj->UnionRect(rect);

			obj = src.Delete(obj->GetID());
			if (obj)
			{
				rect = obj->UnionRect(rect);
				obj->Release();
			}
		}
	}
}

void CDeleteAction::SendActionXML(bool bUndo, bool fCompact, bool aNotifyServer, bool aUndoRedoAction)
{
	if (!aNotifyServer || !IsMeetingInProgress())
		return;

//	xml += _T("<?xml version=\"1.0\" encoding=\"UTF8\"?>\n");
	wxString userId, temp, xml;
	m_pDocument->m_Meeting.GetMeetingUserID(userId);

	OBJLIST::iterator i;
    for (i =  m_listObj.begin(); i != m_listObj.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;
		wxString xml;

		if (bUndo)
		{
			temp = wxString::Format(_T("<action type=\"%s\" actor=\"%s\" undoredo=\"%d\">\n"), _T("create"), userId.wc_str( ), aUndoRedoAction?1:0);
			fCompact = false;
		}
		else
		{
			temp = wxString::Format(_T("<action type=\"%s\" actor=\"%s\" undoredo=\"%d\">\n"), _T("delete"), userId.wc_str( ), aUndoRedoAction?1:0);
			fCompact = true;
		}
		xml += temp;

		if (obj)
		{
			obj->GetXML(xml, fCompact);
		}

		xml += _T("</action>\n");

		SendXML(xml);
	}
}

///////////////////////////////////////////////////////////////////////////////
CAssignAction::CAssignAction(CIICDoc* pDocument)
	: CAction(pDocument)
{
	m_nType = AT_ASSIGN;
}

void CAssignAction::Undo(CDrawingObjects& src, IICRect& rect)
{
	OBJLIST::iterator i;

    for (i =  m_listObj.begin(); i != m_listObj.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;

		if (obj)
		{
			rect = obj->UnionRect(rect);

			CBaseDrawingObject* o = src.Find(obj->GetID());
			if (o)
			{
				rect = o->UnionRect(rect);

				o->Assign(*obj);
			}
		}
	}
}

void CAssignAction::Redo(CDrawingObjects& src, IICRect& rect)
{
	Undo(src, rect);
}

void CAssignAction::SendActionXML(bool bUndo, bool fCompact, bool aNotifyServer, bool aUndoRedoAction)
{
	if (!aNotifyServer || !IsMeetingInProgress())
		return;

//	xml += _T("<?xml version=\"1.0\" encoding=\"UTF8\"?>\n");
	wxString userId, temp, xml;
	m_pDocument->m_Meeting.GetMeetingUserID(userId);

	OBJLIST::iterator i;
    for (i =  m_listObj.begin(); i != m_listObj.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;
		wxString xml;

		if (bUndo)
		{
			temp = wxString::Format(_T("<action type=\"%s\" actor=\"%s\" undoredo=\"%d\">\n"), _T("change"), userId.wc_str( ), aUndoRedoAction?1:0);
			fCompact = false;
		}
		else
		{
			temp = wxString::Format(_T("<action type=\"%s\" actor=\"%s\" undoredo=\"%d\">\n"), _T("change"), userId.wc_str( ), aUndoRedoAction?1:0);
			fCompact = true;
		}
		xml += temp;

		if (obj)
		{
			obj->GetXML(xml, fCompact);
		}

		xml += _T("</action>\n");

		SendXML(xml);
	}
}
///////////////////////////////////////////////////////////////////////////////
CChangeAction::CChangeAction(CIICDoc* pDocument)
	: CAction(pDocument)
{
	m_nType = AT_CHANGE;
	m_pPreAction = NULL;
	m_pPostAction = NULL;
}

CChangeAction::~CChangeAction()
{
	if (m_pPreAction!=NULL)
	{
		delete m_pPreAction;
		m_pPreAction = NULL;
	}
	if (m_pPostAction!=NULL)
	{
		delete m_pPostAction;
		m_pPostAction = NULL;
	}
}

void CChangeAction::Undo(CDrawingObjects& src, IICRect& rect)
{
	m_pPreAction->Undo(src, rect);
}

void CChangeAction::Redo(CDrawingObjects& src, IICRect& rect)
{
	m_pPostAction->Redo(src, rect);
}

void CChangeAction::SendActionXML(bool bUndo, bool fCompact, bool aNotifyServer, bool aUndoRedoAction)
{
//	xml += _T("<?xml version=\"1.0\" encoding=\"UTF8\"?>\n");

	if (bUndo)
	{
		m_pPreAction->SendActionXML(false, true, aNotifyServer, aUndoRedoAction);
	}
	else
	{
		m_pPostAction->SendActionXML(false, true, aNotifyServer, aUndoRedoAction);
	}
}

void CChangeAction::PreAction(CDrawingObjects& src)
{
	m_pPreAction = new CAssignAction(m_pDocument);
	m_pPreAction->Do(src, true);
}

void CChangeAction::PostAction(CDrawingObjects& src)
{
	m_pPostAction = new CAssignAction(m_pDocument);
	m_pPostAction->Do(src);
}

///////////////////////////////////////////////////////////////////////////////
CReOrderAction::CReOrderAction(CIICDoc* pDocument, int nType)
	: CAction(pDocument)
{
	m_nType = nType;
}

void CReOrderAction::Undo(CDrawingObjects& src, IICRect& rect)
{
	if (m_nType == AT_SENDBACK)
	{
		OBJLIST::iterator i;
		for (i =  m_listObj.begin(); i != m_listObj.end(); ++i)
		{
			CBaseDrawingObject* obj = *i;

			if (obj)
			{
				rect = obj->UnionRect(rect);
				src.BringToFront(obj->GetID());
			}
		}
	}
	else
	{
		OBJLIST::reverse_iterator i;
		for (i =  m_listObj.rbegin(); i != m_listObj.rend(); ++i)
		{
			CBaseDrawingObject* obj = *i;

			if (obj)
			{
				rect = obj->UnionRect(rect);
				src.SendBack(obj->GetID());
			}
		}
	}
}

void CReOrderAction::Redo(CDrawingObjects& src, IICRect& rect)
{
	if (m_nType == AT_BRINGTOFRONT)
	{
		OBJLIST::iterator i;
		for (i =  m_listObj.begin(); i != m_listObj.end(); ++i)
		{
			CBaseDrawingObject* obj = *i;

			if (obj)
			{
				rect = obj->UnionRect(rect);
				src.BringToFront(obj->GetID());
			}
		}
	}
	else
	{
		OBJLIST::reverse_iterator i;
		for (i =  m_listObj.rbegin(); i != m_listObj.rend(); ++i)
		{
			CBaseDrawingObject* obj = *i;

			if (obj)
			{
				rect = obj->UnionRect(rect);
				src.SendBack(obj->GetID());
			}
		}
	}
}

void CReOrderAction::SendActionXML(bool bUndo, bool fCompact, bool aNotifyServer, bool aUndoRedoAction)
{
	if (!aNotifyServer || !IsMeetingInProgress())
		return;

//	xml += _T("<?xml version=\"1.0\" encoding=\"UTF8\"?>\n");
	wxString userId, temp, xml;
	m_pDocument->m_Meeting.GetMeetingUserID(userId);

	if ((bUndo && m_nType==AT_SENDBACK) || (!bUndo && m_nType==AT_BRINGTOFRONT))
        temp = wxString::Format(_T("<action type=\"%s\" actor=\"%s\" undoredo=\"%d\">\n"), _T("bringtofront"), userId.wc_str( ), aUndoRedoAction?1:0);
	else
		temp = wxString::Format(_T("<action type=\"%s\" actor=\"%s\" undoredo=\"%d\">\n"), _T("sendback"), userId.wc_str( ), aUndoRedoAction?1:0);
	xml += temp;

	OBJLIST::iterator i;
    for (i =  m_listObj.begin(); i != m_listObj.end(); ++i)
	{
		CBaseDrawingObject* obj = *i;

		if (obj)
		{
			obj->GetXML(xml, true);
		}
	}
	xml += _T("</action>\n");

	SendXML(xml);
}

