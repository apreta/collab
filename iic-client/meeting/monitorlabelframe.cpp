#include "monitorlabelframe.h"

//(*InternalHeaders(MonitorLabelFrame)
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(MonitorLabelFrame)
//*)
IMPLEMENT_CLASS(MonitorLabelFrame, wxFrame)

BEGIN_EVENT_TABLE(MonitorLabelFrame, wxFrame)
	//(*EventTable(MonitorLabelFrame)
	//*)
END_EVENT_TABLE()

MonitorLabelFrame::MonitorLabelFrame(wxWindow *parent, wxString& label, int x, int y)
{
	//(*Initialize(MonitorLabelFrame)
	wxXmlResource::Get()->LoadObject(this,parent,_T("MonitorLabelFrame"),_T("wxFrame"));
	m_DisplayLabel = (wxStaticText*)FindWindow(XRCID("ID_LABEL_STATIC"));
	//*)
	
	m_DisplayLabel->SetLabel(label);
	Move(x,y);
}

MonitorLabelFrame::~MonitorLabelFrame()
{
	//(*Destroy(MonitorLabelFrame)
	//*)
}
