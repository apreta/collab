/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"
#include "contacttreelist.h"
#include "meetingcentercontroller.h"
#include "contactservice.h"
#include <wx/tokenzr.h>

static int kStatusMeetingUnknown = 0;
static int kStatusMeetingOffline = 1;
static int kStatusMeetingOnline  = 2;

static int kStatusPhoneOffline = 0;
static int kStatusPhoneOnline  = 1;
static int kStatusPhoneDialing = 2;
static int kStatusPhoneRinging = 3;
static int kStatusPhoneBusy    = 4;
static int kStatusPhoneTalking = 5;
static int kStatusPhoneMute	   = 6;

static int kStatusContactOffline      = 0;
static int kStatusContactOnline       = 1;
static int kStatusContactAway         = 2;
static int kStatusContactGoingOffline = 3;
static int kStatusContactComingOnline = 4;


ContactTreeItemData::ContactTreeItemData( int nType, contact_t contact  ):
ContactCommonTreeItemData( nType ),
m_contact(contact)
{

}

ContactTreeItemData::ContactTreeItemData( int nType, contact_t contact, const wxString & password ):
ContactCommonTreeItemData( nType ),
m_contact(contact),
m_strPassword(password)
{

}

ContactTreeItemData::ContactTreeItemData( const ContactTreeItemData & from ):
ContactCommonTreeItemData(from),
m_contact(from.m_contact),
m_strPassword(from.m_strPassword)
{

}

GroupTreeItemData::GroupTreeItemData( int nType, group_t group ):
ContactCommonTreeItemData( nType ),
m_group(group)
{

}
GroupTreeItemData::GroupTreeItemData(const GroupTreeItemData & from):
ContactCommonTreeItemData( from ),
m_group(from.m_group)
{

}

ContactTreeList::ContactTreeList( wxTreeListCtrl *pTreeListCtrl ):
m_fInitialized(false),
m_pTree( pTreeListCtrl ),
m_nSearchNumber(0),
m_fEditingSearchCriteria(false),
m_fUseEditorImageList(false)
{
    m_nSearchNumber = time( NULL );
}

ContactTreeList::~ContactTreeList()
{
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_PRESENCE_EVENT,          PresenceEventHandler( ContactTreeList::OnPresenceEvent), 0, this );
}

void ContactTreeList::Initialize()
{
    if( m_fInitialized )
    {
        return;
    }

    m_fInitialized = true;


    if( m_pTree->GetColumnCount() == 0 )
    {
        m_ContactDisplayManager.UpdateColumnHeadings( *this );

        // The Root node exists for the life of the window.
        m_RootId = m_pTree->AddRoot( _T("Contacts"), -1, -1,
        new ContactCommonTreeItemData(Node_Root) );

        if( m_fUseEditorImageList )
        {
            // Load the images for the meeting list
            wxImageList *pImageList = new wxImageList( 18, 18 );
            wxBitmap contacts( wxGetApp().GetResourceFile(wxT("res/contacts.png")), wxBITMAP_TYPE_PNG );
            pImageList->Add( contacts );
            m_pTree->SetImageList(pImageList);
        }else
        {
            // Prepare the status images
            m_StatusOnline =    m_StatusImages.AddSubImageArray( wxGetApp().GetResourceFile(_T("res/status_meeting_online")), 3 );
            m_StatusPhone =     m_StatusImages.AddSubImageArray( wxGetApp().GetResourceFile(_T("res/status_meeting_phone")), 7 );
            m_StatusContact =   m_StatusImages.AddSubImageArray( wxGetApp().GetResourceFile(_T("res/status_meeting_contact")), 5 );
            m_StatusImages.Create( NULL );

            m_pTree->SetImageList( m_StatusImages.GetImageList() );
        }
    }

   	m_pTree->Connect(wxID_ANY,wxEVT_COMMAND_TREE_ITEM_COLLAPSED,(wxObjectEventFunction)&ContactTreeList::OnItemCollapsed, 0, this );
	m_pTree->Connect(wxID_ANY,wxEVT_COMMAND_TREE_ITEM_EXPANDED,(wxObjectEventFunction)&ContactTreeList::OnItemExpanded, 0, this);

    CONTROLLER.Connect(wxID_ANY, wxEVT_PRESENCE_EVENT,          PresenceEventHandler( ContactTreeList::OnPresenceEvent), 0, this );
}

void ContactTreeList::RemoveContacts()
{
    Tree()->DeleteChildren( m_CABId );
    Tree()->DeleteChildren( m_PABId );

    for (int i=0; i<m_DirectoryIds.GetCount(); i++)
        Tree()->Delete(m_DirectoryIds[i]);
    m_DirectoryIds.Empty();
}


wxTreeItemId ContactTreeList::AppendSearchResultsRoot()
{
    m_CommunitySearchId = m_pTree->AppendItem( m_RootId, _("Community search results"), -1, -1, new ContactCommonTreeItemData(Node_SearchRoot) );

    return m_CommunitySearchId;
}

wxTreeItemId ContactTreeList::AppendDirectoryNode( const wxString & title, directory_t contact_dir, int nDirType, bool preview )
{
    Initialize();
    wxTreeItemId directory_node = NULL;
    int iImage = 0;
    int nDirNodeType = Node_PersonalDir;

    switch( nDirType )
    {
    case dtGlobal:      nDirNodeType = Node_GlobalDir; break;
    case dtPersonal:    nDirNodeType = Node_PersonalDir; break;
    default:            nDirNodeType = Node_UserDir; break;
    }

    wxLogDebug(wxT("ContactTreeList::AppendDirectoryNode %s"),title.c_str() );

    if( nDirNodeType == Node_GlobalDir && m_CABId.IsOk() )
    {
        directory_node = m_CABId;
    }
    else if( nDirNodeType == Node_PersonalDir && m_PABId.IsOk() )
    {
        directory_node = m_PABId;
    }
    else if( nDirNodeType == Node_UserDir )
    {
        directory_node = FindUserDirectory(title);
    }
    
    if ( directory_node == (wxTreeItemId)NULL )
    {
        directory_node = m_pTree->AppendItem( m_RootId, title, -1, -1, new ContactCommonTreeItemData( nDirNodeType ) );

        if( nDirNodeType == Node_GlobalDir )
        {
            m_CABId = directory_node;
        }
        if( nDirNodeType == Node_PersonalDir )
        {
            m_PABId = directory_node;
        }
        if( nDirNodeType == Node_UserDir )
        {
            m_DirectoryIds.Add(directory_node);
            if (preview)
                m_pTree->AppendItem( directory_node, _("Downloading..."), -1, -1, new ContactCommonTreeItemData( nDirNodeType ) );
        }
    }
    else 
    {
        Tree()->DeleteChildren( directory_node );
    }

    if( !contact_dir || !contact_dir->enabled )
        return 0;

/*
group_iterator_t    directory_group_iterate(directory_t dir);
group_t             directory_group_next(directory_t dir, group_iterator_t iter);
int                 directory_group_get_size(group_t group);
const char *        directory_group_get_member(group_t group, int member);
*/
    group_iterator_t git_unsorted = directory_group_iterate( contact_dir );
    group_t group;

    // For all groups within this directory, add a new group node.
    while( (group = directory_group_next( contact_dir, git_unsorted )) != NULL )
    {
        wxString group_id(group->group_id,wxConvUTF8);
        wxString rawName(group->name,wxConvUTF8);
        wxString name( wxGetTranslation( rawName ) );
        wxString type(wxT("Unknown type"));
        if( group->type == dtAll) type = wxT("All");
        if( group->type == dtBuddies ) type = wxT("Buddies");
        if( group->type == dtOther) type = wxT("Other");
        wxLogDebug(wxT("   Group: Id:%s Type: %s |%s|"),group_id.c_str(),type.c_str(),name.c_str());

        wxTreeItemId group_node;
        iImage = ( m_fUseEditorImageList ) ? EditImageGroup : -1;

        group_node = m_pTree->AppendItem( directory_node, name, iImage, iImage, new GroupTreeItemData(Node_Group, group) );

        wxSortedArrayString     arrUserIDs;

        // For all userids in this group, lookup the contact record and add the contact node.
        for( int iG = 0; iG < directory_group_get_size(group); ++ iG )
        {
            wxString                userid( directory_group_get_member( group, iG ), wxConvUTF8 );
            contact_t               contact = FindContact( userid );
            if( !contact )
            {
                wxLogDebug( wxT("ContactTreeList::AppendDirectoryNode - Data error: The contact record for userid %s cannot be found."),userid.c_str() );
                continue;
            }

            if (contact != NULL)    // Shouldn't be NULL, but could be bad data from server
            {
                //  Don't add it again if this contact is already in the tree
                /*
                if( arrUserIDs.Index( userid )  !=  wxNOT_FOUND )
                {
                    wxLogDebug( wxT("ContactTreeList::AppendDirectoryNode - Logic error: The contact record for userid %s is already displayed in the tree."),userid.c_str() );
                    continue;
                }
                */
                arrUserIDs.Add( userid );

                // Add the contact
                iImage = 0;
                if( m_fUseEditorImageList )
                {
                    iImage = ( contact->type == UserTypeContact ) ? EditImageContact : EditImageUser;
                }
                wxTreeItemId idContact = m_pTree->AppendItem( group_node, title, iImage, iImage, new ContactTreeItemData( Node_Contact, contact ) );
                UpdateDisplay( contact );

                SetPresence( idContact );
            }
        }
        ExpandNodeIfRequired( group_node );
    }

    ExpandNodeIfRequired( directory_node );

    SetPresence( m_RootId );

    return directory_node;
}

bool ContactTreeList::HasContactsSelected()
{
    bool fFoundContact = false;
    wxArrayTreeItemIds selections;
    unsigned int nSel = m_pTree->GetSelections( selections );
    for( unsigned int i = 0; i < nSel; ++i )
    {
        ContactTreeItemData *pData = (ContactTreeItemData *)m_pTree->GetItemData( selections[i] );
        if( pData && pData->m_contact )
        {
            fFoundContact = true;
            break;
        }
    }
    return fFoundContact;
}

void ContactTreeList::StartContactSearch( const ContactSearch & criteria )
{
    wxLogDebug(wxT("ContactTreeList::StartContactSearch"));

    wxString encodedCriteria;
    criteria.Encode( encodedCriteria );
    bool fFoundMatchingSearch = false;
    wxTreeItemId parent;
    int nSearchNumber;

    // Look for an existing search mith matching criteria:
    // If one is found then that node is repopulated, a new search results
    // node is created otherwise.
    wxTreeItemIdValue cookie;
    for ( wxTreeItemId item = m_pTree->GetFirstChild(m_CommunitySearchId, cookie);
      item.IsOk();
      item = m_pTree->GetNextChild(m_CommunitySearchId, cookie) )
    {
        ContactSearchResultsItemData *pData = (ContactSearchResultsItemData *)m_pTree->GetItemData(item); // Const cast s/b safe enough
        if( pData->m_strSearchCriteria == encodedCriteria )
        {
            fFoundMatchingSearch = true;
            parent = item;
            m_pTree->DeleteChildren( parent );
            nSearchNumber = pData->m_nSearchNumber;
            wxLogDebug(wxT("   Found existing search results node: Search Number = %d"), nSearchNumber );
            break;
        }
    }

    if( !fFoundMatchingSearch )
    {
        ContactSearchResultsItemData *pItemData = new ContactSearchResultsItemData( Node_SearchResults );
        pItemData->m_strSearchCriteriaName = criteria.m_strSearchName;
        pItemData->m_strSearchCriteria = encodedCriteria;
        nSearchNumber = pItemData->m_nSearchNumber = m_nSearchNumber++;

        parent = m_pTree->AppendItem( m_CommunitySearchId, _("Searching..."), -1, -1, pItemData );
        wxLogDebug(wxT("   Creating new search results node: Search Number = %d"), nSearchNumber );
    }
    StartContactSearch( parent );
}

void ContactTreeList::PrepareSearchValue( wxString & strValue )
{
    wxString strSqlWildcard(wxT("%"));

    strValue.Trim(true);                        // remove trailing blanks
    strValue.Trim(false);                       // remove leading blanks
    strValue.Replace(wxT("*"),strSqlWildcard);  // change to SQL style wildcards

    // now implement "contains" search by placing
    // wildcards at the beginning and end of the
    // search value...

    // add a wildcard to the beginning, if necessary
    if (strValue.Find('%') != 0)
        strValue.Prepend(strSqlWildcard);

    // add a wildcard to the end, if necessary
    if (strValue.Find('%', true) != (strValue.length() - 1))
        strValue.Append(strSqlWildcard);
}

void ContactTreeList::StartContactSearch( const wxTreeItemId & id )
{
    ContactSearchResultsItemData *pSrch = GetSearchNode( id );
    if( !pSrch )
        return;
    ContactSearch criteria;
    criteria.Decode( pSrch->m_strSearchCriteria );

    contact_condition_t c = contact_condition_new();
    wxString strValue;

    if( criteria.m_nSelectedGroups & ContactSearch::Groups_Global )
    {
        contact_condition_add_directory( c, dtGlobal );
    }

    if( criteria.m_nSelectedGroups & ContactSearch::Groups_Personal )
    {
        contact_condition_add_directory( c, dtPersonal );
    }

    if( criteria.m_fEmailAddresses )
    {
        strValue = criteria.m_strEmailAddress;
        this->PrepareSearchValue(strValue);

        // NOTE: "emails" is a virtual field.  The server will search all email fields.
        contact_condition_add_field(c, "emails", "LIKE-NOCASE", strValue.mb_str(wxConvUTF8) );
    }

    if( criteria.m_fPhoneNumbers )
    {
        strValue = criteria.m_strPhoneNumbers;
        this->PrepareSearchValue(strValue);

        // NOTE: "phones" is a virtual field.  The server will search all phone fields.
        contact_condition_add_field(c, "phones", "LIKE-NOCASE",  strValue.mb_str(wxConvUTF8) );
    }

    if( criteria.m_fFirstName )
    {
        strValue = criteria.m_strFirstName;
        this->PrepareSearchValue(strValue);
        wxCharBuffer value = strValue.mb_str();

        contact_condition_add_field(c, "first",     "LIKE-NOCASE",  value );
    }

    if( criteria.m_fLastName )
    {
        strValue = criteria.m_strLastName;
        this->PrepareSearchValue(strValue);

        contact_condition_add_field(c, "last",      "LIKE-NOCASE",  strValue.mb_str(wxConvUTF8) );
    }

    if( criteria.m_fMore1 )
    {
        strValue = criteria.m_strField1Text;
        this->PrepareSearchValue(strValue);

        wxCharBuffer field = criteria.DatabaseField( criteria.m_nField1 ).mb_str();
        wxCharBuffer value = strValue.mb_str(wxConvUTF8);
        contact_condition_add_field( c, field, "LIKE-NOCASE", value );
    }

    if( criteria.m_fMore2 )
    {
        strValue = criteria.m_strField2Text;
        this->PrepareSearchValue(strValue);

        wxCharBuffer field = criteria.DatabaseField( criteria.m_nField2 ).mb_str();
        wxCharBuffer value = strValue.mb_str(wxConvUTF8);
        contact_condition_add_field( c, field, "LIKE-NOCASE", value );
    }

    if( criteria.m_fMore3 )
    {
        strValue = criteria.m_strField3Text;
        this->PrepareSearchValue(strValue);

        wxCharBuffer field = criteria.DatabaseField( criteria.m_nField3 ).mb_str();
        wxCharBuffer value = strValue.mb_str(wxConvUTF8);
        contact_condition_add_field( c, field, "LIKE-NOCASE", value );
    }

    wxString opid;
    opid = CONTROLLER.opcode( MeetingCenterController::OP_EditMeeting_Search, pSrch->m_nSearchNumber );

    addressbk_search_contacts(CONTROLLER.book, opid.mb_str(), c, "first", pSrch->m_nStartingRow, criteria.m_nMaxResults );

    contact_condition_destroy(c);
}

ContactSearchResultsItemData * ContactTreeList::GetSearchNode()
{
    ContactSearchResultsItemData * ret = 0;
    wxTreeItemId selId;
    selId = m_pTree->GetSelection();
    if( !selId.IsOk() )
    {
        return ret; // No selection!
    }
    ret = GetSearchNode( selId );
    return ret;
}

ContactSearchResultsItemData * ContactTreeList::GetSearchNode( const wxTreeItemId & id )
{
    ContactCommonTreeItemData *pCommonData = (ContactCommonTreeItemData *)m_pTree->GetItemData( id );
    ContactSearchResultsItemData * ret = 0;
    if( pCommonData->m_nType == Node_SearchResults )
    {
        ret = (ContactSearchResultsItemData *)pCommonData;
    }
    return ret;
}

void ContactTreeList::ProcessOnDirectoryFetched( wxCommandEvent & event )
{
    wxString opid;
    opid = event.GetString();
    wxString strSearchOp;
    strSearchOp = CONTROLLER.opcode( MeetingCenterController::OP_EditMeeting_Search );
    strSearchOp += wxT(":");

    if( opid.Left( strSearchOp.length() ) != strSearchOp )
    {
        // Not from around here
        event.Skip();
        return;
    }

    directory_t dir = (directory_t)event.GetClientData();

    // Get the search number and it's associated node data
    ContactSearchResultsItemData *pSrch = 0;
    wxString strSearchNumber;
    strSearchNumber = event.GetString().Mid( strSearchOp.length() );
    long nSearchNumber = -1;
    bool fUnknownParent = true;
    wxTreeItemId parent_node;

    bool fOK = strSearchNumber.ToLong( &nSearchNumber, 10 );
    if( fOK )
    {
        // Locate the parent node for this search number
        wxTreeItemIdValue cookie;
        fUnknownParent = true;
        for ( wxTreeItemId item = m_pTree->GetFirstChild(m_CommunitySearchId, cookie);
          item.IsOk();
          item = m_pTree->GetNextChild(m_CommunitySearchId, cookie) )
        {
            ContactSearchResultsItemData *pData = (ContactSearchResultsItemData *)m_pTree->GetItemData(item); // Const cast s/b safe enough
            if( pData->m_nSearchNumber == nSearchNumber )
            {
                parent_node = item;
                fUnknownParent = false;
                pSrch = GetSearchNode( parent_node );
                break;
            }
        }
    }

    if( fUnknownParent )
        return;

    contact_iterator_t iter;
    contact_t contact;
    int nRecords = 0;

    iter = directory_contact_iterate( dir );

    while ((contact = directory_contact_next( dir, iter)) != NULL)
    {
        ++nRecords;

        // If we have already displayed partial search results.
        if( nRecords <= pSrch->m_nStartingRow )
            continue; // Skip this record...

        int iImage = 0;
        if( m_fUseEditorImageList )
        {
            iImage = ( contact->type == UserTypeContact ) ? EditImageContact : EditImageUser;
        }

        wxString title(wxT(""));
        wxTreeItemId idContact = m_pTree->AppendItem( parent_node, title, iImage, iImage, new ContactTreeItemData( Node_Contact, contact ) );
        UpdateDisplay( contact );
    }

    // Figure out the current state of the search
    ContactSearch criteria;
    criteria.Decode( pSrch->m_strSearchCriteria );

    // Track the list in case we need to clean it up later
    pSrch->m_dir = dir;

    if( pSrch->m_nSearchNumber == 0 )
    {
        // Make sure the outer search node is expanded if this is the first
        // search.
        m_pTree->Expand( m_CommunitySearchId );
    }
    if( pSrch->m_nStartingRow == 0 )
    {
        // Make sure the search results node is expanded on the first chunk
        m_pTree->Expand( parent_node );
        m_pTree->EnsureVisible( parent_node );

        // Should I select the node as well ??????
    }

    m_pTree->SortChildren( parent_node );

    wxLogDebug(wxT("ContactTreeList::ProcessOnDirectoryFetched  nRecords = %d, nTotalRetrieved = %d, nStartingRow = %d"),
            nRecords, pSrch->m_nTotalRetrieved, pSrch->m_nStartingRow );

    if( !pSrch->m_fComplete )
    {
        wxString searchTitlePrefix(wxT(""));
        wxString searchDesc;
        wxString searchTitle;

        searchDesc = criteria.FormatDescription();

        if( nRecords == 0 )
        {
            // Empty result set
            pSrch->m_fComplete = true;
            searchTitlePrefix = _("No contacts found");
        }else
        {
            if( nRecords < criteria.m_nMaxResults )
            {
                // We got back fewer records than we asked for,
                // must be the end of the data
                pSrch->m_fComplete = true;
                searchTitlePrefix = _("Search complete");
            }else if( nRecords == pSrch->m_nTotalRetrieved )
            {
                // No change in the total indicates the end of the list
                pSrch->m_fComplete = true;
                searchTitlePrefix = _("Search complete");
            }else if( (nRecords - pSrch->m_nTotalRetrieved) < criteria.m_nMaxResults )
            {
                // We got back fewer records than we asked for on this last pass,
                // must be the end of the data
                pSrch->m_fComplete = true;
                searchTitlePrefix = _("Search complete");
            }else
            {
                searchTitlePrefix = _("Partial search");
            }
        }

        // If the search is now complete
        if( pSrch->m_fComplete )
        {
            // Todo: figure out alternate way to delete search directory--needs to be removed from iiclib layer also
            //meeting_list_destroy(list);
            //pSrch->m_list = 0;
            //SetUIFeedback( 0, pSrch );
        }

        // Update the search results title as required
        searchTitle = searchTitlePrefix;
        searchTitle += wxT(": ");
        searchTitle += searchDesc;
        m_pTree->SetItemText( parent_node, searchTitle );
    }
    // Track the total number of records retrieved.
    pSrch->m_nTotalRetrieved = nRecords;

    // Remember where to start on the next pass
    pSrch->m_nStartingRow = nRecords;

}

bool ContactTreeList::EditCurrentSearch( bool f )
{
    bool ret = m_fEditingSearchCriteria;
    m_fEditingSearchCriteria = f;
    return ret;
}

void ContactTreeList::RemoveSearch()
{
    ContactSearchResultsItemData *pSrch = GetSearchNode();
    if( !pSrch )
        return;

    wxTreeItemId searchNode = m_pTree->GetSelection();

    // Todo: figure out alternate way to delete search directory--needs to be removed from iiclib layer also
    //if( pSrch->m_list )
    //{
    //    meeting_list_destroy(pSrch->m_list);
    //    pSrch->m_list = 0;
    //}

    // Remove the children first to avoid EVT_TREE_DELETE_ITEM events
    // that we don't care about
    m_pTree->DeleteChildren( searchNode );

    // Now remove the node
    m_pTree->Delete( searchNode );
}

int ContactTreeList::GetSelectionInfo( wxArrayTreeItemIds & selections, size_t & nSelections )
{
    nSelections = m_pTree->GetSelections(selections);
    int nSelectionMask = 0;

    for( size_t i = 0; i < nSelections; ++i )
    {
        wxTreeItemId id = selections[i];
        nSelectionMask |= GetNodeType( id );
    }
    return nSelectionMask;
}

size_t ContactTreeList::GetBuddyCount( wxArrayTreeItemIds & selections )
{
    size_t nBuddyCount = 0;

    wxTreeItemId buddyGroupId;
    GetBuddiesGroup( buddyGroupId );
    if( !buddyGroupId.IsOk( ) )
        return nBuddyCount;

    wxTreeItemId itemId;

    // For all selections
    for( size_t i = 0; i < selections.GetCount(); ++i )
    {
        itemId = selections[i];
        ContactTreeItemData *pData1 = (ContactTreeItemData *)m_pTree->GetItemData( itemId );
        if( !GetNodeType( itemId ) == Node_Contact )
        {
            continue;
        }

        // For all entries in the buddy node
        if( 0  ==  (int) m_pTree->GetChildrenCount( buddyGroupId, false ) )
            continue;

        wxTreeItemIdValue cookie;
        for ( wxTreeItemId item = m_pTree->GetFirstChild( buddyGroupId, cookie);
          item.IsOk();
          item = m_pTree->GetNextChild(buddyGroupId, cookie) )
        {
            ContactTreeItemData *pData2 = (ContactTreeItemData *)m_pTree->GetItemData( item );

            if( pData1->m_contact == pData2->m_contact )
            {
                ++nBuddyCount;
            }
        } // end for all entries in the buddy node
    } // end for all selections
    return nBuddyCount;
}

wxTreeItemId ContactTreeList::FindBuddyNode( contact_t contact )
{
    wxTreeItemId buddyId;

    wxTreeItemId buddyGroupId;
    GetBuddiesGroup( buddyGroupId );
    if( !buddyGroupId.IsOk() )
    {
        return buddyId;
    }

    // For all entries in the buddy node
    wxTreeItemIdValue cookie;
    for ( wxTreeItemId item = m_pTree->GetFirstChild( buddyGroupId, cookie);
      item.IsOk();
      item = m_pTree->GetNextChild(buddyGroupId, cookie) )
    {
        if( GetNodeType( item ) == Node_Contact )
        {
            ContactTreeItemData *pData = (ContactTreeItemData *)m_pTree->GetItemData( item );

            if( contact == pData->m_contact )
            {
                buddyId = item;
                return buddyId;
            }
        }
    } // end for all entries in the buddy node

    return buddyId;
}


group_t ContactTreeList::GetCABAllGroup( wxTreeItemId & id )
{
    group_t pRet = 0;

    if( m_CABId.IsOk() )
    {
        wxTreeItemIdValue cookie;
        for ( wxTreeItemId child = Tree()->GetFirstChild(m_CABId, cookie);
          child.IsOk();
          child = Tree()->GetNextChild(m_CABId, cookie) )
        {
            if( GetNodeType( child ) == Node_Group )
            {
                GroupTreeItemData *pData = (GroupTreeItemData *)Tree()->GetItemData(child);
                if( pData->m_group->type == dtAll )
                {
                    pRet = pData->m_group;
                    id = child;
                    break;
                }
            }
        }
    }

    return pRet;
}

group_t ContactTreeList::GetPABAllGroup( wxTreeItemId & id )
{
    group_t pRet = 0;

    if( m_PABId.IsOk() )
    {
        wxTreeItemIdValue cookie;
        for ( wxTreeItemId child = Tree()->GetFirstChild(m_PABId, cookie);
          child.IsOk();
          child = Tree()->GetNextChild(m_PABId, cookie) )
        {
            if( GetNodeType( child ) == Node_Group )
            {
                GroupTreeItemData *pData = (GroupTreeItemData *)Tree()->GetItemData(child);
                if( pData->m_group->type == dtAll )
                {
                    pRet = pData->m_group;
                    id = child;
                    break;
                }
            }
        }
    }

    return pRet;
}

group_t ContactTreeList::GetBuddiesGroup( wxTreeItemId & id )
{
    group_t pRet = 0;

    if( m_PABId.IsOk() )
    {
        wxTreeItemIdValue cookie;
        for ( wxTreeItemId child = Tree()->GetFirstChild(m_PABId, cookie);
          child.IsOk();
          child = Tree()->GetNextChild(m_PABId, cookie) )
        {
            if( GetNodeType( child ) == Node_Group )
            {
                GroupTreeItemData *pData = (GroupTreeItemData *)Tree()->GetItemData(child);
                if( pData->m_group->type == dtBuddies )
                {
                    pRet = pData->m_group;
                    id = child;
                    break;
                }
            }
        }
    }

    return pRet;
}

wxTreeItemId ContactTreeList::AppendContact( directory_t dir, contact_t contact )
{
    // Locat the "All" group for the new contact.
    wxTreeItemId group_node;
    if( dir->type == dtGlobal )
    {
        wxTreeItemIdValue cookie;
        for ( wxTreeItemId child = Tree()->GetFirstChild(m_CABId, cookie);
          child.IsOk();
          child = Tree()->GetNextChild(m_CABId, cookie) )
        {
            if( GetNodeType( child ) == Node_Group )
            {
                GroupTreeItemData *pData = (GroupTreeItemData *)Tree()->GetItemData(child);
                if( pData->m_group->type == dtAll )
                {
                    group_node = child;
                    break;
                }
            }
        }
    }else if( dir->type == dtPersonal )
    {
        wxTreeItemIdValue cookie;
        for ( wxTreeItemId child = Tree()->GetFirstChild(m_PABId, cookie);
          child.IsOk();
          child = Tree()->GetNextChild(m_PABId, cookie) )
        {
            if( GetNodeType( child ) == Node_Group )
            {
                GroupTreeItemData *pData = (GroupTreeItemData *)Tree()->GetItemData(child);
                if( pData->m_group->type == dtAll )
                {
                    group_node = child;
                    break;
                }
            }
        }
    }

    // Add the contact
    int iImage = ( m_fUseEditorImageList ) ? EditImageContact : 0;
    if( contact->type == UserTypeUser || contact->type == UserTypeAdmin )
    {
        iImage = (m_fUseEditorImageList) ? EditImageUser : 0;
    }

    ContactTreeItemData *pData = new ContactTreeItemData( Node_Contact, contact );
    if( !(contact->userid && *contact->userid) )
    {
        // No contact.userid, must be new
        pData->m_nStatus = Node_Status_New;
    }

    wxTreeItemId idContact = m_pTree->AppendItem( group_node, wxT(""), iImage, iImage, pData );
    UpdateDisplay( contact );

    SetPresence( idContact );

    m_pTree->SortChildren( group_node );

    return idContact;
}

wxTreeItemId ContactTreeList::AppendContact( directory_t dir, contact_t contact, const wxString & password )
{
    wxTreeItemId contact_node = AppendContact( dir, contact );
    if( dir->type == dtGlobal && contact_node.IsOk() )
    {
        ContactTreeItemData *pData = (ContactTreeItemData *)m_pTree->GetItemData( contact_node );
        pData->m_strPassword = password;
    }
    return contact_node;
}

wxTreeItemId ContactTreeList::AppendContact( directory_t dir, const wxTreeItemId & group_node, ContactTreeItemData *pContactData )
{
    // Add the contact
    int iImage = ( m_fUseEditorImageList ) ? EditImageContact : 0;
    contact_t contact = pContactData->m_contact;

    wxTreeItemId idContact = m_pTree->AppendItem( group_node, wxT(""), iImage, iImage, pContactData );
    UpdateDisplay( contact );

    SetPresence( idContact );

    m_pTree->SortChildren( group_node );

    return idContact;
}

wxTreeItemId ContactTreeList::AppendGroup( directory_t dir, group_t group )
{
    wxTreeItemId directory_node;

    if( dir->type == dtGlobal )
    {
        directory_node = m_CABId;
    }else
    {
        directory_node = m_PABId;
    }

    wxString name(group->name,wxConvUTF8);

    int iImage = ( m_fUseEditorImageList ) ? EditImageGroup : -1;

    wxTreeItemId group_node = m_pTree->AppendItem( directory_node, name, iImage, iImage, new GroupTreeItemData(Node_Group, group) );
    if( !(group->group_id && *group->group_id) )
    {
        GroupTreeItemData *pData = (GroupTreeItemData *)m_pTree->GetItemData( group_node );
        pData->m_nStatus = Node_Status_New;
    }

    return group_node;
}

contact_t ContactTreeList::FindContact( const wxString & userid )
{
    contact_t contact = 0;

    contact = CONTROLLER.Directories()->FindContact(userid);

/*
    if( CONTROLLER.GetPAB() )
    {
        contact = directory_find_contact(CONTROLLER.GetPAB(), userid.mb_str(wxConvUTF8) );
    }
    if( !contact && CONTROLLER.GetCAB() )
    {
        contact = directory_find_contact(CONTROLLER.GetCAB(), userid.mb_str(wxConvUTF8) );
    }
*/
    return contact;
}

int ContactTreeList::RemoveContactNodes( contact_t contact )
{
    int nTotalRemoved = 0;
    RemoveContactNodes( m_RootId, contact, nTotalRemoved );
    return 0;
}

void ContactTreeList::RemoveContactNodes( wxTreeItemId parent, contact_t contact, int & count )
{
    wxTreeItemIdValue cookie;
    for ( wxTreeItemId item = m_pTree->GetFirstChild(parent, cookie);
      item.IsOk();
      item = m_pTree->GetNextChild(parent, cookie) )
    {
        int nodeType = GetNodeType( item );
        if( nodeType == Node_Contact )
        {
            ContactTreeItemData *pCData = (ContactTreeItemData *)m_pTree->GetItemData( item );
            if( pCData->m_contact == contact )
            {
                m_pTree->Delete( item );
                ++count;
            }
        }else
        {
            RemoveContactNodes( item, contact, count );
        }
    }
}

void ContactTreeList::UpdateDisplay( contact_t contact )
{
    UpdateDisplay( m_RootId, contact );
}

void ContactTreeList::UpdateDisplay( wxTreeItemId parent, contact_t contact )
{
    wxTreeItemIdValue cookie;
    for ( wxTreeItemId item = m_pTree->GetFirstChild(parent, cookie);
      item.IsOk();
      item = m_pTree->GetNextChild(parent, cookie) )
    {
        int nodeType = GetNodeType( item );
        if( nodeType == Node_Contact )
        {
            ContactTreeItemData *pCData = (ContactTreeItemData *)m_pTree->GetItemData( item );
            if( pCData->m_contact == contact )
            {
                //wxLogDebug(wxT("ContactTreeList::UpdateDisplay - Updating matching contact."));
                UpdateDisplay( item );
            }
        }else
        {
            UpdateDisplay( item, contact );
        }
    }
}

void ContactTreeList::UpdateDisplay( wxTreeItemId & item )
{
    m_ContactDisplayManager.UpdateColumnDisplay( *this, item );
}

void ContactTreeList::SetPresence( wxTreeItemId parent )
{
    if( m_fUseEditorImageList )
        return;

    wxTreeItemIdValue cookie;
    for ( wxTreeItemId item = m_pTree->GetFirstChild(parent, cookie);
      item.IsOk();
      item = m_pTree->GetNextChild(parent, cookie) )
    {
        int nodeType = GetNodeType( item );
        if( nodeType == Node_Contact )
        {
            ContactTreeItemData *pCData = (ContactTreeItemData *)m_pTree->GetItemData( item );
            if( pCData->m_contact && pCData->m_contact->screenname )
            {
                PresenceInfo * info = CONTROLLER.Presence().GetPresence(_T("zon"), wxString(pCData->m_contact->screenname, wxConvUTF8).wc_str());
                if( info )
                {
                    int intContactStatus;
                    switch (info->GetPresence())
                    {
                        case PresenceInfo::PresenceAway:
                            intContactStatus = kStatusContactAway;
                            break;
                        case PresenceInfo::PresenceAvailable:
                            intContactStatus = kStatusContactOnline;
                            break;
                        case PresenceInfo::PresenceUnavailable:
                        default:
                            intContactStatus = kStatusContactOffline;
                    }

                    int intMeetingStatus = kStatusMeetingUnknown;
                    if (info->GetSpecial() & PresenceInfo::SpecialMeeting)
                        intMeetingStatus = kStatusMeetingOnline;

                    int intPhoneStatus = kStatusPhoneOffline;
                    if (info->GetSpecial() & PresenceInfo::SpecialPhone)
                        intPhoneStatus = kStatusPhoneOnline;

                    m_StatusImages.SelectSubImage(m_StatusOnline, intMeetingStatus);
                    m_StatusImages.SelectSubImage(m_StatusPhone, intPhoneStatus);
                    m_StatusImages.SelectSubImage(m_StatusContact, intContactStatus);
                    int statusImage = m_StatusImages.GetSelectedListIndex( );

                    m_pTree->SetItemImage( item, statusImage );
                    m_pTree->SetItemImage( item, statusImage, wxTreeItemIcon_Selected );
                }
            }
        }else
        {
            SetPresence( item );
        }
    }
}

void ContactTreeList::OnPresenceEvent( PresenceEvent& event )
{
    SetPresence( m_RootId );
    event.Skip();
}

//////////////////////////////////////////////////////////////////////////////////////////////
ContactTreeListData::~ContactTreeListData()
{

}

int ContactTreeListData::CopySelectedData( ContactTreeList & src )
{
    int nRet = CopySelectedData( src, m_EncodedContact );
    return nRet;
}

int ContactTreeListData::CopySelectedData( ContactTreeList & src, wxArrayString & dst )
{
    int nRet = 0;
    wxArrayTreeItemIds selections;
    size_t nSelected;
    src.GetSelectionInfo( selections, nSelected );

    dst.Clear();

    if( nSelected > 0 )
    {
        for( size_t i = 0; i < nSelected; ++i )
        {
            wxTreeItemId item = selections[i];
            if( src.GetNodeType( item ) == ContactTreeList::Node_Contact )
            {
                ContactTreeItemData *pCData = (ContactTreeItemData *)src.Tree()->GetItemData( item );
                contact_t contact = pCData->m_contact;

                dst.Add( EncodeContactRecord( contact ) );
            }
        }
    }
    return nRet;
}

contact_t ContactTreeListData::CreateContact( directory_t dir, size_t ndx, bool fCreatingBuddy )
{
    contact_t contact = CreateContact( dir, m_EncodedContact, ndx, fCreatingBuddy );
    return contact;
}

contact_t ContactTreeListData::CreateContact( directory_t dir, wxArrayString src, size_t ndx, bool fCreatingBuddy )
{
    contact_t contact = 0;
    if( (ndx >= 0) && (ndx < src.GetCount()) )
    {
        wxString strEncodedContact = src[ndx];
        contact = DecodeContactRecord( dir, strEncodedContact, fCreatingBuddy );
    }
    return contact;
}

wxString ContactTreeListData::EncodeContactRecord( contact_t contact )
{
    wxString strEncoded(wxT(""));
    wxString strDelimiter(wxT("|"));

    //pstring     userid;
    //pstring     username;           // full jabber id

    /* 00 */  AppendField( contact->screenname, strEncoded );
    /* 01 */  AppendField( contact->title, strEncoded );
    /* 02 */  AppendField( contact->first, strEncoded );
    /* 03 */  AppendField( contact->middle, strEncoded );
    /* 04 */  AppendField( contact->last, strEncoded );
    /* 05 */  AppendField( contact->suffix, strEncoded );
    /* 06 */  AppendField( contact->company, strEncoded );
    /* 07 */  AppendField( contact->jobtitle, strEncoded );
    /* 08 */  AppendField( contact->address1, strEncoded );
    /* 09 */  AppendField( contact->address2, strEncoded );
    /* 10 */  AppendField( contact->city, strEncoded );  // missing on server, oops
    /* 11 */  AppendField( contact->state, strEncoded );
    /* 12 */  AppendField( contact->country, strEncoded );
    /* 13 */  AppendField( contact->postalcode, strEncoded );
    /* 14 */  AppendField( contact->email, strEncoded );
    /* 15 */  AppendField( contact->email2, strEncoded );
    /* 16 */  AppendField( contact->email3, strEncoded );
    /* 17 */  AppendField( contact->busphone, strEncoded );
    /* 18 */  AppendField( contact->homephone, strEncoded );
    /* 19 */  AppendField( contact->mobilephone, strEncoded );
    /* 20 */  AppendField( contact->otherphone, strEncoded );
    /* 21 */  AppendField( contact->aimscreen, strEncoded );
    /* 22 */  AppendField( contact->msnscreen, strEncoded );
    /* 23 */  AppendField( contact->yahooscreen, strEncoded );
    /* 24 */  AppendField( contact->user1, strEncoded );
    /* 25 */  AppendField( contact->user2, strEncoded );
    /* 26 */  AppendField( contact->profileid, strEncoded, false /* No Delimiter. This is the last field */ );

    wxLogDebug(wxT("Enocded Contact Rec: \"%s\""),strEncoded.c_str() );
    return strEncoded;
}

void ContactTreeListData::AppendField( pstring pfield, wxString & strEncoded, bool fAppendDelimiter )
{

    if( pfield && *pfield )
    {
        wxString strField( pfield, wxConvUTF8 );
        strEncoded += strField;
    }

    if( fAppendDelimiter )
    {
        strEncoded += wxT("|");
    }

}

contact_t ContactTreeListData::DecodeContactRecord( directory_t dir, const wxString & strEncoded, bool fCreatingBuddy )
{
    contact_t contact = 0;
    wxArrayString arrayData;
    wxStringTokenizer tkz( strEncoded, wxT("|"), wxTOKEN_RET_EMPTY	);

    while ( tkz.HasMoreTokens() )
    {
        wxString token = tkz.GetNextToken();
        arrayData.Add( token );
    }
    // Sanity check ?

    // Create a new (empty) contact record
    contact = directory_contact_new( dir );
    wxString strField;

    wxLogDebug(wxT("DecodeContactRecord: (%d fields)"), arrayData.GetCount() );

    int     iMaxFields = (int) arrayData.GetCount( );
    if( fCreatingBuddy )
        iMaxFields = 6;
    for( int i = 0; i < iMaxFields; ++i )
    {
        strField = arrayData[i];
        switch( i )
        {
        case 0: contact->screenname = allocField( dir, strField ); break;
        case 1: contact->title = allocField( dir, strField ); break;
        case 2: contact->first = allocField( dir, strField ); break;
        case 3: contact->middle = allocField( dir, strField ); break;
        case 4: contact->last = allocField( dir, strField ); break;
        case 5: contact->suffix = allocField( dir, strField ); break;
        case 6: contact->company = allocField( dir, strField ); break;
        case 7: contact->jobtitle = allocField( dir, strField ); break;
        case 8: contact->address1 = allocField( dir, strField ); break;
        case 9: contact->address2 = allocField( dir, strField ); break;
        case 10:contact->city = allocField( dir, strField ); break;  // missing on server, oops
        case 11:contact->state = allocField( dir, strField ); break;
        case 12:contact->country = allocField( dir, strField ); break;
        case 13:contact->postalcode = allocField( dir, strField ); break;
        case 14:contact->email = allocField( dir, strField ); break;
        case 15:contact->email2 = allocField( dir, strField ); break;
        case 16:contact->email3 = allocField( dir, strField ); break;
        case 17:contact->busphone = allocField( dir, strField ); break;
        case 18:contact->homephone = allocField( dir, strField ); break;
        case 19:contact->mobilephone = allocField( dir, strField ); break;
        case 20:contact->otherphone = allocField( dir, strField ); break;
        case 21:contact->aimscreen = allocField( dir, strField ); break;
        case 22:contact->msnscreen = allocField( dir, strField ); break;
        case 23:contact->yahooscreen = allocField( dir, strField ); break;
        case 24:contact->user1 = allocField( dir, strField ); break;
        case 25:contact->user2 = allocField( dir, strField ); break;
        case 26:contact->profileid = allocField( dir, strField ); break;
        default:
            break;
        }

    }
    // Finalize the record
    contact->type = UserTypeContact;

    return contact;
}

ContactDisplayManager::ContactDisplayManager()
{
    /*
        Get the user selected display columns from UserPreferences.
    */
    m_pDisplayList = &PREFERENCES.m_DisplayedContactColumns;

    InitializeDisplayList();

    /*
        If the user has specified some display columns
        then mark the corresponding MasterAvailable entries as false.

        If there are no user specifed display columns then create a default set.
    */
    if( m_pDisplayList->GetCount() )
    {
        wxLogDebug(wxT("ContactDisplayManager::ContactDisplayManager() - Using defined columns"));
        for( size_t i = 0; i < MaxContactFieldIndex; ++i )
        {
            m_MasterAvailable[i] = 1;
        }
        for( size_t i = 0; i < m_pDisplayList->GetCount(); ++i )
        {
            m_MasterAvailable[ m_pDisplayList->Item(i) ] = 0;
        }
    }else
    {
        wxLogDebug(wxT("ContactDisplayManager::ContactDisplayManager() - Using Default Columns"));
        SetDefaultValues();
    }
}

pstring ContactTreeListData::allocField( directory_t dir, const wxString & strField )
{
    if( !strField.IsEmpty() )
    {
        return directory_string( dir, strField.mb_str( wxConvUTF8 ) );
    }else
    {
        wxString blank(wxT(""));
        return directory_string( dir, blank.mb_str() );
    }
}

void ContactDisplayManager::InitializeDisplayList()
{
    m_MasterList.Clear();
    m_MasterAvailable.Clear();
    m_MasterDisplayNames.Clear();

    //NOTE: The AddField calls set the MasterAvailable flags to true and the MasterColumnWidth
    //entries to -1 (Default).
    //The initial columns widths could also be passed as the third parameter in AddField().

    AddField( Contact_AddressBook,      _("Address book") );
    AddField( Contact_Title,            _("Title") );
    AddField( Contact_First,            _("First") );
    AddField( Contact_Middle,           _("Middle") );
    AddField( Contact_Last,             _("Last") );
    AddField( Contact_Suffix,           _("Suffix") );
    AddField( Contact_Company,          _("Company") );
    AddField( Contact_JobTitle,         _("Job title") );
    AddField( Contact_Address1,         _("Address 1") );
    AddField( Contact_Address2,         _("Address 2") );
    AddField( Contact_State,            _("State") );
    AddField( Contact_Country,          _("Country") );
    AddField( Contact_PostalCode,       _("Postal code") );
    AddField( Contact_BusinessPhone,    _("Business phone") );
    AddField( Contact_HomePhone,        _("Home phone") );
    AddField( Contact_OtherPhone,       _("Other phone") );
    AddField( Contact_Email1,           _("Email") );
    AddField( Contact_Email2,           _("Email 2") );
    AddField( Contact_Email3,           _("Email 3") );
    AddField( Contact_MSNScreen,        _("MSM screen") );
    AddField( Contact_YIMScreen,        _("YIM screen") );
    AddField( Contact_AIMScreen,        _("AIM screen") );
    AddField( Contact_ScreenName,       _("Screen") );
}

void ContactDisplayManager::SetDefaultValues()
{
    if( m_pDisplayList->GetCount() == 0 )
    {
        AddToDisplayed( Contact_ScreenName );
        AddToDisplayed( Contact_BusinessPhone );
    }
}

void ContactDisplayManager::AddField( size_t ndx, const wxString & strDisplayName, int nWidth )
{
    m_MasterList.Add( ndx );

    m_MasterDisplayNames.Add( strDisplayName );

    m_MasterAvailable.Add(1);

    m_MasterColumnWidths.Add(nWidth);
}

void ContactDisplayManager::PopulateAvailableListBox( wxListBox *listbox )
{
    listbox->Clear();
    for( size_t i = 0; i < m_MasterAvailable.GetCount(); ++i )
    {
        if( m_MasterAvailable[i] )
        {
            listbox->Append( m_MasterDisplayNames[ i ] );
        }
    }
}

void ContactDisplayManager::PopulateDisplayListBox( wxListBox *listbox )
{
   listbox->Clear();
   for( size_t i = 0; i < m_pDisplayList->GetCount(); ++i )
   {
       listbox->Append( m_MasterDisplayNames[ m_pDisplayList->Item(i) ] );
   }
}

bool ContactDisplayManager::AddToDisplayed( size_t ndx )
{
    if( ndx >= MaxContactFieldIndex )
    {
        return false;
    }

    for( size_t i = 0; i < m_pDisplayList->GetCount(); ++i )
    {
        if( m_pDisplayList->Item(i) == (int)ndx )
        {
            return false; // ndx is already in the list.
        }
    }

    m_MasterAvailable[ndx] = 0;

    m_pDisplayList->Add( ndx );

    wxLogDebug(wxT("Moving ndx=%d |%s| to displayed."),ndx,m_MasterDisplayNames[ndx].c_str() );

    return true;
}

bool ContactDisplayManager::RemoveFromDisplayed( size_t ndx )
{
    if( ndx >= MaxContactFieldIndex )
    {
        return false;
    }

    bool fFound = false;
    size_t iFound = 0;
    for( size_t i = 0; i < m_pDisplayList->GetCount(); ++i )
    {
        if( m_pDisplayList->Item(i) == (int)ndx )
        {
            fFound = true;
            iFound = i;
            break;
        }
    }

    if( !fFound )
        return false;

    // Remove Column[ndx] from the display list
    m_pDisplayList->RemoveAt( iFound );

    // Mark Column[ndx] as available
    m_MasterAvailable[ndx] = 1;

    return true;
}

bool ContactDisplayManager::ProcessMoveFromDisplayed( wxListBox *displayedListbox, wxListBox *availableListbox )
{
    int nMoved = 0;
    wxArrayInt listboxSelection;
    wxArrayInt ndxSelection;
    size_t nSel = displayedListbox->GetSelections( listboxSelection );
    if( nSel == 0 )
        return false; // Nothing was selected.

    if( nSel > m_pDisplayList->GetCount() )
    {
        return false; // This should never happen.
    }

    for( size_t i = 0; i < nSel; ++i )
    {
        ndxSelection.Add( m_pDisplayList->Item( listboxSelection[i] ) );
    }

    for( size_t i = 0; i < nSel; ++i )
    {
        RemoveFromDisplayed( ndxSelection[i] );
        ++nMoved;
    }

    if( nMoved )
    {
        PopulateAvailableListBox( availableListbox );
        PopulateDisplayListBox( displayedListbox );
    }

    return true;
}

bool ContactDisplayManager::ProcessMoveToDisplayed( wxListBox *availableListbox, wxListBox *displayedListbox )
{
    int nMoved = 0;
    wxArrayInt listboxSelection;
    wxArrayInt ndxSelection;
    size_t nSel = availableListbox->GetSelections( listboxSelection );
    if( nSel == 0 )
        return false; // Nothing was selected.

    // Build an array of index values
    for( size_t i = 0; i < nSel; ++i )
    {
        ndxSelection.Add( AvailableToMasterIndex( listboxSelection[i] ) );
    }

    for( size_t i = 0; i < nSel; ++i )
    {
        AddToDisplayed( ndxSelection[i] );
        ++nMoved;
    }

    if( nMoved )
    {
        PopulateAvailableListBox( availableListbox );
        PopulateDisplayListBox( displayedListbox );
    }

    return true;
}

size_t ContactDisplayManager::DisplayToMasterIndex( size_t iSel )
{
    size_t ndx = m_pDisplayList->Item( iSel );
    return ndx;
}

size_t ContactDisplayManager::AvailableToMasterIndex( size_t iSel )
{
    size_t ndx = 0;

    wxArrayInt availableIndex;
    for( size_t j = 0; j < MaxContactFieldIndex; ++j )
    {
        if( m_MasterAvailable[j] )
        {
            availableIndex.Add(j);
        }
    }

    ndx = availableIndex[iSel];

    return ndx;
}

bool ContactDisplayManager::ProcessMove( wxListBox * displayListbox, bool fMoveUp )
{
    wxArrayInt listboxSelection;
    size_t nSel = displayListbox->GetSelections( listboxSelection );
    size_t last = nSel - 1;

    if( nSel == 0 )
        return false;

    if( fMoveUp && listboxSelection[0] == 0 )
        return false;

    if( !fMoveUp && listboxSelection[nSel - 1] == (int)(m_pDisplayList->GetCount() - 1) )
        return false;

    if( fMoveUp )
    {
        int displaced_ndx = m_pDisplayList->Item( listboxSelection[0] - 1 );
        m_pDisplayList->RemoveAt( listboxSelection[0] - 1, 1 );
        m_pDisplayList->Insert( displaced_ndx, listboxSelection[last], 1 );
        PopulateDisplayListBox( displayListbox );
        for( size_t i = 0; i < nSel; ++i )
        {
            displayListbox->Select( listboxSelection[i] - 1 );
        }
    }else
    {
        int displaced_ndx = m_pDisplayList->Item( listboxSelection[last] + 1 );
        m_pDisplayList->RemoveAt( listboxSelection[last] + 1 );
        m_pDisplayList->Insert( displaced_ndx, listboxSelection[0], 1 );
        PopulateDisplayListBox( displayListbox );
        for( size_t i = 0; i < nSel; ++i )
        {
            displayListbox->Select( listboxSelection[i] + 1 );
        }
    }

    return true;
}

bool ContactDisplayManager::UpdateColumnHeadings( ContactTreeList & treelist )
{
    if( !treelist.Tree() )
        return false;

    size_t nColumns = treelist.Tree()->GetColumnCount();
    size_t nDisplayed = m_pDisplayList->GetCount();
    size_t iDisplay = 0;
    size_t iCol = 1;

    // Make sure the Column zero (Name) entry exists.
    if( nColumns == 0 )
    {
        treelist.Tree()->AddColumn( _("Name"), 220 );
        nColumns = 1;
    }

    // First use the existing columns
    while( (iCol < nColumns) && (iDisplay < nDisplayed) )
    {
        treelist.Tree()->SetColumnText( iCol, m_MasterDisplayNames.Item( m_pDisplayList->Item( iDisplay ) ) );
        ++iCol;
        ++iDisplay;
    }

    // No add any new columns required
    while( iDisplay < nDisplayed )
    {
        treelist.Tree()->AddColumn( m_MasterDisplayNames.Item( m_pDisplayList->Item(iDisplay) ) );
        ++iDisplay;
    }

    // Cull any left over columns
    while( (nColumns - 1) > nDisplayed )
    {
        treelist.Tree()->RemoveColumn( nColumns - 1 );
        --nColumns;
    }

    return true;
}

bool ContactDisplayManager::UpdateColumnDisplay( ContactTreeList & treelist, wxTreeItemId & item )
{
    if( !treelist.Tree() )
        return false;
    if( !item.IsOk() )
        return false;

    size_t iCol = 1;
    size_t iDisplay = 0;
    size_t nDisplayed = m_pDisplayList->GetCount();

    int nNodeType = treelist.GetNodeType( item );

    if( nNodeType == ContactTreeList::Node_Group )
    {
        GroupTreeItemData *pGData = (GroupTreeItemData *)treelist.Tree()->GetItemData( item );
        group_t group = pGData->m_group;
        wxString strGroupName( group->name, wxConvUTF8 );

        treelist.Tree()->SetItemText( item, 0, strGroupName );

        return true;
    }

    if( nNodeType != ContactTreeList::Node_Contact )
        return true;

    ContactTreeItemData *pCData = (ContactTreeItemData *)treelist.Tree()->GetItemData( item );
    contact_t contact = pCData->m_contact;

    //PLF-TODO: Construct the Name field according to user selected rules
    wxString first( contact->first, wxConvUTF8 );
    wxString last( contact->last, wxConvUTF8 );
    wxString strName;
    strName = first;
    strName += wxT(" ");
    strName += last;
    treelist.Tree()->SetItemText( item, 0, strName );

    while( iDisplay < nDisplayed )
    {
        wxString strValue;

        switch( m_pDisplayList->Item( iDisplay++ ) )
        {
        case Contact_AddressBook:   strValue = (contact->directory->type == dtGlobal) ? _("Community"):_("Personal"); break;
        case Contact_Title:         strValue = wxString( contact->title, wxConvUTF8 ); break;
        case Contact_First:         strValue = wxString( contact->first, wxConvUTF8 ); break;
        case Contact_Middle:        strValue = wxString( contact->middle, wxConvUTF8 ); break;
        case Contact_Last:          strValue = wxString( contact->last, wxConvUTF8 ); break;
        case Contact_Suffix:        strValue = wxString( contact->suffix, wxConvUTF8 ); break;
        case Contact_Company:       strValue = wxString( contact->company, wxConvUTF8 ); break;
        case Contact_JobTitle:      strValue = wxString( contact->jobtitle, wxConvUTF8 ); break;
        case Contact_Address1:      strValue = wxString( contact->address1, wxConvUTF8 ); break;
        case Contact_Address2:      strValue = wxString( contact->address2, wxConvUTF8 ); break;
        case Contact_State:         strValue = wxString( contact->state, wxConvUTF8 ); break;
        case Contact_Country:       strValue = wxString( contact->country, wxConvUTF8 ); break;
        case Contact_PostalCode:    strValue = wxString( contact->postalcode, wxConvUTF8 ); break;
        case Contact_BusinessPhone: strValue = wxString( contact->busphone, wxConvUTF8 ); break;
        case Contact_HomePhone:     strValue = wxString( contact->homephone, wxConvUTF8 ); break;
        case Contact_OtherPhone:    strValue = wxString( contact->otherphone, wxConvUTF8 ); break;
        case Contact_Email1:        strValue = wxString( contact->email, wxConvUTF8 ); break;
        case Contact_Email2:        strValue = wxString( contact->email2, wxConvUTF8 ); break;
        case Contact_Email3:        strValue = wxString( contact->email3, wxConvUTF8 ); break;
        case Contact_MSNScreen:     strValue = wxString( contact->msnscreen, wxConvUTF8 ); break;
        case Contact_YIMScreen:     strValue = wxString( contact->yahooscreen, wxConvUTF8 ); break;
        case Contact_AIMScreen:     strValue = wxString( contact->aimscreen, wxConvUTF8 ); break;
        case Contact_ScreenName:    strValue = wxString( contact->screenname, wxConvUTF8 ); CUtils::DecodeScreenName(strValue); break;
        default:
            strValue = wxT("");
            break;
        }

        treelist.Tree()->SetItemText( item, iCol++, strValue );
    }

    return true;
}

void ContactTreeList::OnItemCollapsed(wxTreeEvent& event)
{
    wxTreeItemId selId;
    selId = event.GetItem();

    wxString strNodeKey;
    strNodeKey = GetNodeKeyName( selId );
    if( !strNodeKey.IsEmpty() )
    {
        wxCharBuffer key = strNodeKey.mb_str();

        OPTIONS.SetUserBool( key, false );
    }
}

void ContactTreeList::OnItemExpanded(wxTreeEvent& event)
{
    wxTreeItemId selId;
    selId = event.GetItem();

    wxString strNodeKey;
    strNodeKey = GetNodeKeyName( selId );
    if( !strNodeKey.IsEmpty() )
    {
        wxCharBuffer key = strNodeKey.mb_str();

        OPTIONS.SetUserBool( key, true );
    }
}

wxString ContactTreeList::GetNodeKeyName( wxTreeItemId & item )
{
    wxString strNodeId(wxT(""));
    wxString strKeyName(wxT(""));

    if( GetNodeType( item ) == Node_GlobalDir )
    {
        strNodeId = wxT("CAB");
    }
    else if( GetNodeType( item ) == Node_PersonalDir )
    {
        strNodeId = wxT("PAB");
    }
    else if( GetNodeType( item ) == Node_Group )
    {
        GroupTreeItemData *pG = (GroupTreeItemData *)m_pTree->GetItemData( item );
        strNodeId = wxString( pG->m_group->group_id, wxConvUTF8 );
    }
    else if( GetNodeType( item ) == Node_UserDir )
    {
        strNodeId = wxT("USER_") + m_pTree->GetItemText( item );
    }
    if( !strNodeId.IsEmpty() )
    {
        strKeyName = m_strTreeName;
        strKeyName += wxT("_");
        strKeyName += strNodeId;
    }
    return strKeyName;
}

void ContactTreeList::ExpandNodeIfRequired( wxTreeItemId & item )
{
    wxString strNodeKey;
    strNodeKey = GetNodeKeyName( item );
    if( !strNodeKey.IsEmpty() )
    {
        wxCharBuffer key = strNodeKey.mb_str();

        bool fExpand = OPTIONS.GetUserBool( key, true );
        if( fExpand )
        {

            m_pTree->Expand( item );
        }
    }
}

wxTreeItemId ContactTreeList::FindUserDirectory( const wxString& name )
{
    for( unsigned int i=0; i<m_DirectoryIds.GetCount(); i++ )
    {
        if (m_pTree->GetItemText(m_DirectoryIds[i]) == name)
            return m_DirectoryIds[i];
    }
    return NULL;
}
