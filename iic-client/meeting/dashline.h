/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
// Line.h : main header file for the CDashLine
//
//	The Bresenham function in this file is derived from code from 
//			Jean-Claude Lanz mailto:Jclanz@bluewin.ch
//		and he presumably shares copyright to it
//	Otherwise the copyright belongs to Llew S. Goodstadt 
//		http://www.lg.ndirect.co.uk    mailto:lg@ndirect.co.uk
//		who hereby grants you fair use and distribution rights of the code 
//		in both commercial and non-commercial applications.
//
/////////////////////////////////////////////////////////////////////////////
// CDashLine

#if !defined _DASHLINE_H_FILE_
#define _DASHLINE_H_FILE_

#include <list>

#ifndef __WXMSW__

#define BYTE        unsigned char
#define PS_SOLID    0
#define PS_DASH     1
#define PS_DOT      2
#define PS_DASHDOT  3
#define PS_NULL     5

#define PT_MOVETO   1
#define PT_LINETO   2

#define DT_LEFT     0

typedef unsigned int UINT;

#endif

#ifndef MIN
  #define MIN(x, y) ((x) < (y)) ? (x) : (y)
#endif
#ifndef MAX
  #define MAX(x, y) ((x) > (y)) ? (x) : (y)
#endif


typedef	std::list<wxPoint> POINTLIST;
typedef	std::list<BYTE> BYTELIST;


class CDashLine
{
public:
	CDashLine( wxDC& dc, unsigned* pattern, unsigned count);
	~CDashLine();
	void SetPattern(unsigned* pattern, unsigned count);
	enum {	
		DL_SOLID	= PS_SOLID, 
		DL_DASH		= PS_DASH, 
		DL_DOT		= PS_DOT, 
		DL_DASHDOT	= PS_DASHDOT, 
		DL_NULL		= PS_NULL,
		DL_DASHDOTDOT, 
		DL_DASHDOTDOTDOT, 
		DL_DASH_GAP, 
		DL_DOT_GAP, 
		DL_DASHDOT_GAP, 
		DL_DASHDOTDOT_GAP, 
		DL_DASHDOTDOTDOT_GAP}; 
// Returns count of elements (dash/dot and gaps)
// You must be careful to pass in enough memory for pattern
// It is probably safest to always have an array of [8]
	static unsigned GetPattern(unsigned* pattern, bool round, unsigned pensize, unsigned style);

protected:
    wxDC&           m_DC;
	unsigned int	m_CurPat;
	unsigned int	m_Count;
	unsigned int	m_CurStretch;
	unsigned int*	m_Pattern;
    wxPoint			m_CurPos;       //  Current position
    wxPoint			m_PriPos;       //  Prior position

	void Reset();
	void Bresenham( long x, long y, POINTLIST& pathPoints, BYTELIST& pathTypes );

public:
	void BezierTo( wxPoint* dest, POINTLIST& pathPoints, BYTELIST& pathTypes );
//	void MoveTo(const POINT& p) {MoveTo(p.x, p.y);}
	void MoveTo(int x, int y);
//	void LineTo(const POINT& p) {LineTo(p.x, p.y);}
    void LineTo(int x, int y, POINTLIST& pathPoints, BYTELIST& pathTypes );
};

#endif      //  #if !defined _DASHLINE_H_FILE_
