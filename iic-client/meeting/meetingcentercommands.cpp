/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 *
 *
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */
#include "app.h"
#include "meetingcentercontroller.h"
#include "meetingmainframe.h"
#include "meetingsetupdialog.h"
#include "chatmainframe.h"
#include "pcphonecontroller.h"

void MeetingCenterController::ExecutePendingCommands(bool wait)
{
    wxLogDebug( wxT("MeetingCenterController::ExecutePendingCommands()") );

    wxEvtHandler *e = GetEventHandler();
    if( e )
    {
#ifndef __WXGTK__
        if (!wait)
        {
            wxCommandEvent event( wxEVT_EXECUTE_PENDING_COMMANDS, Tools_ExecutePendingCommands );
            e->AddPendingEvent( event );
        }
        else if (!m_PendingTimer.IsRunning())
        {
            m_PendingTimer.Start(200, wxTIMER_ONE_SHOT);
        }
#else
        // Long story short, on GTK pending events are handled from an "idle" handler.  While in the
        // idle handler, further idle processing is blocked so no more pending events can be processed.
        // Timer events are sent from the main GTK loop and avoid this problem.
        if (!m_PendingTimer.IsRunning())
        {
            m_PendingTimer.Start(200, wxTIMER_ONE_SHOT);
        }
#endif
    }
}

void MeetingCenterController::OnExecutePendingCommands( wxCommandEvent& event )
{
    wxLogDebug( wxT("MeetingCenterController::OnExecutePendingCommands") );

    if( m_fCheckVersionCompleted )
        Internal_ExecutePendingCommands( );
    else if (!m_PendingTimer.IsRunning())
    {
        m_PendingTimer.Start( 400, wxTIMER_ONE_SHOT);
    }
}

void MeetingCenterController::OnExecutePendingTimer( wxTimerEvent& event )
{
    wxLogDebug( wxT("MeetingCenterController::OnExecutePendingTimer") );

    if( m_fCheckVersionCompleted )
        Internal_ExecutePendingCommands( );
    else if (!m_PendingTimer.IsRunning( ))
    {
        m_PendingTimer.Start( 400, wxTIMER_ONE_SHOT );
    }
}

void MeetingCenterController::Internal_ExecutePendingCommands()
{
    wxLogDebug( wxT("MeetingCenterController::Internal_ExecutePendingCommands( Count=%d )"),Commands().GetList().GetCount() );

    CommandQueueElement *pCmd = Commands().GetNextCommand();
    bool fCommandHandled = false;
    bool fNeedAdditionalInput = false;
    while( pCmd && !fNeedAdditionalInput )
    {
        fCommandHandled = Internal_ExecuteCommand( pCmd, fNeedAdditionalInput );

        ///////////////////////////////////////////////////////////////////////////
        if( !fCommandHandled )
        {
            wxLogError( wxT("WARNING: Unknown Command |%s| - Not handled."), pCmd->GetTaskName().c_str() );
        }
        else
        {
            // SPECIAL CASE: The comman cannot execute now because additional input is
            // required. We need to stop and wait until later.
            if( fNeedAdditionalInput )
            {
                // Put the command back into the queue
                Commands().AppendCommand( pCmd );
                break; // Leave for now.
            }
        }

        // Done with the memory now
        delete pCmd;

        ///////////////////////////////////////////////////////////////////////////
        pCmd = Commands().GetNextCommand();
    }

}

bool MeetingCenterController::Internal_ExecuteCommand( CommandQueueElement *pCmd, bool &fWaitingForInput  )
{
    bool fCommandHandled = false;
    fWaitingForInput = false;

    wxLogDebug( wxT("MeetingCenterController: Exec command %s"), pCmd->GetTaskName().c_str() );

/* Example command handler
    if( pCmd->IsTaskName( wxT("iic-command" ) ) )
    {
        // If a command requires the instant meeting ID, for example:
        // if( (m_nCommandState & State_HaveInstantMID) == 0 )
        // {
        //     fWaitingForInput = true;
        // }
           fCommandHandled = true;
    }
*/

    //  Command:     iic:launch
    //  Parameters:  pid,  pid_title,  mid,  mid_title
    if( pCmd->IsTaskName(wxT("iic:launch")) )
    {
        wxString meetingId = pCmd->GetParam(2);

        if (IsMeetingActive())
        {
            // TODO: ask user if they want to switch meetings.
            wxMessageBox(_("You may only participate in one meeting at a time.\nPlease exit the current meeting if you wish to start or join a new meeting"), wxT("Meeting Center"), wxOK | wxICON_EXCLAMATION);
            return true;
        }

        if (!IsMeetingActive(meetingId))
        {
            //  REMINDER: We don't own the MeetingMainframe memory
            MeetingMainFrame *pMeetingMainFrame = new MeetingMainFrame( 0L );

            // Tell the server to join the meeting.
            wxString    password( wxT("") );
            if( pCmd->GetParamCount() == 5 )
            {
                password = pCmd->GetParam(4);
            }
            controller_meeting_join( controller,
                                     pCmd->GetParam(2).mb_str(), // Meeting ID
                                     pCmd->GetParam(0).mb_str(), // Part ID
                                     password.mb_str() );
            pMeetingMainFrame->StartMeeting( pCmd->GetParam(0),
                                             pCmd->GetParam(1),
                                             pCmd->GetParam(2),
                                             pCmd->GetParam(3),
                                             password,
                                             true );
            pMeetingMainFrame->Show();

            wxGetApp( ).SetTopWindow( pMeetingMainFrame );

            if (!IsVisible())
            {
                pMeetingMainFrame->CloseAppOnMeetingEnd( );
                wxGetApp( ).SetExitOnFrameDelete( true );
            }
        }
        fCommandHandled = true;
    }

    //  Command:     start-meeting
    //  Parameters:  pid,  pid_title,  mid,  mid_title
    if( pCmd->IsTaskName(wxT("start-meeting")) )
    {
        // If contact information is not available yet, then wait till later.
        if( !ContactInformationDownloaded() )
        {
            wxLogDebug( wxT("  directory info has not yet arrived - deferring") );
            fWaitingForInput = true;
            return true;
        }

        wxString meetingId = pCmd->GetParam(2);

        if (!IsMeetingActive(meetingId))
        {
            MeetingSetupDialog Dlg( m_fHaveGUI ? this:0, wxID_ANY );
            Dlg.SetupForStartMeeting();
            int nResp = Dlg.EditMeeting( meetingId, false );
            if( nResp == wxID_OK )
            {
                //  REMINDER: We don't own the MeetingMainframe memory
                MeetingMainFrame *pMeetingMainFrame = new MeetingMainFrame( 0L );

                //  Get the meeting definition from the MeetingSetupDialog
                meeting_details_t   details = Dlg.GetMeetingDetails( );

                // Tell the server to create the meeting.
                controller_meeting_create( controller, details );

                wxString    password( wxT("") );
                if( pCmd->GetParamCount() == 5 )
                {
                    password = pCmd->GetParam(4);
                }
                pMeetingMainFrame->StartMeeting( pCmd->GetParam(0),
                                                 pCmd->GetParam(1),
                                                 pCmd->GetParam(2),
                                                 pCmd->GetParam(3),
                                                 password );
                pMeetingMainFrame->Show();

                if (!Dlg.GetProjectCode().IsEmpty())
                {
                    wxString info = wxT("project: ") + Dlg.GetProjectCode();
                    controller_log_event(CONTROLLER.controller, meetingId.mb_str(), info.mb_str(wxConvUTF8));
                }
            }
        }
        else
        {
            ClearWaitingForSetup();
        }
        fCommandHandled = true;
    }

    //  Command:     start-im
    //  Parameters:  screen, server, body
    if( pCmd->IsTaskName(wxT("start-im")) )
    {
        // If contact information is not available yet, then wait till later.
        if( !ContactInformationDownloaded() )
        {
            wxLogDebug( wxT("  directory info has not yet arrived - deferring") );
            fWaitingForInput = true;
            return true;
        }

        if( m_pChatMainFrame == NULL )
        {
            m_pChatMainFrame = new ChatMainFrame(this, &m_pChatMainFrame);
            m_pChatMainFrame->SetDisplayName(m_displayName);
        }

        if( m_pChatMainFrame != NULL )
        {
            if (pCmd->GetParam(2).IsEmpty())
                m_pChatMainFrame->StartIM(pCmd->GetParam(0));
            else
                m_pChatMainFrame->IncomingIM(pCmd->GetParam(0), pCmd->GetParam(2));

            m_pChatMainFrame->Show();
        }
        fCommandHandled = true;
    }

    //  Command:     join-meeting-by-pin
    //  Parameters:  mid From the sign on screen or user entered data.
    if( pCmd->IsTaskName(wxT("join-meeting-by-pin")) )
    {
        wxString strPin = pCmd->GetParam(0);
        wxString msg;
        wxString caption;
        caption = _("Meeting Center");

        if (controller_lookup_invite( controller, strPin.mb_str() ))
        {
            msg = _("The server was temporarily unable to start this meeting. Please try again later.");
            wxMessageDialog Dlg(this, msg, caption, wxOK | wxICON_ERROR);
            Dlg.ShowModal();
        }

        fCommandHandled = true;
    }

    //  Command:     join-meeting
    //  Parameters:  pid,  pid_title,  mid,  mid_title, password
    if( pCmd->IsTaskName(wxT("join-meeting")) )
    {
        wxString meetingId = pCmd->GetParam(2);

        // NOTE: We don't own the MeetingMainframe memory
        // Parameters: pid, pid_title, mid, mid_title [,password]
        if (!IsMeetingActive(meetingId))
        {
            MeetingMainFrame *pMeetingMainFrame = new MeetingMainFrame( 0L );

            wxString password( wxT("") );
            bool fJoining = false;

            if( pCmd->GetParamCount() == 5 )
            {
                password = pCmd->GetParam(4);
            }

            // Tell the server we want to join the meeting.
            controller_meeting_join( controller,
                                     pCmd->GetParam(2).mb_str(), // Meeting ID
                                     pCmd->GetParam(0).mb_str(), // Part ID
                                     password.mb_str() );

            pMeetingMainFrame->StartMeeting( pCmd->GetParam(0),
                                             pCmd->GetParam(1),
                                             pCmd->GetParam(2),
                                             pCmd->GetParam(3),
                                             password,
                                             fJoining           );
            pMeetingMainFrame->Show();

            if ( AnonymousMode() )
            {
                if ( IsVisible() )
                    this->Show( false );
                pMeetingMainFrame->CloseAppOnMeetingEnd( );
                wxGetApp( ).SetTopWindow( pMeetingMainFrame );
                wxGetApp( ).SetExitOnFrameDelete( true );
            }
        }

        fCommandHandled = true;
    }

    //  Command:     connect-pcphone
    //  Parameters:  mid
    if( pCmd->IsTaskName(wxT("connect-pcphone")) )
    {
        wxString meetingId = pCmd->GetParam(0);

        MeetingMainFrame * mtgFrame = FindMeetingWindow(meetingId);
        if (!mtgFrame)
        {
            wxLogDebug( wxT("  meeting not started yet - deferring") );
            fWaitingForInput = true;
            return true;
        }
        
        if (mtgFrame->PhoneController() && !mtgFrame->PhoneController()->IsInCall())
            mtgFrame->PhoneController()->ConnectToMeeting();
        fCommandHandled = true;        
    }

    //  Command:     iic:meetmany
    //  Parameters:  meetingtoken
    //  Original command line:  iic:meetmany?meetingtoken=169
    if( pCmd->IsTaskName(wxT("iic:meetmany")) )
    {
        wxLogDebug( wxT("MeetingCenterController: Exec command %s  -  token is  ***%s***"), pCmd->GetTaskName().c_str(), pCmd->GetParam( 0 ).wc_str( ) );

        // If contact information is not available yet, then wait till later.
        if( !ContactInformationDownloaded() )
        {
            wxLogDebug( wxT("  directory info has not yet arrived - deferring") );
            fWaitingForInput = true;
            return true;
        }

        //  iic:meetmany runs the user's Instant Meeting, so query the controller for that info
        wxString    strInstMID;
        if( sched )
        {
            strInstMID = wxString( schedule_get_my_instant_meeting( sched ), wxConvUTF8 );
            wxLogDebug( wxT("  the instant mtg ID is - ***%s***"), strInstMID.wc_str( ) );
        }

        wxString    meetingId = pCmd->GetParam( 0 );    // Actually meeting token

        MeetingMainFrame *pMeetingMainFrame = FindMeetingWindow( );

        meetingId.Append( _(";") );
        meetingId.Append( strInstMID );
        MeetingSetupDialog Dlg( pMeetingMainFrame ? (wxWindow*)pMeetingMainFrame : (m_fHaveGUI ? this : NULL), wxID_ANY );
        if (pMeetingMainFrame == NULL)
            Dlg.SetupForStartMeeting();
        else
            Dlg.SetupForMeetingInProgress();
        int nResp = Dlg.EditMeeting( meetingId );
        if( nResp == wxID_OK )
        {
            meeting_details_t   details = Dlg.GetMeetingDetails( );
            wxString            strPartID;
            if( details )
                strPartID = wxString( details->host_id, wxConvUTF8 );

            if (details->type != TypeScheduled)
            {
                if (pMeetingMainFrame == NULL)
                {
                    //  Reminder: We don't own the MeetingMainframe memory
                    pMeetingMainFrame = new MeetingMainFrame( 0L );
                }

                //  Find the ParticipantID of the host
                invitee_iterator_t iter;
                invitee_t invt;

                iter = meeting_details_iterate( details );
                wxLogDebug( wxT("  ScreenName  --  Name  --  PartID  --  UserID") );
                while ((invt = meeting_details_next( details, iter)) != NULL)
                {
                    wxString    strUserID( invt->user_id, wxConvUTF8 );
                    wxString    strPID( invt->participant_id, wxConvUTF8 );
                    wxString    strScreen( invt->screenname, wxConvUTF8 );
                    wxString    strName( invt->name, wxConvUTF8 );
                    wxLogDebug( wxT("  %s  --  %s  --  %s  --  %s"), strScreen.wc_str( ), strName.wc_str( ), strPID.wc_str( ), strUserID.wc_str( ) );
                    if( strPartID.Cmp( strUserID )  ==  0 )
                    {
                        wxString    strPID( invt->participant_id, wxConvUTF8 );
                        strPartID = strPID;
                        break;
                    }
                }

                // Tell the server to create the meeting.
                controller_meeting_create( controller, details );

                wxString    password( wxT("") );
                if( pCmd->GetParamCount() == 5 )
                {
                    password = pCmd->GetParam(4);
                }
                pMeetingMainFrame->StartMeeting( strPartID,
                                                 wxString( _("Participant Name") ),
                                                 strInstMID,
                                                 wxString( details->title, wxConvUTF8 ),
                                                 password );
                pMeetingMainFrame->Show( );

                pMeetingMainFrame->CloseAppOnMeetingEnd( );
                wxGetApp( ).SetTopWindow( pMeetingMainFrame );
                wxGetApp( ).SetExitOnFrameDelete( true );
            }
            else
            {
                RefreshMeetingsList();
            }
        }
        fCommandHandled = true;
    }

    //  Command:     iic:meetone
    //  Parameters:  screenName
    //  Original command line:  iic:meetone?screenName=miket
    if( pCmd->IsTaskName(wxT("iic:meetone")) || pCmd->IsTaskName(wxT("start-instant")) )
    {
        // If contact information is not available yet, then wait till later.
        if( !ContactInformationDownloaded() )
        {
            wxLogDebug( wxT("  directory info has not yet arrived - deferring") );
            fWaitingForInput = true;
            return true;
        }

        wxString    strInstMID;
        if( sched )
        {
            strInstMID = wxString( schedule_get_my_instant_meeting( sched ), wxConvUTF8 );
            //wxLogDebug( wxT("  the instant mtg ID is - %s"), strInstMID.wc_str( ) );
        }

        wxString    meetingId = pCmd->GetParam( 0 ); // Actually screen name

        if (!IsMeetingActive(strInstMID))
        {
            if (!meetingId.IsEmpty())
                meetingId.Append( _(";") );
            meetingId.Append( strInstMID );
            MeetingSetupDialog Dlg( m_fHaveGUI ? this:0, wxID_ANY );
            Dlg.SetupForStartMeeting();
            int nResp = Dlg.EditMeeting( meetingId );
            if( nResp == wxID_OK )
            {
                //  Reminder: We don't own the MeetingMainframe memory
                MeetingMainFrame *pMeetingMainFrame = new MeetingMainFrame( 0L );

                meeting_details_t   details = Dlg.GetMeetingDetails( );
                wxString            strPartID;
                if( details )
                    strPartID = wxString( details->host_id, wxConvUTF8 );

                //  Find the ParticipantID of the host
                invitee_iterator_t iter;
                invitee_t invt;

                iter = meeting_details_iterate( details );
                wxLogDebug( wxT("  ScreenName  --  Name  --  PartID  --  UserID") );
                while ((invt = meeting_details_next( details, iter)) != NULL)
                {
                    wxString    strUserID( invt->user_id, wxConvUTF8 );
                    wxString    strPID( invt->participant_id, wxConvUTF8 );
                    wxString    strScreen( invt->screenname, wxConvUTF8 );
                    wxString    strName( invt->name, wxConvUTF8 );
                    wxLogDebug( wxT("  %s  --  %s  --  %s  --  %s"), strScreen.wc_str( ), strName.wc_str( ), strPID.wc_str( ), strUserID.wc_str( ) );
                    if( strPartID.Cmp( strUserID )  ==  0 )
                    {
                        wxString    strPID( invt->participant_id, wxConvUTF8 );
                        strPartID = strPID;
                        break;
                    }
                }

                // Tell the server to create the meeting.
                controller_meeting_create( controller, details );

                wxString    password( wxT("") );
                if( pCmd->GetParamCount() == 5 )
                {
                    password = pCmd->GetParam(4);
                }
                pMeetingMainFrame->StartMeeting( strPartID,
                                                 wxString( _("Participant Name") ),
                                                 strInstMID,
                                                 wxString( details->title, wxConvUTF8 ),
                                                 password );
                pMeetingMainFrame->Show( );

                if (pCmd->IsTaskName(wxT("iic:meetone")))
                {
                    pMeetingMainFrame->CloseAppOnMeetingEnd( );
                    wxGetApp( ).SetTopWindow( pMeetingMainFrame );
                    wxGetApp( ).SetExitOnFrameDelete( true );
                }
                
                if (!Dlg.GetProjectCode().IsEmpty())
                {
                    wxString info = wxT("project: ") + Dlg.GetProjectCode();
                    controller_log_event(CONTROLLER.controller, meetingId.mb_str(), info.mb_str(wxConvUTF8));
                }
                
            }
        }
        fCommandHandled = true;
    }

    //  Command:     start-invite
    //  Parameters:  userid screen name
    if( pCmd->IsTaskName(wxT("start-invite")) )
    {
        // If contact information is not available yet, then wait till later.
        if( !ContactInformationDownloaded() )
        {
            wxLogDebug( wxT("  directory info has not yet arrived - deferring") );
            fWaitingForInput = true;
            return true;
        }

        wxString    strInstMID;
        if( sched )
        {
            strInstMID = wxString( schedule_get_my_instant_meeting( sched ), wxConvUTF8 );
        }

        MeetingMainFrame *pMeetingMainFrame = FindMeetingWindow( );
        wxString    meetingId = strInstMID;
        wxWindow *parent = (pMeetingMainFrame != NULL) ? (wxWindow*) pMeetingMainFrame : (m_fHaveGUI ? this : NULL);

        MeetingSetupDialog* pDlg = GetMeetingSetup();
        if (pDlg != NULL)
        {
            pDlg->InviteUserId( pCmd->GetParam(0) );
            return true;
        }

        MeetingSetupDialog dlg( parent, wxID_ANY);
        if (!IsMeetingActive(strInstMID))
            dlg.SetupForStartMeeting();
        else
            dlg.SetupForMeetingInProgress();
        dlg.InviteUserId( pCmd->GetParam(0) );

        int nResp = dlg.EditMeeting( meetingId );
        if( nResp == wxID_OK )
        {
            if (pMeetingMainFrame == NULL)
            {
                //  Reminder: We don't own the MeetingMainframe memory
                pMeetingMainFrame = new MeetingMainFrame( 0L );
            }

            meeting_details_t   details = dlg.GetMeetingDetails( );
            wxString            strPartID;

            //  Find the ParticipantID of the host
            invitee_t invt = meeting_details_find_host(details);
            if (invt)
                strPartID = wxString(invt->participant_id, wxConvUTF8);

            // Tell the server to create the meeting.
            controller_meeting_create( controller, details );

            wxString    password( wxT("") );
            if( pCmd->GetParamCount() == 5 )
            {
                password = pCmd->GetParam(4);
            }
            pMeetingMainFrame->StartMeeting( strPartID,
                                             wxString( _("Participant Name") ),
                                             strInstMID,
                                             wxString( details->title, wxConvUTF8 ),
                                             password );
            pMeetingMainFrame->Show( );

            if (!dlg.GetProjectCode().IsEmpty())
            {
                wxString info = wxT("project: ") + dlg.GetProjectCode();
                controller_log_event(CONTROLLER.controller, meetingId.mb_str(), info.mb_str(wxConvUTF8));
            }
        }

        fCommandHandled = true;
    }

    //  Command:     schedule-invite
    //  Parameters:  userid screen name
    if( pCmd->IsTaskName(wxT("schedule-invite")) )
    {
        // If contact information is not available yet, then wait till later.
        if( !ContactInformationDownloaded() )
        {
            wxLogDebug( wxT("  directory info has not yet arrived - deferring") );
            fWaitingForInput = true;
            return true;
        }

        MeetingSetupDialog* pDlg = GetMeetingSetup();
        if (pDlg != NULL)
        {
            pDlg->InviteUserId( pCmd->GetParam(0) );
            return true;
        }

        MeetingSetupDialog dlg( (m_fHaveGUI ? this:0), wxID_ANY);
        dlg.InviteUserId( pCmd->GetParam(0) );
        int nResp = dlg.NewMeeting( );
        if( nResp == wxID_OK )
        {
        }

        fCommandHandled = true;
    }

    //  Command:     iic:start
    //  Parameters:  meetingID
    //  Original command line:  iic:start?meetingid=111
    if( pCmd->IsTaskName(wxT("iic:start")) )
    {
        wxLogDebug( wxT("MeetingCenterController: Exec command %s"), pCmd->GetTaskName().c_str() );

        wxString meetingId = pCmd->GetParam(0);

        if (!IsMeetingActive(meetingId))
        {
            // NOTE: We don't own the MeetingMainframe memory
            MeetingMainFrame *pMeetingMainFrame = new MeetingMainFrame( 0L );

            // Tell the server to start the meeting.
            controller_meeting_start(controller, pCmd->GetParam(0).mb_str(wxConvUTF8));  //  Yes, this should really be "start" rather than "create"

            pMeetingMainFrame->Show();
            fCommandHandled = true;
        }
    }

    if ( pCmd->IsTaskName(wxT("manage-contacts")) )
    {
        // If contact information is not available yet, then wait till later.
        /*if( !ContactInformationDownloaded() )
        {
            wxLogDebug( wxT("  directory info has not yet arrived - deferring") );
            fWaitingForInput = true;
            return true;
        }*/

        ContactsManage();
        fCommandHandled = true;
    }

    //  Command:     meeting-cented
    if( pCmd->IsTaskName(wxT("meeting-center")) )
    {
        ShowMeetingCenterWindow();
        fCommandHandled = true;
    }

    return fCommandHandled;
}

