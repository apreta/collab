/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 *
 *
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */
#include "app.h"

#include <wx/xrc/xmlres.h>

#include "sharecontrol.h"
#include "services/common/common.h"
#include "sharingcontroldialog.h"
#include "selectmonitordialog.h"
#include "meetingmainframe.h"
#include "../sharelib/util_linux.h"

bool ShareControl::s_bShareDesktop = false;         // also used for ShareApplication
bool ShareControl::s_bShareDocument = false;
bool ShareControl::s_bRecordShare = false;

//*****************************************************************************
ShareControl::ShareControl( MeetingMainFrame *pMMF ):
m_pMMF( pMMF )
{
    wxLogDebug( wxT("entering ShareControl CONSTRUCTOR") );
    m_State = ShareNone;
    m_Controller = NULL;
    m_pSharingCtrlDlg = NULL;
    m_bFullScreen = false;
    m_bEnableRecord = false;
    m_bEnableHold = false;
    m_bEnableModerator = false;
    m_bEnableTransparent = true;
    m_bIsControlGranted = false;
}


//*****************************************************************************
ShareControl::~ShareControl()
{
    wxLogDebug( wxT("entering ShareControl DISTRUCTOR") );
    ShareEnd();

    if( m_pSharingCtrlDlg )
        m_pSharingCtrlDlg->Destroy( );
}


//*****************************************************************************
void ShareControl::Initialize(controller_t controller, const wxString& strMeetingId)
{
    wxLogDebug( wxT("entering ShareControl::Initialize( )") );
    m_Controller = controller;
    m_strMeetingId = strMeetingId;
    m_bFullScreen = false;

    m_pSharingCtrlDlg = new SharingControlDialog( m_pMMF, 0 );
    m_pSharingCtrlDlg->SetShareControl( this );

    // Since is top-level and it's parent is the desktop, it will show up in the OS window list.
    // Set a reasonable title and icon.
    m_pSharingCtrlDlg->SetTitle(_("Share Toolbar"));
    m_pSharingCtrlDlg->SetIcon(m_pMMF->GetIcon());
}


//*****************************************************************************
// Connect event handler.
// Note that the callback may be made from a background thread.
// Example usage:
//     Connect(CB_Connecting, ShareControl::SharePresenting, new MemFuncPtr<Class>(this, &Class::memFunc));
void ShareControl::Connect(int event, int shareState, FuncPtr * callback)
{
    wxLogDebug( wxT("entering ShareControl::Connect( shareState = %d, event = %d )"), shareState, event );
    if (event >= 0 && event < CB_Max && shareState >=0 && shareState < ShareMax)
    {
        m_pFuncs[shareState][event] = AutoFuncPtr(callback);
    }
}


//*****************************************************************************
// Disconnect event handler.
void ShareControl::Disconnect(int event, int shareState)
{
    wxLogDebug( wxT("entering ShareControl::Disconnect( )") );
    if (event >= 0 && event < CB_Max)
    {
        m_pFuncs[shareState][event].release();
    }
}


//*****************************************************************************
int ShareControl::SelectDisplay()
{
    wxLogDebug( wxT("entering ShareControl::SelectDisplay( )") );
    std::list<MonitorInfo> monitors = share_get_displays( );
    if ( monitors.size() > 1 )
    {
        SelectMonitorDialog dlg( m_pMMF, monitors );
        if ( dlg.ShowModal( ) != wxID_OK )
        {
            return ShareNoDisplaySelected;
        }
        m_DisplayDevice = dlg.GetDisplay();
        wxLogDebug( wxT("Selected monitor: %s"), m_DisplayDevice.c_str());
    }
    
    return 0;
}


//*****************************************************************************
int ShareControl::ShareDesktop()
{
    wxLogDebug( wxT("entering ShareControl::ShareDesktop( )") );
    if (m_State != ShareNone || s_bShareDesktop)
        return ShareErrBusy;

    appshare_info_t info = controller_get_appshare_info(m_Controller, m_strMeetingId.mb_str(wxConvUTF8));
    if (info == NULL || info->address == NULL)
        return ShareNoServer;

    std::list<MonitorInfo> monitors = share_get_displays( );
    if ( monitors.size() > 1 )
    {
        SelectMonitorDialog dlg( m_pMMF, monitors );
        if (dlg.ShowModal( ) != wxID_OK)
            return ShareNoDisplaySelected;
            
        m_DisplayDevice = dlg.GetDisplay();
    }
    else
    {
        m_DisplayDevice.Clear();
        
        int     dW, dH;
        ::wxDisplaySize( &dW, &dH );
        wxLogDebug( wxT("ShareDesktop: wxDisplaySize( ) returned (%d, %d)"), dW, dH );

        if( dW  >  1024 )
        {
            wxString    strTitle( _("Screen Size Warning") );
            wxString    strMsg( _("Your screen size is currently wider than 1024 pixels.\nMeeting participants with smaller screen sizes may need to scroll back and forth to view your data share.\n") );
    #ifdef __WXMSW__
            strMsg.Append( _("\nTo change your screen size:\nRight click on your computer's Desktop and select Properties from the menu.\nScreen size can be changed on the Settings tab.") );
    #endif
            wxMessageDialog dlg( m_pMMF, strMsg, strTitle, wxOK | wxCANCEL | wxICON_QUESTION );
            if( dlg.ShowModal( )  !=  wxID_OK )
                return ShareUserAborted;
        }
    }

    m_pSharingCtrlDlg->UpdateStatus( CB_Connecting );
    m_pSharingCtrlDlg->ShowToolbar( false );

    m_State = SharePresenting;
    s_bShareDesktop = true;

    if (m_pFuncs[m_State][CB_Connecting].get() != NULL)
        (*(m_pFuncs[m_State][CB_Connecting]))();

    meeting_t mtg = controller_find_meeting(m_Controller, m_strMeetingId.mb_str(wxConvUTF8));
    participant_t part = meeting_find_me(mtg);

    bool    fFlag   = info->key != NULL;

    m_iStartTime = time(NULL);

    share_initialize(wxGetApp().m_strExecDir.mb_str(wxConvUTF8), m_strMeetingId.mb_str(wxConvUTF8), info->address, info->password, info->key, part->part_id, "Z", fFlag, m_iStartTime);
    share_set_event_handler(this, on_appshare_callback);

    share_enable_recording(m_bEnableRecord);
    if (m_pSharingCtrlDlg)
        m_pSharingCtrlDlg->Recording(m_bEnableRecord);

    share_set_options("directx", PREFERENCES.m_fUseDirectX ? "enable" : "disable");
    share_set_options("transparent", m_bEnableTransparent ? "enable" : "disable");
    if (!m_DisplayDevice.IsEmpty())
        share_set_options("display", m_DisplayDevice.mb_str());

    if (share_desktop())
        return ShareNoServer;

    return ShareStarting;
}


//*****************************************************************************
int ShareControl::ShareDocument(bool fPresenter, bool fWhiteboard)
{
    wxLogDebug( wxT("entering ShareControl::ShareDocument( )") );
    if (m_State != ShareNone || s_bShareDocument)
    {
        if (m_State == ShareViewing && !s_bShareDocument)
            ShareEnd();
        else
            return ShareErrBusy;
    }

    m_pSharingCtrlDlg->UpdateStatus( CB_Connecting );   // So toolbar sets presenting flag...
    m_pSharingCtrlDlg->UpdateStatus( CB_Connected );
    m_pSharingCtrlDlg->ShowToolbar( true );

    if (fPresenter)
        m_State = SharePresenting;
    else
        m_State = ShareViewing;
    s_bShareDocument = true;

    if (m_pFuncs[m_State][CB_Connecting].get() != NULL)
        (*(m_pFuncs[m_State][CB_Connecting]))();

    m_iStartTime = time(NULL);

    if (fPresenter)
        controller_docshare_start( m_Controller, m_strMeetingId.mb_str(wxConvUTF8), m_iStartTime );
    
    return 0;
}


//*****************************************************************************
int ShareControl::ShareApplication(std::string &id)
{
    wxLogDebug( wxT("entering ShareControl::ShareApplication( )") );
    if (m_State != ShareNone || s_bShareDesktop)
        return ShareErrBusy;

    appshare_info_t info = controller_get_appshare_info(m_Controller, m_strMeetingId.mb_str(wxConvUTF8));
    if (info == NULL || info->address == NULL)
        return ShareNoServer;

    m_pSharingCtrlDlg->UpdateStatus( CB_Connecting );
    m_pSharingCtrlDlg->ShowToolbar( false );

    m_State = SharePresenting;
    s_bShareDesktop = true;             // actually an app

    if (m_pFuncs[m_State][CB_Connecting].get() != NULL)
        (*(m_pFuncs[m_State][CB_Connecting]))();

    meeting_t mtg = controller_find_meeting(m_Controller, m_strMeetingId.mb_str(wxConvUTF8));
    participant_t part = meeting_find_me(mtg);

    bool    fFlag   = info->key != NULL;

    m_iStartTime = time(NULL);

    share_initialize(wxGetApp().m_strExecDir.mb_str(wxConvUTF8), m_strMeetingId.mb_str(wxConvUTF8), info->address, info->password, info->key, part->part_id, "Q", fFlag, m_iStartTime);
    share_set_event_handler(this, on_appshare_callback);

    share_enable_recording(m_bEnableRecord);
    if (m_pSharingCtrlDlg)
        m_pSharingCtrlDlg->Recording(m_bEnableRecord);

    share_set_options("directx", PREFERENCES.m_fUseDirectX ? "enable" : "disable");
    share_set_options("transparent", m_bEnableTransparent ? "enable" : "disable");
    if (!m_DisplayDevice.IsEmpty())
        share_set_options("display", m_DisplayDevice.mb_str());

    if (share_applications(id,false))
        return ShareNoServer;

    return ShareStarting;
}


//*****************************************************************************
int ShareControl::RecordShare(void * present, void * parent)
{
    appshare_info_t info = controller_get_appshare_info(m_Controller, m_strMeetingId.mb_str(wxConvUTF8));
    if (info == NULL || info->address == NULL)
        return ShareNoServer;

    meeting_t mtg = controller_find_meeting(m_Controller, m_strMeetingId.mb_str(wxConvUTF8));
    participant_t part = meeting_find_me(mtg);

    bool    fFlag   = info->key != NULL;

    wxLogDebug(wxT("Launching share recorder...recording is %s"), m_bEnableRecord ? wxT("on") : wxT("off"));

    share_initialize(wxGetApp().m_strExecDir.mb_str(wxConvUTF8), m_strMeetingId.mb_str(wxConvUTF8), info->address, info->password, info->key, part->part_id, "R", fFlag, m_iStartTime);

    share_enable_recording(m_bEnableRecord);
    
    share_presentation(present, parent, true);

    s_bRecordShare = true;
        
    return 0;
}

//*****************************************************************************
int ShareControl::ShareRefresh()
{
    if (s_bShareDocument)
    {
        wxCommandEvent ev(wxEVT_COMMAND_TOOL_CLICKED, XRCID("ID_SHARECONTROL_REFRESH"));
        if (m_pMMF)
            m_pMMF->AddPendingEvent(ev);
    }
    else if (m_State == ShareViewing || s_bShareDesktop)
        return share_refresh();
        
    return 0;
}

//*****************************************************************************
int ShareControl::ShareEnd()
{
    wxLogDebug( wxT("entering ShareControl::ShareEnd( )") );
    if (m_State == SharePresenting)
    {
        if (s_bShareDocument)
        {
            controller_docshare_end( m_Controller, m_strMeetingId.mb_str(wxConvUTF8) );
            
            if (s_bRecordShare)
            {
                share_end();
                s_bRecordShare = false;
            }
        }
        else
        {
            controller_appshare_end(m_Controller, m_strMeetingId.mb_str(wxConvUTF8));

            share_set_event_handler(NULL, NULL);
            share_end();
        }
    }
    else if (m_State == ShareViewing)
    {
        if (s_bShareDocument)
        {
            ;
        }
        else
        {
            share_set_event_handler(NULL, NULL);
            share_view_end();
        }
    }

    s_bShareDesktop = false;
    s_bShareDocument = false;

    m_State = ShareNone;

    m_pSharingCtrlDlg->HideToolbar( );

    return 0;
}


//*****************************************************************************
int ShareControl::ShareViewer(void * hwnd)
{
    wxLogDebug( wxT("entering ShareControl::ShareViewer( )") );
    if (m_State != ShareNone)
    {
        if (m_State == ShareViewing && s_bShareDocument)
            ShareEnd();
        else
            return ShareErrBusy;
    }

    m_pSharingCtrlDlg->UpdateStatus( CB_Connecting );
    m_pSharingCtrlDlg->ShowToolbar( true );

    m_State = ShareViewing;
    appshare_info_t info = controller_get_appshare_info(m_Controller, m_strMeetingId.mb_str(wxConvUTF8));
    meeting_t mtg = controller_find_meeting(m_Controller, m_strMeetingId.mb_str(wxConvUTF8));
    participant_t part = meeting_find_me(mtg);

    bool    fFlag   = info->key != NULL;

    m_iStartTime = time(NULL);

    share_initialize(wxGetApp().m_strExecDir.mb_str(wxConvUTF8), m_strMeetingId.mb_str(wxConvUTF8), info->address, info->password, info->key, part->part_id, "X", fFlag, m_iStartTime);
    share_set_event_handler(this, on_appshare_callback);

    // Default view is non-fullscreen
    m_bFullScreen = false;
    m_bIsControlGranted = false;

    share_view(hwnd);
    return ShareStarting;
}


//*****************************************************************************
void ShareControl::ViewerFullScreen(bool bEnable)
{
    if (m_State == ShareViewing)
    {
        m_bFullScreen = bEnable;
        share_enable_fullscreen(bEnable);
    }
}


//*****************************************************************************
void ShareControl::ViewerMoved(int x, int y, int cx, int cy)
{
    if (m_State == ShareViewing)
        share_move_viewer(x, y, cx, cy);
}


//*****************************************************************************
void ShareControl::ViewerErase(int x, int y, int cx, int cy)
{
    if (m_State == ShareViewing)
        share_erase_background(x, y, cx, cy);
}

//*****************************************************************************
void ShareControl::ViewerGetImage(wxImage& image)
{
	if (m_State == ShareViewing) 
	{
		int width=0, height=0;
		void *buffer;

		share_get_buffer(buffer, width, height);
		if (width && height)
			image.SetData((unsigned char*) buffer, width, height, true);
	}
}

//*****************************************************************************
void ShareControl::ViewerRequestControl()
{
    wxLogDebug(wxT("Requesting remote control"));
    if (m_State == ShareViewing)
        share_request_control();
}

//*****************************************************************************
void ShareControl::ViewerMouseMove(int x, int y)
{
    if (m_State == ShareViewing)
        share_remote_mouse_move(x, y);
}

//*****************************************************************************
void ShareControl::ViewerMouseButton(int button, bool pressed, int x, int y)
{
    if (m_State == ShareViewing)
        share_remote_button(button, pressed, x, y);    
}

//*****************************************************************************
void ShareControl::ViewerKey(int key, bool pressed)
{
    if (m_State == ShareViewing)
        share_remote_key(key, pressed);
}

//*****************************************************************************
void ShareControl::EnableShareControl(bool flag)
{
    share_enable_control(flag);
}


//*****************************************************************************
void ShareControl::EnableRecord(bool flag)
{
    if (flag != m_bEnableRecord)
    {
        m_bEnableRecord = flag;
        share_enable_recording(flag);
    }
    if (m_pSharingCtrlDlg)
        m_pSharingCtrlDlg->Recording(flag);
}


//*****************************************************************************
void ShareControl::EnableHold(bool flag)
{
    if (flag != m_bEnableHold)
    {
        m_bEnableHold = flag;
    }
    if (m_pSharingCtrlDlg)
        m_pSharingCtrlDlg->Holding(flag);
}


//*****************************************************************************
void ShareControl::EnableModerator(bool flag)
{
    if (flag != m_bEnableModerator)
    {
        m_bEnableModerator = flag;
        if (m_pSharingCtrlDlg)
            m_pSharingCtrlDlg->Moderator(flag);
    }
}


//*****************************************************************************
void ShareControl::EnableTransparent(bool flag)
{
    if (flag != m_bEnableTransparent)
    {
        m_bEnableTransparent = flag;
    }
}

//*****************************************************************************
void ShareControl::SetCommand(int ID, bool flag)
{
    if (m_pSharingCtrlDlg)
        m_pSharingCtrlDlg->SetCommand(ID, flag);
}

//*****************************************************************************
bool ShareControl::IsDesktopShared()
{
    return( (m_State == SharePresenting) && s_bShareDesktop );
}


//*****************************************************************************
bool ShareControl::IsPresenter()
{
    meeting_t mtg = controller_find_meeting(m_Controller, m_strMeetingId.mb_str(wxConvUTF8));
    if( mtg )
    {
        participant_t part = meeting_find_me(mtg);
        return (part->status & StatusPresenter) != 0;
    }
    else
        return false;
}


//*****************************************************************************
bool ShareControl::IsViewing()
{
    return (m_State == ShareViewing);
}


//*****************************************************************************
bool ShareControl::IsFullScreen()
{
    return (m_State == ShareViewing && m_bFullScreen);
}


//*****************************************************************************
bool ShareControl::IsControlGranted()
{
    return (m_State == ShareViewing && m_bIsControlGranted);
}


//*****************************************************************************
void ShareControl::ServerAppShareEnd()
{
    wxLogDebug( wxT("entering ShareControl::ServerAppShareEnd( )") );
    controller_appshare_end(m_Controller, m_strMeetingId.mb_str(wxConvUTF8));
}


//*****************************************************************************
// Events are sent through meeting main frame's event loop so they can
// be dispatched here on the main thread.
void ShareControl::OnCallback(int code)
{
    wxLogDebug( wxT("entering ShareControl::OnCallback( code == %d )"), code );

    if( m_pSharingCtrlDlg )
        m_pSharingCtrlDlg->PostUpdateStatus( code );

    bool handled = share_handle_event(code);

    if (!handled)
    {
        switch (code)
        {
            case CB_Connected:
                if (m_State == SharePresenting)
                    controller_appshare_start(m_Controller, m_strMeetingId.mb_str(wxConvUTF8), NormalScreen, share_get_start_time());
                break;

            case CB_Disconnected:
                break;

            case CB_InternalError:
            case CB_ConnectFailed:
            case CB_SessionEnded:
                if (m_State == SharePresenting)
                {
                    s_bShareDesktop = false;
                    s_bShareDocument = false;
                }
                m_State = ShareNone;
                break;

            case CB_Connecting:
                break;

            case CB_DisplayUpdated:
            case CB_DisplayFullScreen:
            case CB_DisplayNormal:
            case CB_GrantControl:
            case CB_RevokeControl:
				break;                

            default:
                return;
        }
    }

    if (m_pFuncs[m_State][code].get() != NULL)
        (*(m_pFuncs[m_State][code]))();
}


//*****************************************************************************
void ShareControl::on_appshare_callback(void *user, int code)
{
    wxLogDebug( wxT("entering ShareControl::on_appshare_callback( code == %d )"), code );
    ShareControl *contol = (ShareControl*)user;

    // Dispatch event through window so it is handled by main thread.
    contol->m_pMMF->PostShareEvent(code);
}


//*****************************************************************************
void ShareControl::Handup( wxString strName, bool fHandup )
{
    if( m_pSharingCtrlDlg )
    {
        m_pSharingCtrlDlg->Handup( strName, fHandup );
    }
}


//*****************************************************************************
void ShareControl::NewChat( )
{
    if( m_pSharingCtrlDlg )
    {
        m_pSharingCtrlDlg->NewChat( );
    }
}


//*****************************************************************************
wxEvtHandler *ShareControl::GetToolbarHandler()
{
    return m_pSharingCtrlDlg->GetEventHandler();
}
