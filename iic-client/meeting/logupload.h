#ifndef LOGUPLOAD_H
#define LOGUPLOAD_H

#include <wx/zipstrm.h>

#include "webdav.h"
#include "progress1dialog.h"


class LogUpload
{
    friend class UploadThread;
    
    public:
        LogUpload();
        virtual ~LogUpload();
        
        void UploadLogs(wxWindow * parent);
        
        static bool IsAvailable();
        
    protected:
        wxString genTempName();
        void addLogFile(wxZipOutputStream& zip, wxFileName& file);
        wxString CompressLogs();
 
        long makeDir(const wxString& aTargetUrl, const wxString& aTargetPath,
                     wxString& strStatusCode, wxString& strStatusText);
        long sendFile(const wxString& aTargetUrl, const wxString& aTargetPath,
                      const wxString& aFileName, wxString& strStatusCode, wxString& strStatusText);
        void UploadArchive(wxString& file);
        
        void showProgress();
        void hideProgress();
        
    private:
        wxString strUploadURL;
        wxString strLogDir;
        wxString strFileName;
        wxString strUserName;
        wxString strPassword;
        Progress1Dialog *m_pDialogProgress;
        CWebDavUtils m_WebDavUtils;
        wxWindow *m_Parent;
        
        bool inProgress;
        long lastStatus;
        
};


class UploadThread : public wxThread
{
    LogUpload *upload;

public:
    UploadThread(LogUpload * upload)
    {
        this->upload = upload;
    }
    void* Entry();
};


#endif // LOGUPLOAD_H
