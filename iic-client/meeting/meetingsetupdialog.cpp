/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 *
 *
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */
#include "app.h"
#include "meetingcentercontroller.h"
#include "meetingsetupdialog.h"
#include "selectcontactdialog.h"
#include "findcontactsdialog.h"
#include "contactsearch.h"
#include "schedule.h"
#include "meetingconfirmation.h"
#include "contactservice.h"
#include "projectcodedialog.h"
#include <wx/utils.h>
#include <wx/stopwatch.h>
#include <wx/file.h>

//(*InternalHeaders(MeetingSetupDialog)
#include <wx/xrc/xmlres.h>
//*)

#include <set>
#include "createcontactdialog.h"
#include "presencemanager.h"
#include "iicdoc.h"
#include "docshareview.h"
#include "imageuploaddialog.h"
#include "filemanager.h"
using namespace FileManagerNS;

static int kStatusMeetingUnknown = 0;
//static int kStatusMeetingOffline = 1;
static int kStatusMeetingOnline  = 2;

static int kStatusPhoneOffline = 0;
static int kStatusPhoneOnline  = 1;
//static int kStatusPhoneDialing = 2;
//static int kStatusPhoneRinging = 3;
//static int kStatusPhoneBusy    = 4;
//static int kStatusPhoneTalking = 5;
//static int kStatusPhoneMute	   = 6;

static int kStatusContactOffline      = 0;
static int kStatusContactOnline       = 1;
static int kStatusContactAway         = 2;
//static int kStatusContactGoingOffline = 3;
//static int kStatusContactComingOnline = 4;

static wxString kNewInvitee = _("<Type name here>");
static wxString kTempId = wxT("TempUpload");

int idTimer = wxNewId();

int ReverseSortInvitees( invitee_t *i1, invitee_t *i2 )
{
    int iRet = 0;
    if( *i1 < *i2 )
        iRet = 1;
    else if( *i1 > *i2 )
        iRet = -1;
    return iRet;
}


/////////////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_DYNAMIC_CLASS(wxContactCellEditor, GridCellTextEditor)

wxContactCellEditor::wxContactCellEditor()
:GridCellTextEditor(),
m_invt(0),
m_contact(0),
m_fAddAsNewContact(false),
m_nContactSource(ContactFromUnknown)
{
}

wxContactCellEditor::wxContactCellEditor( invitee_t invt, contact_t contact, bool fAddAsNewContact )
:GridCellTextEditor(),
m_invt( invt ),
m_contact( contact ),
m_fAddAsNewContact(fAddAsNewContact),
m_nContactSource(ContactFromUnknown)
{

}

/////////////////////////////////////////////////////////////////////////////////////////

//(*IdInit(MeetingSetupDialog)
//*)

BEGIN_EVENT_TABLE(MeetingSetupDialog,wxDialog)
	//(*EventTable(MeetingSetupDialog)
	//*)

EVT_BUTTON( wxID_HELP, MeetingSetupDialog::OnClickHelp)

END_EVENT_TABLE()

MeetingSetupDialog::MeetingSetupDialog(wxWindow* parent,wxWindowID id):
m_nMode(Mode_Unknown),
m_nSendEmailOption(0),
m_mtg_FROM(0),
m_mtg_TO(0),
m_pContactDlg(0),
m_pContactInviteBtn(0),
m_pFindContactsDlg(0),
m_InviteePopup(0),
m_fCommitOnOK(false),
m_fDataSaved(false),
m_fStartMeetingMode(false),
m_fMeetingNotActive(false),
m_fUploadedDocs(false),
m_FileManager(0),
m_nPendingNewContacts(0),
m_hostContact(0),
m_nState(State_Idle),
m_SearchPopup(0),
m_iStartTimeSelection(0),
m_iEndTimeSelection(0),
m_pDoc(0)
{
    m_strCaption = _("Meeting Setup");

	//(*Initialize(MeetingSetupDialog)
	wxXmlResource::Get()->LoadObject(this,parent,_T("MeetingSetupDialog"),_T("wxDialog"));
	m_VoiceDataRadio = (wxRadioButton*)FindWindow(XRCID("ID_VOICEDATA_RADIO"));
	m_VoiceOnlyRadio = (wxRadioButton*)FindWindow(XRCID("ID_VOICEONLY_RADIO"));
	m_DataShareRadio = (wxRadioButton*)FindWindow(XRCID("ID_DATASHARE_RADIO"));
	m_MeetingTitle = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL_MEETINGTITLE"));
	m_NoMeetingTimeCheck = (wxCheckBox*)FindWindow(XRCID("ID_CHECKBOX_NOMEETINGTIME"));
	m_StartDate = (wxDatePickerCtrl*)FindWindow(XRCID("ID_DATEPICKERCTRL_START"));
	m_StartTime = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL_STARTTIME"));
	m_SpinBtnStart = (wxSpinButton*)FindWindow(XRCID("ID_SPINBTN_START"));
	m_EndDate = (wxDatePickerCtrl*)FindWindow(XRCID("ID_DATEPICKERCTRL_END"));
	m_EndTime = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL_ENDTIME"));
	m_SpinBtnEnd = (wxSpinButton*)FindWindow(XRCID("ID_SPINBTN_END"));
	m_TimeZone = (wxStaticText*)FindWindow(XRCID("ID_TIMEZONETEXT"));
	m_MeetingDescription = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL_MEETINGDESCRIP"));
	m_MeetingInviteMessage = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL_MEETINGMSG"));
	m_InfoPanel = (wxPanel*)FindWindow(XRCID("ID_PANEL2"));
	m_StaticTextSetPhones = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT_SETPHONES"));
	m_ButtonPhoneAll = (wxButton*)FindWindow(XRCID("ID_BUTTON_PHONEALL"));
	m_ButtonPhoneNone = (wxButton*)FindWindow(XRCID("ID_BUTTON_PHONENONE"));
	m_InviteesPanel = (wxPanel*)FindWindow(XRCID("ID_INVITEES_PANEL"));
	m_OneToOneCheck = (wxCheckBox*)FindWindow(XRCID("ID_CHECKBOX_ONETOONE"));
	m_RemoveSelectedBtn = (wxButton*)FindWindow(XRCID("ID_BUTTON_REMOVE_SELECTED"));
	m_InviteNewBtn = (wxButton*)FindWindow(XRCID("ID_BUTTON_INVITE_NEW"));
	m_ShowContactsBtn = (wxButton*)FindWindow(XRCID("ID_SHOWCONTACTS_BTN"));
	m_ParticipantsPanel = (wxPanel*)FindWindow(XRCID("ID_PANEL1"));
	StaticText8 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT8"));
	m_PrivacyPrivateRadio = (wxRadioButton*)FindWindow(XRCID("ID_RADIOBUTTON_PRIVATE"));
	m_PrivacyUnlistedRadio = (wxRadioButton*)FindWindow(XRCID("ID_RADIOBUTTON_UNLISTED"));
	m_PrivacyPublicRadio = (wxRadioButton*)FindWindow(XRCID("ID_RADIOBUTTON_PUBLIC"));
	m_MeetingPasswordText = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL_MEETINGPASSWORD"));
	m_DisplayAllRadio = (wxRadioButton*)FindWindow(XRCID("ID_RADIOBUTTON9"));
	m_DisplayModeratorsRadio = (wxRadioButton*)FindWindow(XRCID("ID_RADIOBUTTON8"));
	m_DisplayNoneRadio = (wxRadioButton*)FindWindow(XRCID("ID_RADIOBUTTON7"));
	StaticText10 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT10"));
	m_MeetingContinueCheck = (wxCheckBox*)FindWindow(XRCID("ID_CHECKBOX_MEETINGCONTINUE"));
	m_ModeratorEditCheck = (wxCheckBox*)FindWindow(XRCID("ID_CHECKBOX_MODERATOREDIT"));
	m_RollCallCheck = (wxCheckBox*)FindWindow(XRCID("ID_CHECKBOX_ROLLCALL"));
	m_LectureModeCheck = (wxCheckBox*)FindWindow(XRCID("ID_CHECKBOX_LECTUREMODE"));
	m_HoldModeCheck = (wxCheckBox*)FindWindow(XRCID("ID_CHECKBOX_HOLDMODE"));
	m_RecordingCheck = (wxCheckBox*)FindWindow(XRCID("ID_CHECKBOX_RECORDING"));
	m_InvalidateInstantPinCheck = (wxCheckBox*)FindWindow(XRCID("ID_CHECKBOX_INVALIDATEPIN"));
	m_PINExpireText1 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT_EXPIRE1"));
	m_InvalidateHoursSpin = (wxSpinCtrl*)FindWindow(XRCID("ID_SPINCTRL_INVALIDATEHOURS"));
	m_PINExpireText2 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT_EXPIRE2"));
	m_ChatAllRadio = (wxRadioButton*)FindWindow(XRCID("ID_RADIOBUTTON12"));
	m_ChatNoneRadio = (wxRadioButton*)FindWindow(XRCID("ID_RADIOBUTTON10"));
	m_ChatModeratorsRadio = (wxRadioButton*)FindWindow(XRCID("ID_RADIOBUTTON11"));
	m_IncludeDocsInRecordingCheck = (wxCheckBox*)FindWindow(XRCID("ID_CHECKBOX_DOCSINRECORDING"));
	m_UploadBtn = (wxButton*)FindWindow(XRCID("ID_BUTTON_UPLOAD"));
	m_OptionsPanel = (wxPanel*)FindWindow(XRCID("ID_PANEL3"));
	m_PictureContainer = (wxPanel*)FindWindow(XRCID("ID_PICTURES_PANEL"));
	m_ProfileImageBtn = (wxButton*)FindWindow(XRCID("ID_BUTTON_PROFILEIMAGE"));
	m_SekectImagesBtn = (wxButton*)FindWindow(XRCID("ID_BUTTON_SELECTIMAGES"));
	m_CropImageCheck = (wxCheckBox*)FindWindow(XRCID("ID_CROP_CHECKBOX"));
	m_PicturesPanel = (wxPanel*)FindWindow(XRCID("ID_PANEL4"));
	Notebook1 = (wxNotebook*)FindWindow(XRCID("ID_NOTEBOOK1"));
	
	Connect(XRCID("ID_DATEPICKERCTRL_START"),wxEVT_DATE_CHANGED,(wxObjectEventFunction)&MeetingSetupDialog::OnDateChangedStart);
	Connect(XRCID("ID_SPINBTN_START"),wxEVT_SCROLL_THUMBTRACK,(wxObjectEventFunction)&MeetingSetupDialog::OnSpinBtnStartChange);
	Connect(XRCID("ID_SPINBTN_START"),wxEVT_SCROLL_LINEUP,(wxObjectEventFunction)&MeetingSetupDialog::OnSpinBtnStartChangeUp);
	Connect(XRCID("ID_SPINBTN_START"),wxEVT_SCROLL_LINEDOWN,(wxObjectEventFunction)&MeetingSetupDialog::OnSpinBtnStartChangeDown);
	Connect(XRCID("ID_DATEPICKERCTRL_END"),wxEVT_DATE_CHANGED,(wxObjectEventFunction)&MeetingSetupDialog::OnDateChangedEnd);
	Connect(XRCID("ID_SPINBTN_END"),wxEVT_SCROLL_THUMBTRACK,(wxObjectEventFunction)&MeetingSetupDialog::OnSpinBtnEndChange);
	Connect(XRCID("ID_SPINBTN_END"),wxEVT_SCROLL_LINEUP,(wxObjectEventFunction)&MeetingSetupDialog::OnSpinBtnEndChangeUp);
	Connect(XRCID("ID_SPINBTN_END"),wxEVT_SCROLL_LINEDOWN,(wxObjectEventFunction)&MeetingSetupDialog::OnSpinBtnEndChangeDown);
	Connect(XRCID("ID_BUTTON_PHONEALL"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&MeetingSetupDialog::OnPhoneAllClick);
	Connect(XRCID("ID_BUTTON_PHONENONE"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&MeetingSetupDialog::OnPhoneNoneClick);
	Connect(XRCID("ID_BUTTON_EMAILALL"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&MeetingSetupDialog::OnEmailAllClick);
	Connect(XRCID("ID_BUTTON_EMAILNONE"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&MeetingSetupDialog::OnEmailNoneClick);
	Connect(XRCID("ID_BUTTON_IMALL"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&MeetingSetupDialog::OnIMAllClick);
	Connect(XRCID("ID_BUTTON_IMNONE"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&MeetingSetupDialog::OnIMNoneClick);
	Connect(XRCID("ID_BUTTON_REMOVE_SELECTED"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&MeetingSetupDialog::OnRemoveSelectedBtnClick);
	Connect(XRCID("ID_BUTTON_INVITE_NEW"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&MeetingSetupDialog::OnInviteNewClick);
	Connect(XRCID("ID_SHOWCONTACTS_BTN"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&MeetingSetupDialog::OnShowContactsClick);
	Connect(XRCID("ID_CHECKBOX_INVALIDATEPIN"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&MeetingSetupDialog::OnInvidateInstantPinCheckClick);
	Connect(XRCID("ID_BUTTON_UPLOAD"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&MeetingSetupDialog::OnUploadBtnClick);
	Connect(XRCID("ID_BUTTON_PROFILEIMAGE"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&MeetingSetupDialog::OnProfileImageBtnClick);
	Connect(XRCID("ID_BUTTON_SELECTIMAGES"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&MeetingSetupDialog::OnUploadImagesClick);
	//*)

    // These events were taken out of wxSmith b/c they don't work on Gtk+...which they don't so we need to replace
	m_StartTime->Connect(XRCID("ID_TEXTCTRL_STARTTIME"),wxEVT_LEFT_UP,(wxObjectEventFunction)&MeetingSetupDialog::OnLeftUpStartTime,NULL,this);
	m_EndTime->Connect(XRCID("ID_TEXTCTRL_ENDTIME"),wxEVT_LEFT_UP,(wxObjectEventFunction)&MeetingSetupDialog::OnLeftUpEndTime,NULL,this);
    m_StartTime->Connect(XRCID("ID_TEXTCTRL_STARTTIME"),wxEVT_CHAR,(wxObjectEventFunction)&MeetingSetupDialog::OnCharStartTime,NULL,this);
	m_StartTime->Connect(XRCID("ID_TEXTCTRL_STARTTIME"),wxEVT_SET_FOCUS,(wxObjectEventFunction)&MeetingSetupDialog::OnSetFocusStartTime,NULL,this);
	m_StartTime->Connect(XRCID("ID_TEXTCTRL_STARTTIME"),wxEVT_KILL_FOCUS,(wxObjectEventFunction)&MeetingSetupDialog::OnEndFocusStartTime,NULL,this);
	m_EndTime->Connect(XRCID("ID_TEXTCTRL_ENDTIME"),wxEVT_CHAR,(wxObjectEventFunction)&MeetingSetupDialog::OnCharEndTime,NULL,this);
	m_EndTime->Connect(XRCID("ID_TEXTCTRL_ENDTIME"),wxEVT_SET_FOCUS,(wxObjectEventFunction)&MeetingSetupDialog::OnSetFocusEndTime,NULL,this);

	Connect(XRCID("ID_CHECKBOX_NOMEETINGTIME"), wxEVT_COMMAND_CHECKBOX_CLICKED, (wxObjectEventFunction)&MeetingSetupDialog::OnNoMeetingTimeCheck);

    ////////////////////////////////////////////////////////////////////////////////////////
    // Set up meeting picture view/upload
    wxSize size = m_PictureContainer->GetSize();
    m_Pictures = new ImagePanel( m_PictureContainer, size, _("Meeting pictures"), false);
    wxSizer *pictureSizer = new wxBoxSizer(wxVERTICAL);
    pictureSizer->Add( m_Pictures, 1, wxEXPAND, 0);
    m_PictureContainer->SetSizer( pictureSizer);
    m_CropImageCheck->SetValue(wxGetApp().Options().GetUserBool("AutoCropPictures", true ));

    ////////////////////////////////////////////////////////////////////////////////////////
    // Create a Custom Grid - wxSheet
    // NOTE: An empty grid cannot be created due to a bug in wxSheet.
    // Grid initialization is done in the AppendRow() method.
    //
    // If you call m_Invitees->CreateGrid( 0, Max_Columns ) then very bad things happen.
    ////////////////////////////////////////////////////////////////////////////////////////
    int proportion = 1;
    int border = 5;
    int flag = wxEXPAND | wxRIGHT | wxLEFT | wxTOP | wxBOTTOM;
    m_Invitees = new wxSheet( m_InviteesPanel, XRCID("ID_INVITEES_GRID"),wxDefaultPosition,wxDefaultSize,wxWANTS_CHARS|wxSIMPLE_BORDER);
    m_panelSizer = new wxBoxSizer(wxVERTICAL);
    m_panelSizer->Add( m_Invitees, proportion, flag, border);
    m_InviteesPanel->SetSizer( m_panelSizer );

    /////////////////////////////////////////////////////////////////////////////////
    // Don't allow overflow of text from one cell to the next:
    // Combo boxes that jump to the right when activated look really really bad.
    m_Invitees->SetAttrOverflow( wxGridCellSheetCoords, false, wxSHEET_AttrDefault );
    m_Invitees->SetAttrOverflowMarker( wxGridCellSheetCoords, false, wxSHEET_AttrDefault );

    ////////////////////////////////////////////////////////////
    // Force select by row: ?
    // Note: This looks really ugly with wxSheet
    m_Invitees->SetSelectionMode( wxSHEET_SelectRows );

    // m_Invitees->SetScrollBarMode( wxSheet::SB_ALWAYS);
    m_Invitees->SetDefaultRowHeight( 20 );

    // Connect grid events
    Connect(wxID_ANY, wxEVT_SHEET_CELL_VALUE_CHANGED, (wxObjectEventFunction)&MeetingSetupDialog::OnGridCellChanged);

    // Prepare the status images
    m_StatusOnline =    m_StatusImages.AddSubImageArray( wxGetApp().GetResourceFile(_T("res/status_meeting_online")), 3 );
    m_StatusPhone =     m_StatusImages.AddSubImageArray( wxGetApp().GetResourceFile(_T("res/status_meeting_phone")), 7 );
    m_StatusContact =   m_StatusImages.AddSubImageArray( wxGetApp().GetResourceFile(_T("res/status_meeting_contact")), 5 );
    m_StatusImages.Create( NULL );

    // Restore the window metrics
    // wxGetApp().Options().LoadScreenMetrics( "MeetingSetupDialog", *this );

    m_InviteePopup = new wxMenu();
	m_InviteePopup->Append( Invitee_ChooseFromList, _T("Choose from address books ..."), _T("Display the address book list."), wxITEM_NORMAL );
	m_InviteePopup->Append( Invitee_CreateOrEdit,  _T("Edit ...") , _T("Edit this invitee or create a new contact."), wxITEM_NORMAL );
	m_InviteePopup->Append( Invitee_RemoveFromMeeting, _T("Remove ..."), _T("Remove this invitee from the meeting"), wxITEM_NORMAL );

    // Create the popup menu for the search nodes
    m_SearchPopup = new wxMenu();
    m_SearchPopup->Append( Search_Continue, _("Find more"), _("Search for additional meetings"), wxITEM_NORMAL );
    m_SearchPopup->Append( Search_Remove, _("Remove search"), _("Remove these search results"), wxITEM_NORMAL );
    m_SearchPopup->AppendSeparator();
    m_SearchPopup->Append( Search_Edit, _("Edit search criteria"), _("Change the search critera and start the search again"), wxITEM_NORMAL );

    Connect( Invitee_ChooseFromList, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingSetupDialog::OnChooseInvitee) );
    Connect( Invitee_CreateOrEdit, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingSetupDialog::OnEditInvitee) );
    Connect( Invitee_RemoveFromMeeting, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingSetupDialog::OnRemoveInvitee) );

    CONTROLLER.Connect(wxID_ANY, wxEVT_PRESENCE_EVENT, PresenceEventHandler( MeetingSetupDialog::OnPresenceEvent), 0, this );

	Connect( wxID_CANCEL, wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&MeetingSetupDialog::OnCancel);

    wxDateTime now = wxDateTime::Now();
    m_TimeZone->SetLabel(now.Format(wxT("%Z")));

    if ( PREFERENCES.m_fEnablePrivateMeetings == false )
        m_PrivacyPrivateRadio->Enable( false );

    if ( wxGetApp().Options().GetSystemBool( "DisablePINExpire", false ) )
    {
        m_InvalidateInstantPinCheck->Show( false );
        m_InvalidateHoursSpin->Show( false );
        m_PINExpireText1->Show( false );
        m_PINExpireText2->Show( false );
    }

    SetupAutoComplete();
}

void MeetingSetupDialog::InitializeColumnHeaders()
{
    m_Invitees->SetCellValue( wxSheetCoords(-1,Col_Status),     _("Status") );
    m_Invitees->SetCellValue( wxSheetCoords(-1,Col_Contact),    _("Contact"));
    m_Invitees->SetCellValue( wxSheetCoords(-1,Col_Phone),      _("Phone"));
    m_Invitees->SetCellValue( wxSheetCoords(-1,Col_Email),      _("Email"));
    m_Invitees->SetCellValue( wxSheetCoords(-1,Col_IM),         _("IM"));
    m_Invitees->SetCellValue( wxSheetCoords(-1,Col_Moderator),  _("Moderator"));

    m_Invitees->SetRowLabelWidth(0);
    m_Invitees->SetColLabelHeight(20);

    m_Invitees->SetColWidth(Col_Status, 80);
    m_Invitees->SetColWidth(Col_Contact, 140);

    bool        fTelAvail;
    fTelAvail = CONTROLLER.IsTelephonyAvailable( );
    if( fTelAvail )
    {
        m_Invitees->SetColWidth(Col_Phone, 132);
    }
    else
    {
        m_Invitees->SetMinimalAcceptableColWidth( 0 );
        m_Invitees->SetColWidth( Col_Phone, 0 );
    }

    m_Invitees->SetColWidth(Col_Email, 132);
    m_Invitees->SetColWidth(Col_IM, 132);
    m_Invitees->SetColWidth(Col_Moderator, 80);

    m_Invitees->EnableGridLines(false);
    m_Invitees->EnableDragRowSize(false);
    m_Invitees->EnableDragGridSize(false);
}

void MeetingSetupDialog::ConnectToController()
{
    CONTROLLER.Connect(wxID_ANY, wxEVT_MEETING_FETCHED,         wxCommandEventHandler(MeetingSetupDialog::OnMeetingFetched),        0, this );
    CONTROLLER.Connect(wxID_ANY, wxEVT_SCHEDULE_UPDATED,        wxCommandEventHandler(MeetingSetupDialog::OnScheduleUpdated),       0, this );
    CONTROLLER.Connect(wxID_ANY, wxEVT_SCHEDULE_ERROR,          wxCommandEventHandler(MeetingSetupDialog::OnScheduleError),         0, this );
    CONTROLLER.Connect(wxID_ANY, wxEVT_SESSION_ERROR,           SessionErrorEventHandler(MeetingSetupDialog::OnSessionError),       0, this );
    CONTROLLER.Connect(wxID_ANY, wxEVT_ADDRESSBK_UPDATE_FAILED, wxCommandEventHandler(MeetingSetupDialog::OnAddressbkUpdateFailed), 0, this );
    CONTROLLER.Connect(wxID_ANY, wxEVT_ADDRESS_UPDATED,         wxCommandEventHandler(MeetingSetupDialog::OnAddressUpdated),        0, this );

    CONTROLLER.Connect(wxID_ANY, wxEVT_DIRECTORY_FETCHED,       wxCommandEventHandler( MeetingSetupDialog::OnDirectoryFetched),     0, this );
}

MeetingSetupDialog::~MeetingSetupDialog()
{
    wxLogDebug( wxT("entering MeetingSetupDialog::~MeetingSetupDialog( )  DESTRUCTOR") );

    if (m_pDoc)
        m_pDoc->DeleteAllViews();

    CONTROLLER.Disconnect(wxID_ANY, wxEVT_MEETING_FETCHED,         wxCommandEventHandler( MeetingSetupDialog::OnMeetingFetched), 0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_SCHEDULE_UPDATED,        wxCommandEventHandler(MeetingSetupDialog::OnScheduleUpdated), 0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_SCHEDULE_ERROR,          wxCommandEventHandler(MeetingSetupDialog::OnScheduleError), 0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_SESSION_ERROR,           SessionErrorEventHandler(MeetingSetupDialog::OnSessionError),       0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_ADDRESSBK_UPDATE_FAILED, wxCommandEventHandler(MeetingSetupDialog::OnAddressbkUpdateFailed), 0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_ADDRESS_UPDATED,         wxCommandEventHandler(MeetingSetupDialog::OnAddressUpdated),        0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_DIRECTORY_FETCHED,       wxCommandEventHandler( MeetingSetupDialog::OnDirectoryFetched),     0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_PRESENCE_EVENT,          PresenceEventHandler( MeetingSetupDialog::OnPresenceEvent),         0, this );

    CleanupMeetingResources();
    wxLogDebug( wxT("    leaving MeetingSetupDialog::~MeetingSetupDialog( )  DESTRUCTOR") );
}

void MeetingSetupDialog::OnSize( wxSizeEvent& event )
{
    wxGetApp().Options().SaveScreenMetrics( "MeetingSetupDialog", *this );
    event.Skip();
}

void MeetingSetupDialog::OnMove( wxMoveEvent &event  )
{
    wxGetApp().Options().SaveScreenMetrics( "MeetingSetupDialog", *this );
    event.Skip();
}

void MeetingSetupDialog::SetupForStartMeeting()
{
	m_fMeetingNotActive = true;
    m_fStartMeetingMode = true;
    SetOKButtonLabel( _("Start meeting") );
}

void MeetingSetupDialog::SetupForMeetingInProgress()
{
    m_fMeetingNotActive = false;
    m_fStartMeetingMode = true;
    SetOKButtonLabel( _("Invite") );
}

void MeetingSetupDialog::SetupForChangeOptions()
{
    m_fMeetingNotActive = false;
    m_fStartMeetingMode = true;
    SetOKButtonLabel( _("OK") );
}

void MeetingSetupDialog::InviteUserId(wxString strUserId)
{
    if (!IsShown())
        m_strUserIdToAdd = strUserId;
    else
    {
        //  Lookup screen name and get contact_t for them
        contact_t contact = CONTROLLER.Directories()->FindContact(strUserId);
        if( contact )
        {
            wxLogDebug( wxT("  dir_find_contact( ) returned: username: *%s*   screenname: *%s*"), contact->username, contact->screenname );
            // Make sure this contact is not already invited
            if( !AlreadyInvited( contact ) )
            {
                InviteContact( contact );
            }
        }
        else
            wxLogDebug( wxT("  dir_find_cont_by_scrnname( ) returned a NULL pointer") );
    }
}

void MeetingSetupDialog::ResetForScheduleMeeting()
{
    CONTROLLER.Show( );
    m_nMode = Mode_EditMeeting;
    m_fCommitOnOK = true;
    m_fStartMeetingMode = false;
    wxString    strTitle( _("New Meeting"), wxConvUTF8 );
    m_mtg_TO->title = meeting_details_string(m_mtg_TO, strTitle.mb_str( wxConvUTF8 ) );
    m_mtg_TO->meeting_id = 0;
    m_mtg_TO->type = TypeScheduled;
    wxString strLabel( _("Schedule meeting"), wxConvUTF8 );
    SetOKButtonLabel( strLabel );
}

void MeetingSetupDialog::SetOKButtonLabel( const wxString & newLabel )
{
    wxWindow *pOKWindow = wxWindow::FindWindowById( wxID_OK, this );
    if( pOKWindow )
    {
        wxButton *pOKButton = wxDynamicCast( pOKWindow, wxButton );
        if( pOKButton )
        {
            pOKButton->SetLabel( newLabel );
            //pOKButton->SetDefault();
        }
    }
}

int MeetingSetupDialog::EditMeeting( const wxString & meetingId, bool fCommitOnOK, bool fDispInvitees, bool fDispOptions, bool fUseActive, bool fInviteNew, int iNumPart, participant_t part[ ] )
{
    m_nMode = Mode_EditMeeting;
    m_fCommitOnOK = fCommitOnOK;

    // Parse meeting parameters from meeting id (yuck, replace with something something better)
    wxString    strRest;
    if( meetingId.StartsWith( wxT("?meetingtoken="), &strRest ) )
    {
        wxString    strMeetingToken = strRest.BeforeFirst( wxChar( ';' ) );
        wxString    strMeetingID = strRest.AfterFirst( wxChar( ';' ) );
        wxLogDebug( wxT("  in EditMeeting( ) - MtgToken = ***%s***   MtgID = ***%s***"), strMeetingToken.wc_str( ), strMeetingID.wc_str( ) );

        // Go and retrieve the meeting details.
        // OnMeetingFetched() will be called when the details are ready.
        SetMeetingTokenToEdit( strMeetingToken, strMeetingID );
    }
    else if( meetingId.StartsWith( wxT("?screenName="), &strRest ) )
    {
        wxString    strScreenName = strRest.BeforeFirst( wxChar( ';' ) );
        wxString    strMeetingID = strRest.AfterFirst( wxChar( ';' ) );
        wxLogDebug( wxT("  in EditMeeting( ) - ScreenName = ***%s***   MtgID = ***%s***"), strScreenName.wc_str( ), strMeetingID.wc_str( ) );

        // Go and retrieve the meeting details.
        // OnMeetingFetched() will be called when the details are ready.
        m_strScreenNameToAdd = strScreenName;
        SetMeetingIdToEdit( strMeetingID );
    }
    else if( fUseActive || iNumPart  >  0 )
    {
        //  Pass the array of participant_t's and get back a meeting_detail_t with only those participants...
        meeting_t       mtg_t;
        mtg_t = controller_find_meeting( CONTROLLER.controller, meetingId.mb_str( wxConvUTF8 ) );
        if( mtg_t )
        {
            m_mtg_FROM = controller_get_meeting_details( CONTROLLER.controller, mtg_t, iNumPart, part );

            if( m_mtg_FROM )
            {
                // Make a copy of the data and display the data
                m_mtg_TO = meeting_details_copy( CONTROLLER.Schedule( ), m_mtg_FROM );
                Value_to_GUI( );

                ConnectToController();
            }
        }

        Notebook1->RemovePage( 3 );
        m_PicturesPanel->Hide( );
        
        if (fInviteNew)
        {
            wxCommandEvent dummy;
            OnInviteNewClick(dummy);
        }
    }
    else
    {
        // Go and retrieve the meeting details.
       // OnMeetingFetched() will be called when the details are ready.
        SetMeetingIdToEdit( meetingId );
    }

    // Display the screen
    if( !fDispOptions )
    {
        Notebook1->RemovePage( 2 );
        m_OptionsPanel->Hide( );
        Notebook1->RemovePage( 0 );
        m_InfoPanel->Hide( );
    }
    if( !fDispInvitees )
    {
        Notebook1->RemovePage( 1 );
        m_ParticipantsPanel->Hide( );
    }
    if( fDispOptions && fDispInvitees &&!fCommitOnOK )
    {
        // Starting a meeting, start on invitees pane
        Notebook1->SetSelection( 1 );
    }

    int nRet = ShowModal();

    wxGetApp().Options().Write();

    return nRet;
}

int MeetingSetupDialog::CopyMeeting( const wxString & meetingId )
{
    m_nMode = Mode_CopyMeeting;
    m_fCommitOnOK = true;

    // Go and retrieve the meeting details.
    // OnMeetingFetched() will be called when the details are ready.
    SetMeetingIdToEdit( meetingId );

    int nRet = ShowModal();

    wxGetApp().Options().Write();

    return nRet;
}

int MeetingSetupDialog::NewMeeting()
{
    m_nMode = Mode_EditMeeting;
    m_fCommitOnOK = true;

    //m_UploadBtn->Disable();

    m_mtg_TO = meeting_details_new( CONTROLLER.Schedule() );
    wxString    strTitle( _("New Meeting"), wxConvUTF8 );
    m_mtg_TO->title = meeting_details_string( m_mtg_TO, strTitle.mb_str( wxConvUTF8 ) );
    Value_to_GUI();

    ConnectToController();
    SetupFileManager();

    if( !m_strUserIdToAdd.IsEmpty( ) )
    {
        //  Lookup screen name and get contact_t for them
        contact_t   contact = CONTROLLER.Directories()->FindContact(m_strUserIdToAdd);
        if( contact )
        {
            wxLogDebug( wxT("  dir_find_contact( ) returned: username: *%s*   screenname: *%s*"), contact->username, contact->screenname );
            // Make sure this contact is not already invited
            if( !AlreadyInvited( contact ) )
            {
                InviteContact( contact );
            }
        }
        else
            wxLogDebug( wxT("  dir_find_cont_by_scrnname( ) returned a NULL pointer") );
        m_strUserIdToAdd.Empty( );
    }

    // There may be additional invite commands queued up, process now
    CONTROLLER.ExecutePendingCommands(true);

    // Display the screen
    int nRet = ShowModal();

    CleanupMeetingResources();

    wxGetApp().Options().Write();

    return nRet;
}

bool MeetingSetupDialog::RequestSaveData()
{
    wxLogDebug( wxT("entering MeetingSetupDialog::RequestSaveData( )") );

    // Add new PAB contacts
    int err = AddNewPABContactRecords(); // N.B.: not currently used

    if( err == 0 )
    {
    #ifdef DEBUG_MEETINGSETUPDIALOG
        wxLogDebug(wxT("====================================="));
        wxLogDebug(wxT("The following data is to be saved:"));
        dump_meeting( m_mtg_TO );
    #endif

        // Update the rest of the meeting data
        err = schedule_update_meeting(
            CONTROLLER.Schedule(),
            CONTROLLER.opcode( MeetingCenterController::OP_EditMeeting).mb_str(wxConvUTF8),
            m_mtg_TO,
            m_nSendEmailOption
            );

        wxLogDebug( wxT("MeetingSetupDialog::RequestSaveData returned %d"),err );
    }

    return (err == 0);
}

bool MeetingSetupDialog::Validate()
{
    wxLogDebug(wxT("MeetingSetupDialog::Validate()"));

    // OK button clicked, close grid edit control
    if (m_Invitees->IsCellEditControlShown())
    {
        m_Invitees->SaveEditControlValue();
    }

    bool fValidated = true;
    if( !m_mtg_TO )
    {
        fValidated = false;
    }
    else
    {
        // Validate GUI content
        if (!m_NoMeetingTimeCheck->GetValue())
        {
            int iH, iM;
            wxDateTime  dtStart, dtEnd;

            iH  = m_dtStartTime.GetHour( );
            iM  = m_dtStartTime.GetMinute( );
            dtStart = m_StartDate->GetValue( );
            dtStart.SetHour( iH );
            dtStart.SetMinute( iM );

            iH  = m_dtEndTime.GetHour( );
            iM  = m_dtEndTime.GetMinute( );
            dtEnd = m_EndDate->GetValue( );
            dtEnd.SetHour( iH );
            dtEnd.SetMinute( iM );

            if( dtStart.IsLaterThan( dtEnd ) )
            {
               ::wxMessageBox( _("Meeting start time is after end time."), m_strCaption, wxOK|wxICON_EXCLAMATION, this );
               return false;
            }
        }

        //  Now is the time to delete those invitees the user indicated earlier
        if( m_arrInviteesToDelete.GetCount( )  >  0 )
        {
            //  First, reverse sort the array
            m_arrInviteesToDelete.Sort( ReverseSortInvitees );

            //  Second, remove the invitees from m_mtg_TO, skipping duplicate values
            int         i, iCount;
            invitee_t   invLast = NULL;
            for( i=0, iCount = (int)m_arrInviteesToDelete.GetCount( ); i<iCount; i++ )
            {
                invitee_t   invCurr = m_arrInviteesToDelete.Item( i );
                if( invLast  !=  invCurr )
                {
                    meeting_details_remove( m_mtg_TO, invCurr );
                }
                invLast = invCurr;
            }
        }
    }

    if( fValidated )
    {
        GUI_to_Value();

        MeetingConfirmation confirmDlg(this);
        int nMask = ConfirmationRequired();
        int nConf = confirmDlg.GetConfirmation( m_fStartMeetingMode, nMask );
        if( nConf == -1 )
        {
            return false; // The user said no
        }
        // Save the send email options the caller will want these
        m_nSendEmailOption = nConf;

        if( m_FileManager && m_FileManager->IsDirty() )
        {
            if (!m_FileManager->StoreCollection(wxT("meetingPictures")))
            {
                wxString msg = wxString(_("Unable to send updated pictures to the document server.")) + wxT("\n") +
                    _("Any existing pictures will not be modified.");
                ::wxMessageBox( msg, m_strCaption, wxOK, this );                
            }
        }

        if( m_fCommitOnOK )
        {
            RequestSaveData();

            // Prevent the OK click from being processed until after the update
            // is done.
            return false;
        }

        if (m_fMeetingNotActive && m_fStartMeetingMode)
        {
            ProjectCodeDialog dlg(this);
            dlg.GetProjectCode(m_strProjectCode);
        }
        
        return true;
    }

    // Invalid data
    return false;
}

int MeetingSetupDialog::AddNewPABContactRecords()
{
    int err = 0;
    int row;
    bool fNewContactsAdded = false;
    int nNewCount = 0;

    for( row = 0; err == 0 && row < NumberOfInvitees(); ++row )
    {
        wxContactCellEditor *pCE = (wxContactCellEditor *)m_Invitees->GetAttrEditor( wxSheetCoords(row,Col_Contact)).GetRefData();
        contact_t contact = ContactFromRow( row );
        if( pCE->GetAddAsNewContact() )
        {
            fNewContactsAdded = true;

            wxString strOpid;
            strOpid = MeetingCenterController::opcode( MeetingCenterController::OP_EditMeeting_Contact );

            wxString strCount;
            strCount = wxString::Format(wxT("%d"),nNewCount++);

            strOpid += strCount;

            contact->type = UserTypeContact;

            // If no profile has been specified, then assign one
            if( !contact->profileid || !(*contact->profileid) )
            {
                wxLogDebug(wxT("Using the default profile."));
                profile_iterator_t iter = profiles_iterate(CONTROLLER.AddressBook());
                profile_t profile = profiles_next(CONTROLLER.AddressBook(), iter);
                if( profile )
                {
                    contact->profileid = directory_string( CONTROLLER.GetPAB(), profile->profile_id);
                }
            }

            IncrPendingContactAdd();

            wxString strPassword(wxT(""));

            wxLogDebug(wxT("MeetingSetupDialog: Attempting to add a new PAB contact: %d at Row %d"),nNewCount,row);
#ifdef DEBUG_MEETINGSETUPDIALOG
            dump_contact( contact );
            wxLogDebug(wxT("==================================="));
#endif
            wxCharBuffer opid = strOpid.mb_str();
            wxCharBuffer pwd = strPassword.mb_str();

            err = addressbk_contact_update(CONTROLLER.AddressBook(),
                opid,dtPersonal,contact,pwd);

            wxLogDebug(wxT("MeetingSetupDialog: addressbk_contact_update returned: %d"),err);
            if( err != 0 )
            {
                --m_nPendingNewContacts;
                if( m_nPendingNewContacts == 0 )
                {
                    m_nState &= ~State_SavingNewContacts;
                }
                wxString err_str;
                switch( err )
                {
                case ERR_RPCFAILED: err_str = _("RPC Failed"); break;
                default:            err_str = wxString::Format(wxT("Logic error: addressbk_contact_update returned %d"),err);
                }
                m_strDatabaseError = wxString::Format(_("Unable to add contact record:\n%s"), err_str.c_str() );
                ::wxMessageBox( m_strDatabaseError, m_strCaption, wxOK, this );
            }
        }
    }

    // If there are pending contact records that we being processed
    wxStopWatch sw;
    long start_time = sw.Time();
    long timeout_ms = 10000;
    while( m_nPendingNewContacts > 0 )
    {
        CUtils::MsgWait( 500, 1 );
        wxGetApp().Yield();

        if( (sw.Time() - start_time) > timeout_ms )
        {
            wxLogDebug(wxT("MeetingSetupDialog: Timeout while writing new contacts: %d remaining..."),m_nPendingNewContacts);
            break;
        }
    }
    ClearPendingContactAdd();

    // If new contacts were added, then we need to reload the PAB and
    // grab the new user ID's
    if( fNewContactsAdded )
    {
        for( row = 0; err == 0 && row < NumberOfInvitees(); ++row )
        {
            wxContactCellEditor *pCE = (wxContactCellEditor *)m_Invitees->GetAttrEditor( wxSheetCoords(row,Col_Contact)).GetRefData();
            if( pCE->GetAddAsNewContact() )
            {
                contact_t contact = ContactFromRow( row );
                invitee_t invt = InviteeFromRow( row );

                // Update the new invitee record
                wxString userid( contact->userid, wxConvUTF8 );
                wxString screen( contact->screenname, wxConvUTF8 );

                invt->user_id = Alloc_pstring( userid );
                wxLogDebug(wxT("MeetingSetupDialog::AddNewPABContactRecords - New contact userid %s screen %s"),userid.c_str(),screen.c_str() );

            }
        }
    }

    return err;
}

int MeetingSetupDialog::AppendRow( invitee_t invt, contact_t contact, bool fAddAsNewContact )
{
    int nRow = -1; // The returned row index
    /****************************************************************
     Grid initialzation is here to work around a wsSheet bug:
     You cannot create a sheet with zero rows and N columns. If you try to do
     CreateGrid(0,N) thn the columns never get created. And new rows cannot be appended
     without causing asserts and crashes.
     *****************************************************************/
    if( m_Invitees->GetNumberRows() == 0 )
    {
        wxLogDebug(wxT("Creating meeting setup grid"));
        
        m_Invitees->CreateGrid(1,Max_Columns);
        InitializeColumnHeaders();
        m_panelSizer->Fit( m_Invitees );
        GetSizer()->Fit(this);

        // Make sure the users window size is restored
        if (!wxGetApp().Options().LoadScreenMetrics( "MeetingSetupDialog", *this ))
        {
            SetSize(800, 540);
#ifdef __WXMAC__
            CentreOnScreen();
#endif
        }

        Connect( wxEVT_SIZE, (wxObjectEventFunction)&MeetingSetupDialog::OnSize );
        Connect( wxEVT_MOVE, (wxObjectEventFunction)&MeetingSetupDialog::OnMove );

        nRow = 0;
    }
    else
    {
        if( !m_Invitees->AppendRows(1, wxSHEET_UpdateAll  ) )
        {
            wxLogDebug(wxT("AppendRows failed!"));
            return -1; // Error
        }
        nRow = NumberOfInvitees() - 1;

        // Keep rows the same height...not sure this is the best approach.
        int height = m_Invitees->GetRowHeight(0);
        m_Invitees->SetRowHeight(nRow, height);
    }

    // Col_Status - The graphical feedback
    GridCellImageRenderer *pR = new GridCellImageRenderer( &m_StatusImages );
    m_Invitees->SetAttrRenderer( wxSheetCoords(nRow, Col_Status), wxSheetCellRenderer(pR));
    m_Invitees->SetAttrReadOnly( wxSheetCoords(nRow, Col_Status), true );

    // Col_Contact - The contact name
    wxContactCellEditor *editor = new wxContactCellEditor( invt, contact, fAddAsNewContact );
    if (!m_autoNames.IsEmpty())
        editor->AutoComplete(m_autoNames);
    m_Invitees->SetAttrEditor( wxSheetCoords(nRow, Col_Contact), editor );
    m_Invitees->SetCellValue(  wxSheetCoords(nRow, Col_Contact), wxString( invt->name, wxConvUTF8 ) );

    // Col_Phone
    m_Invitees->SetAttrRenderer( wxSheetCoords(nRow, Col_Phone), wxSheetCellRenderer(new wxSheetCellStringRendererRefData() ) );
    m_Invitees->SetAttrEditor( wxSheetCoords(nRow, Col_Phone), wxSheetCellEditor(new GridCellComboIntEditor(0, 0, true) ) );

    // Col_Email
    m_Invitees->SetAttrRenderer( wxSheetCoords(nRow, Col_Email), wxSheetCellRenderer(new wxSheetCellStringRendererRefData() ) );
    m_Invitees->SetAttrEditor( wxSheetCoords(nRow, Col_Email), wxSheetCellEditor(new GridCellComboIntEditor(0, 0, true) ) );

    // Col_IM
    m_Invitees->SetAttrRenderer( wxSheetCoords(nRow, Col_IM), wxSheetCellRenderer(new wxSheetCellStringRendererRefData() ) );
    m_Invitees->SetAttrEditor( wxSheetCoords(nRow, Col_IM), wxSheetCellEditor(new GridCellComboIntEditor() ) );

    // Col_Moderator
#ifdef MOD_CHECK
    m_Invitees->SetCellValue( wxSheetCoords(nRow, Col_Moderator), _T("0"));
    m_Invitees->SetAttrRenderer( wxSheetCoords(nRow, Col_Moderator), wxSheetCellRenderer(new wxSheetCellBoolRendererRefData()));
    m_Invitees->SetAttrEditor( wxSheetCoords(nRow, Col_Moderator), wxSheetCellEditor(new GridCellBoolEditor()));
    m_Invitees->SetAttrAlignment( wxSheetCoords(nRow, Col_Moderator), wxSHEET_AttrAlignTop|wxSHEET_AttrAlignCenterHoriz);
#else
    m_Invitees->SetCellValue( wxSheetCoords(nRow, Col_Moderator), _T("No"));
    m_Invitees->SetAttrRenderer( wxSheetCoords(nRow, Col_Moderator), wxSheetCellRenderer(new wxSheetCellStringRendererRefData() ) );
    m_Invitees->SetAttrEditor( wxSheetCoords(nRow, Col_Moderator), wxSheetCellEditor(new GridCellComboIntEditor() ) );
#endif

    if( invt->role == RollHost )
    {
        m_Invitees->SetAttrReadOnly( wxSheetCoords(nRow, Col_Moderator), true );
    }

    SetPresence( nRow, false );

    // New row might cause us to need scrollbars
    m_Invitees->AdjustScrollbars( );

    return nRow;
}

void MeetingSetupDialog::OnInviteNewClick(wxCommandEvent& event)
{
    // Create a new invitee record
    invitee_t newInvt = meeting_details_add( m_mtg_TO );
    newInvt->is_adhoc = TRUE;

    // Create a new blank contact record
    int row = AppendRow( newInvt ); // Append the new Ad Hoc invitee

    // Populate the new row
    Invitee_to_GUI( row );
    
    wxSheetCoords newName(row, Col_Contact);

    m_Invitees->SetCellValue( newName, kNewInvitee);
    m_Invitees->SetAttrReadOnly( newName, false );

    // Set focus to and open edit control on new invitee
    m_Invitees->MakeCellVisible( newName );
    m_Invitees->SetGridCursorCell( newName );
    m_Invitees->EnableCellEditControl( newName );

}

void MeetingSetupDialog::OnShowContactsClick(wxCommandEvent& event)
{
    ::wxBeginBusyCursor( );

    if( !m_pContactDlg )
    {
        m_pContactDlg = new SelectContactDialog( this );
        m_pContactDlg->Connect( XRCID("ID_HIDE_BTN"),   wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&MeetingSetupDialog::OnHideContactsClick, 0, this);
        m_pContactDlg->Connect( XRCID("ID_INVITE_BTN"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&MeetingSetupDialog::OnInviteContactsClick, 0, this);
        m_pContactDlg->Connect( XRCID("ID_SEARCH_BTN"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&MeetingSetupDialog::OnSearchContactsClick, 0, this);

        m_pContactDlg->Connect( XRCID("ID_CONTACT_TREELIST"), wxEVT_COMMAND_TREE_SEL_CHANGED, (wxObjectEventFunction)&MeetingSetupDialog::OnSelectedContactsChanged, 0, this);
        m_pContactDlg->Connect(XRCID("ID_CONTACT_TREELIST"),wxEVT_COMMAND_TREE_ITEM_RIGHT_CLICK,(wxObjectEventFunction)&MeetingSetupDialog::OnContactItemRightClick, 0, this);
        m_pContactDlg->Connect(XRCID("ID_CONTACT_TREELIST"),wxEVT_COMMAND_LIST_COL_CLICK, (wxObjectEventFunction)&MeetingSetupDialog::OnContactListColumnLeftClick, 0, this);

        m_pContactInviteBtn = (wxButton *)m_pContactDlg->FindWindow( XRCID("ID_INVITE_BTN") );
        m_pContactInviteBtn->Enable( false );

        // Setup the contacts list
        wxString strTreeName(wxT("MS"));
        m_ContactTree.SetTree( (wxTreeListCtrl *)m_pContactDlg->FindWindow( XRCID("ID_CONTACT_TREELIST") ), strTreeName );

        if( CONTROLLER.GetCAB() )
        {
            m_CommunityNode = m_ContactTree.AppendDirectoryNode( _("Community"), CONTROLLER.GetCAB(), dtGlobal );
        }
        m_PersonalNode = m_ContactTree.AppendDirectoryNode( _("Personal"), CONTROLLER.GetPAB(), dtPersonal );

        // Add user imported directories
        directory_t dir = CONTROLLER.Directories()->GetDirectoryFirst();
        while (dir)
        {
            wxString dirName(dir->name, wxConvUTF8);
            m_ContactTree.AppendDirectoryNode( dirName, dir, dtUser );
            
            dir = CONTROLLER.Directories()->GetDirectoryNext();
        }

        m_SearchNode = m_ContactTree.AppendSearchResultsRoot();

        m_ContactTree.Tree()->SetSortColumn( 0 );
        SortContacts();
    }

    ::wxEndBusyCursor( );

    if( m_pContactDlg )
    {
        m_pContactDlg->Show();

        /* Gtk disables all other windows when one is modal, including modeless dialogs created by that
           window.  For now, hack around this by setting ourselves non-modal while this dialog is up. */
        CUtils::SetModal(this, false);
    }
}

void MeetingSetupDialog::OnHideContactsClick(wxCommandEvent& event)
{
    if( m_pContactDlg )
    {
        if( m_pFindContactsDlg )
        {
            m_pFindContactsDlg->Hide();
        }
        m_pContactDlg->Hide();
        CUtils::SetModal(this, true);
    }
}

void MeetingSetupDialog::OnSelectedContactsChanged(wxTreeEvent& event)
{
    wxLogDebug(wxT("MeetingSetupDialog::OnSelectedContactsChanged"));
    // Figure out if the Invite to meeting button s/b enabled
    if( m_pContactInviteBtn )
    {
        m_pContactInviteBtn->Enable( m_ContactTree.HasContactsSelected() );
    }
}

void MeetingSetupDialog::OnContactItemRightClick(wxTreeEvent& event)
{
    ContactSearchResultsItemData *pSrch = m_ContactTree.GetSearchNode();
    if( pSrch )
    {
        // Prep the menu
        m_SearchPopup->Enable( Search_Continue, !pSrch->m_fComplete );
        //Search_Remove - Always OK
        //Search_Edit - Always OK

        // NOTE: MeetingSetupDialog is processing a right click
        // event from the SelectContact dialog. The mouse
        // click coordinates need to be relative to the
        // MeetingSetupDialog's client area.
        wxPoint menuPos;
        menuPos = m_pContactDlg->ClientToScreen( event.GetPoint() );
        menuPos = ScreenToClient( menuPos );
        PopupMenu( m_SearchPopup, menuPos );
    }
    else
    {
        // Popup ????
    }
}

void MeetingSetupDialog::OnInviteContactsClick(wxCommandEvent& event)
{
    // One or more contacts have been selected, the user want's to invite all
    // of them to the meeting.

    int nToalNewInvitees = 0;
    wxArrayTreeItemIds selections;
    unsigned int nSel = m_ContactTree.Tree()->GetSelections( selections );
    for( unsigned int iContact = 0; iContact < nSel; ++iContact )
    {
        ContactTreeItemData *pData = (ContactTreeItemData *)m_ContactTree.Tree()->GetItemData( selections[iContact] );
        if( pData && (pData->m_nType & ContactTreeList::Node_Contact) && pData->m_contact )
        {
            // Make sure this contact is not already invited
            if( !AlreadyInvited( pData->m_contact ) )
            {
                InviteContact( pData->m_contact );
                ++nToalNewInvitees;
            }
        }
    }

    // Confusing to keep dialog open, so close it now...maybe add an option later to keep dialog open
    OnHideContactsClick(event);
}

bool MeetingSetupDialog::AlreadyInvited( contact_t contact )
{
    wxString userId_Requested( contact->userid, wxConvUTF8 );
    wxString userId_Existing;

    for( int iCurrent = 0; iCurrent < NumberOfInvitees(); ++iCurrent )
    {
        contact_t existing_contact = ContactFromRow( iCurrent );
        if( existing_contact )
        {
            userId_Existing = wxString( existing_contact->userid, wxConvUTF8 );
            if( userId_Requested == userId_Existing )
            {
                return true; // userId_Requested is already invited.
            }
        }
    }
    return false;
}

bool MeetingSetupDialog::InviteContact( contact_t contact )
{
    // Create a new invitee record
    invitee_t newInvt = meeting_details_add( m_mtg_TO );

    // Gather some data from the contact
    wxString userid( contact->userid, wxConvUTF8 );
    wxString first( contact->first, wxConvUTF8 );
    wxString last( contact->last, wxConvUTF8 );
    wxString screenname( contact->screenname, wxConvUTF8 );    
    wxString invt_name = first + wxT(" ") + last;
    wxString email;

    newInvt->selemail = SelEmailNone;
    if( contact->email && *contact->email )
    {
        newInvt->email = Alloc_pstring(contact->email);
        newInvt->selemail = SelEmail1;
    }
    else if( contact->email2 && *contact->email2 )
    {
        newInvt->email = Alloc_pstring(contact->email2);
        newInvt->selemail = SelEmail2;
    }
    else if( contact->email3 && *contact->email3 )
    {
        newInvt->email =  Alloc_pstring(contact->email3);
        newInvt->selemail = SelEmail3;
    }

    newInvt->user_id = Alloc_pstring( userid );
    newInvt->name = Alloc_pstring( invt_name );
    newInvt->screenname = Alloc_pstring( screenname );

    int row = AppendRow( newInvt, contact ); // Append the new contact

    if( row != -1 )
    {
        wxContactCellEditor *pCE = (wxContactCellEditor *)m_Invitees->GetAttrEditor( wxSheetCoords(row,Col_Contact)).GetRefData();
        if( contact->directory == CONTROLLER.GetCAB() )
        {
            pCE->SetFromCAB();
        }
        else if( contact->directory == CONTROLLER.GetPAB() )
        {
            pCE->SetFromPAB();
        }
        // Populate the new row
        Invitee_to_GUI( row );
    }

    return true;
}

void MeetingSetupDialog::OnRemoveSelectedBtnClick(wxCommandEvent& event)
{
    wxArrayInt selectedRows;
    selectedRows = GetSelectedInvitees();

    wxLogDebug(wxT("OnRemoveSelectedBtnClicked: Removing %d rows"), selectedRows.GetCount() );

    if( selectedRows.GetCount() )
    {
        // Remove the rows from the bottom to the top
        for( int iRow = selectedRows.GetCount() - 1; iRow >= 0; --iRow )
        {
            int nRow = selectedRows[iRow];

            DeleteRow( nRow );
        }
    }
}

bool MeetingSetupDialog::DeleteRow( int row )
{
    bool fDeleted = false;

    wxContactCellEditor *pCE = ContactEditorFromRow( row );

    if( pCE->m_invt->role != RollHost )
    {
        wxLogDebug(wxT("  Deleting row %d"),row);

        wxLogDebug(wxT("     From the m_mtg_TO structure"));
        m_arrInviteesToDelete.Add( pCE->m_invt );

        wxLogDebug(wxT("     From the m_Invitees grid"));
        m_Invitees->DeleteRows( row, 1, wxSHEET_UpdateAll );

        fDeleted = true;
    }
    return fDeleted;
}


void MeetingSetupDialog::SetMeetingIdToEdit( const wxString & meetingId )
{
    MeetingCenterController *pC = wxGetApp().m_pMC;
    if( !pC )
    {
        return; // This can never be.......
    }

    m_meetingId = meetingId;
    ConnectToController();
    SetupFileManager();

    // Set a state flag so we know how to handle
    m_nState |= State_WaitingForMeetingDetails;
    pC->FetchMeetingDetails( MeetingCenterController::OP_EditMeeting, m_meetingId );
}


void MeetingSetupDialog::SetMeetingTokenToEdit( const wxString & meetingToken, const wxString & meetingId )
{
    MeetingCenterController *pC = wxGetApp().m_pMC;
    if( !pC )
    {
        return; // This can never be.......
    }

    m_meetingId = meetingId;
    ConnectToController();
    SetupFileManager();

    //  This call generates an event from the server which is caught by on_get_params( ), which is where the meeting is started...
    int         iRet;
    m_nState |= State_WaitingForMeetingDetails;
    iRet = controller_get_meeting_params( CONTROLLER.controller, meetingId.mb_str(wxConvUTF8), meetingToken.mb_str(wxConvUTF8) );
    wxLogDebug( wxT("  controller_get_meeting_params( ) returned %d"), iRet );
}


void MeetingSetupDialog::OnMeetingFetched(wxCommandEvent& event)
{
    wxLogDebug( wxT("entering MeetingSetupDialog::OnMeetingFetched( )") );
    MeetingCenterController *pC = wxGetApp().m_pMC;
    if( !pC )
    {
        event.Skip(); // This should never happen!!!!!!
        return;
    }

    if( !pC->IsOpcode( event.GetString(), MeetingCenterController::OP_EditMeeting ) )
    {
        // This event is not for us
        wxLogDebug( wxT("   in OnMeetingFetched( ) - this event is not for us") );
        event.Skip();
        return;
    }

    // No longer waiting for this
    m_nState &= ~State_WaitingForMeetingDetails;

    if( m_nMode == Mode_CopyMeeting )
    {
        meeting_details_t original_mtg = (meeting_details_t)event.GetClientData();
        m_mtg_FROM = 0;
        m_mtg_TO = meeting_details_copy( CONTROLLER.Schedule(), original_mtg );

        // Make a new title and zap the meeting ID
        wxString original_title( m_mtg_TO->title, wxConvUTF8 );
        wxString copy_of_title;
        copy_of_title = _("Copy of ");
        copy_of_title += original_title;
        m_mtg_TO->title = Alloc_pstring( copy_of_title );
        m_mtg_TO->meeting_id = 0;
    }
    else
    {
        // Grab the meeting detail pointer
        m_mtg_FROM = (meeting_details_t)event.GetClientData();

        // Make a copy of the data and display the data
        m_mtg_TO = meeting_details_copy(CONTROLLER.Schedule(), m_mtg_FROM);
    }

    // May be a forum request to schedule a new meeting rather then start a meeting...we can't tell
    // the difference until we get this result from the server.
    if (event.GetInt() & ForumScheduleMeeting)
    {
        ResetForScheduleMeeting( );
    }

#ifdef DEBUG_MEETINGSETUPDIALOG
    wxLogDebug(wxT("===== Meeting Details Input ===="));
    dump_meeting( m_mtg_TO );
#endif

    Value_to_GUI();

    if( !m_strScreenNameToAdd.IsEmpty( ) )
    {
        //  Lookup screen name and get contact_t for them
        contact_t   contact;
        contact = directory_find_contact_by_screenname( CONTROLLER.GetCAB( ), m_strScreenNameToAdd.mb_str(wxConvUTF8) );
        if( contact )
        {
            wxLogDebug( wxT("  dir_find_cont_by_scrnname( ) returned: username: *%s*   screenname: *%s*"), contact->username, contact->screenname );
            // Make sure this contact is not already invited
            if( !AlreadyInvited( contact ) )
            {
                InviteContact( contact );
            }
        }
        else
            wxLogDebug( wxT("  dir_find_cont_by_scrnname( ) returned a NULL pointer") );
        m_strScreenNameToAdd.Empty( );
    }

    if( !m_strUserIdToAdd.IsEmpty( ) )
    {
        //  Lookup screen name and get contact_t for them
        contact_t   contact = CONTROLLER.Directories()->FindContact(m_strUserIdToAdd);
        if( contact )
        {
            wxLogDebug( wxT("  dir_find_contact( ) returned: username: *%s*   screenname: *%s*"), contact->username, contact->screenname );
            // Make sure this contact is not already invited
            if( !AlreadyInvited( contact ) )
            {
                InviteContact( contact );
            }
        }
        else
            wxLogDebug( wxT("  dir_find_cont_by_scrnname( ) returned a NULL pointer") );
        m_strUserIdToAdd.Empty( );
    }
    
    // There may be additional invite commands queued up, process now
    CONTROLLER.ExecutePendingCommands(true);
}

void MeetingSetupDialog::Value_to_GUI()
{
    // m_mtg_TO ======> UI

    wxString caption;
    wxString space(wxT(" "));
    wxString dash(wxT(" - "));
    wxString id(_("ID: "));

    caption += m_strCaption;
    caption += dash;
    caption += id;
    caption += wxString(m_mtg_TO->meeting_id,wxConvUTF8);
    caption += dash;
    caption += wxString(m_mtg_TO->title,wxConvUTF8);
    SetTitle( caption );

	//int     type;                       (Edit == TypeScheduled or TypeInstant) (New == TypeScheduled) (Not modified)

	//int     privacy;                    wxRadioButton* m_PrivacyPrivateRadio, m_PrivacyUnlistedRadio, m_PrivacyPublicRadio;
	if( m_mtg_TO->privacy == PrivacyPublic ) m_PrivacyPublicRadio->SetValue(true);
	else if( m_mtg_TO->privacy == PrivacyUnlisted ) m_PrivacyUnlistedRadio->SetValue(true);
	else if( m_mtg_TO->privacy == PrivacyPrivate ) m_PrivacyPrivateRadio->SetValue(true);

	//pstring title;                      wxTextCtrl m_MeetingTitle
	m_MeetingTitle->SetValue( wxString( m_mtg_TO->title,  wxConvUTF8 ) );

	//int     start_time;                 wxDatePickerCtrl* m_StartDate, wxTextCtrl* m_StartTime
    DateTime_to_GUI( m_mtg_TO->start_time, m_StartDate, m_StartTime, m_dtStartTime );

	//int     options;                    (All over the place)
	int ops = m_mtg_TO->options;

    //UNREFERENCED options
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//WaitForModerator         = 1,       // ** Not used (see continue without moderator)
	//LockParticipants         = 8,       // Don't allow new particiants to join
	//AutoTimeExtension        = 16,      // Extend meeting if runs long
	//RequirePassword          = 32,      // Absence of password may be sufficient indication
    //DefaultCallUsers         = 512,     // Set the Call Users check in meeting setup dialog
    //DefaultSendEmail         = 1024,    // Set the Send EmailInvites check in meeting setup dialog
    //DefaultSendIM            = 2048,    // Set the Send IM Invite check in meeting setup dialog
    //DefaultModerator         = 4096,    // Set the Is Moderator check in meeting setup dialog
    //InviteAlwaysValidBeforeStart = 32768,  // Invites work any time prior to meeting, otherwise use specific value
	//MuteJoinLeave            = 131072,	// Don't play entry/exit tones/prompts when participants enter/exit
    //InvitationPCPhone        = 524288,  // Include PC Phone instructions in invitations

    bool voice = (ops & InvitationVoiceOnly) != 0;
    bool data  = (ops & InvitationDataOnly) != 0;
    // Note: both bits shouldn't be set together; voice + data is neither bit set

    bool        fTelAvail;
    fTelAvail = CONTROLLER.IsTelephonyAvailable( );
    if( fTelAvail )
    {
        if( voice )
            m_VoiceOnlyRadio->SetValue(true);
        else if( data )
            m_DataShareRadio->SetValue(true);
        else
            m_VoiceDataRadio->SetValue(true);

        SetOption( ops, Prefer1To1,       m_OneToOneCheck );
        SetOption( ops, ScreenCallers,    m_RollCallCheck );
        SetOption( ops, LectureMode,      m_LectureModeCheck );
        SetOption( ops, HoldMode,         m_HoldModeCheck );
    }
    else
    {
        m_VoiceDataRadio->SetValue(false);
        m_VoiceOnlyRadio->SetValue(false);
        m_DataShareRadio->SetValue(true);
        m_VoiceDataRadio->Disable( );
        m_VoiceOnlyRadio->Disable( );
        m_DataShareRadio->Enable( );

        m_OneToOneCheck->Disable( );
        m_StaticTextSetPhones->Hide( );
        m_ButtonPhoneAll->Hide( );
        m_ButtonPhoneNone->Hide( );
        m_RollCallCheck->Disable( );
        m_LectureModeCheck->Disable( );
        m_HoldModeCheck->Disable( );
    }

	SetOption( ops, ContinueWithoutModerator, m_MeetingContinueCheck );
	SetOption( ops, ModeratorMayEdit, m_ModeratorEditCheck );
    SetOption( ops, RecordingMeeting, m_RecordingCheck );
    SetOption( ops, IncludeDocsInRecording, m_IncludeDocsInRecordingCheck );


    //NOTE: Checking this disables: m_InvalidateHoursSpin
	SetOption( ops, InviteNeverExpires, m_InvalidateInstantPinCheck );
    m_InvalidateHoursSpin->Enable( !m_InvalidateInstantPinCheck->GetValue() );

	//int     host_enabled_features;      (n/a)

	//pstring description;                wxTextCtrl* m_MeetingDescription
	m_MeetingDescription->SetValue( wxString( m_mtg_TO->description, wxConvUTF8 ) );

	//int     end_time;                   wxDatePickerCtrl* m_EndDate, wxTextCtrl* m_EndTime;
	DateTime_to_GUI( m_mtg_TO->end_time, m_EndDate, m_EndTime, m_dtEndTime, (m_mtg_TO->end_time == 0) ? 1800 : 0 );

    if( m_mtg_TO->start_time == 0 && m_mtg_TO->end_time == 0 )
    {
        m_NoMeetingTimeCheck->SetValue( true );
    }
    else
    {
        m_NoMeetingTimeCheck->SetValue( false );
    }

	//int     invite_expiration;          wxSpinCtrl* m_InvalidateHoursSpin;
	m_InvalidateHoursSpin->SetValue( m_mtg_TO->invite_expiration == -1 ? 2 : m_mtg_TO->invite_expiration / 3600 );    // stored in seconds

	//int     invite_valid_before;        (n/a)

	//pstring invite_message;             m_MeetingInviteMessage
	m_MeetingInviteMessage->SetValue( wxString(m_mtg_TO->invite_message, wxConvUTF8) );

	//int     display_attendees;          wxRadioButton* m_DisplayNoneRadio, m_DisplayModeratorsRadio, m_DisplayAllRadio
	if( m_mtg_TO->display_attendees == DisplayNone ) m_DisplayNoneRadio->SetValue( true );
	else if( m_mtg_TO->display_attendees == DisplayModerators ) m_DisplayModeratorsRadio->SetValue( true );
	else if( m_mtg_TO->display_attendees == DisplayAll ) m_DisplayAllRadio->SetValue( true );

	//int     chat_options;               wxRadioButton* m_ChatNoneRadio, m_ChatModeratorsRadio, m_ChatAllRadio
	if( m_mtg_TO->chat_options == ChatNone ) m_ChatNoneRadio->SetValue( true );
	else if( m_mtg_TO->chat_options == ChatModerators ) m_ChatModeratorsRadio->SetValue( true );
	else if( m_mtg_TO->chat_options == ChatAll ) m_ChatAllRadio->SetValue( true );

	//pstring password;                   wxTextCtrl* m_MeetingPasswordText
	m_MeetingPasswordText->SetValue( wxString(m_mtg_TO->password, wxConvUTF8) );

	//pstring bridge_phone;               (n/a)
	//pstring system_url;                 (n/a)
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    invitee_iterator_t iter;
    invitee_t invt;

    iter = meeting_details_iterate(m_mtg_TO);
    while ((invt = meeting_details_next(m_mtg_TO, iter)) != NULL)
    {
        int nRow = -1;
        bool fCAB = true;

        ////////////////////////////////////////////////////////////////////
        // Attempt to locate the contact record.
        contact_t   contact   = NULL;
        if( !invt->is_adhoc )
            contact = LookupContact( invt, fCAB );

        // Add a new row to the grid
        nRow = AppendRow( invt, contact );
        if( nRow != -1 )
        {
            if( contact )
            {
                wxContactCellEditor *pE = ContactEditorFromRow( nRow );
                if( fCAB )
                {
                    pE->SetFromCAB();
                }
                else
                {
                    pE->SetFromPAB();
                }
            }
            Invitee_to_GUI( nRow );
        }
    }

    m_Invitees->AutoSizeRows();
}

void MeetingSetupDialog::Invitee_to_GUI( int nRow )
{
    invitee_t invt = 0;
    contact_t contact = 0;
    contact_t contact2 = 0;

    wxString contactTag(wxT(""));
    wxString contactTag2(wxT(""));

    wxContactCellEditor *pCE = ContactEditorFromRow( nRow );
    invt = pCE->m_invt;
    contact = pCE->m_contact;

    contactTag = contactTagText( pCE->m_nContactSource );

    if( pCE->m_nContactSource == wxContactCellEditor::ContactFromCAB )
    {
        //  Initial info is from the CAB - see if contact also has a PAB record
        if ( CONTROLLER.GetPAB( ) )
            contact2 = directory_find_contact_by_screenname( CONTROLLER.GetPAB( ), contact->screenname );
        if( contact2 )
            contactTag2 = contactTagText( wxContactCellEditor::ContactFromPAB );
    }
    else if( pCE->m_nContactSource == wxContactCellEditor::ContactFromPAB )
    {
        //  Initial info is from the PAB - see if contact also has a CAB record
        contact2 = directory_find_contact_by_screenname( CONTROLLER.GetCAB( ), contact->screenname );
        if( contact2 )
            contactTag2 = contactTagText( wxContactCellEditor::ContactFromCAB );
    }

    // Col_Status - The graphical feedback

    // Col_Contact - The contact name
    m_Invitees->SetCellValue(  wxSheetCoords(nRow, Col_Contact), wxString( invt->name, wxConvUTF8 ) );

    // Col_Phone
    GridCellComboIntEditor *pE = (GridCellComboIntEditor *)m_Invitees->GetAttrEditor( wxSheetCoords(nRow, Col_Phone) ).GetRefData();
    if( !CONTROLLER.IsTelephonyAvailable( ) )
    {
        // Column should be hidden; set to reasonable value
        int nComboSelection = Phone_DontCall;
        pE->Append( _("Don't call"), Phone_DontCall );
        pE->SetValue( nComboSelection );
        m_Invitees->SetCellValue( wxSheetCoords(nRow, Col_Phone), pE->StartValue() );
    }
    else
    {
        UpdatePhoneCellEditor( pE, invt, contact, contactTag, contact2, contactTag2 );
        m_Invitees->SetCellValue( wxSheetCoords(nRow, Col_Phone), pE->StartValue() );
    }

    // Col_Email
    pE = (GridCellComboIntEditor *)m_Invitees->GetAttrEditor( wxSheetCoords(nRow, Col_Email) ).GetRefData();
    UpdateEmailCellEditor( pE, invt, contact, contactTag, contact2, contactTag2 );
    m_Invitees->SetCellValue( wxSheetCoords(nRow, Col_Email), pE->StartValue() );

    // Col_IM
    pE = (GridCellComboIntEditor *)m_Invitees->GetAttrEditor( wxSheetCoords(nRow, Col_IM) ).GetRefData();
    UpdateIMCellEditor( pE, invt, contact, contactTag );
    m_Invitees->SetCellValue( wxSheetCoords(nRow, Col_IM), pE->StartValue() );

    // Col_Moderator
    bool fModerator = invt->role == RollModerator || invt->role == RollHost;
#ifdef MOD_CHECK
    m_Invitees->SetCellValue( wxSheetCoords(nRow, Col_Moderator), fModerator ? _("1"):_("0") );
#else
    m_Invitees->SetCellValue( wxSheetCoords(nRow, Col_Moderator), fModerator ? _("Yes"):_("No") );
    pE = (GridCellComboIntEditor *)m_Invitees->GetAttrEditor( wxSheetCoords(nRow, Col_Moderator) ).GetRefData();
    pE->Append(_("No"), 0);
    pE->Append(_("Yes"), 1);
    pE->SetValue( fModerator ? 1 : 0 );
#endif

    // Gather information about the host
    if( invt->role == RollHost && contact )
    {
        m_hostContact = contact;
        m_strHostProfileId = wxString( contact->profileid, wxConvUTF8 );
        wxLogDebug(wxT("Host profile id: |%s|"),m_strHostProfileId.c_str());
    }

    //pstring user_id;              // copy from contact for known users, blank for adhoc
    //pstring screenname;
    //int     selphone;
    //pstring phone;
    //int     selemail;
    //pstring email;
    //int     notify_type;
    //BOOL    is_adhoc;           // true if not a registered system user
    //BOOL    removed;

}

void MeetingSetupDialog::GUI_to_Invitee( int nRow, invitee_t invt )
{
    wxString blank(wxT(""));
    wxString busphone;
    wxString homephone;
    wxString mobilephone;
    wxString otherphone;
    wxString busphone2;
    wxString homephone2;
    wxString mobilephone2;
    wxString otherphone2;

    wxString email1;
    wxString email2;
    wxString email3;
    wxString email1_2;
    wxString email2_2;
    wxString email3_2;

    // Access the associated contact record via the Col_Contact editor
    wxContactCellEditor *pCE = ContactEditorFromRow( nRow );
    contact_t contact = pCE->m_contact;
    contact_t contact2 = 0;

    if( pCE->m_nContactSource == wxContactCellEditor::ContactFromCAB )
    {
        //  Initial info is from the CAB - see if contact also has a PAB record
        if ( CONTROLLER.GetPAB() )
            contact2 = directory_find_contact_by_screenname( CONTROLLER.GetPAB( ), contact->screenname );
    }
    else if( pCE->m_nContactSource == wxContactCellEditor::ContactFromPAB )
    {
        //  Initial info is from the PAB - see if contact also has a CAB record
        contact2 = directory_find_contact_by_screenname( CONTROLLER.GetCAB( ), contact->screenname );
    }

    if( contact )
    {
        if( contact->busphone && *contact->busphone )
            busphone = wxString( contact->busphone, wxConvUTF8 );

        if( contact->homephone && *contact->homephone )
            homephone = wxString( contact->homephone, wxConvUTF8 );

        if( contact->mobilephone && *contact->mobilephone )
            mobilephone = wxString( contact->mobilephone, wxConvUTF8 );

        if( contact->otherphone && *contact->otherphone )
            otherphone = wxString( contact->otherphone, wxConvUTF8 );

        if( contact->email && *contact->email )
        {
            email1 = wxString( contact->email, wxConvUTF8 );
        }

        if( contact->email2 && *contact->email2 )
        {
            email2 = wxString( contact->email2, wxConvUTF8 );
        }

        if( contact->email3 && *contact->email3 )
        {
            email3 = wxString( contact->email3, wxConvUTF8 );
        }
    }

    if( contact2 )
    {
        if( contact2->busphone && *contact2->busphone )
            busphone2 = wxString( contact2->busphone, wxConvUTF8 );

        if( contact2->homephone && *contact2->homephone )
            homephone2 = wxString( contact2->homephone, wxConvUTF8 );

        if( contact2->mobilephone && *contact2->mobilephone )
            mobilephone2 = wxString( contact2->mobilephone, wxConvUTF8 );

        if( contact2->otherphone && *contact2->otherphone )
            otherphone2 = wxString( contact2->otherphone, wxConvUTF8 );

        if( contact2->email && *contact2->email )
        {
            email1_2 = wxString( contact2->email, wxConvUTF8 );
        }

        if( contact2->email2 && *contact2->email2 )
        {
            email2_2 = wxString( contact2->email2, wxConvUTF8 );
        }

        if( contact2->email3 && *contact2->email3 )
        {
            email3_2 = wxString( contact2->email3, wxConvUTF8 );
        }
    }

    // Col_Status - The graphical feedback

    // Col_Contact - The contact name
    invt->name = Alloc_pstring( m_Invitees->GetCellValue( wxSheetCoords(nRow, Col_Contact) ) );

    // Col_Phone
    int nValue = Phone_EnterPhoneValue;
    wxString strValue;
    strValue = m_Invitees->GetCellValue( wxSheetCoords(nRow, Col_Phone) );

    GridCellComboIntEditor *pE = (GridCellComboIntEditor *)m_Invitees->GetAttrEditor( wxSheetCoords(nRow, Col_Phone) ).GetRefData();
    nValue = pE->GetIntValue( strValue );

    // Make sure if selection is PC Phone the phone type is set correctly.
    if ( strValue == _("Use web phone") )
        nValue = Phone_PC;

    invt->notify_type |= NotifyPhone; // Assume there will be calls

    switch( nValue )
    {
    case Phone_DontCall:
        invt->selphone = SelPhoneNone;
        invt->phone = Alloc_pstring( pE->GetCustomValue() );
        invt->notify_type &= ~NotifyPhone; // No call notifications
        break;
    case Phone_FollowMe:
        /**< The default invt.selphone = SelPhoneDefault, invt.phone = The referenced phone */
        invt->selphone = SelPhoneDefault;
        if( contact )
        {
            wxString theDefaultPhone;
            switch( contact->defphone )
            {
            case SelPhoneBus:
                theDefaultPhone = busphone;
                break;
            case SelPhoneHome:
                theDefaultPhone = homephone;
                break;
            case SelPhoneMobile:
                theDefaultPhone = mobilephone;
                break;
            case SelPhoneOther:
                theDefaultPhone = otherphone;
                break;
            case SelPhonePC:
                theDefaultPhone = _("Web phone");
                break;
            }
            invt->phone = Alloc_pstring( theDefaultPhone );
        }
        break;
    case Phone_Business:
        /**< Business #  invt.selphone = SelPhoneThis, invt.phone = contact.busphone */
        invt->selphone = SelPhoneThis;
        invt->phone = Alloc_pstring( busphone );
        break;
    case Phone_Home:
        /**< Home #      invt.selphone = SelPhoneThis, invt.phone = contact.homephone */
        invt->selphone = SelPhoneThis;
        invt->phone = Alloc_pstring( homephone );
        break;
    case Phone_Mobile:
        /**< Mobile #    invt.selphone = SelPhoneThis, invt.phone = contact.mobilephone */
        invt->selphone = SelPhoneThis;
        invt->phone = Alloc_pstring( mobilephone );
        break;
    case Phone_Other:
        /**< Other #     invt.selphone = SelPhoneThis, invt.phone = contact.otherphone */
        invt->selphone = SelPhoneThis;
        invt->phone = Alloc_pstring( otherphone );
        break;
    case Phone_Business2:
        /**< Business #  invt.selphone = SelPhoneThis, invt.phone = 2nd contact.busphone */
        invt->selphone = SelPhoneThis;
        invt->phone = Alloc_pstring( busphone2 );
        break;
    case Phone_Home2:
        /**< Home #      invt.selphone = SelPhoneThis, invt.phone = 2nd contact.homephone */
        invt->selphone = SelPhoneThis;
        invt->phone = Alloc_pstring( homephone2 );
        break;
    case Phone_Mobile2:
        /**< Mobile #    invt.selphone = SelPhoneThis, invt.phone = 2nd contact.mobilephone */
        invt->selphone = SelPhoneThis;
        invt->phone = Alloc_pstring( mobilephone2 );
        break;
    case Phone_Other2:
        /**< Other #     invt.selphone = SelPhoneThis, invt.phone = 2nd contact.otherphone */
        invt->selphone = SelPhoneThis;
        invt->phone = Alloc_pstring( otherphone2 );
        break;
    case Phone_PC:
        /**< PC Phone    invt.selphone = SelPhonePC,   invt.phone = "" */
        invt->selphone = SelPhonePC;
        invt->phone = Alloc_pstring( blank );
        break;
    case Phone_Custom:
        invt->selphone = SelPhoneThis;
        //No change to invt->phone
        break;
    case Phone_EnterPhoneValue:
        /**< User #      invt.selphone = SelPhoneThis, invt.phone = "The custom number" */
        invt->selphone = SelPhoneThis;
        invt->phone = Alloc_pstring( strValue );
        break;
    };

    // Col_Email
    nValue = Email_EnterEmailValue;
    strValue = m_Invitees->GetCellValue( wxSheetCoords(nRow, Col_Email) );
    pE = (GridCellComboIntEditor *)m_Invitees->GetAttrEditor( wxSheetCoords (nRow, Col_Email) ).GetRefData();
    nValue = pE->GetIntValue( strValue );

    invt->notify_type |= NotifyEmail; // Assume there will be email notifications

    switch( nValue )
    {
    case Email_DontEmail:
        /**< Don't Email invt.selemail = SelEmailNone, invt.email = null */
        invt->selemail = SelEmailNone;
        invt->email = Alloc_pstring( pE->GetCustomValue() );
        invt->notify_type &= ~NotifyEmail; // Clear email notifications
        break;
    case Email_1:
        /**< Email 1     invt.selemail = SelEmailThis, invt.email = last chosen address */
        invt->selemail = SelEmailThis;
        invt->email = Alloc_pstring( email1 );
        break;
    case Email_2:
        /**< Email 2     invt.selemail = SelEmailThis, invt.email = last chosen address */
        invt->selemail = SelEmailThis;
        invt->email = Alloc_pstring( email2 );
        break;
    case Email_3:
        /**< Email 3     invt.selemail = SelEmailThis, invt.email = last chosen address */
        invt->selemail = SelEmailThis;
        invt->email = Alloc_pstring( email3 );
        break;
    case Email_1_2:
        /**< Email 1     invt.selemail = SelEmailThis, invt.email = last chosen address, 2nd contact */
        invt->selemail = SelEmailThis;
        invt->email = Alloc_pstring( email1_2 );
        break;
    case Email_2_2:
        /**< Email 2     invt.selemail = SelEmailThis, invt.email = last chosen address, 2nd contact */
        invt->selemail = SelEmailThis;
        invt->email = Alloc_pstring( email2_2 );
        break;
    case Email_3_2:
        /**< Email 3     invt.selemail = SelEmailThis, invt.email = last chosen address, 2nd contact */
        invt->selemail = SelEmailThis;
        invt->email = Alloc_pstring( email3_2 );
        break;
    case Email_Custom:
        invt->selemail = SelEmailThis;
        invt->email = Alloc_pstring( strValue );
        break;
    case Email_EnterEmailValue:
        /**< User email  invt.selemail = SelEmailThis, invt.email = last chosen addreed */
        invt->selemail = SelEmailThis;
        invt->email = Alloc_pstring( strValue );
        break;
    };

    // Col_IM
    pE = (GridCellComboIntEditor *)m_Invitees->GetAttrEditor( wxSheetCoords(nRow, Col_IM) ).GetRefData();
    strValue = m_Invitees->GetCellValue( wxSheetCoords(nRow, Col_IM) );
    nValue = pE->GetIntValue( strValue );

    // Clear all of the IM bits
    int AllIMFlags = NotifyIIC|NotifyIMAny|NotifyAIM|NotifyYahoo|NotifyMessenger;
    invt->notify_type &= ~AllIMFlags;

    // Set the one notification bit.
    invt->notify_type |= nValue;

    // Col_Moderator
    wxSheetCellBoolEditorRefData *pBE = (wxSheetCellBoolEditorRefData *)m_Invitees->GetAttrEditor( wxSheetCoords(nRow, Col_Moderator) ).GetRefData();
    if (pBE && pBE->IsCreated())
        strValue = pBE->GetValue();
    else
        strValue = m_Invitees->GetCellValue( wxSheetCoords(nRow, Col_Moderator) );
    if ( invt->role != RollHost )
    {
#ifdef MOD_CHECK
        invt->role = strValue == _("1") ? RollModerator : RollNormal;
#else
        invt->role = strValue == _("Yes") ? RollModerator : RollNormal;
#endif
    }

    // If this invitee is an ad hoc entry, then make sure the unused fields have default values
    // that the server can deal with
    if( !contact || contact->readonly /*islocal*/ )
    {
        invt->user_id = Alloc_pstring( wxT("") );
    }
}

GridCellComboIntEditor * MeetingSetupDialog::UpdatePhoneCellEditor( GridCellComboIntEditor * pE, invitee_t invt, contact_t contact, const wxString & contactTag, contact_t contact2, const wxString & contactTag2 )
{
    wxASSERT( pE );
    if( !pE )
        return 0;

    wxString invt_phone( invt->phone, wxConvUTF8 );
        
    pE->Reset();
        
    int nComboSelection = Phone_DontCall;

    pE->Append( _("Don't call"), Phone_DontCall );

    if( contact )
    {
        // This invitee has contact information.
        wxString first_name(contact->first, wxConvUTF8 );
        wxString last_name(contact->last, wxConvUTF8 );
        wxString name;

        name = first_name;
        name += wxT(" ");
        name += last_name;

        wxString contact_name;

        // Contact in the system

        if(( contactTag  ==  contactTagText( wxContactCellEditor::ContactFromCAB ))  &&  contact->defphone != SelPhoneNone )
        {
            wxString follow_me_fmt;
            follow_me_fmt = _("Follow me (%s %s)");

            wxString follow_me_type;
            wxString follow_me;
            wxString follow_me_number;

            if( contact->defphone == SelPhoneBus )
            {
                follow_me_type = _("Business");
                follow_me_number = wxString( contact->busphone, wxConvUTF8 );
            }
            else if( contact->defphone == SelPhoneHome )
            {
                follow_me_type = _("Home");
                follow_me_number = wxString( contact->homephone, wxConvUTF8 );
            }
            else if( contact->defphone == SelPhoneMobile )
            {
                follow_me_type = _("Mobile");
                follow_me_number = wxString( contact->mobilephone, wxConvUTF8 );
            }
            else if( contact->defphone == SelPhoneOther )
            {
                follow_me_type = _("Other");
                follow_me_number = wxString( contact->otherphone, wxConvUTF8 );
            }
            else if( contact->defphone == SelPhonePC )
            {
                follow_me_type = _("Web phone");
            }
            if( !follow_me_type.IsEmpty() )
            {
                follow_me = wxString::Format( follow_me_fmt, follow_me_type.c_str(), follow_me_number.c_str() );
                pE->Append( follow_me, Phone_FollowMe );
                if( invt->selphone == SelPhoneDefault )
                {
                    nComboSelection = Phone_FollowMe;
                    invt_phone.Empty( );    //  Don't let anyone overwrite the combo selection we just set
                }
            }
        }

        if( contact->busphone && *contact->busphone )
        {
            wxString business( _("Business") );
            wxString business_phone( contact->busphone, wxConvUTF8 );
            if( invt_phone == business_phone )
            {
                nComboSelection = Phone_Business;
            }
            pE->Append( formatComboEntry( contact->busphone, business, contactTag,  name ), Phone_Business );
        }

        if( contact->homephone && *contact->homephone )
        {
            wxString home( _("Home") );
            wxString home_phone( contact->homephone, wxConvUTF8 );
            if( invt_phone == home_phone )
            {
                nComboSelection = Phone_Home;
            }
            pE->Append( formatComboEntry( contact->homephone, home, contactTag,  name ), Phone_Home );
        }

        if( contact->mobilephone && *contact->mobilephone )
        {
            wxString mobile( _("Mobile") );
            wxString mobile_phone( contact->mobilephone, wxConvUTF8 );
            if( invt_phone == mobile_phone )
            {
                nComboSelection = Phone_Mobile;
            }
            pE->Append( formatComboEntry( contact->mobilephone, mobile, contactTag,  name ), Phone_Mobile );
        }

        if( contact->otherphone && *contact->otherphone )
        {
            wxString other( _("Other") );
            wxString other_phone( contact->otherphone, wxConvUTF8 );
            if( invt_phone == other_phone )
            {
                nComboSelection = Phone_Other;
            }
            pE->Append( formatComboEntry( contact->otherphone, other, contactTag,  name ), Phone_Other );
        }
    }

    if( contact2 )
    {
        // This invitee has contact2 information.
        wxString first_name(contact2->first, wxConvUTF8 );
        wxString last_name(contact2->last, wxConvUTF8 );
        wxString name;

        name = first_name;
        name += wxT(" ");
        name += last_name;

        wxString contact_name;

        // Contact2 in the system
        if(( contactTag2  ==  contactTagText( wxContactCellEditor::ContactFromCAB ))  &&  contact2->defphone != SelPhoneNone )
        {
            wxString follow_me_fmt;
            follow_me_fmt = _("Follow me (%s %s)");

            wxString follow_me_type;
            wxString follow_me;
            wxString follow_me_number;

            if( contact2->defphone == SelPhoneBus )
            {
                follow_me_type = _("Business");
                follow_me_number = wxString( contact2->busphone, wxConvUTF8 );
            }
            else if( contact2->defphone == SelPhoneHome )
            {
                follow_me_type = _("Home");
                follow_me_number = wxString( contact2->homephone, wxConvUTF8 );
            }
            else if( contact2->defphone == SelPhoneMobile )
            {
                follow_me_type = _("Mobile");
                follow_me_number = wxString( contact2->mobilephone, wxConvUTF8 );
            }
            else if( contact2->defphone == SelPhoneOther )
            {
                follow_me_type = _("Other");
                follow_me_number = wxString( contact2->otherphone, wxConvUTF8 );
            }
            else if( contact2->defphone == SelPhonePC )
            {
                follow_me_type = _("Web phone");
            }
            if( !follow_me_type.IsEmpty() )
            {
                follow_me = wxString::Format( follow_me_fmt, follow_me_type.c_str(), follow_me_number.c_str() );
                pE->Append( follow_me, Phone_FollowMe );
                if( invt->selphone == SelPhoneDefault )
                {
                    nComboSelection = Phone_FollowMe;
                    invt_phone.Empty( );    //  Don't let anyone overwrite the combo selection we just set
                }
            }
        }

        if( contact2->busphone && *contact2->busphone )
        {
            wxString business( _("Business") );
            wxString business_phone( contact2->busphone, wxConvUTF8 );
            if( invt_phone == business_phone )
            {
                nComboSelection = Phone_Business2;
            }
            pE->Append( formatComboEntry( contact2->busphone, business, contactTag2,  name ), Phone_Business2 );
        }

        if( contact2->homephone && *contact2->homephone )
        {
            wxString home( _("Home") );
            wxString home_phone( contact2->homephone, wxConvUTF8 );
            if( invt_phone == home_phone )
            {
                nComboSelection = Phone_Home2;
            }
            pE->Append( formatComboEntry( contact2->homephone, home, contactTag2,  name ), Phone_Home2 );
        }

        if( contact2->mobilephone && *contact2->mobilephone )
        {
            wxString mobile( _("Mobile") );
            wxString mobile_phone( contact2->mobilephone, wxConvUTF8 );
            if( invt_phone == mobile_phone )
            {
                nComboSelection = Phone_Mobile2;
            }
            pE->Append( formatComboEntry( contact2->mobilephone, mobile, contactTag2,  name ), Phone_Mobile2 );
        }

        if( contact2->otherphone && *contact2->otherphone )
        {
            wxString other( _("Other") );
            wxString other_phone( contact2->otherphone, wxConvUTF8 );
            if( invt_phone == other_phone )
            {
                nComboSelection = Phone_Other2;
            }
            pE->Append( formatComboEntry( contact2->otherphone, other, contactTag2,  name ), Phone_Other2 );
        }
    }

    // Even though adhoc participant can't be directly called, the setting will be remembered when the user joins the meeting
    // and his PC Phone will automatically be connected.
    pE->Append( _("Use web phone"), Phone_PC );

    if( invt->selphone == SelPhonePC || invt_phone == _("Web phone") )
    {
        if ( invt->notify_type & NotifyPhone )
            nComboSelection = Phone_PC;
    }
    else if( invt->selphone == SelPhoneThis && nComboSelection == Phone_DontCall && !invt_phone.IsEmpty() )
    {
        // Must be a custom number
        wxString lastSelectedPhone( invt->phone, wxConvUTF8 );
        lastSelectedPhone += wxT(" ");
        lastSelectedPhone += _("(Last selected)");
        pE->Append( lastSelectedPhone, Phone_EnterPhoneValue );
        if (invt->notify_type & NotifyPhone)
            nComboSelection = Phone_EnterPhoneValue;
    }
    else if ( invt->selphone == SelPhoneNone && !invt_phone.IsEmpty() )
    {
        pE->Append( invt_phone, Phone_EnterPhoneValue );
    }

    // NOTE: We are enabling text entry into the combobox, but in case a user doesn't
    // figure out that option, also add an explicit choice on the drop down to enter a new number
    bool fUseCustomPhoneNumberEntry = true;
    if( fUseCustomPhoneNumberEntry )
    {
        // The user selected an "Enter #" item from the combo box.
        // A dialog pops up asking for the new phone number.
        pE->Append( _("Enter phone # ..."), Phone_EnterPhone );
    }

    pE->SetupCustomEntry( this, _("Meeting Setup"), _("Phone number:"), Phone_EnterPhone, Phone_EnterPhoneValue );

    pE->SetValue( nComboSelection );

    return pE;
}

wxString MeetingSetupDialog::formatComboEntry( pstring value, const wxString & type, const wxString & contactTag, wxString & name )
{
    wxString ret(wxT(""));

    wxString strValue(wxT(""));

    if( value && *value )
    {
        strValue = wxString(value,wxConvUTF8);
    }
    ret = wxString::Format(wxT("%s (%s %s %s)"),
                           strValue.c_str(),
                           type.c_str(),
                           contactTag.c_str(),
                           name.c_str()
                          );
    return ret;
}

GridCellComboIntEditor * MeetingSetupDialog::UpdateEmailCellEditor( GridCellComboIntEditor * pE, invitee_t invt, contact_t contact, const wxString & contactTag, contact_t contact2, const wxString & contactTag2 )
{
    wxASSERT( pE );
    if( !pE )
        return 0;
        
    pE->Reset();
    
    int nComboSelection = Email_DontEmail;
    wxString invt_email( invt->email, wxConvUTF8 );

    if( invt->selemail == SelEmailThis && !invt_email.IsEmpty() )
    {
        pE->Append( invt_email, Email_EnterEmailValue );
        nComboSelection = Email_EnterEmailValue;
    }

    pE->Append( _("Don't Email"), Email_DontEmail );

    if( contact )
    {
        wxString first_name(contact->first, wxConvUTF8 );
        wxString last_name(contact->last, wxConvUTF8 );
        wxString name;
        name = first_name;
        name += wxT(" ");
        name += last_name;
        wxString email1_value( contact->email, wxConvUTF8 );
        wxString email2_value( contact->email2, wxConvUTF8 );
        wxString email3_value( contact->email3, wxConvUTF8 );

        if( !email1_value.IsEmpty() )
        {
            wxString email1( _("Email 1") );
            pE->Append( formatComboEntry( contact->email, email1, contactTag,  name ), Email_1 );
            if( invt_email.CmpNoCase( email1_value ) == 0 )
                nComboSelection = Email_1;
        }

        if( !email2_value.IsEmpty() )
        {
            wxString email2( _("Email 2") );
            pE->Append( formatComboEntry( contact->email2, email2, contactTag,  name ), Email_2 );
            if( invt_email.CmpNoCase( email2_value ) == 0 )
                nComboSelection = Email_2;
        }

        if( !email3_value.IsEmpty() )
        {
            wxString email3( _("Email 3") );
            pE->Append( formatComboEntry( contact->email3, email3, contactTag,  name ), Email_3 );
            if( invt_email.CmpNoCase( email3_value ) == 0)
                nComboSelection = Email_3;
        }
    }

    if( contact2 )
    {
        wxString first_name(contact2->first, wxConvUTF8 );
        wxString last_name(contact2->last, wxConvUTF8 );
        wxString name;
        name = first_name;
        name += wxT(" ");
        name += last_name;
        wxString email1_value( contact2->email,  wxConvUTF8 );
        wxString email2_value( contact2->email2, wxConvUTF8 );
        wxString email3_value( contact2->email3, wxConvUTF8 );

        if( !email1_value.IsEmpty() )
        {
            wxString email1( _("Email 1") );
            pE->Append( formatComboEntry( contact2->email, email1, contactTag2,  name ), Email_1_2 );
            if( invt_email.CmpNoCase( email1_value ) == 0 )
                nComboSelection = Email_1_2;
        }

        if( !email2_value.IsEmpty() )
        {
            wxString email2( _("Email 2") );
            pE->Append( formatComboEntry( contact2->email2, email2, contactTag2,  name ), Email_2_2 );
            if( invt_email.CmpNoCase( email2_value ) == 0 )
                nComboSelection = Email_2_2;
        }

        if( !email3_value.IsEmpty() )
        {
            wxString email3( _("Email 3") );
            pE->Append( formatComboEntry( contact2->email3, email3, contactTag2,  name ), Email_3_2 );
            if( invt_email.CmpNoCase( email3_value ) == 0)
                nComboSelection = Email_3_2;
        }
    }

    if( invt->selemail == SelEmailNone && !invt_email.IsEmpty() )
    {
        wxString strEmail( invt_email );
        pE->Append( strEmail, Email_EnterEmailValue );
    }

    pE->Append( _("Enter Email ..."), Email_EnterEmail );

    pE->SetupCustomEntry( this, _("Meeting Setup"), _("Email:"), Email_EnterEmail, Email_EnterEmailValue );

    pE->SetValue( nComboSelection );

    return pE;
}

GridCellComboIntEditor * MeetingSetupDialog::UpdateIMCellEditor( GridCellComboIntEditor * pE, invitee_t invt, contact_t contact, const wxString & contactTag )
{
    wxASSERT( pE );
    if( !pE )
        return 0;
        
    pE->Reset();
    
    int nComboSelection = 0;
    bool force_yahoo = (invt->notify_type & NotifyYahoo) != 0;
    bool force_messenger = (invt->notify_type & NotifyMessenger ) != 0;
    bool force_aim = (invt->notify_type & NotifyAIM) != 0;
    bool yahoo_screen = false;
    bool messenger_screen = false;
    bool aim_screen = false;
    if( contact )
    {
        yahoo_screen = ( contact->yahooscreen && *contact->yahooscreen );
        messenger_screen = ( contact->msnscreen && *contact->msnscreen );
        aim_screen = ( contact->aimscreen && *contact->aimscreen );
    }

    wxString open_zon( PRODNAME );
    wxString messenger( _("Messenger") );
    wxString yahoo( _("Yahoo") );
    wxString aim( _("AIM") );

    wxString available( _(" (Available)") );
    wxString not_available( _(" (Not available)") );

    wxString choice;

    pE->Append( _("Don't Send"), NotifyNone );

    pE->Append( _("Any online"), NotifyIMAny );

//    if( contact )
    {
        pE->Append( open_zon, NotifyIIC );
    }

    if( force_messenger || messenger_screen )
    {
        choice = messenger;
        choice += not_available;
        pE->Append( choice, NotifyMessenger );
    }

    if( force_yahoo || yahoo_screen )
    {
        choice = yahoo;
        choice += not_available;
        pE->Append( choice, NotifyYahoo );
    }

    if( force_aim || aim_screen )
    {
        choice = aim;
        choice += not_available;
        pE->Append( choice, NotifyAIM );
    }

    if( invt->notify_type & NotifyIMAny )
        nComboSelection = NotifyIMAny;
    else if( invt->notify_type & NotifyIIC )
        nComboSelection = NotifyIIC;
    else if( invt->notify_type & NotifyMessenger )
        nComboSelection = NotifyMessenger;
    else if( invt->notify_type & NotifyYahoo )
        nComboSelection = NotifyYahoo;
    else if( invt->notify_type & NotifyAIM )
        nComboSelection = NotifyAIM;
    else
        nComboSelection = NotifyNone;

    pE->SetValue( nComboSelection );

    return pE;
}

void MeetingSetupDialog::GUI_to_Value()
{
    // m_mtg_TO Contains the data

	//int     privacy;                    wxRadioButton* m_PrivacyPrivateRadio, m_PrivacyUnlistedRadio, m_PrivacyPublicRadio;
	if( m_PrivacyPublicRadio->GetValue() ) m_mtg_TO->privacy = PrivacyPublic;
	else if( m_PrivacyUnlistedRadio->GetValue() ) m_mtg_TO->privacy = PrivacyUnlisted;
	else m_mtg_TO->privacy = PrivacyPrivate;

	//pstring title;                      wxTextCtrl m_MeetingTitle
    m_mtg_TO->title = Alloc_pstring( m_MeetingTitle->GetValue() );

	//int     start_time;                 wxDatePickerCtrl* m_StartDate, wxTextCtrl* m_StartTime
    m_mtg_TO->start_time = GUI_to_DateTime( m_StartDate, m_StartTime );

    // Note: voice + data means neither bit should be set
    bool voice = m_VoiceOnlyRadio->GetValue();
    bool data  = m_DataShareRadio->GetValue();

    bool        fTelAvail;
    fTelAvail = CONTROLLER.IsTelephonyAvailable( );
    if( fTelAvail )
    {
        if ( voice )
        {
            m_mtg_TO->options |= InvitationVoiceOnly;
        }
        else
        {
            m_mtg_TO->options &= ~InvitationVoiceOnly;
        }

        if ( data )
        {
            m_mtg_TO->options |= InvitationDataOnly;
        }
        else
        {
            m_mtg_TO->options &= ~InvitationDataOnly;
        }

        m_mtg_TO->options = GetOption( m_mtg_TO->options, Prefer1To1, m_OneToOneCheck );

        m_mtg_TO->options = GetOption( m_mtg_TO->options, ScreenCallers, m_RollCallCheck );

        m_mtg_TO->options = GetOption( m_mtg_TO->options, LectureMode, m_LectureModeCheck );

        m_mtg_TO->options = GetOption( m_mtg_TO->options, HoldMode, m_HoldModeCheck );
    }
    else
    {
        //  Telephony is not available, so set to DataOnly and clear Prefer1To1
        m_mtg_TO->options &= ~InvitationVoiceOnly;
        m_mtg_TO->options |= InvitationDataOnly;
        m_mtg_TO->options &= (~Prefer1To1);
    }

    m_mtg_TO->options = GetOption( m_mtg_TO->options, ContinueWithoutModerator, m_MeetingContinueCheck );
	m_mtg_TO->options = GetOption( m_mtg_TO->options, ModeratorMayEdit, m_ModeratorEditCheck );
    m_mtg_TO->options = GetOption( m_mtg_TO->options, RecordingMeeting, m_RecordingCheck );
    m_mtg_TO->options = GetOption( m_mtg_TO->options, IncludeDocsInRecording, m_IncludeDocsInRecordingCheck );

    //NOTE: Checking this disables: m_InvalidateHoursSpin
	m_mtg_TO->options = GetOption( m_mtg_TO->options, InviteNeverExpires, m_InvalidateInstantPinCheck );

	//int     host_enabled_features;      (n/a)

	//pstring description;                wxTextCtrl* m_MeetingDescription
	m_mtg_TO->description = Alloc_pstring( m_MeetingDescription->GetValue() );

	//int     end_time;                   wxDatePickerCtrl* m_EndDate, wxTextCtrl* m_EndTime;
	m_mtg_TO->end_time = GUI_to_DateTime( m_EndDate, m_EndTime );

	//int     invite_expiration;          wxSpinCtrl* m_InvalidateHoursSpin;
	m_mtg_TO->invite_expiration = m_InvalidateHoursSpin->GetValue() * 3600;     // stored as seconds

	//int     invite_valid_before;        (n/a)

	//pstring invite_message;             m_MeetingInviteMessage
    m_mtg_TO->invite_message = Alloc_pstring( m_MeetingInviteMessage->GetValue() );

	//int     display_attendees;          wxRadioButton* m_DisplayNoneRadio, m_DisplayModeratorsRadio, m_DisplayAllRadio
    if( m_DisplayNoneRadio->GetValue() ) m_mtg_TO->display_attendees = DisplayNone;
    else if( m_DisplayModeratorsRadio->GetValue() ) m_mtg_TO->display_attendees = DisplayModerators;
    else if( m_DisplayAllRadio->GetValue() ) m_mtg_TO->display_attendees = DisplayAll;

	//int     chat_options;               wxRadioButton* m_ChatNoneRadio, m_ChatModeratorsRadio, m_ChatAllRadio
	if( m_ChatNoneRadio->GetValue() ) m_mtg_TO->chat_options = ChatNone;
	else if( m_ChatModeratorsRadio->GetValue() ) m_mtg_TO->chat_options = ChatModerators;
    else if( m_ChatAllRadio->GetValue() ) m_mtg_TO->chat_options = ChatAll;

	//pstring password;                   wxTextCtrl* m_MeetingPasswordText
	m_mtg_TO->password = Alloc_pstring( m_MeetingPasswordText->GetValue() );

    // Now grab the invitee record data from the screen
    invitee_iterator_t iter;
    invitee_t invt;

    iter = meeting_details_iterate(m_mtg_TO);
    int nRow = 0;
    while ((invt = meeting_details_next(m_mtg_TO, iter)) != NULL)
    {
        GUI_to_Invitee( nRow++, invt );
    }

    // Remove any participants that the user added but didn't change text for
    iter = meeting_details_iterate(m_mtg_TO);
    while ((invt = meeting_details_next(m_mtg_TO, iter)) != NULL)
    {
        wxString name(invt->name, wxConvUTF8);
        if (name == kNewInvitee)
        {
            meeting_details_remove(m_mtg_TO, invt);
            iter = meeting_details_iterate(m_mtg_TO);
        }
    }

    wxLogDebug(wxT("============================================="));
    wxLogDebug(wxT("After GUI_to_Value()"));

#ifdef DEBUG_MEETINGSETUPDIALOG
    dump_meeting( m_mtg_TO );
#endif

}

meeting_details_t MeetingSetupDialog::GetMeetingDetails()
{
    meeting_details_t mtgRet = m_mtg_TO;

    // The caller has asked for the meeting details.
    // The caller now owns the memory. OK to zero the pointer.
    m_mtg_TO = 0;

    CleanupMeetingResources(); // Except m_mtg_TO of course

    return mtgRet;
}

void MeetingSetupDialog::CleanupMeetingResources()
{
    if( m_mtg_FROM )
    {
        meeting_details_destroy( m_mtg_FROM );
        m_mtg_FROM = 0;
    }

    if( m_mtg_TO )
    {
        meeting_details_destroy( m_mtg_TO );
        m_mtg_TO = 0;
    }
}

void MeetingSetupDialog::DateTime_to_GUI( time_t time, wxDatePickerCtrl *pD, wxTextCtrl *pT, wxDateTime &theTime, int iTimeOffset )
{
    time_t      ticks = time;

    if( 0 == time )
    {
        ticks = wxDateTime::Now().GetTicks();

        theTime.Set( ticks + iTimeOffset );
        theTime.SetMillisecond( 0 );
        theTime.SetSecond( 0 );
        int     iMin, iHour;
        iMin  = theTime.GetMinute( );
        iHour = theTime.GetHour( );
        if( iMin != 0  &&  iMin != 30 )
        {
            //  Round up to next half-hour
            if( iMin < 30 )
                iMin = 30;
            else
            {
                iMin = 0;
                iHour++;
            }
            if (iHour == 24) iHour = 0;

            theTime.SetMinute( iMin );
            theTime.SetHour( iHour );
        }
    }
    else
        theTime.Set( ticks + iTimeOffset );

    // Note: Need to set the time value first
    pT->SetValue( theTime.Format( wxT("%I:%M %p") ) );

    // There is a bogus ASSERT in wxDateTime::GetValue()
    // This hack bypasses the ASSERT:
    // Setting the HH:MM:SS.ms values to zero for the date picker
    wxDateTime  dtT     = theTime;
    dtT.SetHour(0);
    dtT.SetMinute(0);
    dtT.SetSecond(0);
    dtT.SetMillisecond(0);
    /*
        THE BOGUS ASSERT:
        wxDateTime wxDatePickerCtrl::GetValue() const
        {
        #ifdef __WXDEBUG__
            wxDateTime dt;
            SYSTEMTIME st;
            if ( DateTime_GetSystemtime(GetHwnd(), &st) == GDT_VALID )
            {
                wxFromSystemTime(&dt, st);
            }

            wxASSERT_MSG( m_date.IsValid() == dt.IsValid() &&
                            (!dt.IsValid() || dt == m_date),
                          _T("bug in wxDatePickerCtrl: m_date not in sync") );
        #endif // __WXDEBUG__

            return m_date;
        }
    */
    pD->SetValue( dtT );

    pD->Enable( time != 0 );
    pT->Enable( time != 0 );
}

time_t MeetingSetupDialog::GUI_to_DateTime( wxDatePickerCtrl *pD, wxTextCtrl *pT )
{
    if( m_NoMeetingTimeCheck->GetValue() )
    {
        // Special case. No date / time are specified
        return 0;
    }
    else
    {
        wxDateTime theDateTime;
        theDateTime = pD->GetValue();
        theDateTime.ParseTime( pT->GetValue().c_str() );
        return theDateTime.GetTicks();
    }
}

void MeetingSetupDialog::SetOption( int options, int mask, wxCheckBox *pCheckBox )
{
    if( (options & mask) != 0 )
    {
        pCheckBox->SetValue( true );
    }
    else
    {
        pCheckBox->SetValue( false );
    }

}

int MeetingSetupDialog::GetOption( int options, int mask, wxCheckBox *pCheckBox )
{
    int retOptions = options;

    if( pCheckBox->GetValue() )
    {
        retOptions |= mask;     // Set
    }
    else
    {
        retOptions &= (~mask);  // Clear
    }
    return retOptions;
}

pstring MeetingSetupDialog::Alloc_pstring( const wxString & strValue )
{
    return meeting_details_string( m_mtg_TO, strValue.mb_str( wxConvUTF8) );
}

pstring MeetingSetupDialog::Alloc_pstring( const char * szValue )
{
    return meeting_details_string( m_mtg_TO, szValue );
}

void MeetingSetupDialog::OnGridCellChanged(wxSheetEvent& event)
{
    if (event.GetCol() == Col_Contact)
    {
        int row = event.GetRow();
        wxContactCellEditor *pCE = ContactEditorFromRow( row );

        wxString gridName = m_Invitees->GetCellValue( wxSheetCoords(row, Col_Contact) );
        wxString contactName((pCE->m_contact && pCE->m_contact->displayname) ? pCE->m_contact->displayname : "", wxConvUTF8);

        // See if user entered or changed name
        if (pCE->m_contact == NULL || (pCE->m_contact != NULL && contactName != gridName))
        {
            bool isCAB;
            contact_t contact = LookupContact(wxT("displayname"), gridName, isCAB);
            if (contact)
            {
                pCE->SetContact(contact);

                // Copy data from contact to invitee record as well
                pCE->m_invt->is_adhoc = FALSE;
                pCE->m_invt->user_id = contact->userid;
                pCE->m_invt->name = Alloc_pstring( gridName );
                pCE->m_invt->screenname = contact->screenname;

                if (contact->directory == CONTROLLER.GetCAB())
                {
                    pCE->SetFromCAB();
                }
                else if (contact->directory == CONTROLLER.GetPAB())
                {
                    pCE->SetFromPAB();
                }

                // Populate screen controls
                Invitee_to_GUI(row);
            }
        }
    }
    
    event.Skip();
}

bool MeetingSetupDialog::EditRow( int row )
{
    bool fAccepted = false;

    invitee_t invt = InviteeFromRow( row );
    contact_t contact = ContactFromRow( row );

    if( invt->is_adhoc )
    {
        CreateContactDialog Dlg( this, wxID_ANY );
        if( Dlg.EditNewAdHoc( CONTROLLER.GetPAB(), m_mtg_TO, invt, contact ) )
        {
            fAccepted = true;
            if( strlen( invt->phone )  >  0 )
                invt->notify_type  |= NotifyPhone;
            if( strlen( invt->email )  >  0 )
                invt->notify_type  |= NotifyEmail;
            Invitee_to_GUI( row );
        }
    }
    else if( !invt->is_adhoc && contact )
    {
        CreateContactDialog Dlg( this, wxID_ANY );
        if( Dlg.EditNewContact( CONTROLLER.GetPAB(), m_mtg_TO, invt, contact ) )
        {
            fAccepted = true;
            Invitee_to_GUI( row );
        }
    }
    return fAccepted;
}

void MeetingSetupDialog::OnNoMeetingTimeCheck(wxCommandEvent& event)
{
    bool fEnable = !m_NoMeetingTimeCheck->GetValue();

    m_StartDate->Enable( fEnable );
	m_StartTime->Enable( fEnable );
	m_EndDate->Enable( fEnable );
	m_EndTime->Enable( fEnable );
}


wxArrayInt MeetingSetupDialog::GetSelectedInvitees() const
{
    wxArrayInt rows;

    if( m_Invitees->GetSelectionMode() == wxSHEET_SelectRows )
    {
        for( int row = 0; row < m_Invitees->GetNumberRows(); ++row )
        {
            if( m_Invitees->IsRowSelected( row ) )
            {
                rows.Add( row );
            }
        }
    }
    else
    {
        for( int row = 0; row < m_Invitees->GetNumberRows(); ++row )
        {
            for( int col = 0; col < m_Invitees->GetNumberCols(); ++col )
            {
                if( m_Invitees->IsCellSelected( wxSheetCoords( row,col ) ) )
                {
                    rows.Add( row );
                    break; // One col selected implies row selection
                }
            }
        }
    }

    return rows;
}

void MeetingSetupDialog::OnInvidateInstantPinCheckClick(wxCommandEvent& event)
{
    m_InvalidateHoursSpin->Enable( !m_InvalidateInstantPinCheck->GetValue() );
}

void MeetingSetupDialog::OnChooseInvitee( wxCommandEvent & event )
{
    // If the SelectContactDialog is currently displayed
    if( m_pContactDlg && m_pContactDlg->IsShown() )
    {

    }
}

void MeetingSetupDialog::OnEditInvitee( wxCommandEvent & event )
{
    wxArrayInt selectedRows;
    selectedRows = GetSelectedInvitees();
    if( selectedRows.GetCount() == 1 )
    {
        EditRow( selectedRows[0] );
    }
}

void MeetingSetupDialog::OnRemoveInvitee( wxCommandEvent & event )
{
}

wxContactCellEditor * MeetingSetupDialog::ContactEditorFromRow( int row )
{
    wxContactCellEditor *pCE = (wxContactCellEditor *)m_Invitees->GetAttrEditor( wxSheetCoords(row,Col_Contact)).GetRefData();
    return pCE;
}

invitee_t MeetingSetupDialog::InviteeFromRow( int row )
{
    invitee_t ret = 0;
    wxContactCellEditor *pE = ContactEditorFromRow( row );
    if( pE )
    {
        ret = pE->m_invt;
    }
    return ret;
}

contact_t MeetingSetupDialog::ContactFromRow( int row )
{
    contact_t ret = 0;
    wxContactCellEditor *pE = ContactEditorFromRow( row );
    if( pE )
    {
        ret = pE->m_contact;
    }
    return ret;
}

void MeetingSetupDialog::OnScheduleUpdated(wxCommandEvent& event)
{
    wxLogDebug(wxT("MeetingSetupDialog::OnScheduleUpdated"));

    wxString opcode = event.GetString().BeforeFirst(wxT('|'));
    wxString meetingid = event.GetString().AfterFirst(wxT('|'));

    if( !CONTROLLER.IsOpcode( opcode, MeetingCenterController::OP_EditMeeting ) )
    {
        wxLogDebug(wxT("   MeetingSetupDialog: NOT From OP_EditMeeting - Skipping event"));
        event.Skip();
    }

    // This one is ours
    if (event.GetInt() == 0)
    {
        if (m_meetingId.IsEmpty() && m_fUploadedDocs)
        {
            // Need to copy docs from temp directory to new meeting
            MoveTempDocuments(meetingid);
            m_fUploadedDocs = false;
        }
        if (m_meetingId.IsEmpty() && m_FileManager && m_FileManager->IsDirty())
        {
            MoveTempImages(meetingid);
        }
    }

    // Done
    m_fDataSaved = true;
    m_strDatabaseError = wxT("");

    // Now we can end the dialog
    EndModal( wxID_OK );
}

void MeetingSetupDialog::OnScheduleError(wxCommandEvent& event)
{
    wxLogDebug(wxT("MeetingSetupDialog::OnScheduleError"));

    wxString str;
    str = event.GetString();
    int nd = str.Find('|');
    wxString opid;
    wxString msg;
    if( nd >= 0 )
    {
       opid = str.Left(nd);
        msg =  str.Mid(nd+1);
    }
    else
    {
        opid = wxT("");
        msg = str;
    }

    if( !CONTROLLER.IsOpcode( opid, MeetingCenterController::OP_EditMeeting ) )
        event.Skip();

    // This one is ours

    if( m_nState & State_WaitingForMeetingDetails )
    {
        m_strDatabaseError = _("Unable to locate the meeting data:");
        m_strDatabaseError += wxT("\n");
        m_strDatabaseError += _("It is possible that the meeting has been deleted.");

        m_nState |= State_AbortingProcess;
        ::wxMessageBox( m_strDatabaseError, m_strCaption, wxOK, this );

        // Click Cancel for the user
        wxEvtHandler *e = GetEventHandler();
        if( e )
        {
            wxCommandEvent event( wxEVT_COMMAND_BUTTON_CLICKED, wxID_CANCEL );
            e->AddPendingEvent( event );
        }
    }
    else
    {
        m_fDataSaved = false;
        m_strDatabaseError = _("Unable to save the meeting data:");
        m_strDatabaseError += wxT("\n");
        m_strDatabaseError += msg;  // todo: server error prob. not appropriate to show
        ::wxMessageBox( m_strDatabaseError, m_strCaption, wxOK, this );
    }

}

void MeetingSetupDialog::OnSessionError(SessionErrorEvent& event)
{
    wxString error_msg;
    wxString rpc_id;
    wxString opid;
    error_msg = event.GetMessage();
    rpc_id = event.GetRpcId();
    opid = event.GetOpid();

    wxLogDebug(wxT("MeetingSetupDialog::OnSessionError opid=%s msg=%s"),opid.c_str(),error_msg.c_str() );

    // This one is ours
    if( m_nState & State_WaitingForMeetingDetails )
    {
        m_nState |= State_AbortingProcess;

#if 0
        // Click Cancel for the user
        wxEvtHandler *e = GetEventHandler();
        if( e )
        {
            wxCommandEvent event( wxEVT_COMMAND_BUTTON_CLICKED, wxID_CANCEL );
            e->AddPendingEvent( event );
        }
#endif
    }
    
    event.Skip();
}

void MeetingSetupDialog::OnAddressbkUpdateFailed(wxCommandEvent& event)
{
    wxString str;
    str = event.GetString();
    int nd = str.Find('|');
    wxString opid;
    wxString msg;
    if( nd >= 0 )
    {
        opid = str.Left(nd);
        msg =  str.Mid(nd+1);
    }
    else
    {
        opid = wxT("");
        msg = str;
    }

    // This one is ours
    wxLogDebug(wxT("MeetingSetupDialog::OnAddressbkUpdateFailed opid=%s, msg=%s"),opid.c_str(),msg.c_str());

    if( !CONTROLLER.IsOpcode( opid, MeetingCenterController::OP_EditMeeting ) )
        event.Skip();

}

void MeetingSetupDialog::OnAddressUpdated(wxCommandEvent& event)
{
    wxString str;
    str = event.GetString();
    int nd = str.Find('|');
    wxString opid;
    wxString itemid;
    if( nd >= 0 )
    {
        opid = str.Left(nd);
        itemid =  str.Mid(nd+1);
    }
    else
    {
        opid = wxT("");
        itemid = str;
    }
    wxLogDebug(wxT("MeetingSetupDialog: Address Book Updated: opid=%s, itemid=%s"),
        opid.c_str(), itemid.c_str() );

    DecrPendingContactAdd();

    wxLogDebug(wxT("   m_nPendingNewContacts is now %d"),m_nPendingNewContacts);

    event.Skip();
}

void MeetingSetupDialog::ForceNoSave( )
{
    m_mtg_TO = NULL;
    m_nState |= State_AbortingProcess;
}


void MeetingSetupDialog::OnCancel( wxCommandEvent& event )
{
    wxLogDebug(wxT("MeetingSetupDialog::OnCancel"));
    bool fAborting = false;

    if (m_mtg_TO == NULL)
    {
        // Perhaps server never returned meeting details...abort rest of processing if so.
        m_nState |= State_AbortingProcess;
    }
    else
    {
        //  Now is the time to delete those invitees the user indicated earlier
        if( m_arrInviteesToDelete.GetCount( )  >  0 )
        {
            //  First, reverse sort the array
            m_arrInviteesToDelete.Sort( ReverseSortInvitees );

            //  Second, remove the invitees from m_mtg_TO, skipping duplicate values
            int         i, iCount;
            invitee_t   invLast = NULL;
            for( i=0, iCount = (int)m_arrInviteesToDelete.GetCount( ); i<iCount; i++ )
            {
                invitee_t   invCurr = m_arrInviteesToDelete.Item( i );
                if( invLast  !=  invCurr )
                {
                    meeting_details_remove( m_mtg_TO, invCurr );
                }
                invLast = invCurr;
            }
        }
    }

    if( (m_nState & State_AbortingProcess) != 0)
    {
        wxLogDebug(wxT("Aborting process"));
        fAborting = true;
    }

    // Grab the screen data
    if( !fAborting )
    {
        GUI_to_Value();
    }

    // Check to see if the user has changed any data
    if( m_fCommitOnOK && !fAborting && DataHasChanged() )
    {
        wxString question;
        question = _("This meeting has changed.\nDo you wish to save your changes ?");

        wxMessageDialog AskTheUser( this, question, m_strCaption, wxYES_NO|wxCANCEL|wxICON_EXCLAMATION );
        int nResp = AskTheUser.ShowModal();
        if( nResp == wxID_YES )
        {
            wxLogDebug(wxT("   Yes clicked. Initiate OK processing."));
            event.Skip();// Turn Cancel into OK

            wxEvtHandler *e = GetEventHandler();
            if( e )
            {
                wxCommandEvent event( wxEVT_COMMAND_BUTTON_CLICKED, wxID_OK );
                e->AddPendingEvent( event );
            }
        }
        else if( nResp == wxID_CANCEL )
        {
            // We eat the Cancel click
            wxLogDebug(wxT("   Cancel clicked. Do Nothing."));
        }
        else
        {
            wxLogDebug(wxT("   No clicked. Exit dialog."));
            event.Skip(); // Let the cancel continue;
        }
    }
    else
    {
        event.Skip(); // No data changed. Cancel is OK...
    }
    
    if (event.GetSkipped())
    {
        if (m_meetingId.IsEmpty() && m_fUploadedDocs)
        {
            // Remove temp uploaded docs
            MoveTempDocuments(wxT(""));
            m_fUploadedDocs = false;
        }
    }
}

#define CMP_PSTRING( PS1, PS2, msg ) if( cmp_pstring( PS1, PS2 ) != 0 ) { ++nChangeCount; wxLogDebug(wxT(msg)); }
#define CMP_INTEGER( INT1, INT2, msg ) if( INT1 != INT2 ) { ++nChangeCount; wxLogDebug(wxT(msg)); }

bool MeetingSetupDialog::DataHasChanged()
{    int nChangeCount = 0;
    bool fDone = false;

    if( !m_mtg_FROM || !m_mtg_TO )
    {
        return TRUE;
    }
    wxLogDebug(wxT("DataHaseChanged - Start"));

    while( !fDone )
    {
        CMP_INTEGER(m_mtg_FROM->privacy, m_mtg_TO->privacy,"privacy");
        CMP_PSTRING(m_mtg_FROM->title, m_mtg_TO->title,"title");
        CMP_INTEGER(m_mtg_FROM->start_time, m_mtg_TO->start_time,"start_time");
        CMP_INTEGER(m_mtg_FROM->options, m_mtg_TO->options, "options" );
        CMP_INTEGER(m_mtg_FROM->host_enabled_features,m_mtg_TO->host_enabled_features,"options");
        CMP_PSTRING(m_mtg_FROM->description, m_mtg_TO->description, "description");
        CMP_INTEGER(m_mtg_FROM->end_time, m_mtg_TO->end_time, "end_time" );
        CMP_INTEGER(m_mtg_FROM->invite_expiration, m_mtg_TO->invite_expiration, "invite_expiration" );
        CMP_INTEGER(m_mtg_FROM->invite_valid_before, m_mtg_TO->invite_valid_before, "invite_valid_before" );
        CMP_PSTRING(m_mtg_FROM->invite_message, m_mtg_TO->invite_message, "invite_message" );
        CMP_INTEGER(m_mtg_FROM->display_attendees, m_mtg_TO->display_attendees, "display_attendees" );
        CMP_INTEGER(m_mtg_FROM->chat_options, m_mtg_TO->chat_options, "chat_options" );
        CMP_PSTRING(m_mtg_FROM->password, m_mtg_TO->password, "password" );
        CMP_PSTRING(m_mtg_FROM->bridge_phone, m_mtg_TO->bridge_phone, "bridge_phone" );
        CMP_PSTRING(m_mtg_FROM->system_url, m_mtg_TO->system_url, "system_url" );

        invitee_t I1, I2;
        invitee_iterator_t iter1 = meeting_details_iterate(m_mtg_FROM);
        invitee_iterator_t iter2 = meeting_details_iterate(m_mtg_TO);

        I1 = meeting_details_next(m_mtg_FROM, iter1);
        I2 = meeting_details_next(m_mtg_TO, iter2);
        if( !I1 || !I2 )
        {
            ++nChangeCount;
            break;
        }
        while( I1 && I2 )
        {
            CMP_INTEGER(I1->role,I2->role,"role");
            CMP_PSTRING(I1->name, I2->name, "name");
            CMP_PSTRING(I1->screenname, I2->screenname, "screenname" );
            CMP_INTEGER(I1->selphone,I2->selphone, "selphone");
            CMP_PSTRING(I1->phone, I2->phone, "phone" );
            CMP_INTEGER(I1->selemail, I2->selemail, "selemail" );
            CMP_PSTRING(I1->email, I2->email, "email" );
            CMP_INTEGER(I1->notify_type, I2->notify_type, "notify_type" );
            //////////////////////////////////////////////
            I1 = meeting_details_next(m_mtg_FROM, iter1);
            I2 = meeting_details_next(m_mtg_TO, iter2);
        }

        // I1 and I2 s/b both zero if both collections have the same number of entries
        if( I1 != I2 )
        {
            ++nChangeCount;
        }
        fDone = true;
    }

    return (nChangeCount > 0);
}



int MeetingSetupDialog::ConfirmationRequired()
{
    int  nInviteeCount = NumberOfInvitees();
   int  nMask = 0;

    // If no start time is specified and the meeting isn't about to start.
    if( m_mtg_TO->start_time == 0 && !m_fStartMeetingMode )
    {
        // Them warn about time.
        nMask |= MeetingConfirmation::MCM_InvalidTime;
    }

    for( int row = 0; row < NumberOfInvitees(); ++row )
    {
        invitee_t invt = InviteeFromRow( row );
        //contact_t contact = ContactFromRow( row );

        if( invt->notify_type & NotifyPhone )
        {
            nMask |= MeetingConfirmation::MCM_NotifyCall;
        }
        if( invt->notify_type & NotifyEmail )
        {
            nMask |= MeetingConfirmation::MCM_NotifyEmail;
        }
        if( invt->notify_type & NotifyIMMask )
        {
            nMask |= MeetingConfirmation::MCM_NotifyIM;
        }
    }

    if( nInviteeCount > MeetingConfirmation::LargeMeetingThreshold )
    {
        nMask |= MeetingConfirmation::MCM_LargeMeeting;
    }
    return nMask;
}


/////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEBUG_MEETINGSETUPDIALOG
void show_pstring( const wxString & label, pstring s )
{
    if( s )
    {
        wxString wxs(s,wxConvUTF8);
       wxLogDebug(wxT("%s: %s"),label.c_str(), wxs.c_str() );
    }
    else
    {
        wxLogDebug(wxT("%s: NULL"),label.c_str());
    }
}

void MeetingSetupDialog::dump_meeting( meeting_details_t mtg )
{
    wxLogDebug( wxT("MeetingSetupDialog::dump_meeting\n") );

    show_pstring(wxT("meeting_id"),mtg->meeting_id);
    show_pstring(wxT("host_id"),mtg->host_id);

    if( mtg->type == TypeInstant ) wxLogDebug(wxT("type: TypeInstant"));
    else if( mtg->type == TypeScheduled ) wxLogDebug(wxT("type: TypeScheduled"));

    if( mtg->privacy == PrivacyPublic ) wxLogDebug(wxT("privacy: PrivacyPublic"));
	else if( mtg->privacy == PrivacyUnlisted ) wxLogDebug(wxT("privacy: PrivacyUnlisted"));
	else if( mtg->privacy == PrivacyPrivate ) wxLogDebug(wxT("privacy: PrivacyPrivate"));

    show_pstring(wxT("title"),mtg->title);

    wxLogDebug(wxT("time: %u"),mtg->start_time);

    wxString Ops;
    Ops = wxT("options:");
	if( mtg->options & WaitForModerator ) Ops += wxT("WaitForModerator|");
	if( mtg->options & ScreenCallers ) Ops += wxT("ScreenCallers|");
	if( mtg->options & LectureMode ) Ops += wxT("LectureMode|");
	if( mtg->options & HoldMode ) Ops += wxT("HoldMode|");
	if( mtg->options & LockParticipants ) Ops += wxT("LockParticipants|");
	if( mtg->options & AutoTimeExtension ) Ops += wxT("AutoTimeExtension|");
	if( mtg->options & RequirePassword ) Ops += wxT("RequirePassword|");
    if( mtg->options & ContinueWithoutModerator ) Ops += wxT("ContinueWithoutModerator|");
    if( mtg->options & InviteNeverExpires ) Ops += wxT("InviteNeverExpires|");
    if( mtg->options & Prefer1To1 ) Ops += wxT("Prefer1To1|");
    if( mtg->options & DefaultCallUsers ) Ops += wxT("DefaultCallUsers|");
    if( mtg->options & DefaultSendEmail ) Ops += wxT("DefaultSendEmail|");
    if( mtg->options & DefaultSendIM ) Ops += wxT("DefaultSendIM|");
    if( mtg->options & DefaultModerator ) Ops += wxT("DefaultModerator|");
    if( mtg->options & InvitationVoiceOnly ) Ops += wxT("InvitationVoiceOnly|");
    if( mtg->options & InvitationDataOnly ) Ops += wxT("InvitationDataOnly|");
    if( mtg->options & InviteAlwaysValidBeforeStart ) Ops += wxT("InviteAlwaysValidBeforeStart|");
	if( mtg->options & RecordingMeeting ) Ops += wxT("RecordingMeeting|");
	if( mtg->options & MuteJoinLeave ) Ops += wxT("MuteJoinLeave|");
    if( mtg->options & ModeratorMayEdit ) Ops += wxT("ModeratorMayEdit|");
    if( mtg->options & InvitationPCPhone ) Ops += wxT("InvitationPCPhone|");
    wxLogDebug(Ops);

    wxLogDebug(wxT("host_enabled_features: %u"),mtg->host_enabled_features);
    show_pstring(wxT("description"),mtg->description);
    wxLogDebug(wxT("end_time: %u"),mtg->end_time);
    wxLogDebug(wxT("invite_expiration: %u"),mtg->invite_expiration);
    wxLogDebug(wxT("invite_valid_before: %u"),mtg->invite_valid_before);
    show_pstring(wxT("invite_message"),mtg->invite_message);

    if( mtg->display_attendees == DisplayNone ) wxLogDebug(wxT("display_attendees: DisplayNone"));
	else if( mtg->display_attendees == DisplayModerators ) wxLogDebug(wxT("display_attendees: DisplayModerators"));
	else if( mtg->display_attendees == DisplayAll ) wxLogDebug(wxT("display_attendees: DisplayAll"));


	if( mtg->chat_options == ChatNone ) wxLogDebug(wxT("chat_options: ChatNone"));
	else if( mtg->chat_options == ChatModerators ) wxLogDebug(wxT("chat_options: ChatModerators"));
	else if( mtg->chat_options == ChatAll ) wxLogDebug(wxT("chat_options: ChatAll"));

    show_pstring(wxT("password"),mtg->password);
    show_pstring(wxT("bridge_phone"),mtg->bridge_phone);
    show_pstring(wxT("system_url"),mtg->system_url);

    invitee_iterator_t iter;
    invitee_t invt;

    iter = meeting_details_iterate(mtg);

    while ((invt = meeting_details_next(mtg, iter)) != NULL)
    {
        dump_invitee( invt );

        contact_t contact = 0;
        if( invt->user_id && *invt->user_id )
        {
            contact = CONTROLLER.LookupCABContact( invt->user_id );
            if( !contact )
            {
                contact = CONTROLLER.LookupPABContact( invt->user_id );
            }
        }
        if( contact )
        {
            dump_contact( contact );
        }
    }
}

void MeetingSetupDialog::dump_invitee( invitee_t invt )
{
    wxLogDebug(wxT("--- Invitee ---"));
    show_pstring(wxT("meeting_id"),invt->meeting_id);
    show_pstring(wxT("participant_id"), invt->participant_id);
    show_pstring(wxT("user_id"), invt->user_id);

	if( invt->role == RollNormal ) wxLogDebug(wxT("role: RollNormal"));
	else if( invt->role == RollListener ) wxLogDebug(wxT("role: RollListener"));
	else if( invt->role == RollModerator ) wxLogDebug(wxT("role: RollModerator"));
	else if( invt->role == RollHost ) wxLogDebug(wxT("role: RollHost"));

    show_pstring(wxT("name"),invt->name);
    show_pstring(wxT("screenname"),invt->screenname);

	     if( invt->selphone == SelPhoneDefault ) wxLogDebug(wxT("selphone: SelPhoneDefault"));
	else if( invt->selphone == SelPhoneNone ) wxLogDebug(wxT("selphone: SelPhoneThis"));
	else if( invt->selphone == SelPhoneBus ) wxLogDebug(wxT("selphone: SelPhoneBus"));
	else if( invt->selphone == SelPhoneHome ) wxLogDebug(wxT("selphone: SelPhoneHome"));
	else if( invt->selphone == SelPhoneMobile ) wxLogDebug(wxT("selphone: SelPhoneMobile"));
	else if( invt->selphone == SelPhoneOther ) wxLogDebug(wxT("selphone: SelPhoneOther"));
	else if( invt->selphone == SelPhonePC ) wxLogDebug(wxT("selphone: SelPhonePC"));

    show_pstring(wxT("phone"),invt->phone);

	     if( invt->selemail == SelEmailDefault ) wxLogDebug(wxT("selemail: SelEmailDefault"));
	else if( invt->selemail == SelEmailNone ) wxLogDebug(wxT("selemail: SelEmailNone"));
	else if( invt->selemail == SelEmailThis ) wxLogDebug(wxT("selemail: SelEmailThis"));
	else if( invt->selemail == SelEmail1 ) wxLogDebug(wxT("selemail: SelEmail1"));
	else if( invt->selemail == SelEmail2 ) wxLogDebug(wxT("selemail: SelEmail2"));
	else if( invt->selemail == SelEmail3 ) wxLogDebug(wxT("selemail: SelEmail3"));

    show_pstring(wxT("email"),invt->email);

    wxString ntype;
    if( invt->notify_type == NotifyNone ) wxLogDebug(wxT("notify_type: NotifyNone (0)"));
    ntype = wxT("notify_type: ");
	if( invt->notify_type & NotifyEmail ) ntype += wxT("NotifyEmail|");
	if( invt->notify_type & NotifyIIC ) ntype += wxT("NotifyIIC|");
	if( invt->notify_type & NotifyPhone ) ntype += wxT("NotifyPhone|");
	if( invt->notify_type & NotifyReminderEmail ) ntype += wxT("NotifyReminderEmail|");
	if( invt->notify_type & NotifyAIM ) ntype += wxT("NotifyAIM|");
	if( invt->notify_type & NotifyYahoo ) ntype += wxT("NotifyYahoo|");
	if( invt->notify_type & NotifyMessenger ) ntype += wxT("NotifyMessenger|");
	if( invt->notify_type & NotifyIMAny ) ntype += wxT("NotifyIMAny|");
	if( invt->notify_type & NotifyIMMask ) ntype += wxT("NotifyIMMask|");
    wxLogDebug(ntype);

    wxLogDebug( invt->is_adhoc ? wxT("is_adhoc: true"):wxT("is_adhoc: false") );
    wxLogDebug( invt->removed ? wxT("removed: true"):wxT("removed: false") );
}

void MeetingSetupDialog::dump_contact( contact_t contact )
{
    wxLogDebug( wxT("   --- Contact ---" ));
    show_pstring( wxT("   userid"), contact->userid );
    show_pstring( wxT("   username"), contact->username );
    show_pstring( wxT("   screenname"), contact->screenname );
    if( contact->type == UserTypeContact ) wxLogDebug(wxT("   type: Contact"));
    else if( contact->type == UserTypeUser ) wxLogDebug(wxT("   type: User"));
	else if( contact->type == UserTypeAdmin ) wxLogDebug(wxT("   type: Admin"));
    show_pstring(wxT("   profileid"), contact->profileid );

	     if( contact->defphone == SelPhoneDefault ) wxLogDebug(wxT("   defphone: SelPhoneDefault"));
	else if( contact->defphone == SelPhoneNone ) wxLogDebug(wxT("   defphone: SelPhoneThis"));
	else if( contact->defphone == SelPhoneBus ) wxLogDebug(wxT("   defphone: SelPhoneBus"));
	else if( contact->defphone == SelPhoneHome ) wxLogDebug(wxT("   defphone: SelPhoneHome"));
	else if( contact->defphone == SelPhoneMobile ) wxLogDebug(wxT("   defphone: SelPhoneMobile"));
	else if( contact->defphone == SelPhoneOther ) wxLogDebug(wxT("   defphone: SelPhoneOther"));
	else if( contact->defphone == SelPhonePC ) wxLogDebug(wxT("   defphone: SelPhonePC"));

    show_pstring( wxT("   title"), contact->title );
    show_pstring( wxT("   first"), contact->first );
    show_pstring( wxT("   middle"), contact->middle );
    show_pstring( wxT("   last"), contact->last );
    show_pstring( wxT("   suffix"), contact->suffix );
    show_pstring( wxT("   company"), contact->company );
    show_pstring( wxT("   jobtitle"), contact->jobtitle );
    show_pstring( wxT("   address1"), contact->address1 );
    show_pstring( wxT("   address2"), contact->address2 );
    show_pstring( wxT("   city"), contact->city );
    show_pstring( wxT("   state"), contact->state );
    show_pstring( wxT("   country"), contact->country );
    show_pstring( wxT("   postalcode"), contact->postalcode );
    show_pstring( wxT("   email"), contact->email );
    show_pstring( wxT("   email2"), contact->email2 );
    show_pstring( wxT("   email3"), contact->email3 );
    show_pstring( wxT("   busphone"), contact->busphone );
    show_pstring( wxT("   homephone"), contact->homephone );
    show_pstring( wxT("   mobilephone"), contact->mobilephone );
    show_pstring( wxT("   otherphone"), contact->otherphone );
    show_pstring( wxT("   aimscreen"), contact->aimscreen );
    show_pstring( wxT("   msnscreen"), contact->msnscreen );
    show_pstring( wxT("   yahooscreen"), contact->yahooscreen );
    show_pstring( wxT("   user1"), contact->user1 );
    show_pstring( wxT("   user"), contact->user2 );
	     if( contact->defphone == SelPhoneDefault ) wxLogDebug(wxT("   defphone: SelPhoneDefault"));
	else if( contact->defphone == SelPhoneNone ) wxLogDebug(wxT("   defphone: SelPhoneThis/SelPhoneNone ?"));
	else if( contact->defphone == SelPhoneBus ) wxLogDebug(wxT("   defphone: SelPhoneBus"));
	else if( contact->defphone == SelPhoneHome ) wxLogDebug(wxT("   defphone: SelPhoneHome"));
	else if( contact->defphone == SelPhoneMobile ) wxLogDebug(wxT("   defphone: SelPhoneMobile"));
	else if( contact->defphone == SelPhoneOther ) wxLogDebug(wxT("   defphone: SelPhoneOther"));
	else if( contact->defphone == SelPhonePC ) wxLogDebug(wxT("   defphone: SelPhonePC"));

}

#endif


void MeetingSetupDialog::OnSearchContactsClick(wxCommandEvent& event)
{
    if( !m_pFindContactsDlg )
    {
        m_pFindContactsDlg = new FindContactsDialog( this );
        m_pFindContactsDlg->Connect( wxID_OK,     wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&MeetingSetupDialog::OnSearchContactsOK, 0, this);
        m_pFindContactsDlg->Connect( wxID_CANCEL, wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&MeetingSetupDialog::OnSearchContactsCancel, 0, this);
        Connect( Search_Continue,wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingSetupDialog::OnSearchContinue) );
        Connect( Search_Edit, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingSetupDialog::OnSearchEdit) );
        Connect( Search_Remove, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(MeetingSetupDialog::OnSearchRemove) );
    }
    m_pFindContactsDlg->EditSearchCriteria(); // Prep & Show dialog
}

void MeetingSetupDialog::OnSearchContactsOK(wxCommandEvent& event)
{
    if( m_pFindContactsDlg )
    {
        m_pFindContactsDlg->SaveSearchCriteria();
        m_pFindContactsDlg->Hide();
        ContactSearch criteria;
        criteria = m_pFindContactsDlg->GetCriteria();

        if( m_ContactTree.EditCurrentSearch(false) )
        {
            ContactSearchResultsItemData *pSrch = m_ContactTree.GetSearchNode();
            if( !pSrch )
                return;

            wxTreeItemId searchNode = m_ContactTree.Tree()->GetSelection();
            m_ContactTree.Tree()->DeleteChildren( searchNode );

            pSrch->m_strSearchCriteriaName = criteria.m_strSearchName;
            criteria.Encode( pSrch->m_strSearchCriteria );

            // Reset the results state
            pSrch->m_nStartingRow = 0;
            pSrch->m_nTotalRetrieved = 0;
            pSrch->m_fComplete = false;

            // Use a different search number
            pSrch->m_nSearchNumber = m_ContactTree.NextSearchNumber();

            m_ContactTree.StartContactSearch();
        }
        else
        {
            m_ContactTree.StartContactSearch( criteria );
        }
    }
}

void MeetingSetupDialog::OnSearchContactsCancel(wxCommandEvent& event)
{
    if( m_pFindContactsDlg )
    {
        m_pFindContactsDlg->Hide();
    }
}

void MeetingSetupDialog::OnDirectoryFetched(wxCommandEvent& event)
{
    event.Skip();
    if( m_nState & State_WaitingForPABLoad )
    {
        wxLogDebug(wxT("MeetingSetupDialog::OnDirectoryFetched - PAB updated."));
        m_nState &= ~State_WaitingForPABLoad;
    }
    else if (event.GetString() == wxT("import"))
    {
        // Imported directory (e.g. Outlook). Make sure tree has been initialized first. 
        if (m_ContactTree.Tree() != NULL) { 
            directory_t dir = (directory_t)event.GetClientData();
            wxString dirName(dir->name, wxConvUTF8);
            wxTreeItemId id = m_ContactTree.AppendDirectoryNode( dirName, dir, dtUser );
            SortContacts( id, true );
        }
    }
    else
    {
        // This must be the search results.
        m_ContactTree.ProcessOnDirectoryFetched( event );
    }
}

void MeetingSetupDialog::OnSearchContinue( wxCommandEvent& event )
{
    m_ContactTree.StartContactSearch();
}

void MeetingSetupDialog::OnSearchEdit( wxCommandEvent& event )
{
    ContactSearchResultsItemData *pSrch = m_ContactTree.GetSearchNode();
    if( !pSrch )
        return;

    wxTreeItemId searchNode = m_ContactTree.Tree()->GetSelection();

    if( !m_pFindContactsDlg )
    {
        m_pFindContactsDlg = new FindContactsDialog( this );
        m_pFindContactsDlg->Connect( wxID_OK,     wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&MeetingSetupDialog::OnSearchContactsOK, 0, this);
        m_pFindContactsDlg->Connect( wxID_CANCEL, wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&MeetingSetupDialog::OnSearchContactsCancel, 0, this);
    }

    ContactSearch criteria;
    criteria.Decode( pSrch->m_strSearchCriteria );
    m_ContactTree.EditCurrentSearch(true);
    m_pFindContactsDlg->PresetSearchCriteria( pSrch->m_strSearchCriteriaName, criteria );
    m_pFindContactsDlg->EditSearchCriteria(); // Prep & Show dialog
}

void MeetingSetupDialog::OnSearchRemove( wxCommandEvent& event )
{
    m_ContactTree.RemoveSearch();
}

contact_t MeetingSetupDialog::LookupContact( invitee_t invt, bool & fCAB )
{
    contact_t contact = 0;

    // Try the user_id first
    if( invt->user_id && *invt->user_id )
    {
        contact = CONTROLLER.LookupCABContact( invt->user_id );
        if( !contact )
        {
            contact = CONTROLLER.LookupPABContact( invt->user_id );
            if( contact )
            {
                fCAB = false;
            }
        }
        if( !contact )
        {
            // A UserId w/o a contact record ????
            wxString name( invt->name, wxConvUTF8 );
            wxString userid( invt->user_id, wxConvUTF8 );
            wxLogDebug(wxT("MeetingSetupDialog: Logic error: Unable to locate contact rec: Name=|%s|, UserId=%s"),name.c_str(),userid.c_str());
        }
    }
    if( contact )
    {
        return contact;
    }

    wxString strScreenname( invt->screenname, wxConvUTF8 );

    // Try the screenname
    contact = LookupContact( "screenname", invt->screenname, fCAB );
    if( contact )
    {
        return contact;
    }

    // Try email
    contact = LookupContact( "email", invt->email, fCAB );
    if( contact )
    {
        return contact;
    }

    // Try phone
    contact = LookupContact( "busphone", invt->phone, fCAB );
    if( contact )
    {
        return contact;
    }

    return contact;
}

contact_t MeetingSetupDialog::LookupContact( const char *fieldname, const char *fieldvalue, bool & fCAB )
{
    contact_t contact = 0;
    if( fieldname && fieldvalue )
    {
        wxString strFieldName( fieldname, wxConvUTF8 );
        wxString strFieldValue( fieldvalue, wxConvUTF8 );
        contact = LookupContact( strFieldName, strFieldValue, fCAB );
    }
    return contact;
}

contact_t MeetingSetupDialog::LookupContact( const wxString & fieldname, const wxString & fieldvalue, bool & fCAB )
{
    contact_t contact = 0;
    
    if ( !fieldvalue.IsEmpty() )
    {
        wxCharBuffer fname = fieldname.mb_str(wxConvUTF8);
        wxCharBuffer fvalue = fieldvalue.mb_str(wxConvUTF8);

        if( CONTROLLER.GetCAB() )
        {
            contact = directory_find_contact_by_field( CONTROLLER.GetCAB(), fname, fvalue );
            if( contact )
            {
                fCAB = true;
            }
        }
        if( !contact )
        {
            if( CONTROLLER.GetPAB() )
            {
                contact = directory_find_contact_by_field( CONTROLLER.GetPAB(), fname, fvalue );
                if( contact )
                {
                    fCAB = false;
                }
            }
        }
        
        if( !contact )
        {
            directory_t dir = CONTROLLER.Directories()->GetDirectoryFirst();
            while (dir)
            {
                contact = directory_find_contact_by_field( dir, fname, fvalue );
                if( contact )
                {
                    fCAB = false;
                    break;
                }

                dir = CONTROLLER.Directories()->GetDirectoryNext();
            }
        }
    }
    return contact;
}

void MeetingSetupDialog::OnPhoneAllClick(wxCommandEvent& event)
{
    int order[] = {
        Phone_EnterPhoneValue,
        Phone_Custom,
        Phone_FollowMe,
        Phone_Business,
        Phone_Home,
        Phone_Mobile,
        Phone_Other,
        Phone_Business2,
        Phone_Home2,
        Phone_Mobile2,
        Phone_Other2,
        -1};
        
    for( int row = 0; row < m_Invitees->GetNumberRows(); ++row )
    {
        int i=-1;
        GridCellComboIntEditor *pE = (GridCellComboIntEditor *)m_Invitees->GetAttrEditor( wxSheetCoords(row, Col_Phone) ).GetRefData();
        
        while (order[++i] >= 0)
        {
            if ( pE->SetValue( order[i] ) ) 
            {
                m_Invitees->SetCellValue( wxSheetCoords(row, Col_Phone), pE->StartValue() );
                break;
            }
        }
    }
}

void MeetingSetupDialog::OnPhoneNoneClick(wxCommandEvent& event)
{
    for( int row = 0; row < m_Invitees->GetNumberRows(); ++row )
    {
        GridCellComboIntEditor *pE = (GridCellComboIntEditor *)m_Invitees->GetAttrEditor( wxSheetCoords(row, Col_Phone) ).GetRefData();
        pE->SetValue( Phone_DontCall );
        m_Invitees->SetCellValue( wxSheetCoords(row, Col_Phone), pE->StartValue() );
    }
}

void MeetingSetupDialog::OnEmailAllClick(wxCommandEvent& event)
{
    int order[] = {
        Email_EnterEmailValue,
        Email_Custom,
        Email_This,
        Email_1,
        Email_2,
        Email_3,
        Email_1_2,
        Email_2_2,
        Email_3_2,
        -1};

    for( int row = 0; row < m_Invitees->GetNumberRows(); ++row )
    {
        int i=-1;
        GridCellComboIntEditor *pE = (GridCellComboIntEditor *)m_Invitees->GetAttrEditor( wxSheetCoords(row, Col_Email) ).GetRefData();

        while (order[++i] >= 0)
        {
            if ( pE->SetValue( order[i] ) ) 
            {
                m_Invitees->SetCellValue( wxSheetCoords(row, Col_Email), pE->StartValue() );
                break;
            }
        }
    }
}

void MeetingSetupDialog::OnEmailNoneClick(wxCommandEvent& event)
{
    for( int row = 0; row < m_Invitees->GetNumberRows(); ++row )
    {
        GridCellComboIntEditor *pE = (GridCellComboIntEditor *)m_Invitees->GetAttrEditor( wxSheetCoords(row, Col_Email) ).GetRefData();
        pE->SetValue( Email_DontEmail );
        m_Invitees->SetCellValue( wxSheetCoords(row, Col_Email), pE->StartValue() );
    }
}

void MeetingSetupDialog::OnIMAllClick(wxCommandEvent& event)
{
    for( int row = 0; row < m_Invitees->GetNumberRows(); ++row )
    {
        GridCellComboIntEditor *pE = (GridCellComboIntEditor *)m_Invitees->GetAttrEditor( wxSheetCoords(row, Col_IM) ).GetRefData();
        pE->SetValue( NotifyIIC );
        m_Invitees->SetCellValue( wxSheetCoords(row, Col_IM), pE->StartValue() );
    }
}

void MeetingSetupDialog::OnIMNoneClick(wxCommandEvent& event)
{
    for( int row = 0; row < m_Invitees->GetNumberRows(); ++row )
    {
        GridCellComboIntEditor *pE = (GridCellComboIntEditor *)m_Invitees->GetAttrEditor( wxSheetCoords(row, Col_IM) ).GetRefData();
        pE->SetValue( NotifyNone );
        m_Invitees->SetCellValue( wxSheetCoords(row, Col_IM), pE->StartValue() );
    }
}

void MeetingSetupDialog::OnContactListColumnLeftClick(wxListEvent& event)
{
    if( m_nState & State_Sorting )
        return; // Not now! Prevent recursive sorts

    m_nState |= State_Sorting;

    long new_sort_col = event.GetColumn();
    wxLogDebug(wxT("MeetingCenterController::OnMeetingListColumnLeftClick( col %d )"),new_sort_col);

    long current_sort_col = m_ContactTree.Tree()->GetSortColumn();

    if( new_sort_col == current_sort_col )
    {
        m_ContactTree.Tree()->ReverseSortOrder();
    }
    else
    {
        m_ContactTree.Tree()->SetSortColumn( new_sort_col );
    }
    SortContacts();

    m_nState &= ~State_Sorting;
}

void MeetingSetupDialog::SortContacts()
{
    if( m_CommunityNode.IsOk() )
        SortContacts( m_CommunityNode );

    if( m_PersonalNode.IsOk() )
        SortContacts( m_PersonalNode );

    if( m_SearchNode.IsOk() )
        SortContacts( m_SearchNode );

    const wxArrayTreeItemIds& dirs = m_ContactTree.GetUserDirectories( );
    for( unsigned int i=0; i<dirs.GetCount(); i++ )
        SortContacts( dirs[i] );
}

void MeetingSetupDialog::SortContacts( const wxTreeItemId & item, bool fSortParent  )
{
    if( fSortParent )
    {
        m_ContactTree.Tree()->SortChildren( item );
    }

    wxTreeItemIdValue cookie;
    for ( wxTreeItemId child = m_ContactTree.Tree()->GetFirstChild(item, cookie);
      child.IsOk();
      child = m_ContactTree.Tree()->GetNextChild(item, cookie) )
    {
        m_ContactTree.Tree()->SortChildren( child );
    }
}

void MeetingSetupDialog::OnPresenceEvent( PresenceEvent& event )
{
    SetPresence( );
    event.Skip( );
}

void MeetingSetupDialog::SetPresence( )
{
    for( int row = 0; row < m_Invitees->GetNumberRows(); ++row )
    {
        SetPresence( row, true );
    }
}

void MeetingSetupDialog::SetPresence( int row, bool refreshScreen )
{
    // Access the associated contact record via the Col_Contact editor
    wxContactCellEditor *pCE = ContactEditorFromRow( row );
    if( !pCE )
        return;

    contact_t contact = pCE->m_contact;
    if( !contact || contact->screenname == '\0' )
        return;

    PresenceInfo * info = CONTROLLER.Presence().GetPresence(_T("zon"), wxString(contact->screenname, wxConvUTF8).wc_str());
    if( info )
    {
        int intContactStatus;
        switch (info->GetPresence())
        {
            case PresenceInfo::PresenceAway:
                intContactStatus = kStatusContactAway;
                break;
            case PresenceInfo::PresenceAvailable:
                intContactStatus = kStatusContactOnline;
                break;
            case PresenceInfo::PresenceUnavailable:
            default:
                intContactStatus = kStatusContactOffline;
        }

        int intMeetingStatus = kStatusMeetingUnknown;
        if (info->GetSpecial() & PresenceInfo::SpecialMeeting)
            intMeetingStatus = kStatusMeetingOnline;

        int intPhoneStatus = kStatusPhoneOffline;
        if (info->GetSpecial() & PresenceInfo::SpecialPhone)
            intPhoneStatus = kStatusPhoneOnline;

        m_StatusImages.SelectSubImage(m_StatusOnline, intMeetingStatus);
        m_StatusImages.SelectSubImage(m_StatusPhone, intPhoneStatus);
        m_StatusImages.SelectSubImage(m_StatusContact, intContactStatus);
        int statusImage = m_StatusImages.GetSelectedListIndex( );

        GridCellImageRenderer *pImage = (GridCellImageRenderer *) m_Invitees->GetAttrRenderer( wxSheetCoords(row,Col_Status)).GetRefData();
        pImage->SetImage( statusImage );

        if (refreshScreen)
            m_Invitees->RefreshCell( wxSheetCoords(row, Col_Status) );
    }
}


//*****************************************************************************
void MeetingSetupDialog::OnClickHelp(wxCommandEvent& event)
{
    wxString help( wxGetApp().Options().GetSystem( "HelpPath", "" ),  wxConvUTF8);
    help += LOCALE->GetCanonicalName( );
    help += wxT("/");
    help += wxT("meetings.htm");
    wxLaunchDefaultBrowser( help );
}


//*****************************************************************************
int MeetingSetupDialog::ShowModal( )
{
    wxStopWatch sw;
    long        start_time  = sw.Time( );
    long        timeout_ms  = 10000;

    while( (m_nState & State_WaitingForMeetingDetails )  ==  State_WaitingForMeetingDetails )
    {
        CUtils::MsgWait( 200, 1 );
        wxGetApp( ).Yield( );
        
        if( m_nState & State_AbortingProcess )
        {
            return wxID_CANCEL;
        }
        
        if( (sw.Time( ) - start_time)  >  timeout_ms )
        {
            wxLogDebug(wxT("MeetingSetupDialog::ShowModal( ): Timeout while waiting for meeting fetch"));

            wxString        strMsg( _("The server has not responded to a request for information.  Please try this action again later."), wxConvUTF8 );
            wxString        strTitle( _("Warning"), wxConvUTF8 );
            wxMessageDialog dlg( NULL, strMsg, strTitle, wxOK | wxICON_EXCLAMATION );
            dlg.ShowModal( );

            CONTROLLER.Show( );

            return wxID_CANCEL;
        }
    }

    CONTROLLER.RegisterMeetingSetup(this);

    int res = wxDialog::ShowModal( );

    CONTROLLER.UnregisterMeetingSetup(this);

    wxGetApp().Options().SetUserBool( "AutoCropPictures", m_CropImageCheck->IsChecked() );
    
    return res;
}


//*****************************************************************************
void MeetingSetupDialog::OnDateChangedStart( wxDateEvent& event )
{
    if( m_NoMeetingTimeCheck->GetValue( ) )
    {
        return;        // Special case. No date / time are specified
    }
    else
    {
        wxDateTime  dtStart, dtEnd;
        dtStart = m_StartDate->GetValue( );
        dtEnd   = m_EndDate->GetValue( );

        if( dtStart.IsLaterThan( dtEnd ) )
        {
            dtEnd = dtStart;
            dtEnd.SetHour( 0 );
            dtEnd.SetMinute( 0 );
            m_EndDate->SetValue( dtEnd );
        }
    }
}


//*****************************************************************************
void MeetingSetupDialog::OnDateChangedEnd( wxDateEvent& event )
{
}


//*****************************************************************************
void MeetingSetupDialog::OnCharStartTime(wxKeyEvent& event)
{
    int             iHour, iMin;
    wxTimeSpan      anHour( 1, 0, 0, 0 );
    wxTimeSpan      aMinute( 0, 1, 0, 0 );
    wxFocusEvent    eventDummy;
    int     iCode       = event.GetKeyCode( );
    switch( iCode )
    {
        case WXK_LEFT:
            if( m_iStartTimeSelection  ==  0 )
                m_iStartTimeSelection = 2;
            else
                m_iStartTimeSelection--;
            break;
        case WXK_RIGHT:
            if( m_iStartTimeSelection  ==  2 )
                m_iStartTimeSelection = 0;
            else
                m_iStartTimeSelection++;
            break;
        case WXK_UP:
            if( m_iStartTimeSelection  ==  0 )
                m_dtStartTime.Add( anHour );
            else if( m_iStartTimeSelection  ==  1 )
                m_dtStartTime.Add( aMinute );
            else
            {
                iHour = m_dtStartTime.GetHour( );
                iHour = (iHour + 12) % 24;
                m_dtStartTime.SetHour( iHour );
            }
            m_StartTime->SetValue( m_dtStartTime.Format( wxT("%I:%M %p") ) );
            break;
        case WXK_DOWN:
            if( m_iStartTimeSelection  ==  0 )
                m_dtStartTime.Subtract( anHour );
            else if( m_iStartTimeSelection  ==  1 )
                m_dtStartTime.Subtract( aMinute );
            else
            {
                iHour = m_dtStartTime.GetHour( );
                iHour = (iHour + 12) % 24;
                m_dtStartTime.SetHour( iHour );
            }
            m_StartTime->SetValue( m_dtStartTime.Format( wxT("%I:%M %p") ) );
            break;
        case 'A':
        case 'a':
            if( m_iStartTimeSelection  ==  2 )
            {
                iHour = m_dtStartTime.GetHour( );
                if( iHour  >=  12 )
                {
                    m_dtStartTime.SetHour( iHour - 12 );
                    m_StartTime->SetValue( m_dtStartTime.Format( wxT("%I:%M %p") ) );
                }
            }
            break;
        case 'P':
        case 'p':
            if( m_iStartTimeSelection  ==  2 )
            {
                iHour = m_dtStartTime.GetHour( );
                if( iHour  <  12 )
                {
                    m_dtStartTime.SetHour( iHour + 12 );
                    m_StartTime->SetValue( m_dtStartTime.Format( wxT("%I:%M %p") ) );
                }
            }
            break;
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            if( m_iStartTimeSelection  ==  0 )
            {
                iHour = m_dtStartTime.GetHour( );
                iHour *= 10;
                iHour += ( iCode - '0' );
                if( iHour  >=  24 )
                    iHour = ( iCode - '0' );
                m_dtStartTime.SetHour( iHour );
                m_StartTime->SetValue( m_dtStartTime.Format( wxT("%I:%M %p") ) );
            }
            else if( m_iStartTimeSelection  ==  1 )
            {
                iMin = m_dtStartTime.GetMinute( );
                iMin *= 10;
                iMin += ( iCode - '0' );
                if( iMin  >=  60 )
                    iMin = ( iCode - '0' );
                m_dtStartTime.SetMinute( iMin );
                m_StartTime->SetValue( m_dtStartTime.Format( wxT("%I:%M %p") ) );
            }
            break;

        default:
            break;
    }
    OnSetFocusStartTime( eventDummy );
}


//*****************************************************************************
void MeetingSetupDialog::OnSpinBtnStartChange(wxSpinEvent& event)
{
}


//*****************************************************************************
void MeetingSetupDialog::OnSpinBtnStartChangeUp(wxSpinEvent& event)
{
    SetStartSelection();
    
    if( m_iStartTimeSelection  ==  0 )
    {
        wxTimeSpan      anHour( 1, 0, 0, 0 );
        m_dtStartTime.Add( anHour );
    }
    else if( m_iStartTimeSelection  ==  1 )
    {
        wxTimeSpan      aMinute( 0, 1, 0, 0 );
        m_dtStartTime.Add( aMinute );
    }
    else
    {
        int iHour   = m_dtStartTime.GetHour( );
        iHour = (iHour + 12) % 24;
        m_dtStartTime.SetHour( iHour );
    }

    wxString updatedTime = m_dtStartTime.Format( wxT("%I:%M %p") );

#ifdef __WXMAC__
    long start, end;
    m_StartTime->GetSelection(&start, &end);
    m_StartTime->Replace(start, end, updatedTime.Mid(start, end-start));
    m_StartTime->SetFocus();
#else
    m_StartTime->SetValue(updatedTime);
    wxFocusEvent    eventDummy;
    OnSetFocusStartTime( eventDummy );
#endif
}


//*****************************************************************************
void MeetingSetupDialog::OnSpinBtnStartChangeDown(wxSpinEvent& event)
{
    SetStartSelection();

    if( m_iStartTimeSelection  ==  0 )
    {
        wxTimeSpan      anHour( 1, 0, 0, 0 );
        m_dtStartTime.Subtract( anHour );
    }
    else if( m_iStartTimeSelection  ==  1 )
    {
        wxTimeSpan      aMinute( 0, 1, 0, 0 );
        m_dtStartTime.Subtract( aMinute );
    }
    else
    {
        int     iHour   = m_dtStartTime.GetHour( );
        iHour = (iHour + 12) % 24;
        m_dtStartTime.SetHour( iHour );
    }

    wxString updatedTime = m_dtStartTime.Format( wxT("%I:%M %p") );
    
#ifdef __WXMAC__
    long start, end;
    m_StartTime->GetSelection(&start, &end);
    m_StartTime->Replace(start, end, updatedTime.Mid(start, end-start));
    m_StartTime->SetFocus();
#else
    m_StartTime->SetValue(updatedTime);
    wxFocusEvent    eventDummy;
    OnSetFocusStartTime( eventDummy );
#endif
}


//*****************************************************************************
void MeetingSetupDialog::OnSetFocusStartTime(wxFocusEvent& event)
{
    if( m_iStartTimeSelection  ==  0 )
        m_StartTime->SetSelection( 0, 2 );
    else if( m_iStartTimeSelection  ==  1 )
        m_StartTime->SetSelection( 3, 5 );
    else
        m_StartTime->SetSelection( 6, 8 );
}


//*****************************************************************************
void MeetingSetupDialog::SetStartSelection()
{
#ifdef __WXMAC__
    long start, end;
    m_StartTime->GetSelection(&start, &end);

    if (start > 5)
        m_iStartTimeSelection = 2;
    else if (start > 2)
        m_iStartTimeSelection = 1;
    else
        m_iStartTimeSelection = 0;

    wxFocusEvent    eventDummy;
    OnSetFocusStartTime( eventDummy );
#endif
}


//*****************************************************************************
void MeetingSetupDialog::OnEndFocusStartTime(wxFocusEvent& event)
{
    ValidateStartEndOrder( true );
}

//*****************************************************************************
void MeetingSetupDialog::OnSpinBtnEndChange(wxSpinEvent& event)
{
}


//*****************************************************************************
void MeetingSetupDialog::OnSpinBtnEndChangeUp(wxSpinEvent& event)
{
    SetEndSelection();
    
    if( m_iEndTimeSelection  ==  0 )
    {
        wxTimeSpan      anHour( 1, 0, 0, 0 );
        m_dtEndTime.Add( anHour );
    }
    else if( m_iEndTimeSelection  ==  1 )
    {
        wxTimeSpan      aMinute( 0, 1, 0, 0 );
        m_dtEndTime.Add( aMinute );
    }
    else
    {
        int iHour   = m_dtEndTime.GetHour( );
        iHour = (iHour + 12) % 24;
        m_dtEndTime.SetHour( iHour );
    }
    
    wxString updatedTime = m_dtEndTime.Format( wxT("%I:%M %p") ); 
#ifdef __WXMAC__
    long start, end;
    m_EndTime->GetSelection(&start, &end);
    m_EndTime->Replace(start, end, updatedTime.Mid(start, end-start)); 
    m_EndTime->SetFocus();
#else
    m_EndTime->SetValue( updatedTime );
    wxFocusEvent    eventDummy;
    OnSetFocusEndTime( eventDummy );
#endif
}


//*****************************************************************************
void MeetingSetupDialog::OnSpinBtnEndChangeDown(wxSpinEvent& event)
{
    SetEndSelection();

    if( m_iEndTimeSelection  ==  0 )
    {
        wxTimeSpan      anHour( 1, 0, 0, 0 );
        m_dtEndTime.Subtract( anHour );
    }
    else if( m_iEndTimeSelection  ==  1 )
    {
        wxTimeSpan      aMinute( 0, 1, 0, 0 );
        m_dtEndTime.Subtract( aMinute );
    }
    else
    {
        int     iHour   = m_dtEndTime.GetHour( );
        iHour = (iHour + 12) % 24;
        m_dtEndTime.SetHour( iHour );
    }

    wxString updatedTime = m_dtEndTime.Format( wxT("%I:%M %p") ); 
#ifdef __WXMAC__
    long start, end;
    m_EndTime->GetSelection(&start, &end);
    m_EndTime->Replace(start, end, updatedTime.Mid(start, end-start));    
    m_EndTime->SetFocus();
#else
    m_EndTime->SetValue( updatedTime );
    wxFocusEvent    eventDummy;
    OnSetFocusEndTime( eventDummy );
#endif
}


//*****************************************************************************
void MeetingSetupDialog::OnCharEndTime(wxKeyEvent& event)
{
    int             iHour, iMin;
    wxTimeSpan      anHour( 1, 0, 0, 0 );
    wxTimeSpan      aMinute( 0, 1, 0, 0 );
    wxFocusEvent    eventDummy;
    int     iCode       = event.GetKeyCode( );
    switch( iCode )
    {
        case WXK_LEFT:
            if( m_iEndTimeSelection  ==  0 )
                m_iEndTimeSelection = 2;
            else
                m_iEndTimeSelection--;
            break;
        case WXK_RIGHT:
            if( m_iEndTimeSelection  ==  2 )
                m_iEndTimeSelection = 0;
            else
                m_iEndTimeSelection++;
            break;
        case WXK_UP:
            if( m_iEndTimeSelection  ==  0 )
                m_dtEndTime.Add( anHour );
            else if( m_iEndTimeSelection  ==  1 )
                m_dtEndTime.Add( aMinute );
            else
            {
                int iHour   = m_dtEndTime.GetHour( );
                iHour = (iHour + 12) % 24;
                m_dtEndTime.SetHour( iHour );
            }
            m_EndTime->SetValue( m_dtEndTime.Format( wxT("%I:%M %p") ) );
            break;
        case WXK_DOWN:
            if( m_iEndTimeSelection  ==  0 )
                m_dtEndTime.Subtract( anHour );
            else if( m_iEndTimeSelection  ==  1 )
                m_dtEndTime.Subtract( aMinute );
            else
            {
                int iHour   = m_dtEndTime.GetHour( );
                iHour = (iHour + 12) % 24;
                m_dtEndTime.SetHour( iHour );
            }
            m_EndTime->SetValue( m_dtEndTime.Format( wxT("%I:%M %p") ) );
            break;
        case 'A':
        case 'a':
            if( m_iEndTimeSelection  ==  2 )
            {
                iHour = m_dtEndTime.GetHour( );
                if( iHour  >=  12 )
                {
                    m_dtEndTime.SetHour( iHour - 12 );
                    m_EndTime->SetValue( m_dtEndTime.Format( wxT("%I:%M %p") ) );
                }
            }
            break;
        case 'P':
        case 'p':
            if( m_iEndTimeSelection  ==  2 )
            {
                iHour = m_dtEndTime.GetHour( );
                if( iHour  <  12 )
                {
                    m_dtEndTime.SetHour( iHour + 12 );
                    m_EndTime->SetValue( m_dtEndTime.Format( wxT("%I:%M %p") ) );
                }
            }
            break;
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            if( m_iEndTimeSelection  ==  0 )
            {
                iHour = m_dtEndTime.GetHour( );
                iHour *= 10;
                iHour += ( iCode - '0' );
                if( iHour  >=  24 )
                    iHour = ( iCode - '0' );
                m_dtEndTime.SetHour( iHour );
                m_EndTime->SetValue( m_dtEndTime.Format( wxT("%I:%M %p") ) );
            }
            else if( m_iEndTimeSelection  ==  1 )
            {
                iMin = m_dtEndTime.GetMinute( );
                iMin *= 10;
                iMin += ( iCode - '0' );
                if( iMin  >=  60 )
                    iMin = ( iCode - '0' );
                m_dtEndTime.SetMinute( iMin );
                m_EndTime->SetValue( m_dtEndTime.Format( wxT("%I:%M %p") ) );
            }
            break;

        default:
            break;
    }
    OnSetFocusEndTime( eventDummy );
}


//*****************************************************************************
void MeetingSetupDialog::OnSetFocusEndTime(wxFocusEvent& event)
{
    if( m_iEndTimeSelection  ==  0 )
        m_EndTime->SetSelection( 0, 2 );
    else if( m_iEndTimeSelection  ==  1 )
        m_EndTime->SetSelection( 3, 5 );
    else
        m_EndTime->SetSelection( 6, 8 );
}


//*****************************************************************************
void MeetingSetupDialog::SetEndSelection()
{
#ifdef __WXMAC__
    long start, end;
    m_EndTime->GetSelection(&start, &end);
    
    if (start > 5)
        m_iEndTimeSelection = 2;
    else if (start > 2)
        m_iEndTimeSelection = 1;
    else
        m_iEndTimeSelection = 0;
    
    wxFocusEvent    eventDummy;
    OnSetFocusEndTime( eventDummy );
#endif
}


//*****************************************************************************
void MeetingSetupDialog::ValidateStartEndOrder( bool fStartChanged )
{
    int         iH, iM;
    wxDateTime  dtStart, dtEnd;

    iH  = m_dtStartTime.GetHour( );
    iM  = m_dtStartTime.GetMinute( );
    dtStart = m_StartDate->GetValue( );
    dtStart.SetHour( iH );
    dtStart.SetMinute( iM );

    iH  = m_dtEndTime.GetHour( );
    iM  = m_dtEndTime.GetMinute( );
    dtEnd = m_EndDate->GetValue( );
    dtEnd.SetHour( iH );
    dtEnd.SetMinute( iM );

    if( dtStart.IsLaterThan( dtEnd ) )
    {
        wxTimeSpan  ts30Min( 0, 30, 0, 0 );

        //  Wrong order!  Move one so there is 30 minute separation
        if( fStartChanged )
        {
            dtEnd = dtStart;
            dtEnd.Add( ts30Min );
            iH = dtEnd.GetHour( );
            iM = dtEnd.GetMinute( );
            m_dtEndTime.SetHour( iH );
            m_dtEndTime.SetMinute( iM );
            m_EndTime->SetValue( m_dtEndTime.Format( wxT("%I:%M %p") ) );
            dtEnd.SetHour( 0 );
            dtEnd.SetMinute( 0 );
            m_EndDate->SetValue( dtEnd );
        }
        else
        {
            dtStart = dtEnd;
            dtStart.Subtract( ts30Min );
            iH = dtStart.GetHour( );
            iM = dtStart.GetMinute( );
            m_dtStartTime.SetHour( iH );
            m_dtStartTime.SetMinute( iM );
            m_StartTime->SetValue( m_dtStartTime.Format( wxT("%I:%M %p") ) );
            dtStart.SetHour( 0 );
            dtStart.SetMinute( 0 );
            m_StartDate->SetValue( dtStart );
        }
    }
}


//*****************************************************************************
void MeetingSetupDialog::OnLeftUpStartTime(wxMouseEvent& event)
{
    wxPoint     pt;
    pt = event.GetPosition( );

    wxTextCoord     iCol, iRow;
#ifdef LINUX
    wxTextCtrlHitTestResult res = m_StartTime->HitTest( pt, &iCol, &iRow );
    if( res  ==  wxTE_HT_UNKNOWN )
    {
        int         iX, iY;
        wxString    strT    = m_StartTime->GetValue( );
        for( iCol=0; iCol<8; iCol++ )
        {
            m_StartTime->GetTextExtent( strT.Left( iCol ), &iX, &iY );
            if( pt.x  >  iX )
                continue;
            break;
        }
    }
#else
    m_StartTime->HitTest( pt, &iCol, &iRow );
#endif
    if( iCol >= 0  &&  iCol <= 2 )
        m_iStartTimeSelection = 0;
    else if( iCol >= 3  &&  iCol <= 5 )
        m_iStartTimeSelection = 1;
    else
        m_iStartTimeSelection = 2;

    wxFocusEvent    eventDummy;
    OnSetFocusStartTime( eventDummy );

    event.Skip( );
}


//*****************************************************************************
void MeetingSetupDialog::OnLeftUpEndTime(wxMouseEvent& event)
{
    wxPoint     pt;
    pt = event.GetPosition( );

    wxTextCoord     iCol, iRow;
#ifdef LINUX
    wxTextCtrlHitTestResult res = m_EndTime->HitTest( pt, &iCol, &iRow );
    if( res  ==  wxTE_HT_UNKNOWN )
    {
        int         iX, iY;
        wxString    strT    = m_EndTime->GetValue( );
        for( iCol=0; iCol<8; iCol++ )
        {
            m_EndTime->GetTextExtent( strT.Left( iCol ), &iX, &iY );
            if( pt.x  >  iX )
                continue;
            break;
        }
    }
#else
    m_EndTime->HitTest( pt, &iCol, &iRow );
#endif
    if( iCol >= 0  &&  iCol <= 2 )
        m_iEndTimeSelection = 0;
    else if( iCol >= 3  &&  iCol <= 5 )
        m_iEndTimeSelection = 1;
    else
        m_iEndTimeSelection = 2;

    wxFocusEvent    eventDummy;
    OnSetFocusEndTime( eventDummy );

    event.Skip( );
}


void addDirectory(std::set<std::string>& autoNames, directory_t dir)
{
    contact_t   contact;
    
    if (dir)
    {
        contact_iterator_t iter = directory_contact_iterate(dir);
        while ((contact = directory_contact_next(dir, iter)) != NULL)
        {
            autoNames.insert(std::string(contact->displayname));
        }
    }
}


//*****************************************************************************
void MeetingSetupDialog::SetupAutoComplete()
{
    directory_t dir;
    std::set<std::string> list;

    m_autoNames.Clear();
    
    dir = CONTROLLER.GetCAB();
    addDirectory(list, dir);
    
    dir = CONTROLLER.GetPAB();
    addDirectory(list, dir);

    dir = CONTROLLER.Directories()->GetDirectoryFirst();
    while (dir)
    {
        addDirectory(list, dir);
        dir = CONTROLLER.Directories()->GetDirectoryNext();
    }

    std::set<std::string>::iterator iter2 = list.begin();
    while (iter2 != list.end())
    {
        wxString name( (*iter2).c_str(), wxConvUTF8 );
        m_autoNames.Add( name );
        iter2++;
    }
}


//*****************************************************************************
void MeetingSetupDialog::OnUploadBtnClick(wxCommandEvent& event)
{
    const char *pChar;
    pChar = controller_get_docshare_server( CONTROLLER.controller, (const char *) m_meetingId.mb_str( wxConvUTF8 ) );
    wxString docServer( pChar, wxConvUTF8 );

    wxString tempId;
    wxString screenName, password, community;
    CONTROLLER.GetSignOnInfo( screenName, password, community );

    if (!m_pDoc)
    {
        m_pDoc = (CIICDoc *) wxGetApp( ).m_pDocManager->CreateDocument( wxEmptyString, wxDOC_NEW );

        m_pDoc->SetSessionInfo( screenName, password, docServer );
        m_pDoc->m_Meeting.SetMeetingId( m_meetingId );
    }

    DocShareView    *pView;
    pView = (DocShareView *) m_pDoc->GetFirstView(  );

    // Use temporary identifier for new meeting
    if (m_meetingId.IsEmpty())
        tempId = kTempId + wxT("/") + screenName;
    else
        tempId = m_meetingId;
    CUtils::EncodeStringFully( tempId );

    pView->InitializeInfo( m_pDoc, tempId );

    wxPoint pos = GetPosition( );
    DocShareCanvas * pDocSharePanel = new DocShareCanvas( this, pos, wxSize( 1, 1 ), 0 );

    pView->Start( NULL, pDocSharePanel, false, true, false );

    // The document and view are kept for later re-use
    pDocSharePanel->Destroy();
    
    m_fUploadedDocs = true;
}


//*****************************************************************************
void MeetingSetupDialog::MoveTempDocuments(wxString meetingId)
{
    DocShareView    *pView;
    pView = (DocShareView *) m_pDoc->GetFirstView(  );

    wxString screenName, password, community;
    CONTROLLER.GetSignOnInfo( screenName, password, community );

    wxString tempId = kTempId + wxT("/") + screenName;
    CUtils::EncodeStringFully( tempId );
    
    pView->CopyDocuments(this, tempId, meetingId);
}


//*****************************************************************************
void MeetingSetupDialog::SetupFileManager()
{
    wxString tempId;
    wxString screenName(CONTROLLER.GetCurrentScreenName());
    
    if (m_meetingId.IsEmpty())
        tempId = kTempId + wxT("/") + screenName;
    else
        tempId = m_meetingId;
    CUtils::EncodeStringFully( tempId );
    
    m_FileManager = new FileManager(FileManager::GetPathForMeeting(tempId), FileManager::Create);
    m_FileManager->LoadCollection(wxT("meetingPictures"));
    m_Pictures->Initialize(m_FileManager);
}


//*****************************************************************************
void MeetingSetupDialog::OnUploadImagesClick(wxCommandEvent& event)
{
    m_Pictures->AddFile(m_CropImageCheck->IsChecked());
}


//*****************************************************************************
void MeetingSetupDialog::OnProfileImageBtnClick(wxCommandEvent& event)
{
    wxString profileImage = CONTROLLER.FetchProfileImage();
    if (!profileImage.IsEmpty())
    {
        wxFile img(profileImage);
        if (img.Length() > 0 && !m_FileManager->Exists(profileImage))
        {
            m_FileManager->AddFile(-1, profileImage, CONTROLLER.GetDisplayName());
            m_Pictures->PopulateFiles();
        }
    }
}


//*****************************************************************************
void MeetingSetupDialog::MoveTempImages(wxString meetingId)
{
    wxString screenName(CONTROLLER.GetCurrentScreenName());
    
    wxString tempId = kTempId + wxT("/") + screenName;
    CUtils::EncodeStringFully( tempId );
    
    wxString targetId = meetingId;
    CUtils::EncodeStringFully( targetId );

    wxString srcPath = FileManager::GetPathForMeeting(tempId) + wxT("meetingPictures/");

    FileManager mgr(FileManager::GetPathForMeeting(targetId), true);
    mgr.CopyCollection(wxT("meetingPictures"), srcPath);
}

