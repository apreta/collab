#!/bin/sh

brand=$1

for dir in locale/*; do
	if [ -d $dir ]; then
    	echo "Compiling $dir"
		pushd $dir >/dev/null
		sed -e "/msgstr/s/Zon/$brand/"  meeting.po >meeting.tmp
		if ! msgfmt -f -o meeting.mo meeting.tmp ; then
			echo "Failed"
			exit -1;
		fi
		popd >/dev/null
	fi
done
