/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
//
// imagelistcomposite.cpp: implementation of the CImageListComposite class.
//
//////////////////////////////////////////////////////////////////////

#include "wx_pch.h"

#include "imagelistcomposite.h"

#include <wx/file.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CImageListComposite::CImageListComposite()
{
    m_SubimageCount = 0;
    m_GeneratedOffset = 0;
    m_BitmapHeight = 0;
    m_BitmapWidth = 0;
}


//*****************************************************************************
CImageListComposite::~CImageListComposite()
{
    for (int loop=0; loop < (int) m_SubimageArray.GetCount( ); loop++)
    {
        wxBitmap *pBitmap = (wxBitmap *)m_SubimageArray.Item(loop);
        delete pBitmap;
    }
}


//////////////////////////////////////////////////////////////////////
// Implementation
//////////////////////////////////////////////////////////////////////


//*****************************************************************************
// AddSubImageArray
//
int CImageListComposite::AddSubImageArray(const wxString &aNameBase, int aArraySize)
{
    if( aArraySize  >  0 )
    {
        bool        fRet;
        wxString    strT;
        wxBitmap    *pBitmap;
        for( int loop=0; loop < aArraySize; loop++ )
        {
            pBitmap = new wxBitmap( );
            
            // Load image if we can, but put it into the array regardless so we can run without the images
            strT = wxString::Format( _T("%d.bmp"), loop );
            strT.Prepend( aNameBase );
            if( wxFile::Exists( strT ) )
            {
                fRet = pBitmap->LoadFile( strT, wxBITMAP_TYPE_BMP );
            }
            
            m_SubimageArray.Add( pBitmap );
        }

        m_SubimageArraySize.Add(aArraySize);
        m_SubimageSelections.Add(0);
        m_SubimageCount++;
    }

    return (int) m_SubimageArraySize.GetCount( ) - 1;
}


//*****************************************************************************
// Create
//
void CImageListComposite::Create(wxImageList *aInitialImageList)
{
    intArray    arraySubimageIndexes;

    // Figure out the height and width of the composite
    // bitmaps that we are going to generate.
    //
    // Also,  initialize arraySubimageIndexes
    m_BitmapHeight = 0;
    m_BitmapWidth = 0;

    int         iIdx    = 0;
    for( int loop=0; loop < (int) m_SubimageArraySize.GetCount(); loop++ )
    {
        wxBitmap    *pBitmap = (wxBitmap *) m_SubimageArray.Item( iIdx );

        m_BitmapWidth += pBitmap->GetWidth( );
        if( pBitmap->GetHeight( )  >  m_BitmapHeight )
        {
            m_BitmapHeight = pBitmap->GetHeight( );
        }

        arraySubimageIndexes.Add( 0 );

        iIdx += m_SubimageArraySize.Item( loop );
    }

    // Create the internal image list
    m_ImageList.Create( m_BitmapWidth, m_BitmapHeight, 0, 1 );

    // Add the initial wxImageList if necessary
    if( NULL != aInitialImageList )
    {
        for( int index=0; index < aInitialImageList->GetImageCount( ); index++)
        {
            wxBitmap    wBmp = aInitialImageList->GetBitmap( index );
            m_ImageList.Add( wBmp );
            m_GeneratedOffset++;
        }
    }

    // Now generate the subimages
    int intCurrentSubimages = 0;
    GenerateBitmaps( intCurrentSubimages, arraySubimageIndexes );
}


//*****************************************************************************
// GenerateBitmaps
//
void CImageListComposite::GenerateBitmaps(int aCurrentSubimages, intArray &arraySubimageIndexes)
{
    int iIdx        = 0;
    if( aCurrentSubimages < (int) arraySubimageIndexes.GetCount( ) )
    {
        int intBitmapCount = m_SubimageArraySize.Item(aCurrentSubimages);

        for (int loop=0; loop < intBitmapCount; loop++)
        {
            arraySubimageIndexes.Item( aCurrentSubimages ) = loop;
            GenerateBitmaps(aCurrentSubimages + 1, arraySubimageIndexes);
        }
    }
    else
    {
        // Create the new bitmap...
        wxMemoryDC  dcMemory;
        wxBitmap    *pBitmap;

        int iH  = 0;
        int iW  = 0;
        for( int index=0; index < (int) m_SubimageArraySize.GetCount( ); index++ )
        {
            pBitmap = (wxBitmap *) m_SubimageArray.Item( iIdx + arraySubimageIndexes.Item( index ) );
            if( pBitmap->GetHeight( )  >  iH )
                iH = pBitmap->GetHeight( );
            iW += pBitmap->GetWidth( );
            iIdx += m_SubimageArraySize.Item( index );
        }

        wxBitmap *bitmapGenerated;
        bitmapGenerated = new wxBitmap( );
        bitmapGenerated->Create( iW, iH );

        dcMemory.SelectObject( *bitmapGenerated );

        int     intCurrentX = 0;
        iIdx = 0;
        for( int index=0; index < (int) m_SubimageArraySize.GetCount( ); index++ )
        {
            pBitmap = (wxBitmap *) m_SubimageArray.Item( iIdx + arraySubimageIndexes.Item( index ) );
            dcMemory.DrawBitmap( *pBitmap, intCurrentX, 0, false );
            intCurrentX += pBitmap->GetWidth( );
            iIdx += m_SubimageArraySize.Item( index );
        }

        dcMemory.SelectObject( wxNullBitmap );  //  release bitmapGenerated from the DC

        wxMask  *pMask;
        pMask = new wxMask( *bitmapGenerated, wxColour( 255, 255, 255 ) );
        bitmapGenerated->SetMask( pMask );

        iIdx = m_ImageList.Add( *bitmapGenerated );

        delete bitmapGenerated;
    }
}


//*****************************************************************************
// GetImageList
//
wxImageList *CImageListComposite::GetImageList( )
{
    int     iRet;
    iRet = m_ImageList.GetImageCount( );

    bool    fRet;
    int     iW, iH;
    fRet = m_ImageList.GetSize( 0, iW, iH );

    wxBitmap    wB;
    wB = m_ImageList.GetBitmap( 0 );

    fRet = wB.IsOk( );
    iRet = wB.GetHeight( );
    iRet = wB.GetWidth( );

    return &m_ImageList;
}


//*****************************************************************************
// SelectSubImage
//
// Both aSubImageId and aSelectedIndex are zero-based
//
void CImageListComposite::SelectSubImage(int aSubImageId, int aSelectedIndex)
{
    if (aSubImageId >= 0  &&  aSubImageId < (int) m_SubimageArraySize.Count( ))
    {
        if (aSelectedIndex >= 0 && aSelectedIndex < m_SubimageArraySize.Item(aSubImageId))
        {
            m_SubimageSelections.Item( aSubImageId ) = aSelectedIndex;
        }
    }
}


//*****************************************************************************
// GetSelectedListIndex
//
int CImageListComposite::GetSelectedListIndex()
{
    int intSelectedIndex = 0;
    int intMultiplier = 1;

    for( int loop=m_SubimageSelections.GetCount( ) - 1; loop >= 0; loop--)
    {
        intSelectedIndex += m_SubimageSelections.Item( loop ) * intMultiplier;
        intMultiplier *= m_SubimageArraySize.Item( loop );
    }

    return( intSelectedIndex + m_GeneratedOffset );
}
