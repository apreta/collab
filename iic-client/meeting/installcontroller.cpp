/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
// installcontroller.cpp: implementation of the InstallController class.
//
//////////////////////////////////////////////////////////////////////

#include "app.h"

#include "installcontroller.h"
#include "progress1dialog.h"
#include <wx/stdpaths.h>


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
InstallController::InstallController()
{
    m_bWait = FALSE;
	m_hInternetSession = NULL;
	m_hConnection = NULL;
	m_hRequest = NULL;
}


//*****************************************************************************
InstallController::~InstallController()
{
}


//*****************************************************************************
wxString InstallController::genTempFile( )
{
    wxString strTempFile;

    // Get the path for the User's data directory
    strTempFile = wxStandardPaths::Get().GetUserDataDir() + wxT("/");

    // Get the name of the file being downloaded
    int index = m_installerURL.Find( _T('/'), true);

    if (index == -1)
        strTempFile.Append(wxT("/conferencing.exe"));
    else
        strTempFile.Append(m_installerURL.Right(m_installerURL.Len() - index - 1));

    // Return the directory + filename
    return strTempFile;
}


//*****************************************************************************
void InstallController::error( const wxString &functionName )
{
    wxString    strBuf;
    int         iErr    = GetLastError( );

    strBuf = wxString::Format( _("%s [%d]\n"), functionName.c_str( ), iErr );

    wxLogDebug(wxT("InstallController error: %s"), strBuf.c_str());
    
    wxString            strError( _("Error") );
    wxMessageDialog     dlg( NULL, strBuf, strError, wxOK );
    dlg.ShowModal( );
}


// This is not yet defined in mingw's wininet.h
// Hopefully it will be someday, and this can be removed!
#define ERROR_WINHTTP_SECURE_CERT_REV_FAILED (INTERNET_ERROR_BASE+57)


//*****************************************************************************
// saves the installer from a known URL to a temp location
int InstallController::SaveInstaller( void* pData, Progress1Dialog* pProgressDlg )
{
    m_installerFileName = genTempFile( );

	m_installFile.Open( m_installerFileName, wxFile::write );

	// Make internet connection.
    m_hInternetSession = netHttpOpen( "IICDownload", 2, true );

    if( m_hInternetSession  ==  NULL )
	{
        wxString     strT( _("Error opening network session") );
		error( strT );
        pProgressDlg->Commands().Add( wxT("hide") );
		return 0;
	}

    int     retry = 0;
    int     flags = INTERNET_FLAG_RELOAD|INTERNET_FLAG_DONT_CACHE;
    bool    ignoreCA = false;
    bool    ignoreRev = false;

    wxString    proto = m_installerURL.Left(5);
	proto.MakeUpper( );
	if( proto  ==  wxT("HTTPS") )
	{
		flags |= INTERNET_FLAG_SECURE |
				INTERNET_FLAG_IGNORE_CERT_DATE_INVALID |
				INTERNET_FLAG_IGNORE_CERT_CN_INVALID;
	}

    wxString        strHost;
    wxString        strScheme;
    wxString        strPath;
    unsigned short  usPort;
    wxURI           uri( m_installerURL );

    if( uri.HasServer( ) )
        strHost = uri.GetServer( );
    if( uri.HasScheme( ) )
        strScheme = uri.GetScheme( );
    if( uri.HasPath( ) )
        strPath = uri.GetPath( );
    if( uri.HasPort( ) )
        usPort = wxAtoi( uri.GetPort( ) );
    else
    {
        if( strScheme.CmpNoCase( wxT("HTTPS") )  == 0 )
            usPort = INTERNET_DEFAULT_HTTPS_PORT;       //  use the default port for HTTPS if scheme is HTTPS
        else
            usPort = INTERNET_DEFAULT_HTTP_PORT;        //  use the default port for HTTP if none is otherwise specified
    }

    if( strHost.IsEmpty( ) )
    {
        wxString errmsg( _("Cannot parse download URL ") );
        errmsg += m_installerURL;
        error( errmsg );
        pProgressDlg->Commands().Add( wxT("hide") );
        return 0;
    }

    m_hConnection = netHttpConnect( m_hInternetSession, m_installerURL.mb_str( wxConvUTF8 ), strHost.mb_str( wxConvUTF8 ),
                                    usPort, NULL, NULL );

    wxString    strCmd( wxT("GET") );
	while( true )
	{
        m_hRequest = netHttpOpenRequest( m_hConnection, strCmd.mb_str( wxConvUTF8 ), strPath.mb_str( wxConvUTF8 ), NULL, flags );
        if( m_hRequest  ==  NULL )
		{
            wxString    strT( _("Error opening installer URL ") );
            strT += m_installerURL;
            error( strT );
            pProgressDlg->Commands().Add( wxT("hide") );
			return 0;
		}

#ifdef WIN32
        if( ignoreCA )
		{
			DWORD dwFlags;
			DWORD dwBuffLen = sizeof(dwFlags);

			InternetQueryOption (m_hRequest, INTERNET_OPTION_SECURITY_FLAGS,
				(LPVOID)&dwFlags, &dwBuffLen);

			dwFlags |= SECURITY_FLAG_IGNORE_UNKNOWN_CA;
			InternetSetOption (m_hRequest, INTERNET_OPTION_SECURITY_FLAGS,
								&dwFlags, sizeof (dwFlags) );
		}

        if( ignoreRev )
		{
			DWORD dwFlags;
			DWORD dwBuffLen = sizeof(dwFlags);

			InternetQueryOption (m_hRequest, INTERNET_OPTION_SECURITY_FLAGS,
				(LPVOID)&dwFlags, &dwBuffLen);

			dwFlags |= SECURITY_FLAG_IGNORE_REVOCATION;
			InternetSetOption (m_hRequest, INTERNET_OPTION_SECURITY_FLAGS,
								&dwFlags, sizeof (dwFlags) );
		}
#endif

        if( !netHttpSendRequest( m_hRequest, m_hInternetSession, NULL, 0, NULL, 0 ) )
		{
			if (++retry < 3)
			{
				int err = GetLastError( );
				if (err == ERROR_INTERNET_TIMEOUT)
				{
					continue;
				}
				else if (err == ERROR_INTERNET_INVALID_CA && !ignoreCA)
				{
                    ignoreCA = true;
                    netInternetCloseHandle( m_hRequest );
                    retry--;  // don't count this error as a retry
                    continue;
				}
				else if (err == ERROR_WINHTTP_SECURE_CERT_REV_FAILED && !ignoreRev)
				{
                    ignoreRev = true;
                    netInternetCloseHandle( m_hRequest );
                    retry--;  // don't count this error as a retry
                    continue;
				}
			}

            wxString     strT( _("Error opening installer URL ") );
            strT += m_installerURL;
            error( strT );
            pProgressDlg->Commands().Add( wxT("hide") );
			return 0;
		}

		break;
	}

    int     iLC = 0;
    while( !netHttpQueryResultsReady( m_hRequest, TRUE ) )
    {
        CUtils::MsgWait( 20, 1 );
        iLC++;
    }


    char            StatusCode[MAX_PATH];
    unsigned long   dwBufferLength = MAX_PATH-1, dwIndex=0;
    if( netHttpQueryInfo( m_hRequest, HTTP_QUERY_STATUS_CODE, StatusCode, &dwBufferLength, &dwIndex ) )
	{
        if( strncmp( StatusCode, "200", 3)  !=  0 )
        {
            wxString     strT( _("Unable to download installer from ") );
            strT += m_installerURL;
            error( strT );
            pProgressDlg->Commands().Add( wxT("hide") );
            return 0;
        }
    }

    bool            bResult;
    unsigned long   dwTotalRead = 0;
    char            cBuffer[1024];         // I'm only going to access 1K of info.

	// Read page into memory buffer.
    INTERNET_BUFFERSA   Output;
    memset( &Output, 0, sizeof(Output) );
    Output.dwStructSize = sizeof(Output);
    Output.lpvBuffer = cBuffer;
    Output.Next = &Output;
    Output.dwBufferLength = sizeof(cBuffer);
    bResult = netInternetReadFileExA( m_hRequest, &Output );

    if( bResult  ==  false )
	{
        wxString    strT( _("Unable to download update (1)") );
        error( strT );
        wxLogDebug( wxT("Update installer download failed (%d)"), GetLastError() );
        pProgressDlg->Commands().Add( wxT("hide") );
		return 0;
	}

    int     totalExpected = 4800000;    // approx size (4.7 MB) - just used for progress bar
    int     reportingCount = 0;         // writing 1K at time, report every 100K
    while( Output.dwBufferLength )
    {
        // write result
        dwTotalRead += Output.dwBufferLength;
        m_installFile.Write( cBuffer, Output.dwBufferLength );

        // read next
        Output.dwBufferLength = sizeof(cBuffer);
        bResult = netInternetReadFileExA( m_hRequest, &Output );

        if( bResult  ==  false )
		{
            wxString    strT( _("Unable to download update (2)") );
            error( strT  );
            wxLogDebug( wxT("Update installer failed after %d bytes (%d)"), (int) dwTotalRead, GetLastError() );
            pProgressDlg->Commands().Add( wxT("hide") );
			return 0;
		}

        if( pProgressDlg )
        {
            //Return immediately if user requested it
            if( pProgressDlg->IsCancelled( ) )
            {
                pProgressDlg->Commands().Add( wxT("hide") );
                return 0;
            }

            reportingCount++;
            if( reportingCount  ==  100 )
            {
                reportingCount = 0;
                //othewise update the current position
                long    nPercentage = (long)(((float)dwTotalRead / (float)totalExpected) * 100.0);
                if( nPercentage  >  99 )
                    nPercentage = 99;

                wxString    strValue = wxString::Format( wxT("%d"), nPercentage );
                pProgressDlg->Commands().Add( wxT("value"), wxString::Format( wxT("%d"), nPercentage ) );

                if( pProgressDlg->IsCancelled( ) )
                {
                    //  The progress dialog returns false if the Cancel button has been pressed
                    pProgressDlg->Commands().Add( wxT("hide") );
                    return 0;
                }
                ::wxMilliSleep( 2 ); // tiny artificial delay to give some feedback
            }
        }
    }

    wxLogDebug( wxT("Update installer download complete - %d bytes"), (int) dwTotalRead );
    pProgressDlg->Commands().Add( wxT("OK") );
    pProgressDlg->Commands().Add( wxT("hide") );

    return 1;
}


//*****************************************************************************
// clean up open file and handles from the save
bool InstallController::Cleanup( )
{
    m_installFile.Close( );

	// Close down connections.
    if( m_hRequest )
        netInternetCloseHandle( m_hRequest );
    netHttpClose( m_hInternetSession, m_hConnection );

	return true;
}


//*****************************************************************************
// executes the installer which installs the IIC client
bool InstallController::ExecInstaller( )
{
    wxLogDebug( wxT("entering InstallController::ExecInstaller( )") );
    if( ::wxFileExists( m_installerFileName ) )
	{
        LaunchInstaller( );
	}
	else
	{
        wxString    strT( _("Unable to launch update installer - file not found") );
        error( strT );
		return false;
	}

	return true;
}


//*****************************************************************************
void InstallController::LaunchInstaller( )
{
#if defined(LINUX) // really suse only

    wxString cmd = wxT("zen-installer ") + m_installerFileName;
    ::wxExecute( cmd, ( m_bWait ) ? wxEXEC_SYNC : wxEXEC_ASYNC );

#elif defined(MACOSX)

    int pos;
    wxStandardPathsBase& stdPaths = wxStandardPaths::Get();
    wxString execPath = wxFileName(stdPaths.GetExecutablePath( )).GetPath();
    if( (pos = execPath.Find( wxT(".app") ))  ==  wxNOT_FOUND )
    {
        error( _("Application update could not be applied (existing application not in expected format).") );
        return;
    }
    execPath = execPath.Left( pos );
    pos = execPath.Find( wxT('/'), true );
    execPath = execPath.Left( pos + 1);
    wxString upgradeScript = stdPaths.GetDataDir( ) + wxT("/meeting-upgrade.sh");
    wxString cmd = upgradeScript + wxT(" \"") +  m_installerFileName + wxT("\" \"") + execPath + wxT("\"");
    wxLogDebug( wxT("Update command: %s"), cmd.c_str() );

    // For the mac, we wait for the script to finish as it can't report errors to the user.
    // If it suceeds it should launch the new client.
    int res = ::wxExecute( cmd, wxEXEC_SYNC );
    if (res != 0)
    {
        error( _("Application update could not be applied.  Please download the latest version from your browser.") );
        return;
    }

#else
    
    ::wxExecute( m_installerFileName, ( m_bWait ) ? wxEXEC_SYNC : wxEXEC_ASYNC );

#endif
}


//*****************************************************************************
// removes the installer from its temp location
bool InstallController::CleanInstaller( )
{
    if( !m_installerFileName.IsEmpty( ) )
    {
        if( ::wxFileExists( m_installerFileName ) )
            ::wxRemoveFile( m_installerFileName );
        m_installerFileName.Empty( );
    }
    return true;
}


//*****************************************************************************
void InstallController::SetWaitForInstallToComplete( bool bWait )
{
    m_bWait = bWait;
}


//*****************************************************************************
void InstallController::SetMeetingIds( wxString &aMeetingId, wxString &aParticipantId )
{
    if( !aMeetingId.IsEmpty( )  &&  !aParticipantId.IsEmpty( ) )
    {
        m_MeetingId = aMeetingId;
        m_ParticipantId = aParticipantId;
    }
    else if( !aMeetingId.IsEmpty( ) )
    {
        m_MeetingId = aMeetingId;
        m_ParticipantId = aMeetingId;
    }
    else
    {
        m_MeetingId.Empty( );
        m_ParticipantId.Empty( );
    }
}

//*****************************************************************************
int SaveInstaller( void* pData, Progress1Dialog *pProgressDlg )
{
    wxLogDebug( wxT("entering static function SaveInstaller( )") );
    InstallController* anInstaller = (InstallController*)pData;

    return anInstaller->SaveInstaller( pData, pProgressDlg );
}


//*****************************************************************************
bool InstallController::DownloadInstaller( const wxString& sProgressTitle,
                                           int iFlags, const wxString& sConfirmPrompt, 
                                           wxWindow* pParent )
{
    //Create the progress dialog and set its various flags
    Progress1Dialog     dlg( sProgressTitle, sConfirmPrompt, 100, pParent, iFlags );

    dlg.SetReturnCode( wxID_CANCEL );

    UpdateDownloadParams    Info;
    Info.m_pProgressDlg     = &dlg;
    Info.m_pfnFunction      = ::SaveInstaller;
    Info.m_pData            = this;
    Info.m_bCompleted       = false;

    UpdateDownloadFileThread *pUpdateDownloadFileThread = new UpdateDownloadFileThread( &Info );
    if( pUpdateDownloadFileThread )
    {
        if( pUpdateDownloadFileThread->Create( )  !=  wxTHREAD_NO_ERROR )
        {
            wxLogError( wxT("Could not create UpdateDownload File thread!") );
            return false;
        }
        else
        {
            wxThreadError   thrErr;
            thrErr = pUpdateDownloadFileThread->Run( );

            int     iLC = 0;

            while( dlg.IsShown( ) )
            {
                iLC = 0;
                ::wxMilliSleep( 1 );
#ifdef WIN32
                while( wxGetApp( ).Pending( )  &&  iLC < 20 )
                {
                    iLC++;
                    wxGetApp( ).Dispatch( );
                }
#else
                while( wxGetApp( ).Pending( ) )
                {
                    iLC++;
                    wxGetApp( ).Dispatch( );
                }
#ifdef LINUX
                    if( g_main_context_iteration( NULL, false ) )
                        ;
#endif

#endif
            }
        }
    }

    return Info.m_bCompleted;
}


//*****************************************************************************
UpdateDownloadFileThread::UpdateDownloadFileThread( UpdateDownloadParams *pParams )
{
    m_pParams = pParams;
}


//*****************************************************************************
void UpdateDownloadFileThread::OnExit( )
{
    wxLogDebug( wxT("entering UpdateDownloadFileThread::OnExit( )") );
}


//*****************************************************************************
void *UpdateDownloadFileThread::Entry( )
{
    wxLogDebug( wxT("entering UpdateDownloadFileThread::Entry( )") );
    UpdateDownloadParams    *params = m_pParams;
    if (params->m_pfnFunction( params->m_pData, params->m_pProgressDlg ))
    {
        params->m_bCompleted = true;
    }
    return NULL;
}

