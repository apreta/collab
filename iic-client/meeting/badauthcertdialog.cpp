/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"
#include "badauthcertdialog.h"
#include "meetingcentercontroller.h"
#include <wx/statline.h>

//(*InternalHeaders(BadAuthCertDialog)
#include <wx/xrc/xmlres.h>
//*)


BEGIN_EVENT_TABLE(BadAuthCertDialog,wxDialog)
    //(*EventTable(BadAuthCertDialog)
    //*)
END_EVENT_TABLE()


BadAuthCertDialog::BadAuthCertDialog( wxWindow *parent, wxXmlNode *pRoot )
{
    m_parent = parent;

    //(*Initialize(BadAuthCertDialog)
    wxXmlResource::Get()->LoadObject(this,parent,_T("BadAuthCertDialog"),_T("wxDialog"));
    m_html1 = (wxHtmlWindow*)FindWindow(XRCID("ID_HTMLWINDOW1"));
    m_RadioAccept = (wxRadioButton*)FindWindow(XRCID("ID_BADAUTH_RADIO_ACCEPT"));
    m_RadioTemp = (wxRadioButton*)FindWindow(XRCID("ID_RADIO_TEMP"));
    m_RadioReject = (wxRadioButton*)FindWindow(XRCID("ID_BADAUTH_RADIO_REJECT"));
    //*)

    //m_RadioTemp->Hide( );

    CreateReport( pRoot );

    DisplayHTML( );
}


BadAuthCertDialog::~BadAuthCertDialog()
{
}

void BadAuthCertDialog::getField( wxString fName, wxString strIn, wxString &strOut )
{
    wxLogDebug( wxT("entering getField( %s, %s, %s )"), fName.c_str( ), strIn.c_str( ), strOut.c_str( ) );
    wxString    toFind, strRest, strT;
    toFind = fName + wxT("=");
    int         iIdx;
    iIdx = strIn.Find( toFind );
    iIdx += toFind.Len( );
    strRest = strIn.Mid( iIdx );
    strT = strRest.BeforeFirst( wxT(',') );

    strOut.Empty( );
    strOut.Append( strT );
}


void BadAuthCertDialog::CreateReport( wxXmlNode *pRoot )
{
    wxColour bg( m_parent->GetBackgroundColour() );

    wxString    strServer( wxT("strServer") );

    //  Example of strings in "issto" and "issby" nodes
    //    Subject's DN: C=US,ST=Massachusetts,L=Maynard,O=Sitescape,CN=iic.homedns.org
    //    Issuer's DN: C=US,ST=Massachusetts,L=Maynard,O=Sitescape,OU=Service,CN=General Server CA

    wxString    strOrg( wxT("org") );
//    wxString    strUnit( wxT("Production") );
    wxString    strSerial( wxT("strSerial") );

    wxString    strNameBy( wxT("nameby") );
    wxString    strOrgBy( wxT("orgby") );
    wxString    strUnitBy( wxT("unitby") );

    wxString    strIssued( wxT("strIssued") );
    wxString    strExpires( wxT("strExpires") );

//    wxString    strSHAFinger( wxT("sha finger belongs here") );
    wxString    strMD5Finger( wxT("md5 finger belongs here") );

    wxString    strT, strNName, strContent, strCert;
    strT = pRoot->GetContent( );
    strT = pRoot->GetName( );
    wxXmlNode   *pChild, *pgChild;
    pChild = pRoot->GetChildren( );
    while( pChild )
    {
        strNName = pChild->GetName( );
        strContent = pChild->GetContent( );
        pgChild = pChild->GetChildren( );
        if( pgChild )
        {
            strT = pgChild->GetName( );
            strContent = pgChild->GetContent( );
            if( strNName == wxT("certificate") )
                strCert = strContent;
            else if( strNName == wxT("issued") )
                strIssued = strContent;
            else if( strNName == wxT("expires") )
                strExpires = strContent;
            else if( strNName == wxT("serial") )
                strSerial = strContent;
            else if( strNName == wxT("md5") )
                strMD5Finger = strContent;
            else if( strNName == wxT("issto") )
            {
                getField( wxString( wxT("CN") ), strContent, strServer );
                getField( wxString( wxT("O") ), strContent, strOrg );
            }
            else if( strNName == wxT("issby") )
            {
                getField( wxT("CN"), strContent, strNameBy );
                getField( wxT("O"), strContent, strOrgBy );
                getField( wxT("OU"), strContent, strUnitBy );
            }
        }
        pChild = pChild->GetNext( );
    }

    HtmlGen r;
    r.clear( false );

    r.startHtml();

    wxString    blank;

    r.simpletext( _("Unable to verify the identity of ") );
    r.simpletext( strServer );
    r.line( _(" as a trusted site.") );
    r.linebreak( );
    r.line( _("Possible reasons for this error:") );
    r.line( _("- The Certificate Authority that issued the site's certificate is not recognized.") );
    r.line( _("- The site's certificate is incomplete due to a server misconfiguration.") );
    r.simpletext( _("- You are connected to a site pretending to be ") );
    r.simpletext( strServer );
    r.line( _(", possibly to obtain your confidential information.") );
    r.linebreak( );
    r.line( _("Please notify the site's webmaster about this problem.") );
    r.linebreak( );
    r.simpletext( _("Before accepting this certificate, you should examine this site's certificate carefully.  ") );
    r.simpletext( _("Are you willing to accept this certificate for the purpose of identifying the Web site ") );
    r.simpletext( strServer );
    r.line( _("?") );

    r.startTable( bg, _T("110%") );
    addRow( r, blank, blank );
    addRow( r, _("<b>Issued To</b>"), blank );
    addRow( r, _("Common Name (CN)"), strServer );
    addRow( r, _("Organization (O)"), strOrg );
//    addRow( r, _("Organizational Unit (OU)"), strUnit );
    addRow( r, _("Serial Number"), strSerial );
    addRow( r, blank, blank );

    addRow( r, _("<b>Issued By</b>"), blank );
    addRow( r, _("Common Name (CN)"), strNameBy );
    addRow( r, _("Organization (O)"), strOrgBy );
    addRow( r, _("Organizational Unit (OU)"), strUnitBy );

    addRow( r, blank, blank );
    addRow( r, _("<b>Validity</b>"), blank );
    addRow( r, _("Issued On"), strIssued );
    addRow( r, _("Expires On"), strExpires );

    addRow( r, blank, blank );
    addRow( r, _("<b>Fingerprints</b>"), blank );
//    addRow( r, _("SHA1 Fingerprint"), strSHAFinger );
    addRow( r, _("MD5 Fingerprint"),  strMD5Finger );

    r.endTable();

    r.endHtml();

    m_strHTML = r.buf();
}


void BadAuthCertDialog::addRow( HtmlGen & r, const wxString & label, const wxString & value )
{
    r.startTableRow();
    r.tableDataCell( label );
    r.tableDataCell( value );
    r.endTableRow();
}


void BadAuthCertDialog::DisplayHTML( )
{
    m_html1->SetBorders( 3 );
    m_html1->SetPage( m_strHTML );

#ifdef __WXMSW__
    // The HTML control seems to report incorrect sizes on Linux so don't use
    // Fit the HTML window to the size of its contents
    // int w = m_html1->GetInternalRepresentation()->GetWidth();
    int h = m_html1->GetInternalRepresentation()->GetHeight();
    int cw, ch, dw, dh;
    m_html1->GetSize( &cw, &ch );
    GetSize( &dw, &dh );
    SetSize( dw, dh + (h - ch) );
#endif

    m_RadioAccept->SetValue( false );
    m_RadioTemp->SetValue( false );
    m_RadioReject->SetValue( true );

    this->Center();
}
