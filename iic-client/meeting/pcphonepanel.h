/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef PCPHONEPANEL_H
#define PCPHONEPANEL_H

//(*Headers(PCPhonePanel)
#include <wx/stattext.h>
#include <wx/panel.h>
#include <wx/bmpbuttn.h>
#include <wx/button.h>
#include <wx/gauge.h>
//*)

class PCPhonePanel: public wxPanel
{
public:

	PCPhonePanel(wxWindow* parent,wxWindowID id = -1);
	virtual ~PCPhonePanel();

public:
    void        StartMeeting( const wxString &strParticipantID, const wxString &strParticipantName, const wxString &strMeetingID, const wxString &strMeetingTitle, const wxString &strPassword, bool fJoining = false );
    void        SetParticipantID( const wxString &strParticipantID )    { }
    void        EndMeeting( );
    void        ConnectionLost( );
    void        ConnectionReconnected( );
    void        SetMuted(bool muted);
    bool        IsMuted( )  { return m_fMuted; }

    public:
        /** Update the voip status text */
        void UpdateVoipStatus( wxCommandEvent& event );

        /** Update the voip levels */
        void UpdateVoipLevels( wxCommandEvent& event );


		//(*Identifiers(PCPhonePanel)
		//*)

	protected:

		//(*Handlers(PCPhonePanel)
		void OnMicrophoneButtonClick(wxCommandEvent& event);
		void OnVolumeButtonClick(wxCommandEvent& event);
		//*)

		//(*Declarations(PCPhonePanel)
		wxBitmapButton* m_VolumeButton;
		wxGauge* m_MicGauge;
		wxButton* m_AudioSettingsButton;
		wxButton* m_DialButton;
		wxButton* m_ConnectDisconnectButton;
		wxGauge* m_VolGauge;
		wxStaticText* m_StatusText;
		wxBitmapButton* m_MicrophoneButton;
		//*)

        bool m_fMuted;
        bool m_fSpeakerOff;

	private:

		DECLARE_EVENT_TABLE()
};

#endif
