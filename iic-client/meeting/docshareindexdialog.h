/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef DOCSHAREINDEXDIALOG_H
#define DOCSHAREINDEXDIALOG_H

//(*Headers(DocShareIndexDialog)
#include <wx/listctrl.h>
#include <wx/button.h>
#include <wx/dialog.h>
//*)

//#include "app.h"
#include "iicdoc.h"
#include "docshareview.h"


class DocShareIndexDialog: public wxDialog
{
public:

    DocShareIndexDialog( wxWindow* parent, PAGEVECTOR &saPages, bool fReadOnly );
    virtual ~DocShareIndexDialog();

    //(*Identifiers(DocShareIndexDialog)
    //*)

    void    SetReadOnly( bool fReadOnly );
    bool    IsDirty( )          { return m_fDirty; }

    PAGEVECTOR  m_saPages;

protected:

    //(*Handlers(DocShareIndexDialog)
    void OnClickOK(wxCommandEvent& event);
    void OnClickCancel(wxCommandEvent& event);
    void OnClickMoveUp(wxCommandEvent& event);
    void OnClickMoveDown(wxCommandEvent& event);
    void OnClickRename(wxCommandEvent& event);
    void OnClickDelete(wxCommandEvent& event);
    void OnEndLabelEdit(wxListEvent& event);
    //*)

    void    OnTimer( wxTimerEvent& event );         //  Update status of dialog buttons

    //(*Declarations(DocShareIndexDialog)
    wxButton* m_btnDelete;
    wxButton* m_btnRename;
    wxButton* m_btnMoveUp;
    wxButton* m_btnMoveDown;
    wxButton* m_btnOK;
    wxButton* m_btnCancel;
    wxListCtrl* m_listCtrl;
    //*)

private:
    bool        m_fReadOnly;
    bool        m_fDirty;

    wxTimer     m_timer;

    DECLARE_EVENT_TABLE()
};

#endif
