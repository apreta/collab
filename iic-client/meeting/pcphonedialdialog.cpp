/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "pcphonedialdialog.h"

//(*InternalHeaders(PCPhoneDialDialog)
#include <wx/bitmap.h>
#include <wx/font.h>
#include <wx/fontenum.h>
#include <wx/fontmap.h>
#include <wx/image.h>
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(PCPhoneDialDialog)
//*)

BEGIN_EVENT_TABLE(PCPhoneDialDialog,wxDialog)
	//(*EventTable(PCPhoneDialDialog)
	//*)
END_EVENT_TABLE()

PCPhoneDialDialog::PCPhoneDialDialog(wxWindow* parent,wxWindowID id)
{
	//(*Initialize(PCPhoneDialDialog)
	wxXmlResource::Get()->LoadObject(this,parent,_T("PCPhoneDialDialog"),_T("wxDialog"));
	m_TTPad1_Button = (wxButton*)FindWindow(XRCID("ID_TTPAD1_Button"));
	m_TTPad2_Button = (wxButton*)FindWindow(XRCID("ID_TTPAD2_BUTTON"));
	m_TTPad3_Button = (wxButton*)FindWindow(XRCID("ID_TTPAD3_BUTTON"));
	m_TTPad4_Button = (wxButton*)FindWindow(XRCID("ID_TTPAD4_BUTTON"));
	m_TTPad5_Button = (wxButton*)FindWindow(XRCID("ID_TTPAD5_BUTTON"));
	m_TTPad6_Button = (wxButton*)FindWindow(XRCID("ID_TTPAD6_BUTTON"));
	m_TTPad7_Button = (wxButton*)FindWindow(XRCID("ID_TTPAD7_BUTTON"));
	m_TTPad8_Button = (wxButton*)FindWindow(XRCID("ID_TTPAD8_BUTTON"));
	m_TTPad9_Button = (wxButton*)FindWindow(XRCID("ID_TTPAD9_BUTTON"));
	m_TTPadStar_Button = (wxButton*)FindWindow(XRCID("ID_TTPADSTAR_BUTTON"));
	m_TTPad0_Button = (wxButton*)FindWindow(XRCID("ID_TTPAD0_BUTTON"));
	m_TTPadPound_Button = (wxButton*)FindWindow(XRCID("ID_TTPADPOUND_BUTTON"));
	//*)
}

PCPhoneDialDialog::~PCPhoneDialDialog()
{
	//(*Destroy(PCPhoneDialDialog)
	//*)
}

