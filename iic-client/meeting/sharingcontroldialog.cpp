/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 *
 *
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */
#include "app.h"
#include "sharingcontroldialog.h"

//(*InternalHeaders(SharingControlDialog)
#include <wx/xrc/xmlres.h>
//*)
#include <wx/display.h>

//(*IdInit(SharingControlDialog)
//*)


//#include "../../services/common/common.h"
#include "controls/wx/wxTogBmpBtn.h"       //  for gTransColour
#include "../sharelib/share_api.h"
#include "sharecontrol.h"
#include "meetingmainframe.h"
#include "events.h"

static int kCloseNewChat    = 10000;
static int kFlashDelay      = 1000;
static int kOpeningDelay    = 50;

BEGIN_EVENT_TABLE(SharingControlDialog,wxFrame)
	//(*EventTable(SharingControlDialog)
	//*)

EVT_LEFT_DOWN( SharingControlDialog::OnLButtonDown )
EVT_LEFT_UP( SharingControlDialog::OnLButtonUp )
EVT_MOTION( SharingControlDialog::OnMotion )
EVT_MOUSE_CAPTURE_LOST( SharingControlDialog::OnCaptureLost )
END_EVENT_TABLE()


//*****************************************************************************
SharingControlDialog::SharingControlDialog(wxWindow* parent,wxWindowID id):
    m_timer( this )
{
    m_pMeetingWindow = parent;
    m_fFirstDisplay = true;
    m_fMtgIsVisible = false;
    m_fPresenting = false;
    m_fDragging = false;
    m_fOpeningStatus = false;
    m_fLeaveStatusClosed = false;
    m_iFlashDelay = 0;
    m_fRecording = false;
    m_fHolding = false;

    //(*Initialize(SharingControlDialog)
    wxXmlResource::Get()->LoadObject(this,parent,_T("SharingControlDialog"),_T("wxFrame"));
    StaticBitmap1 = (wxStaticBitmap*)FindWindow(XRCID("ID_STATICBITMAP1"));
    m_BtnViewMeeting = (wxButton*)FindWindow(XRCID("ID_SHARECTRL_BTN_VIEWMTG"));
    m_BtnStopSharing = (wxButton*)FindWindow(XRCID("ID_SHARECTRL_BTN_STOP"));
    m_BtnRefresh = (wxButton*)FindWindow(XRCID("ID_SHARECTRL_BTN_REFRESH"));
    m_BtnOptions = (wxButton*)FindWindow(XRCID("ID_SHARECTRL_BTN_OPTIONS"));
    m_ButtonHidden = (wxButton*)FindWindow(XRCID("ID_SHARECTRL_BTN_HIDDEN"));
    m_BtnOptions2 = (wxBitmapButton*)FindWindow(XRCID("ID_SHARECTRL_BTN_OPTIONS2"));
    m_StaticStatus = (wxStaticText*)FindWindow(XRCID("ID_SHARECTRL_STATIC_STATUS"));
    m_ButtonStatus = (wxButton*)FindWindow(XRCID("ID_SHARECTRL_BTN_STATUS"));
    
    Connect(XRCID("ID_SHARECTRL_BTN_VIEWMTG"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&SharingControlDialog::OnBtnViewMeetingClick);
    Connect(XRCID("ID_SHARECTRL_BTN_STOP"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&SharingControlDialog::OnBtnStopSharingClick);
    Connect(XRCID("ID_SHARECTRL_BTN_REFRESH"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&SharingControlDialog::OnBtnRefreshClick);
    Connect(XRCID("ID_SHARECTRL_BTN_OPTIONS"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&SharingControlDialog::OnBtnOptionsClick);
    Connect(XRCID("ID_SHARECTRL_BTN_OPTIONS2"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&SharingControlDialog::OnBtnOptionsClick);
    //*)

    // Connect the status button to its handler
    Connect(XRCID("ID_SHARECTRL_BTN_STATUS"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&SharingControlDialog::OnBtnStatusClick);

    StaticBitmap1->Connect( wxID_ANY, wxEVT_LEFT_DOWN, (wxObjectEventFunction)&SharingControlDialog::OnLButtonDown, NULL, this);
    StaticBitmap1->Connect( wxID_ANY, wxEVT_LEFT_UP,   (wxObjectEventFunction)&SharingControlDialog::OnLButtonUp,   NULL, this);
    StaticBitmap1->Connect( wxID_ANY, wxEVT_MOTION,    (wxObjectEventFunction)&SharingControlDialog::OnMotion,      NULL, this);
    StaticBitmap1->Connect( wxID_ANY, wxEVT_MOUSE_CAPTURE_LOST, 
                                                       (wxObjectEventFunction)&SharingControlDialog::OnCaptureLost, NULL, this);

    Connect( wxID_ANY, wxEVT_TIMER, wxTimerEventHandler(SharingControlDialog::OnTimer) );
    Connect( wxID_ANY, wxEVT_SHARE_TOOLBAR_STATUS, wxCommandEventHandler(SharingControlDialog::OnUpdateStatus) );

#ifndef __WXMAC__
    // If a window uses stay on top on Mac, then the app hangs if it uses a system alert dialog
    // (e.g. wxMessageDialog)
    SetWindowStyle(wxSTAY_ON_TOP|wxDIALOG_NO_PARENT);
#endif

    wxString    strT;
    SetTitle( strT );

    m_BtnOptions->Enable( false );

    // Doesn't repaint correctly when transparent
    //SetWindowStyle( GetWindowStyle() | wxTRANSPARENT_WINDOW );
    //Refresh( );

    m_pPopupMenu = new wxMenu();

    m_pPopupMenu->AppendCheckItem( XRCID("ID_SHARECTRL_RECORDING"), _("Recording"), _("Enable or disable meeting recording.") );
    m_pPopupMenu->AppendSeparator( );
    m_pPopupMenu->AppendCheckItem( XRCID("ID_SHARECTRL_LECTUREMODE"), _("Lecture Mode"), _("Enable or disable lecture mode.") );

    bool fHoldMode = OPTIONS.GetSystemBool( "EnableHoldMode", false );
    if (fHoldMode)
    {
        m_pPopupMenu->AppendSeparator( );
        m_pPopupMenu->AppendCheckItem( XRCID("ID_SHARECTRL_HOLDMODE"), _("Hold Mode"), _("Enable or disable hold mode.") );
    }

    bool fOperatorAssist = OPTIONS.GetSystemBool( "EnableOperatorAssist", false );
    if (fOperatorAssist)
    {
        m_pPopupMenu->AppendSeparator( );
        m_pPopupMenu->Append( XRCID("ID_SHARECTRL_OPERATOR"), _("Operator assistance"), _("Request operator assistance.") );
    }
}


//*****************************************************************************
SharingControlDialog::~SharingControlDialog()
{
    m_iFlashDelay = 0;
    m_timer.Stop( );
    Disconnect( wxID_ANY, wxEVT_TIMER, wxTimerEventHandler(SharingControlDialog::OnTimer) );

    delete m_pPopupMenu;
}


//*****************************************************************************
void SharingControlDialog::PostUpdateStatus( int iNewStatus )
{
    wxCommandEvent ev(wxEVT_SHARE_TOOLBAR_STATUS);
    ev.SetInt(iNewStatus);
    AddPendingEvent(ev);
}


//*****************************************************************************
void SharingControlDialog::OnUpdateStatus(wxCommandEvent& event)
{
    UpdateStatus(event.GetInt());
}


//*****************************************************************************
void SharingControlDialog::FirstDisplay( )
{
    if( m_fFirstDisplay )
    {
        int     x, y, w, h, myW, myH;
        ::wxClientDisplayRect( &x, &y, &w, &h );
        ::wxDisplaySize( &w, &h );
        GetSize( &myW, &myH );
        m_iPosX = w - myW - 80;
        m_iPosY = 0; // We'll adjust for Mac menu bar when we show window

        SetSize( m_iPosX, m_iPosY, myW, myH );
        m_fFirstDisplay = false;

        //  Do the tedious calculations only once - cache the various heights of the dialog
        int     btnW, btnH, statusW, statusH;
        m_ButtonStatus->GetSize( &btnW, &btnH );
        m_StaticStatus->GetSize( &statusW, &statusH );

        m_iMaxHeight = myH - statusH;
        m_iStatusHeight = myH - btnH;
        m_iMinHeight = m_iStatusHeight - statusH;

        m_iNormalWidth = myW;

        //  Make the status button the same height as the others...
        int     newW, newH;
        m_BtnViewMeeting->GetSize( &newW, &newH );
        m_ButtonStatus->SetSize( btnW, newH );
    }
}


//*****************************************************************************
void SharingControlDialog::UpdateStatus( int iNewStatus )
{
//    wxLogDebug( wxT("entering SharingControlDialog::UpdateStatus( iNewStatus == %d )"), iNewStatus );

    FirstDisplay( );

#ifdef WIN32
    // Testing reconnect revealed cases where toolbar was shown under meeting viewer (even though viewer window is
    // not topmost but we are), so reset topmost flag as status changes to try to keep the toolbar on top.
    HWND hwnd = (HWND) GetHandle();
    SetWindowPos(hwnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_ASYNCWINDOWPOS | SWP_NOACTIVATE | SWP_NOMOVE | SWP_NOSIZE);
#endif

    switch( iNewStatus )
    {
        case CB_Connected:
            m_StaticStatus->SetLabel( wxString( _("Connected") ) );
            m_StaticStatus->Hide( );
            m_iFlashDelay = 0;
            ShowStatus( false );
            if( m_pSC && m_pSC->IsViewing() )
            {
                m_BtnStopSharing->Enable( true );
            }
            break;

        case CB_Disconnected:
            ShowStatus( true );
            m_StaticStatus->SetLabel( wxString( _("Disconnected") ) );
            m_iFlashDelay = kFlashDelay;
            break;

        case CB_InternalError:
            ShowStatus( true );
            m_StaticStatus->SetLabel( wxString( _("Internal Error") ) );
            m_iFlashDelay = kFlashDelay;
            break;

        case CB_ConnectFailed:
            ShowStatus( true );
            m_StaticStatus->SetLabel( wxString( _("Connection Failed") ) );
            m_iFlashDelay = kFlashDelay;
            break;

        case CB_SessionEnded:
            ShowStatus( true );
            m_StaticStatus->SetLabel( wxString( _("Session Ended") ) );
            m_iFlashDelay = kFlashDelay;
            break;

        case CB_Connecting:
            ShowStatus( true );
            m_StaticStatus->SetLabel( wxString( _("Connecting...") ) );
            m_iFlashDelay = kFlashDelay;
            if( m_pSC )
                m_fPresenting = m_pSC->IsPresenter( );
            if( m_fPresenting )
            {
                SetLabel( m_BtnStopSharing, _("Stop\nSharing") );
                m_BtnStopSharing->Enable( true );
            }
            else
            {
                if (!m_pSC->IsFullScreen())
                {
                    SetLabel( m_BtnStopSharing, _("Full\nScreen") );
                    m_BtnStopSharing->Enable( false );
                }
                else
                {
                    SetLabel( m_BtnStopSharing, _("Exit Full\nScreen") );                    
                    m_BtnStopSharing->Enable( true );
                }
            }
            break;

        default:
            return;
    }

    if( m_iFlashDelay  !=  0 )
        m_timer.Start( m_iFlashDelay, wxTIMER_ONE_SHOT );
}


//*****************************************************************************
void SharingControlDialog::Handup( wxString strName, bool fHandup )
{
    if( fHandup )
    {
        wxString    strT( _(": hands up") );
        strT.Prepend( strName );
        m_ButtonStatus->SetLabel( strT );
        m_iFlashDelay = kOpeningDelay;
    }

    ShowStatus( fHandup, true );
}


//*****************************************************************************
void SharingControlDialog::Recording( bool fRecording )
{
    m_fRecording = fRecording;

    if ( m_fHolding )
        return;

    if( fRecording )
    {
        m_StaticStatus->SetLabel( _("Recording...") );
        m_iFlashDelay = kFlashDelay;
    }
    
    ShowStatus( fRecording );
    if( m_iFlashDelay  !=  0 )
        m_timer.Start( m_iFlashDelay, wxTIMER_ONE_SHOT );
}


//*****************************************************************************
void SharingControlDialog::Holding( bool fHolding )
{
    m_fHolding = fHolding;

    if( fHolding )
    {
        m_StaticStatus->SetLabel( _("Holding...") );
        m_iFlashDelay = kFlashDelay;
    }
    
    ShowStatus( fHolding );
    if( m_iFlashDelay  !=  0 )
        m_timer.Start( m_iFlashDelay, wxTIMER_ONE_SHOT );
}


//*****************************************************************************
void SharingControlDialog::Moderator( bool mod )
{
    m_BtnOptions->Enable( mod );
}


//*****************************************************************************
void SharingControlDialog::SetCommand( int ID, bool fCheck )
{
    m_pPopupMenu->Check( ID, fCheck );

}


//*****************************************************************************
void SharingControlDialog::NewChat( )
{
    wxString    strT( _("New chat, click to view.") );
    m_ButtonStatus->SetLabel( strT );
    m_iFlashDelay = kOpeningDelay;

    ShowStatus( true, true );
}


//*****************************************************************************
void SharingControlDialog::ShowToolbar( bool fMeetingVisible )
{
    Show( );
    CUtils::StayOnTop( this );

    wxRect dispPos;
    int dispIdx = wxDisplay::GetFromWindow(m_pMeetingWindow);
    if (dispIdx != wxNOT_FOUND)
    {
        wxDisplay disp(dispIdx);
        dispPos = disp.GetClientArea();
    }

#ifdef __WXGTK__
    // HACK: it seems like the window manager moves the window when it is re-displayed, but wxgtk doesn't know
    // about it so ignores the move to the same position.  Move it slightly first so it sees a different position.
    Move( m_iPosX + 1, m_iPosY + 1);
#endif
    Move( dispPos.GetX() + m_iPosX, dispPos.GetY() + m_iPosY );

    m_fMtgIsVisible = fMeetingVisible;
    SetLabel( m_BtnViewMeeting, fMeetingVisible ? _("Hide\nMeeting") : _("Show\nMeeting") );
}


//*****************************************************************************
void SharingControlDialog::HideToolbar( )
{
    GetScreenPosition( &m_iPosX, &m_iPosY );

    wxRect dispPos;
    int dispIdx = wxDisplay::GetFromWindow(this);
    if (dispIdx != wxNOT_FOUND)
    {
        wxDisplay disp(dispIdx);
        dispPos = disp.GetClientArea();
        m_iPosX -= dispPos.GetX();
        m_iPosY -= dispPos.GetY();
    }

    Hide( );

    if (!m_fMtgIsVisible)
    {
        m_fMtgIsVisible = true;

        //  We have a pointer to our ShareControl and it has a pointer to its MeetingMainFrame
        if( m_pSC  &&  m_pSC->m_pMMF )
            m_pSC->m_pMMF->Show( m_fMtgIsVisible );
    }
}


//*****************************************************************************
void SharingControlDialog::ShowStatus( bool fShow, bool fDramatic )
{
    FirstDisplay( );

    if ( !fShow && m_fHolding )
    {
        Holding(true);
        return;
    }
    
    if ( !fShow && m_fRecording )
    {
        Recording(true);
        return;
    }

    if( fShow )
    {
        if( fDramatic )
        {
            m_StaticStatus->Hide( );
            m_ButtonStatus->Show( );
            m_fOpeningStatus = true;
            SetSize( m_iNormalWidth, m_iMinHeight );
            m_timer.Start( m_iFlashDelay, wxTIMER_ONE_SHOT );
        }
        else
        {
            m_StaticStatus->Show( );
            m_ButtonStatus->Hide( );
            m_fOpeningStatus = false;
            SetSize( m_iNormalWidth, m_iStatusHeight );
        }
    }
    else
    {
        if( m_timer.IsRunning( ) )
            m_timer.Stop( );
        m_iFlashDelay = 0;
        m_StaticStatus->Hide( );
        m_ButtonStatus->Hide( );
        m_fOpeningStatus = false;
        m_fLeaveStatusClosed = true;
        SetSize( m_iNormalWidth, m_iMinHeight );
    }
}


//*****************************************************************************
void SharingControlDialog::SetLabel(wxButton *button, wxString label)
{
#ifdef __WXMAC__
    // For some reason Carbon bevel buttons don't handle newlines
    // Note that all the labels in the XRC have an embedded newline--this causes
    // wxMac to use a toggle button instead of a push button (which is rounded and
    // looks funny in the toolbar).  However, since carbon won't handle a newline
    // in a toggle button label, we remove the newlines here.
    label.Replace(wxT("\n"), wxT(" "), TRUE);
#endif
    button->SetLabel(label);
}


//*****************************************************************************
//
//    Event Handlers
//
//*****************************************************************************
void SharingControlDialog::OnBtnViewMeetingClick(wxCommandEvent& event)
{
    wxLogDebug( wxT("entering SharingControlDialog::OnBtnViewMeetingClick( )") );

    m_fMtgIsVisible = !m_fMtgIsVisible;

    if( m_fPresenting && m_pSC && m_pSC->IsDesktopShared())
    {
        //  We have a pointer to our ShareControl and it has a pointer to its MeetingMainFrame
        if( m_pSC &&  m_pSC->m_pMMF )
            m_pSC->m_pMMF->Show( m_fMtgIsVisible );
    }
    else
    {
        if( m_pSC  &&  m_pSC->m_pMMF )
            m_pSC->m_pMMF->MinimizeChatAndPart( m_fMtgIsVisible );
    }

    wxString    strT;
    if( m_fMtgIsVisible )
        strT.Append( _("Hide\nMeeting") );
    else
        strT.Append( _("View\nMeeting") );
    SetLabel( m_BtnViewMeeting, strT );
}


//*****************************************************************************
void SharingControlDialog::OnBtnStatusClick(wxCommandEvent& event)
{
    wxLogDebug( wxT("entering SharingControlDialog::OnBtnStatusClick( )") );

    if( !m_fMtgIsVisible )
    {
        m_fMtgIsVisible = true;
        if( m_fPresenting )
        {
            //  We have a pointer to our ShareControl and it has a pointer to its MeetingMainFrame
            if( m_pSC  &&  m_pSC->m_pMMF )
            {
                m_pSC->m_pMMF->Show( m_fMtgIsVisible );
                m_pSC->m_pMMF->SetFocus( );
            }
        }
        else
        {
            if( m_pSC  &&  m_pSC->m_pMMF )
            {
                m_pSC->m_pMMF->MinimizeChatAndPart( m_fMtgIsVisible );
                m_pSC->m_pMMF->SetFocus( );
            }
        }
    }

    wxString    strT;
    if( m_fMtgIsVisible )
        strT.Append( _("Hide\nMeeting") );
    else
        strT.Append( _("View\nMeeting") );
    SetLabel( m_BtnViewMeeting, strT );

    ShowStatus( false );
}


//*****************************************************************************
void SharingControlDialog::OnBtnStopSharingClick(wxCommandEvent& event)
{
    wxLogDebug( wxT("entering SharingControlDialog::OnBtnStopSharingClick( )") );

    if( m_fPresenting )
    {
        if( m_pSC )
        {
            if( m_pSC->m_pMMF )
            {
                m_pSC->m_pMMF->Show( true );
                //  Ensure Participant and Chat panels are visible when ending the share
                m_pSC->m_pMMF->MinimizeChatAndPart( true );
            }
            m_pSC->ShareEnd( );
        }
    }
    else
    {
        if (m_pSC->IsFullScreen())
        {
            m_pSC->ViewerFullScreen(false);
            SetLabel( m_BtnStopSharing, _("Full\nScreen") );
        }
        else
        {
            m_pSC->ViewerFullScreen(true);
            SetLabel( m_BtnStopSharing, _("Exit Full\nScreen") );
        }
    }
}


//*****************************************************************************
void SharingControlDialog::OnBtnRefreshClick(wxCommandEvent& event)
{
    wxLogDebug( wxT("entering SharingControlDialog::OnBtnRefreshClick( )") );
    if( m_pSC )
        m_pSC->ShareRefresh();
}


//*****************************************************************************
void SharingControlDialog::OnTimer( wxTimerEvent& event )
{
//    wxLogDebug( wxT("entering SharingControlDialog::OnTimer( )") );

    int     iInt = event.GetInterval( );
    if( iInt == kCloseNewChat )
    {
        m_iFlashDelay = 0;
        ShowStatus( false );
        return;
    }

    if( m_fOpeningStatus )
    {
        int     myW, myH;
        GetSize( &myW, &myH );
        if( myH  <  m_iMaxHeight )
        {
            SetSize( myW, myH + 2 );
        }
        else
        {
            m_fOpeningStatus = false;
            m_iFlashDelay = kCloseNewChat;
        }
    }
    else
    {
        if( m_StaticStatus->IsShown( )  ||  m_fLeaveStatusClosed )
        {
            m_StaticStatus->Hide( );
            m_fLeaveStatusClosed = false;
        }
        else
            m_StaticStatus->Show( );
    }

    if( m_iFlashDelay  !=  0 )
        m_timer.Start( m_iFlashDelay, wxTIMER_ONE_SHOT );
}


//*****************************************************************************
void SharingControlDialog::OnLButtonDown( wxMouseEvent& event )
{
    wxLogDebug( wxT("entering SharingControlDialog::OnLButtonDown( )") );

    if ( !m_fDragging )
    {
        m_fDragging = true;
        event.GetPosition( &m_iOffsetX, &m_iOffsetY );

        SetFocus( );
        CaptureMouse( );
    }
    else
        event.Skip();
}


//*****************************************************************************
void SharingControlDialog::OnLButtonUp( wxMouseEvent& event )
{
    wxLogDebug( wxT("entering SharingControlDialog::OnLButtonUp( )") );

    if ( m_fDragging )
        ReleaseMouse( );

    m_fDragging = false;
}


//*****************************************************************************
void SharingControlDialog::OnMotion( wxMouseEvent& event )
{
    if( m_fDragging )
    {
        wxPoint pt = ::wxGetMousePosition();
        int ix = pt.x - m_iOffsetX;
        int iy = pt.y - m_iOffsetY;
        Move( ix, iy );
    }
}


//*****************************************************************************
void SharingControlDialog::OnCaptureLost( wxMouseCaptureLostEvent& event )
{
    wxLogDebug( wxT("entering SharingControlDialog::OnCaptureLost( )") );
    m_fDragging = false;
}


//*****************************************************************************
void SharingControlDialog::OnBtnOptionsClick(wxCommandEvent& event)
{

    m_pPopupMenu->Check( XRCID("ID_SHARECTRL_RECORDING"), m_fRecording );
    
    PopupMenu(m_pPopupMenu);//, point.x, point.y);
    
}
