/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
// InstallController.h: interface for the InstallController class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(INSTALLCONTROLLER_H)
#define INSTALLCONTROLLER_H

#include <wx/file.h>
#include <wx/filename.h>
#include <wx/msgdlg.h>
#include <wx/uri.h>
#include <wx/utils.h>

#include "../netlib/nethttp.h"


class Progress1Dialog;


// The function declaration of the function to be called in a secondary thread with a progress dialog
typedef int (FUNCTION_WITH_PROGRESS)(void* pData, Progress1Dialog* pProgressDlg);


//*****************************************************************************
typedef struct _UpdateDownloadParams
{
    Progress1Dialog         *m_pProgressDlg;        //  Progress1Dialog to provide feedback to user
    FUNCTION_WITH_PROGRESS  *m_pfnFunction;         //  Function to call to do the work
    void                    *m_pData;               //  data to pass to the caller function
    bool                     m_bCompleted;          //  download was completed
}   UpdateDownloadParams;


//*****************************************************************************
class UpdateDownloadFileThread : public wxThread
{
public:
    UpdateDownloadFileThread( UpdateDownloadParams *pParams );

    // thread execution starts here
    virtual void *Entry();

    // called when the thread exits - whether it terminates normally or is
    // stopped with Delete() (but not when it is Kill()ed!)
    virtual void OnExit( );

private:
    UpdateDownloadParams    *m_pParams;
};


//*****************************************************************************
class InstallController
{
public:
    InstallController( );
	virtual ~InstallController( );

    void    SetWaitForInstallToComplete( bool bWait );

    void    SetInstallerURL( wxString installerURL)    { m_installerURL = installerURL; }

    void    SetMeetingIds( wxString &aMeetingId, wxString &aParticipantId );

	// saves the installer from the passed URL to a temp location
    int     SaveInstaller(void* pData, Progress1Dialog* pProgressDlg );

	// cleans up open resources from the save
	bool    Cleanup( );

	// executes the installer which installs the IIC client
    bool    ExecInstaller( );

	// removes the installer from its temp location
    bool    CleanInstaller( );

    bool    DownloadInstaller( const wxString& sProgressTitle,
                               int iFlags, const wxString& sConfirmPrompt, 
                               wxWindow* pParent );
                               
protected:
    wxString    m_MeetingId;
    wxString    m_ParticipantId;

private:
    bool        m_bWait;

    void        error( const wxString &functionName );

    wxString    genTempFile( );

	// launches installer, waits for it to complete
    void        LaunchInstaller( );

    wxString    m_installerURL;         // URL with installer
	wxString    m_installerFileName;    // generated temp name for file to store into
    wxFile      m_installFile;          // wxFile to temp file

#ifdef WIN32
	HINTERNET   m_hInternetSession;     // HINTERNET to open session
	HINTERNET   m_hConnection;
    HINTERNET   m_hRequest;
#else
    HSESSION     m_hInternetSession;     // HINTERNET to open session
    HCONNECTION  m_hConnection;
    HREQUEST     m_hRequest;
#endif

};

#endif
