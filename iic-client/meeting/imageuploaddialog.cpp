#include "app.h"
#include "meetingcentercontroller.h"
#include "imageuploaddialog.h"
#include "filemanager.h"

#include <wx/file.h>
#include <wx/html/htmlwin.h>


//(*InternalHeaders(ImageUploadDialog)
#include <wx/xrc/xmlres.h>
//*)

ImagePanel::ImagePanel(wxWindow* parent, wxSize size, wxString title, bool singleImage) :
    wxPanel(parent, -1, wxDefaultPosition, size)
{
    wxLogDebug(wxT("ImagePanel: %d %d"), size.GetWidth(), size.GetHeight());
    
    m_title = title;
    m_fileManager = NULL;
    m_singleImage = singleImage;
    m_FileDisplay = new wxHtmlWindow(this, -1, wxDefaultPosition, size, wxHW_SCROLLBAR_AUTO|wxHW_NO_SELECTION);

    Connect(wxID_ANY, wxEVT_COMMAND_HTML_LINK_CLICKED, wxHtmlLinkEventHandler(ImagePanel::OnLinkClicked));

    wxBoxSizer *topsizer = new wxBoxSizer( wxVERTICAL );
    topsizer->Add(m_FileDisplay, 1, wxEXPAND);
    SetSizer(topsizer);
}

ImagePanel::~ImagePanel()
{
    
}

void ImagePanel::Initialize(FileManager* mgr)
{
    m_fileManager = mgr;
    PopulateFiles();
}

void ImagePanel::AddFile(bool crop)
{
    wxFileDialog dlg(this, _("Select image file"));
    dlg.SetWildcard(wxT("JPEG files (*.jpg)|*.jpg|PNG files (*.png)|*.png|BMP files (*.bmp)|*.bmp|GIF files (*.gif)|*.gif"));
    if( dlg.ShowModal( )  ==  wxID_OK )
    {
        wxImage image;
        if (!image.LoadFile(dlg.GetPath()))
        {
            wxMessageBox( _("Unable to read selected image file.\nPlease select a different file."), _("Error!"), wxICON_ERROR, this);
            return;
        }

        wxImage uploadImage = ResizeImage(image, ProfileWidth, ProfileHeight, crop);

        wxString tempFile = m_fileManager->GetTempFile(wxT("jpg"));
        uploadImage.SaveFile(tempFile);
        
        wxFileName fn(dlg.GetPath());

        if (m_singleImage && m_fileManager->GetFileCount() > 0)
            m_fileManager->RemoveFile(0);
        m_fileManager->AddFile(-1, tempFile, fn.GetName());
        PopulateFiles();
    }
}


wxImage ImagePanel::ResizeImage(wxImage& image, int width, int height, bool crop)
{
    int w = image.GetWidth();
    int h = image.GetHeight();
    int targetRatio = width/height;
    if ((w/h > targetRatio  && !crop) || (w/h <= targetRatio && crop))
    {
        // Image is wider or we are cropping, pad height first
        int adjh = (double)w / (double)width * height;
        image.Resize(wxSize(w, adjh), wxPoint(0, (adjh-h)/2), 255, 255, 255);
    }
    else
    {
        // Image is taller or we aren't cropping, pad width first
        int adjw = (double)h / (double)height * width;
        image.Resize(wxSize(adjw, h), wxPoint((adjw-w)/2,0), 255, 255, 255);
    }
    
    wxImage scaled = image.Scale(width, height, wxIMAGE_QUALITY_HIGH);
        
    return scaled;
}

void ImagePanel::OnLinkClicked(wxHtmlLinkEvent &event)
{
    wxString url = event.GetLinkInfo().GetHref();
    wxLogMessage(wxT("The url '%s' has been clicked!"), url.c_str());

    wxString arg;
    long idx;
    if (url.StartsWith(wxT("del:"), &arg))
    {
        arg.ToLong(&idx);
        m_fileManager->RemoveFile(idx);
        PopulateFiles();
    }
    else if (url.StartsWith(wxT("up:"), &arg))
    {
        arg.ToLong(&idx);
        m_fileManager->MoveFile(idx, -1);
        PopulateFiles(); 
    }
    else if (url.StartsWith(wxT("down:"), &arg))
    {
        arg.ToLong(&idx);
        m_fileManager->MoveFile(idx, +1);
        PopulateFiles();        
    }
    else if (url.StartsWith(wxT("edit:"), &arg))
    {
        arg.ToLong(&idx);
        FileDetails* info = m_fileManager->GetFile(idx);
        wxTextEntryDialog dlg(this, _("Enter a new title for the image:"), m_title, info->m_Title);
        if (dlg.ShowModal() == wxID_OK)
        {
            info->m_Title = dlg.GetValue();
            m_fileManager->SetDirty();
            PopulateFiles();
        }
    }
}

void ImagePanel::PopulateFile(FileDetails * file, wxString& html, int index)
{
    wxString item;
    
    if (!m_singleImage)
    {
        item = 
            _("<td width=25% valign=top bgcolor='#E0E0E0'>"
                "<table>"
                    "<tr>"
                        "<td>"
                            "<table>"
                                "<tr>"
                                    "<td><img align=center src='IMAGE_FILE'/></td>"
                                    "<td align=left>"
                                        "<a href='del:INDEX'><img src='res/list-remove.png'/></a><br>"
                                        "<a href='up:INDEX'><img src='res/go-up.png'/></a><br>"
                                        "<a href='down:INDEX'><img src='res/go-down.png'/></a>"
                                    "</td>"
                                "</tr>"
                            "</table>"
                        "</td>"
                    "</tr>"
                    "<tr>"
                        "<td colspan=2><p align=center><a href='edit:INDEX'><img src='res/edit-16.png'/></a>&nbsp;<font size=3>TITLE</font></p>"
                        "</td>"
                    "</tr>"
                "</table>"
              "</td>");
    }
    else
    {
        item =
            _("<td align=center valign=top bgcolor='#E0E0E0'>"
                "<img align=center src='IMAGE_FILE'/>"
              "</td>");

    }

    item.Replace(wxT("IMAGE_FILE"), file->m_LocalPath);
    item.Replace(wxT("TITLE"), file->m_Title);
    item.Replace(wxT("INDEX"), wxString::Format(wxT("%d"), index));
    
    html.Append(item);
}

void ImagePanel::PopulateFiles()
{
    int i;
    wxString html = wxT("<html><body><table width=100% ><tr>");
    
    for (i=0; i < m_fileManager->GetFileCount(); i++)
    {
        if (i > 1 && i % RowSize == 0)
            html.Append(wxT("</tr><tr>"));
        FileDetails *file = m_fileManager->GetFile(i);
        PopulateFile(file, html, i);
    }

    while (!m_singleImage && i++ % RowSize != 0)
        html.Append(wxT("<td width=25% />"));

    html.Append(wxT("</tr></table></body></html>"));
    
    int x,y;
    m_FileDisplay->GetViewStart(&x, &y);
    m_FileDisplay->SetPage(html);
    m_FileDisplay->Scroll(x,y);
}

//(*IdInit(ImageUploadDialog)
//*)

BEGIN_EVENT_TABLE(ImageUploadDialog,wxDialog)
	//(*EventTable(ImageUploadDialog)
	//*)
END_EVENT_TABLE()

ImageUploadDialog::ImageUploadDialog(wxWindow* parent, const wxString& caption, const wxString& repoPath, const wxString& storeName, bool singleImage)
{
    m_singleImage = singleImage;
    m_repoPath = repoPath;
    m_storeName = storeName;
    
	//(*Initialize(ImageUploadDialog)
	wxXmlResource::Get()->LoadObject(this,parent,_T("ImageUploadDialog"),_T("wxDialog"));
	m_ImageContainer = (wxPanel*)FindWindow(XRCID("ID_PANEL2"));
	m_AddButton = (wxButton*)FindWindow(XRCID("ID_ADD_BUTTON"));
	m_CropImageCheck = (wxCheckBox*)FindWindow(XRCID("ID_CROP_CHECKBOX"));
	
	Connect(XRCID("ID_ADD_BUTTON"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&ImageUploadDialog::OnAddButtonClick);
	//*)

    wxSize size = m_ImageContainer->GetSize();
    m_imagePanel = new ImagePanel(m_ImageContainer, size, caption, singleImage);

    wxBoxSizer *panelsizer = new wxBoxSizer( wxVERTICAL );
    panelsizer->Add(m_imagePanel, 1, wxEXPAND);
    m_ImageContainer->SetSizer(panelsizer);
    
	SetTitle(caption);

    m_fileManager = new FileManager(repoPath);
    m_imagePanel->Initialize(m_fileManager);

    m_CropImageCheck->SetValue(wxGetApp().Options().GetUserBool("AutoCropPictures", true));
    
    if (!singleImage)
    {
        SetSize(640, 600);
        Center();
        m_AddButton->SetLabel(_("Add picture..."));
     }
}

ImageUploadDialog::~ImageUploadDialog()
{
	//(*Destroy(ImageUploadDialog)
	//*)
}

bool ImageUploadDialog::ShowDialog()
{
    bool res = false;
    
    if (!LoadImages() && m_fileManager->GetLastError() != FileManager::FileNotFound)
    {
        wxString msg = _("Unable to connect to the document server.\nPlease try selecting a profile picture later.");
        wxMessageBox(msg, GetTitle(), wxICON_ERROR|wxOK, this);
        return res;
    }
    
    if (ShowModal() == wxID_OK)
    {
        if (StoreImages())
        {
            res = true;
        }
        else
        {
            wxString msg = _("Unable to send picture to the document server.\nPlease try again later.");
            wxMessageBox(msg, GetTitle(), wxICON_ERROR|wxOK, this);        
            res = false;
        }
    }

    wxGetApp().Options().SetUserBool("AutoCropPictures", m_CropImageCheck->IsChecked());
    return res;
}

bool ImageUploadDialog::LoadImages()
{
    wxString existingImage;

    if (m_singleImage)
    {
        if (m_fileManager->DownloadFile(m_storeName, existingImage, true))
        {
            m_fileManager->AddFile(-1, existingImage, wxT(""));
        }
        else
            return false;
    }
    else
    {
        if (!m_fileManager->LoadCollection(m_storeName))
            return false;
    }
    
    m_imagePanel->PopulateFiles();
    return true;
}

bool ImageUploadDialog::StoreImages()
{
    if (m_singleImage && m_fileManager->GetFileCount() > 0)
    {
        FileDetails* info = m_fileManager->GetFile(0);
        if (!m_fileManager->UploadFile(m_storeName, info->m_LocalPath, true))
            return false;
    }
    
    if (!m_singleImage)
    {
        if (!m_fileManager->StoreAsync(m_storeName, this, true))
            return false;
    }
    return true;
}

void ImageUploadDialog::OnAddButtonClick(wxCommandEvent& event)
{
    m_imagePanel->AddFile(m_CropImageCheck->IsChecked());
}
