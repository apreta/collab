/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef PARTICIPANTPANEL_H
#define PARTICIPANTPANEL_H

//(*Headers(ParticipantPanel)
#include <wx/panel.h>
#include <wx/treelistctrl.h>
//*)
#include "controls/wx/treelistctrl.h"
#include "controls/wx/wxTogBmpBtn.h"
#include "imagelistcomposite.h"
#include "events.h"
#include "controller.h"

#include "meetingview.h"

#include <map>


class MeetingMainFrame;
class ShareControl;

typedef std::map<wxString,wxTreeItemId> wxString2TreeItemIDMap;

WX_DEFINE_ARRAY_PTR(participant_t, participant_tArray);
WX_DEFINE_ARRAY_PTR(submeeting_t, submeeting_tArray);


//*****************************************************************************
class BaseTreeItemData : public wxTreeItemData
{
    public:
        virtual bool IsParticipant() = 0;
};


//*****************************************************************************
class ParticipantTreeItemData : public BaseTreeItemData
{
    public:
        ParticipantTreeItemData( const participant_t p) : m_p(p) { m_fHandUp = (p->status & StatusHandUp); }
        bool IsParticipant() { return true; }
        participant_t   m_p;
        bool            m_fHandUp;
};


//*****************************************************************************
class SubmeetingTreeItemData : public BaseTreeItemData
{
    public:
        SubmeetingTreeItemData( const submeeting_t s, bool fIsNotInMeeting = false) : m_s(s), m_fNotInMeeting(fIsNotInMeeting) { }
        bool IsParticipant() { return false; }
        submeeting_t    m_s;
        bool            m_fNotInMeeting;
};


//*****************************************************************************
class ParticipantPanel: public wxPanel
{
public:
    ParticipantPanel(wxWindow* parent,wxWindowID id = -1,ShareControl *pShareControl=NULL);
    virtual ~ParticipantPanel();

    void        StartMeeting( const wxString &strParticipantID, const wxString &strParticipantName, const wxString &strMeetingID, const wxString &strMeetingTitle, const wxString &strPassword, bool fJoining = false );
    void        EndMeeting( );
    void        ConnectionLost( );
    void        ConnectionReconnected( );

    //(*Identifiers(ParticipantPanel)
    //*)

    wxTreeListCtrl* GetParticipantTree( ) const { return m_ParticipantTree; }
    int             GetSelectedParticipants( participant_tArray &outArray );
    participant_t   GetFirstSelectedParticipant( );
    int             GetAllParticipants( participant_tArray &outArray, bool fIncludeMain = true );
    int             GetAllSubmeetings( submeeting_tArray &outArray, bool fIncludeMain = true );
    bool            IsPresenter( ) const { return m_fCurrentlyPresenter; }
    participant_t   GetSingleSelection( );

protected:

    //(*Handlers(ParticipantPanel)
    void OnClickBtnHandUp(wxCommandEvent& event);
    void OnClickBtnOperator(wxCommandEvent& event);
    void OnClickBtnRecord(wxCommandEvent& event);
    void OnClickBtnLecture(wxCommandEvent& event);
    void OnClickBtnHold(wxCommandEvent& event);
    void OnClickBtnMeeting(wxCommandEvent& event);
    void OnClickBtnMyPhone(wxCommandEvent& event);
    void OnClickBtnShare(wxCommandEvent& event);
    void OnClickBtnInvite(wxCommandEvent& event);
    void OnClickBtnParticipant(wxCommandEvent& event);
    //*)

    //  Context menu handlers
    void OnContextMenu( wxContextMenuEvent& event );
    void OnTreeContextMenu( wxTreeEvent& event );
    void OnMenuContVolume(wxCommandEvent& event);
    void OnMenu1To1Call( wxCommandEvent& event );
    void OnMenuConfCall( wxCommandEvent& event );
    void OnMenuEmail( wxCommandEvent& event );
    void OnMenuIM( wxCommandEvent& event );

    //  Main menu handlers
    void OnMenuChangeOptions( wxCommandEvent& event );
    void OnMenuShowDetails( wxCommandEvent& event );
    void OnMenuLock( wxCommandEvent& event );
    void OnMenuLectureMode( wxCommandEvent& event );
    void OnMenuHoldMode( wxCommandEvent& event );
    void OnMenuOperator( wxCommandEvent& event );
    void OnMenuNoJoinTones( wxCommandEvent& event );
    void OnMenuRollCall( wxCommandEvent& event );
    void OnMenuHandUp( wxCommandEvent& event );
    void OnMenuRecordMeeting( wxCommandEvent& event );
    void OnMenuPreviewRecording( wxCommandEvent& event );
    void OnMenuResetRecording( wxCommandEvent& event );
    void OnMenuExitMeeting( wxCommandEvent& event );
    void OnMenuCallMe( wxCommandEvent& event );
    void OnMenuMuteMe( wxCommandEvent& event );
    void OnMenuVolumeMe( wxCommandEvent& event );
    void OnMenuHangUpMe( wxCommandEvent& event );
    void OnMenuInviteNew( wxCommandEvent& event );
    void OnMenuInviteSelected( wxCommandEvent& event );
    void OnMenuMakePresenter( wxCommandEvent& event );
    void OnMenuMakeModerator( wxCommandEvent& event );
    void OnMenuUnmakeModerator( wxCommandEvent& event );
    void OnMenuGrantRemoteControl( wxCommandEvent& event );
    void OnMenuRevokeRemoteControl( wxCommandEvent& event );
    void OnMenuRemoveSelected( wxCommandEvent& event );
    void OnMenuMoveAllToMain( wxCommandEvent& event );
    void OnMenuMoveSelToNewSub( wxCommandEvent& event );
    void OnMenuMoveSelToSub( wxCommandEvent& event );
    void OnMenuRemoveSub( wxCommandEvent& event );
    void OnMenuMakeMePresenter( wxCommandEvent& event );
    void OnMenuShareDesktop( wxCommandEvent& event );
    void OnMenuShareApplication( wxCommandEvent& event );
    void OnMenuSharePresentation( wxCommandEvent& event );
    void OnMenuShareDocument( wxCommandEvent& event );
    void OnMenuShareDocumentExplorer( wxCommandEvent& event );
    void OnMenuShareStop( wxCommandEvent& event );
    void OnMenuOpenWhiteboard( wxCommandEvent& event );
    void OnMenuShareScale( wxCommandEvent& event );
    void OnMenuShareScroll( wxCommandEvent& event );
    void OnMenuShareFitToWidth( wxCommandEvent& event );

    //  Popup menu handlers
    void OnMenuHandDown(wxCommandEvent& event);
    void OnMenuCallParticipant(wxCommandEvent& event);
    void OnMenuMuteParticipant(wxCommandEvent& event);
    void OnMenuUnmuteParticipant(wxCommandEvent& event);
    void OnMenuVolumeParticipant(wxCommandEvent& event);
    void OnMenuHangUpParticipant(wxCommandEvent& event);

    void OnMenuLockUpdateUI( wxUpdateUIEvent &event );
    void OnMenuHandUpUpdateUI( wxUpdateUIEvent &event );
    void OnMenuCallMeUpdateUI( wxUpdateUIEvent &event );
    void OnMenuCallMeItemUpdateUI( wxUpdateUIEvent &event );
    void OnMenuMuteMeUpdateUI( wxUpdateUIEvent &event );
    void OnMenuVolumeMeUpdateUI( wxUpdateUIEvent &event );
    void OnMenuHangUpMeUpdateUI( wxUpdateUIEvent &event );
    void OnMenuMakeMePresenterUpdateUI( wxUpdateUIEvent &event );
    void OnMenuShareDesktopUpdateUI( wxUpdateUIEvent &event );
    void OnMenuShareApplicationUpdateUI( wxUpdateUIEvent &event );
    void OnMenuShareDocumentUpdateUI( wxUpdateUIEvent &event );
    void OnMenuShareStopUpdateUI( wxUpdateUIEvent &event );
    void OnMenuOpenWhiteboardUpdateUI( wxUpdateUIEvent &event );
    void OnMenuShareScaleUpdateUI( wxUpdateUIEvent &event );
    void OnMenuShareScrollUpdateUI( wxUpdateUIEvent &event );
    void OnMenuShareFitToWidthUpdateUI( wxUpdateUIEvent &event );

    void OnMenuInviteNewUpdateUI( wxUpdateUIEvent &event );
    void OnMenuInviteSelUpdateUI( wxUpdateUIEvent &event );
    void OnMenuMakePresUpdateUI( wxUpdateUIEvent &event );
    void OnMenuMakeModeratorUpdateUI( wxUpdateUIEvent &event );
    void OnMenuUnmakeModeratorUpdateUI( wxUpdateUIEvent &event );
    void OnMenuGrantRemoteControlUpdateUI( wxUpdateUIEvent &event );
    void OnMenuRevokeRemoteControlUpdateUI( wxUpdateUIEvent &event );
    void OnMenuHandDownUpdateUI( wxUpdateUIEvent &event );
    void OnMenuCallParticipantUpdateUI( wxUpdateUIEvent &event );
    void OnMenuMuteParticipantUpdateUI( wxUpdateUIEvent &event );
    void OnMenuUnmuteParticipantUpdateUI( wxUpdateUIEvent &event );
    void OnMenuVolumeParticipantUpdateUI( wxUpdateUIEvent &event );
    void OnMenuHangUpParticipantUpdateUI( wxUpdateUIEvent &event );
    void OnMenuRemoveSelectedUpdateUI( wxUpdateUIEvent &event );

    void OnMenuMoveAllToMainUpdateUI( wxUpdateUIEvent &event );
    void OnMenuMoveSelToNewSubUpdateUI( wxUpdateUIEvent &event );
    void OnMenuMoveSelToSubUpdateUI( wxUpdateUIEvent &event );
    void OnMenuRemoveSubUpdateUI( wxUpdateUIEvent &event );

    void OnMenuContVolumeItemUpdateUI( wxUpdateUIEvent &event );
    void OnMenuContCallItemUpdateUI( wxUpdateUIEvent &event );
    void OnMenuEmailItemUpdateUI( wxUpdateUIEvent &event );
    void OnMenuIMItemUpdateUI( wxUpdateUIEvent &event );

    void OnMeetingError(MeetingError& event);
    void OnMeetingEvent(MeetingEvent& event);
    void OnSubmeetingAdded(wxCommandEvent& event);

    //  Handle DocShareStarted events to create and open the Whiteboard
    void OnDocshareEvent( DocshareEvent& event );
    void DoOpenWhiteboard( bool fPresenting, bool fWhiteboard, bool fFileSelect, bool fPresentation = false );

    void DoShareStop( );

    void OnParticipantListColClick( wxListEvent& event );

    void OnPresenceEvent(PresenceEvent& event);
    void SetPresence(wxTreeItemId parent);

    void OnInternalIdle();

    //(*Declarations(ParticipantPanel)
    wxTogBmpBtn* m_TBBLecture;
    wxTogBmpBtn* m_TBBHold;
    wxTogBmpBtn* m_TBBInvite;
    wxTogBmpBtn* m_TBBHandUp;
    wxTogBmpBtn* m_TBBShare;
    wxTogBmpBtn* m_TBBRecord;
    wxTogBmpBtn* m_TBBOperator;
    wxTogBmpBtn* m_TBBMyPhone;
    wxTogBmpBtn* m_TBBMeeting;
    wxTreeListCtrl* m_ParticipantTree;
    wxTogBmpBtn* m_TBBParticipant;
    //*)

private:
    void        ShowStatus( const wxString & msg );
    void        UpdateTitle( );
    void        BuildTree( meeting_t meeting );
    bool        AddParticipantToTree( wxString strPartID, bool fSort = false );
    bool        AddParticipantToTree( participant_t p, bool fSort = false );
    void        RemoveParticipantFromTree( wxString strPartID );
    void        UpdateParticipantState( wxString strSubID, wxString strPartID );
    void        FormatParticipantStatus( participant_t p, wxString &strName, wxString &strStatus );
    void        SetParticipantStatus( participant_t p );
    void        SetParticipantPrivileges( participant_t p );
    int         GetNextSubNumber( ) { return m_iNextSubNumber++; }
    void        SetupCallMeMenu( );
    void        FillPhoneList( participant_t part_t );
    void        FillEmailList( participant_t part_t );
    void        FillIMList( participant_t part_t );
    bool        InviteeAlreadyInMeeting( invitee_t invt );
    void        FindFollowmeNumber( participant_t part_t, wxString& strRet, bool& isPCPhone );
    void        GetSubmeetingTitle( submeeting_t sm, wxString &strTitle );
    void        GetParticipantID( meeting_t mtg );
    bool        IsPCPhone( int delta );
    bool        CheckRestart( );


private:
    CImageListComposite *m_TreeImages;
    wxMenuBar   *m_pPopupMenuBar;
    wxMenu      *m_pPopMenuMeeting;
    wxMenu      *m_pPopMenuMyPhone;
    wxMenu      *m_pPopMenuShare;
    wxMenu      *m_pPopMenuInvite;
    wxMenu      *m_pPopMenuParticipant;

    MeetingMainFrame *m_pMMF;
    wxMenuBar   *m_pMainMenuBar;

    //  Context menu and its submenus
    wxMenu      *m_pcontextMenu;
    wxMenu      *m_pcontextMenuVol;
    wxMenu      *m_pcontextMenu1To1;
    wxMenu      *m_pcontextMenuConfCall;
    wxMenu      *m_pcontextMenuEmail;
    wxMenu      *m_pcontextMenuIM;
    wxMenuItem  *m_pmenuitemSep;
    wxMenuItem  *m_pmenuitem1To1;
    wxMenuItem  *m_pmenuitemConfCall;
    wxMenuItem  *m_pmenuitemEmail;
    wxMenuItem  *m_pmenuitemIM;
    int         m_fLastContextNumSel;
    wxArrayString   m_arrstrCallMe;
    wxArrayString   m_arrstrCallMePhone;
    wxArrayInt      m_arrintCallMe;
    wxArrayString   m_arrstrEmail;
    wxArrayString   m_arrstrEmailAddr;
    wxArrayInt      m_arrintEmail;
    wxArrayString   m_arrstrIM;
    wxArrayInt      m_arrintIM;

    bool        m_fAdmin;
    bool        m_fCurrentlyPresenter;
    int         m_iNextSubNumber;
    bool        m_fMeetingHasStarted;          /**< Meeting is fully started (not waiting for moderator) */
    long        m_nLastSortColumn;             /**< The last known sort column */
    bool        m_fLastSortAscending;          /**< The ascending/descending state for m_nLastSortColumn */
    bool        m_fUpdatePrivileges;           /**< Update menu & button states during next idle period */
    bool        m_fUpdateCaption;              /**< Update title of panel (number of participants changed) */

    wxString    m_strParticipantID;
    wxString    m_strParticipantName;
    wxString    m_strParticipantScreenName;
    wxString    m_strMeetingID;
    wxString    m_strMeetingTitle;
    wxString    m_strPassword;
    wxString    m_strConnectedPhone;

    ShareControl *m_pShareControl;
    
    // Document share--code ported from Zon expects doc and view objects.
    // Not sure this is the best place for them, but better then in app.
    DocShareView *m_pDocShareView;
    CIICDoc      *m_pDoc;

    meeting_t           m_meeting_t;
    participant_tArray  m_arrpart_tMoveToNew;   /**< Array of parts to move to new SubMtg, once ctrl'er says it's created... */
    wxString            m_strNewSubMtgID;       /**< ID of new SubMtg to move list of participants to */
    participant_t       m_participant_tMe;
    submeeting_t        m_submeeting_tMain;
    wxTreeItemId        m_idMainMeeting;
    wxTreeItemId        m_idNotInMeeting;
    MeetingView         m_MeetingView;          /**< The meeting view interface */
    wxString2TreeItemIDMap  m_mapSubID2TIID;    /**< Map of submeeting IDs to tree node IDs */
    wxString2TreeItemIDMap  m_mapPartID2TIID;   /**< Map of participant IDs to tree node IDs */
    bool                m_fModalDialogOpen; 

    int         m_StatusHandup;
    int         m_StatusMeeting;
    int         m_StatusPhone;
    int         m_StatusContact;

    DECLARE_EVENT_TABLE()
};

#endif
