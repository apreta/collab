/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 *
 *
 * All changes from ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */
// DocShareView.cpp : implementation file
//

#include "app.h"
#include "meetingcentercontroller.h"
#include "sharecontrol.h"

#include "iicdoc.h"
#include "docshareview.h"
#include "meetingmainframe.h"
#include "mainpanel.h"
#include "utils.h"
#include "sendrecvprogressdialog.h"
#include "docshareindexdialog.h"
#include "docexplorerdialog.h"

#include "controls/wx/wxTogBmpBtn.h"       //  for gTransColour

#include "../netlib/nethttp.h"
#include "base64.h"

#ifdef SAVEASSVG
#include "dcsvg.h"
#endif

#include <wx/xrc/xmlres.h>
#include <wx/colordlg.h>
#include <wx/fontdlg.h>
#include <wx/clipbrd.h>
#include <wx/dir.h>
#include <wx/ffile.h>
#include <wx/file.h>
#include <wx/filedlg.h>
#include <wx/mstream.h>
#include <wx/stdpaths.h>
#include <wx/process.h>

#include <string>

#ifdef WIN32
#include "../sharelib/procutil.h"
#endif

// TODO: wxWidgets really seems to want to have keybaord focus on a child of a panel, not the panel itself.
// Maybe we could add a dummy child window to have the focus and process the keyboard slide controls; otherwise
// we need to step around the framework and use the native APIs to set focus to the panel.


BEGIN_EVENT_TABLE(DocShareCanvas, wxScrolledWindow)
EVT_MOUSE_EVENTS(DocShareCanvas::OnMouseEvent)
EVT_CHAR(DocShareCanvas::OnChar)
END_EVENT_TABLE()

//*****************************************************************************
// Define a constructor for my canvas
DocShareCanvas::DocShareCanvas(wxWindow *pParent, const wxPoint& pos, const wxSize& size, const long style):
    wxScrolledWindow(pParent, wxID_ANY, pos, size, style), m_pView(NULL)
{
    wxLogDebug( wxT("entering DocShareCanvas CONSTRUCTOR") );

    m_ShareHTML = new wxHtmlWindow( this, wxID_ANY, wxDefaultPosition,
                                    wxSize(480, 600), wxHW_SCROLLBAR_NEVER );
    m_ShareHTML->SetBorders( 10 );
    m_ShareHTML->SetPage( 
        _("<HTML><BODY><h3>No presentation/document share in progress</h3><br><br><br><br><br><br>"
          "</BODY></HTML>") );
    m_ShareHTML->SetSize( m_ShareHTML->GetInternalRepresentation()->GetWidth(),
                          m_ShareHTML->GetInternalRepresentation()->GetHeight());

    m_ShareHTML->Show(true);

    m_PointerPos = wxPoint(-1,-1);
    m_PointerBitmap.LoadFile( wxGetApp().GetResourceFile(_T("res/remote-cursor.png")), wxBITMAP_TYPE_PNG );
#ifdef POINTER_CONTROL
    m_Pointer = new wxStaticBitmap( this, wxID_ANY, bmpT, wxDefaultPosition, wxDefaultSize );
    m_Pointer->Show( false );
#endif

    Layout( );

    Connect( wxEVT_SIZE, (wxObjectEventFunction) &DocShareCanvas::OnSize );
}


//*****************************************************************************
// Define a destructor for my canvas
DocShareCanvas::~DocShareCanvas( )
{
    wxLogDebug( wxT("entering DocShareCanvas DESTRUCTOR") );
}


//*****************************************************************************
void DocShareCanvas::SetPresenter(bool flag, bool isModerator)
{
    if (flag)
        m_ShareHTML->SetPage( 
            _("<HTML><BODY><h3>No presentation/document share in progress</h3>"
              "<a href='IDC_DLG_MTG_MENU_SHAREPRESENTATION'>Share a new presentation</a><br>"
              "<a href='IDC_DLG_MTG_MENU_SHAREDOCUMENT'>Share a new document</a><br><br>"
              "<a href='IDC_DLG_MTG_MENU_OPENWHITEBOARD'>Open whiteboard</a><br>"
              "<a href='IDC_DLG_MTG_MENU_DOCUMENTEXPLORER'>Open uploaded presentation or document</a>"
              "</BODY></HTML>") );
    else if ( isModerator )
        m_ShareHTML->SetPage( 
            _("<HTML><BODY><h3>No presentation/document share in progress</h3>"
              "<a href='IDC_DLG_MTG_MENU_MAKEMEPRESENTER'>Make Me Presenter</a><br>"
              "</BODY></HTML>") );
    else
        m_ShareHTML->SetPage( 
            _("<HTML><BODY><h3>No presentation/document share in progress</h3><br><br><br><br><br><br>"
              "</BODY></HTML>") );

}


//*****************************************************************************
// Define the repainting behaviour
void DocShareCanvas::OnDraw( wxDC& dc )
{
    if( m_pView)
        m_pView->OnDraw( &dc );

#ifndef POINTER_CONTROL
    if ( m_PointerPos.x >= 0 && m_PointerPos.y >= 0 ) {
        dc.SetUserScale(1.0, 1.0);
        dc.DrawBitmap(m_PointerBitmap,
                      m_PointerPos.x-12,
                      m_PointerPos.y-12,
                      true /* use mask */);
    }
#endif    
}


//*****************************************************************************
void DocShareCanvas::OnMouseEvent( wxMouseEvent& event )
{
    if( !m_pView )
        return;

    if( event.Moving( )  ||  event.Dragging( )  ||  event.LeftDown( )  ||  event.LeftDClick( )  ||  event.LeftUp( )  ||  event.RightDown( ) )
        m_pView->AddPendingEvent( event );

}


//*****************************************************************************
void DocShareCanvas::OnChar( wxKeyEvent& event )
{
    wxLogDebug(wxT("Key: %d"), event.GetKeyCode());

    wxCommandEvent dummy;
    
    if ( event.GetKeyCode() == WXK_SPACE )
    {
        m_pView->OnDocshareNextPage(dummy);
    }
    else if ( event.GetKeyCode() == WXK_BACK )
    {
        m_pView->OnDocsharePreviousPage(dummy);
    }
    else if ( event.GetKeyCode() == WXK_DELETE )
    {
        if (m_pView->CanEdit())
            m_pView->OnDocshareDeleteSel(dummy);
    }
}


//*****************************************************************************
void DocShareCanvas::OnSize( wxSizeEvent &event )
{
    if( m_pView )
        m_pView->OnSize( event );
    // We need to repaint the entire window b/c the scale factor may need to change
    Refresh();
}


//*****************************************************************************
void DocShareCanvas::SetPointerPos( int x, int y )
{
    int scrollX, scrollY;
    int unitsX, unitsY;
    this->GetViewStart(&scrollX, &scrollY);
    this->GetScrollPixelsPerUnit(&unitsX, &unitsY);
    scrollX *= unitsX;
    scrollY *= unitsY;
    
    if (m_PointerPos.x >= 0 && m_PointerPos.y >= 0)
        RefreshRect(wxRect(m_PointerPos.x-scrollX-12, m_PointerPos.y-scrollY-12, 32, 32), false);
        
    m_PointerPos.x = x;
    m_PointerPos.y = y;

    if (m_PointerPos.x >= 0 && m_PointerPos.y >= 0)
        RefreshRect(wxRect(m_PointerPos.x-scrollX-12, m_PointerPos.y-scrollY-12, 32, 32), false);
    
#ifdef POINTER_CONTROL    
    if (x < 0 || y < 0) {
        m_Pointer->Show(false);
    } else {
        m_Pointer->Show(true);
        m_Pointer->Move(x-12, y-12);
    }
#endif
}


IMPLEMENT_DYNAMIC_CLASS(DocShareView, wxView)


BEGIN_EVENT_TABLE(DocShareView, wxView)
    EVT_TOOL(XRCID("IDC_DOCSHAREEVENT_PARTJOINED"), DocShareView::OnDocshareParticipantJoined)

    EVT_LEFT_DOWN( DocShareView::OnLButtonDown )
    EVT_LEFT_DCLICK( DocShareView::OnLButtonDClick )
    EVT_LEFT_UP( DocShareView::OnLButtonUp )
    EVT_MOTION( DocShareView::OnMotion )
    EVT_RIGHT_DOWN( DocShareView::OnRButtonDown )
    EVT_MOUSE_CAPTURE_LOST( DocShareView::OnCaptureLost )
END_EVENT_TABLE()



static int  kMAX_LINESIZE   = 48;
static int  iChunkSize      = 2048;

#ifndef __WXMSW__

#define WINAPI
#define FAILED(x)       (x < 0)
#define SUCCEEDED(x)    (x >= 0)
#define _tcsicmp(x,y)   strcmp((x),(y))
#define stricmp(x,y)    strcmp((x),(y))
#define S_OK            0
#define SW_SHOW         1

#endif


LINESIZE lineSizes[] = {
    {wxString(_("1  pt"), wxConvUTF8), 1},
    {wxString(_("2  pt"), wxConvUTF8), 2},
    {wxString(_("4  pt"), wxConvUTF8), 4},
    {wxString(_("6  pt"), wxConvUTF8), 6},
    {wxString(_("12 pt"), wxConvUTF8), 12},
    {wxString(_("24 pt"), wxConvUTF8), 24},
    {wxString(_("36 pt"), wxConvUTF8), 36},
    {wxString(_("48 pt"), wxConvUTF8), 48},
};


LINESTYLE lineStyles[] = {
    {wxString(_("Solid"), wxConvUTF8),      PS_SOLID},
    {wxString(_("Dash"), wxConvUTF8),       PS_DASH},
    {wxString(_("Dot"), wxConvUTF8),        PS_DOT},
    {wxString(_("DashDot"), wxConvUTF8),    PS_DASHDOT},
    {wxString(_("Null"), wxConvUTF8),       PS_NULL},
};


wxChar      TempUploadDir[]= wxT("^TempoUpload^");
wxString    gTempUploadDir;


//*****************************************************************************
DocShareView::DocShareView() :
    m_timer( this ), m_fIgnoreUpdateSignal( false )
{
    m_pMMF = NULL;
    m_pCanvas = NULL;

    m_pDialogProgress = NULL;
    m_iAllowHideProgress = 0;
    m_pDocument = NULL;
    m_bPresenter = false;
    m_bRemoteControl = false;

    m_iID_MENU_BASE = 0;
    m_nAction = MA_NONE;
    m_bRemotePointer = false;
    m_bDragging = false;
    m_pLastSelectedItem = NULL;
    m_bDirty = false;
    m_bCreating = false;
    m_nPageIndex = 0;
    m_pImage = NULL;
#ifdef METAFILE
    m_pMetafile = NULL;
#endif
    m_fOpenedAltImage = false;
    m_nImageCount = 1;
    m_bSelected = false;
    m_bFileLocked = false;
    m_pCtlEdit = NULL;
    
    m_pcontextMenu          = NULL;
    m_pcontextMenuLineStyle = NULL;
    m_pcontextMenuLineSize  = NULL;
    m_pcontextMenuObj       = NULL;
    m_pmenuItemLineSize     = NULL;
    m_pmenuItemLineStyle    = NULL;
    m_fReadOnly = false;
    m_dblZoom = 1.0;
    m_dblImageZoom = 1.0;
    m_iLastUnitsX = 0;
    m_iLastUnitsY = 0;

    m_lock = 0;
    m_bRefreshRequested = false;
    m_lPreLoadExeHnd = 0;
    m_bWhiteboarding = false;
    m_iXScrollRate = 0;
    m_iYScrollRate = 0;
    m_fForceScroll = false;
    m_fFitToWidth = false;

    m_ptPasteOffset = wxPoint(-1, -1);
    
    m_in_handler = false;
    
    m_fGrabFocus = false;

#ifdef __WXMSW__
    m_bRestrictedUser = CUtils::IsRestrictedUser( );
#else
    m_bRestrictedUser = false;
#endif      //  #ifdef __WXMSW__

    wxMask      *pMask;
    wxImage     imgT;
    wxBitmap    bmpT;
    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/cur_leftright.bmp")), wxBITMAP_TYPE_BMP );
    pMask = new wxMask( bmpT, gTransColour );
    bmpT.SetMask( pMask );
    imgT = bmpT.ConvertToImage( );
    imgT.SetOption( wxIMAGE_OPTION_CUR_HOTSPOT_X, 15 );
    imgT.SetOption( wxIMAGE_OPTION_CUR_HOTSPOT_Y, 15 );
    m_CursorLeftright = wxCursor( imgT );

    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/cur_cross.bmp")), wxBITMAP_TYPE_BMP );
    pMask = new wxMask( bmpT, gTransColour );
    bmpT.SetMask( pMask );
    imgT = bmpT.ConvertToImage( );
    imgT.SetOption( wxIMAGE_OPTION_CUR_HOTSPOT_X, 15 );
    imgT.SetOption( wxIMAGE_OPTION_CUR_HOTSPOT_Y, 15 );
    m_CursorCross = wxCursor( imgT );

    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/cur_vert.bmp")), wxBITMAP_TYPE_BMP );
    pMask = new wxMask( bmpT, gTransColour );
    bmpT.SetMask( pMask );
    imgT = bmpT.ConvertToImage( );
    imgT.SetOption( wxIMAGE_OPTION_CUR_HOTSPOT_X, 15 );
    imgT.SetOption( wxIMAGE_OPTION_CUR_HOTSPOT_Y, 15 );
    m_CursorVert = wxCursor( imgT );

    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/cur_nesw.bmp")), wxBITMAP_TYPE_BMP );
    pMask = new wxMask( bmpT, gTransColour );
    bmpT.SetMask( pMask );
    imgT = bmpT.ConvertToImage( );
    imgT.SetOption( wxIMAGE_OPTION_CUR_HOTSPOT_X, 15 );
    imgT.SetOption( wxIMAGE_OPTION_CUR_HOTSPOT_Y, 15 );
    m_CursorNESW = wxCursor( imgT );

    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/cur_nwse.bmp")), wxBITMAP_TYPE_BMP );
    pMask = new wxMask( bmpT, gTransColour );
    bmpT.SetMask( pMask );
    imgT = bmpT.ConvertToImage( );
    imgT.SetOption( wxIMAGE_OPTION_CUR_HOTSPOT_X, 15 );
    imgT.SetOption( wxIMAGE_OPTION_CUR_HOTSPOT_Y, 15 );
    m_CursorNWSE = wxCursor( imgT );

    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/cur_hand.bmp")), wxBITMAP_TYPE_BMP );
    pMask = new wxMask( bmpT, gTransColour );
    bmpT.SetMask( pMask );
    imgT = bmpT.ConvertToImage( );
    imgT.SetOption( wxIMAGE_OPTION_CUR_HOTSPOT_X, 15 );
    imgT.SetOption( wxIMAGE_OPTION_CUR_HOTSPOT_Y, 15 );
    m_CursorHand = wxCursor( imgT );

    bmpT.LoadFile( wxGetApp().GetResourceFile(_T("res/cur_move.bmp")), wxBITMAP_TYPE_BMP );
    pMask = new wxMask( bmpT, gTransColour );
    bmpT.SetMask( pMask );
    imgT = bmpT.ConvertToImage( );
    imgT.SetOption( wxIMAGE_OPTION_CUR_HOTSPOT_X, 15 );
    imgT.SetOption( wxIMAGE_OPTION_CUR_HOTSPOT_Y, 15 );
    m_CursorMove = wxCursor( imgT );

}


//*****************************************************************************
DocShareView::~DocShareView()
{
    wxLogDebug( wxT("entering DocShareView DESTRUCTOR") );

    m_pLastSelectedItem = NULL;
    m_drawingObjects.DeleteAllItems( );
    ClearBackgroundImages( );

    m_strMeetingID.Empty( );
    m_strLockToken.Empty( );

    if ( m_pMMF )
    {
        m_fIgnoreUpdateSignal = true;
        Disconnect( wxID_ANY, wxEVT_TIMER, wxTimerEventHandler(DocShareView::OnTimer) );
        m_timer.Stop( );
        
        //if (!m_strSavedTitle.IsEmpty())
        //    m_pMMF->SetTitle( m_strSavedTitle );
    }
    
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_DOCSHARE_EVENT, DocshareEventHandler(DocShareView::OnDocshareEvent), 0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_CONVERSION_EVENT, wxCommandEventHandler(DocShareView::OnConversionEvent), 0, this );
}


//*****************************************************************************
bool DocShareView::OnCreate( wxDocument *pDoc, long lFlags )
{
    CONTROLLER.Connect(wxID_ANY, wxEVT_DOCSHARE_EVENT, DocshareEventHandler(DocShareView::OnDocshareEvent), 0, this );
    CONTROLLER.Connect(wxID_ANY, wxEVT_CONVERSION_EVENT, wxCommandEventHandler(DocShareView::OnConversionEvent), 0, this );
    return true;
}


//*****************************************************************************
void DocShareView::InitializeInfo(CIICDoc *pDocument, const wxString& aMeetingID)
{
    m_strMeetingID = aMeetingID;
    m_pDocument = pDocument;
    m_drawingObjects.SetOwner(this, m_pDocument);
}

//*****************************************************************************
void DocShareView::ConnectToolbarEvents( )
{
    wxToolBar* wndToolBar = m_pMMF->GetMainPanel()->GetDocShareToolbar();
    wxToolBar* wndToolBar2 = m_pMMF->GetMainPanel()->GetDocShareToolbar2();

    if (NULL != wndToolBar)
    {
        wndToolBar->Connect((XRCID("IDC_DOCSHARETB_NEW")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareNew), 0, this );
        wndToolBar->Connect((XRCID("IDC_DOCSHARETB_OPEN")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareOpen), 0, this );
        wndToolBar->Connect((XRCID("IDC_DOCSHARETB_SAVE")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareSave), 0, this );
        wndToolBar->Connect((XRCID("IDC_DOCSHARETB_SAVEAS")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareSaveAs), 0, this );
        wndToolBar->Connect((XRCID("IDC_DOCSHARETB_UPLOADDOC")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareUploadDoc), 0, this );
        wndToolBar->Connect((XRCID("IDC_DOCSHARETB_ORGANIZEPAGES")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareOrganizePages), 0, this );
        wndToolBar->Connect((XRCID("IDC_DOCSHARETB_INSERTPAGE")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareInsertPage), 0, this );
        wndToolBar->Connect((XRCID("IDC_DOCSHARETB_FIRSTPAGE")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareFirstPage), 0, this );
        wndToolBar->Connect((XRCID("IDC_DOCSHARETB_PREVIOUSPAGE")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocsharePreviousPage), 0, this );
        wndToolBar->Connect((XRCID("IDC_DOCSHARETB_GOTOPAGE")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareGotoPage), 0, this );
        wndToolBar->Connect((XRCID("IDC_DOCSHARETB_NEXTPAGE")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareNextPage), 0, this );
        wndToolBar->Connect((XRCID("IDC_DOCSHARETB_LASTPAGE")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareLastPage), 0, this );
        wndToolBar->Connect((XRCID("IDC_DOCSHARETB_CUT")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareCut), 0, this );
        wndToolBar->Connect((XRCID("IDC_DOCSHARETB_COPY")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareCopy), 0, this );
        wndToolBar->Connect((XRCID("IDC_DOCSHARETB_PASTE")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocsharePaste), 0, this );
        wndToolBar->Connect((XRCID("IDC_DOCSHARETB_DELETESEL")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareDeleteSel), 0, this );
        wndToolBar->Connect((XRCID("IDC_DOCSHARETB_CLEARPAGE")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareClearPage), 0, this );
        wndToolBar->Connect((XRCID("IDC_DOCSHARETB_CLEARALL")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareClearAll), 0, this );
        wndToolBar->Connect((XRCID("IDC_DOCSHARETB_UNDO")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareUndo), 0, this );
        wndToolBar->Connect((XRCID("IDC_DOCSHARETB_REDO")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareRedo), 0, this );
    }

    if (NULL != wndToolBar2)
    {
        wndToolBar2->Connect((XRCID("IDC_DOCSHARETB_POINTER")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocsharePointer), 0, this );
        wndToolBar2->Connect((XRCID("IDC_DOCSHARETB_SELECT")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareSelect), 0, this );
        wndToolBar2->Connect((XRCID("IDC_DOCSHARETB_LINE")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareLine), 0, this );
        wndToolBar2->Connect((XRCID("IDC_DOCSHARETB_ARROW")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareArrow), 0, this );
        wndToolBar2->Connect((XRCID("IDC_DOCSHARETB_SCRIBBLE")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareScribble), 0, this );
        wndToolBar2->Connect((XRCID("IDC_DOCSHARETB_HIGHLIGHT")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareHighlight), 0, this );
        wndToolBar2->Connect((XRCID("IDC_DOCSHARETB_RECTANGLE")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareRectangle), 0, this );
        wndToolBar2->Connect((XRCID("IDC_DOCSHARETB_ELLIPSE")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareEllipse), 0, this );
        wndToolBar2->Connect((XRCID("IDC_DOCSHARETB_TEXTBOX")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareTextbox), 0, this );
        wndToolBar2->Connect((XRCID("IDC_DOCSHARETB_CHECKMARK")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareCheckmark), 0, this );
        wndToolBar2->Connect((XRCID("IDC_DOCSHARETB_LINETHICKNESS")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareLineThickness), 0, this );
        wndToolBar2->Connect((XRCID("IDC_DOCSHARETB_LINESTYLE")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareLineStyle), 0, this );
        wndToolBar2->Connect((XRCID("IDC_DOCSHARETB_FONTSETTINGS")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareFontSettings), 0, this );
        wndToolBar2->Connect((XRCID("IDC_DOCSHARETB_LINECOLOR")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareLineColor), 0, this );
        wndToolBar2->Connect((XRCID("IDC_DOCSHARETB_BGCOLOR")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareBgColor), 0, this );
        wndToolBar2->Connect((XRCID("IDC_DOCSHARETB_TEXTCOLOR")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareTextColor), 0, this );
        wndToolBar2->Connect((XRCID("IDC_DOCSHARETB_TOFRONT")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareToFront), 0, this );
        wndToolBar2->Connect((XRCID("IDC_DOCSHARETB_TOBACK")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareToBack), 0, this );
        wndToolBar2->Connect((XRCID("IDC_DOCSHARETB_TOGTRANS")), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnDocshareTogTrans), 0, this );
    }

    m_pMMF->Connect(XRCID("ID_SHARECONTROL_REFRESH"), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnRefresh), 0, this );
}

//*****************************************************************************
void DocShareView::DisconnectToolbarEvents( )
{
    wxToolBar* wndToolBar = m_pMMF->GetMainPanel()->GetDocShareToolbar();
    wxToolBar* wndToolBar2 = m_pMMF->GetMainPanel()->GetDocShareToolbar2();

    if (NULL != wndToolBar)
    {
        wndToolBar->Disconnect((XRCID("IDC_DOCSHARETB_NEW")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar->Disconnect((XRCID("IDC_DOCSHARETB_OPEN")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar->Disconnect((XRCID("IDC_DOCSHARETB_SAVE")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar->Disconnect((XRCID("IDC_DOCSHARETB_SAVEAS")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar->Disconnect((XRCID("IDC_DOCSHARETB_UPLOADDOC")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar->Disconnect((XRCID("IDC_DOCSHARETB_ORGANIZEPAGES")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar->Disconnect((XRCID("IDC_DOCSHARETB_INSERTPAGE")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar->Disconnect((XRCID("IDC_DOCSHARETB_FIRSTPAGE")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar->Disconnect((XRCID("IDC_DOCSHARETB_PREVIOUSPAGE")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar->Disconnect((XRCID("IDC_DOCSHARETB_GOTOPAGE")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar->Disconnect((XRCID("IDC_DOCSHARETB_NEXTPAGE")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar->Disconnect((XRCID("IDC_DOCSHARETB_LASTPAGE")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar->Disconnect((XRCID("IDC_DOCSHARETB_CUT")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar->Disconnect((XRCID("IDC_DOCSHARETB_COPY")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar->Disconnect((XRCID("IDC_DOCSHARETB_PASTE")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar->Disconnect((XRCID("IDC_DOCSHARETB_DELETESEL")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar->Disconnect((XRCID("IDC_DOCSHARETB_CLEARPAGE")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar->Disconnect((XRCID("IDC_DOCSHARETB_CLEARALL")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar->Disconnect((XRCID("IDC_DOCSHARETB_UNDO")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar->Disconnect((XRCID("IDC_DOCSHARETB_REDO")), wxEVT_COMMAND_TOOL_CLICKED);
    }

    if (NULL != wndToolBar2)
    {
        wndToolBar2->Disconnect((XRCID("IDC_DOCSHARETB_POINTER")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar2->Disconnect((XRCID("IDC_DOCSHARETB_SELECT")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar2->Disconnect((XRCID("IDC_DOCSHARETB_LINE")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar2->Disconnect((XRCID("IDC_DOCSHARETB_ARROW")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar2->Disconnect((XRCID("IDC_DOCSHARETB_SCRIBBLE")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar2->Disconnect((XRCID("IDC_DOCSHARETB_HIGHLIGHT")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar2->Disconnect((XRCID("IDC_DOCSHARETB_RECTANGLE")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar2->Disconnect((XRCID("IDC_DOCSHARETB_ELLIPSE")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar2->Disconnect((XRCID("IDC_DOCSHARETB_TEXTBOX")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar2->Disconnect((XRCID("IDC_DOCSHARETB_CHECKMARK")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar2->Disconnect((XRCID("IDC_DOCSHARETB_LINETHICKNESS")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar2->Disconnect((XRCID("IDC_DOCSHARETB_LINESTYLE")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar2->Disconnect((XRCID("IDC_DOCSHARETB_FONTSETTINGS")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar2->Disconnect((XRCID("IDC_DOCSHARETB_LINECOLOR")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar2->Disconnect((XRCID("IDC_DOCSHARETB_BGCOLOR")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar2->Disconnect((XRCID("IDC_DOCSHARETB_TEXTCOLOR")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar2->Disconnect((XRCID("IDC_DOCSHARETB_TOFRONT")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar2->Disconnect((XRCID("IDC_DOCSHARETB_TOBACK")), wxEVT_COMMAND_TOOL_CLICKED);
        wndToolBar2->Disconnect((XRCID("IDC_DOCSHARETB_TOGTRANS")), wxEVT_COMMAND_TOOL_CLICKED);
    }

    m_pMMF->Disconnect(XRCID("ID_SHARECONTROL_REFRESH"), wxEVT_COMMAND_TOOL_CLICKED, wxCommandEventHandler( DocShareView::OnRefresh), 0, this );
}


//*****************************************************************************
void DocShareView::UnscaleThePoint( wxPoint &pt )
{
    pt.x = (int)((((double) pt.x) / m_dblZoom) + 0.5);
    pt.y = (int)((((double) pt.y) / m_dblZoom) + 0.5);
}


//*****************************************************************************
void DocShareView::ScaleThePoint( wxPoint &pt )
{
    pt.x = (int)((((double) pt.x) * m_dblZoom) + 0.5);
    pt.y = (int)((((double) pt.y) * m_dblZoom) + 0.5);
}


//*****************************************************************************
void DocShareView::UnscaleTheRect( IICRect &rect )
{
    rect.left   = (int)((((double) rect.left  ) / m_dblZoom) + 0.5);
    rect.top    = (int)((((double) rect.top   ) / m_dblZoom) + 0.5);
    rect.right  = (int)((((double) rect.right ) / m_dblZoom) + 0.5);
    rect.bottom = (int)((((double) rect.bottom) / m_dblZoom) + 0.5);
}


//*****************************************************************************
void DocShareView::ScaleTheRect( IICRect &rect )
{
    rect.left   = (int)((((double) rect.left  ) * m_dblZoom) + 0.5);
    rect.top    = (int)((((double) rect.top   ) * m_dblZoom) + 0.5);
    rect.right  = (int)((((double) rect.right ) * m_dblZoom) + 0.5);
    rect.bottom = (int)((((double) rect.bottom) * m_dblZoom) + 0.5);
}


//*****************************************************************************
void DocShareView::CalcScrolledPosition( int x, int y, int *xx, int *yy )
{
    //  All params are unscaled coordinates - scale before calling
    //    CalcScrolledPositon( ) then re-unscale the result.
    wxPoint resxy;
    wxPoint rawxy( x, y );
    ScaleThePoint( rawxy );
    m_pCanvas->CalcScrolledPosition( rawxy.x, rawxy.y, &resxy.x, &resxy.y );
    UnscaleThePoint( resxy );
    *xx = resxy.x;
    *yy = resxy.y;
}


//*****************************************************************************
void DocShareView::CalcUnscrolledPosition( int x, int y, int *xx, int *yy )
{
    //  All params are unscaled coordinates - scale before calling
    //    CalcUnscrolledPositon( ) then re-unscale the result.
    wxPoint resxy;
    wxPoint rawxy( x, y );
    ScaleThePoint( rawxy );
    m_pCanvas->CalcUnscrolledPosition( rawxy.x, rawxy.y, &resxy.x, &resxy.y );
    UnscaleThePoint( resxy );
    *xx = resxy.x;
    *yy = resxy.y;
}


//*****************************************************************************
void DocShareView::CalcScrolledPosition( IICRect &rect )
{
    CalcScrolledPosition( rect.left, rect.top, &rect.left, &rect.top );
    CalcScrolledPosition( rect.right, rect.bottom, &rect.right, &rect.bottom );
}


//*****************************************************************************
void DocShareView::CalcUnscrolledPosition( IICRect &rect )
{
    CalcUnscrolledPosition( rect.left, rect.top, &rect.left, &rect.top );
    CalcUnscrolledPosition( rect.right, rect.bottom, &rect.right, &rect.bottom );
}


//*****************************************************************************
void DocShareView::CalcScrolledPosition( wxPoint &pt )
{
    CalcScrolledPosition( pt.x, pt.y, &pt.x, &pt.y );
}


//*****************************************************************************
void DocShareView::CalcUnscrolledPosition( wxPoint &pt )
{
    CalcUnscrolledPosition( pt.x, pt.y, &pt.x, &pt.y );
}


//*****************************************************************************
bool DocShareView::AllowFitToWidth( )
{
    bool    fRet    = false;
    int     iSize   = (int) m_saPages.size( );

    if( iSize > 0  && m_nPageIndex < iSize )
    {
        fRet = m_saPages.at( m_nPageIndex ).m_fFitToWidth;
    }

    return fRet;
}


//*****************************************************************************
void DocShareView::ForceFitToWidth( )
{
    m_fForceScroll = false;
    m_fFitToWidth = true;
    m_pCanvas->SetScrollRate( 0, m_iYScrollRate );
    m_pCanvas->Refresh( true );
}


//*****************************************************************************
void DocShareView::ForceScale( )
{
    m_fForceScroll = false;
    m_fFitToWidth = false;
    m_pCanvas->SetScrollRate( 0, 0 );
    m_pCanvas->Refresh( true );
}


//*****************************************************************************
void DocShareView::ForceScroll( )
{
    m_fForceScroll = true;
    m_fFitToWidth = false;
    m_pCanvas->SetScrollRate( m_iXScrollRate, m_iYScrollRate );
    m_pCanvas->Refresh( true );
}


//*****************************************************************************
void DocShareView::InitializeScroll( bool fForceScroll )
{
    // Reset to settings as when window was created
    m_iLastUnitsX = 0;
    m_iLastUnitsY = 0;
    m_iXScrollRate = kDocSharePixelsPerUnitX;
    m_iYScrollRate = kDocSharePixelsPerUnitY;

    m_pCanvas->Scroll(0,0);
    m_pCanvas->SetScrollbars( kDocSharePixelsPerUnitX, kDocSharePixelsPerUnitY, 50, 50 );

    if( fForceScroll )
    {
        m_fForceScroll = true;
        m_fFitToWidth = false;
        m_pCanvas->SetScrollRate( m_iXScrollRate, m_iYScrollRate );
    }
    else
    {
        m_fForceScroll = false;
        m_pCanvas->SetScrollRate( 0, 0 );
    }
}


//*****************************************************************************
void DocShareView::SetScrollbars( int iUnitsX, int iUnitsY )
{
    int     scrollx, scrolly;

    // Figure out which scrollbars we really want...
    if( m_fForceScroll ) 
    {
        scrollx = scrolly = 1;
    }
    else if( m_fFitToWidth ) 
    {
        scrollx = 0;
        scrolly = 1;
    }
    else 
    {
        scrollx = scrolly = 0;
    }
    
    // then update if one or both have changed more then a small amount
    if( (scrollx && abs(m_iLastUnitsX-iUnitsX) > kDocShareUnitMinChange) || 
        (scrolly && abs(m_iLastUnitsY-iUnitsY) > kDocShareUnitMinChange) )
    {
        m_iLastUnitsX = iUnitsX * scrollx;
        m_iLastUnitsY = iUnitsY * scrolly;
        m_pCanvas->SetScrollbars( scrollx * kDocSharePixelsPerUnitX, scrolly * kDocSharePixelsPerUnitY, iUnitsX, iUnitsY );
    }
}


//*****************************************************************************
void DocShareView::OnSize( wxSizeEvent &event )
{
}

//*****************************************************************************
void DocShareView::OnRefresh( wxCommandEvent& event )
{
    wxLogDebug( wxT("entering DocShareView::OnRefresh( )") );
    if( this->m_iAllowHideProgress )
    {
        wxLogDebug( wxT("Operation already in progress, ignoring refresh") );
        return;
    }

    if( !m_strDocId.IsEmpty() && m_saPages.size() > 0 )
    {
        AllowHideProgress( false );
        if( m_pDocument->m_Meeting.IsUserPresenter() )
            UpdatePageMarkup( );		// save current markup
        LoadPageMarkup( m_strDocId, m_nPageIndex );
        AllowHideProgress( true );
    }
    
}

//*****************************************************************************
bool DocShareView::Start( MeetingMainFrame *pMMF, DocShareCanvas *pCanvas, bool bWhiteboarding, bool bPresenter, bool bFileSelect, bool bPresentation )
{
    wxLogDebug( wxT("entering DocShareView::Start( )") );

    bool    fRet    = true;

    // Don't process server doc share events until we're done
    m_in_handler = true;
    m_event_queue.clear();

    AllowHideProgress( false );

    m_bPresenter = bPresenter;
    m_bWhiteboarding = bWhiteboarding;
    m_pMMF = pMMF;
    m_pCanvas = pCanvas;
    m_pCanvas->m_pView = this;
    m_bRemotePointer = bPresenter;
    m_pointerPos.x = m_pointerPos.y = -1;
    m_strObjType.clear();

    m_pCanvas->m_ShareHTML->Show( false );
    m_pCanvas->m_ShareHTML->Enable( false );

    InitializeScroll( bWhiteboarding );

    if ( m_pMMF )
    {
        // In a meeting, set up normal doc share session
        m_strSavedTitle = m_pMMF->GetTitle( );

        ConnectToolbarEvents( );

        //  Create the Edit wxTextCtrl
        wxString    strB( wxT("") );
        wxPoint     pt( 0, 0 );
        wxSize      sz( 10, 10 );

        m_pCtlEdit = new wxTextCtrl( m_pCanvas, XRCID("IDC_DOCSHARE_TEXTEDIT"), strB, pt, sz, wxTE_MULTILINE | wxTE_RICH );
        m_pCtlEdit->Hide( );
    }

    m_bDirty = false;
    m_strDocId.Empty( );
    m_saPages.clear( );
    m_nPageIndex = 0;
    m_drawingObjects.DeleteAllItems();
    m_pLastSelectedItem = NULL;
    m_dblZoom = 1.0;

    ClearBackgroundImages( );

    if( m_strMeetingID.IsEmpty( ) )
    {
        m_pDocument->m_Meeting.GetMeetingId( m_strMeetingID );
    }
    
    m_pDocument->GetSessionInfo( m_strUserName, m_strPasswd, m_iicServer );

    // has to generate the temp directory, because multiple could upload in the same time
    gTempUploadDir = TempUploadDir;
    CUtils::EncodeStringFully( gTempUploadDir );

    wxString     strT( m_strUserName );
    strT.Replace(wxT("/"), wxT("+"), true);     // anonymouse user has "/mtg<pin>" at end, replace slash
    CUtils::EncodeStringFully( strT );
    gTempUploadDir += strT;
    gTempUploadDir+= _T("/");

    m_strLockToken.Empty( );
    m_bFileLocked = false;

    HideProgressWindow( );

    // If we are in a meeting main frame, then we are in a running meeting and the goal is to get the user to select
    // a document.  If we are not in a meeting, then the user is uploading documents in preperation for a meeting, so
    // we don't actually open and start sharing the document.
    // We may change this in the future, to allow the user to see and markup documents during the upload process even if
    // not in a meeting.
    
    if (m_pMMF)
    {
        wxString title = m_bWhiteboarding ? _("Share Whiteboard") : _("Share Document");
        m_pMMF->SetTitle( title );

        m_fIgnoreUpdateSignal = false;
        Connect( wxID_ANY, wxEVT_TIMER, wxTimerEventHandler(DocShareView::OnTimer) );
        m_timer.Start( 500, wxTIMER_ONE_SHOT );
    }
    else
    {
        // We are not in a meeting, must access as host of meeting not participant in meeting
        m_WebDavUtils.AddHeader( wxT("X-iic-host: true") );
    }

    wxString strDocId;

    fRet = SelectInitialDoc( bFileSelect, strDocId, bPresentation );
    if (!fRet)
    {
        CloseDialogs( );
    }
    else if ( m_pMMF )
    {
        // In a meeting, load document and process any pending meeting events.
        if ( !strDocId.IsEmpty() )
            LoadDocument( strDocId );

        ProcessEventQueue();
        m_in_handler = false;
    }

    AllowHideProgress( true );

    if (fRet)
    {
        m_fGrabFocus = true;
    }
    else
    {
        // Failed, reset things...
        Stop( );
    }

    wxLogDebug( wxT("leaving DocShareView::Start( )  --  fRet is %d"), fRet );
    return fRet;
}


//*****************************************************************************
bool DocShareView::SelectInitialDoc( bool bFileSelect, wxString& strDocId, bool bPresentation )
{
    bool fRet = true;

    if( m_bWhiteboarding )
    {
        // Open whiteboard document
        strDocId = _T("Whiteboard") + m_strMeetingID;
    }
    else
    {
        int iRet;
        if( IsPresenter( ) )
        {
            if (!bFileSelect)
            {
                // Let user pick existing document...if not in meeting main frame, then we want the dialog to stay
                // open if even user uploads a new document.
                wxBeginBusyCursor( );
                DocExplorerDialog dialog( m_pCanvas, m_pDocument, m_strMeetingID, m_strDocId, m_pMMF != NULL );
                wxEndBusyCursor( );

                if( dialog.m_fErrorInitializing )
                {
                    fRet = false;
                }
                else
                {
                    AllowHideProgress( true );      //  Allows Progress dlg to hide after deleting docs
                    iRet = dialog.ShowModal( );
                    strDocId = dialog.m_strDocID;
                    AllowHideProgress( false );
                }
            }
            else 
            {
                // Upload a new document
                fRet = Upload( strDocId, bPresentation );
                iRet = wxID_OK;
            }
        }
        
        if( IsPresenter( )  &&  fRet )
        {
            if( iRet  ==  wxID_CANCEL )
            {
                fRet = false;
            }
            else if( iRet  ==  wxID_OK )
            {
                CUtils::MsgWait( 10, 1 );	// avoid stuck dialog bkgnd
                if( strDocId.IsEmpty( ) )
                    fRet = Upload( strDocId );
            }
            else if( iRet  ==  wxID_DELETE )
            {
                //  Delete the selected share document then return
                fRet = false;
            }
            else if( iRet  ==  wxID_UP )
            {
                //  Upload a share document then start the share session
                fRet = Upload( strDocId );
            }
            else
            {
                wxLogDebug( wxT("DocExplorerDialog.ShowModal( ) returned unexpected result - %d"), iRet );
                fRet = false;
            }
        }
    }
    
    return fRet;
}

//*****************************************************************************
void DocShareView::SetRemotePointer( bool bEnable )
{
    if (m_bRemotePointer != bEnable) {
        m_bRemotePointer = bEnable;
        if (m_bRemotePointer) {
            m_strObjType.Empty( );
        } else {
            SendPointerAction( wxT("pointer"), -1, -1 );
            m_pCanvas->SetPointerPos( -1, -1 );
            m_pointerPos.x = m_pointerPos.y = -1;
        }
    }
}

//*****************************************************************************
//*****************************************************************************
//
//   Toolbar event handlers
//
//*****************************************************************************
//*****************************************************************************
void DocShareView::OnDocshareSelect( wxCommandEvent& event )
{
    m_pCanvas->SetCursor( wxCursor( wxCURSOR_ARROW ) );
    m_strObjType.Empty( );
    SetRemotePointer( false );
}


//*****************************************************************************
void DocShareView::OnDocsharePointer( wxCommandEvent& event )
{
    IICRect rect;
    m_drawingObjects.UnSelect( rect );
    m_pLastSelectedItem = NULL;
    InvalidateSelection( rect );
    
    m_pCanvas->SetCursor( wxCursor( wxCURSOR_ARROW ) );
    m_strObjType.Empty( );
    SetRemotePointer( !m_bRemotePointer );
}


//*****************************************************************************
void DocShareView::OnDocshareLine( wxCommandEvent& event )
{
    SetCursor( LINE_START );
    m_strObjType = DOType_Line;
    SetRemotePointer( false );
}


//*****************************************************************************
void DocShareView::OnDocshareArrow( wxCommandEvent& event )
{
    SetCursor( LINE_START );
    m_strObjType = DOType_ArrowLine;
    SetRemotePointer( false );
}


//*****************************************************************************
void DocShareView::OnDocshareScribble( wxCommandEvent& event )
{
    SetCursor( LINE_START );
    m_strObjType = DOType_CurveLine;
    SetRemotePointer( false );
}


//*****************************************************************************
void DocShareView::OnDocshareHighlight( wxCommandEvent& event )
{
    SetCursor( LINE_START );
    m_strObjType = DOType_HighLite;
    SetRemotePointer( false );
}


//*****************************************************************************
void DocShareView::OnDocshareRectangle( wxCommandEvent& event )
{
    SetCursor( LINE_START );
    m_strObjType = DOType_Rectangle;
    SetRemotePointer( false );
}


//*****************************************************************************
void DocShareView::OnDocshareEllipse( wxCommandEvent& event )
{
    SetCursor( LINE_START );
    m_strObjType = DOType_Ellipse;
    SetRemotePointer( false );
}


//*****************************************************************************
void DocShareView::OnDocshareTextbox( wxCommandEvent& event )
{
    SetCursor( LINE_START );
    m_strObjType = DOType_TextBox;
    SetRemotePointer( false );
}


//*****************************************************************************
void DocShareView::OnDocshareCheckmark( wxCommandEvent& event )
{
    SetCursor( LINE_START );
    m_strObjType = DOType_CheckMark;
    SetRemotePointer( false );
}


//*****************************************************************************
void DocShareView::OnDocshareLineThickness( wxCommandEvent& event )
{
    wxMenu      menu;

    menu.Append( XRCID("IDC_DOCSHARE_POPMENU_LINETHICKNESS1"),  lineSizes[ 0 ].desc );
    menu.Append( XRCID("IDC_DOCSHARE_POPMENU_LINETHICKNESS2"),  lineSizes[ 1 ].desc );
    menu.Append( XRCID("IDC_DOCSHARE_POPMENU_LINETHICKNESS4"),  lineSizes[ 2 ].desc );
    menu.Append( XRCID("IDC_DOCSHARE_POPMENU_LINETHICKNESS6"),  lineSizes[ 3 ].desc );
    menu.Append( XRCID("IDC_DOCSHARE_POPMENU_LINETHICKNESS12"), lineSizes[ 4 ].desc );
    menu.Append( XRCID("IDC_DOCSHARE_POPMENU_LINETHICKNESS24"), lineSizes[ 5 ].desc );
    menu.Append( XRCID("IDC_DOCSHARE_POPMENU_LINETHICKNESS36"), lineSizes[ 6 ].desc );
    menu.Append( XRCID("IDC_DOCSHARE_POPMENU_LINETHICKNESS48"), lineSizes[ 7 ].desc );

    menu.Connect( XRCID("IDC_DOCSHARE_POPMENU_LINETHICKNESS1"), XRCID("IDC_DOCSHARE_POPMENU_LINETHICKNESS48"),
             wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnLineThicknessSelected), 0, this );

    m_pCanvas->PopupMenu( &menu );
}


//*****************************************************************************
void DocShareView::OnLineThicknessSelected( wxCommandEvent& event )
{
    int     index = event.GetId( ) - XRCID("IDC_DOCSHARE_POPMENU_LINETHICKNESS1");
    if( index < 0  ||  index > 7 )
    {
        index = event.GetId( ) - XRCID("IDC_DOCSHARE_CONTMENU_LINESIZE1");
        if( index < 0  ||  index > 7 )
            return;
    }

    int nLineSize = lineSizes[ index ].size;
//    wxLogDebug( wxT("entering DocShareView::OnLineThicknessSelected( )  --  nLineSize is %d"), nLineSize );
    if( m_drawingObjects.GetSelCount( )  >  0 )
    {
        SetPageDirty( true );
        HideEditWindow( );

        OBJLIST::iterator i;
        OBJLIST &listSelected = m_drawingObjects.GetSelectedItems();

        CChangeAction* action = new CChangeAction(m_pDocument);
        action->PreAction(m_drawingObjects);

        for( i = listSelected.begin(); i != listSelected.end(); ++i )
        {
            CBaseDrawingObject* obj = *i;
            if( obj )
            {
                obj->SetLineSize( nLineSize, true );
            }
        }

        action->PostAction( m_drawingObjects );
        m_drawingObjects.AddAction( action );
        InvalidateSelection( );
    }
}


//*****************************************************************************
void DocShareView::OnDocshareLineStyle( wxCommandEvent& event )
{
    wxMenu      menu;

    menu.Append( XRCID("IDC_DOCSHARE_POPMENU_LINESTYLE_SOLID"),   lineStyles[ 0 ].desc );
    menu.Append( XRCID("IDC_DOCSHARE_POPMENU_LINESTYLE_DASH"),    lineStyles[ 1 ].desc );
    menu.Append( XRCID("IDC_DOCSHARE_POPMENU_LINESTYLE_DOT"),     lineStyles[ 2 ].desc );
    menu.Append( XRCID("IDC_DOCSHARE_POPMENU_LINESTYLE_DASHDOT"), lineStyles[ 3 ].desc );
    menu.Append( XRCID("IDC_DOCSHARE_POPMENU_LINESTYLE_NULL"),    lineStyles[ 4 ].desc );

    menu.Connect( XRCID("IDC_DOCSHARE_POPMENU_LINESTYLE_SOLID"), XRCID("IDC_DOCSHARE_POPMENU_LINESTYLE_NULL"),
                  wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnLineStyleSelected), 0, this );

    m_pCanvas->PopupMenu( &menu );
}


//*****************************************************************************
void DocShareView::OnLineStyleSelected( wxCommandEvent& event )
{
    int     index = event.GetId( ) - XRCID("IDC_DOCSHARE_POPMENU_LINESTYLE_SOLID");
    if( index < 0  ||  index > 4 )
    {
        index = event.GetId( ) - XRCID("IDC_DOCSHARE_CONTMENU_LINESTYLE_SOLID");
        if( index < 0  ||  index > 4 )
            return;
    }

    int nLineStyle = lineStyles[ index ].style;
    if( m_drawingObjects.GetSelCount( )  >  0 )
    {
        SetPageDirty( true );
        HideEditWindow( );

        OBJLIST::iterator i;
        OBJLIST &listSelected = m_drawingObjects.GetSelectedItems( );

        CChangeAction* action = new CChangeAction( m_pDocument );
        action->PreAction( m_drawingObjects );

        for( i = listSelected.begin(); i != listSelected.end(); ++i )
        {
            CBaseDrawingObject* obj = *i;

            if( obj )
            {
                obj->SetLineStyle( nLineStyle, true );
            }
        }

        action->PostAction( m_drawingObjects );
        m_drawingObjects.AddAction( action );
        InvalidateSelection( );
    }
}


//*****************************************************************************
void DocShareView::OnDocshareFontSettings( wxCommandEvent& event )
{
    wxFont      cfont;
    wxFontData  oldLogfont;
    if( m_drawingObjects.GetSelCount( ) == 1  &&  m_pLastSelectedItem != NULL )
    {
        bool    fULine  = false;
        int     iStyle  = wxFONTSTYLE_NORMAL;
        int     iWeight = wxFONTWEIGHT_NORMAL;
        wxString    strFontStyle    = m_pLastSelectedItem->GetFontStyle( );
        if( strFontStyle.Find( wxT("bold|") ) != wxNOT_FOUND )
            iWeight = wxFONTWEIGHT_BOLD;
        if( strFontStyle.Find( wxT("italic")) != wxNOT_FOUND )
            iStyle = wxFONTSTYLE_ITALIC;
        if( strFontStyle.Find( wxT("underline")) != wxNOT_FOUND )
            fULine = true;

        cfont = wxFont( m_pLastSelectedItem->GetFontSize( ), wxFONTFAMILY_SWISS, iStyle, iWeight, fULine, m_pLastSelectedItem->GetFontFace( ) );
        oldLogfont.SetInitialFont( cfont );
        oldLogfont.SetColour( m_pLastSelectedItem->GetTextColor( ) );
    }

    wxFontDialog    dlg( m_pCanvas, oldLogfont );
    if( dlg.ShowModal( )  ==  wxID_OK )
    {
        SetPageDirty( true );
        HideEditWindow( );

        OBJLIST::iterator i;
        OBJLIST &listSelected = m_drawingObjects.GetSelectedItems( );

        CChangeAction* action = new CChangeAction( m_pDocument );
        action->PreAction( m_drawingObjects );

        wxFontData  logFont;
        logFont = dlg.GetFontData( );

        for( i = listSelected.begin(); i != listSelected.end(); ++i )
        {
            CBaseDrawingObject* obj = *i;

            if( obj )
            {
                obj->SetLogFont( logFont, true );
                wxFont      newFont = logFont.GetChosenFont( );
                obj->SetFontFace( newFont.GetFaceName( ), true );

                wxString    style;
                if( newFont.GetWeight( )  ==  wxFONTWEIGHT_BOLD )
                    style.Append( wxT("bold|") );
                if( newFont.GetStyle( )  ==  wxFONTSTYLE_ITALIC )
                    style.Append( wxT("italic|") );
                if( newFont.GetUnderlined( ) )
                    style.Append( wxT("underline") );
                obj->SetFontStyle( style, true );
                obj->SetFontSize( newFont.GetPointSize( ), true );
            }
        }

        action->PostAction(m_drawingObjects);
        m_drawingObjects.AddAction(action);
        InvalidateSelection();
    }
}


//*****************************************************************************
void DocShareView::OnDocshareLineColor( wxCommandEvent& event )
{
    if( m_drawingObjects.GetSelCount( )  >  0 )
    {
        wxColourData    cData;
        cData.SetColour( m_pLastSelectedItem ? m_pLastSelectedItem->GetForegroundColor( ) : *wxBLACK );

        wxColourDialog  dlg( m_pCanvas, &cData );
        if( dlg.ShowModal( )  ==  wxID_OK )
        {
            SetPageDirty( true );
            HideEditWindow( );

            OBJLIST::iterator i;
            OBJLIST &listSelected = m_drawingObjects.GetSelectedItems( );

            CChangeAction* action = new CChangeAction( m_pDocument );
            action->PreAction( m_drawingObjects );

            wxColourData    cData = dlg.GetColourData( );
            wxColour        color = cData.GetColour( );
            for( i = listSelected.begin(); i != listSelected.end(); ++i )
            {
                CBaseDrawingObject* obj = *i;

                if( obj )
                {
                    obj->SetForegroundColor( color, true );
                }
            }
            action->PostAction( m_drawingObjects );
            m_drawingObjects.AddAction( action );
            InvalidateSelection( false );
        }
    }
}


//*****************************************************************************
void DocShareView::OnDocshareBgColor( wxCommandEvent& event )
{
    if( m_drawingObjects.GetSelCount( )  >  0 )
    {
        wxColourData    cData;
        cData.SetColour( m_pLastSelectedItem ? m_pLastSelectedItem->GetBackgroundColor( ) : *wxWHITE );

        wxColourDialog  dlg( m_pCanvas, &cData );
        if( dlg.ShowModal( )  ==  wxID_OK )
        {
            SetPageDirty( true );
            HideEditWindow( );

            OBJLIST::iterator i;
            OBJLIST &listSelected = m_drawingObjects.GetSelectedItems();

            CChangeAction* action = new CChangeAction( m_pDocument );
            action->PreAction( m_drawingObjects );

            wxColourData    cData = dlg.GetColourData( );
            wxColour        color = cData.GetColour( );
            for( i = listSelected.begin(); i != listSelected.end(); ++i )
            {
                CBaseDrawingObject* obj = *i;

                if( obj )
                {
                    obj->SetBackgroundColor( color, true );
                }
            }

            action->PostAction( m_drawingObjects );
            m_drawingObjects.AddAction( action );
            InvalidateSelection( false );
        }
    }
}


//*****************************************************************************
void DocShareView::OnDocshareTextColor( wxCommandEvent& event )
{
    if( m_drawingObjects.GetSelCount( )  >  0 )
    {
        wxColourData    cData;
        cData.SetColour( m_pLastSelectedItem ? m_pLastSelectedItem->GetTextColor( ) : *wxBLACK );

        wxColourDialog  dlg( m_pCanvas, &cData );
        if( dlg.ShowModal( )  ==  wxID_OK )
        {
            SetPageDirty( true );
            HideEditWindow( );

            OBJLIST::iterator i;
            OBJLIST &listSelected = m_drawingObjects.GetSelectedItems( );

            CChangeAction* action = new CChangeAction( m_pDocument );
            action->PreAction( m_drawingObjects );

            wxColourData    cData = dlg.GetColourData( );
            wxColour        color = cData.GetColour( );
            for( i = listSelected.begin(); i != listSelected.end(); ++i )
            {
                CBaseDrawingObject* obj = *i;

                if (obj)
                {
                    obj->SetTextColor( color, true );
                }
            }

            action->PostAction( m_drawingObjects );
            m_drawingObjects.AddAction( action );
            InvalidateSelection( false );
        }
    }
}


//*****************************************************************************
void DocShareView::OnDocshareToFront( wxCommandEvent& event )
{
    IICRect rect;
    m_drawingObjects.BringToFront( rect );

    SetPageDirty( true );
    HideEditWindow( );

    InvalidateSelection( rect, false );
}


//*****************************************************************************
void DocShareView::OnDocshareToBack( wxCommandEvent& event )
{
    IICRect rect;
    m_drawingObjects.SendBack( rect );

    SetPageDirty( true );
    HideEditWindow( );

    InvalidateSelection( rect, false );
}


//*****************************************************************************
void DocShareView::OnDocshareTogTrans( wxCommandEvent& event )
{
    if( m_drawingObjects.GetSelCount( )  >  0 )
    {
        SetPageDirty( true );
        HideEditWindow( );

        OBJLIST::iterator i;
        OBJLIST &listSelected = m_drawingObjects.GetSelectedItems();

        CChangeAction* action = new CChangeAction( m_pDocument );
        action->PreAction( m_drawingObjects );
        CBaseDrawingObject* obj = *listSelected.begin( );
        bool    bTransparent = !obj->IsTransparent( );
        for( i = listSelected.begin(); i != listSelected.end(); ++i )
        {
            obj = *i;

            if (obj)
            {
                obj->ToggleTransparency( bTransparent, true );
            }
        }

        action->PostAction( m_drawingObjects );
        m_drawingObjects.AddAction( action );
        InvalidateSelection( false );
    }
}


//*****************************************************************************
void DocShareView::OnDocshareNew( wxCommandEvent& event )
{
    AllowHideProgress( false );

    if (m_bDirty)
    {
        int ret  = CUtils::DisplayMessage( m_pCanvas, CUtils::IDS_IIC_PLEASECONFIRM, CUtils::IDS_IIC_PROMPT_SAVEDOC, wxYES_NO | wxCANCEL | wxICON_QUESTION );
        if( ret == wxID_YES  ||  ret == wxID_NO )
        {
            if( !UpdatePageMarkup( true, ret==wxID_YES ? true : false ) )
            {
                AllowHideProgress( true );
                return;
            }
        }
        else	// in case someone else end the meeting
        {
            AllowHideProgress( true );
            return;
        }
    }
    else
    {
        if( !UpdatePageMarkup( true, false ) )	// unlock, no save
        {
            AllowHideProgress( true );
            return;
        }
    }

    while( true )
    {
        wxString    strTitle( _("Notice"), wxConvUTF8 );
        wxString    strPrompt( _("Input a name:"), wxConvUTF8 );
        wxTextEntryDialog   dlg( m_pCanvas, strPrompt, strTitle );
        if( dlg.ShowModal( )  ==  wxID_OK )
        {
            CUtils::MsgWait( 10, 1 );	// avoid stuck dialog bkgnd

            wxString    strDocID;
            strDocID = dlg.GetValue( );

            if( CUtils::ContainExtChars( strDocID ) )
            {
                CUtils::DisplayMessage( m_pCanvas, CUtils::IDS_IIC_WARNING, CUtils::IDS_DOCSHARE_ERR_INVALID );
                continue;
            }
            CUtils::EncodeStringFully( strDocID );
            LoadDocument( strDocID );
        }
        break;
    }
    AllowHideProgress( true );
}


//*****************************************************************************
void DocShareView::OnDocshareOpen( wxCommandEvent& event )
{
    // Make sure requestor is presenter for this meeting
    if( !IsPresenter( ) )
    {
        return;
    }

    AllowHideProgress(false);
    
    if( m_bDirty )
    {
        if( !UpdatePageMarkup( true, true) )
        {
            AllowHideProgress(true);
            return;
        }
    }
    else
    {
        if( !UpdatePageMarkup( true, false ) )
        {
            AllowHideProgress(true);
            return;
        }
    }

    bool        fRet = true;
    int         iRet;
    wxString    strDlgDocID;

    wxBeginBusyCursor( );
    DocExplorerDialog dialog( m_pCanvas, m_pDocument, m_strMeetingID, m_strDocId, true );
    wxEndBusyCursor( );

    if( dialog.m_fErrorInitializing )
    {
        fRet = false;
    }
    else
    {
        AllowHideProgress( true );      //  Allows Progress dlg to hide after deleting docs
        iRet = dialog.ShowModal( );
        strDlgDocID = dialog.m_strDocID;
        AllowHideProgress( false );
    }

    if( fRet )
    {
        if( iRet  ==  wxID_CANCEL )
        {
            fRet = false;
        }
        else if( iRet  ==  wxID_OK )
        {
            CUtils::MsgWait( 10, 1 );	// avoid stuck dialog bkgnd
            if( strDlgDocID.IsEmpty( ) )
                fRet = false;
            else
                fRet = LoadDocument( strDlgDocID );
        }
        else if( iRet  ==  wxID_DELETE )
        {
            //  Delete the selected share document then return
            fRet = false;
        }
        else if( iRet  ==  wxID_UP )
        {
            //  Upload a share document then start the share session
            wxString strDocId;
            fRet = Upload( strDocId );
            if ( fRet)
                fRet = LoadDocument( strDocId );
        }
        else
        {
            wxLogDebug( wxT("DocExplorerDialog.ShowModal( ) returned unexpected result - %d"), iRet );
            fRet = false;
        }
    }

    if (!fRet)
    { 
        // Relock current page
        LockCurrentPage( );
    }
    
    AllowHideProgress( true );
}


//*****************************************************************************
// Upload document to document share server
void DocShareView::OnDocshareSave( wxCommandEvent& event )
{
    wxLogDebug( wxT("entering DocShareView::OnDocshareSave( )") );

    AllowHideProgress(false);

    wxString xml;
    m_drawingObjects.GetXML( xml );

    UpdatePageMarkup(false, true, true);

    AllowHideProgress(true);
}


//*****************************************************************************
void DocShareView::OnDocshareSaveAs( wxCommandEvent& event )
{
//    wxLogDebug( wxT("entering DocShareView::OnDocshareSaveAs( )  --  not implemented  --  Only needed for Document Sharing") );

#ifdef SAVEASSVG
    wxSVGFileDC     svgDC( wxT("c:/svg01"), 600, 650 );
    OnDraw( &svgDC );
    bool    fRet;
    fRet = svgDC.Ok( );
#endif


    //  Only needed for Document Sharing - needs to be ported
#if 0
    if (!m_strDocId.IsEmpty())
    {
        DialogDocShareSaveDoc dlg(m_pDocument, m_strMeetingID, m_strDocId, this);
        if (dlg.DoModal()==IDOK)
        {
            CUtils::MsgWait(NULL, 10, 1);	// avoid stuck dialog bkgnd

            UpdatePageMarkup(true);	// Save and unlock

            CString strDocId;
            dlg.GetDocId(strDocId);

            CString strSourceURL(m_pDocument->m_Meeting.GetDocShareServer());
            strSourceURL += m_strMeetingID;
            strSourceURL += _T("/");
            strSourceURL += m_strDocId;
            strSourceURL += _T('/');

            CString strSourcePath(m_pDocument->m_Meeting.GetDocSharePath());
            strSourcePath += m_strMeetingID;
            strSourcePath += _T("/");
            CString     cstrT( m_strDocId );
            cstrT = CMBCSToWideNoXlate( CUTF8String( cstrT ));
            CUtils::EncodeStringFully( cstrT );
            strSourcePath += cstrT;
            strSourcePath += _T('/');

            CString strDestinationURL(m_pDocument->m_Meeting.GetDocShareServer());
            strDestinationURL += m_strMeetingID;
            strDestinationURL += _T("/");
            CString     cstrT2( strDocId );
            cstrT2 = CMBCSToWideNoXlate( CUTF8String( cstrT2 ));
            CUtils::EncodeStringFully( cstrT2 );
            strDestinationURL += cstrT2;
            if (m_strDocId == strDocId)
            {
                strDestinationURL += _T("_copy");
                strDocId += _T("_copy");
            }
            strDestinationURL += _T('/');

            CString strStatusCode, strStatusText;
            string xml;
            HRESULT hr = m_WebDavUtils.DAVCopyFolder(m_pCanvas, strSourceURL, strSourcePath, strDestinationURL, m_strUserName, m_strPasswd, xml, strStatusCode, strStatusText);
            if (FAILED(hr) || (strStatusCode!=_T("204")&&strStatusCode!=_T("201")))
            {
                CString cstrT;
                cstrT.LoadString( IDS_PANESHAREDOC_CANTSAVEDOC );
                CUtils::ReportDAVError(this, _ttoi(strStatusCode), strStatusText,
                                       _T("%s\n%s\n%s"), cstrT, hr);

                LoadPageMarkup( m_strDocId, m_nPageIndex ); // lock it again
            }
            else
            {
                LoadDocument(strDocId);
            }

        }
    }
    else
    {
        CUtils::DisplayMessage( m_pCanvas, CUtils::IDS_IIC_PLEASENOTE, CUtils::IDS_IIC_NODOC_TOSAVE, wxOK );
    }
#endif
}


//*****************************************************************************
void DocShareView::OnDocshareUploadDoc( wxCommandEvent& event )
{
    // Make sure requestor is presenter for this meeting
    if( !IsPresenter( ) )
    {
        return;
    }

    AllowHideProgress(false);

    if( m_bDirty )
    {
        if( !UpdatePageMarkup( true, true, true ) )
        {
            AllowHideProgress(true);
            return;
        }
    }
    else
    {
        if( !UpdatePageMarkup( true, false ) )  // give away the lock, the change during during uploading will be lost
        {
            AllowHideProgress(true);
            return;
        }
    }

    wxString strDocId;
    bool fRet = Upload( strDocId );
    if (fRet)
        LoadDocument( strDocId );
    
    // Relock current page if upload failed
    if (!fRet)
        LockCurrentPage( );

    AllowHideProgress(true);
}


//*****************************************************************************
void DocShareView::OnDocshareOrganizePages( wxCommandEvent& event )
{
    wxString    errmsg( _("Unable to organize file index. Please try it again later."), wxConvUTF8 );

    PAGEVECTOR  saPages;
    saPages.assign( m_saPages.begin( ), m_saPages.end( ) );

    wxString    strStatusCode, strStatusText;
    wxString    lockToken, lastModified;

    wxString    strTargetFileURL( m_pDocument->m_Meeting.GetDocShareServer( ) );

    wxString    strTargetFilePath( m_pDocument->m_Meeting.GetDocSharePath( ) );
    strTargetFilePath += m_strMeetingID;
    strTargetFilePath += wxT("/");

    wxString    strT( m_strDocId );
    CUtils::EncodeStringFully( strT );
    strTargetFilePath += strT;
    strTargetFilePath += wxT("/");

    AllowHideProgress( false );

    // Create directory
    long hr = AsyncSendRequest( wxT("MKCOL"), strTargetFileURL, strTargetFilePath, NULL, 0,
                                   strStatusCode, strStatusText, false, _T(""));
    if( FAILED(hr) )
    {
        long    lCode;
        strStatusCode.ToLong( &lCode );
        CUtils::ReportDAVError( m_pCanvas, lCode, strStatusText, wxT("%s\n%s\n%s"), errmsg, hr );

        AllowHideProgress( true );
        return;
    }

    wxString    indexFileUrl  = strTargetFileURL;
    wxString    indexFilePath = strTargetFilePath + wxT("index.xml");
    bool        fWasReadOnly = m_fReadOnly;	// remember the page state, in case cannnot lock the index file
    hr = LockFile( indexFileUrl, indexFilePath, true, lockToken, lastModified );
    if( SUCCEEDED(hr) )
    {
        UpdatePageMarkup( );	// unlock the current page first, it might be deleted

        // the index file maybe locked by someone else
        DocShareIndexDialog     dlg( m_pCanvas, saPages, m_fReadOnly );
        dlg.SetReadOnly( m_fReadOnly );

        AllowHideProgress( true );

        int     ret = dlg.ShowModal( );
        
        AllowHideProgress( false );

        CUtils::MsgWait( 10, 1 );	// avoid stuck dialog bkgnd
        if( ret == wxID_OK  &&  dlg.IsDirty( ) )
        {
            m_saPages.assign( dlg.m_saPages.begin( ), dlg.m_saPages.end( ) );
            wxString    indexFile;
            indexFile = wxT("<index>");

            for( int i=0; i<(int)m_saPages.size(); i++)
            {
                CDocPage    &p      = m_saPages.at( i );
                wxString    title   = p.m_strTitle;
                wxString    temp;
                CUtils::EncodeXMLString( title );
                temp = wxString::Format( wxT("<page title=\"%s\" file=\"%s\" altfile=\"%s\" scale=\"%.2f\" altscale=\"%.2f\" fittowidth=\"%C\"/>"),
                                         title.wc_str( ), p.m_strFile.wc_str( ), p.m_strAltFile.wc_str( ), p.m_dblScale, p.m_dblAltScale, (p.m_fFitToWidth) ? "T" : "F" );
                indexFile += temp;
            }

            indexFile += wxT("</index>");

            std::string     s = (const char *) indexFile.mb_str( wxConvUTF8 );
            hr = AsyncSendRequest( wxT("PUT"), indexFileUrl, indexFilePath, (BYTE*)s.c_str(), s.length(),
                                   strStatusCode, strStatusText, true, lockToken);
            if( FAILED(hr) )
            {
                if( !m_fReadOnly )	// had locked the index file, unlock it here
                {
                    LockFile( indexFileUrl, indexFilePath, false, lockToken, lastModified );	// unlock the index file
                }
                long    lCode;
                strStatusCode.ToLong( &lCode );
                CUtils::ReportDAVError( m_pCanvas, lCode, strStatusText, wxT("%s\n%s\n%s"), errmsg, hr );
                m_fReadOnly = fWasReadOnly;		// restore the page state

                AllowHideProgress( true );
                return;
            }

            if( m_nPageIndex  >=  (int)m_saPages.size( ) )
                m_nPageIndex = m_saPages.size( )-1;

            if( !m_fReadOnly )	// had locked the index file, unlock it here
            {
                LockFile( indexFileUrl, indexFilePath, false, lockToken, lastModified );	// unlock the index file
            }
            m_fReadOnly = fWasReadOnly;		// restore the page state

            // Force participants to reload index
            if( ret == wxID_OK )
                SendAction( wxT("load_index") );

            if( IsPresenter( ) )
            {
                LoadPageMarkup( m_strDocId, m_nPageIndex ); // lock the file again.
            }
            else
            {
                SendGotoAction( wxT("goto"), m_nPageIndex );
            }
        }
    }
    AllowHideProgress( true );
}


//*****************************************************************************
void DocShareView::OnDocshareInsertPage( wxCommandEvent& event )
{
    wxString    errmsg( _("Unable to create new document. Please try it again later."), wxConvUTF8 );

    AllowHideProgress( false );

    if( !UpdatePageMarkup( ) )
    {
        AllowHideProgress( true );
        return;
    }

    wxString    strStatusCode, strStatusText, strLockToken, strLastModified;
    wxString    strTargetFileURL( m_pDocument->m_Meeting.GetDocShareServer( ) );

    wxString    strTargetFilePath( m_pDocument->m_Meeting.GetDocSharePath( ) );
    strTargetFilePath += m_strMeetingID;
    strTargetFilePath += wxT("/");

    wxString    strT( m_strDocId );
    CUtils::EncodeStringFully( strT );
    strTargetFilePath += strT;
    strTargetFilePath += wxT("/");

    // Create directory
    long hr = AsyncSendRequest( _T("MKCOL"), strTargetFileURL, strTargetFilePath, NULL, 0,
                                   strStatusCode, strStatusText, false, m_strLockToken);
    if( FAILED( hr ) )
    {
        long    lCode;
        strStatusCode.ToLong( &lCode );
        CUtils::ReportDAVError( m_pCanvas, lCode, strStatusText,
                               _T("%s\n%s\n%s"), errmsg, hr);
        AllowHideProgress( true );
        return;
    }

    wxString    indexFileUrl  = strTargetFileURL;
    wxString    indexFilePath = strTargetFilePath + _T("index.xml");
    hr = LockFile( indexFileUrl, indexFilePath, true, strLockToken, strLastModified );
    if( SUCCEEDED( hr ) )
    {
        m_drawingObjects.DeleteAllItems( );
        m_pLastSelectedItem = NULL;

        ClearBackgroundImages( );

        CDocPage    page;
        bool        is_unique = true;
        int         i;
        for( i= (int)m_saPages.size()+1; ; i++ )
        {
            page.m_strTitle = wxString::Format( _("Page%d"), i );
            page.m_strFile = page.m_strTitle;
            page.m_dblScale = 1.0; // was m_dblZoom; but that seems wrong
            page.m_dblAltScale = 1.0;
            page.m_fFitToWidth = m_fFitToWidth;

            is_unique = true;
            for( int j=0; j<(int)m_saPages.size(); j++ )
            {
                CDocPage& p = m_saPages.at( j );
                if( page.m_strFile.CmpNoCase( p.m_strFile ) == 0 )
                {
                    is_unique = false;
                    break;
                }
            }

            if( is_unique )
                break;
        }
        ++m_nPageIndex;
        m_saPages.insert( m_saPages.begin( ) + m_nPageIndex, page );

        wxString    indexFile;
        indexFile = wxT("<index>");
        for( i=0; i<(int)m_saPages.size(); i++ )
        {
            CDocPage    &p      = m_saPages.at( i );
            wxString    title   = p.m_strTitle;
            wxString    temp;
            CUtils::EncodeXMLString( title );
            temp = wxString::Format( wxT("<page title=\"%s\" file=\"%s\" altfile=\"%s\" scale=\"%.2f\" altscale=\"%.2f\" fittowidth=\"%C\"/>"),
                                     title.wc_str( ), p.m_strFile.wc_str( ), p.m_strAltFile.wc_str( ), p.m_dblScale, p.m_dblAltScale, (p.m_fFitToWidth) ? "T" : "F" );
            indexFile += temp;
        }
        indexFile += wxT("</index>");
        std::string     s = (const char *) indexFile.mb_str( wxConvUTF8 );
        hr = AsyncSendRequest( wxT("PUT"), indexFileUrl, indexFilePath, (BYTE*)s.c_str(), s.length(),
                              strStatusCode, strStatusText, true, strLockToken );
        if( SUCCEEDED( hr ) )
            hr = LockFile( indexFileUrl, indexFilePath, false, strLockToken, strLastModified );	// unlock the index file
        if( FAILED( hr ) )
        {
            long    lCode;
            strStatusCode.ToLong( &lCode );
            CUtils::ReportDAVError( m_pCanvas, lCode, strStatusText, wxT("%s\n%s\n%s"), errmsg, hr );
            AllowHideProgress( true );
            return;
        }

        // Force participants to reload index
        SendAction( wxT("load_index") );

        if( IsPresenter( ) )
        {
            LoadPageMarkup( m_strDocId, m_nPageIndex );
        }
        else
        {
            SendGotoAction( wxT("goto"), m_nPageIndex );
        }
    }
    else
    {
        CUtils::ReportErrorMsg( hr, true, errmsg, m_pCanvas );
        if( IsPresenter()  &&  m_nPageIndex < (int)m_saPages.size( ) )
        {
            LoadPageMarkup( m_strDocId, m_nPageIndex );
        }
    }
    AllowHideProgress( true );
}


//*****************************************************************************
void DocShareView::SendGotoAction( wxString Action, int nID )
{
    wxString    userId;
    m_pDocument->m_Meeting.GetMeetingUserID( userId );
    wxString    actionXML;
    actionXML = wxString::Format( wxT("<action type=\"%s\" actor=\"%s\" page=\"%d\"/>"), Action.wc_str( ), userId.wc_str( ), nID );
    m_drawingObjects.SendActionXML( true, actionXML );
}


//*****************************************************************************
void DocShareView::SendPointerAction( wxString Action, int x, int y )
{
    wxString    userId;
    m_pDocument->m_Meeting.GetMeetingUserID( userId );
    wxString    actionXML;
    actionXML = wxString::Format( wxT("<action type=\"%s\" actor=\"%s\" x=\"%d\" y=\"%d\"/>"), Action.wc_str( ), userId.wc_str( ), x, y );
    m_drawingObjects.SendActionXML( true, actionXML );
}


//*****************************************************************************
void DocShareView::SendAction( wxString Action )
{
    wxString    userId;
    m_pDocument->m_Meeting.GetMeetingUserID( userId );
    wxString    actionXML;
    actionXML = wxString::Format( wxT("<action type=\"%s\" actor=\"%s\"/>"), Action.wc_str( ), userId.wc_str( ) );
    m_drawingObjects.SendActionXML( true, actionXML );
}


//*****************************************************************************
void DocShareView::OnDocshareFirstPage( wxCommandEvent& event )
{
    AllowHideProgress( false );
    if( IsPresenter( ) )
    {
        if( !UpdatePageMarkup( ) )
        {
            AllowHideProgress( true );
            return;
        }
        int nPageIndex = 0;

        int     nSaved = m_nPageIndex;
        m_nPageIndex = nPageIndex;
        if( !LoadPageMarkup( m_strDocId, nPageIndex ) )
        {
            m_nPageIndex = nSaved;
        }
    }
    else if( m_bRemoteControl )     // for remote control, send a msg to presenter
    {
        SendAction( wxT("top") );
    }
#ifdef LOCAL_SWITCH
    else if( !m_bWhiteboarding )    //  actually sharing a document
    {
        int nPageIndex = 0;

        int     nSaved = m_nPageIndex;
        m_nPageIndex = nPageIndex;
        if( !LoadPageMarkup( m_strDocId, nPageIndex ) )
        {
            m_nPageIndex = nSaved;
        }
    }
#endif
    AllowHideProgress( true );
    m_fGrabFocus = true;
}


//*****************************************************************************
void DocShareView::OnDocsharePreviousPage( wxCommandEvent& event )
{
    if( m_nPageIndex <= 0 )
        return;

    AllowHideProgress( false );
    if( IsPresenter( ) )
    {
        if( !UpdatePageMarkup( ) )
        {
            AllowHideProgress( true );
            return;
        }

        int     nPageIndex = m_nPageIndex - 1;
        if( nPageIndex  <  0 )
        {
            AllowHideProgress( true );
            return;
        }

        int     nSaved = m_nPageIndex;
        m_nPageIndex = nPageIndex;
        if( !LoadPageMarkup( m_strDocId, nPageIndex ) )
        {
            m_nPageIndex = nSaved;
        }
    }
    else if( m_bRemoteControl )     // for remote control, send a msg to presenter
    {
        SendAction( wxT("back") );
    }
#ifdef LOCAL_SWITCH
    else if( !m_bWhiteboarding )    //  actually sharing a document
    {
        int     nPageIndex = m_nPageIndex - 1;
        if( nPageIndex  <  0 )
        {
            AllowHideProgress( true );
            return;
        }

        int     nSaved = m_nPageIndex;
        m_nPageIndex = nPageIndex;
        if( !LoadPageMarkup( m_strDocId, nPageIndex ) )
        {
            m_nPageIndex = nSaved;
        }
    }
#endif
    AllowHideProgress( true );
    m_fGrabFocus = true;
}


//*****************************************************************************
void DocShareView::OnDocshareGotoPage( wxCommandEvent& event )
{
    int     i;

    AllowHideProgress( false );
    //  Reserve 100 menu entries for the "go to page" popup menu
    if( m_iID_MENU_BASE  ==  0 )
    {
        wxString    strT;
        strT = wxString::Format( wxT("IDC_DOCSHARE_POPMENU_GOTOPAGE%3.3d"), 0 );
        m_iID_MENU_BASE = wxXmlResource::GetXRCID( strT );
        for( i=1; i<100; i++ )
        {
            strT = wxString::Format( wxT("IDC_DOCSHARE_POPMENU_GOTOPAGE%3.3d"), i );
            wxXmlResource::GetXRCID( strT );
        }
    }

    wxMenu      menu;
    wxString    title;

    for( i=0; i < (int) m_saPages.size( ); ++i )
    {
        CDocPage    &page   = m_saPages.at( i );
        title = wxString::Format( wxT("%d %s"), i+1, page.m_strTitle.wc_str( ) );
        menu.AppendCheckItem( m_iID_MENU_BASE + i, title );
        if( m_nPageIndex  == i )
            menu.Check( m_iID_MENU_BASE + i, true );
    }

    menu.Connect( m_iID_MENU_BASE, m_iID_MENU_BASE + 99,
                  wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnGotoPage), 0, this );

    m_pCanvas->PopupMenu( &menu );
    AllowHideProgress( true );
}


//*****************************************************************************
void DocShareView::OnGotoPage( wxCommandEvent& event )
{
    int     index = event.GetId( ) - m_iID_MENU_BASE;

    AllowHideProgress( false );
    if( IsPresenter( ) )
    {
        // Save markup
        if( !UpdatePageMarkup( ) )
        {
            AllowHideProgress( true );
            return;
        }
        if( index  !=  m_nPageIndex )
        {
            m_nPageIndex = index;
            LoadPageMarkup( m_strDocId, m_nPageIndex );
        }
        else	// maybe a refresh message
        {
            SendAction( wxString( wxT("refresh")) );
        }
    }
    else if( m_bRemoteControl )     // for remote control, send a msg to presenter
    {
        SendGotoAction( wxT("goto"), index );
    }
#ifdef LOCAL_SWITCH
    else if( !m_bWhiteboarding )    //  actually sharing a document
    {
        if( index  !=  m_nPageIndex )
        {
            m_nPageIndex = index;
            LoadPageMarkup( m_strDocId, m_nPageIndex );
        }
    }
#endif
    AllowHideProgress( true );
    m_fGrabFocus = true;
}


//*****************************************************************************
void DocShareView::OnDocshareNextPage( wxCommandEvent& event )
{
    if( m_nPageIndex >= (int) m_saPages.size( ) - 1 )
        return;

    AllowHideProgress( false );
    if( IsPresenter( ) )
    {
        if( !UpdatePageMarkup( ) )
        {
            AllowHideProgress( true );
            return;
        }

        int     nPageIndex = m_nPageIndex + 1;
        if( nPageIndex >= (int) m_saPages.size( ) )
            nPageIndex = m_saPages.size() - 1;

        int     nSaved = m_nPageIndex;
        m_nPageIndex = nPageIndex;
        if( !LoadPageMarkup( m_strDocId, nPageIndex ) )
        {
            m_nPageIndex = nSaved;
        }
    }
    else if( m_bRemoteControl )     // for remote control, send a msg to presenter
    {
        SendAction( wxT("next") );
    }
#ifdef LOCAL_SWITCH
    else if( !m_bWhiteboarding )    //  actually sharing a document
    {
        int     nPageIndex = m_nPageIndex + 1;
        if( nPageIndex >= (int) m_saPages.size( ) )
            nPageIndex = m_saPages.size() - 1;

        int     nSaved = m_nPageIndex;
        m_nPageIndex = nPageIndex;
        if( !LoadPageMarkup( m_strDocId, nPageIndex ) )
        {
            m_nPageIndex = nSaved;
        }
    }
#endif
    AllowHideProgress( true );
    m_fGrabFocus = true;
}


//*****************************************************************************
void DocShareView::OnDocshareLastPage( wxCommandEvent& event )
{
    AllowHideProgress( false );
    if( IsPresenter( ) )
    {
        if( !UpdatePageMarkup( ) )
        {
            AllowHideProgress( true );
            return;
        }

        int     nPageIndex = m_saPages.size() - 1;
        int     nSaved = m_nPageIndex;
        m_nPageIndex = nPageIndex;
        if( !LoadPageMarkup( m_strDocId, nPageIndex ) )
        {
            m_nPageIndex = nSaved;
        }
    }
    else if( m_bRemoteControl )     // for remote control, send a msg to presenter
    {
        SendAction( wxT("bottom") );
    }
#ifdef LOCAL_SWITCH
    else if( !m_bWhiteboarding )    //  actually sharing a document
    {
        int     nPageIndex = m_saPages.size() - 1;
        int     nSaved = m_nPageIndex;
        m_nPageIndex = nPageIndex;
        if( !LoadPageMarkup( m_strDocId, nPageIndex ) )
        {
            m_nPageIndex = nSaved;
        }
    }
#endif
    AllowHideProgress( true );
    m_fGrabFocus = true;
}


//*****************************************************************************
void DocShareView::OnDocshareCut( wxCommandEvent& event )
{
    SetPageDirty( true );
    HideEditWindow( );

    IICRect rect;
    m_drawingObjects.GetSelectedRegion( rect );

    OnDocshareCopy( event );
    m_drawingObjects.OnEditDelete( );
    m_pLastSelectedItem = NULL;

    InvalidateSelection( rect );
}


//*****************************************************************************
void DocShareView::OnDocshareCopy( wxCommandEvent& event )
{
    m_drawingObjects.OnEditCopy( );
    if( (IsPresenter() || IsModerator())  &&  wxTheClipboard->Open( ) )	// also copy to clipboard as image
    {
        wxTheClipboard->Clear( );

        wxMemoryDC  dcMemory;

        IICRect     rect;
        // The image size is decided by the selected region
        m_drawingObjects.GetSelectedRegion( rect, false );

        wxBitmap    bitmap( rect.Width( ), rect.Height( ) );
        dcMemory.SelectObject( bitmap );
        dcMemory.SetBrush( *wxWHITE_BRUSH );

        wxPoint     offset = rect.TopLeft( );
        rect -= offset;
        dcMemory.DrawRectangle( 0, 0, rect.Width( ), rect.Height( ) );
        rect -= offset;		// so that drawing will start at (0,0)
        m_drawingObjects.PaintSelection( &dcMemory, rect, wxPoint(0,0) );
        dcMemory.SelectObject( wxNullBitmap );

        wxBitmapDataObject  *pBDO = new wxBitmapDataObject( bitmap );
        wxTheClipboard->SetData( pBDO );
        wxTheClipboard->Close( );
    }
}


//*****************************************************************************
void DocShareView::OnDocsharePaste( wxCommandEvent& event )
{
    HideEditWindow();

    wxPoint point = m_ptPasteOffset;

    IICRect rect;
    m_drawingObjects.UnSelect( rect );
    m_pLastSelectedItem = NULL;
    InvalidateSelection( rect );

    if( !m_drawingObjects.IsClipboardEmpty( ) )
    {
        m_drawingObjects.OnEditPaste( m_ptPasteOffset );
    }
    else if( wxTheClipboard->Open( ) )
    {
        if( wxTheClipboard->IsSupported( wxDF_TEXT )  ||  wxTheClipboard->IsSupported( wxDF_BITMAP ) )
        {
            if( wxTheClipboard->IsSupported( wxDF_TEXT ) )
            {
                if( point.x < 0 )
                    point.x = 100;
                if( point.y < 0 )
                    point.y = 100;

                wxTextDataObject    text;
                if( wxTheClipboard->GetData( text ) )
                {
                    wxString str = text.GetText( );
                    if( !str.IsEmpty( ) )
                    {
                        CTextBox    *pBox = new CTextBox( IICRect(point.x, point.y, point.x + 100, point.y + 100) );
                        pBox->SetText( str );
                        m_drawingObjects.Add( pBox, true );
                    }
                }
            }
            else if( wxTheClipboard->IsSupported( wxDF_BITMAP ) )
            {
                if( point.x < 0 )
                    point.x = 100;
                if( point.y < 0 )
                    point.y = 100;

                wxBitmapDataObject  bitmap;
                if( wxTheClipboard->GetData( bitmap ) )
                {
                    CImage  *pBox = new CImage( IICRect( point.x, point.y, point.x + 100, point.y + 100 ) );
                    pBox->SetBitmap( bitmap.GetBitmap( ) );

                    DWORD   size = pBox->GetImageSize( );
                    if( size  <  0x10000L )
                    {
                        m_drawingObjects.Add( pBox, true );
                    }
                    else
                    {
                        delete pBox;
                        // Close the clipboard.
                        wxTheClipboard->Close( );

                        CUtils::DisplayMessage( m_pCanvas, CUtils::IDS_IIC_WARNING, CUtils::IDS_DOCSHARE_IMAGETOOLARGE );
                        return;
                    }
                }
            }
        }
        // Close the clipboard.
        wxTheClipboard->Close( );
    }

    SetPageDirty( true );
    m_drawingObjects.GetSelectedRegion( rect );
    InvalidateSelection( rect );
}


//*****************************************************************************
void DocShareView::OnDocshareDeleteSel( wxCommandEvent& event )
{
    EditDelete( true );
}


//*****************************************************************************
void DocShareView::EditDelete( bool aNotifyServer )
{
    SetPageDirty( true );
    HideEditWindow( );

    IICRect rect;
    m_drawingObjects.GetSelectedRegion( rect );

    if( !rect.IsRectEmpty( ) )
    {
        m_drawingObjects.OnEditDelete( aNotifyServer );
    }
    m_pLastSelectedItem = NULL;
    InvalidateSelection( rect );
}


//*****************************************************************************
void DocShareView::OnDocshareClearPage( wxCommandEvent& event )
{
    IICRect rect;
    rect.SetRectEmpty( );
    m_drawingObjects.SelectInRegion( rect );	// will select all

    OnDocshareDeleteSel( event );
}


//*****************************************************************************
void DocShareView::OnDocshareClearAll( wxCommandEvent& event )
{
    wxString    strT( _("This will remove annotations from all pages.\nAre you sure you want to continue?"), wxConvUTF8 );
    wxString    strTitle = PRODNAME;
    wxMessageDialog dlg( m_pCanvas, strT, strTitle, wxYES_NO | wxICON_QUESTION );
    if ( dlg.ShowModal( ) != wxID_YES )
        return;

    AllowHideProgress( false );
        
    // Clear current page
    OnDocshareClearPage( event );
    UpdatePageMarkup( true, true, true, true );
        
    // Delete markup from all pages
    int saveIndex = m_nPageIndex;
    for( int i=0; i < (int) m_saPages.size( ); ++i )
    {
        if ( saveIndex != i )
        {
            m_nPageIndex = i;
            UpdatePageMarkup( false, true, true, true );    // force save of an empty XML file
        }
    }
    
    // Reload current page
    m_nPageIndex = saveIndex;
    LoadPageMarkup( m_strDocId, m_nPageIndex );    

    SendAction( wxString( wxT("refresh")) );

    AllowHideProgress( true );
}


//*****************************************************************************
void DocShareView::OnDocshareUndo( wxCommandEvent& event )
{
    SetPageDirty( true );
    HideEditWindow( );

    IICRect rect(0,0,0,0);
    m_drawingObjects.OnEditUndo( rect );
    m_pLastSelectedItem = NULL;
    InvalidateSelection( rect );
}


//*****************************************************************************
void DocShareView::OnDocshareRedo( wxCommandEvent& event )
{
    SetPageDirty( true );
    HideEditWindow( );

    IICRect rect(0,0,0,0);
    m_drawingObjects.OnEditRedo( rect );
    m_pLastSelectedItem = NULL;
    InvalidateSelection( rect );
}


//*****************************************************************************
void DocShareView::OnTimer( wxTimerEvent& event )
{
    if( m_fIgnoreUpdateSignal )
        return;

    // I wanted keyboard focus to go to the canvas when the doc share session was started, but SetFocus was
    // ignored...so call during timer event after window is created.  Also take focus after page change.
    if (m_fGrabFocus)
    {
        if (!m_pMMF->IsChatActive())
            CUtils::SetFocus(m_pCanvas);
        m_fGrabFocus = false;
    }

    //  Update the status of all the toolbar buttons at once
    bool    fEnable = true;
    if( m_pDialogProgress )
        if( m_pDialogProgress->IsEnabled( ))
            fEnable = false;

    MainPanel *pMainPanel = m_pMMF->GetMainPanel();
    wxToolBar *wndToolBar  = pMainPanel->GetDocShareToolbar();
    wxToolBar *wndToolBar2 = pMainPanel->GetDocShareToolbar2();

    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_NEW"),           ( false ) );
    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_OPEN"),          (IsPresenter( ) && fEnable ) );
    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_SAVE"),          (IsPresenter( ) && !m_fReadOnly && fEnable ) );

#ifdef SAVEASSVG
    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_SAVEAS"),        ( true ) );
#else
    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_SAVEAS"),        ( false ) );
#endif

    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_UPLOADDOC"),     (IsPresenter( ) && fEnable ) );
    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_ORGANIZEPAGES"), (IsPresenter( ) && fEnable ) );
    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_INSERTPAGE"),    (IsPresenter( ) && fEnable ) );
    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_FIRSTPAGE"),     (CanEdit( ) && m_nPageIndex != 0 && fEnable ) );
    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_PREVIOUSPAGE"),  (CanEdit( ) && m_nPageIndex  > 0 && fEnable ) );
    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_GOTOPAGE"),      (CanEdit( ) && 1 < (int)(m_saPages.size()) && fEnable ) );
    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_NEXTPAGE"),      (CanEdit( ) && m_nPageIndex  < (int)(m_saPages.size()-1) && fEnable ) );
    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_LASTPAGE"),      (CanEdit( ) && m_nPageIndex  < (int)(m_saPages.size()-1) && fEnable ) );
    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_CUT"),           m_drawingObjects.GetSelCount()>0 && CanEdit( ) );
    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_COPY"),          m_drawingObjects.GetSelCount()>0 && CanEdit( ) );
    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_PASTE"),     CanEdit() );
    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_DELETESEL"), m_drawingObjects.GetSelCount()>0 && CanEdit( ) );
    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_CLEARPAGE"), m_drawingObjects.GetSize()>0 && CanEdit( ) );
    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_CLEARALL"),  IsPresenter( ) && !IsWhiteboard( ) );
    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_UNDO"),      m_drawingObjects.CanUndo( ) && CanEdit( ) );
    wndToolBar->EnableTool( XRCID("IDC_DOCSHARETB_REDO"),      m_drawingObjects.CanRedo( ) && CanEdit( ) );

    wndToolBar2->ToggleTool( XRCID("IDC_DOCSHARETB_POINTER"), m_bRemotePointer );
    wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_POINTER"), IsPresenter( ) );

    wndToolBar2->ToggleTool( XRCID("IDC_DOCSHARETB_SELECT"), m_strObjType.IsEmpty() && !m_bRemotePointer );
    wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_SELECT"), CanEdit( ) );

    wndToolBar2->ToggleTool( XRCID("IDC_DOCSHARETB_LINE"),  m_strObjType==DOType_Line);
    wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_LINE"),  CanEdit( ) );

    wndToolBar2->ToggleTool( XRCID("IDC_DOCSHARETB_ARROW"), m_strObjType==DOType_ArrowLine );
    wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_ARROW"), CanEdit( ) );

    wndToolBar2->ToggleTool( XRCID("IDC_DOCSHARETB_SCRIBBLE"), m_strObjType==DOType_CurveLine );
    wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_SCRIBBLE"), CanEdit( ) );

    wndToolBar2->ToggleTool( XRCID("IDC_DOCSHARETB_HIGHLIGHT"), m_strObjType==DOType_HighLite );
    wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_HIGHLIGHT"), CanEdit( ) );

    wndToolBar2->ToggleTool( XRCID("IDC_DOCSHARETB_RECTANGLE"), m_strObjType==DOType_Rectangle );
    wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_RECTANGLE"), CanEdit( ) );

    wndToolBar2->ToggleTool( XRCID("IDC_DOCSHARETB_ELLIPSE"), m_strObjType==DOType_Ellipse );
    wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_ELLIPSE"), CanEdit( ) );

    wndToolBar2->ToggleTool( XRCID("IDC_DOCSHARETB_TEXTBOX"), m_strObjType==DOType_TextBox );
    wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_TEXTBOX"), CanEdit( ) );

    wndToolBar2->ToggleTool( XRCID("IDC_DOCSHARETB_CHECKMARK"), m_strObjType==DOType_CheckMark );
    wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_CHECKMARK"), CanEdit( ) );

    wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_LINETHICKNESS"), m_drawingObjects.GetSelCount()>0 && CanEdit() );

    wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_LINESTYLE"), m_drawingObjects.GetSelCount()>0 && CanEdit() );

    bool enable = true;
    if( m_drawingObjects.GetSelCount()==1 && m_pLastSelectedItem!=NULL )
    {
        enable = m_pLastSelectedItem->AcceptText();
    }
    wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_FONTSETTINGS"), m_drawingObjects.GetSelCount()>0 && CanEdit() && enable );

    wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_LINECOLOR"), m_drawingObjects.GetSelCount()>0 && CanEdit() );

    enable = true;
    if( m_drawingObjects.GetSelCount()==1 && m_pLastSelectedItem!=NULL )
    {
        enable = m_pLastSelectedItem->AcceptBkgndColor();
    }
    wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_BGCOLOR"), m_drawingObjects.GetSelCount()>0 && CanEdit() && enable );

    enable = true;
    if( m_drawingObjects.GetSelCount()==1 && m_pLastSelectedItem!=NULL )
    {
        enable = m_pLastSelectedItem->AcceptText();
    }
    wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_TEXTCOLOR"), m_drawingObjects.GetSelCount()>0 && CanEdit() && enable);
    wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_TOFRONT"),   m_drawingObjects.GetSelCount()>0 && CanEdit() );
    wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_TOBACK"),    m_drawingObjects.GetSelCount()>0 && CanEdit() );
    wndToolBar2->EnableTool( XRCID("IDC_DOCSHARETB_TOGTRANS"),  m_drawingObjects.GetSelCount()>0 && CanEdit() );

    if (m_bRemotePointer && (m_pointerPos.x >=0 || m_pointerPos.y >= 0)) 
    {
        SendPointerAction( wxT("pointer"), m_pointerPos.x, m_pointerPos.y );
        m_pointerPos.x = m_pointerPos.y = -1;
    }

    m_timer.Start( 500, wxTIMER_ONE_SHOT );
}


//*****************************************************************************
//*****************************************************************************
//
//   Rodent events
//
//*****************************************************************************
//*****************************************************************************
void DocShareView::OnLButtonDown( wxMouseEvent& event )     // was (UINT nFlags, CPoint point)
{
    if( !IsPresenter( )  &&  !m_bRemoteControl )
        return;

    wxPoint point = event.GetPosition( );
    UnscaleThePoint( point );

    wxLogDebug( wxT("entering DocShareView::OnLButtonDown( ) at (%d, %d)"), point.x, point.y );

    if( !CanEdit( ) || m_bRemotePointer )
        return;

    wxClientDC  dc( m_pCanvas );
    m_pCanvas->DoPrepareDC( dc );
    CUtils::SetFocus(m_pCanvas);

	HideEditWindow( );

	m_ptEnd = m_ptStart = point;

    CalcUnscrolledPosition( point );

	m_ptPasteOffset = point;	// remember this position for paste
	m_drawingObjects.ResetPasteOffset( );

    CBaseDrawingObject  *pHitTest = NULL;

	m_nDragSide = NO_SIDE;
	m_bSelected = false;

    if( !m_strObjType.IsEmpty() )
	{
		IICRect  rect;
		m_drawingObjects.UnSelect( rect );
		m_pLastSelectedItem = NULL;
		InvalidateSelection( rect );

		snap_to_grid( point );
        wxPoint sp = point;
        CalcScrolledPosition( sp );
        ScaleThePoint( sp );
        sp = m_pCanvas->ClientToScreen( sp );

#ifdef __WXMSW__
		SetCursorPos( sp.x, sp.y );
#endif

        IICRect rc(point, wxSize(0,0));
		point += wxSize(0,0);

		m_bCreating = true;
		if (m_strObjType==DOType_Line)
		{
			pHitTest = new CLine(rc);
			m_drawingObjects.Add(pHitTest, false);
			m_nDragSide = LINE_END;
		}
		else if (m_strObjType==DOType_CurveLine)
		{
			pHitTest = new CCurveLine(rc);
			m_drawingObjects.Add(pHitTest, false);
			m_nDragSide = LINE_END;
		}
		else if (m_strObjType==DOType_HighLite)
		{
			pHitTest = new CHighLite(rc);
			m_drawingObjects.Add(pHitTest, false);
			m_nDragSide = LINE_END;
		}
		else if (m_strObjType==DOType_ArrowLine)
		{
			pHitTest = new CArrowLine(rc);
			m_drawingObjects.Add(pHitTest, false);
			m_nDragSide = LINE_END;
		}
		else if (m_strObjType==DOType_Rectangle)
		{
			pHitTest = new CRectangle(rc);
			m_drawingObjects.Add(pHitTest, false);
			m_nDragSide = BOTTOMRIGHT_SIDE;
		}
		else if (m_strObjType==DOType_Ellipse)
        {
            //  Ellipse have problems with zero size rectangles...
            IICRect     rcEllipse( point, wxSize(2,2) );
			pHitTest = new CEllipse( rcEllipse );
			m_drawingObjects.Add(pHitTest, false);
			m_nDragSide = BOTTOMRIGHT_SIDE;
		}
		else if (m_strObjType==DOType_Rectangle_Opaque)
		{
			pHitTest = new CRectangle(rc);
			m_drawingObjects.Add(pHitTest, false);
			m_nDragSide = BOTTOMRIGHT_SIDE;
		}
		else if (m_strObjType==DOType_Ellipse_Opaque)
		{
			pHitTest = new CEllipse(rc);
			m_drawingObjects.Add(pHitTest, false);
			m_nDragSide = BOTTOMRIGHT_SIDE;
		}
		else if (m_strObjType==DOType_TextBox)
		{
			pHitTest = new CTextBox(rc);
			pHitTest->SetStyle(CTextBox::m_nDefaultStyle);
			m_drawingObjects.Add(pHitTest, false);
			m_nDragSide = BOTTOMRIGHT_SIDE;
		}
		else if (m_strObjType==DOType_CheckMark)
        {
			rc.bottom = rc.top + 64;
			rc.right = rc.left + 64;
			rc += wxPoint(-32, -32);
			pHitTest = new CCheckMark(rc);
			m_drawingObjects.Add(pHitTest, true);
			m_nDragSide = NO_SIDE;
			m_bCreating = false;
		}
		else if (m_strObjType==DOType_RadioMark)
		{
			pHitTest = CRadioMark::GetRadioMark();
			IICRect rcMark = pHitTest->GetRect();
			InvalidateSelection(rcMark);

			rc.bottom = rc.top + 32;
			rc.right = rc.left + 32;
			rc += wxPoint(-16, -16);
			pHitTest->SetRect(rc);
			m_drawingObjects.Add(pHitTest, true);
			m_nDragSide = NO_SIDE;
			m_bCreating = false;
		}
		SetPageDirty(true);
	}

	if (pHitTest==NULL)
	{
		if (m_pLastSelectedItem)
		{
			pHitTest = m_pLastSelectedItem->HitTest(point, m_nDragSide);
		}

		if (pHitTest==NULL)
		{
			pHitTest = m_drawingObjects.HitTest(point, m_nDragSide);
		}
	}

    bool    fIsDown = ::wxGetKeyState( WXK_SHIFT );
	if( fIsDown )
	{
		m_drawingObjects.SelectItem( pHitTest );
	}
    else if( m_drawingObjects.GetSelCount( ) > 0 )
    {
        if( !pHitTest || !pHitTest->IsSelected() || m_nDragSide!=NO_SIDE )
		{
            IICRect rect(0,0,0,0);
			m_drawingObjects.UnSelect(rect);
            m_pLastSelectedItem = NULL;
			InvalidateSelection(rect);
		}
	}

	if (pHitTest)
	{
		if (pHitTest != m_pLastSelectedItem)		// select the new item
		{
			m_drawingObjects.SelectItem(pHitTest);
			DrawItem( dc, pHitTest);

			m_pLastSelectedItem = pHitTest;

			m_bSelected = false;
		}
		else
		{
			m_bSelected = true;
		}

		m_bDragging = m_bCreating;	// we will set default size for certain objects, so always assume dragging when creating
		if (pHitTest->IsMovable() || m_nDragSide != NO_SIDE)	// move or resize
		{
            m_pCanvas->CaptureMouse( );
			if (m_nDragSide != NO_SIDE)	// if mouse is on the border, resize it
			{
				m_nAction = MA_RESIZING;
			}
			else
			{
				m_nAction = MA_DRAGGING;
            }
            m_sizeDelta.Set( point.x - pHitTest->GetRect( ).left, point.y - pHitTest->GetRect( ).top );
			SetCursor(m_nDragSide);

			m_lock += 1;	// lock the period between mouse down/up
		}
	}
	else
	{
		m_nAction = MA_MULTI_SELECTION;
		m_bDragging = false;
        m_pCanvas->CaptureMouse( );
	}
}


//*****************************************************************************
void DocShareView::OnLButtonDClick( wxMouseEvent& event )       //  WAS:  (UINT nFlags, CPoint point)
{
//    wxLogDebug( wxT("entering DocShareView::OnLButtonDClick( )") );
    if( !IsPresenter( )  &&  !m_bRemoteControl )
        return;

    if( !CanEdit( ) || m_bRemotePointer )
        return;

    if( m_pLastSelectedItem  &&  m_pLastSelectedItem->AcceptText( ) )
    {
        IICRect     rc = m_pLastSelectedItem->GetRect( );
        CalcScrolledPosition( rc );
        ScaleTheRect( rc );
        m_pCtlEdit->SetSize( rc.left, rc.top, rc.Width( ), rc.Height( ) );
        wxString    txt;
        m_pLastSelectedItem->GetText( txt );

        bool        fULine  = false;
        int         iStyle  = wxFONTSTYLE_NORMAL;
        int         iWeight = wxFONTWEIGHT_NORMAL;
        wxString    strStyle    = m_pLastSelectedItem->GetFontStyle( );
        if( strStyle.Find( wxT("bold|") ) != wxNOT_FOUND )
            iWeight = wxFONTWEIGHT_BOLD;
        if( strStyle.Find( wxT("italic")) != wxNOT_FOUND )
            iStyle = wxFONTSTYLE_ITALIC;
        if( strStyle.Find( wxT("underline")) != wxNOT_FOUND )
            fULine = true;
        wxFont      cfont( m_pLastSelectedItem->GetFontSize( ), wxFONTFAMILY_SWISS, iStyle, iWeight, fULine, m_pLastSelectedItem->GetFontFace( ) );
        m_pCtlEdit->SetFont( cfont );

        m_pCtlEdit->SetValue( txt );
        m_pCtlEdit->Show( );
        m_pCtlEdit->SetFocus();
    }
}


//*****************************************************************************
void DocShareView::OnLButtonUp( wxMouseEvent& event )       //  was (UINT nFlags, CPoint point)
{
//    wxLogDebug( wxT("entering DocShareView::OnLButtonUp( )") );
    if( !IsPresenter( )  &&  !m_bRemoteControl )
        return;

    wxPoint point = event.GetPosition( );
    UnscaleThePoint( point );

	if ( !CanEdit() || m_bRemotePointer )
		return;

    wxClientDC  dc( m_pCanvas );
    m_pCanvas->DoPrepareDC( dc );

	switch(m_nAction)
	{
	case MA_DRAGGING:
        m_pCanvas->ReleaseMouse( );
        m_pCanvas->SetCursor( wxNullCursor );

		if (m_bDragging && m_pLastSelectedItem!=NULL)
		{
			if (m_ptStart != m_ptEnd)
			{
				CChangeAction* action = new CChangeAction(m_pDocument);
				action->PreAction(m_drawingObjects);

				point -= m_sizeDelta;
				OBJLIST::iterator i;
				OBJLIST listSelected = m_drawingObjects.GetSelectedItems();
                wxPoint ref = m_pLastSelectedItem->GetOrigRect().TopLeft();
				bool itemDeleted = false;
				for (i =  listSelected.begin(); i != listSelected.end(); ++i)
				{
					CBaseDrawingObject* obj = *i;
					if (obj && obj->IsMovable())
					{
                        wxPoint pt = point + obj->GetOrigRect().TopLeft() - ref;
                        wxPoint ptScrolled( 0, 0 );
                        CalcScrolledPosition( ptScrolled );
                        obj->Drop( (wxDC *) &dc, pt, ptScrolled );
						DrawItem( dc, obj);
                        if( obj->IsDeleting() )
                            itemDeleted = true;
					}
				}
				action->PostAction(m_drawingObjects);
				m_drawingObjects.AddAction(action);
				SetPageDirty(true);

				if (itemDeleted)
				{
					// Fix memory leak, if remote user deletes objects being dragged
					for (i =  listSelected.begin(); i != listSelected.end(); ++i)
					{
						CBaseDrawingObject* obj = *i;
						if (obj->IsDeleting())
						{
							obj->Release();
						}
						else
						{
							obj->SetMode(BOX_MODE_IDLE);
						}
					}
					m_drawingObjects.UnSelect();
					m_pLastSelectedItem = NULL;
				}
			}
		}

		if (m_ptStart == m_ptEnd && m_bSelected)
        {
			if (m_pLastSelectedItem && m_pLastSelectedItem->AcceptText())
			{
				IICRect rc = m_pLastSelectedItem->GetRect( );
                CalcScrolledPosition( rc );
                ScaleTheRect( rc );
				m_pCtlEdit->SetSize( rc.left, rc.top, rc.Width( ), rc.Height( ) );
                wxString    txt;
				m_pLastSelectedItem->GetText( txt );
                //  Text entry setup
                bool        fULine  = false;
                int         iStyle  = wxFONTSTYLE_NORMAL;
                int         iWeight = wxFONTWEIGHT_NORMAL;
                wxString    strStyle    = m_pLastSelectedItem->GetFontStyle( );
                if( strStyle.Find( wxT("bold|") ) != wxNOT_FOUND )
                    iWeight = wxFONTWEIGHT_BOLD;
                if( strStyle.Find( wxT("italic")) != wxNOT_FOUND )
                    iStyle = wxFONTSTYLE_ITALIC;
                if( strStyle.Find( wxT("underline")) != wxNOT_FOUND )
                    fULine = true;
                wxFont      cfont( m_pLastSelectedItem->GetFontSize( ), wxFONTFAMILY_SWISS, iStyle, iWeight, fULine, m_pLastSelectedItem->GetFontFace( ) );
                m_pCtlEdit->SetFont( cfont );

				m_pCtlEdit->SetValue( txt );
				m_pCtlEdit->Show( );
				m_pCtlEdit->SetFocus( );
            }
		}

		break;

	case MA_RESIZING:
        m_pCanvas->ReleaseMouse( );
        m_pCanvas->SetCursor( wxNullCursor );
        if( m_bDragging  &&  m_pLastSelectedItem != NULL )
		{
			CChangeAction* resizeAction = NULL;
            if( !m_bCreating )
			{
				resizeAction = new CChangeAction( m_pDocument );
				resizeAction->PreAction( m_drawingObjects );
			}

			if (m_bCreating && m_pLastSelectedItem->IsCurveLine())
            {
                wxPoint pt( 0, 0 );
                CalcScrolledPosition( pt );
                ((CCurveLine*)m_pLastSelectedItem)->AddPoint( point - pt, TRUE );
			}
			else
			{
                wxPoint pt( 0, 0 );
                CalcScrolledPosition( pt );
                m_pLastSelectedItem->EndResize( (wxDC *) &dc, point, m_nDragSide, pt );
			}
			//if initial size is null, delete it
            if( m_bCreating  &&  m_pLastSelectedItem->GetRect().Width() == 0  &&  m_pLastSelectedItem->GetRect().Height() == 0 )
			{
				wxPoint pt = point;
                if( m_pLastSelectedItem->GetType() == DOType_Rectangle )
				{
					pt.x += 180;
					pt.y += 120;
                    wxPoint pt2( 0, 0 );
                    CalcScrolledPosition( pt2 );
					m_pLastSelectedItem->EndResize( (wxDC *) &dc, pt, m_nDragSide, pt2 );
				}
                else if( m_pLastSelectedItem->GetType() == DOType_Ellipse )
				{
					pt.x += 120;
					pt.y += 120;
                    wxPoint pt2( 0, 0 );
                    CalcScrolledPosition( pt2 );
                    m_pLastSelectedItem->EndResize( (wxDC *) &dc, pt, m_nDragSide, pt2 );
				}
                else if( m_pLastSelectedItem->GetType() == DOType_TextBox )
				{
					pt.x += 180;
					pt.y += 180;
                    wxPoint pt2( 0, 0 );
                    CalcScrolledPosition( pt2 );
                    m_pLastSelectedItem->EndResize( (wxDC *) &dc, pt, m_nDragSide, pt2 );
				}
				else
				{
					DrawItem( dc, m_pLastSelectedItem);
					m_drawingObjects.Delete(m_pLastSelectedItem);
					m_drawingObjects.UnSelect();
					delete m_pLastSelectedItem;
					m_pLastSelectedItem = NULL;
					break;
				}
			}

			SetPageDirty(true);
			DrawItem( dc, m_pLastSelectedItem);

			if (m_bCreating)
			{
				CCreateAction* action = new CCreateAction(m_pDocument);
				action->Do(m_drawingObjects);
				m_drawingObjects.AddAction(action);
				m_bCreating = false;
			}
			else
			{
				resizeAction->PostAction( m_drawingObjects );
				m_drawingObjects.AddAction( resizeAction );
			}
		}
        else if( m_bCreating  &&  m_pLastSelectedItem != NULL )	// a single click
		{
			DrawItem( dc, m_pLastSelectedItem );
			m_drawingObjects.Delete( m_pLastSelectedItem );
			m_drawingObjects.UnSelect( );
			delete m_pLastSelectedItem;
			m_pLastSelectedItem = NULL;
		}

		break;

	case MA_MULTI_SELECTION:
        m_pCanvas->ReleaseMouse( );
        m_pCanvas->SetCursor( wxNullCursor );
        if( m_bDragging )
        {
            //  Draw this box to obliterate the final selection rectangle
			IICRect     rect( m_ptStart.x, m_ptStart.y, m_ptEnd.x, m_ptEnd.y );
            rect.NormalizeRect( );
            CalcUnscrolledPosition( rect );
            ScaleTheRect( rect );
            if( rect.left != rect.right  ||  rect.top != rect.bottom )
			{
                wxColour    black( 0, 0, 0 );
                wxPen   newPen( black, 1, wxDOT );
                wxPen   oldPen = dc.GetPen( );
                dc.SetPen( newPen );

//                wxLogDebug( wxT("in DocShareView::OnLButtonUp( ) - case MULTI_SELECT - drawing box!?!?") );
                int iLogical = dc.GetLogicalFunction( );
                dc.SetLogicalFunction( wxEQUIV );
                dc.DrawLine( rect.left, rect.top, rect.right, rect.top );
                dc.DrawLine( rect.right, rect.top, rect.right, rect.bottom );
                dc.DrawLine( rect.right, rect.bottom, rect.left, rect.bottom );
                dc.DrawLine( rect.left, rect.bottom, rect.left, rect.top );
                dc.SetLogicalFunction( iLogical );

                dc.SetPen( oldPen );
			}

            if( !rect.IsRectEmpty( ) )
			{
                UnscaleTheRect( rect );
				m_drawingObjects.SelectInRegion( rect );
                if( m_drawingObjects.GetSelCount( )  >  0 )
				{
					InvalidateSelection( rect );
				}
			}
        }
		break;
	default:
        if( m_pCanvas->HasCapture( ) )
        {
            m_pCanvas->ReleaseMouse( );
        }
        m_pCanvas->SetCursor( wxNullCursor );
		break;
	}

	m_bDragging = false;
	m_nAction = MA_NONE;
	m_sizeDelta = wxSize(0,0);

	// stay for highlite pen
	if( m_strObjType == DOType_HighLite )
	{
		OnDocshareHighlight( (wxCommandEvent &) event );
	}
	else
	{
		OnDocshareSelect( (wxCommandEvent &) event );
	}

    m_lock = 0;	// unlock it

}


//*****************************************************************************
void DocShareView::OnMotion( wxMouseEvent& event )
{
    if( !IsPresenter( )  &&  !m_bRemoteControl )
        return;

    wxPoint point = event.GetPosition( );
    UnscaleThePoint( point );

//    wxLogDebug( wxT("entering DocShareView::OnMotion( %d, %d )"), point.x, point.y );

	if ( !CanEdit() )
		return;

    if (m_bRemotePointer) {
        CalcUnscrolledPosition( point );
        // Periodic timer will check for new pointer pos to send
        m_pointerPos = point;
        return;
    }

    wxClientDC  dc( m_pCanvas );
    m_pCanvas->DoPrepareDC( dc );

	switch (m_nAction)
	{
		case MA_DRAGGING:
		{
//            wxLogDebug( wxT("::OnMotion( )  --  DRAGGING") );
			if (m_ptStart != point && m_pLastSelectedItem!=NULL)
			{
				SetCursor(m_nDragSide);
				m_ptEnd = point;
                m_bDragging = true;

				point -= m_sizeDelta;

				OBJLIST::iterator i;
                OBJLIST listSelected = m_drawingObjects.GetSelectedItems();
                wxPoint ref = m_pLastSelectedItem->GetOrigRect().TopLeft();
			    for (i =  listSelected.begin(); i != listSelected.end(); ++i)
				{
					CBaseDrawingObject* obj = *i;
					if( obj  &&  obj->IsMovable( ) )
                    {
                        wxPoint pt = event.GetPosition( );
                        UnscaleThePoint( pt );
                        pt -= m_sizeDelta;

                        wxPoint offset( 0, 0 );
                        CalcScrolledPosition( offset );

                        if( m_pLastSelectedItem  !=  obj )
                        {
                            wxPoint pt1 = m_pLastSelectedItem->GetOrigRect( ).TopLeft( );
                            wxPoint pt2 = obj->GetOrigRect( ).TopLeft( );
                            pt1 -= pt2;
                            offset += pt1;
                        }
						obj->Drag( (wxDC *) &dc, pt, offset, m_dblZoom );
					}
				}
			}
			break;
		}
		case MA_MULTI_SELECTION:
        {
//            wxLogDebug( wxT("::OnMotion( )  --  MULTI SELECT  -  m_bDragging = %d"), m_bDragging );
//            wxLogDebug( wxT("      m_ptStart = (%d, %d)  --  oldEnd = (%d, %d)  --  newEnd = (%d, %d)"),
//                        m_ptStart.x, m_ptStart.y, m_ptEnd.x, m_ptEnd.y, point.x, point.y );
            IICRect rect(m_ptStart.x, m_ptStart.y, m_ptEnd.x, m_ptEnd.y);
            rect.NormalizeRect();

            wxColour    black( 0, 0, 0 );
            wxPen   newPen( black, 1, wxDOT );
            wxPen   oldPen = dc.GetPen( );
            dc.SetPen( newPen );

			if(m_bDragging)
			{
				if (rect.left!=rect.right || rect.top!=rect.bottom)
				{
                    int iLogical = dc.GetLogicalFunction( );
                    dc.SetLogicalFunction( wxEQUIV );
                    CalcUnscrolledPosition( rect );
                    ScaleTheRect( rect );
                    dc.DrawLine( rect.left, rect.top, rect.right, rect.top );
                    dc.DrawLine( rect.right, rect.top, rect.right, rect.bottom );
                    dc.DrawLine( rect.right, rect.bottom, rect.left, rect.bottom );
                    dc.DrawLine( rect.left, rect.bottom, rect.left, rect.top );
                    dc.SetLogicalFunction( iLogical );
                }
			}
			m_bDragging = true;
//            wxLogDebug( wxT("    -- in case MA_MULTI_SELECTION - setting m_bDragging to TRUE") );
			m_ptEnd = point;
			rect = IICRect(m_ptStart.x, m_ptStart.y, m_ptEnd.x, m_ptEnd.y);
			rect.NormalizeRect();
			if (rect.left!=rect.right || rect.top!=rect.bottom)
			{
                int iLogical = dc.GetLogicalFunction( );
                dc.SetLogicalFunction( wxEQUIV );
                CalcUnscrolledPosition( rect );
                ScaleTheRect( rect );
                dc.DrawLine( rect.left, rect.top, rect.right, rect.top );
                dc.DrawLine( rect.right, rect.top, rect.right, rect.bottom );
                dc.DrawLine( rect.right, rect.bottom, rect.left, rect.bottom );
                dc.DrawLine( rect.left, rect.bottom, rect.left, rect.top );
                dc.SetLogicalFunction( iLogical );
            }
            dc.SetPen( oldPen );
			break;
		}
		case MA_RESIZING:
		{
			SetCursor( m_nDragSide );
            if( m_ptEnd != point  &&  m_pLastSelectedItem != NULL )
			{
                wxPoint offset( 0, 0 );
                CalcScrolledPosition( offset );

				m_ptEnd = point;
				m_bDragging = true;
//                wxLogDebug( wxT("in DocShareView::OnMotion( ) - case MA_RESIZING - setting m_bDragging to TRUE") );
                if( m_bCreating  &&  m_pLastSelectedItem->IsCurveLine( ) )
				{
                    ((CCurveLine*)m_pLastSelectedItem)->AddPoint((point-offset), FALSE);

                    IICRect rc(m_ptEnd, point);
                    rc.NormalizeRect();
                    rc -= offset;
					InvalidateSelection( rc, FALSE );
				}
				else
				{
                    m_pLastSelectedItem->Resize( (wxDC *) &dc, point, m_nDragSide, offset, m_dblZoom );
				}
			}
			break;
		}
		case MA_NONE:
		{
//            wxLogDebug( wxT("::OnMotion( )  --  NONE") );
			CBaseDrawingObject * pHitTest = NULL;

            CalcUnscrolledPosition( point );
			int     flags   = -1;
			pHitTest = m_drawingObjects.HitTest( point, flags );

            if( m_strObjType.IsEmpty( ) )
            {
                if (pHitTest )
                {
                    if (pHitTest->IsMovable() || flags != NO_SIDE)
                    {
                        SetCursor(flags);
                    }
                }
                else
                {
                    m_pCanvas->SetCursor( wxCursor( wxCURSOR_ARROW ) );
                }
            }
			break;
		}
	}
   
}


//*****************************************************************************
void DocShareView::OnRButtonDown( wxMouseEvent& event )
{
    if( !IsPresenter( )  &&  !m_bRemoteControl )
        return;

    if( !CanEdit( ) || m_bRemotePointer )
        return;

    wxCommandEvent  cmdEvent;
    OnDocshareSelect( cmdEvent );

    wxClientDC  dc( m_pCanvas );
    m_pCanvas->DoPrepareDC( dc );
    CUtils::SetFocus(m_pCanvas);

    HideEditWindow( );

    wxPoint _point = event.GetPosition( );
    UnscaleThePoint( _point );

    CalcUnscrolledPosition( _point );

    m_ptPasteOffset = _point;	// remember this position for paste
    m_drawingObjects.ResetPasteOffset( );

    int                 nDragSide = NO_SIDE;
    CBaseDrawingObject  *pHitTest = m_drawingObjects.HitTest( _point, nDragSide );
    if( pHitTest == NULL  ||  !pHitTest->IsSelected( ) )
    {
        if( m_drawingObjects.GetSelCount( )  >  0 )	// unselect previous selection
        {
            IICRect     rect( 0, 0, 0, 0 );
            m_drawingObjects.UnSelect( rect );
            m_pLastSelectedItem = NULL;
            InvalidateSelection( rect );
        }

        if( pHitTest )	// select the new item
        {
            m_drawingObjects.SelectItem( pHitTest );
            DrawItem( dc, pHitTest );
            m_pLastSelectedItem = pHitTest;
            m_bSelected = false;
        }
    }

    if( m_pcontextMenuLineSize  ==  NULL )
    {
        m_pcontextMenuLineSize = new wxMenu( );
        m_pcontextMenuLineSize->Append( XRCID("IDC_DOCSHARE_CONTMENU_LINESIZE1"),  lineSizes[ 0 ].desc );
        m_pcontextMenuLineSize->Append( XRCID("IDC_DOCSHARE_CONTMENU_LINESIZE2"),  lineSizes[ 1 ].desc );
        m_pcontextMenuLineSize->Append( XRCID("IDC_DOCSHARE_CONTMENU_LINESIZE4"),  lineSizes[ 2 ].desc );
        m_pcontextMenuLineSize->Append( XRCID("IDC_DOCSHARE_CONTMENU_LINESIZE6"),  lineSizes[ 3 ].desc );
        m_pcontextMenuLineSize->Append( XRCID("IDC_DOCSHARE_CONTMENU_LINESIZE12"), lineSizes[ 4 ].desc );
        m_pcontextMenuLineSize->Append( XRCID("IDC_DOCSHARE_CONTMENU_LINESIZE24"), lineSizes[ 5 ].desc );
        m_pcontextMenuLineSize->Append( XRCID("IDC_DOCSHARE_CONTMENU_LINESIZE36"), lineSizes[ 6 ].desc );
        m_pcontextMenuLineSize->Append( XRCID("IDC_DOCSHARE_CONTMENU_LINESIZE48"), lineSizes[ 7 ].desc );

        m_pcontextMenuLineStyle = new wxMenu( );
        m_pcontextMenuLineStyle->Append( XRCID("IDC_DOCSHARE_CONTMENU_LINESTYLE_SOLID"),   lineStyles[ 0 ].desc );
        m_pcontextMenuLineStyle->Append( XRCID("IDC_DOCSHARE_CONTMENU_LINESTYLE_DASH"),    lineStyles[ 1 ].desc );
        m_pcontextMenuLineStyle->Append( XRCID("IDC_DOCSHARE_CONTMENU_LINESTYLE_DOT"),     lineStyles[ 2 ].desc );
        m_pcontextMenuLineStyle->Append( XRCID("IDC_DOCSHARE_CONTMENU_LINESTYLE_DASHDOT"), lineStyles[ 3 ].desc );
        m_pcontextMenuLineStyle->Append( XRCID("IDC_DOCSHARE_CONTMENU_LINESTYLE_NULL"),    lineStyles[ 4 ].desc );

        wxMenuItem  *pMI;
        m_pcontextMenu = new wxMenu( );
        m_pcontextMenu->Append( XRCID("IDC_DOCSHARE_CONTMENU_NEWDOC"),  _("New Document") );
        pMI = m_pcontextMenu->Append( XRCID("IDC_DOCSHARE_CONTMENU_OPENDOC"), _("Open Document") );
        pMI->Enable( false );
        m_pcontextMenu->Append( XRCID("IDC_DOCSHARE_CONTMENU_SAVEDOC"), _("Save Changes") );
        pMI = m_pcontextMenu->Append( XRCID("IDC_DOCSHARE_CONTMENU_SAVEAS"),  _("Save As") );
#ifdef SAVEASSVG
        pMI->Enable( true );
#else
        pMI->Enable( false );
#endif
        m_pcontextMenu->AppendSeparator( );
        pMI = m_pcontextMenu->Append( XRCID("IDC_DOCSHARE_CONTMENU_UPLOADDOC"), _("Upload Document") );
        pMI->Enable( false );
        m_pcontextMenu->Append( XRCID("IDC_DOCSHARE_CONTMENU_ORGANIZE"),  _("Organize Pages") );
        m_pcontextMenu->Append( XRCID("IDC_DOCSHARE_CONTMENU_INSERT"),    _("Insert New Page") );
        m_pcontextMenu->AppendSeparator( );
        pMI = m_pcontextMenu->Append( XRCID("IDC_DOCSHARE_CONTMENU_SAVEASIMAGE"),  _("Save As Image") );
        pMI->Enable( false );
        pMI = m_pcontextMenu->Append( XRCID("IDC_DOCSHARE_CONTMENU_COPY"),         _("Copy to Clipboard") );
        pMI->Enable( false );
        m_pcontextMenu->Append( XRCID("IDC_DOCSHARE_CONTMENU_PASTE"),        _("Paste") );
        pMI = m_pcontextMenu->Append( XRCID("IDC_DOCSHARE_CONTMENU_PASTESPECIAL"), _("Paste Special") );
        pMI->Enable( false );

        m_pcontextMenuObj = new wxMenu( );
        m_pcontextMenuObj->Append( XRCID("IDC_DOCSHARE_CONTMENUOBJ_CUT"),          _("Cut") );
        m_pcontextMenuObj->Append( XRCID("IDC_DOCSHARE_CONTMENUOBJ_COPY"),         _("Copy") );
        m_pcontextMenuObj->Append( XRCID("IDC_DOCSHARE_CONTMENUOBJ_PASTE"),        _("Paste") );
        pMI = m_pcontextMenuObj->Append( XRCID("IDC_DOCSHARE_CONTMENUOBJ_PASTESPECIAL"), _("Paste Special") );
        pMI->Enable( false );
        m_pcontextMenuObj->Append( XRCID("IDC_DOCSHARE_CONTMENUOBJ_DELETE"),       _("Delete") );
        m_pcontextMenuObj->AppendSeparator( );
        m_pcontextMenuObj->Append( XRCID("IDC_DOCSHARE_CONTMENUOBJ_TOFRONT"),         _("Bring To Front") );
        m_pcontextMenuObj->Append( XRCID("IDC_DOCSHARE_CONTMENUOBJ_TOBACK"),          _("Send Back") );
        pMI = m_pmenuItemLineSize  = m_pcontextMenuObj->AppendSubMenu( m_pcontextMenuLineSize,  _("Line Size") );
        pMI = m_pmenuItemLineStyle = m_pcontextMenuObj->AppendSubMenu( m_pcontextMenuLineStyle, _("Line Style") );
        m_pcontextMenuObj->Append( XRCID("IDC_DOCSHARE_CONTMENUOBJ_FONT"),            _("Font...") );
        m_pcontextMenuObj->AppendSeparator( );
        m_pcontextMenuObj->Append( XRCID("IDC_DOCSHARE_CONTMENUOBJ_LINECOLOR"),       _("Line Color...") );
        m_pcontextMenuObj->Append( XRCID("IDC_DOCSHARE_CONTMENUOBJ_BACKCOLOR"),       _("Background Color...") );
        m_pcontextMenuObj->Append( XRCID("IDC_DOCSHARE_CONTMENUOBJ_TEXTCOLOR"),       _("Text Color...") );
        m_pcontextMenuObj->AppendSeparator( );
        m_pcontextMenuObj->Append( XRCID("IDC_DOCSHARE_CONTMENUOBJ_TOGTRANS"),        _("Toggle Transparency") );


        m_pcontextMenuObj->Connect( XRCID("IDC_DOCSHARE_CONTMENU_LINESIZE1"), XRCID("IDC_DOCSHARE_CONTMENU_LINESIZE48"),
                                         wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnLineThicknessSelected), 0, this );
        m_pcontextMenuLineSize->Connect( XRCID("IDC_DOCSHARE_CONTMENU_LINESIZE1"), XRCID("IDC_DOCSHARE_CONTMENU_LINESIZE48"),
                                    wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnLineThicknessSelected), 0, this );

        m_pcontextMenuObj->Connect( XRCID("IDC_DOCSHARE_CONTMENU_LINESTYLE_SOLID"), XRCID("IDC_DOCSHARE_CONTMENU_LINESTYLE_NULL"),
                                          wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnLineStyleSelected), 0, this );


        m_pcontextMenu->Connect( XRCID("IDC_DOCSHARE_CONTMENU_NEWDOC"),       wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocshareNew),           0, this );
        m_pcontextMenu->Connect( XRCID("IDC_DOCSHARE_CONTMENU_OPENDOC"),      wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocshareOpen),          0, this );
        m_pcontextMenu->Connect( XRCID("IDC_DOCSHARE_CONTMENU_SAVEDOC"),      wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocshareSave),          0, this );
        m_pcontextMenu->Connect( XRCID("IDC_DOCSHARE_CONTMENU_SAVEAS"),       wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocshareSaveAs),        0, this );
        m_pcontextMenu->Connect( XRCID("IDC_DOCSHARE_CONTMENU_UPLOADDOC"),    wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocshareUploadDoc),     0, this );
        m_pcontextMenu->Connect( XRCID("IDC_DOCSHARE_CONTMENU_ORGANIZE"),     wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocshareOrganizePages), 0, this );
        m_pcontextMenu->Connect( XRCID("IDC_DOCSHARE_CONTMENU_INSERT"),       wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocshareInsertPage),    0, this );
//        m_pcontextMenuObj->Connect( XRCID("IDC_DOCSHARE_CONTMENU_SAVEASIMAGE"),  wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocshareSaveAsImage),   0, this );
        m_pcontextMenu->Connect( XRCID("IDC_DOCSHARE_CONTMENU_COPY"),         wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocshareCopy),          0, this );
        m_pcontextMenu->Connect( XRCID("IDC_DOCSHARE_CONTMENU_PASTE"),        wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocsharePaste),         0, this );
//        m_pcontextMenuObj->Connect( XRCID("IDC_DOCSHARE_CONTMENU_PASTESPECIAL"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocsharePasteSpecial),  0, this );


        m_pcontextMenuObj->Connect( XRCID("IDC_DOCSHARE_CONTMENUOBJ_CUT"),          wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocshareCut),          0, this );
        m_pcontextMenuObj->Connect( XRCID("IDC_DOCSHARE_CONTMENUOBJ_COPY"),         wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocshareCopy),         0, this );
        m_pcontextMenuObj->Connect( XRCID("IDC_DOCSHARE_CONTMENUOBJ_PASTE"),        wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocsharePaste),        0, this );
//        m_pcontextMenuObj->Connect( XRCID("IDC_DOCSHARE_CONTMENUOBJ_PASTESPECIAL"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocsharePasteSpecial), 0, this );
        m_pcontextMenuObj->Connect( XRCID("IDC_DOCSHARE_CONTMENUOBJ_DELETE"),       wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocshareDeleteSel),    0, this );
        m_pcontextMenuObj->Connect( XRCID("IDC_DOCSHARE_CONTMENUOBJ_TOFRONT"),      wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocshareToFront),      0, this );
        m_pcontextMenuObj->Connect( XRCID("IDC_DOCSHARE_CONTMENUOBJ_TOBACK"),       wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocshareToBack),       0, this );
        m_pcontextMenuObj->Connect( XRCID("IDC_DOCSHARE_CONTMENUOBJ_FONT"),         wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocshareFontSettings), 0, this );
        m_pcontextMenuObj->Connect( XRCID("IDC_DOCSHARE_CONTMENUOBJ_LINECOLOR"),    wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocshareLineColor),    0, this );
        m_pcontextMenuObj->Connect( XRCID("IDC_DOCSHARE_CONTMENUOBJ_BACKCOLOR"),    wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocshareBgColor),      0, this );
        m_pcontextMenuObj->Connect( XRCID("IDC_DOCSHARE_CONTMENUOBJ_TEXTCOLOR"),    wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocshareTextColor),    0, this );
        m_pcontextMenuObj->Connect( XRCID("IDC_DOCSHARE_CONTMENUOBJ_TOGTRANS"),     wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DocShareView::OnDocshareTogTrans),     0, this );
    }

    _point = event.GetPosition( );
    
    if( m_drawingObjects.GetSelCount( )  > 0 )
    {
        m_pCanvas->PopupMenu( m_pcontextMenuObj, _point.x, _point.y );
    }
    else
    {
        m_pCanvas->PopupMenu( m_pcontextMenu, _point.x, _point.y );
    }
}


//*****************************************************************************
void DocShareView::OnCaptureLost( wxMouseCaptureLostEvent& event )
{
    wxLogDebug( wxT("entering DocShareView::OnCaptureLost( )") );
    m_bDragging = false;
    m_nAction = MA_NONE;
    m_sizeDelta = wxSize(0,0);
}


//*****************************************************************************
//*****************************************************************************
//
//  Miscellaneous functions
//
//*****************************************************************************
//*****************************************************************************
void DocShareView::SetPageDirty(bool bDirty)
{
    m_bDirty = bDirty;

    if ( m_pMMF )
    {
        wxString    title;
        wxString    strStar( _("*"), wxConvUTF8 );
        title = m_pMMF->GetTitle( );
        if( bDirty )
        {
            if( title.Len( ) > 0  &&  !title.EndsWith( strStar ) )
            {
                title.Append( strStar );
                m_pMMF->SetTitle( title );
            }
        }
        else
        {
            wxString    strRest;
            if( title.EndsWith( wxT("*"), &strRest ) )
            {
                m_pMMF->SetTitle( strRest );
            }
        }
    }
}


//*****************************************************************************
void DocShareView::InvalidateSelection( bool bEraseBkgnd )
{
    IICRect     rect;
    m_drawingObjects.GetSelectedRegion( rect );
    InvalidateSelection( rect, bEraseBkgnd );
}


//*****************************************************************************
void DocShareView::InvalidateSelection( IICRect& rect, bool bEraseBkgnd )
{
    wxPoint offset( 0, 0 );
    CalcScrolledPosition( offset );
    rect += offset;

    rect.InflateRect( kMAX_LINESIZE/2, kMAX_LINESIZE/2 );
    ScaleTheRect( rect );
    m_pCanvas->RefreshRect( wxRect( rect.left, rect.top, rect.Width( ), rect.Height( )), bEraseBkgnd );
}


//*****************************************************************************
void DocShareView::SetCursor( int Side )
{
    switch (Side)
    {
        case LEFT_SIDE:
        case RIGHT_SIDE:
            m_pCanvas->SetCursor( m_CursorLeftright );
            break;

        case TOP_SIDE:
        case BOTTOM_SIDE:
            m_pCanvas->SetCursor( m_CursorVert );
            break;

        case BOTTOMRIGHT_SIDE:
        case TOPLEFT_SIDE:
            m_pCanvas->SetCursor( m_CursorNWSE );
            break;

        case BOTTOMLEFT_SIDE:
        case TOPRIGHT_SIDE:
            m_pCanvas->SetCursor( m_CursorNESW );
            break;

        case LINE_START:
        case LINE_END:
            m_pCanvas->SetCursor( m_CursorCross );
            break;

        default:
            m_pCanvas->SetCursor( m_CursorMove );
            break;
    }
}


//*****************************************************************************
wxSize DocShareView::GetWhiteboardSize( wxClientDC& pDC )
{
    int     w = 1024;
    int     h = 768;

    //  Only needed for Document Sharing
    if (m_pImage)
    {
        if( m_pImage->GetWidth( ) > w)
            w = m_pImage->GetWidth( );

        if (m_pImage->GetHeight( ) > h)
            h = m_pImage->GetHeight( );
    }

    wxSize size = m_drawingObjects.GetExtent( pDC );

    if( (int)w < size.x )
        w = size.x;
    if( (int)h < size.y )
        h = size.y;

    return wxSize(w, h);
}


//*****************************************************************************
bool DocShareView::CanEdit( )
{
//    return ((IsPresenter() && m_bFileLocked) ||
//            (m_pDocument!=NULL && m_pDocument->m_Meeting.IsUserRemoteController()) ||
//            IsModerator()) && !m_fReadOnly && !m_dialogProgress.m_hWnd;
    return IsPresenter() || m_bRemoteControl;
}


//*****************************************************************************
void DocShareView::DrawItem( wxDC& dc, CBaseDrawingObject *pBox, bool bEraseBkgnd )
{
    IICRect rc(0,0,0,0);
    rc = pBox->UnionRect( rc );
    InvalidateSelection( rc, bEraseBkgnd );
}


//*****************************************************************************
void DocShareView::OnDraw( wxDC* pDC )
{
    int     iw, ih;
    
    m_pCanvas->GetClientSize( &iw, &ih );

    if( m_pImage )
    {
        //  We always need to calc and set the scaling factor
        double  inflation   = (m_fOpenedAltImage) ? m_saPages.at( m_nPageIndex ).m_dblAltScale : m_saPages.at( m_nPageIndex ).m_dblScale;
        int     imgH        = m_pImage->GetHeight( );
        int     imgW        = m_pImage->GetWidth( );
        double  xscale      = (double) iw / (double)imgW;
        double  yscale      = (double) ih / (double)imgH;
        if( yscale  <  xscale )
        {
            m_dblImageZoom = yscale * inflation;
            iw = (int) ((((double) imgW) * yscale) / m_dblImageZoom);
            ih = (int) ((((double) imgH) * yscale) / m_dblImageZoom);
        }
        else
        {
            m_dblImageZoom = xscale * inflation;
            ih = (int) ((((double) imgH) * xscale) / m_dblImageZoom);
            iw = (int) ((((double) imgW) * xscale) / m_dblImageZoom);
        }

        if( m_fForceScroll ) {
            m_dblImageZoom = 1.0;
        }
        else if( m_fFitToWidth ) {
            m_dblImageZoom = xscale * inflation;
        }

        //  Force scaling to be a multiple of 0.05, rounding down
        //i = ((int)(m_dblImageZoom * 20.0));
        //m_dblImageZoom = ((double) i) / 20.0;

        //  Size scrolling window so entire document is available
        int     iUnitsX, iUnitsY;
        iUnitsX = (int) (( (iw * m_dblImageZoom) + kDocSharePixelsPerUnitX - 1) / kDocSharePixelsPerUnitX);
        iUnitsY = (int) (( (ih * m_dblImageZoom) + kDocSharePixelsPerUnitY - 1) / kDocSharePixelsPerUnitY);
        SetScrollbars( iUnitsX, iUnitsY );

        // Empirically determined ratio between image size and drawing coordinates
        m_dblZoom = m_dblImageZoom * 1.2;

        wxLogDebug(wxT("DocShareView zoom = %.3f, image size = %d, %d"), m_dblZoom, iw, ih);
    }
#ifdef METAFILE
    else if( m_pMetafile )
    {
        // The metafile size is computed using the metafile info and the system resolution (DPI), whereas we want to 
        // perform calculations that will be the same for all clients.  So, just assume a fixed size for the metafile, and later
        // draw it using the same size.
        double  inflation   = (m_fOpenedAltImage) ? m_saPages.at( m_nPageIndex ).m_dblAltScale : m_saPages.at( m_nPageIndex ).m_dblScale;
        int     imgH        = 600; //m_pMetafile->GetHeight( );
        int     imgW        = 800; //m_pMetafile->GetWidth( );
        double  xscale      = (double) iw / (double)imgW;
        double  yscale      = (double) ih / (double)imgH;
        if( yscale  <  xscale )
        {
            m_dblImageZoom = yscale * inflation;
            iw = (int) ((((double) imgW) * yscale) / m_dblImageZoom);
            ih = (int) ((((double) imgH) * yscale) / m_dblImageZoom);
        }
        else
        {
            m_dblImageZoom = xscale * inflation;
            ih = (int) ((((double) imgH) * xscale) / m_dblImageZoom);
            iw = (int) ((((double) imgW) * xscale) / m_dblImageZoom);
        }

        if( m_fForceScroll ) 
        {
            m_dblImageZoom = 1.0;
        }
        else if( m_fFitToWidth ) 
        {
            m_dblImageZoom = xscale * inflation;
        }

        //  Size scrolling window so entire document is available
        int     iUnitsX, iUnitsY;
        iUnitsX = (int) (( (iw * m_dblImageZoom) ) / kDocSharePixelsPerUnitX);
        iUnitsY = (int) (( (ih * m_dblImageZoom) ) / kDocSharePixelsPerUnitY);
        SetScrollbars( iUnitsX, iUnitsY );

        m_dblZoom = m_dblImageZoom;

        wxLogDebug(wxT("DocShareView zoom = %.3f, metafile size = %d, %d, scaled size = %d, %d"), m_dblZoom, imgW, imgH, iw, ih);
    }
#endif
    else
    {
        m_dblZoom = m_dblImageZoom = 1.0;
        wxLogDebug(wxT("DocShareView zoom = %.3f"), m_dblZoom);
    }

    IICRect rcClientRect( 0, 0, iw, ih );
    InternalPaint( pDC, rcClientRect, 0, 0 );
}

void EraseOutsideRect(wxDC* pDC, int width, int height) 
{
    wxBrush backg(pDC->GetBackground());
    pDC->SetBrush(backg);
    pDC->SetPen(*wxTRANSPARENT_PEN);
    pDC->DrawRectangle(width,0,width,height);
    pDC->DrawRectangle(0,height,width,height);
}


//*****************************************************************************
// DocShareView drawing
void DocShareView::InternalPaint( wxDC* pDC, IICRect& rcClientRect, int x, int y )
{
    //  Only needed for Document Sharing
    if( m_pImage  &&  m_pImage->GetWidth( ) > 0 )
    {
        int width = m_pImage->GetWidth( ) * m_dblImageZoom;
        int height = m_pImage->GetHeight( ) * m_dblImageZoom;

        // Erase area outside of bitmap 
        EraseOutsideRect(pDC, width, height);

        // And then draw the bitmap.
#ifdef WIN32
        // Not sure why the passed in DC doesn't work...but it doesn't.
        // We are called from a paint function so this should be safe.
        wxPaintDC pdc( m_pCanvas );
        wxGraphicsContext * gc = wxGraphicsContext::Create( pdc );
        gc->DrawBitmap( *m_pImage, 0, 0, width, height );
        delete gc;
#else
        pDC->SetUserScale( m_dblImageZoom, m_dblImageZoom );
        pDC->DrawBitmap( *m_pImage, 0, 0, false );
#endif
    }
#ifdef METAFILE
    else if( m_pMetafile  &&  m_pMetafile->GetWidth( ) > 0 )
    {
        pDC->SetUserScale( m_dblImageZoom, m_dblImageZoom );
        int width = 800, height = 600;
        EraseOutsideRect(pDC, width, height);
        wxRect rect(0,0,width,height);
        m_pMetafile->Play( pDC, &rect );
    }
#endif
    else {
        pDC->Clear();
    }

    pDC->SetUserScale( m_dblZoom, m_dblZoom );        
    wxPoint     pt(0, 0);
    m_drawingObjects.Paint( pDC, rcClientRect, pt );
}


//*****************************************************************************
void DocShareView::HideEditWindow( )
{
    if( m_pLastSelectedItem &&
          m_pLastSelectedItem->AcceptText() && m_pCtlEdit->IsShown( ) )
    {
        CChangeAction* action = new CChangeAction( m_pDocument );
        action->PreAction( m_drawingObjects );

        wxString    txt, txtOrig;
        txt = m_pCtlEdit->GetValue( );
        m_pLastSelectedItem->GetText( txtOrig );
        if( txt  !=  txtOrig )
        {
            m_pLastSelectedItem->SetText( txt );

            action->PostAction( m_drawingObjects );
            m_drawingObjects.AddAction( action );

            SetPageDirty( true );
        }
        else
        {
            delete action;
        }
    }
    if( m_pCtlEdit )
        m_pCtlEdit->Hide( );
}


//*****************************************************************************
void DocShareView::RemoteControl( bool fRC )
{
    m_bRemoteControl = fRC;
}


//*****************************************************************************
bool DocShareView::IsPresenter( )
{
    return m_bPresenter;
}


//*****************************************************************************
bool DocShareView::IsModerator( )
{
//    return m_pDocument!=NULL && (m_pDocument->m_Meeting.IsUserModerator() || m_pDocument->IsAuthUserAdmin());
    return true;
}


//*****************************************************************************
bool DocShareView::CreateFont( const wxString& strFontFace, int iFontSize, const wxString& strFontStyle, wxFontData &pLogFont )
{
    if( strFontFace.IsEmpty( ) )	// use default font
        return false;

    wxFont  newFont = pLogFont.GetChosenFont( );
    newFont.SetFaceName( strFontFace );
    newFont.SetPointSize( iFontSize );

    if( strFontStyle.Find( wxT("bold|") ) != wxNOT_FOUND )
        newFont.SetWeight( wxFONTWEIGHT_BOLD );
    if( strFontStyle.Find( wxT("italic")) != wxNOT_FOUND )
        newFont.SetStyle( wxFONTSTYLE_ITALIC );
    if( strFontStyle.Find( wxT("underline")) != wxNOT_FOUND )
        newFont.SetUnderlined( true );

    return true;
}



//*****************************************************************************
//*****************************************************************************
//
//  Document handling functions
//
//*****************************************************************************
//*****************************************************************************
bool DocShareView::LoadDocument( const wxString& aDocId )
{
//    CWaitCursor wait;

    wxString    errmsg( _("Failed to load document from server, please try again later."), wxConvUTF8 );
    std::string xml;
    wxString    strStatusCode;
    wxString    strStatusText;
    bool        rc          = true;
    bool        indexFileFound  = false;

    wxString    strT( _("Loading"), wxConvUTF8 );
    wxString    strT2( _("Loading document...please wait."), wxConvUTF8 );
    ShowProgressWindow( strT, strT2, true );

    wxString    strEncodedDocID( aDocId );
    CUtils::EncodeStringFully( strEncodedDocID );

    // Keep progress dialog open during all async operations
    AllowHideProgress( false );

    while( true )	// just for breakable
    {
        // Fetch index file
        long hr = AsyncRetrieveFile( strEncodedDocID, _T("index.xml"), xml, strStatusCode, strStatusText, false);
        if (SUCCEEDED(hr) && strStatusCode == _T("200"))
        {
            iksparser   *prs;
            iks         *x      = NULL;
            iks         *y      = NULL;
            prs = iks_dom_new( &x );
            if( prs )
            {
                iks_parse(prs, xml.c_str( ), xml.length(), 1);
                if (x)
                {
                    // we should have got the index file at this poiint,
                    // cleanup the current document
                    m_drawingObjects.DeleteAllItems();
                    m_pLastSelectedItem = NULL;
                    m_saPages.clear( );
                    m_nPageIndex = 0;
                    m_strDocId = aDocId;

                    y = iks_child( x );
                    while( y )
                    {
                        if( iks_type(y) == IKS_TAG  &&  stricmp( iks_name(y), "page" ) == 0 )
                        {
                            CDocPage page;
                            page.m_strTitle = wxString( iks_find_attrib( y, "title" ), wxConvUTF8 );
                            page.m_strFile  = wxString( iks_find_attrib( y, "file" ), wxConvUTF8 );
                            page.m_strAltFile  = wxString( iks_find_attrib( y, "altfile" ), wxConvUTF8 );

                            wxString    strScale( iks_find_attrib( y, "scale" ), wxConvUTF8 );
                            page.m_dblScale = 1.0;
                            if( strScale.ToDouble( &page.m_dblScale ) )
                            {
                                if( page.m_dblScale < 1.0  )
                                    page.m_dblScale = 1.0;
                                else if( page.m_dblScale > 4.0 )
                                    page.m_dblScale = 4.0;
                            }

                            wxString    strAltScale( iks_find_attrib( y, "altscale" ), wxConvUTF8 );
                            if( strAltScale.ToDouble( &page.m_dblAltScale ) )
                            {
                                if( page.m_dblAltScale < 1.0  )
                                    page.m_dblAltScale = 1.0;
                                else if( page.m_dblAltScale > 4.0 )
                                    page.m_dblAltScale = 4.0;
                                else if( page.m_dblAltScale == 4.0 )   // Windows version was buggy and always set alt scale to 4
                                    page.m_dblAltScale = 1.0;
                            }

                            page.m_fFitToWidth = false;
                            wxString    strFitToWidth( iks_find_attrib( y, "fittowidth" ), wxConvUTF8 );
                            strFitToWidth.Trim( false );        //  Remove any leading whitespace
                            strFitToWidth.MakeUpper( );
                            strFitToWidth = strFitToWidth.Left( 1 );
                            if(( strFitToWidth.Cmp( wxT('T') ) == 0 )  ||  ( strFitToWidth.Cmp( wxT('1') ) == 0 ))
                                page.m_fFitToWidth = true;

                            m_saPages.push_back( page );

                            indexFileFound = TRUE;
                        }
                        y = iks_next( y );
                    }
                }
                else
                {
                    CUtils::DisplayMessage( m_pCanvas, CUtils::IDS_IIC_ERROR, CUtils::IDS_IIC_INDEX_PARSE_ERROR, wxOK | wxICON_EXCLAMATION );
                    iks_parser_delete( prs );
                    rc = false;
                    break;
                }
                iks_parser_delete( prs );
            }
            else	// if (prs)
            {
                CUtils::DisplayMessage( m_pCanvas, CUtils::IDS_IIC_ERROR, CUtils::IDS_DOCSHARE_NOPARSER, wxOK | wxICON_EXCLAMATION );
                rc = false;
                break;
            }
        }
#ifndef __WXMSW__
        else if( (hr & 0xFFFF) == ERROR_INTERNET_INVALID_CA )
        {
            if (m_WebDavUtils.HandleCertError( m_pCanvas ))
                continue;
            rc = false;
            break;
        }
#endif
        else if( strStatusCode == _T("404") )	// (!indexFileFound)
        {
            // Create new whiteboard
            wxString    strTargetFileURL( m_pDocument->m_Meeting.GetDocShareServer() );
            strTargetFileURL += m_strMeetingID;
            strTargetFileURL += _T("/");
            wxString strTargetFilePath( m_pDocument->m_Meeting.GetDocSharePath() );
            strTargetFilePath += m_strMeetingID;
            strTargetFilePath += _T("/");

            hr = AsyncSendRequest(_T("MKCOL"), strTargetFileURL, strTargetFilePath, NULL, 0,
                                  strStatusCode, strStatusText, false, m_strLockToken);
            if( FAILED(hr) )
            {
                long    lStatus     = 0;
                strStatusCode.ToLong( &lStatus );
                CUtils::ReportDAVError( m_pCanvas, lStatus, strStatusText,
                                       _T("%s\n%s\n%s"), errmsg, hr);
                rc = false;
                break;
            }
            strTargetFileURL += aDocId;
            strTargetFileURL += _T("/");
            strTargetFilePath += strEncodedDocID;
            strTargetFilePath += _T("/");

            // Create directory
            hr = AsyncSendRequest( _T("MKCOL"), strTargetFileURL, strTargetFilePath, NULL, 0,
                                  strStatusCode, strStatusText, false, m_strLockToken);
            if( FAILED(hr) )
            {
                long    lStatus     = 0;
                strStatusCode.ToLong( &lStatus );
                CUtils::ReportDAVError( m_pCanvas, lStatus, strStatusText,
                                       _T("%s\n%s\n%s"), errmsg, hr);
                rc = false;
                break;
            }

            // Create index file with blank page
            wxString	indexFileUrl  = strTargetFileURL  + _T("index.xml");
            wxString	indexFilePath = strTargetFilePath + _T("index.xml");
            std::string indexFile;
            indexFile = "<index><page title=\"Page1\" file=\"Page1\"/></index>";
            hr = AsyncSendRequest( _T("PUT"), indexFileUrl, indexFilePath, (BYTE*)indexFile.c_str(), indexFile.length(),
                                   strStatusCode, strStatusText, false, m_strLockToken);
            if( FAILED(hr) )
            {
                long    lStatus     = 0;
                strStatusCode.ToLong( &lStatus );
                CUtils::ReportDAVError( m_pCanvas, lStatus, strStatusText,
                                       _T("%s\n%s\n%s"), errmsg, hr);
                rc = false;
                break;
            }

            m_saPages.clear( );
            m_nPageIndex = 0;

            CDocPage page;
            page.m_strTitle = _T("Page1");
            page.m_strFile  = _T("Page1");
            page.m_dblScale = 1.0; 
            page.m_dblAltScale = 1.0;
            page.m_fFitToWidth = false;
            m_saPages.push_back( page );
        }
        else
        {
            long    lStatus     = 0;
            strStatusCode.ToLong( &lStatus );
            CUtils::ReportDAVError( m_pCanvas, lStatus, strStatusText,
                                    _T("%s\n%s\n%s"), errmsg, hr);

            rc = false;
            break;
        }

        // Load specified document
        m_strDocId = aDocId;
        m_nPageIndex = 0;

        if( m_nPageIndex >=0  &&  m_nPageIndex < (int)m_saPages.size() )
        {
            rc = LoadPageMarkup( aDocId, m_nPageIndex );
            if( !rc )
                break;
        }

        if( IsWhiteboard( ) )
            ForceScroll( );
        else if( AllowFitToWidth( ) )
            ForceFitToWidth( );
        else
            ForceScale( );
    
        if ( m_pMMF && !IsWhiteboard( ) )
        {
            wxString title = _("Share Document");
            title += _T( " - ");
            title += m_strDocId;
            m_pMMF->SetTitle( title );
        }

        break;
    }

    HideProgressWindow( );

    AllowHideProgress( true );
    return rc;
}


//*****************************************************************************
bool DocShareView::ShowProgressWindow( wxString aTitle, wxString aStatus, bool fReceiveMode )
{
//    wxLogDebug( wxT("entering DocShareView::ShowProgressWindow( )  --  m_pDialogProgress = 0x%8.8x"), m_pDialogProgress );
    if( m_pDialogProgress  ==  NULL )
    {
        m_pDialogProgress = new SendRecvProgressDialog( m_pMMF );

        //  Initialize dialog we just created
        if( m_pDialogProgress  !=  NULL )
        {
            m_pDialogProgress->SetProgressRange( 1 );
            m_pDialogProgress->SetProgressPos( 0 );
            m_pDialogProgress->StartAutoProgress( 500 );
            m_pDialogProgress->SetTitle( aTitle );
            m_pDialogProgress->SetLabelTitle( aStatus );
            m_pDialogProgress->SetLabelTitle2( wxT("") );
            m_pDialogProgress->SetProgressRange2( 0 );
            m_pDialogProgress->SetProgressPos2( 0 );
            m_pDialogProgress->Fit( );

            m_pDialogProgress->Raise( );
            m_pDialogProgress->Enable( );

            m_pDialogProgress->Refresh( );
            m_pDialogProgress->Update( );
            
            m_pDialogProgress->SetReceiveMode( fReceiveMode );
            m_pDialogProgress->Show( SW_SHOW );
        }
        else
        {
            wxLogDebug( wxT("m_pDialogProgress Constructor FAILED") );
            return false;
        }
    }
    else
    {
        //  Update the strings on the existing dialog
        m_pDialogProgress->SetTitle( aTitle );
        m_pDialogProgress->SetLabelTitle( aStatus );
        m_pDialogProgress->SetLabelTitle2( wxT("") );
        m_pDialogProgress->SetProgressRange2( 0 );
        m_pDialogProgress->SetProgressPos2( 0 );
        m_pDialogProgress->Fit( );
        
        m_pDialogProgress->Raise( );
        m_pDialogProgress->Enable( );
        m_pDialogProgress->Refresh( );
        m_pDialogProgress->Update( );

        m_pDialogProgress->SetReceiveMode( fReceiveMode );
        m_pDialogProgress->Show( SW_SHOW );
    }

    return true;
}


//*****************************************************************************
void DocShareView::HideProgressWindow( )
{
    if( m_pDialogProgress != NULL  &&  m_iAllowHideProgress == 0 )
    {
        m_pDialogProgress->Disable( );
        m_pDialogProgress->Hide( );
    }
}


//*****************************************************************************
long DocShareView::AsyncRetrieveFile( const wxString& aDocId, const wxString& aPageId, std::string& out,
                                         wxString& strStatusCode, wxString& strStatusText, bool aSaveToFile )
{
    RetrieveParams   *params = new RetrieveParams();

    wxString        strT( _("Receiving..."), wxConvUTF8 );
    wxString        strT2( _("Receiving document...please wait."), wxConvUTF8 );
    ShowProgressWindow( strT, strT, true );

    params->_this = this;
    params->aDocId = aDocId,
    params->aPageId = aPageId;
    params->aSaveToFile = aSaveToFile;
    params->m_bStopped = false;

    wxLongLong  start_time      = ::wxGetLocalTimeMillis( );
    RetrieveFileThread  *pRetrieveFileThread    = new RetrieveFileThread( params );
    if( pRetrieveFileThread )
    {
        if( pRetrieveFileThread->Create( )  !=  wxTHREAD_NO_ERROR )
        {
            wxLogError( wxT("Could not create Retrieve File thread!") );
        }
        else
        {
            pRetrieveFileThread->Run( );

            int     iLC = 0;
            wxLogDebug( wxT("in DocShareView::AsyncRetrieveFile( %s,   %s )  -- BEFORE LOOPING"), aDocId.wc_str( ), aPageId.wc_str( ) );

            while( !params->m_bStopped )
            {
                iLC++;
                CUtils::MsgWait( 20, 5 );
                
                // If dialog is gone, operation was canceled
                if( m_pDialogProgress == NULL )
                {
                    params->hr = E_ABORT;
                    break;
                }
                
                if( m_pDialogProgress->fCanceled( ) )
                {
                    params->hr = E_ABORT;
                    HideProgressWindow( );
                    break;
                }
            }
            
            wxLogDebug( wxT("in DocShareView::AsyncRetrieveFile( ) looped waiting for results %d times - status %s"), iLC, params->strStatusCode.c_str() );
        }
    }

    strStatusCode = params->strStatusCode;
    strStatusText = params->strStatusText;
    out = (const char *) params->out.mb_str( wxConvUTF8 );
    long result = params->hr;
    
    params->RemoveRef();
    
    HideProgressWindow( );

    return result;
}


//*****************************************************************************
RetrieveFileThread::RetrieveFileThread( RetrieveParams *pParams )
{
    m_pParams = pParams;
    m_pParams->AddRef();
}


//*****************************************************************************
void RetrieveFileThread::OnExit( )
{
}


//*****************************************************************************
void *RetrieveFileThread::Entry( )
{
    wxString        strStatusCode, strStatusText;
    RetrieveParams  *params = m_pParams;
    params->hr = params->_this->RetrieveFile( params->aDocId, params->aPageId, params->out, params->strStatusCode,
                                              params->strStatusText, params->aSaveToFile );
    params->m_bStopped = true;
    params->RemoveRef();
    return NULL;
}


//*****************************************************************************
long DocShareView::RetrieveFile( const wxString& aDocId, const wxString& aPageId, wxString& out,
                                    wxString& strStatusCode, wxString& strStatusText, bool aSaveToFile )
{
    wxString    strTargetFileURL( m_pDocument->m_Meeting.GetDocShareServer( ) );

    wxString    strTargetFilePath( m_pDocument->m_Meeting.GetDocSharePath( ) );
    strTargetFilePath += m_strMeetingID;
    strTargetFilePath += _T("/");
    strTargetFilePath += aDocId;
    strTargetFilePath += _T("/");
    strTargetFilePath += aPageId;

    return m_WebDavUtils.RetrieveFile( strTargetFileURL, strTargetFilePath, out, m_strUserName, m_strPasswd,
                                       strStatusCode, strStatusText, aSaveToFile, m_pCanvas );
}


//*****************************************************************************
long DocShareView::AsyncSendRequest( wxString szReq, const wxString& strFileURL, const wxString& strFilePath,
                                       const BYTE* pData, DWORD dwLength,
                                       wxString& strStatusCode, wxString& strStatusText,
                                       bool bAddLockToken, const wxString& strLockToken, bool fMultiFileSend )
{
//    CWaitCursor wait;

    RequestParams   *params = new RequestParams();
    
    wxString        strT( _("Sending"), wxConvUTF8 );
    wxString        strT2( _("Sending request...please wait."), wxConvUTF8 );
    bool            fDeleteNeeded   = false;

    if( !fMultiFileSend )
    {
        fDeleteNeeded = ShowProgressWindow( strT, strT2, true );
    }

    params->_this        = this;
    params->szReq        = szReq;
    params->strFileURL   = strFileURL;
    params->strFilePath  = strFilePath;
    params->pData        = pData;
    params->dwLength     = dwLength;
    params->bAddLockToken = bAddLockToken;
    params->strLockToken = strLockToken;
    params->m_bStopped   = false;
    params->hr           = 0;

    wxLongLong  start_time      = ::wxGetLocalTimeMillis( );

    SendRequestThread  *pSendRequestThread    = new SendRequestThread( params );
    if( pSendRequestThread )
    {
        if( pSendRequestThread->Create( )  !=  wxTHREAD_NO_ERROR )
        {
            wxLogError( wxT("Could not create Send Request thread!") );
        }
        else
        {
            pSendRequestThread->Run( );

            int     iLC = 0;
            wxLogDebug( wxT("in DocShareView::AsyncSendRequest( )  -- BEFORE LOOPING") );

            while( !params->m_bStopped )
            {
                iLC++;
                CUtils::MsgWait( 20, 5 );

                if( m_pDialogProgress == NULL)
                {
                    params->hr = E_ABORT;
                    break;
                }

                if( m_pDialogProgress->fCanceled( ) )
                {
                    params->hr = E_ABORT;
                    HideProgressWindow( );
                    break;
                }
            }

            wxLogDebug( wxT("in DocShareView::AsyncSendRequest( ) looped waiting for results %d times - status %s"), iLC, params->strStatusCode.c_str() );
        }
    }

    long result = params->hr;
    strStatusCode = params->strStatusCode;
    strStatusText = params->strStatusText;

    params->RemoveRef();

    if( fDeleteNeeded  &&  !fMultiFileSend )
    {
        CUtils::MsgWait( 20, 5 );
        HideProgressWindow( );
    }

    return result;
}


//*****************************************************************************
SendRequestThread::SendRequestThread( RequestParams *pParams )
: wxThread( )
{
    m_pParams = pParams;
    m_pParams->AddRef();
}


//*****************************************************************************
void SendRequestThread::OnExit( )
{
//  Do we need to do something like this??
//    wxCriticalSectionLocker locker(wxGetApp().m_critsect);
//
//    wxArrayThread& threads = wxGetApp().m_threads;
//    threads.Remove(this);
//
//    if ( threads.IsEmpty() )
//    {
//        // signal the main thread that there are no more threads left if it is
//        // waiting for us
//        if ( wxGetApp().m_waitingUntilAllDone )
//        {
//            wxGetApp().m_waitingUntilAllDone = false;
//
//            wxGetApp().m_semAllDone.Post();
//        }
//    }
}


//*****************************************************************************
void *SendRequestThread::Entry( )
{
    wxString strStatusCode, strStatusText;
    RequestParams *params = m_pParams;
    params->hr = params->_this->SendRequest(params->szReq, params->strFileURL, params->strFilePath, params->pData, params->dwLength,
                                             params->strStatusCode, params->strStatusText, params->bAddLockToken, params->strLockToken);
    params->m_bStopped = true;
    params->RemoveRef();
    return NULL;
}


//*****************************************************************************
// Lock the markup file (unless saveas image)
// Load & parse the markup file
// Load the image
bool DocShareView::LoadPageMarkup( const wxString& aDocId, const int iPageIndex, bool aNotifyServer, bool aLockFile )
{
    if( iPageIndex < 0  ||  iPageIndex >= (int)m_saPages.size( ) )
        return false;

    wxString    aPageId( m_saPages.at( iPageIndex ).m_strFile );
    wxString    aAltPageId( m_saPages.at( iPageIndex ).m_strAltFile );

    wxASSERT(!aDocId.IsEmpty());
    wxASSERT(!aPageId.IsEmpty());

    wxString     cstrEncodedDocID( aDocId );
    CUtils::EncodeStringFully( cstrEncodedDocID );

    wxString    strT( _("Loading"), wxConvUTF8 );
    wxString    strT2( _("Loading document...please wait."), wxConvUTF8 );
    ShowProgressWindow( strT, strT2, true );

    bool        rc = true;

    if ( !m_pDocument->m_Meeting.IsInProgress())
    {
        if ( m_bPresenter )
            m_pMMF->GetShareControl( )->ShareDocument( m_bPresenter, m_bWhiteboarding );
        m_pDocument->m_Meeting.SetIsInProgress( true );
    }
    
    while( true )	// just for breakable
    {
        if (IsPresenter() && aLockFile)
        {
            wxString strTargetFileURL(m_pDocument->m_Meeting.GetDocShareServer());
            wxString strTargetFilePath(m_pDocument->m_Meeting.GetDocSharePath());
            strTargetFilePath += m_strMeetingID;
            strTargetFilePath += _T("/");
            strTargetFilePath += cstrEncodedDocID;
            strTargetFilePath += _T('/');
            strTargetFilePath += aPageId+_T(".xml");
            long hr = LockFile( strTargetFileURL, strTargetFilePath, true, m_strLockToken, m_strLastModified );
            if( FAILED(hr) )
            {
                wxString    errmsg( _("Failed to load document from server, please try again later."), wxConvUTF8 );
                CUtils::ReportErrorMsg( hr, true, errmsg, m_pCanvas /* m_hWnd */ );
                rc = false;
                break;
            }
            m_bFileLocked = true;
        }

        std::string     xml;
        wxString        strStatusCode, strStatusText;
        long hr = AsyncRetrieveFile( cstrEncodedDocID, aPageId+_T(".xml"), xml, strStatusCode, strStatusText, false);
        if (SUCCEEDED(hr) && strStatusCode==_T("200"))
        {
            iks *x=NULL, *y=NULL;
            iksparser *prs;

            prs = iks_dom_new(&x);
            if(prs)
            {
                if (iks_parse(prs, xml.c_str(), xml.length(), 1)!=IKS_BADXML)
                {
                    m_drawingObjects.DeleteAllItems();
                    m_pLastSelectedItem = NULL;

                    y = iks_child (x);
                    while (y)
                    {
                        if (iks_type (y) == IKS_TAG && strcmp (iks_name (y), "item") == 0)
                        {
                            CBaseDrawingObject* obj = NULL;
                            char* type = iks_find_cdata (y, "type");
                            if (strcmp(type, DOType_Line.mb_str( ))==0)
                                obj = new CLine();
                            else if (strcmp(type, DOType_CurveLine.mb_str( ))==0)
                                obj = new CCurveLine();
                            else if (strcmp(type, DOType_HighLite.mb_str( ))==0)
                                obj = new CHighLite();
                            else if (strcmp(type, DOType_ArrowLine.mb_str( ))==0)
                                obj = new CArrowLine();
                            else if (strcmp(type, DOType_Rectangle.mb_str( ))==0)
                                obj = new CRectangle();
                            else if (strcmp(type, DOType_Ellipse.mb_str( ))==0)
                                obj = new CEllipse();
                            else if (strcmp(type, DOType_TextBox.mb_str( ))==0)
                                obj = new CTextBox();
                            else if (strcmp(type, DOType_Image.mb_str( ))==0)
                            {
                                obj = new CImage();
                            }
                            else if (strcmp(type, DOType_CheckMark.mb_str( ))==0)
                            {
                                obj = new CCheckMark();
                            }
                            else if (strcmp(type, DOType_RadioMark.mb_str( ))==0)
                            {
                                obj = CRadioMark::GetRadioMark();
                            }

                            if (obj)
                            {
                                obj->SetAttributes(y);
                                wxFontData  logFont;
                                if (CreateFont(obj->GetFontFace(), obj->GetFontSize(), obj->GetFontStyle(), logFont) == false)
                                {
                                    logFont = m_logFont;
                                }
                                obj->SetLogFont( logFont, false );
                                m_drawingObjects.Add( obj, false );
                            }
                        }
                        y = iks_next (y);
                    }
                    iks_parser_delete(prs);
                }
                else
                {
                    wxLogDebug( wxT("XML document is not well-formed\n") );
                    CUtils::DisplayMessage( m_pCanvas, CUtils::IDS_IIC_ERROR, CUtils::IDS_DOCSHARE_MARKUPNOTWELLFORMED, wxOK | wxICON_EXCLAMATION );
                    rc = false;
                    break;
                }
            }
            else
            {
                CUtils::DisplayMessage( m_pCanvas, CUtils::IDS_IIC_ERROR, CUtils::IDS_DOCSHARE_NOPARSER, wxOK | wxICON_EXCLAMATION );
                rc = false;
                break;
            }
        }
        else if( strStatusCode==_T("404") )
        {
            //  No markup created yet, it is ok
            m_drawingObjects.DeleteAllItems();
            m_pLastSelectedItem = NULL;
        }
        else if( hr == E_ABORT )
        {
            wxLogDebug( wxT("Operation was aborted") );
            rc = false;
            break;
        }
        else
        {
            wxString    errmsg( _("Failed to load document from server, please try again later."), wxConvUTF8 );
            long    lStatus     = 0;
            strStatusCode.ToLong( &lStatus );
            CUtils::ReportDAVError( m_pCanvas, lStatus, strStatusText,
                                   _T("%s\n%s\n%s"), errmsg, hr );
            rc = false;
            break;
        }

        ClearBackgroundImages( );
        ++m_nImageCount;

        //  Load background image for document share
        m_fOpenedAltImage = false;

        std::string     s;
        bool            fLoaded     = false;
        wxString        strExt      = aPageId.AfterLast( wxChar('.') ).MakeUpper( );
        
        if (!strExt.IsEmpty())
        {
            bool            fEMF        = strExt.EndsWith( wxT("EMF") ) || strExt.EndsWith( wxT("EMZ") );
            if( fEMF  &&  !m_fEMFSupported )
                hr = -1; 
            else
                hr = AsyncRetrieveFile( cstrEncodedDocID, aPageId, s, strStatusCode, strStatusText, true );
            if( (FAILED(hr) && hr != E_ABORT)  ||  strStatusCode != wxT("200") )
            {
                // Try to load alternate version of background image
                wxString        strAltExt   = aAltPageId.AfterLast( wxChar('.') ).MakeUpper( );
                bool            fAltEMF     = strAltExt.EndsWith( wxT("EMF") ) || strAltExt.EndsWith( wxT("EMZ") );
                if( fAltEMF  &&  !m_fEMFSupported )
                    hr = -1;
                else
                    hr = AsyncRetrieveFile( cstrEncodedDocID, aAltPageId, s, strStatusCode, strStatusText, true );
                strExt = aAltPageId.AfterLast( wxChar('.') );
                m_fOpenedAltImage = true;
            }
            if( SUCCEEDED(hr)  &&  strStatusCode == wxT("200") )
            {
                wxString    tempFile( s.c_str( ), wxConvUTF8 );
                if( !tempFile.IsEmpty( ) )
                {
                    strExt.MakeUpper( );

                    fEMF = strExt.EndsWith( wxT("EMF") ) || strExt.EndsWith( wxT("EMZ") );
                    bool    fBMP    = strExt.EndsWith( wxT("BMP") );
                    bool    fJPEG   = (strExt.EndsWith( wxT("JPEG") )  ||  strExt.EndsWith( wxT("JPG") ) );
                    bool    fPNG    = strExt.EndsWith( wxT("PNG") );
                    bool    fTIFF   = (strExt.EndsWith( wxT("TIFF") )  ||  strExt.EndsWith( wxT("TIF") ) );

                    if( fPNG )
                    {
                        m_pImage = new wxImage( tempFile, wxBITMAP_TYPE_PNG );
                        if( m_pImage  &&  m_pImage->IsOk( ) )
                            fLoaded = true;
                    }
    #ifdef METAFILE
                    else if( fEMF )
                    {
                        if (strExt.EndsWith( wxT("EMZ")))
                        {
                            wxString tempFile1 = tempFile + wxT("1");
                            CUtils::DecompressFile( tempFile, tempFile1 );
                            wxRemoveFile( tempFile );
                            tempFile = tempFile1;
                        }

                        m_pMetafile = new wxEnhMetaFile( tempFile );
                        if( m_pMetafile  &&  m_pMetafile->IsOk( ) )
                            fLoaded = true;
                    }
    #endif
                    else if( fTIFF )
                    {
                        m_pImage = new wxImage( tempFile, wxBITMAP_TYPE_TIF );
                        if( m_pImage  &&  m_pImage->IsOk( ) )
                            fLoaded = true;
                    }
                    else if( fJPEG )
                    {
                        m_pImage = new wxImage( tempFile, wxBITMAP_TYPE_JPEG );
                        if( m_pImage  &&  m_pImage->IsOk( ) )
                            fLoaded = true;
                    }
                    else if( fBMP )
                    {
                        m_pImage = new wxImage( tempFile, wxBITMAP_TYPE_BMP );
                        if( m_pImage  &&  m_pImage->IsOk( ) )
                            fLoaded = true;
                    }

                    ::wxRemoveFile( tempFile );
                }
            }
        }

        // writeboard has no bkgnd image, no need to report error
        // TODO: report error for docshare

        if( m_pDocument->m_Meeting.IsUserPresenter() && aNotifyServer &&
              m_pDocument->m_Meeting.IsPresenting() )	// avoid getting into a loop condition in reconnecting
        {
            controller_docshare_download( CONTROLLER.controller, m_strMeetingID.mb_str( wxConvUTF8 ), aDocId.mb_str( wxConvUTF8 ), aPageId.mb_str( wxConvUTF8 ));
        }

        wxString strLabel = wxString::Format( _("Page: %d / %d"), iPageIndex+1, m_saPages.size( ) );
        m_pMMF->GetMainPanel()->GetPositionLabel()->SetLabel( strLabel );
        m_pMMF->GetMainPanel()->GetPositionLabel()->Fit( );        
        
        IICRect rect;
        wxSize  sizeT = m_pCanvas->GetClientSize( );
        rect.right = sizeT.x;
        rect.bottom = sizeT.y;

        m_pCanvas->RefreshRect( wxRect( rect.left, rect.top, rect.Width( ), rect.Height( )) );
        break;
    }

    HideProgressWindow( );

    return rc;
}


//*****************************************************************************
//  Lock / Unlock file
//  If file does not exist, create a new file
//  If file is already locked by another user, either fail or open as read-only;
//    HOWEVER if it is already in read-only state, do nothing
//
long DocShareView::LockFile( const wxString& aUrl, const wxString& aUrlPath, bool bLockUnlock, wxString& strLockToken, wxString& strLastModified )
{
    long hr = wxID_OK;

    wxString statusCode, strStatusText;
    std::string xml;
    wxString owner, lockToken, lastModified;
    // check if file is locked or available
    hr = GetLockInfo( aUrl, aUrlPath, lockToken, owner, lastModified, statusCode, strStatusText );
    if( SUCCEEDED(hr) && statusCode==_T("207") )
    {
    }
    else if( statusCode == _T("404") )	// file does not exist - create one first
    {
        UpdatePageMarkup( false, true, true, true );    // force save of an empty XML file
    }
    else
    {
        wxLogDebug( wxT("Unable to retrieve file info, server might not be reachable.") );
        return hr;
    }

    // cannot lock/unlock a file if it is locked by other users
    if (bLockUnlock)
    {
        if (!owner.IsEmpty()  &&  owner != m_strUserName)
        {
            if (!m_fReadOnly)
            {
                if (IsPresenter() && m_pDocument->m_Meeting.IsInProgress())
                {
                    wxLogDebug( wxT("File is locked by user: %s. Unlock it."), owner.wc_str( ) );

                    // I'm the presenter & in the meeting, unlock it anyway
                    hr = m_WebDavUtils.DAVLockUnlock( m_pCanvas, aUrl, aUrlPath, m_strUserName, m_strPasswd, lockToken, false, xml, statusCode, strStatusText);
                    if (SUCCEEDED(hr) && (statusCode==_T("200") || statusCode==_T("204")))
                    {
                        lockToken.Empty();	// continue to lock the file
                    }
                }

                if (!lockToken.IsEmpty())
                {
                    wxString    strT( _("Unable to lock file to edit.\nFile is locked by user: %s.\nOpen as read-only."), wxConvUTF8 );
                    wxString    msg = wxString::Format( strT, owner.wc_str( ) );
                    wxLogDebug( wxT("%s"), msg.wc_str( ) );
                    strT.Empty( );
                    strT.Append( wxString( _("Warning"), wxConvUTF8 ) );
                    wxMessageDialog     dlg( m_pCanvas, msg, strT, wxOK | wxICON_EXCLAMATION );
                    dlg.ShowModal( );
                    m_fReadOnly = true;
                }
            }
        }
        strLockToken = lockToken;
        if (!strLockToken.IsEmpty())	// already locked by the same user
        {
            strLastModified = lastModified;	// save the last modified time
            return wxID_OK;
        }
    }
    else
    {
        if (lockToken.IsEmpty() || owner!=m_strUserName)// || lockToken!=strLockToken)
        {
            wxLogDebug( wxT("Unlock failed. File was not locked by user: %s"), m_strUserName.wc_str( ) );
            return wxID_OK;	// ignore this error
        }
        strLockToken = lockToken;
    }

    xml = "";

    strLastModified = lastModified;	// save the last modified time

    hr = m_WebDavUtils.DAVLockUnlock( m_pCanvas, aUrl, aUrlPath, m_strUserName, m_strPasswd, strLockToken, bLockUnlock, xml, statusCode, strStatusText);
    if (SUCCEEDED(hr) && (statusCode==_T("200") || statusCode==_T("204")))
    {
        m_fReadOnly = false;

        strLockToken.Empty();

        if (bLockUnlock)
        {
            iks *x=NULL, *y=NULL, *z=NULL;
            iksparser *prs;

            prs = iks_dom_new(&x);
            if(prs)
            {
                iks_parse(prs, xml.c_str(), xml.length(), 1);
                y = iks_find(y, "D:prop");
                if (y)
                {
                    strLastModified = wxString( iks_find_cdata(y, "lp1:getlastmodified"), wxConvUTF8 );
                }

                z = iks_find(x, "D:lockdiscovery");
                if (z)
                {
                    z = iks_find(z, "D:activelock");
                    if (z)
                    {
                        z = iks_find(z, "D:locktoken");
                        if (z)
                        {
                            strLockToken = wxString( iks_find_cdata(z, "D:href"), wxConvUTF8 );
                        }
                    }
                }

                iks_parser_delete(prs);
            }
        }

        return wxID_OK;
    }

    return hr;
}


//*****************************************************************************
long DocShareView::GetLockInfo( const wxString& aUrl, const wxString& aUrlPath, wxString& strLockToken, wxString& strOwner,
                                   wxString& strLastModified, wxString& statusCode, wxString& strStatusText )
{
    long hr = wxID_OK;

    wxString    strXML;
    hr = m_WebDavUtils.DAVRetrieveInfo( (wxWindow*) m_pCanvas, aUrl, aUrlPath, m_strUserName, m_strPasswd, strXML, statusCode, strStatusText);
    std::string     xml = (const char *) strXML.mb_str( wxConvUTF8 );
    if (SUCCEEDED(hr) && statusCode==_T("207"))
    {
        iks *x=NULL, *y=NULL, *z=NULL;
        iksparser *prs;

        prs = iks_dom_new(&x);
        if(prs)
        {
            iks_parse(prs, xml.c_str(), xml.length(), 1);

            y = iks_find (x, "D:response");
            if (y)
            {
                y = iks_find(y, "D:propstat");
                if (y)
                {
                    y = iks_find(y, "D:prop");
                    if (y)
                    {
                        strLastModified = wxString( iks_find_cdata(y, "lp1:getlastmodified"), wxConvUTF8 );

                        y = iks_find(y, "D:lockdiscovery");
                        if (y)
                        {
                            y = iks_find(y, "D:activelock");
                            if (y)
                            {
                                z = iks_find(y, "ns0:owner");
                                if (z)
                                {
                                    strOwner = wxString( iks_find_cdata(z, "ns0:href"), wxConvUTF8 );
                                }

                                z = iks_find(y, "D:locktoken");
                                if (z)
                                {
                                    strLockToken = wxString( iks_find_cdata(z, "D:href"), wxConvUTF8 );
                                }
                            }
                        }
                    }
                }
            }
            iks_parser_delete(prs);
        }
    }

    return hr;
}


//*****************************************************************************
bool DocShareView::UpdatePageMarkup( bool bUnlock, bool bUpdate, bool bForceUpdate, bool fSaveEmptyXML )
{
    wxLogDebug( wxT("entering DocShareView::UpdatePageMarkup( bUnlock = %d, bUpdate = %d, bForceUpdate = %d )"), bUnlock, bUpdate, bForceUpdate );
    
    long hr = wxID_OK;
    wxString    errmsg( _("Unable to update document. Please try it again later."), wxConvUTF8 );
    HideEditWindow( );

    if( m_strDocId.IsEmpty() || m_saPages.size() == 0 || m_fReadOnly )
        return true;	// nothing to do

    wxString strStatusCode, strStatusText;
    wxString xml;
    wxString owner, lockToken, lastModified;

    if( fSaveEmptyXML )
        xml.Append( wxT("<markup></markup>") );
    else
        m_drawingObjects.GetXML( xml );
    wxString strTargetFileURL(m_pDocument->m_Meeting.GetDocShareServer());

    wxString strTargetFilePath(m_pDocument->m_Meeting.GetDocSharePath());
    strTargetFilePath += m_strMeetingID;
    strTargetFilePath += _T("/");

    wxString    strT( m_strDocId );
    CUtils::EncodeStringFully( strT );
    strTargetFilePath += strT;
    strTargetFilePath += _T('/');
    strTargetFilePath += m_saPages.at( m_nPageIndex ).m_strFile + _T(".xml");

    if( IsPresenter() || bForceUpdate )	// To avoid locking, only presenter can save
    {
        if ((bUpdate && m_bDirty) || bForceUpdate)
        {
            if (!m_strLockToken.IsEmpty())	// check if still the owner -- maybe locked by another user, or lock expired, or changed by another user
            {
                hr = GetLockInfo(strTargetFileURL, strTargetFilePath, lockToken, owner, lastModified, strStatusCode, strStatusText);
//                wxLogDebug( wxT("   GetLockInfo( ) returned lastmodified date of %s"), lastModified.wc_str( ) );
                if (SUCCEEDED(hr) && strStatusCode == _T("207") )
                {
                    if (!owner.IsEmpty() && owner!=m_strUserName)	// file is locked by another user
                    {
                        if( CUtils::DisplayMessage( m_pCanvas, CUtils::IDS_IIC_ERROR, CUtils::IDS_DOCSHARE_LOCKED, wxYES_NO | wxICON_EXCLAMATION ) != wxID_YES )
                            return false;
                        m_bFileLocked = false;	// give up changes
                    }
                    else if (lastModified != m_strLastModified && m_strLockToken!=lockToken)	// file is not locked, but changed by another user
                    {
                        wxLogDebug( wxT("inside DocShareView::UpdatePageMarkup( ) - just hit the unimplemented code!!") );
#if 0 // ToDo: review
                        COleDateTime date1;
                        COleDateTime date2;

                        CUtils::ParseDateTime(_bstr_t(m_strLastModified), date1);
                        CUtils::ParseDateTime(_bstr_t(lastModified), date2);

                        if (date2 > date1)	// in a cluster environment, the meeting maybe assigned to a different doc server, and has different lock info
                        {
                            if (CUtils::DisplayMessage( m_pCanvas, CUtils::IDS_IIC_ERROR, CUtils::IDS_DOCSHARE_ALTERED, MB_YESNO|MB_ICONWARNING)!=IDYES)
                                return false;
                            m_bFileLocked = false;	// give up changes
                        }
#endif
                    }
                    m_strLockToken = lockToken;	// either empty, or the same
                    m_strLastModified = lastModified;
                }
                else
                {
                    if (!m_strLockToken.IsEmpty() && strStatusCode == _T("404"))	// had the lock before, file was deleted somehow
                    {
                        m_strLockToken.Empty();
                        m_bFileLocked = true;
                    }
                    else
                    {
                        errmsg = wxString( _("Failed to retrieve document information from server. Please try it again later."), wxConvUTF8 );
                        long    lStatus     = 0;
                        strStatusCode.ToLong( &lStatus );
                        CUtils::ReportDAVError( m_pCanvas, lStatus, strStatusText, _T("%s\n%s\n%s"), errmsg, hr);
                        return false;
                    }
                }
            }

            if (m_bFileLocked)	// if we are not giving up the changes
            {
                std::string s = (const char *) xml.mb_str( wxConvUTF8 );
                hr = AsyncSendRequest(_T("PUT"), strTargetFileURL, strTargetFilePath, (BYTE*)s.c_str(), s.length(),
                                      strStatusCode, strStatusText, true, m_strLockToken);
                if (FAILED(hr) && hr !=E_ABORT)
                {
                    long    lStatus     = 0;
                    strStatusCode.ToLong( &lStatus );
                    CUtils::ReportDAVError( m_pCanvas, lStatus, strStatusText,
                                           _T("%s\n%s\n%s"), errmsg, hr);
                    return false;
                }
                // update the last modified time
                hr = GetLockInfo(strTargetFileURL, strTargetFilePath, lockToken, owner, lastModified, strStatusCode, strStatusText);
                if (SUCCEEDED(hr) && strStatusCode == _T("207"))
                {
                    m_strLastModified = lastModified;
                }

                // now create a merged image and upload to server
                UploadCurrentPage( );
            }
            else	// create initial markup page
            {
                std::string s = "<markup></markup>\n";
                hr = AsyncSendRequest(_T("PUT"), strTargetFileURL, strTargetFilePath, (BYTE*)s.c_str(), s.length(),
                                      strStatusCode, strStatusText, false, wxT("") );
            }
        }

        SetPageDirty( false );

    }

    if (bUnlock && m_bFileLocked)
    {
        long hr = LockFile( strTargetFileURL, strTargetFilePath, false, m_strLockToken, m_strLastModified );
        if (FAILED(hr))
        {
            CUtils::ReportErrorMsg(hr, TRUE, errmsg, m_pCanvas );
            return false;
        }
        m_bFileLocked = false;
    }

    return true;
}


//*****************************************************************************
long DocShareView::UploadCurrentPage( )
{

    wxString    strT( _("Uploading"), wxConvUTF8 );
    wxString    strT2( _("Uploading merged document...please wait."), wxConvUTF8 );
    ShowProgressWindow( strT, strT2, true );

    wxString    strTemp;
    strTemp = wxFileName::CreateTempFileName( strTemp );

    strTemp.Append( m_saPages.at( m_nPageIndex ).m_strFile );
    strTemp = strTemp.Lower( );
    strT = wxString( _("Fit to size"), wxConvUTF8 );
    SaveasImageEx( strTemp, _T("JPG"), strT );

    wxString strStatusCode, strStatusText;
    wxString strTargetFileURL = m_pDocument->m_Meeting.GetDocShareServer();
    strTargetFileURL += m_strMeetingID;
    strTargetFileURL += _T("/");
    strTargetFileURL += m_strDocId;
    strTargetFileURL += _T('/');
    strTargetFileURL += m_saPages.at( m_nPageIndex ).m_strFile;

    wxString strTargetFilePath = m_pDocument->m_Meeting.GetDocSharePath();
    strTargetFilePath += m_strMeetingID;
    strTargetFilePath += _T("/");

    wxString    strDID( m_strDocId );
    CUtils::EncodeStringFully( strDID );
    strTargetFilePath += strDID;
    strTargetFilePath += _T('/');
    strTargetFilePath += m_saPages.at( m_nPageIndex ).m_strFile;
    if( strTemp.Find( _T(".jpg"))  !=  wxNOT_FOUND )
    {
        strTargetFileURL  += _T(".merged");
        strTargetFilePath += _T(".merged");
    }
    else
    {
        strTargetFileURL  += _T(".jpg.merged");
        strTargetFilePath += _T(".jpg.merged");
    }

    long     hr = AsyncSending( strTargetFileURL, strTargetFilePath, strTemp, strStatusCode, strStatusText );
    if( FAILED(hr) )
    {
        if( !m_pDialogProgress->fCanceled( ) )
        {
            wxString    errmsg;
            long    lStatus     = 0;
            strStatusCode.ToLong( &lStatus );
            CUtils::ReportDAVError( m_pCanvas, lStatus, strStatusText,
                                   _T("%s\n%s\n%s"), errmsg, hr);
        }
    }

    HideProgressWindow( );

    ::wxRemoveFile( strTemp );

    return hr;
}


//*****************************************************************************
void DocShareView::SaveasImageEx( const wxString& strPath, const wxString& strType, const wxString& strPageSize )
{
    wxString    strFitToSize( _("Fit to size"), wxConvUTF8 );

    int w = 640, h = 480;
    if( strPageSize  ==  wxT("800 x 600") )
    {
        w = 800;
        h = 600;
    }
    else if( strPageSize  ==  wxT("1024 x 768") )
    {
        w = 1024;
        h = 768;
    }
    else if( strPageSize  ==  strFitToSize )
    {
        wxMemoryDC  memDC;
        w = 288;
        h = 208;
        if( m_pImage  &&  m_pImage->IsOk( ) )
        {
            w = m_pImage->GetWidth( );
            h = m_pImage->GetHeight( );
        }
        wxSize  size = m_drawingObjects.GetExtent( memDC );
        if (w < size.GetWidth( ))
            w = size.GetWidth( );
        if (h < size.GetHeight( ))
            h = size.GetHeight( );
        w = (w/32 + 1) * 32;
        h = (h/32 + 1) * 32;
    }

    wxMemoryDC  dcMemory;
    wxBitmap    bitmap( w, h );
    dcMemory.SelectObject( bitmap );
    IICRect     rect( 0, 0, w, h );

    wxBrush     brWhite( wxColor(255, 255, 255) );
    dcMemory.SetBrush( brWhite );

    wxPen       penWhite( wxColor(255, 255, 255) );
    dcMemory.SetPen( penWhite );

    dcMemory.DrawRectangle( 0, 0, w, h );

    InternalPaint( &dcMemory, rect, 0, 0 );

    dcMemory.SelectObject( wxNullBitmap );

    bool        fRet;
    fRet = bitmap.IsOk( );

    wxImage     image;
    image = bitmap.ConvertToImage( );
    fRet = image.IsOk( );

    if( strType  ==  _T("JPG") )
    {
        fRet = image.SaveFile( strPath, wxBITMAP_TYPE_JPEG );
    }
    else
    {
        fRet = image.SaveFile( strPath, wxBITMAP_TYPE_BMP );
    }
}


//*****************************************************************************
long DocShareView::AsyncSending( const wxString& aTargetUrl, const wxString& aTargetPath, const wxString& aFileName, wxString& strStatusCode, wxString& strStatusText, bool fMultiFileSend )
{
    SendingParams   *params = new SendingParams();
    params->_this        = this;
    params->m_TargetUrl  = aTargetUrl;
    params->m_TargetPath = aTargetPath;
    params->m_FileName   = aFileName;
    params->m_bStopped   = false;
    params->hr           = 0;
    params->iChunk       = 0;

    bool        fExists     = wxFile::Exists( aFileName );
    wxFile      hfile;
    if( fExists )
        hfile.Open( aFileName, wxFile::read );
    if( hfile.IsOpened( ) )
    {
        unsigned long   dwFileSize  = hfile.Length( );
        hfile.Close( );

        wxString    title       = aTargetPath;
        int         index       = title.Find( _T('/'), true );
        if( index  !=  -1 )
            title = title.Right( title.Len( ) - index - 1 );
        wxString    cstrT( _("File: "), wxConvUTF8 );
        cstrT += title;
        m_pDialogProgress->SetLabelTitle2( cstrT );
        int     chunks = dwFileSize / iChunkSize;
        if( chunks  ==  0 )
            chunks = 1;
        m_pDialogProgress->SetProgressRange2( chunks );
        m_pDialogProgress->SetProgressPos2( 0 );

        SendFileThread  *pSendFileThread    = new SendFileThread( params );
        if( pSendFileThread )
        {
            if( pSendFileThread->Create( )  !=  wxTHREAD_NO_ERROR )
            {
                wxLogError( wxT("Could not create Send thread!") );
            }
            else
            {
                pSendFileThread->Run( );

                int     iLC = 0;
//                wxLogDebug( wxT("in DocShareView::AsyncSending( )  -- BEFORE LOOPING") );

                int     chunk = 0;
                while( !params->m_bStopped )
                {
                    iLC++;
                    if( chunk != params->iChunk )
                    {
                        m_pDialogProgress->SetProgressPos2( params->iChunk );
                    }
                    CUtils::MsgWait( 20, 5 );
                    
                    if( m_pDialogProgress == NULL )
                    {
                        params->hr = E_ABORT;
                        break;
                    }
                    
                    if( m_pDialogProgress->fCanceled( ) )
                    {
                        params->hr = E_ABORT;
                        if( !fMultiFileSend )
                            HideProgressWindow( );
                        break;
                    }
                }
//                wxLogDebug( wxT("in DocShareView::AsyncSending( ) looped waiting for results %d times"), iLC );
            }
        }

        strStatusCode = params->strStatusCode;
        strStatusText = params->strStatusText;
        long result = params->hr;
        
        params->RemoveRef();
        
        return result;
    }
    else
    {
        wxLogDebug( wxT("Could not open %s"), aFileName.wx_str( ) );
        params->RemoveRef();
    }

    return GetLastError( ) | 0x8007000;
}


//*****************************************************************************
SendFileThread::SendFileThread( SendingParams *pParams )
    : wxThread( )
{
    m_pParams = pParams;
    m_pParams->AddRef();
}


//*****************************************************************************
void SendFileThread::OnExit( )
{
}


//*****************************************************************************
void *SendFileThread::Entry( )
{
    wxString strStatusCode, strStatusText;
    SendingParams *params = m_pParams;
    params->hr = params->_this->SendFileEx(params->m_TargetUrl, params->m_TargetPath, params->m_FileName,
                                           params->strStatusCode, params->strStatusText, &params->iChunk);
    params->m_bStopped = true;
    params->RemoveRef();
    return NULL;
}


//*****************************************************************************
long DocShareView::SendFileEx( const wxString& aTargetUrl, const wxString& aTargetPath,
                                  const wxString& aFileName, wxString& strStatusCode,
                                  wxString& strStatusText, int* piChunk )
{
    long hr = S_OK;

    bool        fExists     = wxFile::Exists( aFileName );
    wxFile      hfile;
    if( fExists )
        hfile.Open( aFileName, wxFile::read );
    if( hfile.IsOpened( ) )
    {
        unsigned long   dwFileSize  = hfile.Length( );
        BYTE*   lpBuf       = new BYTE[ dwFileSize+1 ];
        memset( lpBuf, 0, dwFileSize+1 );
        DWORD   iBytesRead;
        iBytesRead = hfile.Read( lpBuf, dwFileSize );
        hfile.Close( );
        if( iBytesRead  >  0 )
        {
            // Don't use AsyncSendRequest here, cause it is called in aSendFileEx bkgnd thread
            wxString    strT( _T("PUT") );
            hr = SendRequest( strT, aTargetUrl, aTargetPath, lpBuf, iBytesRead,
                             strStatusCode, strStatusText, false, m_strLockToken, piChunk );
            if( m_pDialogProgress->fCanceled( ) )
            {
                hr = E_ABORT;
            }
        }
        delete [] lpBuf;
    }
    else
    {
        wxString errmsg( _("Failed to open: "), wxConvUTF8 );
        errmsg += aFileName;
        errmsg += wxT("\r\n");
        CUtils::ReportErrorMsg( GetLastError( ), true, errmsg, m_pCanvas );
        wxLogDebug( wxT("%s"), errmsg.wc_str( ) );

        hr = E_FAIL;
    }

    return hr;
}


//*****************************************************************************
long DocShareView::SendRequest( wxString& szReq, const wxString& strFileURL, const wxString& strFilePath,
                                  const BYTE* pData, DWORD dwLength, wxString& strStatusCode, wxString& strStatusText,
                                  bool bAddLockToken, const wxString& strLockToken, int* piChunk )
{
    return m_WebDavUtils.SendRequest( szReq, strFileURL, strFilePath, m_strUserName, m_strPasswd, pData, dwLength, strStatusCode,
                                      strStatusText, bAddLockToken, strLockToken, m_pCanvas, piChunk );
}


//*****************************************************************************
void DocShareView::ProcessEventQueue()
{
    while (!m_event_queue.empty())
    {
        DocshareEvent * event = m_event_queue.front();
        m_event_queue.pop_front();

        switch( event->GetEvent( ) )
        {
            case DocShareStarted:
                break;

            case DocShareEnded:
                wxLogDebug( wxT("entering DocShareView::OnDocshareEvent( ) to handle DocShareEnded event") );
                Stop( );
                break;

            case DocShareDownloadPage:
                wxLogDebug( wxT("entering DocShareView::OnDocshareEvent( ) to handle DocShareDownloadPage - docID = %s,  pageID = %s"),
                            event->GetDocId( ).wc_str( ), event->GetPageId( ).wc_str( ) );
                OnDocshareDownload( event->GetDocId( ), event->GetPageId( ) );
                break;

            case DocShareConfigure:
                wxLogDebug( wxT("entering DocShareView::OnDocshareEvent( ) to handle DocShareConfigure event  --  NOT IMPLEMENTED!!!!") );
                break;

            case DocShareDrawing:
                wxLogDebug( wxT("entering DocShareView::OnDocshareEvent( ) to handle DocShareDrawing event") );
                if( m_bDragging )
                {
                    AddPendingEvent( *event );
                }
                else
                {
                    OnDocshareDrawing( event->GetDrawing( ) );
                }
                break;

            case ParticipantJoined:
            {
                wxLogDebug( wxT("entering DocShareView::OnDocshareEvent( ) to handle ParticipantJoined event") );
                wxCommandEvent  evt( wxEVT_COMMAND_TOOL_CLICKED, XRCID("IDC_DOCSHAREEVENT_PARTJOINED") );
                AddPendingEvent( evt );
                break;
            }

            default:
                wxLogDebug( wxT("in DocShareView::OnDocshareEvent( )  --  invalid/unexpected event type received within a DocShareEvent - %d"), event->GetEvent( ) );
                break;
        }

        delete event;
    }    
}


//*****************************************************************************
// OnDocshareEvent
void DocShareView::OnDocshareEvent( DocshareEvent& event )
{
    if ( m_pMMF == NULL )
    {
        event.Skip();
        return;
    }
    
    switch( event.GetEvent( ) )
    {
        case DocShareStarted:
            event.Skip();
            break;

        case DocShareEnded:
            Stop( );
            event.Skip( );
            break;
            
        case DocShareDownloadPage:
        case DocShareConfigure:
        case DocShareDrawing:
        case ParticipantJoined:
            // We'll handle these
            m_event_queue.push_back( new DocshareEvent( event ));

            // Only handle now if not already processing events
            if (!m_in_handler)
            {
                m_in_handler = true;
                ProcessEventQueue();
                m_in_handler = false;
            }
            event.Skip();
            break;
    }

}


//*****************************************************************************
// OnDocshareDrawing
void DocShareView::OnDocshareDrawing( const wxString& strDrawing )
{
    if( strDrawing.Len( )  ==  0 )
        return;

    std::string drawing = (const char *) strDrawing.mb_str( wxConvUTF8 );
    Draw( drawing );
}


//*****************************************************************************
// OnDocshareDownload
void DocShareView::OnDocshareDownload( const wxString& strDocID, const wxString& strPageID )
{
    Download( strDocID, strPageID );
}


//*****************************************************************************
void DocShareView::Download( const wxString& aDocId, const wxString& aPageId )
{
    if( IsPresenter( ) )		// now we allow remote controller to insert/delete page
        return;

    bool    fRet    = false;
    AllowHideProgress( false );

    bool bFoundIndex = false;
    if( m_saPages.size( )  >  0 )
    {
        for( int i=0; i<(int) m_saPages.size(); i++ )
        {
            if( aPageId  ==  m_saPages.at( i ).m_strFile )
            {
                bFoundIndex = true;
                m_nPageIndex = i;
            }
        }
    }

    if( aDocId != m_strDocId  ||  !bFoundIndex )
    {
        m_strDocId = aDocId;
        LoadIndex( );
        for( int i=0; i<(int) m_saPages.size(); i++ )
        {
            if( aPageId  ==  m_saPages.at(i).m_strFile)
            {
                bFoundIndex = true;
                m_nPageIndex = i;
            }
        }
    }

    if( bFoundIndex )
        fRet = LoadPageMarkup( aDocId, m_nPageIndex );
//    else
//        fRet = LoadPageMarkup( aDocId, aPageId, m_saPages.at( m_nPageIndex ).m_strAltFile );
//    }
//    else //index order may have been changed. if (nPageIndex != m_nPageIndex)
//    {
////        fRet = LoadPageMarkup( aDocId, aPageId, m_saPages.at( m_nPageIndex ).m_strAltFile );
//        fRet = LoadPageMarkup( aDocId, m_nPageIndex );
//    }

    if( fRet )
    {
        wxString    strWBName = wxT("Whiteboard") + m_strMeetingID;
        m_bWhiteboarding = ( strWBName.Cmp( aDocId )  == 0 );
        if( m_bWhiteboarding )
            m_pCanvas->SetScrollRate( m_iXScrollRate, m_iYScrollRate );
        else
            m_pCanvas->SetScrollRate( 0, 0 );
    }
    AllowHideProgress( true );
}


//*****************************************************************************
void DocShareView::OnDocshareParticipantJoined( wxCommandEvent& event )
{
    //  Someone just joined the meeting so update the whiteboard info
    //  on the server and force an update of all observers
    wxLogDebug( wxT("entering DocShareView::OnDocshareParticipantJoined( )") );
    if( m_pDocument->m_Meeting.IsUserPresenter()  &&
        !m_strDocId.IsEmpty()  &&
        m_saPages.size() > 0 )		// more checking, in case invoked when sharing is not started
    {
        AllowHideProgress( false );
        UpdatePageMarkup( );		// save current markup
        LoadPageMarkup( m_strDocId, m_nPageIndex );
        AllowHideProgress( true );
    }
}


//*****************************************************************************
void DocShareView::Draw( const std::string& aDrawing )
{
    if( !m_pCanvas->IsShown( ) )
        return;
    if( aDrawing.length( )  ==  0 )
    {
        return;
    }

    while (m_bDragging)
    {
        CUtils::MsgWait( 10, 10 );
    }

    wxString userId;
    m_pDocument->m_Meeting.GetMeetingUserID(userId);

    iks *x, *y;
    iksparser *p;
    p = iks_dom_new (&x);

    if (0 != iks_parse (p, aDrawing.c_str(), aDrawing.length(), 1))
    {
        wxLogDebug( wxT("parse error") );
        return;
    }

    IICRect rcRefresh(0,0,0,0);
    char* type = iks_find_attrib (x, "type");

    if( type  ==  NULL )
    {
	    wxLogDebug( wxT("  iks_find_attrib( ) returned NULL") );
	    return;
    }
    if( strlen( type )  ==  0 )
    {
	    wxLogDebug( wxT("  iks_find_attrib( ) returned a ZERO length string") );
	    return;
    }

    char* actor = iks_find_attrib (x, "actor");
    char* undoRedoAction = iks_find_attrib (x, "undoredo");
    bool isUndoRedoAction = false;
    if (undoRedoAction)
    {
        isUndoRedoAction = atoi(undoRedoAction)>0;
    }
    if( isUndoRedoAction  &&  strcmp(actor, (const char *) userId.mb_str(wxConvUTF8)) == 0 )	// skip undo/redo actions sent to myself
    {
        iks_parser_delete(p);
        return;
    }

    m_drawingObjects.UnSelect(rcRefresh);
    CBaseDrawingObject* pSaved = m_pLastSelectedItem;
    m_pLastSelectedItem = NULL;
    wxToolBar* wndToolBar = m_pMMF->GetMainPanel()->GetDocShareToolbar();

    int action_type = CAction::AT_NONE;
    if (strcmp(type, "create")==0)
    {
        action_type = CAction::AT_CREATE;
    }
    else if (strcmp(type, "paste")==0)
    {
        action_type = CAction::AT_PASTE;
    }
    else if (strcmp(type, "delete")==0)
    {
        action_type = CAction::AT_DELETE;
        HideEditWindow();
    }
    else if (strcmp(type, "change")==0)
    {
        action_type = CAction::AT_CHANGE;
    }
    else if (strcmp(type, "bringtofront")==0)
    {
        action_type = CAction::AT_BRINGTOFRONT;
    }
    else if (strcmp(type, "sendback")==0)
    {
        action_type = CAction::AT_SENDBACK;
    }
    else if (strcmp(type, "back")==0)
    {
        if (IsPresenter())	// only pass it to presenter
        {
            wxCommandEvent  event( wxEVT_COMMAND_TOOL_CLICKED, XRCID("IDC_DOCSHARETB_PREVIOUSPAGE") );
            wndToolBar->AddPendingEvent( event );
        }
        iks_parser_delete(p);
        return;
    }
    else if (strcmp(type, "bottom")==0)
    {
        if (IsPresenter())
        {
            wxCommandEvent  event( wxEVT_COMMAND_TOOL_CLICKED, XRCID("IDC_DOCSHARETB_LASTPAGE") );
            wndToolBar->AddPendingEvent( event );
        }
        iks_parser_delete(p);
        return;
    }
    else if (strcmp(type, "top")==0)
    {
        if (IsPresenter())
        {
            wxCommandEvent  event( wxEVT_COMMAND_TOOL_CLICKED, XRCID("IDC_DOCSHARETB_FIRSTPAGE") );
            wndToolBar->AddPendingEvent( event );
        }
        iks_parser_delete(p);
        return;
    }
    else if (strcmp(type, "next")==0)
    {
        if (IsPresenter())
        {
            wxCommandEvent  event( wxEVT_COMMAND_TOOL_CLICKED, XRCID("IDC_DOCSHARETB_NEXTPAGE") );
            wndToolBar->AddPendingEvent( event );
        }
        iks_parser_delete(p);
        return;
    }
    else if (strcmp(type, "goto")==0)
    {
        if (IsPresenter())
        {
            char *id = iks_find_attrib(x, "page");
            if (id)
            {
                m_nPageIndex = atoi(id);
                LoadPageMarkup( m_strDocId, m_nPageIndex );
            }
        }
        iks_parser_delete(p);
        return;
    }
    else if (strcmp(type, "pointer")==0)
    {
        if (true /*!IsPresenter()*/)
        {
            int mx = -1, my = -1;
            char *str = iks_find_attrib(x, "x");
            if (str) mx = atoi(str);
            str = iks_find_attrib(x, "y");
            if (str) my = atoi(str);
            //wxLogDebug(wxT("Pointer x: %d, y: %d"), mx, my );
            if (mx < 0 || my < 0) 
            {
                m_pCanvas->SetPointerPos(-1, -1);
            } 
            else
            {
                wxPoint ptOffset;
#ifdef POINTER_CONTROL                
                m_pCanvas->CalcScrolledPosition( 0, 0, &ptOffset.y, &ptOffset.y );
#endif
                
                wxPoint ptPointer( mx, my );
                ScaleThePoint( ptPointer );
                
                m_pCanvas->SetPointerPos( ptPointer.x + ptOffset.x, ptPointer.y + ptOffset.y );
            }
        }
        iks_parser_delete(p);
        return;
    }
    else if (strcmp(type, "load_index")==0)
    {
        if (strcmp(actor, (const char *) userId.mb_str(wxConvUTF8)) != 0)
            LoadIndex();
        iks_parser_delete(p);
        return;
    }
    else if (strcmp(type, "refresh")==0 && m_bRefreshRequested)	// only refresh one viewer
    {
        LoadPageMarkup( m_strDocId, m_nPageIndex );

        iks_parser_delete(p);
        return;
    }

    y = iks_child (x);
    while (y)
    {
        if (iks_type (y) == IKS_TAG && strcmp (iks_name (y), "item") == 0)
        {
            CChangeAction* change_action = NULL;

            CBaseDrawingObject  *obj = NULL;
            wxString            id   = wxString( iks_find_cdata (y, "id"), wxConvUTF8 );
            type = iks_find_cdata (y, "type");
            bool fSetAttributes = false;
            bool bResetSaved = false;
            switch(action_type)
            {
                case CAction::AT_CREATE:
                case CAction::AT_PASTE:
                    obj = m_drawingObjects.Find(id);
                    if (obj!=NULL)
                    {
                        if (pSaved == obj)	// the LastSelectedObject is deleted
                        {
                            pSaved = NULL;
                            bResetSaved = true;
                        }

                        m_drawingObjects.Delete(obj);
                        delete obj;
                        obj = NULL;
                    }
                    fSetAttributes = true;
                    if (strcmp(type, DOType_Line.mb_str( ))==0)
                        obj = new CLine();
                    else if (strcmp(type, DOType_CurveLine.mb_str( ))==0)
                        obj = new CCurveLine();
                    else if (strcmp(type, DOType_HighLite.mb_str( ))==0)
                        obj = new CHighLite();
                    else if (strcmp(type, DOType_ArrowLine.mb_str( ))==0)
                        obj = new CArrowLine();
                    else if (strcmp(type, DOType_Rectangle.mb_str( ))==0)
                        obj = new CRectangle();
                    else if (strcmp(type, DOType_Ellipse.mb_str( ))==0)
                        obj = new CEllipse();
                    else if (strcmp(type, DOType_TextBox.mb_str( ))==0)
                        obj = new CTextBox();
                    else if (strcmp(type, DOType_Image.mb_str( ))==0)
                    {
                        obj = new CImage();
                    }
                    else if (strcmp(type, DOType_CheckMark.mb_str( ))==0)
                    {
                        obj = new CCheckMark();
                    }
                    else if (strcmp(type, DOType_RadioMark.mb_str( ))==0)
                    {
                        obj = CRadioMark::GetRadioMark();
                    }

                    if (obj)
                    {
                        m_drawingObjects.Add(obj, false);
                        m_drawingObjects.SelectItem(obj);	// need it to handle undo/redo

                        if (bResetSaved)
                        {
                            pSaved = obj;
                        }
                    }

                    break;
                case CAction::AT_DELETE:
                    obj = m_drawingObjects.Find(id);
                    if (obj)	// repaint the item previous position
                    {
                        rcRefresh = obj->UnionRect(rcRefresh);
                        m_drawingObjects.SelectItem(obj);	// delete it later

                        if (pSaved == obj)	// the LastSelectedObject is deleted
                        {
                            pSaved = NULL;
                        }
                    }
                    break;
                case CAction::AT_CHANGE:
                    fSetAttributes = true;
                    obj = m_drawingObjects.Find(id);
                    if (obj)	// repaint the item previous position
                    {
                        rcRefresh = obj->UnionRect(rcRefresh);
                        m_drawingObjects.SelectItem(obj);	// need it to handle undo/redo
                        change_action = new CChangeAction(m_pDocument);
                        change_action->PreAction(m_drawingObjects);
                    }
                    break;
                case CAction::AT_BRINGTOFRONT:
                    obj = m_drawingObjects.Find(id);
                    if (obj)	// repaint the item previous position
                    {
                        rcRefresh = obj->UnionRect(rcRefresh);
                        m_drawingObjects.BringToFront(id);
                        m_drawingObjects.SelectItem(obj);	// need it to handle undo/redo
                    }
                    break;
                case CAction::AT_SENDBACK:
                    obj = m_drawingObjects.Find(id);
                    if (obj)	// repaint the item previous position
                    {
                        rcRefresh = obj->UnionRect(rcRefresh);
                        m_drawingObjects.SendBack(id);
                        m_drawingObjects.SelectItem(obj);	// need it to handle undo/redo
                    }
                    break;
            }

            if (obj && fSetAttributes)
            {
                obj->SetAttributes(y);
                rcRefresh = obj->UnionRect(rcRefresh);	// refresh new location too
                wxFontData  logFont;
                if( CreateFont(obj->GetFontFace(), obj->GetFontSize(), obj->GetFontStyle(), logFont) == false )
                {
                    logFont = m_logFont;
                }
                obj->SetLogFont( logFont, false );
            }

            // handle Undo/Redo
            if (obj!=NULL)
            {
                switch(action_type)
                {
                    case CAction::AT_CREATE:
                    case CAction::AT_PASTE:
                    {
                        CAction* action = new CPasteAction(m_pDocument);
                        action->Do(m_drawingObjects);
                        m_drawingObjects.AddAction(action, FALSE);
                        break;
                    }
                    case CAction::AT_DELETE:
                        EditDelete(false);
                        break;
                    case CAction::AT_CHANGE:
                        if (change_action!=NULL)
                        {
                            change_action->PostAction(m_drawingObjects);
                            m_drawingObjects.AddAction(change_action, FALSE);
                            break;
                        }
                    case CAction::AT_BRINGTOFRONT:
                    {
                        CAction* action = new CReOrderAction(m_pDocument, CAction::AT_BRINGTOFRONT);
                        action->Do(m_drawingObjects);
                        m_drawingObjects.AddAction(action, FALSE);
                        break;
                    }
                    case CAction::AT_SENDBACK:
                    {
                        CAction* action = new CReOrderAction(m_pDocument, CAction::AT_SENDBACK);
                        action->Do(m_drawingObjects);
                        m_drawingObjects.AddAction(action, FALSE);
                        break;
                    }
                }
            }

            m_drawingObjects.UnSelect();
        }
        y = iks_next (y);
    }

    iks_parser_delete(p);

    if (pSaved != NULL)
    {
        m_drawingObjects.SelectItem(pSaved);
        m_pLastSelectedItem = pSaved;
    }

    InvalidateSelection(rcRefresh);

    SetPageDirty(true);

    wxLogDebug( wxT("    leaving DocShareView::Draw( )") );
}


//*****************************************************************************
void DocShareView::LoadIndex( )
{
    std::string xml;
    wxString strStatusCode, strStatusText;

    wxString     strEncodedDocID( m_strDocId );
    CUtils::EncodeStringFully( strEncodedDocID );

    long hr = AsyncRetrieveFile( strEncodedDocID, _T("index.xml"), xml, strStatusCode, strStatusText, false );
    if( SUCCEEDED(hr) )
    {
        if( strStatusCode == _T("200") )
        {
            iks *x=NULL, *y=NULL;
            iksparser *prs;
            prs = iks_dom_new(&x);
            if(prs)
            {
                m_saPages.clear( );

                iks_parse(prs, xml.c_str(), xml.length(), 1);

                y = iks_child (x);
                while (y)
                {
                    if (iks_type (y) == IKS_TAG && stricmp (iks_name (y), "page") == 0)
                    {
                        CDocPage page;
                        page.m_strTitle = wxString( iks_find_attrib (y, "title"), wxConvUTF8 );
                        page.m_strFile  = wxString( iks_find_attrib (y, "file"),  wxConvUTF8 ) ;
                        page.m_strAltFile  = wxString( iks_find_attrib (y, "altfile"),  wxConvUTF8 ) ;

                        wxString    strScale( iks_find_attrib( y, "scale" ), wxConvUTF8 );
                        page.m_dblScale = 1.0;
                        if( strScale.ToDouble( &page.m_dblScale ) )
                        {
                            if( page.m_dblScale < 1.0  )
                                page.m_dblScale = 1.0;
                            else if( page.m_dblScale > 4.0 )
                                page.m_dblScale = 4.0;
                        }

                        wxString    strAltScale( iks_find_attrib( y, "altscale" ), wxConvUTF8 );
                        if( strAltScale.ToDouble( &page.m_dblAltScale ) )
                        {
                            if( page.m_dblAltScale < 1.0  )
                                page.m_dblAltScale = 1.0;
                            else if( page.m_dblAltScale > 4.0 )
                                page.m_dblAltScale = 4.0;
                            else if( page.m_dblAltScale == 4.0 )   // Windows version was buggy and always set alt scale to 4
                                page.m_dblAltScale = 1.0;
                        }

                        page.m_fFitToWidth = false;
                        wxString    strFitToWidth( iks_find_attrib( y, "fittowidth" ), wxConvUTF8 );
                        strFitToWidth.Trim( false );        //  Remove any leading whitespace
                        strFitToWidth.MakeUpper( );
                        strFitToWidth = strFitToWidth.Left( 1 );
                        if(( strFitToWidth.Cmp( wxT('T') ) == 0 )  ||  ( strFitToWidth.Cmp( wxT('1') ) == 0 ))
                            page.m_fFitToWidth = true;

                        m_saPages.push_back( page );
                    }
                    y = iks_next( y );
                }
                iks_parser_delete( prs );
            }
        }
    }
    else if( hr == E_ABORT )
    {
        wxLogDebug(wxT("Operation aborted"));
        return;
    }
    else
    {
        wxString errmsg( _("Failed to load document from server, please try again later."), wxConvUTF8 );
        long    lStatus     = 0;
        strStatusCode.ToLong( &lStatus );
        CUtils::ReportDAVError( m_pCanvas, lStatus, strStatusText, _T("%s\n%s\n%s"), errmsg, hr );
        return;
    }
}


//*****************************************************************************
void DocShareView::Stop( )
{
    wxLogDebug( wxT("entering DocShareView::Stop( )") );

    if( IsPresenter( )  &&  !m_strDocId.IsEmpty( ) )
    {
        m_bPresenter = false;	// could be called twice, reset it as soon as we can
        if( m_bDirty )
        {
            UpdatePageMarkup( true, true, true );	// unlock & save
        }
        else
        {
            UpdatePageMarkup( true, false );	// unlock & no save
        }
    }

    m_pDocument->m_Meeting.SetIsInProgress( false );
        
    ClearDocument( );

    m_event_queue.clear( );

    CloseDialogs( );

    if ( m_pMMF )
    {
        if (!m_strSavedTitle.IsEmpty())
        {
            m_pMMF->SetTitle( m_strSavedTitle );
            m_strSavedTitle.Empty();
        }

        m_pMMF->GetMainPanel()->GetPositionLabel()->SetLabel( wxT("") );
    }

    m_pCanvas->Scroll(0,0);
    m_pCanvas->SetPointerPos( -1, -1 );
    m_pCanvas->m_ShareHTML->Show( true );
    m_pCanvas->m_ShareHTML->Enable( true );
}


//*****************************************************************************
void DocShareView::ClearBackgroundImages( )
{
    if( m_pImage )
    {
        delete m_pImage;
        m_pImage = NULL;
    }
#ifdef METAFILE
    if( m_pMetafile )
    {
        delete m_pMetafile;
        m_pMetafile = NULL;
    }
#endif
}


//*****************************************************************************
void DocShareView::ClearDocument( )
{
    wxLogDebug( wxT("entering DocShareView::ClearDocuments( )") );

    m_pLastSelectedItem = NULL;
    m_drawingObjects.DeleteAllItems( );
    ClearBackgroundImages( );

    m_strMeetingID.Empty( );
    m_strLockToken.Empty( );

    int     w, h;
    m_pCanvas->GetSize( &w, &h );
    IICRect rect( 0, 0, w, h );;
    UnscaleTheRect( rect );
    InvalidateSelection( rect );

    if( m_pCtlEdit )
    {
        m_pCtlEdit->Destroy( );
        m_pCtlEdit = NULL;
    }
}


//*****************************************************************************
void DocShareView::CloseDialogs( )
{
    wxLogDebug( wxT("entering DocShareView::CloseDialogs( )") );

    if ( m_pMMF )
    {
        m_fIgnoreUpdateSignal = true;
        Disconnect( wxID_ANY, wxEVT_TIMER, wxTimerEventHandler(DocShareView::OnTimer) );
        m_timer.Stop( );

        DisconnectToolbarEvents();
        m_pMMF->GetMainPanel()->DisableDocShareToolbar();
    }

    if( m_pDialogProgress )
    {
        // This also serves as a flag to any asynchronous operations still in progress that doc share is done.
        m_pDialogProgress->Cancel();
    }

    wxLogDebug( wxT("    leaving DocShareView::CloseDialogs( )") );
}

#ifdef LOCAL_CONVERSION

//*****************************************************************************
bool DocShareView::FindUploadExts( )
{
    m_saExtData.clear( );

    wxString    fileSpec( wxT("*.*") );
    wxString    fileName;
    wxString    strExePath( wxStandardPaths::Get().GetExecutablePath() );
    wxFileName  wxFN( strExePath );
    wxString    strPathOnly( wxFN.GetPath( ) );
    strPathOnly.Append( wxT("/docshare") );
    wxDir       dir( strPathOnly );
    if( !dir.IsOpened() )
        return false;

    bool fMore = dir.GetFirst( &fileName, fileSpec, wxDIR_FILES );
    while( fMore )
    {
        wxString strLibExt(wxFileName(fileName).GetExt());
        strLibExt.MakeLower();

        if (strLibExt == wxT("dll") || strLibExt == wxT("so") || strLibExt == wxT("dylib"))
        {
            wxString    strFullPath;
            strFullPath = strPathOnly + wxT("/") + fileName;

            bool                fLoaded;
            wxDynamicLibrary    *pdllUpload;
            pdllUpload = new wxDynamicLibrary( );
            fLoaded = pdllUpload->Load( strFullPath );
            fLoaded = pdllUpload->IsLoaded( );
            if( fLoaded  &&  pdllUpload->HasSymbol( wxT("dsGetStatus") ) )
            {
                typedef int (WINAPI *DSGETPRILEN)( void );
                wxDYNLIB_FUNCTION( DSGETPRILEN, dsGetPriLen, *pdllUpload );

                typedef int (WINAPI *DSGETSECLEN)( void );
                wxDYNLIB_FUNCTION( DSGETSECLEN, dsGetSecLen, *pdllUpload );

                int     iLen = 0;
                if( pfndsGetPriLen )
                    iLen = (*pfndsGetPriLen)( );
                if( pfndsGetSecLen )
                {
                    int iSecLen = (*pfndsGetSecLen)( );
                    if( iSecLen  >  iLen )
                        iLen = iSecLen;
                }

                if( iLen  >  0 )
                {
                    iLen += 4;      //  just to make sure
                    struct _UploadExtData   eData;
                    eData.strDLLName = strFullPath;
                    eData.pdllUpload = pdllUpload;

                    char    *pBuf = NULL;
                    pBuf = (char *) malloc( iLen + 4 );

                    typedef int (WINAPI *DSGETPRITYPES)( char *pC, int iMaxLen );
                    wxDYNLIB_FUNCTION( DSGETPRITYPES, dsGetPriTypes, *pdllUpload );

                    typedef int (WINAPI *DSGETSECTYPES)( char *pC, int iMaxLen );
                    wxDYNLIB_FUNCTION( DSGETSECTYPES, dsGetSecTypes, *pdllUpload );

                    if( pfndsGetPriTypes )
                    {
                        (*pfndsGetPriTypes)(pBuf, iLen );
                        wxString    strT( pBuf, wxConvUTF8 );
                        eData.strPri = strT;
                    }

                    if( pfndsGetSecTypes )
                    {
                        (*pfndsGetSecTypes)(pBuf, iLen );
                        wxString    strT( pBuf, wxConvUTF8 );
                        eData.strSec = strT;
                    }
                    free( pBuf );

                    typedef int (WINAPI *DSRELPRI)( void );
                    wxDYNLIB_FUNCTION( DSRELPRI, dsGetRelPri, *pdllUpload );

                    eData.iRelPri = (*pfndsGetRelPri)( );

                    eData.fUsed = false;
                    m_saExtData.push_back( eData );
                }
            }
//        if( fLoaded )
//            pdllUpload->Unload( );
        }

        fMore = dir.GetNext( &fileName );
    }

    return true;
}


//*****************************************************************************

bool DocShareView::Upload( wxString& strDocId, bool bPresentation )
{
    wxLogDebug( wxT("entering DocShareView::Upload( )") );

    int         i, j, iLastPri;
    int         iMinPri, iMinIdx;
    wxString    strT, strStripped, strFilters;

    wxString    strAllFiles( _("All Files"), wxConvUTF8 );
    wxString    strBar( wxT("|") );
    wxString    strDelim( wxT("--------------------|*.*|") );
    wxString    strStarDotStar( wxT(" (*.*)|*.*|") );

    //  Loop over all the Shared libraries/DLLs and get their filter strings
    FindUploadExts( );

    //  Order the filter strings for the dialog
    m_saFilterData.clear( );
    for( i=0, iLastPri=0; i<(int) m_saExtData.size( ); i++ )
    {
        int                     iCnt;
        struct _UploadExtData   eData;
        iMinPri = 1000000000;
        iMinIdx = -1;

        //  Find lowest Priority that hasn't already been used
        for( j=0; j<(int) m_saExtData.size( ); j++ )
        {
            eData = m_saExtData.at( j );
            if( eData.fUsed )
                continue;

            if( eData.iRelPri < iMinPri  &&  eData.iRelPri > iLastPri )
            {
                iMinPri = eData.iRelPri;
                iMinIdx = j;
            }
        }
        if( iMinIdx  ==  -1 )
            break;

        eData = m_saExtData.at( iMinIdx );
        if( eData.fUsed )
            continue;
        eData.fUsed = true;
        iLastPri = eData.iRelPri;

        struct _UploadFilterData    fData;
        fData.strDLLName = eData.strDLLName;
        fData.pdllUpload = eData.pdllUpload;
        iCnt = strFilters.Replace( strBar, strBar );     //  returns count of "|" chars
        iCnt = (int) ( iCnt / 2 );
        fData.iStart = (i == 0) ? 0 : iCnt;

        //  Add the Primary filters
        strT = eData.strPri;
        if( strT.StartsWith( strBar, &strStripped ) )
            strT = strStripped;
        if( strT.EndsWith( strBar, &strStripped ) )
            strT = strStripped;

        if( !strT.IsEmpty( ) )
        {
            if( !strFilters.IsEmpty( ) )
            {
                if( !strFilters.EndsWith( strBar ) )
                    strFilters.Append( strBar );
                strFilters.Append( strDelim );
            }
            strFilters.Append( strT );
            strFilters.Append( strBar );
        }

        //  Add the Secondary filters
        strT = eData.strSec;
        if( strT.StartsWith( strBar, &strStripped ) )
            strT = strStripped;
        if( strT.EndsWith( strBar, &strStripped ) )
            strT = strStripped;

        if( !strT.IsEmpty( ) )
        {
            if( !strFilters.IsEmpty( ) )
            {
                if( !strFilters.EndsWith( strBar ) )
                    strFilters.Append( strBar );
//                strFilters.Append( strDelim );
            }
            strFilters.Append( strT );
            strFilters.Append( strBar );
        }

        iCnt = strFilters.Replace( strBar, strBar );     //  returns count of "|" chars
        iCnt = (int) ( iCnt / 2 );
        fData.iEnd = iCnt;

        m_saFilterData.push_back( fData );
    }
    wxLogDebug( wxT("Loaded %d doc filters"), m_saExtData.size( ) );

    strFilters.Append( strAllFiles );
    strFilters.Append( strStarDotStar );

    if( !strFilters.EndsWith( strBar ) )
        strFilters.Append( strBar );
    strFilters.Append( strBar );


    bool        rc          = false;
    wxString    strCaption( _("Upload Document"), wxConvUTF8 );
    wxString    strDefaultDir, strDefaultFilename;

    wxDynamicLibrary    *pdllUpload     = NULL;

#ifndef WIN32
    if( strFilters.EndsWith( strBar ) )
        strFilters.RemoveLast( );
    if( strFilters.EndsWith( strBar ) )
        strFilters.RemoveLast( );
#endif
    wxFileDialog    dlg( m_pCanvas, strCaption );
    dlg.SetWildcard( strFilters );
    if( dlg.ShowModal( )  ==  wxID_OK )
    {
        CUtils::MsgWait( 10, 1 );	// avoid stuck dialog bkgnd

        wxString    strDirectory    = dlg.GetDirectory( );
        wxString    strFileName     = dlg.GetFilename( );
        wxString    strPath         = dlg.GetPath( );

        wxFileName  fName( strPath, strFileName );
        wxString    strExt = wxT(".") +  fName.GetExt( ).Lower( );
        strDocId = fName.GetName( );

        int pos = strFilters.Find(strExt);
        if (pos)
        {
            // Use position of extension in filter string to figure out which upload dll to use
            wxString temp = strFilters.Right(pos);
            j = temp.Replace( strBar, strBar ) / 2; // count delims, 2 per filter entry

            for( i=0; i<(int) m_saFilterData.size( ); i++ )
            {
                struct _UploadFilterData    fData;
                fData = m_saFilterData.at( i );
                if( j >= fData.iStart  &&  j < fData.iEnd )
                {
                    pdllUpload = fData.pdllUpload;
                    break;
                }
            }
        }

        if ( !pdllUpload )
        {
            wxString    strT( _("Unable to share the selected document.\nThe selected file type is not supported."), wxConvUTF8 );
            wxString    strTitle = PRODNAME;
            wxMessageDialog dlg( m_pCanvas, strT, strTitle, wxOK | wxICON_EXCLAMATION );
            dlg.ShowModal( );
                return false;
        }

        rc = SendDocShareFile( pdllUpload, strPath, strDocId );
        if( rc )
        {
            strFileName.MakeLower( );
            if( !strFileName.IsEmpty( ) )
            {
                if( IsPresenter( ) )
                {
                    // someone changed the document during uploading
//                    if (m_bDirty)
//                    {
//                        int ret  = CUtils::DisplayMessage(m_hWnd, IDS_IIC_PLEASECONFIRM, IDS_IIC_PROMPT_SAVEDOC, MB_YESNOCANCEL|MB_ICONQUESTION);
//                        if (ret == IDYES || ret == IDNO)
//                        {
//                            if (!UpdatePageMarkup(true, ret==IDYES?true:false))
//                                return false;
//                        }
//                        else	// in case someone else end the meeting
//                        {
//                            return false;
//                        }
//                    }
//                    else	// unlock it
//                    {
                        UpdatePageMarkup(true, false);
//                    }

                    return true;
                }
                rc = true;
            }
        }
    }
    
    return rc;
}

#else

bool DocShareView::Upload( wxString& strDocId, bool bPresentation )
{
    wxLogDebug( wxT("entering DocShareView::Upload( )") );

    // "BMP files (*.bmp)|*.bmp|GIF files (*.gif)|*.gif"
    
    wxString    strT, strFilters;

    wxString    strPresentationFiles( _("Presentation files"), wxConvUTF8 );
    wxString    strDocumentFiles( _("Document files"), wxConvUTF8 );
    wxString    strSpreadsheetFiles( _("Speadsheet files"), wxConvUTF8 );
    wxString    strImageFiles( _("Image files"), wxConvUTF8 );
    wxString    strPDFFiles( _("PDF files"), wxConvUTF8 );
    wxString    strAllFiles( _("All Files"), wxConvUTF8 );
    wxString    strBar( wxT("|") );
    wxString    strDelim( wxT("--------------------|*.*|") );
    wxString    strPPT( wxT(" (*.ppt;*.pptx;*.odp)|*.ppt;*.pptx;*.odp") );
    wxString    strDocs( wxT(" (*.doc;*.docx;*.odt)|*.doc;*.docx;*.odt") );
    wxString    strSheets( wxT(" (*.xls;*.xlsx,*.ods)|*.xls;*.xlsx;*.ods") );
    wxString    strImages( wxT(" (*.jpg;*.jpeg;*.png;*.bmp)|*.jpg;*.jpeg;*.png;*.bmp") );
    wxString    strPDF( wxT(" (*.pdf)|*.pdf") );
    wxString    strStarDotStar( wxT(" (*.*)|*.*|") );

    strFilters.Append( strPresentationFiles );
    strFilters.Append( strPPT );
    strFilters.Append( strBar );

    strFilters.Append( strDocumentFiles );
    strFilters.Append( strDocs );
    strFilters.Append( strBar );

    strFilters.Append( strSpreadsheetFiles );
    strFilters.Append( strSheets );
    strFilters.Append( strBar );

    strFilters.Append( strPDFFiles );
    strFilters.Append( strPDF );
    strFilters.Append( strBar );

    strFilters.Append( strImageFiles );
    strFilters.Append( strImages );
    strFilters.Append( strBar );

    strFilters.Append( strAllFiles );
    strFilters.Append( strStarDotStar );

    if( !strFilters.EndsWith( strBar ) )
        strFilters.Append( strBar );
    strFilters.Append( strBar );

    bool        rc          = false;
    wxString    strCaption( _("Upload Document"), wxConvUTF8 );
    wxString    strDefaultDir, strDefaultFilename;

    wxFileDialog    dlg( m_pCanvas, strCaption );
    dlg.SetWildcard( strFilters );
    
    if ( bPresentation )
        dlg.SetFilterIndex(0);
    else
        dlg.SetFilterIndex(1);
    
    if( dlg.ShowModal( )  ==  wxID_OK )
    {
        CUtils::MsgWait( 10, 1 );	// avoid stuck dialog bkgnd

        wxString    strDirectory    = dlg.GetDirectory( );
        wxString    strFileName     = dlg.GetFilename( );
        wxString    strPath         = dlg.GetPath( );

        wxFileName  fName( strPath, strFileName );
        strDocId = fName.GetName( );
        wxString    strExt = fName.GetExt( ).Lower( );

#ifdef WIN32
        if (PREFERENCES.m_fUsePowerPoint && (strExt == wxT("ppt") || strExt == wxT("pptx"))) 
        {
            wxDynamicLibrary* pdll = LoadLocalConverter( strExt );
            rc = ConvertDocShareFile( pdll, strPath, strDocId );
        }
        else
#endif
            rc = SendDocShareFile( strPath, strDocId );
        if( rc )
        {
            strFileName.MakeLower( );
            if( !strFileName.IsEmpty( ) )
            {
                if( IsPresenter( ) )
                {
					// Necessary?
                    UpdatePageMarkup(true, false);
                    return true;
                }
                rc = true;
            }
        }
    }
    
    return rc;
}

#endif

//*****************************************************************************
bool DocShareView::LockCurrentPage( )
{
    bool        rc          = true;

    if( !m_bFileLocked  &&  IsPresenter( )  &&  !m_strDocId.IsEmpty( )  &&  m_saPages.size( ) > 0 )     // lock the file again
    {
        wxString    strTargetFileURL(m_pDocument->m_Meeting.GetDocShareServer());
        strTargetFileURL += m_strMeetingID;
        strTargetFileURL += wxT("/");
        strTargetFileURL += m_strDocId;
        strTargetFileURL += wxT('/');
        strTargetFileURL += m_saPages.at(m_nPageIndex).m_strFile+_T(".xml");

        wxString    strTargetFilePath(m_pDocument->m_Meeting.GetDocSharePath());
        strTargetFilePath += m_strMeetingID;
        strTargetFilePath += wxT("/");
        strTargetFilePath += m_strDocId;
        strTargetFilePath += wxT('/');
        strTargetFilePath += m_saPages.at(m_nPageIndex).m_strFile+_T(".xml");

        long hr = LockFile(strTargetFileURL, strTargetFilePath, true, m_strLockToken, m_strLastModified);
        if (FAILED(hr))
        {
            wxString errmsg( _("Unable to lock/unlock file, please try again later."), wxConvUTF8 );
            CUtils::ReportErrorMsg( hr, true, errmsg, m_pCanvas );
            rc = false;
        }
        else
        {
            m_bFileLocked = true;
        }
    }

    return rc;
}

//*****************************************************************************
wxDynamicLibrary *DocShareView::LoadLocalConverter( wxString strExt )
{
    wxString    strExePath( wxStandardPaths::Get().GetExecutablePath() );
    wxFileName  wxFN( strExePath );
    wxString    strPath( wxFN.GetPath( ) );
    strPath.Append( wxT("/docshare/ms_ppt.dll") );
    
    wxDynamicLibrary    *pdllUpload;
    pdllUpload = new wxDynamicLibrary( );
    pdllUpload->Load( strPath );

    return pdllUpload;
}

//*****************************************************************************
//  Use appropriate filter to convert file to images and upload images to server
//  strDocId - (out) Returns name of file that was just uploaded that is to be opened to share
int DocShareView::ConvertDocShareFile( const wxDynamicLibrary *pdllUpload, const wxString &strFilePath, wxString &strDocId )
{
    bool        fRet;
    int         iRet;
    int         i;
    
    wxString    strT;

    unsigned long ulHWnd;
    wxPanel     panelT( m_pCanvas );
    panelT.Hide( );
    ulHWnd = (unsigned long) panelT.GetHandle( );

    bool                fLoaded;
    fLoaded = pdllUpload->IsLoaded( );
    if( !fLoaded )
        return 0;

    fRet = pdllUpload->HasSymbol( wxT("dsGetStatus") );
    if( !fRet )
        return 0;

    wxString    strProg( _("Preparing"), wxConvUTF8 );
    wxString    strProg2( _("Preparing document to send...please wait."), wxConvUTF8 );
    ShowProgressWindow( strProg, strProg2 );
    m_pDialogProgress->StartAutoProgress( 500 );

    typedef int (WINAPI *DSABORT)( void );
    wxDYNLIB_FUNCTION( DSABORT, dsAbort, *pdllUpload );

    typedef int (WINAPI *DSSTATUS)( void );
    wxDYNLIB_FUNCTION( DSSTATUS, dsGetStatus, *pdllUpload );

    typedef int (WINAPI *DSPRELOADEXECMDLEN)( void );
    wxDYNLIB_FUNCTION( DSPRELOADEXECMDLEN, dsGetPreLoadExeCmdLen, *pdllUpload );

    typedef int (WINAPI *DSPRELOADEXECMD)( char *pC, int iMaxLen );
    wxDYNLIB_FUNCTION( DSPRELOADEXECMD, dsGetPreLoadExeCmd, *pdllUpload );

    typedef int (WINAPI *DSPOSTLOADKILLCMDLEN)( void );
    wxDYNLIB_FUNCTION( DSPOSTLOADKILLCMDLEN, dsGetPostLoadKillCmdLen, *pdllUpload );

    typedef int (WINAPI *DSPOSTLOADKILLCMD)( char *pC, int iMaxLen );
    wxDYNLIB_FUNCTION( DSPOSTLOADKILLCMD, dsGetPostLoadKillCmd, *pdllUpload );

    typedef int (WINAPI *DSPRELOAD)( void );
    wxDYNLIB_FUNCTION( DSPRELOAD, dsPreLoad, *pdllUpload );

    typedef int (WINAPI *DSPOSTLOAD)( void );
    wxDYNLIB_FUNCTION( DSPOSTLOAD, dsPostLoad, *pdllUpload );

    typedef int (WINAPI *DSLOAD)( const char *pFN, unsigned long ulHWnd );
    wxDYNLIB_FUNCTION( DSLOAD, dsLoad, *pdllUpload );

    typedef int (WINAPI *DSNUMPAGES)( void );
    wxDYNLIB_FUNCTION( DSNUMPAGES, dsGetNumPages, *pdllUpload );

    typedef int (WINAPI *DSTEMPPATH)( char *pC, int iMaxLen );
    wxDYNLIB_FUNCTION( DSTEMPPATH, dsGetTempPath, *pdllUpload );

    typedef int (WINAPI *DSSETTEMPPATH)( const char *pC );
    wxDYNLIB_FUNCTION( DSSETTEMPPATH, dsSetTempPath, *pdllUpload );

    // Set temporary directory
    wxString strDataPath( wxStandardPaths::Get().GetUserLocalDataDir() );
    pfndsSetTempPath( strDataPath.mb_str() );

    //  See if there is a PreLoadExeCmd to execute
    m_lPreLoadExeHnd = 0;
    int     iPreLoadCmdLen  = (*pfndsGetPreLoadExeCmdLen)( );
    if( iPreLoadCmdLen  >  0 )
    {
        char    *pBuf   = (char *) malloc( iPreLoadCmdLen + 1 );
        iRet = (*pfndsGetPreLoadExeCmd)( pBuf, iPreLoadCmdLen + 1 );
        if( iRet  >  0 )
        {
            wxString    strCmd( pBuf, wxConvUTF8 );
            m_lPreLoadExeHnd = ::wxExecute( strCmd );
#ifdef WIN32
            CUtils::MsgWait( 200, 10 );     //  2 second delay
#else
            CUtils::MsgWait( 200, 50 );     //  10 second delay
#endif
            bool            fRet2;
            fRet2 = wxProcess::Exists( m_lPreLoadExeHnd );
        }
        free( pBuf );
        m_pDialogProgress->Raise( );
    }

    m_strPostLoadKillCmd.Empty( );
    int     iPostLoadCmdLen  = (*pfndsGetPostLoadKillCmdLen)( );
    if( iPostLoadCmdLen  >  0 )
    {
        char    *pBuf   = (char *) malloc( iPostLoadCmdLen + 1 );
        iRet = (*pfndsGetPostLoadKillCmd)( pBuf, iPostLoadCmdLen + 1 );
        if( iRet  >  0 )
        {
            wxString    strCmd( pBuf, wxConvUTF8 );
            m_strPostLoadKillCmd.Append( strCmd );
        }
        free( pBuf );
    }

    iRet = (*pfndsPreLoad)( );
    for( i=0; i<6; i++ )
    {
        CUtils::MsgWait( 100, 5 );
        if( (*pfndsGetStatus)( ) )
        {
            break;
        }
    }

    //  Load the file and export all slides
    (*pfndsLoad)( strFilePath.mb_str( wxConvUTF8 ), ulHWnd );

    //  Wait until the exports are completed
    while( ! pfndsGetStatus() )
    {
        if( m_pDialogProgress->fCanceled( ) )
            (*pfndsAbort)( );

        m_pDialogProgress->Raise( );

        CUtils::MsgWait( 100, 5 );
    }

    (*pfndsPostLoad)( );
    for( i=0; i<6; i++ )
    {
        CUtils::MsgWait( 100, 5 );
        if( (*pfndsGetStatus)( ) )
        {
            break;
        }
    }

    //  Determine num of pages exported and folder where they were put
    int iNumPages;
    iNumPages = (*pfndsGetNumPages)( );

    char    pathBuf[ 512 ];
    iRet = (*pfndsGetTempPath)( pathBuf, 511 );

    wxString    strPath2( pathBuf, wxConvUTF8 );
    wxString    strPath3;
    if( !strPath2.StartsWith( wxT("file:///"), &strPath3 ) )
        strPath3 = strPath2;
#ifdef LINUX
    strPath3.Prepend( wxT("/") );
#endif

    if( !m_strPostLoadKillCmd.IsEmpty( ) )
    {
#ifdef WIN32
        KillNamedProcess( m_strPostLoadKillCmd.mb_str( wxConvUTF8 ) );
#else
        wxKillError     killErr;
        iRet = ::wxKill( m_lPreLoadExeHnd, wxSIGTERM, &killErr );
#endif
        m_strPostLoadKillCmd.Empty( );
    }
    
    if( m_pDialogProgress->fCanceled( ) )
    {
        HideProgressWindow( );
        return 0;
    }

    // If no pages were generated, we'll assume there was an error.
    if (iNumPages == 0)
    {
        HideProgressWindow( );
        
        wxString    strT( _("Unable to share the selected document.\nCan not convert document to images."), wxConvUTF8 );
        wxString    strTitle = PRODNAME;
        wxMessageDialog dlg( m_pCanvas, strT, strTitle, wxOK | wxICON_EXCLAMATION );
        dlg.ShowModal( );
        
        return 0;
    }

    return SendProcessedFile( strFilePath, strDocId, iNumPages, strPath3 );
}

int DocShareView::SendProcessedFile( const wxString &strFilePath, wxString &strDocId, int iNumPages, wxString &strProcessedPath )
{
    bool        fRet;
    
    m_pDialogProgress->Raise( );

    wxString    errmsg( _("Failed to upload document, please try it again later."), wxConvUTF8 );

    wxFileName  wxfileName( strFilePath );
    fRet = wxfileName.IsOk( );
    fRet = wxfileName.HasName( );
    wxString    strName;
    strName = wxfileName.GetName( );

    wxString    strStatusCode, strStatusText;

    wxString    strTargetFileURL(m_pDocument->m_Meeting.GetDocShareServer());

    wxString    strTargetFilePath(m_pDocument->m_Meeting.GetDocSharePath());
    strTargetFilePath += m_strMeetingID;
    strTargetFilePath += _T("/");

    wxString    strTempURL  = strTargetFileURL;	// upload to this temp dir first
    wxString    strTempPath = strTargetFilePath + gTempUploadDir;
    int         hr;

    wxString    strProg3( _("Sending"), wxConvUTF8 );
    wxString    strProg4( _("Sending request...please wait."), wxConvUTF8 );
    m_pDialogProgress->StopAutoProgress( );
    ShowProgressWindow( strProg3, strProg4 );

    // Create directory
    hr = AsyncSendRequest( wxT("MKCOL"), strTargetFileURL, strTargetFilePath, NULL, 0,
                           strStatusCode, strStatusText, false, m_strLockToken, true );
    if( FAILED(hr) )
    {
        if( !m_pDialogProgress->fCanceled( ) )
        {
            long    lCode;
            strStatusCode.ToLong( &lCode );
            CUtils::ReportDAVError( m_pCanvas, lCode, strStatusText,
                                    wxT("%s\n%s\n%s"), errmsg, hr );
        }
        HideProgressWindow( );
        return hr;
    }

    //  Ensure the temp folder exists
    hr = AsyncSendRequest( wxT("MKCOL"), strTempURL, strTempPath, NULL, 0,
                           strStatusCode, strStatusText, false, m_strLockToken, true );

    strTargetFileURL += strName;
    strTargetFileURL += _T('/');

    wxString strT = strName;
    CUtils::EncodeStringFully( strT );
    strTargetFilePath += strT;
    strTargetFilePath += _T('/');

    //  Create the destination directory
    hr = AsyncSendRequest( _T("MKCOL"), strTargetFileURL, strTargetFilePath, NULL, 0,
                           strStatusCode, strStatusText, false, m_strLockToken, true );
    if( FAILED(hr) )
    {
        if( !m_pDialogProgress->fCanceled( ) )
        {
            long    lCode;
            strStatusCode.ToLong( &lCode );
            CUtils::ReportDAVError( m_pCanvas, lCode, strStatusText,
                                    wxT("%s\n%s\n%s"), errmsg, hr );
        }
        HideProgressWindow( );
        return hr;
    }

    if (strStatusCode == wxT("405"))	// folder already exists - copy its contents to temp dir first, cause DAV COPY will delete every first
    {
        std::string xml;
        wxString    strTargetURL( m_pDocument->m_Meeting.GetDocShareServer( ) );
        wxString    strSourcePath( m_pDocument->m_Meeting.GetDocSharePath( ) );

        strTargetURL += strSourcePath;
        strTargetURL += m_strMeetingID;
        strTargetURL += _T("/");
        strTargetURL += gTempUploadDir;

        strSourcePath += m_strMeetingID;
        strSourcePath += _T('/');
        strSourcePath += strT;
        strSourcePath += _T('/');
        hr = m_WebDavUtils.DAVCopyFolder( m_pCanvas, strTempURL, strSourcePath, strTargetURL, m_strUserName, m_strPasswd, xml, strStatusCode, strStatusText);
        if( FAILED(hr)  ||  (strStatusCode != _T("204") && strStatusCode != _T("201") ) )
        {
            long    lCode;
            strStatusCode.ToLong( &lCode );
            CUtils::ReportDAVError( m_pCanvas, lCode, strStatusText,
                                    wxT("%s\n%s\n%s"), errmsg, hr);
        }
    }

    CUtils::MsgWait( 10, 1 );   // Dispatch msgs so that Progress Dialog will display

    if( hr == S_OK  &&  !m_pDialogProgress->fCanceled( ) )
    {
        // Upload index file
        wxString    strT( strProcessedPath );
        strT.Append( wxT("/index.xml") );

        wxString    targetFile = strTempURL;
        targetFile += wxT("index.xml");
        wxString    targetPath = strTempPath;
        targetPath += wxT("index.xml");
                                
        hr = AsyncSending(targetFile, targetPath, strT, strStatusCode, strStatusText);

        if( SUCCEEDED(hr) )
        {
            // Upload source file
            wxFileName fn( strFilePath );

            targetFile = strTempURL + wxT("original.") + fn.GetExt();
            targetPath = strTempPath + wxT("original.") + fn.GetExt();
            
            AsyncSending(targetFile, targetPath, strFilePath, strStatusCode, strStatusText);

            // Upload generated images
            hr = SendDirectory( strProcessedPath, strDocId, iNumPages, TRUE );
        }
        else
        {
            if( !m_pDialogProgress->fCanceled( ) )
            {
                long    lCode;
                strStatusCode.ToLong( &lCode );
                CUtils::ReportDAVError( m_pCanvas, lCode, strStatusText,
                                       wxT("%s\n%s\n%s"), errmsg, hr );
            }
        }
    }

    HideProgressWindow( );

    return( (hr == S_OK) ? 1 : 0 );
}


//*****************************************************************************
int DocShareView::SendDirectory( wxString &aDirectory, wxString &aDocId, int iNumFiles, bool aDeleteAfterSent )
{
    int         hr;
    wxString    errmsg( _("Failed to upload document, please try it again later."), wxConvUTF8 );

    wxString    strDirectory = aDirectory;
    if( !strDirectory.EndsWith( wxT("/") ) )
        strDirectory += wxT("/");

    wxString    strStatusCode, strStatusText;
    wxString    strTargetFileURL( m_pDocument->m_Meeting.GetDocShareServer( ) );

    wxString    strTargetFilePath( m_pDocument->m_Meeting.GetDocSharePath( ) );
    strTargetFilePath += m_strMeetingID;
    strTargetFilePath += wxT("/");

    wxString    strTempURL  = strTargetFileURL;	// upload to this temp dir first
    wxString    strTempPath = strTargetFilePath + gTempUploadDir;

    wxString    strT( aDocId );
    CUtils::EncodeStringFully( strT );

    strTargetFileURL += strT;
    strTargetFileURL += wxT("/");

    strTargetFilePath += strT;
    strTargetFilePath += wxT("/");

    m_pDialogProgress->StopAutoProgress( );

    m_pDialogProgress->SetProgressRange( iNumFiles * 2 );
    int         nCount  = 0;
    wxArrayString    arrExts;
    arrExts.Add( wxT("BMP") );
    arrExts.Add( wxT("JPG") );
    arrExts.Add( wxT("JPEG") );
    arrExts.Add( wxT("PDF") );
    arrExts.Add( wxT("PNG") );
    arrExts.Add( wxT("TIF") );
    arrExts.Add( wxT("TIFF") );
    arrExts.Add( wxT("EMF") );
    arrExts.Add( wxT("EMZ") );

    while( SUCCEEDED(hr)  &&  nCount < (int)arrExts.GetCount( ) )
    {
        hr = SendFilesWithExt( strDirectory, strTempURL, strTempPath, aDeleteAfterSent, arrExts[ nCount++ ] );
    }

    if( SUCCEEDED(hr) )
    {
        //  Copy the contents of the temp directory to the destination directory
        std::string     xml;
        wxString    strTargetURL( m_pDocument->m_Meeting.GetDocShareServer( ) );
        if( strTargetURL.EndsWith( wxT("/") )  &&  strTargetFilePath.StartsWith( wxT("/") ) )
            strTargetURL.RemoveLast( );
        strTargetURL += strTargetFilePath;

        hr = m_WebDavUtils.DAVCopyFolder( m_pCanvas, strTempURL, strTempPath, strTargetURL, m_strUserName, m_strPasswd, xml, strStatusCode, strStatusText );
        if( FAILED(hr)  ||  (strStatusCode != wxT("204") && strStatusCode != wxT("201")) )
        {
            long    lCode;
            strStatusCode.ToLong( &lCode );
            CUtils::ReportDAVError( m_pCanvas, lCode, strStatusText,
                                   wxT("%s\n%s\n%s"), errmsg, hr);
        }
    }

    //  Time to cleanup - empty out the temp folder
    hr = AsyncSendRequest( wxT("DELETE"), strTempURL, strTempPath, NULL, 0, strStatusCode, strStatusText, false, wxT(""), true );

    HideProgressWindow( );

    return hr;
}


//*****************************************************************************
int DocShareView::SendFilesWithExt( wxString &strDirectory, wxString &strTempURL, wxString &strTempPath, bool aDeleteAfterSent, wxString strExt )
{
    wxLogDebug( wxT("entering DocShareView::SendFilesWithExt( ) - strDirectory = %s - strExt = %s"), strDirectory.wc_str( ), strExt.wc_str( ) );

    int         hr = S_OK;
    wxString    strStatusCode, strStatusText;
    wxString    errmsg( _("Failed to upload document, please try it again later."), wxConvUTF8 );

    if (SUCCEEDED(hr))
    {
        wxArrayString removeFiles;
        wxString    fileName;
        wxDir       dir( strDirectory );

        if( dir.IsOpened( ) )
        {
            bool    fMore = dir.GetFirst( &fileName, wxEmptyString, wxDIR_FILES | wxDIR_HIDDEN );
            while( fMore )
            {
                if( strExt.CmpNoCase( fileName.Right( strExt.Len( ) ) )  ==  0 )
                {
                    wxString    targetFile = strTempURL;
                    wxString    targetPath = strTempPath;
                    targetPath += fileName;
                    wxString    dirPlusFN;
                    dirPlusFN = strDirectory + fileName;

                    hr = AsyncSending( targetFile, targetPath, dirPlusFN, strStatusCode, strStatusText, true );
                    if( hr != S_OK )
                    {
                        long    lCode;
                        strStatusCode.ToLong( &lCode );
                        CUtils::ReportDAVError( m_pCanvas, lCode, strStatusText, wxT("%s\n%s\n%s"), errmsg, hr );
                        break;
                    }

                    if( aDeleteAfterSent )
                    {
                        removeFiles.Add( dirPlusFN );
                    }

                    m_pDialogProgress->IncrementProgressPos( );
                }
                fMore = dir.GetNext( &fileName );
            }
            
            for (unsigned i = 0; i < removeFiles.GetCount(); i++)
            {
                ::wxRemoveFile( removeFiles[i] );
            }
        }
    }
    return hr;
}


int DocShareView::SendDocShareFile( const wxString &strFilePath, wxString &strDocId )
{
    bool        fRet;
    
    wxString    errmsg( _("Failed to upload document, please try it again later."), wxConvUTF8 );

    wxFileName  wxfileName( strFilePath );
    fRet = wxfileName.IsOk( );
    fRet = wxfileName.HasName( );
    wxString    strName;
    strName = wxfileName.GetName( );

    wxString    strStatusCode, strStatusText;

    wxString    strTargetFileURL(m_pDocument->m_Meeting.GetDocShareServer());

    wxString    strTargetFilePath(m_pDocument->m_Meeting.GetDocSharePath());
    strTargetFilePath += m_strMeetingID;
    strTargetFilePath += _T("/");

    wxString    strTempURL  = strTargetFileURL;	// upload to this temp dir first
    wxString    strTempPath = strTargetFilePath + gTempUploadDir;
    int         hr;

    wxString    strProg1( _("Sending"), wxConvUTF8 );
    wxString    strProg2( _("Sending request...please wait."), wxConvUTF8 );
    ShowProgressWindow( strProg1, strProg2 );

	m_pDialogProgress->SetProgressRange( 3 );

    // Create directory
    hr = AsyncSendRequest( wxT("MKCOL"), strTargetFileURL, strTargetFilePath, NULL, 0,
                           strStatusCode, strStatusText, false, m_strLockToken, true );
    if( FAILED(hr) )
    {
        if( !m_pDialogProgress->fCanceled( ) )
        {
            long    lCode;
            strStatusCode.ToLong( &lCode );
            CUtils::ReportDAVError( m_pCanvas, lCode, strStatusText,
                                    wxT("%s\n%s\n%s"), errmsg, hr );
        }
        HideProgressWindow( );
        return 0;
    }
    m_pDialogProgress->IncrementProgressPos( );

    //  Ensure the temp folder exists
    hr = AsyncSendRequest( wxT("MKCOL"), strTempURL, strTempPath, NULL, 0,
                           strStatusCode, strStatusText, false, m_strLockToken, true );

    strTargetFileURL += strName;
    strTargetFileURL += _T('/');

    wxString strT = strName;
    CUtils::EncodeStringFully( strT );
    strTargetFilePath += strT;
    strTargetFilePath += _T('/');

    //  Create the destination directory
    hr = AsyncSendRequest( _T("MKCOL"), strTargetFileURL, strTargetFilePath, NULL, 0,
                           strStatusCode, strStatusText, false, m_strLockToken, true );
    if( FAILED(hr) )
    {
        if( !m_pDialogProgress->fCanceled( ) )
        {
            long    lCode;
            strStatusCode.ToLong( &lCode );
            CUtils::ReportDAVError( m_pCanvas, lCode, strStatusText,
                                    wxT("%s\n%s\n%s"), errmsg, hr );
        }
        HideProgressWindow( );
        return 0;
    }
    m_pDialogProgress->IncrementProgressPos( );

    if (strStatusCode == wxT("405"))	// folder already exists - copy its contents to temp dir first, cause DAV COPY will delete every first
    {
        std::string xml;
        wxString    strTargetURL( m_pDocument->m_Meeting.GetDocShareServer( ) );
        wxString    strSourcePath( m_pDocument->m_Meeting.GetDocSharePath( ) );

        strTargetURL += strSourcePath;
        strTargetURL += m_strMeetingID;
        strTargetURL += _T("/");
        strTargetURL += gTempUploadDir;

        strSourcePath += m_strMeetingID;
        strSourcePath += _T('/');
        strSourcePath += strT;
        strSourcePath += _T('/');
        hr = m_WebDavUtils.DAVCopyFolder( m_pCanvas, strTempURL, strSourcePath, strTargetURL, m_strUserName, m_strPasswd, xml, strStatusCode, strStatusText);
        if( FAILED(hr)  ||  (strStatusCode != _T("204") && strStatusCode != _T("201") ) )
        {
            long    lCode;
            strStatusCode.ToLong( &lCode );
            CUtils::ReportDAVError( m_pCanvas, lCode, strStatusText,
                                    wxT("%s\n%s\n%s"), errmsg, hr);
        }
    }

    CUtils::MsgWait( 10, 1 );   // Dispatch msgs so that Progress Dialog will display
	
	// These use wxURI internally, which in 2.8 doesn't encode extended/unicode characters in the most
	// sane way.  However, if we pass the same name to the conversion server it should be able to find the doc.
	wxFileName fn( strFilePath );
	wxString encodedName = fn.GetFullName();
	CUtils::EncodeStringFully(encodedName);
	wxString encodedDocId = strDocId;
	CUtils::EncodeStringFully(encodedDocId);

    if( hr == S_OK  &&  !m_pDialogProgress->fCanceled( ) )
    {
		wxString targetFile = strTargetFileURL + encodedName;
		wxString targetPath = strTargetFilePath + encodedName;
		
		hr = AsyncSending(targetFile, targetPath, strFilePath, strStatusCode, strStatusText);
    }
    m_pDialogProgress->IncrementProgressPos( );
	
	if ( SUCCEEDED(hr) && !m_pDialogProgress->fCanceled( ) )
	{
		m_iConversionResult = -1;
		m_iConversionPage = -1;
		
		// Make RPC call...
		session_make_rpc_call( CONTROLLER.session, "rpc_convert", "converter", "converter.convert_document", "(ssss)",
							   "", (const char *)m_strMeetingID.mb_str( wxConvUTF8 ), (const char *)encodedDocId.mb_str( wxConvUTF8 ), 
							   (const char *) encodedName.mb_str( wxConvUTF8 ) );
	}
							  
	hr = WaitForConversion( strFilePath, strDocId );

    HideProgressWindow( );

	if (hr != S_OK) 
	{
        wxString    strT( _("Unable to share the selected document.\nCan not convert document to images."), wxConvUTF8 );
        wxString    strTitle = PRODNAME;
        wxMessageDialog dlg( m_pCanvas, strT, strTitle, wxOK | wxICON_EXCLAMATION );
        dlg.ShowModal( );
	}
	
    return( (hr == S_OK) ? 1 : 0 );
}

int DocShareView::WaitForConversion( const wxString &strFilePath, wxString &strDocId )
{
    wxString    strProg( _("Processing"), wxConvUTF8 );
    wxString    strProg2( _("Preparing uploaded document...please wait."), wxConvUTF8 );
    ShowProgressWindow( strProg, strProg2 );
    m_pDialogProgress->StartAutoProgress( 500 );

    int lastPage = -1;
    
	while (1)
	{
		CUtils::MsgWait( 150, 3 );	
		if( m_pDialogProgress->fCanceled( ) ) {
			return E_FAIL;
		}
		if (m_iConversionResult != -1)
			break;

        if (lastPage != m_iConversionPage) 
        {
            if (lastPage == -1) 
            {
                m_pDialogProgress->StopAutoProgress( );
                m_pDialogProgress->SetProgressRange( 50 );
            }
            m_pDialogProgress->IncrementProgressPos( );
            wxString    cstrT = wxString::Format( _("Page %d completed"), m_iConversionPage + 1 );
            m_pDialogProgress->SetLabelTitle2( cstrT );
            lastPage = m_iConversionPage;
        }
	}

    m_pDialogProgress->StopAutoProgress( );
	HideProgressWindow( );

	return m_iConversionResult == 0 ? S_OK : E_FAIL;
}


//*****************************************************************************
void DocShareView::OnConversionEvent( wxCommandEvent& event )
{
    //  Someone just joined the meeting so update the whiteboard info
    //  on the server and force an update of all observers
    wxLogDebug( wxT("entering DocShareView::OnConversionEvent( %s )"), event.GetString().c_str() );
    
    if (event.GetString() == wxT("ConversionComplete") || event.GetString() == wxT("ConversionFailed")) 
    {
        m_iConversionResult = event.GetInt();
    }
    else if (event.GetString() == wxT("PageOutput"))
    {
        m_iConversionPage = event.GetInt();
    }
}

//*****************************************************************************
long DocShareView::CopyDocuments( wxWindow * parent, wxString &fromMeetingId, wxString &toMeetingId, bool fDeleteSource )
{
    long hr = 0;
    wxString strStatusCode, strStatusText;
    wxString  errmsg( _("Unable to move uploaded documents to new meeting. Please try uploading again."), wxConvUTF8 );
    
    wxString screenName, password, community;
    CONTROLLER.GetSignOnInfo( screenName, password, community );

    wxString strTempURL( m_pDocument->m_Meeting.GetDocShareServer( ) );
    wxString strTempPath( m_pDocument->m_Meeting.GetDocSharePath( ) );
    strTempPath += fromMeetingId + wxT("/");

    AllowHideProgress( false );

    if ( !toMeetingId.IsEmpty() )
    {
        wxString strTargetURL( m_pDocument->m_Meeting.GetDocShareServer( ) );
        wxString strTargetPath( m_pDocument->m_Meeting.GetDocSharePath( ) );
        strTargetPath += toMeetingId + wxT("/");

        if( strTargetURL.EndsWith( wxT("/") )  &&  strTargetPath.StartsWith( wxT("/") ) )
            strTargetURL.RemoveLast( );
        strTargetURL += strTargetPath;

        hr = AsyncSendRequest( wxT("MKCOL"), strTargetURL, strTargetPath, NULL, 0,
                                           strStatusCode, strStatusText, false, _T(""));
        if( !FAILED(hr) )
        {
            std::string xml;

            hr = m_WebDavUtils.DAVCopyFolder( parent, strTempURL, strTempPath, strTargetURL, screenName, password, xml, strStatusCode, strStatusText );
            if (strStatusCode != wxT("204") && strStatusCode != wxT("201"))
                hr = E_FAIL;
        }
    }

    //  Time to cleanup - empty out the temp folder
    if ( fDeleteSource )
        AsyncSendRequest( wxT("DELETE"), strTempURL, strTempPath, NULL, 0, strStatusCode, strStatusText, false, wxT(""), true );

    AllowHideProgress( true );
    
    if (FAILED(hr))
    {
        long    lCode;
        strStatusCode.ToLong( &lCode );
        CUtils::ReportDAVError( parent, lCode, strStatusText, wxT("%s\n%s\n%s"), errmsg, hr );
    }

    return hr;
}

//*****************************************************************************
void DocShareView::AllowHideProgress( bool fFlag )
{
    if( fFlag == false )
    {
        // Bit of a hack: treat request to not hide progress dialog as a sign of a new operation starting,
        // and clear cancel flag.
        if (m_pDialogProgress)
            m_pDialogProgress->ClearCancel();
        m_iAllowHideProgress++;
    }
    else
        m_iAllowHideProgress--;
    if( m_iAllowHideProgress  <=  0 )
    {
        m_iAllowHideProgress = 0;
        HideProgressWindow( );
    }
}

