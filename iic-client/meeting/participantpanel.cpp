/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 *
 *
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */
#include "app.h"
#include "participantpanel.h"

#include "iicdoc.h"
#include "docshareview.h"
#include "meetingcentercontroller.h"
#include "meetingmainframe.h"
#include "mainpanel.h"
#include "meetingsetupdialog.h"
#include "endmeetingdialog.h"
#include "services/common/eventnames.h"
#include "sharecontrol.h"
#include "selectapplication.h"
#include "wxzonoptions.h"

#include <wx/tooltip.h>
#include <wx/choicdlg.h>

#include "pcphonecontroller.h"

static int kStatusHandDown = 0;
static int kStatusHandUp   = 1;

static int kStatusMeetingUnknown = 0;
static int kStatusMeetingOffline = 1;
static int kStatusMeetingOnline  = 2;

static int kStatusPhoneOffline = 0;
static int kStatusPhoneOnline  = 1;
static int kStatusPhoneDialing = 2;
static int kStatusPhoneRinging = 3;
static int kStatusPhoneBusy    = 4;
static int kStatusPhoneTalking = 5;
static int kStatusPhoneMute	   = 6;
static int kStatusPhoneBroadcast = 7;

static int kStatusContactOffline      = 0;
static int kStatusContactOnline       = 1;
static int kStatusContactAway         = 2;
static int kStatusContactGoingOffline = 3;
static int kStatusContactComingOnline = 4;

// N.B. these had better match the menu order in the XRC file
static int kMenuIndexMyPhone        = 1;    // IDC_DLG_MTG_MENU_PHONE
static int kMenuIndexParticipants   = 2;    // IDC_DLG_MTG_MENU_PARTICIPANTS
static int kMenuIndexSubMeetings    = 3;    // IDC_DLG_MTG_MENU_SUBMEETINGS
static int kMenuIndexSharing        = 4;    // IDC_DLG_MTG_MENU_SHARING

//(*InternalHeaders(ParticipantPanel)
#include <wx/xrc/xmlres.h>
//*)


/**
  * Specialized sort comparator for the meeting list.
  */
class ParticipantListComparator : public wxTreeListCtrlComparator
{
public:
    ParticipantListComparator( ParticipantPanel *pPanel, wxTreeListCtrl * pTree )
    :wxTreeListCtrlComparator(pTree),
    m_pPanel(pPanel)
    {
    }
    virtual ~ParticipantListComparator( )
    {
    }

    // Special comparator to move participants with hand up to the top
    virtual int CompareItems(const wxTreeItemId& item1,const wxTreeItemId& item2)
    {
        int cmp = 0;
        long sort_col = GetTree()->GetSortColumn();

        if( sort_col == wxNOT_FOUND || sort_col == 0 )
        {
            ParticipantTreeItemData *pPTID1 = (ParticipantTreeItemData *) m_pPanel->GetParticipantTree( )->GetItemData( item1 );
            ParticipantTreeItemData *pPTID2 = (ParticipantTreeItemData *) m_pPanel->GetParticipantTree( )->GetItemData( item2 );

            if( !pPTID1->IsParticipant() || !pPTID2->IsParticipant() )
            {
                cmp = GetTree()->GetItemText(item1).CmpNoCase( GetTree()->GetItemText(item2) );
            }
            else
            {
                bool    fHandup1 = pPTID1->m_fHandUp;
                bool    fHandup2 = pPTID2->m_fHandUp;
                
                if( fHandup1 == true && fHandup2 == false )
                    cmp = -1;
                else if( fHandup1 == false && fHandup2 == true )
                    cmp = 1;
                else /* fHandup1 == fHandup2 */
                    cmp = GetTree()->GetItemText(item1).CmpNoCase( GetTree()->GetItemText(item2) );
            }
        }
        else
        {
            cmp = GetTree()->GetItemText(item1,sort_col).CmpNoCase( GetTree()->GetItemText(item2,sort_col) );
        }

        wxTreeListColumnInfo& info = GetTree()->GetColumn(sort_col);
        if( !info.GetSortAscending() )
        {
            cmp = -cmp;
        }
        return cmp;
    }

private:
    ParticipantPanel *m_pPanel;
};


//(*IdInit(ParticipantPanel)
//*)

BEGIN_EVENT_TABLE(ParticipantPanel,wxPanel)
	//(*EventTable(ParticipantPanel)
	//*)
EVT_CONTEXT_MENU(ParticipantPanel::OnContextMenu)
EVT_TREE_ITEM_RIGHT_CLICK(wxID_ANY, ParticipantPanel::OnTreeContextMenu)
EVT_TREE_ITEM_MENU(wxID_ANY, ParticipantPanel::OnTreeContextMenu)

// Buttons on participant panel
EVT_BUTTON(XRCID("IDC_DLG_MTG_BTN_HANDUP"),ParticipantPanel::OnClickBtnHandUp)
EVT_BUTTON(XRCID("IDC_DLG_MTG_BTN_OPERATOR"),ParticipantPanel::OnClickBtnOperator)
EVT_BUTTON(XRCID("IDC_DLG_MTG_BTN_RECORD"),ParticipantPanel::OnClickBtnRecord)
EVT_BUTTON(XRCID("IDC_DLG_MTG_BTN_LECTURE"),ParticipantPanel::OnClickBtnLecture)
EVT_BUTTON(XRCID("IDC_DLG_MTG_BTN_HOLD"),ParticipantPanel::OnClickBtnHold)
EVT_BUTTON(XRCID("IDC_DLG_MTG_BTN_MEETING"),ParticipantPanel::OnClickBtnMeeting)
EVT_BUTTON(XRCID("IDC_DLG_MTG_BTN_MYPHONE"),ParticipantPanel::OnClickBtnMyPhone)
EVT_BUTTON(XRCID("IDC_DLG_MTG_BTN_SHARE"),ParticipantPanel::OnClickBtnShare)
EVT_BUTTON(XRCID("IDC_DLG_MTG_BTN_INVITE"),ParticipantPanel::OnClickBtnInvite)
EVT_BUTTON(XRCID("IDC_DLG_MTG_BTN_PARTICIPANT"),ParticipantPanel::OnClickBtnParticipant)

//  Main menu events are connected in constructor

//  Popup menu events
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_HANDUP"), ParticipantPanel::OnMenuHandUp)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_EXIT"), ParticipantPanel::OnMenuExitMeeting)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_CALLME1"), ParticipantPanel::OnMenuCallMe)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_CALLME2"), ParticipantPanel::OnMenuCallMe)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_CALLME3"), ParticipantPanel::OnMenuCallMe)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_CALLME4"), ParticipantPanel::OnMenuCallMe)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_CALLME5"), ParticipantPanel::OnMenuCallMe)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_CALLME6"), ParticipantPanel::OnMenuCallMe)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_CALLME7"), ParticipantPanel::OnMenuCallMe)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_CALLME8"), ParticipantPanel::OnMenuCallMe)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_CALLME9"), ParticipantPanel::OnMenuCallMe)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_CALLME10"), ParticipantPanel::OnMenuCallMe)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_MUTEME"), ParticipantPanel::OnMenuMuteMe)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_VOLLEV8ME"), ParticipantPanel::OnMenuVolumeMe)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_VOLLEV7ME"), ParticipantPanel::OnMenuVolumeMe)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_VOLLEV6ME"), ParticipantPanel::OnMenuVolumeMe)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_VOLLEV5ME"), ParticipantPanel::OnMenuVolumeMe)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_VOLLEV4ME"), ParticipantPanel::OnMenuVolumeMe)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_VOLLEV3ME"), ParticipantPanel::OnMenuVolumeMe)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_VOLLEV2ME"), ParticipantPanel::OnMenuVolumeMe)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_VOLLEV1ME"), ParticipantPanel::OnMenuVolumeMe)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_HANGUP"), ParticipantPanel::OnMenuHangUpMe)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_DESKTOP"), ParticipantPanel::OnMenuShareDesktop)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_APPL"), ParticipantPanel::OnMenuShareApplication)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_STOPSHARING"), ParticipantPanel::OnMenuShareStop)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_OPENWHITEBOARD"), ParticipantPanel::OnMenuOpenWhiteboard)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_SHAREPRESENTATION"), ParticipantPanel::OnMenuSharePresentation)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_SHAREDOCUMENT"), ParticipantPanel::OnMenuShareDocument)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_DOCUMENTEXPLORER"), ParticipantPanel::OnMenuShareDocument)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_INVITENEW"), ParticipantPanel::OnMenuInviteNew)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_INVITESEL"), ParticipantPanel::OnMenuInviteSelected)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_MAKEPRES"), ParticipantPanel::OnMenuMakePresenter)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_MAKEMOD"), ParticipantPanel::OnMenuMakeModerator)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_UNMAKEMOD"), ParticipantPanel::OnMenuUnmakeModerator)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_GRANT"), ParticipantPanel::OnMenuGrantRemoteControl)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_REVOKE"), ParticipantPanel::OnMenuRevokeRemoteControl)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_HANDDOWN"), ParticipantPanel::OnMenuHandDown)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_CALL1"), ParticipantPanel::OnMenuConfCall)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_CALL2"), ParticipantPanel::OnMenuConfCall)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_CALL3"), ParticipantPanel::OnMenuConfCall)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_CALL4"), ParticipantPanel::OnMenuConfCall)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_CALL5"), ParticipantPanel::OnMenuConfCall)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_CALL6"), ParticipantPanel::OnMenuConfCall)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_CALL7"), ParticipantPanel::OnMenuConfCall)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_CALL8"), ParticipantPanel::OnMenuConfCall)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_CALL9"), ParticipantPanel::OnMenuConfCall)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_CALL10"), ParticipantPanel::OnMenuConfCall)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_MUTEPART"), ParticipantPanel::OnMenuMuteParticipant)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_UNMUTEPART"), ParticipantPanel::OnMenuUnmuteParticipant)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_VOLLEV8PART"), ParticipantPanel::OnMenuVolumeParticipant)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_VOLLEV7PART"), ParticipantPanel::OnMenuVolumeParticipant)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_VOLLEV6PART"), ParticipantPanel::OnMenuVolumeParticipant)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_VOLLEV5PART"), ParticipantPanel::OnMenuVolumeParticipant)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_VOLLEV4PART"), ParticipantPanel::OnMenuVolumeParticipant)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_VOLLEV3PART"), ParticipantPanel::OnMenuVolumeParticipant)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_VOLLEV2PART"), ParticipantPanel::OnMenuVolumeParticipant)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_VOLLEV1PART"), ParticipantPanel::OnMenuVolumeParticipant)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_HANGUP2"), ParticipantPanel::OnMenuHangUpParticipant)
EVT_MENU(XRCID("IDC_DLG_MTG_POPMENU_REMOVE"), ParticipantPanel::OnMenuRemoveSelected)

EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_HANDUP"), ParticipantPanel::OnMenuHandUpUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_CALLME"), ParticipantPanel::OnMenuCallMeUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_MUTEME"), ParticipantPanel::OnMenuMuteMeUpdateUI)
EVT_UPDATE_UI_RANGE(XRCID("IDC_DLG_MTG_POPMENU_VOLLEV8ME"), XRCID("IDC_DLG_MTG_POPMENU_VOLLEV1ME"), ParticipantPanel::OnMenuVolumeMeUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_HANGUP"), ParticipantPanel::OnMenuHangUpMeUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_DESKTOP"), ParticipantPanel::OnMenuShareDesktopUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_APPL"), ParticipantPanel::OnMenuShareApplicationUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_STOPSHARING"), ParticipantPanel::OnMenuShareStopUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_SHAREPRESENTATION"), ParticipantPanel::OnMenuOpenWhiteboardUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_OPENWHITEBOARD"), ParticipantPanel::OnMenuOpenWhiteboardUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_SHAREDOCUMENT"), ParticipantPanel::OnMenuOpenWhiteboardUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_DOCUMENTEXPLORER"), ParticipantPanel::OnMenuOpenWhiteboardUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_INVITENEW"), ParticipantPanel::OnMenuInviteNewUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_INVITESEL"), ParticipantPanel::OnMenuInviteSelUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_MAKEPRES"), ParticipantPanel::OnMenuMakePresUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_MAKEMOD"), ParticipantPanel::OnMenuMakeModeratorUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_UNMAKEMOD"), ParticipantPanel::OnMenuUnmakeModeratorUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_GRANT"), ParticipantPanel::OnMenuGrantRemoteControlUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_REVOKE"), ParticipantPanel::OnMenuRevokeRemoteControlUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_HANDDOWN"), ParticipantPanel::OnMenuHandDownUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_CALL"), ParticipantPanel::OnMenuCallParticipantUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_MUTEPART"), ParticipantPanel::OnMenuMuteParticipantUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_UNMUTEPART"), ParticipantPanel::OnMenuUnmuteParticipantUpdateUI)
EVT_UPDATE_UI_RANGE(XRCID("IDC_DLG_MTG_POPMENU_VOLLEV8PART"), XRCID("IDC_DLG_MTG_POPMENU_VOLLEV1PART"), ParticipantPanel::OnMenuVolumeParticipantUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_HANGUP2"), ParticipantPanel::OnMenuHangUpParticipantUpdateUI)
EVT_UPDATE_UI(XRCID("IDC_DLG_MTG_POPMENU_REMOVE"), ParticipantPanel::OnMenuRemoveSelectedUpdateUI)

END_EVENT_TABLE()



//*****************************************************************************
//  Constructore and Destructors
//*****************************************************************************
ParticipantPanel::ParticipantPanel(wxWindow *parent, wxWindowID id, ShareControl *pShareControl):
    m_nLastSortColumn(0),
    m_fLastSortAscending(true),
    m_pDocShareView(0),
    m_pDoc(0)
{
    wxLogDebug( wxT("entering ParticipantPanel CONSTRUCTOR" ) );

	//(*Initialize(ParticipantPanel)
	wxXmlResource::Get()->LoadObject(this,parent,_T("ParticipantPanel"),_T("wxPanel"));
	m_ParticipantTree = (wxTreeListCtrl*)FindWindow(XRCID("ID_CUSTOM1"));
	m_TBBHandUp = (wxTogBmpBtn*)FindWindow(XRCID("IDC_DLG_MTG_BTN_HANDUP"));
	m_TBBOperator = (wxTogBmpBtn*)FindWindow(XRCID("IDC_DLG_MTG_BTN_OPERATOR"));
	m_TBBMeeting = (wxTogBmpBtn*)FindWindow(XRCID("IDC_DLG_MTG_BTN_MEETING"));
	m_TBBMyPhone = (wxTogBmpBtn*)FindWindow(XRCID("IDC_DLG_MTG_BTN_MYPHONE"));
	m_TBBShare = (wxTogBmpBtn*)FindWindow(XRCID("IDC_DLG_MTG_BTN_SHARE"));
	m_TBBInvite = (wxTogBmpBtn*)FindWindow(XRCID("IDC_DLG_MTG_BTN_INVITE"));
	m_TBBParticipant = (wxTogBmpBtn*)FindWindow(XRCID("IDC_DLG_MTG_BTN_PARTICIPANT"));
	m_TBBRecord = (wxTogBmpBtn*)FindWindow(XRCID("IDC_DLG_MTG_BTN_RECORD"));
	m_TBBLecture = (wxTogBmpBtn*)FindWindow(XRCID("IDC_DLG_MTG_BTN_LECTURE"));
	m_TBBHold = (wxTogBmpBtn*)FindWindow(XRCID("IDC_DLG_MTG_BTN_HOLD"));
	//*)

    bool fOperatorAssist = OPTIONS.GetSystemBool( "EnableOperatorAssist", false );
    if (!fOperatorAssist)
        m_TBBOperator->Hide();

    bool fHoldMode = OPTIONS.GetSystemBool( "EnableHoldMode", false );
    if (!fHoldMode)
        m_TBBHold->Hide();

    m_pMMF = (MeetingMainFrame *) GetParent( );

    m_pPopupMenuBar = wxXmlResource::Get()->LoadMenuBar( NULL, _T("ParticipantMenuBar"));
    m_pMainMenuBar = ((wxFrame *) GetParent( ))->GetMenuBar( );

    m_pMainMenuBar->EnableTop( kMenuIndexMyPhone, false );
    m_TBBMyPhone->Disable( );
    m_TBBShare->Disable( );
    m_TBBMeeting->Show(false);

    m_pShareControl = pShareControl;

    m_ParticipantTree->SetIndent( 4 );
    m_ParticipantTree->SetLineSpacing( 0 );

	m_ParticipantTree->AddColumn( _("Name"),   320);
    m_ParticipantTree->AddColumn( _("PIN"),     60);
	m_ParticipantTree->AddColumn( _("Status"), 200);

    m_pcontextMenu = NULL;
    m_pcontextMenuVol = NULL;
    m_pcontextMenu1To1 = NULL;
    m_pcontextMenuConfCall = NULL;
    m_pcontextMenuEmail = NULL;
    m_pcontextMenuIM = NULL;
    m_pPopMenuMeeting = NULL;
    m_pPopMenuMyPhone = NULL;
    m_pPopMenuShare = NULL;
    m_pPopMenuInvite = NULL;
    m_pPopMenuParticipant = NULL;

    m_fModalDialogOpen = false;

    m_iNextSubNumber = 0;
    m_fCurrentlyPresenter = false;
    m_fAdmin = (( CONTROLLER.AdministratorMode() ) ? true : false );
    m_fMeetingHasStarted = false;
    m_fUpdatePrivileges = false;
    m_fUpdateCaption = false;

    m_meeting_t = NULL;
    m_participant_tMe = NULL;
    m_idMainMeeting.Unset( );
    m_idNotInMeeting.Unset( );

    m_ParticipantTree->SetFont( wxGetApp().GetAppDefFont() );
    m_ParticipantTree->Connect(wxEVT_CONTEXT_MENU, wxContextMenuEventHandler(ParticipantPanel::OnContextMenu), 0, this);
    m_ParticipantTree->Connect(wxID_ANY, wxEVT_COMMAND_TREE_ITEM_MENU, wxTreeEventHandler(ParticipantPanel::OnTreeContextMenu), 0, this);
    m_ParticipantTree->Connect(wxID_ANY, wxEVT_COMMAND_TREE_ITEM_RIGHT_CLICK, wxTreeEventHandler(ParticipantPanel::OnTreeContextMenu), 0, this);

    // iic session API
    if( wxGetApp( ).m_pMC )
    {
        CONTROLLER.Connect(wxID_ANY, wxEVT_MEETING_EVENT,           MeetingEventHandler(ParticipantPanel::OnMeetingEvent), 0, this );
        CONTROLLER.Connect(wxID_ANY, wxEVT_SUBMEETING_ADDED,        wxCommandEventHandler( ParticipantPanel::OnSubmeetingAdded), 0, this );
        CONTROLLER.Connect(wxID_ANY, wxEVT_MEETING_ERROR,           MeetingErrorHandler(ParticipantPanel::OnMeetingError), 0, this );

        //  Make connections for menu events
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_CHANGEOPTIONS"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuChangeOptions), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_SHOWDETAILS"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuShowDetails), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_LOCK"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuLock), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_LECTUREMODE"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuLectureMode), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_HOLDMODE"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuHoldMode), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_NOJOINTONES"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuNoJoinTones), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_ROLLCALL"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuRollCall), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_HANDUP"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuHandUp), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_RECORDMEETING"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuRecordMeeting), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_PREVIEWRECORDING"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuPreviewRecording), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_RESETRECORDING"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuResetRecording), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_EXITMEETING"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuExitMeeting), 0, this );

        parent->Connect(XRCID("IDC_DLG_MTG_MENU_CALLME1"), XRCID("IDC_DLG_MTG_MENU_CALLME10"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuCallMe), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_MUTEME"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuMuteMe), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_VOLLEV8ME"), XRCID("IDC_DLG_MTG_MENU_VOLLEV1ME"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuVolumeMe), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_HANGUP"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuHangUpMe), 0, this );

        parent->Connect(XRCID("IDC_DLG_MTG_MENU_INVITENEW"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuInviteNew), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_INVITESELECTED"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuInviteSelected), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_MAKEPRESENTER"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuMakePresenter), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_MAKEMODERATOR"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuMakeModerator), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_UNMAKEMODERATOR"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuUnmakeModerator), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_GRANTREMOTECONTROL"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuGrantRemoteControl), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_REVOKEREMOTECONTROL"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuRevokeRemoteControl), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_REMOVESELECTED"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuRemoveSelected), 0, this );

        parent->Connect(XRCID("IDC_DLG_MTG_MENU_MOVEALLTOMAIN"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuMoveAllToMain), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_MOVESELTONEWSUB"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuMoveSelToNewSub), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_MOVESELTOSUB"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuMoveSelToSub), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_REMOVESUBMEETING"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuRemoveSub), 0, this );

        parent->Connect(XRCID("IDC_DLG_MTG_MENU_MAKEMEPRESENTER"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuMakeMePresenter), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_SHAREDESKTOP"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuShareDesktop), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_SHAREAPPLICATION"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuShareApplication), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_SHAREPRESENTATION"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuSharePresentation), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_SHAREDOCUMENT"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuShareDocument), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_STOPSHARING"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuShareStop), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_OPENWHITEBOARD"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuOpenWhiteboard), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_DOCUMENTEXPLORER"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuShareDocumentExplorer), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_SHARESCALE"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuShareScale), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_SHARESCROLL"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuShareScroll), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_SHAREFITTOWIDTH"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuShareFitToWidth), 0, this );

        parent->Connect(XRCID("IDC_DLG_MTG_MENU_STOPSHARING"), wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(ParticipantPanel::OnMenuShareStop), 0, this );

        //  Make connections for menu UpdateUI events
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_LOCK"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuLockUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_HANDUP"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuHandUpUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_CALLME"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuCallMeUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_MUTEME"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuMuteMeUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_VOLLEV8ME"), XRCID("IDC_DLG_MTG_MENU_VOLLEV1ME"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuVolumeMeUpdateUI), 0, this );

        parent->Connect(XRCID("IDC_DLG_MTG_MENU_MAKEPRESENTER"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuMakePresUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_MAKEMODERATOR"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuMakeModeratorUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_UNMAKEMODERATOR"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuUnmakeModeratorUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_GRANTREMOTECONTROL"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuGrantRemoteControlUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_REVOKEREMOTECONTROL"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuRevokeRemoteControlUpdateUI), 0, this );

        parent->Connect(XRCID("IDC_DLG_MTG_MENU_MOVEALLTOMAIN"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuMoveAllToMainUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_MOVESELTONEWSUB"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuMoveSelToNewSubUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_MOVESELTOSUB"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuMoveSelToSubUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_REMOVESUBMEETING"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuRemoveSubUpdateUI), 0, this );

        parent->Connect(XRCID("IDC_DLG_MTG_MENU_MAKEMEPRESENTER"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuMakeMePresenterUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_SHAREDESKTOP"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuShareDesktopUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_SHAREAPPLICATION"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuShareApplicationUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_SHAREDOCUMENT"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuShareDocumentUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_SHAREPRESENTATION"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuShareDocumentUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_STOPSHARING"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuShareStopUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_OPENWHITEBOARD"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuOpenWhiteboardUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_DOCUMENTEXPLORER"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuOpenWhiteboardUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_SHARESCALE"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuShareScaleUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_SHARESCROLL"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuShareScrollUpdateUI), 0, this );
        parent->Connect(XRCID("IDC_DLG_MTG_MENU_SHAREFITTOWIDTH"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuShareFitToWidthUpdateUI), 0, this );
    }

    CONTROLLER.Connect(wxID_ANY, wxEVT_DOCSHARE_EVENT, DocshareEventHandler(ParticipantPanel::OnDocshareEvent), 0, this );
    CONTROLLER.Connect(wxID_ANY, wxEVT_PRESENCE_EVENT, PresenceEventHandler(ParticipantPanel::OnPresenceEvent), 0, this );

    Connect(XRCID("ID_CUSTOM1"),wxEVT_COMMAND_LIST_COL_CLICK,(wxObjectEventFunction)&ParticipantPanel::OnParticipantListColClick);

    // Set up the image lists for the tree
    m_TreeImages = new CImageListComposite( );
    m_StatusHandup = m_TreeImages->AddSubImageArray( wxGetApp().GetResourceFile(_T("res/status_meeting_handup")), 2 );
    m_StatusMeeting = m_TreeImages->AddSubImageArray( wxGetApp().GetResourceFile(_T("res/status_meeting_online")), 3 );
    m_StatusPhone = m_TreeImages->AddSubImageArray( wxGetApp().GetResourceFile(_T("res/status_meeting_phone")), 8 );

    // Don't show presence status icons if not in im mode
    if( CONTROLLER.IMClientMode() )
        m_StatusContact = m_TreeImages->AddSubImageArray( wxGetApp().GetResourceFile(_T("res/status_meeting_contact")), 5 );

    m_TreeImages->Create( NULL );
    m_ParticipantTree->SetImageList( m_TreeImages->GetImageList( ) );

    m_ParticipantTree->SetSortColumn( m_nLastSortColumn );
    m_ParticipantTree->SetSortAscending( m_nLastSortColumn, m_fLastSortAscending );

    // Setup the custom sort comparator
    m_ParticipantTree->SetSortComparator( new ParticipantListComparator( this, m_ParticipantTree ) );

    m_submeeting_tMain = NULL;

    SetParticipantPrivileges( NULL );

#ifdef MY_AUI_WORKAROUND
    Connect( wxEVT_SIZE, (wxObjectEventFunction)&ParticipantPanel::OnSize );
#endif
}


//*****************************************************************************
ParticipantPanel::~ParticipantPanel()
{
    wxLogDebug( wxT("entering ParticipantPanel DESTRUCTOR" ) );

    //  If we are sharing anything, stop it now
    bool    flag1   = ( (m_pShareControl->IsDesktopShared( ) || m_pShareControl->IsDocumentShared( ) ) ? true : false );
    bool    flag2   = CONTROLLER.CanShareDeskTop( );
    if( flag1  &&  flag2 )
    {
        DoShareStop( );
    }

    if( m_pDoc )
    {
        // Deleting all the views should delete the document as well
        m_pDoc->DeleteAllViews();
    }

    CONTROLLER.Disconnect(wxID_ANY, wxEVT_DOCSHARE_EVENT, DocshareEventHandler(ParticipantPanel::OnDocshareEvent), 0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_PRESENCE_EVENT, PresenceEventHandler(ParticipantPanel::OnPresenceEvent), 0, this );

    delete m_pcontextMenu;
    delete m_TreeImages;

#ifdef MY_AUI_WORKAROUND
    Disconnect( wxEVT_SIZE, (wxObjectEventFunction)&ParticipantPanel::OnSize );
#endif

}

//*****************************************************************************
int ParticipantPanel::GetAllParticipants( participant_tArray &outArray, bool fIncludeMain )
{
    outArray.Empty( );

    //  Participant tree depth is fixed at 2 levels - Submeetings contain Participants
    wxTreeItemId    tRootID;
    tRootID = m_ParticipantTree->GetRootItem( );
    if( tRootID.IsOk( ) )
    {
        wxTreeItemIdValue   cookieSub;
        wxTreeItemId        tSubID;
        tSubID = m_ParticipantTree->GetFirstChild( tRootID, cookieSub );
        while( tSubID.IsOk( ) )
        {
            SubmeetingTreeItemData  *pSTID;
            pSTID = (SubmeetingTreeItemData *) m_ParticipantTree->GetItemData( tSubID );
            bool    fIncludeThisSubmeeting;
            fIncludeThisSubmeeting = ( fIncludeMain  ||  !pSTID->m_s->is_main );

            wxTreeItemIdValue   cookiePart;
            wxTreeItemId        tPartID;
            tPartID = m_ParticipantTree->GetFirstChild( tSubID, cookiePart );
            while( tPartID.IsOk( ) )
            {
                ParticipantTreeItemData *pPTID;
                pPTID = (ParticipantTreeItemData *) m_ParticipantTree->GetItemData( tPartID );
                if( pPTID  &&  fIncludeThisSubmeeting )
                    outArray.Add( pPTID->m_p );
                tPartID = m_ParticipantTree->GetNextChild( tSubID, cookiePart );
            }
            tSubID = m_ParticipantTree->GetNextChild( tRootID, cookieSub );
        }
    }

    return (int) outArray.GetCount( );
}


//*****************************************************************************
int ParticipantPanel::GetAllSubmeetings( submeeting_tArray &outArray, bool fIncludeMain )
{
    outArray.Empty( );

    //  Participant tree depth is fixed at 2 levels - Submeetings contain Participants
    wxTreeItemId    tRootID;
    tRootID = m_ParticipantTree->GetRootItem( );
    if( tRootID.IsOk( ) )
    {
        wxTreeItemIdValue   cookieSub;
        wxTreeItemId        tSubID;
        tSubID = m_ParticipantTree->GetFirstChild( tRootID, cookieSub );
        while( tSubID.IsOk( ) )
        {
            SubmeetingTreeItemData *pSTID;
            pSTID = (SubmeetingTreeItemData *) m_ParticipantTree->GetItemData( tSubID );
            if( pSTID )
                if( fIncludeMain  ||  !pSTID->m_s->is_main )
                    outArray.Add( pSTID->m_s );
            tSubID = m_ParticipantTree->GetNextChild( tRootID, cookieSub );
        }
    }

    return (int) outArray.GetCount( );
}


//*****************************************************************************
int ParticipantPanel::GetSelectedParticipants( participant_tArray &outArray )
{
    int                 iNumSelected;
    wxArrayTreeItemIds  selectedItems;
    iNumSelected = m_ParticipantTree->GetSelections( selectedItems );

    ParticipantTreeItemData *pPTID;

    outArray.Empty( );
    for( int i=0; i<iNumSelected; i++ )
    {
        wxTreeItemId parent = m_ParticipantTree->GetItemParent( selectedItems.Item( i ) );
        if ( parent == m_ParticipantTree->GetRootItem() )
        {
            wxTreeItemIdValue   cookie;
            wxTreeItemId        idT;
            idT = m_ParticipantTree->GetFirstChild( selectedItems.Item( i ), cookie );
            while( idT.IsOk( ) )
            {
                pPTID = (ParticipantTreeItemData *) m_ParticipantTree->GetItemData( idT );
                if( pPTID )
                {
                    int     iRet = outArray.Index( pPTID->m_p );
                    if( iRet  ==  wxNOT_FOUND )
                        outArray.Add( pPTID->m_p );
                }
                idT = m_ParticipantTree->GetNextChild( selectedItems.Item( i ), cookie );
            }
        }
        else
        {
            pPTID = (ParticipantTreeItemData *) m_ParticipantTree->GetItemData( selectedItems.Item( i ) );
            if( pPTID )
            {
                int     iRet = outArray.Index( pPTID->m_p );
                if( iRet  ==  wxNOT_FOUND )
                    outArray.Add( pPTID->m_p );
            }
        }
    }
    return (int) outArray.GetCount( );
}


//*****************************************************************************
participant_t ParticipantPanel::GetFirstSelectedParticipant( )
{
    int                 iNumSelected;
    wxArrayTreeItemIds  selectedItems;
    iNumSelected = m_ParticipantTree->GetSelections( selectedItems );
    if( iNumSelected  <=  0 )
        return NULL;

    ParticipantTreeItemData *pPTID;
    for( int i=0; i<iNumSelected; i++ )
    {
        wxTreeItemId parent = m_ParticipantTree->GetItemParent( selectedItems.Item( i ) );
        if ( parent == m_ParticipantTree->GetRootItem() )
        {
            wxTreeItemIdValue   cookie;
            wxTreeItemId        idT;
            idT = m_ParticipantTree->GetFirstChild( selectedItems.Item( i ), cookie );
            while( idT.IsOk( ) )
            {
                pPTID = (ParticipantTreeItemData *) m_ParticipantTree->GetItemData( idT );
                if( pPTID )
                {
                    return pPTID->m_p;
                }
                idT = m_ParticipantTree->GetNextChild( selectedItems.Item( i ), cookie );
            }
        }
        else
        {
            pPTID = (ParticipantTreeItemData *) m_ParticipantTree->GetItemData( selectedItems.Item( i ) );
            if( pPTID )
            {
                return pPTID->m_p;
            }
        }
    }
    return NULL;
}


//*****************************************************************************
participant_t ParticipantPanel::GetSingleSelection()
{
    int                 iNumSelected;
    wxArrayTreeItemIds  selectedItems;
    iNumSelected = m_ParticipantTree->GetSelections( selectedItems );
    if( iNumSelected  == 1 && !m_ParticipantTree->HasChildren( selectedItems.Item( 0 ) ) )
    {
        //  Check if selected participant is already presenter
        ParticipantTreeItemData *pPTID = (ParticipantTreeItemData *) m_ParticipantTree->GetItemData( selectedItems.Item( 0 ) );
        if( pPTID )
        {
            return pPTID->m_p;
        }
    }
    return NULL;
}


//*****************************************************************************
void ParticipantPanel::EndMeeting( )
{
    wxLogDebug( wxT("entering ParticipantPanel::EndMeeting( )") );

    //  Shut down the event handlers - This seemed to be causing infinite recursion on shutdown
    if( wxGetApp( ).m_pMC )
    {
        CONTROLLER.Disconnect(wxID_ANY, wxEVT_MEETING_EVENT,           MeetingEventHandler(ParticipantPanel::OnMeetingEvent), 0, this );
        CONTROLLER.Disconnect(wxID_ANY, wxEVT_SUBMEETING_ADDED,        wxCommandEventHandler( ParticipantPanel::OnSubmeetingAdded), 0, this );
        CONTROLLER.Disconnect(wxID_ANY, wxEVT_MEETING_ERROR,           MeetingErrorHandler(ParticipantPanel::OnMeetingError), 0, this );
    }

    DoShareStop( );
}


//*****************************************************************************
void ParticipantPanel::StartMeeting( const wxString &strParticipantID, const wxString &strParticipantName, const wxString &strMeetingID, const wxString &strMeetingTitle, const wxString &strPassword, bool fJoining )
{
    m_strParticipantID = strParticipantID;
    m_strParticipantID.StartsWith( wxT("part:"), &m_strParticipantID );
    m_strParticipantName = strParticipantName;
    m_strMeetingID = strMeetingID;
    m_strMeetingTitle = strMeetingTitle;
    m_strPassword = strPassword;

    SetName( m_strMeetingTitle );

    m_pShareControl->Initialize(CONTROLLER.controller, strMeetingID);

    m_pShareControl->GetToolbarHandler()->Connect( XRCID("ID_SHARECTRL_RECORDING"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuRecordMeeting), 0, this );
    m_pShareControl->GetToolbarHandler()->Connect( XRCID("ID_SHARECTRL_LECTUREMODE"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuLectureMode), 0, this );
    m_pShareControl->GetToolbarHandler()->Connect( XRCID("ID_SHARECTRL_HOLDMODE"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuHoldMode), 0, this );
    m_pShareControl->GetToolbarHandler()->Connect( XRCID("ID_SHARECTRL_OPERATOR"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuOperator), 0, this );
    
}


//*****************************************************************************
void ParticipantPanel::UpdateTitle( )
{
    int active = 0, total = 0;

    if (m_meeting_t)
    {
        meeting_lock(m_meeting_t);

        participant_t p;

        for (p = meeting_next_participant(m_meeting_t,NULL); p != NULL; p = meeting_next_participant(m_meeting_t,p))
        {
            ++total;
            if (p->status & StatusInMeeting)
                ++active;
        }

        meeting_unlock(m_meeting_t);
        
        wxString title = wxString::Format(_("Invitees (%d/%d)"), active, total);

        wxAuiManager * pMgr = wxAuiManager::GetManager(this);
        wxAuiPaneInfo & pPane = pMgr->GetPane(this);
        pPane.Caption(title);
        pMgr->Update();
    }
}


//*****************************************************************************
void ParticipantPanel::BuildTree( meeting_t meeting )
{
    bool    fJustBecamePresenter = false;

    m_meeting_t = meeting;
    m_ParticipantTree->DeleteRoot( );
    m_mapSubID2TIID.clear( );
    m_mapPartID2TIID.clear( );

    m_idMainMeeting.Unset( );
    m_idNotInMeeting.Unset( );

    wxTreeItemId    root = m_ParticipantTree->AddRoot(_T("root"));

    meeting_lock(meeting);

    submeeting_t sm;
    participant_t p;

    for (sm = meeting_next_submeeting(meeting,NULL); sm != NULL; sm = meeting_next_submeeting(meeting,sm))
    {
        SubmeetingTreeItemData  *pSMTID;
        pSMTID = new SubmeetingTreeItemData( sm );
        wxString    strTitle;
        GetSubmeetingTitle( sm, strTitle );
        wxTreeItemId id = m_ParticipantTree->PrependItem(root, strTitle, -1, -1, pSMTID);
        m_mapSubID2TIID.insert(wxString2TreeItemIDMap::value_type(wxString(sm->sub_id, wxConvUTF8), id));
        if( sm->is_main )
        {
            m_idMainMeeting = id;
            m_submeeting_tMain = sm;
        }
    }

    for (p = meeting_next_participant(meeting,NULL); p != NULL; p = meeting_next_participant(meeting,p))
    {
        fJustBecamePresenter |= AddParticipantToTree( p );
    }

    m_ParticipantTree->ExpandAll( root );

    meeting_unlock(meeting);

    m_fUpdateCaption = true;
}


//*****************************************************************************
void ParticipantPanel::GetSubmeetingTitle( submeeting_t sm, wxString &strTitle )
{
    wxString    strT( sm->title, wxConvUTF8 );
    strTitle.Empty( );

    if( sm->is_main )
    {
        wxString    strFormat( _("Main"), wxConvUTF8 );
        strTitle.Append( strFormat );
    }
    else
    {
        wxString    strT2( sm->title, wxConvUTF8 );
        strT2.Trim( true );
        strT2.Trim( false );
        wxString    strNum( strT2.AfterFirst( ' ' ) );
        long        lNum;
        bool        fRet    = strNum.ToLong( &lNum );
        wxString    strFormat( _("Submeeting %d"), wxConvUTF8 );
        if( fRet  &&  !strFormat.IsEmpty( ) )
            strTitle.Append( wxString::Format( strFormat, lNum ) );
    }
    if( strTitle.IsEmpty( ) )
        strTitle.Append( strT );
}


//*****************************************************************************
bool ParticipantPanel::AddParticipantToTree( wxString strPartID, bool fSort )
{
    participant_t p = NULL;
    if( m_meeting_t )
        p = meeting_find_participant( m_meeting_t, strPartID.mb_str(wxConvUTF8) );
    if ( p )
        return AddParticipantToTree( p, fSort );
    else
        return false;
}


//*****************************************************************************
bool ParticipantPanel::AddParticipantToTree( participant_t p, bool fSort )
{
    bool    fJustBecamePresenter = false;;

    wxTreeItemId    idNewSub;
    idNewSub.Unset( );
    wxString2TreeItemIDMap::iterator iter = m_mapSubID2TIID.find(wxString(p->sub_id, wxConvUTF8));
    if( iter  ==  m_mapSubID2TIID.end() )
    {
        //  Create the necessary submeeting
        submeeting_t sm;
        sm = meeting_find_submeeting( m_meeting_t, p->sub_id );
        if( sm )
        {
            SubmeetingTreeItemData  *pSMTID;
            pSMTID = new SubmeetingTreeItemData( sm );
            wxString    strTitle;
            GetSubmeetingTitle( sm, strTitle );
            idNewSub = m_ParticipantTree->PrependItem( m_ParticipantTree->GetRootItem( ), strTitle, -1, -1, pSMTID);
            m_mapSubID2TIID.insert(wxString2TreeItemIDMap::value_type(wxString(sm->sub_id, wxConvUTF8), idNewSub));
        }
    }
    iter = m_mapSubID2TIID.find(wxString(p->sub_id, wxConvUTF8));
    if( iter != m_mapSubID2TIID.end() )
    {
        wxString    strName;
        wxString    strStatus;
        FormatParticipantStatus( p, strName, strStatus );

        int         iStatus = p->status;
        wxString    strPartID( p->part_id, wxConvUTF8 );
        strPartID.Replace( _T("part:"), _T("") );

        //  Select image corresponding to their current status
        SetParticipantStatus( p );
        int statusImage = m_TreeImages->GetSelectedListIndex();

        //  Create line in tree
        wxTreeItemId    id = (*iter).second;
        wxTreeItemId    newNode;
        if( id == m_idMainMeeting  &&  !(iStatus & StatusInMeeting) )
        {
            if( !m_idNotInMeeting.IsOk( ) )
            {
                SubmeetingTreeItemData  *pSMTID;
                pSMTID = new SubmeetingTreeItemData( m_submeeting_tMain, true );
                m_idNotInMeeting = m_ParticipantTree->AppendItem( m_ParticipantTree->GetRootItem( ), _("Not In Meeting"), -1, -1, pSMTID);
            }
            id = m_idNotInMeeting;
        }
        idNewSub = id;

        ParticipantTreeItemData     *pPTID;
        pPTID = new ParticipantTreeItemData( p );

        newNode = m_ParticipantTree->AppendItem(id, strName, statusImage, -1, pPTID);
        if( fSort )
            m_ParticipantTree->SortChildren( id );

        m_mapPartID2TIID.insert( wxString2TreeItemIDMap::value_type(strPartID, newNode) );

        m_ParticipantTree->SetItemText( newNode, 1, strPartID );
        m_ParticipantTree->SetItemText( newNode, 2, strStatus );

        if( m_strParticipantID.CmpNoCase( strPartID )  ==  0 )
        {
            //  Hey!  This is MY info!!
            //  If I have just become the Presenter pop up the warning dialog - delayed until after we do the meeting_unlock() call
            if( m_participant_tMe  &&  !m_fCurrentlyPresenter  &&  (p->status & StatusPresenter) )
                fJustBecamePresenter = true;

            if( m_pDocShareView )
                m_pDocShareView->RemoteControl( ((p->status & StatusControl) != 0) ? true : false );

            m_fCurrentlyPresenter = (p->status & StatusPresenter);

            int         iRoll   = p->roll;      //  Still spelt wrong
            m_ParticipantTree->SetColumnShown( 1, ( iRoll == RollHost ) || ( iRoll == RollModerator ) || m_fAdmin );

            // Pass on to doc share
            if( m_pDoc )
                m_pDoc->m_Meeting.SetIsUserPresenter( m_fCurrentlyPresenter );

            m_fUpdatePrivileges = true;
        }
    }

    //  If we just created a new submeeting node, expand it
    if( idNewSub.IsOk( ) )
        m_ParticipantTree->ExpandAll( idNewSub );

    m_fUpdateCaption = true;

    return fJustBecamePresenter;
}


//*****************************************************************************
void ParticipantPanel::RemoveParticipantFromTree( wxString strPartID )
{
    wxLogDebug( wxT("entering RemoveParticipantFromTree( ) - strPartID = %s"), strPartID.wc_str( ) );

    wxString    strT( strPartID );
    strT.Replace( _T("part:"), _T("") );
    wxString2TreeItemIDMap::iterator partIter = m_mapPartID2TIID.find( strT );
    if( partIter  !=  m_mapPartID2TIID.end( ) )
    {
        wxTreeItemId    id = (*partIter).second;
        {
            m_ParticipantTree->Delete( id );
            m_ParticipantTree->Refresh( );
            m_mapPartID2TIID.erase( partIter );
        }
    }

    m_fUpdateCaption = true;
}


//*****************************************************************************
void ParticipantPanel::UpdateParticipantState( wxString strSubID, wxString strPartID )
{
    if( m_meeting_t  ==  NULL )
        return;

    participant_t   p;
    p = meeting_find_participant( m_meeting_t, strPartID.mb_str(wxConvUTF8) );
    if( p  ==  NULL )
        return;

    //  Find the participant's wxTreeItem...
    wxString    strPID( strPartID );
    strPID.Replace( _T("part:"), _T("") );
    wxString2TreeItemIDMap::iterator iter = m_mapPartID2TIID.find( strPID );
    if( iter  !=  m_mapPartID2TIID.end( ) )
    {
        wxString    strName;
        wxString    strStatus;
        FormatParticipantStatus( p, strName, strStatus );

        wxTreeItemId    id = (*iter).second;
        m_ParticipantTree->SetItemText( id, strName );
        m_ParticipantTree->SetItemText( id, 1, strPID );
        m_ParticipantTree->SetItemText( id, 2, strStatus );

        SetParticipantStatus( p );
        int     statusImage = m_TreeImages->GetSelectedListIndex( );
        m_ParticipantTree->SetItemImage( id, statusImage );

        ParticipantTreeItemData *pPTID;
        pPTID = (ParticipantTreeItemData *) m_ParticipantTree->GetItemData( id );
        bool    fPriorHandup = pPTID->m_fHandUp;
        bool    fCurrentHandup = ((p->status & StatusHandUp) != 0);

        if( p != m_participant_tMe )
        {
            //  If we are presenting a shared session and someone raises their hand notify the SharingControlDialog
            if( fPriorHandup  !=  fCurrentHandup )
            {
                wxString    strT;
                strT = strName.BeforeFirst( wxT(' ') );
                m_pShareControl->Handup( strT, fCurrentHandup );
            }
        }
        else
        {
            // Change state and tooltip for hand up button 
            if ( fPriorHandup != fCurrentHandup )
            {
                m_TBBHandUp->SetDown( fCurrentHandup );
                m_TBBHandUp->SetToolTip( fCurrentHandup ? _("Put my hand down") : _("Put my hand up") );
            }
        }
        pPTID->m_fHandUp = fCurrentHandup;

        // Resort participants
        if ( fPriorHandup != fCurrentHandup )
            m_ParticipantTree->SortChildren( m_idMainMeeting );

        bool    fPriorOperator = m_TBBOperator->IsDown();
        bool    fCurrentOperator = ((p->status & StatusBridgeRequest) != 0);

        if( p == m_participant_tMe )
        {
            if( fPriorOperator != fCurrentOperator )
            {
                m_TBBOperator->SetDown( fCurrentOperator );
                m_TBBOperator->SetToolTip( fCurrentOperator ? _("Cancel request for operator assistance") : _("Request operator assistance") );
            }
        }
    }

    //  Additional work if updating my status
    if( p  ==  m_participant_tMe )
    {
        if( !m_fCurrentlyPresenter  &&  (p->status & StatusPresenter) && !m_fModalDialogOpen )
        {
            m_fModalDialogOpen = true;
            wxString    strBrand = wxGetApp().m_strProductName;
            wxMessageDialog     dlg( this, _("You have just become the meeting presenter.\nTo share an application or your desktop click the\nshare button at the top of your meeting window."), strBrand, wxOK | wxICON_INFORMATION );
            dlg.ShowModal( );
            m_fModalDialogOpen = false;
        }
        m_fCurrentlyPresenter = (p->status & StatusPresenter);

        int         iRoll   = p->roll;      //  Still spelt wrong
        m_ParticipantTree->SetColumnShown( 1, ( iRoll == RollHost ) || ( iRoll == RollModerator ) || m_fAdmin );

        // Pass on to doc share
        if( m_pDoc )
            m_pDoc->m_Meeting.SetIsUserPresenter( m_fCurrentlyPresenter );

        m_fUpdatePrivileges = true;
    }
}


//*****************************************************************************
void ParticipantPanel::FormatParticipantStatus( participant_t p, wxString &strName, wxString &strStatus )
{
    //  Format participant's name and their role(s)
    int         iRoll   = p->roll;      //  Still spelt wrong
    int         iStatus = p->status;

    strName.Empty( );
    strName.Append( wxString( p->name, wxConvUTF8 ) );

    if( iRoll == RollHost )
        strName.Append( _(" [Host]") );
    else if( iRoll == RollModerator )
        strName.Append( _(" [Moderator]") );

    if( iStatus & StatusPresenter )
        strName.Append( _(" [Presenter]") );
    if( iStatus & StatusControl )
        strName.Append( _(" [Remote Ctl]") );

    //  Format participant's status
    strStatus.Append( _("Current Phone:  ") );
    if( m_strConnectedPhone.Len( )  ==  0 )
        strStatus.Append( _("None") );
    else
        strStatus.Append( m_strConnectedPhone );

    // Set Phone Status
    if( iStatus & StatusTalking )
    {
        strStatus.Append( _(" <connected>") );
    }
    else if( iStatus & StatusDialing )
    {
        strStatus.Append( _(" <dialing>") );
    }
    else if( iStatus & StatusRinging)
    {
        strStatus.Append( _(" <ringing>") );
    }
    else if ( iStatus & StatusBusy)
    {
        strStatus.Append( _(" <busy>") );
    }
    else if ( iStatus & StatusAnswered)
    {
        strStatus.Append( _(" <connected>") );
    }
    else if ( iStatus & StatusVoice)  // must be last else...
    {
        strStatus.Append( _(" <connected>") );
    }
    else if ( iStatus & StatusSpecialInfo)
    {
        strStatus.Append( _(" <special info>") );
    }
    else if ( iStatus & StatusTelephonyError)
    {
        strStatus.Append( _(" <error>") );
    }
    else if ( iStatus & StatusRemoteHangup)
    {
        strStatus.Append( _(" <hangup>") );
    }
}

//*****************************************************************************
void ParticipantPanel::OnInternalIdle()
{
    // This extra level of indirection was to reduce flickering of the menus as meeting events come in.
    if (m_fUpdatePrivileges && m_fMeetingHasStarted)
    {
        SetParticipantPrivileges(m_participant_tMe);
        m_fUpdatePrivileges = false;
    }
    if (m_fUpdateCaption)
    {
        UpdateTitle();
        m_fUpdateCaption = false;
    }
    wxPanel::OnInternalIdle();
}

//*****************************************************************************
void ParticipantPanel::SetParticipantPrivileges( participant_t p )
{
    bool        fPresent = false, fEnable = false;
    bool        fPhoneEnable = false;
    int         iRoll    = 0;

    if( p )
    {
        iRoll = p->roll;      //  Still spelt wrong

        //  Set anything that depends upon your role as Presenter
        fPresent = ( p->status & StatusPresenter );
        //   or remote controller
        if (share_is_control_enabled() != ((p->status & StatusControl) != 0))
            share_enable_control(p->status & StatusControl);
    }

    //  Set anything that depends upon your role as Host or Moderator or Administrator
    fEnable = ( iRoll == RollHost || iRoll == RollModerator || m_fAdmin );

    fPhoneEnable = (fEnable  &&  CONTROLLER.IsTelephonyAvailable( ));

    //  If meeting has not yet started disable everything except exit and help
    if( !m_fMeetingHasStarted )
    {
        fPresent = false;
        fEnable = false;
        fPhoneEnable = false;
    }

    m_pShareControl->EnableModerator( fEnable );

    m_TBBShare->Enable( fPresent );
    m_TBBInvite->Enable( fEnable );
    m_TBBParticipant->Enable( fEnable );
    m_TBBRecord->Enable( fEnable );
    m_TBBLecture->Enable( fPhoneEnable );
    m_TBBHold->Enable( fPhoneEnable );
    m_TBBOperator->Enable( CONTROLLER.IsTelephonyAvailable( ) && p && (p->status & StatusVoice));

    wxMenuBar *pBar = m_pMainMenuBar;
    wxMenuItem      *pMI;
    if( pBar )
    {
        bool    fIsRecording    = false;
        pBar->EnableTop( kMenuIndexParticipants,    fEnable );
        pBar->EnableTop( kMenuIndexSubMeetings,     fEnable );
        pBar->EnableTop( kMenuIndexSharing,         m_fMeetingHasStarted );

        pMI = pBar->FindItem( XRCID("IDC_DLG_MTG_MENU_CHANGEOPTIONS") );
        if( pMI )
            pMI->Enable( fEnable );
        pMI = pBar->FindItem( XRCID("IDC_DLG_MTG_MENU_RECORDMEETING") );
        if( pMI )
        {
            pMI->Enable( fEnable );
            fIsRecording = pMI->IsChecked( );
        }
        pMI = pBar->FindItem( XRCID("IDC_DLG_MTG_MENU_PREVIEWRECORDING") );
        if( pMI )
            pMI->Enable( (fIsRecording) ? false : fEnable );
        pMI = pBar->FindItem( XRCID("IDC_DLG_MTG_MENU_RESETRECORDING") );
        if( pMI )
            pMI->Enable( (fIsRecording) ? false : fEnable );
        pMI = pBar->FindItem( XRCID("IDC_DLG_MTG_MENU_LOCK") );
        if( pMI )
            pMI->Enable( fEnable );
    }

    if( pBar )
    {
        pMI = pBar->FindItem( XRCID("IDC_DLG_MTG_MENU_LECTUREMODE") );
        if( pMI )
            pMI->Enable( fPhoneEnable );
        pMI = pBar->FindItem( XRCID("IDC_DLG_MTG_MENU_HOLDMODE") );
        if( pMI )
            pMI->Enable( fPhoneEnable );
        pMI = pBar->FindItem( XRCID("IDC_DLG_MTG_MENU_NOJOINTONES") );
        if( pMI )
            pMI->Enable( fPhoneEnable );
        pMI = pBar->FindItem( XRCID("IDC_DLG_MTG_MENU_ROLLCALL") );
        if( pMI )
            pMI->Enable( fPhoneEnable );

        pBar->EnableTop( kMenuIndexMyPhone, (m_fMeetingHasStarted && CONTROLLER.IsTelephonyAvailable( )) );
    }

    if( pBar )
    {
        if( (pMI = pBar->FindItem( XRCID("IDC_DLG_MTG_MENU_SHOWDETAILS") ))  != NULL )
            pMI->Enable( m_fMeetingHasStarted );
        if( (pMI = pBar->FindItem( XRCID("IDC_DLG_MTG_MENU_HANDUP") ))  != NULL )
            pMI->Enable( m_fMeetingHasStarted );
    }
    
    wxLogDebug(wxT("Updated meeting menus"));
}


//*****************************************************************************
//
//  Handle click events from the wxTogBmpBtn's to popup menus
//
//*****************************************************************************

void ParticipantPanel::OnClickBtnHandUp(wxCommandEvent& event)
{
    bool fHandUp = m_TBBHandUp->IsDown();
    m_TBBHandUp->SetToolTip( fHandUp ? _("Put my hand down") : _("Put my hand up") );

    wxString    strT = wxT("part:") + m_strParticipantID;
    controller_participant_hand_up( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), strT.mb_str(wxConvUTF8), fHandUp );
}

void ParticipantPanel::OnClickBtnOperator(wxCommandEvent& event)
{
    bool fOper = m_TBBOperator->IsDown();
    m_TBBOperator->SetToolTip( fOper ? _("Cancel request for operator assistance") : _("Request operator assistance") );

    wxString    strT = wxT("part:") + m_strParticipantID;
    controller_participant_bridge_request( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), strT.mb_str(wxConvUTF8), fOper ? 1 : 0,
                                           fOper ? "request_operator" : "cancel_operator" );
}

void ParticipantPanel::OnClickBtnRecord(wxCommandEvent& event)
{
    bool fOpt = m_TBBRecord->IsDown();
    m_TBBRecord->SetToolTip( fOpt ? _T("Click to stop recording") : _T("Click to start recording") );
    controller_meeting_record( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), fOpt );

}

void ParticipantPanel::OnClickBtnLecture(wxCommandEvent& event)
{
    bool fOpt = m_TBBLecture->IsDown();
    m_TBBLecture->SetToolTip( fOpt ? _("Click to exit lecture mode") : _("Click to enter lecture mode") );
    controller_meeting_lecture_mode( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), fOpt );
}

void ParticipantPanel::OnClickBtnHold(wxCommandEvent& event)
{
    bool fOpt = m_TBBHold->IsDown();
    m_TBBHold->SetToolTip( fOpt ? _("Click to release all calls from hold") : _("Click to place all calls on hold") );
    controller_meeting_hold_mode( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), fOpt );
}

void ParticipantPanel::OnClickBtnMeeting(wxCommandEvent& event)
{
    bool        fRet    = false;
    wxToolTip   *pTT    = NULL;
    wxWindow    *pW     = NULL;
    pTT = m_TBBMeeting->GetToolTip( );
    if( pTT )
    {
        pW = pTT->GetWindow( );
        if( pW )
        {
            fRet = pW->IsShownOnScreen( );
            if( fRet )
                pW->Enable( false );
        }
    }
    wxRect  wR;
    wR = m_TBBMeeting->GetRect( );
    if( NULL  ==  m_pPopMenuMeeting )
        m_pPopMenuMeeting = m_pPopupMenuBar->GetMenu( m_pPopupMenuBar->FindMenu( _T("IDC_DLG_MTG_POPMENU_MEETING")) );
    PopupMenu( m_pPopMenuMeeting, wR.x, wR.y + wR.height );

    if( pW  &&  fRet )
        pW->Enable( true );
}


//*****************************************************************************
void ParticipantPanel::OnClickBtnMyPhone(wxCommandEvent& event)
{
    bool        fRet    = false;
    wxToolTip   *pTT    = NULL;
    wxWindow    *pW     = NULL;
    pTT = m_TBBMyPhone->GetToolTip( );
    if( pTT )
    {
        pW = pTT->GetWindow( );
        if( pW )
        {
            fRet = pW->IsShownOnScreen( );
            if( fRet )
                pW->Enable( false );
        }
    }
    wxRect  wR;
    wR = m_TBBMyPhone->GetRect( );
    if( NULL  ==  m_pPopMenuMyPhone )
        m_pPopMenuMyPhone = m_pPopupMenuBar->GetMenu( m_pPopupMenuBar->FindMenu( _T("IDC_DLG_MTG_POPMENU_MYPHONE")) );
    PopupMenu( m_pPopMenuMyPhone, wR.x, wR.y + wR.height );

    if( pW  &&  fRet )
        pW->Enable( true );
}


//*****************************************************************************
void ParticipantPanel::OnClickBtnShare(wxCommandEvent& event)
{
    bool        fRet    = false;
    wxToolTip   *pTT    = NULL;
    wxWindow    *pW     = NULL;
    pTT = m_TBBShare->GetToolTip( );
    if( pTT )
    {
        pW = pTT->GetWindow( );
        if( pW )
        {
            fRet = pW->IsShownOnScreen( );
            if( fRet )
                pW->Enable( false );
        }
    }
    wxRect  wR;
    wR = m_TBBShare->GetRect( );
    if( NULL  ==  m_pPopMenuShare )
        m_pPopMenuShare = m_pPopupMenuBar->GetMenu( m_pPopupMenuBar->FindMenu( _T("IDC_DLG_MTG_POPMENU_SHARE")) );
    PopupMenu( m_pPopMenuShare, wR.x, wR.y + wR.height );

    if( pW  &&  fRet )
        pW->Enable( true );
}


//*****************************************************************************
void ParticipantPanel::OnClickBtnInvite(wxCommandEvent& event)
{
    bool        fRet    = false;
    wxToolTip   *pTT    = NULL;
    wxWindow    *pW     = NULL;
    pTT = m_TBBInvite->GetToolTip( );
    if( pTT )
    {
        pW = pTT->GetWindow( );
        if( pW )
        {
            fRet = pW->IsShownOnScreen( );
            if( fRet )
                pW->Enable( false );
        }
    }
    wxRect  wR;
    wR = m_TBBInvite->GetRect( );
    if( NULL  ==  m_pPopMenuInvite )
        m_pPopMenuInvite = m_pPopupMenuBar->GetMenu( m_pPopupMenuBar->FindMenu( _T("IDC_DLG_MTG_POPMENU_INVITE")) );
    PopupMenu( m_pPopMenuInvite, wR.x, wR.y + wR.height );

    if( pW  &&  fRet )
        pW->Enable( true );
}


//*****************************************************************************
void ParticipantPanel::OnClickBtnParticipant(wxCommandEvent& event)
{
    bool        fRet    = false;
    wxToolTip   *pTT    = NULL;
    wxWindow    *pW     = NULL;
    pTT = m_TBBParticipant->GetToolTip( );
    if( pTT )
    {
        pW = pTT->GetWindow( );
        if( pW )
        {
            fRet = pW->IsShownOnScreen( );
            if( fRet )
                pW->Enable( false );
        }
    }
    wxRect  wR;
    wR = m_TBBParticipant->GetRect( );
    if( NULL  ==  m_pPopMenuParticipant )
        m_pPopMenuParticipant = m_pPopupMenuBar->GetMenu( m_pPopupMenuBar->FindMenu( _T("IDC_DLG_MTG_POPMENU_PARTICIPANT")) );

    //  Some options are disabled if no telephony available
    bool        fEnable;
    fEnable = CONTROLLER.IsTelephonyAvailable( );

    if( m_pPopMenuParticipant )
    {
        wxMenuItem      *pMI;
        pMI = m_pPopMenuParticipant->FindItem( XRCID("IDC_DLG_MTG_POPMENU_CALL") );
        if( pMI )
            pMI->Enable( fEnable );
        pMI = m_pPopMenuParticipant->FindItem( XRCID("IDC_DLG_MTG_POPMENU_MUTEPART") );
        if( pMI )
            pMI->Enable( fEnable );
        pMI = m_pPopMenuParticipant->FindItem( XRCID("IDC_DLG_MTG_POPMENU_UNMUTEPART") );
        if( pMI )
            pMI->Enable( fEnable );
        pMI = m_pPopMenuParticipant->FindItem( XRCID("IDC_DLG_MTG_POPMENU_HANGUPPART") );
        if( pMI )
            pMI->Enable( fEnable );
        wxMenu          *pM;
        pMI = m_pPopMenuParticipant->FindItem( XRCID("IDC_DLG_MTG_POPMENU_VOLUMEPART"), &pM );
        if( pMI )
            pMI->Enable( fEnable );
    }

    PopupMenu( m_pPopMenuParticipant, wR.x, wR.y + wR.height );

    if( pW  &&  fRet )
        pW->Enable( true );
}


//*****************************************************************************
//
//  Context Menu handler
//
//*****************************************************************************
void ParticipantPanel::OnTreeContextMenu(wxTreeEvent& event)
{
    wxContextMenuEvent  context;
    context.SetPosition( event.GetPoint( ) );
    OnContextMenu( context );
}


//*****************************************************************************
void ParticipantPanel::OnContextMenu(wxContextMenuEvent& event)
{
    int         iRoll   = 0;
    if( m_participant_tMe )
        iRoll = m_participant_tMe->roll;        //  Still spelt wrong

    //  Only display the context menu if user is Host, Moderator, or Administrator
    if(( iRoll != RollHost ) && ( iRoll != RollModerator ) && !m_fAdmin )
        return;

    wxPoint point = event.GetPosition();

    // If from keyboard
    if (point.x == -1 && point.y == -1)
    {
        wxSize size = GetSize();
        point.x = size.x / 2;
        point.y = size.y / 2;
    }

    int                 iNumSel;
    participant_tArray  pArray;

    iNumSel = GetSelectedParticipants( pArray );

    if( m_pcontextMenuVol  ==  NULL )
    {
        m_pcontextMenuVol = new wxMenu( );
        m_pcontextMenuVol->AppendCheckItem( XRCID("IDC_DLG_MTG_CONTMENU_VOLLEV8PART"), _("Loudest" ) );
        m_pcontextMenuVol->AppendCheckItem( XRCID("IDC_DLG_MTG_CONTMENU_VOLLEV7PART"), _("++++++" ) );
        m_pcontextMenuVol->AppendCheckItem( XRCID("IDC_DLG_MTG_CONTMENU_VOLLEV6PART"), _("+++++" ) );
        m_pcontextMenuVol->AppendCheckItem( XRCID("IDC_DLG_MTG_CONTMENU_VOLLEV5PART"), _("++++" ) );
        m_pcontextMenuVol->AppendCheckItem( XRCID("IDC_DLG_MTG_CONTMENU_VOLLEV4PART"), _("+++" ) );
        m_pcontextMenuVol->AppendCheckItem( XRCID("IDC_DLG_MTG_CONTMENU_VOLLEV3PART"), _("++" ) );
        m_pcontextMenuVol->AppendCheckItem( XRCID("IDC_DLG_MTG_CONTMENU_VOLLEV2PART"), _("+" ) );
        m_pcontextMenuVol->AppendCheckItem( XRCID("IDC_DLG_MTG_CONTMENU_VOLLEV1PART"), _("Lowest" ) );

        m_pcontextMenu1To1 = new wxMenu( );
        m_pcontextMenu1To1->Append( XRCID("IDC_DLG_MTG_CONTMENU_1TO1CALL1"), wxT("1st phone number") );
        m_pcontextMenu1To1->Append( XRCID("IDC_DLG_MTG_CONTMENU_1TO1CALL2"), wxT("2nd phone number") );
        m_pcontextMenu1To1->Append( XRCID("IDC_DLG_MTG_CONTMENU_1TO1CALL3"), wxT("3rd phone number") );
        m_pcontextMenu1To1->Append( XRCID("IDC_DLG_MTG_CONTMENU_1TO1CALL4"), wxT("4th phone number") );
        m_pcontextMenu1To1->Append( XRCID("IDC_DLG_MTG_CONTMENU_1TO1CALL5"), wxT("5th phone number") );
        m_pcontextMenu1To1->Append( XRCID("IDC_DLG_MTG_CONTMENU_1TO1CALL6"), wxT("6th phone number") );
        m_pcontextMenu1To1->Append( XRCID("IDC_DLG_MTG_CONTMENU_1TO1CALL7"), wxT("7th phone number") );
        m_pcontextMenu1To1->Append( XRCID("IDC_DLG_MTG_CONTMENU_1TO1CALL8"), wxT("8th phone number") );
        m_pcontextMenu1To1->Append( XRCID("IDC_DLG_MTG_CONTMENU_1TO1CALL9"), wxT("9th phone number") );
        m_pcontextMenu1To1->Append( XRCID("IDC_DLG_MTG_CONTMENU_1TO1CALL10"), wxT("10th phone number") );

        m_pcontextMenuConfCall = new wxMenu( );
        m_pcontextMenuConfCall->Append( XRCID("IDC_DLG_MTG_CONTMENU_CONFCALL1"), wxT("1st phone number") );
        m_pcontextMenuConfCall->Append( XRCID("IDC_DLG_MTG_CONTMENU_CONFCALL2"), wxT("2nd phone number") );
        m_pcontextMenuConfCall->Append( XRCID("IDC_DLG_MTG_CONTMENU_CONFCALL3"), wxT("3rd phone number") );
        m_pcontextMenuConfCall->Append( XRCID("IDC_DLG_MTG_CONTMENU_CONFCALL4"), wxT("4th phone number") );
        m_pcontextMenuConfCall->Append( XRCID("IDC_DLG_MTG_CONTMENU_CONFCALL5"), wxT("5th phone number") );
        m_pcontextMenuConfCall->Append( XRCID("IDC_DLG_MTG_CONTMENU_CONFCALL6"), wxT("6th phone number") );
        m_pcontextMenuConfCall->Append( XRCID("IDC_DLG_MTG_CONTMENU_CONFCALL7"), wxT("7th phone number") );
        m_pcontextMenuConfCall->Append( XRCID("IDC_DLG_MTG_CONTMENU_CONFCALL8"), wxT("8th phone number") );
        m_pcontextMenuConfCall->Append( XRCID("IDC_DLG_MTG_CONTMENU_CONFCALL9"), wxT("9th phone number") );
        m_pcontextMenuConfCall->Append( XRCID("IDC_DLG_MTG_CONTMENU_CONFCALL10"), wxT("10th phone number") );

        m_pcontextMenuEmail = new wxMenu( );
        m_pcontextMenuEmail->Append( XRCID("IDC_DLG_MTG_CONTMENU_EMAIL1"), wxT("1st email address") );
        m_pcontextMenuEmail->Append( XRCID("IDC_DLG_MTG_CONTMENU_EMAIL2"), wxT("2nd email address") );
        m_pcontextMenuEmail->Append( XRCID("IDC_DLG_MTG_CONTMENU_EMAIL3"), wxT("3rd email address") );
        m_pcontextMenuEmail->Append( XRCID("IDC_DLG_MTG_CONTMENU_EMAIL4"), wxT("4th email address") );
        m_pcontextMenuEmail->Append( XRCID("IDC_DLG_MTG_CONTMENU_EMAIL5"), wxT("5th email address") );
        m_pcontextMenuEmail->Append( XRCID("IDC_DLG_MTG_CONTMENU_EMAIL6"), wxT("6th email address") );
        m_pcontextMenuEmail->Append( XRCID("IDC_DLG_MTG_CONTMENU_EMAIL7"), wxT("7th email address") );
        m_pcontextMenuEmail->Append( XRCID("IDC_DLG_MTG_CONTMENU_EMAIL8"), wxT("8th email address") );
        m_pcontextMenuEmail->Append( XRCID("IDC_DLG_MTG_CONTMENU_EMAIL9"), wxT("9th email address") );
        m_pcontextMenuEmail->Append( XRCID("IDC_DLG_MTG_CONTMENU_EMAIL10"), wxT("10th email address") );

        m_pcontextMenuIM = new wxMenu( );
        m_pcontextMenuIM->Append( XRCID("IDC_DLG_MTG_CONTMENU_IM1"), wxT("1st IM address") );
        m_pcontextMenuIM->Append( XRCID("IDC_DLG_MTG_CONTMENU_IM2"), wxT("2nd IM address") );
        m_pcontextMenuIM->Append( XRCID("IDC_DLG_MTG_CONTMENU_IM3"), wxT("3rd IM address") );
        m_pcontextMenuIM->Append( XRCID("IDC_DLG_MTG_CONTMENU_IM4"), wxT("4th IM address") );
        m_pcontextMenuIM->Append( XRCID("IDC_DLG_MTG_CONTMENU_IM5"), wxT("5th IM address") );
        m_pcontextMenuIM->Append( XRCID("IDC_DLG_MTG_CONTMENU_IM6"), wxT("6th IM address") );
        m_pcontextMenuIM->Append( XRCID("IDC_DLG_MTG_CONTMENU_IM7"), wxT("7th IM address") );
        m_pcontextMenuIM->Append( XRCID("IDC_DLG_MTG_CONTMENU_IM8"), wxT("8th IM address") );
        m_pcontextMenuIM->Append( XRCID("IDC_DLG_MTG_CONTMENU_IM9"), wxT("9th IM address") );
        m_pcontextMenuIM->Append( XRCID("IDC_DLG_MTG_CONTMENU_IM10"), wxT("10th IM address") );

        bool        fVoice  = CONTROLLER.IsTelephonyAvailable( );
        bool        fRemote = CONTROLLER.CanShareRemoteControl( );

//        wxLogDebug( wxT("fVoice = %d  --  m_meeting_t->options = %d (0x%8.8x)  --  InvitationDataOnly = %d (0x%8.8x)  --  InvitationVoiceOnly = %d (0x%8.8x)"), fVoice,
//                    m_meeting_t->options, m_meeting_t->options, InvitationDataOnly, InvitationDataOnly, InvitationVoiceOnly, InvitationVoiceOnly );
        wxMenuItem  *pMI;
        m_pcontextMenu = new wxMenu( );
        m_pcontextMenu->Append( XRCID("IDC_DLG_MTG_POPMENU_INVITESEL"), _("Call/Invite...") );
        m_pcontextMenu->AppendSeparator( );
        m_pcontextMenu->Append( XRCID("IDC_DLG_MTG_MENU_MOVEALLTOMAIN"), _("Move all to main meeting") );
        m_pcontextMenu->Append( XRCID("IDC_DLG_MTG_MENU_MOVESELTONEWSUB"), _("Move selected to new sub-meeting") );
        m_pcontextMenu->Append( XRCID("IDC_DLG_MTG_MENU_MOVESELTOSUB"), _("Move selected to existing sub-meeting") );
        m_pcontextMenu->AppendSeparator( );
        m_pcontextMenu->AppendCheckItem( XRCID("IDC_DLG_MTG_MENU_MAKEPRESENTER"), _("Presenter") );
        m_pcontextMenu->Append( XRCID("IDC_DLG_MTG_MENU_MAKEMODERATOR"),  _("Grant moderator privilege") );
        m_pcontextMenu->Append( XRCID("IDC_DLG_MTG_MENU_UNMAKEMODERATOR"),  _("Revoke moderator privilege") );
        pMI = m_pcontextMenu->Append( XRCID("IDC_DLG_MTG_MENU_GRANTREMOTECONTROL"),  _("Grant remote control") );
        pMI->Enable( fRemote );
        pMI = m_pcontextMenu->Append( XRCID("IDC_DLG_MTG_MENU_REVOKEREMOTECONTROL"),  _("Revoke remote control") );
        pMI->Enable( fRemote );
        m_pcontextMenu->Append( XRCID("IDC_DLG_MTG_POPMENU_HANDDOWN"),   _("Put hand down") );
        m_pcontextMenu->Append( XRCID("IDC_DLG_MTG_MENU_REMOVESELECTED"), _("Remove") );
        m_pcontextMenu->AppendSeparator( );
        pMI = m_pcontextMenu->Append( XRCID("IDC_DLG_MTG_POPMENU_MUTEPART"), _("Mute") );
        pMI->Enable( fVoice );
        pMI = m_pcontextMenu->Append( XRCID("IDC_DLG_MTG_POPMENU_UNMUTEPART"), _("UnMute") );
        pMI->Enable( fVoice );
        pMI = m_pcontextMenu->AppendSubMenu( m_pcontextMenuVol, _("Volume") );
        pMI->Enable( fVoice );
        pMI = m_pcontextMenu->Append( XRCID("IDC_DLG_MTG_POPMENU_HANGUP2"), _("Hang up") );
        pMI->Enable( fVoice );
        m_pmenuitemSep = m_pcontextMenu->AppendSeparator( );
        pMI = m_pmenuitem1To1 = m_pcontextMenu->AppendSubMenu( m_pcontextMenu1To1, _("1 To 1 Call") );
        pMI->Enable( fVoice );
        pMI = m_pmenuitemConfCall = m_pcontextMenu->AppendSubMenu( m_pcontextMenuConfCall, _("Conference Call") );
        pMI->Enable( fVoice );
        m_pmenuitemEmail = m_pcontextMenu->AppendSubMenu( m_pcontextMenuEmail, _("Send Email Invite") );
        m_pmenuitemIM = m_pcontextMenu->AppendSubMenu( m_pcontextMenuIM, _("Send IM Invite") );
        m_fLastContextNumSel = 1;

        Connect( XRCID("IDC_DLG_MTG_CONTMENU_VOLLEV8PART"), XRCID("IDC_DLG_MTG_CONTMENU_VOLLEV1PART"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuContVolume) );
        Connect( XRCID("IDC_DLG_MTG_CONTMENU_1TO1CALL1"), XRCID("IDC_DLG_MTG_CONTMENU_1TO1CALL10"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenu1To1Call) );
        Connect( XRCID("IDC_DLG_MTG_CONTMENU_CONFCALL1"), XRCID("IDC_DLG_MTG_CONTMENU_CONFCALL10"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuConfCall) );
        Connect( XRCID("IDC_DLG_MTG_CONTMENU_EMAIL1"), XRCID("IDC_DLG_MTG_CONTMENU_EMAIL10"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuEmail) );
        Connect( XRCID("IDC_DLG_MTG_CONTMENU_IM1"), XRCID("IDC_DLG_MTG_CONTMENU_IM10"), wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(ParticipantPanel::OnMenuIM) );

        Connect( XRCID("IDC_DLG_MTG_CONTMENU_VOLLEV8PART"), XRCID("IDC_DLG_MTG_CONTMENU_VOLLEV1PART"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuContVolumeItemUpdateUI) );
#ifndef LINUX
        Connect( XRCID("IDC_DLG_MTG_CONTMENU_1TO1CALL1"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuContCallItemUpdateUI) );
        Connect( XRCID("IDC_DLG_MTG_CONTMENU_EMAIL1"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuEmailItemUpdateUI) );
        Connect( XRCID("IDC_DLG_MTG_CONTMENU_IM1"), wxEVT_UPDATE_UI, wxUpdateUIEventHandler(ParticipantPanel::OnMenuIMItemUpdateUI) );
#endif

    }

#ifdef LINUX
    // ToDo:  figure out why removing and re-creating the submenus is causing traps on linux...
    // in the mean time, leave menus there but empty
    wxUpdateUIEvent dummy;
    OnMenuContCallItemUpdateUI(dummy);
    OnMenuEmailItemUpdateUI(dummy);
    OnMenuIMItemUpdateUI(dummy);
#else
    if( iNumSel  ==  1 )
    {
        bool        fVoice  = CONTROLLER.IsTelephonyAvailable( );

        if( m_fLastContextNumSel  !=  1 )
        {
            wxMenuItem  *pMI;
            m_pmenuitemSep = m_pcontextMenu->AppendSeparator( );
            pMI = m_pmenuitem1To1 = m_pcontextMenu->AppendSubMenu( m_pcontextMenu1To1, _("1 To 1 Call") );
            pMI->Enable( fVoice );
            pMI = m_pmenuitemConfCall = m_pcontextMenu->AppendSubMenu( m_pcontextMenuConfCall, _("Conference Call") );
            pMI->Enable( fVoice );
            m_pmenuitemEmail = m_pcontextMenu->AppendSubMenu( m_pcontextMenuEmail, _("Send Email Invite") );
            m_pmenuitemIM = m_pcontextMenu->AppendSubMenu( m_pcontextMenuIM, _("Send IM Invite") );
            m_fLastContextNumSel = 1;
        }
        
        if( fVoice )
        {
            // 1-to-1 calls are to bring someone else into the meeting, not to call myself
            int item = m_pcontextMenu->FindItem(  _("1 To 1 Call") );
            if (item != wxNOT_FOUND)
                m_pcontextMenu->Enable( item, pArray.Item(0) != m_participant_tMe );
        }
    }
    else
    {
        if( m_fLastContextNumSel  ==  1 )
        {
            m_pcontextMenu->Remove( m_pmenuitemSep );
            m_pcontextMenu->Remove( m_pmenuitem1To1 );
            m_pcontextMenu->Remove( m_pmenuitemConfCall );
            m_pcontextMenu->Remove( m_pmenuitemEmail );
            m_pcontextMenu->Remove( m_pmenuitemIM );
            m_fLastContextNumSel = iNumSel;
        }
    }
#endif

    PopupMenu(m_pcontextMenu, point.x, point.y);
}


//*****************************************************************************
//
//  Menu handlers
//
//*****************************************************************************


//*****************************************************************************
void ParticipantPanel::OnMenuChangeOptions( wxCommandEvent& event )
{
    int iRet;

    // Create the dialog
    MeetingSetupDialog dlg(this);
    dlg.SetupForChangeOptions();

    int nRet = dlg.EditMeeting( m_strMeetingID, false, false, true, true );
    if( nRet == wxID_OK )
    {
        meeting_details_t   details;
        details = dlg.GetMeetingDetails( );

        iRet = controller_meeting_modify(CONTROLLER.controller, details, FALSE);
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuShowDetails( wxCommandEvent& event )
{
    m_MeetingView.Display( this, m_strMeetingID );
}


//*****************************************************************************
void ParticipantPanel::OnMenuLock( wxCommandEvent& event )
{
    int     iRet;
    iRet = controller_meeting_lock( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), event.IsChecked( ) );
}


//*****************************************************************************
void ParticipantPanel::OnMenuLectureMode( wxCommandEvent& event )
{
    int     iRet;
    iRet = controller_meeting_lecture_mode( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), event.IsChecked( ) );
}

//*****************************************************************************
void ParticipantPanel::OnMenuHoldMode( wxCommandEvent& event )
{
    int     iRet;
    iRet = controller_meeting_hold_mode( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), event.IsChecked( ) );
}


//*****************************************************************************
void ParticipantPanel::OnMenuOperator( wxCommandEvent& event )
{
    if( m_participant_tMe == NULL || (m_participant_tMe->status & StatusVoice) == 0)
    {
        wxMessageDialog dlg( this, _("You must join the meeting via phone to request operator assistance."), PRODNAME, wxOK | wxICON_EXCLAMATION );
        dlg.ShowModal( );
        return;
    }
    
    wxString    strT = wxT("part:") + m_strParticipantID;
    controller_participant_bridge_request( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), strT.mb_str(wxConvUTF8), 1,
                                           "request_operator" );
}


//*****************************************************************************
void ParticipantPanel::OnMenuNoJoinTones( wxCommandEvent& event )
{
    int     iRet;
    iRet = controller_meeting_mute_joinleave_tones( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), event.IsChecked( ) );
}


//*****************************************************************************
void ParticipantPanel::OnMenuRollCall( wxCommandEvent& event )
{
    int     iRet;
    iRet = controller_roll_call( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8) );
}


//*****************************************************************************
void ParticipantPanel::OnMenuHandUp( wxCommandEvent& event )
{
    bool        fHandUp     = event.IsChecked( );
    wxString    strT;
    strT.Append( wxT("part:") );
    strT.Append( m_strParticipantID );
    controller_participant_hand_up( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), strT.mb_str(wxConvUTF8), fHandUp );
}


//*****************************************************************************
void ParticipantPanel::OnMenuRecordMeeting( wxCommandEvent& event )
{
    int     iRet;
    iRet = controller_meeting_record( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), event.IsChecked( ) );
}


//*****************************************************************************
void ParticipantPanel::OnMenuPreviewRecording( wxCommandEvent& event )
{
    int                 iRet        = 1;

    if( m_participant_tMe )
        iRet = controller_recording_modify( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), m_participant_tMe->email, PreviewRecording );

    wxString    strBrand = wxGetApp().m_strProductName;
    if( iRet  ==  0 )
    {
        wxMessageDialog dlg( this, _("A link to your recording will be sent to your e-mail address in the next few minutes."), strBrand, wxOK | wxICON_INFORMATION );
        dlg.ShowModal( );
    }
    else
    {
        wxMessageDialog dlg( this, _("Unable to generate preview."), strBrand, wxOK | wxICON_INFORMATION );
        dlg.ShowModal( );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuResetRecording( wxCommandEvent& event )
{
    int                 iRet;
    wxString            strBlank( wxT("") );
    wxString            strTitle( _("Warning"), wxConvUTF8 );
    wxString            strMsg( _("The previously recorded files for this meeting: "), wxConvUTF8 );
    strMsg.Append( m_strMeetingID );
    strMsg.Append( wxString( _(" will be permanently deleted.\nProceed?"), wxConvUTF8 ));

    wxMessageDialog     dlg( this, strMsg, strTitle, wxYES_NO | wxICON_QUESTION );
    switch( dlg.ShowModal( ) )
    {
        case wxID_YES:
            wxLogDebug( wxT("Recordings are to be deleted") );
            iRet = controller_recording_modify( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), strBlank.mb_str(wxConvUTF8), ResetRecording );
            break;
        case wxID_NO:
            wxLogDebug( wxT("Recordings will NOT be deleted") );
            break;
        default:
            wxLogDebug( wxT("Recordings - we got something other than YES or NO") );
            break;
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuExitMeeting( wxCommandEvent& event )
{
    wxLogDebug( wxT("entering ParticipantPanel::OnMenuExitMeeting( )  --  event.GetInt( ) returned %d" ), event.GetInt( ) );

/*  Note:  code to specially handle a waitforstartdialog cancel was removed.
    The server should not stop a pending meeting if others are waiting...if it does, fix the
    bug there, not here.
*/

    bool    fSharingDoc = ( m_pShareControl->IsDocumentShared( ) );
    bool    fEndMeeting = false;
    bool    fExitMeeting = false;

    //  If they are a moderator or Admin see if they want the meeting to end or continue?
    if( m_fMeetingHasStarted && m_participant_tMe  &&
        ((m_participant_tMe->roll & RollModerator) || (m_participant_tMe->roll & RollHost) || m_fAdmin) )
    {
        EndMeetingDialog    dlg( m_pMMF );

        bool fAllowMeetingToContinue = ( m_meeting_t->options & ContinueWithoutModerator ) ? true : false;
        bool fUserIsHost = (m_participant_tMe->roll == RollHost);

        //  Tell dialog if meeting can continue without moderator
        if( fAllowMeetingToContinue )
        {
            dlg.m_fAllowContinueMeeting = fAllowMeetingToContinue;
            dlg.m_fContinueMeeting = fAllowMeetingToContinue;
            dlg.m_fEndMeeting = fUserIsHost;
        }
        else
        {
            dlg.m_fAllowContinueMeeting = false;
            dlg.m_fContinueMeeting = false;
            dlg.m_fEndMeeting = fUserIsHost;
        }
        dlg.InitializeDialog( );

        //  Tell dialog if user is host
        if( dlg.ShowModal( )  ==  wxID_OK )
        {
            if( dlg.GetEndNow( ) )
            {
                fEndMeeting = true;
            }
            else
            {
                if( fAllowMeetingToContinue  !=  dlg.GetContinue( ) )
                {
                    //  If mtg is not already set to allow continue without moderator, modify mtg
                    int     iVal    = (dlg.GetContinue( )) ? ContinueWithoutModerator : 0;
                    int     iRet;
                    iRet = controller_meeting_modify_options( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), ContinueWithoutModerator, iVal );
                }
                fExitMeeting = true;
            }
        }
        else
            return;
    }
    else
    {
        wxString    strMsg( _("Are you sure you want to leave this meeting?") );
        if( m_pMMF && m_pMMF->PhoneController() && m_pMMF->PhoneController()->IsInCall())
        {
            strMsg = strMsg + wxT("\n") + _("Exiting the meeting will also end your web call.");
        }
        wxMessageDialog     dlg( this, strMsg, wxGetApp().m_strProductName, wxYES_NO | wxICON_QUESTION );
        if( dlg.ShowModal( )  !=  wxID_YES )
            return;

        //  We are not moderator or host - just leave
        fExitMeeting = true;
    }

    if( fEndMeeting || fExitMeeting )
    {
        // Hang up PC phone
        if( m_pMMF && m_pMMF->PhoneController() && m_pMMF->PhoneController()->IsInCall())
        {
            m_pMMF->PhoneController()->HangupCall();
        }

        if( fSharingDoc )
        {
            DoShareStop( );
        }
    }
    
    if ( fEndMeeting )
    {
        controller_meeting_end( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8) );
    }

    if ( fExitMeeting )
    {
        controller_meeting_leave( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8) );

        m_pMMF->EndMeeting( );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuCallMe( wxCommandEvent& event )
{
    int     iRet;
    int     iDelta;

    iRet = event.GetId( );
    iDelta = iRet - XRCID("IDC_DLG_MTG_POPMENU_CALLME1");
    if( iDelta < 0  ||  iDelta > 7 )
    {
        iDelta = iRet - XRCID("IDC_DLG_MTG_MENU_CALLME1");
        if( iDelta < 0  ||  iDelta > 7 )
        {
            wxLogDebug( wxT("    in OnMenuCallMe( ) - ID is %d;  iDelta is %d,  iDelta is invalid, ignoring" ), iRet, iDelta );
            return;
        }
    }
    wxLogDebug( wxT("    in OnMenuCallMe( ) - ID is %d;  iDelta is %d"), iRet, iDelta );

    if ( IsPCPhone( iDelta) )
    {
        m_pMMF->PhoneController()->ConnectToMeeting();
        return;
    }

    if( /*(m_arrintCallMe.Item( iDelta ) == MeetingSetupDialog::Phone_Custom )  ||*/  (m_arrintCallMe.Item( iDelta ) == MeetingSetupDialog::Phone_EnterPhone ) )
    {
        //  Get phone number from user
        wxString    strMessage( _("Phone Number:\nThe entered number will be used for this call only."), wxConvUTF8 );
        wxString    strTitle( _("Enter Phone Number"), wxConvUTF8 );
        wxTextEntryDialog   dlg( this, strMessage, strTitle, _T(""), wxOK | wxCANCEL );
        if( dlg.ShowModal( )  ==  wxID_OK )
        {
            wxLogDebug( wxT("  the phone number the user entered: *%s*"), dlg.GetValue( ).wc_str( ) );
        }
        else
            return;

        //  Dial phone number entered by the user
        iRet = controller_participant_call( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), m_participant_tMe->part_id, 
                                            SelPhoneThis, dlg.GetValue( ).mb_str(wxConvUTF8), CallDirect );
    }
    else
    {
        iRet = controller_participant_call( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), m_participant_tMe->part_id, 
                                            SelPhoneThis, m_arrstrCallMePhone.Item( iDelta ).mb_str(wxConvUTF8), CallDirect );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuMuteMe( wxCommandEvent& event )
{
    bool    fMute   = event.IsChecked( );
    int     iRet;
    iRet = controller_participant_volume( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), m_participant_tMe->part_id, -1, -1, (fMute) ? 1 : 0 );
}


//*****************************************************************************
void ParticipantPanel::OnMenuVolumeMe( wxCommandEvent& event )
{
    int     iRet;
    int     iDelta;

    iRet = event.GetId( );
    iDelta = XRCID("IDC_DLG_MTG_POPMENU_VOLLEV1ME") - iRet;
    if( iDelta < 0  ||  iDelta > 7 )
    {
        iDelta = XRCID("IDC_DLG_MTG_MENU_VOLLEV1ME") - iRet;
        if( iDelta < 0  ||  iDelta > 7 )
        {
            wxLogDebug( wxT("  inside OnMenuVolumeMe( ) - ID is %d;  iDelta is %d,  iNewVol is invalid, ignoring" ), iRet, iDelta );
            return;
        }
    }

    iRet = controller_participant_volume( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), m_participant_tMe->part_id, iDelta, -1, -1 );
}


//*****************************************************************************
void ParticipantPanel::OnMenuInviteNew( wxCommandEvent& event )
{
    wxLogDebug( wxT("  entering ParticipantPanel::OnMenuInviteNew( )") );

    MeetingSetupDialog  dlg( this );
    dlg.SetupForMeetingInProgress( );

    int     iRet;
    iRet = dlg.EditMeeting( m_strMeetingID, false, true, false, true, true, 1, &m_participant_tMe );
    if( iRet == wxID_OK )
    {
        meeting_details_t   details;
        details = dlg.GetMeetingDetails( );

        iRet = controller_meeting_modify(CONTROLLER.controller, details, TRUE);
    }
}


//*****************************************************************************
bool ParticipantPanel::InviteeAlreadyInMeeting( invitee_t invt )
{
    return false;
}


//*****************************************************************************
void ParticipantPanel::OnMenuInviteSelected( wxCommandEvent& event )
{
    MeetingSetupDialog  dlg( this );
    dlg.SetupForMeetingInProgress( );

    int                 iRet;
    int                 iNumSel;
    participant_tArray  pArray;
    participant_t       p;

    participant_t       *parrpart_t;
    iNumSel = GetSelectedParticipants( pArray );
    if( iNumSel  <=  0 )
        return;

    parrpart_t = new participant_t[ iNumSel ];

    for( int i=0; i<iNumSel; i++ )
    {
        p = pArray.Item( i );
        parrpart_t[ i ] = p;
    }

    iRet = dlg.EditMeeting( m_strMeetingID, false, true, false, true, false, iNumSel, parrpart_t );

    if( iRet == wxID_OK )
    {
//        wxLogDebug( wxT("MeetingSetupDialog returned wxID_OK" ) );

        meeting_details_t   details;
        details = dlg.GetMeetingDetails( );

        iRet = controller_meeting_modify(CONTROLLER.controller, details, TRUE);
    }
    else
    {
//        wxLogDebug( wxT("MeetingSetupDialog did NOT return wxID_OK" ) );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuMakePresenter( wxCommandEvent& event )
{
    bool    fMakePres       = event.IsChecked( );
    int                 iRet;
    int                 iNumSel;
    participant_tArray  pArray;
    participant_t       p;

    iNumSel = GetSelectedParticipants( pArray );
    for( int i=0; i<iNumSel; i++ )
    {
        p = pArray.Item( i );
        iRet = controller_change_presenter( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), (fMakePres) ? p->part_id : "" );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuMakeModerator( wxCommandEvent& event )
{
    int                 iRet;
    int                 iNumSel;
    participant_tArray  pArray;
    participant_t       p;

    iNumSel = GetSelectedParticipants( pArray );
    for( int i=0; i<iNumSel; i++ )
    {
        p = pArray.Item( i );
        if (p->roll != RollHost)
            iRet = controller_participant_moderator( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), p->part_id, true );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuUnmakeModerator( wxCommandEvent& event )
{
    int                 iRet;
    int                 iNumSel;
    participant_tArray  pArray;
    participant_t       p;

    iNumSel = GetSelectedParticipants( pArray );
    for( int i=0; i<iNumSel; i++ )
    {
        p = pArray.Item( i );
        if (p->roll != RollHost)
            iRet = controller_participant_moderator( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), p->part_id, false );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuGrantRemoteControl( wxCommandEvent& event )
{
    int                 iRet;
    int                 iNumSel;
    participant_tArray  pArray;
    participant_t       p;

    iNumSel = GetSelectedParticipants( pArray );
    for( int i=0; i<iNumSel; i++ )
    {
        p = pArray.Item( i );
        iRet = controller_participant_remote_control( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), p->part_id, true );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuRevokeRemoteControl( wxCommandEvent& event )
{
    int                 iRet;
    int                 iNumSel;
    participant_tArray  pArray;
    participant_t       p;

    iNumSel = GetSelectedParticipants( pArray );
    for( int i=0; i<iNumSel; i++ )
    {
        p = pArray.Item( i );
        iRet = controller_participant_remote_control( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), p->part_id, false );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuRemoveSelected( wxCommandEvent& event )
{
    int                 iRet;
    int                 iNumSel;
    participant_tArray  pArray;
    participant_t       p;

    iNumSel = GetSelectedParticipants( pArray );
    for( int i=0; i<iNumSel; i++ )
    {
        p = pArray.Item( i );
        iRet = controller_participant_remove( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), p->part_id );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuMoveAllToMain(wxCommandEvent& event)
{
    if( m_submeeting_tMain  ==  NULL )
        return;     //  Should never get here...

    int                 iNumSel;
    participant_tArray  pArray;
    iNumSel = GetAllParticipants( pArray, false );
    for( int i=0; i<iNumSel; i++ )
    {
        controller_participant_move( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), pArray.Item( i )->part_id, m_submeeting_tMain->sub_id );
    }

    submeeting_tArray  sArray;
    iNumSel = GetAllSubmeetings( sArray, false );
    for( int i=0; i<iNumSel; i++ )
    {
        controller_submeeting_remove( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), sArray.Item( i )->sub_id );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuMoveSelToNewSub(wxCommandEvent& event)
{
    //  This needs to be handled in two separate operations...
    //  First, we tell the server to create the new sub-meeting.
    //    The server will notify us - via a callback - when the
    //    new sub-meeting is ready to be populated.
    //  Finally, the callback function does the actual populating.

    //  But first, save the current list of selected participants to be moved...
    int     iRet;
    iRet = GetSelectedParticipants( m_arrpart_tMoveToNew );

    wxString    strT;
    strT = wxString::Format( _T("%d"), GetNextSubNumber( ) );
    strT.Prepend( m_strParticipantID );
    m_strNewSubMtgID = strT;

    iRet = controller_submeeting_add(CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), strT.mb_str(wxConvUTF8) );
}


//*****************************************************************************
void ParticipantPanel::OnMenuMoveSelToSub(wxCommandEvent& event)
{
    wxArrayString   subMeetings;
    wxArrayString   subMeetingIDs;

    submeeting_t    sm;
    meeting_t       meeting;
    meeting = controller_find_meeting( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8) );
    if( meeting )
    {
        for( sm = meeting_next_submeeting(meeting,NULL); sm != NULL; sm = meeting_next_submeeting(meeting,sm))
        {
            wxString strT;
            GetSubmeetingTitle( sm, strT );
            subMeetings.Add( strT );
            subMeetingIDs.Add( wxString( sm->sub_id, wxConvUTF8 ) );
        }
    }

    wxSingleChoiceDialog    dlg( this, _("Select submeeting"), _("Move participant(s)"), subMeetings );
    if( dlg.ShowModal( )  ==  wxID_OK )
    {
        int                 iNumSel;
        participant_tArray  pArray;
        wxString            strT;
        strT = subMeetingIDs.Item( dlg.GetSelection( ) );

        iNumSel = GetSelectedParticipants( pArray );
        for( int i=0; i<iNumSel; i++ )
        {
            controller_participant_move( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), pArray.Item( i )->part_id, strT.mb_str(wxConvUTF8) );
        }
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuRemoveSub(wxCommandEvent& event)
{
    int                 iNumSelected;
    wxArrayTreeItemIds  selectedItems;
    iNumSelected = m_ParticipantTree->GetSelections( selectedItems );
    if( iNumSelected  !=  1 )
        return;                 //  Shouldn't ever get here
    if( !m_ParticipantTree->HasChildren( selectedItems.Item( 0 ) ) )
        return;                 //  Shouldn't ever get here
    if( m_submeeting_tMain  ==  NULL )
        return;                 //  Shouldn't ever get here

    int                 iNumSel;
    int                 iRet;
    participant_tArray  pArray;

    iNumSel = GetSelectedParticipants( pArray );
    for( int i=0; i<iNumSel; i++ )
    {
        iRet = controller_participant_move( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), pArray.Item( i )->part_id, m_submeeting_tMain->sub_id );
    }
    SubmeetingTreeItemData  *pSTID;
    pSTID = (SubmeetingTreeItemData *) m_ParticipantTree->GetItemData( selectedItems.Item( 0 ) );
    iRet = controller_submeeting_remove( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), pSTID->m_s->sub_id);
}

//*****************************************************************************
void ParticipantPanel::OnMenuMakeMePresenter( wxCommandEvent& event )
{
    int                 iRet;
    participant_t       p = m_participant_tMe;
    iRet = controller_change_presenter( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), p->part_id );
}

//*****************************************************************************
void ParticipantPanel::OnMenuShareDesktop( wxCommandEvent& event )
{
    if (!CheckRestart())
    {
        wxLogDebug( wxT("user declined share restart"));
        return;
    }

    m_pShareControl->EnableTransparent( PREFERENCES.m_fHidePresenterMeetingWindow );
    m_pMMF->DoSetTransparent( PREFERENCES.m_fHidePresenterMeetingWindow );
        
    int res = m_pShareControl->ShareDesktop();
    
    if ( res == ShareControl::ShareNoServer )
    {
        wxMessageDialog dlg( this, _("Unable to share application or desktop.\nShare server is not currently available."), PRODNAME, wxOK | wxICON_EXCLAMATION );
        dlg.ShowModal( );
        return;
    }

    if ( res !=  ShareControl::ShareStarting )
        return;

    // Pass on to doc share
    if( m_pDoc )
        m_pDoc->m_Meeting.SetIsPresenting( true );

    if( m_pMMF )
        m_pMMF->Hide( );
}


//*****************************************************************************
void ParticipantPanel::OnMenuShareApplication( wxCommandEvent& event )
{
    if (!CheckRestart())
    {
        wxLogDebug( wxT("user declined share restart"));
        return;
    }

    m_pShareControl->EnableTransparent( PREFERENCES.m_fHidePresenterMeetingWindow );
    m_pMMF->DoSetTransparent( PREFERENCES.m_fHidePresenterMeetingWindow );
    
    if (m_pShareControl->SelectDisplay())
        return;
    
    // fire off SelectApplication dialog
    SelectApplication dlg(this, m_pShareControl->GetDisplay());
    int     iRet    = wxID_OK;
    while( iRet  !=  wxID_CANCEL )
    {
        iRet = dlg.ShowModal( );
        if( iRet  ==  wxID_OK )
        {
            int sel = dlg.Selected;
            if( sel  <  0 )
                return;

            std::string     id;
            dlg.GetSelectedApps( id );
            
            int res = m_pShareControl->ShareApplication(id);
            if ( res == ShareControl::ShareNoServer )
            {
                wxMessageDialog dlg( this, _("Unable to share application or desktop.\nShare server is not currently available."), PRODNAME, wxOK | wxICON_EXCLAMATION );
                dlg.ShowModal( );
                return;
            }

            // Pass on to doc share
            if( m_pDoc )
                m_pDoc->m_Meeting.SetIsPresenting( true );

            if( m_pMMF )
                m_pMMF->Hide( );
            break;
        }
        if( iRet  ==  wxID_NO )
        {
            //  Actually, this means REFRESH
            dlg.refresh( event );
        }
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuSharePresentation( wxCommandEvent& event )
{
    wxLogDebug( wxT("entering ParticipantPanel::OnMenuSharePresentation( )") );
    DoOpenWhiteboard( true, false, true, true );
}


//*****************************************************************************
void ParticipantPanel::OnMenuShareDocument( wxCommandEvent& event )
{
    wxLogDebug( wxT("entering ParticipantPanel::OnMenuShareDocument( )") );
    DoOpenWhiteboard( true, false, true, false );
}


//*****************************************************************************
void ParticipantPanel::OnMenuShareDocumentExplorer( wxCommandEvent& event )
{
    wxLogDebug( wxT("entering ParticipantPanel::OnMenuShareDocumentExplorer( )") );
    DoOpenWhiteboard( true, false, false );
}


//*****************************************************************************
void ParticipantPanel::DoShareStop( )
{
    if( !m_pShareControl->IsDesktopShared( ) && !m_pShareControl->IsDocumentShared( ) )
    {
        wxLogDebug( wxT("entering ParticipantPanel::DoShareStop( )  --  nothing is shared so returning immediately") );

        if( m_pMMF )
        {
            m_pMMF->Show( );
            m_pMMF->GetMainPanel( )->DisplayPage( 0 );
            m_pMMF->DoSetTransparent( true );
        }
        return;
    }

    wxLogDebug( wxT("entering ParticipantPanel::DoShareStop( )") );

    if( m_pDocShareView )
        m_pDocShareView->Stop( );

    m_pShareControl->ShareEnd();

    // Pass on to doc share
    if( m_pDoc )
        m_pDoc->m_Meeting.SetIsPresenting( false );

    if( m_pMMF )
    {
        m_pMMF->Show( );
        m_pMMF->GetMainPanel( )->DisplayPage( 0 );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuShareStop( wxCommandEvent& event )
{
    DoShareStop( );
}


//*****************************************************************************
void ParticipantPanel::OnMenuOpenWhiteboard( wxCommandEvent& event )
{
    DoOpenWhiteboard( true, true, false );
}


//*****************************************************************************
void ParticipantPanel::OnMenuShareScale( wxCommandEvent& event )
{
    if( m_pShareControl->IsDocumentShared( ) )
    {
        wxLogDebug( wxT("entering ParticipantPanel::OnMenuShareScale( )") );
        if( m_pDocShareView )
            m_pDocShareView->ForceScale( );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuShareScroll( wxCommandEvent& event )
{
    if( m_pShareControl->IsDocumentShared( ) )
    {
        wxLogDebug( wxT("entering ParticipantPanel::OnMenuShareScroll( )") );
        if( m_pDocShareView )
            m_pDocShareView->ForceScroll( );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuShareFitToWidth( wxCommandEvent& event )
{
    if( m_pShareControl->IsDocumentShared( ) )
    {
        wxLogDebug( wxT("entering ParticipantPanel::OnMenuShareFitToWidth( )") );
        if( m_pDocShareView )
            m_pDocShareView->ForceFitToWidth( );
    }
}


//*****************************************************************************
void ParticipantPanel::DoOpenWhiteboard( bool fPresenter, bool fWhiteboard, bool fFileSelect, bool fPresentation )
{
    int     iRet;

    if (!fPresenter)
    {
        iRet = m_pShareControl->ShareDocument( fPresenter, fWhiteboard );
        if( iRet  !=  ShareControl::ShareStarting )
        {
            wxLogDebug( wxT("entering ParticipantPanel::DoOpenWhiteboard( )  --  fPresenter is %d  --  RETURNING IMMEDIATELY - returned %d"), fPresenter, iRet );
            return;
        }
    }
    else
    {
        if (!CheckRestart()) 
        {
            wxLogDebug( wxT("user declined share restart"));
            return;
        }
    }

    wxLogDebug( wxT("entering ParticipantPanel::DoOpenWhiteboard( )  --  fPresenter = %d - fWhiteboard = %d"), fPresenter, fWhiteboard );

    if( m_pDoc  ==  NULL )
        m_pDoc = (CIICDoc *) wxGetApp( ).m_pDocManager->CreateDocument( wxEmptyString, wxDOC_NEW );

    m_pDoc->m_Meeting.SetIsUserPresenter( fPresenter );
    m_pDoc->m_Meeting.SetIsPresenting( fPresenter );

    const char    *pChar;
    pChar = controller_get_docshare_server( CONTROLLER.controller, (const char *) m_strMeetingID.mb_str( wxConvUTF8 ) );
    wxString strT( pChar, wxConvUTF8 );

    wxString    strScreenName, strPassword, strCommunity;

    CONTROLLER.GetSignOnInfo( strScreenName, strPassword, strCommunity );

    m_pDoc->SetSessionInfo( strScreenName, strPassword, strT );
    m_pDoc->m_Meeting.SetMeetingId( m_strMeetingID );

    m_pDocShareView = (DocShareView *) m_pDoc->GetFirstView( );

    // (Re)-connect the wxEVT_DOCSHARE_EVENT handler for DocShareView to ensure events are handled in the correct order
    // ToDo: figure out if still needed--both handlers skip events
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_DOCSHARE_EVENT, DocshareEventHandler(DocShareView::OnDocshareEvent), 0, m_pDocShareView );
    CONTROLLER.Connect(   wxID_ANY, wxEVT_DOCSHARE_EVENT, DocshareEventHandler(DocShareView::OnDocshareEvent), 0, m_pDocShareView );

    m_pDocShareView->InitializeInfo( m_pDoc, m_strMeetingID );

    if( m_pMMF )
    {
        bool    fRet = m_pDocShareView->Start( m_pMMF, m_pMMF->GetMainPanel( )->GetDocSharePanel( ), fWhiteboard, fPresenter, fFileSelect, fPresentation );

        if( m_participant_tMe  &&  ((m_participant_tMe->status & StatusControl) != 0) )
            m_pDocShareView->RemoteControl( true );
        else
            m_pDocShareView->RemoteControl( false );

        if( fRet )
        {
            m_pMMF->GetMainPanel( )->DisplayPage( 2 );

            if (fPresenter)
            {
                // Make sure we're not transparent or recorder won't be able to capture screen.
                m_pMMF->DoSetTransparent( false );

                // Recorder is launched now, but data won't be captured/sent if recording is not enabled
                wxWindow* docWin = m_pMMF->GetMainPanel()->GetDocSharePanel();
                m_pShareControl->RecordShare( docWin->GetHandle(), m_pMMF->GetHandle());
            }
        }
        else
        {
            //  Whiteboard failed to start - time to clean up...
            m_pMMF->GetMainPanel( )->DisplayPage( 0 );
            m_pShareControl->ShareEnd( );
        }
    }
}


//*****************************************************************************
// OnDocshareEvent
void ParticipantPanel::OnDocshareEvent( DocshareEvent& event )
{
    switch( event.GetEvent( ) )
    {
        case DocShareStarted:
            wxLogDebug( wxT("entering ParticipantPanel::OnDocshareEvent( ) to handle DocShareStarted event") );
            DoOpenWhiteboard( false, false, false );
            event.Skip( );
            break;

        case DocShareEnded:
        {
            DoShareStop( );
            event.Skip( );
            break;
        }
        case DocShareDownloadPage:
        case DocShareConfigure:
        case DocShareDrawing:
        case ParticipantJoined:
            // Handled in DocShareView
            event.Skip( );
            break;

        default:
            wxLogDebug( wxT("entering ParticipantPanel::OnDocshareEvent( )  --  invalid event type received within a DocShareEvent - %d"), event.GetEvent( ) );
            break;
    }
}


//*****************************************************************************
//
//  Menu Update UI handlers
//
//*****************************************************************************


//*****************************************************************************
void ParticipantPanel::OnMenuLockUpdateUI( wxUpdateUIEvent &event )
{
//    event.Enable( ( m_meeting_t != NULL ) );
}


//*****************************************************************************
void ParticipantPanel::OnMenuHandUpUpdateUI( wxUpdateUIEvent &event )
{
    event.Check( m_participant_tMe && (m_participant_tMe->status & StatusHandUp) );
}


//*****************************************************************************
void ParticipantPanel::OnMenuCallMeUpdateUI( wxUpdateUIEvent &event )
{    
}


//*****************************************************************************
void ParticipantPanel::SetupCallMeMenu( )
{
    if (!CONTROLLER.IsTelephonyAvailable())
    {
        return;
    }

    if( m_participant_tMe )
    {
        FillPhoneList( m_participant_tMe );
    }

    m_TBBMyPhone->Enable( );

    if( NULL == m_pPopMenuMyPhone )
        m_pPopMenuMyPhone = m_pPopupMenuBar->GetMenu( m_pPopupMenuBar->FindMenu( _T("IDC_DLG_MTG_POPMENU_MYPHONE")) );

    wxMenuBar *pBar = m_pMainMenuBar;
    if( pBar )
    {
        pBar->EnableTop( kMenuIndexMyPhone, true );

        int         i;
        wxMenu      *pMenu          = NULL;
        wxMenu      *pMenuCallMe    = NULL;
        wxMenu      *pPopMenuCallMe = NULL;
        wxMenuItem  *pItem;
        for( i=0; i<10; i++ )
        {
            pItem = pBar->FindItem( XRCID("IDC_DLG_MTG_MENU_CALLME1") + i, &pMenu );
            if( pMenu  &&  pMenuCallMe == NULL )
                pMenuCallMe = pMenu;
            if( pItem  &&  pMenu )
            {
                pMenu->Delete( pItem );
            }

            pItem = m_pPopMenuMyPhone->FindItem( XRCID("IDC_DLG_MTG_POPMENU_CALLME1") + i, &pMenu );
            if( pMenu  &&  pPopMenuCallMe == NULL )
                pPopMenuCallMe = pMenu;
            if( pItem  &&  pMenu )
            {
                pMenu->Delete( pItem );
            }
        }
        for( i=0; i<(int) m_arrstrCallMe.GetCount( ); i++ )
        {
            if( pMenuCallMe )
                pItem = pMenuCallMe->Append( XRCID("IDC_DLG_MTG_MENU_CALLME1") + i, m_arrstrCallMe.Item( i ) );
            if( pPopMenuCallMe )
                pItem = pPopMenuCallMe->Append( XRCID("IDC_DLG_MTG_POPMENU_CALLME1") + i, m_arrstrCallMe.Item( i ) );
        }
    }
}


//*****************************************************************************
void ParticipantPanel::FindFollowmeNumber( participant_t part_t, wxString& strRet, bool& isPCPhone )
{
    contact_t       contact     = 0;
  
    isPCPhone = false;

    if( part_t  &&  part_t->user_id  &&  *part_t->user_id )
    {
        contact = CONTROLLER.LookupCABContact( part_t->user_id );
        if( !contact )
        {
            contact = CONTROLLER.LookupPABContact( part_t->user_id );
        }
    }

    if( contact )
    {
        // This participant has contact information.
        if( contact->defphone != SelPhoneNone )
        {
            if( contact->defphone == SelPhoneBus )
            {
                strRet = wxString( contact->busphone, wxConvUTF8 );
            }
            else if( contact->defphone == SelPhoneHome )
            {
                strRet = wxString( contact->homephone, wxConvUTF8 );
            }
            else if( contact->defphone == SelPhoneMobile )
            {
                strRet = wxString( contact->mobilephone, wxConvUTF8 );
            }
            else if( contact->defphone == SelPhoneOther )
            {
                strRet = wxString( contact->otherphone, wxConvUTF8 );
            }
            else if( contact->defphone == SelPhonePC )
            {
                isPCPhone = true;
            }
        }
    }
}


//*****************************************************************************
void ParticipantPanel::FillPhoneList( participant_t part_t )
{
    contact_t       contact     = 0;
    wxString        contactTag;

    m_arrintCallMe.Empty( );
    m_arrstrCallMe.Empty( );
    m_arrstrCallMePhone.Empty( );

    if ( part_t )
    {
        wxString invt_phone( part_t->phone, wxConvUTF8 );

        if( part_t->selphone == SelPhoneThis && !invt_phone.IsEmpty() )
        {
            wxString lastSelectedPhone( invt_phone );
            lastSelectedPhone += _(" ");
            lastSelectedPhone += _("(Last selected)");
            m_arrstrCallMe.Add( lastSelectedPhone );
            m_arrstrCallMePhone.Add( lastSelectedPhone );
            m_arrintCallMe.Add( MeetingSetupDialog::Phone_Custom );
        }

        if( part_t->user_id  &&  *part_t->user_id )
        {
            contact = CONTROLLER.LookupCABContact( part_t->user_id );
            if( !contact )
            {
                contact = CONTROLLER.LookupPABContact( part_t->user_id );
                if( contact )
                {
                    contactTag = MeetingSetupDialog::contactTagText( wxContactCellEditor::ContactFromPAB );
                }
            }
            else
            {
                contactTag = MeetingSetupDialog::contactTagText( wxContactCellEditor::ContactFromCAB );
            }
        }

        if( contact )
        {
            // This invitee has contact information.
            wxString first_name(contact->first, wxConvUTF8 );
            wxString last_name(contact->last, wxConvUTF8 );
            wxString name;

            name = first_name;
            name += _(" ");
            name += last_name;

            wxString contact_name;

            // Contact in the system

            if( contact->defphone != SelPhoneNone )
            {
                wxString follow_me_fmt;
                follow_me_fmt = _("Follow me (%s %s)");

                wxString follow_me_type;
                wxString follow_me;
                wxString follow_me_number;

                if( contact->defphone == SelPhoneBus )
                {
                    follow_me_type = _("Business");
                    follow_me_number = wxString( contact->busphone, wxConvUTF8 );
                }
                else if( contact->defphone == SelPhoneHome )
                {
                    follow_me_type = _("Home");
                    follow_me_number = wxString( contact->homephone, wxConvUTF8 );
                }
                else if( contact->defphone == SelPhoneMobile )
                {
                    follow_me_type = _("Mobile");
                    follow_me_number = wxString( contact->mobilephone, wxConvUTF8 );
                }
                else if( contact->defphone == SelPhoneOther )
                {
                    follow_me_type = _("Other");
                    follow_me_number = wxString( contact->otherphone, wxConvUTF8 );
                }
                else if( contact->defphone == SelPhonePC )
                {
                    follow_me_type = _("Web phone");
                }
                if( !follow_me_type.IsEmpty() )
                {
                    follow_me = wxString::Format( follow_me_fmt, follow_me_type.c_str(), follow_me_number.c_str() );
                    m_arrstrCallMe.Add( follow_me );
                    m_arrstrCallMePhone.Add( follow_me_number );
                    m_arrintCallMe.Add( MeetingSetupDialog::Phone_FollowMe );
                }

            }

            if( contact->busphone && *contact->busphone )
            {
                wxString business( _("Business") );
                wxString business_phone( contact->busphone, wxConvUTF8 );
                m_arrstrCallMe.Add( MeetingSetupDialog::formatComboEntry( contact->busphone, business, contactTag,  name ) );
                m_arrstrCallMePhone.Add( business_phone );
                m_arrintCallMe.Add( MeetingSetupDialog::Phone_Business );
            }

            if( contact->homephone && *contact->homephone )
            {
                wxString home( _("Home") );
                wxString home_phone( contact->homephone, wxConvUTF8 );
                m_arrstrCallMe.Add( MeetingSetupDialog::formatComboEntry( contact->homephone, home, contactTag,  name ) );
                m_arrstrCallMePhone.Add( home_phone );
                m_arrintCallMe.Add( MeetingSetupDialog::Phone_Home );
            }

            if( contact->mobilephone && *contact->mobilephone )
            {
                wxString mobile( _("Mobile") );
                wxString mobile_phone( contact->mobilephone, wxConvUTF8 );
                m_arrstrCallMe.Add( MeetingSetupDialog::formatComboEntry( contact->mobilephone, mobile, contactTag,  name ) );
                m_arrstrCallMePhone.Add( mobile_phone );
                m_arrintCallMe.Add( MeetingSetupDialog::Phone_Mobile );
            }

            if( contact->otherphone && *contact->otherphone )
            {
                wxString other( _("Other") );
                wxString other_phone( contact->otherphone, wxConvUTF8 );
                m_arrstrCallMe.Add( MeetingSetupDialog::formatComboEntry( contact->otherphone, other, contactTag,  name ) );
                m_arrstrCallMePhone.Add( other_phone );
                m_arrintCallMe.Add( MeetingSetupDialog::Phone_Other );
            }
        }

        // If a phone number was entered, but not used, display it 
        if ( part_t->selphone == SelPhoneNone && !invt_phone.IsEmpty() )
        {
            wxString lastSelectedPhone( invt_phone );
            m_arrstrCallMe.Add( lastSelectedPhone );
            m_arrstrCallMePhone.Add( lastSelectedPhone );
            m_arrintCallMe.Add( MeetingSetupDialog::Phone_Custom );
        }

        m_arrstrCallMe.Add( _("Use web phone") );
        m_arrstrCallMePhone.Add( wxString( wxT("") ) );
        m_arrintCallMe.Add( MeetingSetupDialog::Phone_PC );
    }
    
    m_arrstrCallMe.Add( _("Enter phone # ...") );
    m_arrstrCallMePhone.Add( wxString( wxT("") ) );
    m_arrintCallMe.Add( MeetingSetupDialog::Phone_EnterPhone );
}


//*****************************************************************************
void ParticipantPanel::FillEmailList( participant_t part_t )
{
    contact_t       contact     = 0;
    wxString        contactTag;

    m_arrintEmail.Empty( );
    m_arrstrEmail.Empty( );
    m_arrstrEmailAddr.Empty( );

    if( !part_t )
        return;

    if( part_t->user_id  &&  *part_t->user_id )
    {
        contact = CONTROLLER.LookupCABContact( part_t->user_id );
        if( !contact )
        {
            contact = CONTROLLER.LookupPABContact( part_t->user_id );
            if( contact )
            {
                contactTag = MeetingSetupDialog::contactTagText( wxContactCellEditor::ContactFromPAB );
            }
        }
        else
        {
            contactTag = MeetingSetupDialog::contactTagText( wxContactCellEditor::ContactFromCAB );
        }
    }

    wxString invt_email( part_t->email, wxConvUTF8 );
    if( part_t->selemail == SelEmailThis  &&  !invt_email.IsEmpty( ) )
    {
        m_arrstrEmail.Add( invt_email );
        m_arrintEmail.Add( MeetingSetupDialog::Email_Custom );
        m_arrstrEmailAddr.Add( invt_email );
    }

    if( contact )
    {
        wxString    first_name( contact->first, wxConvUTF8 );
        wxString    last_name( contact->last, wxConvUTF8 );
        wxString    name;
        name = first_name;
        name += _(" ");
        name += last_name;
        wxString    email1_value( contact->email, wxConvUTF8 );
        wxString    email2_value( contact->email2, wxConvUTF8 );
        wxString    email3_value( contact->email3, wxConvUTF8 );

        if( !email1_value.IsEmpty() )
        {
            wxString email1( _("Email 1") );
            m_arrstrEmail.Add( MeetingSetupDialog::formatComboEntry( contact->email, email1, contactTag,  name ) );
            m_arrintEmail.Add( MeetingSetupDialog::Email_1 );
            m_arrstrEmailAddr.Add( email1_value );
        }

        if( !email2_value.IsEmpty() )
        {
            wxString email2( _("Email 2") );
            m_arrstrEmail.Add( MeetingSetupDialog::formatComboEntry( contact->email2, email2, contactTag,  name ) );
            m_arrintEmail.Add( MeetingSetupDialog::Email_2 );
            m_arrstrEmailAddr.Add( email2_value );
        }

        if( !email3_value.IsEmpty() )
        {
            wxString email3( _("Email 3") );
            m_arrstrEmail.Add( MeetingSetupDialog::formatComboEntry( contact->email3, email3, contactTag,  name ) );
            m_arrintEmail.Add( MeetingSetupDialog::Email_3 );
            m_arrstrEmailAddr.Add( email3_value );
        }
    }

    // Email address was entered, but not selected--add to list
    if( part_t->selemail == SelEmailNone && !invt_email.IsEmpty() )
    {
        wxString lastSelectedEmail( invt_email );
        m_arrstrEmail.Add( lastSelectedEmail );
        m_arrintEmail.Add( MeetingSetupDialog::Email_Custom );
        m_arrstrEmailAddr.Add( invt_email );
    }

    m_arrstrEmail.Add( _("Enter Email ...") );
    m_arrintEmail.Add( MeetingSetupDialog::Email_EnterEmail );
}


//*****************************************************************************
void ParticipantPanel::FillIMList( participant_t part_t )
{
    contact_t       contact     = 0;
    wxString        contactTag;

    m_arrintIM.Empty( );
    m_arrstrIM.Empty( );

    if ( !part_t )
        return;

    if( part_t->user_id  &&  *part_t->user_id )
    {
        contact = CONTROLLER.LookupCABContact( part_t->user_id );
        if( !contact )
        {
            contact = CONTROLLER.LookupPABContact( part_t->user_id );
            if( contact )
            {
                contactTag = MeetingSetupDialog::contactTagText( wxContactCellEditor::ContactFromPAB );
            }
        }
        else
        {
            contactTag = MeetingSetupDialog::contactTagText( wxContactCellEditor::ContactFromCAB );
        }
    }

    bool    force_yahoo         = (part_t->notify_type & NotifyYahoo) != 0;
    bool    force_messenger     = (part_t->notify_type & NotifyMessenger ) != 0;
    bool    force_aim           = (part_t->notify_type & NotifyAIM) != 0;
    bool    yahoo_screen        = false;
    bool    messenger_screen    = false;
    bool    aim_screen          = false;

    if( contact )
    {
        yahoo_screen = ( contact->yahooscreen && *contact->yahooscreen );
        messenger_screen = ( contact->msnscreen && *contact->msnscreen );
        aim_screen = ( contact->aimscreen && *contact->aimscreen );
    }

    wxString open_zon( PRODNAME );
    wxString messenger( _("Messenger") );
    wxString yahoo( _("Yahoo") );
    wxString aim( _("AIM") );

    wxString available( _(" (Available)") );
    wxString not_available( _(" (Not available)") );

    wxString choice;

//    m_arrstrIM.Add( _("Don't Send") );
//    m_arrintIM.Add( NotifyNone );

    m_arrstrIM.Add( _("Any online") );
    m_arrintIM.Add( NotifyIMAny );

    if( contact )
    {
        m_arrstrIM.Add( open_zon );
        m_arrintIM.Add( NotifyIIC );
    }

    if( force_messenger || messenger_screen )
    {
        choice = messenger;
        choice += not_available;
        m_arrstrIM.Add( choice );
        m_arrintIM.Add( NotifyMessenger );
    }

    if( force_yahoo || yahoo_screen )
    {
        choice = yahoo;
        choice += not_available;
        m_arrstrIM.Add( choice );
        m_arrintIM.Add( NotifyYahoo );
    }

    if( force_aim || aim_screen )
    {
        choice = aim;
        choice += not_available;
        m_arrstrIM.Add( choice );
        m_arrintIM.Add( NotifyAIM );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuCallMeItemUpdateUI( wxUpdateUIEvent &event )
{
}


//*****************************************************************************
void ParticipantPanel::OnMenuMuteMeUpdateUI( wxUpdateUIEvent &event )
{
    event.Check( m_participant_tMe && (m_participant_tMe->mute) );
}


//*****************************************************************************
void ParticipantPanel::OnMenuVolumeMeUpdateUI( wxUpdateUIEvent &event )
{
    int     iRet;
    int     iDelta;

    iRet = event.GetId( );
    iDelta = XRCID("IDC_DLG_MTG_POPMENU_VOLLEV1ME") - iRet;
    if( iDelta < 0  ||  iDelta > 7 )
    {
        iDelta = XRCID("IDC_DLG_MTG_MENU_VOLLEV1ME") - iRet;
        if( iDelta < 0  ||  iDelta > 7 )
        {
            wxLogDebug( wxT("  ID is %d;  iDelta is %d - UpdateUI is not for menubar - RETURNING/ABORTING" ), iRet, iDelta );
            return;
        }
    }

    event.Check( (iDelta == ((m_participant_tMe) ? m_participant_tMe->volume : 3)) );
}


//*****************************************************************************
void ParticipantPanel::OnMenuHangUpMeUpdateUI( wxUpdateUIEvent &event )
{
}

//*****************************************************************************
void ParticipantPanel::OnMenuMakeMePresenterUpdateUI( wxUpdateUIEvent &event )
{
    bool    flag1   = m_pShareControl->IsModerator( );
    bool    flag2   = CONTROLLER.CanShareDeskTop( )  &&  IsPresenter( );
    event.Enable( flag1 && !flag2 );
}

//*****************************************************************************
void ParticipantPanel::OnMenuShareDesktopUpdateUI( wxUpdateUIEvent &event )
{
    bool    flag1   = (( m_pShareControl->IsDesktopShared( ) || m_pShareControl->IsDocumentShared( ) ) ? false : true );
    bool    flag2   = CONTROLLER.CanShareDeskTop( )  &&  IsPresenter( );
    event.Enable( flag1 && flag2 );
}


//*****************************************************************************
void ParticipantPanel::OnMenuShareApplicationUpdateUI( wxUpdateUIEvent &event )
{
    bool    flag1   = (( m_pShareControl->IsDesktopShared( ) || m_pShareControl->IsDocumentShared( ) ) ? false : true );
    bool    flag2   = CONTROLLER.CanShareApplication( )  &&  IsPresenter( );
    event.Enable( flag1 && flag2 );
}


//*****************************************************************************
void ParticipantPanel::OnMenuShareDocumentUpdateUI( wxUpdateUIEvent &event )
{
    bool    flag1   = (( m_pShareControl->IsDesktopShared( ) || m_pShareControl->IsDocumentShared( ) ) ? false : true );
    bool    flag2   = CONTROLLER.CanShareDocument( )  &&  IsPresenter( );
    event.Enable( flag1 && flag2 );
}


//*****************************************************************************
void ParticipantPanel::OnMenuShareStopUpdateUI( wxUpdateUIEvent &event )
{
    bool    flag1   = (( m_pShareControl->IsDesktopShared( ) || m_pShareControl->IsDocumentShared( ) ) ? true : false );
    bool    flag2   = CONTROLLER.CanShareDeskTop( )  &&  IsPresenter( );
    event.Enable( flag1 && flag2 );
}


//*****************************************************************************
void ParticipantPanel::OnMenuOpenWhiteboardUpdateUI( wxUpdateUIEvent &event )
{
    bool    flag1   = (( m_pShareControl->IsDesktopShared( ) || m_pShareControl->IsDocumentShared( ) ) ? false : true );
    bool    flag2   = CONTROLLER.CanShareWhiteboard( )  &&  IsPresenter( );
    event.Enable( flag1 && flag2 );
}


//*****************************************************************************
void ParticipantPanel::OnMenuShareScaleUpdateUI( wxUpdateUIEvent &event )
{
    bool    flag1   = (( m_pShareControl->IsDesktopShared( ) || m_pShareControl->IsDocumentShared( ) ) ? true : false );
    bool    flag2   = false;
    if( m_pDocShareView  &&  !m_pDocShareView->IsWhiteboard( ) )
        flag2 = m_pDocShareView->IsScrollOnly( ) || m_pDocShareView->IsFitToWidth( );

    event.Enable( flag1 && flag2 );
}


//*****************************************************************************
void ParticipantPanel::OnMenuShareScrollUpdateUI( wxUpdateUIEvent &event )
{
    bool    flag1   = (( m_pShareControl->IsDesktopShared( ) || m_pShareControl->IsDocumentShared( ) ) ? true : false );
    bool    flag2   = false;
    if( m_pDocShareView  &&  !m_pDocShareView->IsWhiteboard( ) )
        flag2 = !m_pDocShareView->IsScrollOnly( );

    event.Enable( flag1 && flag2 );
}


//*****************************************************************************
void ParticipantPanel::OnMenuShareFitToWidthUpdateUI( wxUpdateUIEvent &event )
{
    bool    flag1   = (( m_pShareControl->IsDesktopShared( ) || m_pShareControl->IsDocumentShared( ) ) ? true : false );
    bool    flag2   = false;
    if( m_pDocShareView  &&  !m_pDocShareView->IsWhiteboard( ) )
        flag2 = !m_pDocShareView->IsFitToWidth( );

    event.Enable( flag1 && flag2 );
}


//*****************************************************************************
void ParticipantPanel::OnMenuInviteNewUpdateUI( wxUpdateUIEvent &event )
{
}


//*****************************************************************************
void ParticipantPanel::OnMenuInviteSelUpdateUI( wxUpdateUIEvent &event )
{
}

//*****************************************************************************
void ParticipantPanel::OnMenuMakePresUpdateUI( wxUpdateUIEvent &event )
{
    //  Always disable if more than one participant is selected...
    participant_t p = GetSingleSelection();
    if (p)
    {
        event.Enable( true );
        event.Check( p->status & StatusPresenter );
    }
    else
        event.Enable( false );
}


//*****************************************************************************
void ParticipantPanel::OnMenuMakeModeratorUpdateUI( wxUpdateUIEvent &event )
{
    participant_t p = GetSingleSelection();
    if (p)
    {
        event.Enable( p->roll == RollModerator ? false : true );
    }
    else
        event.Enable( true );
}


//*****************************************************************************
void ParticipantPanel::OnMenuUnmakeModeratorUpdateUI( wxUpdateUIEvent &event )
{
    participant_t p = GetSingleSelection();
    if (p)
    {
        event.Enable( p->roll == RollModerator ? true : false );
    }
    else
        event.Enable( true );
}


//*****************************************************************************
void ParticipantPanel::OnMenuGrantRemoteControlUpdateUI( wxUpdateUIEvent &event )
{
    if ( !CONTROLLER.CanShareRemoteControl( ) )
    {
        event.Enable( false );
        return;
    }

    participant_t p = GetSingleSelection();
    if (p)
    {
        event.Enable( ( p->status & StatusControl ) ? false : true );
    }
    else
        event.Enable( true );
}


//*****************************************************************************
void ParticipantPanel::OnMenuRevokeRemoteControlUpdateUI( wxUpdateUIEvent &event )
{
    if ( !CONTROLLER.CanShareRemoteControl( ) )
    {
        event.Enable( false );
        return;
    }

    participant_t p = GetSingleSelection();
    if (p)
    {
        event.Enable( ( p->status & StatusControl ) ? true : false );
    }
    else
        event.Enable( true );
}


//*****************************************************************************
void ParticipantPanel::OnMenuHandDownUpdateUI( wxUpdateUIEvent &event )
{
}


//*****************************************************************************
void ParticipantPanel::OnMenuCallParticipantUpdateUI( wxUpdateUIEvent &event )
{
    //  Go fetch the single(!!) selected participant's phone numbers...
    participant_t   p       = GetFirstSelectedParticipant( );
    if( p == NULL )
        return;
        
    FillPhoneList( p );

    wxMenuBar   *pBar = m_pMainMenuBar;
    if( pBar )
    {
        int         i;
        wxMenu      *pMenu          = NULL;
        wxMenu      *pPopMenuCallMe = NULL;
        wxMenuItem  *pItem;
        for( i=0; i<10; i++ )
        {
            if( m_pPopMenuParticipant )
            {
                pItem = m_pPopMenuParticipant->FindItem( XRCID("IDC_DLG_MTG_POPMENU_CALL1") + i, &pMenu );
                if( pMenu  &&  pPopMenuCallMe == NULL )
                    pPopMenuCallMe = pMenu;
                if( pItem  &&  pMenu )
                {
                    pMenu->Delete( pItem );
                }
            }
        }
        for( i=0; i<(int) m_arrstrCallMe.GetCount( ); i++ )
        {
            if( pPopMenuCallMe )
                pItem = pPopMenuCallMe->Append( XRCID("IDC_DLG_MTG_POPMENU_CALL1") + i, m_arrstrCallMe.Item( i ) );
        }
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuMuteParticipantUpdateUI( wxUpdateUIEvent &event )
{
    int                 iNumSelected;
    wxArrayTreeItemIds  selectedItems;
    iNumSelected = m_ParticipantTree->GetSelections( selectedItems );
    if( iNumSelected  !=  1 )
    {
        event.Enable( true );
    }
    else if( m_ParticipantTree->HasChildren( selectedItems.Item( 0 ) ) )
    {
        event.Enable( true );
    }
    else
    {
        event.Enable( true );

        //  Check if selected participant is already presenter
        ParticipantTreeItemData *pPTID;
        pPTID = (ParticipantTreeItemData *) m_ParticipantTree->GetItemData( selectedItems.Item( 0 ) );
        if( pPTID )
        {
            event.Enable( (pPTID->m_p->mute ) ? false : true );
        }
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuUnmuteParticipantUpdateUI( wxUpdateUIEvent &event )
{
    int                 iNumSelected;
    wxArrayTreeItemIds  selectedItems;
    iNumSelected = m_ParticipantTree->GetSelections( selectedItems );
    if( iNumSelected  !=  1 )
    {
        event.Enable( true );
    }
    else if( m_ParticipantTree->HasChildren( selectedItems.Item( 0 ) ) )
    {
        event.Enable( true );
    }
    else
    {
        event.Enable( true );

        //  Check if selected participant is already presenter
        ParticipantTreeItemData *pPTID;
        pPTID = (ParticipantTreeItemData *) m_ParticipantTree->GetItemData( selectedItems.Item( 0 ) );
        if( pPTID )
        {
            event.Enable( (pPTID->m_p->mute ) ? true : false );
        }
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuVolumeParticipantUpdateUI( wxUpdateUIEvent &event )
{
    int     iRet;
    int     iDelta;

    iRet = event.GetId( );
    iDelta = XRCID("IDC_DLG_MTG_POPMENU_VOLLEV1PART") - iRet;
    if( iDelta < 0  ||  iDelta > 7 )
    {
        wxLogDebug( wxT("  ID is %d;  iDelta is %d - UpdateUI is not for menubar - RETURNING/ABORTING" ), iRet, iDelta );
        return;
    }

    //  Use volume level of FIRST selected participant...
    participant_t       p               = GetFirstSelectedParticipant( );
    int                 iCurrVolLevel   = ( p ) ? p->volume : 3;
    event.Check( (iDelta == iCurrVolLevel) );
}


//*****************************************************************************
void ParticipantPanel::OnMenuHangUpParticipantUpdateUI( wxUpdateUIEvent &event )
{
}


//*****************************************************************************
void ParticipantPanel::OnMenuRemoveSelectedUpdateUI( wxUpdateUIEvent &event )
{
}


//*****************************************************************************
void ParticipantPanel::OnMenuMoveAllToMainUpdateUI( wxUpdateUIEvent &event )
{
}


//*****************************************************************************
void ParticipantPanel::OnMenuMoveSelToNewSubUpdateUI( wxUpdateUIEvent &event )
{
}


//*****************************************************************************
void ParticipantPanel::OnMenuMoveSelToSubUpdateUI( wxUpdateUIEvent &event )
{
}


//*****************************************************************************
void ParticipantPanel::OnMenuRemoveSubUpdateUI( wxUpdateUIEvent &event )
{
    bool                fEnable     = true;
    int                 iNumSelected;
    wxArrayTreeItemIds  selectedItems;
    iNumSelected = m_ParticipantTree->GetSelections( selectedItems );
    if( (iNumSelected  !=  1 )  ||
        (!m_ParticipantTree->HasChildren( selectedItems.Item( 0 ) ) )  ||
        ( m_submeeting_tMain  ==  NULL ) )
        fEnable = false;
    if( fEnable )
    {
        SubmeetingTreeItemData  *pSTID;
        pSTID = (SubmeetingTreeItemData *) m_ParticipantTree->GetItemData( selectedItems.Item( 0 ) );
        if( pSTID  &&  pSTID->m_s )
        {
            if( pSTID->m_s->is_main )
                fEnable = false;
        }
        else
            fEnable = false;
    }
    event.Enable( fEnable );
}


//*****************************************************************************
void ParticipantPanel::OnMenuContVolumeItemUpdateUI( wxUpdateUIEvent &event )
{
    int     iRet    = event.GetId( );
    if( (iRet < XRCID("IDC_DLG_MTG_CONTMENU_VOLLEV8PART"))  ||  (iRet > XRCID("IDC_DLG_MTG_CONTMENU_VOLLEV1PART")) )
        return;

    int     iDelta;
    iDelta = XRCID("IDC_DLG_MTG_CONTMENU_VOLLEV1PART") - iRet;
    if( iDelta < 0  ||  iDelta > 7 )
    {
        wxLogDebug( wxT("  ID is %d;  iDelta is %d - UpdateUI is not for menubar - RETURNING/ABORTING" ), iRet, iDelta );
        return;
    }

    //  Go fetch the single(!!) selected participant's volume.  If more than one
    //   participant is selected put the checkmark at volume level 3.
    participant_t   p               = GetFirstSelectedParticipant( );
    int             iCurrVolLevel   = ( p ) ? p->volume : 3;
    event.Check( (iDelta == iCurrVolLevel) );
}


//*****************************************************************************
void ParticipantPanel::OnMenuContCallItemUpdateUI( wxUpdateUIEvent &event )
{
#ifndef LINUX
    int     iRet    = event.GetId( );
    if( iRet  !=  XRCID("IDC_DLG_MTG_CONTMENU_1TO1CALL1") )
    {
        if( iRet  !=  XRCID("IDC_DLG_MTG_CONTMENU_CONFCALL1") )
        {
            return;
        }
    }
#endif

    //  Go fetch the single(!!) selected participant's phone numbers...
    participant_t   p       = GetFirstSelectedParticipant( );
    if( p == NULL) 
        return;

    FillPhoneList( p );

    int         i;
    int         iNumStrs        = (int) m_arrstrCallMe.GetCount( );
    wxMenu      *pM             = NULL;
    wxMenuItem  *pItem;
    for( i=0; i<10; i++ )
    {
        pItem = m_pcontextMenu1To1->FindItem( XRCID("IDC_DLG_MTG_CONTMENU_1TO1CALL1") + i, &pM );
        if( pItem  &&  pM )
        {
            if( i < iNumStrs )
            {
                pItem->Enable( true );
                pItem->SetText( m_arrstrCallMe.Item( i ) );
            }
            else
            {
                pM->Delete( pItem );
            }
        }
        else
        {
            if( i < iNumStrs )
                pItem = m_pcontextMenu1To1->Append( XRCID("IDC_DLG_MTG_CONTMENU_1TO1CALL1") + i, m_arrstrCallMe.Item( i ) );
        }

        pItem = m_pcontextMenuConfCall->FindItem( XRCID("IDC_DLG_MTG_CONTMENU_CONFCALL1") + i, &pM );
        if( pItem  &&  pM )
        {
            if( i < iNumStrs )
            {
                pItem->Enable( true );
                pItem->SetText( m_arrstrCallMe.Item( i ) );
            }
            else
            {
                pM->Delete( pItem );
            }
        }
        else
        {
            if( i < iNumStrs )
                pItem = m_pcontextMenuConfCall->Append( XRCID("IDC_DLG_MTG_CONTMENU_CONFCALL1") + i, m_arrstrCallMe.Item( i ) );
        }
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuEmailItemUpdateUI( wxUpdateUIEvent &event )
{
#ifndef LINUX
    int     iRet    = event.GetId( );
     if( iRet  !=  XRCID("IDC_DLG_MTG_CONTMENU_EMAIL1") )
        return;
#endif

    //  Go fetch the single(!!) selected participant's emails...
    participant_t   p       = GetFirstSelectedParticipant( );
    if( p == NULL) 
        return;

    FillEmailList( p );

    int         i;
    int         iNumStrs        = (int) m_arrstrEmail.GetCount( );
    wxMenu      *pM             = NULL;
    wxMenuItem  *pItem;
    for( i=0; i<10; i++ )
    {
        pItem = m_pcontextMenuEmail->FindItem( XRCID("IDC_DLG_MTG_CONTMENU_EMAIL1") + i, &pM );
        if( pItem  &&  pM )
        {
            if( i < iNumStrs )
            {
                pItem->Enable( true );
                pItem->SetText( m_arrstrEmail.Item( i ) );
            }
            else
            {
                pM->Delete( pItem );
            }
        }
        else
        {
            if( i < iNumStrs )
                pItem = m_pcontextMenuEmail->Append( XRCID("IDC_DLG_MTG_CONTMENU_EMAIL1") + i, m_arrstrEmail.Item( i ) );
        }
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuIMItemUpdateUI( wxUpdateUIEvent &event )
{
#ifndef LINUX
    int     iRet    = event.GetId( );
    if( iRet  !=  XRCID("IDC_DLG_MTG_CONTMENU_IM1") )
        return;
#endif

    //  Go fetch the single(!!) selected participant's IMs...
    participant_t   p       = GetFirstSelectedParticipant( );
    if( p == NULL) 
        return;

    FillIMList( p );

    int         i;
    int         iNumStrs        = (int) m_arrstrIM.GetCount( );
    wxMenu      *pM             = NULL;
    wxMenuItem  *pItem;
    for( i=0; i<10; i++ )
    {
        pItem = m_pcontextMenuIM->FindItem( XRCID("IDC_DLG_MTG_CONTMENU_IM1") + i, &pM );
        if( pItem  &&  pM )
        {
            if( i < iNumStrs )
            {
                pItem->Enable( true );
                pItem->SetText( m_arrstrIM.Item( i ) );
            }
            else
            {
                pM->Delete( pItem );
            }
        }
        else
        {
            if( i < iNumStrs )
                pItem = m_pcontextMenuIM->Append( XRCID("IDC_DLG_MTG_CONTMENU_IM1") + i, m_arrstrIM.Item( i ) );
        }
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuHangUpMe(wxCommandEvent& event)
{
    int                 iRet;
    if( m_participant_tMe )
        iRet = controller_participant_disconnect( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), m_participant_tMe->part_id );

    if( m_pMMF && m_pMMF->PhoneController() )
    {
        m_pMMF->PhoneController()->HangupCall();
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuHandDown(wxCommandEvent& event)
{
    int                 iRet;
    int                 iNumSel;
    participant_tArray  pArray;
    participant_t       p;

    iNumSel = GetSelectedParticipants( pArray );
    for( int i=0; i<iNumSel; i++ )
    {
        p = pArray.Item( i );
        iRet = controller_participant_hand_up( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), p->part_id, false );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuCallParticipant(wxCommandEvent& event)
{
}


//*****************************************************************************
void ParticipantPanel::OnMenuMuteParticipant(wxCommandEvent& event)
{
    int                 iRet;
    int                 iNumSel;
    participant_tArray  pArray;
    participant_t       p;

    iNumSel = GetSelectedParticipants( pArray );
    for( int i=0; i<iNumSel; i++ )
    {
        p = pArray.Item( i );
        iRet = controller_participant_volume( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), p->part_id, -1, -1, 1 );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuUnmuteParticipant(wxCommandEvent& event)
{
    int                 iRet;
    int                 iNumSel;
    participant_tArray  pArray;
    participant_t       p;

    iNumSel = GetSelectedParticipants( pArray );
    for( int i=0; i<iNumSel; i++ )
    {
        p = pArray.Item( i );
        iRet = controller_participant_volume( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), p->part_id, -1, -1, 0 );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuVolumeParticipant(wxCommandEvent& event)
{
    int     iRet;
    int     iDelta;

    iRet = event.GetId( );
    iDelta = XRCID("IDC_DLG_MTG_POPMENU_VOLLEV1PART") - iRet;
    if( iDelta < 0  ||  iDelta > 7 )
    {
        wxLogDebug( wxT("Entering OnMenuVolumeParticipant( ) - ID is %d;  iDelta is %d,  iNewVol is BAD - RETURNING/ABORTING" ), iRet, iDelta );
        return;
    }

    int                 iNumSel;
    participant_tArray  pArray;
    participant_t       p;

    iNumSel = GetSelectedParticipants( pArray );
    for( int i=0; i<iNumSel; i++ )
    {
        p = pArray.Item( i );
        iRet = controller_participant_volume( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), p->part_id, iDelta, -1, -1 );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuHangUpParticipant(wxCommandEvent& event)
{
    int                 iRet;
    int                 iNumSel;
    participant_tArray  pArray;
    participant_t       p;

    iNumSel = GetSelectedParticipants( pArray );
    for( int i=0; i<iNumSel; i++ )
    {
        p = pArray.Item( i );
        iRet = controller_participant_disconnect( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), p->part_id );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuContVolume(wxCommandEvent& event)
{
    int     iRet;
    int     iDelta;

    iRet = event.GetId( );
    iDelta = XRCID("IDC_DLG_MTG_CONTMENU_VOLLEV1PART") - iRet;
    if( iDelta < 0  ||  iDelta > 7 )
    {
        wxLogDebug( wxT("Entering OnMenuVolumeParticipant( ) - ID is %d;  iDelta is %d,  iNewVol is BAD - RETURNING/ABORTING" ), iRet, iDelta );
        return;
    }

    int                 iNumSel;
    participant_tArray  pArray;
    participant_t       p;

    iNumSel = GetSelectedParticipants( pArray );
    for( int i=0; i<iNumSel; i++ )
    {
        p = pArray.Item( i );
        iRet = controller_participant_volume( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), p->part_id, iDelta, -1, -1 );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenu1To1Call(wxCommandEvent& event)
{
    int     iRet;
    int     iDelta;

    iRet = event.GetId( );
    iDelta = iRet - XRCID("IDC_DLG_MTG_CONTMENU_1TO1CALL1");
    if( iDelta < 0  ||  iDelta > 9 )
    {
        return;
    }

    participant_t       pToCall;
    pToCall = GetFirstSelectedParticipant( );
    if( pToCall == NULL )
        return;

    int                 iNumParts;
    participant_t       pArr[ 2 ];
    iNumParts = 1;
    pArr[ 0 ] = pToCall;
    pArr[ 1 ] = NULL;

    //  Reminder:  This call always adds the current user - in this case, that is the caller -
    //  to its list of invitees so we only need to explicitly add the one selected participant.
    meeting_details_t       details;
    details = controller_get_meeting_details( CONTROLLER.controller, m_meeting_t, iNumParts, pArr );

    //  Loop over the TWO invitees and make sure we have phone numbers for both of them
    invitee_iterator_t iter;
    invitee_t invt;

    iter = meeting_details_iterate( details );
    while ((invt = meeting_details_next( details, iter)) != NULL)
    {
        //  Ensure we have a valid phone number for the caller
        if( strcmp( invt->user_id, m_participant_tMe->user_id )  ==  0 )
        {
            invt->selphone = m_participant_tMe->selphone;
            invt->phone = meeting_details_string( details, m_participant_tMe->phone );
            invt->call_options = CallSupervised | CallMaster;
            invt->notify_type = NotifyPhone;

            // We're already on the phone
            if ( (m_participant_tMe->status & StatusVoice) != 0)
                continue;
                
            //  Look for the caller's follow me number.  If it's not blank, use it; otherwise look further...
            wxString        strFollowmeNumber;
            bool isPCPhone;
            FindFollowmeNumber( m_participant_tMe, strFollowmeNumber, isPCPhone );
            if ( isPCPhone )
            {
                invt->selphone = SelPhonePC;
            }
            else if( strFollowmeNumber.Len( )  ==  0 )
            {
                //  If the caller has a valid "last selected" number, use it; otherwise prompt for a number...
                if( strlen( m_participant_tMe->phone )  >  0 )
                {
                    invt->phone = meeting_details_string( details, m_participant_tMe->phone );
                    invt->selphone = SelPhoneThis;
                }
                else
                {
                    wxString    strMessage( _("My Phone Number:\nThe entered number will be used for this call only."), wxConvUTF8 );
                    wxString    strTitle( _("Enter My Phone Number"), wxConvUTF8 );
                    wxTextEntryDialog   dlg( this, strMessage, strTitle, _T(""), wxOK | wxCANCEL );
                    if( dlg.ShowModal( )  ==  wxID_OK )
                    {
                        invt->phone = meeting_details_string( details, dlg.GetValue( ).mb_str(wxConvUTF8) );
                        invt->selphone = SelPhoneThis;
                    }
                    else
                        return;
                }
            }
            else
            {
                invt->phone = meeting_details_string( details, strFollowmeNumber.mb_str(wxConvUTF8) );
                invt->selphone = SelPhoneThis;
            }
            continue;
        }

        //  Ensure we have a valid phone number for the callee
        if( strcmp( invt->user_id, pToCall->user_id )  ==  0 )
        {
            invt->selphone = pToCall->selphone;
            invt->call_options = CallSupervised;
            invt->notify_type = NotifyPhone;

            int selected = IsPCPhone( iDelta ) ? SelPhonePC : SelPhoneThis;

            //  Do we need to prompt for a phone number?
            if( selected == SelPhoneThis && (m_arrstrCallMePhone.Item( iDelta ).IsEmpty( )  ||  (m_arrstrCallMePhone.Item( iDelta ).Len( ) == 0)) )
            {
                //  Get phone number from user
                wxString    strMessage( _("Phone Number:\nThe entered number will be used for this call only."), wxConvUTF8 );
                wxString    strTitle( _("Enter Phone Number"), wxConvUTF8 );
                wxTextEntryDialog   dlg( this, strMessage, strTitle, _T(""), wxOK | wxCANCEL );
                if( dlg.ShowModal( )  ==  wxID_OK )
                {
                    invt->phone = meeting_details_string( details, dlg.GetValue( ).mb_str(wxConvUTF8) );
                    invt->selphone = SelPhoneThis;
                }
                else
                    return;
            }
            else
            {
                invt->phone = meeting_details_string( details, m_arrstrCallMePhone.Item( iDelta ).mb_str(wxConvUTF8) );
                invt->selphone = selected;
            }
            continue;
        }

        //  We didn't invite this person - remove them from the list
        meeting_details_remove( details, invt );
    }

    //  Finally, we are ready to place the phone call...
    iRet = controller_meeting_modify( CONTROLLER.controller, details, true );
}


//*****************************************************************************
void ParticipantPanel::OnMenuConfCall(wxCommandEvent& event)
{
    int     iRet;
    int     iDelta;

    iRet = event.GetId( );
    iDelta = iRet - XRCID("IDC_DLG_MTG_CONTMENU_CONFCALL1");
    if( iDelta < 0  ||  iDelta > 9 )
    {
        iDelta = iRet - XRCID("IDC_DLG_MTG_POPMENU_CALL1");
        if( iDelta < 0  ||  iDelta > 9 )
            return;
    }
    wxLogDebug(wxT("Conferencing call option %d"), iDelta);

    participant_t       p;
    p = GetFirstSelectedParticipant( );
    if( p == NULL) 
        return;
    
    int selected = IsPCPhone( iDelta ) ? SelPhonePC : SelPhoneThis;

    //  Do we need to prompt for a phone number?
    if( selected == SelPhoneThis && (m_arrstrCallMePhone.Item( iDelta ).IsEmpty( )  ||  (m_arrstrCallMePhone.Item( iDelta ).Len( ) == 0)) )
    {
        //  Get phone number from user
        wxString    strMessage( _("Phone Number:\nThe entered number will be used for this call only."), wxConvUTF8 );
        wxString    strTitle( _("Enter Phone Number"), wxConvUTF8 );
        wxTextEntryDialog   dlg( this, strMessage, strTitle, _T(""), wxOK | wxCANCEL );
        if( dlg.ShowModal( )  ==  wxID_OK )
        {
            wxLogDebug( wxT("  the phone number the user entered: *%s*"), dlg.GetValue( ).wc_str( ) );
        }
        else
            return;

        //  Dial phone number entered by the user
        iRet = controller_participant_update( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), p->sub_id,
                                              p->part_id, p->user_id, p->screenname,
                                              p->roll, p->name,
                                              SelPhoneThis, dlg.GetValue( ).mb_str(wxConvUTF8),
                                              p->selemail, p->email,
                                              p->screenname, NotifyPhone, CallNormal );
    }
    else
    {
        iRet = controller_participant_update( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), m_participant_tMe->sub_id,
                                              p->part_id, p->user_id, p->screenname,
                                              p->roll, p->name,
                                              selected, m_arrstrCallMePhone.Item( iDelta ).mb_str(wxConvUTF8),
                                              p->selemail, p->email,
                                              p->screenname, NotifyPhone, CallNormal );
    }

}


//*****************************************************************************
void ParticipantPanel::OnMenuEmail(wxCommandEvent& event)
{
    int     iRet;
    int     iDelta;

    iRet = event.GetId( );
    iDelta = iRet - XRCID("IDC_DLG_MTG_CONTMENU_EMAIL1");
    if( iDelta < 0  ||  iDelta > 9 )
    {
        wxLogDebug( wxT("    in OnMenuEmail( ) - ID is %d;  iDelta is %d  - which is BAD - RETURNING/ABORTING" ), iRet, iDelta );
        return;
    }

    participant_t       p;
    p = GetFirstSelectedParticipant( );
    if( p == NULL) 
        return;

    if( m_arrintEmail.Item( iDelta )  ==  MeetingSetupDialog::Email_EnterEmail )
    {
        wxString    strMessage( _("Enter an email for the participant:"), wxConvUTF8 );
        wxString    strTitle( _("Email Invitation"), wxConvUTF8 );
        wxTextEntryDialog   dlg( this, strMessage, strTitle, wxT(""), wxOK | wxCANCEL );
        if( dlg.ShowModal( )  ==  wxID_OK )
        {
            wxLogDebug( wxT("user entered *%s*"), dlg.GetValue( ).c_str( ) );
            iRet = controller_participant_update( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), p->sub_id,
                p->part_id, p->user_id, p->screenname,
                p->roll, p->name,
                p->selphone, p->phone,
                SelEmailThis, dlg.GetValue( ).mb_str(wxConvUTF8),
                p->screenname, NotifyEmail, CallNormal );
        }
    }
    else
    {
        iRet = controller_participant_update( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), p->sub_id,
                                              p->part_id, p->user_id, p->screenname,
                                              p->roll, p->name,
                                              p->selphone, p->phone,
                                              SelEmailThis, m_arrstrEmailAddr.Item( iDelta ).mb_str( ),
                                              p->screenname, NotifyEmail, CallNormal );
    }
}


//*****************************************************************************
void ParticipantPanel::OnMenuIM(wxCommandEvent& event)
{
    int     iRet;
    int     iDelta;

    iRet = event.GetId( );
    iDelta = iRet - XRCID("IDC_DLG_MTG_CONTMENU_IM1");
    if( iDelta < 0  ||  iDelta > 9 )
    {
        wxLogDebug( wxT("    in OnMenuIM( ) - ID is %d;  iDelta is %d  - which is BAD - RETURNING/ABORTING" ), iRet, iDelta );
        return;
    }

    participant_t       p;
    p = GetFirstSelectedParticipant( );
    if( p == NULL) 
        return;

    iRet = controller_participant_update( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), p->sub_id,
                                          p->part_id, p->user_id, p->screenname,
                                          p->roll, p->name,
                                          p->selphone, p->phone, p->selemail, p->email,
                                          p->screenname, m_arrintIM.Item( iDelta ), CallNormal );
}

//*****************************************************************************
void ParticipantPanel::OnMeetingError(MeetingError& event)
{
    event.Skip();
    
    if( event.GetError() != 0  && !m_fMeetingHasStarted )
    {
        //  Make this call to disable menu items when the meeting has not started
        SetParticipantPrivileges( NULL );
    }
}

//*****************************************************************************
void ParticipantPanel::OnMeetingEvent(MeetingEvent& event)
{
    meeting_t   mtg;
    int         iEv;
    iEv = event.GetEvent( );
    wxString    strT( event_names[ iEv ], wxConvUTF8 );

    wxString    strm_MID( m_strMeetingID );
    wxString    strMID( event.GetMeetingId( ) );
    wxString    strSubID( event.GetSubId( ) );
    wxString    strPartID( event.GetParticipantId( ) );

    wxLogDebug( wxT("    received MeetingEvent:  %s - MtgID: %s  - SubID: %s  - PartID: %s"), strT.wc_str( ), strMID.wc_str( ), strSubID.wc_str( ), strPartID.wc_str( ) );

    event.Skip( );

    if( m_strMeetingID.Cmp( strMID )  !=  0 )
    {
        //  Wrong meeting!!
        return;
    }

    switch( iEv )
    {
        case AppShareStarted:
        case AppShareEnded:
            break;

        case ParticipantJoined:
            wxLogDebug( wxT("just received PARTICIPANTJOINED event in ParticipantPanel::OnMeetingEvent( )") );
             // Pass on to doc share
            if( m_pDoc  &&  m_pDoc->m_Meeting.IsPresenting( )  &&  m_pShareControl->IsDocumentShared( ) )
            {
                //  create event and pass it to the docshare Frame
                wxCommandEvent  evt( wxEVT_COMMAND_TOOL_CLICKED, XRCID("IDC_DOCSHAREEVENT_PARTJOINED") );
                if( m_pDocShareView )
                    m_pDocShareView->AddPendingEvent( evt );
            }
            //  intentional drop through...

        case ParticipantAdded:
        case ParticipantLeft:
        case ParticipantMoved:
            RemoveParticipantFromTree( strPartID );
            AddParticipantToTree( strPartID, true );
            break;

        case ParticipantRemoved:
            //  Remove their line from the tree...
            RemoveParticipantFromTree( strPartID );
            break;

        case ParticipantState:
            UpdateParticipantState( strSubID, strPartID );
            break;

        case PresenterGranted:
//            UpdateParticipantState( strSubID, strPartID );
            if (m_pShareControl->IsViewing())
            {
                if (!m_pShareControl->IsDocumentShared())
                    m_pShareControl->ShareEnd();
                this->DoShareStop();
            }
            break;

        case PresenterRevoked:
//            UpdateParticipantState( strSubID, strPartID );
            break;

        case ControlGranted:
            m_pShareControl->EnableShareControl( true );
            if( m_pDocShareView )
                m_pDocShareView->RemoteControl( true );
            break;

        case ControlRevoked:
            m_pShareControl->EnableShareControl( false );
            if( m_pDocShareView )
                m_pDocShareView->RemoteControl( false );
            break;

        case SubmeetingAdded:
            {
                int     iNumSel     = m_arrpart_tMoveToNew.GetCount( );
                //  If we created this submeeting then we have been waiting to move participants into it.  Do so now...
                if( iNumSel > 0  &&  m_strNewSubMtgID.Cmp( strSubID ) == 0 )
                {
                    mtg = controller_find_meeting( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8) );
                    if (mtg != NULL)
                    {
                        meeting_lock( mtg );
                        for( int i=0; i<iNumSel; i++ )
                        {
                            controller_participant_move( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8),
                                m_arrpart_tMoveToNew.Item( i )->part_id, strSubID.mb_str(wxConvUTF8) );
                        }
                        meeting_unlock( mtg );
                    }

                    //  Empty out the array of participants to move to this new submeeting...
                    m_arrpart_tMoveToNew.Empty( );
                    m_strNewSubMtgID.Clear( );
                }
            }
            break;

        case SubmeetingRemoved:
            {
                wxString2TreeItemIDMap::iterator iter = m_mapSubID2TIID.find( strSubID );
                if( iter  !=  m_mapSubID2TIID.end( ) )
                {
                    wxTreeItemId    id = (*iter).second;
                    if( m_ParticipantTree->HasChildren( id ) )
                        wxLogDebug( wxT("Submeeting to be removed (%s) has CHILDREN!!  This should not happen!!"), strSubID.wc_str( ) );
                    else
                    {
                        m_ParticipantTree->Delete( id );
                        m_mapSubID2TIID.erase( iter );
                    }
                }
            }
            break;

        case MeetingStarted:
            if (!m_fMeetingHasStarted)
            {                
                m_fMeetingHasStarted = true;
                if( m_participant_tMe  !=  NULL )
                    m_fUpdatePrivileges = true;

                mtg = controller_find_meeting( CONTROLLER.controller, event.GetMeetingId().mb_str(wxConvUTF8));
                if( mtg !=  NULL )
                {
                    bool fRecord = (mtg->options & RecordingMeeting) ? true : false;
                    bool fLecture = (mtg->options & LectureMode) ? true : false;
                    bool fHold = (mtg->options & HoldMode) ? true : false;

                    m_pShareControl->EnableHold( fHold );
                    m_pShareControl->EnableRecord( fRecord );
                    m_pShareControl->SetCommand( XRCID("ID_SHARECTRL_LECTUREMODE"), fLecture );
                    m_pShareControl->SetCommand( XRCID("ID_SHARECTRL_HOLDMODE"), fHold );

                    m_TBBRecord->SetDown( fRecord );
                    m_TBBRecord->SetToolTip( fRecord ? _("Click to stop recording") : _("Click to start recording") );
                    m_TBBLecture->SetDown( fLecture );
                    m_TBBLecture->SetToolTip( fLecture ? _("Click to exit lecture mode") : _("Click to enter lecture mode") );
                    m_TBBHold->SetDown( fHold );
                    m_TBBHold->SetToolTip( fHold ? _("Click to release all calls from hold") : _("Click to place all calls on hold") );
                    
                }
                if( m_participant_tMe )
                {
                    wxString strNumber;
                    bool isPCPhone = false;

                    // Set non-moderators to muted by default
                    if ( m_participant_tMe->roll == RollNormal )
                    {
                        if (m_pMMF->PhoneController())
                            m_pMMF->PhoneController()->SetMuted(true);
                    }
                    else if ( m_participant_tMe->roll == RollHost || m_participant_tMe->roll == RollModerator )
                    {
                        this->m_pShareControl->EnableModerator(true);
                    }

                    // Our data on follow me phone could be out of date, but it's unlikely anyone else would update it
                    if ( m_participant_tMe->selphone == SelPhoneDefault)
                    {
                        FindFollowmeNumber( m_participant_tMe, strNumber, isPCPhone);
                    }

                    // ToDo: ask user if this is ok?
                    if( m_participant_tMe->selphone == SelPhonePC || isPCPhone )
                    {
                        if (m_pMMF->PhoneController())
                            m_pMMF->PhoneController()->ConnectToMeeting();
                    }
                }
                CONTROLLER.ExecutePendingCommands(true);
            }
            break;

        case MeetingWaiting:
            break;
        
        case MeetingJoined:
            mtg = controller_find_meeting( CONTROLLER.controller, event.GetMeetingId().mb_str(wxConvUTF8) );
            if( mtg  !=  NULL )
            {
                GetParticipantID( mtg );
                BuildTree( mtg );
                SetupCallMeMenu( );
            }
            CONTROLLER.ExecutePendingCommands(true);
            break;

        case MeetingEnded:
            wxLogDebug( wxT("! ! ! received MeetingEnded message ! ! !") );
            m_pMMF->EndMeeting( );
            break;

        case MeetingLeft:
            //  Participant receives this when they are removed from the meeting by a moderator
            wxLogDebug( wxT("! ! ! received MeetingLeft message ! ! !") );
            m_pMMF->EndMeeting( );
            break;

        case MeetingModified:
            // Meeting options changed.
            wxLogDebug( wxT("in ParticipantPanel::OnMeetingEvent( )  --  ! ! ! received MeetingModified message ! ! !") );
            mtg = controller_find_meeting( CONTROLLER.controller, event.GetMeetingId().mb_str(wxConvUTF8));
            if( mtg  !=  NULL )
            {
                wxLogDebug( wxT("    fetched latest meeting_t struct - options = 0x%8.8x  =  %d  --  RecordingMeeting flag is %d"), mtg->options, mtg->options, (mtg->options & RecordingMeeting) );

                bool    fRecord = (mtg->options & RecordingMeeting) ? true : false;
                bool    fLecture = (mtg->options & LectureMode) ? true : false;
                bool    fHold = (mtg->options & HoldMode) ? true : false;

                m_pShareControl->EnableHold( fHold );
                m_pShareControl->EnableRecord( fRecord );
                m_pShareControl->SetCommand( XRCID("ID_SHARECTRL_LECTUREMODE"), fLecture );
                m_pShareControl->SetCommand( XRCID("ID_SHARECTRL_HOLDMODE"), fHold );

                m_TBBRecord->SetDown( fRecord );
                m_TBBRecord->SetToolTip( fRecord ? _("Click to stop recording") : _("Click to start recording") );
                m_TBBLecture->SetDown( fLecture );
                m_TBBLecture->SetToolTip( fLecture ? _("Click to exit lecture mode") : _("Click to enter lecture mode") );
                m_TBBHold->SetDown( fHold );
                m_TBBHold->SetToolTip( fHold ? _("Click to release all calls from hold") : _("Click to place all calls on hold") );

                wxMenuBar   *pBar   = m_pMainMenuBar;
                if( pBar )
                {
                    wxMenuItem  *pMI    = pBar->FindItem( XRCID("IDC_DLG_MTG_MENU_RECORDMEETING") );
                    if( pMI )
                    {
                        pMI->Check( fRecord );
                    }

                    pMI    = pBar->FindItem( XRCID("IDC_DLG_MTG_MENU_LECTUREMODE") );
                    if( pMI )
                    {
                        pMI->Check( fLecture );
                    }

                    pMI    = pBar->FindItem( XRCID("IDC_DLG_MTG_MENU_HOLDMODE") );
                    if( pMI )
                    {
                        pMI->Check( fHold );
                    }
                }
            }
            else
            {
                wxLogDebug( wxT("    fetched latest meeting_t struct FAILED  --  prior value of options were 0x%8.8x = %d  --  RecordingMeeting flag is %d"), m_meeting_t->options, m_meeting_t->options, (m_meeting_t->options & RecordingMeeting) );
            }

            // intentional fall through...

        default:
            mtg = controller_find_meeting( CONTROLLER.controller, event.GetMeetingId().mb_str(wxConvUTF8));
            if (mtg != NULL)
            {
                BuildTree( mtg );
            }
            break;
    }
}


//*****************************************************************************
void ParticipantPanel::OnSubmeetingAdded(wxCommandEvent& event)
{
    wxString    *pstrSubID      = (wxString *) event.GetClientData( );
    wxString    strOpID;
    strOpID = event.GetString( );

    int         iNumSel     = m_arrpart_tMoveToNew.GetCount( );
    if( iNumSel > 0  &&  m_strNewSubMtgID.Cmp( strOpID ) == 0 )
    {
        //  When we tell the controller to create a new SubMtg we pass it a unique ID we create.
        //  This event is fired when the controller completes the creation of the SubMtg.
        //  It contains both the unique ID we sent (strOpID) and the actual name of the
        //  new SubMtg (pstrSubID).  We actually move the participants to the new sub-meeting
        //  when we receive the meeting event SubmeetingAdded.  Moving them now causes other
        //  meeting participants to not update correctly.
        //  All we need to do here is save off the actual name of the new submeeting so we
        //  know when to move the participants.
        m_strNewSubMtgID = *pstrSubID;
        delete pstrSubID;
    }
    else
    {
        //  This isn't our submeeting - let someone else handle it...
        event.Skip( );
    }
}


//*****************************************************************************
void ParticipantPanel::ShowStatus( const wxString & msg )
{
    wxLogDebug( msg );
//    wxStatusBar *pStatus = wxDynamicCast( FindWindow( wxT("ID_PARTICIPANT_STATUSBAR") ), wxStatusBar );
//    if( pStatus )
//    {
//        pStatus->SetStatusText( msg );
//    }
}


bool gfMeetingOffline = false;
bool gfContactOffline = true;

//*****************************************************************************
void ParticipantPanel::SetParticipantStatus( participant_t aParticipant )
{
    // Set Handup Status
    int intHandupStatus = kStatusHandDown;

    if (aParticipant->status & StatusHandUp)
    {
        intHandupStatus = kStatusHandUp;
    }

    m_TreeImages->SelectSubImage(m_StatusHandup, intHandupStatus);

    // Set Phone Status
    int intPhoneStatus = kStatusPhoneOffline;

    if (aParticipant->status & StatusTalking)
    {
        intPhoneStatus = kStatusPhoneTalking;
    }
    else if (aParticipant->status & StatusDialing)
    {
        intPhoneStatus = kStatusPhoneDialing;
    }
    else if (aParticipant->status & StatusRinging)
    {
        intPhoneStatus = kStatusPhoneRinging;
    }
    else if (aParticipant->status & StatusBusy)
    {
        intPhoneStatus = kStatusPhoneBusy;
    }
    else if (aParticipant->status & StatusAnswered)
    {
        intPhoneStatus = kStatusPhoneOnline;
    }
    else if (aParticipant->status & StatusBroadcast)
    {
        intPhoneStatus = kStatusPhoneBroadcast;
    }
    else if (aParticipant->status & StatusVoice)
    {
        intPhoneStatus = kStatusPhoneOnline;
    }

    if (intPhoneStatus == kStatusPhoneOnline)
    {
        if (aParticipant->mute != 0  &&  aParticipant->mute != -1)
        {
            intPhoneStatus = kStatusPhoneMute;
        }
    }

    m_TreeImages->SelectSubImage(m_StatusPhone, intPhoneStatus);

    // Set Contact Presence status
    int intContactStatus = kStatusContactOffline;

    contact_t contact = CONTROLLER.LookupCABContact( aParticipant->user_id );
    if (!contact)
        contact = CONTROLLER.LookupPABContact( aParticipant->user_id );

    if (contact && contact->screenname)
    {
        PresenceInfo * info = CONTROLLER.Presence().GetPresence(_T("zon"), wxString(contact->screenname, wxConvUTF8).wc_str());
        if( info )
        {
            switch (info->GetPresence())
            {
                case PresenceInfo::PresenceAway:
                    intContactStatus = kStatusContactAway;
                    break;
                case PresenceInfo::PresenceAvailable:
                    intContactStatus = kStatusContactOnline;
                    break;
                case PresenceInfo::PresenceUnavailable:
                    intContactStatus = kStatusContactOffline;
            }
        }
    }
    if( CONTROLLER.IMClientMode() )
        m_TreeImages->SelectSubImage(m_StatusContact, intContactStatus);

    // Set Meeting status
    int intMeetingStatus = kStatusMeetingUnknown;
    intMeetingStatus = (aParticipant->status & StatusData) ? kStatusMeetingOnline  : kStatusMeetingOffline;

    m_TreeImages->SelectSubImage(m_StatusMeeting, intMeetingStatus);
}


//*****************************************************************************
void ParticipantPanel::ConnectionLost( )
{
    wxLogDebug( wxT("entering ParticipantPanel::ConnectionLost( )") );
}


//*****************************************************************************
void ParticipantPanel::ConnectionReconnected( )
{
    wxLogDebug( wxT("entering ParticipantPanel::ConnectionReconnected( ) - trying to find MID  ***%s***"), m_strMeetingID.wc_str( ) );

    m_meeting_t = NULL;             //  initialized in BuildTree( )
    m_arrpart_tMoveToNew.Empty( );
    m_participant_tMe = NULL;       //  initialized in BuildTree( )
    m_submeeting_tMain = NULL;      //  initialized in BuildTree( ), et. al.
    m_idMainMeeting.Unset( );       //  initialized in BuildTree( )
    m_idNotInMeeting.Unset( );      //  initialized in BuildTree( )
    m_mapSubID2TIID.clear( );       //  initialized in BuildTree( )
    m_mapPartID2TIID.clear( );      //  initialized in BuildTree( )

    int     iRet;
    iRet = controller_meeting_join( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8), m_strParticipantID.mb_str(wxConvUTF8), m_strPassword.mb_str(wxConvUTF8) );
    if( iRet  ==  0 )
    {
        wxLogDebug( wxT("  join meeting succeeded!") );

        meeting_t       meeting;
        meeting = controller_find_meeting( CONTROLLER.controller, m_strMeetingID.mb_str(wxConvUTF8) );
        if( meeting )
        {
            wxLogDebug( wxT("    found our meeting again") );
            BuildTree( meeting );
        }
        else
        {
            wxLogDebug( wxT("    WE DID NOT FIND OUR MEETING") );

        }
    }
    else
        wxLogDebug( wxT("  join meeting FAILED!") );
}


//*****************************************************************************
void ParticipantPanel::OnPresenceEvent(PresenceEvent& event)
{
    SetPresence( m_ParticipantTree->GetRootItem( ) );
    event.Skip( );
}


//*****************************************************************************
void ParticipantPanel::SetPresence(wxTreeItemId tRootID)
{
    //  Participant tree depth is fixed at 2 levels - Submeetings contain Participants
    if( tRootID.IsOk( ) )
    {
        wxTreeItemIdValue   cookieSub;
        wxTreeItemId        tSubID;
        tSubID = m_ParticipantTree->GetFirstChild( tRootID, cookieSub );
        while( tSubID.IsOk( ) )
        {
            SubmeetingTreeItemData  *pSTID;
            pSTID = (SubmeetingTreeItemData *) m_ParticipantTree->GetItemData( tSubID );

            wxTreeItemIdValue   cookiePart;
            wxTreeItemId        tPartID;
            tPartID = m_ParticipantTree->GetFirstChild( tSubID, cookiePart );
            while( tPartID.IsOk( ) )
            {
                ParticipantTreeItemData *pPTID;
                pPTID = (ParticipantTreeItemData *) m_ParticipantTree->GetItemData( tPartID );

                SetParticipantStatus( pPTID->m_p );

                int statusImage = m_TreeImages->GetSelectedListIndex( );
                m_ParticipantTree->SetItemImage( tPartID, statusImage );

                tPartID = m_ParticipantTree->GetNextChild( tSubID, cookiePart );
            }
            tSubID = m_ParticipantTree->GetNextChild( tRootID, cookieSub );
        }
    }
}


//*****************************************************************************
void ParticipantPanel::OnParticipantListColClick( wxListEvent& event )
{
    long new_sort_col = event.GetColumn();
//    wxLogDebug(wxT("ParticipantPanel::OnParticipantListColClick( col %d )"), new_sort_col );

    long current_sort_col = m_ParticipantTree->GetSortColumn( );

    if( new_sort_col == current_sort_col )
    {
        m_ParticipantTree->ReverseSortOrder( );
    }
    else
    {
        m_ParticipantTree->SetSortColumn( new_sort_col );
    }

    wxTreeItemId    tRootID;
    tRootID = m_ParticipantTree->GetRootItem( );
    if( new_sort_col  ==  0 )
    {
        m_ParticipantTree->SortChildren( tRootID );
    }

    wxTreeItemIdValue   cookie;
    for( wxTreeItemId child = m_ParticipantTree->GetFirstChild( tRootID, cookie );
         child.IsOk();
         child = m_ParticipantTree->GetNextChild( tRootID, cookie ) )
    {
        m_ParticipantTree->SortChildren( child );
    }
}


//*****************************************************************************
void ParticipantPanel::GetParticipantID(meeting_t mtg)
{
    participant_t p = meeting_find_me(mtg);
    if (p != NULL)
    {
        wxString    strPID( p->part_id, wxConvUTF8 );
        strPID.StartsWith( wxT("part:"), &strPID );
        if( strPID.CmpNoCase( m_strParticipantID )  !=  0 )
        {
            m_strParticipantID.Empty( );
            m_strParticipantID.Append( strPID );

            //  Since we had a bad PartID, our sibling panels (Chat, Main, and PCPhone) must also
            m_pMMF->SetParticipantID( m_strParticipantID );
        }

        m_participant_tMe = p;
    }
}


//*****************************************************************************
bool ParticipantPanel::IsPCPhone( int iDelta )
{
    if (m_arrintCallMe.Item( iDelta ) == MeetingSetupDialog::Phone_PC)
        return true;
        
    if ( m_arrintCallMe.Item( iDelta ) == MeetingSetupDialog::Phone_FollowMe &&
         m_arrstrCallMe.Item( iDelta ).Contains(_("Web phone")) )
        return true;
    
    return false;
}


//*****************************************************************************
bool ParticipantPanel::CheckRestart( )
{
    int res = wxID_OK;
    
    if (!m_pShareControl->IsShareIdle())
    {
        wxString msg;
        if (m_pShareControl->IsDesktopShared())
        {
            msg = _("To start a new share, your current desktop share will be stopped.\nWould you like to continue?");
        }
        else if (m_pShareControl->IsDocumentShared())
        {
            msg = _("To start a new share, you current document/presentation share will be stopped.\nWould you like to continue?");
        }
        m_fModalDialogOpen = true;
        wxString    strBrand = wxGetApp().m_strProductName;
        wxMessageDialog     dlg( this, msg, strBrand, wxOK | wxCANCEL | wxICON_QUESTION );
        res = dlg.ShowModal( );
        m_fModalDialogOpen = false;
        
        if (res == wxID_OK) 
        {
            DoShareStop();
            CUtils::MsgWait( 20, 5 );
        }
    }

    return res == wxID_OK;
}


#ifdef MY_AUI_WORKAROUND
//*****************************************************************************
void ParticipantPanel::OnSize( wxSizeEvent& event )
{
    wxSize  sz, szMin;
    sz    = event.GetSize( );
    szMin = this->GetMinSize( );

    event.Skip( );

    if( sz.y < szMin.y  ||  sz.x < szMin.x )
    {
        //  This is an attempt at a work-around of a bug in wxAUIManager where
        //  the sizes of managed windows are computed incorrectly when the user
        //  resizes one and it totally covers another managed window.  The
        //  obscured window is given a height of zero and the obscuring window
        //  is given a height taller than the parent window.
        wxCommandEvent      ev( wxEVT_AUIMGR_FIXUP, wxID_ANY );
        if( m_pMMF )
            m_pMMF->AddResizeEvent( ev );
    }
}
#endif
