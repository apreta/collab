/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"
#include "pcphonepanel.h"

#include "meetingmainframe.h"
#include "pcphonecontroller.h"

//(*InternalHeaders(PCPhonePanel)
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(PCPhonePanel)
//*)

BEGIN_EVENT_TABLE(PCPhonePanel,wxPanel)
	//(*EventTable(PCPhonePanel)
	//*)
END_EVENT_TABLE()

PCPhonePanel::PCPhonePanel(wxWindow* parent,wxWindowID id)
{
	//(*Initialize(PCPhonePanel)
	wxXmlResource::Get()->LoadObject(this,parent,_T("PCPhonePanel"),_T("wxPanel"));
	m_StatusText = (wxStaticText*)FindWindow(XRCID("ID_STATUS_TEXT"));
	m_ConnectDisconnectButton = (wxButton*)FindWindow(XRCID("ID_CONNECTDISCONNECT_BUTTON"));
	m_DialButton = (wxButton*)FindWindow(XRCID("ID_DIAL_BUTTON"));
	m_AudioSettingsButton = (wxButton*)FindWindow(XRCID("ID_AUDIOPROPERTIES_BUTTON"));
	m_VolumeButton = (wxBitmapButton*)FindWindow(XRCID("ID_VOLBUTTON"));
	m_VolGauge = (wxGauge*)FindWindow(XRCID("ID_VOL_GAUGE"));
	m_MicrophoneButton = (wxBitmapButton*)FindWindow(XRCID("ID_MICBUTTON"));
	m_MicGauge = (wxGauge*)FindWindow(XRCID("ID_MIC_GAUGE"));
	
	Connect(XRCID("ID_VOLBUTTON"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&PCPhonePanel::OnVolumeButtonClick);
	Connect(XRCID("ID_MICBUTTON"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&PCPhonePanel::OnMicrophoneButtonClick);
	//*)

	m_VolGauge->SetRange( 40 );
	m_MicGauge->SetRange( 32 );

#ifdef __WXMAC__
    m_AudioSettingsButton->Show(false);
#endif
    
    bool fShowDialPad = OPTIONS.GetSystemBool( "ShowDialPad", true );
    if (!fShowDialPad)
        m_DialButton->Show(false);
    
    m_fMuted = false;
    m_fSpeakerOff = false;
}

PCPhonePanel::~PCPhonePanel()
{
	//(*Destroy(PCPhonePanel)
	//*)
}

void PCPhonePanel::StartMeeting( const wxString &strParticipantID, const wxString &strParticipantName, const wxString &strMeetingID, const wxString &strMeetingTitle, const wxString &strPassword, bool fJoining )
{
}

void PCPhonePanel::EndMeeting( )
{
}

void PCPhonePanel::ConnectionLost( )
{
}

void PCPhonePanel::ConnectionReconnected( )
{
}

void PCPhonePanel::UpdateVoipStatus( wxCommandEvent& event )
{
    wxString strStatus;

    PCPhoneStatus status = (PCPhoneStatus)event.GetInt();

    strStatus = event.GetString();
    m_StatusText->SetLabel( strStatus );
    // TODO: display errors more prominently

    if( status == PCPhoneStatus_Connected )
    {
        m_ConnectDisconnectButton->SetLabel( _("Disconnect") );
    }
    else
    {
        m_ConnectDisconnectButton->SetLabel( _("Connect") );
    }
#ifdef __WXMAC__
    Layout();
#endif

    event.Skip();
}

void PCPhonePanel::UpdateVoipLevels( wxCommandEvent& event )
{
    int Vol = event.GetInt() + 40;
    int Mic = event.GetExtraLong() + 32;

    //wxLogDebug( wxT("PCPhoneDialog::UpdateVoipLevels Vol %d, Mic %d"), Vol, Mic );

    if( ! (Vol >=0 && Vol < m_VolGauge->GetRange()) )
        Vol = 0;

    m_VolGauge->SetValue( Vol );

    if( ! (Mic >= 0 && Mic < m_MicGauge->GetRange()) )
        Mic = 0;

    m_MicGauge->SetValue( Mic );

    event.Skip();
}

void PCPhonePanel::SetMuted(bool muted)
{
    if (m_fMuted != muted)
    {
        wxBitmap    bmpT = m_MicrophoneButton->GetBitmapSelected( );
        m_MicrophoneButton->SetBitmapSelected( m_MicrophoneButton->GetBitmapLabel( ) );
        m_MicrophoneButton->SetBitmapLabel( bmpT );
        m_fMuted = !m_fMuted;
    }
}

void PCPhonePanel::OnMicrophoneButtonClick(wxCommandEvent& event)
{
    wxBitmap    bmpT = m_MicrophoneButton->GetBitmapSelected( );
    m_MicrophoneButton->SetBitmapSelected( m_MicrophoneButton->GetBitmapLabel( ) );
    m_MicrophoneButton->SetBitmapLabel( bmpT );
    m_fMuted = !m_fMuted;
    
    wxCommandEvent ev(wxEVT_COMMAND_CHECKBOX_CLICKED, XRCID("ID_MUTE_CHECK"));
    ev.SetInt(m_fMuted ? 1 : 0);
    AddPendingEvent(ev);
}

void PCPhonePanel::OnVolumeButtonClick(wxCommandEvent& event)
{
    wxBitmap    bmpT = m_VolumeButton->GetBitmapSelected( );
    m_VolumeButton->SetBitmapSelected( m_VolumeButton->GetBitmapLabel( ) );
    m_VolumeButton->SetBitmapLabel( bmpT );
    m_fSpeakerOff = !m_fSpeakerOff;

    wxCommandEvent ev(wxEVT_COMMAND_CHECKBOX_CLICKED, XRCID("ID_SPEAKER_MUTE"));
    ev.SetInt(m_fSpeakerOff ? 1 : 0);
    AddPendingEvent(ev);
}
