#include "app.h"
#include "meetingcentercontroller.h"

#include "logupload.h"

#include <wx/stdpaths.h>
#include <wx/datetime.h>
#include <wx/wfstream.h>
#include <wx/uri.h>

#ifndef FAILED
  #define FAILED(x) 	(x<0)
#endif


LogUpload::LogUpload()
{
    inProgress = false;
    strLogDir = wxStandardPaths::Get().GetUserDataDir() + wxT("/");
    strUploadURL = wxString(wxGetApp().Options().GetSystem("UploadURL", ""), wxConvUTF8);
}

LogUpload::~LogUpload()
{    
}

bool LogUpload::IsAvailable()
{
    const char * setting = wxGetApp().Options().GetSystem("UploadURL", "");
    if (setting == NULL || !*setting)
        return false;
    return true;
}

wxString LogUpload::genTempName()
{
    wxDateTime dt = wxDateTime::Now();
    return strLogDir + dt.Format(wxT("report-%y-%m-%d-%H-%M-%S.zip"));
}

void LogUpload::addLogFile(wxZipOutputStream& zip, wxFileName& file)
{
    wxLogDebug(wxT("Adding file %s"), file.GetFullName().wc_str());
    zip.PutNextEntry(file.GetFullName());
    wxFileInputStream is(file.GetFullPath());
    is.Read(zip);
}

// Compress all log files (*.log and *.log.*) into zip report file.
wxString LogUpload::CompressLogs()
{
    wxString zipName = genTempName();
    
    wxLogDebug(wxT("Creating report file %s"), zipName.wc_str());
    
    wxFFileOutputStream out(zipName);
    wxZipOutputStream zip(out);
    
    wxFileSystem fs;
    fs.ChangePathTo(strLogDir, true);
    wxString logFile = fs.FindFirst(wxT("*.log*"), wxFILE);
    while (!logFile.IsEmpty()) 
    {
        wxFileName file(logFile);
        addLogFile(zip, file);
        logFile = fs.FindNext();
        
        if (m_pDialogProgress->IsCancelled())
        {
            hideProgress();
            return wxT("");
        }
    }
    
    zip.Close();
    out.Close();
    
    return zipName;
}

// Make directory on webdav server.
// URL = http://server, path = /repository/...
long LogUpload::makeDir(const wxString& aTargetUrl, const wxString& aTargetPath,
                        wxString& strStatusCode, wxString& strStatusText)
{
    long hr = S_OK;
    int len = 0;
    wxString strLockToken;
    
    hr = m_WebDavUtils.SendRequest(wxT("MKCOL"), aTargetUrl, aTargetPath, strUserName, strPassword, NULL, 0, strStatusCode,
                              strStatusText, false, strLockToken, m_Parent, &len);
    if (m_pDialogProgress->IsCancelled())
    {
        hr = E_ABORT;
    }

    return hr;
}


// Send local file to webdav server.
// URL = http://server, path = /repository/...
long LogUpload::sendFile(const wxString& aTargetUrl, const wxString& aTargetPath,
                         const wxString& aFileName, wxString& strStatusCode, wxString& strStatusText)
{
    long hr = S_OK;
    int len = 0;
    wxString strLockToken;
    wxFile hfile;
    
    if (wxFile::Exists(aFileName))
        hfile.Open(aFileName, wxFile::read);
    if (hfile.IsOpened())
    {
        unsigned long dwFileSize = hfile.Length();
        unsigned char* lpBuf = new unsigned char[ dwFileSize+1 ];
        long iBytesRead;
        iBytesRead = hfile.Read(lpBuf, dwFileSize);
        hfile.Close();
        if (iBytesRead > 0)
        {
            hr = m_WebDavUtils.SendRequest(wxT("PUT"), aTargetUrl, aTargetPath, strUserName, strPassword, lpBuf, iBytesRead, strStatusCode,
                                      strStatusText, false, strLockToken, m_Parent, &len);
            if (m_pDialogProgress->IsCancelled())
            {
                hr = E_ABORT;
            }
        }
        delete [] lpBuf;
    }
    else
    {
        wxLogDebug(wxT("Unable to open log report file %s"), aFileName.wc_str());
        hr = E_FAIL;
    }

    return hr;
}

void LogUpload::UploadArchive(wxString& name)
{
    wxURI url(strUploadURL);
    wxString strDocServer = url.GetScheme() + wxT("://") + url.GetServer();
 
    wxString community;
    if (CONTROLLER.AlreadySignedOn())
    {
        CONTROLLER.GetSignOnInfo(strUserName, strPassword, community);
    }
    else
    {
        strUserName = wxT("anonymous");
        strPassword = wxT("");
    }

    wxString strDir( strUserName );
    CUtils::EncodeStringFully( strDir );
    
    wxString path = url.GetPath() + wxT("/") + strDir + wxT("/");

    wxString statusCode, statusText;

    // TODO: get progress from and allow http requests to be aborted

    long hr = makeDir(strDocServer, path, statusCode, statusText);
    if (!FAILED(hr) && !m_pDialogProgress->IsCancelled())
    {
        wxFileName fileName(name);
        path += fileName.GetFullName();
    
        hr = sendFile(strDocServer, path, name, statusCode, statusText);
    }
    
    if (FAILED(hr) || (statusCode.Left(1) != wxT("2")))
    {
        wxString errmsg = wxString(_("Unable to upload log files.")) + wxT("\r\n");
        long    lCode;
        statusCode.ToLong( &lCode );
        CUtils::ReportDAVError(NULL, lCode, statusText, wxT("%s\n%s\n%s"), errmsg, hr);

        hideProgress();        
    }
    
    wxLogDebug(wxT("Upload result: %s"), statusCode.wc_str());
}

void LogUpload::showProgress()
{
    if (m_pDialogProgress != NULL && !m_pDialogProgress->IsCancelled())
        m_pDialogProgress->Commands().Add(wxT("pulse"));
}

void LogUpload::hideProgress()
{
    if (m_pDialogProgress != NULL)
        m_pDialogProgress->Commands().Add(wxT("hide"));
}

void LogUpload::UploadLogs(wxWindow* parent)
{
    if (inProgress)
        return;
    inProgress = true;

    if (strUploadURL.IsEmpty())
    {
        wxLogDebug(wxT("Upload server not set"));
        return;        
    }
 
    UploadThread* thread = new UploadThread(this);
    thread->Create();
 
    wxString title = _("Upload Logs");
    wxString prompt = _("Please wait while logs are uploaded to the server.");

    Progress1Dialog dlg(title, prompt, 100, parent, wxPD_APP_MODAL|wxPD_CAN_ABORT);
    dlg.SetReturnCode(wxID_CANCEL);
    m_pDialogProgress = &dlg;
    
    thread->Run();

    while (dlg.IsShown())
    {
        showProgress();
        
        int iLC = 0;
        ::wxMilliSleep(10);
#ifdef WIN32
        while (wxGetApp().Pending() && iLC < 20)
        {
            iLC++;
            wxGetApp().Dispatch();
        }
#else
        while (wxGetApp().Pending())
        {
            iLC++;
            wxGetApp().Dispatch();
        }
#ifdef LINUX
        if (g_main_context_iteration(NULL, false))
            ;
#endif
#endif
    }
    
    inProgress = false;
}


void* UploadThread::Entry()
{
    wxString logFile = upload->CompressLogs();
    if (upload->m_pDialogProgress->IsCancelled())
        return NULL;
    upload->UploadArchive(logFile);

    upload->m_pDialogProgress->Commands().Add(wxT("hide"));

    ::wxRemoveFile(logFile);
    
    return NULL;
}
