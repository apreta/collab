#include "app.h"
#include "selectmonitordialog.h"

//(*InternalHeaders(SelectMonitorDialog)
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(SelectMonitorDialog)
//*)

BEGIN_EVENT_TABLE(SelectMonitorDialog,wxDialog)
	//(*EventTable(SelectMonitorDialog)
	//*)
END_EVENT_TABLE()

SelectMonitorDialog::SelectMonitorDialog(wxWindow* parent, std::list<MonitorInfo> & monitors)
{
	//(*Initialize(SelectMonitorDialog)
	wxXmlResource::Get()->LoadObject(this,parent,_T("SelectMonitorDialog"),_T("wxDialog"));
	m_Panel = (wxPanel*)FindWindow(XRCID("ID_PANEL1"));
	
	Connect(wxID_ANY,wxEVT_INIT_DIALOG,(wxObjectEventFunction)&SelectMonitorDialog::OnInit);
	//*)
	
	m_Monitors = monitors;
	
	wxArrayString choices;
	int i=1;
	std::list<MonitorInfo>::iterator iter;
	for (iter = monitors.begin(); iter != monitors.end(); iter++, i++) 
	{
	    MonitorInfo& info(*iter);
	    wxString name(info.description.c_str(), wxConvUTF8);
	    wxString label = wxString::Format(wxT("Display %d - %s (%d x %d)"), i, name.c_str(), info.bx-info.tx, info.by-info.ty);
	    choices.Add(label);
	}
	
	m_Displays = new wxRadioBox(m_Panel, XRCID("ID_DISPLAY_RADIOBOX"), _T("Select Display"), wxDefaultPosition, wxDefaultSize, choices, 0, wxVERTICAL);
    m_Panel->GetSizer()->Insert(1, m_Displays, wxSizerFlags().Expand().Border(wxALL, 4));
	
	Fit();
	Layout();
}

SelectMonitorDialog::~SelectMonitorDialog()
{
	//(*Destroy(SelectMonitorDialog)
	//*)
	CloseLabels();
}

bool SelectMonitorDialog::Validate()
{
    int i = m_Displays->GetSelection();
    if (i >= 0)
    {
        std::list<MonitorInfo>::iterator iter;
        for (iter = m_Monitors.begin(); iter != m_Monitors.end(); iter++)
        {
            if (i-- == 0)
            {
                m_Selected = wxString((*iter).deviceName.c_str(), wxConvUTF8);
                wxLogDebug(wxT("Selected display: %s"), m_Selected.c_str());
                return true;
            }
        }
    }
    
    wxLogDebug(wxT("No selected display"));
    m_Selected.Clear();
    return true;
}

void SelectMonitorDialog::CloseLabels()
{
    std::list<MonitorLabelFrame*>::iterator iter;
    for (iter = m_Labels.begin(); iter != m_Labels.end(); iter++)
    {
        MonitorLabelFrame *label = *iter;
        label->Close();
    }
    m_Labels.clear();
}

void SelectMonitorDialog::OnInit(wxInitDialogEvent& event)
{
	int i=1;
	std::list<MonitorInfo>::iterator iter;
	for (iter = m_Monitors.begin(); iter != m_Monitors.end(); iter++, i++) 
	{
	    MonitorInfo& info(*iter);
	    wxString label = wxString::Format(wxT("Display %d"), i);
	    MonitorLabelFrame *frame = new MonitorLabelFrame(NULL, label, info.tx, info.ty);
	    m_Labels.push_back(frame);
	    frame->Show();
	}
}

