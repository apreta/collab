/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 *
 *
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */
#include "app.h"
#include "meetingcentercontroller.h"
#include "userpreferencesdialog.h"
#include <wx/colordlg.h>
#include <wx/tooltip.h>
#include <wx/richtext/richtextstyles.h>
#include <wx/fontdlg.h>
#include "VoipService.h"
#include "contactservice.h"

//(*InternalHeaders(UserPreferencesDialog)
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(UserPreferencesDialog)
//*)

BEGIN_EVENT_TABLE(UserPreferencesDialog,wxDialog)
	//(*EventTable(UserPreferencesDialog)
	//*)
EVT_BUTTON(XRCID("ID_APPFONT_BTN"), UserPreferencesDialog::OnClickAppFont)
EVT_BUTTON(XRCID("ID_FONT_SETFORE_BTN"),UserPreferencesDialog::OnClickFore)
EVT_BUTTON(XRCID("ID_FONT_SETBACK_BTN"),UserPreferencesDialog::OnClickBack)
EVT_BUTTON(XRCID("ID_FONT_SETSMALLER_BTN"),UserPreferencesDialog::OnClickSmaller)
EVT_BUTTON(XRCID("ID_FONT_SETNORM_BTN"),UserPreferencesDialog::OnClickNorm)
EVT_BUTTON(XRCID("ID_FONT_SETLARGER_BTN"),UserPreferencesDialog::OnClickLarger)
EVT_BUTTON(XRCID("ID_FONT_SETBOLD_BTN"),UserPreferencesDialog::OnClickBold)
EVT_BUTTON(XRCID("ID_FONT_SETITALICS_BTN"),UserPreferencesDialog::OnClickItalics)
EVT_BUTTON(XRCID("ID_FONT_SETULINE_BTN"),UserPreferencesDialog::OnClickUline)

EVT_BUTTON( wxID_HELP, UserPreferencesDialog::OnClickHelp)

END_EVENT_TABLE()

UserPreferencesDialog::UserPreferencesDialog(wxWindow* parent,wxWindowID id):
m_enabled_features(ProfileUserSavePassword),
m_refreshContacts(false)
{
	//(*Initialize(UserPreferencesDialog)
	wxXmlResource::Get()->LoadObject(this,parent,_T("UserPreferencesDialog"),_T("wxDialog"));
	m_SavePasswordCheck = (wxCheckBox*)FindWindow(XRCID("ID_SAVEPASSWORD_CHECK"));
	m_AutoSignOnCheck = (wxCheckBox*)FindWindow(XRCID("ID_AUTOSIGNON_CHECK"));
	m_StartOnComputerStartCheck = (wxCheckBox*)FindWindow(XRCID("ID_STARTONCOMPUTERSTART_CHECK"));
	m_fKeepTryingConnectionCheck = (wxCheckBox*)FindWindow(XRCID("ID_KEEPTRYINGCONNECTION_CHECK"));
	m_DownloadPublicMeetingsCheck = (wxCheckBox*)FindWindow(XRCID("ID_DOWNLOADPUBLICMEETINGS_CHECK"));
	m_DownloadCABCheck = (wxCheckBox*)FindWindow(XRCID("ID_DOWNLOADCAB_CHECK"));
	m_CacheABCheck = (wxCheckBox*)FindWindow(XRCID("ID_CACHEAB_CHECK"));
	m_DownloadOutlookCheck = (wxCheckBox*)FindWindow(XRCID("ID_DOWNLOADOUTLOOK_CHECK"));
	m_ConfigureOutlookButton = (wxButton*)FindWindow(XRCID("ID_CONFIGURE_OUTLOOK"));
	m_ShowInstantMeetingCheck = (wxCheckBox*)FindWindow(XRCID("ID_INSTANTMEETING_CHECK"));
	m_EnablePrivateMeetings = (wxCheckBox*)FindWindow(XRCID("ID_ENABLEPRIVATEMEETING_CHECKBOX"));
	m_BuiltInIMCheck = (wxCheckBox*)FindWindow(XRCID("ID_BUILTINIM_CHECK"));
	m_General = (wxPanel*)FindWindow(XRCID("ID_PANEL_GENERAL"));
	m_AvailableColumnsListBox = (wxListBox*)FindWindow(XRCID("ID_LISTBOX1"));
	m_MoveToDisplayedButton = (wxButton*)FindWindow(XRCID("ID_MOVETODISPLAYED_BUTTON"));
	m_MoveToAvailableButton = (wxButton*)FindWindow(XRCID("ID_MOVETOAVIALABLE_BUTTON"));
	m_DisplayedColumnsListBox = (wxListBox*)FindWindow(XRCID("ID_LISTBOX2"));
	m_MoveColumnsUpButton = (wxButton*)FindWindow(XRCID("ID_BUTTON1"));
	m_MoveColumnsDownButton = (wxButton*)FindWindow(XRCID("ID_MOVECOLUMNSDOWN_BUTTON"));
	m_ContactDisplay = (wxPanel*)FindWindow(XRCID("ID_PANEL_CONTACTDISPLAY"));
	m_AppFontBtn = (wxButton*)FindWindow(XRCID("ID_APPFONT_BTN"));
	m_FontSampleEdit = (wxTextCtrl*)FindWindow(XRCID("ID_FONT_SAMPLE_EDIT"));
	m_ChatFontLabel = (wxStaticText*)FindWindow(XRCID("ID_CHATFONTLABEL"));
	m_buttonSetFore = (wxTogBmpBtn*)FindWindow(XRCID("ID_FONT_SETFORE_BTN"));
	m_buttonSetBack = (wxTogBmpBtn*)FindWindow(XRCID("ID_FONT_SETBACK_BTN"));
	m_buttonSetSmaller = (wxTogBmpBtn*)FindWindow(XRCID("ID_FONT_SETSMALLER_BTN"));
	m_buttonSetNorm = (wxTogBmpBtn*)FindWindow(XRCID("ID_FONT_SETNORM_BTN"));
	m_buttonSetLarger = (wxTogBmpBtn*)FindWindow(XRCID("ID_FONT_SETLARGER_BTN"));
	m_buttonSetBold = (wxTogBmpBtn*)FindWindow(XRCID("ID_FONT_SETBOLD_BTN"));
	m_buttonSetItalics = (wxTogBmpBtn*)FindWindow(XRCID("ID_FONT_SETITALICS_BTN"));
	m_buttonSetUline = (wxTogBmpBtn*)FindWindow(XRCID("ID_FONT_SETULINE_BTN"));
	m_ChatSampleEdit = (wxTextCtrlEx*)FindWindow(XRCID("ID_FONT_CHATSAMPLE_EDIT"));
	m_Font = (wxPanel*)FindWindow(XRCID("ID_FONT_PANEL"));
	StaticText1 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT1"));
	m_MuteAllSoundsCheck = (wxCheckBox*)FindWindow(XRCID("ID_MUTEALLSOUNDS_CHECK"));
	m_ChatSoundsLabel = (wxStaticText*)FindWindow(XRCID("ID_CHATSOUNDS_LABEL"));
	m_ChatSoundsCombo = (wxComboBox*)FindWindow(XRCID("ID_CHATSOUNDS_COMBO"));
	StaticText3 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT3"));
	m_PlayChatInvtSoundCheck = (wxCheckBox*)FindWindow(XRCID("ID_PLAYCHATINVTSOUND_CHECK"));
	m_InMeetingSoundsLabel = (wxStaticText*)FindWindow(XRCID("ID_INMEETINGSOUNDS_LABEL"));
	m_InMeetingSoundsCombo = (wxComboBox*)FindWindow(XRCID("ID_INMEETINGSOUNDS_COMBO"));
	StaticText5 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT6"));
	m_DisableInMeetingSoundsCheck = (wxCheckBox*)FindWindow(XRCID("ID_DISABLEINMEETINGSOUNDS_CHECK"));
	m_Sounds = (wxPanel*)FindWindow(XRCID("ID_SOUNDS_PANEL"));
	m_HidePresenterMeetingWindowCheck = (wxCheckBox*)FindWindow(XRCID("ID_HIDEPRESENTERMEETINGWINDOW_CHECK"));
	m_AutoDetectSlowConnectionCheck = (wxCheckBox*)FindWindow(XRCID("ID_AUTODETECTSLOWCONNECTION_CHECK"));
	m_ReduceColorCheck = (wxCheckBox*)FindWindow(XRCID("ID_REDUCECOLOR_CHECK"));
	m_UsePowerPointConverterCheck = (wxCheckBox*)FindWindow(XRCID("ID_USE_POWERPOINT_CHECK"));
	m_UseDirectXCaptureCheck = (wxCheckBox*)FindWindow(XRCID("ID_DIRECTX_CHECK"));
	m_DataShare = (wxPanel*)FindWindow(XRCID("ID_DATASHARE_PANEL"));
	m_NetworkBandwidthLabel = (wxStaticText*)FindWindow(XRCID("ID_NETWORKBANWIDTH_LABEL"));
	m_AutoBandwidthRadio = (wxRadioButton*)FindWindow(XRCID("ID_AUTOBANDWIDTH_RADIO"));
	m_HighBandwidthRadio = (wxRadioButton*)FindWindow(XRCID("ID_HIGHBANDWIDTH_RADIO"));
	m_MediumBandwidthRadio = (wxRadioButton*)FindWindow(XRCID("ID_MEDIUMBANDWIDTH_RADIO"));
	m_LowBandwidthRadio = (wxRadioButton*)FindWindow(XRCID("ID_LOWBANDWIDTH_RADIO"));
	m_EnableEchoCancellationCheck = (wxCheckBox*)FindWindow(XRCID("ID_ENABLEECHOCANCELLATION_CHECK"));
	m_EnableNoiseFilterCheck = (wxCheckBox*)FindWindow(XRCID("ID_ENABLENOISEFILTER_CHECK"));
	m_LowVadRadio = (wxRadioButton*)FindWindow(XRCID("ID_LOW_VAD_RADIO"));
	m_MedVadRadio = (wxRadioButton*)FindWindow(XRCID("ID_MED_VAD_RADIO"));
	m_HighVadRadio = (wxRadioButton*)FindWindow(XRCID("ID_HIGH_VAD_RADIO"));
	m_MaxJitterEdit = (wxTextCtrl*)FindWindow(XRCID("ID_MAX_JITTER_EDIT"));
	m_AudioSubsystemLabel = (wxStaticText*)FindWindow(XRCID("ID_AUDIOSUBSYSTEM_LABEL"));
	m_AudioSubsystemCombo = (wxComboBox*)FindWindow(XRCID("ID_AUDIOSUBSYSTEM_COMBO"));
	m_PCPhone = (wxPanel*)FindWindow(XRCID("ID_PCPHONE_PANEL"));
	m_LanguageList = (wxListBox*)FindWindow(XRCID("ID_LANGUAGE_LISTBOX"));
	StaticText2 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT7"));
	m_Language = (wxPanel*)FindWindow(XRCID("ID_PANEL_LANGUAGE"));
	m_Notebook = (wxNotebook*)FindWindow(XRCID("ID_NOTEBOOK"));
	
	Connect(XRCID("ID_SAVEPASSWORD_CHECK"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&UserPreferencesDialog::OnSavePasswordCheckClick);
	Connect(XRCID("ID_AUTOSIGNON_CHECK"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&UserPreferencesDialog::OnAutoSignOnCheckClick);
	Connect(XRCID("ID_DOWNLOADOUTLOOK_CHECK"),wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&UserPreferencesDialog::OnDownloadOutlookCheckClick);
	Connect(XRCID("ID_CONFIGURE_OUTLOOK"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&UserPreferencesDialog::OnConfigureOutlookButtonClick);
	Connect(XRCID("ID_MOVETODISPLAYED_BUTTON"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&UserPreferencesDialog::OnMoveToDisplayedButtonClick);
	Connect(XRCID("ID_MOVETOAVIALABLE_BUTTON"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&UserPreferencesDialog::OnMoveToAvailableButtonClick);
	Connect(XRCID("ID_BUTTON1"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&UserPreferencesDialog::OnMoveColumnsUpButtonClick);
	Connect(XRCID("ID_MOVECOLUMNSDOWN_BUTTON"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&UserPreferencesDialog::OnMoveColumnsDownButtonClick);
	//*)

    m_Notebook->RemovePage( 3 );
    m_Sounds->Hide( );

#ifndef WIN32
    m_UsePowerPointConverterCheck->Hide( );
    m_UseDirectXCaptureCheck->Hide( );
#else
    DWORD version = GetVersion();
    DWORD major = (DWORD) (LOBYTE(LOWORD(version)));
    DWORD minor = (DWORD) (HIBYTE(LOWORD(version)));
    bool isWin7 = (major > 6) || ((major == 6) && (minor == 1));

    if (!isWin7)
    {
        m_UseDirectXCaptureCheck->Enable(false);
    }
#endif

    m_buttonSetFore->SetBackgroundColour( GetBackgroundColour( ) );
    m_buttonSetBack->SetBackgroundColour( GetBackgroundColour( ) );
    m_buttonSetSmaller->SetBackgroundColour( GetBackgroundColour( ) );
    m_buttonSetNorm->SetBackgroundColour( GetBackgroundColour( ) );
    m_buttonSetLarger->SetBackgroundColour( GetBackgroundColour( ) );
    m_buttonSetBold->SetBackgroundColour( GetBackgroundColour( ) );
    m_buttonSetItalics->SetBackgroundColour( GetBackgroundColour( ) );
    m_buttonSetUline->SetBackgroundColour( GetBackgroundColour( ) );

	m_ChatSampleEdit->SetValue(_("The five boxing wizards jump quickly"));

#ifdef __WXMAC__
	Centre();
#endif
	
    if( !wxGetApp().Options().LoadScreenMetrics( "UserPreferencesDialog", *this ) )
    {
        wxSize size(640,480);
        wxRect defaultSize( GetPosition(), size );
        SetSize( defaultSize );
    }

    m_ContactDisplayManager.PopulateAvailableListBox( m_AvailableColumnsListBox );
    m_ContactDisplayManager.PopulateDisplayListBox( m_DisplayedColumnsListBox );

#ifdef LINUX
    m_AudioSubsystemCombo->Append( _("OSS") );
    m_AudioSubsystemCombo->Append( _("ALSA") );
#else
    m_AudioSubsystemCombo->Append( _("Standard") );
#endif

    InitLanguages();

    ApplyCurrentProfileSettings();
}

UserPreferencesDialog::~UserPreferencesDialog()
{
}

void UserPreferencesDialog::InitLanguages( )
{
    //  Fill the language selection list control
    int     iCurrLangID;
    if( wxGetApp( ).m_pLocale  !=  NULL )
    {
        iCurrLangID = wxGetApp( ).m_pLocale->GetLanguage( );
    }

    wxLanguageInfo  *pInfo;
    wxString    strDir;

    //  I need to ensure the following strings are caught by GetText for translation...
    strDir = wxString( _("&Ok") );
    strDir = wxString( _("&OK") );
    strDir = wxString( _("&No") );
    strDir = wxString( _("&Yes") );
    strDir = wxString( _("&Help") );
    strDir = wxString( _("&Apply") );
    strDir = wxString( _("&Cancel") );
    strDir = wxString( _("Personal Address Book") );
    strDir = wxString( _("Search Results") );
    strDir = wxString( _("Community Address Book") );
    strDir = wxString( _("Authorization failed.") );
    strDir = wxString( _("Proxy required authentication." ) );
    strDir = wxString( _("Proxy authentication failed.") );
    strDir = wxString( _("Unable to connect to server.") );
    strDir = wxString( _("SSL/TLS handshake failed.") );
    strDir = wxString( _("Server did not send any certificate.") );
    strDir = wxString( _("Unable to verify server's certificate.") );
    strDir = wxString( _("Server's certificate issuer is unknown.") );
    strDir = wxString( _("Server's certificate is NOT trusted.") );
    strDir = wxString( _("Connection lost.") );


    wxString    strRest;
    int         iIndex      = 0;
    wxString strBase =  wxGetApp().GetResourceFile( wxT("locale") );
    strDir = wxFindFirstFile( strBase + wxT("/*"), wxDIR );
    while( !strDir.IsEmpty( ) )
    {
        if( strDir.StartsWith( strBase, &strRest ) )
                strDir = strRest;
        if( strDir.StartsWith( wxT("."), &strRest ) )
            strDir = strRest;
        if( strDir.StartsWith( wxT("."), &strRest ) )
            strDir = strRest;
        if( strDir.StartsWith( wxT("/"), &strRest ) )
            strDir = strRest;
        if( strDir.StartsWith( wxT("\\"), &strRest ) )
            strDir = strRest;
        pInfo = (wxLanguageInfo *) wxLocale::FindLanguageInfo( strDir );
        if( pInfo )
        {
            wxString    xlate( wxGetTranslation( pInfo->CanonicalName ) );
            if( xlate.CmpNoCase( pInfo->CanonicalName )  ==  0 )
            {
                //  No translation was available - make something more understandable than "de_DE", "pt_BR", or "zh_TW"
                xlate.Empty( );
                xlate.Append( pInfo->Description );
                xlate.Append( wxT(" (") );
                xlate.Append( pInfo->CanonicalName );
                xlate.Append( wxT(")") );
            }

            m_LanguageList->Append( xlate, (void *) pInfo->Language );
            if( pInfo->Language  ==  iCurrLangID )
                m_LanguageList->SetSelection( iIndex );
            iIndex++;
        }
        strDir = wxFindNextFile( );
    }

    if( m_LanguageList->GetCount( )  <=  1 )
    {
        //  1 or fewer languages in the list - the user has no options - remove this page
        int iNumPages = m_Notebook->GetPageCount( );
        if( iNumPages > 1 )
        {
            m_Language->Hide( );
            m_LanguageList->Hide( );
            StaticText2->Hide( );
            m_Notebook->RemovePage( iNumPages - 1 );
        }
    }
    else
    {
        m_LanguageList->Fit( );
    }
}

int UserPreferencesDialog::EditLanguageOnly( )
{
    //  If no languages are installed force locale to en_US and return a wxID_CANCEL
    if( m_LanguageList->GetCount( )  <=  1 )
    {
        OPTIONS.SetSystemInt( "LocaleID", wxLANGUAGE_ENGLISH_US );
        return wxID_CANCEL;
    }

    //  Remove all the panels except Language
    m_Notebook->RemovePage( 4 );
    m_PCPhone->Hide( );

    m_Notebook->RemovePage( 3 );
    m_DataShare->Hide( );

    m_Notebook->RemovePage( 2 );
    m_Font->Hide( );

    m_Notebook->RemovePage( 1 );
    m_ContactDisplay->Hide( );

    m_Notebook->RemovePage( 0 );
    m_General->Hide( );

    //  We have languages to choose from - ask the user
    int nResp = ShowModal( );
    if( nResp == wxID_OK )
    {
        //  Language panel
        if( m_LanguageList->GetCount( )  >  1 )
        {
            int iSel;
            iSel = m_LanguageList->GetSelection( );
            if( iSel  !=  wxNOT_FOUND )
            {
                int iLocaleID = (int)(long) m_LanguageList->GetClientData( iSel );
                OPTIONS.SetSystemInt( "LocaleID", iLocaleID );
            }
        }
    }
    return nResp;
}

int UserPreferencesDialog::EditPreferences( UserPreferences & prefs )
{
    m_prefs = prefs;

    Value_to_GUI();

    int nResp = ShowModal();
    if( nResp == wxID_OK )
    {
        GUI_to_Value();
        prefs = m_prefs;
    }
    return nResp;
}

void UserPreferencesDialog::Value_to_GUI()
{
    // Enforce the profile settings.
    if( (m_enabled_features & ProfileDisableMeetingDownload) != 0 )
    {
        m_prefs.m_fDownloadPublicMeetings = false;
    }
    if( (m_enabled_features & ProfileDisableCommunityDownload) != 0 )
    {
        m_prefs.m_fDownloadCAB = false;
    }

    ///// General Panel ////////////////////////////////////////////////////////
	m_SavePasswordCheck->SetValue( m_prefs.m_fSavePassword );
	m_AutoSignOnCheck->SetValue( m_prefs.m_fAutoSignOn );
	m_StartOnComputerStartCheck->SetValue( m_prefs.m_fStartOnComputerStart );
	m_fKeepTryingConnectionCheck->SetValue( m_prefs.m_fKeepTryingConnection );
    m_DownloadPublicMeetingsCheck->SetValue( m_prefs.m_fDownloadPublicMeetings );
    m_DownloadCABCheck->SetValue( m_prefs.m_fDownloadCAB );
    m_CacheABCheck->SetValue( m_prefs.m_fCacheAB );
    m_DownloadOutlookCheck->SetValue( m_prefs.m_fDownloadOutlook );
    m_BuiltInIMCheck->SetValue( m_prefs.m_fIMClient );
    m_ShowInstantMeetingCheck->SetValue( m_prefs.m_fShowInstantMeeting );
    m_EnablePrivateMeetings->SetValue( m_prefs.m_fEnablePrivateMeetings );
    
    m_ConfigureOutlookButton->Enable( m_prefs.m_fDownloadOutlook );

    ///// Font Panel ///////////////////////////////////////////////////////////
    m_ChatSampleEdit->SelectAll();
    m_ChatSampleEdit->FormatForegroundColor( m_prefs.m_ChatFG );
    m_ChatSampleEdit->FormatBackgroundColor( m_prefs.m_ChatBG );
    m_ChatSampleEdit->FormatFontSize( m_prefs.m_nChatFontSize );
    m_ChatSampleEdit->FormatBold( m_prefs.m_fChatBold );
    m_ChatSampleEdit->FormatItalic( m_prefs.m_fChatItalic );
    m_ChatSampleEdit->FormatUnderline( m_prefs.m_fChatUnderline );
    m_ChatSampleEdit->SetSelection(0,0);

    UpdateAppFontSample();

    ///// Sound panel ///////////////////////////////////////////////////////////
	m_MuteAllSoundsCheck->SetValue( m_prefs.m_fMuteAllSounds );
	m_ChatSoundsCombo->SetSelection( m_prefs.m_nChatSounds );
	m_PlayChatInvtSoundCheck->SetValue( m_prefs.m_fPlayChatInvtSound );
	m_InMeetingSoundsCombo->SetSelection( m_prefs.m_nInMeetingSounds );
	m_DisableInMeetingSoundsCheck->SetValue( m_prefs.m_fDisableInMeetingSounds );

    ///// Data share panel ///////////////////////////////////////////////////////
	m_HidePresenterMeetingWindowCheck->SetValue( m_prefs.m_fHidePresenterMeetingWindow );
	m_AutoDetectSlowConnectionCheck->SetValue(m_prefs.m_fAutoDetectSlowConnection );
	m_ReduceColorCheck->SetValue(m_prefs.m_fReduceColor);
	m_UsePowerPointConverterCheck->SetValue(m_prefs.m_fUsePowerPoint);
	m_UseDirectXCaptureCheck->SetValue(m_prefs.m_fUseDirectX);

    // PC phone Panel ////////////////////////////////////////////////////////////
    if( m_prefs.m_nPCPhoneBandwidth == 0 )
        m_AutoBandwidthRadio->SetValue(true);
    else if( m_prefs.m_nPCPhoneBandwidth == 1 )
        m_LowBandwidthRadio->SetValue(true);
    else if( m_prefs.m_nPCPhoneBandwidth == 2 )
        m_MediumBandwidthRadio->SetValue(true);
    else
        m_HighBandwidthRadio->SetValue(true);

    if( m_prefs.m_nPCPhoneVAD == 0 )
        m_LowVadRadio->SetValue(true);
    else if( m_prefs.m_nPCPhoneVAD == 1 )
        m_MedVadRadio->SetValue(true);
    else
        m_HighVadRadio->SetValue(true);

	m_EnableEchoCancellationCheck->SetValue(m_prefs.m_fEnableEchoCancellation);
	m_EnableNoiseFilterCheck->SetValue(m_prefs.m_fEnableNoiseFilter);

    if( m_prefs.m_nAudioSubsystem == Voip_Audio_Internal_PA )
        m_AudioSubsystemCombo->SetSelection(0);
    else if( m_prefs.m_nAudioSubsystem == Voip_Audio_Internal_Alsa )
        m_AudioSubsystemCombo->SetSelection(1);

    m_MaxJitterEdit->SetValue( wxString::Format(_T("%d"), m_prefs.m_nMaxJitter) );
}

void UserPreferencesDialog::GUI_to_Value()
{
    ///// General Panel ////////////////////////////////////////////////////////
    m_prefs.m_fSavePassword = m_SavePasswordCheck->GetValue();
    m_prefs.m_fAutoSignOn = m_AutoSignOnCheck->GetValue();
    m_prefs.m_fStartOnComputerStart = m_StartOnComputerStartCheck->GetValue();
    m_prefs.m_fKeepTryingConnection = m_fKeepTryingConnectionCheck->GetValue();
    m_prefs.m_fDownloadPublicMeetings = m_DownloadPublicMeetingsCheck->GetValue();
    m_prefs.m_fDownloadCAB = m_DownloadCABCheck->GetValue();
    m_prefs.m_fCacheAB = m_CacheABCheck->GetValue();
    m_prefs.m_fIMClient = m_BuiltInIMCheck->GetValue();
    m_prefs.m_fShowInstantMeeting = m_ShowInstantMeetingCheck->GetValue();
    m_prefs.m_fEnablePrivateMeetings = m_EnablePrivateMeetings->GetValue();

    if ( m_prefs.m_fDownloadOutlook != m_DownloadOutlookCheck->GetValue() )
    {
        m_prefs.m_fDownloadOutlook = m_DownloadOutlookCheck->GetValue();
        m_refreshContacts = true;
    }

    ///// Font Panel ///////////////////////////////////////////////////////////
    // (Data is collected via user interaction)

    ///// Sound panel ///////////////////////////////////////////////////////////
    m_prefs.m_fMuteAllSounds = m_MuteAllSoundsCheck->GetValue();
	m_prefs.m_nChatSounds = m_ChatSoundsCombo->GetSelection();
    m_prefs.m_fPlayChatInvtSound = m_PlayChatInvtSoundCheck->GetValue();
    m_prefs.m_nInMeetingSounds = m_InMeetingSoundsCombo->GetSelection();
    m_prefs.m_fDisableInMeetingSounds = m_DisableInMeetingSoundsCheck->GetValue();

    m_prefs.m_fHidePresenterMeetingWindow = m_HidePresenterMeetingWindowCheck->GetValue();
    m_prefs.m_fAutoDetectSlowConnection = m_AutoDetectSlowConnectionCheck->GetValue();
    m_prefs.m_fReduceColor = m_ReduceColorCheck->GetValue();
    m_prefs.m_fUsePowerPoint = m_UsePowerPointConverterCheck->GetValue();
    m_prefs.m_fUseDirectX = m_UseDirectXCaptureCheck->GetValue();

	if( m_HighBandwidthRadio->GetValue() )
        m_prefs.m_nPCPhoneBandwidth = 3;
	else if( m_MediumBandwidthRadio->GetValue() )
        m_prefs.m_nPCPhoneBandwidth = 2;
	else if( m_LowBandwidthRadio->GetValue() )
        m_prefs.m_nPCPhoneBandwidth = 1;
    else
        m_prefs.m_nPCPhoneBandwidth = 0;

	if( m_HighVadRadio->GetValue() )
        m_prefs.m_nPCPhoneVAD = 2;
	else if( m_MedVadRadio->GetValue() )
        m_prefs.m_nPCPhoneVAD = 1;
    else
        m_prefs.m_nPCPhoneVAD = 0;

	m_prefs.m_fEnableEchoCancellation = m_EnableEchoCancellationCheck->GetValue();
	m_prefs.m_fEnableNoiseFilter = m_EnableNoiseFilterCheck->GetValue();

    int nAudioSubsystem = m_AudioSubsystemCombo->GetSelection();
    if( nAudioSubsystem == 0 )
        m_prefs.m_nAudioSubsystem = Voip_Audio_Internal_PA;
    else if( nAudioSubsystem == 1 )
        m_prefs.m_nAudioSubsystem = Voip_Audio_Internal_Alsa;

    long jitter;
    m_MaxJitterEdit->GetValue().ToLong(&jitter);
    if (jitter > 0)
        m_prefs.m_nMaxJitter = jitter;

    //  Language panel
    if( m_LanguageList->GetCount( )  >  1 )
    {
        int iSel;
        iSel = m_LanguageList->GetSelection( );
        if( iSel  !=  wxNOT_FOUND )
        {
            int iLocaleID = (int)(long) m_LanguageList->GetClientData( iSel );
            OPTIONS.SetSystemInt( "LocaleID", iLocaleID );
        }
    }
}

//*****************************************************************************
void UserPreferencesDialog::OnClickFore(wxCommandEvent& event)
{
    wxColourData    wData;
    wData.SetChooseFull( true );
    wxColour        wCol;
    m_ChatSampleEdit->GetForeground( wCol );
    wData.SetColour( wCol );

    wxColourDialog  wColourDlg( this, &wData );
    wColourDlg.SetTitle( wxT("Font colour") );
    if( wColourDlg.ShowModal( )  ==  wxID_OK )
    {
        wxColourData    wRet;
        wRet = wColourDlg.GetColourData( );
        m_prefs.m_ChatFG = wRet.GetColour();

        m_ChatSampleEdit->SelectAll();
        m_ChatSampleEdit->FormatForegroundColor( m_prefs.m_ChatFG );
        m_ChatSampleEdit->SetSelection(0,0);
    }
}


//*****************************************************************************
void UserPreferencesDialog::OnClickBack(wxCommandEvent& event)
{
    wxColourData    wData;
    wData.SetChooseFull( true );
    wxColour        wCol;
    m_ChatSampleEdit->GetBackground( wCol );
    wData.SetColour( wCol );

    wxColourDialog  wColourDlg( this, &wData );
    wColourDlg.SetTitle( wxT("Background colour") );
    if( wColourDlg.ShowModal( )  ==  wxID_OK )
    {
        wxColourData    wRet;
        wRet = wColourDlg.GetColourData( );
        m_prefs.m_ChatBG = wRet.GetColour();

        m_ChatSampleEdit->SelectAll();
        m_ChatSampleEdit->FormatBackgroundColor( m_prefs.m_ChatBG );
        m_ChatSampleEdit->SetSelection(0,0);
    }
}


//*****************************************************************************
void UserPreferencesDialog::OnClickSmaller(wxCommandEvent& event)
{
    m_ChatSampleEdit->SelectAll();
    m_prefs.m_nChatFontSize = m_ChatSampleEdit->FormatFontSizeDecrease();
    m_ChatSampleEdit->SetSelection(0,0);
}

//*****************************************************************************
void UserPreferencesDialog::OnClickNorm(wxCommandEvent& event)
{
    m_ChatSampleEdit->SelectAll();
    m_prefs.m_nChatFontSize = m_ChatSampleEdit->FormatFontSizeNormal();
    m_ChatSampleEdit->SetSelection(0,0);
}


//*****************************************************************************
void UserPreferencesDialog::OnClickLarger(wxCommandEvent& event)
{
    m_ChatSampleEdit->SelectAll();
    m_prefs.m_nChatFontSize = m_ChatSampleEdit->FormatFontSizeIncrease();
    m_ChatSampleEdit->SetSelection(0,0);
}


//*****************************************************************************
void UserPreferencesDialog::OnClickBold(wxCommandEvent& event)
{
    m_ChatSampleEdit->SelectAll();
    m_prefs.m_fChatBold = m_buttonSetBold->IsDown();
    m_ChatSampleEdit->FormatBold( m_prefs.m_fChatBold );
    m_ChatSampleEdit->SetSelection(0,0);
}


//*****************************************************************************
void UserPreferencesDialog::OnClickItalics(wxCommandEvent& event)
{
    m_ChatSampleEdit->SelectAll();
    m_prefs.m_fChatItalic = m_buttonSetItalics->IsDown( );
    m_ChatSampleEdit->FormatItalic( m_prefs.m_fChatItalic );
    m_ChatSampleEdit->SetSelection(0,0);
}


//*****************************************************************************
void UserPreferencesDialog::OnClickUline(wxCommandEvent& event)
{
    m_ChatSampleEdit->SelectAll();
    m_prefs.m_fChatUnderline = m_buttonSetUline->IsDown( );
    m_ChatSampleEdit->FormatUnderline( m_prefs.m_fChatUnderline );
    m_ChatSampleEdit->SetSelection(0,0);
}

void UserPreferencesDialog::OnClickAppFont(wxCommandEvent& event)
{
    // Populate a font data object to initialize the selected font in the font dlg.
    wxFontData fdata;
    fdata.SetInitialFont(m_prefs.m_AppFont);

    // Get the users selection
    wxFontDialog Dlg(this, fdata );
    int nResp = Dlg.ShowModal();
    if( nResp == wxID_OK )
    {
        fdata = Dlg.GetFontData();
        m_prefs.m_AppFont = fdata.GetChosenFont();
        UpdateAppFontSample();
    }
}


//*****************************************************************************
void UserPreferencesDialog::OnClickHelp(wxCommandEvent& event)
{
    wxString help( wxGetApp().Options().GetSystem( "HelpPath", "" ),  wxConvUTF8);
    help += LOCALE->GetCanonicalName( );
    help += wxT("/");
    help += wxT("preferences.htm");
    wxLaunchDefaultBrowser( help );
}

void UserPreferencesDialog::UpdateAppFontSample()
{
    wxString faceName;
    faceName = m_prefs.m_AppFont.GetFaceName();

    wxString points;
    points = wxString::Format(wxT("%d Pt."), m_prefs.m_AppFont.GetPointSize() );

    wxString sample;
    sample = faceName;
    sample += wxT(" ");
    sample += points;

    m_FontSampleEdit->SetFont( m_prefs.m_AppFont );
    m_FontSampleEdit->SetValue( sample );
}


void UserPreferencesDialog::OnSavePasswordCheckClick(wxCommandEvent& event)
{
    if( !m_SavePasswordCheck->GetValue() )
    {
        m_AutoSignOnCheck->SetValue(false);
    }
}

void UserPreferencesDialog::OnAutoSignOnCheckClick(wxCommandEvent& event)
{
    if( m_AutoSignOnCheck->GetValue() )
    {
        if( !m_SavePasswordCheck->GetValue() )
        {
            m_SavePasswordCheck->SetValue(true);
        }
    }
}

void UserPreferencesDialog::OnSize( wxSizeEvent& event )
{
    wxGetApp().Options().SaveScreenMetrics( "UserPreferencesDialog", *this );
    event.Skip();
}

void UserPreferencesDialog::OnMove( wxMoveEvent& event )
{
    wxGetApp().Options().SaveScreenMetrics( "UserPreferencesDialog", *this );
    event.Skip();
}

void UserPreferencesDialog::OnMoveToDisplayedButtonClick(wxCommandEvent& event)
{
    m_ContactDisplayManager.ProcessMoveToDisplayed( m_AvailableColumnsListBox, m_DisplayedColumnsListBox );
}

void UserPreferencesDialog::OnMoveToAvailableButtonClick(wxCommandEvent& event)
{
    m_ContactDisplayManager.ProcessMoveFromDisplayed( m_DisplayedColumnsListBox, m_AvailableColumnsListBox );
}

void UserPreferencesDialog::OnMoveColumnsUpButtonClick(wxCommandEvent& event)
{
    m_ContactDisplayManager.ProcessMove( m_DisplayedColumnsListBox, true );
}

void UserPreferencesDialog::OnMoveColumnsDownButtonClick(wxCommandEvent& event)
{
    m_ContactDisplayManager.ProcessMove( m_DisplayedColumnsListBox, false );
}

void UserPreferencesDialog::ApplyCurrentProfileSettings()
{
    profile_t profile = CONTROLLER.UserProfile();
    m_enabled_features = (profile) ? profile->enabled_features : ProfileUserSavePassword;

    if( (m_enabled_features & ProfileUserSavePassword) == 0 )
    {
        // The user cannot save passwords, so....
        m_AutoSignOnCheck->Enable( false );
        m_SavePasswordCheck->Enable( false );
    }

	if( (m_enabled_features & ProfileDisableMeetingDownload) != 0 )
	{
	    m_DownloadPublicMeetingsCheck->Enable( false );
	}

	if( (m_enabled_features & ProfileDisableCommunityDownload) != 0 )
	{
        m_DownloadCABCheck->Enable( false );
	}
}

void UserPreferencesDialog::OnConfigureOutlookButtonClick(wxCommandEvent& event)
{
    if (CONTROLLER.Directories()->Configure(wxT("ms_outlook"), this))
        m_refreshContacts = true;
}

void UserPreferencesDialog::OnDownloadOutlookCheckClick(wxCommandEvent& event)
{
    m_ConfigureOutlookButton->Enable( event.IsChecked() );
    
    if ( event.IsChecked() && !CONTROLLER.Directories()->IsConfigured(wxT("ms_outlook")))
        if (CONTROLLER.Directories()->Configure(wxT("ms_outlook"), this))
            m_refreshContacts = true;
}
