/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 *
 *
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */

#include "app.h"
#include "webdav.h"
#include "base64.h"

#include <wx/uri.h>
#include <wx/file.h>
#include <wx/mstream.h>
#include <wx/xml/xml.h>

#include "badauthcertdialog.h"
#include "certhandler.h"


const unsigned long iChunkSize = 2048;

// This is not yet defined in mingw's wininet.h
// Hopefully it will be someday, and this can be removed!
#define ERROR_WINHTTP_SECURE_CERT_REV_FAILED (INTERNET_ERROR_BASE+57)


//***********************************************************************


bool CWebDavUtils::m_bIgnoreUnknownCA = false;
bool CWebDavUtils::m_bIgnoreCertRev = false;
bool CWebDavUtils::m_bEnableExternalAuth = false;
wxString CWebDavUtils::m_strCAFile;
wxString CWebDavUtils::m_strLastCert;

#if 0
//*****************************************************************************
static void genBasicAuthString( const wxString& userName, const wxString& passwd, wxString& strRet, bool fPlainText )
{
    std::string name;
    char        unEncoded[ 200 ];

    name.append( userName.mb_str(wxConvUTF8) );

    if (!fPlainText)
    {
        char        hash_passwd[ 41 ];
        session_sha_hash( passwd.mb_str( ), hash_passwd );

        sprintf( unEncoded, "%s:%s", name.c_str( ), hash_passwd );
    }
    else
    {
        sprintf( unEncoded, "%s:%s", name.c_str( ), (const char *)passwd.mb_str(wxConvUTF8) );
    }

    CBase64     base64Encoder;
    wxString    strEncrypted( base64Encoder.Encode( unEncoded, strlen( unEncoded ) ), wxConvUTF8 );

    strRet = wxT("Authorization: Basic ");
    strRet += strEncrypted;
}
#endif


//*****************************************************************************
CWebDavUtils::CWebDavUtils()
{
    m_bShowErrors = true;
	m_InternetSession = NULL;
	m_Connection = NULL;
}


//*****************************************************************************
CWebDavUtils::~CWebDavUtils()
{
	CloseConnection( );
}

//*****************************************************************************
bool CWebDavUtils::OpenConnection(const wxString& aServerUrl, const wxString& aUserName, const wxString& aPasswd)
{
    wxLogDebug( wxT("entering WebDavUtils::OpenConnection( )") );
    if (m_Connection!=NULL)
    {
        wxLogDebug( wxT("  connection already open") );
        return TRUE;
    }

    int         iTimeout         = 5000;	// 20 second timeout spread over 4 options - Connect, Recv, Send, etc.
    wxString    strHttpAppName( wxT("IICDocShareClient") );
    m_InternetSession = netHttpOpen( strHttpAppName.mb_str( wxConvUTF8 ), (iTimeout * 4)/1000, false );
    if( m_InternetSession )
    {
        wxString        strHost;
        wxString        strScheme;
        wxString        strPath;
        unsigned short  usPort;
        wxURI       uri( aServerUrl );
        if( uri.HasServer( ) )
            strHost = uri.GetServer( );
        if( uri.HasScheme( ) )
            strScheme = uri.GetScheme( );
        if( uri.HasPath( ) )
            strPath = uri.GetPath( );
        if( uri.HasPort( ) )
            usPort = wxAtoi( uri.GetPort( ) );
        else
        {
            if( strScheme.CmpNoCase( wxT("HTTPS") )  == 0 )
                usPort = INTERNET_DEFAULT_HTTPS_PORT;       //  use the default port for HTTPS if scheme is HTTPS
            else
                usPort = INTERNET_DEFAULT_HTTP_PORT;        //  use the default port for HTTP if none is otherwise specified
        }

        if( strHost.IsEmpty( )  ||  strScheme.IsEmpty( ) )
		{
			wxLogDebug( wxT("Cannot parse URL: %s\n"), aServerUrl.wc_str( ));
		}
		else
		{
			wxString strPassword;

			if (!m_bEnableExternalAuth)
			{
				char hash_passwd[41];
                session_sha_hash( aPasswd.mb_str( ), hash_passwd );

                wxString    strT( hash_passwd, wxConvUTF8 );
                strPassword = strT;
			}
			else
			{
				// Pass plain-text password for external auth.
				strPassword = aPasswd;
			}

            m_Connection = netHttpConnect( m_InternetSession, aServerUrl.mb_str( wxConvUTF8 ), strHost.mb_str( wxConvUTF8 ),
                                           usPort, aUserName.mb_str( wxConvUTF8 ), strPassword.mb_str( wxConvUTF8 ) );
#ifdef __WXMSW__
            if (m_Connection!=NULL)
			{
				InternetSetOption(m_Connection, INTERNET_OPTION_CONNECT_TIMEOUT,      &iTimeout, 4);
				InternetSetOption(m_Connection, INTERNET_OPTION_DATA_SEND_TIMEOUT,    &iTimeout, 4);
				InternetSetOption(m_Connection, INTERNET_OPTION_DATA_RECEIVE_TIMEOUT, &iTimeout, 4);
            }
#endif      //  #ifdef __WXMSW__
		}
	}

    wxLogDebug( wxT("    leaving WebDavUtils::OpenConnection( )"), m_Connection );
	return( m_Connection != NULL );
}


//*****************************************************************************
void CWebDavUtils::CloseConnection()
{
    netHttpClose( m_InternetSession, m_Connection );
    m_Connection = NULL;
    m_InternetSession = NULL;
}


//*****************************************************************************
void CWebDavUtils::SetInternetSecurityOptions( HREQUEST hRequest )
{
#ifdef __WXMSW__
    // TODO: move into nethttp layer
    if (m_bIgnoreUnknownCA || m_bIgnoreCertRev)
    {
        unsigned long dwFlags = 0;
        unsigned long dwBuffLen = sizeof(dwFlags);

        InternetQueryOption (hRequest, INTERNET_OPTION_SECURITY_FLAGS,
                             (LPVOID)&dwFlags, &dwBuffLen);

        if (m_bIgnoreUnknownCA)
            dwFlags |= (SECURITY_FLAG_IGNORE_UNKNOWN_CA | SECURITY_FLAG_IGNORE_CERT_CN_INVALID | SECURITY_FLAG_IGNORE_CERT_DATE_INVALID);

        if (m_bIgnoreCertRev)
            dwFlags |= SECURITY_FLAG_IGNORE_REVOCATION;

        InternetSetOption (hRequest, INTERNET_OPTION_SECURITY_FLAGS,
                           &dwFlags, sizeof (dwFlags) );
    }
#else
    netSetSecurityOptions( m_InternetSession, m_strCAFile.mbc_str() );
#endif
    if (!m_strHeaders.IsEmpty())
    {
        netHttpAddRequestHeaders( hRequest, m_strHeaders.mb_str( wxConvUTF8 ), m_strHeaders.Len( ) );
    }
}


bool CWebDavUtils::SetAuthentication( HREQUEST hRequest, const wxString & userName, const wxString & password)
{
#ifdef __WXMSW__
    // Not sure this is needed anymore, we set username and password when creating connection
    wxString userPassword;

    if (!m_bEnableExternalAuth)
    {
        char        hash_passwd[ 41 ];
        session_sha_hash( password.mb_str( ), hash_passwd );
        userPassword.FromUTF8(hash_passwd);
    }
    else
    {
        userPassword = password;
    }

    InternetSetOption(hRequest, INTERNET_OPTION_USERNAME,
                      (void*)userName.c_str(), userName.length());
    InternetSetOption(hRequest, INTERNET_OPTION_PASSWORD,
                      (void*)userPassword.c_str(), userPassword.length());

    return true;
#else
    return false;
#endif
}


//*****************************************************************************
int CWebDavUtils::DAVRetrieveInfo( wxWindow* pWindow, const wxString& aUrl, const wxString& aUrlPath,
                                      const wxString& userName, const wxString& passwd,
                                      wxString& outXML, wxString& strStatusCode, wxString& strStatusText)
{
    int    hr = S_OK;
	int flags =INTERNET_FLAG_RELOAD|INTERNET_FLAG_DONT_CACHE;

	wxString proto = aUrl.Left(5);
	proto.MakeUpper();
	if (proto == wxT("HTTPS"))
	{
		flags |= INTERNET_FLAG_SECURE |
				INTERNET_FLAG_IGNORE_CERT_DATE_INVALID |
				INTERNET_FLAG_IGNORE_CERT_CN_INVALID;
	}

	wxString strQuery( wxT("<?xml version=\"1.0\"?>\n")
                       wxT("<A:propfind xmlns:A=\"DAV:\">\n")
                       wxT("<A:prop>\n")
                       wxT("<A:displayname/>\n")
                       wxT("<A:resourcetype/>\n")
                       wxT("<A:getcontenttype/>\n")
                       wxT("<A:getcontentlength/>\n")
                       wxT("<A:getlastmodified/>\n")
                       wxT("<A:lockdiscovery/>\n")
                       wxT("<A:checked-in/>\n")
                       wxT("<A:checked-out/>\n")
                       wxT("<A:version-name/>\n")
                       wxT("</A:prop>\n")
                       wxT("</A:propfind>\n") );

	if (m_Connection==NULL || m_InternetSession==NULL)
	{
		OpenConnection(aUrl, userName, passwd);
	}

    HREQUEST hRequest   = NULL;
	if (m_InternetSession)
	{
        if (m_Connection)
        {
            wxString    strCmd( wxT("PROPFIND") );
            hRequest = netHttpOpenRequest( m_Connection, strCmd.mb_str( wxConvUTF8 ), aUrlPath.mb_str( wxConvUTF8 ), NULL, flags );
            if( hRequest )
            {
                wxString   strT = wxT("Depth: 1");
                netHttpAddRequestHeaders( hRequest, strT.mb_str( wxConvUTF8 ), strT.Len( ) );
                strT = wxT("Content-type: text/xml");
                netHttpAddRequestHeaders( hRequest, strT.mb_str( wxConvUTF8 ), strT.Len( ) );

                int count = 6;
                while (count-->0)
                {
                    SetInternetSecurityOptions( hRequest );

                    std::string     sContent;
                    std::string     sQuery      = (const char *) strQuery.mb_str( wxConvUTF8 );
                    if( netHttpSendRequest( hRequest, m_InternetSession, sContent.c_str( ), sContent.length( ),
                                            sQuery.c_str( ), sQuery.length( ) ) )
                    {
//                        wxLogDebug( wxT("  after call to netHttpSendRequest( )  --  it returned TRUE") );
                        char    StatusCode[MAX_PATH];
                        unsigned long dwBufferLength=MAX_PATH-1, dwIndex=0;

                        int     iLC = 0;
                        while( !netHttpQueryResultsReady( hRequest, TRUE ) )
                        {
                            CUtils::MsgWait( 20, 1 );
                            iLC++;
                        }

                        if( netHttpQueryInfo( hRequest, HTTP_QUERY_STATUS_CODE, StatusCode, &dwBufferLength, &dwIndex ) )
                        {
//                            wxLogDebug( wxT("  after call to netHttpQueryInfo( )  --  it returned TRUE") );
                            strStatusCode = wxString( StatusCode, wxConvUTF8 );
                            dwBufferLength=MAX_PATH-1;
                            dwIndex=0;
                            if( netHttpQueryInfo( hRequest, HTTP_QUERY_STATUS_TEXT, StatusCode, &dwBufferLength, &dwIndex ) )
                            {
                                strStatusText = wxString( StatusCode, wxConvUTF8 );
                            }
//							if (_tcscmp(strStatusCode, wxT("207"))==0)
                            {
                                unsigned long   dwRead;
                                char szTemp[iChunkSize+1];
                                INTERNET_BUFFERSA Output;
                                memset(&Output,0,sizeof(Output));
                                Output.dwStructSize = sizeof(Output);
                                Output.lpvBuffer = szTemp;
                                Output.Next = &Output;
                                Output.dwBufferLength = iChunkSize;

                                while( netInternetReadFileExA( hRequest, &Output ) )
                                {
                                    dwRead = Output.dwBufferLength;
                                    if (!dwRead) break;
                                    Output.dwBufferLength = iChunkSize;
                                    szTemp[dwRead]='\0';
                                    outXML.Append( wxString(szTemp, wxConvUTF8) );
                                }
                            }
                        }
                        else
                        {
//                            wxLogDebug( wxT("  after call to netHttpQueryInfo( )  --  it returned FALSE") );
                        }
                    }
                    else
                    {
//                        wxLogDebug( wxT("  after call to netHttpSendRequest( )  --  it returned FALSE") );
                        int err = GetLastError();
                        if (err==ERROR_INTERNET_TIMEOUT)			// ERROR_INTERNET_TIMEOUT
                        {
                            continue;
                        }
                        else if (err == ERROR_INTERNET_INVALID_CA)
                        {
                            if (m_bIgnoreUnknownCA || (netInternetErrorDlg(pWindow ? pWindow->GetHandle() : NULL, hRequest, err) != ERROR_CANCELLED))
                            {
                                if (err == ERROR_INTERNET_INVALID_CA)
                                    m_bIgnoreUnknownCA = true;
                                continue;
                            }
                        }
                        else if( err == ERROR_INTERNET_SECURITY_CHANNEL_ERROR )
                        {
                            if( !m_bIgnoreUnknownCA )
                            {
                                m_bIgnoreUnknownCA = true;
                            }
                            continue;
                        }
                        else if( err == ERROR_WINHTTP_SECURE_CERT_REV_FAILED )
                        {
                            if( !m_bIgnoreCertRev )
                            {
                                m_bIgnoreCertRev = true;
                            }
                            continue;
                        }

                        wxString strErrMsg( _("Could not send request to server."));
                        hr = err|0x80070000;
                        if (err != ERROR_INTERNET_INVALID_CA)
                        {
                            CUtils::ReportErrorMsg(hr, FALSE, strErrMsg, pWindow );
                        }
                        wxLogDebug( wxT("%s"), strErrMsg.wc_str( ));
                    }
                    break;
                }	// while
            }	// if (hRequest)
            else
            {
                wxString strErrMsg( _("Could not create a request handle."));
                hr = GetLastError()|0x80070000;
                CUtils::ReportErrorMsg(hr, FALSE, strErrMsg, pWindow );
                wxLogDebug( wxT("%s"), strErrMsg.wc_str( ));
            }
        } // if (hConnection)
        else
        {
            wxString strErrMsg( _("Could not create connection to server: "));

            wxString    strHost;
            wxURI       uri( aUrl );
            if( uri.HasServer( ) )
                strHost = uri.GetServer( );

            if( uri.HasServer( ) )
                strErrMsg += strHost;
            else
                strErrMsg += aUrl;
            hr = GetLastError()|0x80070000;
            CUtils::ReportErrorMsg(hr, FALSE, strErrMsg, pWindow );
            wxLogDebug( wxT("%s"), strErrMsg.wc_str( ));
        }
	} // if (hInternetSession)
	else
	{
        wxString strErrMsg( _("Could not create HTTP session."));
		hr = GetLastError()|0x80070000;
		CUtils::ReportErrorMsg(hr, FALSE, strErrMsg, pWindow );
		wxLogDebug( wxT("%s"), strErrMsg.wc_str( ));
	}

	if( hRequest )
        netInternetCloseHandle( hRequest );

	return hr;
}


//*****************************************************************************
int CWebDavUtils::DAVLockUnlock( wxWindow* pWindow, const wxString& aParamUrl, const wxString& aUrlPath, const wxString& userName,
                                    const wxString& passwd, const wxChar* lpLockToken, bool bLock, std::string& outXML,
                                    wxString& strStatusCode, wxString& strStatusText)
{
    int         hr = S_OK;
    wxString    aUrl = aParamUrl;

#ifdef __WXMSW__
    aUrl += aUrlPath;
#endif

    wxLogDebug( wxT("entering WebDavUtils::DAVLockUnlock(%s)"), aUrl.wc_str( ) );

    const wxChar* strLock = wxT("<?xml version=\"1.0\"?>\n")
                     wxT("<A:lockinfo xmlns:A=\"DAV:\">\n")
                     wxT("<A:locktype>\n")
                     wxT("<A:write/>\n")
                     wxT("</A:locktype>\n")
                     wxT("<A:lockscope>\n")
                     wxT("<A:exclusive/>\n")
                     wxT("</A:lockscope>\n")
                     wxT("<A:owner>\n")
                     wxT("<A:href>%s</A:href>\n")
                     wxT("</A:owner>\n")
                     wxT("</A:lockinfo>\n");

	wxString strQuery;

	if (bLock)
	{
        strQuery = wxString::Format(strLock, userName.wc_str( ));
	}

	int flags =INTERNET_FLAG_RELOAD|INTERNET_FLAG_DONT_CACHE;

	wxString proto = aUrl.Left(5);
	proto.MakeUpper();
	if (proto == wxT("HTTPS"))
	{
		flags |= INTERNET_FLAG_SECURE |
				INTERNET_FLAG_IGNORE_CERT_DATE_INVALID |
				INTERNET_FLAG_IGNORE_CERT_CN_INVALID;
	}

	if (m_Connection==NULL || m_InternetSession==NULL)
	{
		OpenConnection(aUrl, userName, passwd);
	}

    HREQUEST hRequest   = NULL;
	if (m_InternetSession)
	{
        if (m_Connection)
        {
            wxString    strCmd( bLock ? wxT("LOCK") : wxT("UNLOCK") );
            hRequest = netHttpOpenRequest(m_Connection, strCmd.mb_str( wxConvUTF8 ), aUrlPath.mb_str( wxConvUTF8 ), NULL, flags );
            if( hRequest )
            {
                SetInternetSecurityOptions( hRequest );

                if (bLock)
                {
                    wxString    strT( wxT("Timeout: Second-28800") );
                    netHttpAddRequestHeaders( hRequest, strT.mb_str( wxConvUTF8 ), strT.Len( ) );
                    strT = wxT("Depth: infinity");
                    netHttpAddRequestHeaders( hRequest, strT.mb_str( wxConvUTF8 ), strT.Len( ) );
                    strT = wxT("Content-type: text/xml");
                    netHttpAddRequestHeaders( hRequest, strT.mb_str( wxConvUTF8 ), strT.Len( ) );
                }
                else
                {
                    wxString   strT = wxT("Depth: infinity");
                    netHttpAddRequestHeaders( hRequest, strT.mb_str( wxConvUTF8 ), strT.Len( ) );
                    strT = wxT("Content-type: text/xml");
                    netHttpAddRequestHeaders( hRequest, strT.mb_str( wxConvUTF8 ), strT.Len( ) );
                    strT.Printf(wxT("Lock-Token: <%s>"), lpLockToken);
                    netHttpAddRequestHeaders( hRequest, strT.mb_str( wxConvUTF8 ), strT.Len( ) );
                    strQuery.Empty();
                }

                int count = 6;
                while (count-- > 0)
                {
                    std::string     sContent;
                    std::string     sQuery      = (const char *) strQuery.mb_str( wxConvUTF8 );

                    if( netHttpSendRequest(hRequest, m_InternetSession, sContent.c_str( ), sContent.length( ), sQuery.c_str( ), sQuery.length( ) ) )
                    {

                        int     iLC = 0;
                        while( !netHttpQueryResultsReady( hRequest, TRUE ) )
                        {
                            CUtils::MsgWait( 20, 1 );
                            iLC++;
                        }

                        char    StatusCode[MAX_PATH];
                        unsigned long dwBufferLength=MAX_PATH-1, dwIndex=0;
                        if( netHttpQueryInfo(hRequest, HTTP_QUERY_STATUS_CODE, StatusCode, &dwBufferLength, &dwIndex))
                        {
                            strStatusCode = wxString( StatusCode, wxConvUTF8 );
                            dwBufferLength=MAX_PATH-1;
                            dwIndex=0;
                            if( netHttpQueryInfo(hRequest, HTTP_QUERY_STATUS_TEXT, StatusCode, &dwBufferLength, &dwIndex))
                            {
                                strStatusText = wxString( StatusCode, wxConvUTF8 );
                            }
//							if (_tcscmp(strStatusCode, wxT("200"))==0)
                            {
                                unsigned long   dwRead;
                                char szTemp[iChunkSize+1];
                                INTERNET_BUFFERSA Output;
                                memset(&Output,0,sizeof(Output));
                                Output.dwStructSize = sizeof(Output);
                                Output.lpvBuffer = szTemp;
                                Output.Next = &Output;
                                Output.dwBufferLength = iChunkSize;

                                while( netInternetReadFileExA( hRequest, &Output ) )
                                {
                                    dwRead = Output.dwBufferLength;
                                    if (!dwRead) break;
                                    Output.dwBufferLength = iChunkSize;
                                    szTemp[dwRead]='\0';
                                    outXML += szTemp;
                                }
                            }
                        }
                    }
                    else
                    {
                        int err = GetLastError();
                        if (err==ERROR_INTERNET_TIMEOUT)			// ERROR_INTERNET_TIMEOUT
                        {
                            continue;
                        }
                        else if (err == ERROR_INTERNET_INVALID_CA)
                        {
                            if (m_bIgnoreUnknownCA || (netInternetErrorDlg(pWindow ? pWindow->GetHandle() : NULL, hRequest, err) != ERROR_CANCELLED))
                            {
                                if (err == ERROR_INTERNET_INVALID_CA)
                                    m_bIgnoreUnknownCA = true;
                                continue;
                            }
                        }

                        wxString strErrMsg( _("Could not send request to server."));
                        hr = err|0x80070000;
                        CUtils::ReportErrorMsg(hr, FALSE, strErrMsg, pWindow );
                        wxLogDebug( wxT("%s"), strErrMsg.wc_str( ));
                    }
                    break;
                }	// while
            }	// if (hRequest)
            else
            {
                wxString strErrMsg( _("Could not create a request handle."));
                hr = GetLastError()|0x80070000;
                CUtils::ReportErrorMsg(hr, FALSE, strErrMsg, pWindow );
                wxLogDebug( wxT("%s"), strErrMsg.wc_str( ));
            }

        } // if (hConnection)
        else
        {
            wxString strErrMsg( _("Could not create connection to server: "));

            wxString    strHost;
            wxURI       uri( aUrl );
            if( uri.HasServer( ) )
                strHost = uri.GetServer( );

            if( uri.HasServer( ) )
                strErrMsg += strHost;
            else
                strErrMsg += aUrl;
            hr = GetLastError()|0x80070000;
            CUtils::ReportErrorMsg(hr, FALSE, strErrMsg, pWindow );
            wxLogDebug( wxT("%s"), strErrMsg.wc_str( ));
        }
	} // if (hInternetSession)
	else
	{
        wxString strErrMsg( _("Could not create HTTP session."));
		hr = GetLastError()|0x80070000;
		CUtils::ReportErrorMsg(hr, FALSE, strErrMsg, pWindow );
		wxLogDebug( wxT("%s"), strErrMsg.wc_str( ));
	}

	if( hRequest )
		netInternetCloseHandle( hRequest );

    CloseConnection( );

    wxLogDebug( wxT("    leaving WebDavUtils::DAVLockUnlock %s %d"), strStatusCode.c_str(), hr );

	return hr;
}


//*****************************************************************************
int CWebDavUtils::DAVCopyFolder(wxWindow *pWindow, const wxString& source, const wxString& aUrlPath,
                                    const wxString& destination, const wxString& userName, const wxString& passwd,
                                    std::string& outXML, wxString& strStatusCode, wxString& strStatusText)
{
	int         hr = S_OK;
    wxString    strQuery( wxT("<?xml version=\"1.0\"?>\n")
                          wxT("<A:propertybehavior xmlns:A=\"DAV:\">\n")
                          wxT("<A:keepalive>*</A:keepalive>\n")
                          wxT("</A:propertybehavior>\n") );
	int flags =INTERNET_FLAG_RELOAD|INTERNET_FLAG_DONT_CACHE;

    wxString    strFileURL  = source;

#ifdef __WXMSW__
    strFileURL += aUrlPath;
#endif

    wxLogDebug(wxT("Entering DAVCopyFolder(%s,%s)"), strFileURL.c_str(), destination.c_str());

	wxString proto = source.Left(5);
	proto.MakeUpper();
	if (proto == wxT("HTTPS"))
	{
		flags |= INTERNET_FLAG_SECURE |
				INTERNET_FLAG_IGNORE_CERT_DATE_INVALID |
				INTERNET_FLAG_IGNORE_CERT_CN_INVALID;
	}

	if (m_Connection==NULL || m_InternetSession==NULL)
	{
		OpenConnection( strFileURL, userName, passwd);
	}

    HREQUEST hRequest   = NULL;
	if (m_InternetSession)
	{
        if (m_Connection)
        {
            wxString    strCmd( wxT("COPY") );
            hRequest = netHttpOpenRequest( m_Connection, strCmd.mb_str( wxConvUTF8 ), aUrlPath.mb_str( wxConvUTF8 ), NULL, flags );
            if( hRequest )
            {
                wxString    strT = wxT("Destination: ");
                strT.Append( destination );
                netHttpAddRequestHeaders( hRequest, strT.mb_str( wxConvUTF8 ), strT.Len( ) );
                strT = wxT("Content-type: text/xml");
                netHttpAddRequestHeaders( hRequest, strT.mb_str( wxConvUTF8 ), strT.Len( ) );

                int count = 6;
                while (count-- > 0)
                {
                    SetInternetSecurityOptions( hRequest );

                    std::string     sContent;
                    std::string     sQuery      = (const char *) strQuery.mb_str( wxConvUTF8 );
                    if( netHttpSendRequest(hRequest, m_InternetSession, sContent.c_str( ), sContent.length( ), sQuery.c_str( ), sQuery.length( ) ) )
                    {

                        int     iLC = 0;
                        while( !netHttpQueryResultsReady( hRequest, TRUE ) )
                        {
                            CUtils::MsgWait( 20, 1 );
                            iLC++;
                        }

                        char    StatusCode[MAX_PATH];
                        unsigned long dwBufferLength=MAX_PATH-1, dwIndex=0;
                        if( netHttpQueryInfo(hRequest, HTTP_QUERY_STATUS_CODE, StatusCode, &dwBufferLength, &dwIndex))
                        {
                            strStatusCode = wxString( StatusCode, wxConvUTF8 );
                            dwBufferLength=MAX_PATH-1;
                            dwIndex=0;
                            if( netHttpQueryInfo(hRequest, HTTP_QUERY_STATUS_TEXT, StatusCode, &dwBufferLength, &dwIndex))
                            {
                                strStatusText = wxString( StatusCode, wxConvUTF8 );
                            }
//							if (_tcscmp(strStatusCode, wxT("207"))==0)
                            {
                                unsigned long dwRead;
                                char szTemp[iChunkSize+1];
                                INTERNET_BUFFERSA Output;
                                memset(&Output,0,sizeof(Output));
                                Output.dwStructSize = sizeof(Output);
                                Output.lpvBuffer = szTemp;
                                Output.Next = &Output;
                                Output.dwBufferLength = iChunkSize;

                                while( netInternetReadFileExA( hRequest, &Output ) )
                                {
                                    dwRead = Output.dwBufferLength;
                                    if (!dwRead) break;
                                    Output.dwBufferLength = iChunkSize;
                                    szTemp[dwRead]='\0';
                                    outXML += szTemp;
                                }
                            }
                        }
                    }
                    else
                    {
                        int err = GetLastError();
                        if (err==ERROR_INTERNET_TIMEOUT)			// ERROR_INTERNET_TIMEOUT
                        {
                            continue;
                        }
                        else if (err == ERROR_INTERNET_INVALID_CA)
                        {
                            if (m_bIgnoreUnknownCA || (netInternetErrorDlg(pWindow ? pWindow->GetHandle() : NULL, hRequest, err) != ERROR_CANCELLED))
                            {
                                if (err == ERROR_INTERNET_INVALID_CA)
                                    m_bIgnoreUnknownCA = true;
                                continue;
                            }
                        }
                        wxString strErrMsg( _("Could not send request to server."));
                        hr = err|0x80070000;
                        CUtils::ReportErrorMsg(hr, FALSE, strErrMsg, pWindow );
                        wxLogDebug( wxT("%s"), strErrMsg.wc_str( ));
                    }
                    break;
                }	// while
            }	// if (hRequest)
            else
            {
                wxString strErrMsg( _("Could not create a request handle."));
                hr = GetLastError()|0x80070000;
                CUtils::ReportErrorMsg(hr, FALSE, strErrMsg, pWindow );
                wxLogDebug( wxT("%s"), strErrMsg.wc_str( ));
            }

        } // if (hConnection)
        else
        {
            wxString strErrMsg( _("Could not create connection to server: "));

            wxString    strHost;
            wxURI       uri( source );
            if( uri.HasServer( ) )
                strHost = uri.GetServer( );

            if( uri.HasServer( ) )
                strErrMsg += strHost;
            else
                strErrMsg += source;
            hr = GetLastError()|0x80070000;
            CUtils::ReportErrorMsg(hr, FALSE, strErrMsg, pWindow );
            wxLogDebug( wxT("%s"), strErrMsg.wc_str( ));
        }
	} // if (hInternetSession)
	else
	{
        wxString strErrMsg( _("Could not create HTTP session."));
		hr = GetLastError()|0x80070000;
		CUtils::ReportErrorMsg(hr, FALSE, strErrMsg, pWindow );
		wxLogDebug( wxT("%s"), strErrMsg.wc_str( ));
	}

	if( hRequest )
		netInternetCloseHandle( hRequest );

    wxLogDebug(wxT("    leaving DAVCopyFolder() %s %d"), strStatusCode.c_str(), hr);

	return hr;
}


//*****************************************************************************
int CWebDavUtils::SendRequest(const wxString& szReq,
                                   const wxString& strParamFileURL,
                                   const wxString& aUrlPath,
                                   const wxString& userName,
                                   const wxString& passwd,
                                   const unsigned char* pData,
                                   unsigned long dwLength,
                                   wxString& strStatusCode,
                                   wxString& strStatusText,
                                   bool bAddLockToken,
                                   const wxString& strLockToken,
                                   wxWindow* pWindow,
                                   int* piChunk)
{
	int    hr = E_FAIL;
	int flags =INTERNET_FLAG_RELOAD|INTERNET_FLAG_DONT_CACHE;

    wxString    strFileURL  = strParamFileURL;

#ifdef __WXMSW__
    strFileURL += aUrlPath;
#endif
    
    wxLogDebug(wxT("Entering SendRequest(%s, %s)"), szReq.c_str(), strFileURL.c_str());
    
	wxString proto = strFileURL.Left(5);
	proto.MakeUpper();
	if (proto == wxT("HTTPS"))
	{
		flags |= INTERNET_FLAG_SECURE |
				INTERNET_FLAG_IGNORE_CERT_DATE_INVALID |
				INTERNET_FLAG_IGNORE_CERT_CN_INVALID;
	}

	if (m_Connection==NULL || m_InternetSession==NULL)
	{
		OpenConnection(strFileURL, userName, passwd);
	}

    HREQUEST	hRequest   = NULL;
	if (m_InternetSession)
	{
        if (m_Connection)
        {
            hRequest = netHttpOpenRequest( m_Connection, szReq.mb_str( wxConvUTF8 ), aUrlPath.mb_str( wxConvUTF8 ), NULL, flags );
            if( hRequest )
            {
                int count = 6;
                while (count-- > 0)
                {
                    SetInternetSecurityOptions( hRequest );

                    wxString strContentType = wxT("");
                    if( aUrlPath.EndsWith( wxT("jpg.merged"))  ||  aUrlPath.EndsWith( wxT("jpg")) )
                    {
                        strContentType = wxT("Content-Type:image/jpg");
                    }
                    else if( aUrlPath.EndsWith( wxT("xml") ) )
                    {
                        strContentType = wxT("Content-Type:application/xml");
                    }
                    netHttpAddRequestHeaders( hRequest, strContentType.mb_str( wxConvUTF8 ), strContentType.Len( ) );

                    if( bAddLockToken  &&  szReq.Cmp( wxT("PUT") ) == 0  &&  !strLockToken.IsEmpty( ) )
                    {
                        wxString headers;
                        headers = wxString::Format(wxT("If: <%s> (<%s>)"), strFileURL.wc_str( ), strLockToken.wc_str( ));
                        if( !netHttpAddRequestHeaders( hRequest, headers.mb_str( wxConvUTF8 ), headers.Len( ) ) )
                        {
                            wxString errmsg( _("Error adding headers to the request: "));
                            errmsg += strFileURL;
                            errmsg += wxT("\r\n");
                            hr = GetLastError()|0x80070000;
                            CUtils::ReportErrorMsg(hr, FALSE, errmsg, pWindow );
                            wxLogDebug( wxT("%s"), errmsg.wc_str( ));
                            netInternetCloseHandle( hRequest );
                            return hr;
                        }
                    }

                    if( szReq.Cmp( wxT("PUT") )  ==  0 )
                    {
                        wxString header = wxT("Connection: close");
                        netHttpAddRequestHeaders( hRequest, header.mb_str( wxConvUTF8 ), header.Len( ) );
                    }

                    unsigned long dwPos = 0;
                    unsigned long dwOut;
                    INTERNET_BUFFERS Output;

                    // Set output initial buffer
                    memset(&Output,0,sizeof(Output));
                    Output.dwStructSize = sizeof(Output);

                    Output.dwBufferTotal = dwLength;
                    // Although this buffer is called Output,
                    // it is being passed as the input buffer parameter;
                    // therefore, we believe the data will not be
                    // modified (and therefore it is safe to remove
                    // the const modifier).
                    Output.lpvBuffer = (void *) pData;
                    Output.Next = (_INTERNET_BUFFERS*) &Output;
                    Output.dwBufferLength = 0;

                    // Initiate request
                    if( netHttpSendRequestEx( hRequest, m_InternetSession, &Output, NULL ) )
                    {
                        int count = 0;
                        while (dwPos < dwLength)
                        {
                            // Get size of next piece of data
                            dwOut = ((dwLength - dwPos) < iChunkSize) ? (dwLength - dwPos) : iChunkSize;

                            // Send next chunk of file
                            unsigned long dwBytesWritten = 0;
                            if( dwOut  >  0 )
                            {
                                if( !netInternetWriteFile( hRequest, pData + dwPos, dwOut, &dwBytesWritten ) )
                                {
                                    wxLogDebug(wxT("WriteFile failed [%d]"), GetLastError());
                                    break;
                                }

                                ::wxSleep(0);
                            }

                            dwPos += dwOut;

                            ++count;
                            if( piChunk )
                                *piChunk = count;
                        }

                        if (dwPos >= dwLength)
                            hr = S_OK;
                        else
                            continue;   // Didn't finish, retry operation

                        netHttpEndRequest( hRequest );

                        int     iLC = 0;
                        while( !netHttpQueryResultsReady( hRequest, TRUE ) )
                        {
                            CUtils::MsgWait( 20, 1 );
                            if (++iLC > 200)
                                break;
                        }
                        if (iLC > 200)
                        {
                            continue;
                        }
                        wxLogDebug( wxT("SendRequest( ) looped waiting for results %d times"), iLC );

                        char    StatusCode[MAX_PATH];
                        unsigned long dwBufferLength=MAX_PATH-1, dwIndex=0;
                        if( netHttpQueryInfo(hRequest, HTTP_QUERY_STATUS_CODE, StatusCode, &dwBufferLength, &dwIndex))
                        {
                            strStatusCode = wxString( StatusCode, wxConvUTF8 );
                        }
                        dwBufferLength=MAX_PATH-1;
                        dwIndex=0;
                        if( netHttpQueryInfo(hRequest, HTTP_QUERY_STATUS_TEXT, StatusCode, &dwBufferLength, &dwIndex))
                        {
                            strStatusText = wxString( StatusCode, wxConvUTF8 );
                        }

                    }
                    else	// if (HttpSendRequestEx())
                    {
                        wxLogDebug(wxT("SendRequest failed [%d]"), GetLastError());
                    }

                    int err = GetLastError();
                    if (err==ERROR_INTERNET_TIMEOUT)			// ERROR_INTERNET_TIMEOUT
                    {
                        continue;
                    }
                    else if (err == ERROR_INTERNET_INVALID_CA)
                    {
                        if (m_bIgnoreUnknownCA || (netInternetErrorDlg(pWindow ? pWindow->GetHandle() : NULL, hRequest, err) != ERROR_CANCELLED))
                        {
                            if (err == ERROR_INTERNET_INVALID_CA)
                                m_bIgnoreUnknownCA = true;
                            continue;
                        }
                    }
                    else if( err == ERROR_INTERNET_SECURITY_CHANNEL_ERROR )
                    {
                        if( !m_bIgnoreUnknownCA )
                        {
                            m_bIgnoreUnknownCA = true;
                        }
                        continue;
                    }
                    else if( err == ERROR_WINHTTP_SECURE_CERT_REV_FAILED )
                    {
                        if( !m_bIgnoreCertRev )
                        {
                            m_bIgnoreCertRev = true;
                        }
                        continue;
                    }

                    if (strStatusCode == wxT("401"))
                    {
                        // Target server (i.e. conferencing portal) requires authentication
                        if (SetAuthentication(hRequest, userName, passwd))
                            continue;
                    }
                    else if (strStatusCode == wxT("407"))
                    {
                        // Intermediate proxy requires authentication
                        err = netInternetErrorDlg(pWindow ? pWindow->GetHandle() : NULL, hRequest, err);
                        if (err == ERROR_INTERNET_FORCE_RETRY)
                            continue;
                    }

                    long    lStatus;
                    strStatusCode.ToLong( &lStatus );
                    if( !strStatusCode.IsEmpty( )  && (lStatus < 300  ||  lStatus == 405) )	// MKCOL again get 405 error -- ignore it
                        hr = S_OK;
                    else 
                        hr = E_FAIL;
                    break;
                } // while

                if ( hr < 0 )
                {
                    wxString errmsg( _("Unable to upload file: "));
                    errmsg += strFileURL;
                    errmsg += wxT("\r\n");
                    hr = GetLastError()|0x80070000;
                    if ( m_bShowErrors )
                    {
                        CUtils::ReportErrorMsg(hr, FALSE, errmsg, pWindow );
                    }
                    wxLogDebug( wxT("%s"), errmsg.wc_str( ));
                }
            }	// if (hRequest)
            else
            {
                wxString errmsg( _("Could not create a request handle."));
                hr = GetLastError()|0x80070000;
                CUtils::ReportErrorMsg(hr, FALSE, errmsg, pWindow );
                wxLogDebug( wxT("%s"), errmsg.wc_str( ));
            }

        } // if (hConnection)
        else
        {
            wxString errmsg( _("Could not create connection to server: "), wxConvUTF8 );

            wxString    strHost;
            wxURI       uri( strFileURL );
            if( uri.HasServer( ) )
                strHost = uri.GetServer( );

            if( uri.HasServer( ) )
                errmsg += strHost;
            else
                errmsg += strFileURL;
            errmsg += wxT("\r\n");
            hr = GetLastError()|0x80070000;
            CUtils::ReportErrorMsg(hr, FALSE, errmsg, pWindow );
            wxLogDebug( wxT("%s"), errmsg.wc_str( ));
        }
	} // if (hInternetSession)
	else
	{
        wxString errmsg( _("Could not create HTTP session: "), wxConvUTF8 );
		errmsg += wxT("\r\n");
		hr = GetLastError()|0x80070000;
		CUtils::ReportErrorMsg(hr, FALSE, errmsg, pWindow );
		wxLogDebug( wxT("%s"), errmsg.wc_str( ));
	}

	if( hRequest )
		netInternetCloseHandle( hRequest );

    CloseConnection( );

    wxLogDebug(wxT("    leaving SendRequest() %s %d"), strStatusCode.c_str(), hr);

	return hr;
}


//*****************************************************************************
int CWebDavUtils::RetrieveFile(const wxString& strParamTargetFileURL, const wxString& aUrlPath,
                                   wxString& out, const wxString& userName, const wxString& passwd,
                                   wxString& strStatusCode, wxString& strStatusText, bool aSaveToFile, wxWindow *pWindow)
{
	int    hr      = E_FAIL;
	int    flags   = INTERNET_FLAG_RELOAD|INTERNET_FLAG_DONT_CACHE;

    wxString    strTargetFileURL = strParamTargetFileURL;

#ifdef __WXMSW__
    strTargetFileURL += aUrlPath;
#endif

    wxLogDebug(wxT("Entering RetrieveFile(%s)"), strTargetFileURL.c_str());

    wxString    proto = strTargetFileURL.Left(5);
	proto.MakeUpper( );
	if( proto == wxT("HTTPS"))
	{
		flags |= INTERNET_FLAG_SECURE |
				INTERNET_FLAG_IGNORE_CERT_DATE_INVALID |
				INTERNET_FLAG_IGNORE_CERT_CN_INVALID;
	}

    wxFile      fileTemp;

    if( aSaveToFile )
	{
        wxString    strTempPath;
        strTempPath = wxFileName::GetTempDir( );
#ifdef __WXGTK__
        strTempPath.Append( wxT("/") );
#endif
        out = wxFileName::CreateTempFileName( strTempPath );

        fileTemp.Open( out, wxFile::write );
	}

	if (m_Connection==NULL || m_InternetSession==NULL)
	{
		OpenConnection(strTargetFileURL, userName, passwd);
	}

    HREQUEST	hRequest   = NULL;
    if( fileTemp.IsOpened( )  ||  !aSaveToFile )
    {
		if (m_InternetSession)
		{
            int     err = 0;
            if (m_Connection)
            {
                wxString    strCmd( wxT("GET") );
                hRequest = netHttpOpenRequest( m_Connection, strCmd.mb_str( wxConvUTF8 ), aUrlPath.mb_str( wxConvUTF8 ), NULL, flags );
                if( hRequest )
                {
                    int count = 6;
                    while (count-- > 0)
                    {
                        SetInternetSecurityOptions( hRequest );

                        if( netHttpSendRequest( hRequest, m_InternetSession, NULL, 0, NULL, 0))
                        {
                            // Loop waiting for results...no-op on win32
                            int     iLC = 0;
                            while( !netHttpQueryResultsReady( hRequest, TRUE ) )
                            {
                                CUtils::MsgWait( 20, 1 );
                                if (++iLC > 200)
                                    break;
                            }
                            if (iLC > 200)
                            {
                                continue;
                            }

                            char            StatusCode[MAX_PATH];
                            unsigned long   dwBufferLength = MAX_PATH-1;
                            unsigned long   dwIndex = 0;
                            if( netHttpQueryInfo(hRequest, HTTP_QUERY_STATUS_CODE, StatusCode, &dwBufferLength, &dwIndex))
                            {
                                strStatusCode.Empty( );
                                strStatusCode.Append( wxString( StatusCode, wxConvUTF8 ) );
                                dwBufferLength = MAX_PATH-1;
                                dwIndex = 0;
                                if( netHttpQueryInfo(hRequest, HTTP_QUERY_STATUS_TEXT, StatusCode, &dwBufferLength, &dwIndex))
                                {
                                    strStatusText.Empty( );
                                    strStatusText.Append( wxString( StatusCode, wxConvUTF8 ) );
                                }

                                unsigned long   dwRead, dwBytesWritten;
                                char szTemp[iChunkSize+1];
                                INTERNET_BUFFERSA Output;
                                memset(&Output,0,sizeof(Output));
                                Output.dwStructSize = sizeof(Output);
                                Output.lpvBuffer = szTemp;
                                Output.Next = &Output;
                                Output.dwBufferLength = iChunkSize;

                                if (strStatusCode == wxT("200"))
                                {
                                    while( netInternetReadFileExA( hRequest, &Output ) )
                                    {
                                        dwRead = Output.dwBufferLength;
                                        if( !dwRead )
                                            break;
                                        szTemp[ dwRead ] = 0;
                                        Output.dwBufferLength = iChunkSize;
                                        if( aSaveToFile )
                                        {
                                            int     iWritten;
                                            iWritten = fileTemp.Write( szTemp, dwRead );
                                            dwBytesWritten += iWritten;
                                        }
                                        else
                                            out.Append( wxString(szTemp, wxConvUTF8) );
                                    }
                                    wxLogDebug(wxT("Retrieved %s"), strTargetFileURL.wc_str( ));
                                    err = 0;
                                    hr = S_OK;
                                }
                                else if (strStatusCode==wxT("401"))
                                {
                                    // Target server (i.e. conferencing portal) requires authentication
                                    if (SetAuthentication(hRequest, userName, passwd))
                                        continue;
                                }
                                else if (strStatusCode==wxT("407"))
                                {
                                    // Intermediate proxy requires authentication
                                    err = netInternetErrorDlg(pWindow ? pWindow->GetHandle() : NULL, hRequest, err);
                                    if (err == ERROR_INTERNET_FORCE_RETRY)
                                        continue;
                                }
                                else
                                {
                                    wxLogDebug( wxT("Failed to retrieve %s, %s, Status code = %s"), strTargetFileURL.wc_str( ), strStatusText.wc_str( ), strStatusCode.wc_str( ));
                                }

                            }
                            else
                            {
                                err = GetLastError();
                                wxLogDebug(wxT("netHttpQueryInfo failed, status = %d"), err);
                            }
                        }
                        else
                        {
                            err = GetLastError();
                            wxLogDebug(wxT("netHttpSendRequest failed, status = %d"), err);
                        }

                        // Check for errors we rety on
                        if (err == ERROR_INTERNET_TIMEOUT 
#ifdef __WXMSW__
                           || err == WSAECONNABORTED
#endif
                           )
                        {
                            continue;
                        }
                        else if (err == ERROR_INTERNET_INVALID_CA)
                        {
                            if (m_bIgnoreUnknownCA || (netInternetErrorDlg(pWindow ? pWindow->GetHandle() : NULL, hRequest, err) != ERROR_CANCELLED))
                            {
                                if (err == ERROR_INTERNET_INVALID_CA)
                                    m_bIgnoreUnknownCA = true;
                                continue;
                            }
#ifndef __WXMSW__
                            const char *szDetails = netGetServerCertificate(hRequest);
                            m_strLastCert = wxString(szDetails, wxConvUTF8);
#endif
                        }
                        else if( err == ERROR_INTERNET_SECURITY_CHANNEL_ERROR )
                        {
                            if( !m_bIgnoreUnknownCA )
                            {
                                m_bIgnoreUnknownCA = true;
                            }
                            continue;
                        }
                        else if( err == ERROR_WINHTTP_SECURE_CERT_REV_FAILED )
                        {
                            if( !m_bIgnoreCertRev )
                            {
                                m_bIgnoreCertRev = true;
                            }
                            continue;
                        }
                        break;
                    }	// while
                }
                else
                {
                    err = GetLastError();
                    wxLogDebug(wxT("netHttpOpenRequest failed, status = %d"), err);
                }
            }

            if (err != 0)
            {
                hr = err|0x80070000;
                unsigned long dwError = hr, dwLength;
                char szBuf[MAX_PATH];
                dwLength = MAX_PATH;
                wxString msg( _("Unable to download file."), wxConvUTF8 );
                if (m_bShowErrors && err != ERROR_INTERNET_INVALID_CA)
                {
                    CUtils::ReportErrorMsg(dwError, FALSE, msg, pWindow);
                }
                wxLogDebug( wxT("Failed to open %s, %s, error code = %d"), strTargetFileURL.wc_str( ), msg.wc_str( ), dwError);
                if( netInternetGetLastResponseInfo( &dwError, szBuf, &dwLength )  &&  dwLength > 0 )
                {
                    strStatusText = wxString( szBuf, wxConvUTF8 );
                    wxLogDebug( wxT("%s"), szBuf);
                }
            }
		}

        if( fileTemp.IsOpened( ) )
            fileTemp.Close( );
	}

    if( hRequest )
        netInternetCloseHandle( hRequest );

    CloseConnection( );

    wxLogDebug(wxT("    leaving RetrieveFile() %s %d"), strStatusCode.c_str(), hr);

    return hr;
}

bool CWebDavUtils::HandleCertError(wxWindow *parent)
{
    wxString details = m_strLastCert;

    int         iStart, iEnd;
    wxString    strStart( wxT("<certificate>") );
    wxString    strEnd( wxT("</certificate>") );
    wxString    strCert;
    iStart = details.Find( strStart );
    iEnd = details.Find( strEnd );
    if( iStart != wxNOT_FOUND  &&  iEnd != wxNOT_FOUND )
    {
        iStart += strStart.Len( );
        strCert = details.Mid( iStart, iEnd - iStart );
    }

    details.Replace( wxT("\n"), wxT("") );

    char    *pBuf   = NULL;
    pBuf = (char *) malloc( details.Len( ) + 1 );
    strcpy( pBuf, details.mb_str( wxConvUTF8 ) );

    int                 iRet;
    wxMemoryInputStream iStream( pBuf, details.Len( ) );
    wxXmlDocument       doc( iStream );
    wxXmlNode           *pRoot;
    if( doc.IsOk( ) )
    {
        pRoot = doc.GetRoot( );
        if( pRoot )
        {
            BadAuthCertDialog   dlg( parent, pRoot );
            iRet = dlg.ShowModal( );

            free(pBuf);

            if( iRet == wxID_OK )
            {
                if (dlg.isRadioAccept())
                {
                    CertHandler::GetHandler().AddCertToFile(strCert, 1);
                    CertHandler::GetHandler().WriteCerts();
                    return true;
                }
                else if (dlg.isRadioTemp())
                {
                    CertHandler::GetHandler().AddTempCert(strCert);
                    CertHandler::GetHandler().WriteCerts();
                    return true;
                }
            }
        }
    }
    return false;
}
