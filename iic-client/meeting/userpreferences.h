/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef _USERPREFERENCES_H_
#define _USERPREFERENCES_H_

#include <wx/dynarray.h>

/*******************************************************************************************
 Procedure for adding a new UserPreference value:

    1) Add the new value to the UserPreferences class
        a) Add the member variable.
        b) Initialize in the constructor.
        b) Update the assignment operator.
        c) Add code to the Load() and Save() methods to read/write the variable.


    2) Add new GUI code to UserPreferencesDialog. (userpreferencesdialog.h/.cpp)
       (Optional - If the pref value is displayed on the preferences screen)
        a) Add the UI widgets.
        b) Add code to Value_to_GUI() if required.
        c) Add code to GUI_to_Value() if required.
********************************************************************************************/

class UserPreferences
{
public:
    UserPreferences();

    /** Set default font for application */
    void SetDefaultFont(wxFont &defFont);

    /** Load user preferences from the options store */
    void Load();

    /** Save user preferences to the options store */
    void Save();

    UserPreferences & operator=( const UserPreferences & rhs );

    // PLF-TODO: Add accessors for UserPreferences.

public:
    /////// General Panel /////////////////////////////////////////////////////////////////////////
    // NOTE: The next three fields are also defined in SignOnParams
    wxString m_strPassword;         /**< The user's password */
    bool m_fSavePassword;           /**< Flag: Save password (Required if Auto sign on is used) */
    bool m_fAutoSignOn;             /**< Flag: Auto sign on  (Requires Save password) */
    bool m_fKeepTryingConnection;   /**< Flag: Keep trying the connection until successful */
    bool m_fStartOnComputerStart;   /**< Flag: Auto start the product on system start */
    bool m_fIMClient;               /**< Flag: Run as IM client (as well as meeting client) */

    bool m_fDownloadPublicMeetings; /**< Flag: Download public meetings on startup */
    bool m_fDownloadCAB;            /**< Flag: Download CAB on startup */
    bool m_fCacheAB;            	/**< Flag: Cache contacts downloaded from address books */
    bool m_fDownloadOutlook;        /**< Flag: Download Outlook contacts */
    bool m_fShowInstantMeeting;     /**< Flag: Show instant meeting in meeting center */
    bool m_fEnablePrivateMeetings;  /**< Flag: Allow private meeting privacy setting */

    ////// Contact Display Panel //////////////////////////////////////////////////////////////////
    /** The contact column index values (See ContactDisplayManager (ContactTreeList.h) for Info.) */
    wxArrayInt m_DisplayedContactColumns;

    ////// Font Panel /////////////////////////////////////////////////////////////////////////////
    static wxFont s_AppDefFont;            /**< Default application font */
    wxFont m_AppFont;               /**< The application font */
    wxColour m_ChatBG;              /**< The chat panel background color */
    wxColour m_ChatFG;              /**< The chat panel foreground color */
    int      m_nChatFontSize;       /**< The relative chat font size */
    bool     m_fChatBold;           /**< Bold chat font */
    bool     m_fChatItalic;         /**< Italic chat font */
    bool     m_fChatUnderline;      /**< Underline chat font */

    ///// Sounds panel ////////////////////////////////////////////////////////////////////////
    bool m_fMuteAllSounds;
	int m_nChatSounds;
	bool m_fPlayChatInvtSound;
	int m_nInMeetingSounds;
	bool m_fDisableInMeetingSounds;

    ///// Data share panel ////////////////////////////////////////////////////////////////////
    bool m_fHidePresenterMeetingWindow;
    bool m_fAutoDetectSlowConnection;
    bool m_fReduceColor;
    bool m_fUsePowerPoint;
    bool m_fUseDirectX;

    ///// PC phone panel //////////////////////////////////////////////////////////////////////
    int m_nPCPhoneBandwidth;        /**< 0 == Auto, 1 == Low, 2 == Med, 3 = High  */
    int m_nPCPhoneVAD;              /**< 0 == Low, 1 == Med, 2 == High */
	bool m_fEnableEchoCancellation;
	bool m_fEnableNoiseFilter;
    int m_nAudioSubsystem;          /**< Voip_Audio_Internal_PA = 0, Voip_Audio_Internal_Alsa = 3, From VoipApi.h and iaxclient.h */
    int m_nMaxJitter;
    int m_nSuppressionThreshold;    /**< Input is suppressed unless this many DBs higher then output (expressed in 1/100s) */
    int m_nSilenceThreshold;        /**< Silence threshold, 1 = use Speex VAD */

    ///// Internal communications //////////////////////////////////////////////////////////////////////
    int         m_nVersion;
    wxString    m_strLastJID;
};

#endif
