/////////////////////////////////////////////////////////////////////////////
// Name:        wxTogBmpBtn based on wxBitmapButton
// Purpose:     a toggling bitmap button based on wxBitmapButton
// Author:      Jim Park
// Modified by:
// Created:     2007.01.16
// RCS-ID:
// Copyright:
// Licence:
/////////////////////////////////////////////////////////////////////////////

#if defined(__GNUG__) && !defined(NO_GCC_PRAGMA)
    #pragma implementation "wxTogBmpBtn.h"
#endif

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

#ifndef WX_PRECOMP
    #include <wx/bmpbuttn.h>
#endif // WX_PRECOMP

#include "wx/wxTogBmpBtn.h"

#include <wx/file.h>

// ==========================================================================
// wxTogBmpBtn
// ==========================================================================

#if 0
wxBEGIN_PROPERTIES_TABLE(wxTogBmpBtn)
    wxEVENT_PROPERTY( Click , wxEVT_COMMAND_BUTTON_CLICKED , wxCommandEvent)
wxEND_PROPERTIES_TABLE()

wxBEGIN_HANDLERS_TABLE(wxTogBmpBtn)
wxEND_HANDLERS_TABLE()
#endif

IMPLEMENT_DYNAMIC_CLASS( wxTogBmpBtn, wxBitmapButton )

BEGIN_EVENT_TABLE(wxTogBmpBtn,wxBitmapButton)
    EVT_BUTTON(wxID_ANY,wxTogBmpBtn::OnClick)
END_EVENT_TABLE()




//********************************************************************
wxTogBmpBtn::wxTogBmpBtn(wxWindow *parent,
                         wxWindowID id,
                         const wxString& strBmpBaseName,
                         const wxPoint& pos,
                         const wxSize& size,
                         long style,
                         const wxValidator& validator,
                         const wxString& name)
            : wxBitmapButton( parent, id, wxNullBitmap, pos, size, style, validator, name )
{
    Create( parent, id, strBmpBaseName, pos, size, style, validator, name );
}


//********************************************************************
bool wxTogBmpBtn::Create(wxWindow *parent,
                         wxWindowID id,
                         const wxString& strBmpBaseName,
                         const wxPoint& pos,
                         const wxSize& size,
                         long style,
                         const wxValidator& validator,
                         const wxString& name)
{
    wxBitmapButton::Create( parent, id, wxNullBitmap, pos, size, style, validator, name );

    m_fTogglable = true;
    m_fIsDown = false;

    wxMask      *pMask;
    wxString    strT;

    //  The "U" version of the bitmap MUST exist...
    strT = strBmpBaseName + _T("U.bmp");
    wxBitmap bmpU( strT, wxBITMAP_TYPE_BMP );
    if( !bmpU.IsOk() )
        wxLogDebug( _T("In wxTogBmpBtn constructor:  bmpU is BAD!!!  strBmpBaseName is *%s*\n\r"), strBmpBaseName.c_str( ) );

    pMask = new wxMask( bmpU, gTransColour );
    bmpU.SetMask( pMask );
    SetBitmapLabel( bmpU );

    strT = strBmpBaseName + _T("F.bmp");
    if( wxFile::Exists( strT ) )
    {
        wxBitmap bmpF( strT, wxBITMAP_TYPE_BMP );
        if( bmpF.IsOk() )
        {
            pMask = new wxMask( bmpF, gTransColour );
            bmpF.SetMask( pMask );
            SetBitmapFocus( bmpF );
        }
        else
            SetBitmapFocus( bmpU );
    }

    strT = strBmpBaseName + _T("H.bmp");
    if( wxFile::Exists( strT ) )
    {
        wxBitmap bmpH( strT, wxBITMAP_TYPE_BMP );
        if( bmpH.IsOk() )
        {
            pMask = new wxMask( bmpH, gTransColour );
            bmpH.SetMask( pMask );
            SetBitmapHover( bmpH );
        }
        else
            SetBitmapHover( bmpU );
    }

    strT = strBmpBaseName + _T("D.bmp");
    if( wxFile::Exists( strT ) )
    {
        wxBitmap bmpD( strT, wxBITMAP_TYPE_BMP );
        if( bmpD.IsOk() )
        {
            pMask = new wxMask( bmpD, gTransColour );
            bmpD.SetMask( pMask );
            SetBitmapSelected( bmpD );
        }
        else
            SetBitmapSelected( bmpU );
    }

    strT = strBmpBaseName + _T("X.bmp");
    if( wxFile::Exists( strT ) )
    {
        wxBitmap bmpX( strT, wxBITMAP_TYPE_BMP );
        if( bmpX.IsOk() )
        {
            pMask = new wxMask( bmpX, gTransColour );
            bmpX.SetMask( pMask );
            SetBitmapDisabled( bmpX );
        }
        else
            SetBitmapDisabled( bmpU );
    }

    return true;
}


//********************************************************************
void wxTogBmpBtn :: OnClick( wxCommandEvent& event )
{
    ExecuteClick( );

    event.Skip( );
}


//********************************************************************
void wxTogBmpBtn :: ExecuteClick( )
{
    if( m_fTogglable )
    {
        wxBitmap    bmpT = GetBitmapSelected( );
        SetBitmapSelected( GetBitmapLabel( ) );
        SetBitmapLabel( bmpT );
        m_fIsDown = !m_fIsDown;
    }
}
