/////////////////////////////////////////////////////////////////////////////
// Name:        wxTextCtrlEx based on wxTextCtrl
// Purpose:     extensions to wxTextCtrl
// Author:      Jim Park
// Modified by:
// Created:     2007.01.18
// RCS-ID:
// Copyright:
// Licence:
/////////////////////////////////////////////////////////////////////////////

#if defined(__GNUG__) && !defined(NO_GCC_PRAGMA)
#pragma implementation "wxTextCtrlEx.h"
#endif

// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
#pragma hdrstop
#endif

#ifndef WX_PRECOMP
#include <wx/textctrl.h>
#endif // WX_PRECOMP

#include "../app.h"
#include "wx/wxTextCtrlEx.h"


//********************************************************************

static int nFontNormal      = 1;
static int nFontMin         = 0;
static int nFontMax         = 6;
static int nFontSizes[ ]    = { 8, 10, 12, 14, 18, 24, 38 };

const static int wxTCE_BOLD         = 1;
const static int wxTCE_ITALIC       = 2;
const static int wxTCE_UNDERLINE    = 3;

//********************************************************************


// ==========================================================================
// wxTextCtrlEx
// ==========================================================================


DEFINE_EVENT_TYPE(wxEVT_MY_TEXTCTRLEX_ONCHAR)


IMPLEMENT_DYNAMIC_CLASS( wxTextCtrlEx, wxTextCtrl )

BEGIN_EVENT_TABLE(wxTextCtrlEx,wxTextCtrl)
    EVT_CHAR(wxTextCtrlEx::OnChar)
    EVT_LEFT_UP(wxTextCtrlEx::OnLeftUp)
END_EVENT_TABLE()



//********************************************************************
static int wxPointsToTruePoints( int iPoints )
{
#ifdef __WINDOWS__
    return( (int) (iPoints / 15) );
#else
    return iPoints;
#endif
}


//********************************************************************
bool wxTextCtrlEx::SetStyleU( long lStart, long lEnd, const wxTextAttr &style )
{
#ifndef __WXMSW__
    if( lStart == lEnd  &&  lStart == (long) m_vecAttr.size( ) )
    {
        m_vecAttr.push_back( style );
        wxLogDebug( wxT("entering wxTextCtrlEx::SetStyleU( %d, %d )  --  appending"), lStart, lEnd );
        return true;
    }
    else
    {
        m_vecAttr.insert( m_vecAttr.begin( ) + lStart, style );
        wxLogDebug( wxT("entering wxTextCtrlEx::SetStyleU( %d, %d )  --  inserting"), lStart, lEnd );
        return true;
    }
#else
    return true;
#endif
}


//********************************************************************
bool wxTextCtrlEx::GetStyleU( long lPos, wxTextAttr &style )
{
#ifdef __WXMSW__
    return GetStyle( lPos, style );
#else
    if( lPos >= 0  &&  lPos < (long) m_vecAttr.size( ) )
    {
        style = m_vecAttr.at( lPos );
        return true;
    }

    return false;
#endif
}


//********************************************************************
wxTextCtrlEx::wxTextCtrlEx(wxWindow *parent,
                       wxWindowID id,
                       const wxString& value,
                       const wxPoint& pos,
                       const wxSize& size,
                       long style,
                       const wxValidator& validator,
                       const wxString& name)
: wxTextCtrl( parent, id, value, pos, size, style, validator, name )
{
    Create( parent, id, value, pos, size, style, validator, name );
}


//********************************************************************
bool wxTextCtrlEx::Create(wxWindow *parent,
                        wxWindowID id,
                        const wxString& value,
                        const wxPoint& pos,
                        const wxSize& size,
                        long style,
                        const wxValidator& validator,
                        const wxString& name)
{
    m_vecAttr.clear( );
    m_LineCount = 0;
    cb_KeyPressed = NULL;
    m_bIgnoreOnCharEntry = false;
    m_iPriorInsertPoint = -1;

    m_FontDefault = wxGetApp( ).GetAppDefFont( );

    m_TextAttrDefault = GetDefaultStyle( );
    m_TextAttrDefault.SetFont( m_FontDefault );
    m_TextAttrDefault.SetFlags( wxTEXT_ATTR_FONT );

    //SetDefaultStyle( m_TextAttrDefault ); // Moved to after create see below.
    
    bool fOK = wxTextCtrl::Create( parent, id, value, pos, size, style, validator, name );
    if( fOK )
    {
      // NOTE: The call to SetDefaultStyle was moved to after the call to create to get around
      // a crash on Mac OS X
      SetDefaultStyle( m_TextAttrDefault );
    }

    return fOK;
}


//********************************************************************
bool wxTextCtrlEx::SetSelectionTextAttr( wxTextAttr &ta)
{
    long    lFrom, lTo;
    GetSelection( &lFrom, &lTo );
    return SetStyle( lFrom, lTo, ta );
}


//********************************************************************
bool wxTextCtrlEx::GetSelectionTextAttr( wxTextAttr &ta)
{
    return GetStyleU( GetInsertionPoint( ), ta );
}


//********************************************************************
void wxTextCtrlEx::GetBackground( wxColour &wColour )
{
    wxTextAttr  tAttr;
    if( GetSelectionTextAttr( tAttr ) )
    {
        wColour = tAttr.GetBackgroundColour( );
    }
    wColour = tAttr.GetBackgroundColour( );
}


//********************************************************************
void wxTextCtrlEx::GetForeground( wxColour &wColour )
{
    wxTextAttr  tAttr;
    if( GetSelectionTextAttr( tAttr ) )
    {
        wColour = tAttr.GetTextColour( );
    }
    wColour = tAttr.GetTextColour( );
}


//********************************************************************
void wxTextCtrlEx::InitializeForChat(const wxString &aDefaultFontFace,
                                     int aDefaultFontSize,
                                     const wxColour &aDefaultFontForeground,
                                     const wxColour &aDefaultFontBackground,
                                     bool aIsBold,
                                     bool aIsItalic,
                                     bool aIsUnderline)
{
    // Set up our default character format
    m_FontSize = aDefaultFontSize - 1;

    m_FontDefault.SetFaceName( aDefaultFontFace );
    m_FontDefault.SetPointSize( nFontSizes[ m_FontSize ] );
    m_FontDefault.SetWeight( (aIsBold) ? wxFONTWEIGHT_BOLD : wxFONTWEIGHT_NORMAL );
    m_FontDefault.SetStyle( (aIsItalic) ? wxFONTSTYLE_ITALIC : wxFONTSTYLE_NORMAL );
    m_FontDefault.SetUnderlined( aIsUnderline );

    m_TextAttrDefault.SetFlags( wxTEXT_ATTR_FONT | wxTEXT_ATTR_TEXT_COLOUR | wxTEXT_ATTR_BACKGROUND_COLOUR );
    m_TextAttrDefault.SetFont( m_FontDefault );
    m_TextAttrDefault.SetBackgroundColour( aDefaultFontBackground );
    m_TextAttrDefault.SetTextColour( aDefaultFontForeground );
    SetDefaultStyle( m_TextAttrDefault );

    Clear( );
    m_iPriorInsertPoint = GetInsertionPoint( );
}


//********************************************************************
void wxTextCtrlEx::ResetReceiveFont()
{
    // TODO: these should come from user preferences
    m_ReceiveFaceName     = _("Tahoma");
    m_ReceiveFontSize     = 2;
    m_bReceiveIsBold      = false;
    m_bReceiveIsItalic    = false;
    m_bReceiveIsUnderline = false;
    m_ReceiveForeground.Set(   0,   0,   0 );
    m_ReceiveBackground.Set( 255, 255, 255 );
}


//********************************************************************
void wxTextCtrlEx::FormatBold( bool aBold )
{
    long    lFrom, lTo;
    GetSelection( &lFrom, &lTo );

    //  First, set the Default style...
    wxFontWeight    weightTarget;
    wxRichTextAttr  tAttr;
    bool            bRet;

    weightTarget = ((aBold) ? wxFONTWEIGHT_BOLD : wxFONTWEIGHT_NORMAL);
    tAttr = GetDefaultStyle( );
    tAttr.SetFontWeight( weightTarget );
    bRet = SetDefaultStyle( tAttr );

    m_FontDefault.SetWeight( weightTarget );

    //  Now, if any text is selected set its bold attribute correctly
    wxFont  wFont;
    if( lFrom  !=  lTo )
    {
        wxTextAttr  tAttr;
        for( int i=lFrom; i<lTo; i++ )
        {
            if( GetStyleU( i, tAttr ) )
            {
                wFont = tAttr.GetFont( );
                if( weightTarget  !=  wFont.GetWeight( ) )
                {
                    wFont.SetWeight( weightTarget );
                    wFont.SetPointSize( wxPointsToTruePoints( wFont.GetPointSize( ) ) );   // avoid the wxWidgets bug!!
                    tAttr.SetFlags( wxTEXT_ATTR_FONT_WEIGHT | wxTEXT_ATTR_FONT_SIZE );
                    tAttr.SetFont( wFont );
                    SetStyle( i, i+1, tAttr );
                }
            }
        }
    }
}


//********************************************************************
void wxTextCtrlEx::FormatItalic( bool aItalic )
{
    long    lFrom, lTo;
    GetSelection( &lFrom, &lTo );

    wxRichTextAttr  tAttr;
    bool            bRet;
    int             italicTarget = ((aItalic) ? wxFONTSTYLE_ITALIC : wxFONTSTYLE_NORMAL);

    tAttr = GetDefaultStyle( );
    tAttr.SetFontStyle( italicTarget );
    bRet = SetDefaultStyle( tAttr );

    m_FontDefault.SetStyle( italicTarget );

    //  Now, if any text is selected set its italic attribute correctly
    wxFont  wFont;
    if( lFrom  !=  lTo )
    {
        wxTextAttr  tAttr;
        for( int i=lFrom; i<lTo; i++ )
        {
            if( GetStyleU( i, tAttr ) )
            {
                wFont = tAttr.GetFont( );
                if( italicTarget  !=  wFont.GetStyle( ) )
                {
                    wFont.SetStyle( italicTarget );
                    wFont.SetPointSize( wxPointsToTruePoints( wFont.GetPointSize( ) ) );   // avoid the wxWidgets bug!!
                    tAttr.SetFlags( wxTEXT_ATTR_FONT_ITALIC | wxTEXT_ATTR_FONT_SIZE );
                    tAttr.SetFont( wFont );
                    SetStyle( i, i+1, tAttr );
                }
            }
        }
    }
}


//********************************************************************
void wxTextCtrlEx::FormatUnderline( bool aUnderline )
{
    long    lFrom, lTo;
    GetSelection( &lFrom, &lTo );

    wxRichTextAttr  tAttr;
    bool            bRet;

    tAttr = GetDefaultStyle( );
    tAttr.SetFontUnderlined( aUnderline );
    bRet = SetDefaultStyle( tAttr );

    m_FontDefault.SetUnderlined( aUnderline );

    //  Now, if any text is selected set its underlined attribute correctly
    wxFont  wFont;
    if( lFrom  !=  lTo )
    {
        wxTextAttr  tAttr;
        for( int i=lFrom; i<lTo; i++ )
        {
            if( GetStyleU( i, tAttr ) )
            {
                wFont = tAttr.GetFont( );
                if( aUnderline  !=  wFont.GetUnderlined( ) )
                {
                    wFont.SetUnderlined( aUnderline );
                    wFont.SetPointSize( wxPointsToTruePoints( wFont.GetPointSize( ) ) );   // avoid the wxWidgets bug!!
                    tAttr.SetFlags( wxTEXT_ATTR_FONT_UNDERLINE | wxTEXT_ATTR_FONT_SIZE );
                    tAttr.SetFont( wFont );
                    SetStyle( i, i+1, tAttr );
                }
            }
        }
    }
}


//********************************************************************
void wxTextCtrlEx::FormatFontSize(int aFontSize)
{
    if (aFontSize >= nFontMin && aFontSize <= nFontMax)
    {
        m_FontSize = aFontSize;

        long    lFrom, lTo;
        GetSelection( &lFrom, &lTo );

        m_FontDefault.SetPointSize( nFontSizes[ m_FontSize ] );
        m_TextAttrDefault.SetFlags( wxTEXT_ATTR_FONT );
        m_TextAttrDefault.SetFont( m_FontDefault );
        SetDefaultStyle( m_TextAttrDefault );

        //  Now, if any text is selected set its size
        wxFont  wFont;
        if( lFrom  !=  lTo )
        {
            wxTextAttr  tAttr;
            for( int i=lFrom; i<lTo; i++ )
            {
                if( GetStyleU( i, tAttr ) )
                {
                    wFont = tAttr.GetFont( );
                    if( nFontSizes[ m_FontSize ]  !=  GetFontSizeFromwxTwips( wFont.GetPointSize( ) ) )
                    {
                        wFont.SetPointSize( nFontSizes[ m_FontSize ] );
                        tAttr.SetFlags( wxTEXT_ATTR_FONT_SIZE );
                        tAttr.SetFont( wFont );
                        SetStyle( i, i+1, tAttr );
                    }
                }
            }
        }
    }
}


//********************************************************************
int wxTextCtrlEx::FormatFontSizeNormal()
{
    long    lFrom, lTo;
    GetSelection( &lFrom, &lTo );

    m_FontSize = nFontNormal;

    m_FontDefault.SetPointSize( nFontSizes[ m_FontSize ] );
    m_TextAttrDefault.SetFlags( wxTEXT_ATTR_FONT );
    m_TextAttrDefault.SetFont( m_FontDefault );
    SetDefaultStyle( m_TextAttrDefault );

    //  Now, if any text is selected set its size
    wxFont  wFont;
    if( lFrom  !=  lTo )
    {
        wxTextAttr  tAttr;
        for( int i=lFrom; i<lTo; i++ )
        {
            if( GetStyleU( i, tAttr ) )
            {
                wFont = tAttr.GetFont( );
                if( nFontSizes[ m_FontSize ]  !=  GetFontSizeFromwxTwips( wFont.GetPointSize( ) ) )
                {
                    wFont.SetPointSize( nFontSizes[ m_FontSize ] );
                    tAttr.SetFlags( wxTEXT_ATTR_FONT_SIZE );
                    tAttr.SetFont( wFont );
                    SetStyle( i, i+1, tAttr );
                }
            }
        }
    }
    return m_FontSize;
}


//********************************************************************
int wxTextCtrlEx::FormatFontSizeIncrease()
{
    if( m_FontSize < nFontMax )
    {
        long    lFrom, lTo;
        GetSelection( &lFrom, &lTo );

        m_FontSize++;

        m_FontDefault.SetPointSize( nFontSizes[ m_FontSize ] );
        m_TextAttrDefault.SetFlags( wxTEXT_ATTR_FONT );
        m_TextAttrDefault.SetFont( m_FontDefault );
        SetDefaultStyle( m_TextAttrDefault );

        //  Now, if any text is selected set its size
        wxFont  wFont;
        if( lFrom  !=  lTo )
        {
            wxTextAttr  tAttr;
            for( int i=lFrom; i<lTo; i++ )
            {
                if( GetStyleU( i, tAttr ) )
                {
                    wFont = tAttr.GetFont( );
                    if( nFontSizes[ m_FontSize ]  !=  GetFontSizeFromwxTwips( wFont.GetPointSize( ) ) )
                    {
                        wFont.SetPointSize( nFontSizes[ m_FontSize ] );
                        tAttr.SetFlags( wxTEXT_ATTR_FONT_SIZE );
                        tAttr.SetFont( wFont );
                        SetStyle( i, i+1, tAttr );
                    }
                }
            }
        }
    }
    return m_FontSize;
}


//********************************************************************
int wxTextCtrlEx::FormatFontSizeDecrease()
{
    if (m_FontSize > nFontMin)
    {
        long    lFrom, lTo;
        GetSelection( &lFrom, &lTo );

        m_FontSize--;
        m_FontDefault.SetPointSize( nFontSizes[ m_FontSize ] );
        m_TextAttrDefault.SetFlags( wxTEXT_ATTR_FONT );
        m_TextAttrDefault.SetFont( m_FontDefault );
        SetDefaultStyle( m_TextAttrDefault );

        //  Now, if any text is selected set its size
        wxFont  wFont;
        if( lFrom  !=  lTo )
        {
            wxTextAttr  tAttr;
            for( int i=lFrom; i<lTo; i++ )
            {
                if( GetStyleU( i, tAttr ) )
                {
                    wFont = tAttr.GetFont( );
                    if( nFontSizes[ m_FontSize ]  !=  GetFontSizeFromwxTwips( wFont.GetPointSize( ) ) )
                    {
                        wFont.SetPointSize( nFontSizes[ m_FontSize ] );
                        tAttr.SetFlags( wxTEXT_ATTR_FONT_SIZE );
                        tAttr.SetFont( wFont );
                        SetStyle( i, i+1, tAttr );
                    }
                }
            }
        }
    }
    return m_FontSize;
}


//********************************************************************
void wxTextCtrlEx::FormatForegroundColor(wxColour &aColour)
{
    long    lFrom, lTo;
    GetSelection( &lFrom, &lTo );

    m_TextAttrDefault.SetTextColour( aColour );
    m_TextAttrDefault.SetFlags( wxTEXT_ATTR_TEXT_COLOUR );
    SetDefaultStyle( m_TextAttrDefault );

    //  Now, if any text is selected set its Text Colour
    if( lFrom  !=  lTo )
    {
        wxTextAttr  tAttr;
        for( int i=lFrom; i<lTo; i++ )
        {
            if( GetStyleU( i, tAttr ) )
            {
                if( aColour  !=  tAttr.GetTextColour( ) )
                {
                    tAttr.SetTextColour( aColour );
                    tAttr.SetFlags( wxTEXT_ATTR_TEXT_COLOUR );
                    SetStyle( i, i+1, tAttr );
                }
            }
        }
    }
}


//********************************************************************
void wxTextCtrlEx::FormatBackgroundColor(wxColour &aColour)
{
    long    lFrom, lTo;
    GetSelection( &lFrom, &lTo );

    m_TextAttrDefault.SetBackgroundColour( aColour );
    m_TextAttrDefault.SetFlags( wxTEXT_ATTR_BACKGROUND_COLOUR );
    SetDefaultStyle( m_TextAttrDefault );

    //  Now, if any text is selected set its Background Colour
    if( lFrom  !=  lTo )
    {
        wxTextAttr  tAttr;
        for( int i=lFrom; i<lTo; i++ )
        {
            if( GetStyleU( i, tAttr ) )
            {
                if( aColour  !=  tAttr.GetBackgroundColour( ) )
                {
                    tAttr.SetBackgroundColour( aColour );
                    tAttr.SetFlags( wxTEXT_ATTR_BACKGROUND_COLOUR );
                    SetStyle( i, i+1, tAttr );
                }
            }
        }
    }
}


//********************************************************************
void wxTextCtrlEx::AddNewLine()
{
    SetInsertionPointEnd( );
    WriteText( _T("\n") );
}


//********************************************************************
void wxTextCtrlEx::OnChar( wxKeyEvent &event )
{
    //  Check m_bIgnoreOnCharEntry flag
    if( m_bIgnoreOnCharEntry )
        return;

    //  Check for SHIFT-Enter; which embeds a Carriage Return into the text being entered
    if( (WXK_RETURN == event.GetKeyCode( ))  &&  (wxMOD_SHIFT == event.GetModifiers( )) )
    {
        //  Intentionally eat this character
        return;
    }

    //  This event keeps the state (visual display) of the formatting buttons current in the ChatPanel
    wxCommandEvent eventCustom(wxEVT_MY_TEXTCTRLEX_ONCHAR);
    wxPostEvent(this, eventCustom);

    //  Check for SHIFT-LeftArrow: used when selecting text via the keyboard
    if( ((WXK_LEFT == event.GetKeyCode( ))  &&  (wxMOD_SHIFT == event.GetModifiers( )))        ||
        ((WXK_NUMPAD_LEFT == event.GetKeyCode( ))  &&  (wxMOD_SHIFT == event.GetModifiers( )))    )
    {
        //  Avoid the wxWidgets bug!  Handle the selection ourselves.
        long    lFrom, lTo;
        GetSelection( &lFrom, &lTo );
        SetSelection( lFrom-1, lTo );
        return;
    }

    int     iKey;
    iKey = event.GetKeyCode( );

    if(( iKey < WXK_START  ||  iKey > WXK_COMMAND )  &&
       (iKey != WXK_BACK && iKey != WXK_TAB && iKey != WXK_RETURN && iKey != WXK_ESCAPE && iKey != WXK_DELETE ) )
    {
        wxString    strT( (wxChar)iKey );
        int         iLen        = GetLastPosition( );
        int         iPt         = GetInsertionPoint( );
        SetStyleU( iPt, iPt, GetDefaultStyle( ) );
        WriteText( strT );
        if( iLen  ==  GetLastPosition( ) )
        {
            //  Control is in overwrite mode - adjust our internal TextAttr vector accordingly...
            m_vecAttr.erase( m_vecAttr.begin( ) + GetInsertionPoint( ) );
        }
    }
    else
        event.Skip( );
}


//********************************************************************
void wxTextCtrlEx::OnLeftUp( wxMouseEvent &event )
{
    long    lFrom, lTo;
    GetSelection( &lFrom, &lTo );
    wxCommandEvent eventCustom( wxEVT_MY_TEXTCTRLEX_ONCHAR );
    wxPostEvent( this, eventCustom );

    event.Skip();
}


//********************************************************************
//
//  Encoding Text functions
//
//********************************************************************
void wxTextCtrlEx::GetTextAsMarkup( wxString &aString )
{
    wxString    strSelText;
    bool        fGotAttr;
    wxTextAttr  tAttr;

    wxColour    sendForeground(  0,   0,   0);
    wxColour    sendBackground(255, 255, 255);
    bool        isBold          = false;
    bool        isItalic        = false;
    bool        isUnderline     = false;
    int         sendFontSize    = 2;
    int         size            = this->GetLastPosition( );

    EncodeMarkupInitialFont( aString );

    for( int position=0; position < size; position++ )
    {
        strSelText = GetRange( position, position+1 );
        fGotAttr = GetStyleU( position, tAttr );

        // String will be empty if we are not in a valid region.
        // Do not process font if this is the case, since the
        // TextAttribute information will be invalid.
        if( fGotAttr  &&  !strSelText.IsEmpty( ) )
        {
            EncodeMarkupFont(aString, tAttr, sendFontSize, sendForeground, sendBackground);
            EncodeMarkupState(aString, isBold, tAttr, wxTCE_BOLD, wxT("B"));
            EncodeMarkupState(aString, isItalic, tAttr, wxTCE_ITALIC, wxT("I"));
            EncodeMarkupState(aString, isUnderline, tAttr, wxTCE_UNDERLINE, wxT("U"));
        }

        if (strSelText == wxT("\r"))
        {
            aString += wxT("<BR>");
        }
        else if (strSelText != wxT("\n"))
        {
            aString += strSelText;
        }
    }

    // Finally, close up the initial font tag
    aString += wxT("</FONT>");
}


//********************************************************************
// EncodeMarkupInitialFont
//
// Handles the initial font tag
//
//********************************************************************
void wxTextCtrlEx::EncodeMarkupInitialFont(wxString &aString)
{
    // TODO: initial font setting should come from user preferences

    aString += wxT("<FONT FACE=\"");
    aString += m_ReceiveFaceName;
    aString += wxT("\" LANG=\"0\" SIZE=2>");
}


//********************************************************************
// EncodeMarkupFont
//
// Handles changes in font size, foreground color, and background color
//
//********************************************************************
void wxTextCtrlEx::EncodeMarkupFont(wxString &aString,
                                    wxTextAttr &tAttr,
                                    int &aFontSize,
                                    wxColour &aForeground,
                                    wxColour &aBackground)
{
    wxString    strFontString   = wxT("");
    wxString    strFaceName     = wxT("");

    if( tAttr.HasFont( ) )
    {
        wxFont      wFont = tAttr.GetFont( );
        if( wFont.IsOk( ) )
        {
            int     currentFontSize = GetFontSizeFromwxTwips( wFont.GetPointSize( ) );

            // See if font size is valid.  If so, see if it has changed
            if( currentFontSize != 0 && currentFontSize != aFontSize )
            {
                aFontSize = currentFontSize;
                wxString strTemp;
                strTemp.Printf( wxT(" SIZE=%d"), currentFontSize );
                strFontString += strTemp;
            }

            strFaceName = wFont.GetFaceName( );
        }
    }

    if( tAttr.HasTextColour( )  &&  tAttr.GetTextColour( ) != aForeground )
    {
        aForeground = tAttr.GetTextColour( );
        wxString strTemp    = aForeground.GetAsString( wxC2S_HTML_SYNTAX );
        strFontString += wxT(" COLOR=\"");
        strFontString += strTemp;
        strFontString += wxT("\"");
    }

    // Don't let background color match foreground
    if( tAttr.HasBackgroundColour( )  &&  tAttr.GetBackgroundColour( ) != aBackground && tAttr.GetBackgroundColour( ) != aForeground )
    {
        aBackground = tAttr.GetBackgroundColour( );
        wxString strTemp    = aBackground.GetAsString( wxC2S_HTML_SYNTAX );
        strFontString += wxT(" BACK=\"");
        strFontString += strTemp;
        strFontString += wxT("\"");
    }

    if (!strFontString.IsEmpty())
    {
        aString += wxT("</FONT><FONT FACE=\"");
        aString += strFaceName;
        aString += wxT("\"");
        aString += strFontString;
        aString += wxT(">");
    }
}


//********************************************************************
// EncodeMarkupState
//
// Handles Bold, Italic, and Underline
//
//********************************************************************
void wxTextCtrlEx::EncodeMarkupState(wxString &aString,
                                     bool &aPreviousState,
                                     wxTextAttr &tAttr,
                                     unsigned aFlag,
                                     const wchar_t *aTag)
{
    bool    bCurrentState;
    if( !tAttr.HasFont( ) )
        return;

    wxFont  wFont           = tAttr.GetFont( );
    if( !wFont.IsOk( ) )
        return;

    switch( aFlag )
    {
        case wxTCE_BOLD:
            bCurrentState = ( wFont.GetWeight( )  ==  wxFONTWEIGHT_BOLD );
            break;
        case wxTCE_ITALIC:
            bCurrentState = ( wFont.GetStyle( )  ==  wxFONTSTYLE_ITALIC );
            break;
        case wxTCE_UNDERLINE:
            bCurrentState = wFont.GetUnderlined( );
            break;
        default:
            return;
            break;
    }

    // If the state changed...
    if( aPreviousState  !=  bCurrentState )
    {
        // Set the state...
        aPreviousState = bCurrentState;

        // Add the appropriate tag to the text string
        // to indicate the current state
        if( bCurrentState )
        {
            aString += wxT("<");
        }
        else
        {
            aString += wxT("</");
        }
        aString += aTag;
        aString += wxT(">");
    }
}


//********************************************************************
int wxTextCtrlEx::GetFontSizeFromPoints( int aPts )
{
    // Convert twips to printer points
//    int intPrinterPoints = aTwips / 20;

    // Now see if it matches one of the allowed font sizes (it should)
    int     loop;
    bool    bFound = false;

    for( loop=nFontMin; !bFound && loop <= nFontMax; loop++ )
    {
        bFound = (nFontSizes[ loop ] == aPts);
    }

    // If not found, set to 0 (valid result is 1..7)
    if( !bFound )
    {
        loop = 0;
    }

    return loop;
}


//********************************************************************
//
//  20 Twips per printer point
//
//********************************************************************
int wxTextCtrlEx::GetFontSizeFromTwips(int aTwips)
{
    // Convert twips to printer points
    int intPrinterPoints = aTwips / 20;

    // Now see if it matches one of the allowed font sizes (it should)
    int     loop;
    bool    bFound = false;

    for( loop=nFontMin; !bFound && loop <= nFontMax; loop++ )
    {
        bFound = nFontSizes[loop] == intPrinterPoints;
    }

    // If not found, set to 0 (valid result is 1..7)
    if( !bFound)
    {
        loop = 0;
    }

    return loop;
}


//********************************************************************
//
//  15 "wxTwips" per printer point
//
//********************************************************************
int wxTextCtrlEx::GetFontSizeFromwxTwips(int awxTwips)
{
    // Convert twips to printer points
    int intPrinterPoints;

    intPrinterPoints = wxPointsToTruePoints( awxTwips );

    // Now see if it matches one of the allowed font sizes (it should)
    int     loop;
    bool    bFound = false;

    for( loop=nFontMin; !bFound && loop <= nFontMax; loop++ )
    {
        bFound = nFontSizes[loop] == intPrinterPoints;
    }

    // If not found, set to 0 (valid result is 1..7)
    if( !bFound)
    {
        loop = 0;
    }

    return loop;
}


//********************************************************************
void wxTextCtrlEx::ClearContents( )
{
    Clear( );
    m_vecAttr.clear( );     //  Clear the attributes vector
    m_TextAttrDefault.SetFlags( wxTEXT_ATTR_FONT | wxTEXT_ATTR_TEXT_COLOUR | wxTEXT_ATTR_BACKGROUND_COLOUR );
    SetDefaultStyle( m_TextAttrDefault );

    // Finally, reset our line counter;
    m_LineCount = 0;
}


//********************************************************************
//
//  Decoding (and displaying) text functions
//
//********************************************************************
void wxTextCtrlEx::DisplayMessage(wxTextAttr &tAttr, const wxString &aName, const wxString &aMessage, bool aShowTimestamp)
{
    bool    fInsertStartedAtEnd     = ( this->GetInsertionPoint( )  ==  this->GetLastPosition( ) ) ? true : false;
    long    lOrigInsertionPoint     = this->GetInsertionPoint( );
    int     iW, iH;
    this->GetClientSize( &iW, &iH );
    wxPoint wxPt( 0, (iH > 0) ? (iH - 1) : 0 );
    wxTextCoord     tcCol, tcRow;
    if( this->HitTest( wxPt, &tcCol, &tcRow )  !=  wxTE_HT_UNKNOWN )
    {
        fInsertStartedAtEnd = ( tcRow  ==  (GetNumberOfLines( ) - 1) ) ? true : false;
        if( !fInsertStartedAtEnd )
            this->Freeze( );
    }

    this->ResetReceiveFont( );

    if( aShowTimestamp )
    {
        wxString    strTime;
        GetTimeString( strTime );
        this->AddText( tAttr, aName + strTime + _(": "), false );
    }
    else
    {
        this->AddText( tAttr, aName + _(": "), false );
    }

    int length          = aMessage.Len( );
    int positionCurrent = 0;
    int positionStart   = 0;
    int currentLength   = 0;

    bool bFoundMarkup;
    bool bFoundBreak;

    while( positionCurrent < length )
    {
        bFoundMarkup = false;
        bFoundBreak = false;

        switch( aMessage[positionCurrent] )
        {
            // format markup
            case wxT('<'):
            {
                wxString        strT1( aMessage.Mid(positionStart, currentLength) );

                this->AddText( tAttr, strT1, true);

                bFoundBreak = DecodeFormatMarkup(aMessage, positionCurrent, length, bFoundMarkup);

                if (bFoundBreak)
                {
                    this->AddNewLine();
                }

                currentLength = 0;
                positionStart = positionCurrent;
            }

            // plain text
            default:
            {
                break;
            }
        }

        if( !bFoundMarkup )
        {
            // normal text
            positionCurrent++;
            currentLength++;
        }
    }

    this->AddText( tAttr, aMessage.Mid(positionStart, currentLength), true );

    this->AddNewLine( );

    //  ! ! !  HACK ALERT  ! ! !
    //  When text is added to the Receive box that causes the box to scroll the last line
    //  of the added text is displayed on the first line of the control (under Windows).
    //  Since the chat messages always end with a CR the control is always blank.
    //  The following loop tries to ensure at least some text is scrolled back into
    //  the control.  Since the message is auto-wrapped and may contain characters of
    //  varying point sizes, doing reasonably accurate and RELIABLE calculations to try
    //  to always fill the control with text seems daunting.  But I am open to suggestions...
    //  ! ! !  HACK ALERT  ! ! !
    if( fInsertStartedAtEnd )
    {
        int iNumLines;
        iNumLines = GetNumberOfLines( );

        int iCP;
        for( int i=8; i>0; i-- )
        {
            iCP = XYToPosition( 0, iNumLines-i );
            ShowPosition( iCP );
        }
    }
    else
    {
        this->SetInsertionPoint( lOrigInsertionPoint);
        this->Thaw( );
    }
}


//********************************************************************
void wxTextCtrlEx::AddText( wxTextAttr &tAttr, const wxString &aText, bool bApplyFormatting)
{
    if (bApplyFormatting)
    {
        wxTextAttr  tAttrSave;
        wxFont      wFont;

        tAttrSave = GetDefaultStyle( );
        wFont = tAttr.GetFont( );
        wFont.SetFaceName( m_ReceiveFaceName );
        wFont.SetPointSize( nFontSizes[ m_ReceiveFontSize - 1 ] );
        wFont.SetWeight( (m_bReceiveIsBold) ? wxFONTWEIGHT_BOLD : wxFONTWEIGHT_NORMAL );
        wFont.SetStyle( (m_bReceiveIsItalic) ? wxFONTSTYLE_ITALIC : wxFONTSTYLE_NORMAL );
        wFont.SetUnderlined( m_bReceiveIsUnderline );
        tAttr.SetFont( wFont );
        tAttr.SetTextColour( m_ReceiveForeground );
        tAttr.SetBackgroundColour( m_ReceiveBackground );
        tAttr.SetFlags( wxTEXT_ATTR_FONT | wxTEXT_ATTR_TEXT_COLOUR | wxTEXT_ATTR_BACKGROUND_COLOUR );
        SetDefaultStyle( tAttr );
        SetInsertionPointEnd( );
        WriteText( aText );
        tAttrSave.SetFlags( wxTEXT_ATTR_FONT | wxTEXT_ATTR_TEXT_COLOUR | wxTEXT_ATTR_BACKGROUND_COLOUR );
        SetDefaultStyle( tAttrSave );
    }
    else
    {
        SetInsertionPointEnd( );
        WriteText( aText );
    }
}


//********************************************************************
bool wxTextCtrlEx::DecodeFormatMarkup(const wxString &aMessage,
                                      int &aPositionCurrent,
                                      int aLength,
                                      bool &aFoundMarkup)
{
    int positionAdjust = 1;
    bool bFoundBreak = false;
    bool bIsCloseTag = false;
    bool bIsFont = false;

    // Check to see if this is a closing tag
    if (aMessage[aPositionCurrent + positionAdjust] == wxT('/'))
    {
        bIsCloseTag = true;
        positionAdjust = 2;
    }

    int intNextPosition = aPositionCurrent + positionAdjust + 1;

    switch (aMessage[aPositionCurrent + positionAdjust])
    {
        case wxT('B'):
        case wxT('b'):
        {
            if (aMessage[intNextPosition] == wxT('R') ||
                  aMessage[intNextPosition] == wxT('r'))
            {
                if (intNextPosition<(aLength-1) && aMessage[intNextPosition+1]==wxT('>'))
                {
                    bFoundBreak = true;
                    aFoundMarkup = true;
                }
            }
            else if (intNextPosition<aLength && aMessage[intNextPosition]==wxT('>'))
            {
                m_bReceiveIsBold = !bIsCloseTag;
                aFoundMarkup = true;
            }
            break;
        }

        case wxT('I'):
        case wxT('i'):
        {
            if (intNextPosition<aLength && aMessage[intNextPosition]==wxT('>'))
            {
                m_bReceiveIsItalic = !bIsCloseTag;
                aFoundMarkup = true;
            }
            break;
        }

        case wxT('U'):
        case wxT('u'):
        {
            if (intNextPosition<aLength && aMessage[intNextPosition]==wxT('>'))
            {
                m_bReceiveIsUnderline = !bIsCloseTag;
                aFoundMarkup = true;
            }
            break;
        }

        case wxT('F'):
        case wxT('f'):
        {
            wxString strTemp = aMessage.Mid(aPositionCurrent + positionAdjust, 4);

            if( strTemp.CmpNoCase(wxT("FONT")) == 0)
            {
                bIsFont = true;
                if (!bIsCloseTag)
                {
                    DecodeFont(aMessage, aPositionCurrent, aLength);
                }
                aFoundMarkup = true;
            }
            break;
        }

        // Unknown tag, ignore it
        default:
        {
            break;
        }
    }

    // Find the closing tag and position just past it
    if (aFoundMarkup)
    {
        while (aPositionCurrent < aLength &&
               aMessage[aPositionCurrent] != wxT('>'))
        {
            aPositionCurrent++;
        }

        if (aPositionCurrent < aLength)
        {
            aPositionCurrent++;
        }
    }

    return bFoundBreak;
}


//********************************************************************
void wxTextCtrlEx::DecodeFont(const wxString &aMessage,
                              int &aPositionCurrent,
                              int aLength)
{
    // Get past the FONT keyword
    DecodeEatChars(aMessage, aPositionCurrent, aLength);

    while (DecodeFontAttribute(aMessage, aPositionCurrent, aLength));

}


//********************************************************************
bool wxTextCtrlEx::DecodeFontAttribute(const wxString &aMessage,
                                       int &aPositionCurrent,
                                       int aLength)
{
    // Move past any spaces to the first character of the attribute
    if (!DecodeEatSpaces(aMessage, aPositionCurrent, aLength))
    {
        return false;
    }

    // Get the attribute name...
    int intValuePosition = aMessage.find(wxT("="), aPositionCurrent);

    if (intValuePosition == -1)
    {
        return false;
    }

    wxString strAttribute = aMessage.Mid(aPositionCurrent, intValuePosition - aPositionCurrent);

    // Move current position to the next char after the equal
    aPositionCurrent = intValuePosition + 1;

    // Eliminate any spaces
    if (!DecodeEatSpaces(aMessage, aPositionCurrent, aLength))
    {
        return false;
    }

    // Get the value...
    /*
    intValuePosition = aPositionCurrent;
    DecodeEatChars(aMessage, aPositionCurrent, aLength);
    CString strValue = aMessage.Mid(intValuePosition, aPositionCurrent - intValuePosition);
    */

    if (strAttribute.CmpNoCase(wxT("FACE")) == 0)
    {
        DecodeFontFace(aMessage, aPositionCurrent, aLength);
    }
    else if (strAttribute.CmpNoCase(wxT("COLOR")) == 0)
    {
        DecodeFontColor(aMessage, aPositionCurrent, aLength, m_ReceiveForeground);
    }
    else if (strAttribute.CmpNoCase(wxT("BACK")) == 0)
    {
        DecodeFontColor(aMessage, aPositionCurrent, aLength, m_ReceiveBackground);
    }
    else if (strAttribute.CmpNoCase(wxT("SIZE")) == 0)
    {
        DecodeFontSize(aMessage, aPositionCurrent, aLength);
    }
    else
    {
        // Unhandled attribute, read past the value
        DecodeEatChars(aMessage, aPositionCurrent, aLength);
    }

    // return true if there may be more attributes to process
    return (aPositionCurrent < aLength &&
            aMessage[aPositionCurrent] != wxT('>'));
}


//********************************************************************
void wxTextCtrlEx::DecodeFontFace(const wxString &aMessage,
                                  int &aPositionCurrent,
                                  int aLength)
{
    // Need to extract the face name.
    // First, set our start point past the first quote
    aPositionCurrent++;
    int startPosition = aPositionCurrent;

    // Find the matching quote
    int endPosition = aMessage.find( wxT('\"'), startPosition );

    // If we found our matching quote...
    if (endPosition != -1 )
    {
        // Extract the face name
        m_ReceiveFaceName = aMessage.Mid( startPosition, endPosition - startPosition );
        aPositionCurrent = endPosition;
    }

    // Set the current position one char past the closing
    // quote (if there is such a position)
    if (aPositionCurrent < aLength)
    {
        aPositionCurrent++;
    }
}


//********************************************************************
/////////////////////////////////////////////////////////////////////////////
// DecodeFontColor
//
void wxTextCtrlEx::DecodeFontColor(const wxString &aMessage,
                                   int &aPositionCurrent,
                                   int aLength,
                                   wxColour &aColourref)
{
    // First, set our start point past the first quote
    // and the # character
    aPositionCurrent += 2;
    int startPosition = aPositionCurrent;

    // Find the matching quote
    int endPosition = aMessage.find( wxT('\"'), startPosition );

    // If we found our matching quote...
    if (endPosition != -1)
    {
        // Extract the hexadecimal RRGGBB value
        wxString strTemp = aMessage.Mid(startPosition, endPosition - startPosition);
        int intR = 0;
        int intG = 0;
        int intB = 0;
        if( swscanf(strTemp.GetData( ), wxT("%2X%2X%2X"), &intR, &intG, &intB) == 3 )
        {
            aColourref.Set( intR, intG, intB );
        }
        aPositionCurrent = endPosition;
    }

    // Set the current position one char past the closing
    // quote (if there is such a position)
    if (aPositionCurrent < aLength)
    {
        aPositionCurrent++;
    }
}


//********************************************************************
void wxTextCtrlEx::DecodeFontSize(const wxString &aMessage,
                                  int &aPositionCurrent,
                                  int aLength)
{
    int startPosition = aPositionCurrent;
    DecodeEatChars(aMessage, aPositionCurrent, aLength);
    wxString strSize = aMessage.Mid(startPosition, aPositionCurrent - startPosition);

    if (!strSize.IsEmpty())
    {
        int tempSize = 0;
        if( swscanf(strSize.GetData( ), wxT("%d"), &tempSize) == 1 )
        {
            m_ReceiveFontSize = tempSize;
        }
    }
}


//********************************************************************
//
// DecodeEatSpaces
//
// Returns false if we end up at the end of the string, or at
// the end of the tag.
//
bool wxTextCtrlEx::DecodeEatSpaces(const wxString &aMessage,
                                   int &aPosition,
                                   int aLength)
{
    // Eliminate any spaces
    while(aPosition < aLength &&
          aMessage[aPosition] == wxT(' '))
    {
        aPosition++;
    }

    if (aPosition >= aLength ||
          aMessage[aPosition] == wxT('>'))
    {
        return false;
    }

    return true;
}


//********************************************************************
void wxTextCtrlEx::DecodeEatChars(const wxString &aMessage,
                                  int &aPosition,
                                  int aLength)
{
    while (aPosition < aLength &&
           aMessage[aPosition] != wxT(' ') &&
           aMessage[aPosition] != wxT('>'))
    {
        aPosition++;
    }
}


//********************************************************************
void wxTextCtrlEx::GetTimeString( wxString &strTime )
{
    wxDateTime   dtNow = wxDateTime::Now( );

    strTime = dtNow.Format( _(" (%X)") );
}
