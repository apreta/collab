/////////////////////////////////////////////////////////////////////////////
// Name:        xh_wxTextCtrlEx.cpp
// Purpose:     XRC resource for wxTextCtrlEx
// Author:
// Created:
// RCS-ID:
// Copyright:
// Licence:
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

#if wxUSE_XRC

#include "wx/xh_wxTextCtrlEx.h"
#include "wx/wxTextCtrlEx.h"
//#include "wx/log.h"

IMPLEMENT_DYNAMIC_CLASS(wxTextCtrlExXmlHandler, wxXmlResourceHandler)

wxTextCtrlExXmlHandler::wxTextCtrlExXmlHandler()
: wxXmlResourceHandler()
{
    XRC_ADD_STYLE(wxTE_PROCESS_ENTER);
    XRC_ADD_STYLE(wxTE_PROCESS_TAB);
    XRC_ADD_STYLE(wxTE_MULTILINE);
    XRC_ADD_STYLE(wxTE_PASSWORD);
    XRC_ADD_STYLE(wxTE_READONLY);
    XRC_ADD_STYLE(wxTE_RICH);
    XRC_ADD_STYLE(wxTE_RICH2);
    XRC_ADD_STYLE(wxTE_AUTO_URL);
    XRC_ADD_STYLE(wxTE_NOHIDESEL);
    XRC_ADD_STYLE(wxHSCROLL);
    XRC_ADD_STYLE(wxTE_LEFT);
    XRC_ADD_STYLE(wxTE_CENTRE);
    XRC_ADD_STYLE(wxTE_RIGHT);
    XRC_ADD_STYLE(wxTE_DONTWRAP);
    XRC_ADD_STYLE(wxTE_CHARWRAP);
    XRC_ADD_STYLE(wxTE_WORDWRAP);
    XRC_ADD_STYLE(wxTE_BESTWRAP);
    XRC_ADD_STYLE(wxTE_CAPITALIZE);

    AddWindowStyles();
}


//***********************************************************
wxObject *wxTextCtrlExXmlHandler::DoCreateResource()
{
    XRC_MAKE_INSTANCE(TCEx, wxTextCtrlEx)

    TCEx->Create(m_parentAsWindow,
                 GetID(),
                 _T(""),  //  strBmpName,
                 GetPosition(),
                 GetSize(),
                 GetStyle(_T("style"), wxBU_AUTODRAW),
                 wxDefaultValidator,
                 GetName());

    SetupWindow(TCEx);

    return TCEx;
}


//***********************************************************
bool wxTextCtrlExXmlHandler::CanHandle(wxXmlNode *node)
{
    return IsOfClass(node, wxT("wxTextCtrlEx"));
}

#endif // wxUSE_XRC
