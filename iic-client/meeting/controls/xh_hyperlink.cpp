/////////////////////////////////////////////////////////////////////////////
// Name:        xh_HyperLink.cpp
// Purpose:     XRC resource for wxHyperLink
// Author:
// Created:
// RCS-ID:
// Copyright:
// Licence:     wxWindows licence
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

#if wxUSE_XRC

#include "wx/xh_hyperlink.h"
#include "wx/hyperlink.h"

IMPLEMENT_DYNAMIC_CLASS(wxHyperLinkXmlHandler, wxXmlResourceHandler)

wxHyperLinkXmlHandler::wxHyperLinkXmlHandler()
: wxXmlResourceHandler()
{
    //XRC_ADD_STYLE(wxTR_EDIT_LABELS);
    AddWindowStyles();
}

wxObject *wxHyperLinkXmlHandler::DoCreateResource()
{
    XRC_MAKE_INSTANCE(tree, wxHyperLink)

    tree->Create(m_parentAsWindow,
                GetID(),
                GetText(_T("label")),
                GetPosition(), GetSize(),
                GetStyle(_T("style"), 0),
                GetName());

    SetupWindow(tree);

    return tree;
}

bool wxHyperLinkXmlHandler::CanHandle(wxXmlNode *node)
{
    return IsOfClass(node, wxT("wxHyperLink"));
}

#endif // wxUSE_XRC && wxUSE_TREECTRL
