/////////////////////////////////////////////////////////////////////////////
// Name:        xh_wxTogBmpBtn.cpp
// Purpose:     XRC resource for wxTogBmpBtn
// Author:
// Created:
// RCS-ID:
// Copyright:
// Licence:
/////////////////////////////////////////////////////////////////////////////

// For compilers that support precompilation, includes "wx.h".
#include "wx/wxprec.h"

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

#if wxUSE_XRC

#include "wx/xh_wxTogBmpBtn.h"
#include "wx/wxTogBmpBtn.h"
#include "wx/log.h"

IMPLEMENT_DYNAMIC_CLASS(wxTogBmpBtnXmlHandler, wxXmlResourceHandler)

wxTogBmpBtnXmlHandler::wxTogBmpBtnXmlHandler()
: wxXmlResourceHandler()
{
    XRC_ADD_STYLE(wxBU_LEFT);
    XRC_ADD_STYLE(wxBU_RIGHT);
    XRC_ADD_STYLE(wxBU_TOP);
    XRC_ADD_STYLE(wxBU_BOTTOM);
    XRC_ADD_STYLE(wxBU_AUTODRAW);

    AddWindowStyles();
}


//***********************************************************
wxObject *wxTogBmpBtnXmlHandler::DoCreateResource()
{
    XRC_MAKE_INSTANCE(TBB, wxTogBmpBtn)
    wxString    strBmpName;
    strBmpName = GetText( _T("bitmap") );

    wxString    strT;
    strT = GetText( _T("toggle") );
    bool    fTogglable;
    //  The following line of code is what we SHOULD be using, but it doesn't work, so I did it the old fashioned way...
//    fTogglable = GetBool( _T("toggle") );
    fTogglable = ( 0 == strT.CmpNoCase( _T("yes") ) );

    TBB->Create(m_parentAsWindow,
                GetID(),
                strBmpName,
                GetPosition(),
                GetSize(),
                GetStyle(_T("style"), wxBU_AUTODRAW),
                wxDefaultValidator,
                GetName());

    TBB->SetTogglable( fTogglable );

    wxString    strBmpTip;
    strBmpTip = GetText( _T("tooltip") );
    TBB->SetToolTip( strBmpTip );

    SetupWindow(TBB);

    return TBB;
}


//***********************************************************
bool wxTogBmpBtnXmlHandler::CanHandle(wxXmlNode *node)
{
    return IsOfClass(node, wxT("wxTogBmpBtn"));
}

#endif // wxUSE_XRC
