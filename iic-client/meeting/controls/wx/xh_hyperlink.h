/////////////////////////////////////////////////////////////////////////////
// Name:        wx/xrc/xh_HyperLink.h
// Purpose:     XML resource handler for wxHyperLink
// Copyright:   (c) 2010 Michael Tinglof
// Licence:     wxWindows licence
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_XH_HYPERLINK_H_
#define _WX_XH_HYPERLINK_H_

#include <wx/xrc/xmlres.h>

#if wxUSE_XRC

class /*WXDLLIMPEXP_XRC*/ wxHyperLinkXmlHandler : public wxXmlResourceHandler
{
    DECLARE_DYNAMIC_CLASS(wxHyperLinkXmlHandler)

public:
    wxHyperLinkXmlHandler();
    virtual wxObject *DoCreateResource();
    virtual bool CanHandle(wxXmlNode *node);
};

#endif // wxUSE_XRC

#endif // _WX_XH_HYPERLINK_H_
