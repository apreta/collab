/////////////////////////////////////////////////////////////////////////////
// Name:        wx/xh_wxTogBmpBtn.h
// Purpose:     XML resource handler for wxTogBmpBtn
// Author:      Jim Park
// Created:     2007/01/12
// RCS-ID:      
// Copyright:   
// Licence:     
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_XH_WXTOGBMPBTN_H_
#define _WX_XH_WXTOGBMPBTN_H_

#include <wx/xrc/xmlres.h>

#if wxUSE_XRC

class wxTogBmpBtnXmlHandler : public wxXmlResourceHandler
{
    DECLARE_DYNAMIC_CLASS(wxTogBmpBtnXmlHandler)

public:
    wxTogBmpBtnXmlHandler();
    virtual wxObject *DoCreateResource();
    virtual bool CanHandle(wxXmlNode *node);
};

#endif // wxUSE_XRC

#endif // _WX_XH_WXTOGBMPBTN_H_
