/////////////////////////////////////////////////////////////////////////////
// Name:        wxTextCtrlEx.h
// Purpose:     wxTextCtrlEx class interface
// Author:      Jim Park
// Modified by:
// Created:     2007.01.18
// RCS-ID:
// Copyright:
// Licence:
/////////////////////////////////////////////////////////////////////////////

#ifndef _WXTEXTCTRLEX_H_BASE_
#define _WXTEXTCTRLEX_H_BASE_

#include "wx/colour.h"
#include "wx/string.h"
#include "wx/textctrl.h"
#include <wx/richtext/richtextbuffer.h>

#include <vector>


BEGIN_DECLARE_EVENT_TYPES()
    DECLARE_LOCAL_EVENT_TYPE(wxEVT_MY_TEXTCTRLEX_ONCHAR, wxID_HIGHEST + 1234)
END_DECLARE_EVENT_TYPES()

// it may also be convenient to define an event table macro for this event type
#define EVT_MY_TEXTCTRLEX_ONCHAR(id, fn) \
DECLARE_EVENT_TABLE_ENTRY( \
        wxEVT_MY_TEXTCTRLEX_ONCHAR, id, wxID_ANY, \
        (wxObjectEventFunction)(wxEventFunction) wxStaticCastEvent( wxCommandEventFunction, &fn ), \
        (wxObject *) NULL \
        ),

//DEFINE_EVENT_TYPE(wxEVT_MY_TEXTCTRLEX_ONCHAR)

// ----------------------------------------------------------------------------
// wxTextCtrlEx: Extensions to wxTextCtrl.
//  This class is based on wxTextCtrl.
// ----------------------------------------------------------------------------


typedef     std::vector<wxTextAttr> ATTRVECTOR;


class wxTextCtrlEx : public wxTextCtrl
{
protected:

//    CPtrArray *m_pEmoticons;

    int         m_LineCount;
    wxFont      m_FontDefault;
    wxTextAttr  m_TextAttrDefault;
    int         m_iPriorInsertPoint;
    int         m_FontSize;

    wxFont      m_ReceiveFont;
    wxTextAttr  m_ReceiveTextAttr;
    wxString    m_ReceiveFaceName;
    int         m_ReceiveFontSize;
    wxColour    m_ReceiveForeground;
    wxColour    m_ReceiveBackground;
    bool        m_bReceiveIsBold;
    bool        m_bReceiveIsItalic;
    bool        m_bReceiveIsUnderline;

    bool        m_bIgnoreOnCharEntry;

    ATTRVECTOR  m_vecAttr;

public:
    bool        SetStyleU( long lStart, long lEnd, const wxTextAttr &style );
    bool        GetStyleU( long lPos, wxTextAttr &style );

    void        GetBackground( wxColour &wColour );
    void        GetForeground( wxColour &wColour );
    wxTextAttr  GetTextAttr( )    const { return m_TextAttrDefault; };

    void ResetReceiveFont();
//    void AddText(CHARFORMAT2 &aFormat, LPCTSTR aText, BOOL bApplyFormatting);
    void AddText(wxTextAttr &tAttr, const wxString &aText, bool bApplyFormatting);
    void AddNewLine();
//    void AddEmoticon(HBITMAP aBitmap, BOOL aAddAtEnd);
//    void GetTimeString(CString &strTime);
    void GetTimeString(wxString &strTime);

    bool DecodeFormatMarkup(const wxString &aMessage,
                            int &aPositionCurrent,
                            int aLength,
							bool &aFoundMarkup);

    void DecodeFont(const wxString &aMessage,
                    int &aPositionCurrent,
                    int aLength);

    bool DecodeFontAttribute(const wxString &aMessage,
                             int &aPositionCurrent,
                             int aLength);

    void DecodeFontFace(const wxString &aMessage,
                        int &aPositionCurrent,
                        int aLength);

    void DecodeFontColor(const wxString &aMessage,
                         int &aPositionCurrent,
                         int aLength,
                         wxColour &aColorref);

    void DecodeFontSize(const wxString &aMessage,
                        int &aPositionCurrent,
                        int aLength);

    bool DecodeEatSpaces(const wxString &aMessage,
                         int &aPosition,
                         int aLength);

    void DecodeEatChars(const wxString &aMessage,
                        int &aPosition,
                        int aLength);

    void EncodeMarkupInitialFont(wxString &aString);

    void EncodeMarkupFont(wxString &aString,
                          wxTextAttr &cf,
                          int &aFontSize,
                          wxColour &aForeground,
                          wxColour &aBackground);

    void EncodeMarkupState(wxString &aString,
                           bool &aPreviousState,
                           wxTextAttr &cf,
                           unsigned aFlag,
                           const wchar_t *aTag);


public:
	wxTextCtrlEx() : wxTextCtrl() { }
    wxTextCtrlEx(wxWindow *parent,
                 wxWindowID id,
                 const wxString& value = _T(""),
                 const wxPoint& pos = wxDefaultPosition,
                 const wxSize& size = wxDefaultSize,
                 long style = 0,
                 const wxValidator& validator = wxDefaultValidator,
                 const wxString& name = wxTextCtrlNameStr);

    virtual ~wxTextCtrlEx() { }

    bool Create(wxWindow *parent,
                wxWindowID id,
                const wxString& value = _T(""),
                const wxPoint& pos = wxDefaultPosition,
                const wxSize& size = wxDefaultSize,
                long style = 0,
                const wxValidator& validator = wxDefaultValidator,
                const wxString& name = wxTextCtrlNameStr);

    void    OnChar( wxKeyEvent &event );
    void    OnLeftUp( wxMouseEvent &event );

    void InitializeForChat(  //  CPtrArray *pEmoticons,
                           const wxString &aDefaultFontFace,
                           int aDefaultFontSize,
                           const wxColour &aDefaultFontForeground,
                           const wxColour &aDefaultFontBackground,
                           bool aIsBold,
                           bool aIsItalic,
                           bool aIsUnderline);

    void ClearContents();
    void GetTextAsMarkup(wxString &aString);

    bool SetSelectionTextAttr(wxTextAttr &ta);
    bool GetSelectionTextAttr(wxTextAttr &ta);
    int  GetFontSizeFromTwips(int aTwips);        // 20 Twips per point
    int  GetFontSizeFromwxTwips(int awxTwips);    // 15 "wxTwips" per point
    int  GetFontSizeFromPoints(int aPts);

//    void InsertEmoticon(int aEmoticonId);
//    void InsertNewLine();
    void DisplayMessage(wxTextAttr &tAttr, const wxString &aName, const wxString &aMessage, bool aShowTimestamp);
    void DisplaySystemMessage(wxTextAttr aMessageFormat, const wxString &aMessage);

    void FormatBold(bool aBold);
    void FormatItalic(bool aItalic);
    void FormatUnderline(bool aUnderline);
    int FormatFontSizeNormal();
    int FormatFontSizeIncrease();
    int FormatFontSizeDecrease();
    void FormatFontSize(int aFontSize);
    void FormatForegroundColor(wxColour &aColor);
    void FormatBackgroundColor(wxColour &aColor);

    void SetIgnoreOnCharEntry( bool aFlag )     { m_bIgnoreOnCharEntry = aFlag; };
    bool GetIgnoreOnCharEntry( )                { return m_bIgnoreOnCharEntry; };

	// A key pressed callback function, to trap the enter key.
	void (*cb_KeyPressed)(unsigned key);

    DECLARE_NO_COPY_CLASS(wxTextCtrlEx)
    DECLARE_DYNAMIC_CLASS(wxTextCtrlEx)
    DECLARE_EVENT_TABLE()
};

/////////////////////////////////////////////////////////////////////////////


#endif // #ifndef _WXTEXTCTRLEX_H_BASE_
