/////////////////////////////////////////////////////////////////////////////
// Name:        wx/xrc/xh_tree.h
// Purpose:     XML resource handler for wxTreeCtrl
// Author:      Brian Gavin
// Created:     2000/09/09
// RCS-ID:      $Id: xh_treelist.h,v 1.1 2006-12-26 19:00:05 mike Exp $
// Copyright:   (c) 2000 Brian Gavin
// Licence:     wxWindows licence
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_XH_TREELIST_H_
#define _WX_XH_TREELIST_H_

#include <wx/xrc/xmlres.h>

#if wxUSE_XRC

class /*WXDLLIMPEXP_XRC*/ wxTreeListCtrlXmlHandler : public wxXmlResourceHandler
{
    DECLARE_DYNAMIC_CLASS(wxTreeListCtrlXmlHandler)

public:
    wxTreeListCtrlXmlHandler();
    virtual wxObject *DoCreateResource();
    virtual bool CanHandle(wxXmlNode *node);
};

#endif // wxUSE_XRC && wxUSE_TREECTRL

#endif // _WX_XH_TREE_H_
