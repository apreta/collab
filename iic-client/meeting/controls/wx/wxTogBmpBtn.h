/////////////////////////////////////////////////////////////////////////////
// Name:        wx/wxTogBmpBtn.h
// Purpose:     wxTogBmpBtn class interface
// Author:      Jim Park
// Modified by:
// Created:     2007.01.16
// RCS-ID:
// Copyright:
// Licence:
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_WXTOGBMPBTN_H_BASE_
#define _WX_WXTOGBMPBTN_H_BASE_

#include "wx/defs.h"

//#if wxUSE_WXTOGBMPBTN

#include "wx/bitmap.h"
#include "wx/button.h"
#include "wx/colour.h"
#include "wx/bmpbuttn.h"

//extern WXDLLEXPORT_DATA(const wxChar) wxTogBmpBtnNameStr[];

// ----------------------------------------------------------------------------
// wxTogBmpBtn: a button which shows bitmaps instead of the usual string.
// It has different bitmaps for different states (focused/disabled/pressed).
// This class also supports a toggled state.
// This slass is based on wxBitmapButton.
// ----------------------------------------------------------------------------

static wxColour gTransColour( 255, 0, 255 );

class wxTogBmpBtn : public wxBitmapButton
{
public:
    wxTogBmpBtn() : wxBitmapButton() {  m_fTogglable = true; m_fIsDown = false; }

    wxTogBmpBtn(wxWindow *parent,
              wxWindowID id,
              const wxBitmap& bitmap,
              const wxPoint& pos = wxDefaultPosition,
              const wxSize& size = wxDefaultSize,
              long style = wxBU_AUTODRAW,
              const wxValidator& validator = wxDefaultValidator,
              const wxString& name = wxButtonNameStr)
            : wxBitmapButton( parent, id, bitmap, pos, size, style, validator, name )
    {
        m_fTogglable = true;
        m_fIsDown = false;
    }

    wxTogBmpBtn(wxWindow *parent,
                wxWindowID id,
                const wxString& strBmpBaseName,
                const wxPoint& pos = wxDefaultPosition,
                const wxSize& size = wxDefaultSize,
                long style = wxBU_AUTODRAW,
                const wxValidator& validator = wxDefaultValidator,
                const wxString& name = wxButtonNameStr);


    bool Create(wxWindow *parent,
                wxWindowID id,
                const wxString& strBmpBaseName,
                const wxPoint& pos = wxDefaultPosition,
                const wxSize& size = wxDefaultSize,
                long style = wxBU_AUTODRAW,
                const wxValidator& validator = wxDefaultValidator,
                const wxString& name = wxButtonNameStr);

    bool GetTogglable( )                { return m_fTogglable; }
    void SetTogglable(bool fTogglable)  { m_fTogglable = fTogglable; }

    bool IsDown( )                      { return m_fIsDown; }
    void SetDown( bool fSetDown )       { if( m_fTogglable && (fSetDown != m_fIsDown )) ExecuteClick( ); }
    void SetDown( )                     { if( m_fTogglable && !m_fIsDown) ExecuteClick( ); }
    void SetUp( )                       { if( m_fTogglable && m_fIsDown) ExecuteClick( ); }


protected:
    //  function called when button is clicked
    void    OnClick( wxCommandEvent& event );
    void    ExecuteClick( );


private:
    bool    m_fTogglable;
    bool    m_fIsDown;


    DECLARE_NO_COPY_CLASS(wxTogBmpBtn)
    DECLARE_DYNAMIC_CLASS(wxTogBmpBtn)
    DECLARE_EVENT_TABLE()
};

//#endif // wxUSE_WXTOGBMPBTN

#endif // _WX_WXTOGBMPBTN_H_BASE_
