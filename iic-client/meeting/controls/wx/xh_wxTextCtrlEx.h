/////////////////////////////////////////////////////////////////////////////
// Name:        wx/xh_wxTextCtrlEx.h
// Purpose:     XML resource handler for wxTextCtrlEx
// Author:      Jim Park
// Created:     2007/01/12
// RCS-ID:
// Copyright:
// Licence:
/////////////////////////////////////////////////////////////////////////////

#ifndef _WX_XH_WXTEXTCTRLEX_H_
#define _WX_XH_WXTEXTCTRLEX_H_

#include <wx/xrc/xmlres.h>

#if wxUSE_XRC

class wxTextCtrlExXmlHandler : public wxXmlResourceHandler
{
    DECLARE_DYNAMIC_CLASS(wxTextCtrlExXmlHandler)

public:
    wxTextCtrlExXmlHandler();
    virtual wxObject *DoCreateResource();
    virtual bool CanHandle(wxXmlNode *node);
};

#endif // wxUSE_XRC

#endif // _WX_XH_WXTEXTCTRLEX_H_
