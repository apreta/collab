/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include "events.h"

DEFINE_LOCAL_EVENT_TYPE(wxEVT_SESSION_CONNECTED)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_CONTROLLER_REGISTERED)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_CONTROLLER_INVITE)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_CONTROLLER_INVITE_LOOKUP)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_ADDRESSBK_AUTH)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_ADDRESSBK_PROGRESS)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_SCHEDULE_FETCHED)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_SCHEDULE_PROGRESS)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_SCHEDULE_UPDATED)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_SCHEDULE_ERROR)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_MEETING_FETCHED)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_DIRECTORY_FETCHED)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_SUBMEETING_ADDED)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_CONTROLLER_IM_NOTIFY)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_CONTROLLER_PING)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_RECORDINGS_FETCHED)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_DOCUMENTS_FETCHED)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_DIRECTORY_STARTED)

DEFINE_LOCAL_EVENT_TYPE(wxEVT_SESSION_ERROR)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_MEETING_EVENT)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_MEETING_ERROR)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_MEETING_CHAT)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_ADDRESSBK_UPDATE_FAILED)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_ADDRESS_UPDATED)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_ADDRESS_DELETED)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_ADDRESS_VERSION_CHECK)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_PROFILE_FETCHED)

DEFINE_LOCAL_EVENT_TYPE(wxEVT_IM_CHAT)
DEFINE_LOCAL_EVENT_TYPE(wxEVT_IM_INVITE)

DEFINE_LOCAL_EVENT_TYPE(wxEVT_DOCSHARE_EVENT)

DEFINE_LOCAL_EVENT_TYPE(wxEVT_PRESENCE_EVENT)

DEFINE_LOCAL_EVENT_TYPE(wxEVT_EXECUTE_PENDING_COMMANDS);
DEFINE_LOCAL_EVENT_TYPE(wxEVT_UPDATE_RECONNECT_STATUS);
DEFINE_LOCAL_EVENT_TYPE(wxEVT_UPDATE_PROGRESS1_STATUS);

DEFINE_LOCAL_EVENT_TYPE(wxEVT_SHARE_TOOLBAR_STATUS);

DEFINE_LOCAL_EVENT_TYPE(wxEVT_QUEUE_EVENT);

DEFINE_LOCAL_EVENT_TYPE(wxEVT_VOIP_STATUS);
DEFINE_LOCAL_EVENT_TYPE(wxEVT_VOIP_LEVELS);

DEFINE_LOCAL_EVENT_TYPE(wxEVT_MEETING_EDIT_MEETING);
DEFINE_LOCAL_EVENT_TYPE(wxEVT_GET_PASSWORD);

DEFINE_LOCAL_EVENT_TYPE(wxEVT_SHARE_EVENT);

DEFINE_LOCAL_EVENT_TYPE(wxEVT_CONVERSION_EVENT);

DEFINE_LOCAL_EVENT_TYPE(wxEVT_FILEMANAGER_DONE);
