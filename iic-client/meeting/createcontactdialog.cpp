/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"
#include "meetingcentercontroller.h"
#include "createcontactdialog.h"
#include "wxgridext.h"
#include "addressbk.h"

//(*InternalHeaders(CreateContactDialog)
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(CreateContactDialog)
//*)

IMPLEMENT_CLASS(CreateContactDialog,wxDialog)

BEGIN_EVENT_TABLE(CreateContactDialog,wxDialog)
	//(*EventTable(CreateContactDialog)
	//*)
EVT_BUTTON( XRCID("ID_BTN_CHANGE_PASS"), CreateContactDialog::OnClickChangePass)
EVT_BUTTON( wxID_HELP, CreateContactDialog::OnClickHelp)
END_EVENT_TABLE()

CreateContactDialog::CreateContactDialog(wxWindow* parent,wxWindowID id):
m_dir(0),
m_mtg(0),
m_invt(0),
m_contact(0),
//m_group(0),
m_fCreateContact(false),
m_Grid(0),
m_fThisIsMe(false)
{
	//(*Initialize(CreateContactDialog)
	wxXmlResource::Get()->LoadObject(this,parent,_T("CreateContactDialog"),_T("wxDialog"));
	m_GridPanel = (wxPanel*)FindWindow(XRCID("ID_PANEL1"));
	m_BtnChangePass = (wxButton*)FindWindow(XRCID("ID_BTN_CHANGE_PASS"));
	//*)
}

CreateContactDialog::~CreateContactDialog()
{
//    wxLogDebug(wxT("CreateContactDialog::~CreateContactDialog"));
}

bool CreateContactDialog::EditNewAdHoc(   directory_t dir, meeting_details_t mtg, invitee_t invt, contact_t contact )
{
//    wxLogDebug(wxT("CreateContactDialog::EditNewAdHoc"));
    bool fOK = false;
    m_dir = dir;
    m_mtg = mtg;
    m_invt = invt;
    m_contact = contact;
    m_fCreateContact = false;
    m_fThisIsMe = false;

    this->SetTitle( _("Invite New") );

    InitGrid();

    Value_to_GUI();

    int nResp = ShowModal();
    if( nResp == wxID_OK )
    {
        GUI_to_Value();
        fOK = true;
    }
    return fOK;
}

bool CreateContactDialog::EditNewContact(  directory_t dir, meeting_details_t mtg, invitee_t invt, contact_t contact )
{
//    wxLogDebug(wxT("CreateContactDialog::EditNewContact(dir,mtg,invt,contact)"));
    bool fOK = false;
    m_dir = dir;
    m_mtg = mtg;
    m_invt = invt;
    m_contact = contact;
    m_fCreateContact = true;
    m_fThisIsMe = false;

    this->SetTitle( _("Manage Contacts") );

    InitGrid();

    Value_to_GUI();

    int nResp = ShowModal();
    if( nResp == wxID_OK )
    {
        GUI_to_Value();
        fOK = true;
    }

    return fOK;
}

bool CreateContactDialog::EditNewContact( directory_t dir, group_t group, contact_t contact, wxString & password, bool fThisIsMe )
{
//    wxLogDebug(wxT("CreateContactDialog::EditNewContact(dir,group,contact,p,fThisIsMe)"));
    bool fOK = false;
    m_dir = dir;
    m_mtg = 0;
    m_invt = 0;
    m_contact = contact;
    m_fThisIsMe = fThisIsMe;

    this->SetTitle( _("Manage Contacts") );

    m_password = password;

    m_fCreateContact = true;

    InitGrid();

    Value_to_GUI();

    int nResp = ShowModal();
    if( nResp == wxID_OK )
    {
        password = m_password;
        fOK = true;
    }

    return fOK;

}

bool CreateContactDialog::Validate()
{
    bool fValidData = true;

    // This seems silly, but the only required field is First Name (The in meeting name)
    wxString First;
    First = m_Grid->GetPropertyValueAsString( m_id_first );
    if( First.IsEmpty() || First.length() == 0 )
    {
        wxString msg( _("First name cannot be blank."), wxConvUTF8 );
        wxString caption( _("Meeting Setup Invitee"), wxConvUTF8 );
        wxMessageDialog Err(this, msg, caption );
        Err.ShowModal();
        fValidData = false;
        //PLF-TODO: Find a way to set focus on a grid cell activate the Cell's editor. Error processing requirement.
    }

    if( fValidData )
    {
        GUI_to_Value();
    }

    return fValidData;
}

void CreateContactDialog::InitGrid()
{
//    wxLogDebug(wxT("CreateContactDialog::InitGrid()"));

    wxString blank(wxT(""));

    int targetGridHeight = 0;
    int targetGridWidth = 0;
    int targetGridRows = 0;

    int proportion = 8;
    int border = 5;
    int flag = wxEXPAND | wxRIGHT | wxLEFT | wxTOP | wxBOTTOM;
    m_Grid = new wxPropertyGrid(
            m_GridPanel,     // parent
            XRCID("ID_PROPERTY_GRID"), // id
            wxDefaultPosition,  // position
            wxDefaultSize,      // size
            // Some specific window styles - for all additional styles,
            // see Modules->PropertyGrid Window Styles
            //wxPG_AUTO_SORT | // Automatic sorting after items added
            wxPG_SPLITTER_AUTO_CENTER | // Automatically center splitter until user manually adjusts it
            // Default style
            wxPG_DEFAULT_STYLE );

    wxBoxSizer *panelSizer = new wxBoxSizer(wxVERTICAL);

    panelSizer->Add( m_Grid, proportion, flag, border);

    m_GridPanel->SetSizer( panelSizer );

    // The only way to create a read only property is to diable it
    wxPropertyGrid *pg = m_Grid;

    if( m_fCreateContact )
    {
        wxString strAddressBook;
        if( m_dir )
        {
            strAddressBook = wxString(m_dir->name,wxConvUTF8);
        }
        else
        {
            strAddressBook = _("Personal contacts");
        }

        wxString    strXlated( wxGetTranslation( strAddressBook ) );
        m_id_addressbook = pg->Append( new wxStringProperty(_("Address book:"),wxT("dir"),strXlated) );
        pg->DisableProperty( m_id_addressbook );

//        wxString strGroup;
//        if( m_group )
//        {
//            strGroup = wxString( m_group->name, wxConvUTF8 );
//        }else
//        {
//            strGroup = _("My contacts and buddies");
//        }
//
//        m_id_group = pg->Append( new wxStringProperty(_("Group:"),wxT("group"),strGroup) );
//        pg->DisableProperty( m_id_group );
        
        m_id_screenname = pg->Append( new wxStringProperty(_("Screen name:"),wxT("screenname"),blank) );
        bool is_existing_contact = (m_contact->userid && *m_contact->userid);
        if( IsUserRecord( ) && is_existing_contact )
            pg->DisableProperty( m_id_screenname );

        if( IsUserRecord( )  &&  CONTROLLER.AdministratorMode( ) )
        {
            m_id_password = pg->Append( new wxStringProperty(_("Password:"),wxT("password"),blank) );
            pg->SetPropertyAttribute( wxT("password"), wxPG_STRING_PASSWORD, true );

            wxArrayString arrAdmin;
            wxArrayInt arrUserType;
            arrAdmin.Add(_("No"));    arrUserType.Add( UserTypeUser );
            arrAdmin.Add(_("Yes"));   arrUserType.Add( UserTypeAdmin );
            m_id_type = m_Grid->Append( new wxEnumProperty(_("Administrator:"),wxT("type"),arrAdmin, arrUserType ) );

            //  See if the Profiles have been downloaded - we need their names for the Profile: pulldown
            wxArrayString       arrProfile;
            profile_iterator_t  iter    = profiles_iterate( CONTROLLER.book );
            profile_t           profile = profiles_next( CONTROLLER.book, iter );
            if( profile == 0 )
            {
                //  It looks like the policies have not been fetched from the server - fetch
                //  them now, waiting for the download to finish
                wxString strOpid;
                strOpid = MeetingCenterController::opcode( MeetingCenterController::OP_ManageContacts, -1 ); // reuse another class' opid
                wxCharBuffer opid = strOpid.mb_str();

                //  Start the fetch of the Profiles, so we have their names when needed
                CONTROLLER.Connect( wxID_ANY, wxEVT_PROFILE_FETCHED, wxCommandEventHandler(CreateContactDialog::OnProfileFetched), 0, this );

                m_fProfileLoaded = false;
                addressbk_fetch_profiles( CONTROLLER.AddressBook(), opid );

                //  Wait for it...
                wxStopWatch     sw;
                long            start_time = sw.Time();
                long            timeout_ms = 15000;
                while( !m_fProfileLoaded )
                {
                    CUtils::MsgWait( 500, 1 );
                    wxGetApp().Yield();
                    if( (sw.Time() - start_time) > timeout_ms )
                    {
                        wxLogDebug( wxT("CreateContactDialog: Timeout while waiting for Profiles to download") );
                        break;
                    }
                }
                CONTROLLER.Disconnect( wxID_ANY, wxEVT_PROFILE_FETCHED, wxCommandEventHandler(CreateContactDialog::OnProfileFetched), 0, this );
            }
            iter = profiles_iterate( CONTROLLER.book );
            while( (profile = profiles_next(CONTROLLER.book, iter)) != 0 )
            {
               wxString name( profile->name, wxConvUTF8 );
               arrProfile.Add( name );
            }
            m_id_profile = m_Grid->Append( new wxEnumProperty(_("Policy:"),wxT("policy"),arrProfile) );
        }

        m_id_title = pg->Append( new wxStringProperty(_("Title:"),wxT("title"),blank) );
        m_id_first = pg->Append( new wxStringProperty(_("First name:"),wxT("first"),blank) );
        m_id_middle = pg->Append( new wxStringProperty(_("Middle name:"),wxT("middle"),blank) );
        m_id_last = pg->Append( new wxStringProperty(_("Last name:"),wxT("last"),blank) );
        m_id_suffix = pg->Append( new wxStringProperty(_("Suffix:"),wxT("suffix"),blank) );
        m_id_company = pg->Append( new wxStringProperty(_("Company:"),wxT("company"),blank) );
        m_id_jobtitle = pg->Append( new wxStringProperty(_("Job title:"),wxT("jobtitle"),blank) );
        m_id_address1 = pg->Append( new wxStringProperty(_("Address 1:"),wxT("address1"),blank) );
        m_id_address2 = pg->Append( new wxStringProperty(_("Address 2:"),wxT("address2"),blank) );
        m_id_city = pg->Append( new wxStringProperty(_("City:"),wxT("city"),blank) );
        m_id_state = pg->Append( new wxStringProperty(_("State:"),wxT("state"),blank) );
        m_id_country = pg->Append( new wxStringProperty(_("Country:"),wxT("country"),blank) );
        m_id_postalcode = pg->Append( new wxStringProperty(_("Postal code:"),wxT("postalcode"),blank) );
        m_id_email = pg->Append( new wxStringProperty(_("Email 1:"),wxT("email"),blank) );
        m_id_email2 = pg->Append( new wxStringProperty(_("Email 2:"),wxT("email2"),blank) );
        m_id_email3 = pg->Append( new wxStringProperty(_("Email 3:"),wxT("email3"),blank) );
        m_id_busphone = pg->Append( new wxStringProperty(_("Business phone:"),wxT("busphone"),blank) );
        m_id_homephone = pg->Append( new wxStringProperty(_("Home phone:"),wxT("homephone"),blank) );
        m_id_mobilephone = pg->Append( new wxStringProperty(_("Mobile phone:"),wxT("mobilephone"),blank) );
        m_id_otherphone = pg->Append( new wxStringProperty(_("Other phone:"),wxT("otherphone"),blank) );

        if( !m_contact->isbuddy )
        {
            wxArrayString arrFollowme;
            wxArrayInt arrFollowmeType;
            arrFollowme.Add(_("none"));              arrFollowmeType.Add( SelPhoneNone );
            arrFollowme.Add(_("Business phone"));    arrFollowmeType.Add( SelPhoneBus );
            arrFollowme.Add(_("Home phone"));        arrFollowmeType.Add( SelPhoneHome );
            arrFollowme.Add(_("Mobile phone"));      arrFollowmeType.Add( SelPhoneMobile );
            arrFollowme.Add(_("Other phone"));       arrFollowmeType.Add( SelPhoneOther );
            arrFollowme.Add(_("Web phone"));               arrFollowmeType.Add( SelPhonePC );
            m_id_followme   = pg->Append( new wxEnumProperty(_("Follow Me:"),wxT("defphone"),arrFollowme, arrFollowmeType ) );
        }

        m_id_aimscreen = m_Grid->Append( new wxStringProperty(_("AIM screen:"), wxT("aimscreen"), blank ) );
        m_id_yahooscreen = m_Grid->Append( new wxStringProperty(_("Yahoo screen:"), wxT("yahooscreen"), blank ) );
        m_id_msnscreen = m_Grid->Append( new wxStringProperty(_("Messenger screen:"), wxT("msnscreen"), blank ) );
        m_id_user1 = m_Grid->Append( new wxStringProperty(_("User 1:"),wxT("user1"),blank) );
        m_id_user2 = m_Grid->Append( new wxStringProperty(_("User 2:"),wxT("user2"),blank) );


        if( m_contact->readonly || (m_dir == CONTROLLER.GetCAB( )  &&  !CONTROLLER.AdministratorMode( )  &&  !m_fThisIsMe) )
        {
            //  non-admins cannot modify information in the CAB
//            pg->DisableProperty( m_id_password );
//            pg->DisableProperty( m_id_type );
//            pg->DisableProperty( m_id_profile );
            pg->DisableProperty( m_id_title );
            pg->DisableProperty( m_id_first );
            pg->DisableProperty( m_id_middle );
            pg->DisableProperty( m_id_last );
            pg->DisableProperty( m_id_suffix );
            pg->DisableProperty( m_id_company );
            pg->DisableProperty( m_id_jobtitle );
            pg->DisableProperty( m_id_address1 );
            pg->DisableProperty( m_id_address2 );
            pg->DisableProperty( m_id_city );
            pg->DisableProperty( m_id_state );
            pg->DisableProperty( m_id_country );
            pg->DisableProperty( m_id_postalcode );
            pg->DisableProperty( m_id_email );
            pg->DisableProperty( m_id_email2 );
            pg->DisableProperty( m_id_email3 );
            pg->DisableProperty( m_id_busphone );
            pg->DisableProperty( m_id_homephone );
            pg->DisableProperty( m_id_mobilephone );
            pg->DisableProperty( m_id_otherphone );
            if( !m_contact->isbuddy )
                pg->DisableProperty( m_id_followme );
//            pg->DisableProperty( m_id_ah_service );
//            pg->DisableProperty( m_id_ah_screen );
            pg->DisableProperty( m_id_screenname );
            pg->DisableProperty( m_id_aimscreen );
            pg->DisableProperty( m_id_msnscreen );
            pg->DisableProperty( m_id_yahooscreen );
            pg->DisableProperty( m_id_user1 );
            pg->DisableProperty( m_id_user2 );

            FindWindow(XRCID("wxID_OK"))->Disable( );
            wxString    strT = this->GetTitle( );
            strT.Append( wxString( _(" (read only)"), wxConvUTF8 ) );
            this->SetTitle( strT );
        }
        else
        {
            if ( IsUserRecord() )
                pg->SelectProperty(m_id_screenname);
            else
                pg->SelectProperty(m_id_first);
        }

        if( !m_fThisIsMe  ||  CONTROLLER.AdministratorMode( ) )
        {
            m_BtnChangePass->Disable( );
            m_BtnChangePass->Hide( );
        }
    }
    else
    {
        m_id_first = pg->Append( new wxStringProperty(_("Name:"),wxT("first"),blank) );
        m_id_email = pg->Append( new wxStringProperty(_("Email:"),wxT("email"),blank) );
        m_id_busphone = pg->Append( new wxStringProperty(_("Phone:"),wxT("busphone"),blank) );

        pg->SelectProperty(m_id_first);

        m_BtnChangePass->Disable( );
        m_BtnChangePass->Hide( );
    }

    targetGridRows = pg->GetChildrenCount();
    targetGridHeight = (targetGridRows * pg->GetRowHeight()) + 30;
    targetGridWidth = 480;
    wxSize targetSize( targetGridWidth, targetGridHeight );

    m_GridPanel->SetSize( targetSize );

    this->SetSize( targetSize );

    // Can't figure out any other way to get the initial grid size to the size we want it, so
    // set minimum grid size, then set dialog size, then reset minmum grid size so user can resize.
    m_GridPanel->SetMinSize(targetSize);
    wxSize minSize = GetSizer()->GetMinSize( );
    SetClientSize( minSize );
    m_GridPanel->SetMinSize(wxDefaultSize);

    this->Center();
}

#include <wx/tokenzr.h>

void CreateContactDialog::Value_to_GUI()
{
    wxASSERT(m_Grid);
    if( m_contact )
    {
        if( IsUserRecord( )  &&  CONTROLLER.AdministratorMode( ) )
        {
            m_type = m_contact->type;

            if( m_contact->profileid && *m_contact->profileid )
            {
                wxString profileid( m_contact->profileid, wxConvUTF8 );
                profile_iterator_t iter = profiles_iterate( CONTROLLER.book );
                profile_t profile = 0;
                while( (profile = profiles_next(CONTROLLER.book, iter)) != 0 )
                {
                    wxString checkId( profile->profile_id, wxConvUTF8 );
                    if( profileid.CmpNoCase( checkId ) == 0 )
                    {
                        m_profile = wxString( profile->name, wxConvUTF8 );
                        break;
                    }
                }
            }
            else
            {
                m_profile = wxT("");
            }
        }

        m_title = wxString( m_contact->title, wxConvUTF8 );
        m_first = wxString( m_contact->first, wxConvUTF8 );
        m_middle = wxString( m_contact->middle, wxConvUTF8 );
        m_last = wxString( m_contact->last, wxConvUTF8 );
        m_suffix = wxString( m_contact->suffix, wxConvUTF8 );
        m_company = wxString( m_contact->company, wxConvUTF8 );
        m_jobtitle = wxString( m_contact->jobtitle, wxConvUTF8 );
        m_address1 = wxString( m_contact->address1, wxConvUTF8 );
        m_address2 = wxString( m_contact->address2, wxConvUTF8 );
        m_city = wxString( m_contact->city, wxConvUTF8 );
        m_state = wxString( m_contact->state, wxConvUTF8 );
        m_country = wxString( m_contact->country, wxConvUTF8 );
        m_postalcode = wxString( m_contact->postalcode, wxConvUTF8 );
        m_email1 = wxString( m_contact->email, wxConvUTF8 );
        m_email2 = wxString( m_contact->email2, wxConvUTF8 );
        m_email3 = wxString( m_contact->email3, wxConvUTF8 );
        m_busphone = wxString( m_contact->busphone, wxConvUTF8 );
        m_homephone = wxString( m_contact->homephone, wxConvUTF8 );
        m_mobilephone = wxString( m_contact->mobilephone, wxConvUTF8 );
        m_otherphone = wxString( m_contact->otherphone, wxConvUTF8 );

        if( !m_contact->isbuddy )
            m_followme = m_contact->defphone;

        m_screenname = wxString( m_contact->screenname, wxConvUTF8 );
        CUtils::DecodeScreenName( m_screenname );
        m_aimscreen = wxString( m_contact->aimscreen, wxConvUTF8 );
        m_yahooscreen = wxString( m_contact->yahooscreen, wxConvUTF8 );
        m_msnscreen = wxString( m_contact->msnscreen, wxConvUTF8 );
        m_user1 = wxString( m_contact->user1, wxConvUTF8 );
        m_user2 = wxString( m_contact->user2, wxConvUTF8 );
    }
    else
    {
        // There is, at present, no contact record.
        // Gather the information that can be obtained from the invitee record.

        wxString fullname(wxT(""));
        fullname = wxString( m_invt->name, wxConvUTF8 );
        if( fullname.length() )
        {
            wxStringTokenizer tkz( fullname, wxT(" "));

            bool fOK = true;
            int iTok = 0;
            while ( fOK && tkz.HasMoreTokens() )
            {
                wxString token = tkz.GetNextToken();
                switch (iTok++)
                {
                case 0:  m_first = token; break;
                case 1:  m_last  = token; break;
                default: m_last += token; break;
                }
            }
        }

        m_busphone  = wxString( m_invt->phone, wxConvUTF8 );
        m_email1 = wxString( m_invt->email, wxConvUTF8 );
    }

    if( m_invt )
    {
        if( m_invt->notify_type | NotifyAIM )
        {
            m_nAdhocIM = NotifyAIM;
        }
        else if( m_invt->notify_type | NotifyYahoo )
        {
            m_nAdhocIM = NotifyYahoo;
        }
        else if( m_invt->notify_type | NotifyMessenger )
        {
            m_nAdhocIM = NotifyMessenger;
        }
        else
        {
            m_nAdhocIM = 0;
        }
    }

    // Populate the grid
    wxPropertyGrid *pg = m_Grid;

    if( m_fCreateContact )
    {
        if( IsUserRecord( )  &&  CONTROLLER.AdministratorMode( ) )
        {
            pg->SetPropertyValue( m_id_password, m_password );
            pg->SetPropertyValue( m_id_type, m_type );
            pg->SetPropertyValue( m_id_profile, m_profile );
        }
        pg->SetPropertyValue( m_id_title, m_title );
        pg->SetPropertyValue( m_id_first, m_first );
        pg->SetPropertyValue( m_id_middle, m_middle );
        pg->SetPropertyValue( m_id_last, m_last );
        pg->SetPropertyValue( m_id_suffix, m_suffix );
        pg->SetPropertyValue( m_id_company, m_company );
        pg->SetPropertyValue( m_id_jobtitle, m_jobtitle );
        pg->SetPropertyValue( m_id_address1, m_address1 );
        pg->SetPropertyValue( m_id_address2, m_address2 );
        pg->SetPropertyValue( m_id_city, m_city );
        pg->SetPropertyValue( m_id_state, m_state );
        pg->SetPropertyValue( m_id_country, m_country );
        pg->SetPropertyValue( m_id_postalcode, m_postalcode );
        pg->SetPropertyValue( m_id_email, m_email1 );
        pg->SetPropertyValue( m_id_email2, m_email2 );
        pg->SetPropertyValue( m_id_email3, m_email3 );
        pg->SetPropertyValue( m_id_busphone, m_busphone );
        pg->SetPropertyValue( m_id_homephone, m_homephone );
        pg->SetPropertyValue( m_id_mobilephone, m_mobilephone );
        pg->SetPropertyValue( m_id_otherphone, m_otherphone );

        if( !m_contact->isbuddy )
            pg->SetPropertyValue( m_id_followme, m_followme );

        m_screenname.MakeLower( );
        pg->SetPropertyValue( m_id_screenname, m_screenname );
        pg->SetPropertyValue( m_id_aimscreen, m_aimscreen );
        pg->SetPropertyValue( m_id_msnscreen, m_msnscreen );
        pg->SetPropertyValue( m_id_yahooscreen, m_yahooscreen );
        pg->SetPropertyValue( m_id_user1, m_user1 );
        pg->SetPropertyValue( m_id_user2, m_user2 );
    }
    else
    {
        pg->SetPropertyValue( m_id_first, m_first );
        pg->SetPropertyValue( m_id_email, m_email1 );
        pg->SetPropertyValue( m_id_busphone, m_busphone );
    }

}

void CreateContactDialog::GUI_to_Value()
{
    wxASSERT(m_Grid);
    wxPropertyGrid *pg = m_Grid;

    if( m_fCreateContact )
    {
        if( IsUserRecord( )  &&  CONTROLLER.AdministratorMode( ) )
        {
            m_password = pg->GetPropertyValueAsString( m_id_password );
            m_type = pg->GetPropertyValueAsLong( m_id_type );
            m_profile = pg->GetPropertyValueAsString( m_id_profile );
        }
        m_title = pg->GetPropertyValueAsString( m_id_title );
        m_first = pg->GetPropertyValueAsString( m_id_first );
        m_middle = pg->GetPropertyValueAsString( m_id_middle );
        m_last = pg->GetPropertyValueAsString( m_id_last );
        m_suffix = pg->GetPropertyValueAsString( m_id_suffix );
        m_company = pg->GetPropertyValueAsString( m_id_company );
        m_jobtitle = pg->GetPropertyValueAsString( m_id_jobtitle );
        m_address1 = pg->GetPropertyValueAsString( m_id_address1 );
        m_address2 = pg->GetPropertyValueAsString( m_id_address2 );
        m_city = pg->GetPropertyValueAsString( m_id_city );
        m_state = pg->GetPropertyValueAsString( m_id_state );
        m_country = pg->GetPropertyValueAsString( m_id_country );
        m_postalcode = pg->GetPropertyValueAsString( m_id_postalcode );
        m_email1 = pg->GetPropertyValueAsString( m_id_email );
        m_email2 = pg->GetPropertyValueAsString( m_id_email2 );
        m_email3 = pg->GetPropertyValueAsString( m_id_email3 );
        m_busphone = pg->GetPropertyValueAsString( m_id_busphone );
        m_homephone = pg->GetPropertyValueAsString( m_id_homephone );
        m_mobilephone = pg->GetPropertyValueAsString( m_id_mobilephone );
        m_otherphone = pg->GetPropertyValueAsString( m_id_otherphone );

        if( !m_contact->isbuddy )
            m_followme = pg->GetPropertyValueAsLong( m_id_followme );

        m_screenname = pg->GetPropertyValueAsString( m_id_screenname );
        CUtils::EncodeScreenName( m_screenname );
        m_aimscreen = pg->GetPropertyValueAsString( m_id_aimscreen );
        m_yahooscreen = pg->GetPropertyValueAsString( m_id_yahooscreen );
        m_msnscreen = pg->GetPropertyValueAsString( m_id_msnscreen );
        m_user1 = pg->GetPropertyValueAsString( m_id_user1 );
        m_user2 = pg->GetPropertyValueAsString( m_id_user2 );
    }
    else
    {
        m_first = pg->GetPropertyValueAsString( m_id_first );
        m_email1 = pg->GetPropertyValueAsString( m_id_email );
        m_busphone = pg->GetPropertyValueAsString( m_id_busphone );
    }

    if( m_contact )
    {
        if( IsUserRecord( )  &&  CONTROLLER.AdministratorMode( ) )
        {
            m_contact->type = m_type;

            if( !m_profile.IsEmpty() )
            {
                profile_iterator_t iter = profiles_iterate( CONTROLLER.book );
                profile_t profile = 0;
                while( (profile = profiles_next(CONTROLLER.book, iter)) != 0 )
                {
                   wxString checkName( profile->name, wxConvUTF8 );
                    if( m_profile.CmpNoCase( checkName ) == 0 )
                    {
                        wxString profile_id( profile->profile_id, wxConvUTF8 );
                        m_contact->profileid = Alloc_dir_pstring( profile_id );
                        break;
                    }
                }
            }
        }

        m_contact->title = Alloc_dir_pstring( m_title );
        m_contact->first = Alloc_dir_pstring( m_first );
        m_contact->middle = Alloc_dir_pstring( m_middle );
        m_contact->last = Alloc_dir_pstring( m_last );
        m_contact->suffix = Alloc_dir_pstring( m_suffix );
        m_contact->company = Alloc_dir_pstring( m_company );
        m_contact->jobtitle = Alloc_dir_pstring( m_jobtitle );
        m_contact->address1 = Alloc_dir_pstring( m_address1 );
        m_contact->address2 = Alloc_dir_pstring( m_address2 );
        m_contact->city = Alloc_dir_pstring( m_city );
        m_contact->state = Alloc_dir_pstring( m_state );
        m_contact->country = Alloc_dir_pstring( m_country );
        m_contact->postalcode = Alloc_dir_pstring( m_postalcode );
        m_contact->email = Alloc_dir_pstring( m_email1 );
        m_contact->email2 = Alloc_dir_pstring( m_email2 );
        m_contact->email3 = Alloc_dir_pstring( m_email3 );
        m_contact->busphone = Alloc_dir_pstring( m_busphone );
        m_contact->homephone = Alloc_dir_pstring( m_homephone );
        m_contact->mobilephone = Alloc_dir_pstring( m_mobilephone );
        m_contact->otherphone = Alloc_dir_pstring( m_otherphone );

        if( !m_contact->isbuddy )
            m_contact->defphone = m_followme;

        m_screenname.MakeLower( );
        m_contact->screenname = Alloc_dir_pstring( m_screenname );
        m_contact->aimscreen = Alloc_dir_pstring( m_aimscreen );
        m_contact->msnscreen = Alloc_dir_pstring( m_msnscreen );
        m_contact->yahooscreen = Alloc_dir_pstring( m_yahooscreen );
        m_contact->user1 = Alloc_dir_pstring( m_user1 );
        m_contact->user2 = Alloc_dir_pstring( m_user2 );
    }

    if( m_invt )
    {
        wxString name;
        name = m_first;
        name += wxT(" ");
        name += m_last;
        m_invt->name = Alloc_invt_pstring( name );
        m_invt->selphone = SelPhoneThis;
        m_invt->phone = Alloc_invt_pstring( m_busphone );
        m_invt->selemail = SelEmailThis;
        m_invt->email = Alloc_invt_pstring( m_email1 );
    }
}


//*****************************************************************************
void CreateContactDialog::OnClickHelp(wxCommandEvent& event)
{
    wxString help( wxGetApp().Options().GetSystem( "HelpPath", "" ),  wxConvUTF8);
    help += LOCALE->GetCanonicalName( );
    help += wxT("/");
    if( m_fThisIsMe )
        help += wxT("configuringcontactinfo.htm");
    else
        help += wxT("editing_a_contacts_information.htm");
    wxLaunchDefaultBrowser( help );
}


//*****************************************************************************
void CreateContactDialog::OnClickChangePass( wxCommandEvent& event )
{
    wxPasswordEntryDialog   dlg( this, _("Enter a new password"), _("Change Password"), _T(""), wxOK | wxCANCEL );
    if( dlg.ShowModal( )  ==  wxID_OK )
    {
        wxString    strPass;
        strPass = dlg.GetValue( ).wc_str( );
        wxLogDebug( wxT("  a new password was entered: *%s*"), strPass.wc_str( ) );

        session_change_password( CONTROLLER.session, strPass.mb_str( wxConvUTF8 ) );
    }
}


//*****************************************************************************
void CreateContactDialog::OnProfileFetched(wxCommandEvent& event)
{
    wxString opid;
    opid = event.GetString();

    wxLogDebug(wxT("CreateContactDialog::OnProfileFetched opid=%s"),opid.c_str() );

    if( !CONTROLLER.IsOpcode( opid, MeetingCenterController::OP_ManageContacts, false /* Not exact match */ ) )
        event.Skip();

    m_fProfileLoaded = true;
}
