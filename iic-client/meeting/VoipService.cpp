/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "wx/wx.h"
#include "VoipService.h"
#include "VoipApi.h"
//#include "../netlib/logger.h" // Replaced with wxLogDebug
//#include "utils.h"

CVoipEvents *CVoipService::s_events = 0;

CVoipService::CVoipService(CVoipEvents * events)
{
	s_events = events;
	VoipSetOnRinging(OnRingingHandler);
	VoipSetOnEstablished(OnEstablishedHandler);
	VoipSetOnCleared(OnClearedHandler);
	VoipSetLogger(Logger);
	VoipSetLevelCallback(OnLevels);
}

CVoipService::~CVoipService(void)
{
	VoipShutdown();
}

int CVoipService::Initialize(const char *dir)
{
    // Library logging goes to stderr; we'll want to capture that and write to a file
    // in our user directory.
    
	return VoipInitialize(dir);
}

int CVoipService::Shutdown()
{
	return VoipShutdown();
}

int CVoipService::SetOption(const char * option, const char * value)
{
    wxString strOption( option, wxConvUTF8 );
    wxString strValue( value, wxConvUTF8 );
	wxLogDebug( wxT("Setting option %s to %s"), strOption.c_str(), strValue.c_str() );
	return VoipSetOption(option, value);
}

int CVoipService::MakeCall(const char * from_user, const char * to_address)
{
    wxString strFrom_user( from_user, wxConvUTF8 );
    wxString strTo_address( to_address, wxConvUTF8 );

	wxLogDebug( wxT("CVoipService::MakeCall - Starting VOIP call from %s to %s"), strFrom_user.c_str(), strTo_address.c_str() );

	int nRet = VoipMakeCall(from_user, to_address);

	wxLogDebug( wxT("CVoipService::MakeCall - Returned %d"), nRet );

	return nRet;
}

int CVoipService::Hangup()
{
	wxLogDebug(wxT("Hanging up VOIP call"));
	return VoipHangup();
}

int CVoipService::Dial(const char * str)
{
	return VoipDial(str);
}

int CVoipService::MuteMicrophone(bool muted)
{
    return VoipMute(muted ? TRUE : FALSE);
}

int CVoipService::MuteSpeaker(bool muted)
{
    return VoipSpeakerMute(muted ? TRUE : FALSE);
}

bool CVoipService::IsInCall()
{
	return VoipIsInCall();
}

void CVoipService::OnRingingHandler(int status)
{
    wxLogDebug(wxT("CVoipService::OnRingingHandler"));
	if (s_events)
		s_events->OnRinging(status);
}

void CVoipService::OnEstablishedHandler(int status)
{
    wxLogDebug(wxT("CVoipService::OnEstablishedHandler"));
	if (s_events)
		s_events->OnEstablished(status);
}

void CVoipService::OnClearedHandler(int status)
{
    wxLogDebug(wxT("CVoipService::OnClearedHandler"));
	if (s_events)
		s_events->OnCleared(status);
}

void CVoipService::Logger(int level, const char * msg)
{
	switch (level) {
		case IICVOIP_DEBUG:
			wxLogDebug(wxT("Voip Service debug: %s"), wxString(msg, wxConvUTF8).c_str());
			break;
		case IICVOIP_ERROR:
			wxLogDebug(wxT("Voip Service error: %s"), wxString(msg, wxConvUTF8).c_str());
			break;
		case IICVOIP_INFO:
			wxLogDebug(wxT("Voip Service info: %s"), wxString(msg, wxConvUTF8).c_str());
			break;
		case IICVOIP_WARN:
		default:
			wxLogDebug(wxT("Voip Service warning: %s"), wxString(msg, wxConvUTF8).c_str());
			break;
	}
}
void CVoipService::OnLevels(float input, float output)
{
    //wxLogDebug(wxT("CVoipService::OnLevels %f %f"),input,output);
	if (s_events)
		s_events->OnLevels(input, output);
}

const wxString & CVoipService::GetErrorText( int errorCode )
{
    wxString errorText;

    m_strLastErrorText = wxString::Format(_("Unknown VOIP error %d"), errorCode );

    switch( errorCode )
    {
    case IICVOIP_OK:        errorText = _("No error"); break;
    case IICVOIP_FAIL:      errorText = _("Connection failure"); break;
    case IICVOIP_NOTINIT:   errorText = _("VOIP Service not initialized"); break;
    case IICVOIP_CALLACTIVE:errorText = _("A VOIP connection is already in progress."); break;
    };

    return m_strLastErrorText;
}

