/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"

#if !wxUSE_DOC_VIEW_ARCHITECTURE
#error You must set wxUSE_DOC_VIEW_ARCHITECTURE to 1 in setup.h!
#endif


#include "meetingcentercontroller.h"
#include "iicdoc.h"
#include "docshareview.h"

#include <wx/uri.h>

#ifndef WIN32
#include "netlib/nethttp.h"      //  for definition of INTERNET_DEFAULT_HTTPS_PORT
#endif

#include "cryptlib/cryptlib.h"

IMPLEMENT_DYNAMIC_CLASS(CIICDoc, wxDocument)


//*****************************************************************************
CIICDoc::CIICDoc( void )
{
    wxLogDebug( wxT("entering IICDOC CONSTRUCTOR") );

    m_Meeting.SetIsUserPresenter( false );
    m_Meeting.SetIsPresenting( false );
    m_Meeting.SetIsInProgress( false );
}


//*****************************************************************************
CIICDoc::~CIICDoc( void )
{
    wxLogDebug( wxT("entering IICDOC DESTRUCTOR") );
}


//*****************************************************************************
void CIICDoc::GetSessionInfo( wxString &aScreenName, wxString &aPassword, wxString &aServer )
{
    m_Meeting.GetMeetingUserID( aScreenName );
    aPassword   = m_Meeting.GetPassword( );
    aServer     = m_Meeting.GetDocShareServer( );

    // If an anonymous user is trying to access DocShare, we need to auth with user name as the controller knows it:
    // anonymous@server/mtgXXXXXXXXXX...
    // ToDo: continue to support old server without resource
    if (CONTROLLER.AnonymousMode())
        CONTROLLER.GetControllerUsername(aScreenName);
}


//*****************************************************************************
void CIICDoc::SetSessionInfo( wxString &aScreenName, wxString &aPassword, wxString &aServer )
{
    m_Meeting.SetMeetingUserID( aScreenName );
    m_Meeting.SetPassword( aPassword );


    wxString        strHost;
    wxString        strScheme;
    wxString        strPath;
    unsigned short  usPort;
    wxURI           uri( aServer );

    if( uri.HasServer( ) )
        strHost = uri.GetServer( );
    if( uri.HasScheme( ) )
        strScheme = uri.GetScheme( );
    if( uri.HasPath( ) )
        strPath = uri.GetPath( );
    if( uri.HasPort( ) )
        usPort = wxAtoi( uri.GetPort( ) );
    else
    {
        if( strScheme.CmpNoCase( wxT("HTTPS") )  == 0 )
            usPort = INTERNET_DEFAULT_HTTPS_PORT;       //  use the default port for HTTPS if scheme is HTTPS
        else
            usPort = INTERNET_DEFAULT_HTTP_PORT;        //  use the default port for HTTP if none is otherwise specified
    }

    m_Meeting.SetDocSharePath( strPath );
    strScheme.Append( wxT("://") );
    strScheme.Append( strHost );
    strScheme.Append( wxT("/") );
    m_Meeting.SetDocShareServer( strScheme );

#ifdef __WXMSW__
    TCHAR           server[512];
    TCHAR           szHost[256];
    TCHAR           szScheme[64];
    TCHAR           szPath[512];
    URL_COMPONENTS  url;
    memset( &url, 0, sizeof(url) );
    url.dwStructSize = sizeof( url );
    url.dwHostNameLength = 256;
    url.lpszHostName = szHost;
    url.dwSchemeLength = 64;
    url.lpszScheme = szScheme;
    url.dwUrlPathLength = 512;
    url.lpszUrlPath = szPath;

    _tcscpy( server, aServer.wc_str( ) );
    if( InternetCrackUrl( server, _tcslen( server), ICU_DECODE, &url ) )
    {
        wxString    strT( szPath );
        m_Meeting.SetDocSharePath( strT );
    }
    else
    {
        wxString    strT;
        m_Meeting.SetDocSharePath( strT );
    }
#endif      //  #ifdef __WXMSW__
}


//*****************************************************************************
void CIICDoc::SendDrawOnWhiteBoard( wxString& meetingId, const wxString& actionXML)
{
    wxLogDebug( wxT("entering CIICDoc::SendDrawOnWhiteBoard( )  --  mtgID = %s"), meetingId.wc_str( ) );
    
    std::string      s	= (const char *) actionXML.mb_str( wxConvUTF8 );
    const char  *inbuf  = s.c_str();
    
    // pad input to mod BLOWFISH_BLOCKSIZE
    int length = strlen((const char*) inbuf);
    int bufLen = length + (BLOWFISH_BLOCKSIZE - (length % BLOWFISH_BLOCKSIZE));
    
    char* buf = new char[bufLen];
    memset(buf, 0, bufLen);
    strcpy(buf, (const char*) inbuf);
    
    // encrypt the string before sending it out
    CBlowFish   blowfish;
    blowfish.Initialize( BLOWFISH_KEY, sizeof(BLOWFISH_KEY) );
    blowfish.Encode( (unsigned char*) buf, (unsigned char*) buf, bufLen );
    
    // base 64 encode
    CBase64 base64Encoder;
    char	*encryptedValue;
    encryptedValue = base64Encoder.Encode( buf, bufLen );
    
    int iRet;
    iRet = controller_docshare_draw( CONTROLLER.controller, meetingId.mb_str( wxConvUTF8 ), encryptedValue );
    wxLogDebug( wxT("    controller_docshare_draw( ) returned %d"), iRet );
    
    delete [ ] buf;
    delete [ ] encryptedValue;
}

