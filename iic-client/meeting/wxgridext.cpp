/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 *
 *
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */
#include "wx_pch.h"
#include "wxgridext.h"
#include "utils.h"

#ifdef __WXGTK__
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#endif

#include <wx/textdlg.h>
#include <wx/generic/grid.h>
#include <wx/settings.h>
#include <wx/dynlib.h>

#include "imagelistcomposite.h"

IMPLEMENT_DYNAMIC_CLASS(GridCellComboIntEditor, wxSheetCellChoiceEditorRefData)
IMPLEMENT_DYNAMIC_CLASS(GridCellImageRenderer, wxSheetCellBitmapRendererRefData )
IMPLEMENT_DYNAMIC_CLASS(GridCellTextEditor, wxSheetCellTextEditorRefData)
IMPLEMENT_DYNAMIC_CLASS(GridCellBoolEditor, wxSheetCellBoolEditorRefData)



//////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __WXMSW__

#include "wx/msw/ole/oleutils.h"
#include <shldisp.h>

#if defined(__MINGW32__)
    // needed for IID_IAutoComplete, IID_IAutoComplete2 and ACO_AUTOSUGGEST
    #include <shlguid.h>
#endif

#ifndef ACO_AUTOAPPEND
    #define ACO_AUTOAPPEND 0x2
#endif
#ifndef ACO_USETAB
    #define ACO_USETAB 0x10
#endif
#ifndef ACO_UPDOWNKEYDROPSLIST
    #define ACO_UPDOWNKEYDROPSLIST 0x20
#endif

DEFINE_GUID(CLSID_AutoComplete,
    0x00bb2763, 0x6a77, 0x11d0, 0xa5, 0x35, 0x00, 0xc0, 0x4f, 0xd7, 0xd0, 0x62);


class wxIEnumString : public IEnumString
{
public:
    wxIEnumString(const wxArrayString& strings) : m_strings(strings)
    {
        m_index = 0;
    }

    DECLARE_IUNKNOWN_METHODS;

    virtual HRESULT STDMETHODCALLTYPE Next(ULONG celt,
                                           LPOLESTR *rgelt,
                                           ULONG *pceltFetched)
    {
        if ( !rgelt || (!pceltFetched && celt > 1) )
            return E_POINTER;

        ULONG pceltFetchedDummy;
        if ( !pceltFetched )
            pceltFetched = &pceltFetchedDummy;

        *pceltFetched = 0;

        for ( const unsigned count = m_strings.size(); celt--; ++m_index )
        {
            if ( m_index == count )
                return S_FALSE;

            const wxWX2WCbuf wcbuf = m_strings[m_index].wc_str();
            const size_t size = (wcslen(wcbuf) + 1)*sizeof(wchar_t);
            void *olestr = CoTaskMemAlloc(size);
            if ( !olestr )
                return E_OUTOFMEMORY;

            memcpy(olestr, wcbuf, size);

            *rgelt++ = static_cast<LPOLESTR>(olestr);

            ++(*pceltFetched);
        }

        return S_OK;
    }

    virtual HRESULT STDMETHODCALLTYPE Skip(ULONG celt)
    {
        m_index += celt;
        if ( m_index > m_strings.size() )
        {
            m_index = m_strings.size();
            return S_FALSE;
        }

        return S_OK;
    }

    virtual HRESULT STDMETHODCALLTYPE Reset()
    {
        m_index = 0;

        return S_OK;
    }

    virtual HRESULT STDMETHODCALLTYPE Clone(IEnumString **ppEnum)
    {
        if ( !ppEnum )
            return E_POINTER;

        wxIEnumString *e = new wxIEnumString(m_strings);
        e->m_index = m_index;

        e->AddRef();
        *ppEnum = e;

        return S_OK;
    }

private:
    // dtor doesn't have to be virtual as we're only ever deleted from our own
    // Release() and are not meant to be derived form anyhow, but making it
    // virtual silences gcc warnings; making it private makes it impossible to
    // (mistakenly) delete us directly instead of calling Release()
    virtual ~wxIEnumString() { }


    const wxArrayString m_strings;
    unsigned m_index;

    DECLARE_NO_COPY_CLASS(wxIEnumString)
};

BEGIN_IID_TABLE(wxIEnumString)
    ADD_IID(Unknown)
    ADD_IID(EnumString)
END_IID_TABLE;

// Not exported from wxWidgets, so copy here
bool IsIidFromList(REFIID riid, const IID *aIids[], size_t nCount)
{
  for ( size_t i = 0; i < nCount; i++ ) {
    if ( riid == *aIids[i] )
      return true;
  }

  return false;
}

IMPLEMENT_IUNKNOWN_METHODS(wxIEnumString)



// I believe that autocompletion is subclassing the control and handling tab characters itself.
// So, in order to trap tab characters, I've got to subclass and capture tab characters myself.
LRESULT CALLBACK GridCellTextEditor::TextEditSubClass(HWND hWnd, UINT uMsg, WPARAM wParam,
                                                      LPARAM lParam, UINT_PTR uIdSubclass, DWORD_PTR dwRefData)
{
    switch (uMsg)
    {
        case WM_GETDLGCODE:
/*        {
            MSG *msg = (MSG*)lParam;
            if (msg != NULL && msg->message == WM_CHAR && msg->wParam == VK_TAB)
            {
                // Handle tab character
                wxWindow *wnd = (wxWindow*)dwRefData;
                wxKeyEvent key(wxEVT_KEY_DOWN);
                key.m_keyCode = WXK_TAB;
                wnd->AddPendingEvent(key);
            }
        }
 */       
        case WM_CHAR:
        {
            if (wParam == VK_TAB)
            {
                wxWindow *wnd = (wxWindow*)dwRefData;
                wxKeyEvent key(wxEVT_KEY_DOWN);
                key.m_keyCode = WXK_TAB;
                wnd->AddPendingEvent(key);
                return 0;
            }
        }
    } 
    return m_DefSubclassProc(hWnd, uMsg, wParam, lParam);
}

bool GridCellTextEditor::m_LoadedFuncs = false;
DefSubclassProcPtr GridCellTextEditor::m_DefSubclassProc = NULL;
SetWindowSubclassPtr GridCellTextEditor::m_SetWindowSubclass = NULL;

#endif


void GridCellTextEditor::CreateEditor(wxWindow* parent,
                                      wxWindowID id,
                                      wxEvtHandler* evtHandler,
                                      wxSheet* sheet)
{
#ifdef __WXMSW__
    if (!m_LoadedFuncs)
    {
        // Dynamically load funcs so we are not dependent on comctl32
        // (also DefSubclassProc is missing in mingw lib)
        wxDynamicLibrary lib(wxT("comctl32.dll"));
        if (lib.IsLoaded())
        {
            m_SetWindowSubclass = (SetWindowSubclassPtr)lib.GetSymbol(wxT("SetWindowSubclass"));
            m_DefSubclassProc = (DefSubclassProcPtr)lib.GetSymbol(wxT("DefSubclassProc"));
            lib.Detach();
        }
        m_LoadedFuncs = true;
    }
#endif
    
    // N.B. we add the "i want tabs" flag here, but once autocomplete is enabled, we won't get them anyway.
    // Tabs are used to autocomplete the text entry instead.  Use enter to close the edit field, then tab.
    SetControl( new wxTextCtrl(parent, id, wxEmptyString,
                               wxDefaultPosition, wxDefaultSize,
                               wxTE_PROCESS_ENTER | wxTE_PROCESS_TAB) );

    wxSheetCellEditorRefData::CreateEditor(parent, id, evtHandler, sheet);
}

void GridCellTextEditor::DestroyControl()
{
    DestroyAutoList();
    wxSheetCellTextEditorRefData::DestroyControl();
}

void GridCellTextEditor::BeginEdit(const wxSheetCoords& coords, wxSheet* sheet)
{
    wxSheetCellTextEditorRefData::BeginEdit(coords, sheet);

    if (!m_autoNames)
        return;

#ifdef __WXMSW__
    // create an object exposing IAutoComplete interface (don't go for
    // IAutoComplete2 immediately as, presumably, it might be not available on
    // older systems as otherwise why do we have both -- although in practice I
    // don't know when can this happen)
    IAutoComplete *pAutoComplete = NULL;
    HRESULT hr = CoCreateInstance
                 (
                    CLSID_AutoComplete,
                    NULL,
                    CLSCTX_INPROC_SERVER,
                    IID_IAutoComplete,
                    reinterpret_cast<void **>(&pAutoComplete)
                 );
    if ( FAILED(hr) )
    {
        wxLogApiError(_T("CoCreateInstance(CLSID_AutoComplete)"), hr);
        return;
    }

    // associate it with our strings
    wxIEnumString *pEnumString = new wxIEnumString(*m_autoNames);
    pEnumString->AddRef();
    hr = pAutoComplete->Init((HWND)GetTextCtrl()->GetHandle(), pEnumString, NULL, NULL);
    pEnumString->Release();
    if ( FAILED(hr) )
    {
        wxLogApiError(_T("IAutoComplete::Init"), hr);
        return;
    }

    // if IAutoComplete2 is available, set more user-friendly options
    IAutoComplete2 *pAutoComplete2 = NULL;
    hr = pAutoComplete->QueryInterface
                        (
                           IID_IAutoComplete2,
                           reinterpret_cast<void **>(&pAutoComplete2)
                        );
    if ( SUCCEEDED(hr) )
    {
        pAutoComplete2->SetOptions(ACO_AUTOSUGGEST | ACO_UPDOWNKEYDROPSLIST);
        pAutoComplete2->Release();
    }

    // the docs are unclear about when can we release it but it seems safe to
    // do it immediately, presumably the edit control itself keeps a reference
    // to the auto completer object
    pAutoComplete->Release();

    // Subclass window proc if possible
    if (m_SetWindowSubclass && m_DefSubclassProc)
        m_SetWindowSubclass((HWND)GetTextCtrl()->GetHandle(), TextEditSubClass, 0x1, (DWORD_PTR)sheet);

#elif __WXGTK__

    // This only works for single line entry fields
    GtkEntry * const entry = GTK_ENTRY(GetTextCtrl()->GetHandle());

    GtkListStore * const store = gtk_list_store_new(1, G_TYPE_STRING);
    GtkTreeIter iter;

    for ( wxArrayString::const_iterator i = m_autoNames->begin();
          i != m_autoNames->end();
          ++i )
    {
        gtk_list_store_append(store, &iter);
        gtk_list_store_set(store, &iter,
                           0, (const gchar *)i->utf8_str(),
                           -1);
    }

    GtkEntryCompletion * const completion = gtk_entry_completion_new();
    gtk_entry_completion_set_model(completion, GTK_TREE_MODEL(store));
    gtk_entry_completion_set_text_column(completion, 0);
#if GTK_CHECK_VERSION(2,12,0)
    gtk_entry_completion_set_inline_selection(completion, TRUE);
#endif
    gtk_entry_set_completion(entry, completion);
    g_object_unref(completion);

#else

    // No platform autocomplete available, use list control as popup
    m_autoList = new GridCellTextPopup(::wxGetTopLevelParent(sheet), -1, wxDefaultPosition, wxDefaultSize, 0, NULL);
    m_autoList->Show(false);
    m_autoList->Append(wxT(""));
    m_listHandler.Connect(this, sheet, m_autoList);

#endif
}

bool GridCellTextEditor::EndEdit(const wxSheetCoords& coords, wxSheet* sheet)
{
    DestroyAutoList();
	return wxSheetCellTextEditorRefData::EndEdit(coords, sheet);
}

bool GridCellTextEditor::OnKeyDown(wxKeyEvent& event)
{
#if !defined(__WXMSW__) && !defined(__WXGTK__)
    int ch;

#if wxUSE_UNICODE
    ch = event.GetUnicodeKey();
    if (ch <= 127)
        ch = event.GetKeyCode();
#else
    ch = event.GetKeyCode();
#endif
    if (ch == WXK_DOWN || ch == WXK_UP)
    {
        m_autoList->SetFocus();
        if (m_autoList->GetSelection() == wxNOT_FOUND)
        {
            m_autoList->SetSelection(0);
            GetTextCtrl()->SetValue(m_autoList->GetStringSelection());
        }
        return false;
    }
    else if (ch >= ' ' && ch < 127) 
    {
        ShowAutoList();
    }

    m_autoList->Clear();
    m_timer.Start(200, true);

#endif
    return wxSheetCellTextEditorRefData::OnKeyDown(event);
}

bool GridCellTextEditor::OnChar(wxKeyEvent& event)
{
    return wxSheetCellTextEditorRefData::OnChar(event);
}

void GridCellTextEditor::AutoComplete(const wxArrayString& choices)
{
    m_autoNames = &choices;
}

void GridCellTextEditor::ShowAutoList()
{
#if !defined(__WXMSW__) && !defined(__WXGTK__)
    wxRect editRect = GetControl()->GetScreenRect();
    wxPoint top = editRect.GetBottomLeft();
    top = ::wxGetTopLevelParent(m_autoList)->ScreenToClient(top);
    m_autoList->SetSize(top.x, top.y + 2, editRect.GetWidth() - 2, 20);
#endif
}

void GridCellTextEditor::DestroyAutoList()
{
#if !defined(__WXMSW__) && !defined(__WXGTK__)
    m_timer.Stop();
    if (m_autoList)
    {
        m_autoList->Show(false);
        m_autoList->Destroy();
        m_autoList = NULL;
    }
#endif
}

void GridCellTextEditor::FillMatches(const wxString& value)
{
    int size = 0;

    if (value.Length() > 0)
    {
        // TODO: list is sorted, search more efficiently
        for (int i=0; i<m_autoNames->Count() && size<12; i++) {
            if (m_autoNames->Item(i).StartsWith(value))
            {
                m_autoList->Append(m_autoNames->Item(i));
                ++size;
            }
        }
    }
    
    if (size > 0)
    {
        wxSize listSize = m_autoList->GetSize();
        listSize.SetHeight(size*26);
        m_autoList->SetSize(listSize);
        m_autoList->Show();
        m_autoList->Update();
    }
    else
        m_autoList->Show(false);
    
}

GridCellTextEditorEvtHandler::GridCellTextEditorEvtHandler():
m_sheet(0), m_pEditor(0), m_autoList(0)
{

}

void GridCellTextEditorEvtHandler::Connect(GridCellTextEditor *pEditor, wxSheet *pSheet, GridCellTextPopup *pAutoList)
{
    m_pEditor = pEditor;
    m_autoList = pAutoList;
    m_sheet = pSheet;
    m_autoList->Connect(wxID_ANY, wxEVT_KEY_DOWN, wxKeyEventHandler(GridCellTextEditorEvtHandler::OnListChar), 0, this);
    m_autoList->GetParent()->Connect(wxID_ANY, wxEVT_COMMAND_LISTBOX_SELECTED, wxCommandEventHandler(GridCellTextEditorEvtHandler::OnListSelected), 0, this);

    wxEvtHandler::Connect(wxID_ANY, wxEVT_TIMER, wxTimerEventHandler(GridCellTextEditorEvtHandler::OnTimer), 0, this);
}

void GridCellTextEditorEvtHandler::OnListChar(wxKeyEvent& event)
{
    switch (event.GetKeyCode())
    {
        case WXK_TAB:
        case WXK_RETURN:
            m_sheet->GetEventHandler()->ProcessEvent(event);
            break;
        case WXK_ESCAPE:
            m_sheet->DisableCellEditControl(false);
            break;
        default:
            event.Skip();
    }
}

void GridCellTextEditorEvtHandler::OnListSelected(wxCommandEvent& event)
{
    wxString val = m_autoList->GetStringSelection();
    m_pEditor->GetTextCtrl()->SetValue(val);
}

void GridCellTextEditorEvtHandler::OnTimer(wxTimerEvent& event)
{
    wxString val = m_pEditor->GetTextCtrl()->GetValue();
    m_pEditor->FillMatches(val);
}


//////////////////////////////////////////////////////////////////////////////////////////////


GridCellComboIntEditor::GridCellComboIntEditor(size_t count, const wxString choices[], bool allowOthers )
:wxSheetCellChoiceEditorRefData(count,choices,allowOthers),
m_grid(0),
m_parentCustomEntry(0),
m_nCustomEntryTrigger(-1),
m_nCustomEntryIntValue(-1)
{
    if( count > 0 )
    {
        m_intChoices.Alloc( count );
        for( size_t i = 0; i < count; ++i )
        {
            m_choices.Add( choices[i] );
            m_intChoices[i] = -1;
        }
    }
}

GridCellComboIntEditor::GridCellComboIntEditor(const wxArrayString& choices, const wxArrayInt & intChoices, bool allowOthers )
:wxSheetCellChoiceEditorRefData(choices,allowOthers),
m_grid(0),
m_intChoices(intChoices),
m_parentCustomEntry(0),
m_nCustomEntryTrigger(-1),
m_nCustomEntryIntValue(-1)
{
    m_pointActivate.x = -1;
    m_pointActivate.y = -1;
}

bool GridCellComboIntEditor::Copy(const GridCellComboIntEditor& other)
{
    m_intChoices = other.m_intChoices;
    m_parentCustomEntry = other.m_parentCustomEntry;
    m_strCustomEntryCaption = other.m_strCustomEntryCaption;
    m_strCustomEntryPrompt = other.m_strCustomEntryPrompt;
    m_nCustomEntryTrigger = other.m_nCustomEntryTrigger;
    m_nCustomEntryIntValue = other.m_nCustomEntryIntValue;
    m_pointActivate = other.m_pointActivate;

    return wxSheetCellChoiceEditorRefData::Copy( other );
}

void GridCellComboIntEditor::Show(bool show, const wxSheetCellAttr &attr)
{
    wxLogDebug(wxT("GridCellComboIntEditor::Show( %d )"), show ? 1:0 );
    wxSheetCellChoiceEditorRefData::Show( show, attr );
}

void GridCellComboIntEditor::CreateEditor(wxWindow* parent, wxWindowID id, wxEvtHandler* evtHandler, wxSheet* sheet)
{
    const size_t count = m_choices.GetCount();
    wxString *choices = new wxString[count];
    for ( size_t n = 0; n < count; n++ )
        choices[n] = m_choices[n];

    SetControl( new wxComboBox(parent, id, wxEmptyString,
                               wxDefaultPosition, wxDefaultSize,
                               count, choices,
                               wxTE_PROCESS_ENTER | wxWANTS_CHARS | 
                               (m_allowOthers ? 0 : wxCB_READONLY)) );

    delete []choices;
    
    wxSheetCellEditorRefData::CreateEditor(parent, id, evtHandler, sheet);
    m_comboHandler.Connect(this);
}

void GridCellComboIntEditor::SetSize(const wxRect& rect, const wxSheetCellAttr &attr)
{
    wxSheetCellChoiceEditorRefData::SetSize( rect, attr );

#ifdef __WXMSW__
    // I think we are specifying the text height with this call, so make size smaller to leave room for borders
    wxComboBox * ctrl = (wxComboBox*)GetControl();
    ::SendMessage((HWND)ctrl->GetHandle(), CB_SETITEMHEIGHT, (WPARAM)-1, rect.GetHeight() - 6);
#endif
}

void GridCellComboIntEditor::BeginEdit(const wxSheetCoords& coords, wxSheet* sheet)
{
    wxLogDebug(wxT("GridCellComboIntEditor::BeginEdit"));

    wxSheetCellChoiceEditorRefData::BeginEdit( coords, sheet );

    m_coords = coords;
    m_grid = sheet;

    // Not currently using this info...
    m_pointActivate = GetComboBox()->ScreenToClient(m_pointActivate);
    
    sheet->RefreshCell(coords, true);
}

void GridCellComboIntEditor::StartingKey(wxKeyEvent& event) 
{ 
    wxChar ch;

#if wxUSE_UNICODE
    ch = event.GetUnicodeKey();
    if (ch <= 127)
        ch = (wxChar)event.GetKeyCode();
#else
    ch = (wxChar)event.GetKeyCode();
#endif
    switch (ch)
    {
        case WXK_DELETE:
            // delete the character at the cursor
            /*pos = tc->GetInsertionPoint();
            if (pos < tc->GetLastPosition())
                tc->Remove(pos, pos+1);*/
            break;

        case WXK_BACK:
            // delete the character before the cursor
            /*pos = tc->GetInsertionPoint();
            if (pos > 0)
                tc->Remove(pos-1, pos);*/
            break;

        default:
            GetComboBox()->SetValue(ch);
            GetComboBox()->SetInsertionPointEnd();
            break;
    }

}

bool GridCellComboIntEditor::EndEdit(const wxSheetCoords& coords, wxSheet* sheet)
{
    wxString strValue = GetComboBox()->GetValue();
    SetCustomValue(strValue);

    return wxSheetCellChoiceEditorRefData::EndEdit( coords, sheet );
}

void GridCellComboIntEditor::Reset()
{
    m_choices.Clear();
    m_intChoices.Clear();
    if (GetComboBox())
    {
        GetComboBox()->Clear();
    }
}

void GridCellComboIntEditor::Append( const wxString & strValue, int intValue )
{
    // See if strValue is already present
    unsigned int i = 0;
    unsigned int count = m_choices.GetCount();
    for( i = 0; i < count; ++i )
    {
        if( m_choices[i] == strValue )
        {
            if( m_intChoices.Count() <= i )
            {
                m_intChoices.Alloc( i + 1 );
            }
            m_intChoices[i] = intValue;
            break;
        }
    }
    if( i >= count )
    {
        m_choices.Add( strValue );
        m_intChoices.Add( intValue );

        if( GetComboBox() )
        {
            GetComboBox()->Append( strValue );
        }
    }
}

int GridCellComboIntEditor::GetIntValue( const wxString & strValue, int intDefault )
{
    int i = 0;
    int count = m_choices.GetCount();
    for( i = 0; i < count; ++i )
    {
        if( m_choices[i] == strValue )
        {
            return m_intChoices[i];
        }
    }
    return intDefault;
}

bool GridCellComboIntEditor::SetValue( int intValue )
{
    int i = 0;
    int count = m_intChoices.Count();
    for( i = 0; i < count; ++i )
    {
        if( m_intChoices[i] == intValue )
        {
            m_startValue = m_choices[i];
            return true;
        }
    }
    // Failure to locate the intValue
    //wxLogDebug( wxT("GridCellComboIntEditor::SetValue( %d ) - Failed"), intValue );
    return false;
}

void GridCellComboIntEditor::SetCustomValue(wxString& strNewValue)
{
    int i = 0;
    int count = m_choices.Count();
    for( i = 0; i < count; ++i )
    {
        if( m_choices[i] == strNewValue )
        {
            return;
        }
    }

    // Append the new custom value or replace the previous custom value.
    bool fHaveCustomEntry = false;
    for( size_t i = 0; i < IntChoices().Count(); ++i )
    {
        if( IntChoices()[i] == GetCustomEntryIntValue() )
        {
            fHaveCustomEntry = true;
            GetComboBox()->SetString( i, strNewValue );
            m_choices[i] = strNewValue;
        }
    }
    if( !fHaveCustomEntry )
    {
        Append( strNewValue, GetCustomEntryIntValue() );
    }

    SetValue( GetCustomEntryIntValue() );

    GetComboBox()->SetSelection( Choices().Count() - 1 );

    Grid()->SetCellValue( GetCoords(), strNewValue );
}

wxString GridCellComboIntEditor::GetCustomValue()
{
    wxString res;
    
    for( size_t i = 0; i < IntChoices().Count(); ++i )
    {
        if( IntChoices()[i] == GetCustomEntryIntValue() )
        {
            res = Choices()[i];
            break;
        }
    }
    
    return res;
}

void GridCellComboIntEditor::SetupCustomEntry( wxWindow *parent, const wxString & caption, const wxString & prompt, int nTrigger, int nValue )
{
    m_parentCustomEntry = parent;
    m_strCustomEntryCaption = caption;
    m_strCustomEntryPrompt = prompt;
    m_nCustomEntryTrigger = nTrigger;
    m_nCustomEntryIntValue = nValue;
}

GridCellComboIntEditorEvtHandler::GridCellComboIntEditorEvtHandler():
m_pEditor(0)
{

}

void GridCellComboIntEditorEvtHandler::Connect( GridCellComboIntEditor *pEditor )
{
    m_pEditor = pEditor;
    pEditor->GetComboBox()->Connect( wxID_ANY, wxEVT_COMMAND_COMBOBOX_SELECTED, wxCommandEventHandler(GridCellComboIntEditorEvtHandler::OnComboBoxSelect), 0, this);
    pEditor->GetComboBox()->Connect( wxID_ANY, wxEVT_COMMAND_TEXT_UPDATED,      wxCommandEventHandler(GridCellComboIntEditorEvtHandler::OnComboBoxTextUpdated), 0, this);
}

void GridCellComboIntEditorEvtHandler::OnComboBoxSelect( wxCommandEvent & event )
{
    wxString strValue;
    strValue = m_pEditor->GetComboBox()->GetValue();
    int nSelectedValue = m_pEditor->GetIntValue( strValue );

    if( m_pEditor->GetCustomEntryTrigger() >= 0 && m_pEditor->GetComboBox() != NULL &&
        nSelectedValue == m_pEditor->GetCustomEntryTrigger())
    {
        wxTextEntryDialog Dlg( m_pEditor->GetParentCustomEntry(), m_pEditor->GetCustomEntryPrompt(), m_pEditor->GetCustomEntryCaption() );
        int nResp = Dlg.ShowModal();
        if( nResp == wxID_OK )
        {
            //wxLogDebug(wxT("Custom value selected for combo box."));
            wxString strNewValue;
            strNewValue = Dlg.GetValue();

            // Append the new custom value or replace the previous custom value.
            m_pEditor->SetCustomValue(strNewValue);
        }
        //return;
    }else
    {
        m_pEditor->Grid()->SetCellValue( m_pEditor->GetCoords(), strValue );
    }
    //event.Skip();
}

void GridCellComboIntEditorEvtHandler::OnComboBoxTextUpdated( wxCommandEvent & event )
{
//    wxString strValue = m_pEditor->GetComboBox()->GetValue();
//    wxLogDebug(wxT("Combo box text updated: Value = %s"),strValue.c_str());
}


//////////////////////////////////////////////////////////////////////////////////////////////

void GridCellBoolEditor::CreateEditor(wxWindow* parent, wxWindowID id, wxEvtHandler* evtHandler, wxSheet* sheet)
{

    SetControl( new wxCheckBox(parent, id, wxEmptyString,
                               wxDefaultPosition, wxDefaultSize,
                               wxNO_BORDER ) );

    wxSheetCellEditorRefData::CreateEditor(parent, id, evtHandler, sheet);

//    CUtils::DisableTheme( GetControl() );
}

//////////////////////////////////////////////////////////////////////////////////////////////

GridCellImageRenderer::GridCellImageRenderer(CImageListComposite * pImageList, int align ):
wxSheetCellBitmapRendererRefData( wxNullBitmap, align ),
m_pImageListSrc(pImageList),
m_nImage(0)
{
    if( pImageList )
    {
        m_pImageList = m_pImageListSrc->GetImageList();
    }
}

void GridCellImageRenderer::Draw(wxSheet& sheet, const wxSheetCellAttr& attr,
                      wxDC& dc, const wxRect& rect,
                      const wxSheetCoords& coords, bool isSelected)
{
    if( m_pImageList && m_pImageListSrc )
    {
        wxBitmap bmp;
        bmp = m_pImageList->GetBitmap( m_nImage );
        bmp.SetMask( new wxMask( bmp, wxColour( 255,255,255) ) );
        SetBitmap( bmp );
    }
    // The base class draws with or without a bitmap, there could be text as well.
    wxSheetCellBitmapRendererRefData::Draw( sheet, attr, dc, rect, coords, isSelected );
}

wxSize GridCellImageRenderer::GetBestSize(wxSheet& sheet, const wxSheetCellAttr& attr,
                                    wxDC& dc, const wxSheetCoords& coords)
{
    wxSize size( 10,10 );
    int HBorder = 0;
    int VBorder = -1;
    if( m_pImageList )
    {
        int nWidth, nHeight;
        m_pImageList->GetSize( 0, nWidth, nHeight );
        size.SetWidth( nWidth + (2 * HBorder) );
        size.SetHeight( nHeight + (2 * VBorder) );
    }
    return size;
}

void GridCellImageRenderer::SetImage(int image)
{
    m_nImage = image;
}
