/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef SHARECONTROL_H_INCLUDED
#define SHARECONTROL_H_INCLUDED

#include "funcptr.h"
#include "controller.h"
#include "../sharelib/share_api.h"
#include <memory>

/*
   Share control class.
   Create one instance per active meeting, but only only one meeting to be sharing at a time.
*/


class SharingControlDialog;
class MeetingMainFrame;

class ShareControl
{
public:
    enum {
        ShareNone,
        SharePresenting,
        ShareViewing,
        ShareMax
    };
    enum {
        ShareStarting       =  0,
        ShareErrBusy        = -1,
        ShareUserAborted    = -2,
        ShareNoServer       = -3,
        ShareNoDisplaySelected = -4,
    };

    ShareControl(MeetingMainFrame *pMMF);
    ~ShareControl();

    void Initialize(controller_t controller, const wxString& strMeetingId);
    void Connect(int event, int shareState, FuncPtr* callback);
    void Disconnect(int event, int shareState);

    int SelectDisplay();
    const wxString& GetDisplay()  { return m_DisplayDevice; }
    
    int ShareDesktop();
    int ShareDocument(bool fPresenter, bool fWhiteboard);
    int ShareApplication(std::string &id);

    // Launch share server to record specified window
    int RecordShare(void * present, void * parent);

    int ShareRefresh();
    
    int ShareEnd();

    int ShareViewer(void * hwnd);
    void ViewerFullScreen(bool bEnable);
    void ViewerMoved(int x, int y, int cx, int cy);
    void ViewerErase(int x, int y, int cx, int cy);
	void ViewerGetImage(wxImage& image);
    void ViewerRequestControl();
	void ViewerMouseMove(int x, int y);
	void ViewerMouseButton(int button, bool pressed, int x, int y);
    void ViewerKey(int key, bool pressed);

    void EnableShareControl(bool flag);
    void EnableRecord(bool flag);
    void EnableHold(bool flag);
    void EnableModerator(bool flag);
    void EnableTransparent(bool flag);
    void SetCommand(int ID, bool flag);

    bool IsModerator()          { return m_bEnableModerator; }
    bool IsShareIdle()          { return m_State == ShareNone; }
    bool IsDesktopShared();
    bool IsDocumentShared()     { return s_bShareDocument; }
    bool IsPresenter();
    bool IsViewing();
    bool IsFullScreen();
    bool IsControlGranted();
    
    // Send request to server to end app share
    void ServerAppShareEnd();

    // Toolbar notifications
    void Handup(wxString strName, bool fHandup);
    void NewChat();

    wxEvtHandler *GetToolbarHandler();

    // Share server/viewer notifications
    void OnCallback(int code);
    static void on_appshare_callback(void *user, int code);

public:
    MeetingMainFrame    *m_pMMF;

private:
    static bool     s_bShareDesktop;
    static bool     s_bShareDocument;
    static bool     s_bRecordShare;

    int             m_State;
    controller_t    m_Controller;
    wxString        m_strMeetingId;
    wxString        m_DisplayDevice;
    bool            m_bFullScreen;
    bool            m_bEnableRecord;
    bool            m_bEnableHold;
    bool            m_bEnableModerator;
    bool            m_bEnableTransparent;
    bool            m_bIsControlGranted;
    int             m_iStartTime;

    SharingControlDialog    *m_pSharingCtrlDlg;

    /* Callback function ptrs...I'm assuming one handler per event per share type (presenter, viewer) will be
       sufficient...if not, convert to an stl list */
    AutoFuncPtr m_pFuncs[ShareMax][CB_Max];
};

#endif // SHARECONTROL_H_INCLUDED
