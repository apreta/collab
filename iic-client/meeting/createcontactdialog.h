/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef CREATECONTACTDIALOG_H
#define CREATECONTACTDIALOG_H

#include <wx/generic/grid.h>
#include "schedule.h"
#include "addressbk.h"
#include "wx/propgrid/propgrid.h"
#include "services/common/common.h"

//(*Headers(CreateContactDialog)
#include <wx/panel.h>
#include <wx/button.h>
#include <wx/dialog.h>
//*)

class CreateContactDialog: public wxDialog
{
	public:

		CreateContactDialog(wxWindow* parent,wxWindowID id = -1);
		virtual ~CreateContactDialog();

        /** Data entry grid column metrics */
        enum DataGridColumns
        {
            DataColumnWidth = 250,
            RowLabelWidth = 120
        };

        /**
          * Ask the user for changes to Invitee/Contact entries.<br>
          * @param dir The target directory for new contact records.
          * @param mtg The target meeting for new invitees.
          * @return bool Returns true if new data was created.
          */
        bool EditNewContact(  directory_t dir, meeting_details_t mtg, invitee_t invt, contact_t contact );

        /**
          * Ask the user for changes to the Invitee entry.<br>
          * @param dir The target directory for new contact records.
          * @param mtg The target meeting for new invitees.
          * @return bool Returns true if new data was created.
          */
        bool EditNewAdHoc( directory_t dir, meeting_details_t mtg, invitee_t invt, contact_t contact );

        /**
          * Ask the user for changes to the Contact.
          * @param dir The target directory for the contact record.
          * @param group The target group for the contact.
          * @param contact The contact record.
          * @param password Input: The initial password, Output: The new password if OK is clicked.
          */
        bool EditNewContact( directory_t dir, group_t group, contact_t contact, wxString & password, bool fThisIsMe = false );

        /**
          * Validate the content
          */
        virtual bool Validate();

        /**
          * Return the new Invitee record.
          * @return invitee_t Returns a pointer to the Invitee record.
          */
        invitee_t GetInvitee()
        {
            return m_invt;
        }

        /**
          * Return the new Contact record.
          * @return contact_t Returns a pointer to the Contact record.
          */
        contact_t GetContact()
        {
            return m_contact;
        }

        /**
          * Return true if the user still wants to create a new PAB contact.<br>
          */
        bool CreatePABContact()
        {
            return m_fCreateContact;
        }

		//(*Identifiers(CreateContactDialog)
		//*)


	protected:
        /**
          * Prepare the data entry grid.
          */
        void InitGrid();

		//(*Handlers(CreateContactDialog)
		void OnAdHocRadioSelect(wxCommandEvent& event);
		void OnPABRadioSelect(wxCommandEvent& event);
		//*)
        void    OnClickHelp( wxCommandEvent& event );
        void    OnClickChangePass( wxCommandEvent& event );
        void    OnProfileFetched(wxCommandEvent& event);

		//(*Declarations(CreateContactDialog)
		wxButton* m_BtnChangePass;
		wxPanel* m_GridPanel;
		//*)

        void Value_to_GUI();
        void GUI_to_Value();

        pstring Alloc_dir_pstring( const wxString & str )
        {
            if( m_dir )
            {
                return directory_string( m_dir, str.mb_str( wxConvUTF8 ) );
            }
            return 0;
        }

        pstring Alloc_invt_pstring( const wxString & str )
        {
            if( m_mtg )
            {
                return meeting_details_string( m_mtg, str.mb_str( wxConvUTF8 ) );
            }
            return 0;
        }

        /**
          * See if a user type is being edited.
          * @return bool Returns true if the m_contact->type is UserTypeUser or UserTypeAdmin
          */
        bool IsUserRecord()
        {
            return ( ( m_contact->type == UserTypeUser || m_contact->type == UserTypeAdmin ) );
        }

	private:
        directory_t m_dir;          /**< The target directory for new contacts */
        meeting_details_t m_mtg;    /**< The target meeting for new invitees */
        invitee_t m_invt;           /**< The new invitee record */
        contact_t m_contact;        /**< The new contact record */
//        group_t m_group;            /**< The associated group (Optional) All is assumed if not present */

        bool    m_fCreateContact;   /**< Flag: true == The user was requested the creation of a new contact in the database */
        wxPropertyGrid *m_Grid;     /**< The property grid */
        bool    m_fThisIsMe;        /**< Determine which help to display and which fields to display */
        bool    m_fProfileLoaded;

        // Record data mapped to strings
        wxString m_password;        /**< dtGlobal only */
        int m_type;                 /**< dtGlobal only */
        wxString m_profile;         /**< dtGlobal only */
        wxString m_title;
        wxString m_first;
        wxString m_middle;
        wxString m_last;
        wxString m_suffix;
        wxString m_company;
        wxString m_jobtitle;
        wxString m_address1;
        wxString m_address2;
        wxString m_city;
        wxString m_state;
        wxString m_country;
        wxString m_postalcode;
        wxString m_email1;
        wxString m_email2;
        wxString m_email3;
        wxString m_busphone;
        wxString m_homephone;
        wxString m_mobilephone;
        wxString m_otherphone;
        int      m_followme;
        wxString m_screenname;
        wxString m_aimscreen;
        wxString m_yahooscreen;
        wxString m_msnscreen;
        wxString m_user1;
        wxString m_user2;

        int      m_nAdhocIM;
        wxString m_adhocscreen;

        // Property grid Id values
        wxPGId m_id_addressbook;    /**< R/O Contact */
        wxPGId m_id_group;          /**< R/O Contact */
        wxPGId m_id_password;       /**< dtGlobal only */
        wxPGId m_id_type;           /**< dtGlobal only */
        wxPGId m_id_profile;        /**< dtGlobal only */
        wxPGId m_id_title;
        wxPGId m_id_first;
        wxPGId m_id_middle;
        wxPGId m_id_last;
        wxPGId m_id_suffix;
        wxPGId m_id_company;
        wxPGId m_id_jobtitle;
        wxPGId m_id_address1;
        wxPGId m_id_address2;
        wxPGId m_id_city;
        wxPGId m_id_state;
        wxPGId m_id_country;
        wxPGId m_id_postalcode;
        wxPGId m_id_email;
        wxPGId m_id_email2;
        wxPGId m_id_email3;
        wxPGId m_id_busphone;
        wxPGId m_id_homephone;
        wxPGId m_id_mobilephone;
        wxPGId m_id_otherphone;
        wxPGId m_id_followme;
        wxPGId m_id_ah_service;     /**< Ad Hoc only */
        wxPGId m_id_ah_screen;      /**< not used */
        wxPGId m_id_screenname;
        wxPGId m_id_aimscreen;
        wxPGId m_id_msnscreen;
        wxPGId m_id_yahooscreen;
        wxPGId m_id_user1;
        wxPGId m_id_user2;

        DECLARE_CLASS(CreateContactDialog)
		DECLARE_EVENT_TABLE()
};

#endif
