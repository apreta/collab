/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef FUNCPTR_H_INCLUDED
#define FUNCPTR_H_INCLUDED

#include <memory>

/*
   Quick implementation of function object
*/

class FuncPtr
{
public:
    virtual ~FuncPtr() { }
    virtual void operator()() = 0;
};

template <typename T>
class MemFuncPtr : public FuncPtr
{
public:
    typedef  void (T::*MemberFn)();
    MemFuncPtr(T *_obj, MemberFn _fn) : obj(_obj), fn(_fn) {  }
    void operator()() { ((obj)->*(fn))(); }
private:
    T *obj;
    MemberFn fn;
};

template <typename T, typename Arg1>
class MemFuncPtr1 : public FuncPtr
{
public:
    typedef  void (T::*MemberFn)(Arg1 a1);
    MemFuncPtr1(T *_obj, MemberFn _fn, Arg1 _a1) : obj(_obj), a1(_a1), fn(_fn) {  }
    void operator()() { ((obj)->*(fn))(a1); }
private:
    T *obj;
    Arg1 a1;
    MemberFn fn;
};

template <typename T, typename Arg1, typename Arg2>
class MemFuncPtr2 : public FuncPtr
{
public:
    typedef  void (T::*MemberFn)(Arg1 a1, Arg2 a2);
    MemFuncPtr2(T *_obj, MemberFn _fn, Arg1 _a1, Arg2 _a2) : obj(_obj), a1(_a1), a2(_a2), fn(_fn) {  }
    void operator()() { ((obj)->*(fn))(a1,a2); }
private:
    T *obj;
    Arg1 a1;
    Arg2 a2;
    MemberFn fn;
};

typedef std::auto_ptr<FuncPtr> AutoFuncPtr;

#endif // FUNCPTR_H_INCLUDED
