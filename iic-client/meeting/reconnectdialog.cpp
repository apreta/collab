/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "reconnectdialog.h"

//(*InternalHeaders(ReconnectDialog)
#include <wx/bitmap.h>
#include <wx/button.h>
#include <wx/font.h>
#include <wx/fontenum.h>
#include <wx/fontmap.h>
#include <wx/image.h>
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(ReconnectDialog)
//*)

#include "app.h"
#include "meetingcentercontroller.h"
#include "session_api.h"
#include "events.h"


BEGIN_EVENT_TABLE(ReconnectDialog,wxDialog)
	//(*EventTable(ReconnectDialog)
	//*)
END_EVENT_TABLE()


//*****************************************************************************
ReconnectDialog::ReconnectDialog(wxWindow* parent,wxWindowID id):
m_timer( this )
{
	//(*Initialize(ReconnectDialog)
	wxXmlResource::Get()->LoadObject(this,parent,_T("ReconnectDialog"),_T("wxDialog"));
	StaticText1 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT1"));
	StaticText2 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT2"));
    //*)

    m_fWaitingForReload = false;

    Connect( wxID_ANY, wxEVT_TIMER,                   wxTimerEventHandler(ReconnectDialog::OnTimer) );
    Connect( wxID_ANY, wxEVT_UPDATE_RECONNECT_STATUS, wxCommandEventHandler(ReconnectDialog::OnUpdateStatus) );
}


//*****************************************************************************
ReconnectDialog::~ReconnectDialog()
{
    Disconnect( wxID_ANY, wxEVT_UPDATE_RECONNECT_STATUS, wxCommandEventHandler(ReconnectDialog::OnUpdateStatus) );
    Disconnect( wxID_ANY, wxEVT_TIMER,                   wxTimerEventHandler(ReconnectDialog::OnTimer) );
}


//*****************************************************************************
void ReconnectDialog::OnUpdateStatus( wxCommandEvent& event )
{
//    wxLogDebug( wxT("entering ReconnectDialog::OnUpdateStatus( )") );
    int     iStatus = event.GetInt( );
    if( iStatus  ==  ERR_SUCCESS )
    {
        wxLogDebug( wxT("entering ReconnectDialog::OnUpdateStatus( ) -- status is:  RECONNECTED!!!") );
        m_fWaitingForReload = true;
        m_timer.Start( kConnectDelay, wxTIMER_ONE_SHOT );
    }
    else
    {
        wxLogDebug( wxT("entering ReconnectDialog::OnUpdateStatus( ) -- status is:  we are not connected (%d)"), iStatus );
        m_fWaitingForReload = false;
        m_timer.Start( kReconnectDelay, wxTIMER_ONE_SHOT );
    }
}


//*****************************************************************************
void ReconnectDialog::OnTimer( wxTimerEvent& event )
{
    wxLogDebug( wxT("entering ReconnectDialog::OnTimer( )") );

    if( !m_fWaitingForReload )
    {
        session_connect( CONTROLLER.session, false );
    }
    else
    {
        Hide( );
        m_fWaitingForReload = false;
    }
}
