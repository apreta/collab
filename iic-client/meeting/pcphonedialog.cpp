/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "pcphonedialog.h"

//(*InternalHeaders(PCPhoneDialog)
#include <wx/bitmap.h>
#include <wx/font.h>
#include <wx/fontenum.h>
#include <wx/fontmap.h>
#include <wx/image.h>
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(PCPhoneDialog)
//*)

BEGIN_EVENT_TABLE(PCPhoneDialog,wxDialog)
	//(*EventTable(PCPhoneDialog)
	//*)
END_EVENT_TABLE()

PCPhoneDialog::PCPhoneDialog(wxWindow* parent,wxWindowID id)
{
	//(*Initialize(PCPhoneDialog)
	wxXmlResource::Get()->LoadObject(this,parent,_T("PCPhoneDialog"),_T("wxDialog"));
	StaticText1 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT1"));
	m_VoipStatusText = (wxTextCtrl*)FindWindow(XRCID("ID_VOIPSTATUS_TEXT"));
	m_TTPad1_Button = (wxButton*)FindWindow(XRCID("ID_TTPAD1_BUTTON"));
	m_TTPad2_Button = (wxButton*)FindWindow(XRCID("ID_TTPAD2_BUTTON"));
	m_TTPad3_Button = (wxButton*)FindWindow(XRCID("ID_TTPAD3_BUTTON"));
	m_TTPad4_Button = (wxButton*)FindWindow(XRCID("ID_TTPAD4_BUTTON"));
	m_TTPad5_Button = (wxButton*)FindWindow(XRCID("ID_TTPAD5_BUTTON"));
	m_TTPad6_Button = (wxButton*)FindWindow(XRCID("ID_TTPAD6_BUTTON"));
	m_TTPad7_Button = (wxButton*)FindWindow(XRCID("ID_TTPAD7_BUTTON"));
	m_TTPad8_Button = (wxButton*)FindWindow(XRCID("ID_TTPAD8_BUTTON"));
	m_TTPad9_Button = (wxButton*)FindWindow(XRCID("ID_TTPAD9_BUTTON"));
	m_TTPadStar_Button = (wxButton*)FindWindow(XRCID("ID_TTPADSTAR_BUTTON"));
	m_TTPad0_Button = (wxButton*)FindWindow(XRCID("ID_TTPAD0_BUTTON"));
	m_TTPadPound_Button = (wxButton*)FindWindow(XRCID("ID_TTPADPOUND_BUTTON"));
	m_ConnectToMeeting_Button = (wxButton*)FindWindow(XRCID("ID_CONNECTTOMEETING_BUTTON"));
	m_DisconnectButton = (wxButton*)FindWindow(XRCID("ID_DISCONNECT_BUTTON"));
	m_AudioProperties_Button = (wxButton*)FindWindow(XRCID("ID_AUDIOPROPERTIES_BUTTON"));
	StaticText2 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT2"));
	m_Vol_Gauge = (wxGauge*)FindWindow(XRCID("ID_VOL_GAUGE"));
	StaticText3 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT3"));
	m_Mic_Gauge = (wxGauge*)FindWindow(XRCID("ID_MIC_GAUGE"));
	TextCtrl2 = (wxTextCtrl*)FindWindow(XRCID("ID_TEXTCTRL2"));
	//*)

	m_Vol_Gauge->SetRange( 32 );
	m_Mic_Gauge->SetRange( 40 );
}

PCPhoneDialog::~PCPhoneDialog()
{
	//(*Destroy(PCPhoneDialog)
	//*)
}

void PCPhoneDialog::UpdateVoipStatus( wxCommandEvent& event )
{
    wxString strStatus;
    strStatus = event.GetString();
    m_VoipStatusText->SetValue( strStatus );

    event.Skip();
}

void PCPhoneDialog::UpdateVoipLevels( wxCommandEvent& event )
{
    int Vol = event.GetInt() + 32;
    int Mic = event.GetExtraLong() + 40;

    //wxLogDebug( wxT("PCPhoneDialog::UpdateVoipLevels Vol %d, Mic %d"), Vol, Mic );

    if( ! (Vol >=0 && Vol < m_Vol_Gauge->GetRange()) )
        Vol = 0;

    m_Vol_Gauge->SetValue( Vol );

    if( ! (Mic >= 0 && Mic < m_Mic_Gauge->GetRange()) )
        Mic = 0;

    m_Mic_Gauge->SetValue( Mic );

    event.Skip();
}

