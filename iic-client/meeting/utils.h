/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
// Utils.h: interface for the CUtils class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(WX_UTILS_H)
#define WX_UTILS_H

#include <wx/cmndata.h>
#include "session_api.h"

class CUtils
{
public:
    enum
    {
        IDS_IIC_ERROR   = 0,
        IDS_IIC_PLEASENOTE,
        IDS_IIC_PLEASECONFIRM,
        IDS_IIC_CONNECTIONERROR,
        IDS_IIC_WARNING
    };

    enum
    {
        IDS_IIC_NOEXTCHARSINPASSWD = 0,
        IDS_IIC_NO_EMAIL,
        IDS_IIC_CONFIRM_SENDING_ACCOUNTINFO,
        IDS_IIC_SELECTDOC,
        IDS_IIC_CONFIRM_DELETEDOC,
        IDS_IIC_CONFIRM_DELETEPAGE,
        IDS_IIC_PAGENAME_DUPLICATED,
        IDS_IIC_DOCNAME_REQUIRED,
        IDS_IIC_INDEX_PARSE_ERROR,
        IDS_IIC_PROMPT_SAVEDOC,
        IDS_IIC_SHARENOTPRESENTER,
        IDS_IIC_CANNOT_START_PPT,
        IDS_DOCSHARE_NOPARSER,
        IDS_DOCSHARE_MARKUPNOTWELLFORMED,
        IDS_DOCSHARE_ALTERED,
        IDS_DOCSHARE_LOCKED,
        IDS_DOCSHARE_ERR_INVALID,
        IDS_DOCSHARE_IMAGETOOLARGE
    };

#ifdef __WXMSW__
    static HMODULE m_hDWMLib;
    static bool m_bLoadDWM;
    static bool m_bEnableDWM;
#endif

public:
	CUtils();
	virtual ~CUtils();

#ifdef __WXMSW__
    static bool IsRestrictedUser( );
#endif      //  #ifdef __WXMSW__

    static bool MsgWait( int aTimeout, int aCount );
    static void GetUniqueID( wxString& strID );
	static void EncodeXMLString(wxString& xml);
	static void DecodeXMLString(wxString& xml);
    static void EncodeStringFully(wxString& xml);
	static void DecodeStringFully(wxString& str);
    static void ReportErrorMsg(unsigned dwError, bool bShow, wxString& msg, void *hWnd=NULL )  {ReportErrorMsg( dwError, bShow, msg, (wxWindow*) NULL); }
    static void ReportErrorMsg(unsigned dwError, bool bShow, wxString& msg, wxWindow* pWindow=NULL );
    static bool ContainExtChars(wxString& xml);
    static void EncodeScreenName(wxString& screen);
    static void DecodeScreenName(wxString& screen);

    /** If string begins and ends with double quotes, strip them */
    static wxString StripQuotes( const wxString& strIn );

    /** Encode wxFontData content into a string */
    static wxString FontDataToString( const wxFontData & fontData );

    /** Decode a string into wxFontData content */
    static wxFontData FontDataFromString( const wxString & strFontData );

    /** Encode wxFont content into a string */
    static wxString FontToString( const wxFont & font );

    /** Decode a string into wxFont cotent */
    static wxFont FontFromString( const wxString & strFont );

    /** Encode wxColour content into a string */
    static wxString ColourToString( const wxColour & colour );

    /** Decode a string into wxColour content */
    static wxColor ColourFromString( const wxString & strColour );

    /** Convert a pstring into a wxString */
    static wxString CvtPstring( pstring ps );

    /** Change Font on all top level windows */
    static void ChangeFont( const wxFont & font );

    /** Change Font on a window and it's children */
    static void ChangeFont( wxWindow *parent, const wxFont & font );

    /** Change Font on a set of children and their children */
    static void ChangeFont( wxWindowList & chilren, const wxFont & font );

    /** Format a date/time into a short date/time string */
    static wxString FormatShortDateTime( int aTime );

    /** Format a date/time into a long date/time string */
    static wxString FormatLongDateTime( int aTime );

    /** Format a date/time into a shot date string */
    static wxString FormatShortDate( int aTime );

    /** Add a standard accelerator table to a wxWindow */
    static void UseStandardAccelerators( wxWindow & window );

    /** Show our window, show and/or restore if needed */
    static void RestoreWindow( wxTopLevelWindow * window );
    
    /** Set focus to window directly using windowing API */
    static void SetFocus( wxWindow * window );

    /** Activate our window */
    static void StealFocus( wxWindow * window );
    
    /** Set window modal */
    static void SetModal( wxWindow * window, bool modal );

    /** Disable desktop compositing (aka aero) */
    static void DisableDWM( );

    /** Re-enable desktop compositing */
    static void RestoreDWM( );

    /** Disable theme for window (Win32 only) */
    static void DisableTheme( wxWindow * window );

	/** Keep window on top */
	static void StayOnTop( wxTopLevelWindow * window );
    
    /** Close dialogs owned by specified window.  If window is NULL, close all owned dialogs (dialogs whose parent is NULL are left open) */
    static void CloseDialogs( wxWindow * window );

    static int DisplayMessage( wxWindow* pParent, int aTitleID, int aMessageID, int aFlags = wxOK | wxICON_INFORMATION );

    static void ReportDAVError( wxWindow* pParent, int aCode, wxString strStatusText, wxString aFormat, wxString aTitle, unsigned dwError );

    /** Get status message for HTTP status code */
    static void GetHTTPStatusText( int status, wxString& output );

    /** Get status message for server error code */
    static void GetServerStatusMsg( int iStatus, wxString &strStatusMsg );

    /** Decompress Zlib compressed file */
    static bool DecompressFile( wxString& aIn, wxString& aOut);
};


#endif 
