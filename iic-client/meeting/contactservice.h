/* (c) 2009 Michael Tinglof */

#ifndef CONTACTSERVICE_H
#define CONTACTSERVICE_H

#include "addressbk.h"
#include <list>

struct ContactPlugIn;


class ContactEvents {
public:
    virtual void OnDirectoryReady(directory_t dir) = 0;
    virtual void OnDirectoryStarted(directory_t dir) = 0;
};

class ContactService
{
    ContactEvents & m_handler;
    wxString m_user;
    wxString m_workingPath;
    wxString m_libraryPath;
    const char **m_fieldNames;
    int m_fieldCount;
    volatile int m_downloadedCount;
    volatile bool m_running;
    
    addressbk_t m_book;
    directory_iter_t m_dirIter;

    typedef std::list<ContactPlugIn*> PlugInList;

    PlugInList m_plugIns;
    
    typedef std::list<wxString> DirectoryList;
    
    DirectoryList m_directoryNames;

public:    
    ContactService(ContactEvents & handler);
    ~ContactService();

    void SetFields(const char * names[], int count);
    void SetBook(addressbk_t book);
    void SetUser(wxString name);
    
    ContactPlugIn* LoadPlugin(const wxString name);

    void ResetDirectories();
    void ImportDirectory(const wxString name);
    void StartImport();
    void StopImport();
    void DoImport();
    void Shutdown();
    
    bool Configure(const wxString name, wxWindow *parent);
    bool IsConfigured(const wxString name);
    bool IsImporting();
    
    directory_t GetDirectoryFirst();
    directory_t GetDirectoryNext();

    contact_t FindContact(const wxString userId);
            
protected:
    wxString& GetLibraryPath();
};

#endif // CONTACTSERVICE_H
