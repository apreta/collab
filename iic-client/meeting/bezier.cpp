/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "wx_pch.h"
#include "iicdoc.h"
#include "drawingobjects.h"

#include "bezier.h"
#include <math.h>



//	Copyright Llew S. Goodstadt 1998
//		http://www.lg.ndirect.co.uk    mailto:lg@ndirect.co.uk
//		you are hereby granted the right tofair use and distribution
//		including in both commercial and non-commercial applications.

// anonymous namespace for static linking
namespace
{
    void DoBezierPts(const LBezier& V, double error, double& len, double& t, double& result);
}
void LBezier::GetCPoint( wxPoint* out)
{
    for (int i = 0; i <= 3; ++i)
        out[i] = p[i].GetCPoint();
}

// split into two halves
void LBezier::Split(LBezier& Left, LBezier& Right) const
{
    // Triangle Matrix
    LBezier   Vtemp[4];

    // Copy control LDPoints
    Vtemp[0] = *this;

    // Triangle computation
    for (int i = 1; i <= 3; i++)
    {
        for (int j =0 ; j <= 3 - i; j++)
        {
            Vtemp[i].p[j].x =   0.5 * Vtemp[i-1].p[j].x + 0.5 * Vtemp[i-1].p[j+1].x;
            Vtemp[i].p[j].y =   0.5 * Vtemp[i-1].p[j].y + 0.5 * Vtemp[i-1].p[j+1].y;
        }
    }
    for (int j = 0; j <= 3; j++)
    {
        Left.p[j]  = Vtemp[j].p[0];
        Right.p[j] = Vtemp[3-j].p[j];
    }
}

//Get length of bezier to within error
double LBezier::Length(double error) const
{
    // control polygon length
    double controlPolygonLen = 0.0;
    for (int i = 0; i <= 2; i++)
        controlPolygonLen += p[i].DistFrom(p[i+1]);

    // chord length
    double chordLen = p[0].DistFrom(p[3]);

    // split into two until each can be approximated by its chord
    if(controlPolygonLen - chordLen > error)
    {
        // split in two recursively and add lengths of each
        LBezier left, right;
        Split(left, right);
        return left.Length(error) + right.Length(error);
    }

    return controlPolygonLen;
}

void LBezier::TSplit(LBezier& Output, double t)
{
    double s    = 1.0 - t;
    double ss   = s * s;
    double tt   = t * t;

    Output.p[0]     = p[0];
    Output.p[1]     = p[1] * t + p[0] * s;
    Output.p[2]     = p[0] * ss + p[1] * s * t * 2.0 + p[2] * tt;
    Output.p[3]     = p[0] * s * ss + p[1] * ss * t * 3.0 +
                      p[2] * s * tt * 3.0 + p[3] * t * tt;

    p[0]        = Output.p[3];
    p[1]        = p[1] * ss + p[2] * s * t * 2.0 + p[3] * tt;
    p[2]        = p[2] * s + p[3] * t;
}

double LBezier::TAtLength(unsigned int& length) const
{
    double t = 0.0;
    double result = 0.0;
    double adjust = 0.0;
    double tempLength = length;
    if (length < 3)
    {
        LDPoint rnd(p[0].GetCPoint());
        adjust = p[0].DistFrom(p[3]) - rnd.DistFrom(p[3]);
        tempLength += adjust;
    }
    DoBezierPts(*this, 0.5 /* 0.5 */, tempLength, t, result);
    length -= static_cast<int>(result - adjust + 0.5);
    return t;
}

double LBezier::TAtLength(double& length, double error) const
{
    double t = 0.0;
    double result = 0.0;
    double adjust = 0.0;
    double tempLength = length;
    if (length < 3.0)
    {
        LDPoint rnd(p[0].GetCPoint());
        adjust = p[0].DistFrom(p[3]) - rnd.DistFrom(p[3]);
        tempLength += adjust;
    }
    DoBezierPts(*this, 0.5 /* 0.5 */, tempLength, t, result);
    length -= static_cast<int>(result - adjust + 0.5);
    return t;
}

// Create points to simulate ellipse using beziers
void EllipseToBezier( IICRect& r, wxPoint* cCtlPt)
{
    const double EToBConst = 0.2761423749154;
    //  2/3*(sqrt(2)-1)
    // error = +0.027%  - 0.0%
    wxSize offset((int)(r.Width() * EToBConst), (int)(r.Height() * EToBConst));
    wxPoint centre((r.left + r.right) / 2, (r.top + r.bottom) / 2);

    cCtlPt[0].x  =                                      //------------------------/
    cCtlPt[1].x  =                                      //                        /
    cCtlPt[11].x =                                      //        2___3___4       /
    cCtlPt[12].x = r.left;                              //     1             5    /
    cCtlPt[5].x  =                                      //     |             |    /
    cCtlPt[6].x  =                                      //     |             |    /
    cCtlPt[7].x  = r.right;                             //     0,12          6    /
    cCtlPt[2].x  =                                      //     |             |    /
    cCtlPt[10].x = centre.x - offset.GetWidth( );       //     |             |    /
    cCtlPt[4].x  =                                      //    11             7    /
    cCtlPt[8].x  = centre.x + offset.GetWidth( );       //       10___9___8       /
    cCtlPt[3].x  =                                      //                        /
    cCtlPt[9].x  = centre.x;                            //------------------------*

    cCtlPt[2].y  =
    cCtlPt[3].y  =
    cCtlPt[4].y  = r.top;
    cCtlPt[8].y  =
    cCtlPt[9].y  =
    cCtlPt[10].y = r.bottom;
    cCtlPt[7].y  =
    cCtlPt[11].y = centre.y + offset.GetHeight( );
    cCtlPt[1].y  =
    cCtlPt[5].y  = centre.y - offset.GetHeight( );
    cCtlPt[0].y  =
    cCtlPt[12].y =
    cCtlPt[6].y  = centre.y;
}


namespace
{
static int recurse = 0;
void DoBezierPts(const LBezier& V, double error, double& len, double& t, double& result)
{
//    wxLogDebug( wxT("entering DoBezierPts( )") );
//  TRACE0("BezierPts\r\n");
    if (len == 0.0)
    {
        wxLogDebug( wxT("!!!!!!!!!!!!!!!!!! WARNING - DoBezierPts( ) - len is 0.0 !!!!!!!!!!!!!") );
        return;
    }

    // control polygon length
    double controlPolygonLen = 0.0;
    for (int i = 0; i <= 2; i++)
        controlPolygonLen += V.p[i].DistFrom(V.p[i+1]);

    // chord length
    double chordLen = V.p[0].DistFrom(V.p[3]);

    // call self recursively until accurate enough
    if(controlPolygonLen - chordLen > error ||
        controlPolygonLen + result > len + error)
    {

        // split in two
        LBezier left, right;
        V.Split(left, right);

        // add left and right sides
        ++recurse;
        DoBezierPts(left, error, len, t, result);
        if (len == 0.0l)
        {
            --recurse;
            return;
        }
        DoBezierPts(right, error, len, t, result);
        --recurse;
        return;
    }

    result  += controlPolygonLen;
    t       += 1.0 / (1 << recurse);
//  TRACE3("recurse=%4.d, length =%.5g, result = %.5g\r\n", recurse, controlPolygonLen, result);
//    wxLogDebug( wxT("recurse=%4.1d, length =%1.5g, result = %1.5g  (%.3g, %.3g) (%.3g, %.3g) (%.3g, %.3g) (%.3g, %.3g)"),
//                recurse, controlPolygonLen, result, V.p[0].x, V.p[0].y, V.p[1].x, V.p[1].y, V.p[2].x, V.p[2].y, V.p[3].x, V.p[3].y );
    if (fabs(result - len)<=error)
    {
//      TRACE0("\tfinished!!\r\n");
//    wxLogDebug( wxT("FINISHED!!  recurse=%4.1d, length =%1.5g, result = %1.5g  (%.3g, %.3g) (%.3g, %.3g) (%.3g, %.3g) (%.3g, %.3g)"),
//                recurse, controlPolygonLen, result, V.p[0].x, V.p[0].y, V.p[1].x, V.p[1].y, V.p[2].x, V.p[2].y, V.p[3].x, V.p[3].y );
        len = 0.0l;
        return;
    }
}

};
