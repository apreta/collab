/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef _PRESETS_H_
#define _PRESETS_H_

class wxComboBox;

/**
=================================================================================
Disk Storage Format: (Using wxZonOptions I/O):

"Preset-<prefix>-<index>","<data>"
"Preset-<prefix>-<index>","<data>"
"Preset-<prefix>-<index>","<data>"
...

Where:
<prefix> == The preset type ( MeetingSearch, ContactSearch, ... )
<index> == The sequential index (0..n). No Gaps in the numeric sequence
are allowed.
<data> == name|s0|s1|s2|s3...|sn

=================================================================================
*/

/**
  * Manage preset data I/O.
  * Used to maintain search criteria and the like.
  */
class PresetData
{
public:
    /** Constructor
      * @param prefix The storage prefix.
      */
    PresetData( const wxString & prefix ):
    m_nLoadedEntries(0),
    m_strPrefix( prefix )
    {
    }

    PresetData( const char *pszPrefix ):
    m_nLoadedEntries(0)
    {
        m_strPrefix = wxString(pszPrefix,wxConvUTF8);
    }

	enum Settings
	{
		Preset_Loaded = 0,	/**< The preset was loaded from storage */
		Preset_New = 1,		/**< The preset is new */
		Preset_Removed = 2,	/**< The preset has been removed */
		Max_Presets = 100   /**< The maximum number of presets */
	};

    /**
      * Return the number of entries.
      */
    int GetCount()
    {
        return m_Name.GetCount();
    }

    /**
      * Load the stored data from the current user option data.
      */
	bool Load();

    /**
      * Save the current data to the current user option data.<br>
      * Proccess:<br>
      * Count the number of entries to be saved (nSave) .<br>
      * Clear any previous entries with index values greater than nSave.<br>
      * Save the new entries.
      */
	bool Save();

    /** Locate a prefix by name
      */
    int Lookup( const wxString & name );

    /**
      * Remove the preset data specified by the index.<br>
      * NOTE: The status associated with the data is set to Preset_Removed.
      */
	bool Remove( int ndx );

    /**
      * Add a new preset.<br>
      * If the name matches an existing entry, then the content of that entry is replaced.
      * If the name is not found then, attempt to replace a preset marked as Preset_Removed.
      * If the name is not found and there are no free slots then append a new preset.<br>
      */
	bool Add( const wxString &name, const wxArrayString & data );

    /**
      * Retrieve the data specified by the index.
      */
	bool Retrieve( int ndx, wxArrayString & data  );

    /**
      * Check to see if preset data has ben changed.
      */
    bool DataHasChanged( const wxString & name, const wxArrayString & data );

public:
    // The following methods are helper functions to make GUI processing
    // simpler:

    /**
      * Populate a combo box with the current preset names.
      */
    void PopulateComboBox( wxComboBox *pCombo );

    /**
      * Remove the currently selected preset name.
      */
    void RemoveCurrentPreset( wxComboBox *pCombo );

    /**
      * Process a preset Save As operation.
      */
    bool HandleSaveAs( wxWindow *parent, wxComboBox *pCombo, const wxString & caption, const wxString & prompt, const wxArrayString & data );

    /**
      * Handle new selection.
      */
    bool HandleNewSelection( wxComboBox*pCombo, wxArrayString & data );

private:
    /**
      * Encode wxArrayString into wxString:<br>
      * Format: s0|s1|s2|...|sn
      */
    void Encode( const wxArrayString & data, wxString & strData );


    /** The number of entries read by the last call to Load() */
    int m_nLoadedEntries;

    /** Construct a key name */
    wxString Key( int ndx );

	/** The identifier prefix */
	wxString m_strPrefix;

    /** The preset names */
    wxArrayString m_Name;

	/** The preset data entries */
	wxArrayString m_Data;

	/** The preset state data */
	wxArrayInt    m_Status;
};

#endif
