#ifndef PROJECTCODEDIALOG_H
#define PROJECTCODEDIALOG_H

//(*Headers(ProjectCodeDialog)
#include <wx/textctrl.h>
#include <wx/dialog.h>
//*)

class ProjectCodeDialog: public wxDialog
{
	public:

		ProjectCodeDialog(wxWindow* parent);
		virtual ~ProjectCodeDialog();

		//(*Declarations(ProjectCodeDialog)
		wxTextCtrl* m_ProjectCodeEntry;
		//*)

        int GetProjectCode(wxString & code);

	protected:

		//(*Identifiers(ProjectCodeDialog)
		//*)

	private:

		//(*Handlers(ProjectCodeDialog)
		void OnProjectCodeEntryText(wxCommandEvent& event);
		//*)

        void EnableButtons();
        
		DECLARE_EVENT_TABLE()
};

#endif
