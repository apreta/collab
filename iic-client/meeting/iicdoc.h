/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef IICDOC_H
#define IICDOC_H

#include "wx/docview.h"


class CIICMtg
{
public:
    CIICMtg( void )     { };
    ~CIICMtg( void )    { };

    void        SetIsUserPresenter( bool fVal )     { m_fIsUserPresenter = fVal; }
    bool        IsUserPresenter( )                  { return m_fIsUserPresenter; }

    void        SetIsPresenting( bool fVal )        { m_fIsPresenting = fVal; }
    bool        IsPresenting( )                     { return m_fIsPresenting; }

    /* Flag to indicate doc share session is occuring during a meeting.
       The intention is to also allow editing outside of a meeting (e.g. to prepate document for later meeting) */
    void        SetIsInProgress( bool fVal )        { m_fIsInProgress = fVal; }
    bool        IsInProgress( )                     { return m_fIsInProgress; }

    void        SetMeetingUserID( wxString& strIn ) { m_strMeetingUserID = strIn; };
    void        GetMeetingUserID( wxString& strIn ) { strIn = m_strMeetingUserID; };

    void        SetMeetingId( wxString& strIn )     { m_strMeetingID = strIn; };
    void        GetMeetingId( wxString& strIn )     { strIn = m_strMeetingID; };

    void        SetDocShareServer( wxString& strIn )    { m_strDSServer = strIn; }
    wxString&   GetDocShareServer( )                    { return m_strDSServer; }

    void        SetDocSharePath( wxString& strIn )  { m_strDSPath = strIn; }
    wxString&   GetDocSharePath( )                  { return m_strDSPath; }

    void        SetPassword( wxString& strIn )      { m_strPassword = strIn; }
    wxString&   GetPassword( )                      { return m_strPassword; }


private:
    bool        m_fIsInMeeting;
    bool        m_fIsUserPresenter;
    bool        m_fIsPresenting;
    bool        m_fIsInProgress;

    wxString    m_strMeetingUserID;
    wxString    m_strMeetingID;
    wxString    m_strDSServer;
    wxString    m_strDSPath;
    wxString    m_strPassword;
};


class CIICDoc : public wxDocument
{
    DECLARE_DYNAMIC_CLASS(CIICDoc)
public:
    CIICDoc( void );
    ~CIICDoc( void );

    void    SetSessionInfo( wxString &aScreenName, wxString &aPassword, wxString &aServer );
    void    GetSessionInfo( wxString &aScreenName, wxString &aPassword, wxString &aServer );

    void    LockDocShareEvent( ) { wxLogDebug( wxT("entering CIIDoc::LockDockShareEvent( )") ); }
    void    SendDrawOnWhiteBoard( wxString& meetingId, const wxString& actionXML );

    CIICMtg m_Meeting;
};

#endif      //  #ifndef IICDOC_H
