/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 *
 *
 * All changes from ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */
// Utils.cpp: implementation of the CUtils class.
//
//////////////////////////////////////////////////////////////////////

#include "app.h"
#include "utils.h"
#include <wx/tokenzr.h>
#include <wx/uri.h>
#include <wx/wfstream.h>
#include <wx/zstream.h>
#include "wx/treelistctrl.h"

#include "services/common/common.h"

#ifdef __WXMSW__
HMODULE CUtils::m_hDWMLib = NULL;
bool CUtils::m_bLoadDWM = false;
bool CUtils::m_bEnableDWM = false;
#endif

#ifdef __WXMAC__
#include <Carbon/Carbon.h>
#endif


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CUtils::CUtils()
{
}

CUtils::~CUtils()
{

}


//*****************************************************************************
bool CUtils::MsgWait( int aTimeout, int aCount )
{
    while( aCount--  >  0 )
    {
        wxMilliSleep( aTimeout );
        while( wxGetApp( ).Pending( ) )
        {
            wxGetApp( ).Dispatch( );
        }
    }

    return true;
}


#ifdef __WXMSW__

//*****************************************************************************
bool CUtils::IsRestrictedUser( )
{
    HKEY hKey;
    LONG res = RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft"), 0, KEY_WRITE, &hKey);

    if (res != ERROR_SUCCESS)
    {
        wxLogDebug(wxT("Restricted user (%ld)"), res);
        return true;
    }

    RegCloseKey(hKey);
    return false;
}

#endif      //  #ifdef __WXMSW__


//*****************************************************************************
void CUtils::GetHTTPStatusText( int status, wxString& output )
{
    if( status < 100  ||  status > 505 )	// invalid status code for HTTP
        return;

    wxString    strT;

    switch(status)
    {
        case 100:   // HTTP_STATUS_CONTINUE
        case 101:   // HTTP_STATUS_SWITCH_PROTOCOLS
        case 200:   // HTTP_STATUS_OK:              // (200)
        case 201:   // HTTP_STATUS_CREATED:         // (201)
        case 202:   // HTTP_STATUS_ACCEPTED:        // (202)
        case 203:   // HTTP_STATUS_PARTIAL:         // (203)
        case 204:   // HTTP_STATUS_NO_CONTENT:      // (204)
        case 205:   // HTTP_STATUS_RESET_CONTENT:   // (205)
        case 206:   // HTTP_STATUS_PARTIAL_CONTENT: // (206)
        case 207:   // HTTP_MULTI_STATUS:           // 207
            strT.Append( wxString( _("Request succeeded."), wxConvUTF8 ) );
            output += strT;
            break;

        case 300:   // HTTP_STATUS_AMBIGUOUS:           // (300)
        case 301:   // HTTP_STATUS_MOVED:               // (301)
        case 302:   // HTTP_STATUS_REDIRECT:            // (302)
        case 303:   // HTTP_STATUS_REDIRECT_METHOD:     // (303)
        case 304:   // HTTP_STATUS_NOT_MODIFIED:        // (304)
        case 305:   // HTTP_STATUS_USE_PROXY:           // (305)
        case 306:   // HTTP_STATUS_REDIRECT_KEEP_VERB:  // (307)
            strT.Append( wxString( _("The document server is experiencing difficulty.\nPlease try your action again or contact your system administrator."), wxConvUTF8 ) );
            output += strT;
            break;

        case 400:   // HTTP_STATUS_BAD_REQUEST:      // (400)
            strT.Append( wxString( _("The document server has received an invalid request.\nPlease try your action again or contact your system administator."), wxConvUTF8 ));
            output += strT;
            break;

        case 401:   // HTTP_STATUS_DENIED:              // (401)
        case 402:   // HTTP_STATUS_PAYMENT_REQ:         // (402)
        case 403:   // HTTP_STATUS_FORBIDDEN:           // (403)
            strT.Append( wxString( _("Access is denied. Please try your action again or contact your system administrator."), wxConvUTF8 ) );
            output += strT;
            break;

        case 404:   // HTTP_STATUS_NOT_FOUND:           // (404)
            strT.Append( wxString( _("The requested document is not found."), wxConvUTF8 ) );
            output += strT;
            break;

        case 423: // HTTP_LOCKED   423
            strT.Append( wxString( _("The file is locked by another user.  Please try your action again later."), wxConvUTF8 ));
            output += strT;
            break;

        case 424: // HTTP_FAILED_DEPENDENCY   424
            strT.Append( wxString( _("The file may be in use, or locked by another user.\nPlease try your action again later."), wxConvUTF8 ) );
            output += strT;
            break;

        case 412:   // HTTP_STATUS_PRECOND_FAILED:      // (412)
            strT.Append( wxString( _("The file is locked by another user. Your changes cannot be saved.\nYou have to manaully make the changes again once the file is unlocked."), wxConvUTF8 ) );
            output += strT;
            break;

        case 405:   // HTTP_STATUS_BAD_METHOD:          // (405)
        case 406:   // HTTP_STATUS_NONE_ACCEPTABLE:     // (406)
        case 407:   // HTTP_STATUS_PROXY_AUTH_REQ:      // (407)
        case 408:   // HTTP_STATUS_REQUEST_TIMEOUT:     // (408)
        case 409:   // HTTP_STATUS_CONFLICT:            // (409)
        case 410:   // HTTP_STATUS_GONE:                // (410)
        case 411:   // HTTP_STATUS_LENGTH_REQUIRED:     // (411)
        case 413:   // HTTP_STATUS_REQUEST_TOO_LARGE:   // (413)
        case 414:   // HTTP_STATUS_URI_TOO_LONG:        // (414)
        case 415:   // HTTP_STATUS_UNSUPPORTED_MEDIA:   // (415)
        case 416:   // HTTP_RANGE_NOT_SATISFIABLE       416
        case 417:   // HTTP_EXPECTATION_FAILED          417
        case 422:   // HTTP_UNPROCESSABLE_ENTITY        422
        case 426:   // HTTP_UPGRADE_REQUIRED            426
        case 449:   // HTTP_STATUS_RETRY_WITH:          // (449)
        case 500:   // HTTP_STATUS_SERVER_ERROR:        // (500)
        case 501:   // HTTP_STATUS_NOT_SUPPORTED:       // (501)
        case 502:   // HTTP_STATUS_BAD_GATEWAY:         // (502)
        case 503:   // HTTP_STATUS_SERVICE_UNAVAIL:     // (503)
        case 504:   // HTTP_STATUS_GATEWAY_TIMEOUT:     // (504)
        case 505:   // HTTP_STATUS_VERSION_NOT_SUP:     // (505)
        default:
            strT.Append( wxString( _("The document server is temporarily unavailable.\nPlease try your action again or contact your system administrator."), wxConvUTF8 ) );
            output += strT;
            break;
    }

    strT = wxString::Format( wxT(" (%d)"), status );
    output += strT;
}


//*****************************************************************************
void CUtils::ReportDAVError( wxWindow* pParent, int aCode, wxString strStatusText, wxString aFormat, wxString aTitle, unsigned dwError )
{
    wxString temp;
    wxString msg;
    if( aCode >= 300 )
    {
        CUtils::GetHTTPStatusText( aCode, msg );
    }

    if( dwError!=0  &&  dwError!=0x80004005 )	// append lower level error message
    {
        if( !msg.IsEmpty() )
        {
            msg += wxT("\r\n");
            ReportErrorMsg( dwError, FALSE, msg, pParent );
        }
    }
    temp = wxString::Format( aFormat, aTitle.wc_str( ), msg.wc_str( ), _T("") );	// the strStatusText is not very meaningfull (aCode>=400?strStatusText:""));

    wxLogDebug( wxT("DAV error: %s"), temp.c_str());
    
    wxString    strTitle( PRODNAME, wxConvUTF8 );
    wxMessageDialog dlg( pParent, temp, strTitle, wxOK | ((aCode>=300||dwError!=0) ? wxICON_EXCLAMATION : wxICON_INFORMATION) );
    dlg.ShowModal( );
}


//*****************************************************************************
int CUtils::DisplayMessage( wxWindow* pParent, int aTitleID, int aMessageID, int aFlags )
{
    wxString strTitle;
    wxString strMessage;
    switch( aTitleID )
    {
        case IDS_IIC_ERROR:
            strTitle = wxString( _("Error"), wxConvUTF8 );
            break;
        case IDS_IIC_PLEASENOTE:
            strTitle = wxString( _("Please Note"), wxConvUTF8 );
            break;
        case IDS_IIC_PLEASECONFIRM:
            strTitle = wxString( _("Please Confirm"), wxConvUTF8 );
            break;
        case IDS_IIC_CONNECTIONERROR:
            strTitle = wxString( _("Connection Error"), wxConvUTF8 );
            break;
        case IDS_IIC_WARNING:
            strTitle = wxString( _("Warning"), wxConvUTF8 );
            break;
    }

    switch( aMessageID )
    {
        case IDS_IIC_PROMPT_SAVEDOC:
            strMessage = wxString( _("Save document?"), wxConvUTF8 );
            break;
        case IDS_IIC_INDEX_PARSE_ERROR:
            strMessage = wxString( _("Failed to parse index file."), wxConvUTF8 );
            break;
        case IDS_IIC_SHARENOTPRESENTER:
            strMessage = wxString( _("You must be the presenter to share an application or your desktop."), wxConvUTF8 );
            break;
        case IDS_IIC_CANNOT_START_PPT:
            strMessage = wxString( _("Could not start Impress, please try starting share\nagain.  Impress must be installed."), wxConvUTF8 );
            break;
        case IDS_DOCSHARE_NOPARSER:
            strMessage = wxString( _("Failed to create XML parser, please restart your client.\nIf the problem persistent, contact your system administrator."), wxConvUTF8 );
            break;
        case IDS_DOCSHARE_MARKUPNOTWELLFORMED:
            strMessage = wxString( _("Markup document is not well-formed."), wxConvUTF8 );
            break;
        case IDS_DOCSHARE_ALTERED:
            strMessage = wxString( _("You currently do not have the file lock,\nand the file has been changed by another user. Your changes cannot be saved.\nYou have to manually make the changes again once you have gained the file lock.\nProceed without saving?"), wxConvUTF8 );
            break;
        case IDS_DOCSHARE_LOCKED:
            strMessage = wxString( _("The file is locked by another user. Your changes cannot be saved.\nYou have to manually make the changes again once the file is unlocked.\nProceed without saving?"), wxConvUTF8 );
            break;
        case IDS_DOCSHARE_ERR_INVALID:
            strMessage = wxString( _("Name cannot contain extented characters or %&?*/\\`.\nPlease rename it first, then try again."), wxConvUTF8 );
            break;
        case IDS_IIC_NOEXTCHARSINPASSWD:
        case IDS_IIC_NO_EMAIL:
        case IDS_IIC_CONFIRM_SENDING_ACCOUNTINFO:
        case IDS_IIC_SELECTDOC:
        case IDS_IIC_CONFIRM_DELETEDOC:
        case IDS_IIC_CONFIRM_DELETEPAGE:
        case IDS_IIC_PAGENAME_DUPLICATED:
        case IDS_IIC_DOCNAME_REQUIRED:
        case IDS_DOCSHARE_IMAGETOOLARGE:
        default:
            strMessage = wxString( _("Unknown Error"), wxConvUTF8 );
            break;
    }
    
    wxLogDebug(wxT("CUtils error: %s"), strTitle.c_str());
    
    wxMessageDialog     dlg( pParent, strMessage, strTitle, aFlags );
    return dlg.ShowModal( );
}


//*****************************************************************************
void CUtils::GetUniqueID( wxString& strID )
{
    wxChar  hDigits[ ] = { wxT('0'), wxT('1'), wxT('2'), wxT('3'), wxT('4'), wxT('5'), wxT('6'), wxT('7'), wxT('8'), wxT('9'), wxT('A'), wxT('B'), wxT('C'), wxT('D'), wxT('E'), wxT('F') };

    static bool seeded = false;
    if (!seeded)
    {
        srand( (unsigned)time( NULL ) );
        seeded = true;
    }

    //  Create an ID string containing 32 hex digits
    strID.Empty( );
    int     i;
    for( i = 0; i<32; i++ )
    {
        strID.Append( hDigits[ (rand( ) % 16) ] );
        if( i == 7  ||  i == 11  ||  i == 15  ||  i == 19 )
            strID.Append( wxT('-') );
    }
}


//*****************************************************************************
//  If string begins and ends with double quotes, strip them
//*****************************************************************************
wxString CUtils::StripQuotes( const wxString& strIn )
{
    wxString    ret;
    ret = strIn;

    if( !strIn.IsEmpty( ) )
    {
        // Strip off double quotes if required
        if( strIn.Find('"') == 0  &&  strIn.Last( ) == '"' )
        {
            ret = strIn.Mid( 1, strIn.length() - 2 );
        }
    }
    return ret;
}


//*****************************************************************************
void CUtils::EncodeXMLString(wxString& xml)
{
	xml.Replace(_T("\r"),  _T("<LF>"));
	xml.Replace(_T("\n"),  _T("<CR>"));
	for (int i=0; i<(int)xml.Len(); i++)
	{
		int c = xml.GetChar(i);
		if (c<0x20)
		{
			xml.SetChar(i, _T(' '));
		}
	}

	xml.Replace(_T("&"),  _T("&amp;"));
	xml.Replace(_T("<"),  _T("&lt;"));
	xml.Replace(_T(">"),  _T("&gt;"));
	xml.Replace(_T("'"),  _T("&apos;"));
	xml.Replace(_T("\""), _T("&quot;"));
}


//*****************************************************************************
void CUtils::DecodeXMLString(wxString& xml)
{
	xml.Replace(_T("&amp;"),  _T("&"));
	xml.Replace(_T("&lt;"),   _T("<"));
	xml.Replace(_T("&gt;"),   _T(">"));
	xml.Replace(_T("&apos;"), _T("'"));
	xml.Replace(_T("&quot;"), _T("\""));
	xml.Replace(_T("<LF>"),  _T("\r"));
	xml.Replace(_T("<CR>"),  _T("\n"));
}


//*****************************************************************************
void CUtils::EncodeStringFully( wxString& src )
{
    wxURI uri(src);
    src = uri.BuildURI();
}


//*****************************************************************************
bool CUtils::ContainExtChars( wxString& xml )
{
    for( int i=0; i<(int)xml.Len( ); i++ )
    {
        int c = xml.GetChar( i );
        if( c==_T('%') || c==_T('&')  || c==_T('?') || c==_T('*') ||
            c==_T('/') || c==_T('\\') || c==_T('`') || c==_T('#')    )
        {
            return true;
        }
    }
    return false;
}


//*****************************************************************************
void CUtils::DecodeStringFully(wxString& src)
{
    wxURI uri;
    src = uri.Unescape(src);
}


/* From XEP-0106
*/
#define NUM_ENC 10
static 
const wxChar * enctable[NUM_ENC][2] = {
    { wxT("\\"), wxT("\\5c") },
    { wxT(" "),  wxT("\\20") },
    { wxT("\""), wxT("\\22") },
    { wxT("&"),  wxT("\\26") },
    { wxT("'"),  wxT("\\27") },
    { wxT("/"),  wxT("\\2f") },
    { wxT(":"),  wxT("\\3a") },
    { wxT("<"),  wxT("\\3c") },
    { wxT(">"),  wxT("\\3e") },
    { wxT("@"),  wxT("\\40") },
};


//*****************************************************************************
void CUtils::EncodeScreenName(wxString& screen)
{
    screen.Lower();

    for (int i=0; i<NUM_ENC; i++)
    {
        screen.Replace(enctable[i][0], enctable[i][1]);
    }
}


//*****************************************************************************
void CUtils::DecodeScreenName(wxString& screen)
{
    screen.Lower();

    for (int i=0; i<NUM_ENC; i++)
    {
        screen.Replace(enctable[i][1], enctable[i][0]);
    }
}


//*****************************************************************************
wxString CUtils::FontDataToString( const wxFontData & fontData )
{
    wxString s;
    wxFont font;
    wxColour colour;

    font = fontData.GetChosenFont();

    s = FontToString( font );

    s += wxT(";"); // NOTE: Different delimiter between Font and Colour

    colour = fontData.GetColour();

    s += ColourToString( colour );

    return s;
}

wxFontData CUtils::FontDataFromString( const wxString & strFontData )
{
    wxFontData fontData;
    wxFont font;
    wxColour colour;

    wxString strFont( wxT("") );
    wxString strColour( wxT("") );

    // Check for a Font/Colour pair
    if( -1 != strFontData.Find(';') )
    {
        int iTok = 0;
        wxStringTokenizer tkz( strFontData, wxT(";")); while ( tkz.HasMoreTokens() )
        {
            wxString token = tkz.GetNextToken();
            if( iTok == 0 ) strFont = token;
            else if( iTok == 1 ) strColour = token;
            ++iTok;
        }
        if( iTok != 2 )
        {
            // Garbage in. Make sure the font is reasonable
            wxLogDebug(wxT("FontDataFromString(%s) - Expected two tokens"),strFontData.c_str());
            strFont = wxT("");
            strColour = wxT("");
            font = wxSystemSettings::GetFont( wxSYS_DEFAULT_GUI_FONT );
        }
    }
    else
    {
        strFont = strFontData;
    }

    if( !strFont.IsEmpty() )
        font = FontFromString( strFont );

    if( !strColour.IsEmpty() )
        colour = ColourFromString( strColour );

    fontData.SetChosenFont( font );
    fontData.SetColour( colour );

    return fontData;
}

/**
  * This little function encodes the font specifiations as a string.<br>
  * The wxFont::GetNativeFontInfoDesc() method was not used because the
  * encoded font string is supposed to be platform independent.
  */
wxString CUtils::FontToString( const wxFont & font )
{
    wxString s;
    wxString delim( wxT("|") );

    s = font.GetFaceName();
    s += delim;

    s += wxString::Format(wxT("%d"),font.GetFamily());
    s += delim;

    s += wxString::Format(wxT("%d"),font.GetPointSize());
    s += delim;

    s += wxString::Format(wxT("%d"),font.GetStyle());
    s += delim;

    s += wxString::Format(wxT("%d"),font.GetUnderlined()); // Windows and Motif only
    s += delim;

    s += wxString::Format(wxT("%d"),font.GetWeight());

    return s;
}

wxFont CUtils::FontFromString( const wxString & strFont )
{
    wxFont font;
    bool fOK = true;
    unsigned long nValue = 0;
    int iTok = 0;
    wxStringTokenizer tkz( strFont, wxT("|"));
    while ( fOK && tkz.HasMoreTokens() )
    {
        wxString token = tkz.GetNextToken();
        switch( iTok )
        {
        case 0:
            font.SetFaceName( token );
            break;
        case 1:
            fOK = token.ToULong( &nValue, 10 );
            if( fOK )
                font.SetFamily( nValue );
            break;
        case 2:
            fOK = token.ToULong( &nValue, 10 );
            if( fOK )
                font.SetPointSize( nValue );
            break;
        case 3:
            fOK = token.ToULong( &nValue, 10 );
            if( fOK )
                font.SetStyle( nValue );
            break;
        case 4:
            fOK = token.ToULong( &nValue, 10 );
            if( fOK )
                font.SetUnderlined( (nValue) ? true:false );
            break;
        case 5:
            fOK = token.ToULong( &nValue, 10 );
            if( fOK )
                font.SetWeight( nValue );
            break;
        }
        ++iTok;
    }
    if( !fOK || iTok != 6 )
    {
        wxString error;
        if( fOK )
        {
            error = wxString::Format(wxT("Found %d tokens, expected %d."),iTok,6);
        }else
        {
            error = wxString::Format(wxT("Parse failure at token %d"),iTok);
        }
        wxLogDebug(wxT("FontFromString - %s"),error.c_str());

        // Garbage in. Make sure the font is reasonable
        font = wxSystemSettings::GetFont( wxSYS_DEFAULT_GUI_FONT );
    }
    return font;
}

wxString CUtils::ColourToString( const wxColour & colour )
{
    wxString s;

    s += wxString::Format( wxT("%d|%d|%d"), colour.Red(), colour.Green(), colour.Blue() );

    return s;
}

wxColor CUtils::ColourFromString( const wxString & strColour )
{
    wxColour colour;
    int iTok = 0;
    bool fOK = true;
    unsigned long nValue;
    unsigned char r = 0;
    unsigned char g = 0;
    unsigned char b = 0;

    wxStringTokenizer tkz( strColour, wxT("|"));
    while ( fOK && tkz.HasMoreTokens() )
    {
        wxString token = tkz.GetNextToken();
        switch( iTok )
        {
        case 0:
            fOK = token.ToULong( &nValue, 10 );
            if( fOK )
                r = (unsigned char)nValue;
            break;
        case 1:
            fOK = token.ToULong( &nValue, 10 );
            if( fOK )
                g = (unsigned char)nValue;
            break;
        case 2:
            fOK = token.ToULong( &nValue, 10 );
            if( fOK )
                b = (unsigned char)nValue;
            break;
        }
        ++iTok;
    }
    if( !fOK || iTok != 3 )
    {
        // Garbage in. Default the colour to black
        r = g = b = 0;
    }
    colour.Set( r, g, b );
    return colour;
}

wxString CUtils::CvtPstring( pstring ps )
{
    wxString ret(wxT(""));
    if( ps )
    {
        ret = wxString( ps, wxConvUTF8 );
    }
    return ret;
}

void CUtils::ChangeFont( const wxFont & font )
{
    wxWindowListNode* node = wxTopLevelWindows.GetFirst();
    while (node)
    {
        wxWindow* win = node->GetData();
        CUtils::ChangeFont( win, font );
        node = node->GetNext();
    }
}

void CUtils::ChangeFont( wxWindow *parent, const wxFont & font )
{
    parent->SetFont( font );
    wxWindowList & children = parent->GetChildren();
    CUtils::ChangeFont( children, font );
}

void CUtils::ChangeFont( wxWindowList & children, const wxFont & font )
{
    wxWindowList::Node *node = children.GetFirst();
    while (node)
    {
        wxWindow *win = node->GetData();
        win->SetFont( font );

        CUtils::ChangeFont( win->GetChildren(), font );
        // Special cases for windows that don't repaint on SetFont
        if( win->IsKindOf(CLASSINFO(wxTreeListCtrl) ) )
        {
            win->Refresh( true );
        }

        node = node->GetNext();
    }
}


wxString CUtils::FormatShortDateTime( int aTime )
{
    time_t t = (time_t)aTime;
    wxDateTime theTime( t );
    wxString ret;
    ret = wxT("");

    if( 0 == aTime )
    {
        ret.Append( wxString( _("Unspecified"), wxConvUTF8 ) );
    }else
    {
#ifdef __WXMAC__
        wxString strFormat(wxT("%b %d, %Y  %I:%M %p"));
#else
        wxString strFormat(wxT("%b %#d, %Y  %#I:%M %p"));
#endif
        ret = theTime.Format( strFormat.GetData() );
    }
    return ret;
}

wxString CUtils::FormatLongDateTime( int aTime )
{
    time_t t = (time_t)aTime;
    wxDateTime theTime( t );
    wxString ret;
    ret = wxT("");

    if( 0 == aTime )
    {
        ret.Append( wxString( _("Unspecified"), wxConvUTF8 ) );
    }else
    {
#ifdef __WXMAC__
		// The locale setting for en_US on Mac uses a 24-hour clock, override to 12 hour
        wxString strFormat(wxT("%r, %e %b %G"));
        ret = theTime.Format( strFormat.GetData() );
#else
        wxString strFormat(wxT("%X, %#d %b %Y"));
        ret = theTime.Format( strFormat.GetData() );
#endif
    }
    return ret;
}

wxString CUtils::FormatShortDate( int aTime )
{
    time_t t = (time_t)aTime;
    wxDateTime theTime( t );
    wxString ret;
    ret = wxT("");

    if( 0 == aTime )
    {
        ret.Append( wxString( _("Unspecified"), wxConvUTF8 ) );
    }else
    {
        wxString strFormat(wxT("%x"));
        ret = theTime.Format( strFormat.GetData() );
    }
    return ret;
}

void CUtils::UseStandardAccelerators( wxWindow & window )
{
#define STD_ACCELERATOR_COUNT 1

    wxAcceleratorEntry entries[STD_ACCELERATOR_COUNT];
    int nAccel = 0;

    // Find /////////////////////////////////////////////////////////////////////
    entries[nAccel++].Set(wxACCEL_CTRL,  (int)'F',     wxID_FIND);

    wxAcceleratorTable accel(STD_ACCELERATOR_COUNT, entries);
    window.SetAcceleratorTable(accel);

#undef STD_ACCELERATOR_COUNT
}

//*****************************************************************************
void CUtils::RestoreWindow( wxTopLevelWindow * window )
{
    window->Show();
    if (window->IsIconized())
        window->Iconize(false);
    window->Raise();
}

//*****************************************************************************
void CUtils::SetFocus( wxWindow * window )
{
#ifdef __WXMSW__
    // wx seems to keep track of the last active child, then activate that child again
    // when setting focusing to the parent.  Sometime we really want to set focus to the 
    // parent, so do it directly...
    HWND hwnd = (HWND) window->GetHandle();
    ::SetFocus(hwnd);
#elif defined(__WXMAC__)
    ControlRef ref = (ControlRef) window->GetHandle();
    SetKeyboardFocus( GetControlOwner( ref ), ref, 1 );
#else
    window->SetFocus();
#endif
}

//*****************************************************************************
void CUtils::StealFocus( wxWindow * window )
{
#ifdef WIN32
    // This machination will cause the IDE to hang, so skip if debugging
    if (!IsDebuggerPresent())
    {
        HWND hwnd = (HWND) window->GetHandle();

        //Attach foreground window thread
        //to our thread
        AttachThreadInput(
            GetWindowThreadProcessId(
                ::GetForegroundWindow(),NULL),
            GetCurrentThreadId(),TRUE);

        //Do our stuff here ;-)
        SetForegroundWindow(hwnd);
        ::SetFocus(hwnd); //Just playing safe

        //Detach the attached thread
        AttachThreadInput(
            GetWindowThreadProcessId(
                ::GetForegroundWindow(),NULL),
            GetCurrentThreadId(),FALSE);
    }
    else
        window->Raise( );
#else
    window->Raise( );
#endif

}

//#include <gtk/gtk.h>
// There is currently a conflict between wxWidget headers and latest Gtk
// libs, so declare function we need here
extern "C" {    
    typedef struct _GtkWindow GtkWindow;
    void gtk_window_set_modal(GtkWindow*, bool);
}

//*****************************************************************************
void CUtils::SetModal( wxWindow * window, bool modal )
{
#ifdef __WXGTK__
    GtkWindow * gtkwin = (GtkWindow*) window->GetHandle();
    gtk_window_set_modal(gtkwin, modal);
#endif    
}

//*****************************************************************************
void CUtils::DisableDWM( )
{
#ifdef __WXMSW__
    static bool m_bLoadDWM;
    if (!m_bLoadDWM)
    {
        m_hDWMLib = LoadLibrary(wxT("dwmapi.dll"));
        m_bLoadDWM = true;
    }
    
    if (m_hDWMLib != NULL)
	{
        HRESULT (__stdcall *pDwmEnableComposition)(BOOL fEnable);
        HRESULT (__stdcall *pDwmIsCompositionEnabled)(BOOL *pfEnabled);

        pDwmEnableComposition = (HRESULT (__stdcall *)(BOOL)) GetProcAddress(m_hDWMLib, "DwmEnableComposition");
        pDwmIsCompositionEnabled = (HRESULT (__stdcall *)(BOOL*)) GetProcAddress(m_hDWMLib, "DwmIsCompositionEnabled");

        if (pDwmEnableComposition && pDwmIsCompositionEnabled)
        {
            BOOL flag = FALSE;
            pDwmIsCompositionEnabled(&flag);
            if (flag)
            {
                pDwmEnableComposition(FALSE);
                m_bEnableDWM = true;
            }
        }
	}
#endif
}

//*****************************************************************************
void CUtils::RestoreDWM( )
{
#ifdef __WXMSW__
    if (m_hDWMLib != NULL && m_bEnableDWM)
	{
        HRESULT (__stdcall *pDwmEnableComposition)(BOOL fEnable);

        pDwmEnableComposition = (HRESULT (__stdcall *)(BOOL)) GetProcAddress(m_hDWMLib, "DwmEnableComposition");

        if (pDwmEnableComposition)
        {
            pDwmEnableComposition(TRUE);
        }

        m_bEnableDWM = false;
	}
#endif
}

//*****************************************************************************
void CUtils::DisableTheme( wxWindow * window )
{
#ifdef WIN32
    HWND hWnd = (HWND)window->GetHandle();
    HMODULE hModUXT = LoadLibrary(wxT("UxTheme.DLL"));

    if ( hWnd && hModUXT != NULL)
    {
         typedef HRESULT (*SetWindowThemeFunc)(HWND hwnd,LPCWSTR pszSubAppName,LPCWSTR pszSubIdList);
         BOOL freeResult, runTimeLinkSuccess = FALSE;
         SetWindowThemeFunc SetThemePtr = NULL;

         //Get pointer to our function using GetProcAddress:
         SetThemePtr = (SetWindowThemeFunc)GetProcAddress(hModUXT, "SetWindowTheme");

         // If the function address is valid, call the function.
         if (runTimeLinkSuccess = (NULL != SetThemePtr))
         {
            (SetThemePtr)(hWnd,L" ",L" ");
         }
         //Free the library:
         freeResult = FreeLibrary(hModUXT);
    }
#endif
}

void CUtils::StayOnTop(wxTopLevelWindow *pWindow)
{
#ifdef __WXMAC__
	WindowGroupRef group = NULL ; 
    group = GetWindowGroupOfClass(kUtilityWindowClass); 
    if ( 0 == group && 0 != pWindow->GetParent() ) 
    { 
    	WindowRef parenttlw = (WindowRef)pWindow->GetParent()->MacGetTopLevelWindowRef(); 
    	if( 0 != parenttlw ) 
        	group = GetWindowGroupParent( GetWindowGroup(parenttlw) ); 
    } 
    if ( 0 != group ) 
    	SetWindowGroup( (WindowRef)pWindow->MacGetWindowRef(), group ); 

    HIWindowChangeClass( (OpaqueWindowPtr*)pWindow->MacGetWindowRef(), kUtilityWindowClass ); 
#endif
}

#ifdef WIN32

BOOL CALLBACK EnumThreadWndProc(HWND hwnd, LPARAM lParam)
{
    HWND top = (HWND)lParam;

    TCHAR classname[MAX_PATH];
    ::GetClassName(hwnd, classname, MAX_PATH);
    // a classname of #32770 indicates that the window is a dialog
    if (_tcscmp(classname, _T("#32770"))==0 && IsWindow(hwnd))
    {
        GetWindowText(hwnd,classname, MAX_PATH);
        if (GetAncestor(hwnd, GA_ROOTOWNER) == top)
        {
            EndDialog(hwnd,-1);
        }
    }
    
    return TRUE;
}
#endif

//*****************************************************************************
void CUtils::CloseDialogs( wxWindow * window )
{
    bool closed = false;
    wxWindow * top = window ? wxGetTopLevelParent( window ) : NULL;

    wxWindowList::Node* node = wxTopLevelWindows.GetFirst();
    while (node)
    {
        wxDialog* dialog = wxDynamicCast(node->GetData(), wxDialog);
        wxWindow* parent = dialog ? dialog->GetParent() : NULL;

        if (dialog && parent)
        {
            // Find top level window that owns this dialog and compare against specified window
            parent = wxGetTopLevelParent(parent);
            if (parent)
            {
                wxLogDebug(wxT("Checking %s against %s, %s"), dialog->GetTitle().c_str(), top->GetLabel().c_str(), parent->GetLabel().c_str());
                if (top  == NULL || top  == parent)
                {
                    wxLogDebug(wxT("Closing dialog %s"), dialog->GetTitle().c_str());
                    closed = true;
                    dialog->Close();
                }
            }
        }
        node = node->GetNext();
    }

    if (closed)
        MsgWait(200,1);

#ifdef WIN32
    // wxMessageDialogs don't get added to the top level window list, so use win32 API to find and remove.
    EnumThreadWindows( GetCurrentThreadId(), EnumThreadWndProc, top ? (LPARAM)top->GetHandle() : (LPARAM)NULL );
#endif

}


//*****************************************************************************
void CUtils::ReportErrorMsg(unsigned dwError, bool bShow, wxString& msg, wxWindow* pWindow )
{
    if( !msg.IsEmpty() )
    {
        wxString        strTitle( _("Error Message"), wxConvUTF8 );
        wxMessageDialog dlg( pWindow, msg, strTitle );
        dlg.ShowModal( );
    }
}


//*****************************************************************************
void CUtils::GetServerStatusMsg( int iStatus, wxString &strStatusMsg )
{
    switch( iStatus )
    {
        case Failed:
            strStatusMsg.Append( wxString( _("Unknown failure"), wxConvUTF8 ) );
            break;
        case InvalidPIN:
            strStatusMsg.Append( wxString( _("Invalid PIN"), wxConvUTF8 ) );
            break;
        case InvalidSessionID:
            strStatusMsg.Append( wxString( _("Invalid Session ID"), wxConvUTF8 ) );
            break;
        case InvalidMeetingID:
            strStatusMsg.Append( wxString( _("Invalid Meeting ID"), wxConvUTF8 ) );
            break;
        case InvalidSubmeetingID:
            strStatusMsg.Append( wxString( _("Invalid Submeeting ID"), wxConvUTF8 ) );
            break;
        case InvalidUserID:
            strStatusMsg.Append( wxString( _("Invalid User ID"), wxConvUTF8 ) );
            break;
        case InvalidParticipantID:
            strStatusMsg.Append( wxString( _("Invalid Participant ID"), wxConvUTF8 ) );
            break;
        case InvalidServiceID:
            strStatusMsg.Append( wxString( _("Invalid Service ID"), wxConvUTF8 ) );
            break;
        case DuplicateID:
            strStatusMsg.Append( wxString( _("Duplicate ID"), wxConvUTF8 ) );
            break;
        case InvalidParameter:
            strStatusMsg.Append( wxString( _("Invalid Parameter"), wxConvUTF8 ) );
            break;
        case ParameterParseError:
            strStatusMsg.Append( wxString( _("Error parsing parameters"), wxConvUTF8 ) );
            break;
        case DuplicateUserName:
            strStatusMsg.Append( wxString( _("Duplicate User Name"), wxConvUTF8 ) );
            break;
        case PINAllocationError:
            strStatusMsg.Append( wxString( _("Error allocating PINs"), wxConvUTF8 ) );
            break;
        case NoClientConnection:
            strStatusMsg.Append( wxString( _("Connection to client lost"), wxConvUTF8 ) );
            break;
        case PINExpired:
            strStatusMsg.Append( wxString( _("The PIN has expired"), wxConvUTF8 ) );
            break;
        case PINNotValidYet:
            strStatusMsg.Append( wxString( _("The PIN is not yet valid"), wxConvUTF8 ) );
            break;
        case InvalidAddress:
            strStatusMsg.Append( wxString( _("Invalid Address"), wxConvUTF8 ) );
            break;
        case NotAuthorized:
            strStatusMsg.Append( wxString( _("User is not authorized"), wxConvUTF8 ) );
            break;
        case NotMember:
            strStatusMsg.Append( wxString( _("User if not a member"), wxConvUTF8 ) );
            break;
        case InsufficientResources:
            strStatusMsg.Append( wxString( _("Insufficient Resources to perform operation"), wxConvUTF8 ) );
            break;
        case NotLoaded:
            strStatusMsg.Append( wxString( _("Meeting information not Loaded"), wxConvUTF8 ) );
            break;
        case AlreadyConnected:
            strStatusMsg.Append( wxString( _("Connected is already established"), wxConvUTF8 ) );
            break;
        case NotConnected:
            strStatusMsg.Append( wxString( _("Not connected to server"), wxConvUTF8 ) );
            break;
        case NotActive:
            strStatusMsg.Append( wxString( _("Not Active"), wxConvUTF8 ) );
            break;
        case NoPhoneNumber:
            strStatusMsg.Append( wxString( _("A Phone Number is required"), wxConvUTF8 ) );
            break;
        case InAnotherMeeting:
            strStatusMsg.Append( wxString( _("User is already in another meeting"), wxConvUTF8 ) );
            break;
        case MeetingExists:
            strStatusMsg.Append( wxString( _("Meeting already exists or is still shutting down"), wxConvUTF8 ) );
            break;
        case InvalidPassword:
            strStatusMsg.Append( wxString( _("Password is invalid"), wxConvUTF8 ) );
            break;
        case Disconnected:
            strStatusMsg.Append( wxString( _("Connection to server has been lost"), wxConvUTF8 ) );
            break;
        case NotWhileLecture:
            strStatusMsg.Append( wxString( _("Operation not allowed while in Lecture mode"), wxConvUTF8 ) );
            break;
        case MeetingLocked:
            strStatusMsg.Append( wxString( _("Operation not allowed while meeting is Locked"), wxConvUTF8 ) );
            break;
        case PhoneNumberNotFound:
            strStatusMsg.Append( wxString( _("Phone Number not found"), wxConvUTF8 ) );
            break;
        case MeetingStartTimeout:
            strStatusMsg.Append( wxString( _("Timeout waiting for meeting to start"), wxConvUTF8 ) );
            break;
        case NoPort:
            strStatusMsg.Append( wxString( _("No Port available"), wxConvUTF8 ) );
            break;
        case MtgArchiverTimeout:
            strStatusMsg.Append( wxString( _("Timeout waiting for meeting archiver"), wxConvUTF8 ) );
            break;
        case MeetParamNotFound:
            strStatusMsg.Append( wxString( _("Meeting parameter not found"), wxConvUTF8 ) );
            break;
        case InvalidCallID:
            strStatusMsg.Append( wxString( _("Invalid call center ID"), wxConvUTF8 ) );
            break;
        case NotConferenced:
            strStatusMsg.Append( wxString( _("Operation not allowed while meeting is not Conferenced"), wxConvUTF8 ) );
            break;
        case MissingPIN:
            strStatusMsg.Append( wxString( _("PIN is missing"), wxConvUTF8 ) );
            break;
        case NoAvailResource:
            strStatusMsg.Append( wxString( _("No Resources are available"), wxConvUTF8 ) );
            break;
        case DBAccessError:
            strStatusMsg.Append( wxString( _("Error during database access"), wxConvUTF8 ) );
            break;
        case AddressBookError:
            strStatusMsg.Append( wxString( _("Error accessing address book"), wxConvUTF8 ) );
            break;
        case DuplicateContact:
            strStatusMsg.Append( wxString( _("Contact already exists"), wxConvUTF8 ) );
            break;
        case ProfileSystemDeleteError:
            strStatusMsg.Append( wxString( _("Error deleting System Profile"), wxConvUTF8 ) );
            break;
        case ProfileInUseError:
            strStatusMsg.Append( wxString( _("Operation not allowed while Profile is in use"), wxConvUTF8 ) );
            break;
        case UnknownGroup:
            strStatusMsg.Append( wxString( _("Group is unknown"), wxConvUTF8 ) );
            break;
        case CommunitySystemDeleteError:
            strStatusMsg.Append( wxString( _("Error deleting System Community"), wxConvUTF8 ) );
            break;
        case CommunityNotFound:
            strStatusMsg.Append( wxString( _("Community was not found"), wxConvUTF8 ) );
            break;
        case CommunityStatusNotFound:
            strStatusMsg.Append( wxString( _("Community Status was not found"), wxConvUTF8 ) );
            break;
        case CommunityDisabled:
            strStatusMsg.Append( wxString( _("Operation not allowed while Community is Disabled"), wxConvUTF8 ) );
            break;
        case CommunityNameNotUnique:
            strStatusMsg.Append( wxString( _("Community Name is not unique"), wxConvUTF8 ) );
            break;
        case DirectoryNotFound:
            strStatusMsg.Append( wxString( _("Directory was not found"), wxConvUTF8 ) );
            break;
        case ProfileNotFound:
            strStatusMsg.Append( wxString( _("Profile was not found"), wxConvUTF8 ) );
            break;
        case DuplicateUsername:
            strStatusMsg.Append( wxString( _("Username already exists"), wxConvUTF8 ) );
            break;
        case MaxMeetingsExceeded:
            strStatusMsg.Append( wxString( _("Maximum number of meetings has been exceeded"), wxConvUTF8 ) );
            break;
        case ProfileNameNotUnique:
            strStatusMsg.Append( wxString( _("Profile Name already exists"), wxConvUTF8 ) );
            break;
        case InstantMeetingNotFound:
            strStatusMsg.Append( wxString( _("Instant Meeting was not found"), wxConvUTF8 ) );
            break;
        case UserNotFound:
            strStatusMsg.Append( wxString( _("User was not found"), wxConvUTF8 ) );
            break;
        case MaxPresenceExceeded:
            strStatusMsg.Append( wxString( _("Maximum number of Presence monitors has been exceeded"), wxConvUTF8 ) );
            break;
        case InvalidScreenName:
            strStatusMsg.Append( wxString( _("Invalid ScreenName"), wxConvUTF8 ) );
            break;
        case InvalidScreenPassword:
            strStatusMsg.Append( wxString( _("Invalid Screen Password"), wxConvUTF8 ) );
            break;
        case CommunityOwnerDeleteError:
            strStatusMsg.Append( wxString( _("Error deleting Community Owner"), wxConvUTF8 ) );
            break;
        case NoShareSession:
            strStatusMsg.Append( wxString( _("No ShareSession exists"), wxConvUTF8 ) );
            break;
        case ShareFailed:
            strStatusMsg.Append( wxString( _("Share failed to start"), wxConvUTF8 ) );
            break;
        default:
            strStatusMsg.Append( wxString( _("Unknown error"), wxConvUTF8 ) );
            break;
    }
}


//*****************************************************************************
bool CUtils::DecompressFile( wxString& aIn, wxString& aOut)
{
    wxFileInputStream inS(aIn);
    wxFileOutputStream outS(aOut);
    wxZlibInputStream inZ(inS, wxZLIB_GZIP);
    
    outS.Write(inZ);
    return true;
}

