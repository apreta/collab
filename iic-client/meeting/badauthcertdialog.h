/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef _BADAUTHCERTDIALOG_H_
#define _BADAUTHCERTDIALOG_H_


//(*Headers(BadAuthCertDialog)
#include <wx/radiobut.h>
#include <wx/dialog.h>
#include <wx/html/htmlwin.h>
//*)

#include <wx/html/htmlwin.h>
#include <wx/xml/xml.h>

#include "htmlgen.h"

class BadAuthCertDialog : public wxDialog
{
public:
	BadAuthCertDialog( wxWindow *parent, wxXmlNode *pRoot );
    virtual ~BadAuthCertDialog( );


    //(*Identifiers(BadAuthCertDialog)
    //*)

    /** Create the report */
    void CreateReport( wxXmlNode *pRoot );

    /** Add a report table row */
    void addRow( HtmlGen & r, const wxString & label, const wxString & value );

    bool    isRadioAccept( )    { return m_RadioAccept->GetValue( ); }
    bool    isRadioTemp( )      { return m_RadioTemp->GetValue( ); }
    bool    isRadioReject( )    { return m_RadioReject->GetValue( ); }

protected:

    //(*Handlers(BadAuthCertDialog)
    //*)

    //(*Declarations(BadAuthCertDialog)
    wxHtmlWindow* m_html1;
    wxRadioButton* m_RadioReject;
    wxRadioButton* m_RadioAccept;
    wxRadioButton* m_RadioTemp;
    //*)

private:
    /** Show the report */
    void DisplayHTML( );

    void getField( wxString fName, wxString strIn, wxString &strOut );

    /** The parent window */
    wxWindow    *m_parent;

    wxString    m_strHTML;

    DECLARE_EVENT_TABLE()
};
#endif
