#include "app.h"
#include "invitedialog.h"
#include "meetingcentercontroller.h"
#include "meetingmainframe.h"

//(*InternalHeaders(InviteDialog)
#include <wx/xrc/xmlres.h>
//*)

//(*IdInit(InviteDialog)
//*)

BEGIN_EVENT_TABLE(InviteDialog,wxDialog)
	//(*EventTable(InviteDialog)
	//*)
END_EVENT_TABLE()

InviteDialog::InviteDialog(wxWindow* parent)
{
	//(*Initialize(InviteDialog)
	wxXmlResource::Get()->LoadObject(this,parent,_T("InviteDialog"),_T("wxDialog"));
	m_HtmlWindow = (wxHtmlWindow*)FindWindow(XRCID("ID_HTMLWINDOW1"));
	Panel1 = (wxPanel*)FindWindow(XRCID("ID_PANEL1"));
	//*)
}

InviteDialog::~InviteDialog()
{
	//(*Destroy(InviteDialog)
	//*)
}


bool InviteDialog::ShowInvite(meeting_invite_t invite)
{
    wxString    strDesc( invite->description, wxConvUTF8 );
    wxString    strHost( invite->from_name, wxConvUTF8 );
    wxString    strMID( invite->meeting_id, wxConvUTF8 );
    wxString    strPID( invite->participant_id, wxConvUTF8 );
    wxString    strPName( invite->participant_name, wxConvUTF8 );
    wxString    strTitle( invite->title, wxConvUTF8 );
    wxString    strMessage( invite->message, wxConvUTF8 );
    wxString    strTime( _("Now") );

    if( !strDesc.IsEmpty( ) )
    {
        strDesc.Replace( wxT("\r\n"), wxT("<BR>") );
        strDesc.Replace( wxT("\n\r"), wxT("<BR>") );
        strDesc.Replace( wxT("\r"), wxT("<BR>") );
        strDesc.Replace( wxT("\n"), wxT("<BR>") );
        strDesc.Replace( wxT("<CR>"), wxT("<BR>") );
    }

    HtmlGen r;
    r.clear( false );

    r.startHtml();
    if( invite->invite_type  ==  InviteNormal )
    {        
        //  Format a normal type of invitation...
        if( !strMessage.IsEmpty( ) )
            r.simpletext( strMessage );
        else
            r.simpletext( _("You have been invited to a meeting.<BR><BR>") );

        //startTable( wxColor & bg, const wxString & width, int border = 0, int cellspacing = 2, int cellpadding = 1 );
        wxColor     bg( GetBackgroundColour( ) );
        r.startTable( bg, _T("85%") );

        MeetingView::addRow( r, _("Title:"), strTitle );
        MeetingView::addRow( r, _("Host:"), strHost );
        if( invite->time  !=  0 )
        {
            //  Format time into strTime
            strTime = CONTROLLER.DateTimeDecode( invite->time );
        }
        MeetingView::addRow( r, _("Time:"), strTime );
        MeetingView::addRow( r, _("Meeting ID:"), strMID );
        wxString    strRest;
        if( strPID.StartsWith( wxT("part:"), &strRest ) )
            MeetingView::addRow( r, _("Participant PIN:"), strRest );
        else
            MeetingView::addRow( r, _("Participant PIN:"), strPID );

        if( !strDesc.IsEmpty( ) )
        {
            MeetingView::addRow( r, _("Description:"), strDesc );
        }

        r.endTable();

        if( invite->options & InvitationDataOnly )
            r.simpletext( _("This is a data only meeting.<BR>") );
        else if( invite->options & InvitationVoiceOnly )
            r.simpletext( _("This is a voice only conference call.<BR>") );
        else
            r.simpletext( _("This is a voice and data meeting.<BR>") );

        if ( CONTROLLER.IsMeetingActive() )
            r.simpletext( _("<BR>If you accept, you will be removed from your current meeting first.<BR>") );
            
        r.simpletext( _("<BR>Would you like to attend this meeting?") );
        
    }
    else
    {
        wxString strInviteMessage;
        strInviteMessage = _("Incoming web call from ");
        strInviteMessage += strHost;
        strInviteMessage += _T("<BR>");

        r.simpletext( strInviteMessage );

        wxColor     bg( GetBackgroundColour( ) );
        r.startTable( bg, _T("85%") );

        MeetingView::addRow( r, _("Title:"), strTitle );

        if( !strDesc.IsEmpty( ) )
        {
            MeetingView::addRow( r, _("Description:"), strDesc );
        }

        r.endTable();

        if ( CONTROLLER.FindMeetingWindow( strMID ) != NULL )
            r.simpletext( _("<BR>Would you like to answer this call?") );
        else
        {
            if ( CONTROLLER.IsMeetingActive() )
                r.simpletext( _("<BR>If you accept, you will be removed from your current meeting first.<BR>") );

            r.simpletext( _("<BR>Would you like to answer this call and attend the meeting?") );
        }
    }

    r.endHtml( );

    //m_HtmlWindow->SetBorders( 0 );
    m_HtmlWindow->SetPage( r.buf( ) );
    m_HtmlWindow->Layout();

    // Set minimum dialog width
    wxSize minsize(300,-1);
    SetSize(minsize);
    SetMinSize(minsize);
    
    // Fit the HTML window to the size of its contents
    wxSize size(m_HtmlWindow->GetInternalRepresentation()->GetWidth()+10,
                m_HtmlWindow->GetInternalRepresentation()->GetHeight());
    m_HtmlWindow->SetSize(size);
    m_HtmlWindow->SetMinSize(size);
    Fit();
    Layout();

    Connect(XRCID("wxID_YES"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&InviteDialog::OnInviteAccepted);
    Connect(XRCID("wxID_NO"), wxEVT_COMMAND_BUTTON_CLICKED, (wxObjectEventFunction)&InviteDialog::OnInviteRejected);

    return ShowModal() == wxID_YES;
}

void InviteDialog::OnInviteAccepted( wxCommandEvent& event )
{
    EndModal(wxID_YES);
}

void InviteDialog::OnInviteRejected( wxCommandEvent& event )
{
    EndModal(wxID_NO);
}
