/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef PCPHONEDIALOG_H
#define PCPHONEDIALOG_H

//(*Headers(PCPhoneDialog)
#include <wx/button.h>
#include <wx/dialog.h>
#include <wx/gauge.h>
#include <wx/sizer.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
//*)

class PCPhoneDialog: public wxDialog
{
	public:

		PCPhoneDialog(wxWindow* parent,wxWindowID id = -1);
		virtual ~PCPhoneDialog();

		//(*Identifiers(PCPhoneDialog)
		//*)

	protected:

		//(*Handlers(PCPhoneDialog)
		//*)

		//(*Declarations(PCPhoneDialog)
		wxStaticText* StaticText1;
		wxTextCtrl* m_VoipStatusText;
		wxButton* m_TTPad1_Button;
		wxButton* m_TTPad2_Button;
		wxButton* m_TTPad3_Button;
		wxButton* m_TTPad4_Button;
		wxButton* m_TTPad5_Button;
		wxButton* m_TTPad6_Button;
		wxButton* m_TTPad7_Button;
		wxButton* m_TTPad8_Button;
		wxButton* m_TTPad9_Button;
		wxButton* m_TTPadStar_Button;
		wxButton* m_TTPad0_Button;
		wxButton* m_TTPadPound_Button;
		wxButton* m_ConnectToMeeting_Button;
		wxButton* m_DisconnectButton;
		wxButton* m_AudioProperties_Button;
		wxStaticText* StaticText2;
		wxGauge* m_Vol_Gauge;
		wxStaticText* StaticText3;
		wxGauge* m_Mic_Gauge;
		wxTextCtrl* TextCtrl2;
		//*)

    public:
        /** Update the voip status text */
        void UpdateVoipStatus( wxCommandEvent& event );

        /** Update the voip levels */
        void UpdateVoipLevels( wxCommandEvent& event );

	private:

		DECLARE_EVENT_TABLE()
};

#endif
