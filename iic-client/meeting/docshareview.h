/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#if !defined(PANESHAREDOCUMENT_H)
#define PANESHAREDOCUMENT_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "drawingobjects.h"
#include "webdav.h"
#include "events.h"

#include <wx/dynlib.h>
#include <wx/metafile.h>

#include <string>
#include <vector>

#ifndef __WXMSW__
#define SW_HIDE 1
#endif

#ifdef WIN32
#define METAFILE
#endif

const static int kDocSharePixelsPerUnitX    = 20;
const static int kDocSharePixelsPerUnitY    = 20;
const static int kDocShareUnitMinChange     = 1;

#if defined(LINUX) || defined(MACOSX)
typedef unsigned int DWORD;
#endif

class CIICDoc;
class DocShareCanvas;
class DocShareView;
class SendRecvProgressDialog;
class SendFileThread;
class RetrieveFileThread;
class MeetingMainFrame;


typedef struct LINESIZE
{
    wxString    desc;
    int         size;
}   LINESIZE;


typedef struct LINESTYLE
{
    wxString    desc;
    int         style;
}   LINESTYLE;



class CDocPage
{
public:
    wxString    m_strTitle;
    wxString    m_strFile;
    wxString    m_strAltFile;
    double      m_dblScale;
    double      m_dblAltScale;
    bool        m_fFitToWidth;
};


typedef	std::vector<CDocPage> PAGEVECTOR;


struct ParamsBase
{
    ParamsBase() { m_iReferences = 1; }
    
    void AddRef() { ++m_iReferences; }
    void RemoveRef() { if (--m_iReferences == 0) delete this; }

    DocShareView* _this;
    int m_iReferences;
    bool m_bStopped;
    long hr;
};


// Retrieving a file
struct RetrieveParams : ParamsBase
{
    wxString aDocId;
    wxString aPageId;
    wxString out;
    wxString strStatusCode;
    wxString strStatusText;
    bool aSaveToFile;
};


// Sending a file
struct SendingParams : ParamsBase
{
    wxString    m_TargetUrl;
    wxString    m_TargetPath;
    wxString    m_FileName;
    wxString    strStatusCode;
    wxString    strStatusText;
    int         iChunk;
};


// Sending an HTTP request
struct RequestParams : ParamsBase
{
    wxString    szReq;
    wxString    strFileURL;
    wxString    strFilePath;
    const BYTE* pData;
    unsigned long   dwLength;
    wxString    strStatusCode;
    wxString    strStatusText;
    bool        bAddLockToken;
    wxString    strLockToken;
};


//*****************************************************************************
//   RetrieveFileThread class
class RetrieveFileThread : public wxThread
{
public:
    RetrieveFileThread( RetrieveParams *pParams );

    // thread execution starts here
    virtual void *Entry();

    // called when the thread exits - whether it terminates normally or is
    // stopped with Delete() (but not when it is Kill()ed!)
    virtual void OnExit();

private:
    RetrieveParams   *m_pParams;
};


//*****************************************************************************
//   SendFileThread class
class SendFileThread : public wxThread
{
public:
    SendFileThread( SendingParams *pParams );

    // thread execution starts here
    virtual void *Entry();

    // called when the thread exits - whether it terminates normally or is
    // stopped with Delete() (but not when it is Kill()ed!)
    virtual void OnExit();

private:
    SendingParams   *m_pParams;
};


//*****************************************************************************
//   SendRequestThread class
class SendRequestThread : public wxThread
{
public:
    SendRequestThread( RequestParams *pParams );

    // thread execution starts here
    virtual void *Entry();

    // called when the thread exits - whether it terminates normally or is
    // stopped with Delete() (but not when it is Kill()ed!)
    virtual void OnExit();

public:
    RequestParams   *m_pParams;
};


//*****************************************************************************
typedef struct _UploadExtData
{
    wxString            strDLLName;
    wxDynamicLibrary    *pdllUpload;
    wxString            strPri;
    wxString            strSec;
    int                 iRelPri;
    bool                fUsed;
}   UploadExtData;

typedef	std::vector<_UploadExtData> EXTDATAVECTOR;


//*****************************************************************************
typedef struct _UploadFilterData
{
    wxString            strDLLName;
    wxDynamicLibrary    *pdllUpload;
    int                 iStart;
    int                 iEnd;
}   UploadFilterData;

typedef	std::vector<_UploadFilterData> FILTERDATAVECTOR;


//*****************************************************************************
//  Define the DocShareCanvas class
class DocShareCanvas: public wxScrolledWindow
{
public:
    DocShareView    *m_pView;

    DocShareCanvas(wxWindow *pParent, const wxPoint& pos, const wxSize& size, const long style);
    ~DocShareCanvas( );

    void SetPresenter(bool flag, bool isModerator);

    virtual void    OnDraw( wxDC& dc );
    void            OnMouseEvent( wxMouseEvent& event );
    void            OnChar( wxKeyEvent& event );

    void            OnSize( wxSizeEvent &event );
    
    void            SetPointerPos( int x, int y );

    wxHtmlWindow* m_ShareHTML;
    wxStaticBitmap* m_Pointer;

    wxBitmap m_PointerBitmap;
    wxPoint m_PointerPos;

    DECLARE_EVENT_TABLE()
};


//*****************************************************************************
//  DocShareView class
class DocShareView : public wxView
{
    friend class DocShareCanvas;
    
    DECLARE_DYNAMIC_CLASS(DocShareView)

    DocShareView();
    virtual ~DocShareView();

// Attributes
private:
    enum {MA_NONE, MA_DRAGGING, MA_RESIZING, MA_MULTI_SELECTION};

    wxTextCtrl      *m_pCtlEdit;
    SendRecvProgressDialog	*m_pDialogProgress;
    int                     m_iAllowHideProgress;

    CIICDoc         *m_pDocument;
    DocShareCanvas  *m_pCanvas;
    wxString        m_strMeetingID;
    wxString        m_strLockToken;
    wxString        m_strLastModified;
    bool            m_bPresenter;
    bool            m_bRemoteControl;

    PAGEVECTOR      m_saPages;          // Keep all the index
    int				m_nPageIndex;       // Current page

    bool            m_in_handler;
    std::list<DocshareEvent*> m_event_queue;

    EXTDATAVECTOR       m_saExtData;
    FILTERDATAVECTOR    m_saFilterData;

    wxImage             *m_pImage;          // Backgound image
#ifdef METAFILE
    wxEnhMetaFile       *m_pMetafile;       // Backgound image
    static const bool   m_fEMFSupported     = true;
#else
    static const bool   m_fEMFSupported     = false;
#endif
    bool                m_fOpenedAltImage;
    int             m_nImageCount;

    wxMenu          *m_pcontextMenu;
    wxMenu          *m_pcontextMenuObj;
    wxMenu          *m_pcontextMenuLineStyle;
    wxMenu          *m_pcontextMenuLineSize;

    wxMenuItem      *m_pmenuItemLineSize;
    wxMenuItem      *m_pmenuItemLineStyle;

    wxString        m_strDocId;		// Document ID (folder Name)

    bool            m_bRemotePointer;
    wxPoint         m_pointerPos;              // Last remote pointer pos
    wxString        m_strObjType;				// Object to create
    wxPoint         m_ptStart;					// Mouse click point
    wxPoint         m_ptEnd;					// Mouse release point
    int             m_nAction;					// Mouse operation
    bool            m_bDragging;				// Dragging started
    int             m_nDragSide;				// Side of object begin dragged
    wxSize          m_sizeDelta;				// Position where the Mouse on the object
    CDrawingObjects m_drawingObjects;			// Holds all the objects
    CBaseDrawingObject* m_pLastSelectedItem;	// Last selected item (all the selected items are stored inside the m_drawingObjects)
    bool            m_bDirty;					// Document is modified
    bool            m_bCreating;				// Indicates that the current object is just created
    bool            m_bSelected;				// If click again on an object, enter edit mode
    bool            m_bFileLocked;				// Indicates if we have the right to modified the document
    bool            m_fReadOnly;				// In case file locked by another user, allow to view
    wxPoint         m_ptPasteOffset;			// To remember the last rButton click position for paste

    int             m_lock;
    wxString        m_strUserName;
    wxString        m_strPasswd;
    wxString        m_iicServer;

    double          m_dblZoom;
    double          m_dblImageZoom;
    int             m_iLastUnitsX;
    int             m_iLastUnitsY;

    bool            m_bRestrictedUser;
    bool            m_bRefreshRequested;

    CWebDavUtils	m_WebDavUtils;

    wxCursor        m_CursorLeftright;
    wxCursor        m_CursorCross;
    wxCursor        m_CursorVert;
    wxCursor        m_CursorNESW;
    wxCursor        m_CursorNWSE;
    wxCursor        m_CursorHand;
    wxCursor        m_CursorMove;

    wxTimer         m_timer;
    bool            m_fIgnoreUpdateSignal;      // Ignore timer event
    bool            m_fGrabFocus;               // Set focus to canvas during next timer event

    wxFontData      m_logFont;					// the default logFont
    int             m_iID_MENU_BASE;

    wxString            m_strSavedTitle;
    MeetingMainFrame    *m_pMMF;
    long            m_lPreLoadExeHnd;
    wxString        m_strPostLoadKillCmd;
    bool            m_bWhiteboarding;
    int             m_iXScrollRate;
    int             m_iYScrollRate;
    bool            m_fForceScroll;
    bool            m_fFitToWidth;

	int             m_iConversionResult;
	int             m_iConversionPage;

public:
    void    InitializeInfo(CIICDoc *pDocument, const wxString& aMeetingID);
    bool    Start( MeetingMainFrame *pMMF, DocShareCanvas *pCanvas, bool bWhiteboarding, bool bPresenter, bool bFileSelect, bool bPresentation = false );
    void    Stop( );

protected:
    bool    OnCreate( wxDocument *doc, long lFlags );
    void    OnDraw( wxDC* pDC );
    void    OnSize( wxSizeEvent &event );

    void    OnRefresh( wxCommandEvent& event );

    void    OnDocshareNew( wxCommandEvent& event );
    void    OnDocshareOpen( wxCommandEvent& event );
    void    OnDocshareSave( wxCommandEvent& event );
    void    OnDocshareSaveAs( wxCommandEvent& event );
    void    OnDocshareUploadDoc( wxCommandEvent& event );
    void    OnDocshareOrganizePages( wxCommandEvent& event );
    void    OnDocshareInsertPage( wxCommandEvent& event );
    void    OnDocshareFirstPage( wxCommandEvent& event );
    void    OnDocsharePreviousPage( wxCommandEvent& event );
    void    OnDocshareGotoPage( wxCommandEvent& event );
    void    OnDocshareNextPage( wxCommandEvent& event );
    void    OnDocshareLastPage( wxCommandEvent& event );
    void    OnDocshareCut( wxCommandEvent& event );
    void    OnDocshareCopy( wxCommandEvent& event );
    void    OnDocsharePaste( wxCommandEvent& event );
    void    OnDocshareDeleteSel( wxCommandEvent& event );
    void    OnDocshareClearPage( wxCommandEvent& event );
    void    OnDocshareClearAll( wxCommandEvent& event );
    void    OnDocshareUndo( wxCommandEvent& event );
    void    OnDocshareRedo( wxCommandEvent& event );

    void    OnDocsharePointer( wxCommandEvent& event );
    void    OnDocshareSelect( wxCommandEvent& event );
    void    OnDocshareLine( wxCommandEvent& event );
    void    OnDocshareArrow( wxCommandEvent& event );
    void    OnDocshareScribble( wxCommandEvent& event );
    void    OnDocshareHighlight( wxCommandEvent& event );
    void    OnDocshareRectangle( wxCommandEvent& event );
    void    OnDocshareEllipse( wxCommandEvent& event );
    void    OnDocshareTextbox( wxCommandEvent& event );
    void    OnDocshareCheckmark( wxCommandEvent& event );
    void    OnDocshareLineThickness( wxCommandEvent& event );
    void    OnDocshareLineStyle( wxCommandEvent& event );
    void    OnDocshareFontSettings( wxCommandEvent& event );
    void    OnDocshareLineColor( wxCommandEvent& event );
    void    OnDocshareBgColor( wxCommandEvent& event );
    void    OnDocshareTextColor( wxCommandEvent& event );
    void    OnDocshareToFront( wxCommandEvent& event );
    void    OnDocshareToBack( wxCommandEvent& event );
    void    OnDocshareTogTrans( wxCommandEvent& event );

    void    OnTimer( wxTimerEvent& event );         //  Update status of toolbar buttons

    void    OnCaptureLost( wxMouseCaptureLostEvent& event );

    void    OnLineThicknessSelected( wxCommandEvent& event );
    void    OnLineStyleSelected( wxCommandEvent& event );
    void    OnGotoPage( wxCommandEvent& event );

    void    OnLButtonDown( wxMouseEvent& event );
    void    OnLButtonDClick( wxMouseEvent& event );
    void    OnLButtonUp( wxMouseEvent& event );
    void    OnMotion( wxMouseEvent& event );
    void    OnRButtonDown( wxMouseEvent& event );

public:
    void    InvalidateSelection( IICRect& rect, bool bEraseBkgnd=true );
    void    RemoteControl( bool fRC );
    void    ForceFitToWidth( );
    void    ForceScale( );
    void    ForceScroll( );
    bool    IsScrollOnly( )     { return m_fForceScroll; }
    bool    IsWhiteboard( )     { return m_bWhiteboarding; }
    bool    IsFitToWidth( )     { return m_fFitToWidth; }
    bool    AllowFitToWidth( );

private:
    bool    CanEdit( );
    bool    IsPresenter( );
    bool    IsModerator( );
    void    EditDelete( bool aNotifyServer );
    void    DrawItem( wxDC& dc, CBaseDrawingObject *pBox, bool bEraseBkgnd=true );
    void    SetCursor( int Side );
    void    SetPageDirty( bool bDirty );
    void    SetRemotePointer( bool bEnable );
    void    InvalidateSelection( bool bEraseBkgnd=true );
    wxSize  GetWhiteboardSize( wxClientDC& pDC );
    void    InternalPaint( wxDC* pDC, IICRect& rcClientRect, int x, int y );
    void    HideEditWindow( );
    bool    CreateFont( const wxString& strFontFace, int iFontSize, const wxString& strFontStyle, wxFontData &pLogFont );

    void    InitializeScroll( bool fForceScroll );
    void    SetScrollbars( int iUnitsX, int iUnitsY );

public:
    long    SendFileEx( const wxString& aTargetUrl, const wxString& aTargetPath, const wxString& aFileName,
                        wxString& strStatusCode, wxString& strStatusText, int* piChunk );
    long    SendRequest( wxString& szReq, const wxString& strFileURL, const wxString& strFilePath,
                         const BYTE* pData, DWORD dwLength, wxString& strStatusCode, wxString& strStatusText,
                         bool bAddLockToken, const wxString& strLockToken, int* piChunkSent=NULL );
    long    RetrieveFile( const wxString& aDocId, const wxString& aPageId, wxString& out,
                          wxString& strStatusCode, wxString& strStatusText, bool aSaveToFile );
    void    OnDocshareEvent( DocshareEvent& event );
    long    AsyncSendRequest( wxString szReq, const wxString& strFileURL, const wxString& strFilePath,
                              const BYTE* pData, DWORD dwLength,
                              wxString& strStatusCode, wxString& strStatusText,
                              bool bAddLockToken, const wxString& strLockToken, bool fMultiFileSend = false );
    bool    Upload( wxString& strDocId, bool bPresentation = false );
    long    CopyDocuments( wxWindow * parent, wxString& fromMeeting, wxString& toMeeting, bool fDeleteSource = true );
    void    SendDrawOnWhiteBoard( wxString& meetingId, const wxString& actionXML );
    
private:
    bool    SelectInitialDoc( bool bFileSelect, wxString& strDocId, bool bPresentation = false );
    void    ProcessEventQueue( );
    void    ConnectToolbarEvents( );
    void    DisconnectToolbarEvents( );
    bool    LoadDocument( const wxString& aDocId );
//    bool    LoadPageMarkup( const wxString& aDocId, const wxString& aPageId, const wxString& aAltPageId=wxT(""), bool aNotifyServer=true, bool aLockFile=true );
    bool    LoadPageMarkup( const wxString& aDocId, const int iPageIndex, bool aNotifyServer=true, bool aLockFile=true );
    bool    UpdatePageMarkup( bool bUnlock=true, bool bUpdate=true, bool bForceUpdate=false, bool fSaveEmptyXML=false );
    long    UploadCurrentPage( );
    void    Download( const wxString& aDocId, const wxString& aPageId );
    void    SaveasImageEx( const wxString& strPath, const wxString& strType, const wxString& strPageSize );
    bool    ShowProgressWindow( wxString aTitle, wxString aStatus, bool fReceiveMode = false );
    void    HideProgressWindow( );
    long    AsyncRetrieveFile( const wxString& aDocId, const wxString& aPageId, std::string& out,
                               wxString& strStatusCode, wxString& strStatusText, bool aSaveToFile );
    long    AsyncSending( const wxString& targetFile, const wxString& targetPath, const wxString& filepath, wxString& strStatusCode, wxString& strStatusText, bool fMultiFileSend = false );

    long    LockFile( const wxString& strPageId, const wxString& strPagePath, bool bLockUnlock, wxString& strLockToken, wxString& strLastModified );
    long    GetLockInfo( const wxString& aUrl, const wxString& aUrlPath, wxString& strLockToken, wxString& strOwner,
                         wxString& strLastModified, wxString& statusCode, wxString& strStatusText);

    void    OnDocshareDrawing( const wxString& strDrawing );
    void    OnDocshareDownload( const wxString& strDocID, const wxString& strPageID );
    void    OnDocshareParticipantJoined( wxCommandEvent& event );
    void    OnConversionEvent( wxCommandEvent& event );

    void    Draw( const std::string& aDrawing );
    void    LoadIndex( );
    void    CloseDialogs( );
    void    ClearDocument( );
    void    ClearBackgroundImages( );
    void    SendAction( wxString Action );
    void    SendGotoAction( wxString Action, int nID );
    void    SendPointerAction( wxString Action, int x, int y );

    bool    LockCurrentPage( );
    int     SendDocShareFile( const wxString &strFile, wxString &strDocId );
	int		WaitForConversion( const wxString &strFilePath, wxString &strDocId );
    int     ConvertDocShareFile( const wxDynamicLibrary * pdll, const wxString &strFile, wxString &strDocId );
    int     SendProcessedFile( const wxString &strFilePath, wxString &strDocId, int iNumPages, wxString &strProcessedPath );
    int     SendDirectory( wxString &aDirectory, wxString &aDocId, int iNumFiles, bool aDeleteAfterSent );
    int     SendFilesWithExt( wxString &strDirectory, wxString &strTempURL, wxString &strTempPath, bool aDeleteAfterSent, wxString strExt );
    void    AllowHideProgress( bool fFlag );
    bool    FindUploadExts( );
    wxDynamicLibrary *LoadLocalConverter( wxString strExt );
    void    ScaleThePoint( wxPoint &pt );
    void    UnscaleThePoint( wxPoint &pt );
    void    ScaleTheRect( IICRect &rect );
    void    UnscaleTheRect( IICRect &rect );
    void    CalcScrolledPosition( int x, int y, int *xx, int *yy );
    void    CalcUnscrolledPosition( int x, int y, int *xx, int *yy );
    void    CalcScrolledPosition( IICRect &rect );
    void    CalcUnscrolledPosition( IICRect &rect );
    void    CalcScrolledPosition( wxPoint &pt );
    void    CalcUnscrolledPosition( wxPoint &pt );

    DECLARE_EVENT_TABLE()

};

#endif
