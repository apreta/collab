/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef MEETINGSETUPDIALOG_H
#define MEETINGSETUPDIALOG_H

//(*Headers(MeetingSetupDialog)
#include <wx/notebook.h>
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/checkbox.h>
#include <wx/datectrl.h>
#include <wx/spinctrl.h>
#include <wx/radiobut.h>
#include <wx/panel.h>
#include <wx/dateevt.h>
#include <wx/button.h>
#include <wx/dialog.h>
#include <wx/spinbutt.h>
//*)

#include <wx/dateevt.h>

#include <wx/sheet/sheet.h>
#include <wx/sheet/sheetedt.h>
#include "wxgridext.h"
#include "contacttreelist.h"
#include "imagelistcomposite.h"


WX_DEFINE_ARRAY(invitee_t, ArrInviteesToDelete);


class SelectContactDialog;
class FindContactsDialog;
class PresenceEvent;
class ImagePanel;
class FileManager;


//////////////////////////////////////////////////////////////////////////////////////
// Uncomment the following for diagnostics output vis wxLogDebug
//#define DEBUG_MEETINGSETUPDIALOG 1

//////////////////////////////////////////////////////////////////////////////////////
/**
  * This class maintains the association between an invitee grid row and it's invitee_t data.<br>
  * The Col_Contact cells make use of this class.
  */
class wxContactCellEditor : public GridCellTextEditor
{
public:
    wxContactCellEditor();


    /** Contact source tags */
    enum ContactSourceTags
    {
        ContactFromCAB = 0,     /**< The contact was obtained from the Community Address Book */
        ContactFromPAB = 1,     /**< The contact was obtained from the Personal Address Book */
        ContactFromUnknown = 3  /**< Don't know */
    };

    /** Constructor */
    wxContactCellEditor( invitee_t invt, contact_t contact = 0, bool fAddAdNewContact = false);

    /** Set the source data associations.
      * @param invt Ptr. to the invitee record.
      * @param contact Ptr. to the contact record.
      */
    void SetDataSource( invitee_t invt, contact_t contact = 0 )
    {
        m_invt = invt;
        m_contact = contact;
    }

    /** Set the source data associations.
      * @param invt Ptr. to the invitee record.
      * @param contact Ptr. to the contact record.
      */
    void SetContact( contact_t contact )
    {
        m_contact = contact;
    }

    /** Mark the contact as PAB */
    void SetFromPAB()
    {
        m_nContactSource = ContactFromPAB;
    }

    /** Mark the contact as CAB */
    void SetFromCAB()
    {
        m_nContactSource = ContactFromCAB;
    }

    /** Return true if this entry is to be created as a new contact
      * @return bool Returns m_fAssAsNewContact.
      */
    bool GetAddAsNewContact()
    {
        return m_fAddAsNewContact;
    }

    bool Copy(const wxContactCellEditor& other)
    {
        m_invt = other.m_invt;
        m_contact = other.m_contact;
        m_fAddAsNewContact = other.m_fAddAsNewContact;
        m_nContactSource = other.m_nContactSource;
        return GridCellTextEditor::Copy(other);
    }
    DECLARE_SHEETOBJREFDATA_COPY_CLASS(wxContactCellEditor,GridCellTextEditor)

public:
    /** Pointer to the associated invitee data */
    invitee_t m_invt;

    /** Pointer to the associated contact data:<br>
      * NOTES:<br>
      * For ad-hoc invitees, this will be NULL.<br>
      * For new invitees, this will initially be NULL.<br>
      */
    contact_t m_contact;

    /** Flag: Add this entry as a new contact in the meeting owners Personal Address Book (PAB)<br>
      * The existance of a contact_t does not imply creation of a new contact_t record in the database.<br>
      * A new contact_t record is added if this->m_invt.is_adhoc is true and this->m-fAddAsNewContact are both true
      * then a new contact_t record is written.
      */
    bool m_fAddAsNewContact;

    /** Contact source information */
    int m_nContactSource;
};

class MeetingSetupDialog: public wxDialog
{
public:

    MeetingSetupDialog(wxWindow* parent,wxWindowID id = -1);
    virtual ~MeetingSetupDialog();

    int ShowModal( );

    //(*Identifiers(MeetingSetupDialog)
    //*)

    /** State flags */
    enum StateFlags
    {
        State_Idle =                     0x00000000,/**< No particular state */
        State_WaitingForMeetingDetails = 0x00000001,/**< Waiting for meeting details */
        State_SavingNewContacts =        0x00000002,/**< Saving new PAB contact records */
        State_WaitingForPABLoad =        0x00000004,/**< Waiting for the PAB to reload from the server */
        State_AbortingProcess =          0x00000008,/**< Something is very wrong. Closing editor */
        State_Sorting =                  0x00000010
    };

    /** Oprating modes */
    enum OperatingModes
    {
        Mode_Unknown,               /**< Mode not defined */
        Mode_NewMeeting,            /**< A new meeting is being created */
        Mode_EditMeeting,           /**< An exiting meeting is being edited */
        Mode_CopyMeeting,           /**< An existing meeting is being copied */
        Mode_PrepareStartMeeting    /**< A meeting is being started. Let the user make any last minute changes. */
    };


    /** Invitee columns */
    enum InviteeColumns
    {
        Col_Status = 0,      /**< Graphical state indicator */
        Col_Contact,        /**< Contact name */
        Col_Phone,          /**< Contact phone number */
        Col_Email,          /**< Contact email */
        Col_IM,             /**< Contact IM */
        Col_Moderator,      /**< Contact is moderator */
        Max_Columns         /**< The total number of grid columnns (MUST BE LAST enum) */
    };

    /** Invitee phone number selections.<br>
      * There is a 1:1 relationship between these values
      * and the combo box text displayed to the user.<br>
      * NOTE: At the present time, the Phone_FollowMe ( SelPhoneDefault ) entry is NOT USED.
      */
    enum PhoneNumberSelections
    {
        Phone_DontCall = 0,     /**< Don't call. invt.selphone = SelPhoneThis, invt.phone = null */
        Phone_FollowMe,         /**< The default invt.selphone = SelPhoneDefault, invt.phone = Mtg. owner default ? */
        Phone_Business,         /**< Business #  invt.selphone = SelPhoneThis, invt.phone = contact.busphone */
        Phone_Home,             /**< Home #      invt.selphone = SelPhoneThis, invt.phone = contact.homephone */
        Phone_Mobile,           /**< Mobile #    invt.selphone = SelPhoneThis, invt.phone = contact.mobilephone */
        Phone_Other,            /**< Other #     invt.selphone = SelPhoneThis, invt.phone = contact.otherphone */
        Phone_PC,               /**< PC Phone    invt.selphone = SelPhonePC,   invt.phone = "Use PC phone" */
        Phone_Custom,           /**< User #      invt.selphone = SelPhoneThis, invt.phone = "The custom number" */
        Phone_EnterPhone,       /**< Command: Ask the user for a phone number. This new selection is then Phone_Custom */
        Phone_EnterPhoneValue,  /**< The user entered phone number */
        Phone_Business2,        /**< Business #  invt.selphone = SelPhoneThis, invt.phone = 2nd contact.busphone */
        Phone_Home2,            /**< Home #      invt.selphone = SelPhoneThis, invt.phone = 2nd contact.homephone */
        Phone_Mobile2,          /**< Mobile #    invt.selphone = SelPhoneThis, invt.phone = 2nd contact.mobilephone */
        Phone_Other2            /**< Other #     invt.selphone = SelPhoneThis, invt.phone = 2nd contact.otherphone */
    };

    /** invitee email selections.<br>
      */
    enum EmailSelections
    {
        Email_DontEmail = 0,    /**< Don't Email invt.selemail = SelEmailNone, invt.email = null */
        Email_This,             /**< Email xxxxx invt.selemail = SelEmailThis, invt.email = last chosen address */
        Email_1,                /**< Email 1     invt.selemail = SelEmailThis, invt.email = last chosen address */
        Email_2,                /**< Email 2     invt.selemail = SelEmailThis, invt.email = last chosen address */
        Email_3,                /**< Email 3     invt.selemail = SelEmailThis, invt.email = last chosen address */
        Email_Custom,           /**< User email  invt.selemail = SelEmailThis, invt.email = last chosen addreed */
        Email_EnterEmail,       /**< Command: Ask the user for a new email address. */
        Email_EnterEmailValue,  /**< The user entered email address */
        Email_1_2,              /**< Email 1     invt.selemail = SelEmailThis, invt.email = last chosen address, 2nd contact */
        Email_2_2,              /**< Email 2     invt.selemail = SelEmailThis, invt.email = last chosen address, 2nd contact */
        Email_3_2,              /**< Email 3     invt.selemail = SelEmailThis, invt.email = last chosen address, 2nd contact */
    };

    enum MenuIDS
    {
        Invitee_ChooseFromList = wxID_HIGHEST + 300,
        Invitee_CreateOrEdit,
        Invitee_RemoveFromMeeting,
        Search_Continue,
        Search_Remove,
        Search_Edit
    };

    /**
      * Setup this dialog for Start Meeting mode.
      */
    void SetupForStartMeeting( );

    /**
      * Setup this dialog for a meeting in progress.
      */
    void SetupForMeetingInProgress();

    /**
      * Setup this dialog for changing meeting options for active meeting.
      */
    void SetupForChangeOptions();

    /**
      * Invite user by ID.
      */
    void InviteUserId(wxString userId);

    /**
      * Abort any pending changes on dialog close.
      */
    void ForceNoSave( );

private:
    /**
      * Reset dialog for scheduling a new meeting.
      */
    void ResetForScheduleMeeting( );

    /**
      * Set the text on the OK button.<br>
      * The default label is "Schedule meeting".
      */
    void SetOKButtonLabel( const wxString & newLabel );

    /**
      * Initialize the columns.
      */
    void InitializeColumnHeaders();

    /** Setup array list with names for auto complete.
      */
    void SetupAutoComplete();

    /** Setup file manager for picture download/upload
      */
    void SetupFileManager();
    
public:
    /**
      * Edit the specified meeting.
      * @param meetingId The meeting to edit.
      * @param fCommitOnOK Save the meeting changes if the user clicks OK.
      * @param fDisplInvitees Display the initees panel if true.
      * @param fDispOptions Display the options widgets if true.
      * @param iNumPart The number of participant ID's in the part[] array.
      * @param part An array of participant_t pointers.
      * @return int Returns wxID_OK or wxID_CANCEL
      */
    int EditMeeting( const wxString & meetingId, bool fCommitOnOK = false, bool fDispInvitees = true, bool fDispOptions = true, 
                    bool fUseActive = false, bool fInviteNew = false, int iNumPart = 0, participant_t part[ ] = NULL );

    /**
      * Make a copy of a meeting.<br>
      * @param meetingId The meeting to copy
      */
    int CopyMeeting( const wxString & MeetingId );

    /**
      * Create a new meeting.
      * @return int Returns wxID_OK or wxID_CANCEL
      */
    int NewMeeting();

    /**
      * Save data.<br>
      * This method calls the server to save the users data.<br>
      * A successful return does not indicate that the data was saved, only that the server
      * received the request.
      * @return bool Returns true if the save request was sent to the server.
      */
    bool RequestSaveData();

    /**
      * Determine of confiirmation of the meeting is required.<br>
      * NOTE: A non-zero return indicates that the MeetingConfirmation dialog is required.
      * @return int Returns the MeetingConfirmation::MeetingConfirmationMask bit mask.
      */
    int ConfirmationRequired();

    /** Return the meeting details.<br>
      * Once you make this call, it is assumed that you now own
      * the memory.
      * @return meeting_details_t Returns the meeting details info.
      */
    meeting_details_t GetMeetingDetails();

    /** Get project code entered for meeting (only set if request project code profile is enabled.
      */
    wxString GetProjectCode()
    {
        return m_strProjectCode;
    }

    /** Translate ContactSourceTags into display strings.
      * @param contactTag The source contact Tag value.
      * @return wxString Returns the display text.
      */
    static  wxString contactTagText( int contactTag )
    {
        wxString ret;
        if( contactTag == wxContactCellEditor::ContactFromCAB )
            ret = _("CAB: ");
        else if( contactTag == wxContactCellEditor::ContactFromPAB )
            ret = _("PAB: ");
        else
            ret = wxT("");
        return ret;
    }

    /** Format:<br>
      * "<value> (<type> <tag>: <name>)"<br>
      */
    static  wxString formatComboEntry( pstring value, const wxString & type, const wxString & contactTag, wxString & name );

private:
    /**
      * Return the number of contacts. (The number of grid cells)
      */
    int NumberOfInvitees()
    {
        return m_Invitees->GetNumberRows();
    }

    /**
      * Add new PAB contacts.<br>
      * This is called by EditMeeting and NewMeeting
      */
    int AddNewPABContactRecords();

    /**
      * Append a new row to m_Invitees grid.<br>
      * @return int Returns the index of the new row if successful. Returns -1 on failure.
      */
    int AppendRow( invitee_t invt, contact_t contact = 0, bool fAddAsNewContact = false );

    /**
      * Set the meeting ID associated with this edit session.<br>
      * @param meetingId Ref to the meeting ID that is being edited.
      * @return none.
      */
    void SetMeetingIdToEdit( const wxString & meetingId );

    /**
      * Set the meeting Token associated with this edit session.<br>
      * @param meetingToken Ref to the meeting token to be looked up.
      * @param meetingId Ref to the meeting ID that is being edited.
      * @return none.
      */
    void SetMeetingTokenToEdit( const wxString & meetingToken, const wxString & meetingId );

    /**
      * Connect to various MeetingCenterController events.
      */
    void ConnectToController();

    /**
      * Save processing<br>
      * Validate - This method starts the Save/Confirm/Exit process.<br>
      * Here is the process flow:<br>
      * Successful save:<br>
      * Validate() --> SaveData() --> OnScheduleUpdated() --> MeetingConfirmation --> Exit dialog.<br>
      * Failed save:<br>
      * Validate() --> SaveData() --> OnSessionError() --> Display error.<br>
      */
    virtual bool Validate();

    /**
      * Cancel processing
      */
    void OnCancel( wxCommandEvent& event );

    /**
      * Check to see if the user has changed any data.
      * @return bool Returns true if the meeting data has changed. Returns false for no changes.
      */
    bool DataHasChanged();

    /**
      * This event is received after a schedule_find_meeting_details() call.<br>
      * The meeting_t record received here is stored as the m_mgt_FROM member.<br>
      * A copy of the original meeting details is stored as m_mtg_TO.<br>
      * The Value_to_GUI() method is called to populate the dialog.
      */
    void OnMeetingFetched(wxCommandEvent& event);

    /**
      * This event is received when an update to the meeting schedule is completed
      * without errors.
      */
    void OnScheduleUpdated(wxCommandEvent& event);

    /**
      * This event is received after adding new contact records to the PAB.
      * When received, the State_WaitingForPABLoad flag is cleared.
      * See AddNewPABContactRecords() for additional information.
      */
    void OnDirectoryFetched(wxCommandEvent& event);

    /**
      * This event is received when an update to the meeting schedule fails.
      */
    void OnScheduleError(wxCommandEvent& event);
    void OnSessionError(SessionErrorEvent& event);

    /**
      * This event is received when an update to the address book fails.
      */
    void OnAddressbkUpdateFailed(wxCommandEvent& event);

    /**
      * This event is received when an update to the address book succeeds.
      */
    void OnAddressUpdated(wxCommandEvent& event);

    /**
      * This event is received when presence info becomes available.
      */
    void OnPresenceEvent(PresenceEvent& event);

    /**
      * Update presence for all rows.
      */
    void SetPresence();

    /**
      * Update presence for one row.
      */
    void SetPresence(int row, bool refreshScreen = false);

    /** Move meeting data to the GUI<br>
      * The m_mtg_TO data is used to populate the dialog.<br>
      * This method is called by the OnMeetingFetched() method. */
    void Value_to_GUI();

    /** Lookup a contact record based on an invitee record.
      * @param invt Ptr. to the invitee record.
      * @param fieldname The field name to search for.
      * @param fieldvalue The field value to search for.
      * @param fCAB Output: Ref to a bool flag that indicates that the contact record is from the Community Address Book.
      * @return contact_t Returns the contact record if found. Returns zero if not found.
      */
    contact_t LookupContact( invitee_t invt, bool & fCAB );
    contact_t LookupContact( const char *fieldname, const char *fieldvalue, bool & fCAB );
    contact_t LookupContact( const wxString & fieldname, const wxString & fieldvalue, bool & fCAB );

    /** Sorting interface */
    void OnContactListColumnLeftClick(wxListEvent& event);
    void SortContacts( const wxTreeItemId & item, bool fSortParent = false );
    void SortContacts();

    /** Move invitee data to the GUI
      * @param nRow The grid row.
      * @param invt Ptr. to the invitee_t record.
      * @param contactIn Optional pointer to the associated contact.
      */
    void Invitee_to_GUI( int nRow );

    /** Move GUI data to invitee data
      * @param nRow The grid row.
      * @param invt Ptr. to the invitee_t record.
      */
    void GUI_to_Invitee( int nRow, invitee_t invt );

    /** Update the GridCellComboIntEditor for Phone, Email, and IM cells.
      * @param invt The source invitee record.
      * @param contact The source contact record.
      * @param contactTag The contact source tag (Usually "PAB:" or "CAB:" )
      * @return GridCellComboIntEditor * Returns a pointer to the cell editor.
      */
    GridCellComboIntEditor * UpdatePhoneCellEditor( GridCellComboIntEditor *pE, invitee_t invt, contact_t contact, const wxString & contactTag, contact_t contact2, const wxString & contactTag2 );
    GridCellComboIntEditor * UpdateEmailCellEditor( GridCellComboIntEditor *pE, invitee_t invt, contact_t contact, const wxString & contactTag, contact_t contact2, const wxString & contactTag2 );
    GridCellComboIntEditor * UpdateIMCellEditor( GridCellComboIntEditor *pE, invitee_t invt, contact_t contact, const wxString & contactTag );

    /**
      * Determine if a contact is already invited to the meeting.
      * @param contact Ptr. to the contact.
      * @return bool Returns true if the contact is already invited.
      */
    bool AlreadyInvited( contact_t contact );

    /**
      * Add a new invitee from a contact record.<br>
      * Process:<br>
      * Append a new row to the m_Invitees grid<br>
      * Create a new invitee_t record and set it's initial content from the contact record.
      * @param contact Ptr. to the contact record.
      * @return bool Return true if the new invitee record was appended. Return false on failure.
      */
    bool InviteContact( contact_t contact );

    /**
      * Return a pointer to the wxContactCellEditor for a given row.
      * @param row The target row index
      * @return wxContactCellEditor * Returns a pointer to the ref data.
      */
    wxContactCellEditor * ContactEditorFromRow( int row );

    /**
      * Return the invitee record associated with the specified row.
      * @param row The row number (0..n)
      * @return invitee_t Returns the invitee record.
      */
    invitee_t InviteeFromRow( int row );

    /**
      * Return the contact record associated with the specified row.
      * @param row The row number (0..n)
      * @return contact_t Returns the contact record or zero if not found.
      */
    contact_t ContactFromRow( int row );

    /**
      * Return the contact record for the meeting host.
      */
    contact_t HostContact()
    {
        return m_hostContact;
    }

    /**
      * Allow the user to edit the records associated with a particular row.<br>
      * The only editable rows are newly ad hoc and new contacts that have not been
      * saved in the database.
      * @param row The row number to edit.
      * @return bool Returns true if the user accepted the changes.
      */
    bool EditRow( int row );

    /**
      * Delete the specified row.
      * @param row The row to be removed.
      * @return bool Returns true if the row was removed.
      */
    bool DeleteRow( int row );

private:
    /** Move GUI data to the meeting */
    void GUI_to_Value();

    /** Cleanup the meeting data.<br>
      * The m_mtg member is released.
      */
    void CleanupMeetingResources();

    /** Split up a time_t Date/Time value for display
      * using a wxDatePickerCtrl for the Date portion and
      * a wxTextCtrl for the time portion.
      * @param time The time_t julian date.
      * @param pD Ptr. to the target wxDatePickerCtrl
      * @param pT Ptr. to the target wxTextCtrl
      * @return none.
      */
    void DateTime_to_GUI( time_t time, wxDatePickerCtrl *pD, wxTextCtrl *pT, wxDateTime &theTime, int iTimeOffset = 0 );

    /** Combine the contents of a wxDatePickerCtrl and a wxTextCtrl
      * into a composite time_t julian date.
      * @param pD Ptr. to the target wxDatePickerCtrl
      * @param pT Ptr. to the target wxTextCtrl
      * @return time_t Returns the julian date.
      */
    time_t GUI_to_DateTime( wxDatePickerCtrl *pD, wxTextCtrl *pT );

    /** Set the value of a wxCheckBox from a mask value.
      * @param options The current options settings
      * @param mask The mask bit(s) to test
      * @param pCheckBox Ptr. to the target check box.
      */
    void SetOption( int options, int mask, wxCheckBox *pCheckBox );

    /** Get the value of a wxCheckBox as a mask value.
      * @param options The current options settings
      * @param mask The mask bit(s) to set or clear.
      * @param pCheckBox Ptr. to the source check box.
      * @return int The new options value after altering the mask bits
      */
    int GetOption( int options, int mask, wxCheckBox *pCheckBox );

    /** Convert a UNICODE text value into a pstring.
      */
    pstring Alloc_pstring( const wxString & strValue );
    
    /** Create copy of pstring */
    pstring Alloc_pstring( const char * szValue );

    /**
      * Increment/Decrement the pending contact add counter.
      */
    void IncrPendingContactAdd()
    {
        ++m_nPendingNewContacts;
        m_nState |= State_SavingNewContacts;
        wxLogDebug(wxT("MeetingSetupDialog - Incr Pending new contacts = %d"),m_nPendingNewContacts);
    }
    void DecrPendingContactAdd()
    {
        --m_nPendingNewContacts;
        if( m_nPendingNewContacts < 0 )
            m_nPendingNewContacts = 0;
        if( m_nPendingNewContacts == 0 )
        {
            m_nState &= ~State_SavingNewContacts;
        }
        wxLogDebug(wxT("MeetingSetupDialog - Decr Pending new contacts = %d"),m_nPendingNewContacts);
    }
    void ClearPendingContactAdd()
    {
        m_nPendingNewContacts = 0;
        m_nState &= ~State_SavingNewContacts;
    }

    /** Ensure Start date/time is earlier than End.  
     */
    void  ValidateStartEndOrder( bool fStartChanged );

    /** Move documents uploaded during new meeting setup to meeting directory.
     */
    void MoveTempDocuments(wxString meetingId);

    /** Move images uploaded during new meeting setup to meeting directory.
     */
    void MoveTempImages(wxString meetingId);

#ifdef DEBUG_MEETINGSETUPDIALOG
    void dump_meeting( meeting_details_t mtg );
    void dump_invitee( invitee_t invt );
    void dump_contact( contact_t contact );
#endif

protected:
    void OnSize( wxSizeEvent& event );
    void OnMove( wxMoveEvent& event );

    void ActivateCell(int row, int col);

    //(*Handlers(MeetingSetupDialog)
    void OnInviteNewClick(wxCommandEvent& event);
    void OnShowContactsClick(wxCommandEvent& event);
    void OnRemoveSelectedBtnClick(wxCommandEvent& event);
    void OnInvidateInstantPinCheckClick(wxCommandEvent& event);
    void OnInviteNewContactBtnClick(wxCommandEvent& event);
    void OnPhoneAllClick(wxCommandEvent& event);
    void OnPhoneNoneClick(wxCommandEvent& event);
    void OnEmailAllClick(wxCommandEvent& event);
    void OnEmailNoneClick(wxCommandEvent& event);
    void OnIMAllClick(wxCommandEvent& event);
    void OnIMNoneClick(wxCommandEvent& event);
    void OnSpinBtnStartChange(wxSpinEvent& event);
    void OnSpinBtnStartChangeUp(wxSpinEvent& event);
    void OnSpinBtnStartChangeDown(wxSpinEvent& event);
    void OnSpinBtnEndChange(wxSpinEvent& event);
    void OnSpinBtnEndChangeUp(wxSpinEvent& event);
    void OnSpinBtnEndChangeDown(wxSpinEvent& event);
    void OnSetFocusStartTime(wxFocusEvent& event);
    void OnCharStartTime(wxKeyEvent& event);
    void OnCharEndTime(wxKeyEvent& event);
    void OnSetFocusEndTime(wxFocusEvent& event);
    void OnDateChangedStart(wxDateEvent& event);
    void OnDateChangedEnd(wxDateEvent& event);
    void OnLeftUpStartTime(wxMouseEvent& event);
    void OnLeftUpEndTime(wxMouseEvent& event);
    void OnUploadBtnClick(wxCommandEvent& event);
    void OnUploadImagesClick(wxCommandEvent& event);
    void OnProfileImageBtnClick(wxCommandEvent& event);
    //*)

    void OnClickHelp(wxCommandEvent& event);

    void OnChooseInvitee( wxCommandEvent & event );
    void OnEditInvitee( wxCommandEvent & event );
    void OnRemoveInvitee( wxCommandEvent & event );

    void OnEndFocusStartTime(wxFocusEvent& event);
    void SetStartSelection();
    void SetEndSelection();
    
    void OnGridCellChanged(wxSheetEvent& event);
    
    void OnHideContactsClick(wxCommandEvent& event);

    /**
      * Processing for the "Invite to meeting" button on the SelectContactDialog:<br>
      * This button is enabled when one or more contacts is selected.<br>
      */
    void OnInviteContactsClick(wxCommandEvent& event);

    /**
      * Handle UI feedback changes that result from selecting / deselecting contacts
      * on the SelectContactDialog. The "Invite to meeting" button is enabled if one
      * or more contacts are selected.
      */
    void OnSelectedContactsChanged(wxTreeEvent& event);

    /**
      * Handle pop up menus on the contact list items.
      */
    void OnContactItemRightClick(wxTreeEvent& event);

    /**
      * Processing for the "Search.." button on the SelectContactsDialog.
      */
    void OnSearchContactsClick(wxCommandEvent& event);

    /**
      * Processing for the FindContactsDialog - OK Button.
      */
    void OnSearchContactsOK(wxCommandEvent& event);

    /**
      * Processing for the FindContactsDialog - Cancel Button
      */
    void OnSearchContactsCancel(wxCommandEvent& event);

    /** Search menu processing */
    void OnSearchContinue( wxCommandEvent& event );
    void OnSearchEdit( wxCommandEvent& event );
    void OnSearchRemove( wxCommandEvent& event );

    void OnNoMeetingTimeCheck(wxCommandEvent& event);

    /** Work around for the strange behavior of the wxGrid::GetSelectedRows() method.
      */
    wxArrayInt GetSelectedInvitees() const;

    //(*Declarations(MeetingSetupDialog)
    wxStaticText* StaticText10;
    wxStaticText* m_TimeZone;
    wxRadioButton* m_DisplayNoneRadio;
    wxPanel* m_ParticipantsPanel;
    wxSpinButton* m_SpinBtnEnd;
    wxNotebook* Notebook1;
    wxButton* m_ButtonPhoneNone;
    wxTextCtrl* m_MeetingInviteMessage;
    wxTextCtrl* m_MeetingDescription;
    wxTextCtrl* m_MeetingTitle;
    wxButton* m_ButtonPhoneAll;
    wxCheckBox* m_ModeratorEditCheck;
    wxTextCtrl* m_EndTime;
    wxDatePickerCtrl* m_StartDate;
    wxSpinCtrl* m_InvalidateHoursSpin;
    wxRadioButton* m_VoiceDataRadio;
    wxCheckBox* m_IncludeDocsInRecordingCheck;
    wxRadioButton* m_ChatAllRadio;
    wxCheckBox* m_LectureModeCheck;
    wxButton* m_SekectImagesBtn;
    wxStaticText* StaticText8;
    wxTextCtrl* m_MeetingPasswordText;
    wxButton* m_ProfileImageBtn;
    wxCheckBox* m_NoMeetingTimeCheck;
    wxRadioButton* m_DisplayModeratorsRadio;
    wxRadioButton* m_ChatModeratorsRadio;
    wxCheckBox* m_InvalidateInstantPinCheck;
    wxPanel* m_InviteesPanel;
    wxRadioButton* m_PrivacyPublicRadio;
    wxCheckBox* m_RollCallCheck;
    wxButton* m_UploadBtn;
    wxPanel* m_PictureContainer;
    wxCheckBox* m_MeetingContinueCheck;
    wxRadioButton* m_DisplayAllRadio;
    wxStaticText* m_PINExpireText1;
    wxCheckBox* m_HoldModeCheck;
    wxDatePickerCtrl* m_EndDate;
    wxPanel* m_OptionsPanel;
    wxRadioButton* m_VoiceOnlyRadio;
    wxButton* m_RemoveSelectedBtn;
    wxCheckBox* m_OneToOneCheck;
    wxButton* m_InviteNewBtn;
    wxRadioButton* m_DataShareRadio;
    wxStaticText* m_StaticTextSetPhones;
    wxSpinButton* m_SpinBtnStart;
    wxTextCtrl* m_StartTime;
    wxStaticText* m_PINExpireText2;
    wxCheckBox* m_CropImageCheck;
    wxRadioButton* m_ChatNoneRadio;
    wxCheckBox* m_RecordingCheck;
    wxPanel* m_PicturesPanel;
    wxRadioButton* m_PrivacyUnlistedRadio;
    wxButton* m_ShowContactsBtn;
    wxRadioButton* m_PrivacyPrivateRadio;
    wxPanel* m_InfoPanel;
    //*)
    wxSheet *m_Invitees;
    wxBoxSizer *m_panelSizer;
    ImagePanel *m_Pictures;

private:
    wxString m_meetingId;           /**< The meeting ID being edited. */
    int m_nMode;                    /**< Flag: A meeting is being edited. */
    int  m_nSendEmailOption;        /**< The send email option from MeetingConfirmation */
    meeting_details_t m_mtg_FROM;   /**< Ptr. to the original meeting data */
    meeting_details_t m_mtg_TO;     /**< Ptr. to a copy of the original meeting data */
    ArrInviteesToDelete m_arrInviteesToDelete;  /**< Array of invitees to be deleted.  Deleting when the user indicates invalidates the invitee pointers stored in the cell editors. */

    /////////////////////////////////////////////////////////////////////////////////////////////////
    /** Contact dialog related data */
    SelectContactDialog *m_pContactDlg; /**< Ptr. to the modeless contacts dialog */
    ContactTreeList m_ContactTree;      /**< The contact tree list handler */
    wxButton *m_pContactInviteBtn;      /**< Ptr. to the "Invite to meeting" button on the SelectContact screen */
    wxTreeItemId m_CommunityNode;       /**< The Top level community contacts node */
    wxTreeItemId m_PersonalNode;        /**< The Top level personal node */
    wxTreeItemId m_SearchNode;          /**< The Top level search node */

    /////////////////////////////////////////////////////////////////////////////////////////////////
    FindContactsDialog *m_pFindContactsDlg;

    int m_StatusOnline;                 /**< Status image processing */
    int m_StatusPhone;
    int m_StatusContact;
    CImageListComposite m_StatusImages;

    wxMenu *m_InviteePopup;            /**< The invitee pop up menu */

    bool m_fCommitOnOK;                /**< Flag: Commit changes to the database on Validate() */
    bool m_fDataSaved;                 /**< Database transaction successful */
    bool m_fStartMeetingMode;          /**< Flag: The dialog is setup for starting a meeting */
    bool m_fMeetingNotActive;          /**< Flag: Meeting is not running yet (i.e. we are starting it) */
    bool m_fUploadedDocs;              /**< Flag: Documents have been uploaded during setup */

    FileManager *m_FileManager;

    wxString m_strDatabaseError;       /**< The last database error */

    wxString m_strScreenNameToAdd;     /**< ScreenName of contact to add to invitee list */
    wxString m_strUserIdToAdd;         /**< ScreenName of contact to add to invitee list */

    wxString m_strProjectCode;         /**< Optional project code entered for meeting */

    wxPoint m_lastLeftClickPos;        /**< The last left click position */

    wxString m_strCaption;             /**< The caption used for all message boxes */

    int m_nPendingNewContacts;         /**< The number of new contact records to be created */

    contact_t m_hostContact;           /**< The meeting host's contact record */

    wxString m_strHostProfileId;       /**< The meeting host's profile Id */

    int m_nState;                      /**< The current MeetingSetupDialog state */

    wxMenu * m_SearchPopup;            /**< The search results pop up menu */

    int         m_iStartTimeSelection;
    int         m_iEndTimeSelection;
    wxDateTime  m_dtStartTime;
    wxDateTime  m_dtEndTime;

    wxArrayString m_autoNames;         /**< Names for auto complete */

    CIICDoc * m_pDoc;                  /**< "Document" object used when uploading */
    
    DECLARE_EVENT_TABLE()
};

#endif
