/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "app.h"
#include "meetingcentercontroller.h"

#include "profilemanagerdialog.h"

//(*InternalHeaders(ProfileManagerDialog)
#include <wx/xrc/xmlres.h>
//*)

CProfile::CProfile():
enabled_features(0),
max_voice_size(0),
max_data_size(0),
max_presence_monitors(0),
max_scheduled_meetings(0),
pin_length(0),
meeting_id_length(0),
password_length(0),
max_idle_minutes(0),
max_connect_minutes(0)
{
}

CProfile & CProfile::operator=(profile_t rhs)
{
    Set( rhs );
    return *this;
}

CProfile & CProfile::operator=(const CProfile & rhs)
{
    name = rhs.name;
    profile_id = rhs.profile_id;
    enabled_features = rhs.enabled_features;
    max_voice_size = rhs.max_voice_size;
    max_data_size = rhs.max_data_size;
    max_presence_monitors = rhs.max_presence_monitors;
    max_scheduled_meetings = rhs.max_scheduled_meetings;
    pin_length = rhs.pin_length;
    meeting_id_length = rhs.meeting_id_length;
    password_length = rhs.password_length;
    max_idle_minutes = rhs.max_idle_minutes;
    max_connect_minutes = rhs.max_connect_minutes;
    return *this;
}

void CProfile::Set( profile_t profile )
{
    profile_id = wxString( profile->profile_id, wxConvUTF8 );
    name = wxString( profile->name, wxConvUTF8 );

    enabled_features = profile->enabled_features;
    max_voice_size = profile->max_voice_size;
    max_data_size = profile->max_data_size;
    max_presence_monitors = profile->max_presence_monitors;
    max_scheduled_meetings = profile->max_scheduled_meetings;
    pin_length = profile->pin_length;
    meeting_id_length = profile->meeting_id_length;
    password_length = profile->password_length;
    max_idle_minutes = profile->max_idle_minutes;
    max_connect_minutes = profile->max_connect_minutes;
}

void CProfile::Get( profile_t profile )
{
    profile->enabled_features = enabled_features;
    profile->max_voice_size = max_voice_size;
    profile->max_data_size = max_data_size;
    profile->max_presence_monitors = max_presence_monitors;
    profile->max_scheduled_meetings = max_scheduled_meetings;
    profile->pin_length = pin_length;
    profile->meeting_id_length = meeting_id_length;
    profile->password_length = password_length;
    profile->max_idle_minutes = max_idle_minutes;
    profile->max_connect_minutes = max_connect_minutes;
}

bool CProfile::IsIdentical( profile_t profile )
{
    return *this == profile;

    return true;
}

bool CProfile::operator == (profile_t rhs)
{
    CProfile Crhs;
    Crhs.Set( rhs );
    return *this == Crhs;
}

bool CProfile::operator == (const CProfile & rhs)
{
    if( name != rhs.name ||
        profile_id != rhs.profile_id ||
        enabled_features != rhs.enabled_features ||
        max_voice_size != rhs.max_voice_size ||
        max_data_size != rhs.max_data_size ||
        max_presence_monitors != rhs.max_presence_monitors ||
        max_scheduled_meetings != rhs.max_scheduled_meetings ||
        pin_length != rhs.pin_length ||
        meeting_id_length != rhs.meeting_id_length ||
        password_length != rhs.password_length ||
        max_idle_minutes != rhs.max_idle_minutes ||
        max_connect_minutes != rhs.max_connect_minutes  )
        return false;
    return true;
}

bool CProfile::operator!=(profile_t rhs)
{
    return !(*this == rhs);
}

bool CProfile::operator!=(const CProfile & rhs)
{
    return !(*this == rhs);
}


//(*IdInit(ProfileManagerDialog)
//*)

BEGIN_EVENT_TABLE(ProfileManagerDialog,wxDialog)
	//(*EventTable(ProfileManagerDialog)
	//*)

EVT_BUTTON( wxID_HELP, ProfileManagerDialog::OnClickHelp)

END_EVENT_TABLE()

ProfileManagerDialog::ProfileManagerDialog(wxWindow* parent,wxWindowID id):
m_nActiveProfile(-1),
m_profile(0),
m_nState(0),
m_nDatabaseError(0)
{

	//(*Initialize(ProfileManagerDialog)
	wxXmlResource::Get()->LoadObject(this,parent,_T("ProfileManagerDialog"),_T("wxDialog"));
	StaticText1 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT1"));
	m_PolicyCombo = (wxComboBox*)FindWindow(XRCID("ID_POLICY_COMBO"));
	m_NewPolicyButton = (wxButton*)FindWindow(XRCID("ID_BUTTON1"));
	m_CopyPolicyButton = (wxButton*)FindWindow(XRCID("ID_COPYPOLICY_BUTTON"));
	m_DeletePolicyButton = (wxButton*)FindWindow(XRCID("ID_DELETEPOLICY_BUTTON"));
	StaticText2 = (wxStaticText*)FindWindow(XRCID("ID_STATICTEXT2"));
	m_PropGridPanel = (wxPanel*)FindWindow(XRCID("ID_PROPGRID_PANEL"));
	
	Connect(XRCID("ID_POLICY_COMBO"),wxEVT_COMMAND_COMBOBOX_SELECTED,(wxObjectEventFunction)&ProfileManagerDialog::OnPolicyComboSelect);
	Connect(XRCID("ID_BUTTON1"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&ProfileManagerDialog::OnNewPolicyButtonClick);
	Connect(XRCID("ID_COPYPOLICY_BUTTON"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&ProfileManagerDialog::OnCopyPolicyButtonClick);
	Connect(XRCID("ID_DELETEPOLICY_BUTTON"),wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&ProfileManagerDialog::OnDeletePolicyButtonClick);
	//*)

    Connect( XRCID("ID_PROPERTY_GRID"), wxEVT_PG_CHANGED, (wxObjectEventFunction)&ProfileManagerDialog::OnPropertyGridChanged );
    Connect( wxID_OK,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&ProfileManagerDialog::OnOkClicked);
    Connect( wxID_CANCEL,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&ProfileManagerDialog::OnCancelClicked);
    Connect( wxID_APPLY,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&ProfileManagerDialog::OnApplylicked);

    m_strCaption = _("Policy manager");
    SetTitle( m_strCaption );

    int targetGridHeight = 0;
    int targetGridWidth = 0;
    int targetGridRows = 0;
    int proportion = 8;
    int border = 5;
    int flag = wxEXPAND | wxRIGHT | wxLEFT | wxTOP | wxBOTTOM;

    m_Grid = new wxPropertyGrid(
            m_PropGridPanel,     // parent
            XRCID("ID_PROPERTY_GRID"), // id
            wxDefaultPosition,  // position
            wxDefaultSize,      // size
            // Some specific window styles - for all additional styles,
            // see Modules->PropertyGrid Window Styles
            //wxPG_AUTO_SORT | // Automatic sorting after items added
            wxPG_SPLITTER_AUTO_CENTER | // Automatically center splitter until user manually adjusts it
            // Default style
            wxPG_DEFAULT_STYLE );

    wxBoxSizer *panelSizer = new wxBoxSizer(wxVERTICAL);

    panelSizer->Add( m_Grid, proportion, flag, border);

    m_PropGridPanel->SetSizer( panelSizer );

    wxArrayString arrayEnabledLabel;
    wxArrayInt    arrayEnabledValue;

    arrayEnabledLabel.Add(_("Disabled"));  arrayEnabledValue.Add(0);
    arrayEnabledLabel.Add(_("Enabled"));   arrayEnabledValue.Add(1);

    //m_id_xxxxx = m_Grid->Append( new wxEnumProperty(_("IM Service:"),wxT("service"),arrIMService, arrNotify ) );

    m_id_savepassword = m_Grid->Append( new wxEnumProperty(_("Save password"), wxT("savepassword"), arrayEnabledLabel,arrayEnabledValue ) );
    m_id_zonim = m_Grid->Append( new wxEnumProperty(_("Zon IM"), wxT("zonim"), arrayEnabledLabel,arrayEnabledValue ) );
    m_id_dldpublicmtg = m_Grid->Append( new wxEnumProperty(_("Download public meetings"), wxT("dldpublicmtg"), arrayEnabledLabel,arrayEnabledValue ) );
    m_id_dldcab = m_Grid->Append( new wxEnumProperty(_("Download community address book"), wxT("dldcab"), arrayEnabledLabel,arrayEnabledValue ) );
    m_id_sharedesktop = m_Grid->Append( new wxEnumProperty(_("Share desktop"), wxT("sharedesktop"), arrayEnabledLabel,arrayEnabledValue ) );
    m_id_shareapp = m_Grid->Append( new wxEnumProperty(_("Share application"), wxT("shareapp"), arrayEnabledLabel,arrayEnabledValue ) );
    m_id_shareppt = m_Grid->Append( new wxEnumProperty(_("Share Power Point"), wxT("shareppt"), arrayEnabledLabel,arrayEnabledValue ) );
    m_id_sharedoc = m_Grid->Append( new wxEnumProperty(_("Share document"), wxT("sharedoc"), arrayEnabledLabel,arrayEnabledValue ) );
    m_id_sharewhiteboard = m_Grid->Append( new wxEnumProperty(_("Share whiteboard"), wxT("sharewhiteboard"), arrayEnabledLabel,arrayEnabledValue ) );
    m_id_remotecontrol = m_Grid->Append( new wxEnumProperty(_("Remote control"), wxT("remotecontrol"), arrayEnabledLabel,arrayEnabledValue ) );
    m_id_projectcode = m_Grid->Append( new wxEnumProperty( _("Request project code"), wxT("projectcode"), arrayEnabledLabel,arrayEnabledValue ) );
    m_id_npresencemon = m_Grid->Append(new wxIntProperty( _("Number of presence monitors"), wxT("npresencemon"), 0 ) );
    m_id_invtpinlen = m_Grid->Append( new wxIntProperty( _("Invitee PIN length"), wxT("invtpinlen"), 0 ) );
    m_id_usermtgidlen = m_Grid->Append( new wxIntProperty( _("User meeting ID length"), wxT("usermtgidlen"), 0 ) );
    m_id_maxschedmetg = m_Grid->Append( new wxIntProperty( _("Maximum scheduled meetings"), wxT("maxschedmetg"), 0 ) );
    m_id_locktimout = m_Grid->Append( new wxIntProperty( _("Lock timeout (minutes)"), wxT("locktimout"), 0 ) );
    m_id_sesstimeout = m_Grid->Append( new wxIntProperty( _("Session timeout"), wxT("sesstimeout"), 0 ) );

    targetGridRows = m_Grid->GetChildrenCount();
    targetGridHeight = (targetGridRows * m_Grid->GetRowHeight()) + 50;
    targetGridWidth = 480;
    wxSize targetSize( targetGridWidth, targetGridHeight );

    m_PropGridPanel->SetSize( targetSize );

    this->SetSize( targetSize );

    // Can't figure out any other way to get the initial grid size to the size we want it, so
    // set minimum grid size, then set dialog size, then reset minmum grid size so user can resize.
    m_PropGridPanel->SetMinSize(targetSize);
    wxSize minSize = GetSizer()->GetMinSize( );
    SetClientSize( minSize );
    m_PropGridPanel->SetMinSize(wxDefaultSize);

}

ProfileManagerDialog::~ProfileManagerDialog()
{
	//(*Destroy(ProfileManagerDialog)
	//*)
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_SESSION_ERROR,           SessionErrorEventHandler(ProfileManagerDialog::OnSessionError),       0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_ADDRESS_UPDATED,         wxCommandEventHandler(ProfileManagerDialog::OnAddressUpdated),        0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_PROFILE_FETCHED,         wxCommandEventHandler(ProfileManagerDialog::OnProfileFetched),        0, this );
    CONTROLLER.Disconnect(wxID_ANY, wxEVT_ADDRESS_DELETED,         wxCommandEventHandler(ProfileManagerDialog::OnAddressDeleted),        0, this );
}

int ProfileManagerDialog::ManageProfiles()
{
    CONTROLLER.Connect(wxID_ANY, wxEVT_SESSION_ERROR,           SessionErrorEventHandler(ProfileManagerDialog::OnSessionError),       0, this );
    CONTROLLER.Connect(wxID_ANY, wxEVT_ADDRESS_UPDATED,         wxCommandEventHandler(ProfileManagerDialog::OnAddressUpdated),        0, this );
    CONTROLLER.Connect(wxID_ANY, wxEVT_PROFILE_FETCHED,         wxCommandEventHandler(ProfileManagerDialog::OnProfileFetched),        0, this );
    CONTROLLER.Connect(wxID_ANY, wxEVT_ADDRESS_DELETED,         wxCommandEventHandler(ProfileManagerDialog::OnAddressDeleted),        0, this );

    int err = FetchProfiles();
    if( err )
    {
        ShowDatabaseError();
        return wxID_CANCEL;
    }

    int nResp = ShowModal();

    return nResp;
}

int ProfileManagerDialog::FetchProfiles()
{
    ClearDatabaseError();
    SetState( State_WaitingForUpdate );

    wxLogDebug(wxT("ProfileManagerDialog::FetchProfiles") );

    wxString strOpid;
    strOpid = MeetingCenterController::opcode( MeetingCenterController::OP_ManageProfiles, -1 );
    wxCharBuffer opid = strOpid.mb_str();

    int err = addressbk_fetch_profiles(CONTROLLER.AddressBook(),opid);

    wxLogDebug(wxT("ProfileManagerDialog::FetchProfiles addressbk_fetch_profiles returned: %d"),err);
    if( err != 0 )
    {
        ClearState(State_WaitingForUpdate);
        SetDatabaseError( err, wxT(""), true );
        return err;
    }

    // Wait for it.........
    err = WaitForClear( State_WaitingForUpdate, 20, _("Timeout while fetching profiles") );
    if( !err )
    {
        if( m_nDatabaseError )
        {
            err = m_nDatabaseError;
        }
    }
    return err;
}

int ProfileManagerDialog::SaveActiveProfile()
{
    int err = SaveProfile( m_profile );

    return err;
}

int ProfileManagerDialog::SaveProfile( profile_t profile )
{
    ClearDatabaseError();
    SetState( State_WaitingForUpdate );

    wxLogDebug(wxT("ProfileManagerDialog::SaveProfile") );

    wxString strOpid;
    strOpid = MeetingCenterController::opcode( MeetingCenterController::OP_ManageProfiles, -1 );
    wxCharBuffer opid = strOpid.mb_str();

    int err = addressbk_profile_update(CONTROLLER.AddressBook(), opid, profile);

    wxLogDebug(wxT("ProfileManagerDialog::SaveProfile addressbk_profile_update returned: %d"),err);
    if( err != 0 )
    {
        ClearState(State_WaitingForUpdate);
        SetDatabaseError( err, wxT(""), true );
        return err;
    }

    // Wait for it.........
    err = WaitForClear( State_WaitingForUpdate, 20, _("Timeout while saving a profile") );
    if( !err )
    {
        if( m_nDatabaseError )
        {
            err = m_nDatabaseError;
        }
    }
    return err;
}

int ProfileManagerDialog::DeleteProfile( profile_t profile )
{
    ClearDatabaseError();
    SetState( State_WaitingForUpdate );

    wxLogDebug(wxT("ProfileManagerDialog::DeleteProfile") );

    wxString strOpid;
    strOpid = MeetingCenterController::opcode( MeetingCenterController::OP_ManageProfiles, -1 );
    wxCharBuffer opid = strOpid.mb_str();

    int err = addressbk_profile_delete(CONTROLLER.AddressBook(), opid, profile);

    wxLogDebug(wxT("ProfileManagerDialog::DeleteProfile addressbk_profile_delete returned: %d"),err);
    if( err != 0 )
    {
        ClearState(State_WaitingForUpdate);
        SetDatabaseError( err, wxT(""), true );
        return err;
    }

    // Wait for it.........
    err = WaitForClear( State_WaitingForUpdate, 20, _("Timeout while removing a profile") );
    if( !err )
    {
        if( m_nDatabaseError )
        {
            err = m_nDatabaseError;
        }
    }
    return err;
}

void ProfileManagerDialog::OnPolicyComboSelect(wxCommandEvent& event)
{
    wxLogDebug(wxT("ProfileManagerDialog::OnPolicyComboSelect"));

    int nNewActiveProfile = m_PolicyCombo->GetSelection();

    QuerySaveChanges();

    SelectProfile( nNewActiveProfile );
}

bool ProfileManagerDialog::QuerySaveChanges()
{
    bool fSaved = false;
    bool fSaveChanges = false;

    if( m_nActiveProfile != -1 )
    {
        GUI_to_Value();
        // See if the user has changed any data in the currently displayed profile record
        if( m_active_profile != m_original_profile )
        {
            wxString strMsg;
            strMsg = wxString::Format(_("You have made changes to policy: \"%s\"\nDo you wish to save these changes ?"), m_active_profile.name.c_str() );
            wxMessageDialog     dlg( this, strMsg, m_strCaption, wxYES_NO | wxICON_QUESTION );
            goModeless();
            if( dlg.ShowModal( )  ==  wxID_YES )
            {
                fSaveChanges = true;
            }else
            {
                m_active_profile = m_original_profile;
                Value_to_GUI();
            }
            goModal();
        }
        m_active_profile.Get( m_profile );
    }

    if( fSaveChanges )
    {
        int err = SaveActiveProfile();
        fSaved = (err == 0);
    }

    return fSaved;
}

wxString ProfileManagerDialog::AskForUniqueName( const wxString & initialDefault )
{
    wxString newProfileName(wxT(""));
    wxString defaultValue( initialDefault );
    wxString prompt;
    bool fHaveUniqueName = false;
    bool fCancelNewPolicy = false;
    prompt = _("New policy name:");

    while( !fCancelNewPolicy && !fHaveUniqueName )
    {
        wxTextEntryDialog Dlg(this, prompt, m_strCaption, defaultValue );

        goModeless();
        int nResp = Dlg.ShowModal();
        goModal();
        if( nResp == wxID_OK )
        {
            // Make sure the name is unique
            newProfileName = Dlg.GetValue();
            fHaveUniqueName = !IsDuplicateProfileName( newProfileName );
            if( !fHaveUniqueName )
            {
                wxString strMsg;
                strMsg = wxString::Format(_("The policy name \"%s\" is aready in use.\nPlease choose an alternate name."), newProfileName.c_str() );
                ::wxMessageBox( strMsg, m_strCaption, wxOK, this );
                defaultValue = newProfileName;
            }
        }else
        {
            fCancelNewPolicy = true;
        }
    }

    if( fCancelNewPolicy )
    {
        newProfileName = wxT("");
    }

    return newProfileName;
}

void ProfileManagerDialog::OnNewPolicyButtonClick(wxCommandEvent& event)
{
    wxString defaultValue(wxT(""));
    wxString newProfileName(wxT(""));
    int err = 0;

    QuerySaveChanges();

    newProfileName = AskForUniqueName( defaultValue );
    if( !newProfileName.IsEmpty() )
    {
        wxLogDebug(wxT("ProfileManagerDialog::OnNewPolicyButtonClick - Creating profile \"%s\""),newProfileName.c_str() );
        err = CreateNewProfile( newProfileName );
        if( err != 0 )
        {
            return;
        }
        err = FetchProfiles();
        if( err != 0 )
        {
            return;
        }
        SelectProfile( newProfileName );
    }
}

void ProfileManagerDialog::OnCopyPolicyButtonClick(wxCommandEvent& event)
{
    wxString defaultValue(wxT(""));
    wxString newProfileName(wxT(""));
    int err = 0;

    QuerySaveChanges();

    defaultValue = wxString( m_profile->name, wxConvUTF8 );
    defaultValue += _(" Copy");

    newProfileName = AskForUniqueName( defaultValue );
    if( !newProfileName.IsEmpty() )
    {
        wxLogDebug(wxT("ProfileManagerDialog::OnCopyPolicyButtonClick - Creating profile \"%s\""),newProfileName.c_str() );
        err = CreateNewProfile( newProfileName, m_profile );
        if( err != 0 )
        {
            return;
        }
        err = FetchProfiles();
        if( err != 0 )
        {
            return;
        }
        SelectProfile( newProfileName );
    }
}

int ProfileManagerDialog::CreateNewProfile( wxString & newName, profile_t fromProfile )
{
    int err = 0;

    profile_t newProfile = profiles_new(CONTROLLER.AddressBook());

    if( fromProfile )
    {
        CProfile temp;
        temp = m_profile;
        temp.Get( newProfile );
    }

    newProfile->name = profiles_string(CONTROLLER.AddressBook(), newName.mb_str(wxConvUTF8) );

    err = SaveProfile( newProfile );

    return err;
}

bool ProfileManagerDialog::IsDuplicateProfileName( const wxString & name )
{
    profile_iterator_t iter = profiles_iterate( CONTROLLER.book );
    profile_t profile = 0;
    while( (profile = profiles_next(CONTROLLER.book, iter)) != 0 )
    {
       wxString checkName( profile->name, wxConvUTF8 );
        if( name.CmpNoCase( checkName ) == 0 )
        {
            return true;
            break;
        }
    }
    return false;
}


void ProfileManagerDialog::OnDeletePolicyButtonClick(wxCommandEvent& event)
{
    bool fDeleteProfile = false;
    wxString strMsg;
    strMsg = wxString::Format(_("Do you wish to remove policy: \"%s\" ?"), m_active_profile.name.c_str() );
    wxMessageDialog     dlg( this, strMsg, m_strCaption, wxYES_NO | wxICON_QUESTION );
    goModeless();
    if( dlg.ShowModal( )  ==  wxID_YES )
    {
        fDeleteProfile = true;
    }
    goModal();
    if( !fDeleteProfile )
    {
        return;
    }

    int ndx = m_PolicyCombo->GetSelection();
    if( ndx == (int)(m_PolicyCombo->GetCount() - 1) )
    {
        --ndx;
    }

    int err = DeleteProfile( m_profile );
    if( err != 0 )
    {
        return;
    }
    err = FetchProfiles();
    if( err != 0 )
    {
        return;
    }
    SelectProfile( ndx );
}

void ProfileManagerDialog::OnPropertyGridChanged( wxPropertyGridEvent& event )
{
    wxLogDebug( wxT("ProfileManagerDialog::OnPropertyGridChanged") );
}

void ProfileManagerDialog::Value_to_GUI()
{
    // Populate the grid
    wxPropertyGrid *pg = m_Grid;

    pg->SetPropertyValue( m_id_savepassword, IsSet( ProfileUserSavePassword ) );
    pg->SetPropertyValue( m_id_zonim, IsSet( ProfileUserIICIM ) );
    pg->SetPropertyValue( m_id_dldpublicmtg, IsClr(ProfileDisableMeetingDownload) );
    pg->SetPropertyValue( m_id_dldcab, IsClr(ProfileDisableCommunityDownload) );
    pg->SetPropertyValue( m_id_sharedesktop, IsClr(ProfileDisableShareDesktop) );
    pg->SetPropertyValue( m_id_shareapp, IsClr(ProfileDisableShareApplication) );
    pg->SetPropertyValue( m_id_shareppt, IsClr(ProfileDisableSharePowerpoint) );
    pg->SetPropertyValue( m_id_sharedoc, IsClr(ProfileDisableShareDocument) );
    pg->SetPropertyValue( m_id_sharewhiteboard, IsClr(ProfileDisableShareWhiteboard) );
    pg->SetPropertyValue( m_id_remotecontrol, IsClr(ProfileDisableShareRemoteControl) );
    pg->SetPropertyValue( m_id_projectcode, IsSet(ProfileRequestProjectCode) );
    if( !m_profile )
        return;

    pg->SetPropertyValue( m_id_npresencemon, m_active_profile.max_presence_monitors );
    pg->SetPropertyValue( m_id_invtpinlen, m_active_profile.pin_length);
    pg->SetPropertyValue( m_id_usermtgidlen, m_active_profile.meeting_id_length);
    pg->SetPropertyValue( m_id_maxschedmetg, m_active_profile.max_scheduled_meetings);
    pg->SetPropertyValue( m_id_locktimout, m_active_profile.max_idle_minutes);
    pg->SetPropertyValue( m_id_sesstimeout, m_active_profile.max_connect_minutes);
}

void ProfileManagerDialog::GUI_to_Value()
{
    // Copy the grid content back into the record.
    wxPropertyGrid *pg = m_Grid;

    SetFlag( ProfileUserSavePassword, m_id_savepassword );
    SetFlag( ProfileUserIICIM, m_id_zonim );
    ClrFlag( ProfileDisableMeetingDownload, m_id_dldpublicmtg );
    ClrFlag( ProfileDisableCommunityDownload, m_id_dldcab );
    ClrFlag( ProfileDisableShareDesktop, m_id_sharedesktop );
    ClrFlag( ProfileDisableShareApplication, m_id_shareapp );
    ClrFlag( ProfileDisableSharePowerpoint, m_id_shareppt );
    ClrFlag( ProfileDisableSharePowerpoint, m_id_sharedoc );
    ClrFlag( ProfileDisableShareWhiteboard, m_id_sharewhiteboard );
    ClrFlag( ProfileDisableShareRemoteControl, m_id_remotecontrol );
    SetFlag( ProfileRequestProjectCode, m_id_projectcode );
    if( !m_profile )
        return;

    m_active_profile.max_presence_monitors = pg->GetPropertyValueAsInt( m_id_npresencemon );
    m_active_profile.pin_length = pg->GetPropertyValueAsInt( m_id_invtpinlen );
    m_active_profile.meeting_id_length = pg->GetPropertyValueAsInt( m_id_usermtgidlen );
    m_active_profile.max_scheduled_meetings = pg->GetPropertyValueAsInt( m_id_maxschedmetg );
    m_active_profile.max_idle_minutes = pg->GetPropertyValueAsInt( m_id_locktimout );
    m_active_profile.max_connect_minutes = pg->GetPropertyValueAsInt( m_id_sesstimeout );
}

void ProfileManagerDialog::OnProfileFetched(wxCommandEvent& event)
{
    wxString opid;
    opid = event.GetString();

    wxLogDebug(wxT("ProfileManagerDialog::OnProfileFetched opid=%s"),opid.c_str() );

    if( !CONTROLLER.IsOpcode( opid, MeetingCenterController::OP_ManageProfiles, false /* Not exact match */ ) )
        event.Skip();

    // Populate the policy name combo box
    m_PolicyCombo->Clear();

    profile_iterator_t iter = profiles_iterate( CONTROLLER.book );
    profile_t profile = 0;
    while( (profile = profiles_next(CONTROLLER.book, iter)) != 0 )
    {
        wxString name( profile->name, wxConvUTF8 );
        m_PolicyCombo->Append( name );
    }

    SelectProfile( 0 );

    ClearState( State_WaitingForUpdate );
}

void ProfileManagerDialog::OnSessionError(SessionErrorEvent& event)
{
    //int error_code = event.GetErrorCode();
    wxString error_msg;
    wxString rpc_id;
    wxString opid;
    error_msg = event.GetMessage();
    rpc_id = event.GetRpcId();
    opid = event.GetOpid();

    wxLogDebug(wxT("ProfileManagerDialog::OnSessionError opid=%s"),opid.c_str() );

    ClearState( State_WaitingForUpdate );

    event.Skip();
}

void ProfileManagerDialog::OnAddressUpdated(wxCommandEvent& event)
{
    wxString opid;
    opid = event.GetString();

    wxLogDebug(wxT("ProfileManagerDialog::OnAddressUpdated opid=%s"),opid.c_str() );

    if( !CONTROLLER.IsOpcode( opid, MeetingCenterController::OP_ManageProfiles, false /* Not exact match */ ) )
        event.Skip();
   ClearState( State_WaitingForUpdate );
}

void ProfileManagerDialog::OnAddressDeleted(wxCommandEvent& event)
{
    wxString opid;
    opid = event.GetString();

    wxLogDebug(wxT("ProfileManagerDialog::OnAddressDeleted opid=%s"),opid.c_str() );

    if( !CONTROLLER.IsOpcode( opid, MeetingCenterController::OP_ManageProfiles, false /* Not exact match */ ) )
        event.Skip();

    ClearState( State_WaitingForUpdate );
}

void ProfileManagerDialog::ShowDatabaseError()
{
    wxString strMsg;
    if( m_nDatabaseError )
    {
        if( m_strDatabaseError.IsEmpty() )
        {
            m_strDatabaseError = _("Unknown error");
        }
        strMsg = wxString::Format(_("Unable to update profile information:\n%s"),m_strDatabaseError.c_str());
        ::wxMessageBox( strMsg, m_strCaption, wxOK, this );
    }
}

int ProfileManagerDialog::WaitForClear( int flags, int nSeconds, const wxString & errMsg )
{
    // If there are pending contact records that we being processed
    wxStopWatch sw;
    long start_time = sw.Time();
    long timeout_ms = (nSeconds * 1000);
    while( IsState( flags ) )
    {
        CUtils::MsgWait( 500, 1 );
        wxGetApp().Yield();

        if( (sw.Time() - start_time) > timeout_ms )
        {
            ClearState( flags );
            if( !errMsg.IsEmpty() )
            {
                SetDatabaseError( -1, errMsg, true );
            }
            return -1;
            break;
        }
    }

    // All is well
    return 0;
}

void ProfileManagerDialog::goModeless()
{
    /* Gtk disables all other windows when one is modal, including modeless dialogs created by that
       window.  For now, hack around this by setting ourselves non-modal while this dialog is up. */
    CUtils::SetModal(this, false);
}

void ProfileManagerDialog::goModal()
{
    // Back to modal
    CUtils::SetModal(this, true);
}

bool ProfileManagerDialog::SelectProfile( int ndx )
{
    bool fFound = false;
    if( ndx < 0 || ndx >= (int)m_PolicyCombo->GetCount() )
        return false;

    int i = 0;
    profile_iterator_t iter = profiles_iterate( CONTROLLER.book );
    profile_t profile = 0;
    while( (profile = profiles_next(CONTROLLER.book, iter)) != 0 )
    {
        if( i == ndx )
        {
            m_profile = profile;
            fFound = true;
            break;
        }
        ++i;
    }
    if( fFound )
    {
        m_PolicyCombo->SetSelection( ndx );
        m_nActiveProfile = ndx;
        m_active_profile.Set( m_profile );
        m_original_profile.Set( m_profile );
        Value_to_GUI();
    }
    return fFound;
}

bool ProfileManagerDialog::SelectProfile( const wxString &name )
{
    bool fFound = false;
    int ndx = -1;

    int i = 0;
    profile_iterator_t iter = profiles_iterate( CONTROLLER.book );
    profile_t profile = 0;
    while( (profile = profiles_next(CONTROLLER.book, iter)) != 0 )
    {
        wxString name_check( profile->name, wxConvUTF8 );
        if( name.CmpNoCase( name_check ) == 0 )
        {
            ndx = i;
            m_profile = profile;
            fFound = true;
            break;
        }
        ++i;
    }

    if( fFound )
    {
        m_PolicyCombo->SetSelection( ndx );
        m_nActiveProfile = ndx;
        m_active_profile.Set( m_profile );
        m_original_profile.Set( m_profile );
        Value_to_GUI();
    }
    return fFound;
}

void ProfileManagerDialog::OnOkClicked(wxCommandEvent& event)
{
    QuerySaveChanges();
    event.Skip();
}

void ProfileManagerDialog::OnCancelClicked(wxCommandEvent& event )
{
    QuerySaveChanges();
    event.Skip();
}

void ProfileManagerDialog::OnApplylicked(wxCommandEvent& event )
{
    SaveActiveProfile();
    event.Skip();
}

void ProfileManagerDialog::OnClickHelp(wxCommandEvent& event)
{
    wxString help( wxGetApp().Options().GetSystem( "HelpPath", "" ),  wxConvUTF8);
    help += LOCALE->GetCanonicalName( );
    help += wxT("/");
    help += wxT("administration.htm");
    wxLaunchDefaultBrowser( help );
}
