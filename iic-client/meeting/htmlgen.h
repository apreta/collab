/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef _HTMLGEN_H_
#define _HTMLGEN_H_

#include <wx/string.h>
#include <wx/colour.h>

/**
  * This class aids in the creation of rich text
  * reports.
  */
class HtmlGen
{
public:
	/** Constructor */
	HtmlGen();

    /** Bullet types */
    enum BulletTypes
    {
    Bullet_disc,
    Bullet_circle,
    Bullet_square
    };

	/** Clear the buffer and insert the standard DOCTYPE
	  * text.
	  */
	void clear( bool fWithHeader = true );

	/** Set the doc name */
	void setDocName( const wxString & name );

	/** Start html block.<br>
	  * Insert <html>
	  */
	void startHtml()
	{
		m_strBuf += wxT("<html>");
	}

	/** End htm block.<br>
	  * Insert </html>
	  */
	void endHtml()
	{
		m_strBuf += wxT("</html>\n");
	}

	/** Start paragraph.<br>
	  * Insert <p>
	  */
	void startParagraph()
	{
		m_strBuf += wxT("<p>");
	}

	/**
	  * End paragraph.
	  * Insert </p>.
	  */
	void endParagraph()
	{
		m_strBuf += wxT("</p>\n");
	}

	/**
	  * Paragraph.<br>
	  */
	void paragraph( const wxString & content, bool fCentered )
	{
		if( fCentered )
		{
			m_strBuf += wxT("<center>");
		}

		tag( wxT("p"), content, true );

		if( fCentered )
		{
			m_strBuf += wxT("</center>\n");
		}
	}

	/** Start centered section.<br> */
	void startCentered()
	{
		m_strBuf += wxT("<center>");
	}

	/** End centered section. */
	void endCentered()
	{
		m_strBuf += wxT("</center>\n");
	}

	/** Start an unordered list. */
	void startUnorderedList( int bulletType = Bullet_disc )
	{
		wxString type;

		switch( bulletType )
		{
		case Bullet_circle:
			type = wxT("circle");
			break;
		case Bullet_square:
			type = wxT("square");
			break;
		default:
			type = wxT("disc");
			break;
		}
		m_strBuf += wxString::Format(wxT("<ul type=\"%s\">\n"),type.c_str());
	}

	/** End an unordered list. */
	void endUnorderedList()
	{
		m_strBuf += wxT("</ul>");
	}

	/** list Item */
	void listItem( const wxString & text )
	{
		tag( wxT("li"), text, true );
	}

	/** anchor */
	void anchor( const wxString & anchorName, const wxString & linkText );

	/** link to external */
	void linkExternal( const wxString & href, const wxString & linkText );

	/** link to internal */
	void linkInternal( const wxString & anchorName, const wxString & linkText );

    /** image */
    void image( const wxString & imageName );

	/**
	  * Returns the internal buffer.
	  */
	wxString & buf()
	{
		return m_strBuf;
	}

	/**
	  * Heading 1
	  */
	void h1( const wxString & text )
	{
		tag( wxT("h1"), text, true );
	}

	/**
	  * Heading 2
	  */
	void h2( const wxString & text )
	{
		tag( wxT("h2"), text, true );
	}

	/**
	  * Heading 3
	  */
	void h3( const wxString & text )
	{
		tag( wxT("h3"), text, true );
	}

	/** Emphsized text. */
	void emphsized( const wxString & text )
	{
		tag( wxT("em"), text );
	}

	/** Strong text. */
	void strong( const wxString & text )
	{
		tag( wxT("strong"), text );
	}

	/** Italic text */
	void italic( const wxString & text )
	{
		tag( wxT("i"), text );
	}

	/** Bold text */
	void bold( const wxString & text )
	{
		tag( wxT("b"), text );
	}

	/** Underline text */
	void underline( const wxString & text )
	{
		tag( wxT("u"), text );
	}

	/** Stikeout text */
	void strikeout( const wxString & text )
	{
		tag( wxT("s"), text );
	}

	/** Larger text */
	void larger( const wxString & text )
	{
		tag( wxT("big"), text );
	}

	/** Smaller text */
	void smaller( const wxString & text )
	{
		tag( wxT("small"), text );
	}

	/** Subscripted text */
	void subscript( const wxString & text )
	{
		tag( wxT("sub"), text );
	}

	/** Superscripted text */
	void superscript( const wxString & text )
	{
		tag( wxT("small"), text );
	}

	/** Typewriter style text */
	void typewriter( const wxString & text )
	{
		tag( wxT("tt"), text );
	}

	/** Font style
	  */
	void font( wxColor &  color, int size, const wxString & face );

	/** Font style
	  */
	void font( const wxString & color, int size, const wxString & face );

	/** line break */
	void linebreak()
	{
		m_strBuf += wxT("<br>");
	}

	/** horizintal line */
	void hline()
	{
		m_strBuf += wxT("<hr>");
	}

	/** Start a table */
	void startTable( wxColor & bg, const wxString & width, int border = 0, int cellspacing = 2, int cellpadding = 1 );

	void startTableRow()
	{
		m_strBuf += wxT("<tr>");
	}

	void startTableRow( wxColor & bg );

	void tableHdrCell( const wxString & text )
	{
		tag( wxT("th"), text );
	}

	void tableDataCell( const wxString & text )
	{
		tag( wxT("td"), text );
	}

	void endTableRow()
	{
		m_strBuf += wxT("</tr>\n");
	}

	/** End a table */
	void endTable()
	{
		m_strBuf += wxT("</table>");
	}

	/**
	  * Append an enclosed tag.
	  */
	void tag( const wxString & xmlTag, const wxString & text, bool fIncludeNewLine = false )
	{
		m_strBuf += wxString::Format(wxT("<%s>%s</%s>"),xmlTag.c_str(),text.c_str(),xmlTag.c_str());
		if( fIncludeNewLine )
		{
			m_strBuf += wxT("\n");
		}
	}

    /**
      * Append some text.
      */
    void simpletext( const wxString & text )
    {
        m_strBuf += text;
    }

	/**
	  *  Append a line of text followed by a break.
	  */
	void line( const wxString & text )
	{
		simpletext( text );
		linebreak();
	}

	/**
	 ** Start a CDATA block
	 **/
	void startCDATA()
	{
		m_strBuf += wxT("<![CDATA[\n");
	}

	/**
	 ** End a CDATA block
	 **/
	void endCDATA()
	{
		m_strBuf += wxT("]]>\n");
	}

public:
	/** The internal string buffer */
	wxString m_strBuf;

	/** The doc name */
	wxString m_strName;
};
#endif
