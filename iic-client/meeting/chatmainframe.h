/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef CHATMAINFRAME_H
#define CHATMAINFRAME_H

//(*Headers(ChatMainFrame)
#include <wx/frame.h>
#include <wx/sizer.h>
//*)
#include <wx/aui/aui.h>
#include <wx/menu.h>
#include <wx/layout.h>
#include <wx/msgdlg.h>

#include "events.h"


class ChatPanel;
class ParticipantPanel;
class ShareControl;


const static int kCDSplitterWidth      = 305;  //  Width of Name col in PartPanel + 5 for frame widths


class ChatMainFrame: public wxFrame
{
public:

    ChatMainFrame(wxWindow* parent, ChatMainFrame **ppThis);
    virtual ~ChatMainFrame();

    //(*Identifiers(ChatMainFrame)
    //*)

public:
    void        SetDisplayName(const wxString& name);
    void        StartIM(const wxString &strIM);
    void        IncomingIM(const wxString &strFrom, const wxString &strMessage);
    void        IncomingComposing(const wxString &strFrom, bool bComposing);

    void        StartChat( const wxString &strParticipantID, const wxString &strParticipantName, const wxString &strChatID, const wxString &strChatTitle, const wxString &strPassword, bool fJoining = false );
    void        SetParticipantID( const wxString &strParticipantID );
    void        EndChat( );

    void        ConnectionLost( );
    void        ConnectionReconnected( );

    ParticipantPanel*   GetParticipantPanel( ) const { return m_pPartPanel; }

protected:
    //(*Handlers(ChatMainFrame)
    //*)
    void OnClose(wxCloseEvent& event);
    void OnQuit(wxCommandEvent& event);
    void OnContents(wxCommandEvent& event);
    void OnAbout(wxCommandEvent& event);

    //(*Declarations(ChatMainFrame)
    //*)

private:
    void            AddChat(const wxString &name);
    int             FindChat(const wxString &name, bool create=true);
    wxAuiNotebook*  CreateNotebook( );
    void            AddIM( wxString strPerson );
    void            AddChatRoom( wxString strRoomName, wxString strPerson );
    void            CreateChatRoom( wxString strRoomName, wxString strPerson );


private:
    ChatMainFrame          **m_ppThis;
    ParticipantPanel        *m_pPartPanel;
    wxStatusBar             *m_pStatusBar;
    wxAuiManager            *m_auiMgr;
    wxAuiNotebook           *m_pNotebook;

    wxString    m_strDisplayName;
    wxString    m_strScreen;

    DECLARE_CLASS(ChatMainFrame)
    DECLARE_EVENT_TABLE()
};

#endif
