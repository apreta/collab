/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 *
 *
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */
#include "app.h"
#include "meetingcentercontroller.h"
#include "iicdoc.h"
#include "docshareview.h"
#include "ipc.h"
#include "userpreferencesdialog.h"

//(*AppHeaders
#include <wx/xrc/xmlres.h>
#include <wx/image.h>
//*)

#include <wx/apptrait.h>
#include <wx/stdpaths.h>
#include <wx/tooltip.h>
#include <wx/tokenzr.h>
#include <wx/uri.h>
#include <wx/display.h>
#include "controls/wx/xh_treelist.h"
#include "controls/wx/xh_hyperlink.h"
#include "controls/wx/xh_wxTogBmpBtn.h"
#include "controls/wx/xh_wxTextCtrlEx.h"

#ifdef WIN32
#include "../sharelib/procutil.h"
#endif

#ifdef MACOSX
#include <ApplicationServices/ApplicationServices.h>
#include <Carbon/Carbon.h>

void InstallGURLHandler();
#endif


IMPLEMENT_APP(MyApp);


//*****************************************************************************
//
//*****************************************************************************

// Accepts a connection from another instance
wxConnectionBase *stServer::OnAcceptConnection(const wxString& topic)
{
    wxLogDebug( wxT("entering stServer::OnAcceptConnection( %s )"), topic.wc_str( ) );
    if( topic.Lower() == wxT("cmdline"))
    {
        // Check that there are no modal dialogs active
        wxWindowList::Node* node = wxTopLevelWindows.GetFirst();
        while (node)
        {
            wxDialog* dialog = wxDynamicCast(node->GetData(), wxDialog);
            if (dialog && dialog->IsModal() && !dialog->IsKindOf(CLASSINFO(SignOnDlg)))
            {
                return false;
            }
            node = node->GetNext();
        }
        return new stConnection();
    }
    else if( topic.Lower() == wxT("raise"))
    {
        return new stConnection();
    }
    else
        return NULL;
}


//*****************************************************************************
//
//*****************************************************************************

//  Accept command line arguments from another instance
bool stConnection::OnExecute(const wxString& topic,
                             wxChar *data,
                             int WXUNUSED(size),
                             wxIPCFormat WXUNUSED(format))
{
    wxString    strCmdLine( data );
    wxLogDebug( wxT("entering stConnection::OnExecute( %s, %s )"), topic.wc_str( ), strCmdLine.wc_str( ) );

    MeetingCenterController *pMCC = wxGetApp().m_pMC;

    //  Parse out the individual arguments from the passed command line
    if ( topic.Lower() == wxT("cmdline"))
    {
        wxGetApp( ).m_argArray.Clear( );
        wxStringTokenizer   tkz( strCmdLine, wxT("!") );
        wxLogDebug( wxT("number of tokens is %d"), tkz.CountTokens( ) );
        while( tkz.HasMoreTokens( ) )
        {
            wxString    strToken = tkz.GetNextToken( );
            wxLogDebug( wxT("  next token is **%s**"), CUtils::StripQuotes( strToken ).wc_str( ) );
            wxGetApp( ).m_argArray.Add( CUtils::StripQuotes( strToken ) );
        }
        
        if ( wxGetApp().m_argArray.GetCount( ) > 1)
        {
            wxGetApp( ).DecodeCommandLineParameters( );

            CONTROLLER.SignOn( );
        }
    }

    // Raise main window
    if( pMCC )
    {
        wxWindow * wnd = pMCC->ShowAppWindow( );
        if (wnd)
            CUtils::StealFocus( wnd );
    }

    return true;
}


//*****************************************************************************
//
//*****************************************************************************
bool MyLogWindow::OnFrameClose( wxFrame *frame )
{
    wxGetApp( ).m_pMyLogWindow = NULL;
    return true;
}

void MyLogWindow::OnFrameDelete( wxFrame *frame )
{
    wxGetApp( ).m_pMyLogWindow = NULL;
}


//*****************************************************************************
//
//*****************************************************************************
void MyApp::Clear()
{
	wxString blank;
	blank = wxT("");

	m_pLocale = 0;

	m_S.ClearStartupParameters();
}


void MyApp::OnAssertFailure( const wxChar *file, int line, const wxChar *func, const wxChar *cond, const wxChar *msg )
{
    wxString    strFile( file );
    wxString    strFunc( func );
    wxString    strCond( cond );
    wxString    strMsg( msg );
    wxLogDebug( wxT("ASSERT in file %s @ %d, func %s, cond = %s, msg = %s"), file, line, func, cond, msg );
}


// The number of log files to keep before deleting the oldest
static unsigned history_len = 10;

// If the current log is >= kMaxLogFileSize, rotate the current log
// into the stack of historical files and create a new log file.
static int kMaxLogFileSize = 100 * 1024;

void MyApp::log_rotate_if_needed( wxString &logFN, bool fAlwaysRotate )
{
    wxString    newerfn;
    wxString    olderfn;
    int i;
    long    sz      = -1;

    struct stat myStats;
    i = stat( logFN.mb_str( ), &myStats );
    if( i  ==  0 )
        sz = myStats.st_size;

    if( (sz < kMaxLogFileSize)  &&  !fAlwaysRotate )
        return;

    newerfn = wxString::Format( wxT("%s.%u"), logFN.wc_str( ), history_len );
    unlink( newerfn.mb_str( ) );
    for( i = history_len-1; i > 0; i--)
    {
        olderfn.Empty( );
        olderfn.Append( newerfn );
        newerfn = wxString::Format( wxT("%s.%u"), logFN.wc_str( ), i );
        rename( newerfn.mb_str( ), olderfn.mb_str( ) );
    }
    olderfn.Empty( );
    olderfn.Append( newerfn );
    newerfn.Empty( );
    newerfn.Append( logFN );
    rename( newerfn.mb_str( ), olderfn.mb_str( ) );
}


//*****************************************************************************
//  Event handlers
//*****************************************************************************
bool MyApp::OnInit()
{
    bool    fAnotherIsRunning = false;

    m_fExitBeforeStart = false;
    m_strProductName = wxT("Zon");
    m_server        = NULL;
    m_pDocManager   = NULL;

#ifdef WIN32
    LoadLibrary(wxT("crashlib.dll"));
#endif

    wxStandardPathsBase& stdPaths = wxStandardPaths::Get();

    m_strResourceDir = stdPaths.GetResourcesDir( );
    m_strDataDir = stdPaths.GetUserDataDir( );

#if defined(MACOSX) || defined(LINUX)
    netInitializeLibrary();
#endif

#ifdef MACOSX
	// Install as iic: handler
	InstallGURLHandler();
#endif

    m_fShowMCC = true;
    m_pMyLogWindow = NULL;

    //  Open the debug logging file
    wxString logFile = m_strDataDir + _T("/zon.log");
    log_rotate_if_needed( logFile, false );
    FILE *log = freopen( logFile.mb_str(wxConvUTF8), "a", stdout );
    wxLog::SetActiveTarget(new wxLogStderr(log));
    wxLogDebug( wxT("\n----------------------------") );

    wxDateTime  dtNow = wxDateTime::Now( );
    wxString    strToday = dtNow.Format( );
    wxLogDebug( wxT("Opening log on %s"), strToday.wc_str( ) );

    //  Set the resources path
    m_strExecDir = wxFileName(stdPaths.GetExecutablePath( )).GetPath();
    if( m_strExecDir.Find( wxT("iic-client\\bin") )  ==  wxNOT_FOUND  &&
        m_strExecDir.Find( wxT("iic-client/bin") ) == wxNOT_FOUND)
    {
        wxLogDebug( wxT("Setting current directory to %s"), m_strResourceDir.wc_str( ) );
        wxSetWorkingDirectory( m_strResourceDir );
    }
    else
    {
        // Running from devel directory, load resource from here.
		wxLogDebug(wxT("Current directory %s"), wxGetCwd().wc_str());
        m_strResourceDir = ::wxGetCwd();
    }
	
    wxString serviceName( getenv("HOME"), wxConvUTF8 );
    serviceName += wxT("/.meeting/instance");

#ifndef TESTCLIENT
    //  Create the one and only instance of the wxSingleInstanceChecker class
    wxString    strSICName = wxString::Format( wxT(".ZON%s"), wxGetUserId( ).c_str( ) );
    m_pSingleInstance = new wxSingleInstanceChecker( strSICName );
    if( m_pSingleInstance->IsAnotherRunning( ) )
    {
        wxLogDebug( wxT("Another instance is running, transferring control") );
        fAnotherIsRunning = true;

        // Suppress writing to log file
        wxLogNull logNull; 
        
        // OK, there IS another one running, so try to connect to it
        // and send it any filename before exiting.
        stClient* client = new stClient;
        wxString hostName = wxT("localhost");   // ignored under DDE, host name in TCP/IP based classes
        wxString command = argc < 2 ? wxT("raise") : wxT("cmdline");

        // Create the connection
        wxConnectionBase* connection = client->MakeConnection(hostName, serviceName, command);
        if( connection )
        {
            //  Recreate the command line arguments and pass them along...
            //  Each argument is wrapped in double quotes and separated with a '!'
            wxString    strToSend;
            for( int i=0; i<argc; i++ )
            {
                 if( !strToSend.IsEmpty( ) )
                    strToSend.Append( wxT("!") );
                wxString strT( argv[ i ] );
                strT.Prepend( wxT("\"") );
                strT.Append( wxT("\"") );
                strToSend.Append( strT );
            }
            wxLogDebug( wxT("sending command line: %s"), strToSend.wc_str( ) );

            //  Send the other instance the command line
            connection->Execute( strToSend );
            connection->Disconnect( );
            delete connection;
        }
        else
        {
            wxString msg(_("Sorry, the existing instance may be too busy too respond.\nPlease close any open dialogs and retry."));
#ifdef WIN32
            msg.Append(_("\n\nIf no windows are visible, use Task Manager to find the\n\"meeting.exe\" process and end it."));
#endif
            wxMessageBox(msg, _("Meeting Center"), wxICON_INFORMATION|wxOK );
        }
        delete client;
        return false;
    }
    else
    {
        // Create a new server
        m_server = new stServer;
        if( !m_server->Create( serviceName ) )
        {
            wxLogDebug( wxT("Failed to create an IPC service.") );
        }
        
#ifdef WIN32
        HWND CreateDetectWindow();
        CreateDetectWindow();
#endif
    }
#endif

    Clear();
    
    //  Setup the Application Default Font...settings will probably get changed when preferences are loaded.
    m_AppDefFont = wxSystemSettings::GetFont( wxSYS_DEFAULT_GUI_FONT );
#if defined(__WXGTK__) || defined(__WXMAC__)
    m_AppDefFont.SetPointSize( 10 );
    m_AppDefFont.SetWeight( wxFONTWEIGHT_NORMAL );
#endif
    PREFERENCES.SetDefaultFont(m_AppDefFont);

    // Create a new meeting center controller.
    // Regardless of what top level window is to be displayed,
    // a MeetingCenterController is created.
    m_pMC = new MeetingCenterController( 0 );
    if( m_pMC  ==  NULL )
    {
        wxLogError( wxT("The MeetingCenterController constructor failed.") );
    }

    // Set up IPC queue
    Connect(wxID_ANY, wxEVT_QUEUE_EVENT, wxCommandEventHandler( MyApp::OnQueueEvent) );
    wxString queueName = wxT("meeting.q");
    m_queue = queue_create(queueName.mb_str());
    if (queue_server(m_queue))
    {
        wxLogDebug( wxT("Error creating message queue (%d)"), GetLastError() );
    }
    queue_set_event_handler(m_queue, queue_handler, this);

    // Parse the command line parameters:
    ParseCommandLineParameters( );

    if (m_fExitBeforeStart)
    {
        OnExit();
        return false;
    }

    //  Do these inits here in case we need to ask the user for a language.
    wxXmlResource::Get()->AddHandler(new wxTextCtrlExXmlHandler());
    wxXmlResource::Get()->AddHandler(new wxTogBmpBtnXmlHandler());
    wxXmlResource::Get()->AddHandler(new wxTreeListCtrlXmlHandler());
    wxXmlResource::Get()->AddHandler(new wxHyperLinkXmlHandler());

    ::wxInitAllImageHandlers( );
    wxXmlResource::Get( )->InitAllHandlers( );

    bool wxsOK = true;
    wxsOK = wxsOK && wxXmlResource::Get( )->Load( _T("userpreferencesdialog.xrc") );

    //  Set the default Locale
    m_pLocale = NULL;
    bool    bRet;
    int     iLocaleID = OPTIONS.GetSystemInt( "LocaleID", wxLANGUAGE_UNKNOWN );

    //  Locale was not found in the options file - ask the system for it.
    if( iLocaleID  ==  wxLANGUAGE_UNKNOWN )
    {
        iLocaleID = GetLocaleFromSys( );
    }

    //  Locale was not found in the options file or returned from the system - ask the user to select one.
    if( iLocaleID  ==  wxLANGUAGE_UNKNOWN )
    {
        UserPreferencesDialog Dlg( NULL, wxID_ANY );

        int nResp = Dlg.EditLanguageOnly( );
        if( nResp == wxID_OK )
        {
            iLocaleID = OPTIONS.GetSystemInt( "LocaleID", wxLANGUAGE_UNKNOWN );
            wxLogDebug( wxT("iLocaleID is now %d"), iLocaleID );
            if( iLocaleID != wxLANGUAGE_UNKNOWN  &&  iLocaleID != wxLANGUAGE_ENGLISH_US )
            {
                m_Options.Write( );

                //  User selected a language other than en_US - we need to restart the app.
                OnExit( );

                wxString    strCmdLine;
                for( int i = 0; i < argc; ++i )
                {
                    strCmdLine.Append( argv[i] );
                    strCmdLine.Append( wxT(" ") );
                }
                ::wxExecute( strCmdLine );

                return false;
            }
        }
        iLocaleID = OPTIONS.GetSystemInt( "LocaleID", wxLANGUAGE_UNKNOWN );
    }

    //  No decision from the user - use en_US
    if( iLocaleID  ==  wxLANGUAGE_UNKNOWN )
    {
        iLocaleID = wxLANGUAGE_ENGLISH_US;
    }

    if( iLocaleID  !=  wxLANGUAGE_UNKNOWN )
    {
        m_pLocale = new wxLocale( );
        bRet = m_pLocale->Init( iLocaleID, wxLOCALE_CONV_ENCODING );
        if( !bRet )
        {
            //  LocaleID existed in the .opt file, but the system could not
            //  find a translation file so we will default to English (US)
            bRet = m_pLocale->Init( wxLANGUAGE_ENGLISH_US, wxLOCALE_CONV_ENCODING );
        }
        if( bRet )
        {
            wxLocale::AddCatalogLookupPathPrefix( wxT("./locale") );
            bRet = m_pLocale->AddCatalog( wxT("meeting") );
        }
        if( !bRet )
        {
            //  One of the steps above failed.  Unload the locale and forget about it.
            delete m_pLocale;
            m_pLocale = NULL;
        }
    }
    else
    {
        wxLogDebug( wxT("We should never be able to get here.  MyApp::OnInit( ) - no locale selected") );
    }

    wxString    strT;
    if( m_pLocale )
    {
#ifdef WIN32
        wchar_t *pWC;
        strT = m_pLocale->GetSysName( );
        pWC = _tsetlocale( LC_ALL, strT.wc_str( wxConvUTF8 ) );
        wxLogDebug( wxT("_tsetlocale( ) returned 0x%8.8X"), pWC );
#else
        strT = m_pLocale->GetCanonicalName( );
        setlocale( LC_ALL, strT.mb_str() );
#endif
    }

    m_strProductName = GetOptionName("BrandName", "Zon");

    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("meetingcentercontroller.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("signondlg.xrc"));
	wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("meetingmainframe.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("chatmainframe.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("meetingmenubar.xrc"));
	wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("mainpanel.xrc"));
	wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("participantpanel.xrc"));
	wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("chatpanel.xrc"));
	wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("meetingsetupdialog.xrc"));
	wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("popupmenus.xrc"));
	wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("selectcontactdialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("meetingconfirmation.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("endmeetingdialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("badauthcertdialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("docexplorerdialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("appsharepanel.xrc"));
//    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("userpreferencesdialog.xrc"));     //  Moved up in case we need to ask the user for a language
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("createcontactdialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("reconnectdialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("connectionprogressdialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("findmeetingsdialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("sharingcontroldialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("findcontactsdialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("managecontactsframe.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("selectapplication.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("docshareindexdialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("sendrecvprogressdialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("profilemanagerdialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("aboutdialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("waitforstartdialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("recordingdialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("pcphonepanel.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("pcphonedialdialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("chatmenubar.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("invitedialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("imageuploaddialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("projectcodedialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("selectmonitordialog.xrc"));
    wxsOK = wxsOK && wxXmlResource::Get()->Load(_T("monitorlabelframe.xrc"));

    if( !wxsOK )
    {
        wxString        strErrTitle( _("Installed File Missing") );
        wxString        strErrMessage( _("A file is missing from the installation folder.\nPlease re-install and try again.\nIf the problem persists, contact your system administrator.") );
        wxMessageDialog Err( NULL, strErrMessage, strErrTitle, wxOK | wxICON_ERROR );
        Err.ShowModal( );
        return false;
    }

#ifdef MACOSX
	// Allow launch event to be processed before we display sign on dialog
	Yield();
#endif

    if( m_pMC )
    {
		wxLogDebug(wxT("Sign on"));

		MakeForeground( );

        CONTROLLER.SignOn( );

        if ( PREFERENCES.m_fIMClient )
        {
            m_pMC->Commands().Add( wxT("manage-contacts") );
        }
        else if ( m_fShowMCC )
        {
            m_pMC->Commands().Add( wxT("meeting-center") );
        }

        // Currently we are always fully creating the meeting center window, even if not displaying it immediately (or at all).
        // The meeting center is able to operate without a GUI, except there may be edge cases that will cause a trap.
        CONTROLLER.CreateMeetingCenterWindow( );

        SetExitOnFrameDelete( true );
    }

    // Start queue processor now...
    queue_run(m_queue);

    datablock_t blk = datablock_create(0);
    datablock_add_string(blk, "/query", "");
    queue_send(m_queue, blk);

    wxToolTip::Enable( true );
    wxToolTip::SetDelay( 200 );

    // Create a document manager
    m_pDocManager = new wxDocManager;

    // Create a template relating drawing documents to their views
    (void) new wxDocTemplate(m_pDocManager, _("Drawing"), _T("*.drw"), _T(""), _T("drw"), _T("Drawing Doc"), _T("Drawing View"),
                             CLASSINFO(CIICDoc), CLASSINFO(DocShareView));

#ifdef __WXMAC__
// ??    wxFileName::MacRegisterDefaultTypeAndCreator( _("drw") , 'WXMB' , 'WXMA' );
#endif

    //  Ensure the Controller is acually visible on the display...
    if( m_fShowMCC )
    {
        wxDisplay   disp0( 0 );
        int         i;
        int         iCount      = disp0.GetCount( );
        bool        fVisible    = false;
        for( i=0; i<iCount && fVisible == false; i++ )
        {
            fVisible = IsControllerVisible( i );
        }
        if( !fVisible )
        {
            CONTROLLER.Centre( );
        }
    }

	MakeForeground( );
	
    return true;
}


//*****************************************************************************
bool MyApp::IsControllerVisible( int iDisplay )
{
    wxDisplay   disp( iDisplay );
    wxRect      rect    = disp.GetClientArea( );
    rect.Deflate( 30, 30 );

    wxRect      rGSR    = CONTROLLER.GetScreenRect( );
    rGSR.SetHeight( 10 );

    return rect.Intersects( rGSR );
}


//*****************************************************************************
int MyApp::OnExit()
{
    wxLogDebug( wxT("entering MyApp::OnExit( )" ) );

    //  Ensure the meeting sharing processes are really dead
#ifdef WIN32
    KillClientProcess( );
    KillServerProcess( );
#endif

    delete m_server;
#ifndef TESTCLIENT
    delete m_pSingleInstance;
#endif
    if( m_pMyLogWindow )
    {
        m_pMyLogWindow->GetFrame( )->Destroy( );
        m_pMyLogWindow = NULL;
    }

    queue_destroy(m_queue);

    return 0;
}

//*****************************************************************************
void MyApp::MakeForeground( )
{
#ifdef MACOSX
    ProcessSerialNumber PSN;
    GetCurrentProcess(&PSN);
	TransformProcessType(&PSN,kProcessTransformToForegroundApplication);
    SetFrontProcess(&PSN);
#endif	
}

//*****************************************************************************
void MyApp::ParseCommandLineParameters( )
{
    m_argArray.Clear( );

    wxLogDebug( wxT("number of command line arguments is %d"), argc );
	for( int i = 0; i < argc; ++i )
	{
		m_argArray.Add( argv[i] );
        wxLogDebug( wxT("   command line argument is %s"), m_argArray.Item( i ).wc_str( ) );
	}

    DecodeCommandLineParameters( );
}


//*****************************************************************************
void MyApp::DecodeCommandLineParameters( )
{
    wxString    strRest;
    wxString    strRest2;

#ifdef TESTCLIENT
    if ( (int) m_argArray.GetCount( ) >= 4 && m_argArray[1].Cmp(wxT("/test")) == 0)
	{
		int param = 2;

		while (param < (int) m_argArray.GetCount( ) && m_argArray[param].GetChar(0) == ('/') )
		{
			if ( m_argArray[param].Cmp(wxT("/noview")) == 0)
			{
				m_S.m_fStartupNoViewer = true;
			}
			if ( m_argArray[param].Cmp(wxT("/debuglog")) == 0)
			{
                //set_debug_session(TRUE);
			}
			if (m_argArray[param].Cmp(wxT("/http")) == 0)
			{
				//proxy_set_http(TRUE);
			}
			if (m_argArray[param].Cmp(wxT("/hunt")) == 0)
			{
				//proxy_reset_hunt();
			}
			++param;
		}

		m_S.m_StartupTest = m_argArray[param++];

		m_S.m_StartupScreenName = m_argArray[param++];
		if ((int) m_argArray.GetCount( ) > param)
            m_S.m_StartupServer = m_argArray[param++];
		if ((int) m_argArray.GetCount( ) > param)
			m_S.m_StartupPassword = m_argArray[param++];
		if ((int) m_argArray.GetCount( ) > param)
			m_S.m_StartupFullName = m_argArray[param++];
		if ((int) m_argArray.GetCount( ) > param)
			m_S.m_StartupMeetingId = m_argArray[param++];

		if (m_S.m_StartupScreenName == wxT("anonymous") )
			m_S.m_fStartupAnonymous = true;

        m_S.m_fStartupProvided = true;

		wxLogDebug(wxT("Test mode launched (screen=%S, password=%S, mode=%S)"), m_S.m_StartupScreenName.c_str(),
		      m_S.m_StartupPassword.c_str(), m_S.m_StartupTest.c_str());
	}
	else
#endif
	if ((int) m_argArray.GetCount( ) == 2 && m_argArray[1].Cmp(wxT("/debug")) == 0)
	{
		wxLogDebug(wxT("invoke: mode 4"));
		//g_debug = true;
	}

	/** Launched from teaming or invite web page **/

	else if ((int) m_argArray.GetCount( ) == 2 && m_argArray[1].Find(wxT("iic:launch")) == 0)
	{
		wxLogDebug(wxT("invoke: mode 6"));

        // find the mid and pid (argv will be of form: /mtg?mid=840&pid=841)
        // or of the form: iic:launch?mid=840?pid=841
        // or of the form: iic:launch?mid=840&pid=841?screen=scrname?server=test1.server.com?fullname=First%20Last
		wxString arg = m_argArray[1];

		wxString midtkn( wxT("?mid="));
		wxString pidtkn( wxT("&pid="));
        wxString scrtkn( wxT("?screen="));
        wxString svrtkn( wxT("?server="));
        wxString fultkn( wxT("?fullname="));

		int midpos = arg.Find( midtkn );
		int pidpos = arg.Find( pidtkn );
        int scrpos = arg.Find( scrtkn );
        int svrpos = arg.Find( svrtkn );
        int fulpos = arg.Find( fultkn );

		if (midpos >= 0 && pidpos >= 0)
		{
			wxLogDebug(wxT("invoke: parsing"));
            //wxLogDebug("  midpos = %d, pidpos = %d", midpos, pidpos );
			int midstart = midpos + midtkn.length();
			int midlen   = pidpos - midstart;

            int pidstart = pidpos + pidtkn.length();
            int pidlen   = (scrpos < 0) ? MAX_STRING : (scrpos - pidstart);

            int scrstart = scrpos + scrtkn.length();
            int scrlen   = svrpos - scrstart;

            int svrstart = svrpos + svrtkn.length();
            int svrlen   = fulpos - svrstart;

            int fulstart = fulpos + fultkn.length();

			wxString mid = arg.Mid( midstart, midlen );
			wxString pid = arg.Mid( pidstart, pidlen );
            wxString scr;
            wxString svr;
            wxString ful;
            if( scrlen  >  0 )
            {
                scr = arg.Mid( scrstart, scrlen );
                svr = arg.Mid( svrstart, svrlen );
                ful = arg.Mid( fulstart);
            }
            if (pidlen == 0)
                pid = mid;

            CUtils::DecodeStringFully( svr );
            CUtils::DecodeStringFully( ful );
            ful = CUtils::StripQuotes( ful );

            wxLogDebug( wxT("  mid = %s,   pid = %s,   svr = %s,   svr = %s,   ful = %s"), mid.wc_str(), pid.wc_str(), scr.wc_str(), svr.wc_str(), ful.wc_str() );

            //  remove any trailing "[1]" cruft from pid
			int iCruft;
			iCruft = pid.Find( wxT("[") );
			if( iCruft  >=  0 )
			{
				wxString cstrT;
				cstrT = pid.Left( iCruft );
				pid = cstrT;
				wxLogDebug( wxT("  new pid value is %S"), pid.c_str() );
			}
            iCruft = ful.Find( wxT("[") );
            if( iCruft  >=  0 )
            {
                wxString cstrT;
                cstrT = ful.Left( iCruft );
                ful = cstrT;
                wxLogDebug( wxT("  new ful value is %S"), ful.c_str() );
            }

            m_S.m_StartupParticipantId = pid;
            m_S.m_StartupMeetingId = mid;

            m_S.m_StartupFullName = ful;
            m_S.m_StartupScreenName = scr;
            m_S.m_StartupServer     = svr;

            wxLogDebug(wxT("Lookup meeting params from document"));

			if ( !ful.IsEmpty() || MeetingLookup(mid, pid) == 0 )
			{
                wxLogDebug(wxT("invoke: params validated"));

                if( m_pMC )
                {
                    m_pMC->Commands().Add( wxT("iic:launch"),  pid, m_S.m_StartupFullName, mid, wxString( wxT("Meeting Title") ) );
                    m_fShowMCC = false;
                }
                else
                {
                    wxLogError( _T("No meeting controller") );
                }
			}
			else
			{
                // Unable to validate meeting.  Display login in
                // anonymous mode with just the pins.
				wxLogDebug(wxT("invoke: unable to validate params"));
				m_S.ClearStartupParameters( );
				m_S.m_StartupScreenName = wxT("anonymous");
				m_S.m_StartupMeetingId = mid;
				m_S.m_StartupParticipantId = pid;
			}

			m_S.m_fStartupProvided = true;
		}
	}
    else if( (int) m_argArray.GetCount( ) == 2  &&
             m_argArray[1].StartsWith( wxT("iic:meetmany"), &strRest ) )
    {
        //  iic:meetmany?meetingtoken=169
        wxLogDebug( wxT("Protocol received: %s"), m_argArray[1].wc_str( ) );
        if( strRest.StartsWith( wxT("?meetingtoken="), &strRest2 ) )
        {
            if( m_pMC )
            {
                m_pMC->Commands().Add( wxT("iic:meetmany"), strRest );
//                m_fShowMCC = false;
            }
            else
            {
                wxLogError( _T("No meeting controller") );
            }
        }
        else
            wxLogError( _T("Malformed iic:meetmany command - ***%s***"), m_argArray[1].wc_str( ) );
	}
    else if( (int) m_argArray.GetCount( ) == 2  &&
             m_argArray[1].StartsWith( wxT("iic:meetone"), &strRest ) )
    {
        //  iic:meetone?screenName=miket
        wxLogDebug( wxT("Protocol received: %s"), m_argArray[1].wc_str( ) );
        if( strRest.StartsWith( wxT("?screenName="), &strRest2 ) )
        {
            if( m_pMC )
            {
                m_pMC->Commands().Add( wxT("iic:meetone"), strRest );
                m_fShowMCC = false;
            }
            else
            {
                wxLogError( _T("No meeting controller") );
            }
        }
        else
            wxLogError( _T("Malformed iic:meetone command - ***%s***"), m_argArray[1].wc_str( ) );
    }
    else if( (int) m_argArray.GetCount( ) == 2  &&
             m_argArray[1].StartsWith( wxT("iic:start"), &strRest ) )
    {
        //  iic:start?meetingid=111
        wxLogDebug( wxT("Protocol received: %s"), m_argArray[1].wc_str( ) );
        if( strRest.StartsWith( wxT("?meetingid="), &strRest2 ) )
        {
            if( m_pMC )
            {
                m_pMC->Commands().Add( wxT("iic:start"), strRest2 );
                m_fShowMCC = false;
            }
            else
            {
                wxLogError( _T("MeetingCenterController not available") );
            }
        }
        else
            wxLogError( _T("Malformed iic:start command - ***%s***"), m_argArray[1].wc_str( ) );
    }
    else if ( (int) m_argArray.GetCount( ) == 2  &&
             m_argArray[1].StartsWith( wxT("iic:im"), &strRest ) )
    {
        wxLogDebug(wxT("Protocol received: %s"), m_argArray[1].wc_str( ) );
        if( strRest.StartsWith( wxT("?screenName="), &strRest2 ) )
        {
            wxString strScreen = wxURI::Unescape(strRest2);

            if ( CONTROLLER.IMClientMode() )
            {
                m_pMC->Commands().Add( wxT("start-im"), strScreen );
            }
            else
            {
                datablock_t blk = datablock_create(0);
                datablock_add_string(blk, "/im", "");
                datablock_add_string(blk, strScreen.mb_str(wxConvUTF8), "");
                queue_send(m_queue, blk);
                datablock_destroy(blk);
            }
        }

        m_fExitBeforeStart = !IsMainLoopRunning( );

    }
	else if ((int) m_argArray.GetCount( ) == 2 && m_argArray[1].Find(wxT("iic:loopback")) == 0 )
	{
		wxLogDebug(wxT("PROTOCOL LOOPBACK - INIT: %s"), m_argArray[1].wc_str());
	}

	/** Following are sent from Zon protocol plugin **/

	else if ((int) m_argArray.GetCount( ) == 2 && m_argArray[1].Cmp(wxT("/cmd")) == 0)
	{
		wxLogDebug(wxT("invoke: mode 5"));
		/* Read command from queue and process before we display sign on dialog */
        ProcessQueueCommand();
	}
	else if ((int) m_argArray.GetCount( ) == 5 && m_argArray[1].Cmp(wxT("/signon")) == 0)
	{
	    int i = 2;
        m_S.m_StartupScreenName =   CUtils::StripQuotes( m_argArray[i++] );
        m_S.m_StartupServer =		CUtils::StripQuotes( m_argArray[i++] );
        m_S.m_StartupPassword =		CUtils::StripQuotes( m_argArray[i++] );
        m_S.m_fStartupProvided = true;
        wxLogDebug(wxT("Invoke: signon for %s"), m_S.m_StartupScreenName.wc_str());
	}
	else if ((int) m_argArray.GetCount( ) == 2 && m_argArray[1].Cmp(wxT("/showmc")) == 0)
	{
		wxLogDebug(wxT("Invoke: show meeting center"));
        MeetingCenterController *pMCC = wxDynamicCast( wxGetApp().GetTopWindow(), MeetingCenterController );
        if( pMCC )
        {
            CUtils::StealFocus(pMCC);
        }
	}
	else if ((int) m_argArray.GetCount( ) == 2 && m_argArray[1].Cmp(wxT("/contacts")) == 0)
	{
		wxLogDebug(wxT("Invoke: manage contacts"));
        if ( m_pMC )
        {
            CUtils::StealFocus(m_pMC);
            m_pMC->Commands().Add( wxT("manage-contacts") );
        }
	}
	else if ((int) m_argArray.GetCount( ) == 2 && m_argArray[1].Cmp(wxT("/start")) == 0)
	{
		wxLogDebug(wxT("Invoke: start instant"));
        if( m_pMC )
        {
            CUtils::StealFocus(m_pMC);
            m_pMC->Commands().Add( wxT("start-instant") );
        }
	}
	else if ((int) m_argArray.GetCount( ) >= 5 && m_argArray[1].Cmp(wxT("/invite")) == 0)
	{
	    int i = 2;
	    wxString userid = CUtils::StripQuotes( m_argArray[i++] );
	    wxString screen = CUtils::StripQuotes( m_argArray[i++] );
	    wxString name = CUtils::StripQuotes( m_argArray[i++] );
        wxLogDebug(wxT("Invoke: start with %s"), userid.wc_str());
        if( m_pMC )
        {
            CUtils::StealFocus(m_pMC);
            m_pMC->Commands().Add( wxT("start-invite"), userid, screen, name);
        }
	}
	else if ((int) m_argArray.GetCount( ) >= 5 && m_argArray[1].Cmp(wxT("/schedule")) == 0)
	{
	    int i = 2;
	    wxString userid = CUtils::StripQuotes( m_argArray[i++] );
	    wxString screen = CUtils::StripQuotes( m_argArray[i++] );
	    wxString name = CUtils::StripQuotes( m_argArray[i++] );
        wxLogDebug(wxT("Invoke: start with %s"), userid.wc_str());
        if( m_pMC )
        {
            CUtils::StealFocus(m_pMC);
            m_pMC->Commands().Add( wxT("schedule-invite"), userid, screen, name);
        }
	}
	else if ((int) m_argArray.GetCount( ) >= 5 && m_argArray[1].Cmp(wxT("/join")) == 0)
	{
	    int i = 2;
	    wxString mid = CUtils::StripQuotes( m_argArray[i++] );
	    wxString pid = CUtils::StripQuotes( m_argArray[i++] );
	    wxString name = CUtils::StripQuotes( m_argArray[i++] );
        wxLogDebug(wxT("Invoke: join with %s"), pid.wc_str());
        if( m_pMC )
        {
            CUtils::StealFocus(m_pMC);
            m_pMC->Commands().Add( wxT("join-meeting-by-pin"), pid);
        }
	}
	else if ((int) m_argArray.GetCount( ) >= 4 && m_argArray[1].Cmp(wxT("/presence")) == 0 && CONTROLLER.IMClientMode() )
	{
	    int i = 2;
	    wxString screen = CUtils::StripQuotes( m_argArray[i++] );
	    wxString presence = CUtils::StripQuotes( m_argArray[i++] );
	    wxString status;
	    if (m_argArray.GetCount( ) >= 5)
            status = CUtils::StripQuotes( m_argArray[i++] );
        wxLogDebug(wxT("Invoke: presence %s for %s"), presence.wc_str(), screen.wc_str());
        if( m_pMC )
        {
            m_pMC->UpdatePresence(_T("zon"), screen, presence, status);
        }
	}

	/** One last form, launched from iicplugin ActiveX **/

    else if( (int) m_argArray.GetCount( ) == 8 || (int) m_argArray.GetCount( ) == 9 )
    {
        int i=1;
        if (CUtils::StripQuotes( m_argArray[i]) == _T("iic:plugin")) i++;
        m_S.m_StartupMeetingId =    CUtils::StripQuotes( m_argArray[i++] );
        m_S.m_StartupParticipantId =CUtils::StripQuotes( m_argArray[i++] );
        m_S.m_StartupScreenName =   CUtils::StripQuotes( m_argArray[i++] );
        m_S.m_StartupServer =		CUtils::StripQuotes( m_argArray[i++] );
        m_S.m_StartupPhone =		CUtils::StripQuotes( m_argArray[i++] );
        m_S.m_StartupEmail =		CUtils::StripQuotes( m_argArray[i++] );
        m_S.m_StartupFullName =		CUtils::StripQuotes( m_argArray[i++] );

        if( m_pMC )
        {
            /* ToDo: don't add if we're not signed on, sign-on with pin will get us into meeting */
            m_pMC->Commands().Add( wxT("iic:launch"),  m_S.m_StartupParticipantId, m_S.m_StartupFullName, m_S.m_StartupMeetingId, wxString( wxT("Meeting Title") ) );
            m_fShowMCC = false;
        }
        m_S.m_fStartupProvided = true;
    }
}

//*****************************************************************************
void MyApp::ProcessQueueCommand()
{
    datablock_t blk = queue_get(m_queue, 5000);

    if (blk)
    {
        const char *param;

        wxGetApp( ).m_argArray.Clear( );
        wxGetApp( ).m_argArray.Add( wxT("meeting.exe") );

        while( (param = datablock_get_string(blk)) != NULL )
        {
            wxString strParam(param, wxConvUTF8);
            wxLogDebug(wxT("Parameter: %s"), strParam.wc_str());
            wxGetApp( ).m_argArray.Add( strParam );
        }

        DecodeCommandLineParameters( );

        if ( CONTROLLER.AlreadySignedOn( ) )
            CONTROLLER.ExecutePendingCommands( );

        datablock_destroy(blk);
    }
}

void MyApp::queue_handler(void *u, datablock_t blk)
{
    MyApp *myapp = (MyApp*)u;
    wxCommandEvent ev(wxEVT_QUEUE_EVENT, wxID_ANY);
    ev.SetClientData( blk );
    myapp->AddPendingEvent( ev );
}

void MyApp::OnQueueEvent(wxCommandEvent& event)
{
    datablock_t blk = (datablock_t) event.GetClientData();
    const char *param;

    param = datablock_peek_string(blk);
    if (param && strcmp(param, "/status") == 0)
    {
        if (m_pMC && m_pMC->IsMeetingActive())
            SendQueueReply(wxT("active"));
        else
            SendQueueReply(wxT("inactive"));
    }
    else
    {
        // Build argument list from datablock
        wxGetApp( ).m_argArray.Clear( );
        wxGetApp( ).m_argArray.Add( wxT("meeting.exe") );

        while( (param = datablock_get_string(blk)) != NULL )
        {
            wxString strParam(param, wxConvUTF8);
            wxLogDebug(wxT("Parameter: %s"), strParam.wc_str());
            wxGetApp( ).m_argArray.Add( strParam );
        }

        // Process new command
        DecodeCommandLineParameters( );

        if ( CONTROLLER.AlreadySignedOn( ) )
            CONTROLLER.ExecutePendingCommands( );
    }

    datablock_destroy(blk);
}

void MyApp::SendQueueReply(wxString msg)
{
    datablock_t blk = datablock_create(0);
    datablock_add_string(blk, msg.mb_str(), "");

    queue_send(m_queue, blk);

    datablock_destroy(blk);
}

void MyApp::SendQueueInvite(wxString& strMeetingID, wxString& strTitle, wxString& strDescrip, wxString& strMessage,
                            wxString& strPartID, wxString& strPartName, wxString& strSystem, wxString& strScreen)
{
    datablock_t blk = datablock_create(0);
    datablock_add_string(blk, "/invite", "");
    datablock_add_string(blk, strMeetingID.mb_str(), "");
    datablock_add_string(blk, strTitle.mb_str(wxConvUTF8), "");
    datablock_add_string(blk, strDescrip.mb_str(wxConvUTF8), "");
    datablock_add_string(blk, strMessage.mb_str(wxConvUTF8), "");
    datablock_add_string(blk, strPartID.mb_str(wxConvUTF8), "");
    datablock_add_string(blk, strPartName.mb_str(wxConvUTF8), "");
    datablock_add_string(blk, strSystem.mb_str(wxConvUTF8), "");
    datablock_add_string(blk, strScreen.mb_str(wxConvUTF8), "");

    queue_send(m_queue, blk);

    datablock_destroy(blk);
}

void MyApp::SendQueueStatus(wxString strStatus)
{
    datablock_t blk = datablock_create(0);
    datablock_add_string(blk, "/status", "");
    datablock_add_string(blk, strStatus.mb_str(), "");

    queue_send(m_queue, blk);

    datablock_destroy(blk);
}

void MyApp::OpenDebugWindow( )
{
    if (m_pMyLogWindow == NULL)
    {
        m_pMyLogWindow = new MyLogWindow( NULL, _T("Log Window") );
        m_pMyLogWindow->GetFrame( )->Centre( );
        m_pMyLogWindow->GetFrame( )->SetSize( 700, 600 );
    }
}

wxString MyApp::GetOptionName(const char *name, const char *defValue)
{
    wxString strDef(defValue, wxConvUTF8);
    // Get product name from system option file, but allow override of brand name from translation
    wxString result = wxString(wxGetApp().Options().GetSystem(name, defValue), wxConvUTF8);
    wxString strCheckName = wxGetTranslation(strDef);
    if (strCheckName != strDef)
        result = strCheckName;
    return result;
}


//*****************************************************************************
int MyApp::GetLocaleFromSys( )
{
    //  See if wxLocale knows the language of the OS
    char    *pC;
    int     iRetVal = wxLANGUAGE_UNKNOWN;
    int     iSysLang;
    iSysLang = wxLocale::GetSystemLanguage( );

    if( iSysLang  ==  wxLANGUAGE_UNKNOWN )
    {
        //  That didn't work - call setlocale( ) and see if its return value matches any know language.
        pC = setlocale( LC_ALL, "" );

        wxString    strT99( pC, wxConvUTF8 );
        wxLanguageInfo  *pLangInfo;
        pLangInfo = ( wxLanguageInfo * ) wxLocale::FindLanguageInfo( strT99 );

        if( !pLangInfo )
        {
            wxString    strT98;
            strT98 = strT99.BeforeFirst( wxChar( '.' ) );
            pLangInfo = ( wxLanguageInfo * ) wxLocale::FindLanguageInfo( strT98 );

            if( !pLangInfo )
            {
                wxString    strT97;
                strT97 = strT98.BeforeFirst( wxChar( '_' ) );
                pLangInfo = ( wxLanguageInfo * ) wxLocale::FindLanguageInfo( strT97 );

                if( !pLangInfo )
                {
                    wxString    strT96;
                    strT96 = strT98;
                    strT96.Replace( wxT("_"), wxT("-") );
                    pLangInfo = ( wxLanguageInfo * ) wxLocale::FindLanguageInfo( strT96 );
                }
            }
        }
        if( pLangInfo )
            iSysLang = pLangInfo->Language;
    }

    //  The system returned a language, see if its one we know about
    if( iSysLang  !=  wxLANGUAGE_UNKNOWN )
    {
        wxLanguageInfo  *pInfo;
        wxString        strDir;
        wxString        strRest;
        strDir = wxFindFirstFile( wxT("locale/*"), wxDIR );
        while( !strDir.IsEmpty( ) )
        {
            if( strDir.StartsWith( wxT("locale"), &strRest ) )
                strDir = strRest;
            if( strDir.StartsWith( wxT("."), &strRest ) )
                strDir = strRest;
            if( strDir.StartsWith( wxT("."), &strRest ) )
                strDir = strRest;
            if( strDir.StartsWith( wxT("/"), &strRest ) )
                strDir = strRest;
            if( strDir.StartsWith( wxT("\\"), &strRest ) )
                strDir = strRest;
            pInfo = (wxLanguageInfo *) wxLocale::FindLanguageInfo( strDir );
            if( pInfo  &&  (pInfo->Language  ==  iSysLang) )
            {
                iRetVal = iSysLang;
                break;
            }
            strDir = wxFindNextFile( );
        }
    }

    return iRetVal;
}


#include <wx/url.h>
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>

//*****************************************************************************
// ToDo: present UI while executing lookup; allow cancellation
int MyApp::MeetingLookup(const wxString& mid, const wxString& pid)
{
    CWebDavUtils utils;
    bool result = FALSE;
    int retries = 3;

    bool m_fCanceled = false;

    if (mid.length() && pid.length())
    {
        // get meeting lookup URL from system preferences
        wxString strUrl(wxGetApp().Options().GetSystem("MeetingLookupURL", ""), wxConvUTF8);

        // A long string of 'M' characters is used in the default installer--it should get replaced by the actual
        // URL during the server installation process, but that might not have happened.
        if (strUrl.IsEmpty() || strUrl.Left(5) == wxT("MMMMM"))
        {
            wxLogDebug(wxT("Meeting Lookup: no meeting lookup url"));
            return -1;
        }   

        wxURL url(strUrl);

        while (--retries && result == FALSE && !m_fCanceled)
        {
			wxString strStatusCode, strStatusText;
			wxString strServerURL, strPath;
			wxString strOut;
			wxString strBlank;

            // make HTTP call to meeting lookup URL with passed params
            strServerURL = wxString::Format(wxT("%s://%s"), url.GetScheme().wc_str(), url.GetServer().wc_str());
            strPath = wxString::Format(wxT("%s?mid=%s&pid=%s"), url.GetPath().wc_str(), mid.c_str(), pid.c_str());

            long res = utils.RetrieveFile(strServerURL, strPath, strOut, strBlank, strBlank, strStatusCode, strStatusText, false, NULL);

            if (res == S_OK && strStatusCode == _T("200"))
            {
                // parse xml if result valid
                xmlrpc_value *retval = NULL;
                xmlrpc_env env;
                xmlrpc_env_init(&env);
				retval = xmlrpc_parse_response(&env, (const char*)strOut.utf8_str(), strlen(strOut.utf8_str()));
                if (!env.fault_occurred)
                {
                    int  meetingFound;
                    char *meetingId, *title, *time;
                    char *host, *server, *partId, *screenName, *name;
                    int  options;
                    char *desc, *invite, *pass, *partPhone, *partEmail;

                    xmlrpc_parse_value(&env, retval, "(issssssssisssss)", &meetingFound, &meetingId, &title, &time,
                                                    &host, &server, &partId, &screenName, &name, &options,
                                                    &desc, &invite, &pass, &partPhone, &partEmail);

                    if (!env.fault_occurred && (meetingFound == 1 || meetingFound == 2))
                    {
                        m_S.m_StartupServer = wxString(server, wxConvUTF8);
                        m_S.m_StartupFullName = wxString(name, wxConvUTF8);
                        m_S.m_StartupScreenName = wxString(screenName, wxConvUTF8);
                        wxLogDebug(wxT("Found name %s, server %s"), m_S.m_StartupFullName.wc_str(), m_S.m_StartupServer.wc_str());
                        result = true;
                    }

                } // if ! fault occurred

                xmlrpc_env_clean(&env);
            }
            else
            {

                wxLogDebug(wxT("Meeting Lookup %d failed: status='%s'"), retries, strStatusCode.wc_str());
			}
        }  // while retries

        if (!retries && !result)
        {
            wxLogDebug(wxT("Meeting Lookup: Maximum retries exceeded"));
            return -1;
        }
    }
    else
    {
        wxLogDebug(wxT("Meeting Lookup: Maximum retries exceeded"));
        return -1;
    }

    return 0;
}


#ifdef WIN32

//*****************************************************************************
// Create hidden window with well known class name so installer can detect 
// if client is running.

#define EXIT_MSG 0x31

static LRESULT CALLBACK MainWndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
        case WM_COPYDATA:
        {
        	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;
            int dwdata = pCopyDataStruct->dwData;
            if (dwdata == EXIT_MSG)
                exit(0);
        }

        default:
            return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }
    return 0;
}

static BOOL InitDetectWindow()
{
    WNDCLASSEX wcx;

    // Fill in the window class structure with parameters
    // that describe the main window.

    wcx.cbSize = sizeof(wcx);          // size of structure
    wcx.style = CS_HREDRAW |
        CS_VREDRAW;                    // redraw if size changes
    wcx.lpfnWndProc = MainWndProc;     // points to window procedure
    wcx.cbClsExtra = 0;                // no extra class memory
    wcx.cbWndExtra = 0;                // no extra window memory
    wcx.hInstance = wxGetInstance();   // handle to instance
    wcx.hIcon = NULL;                  // predefined app. icon
    wcx.hCursor = NULL;                // predefined arrow
    wcx.hbrBackground = NULL;
    wcx.lpszMenuName =  NULL;          // name of menu resource
    wcx.lpszClassName = wxT("IICMainClass"); // name of window class
    wcx.hIconSm = NULL;

    // Register the window class.

    return RegisterClassEx(&wcx);
}

HWND CreateDetectWindow()
{
    HWND hwnd;

    InitDetectWindow();
    
    // Create the main window.

    hwnd = CreateWindow(
        wxT("IICMainClass"), // name of window class
        wxT("Sample"),            // title-bar string
        WS_OVERLAPPEDWINDOW, // top-level window
        CW_USEDEFAULT,       // default horizontal position
        CW_USEDEFAULT,       // default vertical position
        CW_USEDEFAULT,       // default width
        CW_USEDEFAULT,       // default height
        (HWND) NULL,         // no owner window
        (HMENU) NULL,        // use class menu
        wxGetInstance(),     // handle to application instance
        (LPVOID) NULL);      // no window-creation data

    return hwnd;
}

#endif

#ifdef MACOSX

//*****************************************************************************
// Install apple event handler so we can receive notifications of openning 
// "iic:" url.

static OSErr GURLHandler(AppleEvent *appleEvent,
						  AppleEvent* reply,
                         long handlerRefCon) 
{ 
    OSErr   err; 
    DescType  returnedType; 
    Size    actualSize; 
    char    URLString[1024];  
 
    if ((err = AEGetParamPtr(appleEvent, keyDirectObject, typeChar, &returnedType,  
                             URLString, sizeof(URLString)-1, &actualSize)) != noErr)
	{ 
        return err; 
    } 
 
    URLString[actualSize] = 0;    // Terminate the C string 

    wxString url( URLString, wxConvUTF8 );
	wxLogDebug( wxT("Received url: %s"), url.wc_str( ) );

	wxGetApp( ).m_argArray.Clear( );
	wxGetApp( ).m_argArray.Add( wxT("meeting.app") );
    wxGetApp( ).m_argArray.Add( url );
	wxGetApp( ).DecodeCommandLineParameters( );

	if ( CONTROLLER.AlreadySignedOn( ) )
		CONTROLLER.ExecutePendingCommands( );
 
    return noErr; 
} 

void InstallGURLHandler()
{
    OSErr err; 
    AEEventHandlerUPP upp; 
 
	upp = NewAEEventHandlerUPP((AEEventHandlerProcPtr)GURLHandler); 
	err = AEInstallEventHandler('GURL', 'GURL', upp, 0, FALSE); 
}

#endif
