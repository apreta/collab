/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
// imagelistcomposite.h: interface for the CImageListComposite class.
//
//  JEP - 2007/02/07 - ported to use with wxWidgets
//
//////////////////////////////////////////////////////////////////////

#if !defined(WX_IMAGELISTCOMPOSITE_H__2808177B_5876_430B_8FB3_1E1F2C0A51EF__INCLUDED_)
#define WX_IMAGELISTCOMPOSITE_H__2808177B_5876_430B_8FB3_1E1F2C0A51EF__INCLUDED_

#ifdef MACOSX
// We need platoform.h to get __WXMAC_CARBON__ which forces wx/imaglist.h to NOT include wx/generic/imaglist.h on the Mac
#include <wx/platform.h>
#endif

#include <wx/imaglist.h>

/*
    This class allows a single wxImageList to represent
    a set of distinct images which can be manipulated
    independently.  It is intended for use with wxTreeListCtrl.

    The tree control image can be manipulated as a set of
    independent sub-images.

          tree control image
        |-------------------|
        |                   |
        |  A      1      X  | tree item caption
        |                   |
          sub    sub    sub
         image  image  image
           1      2      3

    The consumer provides an array of bitmap ids that can be
    selected for each sub-image.

        sub img 1 = A, B
        sub img 2 = 1, 2
        sub img 3 = X, Y

    When the CImageListComposite is created, bitmaps in the
    imagelist are generated dynamically for all combinations
    of the given bitmap id arrays.

        A 1 X
        A 1 Y
        A 2 X
        A 2 Y
        B 1 X
        B 1 Y
        B 2 X
        B 2 Z

    After Create( ) has been called, GetImageList( ) can be called
    to retrieve a pointer to the generated list of images.

    The consumer can then get an index into the generated
    imagelist by setting each selected sub-image, then
    called GetSelectedIndex( ).

*/

//WX_DECLARE_OBJARRAY(wxString, wxStringArray);

WX_DEFINE_ARRAY_INT(int, intArray);
WX_DEFINE_ARRAY_PTR(wxBitmap*, SubimageArray);


class CImageListComposite : public wxObject
{
    wxImageList     m_ImageList;
    SubimageArray   m_SubimageArray;
    intArray        m_SubimageArraySize;

    intArray        m_SubimageSelections;

    int             m_SubimageCount;
    int             m_GeneratedOffset;
    int             m_BitmapHeight;
    int             m_BitmapWidth;

    void GenerateBitmaps(int aCurrentSubimages, intArray &arraySubimageIndexes);

public:
	CImageListComposite();
	virtual ~CImageListComposite();

    // Add an array of bitmap ids for a sub-image.
    //
    // This is called for each desired sub-image,
    // in the order that the sub-image should be
    // positioned in the resulting composite image.
    //
    // Returns an integer id that can be used to
    // identify the associated sub-image.
    //
    int AddSubImageArray(const wxString &aNameBase, int aArraySize);

    // Creates the composite imagelist.  If the initial
    // image list parameter is not NULL, then its images
    // are added at the front of the created imagelist.
    //
    // All previously added sub-image arrays are then
    // processed.  An image will be generated for each
    // possible combination of the provided sub-image
    // bitmaps.
    //
    void Create(wxImageList *aInitialImageList);

    // Retrieves a pointer to the generated imagelist
    wxImageList *GetImageList();

    // Select the specific image within the specified
    // set of sub-images.
    void SelectSubImage(int aSubImageId, int aSelectedIndex);

    // Get the actual index into the generated imagelist
    // for the current combination of selected sub-images
    int GetSelectedListIndex();
};

#endif // !defined(WX_IMAGELISTCOMPOSITE_H__2808177B_5876_430B_8FB3_1E1F2C0A51EF__INCLUDED_)
