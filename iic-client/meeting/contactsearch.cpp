/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "wx_pch.h"
#include "contactsearch.h"
#include "utils.h"
#include <wx/tokenzr.h>
#include <wx/combobox.h>

ContactSearch::ContactSearch():
m_fFirstName(true),
m_fLastName(true),
m_fEmailAddresses(false),
m_fPhoneNumbers(false),
m_fMore1(false),
m_nField1(0),
m_fMore2(false),
m_nField2(0),
m_fMore3(false),
m_nField3(0),
m_nSelectedGroups(Groups_All),
m_nMaxResults(100)
{
    m_strFirstName = wxT("*");
    m_strLastName = wxT("*");

    InitializeContactFields();

}

void ContactSearch::InitializeContactFields()
{
    AddField( wxT(""),              _("<Select>") );
    AddField( wxT("title"),         _("Title") );
    AddField( wxT("first"),         _("First name") );
    AddField( wxT("middle"),        _("Middle name") );
    AddField( wxT("last"),          _("Last name") );
    AddField( wxT("suffix"),        _("Suffix") );
    AddField( wxT("company"),       _("Company") );
    AddField( wxT("jobtitle"),      _("Job title") );
    AddField( wxT("address1"),      _("Address 1") );
    AddField( wxT("address2"),      _("Address 2") );
    AddField( wxT("state"),         _("State") );
    AddField( wxT("country"),       _("Country") );
    AddField( wxT("postalcode"),    _("Postal code") );
    AddField( wxT("screenname"),    _("Screen name") );
    AddField( wxT("email"),         _("Email") );
    AddField( wxT("email2"),        _("Email 2") );
    AddField( wxT("email3"),        _("Email 3") );
    AddField( wxT("busphone"),      _("Business phone") );
    AddField( wxT("homephone"),     _("Home phone") );
    AddField( wxT("mobilephone"),   _("Mobile phone") );
    AddField( wxT("otherphone"),    _("Other phone") );
    AddField( wxT("extension"),     _("Extension") );
    AddField( wxT("msnscreen"),     _("Messenger screen") );
    AddField( wxT("yahooscreen"),   _("Yahoo screen") );
    AddField( wxT("aimscreen"),     _("AIM screen") );
    AddField( wxT("user1"),         _("User 1") );
    AddField( wxT("user2"),         _("User 2") );

}

void ContactSearch::AddField( const wxString & dbField, const wxString & displayField )
{
    m_dbField.Add( dbField );
    m_displayField.Add( displayField );
}

void ContactSearch::Encode( wxArrayString & data ) const
{
    data.Clear();
    /*  0 */ data.Add( wxString::Format(wxT("%d"),m_fFirstName) );
    /*  1 */ data.Add( m_strFirstName );
    /*  2 */ data.Add( wxString::Format(wxT("%d"),m_fLastName ) );
    /*  3 */ data.Add( m_strLastName );
    /*  4 */ data.Add( wxString::Format(wxT("%d"),m_fEmailAddresses ) );
    /*  5 */ data.Add( m_strEmailAddress );
    /*  6 */ data.Add( wxString::Format(wxT("%d"),m_fPhoneNumbers ) );
    /*  7 */ data.Add( m_strPhoneNumbers );
    /*  8 */ data.Add( wxString::Format(wxT("%d"),m_fMore1 ) );
    /*  9 */ data.Add( wxString::Format(wxT("%d"),m_nField1 ) );
    /* 10 */ data.Add( m_strField1Text );
    /* 11 */ data.Add( wxString::Format(wxT("%d"),m_fMore2 ) );
    /* 12 */ data.Add( wxString::Format(wxT("%d"),m_nField2 ) );
    /* 13 */ data.Add( m_strField2Text );
    /* 14 */ data.Add( wxString::Format(wxT("%d"),m_fMore3 ) );
    /* 15 */ data.Add( wxString::Format(wxT("%d"),m_nField3 ) );
    /* 16 */ data.Add( m_strField3Text );
    /* 17 */ data.Add( wxString::Format(wxT("%d"),m_nSelectedGroups ) );
    /* 18 */ data.Add( wxString::Format(wxT("%d"),m_nMaxResults ) );
    //Add furutre parameters here
}

void ContactSearch::Encode( wxString & data ) const
{
    wxArrayString array;
    Encode( array );
    size_t i;
    data = wxT("");
    for( i = 0; i < array.GetCount(); ++i )
    {
        data += array[i];        if( i < (array.GetCount()-1) )
        {
            data += wxT("|");
        }
    }
}

void ContactSearch::Decode( const wxString & data )
{
    if( data.IsEmpty() )
        return;
    if( data.length() < 1 )
        return;

    wxArrayString arrayData;

    wxStringTokenizer tkz( data, wxT("|"));

    while ( tkz.HasMoreTokens() )
    {
        wxString token = tkz.GetNextToken();
        arrayData.Add( token );
    }
    Decode( arrayData );
}

void ContactSearch::Decode( const wxArrayString & data )
{
    bool fOK = true;
    long val;
    for( size_t i = 0; fOK && i < data.GetCount(); ++i )
    {
        switch(i)
        {
///////////////////////
            case 0: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_fFirstName = (val) ? true:false;
                    break;
            case 1: m_strFirstName = data[i];
                    break;
            case 2: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_fLastName = (val) ? true:false;
                    break;
            case 3: m_strLastName= data[i];
                    break;
            case 4: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_fEmailAddresses = (val) ? true:false;
                    break;
            case 5: m_strEmailAddress = data[i];
                    break;
            case 6: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_fPhoneNumbers = (val) ? true:false;
                    break;
            case 7: m_strPhoneNumbers = data[i];
                    break;
            case 8: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_fMore1 = (val) ? true:false;
                    break;
            case 9: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_nField1 = val;
                    break;
            case 10: m_strField1Text = data[i];
                    break;
            case 11: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_fMore2 = (val) ? true:false;
                    break;
            case 12: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_nField2 = val;
                    break;
            case 13: m_strField2Text = data[i];
                    break;
            case 14: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_fMore3 = (val) ? true:false;
                    break;
            case 15: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_nField3 = val;
                    break;
            case 16: m_strField3Text = data[i];
                    break;
            case 17: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_nSelectedGroups = val;
                    break;
            case 18: fOK = data[i].ToLong( &val, 10 );
                    if( fOK ) m_nMaxResults = val;
                    break;

            // Add future parameters here
            default: break;
        }
    }
}

ContactSearch & ContactSearch::operator=( const ContactSearch & rhs )
{
    // Ref data
    m_strSearchName = rhs.m_strSearchName;

    // Core data
    m_fFirstName = rhs.m_fFirstName;
    m_strFirstName = rhs.m_strFirstName;
    m_fLastName = rhs.m_fLastName;
    m_strLastName = rhs.m_strLastName;
    m_fEmailAddresses = rhs.m_fEmailAddresses;
    m_strEmailAddress = rhs.m_strEmailAddress;
    m_fPhoneNumbers = rhs.m_fPhoneNumbers;
    m_strPhoneNumbers = rhs.m_strPhoneNumbers;
    m_fMore1 = rhs.m_fMore1;
    m_nField1 = rhs.m_nField1;
    m_strField1Text = rhs.m_strField1Text;
    m_fMore2 = rhs.m_fMore2;
    m_nField2 = rhs.m_nField2;
    m_strField2Text = rhs.m_strField2Text;
    m_fMore3 = rhs.m_fMore3;
    m_nField3 = rhs.m_nField3;
    m_strField3Text = rhs.m_strField3Text;
    m_nSelectedGroups = rhs.m_nSelectedGroups;
    m_nMaxResults = rhs.m_nMaxResults;

    return *this;
}

wxString ContactSearch::FormatDescription() const
{
    wxString str(wxT(""));
    wxString sep(wxT(","));

    if( m_fFirstName )
    {
        if( str.length() ) str += sep;
        str += _("First:");
        str += m_strFirstName;
    }

    if( m_fLastName )
    {
        if( str.length() ) str += sep;
        str += _("Last:");
        str += m_strLastName;
    }

    if( m_fEmailAddresses )
    {
        if( str.length() ) str += sep;
        str += _("Emails:");
        str += m_strEmailAddress;
    }

    if( m_fPhoneNumbers )
    {
        if( str.length() ) str += sep;
        str += _("Phones:");
        str += m_strPhoneNumbers;
    }

    if( m_fMore1 )
    {
        if( str.length() ) str += sep;
        str += m_displayField[m_nField1];
        str += wxT(":");
        str += m_strField1Text;
    }

    if( m_fMore2 )
    {
        if( str.length() ) str += sep;
        str += m_displayField[m_nField2];
        str += wxT(":");
        str += m_strField1Text;
    }

    if( m_fMore3 )
    {
        if( str.length() ) str += sep;
        str += m_displayField[m_nField3];
        str += wxT(":");
        str += m_strField1Text;
    }

    if( m_nSelectedGroups == Groups_None )
    {
        if( str.length() ) str += sep;
        str += _("Groups:None");
    }else if( m_nSelectedGroups == Groups_All )
    {
        if( str.length() ) str += sep;
        str += _("Groups:All");
    }else if( m_nSelectedGroups & Groups_Global )
    {
        if( str.length() ) str += sep;
        str += _("Groups:Global");
    }else if( m_nSelectedGroups & Groups_Personal )
    {
        if( str.length() ) str += sep;
        str += _("Groups:Personal");
    }

    return str;
}

void ContactSearch::PopulateDatabaseFieldCombo( wxComboBox *pCombo )
{
    pCombo->Clear();
    for( size_t ndx = 0; ndx < m_displayField.GetCount(); ++ndx )
    {
        pCombo->Append( m_displayField[ndx] );
    }
}

wxString ContactSearch::DatabaseDisplayField( int ndx )
{
    wxString ret(wxT(""));

    if( (ndx >= 0) && (ndx < (int)m_displayField.GetCount()) )
    {
        ret = m_displayField[ndx];
    }
    return ret;
}

wxString ContactSearch::DatabaseField( int ndx )
{
    wxString ret(wxT(""));

    if( (ndx >= 0) && (ndx < (int)m_dbField.GetCount()) )
    {
        ret = m_dbField[ndx];
    }
    return ret;
}

