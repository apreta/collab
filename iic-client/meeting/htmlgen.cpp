/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include "htmlgen.h"


HtmlGen::HtmlGen()
{
	clear();
	m_strName = wxT("Untitled.html");
}

void HtmlGen::clear( bool fWithHeader )
{
    if( fWithHeader )
    {
        m_strBuf = wxT("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n");
    }else
    {
        m_strBuf = wxT("");
    }
}

void HtmlGen::anchor( const wxString & anchorName, const wxString & linkText )
{
	m_strBuf += wxString::Format(wxT("<a name=\"%s\">%s</a>"),anchorName.c_str(), linkText.c_str());
}

void HtmlGen::linkExternal( const wxString & href, const wxString & linkText )
{
	m_strBuf += wxString::Format(wxT("<a href=\"%s\">%s</a>"),href.c_str(),linkText.c_str());
}

void HtmlGen::linkInternal( const wxString & anchorName, const wxString & linkText )
{
	m_strBuf += wxString::Format(wxT("<a href=\"%s#%s\">%s</a>"),m_strName.c_str(),anchorName.c_str(),linkText.c_str());
}

void HtmlGen::image( const wxString & imageName )
{
    m_strBuf += wxString::Format(wxT("<img src=\"%s\">"), imageName.c_str());
}

void HtmlGen::startTable( wxColour &bg, const wxString & width, int border, int cellspacing, int cellpadding )
{
	wxString color;
    color = wxString::Format( wxT("#%02X%02X%02X"), bg.Red(), bg.Green(), bg.Blue() );

	wxString strBg = wxString::Format(wxT("bg=\"%s\""),color.c_str());

	wxString strWidth = wxString::Format(wxT("width=\"%s\""),width.c_str());

	wxString strBorder = wxString::Format(wxT("border=\"%d\""),border);

	wxString strCellspacing = wxString::Format(wxT("cellspacing=\"%d\""),cellspacing);

	wxString strCellpadding = wxString::Format(wxT("cellpadding=\"%d\""),cellpadding);

	m_strBuf += wxString::Format(wxT("<table %s %s %s %s %s>"),
		strBg.c_str(),
		strWidth.c_str(),
		strBorder.c_str(),
		strCellspacing.c_str(),
		strCellpadding.c_str());
}

void HtmlGen::startTableRow( wxColour & bg )
{
	wxString color;
    color = wxString::Format( wxT("#%02X%02X%02X"), bg.Red(), bg.Green(), bg.Blue() );

	wxString strBg;
    strBg = wxString::Format( wxT("bg=\"%1\""), color.c_str() );

	m_strBuf += wxString::Format( wxT("<tr %s>\n"), strBg.c_str() );
}
