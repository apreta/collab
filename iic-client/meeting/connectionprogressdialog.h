/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef CONNECTIONPROGRESSDIALOG_H
#define CONNECTIONPROGRESSDIALOG_H

#include "meetingcentercontroller.h"

//(*Headers(ConnectionProgressDialog)
#include <wx/stattext.h>
#include <wx/textctrl.h>
#include <wx/button.h>
#include <wx/dialog.h>
#include <wx/gauge.h>
//*)

class ConnectionProgressDialog: public wxDialog
{
	public:

		ConnectionProgressDialog(wxWindow* parent,wxWindowID id = -1);
		virtual ~ConnectionProgressDialog();

		//(*Identifiers(ConnectionProgressDialog)
		//*)

        void SetConnected()
        {
            wxString    strT( _("Connected"), wxConvUTF8 );
            AddString (strT);
            m_ConnectGauge->SetValue(1);
        }

        void SetAuthenticated()
        {
            wxString    strT( _("Authenticated"), wxConvUTF8 );
            AddString (strT);
            m_ConnectGauge->SetValue(2);
        }

        void SetRegistered()
        {
            wxString    strT( _("Registered"), wxConvUTF8 );
            AddString (strT);
            m_ConnectGauge->SetValue(3);
        }

        void SetError( const wxString & str )
        {
            m_MsgEdit->SetValue( str );
        }
        
        void AddString( const wxString & str )
        {
            m_MsgEdit->SetValue( m_MsgEdit->GetValue( ) + wxT("\n") + str );
        }

        /** Terminate the dialog */
        void Terminate()
        {
            Show(false);
            Close();
        }
        
        void SetComplete(const wxString & str);

	protected:
		//(*Handlers(ConnectionProgressDialog)
		void OnInit(wxInitDialogEvent& event);
		//*)

		//(*Declarations(ConnectionProgressDialog)
		wxStaticText* m_InfoText;
		wxTextCtrl* m_MsgEdit;
		wxButton* m_CloseDlgBtn;
		wxGauge* m_ConnectGauge;
		//*)

        wxWindow *m_parent;
	private:

		DECLARE_EVENT_TABLE()
};

#endif
