#ifndef CERTHANDLER_H
#define CERTHANDLER_H

#include <wx/arrstr.h>

class CertHandler
{
    public:
        CertHandler();
        ~CertHandler();

        static CertHandler& GetHandler()  { return s_handler; }

        void Initialize(const wxString &caFile);

        void AddCertFile(const wxString &filename);

        void AddCertToFile(const wxString &buffer, int fileIdx);

        void AddTempCert(const wxString &buffer);

        bool WriteCerts();

        const wxString& GetCAFile() { return m_caFile; }

    protected:
    private:
        static CertHandler s_handler;
        wxString m_caFile;
        wxArrayString m_files;
        wxArrayString m_temps;
};

#endif // CERTHANDLER_H
