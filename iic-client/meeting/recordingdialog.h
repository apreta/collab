/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#ifndef RECORDINGDIALOG_H
#define RECORDINGDIALOG_H

//(*Headers(RecordingDialog)
#include <wx/stattext.h>
#include <wx/treelistctrl.h>
#include <wx/button.h>
#include <wx/dialog.h>
//*)

#include <wx/imaglist.h>



//*****************************************************************************
class RecordingTreeItemData : public wxTreeItemData
{
    public:
        RecordingTreeItemData( wxString strIn ) : m_str(strIn) { }
        wxString        m_str;
};


//*****************************************************************************
class RecordingDialog: public wxDialog
{
	public:

		RecordingDialog( wxWindow* parent, wxString strMtgID );
		virtual ~RecordingDialog();

		//(*Identifiers(RecordingDialog)
		//*)

	protected:

		//(*Handlers(RecordingDialog)
		void OnBtnViewClick(wxCommandEvent& event);
		void OnBtnDeleteClick(wxCommandEvent& event);
		void OnBtnRefreshClick(wxCommandEvent& event);
		void OnBtnCancelClick(wxCommandEvent& event);
		//*)

        void OnRecordingListItemActivated(wxTreeEvent& event);
        void OnRecordingListItemCollapsed(wxTreeEvent& event);
        void OnRecordingListItemExpanded(wxTreeEvent& event);
        void OnRecordingListItemRightClick(wxTreeEvent& event);
        void OnRecordingListSelectionChanged(wxTreeEvent& event);
        void OnRecordingListKeyDown(wxTreeEvent& event);
        void OnRecordingListItemMenu(wxTreeEvent& event);
        void OnRecordingListColClick( wxListEvent& event );

        void OnRecordingsFetched( wxCommandEvent& event );

		//(*Declarations(RecordingDialog)
		wxButton* m_btnView;
		wxButton* m_btnRefresh;
		wxButton* m_btnDelete;
		wxTreeListCtrl* m_tree;
		wxButton* m_btnCancel;
		wxStaticText* m_textRecsFor;
		//*)

    private:
        void            ParseFolderName( const std::string &aFolder, wxString &aDate );
        void            FreeANode( wxTreeItemId aItem );
        void            Cleanup( );
        void            AddRecToTree( recording_info_t recInfo );

    private:
        wxImageList     m_imageList;
        wxString        m_strMtgID;

		DECLARE_EVENT_TABLE()
};

#endif
