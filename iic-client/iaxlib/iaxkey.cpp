/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
#include <stdio.h>
#include <gnutls/gnutls.h>
#include <gnutls/x509.h>
#include <gcrypt.h>
#include "iaxkey.h"

#define MAX_AUDIO_PACKET 512

const char * g_cert_file = "cert.pem";

extern "C"
{

/* Need access to certain internal GNUTLS APIs and structures
   May need to be changed for later revs. of the library */

typedef unsigned int uint;
int _gnutls_x509_crt_get_mpis(gnutls_x509_crt cert,
			      gcry_mpi_t * params, int *params_size);

int _gnutls_pkcs1_rsa_encrypt(gnutls_datum * ciphertext,
			      const gnutls_datum * plaintext,
			      gcry_mpi_t * params, uint params_len, uint btype);

int _gnutls_pkcs1_rsadecrypt(gnutls_datum * plaintext,
			      const gnutls_datum ciphertext, /* gnutls_datum * in later rev. */
			      gcry_mpi_t * params, uint params_len, uint btype);

#define MAX_PRIV_PARAMS_SIZE 6
typedef struct gnutls_x509_privkey_int {
    gcry_mpi_t params[MAX_PRIV_PARAMS_SIZE];	/* the size of params depends on the public
					 * key algorithm
					 */
    /*
     * RSA: [0] is modulus
     *      [1] is public exponent
     *      [2] is private exponent
     *      [3] is prime1 (p)
     *      [4] is prime2 (q)
     *      [5] is coefficient (u == inverse of p mod q)
     *          note that other packages used inverse of q mod p,
     *          so we need to perform conversions.
     * DSA: [0] is p
     *      [1] is q
     *      [2] is g
     *      [3] is y (public key)
     *      [4] is x (private key)
     */
    int params_size;		/* holds the number of params */

    gnutls_pk_algorithm pk_algorithm;

    int crippled;		/* The crippled keys will not use the ASN1_TYPE key.
				 * The encoding will only be performed at the export
				 * phase, to optimize copying etc. Cannot be used with
				 * the exported API (used internally only).
				 */
//    ASN1_TYPE key;
} gnutls_x509_privkey_int;

}

void set_cert_file(const char *file)
{
	if (strcmp(file, g_cert_file))
		g_cert_file = strdup(file);
}

int read_public_key(gcry_mpi_t * params, int * params_size)
{
    int ret;
    gnutls_x509_crt cert;
    gnutls_datum cert_data;
    unsigned char buffer[1024];
    int len;

    FILE *inp = fopen(g_cert_file, "rb");
	if (inp == NULL)
		return -1;
    len = fread(buffer, 1, sizeof(buffer), inp);

    fclose(inp);

    cert_data.data = buffer;
    cert_data.size = len;

    ret = gnutls_x509_crt_init(&cert);
    if (ret < 0) {
	return ret;
    }

    ret = gnutls_x509_crt_import(cert, &cert_data, GNUTLS_X509_FMT_PEM);
    if (ret < 0) {
        gnutls_x509_crt_deinit(cert);
        return ret;
    }


    ret = _gnutls_x509_crt_get_mpis(cert, params, params_size);
    if (ret < 0) {
    	return ret;
    }

//    gnutls_x509_crt_deinit(cert);

    fprintf(stderr, "Read certificate\n");
    return ret;
}

int generate_key(unsigned char *session_key, int keylen, unsigned char *out, int *outlen)
{
    gcry_mpi_t pkey[2];
    int pkey_size;
    gnutls_datum plaintext, ciphertext;
    int ret = 0;

	if ((ret = gnutls_global_init()) < 0) {
        fprintf(stderr, "global_init: %s\n", gnutls_strerror(ret));
        return ret;
    }

    // Strong random is appropriate for a session key
    gcry_randomize(session_key, keylen, GCRY_STRONG_RANDOM);

	pkey_size = 2;
    ret = read_public_key(pkey, &pkey_size);
    if (ret)
    {
        fprintf(stderr, "Failed read certificate: %s\n", gnutls_strerror(ret));
        return ret;
    }

    plaintext.data = session_key;
    plaintext.size = keylen;

    ret = _gnutls_pkcs1_rsa_encrypt(&ciphertext, &plaintext, pkey, pkey_size, 2);
    if (ret)
    {
        fprintf(stderr, "Failed to encrypt id: %s\n", gnutls_strerror(ret));
        return ret;
    }

    memcpy(out, ciphertext.data, ciphertext.size);
    *outlen = ciphertext.size;

    return ret;
}

#if 0 // Test code

int read_private_key(gcry_mpi_t ** params, int * params_size)
{
    gnutls_x509_privkey priv_key;
    gnutls_datum cert_data;
    unsigned char buffer[1024];
    int len;
    int ret;

    FILE *inp = fopen("privkey.pem", "rb");
    len = fread(buffer, 1, sizeof(buffer), inp);

    fclose(inp);

    cert_data.data = buffer;
    cert_data.size = len;

    gnutls_x509_privkey_init(&priv_key);

    ret = gnutls_x509_privkey_import(priv_key, &cert_data, GNUTLS_X509_FMT_PEM);
    if (ret < 0)
    {
        fprintf(stderr, "Failed to load key\n");
        return ret;
    }

    *params = priv_key->params;
    *params_size = priv_key->params_size;

    //gnutls_x509_privkey_deinit(priv_key);

    return 0;
}

int decode_key(unsigned char *in, int inlen, unsigned char *out, int *outlen)
{
    gcry_mpi_t *pkey;
    int pkey_size;
    gnutls_datum plaintext, ciphertext;
    int ret = 0;

    ret = read_private_key(&pkey, &pkey_size);
    if (ret)
    {
        fprintf(stderr, "Failed read key: %s\n", gnutls_strerror(ret));
        return ret;
    }

    fprintf(stderr, "Decoding %d bytes\n", inlen);

    ciphertext.data = in;
    ciphertext.size = inlen;

    ret = _gnutls_pkcs1_rsa_decrypt(&plaintext, ciphertext, pkey, pkey_size, 2);
    if (ret)
    {
        fprintf(stderr, "Failed to decrypt id: %s\n", gnutls_strerror(ret));
        return ret;
    }

    memcpy(out, plaintext.data, plaintext.size);
    *outlen = plaintext.size;

    fprintf(stderr, "Decoded value\n");

    return 0;
}
#endif
