/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
// IICIAX.cpp : Defines the entry point for the DLL application.
//
#include <stdlib.h>
#include <stdio.h>
#include "iiciax.h"
#include <string>
#include <time.h>

#ifdef WIN32
#include <process.h>
#endif

#ifdef VOIP
#include <gcrypt.h>

#include "iaxclient.h"

#include <apr.h>
#include <apr_pools.h>
#include <apr_thread_proc.h>

#endif

#include "netlib/net.h"
#include "cryptlib/cryptlib.h"
#include "iaxkey.h"

#define INIT_DIGIT_PAUSE 100
#define INTER_DIGIT_PAUSE 100
#define MAX_MSG_LEN 512
#define MAX_AUDIO_PACKET 512

#define IAX_PORTNUM 4569
#define TCP_PORTNUM 443
#define UDP_CONNECT_TIMEOUT 10
#define TCP_CONNECT_TIMEOUT 15
#define TCP_READ_TIMEOUT 5

#if defined(LINUX) || defined(MACOSX)
#define __stdcall
#endif


void default_logger(int level, const char * msg)
{

}

VoipCallback on_ringing = NULL;
VoipCallback on_established = NULL;
VoipCallback on_cleared = NULL;
VoipLogger logger = default_logger;
VoipLevelCallback on_levels = NULL;

static bool initialized = false;
static bool in_progress = false;
static bool connected = false;
static bool hangup = false;

#ifdef VOIP
#if defined(LINUX) || defined(MACOSX)
  #ifndef _MAX_PATH
    #define _MAX_PATH 255
  #endif
#endif 

static int preferred_codec = IAXC_FORMAT_GSM;
static int default_codecs = IAXC_FORMAT_ULAW | IAXC_FORMAT_ALAW | IAXC_FORMAT_GSM | IAXC_FORMAT_G726
#ifdef SUPPORT_AMR
            | IAXC_FORMAT_AMR_NB
#endif
#ifdef SUPPORT_G729
            | IAXC_FORMAT_G729A
#endif
            ;

static char log_name[_MAX_PATH] = "";
#ifdef EC_RECORD
static char ec_log_name[_MAX_PATH] = "";
#endif

static int low_codec = IAXC_FORMAT_ILBC;
static int med_codec = IAXC_FORMAT_G726;
static int hi_codec = IAXC_FORMAT_ULAW;
static int max_jitter = 2000;
static double silence_threshold = 1;	// use speex VAD
static double suppression_threshold = 5.0;
static SOCKET call_fd = INVALID_SOCKET;
static enum { CallUDP, CallTCP } call_mode = CallUDP;
static struct sockaddr_in call_addr;
static time_t call_start;
static bool call_switch_mode = false;
static std::string call_to;

static FILE * err_stream = NULL;

static bool key_sent = false;
static enum { EncryptNone, EncryptBlowfish } encryption = EncryptNone;
static CBlowFish blowfish_encoder;
static unsigned char g_encrypt_key[ENCRYPTED_KEY_LEN];
static int g_key_len;

// Network connection thread stuff
void ConnectToNetwork();

static bool apr_is_initialized = false;
static apr_pool_t *connect_network_pool = 0;
static apr_thread_t *connect_network_thread = 0;
static void *connect_network_worker(apr_thread_t *thread, void *udata);
int tcp_connect();
int udp_connect();

#endif // VOIP

#ifdef WIN32

BOOL APIENTRY DllMain( HANDLE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
    return TRUE;
}

#endif

#if defined(LINUX) || defined(MACOSX)

static int nWSALastError = 0;
void WSASetLastError(int nError)
{
    nWSALastError = nError;
}

int WSAGetLastError()
{
    return nWSALastError;
}

#endif

void VoipSetOnRinging(VoipCallback h)
{
	on_ringing = h;
}

void VoipSetOnEstablished(VoipCallback h)
{
	on_established = h;
}

void VoipSetOnCleared(VoipCallback h)
{
	on_cleared = h;
}

void VoipSetLogger(VoipLogger h)
{
	logger = h;
}

void VoipSetLevelCallback(VoipLevelCallback h)
{
	on_levels = h;
}

#ifdef VOIP

void close_network()
{
	if (call_fd != INVALID_SOCKET)
	{
		switch (call_mode) {
		case CallUDP:
			_close(call_fd);
			break;
		case CallTCP:
			net_close(call_fd, TRUE);
			break;
		}
		call_fd = INVALID_SOCKET;
	}
}

void ConnectToNetwork()
{
    if( !apr_is_initialized )
    {
        apr_is_initialized = true;
        apr_status_t s = apr_initialize();
        if( s != APR_SUCCESS )
        {
            char szError[256];
            char szMsg[512];
            sprintf(szMsg,"ConnectToNetwork - apr_initialize returned %s",apr_strerror( s, szError, sizeof(szError) ) );
            logger( IICVOIP_DEBUG, szMsg );
        }else
        {
            logger( IICVOIP_DEBUG, "ConnectToNetwork - apr initialize OK." );
        }
    }
    apr_pool_create(&connect_network_pool, NULL);
    apr_thread_create(&connect_network_thread, NULL, (apr_thread_start_t)connect_network_worker, NULL, connect_network_pool);
    apr_thread_detach(connect_network_thread);

}

void * connect_network_worker(apr_thread_t *thread, void *udata)
{
    logger( IICVOIP_DEBUG, "VOIP connect_network thread started" );

	switch (call_mode)
	{
	case CallUDP:
		udp_connect();
		break;
	case CallTCP:
		tcp_connect();
		break;
	}

#ifdef EC_RECORD
	iaxc_set_ec_logging(ec_log_name);
#endif

	iaxc_call(const_cast<char*>(call_to.c_str()));

    logger( IICVOIP_DEBUG, "VOIP connect_network thread ended" );

    apr_thread_exit(thread, 0);

    apr_pool_destroy(connect_network_pool);

	return NULL;
}

int handle_state_event(struct iaxc_ev_call_state c)
{
    // first, handle inactive calls
    if (!(c.state & IAXC_CALL_STATE_ACTIVE)) {
		// Poor man's graceful shutdown: give last message a chance to be sent.
		iaxc_millisleep( 500 );

		key_sent = false;

		if (call_switch_mode)
		{
			call_switch_mode = false;
			close_network();

			// Attempt TCP connection (in separate thread so IAX library lock is not held)
			call_mode = CallTCP;

			// Thread
			ConnectToNetwork();
		}
		else
		{
			in_progress = false;
			OnCleared(0);
			close_network();
		}
    }
	else
	{
		// Figure out state of call
		BOOL outgoing = c.state & IAXC_CALL_STATE_OUTGOING;
		BOOL ringing = c.state & IAXC_CALL_STATE_RINGING;
		BOOL complete = c.state & IAXC_CALL_STATE_COMPLETE;
		BOOL rejected = c.state & 0; //IAXC_CALL_STATE_REJECTED;

		if(outgoing)
		{
			if (ringing)
				OnRinging(0);
			else if (complete)
			{
				char msg[128];
				sprintf(msg, "Call established (jitter=%d,silence=%f)", max_jitter, silence_threshold);
				logger(IICVOIP_DEBUG, msg);

				connected = true;

				OnEstablished(0);
			}
		}
		else
		{
			; // No inbound calls expected yet.
		}
	}
    return 0;
}

int iaxc_callback(iaxc_event e)
{
	int ret = 0;

    switch(e.type) {
        case IAXC_EVENT_LEVELS:
			if (on_levels)
				on_levels(e.ev.levels.input, e.ev.levels.output);
			break;
        case IAXC_EVENT_TEXT:
			logger(IICVOIP_DEBUG, e.ev.text.message);
			break;
		case IAXC_EVENT_STATE:
            ret = handle_state_event(e.ev.call);
			break;
    }
	return ret;
}

static int read_n_bytes(SOCKET fd, char *buff, unsigned size, int timeout)
{
	int ready, read;
	unsigned i;

	for(i=0; i<size;)
	{
		ready = net_checkreadable(fd, timeout);
		if (ready == 0) {
			WSASetLastError(_EWOULDBLOCK);
			return -1;
		}
		if(ready != 1)
			return -1;

		read = net_recv(fd, &buff[i], size-i);
		if (read <= 0)
			return -1;

		i += read;
	}

	return (int)size;
}

static int send_n_bytes(SOCKET fd, char *buff, unsigned size)
{
	int ready, sent;
	unsigned i;

	for(i=0; i<size;)
	{
		ready = net_checkwriteable(fd, TCP_READ_TIMEOUT);
		if(ready!=1)
			return -1;

		sent = net_send(fd, &buff[i], size-i);
		if (sent <= 0)
			return -1;

		i += sent;
	}

	return (int)size;
}

int udp_connect()
{
	SOCKET fd;

	logger(IICVOIP_DEBUG, "Connecting to voice bridge via UDP");

	call_start = time(NULL);

	fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
	if (fd < 0) {
		logger(IICVOIP_ERROR, "Unable to create UDP socket");
		return -1;
	}

/*  Don't bind; let OS pick random port
	struct sockaddr_in sin;
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = 0;
	sin.sin_port = htons((short)IAX_PORTNUM);
	bind(fd, (struct sockaddr *) &sin, sizeof(sin));
*/

    // Need to set non-blocking or it will hang
	_set_nonblock(fd, TRUE);

	call_fd = fd;
	return 0;
}

int tcp_connect()
{
	int ret = -1;
	bool use_proxy = false;
	SOCKET fd;

	logger(IICVOIP_DEBUG, "Connecting to voice bridge via TCP");

	call_start = time(NULL);

	net_set_server(DETECT_TYPE, call_to.c_str());

	while (!hangup)
	{
		fd = net_socket();
		if (fd < 0) {
			logger(IICVOIP_ERROR, "Unable to create TCP socket");
			return -1;
		}

		call_addr.sin_port = htons(TCP_PORTNUM);

		ret = net_connect(fd, call_to.c_str(), (struct sockaddr *)&call_addr, sizeof(call_addr), !use_proxy);
		if (!ret)
		{
            call_fd = fd;
            
			net_set_nonblock(fd, TRUE);
			net_set_nagle_algorithm(fd, FALSE);

			// Write header
			ret = send_n_bytes(fd, "voipgw", 6);

			// and then read header
			char buffer[8];
			if (ret > 0)
				ret = read_n_bytes(fd, buffer, 6, 10);

            if (ret > 0)
                ret = 0;
			break;
		}
		if (ret)
		{
			close_network();

			// Retry after detecting for proxy server
			if (use_proxy)
				break;
			else
				use_proxy = true;
		}
	}

	logger(IICVOIP_DEBUG, ret == 0 ? "TCP connection succeeded" : "TCP connection failed");

	return ret;
}

#ifdef _WIN32
#define CALLBACK_BUF char *
#define SIZE_T int
#define SOCKLEN_T int
#endif

#if defined(LINUX) || defined(MACOSX)
#define CALLBACK_BUF void *
#define SIZE_T size_t
#define SOCKLEN_T socklen_t
#endif

int __stdcall iaxc_sendto_callback(SOCKET, iaxc_send_buf buf, SIZE_T len, int flags, const struct sockaddr *to_addr, SOCKLEN_T to_len)
{
	int ret = -1;

	if (call_fd == -1)
	{
		WSASetLastError(_EWOULDBLOCK);
		return -1;
	}

    //struct sockaddr_in *sin = (struct sockaddr_in *)to_addr;

	switch (call_mode)
	{
	case CallUDP:
		ret = sendto(call_fd, buf, len, flags, to_addr, to_len);
		break;

	case CallTCP:
		char buffer[MAX_MSG_LEN + sizeof(WORD)];

		if (len > MAX_MSG_LEN)
		{
			logger(IICVOIP_ERROR, "Attempted to send oversized message");
			close_network();
			return -1;
		}

		// Send data length word, then data buffer
		*((WORD *)buffer) = htons(len);
		memcpy(buffer + sizeof(WORD), buf, len);

		ret = send_n_bytes(call_fd, buffer, len + sizeof(WORD));
		if (ret < 0)
			close_network();
		break;
	}

	return ret;
}

int __stdcall iaxc_recvfrom_callback(SOCKET, iaxc_recv_buf buf, SIZE_T len, int flags, struct sockaddr *from_addr, SOCKLEN_T *from_len)
{
	int ret = -1;

	if (call_fd == -1)
	{
		WSASetLastError(_EWOULDBLOCK);
		return -1;
	}

	switch (call_mode)
	{
	case CallUDP:
		ret = recvfrom(call_fd, buf, len, flags, from_addr, from_len);
		if (ret < 0 && !connected && time(NULL) - call_start > UDP_CONNECT_TIMEOUT)
		{
			call_switch_mode = true;
			// Not a good idea to dump the call inside receive callback, may lead to crash
			//iaxc_dump_call();
		}
		break;

	case CallTCP:
		WORD msg_len;

		// copy original to address
		memcpy(from_addr, &call_addr, sizeof(call_addr));
		*from_len = sizeof(call_addr);
		((struct sockaddr_in *)from_addr)->sin_port = htons((short)IAX_PORTNUM);

		// Read length word
		ret = read_n_bytes(call_fd, (char *)&msg_len, sizeof(msg_len), 0);
		if (ret > 0)
		{
			msg_len = ntohs(msg_len);
			if (msg_len > len)
			{
				logger(IICVOIP_ERROR, "Incoming message larger then buffer");
				close_network();
				return -1;
			}

			// Read rest of message with timeout; should be immediately following
            // (if this fails we probably won't recover as the framing will be off)
			ret = read_n_bytes(call_fd, (char *)buf, msg_len, 15);
		}
		if (ret < 0 && WSAGetLastError() != _EWOULDBLOCK)
			close_network();
		break;
	}

	return ret;
}


int iaxc_filter_in(void * /*session*/, unsigned char *in, int inlen, unsigned char *out, int *outlen)
{
	/* decrypt data */

	DWORD len = inlen - sizeof(DWORD);

	*outlen = *((DWORD *)in); // read actual length from packet

	if (!key_sent || len > MAX_AUDIO_PACKET || *outlen > (int)len || *outlen < 0)
	{
		*outlen = 0;
		return 1;
	}

	blowfish_encoder.Decode(in + sizeof(DWORD), out, len);

	return 0;
}

int iaxc_filter_out(void * /*session*/, unsigned char *in, int inlen, unsigned char *out, int *outlen)
{
	/* encrypt data */

	*outlen = 0;
	if (!key_sent)
	{
		key_sent = true;

		// send key at start of session
		memcpy(out, g_encrypt_key, g_key_len);
		*outlen += g_key_len;

		// key is stored in buffer, set pointer so encrypted data follows
		out += *outlen;
	}

	DWORD len = inlen + (BLOWFISH_BLOCKSIZE - inlen % BLOWFISH_BLOCKSIZE);

	*((DWORD*)out) = inlen;	// track actual data length in packet

	blowfish_encoder.Encode(in, out + sizeof(DWORD), len);
	*outlen += len + sizeof(DWORD);

	return 0;
}

#endif

int VoipInitialize(const char * dir)
{
    int rtn;

#ifdef VOIP
	if (!initialized)
	{
		static FILE * err_stream = NULL;

        if( dir )
        {
            strcpy(log_name, dir);
            strcat(log_name, "/voip.log");
#ifdef EC_RECORD
            strcpy(ec_log_name, dir);
            strcat(ec_log_name, "/audio");
#endif
        }

		iaxc_set_networking(iaxc_sendto_callback, iaxc_recvfrom_callback);

		if (IICVOIP_OK != (rtn = iaxc_initialize( 3 )))
        {
            return rtn;
        }

        iaxc_set_formats(preferred_codec, default_codecs );

		iaxc_set_event_callback(iaxc_callback);

		iaxc_start_processing_thread();

		initialized = true;
	}

#endif
	return IICVOIP_OK;
}

int VoipShutdown()
{
#ifdef VOIP
	if (initialized)
	{
		iaxc_stop_processing_thread();
		iaxc_shutdown();
		initialized = false;
	}
#endif
	return IICVOIP_OK;
}

int VoipSetOption(const char * option, const char * value)
{
    // Must be set before initialization
	if (strcmp(option, "audiosubsystem") == 0)
	{
	    return IICVOIP_OK;
	}

	if (!initialized)
		return IICVOIP_NOTINIT;

#ifdef VOIP
	if (strcmp(option, "bandwidth") == 0)
	{
	    if (strcmp(value, "auto") == 0)
	    {
            iaxc_set_formats(preferred_codec, default_codecs);
	    }
        else
        {
            int codec;
            if (strcmp(value, "low") == 0)
                codec = low_codec;
            else if (strcmp(value, "medium") == 0)
                codec = med_codec;
            else
                codec = hi_codec;

            iaxc_set_formats(codec, codec);
        }
	}
	else if (strcmp(option, "lowcodec") == 0)
	{
#ifdef SUPPORT_AMR
		if (strcmp(value, "amr-nb") == 0)
		{
			low_codec = IAXC_FORMAT_AMR_NB;
		}
		else
#endif
#ifdef SUPPORT_G729
		if (strcmp(value, "g729") == 0)
		{
			low_codec = IAXC_FORMAT_G729A;
		}
		else
#endif
        if (strcmp(value, "gsm") == 0)
        {
            low_codec = IAXC_FORMAT_GSM;
        }
        else
		{
			low_codec = IAXC_FORMAT_ILBC;
		}
	}
	else if (strcmp(option, "mediumcodec") == 0)
	{
#ifdef SUPPORT_G729
		if (strcmp(value, "g729") == 0)
		{
			med_codec = IAXC_FORMAT_G729A;
		}
		else
#endif
		{
			med_codec = IAXC_FORMAT_G726;
		}
	}
	else if (strcmp(option, "maxjitter") == 0)
	{
		max_jitter = atoi(value);
		iaxc_set_max_jitter(max_jitter);
	}
	else if (strcmp(option, "speechprobability") == 0)
	{
		int prob;
		if (strcmp(value, "low") == 0)
			prob = 37;
		else if (strcmp(value, "medium") == 0)
			prob = 35;
		else
			prob = 33;

		iaxc_set_speech_probability(prob);
	}
	else if (strcmp(option, "silencethreshold") == 0)
	{
		silence_threshold = atof(value);
		iaxc_set_silence_threshold(silence_threshold);
	}
	else if (strcmp(option, "suppressionthreshold") == 0)
	{
		suppression_threshold = atof(value);
#if 0
		iaxc_set_suppression_threshold(suppression_threshold);
#endif
	}
	else if (strcmp(option, "echocanceller") == 0)
	{
		int filters = iaxc_get_filters();
		if (atoi(value))
			filters |= IAXC_FILTER_ECHO;
		else
			filters &= ~IAXC_FILTER_ECHO;
		iaxc_set_filters(filters);
	}
	else if (strcmp(option, "noisefilter") == 0)
	{
		int filters = iaxc_get_filters();
		if (atoi(value))
			filters |= IAXC_FILTER_DENOISE;
		else
			filters &= ~IAXC_FILTER_DENOISE;
		iaxc_set_filters(filters);
	}
	else if (strcmp(option, "netmode") == 0)
	{
		if (strcmp(value, "tcp") == 0)
			call_mode = CallTCP;
		else
			call_mode = CallUDP;
	}
	else if (strcmp(option, "encrypt") == 0)
	{
		if (strcmp(value, "blowfish") == 0)
			encryption = EncryptBlowfish;
		else
			encryption = EncryptNone;
	}
	else if (strcmp(option, "certfile") == 0)
	{
		set_cert_file(value);
	}

#endif
	return IICVOIP_OK;
}

int VoipMakeCall(const char * from_user, const char * to_address)
{
    logger( IICVOIP_DEBUG, "VoipMakeCall - Started" );

	if (!initialized)
		return IICVOIP_NOTINIT;
	if (in_progress)
		return IICVOIP_CALLACTIVE;

#ifdef VOIP
	// IAX library writes some debug output to stderr, capture this for this call.
	// Restart log for each call to minimize size (currently always written).
	err_stream = freopen(log_name, "w", stderr);

	if (encryption != EncryptNone)
	{
		char key[KEY_LEN+1];

		// Encryption is set up here in advance of the making call as generating the random key takes a moment.
		iaxc_set_filter(iaxc_filter_in, iaxc_filter_out);

		if (generate_key((unsigned char *)key, KEY_LEN, g_encrypt_key, &g_key_len))
		{
			logger(IICVOIP_ERROR, "Unable to create VOIP session key");
			return IICVOIP_FAIL;
		}

		key[KEY_LEN] = '\0';
		blowfish_encoder.SetKey(key);
		blowfish_encoder.Initialize(BLOWFISH_KEY, sizeof(BLOWFISH_KEY));
	}
	else
		iaxc_set_filter(NULL, NULL);

	if (strncmp(to_address, "iax2:", 5) == 0)
		to_address += 5;
	call_to = std::string(to_address) + "/" + from_user;

	if (!net_resolve_hostname(to_address, &call_addr))
		return IICVOIP_FAIL;

	in_progress = true;
	connected = false;
	hangup = false;

    logger( IICVOIP_DEBUG, "VoipMakeCall - Attempting connection" );

	// Connect in separate thread to avoid blocking main thread.
	// We may be in TCP mode already if we have connected before.
	ConnectToNetwork();

#endif

    logger( IICVOIP_DEBUG, "VoipMakeCall - Success - Returning OK." );

	return IICVOIP_OK;
}

int VoipHangup()
{
	if (!initialized)
		return IICVOIP_NOTINIT;

    logger( IICVOIP_DEBUG, "VoipHangup - Start" );
#ifdef VOIP
        hangup = true;
        iaxc_dump_call();
        fflush(err_stream);
#endif

    logger( IICVOIP_DEBUG, "VoipHangup - Done" );

	return IICVOIP_OK;
}

int VoipDial(const char * str)
{
	if (!initialized)
		return IICVOIP_NOTINIT;

#ifdef VOIP
	iaxc_millisleep(INIT_DIGIT_PAUSE);
	for (unsigned i=0; i<strlen(str); i++)
	{
		iaxc_send_dtmf(str[i]);
		iaxc_millisleep(INTER_DIGIT_PAUSE);
	}
#endif
	return IICVOIP_OK;
}

int VoipMute(int muted)
{
#ifdef VOIP
    iaxc_mute_microphone(muted);
#endif
    return 0;
}

int VoipSpeakerMute(int muted)
{
#ifdef VOIP
    iaxc_mute_speaker(muted);
#endif
    return 0;
}

bool VoipIsInCall()
{
#ifdef VOIP
	return in_progress;
#else
	return false;
#endif
}

void OnRinging(int status)
{
	if (on_ringing)
		on_ringing(status);
}

void OnEstablished(int status)
{
	if (on_established)
		on_established(status);
}

void OnCleared(int status)
{
	if (on_cleared)
		on_cleared(status);
}

