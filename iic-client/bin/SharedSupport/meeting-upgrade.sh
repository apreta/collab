#!/bin/sh
# Conferencing client upgrade script

download=$1
clientdir=$2

# mount dmg, parse out non-device messages from output
mounted=`hdiutil mount -puppetstrings "$download" | awk ' /\// { print }'`
imagedev=`echo $mounted | cut -d " " -f 1`
imagedir=`echo $mounted | cut -d " " -f 2-`
appname=`basename "$imagedir"/*.app`
if [ -z $imagedev ]; then 
	echo "Open diskimage failed"
	exit 1
fi

echo "Upgrading $appname at $clientdir"

# copy app to current install location
if cp -R "$imagedir/$appname" "$clientdir"; then
	echo "App upgraded"
	res=0
else
	echo "App upgrade failed"
	res=1
fi

# umount image
hdiutil detach "$imagedev"

if [ $res -eq 0 ]; then 
	sleep 4 && open "$clientdir/$appname" &
fi

exit $res
