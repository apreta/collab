/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef CONTOLLER_H_INCLUDED
#define CONTOLLER_H_INCLUDED


#define APP_SHARE_KEY_LEN   16

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef NO_DEFINE_APR
/* Some defs so callers do not need to include APR stuff */
typedef struct apr_pool_t apr_pool_t;
typedef struct apr_array_header_t apr_array_header_t;
typedef struct apr_thread_mutex_t apr_thread_mutex_t;
#endif

#include "session_api.h"
#include "schedule.h"


/*
   Meeting info structure is reference counted so application can keep it as long as needed.
   No sub-items are removed until meeting itself is deleted, but data fields may change.
   You can keep a pointer to a participant_t or submeeting_t as they won't be deleted, but
   if they are removed from the meeting, the removed flag will be set.

   All APIs that interact with the controller are asynchronous; meeting events will be sent
   once the action has been taken.
*/

struct meeting_st
{
    // Private
    apr_pool_t *pool;
    int         references;
    apr_array_header_t *submeetings;
    apr_array_header_t *participants;
    apr_array_header_t *chats;
    apr_thread_mutex_t *lock;

    // Public
    int         state;
    pstring     meeting_id;
    pstring     host_id;
    int         type;
    int         privacy;
    int         priority;               // Not used
    pstring     title;
    pstring     description;
    pstring     invite_message;
    pstring     password;
    int         size;
    int         duration;               // Not used
    int         time;                   // start time
    int         end_time;
    int         invite_expiration;
    int         invite_valid_before;    // Not used
    int         display_attendees;
    int         invitee_chat_options;
    int         recurrence;             // Not used
    int         options;
    int         host_enabled_features;
    int         meeting_instance_time;
    pstring     bridge_address;         // VoIP address
    pstring     bridge_phone;           // PSTN phone number

    pstring     participant_id;         // Current user's participant id
};
typedef struct meeting_st * meeting_t;


struct submeeting_st
{
    // Public
    pstring     meeting_id;
    pstring     sub_id;
    pstring     title;
    BOOL        is_main;

    BOOL        removed;
};
typedef struct submeeting_st * submeeting_t;


struct participant_st
{
    // Public
    pstring     part_id;
    pstring     sub_id;
    int 		roll;       // Still spelt wrong :-)
    pstring     user_id;

    pstring     name;
    pstring     description;   // Not used ??

    int         selphone;
    pstring     phone;

    int         selemail;
    pstring     email;

    pstring     screenname;
    int         status;
    int         notify_type;

    int         volume;
    int         gain;
    int         mute;

    BOOL        removed;
};
typedef struct participant_st * participant_t;


struct meeting_chat_st
{
    // public
    pstring     meeting_id;
    pstring     message;
    int         sent_to;
    pstring     from_participant_id;
    pstring     from_name;
    int         from_role;
    int         time;
};
typedef struct meeting_chat_st * meeting_chat_t;


struct meeting_event_st
{
    // Public
    int         event;
    pstring     meeting_id;
    pstring     sub_id;
    pstring     participant_id;
};
typedef struct meeting_event_st * meeting_event_t;


struct docshare_event_st
{
    // Public
    int         event;
    pstring     meeting_id;

    pstring     presenter_id;
    pstring     doc_id;
    pstring     page_id;
    pstring     drawing;
    int         options;
};
typedef struct docshare_event_st * docshare_event_t;


struct meeting_invite_st
{
    // Private
    apr_pool_t *pool;

    // public
	pstring description;
	pstring host_id;
	pstring meeting_id;
	pstring participant_id; // This user's participant id
	pstring participant_name;
	pstring title;
    int type;               // Meeting type (instant, schedule)
    int privacy;
    int time;
	int options;
	pstring from_name;
	pstring message;        // Invitation message
	int invite_type;        // normal, pc phone
	BOOL cancel;            // Invite has been cancelled
};
typedef struct meeting_invite_st * meeting_invite_t;


// Meeting error
// Error types are:  "join", "call", "im",
// "startmeeting", "joinmeeting", "leavemeeting", "presenter"
// "enablecontrol", "handup", "enablemoderator",
// "participantupdate", "participantmove",
// "addsubmeeting", "removesubmeeting",
// "modifymeeting", "modifyoptions"
// Server error codes are in common.h
struct meeting_error_st
{
    // Public
    int         error;  // server error code
    pstring     meeting_id;  // currently not specified
    pstring     participant_id;
    pstring     type;
    pstring     message;
};
typedef struct meeting_error_st * meeting_error_t;


struct appshare_info_st
{
    pstring     address;
    pstring     password;
    pstring     key;
    pstring     presenter;
    int         share_options;
};
typedef struct appshare_info_st * appshare_info_t;


typedef struct controller_st * controller_t;
typedef struct active_meeting_st * active_meeting_t;


// Create controller object.
controller_t controller_create(session_t sess);

// Destory controller object.
void controller_destroy(controller_t cnt);

// Handler for registration completed.
typedef void (*controller_registered_handler)(void *);

// Handler for finding meeting invite.
typedef void (*controller_invite_lookup_handler)(void *, meeting_invite_t invite);

// Handler for meeting state, participant state, submeeting state changes
typedef void (*controller_meeting_event_handler)(void *, meeting_event_t);

// Handler for meeting chat event
typedef void (*controller_meeting_chat_handler)(void *, meeting_chat_t);

// Handler for meeting errors
typedef void (*controller_meeting_error_handler)(void *, meeting_error_t);

// Handler for submeeting added
typedef void (*controller_submeeting_add_handler)(void *, const char *opid, const char *sub_id);

// Handler for meeting invitation
typedef void (*controller_invite_handler)(void *, meeting_invite_t invite);

// Handler for returned meeting parameters
typedef void (*controller_get_params_handler)(void *, meeting_details_t mtg, int formum_options);

// Handler for document share/whiteboard events
typedef void (*controller_docshare_event_handler)(void *, docshare_event_t);

// Handler for IM notification required events
typedef void (*controller_im_notify_handler)(void *, const char *meeting_id, participant_t);

typedef void (*controller_ping_handler)(void *);

// Handlers for other types of events
typedef void (*controller_ppt_share_handler)(void *);

// Set callback handlers
void controller_set_registered_handler(controller_t, void *user, controller_registered_handler _handler);
void controller_set_invite_lookup_handler(controller_t, void *user, controller_invite_handler);
void controller_set_meeting_event_handler(controller_t, void *user, controller_meeting_event_handler _handler);
void controller_set_meeting_error_handler(controller_t, void *user, controller_meeting_error_handler _handler);
void controller_set_meeting_chat_handler(controller_t, void *user, controller_meeting_chat_handler _handler);
void controller_set_submeeting_add_handler(controller_t, void *user, controller_submeeting_add_handler);
void controller_set_invite_handler(controller_t, void *user, controller_invite_handler);
void controller_set_get_params_handler(controller_t, void *user, controller_get_params_handler);
void controller_set_docshare_event_handler(controller_t, void *user, controller_docshare_event_handler _handler);
void controller_set_im_notify_handler(controller_t, void *user, controller_im_notify_handler _handler);
void controller_set_ping_handler(controller_t, void *user, controller_ping_handler _handler);

// Register with controller...informs controller that user is available for IMs
int controller_register(controller_t cnt, auth_info_t auth, BOOL retry);

// Register external (non-authenticated) user with controller
int controller_register_anonymous(controller_t cnt, const char *pin, const char *display_name);

// Lookup meeting invitation based on PIN.
int controller_lookup_invite(controller_t cnt, const char *pin);

// Check if telephony is available; return mask of TelephonyAvailable, VOIPAvailable
int controller_is_telephony_available(controller_t cnt);

// Send off a ping to the server
int controller_ping_start(controller_t cnt, const char *id);

// Start a meeting as scheduled
int controller_meeting_start(controller_t controller, const char *meeting_id);

// End a meeting
int controller_meeting_end(controller_t controller, const char *meeting_id);

// Join a meeting
// Participant id will be converted if not 'part:##'
int controller_meeting_join(controller_t controller, const char *meeting_id, const char *participant_id, const char *password);

// Leave a meeting
int controller_meeting_leave(controller_t controller, const char *meeting_id);

// Returns pointer to specified meeting; it is add ref'ed internally so must call meeting_remove_reference to free.
meeting_t controller_find_meeting(controller_t, const char *meeting_id);

// Send meeting chat
int controller_meeting_chat(controller_t cnt, const char *meeting_id, int send_to, const char *message, const char *from_part_id, const char *to_part_id);

// Get list of invitees and settings for this meeting
// N.B. the user ids and part ids are not in a format suitable for using with schedule APIs (we could change that if needed),
// but are usable by the controller APIs.
meeting_details_t controller_get_meeting_details(controller_t cnt, meeting_t info, int num_participants, participant_t participants[]);

// Start meeting based on the specified settings and participants
// Will convert user and participant ids if needed.
// If invitees have call_options flags set to indicate a 1-1 call, then server will create a submeeting and attach the participants to it.
int controller_meeting_create(controller_t cnt, meeting_details_t details);

// Update meeting based on meeting details
// Will convert user and participant ids if needed.
// If invitees have call_options flags set to indicate a 1-1 call, then server will create a submeeting and attach the participants to it.
int controller_meeting_modify(controller_t cnt, meeting_details_t details, BOOL update_participants);

// Modify meeting options using mask to control which flags will be set
int controller_meeting_modify_options(controller_t cnt, const char *meeting_id, int set_mask, int new_flags);

int controller_meeting_lock(controller_t cnt, const char *meeting_id, BOOL enable);
int controller_meeting_record(controller_t cnt, const char *meeting_id, BOOL enable);
int controller_meeting_lecture_mode(controller_t cnt, const char *meeting_id, BOOL enable);
int controller_meeting_hold_mode(controller_t cnt, const char *meeting_id, BOOL enable);
int controller_meeting_mute_joinleave_tones(controller_t cnt, const char *meeting_id, BOOL enable);

// Add a new submeeting...server id for meeting is returned in callback
int controller_submeeting_add(controller_t cnt, const char *meeting_id, const char *opid);

// Remove submeeting
int controller_submeeting_remove(controller_t cnt, const char *meeting_id, const char *sub_id);

// Switch presenter
int controller_change_presenter(controller_t cnt, const char *meeting_id, const char *part_id);

// Play recorded names of participants.
int controller_roll_call(controller_t cnt, const char *meeting_id);

// Log custom event for meeting
int controller_log_event(controller_t cnt, const char *meeting_id, const char *info);

int controller_participant_remote_control(controller_t cnt, const char *meeting_id, const char *part_id, BOOL enable);
int controller_participant_hand_up(controller_t cnt, const char *meeting_id, const char *part_id, BOOL enable);
int controller_participant_bridge_request(controller_t cnt, const char *meeting_id, const char *part_id, int request_code, const char *msg);
int controller_participant_moderator(controller_t cnt, const char *meeting_id, const char *part_id, BOOL enable);
int controller_participant_move(controller_t cnt, const char *meeting_id, const char *participant_id, const char *sub_id);
int controller_participant_remove(controller_t cnt, const char *meeting_id, const char *participant_id);
int controller_participant_disconnect(controller_t cnt, const char *meeting_id, const char *participant_id);

// Change participant volume.
// Volume and gain range from 0-7, mute is 0-1.
// Set any to -1 to leave current setting unchanged.
int controller_participant_volume(controller_t cnt, const char *meeting_id, const char *participant_id, int volume, int gain, int mute);

// Add or update particpant.
// Type codes are defined in common.h
// User and participant id will be converted if needed.
int controller_participant_update(controller_t cnt, const char * meeting_id,
            const char *sub_id,     /* NULL will default to main meeting */
            const char *part_id,    /* blank for new participant */
            const char *user_id,    /* blank for non-user, or value from server */
            const char *username,   /* leave blank ?? */
            int role,
            const char *name,       /* display name */
            int selphone, const char *phone,
            int selemail, const char *email,
            const char *screenname, /* set to screen name if IM notification is needed */
            int notify_type,
            int call_options);

// Start call to participant.
int controller_participant_call(controller_t cnt, const char *meeting_id, 
            const char *part_id, int selphone, const char *phone, int call_options);

// Get app share info for this meeting, NULL if none is available.
appshare_info_t controller_get_appshare_info(controller_t cnt, const char *meeting_id);

// Start app share session.
// Set SharePPT for PPT mode, SharePPTnoEraser for PPT version 11 or higher, ShareAutoDetect to detect slow connection.
// The same start time must also be sent to the VNC server process for presenter.
int controller_appshare_start(controller_t cnt, const char *meeting_id, int share_options, int start_time);

// End app share session.
int controller_appshare_end(controller_t cnt, const char *meeting_id);

// Get URL for document share server.
const char * controller_get_docshare_server(controller_t cnt, const char *meeting_id);

// Get username for anonymous screen name
const char * controller_get_username(controller_t cnt);

// Start doc share/whiteboard session
int controller_docshare_start(controller_t cnt, const char *meeting_id, int start_time);

// End doc share/whiteboard session
int controller_docshare_end(controller_t cnt, const char *meeting_id);

// Send drawing primitive to doc share session
int controller_docshare_draw(controller_t cnt, const char *meeting_id, const char *drawing);

// Direct doc share participants to download specified page
int controller_docshare_download(controller_t cnt, const char *meeting_id, const char *doc_id, const char *page_id);

// iic: protocol launch may use a token to indicate the meeting setup required...use this token
// to look up the real data.
int controller_get_meeting_params(controller_t cnt, const char *meeting_id, const char *token);

// Preview or reset current recording for indicated meeting
int controller_recording_modify(controller_t cnt, const char *mtgId, const char *pEmail, int iCmd );


// Accessor functions for meetings
void            meeting_add_reference(meeting_t info);
void            meeting_remove_reference(meeting_t info);
void            meeting_lock(meeting_t info);
void            meeting_unlock(meeting_t info);
participant_t   meeting_find_participant(meeting_t info, const char *part_id);
participant_t   meeting_find_participant_by_userid(meeting_t info, const char *user_id);
submeeting_t    meeting_find_submeeting(meeting_t info, const char *sub_id);
participant_t   meeting_next_participant(meeting_t info, participant_t prev);
submeeting_t    meeting_next_submeeting(meeting_t info, submeeting_t prev);
participant_t   meeting_find_host(meeting_t info);
participant_t   meeting_find_me(meeting_t mtg);


// Functions for meeting invite
void            meeting_invite_destroy(meeting_invite_t invt);


enum controller_states
{
    CONTROLLER_NONE,
    CONTROLLER_REGISTERING,
    CONTROLLER_CONNECTED,
};

enum controller_errors
{
    ERR_CONTROLLER = -899,
    ERR_UNKNOWNMEETING,
    ERR_NOSHARESERVER,
};

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif // API2_H_INCLUDED
