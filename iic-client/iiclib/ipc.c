/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef _WIN32
#include <windows.h>
#define HAVE_WIN32_BOOL
#endif

#include <stdlib.h>

#include <apr.h>
#include <apr_pools.h>
#include <apr_mmap.h>
#include <apr_atomic.h>
#include <apr_time.h>
#include <apr_strings.h>
#include <apr_file_io.h>
#include <apr_thread_proc.h>
#include <apr_thread_mutex.h>


#ifdef LINUX
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#endif

#ifdef MACOSX
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#endif

#ifdef WIN32
  #define MSG_HEADER_SIZE 0
#else
  #define MSG_HEADER_SIZE (sizeof(long))
#endif

#include "ipc.h"
#include "utils.h"

#include <netlib/log.h>


/*
    IPC

    Multiple IM clients, single instance of Zon client
    We may be limited to communicating with one IM client

    IM launches Zon

    IM -> Zon
        Command, screen name, password

        Accept register
        Send Presence info

    Zon -> IM
        Register to receive presence
        Send IM

*/

struct memory_st
{
    int         state;      // 0 = writeable, 1 = readable

    int         len;
    char        data[1];
};

// Shared memory object
struct shm_st
{
    apr_pool_t  *pool;
    const char  *name;
	apr_file_t	 *file;
    apr_mmap_t  *shm;
    BOOL        created;
    struct memory_st *buffer;
    int         size;
};

// Queue object
struct queue_st
{
    apr_pool_t  *pool;
    const char  *name;
    BOOL        is_server;

    queue_handler handler;
    void        *user;
    apr_thread_t *worker;
#ifdef WIN32
    HANDLE  mailslot;           // Receive mailslot for server
    HANDLE  client_mailslot;    // Receive mailslot for client
#else
    int     queue;
    int     client_queue;
#endif

    apr_thread_mutex_t *lock;
    datablock_t head;
    datablock_t tail;
};


static void * worker(apr_thread_t *thread, void *udata);

void ipc_init()
{
	apr_initialize();
}

shm_t shm_create(const char *name, int size, BOOL create)
{
    char fullname[1042];
    apr_pool_t *newpool;
    shm_t newmem;
    apr_status_t stat;
	apr_off_t offset = size + sizeof(struct memory_st);

	apr_pool_create(&newpool, NULL);
    newmem = apr_pcalloc(newpool, sizeof(struct shm_st));
    newmem->pool = newpool;
    newmem->name = apr_pstrdup(newpool, name);

	sprintf(fullname, "/tmp/%s.shm", newmem->name);

	stat = apr_file_open(&newmem->file, fullname, APR_CREATE|APR_READ|APR_WRITE, APR_FPROT_OS_DEFAULT, newpool);
	if (stat != APR_SUCCESS)
	{
		errorf("Error opening memory mapped file (%d)\n", stat);
		return NULL;
	}

	// Extend file to make room for our buffer
	stat = apr_file_seek(newmem->file, APR_SET, &offset);
	if (stat == APR_SUCCESS)
		stat = apr_file_puts("END", newmem->file);
	if (stat != APR_SUCCESS)
	{
		errorf("Error sizing memory mapped file (%d)\n", stat);
		return NULL;
	}
	
    stat = apr_mmap_create(&newmem->shm, newmem->file, 0, offset, APR_MMAP_READ|APR_MMAP_WRITE, newpool);
    if (stat != APR_SUCCESS)
    {
        errorf("Error opening memory segment (%d)\n", stat);
        return NULL;
    }

    newmem->buffer = newmem->shm->mm;
    newmem->size = size;
    newmem->created = create;

    if (create)
    {
        newmem->buffer->state = 0;
    }

    return newmem;
}

// Poor man's sync:  there should only be two parties, so simple flag should suffice
static int wait_state(int *mem, int state, int timeout)
{
    int count = timeout / 50;
    do
    {
        int curr = apr_atomic_read32(mem);
        if (curr == state)
            return 0;
        apr_sleep(50 * 1000);
    }
    while (count--);

    return -1;
}

static void signal_state(int *mem, int state)
{
    apr_atomic_set32(mem, state);
}

int shm_put(shm_t shm, datablock_t blk, int timeout)
{
    if (wait_state(&shm->buffer->state, 0, timeout))
        return -1;

    shm->buffer->len = blk->len;
    memcpy(shm->buffer->data, blk->data, blk->len);

    signal_state(&shm->buffer->state, 1);

    return 0;
}

datablock_t shm_get(shm_t shm, int timeout)
{
    datablock_t blk;

    if (wait_state(&shm->buffer->state, 1, timeout))
        return NULL;

    blk = datablock_create(shm->buffer->len);
    memcpy(blk->data, shm->buffer->data, shm->buffer->len);
    blk->len = shm->buffer->len;
    memset(shm->buffer->data, shm->buffer->len, 0);

    signal_state(&shm->buffer->state, 0);

    return blk;
}

void *shm_get_buffer(shm_t shm)
{
	return shm->buffer->data;
}

void shm_destroy(shm_t shm)
{
	apr_mmap_delete(shm->shm);
	apr_file_close(shm->file);
	//apr_file_remove();
    apr_pool_destroy(shm->pool);
}


queue_t queue_create(const char *name)
{
    apr_pool_t *newpool;
    queue_t newq;

    apr_pool_create(&newpool, NULL);
    newq = apr_pcalloc(newpool, sizeof(struct queue_st));
    newq->pool = newpool;
    newq->name = apr_pstrdup(newpool, name);
    apr_thread_mutex_create(&newq->lock, APR_THREAD_MUTEX_NESTED, newpool);
    
    return newq;
}

#if defined(LINUX) || defined(MACOSX)
static void create_file(const char *fullname)
{
    int fd;
    fd = creat(fullname, S_IRWXU);
    if (fd >= 0)
        close(fd);
}
#endif

int queue_server(queue_t newq)
{
    char fullname[1042];

    newq->is_server = TRUE;

#ifdef WIN32
    sprintf(fullname, "\\\\.\\mailslot\\%s.svr", newq->name);

    newq->mailslot = CreateMailslot(
                          fullname,             // mailslot name
                          MAX_MSG_SIZE,              // input buffer size
                          MAILSLOT_WAIT_FOREVER,    // no timeout
                          NULL);                    // default security attribute

    if (newq->mailslot == INVALID_HANDLE_VALUE)
    {
        return -1;
    }
#else
    key_t key;
    struct msqid_ds queue_ds;

    sprintf(fullname, "/tmp/%s.svr", newq->name);
    create_file(fullname);

    /* Open queue--if it exists, destroy and re-create */
    key = ftok(fullname, 'Z');
    if((newq->queue = msgget(key, 0666)) >= 0)
    {
        msgctl(newq->queue, IPC_STAT, &queue_ds);
        msgctl(newq->queue, IPC_RMID, &queue_ds);
    }

    if((newq->queue = msgget(key, IPC_CREAT|IPC_EXCL|0666)) == -1)
    {
        return -1;
    }
#endif

    return 0;
}

void queue_set_event_handler(queue_t newq, queue_handler handler, void *user)
{
    newq->handler = handler;
    newq->user = user;
}

void queue_run(queue_t newq)
{
    apr_thread_create(&newq->worker, NULL, (apr_thread_start_t)worker, newq, newq->pool);
    apr_thread_detach(newq->worker);
}

static void * worker(apr_thread_t *thread, void *udata)
{
    queue_t q = (queue_t)udata;
    char buffer[MAX_MSG_SIZE];
    BOOL done = FALSE;

    while (!done)
    {
#ifdef WIN32
        HANDLE target_mailslot;

        if (q->is_server)
        {
            target_mailslot = q->mailslot;
        }
        else
        {
            target_mailslot = q->client_mailslot;
        }

        DWORD bytes = 0;
        BOOL result;

        result = ReadFile(
                       target_mailslot,    // handle to mailslot
                       buffer,             // buffer to receive data
                       sizeof(buffer),     // size of buffer
                       &bytes,             // number of bytes read
                       NULL);              // not overlapped I/O

        if ( (!result) || (0 == bytes))
        {
            done = TRUE;
        }
#else
        int target_queue;
        if (q->is_server)
        {
            target_queue = q->queue;
        }
        else
        {
            target_queue = q->client_queue;
        }

        int bytes = 0;

        bytes = msgrcv(target_queue, buffer, sizeof(buffer), 0, 0);

        if (bytes <= 0)
        {
            done = TRUE;
        }
#endif

        /* Build datablock and dispatch */
        if (bytes > 0)
        {
            datablock_t blk = datablock_create(bytes);
            memcpy(blk->data, buffer + MSG_HEADER_SIZE, bytes);
            blk->len = bytes;

			if (q->handler)
            {
				q->handler(q->user, blk);
            }
            else 
            {
                // No handler, add block to buffer
                apr_thread_mutex_lock(q->lock);
                if (q->tail != NULL)
                {
                    q->tail->next = blk;
                    q->tail = blk;
                }
                else
                {
                    q->head = q->tail = blk;
                }
                apr_thread_mutex_unlock(q->lock);
            }
        }
    }

    apr_thread_exit(thread, 0);
	return NULL;
}

int queue_client(queue_t newq)
{
    char fullname[1042];

    newq->is_server = FALSE;

#ifdef WIN32
    /* Open our own mailslot in case server sends a message back */
    sprintf(fullname, "\\\\.\\mailslot\\%s.clnt", newq->name);

    newq->client_mailslot = CreateMailslot(
                          fullname,             // mailslot name
                          MAX_MSG_SIZE,              // input buffer size
                          MAILSLOT_WAIT_FOREVER,    // no timeout
                          NULL);                    // default security attribute

    if (newq->client_mailslot != INVALID_HANDLE_VALUE)
    {
        return 0;
    }
#else
    key_t key;
    struct msqid_ds queue_ds;

    /* Open our own queue to receive messages */
    sprintf(fullname, "/tmp/%s.clnt", newq->name);
    create_file(fullname);

    key = ftok(fullname, 'Z');
    if((newq->client_queue = msgget(key, 0666)) >= 0)
    {
        msgctl(newq->client_queue, IPC_STAT, &queue_ds);
        msgctl(newq->client_queue, IPC_RMID, &queue_ds);
    }

    if((newq->client_queue = msgget(key, IPC_CREAT|IPC_EXCL|0666)) != -1)
    {
        return 0;
    }
#endif

    /* Error creating client side of queue */
    return -1;
}

static long open_queue(queue_t q, int wait_timeout)
{
    char fullname[1042];
    int count = wait_timeout / 50;

#ifdef WIN32
    HANDLE target_mailslot = NULL;

    /* Server sends to client, client sends to server */
    if (q->is_server)
    {
        sprintf(fullname, "\\\\.\\mailslot\\%s.clnt", q->name);
    }
    else
    {
        sprintf(fullname, "\\\\.\\mailslot\\%s.svr", q->name);
    }

    /* Open mailslot...wait for it up to timeout if not there */
    do {
        target_mailslot = CreateFile(
                                fullname,          // mailslot name
                                GENERIC_WRITE,         // mailslot write only
                                FILE_SHARE_READ,       // required for mailslots
                                NULL,                  // default security attributes
                                OPEN_EXISTING,         // opens existing mailslot
                                FILE_ATTRIBUTE_NORMAL, // normal attributes
                                NULL);                 // no template file

        if (target_mailslot != INVALID_HANDLE_VALUE)
        {
            return (long)target_mailslot;
        }

	if (count)
            apr_sleep(50 * 1000);
    }
    while (count--);
#else
    key_t key;
    struct msqid_ds queue_ds;
    int target_queue;

    /* Server sends to client, client sends to server */
    if (q->is_server)
    {
        sprintf(fullname, "/tmp/%s.clnt", q->name);
    }
    else
    {
        sprintf(fullname, "/tmp/%s.svr", q->name);
    }

    /* Open mailslot...wait for it up to timeout if not there */
    create_file(fullname);
    key = ftok(fullname, 'Z');
    do
    {
        if((target_queue = msgget(key, 0666)) != -1)
        {
            return target_queue;
        }

        if (count)
	    apr_sleep(50 * 1000);
    }
    while (count--);
#endif

    return -1;
}

static void close_queue(queue_t q, long handle)
{
#ifdef WIN32
    CloseHandle((HANDLE)handle);
#else
    /* No-op */
#endif
}

int queue_wait(queue_t queue, int timeout)
{
    long target = open_queue(queue, timeout);
    if (target != -1)
    {
        close_queue(queue, target);
        return 0;
    }
    return -1;
}

int queue_send(queue_t queue, datablock_t blk)
{
    long target = open_queue(queue, 0);
    int stat = 0;

    if (target == -1)
    {
        return QERR_NO_REMOTE;
    }

#ifdef WIN32
    DWORD bytes;
    BOOL result;

    result = WriteFile(
                  (HANDLE)target,    // handle to mailslot
                  blk->data,          // buffer to write from
                  blk->len,           // number of bytes to write, include the NULL
                  &bytes,             // number of bytes written
                  NULL);              // not overlapped I/O

    if ( (!result) || bytes != blk->len)
    {
        stat = QERR_SEND_FAILED;
    }
#else
    long *type = blk->data;

    memmove(blk->data + MSG_HEADER_SIZE, blk->data, blk->len);
    *type = 1;

    if((msgsnd(target, (struct msgbuf *)blk->data, blk->len, 0)) == -1)
    {
        stat = QERR_SEND_FAILED;
    }
#endif

    close_queue(queue, target);

    return stat;
}

datablock_t queue_get(queue_t queue, int wait_timeout)
{
    char buffer[MAX_MSG_SIZE];
    int count = wait_timeout / 50;

    // Check local buffer first
    if (queue->handler == NULL)
    {
        datablock_t blk = NULL;
        
        apr_thread_mutex_lock(queue->lock);
        if (queue->head)
        {
            blk = queue->head;
            queue->head = blk->next;
            if (queue->tail == blk)
                queue->tail = NULL;
        }
        apr_thread_mutex_unlock(queue->lock);
        
        return blk;
    }

#ifdef WIN32
    HANDLE target_mailslot;

    if (queue->is_server)
    {
        target_mailslot = queue->mailslot;
    }
    else
    {
        target_mailslot = queue->client_mailslot;
    }

    DWORD bytes;
    DWORD msgs;
    BOOL result;

    /* Wait for message to appear */
    do
    {
        GetMailslotInfo(
                        target_mailslot,
                        NULL,
                        &bytes,
                        &msgs,
                        NULL);
        if (msgs > 0)
            break;

        if (count)
	    apr_sleep(50 * 1000);
    }
    while (count--);

    if (msgs == 0)
        return NULL;

    /* Read message */
    result = ReadFile(
                   target_mailslot,    // handle to mailslot
                   buffer,             // buffer to receive data
                   sizeof(buffer),     // size of buffer
                   &bytes,             // number of bytes read
                   NULL);              // not overlapped I/O

    if ( (!result) || (0 == bytes))
    {
        return NULL;
    }
#else
    int target_queue;
    if (queue->is_server)
    {
        target_queue = queue->queue;
    }
    else
    {
        target_queue = queue->client_queue;
    }

    int bytes = 0;
    struct msqid_ds queue_ds;
    queue_ds.msg_qnum = 0;

    do
    {
        msgctl(target_queue, IPC_STAT, &queue_ds);
        if (queue_ds.msg_qnum > 0)
            break;

	if (count)
	    apr_sleep(50 * 1000);
    }
    while (count--);

    if (queue_ds.msg_qnum == 0)
        return NULL;

    bytes = msgrcv(target_queue, buffer, sizeof(buffer), 0, 0);

    if (bytes <= 0)
    {
        return NULL;
    }
#endif

    datablock_t blk = datablock_create(bytes);
    memcpy(blk->data, buffer + MSG_HEADER_SIZE, bytes);
    blk->len = bytes;
    return blk;
}

void queue_destroy(queue_t queue)
{
#ifdef WIN32
    CloseHandle(queue->mailslot);
    if (queue->client_mailslot)
        CloseHandle(queue->client_mailslot);
#else
    struct msqid_ds queue_ds;
    int target_queue;
    if (queue->is_server)
    {
        target_queue = queue->queue;
    }
    else
    {
        target_queue = queue->client_queue;
    }
    msgctl(target_queue, IPC_RMID, &queue_ds);
#endif
    apr_pool_destroy(queue->pool);
}


datablock_t datablock_create(int size)
{
    datablock_t blk = calloc(1, sizeof(*blk));
    if (size < MIN_IPC_SIZE)
        size = MIN_IPC_SIZE;
    blk->size = size;
    /* We allocate with enough extra space for a long for Linux where a type code
       is supposed to come first in the message. */
    blk->data = malloc(blk->size + MSG_HEADER_SIZE);
    return blk;
}

void datablock_add_string(datablock_t blk, const char *str, const char *def)
{
    char *p = ((char *)blk->data) + blk->len;
    int len;

    if (!str)
        str = def;

    len = strlen(str);

    if (len + blk->len < blk->size)
    {
        strcpy(p, str);
        blk->len += (len+1);
    }
}

void datablock_add_int(datablock_t blk, int val)
{
    char *p = ((char *)blk->data) + blk->len;
	int len = 4;

    if (len + blk->len < blk->size)
    {
        memcpy(p, (char*)&val, 4);
        blk->len += (len);
    }
}

const char *datablock_get_string(datablock_t blk)
{
    char *p = ((char *)blk->data) + blk->pos;

    if (blk->pos < blk->len)
    {
        blk->pos += (strlen(p) + 1);
        return p;
    }

	blk->status = -1;
    return NULL;
}

int datablock_get_int(datablock_t blk)
{
    char *p = ((char *)blk->data) + blk->pos;
	int len = 4;

    if (blk->pos < blk->len)
    {
        blk->pos += len;
        return *(int*)p;
    }

	blk->status = -1;
    return -1;
}

const char *datablock_peek_string(datablock_t blk)
{
    char *p = ((char *)blk->data) + blk->pos;

    if (blk->pos < blk->len)
    {
        return p;
    }

    return NULL;
}

int datablock_status(datablock_t blk)
{
	return blk->status;
}

void datablock_destroy(datablock_t blk)
{
    free(blk->data);
    free(blk);
}
