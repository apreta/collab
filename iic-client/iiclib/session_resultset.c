/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef _WIN32
#include <windows.h>
#endif


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>
#include <apr_general.h>
#include <json.h>

#define NO_DEFINE_APR
#include "session_api.h"

#include <netlib/log.h>

struct resultset_st
{
	const char **fields;

	xmlrpc_env *env;
	xmlrpc_value *value;
	xmlrpc_value *rows;

    BOOL packed_flag;
	struct json_object *packed_rows;

	char *method_name;

	BOOL rowset_flag;

	int first_row;
	int chunk_size;
	int total_rows;
	BOOL more_data;
};

void parse(resultset_t rs);
BOOL resultset_get_output_values(resultset_t rs, char *format, ...);


resultset_t resultset_create(xmlrpc_env *_env, xmlrpc_value *_value)
{
    resultset_t rs = (resultset_t) calloc(1, sizeof(struct resultset_st));

	rs->env = _env;
	rs->value = _value;

	if (rs->value)
		xmlrpc_INCREF(rs->value);

	parse(rs);

	return rs;
}

void resultset_destroy(resultset_t rs)
{
	if (rs->value)
		xmlrpc_DECREF(rs->value);
    if (rs->packed_rows)
        json_object_put(rs->packed_rows);
}

static void parse_packed_data(resultset_t rs)
{
	xmlrpc_value *str_value;
	const char *str;

    struct json_object* rows;

    str_value = xmlrpc_struct_get_value(rs->env, rs->rows, "data");
    if (rs->env->fault_occurred)
    {
        errorf("Error retrieving packed value\r\n");
    }

    xmlrpc_read_string(rs->env, str_value, &str);
    if (rs->env->fault_occurred)
    {
        errorf("Error retrieving packed string data\r\n");
    }

    rows = json_tokener_parse((char*)str);
    if (json_is_error(rows))
    {
        errorf("Error parsing packed string data\r\n");
    }

    rs->packed_rows = rows;
}

void parse(resultset_t rs)
{
	if (xmlrpc_value_type(rs->value) == XMLRPC_TYPE_ARRAY)
	{
		int more;

		xmlrpc_value *first = xmlrpc_array_get_item(rs->env, rs->value, 0);
		if (rs->env->fault_occurred)
		{
			errorf("Error parsing result\r\n");
			return;
		}

		// If first element is an array, assume this is a rowset.
		if (xmlrpc_value_type(first) == XMLRPC_TYPE_ARRAY)
		{
			// Row set encoding is chunk info and array of arrays.
			xmlrpc_parse_value(rs->env, rs->value, "(Aiiii)", &rs->rows, &rs->first_row, &rs->chunk_size, &rs->total_rows, &more);
			if (rs->env->fault_occurred)
			{
				errorf("Error parsing result\r\n");
				rs->rows = NULL;
			}
			else
			{
				rs->more_data = more != 0;
				rs->rowset_flag = TRUE;
			}
		}
		else if (xmlrpc_value_type(first) == XMLRPC_TYPE_STRUCT)
		{
			// Row set encoding is chunk info and array of arrays.
			xmlrpc_parse_value(rs->env, rs->value, "(Siiii)", &rs->rows, &rs->first_row, &rs->chunk_size, &rs->total_rows, &more);
			if (rs->env->fault_occurred)
			{
				errorf("Error parsing result\r\n");
				rs->packed_rows = NULL;
			}
			else
			{
				rs->more_data = more != 0;
				rs->rowset_flag = TRUE;
				rs->packed_flag = TRUE;
			}

			parse_packed_data(rs);
		}
	}
	else
	{
		errorf("Error parsing result\r\n");
		rs->rowset_flag = FALSE;
	}
}

BOOL resultset_is_rowset(resultset_t rs)
{
	return rs->rowset_flag;
}

int resultset_get_first_index(resultset_t rs)
{
	return rs->first_row;
}

int resultset_get_chunk_size(resultset_t rs)
{
	return rs->chunk_size;
}

int resultset_get_total_rows(resultset_t rs)
{
	return rs->total_rows;
}

BOOL resultset_more_rows_available(resultset_t rs)
{
	return rs->more_data;
}

int resultset_get_column(resultset_t rs, const char *field)
{
	int i;

	if (rs->fields == NULL)
		return -1;

	for (i = 0; rs->fields[i] != NULL; i++)
		if (!strcmp(field, rs->fields[i]))
			return i;

	return -1;
}

void resultset_set_column_info(resultset_t rs, const char *_fields[])
{
	rs->fields = _fields;
}

const char *resultset_get_value_by_col(resultset_t rs, int row_num, int col)
{
	const char *ret = NULL;

    if (rs->packed_flag)
    {
        struct json_object *row, *field;

        if (rs->packed_rows == NULL)
        {
            errorf("No packed data in result set\r\n");
            return NULL;
        }

        row = json_object_array_get_idx(rs->packed_rows, row_num);
        if (row == NULL)
            return NULL;

        field = json_object_array_get_idx(row, col);
        if (field == NULL)
            return NULL;

        ret = json_object_get_string(field);
    }
    else
    {
        xmlrpc_value *row_value;
        xmlrpc_value *col_val;

        if (rs->rows == NULL)
        {
            errorf("No row data in result set\r\n");
            return NULL;
        }

        // Get array of field values from array of rows.
        row_value = xmlrpc_array_get_item(rs->env, rs->rows, row_num);
        if (rs->env->fault_occurred)
        {
            errorf("Error accessing row %d in result set\r\n", row_num);
            return NULL;
        }

        // Find specified field value.
        col_val = xmlrpc_array_get_item(rs->env, row_value, col);
        if (rs->env->fault_occurred)
        {
            errorf("Error retrieving value for column %d\r\n", col);
            return NULL;
        }

        // Parse as string (only data type currently supported)
        xmlrpc_parse_value(rs->env, col_val, "s", &ret);
        if (rs->env->fault_occurred)
        {
            errorf("Error retrieving value for column %d\r\n", col);
            return NULL;
        }
    }

	return ret;
}

// Get value from row and column.
// Note:  we shouldn't have to deref and values obtained herein as these
// accessors shouldn't increment the ref count.
const char *resultset_get_value(resultset_t rs, int row_num, const char *field)
{
    int col;

	if (rs->fields == NULL)
	{
		errorf("Column information not specified for result set\r\n");
		return NULL;
	}

	// Figure out column from field name
	col = resultset_get_column(rs, field);
	if (col < 0)
	{
		errorf("Invalid field %s requested\r\n", field);
		return NULL;
	}

	return resultset_get_value_by_col(rs, row_num, col);
}

// Get multiple values from row.
BOOL resultset_get_values(resultset_t rs, int row_num, char *format, ...)
{
    xmlrpc_value *row_value;
	va_list args;
	va_start(args, format);

    if (rs->packed_flag)
    {
        struct json_object *row, *field;
        char *pos = format;
        int col = 0;

        if (rs->packed_rows == NULL)
        {
            errorf("No packed data in result set\r\n");
            return FALSE;
        }

        row = json_object_array_get_idx(rs->packed_rows, row_num);
        if (row == NULL)
            return FALSE;

        while (*pos != '\0' && *pos != ')')
        {
            switch (*pos)
            {
                case '*':
                case '(':
                    break;
                case 's':
                {
                    char ** pstr = va_arg(args, char **);

                    field = json_object_array_get_idx(row, col++);
                    if (field == NULL)
                        return FALSE;

                    (*pstr) = json_object_get_string(field);
                    break;
                }
                case 'i':
                {
                    int * pnum = va_arg(args, int *);

                    field = json_object_array_get_idx(row, col++);
                    if (field == NULL)
                        return FALSE;

                    (*pnum) = json_object_get_int(field);
                    break;
                }
                default:
                    errorf("Unsuppored format character: %c\r\n", *pos);
                    return FALSE;
            }
            ++pos;
        }

    }
    else
    {
        if (rs->rows == NULL)
        {
            errorf("No row data in result set\r\n");
            return FALSE;
        }

        row_value = xmlrpc_array_get_item(rs->env, rs->rows, row_num);
        if (rs->env->fault_occurred)
        {
            errorf("Error accessing row %d in result set\r\n", row_num);
            return FALSE;
        }

        xmlrpc_parse_value_va(rs->env, row_value, format, args);
        if (rs->env->fault_occurred)
        {
            errorf("Error parsing row %d in result set\r\n", row_num);
            return FALSE;
        }
    }

	va_end(args);
	return TRUE;
}

int resultset_get_row_size(resultset_t rs, int row_num)
{
    xmlrpc_value *row_value;

    if (rs->packed_flag)
    {
        struct json_object *row;

        if (rs->packed_rows == NULL)
        {
            errorf("No packed data in result set\r\n");
            return 0;
        }

        row = json_object_array_get_idx(rs->packed_rows, row_num);
        if (row == NULL)
            return 0;

        return json_object_array_length(row);
    }
    else
    {
        if (rs->rows == NULL)
        {
            errorf("No row data in result set\r\n");
            return 0;
        }

        row_value = xmlrpc_array_get_item(rs->env, rs->rows, row_num);
        if (rs->env->fault_occurred)
        {
            errorf("Error accessing row %d in result set\r\n", row_num);
            return 0;
        }

        return xmlrpc_array_size(rs->env, row_value);
    }
}

int resultset_get_status(resultset_t rs)
{
	int status = -1;

	resultset_get_output_values(rs, "(i)", &status);

	return status;
}

BOOL resultset_get_output_values(resultset_t rs, char *format, ...)
{
	va_list args;
	va_start(args, format);

	xmlrpc_parse_value_va(rs->env, rs->value, format, args);
	if (rs->env->fault_occurred)
	{
		errorf("Error parsing output parameters\r\n");
		return FALSE;
	}

	va_end(args);
	return TRUE;
}

int resultset_get_output_count(resultset_t rs)
{
    int count = xmlrpc_array_size(rs->env, rs->value);
    if (rs->env->fault_occurred)
    {
        errorf("Error getting output count\r\n");
        return -1;
    }
    return count;
}
