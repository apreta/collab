/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef IPC_H_INCLUDED
#define IPC_H_INCLUDED

#if !defined(HAVE_WIN32_BOOL) && !defined(SESSION_API_H_INCLUDED)
typedef int BOOL;
#endif

#define MAX_MSG_SIZE 32768

#define MIN_IPC_SIZE 1024
#define MAX_IPC_SIZE (512*1024)

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct shm_st * shm_t;
typedef struct queue_st * queue_t;
typedef struct datablock_st * datablock_t;


struct datablock_st
{
    int size;   // allocated size
    int len;    // size written so far
    void *data;
    int pos;    // read pos in buffer
	int status;
    
    datablock_t next;
};


void ipc_init();

//
// Shared memory abstraction
// Note: not well tested yet, simple locking assumes only two parties will open segment.
//

// Create or open shared memory segment of indicated size.
shm_t shm_create(const char *name, int size, BOOL create);

// Put data into shared memory segment (will block if previous data was not read)
int shm_put(shm_t, datablock_t, int timeout);

// Wait up to timeout for data to be put; get data if available.
datablock_t shm_get(shm_t, int timeout);

// Get direct access to shared memory
void *shm_get_buffer(shm_t);

// Destroy shared memory segment
void shm_destroy(shm_t);


//
// 2 party bi-directional queue abstraction
//

#define QERR_NO_REMOTE -1
#define QERR_SEND_FAILED -2

typedef void (*queue_handler)(void *user, datablock_t blk);

// Create new queue.
queue_t queue_create(const char *name);

// Set callback handler for incoming messages (only called if queue_run is used)
void queue_set_event_handler(queue_t q, queue_handler handler, void *user);

// Create server side of queue.
int queue_server(queue_t q);

// Create client side of queue.
int queue_client(queue_t q);

// Start queue processing thread that waits for incoming messages and dispatches to callback.
// Note: callbacks will be on a different thread.
void queue_run(queue_t queue);

// Wait for other end of queue to be created.
int queue_wait(queue_t queue, int timeout);

// Send message to other side of queue.
int queue_send(queue_t queue, datablock_t blk);

// Get message from queue.
datablock_t queue_get(queue_t queue, int timeout);

// Destroy queue.  Will stop background thread also if needed.
void queue_destroy(queue_t queue);

//
// Describe data to be exchanged in queue or memory.
// Intention is to add other datatypes if needed, and perhaps a validation flag that can be checked to
// make sure all data was retrieved.
//

// Create new datablock.
datablock_t datablock_create(int size);

// Add string to data block.  def is used if str is NULL.
void datablock_add_string(datablock_t blk, const char *str, const char *def);

// Add integer to data block.
void datablock_add_int(datablock_t blk, int val);

// Get string from datablock.
const char *datablock_get_string(datablock_t blk);

// Get integer from datablock.
int datablock_get_int(datablock_t blk);

// Peek at string from datablock.
const char *datablock_peek_string(datablock_t blk);

// Return non-zero if parsing failed (e.g. not enough data to satisfy get calls)
int datablock_status(datablock_t blk);

// Destroy datablock.
void datablock_destroy(datablock_t);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif // IPC_H_INCLUDED
