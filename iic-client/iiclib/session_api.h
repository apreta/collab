/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef SESSION_API_H_INCLUDED
#define SESSION_API_H_INCLUDED

#ifndef HAVE_WIN32_BOOL
typedef int BOOL;
#endif

typedef const char * pstring;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef NO_DEFINE_APR
typedef struct apr_pool_t apr_pool_t;
#endif

typedef struct session_st * session_t;
typedef struct resultset_st * resultset_t;

typedef void * session_array_t;


struct session_message_st
{
    const char * id;
    const char * str_data;
    void * ptr_data;
};
typedef struct session_message_st * session_message_t;


struct messaging_im_st
{
    // Private
    apr_pool_t *pool;

    // Public
    const char *user;
    const char *server;
    const char *resource;
    const char *body;
    int composing;
};
typedef struct messaging_im_st * messaging_im_t;


struct messaging_presence_st
{
    // Private
    apr_pool_t *pool;

    // Public
    const char *user;
    const char *server;
    const char *resource;
    int presence;
    const char *status;
};
typedef struct messaging_presence_st * messaging_presence_t;


//
// Various event callbacks
// Callbacks will be called from background thread.
//

typedef void (*session_error_handler)(void *, int err, const char *msg, int code, const char *id, const char *resource);
typedef void (*session_connected_handler)(void *);
typedef void (*session_password_changed_handler)(void *);

typedef void (*session_rpc_result_handler)(void *, const char *id, resultset_t result);
typedef void (*session_rpc_error_handler)(void *, const char *id, int code, const char *msg);
typedef void (*session_rpc_request_handler)(void *, const char *id, const char *method, resultset_t result);

typedef void (*session_im_handler)(void *, messaging_im_t im);
typedef void (*session_presence_handler)(void *, messaging_presence_t pres);

typedef void (*session_message_handler)(void *, session_message_t msg);

//
// Session APIs.
//

int session_initialize(const char *user_dir);

// Reset data held by any subobjects (controller, addressbk, etc.)
void session_flush(session_t sess);

// Set callback handlers
void session_set_error_handler(session_t sess, void *user, session_error_handler _handler);
void session_set_connected_handler(session_t sess, void *user, session_connected_handler _handler);
void session_set_password_changed_handler(session_t sess, void *user, session_password_changed_handler _handler);
void session_set_rpc_handlers(session_t session, void *user, const char *rpcid, session_rpc_result_handler result, session_rpc_error_handler error);
void session_set_rpc_request_handler(session_t session, void *user, const char *method, session_rpc_request_handler handler);
void session_set_im_handler(session_t session, void *user, session_im_handler im);
void session_set_presence_handler(session_t session, void *user, session_presence_handler pres);

// Broadcast message to registered handlers...mostly intended for use between library components
void session_add_message_handler(session_t sess, void *user, session_message_handler _handler);
void session_remove_message_handler(session_t sess, void *user, session_message_handler _handler);
void session_send_message(session_t sess, session_message_t msg);

// Create new session with indicated credentials--does not connect
session_t session_create(const char *_server, const char * _screen, const char *_password, const char *_resource, BOOL _enable_security, const char* trust_ca, BOOL _plain_text, const char *lastJID);

// Destroy session
void session_destroy(session_t sess);

// Initiate connection to server (result is reported via callback event)
int session_connect(session_t sess, BOOL proxy_only);

// Disconnect session
int session_disconnect(session_t sess, BOOL wait);

// Reset network library hunting
void session_reset_hunt(session_t sess);

// Query if session is using plain text auth
BOOL session_is_plaintext_auth(session_t sess);

// Query if session is SSL-enabled
BOOL session_is_secure(session_t sess);

// Set network connection options
void session_set_target_server(session_t sess, const char *target);
void session_set_proxy(session_t sess, const char * type, const char * address, int port);
void session_set_proxy_auth(session_t sess, const char * type, BOOL use_current_user, const char * user, const char * password);

// Initiate password change request
void session_change_password(session_t sess, const char *newpassword);

// Set session option (option name - values):
//    "http" - "yes", "no"
//    "primaryport" - "yes", "no"
void session_set_option(session_t sess, const char *option, const char *value);

// Get session option:
//    "usinghttp" - "yes", "no"   (are we using http mode?)
const char * session_get_option(session_t sess, const char *option);

// Make RPC request (usually used internally by other components)
int session_make_rpc_call(session_t sess, const char *id, const char *service, const char *method, const char *format, ...);

// Send IM
// (body can be speficied as NULL for sending typing/not typing notifications)
void session_send_im(session_t sess, const char *from, const char *to, const char *body, BOOL composing);

// Send chat room invitation
void session_send_invite(session_t sess, const char *room, const char *to, const char *body);

// Set presence for this session
void session_send_presence(session_t sess, char *to, int presence, char *status);

// Array functions
session_array_t session_create_array(int count, const char **values);
void session_add_array_element(session_array_t _array, const char *format, ...);
void session_free_array(session_array_t array);

// Result set functions
void resultset_destroy(resultset_t rs);
BOOL resultset_is_rowset(resultset_t rs);
int resultset_get_first_index(resultset_t rs);
int resultset_get_chunk_size(resultset_t rs);
int resultset_get_total_rows(resultset_t rs);
BOOL resultset_more_rows_available(resultset_t rs);
int resultset_get_column(resultset_t rs, const char *field);
void resultset_set_column_info(resultset_t rs, const char *_fields[]);
const char *resultset_get_value_by_col(resultset_t rs, int row_num, int col);
const char *resultset_get_value(resultset_t rs, int row_num, const char *field);
BOOL resultset_get_values(resultset_t rs, int row_num, char *format, ...);
int resultset_get_row_size(resultset_t rs, int row_num);
int resultset_get_status(resultset_t rs);
BOOL resultset_get_output_values(resultset_t rs, char *format, ...);
int resultset_get_output_count(resultset_t rs);

// Messaging functions
void messaging_presence_destroy(messaging_presence_t pres);
void messaging_im_destroy(messaging_im_t im);

// Utilities
const char * session_get_jid(session_t);
const char * session_get_password(session_t);
const char * session_get_service_jid(session_t sess, const char * service_name, char * buff);
void session_sha_hash(const char * str, char * hash);
int cmp_pstring( pstring a, pstring b );

// Session states.
typedef enum
{
	NET_OFF,
	NET_CONNECT,
	NET_STREAM,
	NET_REG,
	NET_AUTH,
	NET_ON
} session_states;

// Presence types.
typedef enum
{
    PRES_UNAVAILABLE,
    PRES_AVAILABLE,
    PRES_AWAY
} session_presences;

// These errors may range from -999 to -1
// -1000 and beyond are reserved for server errors (see iiclib/common.h)
typedef enum
{
	ERR_SOCKINIT = -999,
	ERR_INVALIDJID,
	ERR_CONNECTFAILED,
	ERR_NOTCONNECTED,
	ERR_RPCFAILED,
	ERR_AUTHFAILED,
	ERR_REGFAILED,
    ERR_CONNECTLOST,
	ERR_AUTHREQUIRED,
	ERR_PROXYAUTHREQUIRED,
	ERR_PROXYAUTHFAILED,
    ERR_CHANGEPASSWORDFAILED,
	ERR_NOMEMORY,
	ERR_SSLTLS_HANDSHAKE,
	ERR_NO_CERTIFICATE_FOUND,
	ERR_UNABLE_VERIFY_CERTIFICATE,
	ERR_CERT_SIGNER_NOT_FOUND,
	ERR_CERT_INVALID,
    ERR_PASSWD_EXPIRED,
    ERR_ACCOUNT_DEACTIVATED,
    ERR_SIGNEDONELSEWHERE,
    ERR_NOTIMPLEMENTED,
    ERR_PINGFAILED,
    ERR_WRONGSERVERNAME,

	ERR_SUCCESS = 0
} session_errors;

// default error code - matches Failed in iiclib/common.h
#define DefaultErrorCode -1000

#define RPC_RPC_NS  "iic:iq:rpc"
#define IIC_MEETING_NS "iic:x:meeting"
#define CONNECT_TIMEOUT 20

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif // API_H_INCLUDED
