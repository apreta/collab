/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef _WIN32
#include <windows.h>
#endif

#include <apr.h>
#include <apr_tables.h>
#include <apr_strings.h>

#include <stdlib.h>
#include <stdio.h>
#include <time.h>


static int id_seq = 0;

void * get_element_at(apr_array_header_t * hdr, int idx)
{
    if (idx >= hdr->nelts || idx < 0)
        return NULL;

    return hdr->elts + (hdr->elt_size * idx);
}

void remove_element_at(apr_array_header_t * hdr, int idx)
{
    int bytes;

    if (idx >= hdr->nelts || idx < 0)
        return;

    // if remove el 1 from 3 element array, bytes = elt_size
    bytes = (hdr->nelts - idx - 1) * hdr->elt_size;

    if (bytes > 0)
    {
        void *dest = hdr->elts + (hdr->elt_size * idx);
        void *src = hdr->elts + (hdr->elt_size * (idx+1));
        memmove(dest, src, bytes);
    }

    --hdr->nelts;
}

void * get_ptr_at(apr_array_header_t * hdr, int idx)
{
    if (idx >= hdr->nelts || idx < 0)
        return NULL;

    return *(void**)(hdr->elts + (hdr->elt_size * idx));
}

void * add_ptr(apr_array_header_t * hdr, apr_pool_t *pool, int element_size)
{
    void **ptr_element = apr_array_push(hdr);
    *ptr_element = apr_pcalloc(pool, element_size);

    return *ptr_element;
}

/*
   Make unique id for anonymous user by using our user id and incrementing integer.
   User id may be a single number as returned by schedule API, or 'user:###' as used by controller APIs.
*/
const char *make_anon_id(apr_pool_t *pool, const char *user_id)
{
    const char *p = strrchr(user_id, ':');
    if (p == NULL)
        p = user_id;
    if (++id_seq == 1)
        id_seq = time(NULL);
    user_id = apr_psprintf(pool, "anon:%d.%d", atoi(p), id_seq);
    return user_id;
}


/*
   Convert user_id from schedule form to controller form.
   User ids from database are just numbers.
   User ids in the controller are in the form 'user:###' for known users,
   or 'part:###' or 'anon:###' for unknown users
*/
const char *convert_user_id(apr_pool_t *pool, const char *user_id, const char *session_user_id)
{
    if (user_id == NULL)
    {
        user_id = make_anon_id(pool, session_user_id);
    }
    else
    {
        // See if in the form 'user:###' or 'part:###' or 'anon:###' already
        if (strchr(user_id, ':') == NULL)
        {
            // See if simple integer
            if (atoi(user_id) > 0)
            {
                user_id = apr_psprintf(pool, "user:%d", atoi(user_id));
            }
            else
            {
                user_id = make_anon_id(pool, session_user_id);
            }
        }
    }

    return user_id;
}

/*
   Convert user_id from schedule form to controller form.
*/
const char *convert_part_id(apr_pool_t *pool, const char *part_id)
{
    if (part_id == NULL)
    {
        part_id = "";
    }
    else
    {
        // See if in the form 'user:###' or 'part:###' or 'anon:###' already
        if (strchr(part_id, ':') == NULL)
        {
            // See if simple integer
            if (atoi(part_id) > 0)
            {
                part_id = apr_psprintf(pool, "part:%d", atoi(part_id));
            }
            else
            {
                part_id = "";
            }
        }
    }

    return part_id;
}

