/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef SCHEDULE_H_INCLUDED
#define SCHEDULE_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef NO_DEFINE_APR
typedef struct apr_pool_t apr_pool_t;
typedef struct apr_array_header_t apr_array_header_t;
typedef struct apr_hash_t apr_hash_t;
#endif

#include "session_api.h"
#include "addressbk.h"

/*
   Meeting data is static once it is fetched as there are no events from the server indicating
   a meeting schedule has changed.

   Find and Search APIs are asynchronous; the appropriate callback will be called when the
   data has been received from the server.
*/


// Meeting participants
// Role, selphone, selemail, and notify constants are defined in common.h.
struct invitee_st
{
    pstring meeting_id;
    pstring participant_id;     // leave blank when adding participant
    pstring user_id;            // copy from contact for known users, blank for adhoc
    int     role;
    pstring name;
    pstring screenname;
    int     selphone;
    pstring phone;
    int     selemail;
    pstring email;
    int     notify_type;
    BOOL    is_adhoc;           // true if not a registered system user

    int     call_options;       // Used to start 1-1 call, not stored in schedule (CallDirect for caller, CallSupervised for callee)

    BOOL    removed;
};
typedef struct invitee_st * invitee_t;


// Detailed info about meeting, with participant list
// Type, privacy, options and other attribute settings are defined in common.h.
struct meeting_details_st
{
    // private
    apr_pool_t *pool;
	apr_array_header_t *invitees;

    // public
    // Core attributes
	pstring meeting_id;
	pstring host_id;
	int     type;
	int     privacy;
	pstring title;
	int     start_time;
	int     options;

	// Detailed attributes
	int     host_enabled_features;
	pstring description;
	int     end_time;
	int     invite_expiration;
	int     invite_valid_before;
	pstring invite_message;
	int     display_attendees;
	int     chat_options;
	pstring password;
	pstring bridge_phone;
	pstring system_url;

};
typedef struct meeting_details_st * meeting_details_t;


// Summary info about meeting, useful for meeting center window
struct meeting_info_st
{
    // public
	pstring meeting_id;
	pstring host_id;
	int     type;
	int     privacy;
	pstring title;
	int     start_time;
	int     options;

	pstring my_participant_id;  // not filled in by schedule_find_public_meetings
	int     my_role;            // not filled in by schedule_find_public_meetings
	pstring host_first_name;
	pstring host_last_name;
	int     status;

    int     last_start_time;    // only filled in by schedule_find_meeting_for_user
	int     last_end_time;      // only filled in by schedule_find_meeting_for_user
};
typedef struct meeting_info_st * meeting_info_t;


// List of meetings fetched by a particular query
struct meeting_list_st
{
    // private
    apr_pool_t *pool;
    apr_array_header_t *meetings;

    // public
    int         type;
    pstring     directory_id;
    pstring     name;

    int         rows_fetched;
    int         rows_available;
};
typedef struct meeting_list_st * meeting_list_t;


typedef struct schedule_st * schedule_t;
typedef struct meeting_iterator_st * meeting_iterator_t;
typedef struct invitee_iterator_st * invitee_iterator_t;


// Create schedule object.
schedule_t schedule_create(session_t sess);

// Destory schedule object.
void schedule_destroy(schedule_t sched);

// Set authentication info.
void schedule_set_authentication(schedule_t sched, auth_info_t auth);

// Define callback types.
typedef void (*schedule_meeting_handler)(void *user, const char *opid, meeting_details_t mtg);
typedef void (*schedule_list_handler)(void *user, const char *opid, meeting_list_t list);
typedef void (*schedule_progress_handler)(void *user, const char *opid, int num_fetched, int total);
typedef void (*schedule_updated_handler)(void *user, const char *opid, int status, const char *meeting_id);
typedef void (*schedule_error_handler)(void *user, const char *opid, int code, const char *msg);

// Set callbacks for handling results
void schedule_set_list_handler(schedule_t, void *user, schedule_list_handler _handler);
void schedule_set_meeting_handler(schedule_t, void *user, schedule_meeting_handler _handler);
void schedule_set_progress_handler(schedule_t, void *user, schedule_progress_handler _handler);
void schedule_set_updated_handler(schedule_t, void *user, schedule_updated_handler _handler);
void schedule_set_error_handler(schedule_t, void *user, schedule_error_handler _handler);

// Downloads meeting info only for each category
int schedule_find_my_meetings(schedule_t, const char *opid);
int schedule_find_public_meetings(schedule_t, const char *opid);
int schedule_find_invited_meetings(schedule_t, const char *opid);

// Downloads more detailed meeting info for me (e.g. includes my role and pin)
int schedule_find_meeting_info(schedule_t, const char *opid, const char * meeting_id);

// Returns detailed meeting info and participants
int schedule_find_meeting_details(schedule_t sched, const char *opid, const char * meeting_id);

// Search for meeting based on criteria
// Set unspecified string criteria to NULL or empty string; set unspecified dates to MIN_DATE or MAX_DATE
// Call again with new starting_row to fetch next chunk; results will be appended to same result list
// opid must be unique for each search
int schedule_search_meetings(schedule_t sched, const char *opid,
                             const char *meeting_id, const char *title,
                             const char *first_name, const char *last_name,
                             BOOL in_progress,
                             int start_date, int end_date,
                             int starting_row, int chunk_size);

// Find meeting list for already run search (specify same operation id)
meeting_list_t schedule_find_search_results(schedule_t, const char *opid);

// Modify meetings
int schedule_update_meeting(schedule_t, const char *opid, meeting_details_t meeting, int email_options);
int schedule_remove_meeting(schedule_t, const char *opid, const char *meeting_id);

// Get meeting ID for my instant meeting...not available until after fetch of
// my meetings
const char * schedule_get_my_instant_meeting(schedule_t sched);

// Accessor functions for meeting lists
meeting_iterator_t  meeting_list_iterate(meeting_list_t list);
meeting_info_t      meeting_list_next(meeting_list_t list, meeting_iterator_t iter);
void                meeting_list_destroy(meeting_list_t list);

// Create new meeting definition (with suitable default values and host as invitee)
meeting_details_t   meeting_details_new(schedule_t);

// Create new meeting definition (as copy of existing meeting)
meeting_details_t   meeting_details_copy(schedule_t, meeting_details_t);

// Accessor functions for meetings details
invitee_iterator_t  meeting_details_iterate(meeting_details_t mtg);
invitee_t           meeting_details_next(meeting_details_t mtg, invitee_iterator_t iter);
void                meeting_details_destroy(meeting_details_t list);

invitee_t           meeting_details_add(meeting_details_t mtg);
void                meeting_details_remove(meeting_details_t mtg, invitee_t invitee);
invitee_t           meeting_details_find(meeting_details_t mtg, const char *part_id);
invitee_t           meeting_details_find_host(meeting_details_t mtg);

// Use to create string values for fields in meeting_details_t or invitee_t
pstring             meeting_details_string(meeting_details_t mtg, const char *str);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif // SCHEDULE_H_INCLUDED
