/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef _WIN32
#include <windows.h>
#define HAVE_WIN32_BOOL
#else
#include <signal.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>
#include <iksemel.h>

#include <apr.h>
#include <apr_general.h>
#include <apr_pools.h>
#include <apr_hash.h>
#include <apr_thread_proc.h>
#include <apr_tables.h>
#include <apr_strings.h>
#include <apr_time.h>

#define NO_DEFINE_APR
#include "session_api.h"

#include "handlerdefs.h"
#include "utils.h"

#include "services/common/common.h"

#include <netlib/log.h>

#define IKS_CHANGE_PASSWORD "ikschangepassword"
#define IKS_CHANGE_PASSWORD_LEN sizeof("ikschangepassword") - 1


// Private session info.
struct session_st
{
    iksparser		*parser;
    iksid			*id;
    ikstack			*st;
    int				features;
    int				authorized;
    char			*jid;
    char			*stream_id;
    char			*password;
    char			*server;
    char            *resource;
    session_states	state;
    volatile BOOL	done;
    volatile BOOL   stopped;
    int				port;
    int             start;
    volatile BOOL   worker_thread_blocked;

    // For using IIC proxy
    BOOL			proxy;
    char			*proxy_type;
    char			*proxy_address;
    int				proxy_port;

    char			*auth_type;
    BOOL			auth_current_user;
    char			*auth_user;
    char			*auth_password;
    BOOL			plain_text_auth;

    char            *reported_server;

    BOOL		    enable_security;

    DECLARE_EVENT_HANDLER(session,error)
    DECLARE_EVENT_HANDLER(session,connected)
    DECLARE_EVENT_HANDLER(session,password_changed)
    DECLARE_EVENT_HANDLER(session,im)
    DECLARE_EVENT_HANDLER(session,presence)

    apr_array_header_t *message_handlers;

    apr_hash_t      *rpc_handlers;
    apr_hash_t      *method_handlers;

    apr_pool_t      *pool;
    apr_thread_t    *worker;
};

struct message_handler_st
{
    session_message_handler handler;
    void * user;
};

struct rpc_handler_st
{
    const char * rpc_id;
    session_rpc_result_handler result_handler;
    session_rpc_error_handler error_handler;
    void * user;
};

struct rpc_method_st
{
    const char * method;
    session_rpc_request_handler request_handler;
    void * user;
};


//
// Local function defs.
//

resultset_t resultset_create(xmlrpc_env *_env, xmlrpc_value *_value);
static int on_stream(void *udata, int type, iks *node);
static void on_log(void *udata, const char *data, size_t size, int is_incoming);
static void on_packet(session_t sess, ikspak *pak);
static void on_iq_packet(session_t sess, ikspak *pak);
static void on_rpc_result(session_t sess, ikspak *pak);
static void on_rpc_request(session_t sess, ikspak *pak);
static void on_rpc_error(session_t sess, ikspak *pak);
static void on_message_packet(session_t, ikspak *pak);
static void on_presence_packet(session_t, ikspak *pak);
static void *worker(apr_thread_t *thread, void *udata);
static session_rpc_result_handler find_rpc_result_handler(session_t sess, const char *rpc_id, void ** user);
static session_rpc_error_handler find_rpc_error_handler(session_t sess, const char *rpc_id, void ** user);
static session_rpc_request_handler find_rpc_request_handler(session_t sess, const char *method, void ** user);


//
// Default callbacks.
//

void default_on_connected(void *u)
{
}

void default_on_error(void *u, int err, const char *msg, int code, const char *id, const char *resource)
{
	errorf("Error: %s", msg);
}

void default_on_password_changed(void *u)
{
	debugf("Change Password succeeded.");
}

void default_on_im(void *u, messaging_im_t im)
{
    debugf("IM received");
}

void default_on_presence(void *u, messaging_presence_t pres)
{
    debugf("Presence received");
}


//
// Declare APIs to set event callbacks
//
IMPL_SET_HANDLER(session,error)
IMPL_SET_HANDLER(session,connected)
IMPL_SET_HANDLER(session,password_changed)
IMPL_SET_HANDLER(session,im)
IMPL_SET_HANDLER(session,presence)


int session_initialize(const char *user_dir)
{
    apr_initialize();

#ifdef _WIN32
    {
        WSADATA wsaData;
        WORD wVersionRequested = MAKEWORD( 2, 0 );
        int err;

        err = WSAStartup( wVersionRequested, &wsaData );
        if ( err != 0 ) {
            /* Tell the user that we could not find a usable */
            /* WinSock DLL.                                  */

            return ERR_SOCKINIT;
        }
    }
#else
    signal(SIGPIPE, SIG_IGN);
#endif

    iks_set_option("path", user_dir);

    net_init_log(user_dir, TRUE);

	return 0;
}

void session_flush(session_t sess)
{
    struct session_message_st msg;
    msg.id = "session-reset";
    session_send_message(sess, &msg);
}

session_t session_create(const char *_server, const char * _screen, const char *_password, const char *_resource, BOOL _enable_security, const char* trust_ca, BOOL _plain_text, const char *lastJID)
{
    session_t sess = (session_t) calloc(1, sizeof(struct session_st));
    char jid[512];
    char cRes[128];

    apr_pool_create(&sess->pool, NULL);
    sess->rpc_handlers = apr_hash_make(sess->pool);
    sess->method_handlers = apr_hash_make(sess->pool);
    sess->message_handlers = apr_array_make(sess->pool, 4, sizeof(struct message_handler_st));

    sess->on_error = default_on_error;
    sess->on_connected = default_on_connected;
    sess->on_password_changed = default_on_password_changed;
    sess->on_im = default_on_im;
    sess->on_presence = default_on_presence;

	sess->port = 5222;	// ignored by network layer
	sess->password = _strdup(_password);
	sess->resource = _strdup(_resource);
	if (_server != NULL)
	{
	    char *p;

		sess->server = _strdup(_server);
		// _server may contain "::port"
		p = strstr(sess->server, "::");
		if (p)
		{
			*p = 0;
			sess->port = atoi(p+2);
		}
	}
	else
		sess->server = NULL;

    // Use time to make unique resource; probably is unique enough...
    sess->start = time(NULL);

    if(lastJID && strlen(lastJID) > 0)
    {
        // Keep using same resource so server knows this is the same user in the case of a reconnect
        char * last_res = strrchr(lastJID, '/');
        if (last_res)
            strcpy(cRes, last_res + 1);
    }
    else
    {
        //  But let's not take a chance; so we'll add 8 random hex digits to it.  That gives us a one in
        //   4 billion chance of duplicating, IF two users happened to start sessions during the same second.
        srand( sess->start );

        sprintf(cRes, "%s%d", _resource, sess->start);

        int len = strlen(cRes);

        char    cDigits[ ] = { "0123456789ABCDEF" };
        int     i;
        for( i = len; i<len+8; i++ )
        {
            cRes[ i ] = cDigits[ (rand( ) % 16) ];
        }
        cRes[ i ] = 0;
    }

    sprintf(jid, "%s@%s/%s", _screen, _server, cRes);
    sess->jid = _strdup(jid);

    sess->enable_security = _enable_security;
	iks_set_security(_enable_security, trust_ca);
	sess->parser = iks_stream_new(IKS_NS_CLIENT, sess, on_stream);
	iks_set_log_hook (sess->parser, on_log);
	sess->state = NET_OFF;
	sess->stream_id = NULL;
	sess->done = FALSE;
	sess->stopped = FALSE;
	sess->proxy = FALSE;
	sess->proxy_address = NULL;
	sess->proxy_type = NULL;
	sess->proxy_port = 0;

	sess->auth_type = NULL;
	sess->auth_current_user = FALSE;
	sess->auth_user = NULL;
	sess->auth_password = NULL;
	sess->plain_text_auth = _plain_text;

	sess->st = iks_stack_new(8192, 8192);		// TODO
	sess->id = iks_id_new( sess->st, sess->jid );

    return sess;
}

void session_destroy(session_t sess)
{
	session_disconnect(sess, TRUE);

	iks_stack_delete(sess->st);
	free(sess->jid);
	free(sess->password);
	if (sess->server)
		free(sess->server);
	iks_parser_delete(sess->parser);
	if (sess->stream_id)
		free(sess->stream_id);
	if (sess->proxy_address)
		free(sess->proxy_address);
	if (sess->proxy_type)
		free(sess->proxy_type);
	sess->proxy = FALSE;

	if (sess->auth_type)
		free(sess->auth_type);
	if (sess->auth_user)
		free(sess->auth_user);
	if (sess->auth_password)
		free(sess->auth_password);
}

void session_reset_hunt(session_t sess)
{
    iks_set_option("reset", "");
}

BOOL session_is_plaintext_auth(session_t sess)
{
    return sess->plain_text_auth;
}

BOOL session_is_secure(session_t sess)
{
    return sess->enable_security;
}

void session_set_option(session_t sess, const char *option, const char *value)
{
    if (strcasecmp(option, "http") == 0 || strcasecmp(option, "primaryport") == 0 || strcasecmp(option, "disablehunt") == 0)
        iks_set_option(option, value);
}

const char *session_get_option(session_t sess, const char *option)
{
    if (strcasecmp(option, "usinghttp") == 0)
        return iks_get_option(option);
    return NULL;
}

void session_set_target_server(session_t sess, const char * target)
{
    sess->server = _strdup(target);
}

void session_set_proxy(session_t sess, const char * type, const char * address, int port)
{
	if (sess->proxy_address)
		free(sess->proxy_address);
	if (sess->proxy_type)
		free(sess->proxy_type);
	if (type != NULL)
		sess->proxy_type = _strdup(type);
	else
		sess->proxy_type = NULL;
	if (address)
		sess->proxy_address = _strdup(address);
	else
		sess->proxy_address = NULL;
	sess->proxy_port = port;
}

void session_set_proxy_auth(session_t sess, const char * type, BOOL use_current_user, const char * user, const char * password)
{
	if (sess->auth_type)
		free(sess->auth_type);
	if (sess->auth_user)
		free(sess->auth_user);
	if (sess->auth_password)
		free(sess->auth_password);

	sess->auth_current_user = use_current_user;
	if (type != NULL)
		sess->auth_type = _strdup(type);
	else
		sess->auth_type = NULL;
	if (user != NULL)
		sess->auth_user = _strdup(user);
	else
		sess->auth_user  = NULL;
	if (password != NULL)
		sess->auth_password = _strdup(password);
	else
		sess->auth_password = NULL;
}

const char * session_get_jid(session_t sess)
{
    return sess->jid;
}

const char * session_get_password(session_t sess)
{
    return sess->password;
}

int session_connect(session_t sess, BOOL proxy_only)
{
	if(!sess->id || !sess->id->server || !sess->id->user)
	{
		return ERR_INVALIDJID;
	}

	if(sess->state != NET_OFF)
	{
		debugf("Disconnecting session");
		session_disconnect(sess, FALSE);
	}

#ifdef TODO
	char * target_server = sess->server ? sess->server : sess->id->server;

	if (proxy_type != NULL)
		iks_set_proxy(parser, proxy_type, target_server, proxy_address, proxy_port);
	if (auth_type != NULL)
		iks_set_auth(parser, auth_type, auth_current_user, auth_user, auth_password);
#endif

	/* Start background thread to dispatch events */
	sess->done = FALSE;
	sess->stopped = FALSE;
	sess->worker_thread_blocked = FALSE;

    apr_thread_create(&sess->worker, NULL, (apr_thread_start_t)worker, sess, sess->pool);
    apr_thread_detach(sess->worker);

	return ERR_SUCCESS;
}

int session_disconnect(session_t sess, BOOL wait)
{
	sess->done = TRUE;

    int count; // Timeout count. Timeout seconds = (0.2 * count)

	// We may be trying to connect, make sure to interrupt in all cases
	iks_interrupt(sess->parser);

    count = 10; // Wait for a max of 2 seconds.
    while (--count && !sess->stopped)
    {
        apr_sleep(200*1000);    // 200 ms
    }

	// Worker thread will stop when socket is closed.
	if (sess->state != NET_OFF)
	{
		iks_disconnect(sess->parser);
		sess->state = NET_OFF;
		debugf("Disconnected.");

	}
	return 0;
}

void session_change_password(session_t sess, const char *newpassword)
{
	iks *x, *y;

    // Construct an request id
    char fullrequestid[64];
    sprintf(fullrequestid, "%s", IKS_CHANGE_PASSWORD);

    // Construct and make the request
	x = iks_make_iq(IKS_TYPE_SET, IKS_NS_REGISTER);
	iks_insert_attrib(x, "to", sess->id->server);
	iks_insert_attrib(x, "id", fullrequestid);
	y = iks_find(x, "query");
	iks_insert_cdata(iks_insert(y, "password"), newpassword, 0);
	iks_send(sess->parser, x);
	iks_delete(x);
}

void query_authorize(session_t sess)
{
	iks *x, *y;

	x = iks_make_iq(IKS_TYPE_GET, IKS_NS_AUTH);
	iks_insert_attrib(x, "id", "iksauthquery");
	y = iks_find(x, "query");
	iks_insert_cdata (iks_insert (y, "username"), sess->id->user, 0);
	iks_insert_cdata (iks_insert (y, "resource"), sess->id->resource, 0);
	iks_send(sess->parser, x);
	iks_delete(x);
}

/* Send authorization message */
void send_authorize(session_t sess)
{
    char hash[41];
	iks *x;

    // Custom IIC authentication -- password is not stored in plain-text
    if (!sess->plain_text_auth  &&  strlen( sess->password ) < 40 )
    {
        session_sha_hash(sess->password, hash);
        free(sess->password);
        sess->password = _strdup(hash);
	}
	x = iks_make_auth(sess->id, sess->password, sess->plain_text_auth ? NULL : sess->stream_id);
	iks_insert_attrib(x, "id", "iksauth");
	iks_send(sess->parser, x);
	iks_delete(x);
}

/* Figure out service jid based on service name */
const char * session_get_service_jid(session_t sess, const char * service_name, char * buff)
{
    if (strchr(service_name, '@') == NULL)
    {
        sprintf(buff, "%s.service@%s", service_name, sess->id->server);
    }
    else
    {
        /* Fully qualified name, leave alone */
        strcpy(buff, service_name);
    }
	return buff;
}


/* Send registration message */
void session_send_register(session_t sess)
{
	iks *x, *y;
	x = iks_make_iq(IKS_TYPE_SET, IKS_NS_REGISTER);
	iks_insert_attrib(x, "to", sess->id->server);
	iks_insert_attrib(x, "id", "iksregister");
	y = iks_find(x, "query");
	iks_insert_cdata(iks_insert(y, "name"), sess->id->user, 0);
	iks_insert_cdata(iks_insert(y, "username"), sess->id->user, 0);
	iks_insert_cdata(iks_insert(y, "password"), sess->password, 0);
	iks_send(sess->parser, x);
	iks_delete(x);
}

int on_stream(void *udata, int type, iks *node)
{
    session_t sess = (session_t)udata;

	switch (type) {
		case IKS_NODE_START:
			if (sess->enable_security && !iks_is_secure (sess->parser)) {
				iks_start_tls (sess->parser);
				break;
			}

			query_authorize(sess);
			break;

		case IKS_NODE_NORMAL:
			if (strcmp ("stream:features", iks_name (node)) == 0) {
				sess->features = iks_stream_features (node);
			} else if (strcmp ("failure", iks_name (node)) == 0) {
				errorf ("sasl authentication failed");
			} else if (strcmp ("success", iks_name (node)) == 0) {
				sess->authorized = 1;
				iks_send_header (sess->parser, sess->id->server);
			} else {
				ikspak * pak = iks_packet(node);
				on_packet(sess, pak);
			}
			break;

		case IKS_NODE_STOP:
			errorf ("server disconnected");
			break;

		case IKS_NODE_ERROR: {
		    iks *cdata = iks_child(node);
		    if (cdata) {
                char *error = iks_cdata(cdata);
                char *start = strchr(error,'(');
                char *end = strchr(error,')');
                if ((strncmp(error, "Invalid to", 10) == 0) && start && end) {
                    sess->reported_server = strndup(start+1, end-start-1);
                    if (strcmp(sess->reported_server, sess->id->server) != 0) {
                        return ERR_WRONGSERVERNAME;
                    }
                }
		    }
			errorf ("stream error");
			break;
		}
	}

	if (node) iks_delete (node);
	return IKS_OK;
}

void on_log(void *udata, const char *data, size_t size, int is_incoming)
{
	debugf("%s Packet(%d): %.400s", is_incoming?"Incoming":"Outgoing", size, data);
}

void on_packet(session_t sess, ikspak *pak)
{
	if(!pak)
	{
		errorf("Disconnected, no packet!");
		return;
	}

    if (sess->done)
    {
        errorf("Packet received but session already disconnect");
        return;
    }

	switch(pak->type)
	{
	case IKS_PAK_MESSAGE:
        on_message_packet(sess, pak);
		break;

	case IKS_PAK_PRESENCE:
        on_presence_packet(sess, pak);
		break;

	// S10N = subscription
	case IKS_PAK_S10N:
		break;

	case IKS_PAK_IQ:
		on_iq_packet(sess, pak);
		break;

    case IKS_PAK_NONE:
        errorf("Unknown packet type");
        break;
	}

}

void on_iq_packet(session_t sess, ikspak *pak)
{
	char *pid = iks_find_attrib(pak->x, "id");

	if(iks_strcmp(pid, "iksregister") == 0)
	{
		if(pak->subtype == IKS_TYPE_RESULT)
		{
			sess->state = NET_REG;
			debugf("User account registered.");
			send_authorize(sess);
		}
		else
		{
			iks *x;
			x = iks_find(pak->x, "error");
			errorf("Registration failed.");
			sess->on_error(sess->on_error_data, ERR_REGFAILED, "New user registration failed", 0, "", "");
		}
    }
	if(iks_strcmp(pid, "iksauthquery") == 0)
	{
		if(pak->subtype == IKS_TYPE_RESULT)
		{
            iks *query = iks_find(pak->x, "query");
		    if (query && iks_find(query, "digest") != NULL)
		    {
                sess->plain_text_auth = FALSE;
                send_authorize(sess);
		    }
		    else if (query && iks_find(query, "password") != NULL)
		    {
		        sess->plain_text_auth = TRUE;
                send_authorize(sess);
		    }
		    else
            {
       			errorf("No supported authentication methods found.");
                sess->on_error(sess->on_error_data, ERR_REGFAILED, "No supported authentication methods", 0, "", "");
            }
		}
		else
		{
			errorf("Query supported authentication methods failed.");
			sess->on_error(sess->on_error_data, ERR_AUTHFAILED, "Authorization failed.", 0, "", "");
		}
	}
	else if (iks_strncmp(pid, IKS_CHANGE_PASSWORD, IKS_CHANGE_PASSWORD_LEN) == 0)
	{
        if (pak->subtype == IKS_TYPE_RESULT)
		{
			debugf("User password change succeeded.");
			sess->on_password_changed(sess->on_password_changed_data);
		}
		else
		{
			errorf("User password change failed.");
			sess->on_error(sess->on_error_data, ERR_CHANGEPASSWORDFAILED, "Unable to change user's password", DefaultErrorCode, pid, "");
		}
	}
	else if(iks_strcmp(pid, "iksauth") == 0)
	{
		if(pak->subtype == IKS_TYPE_RESULT)
		{
			sess->state = NET_ON;
			debugf("Authorized.");
			// session_flush(sess); // testing flush
			sess->on_connected(sess->on_connected_data);
		}
		else
		{
			iks *x;
			errorf("Authorization failed.");
			x = iks_find(pak->x, "error");
			if(x)
			{
				errorf("Authorization error: %s", iks_cdata(iks_child(x)));
			}
			sess->on_error(sess->on_error_data, ERR_AUTHFAILED, "Authorization failed.", 0, "", "");
		}
	}
	else if (iks_strcmp(pak->ns, RPC_RPC_NS) == 0)
	{
        if (pak->subtype == IKS_TYPE_RESULT)
		{
			on_rpc_result(sess, pak);
		}
		else if (pak->subtype == IKS_TYPE_SET)
		{
			on_rpc_request(sess, pak);
		}
		else
		{
			on_rpc_error(sess, pak);
		}
	}

#ifdef TODO
	else if(iks_strcmp(pak->ns, IKS_NS_VERSION) == 0)
	{
		if(pak->subtype == IKS_TYPE_GET)
		{
			send_version(pak->from);
		}
        else if (pak->subtype == IKS_TYPE_RESULT)
        {
            version_parse(pak);
        }
        else if (pak->subtype == IKS_TYPE_ERROR)
        {
            version_error(pak);
        }
	}

	else if(iks_strcmp(iks_find_attrib(iks_child(pak->x), "xmlns"), IKS_NS_BROWSE) == 0)
	{
		browse_parse(pak);
	}

	else if(iks_strcmp(pak->ns, IKS_NS_SEARCH) == 0)
	{
        if (pak->subtype == IKS_TYPE_RESULT)
        {
			search_parse(pak);
        }
        else if (pak->subtype == IKS_TYPE_ERROR)
        {
			search_error(pak);
        }
	}

	else if(iks_strcmp(pak->ns, IKS_NS_ROSTER) == 0)
	{
		roster_update(pak);
	}

	else if(iks_strcmp(pak->ns, IKS_NS_TIME) == 0)
	{
		if(pak->subtype == IKS_TYPE_GET)
			send_time(pak->from);
        else if (pak->subtype == IKS_TYPE_RESULT)
            time_parse(pak);
	}

	else if(iks_strcmp(iks_find_attrib(iks_child(pak->x), "xmlns"), IKS_NS_PRIVATE) == 0)
	{
		parse_options_from_server(pak);
	}
#endif

}

void on_rpc_result(session_t sess, ikspak *pak)
{
	iks *query;
	iks *result;
	//ikstr *str;
	char *response;
    xmlrpc_value *retval = NULL;
    xmlrpc_env env;

    xmlrpc_env_init(&env);

	query = iks_find(pak->x, "query");
	result = iks_child(query);
	while (result != NULL && iks_type(result) != IKS_TAG)
		result = iks_next(result);

	if (result == NULL)
	{
		debugf("Empty result received");
		return;
	}

	response = iks_string(NULL, result);

    retval = xmlrpc_parse_response(&env, response, strlen(response));
    XMLRPC_FAIL_IF_FAULT(&env);

	// send result along...
    resultset_t rs = resultset_create(&env, retval);
    const char *rpcid = iks_find_attrib(pak->x, "id");
    void *udata;

    debugf("RPC result %s", rpcid);

    session_rpc_result_handler handler = find_rpc_result_handler(sess, rpcid, &udata);
    if (handler)
        handler(udata, rpcid, rs);

 cleanup:
    if (env.fault_occurred)
	{
		char buffer[512];
		buffer[0] = '\0';
		if (env.fault_string)
			strncat(buffer, env.fault_string, sizeof(buffer)-1);
		else
			strcat(buffer, "Unknown server error");
		
		// Treat error as server disconnect, as server doesn't recognize our session id.	
        if (env.fault_code == InvalidSessionID)
        {
            sess->on_error(sess->on_error_data, ERR_CONNECTLOST, "Invalid server session.", 0, "", "");
        }
        else
        {
            // Lookup normal handler and dispatch
            const char *rpcid = iks_find_attrib(pak->x, "id");
            void *udata;
            session_rpc_error_handler handler = find_rpc_error_handler(sess, rpcid, &udata);
            if (handler)
                handler(udata, rpcid, env.fault_code, buffer);
            else
                sess->on_error(sess->on_error_data, ERR_RPCFAILED, buffer, env.fault_code, rpcid, "");
        }
    }
	if (retval)
		xmlrpc_DECREF(retval);
    xmlrpc_env_clean(&env);
	iks_free(response);
}

// Server sends us info using RPC requests.  We don't respond
// to these calls.
void on_rpc_request(session_t sess, ikspak *pak)
{
	iks *query;
	iks *result;
	//ikstr *str;
	char *response;
    xmlrpc_value *retval = NULL;
	const char *method = NULL;
    const char *rpcid = NULL;
    xmlrpc_env env;
    BOOL handled = FALSE;

    xmlrpc_env_init(&env);

	query = iks_find(pak->x, "query");
	result = iks_child(query);
	while (result != NULL && iks_type(result) != IKS_TAG)
		result = iks_next(result);

    rpcid = iks_find_attrib(pak->x, "id");

	if (result == NULL)
	{
		debugf("Empty result received");
		return;
	}

    debugf("RPC event %s", rpcid);

	response = iks_string(NULL, result);

	xmlrpc_parse_call(&env, response, strlen(response), &method, &retval);
    XMLRPC_FAIL_IF_FAULT(&env);

	// send result along...
	{
	    resultset_t rs = resultset_create(&env, retval);
	    void *user;

        if (strcmp(method, "error") == 0)
        {
            char *type, *msg;
            int status;

            debugf("error result received");

            // will get "join", "call", "im", or "logout"
            // handle "logout" here, perhaps hand remainder off to controller handler
            resultset_get_output_values(rs, "(sis*)", &type, &status, &msg);
            if( strcmp( type, "logout")  ==  0 )
            {
                sess->on_error(sess->on_error_data, ERR_SIGNEDONELSEWHERE, msg, 0, "", "");
                handled = TRUE;
            }
            else if( strcmp( type, "ping")  ==  0 )
            {
                sess->on_error(sess->on_error_data, ERR_PINGFAILED, msg, 0, "", "");
                handled = TRUE;
            }
        }

        if (!handled)
        {
            session_rpc_request_handler handler = find_rpc_request_handler(sess, method, &user);
            if (handler)
                handler(user, rpcid, method, rs);
        }
	}

 cleanup:
    if (env.fault_occurred)
	{
		char buffer[512];
		buffer[0] = '\0';
		if (env.fault_string)
			strncat(buffer, env.fault_string, sizeof(buffer)-1);
		else
			strcat(buffer, "Unknown server error");

        void *udata;
        session_rpc_error_handler handler = find_rpc_error_handler(sess, rpcid, &udata);
        if (handler)
            handler(udata, rpcid, env.fault_code, buffer);
    }
	if (retval)
		xmlrpc_DECREF(retval);
	if (method)
		xmlrpc_strfree(method);
    xmlrpc_env_clean(&env);
	iks_free(response);
}

void on_rpc_error(session_t sess, ikspak *pak)
{
	iks *x;
	x = iks_find(pak->x, "error");
	if(x)
	{
        const char *msg = iks_cdata(iks_child(x));
	    const char *rpcid = iks_find_attrib(pak->x, "id");
	    void *udata;

        debugf("RPC error %s", rpcid);

        session_rpc_error_handler handler = find_rpc_error_handler(sess, rpcid, &udata);
        if (handler)
            handler(udata, rpcid, DefaultErrorCode, msg);
	}
}

void on_im_message(session_t sess, ikspak *pak)
{
    BOOL bComposing = FALSE;
    iks *x = iks_find_with_attrib(pak->x, "x", "xmlns", IKS_NS_EVENT);

    if (NULL != x)
    {
        iks *y = iks_find(x, "composing");
        bComposing = y != NULL;
    }

    char *body = iks_find_cdata(pak->x, "body");

    messaging_im_t im;
    apr_pool_t *newpool;

    apr_pool_create(&newpool, NULL);
    im = apr_pcalloc(newpool, sizeof(struct messaging_im_st));
    im->pool = newpool;

    im->user = apr_pstrdup(newpool, pak->from->user);
    im->server = apr_pstrdup(newpool, pak->from->server);
    im->resource = apr_pstrdup(newpool, pak->from->resource);
    im->body = body ? apr_pstrdup(newpool, body) : NULL;
    im->composing = bComposing;

    sess->on_im(sess->on_im_data, im);
}

void on_message_packet(session_t sess, ikspak *pak)
{
	if(pak->subtype == IKS_TYPE_ERROR)
	{
        //ProcessMessageError(pak);
    }
    else if(pak->subtype == IKS_TYPE_GROUPCHAT)
	{
        //ProcessGroupChat(pak);
	}
	else if(pak->subtype == IKS_TYPE_CHAT)
	{
        on_im_message(sess, pak);
	}
	else if(pak->subtype == IKS_TYPE_NONE)
	{
        //ProcessChatInvitation(pak);
	}
	else if(pak->subtype == IKS_TYPE_HEADLINE)
	{
	}
}

void on_presence_packet(session_t sess, ikspak *pak)
{
    int presenceId = PRES_UNAVAILABLE;

	if(pak->subtype == IKS_TYPE_ERROR)
	{
		return;
	}

    switch(pak->show)
	{
	    case IKS_SHOW_UNAVAILABLE:
        {
            presenceId = PRES_UNAVAILABLE;
            break;
        }
	    case IKS_SHOW_AVAILABLE:
        {
            presenceId = PRES_AVAILABLE;
            break;
        }
        case IKS_SHOW_AWAY:
        {
            presenceId = PRES_AWAY;
            break;
        }
        case IKS_SHOW_XA:
        {
            presenceId = PRES_AWAY;
            break;
        }
        case IKS_SHOW_DND:
        {
            presenceId = PRES_AWAY;
            break;
        }
        case IKS_SHOW_CHAT:
        {
            presenceId = PRES_AVAILABLE;
            break;
        }
        default:
        {
            presenceId = PRES_UNAVAILABLE;
        }
    }

    messaging_presence_t pres;
    apr_pool_t *newpool;
    const char *status = iks_find_cdata(pak->x, "status");

    apr_pool_create(&newpool, NULL);
    pres = apr_pcalloc(newpool, sizeof(struct messaging_presence_st));
    pres->pool = newpool;

    pres->user = apr_pstrdup(newpool, pak->from->user);
    pres->server = apr_pstrdup(newpool, pak->from->server);
    pres->resource = apr_pstrdup(newpool, pak->from->resource);
    pres->presence = presenceId;
    pres->status = status ? apr_pstrdup(newpool, status) : NULL;

    sess->on_presence(sess->on_presence_data, pres);
}

// Check for network activity...callback handlers will be called to handle any data.
void * worker(apr_thread_t *thread, void *udata)
{
    session_t s = (session_t)udata;
	char * target_server = s->server ? s->server : s->id->server;
    int err;
    int start;

	debugf("[%8X] Connecting to '%s'...", (unsigned)s, target_server);

	err = iks_connect_netlib(s->parser, s->id->server, target_server, s->port, FALSE /*proxy_only*/);
	if(err == IKS_PROXYAUTHREQUIRED)
	{
		errorf("Cannot connect to %s -- %s", target_server, "Proxy required authentication.");
		s->on_error(s->on_error_data, ERR_PROXYAUTHREQUIRED, "Proxy required authentication.", 0 , "", "");
		return NULL;
	}
	else if(err == IKS_PROXYAUTHFAILED)
	{
		errorf("Cannot connect to %s -- %s", target_server, "Proxy authentication failed.");
		s->on_error(s->on_error_data, ERR_PROXYAUTHFAILED, "Proxy authentication failed.", 0 , "", "");
		return NULL;
	}
	else if(err != IKS_OK)
	{
		errorf("Cannot connect to %s (%d)", target_server, err);
		s->on_error(s->on_error_data, ERR_CONNECTFAILED, "Unable to connect to server.", 0 , "", "");
		return NULL;
	}

	s->state = NET_CONNECT;
	debugf("[%8X] Connected, starting poll.", (unsigned)s);

	start = time(NULL);
	while (!s->done)
	{
		err = iks_recv(s->parser, 1);

        if (s->done)
            break;

		if (s->state <= NET_STREAM && err == IKS_NO_DATA)
		{
			if (time(NULL) - start < CONNECT_TIMEOUT)
				continue;

            if (!s->done)
            {
                errorf("Timeout while connecting.");
                s->on_error(s->on_error_data, ERR_CONNECTLOST, "Unable to connect to server.", 0, "", "");
            }
			break;
		}
		else if (err == IKS_TIMEOUT)
		{
			s->on_error(s->on_error_data, ERR_CONNECTLOST, "Unable to connect to server.", 0, "", "");
			errorf("Connection to server not completed.");
			break;
		}
		else if (err == IKS_NET_TLSFAIL)
		{
			s->on_error(s->on_error_data, ERR_SSLTLS_HANDSHAKE, "SSL/TLS handshake failed.", 0, "", "");
			break;
		}
		else if (err == IKS_NO_CERTIFICATE_FOUND)
		{
			s->on_error(s->on_error_data, ERR_NO_CERTIFICATE_FOUND, "Server did not send any certificate.", 0, "", "");
			break;
		}
		else if (err == IKS_UNABLE_VERIFY_CERTIFICATE)
		{
			s->on_error(s->on_error_data, ERR_UNABLE_VERIFY_CERTIFICATE, "Unable to verify server's certificate.", 0, "", "");
			break;
		}
		else if (err == IKS_CERT_SIGNER_NOT_FOUND)
        {
            char    *pBuf;
            int     iLen    = iks_get_new_cert_len( );
            if( iLen  >  0 )
            {
                int     iCopied;
                pBuf = malloc( iLen + 1 );
                iCopied = iks_get_new_cert( iLen + 1, pBuf );
                if( iCopied  ==  iLen )
                {
                    s->on_error(s->on_error_data, ERR_CERT_SIGNER_NOT_FOUND, "Server's certificate issuer is unknown.", 0, "", pBuf );
                    break;
                }
            }
            s->on_error(s->on_error_data, ERR_CERT_SIGNER_NOT_FOUND, "Server's certificate issuer is unknown.", 0, "", "" );
            break;
		}
		else if (err == IKS_CERT_INVALID)
		{
			s->on_error(s->on_error_data, ERR_CERT_INVALID, "Server's certificate is NOT trusted.", 0, "", "");
			break;
		}
		else if (err == ERR_WRONGSERVERNAME)
		{
            s->on_error(s->on_error_data, ERR_WRONGSERVERNAME, "Wrong server name.", 0, "", s->reported_server);
            errorf("Wrong server name used, should be %s", s->reported_server);
            break;
		}
		else if ((err != IKS_OK && err != IKS_NO_DATA) && !s->done)
		{
		    if ( !s->done )
		    {
                s->on_error(s->on_error_data, ERR_CONNECTLOST, "Connection lost.", 0, "", "");
                errorf("[%8X] Error monitoring socket [%d]", (unsigned)s, errno);
		    }
			break;
		}
	}

    iks_disconnect(s->parser);
    
	debugf("[%8X] Session worker stopping.", (unsigned)s);
	s->stopped = TRUE;

    apr_thread_exit(thread, 0);
	return NULL;
}

int session_make_rpc_call(session_t sess, const char *id, const char *service, const char *method, const char *format, ...)
{
	char buff[256];
	int err = 0;
    va_list args;
    xmlrpc_env env;
    const char *suffix;
    xmlrpc_value *arg_array = NULL;
	xmlrpc_mem_block *req = NULL;
	iks *iq = NULL, *query, *rpc;
	iksparser *prs = NULL;

	if (sess->state != NET_ON)
	{
		errorf("Attempted RPC call when not connected");
		return ERR_NOTCONNECTED;
	}


    xmlrpc_env_init(&env);

    XMLRPC_ASSERT_PTR_OK(format);

    /* Build our argument array. */
    va_start(args, format);
    xmlrpc_build_value_va(&env, (char *)(format), args, &arg_array, &suffix);
    va_end(args);
    XMLRPC_FAIL_IF_FAULT(&env);

	/* Serialize parameters */
    req = xmlrpc_mem_block_new(&env, 0);
    XMLRPC_FAIL_IF_FAULT(&env);
    xmlrpc_serialize_call(&env, req, (char *)(method), arg_array);
    XMLRPC_FAIL_IF_FAULT(&env);

	/* Make packet */
	iq = iks_make_iq(IKS_TYPE_SET, RPC_RPC_NS);
	iks_insert_attrib(iq, "id", id);
	iks_insert_attrib(iq, "to", session_get_service_jid(sess, service, buff));

	prs = iks_dom_new(&rpc);
	if (prs == NULL)
	{
		err = ERR_NOMEMORY;
		goto cleanup;
	}

	if (iks_parse(prs, (char*)xmlrpc_mem_block_contents(req), xmlrpc_mem_block_size(req), 1) != IKS_OK)
	{
		err = ERR_RPCFAILED;
		goto cleanup;
	}

	query = iks_find(iq, "query");
	iks_insert_node(query, rpc);

	/* Send it */
    if (iks_send(sess->parser, iq)!=IKS_OK)
    {
        err = ERR_CONNECTLOST;
    }

    debugf("RPC request %s [%d]", id, err);

cleanup:
    if (env.fault_occurred)
	{
		err = ERR_RPCFAILED;
    }
    if (req)
		xmlrpc_mem_block_free(req);
	if (arg_array)
		xmlrpc_DECREF(arg_array);
    xmlrpc_env_clean(&env);
	if (iq)
		iks_delete(iq);
	if (prs)
	{
		iks_parser_delete(prs);
	}

	return err;
}

BOOL session_make_iq_call(session_t sess, enum iksubtype type, char *xmlns, char *to, char *request)
{
	BOOL res = TRUE;
	iks *iq;
	iksparser *prs = NULL;
	iks *query, *rq;

	iq = iks_make_iq(type, xmlns);
	iks_insert_attrib(iq, "to", to);

	prs = iks_dom_new(&rq);
	if (prs == NULL)
	{
		res = FALSE;
		goto done;
	}

	if (iks_parse(prs, request, strlen(request), 1) != IKS_OK)
	{
		res = FALSE;
		goto done;
	}

	query = iks_find(iq, "query");
	iks_insert_node(query, rq);

    iks_send(sess->parser, iq);

done:
    iks_delete(iq);
	if (prs != NULL)
		iks_parser_delete(prs);
	return res;
}

void session_send_im(session_t sess, const char *from, const char *to, const char *body, BOOL composing)
{
    iks *x;
    iks *y;
    iks *z;

    char jid_to[512];

    if (strchr(to, '@') == NULL)
    {
        sprintf(jid_to, "%s@%s", to, sess->id->server);
    }
    else
    {
        strcpy(jid_to, to);
    }

    x = iks_new("message");
    iks_insert_attrib(x, "to", jid_to);
    iks_insert_attrib(x, "type", "chat");

    if (body)
    {
	    y = iks_insert(x, "body");
	    iks_insert_cdata(y, body, 0);
    }

    z = iks_insert(x, "x");
    iks_insert_attrib(z, "xmlns", IKS_NS_EVENT);

    if (composing)
    {
        iks_insert(z, "composing");
    }

	iks_send(sess->parser, x);
	iks_delete(x);
}

void session_send_invite(session_t sess, const char *room, const char *to, const char *body)
{
    iks *x;
    iks *y;
    iks *z;

    x = iks_new("message");
    iks_insert_attrib(x, "to", to);

    y = iks_insert(x, "x");
    iks_insert_attrib(y, "xmlns", "jabber:x:conference");
    iks_insert_attrib(y, "jid", room);  // ToDo: make jid

	z = iks_insert(x, "body");
	iks_insert_cdata(z, body, 0);

	iks_send(sess->parser, x);
	iks_delete(x);
}

void session_send_presence(session_t sess, char *to, int presence, char *status)
{
    int show;

    if (sess == NULL || sess->state != NET_ON)
    {
        errorf("Attempted to send presence when session not connected, ignored");
        return;
    }

    switch (presence)
    {
        case PRES_AVAILABLE:
            show = IKS_SHOW_AVAILABLE;
            break;
        case PRES_AWAY:
            show = IKS_SHOW_AWAY;
            break;
        case PRES_UNAVAILABLE:
        default:
            show = IKS_SHOW_UNAVAILABLE;
            break;
    }

    iks *x = iks_make_pres(show, status);
	if(to) iks_insert_attrib(x, "to", to);
    iks_send(sess->parser, x);
    iks_delete(x);
}

session_array_t session_create_array(int count, const char **values)
{
    xmlrpc_env env;
	xmlrpc_value *array = NULL;
	xmlrpc_value *member = NULL;
	int i;

    xmlrpc_env_init(&env);

	array = xmlrpc_build_value(&env, "()");
    XMLRPC_FAIL_IF_FAULT(&env);

	for (i=0; (count > 0 && i < count) || (count == -1 && values[i]) ; i++)
    {
        if( values[i]  ==  NULL )
            continue;

        member = xmlrpc_build_value(&env, "s", values[i]);
		XMLRPC_FAIL_IF_FAULT(&env);

		xmlrpc_array_append_item(&env, array, member);
		XMLRPC_FAIL_IF_FAULT(&env);

		xmlrpc_DECREF(member);
		member = NULL;
	}

cleanup:
	if (env.fault_occurred)
	{
		if (array != NULL)
			xmlrpc_DECREF(array);
		array = NULL;

		if (member != NULL)
			xmlrpc_DECREF(member);
	}

	return (session_array_t) array;
}

void session_add_array_element(session_array_t _array, const char *format, ...)
{
    va_list args;
    xmlrpc_env env;
    const char *suffix = NULL;
	xmlrpc_value *array = (xmlrpc_value *)_array;
	xmlrpc_value *member = NULL;

    xmlrpc_env_init(&env);

    XMLRPC_ASSERT_PTR_OK(format);

    /* Add new array element */
    va_start(args, format);
    xmlrpc_build_value_va(&env, (char *)(format), args, &member, &suffix);
    va_end(args);
    XMLRPC_FAIL_IF_FAULT(&env);

	xmlrpc_array_append_item(&env, array, member);
	XMLRPC_FAIL_IF_FAULT(&env);

	xmlrpc_DECREF(member);
	member = NULL;

cleanup:
	if (env.fault_occurred)
	{
		if (member != NULL)
			xmlrpc_DECREF(member);
	}
}

void session_free_array(session_array_t array)
{
	if (array != NULL)
		xmlrpc_DECREF(array);
}

void session_sha_hash(const char * str, char * hash)
{
	iks_sha(str, hash);
}


//
// Manage RPC result & request handlers
//

void session_set_rpc_handlers(session_t session, void *user, const char *rpcid, session_rpc_result_handler result, session_rpc_error_handler error)
{
    struct rpc_handler_st * handler = (struct rpc_handler_st *) apr_hash_get(session->rpc_handlers, rpcid, APR_HASH_KEY_STRING);
    if (handler == NULL)
    {
        handler = (struct rpc_handler_st *) apr_pcalloc(session->pool, sizeof(struct rpc_handler_st));
        apr_hash_set(session->rpc_handlers, rpcid, APR_HASH_KEY_STRING, handler);
    }
    handler->user = user;
    handler->result_handler = result;
    handler->error_handler = error;

}

void session_set_rpc_request_handler(session_t session, void *user, const char *method, session_rpc_request_handler func)
{
    struct rpc_method_st * handler = (struct rpc_method_st *) apr_hash_get(session->method_handlers, method, APR_HASH_KEY_STRING);
    if (handler == NULL)
    {
        handler = (struct rpc_method_st *) apr_pcalloc(session->pool, sizeof(struct rpc_method_st));
        apr_hash_set(session->method_handlers, method, APR_HASH_KEY_STRING, handler);
    }
    handler->user = user;
    handler->request_handler = func;
}

static session_rpc_result_handler find_rpc_result_handler(session_t sess, const char *rpc_id, void ** user)
{
    int num_chars;
    struct rpc_handler_st * handler;
    char *p = strchr(rpc_id, ':');
    num_chars = p ? p - rpc_id : strlen(rpc_id);

    handler = (struct rpc_handler_st *) apr_hash_get(sess->rpc_handlers, rpc_id, num_chars);
    if (handler == NULL)
    {
        errorf("No handler found for rpc result id %s", rpc_id);
        return NULL;
    }
    *user = handler->user;
    return handler->result_handler;
}

static session_rpc_error_handler find_rpc_error_handler(session_t sess, const char *rpc_id, void ** user)
{
    int num_chars;
    struct rpc_handler_st * handler;
    char *p = strchr(rpc_id, ':');
    num_chars = p ? p - rpc_id : strlen(rpc_id);

    handler = (struct rpc_handler_st *) apr_hash_get(sess->rpc_handlers, rpc_id, num_chars);
    if (handler == NULL)
    {
        errorf("No handler found for rpc error id %s", rpc_id);
        return NULL;
    }
    *user = handler->user;
    return handler->error_handler;
}

static session_rpc_request_handler find_rpc_request_handler(session_t sess, const char *method, void ** user)
{
    struct rpc_method_st * handler;

    handler = (struct rpc_method_st *) apr_hash_get(sess->method_handlers, method, APR_HASH_KEY_STRING);
    if (handler == NULL)
    {
        errorf("No handler found for rpc method %s", method);
        return NULL;
    }
    *user = handler->user;
    return handler->request_handler;
}

int cmp_pstring( pstring a, pstring b )
{
    if( !a || !b )
    {
        if( !a && !b )
            return 0;
        else if( !a )
        {
            return (*b) ? 0:1;
        }else //if( !b )
        {
            return (*a) ? 0:1;
        }
    }
    return apr_strnatcmp( a, b );
}

void session_add_message_handler(session_t sess, void *user, session_message_handler _handler)
{
    struct message_handler_st *new_handler = apr_array_push(sess->message_handlers);
    new_handler->user = user;
    new_handler->handler = _handler;
}

void session_remove_message_handler(session_t sess, void *user, session_message_handler _handler)
{
    struct message_handler_st *p;
    int i;

    for (i=0; i<sess->message_handlers->nelts; ++i)
    {
        p = get_element_at(sess->message_handlers, i);
        if (p->user == user && p->handler == _handler)
        {
            remove_element_at(sess->message_handlers, i);
            break;
        }
    }
}

void session_send_message(session_t sess, session_message_t msg)
{
    struct message_handler_st *p;
    int i;

    for (i=0; i<sess->message_handlers->nelts; ++i)
    {
        p = get_element_at(sess->message_handlers, i);
        p->handler(p->user, msg);
    }
}

void messaging_presence_destroy(messaging_presence_t pres)
{
    apr_pool_destroy(pres->pool);
}

void messaging_im_destroy(messaging_im_t im)
{
    apr_pool_destroy(im->pool);
}
