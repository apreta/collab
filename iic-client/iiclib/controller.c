/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef _WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>

#include <apr.h>
#include <apr_general.h>
#include <apr_pools.h>
#include <apr_strings.h>
#include <apr_hash.h>
#include <apr_tables.h>

#define NO_DEFINE_APR
#include "controller.h"
#include "handlerdefs.h"
#include "utils.h"

#include "services/common/common.h"
#include "services/common/address.h"

#define ssize_t int
#include <gnutls/gnutls.h>
#include <gcrypt.h>

#include <netlib/log.h>


// Private controller session info.
struct controller_st
{
    session_t session;
    auth_info_t auth;
    meeting_details_t meeting;

    const char *username;       // jabberid minus '/resource' part
    const char *user_address;   // iic:<jabberid>
    const char *session_id;     // returned by controller
    const char *user_id;
    int is_telephony_avail;
    const char *default_doc_server;
    int state;

    DECLARE_EVENT_HANDLER(controller,registered)
    DECLARE_EVENT_HANDLER(controller,invite_lookup)
    DECLARE_EVENT_HANDLER(controller,meeting_event)
    DECLARE_EVENT_HANDLER(controller,meeting_chat)
    DECLARE_EVENT_HANDLER(controller,meeting_error)
    DECLARE_EVENT_HANDLER(controller,submeeting_add)
    DECLARE_EVENT_HANDLER(controller,invite)
    DECLARE_EVENT_HANDLER(controller,get_params)
    DECLARE_EVENT_HANDLER(controller,docshare_event)
    DECLARE_EVENT_HANDLER(controller,im_notify)
    DECLARE_EVENT_HANDLER(controller,ping)

    apr_pool_t *pool;
    apr_hash_t *meetings;
};

// Keep track of active meeting
struct active_meeting_st
{
    pstring     meeting_id;

    apr_pool_t *pool;       // pool for this struct

    meeting_t   info;

    appshare_info_t  appshare_info;
    pstring     docshare_address;

    apr_array_header_t *im_notifies;
};


//
// Set up mapping from RPC IDs and RPC method names to handler functions.
// Tables are defined at end of file.
//

// RPC result and error messages
struct rpc_handler_st
{
    const char * rpc_id;
    session_rpc_result_handler result_handler;
    session_rpc_error_handler error_handler;
};
extern struct rpc_handler_st c_handler_list[];    // Wanted this to be static but VC++ won't allow it

// RPC request messages
struct rpc_method_st
{
    const char * method;
    session_rpc_request_handler request_handler;
};
extern struct rpc_method_st c_method_list[];


//
// Imported function defs
//

meeting_details_t make_meeting_details(apr_pool_t *pool);
meeting_details_t parse_meeting_details(apr_pool_t *pool, resultset_t data);


//
// Local function defs.
//

static active_meeting_t find_meeting(controller_t cnt, const char * id, BOOL create);
static void dispatch_event(controller_t cnt, active_meeting_t active, int event, const char *meeting_id, const char *sub_id, const char *part_id);
static meeting_t make_info(controller_t cnt, active_meeting_t active, BOOL clear);
static void remove_participant(meeting_t info, const char *partid);
static void remove_submeeting(meeting_t info, const char *subid);
static void mark_invalid(meeting_t info);
static void remove_invalid(meeting_t mtg);
static void copy_meeting_to_details(controller_t cnt, meeting_details_t mtg, meeting_t info);
static void copy_participant_to_invitee(meeting_details_t mtg, participant_t participant, invitee_t invt);
static contact_t find_contact(session_t sess, const char * screenname);
static invitee_t find_by_screenname(meeting_details_t mtg, const char *screen);
static void on_server_doc_share(void *user, const char * rpc_id, const char *method, resultset_t data);


//
// Declare APIs to set event callbacks
//

void default_on_registered(void *u) { }
IMPL_SET_HANDLER(controller,registered)
void default_on_invite_lookup(void *u, meeting_invite_t d) { }
IMPL_SET_HANDLER(controller,invite_lookup);
void default_on_meeting_event(void *u, meeting_event_t d) { }
IMPL_SET_HANDLER(controller,meeting_event);
void default_on_meeting_error(void *u, meeting_error_t d) { }
IMPL_SET_HANDLER(controller,meeting_error);
void default_on_meeting_chat(void *u, meeting_chat_t d) { }
IMPL_SET_HANDLER(controller,meeting_chat);
void default_on_submeeting_add(void *u, const char *opid, const char *subid) { }
IMPL_SET_HANDLER(controller,submeeting_add);
void default_on_invite(void *u, meeting_invite_t d) { }
IMPL_SET_HANDLER(controller,invite);
void default_on_get_params(void *user, meeting_details_t mtg, int forum_options) { }
IMPL_SET_HANDLER(controller,get_params);
void default_on_docshare_event(void *u, docshare_event_t d) { }
IMPL_SET_HANDLER(controller,docshare_event);
void default_on_im_notify(void *u, const char *meeting_id, participant_t p) { }
IMPL_SET_HANDLER(controller,im_notify);
void default_on_ping(void *u) { }
IMPL_SET_HANDLER(controller,ping)


int controller_initialize()
{
    return 0;
}

static void on_session_message(void *user, session_message_t msg)
{
    controller_t controller = (controller_t)user;

    if (strcmp(msg->id, "session-reset") == 0)
    {
        apr_pool_clear(controller->pool);
        controller->meetings = apr_hash_make(controller->pool);
    }
}

controller_t controller_create(session_t sess)
{
    controller_t controller = (controller_t)calloc(1,sizeof(struct controller_st));
    int i;

    controller->session = sess;

    controller->on_registered = default_on_registered;
    controller->on_invite_lookup = default_on_invite_lookup;
    controller->on_meeting_event = default_on_meeting_event;
    controller->on_meeting_chat = default_on_meeting_chat;
    controller->on_meeting_error = default_on_meeting_error;
    controller->on_submeeting_add = default_on_submeeting_add;
    controller->on_invite = default_on_invite;
    controller->on_get_params = default_on_get_params;
    controller->on_docshare_event = default_on_docshare_event;
    controller->on_im_notify = default_on_im_notify;
    controller->on_ping = default_on_ping;

    apr_pool_create(&controller->pool, NULL);

    controller->meetings = apr_hash_make(controller->pool);

    i = -1;
    while (c_handler_list[++i].rpc_id)
    {
        session_set_rpc_handlers(sess, controller, c_handler_list[i].rpc_id, c_handler_list[i].result_handler, c_handler_list[i].error_handler);
    }

    i = -1;
    while (c_method_list[++i].method)
    {
        session_set_rpc_request_handler(sess, controller, c_method_list[i].method, c_method_list[i].request_handler);
    }

    session_add_message_handler(sess, controller, on_session_message);

    return controller;
}

void controller_destroy(controller_t cnt)
{
    session_remove_message_handler(cnt->session, cnt, on_session_message);
    apr_pool_destroy(cnt->pool);
    free(cnt);
}

int controller_register(controller_t cnt, auth_info_t auth_info, BOOL retry)
{
    const char *rpc_id = retry ? "rpc_controllauthretry" : "rpc_controllerauth";
    const char *jid = session_get_jid(cnt->session);
    char * controller_sessionid;
    char * username = apr_pstrdup(cnt->pool, jid);
    char * last = NULL;

    cnt->auth = auth_info;
    cnt->user_id = apr_pstrdup(cnt->pool, auth_info->user_id);

    // Get username from jid (remove resoure)
    cnt->username = apr_strtok(username, "/", &last);

    // Get the user's jabber id and construct a session id
    // for the controller.
    cnt->user_address = apr_psprintf(cnt->pool, "iic:%s", jid);

    // Still gotta fix this one, session id should be dynamic
    controller_sessionid = apr_psprintf(cnt->pool, "user:%s", auth_info->user_id);

    // Make the call
    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
    else if (session_make_rpc_call(
                             cnt->session,
                             rpc_id,
                             "controller",
                             "controller.register_user",
                             "(ssssss)",
                             controller_sessionid,
                             cnt->username,
                             auth_info->display_name,
                             cnt->user_address,
                             "",
                             ""))
	{
		debugf("Controller authorization call failed");
        return ERR_RPCFAILED;
	}

    return 0;
}

int controller_register_anonymous(controller_t cnt, const char *pin, const char *display_name)
{
    const char *rpc_id = "rpc_controllerauth";
    const char *jid = session_get_jid(cnt->session);
    char * controller_sessionid;

    // Get username from jid (leave resource for anon)
    cnt->username = apr_pstrdup(cnt->pool, jid);

    // Get the user's jabber id and construct a session id
    // for the controller.
    cnt->user_address = apr_psprintf(cnt->pool, "iic:%s", jid);

    // Still gotta fix this one, session id should be dynamic
    controller_sessionid = apr_psprintf(cnt->pool, "pin:%s", pin);

    cnt->user_id = apr_pstrdup(cnt->pool, controller_sessionid);

    // Make the call
    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
    else if (session_make_rpc_call(
                             cnt->session,
                             rpc_id,
                             "controller",
                             "controller.register_user",
                             "(ssssss)",
                             controller_sessionid,
                             cnt->username,
                             display_name,
                             cnt->user_address,
                             "",
                             ""))
	{
		debugf("Controller authorization call failed");
        return ERR_RPCFAILED;
	}

    return 0;
}

void on_controller_registered(void *user, const char * rpc_id, resultset_t data)
{
    controller_t cnt = (controller_t)user;
	char *_sid;

	resultset_get_output_values(data, "(s*)", &_sid);

    cnt->session_id = apr_pstrdup(cnt->pool, _sid);

    if (session_make_rpc_call(
                         cnt->session,
                         "rpc_checktelephony",
                         "controller",
                        "controller.check_telephony_avail",
                        "(s)",
                         cnt->username))
    {
		debugf("Controller check telephony call failed");
        cnt->on_registered(cnt->on_registered_data);
    }
}

void on_controller_check_telephone(void *user, const char * rpc_id, resultset_t data)
{
    controller_t cnt = (controller_t)user;
  	resultset_get_output_values(data, "(i)", &cnt->is_telephony_avail);

    if (session_make_rpc_call(
                         cnt->session,
                         "rpc_getdocserver",
                         "controller",
                         "controller.get_doc_share_server",
                         "(ss)",
                         cnt->session_id,
                         ""))
    {
		debugf("Controller get doc server call failed");
        cnt->on_registered(cnt->on_registered_data);
    }
}

void on_controller_get_doc_server(void *user, const char * rpc_id, resultset_t data)
{
    int status;
    char *docserver;

    controller_t cnt = (controller_t)user;
  	resultset_get_output_values(data, "(is)", &status, &docserver);

    cnt->default_doc_server = apr_pstrdup(cnt->pool, docserver);

    cnt->on_registered(cnt->on_registered_data);    
}

int controller_lookup_invite(controller_t cnt, const char *pin)
{
    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
    else if (session_make_rpc_call(
                        cnt->session,
                        "rpc_lookupinvite",
                        "controller",
                        "controller.find_invite",
                        "(sss)",
                        cnt->session_id,
                        "",
                        pin))
	{
	    debugf("Controller start meeting call failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}

int controller_is_telephony_available(controller_t cnt)
{
    return cnt->is_telephony_avail;
}

static void on_invite_lookup(void *user, const char * rpc_id, resultset_t data)
{
    controller_t cnt = (controller_t)user;
    apr_pool_t *newpool;

    int found, options;
    char *meetingid, *mtgtitle, *mtgtime, *mtghost, *mtgserver, *p_id, *p_username, *p_name;
    char *description, *meeting_invite, *password, *p_phone, *p_email;

    // Read result and handle
    //  "FOUND", "MEETINGID", "TITLE", "TIME", "HOST", "SERVER",
    // "PARTICIPANTID", "USERNAME", "NAME", "OPTIONS",
    // "DESCRIPTION", "MEETING INVITATION", "PASSWORD",
    // "PARTICIPANT PHONE", "PARTICIPANT EMAIL"
    //  FOUND: 1 = meeting found, 2 = meeting and user found, 0 = not found
    resultset_get_output_values(data, "(issssssssisssss)", &found, &meetingid, &mtgtitle, &mtgtime, &mtghost, &mtgserver,
            &p_id, &p_username, &p_name, &options, &description, &meeting_invite, &password, &p_phone, &p_email);

    apr_pool_create(&newpool, NULL);

    meeting_invite_t invite = apr_pcalloc(newpool, sizeof(struct meeting_invite_st));
    invite->pool         = newpool;
	invite->description  = apr_pstrdup(newpool, description);
	invite->host_id      = apr_pstrdup(newpool, mtghost);
	invite->meeting_id   = apr_pstrdup(newpool, meetingid);
	invite->participant_id = apr_pstrdup(newpool, p_id);
	invite->title        = apr_pstrdup(newpool, mtgtitle);
//    invite->type         = type;
//    invite->privacy      = privacy;
    invite->time         = atoi(mtgtime);
	invite->options      = options;
//	invite->from_name    = apr_pstrdup(newpool, fromname);
	invite->participant_name = apr_pstrdup(newpool, p_name);
	invite->message      = apr_pstrdup(newpool, meeting_invite);
//	invite->invite_type  = invitetype;
//	invite->cancel       = event == MeetingInviteCancel;

    cnt->on_invite_lookup(cnt->on_invite_lookup_data, invite);

}


int controller_meeting_start(controller_t cnt, const char *id)
{

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
    else if (session_make_rpc_call(
                        cnt->session,
                        "rpc_startmeeting",
                        "controller",
                        "controller.start_meeting",
                        "(ss)",
                        cnt->session_id,
                        id))
	{
	    debugf("Controller start meeting call failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}

static void on_meeting_request_failed(void *user, const char *id, int code, const char *msg)
{
    controller_t cnt = (controller_t)user;
    char * p;
    char type[128];

    // rpc id may have participant id encoded in it--if so, parse it out and return
    if (strncmp(id, "rpc_", 4) == 0)
        strncpy(type, id + 4, sizeof(type)-1);
    else
        strncpy(type, id, sizeof(type)-1);
    type[sizeof(type)-1] = '\0';

    p = strrchr(id, ':');
    if (p != NULL)
        *(p++) = '\0';

    struct meeting_error_st error;

    error.error = code;
    error.meeting_id = "";
    error.participant_id = p;
    error.type = type;
    error.message = msg;

    cnt->on_meeting_error(cnt->on_meeting_error_data, &error);

}

int controller_meeting_end(controller_t cnt, const char *id)
{
    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
    else if (session_make_rpc_call(
                        cnt->session,
                        "rpc_endmeeting",
                        "controller",
                        "controller.end_meeting",
                        "(ss)",
                        cnt->session_id,
                        id))
	{
	    debugf("Controller end meeting call failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}

int controller_meeting_join(controller_t cnt, const char *id, const char *participant_id, const char *password)
{
    char real_part_id[128];

    if (*participant_id && strchr(participant_id, ':') == NULL)
    {
        sprintf(real_part_id, "part:%d", atoi(participant_id));
        participant_id = real_part_id;
    }

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
    else if (session_make_rpc_call(
                        cnt->session,
                        "rpc_joinmeeting",
                        "controller",
                        "controller.join_meeting",
                        "(sssss)",
                        cnt->session_id,
						cnt->user_address,
                        id,
						participant_id,
						password))
	{
	    debugf("Controller join meeting call failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}

int controller_meeting_leave(controller_t cnt, const char *id)
{
    if (cnt == NULL || cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
    else if (session_make_rpc_call(
                        cnt->session,
                        "rpc_leavemeeting",
                        "controller",
                        "controller.leave_meeting",
                        "(sss)",
                        cnt->session_id,
						cnt->user_address,
                        id))
	{
	    debugf("Controller leave meeting call failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}

void on_server_error(void *user, const char * rpc_id, const char *method, resultset_t data)
{
    controller_t cnt = (controller_t)user;
    char* type;
    int status;
    char* msg;
    struct meeting_error_st error;

    resultset_get_output_values(data, "(sis*)", &type, &status, &msg);

    // type: "join", "call", "im", "logout"
    error.error = status;
    error.participant_id = "";
    error.meeting_id = "";
    error.type = type;
    error.message = msg;

    if (strcmp(type, "im") == 0)
    {
        char *meetingid;
        char *userid;
        active_meeting_t active;
        meeting_t mtg;
        const char ** newid;

        resultset_get_output_values(data, "(sisss)", &type, &status, &msg, &meetingid, &userid);

        // Server is notifing us that it can't perform an IM notification.  
        // Queue the data for later dispatch to application.
        active = find_meeting(cnt, meetingid, TRUE);
        mtg = make_info(cnt, active, FALSE);

        newid = apr_array_push(active->im_notifies);
        *newid = apr_pstrdup(active->pool, userid);
    }
    else
    {
        cnt->on_meeting_error(cnt->on_meeting_error_data, &error);
    }
}

static void process_im_notifies(controller_t cnt, active_meeting_t active)
{
    const char *userid;
    participant_t part;
    meeting_t info = active->info;
    int i;

    for (i=0; i<active->im_notifies->nelts; ++i)
    {
        userid = *(const char**)get_element_at(active->im_notifies, i);
        part = meeting_find_participant_by_userid(info, userid);
        if (part)
        {
            remove_element_at(active->im_notifies, i);
            --i;

            cnt->on_im_notify(cnt->on_im_notify_data, info->meeting_id, part);
        }
    }

}

#define DUPSTR(str) (apr_pstrdup(mtg->pool,str))

void on_server_meeting_event(void *user, const char * rpc_id, const char *method, resultset_t data)
{
    controller_t cnt = (controller_t)user;
   	int sendtype, event;
	char *meetingid, *partid, *key=NULL;
	active_meeting_t active;
	meeting_t mtg;

	// "SENDTYPE", "EVENT", "MEETINGID", "PARTICIPANTID", "APPSHAREKEY"
	resultset_get_output_values(data, "(iisss)", &sendtype, &event, &meetingid, &partid, &key);

    active = find_meeting(cnt, meetingid, TRUE);
    mtg = make_info(cnt, active, FALSE);

    meeting_lock(mtg);

    // Store app share encrypt key.
    if (key!=NULL && *key)
    {
        appshare_info_t info;
        if (active->appshare_info == NULL)
            active->appshare_info = apr_pcalloc(active->pool, sizeof(struct appshare_info_st));
        info = active->appshare_info;

        if (info->key == NULL || strcmp(key,info->key) != 0)
            info->key = apr_pstrdup(active->pool, key);
    }

    if (event == ParticipantRemoved)
    {
        remove_participant(mtg,partid);
    }

    meeting_unlock(mtg);

    dispatch_event(cnt, active, event, meetingid, "", partid);
}

void on_server_meeting_state(void *user, const char * rpc_id, const char *method, resultset_t data)
{
    controller_t cnt = (controller_t)user;
	active_meeting_t active;
    meeting_t mtg;

    int event;
    char *meetingid;

    // Parse out meeting info
    {
        int sendtype, type, privacy,
            priority, duration, size, time,
            recurrence, options, end_time,
            invite_expiration, invite_valid_before,
            display_attendees, invitee_chat_options,
            hosts_enabled_features, meeting_instance_time;

        char *hostid, *title, *mdescription, *invite_message, *password;
        char *bridge_address;
        const char *bridge_phone = "";

        // "SENDTYPE", "EVENT", "MEETINGID", "HOSTID", "TYPE",
        // "PRIVACY", "PRIORITY", "TITLE", "DESCRIPTION", "SIZE", "DURATION",
        // "TIME", "RECURRENCE", "OPTIONS", "END_TIME", "INVITE_EXPIRATION",
        // "INVITE_VALID_BEFORE", "INVITE_MESSAGE", "PASSWORD", "DISPLAY_ATTENDEES",
        // "INVITEE_CHAT_OPTIONS", "ENABLED_FEATURES", "MEETING_INSTANCE_TIME",
        // "BRIDGE_ADDRESS", ["BRIDGE PHONE"]
        resultset_get_values(data, 0, "(iissiiissiiiiiiiissiiiis*)",
            &sendtype, &event, &meetingid, &hostid,
            &type, &privacy, &priority, &title, &mdescription,
            &size, &duration, &time, &recurrence, &options,
            &end_time, &invite_expiration, &invite_valid_before,
            &invite_message, &password, &display_attendees,
            &invitee_chat_options, &hosts_enabled_features,
            &meeting_instance_time, &bridge_address);

        if (resultset_get_row_size(data, 0) > 24)
            bridge_phone = resultset_get_value_by_col(data, 0, 24);

        debugf("Event: %d", event);
        debugf("meeting: %s %s %d %s %d", meetingid, title, options, password, meeting_instance_time);
        debugf("ProcessMeetingStatus - HOST ENABLED FEATURES: %d\n", hosts_enabled_features);

        // Will allocate if needed
        active = find_meeting(cnt, meetingid, TRUE);

        // Create new meeting info structure
        mtg = make_info(cnt, active, FALSE);
        meeting_lock(mtg);

        // Mark all participants and submeetings as removed
        mark_invalid(mtg);

        if (event == MeetingReset)  // Sent if meeting display options have changed
        {
            //>>m_pDocument->m_Meeting.ServerReset();
        }

        mtg->meeting_id = DUPSTR(meetingid);
        mtg->host_id = DUPSTR(hostid);
        mtg->type = type;
        mtg->privacy = privacy;
        mtg->priority = priority;
        mtg->title = DUPSTR(title);
        mtg->description = DUPSTR(mdescription);
        mtg->invite_message = DUPSTR(invite_message);
        mtg->password = DUPSTR(password);
        mtg->size = size;
        mtg->duration = duration;
        mtg->time = time;
        mtg->end_time = end_time;
        mtg->invite_expiration = invite_expiration;
        mtg->invite_valid_before = invite_valid_before;
        mtg->display_attendees = display_attendees;
        mtg->invitee_chat_options = invitee_chat_options;
        mtg->recurrence = recurrence;
        mtg->options = options;
        mtg->host_enabled_features = hosts_enabled_features;
        mtg->meeting_instance_time = meeting_instance_time;
        mtg->bridge_address = DUPSTR(bridge_address);
        mtg->bridge_phone = DUPSTR(bridge_phone);
    }

    // Parse participants and submeetings
    {
        int roll, selphone, selemail, status, notify_type;
        char *title, *partid, *subid,  *userid, *name, *description, *phone, *email, *screenname;
        xmlrpc_bool is_main;
        int i;

        for (i = 1; i < resultset_get_chunk_size(data); i++)
        {
            int row_size = resultset_get_row_size(data, i);

            if (row_size == 3)
            {
                submeeting_t sm;

                // "SUBMEETINGID", "ISMAIN", "TITLE"
                resultset_get_values(data, i, "(sbs)", &subid, &is_main, &title);

                sm = meeting_find_submeeting(mtg, subid);
                if (sm == NULL)
                    sm = add_ptr(mtg->submeetings, mtg->pool, sizeof(struct submeeting_st));

                sm->meeting_id = DUPSTR(mtg->meeting_id);
                sm->sub_id = DUPSTR(subid);
                sm->title = DUPSTR(title);
                sm->is_main = is_main;
                sm->removed = FALSE;
            }
            else if (row_size == 13)
            {
                participant_t part;

                // "PARTICIPANTID", "SUBMEETINGID",
                // "ROLL", "USERID", "NAME", "DESCRIPTION",
                // "SELPHONE", "PHONE", "SELEMAIL", "EMAIL",
                // "SCREENNAME", "STATUS", "NOTIFY_TYPE"
                resultset_get_values(data, i, "(ssisssisissii)",
                    &partid, &subid, &roll, &userid, &name, &description,
                    &selphone, &phone, &selemail, &email,
                    &screenname, &status, &notify_type);

                part = meeting_find_participant(mtg,partid);
                if (part == NULL)
                    part = add_ptr(mtg->participants, mtg->pool, sizeof(struct participant_st));

                part->part_id = DUPSTR(partid);
                part->sub_id = DUPSTR(subid);
                part->roll = roll;
                part->user_id = DUPSTR(userid);
                part->name = DUPSTR(name);
                part->description = DUPSTR(description);
                part->selphone = selphone;
                part->phone = DUPSTR(phone);
                part->selemail = selemail;
                part->email = DUPSTR(email);
                part->screenname = DUPSTR(screenname);
                part->status = status;
                part->notify_type = notify_type;
                part->removed = FALSE;

                if ((status & StatusSelf) != 0)
                    mtg->participant_id = part->part_id;
            }
        }
    }

    process_im_notifies(cnt, active);

    // Remove all participants and submeetings that weren't in this set
    remove_invalid(mtg);
    meeting_unlock(mtg);

    dispatch_event(cnt, active, event, meetingid, "", "");
}

void on_server_meeting_info(void *user, const char * rpc_id, const char *method, resultset_t data)
{
    controller_t cnt = (controller_t)user;
	active_meeting_t active;
    meeting_t mtg;

    int event;
    char *meetingid;

    // Parse out meeting info
    {
        int sendtype, type, privacy,
            priority, duration, size, time,
            recurrence, options, end_time,
            invite_expiration, invite_valid_before,
            display_attendees, invitee_chat_options,
            hosts_enabled_features, meeting_instance_time;

        char *hostid, *title, *mdescription, *invite_message, *password;
        char *bridge_address;

        // "SENDTYPE", "EVENT", "MEETINGID", "HOSTID", "TYPE",
        // "PRIVACY", "PRIORITY", "TITLE", "DESCRIPTION", "SIZE", "DURATION",
        // "TIME", "RECURRENCE", "OPTIONS", "END_TIME", "INVITE_EXPIRATION",
        // "INVITE_VALID_BEFORE", "INVITE_MESSAGE", "PASSWORD", "DISPLAY_ATTENDEES",
        // "INVITEE_CHAT_OPTIONS", "ENABLED_FEATURES", "MEETING_INSTANCE_TIME",
        // "BRIDGE_ADDRESS"
        resultset_get_values(data, 0, "(iissiiissiiiiiiiissiiiis*)",
            &sendtype, &event, &meetingid, &hostid,
            &type, &privacy, &priority, &title, &mdescription,
            &size, &duration, &time, &recurrence, &options,
            &end_time, &invite_expiration, &invite_valid_before,
            &invite_message, &password, &display_attendees,
            &invitee_chat_options, &hosts_enabled_features,
            &meeting_instance_time, &bridge_address);

        debugf("Event: %d", event);
        debugf("meeting: %s %s %d %s %d", meetingid, title, options, password, meeting_instance_time);
        debugf("ProcessMeetingStatus - HOST ENABLED FEATURES: %d\n", hosts_enabled_features);

        // Will allocate if needed
        active = find_meeting(cnt, meetingid, TRUE);

        // Get info structure without flushing
        mtg = make_info(cnt, active, FALSE);
        meeting_lock(mtg);

        mtg->meeting_id = DUPSTR(meetingid);
        mtg->host_id = DUPSTR(hostid);
        mtg->type = type;
        mtg->privacy = privacy;
        mtg->priority = priority;
        mtg->title = DUPSTR(title);
        mtg->description = DUPSTR(mdescription);
        mtg->invite_message = DUPSTR(invite_message);
        mtg->password = DUPSTR(password);
        mtg->size = size;
        mtg->duration = duration;
        mtg->time = time;
        mtg->end_time = end_time;
        mtg->invite_expiration = invite_expiration;
        mtg->invite_valid_before = invite_valid_before;
        mtg->display_attendees = display_attendees;
        mtg->invitee_chat_options = invitee_chat_options;
        mtg->recurrence = recurrence;
        mtg->options = options;
        mtg->host_enabled_features = hosts_enabled_features;
        mtg->meeting_instance_time = meeting_instance_time;
        mtg->bridge_address = DUPSTR(bridge_address);
    }

    meeting_unlock(mtg);

    dispatch_event(cnt, active, event, meetingid, "", "");
}

void on_server_submeeting_state(void *user, const char * rpc_id, const char *method, resultset_t data)
{
    controller_t cnt = (controller_t)user;
    active_meeting_t active;
    meeting_t mtg;

	int sendtype, event;
	char *meetingid, *smid, *title;
	xmlrpc_bool is_main;

	// "SENDTYPE", "EVENT", "MEETINGID", "SUBMEETINGID", "ISMAIN", "TITLE"
	resultset_get_output_values(data, "(iissbs)", &sendtype, &event, &meetingid, &smid, &is_main, &title);

	debugf("Event: %d", event);
	debugf("submeeting: %s %s", meetingid, smid);

    active = find_meeting(cnt, meetingid, TRUE);
    mtg = make_info(cnt, active, FALSE);
    meeting_lock(mtg);

    if (event == SubmeetingAdded)
    {
        submeeting_t sm = meeting_find_submeeting(mtg, smid);
        if (sm == NULL)
            sm = add_ptr(mtg->submeetings, mtg->pool, sizeof(struct submeeting_st));

        sm->meeting_id = DUPSTR(meetingid);
        sm->sub_id = DUPSTR(smid);
        sm->is_main = is_main;
        sm->title = DUPSTR(title);
    }
    else if (event == SubmeetingRemoved)
    {
        remove_submeeting(mtg, smid);
    }
    meeting_unlock(mtg);

    dispatch_event(cnt, active, event, meetingid, smid, "");
}

void on_server_meeting_invite(void *user, const char * rpc_id, const char *method, resultset_t data)
{
    controller_t cnt = (controller_t)user;
    apr_pool_t *newpool;

	int sendtype, event, type, privacy,
        priority, duration, size, time,
        recurrence, options, invitetype;

	char *meetingid, *hostid, *title, *mdescription, *fromname, *message, *partid, *partname;

	// "SENDTYPE", "EVENT", "MEETINGID", "HOSTID", "TYPE",
    // "PRIVACY", "PRIORITY", "TITLE", "DESCRIPTION", "SIZE", "DURATION",
    // "TIME", "RECURRENCE", "OPTIONS", "FROMNAME", "MESSAGE", "PARTICIPANTID", PARTICIPANTNAME",
	// "INVITETYPE"
	resultset_get_output_values(data, "(iissiiissiiiiissssi)",
        &sendtype, &event, &meetingid, &hostid,
        &type, &privacy, &priority, &title, &mdescription,
        &size, &duration, &time, &recurrence, &options, &fromname, &message,
        &partid, &partname, &invitetype);

	debugf("Event: %d %s - partid=%s, partname=%s", event, meetingid, partid, partname);

    apr_pool_create(&newpool, NULL);

    meeting_invite_t invite = apr_pcalloc(newpool, sizeof(struct meeting_invite_st));
    invite->pool         = newpool;
	invite->description  = apr_pstrdup(newpool, mdescription);
	invite->host_id      = apr_pstrdup(newpool,hostid);
	invite->meeting_id   = apr_pstrdup(newpool,meetingid);
	invite->participant_id = apr_pstrdup(newpool,partid);
	invite->title        = apr_pstrdup(newpool,title);
    invite->type         = type;
    invite->privacy      = privacy;
    invite->time         = time;
	invite->options      = options;
	invite->from_name    = apr_pstrdup(newpool,fromname);
	invite->participant_name = apr_pstrdup(newpool,partname);
	invite->message      = apr_pstrdup(newpool,message);
	invite->invite_type  = invitetype;
	invite->cancel       = event == MeetingInviteCancel;

    cnt->on_invite(cnt->on_invite_data, invite);
}

void on_server_participant_state(void *user, const char * rpc_id, const char *method, resultset_t data)
{
    controller_t cnt = (controller_t)user;
    active_meeting_t active;
    meeting_t mtg;
    participant_t part;

	int sendtype, event, roll, selphone, selemail, status, notify_type, volume, gain, mute;
	char *meetingid, *partid, *subid,  *userid, *name, *description, *phone, *email, *screenname;

	// "SENDTYPE", "EVENT", "MEETINGID", "PARTICIPANTID", "SUBMEETINGID",
	// "ROLL", "USERID", "NAME", "DESCRIPTION", "PHONE", "EMAIL", "SCREENNAME",
	// "STATUS", "NOTIFY_TYPE", "VOLUME", "GAIN", "MUTE"
	resultset_get_output_values(data, "(iisssisssisissiiiii)", &sendtype, &event, &meetingid, &partid, &subid, &roll,
		&userid, &name, &description, &selphone, &phone, &selemail, &email, &screenname, &status, &notify_type,
		&volume, &gain, &mute);

    debugf("Event: %d", event);
    debugf("participant: %s %s %d %s %s %s %s %d", partid, subid, roll, userid, name, phone, email, status);

    active = find_meeting(cnt, meetingid, TRUE);
    mtg = make_info(cnt, active, FALSE);
    meeting_lock(mtg);

    part = meeting_find_participant(mtg, partid);

    if (part == NULL)
        part = add_ptr(mtg->participants, mtg->pool, sizeof(struct participant_st));

    part->part_id = DUPSTR(partid);
    part->sub_id = DUPSTR(subid);
    part->roll = roll;
    part->user_id = DUPSTR(userid);
    part->name = DUPSTR(name);
    part->description = DUPSTR(description);
    part->selphone = selphone;
    part->phone = DUPSTR(phone);
	part->selemail = selemail;
    part->email = DUPSTR(email);
    part->screenname = DUPSTR(screenname);
    part->status = status;
    part->notify_type = notify_type;
	part->volume = volume;
	part->gain = gain;
	part->mute = mute;

    process_im_notifies(cnt, active);

	meeting_unlock(mtg);

    dispatch_event(cnt, active, event, meetingid, subid, partid);
}

void on_server_app_share(void *user, const char * rpc_id, const char *method, resultset_t data)
{
    controller_t cnt = (controller_t)user;
    active_meeting_t active;
    appshare_info_t info;

	int sendtype, eventcode, status, share_options;

	char *meetingid, *presenterid, *address, *password, *key=NULL;

    resultset_get_output_values(data, "(i*)", &sendtype);

    if (sendtype == SendDocShare)
    {
        // PresenterGranted event is sent for both app share and doc share, so re-route if appropriate
        on_server_doc_share(user, rpc_id, method, data);
        return;
    }

	// "SENDTYPE", "EVENT", "MEETINGID", "PRESENTERID", "ADDRESS", "PASSWORD", "STATUS"
	// N.B. status is not currently used
	resultset_get_output_values(data, "(iissssiis)",
        &sendtype, &eventcode, &meetingid, &presenterid, &address, &password, &status, &share_options, &key);

    debugf("AppShareEvent: %d, %d, %s, %s, %s, %s, %d, %d",
            sendtype, eventcode, meetingid, presenterid, address,
            password, status, share_options);

    active = find_meeting(cnt, meetingid, TRUE);
    if (active->appshare_info == NULL)
        active->appshare_info = apr_pcalloc(active->pool, sizeof(struct appshare_info_st));
    info = active->appshare_info;

	if (eventcode == AppShareStarted || eventcode == PresenterGranted)
	{
        if (*key && (info->key == NULL || strcmp(key,info->key) != 0))
            info->key = apr_pstrdup(active->pool, key);
        if (*address && (info->address == NULL || strcmp(info->address, address) != 0))
            info->address = apr_pstrdup(active->pool, address);
        if (*password && (info->password == NULL || strcmp(info->password, password) != 0))
            info->password = apr_pstrdup(active->pool, password);
        if (*presenterid && (info->presenter == NULL || strcmp(info->presenter, presenterid) != 0))
            info->presenter = apr_pstrdup(active->pool, presenterid);
        info->share_options = share_options;
	}

    if (eventcode == PresenterGranted)
    {
        meeting_t mtg = make_info(cnt, active, FALSE);
        participant_t p = meeting_find_me(mtg);
        if (p)
            p->status |= StatusPresenter;
    }
    else if (eventcode == PresenterRevoked)
    {
        meeting_t mtg = make_info(cnt, active, FALSE);
        participant_t p = meeting_find_me(mtg);
        if (p)
            p->status = p->status & ~StatusPresenter;
    }

    dispatch_event(cnt, active, eventcode, active->meeting_id, "", presenterid);
}


void on_server_ppt_share(void *user, const char * rpc_id, const char *method, resultset_t data)
{
//    controller_t cnt = (controller_t)user;

}

void on_server_meeting_chat(void *user, const char * rpc_id, const char *method, resultset_t data)
{
    controller_t cnt = (controller_t)user;
	active_meeting_t active;
    meeting_t mtg;
    meeting_chat_t newchat;

	int sendtype, event, from_roll, to_type;
	char *meetingid, *from_id, *from_name, *message;

    // "SENDTYPE", "EVENT", "MEETINGID", "FROMID", "TOTYPE", "MESSAGE"
	resultset_get_output_values(data, "(iisssiis)",
        &sendtype, &event, &meetingid, &from_id, &from_name, &from_roll, &to_type, &message);

    active = find_meeting(cnt, meetingid, TRUE);
    mtg = make_info(cnt, active, FALSE);
    meeting_lock(mtg);

    newchat = apr_array_push(mtg->chats);
    newchat->meeting_id = DUPSTR(meetingid);
    newchat->time = time(NULL);
    newchat->message = DUPSTR(message);
    newchat->sent_to = to_type;
    newchat->from_participant_id = DUPSTR(from_id);
    newchat->from_name = DUPSTR(from_name);
    newchat->from_role = from_roll;

    meeting_unlock(mtg);

    cnt->on_meeting_chat(cnt->on_meeting_chat_data, newchat);
}

static active_meeting_t find_meeting(controller_t cnt, const char * id, BOOL create)
{
    active_meeting_t p = apr_hash_get(cnt->meetings, id, APR_HASH_KEY_STRING);
    if (p == NULL && create)
    {
        p = (active_meeting_t)apr_pcalloc(cnt->pool, sizeof(struct active_meeting_st));
        apr_pool_create(&p->pool, cnt->pool);

        p->meeting_id = apr_pstrdup(p->pool, id);

        apr_hash_set(cnt->meetings, p->meeting_id, APR_HASH_KEY_STRING, p);

        p->im_notifies = apr_array_make(p->pool, 10, sizeof(const char *));
    }
    return p;
}

static void dispatch_event(controller_t cnt, active_meeting_t active, int event, const char *meeting_id, const char *sub_id, const char *part_id)
{
    struct meeting_event_st evnt;

    evnt.event = event;
    evnt.meeting_id = meeting_id;
    evnt.sub_id = sub_id;
    evnt.participant_id = part_id;

    cnt->on_meeting_event(cnt->on_meeting_event_data, &evnt);
}

int controller_meeting_chat(controller_t cnt, const char *id, int send_to, const char *message, const char *from_part_id, const char *to_part_id)
{

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
    else if (session_make_rpc_call(cnt->session,
                        "chat_message",
                        "controller",
                        "controller.send_chat_message",
                        "(ssisss)",
						cnt->session_id,
                        id,
                        send_to,
                        message,
                        from_part_id,
                        to_part_id
                        ))
	{
	    debugf("Controller send chat call failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}

// Generate meeting_details structure based on our current meeting state
meeting_details_t controller_get_meeting_details(controller_t cnt, meeting_t info, int num_participants, participant_t participants[])
{
    meeting_details_t mtg = make_meeting_details(NULL);
    invitee_t invt;
    participant_t me = meeting_find_me(info);
    int i;

    // Copy meeting settings
    copy_meeting_to_details(cnt, mtg, info);

    // and me
    invt = meeting_details_add(mtg);
    copy_participant_to_invitee(mtg, me, invt);

    // and requested participants
    for (i=0;i<num_participants;i++)
    {
        if (me != participants[i])
        {
            invt = meeting_details_add(mtg);
            copy_participant_to_invitee(mtg, participants[i], invt);
        }
    }

    return mtg;
}

// Start meeting based on the specified settings and participants
int controller_meeting_create(controller_t cnt, meeting_details_t mtg)
{
    session_array_t array;
    invitee_t invitee;
    invitee_iterator_t iter;

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

	array = session_create_array(0, NULL);

    iter = meeting_details_iterate(mtg);
	invitee = meeting_details_next(mtg, iter);

	while (invitee != NULL)
	{
	    const char *username = "";
	    const char *user_id = convert_user_id(mtg->pool, invitee->user_id, cnt->user_id);
	    const char *part_id = convert_part_id(mtg->pool, invitee->participant_id);

	    // To do:
	    // - figure out if we have IM screen name for this user

        // PARTICIPANTID, USERID, USERNAME, int ROLL, NAME, SEL_PHONE, PHONE, SEL_EMAIL, EMAIL,
		// SCREEN, DESCRIPTION, int NOTIFYTYPE, MESSAGE, CALL_OPTIONS
		session_add_array_element(array, "(sssisisisssisi)",
						part_id,
						user_id,
						username,
						invitee->role,
						CHECK_NULL(invitee->name),
						invitee->selphone, CHECK_NULL(invitee->phone),
						invitee->selemail, CHECK_NULL(invitee->email),
						CHECK_NULL(invitee->screenname),
						"", // description
						invitee->notify_type,
						"", // message
						invitee->call_options);

        invitee = meeting_details_next(mtg, iter);
	}

    //  Create an app share key if we are using a secure session
    char    key[ APP_SHARE_KEY_LEN+1 ];
    char    hexKey[ APP_SHARE_KEY_LEN+APP_SHARE_KEY_LEN+1 ];
    key[ 0 ]    = '\0';
    hexKey[ 0 ] = '\0';
    if (session_is_secure(cnt->session))
    {
        //  Create a random key APP_SHARE_KEY_LEN bytes long
        gcry_randomize( (unsigned char *) key, APP_SHARE_KEY_LEN, GCRY_STRONG_RANDOM );

        //  Convert binary key to hexadecimal
        char    hDigits[ ] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
        int     i, j, k;
        for( i=0, j=0; i<APP_SHARE_KEY_LEN; i++ )
        {
            k = (key[ i ] >> 4) & 0x0F;
            hexKey[ j ] = hDigits[ k ];
            j++;
            k = (key[ i ] % 16) & 0x0F;
            hexKey[ j ] = hDigits[ k ];
            j++;
        }
        hexKey[ j ] = '\0';
    }

    //   SESSIONID, MEETINGID, HOSTID, TYPE, PRIVACY, START_TIME, TITLE, DESCRIPTION, SIZE, DURATION, PASSWORD, OPTIONS
    //   END_TIME, INVITE_EXPIRATION, INVITE_VALID_BEFORE, INVITE_MESSAGE, DISPLAY_ATTENDEES, INVITEE_CHAT_OPTIONS
    //   PARTICIPANT_LIST (array):
	if (session_make_rpc_call(cnt->session,
                        "rpc_createmeeting",
                        "controller",
                        "controller.create_meeting",
                        "(sssiiissiisiiiisiiVs)",
                        cnt->session_id,
                        mtg->meeting_id,
						mtg->host_id,
						mtg->type,
						mtg->privacy,
						mtg->start_time,
						CHECK_NULL(mtg->title),
						CHECK_NULL(mtg->description),
						0,  // voice ports, unused
						0,  // duration, unused
						CHECK_NULL(mtg->password),
						mtg->options,
						mtg->end_time,
						mtg->invite_expiration,
                        mtg->invite_valid_before,
						CHECK_NULL(mtg->invite_message),
						mtg->display_attendees,
						mtg->chat_options,
						array,
                        hexKey) )  // App share key
	{
	    debugf("Controller create meeting call failed");
	    return ERR_RPCFAILED;
	}

	session_free_array(array);

    return 0;
}

// Preview or reset current recording for indicated meeting
int controller_recording_modify(controller_t cnt, const char *mtgId, const char *pEmail, int iCmd )
{
    if (cnt->session == NULL)
    {
        debugf("Session is null");
        return ERR_NOTCONNECTED;
    }

    if( session_make_rpc_call( cnt->session,
                               "rpc_modifyrecording",
                               "controller",
                               "controller.modify_recording",
                               "(ssis)",
                               cnt->session_id,
                               mtgId,
                               iCmd,
                               pEmail
                             ) )
    {
        debugf("Controller modify recording failed");
        return ERR_RPCFAILED;
    }
    return 0;
}


// Update meeting based on meeting details
int controller_meeting_modify(controller_t cnt, meeting_details_t details, BOOL update_participants)
{
    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

    // First update the meeting options.
	if (session_make_rpc_call(cnt->session,
                        "rpc_modifymeeting",
                        "controller",
                        "controller.modify_meeting",
                        "(sssiiisssiiisii)",
                        cnt->session_id,
                        details->meeting_id,
						details->host_id,
						details->type,
						details->privacy,
						0, // priority
						details->title,
						details->description,
						details->password,
						details->options,
						details->invite_expiration,
                        details->invite_valid_before,
						details->invite_message,
						details->display_attendees,
						details->chat_options
						))
    {
	    debugf("Controller modify meeting call failed");
	    return ERR_RPCFAILED;
    }

    // We should add an API to allow a list of participants to be sent, but for now we need to
    // modify the participants one by one.
    if (update_participants)
    {
        invitee_iterator_t iter = meeting_details_iterate(details);
        invitee_t invitee = meeting_details_next(details, iter);

        session_array_t array = session_create_array(0, NULL);

        while (invitee != NULL)
        {
            const char *username = "";
            const char *user_id = convert_user_id(details->pool, invitee->user_id, cnt->user_id);
            const char *part_id = convert_part_id(details->pool, invitee->participant_id);

            // To do:
            // - figure out if we have IM screen name for this user

            // PARTICIPANTID, USERID, USERNAME, int ROLL, NAME, SEL_PHONE, PHONE, SEL_EMAIL, EMAIL,
            // SCREEN, DESCRIPTION, int NOTIFYTYPE, MESSAGE, CALL_OPTIONS
            session_add_array_element(array, "(sssisisisssisi)",
                            part_id,
                            user_id,
                            username,
                            invitee->role,
                            CHECK_NULL(invitee->name),
                            invitee->selphone, CHECK_NULL(invitee->phone),
                            invitee->selemail, CHECK_NULL(invitee->email),
                            CHECK_NULL(invitee->screenname),
                            "", // description
                            invitee->notify_type,
                            "", // message
                            invitee->call_options);

            invitee = meeting_details_next(details, iter);
        }

        if (session_make_rpc_call(cnt->session,
                                "rpc_modifymeeting",
                                "controller",
                                "controller.add_participants",
                                "(ssV)",
                                cnt->session_id,
                                details->meeting_id,
                                array
                                ))
        {
            debugf("Controller add participants call failed");
            return ERR_RPCFAILED;
        }

        session_free_array(array);
    }

    return 0;
}

// Modify meeting options using mask and new flag values.
// There is an inherent race condition in this process in that another user may change unrelated meeting options while
// we are generating this request, and that change would be lost.  However, the risk seems minimal.
int controller_meeting_modify_options(controller_t cnt, const char *meeting_id, int set_flags, int new_flags)
{
    active_meeting_t active;
    meeting_t mtg;
    int options;

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

    active = find_meeting(cnt, meeting_id, TRUE);
    mtg = make_info(cnt, active, FALSE);

    meeting_lock(mtg);

    options = (mtg->options & ~set_flags) | (new_flags & set_flags);

    debugf("Changing meeting options from %d to %d", mtg->options, options);

    // First update the meeting options.
	if (session_make_rpc_call(cnt->session,
                        "rpc_modifyoptions",
                        "controller",
                        "controller.modify_meeting",
                        "(sssiiisssiiisii)",
                        cnt->session_id,
                        mtg->meeting_id,
						mtg->host_id,
						mtg->type,
						mtg->privacy,
						0, // priority
						mtg->title,
						mtg->description,
						mtg->password,
						options,
						mtg->invite_expiration,
                        mtg->invite_valid_before,
						mtg->invite_message,
						mtg->display_attendees,
						mtg->invitee_chat_options
						))
    {
	    debugf("Controller modify meeting call failed");
        meeting_unlock(mtg);
	    return ERR_RPCFAILED;
    }

    meeting_unlock(mtg);
    return 0;
}

int controller_meeting_lock(controller_t cnt, const char *meeting_id, BOOL enable)
{
    return controller_meeting_modify_options(cnt, meeting_id, LockParticipants, enable ? LockParticipants : 0);
}

int controller_meeting_record(controller_t cnt, const char *meeting_id, BOOL enable)
{
    return controller_meeting_modify_options(cnt, meeting_id, RecordingMeeting, enable ? RecordingMeeting : 0);
}

int controller_meeting_lecture_mode(controller_t cnt, const char *meeting_id, BOOL enable)
{
    return controller_meeting_modify_options(cnt, meeting_id, LectureMode, enable ? LectureMode : 0);
}

int controller_meeting_hold_mode(controller_t cnt, const char *meeting_id, BOOL enable)
{
    return controller_meeting_modify_options(cnt, meeting_id, HoldMode, enable ? HoldMode : 0);
}

int controller_meeting_mute_joinleave_tones(controller_t cnt, const char *meeting_id, BOOL enable)
{
    return controller_meeting_modify_options(cnt, meeting_id, MuteJoinLeave, enable ? MuteJoinLeave : 0);
}

int controller_submeeting_add(controller_t cnt, const char *id, const char *opid)
{
    char rpcid[128];

    sprintf(rpcid, "rpc_addsubmeeting:%s", opid);

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
	else if (session_make_rpc_call(cnt->session,
                        rpcid,
                        "controller",
                        "controller.add_submeeting",
                        "(sssi)",
                        cnt->session_id,
                        id,
						"", // title (server always generates)
						3   // expected size
                        ))
	{
	    debugf("Controller add submeeting call failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}

static void on_submeeting_added(void *user, const char * rpc_id, resultset_t data)
{
    controller_t cnt = (controller_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;
	char *sub_id;

  	resultset_get_output_values(data, "(s*)", &sub_id);

    cnt->on_submeeting_add(cnt->on_submeeting_add_data, opid, sub_id);
}

int controller_submeeting_remove(controller_t cnt, const char *id, const char *sub_id)
{
    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
	else if (session_make_rpc_call(cnt->session,
                        "rpc_removesubmeeting",
                        "controller",
                        "controller.remove_submeeting",
                        "(sssi)",
                        cnt->session_id,
                        id,
						sub_id,
						0)) // move all calls to main
	{
	    debugf("Controller add submeeting call failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}

// Change meeting presenter
int controller_change_presenter(controller_t cnt, const char *meeting_id, const char *part_id)
{

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
	else if (session_make_rpc_call(cnt->session,
                        "rpc_presenter",
                        "controller",
                        "controller.change_presenter",
                        "(sss)",
						cnt->session_id,
                        meeting_id,
						part_id))
	{
	    debugf("Controller change presenter call failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}

// Roll call
int controller_roll_call(controller_t cnt, const char *meeting_id)
{

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
	else if (session_make_rpc_call(cnt->session,
                        "rpc_rollcall",
                        "controller",
                        "controller.roll_call",
                        "(ss)",
						cnt->session_id,
                        meeting_id
                        ))
	{
	    debugf("Controller roll call failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}

// Log custom event for mneeting
int controller_log_event(controller_t cnt, const char *meeting_id, const char *info)
{

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
	else if (session_make_rpc_call(cnt->session,
                        "rpc_logevent",
                        "controller",
                        "controller.log_event",
                        "(sss)",
						cnt->session_id,
                        meeting_id,
                        info
                        ))
	{
	    debugf("Controller log event failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}

// Enable/disable remote control control for presenter
int controller_participant_remote_control(controller_t cnt, const char *meeting_id, const char *part_id, BOOL enable)
{

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
	else if (session_make_rpc_call(cnt->session,
                        "rpc_enablecontrol",
                        "controller",
                        "controller.grant_app_share_control",
                        "(sssi)",
						cnt->session_id,
                        meeting_id,
						part_id,
						enable))
	{
	    debugf("Controller remote control call failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}

// Put participant hand up/down
int controller_participant_hand_up(controller_t cnt, const char *meeting_id, const char *part_id, BOOL enable)
{

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
	else if (session_make_rpc_call(cnt->session,
                        "rpc_handup",
                        "controller",
                        "controller.set_handup",
                        "(sssi)",
						cnt->session_id,
                        meeting_id,
						part_id,
						enable))
	{
	    debugf("Controller hand up call failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}

// Make custom voice bridge request
int controller_participant_bridge_request(controller_t cnt, const char *meeting_id, const char *part_id, int code, const char *msg)
{
    
    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
	else if (session_make_rpc_call(cnt->session,
                        "rpc_bridgerequest",
                        "controller",
                        "controller.send_bridge_request",
                        "(sssis)",
						cnt->session_id,
                        meeting_id,
						part_id,
						code,
						msg))
	{
	    debugf("Controller bridge request failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}

// Enable/disable moderator priv. for participant
int controller_participant_moderator(controller_t cnt, const char *meeting_id, const char *part_id, BOOL enable)
{

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
	else if (session_make_rpc_call(cnt->session,
                        "rpc_enablemoderator",
                        "controller",
                        "controller.grant_moderator_privilege",
                        "(sssi)",
						cnt->session_id,
                        meeting_id,
						part_id,
						enable))
	{
	    debugf("Controller moderator call failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}

// Move participant to submeeting
int controller_participant_move(controller_t cnt, const char *meeting_id, const char *part_id, const char *sub_id)
{

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
	else if (session_make_rpc_call(cnt->session,
                        "rpc_participantmove",
                        "controller",
                        "controller.move_participant",
                        "(ssss)",
                        cnt->session_id,
                        meeting_id,
						part_id,
						sub_id
						))
	{
	    debugf("Controller move participant call failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}

int controller_participant_remove(controller_t cnt, const char *meeting_id, const char *participant_id)
{

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
	else if (session_make_rpc_call(cnt->session,
                        "rpc_participantremove",
                        "controller",
                        "controller.remove_participant",
                        "(sss)",
                        cnt->session_id,
                        meeting_id,
						participant_id
						))
	{
	    debugf("Controller remove participant call failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}

int controller_participant_disconnect(controller_t cnt, const char *meeting_id, const char *participant_id)
{

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
	else if (session_make_rpc_call(cnt->session,
                        "rpc_participantdisconnect",
                        "controller",
                        "controller.disconnect_participant",
                        "(sssi)",
                        cnt->session_id,
                        meeting_id,
						participant_id,
						MediaVoice
						))
	{
	    debugf("Controller remove participant call failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}

int controller_participant_volume(controller_t cnt, const char *meeting_id, const char *participant_id, int volume, int gain, int mute)
{

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
	else if (session_make_rpc_call(cnt->session,
                        "rpc_participantvolume",
                        "controller",
                        "controller.set_participant_volume",
                        "(sssiii)",
                        cnt->session_id,
                        meeting_id,
						participant_id,
						volume,
						gain,
						mute
						))
	{
	    debugf("Controller participant volume call failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}


// Add or update participant
// Will call or send notification based on options
int controller_participant_update(controller_t cnt, const char * meeting_id, const char * sub_id,
                                const char *part_id, const char *user_id, const char *username, int role,
                                const char *name, int selphone, const char *phone, int selemail, const char *email,
                                const char *screenname, int notify_type, int call_options)
{
    const char *cvt_user_id;
    active_meeting_t active = find_meeting(cnt, meeting_id, FALSE);
    if (active == NULL)
    {
        return ERR_UNKNOWNMEETING;
    }
    cvt_user_id = convert_user_id(active->pool, user_id, cnt->user_id);


	// Make add participant call:
	// Send SESSIONID, MEETINGID, SUBMEETINGID, USERID, USERNAME, int ROLL, NAME,
	// SELPHONE, PHONE, SELEMAIL, EMAIL, SCREEN, DESCRIPTION, int NOTIFYTYPE, MESSAGE,
	// int CALLTYPE, int EXPIRES

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
	else if (session_make_rpc_call(cnt->session,
                        "rpc_participantupdate",
                        "controller",
                        "controller.add_participant",
                        "(sssssisisisssisii)",
                        cnt->session_id,
                        meeting_id,
                        CHECK_NULL(sub_id),
                        cvt_user_id,
                        CHECK_NULL(username),
                        role,
                        CHECK_NULL(name),
						selphone,
                        CHECK_NULL(phone),
						selemail,
                        CHECK_NULL(email),
                        CHECK_NULL(screenname),
						"",  // description
                        notify_type,
                        "",  // message
						call_options,
						0   // expires
						))
	{
	    debugf("Controller add participant call failed");
	    return ERR_RPCFAILED;
	}

	return 0;
}


// Place call to participant.
// A moderator may call anyone; normal users may only call themselves.
int controller_participant_call(controller_t cnt, const char *meeting_id, 
                                const char *part_id, int selphone, const char *phone, int call_options)
{
    active_meeting_t active = find_meeting(cnt, meeting_id, FALSE);
    if (active == NULL)
    {
        return ERR_UNKNOWNMEETING;
    }

	// Make call participant call:
	// Send SESSIONID, MEETINGID, PARTICIPANTID, int TARGETSELECTEDPHONE, TARGETPHONE, int OPTIONS

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
	else if (session_make_rpc_call(cnt->session,
                        "rpc_participantcall",
                        "controller",
                        "controller.call_participant",
                        "(sssisi)",
                        cnt->session_id,
                        meeting_id,
                        part_id,
						selphone,
                        CHECK_NULL(phone),
						call_options
						))
	{
	    debugf("Controller call participant call failed");
	    return ERR_RPCFAILED;
	}

	return 0;
}

// Get app share info for this meeting, NULL if none is available.
appshare_info_t controller_get_appshare_info(controller_t cnt, const char *meeting_id)
{
    active_meeting_t active = find_meeting(cnt, meeting_id, FALSE);
    if (active)
        return active->appshare_info;
    return NULL;
}

// Start app share session.
// Set SharePPT for PPT mode, SharePPTnoEraser for PPT version 11 or higher, ShareAutoDetect to detect slow connection
// The same start time must also be sent to the VNC server process for presenter.
int controller_appshare_start(controller_t cnt, const char *meeting_id, int share_options, int start_time)
{
    active_meeting_t active = find_meeting(cnt, meeting_id, FALSE);
    if (active == NULL)
    {
        return ERR_UNKNOWNMEETING;
    }
    if (active->appshare_info == NULL)
    {
        // ToDo: call get new share server
        return ERR_NOSHARESERVER;
    }

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

	if (session_make_rpc_call(cnt->session,
                        "rpc_appsharestart",
                        "controller",
                        "controller.start_app_share",
                        "(ssssii)",
						cnt->session_id,
                        meeting_id,
						active->appshare_info->address,
						active->appshare_info->password,
						share_options,
						start_time
						))
	{
	    debugf("Controller start app share failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}

int controller_appshare_end(controller_t cnt, const char *meeting_id)
{

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
	else if (session_make_rpc_call(cnt->session,
                        "rpc_appshareend",
                        "controller",
                        "controller.end_app_share",
                        "(ss)",
                        cnt->session_id,
                        meeting_id
                        ))
	{
	    debugf("Controller end app share failed");
	    return ERR_RPCFAILED;
	}

    return 0;
}

void on_server_doc_share(void *user, const char * rpc_id, const char *method, resultset_t data)
{
    controller_t cnt = (controller_t)user;
    active_meeting_t active;

	int sendtype, eventcode;
	char *meetingid, *presenterid, *address;
	char *docid, *pageid, *drawing;
	int options;

	// "SENDTYPE", "EVENT", "MEETINGID", "PRESENTERID", "ADDRESS"
	resultset_get_output_values(data, "(iisssssis)",
        &sendtype, &eventcode, &meetingid, &presenterid, &address,
		&docid, &pageid, &options, &drawing);

    debugf("DocShareEvent: %d, %d, %s, %s, %s, %s, %s, %d, %s",
            sendtype, eventcode, meetingid, presenterid, address,
	        docid, pageid, options, drawing);

    active = find_meeting(cnt, meetingid, TRUE);

	if (eventcode == DocShareStarted || eventcode == PresenterGranted)
	{
        active->docshare_address = apr_pstrdup(active->pool, address);
	}

    {
        struct docshare_event_st evnt;

        evnt.event = eventcode;
        evnt.meeting_id = meetingid;
        evnt.presenter_id = presenterid;
        evnt.doc_id = docid;
        evnt.page_id = pageid;
        evnt.drawing = drawing;
        evnt.options = options;

        cnt->on_docshare_event(cnt->on_docshare_event_data, &evnt);
    }
}

const char * controller_get_docshare_server(controller_t cnt, const char *meeting_id)
{
    active_meeting_t active = find_meeting(cnt, meeting_id, FALSE);
    if (active == NULL)
    {
        return cnt->default_doc_server;
    }
    // ToDo: call get new share server if NULL
    return active->docshare_address;
}

const char * controller_get_username(controller_t cnt)
{
    return cnt->username;
}

int controller_docshare_start(controller_t cnt, const char *meeting_id, int start_time)
{
    active_meeting_t active = find_meeting(cnt, meeting_id, FALSE);
    if (active == NULL)
    {
        return ERR_UNKNOWNMEETING;
    }
    if (active->docshare_address == NULL || !*active->docshare_address)
    {
        // ToDo: call get new share server
        return ERR_NOSHARESERVER;
    }

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

    if (session_make_rpc_call(cnt->session, "rpc_docsharestart",
                        "controller",
                        "controller.start_doc_share",
                        "(sssi)",
                        cnt->session_id,
                        meeting_id,
						active->docshare_address,
						start_time))
	{
	    return ERR_RPCFAILED;
	}

    return 0;
}

int controller_docshare_end(controller_t cnt, const char *meeting_id)
{
    active_meeting_t active = find_meeting(cnt, meeting_id, FALSE);
    if (active == NULL)
    {
        return ERR_UNKNOWNMEETING;
    }

    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

    if (session_make_rpc_call(cnt->session, "rpc_docshareend",
                        "controller",
                        "controller.end_doc_share",
                        "(ss)",
                        cnt->session_id,
                        meeting_id
                        ))
	{
	    return ERR_RPCFAILED;
	}

    return 0;
}

int controller_docshare_draw(controller_t cnt, const char *meeting_id, const char *drawing)
{
    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

    if (session_make_rpc_call(cnt->session, "rpc_docsharedraw",
                        "controller",
                        "controller.draw_on_whiteboard",
                        "(sss)",
						cnt->session_id,
                        meeting_id,
						drawing))
	{
	    return ERR_RPCFAILED;
	}

    return 0;
}

int controller_docshare_download(controller_t cnt, const char *meeting_id, const char *doc_id, const char *page_id)
{
    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

    if (session_make_rpc_call(cnt->session, "rpc_docsharedownload",
                        "controller",
                        "controller.download_document_page",
                        "(ssss)",
						cnt->session_id,
                        meeting_id,
						doc_id,
						page_id))
	{
	    return ERR_RPCFAILED;
	}

    return 0;
}

int controller_get_meeting_params(controller_t cnt, const char *meeting_id, const char *token)
{
    char rpcid[128];

    sprintf(rpcid, "rpc_controllerfetchdetails:%s", token);

    // First we fetch information about the meeting, then the info associated with the token.
    if (cnt->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
    else if (session_make_rpc_call(cnt->session, rpcid,
                                   "addressbk",
                                   "schedule.find_meeting_details",
                                   "(ss)",
                                   cnt->auth->session_id,
                                   meeting_id
                                   ))
	{
	    debugf("Fetch meeting details call failed");
        return ERR_RPCFAILED;
	}

	return 0;
}

static void on_meeting_details(void *user, const char * rpc_id, resultset_t data)
{
    controller_t cnt = (controller_t)user;
    const char *token = strchr(rpc_id, ':') + 1;

    // Parse result
    cnt->meeting = parse_meeting_details(cnt->pool, data);

    char out_rpc_id[128];
    sprintf(out_rpc_id, "rpc_getmeetingparams:%s", cnt->meeting->meeting_id);

    if (cnt->session == NULL)
    {
		debugf("Session is null");
    }
    else if (session_make_rpc_call(cnt->session,
                        out_rpc_id,
                        "controller",
                        "controller.get_meeting_param",
                        "(ss)",
						cnt->session_id,
                        token
                        ))
	{
	    debugf("Controller end app share failed");
	}
}

static void on_meeting_params(void *user, const char * rpc_id, resultset_t data)
{
    controller_t cnt = (controller_t)user;
    const char * meeting_id = strchr(rpc_id, ':') + 1;

    int status;
    xmlrpc_value *participant_array;
	char *title, *description, *message, *password, *forum_token;
    int schedule_time, forum_options, options_set, options_clear;

	resultset_get_output_values(data, "(iVssssisiii)",
                              &status,
                              &participant_array,
                              &title,
                              &description,
                              &message,
                              &password,
                              &schedule_time,
                              &forum_token,
                              &forum_options,
                              &options_set,
                              &options_clear
                              );

    if (status == Ok)
    {
        meeting_details_t mtg = cnt->meeting;
        invitee_t invt;

        mtg->title = apr_pstrdup(mtg->pool, title);
        mtg->description = apr_pstrdup(mtg->pool, description);
        mtg->invite_message = apr_pstrdup(mtg->pool, message);
        mtg->password = apr_pstrdup(mtg->pool, password);
        if (schedule_time == -1)
        {
            schedule_time = 0;
        }
        mtg->start_time = schedule_time;
        mtg->options = (mtg->options & options_clear) | options_set;

        // Read the participant array
        char *screenname, *displayname, *phone, *email, *im_screenname;
        int im_type, moderator;

        xmlrpc_value *val;
        xmlrpc_env env;
        xmlrpc_env_init(&env);
        int size = xmlrpc_array_size(&env, participant_array);

        for (int loop = 0; loop < size; loop++)
        {
            // Loop through the participant data
            val = xmlrpc_array_get_item(&env, participant_array, loop);
            xmlrpc_parse_value(&env, val, "(ssssisi)",
                               &screenname,
                               &displayname,
                               &phone,
                               &email,
                               &im_type,
                               &im_screenname,
                               &moderator
                               );

            if (find_by_screenname(mtg, screenname) == NULL)
            {
                // ToDo:  handle im_type
                invt = meeting_details_add(mtg);
                invt->screenname = apr_pstrdup(mtg->pool, screenname);
                invt->name = apr_pstrdup(mtg->pool, displayname);
                invt->phone = apr_pstrdup(mtg->pool, phone);
                invt->email = apr_pstrdup(mtg->pool, email);

                if (*invt->screenname)
                {
                    contact_t contact = find_contact(cnt->session, invt->screenname);
                    if (contact)
                    {
                        invt->user_id = apr_pstrdup(mtg->pool, contact->userid);

                        // ToDo: remove demo code
                        invt->notify_type |= NotifyIIC;
                        if (*contact->busphone)
                        {
                            invt->selphone = SelPhoneThis;
                            invt->phone = apr_pstrdup(mtg->pool, contact->busphone);
                            invt->notify_type |= NotifyPhone;
                        }
                    }
                }
            }
        }

        cnt->on_get_params(cnt->on_get_params_data, mtg, forum_options);
    }
    else
    {
        struct meeting_error_st error;

        error.error = status;
        error.meeting_id = meeting_id;
        error.participant_id = "";
        error.type = "getmeetingparams";
        error.message = "Unable to get meeting invitee list";

        cnt->on_meeting_error(cnt->on_meeting_error_data, &error);
    }
}


//
// Meeting info APIs
//

meeting_t controller_find_meeting(controller_t cnt, const char *id)
{
    active_meeting_t active = find_meeting(cnt, id, FALSE);
    if (active != NULL && active->info)
    {
        meeting_add_reference(active->info);
        return active->info;
    }
    return NULL;
}

static meeting_t make_info(controller_t cnt, active_meeting_t active, BOOL clear)
{
    apr_pool_t *newpool;
    meeting_t newmeeting = active->info;

    if (newmeeting != NULL && clear)
    {
        // Clear out old data before storing new data.
        meeting_remove_reference(active->info);
        newmeeting = NULL;
    }

    if (newmeeting == NULL)
    {
        apr_pool_create(&newpool, active->pool);
        newmeeting = apr_pcalloc(newpool, sizeof(struct meeting_st));

        apr_thread_mutex_create(&newmeeting->lock, APR_THREAD_MUTEX_NESTED, newpool);
        newmeeting->pool = newpool;
        newmeeting->submeetings = apr_array_make(newpool, 4, sizeof(submeeting_t));
        newmeeting->participants = apr_array_make(newpool, 10, sizeof(participant_t));
        newmeeting->chats = apr_array_make(newpool, 20, sizeof(struct meeting_chat_st));
        newmeeting->references = 1;

        active->info = newmeeting;
    }

    return newmeeting;
}

static void mark_invalid(meeting_t mtg)
{
    participant_t p;
    submeeting_t sm;
    int i;

    for (i=0; i<mtg->participants->nelts; ++i)
    {
        p = get_ptr_at(mtg->participants, i);
        p->removed = TRUE;
    }

    for (i=0; i<mtg->submeetings->nelts; ++i)
    {
        sm = get_ptr_at(mtg->submeetings, i);
        sm->removed = TRUE;
    }
}

static void remove_invalid(meeting_t mtg)
{
    participant_t p;
    submeeting_t sm;
    int i;

    for (i = mtg->participants->nelts-1; i >= 0; --i)
    {
        p = get_ptr_at(mtg->participants, i);
        if (p->removed)
            remove_element_at(mtg->participants, i);
    }

    for (i = mtg->submeetings->nelts-1; i >= 0; --i)
    {
        sm = get_ptr_at(mtg->submeetings, i);
        if (sm->removed)
            remove_element_at(mtg->submeetings, i);
    }
}

static void remove_participant(meeting_t mtg, const char *part_id)
{
    participant_t p;
    int i;

    meeting_lock(mtg);
    for (i=0; i<mtg->participants->nelts; i++)
    {
        p = get_ptr_at(mtg->participants, i);
        if (strcmp(p->part_id, part_id) == 0)
        {
            p->removed = TRUE;
            remove_element_at(mtg->participants, i);
            meeting_unlock(mtg);
            return;
        }
    }
    meeting_unlock(mtg);
}

static void remove_submeeting(meeting_t mtg, const char *sub_id)
{
    submeeting_t sm;
    int i;

    meeting_lock(mtg);
    for (i=0; i<mtg->submeetings->nelts; i++)
    {
        sm = get_ptr_at(mtg->submeetings, i);
        if (strcmp(sm->sub_id, sub_id) == 0)
        {
            sm->removed = TRUE;
            remove_element_at(mtg->submeetings, i);
            meeting_unlock(mtg);
            return;
        }
    }
    meeting_unlock(mtg);
}

void meeting_add_reference(meeting_t info)
{
    ++info->references;
}

void meeting_remove_reference(meeting_t info)
{
    if (--info->references == 0)
    {
        if (info && info->pool)
        {
            apr_thread_mutex_destroy(info->lock);
            apr_pool_destroy(info->pool);
        }
    }
}

void meeting_lock(meeting_t info)
{
    apr_thread_mutex_lock(info->lock);
}

void meeting_unlock(meeting_t info)
{
    apr_thread_mutex_unlock(info->lock);
}

participant_t meeting_find_participant(meeting_t mtg, const char *part_id)
{
    participant_t p;
    int i;

    meeting_lock(mtg);
    for (i=0; i<mtg->participants->nelts; i++)
    {
        p = get_ptr_at(mtg->participants, i);
        if (strcmp(p->part_id, part_id) == 0)
        {
            meeting_unlock(mtg);
            return p;
        }
    }
    meeting_unlock(mtg);
    return NULL;
}

participant_t meeting_find_participant_by_userid(meeting_t mtg, const char *user_id)
{
    participant_t p;
    int i;

    meeting_lock(mtg);
    for (i=0; i<mtg->participants->nelts; i++)
    {
        p = get_ptr_at(mtg->participants, i);
        if (strcmp(p->user_id, user_id) == 0)
        {
            meeting_unlock(mtg);
            return p;
        }
    }
    meeting_unlock(mtg);
    return NULL;
}

submeeting_t meeting_find_submeeting(meeting_t mtg, const char *sub_id)
{
    submeeting_t sm;
    int i;

    meeting_lock(mtg);
    for (i=0; i<mtg->submeetings->nelts; i++)
    {
        sm = get_ptr_at(mtg->submeetings, i);
        if (strcmp(sm->sub_id, sub_id) == 0)
        {
            meeting_unlock(mtg);
            return sm;
        }
    }
    meeting_unlock(mtg);
    return NULL;
}

// Inefficient iterators that cope with the list being changed underneath
// (will reach a premature end if previous was been deleted though)
participant_t meeting_next_participant(meeting_t mtg, participant_t prev)
{
    participant_t p;
    BOOL next = FALSE;
    int i;

    meeting_lock(mtg);
    for (i=0; i<mtg->participants->nelts; i++)
    {
        p = get_ptr_at(mtg->participants, i);
        if (next || prev == NULL)
        {
            meeting_unlock(mtg);
            return p;
        }
        if (strcmp(p->part_id, prev->part_id) == 0)
        {
            next = TRUE;
        }
    }
    meeting_unlock(mtg);
    return NULL;
}

submeeting_t meeting_next_submeeting(meeting_t mtg, submeeting_t prev)
{
    submeeting_t sm;
    BOOL next = FALSE;
    int i;

    meeting_lock(mtg);
    for (i=0; i<mtg->submeetings->nelts; i++)
    {
        sm = get_ptr_at(mtg->submeetings, i);
        if (next || prev == NULL)
        {
            meeting_unlock(mtg);
            return sm;
        }
        if (strcmp(sm->sub_id, prev->sub_id) == 0)
        {
            next = TRUE;
        }
    }
    meeting_unlock(mtg);
    return NULL;
}

participant_t meeting_find_host(meeting_t mtg)
{
    participant_t p;
    int i;

    for (i=0; i<mtg->participants->nelts; ++i)
    {
        // Should we use role or host id??
        p = get_ptr_at(mtg->participants, i);
        if (p->roll == RollHost)
            return p;
    }

    return NULL;
}

participant_t meeting_find_me(meeting_t mtg)
{
    participant_t p;
    int i;

    for (i=0; i<mtg->participants->nelts; ++i)
    {
        // Should we use role or host id??
        p = get_ptr_at(mtg->participants, i);
        if (strcmp(p->part_id, mtg->participant_id) == 0)
            return p;
    }

    return NULL;
}

invitee_t find_by_screenname(meeting_details_t mtg, const char *screen)
{
    int i;

    if (!screen || !*screen)
        return NULL;

    for (i=0; i<mtg->invitees->nelts; i++)
    {
        invitee_t cur = (invitee_t)get_element_at(mtg->invitees, i);
        if (cur->screenname != NULL && strcmp(screen, cur->screenname) == 0)
        {
            return cur;
        }
    }

    return NULL;
}

void meeting_invite_destroy(meeting_invite_t invt)
{
    apr_pool_destroy(invt->pool);
}

//
// Utilities
//

void copy_meeting_to_details(controller_t cnt, meeting_details_t mtg, meeting_t info)
{
#define COPY(to,from) mtg->to = info->from
#define COPYS(to,from) mtg->to  = info->from ? apr_pstrdup(mtg->pool, info->from) : NULL
    COPYS(meeting_id,meeting_id);
    COPYS(host_id,host_id);
    COPY(type,type);
    COPY(privacy,privacy);
    COPYS(title,title);
    COPYS(description,description);
    COPYS(invite_message,invite_message);
    COPYS(password,password);
    COPY(start_time,time);
    COPY(end_time,end_time);
    COPY(invite_expiration,invite_expiration);
    COPY(invite_valid_before,invite_valid_before);
    COPY(display_attendees,display_attendees);
    COPY(options,options);
    COPY(chat_options,invitee_chat_options);
    COPY(host_enabled_features,host_enabled_features);
    COPY(bridge_phone,bridge_phone);

//    mtg->bridge_phone = apr_pstrdup(mtg->pool, cnt->auth ? cnt->auth->bridge_phone : "");
    mtg->system_url = apr_pstrdup(mtg->pool, cnt->auth ? cnt->auth->system_url : "");

#undef COPY
#undef COPYS
}

void copy_participant_to_invitee(meeting_details_t mtg, participant_t participant, invitee_t invt)
{
#define COPY(to,from) invt->to = participant->from
#define COPYS(to,from) invt->to  = participant->from ? apr_pstrdup(mtg->pool, participant->from) : NULL
    COPYS(participant_id,part_id);
    COPYS(user_id,user_id);
    COPY(role,roll);
    COPYS(name,name);
    COPY(selphone,selphone);
    COPYS(phone,phone);
    COPY(selemail,selemail);
    COPYS(email,email);
    COPYS(screenname,screenname);
    COPY(notify_type,notify_type);
    invt->meeting_id = mtg->meeting_id;
#undef COPY
#undef COPYS
}

// Lookup contact info from local data.
contact_t find_contact(session_t sess, const char * screenname)
{
    struct session_message_st msg;
    msg.id = "contact_by_screen";
    msg.str_data = screenname;
    msg.ptr_data = NULL;

    session_send_message(sess, &msg);
    return msg.ptr_data;
}


int controller_ping_start(controller_t cnt, const char *id)
{

    if (cnt->session == NULL || cnt->session_id == NULL)
    {
        debugf("Session is null");
        return ERR_NOTCONNECTED;
    }

    char rpcid[128];
    sprintf(rpcid, "rpc_controllerping:%s", cnt->session_id);

    if (session_make_rpc_call
          (cnt->session,
           rpcid,
           "controller",
           "controller.ping",
           "(s)",
           cnt->session_id))
    {
        debugf("Controller start ping call failed");
        return ERR_RPCFAILED;
    }

    return 0;
}

void on_controller_ping(void *user, const char *id, resultset_t data)
{
    controller_t cnt = (controller_t)user;
    int     iVal;

    resultset_get_output_values(data, "(i)", &iVal);
    if( iVal  ==  PingResultOK )
    {
        cnt->on_ping(cnt->on_ping_data);
    }
    else
    {
        char    *p;
        char    type[ 128 ];

        debugf("entering on_controller_ping( )  --  iVal = %d", iVal);

        // rpc id may have participant id encoded in it--if so, parse it out and return
        if( strncmp( id, "rpc_", 4 )  ==  0 )
            strncpy( type, id + 4, sizeof(type)-1 );
        else
            strncpy( type, id, sizeof(type)-1 );
        type[ sizeof(type)-1 ] = '\0';

        p = strrchr( id, ':' );
        if( p  !=  NULL )
            *(p++) = '\0';

        struct meeting_error_st error;
        error.error = ERR_CONNECTLOST;
        error.meeting_id = "";
        error.participant_id = p;
        error.type = type;
        error.message = "";

        cnt->on_meeting_error(cnt->on_meeting_error_data, &error);
    }
}

static void on_controller_ping_failed(void *user, const char *id, int code, const char *msg)
{
    controller_t cnt = (controller_t)user;
    char * p;
    char type[128];

    debugf("entering on_controller_ping_failed( )");

    // rpc id may have participant id encoded in it--if so, parse it out and return
    if( strncmp( id, "rpc_", 4 )  ==  0 )
        strncpy( type, id + 4, sizeof(type)-1 );
    else
        strncpy( type, id, sizeof(type)-1 );
    type[sizeof(type)-1] = '\0';

    p = strrchr( id, ':' );
    if( p  !=  NULL )
        *(p++) = '\0';

    struct meeting_error_st error;
    error.error = code;
    error.meeting_id = "";
    error.participant_id = p;
    error.type = type;
    error.message = msg;

    cnt->on_meeting_error(cnt->on_meeting_error_data, &error);
}


//
// Static local data
//

struct rpc_handler_st c_handler_list[] =
{
    { "rpc_controllerauth", on_controller_registered, NULL },
    { "rpc_controllerauthretry", on_controller_registered, NULL },
    { "rpc_checktelephony", on_controller_check_telephone, NULL },
    { "rpc_lookupinvite", on_invite_lookup, NULL },
    { "rpc_startmeeting", NULL, on_meeting_request_failed },
    { "rpc_createmeeting", NULL, on_meeting_request_failed },
    { "rpc_joinmeeting", NULL, on_meeting_request_failed },
    { "rpc_leavemeeting", NULL, on_meeting_request_failed },
    { "rpc_presenter", NULL, on_meeting_request_failed },
    { "rpc_enablecontrol", NULL, on_meeting_request_failed },
    { "rpc_handup", NULL, on_meeting_request_failed },
    { "rpc_bridgerequest", NULL, on_meeting_request_failed },
    { "rpc_enablemoderator", NULL, on_meeting_request_failed },
    { "rpc_participantupdate", NULL, on_meeting_request_failed },
    { "rpc_participantcall", NULL, on_meeting_request_failed },
    { "rpc_addsubmeeting", on_submeeting_added, on_meeting_request_failed },
    { "rpc_removesubmeeting", NULL, on_meeting_request_failed },
    { "rpc_participantmove", NULL, on_meeting_request_failed },
    { "rpc_modifymeeting", NULL, on_meeting_request_failed },
    { "rpc_modifyoptions", NULL, on_meeting_request_failed },
    { "rpc_appsharestart", NULL, on_meeting_request_failed },
    { "rpc_appshareend", NULL, on_meeting_request_failed },
    { "rpc_controllerfetchdetails", on_meeting_details, on_meeting_request_failed },
    { "rpc_getmeetingparams", on_meeting_params, on_meeting_request_failed },
    { "rpc_participantremove", NULL, on_meeting_request_failed },
    { "rpc_participantdisconnect", NULL, on_meeting_request_failed },
    { "rpc_participantvolume", NULL, on_meeting_request_failed },
    { "rpc_rollcall", NULL, on_meeting_request_failed },
    { "rpc_docsharestart", NULL, on_meeting_request_failed },
    { "rpc_docshareend", NULL, on_meeting_request_failed },
    { "rpc_docsharedraw", NULL, on_meeting_request_failed },
    { "rpc_docsharedownload", NULL, on_meeting_request_failed },
    { "rpc_endmeeting", NULL, on_meeting_request_failed },
    { "rpc_controllerping", on_controller_ping, on_controller_ping_failed },
    { "rpc_getdocserver", on_controller_get_doc_server, NULL },
    { "rpc_logevent", NULL, on_meeting_request_failed },
    { NULL, NULL, NULL }
};

struct rpc_method_st c_method_list[] =
{
    { "error", on_server_error },
    { "Error", on_server_meeting_event },
 	{ "MeetingPending", on_server_meeting_state },
	{ "MeetingModified", on_server_meeting_info },
	{ "MeetingEnded", on_server_meeting_event },
	{ "MeetingJoined", on_server_meeting_state },
	{ "MeetingLeft", on_server_meeting_event },
	{ "MeetingInvite", on_server_meeting_invite },
	{ "SubmeetingAdded", on_server_submeeting_state },
	{ "SubmeetingRemoved", on_server_submeeting_state },
	{ "ParticipantAdded", on_server_participant_state },
	{ "ParticipantRemoved", on_server_meeting_event },
	{ "ParticipantMoved", on_server_participant_state },
	{ "ParticipantJoined", on_server_participant_state },
	{ "ParticipantLeft", on_server_participant_state },
	{ "ParticipantState", on_server_participant_state },
	{ "UserState", NULL },          // not used
	{ "ConnectionState", NULL },     // not used
	{ "AppShareStarted", on_server_app_share },
	{ "AppShareEnded", on_server_app_share },
	{ "PresenterGranted", on_server_app_share },
	{ "PresenterRevoked", on_server_app_share },
	{ "ControlGranted", on_server_app_share },
	{ "ControlRevoked", on_server_app_share },
	{ "MeetingStarted", on_server_meeting_event },
	{ "MeetingReset", on_server_meeting_state },
	{ "MeetingChat", on_server_meeting_chat },
	{ "DocShareStarted", on_server_doc_share },
	{ "DocShareEnded", on_server_doc_share },
	{ "DocShareDownloadPage", on_server_doc_share },
	{ "DocShareConfigure", on_server_doc_share },
	{ "DocShareDrawing", on_server_doc_share },
	{ "PPTAction", on_server_ppt_share },
	{ "RecordingNotAvailable", NULL },   // sent as event only or app share event
	{ "MeetingWaiting", on_server_meeting_event },
	{ "RecordingEnabled", NULL },   // used?
	{ "RecordingDisabled", NULL },  // used?
	{ NULL, NULL }
};
