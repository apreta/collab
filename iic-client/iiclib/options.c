/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef _WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>

#include <apr.h>
#include <apr_general.h>
#include <apr_pools.h>
#include <apr_strings.h>
#include <apr_hash.h>
#include <apr_tables.h>
#include <apr_file_io.h>

#define NO_DEFINE_APR

#include "session_api.h"
#include "options.h"
#include "utils.h"

#include <netlib/log.h>


#define MAX_LINE 1024


struct options_st
{
    apr_pool_t * pool;

    pstring base_name;
    pstring screen_name;
    pstring user_dir;
    pstring global_dir;

    pstring def_options_file;
    pstring sys_options_file;
    pstring user_options_file;

    apr_table_t *sys_table;
    apr_table_t *user_table;
};


options_t options_create(const char *base_name, const char *screen_name, const char *user_dir, const char *global_dir)
{
    apr_pool_t * newpool;
    options_t options = NULL;

    apr_pool_create(&newpool, NULL);
    options = apr_pcalloc(newpool, sizeof(struct options_st));
    options->pool = newpool;

    options->base_name = apr_pstrdup(newpool, base_name);
    options->screen_name = apr_pstrdup(newpool, screen_name);
    options->user_dir = apr_pstrdup(newpool, user_dir);
    options->global_dir = apr_pstrdup(newpool, global_dir);

    options->def_options_file = apr_psprintf(newpool, "%s/%s-def.opt", global_dir, base_name);
    options->sys_options_file = apr_psprintf(newpool, "%s/%s.opt", user_dir, base_name);
    options->user_options_file = apr_psprintf(newpool, "%s/%s.opt", user_dir, screen_name);

    // Check for directory & create
    apr_dir_make_recursive(user_dir, APR_OS_DEFAULT, newpool);

    return options;
}

int options_set_screen_name( options_t opts, const char *screen_name, const char *user_dir )
{
    opts->user_options_file = apr_psprintf(opts->pool, "%s/%s.opt", user_dir, screen_name);
    return 0;
}

#define FILENOTFOUND 1
#define FILEREADERROR -1

// Read file into table
int read_file(options_t opts, const char *filename, apr_table_t ** table)
{
    apr_file_t * file;
    int stat;
    char line[MAX_LINE];

    *table = apr_table_make(opts->pool, 20);

    stat = apr_file_open(&file, filename, APR_READ, APR_FPROT_OS_DEFAULT, opts->pool);
    if (stat != APR_SUCCESS)
    {
        errorf("Unable to open option file: %s [%d]", filename, stat);
        return FILENOTFOUND;
    }

    // format is "param","setting"
    stat = apr_file_gets(line, MAX_LINE, file);

    while (stat == APR_SUCCESS)
    {
        char *name, *start_name=NULL, *end_name=NULL;
        char *value, *start_value=NULL, *end_value=NULL;

        name = line;
        value = strchr(line, ',');

        if (value != NULL)
        {
            *(value++) = '\0';

            start_name = strchr(name, '\"');
            end_name = strrchr(name, '\"');

            start_value = strchr(value, '\"');
            end_value = strrchr(value, '\"');
        }

        if (start_name == NULL || end_name == NULL || start_value == NULL || end_value == NULL)
        {
            errorf("Could not parse line from options file %s: %s", filename, line);
            apr_file_close(file);
            return FILEREADERROR;
        }

        // Set positions past quotes.
        ++start_value;
        ++start_name;
        *end_name = '\0';
        *end_value = '\0';

//        debugf("Reading option %s as %s", start_name, start_value);

        apr_table_set(*table, start_name, start_value);

        stat = apr_file_gets(line, MAX_LINE, file);
    }

    if (stat != APR_EOF)
    {
        errorf("Unable to read from options file [%d]", stat);
        apr_file_close(file);
        return FILEREADERROR;
    }

    apr_file_close(file);

    return 0;
}

int options_read(options_t opts)
{
    apr_table_t *global;
    apr_table_t *sys;
    apr_table_t *user;
    int stat;

    // Must be present
    stat = read_file(opts, opts->def_options_file, &global);
    if (stat)
        return stat;

    // These can be missing, but should parse correctly if present
    stat = read_file(opts, opts->sys_options_file, &sys);
    if (stat == FILEREADERROR)
        return stat;
    stat = read_file(opts, opts->user_options_file, &user);
    if (stat == FILEREADERROR)
        return stat;

    opts->user_table = user;

    // Default system settings overide system settings
    apr_table_overlap(sys, global, APR_OVERLAP_TABLES_SET);

    opts->sys_table = sys;

    return 0;
}

int table_do_callback(void *rec, const char *key, const char *value)
{
    char line[MAX_LINE+1];
    int stat;

    sprintf(line, "\"%s\",\"%s\"\n", key, value);

    stat = apr_file_puts(line, (apr_file_t *)rec);

    if (stat)
    {
        errorf("Unable to write option %s", line);
        return FALSE;
    }

    return TRUE;
}

int write_file(options_t opts, const char * filename, apr_table_t * table)
{
    apr_file_t * file;
    int stat;

    stat = apr_file_open(&file, filename, APR_WRITE|APR_CREATE|APR_TRUNCATE, APR_FPROT_OS_DEFAULT, opts->pool);
    if (stat != APR_SUCCESS)
    {
        errorf("Unable to open option file: %s [%d]", filename, stat);
        return -1;
    }

    stat = apr_table_do(table_do_callback, file, table, NULL);

    apr_file_close(file);

    return stat != TRUE;
}

int options_write(options_t opts)
{
    write_file(opts, opts->sys_options_file, opts->sys_table);
    write_file(opts, opts->user_options_file, opts->user_table);

    return 0;
}

const char *options_get_user(options_t opts, const char *name, const char *def)
{
    const char *val = apr_table_get(opts->user_table, name);
    return val != NULL ? val : def;
}

void options_set_user(options_t opts, const char *name, const char *value)
{
    //debugf("Setting user option %s to %s", name, value);
    apr_table_set(opts->user_table, name, value);
}

void options_clr_user(options_t opts, const char * name)
{
    apr_table_unset(opts->user_table, name);
}

const char *options_get_system(options_t opts, const char *name, const char *def)
{
    const char *val = apr_table_get(opts->sys_table, name);
    return val != NULL ? val : def;
}

void options_set_system(options_t opts, const char *name, const char *value)
{
    //debugf("Setting system option %s to %s", name, value);
    apr_table_set(opts->sys_table, name, value);
}

