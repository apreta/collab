/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef ADDRESSBK_H_INCLUDED
#define ADDRESSBK_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#ifndef NO_DEFINE_APR
//typedef struct apr_pool_t apr_pool_t;
typedef struct apr_array_header_t apr_array_header_t;
typedef struct apr_hash_t apr_hash_t;
#endif

#include "session_api.h"

/*
   Contact and user data is mainly static as there are no events sent by the server when this
   data is updated.  However, if contacts are modified locally, those changes will be reflected.

   Most APIs are asynchronous; the appropriate callback will be called when the
   data has been received from the server.
*/

typedef struct directory_st * directory_t;


// Information about a contact
struct contact_st
{
    // private
    directory_t directory;

    // public
    pstring     userid;
    pstring     username;           // full jabber id
    pstring     screenname;

    pstring     title;
    pstring     first;
    pstring     middle;
    pstring     last;
    pstring     suffix;
    pstring     company;
    pstring     jobtitle;
    pstring     address1;
    pstring     address2;
    pstring     city;               // missing on server, oops
    pstring     state;
    pstring     country;
    pstring     postalcode;

    pstring     email;
    pstring     email2;
    pstring     email3;

    pstring     busphone;
    pstring     homephone;
    pstring     mobilephone;
    pstring     otherphone;

    pstring     aimscreen;
    pstring     msnscreen;
    pstring     yahooscreen;

    pstring     user1;
    pstring     user2;

    /* Profile/permissions */
    int         type;               // UserType enum
    pstring     profileid;
    int         enabledfeatures;    // unused
    int         disabledfeatures;   // unused
    int         defphone;           // SelectedPhone enum
    BOOL        isbuddy;

    BOOL        removed;            // contact was deleted locally
    BOOL        readonly;           // contact is read only
    
    pstring     displayname;
};
typedef struct contact_st *contact_t;


// A named list of contacts
struct group_st
{
    // private
    directory_t directory;
    apr_array_header_t *members;  // array of user-id strings

    // public
    pstring     group_id;
    pstring     name;
    int         type;
    BOOL        removed;
    BOOL        readonly;
};
typedef struct group_st *group_t;


// A list of contacts and groups
struct directory_st
{
    // private
    apr_pool_t *pool;
    int         references;
    apr_hash_t *contacts;       // key is user id
    apr_hash_t *groups;         // key is group id

    // public
    int         type;
    pstring     directory_id;
    pstring     name;
    group_t     all_group;
    group_t     buddies_group;
    int         enabled;

    int         rows_fetched;
    int         rows_available;
    
    int         contact_last_mod;
    int         group_last_mod;
};


// Information returned by authentication API
struct auth_info_st
{
    // private
    apr_pool_t *pool;

    // public
    pstring     session_id;
    pstring     user_id;
    pstring     username;
    pstring     community_id;
    pstring     profile_options;
    pstring     directory_id;
    pstring     group_id;
    BOOL        is_superadmin;
    pstring     busphone;
    pstring     homephone;
    pstring     mobilephone;
    pstring     otherphone;
    pstring     defmeeting;
    pstring     first;
    pstring     last;
    pstring     display_name;
    pstring     bridge_phone;
    pstring     system_url;
    int         user_type;
    int         enabled_features;
    int         defphone;
};
typedef struct auth_info_st * auth_info_t;


// Profile information
struct profile_st
{
    // private
    pstring     options;                    // undecoded options from server

    // public
    pstring     profile_id;
    pstring     name;
    unsigned    enabled_features;           // see ProfileFlags enum in iiclib/common.h
    int         max_voice_size;             // currently not used
    int         max_data_size;              // currently not used
    int         max_presence_monitors;
    int         max_scheduled_meetings;
    int         pin_length;
    int         meeting_id_length;
    int         password_length;
    int         max_idle_minutes;
    int         max_connect_minutes;

    BOOL        removed;                    // profile was deleted locally
};
typedef struct profile_st * profile_t;


struct recording_info_st
{
    // public
    pstring     meeting_id;
    int         time;                       // time_t value for meeting instance
    pstring     time_string;                // YYYY-MM-DD-HH-MM-SS value for meeting instance
    pstring     folder_file;

    // Any of the following may be NULL if the corresponding data is not available for this meeting.
    pstring     data_file;
    pstring     voice_file;
    pstring     chat_file;
    pstring     summary_file;
};
typedef struct recording_info_st * recording_info_t;


struct document_info_st
{
    // public
    pstring     meeting_id;
    pstring     name;
    pstring     url;
};
typedef struct document_info_st * document_info_t;



typedef struct addressbk_st * addressbk_t;
typedef struct directory_iter_st * directory_iter_t;
typedef struct contact_iterator_st * contact_iterator_t;
typedef struct group_iterator_st * group_iterator_t;
typedef struct profile_iterator_st * profile_iterator_t;
typedef struct contact_condition_st * contact_condition_t;
typedef struct recording_list_st * recording_list_t;
typedef struct document_list_st * document_list_t;


// Create address book object
addressbk_t addressbk_create(session_t sess);

// Destory address book book
void addressbk_destroy(addressbk_t bk);

// Remove cached contact data from address book
void addressbk_flush(addressbk_t bk);

// Define callback types
typedef void (*addressbk_auth_handler)(void *);
typedef void (*addressbk_version_check_handler)(void *, int upgrade_type, const char *url);
typedef void (*addressbk_directory_fetched_handler)(void *user, const char *opid, directory_t directory);
typedef void (*addressbk_profiles_fetched_handler)(void *user, const char *opid);
typedef void (*addressbk_updated_handler)(void *user, const char *opid, const char *item_id);
typedef void (*addressbk_deleted_handler)(void *user, const char *opid, const char *item_id);
typedef void (*addressbk_update_failed_handler)(void *user, const char *opid, int status, const char *msg);
typedef void (*addressbk_recording_list_handler)(void *user, const char *opid, recording_list_t list);
typedef void (*addressbk_document_list_handler)(void *user, const char *opid, document_list_t list);
typedef void (*addressbk_progress_handler)(void *user, const char *opid, int num_fetched, int total);

// Set callback handlers
void addressbk_set_auth_handler(addressbk_t, void *user, addressbk_auth_handler _handler);
void addressbk_set_version_check_handler(addressbk_t, void *user, addressbk_version_check_handler _handler);
void addressbk_set_directory_fetched_handler(addressbk_t, void *user, addressbk_directory_fetched_handler _handler);
void addressbk_set_profiles_fetched_handler(addressbk_t, void *user, addressbk_profiles_fetched_handler _handler);
void addressbk_set_updated_handler(addressbk_t, void *user, addressbk_updated_handler _handler);
void addressbk_set_deleted_handler(addressbk_t, void *user, addressbk_deleted_handler _handler);
void addressbk_set_update_failed_handler(addressbk_t, void *user, addressbk_update_failed_handler _handler);
void addressbk_set_recording_list_handler(addressbk_t, void *user, addressbk_recording_list_handler _handler);
void addressbk_set_document_list_handler(addressbk_t, void *user, addressbk_document_list_handler _handler);
void addressbk_set_progress_handler(addressbk_t, void *user, addressbk_progress_handler _handler);

// Authenticate with address book.  The Jabber server really authenticates the password,
// this API returns info that will be required to make other API calls.
int addressbk_authenticate(addressbk_t bk, const char *screenname, const char *community);

// Check client version number.
// For some reason, the version number must be formatted as "#, #, #, #".
// In callback, upgrade_type is -1 if an error occured, other values defined in iiclib/common.h.
int addressbk_check_version(addressbk_t bk, const char *version);

// Get authentication info; addressbk_authenticate must have completed first.
auth_info_t addressbk_get_auth_info(addressbk_t bk);

// Get profile for user; addressbk_authenticate must have completed first.
// The profile name and id are not available.
profile_t addressbk_get_profile(addressbk_t bk);

// Get cached directory info if it has already been fetched (caller should add reference if it wished to keep it)
directory_t addressbk_get_directory(addressbk_t bk, int dir_type, BOOL create);

// Set list of fields we're interested in downloading
int addressbk_set_field_list(addressbk_t bk, const char **field_names, int num_names);

// Fetch my contacts from global or personal address book
int addressbk_fetch_contacts(addressbk_t bk, const char *opid, int directory_type);

// Fetch contacts from community that match my buddies (so I can get full address information)
int addressbk_fetch_buddies(addressbk_t bk, const char *opid);

// Fetch all fields for contact
int addressbk_fetch_contact_details(addressbk_t bk, const char *opid, const char *user_id);

// Update/add user or contact.
// Password only needs to be specific if adding new user or updating existing user's password.
// For new users, this API will contruct the username from the screenname.  Leave the userid blank.
// API assumes ownership of contact structure; will add to appropriate directory if succeeds.
int addressbk_contact_update(addressbk_t bk, const char *opid, int dir_type, contact_t contact, const char *password);

// Update password then send an email to user with latest account info
int addressbk_email_account_info( addressbk_t bk, contact_t contact, const char *password );

// Delete contact.
int addressbk_contact_delete(addressbk_t bk, const char *opid, contact_t contact);

// Search for contacts on server based on criteria
// Conditions are: =, >, <, <>, LIKE, LIKE-NOCASE
// '%' can be used in value as wild-card
// Call again with new starting_row to fetch next chunk; results will be appended to same result list
// opid must be unique for each search with different criteria
int addressbk_search_contacts(addressbk_t bk, const char *opid, contact_condition_t conditions, const char *sort_field,
                              int starting_row, int chunk_size);

// Find results for an already-run query
directory_t addressbk_find_search_results(addressbk_t bk, const char *opid);

// Fetch profile information
// Note that the current user's profile is available from addressbk_get_profile after authentication completes.
int addressbk_fetch_profiles(addressbk_t bk, const char *opid);

// Update/add profile
// Leave profile_id unspecified for new profile.
// API assumes ownership of profile structure; will be added to profile list if operation succeeds.
int addressbk_profile_update(addressbk_t bk, const char *opid, profile_t profile);

// Delete profile
int addressbk_profile_delete(addressbk_t bk, const char *opid, profile_t profile);

// Add a group
// API assumes ownership of group; will be added to directory if operation succeeds.
int addressbk_group_update(addressbk_t bk, const char *opid, int dir_type, group_t group);

// Delete group
int addressbk_group_delete(addressbk_t bk, const char *opid, group_t group);

// Add/remove members from groups.
int addressbk_group_add_member(addressbk_t bk, const char *opid, group_t group, const char *userid);
int addressbk_group_remove_member(addressbk_t bk, const char *opid, group_t group, const char *userid);

// Add buddy
int addressbk_buddy_add(addressbk_t bk, const char *opid, contact_t contact, BOOL search_all_communities);

// Remove buddy
int addressbk_buddy_remove(addressbk_t bk, const char *opid, contact_t contact);

// Get list of recordings associaed with the indicated meeting.
int addressbk_get_recording_list(addressbk_t bk, const char *opid, const char *meeting_id);

// Get list of documents associaed with the indicated meeting.
int addressbk_get_document_list(addressbk_t bk, const char *opid, const char *meeting_id);

// Remove meeting recording.  Pass "folder_file" obtained from a meeting_info_t to specify which recording.
int addressbk_recording_remove(addressbk_t bk, const char *opid, const char *meeting_id, const char *folder_file);

// Find contact accross directories
contact_t addressbk_find_contact(addressbk_t bk, const char *userid);

// Read/Write contact info to local cache file.
int addressbk_enable_cache(addressbk_t bk, const char *dir);


// Functions for building condition list
contact_condition_t contact_condition_new();
void                contact_condition_add_directory(contact_condition_t, int directory_type);
void                contact_condition_add_field(contact_condition_t, const char * field, const char * condition, const char * value);
void                contact_condition_destroy(contact_condition_t);


// Accessor functions for directories
directory_iter_t    directory_iterate(addressbk_t bk);
directory_t         directory_next(addressbk_t bk, directory_iter_t iter);

// Create new directory and add to address book
directory_t         directory_store(addressbk_t bk, const char *name);

void                directory_add_reference(directory_t dir);
void                directory_remove_reference(directory_t dir);
pstring             directory_string(directory_t dir, const char *str);

contact_iterator_t  directory_contact_iterate(directory_t dir);
contact_t           directory_contact_next(directory_t dir, contact_iterator_t iter);
contact_t           directory_contact_new(directory_t dir);
contact_t           directory_contact_copy(addressbk_t bk, directory_t dir, contact_t contact);

// Create new contact with specified userid and add to directory
contact_t           directory_contact_store(directory_t dir, const char *userid);
int                 directory_contact_set_field(contact_t contact, const char *field, const char *value);

group_iterator_t    directory_group_iterate(directory_t dir);
group_t             directory_group_next(directory_t dir, group_iterator_t iter);
group_t             directory_group_new(directory_t dir, const char *name);

int                 directory_group_get_size(group_t group);
const char *        directory_group_get_member(group_t group, int member);

// Create new group with specified id and name, and add to directory
group_t             directory_group_store(directory_t dir, const char *groupid, const char *name, int type);
int                 directory_group_add_member(group_t group, const char *userid);

contact_t           directory_find_contact(directory_t dir, const char *userid);
contact_t           directory_find_contact_by_screenname(directory_t dir, const char *screen);
contact_t           directory_find_contact_by_field(directory_t dir, const char *field, const char *value);
group_t             directory_find_group(directory_t dir, const char *groupid);
group_t             directory_find_group_by_name(directory_t dir, const char *name);



// Access functions for profiles
profile_iterator_t  profiles_iterate(addressbk_t bk);
profile_t           profiles_next(addressbk_t bk, profile_iterator_t iter);
profile_t           profiles_find_profile(addressbk_t bk, const char *profileid);
profile_t           profiles_new(addressbk_t bk);
pstring             profiles_string(addressbk_t bk, const char *str);


// Access functions for recording list
int                 recording_list_get_size(recording_list_t);
recording_info_t    recording_list_get_recording(recording_list_t list, int row);
void                recording_list_destroy(recording_list_t);


// Access functions for document list
int                 document_list_get_size(document_list_t);
document_info_t     document_list_get_recording(document_list_t list, int row);
void                document_list_destroy(document_list_t);


enum addressbk_errors
{
    ERR_ADDRESSBK = -799,
    ERR_UNKNOWNFIELD,
    ERR_USERIDNOTFETCHED,
    ERR_DIRECTORYNOTFETCHED,
    ERR_DUPLICATEOPID,
    ERR_MISSINGFIELD,
    ERR_NOTADMIN,
    ERR_NOTUSERGROUP,
    ERR_CACHEWRITE,
    ERR_CACHEREAD,
};

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif // ADDRESSBK_H_INCLUDED
