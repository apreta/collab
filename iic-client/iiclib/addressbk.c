/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef _WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>
#include <string.h>

#include <apr.h>
#include <apr_general.h>
#include <apr_pools.h>
#include <apr_strings.h>
#include <apr_hash.h>
#include <apr_tables.h>

#include <json.h>

#define NO_DEFINE_APR
#include "addressbk.h"
#include "handlerdefs.h"
#include "utils.h"

#include "../../iic/services/common/common.h"
#include "../../iic/services/common/profile.h"

#include <netlib/log.h>

#define CACHE_VERSION 1


// Private addressbk session info.
struct addressbk_st
{
    session_t session;
    int chunk_size;
    int api_ver;    /* 1 = Zon 8.0 compat, 2 = send password to authenicate API, 3 = support packed (json) data forms, 4 = sync API */

    const char *cache_dir;

    const char **fetch_fields;
    int num_fetch_fields;

    const char *fetch_global;
    const char *fetch_personal;

    const char *global_directory_id;
    BOOL global_subscribed;

    const char *personal_directory_id;
    BOOL personal_subscribed;

    auth_info_t auth_info;
    profile_t   auth_profile;
    directory_t global_directory;
    directory_t personal_directory;
    apr_hash_t *directories;
    apr_hash_t *searches;
    apr_hash_t *profiles;

    apr_hash_t *operations;

    DECLARE_EVENT_HANDLER(addressbk,auth)
    DECLARE_EVENT_HANDLER(addressbk,version_check)
    DECLARE_EVENT_HANDLER(addressbk,directory_fetched)
    DECLARE_EVENT_HANDLER(addressbk,profiles_fetched)
    DECLARE_EVENT_HANDLER(addressbk,updated)
    DECLARE_EVENT_HANDLER(addressbk,deleted)
    DECLARE_EVENT_HANDLER(addressbk,update_failed)
    DECLARE_EVENT_HANDLER(addressbk,recording_list)
    DECLARE_EVENT_HANDLER(addressbk,document_list)
    DECLARE_EVENT_HANDLER(addressbk,progress)

    apr_pool_t *pool;
};

// Condition for contact search
struct contact_condition_st
{
    apr_pool_t *pool;

    BOOL search_personal;
    BOOL search_global;

    apr_array_header_t *fields;
    apr_array_header_t *conditions;
    apr_array_header_t *values;
};


struct operation_st
{
    apr_pool_t *pool;
    pstring opid;

    union
    {
        contact_t contact;
        struct
        {
            profile_t profile;
            BOOL update;
        } profile_operation;
        struct
        {
            group_t group;
            const char *userid;
        } group_operation;
        struct{
            int contact_modified;
            int group_modified;
        } fetch_operation;
    };
};
typedef struct operation_st * operation_t;


struct recording_list_st
{
    apr_pool_t *pool;

    apr_array_header_t *items;
};


struct document_list_st
{
    apr_pool_t *pool;

    apr_array_header_t *items;
};


//
// Set up mapping from RPC IDs and RPC method names to handler functions.
// Tables are defined at end of file.
//

// RPC result and error messages
struct rpc_handler_st
{
    const char * rpc_id;
    session_rpc_result_handler result_handler;
    session_rpc_error_handler error_handler;
};
extern struct rpc_handler_st a_handler_list[];    // Wanted this to be static but VC++ won't allow it

// RPC request messages
struct rpc_method_st
{
    const char * method;
    session_rpc_request_handler request_handler;
};
extern struct rpc_method_st a_method_list[];

// Contact fields
struct contact_field_st
{
    const char *dbfield;
    const char *field;
    int offset;
    int type;   // 1 = string, 2 = int
};
extern struct contact_field_st field_list[];


//
// Local function defs.
//
static void on_addressbk_auth_failed(void *user, const char *id, int code, const char *msg);
static void on_fetch_failed(void *user, const char *id, int code, const char *msg);
static int addressbk_fetch_directories(addressbk_t bk, const char *opid);
static const char *get_servername(addressbk_t bk);
static BOOL decode_profile(profile_t profile, char *aProfileOptions);
static BOOL encode_profile(profile_t profile, char *out, int out_len);
static struct contact_field_st * find_dbfield_def(const char *name);
static struct contact_field_st * find_field_def(const char *name);
static BOOL is_admin_session(addressbk_t bk);
static operation_t make_operation(addressbk_t bk, const char *opid, BOOL create, BOOL * created_flag);
static void flush_operation(addressbk_t bk, const char *opid);
static directory_t make_directory(addressbk_t bk, int dir_type, const char *id, BOOL create);
static contact_t make_contact(directory_t dir, const char *userid, BOOL create);
static void set_displayname(contact_t contact);
static group_t make_group(directory_t dir, const char * groupid, BOOL create);
static void add_group_member(group_t group, const char *userid);
static profile_t make_profile(addressbk_t bk, const char *id, BOOL create);
static recording_list_t make_recording_list(addressbk_t bk);
static document_list_t make_document_list(addressbk_t bk);
static int directory_write(addressbk_t bk, int dir_type);
static int directory_read(addressbk_t bk, int dir_type);

//
// Declare APIs to set event callbacks
//

void default_on_auth(void *u) { }
IMPL_SET_HANDLER(addressbk,auth)
void default_on_version_check(void *u, int updrage, const char *url) { }
IMPL_SET_HANDLER(addressbk,version_check)
void default_on_directory_fetched(void *u, const char *opid, directory_t dir) { }
IMPL_SET_HANDLER(addressbk,directory_fetched)
void default_on_profiles_fetched(void *u, const char *opid) { }
IMPL_SET_HANDLER(addressbk,profiles_fetched)
void default_on_updated(void *u, const char *opid, const char *itemid) { }
IMPL_SET_HANDLER(addressbk,updated)
void default_on_deleted(void *u, const char *opid, const char *itemid) { }
IMPL_SET_HANDLER(addressbk,deleted)
void default_on_update_failed(void *u, const char *opid, int code, const char *msg) { }
IMPL_SET_HANDLER(addressbk,update_failed)
void default_on_recording_list(void *u, const char *opid, recording_list_t list) { }
IMPL_SET_HANDLER(addressbk,recording_list)
void default_on_document_list(void *u, const char *opid, document_list_t list) { }
IMPL_SET_HANDLER(addressbk,document_list)
void default_on_progress(void *u, const char *opid, int num, int total) { }
IMPL_SET_HANDLER(addressbk,progress)


int addressbk_initialize()
{
    return 0;
}

static void on_session_message(void *user, session_message_t msg)
{
    addressbk_t bk = (addressbk_t)user;

    if (strcmp(msg->id, "session-reset") == 0)
    {
        addressbk_flush(bk);
    }
    else if (strcmp(msg->id, "contact_by_screen") == 0)
    {
        if (bk->global_directory)
        {
            msg->ptr_data = directory_find_contact_by_screenname(bk->global_directory, msg->str_data);
        }
        if (bk->personal_directory && msg->ptr_data == NULL)
        {
            msg->ptr_data = directory_find_contact_by_screenname(bk->personal_directory, msg->str_data);
        }
    }
}

// Create address book object
addressbk_t addressbk_create(session_t sess)
{
    addressbk_t addressbk = (addressbk_t)calloc(1,sizeof(struct addressbk_st));
    int i;

    addressbk->session = sess;
    addressbk->chunk_size = 200;

    addressbk->on_auth = default_on_auth;
    addressbk->on_version_check = default_on_version_check;
    addressbk->on_directory_fetched = default_on_directory_fetched;
    addressbk->on_profiles_fetched = default_on_profiles_fetched;
    addressbk->on_updated = default_on_updated;
    addressbk->on_deleted = default_on_deleted;
    addressbk->on_update_failed = default_on_update_failed;
    addressbk->on_recording_list = default_on_recording_list;
    addressbk->on_document_list = default_on_document_list;
    addressbk->on_progress = default_on_progress;

    apr_pool_create(&addressbk->pool, NULL);

    addressbk->directories = apr_hash_make(addressbk->pool);
    addressbk->searches = apr_hash_make(addressbk->pool);
    addressbk->profiles = apr_hash_make(addressbk->pool);
    addressbk->operations = apr_hash_make(addressbk->pool);

    // don't allocate in pool, we don't want pointer to change
    addressbk->auth_info = (struct auth_info_st*) calloc(1, sizeof(struct auth_info_st));
    apr_pool_create(&addressbk->auth_info->pool, NULL);

    addressbk->auth_profile = apr_pcalloc(addressbk->auth_info->pool, sizeof(struct profile_st));

    i = -1;
    while (a_handler_list[++i].rpc_id)
    {
        session_set_rpc_handlers(sess, addressbk, a_handler_list[i].rpc_id, a_handler_list[i].result_handler, a_handler_list[i].error_handler);
    }

    i = -1;
    while (a_method_list[++i].method)
    {
        session_set_rpc_request_handler(sess, addressbk, a_method_list[i].method, a_method_list[i].request_handler);
    }

    session_add_message_handler(sess, addressbk, on_session_message);

    return addressbk;
}

// Destroy address book; will also destroy directories, contacts, etc.
void addressbk_destroy(addressbk_t bk)
{
    session_remove_message_handler(bk->session, bk, on_session_message);

    apr_pool_destroy(bk->pool);
    apr_pool_destroy(bk->auth_info->pool);
    free(bk->auth_info);
    free(bk);
}

void addressbk_flush(addressbk_t bk)
{
    apr_pool_clear(bk->pool);
    bk->fetch_global = NULL;
    bk->fetch_personal = NULL;
    bk->global_directory_id = NULL;
    bk->global_subscribed = FALSE;
    bk->personal_directory_id = NULL;
    bk->personal_subscribed = FALSE;
    bk->personal_directory = NULL;
    bk->global_directory = NULL;
    bk->directories = apr_hash_make(bk->pool);
    bk->searches = apr_hash_make(bk->pool);
    bk->profiles = apr_hash_make(bk->pool);
    bk->operations = apr_hash_make(bk->pool);
}

// Set list of fields we're interested in downloading
int addressbk_set_field_list(addressbk_t bk, const char **field_names, int num_names)
{
    int i;

    bk->fetch_fields = calloc(1, num_names * sizeof(char *));
    bk->num_fetch_fields = 0;

    for (i=0; i<num_names; ++i)
    {
        struct contact_field_st * def = find_field_def(field_names[i]);
        if (def == NULL || *def->dbfield == '\0')
            return ERR_UNKNOWNFIELD;

        bk->fetch_fields[i] = def->dbfield;
    }

    bk->num_fetch_fields = num_names;

    return 0;
}


// We're not really authenticating here, the Jabber server handles that...but we do need information
// about this user to use other APIs which this API returns.
// N.B. community is only specified in the case a super-admin signs on to a different community.
int addressbk_authenticate(addressbk_t bk, const char *screenname, const char *community)
{

    if (bk->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
    else
	{
	    const char *passwd = bk->api_ver < 2 ? "" : session_get_password(bk->session);

		// Pass blank password; we've already authenticated with jabber.
		if (session_make_rpc_call(bk->session, "rpc_auth", "addressbk", "addressbk.authenticate_user", "(sss)",
                        screenname,
                        passwd,
                        community))
		{
		    debugf("Addressbook authorization call failed");
            return ERR_RPCFAILED;
		}
	}

    return 0;
}

void on_addressbk_auth(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;

	char *_sessionid, *_userid, *_username, *_communityid, *_profileoptions, *_directoryid, *_groupid, *issuperadmin;
	char *busphone, *homephone, *mobilephone, *otherphone;
	char *defmeeting, *first, *last, *systemphone, *systemurl;
    int usertype, enabledfeatures, defphone;
	resultset_get_output_values(data, "(ssssssisisisssssssss)", &_sessionid, &_userid, &_username, &_communityid, &_directoryid, &_groupid, &usertype, &issuperadmin, &enabledfeatures, &_profileoptions,
				&defphone, &busphone, &homephone, &mobilephone, &otherphone, &defmeeting, &first, &last, &systemphone, &systemurl);
	if (strlen(_userid) == 0)
	{
	    on_addressbk_auth_failed(user, rpc_id, DefaultErrorCode, "Address book authentication failed");
	}
	else
	{
	    apr_pool_clear(bk->auth_info->pool);
	    bk->auth_info->session_id = apr_pstrdup(bk->auth_info->pool, _sessionid);
        bk->auth_info->user_id = apr_pstrdup(bk->auth_info->pool, _userid);
        bk->auth_info->username = apr_pstrdup(bk->auth_info->pool, _username);
        bk->auth_info->community_id = apr_pstrdup(bk->auth_info->pool, _communityid);
        bk->auth_info->directory_id = apr_pstrdup(bk->auth_info->pool, _directoryid);
        bk->auth_info->group_id = apr_pstrdup(bk->auth_info->pool, _groupid);
		bk->auth_info->busphone = apr_pstrdup(bk->auth_info->pool, busphone);
		bk->auth_info->homephone = apr_pstrdup(bk->auth_info->pool, homephone);
		bk->auth_info->mobilephone = apr_pstrdup(bk->auth_info->pool, mobilephone);
		bk->auth_info->otherphone = apr_pstrdup(bk->auth_info->pool, otherphone);
		bk->auth_info->defmeeting = apr_pstrdup(bk->auth_info->pool, defmeeting);
		bk->auth_info->first = apr_pstrdup(bk->auth_info->pool, first);
		bk->auth_info->last = apr_pstrdup(bk->auth_info->pool, last);
        bk->auth_info->bridge_phone = apr_pstrdup(bk->auth_info->pool, systemphone);
        bk->auth_info->system_url = apr_pstrdup(bk->auth_info->pool, systemurl);
        bk->auth_info->user_type = usertype;
        bk->auth_info->enabled_features = enabledfeatures;
        bk->auth_info->defphone = defphone;
        bk->auth_info->display_name = apr_psprintf(bk->auth_info->pool, "%s %s", first, last);

        bk->auth_profile = apr_pcalloc(bk->auth_info->pool, sizeof(struct profile_st));

        bk->auth_profile->enabled_features = enabledfeatures;
        decode_profile(bk->auth_profile, _profileoptions);

        bk->on_auth(bk->on_auth_data);
	}
}

static void on_addressbk_auth_failed(void *user, const char *id, int code, const char *msg)
{
    addressbk_t bk = (addressbk_t)user;

    debugf("Authentication failed: %s", msg);

    // Prob. should have seperate event
    bk->on_auth(bk->on_auth_data);
}

int addressbk_check_version(addressbk_t bk, const char *version)
{
    if (bk->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
    else if (session_make_rpc_call(bk->session,
                                "rpc_versionck",
                                "addressbk",
                                "addressbk.check_version",
                                "(s)",
                                version))
	{
		debugf("Check version call failed");
		return -1;
	}

	return 0;
}

void on_version_check(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;

	int  _clientUpgradeFound;
    char *_upgradeURL;
    char *_upgradeMessage;

    if (resultset_get_output_count(data) < 4)
        resultset_get_output_values(data, "(iss*)", &_clientUpgradeFound, &_upgradeURL, &_upgradeMessage);
    else
        resultset_get_output_values(data, "(issi*)", &_clientUpgradeFound, &_upgradeURL, &_upgradeMessage, &bk->api_ver);

    bk->on_version_check(bk->on_version_check_data, _clientUpgradeFound, _upgradeURL);
}

static void on_version_check_failed(void *user, const char *id, int code, const char *msg)
{
    addressbk_t bk = (addressbk_t)user;

    bk->on_version_check(bk->on_version_check_data, -1, "");
}

// Return authentication info we have cached
auth_info_t addressbk_get_auth_info(addressbk_t bk)
{
    return bk->auth_info;
}

// Return profile info we have cached
profile_t addressbk_get_profile(addressbk_t bk)
{
    return bk->auth_profile;
}

// Return directory info we have cached
directory_t addressbk_get_directory(addressbk_t bk, int dir_type, BOOL create)
{
    return make_directory(bk, dir_type, NULL, create);
}

int start_fetch_contacts(addressbk_t bk, const char *opid, int dir_type, int start_row)
{
    const char * session_id = bk->auth_info->session_id;
    const char * group_by = "directory";
    const char * directory_ids[1];
    session_array_t field_array;
    session_array_t directory_array;
    char rpc_id[256];
    operation_t oper = make_operation(bk, opid, FALSE, NULL);

    if (start_row == 0)
    {
        if (bk->cache_dir)
            directory_read(bk, dir_type);

        directory_t dir = make_directory(bk, dir_type, NULL, FALSE);
        oper->fetch_operation.contact_modified = dir ? dir->contact_last_mod : 0;
        oper->fetch_operation.group_modified = dir ? dir->group_last_mod : 0;
    }

    if (dir_type == dtGlobal)
    {
        sprintf(rpc_id, "rpc_fetchglobal:%s", opid);
        directory_ids[0] =  bk->global_directory_id;
    }
    else if (dir_type == dtPersonal)
    {
        sprintf(rpc_id, "rpc_fetchpersonal:%s", opid);
        directory_ids[0] = bk->personal_directory_id;
    }

    field_array = session_create_array(bk->num_fetch_fields, bk->fetch_fields);
    session_add_array_element(field_array, "s", "lastmodified");

    directory_array = session_create_array(1, directory_ids);

    if (bk->api_ver < 3)
    {
        // Get contacts call:
        //   USERID, DIRECTORIES, GROUPING, SORT, FIELDS, FIRST, CHUNKSIZE
        //
        if (session_make_rpc_call(bk->session,
                             rpc_id,
                             "addressbk",
                             "addressbk.fetch_contacts",
                             "(sVssVii)",
                             session_id,
                             directory_array,
                             group_by,
                             "",
                             field_array,
                             start_row,
                             bk->chunk_size))
        {
            debugf("Addressbook fetch contacts call failed");
            return ERR_RPCFAILED;
        }
    }
    else
    {
        // Extended fetch contacts, uses json and last modified date
        if (session_make_rpc_call(bk->session,
                             rpc_id,
                             "addressbk",
                             "addressbk.fetch_contacts_2",
                             "(sVViii)",
                             session_id,
                             directory_array,
                             field_array,
                             oper ? oper->fetch_operation.contact_modified : 0,
                             start_row,
                             bk->chunk_size))
        {
            debugf("Addressbook fetch contacts call failed");
            return ERR_RPCFAILED;
        }
    }

    session_free_array(field_array);
    session_free_array(directory_array);

    return 0;
}

int start_fetch_deleted_contacts(addressbk_t bk, const char *opid, int dir_type, int start_row)
{
    const char * session_id = bk->auth_info->session_id;
    const char * directory_ids[1];
    session_array_t directory_array;
    char rpc_id[256];
    operation_t oper = make_operation(bk, opid, FALSE, NULL);

    if (dir_type == dtGlobal)
    {
        sprintf(rpc_id, "rpc_deletedglobal:%s", opid);
        directory_ids[0] =  bk->global_directory_id;
    }
    else if (dir_type == dtPersonal)
    {
        sprintf(rpc_id, "rpc_deletedpersonal:%s", opid);
        directory_ids[0] = bk->personal_directory_id;
    }

    directory_array = session_create_array(1, directory_ids);

    // Get contacts call:
    //   USERID, DIRECTORIES, LASTMODIFIED, FIRST, CHUNKSIZE
    //
    if (session_make_rpc_call(bk->session,
                         rpc_id,
                         "addressbk",
                         "addressbk.fetch_deleted_contacts",
                         "(sViii)",
                         session_id,
                         directory_array,
                         oper ? oper->fetch_operation.contact_modified : 0,
                         start_row,
                         bk->chunk_size))
    {
        debugf("Addressbook fetch contacts call failed");
        return ERR_RPCFAILED;
    }

    session_free_array(directory_array);

    return 0;
}

int start_fetch_groups(addressbk_t bk, const char *opid, int dir_type, int start_row)
{
    const char * session_id = bk->auth_info->session_id;
    const char * directory_ids[1];
    session_array_t directory_array;
    char rpc_id[256];
    operation_t oper = make_operation(bk, opid, FALSE, NULL);

    if (dir_type == dtGlobal)
    {
        sprintf(rpc_id, "rpc_groupsglobal:%s", opid);
        directory_ids[0] = bk->global_directory_id;
    }
    else if (dir_type == dtPersonal)
    {
        sprintf(rpc_id, "rpc_groupspersonal:%s", opid);
        directory_ids[0] = bk->personal_directory_id;
    }

    directory_array = session_create_array(1, directory_ids);

    if (bk->api_ver < 3)
    {
        if (session_make_rpc_call(bk->session, rpc_id,
                                   "addressbk",
                                   "addressbk.fetch_groups",
                                   "(sVii)",
                                   session_id,
                                   directory_array,
                                   start_row,
                                   bk->chunk_size))
        {
            debugf("Addressbook fetch groups call failed");
            return ERR_RPCFAILED;
        }
    }
    else
    {
        if (session_make_rpc_call(bk->session, rpc_id,
                                   "addressbk",
                                   "addressbk.fetch_groups_2",
                                   "(sViii)",
                                   session_id,
                                   directory_array,
                                   oper ? oper->fetch_operation.group_modified : 0,
                                   start_row,
                                   bk->chunk_size))
        {
            debugf("Addressbook fetch groups call failed");
            return ERR_RPCFAILED;
        }
    }

    session_free_array(directory_array);

    return 0;
}

int start_fetch_deleted_groups(addressbk_t bk, const char *opid, int dir_type, int start_row)
{
    const char * session_id = bk->auth_info->session_id;
    const char * directory_ids[1];
    session_array_t directory_array;
    char rpc_id[256];
    operation_t oper = make_operation(bk, opid, FALSE, NULL);

    if (dir_type == dtGlobal)
    {
        sprintf(rpc_id, "rpc_delgroupsglobal:%s", opid);
        directory_ids[0] = bk->global_directory_id;
    }
    else if (dir_type == dtPersonal)
    {
        sprintf(rpc_id, "rpc_delgroupspersonal:%s", opid);
        directory_ids[0] = bk->personal_directory_id;
    }

    directory_array = session_create_array(1, directory_ids);

    if (session_make_rpc_call(bk->session, rpc_id,
                               "addressbk",
                               "addressbk.fetch_deleted_groups",
                               "(sViii)",
                               session_id,
                               directory_array,
                               oper ? oper->fetch_operation.group_modified : 0,
                               start_row,
                               bk->chunk_size))
    {
        debugf("Addressbook fetch deleted groups call failed");
        return ERR_RPCFAILED;
    }

    session_free_array(directory_array);

    return 0;
}

int start_fetch_members(addressbk_t bk, const char *opid, int dir_type, int start_row)
{
    const char * session_id = bk->auth_info->session_id;
    const char * directory_ids[1];
    session_array_t directory_array;
    char rpc_id[256];
    operation_t oper = make_operation(bk, opid, FALSE, NULL);

    if (dir_type == dtGlobal)
    {
        sprintf(rpc_id, "rpc_membersglobal:%s", opid);
        directory_ids[0] = bk->global_directory_id;
    }
    else if (dir_type == dtPersonal)
    {
        sprintf(rpc_id, "rpc_memberspersonal:%s", opid);
        directory_ids[0] = bk->personal_directory_id;
    }

    directory_array = session_create_array(1, directory_ids);

    if (bk->api_ver < 3)
    {
        if (session_make_rpc_call(bk->session, rpc_id,
                                        "addressbk",
                                        "addressbk.fetch_group_members",
                                        "(sVii)",
                                        session_id,
                                        directory_array,
                                        start_row,
                                        bk->chunk_size))
        {
            debugf("Addressbook fetch group members call failed");
            return ERR_RPCFAILED;
        }
    }
    else
    {
        if (session_make_rpc_call(bk->session, rpc_id,
                                        "addressbk",
                                        "addressbk.fetch_group_members_2",
                                        "(sViii)",
                                        session_id,
                                        directory_array,
                                        oper ? oper->fetch_operation.group_modified : 0,
                                        start_row,
                                        bk->chunk_size))
        {
            debugf("Addressbook fetch group members call failed");
            return ERR_RPCFAILED;
        }
    }

    session_free_array(directory_array);

    return 0;
}

// Fetch my contacts and groups from global or personal address book
int addressbk_fetch_contacts(addressbk_t bk, const char *opid, int dir_type)
{
    operation_t oper;
    BOOL new_op_flag;
    
    if (bk->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

    oper = make_operation(bk, opid, TRUE, &new_op_flag);
    if (!new_op_flag)
    {
        return ERR_DUPLICATEOPID;
    }
    
    // If we haven't got the directory id yet we'll need to fetch it.
    // (Probably should just return as part of auth call, see if adding will break classic client)
    if ((dir_type == dtGlobal && bk->global_directory_id == NULL) ||
        (dir_type == dtPersonal && bk->personal_directory_id == NULL))
    {
        BOOL start = !bk->fetch_personal && !bk->fetch_global;
        if (dir_type == dtGlobal) bk->fetch_global = apr_pstrdup(bk->pool, opid);
        if (dir_type == dtPersonal) bk->fetch_personal = apr_pstrdup(bk->pool, opid);
        if (start)
            return addressbk_fetch_directories(bk, opid);
        else
            return 0;
    }

    // Otherwise start fetch itself
    return start_fetch_contacts(bk, opid, dir_type, 0);
}

int parse_contact_result(addressbk_t bk, resultset_t data, int dir_type, directory_t dir, BOOL buddies)
{
    int i;

    if (dir == NULL)
        dir = make_directory(bk, dir_type, NULL, TRUE);

    for (i=0; i < resultset_get_chunk_size(data); i++)
	{
	    int loop;
        contact_t contact;
        const char *userid = NULL;

        // Find user id for this contact
        for (loop=0; loop < bk->num_fetch_fields; loop++)
        {
            const char *field = bk->fetch_fields[loop];
            if (_stricmp(field, "userid") == 0)
            {
                userid = resultset_get_value_by_col(data, i, loop);
                break;
            }
        }

        if (userid == NULL)
        {
            debugf("Must request userid field during fetch");
            return ERR_USERIDNOTFETCHED;
        }

        contact = make_contact(dir, userid, TRUE);

        if (buddies)
            contact->isbuddy = TRUE;

        for (loop=0; loop < bk->num_fetch_fields; loop++)
        {
		    // Get a single field value
            const char *field = bk->fetch_fields[loop];
            const char *val = resultset_get_value_by_col(data, i, loop);

            struct contact_field_st * def = find_dbfield_def(field);

            if (def != NULL)
            {
                switch (def->type)
                {
                    case 1: //string
                        *(char **) (((unsigned char *)contact)+def->offset) = apr_pstrdup(dir->pool, val);
                        break;

                    case 2: //int
                        *(int *) (((unsigned char *)contact)+def->offset) = atoi(val);
                        break;
                }
            }
        }
        set_displayname(contact);

        // If there is one more column, it is the last modified value
        if (resultset_get_row_size(data, i) > loop)
        {
            const char *lastmod = resultset_get_value_by_col(data, i, loop);
            int mod = atoi(lastmod);
            if (mod > dir->contact_last_mod)
                dir->contact_last_mod = mod;
        }
	}

    dir->rows_fetched += resultset_get_chunk_size(data);
    dir->rows_available = resultset_get_total_rows(data);

    debugf("Contact last mod %d", dir->contact_last_mod);

	return 0;
}

int parse_deleted_contact_result(addressbk_t bk, resultset_t data, int dir_type, directory_t dir)
{
    int i;

    if (dir == NULL)
        dir = make_directory(bk, dir_type, NULL, TRUE);

    for (i=0; i < resultset_get_chunk_size(data); i++)
	{
        contact_t contact;
        const char *userid = NULL;
        const char *directoryid = NULL;
        const char *lastmodified = NULL;

        if (!resultset_get_values(data, i, "(sss*)", &userid, &directoryid, &lastmodified))
	    {
			debugf("Error parsing deleted contact information: %d", i);
			return -1;
	    }

        contact = make_contact(dir, userid, FALSE);
        if (contact)
            contact->removed = TRUE;
            
        if (lastmodified)
        {
            int mod = atoi(lastmodified);
            if (mod > dir->contact_last_mod)
                dir->contact_last_mod = mod;
        }
	}

    dir->rows_fetched += resultset_get_chunk_size(data);
    dir->rows_available = resultset_get_total_rows(data);

	return 0;
}

// Parse contacts, start fetching deleted contacts
static void on_contacts_fetched(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;
    int type = strstr(rpc_id, "global") != NULL ? dtGlobal : dtPersonal;

    if (parse_contact_result(bk, data, type, NULL, FALSE))
    {
        on_fetch_failed(user, rpc_id, DefaultErrorCode, "Parse contacts data failed");
        return;
    }

	if (resultset_more_rows_available(data) == FALSE)
    {
        if (bk->api_ver < 4)
            start_fetch_groups(bk, opid, type, 0);
        else
            start_fetch_deleted_contacts(bk, opid, type, 0);
    }
    else
    {
        if (start_fetch_contacts(user, opid, type, resultset_get_first_index(data) + bk->chunk_size))
        {
            on_fetch_failed(user, rpc_id, DefaultErrorCode, "Fetch contacts failed");
        }

        // report partial progress
        bk->on_progress( bk->on_progress_data, opid,
                         resultset_get_first_index(data) + bk->chunk_size,
                         resultset_get_total_rows(data) );
    }
}

// Parse deleted contacts, start fetching groups
static void on_deleted_contacts_fetched(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;
    int type = strstr(rpc_id, "global") != NULL ? dtGlobal : dtPersonal;

    if (parse_deleted_contact_result(bk, data, type, NULL))
    {
        on_fetch_failed(user, rpc_id, DefaultErrorCode, "Parse contacts data failed");
        return;
    }

	if (resultset_more_rows_available(data) == FALSE)
    {
        start_fetch_groups(bk, opid, type, 0);
    }
    else
    {
        if (start_fetch_deleted_contacts(user, opid, type, resultset_get_first_index(data) + bk->chunk_size))
        {
            on_fetch_failed(user, rpc_id, DefaultErrorCode, "Fetch contacts failed");
        }

        // report partial progress
        bk->on_progress( bk->on_progress_data, opid,
                         resultset_get_first_index(data) + bk->chunk_size,
                         resultset_get_total_rows(data) );
    }
}

// Parse groups, start fetching group members
static void on_groups_fetched(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;
    int type = strstr(rpc_id, "global") != NULL ? dtGlobal : dtPersonal;
    directory_t dir = make_directory(bk, type, NULL, FALSE);
    int i;

    for (i=0; i < resultset_get_chunk_size(data); i++)
    {
        group_t group;
	    char *name, *groupid, *directoryid, *type, *lastmodified = NULL;
	    BOOL res;
	    
	    if (bk->api_ver < 4)
            res = resultset_get_values(data, i, "(ssss*)", &name, &groupid, &directoryid, &type);
        else
            res = resultset_get_values(data, i, "(sssss*)", &name, &groupid, &directoryid, &type, &lastmodified);

        if (!res)
	    {
			debugf("Error parsing group information: %d", i);
			on_fetch_failed(user, rpc_id, DefaultErrorCode, "Fetch groups failed");
			return;
	    }

	    group = make_group(dir, groupid, TRUE);

        group->name     = apr_pstrdup(dir->pool, name);
        group->type     = atoi(type);
        group->removed  = FALSE;

        // Clear out array
        group->members = apr_array_make(dir->pool, 10, sizeof(char *));

        if (lastmodified)
        {
            int mod = atoi(lastmodified);
            if (mod > dir->group_last_mod)
                dir->group_last_mod = mod;
        }

        if (group->type == dtAll)
            dir->all_group = group;
        else if (group->type == dtBuddies)
            dir->buddies_group = group;
    }

	if (resultset_more_rows_available(data) == FALSE)
    {
        if (bk->api_ver < 4)
            start_fetch_members(bk, opid, type, 0);
        else
            start_fetch_deleted_groups(bk, opid, type, 0);
    }
    else
    {
        if (start_fetch_groups(user, opid, type, resultset_get_first_index(data) + bk->chunk_size))
        {
            on_fetch_failed(user, rpc_id, DefaultErrorCode, "Fetch groups failed");
        }

        // report partial progress
        bk->on_progress( bk->on_progress_data, opid,
                         resultset_get_first_index(data) + bk->chunk_size,
                         resultset_get_total_rows(data) );
    }
}

// Parse deleted groups
static void on_deleted_groups_fetched(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;
    int type = strstr(rpc_id, "global") != NULL ? dtGlobal : dtPersonal;
    directory_t dir = make_directory(bk, type, NULL, FALSE);
    int i;

    for (i=0; i < resultset_get_chunk_size(data); i++)
    {
        group_t group;
	    char *groupid, *directoryid, *lastmodified = NULL;
	    BOOL res;
	    
        res = resultset_get_values(data, i, "(sss*)", &groupid, &directoryid, &lastmodified);

        if (!res)
	    {
			debugf("Error parsing deleted group information: %d", i);
			on_fetch_failed(user, rpc_id, DefaultErrorCode, "Fetch deleted groups failed");
			return;
	    }

	    group = make_group(dir, groupid, FALSE);

        if (group)
        {
            group->removed  = TRUE;

            // Clear out array
            //group->members = apr_array_make(dir->pool, 10, sizeof(char *));
        }

        if (lastmodified)
        {
            int mod = atoi(lastmodified);
            if (mod > dir->group_last_mod)
                dir->group_last_mod = mod;
        }
    }

	if (resultset_more_rows_available(data) == FALSE)
    {
        start_fetch_members(bk, opid, type, 0);
    }
    else
    {
        if (start_fetch_deleted_groups(user, opid, type, resultset_get_first_index(data) + bk->chunk_size))
        {
            on_fetch_failed(user, rpc_id, DefaultErrorCode, "Fetch groups failed");
        }

        // report partial progress
        bk->on_progress( bk->on_progress_data, opid,
                         resultset_get_first_index(data) + bk->chunk_size,
                         resultset_get_total_rows(data) );
    }
}

// Parse members, release to caller
static void on_members_fetched(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;
    int type = strstr(rpc_id, "global") != NULL ? dtGlobal : dtPersonal;
    directory_t dir = make_directory(bk, type, NULL, FALSE);
    int i;

    for (i=0; i < resultset_get_chunk_size(data); i++)
    {
        contact_t contact;
        group_t group;
	    char *groupid, *userid;
	    char **memberid;

        if (!resultset_get_values(data, i, "(ss*)", &groupid, &userid))
	    {
			debugf("Error parsing membership information %d", i);
			on_fetch_failed(user, rpc_id, DefaultErrorCode, "Fetch group members failed");
			return;
	    }

	    group = make_group(dir, groupid, FALSE);
	    if (group == NULL)
	    {
	        debugf("Group %s found in membership result, but does not exist", groupid);
            continue;
	    }

        contact = directory_find_contact(dir, userid);
        if (contact == NULL)
        {
            debugf("User id %s found in group %s, but does not exist", userid, groupid);
            continue;
        }

        if (group->type == dtBuddies)
        {
           contact->isbuddy = TRUE;
        }

	    memberid = apr_array_push(group->members);
	    *memberid = apr_pstrdup(dir->pool, userid);
    }

	if (resultset_more_rows_available(data) == FALSE)
    {
        flush_operation(bk, opid);

        if (bk->cache_dir)
            directory_write(bk, type);
            
        bk->on_directory_fetched(bk->on_directory_fetched_data, opid, dir);
    }
    else
    {
        if (start_fetch_members(user, opid, type, resultset_get_first_index(data) + bk->chunk_size))
        {
            on_fetch_failed(user, rpc_id, DefaultErrorCode, "Fetch contacts failed");
        }

        // report partial progress
        bk->on_progress( bk->on_progress_data, opid,
                         resultset_get_first_index(data) + bk->chunk_size,
                         resultset_get_total_rows(data) );
    }
}

// Fetch directories for this user (usually called internally)
int addressbk_fetch_directories(addressbk_t bk, const char *opid)
{
    const char * session_id = bk->auth_info->session_id;
    const char * community_id = bk->auth_info->community_id;
    const char * user_id = bk->auth_info->user_id;
    char rpc_id[128];

    sprintf(rpc_id, "rpc_dirfetch:%s", opid);

    if (bk->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
    else if (session_make_rpc_call(bk->session, rpc_id,
                                        "addressbk",
                                        "addressbk.fetch_directories",
                                        "(sssii)",
                                        session_id,
                                        user_id,
                                        community_id,
                                        0,
                                        100))
	{
        debugf("Addressbook fetch directories call failed");
        return ERR_RPCFAILED;
	}

	return 0;
}

static void on_directories_fetched(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;
//    const char *opid = strchr(rpc_id, ':') + 1;
    BOOL succeeded = TRUE;
    int i;

	for (i=0; i<resultset_get_chunk_size(data); i++)
	{
		char *name, *type, *subscribed, *id;

		if (!resultset_get_values(data, i, "(ssss*)", &name, &id, &type, &subscribed))
		{
		    debugf("Unable to parse fetch directories result");
            succeeded = FALSE;
			break;
		}
        else
        {
    		int dir_type = atoi(type);
    		if (dir_type == dtGlobal)
    		{
                bk->global_directory_id = apr_pstrdup(bk->pool, id);
                bk->global_subscribed = _stricmp(subscribed, "true") == 0;
                if( bk->global_directory_id  ==  NULL )
                {
//                    debugf("in on_directories_fetched( ) -- after setting bk->global_directory_id  --  it is NULL");
                    succeeded = FALSE;
                }
    		}
            else if (dir_type == dtPersonal)
            {
                bk->personal_directory_id = apr_pstrdup(bk->pool, id);
                bk->personal_subscribed = _stricmp(subscribed, "true") == 0;
                if( bk->personal_directory_id  ==  NULL )
                {
//                    debugf("in on_directories_fetched( ) -- after setting bk->personal_directory_id  --  it is NULL");
                    succeeded = FALSE;
                }
            }
        }
	}

	if (resultset_more_rows_available(data))
	{
		debugf("-- Not all directories fetched");
        succeeded = FALSE;
	}

    if( resultset_get_chunk_size(data)  ==  0 )
    {
        debugf("-- No directories were fetched");
        succeeded = FALSE;
    }

    // We were performing this for internal use; continue contact fetch
    if (bk->fetch_global || bk->fetch_personal)
    {
        if (succeeded)
        {
            if( bk->fetch_global  &&  bk->global_directory_id )
                start_fetch_contacts(bk, bk->fetch_global, dtGlobal, 0);
            if (bk->fetch_personal  &&  bk->personal_directory_id )
                start_fetch_contacts(bk, bk->fetch_personal, dtPersonal, 0);
        }
        else
        {
            // ToDo: report error if really fetching contacts
            on_fetch_failed(user, rpc_id, DefaultErrorCode, "Fetch directory id call failed");
        }
    }
}

static void on_fetch_failed(void *user, const char *id, int code, const char *msg)
{
    debugf("Address book fetched failed: %s", msg);
    // ToDo: notify caller
}


int start_fetch_buddies(addressbk_t bk, const char *opid, int start_row)
{
    const char * session_id = bk->auth_info->session_id;
    const char * username = bk->auth_info->username;
    const char * community_id = bk->auth_info->community_id;
    session_array_t field_array;
    char rpc_id[128];

    sprintf(rpc_id, "rpc_fetchbuddies:%s", opid);

    field_array = session_create_array(bk->num_fetch_fields, bk->fetch_fields);

    // Get contacts call:
    //   SESSIONID, USERNAME, COMMUNITYID, FIELDS, FIRST, CHUNKSIZE
    //
    if (session_make_rpc_call(bk->session,
                         rpc_id,
                         "addressbk",
                         "addressbk.fetch_buddy_contacts",
                         "(sssVii)",
                         session_id,
                         username,
                         community_id,
                         field_array,
                         start_row,
                         bk->chunk_size))
    {
        debugf("Addressbook fetch buddies call failed");
        return ERR_RPCFAILED;
    }

    session_free_array(field_array);

    return 0;
}

int start_fetch_me(addressbk_t bk, const char *opid)
{
    const char * session_id = bk->auth_info->session_id;
    const char * user_id = bk->auth_info->user_id;
    session_array_t field_array;
    char rpc_id[128];

    sprintf(rpc_id, "rpc_fetchme:%s", opid);

    field_array = session_create_array(bk->num_fetch_fields, bk->fetch_fields);

    if (session_make_rpc_call(bk->session,
                         rpc_id,
                         "addressbk",
                         "addressbk.fetch_contact",
                         "(ssV)",
                         session_id,
                         user_id,
                         field_array))
    {
        debugf("Addressbook fetch contact call failed");
        return ERR_RPCFAILED;
    }

    session_free_array(field_array);

    return 0;
}

int addressbk_fetch_buddies(addressbk_t bk, const char *opid)
{
    if (bk->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

    return start_fetch_buddies(bk, opid, 0);
}

void on_buddies_fetched(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;

    // Perhaps should have special directory for buddies??
    if (parse_contact_result(bk, data, dtGlobal, NULL, TRUE))
    {
        on_fetch_failed(user, rpc_id, DefaultErrorCode, "Parse contacts data failed");
        return;
    }

	if (resultset_more_rows_available(data) == FALSE)
    {
        start_fetch_me(user, opid);
    }
    else
    {
        if (start_fetch_buddies(user, opid, resultset_get_first_index(data) + bk->chunk_size))
        {
            on_fetch_failed(user, rpc_id, DefaultErrorCode, "Fetch contacts failed");
        }

        // report partial progress
        bk->on_progress( bk->on_progress_data, opid,
                         resultset_get_first_index(data) + bk->chunk_size,
                         resultset_get_total_rows(data) );
    }
}

void on_contact_fetched(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;

    // Perhaps should have special directory for buddies??
    if (parse_contact_result(bk, data, dtGlobal, NULL, TRUE))
    {
        on_fetch_failed(user, rpc_id, DefaultErrorCode, "Parse contacts data failed");
        return;
    }

    directory_t dir = make_directory(bk, dtGlobal, NULL, FALSE);
    bk->on_directory_fetched(bk->on_directory_fetched_data, opid, dir);
}

// ToDo: modify server API to accept list of fields to be modified; use fetch fields to
// control which fields get updated.
int addressbk_contact_update(addressbk_t bk, const char *opid, int dir_type, contact_t contact, const char *password)
{
    const char * session_id = bk->auth_info->session_id;
    const char * community_id = bk->auth_info->community_id;
    directory_t dir = make_directory(bk, dir_type, NULL, FALSE);
    char rpc_id[256];
    char type_str[64];
    const char *server_name = "";
    operation_t oper;
    BOOL new_op_flag;

    if (bk->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

    if (dir == NULL)
    {
        // Need to fetch the directory before we can add/modify contacts in it
        return ERR_DIRECTORYNOTFETCHED;
    }

    sprintf(type_str, "%d", contact->type);

    // make username if needed
    if (contact->type == UserTypeUser || contact->type == UserTypeAdmin)
    {
        if (!is_admin_session(bk))
        {
            BOOL fExistingRecord = (contact->userid && (*contact->userid) );
            BOOL fUsersOwn = contact->userid && (strcmp( contact->userid, bk->auth_info->user_id ) == 0);

            if( fExistingRecord && !fUsersOwn )
            {
                return ERR_NOTADMIN;
            }
        }

        if (contact->profileid == NULL || *contact->profileid == '\0' ||
            contact->screenname == NULL || *contact->screenname == '\0')
        {
            return ERR_MISSINGFIELD;
        }
        server_name = get_servername(bk);
        if (contact->username == NULL)
            contact->username = apr_psprintf(dir->pool, "%s@%s", contact->screenname, server_name);
    }

    // Keep track of what we need to process server result
    oper = make_operation(bk, opid, TRUE, &new_op_flag);
    if (!new_op_flag)
    {
        return ERR_DUPLICATEOPID;
    }
    oper->contact = contact;
    contact->directory = dir;

    if (contact->userid && *contact->userid)
    {
        sprintf(rpc_id, "rpc_updatecontact:%s", opid);
        if (session_make_rpc_call(bk->session, rpc_id,
                         "addressbk",
                         "addressbk.update_contact",
                         "(sssssssssssssssssssssssssssssssssssssssssssss)",
                         session_id,
                         CHECK_NULL(password),
                         CHECK_NULL(contact->userid),
                         community_id,
                         dir->directory_id,
                         CHECK_NULL(contact->username),
                         CHECK_NULL(server_name),
                         CHECK_NULL(contact->title),
                         CHECK_NULL(contact->first),
                         CHECK_NULL(contact->middle),
                         CHECK_NULL(contact->last),
                         CHECK_NULL(contact->suffix),
                         CHECK_NULL(contact->company),
                         CHECK_NULL(contact->jobtitle),
                         CHECK_NULL(contact->address1),
                         CHECK_NULL(contact->address2),
                         CHECK_NULL(contact->state),
                         CHECK_NULL(contact->country),
                         CHECK_NULL(contact->postalcode),
                         CHECK_NULL(contact->screenname),
                         "",   // old AIM screen name
                         CHECK_NULL(contact->email),
                         CHECK_NULL(contact->email2),
                         CHECK_NULL(contact->email3),
                         CHECK_NULL(contact->busphone),
                         "true",
                         CHECK_NULL(contact->homephone),
                         "true",
                         CHECK_NULL(contact->mobilephone),
                         "true",
                         CHECK_NULL(contact->otherphone),
                         "true",
                         "", // extension
                         CHECK_NULL(contact->msnscreen),
                         CHECK_NULL(contact->yahooscreen),
                         CHECK_NULL(contact->aimscreen),
                         CHECK_NULL(contact->user1),
                         CHECK_NULL(contact->user2),
                         type_str,
                         CHECK_NULL(contact->profileid),
                         "0", // admin max size
                         "0", // admin max priority
                         "0", // admin priority type
                         "0", // enabled features
                         "0"  // disabled features
                         ))
        {
            debugf("Addressbook update contact call failed");
            return ERR_RPCFAILED;
        }

        if (session_make_rpc_call(bk->session, "rpc_setdefphone",
                         "addressbk",
                         "addressbk.set_default_phone",
                         "(ssi)",
                         session_id,
                         CHECK_NULL(contact->userid),
                         contact->defphone
                         ))
         {
             debugf("Addressbook set default phone call failed");
         }
    }
    else
    {
        const char * group_id = dir->all_group->group_id;
        char defphone_str[16];

        sprintf(defphone_str, "%d", contact->defphone);

        sprintf(rpc_id, "rpc_addcontact:%s", opid);
        if (session_make_rpc_call(bk->session, rpc_id,
                         "addressbk",
                         "addressbk.add_contact",
                         "(ssssssssssssssssssssssssssssssssssssssssssssssss)",
                         session_id,
                         CHECK_NULL(password),
                         community_id,
                         group_id,
                         dir->directory_id,
                         CHECK_NULL(contact->username),
                         CHECK_NULL(server_name),
                         CHECK_NULL(contact->title),
                         CHECK_NULL(contact->first),
                         CHECK_NULL(contact->middle),
                         CHECK_NULL(contact->last),
                         CHECK_NULL(contact->suffix),
                         CHECK_NULL(contact->company),
                         CHECK_NULL(contact->jobtitle),
                         CHECK_NULL(contact->address1),
                         CHECK_NULL(contact->address2),
                         CHECK_NULL(contact->state),
                         CHECK_NULL(contact->country),
                         CHECK_NULL(contact->postalcode),
                         CHECK_NULL(contact->screenname),
                         "",   // old AIM screen name
                         CHECK_NULL(contact->email),
                         CHECK_NULL(contact->email2),
                         CHECK_NULL(contact->email3),
                         CHECK_NULL(contact->busphone),
                         "true",
                         CHECK_NULL(contact->homephone),
                         "true",
                         CHECK_NULL(contact->mobilephone),
                         "true",
                         CHECK_NULL(contact->otherphone),
                         "true",
                         "", // extension
                         CHECK_NULL(contact->msnscreen),
                         CHECK_NULL(contact->yahooscreen),
                         CHECK_NULL(contact->aimscreen),
                         CHECK_NULL(contact->user1),
                         CHECK_NULL(contact->user2),
                         defphone_str,
                         "0", // def email
                         type_str,
                         CHECK_NULL(contact->profileid),
                         "0", // admin max size
                         "0", // admin max priority
                         "0", // admin priority type
                         "0", // enabled features
                         "0", // disabled features
                         ""  // user id (generates new one
                         ))
        {
            debugf("Addressbook add contact call failed");
            return ERR_RPCFAILED;
        }
    }

    return 0;
}

static void on_contact_updated(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;
    operation_t oper = make_operation(bk, opid, FALSE, NULL);

    if (oper != NULL)
    {
        bk->on_updated(bk->on_updated_data, opid, oper->contact->userid);

        flush_operation(bk, opid);
    }
    else
    {
        errorf("Unexpected result for operation %s received", opid);
    }
}

static void on_contact_added(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;
    operation_t oper = make_operation(bk, opid, FALSE, NULL);

    if (oper)
    {
        char *userid;
        int status;
        xmlrpc_value *names_array;
        resultset_get_output_values(data, "(isV)", &status, &userid, &names_array);

        if (status == Ok)
        {
            directory_t dir = oper->contact->directory;
            if (dir)
            {
                oper->contact->userid = apr_pstrdup(dir->pool, userid);
                apr_hash_set(dir->contacts, oper->contact->userid, APR_HASH_KEY_STRING, oper->contact);
            }

            // Add contact to correct groups
            if (dir->all_group)
            {
                add_group_member(dir->all_group, userid);
            }

            bk->on_updated(bk->on_updated_data, opid, userid);
        }
        else if (status == DuplicateUsername)
        {
            bk->on_update_failed(bk->on_update_failed_data, opid, status, "Duplicate screen name for new user");
        }

        flush_operation(bk, opid);
    }
    else
    {
        errorf("Unexpected result for operation %s received", opid);
    }
}

static void on_addressbk_update_failed(void *user, const char *rpc_id, int code, const char *msg)
{
    addressbk_t bk = (addressbk_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;

    debugf("Address book update failed: %s", msg);

    bk->on_update_failed(bk->on_update_failed_data, opid, code, msg);

    flush_operation(bk, opid);
}

int addressbk_contact_delete(addressbk_t bk, const char *opid, contact_t contact)
{
    const char * session_id = bk->auth_info->session_id;
    char rpc_id[256];

    sprintf(rpc_id, "rpc_deletecontact:%s", opid);

    if (bk->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

    directory_t dir = contact->directory;
    if (dir != NULL)
    {
        if (dir->type == dtGlobal && !is_admin_session(bk))
            return ERR_NOTADMIN;
    }

    if (contact->userid && *contact->userid)
    {
        if (session_make_rpc_call(bk->session, rpc_id,
                             "addressbk",
                             "addressbk.remove_contact",
                             "(ss)",
                             session_id, contact->userid))
        {
            debugf("Addressbook delete contact call failed");
            return ERR_RPCFAILED;
        }
    }

    // ToDo: wait for server response before removing locally, notify caller on success
    apr_hash_set(dir->contacts, contact->userid, APR_HASH_KEY_STRING, NULL);
    contact->removed = TRUE;
    bk->on_deleted(bk->on_deleted_data, opid, contact->userid);

    return 0;
}

int addressbk_search_contacts(addressbk_t bk, const char *opid, contact_condition_t conditions, const char *sort_field,
                              int starting_row, int chunk_size)
{
    const char * session_id = bk->auth_info->session_id;
    char rpc_id[256];
    const char * directory_ids[2];
    int num_dirs = 0;
    session_array_t fetch_field_array;
    session_array_t directory_array;
    session_array_t field_array;
    session_array_t condition_array;
    session_array_t value_array;
    struct contact_field_st * def;
    int res = 0;
    int i;

    sprintf(rpc_id, "rpc_searchcontacts:%s", opid);

    if (bk->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

    // Figure out field to sort result on
    if (sort_field == NULL || !*sort_field)
    {
        sort_field = "first";
    }
    else
    {
        def = find_field_def(sort_field);
        if (def == NULL)
            return ERR_UNKNOWNFIELD;
        sort_field = def->dbfield;
    }

    // Get list of field values we should be fetching
    fetch_field_array = session_create_array(bk->num_fetch_fields, bk->fetch_fields);

    // Figure out directories to search
    if (conditions->search_global)
        directory_ids[num_dirs++] = bk->global_directory_id;
    if (conditions->search_personal)
        directory_ids[num_dirs++] = bk->personal_directory_id;
    directory_array = session_create_array(num_dirs, directory_ids);

    // Create field, operator, value arrays
    field_array = session_create_array(0, NULL);
    condition_array = session_create_array(0, NULL);
    value_array = session_create_array(0, NULL);

    for (i=0; i<conditions->fields->nelts; i++)
    {
        pstring *field = get_element_at(conditions->fields, i);
        pstring *cond = get_element_at(conditions->conditions, i);
        pstring *value = get_element_at(conditions->values, i);

        struct contact_field_st * def = find_field_def(*field);
        if (def == NULL)
        {
            debugf("Unknown field in search conditions: %s", *field);
            res = ERR_UNKNOWNFIELD;
            goto cleanup;
        }

        if (def->dbfield)
        {
            session_add_array_element(field_array, "s", def->dbfield);
            session_add_array_element(condition_array, "s", *cond);
            session_add_array_element(value_array, "s", *value);
        }
    }

    if (session_make_rpc_call(bk->session, rpc_id,
                         "addressbk",
                         "addressbk.search_contacts",
                         "(sVsVVVVii)",
                         session_id,
                         directory_array,
                         sort_field,
                         fetch_field_array,
                         field_array,
                         condition_array,
                         value_array,
                         starting_row,
                         chunk_size))
    {
        debugf("Contact search rpc call failed");
        res = ERR_RPCFAILED;
    }

cleanup:
    session_free_array(fetch_field_array);
    session_free_array(directory_array);
    session_free_array(field_array);
    session_free_array(condition_array);
    session_free_array(value_array);

    return res;
}

// Parse contacts, start fetching groups
static void on_search_contacts(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;
    directory_t dir = make_directory(bk, dtSearch, opid, TRUE);

    if (parse_contact_result(bk, data, -1, dir, FALSE))
    {
        on_fetch_failed(user, rpc_id, DefaultErrorCode, "Parse contacts data failed");
        return;
    }

    bk->on_directory_fetched(bk->on_directory_fetched_data, opid, dir);
}

directory_t addressbk_find_search_results(addressbk_t bk, const char *opid)
{
    return make_directory(bk, dtSearch, opid, FALSE);
}


int addressbk_fetch_profiles(addressbk_t bk, const char *opid)
{
    const char * session_id = bk->auth_info->session_id;
    const char * community_id = bk->auth_info->community_id;
    char rpc_id[256];

    sprintf(rpc_id, "rpc_fetchprofiles:%s", opid);

    if (bk->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
    else if (session_make_rpc_call(bk->session, rpc_id,
                                        "addressbk",
                                        "addressbk.fetch_profiles",
                                        "(ssii)",
                                        session_id,
                                        community_id,
                                        0,
                                        100))
	{
	    return ERR_RPCFAILED;
	}

    return 0;
}

static BOOL decode_profile(profile_t profile, char *aProfileOptions)
{
    BOOL bResult = FALSE;
    int optioncount = 0;
    int actualcount = 0;
    char dummy[512];

    profile->max_voice_size = PROFILE_MAXVOICE_DEFAULT;
    profile->max_data_size = PROFILE_MAXDATA_DEFAULT;
    profile->max_presence_monitors = PROFILE_PRESENCE_DEFAULT;
    profile->pin_length = PROFILE_PINLENGTH_DEFAULT;
    profile->meeting_id_length = PROFILE_MEETINGIDLENGTH_DEFAULT;
    profile->password_length = PROFILE_PASSWORDLENGTH_DEFAULT;
    profile->max_scheduled_meetings = PROFILE_SCHEDULED_MEETINGS_DEFAULT;

    sscanf(aProfileOptions, "%10u", &optioncount);

    if (optioncount == kProfileOptionCount1)
    {
        // Previous version...parse old blob
        actualcount = sscanf(
                            aProfileOptions,
                            "%10u%10u%10u",
                            &optioncount,
                            &profile->max_voice_size,
                            &profile->max_data_size
                            );

        // Now set defaults for any new flags...
        if (PROFILE_ALLOWTWOPARTY_DEFAULT)
        {
            profile->enabled_features |= (int)ProfileCallAllowTwoParty;
        }

        bResult = actualcount == optioncount + 1;
    }
    else if (optioncount == kProfileOptionCount2)
    {
        // Current version...parse blob
        actualcount = sscanf(
                            aProfileOptions,
                            "%10u%10u%10u%10u%10u%10u%10u%50c%200c",
                            &optioncount,
                            &profile->max_voice_size,
                            &profile->max_data_size,
                            &profile->max_presence_monitors,
                            &profile->pin_length,
                            &profile->meeting_id_length,
                            &profile->password_length,
                            dummy,
                            dummy
                            );

        bResult = actualcount == optioncount + 1;
    }
    else if (optioncount == kProfileOptionCount3)
    {
        // Current version...parse blob
        actualcount = sscanf(
                            aProfileOptions,
                            "%10u%10u%10u%10u%10u%10u%10u%50c%200c%10u",
                            &optioncount,
                            &profile->max_voice_size,
                            &profile->max_data_size,
                            &profile->max_presence_monitors,
                            &profile->pin_length,
                            &profile->meeting_id_length,
                            &profile->password_length,
                            dummy,
                            dummy,
                            &profile->max_scheduled_meetings
                            );

        bResult = actualcount == kProfileOptionCount3 + 1;
    }
    else if (optioncount == kProfileOptionCount4)
    {
        // Current version...parse blob
        actualcount = sscanf(
                            aProfileOptions,
                            "%10u%10u%10u%10u%10u%10u%10u%50c%200c%10u%10u%10u",
                            &optioncount,
                            &profile->max_voice_size,
                            &profile->max_data_size,
                            &profile->max_presence_monitors,
                            &profile->pin_length,
                            &profile->meeting_id_length,
                            &profile->password_length,
                            dummy,
                            dummy,
                            &profile->max_scheduled_meetings,
							&profile->max_idle_minutes,
							&profile->max_connect_minutes
                            );

        bResult = actualcount == kProfileOptionCount4 + 1;
    }

    return bResult;
}

BOOL encode_profile(profile_t profile, char *out, int out_len)
{
    snprintf(out, out_len,
        "%010u%010u%010u%010u%010u%010u%010u%50s%200s%10u%10u%10u",
        kProfileOptionCount4,
        profile->max_voice_size,
        profile->max_data_size,
        profile->max_presence_monitors,
        profile->pin_length,
        profile->meeting_id_length,
        profile->password_length,
        "",     // bridge phone
        "",     // system url
		profile->max_scheduled_meetings,
		profile->max_idle_minutes,
		profile->max_connect_minutes
        );

    return TRUE;
}

void on_profiles_fetched(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;

    for (int i=0; i < resultset_get_chunk_size(data); i++)
    {
        char *profileid;
        char *profilename;
        char *enabledfeatures;
        char *profileoptions;

        if (resultset_get_values(data, i, "(ssss*)",
                                  &profileid,
                                  &profilename,
                                  &enabledfeatures,
                                  &profileoptions
                                  ))
        {
            profile_t profile = make_profile(bk, profileid, TRUE);

            sscanf(enabledfeatures, "%u", &profile->enabled_features);
            profile->name = apr_pstrdup(bk->pool, profilename);
            profile->options = apr_pstrdup(bk->pool, profileoptions);

            decode_profile(profile, profileoptions);
        }
        else
        {
            errorf("Error parsing profile");
        }
    }

    bk->on_profiles_fetched(bk->on_profiles_fetched_data, opid);
}

static int start_profile_update(addressbk_t bk, const char *rpc_id, operation_t oper, profile_t profile)
{
    const char * session_id = bk->auth_info->session_id;

    char sz_enabled_features[32];
    char sz_profile_options[2000];  // current size of db field

    oper->profile_operation.update = TRUE;

    sprintf(sz_enabled_features, "%u", profile->enabled_features);
    encode_profile(profile, sz_profile_options, sizeof(sz_profile_options));

    if (session_make_rpc_call(bk->session, rpc_id,
                                    "addressbk",
                                    "addressbk.update_profile",
                                    "(sssss)",
                                    session_id,
                                    profile->profile_id,
                                    profile->name,
                                    sz_enabled_features,
                                    sz_profile_options
                                    ))
    {
        return ERR_RPCFAILED;
    }

    return 0;
}

int addressbk_profile_update(addressbk_t bk, const char *opid, profile_t profile)
{
    const char * session_id = bk->auth_info->session_id;
    const char * community_id = bk->auth_info->community_id;
    char rpc_id[256];
    operation_t oper;
    BOOL new_op_flag;

    if (bk->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

    if (!is_admin_session(bk))
        return ERR_NOTADMIN;

    // Keep track of what we need to process server result
    oper = make_operation(bk, opid, TRUE, &new_op_flag);
    if (!new_op_flag)
    {
        return ERR_DUPLICATEOPID;
    }
    oper->profile_operation.profile = profile;

    if (profile->profile_id == NULL || *profile->profile_id == '\0')
    {
        sprintf(rpc_id, "rpc_addprofile:%s", opid);

        if (session_make_rpc_call(bk->session, rpc_id,
                                            "addressbk",
                                            "addressbk.add_profile",
                                            "(sss)",
                                            session_id,
                                            community_id,
                                            profile->name
                                            ))
        {
            return ERR_RPCFAILED;
        }
    }
	else
	{
	    sprintf(rpc_id, "rpc_updateprofile:%s", opid);

        if (start_profile_update(bk, rpc_id, oper, profile))
        {
            return ERR_RPCFAILED;
        }
	}

    return 0;
}

static void on_profile_updated(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;
    operation_t oper = make_operation(bk, opid, FALSE, NULL);

    if (oper)
    {
        if (oper->profile_operation.update)
        {
            bk->on_updated(bk->on_updated_data, opid, oper->profile_operation.profile->profile_id);

            flush_operation(bk, opid);
        }
        else
        {
            // Add operation...parse new profile id and issue update to store new data
            profile_t profile = oper->profile_operation.profile;

            char *profileid, *profilename, *enabledfeatures, *profileoptions;

            resultset_get_output_values(data, "(ssss)", &profileid, &profilename, &enabledfeatures, &profileoptions);

            profile->profile_id = apr_pstrdup(bk->pool, profileid);

            apr_hash_set(bk->profiles, profile->profile_id, APR_HASH_KEY_STRING, profile);

            if (start_profile_update(bk, rpc_id, oper, profile))
            {
                errorf("Error updating new profile (%s)", profileid);
            }
        }
    }
    else
    {
        errorf("Unexpected result for operation %s received", opid);
    }
}

int addressbk_profile_delete(addressbk_t bk, const char *opid, profile_t profile)
{
    const char * session_id = bk->auth_info->session_id;
    char rpc_id[256];

    sprintf(rpc_id, "rpc_deleteprofile:%s", opid);

    if (bk->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

    if (!is_admin_session(bk))
        return ERR_NOTADMIN;

    if (session_make_rpc_call(bk->session, rpc_id,
                                        "addressbk",
                                        "addressbk.remove_profile",
                                        "(ss)",
                                        session_id,
                                        profile->profile_id
                                        ))
	{
	    return ERR_RPCFAILED;
	}

    // ToDo: wait for server response before removing locally; notify caller on success
    apr_hash_set(bk->profiles, profile->profile_id, APR_HASH_KEY_STRING, NULL);
    profile->removed = TRUE;
    bk->on_deleted(bk->on_deleted_data, opid, profile->profile_id);

    return 0;
}

int addressbk_group_update(addressbk_t bk, const char *opid, int dir_type, group_t group)
{
    const char * session_id = bk->auth_info->session_id;
    directory_t dir = make_directory(bk, dir_type, NULL, FALSE);
    char rpc_id[256];
    operation_t oper;
    BOOL new_op_flag;

    if (bk->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

    if (dir == NULL)
    {
        // Need to fetch the directory before we can add/modify contacts in it
        return ERR_DIRECTORYNOTFETCHED;
    }

    // check for admin if adding to CAB
    if (dir_type == dtGlobal)
    {
        if (!is_admin_session(bk))
            return ERR_NOTADMIN;
    }

    // Keep track of what we need to process server result
    oper = make_operation(bk, opid, TRUE, &new_op_flag);
    if (!new_op_flag)
    {
        return ERR_DUPLICATEOPID;
    }
    oper->group_operation.group = group;
    group->directory = dir;

    if (group->group_id == NULL)
    {
        sprintf(rpc_id, "rpc_groupadd:%s", opid);
        if (session_make_rpc_call(bk->session, rpc_id,
                             "addressbk",
                             "addressbk.add_group",
                             "(ssss)",
                             session_id,
                             dir->directory_id,
                             group->name,
                             "2"    // group type
                             ))
        {
            return ERR_RPCFAILED;
        }
    }
    else
    {
        sprintf(rpc_id, "rpc_groupupdate:%s", opid);
        if (session_make_rpc_call(bk->session, rpc_id,
                             "addressbk",
                             "addressbk.update_group",
                             "(sssss)",
                             session_id,
                             dir->directory_id,
                             group->group_id,
                             group->name,
                             "2"    // group type
                             ))
        {
            return ERR_RPCFAILED;
        }
    }

    return 0;
}

static void on_group_added(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;
    operation_t oper = make_operation(bk, opid, FALSE, NULL);

    if (oper)
    {
        char *groupid, *dirid, *name;
        int created;
        resultset_get_output_values(data, "(sssi)", &groupid, &dirid, &name, &created);

        if (created)
        {
            group_t group = oper->group_operation.group;
            directory_t dir = group->directory;
            group->group_id = apr_pstrdup(dir->pool, groupid);

            apr_hash_set(dir->groups, group->group_id, APR_HASH_KEY_STRING, group);

            bk->on_updated(bk->on_updated_data, opid, group->group_id);
        }
        else
        {
            debugf("Group %s already exists on server", name);
        }

        flush_operation(bk, opid);
    }
    else
    {
        errorf("Unexpected result for operation %s received", opid);
    }
}

static void on_group_updated(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;
    operation_t oper = make_operation(bk, opid, FALSE, NULL);

    if (oper)
    {
        group_t group = oper->group_operation.group;
        bk->on_updated(bk->on_updated_data, opid, group->group_id);

        flush_operation(bk, opid);
    }
    else
    {
        errorf("Unexpected result for operation %s received", opid);
    }
}

int addressbk_group_add_member(addressbk_t bk, const char *opid, group_t group, const char *userid)
{
    const char * session_id = bk->auth_info->session_id;
    char rpc_id[256];
    operation_t oper;
    BOOL new_op_flag;

    sprintf(rpc_id, "rpc_groupaddmember:%s", opid);

    if (bk->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

    if (group->group_id == NULL || group->directory == NULL)
    {
        debugf("Attempt to modify local group");
        return ERR_MISSINGFIELD;
    }

    // check for admin if removing from CAB
    if (group->directory->type == dtGlobal)
    {
        if (!is_admin_session(bk))
            return ERR_NOTADMIN;
    }

    // Keep track of what we need to process server result
    oper = make_operation(bk, opid, TRUE, &new_op_flag);
    if (!new_op_flag)
    {
        return ERR_DUPLICATEOPID;
    }
    oper->group_operation.group = group;
    oper->group_operation.userid = userid;

    if (session_make_rpc_call(bk->session, rpc_id,
                         "addressbk",
                         "addressbk.add_to_group",
                         "(sss)",
                         session_id,
                         group->group_id,
                         userid
                         ))
	{
		return ERR_RPCFAILED;
	}

    return 0;
}

static void on_member_added(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;
    operation_t oper = make_operation(bk, opid, FALSE, NULL);

    if (oper)
    {
        group_t group = oper->group_operation.group;
        directory_t dir = oper->group_operation.group->directory;
        const char *userid = oper->group_operation.userid;
        char **memberid;

        if (group->members == NULL)
            group->members = apr_array_make(dir->pool, 10, sizeof(char *));

	    memberid = apr_array_push(group->members);
	    *memberid = apr_pstrdup(dir->pool, userid);

        bk->on_updated(bk->on_updated_data, opid, *memberid);

        flush_operation(bk, opid);
    }
    else
    {
        errorf("Unexpected result for operation %s received", opid);
    }
}

int addressbk_group_remove_member(addressbk_t bk, const char *opid, group_t group, const char *userid)
{
    const char * session_id = bk->auth_info->session_id;
    char rpc_id[256];
    operation_t oper;
    BOOL new_op_flag;

    sprintf(rpc_id, "rpc_groupremovemember:%s", opid);

    if (bk->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

    if (group->group_id == NULL || group->directory == NULL)
    {
        debugf("Attempt to modify local group");
        return ERR_MISSINGFIELD;
    }

    // check for admin if removing from CAB
    if (group->directory->type == dtGlobal)
    {
        if (!is_admin_session(bk))
            return ERR_NOTADMIN;
    }

    if (group->directory->type == dtAll)
    {
        return ERR_NOTUSERGROUP;
    }

    // Keep track of what we need to process server result
    oper = make_operation(bk, opid, TRUE, &new_op_flag);
    if (!new_op_flag)
    {
        return ERR_DUPLICATEOPID;
    }
    oper->group_operation.group = group;
    oper->group_operation.userid = userid;

    if (session_make_rpc_call(bk->session, rpc_id,
                         "addressbk",
                         "addressbk.remove_from_group",
                         "(sss)",
                         session_id,
                         group->group_id,
                         userid
                         ))
	{
		return ERR_RPCFAILED;
	}

    // Find and remove this group member locally.
    int i;
    for (i=0; i<group->members->nelts; i++)
    {
        const char *check_userid = directory_group_get_member(group, i);
        if (strcmp(userid, check_userid) == 0)
        {
            remove_element_at(group->members, i);
            break;
        }
    }
    bk->on_deleted(bk->on_deleted_data, opid, userid);

    return 0;
}

int addressbk_group_delete(addressbk_t bk, const char *opid, group_t group)
{
    const char * session_id = bk->auth_info->session_id;
    char rpc_id[256];
    operation_t oper;
    BOOL new_op_flag;

    sprintf(rpc_id, "rpc_groupdelete:%s", opid);

    if (bk->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

    if (group->group_id == NULL || group->directory == NULL)
    {
        debugf("Attempt to delete local group");
        return ERR_MISSINGFIELD;
    }

    // check for admin if removing from CAB
    if (group->directory->type == dtGlobal)
    {
        if (!is_admin_session(bk))
            return ERR_NOTADMIN;
    }

    // Keep track of what we need to process server result
    oper = make_operation(bk, opid, TRUE, &new_op_flag);
    if (!new_op_flag)
    {
        return ERR_DUPLICATEOPID;
    }
    oper->group_operation.group = group;

    if (session_make_rpc_call(bk->session, rpc_id,
                         "addressbk",
                         "addressbk.remove_group",
                         "(ss)",
                         session_id,
                         group->group_id
                         ))
	{
		debugf("Delete group failed: unable to make rpc call");
        return ERR_RPCFAILED;
	}

    // ToDo:  wait for server response before deleting locally.
    apr_hash_set(group->directory->groups, group->group_id, APR_HASH_KEY_STRING, NULL);
    group->removed = TRUE;
    bk->on_deleted(bk->on_deleted_data, opid, group->group_id);

	return 0;
}

int addressbk_buddy_add(addressbk_t bk, const char *opid, contact_t contact, BOOL search_all_communities)
{
    const char * session_id = bk->auth_info->session_id;
    directory_t dir = make_directory(bk, dtPersonal, NULL, FALSE);
    char rpc_id[256];
    operation_t oper;
    BOOL new_op_flag;
    const char *server_name = "";
    const char *restrict_directory = search_all_communities ? "t" : "f";

    sprintf(rpc_id, "rpc_buddyadd:%s", opid);

    if (bk->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

    // Keep track of what we need to process server result
    oper = make_operation(bk, opid, TRUE, &new_op_flag);
    if (!new_op_flag)
    {
        return ERR_DUPLICATEOPID;
    }
    oper->contact = contact;

    server_name = get_servername(bk);
    if (contact->username == NULL || *contact->username == '\0')
        contact->username = apr_psprintf(dir->pool, "%s@%s", contact->screenname, server_name);

    if (session_make_rpc_call(bk->session, rpc_id,
                         "addressbk",
                         "addressbk.add_buddy",
                         "(ssssssssss)",
                         session_id,
                         bk->auth_info->user_id,
                         bk->auth_info->username,
                         CHECK_NULL(contact->userid),
                         CHECK_NULL(contact->username),
                         CHECK_NULL(contact->screenname),
                         CHECK_NULL(contact->first),
                         CHECK_NULL(contact->last),
                         restrict_directory,
                         "S"
                         ))
	{
		debugf("Add buddy failed: unable to make rpc call");
        return ERR_RPCFAILED;
	}

    return 0;
}

static void on_buddy_added(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;
    operation_t oper = make_operation(bk, opid, FALSE, NULL);

    if (oper)
    {
        char *userid, *directoryid, *username, *server,
             *title, *firstname, *middlename, *lastname, *suffix,
             *company, *jobtitle, *address1, *address2, *state, *country, *postalcode,
             *screenname, *aimname, *email, *email2, *email3, *busphone, *busdirect,
             *homephone, *homedirect, *mobilephone, *mobiledirect, *otherphone, *otherdirect,
             *extension, *user1, *user2, *user3, *user4, *user5, *defphone, *defemail,
             *type, *profileid, *maxsize, *maxpriority, *prioritytype,
             *enabledfeatures, *disabledfeatures, *lastmodified;
        char *interbuddyuserid;

        // First row is data for new buddy contact
        resultset_get_values(data, 0, "(sssssssssssssssssssssssssssssssssssssssssssss*)",
             &userid, &directoryid, &username, &server,
             &title, &firstname, &middlename, &lastname, &suffix,
             &company, &jobtitle, &address1, &address2, &state, &country, &postalcode,
             &screenname, &aimname, &email, &email2, &email3, &busphone, &busdirect,
             &homephone, &homedirect, &mobilephone, &mobiledirect, &otherphone, &otherdirect,
             &extension, &user1, &user2, &user3, &user4, &user5, &defphone, &defemail,
             &type, &profileid, &maxsize, &maxpriority, &prioritytype,
             &enabledfeatures, &disabledfeatures, &lastmodified);

        directory_t dir = oper->contact->directory;
        if (dir)
        {
            oper->contact->userid = apr_pstrdup(dir->pool, userid);
            apr_hash_set(dir->contacts, oper->contact->userid, APR_HASH_KEY_STRING, oper->contact);
        }

        // Second row is userid for intercommunity buddy
        if (resultset_get_row_size(data, 1) == 1)
        {
            resultset_get_values(data, 1, "(s*)", &interbuddyuserid);
            // ToDo: handle this buddy info?
        }

        oper->contact->isbuddy = TRUE;

        // Add contact to correct groups
        if (dir->all_group)
        {
            add_group_member(dir->all_group, userid);
        }
        if (dir->buddies_group)
        {
            add_group_member(dir->buddies_group, userid);
        }

        bk->on_updated(bk->on_updated_data, opid, userid);

        flush_operation(bk, opid);
    }
    else
    {
        errorf("Unexpected result for operation %s received", opid);
    }
}

int addressbk_buddy_remove(addressbk_t bk, const char *opid, contact_t contact)
{
    const char * session_id = bk->auth_info->session_id;
    directory_t dir = make_directory(bk, dtPersonal, NULL, FALSE);
    char rpc_id[256];
    BOOL new_op_flag;
    const char *server_name = "";
    operation_t oper;

    sprintf(rpc_id, "rpc_buddyremove:%s", opid);

    if (bk->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }

    // Keep track of what we need to process server result
    oper = make_operation(bk, opid, TRUE, &new_op_flag);
    if (!new_op_flag)
    {
        return ERR_DUPLICATEOPID;
    }
    oper->contact = contact;

    server_name = get_servername(bk);
    if (contact->username == NULL || *contact->username == '\0')
        contact->username = apr_psprintf(dir->pool, "%s@%s", contact->screenname, server_name);

    if (session_make_rpc_call(bk->session, rpc_id,
                         "addressbk",
                         "addressbk.update_presence_subcription",
                         "(ssss)",
                         session_id,
                         bk->auth_info->username,
                         contact->username,
                         "U"
                         ))
	{
		debugf("Remove buddy failed: unable to make rpc call");
        return ERR_RPCFAILED;
	}

    return 0;
}

static void on_buddy_removed(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;
    const char * session_id = bk->auth_info->session_id;
    const char *opid = strchr(rpc_id, ':') + 1;
    operation_t oper = make_operation(bk, opid, FALSE, NULL);

    if (oper)
    {
        contact_t contact = oper->contact;
        group_t group = bk->personal_directory->buddies_group;
        BOOL found = FALSE;

        if (contact && group)
        {
            // Find and remove this group member locally.
            int i;
            for (i=0; i<group->members->nelts; i++)
            {
                const char *check_userid = directory_group_get_member(group, i);
                if (strcmp(contact->userid, check_userid) == 0)
                {
                    remove_element_at(group->members, i);
                    found = TRUE;
                    break;
                }
            }
        }

        if (found)
        {
            // If found locally, then notify server too.
            if (session_make_rpc_call(bk->session, rpc_id,
                                 "addressbk",
                                 "addressbk.remove_from_group",
                                 "(sss)",
                                 session_id,
                                 group->group_id,
                                 contact->userid
                                 ))
            {
                errorf("Remove buddy failed: unable to make rpc call");
            }

            bk->on_deleted(bk->on_deleted_data, opid, contact->userid);
        }

        flush_operation(bk, opid);
    }
    else
    {
        errorf("Unexpected result for operation %s received", opid);
    }
}

int addressbk_get_recording_list(addressbk_t bk, const char *opid, const char *meeting_id)
{
    char rpc_id[256];

    sprintf(rpc_id, "rpc_getrecordinglist:%s", opid);

    if( bk == NULL  ||  bk->session == NULL )
    {
        debugf("Address Book or Session is null");
        return ERR_NOTCONNECTED;
    }

    if( session_make_rpc_call( bk->session, rpc_id,
                               "addressbk",
                               "addressbk.get_recording_list",
                               "(s)",
                               meeting_id ) )
    {
        debugf("get_recording_list failed");
        return ERR_RPCFAILED;
    }

    return 0;
}

static recording_info_t add_recording_info(recording_list_t list, const char *name)
{
    recording_info_t info = apr_array_push(list->items);

    // ToDo: wow, this may not fit, try as string
    int meetingID;
    int year;
    int month;
    int day;
    int hour;
    int minute;
    int second;

    if (sscanf(name, "%d-%4d-%2d-%2d-%2d-%2d-%2d", &meetingID, &year, &month, &day, &hour, &minute, &second)==7)
    {
        const char *pos;
        struct tm time;

        memset(&time, 0, sizeof(struct tm));
        time.tm_sec = second;
        time.tm_min = minute;
        time.tm_hour = hour;
        time.tm_mday = day;
        time.tm_mon = month - 1;
        time.tm_year = year - 1900;

        info->time = mktime(&time);

        pos = strchr(name, '-');
        info->time_string = apr_pstrdup(list->pool, pos + 1);
        info->meeting_id = apr_pstrndup(list->pool, name, pos - name);
    }
    else
    {
        debugf("Error parsing folder name: %s", name);
    }

    return info;
}

static void on_recording_list(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;
    recording_list_t list;
    recording_info_t info;
    char *name, *type, *file;

    list = make_recording_list(bk);

    for (int i=0; i < resultset_get_total_rows(data); i++)
    {
        if (!resultset_get_values(data, i, "(sss)", &name, &type, &file))
        {
            debugf("Error parsing recording list");
            continue;
        }

        if (strcmp(type, "-1") == 0)
        {
            info = add_recording_info(list, name);
            info->folder_file = apr_pstrdup(list->pool, file);
        }
        else if (strcmp(type, "0") == 0)
        {
            info->data_file = apr_pstrdup(list->pool, file);
        }
        else if (strcmp(type, "1") == 0)
        {
            info->voice_file = apr_pstrdup(list->pool, file);
        }
        else if (strcmp(type, "3") == 0)
        {
            info->chat_file = apr_pstrdup(list->pool, file);
        }
        else if (strcmp(type, "4") == 0)
        {
            info->summary_file = apr_pstrdup(list->pool, file);
        }
    }

    bk->on_recording_list(bk->on_recording_list_data, opid, list);
}

int addressbk_recording_remove(addressbk_t bk, const char *opid, const char *meeting_id, const char *folder_file)
{
    char rpc_id[256];

    sprintf(rpc_id, "rpc_getrecordinglist:%s", opid);

    if (bk == NULL  ||  bk->session == NULL)
    {
        debugf("Address Book or Session is null");
        return ERR_NOTCONNECTED;
    }

    if(session_make_rpc_call( bk->session, rpc_id,
                        "addressbk",
                        "addressbk.remove_recordings",
                        "(ss)",
                        meeting_id,
                        folder_file))
    {
        debugf("Remove recording failed");
        return ERR_RPCFAILED;
    }

    return 0;
}

int addressbk_get_document_list(addressbk_t bk, const char *opid, const char *meeting_id)
{
    char rpc_id[256];

    sprintf(rpc_id, "rpc_getdoclist:%s", opid);

    if( bk == NULL  ||  bk->session == NULL )
    {
        debugf("Address Book or Session is null");
        return ERR_NOTCONNECTED;
    }

    if( session_make_rpc_call( bk->session, rpc_id,
                               "addressbk",
                               "addressbk.get_document_list",
                               "(s)",
                               meeting_id ) )
    {
        debugf("get_document_list failed");
        return ERR_RPCFAILED;
    }

    return 0;
}

static void on_document_list(void *user, const char * rpc_id, resultset_t data)
{
    addressbk_t bk = (addressbk_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;
    document_list_t list;
    document_info_t info;
    char *name, *url;

    list = make_document_list(bk);

    for (int i=0; i < resultset_get_total_rows(data); i++)
    {
        if (!resultset_get_values(data, i, "(ss)", &name, &url))
        {
            debugf("Error parsing document list");
            continue;
        }

        info = apr_array_push(list->items);

        info->name = apr_pstrdup(list->pool, name);
        info->url = apr_pstrdup(list->pool, url);
    }

    bk->on_document_list(bk->on_document_list_data, opid, list);
}

int addressbk_email_account_info( addressbk_t bk, contact_t contact, const char *password )
{
    const char      *session_id = bk->auth_info->session_id;
    if( contact->userid && *contact->userid && password && strlen( password ) > 0 )
    {
        if (session_make_rpc_call(bk->session, "send_new_user_confirmation",
                                  "addressbk",
                                  "addressbk.send_new_user_confirmation",   // expecting: "SESSIONID", "USERID", "PASSWORD"
                                  "(sss)",
                                  session_id,
                                  CHECK_NULL(contact->userid),
                                  CHECK_NULL(password)
                                 ))
        {
            debugf("Addressbook email account info call failed");
            return ERR_RPCFAILED;
        }
    }
    return 0;
}

contact_t addressbk_find_contact(addressbk_t bk, const char *userid)
{
    contact_t contact = NULL;
    
    if (bk->global_directory)
    {
        contact = directory_find_contact(bk->global_directory, userid);
    }
    if (!contact && bk->personal_directory)
    {
        contact = directory_find_contact(bk->personal_directory, userid);
    }
    if (!contact)
    {
        directory_iter_t iter = directory_iterate(bk);
        directory_t dir = directory_next(bk, iter);
        while (dir)
        {
            contact = directory_find_contact(dir, userid);
            if (contact)
                break;
            dir = directory_next(bk, iter);
        }
    }
    
    return contact;
}


//
// Condition list accessors
//

contact_condition_t contact_condition_new()
{
    apr_pool_t *newpool;
    apr_pool_create(&newpool, NULL);
    contact_condition_t cond = apr_pcalloc(newpool, sizeof(struct contact_condition_st));
    cond->pool = newpool;
    cond->fields = apr_array_make(newpool, 6, sizeof(pstring));
    cond->conditions = apr_array_make(newpool, 6, sizeof(pstring));
    cond->values = apr_array_make(newpool, 6, sizeof(pstring));
    return cond;
}

void contact_condition_add_directory(contact_condition_t cond, int directory_type)
{
    if (directory_type == dtGlobal)
        cond->search_global = TRUE;
    else if (directory_type == dtPersonal)
        cond->search_personal = TRUE;
}

void contact_condition_add_field(contact_condition_t cond, const char * field, const char * condition, const char * value)
{
    pstring * str = apr_array_push(cond->fields);
    *str = apr_pstrdup(cond->pool, field);

    str = apr_array_push(cond->conditions);
    *str = apr_pstrdup(cond->pool, condition);

    str = apr_array_push(cond->values);
    *str = apr_pstrdup(cond->pool, value);
}

void contact_condition_destroy(contact_condition_t cond)
{
    apr_pool_destroy(cond->pool);
}


//
// Directory accessors
//

struct directory_iter_st
{
    apr_hash_index_t * pos;
};

struct contact_iterator_st
{
    apr_hash_index_t * pos;
};

directory_iter_t directory_iterate(addressbk_t bk)
{
    directory_iter_t iter = apr_pcalloc(bk->pool, sizeof(struct directory_iter_st));
    return iter;
}

directory_t directory_next(addressbk_t bk, directory_iter_t iter)
{
    directory_t dir;
    const char *key;
    int keylen;

    if (iter->pos == NULL)
        iter->pos = apr_hash_first(bk->pool, bk->directories);
    else
        iter->pos = apr_hash_next(iter->pos);

    if (iter->pos != NULL)
    {
        apr_hash_this(iter->pos, (const void**)&key, &keylen, (void**)&dir);
        return dir;
    }

    return NULL;
}

directory_t directory_store(addressbk_t bk, const char *name)
{
    return make_directory(bk, dtUser, name, TRUE);
}

void directory_add_reference(directory_t info)
{
    ++info->references;
}

void directory_remove_reference(directory_t info)
{
    if (--info->references == 0)
    {
        if (info && info->pool)
        {
            apr_pool_destroy(info->pool);
        }
    }
}

pstring directory_string(directory_t dir, const char *str)
{
    return apr_pstrdup(dir->pool, str);
}

contact_iterator_t directory_contact_iterate(directory_t dir)
{
    contact_iterator_t iter = apr_pcalloc(dir->pool, sizeof(struct contact_iterator_st));
    return iter;
}

contact_t directory_contact_next(directory_t dir, contact_iterator_t iter)
{
    contact_t cnt;
    const char *key;
    int keylen;

    if (iter->pos == NULL)
        iter->pos = apr_hash_first(dir->pool, dir->contacts);
    else
        iter->pos = apr_hash_next(iter->pos);

    if (iter->pos != NULL)
    {
        apr_hash_this(iter->pos, (const void**)&key, &keylen, (void**)&cnt);
        return cnt;
    }

    return NULL;
}

contact_t directory_contact_new(directory_t dir)
{
    contact_t contact;

    contact = apr_pcalloc(dir->pool, sizeof(struct contact_st));
    contact->directory = dir;
    contact->profileid = CONTACT_PROFILEID;

    return contact;
}

contact_t directory_contact_copy(addressbk_t bk, directory_t dir, contact_t copy)
{
    contact_t contact;
    int loop;

    contact = apr_pcalloc(dir->pool, sizeof(struct contact_st));
    contact->directory = dir;
    contact->profileid = CONTACT_PROFILEID;

    for (loop=0; loop < bk->num_fetch_fields; loop++)
    {
        // Get a single field value
        const char *field = bk->fetch_fields[loop];
        const char *val_str;
        int val_int;

        struct contact_field_st * def = find_dbfield_def(field);

        if (def != NULL && strcmp(def->field, "userid"))
        {
            switch (def->type)
            {
                case 1: //string
                    val_str = *(char **) (((unsigned char *)copy)+def->offset);
                    *(char **) (((unsigned char *)contact)+def->offset) = apr_pstrdup(dir->pool, val_str);
                    break;

                case 2: //int
                    val_int = *(int *) (((unsigned char *)copy)+def->offset);
                    *(int *) (((unsigned char *)contact)+def->offset) = val_int;
                    break;
            }
        }
    }

    return contact;
}

contact_t directory_contact_store(directory_t dir, const char *userid)
{
    contact_t contact = make_contact(dir, userid, TRUE);
    contact->readonly = TRUE;
    return contact;
}

int directory_contact_set_field(contact_t contact, const char *field, const char *val)
{
    directory_t dir = contact->directory;
    struct contact_field_st *def = find_field_def(field);

    if (def == NULL || def->type > 2)
        return -1;
    
    switch (def->type)
    {
        case 1: //string
            *(char **) (((unsigned char *)contact)+def->offset) = apr_pstrdup(dir->pool, val);
            break;

        case 2: //int
            *(int *) (((unsigned char *)contact)+def->offset) = atoi(val);
            break;
    }

    if (strcmp(field, "first") == 0 || strcmp(field, "last") == 0)
        set_displayname(contact);
    
    return 0;
}

struct group_iterator_st
{
    apr_hash_index_t * pos;
};

group_iterator_t directory_group_iterate(directory_t dir)
{
    group_iterator_t iter = apr_pcalloc(dir->pool, sizeof(struct group_iterator_st));
    return iter;
}

group_t directory_group_next(directory_t dir, group_iterator_t iter)
{
    group_t grp;
    const char *key;
    int keylen;

    if (iter->pos == NULL)
        iter->pos = apr_hash_first(dir->pool, dir->groups);
    else
        iter->pos = apr_hash_next(iter->pos);

    if (iter->pos != NULL)
    {
        apr_hash_this(iter->pos, (const void**)&key, &keylen, (void**)&grp);
        return grp;
    }

    return NULL;
}

group_t directory_group_new(directory_t dir, const char *name)
{
    group_t group;

    group = apr_pcalloc(dir->pool, sizeof(struct contact_st));
    group->directory = dir;
    group->name = apr_pstrdup(dir->pool, name);

    return group;
}

group_t directory_group_store(directory_t dir, const char *groupid, const char *name, int type)
{
    group_t group = make_group(dir, groupid, TRUE);

    group->name     = apr_pstrdup(dir->pool, name);
    group->type     = type;
    group->readonly = TRUE;
    
    return group;
}

int directory_group_add_member(group_t group, const char *userid)
{
    directory_t dir = group->directory;
    char **memberid;

    // Verify userid?

    if (group->members == NULL)
        group->members = apr_array_make(dir->pool, 10, sizeof(char *));
        
    memberid = apr_array_push(group->members);
    *memberid = apr_pstrdup(dir->pool, userid);
    return 0;
}

int directory_group_get_size(group_t group)
{
    if (group == NULL || group->members == NULL)
        return 0;
    return group->members->nelts;
}

const char * directory_group_get_member(group_t group, int member)
{
    if (group == NULL || group->members == NULL)
        return NULL;
    return *(const char**)get_element_at(group->members, member);
}

contact_t directory_find_contact(directory_t dir, const char *userid)
{
    if (strncmp(userid, "user:", 5) == 0)
        userid += 5;

    return make_contact(dir, userid, FALSE);
}

group_t directory_find_group(directory_t dir, const char *groupid)
{
    return make_group(dir, groupid, FALSE);
}

group_t directory_find_group_by_name(directory_t dir, const char *name)
{
    apr_hash_index_t * pos;
    pos = apr_hash_first(dir->pool, dir->groups);

    while (pos)
    {
        group_t g;
        const char *key;
        int keylen;

        apr_hash_this(pos, (const void**)&key, &keylen, (void**)&g);
        if (g->name && strcmp(g->name, name) == 0)
            return g;

        pos = apr_hash_next(pos);
    }

    return NULL;
}


// Look up based on screenname
// May want a second hash index if this is common enough.
// Could use field definitions to lookup based on any field by name.
contact_t directory_find_contact_by_screenname(directory_t dir, const char *screen)
{
    apr_hash_index_t * pos;
    pos = apr_hash_first(dir->pool, dir->contacts);

    while (pos)
    {
        contact_t cnt;
        const char *key;
        int keylen;

        apr_hash_this(pos, (const void**)&key, &keylen, (void**)&cnt);
        if (cnt->screenname && strcmp(cnt->screenname, screen) == 0)
            return cnt;

        pos = apr_hash_next(pos);
    }

    return NULL;
}

contact_t directory_find_contact_by_field(directory_t dir, const char *field, const char *value)
{
    apr_hash_index_t * pos;
    struct contact_field_st *def = find_field_def(field);

    // Only search for strings currently
    if (def == NULL || def->type > 1)
        return NULL;

    pos = apr_hash_first(dir->pool, dir->contacts);

    while (pos)
    {
        contact_t cnt;
        const char *key;
        const char *this_value;
        int keylen;

        apr_hash_this(pos, (const void**)&key, &keylen, (void**)&cnt);
        this_value = *(const char **) (((unsigned char *)cnt)+def->offset);
        if (this_value && strcmp(this_value, value) == 0)
            return cnt;

        pos = apr_hash_next(pos);
    }

    return NULL;
}


struct profile_iterator_st
{
    apr_hash_index_t * pos;
};

profile_iterator_t profiles_iterate(addressbk_t bk)
{
    profile_iterator_t iter = apr_pcalloc(bk->pool, sizeof(struct profile_iterator_st));
    return iter;
}

profile_t profiles_next(addressbk_t bk, profile_iterator_t iter)
{
    profile_t profile;
    const char *key;
    int keylen;

    if (iter->pos == NULL)
        iter->pos = apr_hash_first(bk->pool, bk->profiles);
    else
        iter->pos = apr_hash_next(iter->pos);

    if (iter->pos != NULL)
    {
        apr_hash_this(iter->pos, (const void**)&key, &keylen, (void**)&profile);
        return profile;
    }

    return NULL;
}

profile_t profiles_find_profile(addressbk_t bk, const char *profileid)
{
    return make_profile(bk, profileid, FALSE);
}

profile_t profiles_new(addressbk_t bk)
{
    profile_t profile;

    profile = apr_pcalloc(bk->pool, sizeof(struct profile_st));

    profile->max_voice_size = PROFILE_MAXVOICE_DEFAULT;
    profile->max_data_size = PROFILE_MAXDATA_DEFAULT;
    profile->max_presence_monitors = PROFILE_PRESENCE_DEFAULT;
    profile->pin_length = PROFILE_PINLENGTH_DEFAULT;
    profile->meeting_id_length = PROFILE_MEETINGIDLENGTH_DEFAULT;
    profile->password_length = PROFILE_PASSWORDLENGTH_DEFAULT;
    profile->max_scheduled_meetings = PROFILE_SCHEDULED_MEETINGS_DEFAULT;
    if (PROFILE_ALLOWTWOPARTY_DEFAULT)
    {
        profile->enabled_features |= (int)ProfileCallAllowTwoParty;
    }

    return profile;
}

pstring profiles_string(addressbk_t bk, const char *str)
{
    return apr_pstrdup(bk->pool, str);
}


int recording_list_get_size(recording_list_t list)
{
    if (list == NULL || list->items == NULL)
        return 0;
    return list->items->nelts;
}

recording_info_t recording_list_get_recording(recording_list_t list, int row)
{
    if (list == NULL || list->items == NULL)
        return NULL;
    return (recording_info_t) get_element_at(list->items, row);
}

void recording_list_destroy(recording_list_t list)
{
    apr_pool_destroy(list->pool);
}


int document_list_get_size(document_list_t list)
{
    if (list == NULL || list->items == NULL)
        return 0;
    return list->items->nelts;
}

document_info_t document_list_get_recording(document_list_t list, int row)
{
    if (list == NULL || list->items == NULL)
        return NULL;
    return (document_info_t) get_element_at(list->items, row);
}

void document_list_destroy(document_list_t list)
{
    apr_pool_destroy(list->pool);
}


//
// Utility
//

static BOOL is_admin_session(addressbk_t bk)
{
    if (bk->auth_info->user_type == UserTypeAdmin)
        return TRUE;

    return FALSE;
}

static const char *get_servername(addressbk_t bk)
{
    if (bk->auth_info == NULL || bk->auth_info->username == NULL)
        return NULL;

    char *p = strchr(bk->auth_info->username, '@');
    return p ? p + 1 : NULL;
}

static struct contact_field_st * find_dbfield_def(const char *name)
{
    int i = -1;
    while (field_list[++i].dbfield != NULL)
    {
        if (_stricmp(name, field_list[i].dbfield) == 0)
            return field_list + i;
    }
    return NULL;
}

static struct contact_field_st * find_field_def(const char *name)
{
    int i = -1;
    while (field_list[++i].field != NULL)
    {
        if (_stricmp(name, field_list[i].field) == 0)
            return field_list + i;
    }
    return NULL;
}

static operation_t make_operation(addressbk_t bk, const char *opid, BOOL create, BOOL * created_flag)
{
    operation_t oper;

    oper = (operation_t) apr_hash_get(bk->operations, opid, APR_HASH_KEY_STRING);
    if (oper == NULL && create)
    {
        apr_pool_t *newpool;
        apr_pool_create(&newpool, bk->pool);
        oper = apr_pcalloc(newpool, sizeof(struct operation_st));
        oper->pool = newpool;
        oper->opid = apr_pstrdup(newpool, opid);
        apr_hash_set(bk->operations, oper->opid, APR_HASH_KEY_STRING, oper);
        if (created_flag)
            *created_flag = TRUE;
    }
    else
    {
        if (created_flag)
            *created_flag = FALSE;
    }

    return oper;

}

static void flush_operation(addressbk_t bk, const char *opid)
{
    operation_t oper = (operation_t) apr_hash_get(bk->operations, opid, APR_HASH_KEY_STRING);
    if (oper)
    {
        apr_hash_set(bk->operations, opid, APR_HASH_KEY_STRING, NULL);
        apr_pool_destroy(oper->pool);
    }
}

static directory_t make_directory(addressbk_t bk, int dir_type, const char *id, BOOL create)
{
    apr_pool_t *newpool;
    directory_t newdir;

    if (dir_type != -1 && (dir_type < dtGlobal || dir_type >= dtMaxDirTypes))
    {
        errorf("Illegal directory type %d used", dir_type);
        return NULL;
    }

    switch (dir_type)
    {
        case dtSearch:
            newdir = (directory_t) apr_hash_get(bk->searches, id, APR_HASH_KEY_STRING);
            break;
        case dtUser:
            newdir = (directory_t) apr_hash_get(bk->directories, id, APR_HASH_KEY_STRING);
            break;
        case dtGlobal:
            newdir = bk->global_directory;
            break;
        case dtPersonal:
            newdir = bk->personal_directory;
            break;
    }

    if (newdir == NULL && create)
    {
        apr_pool_create(&newpool, bk->pool);
        newdir = apr_pcalloc(newpool, sizeof(struct directory_st));
        newdir->pool = newpool;
        newdir->type = dir_type;
        newdir->contacts = apr_hash_make(newpool);
        newdir->groups = apr_hash_make(newpool);
        newdir->references = 1;
        if (dir_type == dtSearch)
        {
            newdir->enabled = 1;
            newdir->directory_id = "";
            newdir->name = apr_pstrdup(newpool, "Search Results");
            apr_hash_set(bk->searches, apr_pstrdup(newpool, id), APR_HASH_KEY_STRING, newdir);
        }
        else if (dir_type == dtUser)
        {
            newdir->enabled = 0;
            newdir->directory_id = "";
            newdir->name = apr_pstrdup(newpool, id);
            apr_hash_set(bk->directories, apr_pstrdup(newpool, id), APR_HASH_KEY_STRING, newdir);
        }
        else if (dir_type == dtGlobal)
        {
            newdir->enabled = 1;
            newdir->directory_id = bk->global_directory_id;
            newdir->name = apr_pstrdup(newpool, "Community Address Book");
            bk->global_directory = newdir;
        }
        else if (dir_type == dtPersonal )
        {
            newdir->enabled = 1;
            newdir->directory_id = bk->personal_directory_id;
            newdir->name = apr_pstrdup(newpool, "Personal Address Book");
            bk->personal_directory = newdir;
        }
        else
        {
            errorf("Unkonwn directory type %d", dir_type);
        }
    }

    return newdir;
}

static contact_t make_contact(directory_t dir, const char *userid, BOOL create)
{
    contact_t contact;

    contact = (contact_t) apr_hash_get(dir->contacts, userid, APR_HASH_KEY_STRING);
    if (contact == NULL && create)
    {
        contact = apr_pcalloc(dir->pool, sizeof(struct contact_st));
        contact->directory = dir;
        contact->userid = apr_pstrdup(dir->pool, userid);
        apr_hash_set(dir->contacts, contact->userid, APR_HASH_KEY_STRING, contact);
    }

    return contact;

}

static void set_displayname(contact_t contact)
{
    int len = strlen(CHECK_NULL(contact->first)) + strlen(CHECK_NULL(contact->last)) + 1;
    char *str = apr_pcalloc(contact->directory->pool, len);
    sprintf(str, "%s %s", CHECK_NULL(contact->first), CHECK_NULL(contact->last));
    contact->displayname = str;
}

static group_t make_group(directory_t dir, const char * groupid, BOOL create)
{
    group_t group;

    group = (group_t) apr_hash_get(dir->groups, groupid, APR_HASH_KEY_STRING);
    if (group == NULL && create)
    {
        group = apr_pcalloc(dir->pool, sizeof(struct group_st));
        group->directory = dir;
        group->group_id = apr_pstrdup(dir->pool, groupid);
        apr_hash_set(dir->groups, group->group_id, APR_HASH_KEY_STRING, group);
    }

    return group;
}

static void add_group_member(group_t group, const char *userid)
{
    directory_t dir = group->directory;
    char **memberid;
    int i,size;

    if (group->members == NULL)
        group->members = apr_array_make(dir->pool, 10, sizeof(char *));

    // Make sure user isn't in group already
    size = group->members->nelts;
    for (i=0; i<size; ++i)
    {
        const char *thisid = *(const char**)get_element_at(group->members, i);
        if (strcmp(thisid, userid) == 0)
            return;
    }

    memberid = apr_array_push(group->members);
    *memberid = apr_pstrdup(dir->pool, userid);
}

static profile_t make_profile(addressbk_t bk, const char *id, BOOL create)
{
    profile_t profile;

    profile = (profile_t) apr_hash_get(bk->profiles, id, APR_HASH_KEY_STRING);
    if (profile == NULL && create)
    {
        profile = apr_pcalloc(bk->pool, sizeof(struct profile_st));
        profile->profile_id = apr_pstrdup(bk->pool, id);
        apr_hash_set(bk->profiles, profile->profile_id, APR_HASH_KEY_STRING, profile);
    }

    return profile;
}

static recording_list_t make_recording_list(addressbk_t bk)
{
    recording_list_t list;
    apr_pool_t * newpool;

    apr_pool_create(&newpool, bk->pool);

    list = apr_pcalloc(newpool, sizeof(struct recording_list_st));
    list->pool = newpool;
    list->items = apr_array_make(newpool, 10, sizeof(struct recording_info_st));

    return list;
}

static document_list_t make_document_list(addressbk_t bk)
{
    document_list_t list;
    apr_pool_t * newpool;

    apr_pool_create(&newpool, bk->pool);

    list = apr_pcalloc(newpool, sizeof(struct document_list_st));
    list->pool = newpool;
    list->items = apr_array_make(newpool, 10, sizeof(struct document_info_st));

    return list;
}


//
// Addressbook caching
//

int addressbk_enable_cache(addressbk_t bk, const char *dir)
{
    bk->cache_dir = _strdup(dir);
    return 0;
}

static void contact_write(contact_t contact, struct json_object *row)
{
    struct contact_field_st *def;
    int i;

    for (i = 0; field_list[i].dbfield; i++)
    {
        def = field_list + i;

        if (i == 0)
        {
            char *valstr = *(char **) (((unsigned char *)contact)+def->offset);
            json_object_array_add(row, json_object_new_string((char*)valstr));
        }
        else
        {
            switch (def->type)
            {
                case 1: //string
                {
                    char *valstr = *(char **) (((unsigned char *)contact)+def->offset);
                    if (valstr)
                        json_object_array_add(row, json_object_new_string((char*)valstr));
                    else
                        json_object_array_add(row, NULL);
                    break;
                }

                case 2: //int
                {
                    int valint = *(int *) (((unsigned char *)contact)+def->offset);
                    json_object_array_add(row, json_object_new_int(valint));
                    break;
                }
            }
        }
    }
}

static void group_write(group_t group, struct json_object *row)
{
    struct json_object *members;
    int i, size;
    
    json_object_object_add(row, "id", json_object_new_string((char*)group->group_id));
    json_object_object_add(row, "name", json_object_new_string((char*)group->name));
    json_object_object_add(row, "type", json_object_new_int(group->type));

    members = json_object_new_array();
    
    size = directory_group_get_size(group);
    for (i=0; i<size; i++)
    {
        const char *userid = directory_group_get_member(group, i);
        json_object_array_add(members, json_object_new_string((char*)userid));
    }
    
    json_object_object_add(row, "members", members);
}

static int directory_write(addressbk_t bk, int dir_type)
{
    struct json_object *cache = NULL;
    struct json_object *rows = NULL;
    struct json_object *row = NULL;
	contact_iterator_t iter;
	group_iterator_t group_iter;
	contact_t contact;
	group_t group;
    char filename[FILENAME_MAX];
    directory_t dir;
	int stat;

    dir = make_directory(bk, dir_type, NULL, FALSE);
    if (dir == NULL)
        return 0;

    switch (dir_type)
    {
        case dtGlobal:
            sprintf(filename, "%s/%s.%s", bk->cache_dir, bk->auth_info->user_id, "global.cache");
            break;
        case dtPersonal:
            sprintf(filename, "%s/%s.%s", bk->cache_dir, bk->auth_info->user_id, "personal.cache");
            break;
        default:
            return 0;
    }

    cache = json_object_new_object();

    json_object_object_add(cache, "version", json_object_new_int(CACHE_VERSION));
    json_object_object_add(cache, "lastmod", json_object_new_int(dir->contact_last_mod));
    json_object_object_add(cache, "groupmod", json_object_new_int(dir->group_last_mod));

    rows = json_object_new_array();
    json_object_object_add(cache, "contacts", rows);

    // Copy from result set into json object.
    iter = directory_contact_iterate(dir);
    while ((contact = directory_contact_next(dir, iter)) != NULL)
    {
        if (!contact->removed)
        {
            row = json_object_new_array();

            contact_write(contact, row);

            json_object_array_add(rows, row);
        }
    }

    rows = json_object_new_array();
    json_object_object_add(cache, "groups", rows);
    
    group_iter = directory_group_iterate(dir);
    while ((group = directory_group_next(dir, group_iter)) != NULL)
    {
        if (!group->removed)
        {
            row = json_object_new_object();
            
            group_write(group, row);
            
            json_object_array_add(rows, row);
        }
    }

    stat = json_object_to_file((char*)filename, cache);
    if (stat)
    {
        errorf("Error caching contacts to %s: %d", filename, stat);
        stat = ERR_CACHEWRITE;
    }

    if (cache)
        json_object_put(cache);

    return stat;
}

static void contact_read(directory_t dir, struct json_object *row)
{
    contact_t contact;
    const char *userid;
    struct json_object *field;
    int field_idx = 0;
    struct contact_field_st *def;
    int i;

    for (i = 0; field_list[i].dbfield; i++)
    {
        def = field_list + i;

        if (i == 0)
        {
            field = json_object_array_get_idx(row, field_idx++);
            userid = json_object_get_string(field);
            contact = make_contact(dir, userid, TRUE);
        }
        else
        {
            switch (def->type)
            {
                case 1: //string
                {
                    field = json_object_array_get_idx(row, field_idx++);
                    char *val = json_object_get_string(field);
                    if (val != NULL)
                        *(char **) (((unsigned char *)contact)+def->offset) = apr_pstrdup(dir->pool, val);
                    break;
                }

                case 2: //int
                {
                    field = json_object_array_get_idx(row, field_idx++);
                    int val = json_object_get_int(field);
                    *(int *) (((unsigned char *)contact)+def->offset) = val;
                    break;
                }
            }
        }
    }
    
    set_displayname(contact);
}

static void group_read(directory_t dir, struct json_object *row)
{
    struct json_object *val;
    struct json_object *members;
    const char *groupid, *name;
    group_t group;
    int type;
    int i;
    
    val = json_object_object_get(row, "id");
    groupid = json_object_get_string(val);
    val = json_object_object_get(row, "name");
    name = json_object_get_string(val);
    val = json_object_object_get(row, "type");
    type = json_object_get_int(val);

    group = make_group(dir, groupid, TRUE);

    group->name     = apr_pstrdup(dir->pool, name);
    group->type     = type;
    group->removed  = FALSE;
    
    members = json_object_object_get(row, "members");
    for (i = 0; i < json_object_array_length(members); i++)
    {
        const char *userid = json_object_get_string(json_object_array_get_idx(members, i));
        add_group_member(group, userid);
    }

    if (group->type == dtAll)
        dir->all_group = group;
    else if (group->type == dtBuddies)
        dir->buddies_group = group;
}

static int directory_read(addressbk_t bk, int dir_type)
{
    struct json_object *cache = NULL;
    struct json_object *rows = NULL;
    struct json_object *row = NULL;
    struct json_object *version = NULL;
    struct json_object *mod;
    char filename[FILENAME_MAX];
    directory_t dir;
    int i;

    switch (dir_type)
    {
        case dtGlobal:
            sprintf(filename, "%s/%s.%s", bk->cache_dir, bk->auth_info->user_id, "global.cache");
            break;
        case dtPersonal:
            sprintf(filename, "%s/%s.%s", bk->cache_dir, bk->auth_info->user_id, "personal.cache");
            break;
        default:
            return 0;
    }

    dir = make_directory(bk, dir_type, NULL, TRUE);

    cache = json_object_from_file((char *)filename);
    if (json_is_error(cache))
    {
        errorf("Error reading contacts from cache %s", filename);
        return ERR_CACHEREAD;
    }

    version = json_object_object_get(cache, "version");
    if (!version || json_object_get_int(version) != CACHE_VERSION)
    {
        errorf("Cached data not in current format, ignoring");
        json_object_put(cache);
        return ERR_CACHEREAD;
    }

    mod = json_object_object_get(cache, "lastmod");
    dir->contact_last_mod = json_object_get_int(mod);
    mod = json_object_object_get(cache, "groupmod");
    dir->group_last_mod = json_object_get_int(mod);

    rows = json_object_object_get(cache, "contacts");
    if (rows)
    {
        for (i = 0; i < json_object_array_length(rows); i++)
        {
            row = json_object_array_get_idx(rows, i);

            contact_read(dir, row);
        }
    }

    rows = json_object_object_get(cache, "groups");
    if (rows)
    {
        for (i = 0; i < json_object_array_length(rows); i++)
        {
            row = json_object_array_get_idx(rows, i);

            group_read(dir, row);
        }
    }

    if (cache)
        json_object_put(cache);

    return 0;
}

//
// Static local data
//

struct rpc_handler_st a_handler_list[] =
{
    { "rpc_auth", on_addressbk_auth, on_addressbk_auth_failed },
    { "rpc_versionck", on_version_check, on_version_check_failed },
    { "rpc_fetchglobal", on_contacts_fetched, on_fetch_failed },
    { "rpc_fetchpersonal", on_contacts_fetched, on_fetch_failed },
    { "rpc_deletedglobal", on_deleted_contacts_fetched, on_fetch_failed },
    { "rpc_deletedpersonal", on_deleted_contacts_fetched, on_fetch_failed },
    { "rpc_dirfetch", on_directories_fetched, on_fetch_failed },
    { "rpc_groupsglobal", on_groups_fetched, on_fetch_failed },
    { "rpc_groupspersonal", on_groups_fetched, on_fetch_failed },
    { "rpc_delgroupsglobal", on_deleted_groups_fetched, on_fetch_failed },
    { "rpc_delgroupspersonal", on_deleted_groups_fetched, on_fetch_failed },
    { "rpc_membersglobal", on_members_fetched, on_fetch_failed },
    { "rpc_memberspersonal", on_members_fetched, on_fetch_failed },
    { "rpc_fetchbuddies", on_buddies_fetched, on_fetch_failed },
    { "rpc_fetchme", on_contact_fetched, on_fetch_failed },
    { "rpc_updatecontact", on_contact_updated, on_addressbk_update_failed },
    { "rpc_addcontact", on_contact_added, on_addressbk_update_failed },
    { "rpc_searchcontacts", on_search_contacts, on_fetch_failed },
    { "rpc_fetchprofiles", on_profiles_fetched, on_fetch_failed },
    { "rpc_updateprofile", on_profile_updated, on_addressbk_update_failed },
    { "rpc_addprofile", on_profile_updated, on_addressbk_update_failed },
    { "rpc_deleteprofile", NULL, on_addressbk_update_failed },
    { "rpc_deletecontact", NULL, on_addressbk_update_failed },
    { "rpc_groupadd", on_group_added, on_addressbk_update_failed },
    { "rpc_groupupdate", on_group_updated, on_addressbk_update_failed },
    { "rpc_groupdelete", NULL, on_addressbk_update_failed },
    { "rpc_groupaddmember", on_member_added, on_addressbk_update_failed },
    { "rpc_groupremovemember", NULL, on_addressbk_update_failed },
    { "rpc_buddyadd", on_buddy_added, on_addressbk_update_failed },
    { "rpc_buddyremove", on_buddy_removed, on_addressbk_update_failed },
    { "rpc_getrecordinglist", on_recording_list, on_fetch_failed },
    { "rpc_getdoclist", on_document_list, on_fetch_failed },
    { NULL, NULL, NULL }
};

struct rpc_method_st a_method_list[] =
{
	{ NULL, NULL }
};

struct contact_field_st field_list[] =
{
	{ "userid", "userid", APR_OFFSET(contact_t, userid), 0 },  // set explicity so type is 0
	{ "directoryid", "", 0, 0 },
	{ "group", "", 0, 0 },
	{ "username", "username", APR_OFFSET(contact_t, username), 1 },
	{ "title", "title", APR_OFFSET(contact_t, title), 1 },
	{ "firstname", "first", APR_OFFSET(contact_t, first), 1 },
	{ "middlename", "middle", APR_OFFSET(contact_t, middle), 1 },
	{ "lastname", "last", APR_OFFSET(contact_t, last), 1 },
	{ "suffix", "suffix", APR_OFFSET(contact_t, suffix), 1 },
	{ "company", "company", APR_OFFSET(contact_t, company), 1 },
	{ "jobtitle", "jobtitle", APR_OFFSET(contact_t, jobtitle), 1 },
	{ "address1", "address1", APR_OFFSET(contact_t, address1), 1 },
	{ "address2", "address2", APR_OFFSET(contact_t, address2), 1 },
	{ "", "city", 0, 0 },
	{ "state", "state", APR_OFFSET(contact_t, state), 1 },
	{ "country", "country", APR_OFFSET(contact_t, country), 1 },
	{ "postalcode", "postalcode", APR_OFFSET(contact_t, postalcode), 1 },
	{ "screenname", "screenname", APR_OFFSET(contact_t, screenname), 1 },
	{ "aimname", "", 0 , 0 },
	{ "email", "email", APR_OFFSET(contact_t, email), 1 },
	{ "email2", "email2", APR_OFFSET(contact_t, email2), 1 },
	{ "email3", "email3", APR_OFFSET(contact_t, email3), 1 },
	{ "busphone", "busphone", APR_OFFSET(contact_t, busphone), 1 },
	{ "homephone", "homephone", APR_OFFSET(contact_t, homephone), 1 },
	{ "mobilephone", "mobilephone", APR_OFFSET(contact_t, mobilephone), 1 },
	{ "otherphone", "otherphone", APR_OFFSET(contact_t, otherphone), 1 },
	{ "extension", "", 0, 0 },
	{ "user1", "msnscreen", APR_OFFSET(contact_t, msnscreen), 1 },
	{ "user2", "yahooscreen", APR_OFFSET(contact_t, yahooscreen), 1 },
	{ "user3", "aimscreen", APR_OFFSET(contact_t, aimscreen), 1 },
	{ "user4", "user1", APR_OFFSET(contact_t, user1), 1 },
	{ "user5", "user2", APR_OFFSET(contact_t, user2), 1 },
	{ "defphone", "defphone", APR_OFFSET(contact_t, defphone), 2 },
	{ "defemail", "", 0, 0 },
	{ "displayname", "displayname", APR_OFFSET(contact_t, displayname), 0 },

    // Only set by admins
	{ "type", "type", APR_OFFSET(contact_t, type), 2 },
	{ "password", "", 0, 0 },
	{ "profileid", "profileid", APR_OFFSET(contact_t, profileid), 1 },
	{ "enabledfeatures", "enabledfeatures", APR_OFFSET(contact_t, enabledfeatures), 2 },
	{ "disabledfeatures", "disabledfeatures", APR_OFFSET(contact_t, disabledfeatures), 2 },

	// Virtual search fields
	{ "emails", "emails", 0, 0 },
	{ "phones", "phones", 0, 0 },
	{ NULL, NULL, 0 , 0 }
};

