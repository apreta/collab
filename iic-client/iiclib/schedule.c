/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [web/docroot/images/pics/powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef _WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>

#include <apr.h>
#include <apr_general.h>
#include <apr_pools.h>
#include <apr_strings.h>
#include <apr_hash.h>
#include <apr_tables.h>

#define NO_DEFINE_APR
#include "schedule.h"
#include "handlerdefs.h"
#include "utils.h"

#include "services/common/common.h"

#include <netlib/log.h>


// Private schedule session info.
struct schedule_st
{
    session_t session;
    auth_info_t auth;
    int chunk_size;

    DECLARE_EVENT_HANDLER(schedule,meeting)
    DECLARE_EVENT_HANDLER(schedule,list)
    DECLARE_EVENT_HANDLER(schedule,progress)
    DECLARE_EVENT_HANDLER(schedule,updated)
    DECLARE_EVENT_HANDLER(schedule,error)

    apr_pool_t *pool;

    meeting_list_t my_meetings;
    meeting_list_t public_meetings;
    meeting_list_t invited_meetings;
    apr_hash_t *searches;

    pstring     instant_meeting;
};

//
// Set up mapping from RPC IDs and RPC method names to handler functions.
// Tables are defined at end of file.
//

// RPC result and error messages
struct rpc_handler_st
{
    const char * rpc_id;
    session_rpc_result_handler result_handler;
    session_rpc_error_handler error_handler;
};
extern struct rpc_handler_st s_handler_list[];    // Wanted this to be static but VC++ won't allow it

// RPC request messages
struct rpc_method_st
{
    const char * method;
    session_rpc_request_handler request_handler;
};
extern struct rpc_method_st s_method_list[];


//
// Local function defs.
//
static void on_meeting_fetch_failed(void *user, const char *rpc_id, int code, const char *msg);
static meeting_list_t make_list(apr_pool_t *pool);
meeting_details_t make_meeting_details(apr_pool_t *pool);
meeting_list_t make_list_for_search(schedule_t sched, const char *opid, BOOL create);


//
// Declare APIs to set event callbacks
//

static void default_on_meeting(void *u, const char * opid, meeting_details_t mtg) { }
static void default_on_list(void *u, const char * opid, meeting_list_t list) { }
static void default_on_progress(void *u, const char *opid, int num, int total) { }
static void default_on_updated(void *u, const char *opid, int status, const char *meetingid) { }
static void default_on_error(void *u, const char *opid, int code, const char *msg) { }

IMPL_SET_HANDLER(schedule,meeting)
IMPL_SET_HANDLER(schedule,list)
IMPL_SET_HANDLER(schedule,progress)
IMPL_SET_HANDLER(schedule,updated)
IMPL_SET_HANDLER(schedule,error)


int schedule_initialize()
{
    return 0;
}

static void on_session_message(void *user, session_message_t msg)
{
    schedule_t schedule = (schedule_t)user;

    if (strcmp(msg->id, "session-reset") == 0)
    {
        apr_pool_clear(schedule->pool);
        schedule->my_meetings = NULL;
        schedule->public_meetings = NULL;
        schedule->invited_meetings = NULL;
        schedule->instant_meeting = NULL;
        schedule->searches = apr_hash_make(schedule->pool);
    }
}

// Create schedule object.
schedule_t schedule_create(session_t sess)
{
    schedule_t schedule = (schedule_t)calloc(1,sizeof(struct schedule_st));
    int i;

    schedule->session = sess;
    schedule->chunk_size = 50;

    schedule->on_meeting = default_on_meeting;
    schedule->on_list = default_on_list;
    schedule->on_progress = default_on_progress;
    schedule->on_updated = default_on_updated;

    apr_pool_create(&schedule->pool, NULL);

    schedule->searches = apr_hash_make(schedule->pool);

    i = -1;
    while (s_handler_list[++i].rpc_id)
    {
        session_set_rpc_handlers(sess, schedule, s_handler_list[i].rpc_id, s_handler_list[i].result_handler, s_handler_list[i].error_handler);
    }

    i = -1;
    while (s_method_list[++i].method)
    {
        session_set_rpc_request_handler(sess, schedule, s_method_list[i].method, s_method_list[i].request_handler);
    }

    session_add_message_handler(sess, schedule, on_session_message);

    return schedule;
}

void schedule_destroy(schedule_t sched)
{
    session_remove_message_handler(sched->session, sched, on_session_message);

    apr_pool_destroy(sched->pool);
    free(sched);
}

// Squirrel away authentication info
void schedule_set_authentication(schedule_t sched, auth_info_t auth)
{
    sched->auth = auth;
}

// Send RPC call to server
static int start_find_meetings(schedule_t sched, const char *opid, const char *base_rpcid, const char *api, int start_row)
{
    const char * session_id = sched->auth->session_id;
    const char * user_id = sched->auth->user_id;
    char rpcid[128];

    sprintf(rpcid, "%s:%s", base_rpcid, opid);

    if (sched->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
    else if (session_make_rpc_call(sched->session, rpcid,
                                        "addressbk",
                                        api,
                                        "(ssii)",
                                        session_id,
										user_id,
                                        start_row,
                                        sched->chunk_size
                                        ))
	{
	    debugf("%s meetings call failed", api);
        return ERR_RPCFAILED;
    }

    return 0;
}

#define DUPSTR(str) (apr_pstrdup(list->pool, str))

// Parse meeting list result set
void parse_find_result(schedule_t sched, resultset_t data, meeting_list_t list, BOOL my_meetings)
{
    int i;
	char *meetingid, *hostid, *type;
    char *privacy, *title, *time;
	char *options, *status;
    char *participantid, *participantrole;
    char *hostfirstname=NULL, *hostlastname=NULL;

   	for (i = 0; i < resultset_get_chunk_size(data); i++)
	{
	    meeting_info_t mtg;

        // "MEETINGID", "HOSTID", "TYPE",
        // "PRIVACY", "TITLE", "TIME",
        // "OPTIONS", "STATUS",
        // "PARTICIPANTID", "ROLL"
		resultset_get_values(data, i, "(ssssssssssss*)",
			&meetingid, &hostid, &type,
            &privacy, &title, &time,
            &options, &status,
            &participantid, &participantrole,
            &hostfirstname, &hostlastname);

        mtg = add_ptr(list->meetings, list->pool, sizeof(struct meeting_info_st));

		mtg->host_id     = DUPSTR(hostid);
		mtg->meeting_id  = DUPSTR(meetingid);
		mtg->title       = DUPSTR(title);
		mtg->type        = atoi(type);
		mtg->privacy     = atoi(privacy);
        mtg->start_time  = atoi(time);
		mtg->options     = atoi(options);
        mtg->status      = atoi(status);

        mtg->my_participant_id   = DUPSTR(participantid);
        mtg->my_role             = atoi(participantrole);
        mtg->host_first_name     = DUPSTR(hostfirstname);
        mtg->host_last_name      = DUPSTR(hostlastname);

        if (my_meetings && mtg->type == TypeInstant)
            sched->instant_meeting = apr_pstrdup(sched->pool, meetingid);
	}
}

int schedule_find_my_meetings(schedule_t sched, const char *opid)
{
    return start_find_meetings(sched, opid, "rpc_fetchmymeetings", "schedule.list_meetings_by_host", 0);
}

int schedule_find_invited_meetings(schedule_t sched, const char *opid)
{
    return start_find_meetings(sched, opid, "rpc_fetchinvitedmeetings", "schedule.list_meetings_invited", 0);
}

void on_my_meetings_fetched(void *user, const char * rpc_id, resultset_t data)
{
    schedule_t sched = (schedule_t)user;
    meeting_list_t list;
    const char *opid = strchr(rpc_id, ':') + 1;

    if (resultset_get_first_index(data) == 0)
    {
        list = make_list(sched->pool);
        sched->my_meetings = list;
    }
    else
    {
        list = sched->my_meetings;
    }

    parse_find_result(sched, data, list, TRUE);

    list->rows_fetched += resultset_get_chunk_size(data);
    list->rows_available = resultset_get_total_rows(data);

	if (resultset_more_rows_available(data) == FALSE)
    {
        // report result to app
        sched->on_list(sched->on_list_data, strchr(rpc_id, ':') + 1, list);
    }
    else
    {
        if (start_find_meetings(sched, opid, "rpc_fetchmymeetings", "schedule.list_meetings_by_host",
                                resultset_get_first_index(data) + sched->chunk_size))
        {
	        on_meeting_fetch_failed(user, opid, DefaultErrorCode, "Fetch my meetings failed");
	    }

	    // report partial progress
	    sched->on_progress(sched->on_progress_data, opid,
                        resultset_get_first_index(data) + sched->chunk_size,
                        resultset_get_total_rows(data));
    }
}

void on_invited_meetings_fetched(void *user, const char * rpc_id, resultset_t data)
{
    schedule_t sched = (schedule_t)user;
    meeting_list_t list;
    const char *opid = strchr(rpc_id, ':') + 1;

    if (resultset_get_first_index(data) == 0)
    {
        list = make_list(sched->pool);
        sched->my_meetings = list;
    }
    else
    {
        list = sched->my_meetings;
    }

    parse_find_result(sched, data, list, FALSE);

    list->rows_fetched += resultset_get_chunk_size(data);
    list->rows_available = resultset_get_total_rows(data);

	if (resultset_more_rows_available(data) == FALSE)
    {
        // report result to app
        sched->on_list(sched->on_list_data, opid, list);
    }
    else
    {
        if (start_find_meetings(sched, opid, "rpc_fetchinvitedmeetings", "schedule.list_meetings_invited",
                               resultset_get_first_index(data) + sched->chunk_size))
	    {
	        on_meeting_fetch_failed(user, opid, DefaultErrorCode, "Fetch invited meetings failed");
	    }

	    // report partial progress
	    sched->on_progress(sched->on_progress_data, opid,
                        resultset_get_first_index(data) + sched->chunk_size,
                        resultset_get_total_rows(data));
    }
}

void on_meeting_fetch_failed(void *user, const char *rpc_id, int code, const char *msg)
{
    schedule_t sched = (schedule_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;

    debugf("Meeting fetch failed: %s", msg);

    sched->on_error(sched->on_error_data, opid, code, msg);
}

int schedule_find_public_meetings(schedule_t sched, const char * opid)
{
    const char * session_id = sched->auth->session_id;
    const char * user_id = sched->auth->user_id;
    const char * community_id = sched->auth->community_id;
    char rpcid[128];

    sprintf(rpcid, "rpc_fetchpublicmeetings:%s", opid);

    if (sched->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
    else if (session_make_rpc_call(sched->session, rpcid,
                                        "addressbk",
                                        "schedule.list_meetings_public",
                                        "(sssii)",
                                        session_id,
                                        community_id,
										user_id,
                                        0,
                                        sched->chunk_size
                                        ))
	{
	    debugf("Fetch public meetings call failed");
        return ERR_RPCFAILED;
	}

	return 0;
}

void on_public_meetings_fetched(void *user, const char * rpc_id, resultset_t data)
{
    schedule_t sched = (schedule_t)user;
    meeting_list_t list;
    int i;

	char *meetingid, *hostid, *type;
    char *privacy, *title, *time;
	char *options, *status;
    char *hostfirstname, *hostlastname;

    if (resultset_get_first_index(data) == 0)
    {
        list = make_list(sched->pool);
        sched->public_meetings = list;
    }
    else
    {
        list = sched->public_meetings;
    }

	for (i = 0; i < resultset_get_chunk_size(data); i++)
	{
	    meeting_info_t mtg;

        // "MEETINGID", "HOSTID", "TYPE",
        // "PRIVACY", "TITLE", "TIME",
        // "OPTIONS", "STATUS",
		resultset_get_values(data, i, "(ssssssssss*)",
			&meetingid, &hostid, &type,
            &privacy, &title, &time,
            &options, &status,
            &hostfirstname, &hostlastname);

        mtg = add_ptr(list->meetings, list->pool, sizeof(struct meeting_info_st));

		mtg->host_id     = DUPSTR(hostid);
		mtg->meeting_id  = DUPSTR(meetingid);
		mtg->title       = DUPSTR(title);
		mtg->type        = atoi(type);
		mtg->privacy     = atoi(privacy);
        mtg->start_time  = atoi(time);
		mtg->options     = atoi(options);
        mtg->status      = atoi(status);

        //mtg->my_participant_id   = DUPSTR(participantid);
        //mtg->my_role             = atoi(participantrole);
        mtg->host_first_name     = DUPSTR(hostfirstname);
        mtg->host_last_name      = DUPSTR(hostlastname);

	}

    list->rows_fetched += resultset_get_chunk_size(data);
    list->rows_available = resultset_get_total_rows(data);

	if (resultset_more_rows_available(data) == FALSE)
    {
        // report result to app
        sched->on_list(sched->on_list_data, strchr(rpc_id, ':') + 1, list);
    }
    else
    {
        const char * session_id = sched->auth->session_id;
        const char * user_id = sched->auth->user_id;
        const char * community_id = sched->auth->community_id;

        if (session_make_rpc_call(sched->session, rpc_id,
                                   "addressbk",
                                   "schedule.list_meetings_public",
                                   "(sssii)",
                                   session_id,
                                   community_id,
                                   user_id,
                                   resultset_get_first_index(data) + sched->chunk_size,
                                   sched->chunk_size
                                   ))
	    {
	        on_meeting_fetch_failed(user, rpc_id, DefaultErrorCode, "Fetch public meetings failed");
	    }

	    // report partial progress
	    sched->on_progress(sched->on_progress_data, strchr(rpc_id, ':') + 1,
                        resultset_get_first_index(data) + sched->chunk_size,
                        resultset_get_total_rows(data));
    }
}

int schedule_find_meeting_details(schedule_t sched, const char *opid, const char * meeting_id)
{
    char rpcid[128];

    sprintf(rpcid, "rpc_fetchmeetingdetails:%s", opid);

    if (sched->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
    else if (sched->auth == NULL)
    {
        debugf("Session not authenticated");
        return ERR_AUTHREQUIRED;
    }
    else if (session_make_rpc_call(sched->session, rpcid,
                                   "addressbk",
                                   "schedule.find_meeting_details",
                                   "(ss)",
                                   sched->auth->session_id,
                                   meeting_id
                                   ))
	{
	    debugf("Fetch meeting details call failed");
        return ERR_RPCFAILED;
	}

	return 0;
}

#define DUPSTR2(str) apr_pstrdup(mtg->pool, str)

meeting_details_t parse_meeting_details(apr_pool_t *pool, resultset_t data)
{

	char *meetingid, *hostid, *type, *privacy, *priority, *title, *descrip, *size, *duration, *start_time, *recurrence, *recur_end, *options;
	char *partid, *ptype, *roll, *userid, *username, *name, *descrip2, *selphone, *phone, *selemail, *email, *screen, *aim, *notify;
    char *password, *end_time, *invite_expiration, *invite_valid_before, *invite_message, *display_attendees, *invitee_chat_options;
    char *communityid, *bridge_phone, *system_url, *hosts_enabled_features;

	int num_rows = resultset_get_chunk_size(data);
	meeting_details_t mtg = make_meeting_details(pool);
	int i;

	// Extra row in result set is meeting descriptor
    // "MEETINGID", "HOSTID", "TYPE", "PRIVACY", "PRIORITY", "TITLE",
    // "DESCRIPTION", "SIZE", "DURATION", "TIME", "RECURRENCE", "RECUR_END",
    // "OPTIONS", "PASSWORD", "END_TIME", "INVITE_EXPIRATION", "INVITE_VALID_BEFORE",
    // "INVITE_MESSAGE", "DISPLAY_ATTENDEES", "INVITEE_CHAT_OPTIONS"
    // "COMMUNITYID", "BRIDGEPHONE", "SYSTEMURL", "ENABLEDFEATURES"
	resultset_get_values(data, num_rows, "(ssssssssssssssssssssssss*)", &meetingid, &hostid, &type, &privacy, &priority, &title, &descrip,
				&size, &duration, &start_time, &recurrence, &recur_end, &options,
                &password, &end_time, &invite_expiration, &invite_valid_before, &invite_message, &display_attendees, &invitee_chat_options,
                &communityid, &bridge_phone, &system_url, &hosts_enabled_features);

	// Set up meeting configuration
    mtg->description = DUPSTR2(descrip);
    mtg->title = DUPSTR2(title);
    mtg->invite_message = DUPSTR2(invite_message);
    mtg->type = atoi(type);
    mtg->privacy = atoi(privacy);
    mtg->options = atoi(options);
    mtg->host_id = DUPSTR2(hostid);
    mtg->meeting_id = DUPSTR2(meetingid);
    mtg->password = DUPSTR2(password);
    mtg->start_time = atoi(start_time);
    mtg->end_time = atoi(end_time);
    mtg->invite_expiration = atoi(invite_expiration);
    mtg->invite_valid_before = atoi(invite_valid_before);
    mtg->display_attendees = atoi(display_attendees);
    mtg->chat_options = atoi(invitee_chat_options);
    mtg->bridge_phone = DUPSTR2(bridge_phone);
    mtg->system_url = DUPSTR2(system_url);
    mtg->host_enabled_features = atoi(hosts_enabled_features);

	for (i = 0; i < num_rows; i++)
	{
		// "PARTICIPANTID", "TYPE", "ROLL", "USERID", "USERNAME", "NAME", "DESCRIPTION", "SELPHONE", "PHONE", "SELEMAIL", "EMAIL",
		// "SCREENNAME", "AIMNAME", "NOTIFYTYPE"
		resultset_get_values(data, i, "(ssssssssssssss)",
					&partid, &ptype, &roll, &userid, &username, &name, &descrip2, &selphone,
					&phone, &selemail, &email, &screen, &aim, &notify);

		if (atoi(ptype) == ParticipantNormal)
		{
			BOOL use_userid = atoi(userid) > 0;
            invitee_t invt;

            invt = (invitee_t) add_ptr(mtg->invitees, pool, sizeof(struct invitee_st));

			// Leave blank for anon users
            if (use_userid)
                invt->user_id = DUPSTR2(userid);

            invt->participant_id = DUPSTR2(partid);

			invt->meeting_id = DUPSTR2(meetingid);
			invt->selphone = atoi(selphone);
			invt->phone = DUPSTR2(phone);
			invt->selemail = atoi(selemail);
			invt->email = DUPSTR2(email);
			invt->notify_type = atoi(notify);
            invt->role = atoi(roll);
	        invt->name = DUPSTR2(name);
	        invt->screenname = DUPSTR2(screen);
	        invt->is_adhoc = !use_userid;
		}
	}

    return mtg;
}

void on_find_meeting_details_fetched(void *user, const char * rpc_id, resultset_t data)
{
    schedule_t sched = (schedule_t) user;
    const char *opid = strchr(rpc_id, ':') + 1;

    meeting_details_t mtg = parse_meeting_details(sched->pool, data);

    sched->on_meeting(sched->on_meeting_data, opid, mtg);
}

const char * schedule_get_my_instant_meeting(schedule_t sched)
{
    return sched->instant_meeting;
}

int schedule_update_meeting(schedule_t sched, const char *opid, meeting_details_t meeting, int email_options)
{
    int priority = 0;
    int meetingsize = 0;
    session_array_t participants;
    BOOL create_meeting = TRUE;

    const char * session_id = sched->auth->session_id;
    const char * community_id = sched->auth->community_id;
    char rpcid[128];
    char jid[512];
    char server[512];
    char username[512];

    if (meeting->meeting_id && *meeting->meeting_id)
        create_meeting = FALSE;

    sprintf(rpcid, (create_meeting ? "rpc_newmeeting:%s" : "rpc_updatemeeting:%s"), opid);

    if (sched->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
    
    strcpy(jid, session_get_jid(sched->session));
    strtok(jid, "@");
    strcpy(server, strtok(NULL, "/"));

    // Create an empty participant array
	participants = session_create_array(0, NULL);

    {
        invitee_iterator_t iter;
        invitee_t invitee;

        iter = meeting_details_iterate(meeting);
        invitee = meeting_details_next(meeting, iter);

        while (invitee != NULL)
        {
            if (invitee->screenname && *invitee->screenname)
                sprintf(username, "%s@%s", invitee->screenname, server);
            else
                *username = '\0';
            
            // PARTICIPANTID, MEETINGID, TYPE, ROLE, USERID, USERNAME, NAME, DESCRIPTION,
            // SEL_PHONE, PHONE, SEL_EMAIL, EMAIL, SCREENNAME, NOTIFYTYPE
            session_add_array_element(participants, "(ssiissssisissi)",
                CHECK_NULL(invitee->participant_id),
                CHECK_NULL(invitee->meeting_id),
                ParticipantNormal,
                invitee->role,
                (invitee->user_id && atoi(invitee->user_id) > 0) ? invitee->user_id : "-1",
                username, 
                CHECK_NULL(invitee->name),
                "",  // description, unused
                invitee->selphone,
                CHECK_NULL(invitee->phone),
                invitee->selemail,
                CHECK_NULL(invitee->email),
                CHECK_NULL(invitee->screenname),
                invitee->notify_type);

            invitee = meeting_details_next(meeting, iter);
        }
    }

    if (create_meeting)
    {
        // SESSIONID, MEETINGID, HOSTID, TYPE, PRIVACY, PRIORITY,
        // TITLE, DESCRIPTION, SIZE, DURATION, PASSWORD, OPTIONS, TIME, END_TIME
        // INVITE_EXPIRATION, INVITE_VALID_BEFORE, INVITE_MESSAGE, DISPLAY_ATTENDEES,
        // INVITEE_CHAT_OPTIONS, PARTICIPANTS(array)
        if (session_make_rpc_call(sched->session, rpcid,
                                            "addressbk",
                                            "schedule.create_meeting",
                                            "(sissssssiiissiisiiiiisiiV)",
                                            session_id,
                                            email_options,
                                            sched->auth->display_name,
                                            meeting->bridge_phone,
                                            meeting->system_url,
                                            community_id,
                                            CHECK_NULL(meeting->meeting_id),
                                            meeting->host_id,
                                            meeting->type,
                                            meeting->privacy,
                                            priority,
                                            CHECK_NULL(meeting->title),
                                            CHECK_NULL(meeting->description),
                                            meetingsize,
                                            0, // duration, unused
                                            CHECK_NULL(meeting->password),
                                            meeting->options,
                                            meeting->start_time,
                                            meeting->end_time,
                                            meeting->invite_expiration,
                                            meeting->invite_valid_before,
                                            CHECK_NULL(meeting->invite_message),
                                            meeting->display_attendees,
                                            meeting->chat_options,
                                            participants
                                            ))
        {
            debugf("Create meeting call failed");
            return ERR_RPCFAILED;
        }
    }
    else
    {
        // SESSIONID, UPDATETYPE, MEETINGID, HOSTID, TYPE, PRIVACY, PRIORITY,
        // TITLE, DESCRIPTION, SIZE, DURATION, PASSWORD, OPTIONS, TIME, END_TIME
        // INVITE_EXPIRATION, INVITE_VALID_BEFORE, INVITE_MESSAGE, DISPLAY_ATTENDEES,
        // INVITEE_CHAT_OPTIONS, PARTICIPANTS(array)
        if (session_make_rpc_call(sched->session, rpcid,
                                            "addressbk",
                                            "schedule.update_meeting",
                                            "(siissssssiiissiisiiiiisiiV)",
                                            session_id,
                                            UpdateMeetingAndParticipants,
                                            email_options,
                                            sched->auth->display_name,
                                            meeting->bridge_phone,
                                            meeting->system_url,
                                            community_id,
                                            CHECK_NULL(meeting->meeting_id),
                                            meeting->host_id,
                                            meeting->type,
                                            meeting->privacy,
                                            priority,
                                            CHECK_NULL(meeting->title),
                                            CHECK_NULL(meeting->description),
                                            meetingsize,
                                            0, // duration, unused
                                            CHECK_NULL(meeting->password),
                                            meeting->options,
                                            meeting->start_time,
                                            meeting->end_time,
                                            meeting->invite_expiration,
                                            meeting->invite_valid_before,
                                            CHECK_NULL(meeting->invite_message),
                                            meeting->display_attendees,
                                            meeting->chat_options,
                                            participants
                                            ))
        {
            debugf("Update meeting call failed");
            return ERR_RPCFAILED;
        }
    }

    return 0;
}

void on_meeting_created(void *user, const char * rpc_id, resultset_t data)
{
    schedule_t sched = (schedule_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;
    char *meetingid;
	int status;

  	resultset_get_output_values(data, "(si*)", &meetingid, &status);

    sched->on_updated(sched->on_updated_data, opid, status, meetingid);
}

void on_meeting_updated(void *user, const char * rpc_id, resultset_t data)
{
    schedule_t sched = (schedule_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;
	int status;

  	resultset_get_output_values(data, "(i*)", &status);

    sched->on_updated(sched->on_updated_data, opid, status, "");
}

void on_meeting_update_failed(void *user, const char *rpc_id, int code, const char *msg)
{
    schedule_t sched = (schedule_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;

    debugf("Meeting update failed: %s", msg);

    sched->on_error(sched->on_error_data, opid, code, msg);
}


int schedule_remove_meeting(schedule_t sched, const char *opid, const char *id)
{
    const char * session_id = sched->auth->session_id;
    char rpcid[128];

    sprintf(rpcid, "rpc_removemeeting:%s", opid);

    if (sched->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
    else if (session_make_rpc_call(sched->session, rpcid,
                                        "addressbk",
                                        "schedule.remove_meeting",
                                        "(ss)",
                                        session_id,
										id
                                        ))
	{
	    debugf("Remove meeting call failed");
        return ERR_RPCFAILED;
	}

    return 0;
}

int schedule_search_meetings(schedule_t sched, const char *opid,
                             const char *meeting_id, const char *title,
                             const char *first_name, const char *last_name,
                             BOOL in_progress,
                             int start_date, int end_date,
                             int starting_row, int chunk_size)
{
    const char * session_id = sched->auth->session_id;
    const char * user_id = sched->auth->user_id;
    const char * community_id = sched->auth->community_id;
    char rpcid[128];

    sprintf(rpcid, "rpc_searchmeetings:%s", opid);

    if (sched->session == NULL)
    {
		debugf("Session is null");
		return ERR_NOTCONNECTED;
    }
    else if (session_make_rpc_call(sched->session, rpcid,
                                        "addressbk",
                                        "schedule.search_meetings",
                                        "(sssisiisssii)",
                                        session_id,
                                        community_id,
                                        user_id,
                                        in_progress,
                                        title ? title : "",
                                        start_date,
                                        end_date,
                                        meeting_id ? meeting_id : "",
                                        first_name ? first_name : "",
                                        last_name ? last_name : "",
                                        starting_row,
                                        chunk_size
                                        ))
	{
        return ERR_RPCFAILED;
	}

    return 0;
}

void on_search_meetings(void *user, const char * rpc_id, resultset_t data)
{
    schedule_t sched = (schedule_t)user;
    const char *opid = strchr(rpc_id, ':') + 1;
    meeting_list_t list;
    int i;

	char *meetingid, *hostid, *type;
    char *privacy, *title, *time;
	char *options, *status;
    char *hostfirstname, *hostlastname;
    char *participantid, *participantrole;

    list = make_list_for_search(sched, opid, TRUE);

	for (i = 0; i < resultset_get_chunk_size(data); i++)
	{
	    meeting_info_t mtg;

		resultset_get_values(data, i, "(ssssssssssss*)",
			&meetingid, &hostid, &type,
            &privacy, &title, &time,
            &options, &status,
            &hostfirstname, &hostlastname,
            &participantid, &participantrole);

        mtg = add_ptr(list->meetings, list->pool, sizeof(struct meeting_info_st));

		mtg->host_id     = DUPSTR(hostid);
		mtg->meeting_id  = DUPSTR(meetingid);
		mtg->title       = DUPSTR(title);
		mtg->type        = atoi(type);
		mtg->privacy     = atoi(privacy);
        mtg->start_time  = atoi(time);
		mtg->options     = atoi(options);
        mtg->status      = atoi(status);

        mtg->my_participant_id   = DUPSTR(participantid);
        mtg->my_role             = atoi(participantrole);
        mtg->host_first_name     = DUPSTR(hostfirstname);
        mtg->host_last_name      = DUPSTR(hostlastname);
	}

    // If same chunk is fetched more then once, we can't tell
    list->rows_fetched += resultset_get_chunk_size(data);
    list->rows_available = resultset_get_total_rows(data);

    // report result to app
    sched->on_list(sched->on_list_data, opid, list);
}

meeting_list_t schedule_find_search_results(schedule_t sched, const char *opid)
{
    return make_list_for_search(sched, opid, FALSE);
}

//
// Accessor functios
//

struct meeting_iterator_st
{
    int pos;
};

meeting_iterator_t meeting_list_iterate(meeting_list_t list)
{
    meeting_iterator_t iter = (meeting_iterator_t) apr_pcalloc(list->pool, sizeof(struct meeting_iterator_st));
    iter->pos = -1;
    return iter;
}

meeting_info_t meeting_list_next(meeting_list_t list, meeting_iterator_t iter)
{
    if (++iter->pos < list->meetings->nelts)
    {
        return (meeting_info_t)get_ptr_at(list->meetings,iter->pos);
    }
    return NULL;
}

void meeting_list_destroy(meeting_list_t list)
{
    apr_pool_destroy(list->pool);
}

struct invitee_iterator_st
{
    int pos;
};

meeting_details_t meeting_details_new(schedule_t sched)
{
    char screen[512];
    
    meeting_details_t mtg = make_meeting_details(sched->pool);
    invitee_t invt;

    // Create new meeting description with default values
    // (meeting id is blank until server allocates it)
    mtg->title = meeting_details_string(mtg, "New Meeting");
	mtg->host_id = sched->auth->user_id;

	mtg->type = TypeScheduled;
	mtg->privacy = PrivacyUnlisted;
    mtg->display_attendees = DisplayAll;
    mtg->chat_options = ChatAll;
    mtg->system_url = sched->auth->system_url;
    mtg->bridge_phone = sched->auth->bridge_phone;
    mtg->invite_expiration = 2*60*60;   // 2 hours

	mtg->options =
        ContinueWithoutModerator |
        Prefer1To1 |
        DefaultCallUsers |
        DefaultSendEmail |
        DefaultSendIM |
        InviteAlwaysValidBeforeStart |
        ContinueWithoutModerator
        ;

    strcpy(screen, session_get_jid(sched->session));
    strtok(screen, "@");

    // Also add host as single participant to new meeting
    invt = meeting_details_add(mtg);
    invt->name = sched->auth->display_name;
    invt->user_id = sched->auth->user_id;
    invt->screenname = apr_pstrdup(mtg->pool, screen);
    invt->role = RollHost;

    return mtg;
}

meeting_details_t meeting_details_copy(schedule_t sched, meeting_details_t copy)
{
    meeting_details_t mtg = make_meeting_details(sched->pool);
    int i;

#define COPY(field) mtg->field = copy->field
#define COPYS(field) mtg->field  = copy->field ? apr_pstrdup(mtg->pool, copy->field) : NULL
	COPYS(meeting_id);
	COPYS(host_id);
	COPY(type);
	COPY(privacy);
	COPYS(title);
	COPY(start_time);
	COPY(options);
	COPY(host_enabled_features);
	COPYS(description);
	COPY(end_time);
	COPY(invite_expiration);
	COPY(invite_valid_before);
	COPYS(invite_message);
	COPY(display_attendees);
	COPY(chat_options);
	COPYS(password);
	COPYS(bridge_phone);
	COPYS(system_url);
#undef COPY
#undef COPYS

    for (i=0; i<copy->invitees->nelts; i++)
    {
        invitee_t copy_invt = (invitee_t)get_ptr_at(copy->invitees, i);
        invitee_t invt = meeting_details_add(mtg);

#define COPY(field) invt->field = copy_invt->field
#define COPYS(field) invt->field = copy_invt->field ? apr_pstrdup(mtg->pool, copy_invt->field) : NULL
        COPYS(meeting_id);
        COPYS(participant_id);
        COPYS(user_id);
        COPY(role);
        COPYS(name);
        COPYS(screenname);
        COPY(selphone);
        COPYS(phone);
        COPY(selemail);
        COPYS(email);
        COPY(notify_type);
        COPY(is_adhoc);
#undef COPY
#undef COPYS
    }

    return mtg;
}

invitee_iterator_t meeting_details_iterate(meeting_details_t mtg)
{
    invitee_iterator_t iter = (invitee_iterator_t) apr_pcalloc(mtg->pool, sizeof(struct invitee_iterator_st));
    iter->pos = -1;
    return iter;
}

invitee_t meeting_details_next(meeting_details_t mtg, invitee_iterator_t iter)
{
    if (++iter->pos < mtg->invitees->nelts)
    {
        return (invitee_t)get_ptr_at(mtg->invitees,iter->pos);
    }
    return NULL;
}

void meeting_details_destroy(meeting_details_t mtg)
{
    apr_pool_destroy(mtg->pool);
}

pstring meeting_details_string(meeting_details_t mtg, const char *str)
{
    return apr_pstrdup(mtg->pool, str);
}

invitee_t meeting_details_add(meeting_details_t mtg)
{
    invitee_t invt = (invitee_t) add_ptr(mtg->invitees, mtg->pool, sizeof(struct invitee_st));

    invt->meeting_id = mtg->meeting_id;
    invt->user_id = "-1";
    invt->role = RollNormal;

    return invt;
}

void meeting_details_remove(meeting_details_t mtg, invitee_t invitee)
{
    int i;

    for (i=0; i<mtg->invitees->nelts; i++)
    {
        invitee_t cur = (invitee_t)get_ptr_at(mtg->invitees, i);
        if (cur == invitee)
        {
            remove_element_at(mtg->invitees, i);
            cur->removed = TRUE;
            break;
        }
    }
}

invitee_t meeting_details_find(meeting_details_t mtg, const char *part_id)
{
    const char *p;
    int i;

    if (!*part_id)
        return NULL;

    p = strchr(part_id, ':');
    if (p)
        part_id = p + 1;

    for (i=0; i<mtg->invitees->nelts; i++)
    {
        invitee_t cur = (invitee_t)get_ptr_at(mtg->invitees, i);
        if (strcmp(part_id, cur->participant_id) == 0)
        {
            return cur;
        }
    }

    return NULL;
}

invitee_t meeting_details_find_host(meeting_details_t mtg)
{
    const char *user_id = mtg->host_id;
    const char *p;
    int i;

    if (!user_id || !*user_id)
        return NULL;

    p = strchr(user_id, ':');
    if (p)
        user_id = p + 1;

    for (i=0; i<mtg->invitees->nelts; i++)
    {
        invitee_t cur = (invitee_t)get_ptr_at(mtg->invitees, i);
        if (strcmp(user_id, cur->user_id) == 0)
        {
            return cur;
        }
    }

    return NULL;
}


//
// Utilities
//

meeting_list_t make_list(apr_pool_t *pool)
{
    apr_pool_t *newpool;
    meeting_list_t list;

    apr_pool_create(&newpool, pool);
    list = (meeting_list_t) apr_pcalloc(newpool, sizeof(struct meeting_list_st));
    list->pool = newpool;
    list->meetings = apr_array_make(newpool, 25, sizeof(meeting_info_t));

    return list;
}

meeting_list_t make_list_for_search(schedule_t sched, const char *opid, BOOL create)
{
    meeting_list_t list;

    list = (meeting_list_t) apr_hash_get(sched->searches, opid, APR_HASH_KEY_STRING);
    if (list == NULL && create)
    {
        list = make_list(sched->pool);
        apr_hash_set(sched->searches, apr_pstrdup(sched->pool, opid), APR_HASH_KEY_STRING, list);
    }

    return list;
}

meeting_details_t make_meeting_details(apr_pool_t *pool)
{
    apr_pool_t *newpool;
    meeting_details_t mtg;

    apr_pool_create(&newpool, pool);
    mtg = (meeting_details_t) apr_pcalloc(newpool, sizeof(struct meeting_details_st));
    mtg->pool = newpool;
    mtg->invitees = apr_array_make(newpool, 25, sizeof(invitee_t));

    return mtg;
}


//
// Static local data
//

struct rpc_handler_st s_handler_list[] =
{
    { "rpc_fetchmymeetings", on_my_meetings_fetched, on_meeting_fetch_failed },
    { "rpc_fetchpublicmeetings", on_public_meetings_fetched, on_meeting_fetch_failed },
    { "rpc_fetchinvitedmeetings", on_invited_meetings_fetched, on_meeting_fetch_failed },
    { "rpc_fetchmeetingdetails", on_find_meeting_details_fetched, on_meeting_fetch_failed },
    { "rpc_newmeeting", on_meeting_created, on_meeting_update_failed },
    { "rpc_updatemeeting", on_meeting_updated, on_meeting_update_failed },
    { "rpc_removemeeting", on_meeting_updated, on_meeting_update_failed },
    { "rpc_searchmeetings", on_search_meetings, on_meeting_fetch_failed },
    { NULL, NULL, NULL }
};

struct rpc_method_st s_method_list[] =
{
	{ NULL, NULL }
};
