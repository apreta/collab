#ifdef WIN32
#include <Windows.h>
#include <json.h>
#include "../sharelib/share_api.h"

BOOL IsChildWindow(HWND hWnd)
{
	return GetAncestor(hWnd, GA_ROOTOWNER)!=hWnd;
}

json_object* getWindows(const char * display)
{
	json_object* out = json_object_new_array();

	MonitorInfo * info = share_get_display(display);
	if (info == NULL)
		return out;

    RECT dispRect;
    SetRect(&dispRect, info->tx, info->ty, info->bx, info->by);
    
	HWND hWnd = GetWindow(GetForegroundWindow(), GW_HWNDLAST);

	while (hWnd != NULL)
	{
		if (IsWindowVisible(hWnd) && IsChildWindow(hWnd)==FALSE)
		{
		    RECT rect, intersect;
            wchar_t title[1024] = L"";
            WINDOWPLACEMENT wp = { sizeof(WINDOWPLACEMENT) };

            // Use normal position to figure out which display app is on--if it's really maximized it will still
            // be on the same screen, and if it's minimized the share server will restore it.
            GetWindowRect(hWnd, &rect);
            GetWindowPlacement(hWnd, &wp);
            rect = wp.rcNormalPosition;

            if (IntersectRect(&intersect, &dispRect, &rect) && 
                intersect.bottom - intersect.top > 9 &&
                intersect.right - intersect.left > 9)
            {
                wchar_t wclass[512] = L"";

                int res = GetClassNameW(hWnd, wclass, sizeof(wclass));
				if (res > 0) {
					res = GetWindowTextW(hWnd, title, sizeof(title));
					if (res > 0 && wcscmp(wclass, L"Progman"))
					{
						DWORD procID = 0;
						GetWindowThreadProcessId(hWnd, &procID);
						char buffer[20];
						itoa( procID, buffer, 10 );

						char titleUTF8[1024];

						res = WideCharToMultiByte(CP_UTF8, 0, title, -1, titleUTF8, sizeof(titleUTF8), NULL, NULL);
						if (res > 0) {
							json_object* info = json_object_new_object();
							json_object_object_add(info, (char*)"name", json_object_new_string(titleUTF8));
							json_object_object_add(info, "id", json_object_new_string(buffer));
							json_object_array_add(out, info);
						}
					}
				}
            }
		}
		hWnd = GetNextWindow(hWnd, GW_HWNDPREV);
	}

	return out;
}
#else
#include <json/json.h>

json_object* getWindows(const char * display)
{
	json_object* out = json_object_new_array();
	return out;
}
#endif
