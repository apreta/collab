// sharelaunch.cpp : Defines the entry point for the console application.
//

#ifdef WIN32
#include "stdafx.h"
#include <Windows.h>
#include <sys/types.h>
#include <io.h>
#else 
#include <mach-o/dyld.h>
#include <CoreServices/CoreServices.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <sys/errno.h>
#include <sys/stat.h>
#endif
#include <time.h>

#include "../sharelib/share_api.h"
#include "version.h"

#include <log.h>

#ifdef WIN32
#include <json.h>
#else
#include <json/json.h>
#endif

#ifdef WIN32
#define strcasecmp _stricmp
#define strncasecmp _strnicmp
#define write _write
#define read _read

// make sure to build as console app
#define STDIN_FILENO 0
#define STDOUT_FILENO 1
#define STDERR_FILENO 2
#endif

extern json_object* getWindows(const char * display);

std::string exec_path;

typedef int (*request_handler)(const char *, struct json_object *request); 


int send_string(const char *msg) 
{
    int len = strlen(msg);
    
    int bytes = write(STDOUT_FILENO, &len, 4);
    if (bytes == 4) {
        bytes = write(STDOUT_FILENO, msg, len);
        if (bytes != len) {
            debugf("Send message incomplete: %d", errno);
            return -1;
        }
    }
    if (bytes < 1) {
        debugf("Send message failed: %d", errno);
        return -1;
    }
    
    return 0;
}

int send_simple_message(const char *action, const char *key, const char *data)
{
    struct json_object* response = json_object_new_object();
    if (response) {
        json_object_object_add(response, (char*)"action", json_object_new_string((char*)action));
        json_object_object_add(response, (char*)key, json_object_new_string((char*)data));
    
        char *msg = json_object_get_string(response);
        int stat = send_string(msg);
    
        json_object_put(response);
        return stat;
    }
    return -1;
}

int send_object_message(const char *action, const char *key, json_object *val)
{
    struct json_object* response = json_object_new_object();
    if (response) {
        json_object_object_add(response, (char*)"action", json_object_new_string((char*)action));
        json_object_object_add(response, (char*)key, val);
    
        char *msg = json_object_get_string(response);
        int stat = send_string(msg);
    
        json_object_put(response);
        return stat;
    }
    return -1;
}

int read_message(struct json_object** request)
{
    debugf("Waiting for request");

    char buffer[4096];
    int len;

retry:    
    int bytes = read(STDIN_FILENO, &len, 4);
    if (bytes == 4) {
        debugf("Message len: %d", len);
        
        bytes = read(STDIN_FILENO, buffer, min(len, (int)sizeof(buffer)-1));
        if (bytes > 0) {
            buffer[bytes] = '\0';
            debugf("Message: %s", buffer);
            *request = json_tokener_parse(buffer);
            if (json_is_error(*request)) {
                errorf("Error parsing request, ignoring");
                return -1;
            } else {
                send_simple_message("response", "status", "ok");
            }
        }
    }
    if (bytes < 1) {
        if (errno == EINTR)
            goto retry;
        debugf("Receive error: %d", errno);
        return -1;
    }
    return 0;
}

void event_handler(void *user, int callback_event)
{
    char buffer[32];
    sprintf(buffer, "%d", callback_event);
    send_simple_message("event", "code", buffer);
}

int handle_version(const char *cmd, struct json_object* request)
{
    if (strcmp(cmd, "version") == 0) {
        debugf("Version");
        send_simple_message("response", "version", SHARELAUNCH_VERSION);
        return 1;
    }
    return 0;
}
    
int handle_init(const char *cmd, struct json_object* request)
{
    if (strcmp(cmd, "init") == 0) {
        char *server = json_object_get_string(json_object_object_get(request, (char *)"host"));
        char *password = json_object_get_string(json_object_object_get(request, (char *)"password"));
        char *mtg_id = json_object_get_string(json_object_object_get(request, (char *)"mid"));
        char *part_id = json_object_get_string(json_object_object_get(request, (char *)"pid"));
        int start = json_object_get_int(json_object_object_get(request, (char *)"start"));
        char *key = json_object_get_string(json_object_object_get(request, (char *)"key"));
        bool enable_security = key && *key;

        if (!server || !password || !mtg_id || !part_id) {
            errorf("Init request missing parameter");
            return -1;
        }

        if (strncasecmp(server, "share.mercuri.ca", 16) != 0  && strncasecmp(server, "10.", 3) != 0 && strncasecmp(server, "192.168.", 8) != 0) {
            errorf("Invalid server");
            return -1;
        }

        debugf("Initializing session for %s, %s", mtg_id, part_id);
        share_initialize(exec_path.c_str(), mtg_id, server, password, key, part_id, "Z", enable_security, start);

		share_set_event_handler(NULL, event_handler);

		share_set_options("directx", "enable");

        return 1;
    }
    return 0;
}

int handle_share_desktop(const char *cmd, struct json_object* request)
{
    if (strcmp(cmd, "share_desktop") == 0) {
        debugf("Sharing desktop");
        share_desktop();
        return 1;
    }
    return 0;
}

int handle_share_app(const char *cmd, struct json_object* request)
{
    if (strcmp(cmd, "share_app") == 0) {
        debugf("Sharing app");
        std::string apps = json_object_get_string(json_object_object_get(request, (char *)"apps"));
		boolean flag = json_object_get_boolean(json_object_object_get(request, (char *)"all"));
        share_applications(apps, flag);
        return 1;
    }
    return 0;
}

int handle_share_end(const char *cmd, struct json_object* request)
{
    if (strcmp(cmd, "share_end") == 0) {
        debugf("Sharing end");
        share_end();
        return 1;
    }
    return 0;
}
    
int handle_set_option(const char *cmd, struct json_object* request)
{
    if (strcmp(cmd, "set_option") == 0) {
        char *option = json_object_get_string(json_object_object_get(request, (char *)"option"));
        char *value = json_object_get_string(json_object_object_get(request, (char *)"value"));

        if (!option || !value) {
            errorf("Set option request missing parameter");
            return -1;
        }

        debugf("Set option %s: %s", option, value);
        share_set_options(option, value);
        return 1;
    }
    return 0;
}

int handle_get_monitors(const char *cmd, struct json_object* request)
{
    if (strcmp(cmd, "get_monitors") == 0) {
		json_object* list = json_object_new_array();

		int num = share_get_number_displays();
		for (int i=0; i<num; i++) {
			MonitorInfo *disp = share_get_display_at(i);
			if (disp == NULL)
				break;
			json_object* info = json_object_new_object();
			json_object_object_add(info, (char*)"description", json_object_new_string((char*)disp->description.c_str()));
			json_object_object_add(info, (char*)"name", json_object_new_string((char*)disp->deviceName.c_str()));
			json_object_object_add(info, (char*)"width", json_object_new_int(disp->bx - disp->tx));
			json_object_object_add(info, (char*)"height", json_object_new_int(disp->by - disp->ty));
			json_object_array_add(list, info);
		}
		send_object_message("response", "monitors", list);
		return 1;
	}
	return 0;
}
    
int handle_get_apps(const char *cmd, struct json_object* request)
{
    if (strcmp(cmd, "get_apps") == 0) {
        char *display = json_object_get_string(json_object_object_get(request, (char *)"display"));
		json_object* list = getWindows(display);
		send_object_message("response", "apps", list);
		return 1;
	}
	return 0;
}

int process_message(struct json_object* request) 
{
    static request_handler _handlers[] = {handle_version, handle_init, handle_share_desktop, handle_share_app, handle_share_end, handle_set_option, handle_get_monitors, handle_get_apps, NULL};
    int stat = -1;

    char *cmd = json_object_get_string(json_object_object_get(request, (char *)"command"));
    if (!cmd)
        return stat; 
        
    int i = 0;
    while (_handlers[i]) {
        stat = _handlers[i](cmd, request);
        if (stat != 0)
            break;
        ++i;
    }
    return stat;
}

int run_loop()
{
    struct json_object* request;
    
    int stat = 0;
    while(!stat) {
        stat = read_message(&request);
        if (!stat) {
            process_message(request);
            if (request)
                json_object_put(request);
        }
    }
    
    return 0;
}

void shutdown()
{
	share_end();
}

int main(int argc, char* argv[])
{
	std::string log_path;

#ifdef WIN32
	HMODULE hModule = GetModuleHandle(NULL);
	char path[MAX_PATH];
	GetModuleFileNameA(hModule, path, MAX_PATH);
	char *end = strrchr(path, '\\');
	*(end+1) = '\0';

	exec_path = path;
	log_path = path;
#else
    char path[4096];
    uint32_t size = sizeof(path);
    if (_NSGetExecutablePath(path, &size) == 0) {
        char *end = strrchr(path, '/');
        *(end+1) = '\0';
    } else {
        *path = '\0';
    }
    
    exec_path = path;    
    
    FSRef dir;
    FSFindFolder(kUserDomain, kApplicationSupportFolderType, TRUE, &dir);
    FSRefMakePath(&dir, (UInt8*) path, sizeof(path));
    strncat(path, "/meeting", sizeof(path)-1);
    mkdir(path, 0755);

    log_path = path;
#endif
    
	atexit(shutdown);

    net_init_log(log_path.c_str(), TRUE);

    debugf("Share launch starting");
	
    run_loop();

    debugf("Share launch exiting");

	return 0;
}
