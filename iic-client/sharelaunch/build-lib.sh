#!/bin/sh

# Builds sharedriver library

# To change /usr/include, /usr/lib, etc.
# "-isysroot /Developer/SDKs/MacOSX10.5.sdk -mmacosx_version_min=10.5"
# (should we use -sysroot instead?)

VERSION=$1
BUILD_NUM=$2
OPTIONS=

CURL_VER="7.21.2"
CURL_DIR="curl-$CURL_VER"

WX_VER="2.8.11"
WX_DIR="wxMac-$WX_VER"

JPEG_DIR="jpeg-8b"

# -- No 10.6 SDK in new Xcode, and wx 2.8 doesn't build with newer SDKs
# SDK_ROOT="/Applications/Developer/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs"
SDK_ROOT="/Xcode4/SDKs"

if ! echo $PATH | grep "^/usr/local/bin"; then 
  export PATH=/usr/local/bin:$PATH
  gcc --version
fi

PREFIX=/usr/local
#PREFIX=/opt/conferencing

# modify path so our wxWidgets is picked up first
#export PATH=$PREFIX/bin:$PATH
#export PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig/:$PKG_CONFIG_PATH

function set_build_env {
	osx_sdk=$1
		
	#arch_flags="-arch $2"
	arch_flags="-arch i386"
	#arch_flags="-arch i386 -arch x86_64"
	
	gcc_flags="-g -isysroot $SDK_ROOT/MacOSX${osx_sdk}.sdk -mmacosx-version-min=${osx_sdk} -headerpad_max_install_names"

	export MACOSX_DEPLOYMENT_TARGET="10.6"
	export CFLAGS="$arch_flags $gcc_flags"
	export CXXFLAGS="$arch_flags $gcc_flags"
	export CPPFLAGS="$arch_flags $gcc_flags"
	export LDFLAGS="$arch_flags $gcc_flags"
	export OBJCFLAGS="$arch_flags"
	export OBJCXXFLAGS="$arch_flags"
}

function rebuild_configure {
    bakefilize --copy
    bakefile -f autoconf $1.bkl
    aclocal -I $PREFIX/share/aclocal
    autoreconf -fi
}

function check_env {
	if [ ! -d $WX_DIR ]; then
		echo "wxWidgets source not found, please place in wxMac"
		return -1
	fi 
	if [ ! -e wxMac ]; then
		ln -s $WX_DIR wxMac 
	fi
	if [ ! -d $CURL_DIR ]; then
		echo "libcurl source not found, please place in $CURL_DIR"
		return -1
	fi 
	if [ ! -d $JPEG_DIR ]; then
		echo "libjpeg source not found, please place in $JPEG_DIR"
		return -1
	fi 
	if [ ! -d gnutls-libs/gnutls-2.8.6 ]; then
		echo "gnutls source not found, please place in gnutls/gnutls-2.8.6"
		return -1
	fi

	return 0
}

set_build_env "10.6"

if [ "$1" == "--setenv" ]; then
	exit 0
fi

#pushd ../.. >/dev/null

shift 2

until [ -z "$1" ]
  do
    case $1 in
     --make)  MAKE="y" ;;
     *) echo "Unknown option $1"; exit -1 ;;
    esac
    shift
  done  
 
#check_env || exit -1

if [ -z "$MAKE" ]; then 
	make clean
else
    echo "Make mode"
fi

#
# Build
#

echo "Building sharelaunch..."
( 
  if [ -n "$VERSION" ]; then
  	echo "Setting version to $VERSION"
	sed "s|@@ver@@|$VERSION|g" version.h.in >version.h
  fi
  if [ -z "$MAKE" ]; then
    rebuild_configure sharelaunch
    ./configure --prefix=$PREFIX --disable-dependency-tracking
  fi
  make || exit -1 
  make install
)


echo "DONE"

#popd >/dev/null
