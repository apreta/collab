Summary: Pidgin IM client for Novell Conferencing
Name: pidgin
Version: 2.0.2
Release: 1
License: GPLv2
Group: Applications/Communication
URL: http://www.sitescape.com
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Requires: gnutls gtk2


%description
This package installs the pidgin IM client for Novell Conferencing.

%debug_package

%prep
%setup -q

%build

%install
rm -rf $RPM_BUILD_ROOT

mkdir -p $RPM_BUILD_ROOT/opt/pidgin/lib
mkdir -p $RPM_BUILD_ROOT/opt/pidgin/bin
mkdir -p $RPM_BUILD_ROOT/opt/pidgin/share
( tar -c opt/pidgin/lib opt/pidgin/bin opt/pidgin/share | tar -x -C $RPM_BUILD_ROOT )

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/opt/pidgin/bin
/opt/pidgin/lib
/opt/pidgin/share

%post
echo "/opt/pidgin/lib" >/etc/ld.so.conf.d/pidgin.conf
/sbin/ldconfig
cp /opt/pidgin/share/applications/pidgin.desktop /usr/share/applications/
cp /opt/pidgin/share/icons/hicolor/48x48/apps/pidgin.png /usr/share/pixmaps/

%postun
if [ "$1" = 0 ]; then
  rm -f etc/ld.so.conf.d/pidgin.conf
fi
/sbin/ldconfig 
if [ "$1" = 0 ]; then
  rm /usr/share/applications/pidgin.desktop
  rm /usr/share/pixmaps/pidgin.png
fi

%changelog
* Thu Oct  7 2004 root <root@dbs.homedns.org> 1.0.0-1
- Initial build.


