#!/bin/sh

# prerequisites:
#  build/install netlib & iiclib into /opt/conferencing
#  install intltool
#  install aspell-devel
#  build/install gtkspell-2.0.x into /opt/pidgin
#  add /opt/pidgin/lib/pkgconfig to PKG_CONFIG_PATH

VERSION=$1
BUILD_NUM=$2
ARCHIVE=pidgin-$VERSION

bldtmp=`pwd`/tempdir

if [ ! .$3 == ."--noconfigure" ]; then
    export CFLAGS="-g -O0"
    ./configure --prefix=/opt/pidgin --disable-perl --disable-tcl
fi

make || ( echo "Failed"; exit -1 )

rm -rf ${bldtmp}
mkdir ${bldtmp}
mkdir $bldtmp/$ARCHIVE

echo "Installing..."

# make DESTDIR=${bldtmp}/$ARCHIVE install || ( echo "Failed"; echo -1 )
# cp ./libpurple/protocols/zon/silence.wav ${bldtmp}/$ARCHIVE/opt/pidgin/share/sounds/pidgin

sudo make install || ( echo "Failed"; echo -1 )
sudo cp ./libpurple/protocols/zon/silence.wav /opt/pidgin/share/sounds/pidgin

echo "Packaging sources..."

# tar -zcf tempdir/$ARCHIVE.tar.gz -C $bldtmp $ARCHIVE/opt/pidgin/lib \
# 	$ARCHIVE/opt/pidgin/bin $ARCHIVE/opt/pidgin/share

tar -c --exclude="*.a" --exclude="*.la" --exclude="*.h" -C / /opt/pidgin/lib \
	/opt/pidgin/bin /opt/pidgin/share | tar -x -C $bldtmp/$ARCHIVE
tar -zcf $bldtmp/$ARCHIVE.tar.gz -C $bldtmp $ARCHIVE

cp pidgin-zon.spec $bldtmp
sed -i -e "s/Version: .*/Version: $VERSION/" tempdir/pidgin-zon.spec
sed -i -e "s/Release: .*/Release: $BUILD_NUM/" tempdir/pidgin-zon.spec

