/**
 * @file simple.h
 *
 * purple
 *
 * Copyright (C) 2005, Thomas Butter <butter@uni-mannheim.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef _ZON_H
#define _ZON_H

#include <glib.h>
#include <time.h>

#include "cipher.h"
#include "circbuffer.h"
#include "dnsquery.h"
#include "dnssrv.h"
#include "network.h"
#include "proxy.h"
#include "prpl.h"

#include <session_api.h>
#include <addressbk.h>
#include <options.h>
#include <common.h>
#include <ipc.h>

struct zon_operation
{
    int operation;
    union {
        struct {
            contact_t contact;
            group_t from;
            group_t to;
        } move_buddy;
    };
};

enum operations
{
    ZonMoveBuddy,
};

struct zon_invite
{
    const char *mid;
    const char *title;
    const char *descrip;
    const char *message;
    const char *pid;
    const char *name;
    PurpleConnection *gc;
};

struct zon_event
{
    int event;
    union {
        directory_t directory;
        messaging_presence_t presence;
        messaging_im_t im;
        struct
        {
            BOOL fatal;
            int status;
            const char *message;
        } error;
        struct zon_invite invite;
    };
};

enum event_codes
{
    ZonConnected,
    ZonAuthenticated,
    ZonError,
    ZonBuddies,
    ZonContacts,
    ZonPresence,
    ZonIM,
    ZonInvite,
};

enum presence_types
{
    ZonClient,
    ZonMeeting,
    ZonVoice,
};

struct zon_buddy
{
    gchar *screen;

    int presence;       // Best presence from list of presences
    const gchar *status;
    gboolean meeting;
    gboolean voice;

    GList *clients;     // List of presences from different clients
};

struct zon_account_data
{
	PurpleConnection *gc;
	PurpleAccount *account;

    session_t session;
    addressbk_t book;
    directory_t pab;
    directory_t cab;
    guint pulse_timer;
    gboolean show_pab;
    gboolean show_cab;
    gboolean secure;
    gboolean httponly;
    gint directory_count;

    GAsyncQueue *event_queue;

    struct zon_operation *pending_op;

    queue_t ipc_queue;

	gchar *servername;
	gchar *username;
	gchar *password;

	GHashTable *buddies;
	GHashTable *groups;

};


#endif /* _ZON_H */
