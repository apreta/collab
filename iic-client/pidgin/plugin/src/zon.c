/**
 * @file zon.c
 *
 * Zon protocol plugin
 *
 * Copyright (C) 2007 SiteScape
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA	02111-1307	USA
 *
 */

/* libzon is the Zon protocol plugin.
   It uses iiclib to manage the network connection to the Zon server.

   Account actions:
    - Start my instant meeting
    - Show meeting center
    - Manage contacts/buddies

   Zon buddy actions:
    - Start instant meeting
    - Schedule a meeting
    - Invite to meeting
    - Call

    - Add to personal buddies
    - Remove from personal buddies

   Other:
    - Detect Zon meeting invite & show dialog
    - Send presence info to meeting client
    - Send invites on behalf of meetinc client (might not be necessary)

 */

#ifndef WIN32
#define PURPLE_PLUGINS
#endif

#include <locale.h>
#include <libintl.h>

#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#include <glib.h>


#ifdef WIN32
#include "internal.h"
#else
#define _(String) gettext(String)
#endif

#include "accountopt.h"
#include "blist.h"
#include "conversation.h"
#include "dnsquery.h"
#include "debug.h"
#include "notify.h"
#include "privacy.h"
#include "prpl.h"
#include "plugin.h"
#include "util.h"
#include "version.h"
#include "network.h"
#include "xmlnode.h"
#include "request.h"
#include "sound.h"

#include "zon.h"
#include "dnssrv.h"


static const char *zon_fields[] = { "userid", "username", "screenname", "first", "last", "email", "email2", "email3",
                                    "busphone", "mobilephone", "homephone", "otherphone", "defphone" };
static const int zon_num_fields = sizeof(zon_fields)/sizeof(zon_fields[0]);

#define CAB_SUFFIX _("[CAB]")
#define CAB_SUFFIX_LEN 5
#define BUDDY_GROUP _("Buddies")
#define ALL_GROUP _("All")
#define INSTALL_DIR "/opt/conferencing/client"

static GSList * find_matching_buddies(PurpleConnection *gc, contact_t contact)
{
    PurpleBlistNode *gnode, *cnode, *bnode;
    GSList *list = NULL;

    for(gnode = purple_get_blist()->root; gnode; gnode = gnode->next)
    {
        if(!PURPLE_BLIST_NODE_IS_GROUP(gnode)) continue;
        for(cnode = gnode->child; cnode; cnode = cnode->next)
        {
            if(!PURPLE_BLIST_NODE_IS_CONTACT(cnode)) continue;
            for(bnode = cnode->child; bnode; bnode = bnode->next)
            {
                PurpleBuddy *buddy = (PurpleBuddy*)bnode;
                if(!PURPLE_BLIST_NODE_IS_BUDDY(bnode)) continue;
                if(buddy->account == gc->account)
                {
                    contact_t check = buddy->proto_data;
                    // purple_debug_info("zon", "checking buddy %s\n", check->screenname);
                    if (check == contact)
                        list = g_slist_prepend(list, buddy);
                }
            }
        }
    }

    return list;
}

static GSList * find_online_buddies(PurpleConnection *gc)
{
    PurpleBlistNode *gnode, *cnode, *bnode;
    GSList *list = NULL;

    for(gnode = purple_get_blist()->root; gnode; gnode = gnode->next)
    {
        if(!PURPLE_BLIST_NODE_IS_GROUP(gnode)) continue;
        for(cnode = gnode->child; cnode; cnode = cnode->next)
        {
            if(!PURPLE_BLIST_NODE_IS_CONTACT(cnode)) continue;
            for(bnode = cnode->child; bnode; bnode = bnode->next)
            {
                PurpleBuddy *buddy = (PurpleBuddy*)bnode;
                if(!PURPLE_BLIST_NODE_IS_BUDDY(bnode)) continue;
                if(buddy->account == gc->account)
                {
                    PurplePresence *pres = purple_buddy_get_presence(buddy);
                    if (pres && purple_presence_is_online(pres))
                    {
                        list = g_slist_prepend(list, buddy);
                    }
                }
            }
        }
    }

    return list;
}

/*
 * Track presence for different client sign-ons; select and display the best one.
 */

static void insert_buddy_presence(struct zon_buddy *buddy, messaging_presence_t pres)
{
    GList *l;

    for(l = buddy->clients; l != NULL; l = l->next)
    {
        messaging_presence_t check_pres = l->data;
        if (strcmp(pres->resource, check_pres->resource) == 0)
        {
            l->data = pres;
            messaging_presence_destroy(check_pres);
            return;
        }
    }

    buddy->clients = g_list_prepend(buddy->clients, pres);
}

static void remove_buddy_presence(struct zon_buddy *buddy, messaging_presence_t pres)
{
    GList *l;

    for(l = buddy->clients; l != NULL; l = l->next)
    {
        messaging_presence_t check_pres = l->data;
        if (strcmp(pres->resource, check_pres->resource) == 0)
        {
            buddy->clients = g_list_delete_link(buddy->clients, l);
            messaging_presence_destroy(check_pres);
            return;
        }
    }
}

static void select_buddy_presence(struct zon_buddy *buddy)
{
    int best_pres = PRES_UNAVAILABLE;
    const char *status = "";
    GList *l;

    for(l = buddy->clients; l != NULL; l = l->next)
    {
        messaging_presence_t pres = l->data;
        if (pres->presence == PRES_AVAILABLE)
        {
            best_pres = PRES_AVAILABLE;
            status = "";
        }
        else if (pres->presence == PRES_AWAY && best_pres != PRES_AVAILABLE)
        {
            best_pres = PRES_AWAY;
            status = pres->status;
        }
    }

    buddy->presence = best_pres;
    buddy->status = status;

    purple_debug_info("zon", "presence for %s is now %s\n", buddy->screen, buddy->presence == PRES_UNAVAILABLE ? "offline" : "online");
}

static struct zon_buddy *set_buddy_presence(PurpleConnection *gc, enum presence_types type, messaging_presence_t pres)
{
	struct zon_account_data *zact = (struct zon_account_data *)gc->proto_data;

    struct zon_buddy *buddy = g_hash_table_lookup(zact->buddies, pres->user);
    if (buddy == NULL)
    {
        char *screen = g_strdup(pres->user);
        buddy = g_new0(struct zon_buddy, 1);
        g_hash_table_insert(zact->buddies, screen, buddy);
        buddy->screen = screen;
    }

    switch (type)
    {
        case ZonMeeting:
            if (pres->presence == PRES_AVAILABLE && pres->status != NULL)
                buddy->meeting = strstr(pres->status, "meeting") != NULL;
            else
                buddy->meeting = FALSE;
            messaging_presence_destroy(pres);
            break;
        case ZonVoice:
            if (pres->presence == PRES_AVAILABLE && pres->status != NULL)
                buddy->voice = strstr(pres->status, "voice") != NULL;
            else
                buddy->voice = FALSE;
            messaging_presence_destroy(pres);
            break;
        case ZonClient:
        {
            if (pres->presence == PRES_UNAVAILABLE)
                remove_buddy_presence(buddy, pres);
            else
                insert_buddy_presence(buddy, pres);
            select_buddy_presence(buddy);
        }
	}

    return buddy;
}

static void get_buddy_presence(PurpleConnection *gc, const char *screen, const char **type, const char **status)
{
	struct zon_account_data *zact = (struct zon_account_data *)gc->proto_data;
    struct zon_buddy *buddy = g_hash_table_lookup(zact->buddies, screen);

    *type = "unavailable";
    *status = "";

    if (buddy)
    {
        switch (buddy->presence)
        {
            case PRES_AVAILABLE:
                *type = "available";
                break;
            case PRES_AWAY:
                *type = "away";
                break;
        }

        if (buddy->voice && buddy->meeting)
            *status = "iic:voice,meeting";
        else if (buddy->voice)
            *status = "iic:voice";
        else if (buddy->meeting)
            *status = "iic:meeting";
    }
}

#if 0
static const char *get_buddy_presence(PurpleBuddy *buddy)
{
    PurplePresence *pres = purple_buddy_get_presence(buddy);
    PurpleStatus *status = purple_presence_get_active_status(pres);

    switch (purple_status_type_get_primitive(purple_status_get_type(status)))
    {
        case PURPLE_STATUS_AVAILABLE:
            return "available";
        case PURPLE_STATUS_AWAY:
            return "away";
        case PURPLE_STATUS_OFFLINE:
        default:
            return "unavailable";
    }
}

static const char *get_buddy_status(PurpleBuddy *buddy)
{
    PurplePresence *pres = purple_buddy_get_presence(buddy);
    PurpleStatus *status = purple_presence_get_active_status(pres);

    switch (purple_status_type_get_primitive(purple_status_get_type(status)))
    {
        case PURPLE_STATUS_AVAILABLE:
            return purple_status_get_attr_string(status, "status");

        case PURPLE_STATUS_AWAY:
            return purple_status_get_attr_string(status, "message");

        case PURPLE_STATUS_OFFLINE:
        default:
            return NULL;
    }
}
#endif

/*
 * Send messages to Zon meeting client.
 */

static datablock_t send_command(PurpleConnection *gc, datablock_t blk, BOOL launch, BOOL get_reply)
{
	struct zon_account_data *zact = (struct zon_account_data *)gc->proto_data;
	char *dir = NULL;
	char *exec = NULL;
	datablock_t blk_out = NULL;
	int status;

    queue_t q = zact->ipc_queue;
    if (q == NULL)
        return NULL;

    status = queue_send(q, blk);

    if (status == QERR_NO_REMOTE && launch)
    {
        datablock_t blk_signon;
        BOOL result = TRUE;

        /* Couldn't open queue, assume meeting client not running */
#ifdef WIN32
        const char regkey[] = "SOFTWARE\\Zon_Client\\";

        if ((dir = wpurple_read_reg_string(HKEY_LOCAL_MACHINE, regkey, "Install_Dir")))
        {
            exec = g_strdup_printf("%s\\%s", dir, "meeting.exe");
        }
        else
        {
            purple_notify_error(gc, _("Not Installed"), _("The meeting client is not installed."), NULL);
            result = FALSE;
        }
#else
	 struct stat fs;

        exec = g_strdup_printf("%s/bin/%s", INSTALL_DIR, "meeting");
        if (stat(exec, &fs) == ENOENT)
        {
            purple_notify_error(gc, _("Not Installed"), _("The meeting client is not installed."), NULL);
            result = FALSE;
        }
#endif

        if (result && exec)
        {
            /* Launch client telling it to read command from queue.  We don't want to send password on command line
               as that makes it more vulnerable to interception.
            */
            char *argv[] = { exec, "/cmd", NULL };
            GError *err;

            if (!g_spawn_async(NULL, argv, NULL, G_SPAWN_STDOUT_TO_DEV_NULL|G_SPAWN_STDERR_TO_DEV_NULL, NULL, NULL, NULL, &err))
            {
                purple_notify_error(gc, _("Launch Error"), _("Unable to open meeting center."), err->message);
                result = FALSE;
            }

            /* Wait for server queue to become available */
            if (queue_wait(q, 5000))
            {
                purple_notify_error(gc, _("Not Responding"), _("Meeting center is not responding."), NULL);
                result = FALSE;
            }

            /* Send sign on command */
            blk_signon = datablock_create(0);
            datablock_add_string(blk_signon, "/signon", "");
            datablock_add_string(blk_signon, zact->username, "");
            datablock_add_string(blk_signon, zact->servername, "");
            datablock_add_string(blk_signon, zact->password, "");
            queue_send(q, blk_signon);
            datablock_destroy(blk_signon);
        }

        status = queue_send(q, blk);
    }

    if (!status && get_reply)
    {
        blk_out = queue_get(q, 200);
    }

    if (exec)
        g_free(exec);
    if (dir)
        g_free(dir);

    return blk_out;
}

static void send_presence_message(PurpleConnection *gc, const char *screen, const char *presence, const char *status)
{
    datablock_t blk;

    purple_debug_info("zon", "sending presence %s for %s\n", presence, screen);

    blk = datablock_create(0);
    datablock_add_string(blk, "/presence", "");
    datablock_add_string(blk, screen, "");
    datablock_add_string(blk, presence, "");
    if (status)
        datablock_add_string(blk, status, "");

    /* Send presence info to meeting client if it's listening */
    send_command(gc, blk, FALSE, FALSE);

    datablock_destroy(blk);
}

static guint zon_ht_hash(const char *sn)
{
	char *lc = g_utf8_strdown(sn, -1);
	guint bucket = g_str_hash(lc);
	g_free(lc);

	return bucket;
}

static gboolean zon_ht_equals(const char *sn1, const char *sn2)
{
	return (purple_utf8_strcasecmp(sn1, sn2) == 0);
}

static const char *zon_list_icon(PurpleAccount *a, PurpleBuddy *b)
{
	return "zon";
}

static void zon_tooltip_text(PurpleBuddy *b, PurpleNotifyUserInfo *user_info, gboolean full)
{
	struct zon_account_data *zact = b->account->gc->proto_data;

	if (zact)
	{
	    contact_t contact = b->proto_data;
        if (contact)
        {
            if (!contact->isbuddy)
                purple_notify_user_info_add_pair(user_info, _("Type"), _("Contact"));
            if (contact->email && *contact->email)
                purple_notify_user_info_add_pair(user_info, _("E-mail"), contact->email);
            if (contact->email2 && *contact->email2)
                purple_notify_user_info_add_pair(user_info, _("E-mail (2)"), contact->email2);
            if (contact->email3 && *contact->email3)
                purple_notify_user_info_add_pair(user_info, _("E-mail (3)"), contact->email3);
            if (contact->busphone && *contact->busphone)
                purple_notify_user_info_add_pair(user_info, _("Bus. Phone"), contact->busphone);
            if (contact->mobilephone && *contact->mobilephone)
                purple_notify_user_info_add_pair(user_info, _("Mobile Phone"), contact->mobilephone);
            if (contact->homephone && *contact->homephone)
                purple_notify_user_info_add_pair(user_info, _("Home Phone"), contact->homephone);
            if (contact->otherphone && *contact->otherphone)
                purple_notify_user_info_add_pair(user_info, _("Other Phone"), contact->otherphone);
        }
	}
}

static GList *zon_status_types(PurpleAccount *acc)
{
	PurpleStatusType *type;
	GList *types = NULL;

/*    type = purple_status_type_new_full(
		PURPLE_STATUS_AVAILABLE, NULL, NULL, TRUE, TRUE, FALSE); */
	type = purple_status_type_new_with_attrs(
		PURPLE_STATUS_AVAILABLE, NULL, NULL, TRUE, TRUE, FALSE,
		"status", _("Status"), purple_value_new(PURPLE_TYPE_STRING),
		NULL);
	types = g_list_append(types, type);

	type = purple_status_type_new_full(
		PURPLE_STATUS_OFFLINE, NULL, NULL, TRUE, TRUE, FALSE);
	types = g_list_append(types, type);

	type = purple_status_type_new_with_attrs(
		PURPLE_STATUS_AWAY, NULL, NULL, TRUE, TRUE, FALSE,
		"message", _("Message"), purple_value_new(PURPLE_TYPE_STRING),
		NULL);
	types = g_list_append(types, type);

	type = purple_status_type_new_full(
		PURPLE_STATUS_OFFLINE, "contact", _("contact"), TRUE, FALSE, FALSE);
	types = g_list_append(types, type);

#if 0 // Experimental
	type = purple_status_type_new_full(
		PURPLE_STATUS_UNSET, "meeting", _("meeting"), TRUE, FALSE, TRUE);
	types = g_list_append(types, type);
#endif

	return types;
}

/*
 * Account menu options
 */

static void zon_plugin_show_meeting_center(PurplePluginAction *action)
{
	PurpleConnection *gc = (PurpleConnection *) action->context;
	//struct zon_account_data *zact = (struct zon_account_data *)gc->proto_data;

    datablock_t blk = datablock_create(0);
    datablock_add_string(blk, "/showmc", "");

    send_command(gc, blk, TRUE, FALSE);

    datablock_destroy(blk);
}

static void zon_plugin_manage_contacts(PurplePluginAction *action)
{
	PurpleConnection *gc = (PurpleConnection *) action->context;
	//struct zon_account_data *zact = (struct zon_account_data *)gc->proto_data;

    datablock_t blk = datablock_create(0);
    datablock_add_string(blk, "/contacts", "");

    send_command(gc, blk, TRUE, FALSE);

    datablock_destroy(blk);
}

static void zon_plugin_start_instant(PurplePluginAction *action)
{
	PurpleConnection *gc = (PurpleConnection *) action->context;
	//struct zon_account_data *zact = (struct zon_account_data *)gc->proto_data;

    datablock_t blk = datablock_create(0);
    datablock_add_string(blk, "/start", "");

    send_command(gc, blk, TRUE, FALSE);

    datablock_destroy(blk);
}

static GList *zon_plugin_menu(PurplePlugin *plugin, gpointer context)
{
	GList *m = NULL;
	PurplePluginAction *act;

	act = purple_plugin_action_new(_("Show Meeting Center..."), zon_plugin_show_meeting_center);
	m = g_list_append(m, act);

	act = purple_plugin_action_new(_("Manage Contacts..."), zon_plugin_manage_contacts);
	m = g_list_append(m, act);

	act = purple_plugin_action_new(_("Start My Instant Meeting..."), zon_plugin_start_instant);
	m = g_list_append(m, act);

    return m;
}

/*
 * Buddy context menu options
 */

static void zon_buddy_add_buddy(PurpleBlistNode *node, gpointer data)
{
   	PurpleBuddy *buddy;
	PurpleConnection *gc;
	struct zon_account_data *zact = NULL;
	contact_t contact;

	g_return_if_fail(PURPLE_BLIST_NODE_IS_BUDDY(node));

	buddy = (PurpleBuddy *) node;
	gc = purple_account_get_connection(buddy->account);
	zact = gc->proto_data;
	contact = buddy->proto_data;

    /* Create new contact in personal address book and add to buddies */
    contact = directory_contact_copy(zact->book, zact->pab, contact);

    addressbk_buddy_add(zact->book, "addbuddy", contact, FALSE);
}

static void zon_buddy_remove_buddy(PurpleBlistNode *node, gpointer data)
{
   	PurpleBuddy *buddy;
	PurpleConnection *gc;
	struct zon_account_data *zact = NULL;
	contact_t contact;
    GSList *list = NULL, *ptr;

	g_return_if_fail(PURPLE_BLIST_NODE_IS_BUDDY(node));

	buddy = (PurpleBuddy *) node;
	gc = purple_account_get_connection(buddy->account);
	zact = gc->proto_data;
	contact = buddy->proto_data;

	addressbk_buddy_remove(zact->book, "removebuddy", contact);

    purple_blist_remove_buddy(buddy);

    // remove other buddies associated with this contact
    ptr = list = find_matching_buddies(gc, contact);
    while (ptr != NULL)
    {
        if (ptr->data != buddy)
            purple_blist_remove_buddy(ptr->data);
        ptr = g_slist_next(ptr);
    }
    g_slist_free(list);
}

static void zon_buddy_invite(PurpleBlistNode *node, gpointer data)
{
   	PurpleBuddy *buddy;
	PurpleConnection *gc;
	contact_t contact;
    datablock_t blk;

	g_return_if_fail(PURPLE_BLIST_NODE_IS_BUDDY(node));

	buddy = (PurpleBuddy *) node;
	gc = purple_account_get_connection(buddy->account);
	contact = buddy->proto_data;

    blk = datablock_create(0);
    datablock_add_string(blk, "/invite", "");
    datablock_add_string(blk, contact->userid, "");
    datablock_add_string(blk, contact->screenname, "");
    datablock_add_string(blk, buddy->alias, "");
/*
    datablock_add_string(blk, contact->email, "");
    datablock_add_string(blk, contact->busphone, "");
    datablock_add_string(blk, contact->mobilephone, "");
    datablock_add_string(blk, contact->homephone, "");
*/
    send_command(gc, blk, TRUE, FALSE);

    datablock_destroy(blk);
}

static void zon_buddy_schedule(PurpleBlistNode *node, gpointer data)
{
   	PurpleBuddy *buddy;
	PurpleConnection *gc;
	contact_t contact;
    datablock_t blk;

	g_return_if_fail(PURPLE_BLIST_NODE_IS_BUDDY(node));

	buddy = (PurpleBuddy *) node;
	gc = purple_account_get_connection(buddy->account);
	contact = buddy->proto_data;

    blk = datablock_create(0);
    datablock_add_string(blk, "/schedule", "");
    datablock_add_string(blk, contact->userid, "");
    datablock_add_string(blk, contact->screenname, "");
    datablock_add_string(blk, buddy->alias, "");
/*
    datablock_add_string(blk, contact->email, "");
    datablock_add_string(blk, contact->busphone, "");
    datablock_add_string(blk, contact->mobilephone, "");
    datablock_add_string(blk, contact->homephone, "");
*/
    send_command(gc, blk, TRUE, FALSE);

    datablock_destroy(blk);
}

/* Items added to buddy context menu */
static GList *zon_buddy_menu(PurpleBuddy *buddy)
{
	PurpleConnection *gc = purple_account_get_connection(buddy->account);
    contact_t contact = buddy->proto_data;
	GList *m = NULL;
	PurpleMenuAction *act;
	datablock_t cmd, status;
	BOOL meeting_active = FALSE;

	if(!contact)
		return m;

    cmd = datablock_create(0);
    datablock_add_string(cmd, "/status", "");

    status = send_command(gc, cmd, FALSE, TRUE);
    if (status)
    {
        if (strcmp(datablock_get_string(status), "active") == 0)
            meeting_active = TRUE;
        datablock_destroy(status);
    }
    datablock_destroy(cmd);

    if (contact->directory->type == dtGlobal)
    {
        act = purple_menu_action_new(_("Add to Personal Buddies"),
                                   PURPLE_CALLBACK(zon_buddy_add_buddy),
                                   NULL, NULL);
		m = g_list_append(m, act);
    }

    if (contact->directory->type == dtPersonal && contact->isbuddy)
    {
        act = purple_menu_action_new(_("Remove from Personal Buddies"),
                                   PURPLE_CALLBACK(zon_buddy_remove_buddy),
                                   NULL, NULL);
		m = g_list_append(m, act);
    }

    if (!meeting_active)
    {
        act = purple_menu_action_new(_("Start Instant Meeting..."),
                                   PURPLE_CALLBACK(zon_buddy_invite),
                                   NULL, NULL);
        m = g_list_append(m, act);

        act = purple_menu_action_new(_("Schedule a Meeting..."),
                                   PURPLE_CALLBACK(zon_buddy_schedule),
                                   NULL, NULL);
        m = g_list_append(m, act);
    }
    else
    {
        act = purple_menu_action_new(_("Invite to Meeting..."),
                                   PURPLE_CALLBACK(zon_buddy_invite),
                                   NULL, NULL);
        m = g_list_append(m, act);
    }

	return m;
}

static GList *zon_blist_node_menu(PurpleBlistNode *node)
{
	if(PURPLE_BLIST_NODE_IS_BUDDY(node)) {
		return zon_buddy_menu((PurpleBuddy *) node);
	} else {
		return NULL;
	}
}

static void zon_keep_alive(PurpleConnection *gc)
{
	struct zon_account_data *zact = gc->proto_data;
	if(zact)
	{
		purple_debug_info("zon", "sending keep alive\n");
		// ToDo: send ping
	}
	return;
}

static void zon_set_status(PurpleAccount *account, PurpleStatus *status)
{
	PurpleStatusPrimitive primitive = purple_status_type_get_primitive(purple_status_get_type(status));
	struct zon_account_data *zact = NULL;

	if (!purple_status_is_active(status))
		return;

	if (account->gc)
		zact = account->gc->proto_data;

	if (zact)
	{
	    const char *away_msg = NULL;

	    switch (primitive)
	    {
            case PURPLE_STATUS_AVAILABLE:
                session_send_presence(zact->session, NULL, PRES_AVAILABLE, (char *)"iic:");
                break;
            case PURPLE_STATUS_AWAY:
                away_msg = purple_status_get_attr_string(status, "message");
                session_send_presence(zact->session, NULL, PRES_AWAY, (char *)away_msg);
                break;
            case PURPLE_STATUS_OFFLINE:
            default:
                session_send_presence(zact->session, NULL, PRES_UNAVAILABLE, NULL);
                break;
	    }
    }
}

static void zon_add_buddy(PurpleConnection *gc, PurpleBuddy *buddy, PurpleGroup *g)
{
	struct zon_account_data *zact = (struct zon_account_data *)gc->proto_data;
	contact_t contact;

    purple_debug_info("zon", "zon_add_buddy %s\n", buddy->name);

    contact = directory_contact_new(zact->pab);

    contact->first = directory_string(zact->pab, buddy->name);
    contact->screenname = directory_string(zact->pab, buddy->name);

    addressbk_buddy_add(zact->book, "addbuddy", contact, FALSE);

    buddy->proto_data = contact;

    /* We will re-process the directory when the operation completes, so that the contact
       appears in All and Buddies groups.  If the user added the buddy to a different group,
       it will also show up there in the local buddy list.
    */
}

static group_t find_group(PurpleConnection *gc, const char *name)
{
	struct zon_account_data *zact = (struct zon_account_data *)gc->proto_data;

    if (g_str_has_suffix(name, CAB_SUFFIX))
    {
        char *group_name = g_strndup(name, strlen(name) - CAB_SUFFIX_LEN - 1);
        return directory_find_group_by_name(zact->cab, group_name);
    }

    return directory_find_group_by_name(zact->pab, name);
}

static void move_buddy(PurpleConnection *gc, contact_t contact, group_t group_from, group_t group_to)
{
	struct zon_account_data *zact = (struct zon_account_data *)gc->proto_data;

    if (contact)
    {
        if (contact->directory->type == dtGlobal)
        {
            /* Contact was copied from CAB group, so create a new personal contact first, then put in
               appropriate group */
            contact = directory_contact_copy(zact->book, zact->pab, contact);

            if (zact->pending_op == NULL)
            {
                struct zon_operation *op = g_new0(struct zon_operation, 1);
                op->operation = ZonMoveBuddy;
                op->move_buddy.contact = contact;
                op->move_buddy.from = NULL;
                op->move_buddy.to = group_to;

                zact->pending_op = op;
            }

            addressbk_contact_update(zact->book, "movebuddy", dtPersonal, contact, NULL);
        }
        else if (contact->directory == group_to->directory)
        {
            /* Move contact to appropriate group.
               When the add member operation complete, we will re-process the directory to make sure the buddy is also in
               All/Buddies groups if needed. */
            if (group_from && group_from->type == dtOther)
                addressbk_group_remove_member(zact->book, "removemember", group_from, contact->userid);
            if (group_to->type == dtOther)
                addressbk_group_add_member(zact->book, "addmember", group_to, contact->userid);
        }
    }
}

static void zon_group_change(PurpleConnection *gc, const char *name, const char *old_group, const char *new_group)
{
	struct zon_account_data *zact = (struct zon_account_data *)gc->proto_data;
	PurpleBuddy *b;
	PurpleGroup *g;
	group_t group_from;
	group_t group_to;
    contact_t contact;

	if(!old_group || !new_group || !strcmp(old_group, new_group))
		return;

    g = purple_find_group(new_group);
    b = purple_find_buddy_in_group(gc->account, name, g);
    contact = b->proto_data;

    group_from = find_group(gc, old_group);
    group_to = find_group(gc, new_group);

    if (group_from)
    {
        if (group_to == NULL)
        {
            /* No personal group matching this name on server, create first */
            group_to = directory_group_new(zact->pab, new_group);

            if (zact->pending_op == NULL)
            {
                struct zon_operation *op = g_new0(struct zon_operation, 1);
                op->operation = ZonMoveBuddy;
                op->move_buddy.contact = contact;
                op->move_buddy.from = group_from;
                op->move_buddy.to = group_to;

                zact->pending_op = op;
            }

            addressbk_group_update(zact->book, "movebuddy", dtPersonal, group_to);
        }
        else
        {
            /* Move contact between existing groups */
            move_buddy(gc, contact, group_from, group_to);
            purple_debug_info("zon", "zon_group_change %s\n", b->name);
        }
    }
}

static void zon_group_rename(PurpleConnection *gc, const char *old_name, PurpleGroup *g, GList *moved_buddies)
{
	struct zon_account_data *zact = (struct zon_account_data *)gc->proto_data;
	group_t group;

    group = find_group(gc, old_name);
    if (group && group->type == dtOther)
    {
        group->name = directory_string(group->directory, g->name);
        addressbk_group_update(zact->book, "renamegroup", group->directory->type, group);
    }
}

static void zon_group_remove(PurpleConnection *gc, PurpleGroup *g)
{
	struct zon_account_data *zact = (struct zon_account_data *)gc->proto_data;
	group_t group;

    group = find_group(gc, g->name);
    if (group && group->type == dtOther && group->directory->type == dtPersonal)
    {
        addressbk_group_delete(zact->book, "removegroup", group);
    }
}

static void zon_remove_buddy(PurpleConnection *gc, PurpleBuddy *buddy, PurpleGroup *group)
{
	struct zon_account_data *zact = (struct zon_account_data *)gc->proto_data;
	contact_t contact;

    purple_debug_info("zon", "zon_remove_buddy %s\n", buddy->name);

    contact = buddy->proto_data;

    if (contact && contact->directory->type == dtPersonal)
    {
        GSList *list = NULL, *ptr;

        // Removes contact from all groups it is in on server, so...
        addressbk_contact_delete(zact->book, "removebuddy", contact);

        // remove other buddies associated with this contact
        ptr = list = find_matching_buddies(gc, contact);
        while (ptr != NULL)
        {
            if (ptr->data != buddy)
                purple_blist_remove_buddy(ptr->data);
            ptr = g_slist_next(ptr);
        }
        g_slist_free(list);

        buddy->proto_data = NULL;
    }
}

static int zon_im_send(PurpleConnection *gc, const char *who, const char *what, PurpleMessageFlags flags)
{
	struct zon_account_data *zact = gc->proto_data;

    /* Just to be compatible with classic Zon, which doesn't want quotes in attribute values */
    char *text = purple_strreplace(what, "\"", "");

    session_send_im(zact->session, NULL, who, text, FALSE);

    g_free(text);

	return 1;
}

static unsigned int zon_typing(PurpleConnection *gc, const char *name, PurpleTypingState state)
{
	struct zon_account_data *zact = gc->proto_data;

	if(state == PURPLE_TYPING)
	{
	    session_send_im(zact->session, NULL, name, NULL, TRUE);
	}
	else
	{
	    session_send_im(zact->session, NULL, name, NULL, FALSE);
	}

	return 0;
}

static void on_connected_event(void *user)
{
	PurpleConnection *gc = user;
	struct zon_account_data *zact = gc->proto_data;

    struct zon_event *event = g_new0(struct zon_event, 1);
    event->event = ZonConnected;

    g_async_queue_push(zact->event_queue, event);

    addressbk_authenticate(zact->book, zact->username, "");
}

static void on_error_event(void *user, int err, const char *msg, int code, const char *id, const char *resource)
{
	PurpleConnection *gc = user;
	struct zon_account_data *zact = gc->proto_data;

    struct zon_event *event = g_new0(struct zon_event, 1);
    event->event = ZonError;
    event->error.status = err;
    event->error.fatal = TRUE;
    event->error.message = g_strdup(msg);
    g_async_queue_push(zact->event_queue, event);
}

static void on_update_failed_event(void *user, const char *opid, int status, const char *msg)
{
	PurpleConnection *gc = user;
	struct zon_account_data *zact = gc->proto_data;

    struct zon_event *event = g_new0(struct zon_event, 1);
    event->event = ZonError;
    event->error.status = status;
    event->error.fatal = FALSE;
    event->error.message = g_strdup(msg);
    g_async_queue_push(zact->event_queue, event);
}

static void on_auth_event(void *user)
{
	PurpleConnection *gc = user;
	struct zon_account_data *zact = gc->proto_data;

    struct zon_event *event = g_new0(struct zon_event, 1);
    event->event = ZonAuthenticated;
    g_async_queue_push(zact->event_queue, event);

    addressbk_set_field_list(zact->book, zon_fields, zon_num_fields);

    addressbk_fetch_contacts(zact->book, "personal", dtPersonal);

    if (zact->show_cab)
    {
        zact->directory_count = 2;
        addressbk_fetch_contacts(zact->book, "global", dtGlobal);
    }
    else
        zact->directory_count = 1;

}

static void on_directory_event(void *user, const char *opid, directory_t directory)
{
	PurpleConnection *gc = user;
	struct zon_account_data *zact = gc->proto_data;

    if (strcmp(opid, "buddies") == 0) // Not currently used
    {
        struct zon_event *event = g_new0(struct zon_event, 1);
        event->event = ZonBuddies;
        event->directory = directory;
        g_async_queue_push(zact->event_queue, event);
        zact->pab = directory;
    }
    else if (strcmp(opid, "personal") == 0)
    {
        struct zon_event *event = g_new0(struct zon_event, 1);
        event->event = ZonContacts;
        event->directory = directory;
        g_async_queue_push(zact->event_queue, event);
        zact->pab = directory;
    }
    else if (strcmp(opid, "global") == 0)
    {
        struct zon_event *event = g_new0(struct zon_event, 1);
        event->event = ZonContacts;
        event->directory = directory;
        g_async_queue_push(zact->event_queue, event);
        zact->cab = directory;
    }

    /* Want to have all buddies downloads before we get presence, and we don't get buddy presence until we
       set our presence. */
    if (--zact->directory_count == 0)
        session_send_presence(zact->session, NULL, PRES_AVAILABLE, (char *)"iic:");
}

static void on_updated_event(void *user, const char *opid, const char *item_id)
{
	PurpleConnection *gc = user;
	struct zon_account_data *zact = gc->proto_data;

    if (strcmp(opid, "addbuddy") == 0 || strcmp(opid, "addmember") == 0)
    {
        struct zon_event *event = g_new0(struct zon_event, 1);
        event->event = ZonContacts;
        event->directory = zact->pab;
        g_async_queue_push(zact->event_queue, event);
    }
    else if (strcmp(opid, "movebuddy") == 0)
    {
        struct zon_operation *op = zact->pending_op;
        zact->pending_op = NULL;
        if (op && op->operation == ZonMoveBuddy)
        {
            move_buddy(gc, op->move_buddy.contact, op->move_buddy.from, op->move_buddy.to);
        }
        g_free(op);

        if (zact->show_cab)
        {
            /* Cause directory to be re-processed b/c its been removed from the source group by the
               move operation, but is still in the group on the server side. */
            struct zon_event *event = g_new0(struct zon_event, 1);
            event->event = ZonContacts;
            event->directory = zact->cab;
            g_async_queue_push(zact->event_queue, event);
        }
    }
}

static void on_deleted_event(void *user, const char *opid, const char *item_id)
{
	PurpleConnection *gc = user;
	struct zon_account_data *zact = gc->proto_data;

    if (strcmp(opid, "removebuddy") == 0)
    {
        struct zon_event *event = g_new0(struct zon_event, 1);
        event->event = ZonContacts;
        event->directory = zact->pab;
        g_async_queue_push(zact->event_queue, event);
    }
}

/*
 * Handle incoming presence messages
 */

static void on_presence_event(void *user, messaging_presence_t pres)
{
	PurpleConnection *gc = user;
	struct zon_account_data *zact = gc->proto_data;

    struct zon_event *event = g_new0(struct zon_event, 1);
    event->event = ZonPresence;
    event->presence = pres;
    g_async_queue_push(zact->event_queue, event);
}

static void process_presence(PurpleConnection *gc, messaging_presence_t pres)
{
	struct zon_account_data *zact = gc->proto_data;
    const char *screen = pres->user;
    struct zon_buddy *buddy = NULL;

    if (strncmp(pres->resource, "iic", 3) == 0)
    {
        buddy = set_buddy_presence(gc, ZonClient, pres);

        switch (buddy->presence)
        {
            case PRES_UNAVAILABLE:
                purple_prpl_got_user_status(zact->account, screen, purple_primitive_get_id_from_type(PURPLE_STATUS_OFFLINE), NULL);
                break;
            case PRES_AVAILABLE:
                purple_prpl_got_user_status(zact->account, screen, purple_primitive_get_id_from_type(PURPLE_STATUS_AVAILABLE), NULL);
                break;
            case PRES_AWAY:
                purple_prpl_got_user_status(zact->account, screen, purple_primitive_get_id_from_type(PURPLE_STATUS_AWAY), "message", buddy->status, NULL);
                break;
        }
    }
    else if (strncmp(pres->resource, "mtg", 3) == 0)
    {
        buddy = set_buddy_presence(gc, ZonMeeting, pres);
    }
    else if (strncmp(pres->resource, "voice", 5) == 0)
    {
        buddy = set_buddy_presence(gc, ZonVoice, pres);
    }
    else
    {
        messaging_presence_destroy(pres);
    }

    if (buddy)
    {
        const char *message, *status;
        get_buddy_presence(gc, buddy->screen, &message, &status);
        send_presence_message(gc, buddy->screen, message, status);
    }
}

static void on_im_event(void *user, messaging_im_t im)
{
	PurpleConnection *gc = user;
	struct zon_account_data *zact = gc->proto_data;

    struct zon_event *event = g_new0(struct zon_event, 1);
    event->event = ZonIM;
    event->im = im;
    g_async_queue_push(zact->event_queue, event);
}

static void process_im(PurpleConnection *gc, messaging_im_t im)
{
	struct zon_account_data *zact = gc->proto_data;

    if (im->body == NULL)
    {
        if (im->composing)
            serv_got_typing(zact->gc, im->user, 0, PURPLE_TYPING);
        else
            serv_got_typing(zact->gc, im->user, 0, PURPLE_NOT_TYPING);
    }
    else
        serv_got_im(zact->gc, im->user, purple_unescape_html(im->body), 0, time(NULL));

    messaging_im_destroy(im);
}

/*
 * Handle meeting invites.
 */

static void on_im_invite(void *user, const char *id, const char *method, resultset_t result)
{
	PurpleConnection *gc = user;
	struct zon_account_data *zact = gc->proto_data;
    char *mid, *title, *descrip, *message, *pid, *name;

    struct zon_event *event = g_new0(struct zon_event, 1);

    resultset_get_output_values(result, "(ssssss)", &mid, &title, &descrip, &message, &pid, &name);

    event->event = ZonInvite;
    event->invite.mid = g_strdup(mid);
    event->invite.title = g_strdup(title);
    event->invite.descrip = g_strdup(descrip);
    event->invite.message = g_strdup(message);
    event->invite.pid = g_strdup(pid);
    event->invite.name = g_strdup(name);
    g_async_queue_push(zact->event_queue, event);
}

static void invite_reject(struct zon_invite *invite)
{
    g_free((void *)invite->mid);
    g_free((void *)invite->title);
    g_free((void *)invite->descrip);
    g_free((void *)invite->message);
    g_free((void *)invite->pid);
    g_free((void *)invite->name);
    g_free(invite);
}

static void invite_accept(struct zon_invite *invite)
{
    datablock_t blk;

    blk = datablock_create(0);
    datablock_add_string(blk, "/join", "");
    datablock_add_string(blk, invite->mid, "");
    datablock_add_string(blk, invite->pid, "");
    datablock_add_string(blk, invite->name, "");

    send_command(invite->gc, blk, TRUE, FALSE);

    datablock_destroy(blk);

    g_free((void *)invite->mid);
    g_free((void *)invite->title);
    g_free((void *)invite->descrip);
    g_free((void *)invite->message);
    g_free((void *)invite->pid);
    g_free((void *)invite->name);
    g_free(invite);
}

static void process_im_invite(PurpleConnection *gc, struct zon_invite *invite)
{
	struct zon_account_data *zact = gc->proto_data;
    char *message;
    struct zon_invite *copy = g_new0(struct zon_invite, 1);

    *copy = *invite;
    copy->gc = gc;

    message = g_strdup_printf(_("%s has been invited to a meeting.\nMeeting ID: %s\nMeeting Title: %s\nYour PIN: %s\nAccept the invitation?"),
                 zact->username, invite->mid, invite->title, invite->pid);

    purple_request_yes_no(gc, NULL, _("Accept invitation?"), message,
                           PURPLE_DEFAULT_ACTION_NONE, gc->account, zact->username, NULL,
                           copy, G_CALLBACK(invite_accept),
                           G_CALLBACK(invite_reject));

    g_free(message);
}

static void send_im_invite(PurpleConnection *gc, const char *mid, const char *title, const char *descrip, const char *message,
                           const char *pid, const char *name, const char *system, const char *screen)
{
	struct zon_account_data *zact = gc->proto_data;

    if (strcmp(system, "zon") == 0)
    {
        char jid[512];

        sprintf(jid, "%s@%s", screen, zact->servername);
        //int session_make_rpc_call(session_t sess, const char *id, const char *service, const char *method, const char *format, ...);
        session_make_rpc_call(zact->session, "rpc_iminvite", jid, "im.invite", "(ssssss)", mid, title, descrip, message, pid, name);
    }
}

/*
 * Handle server-side address books (buddies & contacts)
 */

static char *buddy_display_name(contact_t contact)
{
    int number = ((contact->first && *contact->first) ? 1 : 0) + ((contact->last && *contact->last) ? 1 : 0);
    char *display_name = g_strdup_printf("%s%s%s",
                                        contact->first ? contact->first : "",
                                        number > 1 ? " " : "",
                                        contact->last ? contact->last : "");

    return display_name;
}

/* Add buddy from server to local buddy list */
static PurpleBuddy *add_buddy(PurpleConnection *gc, PurpleGroup *g, contact_t contact)
{
	struct zon_account_data *zact = gc->proto_data;
    char *display_name = buddy_display_name(contact);
    char *buddy_name;
	PurpleBuddy *b = NULL;

    if (contact->username && *contact->username)
    {
        buddy_name = g_strdup(contact->username);
        g_strdelimit(buddy_name, "@", '\0');
    }
    else if (contact->screenname && *contact->screenname)
    {
        buddy_name = g_strdup(contact->screenname);
    }
    else
    {
        // Not sure what else to use, only unique item
        buddy_name = g_strdup_printf("%s%s", _("contact"), contact->userid);
    }

    b = purple_find_buddy_in_group(zact->account, buddy_name, g);
    if(!b)
    {
        b = purple_buddy_new(zact->account, buddy_name, display_name);

        purple_debug_info("zon", "zon_add_buddy %s to %s\n", b->alias, g->name);
        purple_blist_add_buddy(b, NULL, g, NULL);
    }
    else
    {
        serv_got_alias(gc, buddy_name, display_name);
    }
    b->proto_data = contact;

    // Still just displays as offline
    if (contact->directory->type == dtPersonal && !contact->isbuddy)
        purple_prpl_got_user_status(zact->account, buddy_name, "contact", NULL);

    g_free(display_name);

    return b;
}

/* Add group from server to local buddy list */
static PurpleGroup *add_group(PurpleConnection *gc, group_t group, const char *group_name)
{
	//struct zon_account_data *zact = gc->proto_data;
	PurpleGroup *g;

    g = purple_find_group(group_name);
    if(!g)
    {
        g = purple_group_new(group_name);
        purple_blist_add_group(g, NULL);
    }

    return g;
}

static void add_group_members(PurpleConnection *gc, directory_t dir, group_t group, const char *name)
{
	struct zon_account_data *zact = gc->proto_data;
	PurpleGroup *g;
    contact_t contact;
    int i,size = directory_group_get_size(group);
    char *group_name;

    if (dir->type == dtGlobal)
    {
        group_name = g_strdup_printf("%s %s", name, CAB_SUFFIX);
    }
    else
    {
        if (!zact->show_pab && group->type == dtAll)
            return;

        group_name = g_strdup(name);
    }

    g = add_group(gc, group, group_name);

    for (i=0; i<size; i++)
    {
        const char *userid = directory_group_get_member(group, i);
        contact = directory_find_contact(dir, userid);

        if (contact && ((dir->type == dtGlobal) || (dir->type == dtPersonal && zact->show_pab) || (dir->type == dtPersonal && contact->isbuddy)))
        {
            add_buddy(gc, g, contact);
        }
    }

    g_free(group_name);
}

static void process_buddies(PurpleConnection *gc, directory_t dir)
{
	//struct zon_account_data *zact = gc->proto_data;
	PurpleGroup *g = NULL;
    contact_iterator_t iter;
    contact_t contact;

    g = add_group(gc, NULL, BUDDY_GROUP);

    iter = directory_contact_iterate(dir);
    for (contact = directory_contact_next(dir, iter); contact != NULL; contact = directory_contact_next(dir, iter))
    {
        add_buddy(gc, g, contact);
    }
}

static void process_contacts(PurpleConnection *gc, directory_t dir)
{
	//struct zon_account_data *zact = gc->proto_data;
    group_iterator_t iter;
    group_t group;

    iter = directory_group_iterate(dir);
    for (group = directory_group_next(dir, iter); group != NULL; group = directory_group_next(dir, iter))
    {
        add_group_members(gc, dir, group, group->name);
    }
}

/**
 ** Handle requests from Zon meeting client.
 **/

static void show_im_window(PurpleConnection *gc, const char *screen)
{
	struct zon_account_data *zact = gc->proto_data;
    PurpleConversation *conv = purple_find_conversation_with_account(
        PURPLE_CONV_TYPE_IM, screen, zact->account);
    if (conv == NULL)
        conv = purple_conversation_new(PURPLE_CONV_TYPE_IM, zact->account, screen);
    purple_conversation_present(conv);
}

static void process_message(PurpleConnection *gc, datablock_t blk)
{
	//struct zon_account_data *zact = gc->proto_data;
	const char *cmd = datablock_get_string(blk);
	if (cmd)
	{
        purple_debug_info("zon", "command %s received\n", cmd);
	    if (strcmp(cmd, "/query") == 0)
	    {
            GSList *list, *ptr;
            ptr = list = find_online_buddies(gc);
            while (ptr != NULL)
            {
                PurpleBuddy *buddy = ptr->data;
                const char *presence, *status;

                get_buddy_presence(gc, buddy->name, &presence, &status);
                send_presence_message(gc, buddy->name, presence, status);

                ptr = g_slist_next(ptr);
            }
            g_slist_free(list);
	    }
	    else if (strcmp(cmd, "/invite") == 0)
	    {
            const char * mid = datablock_get_string(blk);
            const char * title = datablock_get_string(blk);
            const char * descrip = datablock_get_string(blk);
            const char * message = datablock_get_string(blk);
            const char * pid = datablock_get_string(blk);
            const char * name = datablock_get_string(blk);
            const char * system = datablock_get_string(blk);
            const char * screen = datablock_get_string(blk);

	        send_im_invite(gc, mid, title, descrip, message, pid, name, system, screen);
	    }
	    else if (strcmp(cmd, "/im") == 0)
	    {
            const char * screen = datablock_get_string(blk);
        
            show_im_window(gc, screen);
	    }
	}
}

static void zon_connect(PurpleConnection *gc)
{
	struct zon_account_data *zact = gc->proto_data;
	const char *trust_ca;

    if (zact->session)
        session_destroy(zact->session);
    if (zact->book)
        addressbk_destroy(zact->book);

	trust_ca = purple_account_get_string(zact->account, "cacert", "");

    // No certificate = no ssl
	if (trust_ca == NULL || *trust_ca == '\0')
        zact->secure = FALSE;

    zact->session = session_create(zact->servername, zact->username, zact->password, "iic", zact->secure, trust_ca, FALSE, NULL);
    session_set_connected_handler(zact->session, gc, on_connected_event);
    session_set_im_handler(zact->session, gc, on_im_event);
    session_set_presence_handler(zact->session, gc, on_presence_event);
    session_set_error_handler(zact->session, gc, on_error_event);
    session_set_rpc_request_handler(zact->session, gc, "im.invite", on_im_invite);
    session_set_option(zact->session, "http", zact->httponly ? "yes" : "no");
    session_connect(zact->session, FALSE);    

    zact->book = addressbk_create(zact->session);
    addressbk_set_auth_handler(zact->book, gc, on_auth_event);
    addressbk_set_directory_fetched_handler(zact->book, gc, on_directory_event);
    addressbk_set_updated_handler(zact->book, gc, on_updated_event);
    addressbk_set_deleted_handler(zact->book, gc, on_deleted_event);
    addressbk_set_update_failed_handler(zact->book, gc, on_update_failed_event);
}

/* A periodic timer event to process events from our background thread.
   Is there a better way to synchronize between the main UI thread and our thread?  We write to a local network
   connection as a signal, but I'm not sure that's any better then this solution.
*/
static gboolean on_pulse(PurpleConnection *gc)
{
	struct zon_account_data *zact = gc->proto_data;
	struct zon_event * event;
	const char *msg;
	datablock_t blk;

	event = g_async_queue_try_pop(zact->event_queue);
    while (event)
    {
        switch (event->event)
        {
            case ZonConnected:
                purple_connection_update_progress(gc, _("Authenticated"), 2, 3);
                break;

            case ZonError:
                purple_debug_info("zon", "connection error %d\n", event->error.status);

                if (event->error.status == ERR_SSLTLS_HANDSHAKE && zact->secure)
                {
                    zact->secure = FALSE;
                    zon_connect(gc);
                    break;
                }
                else if (event->error.fatal)
                {
                    switch (event->error.status)
                    {
                        case ERR_AUTHFAILED:
                            msg = _("Authorization failed");
                            break;
                        default:
                            msg = _("Connection error");
                            break;
                    }
                    purple_connection_error(gc, msg);
                }
                else
                {
                    purple_notify_error(gc, _("Update error"), _("Couldn't complete server update"), event->error.message);
                }
                g_free((char *) event->error.message);
                break;

            case ZonAuthenticated:
                purple_connection_set_state(gc, PURPLE_CONNECTED);
                break;

            case ZonBuddies:
                process_buddies(gc, event->directory);
                break;

            case ZonContacts:
                process_contacts(gc, event->directory);
                break;

            case ZonPresence:
                process_presence(gc, event->presence);
                break;

            case ZonIM:
                process_im(gc, event->im);
                break;

            case ZonInvite:
                process_im_invite(gc, &event->invite);
                break;
        }

        g_free(event);
        event = g_async_queue_try_pop(zact->event_queue);
    }

    if (zact->ipc_queue)
    {
        blk = queue_get(zact->ipc_queue, 0);
        while (blk)
        {
            process_message(gc, blk);

            datablock_destroy(blk);
            blk = queue_get(zact->ipc_queue, 0);
        }
    }

    return TRUE;
}

static void zon_login(PurpleAccount *account)
{
	PurpleConnection *gc;
	struct zon_account_data *zact;
	gchar **userserver;

	const char *username = purple_account_get_username(account);
	gc = purple_account_get_connection(account);

	if (strpbrk(username, " \t\v\r\n") != NULL) {
		gc->wants_to_die = TRUE;
		purple_connection_error(gc, _("Zon screen names may not contain whitespaces or @ symbols"));
		return;
	}

#ifndef WIN32
    // Hack:  any sound played after we create our session (I suspect b/c we create a new thread) caused
    // pidgin to hang.  If we play a sound now, everything seems to work ok.
    purple_sound_play_file("/opt/pidgin/share/sounds/pidgin/silence.wav", NULL);
#endif

	gc->proto_data = zact = g_new0(struct zon_account_data, 1);
	zact->gc = gc;
	zact->account = account;
	zact->show_pab = purple_account_get_bool(account, "showpab", FALSE);
	zact->show_cab = purple_account_get_bool(account, "showcab", FALSE);
	zact->httponly = purple_account_get_bool(account, "httponly", FALSE);
	zact->secure = TRUE; //purple_account_get_bool(account, "secure", FALSE);

    zact->event_queue = g_async_queue_new();

	userserver = g_strsplit(username, "@", 2);
	purple_connection_set_display_name(gc, userserver[0]);
	zact->username = g_strdup(userserver[0]);
	zact->servername = g_strdup(userserver[1]);
	zact->password = g_strdup(purple_connection_get_password(gc));
	g_strfreev(userserver);

	zact->buddies = g_hash_table_new((GHashFunc)zon_ht_hash, (GEqualFunc)zon_ht_equals);
	zact->groups = g_hash_table_new((GHashFunc)zon_ht_hash, (GEqualFunc)zon_ht_equals);

    zon_connect(gc);

    /* Create IPC queue for communication with local zon meeting client */
    zact->ipc_queue = queue_create("meeting.q");
    if (queue_client(zact->ipc_queue))
    {
        purple_debug_info("zon", "meeting IPC channel already in use, ignoring for this login (%s)\n", zact->username);
        queue_destroy(zact->ipc_queue);
        zact->ipc_queue = NULL;
    }

    /* Use periodic timer to synchronize data from our background thread to main thread */
    zact->pulse_timer = purple_timeout_add(100, (GSourceFunc) on_pulse, gc);

	purple_connection_update_progress(gc, _("Connecting"), 1, 3);

	gc->flags |= PURPLE_CONNECTION_HTML;
}

static void zon_close(PurpleConnection *gc)
{
	struct zon_account_data *zact = gc->proto_data;

	if(zact) {
        session_send_presence(zact->session, NULL, PRES_UNAVAILABLE, NULL);
	    if (zact->pulse_timer)
            purple_timeout_remove(zact->pulse_timer);
        if (zact->session)
            session_destroy(zact->session);
        if (zact->event_queue)
            g_async_queue_unref(zact->event_queue);
        if (zact->ipc_queue)
        {
            send_presence_message(gc, "*", "reset", NULL);
            queue_destroy(zact->ipc_queue);
        }
	}
	g_free(gc->proto_data);
	gc->proto_data = NULL;
}

/* not needed since privacy is checked for every subscribe */
static void dummy_add_deny(PurpleConnection *gc, const char *name) {
}

static void dummy_permit_deny(PurpleConnection *gc) {
}

static int zon_send_raw(PurpleConnection *gc, const char *buf, int len)
{
    return len;
}


static PurplePluginProtocolInfo prpl_info =
{
	0,
	NULL,					/* user_splits */
	NULL,					/* protocol_options */
	NO_BUDDY_ICONS,			/* icon_spec */
	zon_list_icon,		    /* list_icon */
	NULL,					/* list_emblems */
	NULL,					/* status_text */
	zon_tooltip_text,		/* tooltip_text */
	zon_status_types,	    /* away_states */
	zon_blist_node_menu,	/* blist_node_menu */
	NULL,					/* chat_info */
	NULL,					/* chat_info_defaults */
	zon_login,			    /* login */
	zon_close,			    /* close */
	zon_im_send,			/* send_im */
	NULL,					/* set_info */
	zon_typing,			    /* send_typing */
	NULL,					/* get_info */
	zon_set_status,		    /* set_status */
	NULL,					/* set_idle */
	NULL,					/* change_passwd */
	zon_add_buddy,		    /* add_buddy */
	NULL,					/* add_buddies */
	zon_remove_buddy,	    /* remove_buddy */
	NULL,					/* remove_buddies */
	dummy_add_deny,			/* add_permit */
	dummy_add_deny,			/* add_deny */
	dummy_add_deny,			/* rem_permit */
	dummy_add_deny,			/* rem_deny */
	dummy_permit_deny,		/* set_permit_deny */
	NULL,					/* join_chat */
	NULL,					/* reject_chat */
	NULL,					/* get_chat_name */
	NULL,					/* chat_invite */
	NULL,					/* chat_leave */
	NULL,					/* chat_whisper */
	NULL,					/* chat_send */
	zon_keep_alive,		    /* keepalive */
	NULL,	                /* register_user */
	NULL,					/* get_cb_info */
	NULL,					/* get_cb_away */
	NULL,					/* alias_buddy */
	zon_group_change,		/* group_buddy */
	zon_group_rename,       /* rename_group */
	NULL,					/* buddy_free */
	NULL,					/* convo_closed */
	NULL,					/* normalize */
	NULL,					/* set_buddy_icon */
	zon_group_remove,		/* remove_group */
	NULL,					/* get_cb_real_name */
	NULL,					/* set_chat_topic */
	NULL,					/* find_blist_chat */
	NULL,					/* roomlist_get_list */
	NULL,					/* roomlist_cancel */
	NULL,					/* roomlist_expand_category */
	NULL,					/* can_receive_file */
	NULL,					/* send_file */
	NULL,					/* new_xfer */
	NULL,					/* offline_message */
	NULL,					/* whiteboard_prpl_ops */
	zon_send_raw,		    /* send_raw */
	NULL,					/* roomlist_room_serialize */

	/* padding */
	NULL,
	NULL,
	NULL,
	NULL
};


static PurplePluginInfo info =
{
	PURPLE_PLUGIN_MAGIC,
	PURPLE_MAJOR_VERSION,
	PURPLE_MINOR_VERSION,
	PURPLE_PLUGIN_PROTOCOL,                 /**< type           */
	NULL,                                   /**< ui_requirement */
	0,                                      /**< flags          */
	NULL,                                   /**< dependencies   */
	PURPLE_PRIORITY_DEFAULT,                /**< priority       */

	"prpl-zon",                             /**< id             */
	"Kablink",                              /**< name           */
	VERSION,                                /**< version        */
	"Kablink Protocol Plugin",          	/**  summary        */
	"Kablink Protocol Plugin",          	/**  description    */
	"Novell",                      		/**< author         */
	// PURPLE_WEBSITE,                      /**< homepage       */ 
	"http://www.novell.com",

	NULL,                                   /**< load           */
	NULL,                                   /**< unload         */
	NULL,                                   /**< destroy        */

	NULL,                                   /**< ui_info        */
	&prpl_info,                             /**< extra_info     */
	NULL,
	zon_plugin_menu,                        /**< actions        */

	/* padding */
	NULL,
	NULL,
	NULL,
	NULL
};

static void _init_plugin(PurplePlugin *plugin)
{
	PurpleAccountUserSplit *split;
	PurpleAccountOption *option;
	char *user_path;
    char *install_path = ".";
    char *trusted_ca = "";
    const char *server_name = "";
    options_t opts;
    int status;
    gboolean def_secure = FALSE;

#ifdef WIN32
    const char regkey[] = "SOFTWARE\\Zon_Client\\";

    install_path = wpurple_read_reg_string(HKEY_LOCAL_MACHINE, regkey, "Install_Dir");
    if (!install_path || *install_path == '\0')
        install_path = ".";

    user_path = g_strdup_printf("%s\\meeting", g_get_user_config_dir());

    trusted_ca = g_strdup_printf("%s\\serverCA.pem", wpurple_install_dir());
#else
    struct stat fs;

    install_path = INSTALL_DIR "/share/meeting";
    if (stat(install_path, &fs) == ENOENT)
    {
        install_path = ".";
    }

    user_path = g_strdup_printf("%s/.meeting", g_get_home_dir());

	if (!stat("/opt/gnome/share/purple/serverCA.pem", &fs))
		trusted_ca = g_strdup("/opt/gnome/share/purple/serverCA.pem");
	else if (!stat("/usr/share/purple/serverCA.pem", &fs))
		trusted_ca = g_strdup("/usr/share/purple/serverCA.pem");
	else
		trusted_ca = g_strdup("serverCA.pem");
#endif

    session_initialize(user_path);

    opts = options_create("zon", "temp", user_path, install_path);
    status = options_read(opts);
    if (!status)
    {
        const char * secure = options_get_system(opts, "EnableSecurity", "no");
        server_name = options_get_system(opts, "Server", "");
        if (strcasecmp(secure, "yes") == 0)
            def_secure = TRUE;
    }

	split = purple_account_user_split_new(_("Server"), server_name, '@');
	prpl_info.user_splits = g_list_append(prpl_info.user_splits, split);

	option = purple_account_option_bool_new(_("Show all personal contacts"), "showpab", FALSE);
	prpl_info.protocol_options = g_list_append(prpl_info.protocol_options, option);

	option = purple_account_option_bool_new(_("Show community contacts"), "showcab", FALSE);
	prpl_info.protocol_options = g_list_append(prpl_info.protocol_options, option);

	option = purple_account_option_bool_new(_("Only use HTTP"), "httponly", FALSE);
	prpl_info.protocol_options = g_list_append(prpl_info.protocol_options, option);

#if 0
	option = purple_account_option_bool_new(_("Enable SSL"), "secure", def_secure);
	prpl_info.protocol_options = g_list_append(prpl_info.protocol_options, option);
#endif

	option = purple_account_option_string_new(_("Trusted CA certificate"), "cacert", trusted_ca);
	prpl_info.protocol_options = g_list_append(prpl_info.protocol_options, option);

#if 0
	option = purple_account_option_bool_new(_("Use proxy"), "useproxy", FALSE);
	prpl_info.protocol_options = g_list_append(prpl_info.protocol_options, option);
	option = purple_account_option_string_new(_("Proxy"), "proxy", "");
	prpl_info.protocol_options = g_list_append(prpl_info.protocol_options, option);
	option = purple_account_option_string_new(_("Auth User"), "authuser", "");
	prpl_info.protocol_options = g_list_append(prpl_info.protocol_options, option);
	option = purple_account_option_string_new(_("Auth Domain"), "authdomain", "");
	prpl_info.protocol_options = g_list_append(prpl_info.protocol_options, option);
#endif
}

PURPLE_INIT_PLUGIN(simple, _init_plugin, info);
