# norootforbuild

%if %suse_version < 1030
%define _prefix /opt/gnome
%define _sysconfdir /etc/%_prefix
%endif

Name: pidgin-icecore
Summary: Pidgin protocol plugin for ICEcore Conferencing
Group: Productivity/Networking/Instant Messenger
License: GPL
Version: 2.3.1
Release: 0
Source0: pidgin-icecore-%{version}.tar.gz
AutoReqProv: on
BuildRoot: %{_tmppath}/%{name}-%{version}-buildroot
BuildRequires: glib2-devel libapr1-devel libicecore-devel libpurple-devel
Requires: libpurple libiiclib1 libnetlib1 libiksemel3 libxmlrpc3 libjson0

%description
Provides Pidgin protocol plugin for ICEcore Conferencing

%prep

%setup

%build
autoreconf -fi
%configure
make

%install
make install DESTDIR=$RPM_BUILD_ROOT
rm $RPM_BUILD_ROOT/%{_libdir}/purple-2/*.*a
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/purple
cp certs/* $RPM_BUILD_ROOT/%{_datadir}/purple/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)
%{_libdir}/purple-2/*.so
%{_datadir}/pixmaps/pidgin/protocols
%{_datadir}/purple

%post
/sbin/ldconfig

%changelog
