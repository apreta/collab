# norootforbuild

%if %suse_version < 1030
%define _prefix /opt/gnome
%define _sysconfdir /etc/%_prefix
%endif

Name: pidgin-kablink
Summary: Pidgin protocol plugin for Kablink Conferencing
Group: Productivity/Networking/Instant Messenger
License: GPL
Version: 2.3.1
Release: 0
Source0: pidgin-kablink-%{version}.tar.gz
AutoReqProv: on
BuildRoot: %{_tmppath}/%{name}-%{version}-buildroot

BuildRequires: glib2-devel libkablink-devel libpurple-devel
Requires: libpurple libkablink

%if "%{_vendor}" == "suse"
BuildRequires: libapr1-devel gconf2-devel
Requires: libapr1 gconf2 

%if %{suse_version} < 1030
BuildRequires: gnutls-devel
%else
BuildRequires: libgnutls-devel
%endif

%endif

%if "%{_vendor}" == "redhat"
BuildRequires: apr-devel GConf2-devel
Requires: apr GConf2
%endif

Obsoletes: pidgin-icecore

%description
Provides Pidgin protocol plugin for Kablink Conferencing

%prep

%setup

%build
autoreconf -fi
%configure
make

%install
make install DESTDIR=$RPM_BUILD_ROOT
rm $RPM_BUILD_ROOT/%{_libdir}/purple-2/*.*a
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/purple
cp certs/* $RPM_BUILD_ROOT/%{_datadir}/purple/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)
%{_libdir}/purple-2/*.so
%{_datadir}/pixmaps/pidgin/protocols
%{_datadir}/purple

%post
/sbin/ldconfig

%changelog
