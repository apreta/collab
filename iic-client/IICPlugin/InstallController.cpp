// InstallController.cpp: implementation of the CInstallController class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Wininet.h"
#include <sys/types.h>
#include <sys/stat.h>

#include "InstallController.h"
#include "ClientDetector.h"
#include "DLLVersion.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CInstallController::CInstallController()
{
    m_installerFileName = NULL;
}

CInstallController::~CInstallController()
{
	if (m_installerFileName != NULL)
		delete m_installerFileName;
}

TCHAR *CInstallController::genTempFile()
{
	TCHAR *tempName = new TCHAR[_MAX_PATH];
	TCHAR tempPath[_MAX_PATH];

	DWORD result = ::GetTempPath(_MAX_PATH, tempPath);
	result = ::GetTempFileName(tempPath, _T("imi"), 0, tempName);

	// if gen fails try default
	if (result == 0)
		tempName = INSTALLER_FILENAME;

	return tempName;
}


void CInstallController::error(LPSTR functionName)
{
    TCHAR szBuf[256]; 
    DWORD dw = GetLastError(); 
 
    _stprintf(szBuf, _T("%s failed: GetLastError returned %u\n"), 
        functionName, dw); 
 
    MessageBox(NULL, szBuf, _T("Error"), MB_OK); 
}

UINT CInstallController::ThreadMain()
{
    BOOL bResult;
	DWORD dwBytesRead;
	DWORD dwTotalRead = 0;
	char cBuffer[1024];         // I'm only going to access 1K of info.

	// Read page into memory buffer.
	bResult = InternetReadFile(
				m_hURL,									// handle to URL
				(LPSTR)cBuffer,							// pointer to buffer
				(DWORD)1024,							// size of buffer
				&dwBytesRead);							// pointer to var to hold return value

	if (bResult == FALSE)
	{
		error("InternetReadFile");
		return FALSE;
	}

	int totalExpected = 1500000; // approx size (1.5 MB)
	int reportingCount = 0;      // writing 1K at time, report every 100K
	while (dwBytesRead > 0)
	{
		// write result
		dwTotalRead += dwBytesRead;
		fwrite(cBuffer, 1, dwBytesRead, m_installFile);

		// read next
		bResult = InternetReadFile(
					m_hURL,								// handle to URL
					(LPSTR)cBuffer,						// pointer to buffer
					(DWORD)1024,						// size of buffer
					&dwBytesRead);						// pointer to var to hold return value

		if (bResult == FALSE)
		{
			error("InternetReadFile");
			return FALSE;
		}

		reportingCount++;
		if (reportingCount == 100)
		{
			reportingCount = 0;
			if (m_progressListener != NULL)
				m_progressListener->SetProgress((int)(((float)dwTotalRead / (float)totalExpected) * 100.0));
		}

		if (ShouldDie())
			return TRUE;
	}

	m_progressListener->SetComplete();

	return TRUE;
}

// saves the installer from a known URL to a temp location
BOOL CInstallController::SaveInstaller(IProgressListener* listener)
{
	m_progressListener = listener;

	m_installerFileName = genTempFile();

	m_installFile = _tfopen(m_installerFileName, _T("wb"));

	// Make internet connection.
	m_hInternetSession = InternetOpen(
					  _T("Microsoft Internet Explorer"),	// agent
					  INTERNET_OPEN_TYPE_PRECONFIG,			// access
					  NULL, 
					  NULL, 
					  0);            // defaults

	if (m_hInternetSession == NULL)
	{
		error("InternetOpen");
		return FALSE;
	}

	// Make connection to desired page.
	m_hURL = InternetOpenUrl(
			 m_hInternetSession,						// session handle
			 m_installerURL,							// URL
			 NULL, 0, 0, 0);							// defaults

	if (m_hURL == NULL)
	{
		error("InternetOpenURL: installer URL");
		return FALSE;
	}

	// launch installer save into seperate thread
	StartThread();

	return TRUE;
}

// clean up open file and handles from the save
BOOL CInstallController::Cleanup()
{
	fclose(m_installFile);

	// Close down connections.
	InternetCloseHandle(m_hURL);
	InternetCloseHandle(m_hInternetSession);

	return TRUE;
}

// executes the installer which installs the IIC client
BOOL CInstallController::ExecInstaller()
{
	struct _stat buf;
	int result;

	// verify that installer is present
	result = _tstat(m_installerFileName, &buf);

	if (result == 0)
	{
		LaunchInstaller(m_installerFileName, NULL);
	}
	else
	{
		return FALSE;
	}

	return TRUE;
}

HANDLE CInstallController::LaunchInstaller(LPCTSTR program, LPCTSTR args)
{
    HANDLE hProcess = NULL;
    PROCESS_INFORMATION processInfo;
    STARTUPINFO startupInfo;
    ::ZeroMemory(&startupInfo, sizeof(startupInfo));
    startupInfo.cb = sizeof(startupInfo);
    if(::CreateProcess(program, 
					   NULL,  // command line
                       NULL,  // process security
                       NULL,  // thread security
                       FALSE, // no inheritance
                       0,     // no startup flags
                       NULL,  // no special environment
                       NULL,  // default startup directory
                       &startupInfo,
                       &processInfo))
    {
        hProcess = processInfo.hProcess;
    }

    
    // Wait until child process exits.
    WaitForSingleObject( processInfo.hProcess, INFINITE );

    // Close process and thread handles. 
    CloseHandle( processInfo.hProcess );
    CloseHandle( processInfo.hThread );

	return hProcess;
}

// removes the installer from its temp location
BOOL CInstallController::CleanInstaller()
{
	if (m_installerFileName != NULL)
		::DeleteFile(m_installerFileName);
	return TRUE;
}

// detects if the client is already installed on the target
// and if it is a recent enough version
BOOL  CInstallController::IsClientInstalled()
{
	// stat the path+file name for executable
	// if present, client is assumed to be installed
	struct _stat buf;
	int result;

	// verify that installer is present
	result = _tstat(CLIENT_PATH_STR, &buf);

	if (result == 0)
    {
        // verify that the version is current
        if (IsClientVersionCurrent())
    		return true;
    }
	return false;
}

BOOL CInstallController::IsClientVersionCurrent()
{
    // get client version file from web server
    int pos = m_installerURL.ReverseFind('/');
    CString strClientVersionFile = m_installerURL.Left(pos+1);
    strClientVersionFile += "iicversion.txt";   

    // open session
	m_hInternetSession = InternetOpen(
					  _T("Microsoft Internet Explorer"),	// agent
					  INTERNET_OPEN_TYPE_PRECONFIG,			// access
					  NULL, 
					  NULL, 
					  0);            // defaults

	if (m_hInternetSession == NULL)
	{
		error("InternetOpen");
		return FALSE;
	}

	// Make connection to desired page.
	m_hURL = InternetOpenUrl(
			 m_hInternetSession,						// session handle
			 strClientVersionFile,						// URL
			 NULL, 0, 0, 0);							// defaults

	if (m_hURL == NULL)
	{
        char buff[256];
        sprintf(buff, "InternetOpenURL: client version file: %s", strClientVersionFile);
		error(buff);
		return FALSE;
	}

    // read into buffer
	BOOL bResult;
	DWORD dwBytesRead;
	DWORD dwTotalRead = 0;
	char cBuffer[1024];         // I'm only going to access 1K of info.

	// Read page into memory buffer.
	bResult = InternetReadFile(
				m_hURL,									// handle to URL
				(LPSTR)cBuffer,							// pointer to buffer
				(DWORD)1024,							// size of buffer
				&dwBytesRead);							// pointer to var to hold return value

	if (bResult == FALSE)
	{
		error("InternetReadFile");
		return FALSE;
	}

    // close connection
	InternetCloseHandle(m_hURL);
	InternetCloseHandle(m_hInternetSession);

    // read first line of file - form is "1,0,0,2;"
    CString mostRecentVersion = cBuffer;
    pos = mostRecentVersion.Find(';');
    if (pos > 0)
        mostRecentVersion = mostRecentVersion.Left(pos);
    else
    {
        error("Invalid Web Version File");
        return FALSE;
    }

    // get version of installed client
    CDLLVersion* dllVersion = new CDLLVersion(CLIENT_PATH_STR);
    CString currentVersion = dllVersion->GetFullVersion();

    return (! ShouldUpgrade(currentVersion, mostRecentVersion));
}


struct version_struct
{
    int major;
    int minor;
    int build;
    int platform;
};

BOOL parse_version(CString& version, version_struct& version_result)
{
    TCHAR *seps = _T(",");
    TCHAR *token;
    BOOL result = TRUE;

    TCHAR version_s[32];

    _tcscpy(version_s, (LPCTSTR) version);

    token = _tcstok(version_s, seps);
    if (token != NULL)
        version_result.major = _ttoi(token);
    else
    {
        result = FALSE;
        goto cleanup;
    }

    // minor
    token = _tcstok(NULL, seps);
    if (token != NULL)
        version_result.minor = _ttoi(token);
    else
    {
        result = FALSE;
        goto cleanup;
    }

    // build
    token = _tcstok(NULL, seps);
    if (token != NULL)
        version_result.build = _ttoi(token);
    else
    {
        result = FALSE;
        goto cleanup;
    }

    // platform
    token = _tcstok(NULL, seps);
    if (token != NULL)
        version_result.platform = _ttoi(token);
    else
    {
        result = FALSE;
        goto cleanup;
    }

cleanup:
    return result;
}

// returns TRUE if should upgrade
BOOL CInstallController::ShouldUpgrade(CString& currentVersion, CString& mostRecentVersion)
{
    BOOL doUpgrade = FALSE;

    // compare server version (mostRecentVersion) number to current client version
    version_struct currentVersionStruct;
    version_struct mostRecentVersionStruct;

    BOOL result = parse_version(currentVersion, currentVersionStruct);

    if (result == TRUE) // keep going - otherwise parse error
        result = parse_version(mostRecentVersion, mostRecentVersionStruct);

    if (result == TRUE) // keep going - otherwise parse error
    {
        if (mostRecentVersionStruct.major > currentVersionStruct.major ||
            mostRecentVersionStruct.minor > currentVersionStruct.minor ||
            mostRecentVersionStruct.build > currentVersionStruct.build ||
            mostRecentVersionStruct.platform > currentVersionStruct.platform)
        {
            doUpgrade = TRUE;
        }
    }

    return doUpgrade;
}

