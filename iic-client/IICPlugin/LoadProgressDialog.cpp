// LoadProgressDialog.cpp : implementation file
//

#include "stdafx.h"
#include "IICPlugin.h"
#include "LoadProgressDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLoadProgressDialog dialog


CLoadProgressDialog::CLoadProgressDialog(CInstallController* controller,
										 CString installURL,
										 CWnd* pParent /* NULL */)
	: CDialog(CLoadProgressDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLoadProgressDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON_DIALOG);

	m_installURL = installURL;
	m_installController = controller;
}


void CLoadProgressDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLoadProgressDialog)
	DDX_Control(pDX, IDC_LOAD_PROGRESS, m_progress);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLoadProgressDialog, CDialog)
	//{{AFX_MSG_MAP(CLoadProgressDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLoadProgressDialog message handlers

BOOL CLoadProgressDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	
	m_wasCancel = FALSE;
	BOOL result = m_installController->SaveInstaller(this);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

bool CLoadProgressDialog::SetProgress(int percent)
{
	m_progress.SetPos(percent);
	return true;
}

void CLoadProgressDialog::SetComplete()
{
	m_progress.SetPos(100);
	CDialog::OnCancel();
	m_wasCancel = FALSE;
}

void CLoadProgressDialog::OnCancel() 
{
	// TODO: Add extra cleanup here
	m_wasCancel = TRUE;
	m_installController->StopThread();
	
	CDialog::OnCancel();
}
