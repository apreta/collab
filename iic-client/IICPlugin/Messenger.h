// Messenger.h: interface for the CMessenger class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MESSENGER_H__FB6A13F5_FC44_4600_A9DA_AEF9D889E14A__INCLUDED_)
#define AFX_MESSENGER_H__FB6A13F5_FC44_4600_A9DA_AEF9D889E14A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "MeetingMessages.h"

///////////////////////////////////////////////////////////////////////
// provides communication to the IIC client through Windows messaging
///////////////////////////////////////////////////////////////////////
class CMessenger  
{
public:
	CMessenger(HWND clientWindow);
	virtual ~CMessenger();
	LRESULT SendParameters(meetingInfoMessage& message);
private:
	HWND m_clientWindow;
};

#endif // !defined(AFX_MESSENGER_H__FB6A13F5_FC44_4600_A9DA_AEF9D889E14A__INCLUDED_)
