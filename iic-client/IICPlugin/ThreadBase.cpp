#include "stdafx.h"
#include "ThreadBase.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

CThreadBase::CThreadBase() : m_hThread(NULL),
						   m_hStopEvent(NULL),
					       m_hMainWnd(NULL),
						   m_dwThreadID(0)
{
}

CThreadBase::~CThreadBase()
{
	StopThread();
}

BOOL CThreadBase::StartThread(HWND hWnd)
{
	// if thread already running do not start another one
	if(GetThreadStatus())
		return FALSE;

	// create stop event
	m_hStopEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL);
	if(m_hStopEvent == NULL)
		return FALSE;

	// create thread
	DWORD dwThreadId;
	m_hThread = ::CreateThread(NULL, 0, ThreadMain, this, 0, &dwThreadId);
	if(m_hThread == NULL)
		return FALSE;

	m_dwThreadID = dwThreadId;

	return TRUE;
}

BOOL CThreadBase::StopThread()
{
	// signal stop event and close all threads
	if(::SetEvent(m_hStopEvent))
	{
		// if the Thread Main function is not overridden correctly
		// this will freeze the application
		::WaitForSingleObject(m_hThread, INFINITE);
		::CloseHandle(m_hThread);
		::CloseHandle(m_hStopEvent);
		return TRUE;
	}

	return FALSE;
}

BOOL CThreadBase::GetThreadStatus()
{
	// check is the thread still active
	DWORD dwExitCode = 0;
	GetExitCodeThread(m_hThread, &dwExitCode);
	return(dwExitCode == STILL_ACTIVE);
}
