//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by IICPlugin.rc
//
#define IDS_IICPLUGIN                   1
#define IDB_IICPLUGIN                   1
#define IDS_IICPLUGIN_PPG               2
#define IDS_IICPLUGIN_PPG_CAPTION       200
#define IDD_PROPPAGE_IICPLUGIN          200
#define IDD_DIALOG_LOAD_PROGRESS        203
#define IDC_LOAD_PROGRESS               203
#define IDI_ICON_DIALOG                 246

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        205
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         205
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
