// IICPlugin.cpp : Implementation of CIICPluginApp and DLL registration.

#include "stdafx.h"
#include "IICPlugin.h"
#include "IICPluginCtl.h"

#include "cathelp.h"

#ifdef L_IMPL_OBJECTSAFETY
#include <objsafe.h>
#endif // L_IMPL_OBJECTSAFETY 

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


CIICPluginApp NEAR theApp;

const GUID CDECL BASED_CODE _tlid =
		{ 0xd9c3dd1d, 0x7683, 0x4041, { 0xb3, 0xfd, 0xb2, 0xdf, 0x60, 0xf, 0x30, 0xda } };
const WORD _wVerMajor = 1;
const WORD _wVerMinor = 0;


////////////////////////////////////////////////////////////////////////////
// CIICPluginApp::InitInstance - DLL initialization

BOOL CIICPluginApp::InitInstance()
{
	BOOL bInit = COleControlModule::InitInstance();

	if (bInit)
	{
		// TODO: Add your own module initialization code here.
	}

	return bInit;
}


////////////////////////////////////////////////////////////////////////////
// CIICPluginApp::ExitInstance - DLL termination

int CIICPluginApp::ExitInstance()
{
	// TODO: Add your own module termination code here.

	return COleControlModule::ExitInstance();
}


/////////////////////////////////////////////////////////////////////////////
// DllRegisterServer - Adds entries to the system registry

STDAPI DllRegisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleRegisterTypeLib(AfxGetInstanceHandle(), _tlid))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(TRUE))
		return ResultFromScode(SELFREG_E_CLASS);

#ifdef L_USE_COMCAT
	// mark control safe for initialization
	if (FAILED( CreateComponentCategory(CATID_SafeForInitializing,
										L"Controls safely initializable from persistent data") ))
		return ResultFromScode(SELFREG_E_CLASS);

	if (FAILED( RegisterCLSIDInCategory(CIICPluginCtrl::guid, CATID_SafeForInitializing) ))
		return ResultFromScode(SELFREG_E_CLASS);

	// mark control safe for scripting
	if (FAILED( CreateComponentCategory(CATID_SafeForScripting,
										L"Controls that are safely scriptable") ))
		return ResultFromScode(SELFREG_E_CLASS);

	if (FAILED( RegisterCLSIDInCategory(CIICPluginCtrl::guid, CATID_SafeForScripting) ))
		return ResultFromScode(SELFREG_E_CLASS);
#endif

	return NOERROR;
}


/////////////////////////////////////////////////////////////////////////////
// DllUnregisterServer - Removes entries from the system registry

STDAPI DllUnregisterServer(void)
{
	AFX_MANAGE_STATE(_afxModuleAddrThis);

	if (!AfxOleUnregisterTypeLib(_tlid, _wVerMajor, _wVerMinor))
		return ResultFromScode(SELFREG_E_TYPELIB);

	if (!COleObjectFactoryEx::UpdateRegistryAll(FALSE))
		return ResultFromScode(SELFREG_E_CLASS);

#ifdef L_USE_COMCAT
	// This removes the Implemented Categories from the control's registration.
	// Only need to unregister them if registered above.
	// WARNING: Unregister the control before removing the L_USE_COMCAT definition.
	HRESULT hr = NOERROR;
	hr = UnRegisterCLSIDInCategory(CIICPluginCtrl::guid, CATID_SafeForScripting);
	hr = UnRegisterCLSIDInCategory(CIICPluginCtrl::guid, CATID_SafeForInitializing);
#endif // L_USE_COMCAT

	return NOERROR;
}
