// ClientDetector.h: interface for the CClientDetector class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CLIENTDETECTOR_H__3E886D4C_B16E_4335_9BCB_2543FB48EC7B__INCLUDED_)
#define AFX_CLIENTDETECTOR_H__3E886D4C_B16E_4335_9BCB_2543FB48EC7B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define CLIENT_PATH_STR _T("c:/Program Files/Zon/meeting.exe")

//////////////////////////////////////////////////////////////////////
// Detects the presence of the IIC client on the target
//////////////////////////////////////////////////////////////////////
class CClientDetector  
{
public:
	CClientDetector();
	virtual ~CClientDetector();

	// returns handle to the window if it is running
	HWND FindRunningClient(CString screenName);

	HWND LaunchClient(const CString& meetingID, 
                      const CString& participantID, 
                      const CString& screenName, 
                      const CString& server,
        		      const CString& phone,
					  const CString& email,
					  const CString& fullname
                      );

    // launches the client, returning a handle to the window
	HWND LaunchClient(const CString& commandLine);
};

#endif // !defined(AFX_CLIENTDETECTOR_H__3E886D4C_B16E_4335_9BCB_2543FB48EC7B__INCLUDED_)
