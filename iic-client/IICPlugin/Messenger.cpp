// Messenger.cpp: implementation of the CMessenger class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Messenger.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMessenger::CMessenger(HWND clientWindow)
{
	// client window must be valid
	_ASSERTE(clientWindow != NULL);

	m_clientWindow = clientWindow;
}

CMessenger::~CMessenger()
{

}

// sends a meeting message to the IIC client
LRESULT CMessenger::SendParameters(meetingInfoMessage& message)
{
	LRESULT copyDataResult;

	if (m_clientWindow)
	{
		COPYDATASTRUCT cpd;
		cpd.dwData = MEETING_MSG;
		cpd.cbData = sizeof(message);
		cpd.lpData = &message;

		copyDataResult = ::SendMessage(m_clientWindow,
									   WM_COPYDATA,
									   (WPARAM)m_clientWindow,
                                       (LPARAM)&cpd);
	} 

	return copyDataResult;
}

