// CSV.h

#ifndef CSVh
#define CSVh

#if _MSC_VER > 1000
#pragma once
#pragma pack(push)
#pragma pack(1)
#endif // _MSC_VER > 1000

/* Usage:
This class was designed to read Comma Separated Value (.CSV) files but has been extended to read other similar formats as well.
CSV Files are text files using one line to represent one Record (Record Separators are \r\n characters).
Fields are separated by a particular character (comma , by default).
eg:
one,2,three,four,5.0

If a Field contains a Field Separator character it should be enclosed in Text Separators (Speech Marks " by default).
eg:
one,2,three,"four, or 4",5.0

If a Field contains a Text Separator, that Text Separator should be doubled up, and the Field should be enclosed in Text Separators.
eg:
the fourth field of:
one,2,three,"4'5"" tall",5.0
would be read as:
4'5" tall

Leading and trailing white space characters are removed.

The class also handles e-mail recipient lists
where a field may contain a section in Text Separators followed by a section which will never contain Text or Field separators.
eg:
"Smith, John" <JS@Work.com>, "Jones David"   <DJ@Work.com>, Me@Home.com
would be read as the following three records:
"Smith, John" <JS@Work.com>
"Jones David" <DJ@Work.com>
Me@Home.com

Note that the white-space characters between the two sections are replaced with a single space character.

Examples:

#include "CSV.h"

  CString S;
  CCSV MT(&S,"");
  ASSERT(MT.GetFieldCount()==0);
  ASSERT(!MT.GetNextField());
  ASSERT(S=="");
  ASSERT(MT.GetFieldAt(0)=="");
  ASSERT(MT.FindField("")==-1);
  ASSERT(MT.FindField("none")==-1);

  CCSV CSV(&S," one,\"two\",\"thr,ee\" ,\"fo\"\"ur\", \"Smith, John\" <JS@Work.com>");
  ASSERT(CSV.GetFieldCount()==5);
  ASSERT(CSV.FindField("none")==-1);
  ASSERT(CSV.GetNextField());
  ASSERT(S=="one");
  ASSERT(CSV.GetFieldAt(0)=="one");
  ASSERT(CSV.FindField("one")==0);
  ASSERT(CSV.GetFieldCount()==5);
  ASSERT(CSV.GetNextField());
  ASSERT(S=="two");
  ASSERT(CSV.GetFieldAt(1)=="two");
  ASSERT(CSV.FindField("two")==1);
  ASSERT(CSV.GetFieldCount()==5);
  ASSERT(CSV.GetNextField());
  ASSERT(S=="thr,ee");
  ASSERT(CSV.GetFieldAt(2)=="thr,ee");
  ASSERT(CSV.FindField("thr,ee")==2);
  ASSERT(CSV.GetFieldCount()==5);
  ASSERT(CSV.GetNextField());
  ASSERT(S=="fo\"ur");
  ASSERT(CSV.GetFieldAt(3)=="fo\"ur");
  ASSERT(CSV.FindField("fo\"ur")==3);
  ASSERT(CSV.GetFieldCount()==5);
  ASSERT(CSV.GetNextField());
  ASSERT(S=="\"Smith, John\" <JS@Work.com>");
  ASSERT(CSV.GetFieldAt(4)=="\"Smith, John\" <JS@Work.com>");
  ASSERT(CSV.FindField("\"Smith, John\" <JS@Work.com>")==4);
  ASSERT(CSV.GetFieldCount()==5);
  ASSERT(!CSV.GetNextField());
  ASSERT(S=="");
  ASSERT(CSV.GetFieldAt(5)=="");
  ASSERT(CSV.GetFieldCount()==5);

To read from a file:

  CFile File;
  if(!File.Open("Test.csv", CFile::modeRead|CFile::shareDenyNone)) return;
  CArchive Ar(&File, CArchive::load);
  CString S;
  while(Ar.ReadString(S)) { //For all lines of the file
    CCSV CSV;
    CString Field;
    CSV.Set(&Field,S);
    while(CSV.GetNextField()) { //for all Fields of the Record
      //The current Field is now stored in CString Field
  } }

To read command line arguments from GetCommandLine()
(you should really use __argc and __argv[]):
  CString S;
  CString First ;
  CString Second;
  CString Third ;
  CCSV CSV(&S, GetCommandLine(), ' ');
  CSV.GetNextField(); // Application Path
  if(!CSV.GetNextField()) return true; //No Parameters
  CString argv(S);
  if(CSV.GetNextField(First ) // First  Parameter
  && CSV.GetNextField(Second) // Second Parameter
  && CSV.GetNextField(Third ) // Third  Parameter
  && !CSV.GetNextField()) {
    ... //got the parameters
  }

To create a folder from a Path:

  CString CreateBranch(CString Path) { //returns the path that was successfully created.
    CCSV CSV(0, Path, '\\');
    if(Path.Left(2)=="\\\\") { // \\ComputerName\Share\Directory format
      CSV.GetNextField(); //Skip the first two "empty fields"
      CSV.GetNextField();
      CSV.GetNextField(); //Win9x needs the ComputerName and Share to be used together, so we have to skip the ComputerName too.
    }
    while(CSV.GetNextField()
    && ((GetFileAttributes(CSV.GetDone())!=-1)
    || ::CreateDirectory(CSV.GetDone(),0)));
  return CSV.GetDone(); //could be return CSV.GetEnd().IsEmpty() if you wanted the function to return a bool...
*/

class CCSV {
  CString      Rec; //Our copy of the Record
  const TCHAR *src; //Character iterator pointing within the Record
  CString*     dst; //Destination CString
  TCHAR        FS; //Field Separator
  TCHAR        TS; //Text  Separator
public:
  CCSV() : src(0) {}
  CCSV              (CString* out, const TCHAR *in, TCHAR  FS=_T(','), TCHAR  TS=_T('"')) {Set(out,in,FS,TS);}
  void Set          (CString* out, const TCHAR *in, TCHAR _FS=_T(','), TCHAR _TS=_T('"')) {dst=out; src=Rec=in; FS=_FS; TS=_TS;}
  bool GetFirstField(CString* out, const TCHAR *in, TCHAR  FS=_T(','), TCHAR  TS=_T('"')) {Set(out,in,FS,TS); return GetNextField();}
  bool GetFirstField(CString* out) {dst=out; return GetFirstField();}
  bool GetFirstField()             {src=Rec; return GetNextField();}
  bool GetNextField(CString* out=0) {
    if(!src) return false;
    bool Quoted=false;
    while( TCHAR c=*src++) {
      if(c==_T(' ') || c==_T('\t')) continue; //Ignore leading white-space characters.
      CString Text;
      if(c==TS) { //Quoted String:
        Quoted=true;
        while(c=*src++) {
          if(c!=TS) Text+=c;
          else { //Trailing "
            if((c=*src++)==TS) Text+=TS; //We have "" so insert a single " into the string
            else {
              while((c==_T(' ') || c==_T('\t')) && (c=*src++)); //Ignore trailing white-space characters.
              break; //We've got the quoted section, now try to get the rest:
      } } } }
      if(c && (c!=FS)) {
        if(Quoted) Text=_T('"')+Text+_T("\" "); //Parse e-mail Recipient lists like: "Jones, David" <DJ@Home.com>, JS@Work.com
        do Text+=c; //This is the normal Field copying loop:
        while((c=*src++) && (c!=FS));
      }
      if(!c) --src; //Point back at the NULL Terminator
      if(out) *out=Text;
      if(dst) *dst=Text;
      return true;
    }
    --src; //Point back at the NULL Terminator for GetDone and GetEnd.
    if(out) out->Empty();
    if(dst) dst->Empty();
    return false;
  }
  bool    IsLast () const {return !*src;}
  CString GetDone()       {return CString(Rec).Left(src-Rec);} //Returns the parsed section of the Record.
  CString GetEnd ()       {return CString(Rec).Mid (src-Rec);} //Returns the section of the Record that is yet to be parsed.

//CtmpString helps shorten the following functions by storing the current state and providing a temporary CString:
  class CtmpString : public CString { //Store the current position:
    const TCHAR*& src; //references to the src and dst pointers:
    CString*&    dst;
    const TCHAR *tmpsrc; //the stored src and dst values:
    CString*    tmpdst;
  public:
    CtmpString(const TCHAR*& src, CString*& dst) : src(src), tmpsrc(src), dst(dst), tmpdst(dst) {}
    virtual ~CtmpString() {src=tmpsrc; dst=tmpdst;}
  };

  unsigned int GetFieldCount() {
    unsigned i;
    CtmpString S(src, dst);
    for(i=GetFirstField(&S); GetNextField(); ++i);
    return i;
  }
  CString GetFieldAt(unsigned int Pos)  {
    CtmpString S(src, dst);
    GetFirstField(&S);
    while(Pos-- && GetNextField());
    return S;
  }
  int FindField(CString Field) {
    int i;
    CtmpString S(src, dst);
    for(i=GetFirstField(&S)-1; S!=Field; ++i) if(!GetNextField()) return -1;
    return i;
  }
};

#if _MSC_VER > 1000
#pragma pack(pop)
#endif // _MSC_VER > 1000

#endif //ndef CSVh


