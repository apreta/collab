// lets assume we are running MS Compile Version 5 and up
#pragma once

//! Wrapper for win32 threading
class CThreadBase
{
	//! main thread function
	/*!
		This function is used only to call the virtual function (which must be overridden).
		In this way all non-static members can be used from within the thread.
	*/
	static DWORD WINAPI ThreadMain(LPVOID lParam)
	{
		CThreadBase *pThis = static_cast<CThreadBase*>(lParam);
		if(pThis != NULL)
			return pThis->ThreadMain();
		else return -1;
	}

protected:
	//! thread id
	DWORD m_dwThreadID;

	//! main window handle
	HWND m_hMainWnd;

	//! thread handle
	HANDLE m_hThread;

	//! handle of the stop event
	HANDLE m_hStopEvent;

	//! Checks if the thread should abort the action
	BOOL ShouldDie() { return(::WaitForSingleObject(m_hStopEvent, 0) == WAIT_OBJECT_0); };

	//! virtual main thread function
	/*!
		This function must be overridden when new class is derived from this one.
	*/
	virtual UINT ThreadMain() = 0;

public:
	//! Standard constructor
	CThreadBase();
	//! Standard destructor
	virtual ~CThreadBase();

	//! thread startup function
	/*!
		Use this function to start the thread execution.
	*/
	BOOL StartThread(HWND hWnd = NULL);

	//! thread termination function
	/*!
		Use this function to stop the thread execution. Note that the internal event 
		is used to stop the thread. If the event status is not checked during the 
		thread execution, the applicaiton might freeze.
	*/
	BOOL StopThread();

	//! returns thread running status
	/*!
		Use this function to get the thread status. It returns true if the thread
		is running and false if not.
	*/
	BOOL GetThreadStatus();
};
