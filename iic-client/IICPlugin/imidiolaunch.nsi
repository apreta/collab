;; imidiolaunch.nsi
;;
;; Copyright (c) 2005 SiteScape, Inc.
;; 
;; This script installs Imidio's client into c:/Program Files/Imidio.  
;; It remembers the directory, has uninstall support and installs start menu shortcuts.
;;
;; This script runs silently and automatically launches IIC after install. It presents
;; confirmation dialogs if IIC needs to be killed prior to install (or uninstall).
;;
;;

!include "WinMessages.nsh"
!include "UpgradeDLL.nsh"  ;; include this when we need to worry about different dll versions
                           ;; requires nsis2.0rc2 or greater
!include "include.nsi"

; The name of the installer
Name "${product_name}"

; The file to write
OutFile "imidiolaunch.exe"

; set compressor to best compression
SetCompressor lzma

; defines files that are included in installer, registry settings
; must be included after SetCompressor is called
!include "common.nsi"

; The default installation directory
InstallDir $PROGRAMFILES\Imidio

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM SOFTWARE\Imidio_Client "Install_Dir"

; Select desired styles for our dialog
XPStyle on
WindowIcon off
ShowInstDetails nevershow
SpaceTexts none

; automatically close window after install (no Close button)
AutoCloseWindow true

; text in window
BrandingText "${product_name} Installer"

;; NOTE: Install and Uninstall Sections are defined in "common.nsi"
;; Unique sections for this installer should be defined below.

; Automatically launch IIC after install
Function .onInstSuccess
    Call LaunchClient
FunctionEnd
