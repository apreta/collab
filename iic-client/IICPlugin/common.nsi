;;
;; common.nsi
;;
;; Copyright (c) 2005 SiteScape, Inc.
;; 
;; Utilities that define the files copied on install, registry key settings, etc.
;; 
;;

;;--------------------------------
;; Installer Section
;;--------------------------------
Section "Client (required)"
  SectionIn RO
  
  ; Set output path to the installation directory.
  SetOutPath $INSTDIR

  ; Copy files
  Call CopyInstallFiles  

  ; Write Registry Keys
  StrCmp $R9 "LIMITED" SKIP_REGISTRY
      Call WriteRegistryKeys
  SKIP_REGISTRY:

  ; Write out uninstaller
  WriteUninstaller "uninstall.exe"
SectionEnd

;;--------------------------------
;; Set install directory and shortcuts
;; Note: this is called automatically
;;--------------------------------
Section "Start Menu Shortcuts (Required)"
  SectionIn RO
  StrCmp $RunOnce "TRUE" SKIPSHORTCUT
      CreateDirectory "$SMPROGRAMS\Imidio"
      CreateShortCut "$SMPROGRAMS\Imidio\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
      CreateShortCut "$SMPROGRAMS\Imidio\Imidio (IIC).lnk" "$INSTDIR\iic.exe" "" "$INSTDIR\iic.exe" 0
  SKIPSHORTCUT:
SectionEnd

;;--------------------------------
;; Uninstaller Section
;;--------------------------------
UninstallText "This will uninstall ${product_name}. Hit Uninstall to continue."

Section "Uninstall"
  StrCmp $R9 "LIMITED" SKIP_UNREGISTRY
      Call un.DeleteRegistryKeys
  SKIP_UNREGISTRY:

   Call un.DeleteInstallFiles
SectionEnd

;;---------------------------------
; GetWindowsVersion
;;---------------------------------
; Based on Yazno's function, http://yazno.tripod.com/powerpimpit/
; Updated by Joost Verburg
; Only return 'NT' for 'NT x.x' by Bin Liu
;
; Returns on top of stack
;
; Windows Version (95, 98, ME, NT x.x, 2000, XP, 2003)
; or
; '' (Unknown Windows Version)
;
; Usage:
;   Call GetWindowsVersion
;   Pop $R0
;   ; at this point $R0 is "NT 4.0" or whatnot

Function GetWindowsVersion
 
  Push $R0
  Push $R1
 
  ClearErrors
 
  ReadRegStr $R0 HKLM \
  "SOFTWARE\Microsoft\Windows NT\CurrentVersion" CurrentVersion

  IfErrors 0 lbl_winnt
   
  ; we are not NT
  ReadRegStr $R0 HKLM \
  "SOFTWARE\Microsoft\Windows\CurrentVersion" VersionNumber
 
  StrCpy $R1 $R0 1
  StrCmp $R1 '4' 0 lbl_error
 
  StrCpy $R1 $R0 3
 
  StrCmp $R1 '4.0' lbl_win32_95
  StrCmp $R1 '4.9' lbl_win32_ME lbl_win32_98
 
lbl_win32_95:
    StrCpy $R0 '95'
  Goto lbl_done
 
lbl_win32_98:
    StrCpy $R0 '98'
  Goto lbl_done
 
lbl_win32_ME:
    StrCpy $R0 'ME'
  Goto lbl_done
 
lbl_winnt:
 
  StrCpy $R1 $R0 1
 
  StrCmp $R1 '3' lbl_winnt_x
  StrCmp $R1 '4' lbl_winnt_x
 
  StrCpy $R1 $R0 3
 
  StrCmp $R1 '5.0' lbl_winnt_2000
  StrCmp $R1 '5.1' lbl_winnt_XP
  StrCmp $R1 '5.2' lbl_winnt_2003 lbl_error
 
lbl_winnt_x:
    StrCpy $R0 "NT"
  Goto lbl_done
 
lbl_winnt_2000:
    Strcpy $R0 '2000'
  Goto lbl_done
 
lbl_winnt_XP:
    Strcpy $R0 'XP'
  Goto lbl_done
 
lbl_winnt_2003:
    Strcpy $R0 '2003'
  Goto lbl_done
 
lbl_error:
    Strcpy $R0 ''
  lbl_done:
 
  Pop $R1
  Exch $R0
 
FunctionEnd

Function un.GetWindowsVersion
 
  Push $R0
  Push $R1
 
  ClearErrors
 
  ReadRegStr $R0 HKLM \
  "SOFTWARE\Microsoft\Windows NT\CurrentVersion" CurrentVersion

  IfErrors 0 lbl_winnt
   
  ; we are not NT
  ReadRegStr $R0 HKLM \
  "SOFTWARE\Microsoft\Windows\CurrentVersion" VersionNumber
 
  StrCpy $R1 $R0 1
  StrCmp $R1 '4' 0 lbl_error
 
  StrCpy $R1 $R0 3
 
  StrCmp $R1 '4.0' lbl_win32_95
  StrCmp $R1 '4.9' lbl_win32_ME lbl_win32_98
 
lbl_win32_95:
    StrCpy $R0 '95'
  Goto lbl_done
 
lbl_win32_98:
    StrCpy $R0 '98'
  Goto lbl_done
 
lbl_win32_ME:
    StrCpy $R0 'ME'
  Goto lbl_done
 
lbl_winnt:
 
  StrCpy $R1 $R0 1
 
  StrCmp $R1 '3' lbl_winnt_x
  StrCmp $R1 '4' lbl_winnt_x
 
  StrCpy $R1 $R0 3
 
  StrCmp $R1 '5.0' lbl_winnt_2000
  StrCmp $R1 '5.1' lbl_winnt_XP
  StrCmp $R1 '5.2' lbl_winnt_2003 lbl_error
 
lbl_winnt_x:
    StrCpy $R0 "NT"
  Goto lbl_done
 
lbl_winnt_2000:
    Strcpy $R0 '2000'
  Goto lbl_done
 
lbl_winnt_XP:
    Strcpy $R0 'XP'
  Goto lbl_done
 
lbl_winnt_2003:
    Strcpy $R0 '2003'
  Goto lbl_done
 
lbl_error:
    Strcpy $R0 ''
  lbl_done:
 
  Pop $R1
  Exch $R0
 
FunctionEnd

;;--------------------------------
;; Copies files to install directory
;;--------------------------------
Function CopyInstallFiles

;; simply copy exe's
  File "..\IIC\Release\iic.exe"
  File "..\IIC\Release\iicviewer.exe"
  File "..\IIC\Release\iicshare.exe"

;; these dll's do not hook and can be copied
  File "..\IIC\Release\iicskin.dll"
  File "..\IIC\Release\iicoscar.dll"
  File "..\IIC\Release\IICPrintDrvExt.dll"

;; these hook so need to be upgraded
;; the macro will update the dll based on version num and will do things like
;; force reboot if unable to copy over the dll

;;!insertmacro UpgradeDLL "..\IIC\Release\iicidle.dll" "$INSTDIR\iicidle.dll" "$INSTDIR"
;;!insertmacro UpgradeDLL "..\IIC\Release\iicidle2.dll" "$INSTDIR\iicidle2.dll" "$INSTDIR"
!insertmacro UpgradeDLL "..\IIC\Release\iicidle3.dll" "$INSTDIR\iicidle3.dll" "$INSTDIR"
;;!insertmacro UpgradeDLL "..\IIC\Release\iichooks.dll" "$INSTDIR\iichooks.dll" "$INSTDIR"
!insertmacro UpgradeDLL "..\IIC\Release\iichooks2.dll" "$INSTDIR\iichooks2.dll" "$INSTDIR"
!insertmacro UpgradeDLL "..\IIC\Release\IICYInject.dll" "$INSTDIR\IICYInject.dll" "$INSTDIR"
!insertmacro UpgradeDLL "..\IIC\Release\IICHookDll.dll" "$INSTDIR\IICHookDll.dll" "$INSTDIR"

  ; csv files
  File "InstallFiles\*.csv"

  ; bitmaps
  File "InstallFiles\*.bmp"

  ; sounds
  File "InstallFiles\*.wav"

  ; opt files
  File "InstallFiles\*.opt"
  
  ; IIC Plugin ActiveX control
  File ".\Release\IICPlugin.ocx"
  RegDLL $INSTDIR\IICPlugin.ocx

;; Image printer related files
  Call GetWindowsVersion
  Pop $R0
  StrCmp $R0 '95' lbl_Done
  StrCmp $R0 '98' lbl_Done
  StrCmp $R0 'ME' lbl_Done
  StrCmp $R0 'NT' lbl_Done
  File "InstallFiles\Printer\softcopy.dll"
  File "InstallFiles\Printer\softcopyui.dll"
  File "InstallFiles\Printer\softcopy.chm"
  File "InstallFiles\Printer\softcopy.inf"
  File "InstallFiles\Printer\prnadmin.dll"
  File "InstallFiles\Printer\install.vbs"
  RegDLL $INSTDIR\prnadmin.dll
  StrCpy $0 $INSTDIR
  System::Call 'kernel32::SetCurrentDirectory(t) b(r0).r1'
  ExecWait '"$SYSDIR\cscript.exe" "$INSTDIR\install.vbs" add .'

lbl_Done:

FunctionEnd

;;--------------------------------
;; Delete files from install directory
;;--------------------------------
Function un.DeleteInstallFiles
  ; remove files
  Delete $INSTDIR\iic.exe
  Delete $INSTDIR\iicviewer.exe
  Delete $INSTDIR\iicshare.exe
  Delete $INSTDIR\iichooks.dll
  Delete $INSTDIR\iichooks2.dll
  Delete $INSTDIR\IICYInject.dll
  Delete $INSTDIR\iicskin.dll
  Delete $INSTDIR\iicoscar.dll
  Delete $INSTDIR\iicidle.dll
  Delete $INSTDIR\iicidle2.dll
  Delete $INSTDIR\iicidle3.dll
  Delete $INSTDIR\IICHookDll.dll
  Delete $INSTDIR\IICPrintDrvExt.dll


  ; remove uninstaller
  Delete $INSTDIR\uninstall.exe

  ; csv files
  Delete $INSTDIR\*.csv

  ; bitmaps
  Delete $INSTDIR\*.bmp

  ; .opt files
  Delete $INSTDIR\IIC.opt
  Delete $INSTDIR\IICDef.opt

  ; sounds
  Delete $INSTDIR\*.wav

  ; log files
  Delete $INSTDIR\*.log
  Delete $INSTDIR\*.0
  Delete $INSTDIR\*.1
  Delete $INSTDIR\*.2

  ; iic files
  Delete $INSTDIR\*.iic
  
   ; Remove IIC Plugin ActiveX control
  UnRegDLL $INSTDIR\IICPlugin.ocx
  Delete $INSTDIR\IICPlugin.ocx
 
;; Image Printer related files
  Call un.GetWindowsVersion
  Pop $R0
  StrCmp $R0 '95' lbl_Done
  StrCmp $R0 '98' lbl_Done
  StrCmp $R0 'ME' lbl_Done
  StrCmp $R0 'NT' lbl_Done
  StrCpy $0 $INSTDIR
  System::Call 'kernel32::SetCurrentDirectory(t) b(r0).r1'
  ExecWait '"$SYSDIR\cscript.exe" "$INSTDIR\install.vbs" del .'
  Delete $INSTDIR\softcopy.dll
  Delete $INSTDIR\softcopyui.dll
  Delete $INSTDIR\softcopy.chm
  Delete $INSTDIR\softcopy.inf
  Delete $INSTDIR\install.vbs
  UnRegDLL $INSTDIR\prnadmin.dll
  Delete $INSTDIR\prnadmin.dll

lbl_Done:

  ; remove shortcuts, if any
  Delete "$SMPROGRAMS\Imidio\*.*"

  ; remove directories used
  RMDir "$SMPROGRAMS\Imidio"
  RMDir "$INSTDIR"
FunctionEnd

;;--------------------------------
;; Writes registry keys
;;--------------------------------
Function WriteRegistryKeys
  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\Imidio_Client "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Imidio" "DisplayName" "Imidio (remove only)"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Imidio" "UninstallString" '"$INSTDIR\uninstall.exe"'

  ; Write out Protocol handler registry keys
  WriteRegStr HKCR "iic" "" "URL:IIC Protocol"
  WriteRegStr HKCR "iic" "URL Protocol" ""
  WriteRegStr HKCR "iic\DefaultIcon" "" "IIC.exe"
  WriteRegStr HKCR "iic\shell\open\command" "" "$INSTDIR\iic.exe %1"

  ;Write out MIME Type registry keys
  WriteRegStr HKCR ".iic" "" "iic_file"
  WriteRegStr HKCR "iic_file" "" "Instant Collaborator"
  WriteRegBin HKCR "iic_file" "EditFlags" "00000100"
  WriteRegStr HKCR "iic_file\shell\open\command" "" "$INSTDIR\iic.exe /mime %1"
  WriteRegStr HKCR "MIME\Database\Content Type\application/iic" "" ""
  WriteRegStr HKCR "MIME\Database\Content Type\application/iic" "Extension" ".iic"
  
  ; Add IIC to IE's Accept/Type Header
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Accepted Documents" "iic" "application/iic"

  ;; For Image Printer Driver 
  WriteRegStr HKLM SOFTWARE\Dobysoft\SoftCopy "ExtDllPath" "$INSTDIR\IICPrintDrvExt.dll"
FunctionEnd

;;--------------------------------
;; remove registry keys
;;--------------------------------
Function un.DeleteRegistryKeys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Imidio"
  DeleteRegKey HKLM SOFTWARE\Imidio_Client
  DeleteRegKey HKCR "iic"
  DeleteRegKey HKCR ".iic"
  DeleteRegKey HKCR "iic_file"
  DeleteRegKey HKCR "MIME\Database\Content Type\application/iic"
  DeleteRegValue HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Accepted Documents" "iic"
FunctionEnd

;;--------------------------------
;; LaunchClient function - remove registry keys
;;--------------------------------

VAR cmdname
VAR strlen
VAR mid
VAR pid

;; include strtok routine
!include "strtok.nsi"

; Automatically launch IIC after install
Function LaunchClient

    ;; read system var $CMDLINE which contains the name of the installer as it's
    ;; actually invoked on the installing machine.  strategy is to parse
    ;; the name and look for a form <someprefix>/imidio.840.841.exe, where
    ;; 840 is the meeting id and 841 is the participant id.

;    MessageBox MB_OK "Cmd line: $CMDLINE"

    ;; remove containing quotes around cmd line
    StrLen $strlen $CMDLINE
    IntOp $strlen $strlen - 3
    StrCpy $cmdname $CMDLINE $strlen 1

;    MessageBox MB_OK "Cmd line (no quotes): $cmdname"

;;  remove the path portion of the name
;;  form will be something like "c:\temporary files\..\etc\imidio.840.841.exe"
PATH:
    Push $cmdname
    Push "\"    ; path delimeter
    Call StrTok
    Pop $R0     ; R0 contains path portion if any
    Pop $R1     ; R1 is remainder
;    MessageBox MB_OK "PATH parsing: $R0 and $R1"
    StrLen $strlen $R0
    IntCmp 0 $strlen CONTINUE
    StrCpy $cmdname $R1
    Goto PATH   ; more path to go

CONTINUE:

;;  parse the name into component parts
;;  name will be of form imidio.mtgid.partid.exe or imidio.exe
;;  if it contains mtgid and partid these are passed to IIC.exe
;;  at startup
    Push $cmdname
    Push "."
    Call StrTok
    Pop $R0 ; R0 now contains file name prefix (imidio)
    Pop $R1 ; R1 is now suffix

    ;; if suffix is .exe or .tmp, we're done (no contained info)
    StrCmp "exe" $R1 DONE
    StrCmp "tmp" $R1 DONE

    ;; get meeting id
    Push $R1 ; continue parsing the same string
    Push "."
    Call StrTok
    Pop $R0 ; R0 now contains meeting id
    Pop $R1 ; R1 is suffix

    StrCpy $mid $R0
    
    ;; if suffix is .exe or .tmp, we're done (not all params present)
    StrCmp "exe" $R1 DONE
    StrCmp "tmp" $R1 DONE

    ;; get participant id
    Push $R1 ; continue parsing the same string
    Push "."
    Call StrTok
    Pop $R0 ; R0 now contains participant id
    Pop $R1 ; R1 is suffix

    StrCpy $pid $R0

;    MessageBox MB_OK "Mid: $mid"
;    MessageBox MB_OK "Pid: $pid"

    ;; verify mtg id and part id are valid size
    StrLen $strlen $mid
    IntCmp 0 $strlen DONE

    ;StrLen $strlen $pid
    ;IntCmp 0 $strlen DONE

    ;; make the call
    Exec "$INSTDIR\IIC.exe iic:launch?mid=$mid&pid=$pid"
    Goto REALLYDONE

DONE:
;    MessageBox MB_OK "Function End"
    ;; launch client with no args
    Exec "$INSTDIR\IIC.exe"

REALLYDONE:

FunctionEnd
