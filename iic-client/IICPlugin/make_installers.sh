#! /bin/sh
#-------------------------------------------------------------------------
#
# make_installers.sh--
#    Create the Imidio installers
#
# Copyright (c) 2004 Imidio Corporation
#
#
#-------------------------------------------------------------------------

CMDNAME=`basename $0`

help="\
Runs Imidio installers.

Usage:
  $CMDNAME [-i]

Options:
  -h        show help
  -i        if specified, runs interactive version so installers can be tested
"

advice="\
Try '$CMDNAME --help' for more information."

while [ "$#" -gt 0 ]
do
    case "$1" in
	-h|--help|-\?)
	    echo "$help"
	    exit 0
	    ;;
        -i)
	    echo "running interactively"
	    interactive=yes
	    ;;
	-*)
	    echo "$CMDNAME: invalid option: $1" 1>&2
	    echo "$advice" 1>&2
	    exit 1
	    ;;
	*)
	    echo "$CMDNAME: invalid operation mode: $1" 1>&2
	    echo "$advice" 1>&2
	    exit 1
	    ;;
    esac
    shift
done

INSTALLDIR="C:/Program Files/NSIS2"
if [ "$interactive" = yes ]; then
    INSTALLEXE=$INSTALLDIR/MAKENSISW.EXE
else
    INSTALLEXE=$INSTALLDIR/MAKENSIS.EXE
fi

echo "running $INSTALLEXE imidio.nsi"
"$INSTALLEXE" imidio.nsi

echo "running $INSTALLEXE imidiolaunch.nsi"
"$INSTALLEXE" imidiolaunch.nsi

echo "running $INSTALLEXE imidiodlg.nsi"
"$INSTALLEXE" imidiodlg.nsi

echo "tar and feather"
tar cvf installers.tar imidio.exe imidiolaunch.exe imidiodlg.exe
gzip installers.tar

exit 0
