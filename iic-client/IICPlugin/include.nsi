;;
;; include.nsi
;;
;; Copyright (c) 2005 SiteScape, Inc.
;; 
;; Utilities for Finding and Closing IIC app and associated processes.
;;

!include "branding.nsi"

;;--------------------------------
;; Globals
!define iic_main   "IICMainClass"
!define iic_viewer "IICviewerwindow"
!define iic_share  "IICServerTrayIcon"
;;--------------------------------
Var AdminPath
Var TempValue
Var RunOnce
;;--------------------------------

;;--------------------------------
;; If IIC (or other support processes) are running, we prompt to see if the
;; user wants to have IIC killed prior to install
;; NOTE: this method (.onInit) is a standard part of the install
;; process and is called automatically.  Do not change name.
;;--------------------------------
Function .onInit

    ; Sets up for Admin or Limited install based on user privileges
    Call SetInstallType
  
    ;; Turn on install logging
    LogSet on
    
    ;; test if IIC or associated programs are running
    Call TestIICOrFriendsRunning
    Pop $0
    IntCmp $0 0 PROCEED_WITH_INSTALL
    MessageBox MB_YESNO "You must quit ${product_name} prior to installing the new version. \
        OK to stop ${product_name}?" \
	IDNO ABORT_INSTALL

    ;; close IIC and associated programs
    Call CloseIICAndFriends

    ;; uninstall the current version first
    ExecWait '"$INSTDIR\uninstall.exe" _?=$INSTDIR' 
    
    Goto PROCEED_WITH_INSTALL

ABORT_INSTALL:
    Abort

PROCEED_WITH_INSTALL:
    CreateDirectory "$DOCUMENTS\Imidio"

FunctionEnd

;;--------------------------------
;; If IIC (or other support processes) are running, we prompt to see if the
;; user wants to have IIC killed prior to uninstall
;; NOTE: this method (un.onInit) is a standard part of the uninstall
;; process and is called automatically.  Do not change name.
;;--------------------------------
Function un.onInit

    ; Sets up for Admin or Limited install based on user privileges
    Call un.SetInstallType
    
    ;; test if IIC or associated programs are running
    Call un.TestIICOrFriendsRunning
    Pop $0
    IntCmp $0 0 PROCEED_WITH_UNINSTALL
    MessageBox MB_YESNO "You must quit ${product_name} prior to uninstalling. \
        OK to stop ${product_name}?" \
	IDNO ABORT_UNINSTALL

    ;; close IIC and associated programs
    Call un.CloseIICAndFriends
    Goto PROCEED_WITH_UNINSTALL

ABORT_UNINSTALL:
    Abort

PROCEED_WITH_UNINSTALL:
FunctionEnd

;;--------------------------------
;; Sets up for Admin or Limited install based on user privileges
;;
;; Admin if:
;;    - Full access to registry
;;    - Full access to product directory in C:\Program Files 
;;    - Allowed to create shortcuts for All Users
;;
;; Limited if:
;;    - Fails test for Admin
;;    - Full access to product directory in C:\My Documents
;;    - Note that in this case we will NOT install any registry entries
;;
;; If neither type is available, give message and abort the install
;;
;; Update $R9 with one of the following install types: 
;;    "ADMIN", "LIMITED", or "NEITHER"
;;
;;--------------------------------
Function SetInstallType

    ; Assume admin until proven otherwise
    StrCpy $R9 "ADMIN"
    StrCpy $RunOnce "FALSE"

    ; 
    ; Check where files can be installed
    ;
    CreateDirectory "$PROGRAMFILES\Imidio\instest"
    IfErrors DIR_PRIMARY_ERROR DIR_PRIMARY_SUCCESS
  
    DIR_PRIMARY_ERROR:
        CreateDirectory "$DOCUMENTS\Imidio\instest"
        IfErrors DIR_SECONDARY_ERROR DIR_SECONDARY_SUCCESS
  
    DIR_SECONDARY_ERROR:
        ; If we get here, there is no place to install...abort
        MessageBox MB_OK "You do not have sufficient system privileges to install this software.\
                          Please contact a system administrator for assistance."
        Abort
        Goto DIR_SUCCESS
    
    DIR_PRIMARY_SUCCESS:
        ; Able to write to Program files, so continue testing...
        RMDir "$PROGRAMFILES\Imidio\instest"
        Goto DIR_SUCCESS

    DIR_SECONDARY_SUCCESS:
        ; If we get here, jump directly to Limited install type
        RMDir "$DOCUMENTS\Imidio\instest"
        Goto TYPE_LIMITED

    DIR_SUCCESS:
    
    ; 
    ; Check important registry locations to see if we have access
    ;
    REG_CHECK1:
        ; Test HKLM
        WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Accepted Documents" "iictest" ""
        IfErrors REG_ERROR REG_CHECK2 
    
    REG_CHECK2:
        ; Test HKCR
        WriteRegStr HKCR "iictest" "" ""
        IfErrors REG_ERROR REG_SUCCESS
        
    REG_ERROR:
        Goto TYPE_LIMITED
    
    REG_SUCCESS:
    
    ;
    ; Check if shortcuts can be created for All Users
    ;
    SHORTCUT_CHECK:
        ; Actually this should work fine based on how we set
        ; the SetShellVarContext below...
    
    ; 
    ; All done...finalize our install type setup
    ;
    FINALIZE_TYPE:
    
    StrCmp $R9 "ADMIN" TYPE_ADMIN TYPE_LIMITED
    
    TYPE_ADMIN:
        StrCpy $R9 "ADMIN"
        SetShellVarContext all
        StrCpy $InstDir "$PROGRAMFILES\Imidio"
        Goto ALL_DONE
    
    TYPE_LIMITED:
        StrCpy $R9 "LIMITED"
        SetShellVarContext current
        StrCpy $InstDir "$DOCUMENTS\Imidio"
        Call WarnIfUpgrade
        Goto ALL_DONE
        
    ALL_DONE:
        ; Clean up test registry entries
        DeleteRegValue HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Accepted Documents" "iictest"
        DeleteRegKey HKCR "iictest"
        ClearErrors        
    
FunctionEnd

;;--------------------------------
;; Called for Limited installs to check if a previous Admin install is present.  
;; If so, warns the user that an Administrator will need to perform an "official"
;; upgrade.
;;
;; Detects Admin install by looking for the presence of the "Accepted Documents"
;; registry entry, since this entry will cause email and im invitations to attempt
;; to run from the Program Files location.
;;
;;--------------------------------
Function WarnIfUpgrade

  ReadRegStr $TempValue HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Accepted Documents" "iic"

  ; Error will be set if reg entry NOT present, which
  ; indicates that no Admin install is present.  If there
  ; is no error, we found the reg entry and need to warn
  ; the user.
  IfErrors WARNDONE WARNUPGRADE
  
  WARNUPGRADE:
      StrCpy $RunOnce "TRUE"
      MessageBox MB_OK "WARNING:  The previous installation on this PC was performed \
                        by an a user with administrator privileges.  To avoid \
                        downloading the software each time, this upgrade process \
                        must be completed by a user with administrative privileges.$\n$\n\
                        NOTE: Clicking OK will still allow you to join your meeting."
  
  WARNDONE:
    
FunctionEnd


;;--------------------------------
;; closes IIC, iicshare, iicviewer, dlls
;;--------------------------------
Function CloseIICAndFriends
    ;; kill IIC 
    Push ${iic_main}
    Push ${WM_COPYDATA}
    Call CloseProgram

    ;; kill viewer
    Push ${iic_viewer}
    Push ${WM_CLOSE}
    Call CloseProgram

    ;; kill share
    Push ${iic_share}
    Push ${WM_CLOSE}
    Call CloseProgram
FunctionEnd

;;--------------------------------
;; Sends close (or other) msg to main window found from passed main class name
;; Uses FindWindow to find main class
;;--------------------------------
Function CloseProgram 
  Pop $2                    ;; pull kill msg id off stack
  Pop $1                    ;; pull program main class name off stack
  Push $0
  IntFmt $3 "%d" "0"        ;; storage for loop counter
LOOP:
    FindWindow $0 $1
    IntCmp $0 0 DONE        ;; test if kill worked
    IntCmp $3 5 FAILED      ;; test if we're just spinning
    IntCmp $2 ${WM_COPYDATA} COPY_DATA
      SendMessage $0 $2 0 0 ;; whatever msg type was passed
      Goto SLEEP
COPY_DATA:                  ;; copy data with specific message of IIC main
      SendMessage $0 $2 "0" "STR:1"    ;; 1 gets converted to ASCII 0x31
SLEEP:
    Sleep 2000 
    IntOp $3 $3 + 1
    Goto LOOP
FAILED:
  MessageBox MB_OK  "Unable to stop ${product_name}. Please exit the application."
  Abort
DONE: 
  Pop $0 
  Pop $1
FunctionEnd

;;--------------------------------
;; Tests if any of the IIC programs are running.  
;; Returns value at top of stack: 1 (at least 1 running), 0 (none running) 
;;--------------------------------
Function TestIICOrFriendsRunning
;; test for iic
    Push ${iic_main}
    Call IsProgramRunning
    Pop $0
    IntCmp $0 1 PROGRAM_RUNNING

;; test for viewer
    Push ${iic_share}
    Call IsProgramRunning
    Pop $0
    IntCmp $0 1 PROGRAM_RUNNING

;; test for share
    Push ${iic_share}
    Call IsProgramRunning
    Pop $0
    IntCmp $0 1 PROGRAM_RUNNING

;; get here if program not running
    Push 0
    Goto DONE

PROGRAM_RUNNING:
    Push 1

DONE:
FunctionEnd

;;--------------------------------
;; Tests if main class name passed in stack is running
;;--------------------------------
Function IsProgramRunning
    Pop $1
    FindWindow $0 $1 ""
    IntCmp $0 0 PROGRAM_NOT_RUNNING
    Push 1
    Goto DONE
PROGRAM_NOT_RUNNING:
    Push 0
DONE:
FunctionEnd


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Programs called in uninstall section must start with un.*
;; so we duplicate code here to make nsis happy
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;--------------------------------
;; Sets up for Admin or Limited uninstall, based on install directory
;;
;; Admin if:
;;    - Installed to Program Files
;;
;; Limited if:
;;    - NOT installed to Program Files
;;
;;--------------------------------
Function un.SetInstallType

    StrCpy $AdminPath "$PROGRAMFILES\Imidio"
    
    StrCmp $InstDir $AdminPath UNINSTALL_ADMIN UNINSTALL_LIMITED
    
    UNINSTALL_ADMIN:
        StrCpy $R9 "ADMIN"
        SetShellVarContext all
        Goto UNINSTALL_DONE
    
    UNINSTALL_LIMITED:
        StrCpy $R9 "LIMITED"
        SetShellVarContext current
        Goto UNINSTALL_DONE
    
    UNINSTALL_DONE:
    
FunctionEnd

;;--------------------------------
;; closes IIC, iicshare, iicviewer, dlls
;;--------------------------------
Function un.CloseIICAndFriends
    ;; kill IIC
    Push ${iic_main}
    Push ${WM_COPYDATA}
    Call un.CloseProgram

    ;; kill viewer
    Push ${iic_viewer}
    Push ${WM_CLOSE}
    Call un.CloseProgram

    ;; kill share
    Push ${iic_share}
    Push ${WM_CLOSE}
    Call un.CloseProgram
FunctionEnd

;;--------------------------------
;; Sends close (or other) msg to main window found from passed main class name
;; Uses FindWindow to find main class
;;--------------------------------
Function un.CloseProgram 
  Pop $2                    ;; pull kill msg id off stack
  Pop $1                    ;; pull program main class name off stack
  Push $0
  IntFmt $3 "%d" "0"
LOOP:
    FindWindow $0 $1
    IntCmp $0 0 DONE        ;; test if kill worked
    IntCmp $3 5 FAILED      ;; test if we're just spinning
    IntCmp $2 ${WM_COPYDATA} COPY_DATA
      SendMessage $0 $2 0 0 ;; whatever msg type was passed
      Goto SLEEP
COPY_DATA:                  ;; copy data with specific message of IIC main
      SendMessage $0 $2 "0" "STR:1"    ;; 1 gets converted to ASCII 0x31
SLEEP:
    Sleep 2000 
    IntOp $3 $3 + 1
    Goto LOOP
FAILED:
  MessageBox MB_OK  "Unable to stop ${product_name}. Please exit the application."
  Abort
DONE: 
  Pop $0 
  Pop $1
FunctionEnd

;;--------------------------------
;; Tests if any of the IIC programs are running.  
;; Returns value at top of stack: 1 (at least 1 running), 0 (none running) 
;;--------------------------------
Function un.TestIICOrFriendsRunning
;; test for iic
    Push ${iic_main}
    Call un.IsProgramRunning
    Pop $0
    IntCmp $0 1 PROGRAM_RUNNING

;; test for viewer
    Push ${iic_share}
    Call un.IsProgramRunning
    Pop $0
    IntCmp $0 1 PROGRAM_RUNNING

;; test for share
    Push ${iic_share}
    Call un.IsProgramRunning
    Pop $0
    IntCmp $0 1 PROGRAM_RUNNING

;; get here if program not running
    Push 0
    Goto DONE

PROGRAM_RUNNING:
    Push 1

DONE:
FunctionEnd

;;--------------------------------
;; Tests if main class name passed in stack is running
;;--------------------------------
Function un.IsProgramRunning
    Pop $1
    FindWindow $0 $1 ""
    IntCmp $0 0 PROGRAM_NOT_RUNNING
    Push 1
    Goto DONE
PROGRAM_NOT_RUNNING:
    Push 0
DONE:
FunctionEnd
