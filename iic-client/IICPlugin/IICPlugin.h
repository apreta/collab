#if !defined(AFX_IICPLUGIN_H__1A6F4600_391F_4499_BE61_CA85CE549EF1__INCLUDED_)
#define AFX_IICPLUGIN_H__1A6F4600_391F_4499_BE61_CA85CE549EF1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// IICPlugin.h : main header file for IICPLUGIN.DLL

#if !defined( __AFXCTL_H__ )
	#error include 'afxctl.h' before including this file
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CIICPluginApp : See IICPlugin.cpp for implementation.

class CIICPluginApp : public COleControlModule
{
public:
	BOOL InitInstance();
	int ExitInstance();
};

extern const GUID CDECL _tlid;
extern const WORD _wVerMajor;
extern const WORD _wVerMinor;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IICPLUGIN_H__1A6F4600_391F_4499_BE61_CA85CE549EF1__INCLUDED)
