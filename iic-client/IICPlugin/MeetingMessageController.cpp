// CMeetingMessageController.cpp: implementation of the CMeetingMessageController class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"

#include "MeetingMessageController.h"
#include "Messenger.h"
#include "MeetingMessages.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMeetingMessageController::CMeetingMessageController()
{

}

CMeetingMessageController::~CMeetingMessageController()
{

}

void CMeetingMessageController::Initialize(const CString& meetingID,
                                           const CString& participantID,
										   const CString& screenName,
										   const CString& server,
										   const CString& phone,
										   const CString& email,
										   const CString& fullname,
                                           CInstallController* installController)
{
	m_meetingID = meetingID;
    m_participantID = participantID;
	m_screenName = screenName;
	m_server = server;
    m_phone = phone;
    m_email = email;
    m_fullname = fullname;
    m_installController = installController;
}

// assumes the client is already installed
BOOL CMeetingMessageController::ActivateClient()
{
	HWND clientWindow = NULL;
	BOOL result = FALSE;

#ifdef CLASSIC
	// if client is not running check for installation
	if ((clientWindow = m_clientDetector.FindRunningClient(m_screenName)) == NULL)
	{
		// client should now be installed, try launching
		clientWindow = m_clientDetector.LaunchClient(m_meetingID, 
                                                     m_participantID, 
                                                     m_screenName, 
                                                     m_server,
                                                     m_phone,
                                                     m_email,
                                                     m_fullname);
	}

	// if client is now running
	if (clientWindow != NULL)
	{
		// communicate meeting data to client
		ProcessMeetingMessage(clientWindow);
		result = TRUE;
	}
#else
	if (m_meetingID.GetLength())
	{
		const CString DELIM = " ";
		const CString QT = "\"";
		CString command = _T("iic:plugin"); 
		command += DELIM + QT + m_meetingID + QT + DELIM + QT + m_participantID + QT + DELIM + QT + m_screenName + QT + DELIM + QT + m_server + QT;
		command += DELIM + QT + m_phone + QT + DELIM + QT + m_email + QT + DELIM + QT + m_fullname + QT;

		ShellExecute(NULL, _T("open"), command, NULL , NULL, SW_SHOWNORMAL);
	}
#endif
	return result;
}

BOOL CMeetingMessageController::ProcessMeetingMessage(HWND clientWindow)
{
    const CString DELIM = ";";
    meetingInfoMessage message;
	CString command = m_meetingID + DELIM + m_participantID + DELIM + m_screenName + DELIM + m_server + DELIM;
    command += m_phone + DELIM + m_email + DELIM + m_fullname + DELIM;

	message.meetingHeader  = IIC_MEETING_INFO_HEADER;
	message.meetingVersion = IIC_MEETING_INFO_VERSION;
	message.meetingCommand = ADD_MEETING_CMD;

    char    *pBuf;

#ifdef UNICODE
    //  Convert the CString from Unicode to UTF-8

    char    *cTemp    = NULL;
    int     iLen = WideCharToMultiByte( CP_UTF8, 0, command, -1, cTemp, 0, NULL, NULL );
    if( iLen  >  0 )
    {
        cTemp = new char[ iLen + 1 ];
        iLen = WideCharToMultiByte( CP_UTF8, 0, command, -1, cTemp, iLen + 1, NULL, NULL );
        cTemp[ iLen ] = 0;
    }
    strcpy( message.meetingString, cTemp );
    free( cTemp );

#else

    strcpy( message.meetingString, (char*) (LPCTSTR) command );

#endif

	CMessenger messenger(clientWindow);
	messenger.SendParameters(message);

	return TRUE;
}

