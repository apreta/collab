// IICPluginCtl.cpp : Implementation of the CIICPluginCtrl ActiveX Control class.

#include "stdafx.h"
#include "IICPlugin.h"
#include "IICPluginCtl.h"
#include "IICPluginPpg.h"
#include "LoadProgressDialog.h"
#include "CSV.h"
#include "utf8_utils.h"
#include <atlbase.h>

#include "MeetingMessageController.h"

#ifdef L_IMPL_OBJECTSAFETY
#include <objsafe.h>
#include ".\iicpluginctl.h"
#endif // L_IMPL_OBJECTSAFETY

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CIICPluginCtrl, COleControl)

#ifdef L_IMPL_OBJECTSAFETY
BEGIN_INTERFACE_MAP(CIICPluginCtrl, COleControl)
  INTERFACE_PART(CIICPluginCtrl, IID_IObjectSafety, ObjectSafety)
END_INTERFACE_MAP()
#endif // L_IMPL_OBJECTSAFETY

/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CIICPluginCtrl, COleControl)
	//{{AFX_MSG_MAP(CIICPluginCtrl)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
	ON_OLEVERB(AFX_IDS_VERB_PROPERTIES, OnProperties)
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Dispatch map

BEGIN_DISPATCH_MAP(CIICPluginCtrl, COleControl)
	//{{AFX_DISPATCH_MAP(CIICPluginCtrl)
	DISP_PROPERTY_NOTIFY(CIICPluginCtrl, "ScreenName", m_screenName, OnScreenNameChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CIICPluginCtrl, "Host", m_host, OnHostChanged, VT_BSTR)
	DISP_PROPERTY_NOTIFY(CIICPluginCtrl, "Topic", m_topic, OnTopicChanged, VT_BSTR)
	DISP_PROPERTY_EX(CIICPluginCtrl, "MeetingURL", GetMeetingURL, SetMeetingURL, VT_BSTR)
	DISP_PROPERTY_EX(CIICPluginCtrl, "InstallerURL", GetInstallerURL, SetInstallerURL, VT_BSTR)
	DISP_PROPERTY_EX(CIICPluginCtrl, "MeetingID", GetMeetingID, SetMeetingID, VT_BSTR)
	DISP_PROPERTY_EX(CIICPluginCtrl, "ParticipantID", GetParticipantID, SetParticipantID, VT_BSTR)
	DISP_PROPERTY_EX(CIICPluginCtrl, "Participant", GetParticipant, SetParticipant, VT_BSTR)
	DISP_PROPERTY_EX(CIICPluginCtrl, "MeetingTime", GetMeetingTime, SetMeetingTime, VT_BSTR)
	DISP_PROPERTY_EX(CIICPluginCtrl, "Server", GetServer, SetServer, VT_BSTR)
	DISP_PROPERTY_EX(CIICPluginCtrl, "Phone", GetPhone, SetPhone, VT_BSTR)
	DISP_PROPERTY_EX(CIICPluginCtrl, "Email", GetEmail, SetEmail, VT_BSTR)
	DISP_PROPERTY_EX(CIICPluginCtrl, "Token", GetToken, SetToken, VT_BSTR)
	//}}AFX_DISPATCH_MAP
	DISP_PROPERTY_EX_ID(CIICPluginCtrl, "InstallTest", dispidInstallTest, GetInstallTest, SetInstallTest, VT_BSTR)
	DISP_FUNCTION_ID(CIICPluginCtrl, "ServerToken", dispidServerToken, ServerToken, VT_BSTR, VTS_NONE)
	DISP_FUNCTION_ID(CIICPluginCtrl, "StartMeeting", dispidStartMeeting, StartMeeting, VT_BOOL, VTS_NONE)
END_DISPATCH_MAP()


/////////////////////////////////////////////////////////////////////////////
// Event map

BEGIN_EVENT_MAP(CIICPluginCtrl, COleControl)
	//{{AFX_EVENT_MAP(CIICPluginCtrl)
	// NOTE - ClassWizard will add and remove event map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_EVENT_MAP
END_EVENT_MAP()


/////////////////////////////////////////////////////////////////////////////
// Property pages

// TODO: Add more property pages as needed.  Remember to increase the count!
BEGIN_PROPPAGEIDS(CIICPluginCtrl, 1)
	PROPPAGEID(CIICPluginPropPage::guid)
END_PROPPAGEIDS(CIICPluginCtrl)


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CIICPluginCtrl, "IICPLUGIN.IICPluginCtrl.1",
	0x29956d12, 0xf6f5, 0x446a, 0x8c, 0x55, 0xcb, 0x73, 0x47, 0xde, 0xd, 0xc0)


/////////////////////////////////////////////////////////////////////////////
// Type library ID and version

IMPLEMENT_OLETYPELIB(CIICPluginCtrl, _tlid, _wVerMajor, _wVerMinor)


/////////////////////////////////////////////////////////////////////////////
// Interface IDs

const IID BASED_CODE IID_DIICPlugin =
		{ 0x92598520, 0x3fa3, 0x4d3f, { 0x90, 0xeb, 0x1, 0xa2, 0xd7, 0x65, 0x81, 0xc0 } };
const IID BASED_CODE IID_DIICPluginEvents =
		{ 0x552ae1e0, 0xf7cc, 0x4e55, { 0xb4, 0xcf, 0x73, 0xe0, 0xa, 0xb1, 0x86, 0x7b } };


/////////////////////////////////////////////////////////////////////////////
// Control type information

static const DWORD BASED_CODE _dwIICPluginOleMisc =
//	OLEMISC_INVISIBLEATRUNTIME |   // this would be added if the control were invisible
	OLEMISC_ACTIVATEWHENVISIBLE |
	OLEMISC_SETCLIENTSITEFIRST |
	OLEMISC_INSIDEOUT |
	OLEMISC_CANTLINKINSIDE |
	OLEMISC_RECOMPOSEONRESIZE;

IMPLEMENT_OLECTLTYPE(CIICPluginCtrl, IDS_IICPLUGIN, _dwIICPluginOleMisc)


/////////////////////////////////////////////////////////////////////////////
// CIICPluginCtrl::CIICPluginCtrlFactory::UpdateRegistry -
// Adds or removes system registry entries for CIICPluginCtrl

BOOL CIICPluginCtrl::CIICPluginCtrlFactory::UpdateRegistry(BOOL bRegister)
{
	// TODO: Verify that your control follows apartment-model threading rules.
	// Refer to MFC TechNote 64 for more information.
	// If your control does not conform to the apartment-model rules, then
	// you must modify the code below, changing the 6th parameter from
	// afxRegApartmentThreading to 0.

	if (bRegister)
		return AfxOleRegisterControlClass(
			AfxGetInstanceHandle(),
			m_clsid,
			m_lpszProgID,
			IDS_IICPLUGIN,
			IDB_IICPLUGIN,
			afxRegApartmentThreading,
			_dwIICPluginOleMisc,
			_tlid,
			_wVerMajor,
			_wVerMinor);
	else
		return AfxOleUnregisterClass(m_clsid, m_lpszProgID);
}


/////////////////////////////////////////////////////////////////////////////
// CIICPluginCtrl::CIICPluginCtrl - Constructor

CIICPluginCtrl::CIICPluginCtrl()
{
	InitializeIIDs(&IID_DIICPlugin, &IID_DIICPluginEvents);

	// TODO: Initialize your control's instance data here.
	m_firstTime = TRUE;
	m_dataLoaded = FALSE;
}


/////////////////////////////////////////////////////////////////////////////
// CIICPluginCtrl::~CIICPluginCtrl - Destructor

CIICPluginCtrl::~CIICPluginCtrl()
{
	// TODO: Cleanup your control's instance data here.
}


bool CIICPluginCtrl::SetProgress(int _percent)
{
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CIICPluginCtrl::OnDraw - Drawing function

void CIICPluginCtrl::OnDraw(
			CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid)
{
	TEXTMETRIC tm;
	CRect rc = rcBounds;

	// TODO: Replace the following code with your own drawing code.
	pdc->FillRect(rcBounds, CBrush::FromHandle((HBRUSH)GetStockObject(WHITE_BRUSH)));

	GetStockTextMetrics(&tm);
	pdc->SetTextAlign(TA_LEFT | TA_TOP);

	CString PARTICIPANT = _T("Participant:");
	CString TIME = _T("Time:");
	CString TOPIC = _T("Topic:");
	CString HOST = _T("Host:");
	CString who = GetParticipant();
	CString when = GetMeetingTime();

	int NUM_LINES = 5;

	// build left column
	// mtg participant
	pdc->ExtTextOut(rc.left + 10, (rc.top + rc.bottom - tm.tmHeight) / NUM_LINES,
		ETO_CLIPPED, rc, PARTICIPANT, PARTICIPANT.GetLength(), NULL);

	// time
	pdc->ExtTextOut(rc.left + 10, 2* (rc.top + rc.bottom - tm.tmHeight) / NUM_LINES,
		ETO_CLIPPED, rc, TIME, TIME.GetLength(), NULL);

	// topic
	pdc->ExtTextOut(rc.left + 10, 3* (rc.top + rc.bottom - tm.tmHeight) / NUM_LINES,
		ETO_CLIPPED, rc, TOPIC, TOPIC.GetLength(), NULL);

	// host
	pdc->ExtTextOut(rc.left + 10, 4* (rc.top + rc.bottom - tm.tmHeight) / NUM_LINES,
		ETO_CLIPPED, rc, HOST, HOST.GetLength(), NULL);

	// right column
	pdc->SetTextColor(RGB(128,0,128));
	
	pdc->ExtTextOut((rc.left + rc.right) / 3, (rc.top + rc.bottom - tm.tmHeight) / NUM_LINES,
		ETO_CLIPPED, rc, who, who.GetLength(), NULL);
	pdc->ExtTextOut((rc.left + rc.right) / 3, 2*(rc.top + rc.bottom - tm.tmHeight) / NUM_LINES,
		ETO_CLIPPED, rc, when, when.GetLength(), NULL);
	pdc->ExtTextOut((rc.left + rc.right) / 3, 3*(rc.top + rc.bottom - tm.tmHeight) / NUM_LINES,
		ETO_CLIPPED, rc, this->m_topic, this->m_topic.GetLength(), NULL);
	pdc->ExtTextOut((rc.left + rc.right) / 3, 4*(rc.top + rc.bottom - tm.tmHeight) / NUM_LINES,
		ETO_CLIPPED, rc, this->m_host, this->m_host.GetLength(), NULL);
}

/////////////////////////////////////////////////////////////////////////////
// CIICPluginCtrl::DoPropExchange - Persistence support

void CIICPluginCtrl::DoPropExchange(CPropExchange* pPX)	
{
	ExchangeVersion(pPX, MAKELONG(_wVerMinor, _wVerMajor));
	COleControl::DoPropExchange(pPX);

	PX_String(pPX, _T("MeetingURL"), m_meetingURL, _T("Meeting URL not found"));
	PX_String(pPX, _T("InstallerURL"), m_installerURL, _T("Installer URL not found"));
	PX_String(pPX, _T("MeetingID"), m_meetingID, _T("Meeting ID not found"));
	PX_String(pPX, _T("ParticipantID"), m_participantID, _T("Participant ID not found"));
	PX_String(pPX, _T("Participant"), m_participant, _T("Participant not found"));
	PX_String(pPX, _T("MeetingTime"), m_meetingTime, _T("Meeting Time not found"));
	PX_String(pPX, _T("Server"), m_server, _T("Meeting server not found"));
	PX_String(pPX, _T("ScreenName"), m_screenName, _T("Screen name not found"));
	PX_String(pPX, _T("Topic"), m_topic, _T("Topic not found"));
	PX_String(pPX, _T("Host"), m_host, _T("Host not found"));
    PX_String(pPX, _T("Phone"), m_phone, _T("Phone not found"));
    PX_String(pPX, _T("Email"), m_email, _T("Email not found"));
    PX_String(pPX, _T("Token"), m_token, _T(""));

	m_dataLoaded = TRUE;
	
    // If token is empty, this is an old server 
    // Activate client now, rather than waiting 
    // for a call to StartMeeting.
    if (m_token.IsEmpty())
        ActivateClient();
}

// installs and loads client
void CIICPluginCtrl::ActivateClient()
{
	m_installController.SetInstallerURL(m_installerURL);
	m_msgController.Initialize(m_meetingID, 
                               m_participantID, 
                               m_screenName, 
                               m_server, 
                               m_phone,
                               m_email,
                               m_participant, 
                               &m_installController);

    /* Plugin no longer installs or upgrades the client...
	// is client installed?
	BOOL result = m_installController.IsClientInstalled();
	if (result == FALSE)
	{
	    CLoadProgressDialog* dlg = 
			new CLoadProgressDialog(&m_installController, m_installerURL, NULL);
	    dlg->DoModal();
		
		// tbd do inside dialog
		m_installController.Cleanup();

		// exec installer
		if (!dlg->WasCancel())
			result = m_installController.ExecInstaller();

		// clean installer
		if (result == TRUE)
			result = m_installController.CleanInstaller();
	}
    */
    BOOL result = TRUE;

	if (result == TRUE) // client is installed
	{
		// notify the meeting messenger that meeting is loaded
		m_msgController.ActivateClient();
	}
}


/////////////////////////////////////////////////////////////////////////////
// CIICPluginCtrl::OnResetState - Reset control to default state

void CIICPluginCtrl::OnResetState()
{
	COleControl::OnResetState();  // Resets defaults found in DoPropExchange

	// TODO: Reset any other control state here.
}


/////////////////////////////////////////////////////////////////////////////
// CIICPluginCtrl message handlers

#ifdef L_IMPL_OBJECTSAFETY
// Implementation of IObjectSafety
STDMETHODIMP CIICPluginCtrl::XObjectSafety::GetInterfaceSafetyOptions(
			REFIID riid, 
			DWORD __RPC_FAR *pdwSupportedOptions, 
			DWORD __RPC_FAR *pdwEnabledOptions)
{
	METHOD_PROLOGUE_EX(CIICPluginCtrl, ObjectSafety)

	if (!pdwSupportedOptions || !pdwEnabledOptions)
	{
		return E_POINTER;
	}

	*pdwSupportedOptions = INTERFACESAFE_FOR_UNTRUSTED_CALLER | INTERFACESAFE_FOR_UNTRUSTED_DATA;
	*pdwEnabledOptions = 0;

	if (NULL == pThis->GetInterface(&riid))
	{
		TRACE("Requested interface is not supported.\n");
		return E_NOINTERFACE;
	}

	// What interface is being checked out anyhow?
	OLECHAR szGUID[39];
	int i = StringFromGUID2(riid, szGUID, 39);

	if (riid == IID_IDispatch)
	{
		// Client wants to know if object is safe for scripting
		*pdwEnabledOptions = INTERFACESAFE_FOR_UNTRUSTED_CALLER;
		return S_OK;
	}
	else if (riid == IID_IPersistPropertyBag 
		  || riid == IID_IPersistStreamInit
		  || riid == IID_IPersistStorage
		  || riid == IID_IPersistMemory)
	{
		// Those are the persistence interfaces COleControl derived controls support
		// as indicated in AFXCTL.H
		// Client wants to know if object is safe for initializing from persistent data
		*pdwEnabledOptions = INTERFACESAFE_FOR_UNTRUSTED_DATA;
		return S_OK;
	}
	else
	{
		// Find out what interface this is, and decide what options to enable
		TRACE("We didn't account for the safety of this interface, and it's one we support...\n");
		return E_NOINTERFACE;
	}	
}

STDMETHODIMP CIICPluginCtrl::XObjectSafety::SetInterfaceSafetyOptions(
		REFIID riid, 
		DWORD dwOptionSetMask, 
		DWORD dwEnabledOptions)
{
	METHOD_PROLOGUE_EX(CIICPluginCtrl, ObjectSafety)

	OLECHAR szGUID[39];
	// What is this interface anyway?
	// We can do a quick lookup in the registry under HKEY_CLASSES_ROOT\Interface
	int i = StringFromGUID2(riid, szGUID, 39);

	if (0 == dwOptionSetMask && 0 == dwEnabledOptions)
	{
		// the control certainly supports NO requests through the specified interface
		// so it's safe to return S_OK even if the interface isn't supported.
		return S_OK;
	}

	// Do we support the specified interface?
	if (NULL == pThis->GetInterface(&riid))
	{
		TRACE1("%s is not support.\n", szGUID);
		return E_FAIL;
	}


	if (riid == IID_IDispatch)
	{
		TRACE("Client asking if it's safe to call through IDispatch.\n");
		TRACE("In other words, is the control safe for scripting?\n");
		if (INTERFACESAFE_FOR_UNTRUSTED_CALLER == dwOptionSetMask && INTERFACESAFE_FOR_UNTRUSTED_CALLER == dwEnabledOptions)
		{
			return S_OK;
		}
		else
		{
			return E_FAIL;
		}
	}
	else if (riid == IID_IPersistPropertyBag 
		  || riid == IID_IPersistStreamInit
		  || riid == IID_IPersistStorage
		  || riid == IID_IPersistMemory)
	{
		TRACE("Client asking if it's safe to call through IPersist*.\n");
		TRACE("In other words, is the control safe for initializing from persistent data?\n");

		if (INTERFACESAFE_FOR_UNTRUSTED_DATA == dwOptionSetMask && INTERFACESAFE_FOR_UNTRUSTED_DATA == dwEnabledOptions)
		{
			return NOERROR;
		}
		else
		{
			return E_FAIL;
		}
	}
	else
	{
		TRACE1("We didn't account for the safety of %s, and it's one we support...\n", szGUID);
		return E_FAIL;
	}
}

STDMETHODIMP_(ULONG) CIICPluginCtrl::XObjectSafety::AddRef()
{
	METHOD_PROLOGUE_EX_(CIICPluginCtrl, ObjectSafety)
	return (ULONG)pThis->ExternalAddRef();
}

STDMETHODIMP_(ULONG) CIICPluginCtrl::XObjectSafety::Release()
{
	METHOD_PROLOGUE_EX_(CIICPluginCtrl, ObjectSafety)
	return (ULONG)pThis->ExternalRelease();
}

STDMETHODIMP CIICPluginCtrl::XObjectSafety::QueryInterface(
	REFIID iid, LPVOID* ppvObj)
{
	METHOD_PROLOGUE_EX_(CIICPluginCtrl, ObjectSafety)
	return (HRESULT)pThis->ExternalQueryInterface(&iid, ppvObj);
}

#endif // L_IMPL_OBJECTSAFETY


BSTR CIICPluginCtrl::GetMeetingURL() 
{
	// TODO: Add your property handler here

	return m_meetingURL.AllocSysString();
}

void CIICPluginCtrl::SetMeetingURL(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	m_meetingURL = lpszNewValue;

	SetModifiedFlag();
}

BSTR CIICPluginCtrl::GetInstallerURL() 
{
	// TODO: Add your property handler here

	return m_installerURL.AllocSysString();
}

void CIICPluginCtrl::SetInstallerURL(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	m_installerURL = lpszNewValue;

	SetModifiedFlag();
}

BSTR CIICPluginCtrl::GetMeetingID() 
{
	// TODO: Add your property handler here

	return m_meetingID.AllocSysString();
}

void CIICPluginCtrl::SetMeetingID(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	m_meetingID = lpszNewValue;

	SetModifiedFlag();
}

BSTR CIICPluginCtrl::GetParticipantID() 
{
	// TODO: Add your property handler here

	return m_participantID.AllocSysString();
}

void CIICPluginCtrl::SetParticipantID(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	m_participantID = lpszNewValue;

	SetModifiedFlag();
}

BSTR CIICPluginCtrl::GetParticipant() 
{
	// TODO: Add your property handler here

	return m_participant.AllocSysString();
}

void CIICPluginCtrl::SetParticipant(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	m_participant = lpszNewValue;

	SetModifiedFlag();
}

BSTR CIICPluginCtrl::GetMeetingTime() 
{
	// TODO: Add your property handler here

	return m_meetingTime.AllocSysString();
}

void CIICPluginCtrl::SetMeetingTime(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	m_meetingTime = lpszNewValue;

	SetModifiedFlag();
}

BSTR CIICPluginCtrl::GetServer() 
{
	// TODO: Add your property handler here

	return m_server.AllocSysString();
}

void CIICPluginCtrl::SetServer(LPCTSTR lpszNewValue) 
{
	// TODO: Add your property handler here
	m_server = lpszNewValue;

	SetModifiedFlag();
}

void CIICPluginCtrl::OnScreenNameChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CIICPluginCtrl::OnHostChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}

void CIICPluginCtrl::OnTopicChanged() 
{
	// TODO: Add notification handler code

	SetModifiedFlag();
}



BSTR CIICPluginCtrl::GetPhone() 
{
	return m_phone.AllocSysString();
}

void CIICPluginCtrl::SetPhone(LPCTSTR lpszNewValue) 
{
	m_phone = lpszNewValue;

	SetModifiedFlag();
}

BSTR CIICPluginCtrl::GetEmail() 
{
	return m_email.AllocSysString();
}

void CIICPluginCtrl::SetEmail(LPCTSTR lpszNewValue) 
{
	m_email = lpszNewValue;

	SetModifiedFlag();
}

BSTR CIICPluginCtrl::GetToken() 
{
	return m_token.AllocSysString();
}

void CIICPluginCtrl::SetToken(LPCTSTR lpszNewValue) 
{
	m_token = lpszNewValue;

	SetModifiedFlag();
}

BSTR CIICPluginCtrl::GetInstallTest(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString strResult;

	// TODO: Add your dispatch handler code here

	return strResult.AllocSysString();
}

void CIICPluginCtrl::SetInstallTest(LPCTSTR newVal)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: Add your property handler code here

	SetModifiedFlag();
}

BSTR CIICPluginCtrl::ServerToken(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	CString strResult = ReadServerToken();
	return strResult.AllocSysString();
}

VARIANT_BOOL CIICPluginCtrl::StartMeeting(void)
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	// TODO: Add your dispatch handler code here
	ActivateClient();

	return VARIANT_TRUE;
}

CString CIICPluginCtrl::ReadServerToken(void)
{
	CString strServerToken = "0";
    CString strClientFile; // = ClientLocationFromRegistry();
	
	// Plug-in in installed in application directory--find option file in same directory.
	TCHAR buffer[MAX_PATH];
	::GetModuleFileName(AfxGetInstanceHandle(), buffer, sizeof(buffer));
	strClientFile = buffer;
	int pos = strClientFile.ReverseFind(_T('\\'));
	if (pos > 0)
		strClientFile = strClientFile.Left(pos+1);
	strClientFile += _T("zon-def.opt");

    //if (strClientFile.IsEmpty())
    //    return "-1";

    // Process the zon-opt.def file, looking for the ServerToken
	FILE* hfile = _tfopen(strClientFile, _T("r"));
    if (hfile)
    {
        char            cBuffer[MAX_PATH];
        CString         S2;
        CStringArray    arrayPreference;
        char            *pBuf           = NULL;

        while( fgets( cBuffer, MAX_PATH-1, hfile ) )    //For each line of the file
        {
            cBuffer[MAX_PATH-1] = _T('\0');
            // fgets gets a line including the newline char
            char    *pC = strchr( cBuffer, '\n' );
            if( pC )
                *pC = '\0';

            CCSV    CSV;
            CString Field;

#ifdef UNICODE
            S2 = CUNICODEString( cBuffer );
#else
            S2 = cBuffer;
#endif

            CSV.Set( &Field, S2 );
            arrayPreference.RemoveAll();

            // Get all fields for this line in the preference file
            while(CSV.GetNextField())
            { 
                arrayPreference.Add(Field);
            } 

			if (arrayPreference[0] == _T("ServerToken"))
			{
				strServerToken = arrayPreference[1];
			}
        }

        fclose(hfile);
    }

	return strServerToken;
}

CString CIICPluginCtrl::ClientLocationFromRegistry(void)
{
	// N.B.: "Zon_client" is no longer written to registry
	// Read Registry to get the path to the client
    CString strClientLocation = "";
    HKEY hKey;
    DWORD dwCount = -1;

    if ( RegOpenKeyEx( HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Zon_Client"), 0, KEY_READ, &hKey ) == ERROR_SUCCESS )
    {
        CRegKey regKey;
        regKey.Attach( hKey );

        TCHAR szValData[ 100 ];
        CString szValName = _T("Install_Dir");

        if ( regKey.QueryValue( szValData, szValName, &dwCount ) == ERROR_SUCCESS )
        {
            strClientLocation = szValData;
            strClientLocation += "\\zon-def.opt";
        }
    }

    return strClientLocation;
}
