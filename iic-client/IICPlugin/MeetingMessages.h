#ifndef _MEETING_MESSAGES_
#define _MEETING_MESSAGES_

///////////////////////////////////////////////////////////////
// Defines commands and data structure for passing meeting
// messages to the IIC client.
///////////////////////////////////////////////////////////////

// unique id word
#define IIC_MEETING_INFO_HEADER 0x01010101

// version
#define IIC_MEETING_INFO_VERSION 0x01

// meeting commands
#define NULL_MEETING_CMD	0x00
#define ADD_MEETING_CMD		0x01
#define REMOVE_MEETING_CMD	0x02
#define MODIFY_MEETING_CMD  0x04

// data structure for passing meeting info

typedef struct 
{
	int meetingHeader;
	int meetingVersion;
	int meetingCommand;
	char meetingString[4096];
} meetingInfoMessage;

#define MEETING_MSG         0x30        // send 0 if a meeting msg
#define EXIT_MSG            0x31        // send 1 if an exit msg
#define MEETING_LOOKUP_MSG  0x32        // send 2 if mtg needs lookup
#define SHARE_LITE_MSG      0x33        // send 3 for share lite invocation
#define PROTOCOL_HANDLER    0x34        // send 4 for protocol handler event
#endif
