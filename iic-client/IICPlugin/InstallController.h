// InstallController.h: interface for the InstallController class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_INSTALLCONTROLLER_H__4E804A91_700B_4BC7_8CFD_CE31C8864359__INCLUDED_)
#define AFX_INSTALLCONTROLLER_H__4E804A91_700B_4BC7_8CFD_CE31C8864359__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Wininet.h"
#include "ThreadBase.h"
#include "ProgressListener.h"

#define INSTALLER_URL      _T("http://localhost/downloads/imidio.exe")
#define VERSION_URL        _T("http://localhost/downloads/iicversion.txt")
#define INSTALLER_FILENAME _T("c:/TEMP/imidio.exe")

//////////////////////////////////////////////////////////////////////
// Controls the installation process
//////////////////////////////////////////////////////////////////////
class CInstallController : public CThreadBase
{
public:
	CInstallController();
	virtual ~CInstallController();

	void SetInstallerURL(CString installerURL) { m_installerURL = installerURL; }

	// saves the installer from the passed URL to a temp location
	BOOL SaveInstaller(IProgressListener* listener);

	// cleans up open resources from the save
	BOOL Cleanup();

	// executes the installer which installs the IIC client
	BOOL ExecInstaller();

	// removes the installer from its temp location
	BOOL CleanInstaller();

	// detects if the client is already installed
	BOOL IsClientInstalled();

protected:
	// must override this function
	UINT ThreadMain();
	
private:
    BOOL IsClientVersionCurrent();
    BOOL ShouldUpgrade(CString& currentVersion, CString& mostRecentVersion);

	void error(LPSTR functionName);
	
	TCHAR *genTempFile();

	// launches installer, waits for it to complete
	HANDLE LaunchInstaller(LPCTSTR program, LPCTSTR args);

	CString   m_installerURL;			// URL with installer
	TCHAR     *m_installerFileName;		// generated temp name for file to store into
	FILE      *m_installFile;			// FILE* to temp file
	HINTERNET m_hURL;				// HINTERNET to open URL
	HINTERNET m_hInternetSession;	// HINTERNET to open session

	IProgressListener* m_progressListener;  // to report progress to
};

#endif // !defined(AFX_INSTALLCONTROLLER_H__4E804A91_700B_4BC7_8CFD_CE31C8864359__INCLUDED_)
