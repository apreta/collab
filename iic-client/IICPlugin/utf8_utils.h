/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef utf8_utils_h
#define utf8_utils_h

#ifdef UNICODE

//****************************************************************************
class _CUTF8String
{
    char        *m_buffer;
    int         m_length;

public:
    _CUTF8String( const wchar_t* aStr );
    ~_CUTF8String( );

    char        *c_str( );
    int         GetLength( )    { return m_length; }

    operator    char*( );
};
#define CUTF8String(x) (char*)_CUTF8String(x)


//****************************************************************************
class _CUNICODEString
{
    wchar_t     *m_buffer;
    int         m_length;

public:
    _CUNICODEString( const char    *aStr );
    ~_CUNICODEString( );

    wchar_t     *c_str( );
    int         GetLength( )    { return m_length; }

    operator    wchar_t*( );
};
#define CUNICODEString(x) (wchar_t*)_CUNICODEString(x)


//****************************************************************************
class _CUNICODEStringFromWide
{
    wchar_t     *m_buffer;
    int         m_length;

    public:
        _CUNICODEStringFromWide( const wchar_t *aStr );
        ~_CUNICODEStringFromWide( );

        wchar_t     *c_str( );
        int         GetLength( )    { return m_length; }

        operator    wchar_t*( );
};
#define CUNICODEStringFromWide(x) (wchar_t*)_CUNICODEStringFromWide(x)


//****************************************************************************
class _CMBCSToWideNoXlate
{
    wchar_t     *m_buffer;
    int         m_length;

    public:
        _CMBCSToWideNoXlate( const char *aStr );
        ~_CMBCSToWideNoXlate( );

        wchar_t     *c_str( );
        int         GetLength( )    { return m_length; }

        operator    wchar_t*( );
};
#define CMBCSToWideNoXlate(x) (wchar_t*)_CMBCSToWideNoXlate(x)

#else
#define CUTF8String(x) x
#define CUNICODEString(x) x
#define CUNICODEStringFromWide(x) x
#define CMBCSToWideNoXlate(x) x
#endif

#endif
