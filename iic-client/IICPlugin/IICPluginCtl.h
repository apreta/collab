#if !defined(AFX_IICPLUGINCTL_H__CE9CC488_A7BD_4208_B346_90B882DC47DA__INCLUDED_)
#define AFX_IICPLUGINCTL_H__CE9CC488_A7BD_4208_B346_90B882DC47DA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifdef L_IMPL_OBJECTSAFETY
#include <objsafe.h>
#endif// L_IMPL_OBJECTSAFETY

#include "ProgressListener.h"
#include "MeetingMessageController.h"
#include "InstallController.h"

// IICPluginCtl.h : Declaration of the CIICPluginCtrl ActiveX Control class.

/////////////////////////////////////////////////////////////////////////////
// CIICPluginCtrl : See IICPluginCtl.cpp for implementation.

class CIICPluginCtrl : public COleControl
{
	DECLARE_DYNCREATE(CIICPluginCtrl)

// Constructor
public:
	CIICPluginCtrl();
	bool SetProgress(int percent);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIICPluginCtrl)
	public:
	virtual void OnDraw(CDC* pdc, const CRect& rcBounds, const CRect& rcInvalid);
	virtual void DoPropExchange(CPropExchange* pPX);
	virtual void OnResetState();
	//}}AFX_VIRTUAL

// Implementation
protected:
	~CIICPluginCtrl();

	DECLARE_OLECREATE_EX(CIICPluginCtrl)    // Class factory and guid
	DECLARE_OLETYPELIB(CIICPluginCtrl)      // GetTypeInfo
	DECLARE_PROPPAGEIDS(CIICPluginCtrl)     // Property page IDs
	DECLARE_OLECTLTYPE(CIICPluginCtrl)		// Type name and misc status

// Message maps
	//{{AFX_MSG(CIICPluginCtrl)
//	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

public:
#ifdef L_IMPL_OBJECTSAFETY
	BEGIN_INTERFACE_PART(ObjectSafety, IObjectSafety)
		STDMETHOD(GetInterfaceSafetyOptions)(REFIID riid, DWORD __RPC_FAR *pdwSupportedOptions, DWORD __RPC_FAR *pdwEnabledOptions);
		STDMETHOD(SetInterfaceSafetyOptions)(REFIID riid, DWORD dwOptionSetMask, DWORD dwEnabledOptions);
	END_INTERFACE_PART(ObjectSafety)

	DECLARE_INTERFACE_MAP();
#endif // L_IMPL_OBJECTSAFETY

	// Dispatch maps
	//{{AFX_DISPATCH(CIICPluginCtrl)
	CString m_screenName;
	afx_msg void OnScreenNameChanged();
	CString m_host;
	afx_msg void OnHostChanged();
	CString m_topic;
	afx_msg void OnTopicChanged();
	afx_msg BSTR GetMeetingURL();
	afx_msg void SetMeetingURL(LPCTSTR lpszNewValue);
	afx_msg BSTR GetInstallerURL();
	afx_msg void SetInstallerURL(LPCTSTR lpszNewValue);
	afx_msg BSTR GetMeetingID();
	afx_msg void SetMeetingID(LPCTSTR lpszNewValue);
	afx_msg BSTR GetParticipantID();
	afx_msg void SetParticipantID(LPCTSTR lpszNewValue);
	afx_msg BSTR GetParticipant();
	afx_msg void SetParticipant(LPCTSTR lpszNewValue);
	afx_msg BSTR GetMeetingTime();
	afx_msg void SetMeetingTime(LPCTSTR lpszNewValue);
	afx_msg BSTR GetServer();
	afx_msg void SetServer(LPCTSTR lpszNewValue);
	afx_msg BSTR GetPhone();
	afx_msg void SetPhone(LPCTSTR lpszNewValue);
	afx_msg BSTR GetEmail();
	afx_msg void SetEmail(LPCTSTR lpszNewValue);
	afx_msg BSTR GetToken();
	afx_msg void SetToken(LPCTSTR lpszNewValue);
	//}}AFX_DISPATCH
	DECLARE_DISPATCH_MAP()

// Event maps
	//{{AFX_EVENT(CIICPluginCtrl)
	//}}AFX_EVENT
	DECLARE_EVENT_MAP()

// Dispatch and event IDs
public:
	enum {
		dispidStartMeeting = 15L,		dispidServerToken = 14L,		dispidInstallTest = 13,	//{{AFX_DISP_ID(CIICPluginCtrl)
	dispidMeetingURL = 4L,
	dispidInstallerURL = 5L,
	dispidMeetingID = 6L,
	dispidParticipant = 8L,
	dispidMeetingTime = 9L,
	dispidServer = 10L,
	dispidScreenName = 1L,
	dispidHost = 2L,
	dispidTopic = 3L,
	dispidParticipantID = 7L,
	dispidPhone = 11L,
	dispidEmail = 12L,
	//}}AFX_DISP_ID
	};

private:
	CString m_meetingURL;
	CString m_installerURL;
	CString m_meetingID;
    CString m_participantID;
	CString m_participant;
	CString m_meetingTime;
	CString m_server;
    CString m_phone;
    CString m_email;
    CString m_token;

	BOOL m_firstTime;
	BOOL m_dataLoaded;

	CMeetingMessageController m_msgController;
	CInstallController m_installController;

	void ActivateClient();
	CString ReadServerToken();
	CString ClientLocationFromRegistry();

protected:
	BSTR GetInstallTest(void);
	void SetInstallTest(LPCTSTR newVal);
	BSTR ServerToken(void);
	VARIANT_BOOL StartMeeting(void);
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IICPLUGINCTL_H__CE9CC488_A7BD_4208_B346_90B882DC47DA__INCLUDED)
