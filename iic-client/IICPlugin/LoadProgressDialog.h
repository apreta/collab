#if !defined(AFX_LOADPROGRESSDIALOG_H__9DA52407_3822_47D4_A8F6_2010C703D493__INCLUDED_)
#define AFX_LOADPROGRESSDIALOG_H__9DA52407_3822_47D4_A8F6_2010C703D493__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// LoadProgressDialog.h : header file
//

#include "InstallController.h"
#include "ProgressListener.h"

/////////////////////////////////////////////////////////////////////////////
// CLoadProgressDialog dialog

class CLoadProgressDialog : public CDialog, IProgressListener
{
// Construction
public:
	CLoadProgressDialog(CInstallController* installController,
						CString installURL,
						CWnd* pParent = NULL     // standard constructor
						);

// Dialog Data
	//{{AFX_DATA(CLoadProgressDialog)
	enum { IDD = IDD_DIALOG_LOAD_PROGRESS };
	CProgressCtrl	m_progress;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLoadProgressDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	bool SetProgress(int percent);
	void SetComplete();

public:
	bool WasCancel() { return m_wasCancel; }

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CLoadProgressDialog)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

private:
	CInstallController* m_installController;
	CString m_installURL;
	bool m_wasCancel;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOADPROGRESSDIALOG_H__9DA52407_3822_47D4_A8F6_2010C703D493__INCLUDED_)
