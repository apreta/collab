// IICPluginPpg.cpp : Implementation of the CIICPluginPropPage property page class.

#include "stdafx.h"
#include "IICPlugin.h"
#include "IICPluginPpg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNCREATE(CIICPluginPropPage, COlePropertyPage)


/////////////////////////////////////////////////////////////////////////////
// Message map

BEGIN_MESSAGE_MAP(CIICPluginPropPage, COlePropertyPage)
	//{{AFX_MSG_MAP(CIICPluginPropPage)
	// NOTE - ClassWizard will add and remove message map entries
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// Initialize class factory and guid

IMPLEMENT_OLECREATE_EX(CIICPluginPropPage, "IICPLUGIN.IICPluginPropPage.1",
	0x44d8bc2c, 0x6023, 0x4647, 0x9c, 0xba, 0x1, 0xf4, 0xdc, 0x50, 0x7f, 0x1d)


/////////////////////////////////////////////////////////////////////////////
// CIICPluginPropPage::CIICPluginPropPageFactory::UpdateRegistry -
// Adds or removes system registry entries for CIICPluginPropPage

BOOL CIICPluginPropPage::CIICPluginPropPageFactory::UpdateRegistry(BOOL bRegister)
{
	if (bRegister)
		return AfxOleRegisterPropertyPageClass(AfxGetInstanceHandle(),
			m_clsid, IDS_IICPLUGIN_PPG);
	else
		return AfxOleUnregisterClass(m_clsid, NULL);
}


/////////////////////////////////////////////////////////////////////////////
// CIICPluginPropPage::CIICPluginPropPage - Constructor

CIICPluginPropPage::CIICPluginPropPage() :
	COlePropertyPage(IDD, IDS_IICPLUGIN_PPG_CAPTION)
{
	//{{AFX_DATA_INIT(CIICPluginPropPage)
	// NOTE: ClassWizard will add member initialization here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_INIT
}


/////////////////////////////////////////////////////////////////////////////
// CIICPluginPropPage::DoDataExchange - Moves data between page and properties

void CIICPluginPropPage::DoDataExchange(CDataExchange* pDX)
{
	//{{AFX_DATA_MAP(CIICPluginPropPage)
	// NOTE: ClassWizard will add DDP, DDX, and DDV calls here
	//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_DATA_MAP
	DDP_PostProcessing(pDX);
}


/////////////////////////////////////////////////////////////////////////////
// CIICPluginPropPage message handlers
