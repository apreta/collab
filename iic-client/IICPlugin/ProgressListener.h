// ProgressAdvancer.h: interface for the IProgressAdvancer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IPROGRESSADVANCER_H__35C1710E_69CD_4DCD_9785_F2749F1BE6F2__INCLUDED_)
#define AFX_IPROGRESSADVANCER_H__35C1710E_69CD_4DCD_9785_F2749F1BE6F2__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class IProgressListener
{
public:
	IProgressListener();
	virtual ~IProgressListener();

	// set the progress to a percent complete
	// the return code indicates if the operation should continue
	virtual bool SetProgress(int percent) = 0;

	// set the progress to 100 percent complete
	virtual void SetComplete() = 0;
};

#endif // !defined(AFX_IPROGRESSADVANCER_H__35C1710E_69CD_4DCD_9785_F2749F1BE6F2__INCLUDED_)
