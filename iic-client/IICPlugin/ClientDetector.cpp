// ClientDetector.cpp: implementation of the CClientDetector class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientDetector.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CClientDetector::CClientDetector()
{
}

CClientDetector::~CClientDetector()
{

}

// retrieves a pointer to the IIC client (if found) and 
// brings the client to the front
HWND CClientDetector::FindRunningClient(CString screenName)
{
    HWND pWndPrev = NULL;
	 HWND pWndChild = NULL;
	 BOOL clientFound = FALSE;

     // ToDo: figure this out :-)
	 const char* loginTitle = "zunknown"; //CLIENT_LOGIN_TITLE_STR;  // defined in common.h
     const char* className = "zunknown"; //IIC_MAIN_CLASS_NAME;

	 // if login window is displayed, we're going to send the message to 
	 // that window
	 if (pWndPrev = ::FindWindowA(NULL, loginTitle))
	 {
		 clientFound = TRUE;
	 }

	 // not found - search for main IIC class
	 else if (pWndPrev = ::FindWindowA(className, NULL))
	 {
		 clientFound = TRUE;
	 }

	 if (clientFound)
	 {
		// if found, bring to front

        // Does it have any popups?
		pWndChild = ::GetLastActivePopup(pWndPrev);

        // If iconic, restore the main window
        if (::IsIconic(pWndPrev))
			::ShowWindow(pWndPrev, SW_RESTORE);

        // Bring the main window or its popup to the foreground
        ::SetForegroundWindow(pWndChild);

        // and you are done activating the other application
        return pWndChild;
     }

	 return NULL;
}


HWND CClientDetector::LaunchClient(const CString& meetingID,
                                   const CString& participantID,
								   const CString& screenName, 
								   const CString& server,
        		                   const CString& phone,
					               const CString& email,
					               const CString& fullname)
{
    const CString DELIM = " ";
    const CString QT = "\"";
	CString command = CLIENT_PATH_STR;
	command += DELIM + QT + meetingID + QT + DELIM + QT + participantID + QT + DELIM + QT + screenName + QT + DELIM + QT + server + QT;
    command += DELIM + QT + phone + QT + DELIM + QT + email + QT + DELIM + QT + fullname + QT;

	return LaunchClient(command);
}


// launches the client, returning a handle to the window
HWND CClientDetector::LaunchClient(const CString& commandLine)
{
    HANDLE hProcess = NULL;
    PROCESS_INFORMATION processInfo;
    STARTUPINFOA startupInfo;
    
	::ZeroMemory(&startupInfo, sizeof(startupInfo));
    startupInfo.cb = sizeof(startupInfo);

    char    *cTemp    = NULL;
#ifdef UNICODE
    //  Convert the CString from Unicode to UTF-8

    int     iLen = WideCharToMultiByte( CP_UTF8, 0, commandLine, -1, cTemp, 0, NULL, NULL );
    if( iLen  >  0 )
    {
        cTemp = new char[ iLen + 1 ];
        WideCharToMultiByte( CP_UTF8, 0, commandLine, -1, cTemp, iLen + 1, NULL, NULL );
    }

#else

    cTemp = new char[ commandLine.GetLength( ) + 1 ];
    strcpy( cTemp, (char*) (LPCTSTR) commandLine );

#endif
	
    if(::CreateProcessA(NULL, 
					   cTemp,  // command line (includes program name)
                       NULL,  // process security
                       NULL,  // thread security
                       FALSE, // no inheritance
                       0,     // no startup flags
                       NULL,  // no special environment
                       NULL,  // default startup directory
                       &startupInfo,
                       &processInfo))
    {
        hProcess = processInfo.hProcess;
    }

    free( cTemp );

    // Close process and thread handles. 
    CloseHandle( processInfo.hProcess );
    CloseHandle( processInfo.hThread );

	return (HWND)hProcess;
}
