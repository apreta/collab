;; imidio.nsi
;;
;; Copyright (c) 2005 SiteScape, Inc.
;; 
;; This script installs Imidio's client.  It remembers the directory, 
;; has uninstall support and installs start menu shortcuts.
;;
;; It will install <main exec> into c:/Program Files/imidio
;;
;; This script runs silently and does not automatically launch after install.  It presents
;; confirmation dialogs if IIC needs to be killed prior to install (or uninstall).
;;
;;

!include "WinMessages.nsh"
!include "UpgradeDLL.nsh"  ;; include this when we need to worry about different dll versions
                           ;; requires nsis2.0rc2 or greater

;; common functions
!include "include.nsi"

; The name of the installer
Name "Zon"

; The file to write
OutFile "imidio.exe"

; Make installation Silent
SilentInstall silent

; set compressor to best compression
SetCompressor lzma

; defines files that are included in installer, registry settings
; Note: must be included after SetCompressor is called
!include "common.nsi"

; The default installation directory
InstallDir $PROGRAMFILES\Imidio

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM SOFTWARE\Imidio_Client "Install_Dir"

;; NOTE: Install and Uninstall Sections are defined in "common.nsi"
;; Unique sections for this installer should be defined below.
