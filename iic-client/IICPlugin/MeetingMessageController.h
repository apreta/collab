// CMeetingMessageController.h: interface for the CMeetingMessageController class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CMeetingMessageController_H__ED66FFBE_9791_4382_BD3E_7C42704033FF__INCLUDED_)
#define AFX_CMeetingMessageController_H__ED66FFBE_9791_4382_BD3E_7C42704033FF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ClientDetector.h"
#include "InstallController.h"

//////////////////////////////////////////////////////////////////////////
// Controller for the process of getting meeting info messages via an URL.
// Ensures that the IIC client is installed and runs the installer if not.
// Ensures that the IIC client is running.
// Communicates meeting messages to the client.
//////////////////////////////////////////////////////////////////////////
class CMeetingMessageController  
{
public:
	CMeetingMessageController();
	virtual ~CMeetingMessageController();

	// initializes the controller
	void Initialize(const CString& meetingID,
                    const CString& participantID,
					const CString& screenName,
					const CString& server,
					const CString& phone,
					const CString& email,
					const CString& fullname,
                    CInstallController* installController);

	// activates the client and sends a meeting message to it
	BOOL ActivateClient();

private:
	BOOL ProcessMeetingMessage(HWND clientWindow);

	CClientDetector m_clientDetector;
	CInstallController* m_installController;

	CString m_meetingID;
	CString m_participantID;
	CString m_screenName;
	CString m_server;
	CString m_phone;
	CString m_email;
	CString m_fullname;
};

#endif // !defined(AFX_CMeetingMessageController_H__ED66FFBE_9791_4382_BD3E_7C42704033FF__INCLUDED_)
