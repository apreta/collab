/** This header file is used by the crashlib library to produce crash
 ** logs. This header is alos used by the ccrash reporter file parser.
 **/

#define CRASHLIB_MAPNAME ".MAPNAME"
#define CRASHLIB_MAPPATH ".MAPPATH"
#define CRASHLIB_DATE	 ".DATE"
#define CRASHLIB_NAME    ".NAME"
#define CRASHLIB_EXCEPTION ".EXCEPTION"
#define CRASHLIB_LOCATION ".LOCATION"
#define CRASHLIB_READINGLOC ".READING_LOCATION"
#define CRASHLIB_WRITINGLOC ".WRITING_LOCATION"
#define CRASHLIB_REGISTERS ".REGISTERS"
#define CRASHLIB_CONTEXTFLAGS ".CONTEXT_FLAGS"
#define CRASHLIB_CONTEXTSEGS ".CONTEXT_SEGS"
#define CRASHLIB_CALLSTACK ".CALLSTACK"
