/* (c)2004 imidio all rights reserved */

/*
 * sync.h - code to provide shared memory access
 */
#include <sys/types.h>

#ifndef bool
#define bool int
#define true 1
#define false 0
#endif 

#define SHM_SIZE    1024
#define RESERVED_SIZE 14

#ifndef key_t
#define key_t unsigned int
#endif

bool opensem(int *sid, key_t key);
bool createsem(int *sid, key_t key);
bool locksem(int sid);
bool unlocksem(int sid);
void removesem(int sid);

bool openshm(int *shmid, char  **segptr, key_t key);
bool writeshm(int shmid, char *segptr, const char *mid, char cmd, const char * message);
bool readshm(int shmid, char *segptr, const char* mid, int *cmd, char * message);
void removeshm(int shmid);
void writepid(int shmid, char *segptr, int pid);
void readpid(int shmid, char *segptr, int* pid);
