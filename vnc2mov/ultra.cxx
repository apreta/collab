//  Copyright (C) 2000 Tridia Corporation. All Rights Reserved.
//  Copyright (C) 1999 AT&T Laboratories Cambridge. All Rights Reserved.
//
//  This file is part of the VNC system.
//
//  The VNC system is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// TightVNC distribution homepage on the Web: http://www.tightvnc.com/
//
// If the source code for the VNC system is not available from the place 
// whence you received this file, check http://www.uk.research.att.com/vnc or contact
// the authors on vnc@uk.research.att.com for information on obtaining it.

// ZLIB Encoding
//
// The bits of the ClientConnection object to do with zlib.
extern "C"
{
	#include "vncviewer.h"
}

#include "minilzo.h"

unsigned char *m_zipbuf;
int m_zipbufsize;
int m_minPixelBytes=1;

void ReadUltraRect(rfbFramebufferUpdateRectHeader *pfburh) {

	UINT numpixels = pfburh->r.w * pfburh->r.h;
    // this assumes at least one byte per pixel. Naughty.
	UINT numRawBytes = numpixels * m_minPixelBytes;
	UINT numCompBytes;
	UINT new_len;

	rfbZlibHeader hdr;

	// Read in the rfbZlibHeader
	if (!ReadExact((char *)&hdr, sz_rfbZlibHeader)) return;

	numCompBytes = Swap32IfLE(hdr.nBytes);

	// Read in the compressed data
    CheckBufferSize(numCompBytes);
#ifdef BLOWFISH
	if (!ReadEncrypted(m_netbuf, numCompBytes)) return;
#else
	if (!ReadExact(m_netbuf, numCompBytes)) return;
#endif

	CheckZlibBufferSize(numRawBytes);
	lzo1x_decompress((BYTE*)m_netbuf,numCompBytes,(BYTE*)m_zlibbuf,&new_len,NULL);
//	SoftCursorLockArea(pfburh->r.x, pfburh->r.y,pfburh->r.w,pfburh->r.h);
//	if (!Check_Rectangle_borders(pfburh->r.x, pfburh->r.y,pfburh->r.w,pfburh->r.h)) return;
//	if (m_DIBbits) ConvertAll(pfburh->r.w,pfburh->r.h,pfburh->r.x, pfburh->r.y,m_myFormat.bitsPerPixel/8,(BYTE *)m_zlibbuf,(BYTE *)m_DIBbits,si.framebufferWidth);

}

void ReadUltraZip(rfbFramebufferUpdateRectHeader *pfburh)
{
	UINT nNbCacheRects = pfburh->r.x;
	UINT numRawBytes = pfburh->r.y+pfburh->r.w*65535;
	UINT numCompBytes;
	UINT new_len;
	rfbZlibHeader hdr;
	// Read in the rfbZlibHeader
	if (!ReadExact((char *)&hdr, sz_rfbZlibHeader)) return;
	numCompBytes = Swap32IfLE(hdr.nBytes);
	// Check the net buffer
	CheckBufferSize(numCompBytes);
	// Read the compressed data
#ifdef BLOWFISH
	if (!ReadEncrypted(m_netbuf, numCompBytes)) return;
#else
	if (!ReadExact((char *)m_netbuf, numCompBytes)) return;
#endif

	// Verify buffer space for cache rects list
	CheckZipBufferSize(numRawBytes+500);

	lzo1x_decompress((BYTE*)m_netbuf,numCompBytes,(BYTE*)m_zlibbuf,&new_len,NULL);
	BYTE* pzipbuf = m_zipbuf;
	for (int i = 0 ; i < nNbCacheRects; i++)
	{
		rfbFramebufferUpdateRectHeader surh;
		memcpy((char *) &surh,pzipbuf, sz_rfbFramebufferUpdateRectHeader);
		surh.r.x = Swap16IfLE(surh.r.x);
		surh.r.y = Swap16IfLE(surh.r.y);
		surh.r.w = Swap16IfLE(surh.r.w);
		surh.r.h = Swap16IfLE(surh.r.h);
		surh.encoding = Swap32IfLE(surh.encoding);
		pzipbuf += sz_rfbFramebufferUpdateRectHeader;

		RECT rect;
		rect.left = surh.r.x;
		rect.right = surh.r.x + surh.r.w;
		rect.top = surh.r.y;
		rect.bottom = surh.r.y + surh.r.h;
		//border check
		if (!Check_Rectangle_borders(rect.left,rect.top,surh.r.w,surh.r.h)) return;

//		SoftCursorLockArea(rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top);
		
		 if ( surh.encoding==rfbEncodingRaw)
			{
				UINT numpixels = surh.r.w * surh.r.h;							  
//				if (m_DIBbits) ConvertAll(surh.r.w,surh.r.h,surh.r.x, surh.r.y,m_myFormat.bitsPerPixel/8,(BYTE *)pzipbuf,(BYTE *)m_DIBbits,si.framebufferWidth);
				pzipbuf +=numpixels*myFormat.bitsPerPixel/8;
			}
	}
}

Bool CheckZipBufferSize(int bufsize)
{
	unsigned char *newbuf;

	if (m_zipbufsize > bufsize) return False;

	newbuf = (unsigned char *)new char[bufsize + 256];
	if (newbuf == NULL) {
		return False;
	}

	// Only if we're successful...

	if (m_zipbuf != NULL)
		delete [] m_zipbuf;
	m_zipbuf = newbuf;
	m_zipbufsize = bufsize + 256;
	// vnclog.Print(4, _T("zipbufsize expanded to %d\n"), m_zipbufsize);

	return True;
}

bool Check_Rectangle_borders(int x,int y,int w,int h)
{
	if (x<0) return false;
	if (y<0) return false;
	if (x+w>si.framebufferWidth) return false;
	if (y+h>si.framebufferHeight) return false;
	if (x+w<x) return false;
	if (y+h<y) return false;
	return true;
}
