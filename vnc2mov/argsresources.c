/*
 *  Copyright (C) 2002-2003 RealVNC Ltd.
 *  Copyright (C) 1999 AT&T Laboratories Cambridge.  All Rights Reserved.
 *
 *  This is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this software; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 *  USA.
 */

/*
 * argsresources.c - deal with command-line args and resources.
 */

#include "vncviewer.h"
#include "ffmpeg.h"

#ifndef WIN32
#define stricmp strcasecmp
#endif

/*
 * fallback_resources - these are used if there is no app-defaults file
 * installed in one of the standard places.
 */


/*
 * vncServerHost and vncServerPort are set either from the command line or
 * from a dialog box.
 */

char vncServerHost[256];
int vncServerPort = 0;


/*
 * appData is our application-specific data which can be set by the user with
 * application resource specs.  The AppData structure is defined in the header
 * file.
 */

AppData appData;

/*
 * usage() prints out the usage message.
 */

void
usage()
{
	fprintf(stderr,"\n"
	  "usage: %s [<options>] [-input input.dump] -out <out.swf> -listen <host>:<port#>\n"
	  "\n"
	  "<options> are standard Xt options, or:\n"
	  "              -spool (not to work with input)\n"
	  "              -replay (exclusive with spool)\n"
//	  "              -passwd <passwd>\n"
//	  "              -noauto\n"
//	  "              -encodings <encoding-list> (e.g. \"raw copyrect\")\n"
//	  "              -bgr233\n"
//	  "              -owncmap\n"
//	  "              -truecolour\n"
//	  "              -depth <depth>\n"
	  "              -delay <delay>\n"
	  "              -frames <frames>\n"
	  "              -soundfile <mp3-to-play>\n"
	  "              -framerate <frames-per-sec>\n"
	  "              -nommhack\n"
	  "              -startrecording\n"
	  "              -recordingmethod <0 or 1>\n"
	  "                  -immediately (equivalent to -recordingmethod 0)\n"
	  "                  -buffered (equivalent to -recordingmethod 1)\n"
//	  "              -clippinggeometry <geometry>\n"
	  "              -meetingId <meeting-id>\n"
	  "              -password <password>\n"
	  "              -semkey <semkey>\n"
	  "              -shmkey <shmkey>\n"
	  "              -loglevel <0...6>\n"
	  "              -stdlevel <0...6>\n"
      "              -cipher <key>\n"

	  ,programName);

  exit(1);
}

/*
 * GetArgsAndResources() deals with resources and any command-line arguments
 * not already processed by XtVaAppInitialize().  It sets vncServerHost and
 * vncServerPort and all the fields in appData.
 */
bool SwitchMatch(const char* arg, const char* swtch) {
  return (arg[0] == '-' || arg[0] == '/') &&
    (stricmp(&arg[1], swtch) == 0);
}

void
GetArgsAndResources(int argc, char *args[])
{
  int i, j;
  char vncServerName[256];

  appData.spool = False;
  appData.spool_handle = 0;
  appData.replay = False;
  appData.deleteFile = False;
  appData.preview = False;
  memset(appData.passwordFile, 0, sizeof(appData.passwordFile));
  appData.autoDetect = False;
  memset(appData.encodings, 0, sizeof(appData.encodings));
  appData.useBGR233 = False;
  appData.forceOwnCmap = True;
  appData.forceTrueColour = True;
  appData.requestedDepth = 0;

  /* vnc2swf options */
  memset(appData.inputFile, 0, sizeof(appData.inputFile));
  memset(appData.dumpFile, 0, sizeof(appData.dumpFile));
  memset(appData.soundFile, 0, sizeof(appData.soundFile));
  appData.frameRate = 1;
  appData.macromediaHack = True;
  appData.startRecording = False;
  appData.recordingMethod = 0;	// 0;	// 1 - make default as RECORD_BUFFERED to reduce blocks
  memset(appData.meetingId, 0, sizeof(appData.meetingId));
  memset(appData.password, 0, sizeof(appData.password));
  appData.semkey = 0;
  appData.shmkey = 0;
  appData.loglevel = 6;
  appData.stdlevel = 6;
  appData.delay = 0;
  appData.record_enable_time = 0;
  appData.framesPerMovie = 1800;	// 30 min

  memset(appData.cipher, 0, sizeof(appData.cipher));
  appData.format = FORMAT_FLV_SV;
  appData.fileType = FILE_FLV;
  appData.bitRate = 800000;
  appData.audioBitRate = 48000;

  /* Obtain the output swf filename. */

  if (argc < 2) {// || args[1][0] == '-') {
      usage();
  }

  for (j = 0; j < argc; j++) {
    if ( SwitchMatch(args[j], "help") ||
         SwitchMatch(args[j], "?") ||
         SwitchMatch(args[j], "h")) {
      usage();
      exit(1);
    } else if ( SwitchMatch(args[j], "spool")) {
      appData.spool = True;
    } else if ( SwitchMatch(args[j], "replay")) {
      appData.replay = True;
    } else if ( SwitchMatch(args[j], "deletefile")) {
      appData.deleteFile = True;
    } else if ( SwitchMatch(args[j], "preview")) {
      appData.preview = True;
    } else if ( SwitchMatch(args[j], "passwd")) {
      strcpy(appData.passwordFile, args[++j]);
    } else if ( SwitchMatch(args[j], "noauto")) {
      appData.autoDetect = False;
    } else if ( SwitchMatch(args[j], "encodings" )) {
	  int enc = -1;
	  if (++j == argc) {
		continue;
	  }
      if (stricmp(args[j], "raw") == 0) {
		enc = rfbEncodingRaw;
	  } else if (stricmp(args[j], "rre") == 0) {
		enc = rfbEncodingRRE;
	  } else if (stricmp(args[j], "corre") == 0) {
		enc = rfbEncodingCoRRE;
	  } else if (stricmp(args[j], "hextile") == 0) {
		enc = rfbEncodingHextile;
	  } else if (stricmp(args[j], "zlib") == 0) {
		enc = rfbEncodingZlib;
	  } else if (stricmp(args[j], "zlibhex") == 0) {
		enc = rfbEncodingZlibHex;
	  } else if (stricmp(args[j], "tight") == 0) {
		enc = rfbEncodingTight;
	  } else if (stricmp(args[j], "ultra") == 0) {
		enc = rfbEncodingUltra;
	  } else {
		continue;
	  }

	  strcpy(appData.encodings, args[j]);
    } else if ( SwitchMatch(args[j], "bgr233")) {
      appData.useBGR233 = True;
    } else if ( SwitchMatch(args[j], "owncmap")) {
      appData.forceOwnCmap = True;
    } else if ( SwitchMatch(args[j], "truecolour")) {
      appData.forceTrueColour = True;
    } else if ( SwitchMatch(args[j], "depth")) {
      if (++j == argc) {
        continue;
      }
      if (sscanf(args[j], "%d", &appData.requestedDepth) != 1) {
        continue;
      }
    } else if ( SwitchMatch(args[j], "soundfile" )) {
      if (++j == argc)
      {
        continue;
      }
      strcpy(appData.soundFile, args[j]);
    } else if ( SwitchMatch(args[j], "framerate")) {
      if (++j == argc) {
        continue;
      }
      if (sscanf(args[j], "%d", &appData.frameRate) != 1) {
        continue;
      }
    } else if ( SwitchMatch(args[j], "nommhack")) {
      appData.macromediaHack = False;
    } else if ( SwitchMatch(args[j], "startrecording")) {
      appData.startRecording = True;
    } else if ( SwitchMatch(args[j], "recordingmethod")) {
      if (++j == argc) {
        continue;
      }
      if (sscanf(args[j], "%d", &appData.recordingMethod) != 1) {
        continue;
      }
    } else if ( SwitchMatch(args[j], "meetingid"))	{
	if (++j == argc) {
		continue;
	}
	strcpy(appData.meetingId, args[j]);
    } else if ( SwitchMatch(args[j], "password")) {
	if (++j == argc)
	{
		continue;
	}
	strcpy(appData.password, args[j]);
    } else if ( SwitchMatch(args[j], "semkey")) {
      if (++j == argc) {
        continue;
      }
      if (sscanf(args[j], "%d", &appData.semkey) != 1) {
        continue;
      }
    } else if ( SwitchMatch(args[j], "shmkey" )) {
      if (++j == argc) {
        continue;
      }
      if (sscanf(args[j], "%d", &appData.shmkey) != 1) {
        continue;
      }
    } else if ( SwitchMatch(args[j], "loglevel")) {
      if (++j == argc) {
        continue;
      }
      if (sscanf(args[j], "%d", &appData.loglevel) != 1) {
        continue;
      }
    } else if ( SwitchMatch(args[j], "stdlevel")) {
      if (++j == argc) {
        continue;
      }
      if (sscanf(args[j], "%d", &appData.stdlevel) != 1) {
        continue;
      }
    } else if ( SwitchMatch(args[j], "delay")) {
      if (++j == argc) {
        continue;
      }
      if (sscanf(args[j], "%d", &appData.delay) != 1) {
        continue;
      }
    } else if ( SwitchMatch(args[j], "recordtime")) {
      if (++j == argc) {
        continue;
      }
      if (sscanf(args[j], "%d", &appData.record_enable_time) != 1) {
        continue;
      }
    } else if ( SwitchMatch(args[j], "frames")) {
      if (++j == argc) {
        continue;
      }
      if (sscanf(args[j], "%d", &appData.framesPerMovie) != 1) {
        continue;
      }
    } else if ( SwitchMatch(args[j], "cipher"))	{
      if (++j == argc) {
        continue;
      }
      strcpy(appData.cipher, args[j]);
    } else if ( SwitchMatch(args[j], "input"))	{
		if (++j == argc) {
			continue;
		}
		strcpy(appData.inputFile, args[j]);
    } else if ( SwitchMatch(args[j], "out"))	{
		if (++j == argc) {
			continue;
		}
		strcpy(appData.dumpFile, args[j]);
    } else if ( SwitchMatch(args[j], "listen"))	{
		if (++j == argc) {
			continue;
		}
		strcpy(vncServerName, args[j]);
	} else if ( SwitchMatch(args[j], "format")) {
		if (++j == argc) {
			continue;
		}
        if (strcmp(args[j], "flv_sv") == 0)
            appData.format = FORMAT_FLV_SV;
        else if (strcmp(args[j], "flv") == 0)
            appData.format = FORMAT_FLV_VID;
        else if (strcmp(args[j], "h264") == 0)
            appData.format = FORMAT_H264;
        printf("format %d\n", appData.format);
	} else if ( SwitchMatch(args[j], "container")) {
		if (++j == argc) {
			continue;
		}
        if (strcmp(args[j], "flv") == 0)
            appData.fileType = FILE_FLV;
        else if (strcmp(args[j], "mp4") == 0)
            appData.fileType = FILE_MP4;
        printf("outfile type %d\n", appData.fileType);
    } else if ( SwitchMatch(args[j], "bitrate"))	{
		if (++j == argc) {
			continue;
		}
		appData.bitRate = atoi(args[j]);
	} else if ( SwitchMatch(args[j], "audiorate"))	{
		if (++j == argc) {
			continue;
		}
		appData.audioBitRate = atoi(args[j]);
	}
  }

  if (appData.fileType == FILE_MP4 && appData.format != FORMAT_H264) {
      log_write(LL_ERROR, "Must use H264 video format for MP4 output\n");
      exit(-1);
  }

  if (appData.spool && *appData.inputFile) {
      log_write(LL_ERROR, "Cannot load from a file when spooling\n");
      exit(-1);
  }

  if ((appData.replay||appData.preview) && *appData.inputFile==0) {
      log_write(LL_ERROR, "Replaying/Previewing requires input file to retrieve server data.\n");
      exit(-1);
  }

  if (appData.spool && (appData.replay||appData.preview)) {
      log_write(LL_ERROR, "Spooling and replaying are exclusive\n");
      exit(-1);
  }

  if (*vncServerName!=0) {
	  for (i = 0; vncServerName[i] != ':' && vncServerName[i] != 0; i++);

	  strncpy(vncServerHost, vncServerName, i);
	  vncServerHost[i] = 0;

	  if (vncServerName[i] == ':') {
		vncServerPort = atoi(&vncServerName[i+1]);
	  } else {
		vncServerPort = 0;
	  }

	  if (vncServerPort < 100)
		vncServerPort += SERVER_PORT_OFFSET;
  } else {		// get data from dump file
      appData.semkey = 0;
      appData.shmkey = 0;
  }
}

