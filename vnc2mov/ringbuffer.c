
#include <stdlib.h>
#include "ringbuffer.h"

// Must be power of 2
#define RING_BUFFER_SIZE 4*32768

struct _RingBuffer {
    unsigned int writeIndex;
    unsigned int readIndex;
    unsigned int sizeOfBuffer;
    short data[RING_BUFFER_SIZE];
};

RingBuffer *createBuffer() {
    // Initialize the ring buffer
    RingBuffer *ringBuffer = (RingBuffer *)calloc(1, sizeof(RingBuffer));
    ringBuffer->sizeOfBuffer = RING_BUFFER_SIZE;
    ringBuffer->writeIndex = 0;
    ringBuffer->readIndex = 0;

    return ringBuffer;
}

void clearRingBuffer(RingBuffer *ringBuffer)
{
    ringBuffer->writeIndex = 0;
    ringBuffer->readIndex = 0;
}

int samplesInBuffer(RingBuffer *buffer) {
    if (buffer->writeIndex >= buffer->readIndex)
        return buffer->writeIndex - buffer->readIndex;
    return buffer->sizeOfBuffer - (buffer->readIndex - buffer->writeIndex);
}

// A little function to write into the buffer
void writeIntoBuffer(RingBuffer *ringBuffer, short *myData, int numsamples) {
    // -1 for our binary modulo in a moment
    int buffLen = ringBuffer->sizeOfBuffer - 1;
    int lastWrittenSample = ringBuffer->writeIndex;

    int idx, i;
    for (i=0; i < numsamples; ++i) {
        // modulo will automagically wrap around our index
        idx = (i + lastWrittenSample) & buffLen;
        ringBuffer->data[idx] = myData[i];
    }

    // Update the current index of our ring buffer.
    ringBuffer->writeIndex += numsamples;
    ringBuffer->writeIndex &= buffLen;
}

// A little function to read from the buffer
void readFromBuffer(RingBuffer *ringBuffer, short *myData, int numsamples) {
    // -1 for our binary modulo in a moment
    int buffLen = ringBuffer->sizeOfBuffer - 1;
    int lastReadSample = ringBuffer->readIndex;

    int idx, i;
    for (i=0; i < numsamples; ++i) {
        // modulo will automagically wrap around our index
        idx = (i + lastReadSample) & buffLen;
        myData[i] = ringBuffer->data[idx];
    }

    // Update the current index of our ring buffer.
    ringBuffer->readIndex += numsamples;
    ringBuffer->readIndex &= buffLen;
}

