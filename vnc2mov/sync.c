/* (c)2004 imidio all rights reserved */

/*
 * shm.cpp - code to provide shared memory access
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#ifndef WIN32
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#endif
#include "sync.h"
#include "logging.h"

bool opensem(int *sid, key_t key)
{
#ifndef WIN32
	/* Open the semaphore set - do not create! */
	if((*sid = semget(key, 0, 0666)) == -1) 
	{
		log_write(LL_ERROR, "Semaphore set does not exist!\n");
		return false;
	}
#endif
	return true;
}

bool createsem(int *sid, key_t key)
{
#ifndef WIN32
	if((*sid = semget(key, 1, IPC_CREAT|IPC_EXCL|0666)) == -1) 
	{
		log_write(LL_DEBUG, "Semaphore set already exists!\n");
		if((*sid = semget(key, 0, 0666)) == -1) 
		{
			log_write(LL_ERROR, "Semaphore set does not exist!\n");
			return false;
		}
	}

	/* Initialize all member */        
	semctl(*sid, 0, SETVAL, 1);
#endif

	log_write(LL_DEBUG, "Created Semaphore %d\n", key);
	return true;
}

bool locksem(int sid)
{
#ifndef WIN32
	struct sembuf sem_lock={ 0, -1, 0};//IPC_WAIT};

	if((semop(sid, &sem_lock, 1)) == -1)
	{
		log_write(LL_ERROR, "Lock failed\n");
		return false;
	}
#endif
	return true;
}

bool unlocksem(int sid)
{
#ifndef WIN32
	struct sembuf sem_unlock={ 0, 1, 0};//IPC_WAIT};

	/* Attempt to lock the semaphore set */
	if((semop(sid, &sem_unlock, 1)) == -1)
	{
			log_write(LL_ERROR, "Unlock failed\n");
			return false;
	}
#endif
	return true;
}

void removesem(int sid)
{
#ifndef WIN32
	semctl(sid, 0, IPC_RMID, 0);
	log_write(LL_DEBUG, "Semaphore removed\n");
#endif
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool openshm(int *shmid, char  **segptr, key_t key)
{
#ifndef WIN32
    bool create_new = false;
    if((*shmid = shmget(key, SHM_SIZE, IPC_CREAT|IPC_EXCL|0666)) == -1) 
    {
        log_write(LL_DEBUG, "Shared memory segment exists - opening as client\n");

        /* Segment probably already exists - try as a client */
        if((*shmid = shmget(key, SHM_SIZE, 0)) == -1) 
        {
            log_write(LL_ERROR, "shmget");
            return false;
        }
    }
    else
    {
        log_write(LL_DEBUG, "Creating new shared memory segment, %d\n", key);
        create_new = true;
    }

    /* Attach (map) the shared memory segment into the current process */
    *segptr = (char*)shmat(*shmid, 0, 0);        
    if((int)*segptr == -1)
    {
        log_write(LL_ERROR, "shmat");
        return false;
    }        
        
    if (create_new) {
        memset(*segptr, 0, SHM_SIZE);
    }
#endif
    return true;
}

bool writeshm(int shmid, char *segptr, const char *mid, char cmd, const char * message)
{
#ifndef WIN32
    if (segptr[0]==0) {
        segptr[0] = 1;        // mark it unread
        segptr[1] = cmd;      
        strcpy(segptr+RESERVED_SIZE, mid);
        if (message!=NULL) {
           strncpy(segptr + strlen(mid) + RESERVED_SIZE+1, message, SHM_SIZE- strlen(mid) - RESERVED_SIZE -2);
        }        
        return true;
    }
    return false;
}

bool readshm(int shmid, char *segptr, const char* mid, int *cmd, char * message)
{
    if (segptr[0]==0) {     // cmd already read
        return false;
    }
      
    if (strcmp(segptr+RESERVED_SIZE, mid)!=0) { // not for this mid
        return false;
    }
      
    *cmd = segptr[1];
    segptr[0] = 0;  // mark it read
        
    log_write(LL_DEBUG, "Read cmd for meeting: %s\n", segptr+RESERVED_SIZE);
    if (message!=NULL) {
        strcpy(message, segptr+strlen(mid)+RESERVED_SIZE+1);
    }
#endif
	
    return true;
}

void removeshm(int shmid)
{
#ifndef WIN32
	shmctl(shmid, IPC_RMID, 0);
	log_write(LL_DEBUG, "Shared memory segment marked for deletion\n");
#endif
}

void writepid(int shmid, char *segptr, int pid)
{
    memcpy(segptr+2, (char*)&pid, 4);
}

void readpid(int shmid, char *segptr, int * pid)
{
    *pid = *(int*)(segptr+2);
}

void writemoviesize(int shmid, char *segptr, int w, int h)
{
    memcpy(segptr+6, (char*)&w, 4);
    memcpy(segptr+10, (char*)&h, 4);
}
