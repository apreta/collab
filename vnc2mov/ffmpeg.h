
#ifndef VNC2VID_FFMEG_H
#define VNC2VID_FFMEG_H

#define FORMAT_FLV_SV 1
#define FORMAT_FLV_VID 2
#define FORMAT_H264 3

#define FILE_FLV 1
#define FILE_MP4 2

#ifdef __cplusplus
extern "C" {
#endif

int ffmpeg_init();

int ffmpeg_set_format(int format);

int ffmpeg_set_framerate(int rate);

int ffmpeg_set_bitrate(int vid_rate, int aud_rate);

int ffmpeg_open_output(const char *filename);

int ffmpeg_prepare_stream(int width, int height);

int ffmpeg_write_video(unsigned char *data, int width, int height);

int ffmpeg_write_audio(short *data, int samples);

int ffmpeg_close();

#ifdef __cplusplus
}
#endif

#endif
