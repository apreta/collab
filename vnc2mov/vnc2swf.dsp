# Microsoft Developer Studio Project File - Name="vnc2swf" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=vnc2swf - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "vnc2swf.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "vnc2swf.mak" CFG="vnc2swf - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "vnc2swf - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "vnc2swf - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "vnc2swf - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /I "." /I "./rdr" /I "./rfb" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "vnc2swf - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /I "." /I "./rdr" /I "./rfb" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FD /GZ /c
# SUBTRACT CPP /YX
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "vnc2swf - Win32 Release"
# Name "vnc2swf - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Group "rdr"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\rdr\Exception.h
# End Source File
# Begin Source File

SOURCE=.\rdr\FdInStream.cxx
# End Source File
# Begin Source File

SOURCE=.\rdr\FdInStream.h
# End Source File
# Begin Source File

SOURCE=.\rdr\FdOutStream.cxx
# End Source File
# Begin Source File

SOURCE=.\rdr\FdOutStream.h
# End Source File
# Begin Source File

SOURCE=.\rdr\FixedMemOutStream.h
# End Source File
# Begin Source File

SOURCE=.\rdr\InStream.cxx
# End Source File
# Begin Source File

SOURCE=.\rdr\InStream.h
# End Source File
# Begin Source File

SOURCE=.\rdr\MemInStream.h
# End Source File
# Begin Source File

SOURCE=.\rdr\MemOutStream.h
# End Source File
# Begin Source File

SOURCE=.\rdr\NullOutStream.cxx
# End Source File
# Begin Source File

SOURCE=.\rdr\NullOutStream.h
# End Source File
# Begin Source File

SOURCE=.\rdr\OutStream.h
# End Source File
# Begin Source File

SOURCE=.\rdr\types.h
# End Source File
# Begin Source File

SOURCE=.\rdr\ZlibInStream.cxx
# End Source File
# Begin Source File

SOURCE=.\rdr\ZlibInStream.h
# End Source File
# Begin Source File

SOURCE=.\rdr\ZlibOutStream.cxx
# End Source File
# Begin Source File

SOURCE=.\rdr\ZlibOutStream.h
# End Source File
# End Group
# Begin Group "rfb"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\rfb\d3des.c
# End Source File
# Begin Source File

SOURCE=.\rfb\d3des.h
# End Source File
# Begin Source File

SOURCE=.\rfb\vncauth.c
# End Source File
# Begin Source File

SOURCE=.\rfb\vncauth.h
# End Source File
# Begin Source File

SOURCE=.\rfb\zrleDecode.h
# End Source File
# Begin Source File

SOURCE=.\rfb\zrleEncode.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\argsresources.c
# End Source File
# Begin Source File

SOURCE=.\buildtime.c
# End Source File
# Begin Source File

SOURCE=.\cursor.cxx
# End Source File
# Begin Source File

SOURCE=.\desktop.c
# End Source File
# Begin Source File

SOURCE=.\logging.c
# End Source File
# Begin Source File

SOURCE=.\minilzo.c
# End Source File
# Begin Source File

SOURCE=.\rfbproto.c
# End Source File
# Begin Source File

SOURCE=.\sockets.cxx
# End Source File
# Begin Source File

SOURCE=.\sync.c
# End Source File
# Begin Source File

SOURCE=.\tight.cxx
# End Source File
# Begin Source File

SOURCE=.\ultra.cxx
# End Source File
# Begin Source File

SOURCE=.\vnc2swf.c
# End Source File
# Begin Source File

SOURCE=.\writeswf.c
# End Source File
# Begin Source File

SOURCE=.\zlib.cxx
# End Source File
# Begin Source File

SOURCE=.\zrle.cxx
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\logging.h
# End Source File
# Begin Source File

SOURCE=.\lzoconf.h
# End Source File
# Begin Source File

SOURCE=.\minilzo.h
# End Source File
# Begin Source File

SOURCE=".\rfbproto-realvnc.h"
# End Source File
# Begin Source File

SOURCE=.\rfbproto.h
# End Source File
# Begin Source File

SOURCE=.\stdhdrs.h
# End Source File
# Begin Source File

SOURCE=.\sync.h
# End Source File
# Begin Source File

SOURCE=.\vncviewer.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
