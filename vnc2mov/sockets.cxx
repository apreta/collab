/*
 *  Copyright (C) 2002-2003 RealVNC Ltd.
 *  Copyright (C) 1999 AT&T Laboratories Cambridge.  All Rights Reserved.
 *
 *  This is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this software; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 *  USA.
 */

/*
 * sockets.cxx - functions to deal with sockets.
 */

#ifndef WIN32
#include <unistd.h>
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include <assert.h>
#else
#include <io.h>
#include <winsock2.h>	// IIC

#ifndef VNC_SOCKLEN_T
#define VNC_SOCKLEN_T int
#endif

#endif

extern "C" {
#include "vncviewer.h"
}

#include "rdr/FdInStream.h"
#include "rdr/FdOutStream.h"
#include "rdr/Exception.h"

extern "C" { void PrintInHex(char *buf, int len); }

int rfbsock;
rdr::FdInStream* fis;
rdr::FdOutStream* fos;
Bool sameMachine = False;

static Bool rfbsockReady = False;
extern int totalReadBytes;

/*
 * ConnectToRFBServer.
 */

Bool ConnectToRFBServer(const char *hostname, int port)
{
  int sock = ConnectToTcpAddr(hostname, port);

  if (sock < 0) {
    log_write(LL_ERROR,"Unable to connect to VNC server\n");
    return False;
  }

  return SetRFBSock(sock);
}

void DisconnectRFBServer()
{
    close(rfbsock);
    delete fis;
    delete fos;
    fis = NULL;
    fos = NULL;
}

Bool SetRFBSock(int sock)
{
  try {
    rfbsock = sock;
    fis = new rdr::FdInStream(rfbsock, NULL);
    fos = new rdr::FdOutStream(rfbsock);

    struct sockaddr_in peeraddr, myaddr;
    VNC_SOCKLEN_T addrlen = sizeof(struct sockaddr_in);

    getpeername(sock, (struct sockaddr *)&peeraddr, &addrlen);
    getsockname(sock, (struct sockaddr *)&myaddr, &addrlen);

    sameMachine = (peeraddr.sin_addr.s_addr == myaddr.sin_addr.s_addr);

    return True;
  } catch (rdr::Exception& e) {
    log_write(LL_ERROR,"initialiseInStream: %s\n",e.str());
  }
  return False;
}

void StartTiming()
{
  fis->startTiming();
}

void StopTiming()
{
  fis->stopTiming();
}

int KbitsPerSecond()
{
  //log_write(LL_DEBUG," kbps %d     \r",fis->kbitsPerSecond());
  return fis->kbitsPerSecond();
}

int TimeWaitedIn100us()
{
  return fis->timeWaited();
}

// IIC
Bool ReadExact(char *out, unsigned int n)
{
	return ReadFromRFBServer(out, n);
}

Bool ReadFromRFBServer(char *out, unsigned int n)
{
  try {
    totalReadBytes += n;
    fis->readBytes(out, n);
    log_write(LL_DEBUG,"ReadFromRFBServer: Read %d bytes\n",n);
	spoolFrameLength -= n;	// IIC
    log_write(LL_DEBUG,"ReadFromRFBServer: Bytes left in frame: %d bytes\n",spoolFrameLength);
    return True;
  } catch (rdr::Exception& e) {
    log_write(LL_ERROR,"ReadFromRFBServer: %s\n",e.str());
  }
  return False;
}


/*
 * Write an exact number of bytes, and don't return until you've sent them.
 */

Bool WriteExact(char *buf, int n)
{
	return WriteToRFBServer(buf, n);
}

Bool WriteToRFBServer(char *buf, int n)
{
  try {
    fos->writeBytes(buf, n);
    fos->flush();
    return True;
  } catch (rdr::Exception& e) {
    log_write(LL_ERROR,"WriteExact: %s\n",e.str());
  }
  return False;
}


/*
 * ConnectToTcpAddr connects to the given host and port.
 */

int ConnectToTcpAddr(const char* hostname, int port)
{
  int sock;
  struct sockaddr_in addr;
  int one = 1;
  unsigned int host;

  if (!StringToIPAddr(hostname, &host)) {
    log_write(LL_ERROR,"Couldn't convert '%s' to host address\n", hostname);
    return -1;
  }

  memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = host;

  sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0) {
    log_write(LL_DEBUG,programName);
    log_write(LL_ERROR,": ConnectToTcpAddr: socket");
    return -1;
  }

  if (connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
    log_write(LL_DEBUG,programName);
    log_write(LL_ERROR, ": ConnectToTcpAddr: connect");
    close(sock);
    return -1;
  }

  if (setsockopt(sock, IPPROTO_TCP, TCP_NODELAY,
		 (char *)&one, sizeof(one)) < 0) {
    log_write(LL_DEBUG,programName);
    log_write(LL_ERROR,": ConnectToTcpAddr: setsockopt");
    close(sock);
    return -1;
  }

  return sock;
}



/*
 * ListenAtTcpPort starts listening at the given TCP port.
 */

int ListenAtTcpPort(int port)
{
  int sock;
  struct sockaddr_in addr;
  int one = 1;

  memset(&addr, 0, sizeof(addr));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = INADDR_ANY;

  sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0) {
    log_write(LL_DEBUG,programName);
    log_write(LL_ERROR,": ListenAtTcpPort: socket");
    return -1;
  }

  if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR,
		 (const char *)&one, sizeof(one)) < 0) {
    log_write(LL_DEBUG,programName);
    log_write(LL_ERROR, ": ListenAtTcpPort: setsockopt");
    close(sock);
    return -1;
  }

  if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
    log_write(LL_DEBUG,programName);
    log_write(LL_ERROR, ": ListenAtTcpPort: bind");
    close(sock);
    return -1;
  }

  if (listen(sock, 5) < 0) {
    log_write(LL_DEBUG,programName);
    log_write(LL_ERROR, ": ListenAtTcpPort: listen");
    close(sock);
    return -1;
  }

  return sock;
}


/*
 * AcceptTcpConnection accepts a TCP connection.
 */

int AcceptTcpConnection(int listenSock)
{
  int sock;
  struct sockaddr_in addr;
  VNC_SOCKLEN_T addrlen = sizeof(addr);
  int one = 1;

  sock = accept(listenSock, (struct sockaddr *) &addr, &addrlen);
  if (sock < 0) {
    log_write(LL_DEBUG,programName);
    log_write(LL_ERROR,": AcceptTcpConnection: accept");
    return -1;
  }

  if (setsockopt(sock, IPPROTO_TCP, TCP_NODELAY,
		 (char *)&one, sizeof(one)) < 0) {
    log_write(LL_DEBUG,programName);
    log_write(LL_ERROR,": AcceptTcpConnection: setsockopt");
    close(sock);
    return -1;
  }

  return sock;
}


/*
 * StringToIPAddr - convert a host string to an IP address.
 */

Bool StringToIPAddr(const char *str, unsigned int *addr)
{
  struct hostent *hp;

  if (strcmp(str,"") == 0) {
    *addr = 0; /* local */
    return True;
  }

  *addr = inet_addr(str);

  if (*addr != (unsigned int)-1)
    return True;

  hp = gethostbyname(str);

  if (hp) {
    *addr = *(unsigned int *)hp->h_addr;
    return True;
  }

  return False;
}


/*
 * Print out the contents of a packet for debugging.
 */

void PrintInHex(char *buf, int len)
{
  int i, j;
  char c, str[17];

  str[16] = 0;

  fprintf(stderr,"ReadExact: ");

  for (i = 0; i < len; i++)
    {
      if ((i % 16 == 0) && (i != 0)) {
	fprintf(stderr,"           ");
      }
      c = buf[i];
      str[i % 16] = (((c > 31) && (c < 127)) ? c : '.');
      fprintf(stderr,"%02x ",(unsigned char)c);
      if ((i % 4) == 3)
	fprintf(stderr," ");
      if ((i % 16) == 15)
	{
	  fprintf(stderr,"%s\n",str);
	}
    }
  if ((i % 16) != 0)
    {
      for (j = i % 16; j < 16; j++)
	{
	  fprintf(stderr,"   ");
	  if ((j % 4) == 3) fprintf(stderr," ");
	}
      str[i % 16] = 0;
      fprintf(stderr,"%s\n",str);
    }

  fflush(stderr);
}

// IIC
int ReadNewFBSize(rfbFramebufferUpdateRectHeader *pfburh)
{
	if (pfburh->r.w > 4000 || pfburh->r.h > 3200) {
		log_write(LL_MSG, "Invalid new screen size: %dx%d", pfburh->r.w, pfburh->r.h);
		return -1;
	}

	si.framebufferWidth = pfburh->r.w;
	si.framebufferHeight = pfburh->r.h;

    si.framebufferWidth += si.framebufferWidth % 2;
    si.framebufferHeight += si.framebufferHeight % 2;

	log_write(LL_MSG, "Receiving new size:%dx%d.", si.framebufferWidth, si.framebufferHeight);

    SetMovieSizeEx(si.framebufferWidth, si.framebufferHeight);
    return 0;
}

int EncodingStatusWindow=0,OldEncodingStatusWindow=0;
int m_nTO = 1;
int m_nReadSize=0;
int m_nNetRectBufSize = 0;
BYTE* m_pNetRectBuf = NULL;
//
// Ensures that the temporary "alignement" buffer in large enough
//
void CheckNetRectBufferSize(int nBufSize)
{
	if (m_nNetRectBufSize > nBufSize) return;

	BYTE *newbuf = new BYTE[nBufSize + 256];
	if (newbuf == NULL)
	{
		// Error
	}
	if (m_pNetRectBuf != NULL)
		delete [] m_pNetRectBuf;

	m_pNetRectBuf = newbuf;
	m_nNetRectBufSize = nBufSize + 256;
}

void ReadScreenUpdate(rfbFramebufferUpdateRectHeader* pfburh)
{

	rfbFramebufferUpdateRectHeader surh = *pfburh;

//	HDC hdcX,hdcBits;
//	rfbFramebufferUpdateMsg sut;
//	ReadExact(((char *) &sut)+m_nTO, sz_rfbFramebufferUpdateMsg-m_nTO);
//    sut.nRects = Swap16IfLE(sut.nRects);
//	HRGN UpdateRegion=CreateRectRgn(0,0,0,0);

    //if (sut.nRects == 0) return;  XXX tjr removed this - is this OK?

	for (UINT i=0; i < 1; i++)
	{
//		rfbFramebufferUpdateRectHeader surh;
//		ReadExact((char *) &surh, sz_rfbFramebufferUpdateRectHeader);
//		surh.r.x = Swap16IfLE(surh.r.x);
//		surh.r.y = Swap16IfLE(surh.r.y);
//		surh.r.w = Swap16IfLE(surh.r.w);
//		surh.r.h = Swap16IfLE(surh.r.h);
//		surh.encoding = Swap32IfLE(surh.encoding);
		// vnclog.Print(0, _T("%d %d\n"), i,sut.nRects);

		// Tight - If lastrect if must quit this loop (nRects = 0xFFFF)
		if (surh.encoding == rfbEncodingLastRect)
		{
			log_write(LL_DEBUG,"Last rec received\n");
			break;
		}

		if (surh.encoding == rfbEncodingNewFBSize)
		{
			ReadNewFBSize(&surh);
			break;
		}

		// Tight cursor handling
		if ( surh.encoding == rfbEncodingXCursor ||
			surh.encoding == rfbEncodingRichCursor )
		{
			ReadCursorShape(&surh);
			continue;
		}

		if (surh.encoding != rfbEncodingCacheZip && surh.encoding != rfbEncodingSolMonoZip && rfbEncodingUltraZip)
			SoftCursorLockArea(surh.r.x, surh.r.y, surh.r.w, surh.r.h);

		RECT cacherect;
		if (False)	// IIC -- m_opts.m_fEnableCache)
		{
			cacherect.left=surh.r.x;
			cacherect.right=surh.r.x+surh.r.w;
			cacherect.top=surh.r.y;
			cacherect.bottom=surh.r.y+surh.r.h;
		}
//	    log_write(LL_DEBUG, "encoding %d\n", surh.encoding);
		if (surh.encoding==rfbEncodingUltra || surh.encoding==rfbEncodingUltraZip) UltraFast=true;
		if (surh.encoding==rfbEncodingHextile || surh.encoding==rfbEncodingTight || surh.encoding==rfbEncodingZRLE
			|| surh.encoding==rfbEncodingSolidColor || surh.encoding==rfbEncodingSolMonoZip
			|| surh.encoding==rfbEncodingRRE || surh.encoding==rfbEncodingCoRRE|| surh.encoding==rfbEncodingZlib
			|| surh.encoding==rfbEncodingZlibHex || surh.encoding==rfbEncodingXOR_Zlib|| surh.encoding==rfbEncodingXORMultiColor_Zlib)
			UltraFast=False;

		switch (surh.encoding)
		{
		case rfbEncodingHextile:
//			SaveArea(cacherect);
//			ReadHextileRect(&surh);
			EncodingStatusWindow=rfbEncodingHextile;
			break;
		case rfbEncodingUltra:
			ReadUltraRect(&surh);
			EncodingStatusWindow=rfbEncodingUltra;
			break;
		case rfbEncodingUltraZip:
			ReadUltraZip(&surh);
			break;
		case rfbEncodingRaw:
//			SaveArea(cacherect);
//			ReadRawRect(&surh);
			EncodingStatusWindow=rfbEncodingRaw;
			break;
		case rfbEncodingCopyRect:
//			ReadCopyRect(&surh);
			break;
		case rfbEncodingCache:
//			ReadCacheRect(&surh);
			break;
		case rfbEncodingCacheZip:
//			ReadCacheZip(&surh,&UpdateRegion);
			break;
		case rfbEncodingSolMonoZip:
//			ReadSolMonoZip(&surh,&UpdateRegion);
			break;
		case rfbEncodingRRE:
//			SaveArea(cacherect);
//			ReadRRERect(&surh);
			EncodingStatusWindow=rfbEncodingRRE;
			break;
		case rfbEncodingCoRRE:
//			SaveArea(cacherect);
//			ReadCoRRERect(&surh);
			EncodingStatusWindow=rfbEncodingCoRRE;
			break;
		case rfbEncodingZlib:
//			SaveArea(cacherect);
			ReadZlibRect(&surh,0);
			EncodingStatusWindow=rfbEncodingZlib;
			break;
		case rfbEncodingZlibHex:
//			SaveArea(cacherect);
//			ReadZlibHexRect(&surh);
			EncodingStatusWindow=rfbEncodingZlibHex;
			break;
		case rfbEncodingXOR_Zlib:
//			SaveArea(cacherect);
			ReadZlibRect(&surh,1);
			break;
		case rfbEncodingXORMultiColor_Zlib:
//			SaveArea(cacherect);
			ReadZlibRect(&surh,2);
			break;
		case rfbEncodingXORMonoColor_Zlib:
//			SaveArea(cacherect);
			ReadZlibRect(&surh,3);
			break;
		case rfbEncodingSolidColor:
//			SaveArea(cacherect);
//			ReadSolidRect(&surh);
			break;
		case rfbEncodingZRLE:
//			SaveArea(cacherect);
			zrleDecode(surh.r.x, surh.r.y, surh.r.w, surh.r.h);
			EncodingStatusWindow=rfbEncodingZRLE;
			break;
		case rfbEncodingTight:
//			SaveArea(cacherect);
            log_write(LL_DEBUG, "Decoding tight rect: %d x %d\n", surh.r.w, surh.r.h);
			ReadTightRect(&surh);
			EncodingStatusWindow=rfbEncodingTight;
			break;
		default:
			log_write(LL_ERROR, "Unknown encoding %d - not supported!\n", surh.encoding);
			break;
		}

		if (surh.encoding != rfbEncodingCacheZip && surh.encoding != rfbEncodingSolMonoZip && surh.encoding != rfbEncodingUltraZip)
		{
			RECT rect;
			rect.left   = surh.r.x;
			rect.top    = surh.r.y;
			rect.right  = surh.r.x + surh.r.w ;
			rect.bottom = surh.r.y + surh.r.h;
//			InvalidateScreenRect(&rect);
		}
		else
		{
//			InvalidateRgn(m_hwnd, UpdateRegion, FALSE);
//			HRGN tempregion=CreateRectRgn(0,0,0,0);
//			CombineRgn(UpdateRegion,UpdateRegion,tempregion,RGN_AND);
//			DeleteObject(tempregion);
		}
//		SoftCursorUnlockScreen();
	}

	fis->stopTiming();
	int kbitsPerSecond = fis->kbitsPerSecond();
	Bool SentConnectionSpeedMsg = False;		// IIC
/*
 * 	if (m_fAutoDetectSlowConnection) {
		if ((timeGetTime() - m_lConnectionStartTime)>30000) {	// stop detecting after 30 seconds
			m_fAutoDetectSlowConnection = False;
		} else if (kbitsPerSecond!=0 && kbitsPerSecond<19 && (timeGetTime() - m_lConnectionStartTime)>=20000) {
			rfbSendConnectionSpeedMsg msg;
			msg.type = rfbSendConnectionSpeed;
			msg.speed = Swap16IfLE((CARD16)kbitsPerSecond);
			WriteExact((char*)&msg, sz_rfbSendConnectionSpeedMsg);
			m_fAutoDetectSlowConnection = false;
			MessageBeep(-1);
			SentConnectionSpeedMsg = TRUE;
		}
	}
*/
	// Inform the other thread that an update is needed.

//	PostMessage(m_hwnd, WM_REGIONUPDATED, SentConnectionSpeedMsg, NULL);
//	DeleteObject(UpdateRegion);
}
