/*
 *  Copyright (C) 2002 RealVNC Ltd.
 *  Copyright (C) 1999 AT&T Laboratories Cambridge.  All Rights Reserved.
 *
 *  This is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this software; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 *  USA.
 */

/*
 * vncviewer.h for Vnc2Swf
 * $Id: vncviewer.h,v 1.7 2005-09-07 14:18:36 binl Exp $
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifndef WIN32
#include <sys/time.h>
#include <unistd.h>
#else
#include "windows.h"
#endif
#include "rfbproto.h"
#include "logging.h"

//#include "ming-0.3a/src/libming.h"

#ifndef bool
#define bool int
#endif

#ifndef UINT
typedef unsigned int UINT;
#endif

#ifndef BYTE
typedef unsigned char BYTE;
#endif

#ifndef Bool
typedef int Bool;
#endif
/*
#ifndef BOOL
typedef unsigned char BOOL;
#endif
*/
#ifndef True
#define True  1
#define False 0
#endif

#ifndef COLORREF
#define COLORREF CARD32
#endif

#ifndef Pixel
typedef COLORREF Pixel;
#endif

#ifndef PALETTERGB
#define PALETTERGB(r, g, b)	((r<<16)|(g<<8)|b)
#endif

struct mybool {
 bool b0 : 1;
 bool b1 : 1;
 bool b2 : 1;
 bool b3 : 1;
 bool b4 : 1;
 bool b5 : 1;
 bool b6 : 1;
 bool b7 : 1;
};

extern int endianTest;

#define Swap16IfLE(s) \
    (*(char *)&endianTest ? ((((s) & 0xff) << 8) | (((s) >> 8) & 0xff)) : (s))

#define Swap32IfLE(l) \
    (*(char *)&endianTest ? ((((l) & 0xff000000) >> 24) | \
			     (((l) & 0x00ff0000) >> 8)  | \
			     (((l) & 0x0000ff00) << 8)  | \
			     (((l) & 0x000000ff) << 24))  : (l))

#define MAX_ENCODINGS 10

#define FLASH_PORT_OFFSET 5400
#define LISTEN_PORT_OFFSET 5500
#define SERVER_PORT_OFFSET 5900

#define RECORD_IMMEDIATELY 0
#define RECORD_BUFFERED 1

#define max(x,y) ((x)<(y)? (y) : (x))
#define min(x,y) ((x)<(y)? (x) : (y))

#define buf_get_CARD8(buf)                      \
  (*((CARD8 *)buf))

#define buf_get_CARD16(buf)                     \
  ((CARD16)((CARD8 *)buf)[0] << 8 |             \
   (CARD16)((CARD8 *)buf)[1])

#define buf_get_CARD32(buf)                     \
  ((CARD32)((CARD8 *)buf)[0] << 24 |            \
   (CARD32)((CARD8 *)buf)[1] << 16 |            \
   (CARD32)((CARD8 *)buf)[2] << 8  |            \
   (CARD32)((CARD8 *)buf)[3]);

/* argsresources.c */

typedef struct {
    Bool spool;
    FILE* spool_handle;

    Bool replay;
    Bool deleteFile;
    Bool preview;

    Bool autoDetect;

    char encodings[256];

    Bool useBGR233;
    int nColours;
    Bool useSharedColours;
    Bool forceOwnCmap;
    Bool forceTrueColour;
    int requestedDepth;

    Bool useShm;

    int wmDecorationWidth;
    int wmDecorationHeight;

    char passwordFile[512];
    Bool passwordDialog;

    int rawDelay;
    int copyRectDelay;

    Bool debug;

    int popupButtonCount;

    int bumpScrollTime;
    int bumpScrollPixels;

    /* vnc2swf */

    char inputFile[512];
    char dumpFile[512];
    char soundFile[512];
    int frameRate;
    Bool macromediaHack;
    Bool startRecording;
    int recordingMethod;

    int recording;
    int nframes;
    unsigned long compressed_image_bytes;
    unsigned long expanded_image_bytes;

    char clippingGeometry[32];
    char meetingId[32];
    char password[32];
    int semkey;
    int shmkey;
    int loglevel;
    int stdlevel;
    int cgx0, cgy0, cgw, cgh, cgx1, cgy1;
	int last_w, last_h;	// keep the original real size
	int frame_w, frame_h;  // actual frame size (si.framebufferX corresponds to bitmap size)
    int delay;
	int record_enable_time;
	int framesPerMovie;
	Bool recordingStarted;
	char cipher[256];
	int format;
	int bitRate;
	int audioBitRate;
	int fileType;
} AppData;

extern AppData appData;
extern int spoolFrameLength;

extern char *fallback_resources[];
extern char vncServerHost[];
extern int vncServerPort;
//extern Bool listenSpecified;
extern int listenPort, flashPort;

//extern XrmOptionDescRec cmdLineOptions[];
extern int numCmdLineOptions;

extern void GetArgsAndResources(int argc, char *argv[]);
//extern void SetFullScreenState(Widget w, XEvent *event, String *params,
//			       Cardinal *num_params);
//extern void SetBGR233State(Widget w, XEvent *event, String *params,
//                           Cardinal *num_params);
//extern void SetAutoState(Widget w, XEvent *event, String *params,
//                         Cardinal *num_params);
//extern void ToggleAuto(Widget w, XEvent *event, String *params,
//                       Cardinal *num_params);
//extern void ToggleBGR233(Widget w, XEvent *event, String *params,
//                         Cardinal *num_params);

/* colour.c */
/*
extern unsigned long BGR233ToPixel[];

extern Colormap cmap;
extern Visual *vis;
extern unsigned int visdepth, visbpp;
extern Bool usingBGR233;
extern unsigned int redRv[], greenRv[], blueRv[];

extern void GetVisualAndCmap();
extern void SetMyFormatFromVisual();
extern void InitReverseMapping();
*/
/* desktop.c */
/*
extern Atom wmDeleteWindow;
extern Widget form, viewport, desktop;
extern Window desktopWin;
extern GC gc;
extern GC srcGC, dstGC;
extern Dimension dpyWidth, dpyHeight;
extern XImage* image;

extern void DesktopInitBeforeRealization();
extern void DesktopInitAfterRealization();
extern void SendRFBEvent(Widget w, XEvent *event, String *params,
			 Cardinal *num_params);
extern void SynchroniseScreen();


extern void CopyRectangularArea(int x0, int y0, int w, int h, int x1, int y1);

extern void ToggleStatus(Widget w, XEvent *event, String *params,
			 Cardinal *num_params);
*/
extern void DisplayRectangle(Pixel pix, int rx, int ry, int rw, int rh);
extern void CopyDataToScreen(char *buf, int x, int y, int width, int height);
extern void CopyJpegToScreen(char *buf, int len, int x, int y, int width, int height);	// IIC
extern void BufferDataToScreen(char *buf, int x, int y, int width, int height);

/* dialogs.c */
/*
extern void ServerDialogDone(Widget w, XEvent *event, String *params,
			     Cardinal *num_params);
extern char *DoServerDialog();
extern void PasswordDialogDone(Widget w, XEvent *event, String *params,
			     Cardinal *num_params);
extern char *DoPasswordDialog();
*/
/* fullscreen.c */
/*
extern void ToggleFullScreen(Widget w, XEvent *event, String *params,
			     Cardinal *num_params);
extern Bool BumpScroll(XEvent *ev);
extern void FullScreenOn();
extern void FullScreenOff();
*/
/* listen.c */

extern void listenForIncomingConnections();

/* misc.c */

extern void CommonInit();
extern void ChangeToplevelTitle(char *status);
extern void ToplevelInitBeforeRealization();
extern void ToplevelInitAfterRealization();
//extern Time TimeFromEvent(XEvent *ev);
//extern void Pause(Widget w, XEvent *event, String *params,
//		  Cardinal *num_params);
//extern void Quit(Widget w, XEvent *event, String *params,
//		 Cardinal *num_params);
extern void Cleanup();

/* popup.c */
/*
extern Widget popup;
extern void ShowPopup(Widget w, XEvent *event, String *params,
		      Cardinal *num_params);
extern void HidePopup(Widget w, XEvent *event, String *params,
		      Cardinal *num_params);
extern void CreatePopup();
*/
/* rfbproto.c */

extern Bool canUseCoRRE;
extern Bool canUseHextile;
extern char *desktopName;
extern rfbPixelFormat myFormat;
extern Bool pendingFormatChange;
extern rfbServerInitMsg si;
extern char *serverCutText;
extern Bool newServerCutText;

extern void SendInitMsg(const char * meetingId);	//IIC
extern Bool SendFramebufferUpdateRequest(int x, int y, int w, int h, Bool incremental);

extern Bool InitialiseRFBConnection(const char * password);
extern Bool SendSetPixelFormat();
extern Bool SendSetEncodings();
extern void UpdateNeeded(int x, int y, int w, int h);
extern Bool RequestNewUpdate();
extern Bool CheckUpdateNeeded();
extern Bool SendPointerEvent(int x, int y, int buttonMask);
extern Bool SendKeyEvent(CARD32 key, Bool down);
extern Bool SendClientCutText(char *str, int len);
extern Bool HandleRFBServerMessage();

extern void PrintPixelFormat(rfbPixelFormat *format);

/* selection.c */
/*
extern void InitialiseSelection();
extern void SelectionToVNC(Widget w, XEvent *event, String *params,
			   Cardinal *num_params);
extern void SelectionFromVNC(Widget w, XEvent *event, String *params,
			     Cardinal *num_params);
*/
/* shm.c */
/*
extern XImage *CreateShmImage();
extern void ShmCleanup();
*/
/* sockets.cxx */

extern Bool sameMachine;

extern Bool ConnectToRFBServer(const char *hostname, int port);
extern Bool SetRFBSock(int sock);
extern void DisconnectRFBServer();
extern void StartTiming();
extern void StopTiming();
extern int KbitsPerSecond();
extern int TimeWaitedIn100us();
extern Bool ReadExact(char *out, unsigned int n);	// IIC
extern Bool WriteExact(char *buf, int n);		// IIC
extern Bool ReadFromRFBServer(char *out, unsigned int n);
extern Bool WriteToRFBServer(char *buf, int n);
extern int ConnectToTcpAddr(const char* hostname, int port);
extern int ListenAtTcpPort(int port);
extern int AcceptTcpConnection(int listenSock);

extern int StringToIPAddr(const char *str, unsigned int *addr);

/* vncviewer.c */
extern char *programName;
/*
extern XtAppContext appContext;
extern Display* dpy;
extern Widget toplevel;
*/
/* zrle.cxx */

extern Bool zrleDecode(int x, int y, int w, int h);

/* writevid.c */
extern void checkNextFrame();
extern void InsertEmptyFrame(Bool cleanup);           // IIC
extern void SetFrameRate(int rate);              // IIC
extern Bool WriteInitMovie(int w, int h);
extern void OpenMovieStream(int w, int h);
extern Bool WriteFinishMovie(Bool final);
extern Bool WriteImage(char* buf, unsigned long len, int x, int y, int w, int h);
extern Bool WriteJpegImage(char* buf, int len, int x, int y, int w, int h);
extern Bool WriteRectangle(Pixel c, int x, int y, int w, int h);
//extern Bool WriteCopyImage(Display* dpy, Drawable d, int x0, int y0, int w, int h, int x1, int y1);
//extern void SetRecordingState(Widget w, XEvent *event, String *params,
//			      Cardinal *num_params);
//extern void ToggleRecording(Widget w, XEvent *event, String *params,
//			    Cardinal *num_params);
//extern void SwitchMethod(Widget w, XEvent *event, String *params,
//			 Cardinal *num_params);
//extern void ResetRecording(Widget w, XEvent *event, String *params,
//			   Cardinal *num_params);
extern void TurnRecordingOn(int sig);
extern void TurnRecordingOff(int sig);
extern void RecordMessage(const char * message);
extern void SetSoundStream(const char * soundfile);
extern Bool init_recording(void);
extern void SetMovieSize(int w, int h);
extern void LoadMovieFromFile(FILE* f);
extern void SetMovieSizeEx(int w, int h);
extern void save_movie_size(int w, int h);
extern int load_movie_size(int *w, int *h);

extern void OpenSpoolFile();
extern void CloseSpoolFile();
extern void WriteMovieSize(int w, int h);
extern void WriteMovieStartTime();
extern void SpoolMovieData(void *buf, size_t len, int type, int x, int y, int w, int h);

// IIC
#ifndef WIN32
typedef struct _RECT
{
	int left;
	int top;
	int right;
	int bottom;
} RECT;

typedef struct _POINT
{
	int x;
	int y;
} POINT;
#endif

//extern rfbPixelFormat m_myFormat, m_pendingFormat;
extern int EncodingStatusWindow,OldEncodingStatusWindow;
extern int m_nTO;
extern int m_nReadSize;
extern int m_nNetRectBufSize;
extern BYTE* m_pNetRectBuf;

extern void ReadScreenUpdate(rfbFramebufferUpdateRectHeader* pfburh);
extern void InitBlowfish(const char * key);
extern int ReadNewFBSize(rfbFramebufferUpdateRectHeader *pfburh);
extern void SendFullFramebufferUpdateRequest();

// ClientConnectionTight.cpp
extern  void ResetTightEncoder();
extern	void ReadTightRect(rfbFramebufferUpdateRectHeader *pfburh);
extern	int ReadCompactLen();
extern	int InitFilterCopy (int rw, int rh);
extern	int InitFilterGradient (int rw, int rh);
extern	int InitFilterPalette (int rw, int rh);
extern	void FilterCopy8 (int numRows);
extern	void FilterCopy16 (int numRows);
extern	void FilterCopy24 (int numRows);
extern	void FilterCopy32 (int numRows);
extern	void FilterGradient8 (int numRows);
extern	void FilterGradient16 (int numRows);
extern	void FilterGradient24 (int numRows);
extern	void FilterGradient32 (int numRows);
extern	void FilterPalette (int numRows);
extern	void DecompressJpegRect(int x, int y, int w, int h);
extern int ReadEncrypted(char *lpBuffer, int lSize);

extern Bool CheckBufferSize(int bufsize);
extern int m_netbufsize;
extern char* m_netbuf;

// sf@2002 - v1.1.0 - Buffer for zip decompression (FileTransfer)
extern Bool CheckZipBufferSize(int bufsize);
extern unsigned char *m_zipbuf;
extern int m_zipbufsize;

// Buffer for zlib decompression.
extern Bool CheckZlibBufferSize(int bufsize);
extern void ReadZlibRect(rfbFramebufferUpdateRectHeader *pfburh,int XOR);

extern unsigned char *m_zlibbuf;
extern int m_zlibbufsize;

//UltraFast
extern void ConvertAll(int width, int height, int xx, int yy,int bytes_per_pixel,BYTE* source,BYTE* dest,int framebufferWidth);
extern void SolidColor(int width, int height, int xx, int yy,int bytes_per_pixel,BYTE* source,BYTE* dest,int framebufferWidth);
extern bool Check_Rectangle_borders(int x,int y,int w,int h);
extern void ReadUltraZip(rfbFramebufferUpdateRectHeader *pfburh);
extern void ReadUltraRect(rfbFramebufferUpdateRectHeader *pfburh);
extern int m_minPixelBytes;
extern Bool UltraFast;

// Tight ClientConnectionCursor.cpp
extern Bool prevCursorSet;
extern COLORREF *rcSource;
extern Bool *rcMask;
extern int rcHotX, rcHotY, rcWidth, rcHeight;
extern int rcCursorX, rcCursorY;
extern int rcLockX, rcLockY, rcLockWidth, rcLockHeight;
extern Bool rcCursorHidden, rcLockSet;
extern void ReadCursorShape(rfbFramebufferUpdateRectHeader *pfburh);
extern void SoftCursorLockArea(int x, int y, int w, int h);
extern void SoftCursorUnlockScreen();
extern void SoftCursorMove(int x, int y);
extern void SoftCursorFree();
extern Bool SoftCursorInLockedArea();
extern void SoftCursorSaveArea();
extern void SoftCursorRestoreArea();
extern void SoftCursorDraw();
extern void SoftCursorToScreen(RECT *screenArea, POINT *cursorOffset);
extern void InvalidateScreenRect(const RECT *pRect);
// Colour decoding utility functions
// Define rs,rm, bs,bm, gs & gm before using, eg with the following:

// read a pixel from the given address, and return a color value
#define SETUP_COLOR_SHORTCUTS \
	 CARD8 rs = myFormat.redShift;   CARD16 rm = myFormat.redMax;   \
     CARD8 gs = myFormat.greenShift; CARD16 gm = myFormat.greenMax; \
     CARD8 bs = myFormat.blueShift;  CARD16 bm = myFormat.blueMax;  \

#define COLOR_FROM_PIXEL8_ADDRESS(p) (PALETTERGB( \
                (int) (((*(CARD8 *)(p) >> rs) & rm) * 255 / rm), \
                (int) (((*(CARD8 *)(p) >> gs) & gm) * 255 / gm), \
                (int) (((*(CARD8 *)(p) >> bs) & bm) * 255 / bm) ))

#define COLOR_FROM_PIXEL16_ADDRESS(p) (PALETTERGB( \
                (int) ((( *(CARD16 *)(p) >> rs) & rm) * 255 / rm), \
                (int) ((( *(CARD16 *)(p) >> gs) & gm) * 255 / gm), \
                (int) ((( *(CARD16 *)(p) >> bs) & bm) * 255 / bm) ))

#define COLOR_FROM_PIXEL24_ADDRESS(p) (PALETTERGB( \
                (int) (((CARD8 *)(p))[0]), \
                (int) (((CARD8 *)(p))[1]), \
                (int) (((CARD8 *)(p))[2]) ))

#define COLOR_FROM_PIXEL32_ADDRESS(p) (PALETTERGB( \
                (int) ((( *(CARD32 *)(p) >> rs) & rm) * 255 / rm), \
                (int) ((( *(CARD32 *)(p) >> gs) & gm) * 255 / gm), \
                (int) ((( *(CARD32 *)(p) >> bs) & bm) * 255 / bm) ))

// The following may be faster if you already have a pixel value of the appropriate size
#define COLOR_FROM_PIXEL8(p) (PALETTERGB( \
                (int) (((p >> rs) & rm) * 255 / rm), \
                (int) (((p >> gs) & gm) * 255 / gm), \
                (int) (((p >> bs) & bm) * 255 / bm) ))

#define COLOR_FROM_PIXEL16(p) (PALETTERGB( \
                (int) ((( p >> rs) & rm) * 255 / rm), \
                (int) ((( p >> gs) & gm) * 255 / gm), \
                (int) ((( p >> bs) & bm) * 255 / bm) ))

#define COLOR_FROM_PIXEL32(p) (PALETTERGB( \
                (int) (((p >> rs) & rm) * 255 / rm), \
                (int) (((p >> gs) & gm) * 255 / gm), \
                (int) (((p >> bs) & bm) * 255 / bm) ))


#ifdef UNDER_CE
#define SETPIXEL(b,x,y,c) SetPixel((b),(x),(y),(c))
#else
#define SETPIXEL(b,x,y,c) SetPixelV((b),(x),(y),(c))
#endif

#define MYMASK(mask,i,result)						\
	{												\
		int byte_nr,bit_nr;							\
		byte_nr=i/8;								\
		bit_nr=i%8;								\
		if (bit_nr==0) result=mask[byte_nr].b0;		\
		if (bit_nr==1) result=mask[byte_nr].b1;		\
		if (bit_nr==2) result=mask[byte_nr].b2;		\
		if (bit_nr==3) result=mask[byte_nr].b3;		\
		if (bit_nr==4) result=mask[byte_nr].b4;		\
		if (bit_nr==5) result=mask[byte_nr].b5;		\
		if (bit_nr==6) result=mask[byte_nr].b6;		\
		if (bit_nr==7) result=mask[byte_nr].b7;		\
	}
