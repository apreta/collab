# $Id: Vnc2Swf.spec,v 1.1 2004-10-12 15:49:03 eric Exp $
Name: vnc2swf
Version: 0.4
Release: 1
Summary: Vnc2Swf is a recoding tool for Flash.
Group: User Interface/X
Copyright: GPL
URL: http://www.unixuser.org/~euske/vnc2swf/
Source: http://www.unixuser.org/~euske/vnc2swf/vnc2swf-0.4.tar.gz
BuildArch: i386
BuildRoot: %{_tmppath}/%{name}-%{version}-root
Requires: zlib XFree86-libs libstdc++
Packager: yusuke at cs dot nyu do edu

%description
Vnc2Swf is a screen recoding tool for Flash.

%description -l ja
Vnc2Swf は VNC を録画して Flash に変換するプログラムです。

%prep
rm -rf ${RPM_BUILD_ROOT}

%setup -q

%build
./configure
make

%install
rm -rf ${RPM_BUILD_ROOT}
make install DESTDIR="${RPM_BUILD_ROOT}"

%clean
rm -rf ${RPM_BUILD_ROOT}

%files
%defattr(-, root, root)
/usr/X11R6/bin/vnc2swf
%doc README
%doc README.vnc-3.3.7

%changelog
* Mon Apr 17 2004 by Yusuke Shinyama < yusuke at cs dot nyu dot edu >
- 0.4.

* Fri Nov 21 2003 by Yusuke Shinyama < yusuke at cs dot nyu dot edu >
- Initial RPM release.
