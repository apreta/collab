/*
 *  Copyright (C) 2002-2003 RealVNC Ltd.
 *  Copyright (C) 1999 AT&T Laboratories Cambridge.  All Rights Reserved.
 *
 *  This is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this software; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 *  USA.
 */

/*
 * vnc2swf.c - the Xt-based VNC recorder, based on vncviewer.c
 * $Id: vnc2swf.c,v 1.19 2005-09-21 16:43:32 binl Exp $
 */

#include "vncviewer.h"
#include <sys/types.h>
#ifndef WIN32
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/resource.h>
#endif
#include "sync.h"


char *programName;
//XtAppContext appContext;
//Display* dpy;
//Widget toplevel;

extern char buildtime[];
extern int totalReadBytes;

enum {RS_NONE, RS_RECORDING, RS_STOPPED, RS_PAUSE, RS_SOUNDREADY, RS_CHAT, RS_DROP};

void CommonInit()
{
	appData.cgx0 = 0;
	appData.cgy0 = 0;
	appData.cgw = si.framebufferWidth;
	appData.cgh = si.framebufferHeight;
	appData.cgx1 = appData.cgx0 + appData.cgw;
	appData.cgy1 = appData.cgy0 + appData.cgh;
	appData.last_w = si.framebufferWidth;	// remember last real dimension
	appData.last_h = si.framebufferHeight;

	appData.recordingStarted = False;

	myFormat.bitsPerPixel = 32;
	myFormat.depth = 24;

	myFormat.trueColour = 1;
	myFormat.bigEndian = 0;		// TODO: fix this for Windows
	myFormat.redShift = 16;
	myFormat.greenShift = 8;
	myFormat.blueShift = 0;
	myFormat.redMax = 255;
	myFormat.greenMax = 255;
	myFormat.blueMax = 255;

	Ming_useSWFVersion(6);
}


extern void SetStartTime();
extern int GetLastDuration();

static int GenerateMovieEx(int option)
{
	FILE* fp;
	int len;
	int offset, padding, file_size;
	char tmpbuf[128], tmpbuf2[128];
	time_t first_recordtime = 0;
	time_t recordtime = 0;
	time_t last_recordtime = 0;
	Bool skip_firstrecording = True;

	if (!WriteInitMovie(appData.cgw, appData.cgh)) {
		log_write(LL_ERROR, "Failed to init movie, exit. %s\n", appData.meetingId);
		exit(1);
	}

	// the input data holds all the timestamps for each data share session
	fp = fopen(appData.inputFile, "rb");
	if (fp!=NULL) {
//		TurnRecordingOn(0);

		InitBlowfish(appData.cipher);	// IIC

		while(!feof(fp))
		{
			time_t start_time;
			long sessionid;
			if (!ConnectToRFBServer(vncServerHost, vncServerPort)) {
				log_write(LL_ERROR, "Failed to connect to server %s:%d\n", vncServerHost, vncServerPort);
				break;
			}

			if (!InitialiseRFBConnection(appData.password)) {
				log_write(LL_ERROR, "Failed to connect to server %s:%d. Authentication failed. \n", vncServerHost, vncServerPort);
				break;
			}
    
			// Send Meeting ID
			len = strlen(appData.meetingId);
			len = htonl(len);
			if (!WriteToRFBServer(&len, 4))
			{
				log_write(LL_ERROR, "Failed to send data to server %s:%d.\n", vncServerHost, vncServerPort);
				break;
			}
			if (!WriteToRFBServer(appData.meetingId, strlen(appData.meetingId)))
			{
				log_write(LL_ERROR, "Failed to send data to server %s:%d.\n", vncServerHost, vncServerPort);
				break;
			}
			fscanf(fp, "%s ", tmpbuf);
			start_time = atol(tmpbuf);
			log_write(LL_DEBUG, "Sending session ID: %s(%d).\n", tmpbuf, start_time);

            // Send session ID
			sessionid = htonl(start_time);
			if (!WriteToRFBServer(&sessionid, sizeof(sessionid)))
			{
				log_write(LL_ERROR, "Failed to send data to server %s:%d.\n", vncServerHost, vncServerPort);
				break;
			}

			option = htonl(option);
			if (!WriteToRFBServer(&option, 4))
			{
				log_write(LL_ERROR, "Failed to send data to server %s:%d.\n", vncServerHost, vncServerPort);
				break;
			}

			/* manually send a pseudo update request if nowindow mode. */
			UpdateNeeded(0, 0, si.framebufferWidth, si.framebufferHeight);
    
			log_write(LL_DEBUG, "Receiving data from server.\n");

			// Read file size
			if (!ReadFromRFBServer(&file_size, 4))
			{
				break;
			}
			file_size = ntohl(file_size);
			log_write(LL_MSG, "File size = %d.\n", file_size);

			if (file_size==0)	// server cannot find the spool file, maybe recording was not enabled for that section
				continue;	

            totalReadBytes = 0;
			// Read file header
			if (!ReadFromRFBServer(tmpbuf, 12))
			{
				break;
			}
			// read time offset    
			if (!ReadFromRFBServer(tmpbuf2, 4))
			{
				break;
			};
			// read data length
			if (!ReadFromRFBServer(tmpbuf2, 4))
			{
				break;
			};
			len = buf_get_CARD32(tmpbuf2);
			//also get the padding
			padding = 3 - ((len - 1) & 0x03);
			if (padding>0)
			{
				if (!ReadFromRFBServer(tmpbuf, padding))
				{
					break;
				};
			}
            
			// read the new session flag, ignore this one
			len -= 1;	// data len includes one byte for new session flag
			if (!ReadFromRFBServer(tmpbuf, 1))
			{
				break;
			};

			if (!ReadFromRFBServer(tmpbuf, len))
			{
				break;
			};
			recordtime = buf_get_CARD32(&tmpbuf[12]);
			if (first_recordtime==0)		// remember the first share session record time
			{
				first_recordtime = recordtime;
				appData.delay = first_recordtime - appData.record_enable_time;	// delay between data recording & enable recording
				if (appData.delay < 0)	// should not have anything to skip, spool does not record if recording is not enabled
				{
					appData.delay = 0;
				}
				log_write(LL_DEBUG, "The initial delay time is %d (%d - %d).\n", appData.delay, first_recordtime, appData.record_enable_time);
			}

			if (last_recordtime!=0)
			{
				log_write(LL_DEBUG, "The last session start time was %d.\n", last_recordtime);
				appData.delay = recordtime - last_recordtime - GetLastDuration();
				if (appData.delay < 0)
				{
					log_write(LL_ERROR, "The interval between two sessions was wrong: %d (%d - %d - %d).\n", appData.delay, recordtime, last_recordtime, GetLastDuration());
					appData.delay = 0;
				}
				log_write(LL_DEBUG, "The interval between two sessions is %d.\n", appData.delay);
			}
			last_recordtime = recordtime;

			spoolFrameLength = 0;
			offset = appData.delay;
			log_write(LL_DEBUG, "Insert Empty Frames: %d.\n", offset);
			while (offset-- > 0)	// add empty frames to keep in sync with voice 
			{
				InsertEmptyFrame(0);
			}

			si.framebufferWidth = buf_get_CARD16(&tmpbuf[16]);
			si.framebufferHeight = buf_get_CARD16(&tmpbuf[18]);
			memcpy(&si.format, &tmpbuf[20], sizeof(si.format));
			// a meeting can have many share sessions, and presenters, the screen size of presenter maybe different
			// use the maximum screen size
			int cgw = max(si.framebufferWidth, appData.cgw);
			int cgh = max(si.framebufferHeight, appData.cgh);
			if (si.framebufferWidth != appData.last_w || si.framebufferHeight != appData.last_h)	// compare the real size
			{
				appData.last_w = si.framebufferWidth;	// remember last real dimension
				appData.last_h = si.framebufferHeight;

				appData.cgw = cgw;
				appData.cgh = cgh;
				appData.cgx1 = appData.cgx0 + appData.cgw;
				appData.cgy1 = appData.cgy0 + appData.cgh;
				SetMovieSize(appData.cgw, appData.cgh);
			}
			si.framebufferWidth = cgw;
			si.framebufferHeight = cgh;

			// always to clean up, in case we missed the change size message
			if (!skip_firstrecording)	// but dont need to cleanup at the first frame
			{
				TurnRecordingOn(0);	// have to turn it on, otherwise DisplayRectangle will not be saved
				// clean up the screen -- after Insert empty frames to avoid long blank screen
				DisplayRectangle(0xFFFFFF, 0, 0, si.framebufferWidth, si.framebufferHeight);
			}
			skip_firstrecording = False;

			SetStartTime();
			TurnRecordingOff(0);
			while (1) 
			{
				// HandleRFBServerMessage
				if (!HandleRFBServerMessage())
					break;	// connnection maybe closed
			}

			log_write(LL_DEBUG, "Total Read Bytes: %d.\n", totalReadBytes);
		}
		
		fclose(fp);
	}
	else
	{
		log_write(LL_ERROR, "Failed to open session file: %s\n", appData.inputFile);
	}

	WriteFinishMovie(True);

	int ret = (file_size==0 || totalReadBytes>=(file_size+8))?0:-1;
	return ret;
}

static int DeleteRemoteSpool()
{
	FILE* fp;
	int len;
	int option;
	char tmpbuf[128];

	// the input data holds all the timestamps for each data share session
	fp = fopen(appData.inputFile, "rb");
	if (fp!=NULL) {
		while(!feof(fp))
		{
			time_t start_time;
			if (!ConnectToRFBServer(vncServerHost, vncServerPort)) {
				log_write(LL_ERROR, "Failed to connect to server %s:%d\n", vncServerHost, vncServerPort);
				break;
			}

			if (!InitialiseRFBConnection(appData.password)) {
				log_write(LL_ERROR, "Failed to connect to server %s:%d. Authentication failed. \n", vncServerHost, vncServerPort);
				break;
			}
    
			// Send Meeting ID
			len = strlen(appData.meetingId);
			len = htonl(len);
			if (!WriteToRFBServer(&len, 4))
			{
				log_write(LL_ERROR, "Failed to send data to server %s:%d.\n", vncServerHost, vncServerPort);
				break;
			}
			if (!WriteToRFBServer(appData.meetingId, strlen(appData.meetingId)))
			{
				log_write(LL_ERROR, "Failed to send data to server %s:%d.\n", vncServerHost, vncServerPort);
				break;
			}
			fscanf(fp, "%s ", tmpbuf);
			start_time = atol(tmpbuf);
			log_write(LL_DEBUG, "Delete spool file: session ID: %s(%d).\n", tmpbuf, start_time);
			// Send session ID
			start_time = htonl(start_time);
			if (!WriteToRFBServer(&start_time, sizeof(start_time)))
			{
				log_write(LL_ERROR, "Failed to send data to server %s:%d.\n", vncServerHost, vncServerPort);
				break;
			}
			option = htonl(2);
			if (!WriteToRFBServer(&option, 4))
			{
				log_write(LL_ERROR, "Failed to send data to server %s:%d.\n", vncServerHost, vncServerPort);
				break;
			}
		}
		
		fclose(fp);
	}

	return 0;
}

int main(int argc, char **argv)
{
	int cmd;
	char message[SHM_SIZE];
	bool recordingStarted = False;
	bool retry = True;
	int sleeptime = 1;
	int ret;
	programName = argv[0];

	/* Interpret resource specs and process any remaining command-line arguments
	(i.e. the VNC server name).  If the server name isn't specified on the
	command line, getArgsAndResources() will pop up a dialog box and wait
	for one to be entered. */

	setpriority(PRIO_PROCESS, 0, 5);

	GetArgsAndResources(argc, argv);

	log_open("/var/log/iic/vnc2swf.log", appData.loglevel, appData.stdlevel);

	CommonInit();

	/* Perform initialisation that needs doing after realization, now that the X
	windows exist */

	if (appData.replay)
	{
		ret = GenerateMovieEx(0);
	}
	else if (appData.preview)
	{
		ret = GenerateMovieEx(1);
	}
	else if (appData.deleteFile)
	{
		ret = DeleteRemoteSpool();
	}
	else
	{
		ret = GenerateMovieEx(0);
	}

	log_close();

	return ret;
}
