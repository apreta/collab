/*
 *  Copyright (C) 2002 RealVNC Ltd.
 *  Copyright (C) 1999 AT&T Laboratories Cambridge.  All Rights Reserved.
 *
 *  This is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this software; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 *  USA.
 */

/*
 * rfbproto.c - functions to deal with client side of RFB protocol.
 */

#ifndef WIN32
#include <unistd.h>
#include <errno.h>
#include <pwd.h>
#endif
#include "vncviewer.h"
#include "rfb/vncauth.h"

static Bool HandleRRE8(int rx, int ry, int rw, int rh);
static Bool HandleRRE16(int rx, int ry, int rw, int rh);
static Bool HandleRRE32(int rx, int ry, int rw, int rh);
static Bool HandleCoRRE8(int rx, int ry, int rw, int rh);
static Bool HandleCoRRE16(int rx, int ry, int rw, int rh);
static Bool HandleCoRRE32(int rx, int ry, int rw, int rh);
static Bool HandleHextile8(int rx, int ry, int rw, int rh);
static Bool HandleHextile16(int rx, int ry, int rw, int rh);
static Bool HandleHextile32(int rx, int ry, int rw, int rh);

char *desktopName;

// IIC
rfbPixelFormat myFormat = {32, 24, 0, 1, 255, 255, 25, 0,8,16,0,0};

rfbServerInitMsg si = {640, 480};
Bool pendingFormatChange = False;
Bool UltraFast;
int spoolFrameLength;
int totalReadBytes = 0;

/*
 * Macro to compare pixel formats.
 */
#ifndef WIN32
#define strnicmp strncasecmp
#endif

#define PF_EQ(x,y)							\
	((x.bitsPerPixel == y.bitsPerPixel) &&				\
	 (x.depth == y.depth) &&					\
	 ((x.bigEndian == y.bigEndian) || (x.bitsPerPixel == 8)) &&	\
	 (x.trueColour == y.trueColour) &&				\
	 (!x.trueColour || ((x.redMax == y.redMax) &&			\
			    (x.greenMax == y.greenMax) &&		\
			    (x.blueMax == y.blueMax) &&			\
			    (x.redShift == y.redShift) &&		\
			    (x.greenShift == y.greenShift) &&		\
			    (x.blueShift == y.blueShift))))

int currentEncoding = rfbEncodingZRLE;
Bool pendingEncodingChange = False;
int supportedEncodings[] = {
  rfbEncodingZRLE, rfbEncodingHextile, rfbEncodingCoRRE, rfbEncodingRRE,
  rfbEncodingRaw
};
#define NUM_SUPPORTED_ENCODINGS (sizeof(supportedEncodings)/sizeof(int))
const rfbPixelFormat vnc16bitFormat = {16, 16, 0, 1, 63, 31, 31, 0,6,11,0,0};

char *serverCutText = NULL;
Bool newServerCutText = False;

int endianTest = 1;


/* note that the CoRRE encoding uses this buffer and assumes it is big enough
   to hold 255 * 255 * 32 bits -> 260100 bytes.  640*480 = 307200 bytes */
/* also hextile assumes it is big enough to hold 16 * 16 * 32 bits */
#define BUFFER_SIZE (640*480)
static char buffer[BUFFER_SIZE];

// IIC
void SendInitMsg(const char * meetingId)
{
	// IIC -- send init msg first
	rfbOmniInitMsg omni_init;
	rfbClientInitMsg client_ini;

	omni_init.type = Swap16IfLE(1);
	omni_init.idLen = Swap16IfLE(strlen(meetingId));
	WriteToRFBServer((char *)&omni_init, sizeof(omni_init));
	ReadFromRFBServer((char *)&client_ini, sz_rfbClientInitMsg);
	WriteToRFBServer((char*)meetingId, strlen(meetingId));
}

void SetupPixelFormat() {
	if (!si.format.trueColour) {

        // We'll just request a standard 16-bit truecolor
        log_write(LL_INFO, "Requesting 16-bit truecolour\n");
        myFormat = vnc16bitFormat;
    } else {

		// Normally we just use the sever's format suggestion
		myFormat = si.format;
        myFormat.bigEndian = 0; // except always little endian

		// It's silly requesting more bits than our current display has, but
		// in fact it doesn't usually amount to much on the network.
		// Windows doesn't support 8-bit truecolour.
		// If our display is palette-based, we want more than 8 bit anyway,
		// unless we're going to start doing palette stuff at the server.
		// So the main use would be a 24-bit true-colour desktop being viewed
		// on a 16-bit true-colour display, and unless you have lots of images
		// and hence lots of raw-encoded stuff, the size of the pixel is not
		// going to make much difference.
		//   We therefore don't bother with any restrictions, but here's the
		// start of the code if we wanted to do it.
	}

	// The endian will be set before sending
}

/*
 * InitialiseRFBConnection.
 */

Bool
InitialiseRFBConnection(const char* password)
{
  rfbProtocolVersionMsg pv;
  int major,minor;
  CARD32 authScheme, reasonLen, authResult;
  char *reason;
  CARD8 challenge[CHALLENGESIZE];
  char *passwd = (char*)password;
//  int i;
  rfbClientInitMsg ci;

  if (!ReadFromRFBServer(pv, sz_rfbProtocolVersionMsg)) return False;

  pv[sz_rfbProtocolVersionMsg] = 0;

  if (sscanf(pv,rfbProtocolVersionFormat,&major,&minor) != 2) {
    log_write(LL_ERROR,"Not a valid VNC server\n");
    return False;
  }

  log_write(LL_DEBUG,"VNC server supports protocol version %d.%d (viewer %d.%d)\n",
	  major, minor, rfbProtocolMajorVersion, rfbProtocolMinorVersion);

  major = rfbProtocolMajorVersion;
  minor = rfbProtocolMinorVersion;

  sprintf(pv,rfbProtocolVersionFormat,major,minor);

  if (!WriteToRFBServer(pv, sz_rfbProtocolVersionMsg)) return False;
  if (!ReadFromRFBServer((char *)&authScheme, 4)) return False;

  authScheme = Swap32IfLE(authScheme);

  switch (authScheme) {

  case rfbConnFailed:
    if (!ReadFromRFBServer((char *)&reasonLen, 4)) return False;
    reasonLen = Swap32IfLE(reasonLen);

    reason = malloc(reasonLen);

    if (!ReadFromRFBServer(reason, reasonLen)) return False;

    log_write(LL_ERROR,"VNC connection failed: %.*s\n",(int)reasonLen, reason);
    return False;

  case rfbNoAuth:
    log_write(LL_DEBUG,"No authentication needed\n");
    break;

  case rfbVncAuth:
	if (!ReadFromRFBServer((char *)challenge, CHALLENGESIZE)) return False;

	if (strlen(passwd)==0) {
		if (appData.passwordFile) {
			passwd = vncDecryptPasswdFromFile(appData.passwordFile);
			if (!passwd) {
				log_write(LL_ERROR,"Cannot read valid password from file \"%s\"\n",	appData.passwordFile);
				return False;
			}
		} else {
			log_write(LL_ERROR,"No password provided.\n");
			return False;
		}
	}

	if ((!passwd) || (strlen(passwd) == 0)) {
	  log_write(LL_ERROR,"Reading password failed\n");
	  return False;
	}

	if (strlen(passwd) > 8) {
	  passwd[8] = '\0';
	}

	vncEncryptBytes(challenge, passwd);

	/* Lose the password from memory */
//    IIC -- we need reuse the password for (i = strlen(passwd); i >= 0; i--) {
//      passwd[i] = '\0';
//    }

    if (!WriteToRFBServer((char *)challenge, CHALLENGESIZE)) return False;

    if (!ReadFromRFBServer((char *)&authResult, 4)) return False;

    authResult = Swap32IfLE(authResult);

    switch (authResult) {
    case rfbVncAuthOK:
      log_write(LL_DEBUG,"VNC authentication succeeded\n");
      break;
    case rfbVncAuthFailed:
      log_write(LL_ERROR,"VNC authentication failed\n");
      return False;
    case rfbVncAuthTooMany:
      log_write(LL_ERROR,"VNC authentication failed - too many tries\n");
      return False;
    default:
      log_write(LL_ERROR,"Unknown VNC authentication result: %d\n",
	      (int)authResult);
      return False;
    }
    break;

  default:
    log_write(LL_ERROR,"Unknown authentication scheme from VNC server: %d\n",
	    (int)authScheme);
    return False;
  }

  ci.shared = 1;

  if (!appData.replay && !appData.preview && !appData.deleteFile) {
    if (!WriteToRFBServer((char *)&ci, sz_rfbClientInitMsg)) return False;

    if (!ReadFromRFBServer((char *)&si, sz_rfbServerInitMsg)) return False;

    si.framebufferWidth = Swap16IfLE(si.framebufferWidth);
    si.framebufferHeight = Swap16IfLE(si.framebufferHeight);
    si.format.redMax = Swap16IfLE(si.format.redMax);
    si.format.greenMax = Swap16IfLE(si.format.greenMax);
    si.format.blueMax = Swap16IfLE(si.format.blueMax);
    si.nameLength = Swap32IfLE(si.nameLength);

	log_write(LL_MSG, "Screen size:%dx%d.", si.framebufferWidth, si.framebufferHeight);

	appData.last_w = si.framebufferWidth;	// remember last real dimension
	appData.last_h = si.framebufferHeight;
	appData.frame_w = si.framebufferWidth;
	appData.frame_h = si.framebufferHeight;

    appData.cgw = max(si.framebufferWidth, appData.cgw);
    appData.cgh = max(si.framebufferHeight, appData.cgh);
    appData.cgx1 = appData.cgx0 + appData.cgw;
    appData.cgy1 = appData.cgy0 + appData.cgh;
    si.framebufferWidth = appData.cgw;
    si.framebufferHeight = appData.cgh;

    SetMovieSize(appData.cgw, appData.cgh);

    desktopName = malloc(si.nameLength + 1);

    if (!ReadFromRFBServer(desktopName, si.nameLength)) return False;

    desktopName[si.nameLength] = 0;

    log_write(LL_DEBUG,"Desktop name \"%s\"\n",desktopName);

    log_write(LL_DEBUG,"VNC server default format:\n");
    PrintPixelFormat(&si.format);
  }

  log_write(LL_DEBUG,"Connected to VNC server, using protocol version %d.%d\n",
    rfbProtocolMajorVersion, rfbProtocolMinorVersion);

  return True;
}


Bool SendFramebufferUpdateRequest(int x, int y, int w, int h, Bool incremental)
{
  rfbFramebufferUpdateRequestMsg fur;
  fur.type = rfbFramebufferUpdateRequest;
  fur.incremental = incremental ? 1 : 0;
  fur.x = Swap16IfLE(x);
  fur.y = Swap16IfLE(y);
  fur.w = Swap16IfLE(w);
  fur.h = Swap16IfLE(h);

  if (!WriteToRFBServer((char *)&fur, sz_rfbFramebufferUpdateRequestMsg))
    return False;

  return True;
}


Bool SendSetPixelFormat()
{
  rfbSetPixelFormatMsg spf;

  spf.type = rfbSetPixelFormat;
  spf.format = myFormat;
  spf.format.redMax = Swap16IfLE(spf.format.redMax);
  spf.format.greenMax = Swap16IfLE(spf.format.greenMax);
  spf.format.blueMax = Swap16IfLE(spf.format.blueMax);

  return WriteToRFBServer((char *)&spf, sz_rfbSetPixelFormatMsg);
}


Bool SendSetEncodings()
{
	char buf[sz_rfbSetEncodingsMsg + MAX_ENCODINGS * 4];
	rfbSetEncodingsMsg *se = (rfbSetEncodingsMsg *)buf;
	CARD32 *encs = (CARD32 *)(&buf[sz_rfbSetEncodingsMsg]);
	int len = 0;
	int i;

	se->type = rfbSetEncodings;
	se->nEncodings = 0;

	if (appData.encodings) {
		char *encStr = appData.encodings;
		int encStrLen;
		do	{
			char *nextEncStr = strchr(encStr, ' ');
			if (nextEncStr) {
				encStrLen = nextEncStr - encStr;
				nextEncStr++;
			} else {
				encStrLen = strlen(encStr);
			}

			if (strnicmp(encStr,"raw",encStrLen) == 0) {
				encs[se->nEncodings++] = Swap32IfLE(rfbEncodingRaw);
			} else if (strnicmp(encStr,"hextile",encStrLen) == 0) {
				encs[se->nEncodings++] = Swap32IfLE(rfbEncodingHextile);
			} else if (strnicmp(encStr,"corre",encStrLen) == 0) {
				encs[se->nEncodings++] = Swap32IfLE(rfbEncodingCoRRE);
			} else if (strnicmp(encStr,"rre",encStrLen) == 0) {
				encs[se->nEncodings++] = Swap32IfLE(rfbEncodingRRE);
			} else if (strnicmp(encStr,"zrle",encStrLen) == 0) {
				encs[se->nEncodings++] = Swap32IfLE(rfbEncodingZRLE);
			} else {
				log_write(LL_ERROR,"Unknown encoding '%.*s'\n",encStrLen,encStr);
			}

			encStr = nextEncStr;
		} while (encStr && se->nEncodings < MAX_ENCODINGS);
	} else {

		encs[se->nEncodings++] = Swap32IfLE(rfbEncodingCopyRect);
		encs[se->nEncodings++] = Swap32IfLE(currentEncoding);
		for (i = 0; i < NUM_SUPPORTED_ENCODINGS; i++) {
			if (supportedEncodings[i] != currentEncoding)
				encs[se->nEncodings++] = Swap32IfLE(supportedEncodings[i]);
		}
	}

	len = sz_rfbSetEncodingsMsg + se->nEncodings * 4;

	se->nEncodings = Swap16IfLE(se->nEncodings);

	return WriteToRFBServer(buf, len);
}


/*
 * SendPointerEvent.
 */

Bool
SendPointerEvent(int x, int y, int buttonMask)
{
  rfbPointerEventMsg pe;

  pe.type = rfbPointerEvent;
  pe.buttonMask = buttonMask;
  if (x < 0) x = 0;
  if (y < 0) y = 0;
  pe.x = Swap16IfLE(x);
  pe.y = Swap16IfLE(y);
  return WriteToRFBServer((char *)&pe, sz_rfbPointerEventMsg);
}


/*
 * SendKeyEvent.
 */

Bool
SendKeyEvent(CARD32 key, Bool down)
{
  rfbKeyEventMsg ke;

  ke.type = rfbKeyEvent;
  ke.down = down ? 1 : 0;
  ke.key = Swap32IfLE(key);
  return WriteToRFBServer((char *)&ke, sz_rfbKeyEventMsg);
}


/*
 * SendClientCutText.
 */

Bool
SendClientCutText(char *str, int len)
{
  rfbClientCutTextMsg cct;

  if (serverCutText)
    free(serverCutText);
  serverCutText = NULL;

  cct.type = rfbClientCutText;
  cct.length = Swap32IfLE(len);
  return  (WriteToRFBServer((char *)&cct, sz_rfbClientCutTextMsg) &&
	   WriteToRFBServer(str, len));
}


void AutoSelectFormatAndEncodings()
{
  /* Above 16Mbps (timing for at least a second), same machine, switch to raw
     Above 10Mbps, switch to hextile
     Below 4Mbps, switch to ZRLE
     Above 1Mbps, switch from 8bit */

  int kbitsPerSecond = KbitsPerSecond();

  if (!appData.encodings) {

    if (kbitsPerSecond > 16000 && sameMachine && TimeWaitedIn100us() >= 10000)
    {
      if (currentEncoding != rfbEncodingRaw) {
        log_write(LL_DEBUG,"Throughput %d kbit/s - changing to Raw\n",
                kbitsPerSecond);
        currentEncoding = rfbEncodingRaw;
        pendingEncodingChange = True;
      }
    } else if (kbitsPerSecond > 10000) {
      if (currentEncoding != rfbEncodingHextile) {
        log_write(LL_DEBUG,"Throughput %d kbit/s - changing to Hextile\n",
                kbitsPerSecond);
        currentEncoding = rfbEncodingHextile;
        pendingEncodingChange = True;
      }
    } else if (kbitsPerSecond < 4000) {
      if (currentEncoding != rfbEncodingZRLE) {
        log_write(LL_DEBUG,"Throughput %d kbit/s - changing to ZRLE\n",
                kbitsPerSecond);
        currentEncoding = rfbEncodingZRLE;
        pendingEncodingChange = True;
      }
    }
  }
/*
  if (kbitsPerSecond > 1000) {
    if (appData.useBGR233 && vis->class == TrueColor) {
      // only makes sense if using a TrueColor visual
      log_write(LL_DEBUG,"Throughput %d kbit/s - changing from 8bit\n",
              kbitsPerSecond);
      appData.useBGR233 = False;
      pendingFormatChange = True;
    }
  }
*/
}


int updateNeededX1, updateNeededY1, updateNeededX2, updateNeededY2;
Bool updateNeeded = False;
Bool updateDefinitelyExpected = False;

void UpdateNeeded(int x, int y, int w, int h)
{
  if (!updateNeeded) {
    updateNeededX1 = x;
    updateNeededY1 = y;
    updateNeededX2 = x+w;
    updateNeededY2 = y+h;
    updateNeeded = True;
  } else {
    if (x < updateNeededX1) updateNeededX1 = x;
    if (y < updateNeededY1) updateNeededY1 = y;
    if (x+w > updateNeededX2) updateNeededX2 = x+w;
    if (y+h > updateNeededY2) updateNeededY2 = y+h;
  }
}

Bool CheckUpdateNeeded()
{
  if (updateNeeded && !updateDefinitelyExpected) {
    if (!SendFramebufferUpdateRequest(updateNeededX1, updateNeededY1,
                                      updateNeededX2-updateNeededX1,
                                      updateNeededY2-updateNeededY1, False))
      return False;
    updateDefinitelyExpected = True;
    updateNeeded = False;
  }
  return True;
}

void SendFullFramebufferUpdateRequest()
{
    SendFramebufferUpdateRequest(0, 0, si.framebufferWidth,
					si.framebufferHeight, False);
}

Bool RequestNewUpdate()
{
  updateDefinitelyExpected = False;

  if (pendingEncodingChange) {
    pendingEncodingChange = False;
    if (!SendSetEncodings()) return False;
  }

  if (pendingFormatChange)
  {
    rfbPixelFormat oldFormat = myFormat;
    pendingFormatChange = False;
//  SetMyFormatFromVisual();	// IIC

    myFormat.bitsPerPixel = 32;
    myFormat.depth = 24;

    myFormat.trueColour = 1;
    myFormat.bigEndian = 0;		// TODO: fix this for Windows
    myFormat.redShift = 16;
    myFormat.greenShift = 8;
    myFormat.blueShift = 0;
    myFormat.redMax = 255;
    myFormat.greenMax = 255;
    myFormat.blueMax = 255;

    if (!PF_EQ(oldFormat, myFormat)) {
      if (!SendSetPixelFormat()) return False;
      UpdateNeeded(0, 0, si.framebufferWidth, si.framebufferHeight);
    }

  }

  if (updateNeeded) {
    if (!CheckUpdateNeeded()) return False;
  } else {
    if (!SendFramebufferUpdateRequest(0, 0, si.framebufferWidth,
                                      si.framebufferHeight, True))
      return False;
  }

  return True;
}


extern void SetTime(int offset);

void DropFrameBuffer()
{
	char tmpbuf2[2048];
	int len = spoolFrameLength;
	while (len>0)
	{
		int bytes = len > 2048 ? 2048 : len;
		if (!ReadFromRFBServer(tmpbuf2, bytes))
			return;

		len -= bytes;
	}

}

/*
 * HandleRFBServerMessage.
 */

Bool HandleRFBServerMessage()
{
	rfbServerToClientMsg msg;
	int offset, len, padding, readbytes;
	char tmpbuf2[128];
    CARD8 new_session = 0;

	log_write(LL_DEBUG,"Entering HandleRFBServerMessage\n");

	if ((appData.replay||appData.preview) && spoolFrameLength<=0)
	{
		// read time offset
		if (!ReadFromRFBServer(tmpbuf2, 4))
			return False;
		offset = buf_get_CARD32(tmpbuf2);
		log_write(LL_DEBUG,"Time offset: %d.%03d \n",offset/1000, offset%1000);

		SetTime(offset);

		// read data length
		if (!ReadFromRFBServer(tmpbuf2, 4))
			return False;
		len = buf_get_CARD32(tmpbuf2);
		if (len==0)
			return False;

		log_write(LL_DEBUG,"Frame Data: %d bytes\n",len);
		//also get the padding
		padding = 3 - ((len - 1) & 0x03);
		if (padding>0)
		{
			if (!ReadFromRFBServer(tmpbuf2, padding))
				return False;
		}
		// read the new session flag, ignore this one
		len -= 1;	// data len includes one byte for new session flag
		if (!ReadFromRFBServer(&new_session, 1))
		{
			return False;
		};
		spoolFrameLength = len;
	}


  while(1) {
    new_session = 0;    // reset the flag, dont want to stuck in the loop

	if (!ReadFromRFBServer((char *)&msg, 1))
		return False;

	switch (msg.type) {
	case rfbEnableRecording:
		if (!ReadFromRFBServer(tmpbuf2, 1))
      		return False;
		if (tmpbuf2[0])
			TurnRecordingOn(0);
		else
			TurnRecordingOff(0);
		break;
	case rfbFramebufferUpdate:
	{
		rfbFramebufferUpdateRectHeader rect;
		int linesToRead;
		int bytesPerLine;
		int i;

		if (appData.autoDetect)
      		StartTiming();

		if (!ReadFromRFBServer(((char *)&msg.fu) + 1, sz_rfbFramebufferUpdateMsg - 1))
      		return False;

		msg.fu.nRects = Swap16IfLE(msg.fu.nRects);

		for (i = 0; i < msg.fu.nRects; i++) {
			if (appData.replay||appData.preview) {
				if (spoolFrameLength<=0) {
					// read time offset
					if (!ReadFromRFBServer(tmpbuf2, 4))
						return False;
					offset = buf_get_CARD32(tmpbuf2);
					log_write(LL_DEBUG,"Time offset: %d.%03d \n",offset/1000, offset%1000);

                    // Unreasonable time offset most likely means a corrupt file
                    if (offset > 24*60*60*1000) {
						log_write(LL_ERROR,"Excessive time offset %d read, stopping session", offset);
                        return False;
                    }

					SetTime(offset);

					// read data length
					if (!ReadFromRFBServer(tmpbuf2, 4))
						return False;
					len = buf_get_CARD32(tmpbuf2);
					if (len==0)
						return False;

					log_write(LL_DEBUG,"Frame Data: %d bytes\n",len);
					//also get the padding
					padding = 3 - ((len - 1) & 0x03);
					if (padding>0)
					{
						if (!ReadFromRFBServer(tmpbuf2, padding))
							return False;
					}
					// read the new session flag
					len -= 1;	// data len includes one byte for new session flag
					if (!ReadFromRFBServer(&new_session, 1))
					{
						return False;
					};
					spoolFrameLength = len;
					if (new_session)    // if it is starts a new session, get out of this loop
					{
						log_write(LL_ERROR,"Previous session was not complete, start a new session\n");
						break;
					}
				}
			}

      		if (!ReadFromRFBServer((char *)&rect, sz_rfbFramebufferUpdateRectHeader))
				return False;

            // Reflector may output an extra frame header if recording is enabled during a session
            if (rect.r.x == 0 && rect.r.y == -1) {
                log_write(LL_DEBUG, "Extra frame buffer update header, skipping 4 bytes\n");
                char *pos = (char *)&rect;
                memcpy(pos, pos+4, sz_rfbFramebufferUpdateRectHeader-4);
                if (!ReadFromRFBServer(pos + sz_rfbFramebufferUpdateRectHeader-4, 4))
                    return False;
            }

      		rect.r.x = Swap16IfLE(rect.r.x);
      		rect.r.y = Swap16IfLE(rect.r.y);
      		rect.r.w = Swap16IfLE(rect.r.w);
      		rect.r.h = Swap16IfLE(rect.r.h);

      		rect.encoding = Swap32IfLE(rect.encoding);

			if (rect.encoding == rfbEncodingLastRect)
			{
			    log_write(LL_DEBUG,"Last rec received\n");
			    break;
			}

			if (rect.encoding == rfbEncodingNewFBSize)
			{
				if (ReadNewFBSize(&rect)) {
				    log_write(LL_ERROR, "Error reading fbsize\n");
                    DropFrameBuffer();
				    continue;
				}
                break;
			}

			if ((rect.r.x + rect.r.w > si.framebufferWidth) ||
	  			(rect.r.y + rect.r.h > si.framebufferHeight))
			{
	  			log_write(LL_ERROR,"Rect too large: %dx%d at (%d, %d) - ignoring\n",
		  			rect.r.w, rect.r.h, rect.r.x, rect.r.y);

				/* May be bad data, but we will try to continue with the next frame */
				DropFrameBuffer();
				continue;
			}

      		if ((rect.r.h * rect.r.w) == 0) {
	  			log_write(LL_ERROR,"Zero rect size: %dx%d at (%d, %d) - ignoring\n",
		  			rect.r.w, rect.r.h, rect.r.x, rect.r.y);
//				DropFrameBuffer();
//				break;
//				continue;
      		}

      		switch (rect.encoding) {

      		case rfbEncodingRaw:

				bytesPerLine = rect.r.w * myFormat.bitsPerPixel / 8;
				linesToRead = BUFFER_SIZE / bytesPerLine;

				while (rect.r.h > 0) {
					if (linesToRead > rect.r.h)
						linesToRead = rect.r.h;

					if (!ReadFromRFBServer(buffer,bytesPerLine * linesToRead))
						return False;

					CopyDataToScreen(buffer, rect.r.x, rect.r.y, rect.r.w, linesToRead);

					rect.r.h -= linesToRead;
					rect.r.y += linesToRead;

				}
				break;

      		case rfbEncodingRRE:
      		{
				switch (myFormat.bitsPerPixel) {
				case 8:
					if (!HandleRRE8(rect.r.x,rect.r.y,rect.r.w,rect.r.h))
						return False;
					break;
				case 16:
					if (!HandleRRE16(rect.r.x,rect.r.y,rect.r.w,rect.r.h))
						return False;
					break;
				case 32:
					if (!HandleRRE32(rect.r.x,rect.r.y,rect.r.w,rect.r.h))
						return False;
					break;
				}
				break;
			}

      		case rfbEncodingCoRRE:
      		{
				switch (myFormat.bitsPerPixel) {
				case 8:
					if (!HandleCoRRE8(rect.r.x,rect.r.y,rect.r.w,rect.r.h))
						return False;
					break;
				case 16:
					if (!HandleCoRRE16(rect.r.x,rect.r.y,rect.r.w,rect.r.h))
						return False;
					break;
				case 32:
					if (!HandleCoRRE32(rect.r.x,rect.r.y,rect.r.w,rect.r.h))
						return False;
					break;
				}
				break;
      		}

      		case rfbEncodingHextile:
      		{
				switch (myFormat.bitsPerPixel) {
				case 8:
					if (!HandleHextile8(rect.r.x,rect.r.y,rect.r.w,rect.r.h))
						return False;
					break;
				case 16:
					if (!HandleHextile16(rect.r.x,rect.r.y,rect.r.w,rect.r.h))
						return False;
					break;
				case 32:
					if (!HandleHextile32(rect.r.x,rect.r.y,rect.r.w,rect.r.h))
						return False;
					break;
				}
				break;
      		}

      		case rfbEncodingZRLE:
				if (!zrleDecode(rect.r.x,rect.r.y,rect.r.w,rect.r.h))
					return False;
				break;

      		default:
      			ReadScreenUpdate(&rect);
      		}
		}

		if (appData.autoDetect) {
      		StopTiming();
      		AutoSelectFormatAndEncodings();
		}

		if (!(appData.replay||appData.preview))
		{
			if (!RequestNewUpdate()) return False;
		}
    	break;
	}

	case rfbSetColourMapEntries:
	{
		int i;
		CARD16 rgb[3];

		if (!ReadFromRFBServer(((char *)&msg) + 1, sz_rfbSetColourMapEntriesMsg - 1))
			return False;

		msg.scme.firstColour = Swap16IfLE(msg.scme.firstColour);
		msg.scme.nColours = Swap16IfLE(msg.scme.nColours);

		for (i = 0; i < msg.scme.nColours; i++) {
		  if (!ReadFromRFBServer((char *)rgb, 6))
			return False;
		}
		break;
	}

	case rfbResizeFrameBuffer:
	{
		rfbResizeFrameBufferMsg rsmsg;
		if (!ReadFromRFBServer(((char *)&msg) + 1, sz_rfbResizeFrameBufferMsg - 1))
			return False;

		si.framebufferWidth = Swap16IfLE(rsmsg.framebufferWidth);
		si.framebufferHeight = Swap16IfLE(rsmsg.framebufferHeigth);

        si.framebufferWidth += si.framebufferWidth % 2;
        si.framebufferHeight += si.framebufferHeight % 2;

		log_write(LL_MSG, "rfbResizeFrameBuffer: %dx%d.", si.framebufferWidth, si.framebufferHeight);

	    SetMovieSizeEx(si.framebufferWidth, si.framebufferHeight);

		break;
	}

	default:
		log_write(LL_ERROR,"Unknown message type %d from VNC server -- ignoring\n",msg.type);

		DropFrameBuffer();
		break;
	}

	if (new_session)
		continue;   // read the new msg type

	break;  // break the while loop
  }
  return True;
}


#define GET_PIXEL8(pix, ptr) ((pix) = *(ptr)++)

#define GET_PIXEL16(pix, ptr) (((CARD8*)&(pix))[0] = *(ptr)++, \
			       ((CARD8*)&(pix))[1] = *(ptr)++)

#define GET_PIXEL32(pix, ptr) (((CARD8*)&(pix))[0] = *(ptr)++, \
			       ((CARD8*)&(pix))[1] = *(ptr)++, \
			       ((CARD8*)&(pix))[2] = *(ptr)++, \
			       ((CARD8*)&(pix))[3] = *(ptr)++)

/* CONCAT2 concatenates its two arguments.  CONCAT2E does the same but also
   expands its arguments if they are macros */

#define CONCAT2(a,b) a##b
#define CONCAT2E(a,b) CONCAT2(a,b)

#define BPP 8
#include "rre.c"
#include "corre.c"
#include "hextile.c"
#undef BPP
#define BPP 16
#include "rre.c"
#include "corre.c"
#include "hextile.c"
#undef BPP
#define BPP 32
#include "rre.c"
#include "corre.c"
#include "hextile.c"
#undef BPP


/*
 * PrintPixelFormat.
 */

void
PrintPixelFormat(format)
    rfbPixelFormat *format;
{
  if (format->bitsPerPixel == 1) {
    log_write(LL_DEBUG,"  Single bit per pixel.\n");
    log_write(LL_DEBUG,
	    "  %s significant bit in each byte is leftmost on the screen.\n",
	    (format->bigEndian ? "Most" : "Least"));
  } else {
    log_write(LL_DEBUG,"  %d bits per pixel.\n",format->bitsPerPixel);
    if (format->bitsPerPixel != 8) {
      log_write(LL_DEBUG,"  %s significant byte first in each pixel.\n",
	      (format->bigEndian ? "Most" : "Least"));
    }
    if (format->trueColour) {
      log_write(LL_DEBUG,"  True colour: max red %d green %d blue %d",
	      format->redMax, format->greenMax, format->blueMax);
      log_write(LL_DEBUG,", shift red %d green %d blue %d\n",
	      format->redShift, format->greenShift, format->blueShift);
    } else {
      log_write(LL_ERROR,"  Colour map (not true colour).\n");
    }
  }
}
