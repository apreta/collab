/*
 *  Vnc2mov - writevid.c (generates movie using ffmpeg)
 *
 *  Original copyright from vnc2swf:
 *
 *  Copyright (C) 2002 by Yusuke Shinyama (yusuke at cs . nyu . edu)
 *  All Rights Reserved.
 *
 *  This is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this software; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 *  USA.
 *
 *  All modifications to generate video Copyright (C) 2011 Apreta.com
 *
*/


#ifndef WIN32
#include <unistd.h>
#include <sys/time.h>
#else
#include "Winsock2.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <math.h>

#include "vncviewer.h"
#include "waveheader.h"
#include "ffmpeg.h"


#define DEBUG_SWF 0
#define DEBUG_CLIP 0
#define DEBUG_DBL 0
#define DEBUG_ZBUFFER 0

#define CHANNELS 4

static char movie_filename[1024];
static char sound_filename[1024];
static int movie_width, movie_height;

static FILE* soundf = 0;
static FILE* final_soundfile = 0;

static unsigned long tmpbuf_len;
static char* tmpbuf_src = 0;
static char* tmpbuf_compressed = 0;
static long intervalusec;
static long frame_ts;

/*
 *  tv00 = init time of movie; tv0 = start of current frame; tv = current time
 */
static struct timeval tv0, tv, tv00;
#ifndef WIN32
static struct timezone dummytz;
#endif

/*
 * Frame buffer
 */
char* imageData = 0;

static int total_frames;
static long last_frame;
static int trigger;


// IIC  - use slow rate for waiting period
void SetFrameRate(int rate)
{
    intervalusec = 1000000L / rate;
}

// ** set movie start time **
void SetStartTime()
{
    last_frame = 0;
	tv0.tv_sec = tv0.tv_usec = 0;
	tv = tv00 = tv0;
}

// ** update current time as movie spool is read **
void SetTime(int offset)
{
	if (offset==0)
		return;

	tv = tv00;
	tv.tv_sec += offset / 1000;
	tv.tv_usec += (offset % 1000) * 1000L;
	if (1000000L <= tv.tv_usec) {
		tv.tv_usec -= 1000000L;
		tv.tv_sec++;
	}

    long this_frame = offset / frame_ts;

	if (!appData.recordingStarted)	// skip the initial blank screen only
	{
		tv0 = tv;
	}
    else if (this_frame > last_frame)
    {
        checkNextFrame();
        last_frame = this_frame;
    }
}

int GetLastDuration()
{
	int dur = (tv.tv_sec - tv00.tv_sec);
    log_write(LL_DEBUG,"GetLastDuration: %d\n", dur);
	return dur;
}

void writeAudioFrame()
{
    // Stick with writing 1 second at a time
    int samples = 11025;
    short audioData[samples];
    int readSamples = fread(audioData, sizeof(short), samples, soundf);
    if (readSamples > 0)
        ffmpeg_write_audio(audioData, readSamples);
}

/* checkNextFrame:
 *   Add frames necessary.
 */
void checkNextFrame(void)
{
    if (!appData.replay&&!appData.preview) {
#ifndef WIN32
        gettimeofday(&tv, &dummytz);
#else
        unsigned long ticks;
	    ticks = GetTickCount();
	    tv.tv_sec = ticks / 1000000L;
	    tv.tv_usec = ticks % 1000000L;
#endif
    }

    if (tv0.tv_sec < 0) {		/* initialize */
        tv0 = tv;
        tv0.tv_usec += intervalusec;
        if (1000000L <= tv0.tv_usec) {
            tv0.tv_usec -= 1000000L;
            tv0.tv_sec++;
        }
    }

    while(tv0.tv_sec < tv.tv_sec || (tv0.tv_sec == tv.tv_sec && tv0.tv_usec < tv.tv_usec)) {
        if (appData.recordingMethod == RECORD_BUFFERED) {
//            flush_pending_image();
        }
		if ((!appData.replay&&!appData.preview) || appData.delay>=0)
		{
		    // Since the audio sample rate isn't particularly divisable we'll just output 1 second at a time
            if (soundf && (appData.nframes % appData.frameRate == 0))
            {
                log_write(LL_DEBUG, "Audio frame: %d\n", appData.nframes / appData.frameRate);
                writeAudioFrame();
            }

            if (imageData)
                ffmpeg_write_video(imageData, movie_width, movie_height);

			appData.nframes++;
		}

        log_write(LL_DEBUG, "checkNextFrame: frame: %d\n", appData.nframes);
		appData.delay ++;
        tv0.tv_usec += intervalusec;
        if (1000000L <= tv0.tv_usec) {
            tv0.tv_usec -= 1000000L;
            tv0.tv_sec++;
        }
    }
}

void InsertEmptyFrame(Bool cleanup)
{
#if 0
   if (cleanup) {
     WriteRectangle(0, 0, 0,si.framebufferWidth, si.framebufferHeight );
   }
#endif

   if (appData.replay||appData.preview)
   {
       tv.tv_usec += intervalusec;
       if (1000000L <= tv.tv_usec) {
           tv.tv_usec -= 1000000L;
           tv.tv_sec++;
       }
   }

   checkNextFrame();
}

void
SwitchRecording(void)
{
	if (!appData.replay && !appData.preview) {
	  if (appData.recording) {
#ifndef WIN32
		gettimeofday(&tv0, &dummytz);
#else
		unsigned long ticks;
		ticks = GetTickCount();
		tv0.tv_sec = ticks / 1000000L;
		tv0.tv_usec = ticks % 1000000L;
#endif
//		flush_pending_image();
	  }
	}
	trigger = False;
}

// Called when frame buffer size changes within a RFB stream
void SetMovieSizeEx(int w, int h)
{
    // store real current frame size
    appData.frame_h = h;
    appData.frame_w = w;

	// a meeting can have many share sessions, and presenters, the screen size of presenter maybe different
	// use the maximum screen size
	int cgw = max(w, appData.cgw);
	int cgh = max(h, appData.cgh);

    if (cgw > appData.cgw || cgh > appData.cgh)
    {
        log_write(LL_DEBUG, "Movie size enlarged to %dx%d, must restart generation", cgw, cgh);

        save_movie_size(cgw, cgh);

        // Close current movie output
        WriteFinishMovie(False);
        DisconnectRFBServer();

        exit(2);
    }

	si.framebufferWidth = cgw;
	si.framebufferHeight = cgh;

	if (w != appData.last_w || h != appData.last_h)	// compare the real size
	{
		log_write(LL_MSG, "Cleaning up screen:%dx%d.", appData.last_w, appData.last_h);

		// Force buffer recreation
		free(imageData);
		imageData = NULL;

 		// clean up the current screen
		//DisplayRectangle(0xFFFFFF, 0, 0, cgw, cgh);

		appData.last_w = w;	// remember last real dimension
		appData.last_h = h;

		appData.cgw = cgw;
		appData.cgh = cgh;
		appData.cgx1 = appData.cgx0 + appData.cgw;
		appData.cgy1 = appData.cgy0 + appData.cgh;

		SetMovieSize(appData.cgw, appData.cgh);
	}

	// clean up the current screen
	DisplayRectangle(0xFFFFFF, 0, 0, cgw, cgh);

    log_write(LL_DEBUG,"SetMovieSizeEx: %dx%d\n", cgw, cgh);
}

int save(const char *attribute, int value)
{
	char filename[1024];
	sprintf(filename, "%s_%s.txt", appData.dumpFile, attribute);

    FILE * fp = fopen(filename, "w");
    if (fp == NULL)
    {
        log_write(LL_ERROR, "Unable to open attribute file %s: %s",
                  filename, strerror(errno));
        return -1;
    }
    fprintf(fp, "%d\n", value);
    fclose(fp);

    return 0;
}

int load(const char *attribute, int *value)
{
	char filename[1024];
	sprintf(filename, "%s_%s.txt", appData.dumpFile, attribute);

    FILE * fp = fopen(filename, "r");
    if (fp == NULL)
    {
        log_write(LL_ERROR, "Unable to open attribute file %s: %s",
                  filename, strerror(errno));
        return -1;
    }
    fscanf(fp, "%d", value);
    fclose(fp);

    return 0;
}

void save_movie_size(int w, int h)
{
	save("width", w);
	save("height", h);
}

int load_movie_size(int *w, int *h)
{
    if (load("width", w))
        return -1;
    if (load("height", h))
        return -1;
    return 0;
}

void SetMovieSize(int w, int h)
{
    tmpbuf_len = (unsigned long)floor((unsigned long)w*(unsigned long)h*CHANNELS*1.01+12);
	if (movie_width!=w || movie_height!=h)
	{
		if (tmpbuf_src)
		{
			free(tmpbuf_src); tmpbuf_src = NULL;
		}
		if (tmpbuf_compressed)
		{
			free(tmpbuf_compressed); tmpbuf_compressed = NULL;
		}
		if (imageData!=0)
		{
		  free(imageData);
		  imageData = 0;
		  log_write(LL_DEBUG, "Freed previous movie buffer.");
		}
	}

	if (tmpbuf_src==NULL)
		tmpbuf_src = malloc(tmpbuf_len);

	if (tmpbuf_compressed==NULL)
		tmpbuf_compressed = malloc(tmpbuf_len);

    movie_width = w;
    movie_height = h;

    if (!tmpbuf_src || !tmpbuf_compressed) {
        log_write(LL_ERROR, "SetMovieSize: Out of memory (w=%d,h=%d)", w, h);
        return;
    }

    save_movie_size(w, h);
    log_write(LL_DEBUG, "set movie size to %dx%d\n", w, h);

#ifdef SPOOL
    if (appData.spool) {
      WriteMovieSize(w, h);
    }
#endif
}

Bool WriteInitMovie(int w, int h)
{
    wavehdr header;

    ffmpeg_init();

	sprintf(movie_filename, "%s.%s", appData.dumpFile, appData.fileType == FILE_MP4 ? "mp4" : "flv");
    movie_width = w;
    movie_height = h;

    {
        /* warn if the file is going to be overwritten */
        struct stat st;
        if (stat(movie_filename, &st) != -1) {
            log_write(LL_DEBUG, "=== WriteInitMovie: Warning: file %s already exists, overwriting.\n",
        	    movie_filename);
        }
    }
    log_write(LL_INFO,"=== WriteInitMovie: Pid=%d, Opening file: \"%s\" for a movie size (%d, %d), frame rate %d, format %d, bitrate %d...\n",
	    getpid(), movie_filename, movie_width, movie_height, appData.frameRate, appData.format, appData.bitRate);

    soundf = NULL;
    final_soundfile = NULL;

    if (soundf != NULL) {
        fclose(soundf);
    }
    if (final_soundfile!=NULL) {
       fclose(final_soundfile);
    }
    if (*appData.soundFile) {
		sprintf(sound_filename, "%s.wav", appData.soundFile);
        soundf = fopen(sound_filename, "rb");
    	if (!soundf) {
    	    log_write(LL_ERROR, "WriteInitMovie: Cannot open sound file:%s",sound_filename);
//    	    return False;
    	}
    }

    ffmpeg_set_framerate(appData.frameRate);
    ffmpeg_set_bitrate(appData.bitRate, appData.audioBitRate);
    ffmpeg_set_format(appData.format);

    if (ffmpeg_open_output(movie_filename)) {
        log_write(LL_ERROR, "WriteInitMovie: failed to open output file:%s", movie_filename);
        return False;
    }

    if (soundf) {
        log_write(LL_DEBUG,"=== WriteInitMovie: Sound file: \"%s\"\n", appData.soundFile);
    	// Skip header
    	fread(&header, sizeof(header), 1, soundf);
    }

    frame_ts = 1000L / appData.frameRate;
    intervalusec = 1000000L / appData.frameRate;
    appData.nframes = 0;
    trigger = False;
    total_frames = 0;
    last_frame = 0;
    imageData = NULL;

    appData.recording = True;
    appData.compressed_image_bytes = 0;
    appData.expanded_image_bytes = 0;

    //track_image(0, 0, movie_width, movie_height);	// IIC - not to write the initial black frame

    return True;
}

void OpenMovieStream(int w, int h)
{
    ffmpeg_prepare_stream(w, h);
}

/* WriteFinishMovie:
 *   Write the movie file.
 */
Bool WriteFinishMovie(Bool final)
{
#ifdef SPOOL
    if (appData.spool) {
      fclose(appData.spool_handle);
      return True;
    }
#endif

	if (final)
	{
	    // Always generate 10 seconds of padding even if no more audio
	    int minEmpty = 10 * appData.frameRate;

		// Generate frames until there is no more audio
		while (--minEmpty || (soundf && !feof(soundf)))
		{
		    if (minEmpty == 0)
                DisplayRectangle(0xFFFFFF, 0, 0, si.framebufferWidth, si.framebufferHeight);

            InsertEmptyFrame(0);
		}
	}

    log_write(LL_INFO, "=== WriteFinishMovie: %d frames (%.1f sec), compressed=%lu, expanded_image: %lu\n",
	    appData.nframes, (double)appData.nframes/(double)appData.frameRate, appData.compressed_image_bytes, appData.expanded_image_bytes);

    ffmpeg_close();

    if (soundf != NULL) {
        fclose(soundf);
    }
    if (final_soundfile!=NULL) {
       fclose(final_soundfile);
    }
    log_write(LL_INFO, "=== WriteFinishMovie: %s finished.\n", movie_filename);

    if (appData.nframes==0) {
      unlink(movie_filename);
      log_write(LL_INFO, "=== WriteFinishMovie: 0 frames, deleted empty movie file.");
    }

    return True;
}

/* signal handler
 */
void TurnRecordingOn(int sig)
{
    log_write(LL_DEBUG,"TurnRecordingOn called\n");
    appData.recording = True;
	appData.recordingStarted = True;
    if (!appData.replay && !appData.preview) trigger = True;
}

void TurnRecordingOff(int sig)
{
    log_write(LL_DEBUG,"TurnRecordingOff called\n");
    appData.recording = False;
    if (!appData.replay && !appData.preview) trigger = True;
}

void RecordMessage(const char * _message)
{
    log_write(LL_DEBUG,"Recording message - %s\n", _message);
}

void SetSoundStream(const char * soundfile)
{
    if (*appData.soundFile) {
        final_soundfile = fopen(soundfile, "rb");
        if (!final_soundfile) {
            log_write(LL_DEBUG,"SetSoundStream: Cannot open sound file %s\n", soundfile);
        } else {
            log_write(LL_DEBUG,"=== SetSoundStream: Sound file: \"%s\"\n", soundfile);
        }
    }
}

#ifdef SPOOL

// spool data
void OpenSpoolFile()
{
	if (appData.spool) {
		// cannot use 'ab+' here, the fseek won't seek back
		appData.spool_handle = fopen(appData.dumpFile, "rb+");
                if (appData.spool_handle==NULL) {
			appData.spool_handle = fopen(appData.dumpFile, "wb+");
		}
		fseek(appData.spool_handle, 0, SEEK_END);
	}
}

void CloseSpoolFile()
{
	if (appData.spool_handle) {
		fclose(appData.spool_handle);
	}
}

void WriteMovieSize(int w, int h)
{
  int ww=0, hh=0;
  if (appData.spool_handle == NULL)
    return;

  fseek(appData.spool_handle, 0, SEEK_SET);
  if (fread(&ww, 4, 1, appData.spool_handle)==1 &&
      fread(&hh, 4, 1, appData.spool_handle)==1)
  {
    if (ww<w || hh<h)
    {
      fseek(appData.spool_handle, 0, SEEK_SET);
      fwrite(&w, 4, 1, appData.spool_handle);
      fwrite(&h, 4, 1, appData.spool_handle);
      log_write(LL_DEBUG, "SetMovieSize (%dx%d)", w, h);
    }
  }
  else
  {
      if (fseek(appData.spool_handle, 0, SEEK_SET)==0)
      {
        fwrite(&w, 4, 1, appData.spool_handle);
        fwrite(&h, 4, 1, appData.spool_handle);
        log_write(LL_DEBUG, "SetMovieSize (%dx%d)", w, h);
      }
  }

  fseek(appData.spool_handle, 0, SEEK_END);
}

void WriteMovieStartTime()
{
  struct timeval tv;
  int type = -1;
  int empty = 0;
  if (appData.spool_handle == NULL)
    return;

  if (ftell(appData.spool_handle) == 0) {
      fwrite(&empty, 4, 1, appData.spool_handle);
      fwrite(&empty, 4, 1, appData.spool_handle);
  }

  gettimeofday(&tv, &dummytz);
  log_write(LL_DEBUG, "WriteMovieStartTime (%d:%d)", tv.tv_sec, tv.tv_usec);
  if (fwrite(&type, 4, 1, appData.spool_handle)!=1 ||
      fwrite(&tv, sizeof(struct timeval), 1, appData.spool_handle)!=1)
  {
    log_write(LL_ERROR, "WriteMovieStartTime:Could not write movie start time");
    fclose(appData.spool_handle);
    exit(1);
  }
}

void SpoolMovieData(void *buf, size_t len, int type, int x, int y, int w, int h)
{
  CARD8 data_size_buf[4];
  CARD8 timestamp_buf[8];
  CARD32 timestamp;
  int padding;
  struct timeval tv;

  if (appData.spool_handle == NULL)
    return;

  if (len==0)
	  return;

  /* Calculate current timestamp */
  gettimeofday(&tv, &dummytz);
  if (fwrite(&type, 4, 1, appData.spool_handle) != 1 ||
	fwrite(&tv, sizeof(struct timeval), 1, appData.spool_handle) != 1 ||
	fwrite(&x, 4, 1, appData.spool_handle) != 1 ||
	fwrite(&y, 4, 1, appData.spool_handle) != 1 ||
	fwrite(&w, 4, 1, appData.spool_handle) != 1 ||
	fwrite(&h, 4, 1, appData.spool_handle) != 1 ||
	fwrite(&len, 4, 1, appData.spool_handle) != 1 ||
	fwrite(buf, 1, len, appData.spool_handle) != len)
  {
    log_write(LL_ERROR, "Could not write FBS file data");
    fclose(appData.spool_handle);
	exit(1);
  }
}

void LoadMovieFromFile(FILE* f)
{
	int type, x, y, w, h, len;
	char * buf;
	struct timeval tv;

	log_write(LL_DEBUG, "Loading movie dump file to produce flash movie.");

    frame_ts = 1000L / appData.frameRate;
	intervalusec = 1000000L / appData.frameRate;
	tv0.tv_sec = tv0.tv_usec = -1;
	appData.nframes = 0;
	trigger = False;

	// get movie size
	fread(&movie_width, 4, 1, f);
	fread(&movie_height, 4, 1, f);
	log_write(LL_DEBUG, " w=%d h=%d", movie_width,movie_height);
	SetMovieSize(movie_width, movie_height);
	buf = (char*)malloc(movie_width * movie_height * 4);

	if (buf)
	{
		// start time
		fread(&type, 4, 1, f);
		fread(&tv0, sizeof(struct timeval), 1, f);
		log_write(LL_DEBUG, "type:%d timeval0:%d:%d", type, tv0.tv_sec, tv0.tv_usec);

		while(!feof(f))
		{
			fread(&type, 4, 1, f);
			fread(&tv, sizeof(struct timeval), 1, f);
			log_write(LL_DEBUG, "type:%d timeval1:%d:%d", type, tv.tv_sec, tv.tv_usec);
            if (type == -1) {
				tv0 = tv;
				continue;
			}
			fread(&x, 4, 1, f);
			fread(&y, 4, 1, f);
			fread(&w, 4, 1, f);
			fread(&h, 4, 1, f);
			fread(&len, 4, 1, f);

			log_write(LL_DEBUG, "x=%d y=%d w=%d h=%d len=%d", x,y,w,h,len);
			while(tv0.tv_sec < tv.tv_sec || (tv0.tv_sec == tv.tv_sec && tv0.tv_usec < tv.tv_usec)) {
				appData.nframes++;
//				log_write(LL_DEBUG, "checkNextFrame: frame: %d\n", appData.nframes);
				tv0.tv_usec += intervalusec;
				if (1000000L <= tv0.tv_usec) {
					tv0.tv_usec -= 1000000L;
					tv0.tv_sec++;
				}
			}

			fread(buf, len, 1, f);
			switch(type)
			{
				case 0:	// bmp
				{
					WriteRawImage(buf, len, x, y, w, h, x, y);
					break;
				}
				case 1:	// jpeg
				{
					WriteJpegRawImage(buf, len, x, y, w, h, x, y);
					break;
				}
				case 2:
				{
					WriteRawRectangle((Pixel)*(int *)buf, x, y, w, h);
					break;
				}
				default:
					log_write(LL_ERROR, "Unknown image type (%d)", type);
					break;
			}
		}
		free(buf);
	}
	else
	{
		log_write(LL_ERROR, "LoadMovieFromFile: Out of memory.(%dx%d)", movie_width,movie_height);
	}
}

#endif
