/*
 *  Copyright (C) 2002 RealVNC Ltd.
 *  Copyright (C) 1999 AT&T Laboratories Cambridge.  All Rights Reserved.
 *
 *  This is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this software; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 *  USA.
 */

/*
 * desktop.c - functions to deal with "desktop" window.
 */

#include "vncviewer.h"

// Frame buffer
extern char* imageData;

/*
 * CopyDataToScreen.
 */
// Write data to frame buffer and movie
void
CopyDataToScreen(char *buf, int x, int y, int width, int height)
{
	if (x<0 || y<0)
	{
		return;
	}

    int clipWidth = appData.frame_w;
    int clipHeight = appData.frame_h;

	if ((x + width) > clipWidth)
	{
		width = clipWidth - x;
		if (width < 0)
			return;
	}

	if ((y + height) > clipHeight)
	{
		height = clipHeight - y;
		if (height < 0)
			return;
	}

	int scrWidthInBytes = si.framebufferWidth * myFormat.bitsPerPixel / 8;
	if (imageData==0)
	{
		imageData = malloc(scrWidthInBytes * si.framebufferHeight);
		if (!imageData) {
			log_write(LL_ERROR, "malloc failed\n");
			exit(1);
		}
	}

	{
		int h;
		int widthInBytes = width * myFormat.bitsPerPixel / 8;

		char *scr = (imageData + y * scrWidthInBytes
				 + x * myFormat.bitsPerPixel / 8);

		for (h = 0; h < height; h++) {
			memcpy(scr, buf, widthInBytes);
			buf += widthInBytes;
			scr += scrWidthInBytes;
		}
	}
}

// Write data to frame buffer
// when we split movie, make sure we got an initial full screen
void BufferDataToScreen(char *buf, int x, int y, int width, int height)
{
	if (x<0 || y<0)
	{
		return;
	}

    int clipWidth = appData.frame_w;
    int clipHeight = appData.frame_h;

	if ((x + width) > clipWidth)
	{
		width = clipWidth - x;
		if (width < 0)
			return;
	}

	if ((y + height) > clipHeight)
	{
		height = clipHeight - y;
		if (height < 0)
			return;
	}

	int scrWidthInBytes = si.framebufferWidth * myFormat.bitsPerPixel / 8;
	if (imageData==0)
	{
		imageData = malloc(scrWidthInBytes * si.framebufferHeight);
		if (!imageData) {
			log_write(LL_ERROR, "malloc failed\n");
			exit(1);
		}
	}

	{
		int h;
		int widthInBytes = width * myFormat.bitsPerPixel / 8;

		char *scr = (imageData + y * scrWidthInBytes
				 + x * myFormat.bitsPerPixel / 8);

		for (h = 0; h < height; h++) {
			memcpy(scr, buf, widthInBytes);
			buf += widthInBytes;
			scr += scrWidthInBytes;
		}
	}
}

// Write jpeg to movie (frame buffer not updated)
void
CopyJpegToScreen(char *buf, int len, int x, int y, int width, int height)
{
}

/*
  DisplayRectangle
*/
// Write data to frame buffer and movie
void DisplayRectangle(Pixel pix, int x, int y, int width, int height)
{
	if (x< 0 || y<0)
	{
		return;
	}

	if ((x + width) > si.framebufferWidth)
	{
		width = si.framebufferWidth - x;
		if (width < 0)
			return;
	}

	if ((y + height) > si.framebufferHeight)
	{
		height = si.framebufferHeight - y;
		if (height < 0)
			return;
	}

	int scrWidthInBytes = si.framebufferWidth * myFormat.bitsPerPixel / 8;
	if (imageData==0)
	{
		log_write(LL_DEBUG, "Allocated display buffer %d", scrWidthInBytes * si.framebufferHeight);
		imageData = malloc(scrWidthInBytes * si.framebufferHeight);
		if (!imageData) {
			log_write(LL_ERROR, "malloc failed\n");
			exit(1);
		}
	}

	{
	    log_write(LL_DEBUG, "Filling in rectangle %dx%d at %d,%d with %d", width, height, x, y, pix);

		int h,w;
		int widthInBytes = width * myFormat.bitsPerPixel / 8;
		int aline[2048], *paline = aline;
		int wsize = width * sizeof(int);
		if (width > 2048)
		{
			paline = malloc(wsize);
		}
		for (w=0;w<width;w++) {
			paline[w] = pix;
		}

		char *scr = (imageData + y * scrWidthInBytes
				 + x * myFormat.bitsPerPixel / 8);

		for (h = 0; h < height; h++) {
			memcpy(scr, paline, wsize);
			scr += scrWidthInBytes;
		}

		if (width > 2048)
			free(paline);
	}
}
