#!/bin/sh
#
#  recordwin.sh
#
#  usage:
#     recordwin.sh [-display disp] [-name winname] [-id winid] swfname
#
#     $ x11vnc -mouse -viewonly -many -deferupdate 10 -wait 10 -defer 10 &
#     $ recordwin.sh -name xterm xterm.swf
#

VNC2SWF=./vnc2swf
X11VNC=x11vnc
XWININFO=xwininfo
AWK=awk

xwopts=
swfopts=
display="$DISPLAY"
while [ $# -gt 1 ]; do
    case "$1" in
	-name) shift; xwopts="$xwopts -name $1";;
	-id) shift; xwopts="$xwopts -id $1";;
	-display|-d) shift; display="$1"; xwopts="$xwopts -display $1";;
    esac
    shift
done

if [ $# -lt 1 ]; then
    echo "usage: $0 [-display disp] [-name winname] [-id winid] swfname"
    exit 2
fi

swffile="$1"

geom=`$XWININFO $xwopts |
      $AWK '/Absolute upper-left X:/{x=$4} 
            /Absolute upper-left Y:/{y=$4}
            /Width:/{w=$2}
            /Height:/{h=$2}
            END {printf "%dx%d+%d+%d",w,h,x,y}' `

$X11VNC -display "$display" -mouse -viewonly -deferupdate 10 -wait 10 -defer 10 >/dev/null 2>&1 &

# wait x11vnc
sleep 1
$VNC2SWF "$swffile" "$display" $swfopts -nowindow -clippinggeometry $geom
