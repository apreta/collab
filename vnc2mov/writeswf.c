/* 
 *  Vnc2swf - writeswf.c (generates a SWF file with Ming)
 *  $Id: writeswf.c,v 1.11 2005-09-21 18:37:46 binl Exp $
 *
 *  Copyright (C) 2002 by Yusuke Shinyama (yusuke at cs . nyu . edu)
 *  All Rights Reserved.
 *
 *  This is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this software; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 *  USA.
*/


#ifndef WIN32 
#include <unistd.h>
#include <sys/time.h>
#else
#include "Winsock2.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <math.h>

/* ming */
#include <zlib.h>
#include <jpeglib.h>

#include "vncviewer.h"


#define DEBUG_SWF 0
#define DEBUG_CLIP 0
#define DEBUG_DBL 0
#define DEBUG_ZBUFFER 0

#define CHANNELS 4

static char movie_filename[1024];
static char sound_filename[1024];
static FILE* movie_dumpf;
static int movie_width, movie_height;

static SWFMovie movie = 0;
static FILE* soundf = 0;
#ifdef RECORD_CHAT
static SWFTextField message;     // IIC
#endif
static FILE* final_soundfile = 0;

static unsigned long tmpbuf_len;
static char* tmpbuf_src = 0;
static char* tmpbuf_compressed = 0;
static long intervalusec;
static struct timeval tv0, tv, tv00;
#ifndef WIN32
static struct timezone dummytz;
#endif
char* imageData = 0;
char* jpeg_buf = 0; 

static int img_xmin, img_ymin, img_xmax, img_ymax;

static void reset_pending_image(void);
static void track_image(int x, int y, int w, int h);
static void flush_pending_image(void);
static UINT RequiredBuffSize(UINT width, UINT height);

static int total_frames = 0;

extern int semid;
extern int shmid;
extern char* shmptr;

/* ZBuffer: covered pixels = 1 */
typedef unsigned short zpixel;
typedef struct _DisplayItem {
  SWFDisplayItem d;
  zpixel id;
  int x, y, w, h;
  int encoding;	// IIC -- 0 - raw, 1 - jpeg, 2 - solid
  Pixel color;	// IIC
  int len;		// IIC
  int nFrame;
  struct _DisplayItem* next;
  struct _DisplayItem* prev;
} DisplayItem;

static zpixel shapeid;
static zpixel* zbuffer = 0;
static DisplayItem items;

static int trigger;

int file_index = 0;		// the movie will be splitted into 2.5 hour each

/* initializeZBuffer:
 */
static Bool initializeZBuffer(void)
{
    unsigned long buflen;
    buflen = (unsigned long)movie_width*(unsigned long)movie_height*sizeof(zpixel);
    if (zbuffer) free(zbuffer);
	zbuffer = (zpixel*)malloc(buflen);
    if (!zbuffer) {
      log_write(LL_ERROR, "initializeZBuffer: Out of memory");
      return False;
    }
    items.d = NULL;
    items.next = &items;
    items.prev = &items;
    return True;
}

static void clearZBuffer(void)
{
    DisplayItem* di = items.next;

    memset(zbuffer, 0, (unsigned long)movie_width*(unsigned long)movie_height*sizeof(zpixel));
    shapeid = 1;
    while(di->d) {
	  DisplayItem* removed = di;
	  di->next->prev = di->prev;
	  di->prev->next = di->next;
	  di = di->next;
	  free((char*)removed);	
    }
}


/* paintZBuffer:
 *   Fill the given rect with the current shape id.
 */
static Bool paintZBuffer(SWFDisplayItem d, int x, int y, int w, int h, int encoding, Pixel color, int compressed_len)
{
    zpixel* p = zbuffer + y * movie_width + x;
    DisplayItem* di;
    
#if DEBUG_ZBUFFER
    log_write(LL_DEBUG,"paintZBuffer: %u: (%d,%d)-(%d,%d)\n",
	    shapeid, x, y, x+w, y+h);
#endif
    di = (DisplayItem*)malloc(sizeof(DisplayItem));
    if (!di) {
      log_write(LL_ERROR, "fillZBuffer: Out of memory");
      return False;
    }
    di->d = d;
    di->id = shapeid;
    di->x = x;
    di->y = y;
    di->w = w;
    di->h = h;
	di->encoding = encoding;
	di->color = color;
	di->len = compressed_len;
	di->nFrame = appData.nframes;
    /* WARNING: h is broken after this! */
    while(h--) {
      zpixel* p1 = p;
      int w1 = w;
      while(w1--) {
        *(p1++) = shapeid;
	  }
      p += movie_width;
    }
    
    /* add to the list */
    di->next = items.next;
    di->prev = &items;
    items.next->prev = di;
    items.next = di;
    shapeid++;
    return True;
}


/* checkZBufferCovered:
 *   Check if every pixel in the given rect is covered thereafter.
 */
static Bool checkZBufferCovered(zpixel id, int x, int y, int w, int h)
{
    zpixel* p = zbuffer + y * movie_width + x;
    /* WARNING: h is broken after this! */
    while(h--) {
      zpixel* p1 = p;
      int w1 = w;
      while(w1--) {
        if (*(p1++) <= id) return False; /* this pixel is not yet covered! */
	  }
      p += movie_width; 
    }
    return True;
}


/* checkRemoveItems:
 *   Remove covered items from the movie.
 */
static void checkRemoveItems(void)
{
    DisplayItem* di = items.next;
    
    while(di->d) {
		if (checkZBufferCovered(di->id, di->x, di->y, di->w, di->h)) {
			DisplayItem* removed = di;
#if DEBUG_ZBUFFER
			log_write(LL_DEBUG,"checkRemoveItems: covered: %u: (%d,%d)-(%d,%d)\n",
				di->id, di->x, di->y, di->x+di->w, di->y+di->h);
#endif
			if (movie) 
			{
				if (di->nFrame!=appData.nframes)
					SWFMovie_remove(movie, di->d);
				else
					SWFMovie_destoryItem(movie, di->d);
			}
			
			/* remove from the list */
			di->next->prev = di->prev;
			di->prev->next = di->next;
			di = di->next;
			free((char*)removed);
		} else {
			di = di->next;
		}
    }
}

static int matchLastItemWidth(int x, int y, int w, int encoding, int color)
{
    DisplayItem* di = items.next;
    if(di->d) {
		if (di->x==x && di->w==w && di->y<=y && (di->y+di->h)>=y 
			&& di->encoding == encoding && di->color == color
			&& di->nFrame == appData.nframes) {
			int h = y - di->y;
			DisplayItem* removed = di;
#if DEBUG_ZBUFFER
			log_write(LL_DEBUG,"checkRemoveItems: covered: %u: (%d,%d)-(%d,%d)\n",
				di->id, di->x, di->y, di->x+di->w, di->y+di->h);
#endif
			if (movie) 
			{
				SWFMovie_destoryItem(movie, di->d);
			    appData.compressed_image_bytes -= di->len;
			}
			
			/* remove from the list */
			di->next->prev = di->prev;
			di->prev->next = di->next;
			di = di->next;
			free((char*)removed);
			return h;
		}
    }
	return 0;
}

static int matchLastItemHeight(int x, int y, int h, int encoding, int color)
{
    DisplayItem* di = items.next;
    if(di->d) {
		if (di->y==y && di->h==h && di->x<=x && (di->x+di->w)>=x 
			&& di->encoding == encoding && di->color == color
			&& di->nFrame == appData.nframes) {
			int w = x - di->x;
			DisplayItem* removed = di;
#if DEBUG_ZBUFFER
			log_write(LL_DEBUG,"checkRemoveItems: covered: %u: (%d,%d)-(%d,%d)\n",
				di->id, di->x, di->y, di->x+di->w, di->y+di->h);
#endif
			if (movie) 
			{
				SWFMovie_destoryItem(movie, di->d);
			    appData.compressed_image_bytes -= di->len;
			}
			
			/* remove from the list */
			di->next->prev = di->prev;
			di->prev->next = di->next;
			di = di->next;
			free((char*)removed);
			return w;
		}
    }
	return 0;
}

/* ConvBufToRGB:
 *   Convert the area (x,y),(w,h) in a framebuffer (ZPixmap) to a RGB array.
 */
static void ConvBufToRGB(unsigned char* buf, int skip, int w, int h)
{
	int i, j;
	char* p = tmpbuf_src;

	for(i = 0; i < h; i++) {
		unsigned char* linebuf = buf;
		for(j = 0; j < w; j++) {
			unsigned long c = 0, c1, c2, c3;
			switch(myFormat.bitsPerPixel) {
			case 8:
				if (appData.useBGR233) {
//					c = BGR233ToPixel[*(linebuf++)];
				} else {
					c = *(linebuf++);
				}
				break;
			case 16:
				c1 = *(linebuf++);
				c = (*(linebuf++) << 8) | c1;
				break;
			case 32:
				c1 = *(linebuf++);
				c2 = *(linebuf++);
				c3 = *(linebuf++);
				c = (*(linebuf++) << 24) | (c3 << 16) | (c2 << 8) | c1;
				break;
			}
			/* RGB */
			*(p++) = 0;
			*(p++) = (char)(c>>myFormat.redShift) & myFormat.redMax; /* R */
			*(p++) = (char)(c>>myFormat.greenShift) & myFormat.greenMax; /* G */
			*(p++) = (char)(c>>myFormat.blueShift) & myFormat.blueMax; /* B */
		}
		buf += skip;
	}
}

/* ImageToDBL:
 *   Make a DBL image from raw framebuffers.
 *
 * DBL format:
 *   3bytes: 'DBl' (header)
 *   1byte : format (1:noalpha, 2:hasalpha)
 *   4bytes: size of the data contained below (n+6 or n+5)
 *   1byte : palette? (5:nopalette, 3:haspalette)
 *   2bytes: width
 *   2bytes: height
 *   [1byte: num_palette-1] (if has a palette)
 *   nbytes: compressed RGB or RGBA
 */
/* write_ulong */
static char* write_ulong(char* p, unsigned long x)
{
    *(p++) = (char)(x >> 24) & 0xff;
    *(p++) = (char)(x >> 16) & 0xff;
    *(p++) = (char)(x >> 8) & 0xff;
    *(p++) = (char)(x >> 0) & 0xff;
    return p;
}
/* write_ushort */
static char* write_ushort(char* p, unsigned short x)
{
    *(p++) = (char)(x >> 0) & 0xff;
    *(p++) = (char)(x >> 8) & 0xff;
    return p;
}
char* ImageToDBL(unsigned long* len, int x, int y, int w, int h, int* compressed_len)
{
    int scrWidthInBytes;
    char* buf;
    char* dblbuf;
    char* p;
    unsigned long dbllen, dbldatalen, dbltotallen;
    unsigned long imglen = (unsigned long)w * (unsigned long)h * CHANNELS;
    
    scrWidthInBytes = si.framebufferWidth * myFormat.bitsPerPixel / 8;
	if (imageData==0)
	{
		imageData = malloc(scrWidthInBytes * si.framebufferHeight);
		if (!imageData) {
			log_write(LL_ERROR, "malloc failed\n");
			exit(1);
		}
	}
    buf = imageData + y * scrWidthInBytes + x * myFormat.bitsPerPixel / 8;
    ConvBufToRGB(buf, scrWidthInBytes, w, h);
    
    dbllen = tmpbuf_len;
    compress(tmpbuf_compressed, &dbllen, tmpbuf_src, imglen);
    dbldatalen = dbllen+1+2+2;
    dbltotallen = dbldatalen+3+1+4;
    dblbuf = malloc(dbltotallen);
    if (!dblbuf) return(NULL);
    appData.compressed_image_bytes += dbllen;
    appData.expanded_image_bytes += imglen;
	*compressed_len = dbllen;

    log_write(LL_DEBUG,"DBL image data size: %dx%d (%d, %d) =%lu, %lu\n", w, h, x, y,dbllen, imglen);
    
    p = dblbuf;
    /* a DBL header */
    *(p++) = 'D';
    *(p++) = 'B';
    *(p++) = 'l';
    /* no alpha */
    *(p++) = 1;
    /* the dummy data size */
    p = write_ulong(p, dbldatalen);
    /* no pallete */
    *(p++) = 5;			
    /* the image size */
    p = write_ushort(p, w);
    p = write_ushort(p, h);
    /* the compressed data */
    memcpy(p, tmpbuf_compressed, dbllen);
    
    *len = dbltotallen;
    
#if DEBUG_DBL
    {
	/* dump each DBL file into files 
	 * CAUTION: files can be too big and many! 
	 */
	char fname[256];
	FILE* fp; 
	sprintf(fname, "f%05u.dbl", shapeid);
	fp = fopen(fname, "wb");
	fwrite(dblbuf, 1, dbltotallen, fp);
	fclose(fp);
    }
#endif  
    
    return(dblbuf);
}


/*  Lower level swf operations.
 *
 */

/* WriteRawImage:
 *   Put an image obtained from a buffer (x0,y0) into (x1,y1) in the movie.
 */
static Bool WriteRawImage(char* _dblbuf, unsigned long _dbllen, int x0, int y0, int w, int h, int x1, int y1)
{
    SWFShape shape;
    SWFFill fill;
    SWFDBLBitmap dbl;
    SWFDisplayItem d;
    SWFInput input;
    char* dblbuf;
    unsigned long dbllen;
    int compressed_len;

    log_write(LL_DEBUG,"WriteRawImage: %dx%d (%d, %d) to (%d, %d)\n", w, h, x0, y0, x1, y1);
    
    if (!_dblbuf) {
		int lh = matchLastItemWidth(x0, y0, w,0,0);
		if (lh>0) {
			y0 -= lh; y1 -= lh;
			h += lh;
			if (y0<0) 
			{
				h += y0;
				y0 = 0; y1 = 0;
			}
		}
		else
		{
			int lw = matchLastItemHeight(x0, y0, h, 0, 0);
			if (lw>0) {
				x0 -= lw; x1 -= lw;
				if (x0<0) x0 = 0;
				if (x1<0) x1 = 0;
				w += lw;
			}
		}

		dblbuf = ImageToDBL(&dbllen, x0, y0, w, h, &compressed_len);
	} else {
		dblbuf = malloc(_dbllen);
		dbllen = _dbllen;
		memcpy(dblbuf, _dblbuf, _dbllen);
	}
    if (!dblbuf) return(False);
    input = newSWFInput_allocedBuffer(dblbuf, dbllen);
    dbl = newSWFDBLBitmap_fromInput(input);
    shape = newSWFShape();
    fill = SWFShape_addBitmapFill(shape, (SWFBitmap)dbl, SWFFILL_TILED_BITMAP);
    SWFShape_setRightFill(shape, fill);
	destroySWFFill(fill);	// IIC -- fix memory leak
    if (appData.macromediaHack) {
        SWFShape_movePenTo(shape, 1, 1);
    } else {
        SWFShape_movePenTo(shape, 0, 0);
    }
    SWFShape_drawLine(shape, w, 0);
    SWFShape_drawLine(shape, 0, h);
    SWFShape_drawLine(shape,-w, 0);
    SWFShape_drawLine(shape, 0,-h);
    SWFShape_end(shape);
    d = SWFMovie_add(movie, (SWFBlock)shape);
    SWFDisplayItem_moveTo(d, x1, y1);
    
    paintZBuffer(d, x1, y1, w, h, 0, 0, compressed_len);
//    checkRemoveItems();
    return True;
}

/* WriteRawImage:
 *   Put an image obtained from a buffer (x0,y0) into (x1,y1) in the movie.
 */
static Bool WriteJpegRawImage(char* buf, int len, int x0, int y0, int w, int h, int x1, int y1)
{
	SWFFill fill;
	SWFJpegBitmap jpeg;
	SWFShape shape = newSWFShape();
    SWFInput input;
	SWFDisplayItem d;
	
	char * newBuf = malloc(len);
	memcpy(newBuf, buf, len);

    log_write(LL_DEBUG,"WriteJpegRawImage: %dx%d (%d, %d) to (%d, %d)\n", w, h, x0, y0, x1, y1);

    input = newSWFInput_allocedBuffer(newBuf, len);
    jpeg = newSWFJpegBitmap_fromInput(input);
    fill = SWFShape_addBitmapFill(shape, jpeg, SWFFILL_TILED_BITMAP);
    SWFShape_setRightFill(shape, fill);
	destroySWFFill(fill);	// IIC -- fix memory leak
    if (appData.macromediaHack) {
        SWFShape_movePenTo(shape, 1, 1);
    } else {
        SWFShape_movePenTo(shape, 0, 0);
    }
    SWFShape_drawLine(shape, w, 0);
    SWFShape_drawLine(shape, 0, h);
    SWFShape_drawLine(shape,-w, 0);
    SWFShape_drawLine(shape, 0,-h);
    SWFShape_end(shape);

    d = SWFMovie_add(movie, (SWFBlock)shape);
    SWFDisplayItem_moveTo(d, x1, y1);

	appData.compressed_image_bytes += len;
    appData.expanded_image_bytes += len;

    paintZBuffer(d, x1, y1, w, h, 1, 0,0);
//    checkRemoveItems();

    return True;
}

/* WriteRawRectangle:
 *   Put a rectangle.
 */
static Bool WriteRawRectangle(Pixel c, int x, int y, int w, int h)
{
    SWFShape shape;
    SWFFill fill;
    SWFDisplayItem d;
    int red, green, blue;
    
    log_write(LL_DEBUG,"WriteRawRectangle: %dx%d to (%d, %d)\n", w, h, x, y);
    
	int lh = matchLastItemWidth(x, y, w,2,c);
	if (lh>0) {
		y -= lh;
		if (y<0) y = 0;
		h += lh;
	}
	else
	{
		int lw = matchLastItemHeight(x, y, h, 2, c);
		if (lw>0) {
			x -= lw;
			if (x<0) x = 0;
			w += lw;
		}
	}

    shape = newSWFShape();
    red = (c>>myFormat.redShift) & myFormat.redMax;
    green = (c>>myFormat.greenShift) & myFormat.greenMax;
    blue = (c>>myFormat.blueShift) & myFormat.blueMax;
    fill = SWFShape_addSolidFill(shape, red, green, blue, 255);
    SWFShape_setRightFill(shape, fill);
	destroySWFFill(fill);	// IIC -- fix memory leak
    if (appData.macromediaHack) {
        SWFShape_movePenTo(shape, 1, 1);
    } else {
        SWFShape_movePenTo(shape, 0, 0);
    }
    SWFShape_drawLine(shape, w, 0);
    SWFShape_drawLine(shape, 0, h);
    SWFShape_drawLine(shape, -w, 0);
    SWFShape_drawLine(shape, 0, -h);
    SWFShape_end(shape);
    d = SWFMovie_add(movie, (SWFBlock)shape);
    SWFDisplayItem_moveTo(d, x, y);
    
    paintZBuffer(d, x, y, w, h, 2, c,0);
//    checkRemoveItems();
    return True;
}

// IIC  - use slow rate for waiting period
void SetFrameRate(int rate)
{
    if (movie) SWFMovie_setRate(movie, rate);
    intervalusec = 1000000L / rate;
}

void SetStartTime()
{
    gettimeofday(&tv0, &dummytz);
	tv = tv00 = tv0;
}

void SetTime(int offset)
{
	if (offset==0)
		return;

	tv = tv00;
	tv.tv_sec += offset / 1000;
	tv.tv_usec += (offset % 1000) * 1000L;
	if (1000000L <= tv.tv_usec) {
		tv.tv_usec -= 1000000L;
		tv.tv_sec++;
	}    

	if (!appData.recordingStarted)	// skip the initial blank screen only
	{
		tv0 = tv;
	}
}

int GetLastDuration()
{
	int dur = (tv.tv_sec - tv00.tv_sec);
    log_write(LL_DEBUG,"GetLastDuration: %d\n", dur);
	return dur;
}

/* checkNextFrame:
 *   Add frames necessary.
 */
void checkNextFrame(void)
{
    unsigned long ticks;
	BOOL checked = FALSE;
	char buf[2048];

	if (!appData.replay&&!appData.preview) {
#ifndef WIN32
    gettimeofday(&tv, &dummytz);
#else
	ticks = GetTickCount();
	tv.tv_sec = ticks / 1000000L;
	tv.tv_usec = ticks % 1000000L;
#endif
	}

    if (tv0.tv_sec < 0) {		/* initialize */
        tv0 = tv;
        tv0.tv_usec += intervalusec;
        if (1000000L <= tv0.tv_usec) {
            tv0.tv_usec -= 1000000L;
            tv0.tv_sec++;
        }
    }
    while(tv0.tv_sec < tv.tv_sec || (tv0.tv_sec == tv.tv_sec && tv0.tv_usec < tv.tv_usec)) {
        if (appData.recordingMethod == RECORD_BUFFERED) {
            flush_pending_image();
        }
		if ((!appData.replay&&!appData.preview) || appData.delay>=0)
		{
			if (appData.nframes == 1)	// if we stop at ist frame, don't load next movie
			{
				int index = file_index + 1;	// load next movie when playing the current one
				sprintf(buf, "vars=[];vars[0]=0;vars[1]=0;vars[2]=%d;"
						"if(_level%d._currentframe>30||vars[2]==0) {"
 						"getURL(\"FSCommand:current_frames\", \"%d\");"
						"getURL(\"FSCommand:current_file\", \"flash%d.swf\");"
						"getURL(\"FSCommand:current_level\", \"/_level%d\");"
						"loadMovieNum(\"flash%d.swf\", %d);"
						"vars[0] = setInterval(checkLoad, 100, vars);"
						"};"
						"function checkLoad(fvars){"
						"if(!_level%d.getBytesLoaded())"
						"  return;"
						"_level%d._visible=false;"
//						"if (fvars[1]==0) {"
						"sv = new Sound(_level%d);"
						"sv.setVolume(0);"
						"sv = null;"
//						"  fvars[1]=1;"
//						"}"
						"if(_level%d.getBytesLoaded()<_level%d.getBytesTotal())"
						"  return;"
						"clearInterval(fvars[0]);"
						"_level%d.stop();"
						"sv = new Sound(_level%d);"
						"sv.setVolume(100);"
						"sv = null;"
						"if (fvars[2]>0) loadMovieNum(\"flash_empty.swf\", %d);"
						"};",
						file_index,
						file_index,
						total_frames, file_index, file_index+1,
						index, index+1, index+1, index+1, index+1, index+1, index+1, index+1,index+1,
						file_index);
				SWFAction loadAction = compileSWFActionCode(buf);
				SWFMovie_add(movie, (SWFBlock)loadAction);
			}

			if (!checked)
			{
				checkRemoveItems();
				checked = TRUE;
			}

			if (movie) 
			{
				SWFMovie_nextFrame(movie);
			}
			appData.nframes++;
		}
        log_write(LL_DEBUG, "checkNextFrame: frame: %d\n", appData.nframes);
		appData.delay ++;
        tv0.tv_usec += intervalusec;
        if (1000000L <= tv0.tv_usec) {
            tv0.tv_usec -= 1000000L;
            tv0.tv_sec++;
        }    

		if (appData.nframes>=appData.framesPerMovie)
		{
			int i;
			total_frames += appData.nframes;++file_index;
			sprintf(buf, 
					"getURL(\"FSCommand:current_frames\", \"%d\");"
					"getURL(\"FSCommand:current_file\", \"flash%d.swf\");"
					"getURL(\"FSCommand:current_level\", \"/_level%d\");"
					"_level%d._visible=true;"
					"_level%d._visible=false;"
					"sv = new Sound(_level%d);"
					"sv.setVolume(100);"
					"sv = null;"
					"_level%d.gotoAndPlay(1);",
					total_frames, file_index, file_index+1, file_index+1, file_index, file_index+1, file_index+1);
			SWFAction loadAction = compileSWFActionCode(buf);
			SWFMovie_add(movie, (SWFBlock)loadAction);
			for (i=0; i<5; i++) {
				SWFMovie_nextFrame(movie);
			}

			// overlap playing for 10 second
			sprintf(buf, 
					"_level%d.stop();"					// must stop the previous movie
					"loadMovieNum(\"flash_empty.swf\", %d);",
					file_index, file_index);
			loadAction = compileSWFActionCode(buf);
			SWFMovie_add(movie, (SWFBlock)loadAction);
			SWFMovie_nextFrame(movie);
			WriteFinishMovie(False);
			WriteInitMovie(movie_width, movie_height);			

			flush_pending_image();
			break;
		}
    }
}

void InsertEmptyFrame(Bool cleanup)
{
   if (cleanup) {
     WriteRectangle(0, 0, 0,si.framebufferWidth, si.framebufferHeight );
   }
   
   if (appData.replay||appData.preview)
   {
       tv.tv_usec += intervalusec;
       if (1000000L <= tv.tv_usec) {
           tv.tv_usec -= 1000000L;
           tv.tv_sec++;
       }    
   }   
   
   checkNextFrame();
}

void
SwitchRecording(void)
{
	if (!appData.replay && !appData.preview) {
	  if (appData.recording) {
		unsigned long ticks;
#ifndef WIN32
		gettimeofday(&tv0, &dummytz);
#else
		ticks = GetTickCount();
		tv0.tv_sec = ticks / 1000000L;
		tv0.tv_usec = ticks % 1000000L;
#endif
		flush_pending_image();
	  }
	}
	trigger = False;
}

Bool init_recording(void)
{
//	FILE *file;
//	SWFDisplayItem item;

    if (soundf != NULL) {
        fclose(soundf);
    }
    if (movie != NULL) {
        destroySWFMovie(movie); movie = NULL;
    }
    if (final_soundfile!=NULL) {
       fclose(final_soundfile);
    }
    if (*appData.soundFile) {
		sprintf(sound_filename, "%s%d.mp3", appData.soundFile, file_index);
        soundf = fopen(sound_filename, "rb");
    	if (!soundf) {
    	    log_write(LL_ERROR, "WriteInitMovie: Cannot open sound file:%s",sound_filename);
//    	    return False;
    	}
    }
    
    movie = newSWFMovie();

    SWFMovie_setRate(movie, appData.frameRate);
    if (soundf) {
        log_write(LL_DEBUG,"=== WriteInitMovie: Sound file: \"%s\"\n", appData.soundFile);
#if MING03
        /* ming 0.3beta1 */
	   SWFMovie_setSoundStream(movie, (SWFSoundStream)newSWFSoundStream(soundf));
#else
        /* ming 0.2a */
        SWFMovie_setSoundStream(movie, newSWFSound(soundf));
#endif
    }
#ifdef RECORD_CHAT
    SWFMovie_setDimension(movie, movie_width+260, movie_height);    // IIC
#else
    SWFMovie_setDimension(movie, movie_width, movie_height);    // IIC
#endif
    intervalusec = 1000000L / appData.frameRate;
//	tv0.tv_sec = tv0.tv_usec = -1;
//	tv = tv00 = tv0;
    appData.nframes = 0;
    trigger = False;
    
    // IIC
#ifdef RECORD_CHAT
    message = newSWFTextField();
    SWFTextField_setFlags(message, 
//        SWFTEXTFIELD_NOEDIT | 
        SWFTEXTFIELD_MULTILINE |
        SWFTEXTFIELD_WORDWRAP |
        SWFTEXTFIELD_DRAWBOX);  // SWFTEXTFIELD_HTML);
    SWFTextField_setBounds(message, 240, movie_height-4);
    file = fopen("fdb/Bitstream Vera Sans.fdb","rb");
    if (file) {
        SWFFont font = loadSWFFontFromFile(file);
        SWFTextField_setFont(message, font);
        fclose(file);
    }
    SWFTextField_setHeight(message, 20);
    SWFTextField_setColor(message, 0, 0, 255, 255);    
    item = SWFMovie_add(movie, (SWFBlock)message);
    SWFDisplayItem_moveTo(item,movie_width + 10, 1);
#endif
    
    appData.recording = True;
    appData.compressed_image_bytes = 0;
    appData.expanded_image_bytes = 0;
    
    SetMovieSize(movie_width, movie_height);

    track_image(0, 0, movie_width, movie_height);	// IIC - not to write the initial black frame

    return True;
}

void SetMovieSizeEx(int w, int h)
{
	// a meeting can have many share sessions, and presenters, the screen size of presenter maybe different
	// use the maximum screen size
	int cgw = max(w, appData.cgw);
	int cgh = max(h, appData.cgh);
	if (w != appData.last_w || h != appData.last_h)	// compare the real size
	{
		log_write(LL_MSG, "Cleaning up screen:%dx%d.", appData.last_w, appData.last_h);
 		// clean up the current screen
		si.framebufferWidth = appData.last_w;
		si.framebufferHeight = appData.last_h;
		DisplayRectangle(0xFFFFFF, 0, 0, appData.last_w, appData.last_h);

		appData.last_w = w;	// remember last real dimension
		appData.last_h = h;

		appData.cgw = cgw;
		appData.cgh = cgh;
		appData.cgx1 = appData.cgx0 + appData.cgw;
		appData.cgy1 = appData.cgy0 + appData.cgh;
		SetMovieSize(appData.cgw, appData.cgh);
	}

	si.framebufferWidth = cgw;
	si.framebufferHeight = cgh;
}

int save(const char *attribute, int value)
{
	char filename[1024];
	sprintf(filename, "%s_%s.txt", appData.dumpFile, attribute);

    FILE * fp = fopen(filename, "w");
    if (fp == NULL)
    {
        log_write(LL_ERROR, "Unable to open attribute file %s: %s",
                  filename, strerror(errno));
        return -1;
    }
    fprintf(fp, "%d\n", value);
    fclose(fp);

    return 0;
}

void set_movie_size(int w, int h)
{
	save("width", w);
	save("height", h);
}

void SetMovieSize(int w, int h)
{
    if (movie)
#ifdef RECORD_CHAT
      SWFMovie_setDimension(movie, w+260, h);    // IIC
#else
      SWFMovie_setDimension(movie, w, h);    // IIC
#endif
    
    tmpbuf_len = (unsigned long)floor((unsigned long)w*(unsigned long)h*CHANNELS*1.01+12);
	if (movie_width!=w || movie_height!=h)
	{
		if (tmpbuf_src) 
		{
			free(tmpbuf_src); tmpbuf_src = NULL;
		}
		if (tmpbuf_compressed) 
		{
			free(tmpbuf_compressed); tmpbuf_compressed = NULL;
		}
		if (jpeg_buf) 
		{
			free(jpeg_buf); jpeg_buf = NULL;
		}
		if (imageData!=0)
		{
		  free(imageData);
		  imageData = 0;
		  log_write(LL_DEBUG, "Freed previous movie buffer.");
		}
	}

	if (tmpbuf_src==NULL)
		tmpbuf_src = malloc(tmpbuf_len);

	if (tmpbuf_compressed==NULL)
		tmpbuf_compressed = malloc(tmpbuf_len);

	if (jpeg_buf==NULL)
		jpeg_buf = malloc(RequiredBuffSize(w, h));

    movie_width = w;
    movie_height = h;
	
    if (!tmpbuf_src || !tmpbuf_compressed || !jpeg_buf) {
        log_write(LL_ERROR, "SetMovieSize: Out of memory (w=%d,h=%d)", w, h);
        return;
    }

    initializeZBuffer();
    clearZBuffer();

    set_movie_size(w, h);
    log_write(LL_DEBUG, "set movie size to %dx%d\n", w, h);


    if (appData.spool) {
      WriteMovieSize(w, h);
    }
}

Bool WriteInitMovie(int w, int h)
{
    Ming_init();

	sprintf(movie_filename, "%s%d.swf", appData.dumpFile, file_index);
    movie_width = w;
    movie_height = h;

    {
        /* warn if the file is going to be overwritten */
        struct stat st;
        if (stat(movie_filename, &st) != -1) {
            log_write(LL_DEBUG, "=== WriteInitMovie: Warning: file %s already exists, overwriting.\n", 
        	    movie_filename);
        }
    }
    log_write(LL_INFO,"=== WriteInitMovie: Pid=%d, Opening file: \"%s\" for a movie size (%d, %d), frame rate %d...\n", 
	    getpid(), movie_filename, movie_width, movie_height, appData.frameRate);
    
    movie_dumpf = fopen(movie_filename, "wb");
    if (!movie_dumpf) {
        log_write(LL_ERROR, "WriteInitMovie: Cannot open file: %s, errno=%d", movie_filename, errno);
        return False;
    }
        
    movie = NULL;
    soundf = NULL;
    final_soundfile = NULL;
    return init_recording();
}

// if voice is longer than the data
Bool FinishAllTheVoiceClips()
{
	int index = file_index;	// to monitor if there are more sound clips
	while(!SWFMovie_endOfSoundStream(movie))
	{
		InsertEmptyFrame(False);
	}

	while (index != file_index)
	{
		if (*appData.soundFile) {
			sprintf(sound_filename, "%s%d.mp3", appData.soundFile, file_index);
	        struct stat st;
		    if (stat(sound_filename, &st) == -1) {
    			log_write(LL_ERROR, "FinishAllTheVoiceClips: no more sound file:%s",sound_filename);
    			break;
    		}
		}
	
		index = file_index;

		while(!SWFMovie_endOfSoundStream(movie))
		{
			InsertEmptyFrame(False);
		}

		// if file_index increased, keep looping to see if there are more sound
	}
	
	char buf[2048];
	sprintf(buf, "_level%d.stop();vars=[];vars[0]=0;vars[1]=1;"
		"getURL(\"FSCommand:current_frames\", \"0\");"
		"getURL(\"FSCommand:current_file\", \"flash0.swf\");"
		"getURL(\"FSCommand:current_level\", \"/_level1\");"
		"getURL(\"FSCommand:stop_play\", \"1\");"
		"loadMovieNum(\"flash0.swf\", 1);"
		"vars[0] = setInterval(checkLoad, 100, vars);"
		"function checkLoad(fvars){"
		"if(!_level1.getBytesLoaded())"
		"  return;"
		"clearInterval(fvars[0]);"
		"_level1._visible=false;"		// first frame maybe not a full screen, hide it
		"_level1.gotoAndStop(1);"
		"};", file_index+1
	);
	SWFAction loadAction = compileSWFActionCode(buf);
	SWFMovie_add(movie, (SWFBlock)loadAction);

	if (file_index > 0)
	{
		sprintf(buf, 
			"loadMovieNum(\"flash_empty.swf\", %d);",				
			file_index+1);
		loadAction = compileSWFActionCode(buf);
		SWFMovie_add(movie, (SWFBlock)loadAction);

	}
	SWFMovie_nextFrame(movie);
}

/* WriteFinishMovie:
 *   Write the movie file.
 */
Bool WriteFinishMovie(Bool final)
{
    long len;
    int index;
	char script[2048];

    if (appData.spool) {
      fclose(appData.spool_handle);
      return True;
    }

    checkRemoveItems();
    
	if (final)
	{
		FinishAllTheVoiceClips();
	}

    log_write(LL_INFO, "=== WriteFinishMovie: %d frames (%.1f sec), compressed=%lu, expanded_image: %lu\n",
	    appData.nframes, (double)appData.nframes/(double)appData.frameRate, appData.compressed_image_bytes, appData.expanded_image_bytes);

#if MING02
    /* ming 0.3beta1 */
    len = SWFMovie_output(movie, fileOutputMethod, movie_dumpf, 0);
#else
    /* ming 0.2a */
    len = SWFMovie_output(movie, fileOutputMethod, movie_dumpf);
//    len = SWFMovie_write_final(movie, fileOutputMethod, movie_dumpf);

#endif
    destroySWFMovie(movie); movie = NULL;

    log_write(LL_INFO, "=== WriteFinishMovie: %ld output object created.\n", SWFMovie_getOutputObjectsCreated());
    log_write(LL_INFO, "=== WriteFinishMovie: %ld output object deleted.\n", SWFMovie_getOutputObjectsDeleted());

#ifdef RECORD_CHAT
    destroySWFTextField(message);
#endif    
	fclose(movie_dumpf);

    if (soundf != NULL) {
        fclose(soundf);
    }    
    if (final_soundfile!=NULL) {
       fclose(final_soundfile);
    }
    if (0 < len) {
        log_write(LL_INFO, "=== WriteFinishMovie: %ld bytes dumped into \"%s\".\n", len, movie_filename);
    } else {
        log_write(LL_ERROR, "=== WriteFinishMovie: cannot write the file!");
        return False;
    }
    if (appData.nframes==0) {
      unlink(movie_filename);
	  file_index --;
      log_write(LL_INFO, "=== WriteFinishMovie: 0 frames, deleted empty movie file.");
    }

	if (final && file_index >= 0)
	{
		sprintf(movie_filename, "%s.swf", appData.dumpFile);
	    movie_dumpf = fopen(movie_filename, "wb");
		if (!movie_dumpf) {
			log_write(LL_ERROR, "WriteInitMovie: Cannot open file: %s, errno=%d", movie_filename, errno);
			return False;
		}
		
		movie = newSWFMovie();
		SWFMovie_setRate(movie, appData.frameRate);
		SetMovieSize(movie_width, movie_height);
		total_frames += appData.nframes+1;
		{
			sprintf(script, "vars=[];vars[0]=0;vars[1]=1;stop();"
				"getURL(\"FSCommand:total_frames\", \"%d\");"
				"getURL(\"FSCommand:current_frames\", \"0\");"
				"getURL(\"FSCommand:frames_per_movie\", \"%d\");"				
				"getURL(\"FSCommand:current_file\", \"flash0.swf\");"
				"getURL(\"FSCommand:current_level\", \"/_level1\");"
				"loadMovieNum(\"flash0.swf\", 1);"
				"vars[0] = setInterval(checkLoad, 100, vars);"
				"function checkLoad(fvars){"
			    "if(!_level1.getBytesLoaded())"
				"  return;"
				"clearInterval(fvars[0]);"
				"_level1._visible=true;"
				"_level1.gotoAndPlay(1);"
				"getURL(\"FSCommand:total_frames\", \"%d\");"
				"};",
				total_frames,appData.framesPerMovie,total_frames);
			SWFAction loadAction = compileSWFActionCode(script);
			SWFMovie_add(movie, (SWFBlock)loadAction);
			SWFMovie_nextFrame(movie);
		}

#if MING02
		/* ming 0.3beta1 */
		len = SWFMovie_output(movie, fileOutputMethod, movie_dumpf, 0);
#else
		/* ming 0.2a */
		len = SWFMovie_output(movie, fileOutputMethod, movie_dumpf);
#endif
	    destroySWFMovie(movie); movie = NULL;
		fclose(movie_dumpf);

		// work around unloadMovieNum
		sprintf(movie_filename, "%s_empty.swf", appData.dumpFile);
	    movie_dumpf = fopen(movie_filename, "wb");
		if (!movie_dumpf) {
			log_write(LL_ERROR, "WriteInitMovie: Cannot open file: %s, errno=%d", movie_filename, errno);
			return False;
		}
		
		movie = newSWFMovie();
		SWFMovie_setRate(movie, appData.frameRate);
		SetMovieSize(movie_width, movie_height);
		sprintf(script, "stop();"
					"_visible=false;"
					"sv = new Sound();"
					"sv.setVolume(100);"
					"sv = null;"
					);
		SWFAction loadAction = compileSWFActionCode(script);
		SWFMovie_add(movie, (SWFBlock)loadAction);
		SWFMovie_nextFrame(movie);

#if MING02
		/* ming 0.3beta1 */
		len = SWFMovie_output(movie, fileOutputMethod, movie_dumpf, 0);
#else
		/* ming 0.2a */
		len = SWFMovie_output(movie, fileOutputMethod, movie_dumpf);
#endif
	    destroySWFMovie(movie); movie = NULL;
		fclose(movie_dumpf);
	
	}

    clearZBuffer();
    return True;
}


/* return true if the whole rectangle is clipped. */
int cliprect(int* x0, int* y0, int* w, int* h, int* x1, int* y1)
{
    if (*x0 < appData.cgx0) {
	  int dx = appData.cgx0 - *x0;
	  *w -= dx;
	  *x0 += dx;
	  *x1 += dx;
    } else if (appData.cgx1 <= *x0) {
	  return 1;
    }

    if (*y0 < appData.cgy0) {
	  int dy = appData.cgy0 - *y0;
	  *h -= dy;
	  *y0 += dy;
	  *y1 += dy;
    } else if (appData.cgy1 <= *y0) {
	  return 1;
    }

    if (appData.cgx1 < *x1+*w) {
	  *w = appData.cgx1 - *x1;
    }
    if (appData.cgy1 < *y1+*h) {
	  *h = appData.cgy1 - *y1;
    }
    *x1 -= appData.cgx0;
    *y1 -= appData.cgy0;
#if DEBUG_CLIP
    log_write(LL_DEBUG, "%dx%d (%d,%d) -> (%d,%d)\n", *w, *h, *x0, *y0, *x1, *y1);
#endif
    return (*w <= 0) || (*h <= 0);
}

Bool WriteJpegImage(char* buf, int len, int x0, int y0, int w, int h)
{
    int x1 = x0, y1 = y0;
    log_write(LL_DEBUG,"WriteJpegImage: %dx%d from (%d, %d)\n", w, h, x0, y0);

    /* check if a signal is sent. */
    if (trigger) {
        SwitchRecording();
    }
    if (!appData.recording) {
        track_image(x0, y0, w, h);
        return True;
    }
    
    checkNextFrame();
    if (appData.recordingMethod == RECORD_BUFFERED) {
        track_image(x0, y0, w, h);
        return True;
    }
    if (cliprect(&x0, &y0, &w, &h, &x1, &y1)) return True;

    return WriteJpegRawImage(buf, len, x0, y0, w, h, x0, y0);
}

/* WriteImage:
 */
Bool WriteImage(char* dblbuf, unsigned long dbllen, int x0, int y0, int w, int h)
{
    int x1 = x0, y1 = y0;
    log_write(LL_DEBUG,"WriteImage: %dx%d from (%d, %d)\n", w, h, x0, y0);

    /* check if a signal is sent. */
    if (trigger) {
        SwitchRecording();
    }
    if (!appData.recording) {
        track_image(x0, y0, w, h);
        return True;
    }
    
    checkNextFrame();
    if (appData.recordingMethod == RECORD_BUFFERED) {
        track_image(x0, y0, w, h);
        return True;
    }
    if (cliprect(&x0, &y0, &w, &h, &x1, &y1)) return True;

    return WriteRawImage(dblbuf, dbllen, x0, y0, w, h, x1, y1);
}

/* WriteRectangle:
 */
Bool WriteRectangle(Pixel c, int x0, int y0, int w, int h)
{
    int x1 = x0, y1 = y0;
    log_write(LL_DEBUG,"WriteRectangle: %dx%d from (%d, %d)\n", w, h, x0, y0);

    /* check if a signal is sent. */
    if (trigger) {
        SwitchRecording();
    }
    if (!appData.recording) {
        track_image(x0, y0, w, h);
        return True;
    }
    
    checkNextFrame();
    if (appData.recordingMethod == RECORD_BUFFERED) {
        track_image(x0, y0, w, h);
        return True;
    }
    if (cliprect(&x0, &y0, &w, &h, &x1, &y1)) return True;

    return WriteRawRectangle(c, x1, y1, w, h);
}

/*
 * track pixels which have not been recorded. 
 */

#define INF 9999
static void reset_pending_image(void) {
    img_xmin = img_ymin = INF;
    img_xmax = img_ymax =-INF;
}

static void track_image(int x, int y, int w, int h) {
    if (x < img_xmin) img_xmin = x;
    if (y < img_ymin) img_ymin = y;
#ifdef RECORD_CHAT
    SWFMovie_setDimension(movie, movie_width+260, movie_height);    // IIC
    if (img_xmax < x+w) img_xmax = x+w+260; // IIC
#else
    SWFMovie_setDimension(movie, movie_width, movie_height);    // IIC
    if (img_xmax < x+w) img_xmax = x+w; // IIC
#endif
    if (img_ymax < y+h) img_ymax = y+h;
    log_write(LL_DEBUG,"track_image: now (%d, %d)-(%d, %d)\n", img_xmin, img_ymin, img_xmax, img_ymax);
}

static bool jpegError;
static int jpegDstDataLen;

static struct jpeg_destination_mgr jpegDstManager;
static JOCTET *jpegDstBuffer;
static size_t jpegDstBufferLen;

static void JpegInitDestination(j_compress_ptr cinfo);
static boolean JpegEmptyOutputBuffer(j_compress_ptr cinfo);
static void JpegTermDestination(j_compress_ptr cinfo);

static void
JpegInitDestination(j_compress_ptr cinfo)
{
	jpegError = False;
	jpegDstManager.next_output_byte = jpegDstBuffer;
	jpegDstManager.free_in_buffer = jpegDstBufferLen;
}

static boolean
JpegEmptyOutputBuffer(j_compress_ptr cinfo)
{
	jpegError = True;
	jpegDstManager.next_output_byte = jpegDstBuffer;
	jpegDstManager.free_in_buffer = jpegDstBufferLen;

	return TRUE;
}

static void
JpegTermDestination(j_compress_ptr cinfo)
{
	jpegDstDataLen = jpegDstBufferLen - jpegDstManager.free_in_buffer;
}

static void
JpegSetDstManager(j_compress_ptr cinfo, JOCTET *buf, size_t buflen)
{
	jpegDstBuffer = buf;
	jpegDstBufferLen = buflen;
	jpegDstManager.init_destination = JpegInitDestination;
	jpegDstManager.empty_output_buffer = JpegEmptyOutputBuffer;
	jpegDstManager.term_destination = JpegTermDestination;
	cinfo->dest = &jpegDstManager;
}

void PrepareRowForJpeg32(BYTE *dst, CARD32 *src, int count)
{																			
	CARD32 pix;															
	while (count--) {														
		pix = *src++;														
		*(dst++) = (char)(pix>>myFormat.redShift) & myFormat.redMax; /* R */
		*(dst++) = (char)(pix>>myFormat.greenShift) & myFormat.greenMax; /* G */
		*(dst++) = (char)(pix>>myFormat.blueShift) & myFormat.blueMax; /* B */
	}																		
}


int compress_jpeg(BYTE *dst, int x, int y, int w, int h, int quality)
{
	struct jpeg_compress_struct cinfo;
	struct jpeg_error_mgr jerr;
	int dy;
	BYTE *srcBuf = (BYTE*) malloc(w * 3);
	JSAMPROW rowPointer[1];
    int scrWidthInBytes;
    
    scrWidthInBytes = si.framebufferWidth * myFormat.bitsPerPixel / 8;

	rowPointer[0] = (JSAMPROW)srcBuf;

	cinfo.err = jpeg_std_error(&jerr);
	jpeg_create_compress(&cinfo);

	cinfo.image_width = w;
	cinfo.image_height = h;
	cinfo.input_components = 3;
	cinfo.in_color_space = JCS_RGB;

	jpeg_set_defaults(&cinfo);
	jpeg_set_quality(&cinfo, quality, TRUE);

	JpegSetDstManager (&cinfo, dst, RequiredBuffSize(w, h));

	jpeg_start_compress(&cinfo, TRUE);

	for (dy = y; dy < (y+h); dy++) {
		CARD32 *src = (CARD32)(imageData + dy * scrWidthInBytes + x * myFormat.bitsPerPixel / 8);
		PrepareRowForJpeg32(srcBuf, src, w);
//		PrepareRowForJpeg(srcBuf, dy, w);
		jpeg_write_scanlines(&cinfo, rowPointer, 1);
		if (jpegError)
			break;
	}

	if (!jpegError)
		jpeg_finish_compress(&cinfo);

	jpeg_destroy_compress(&cinfo);
	free(srcBuf);

	if (!jpegError)
		WriteJpegRawImage(jpeg_buf, jpegDstDataLen, x, y, w, h, x, y);

	return 0;
}

static UINT RequiredBuffSize(UINT width, UINT height)
{
  // this is a guess - 12 bytes plus 1.5 times raw... (zlib.h says compress
  // needs 12 bytes plus 1.001 times raw data but that's not quite what we give
  // zlib anyway)
  return (width * height * (4) * 3 / 2);
}

static void flush_pending_image(void) {
    if (img_xmin < img_xmax && img_ymin < img_ymax) {	// IIC - not to write the initial black image
        int x0 = img_xmin, y0 = img_ymin;
        int x1 = x0, y1 = y0;
        int w = img_xmax-img_xmin, h = img_ymax-img_ymin;
        if (cliprect(&x0, &y0, &w, &h, &x1, &y1)) return;
//		compress_jpeg(jpeg_buf, x0, y0, w, h, 9);
        WriteRawImage(NULL, 0, x0, y0, w, h, x1, y1);
        log_write(LL_DEBUG,"flush_pending_image: (%d, %d)-(%d, %d)\n", img_xmin, img_ymin, img_xmax, img_ymax);
        reset_pending_image();
    }
}

/* signal handler
 */
void TurnRecordingOn(int sig)
{
    log_write(LL_DEBUG,"TurnRecordingOn called\n");
    appData.recording = True;
	appData.recordingStarted = True;
    if (!appData.replay && !appData.preview) trigger = True;
}
void TurnRecordingOff(int sig)
{
    log_write(LL_DEBUG,"TurnRecordingOff called\n");
    appData.recording = False;
    if (!appData.replay && !appData.preview) trigger = True;
}

void RecordMessage(const char * _message)
{
#ifdef RECORD_CHAT
    log_write(LL_DEBUG,"Recording message - %s\n", _message);
    SWFTextField_addString(message, (char*)_message);
#endif
}

void SetSoundStream(const char * soundfile)
{
    if (*appData.soundFile) {
        final_soundfile = fopen(soundfile, "rb");
        if (!final_soundfile) {
            log_write(LL_DEBUG,"SetSoundStream: Cannot open sound file %s\n", soundfile);
        } else {
            log_write(LL_DEBUG,"=== SetSoundStream: Sound file: \"%s\"\n", soundfile);
#if MING03
            /* ming 0.3beta1 */
            SWFMovie_setSoundStream(movie, (SWFSoundStream)newSWFSoundStream(final_soundfile));
#else
            /* ming 0.2a */
            SWFMovie_setSoundStream(movie, newSWFSound(final_soundfile));
#endif
        }
    }
}

// spool data
void OpenSpoolFile()
{
	if (appData.spool) {
		// cannot use 'ab+' here, the fseek won't seek back
		appData.spool_handle = fopen(appData.dumpFile, "rb+");	
                if (appData.spool_handle==NULL) {
			appData.spool_handle = fopen(appData.dumpFile, "wb+");
		}
		fseek(appData.spool_handle, 0, SEEK_END);
	}
}

void CloseSpoolFile()
{
	if (appData.spool_handle) {
		fclose(appData.spool_handle);
	}
}

void WriteMovieSize(int w, int h)
{
  int ww=0, hh=0;
  if (appData.spool_handle == NULL)
    return;

  fseek(appData.spool_handle, 0, SEEK_SET);
  if (fread(&ww, 4, 1, appData.spool_handle)==1 &&
      fread(&hh, 4, 1, appData.spool_handle)==1)
  {
    if (ww<w || hh<h)
    {
      fseek(appData.spool_handle, 0, SEEK_SET);
      fwrite(&w, 4, 1, appData.spool_handle);
      fwrite(&h, 4, 1, appData.spool_handle);
      log_write(LL_DEBUG, "SetMovieSize (%dx%d)", w, h);
    }
  }
  else
  {
      if (fseek(appData.spool_handle, 0, SEEK_SET)==0)
      {
        fwrite(&w, 4, 1, appData.spool_handle);
        fwrite(&h, 4, 1, appData.spool_handle);
        log_write(LL_DEBUG, "SetMovieSize (%dx%d)", w, h);
      }
  }

  fseek(appData.spool_handle, 0, SEEK_END);
}

void WriteMovieStartTime()
{
  struct timeval tv;
  int type = -1;
  int empty = 0;
  if (appData.spool_handle == NULL)
    return;

  if (ftell(appData.spool_handle) == 0) {
      fwrite(&empty, 4, 1, appData.spool_handle);
      fwrite(&empty, 4, 1, appData.spool_handle);
  }

  gettimeofday(&tv, &dummytz);
  log_write(LL_DEBUG, "WriteMovieStartTime (%d:%d)", tv.tv_sec, tv.tv_usec);
  if (fwrite(&type, 4, 1, appData.spool_handle)!=1 ||
      fwrite(&tv, sizeof(struct timeval), 1, appData.spool_handle)!=1)
  {
    log_write(LL_ERROR, "WriteMovieStartTime:Could not write movie start time");
    fclose(appData.spool_handle);
    exit(1);
  }
}

void SpoolMovieData(void *buf, size_t len, int type, int x, int y, int w, int h)
{
  CARD8 data_size_buf[4];
  CARD8 timestamp_buf[8];
  CARD32 timestamp;
  int padding;
  struct timeval tv;

  if (appData.spool_handle == NULL)
    return;

  if (len==0)
	  return;

  /* Calculate current timestamp */
  gettimeofday(&tv, &dummytz);
  if (fwrite(&type, 4, 1, appData.spool_handle) != 1 ||
	fwrite(&tv, sizeof(struct timeval), 1, appData.spool_handle) != 1 ||
	fwrite(&x, 4, 1, appData.spool_handle) != 1 ||
	fwrite(&y, 4, 1, appData.spool_handle) != 1 ||
	fwrite(&w, 4, 1, appData.spool_handle) != 1 ||
	fwrite(&h, 4, 1, appData.spool_handle) != 1 ||
	fwrite(&len, 4, 1, appData.spool_handle) != 1 ||
	fwrite(buf, 1, len, appData.spool_handle) != len) 
  {
    log_write(LL_ERROR, "Could not write FBS file data");
    fclose(appData.spool_handle);
	exit(1);
  }
}

void LoadMovieFromFile(FILE* f)
{
	int type, x, y, w, h, len;
	char * buf;
	struct timeval tv;

	log_write(LL_DEBUG, "Loading movie dump file to produce flash movie.");

	intervalusec = 1000000L / appData.frameRate;
	tv0.tv_sec = tv0.tv_usec = -1;
	appData.nframes = 0;
	trigger = False;

	// get movie size
	fread(&movie_width, 4, 1, f);
	fread(&movie_height, 4, 1, f);	
	log_write(LL_DEBUG, " w=%d h=%d", movie_width,movie_height);
	SetMovieSize(movie_width, movie_height);
	buf = (char*)malloc(movie_width * movie_height * 4);

	if (buf)
	{
		// start time
		fread(&type, 4, 1, f);
		fread(&tv0, sizeof(struct timeval), 1, f);
		log_write(LL_DEBUG, "type:%d timeval0:%d:%d", type, tv0.tv_sec, tv0.tv_usec);

		while(!feof(f))
		{
			fread(&type, 4, 1, f);
			fread(&tv, sizeof(struct timeval), 1, f);
			log_write(LL_DEBUG, "type:%d timeval1:%d:%d", type, tv.tv_sec, tv.tv_usec);
                        if (type == -1) {
				tv0 = tv;
				continue;
			}
			fread(&x, 4, 1, f);
			fread(&y, 4, 1, f);
			fread(&w, 4, 1, f);
			fread(&h, 4, 1, f);
			fread(&len, 4, 1, f);
			
			log_write(LL_DEBUG, "x=%d y=%d w=%d h=%d len=%d", x,y,w,h,len);
			while(tv0.tv_sec < tv.tv_sec || (tv0.tv_sec == tv.tv_sec && tv0.tv_usec < tv.tv_usec)) {
				SWFMovie_nextFrame(movie);
				appData.nframes++;
//				log_write(LL_DEBUG, "checkNextFrame: frame: %d\n", appData.nframes);
				tv0.tv_usec += intervalusec;
				if (1000000L <= tv0.tv_usec) {
					tv0.tv_usec -= 1000000L;
					tv0.tv_sec++;
				}    
			}

			fread(buf, len, 1, f);
			switch(type)
			{
				case 0:	// bmp
				{
					WriteRawImage(buf, len, x, y, w, h, x, y);
					break;
				}
				case 1:	// jpeg
				{
					WriteJpegRawImage(buf, len, x, y, w, h, x, y);
					break;
				}
				case 2:
				{
					WriteRawRectangle((Pixel)*(int *)buf, x, y, w, h);
					break;
				}
				default:
					log_write(LL_ERROR, "Unknown image type (%d)", type);
					break;
			}
		}
		free(buf);
	}
	else
	{
		log_write(LL_ERROR, "LoadMovieFromFile: Out of memory.(%dx%d)", movie_width,movie_height);
	}
}
