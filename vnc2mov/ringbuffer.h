
#ifndef RINGBUFFER_H
#define RINGBUFFER_H

typedef struct _RingBuffer RingBuffer;

RingBuffer *createBuffer();
void clearRingBuffer(RingBuffer *buffer);
int samplesInBuffer(RingBuffer *buffer);
void writeIntoBuffer(RingBuffer *myRingBuffer, short *myData, int numsamples);
void readFromBuffer(RingBuffer *myRingBuffer, short *myData, int numsamples);

#endif
