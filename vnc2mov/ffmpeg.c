/*
 * Libavformat API example: Output a media file in any supported
 * libavformat format. The default codecs are used.
 *
 * Copyright (c) 2003 Fabrice Bellard
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * ~~~~
 *
 * Adapted to provide simple API for converting frame buffers into a video stream.
 * Copyright (C) 2011 Apreta.com
 *
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "ffmpeg.h"
#include "logging.h"
#include "ringbuffer.h"

#define SOUND_SAMPLE_RATE 11025
#define SOUND_SAMPLE_SIZE 2

#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

static int sws_flags = SWS_BICUBIC;
static int stream_frame_rate = 1;
static int video_bit_rate = 800000;
static int audio_bit_rate = 48000;
static int gop_size = 10;

static int video_codec = CODEC_ID_FLASHSV;
static enum PixelFormat source_pix_fmt = PIX_FMT_RGB24;
static enum PixelFormat stream_pix_fmt = PIX_FMT_RGB24;

/**************************************************************/
/* audio output */

//int16_t *samples;
uint8_t *audio_outbuf;
int audio_outbuf_size;
int audio_input_frame_size = 0;

/*
 * add an audio output stream
 */
static AVStream *add_audio_stream(AVFormatContext *oc, enum CodecID codec_id)
{
    AVCodecContext *c;
    AVStream *st;

    st = av_new_stream(oc, 1);
    if (!st) {
        log_write(LL_ERROR, "Could not alloc audio stream");
        return NULL;
    }

    c = st->codec;
    c->codec_id = codec_id;
    c->codec_type = AVMEDIA_TYPE_AUDIO;

    /* put sample parameters */
    c->sample_fmt = AV_SAMPLE_FMT_S16;
    c->bit_rate = audio_bit_rate;
    c->sample_rate = SOUND_SAMPLE_RATE;
    c->channels = 1;
    c->time_base.den = stream_frame_rate;
    c->time_base.num = 1;

    // some formats want stream headers to be separate
    if(oc->oformat->flags & AVFMT_GLOBALHEADER)
        c->flags |= CODEC_FLAG_GLOBAL_HEADER;

    return st;
}

static int open_audio(AVFormatContext *oc, AVStream *st)
{
    AVCodecContext *c;
    AVCodec *codec;

    c = st->codec;

    /* find the audio encoder */
    codec = avcodec_find_encoder(c->codec_id);
    if (!codec) {
        log_write(LL_ERROR, "audio codec not found");
        return -1;
    }

    /* open it */
    if (avcodec_open(c, codec) < 0) {
        log_write(LL_ERROR, "could not open audio codec");
        return -1;
    }

    audio_outbuf_size = 32000;
    audio_outbuf = av_malloc(audio_outbuf_size);
    audio_input_frame_size = c->frame_size;

    //samples = av_malloc(audio_input_frame_size * 2 * c->channels);
    log_write(LL_DEBUG, "audio frame size: %d", audio_input_frame_size);

    return 0;
}

static int write_audio_frame(AVFormatContext *oc, AVStream *st, short *samples)
{
    AVCodecContext *c;
    AVPacket pkt;
    av_init_packet(&pkt);

    c = st->codec;

    pkt.size = avcodec_encode_audio(c, audio_outbuf, audio_outbuf_size, samples);

    if (c->coded_frame && c->coded_frame->pts != AV_NOPTS_VALUE)
        pkt.pts= av_rescale_q(c->coded_frame->pts, c->time_base, st->time_base);
    pkt.flags |= AV_PKT_FLAG_KEY;
    pkt.stream_index= st->index;
    pkt.data= audio_outbuf;

    /* write the compressed frame in the media file */
    if (av_interleaved_write_frame(oc, &pkt) != 0) {
        log_write(LL_ERROR, "Error while writing audio frame");
        return -1;
    }

    return 0;
}

static void close_audio(AVFormatContext *oc, AVStream *st)
{
    avcodec_close(st->codec);

    //av_free(samples);
    av_free(audio_outbuf);
}

/**************************************************************/
/* video output */

AVFrame *picture, *tmp_picture;
uint8_t *video_outbuf;
int frame_count = 0, video_outbuf_size;

/* add a video output stream */
static AVStream *add_video_stream(AVFormatContext *oc, enum CodecID codec_id, int width, int height)
{
    AVCodecContext *c;
    AVStream *st;

    st = av_new_stream(oc, 0);
    if (!st) {
        log_write(LL_ERROR, "Could not alloc video stream");
        return NULL;
    }

    c = st->codec;
    c->codec_id = codec_id;
    c->codec_type = AVMEDIA_TYPE_VIDEO;

    /* put sample parameters */
    c->bit_rate = video_bit_rate;
    /* resolution must be a multiple of two */
    c->width = width;
    c->height = height;
    /* time base: this is the fundamental unit of time (in seconds) in terms
       of which frame timestamps are represented. for fixed-fps content,
       timebase should be 1/framerate and timestamp increments should be
       identically 1. */
    c->time_base.den = stream_frame_rate;
    c->time_base.num = 1;
    c->gop_size = gop_size; /* emit one intra frame every twelve frames at most */
    c->pix_fmt = stream_pix_fmt;
    if (c->codec_id == CODEC_ID_MPEG2VIDEO) {
        /* just for testing, we also add B frames */
        c->max_b_frames = 2;
    }
    if (c->codec_id == CODEC_ID_MPEG1VIDEO){
        /* Needed to avoid using macroblocks in which some coeffs overflow.
           This does not happen with normal video, it just happens here as
           the motion of the chroma plane does not match the luma plane. */
        c->mb_decision=2;
    }
    // some formats want stream headers to be separate
    if(oc->oformat->flags & AVFMT_GLOBALHEADER)
        c->flags |= CODEC_FLAG_GLOBAL_HEADER;

    return st;
}

static AVFrame *alloc_picture(enum PixelFormat pix_fmt, int width, int height)
{
    AVFrame *picture;
    uint8_t *picture_buf;
    int size;

    picture = avcodec_alloc_frame();
    if (!picture)
        return NULL;
    size = avpicture_get_size(pix_fmt, width, height);
    picture_buf = av_malloc(size);
    if (!picture_buf) {
        av_free(picture);
        return NULL;
    }
    avpicture_fill((AVPicture *)picture, picture_buf,
                   pix_fmt, width, height);
    return picture;
}

static int open_video(AVFormatContext *oc, AVStream *st)
{
    AVCodec *codec;
    AVCodecContext *c;

    c = st->codec;

    /* find the video encoder */
    codec = avcodec_find_encoder(c->codec_id);
    if (!codec) {
        log_write(LL_ERROR, "video codec not found");
        return -1;
    }

    /* open the codec */
    if (avcodec_open(c, codec) < 0) {
        log_write(LL_ERROR, "could not open video codec");
        return -1;
    }

    video_outbuf = NULL;
    if (!(oc->oformat->flags & AVFMT_RAWPICTURE)) {
        /* allocate output buffer */
        /* XXX: API change will be done */
        /* buffers passed into lav* can be allocated any way you prefer,
           as long as they're aligned enough for the architecture, and
           they're freed appropriately (such as using av_free for buffers
           allocated with av_malloc) */
        video_outbuf_size = 16000000;
        video_outbuf = av_malloc(video_outbuf_size);
    }

    /* allocate the encoded raw picture */
    picture = alloc_picture(c->pix_fmt, c->width, c->height);
    if (!picture) {
        log_write(LL_ERROR, "Could not allocate picture");
        return -1;
    }

    /* if the output format is not YUV420P, then a temporary YUV420P
       picture is needed too. It is then converted to the required
       output format */
    tmp_picture = NULL;
    if (c->pix_fmt != source_pix_fmt) {
        tmp_picture = alloc_picture(source_pix_fmt, c->width, c->height);
        if (!tmp_picture) {
            log_write(LL_ERROR, "Could not allocate temporary picture");
            return -1;
        }
    }

    return 0;
}

/* copy image data */
static void fill_image(AVFrame *pict, uint8_t *data, int width, int height)
{
    int x, y;

    if (source_pix_fmt == PIX_FMT_RGB24) {
        /* dest format RGB24 */
        /* source data is RGB+unused byte */
        for(y=0;y<height;y++) {
            int dest_off = y * pict->linesize[0];
            int src_off = y * width * 4;
            for(x=0;x<width;x++) {
                pict->data[0][dest_off + x*3] = data[src_off + x*4];
                pict->data[0][dest_off + x*3 + 1] = data[src_off + x*4 + 1];
                pict->data[0][dest_off + x*3 + 2] = data[src_off + x*4 + 2];
            }
        }
    } else {
        /* dest format RGB32 */
        /* source data is RGB+unused byte */
        for(y=0;y<height;y++) {
            int dest_off = y * pict->linesize[0];
            int src_off = y * width * 4;
            for(x=0;x<width;x++) {
                pict->data[0][dest_off + x*4] = data[src_off + x*4];
                pict->data[0][dest_off + x*4 + 1] = data[src_off + x*4 + 1];
                pict->data[0][dest_off + x*4 + 2] = data[src_off + x*4 + 2];
                pict->data[0][dest_off + x*4 + 3] = 255;
            }
        }
    }
}

static int write_video_frame(AVFormatContext *oc, AVStream *st, unsigned char *data, int *frame_written)
{
    int out_size, ret;
    AVCodecContext *c;

    c = st->codec;
    *frame_written = FALSE;

    if (data == NULL) {
        /* no more frame to compress. The codec has a latency of a few
           frames if using B frames, so we get the last frames by
           passing NULL */
    } else {
        static struct SwsContext *img_convert_ctx;
        if (c->pix_fmt != source_pix_fmt) {
            fill_image(tmp_picture, data, c->width, c->height);

            /* we must convert it to the codec pixel format if needed */
            if (img_convert_ctx == NULL) {
                img_convert_ctx = sws_getContext(c->width, c->height,
                                                 source_pix_fmt,
                                                 c->width, c->height,
                                                 c->pix_fmt,
                                                 sws_flags, NULL, NULL, NULL);
                if (img_convert_ctx == NULL) {
                    log_write(LL_ERROR, "Cannot initialize the conversion context");
                    return -1;
                }
            }
            sws_scale(img_convert_ctx, tmp_picture->data, tmp_picture->linesize,
                      0, c->height, picture->data, picture->linesize);
        } else {
            fill_image(picture, data, c->width, c->height);
        }

        picture->pts = frame_count++;
    }

    if (oc->oformat->flags & AVFMT_RAWPICTURE) {
        /* raw video case. The API will change slightly in the near
           futur for that */
        AVPacket pkt;
        av_init_packet(&pkt);

        pkt.flags |= AV_PKT_FLAG_KEY;
        pkt.stream_index= st->index;
        pkt.data= (uint8_t *)picture;
        pkt.size= sizeof(AVPicture);

        ret = av_interleaved_write_frame(oc, &pkt);
        *frame_written = TRUE;
    } else {

        /* encode the image */
        out_size = avcodec_encode_video(c, video_outbuf, video_outbuf_size, data != NULL ? picture : NULL);
        /* if zero size, it means the image was buffered */
        if (out_size > 0) {
            AVPacket pkt;
            av_init_packet(&pkt);

            if (c->coded_frame->pts != AV_NOPTS_VALUE)
                pkt.pts= av_rescale_q(c->coded_frame->pts, c->time_base, st->time_base);
            if(c->coded_frame->key_frame)
                pkt.flags |= AV_PKT_FLAG_KEY;
            pkt.stream_index= st->index;
            pkt.data= video_outbuf;
            pkt.size= out_size;

            /* write the compressed frame in the media file */
            ret = av_interleaved_write_frame(oc, &pkt);
            *frame_written = TRUE;
        } else {
            ret = 0;
        }
    }
    if (ret != 0) {
        log_write(LL_ERROR, "Error while writing video frame");
        return -1;
    }
    return 0;
}

static void close_video(AVFormatContext *oc, AVStream *st)
{
    avcodec_close(st->codec);
    av_free(picture->data[0]);
    av_free(picture);
    if (tmp_picture) {
        av_free(tmp_picture->data[0]);
        av_free(tmp_picture);
    }
    av_free(video_outbuf);
}

/**************************************************************/
/* media file output */

AVOutputFormat *fmt = 0;
AVFormatContext *oc = 0;
AVStream *audio_st = 0, *video_st = 0;
RingBuffer *ringBuffer = 0;

int ffmpeg_init()
{
    /* initialize libavcodec, and register all codecs and formats */
    av_register_all();

    ringBuffer = createBuffer();

    return 0;
}

int ffmpeg_set_format(int format)
{
    switch (format)
    {
        case FORMAT_FLV_SV:
            break;
        case FORMAT_FLV_VID:
            video_codec = 0;
            source_pix_fmt =  PIX_FMT_RGB32;
            stream_pix_fmt =  PIX_FMT_YUV420P;
            break;
        case FORMAT_H264:
            video_codec = CODEC_ID_H264;
            source_pix_fmt =  PIX_FMT_RGB32;
            stream_pix_fmt =  PIX_FMT_YUV420P;
            break;
    }

    return 0;
}

int ffmpeg_set_framerate(int rate)
{
    stream_frame_rate = rate;
    return 0;
}

int ffmpeg_set_bitrate(int vrate, int arate)
{
    video_bit_rate = vrate;
    audio_bit_rate = arate;
    return 0;
}

int ffmpeg_open_output(const char *filename)
{
    /* auto detect the output format from the name. default is flv. */
    fmt = av_guess_format(NULL, filename, NULL);
    if (!fmt) {
        log_write(LL_DEBUG, "Could not deduce output format from file extension: using flv.");
        fmt = av_guess_format("flv", NULL, NULL);
    }
    if (!fmt) {
        log_write(LL_ERROR, "Could not find suitable output format");
        return -1;
    }
    if (video_codec != 0)
        fmt->video_codec = video_codec;

    log_write(LL_DEBUG, "FFMPEG open file %s (format %d)", filename, fmt->video_codec);

    /* allocate the output media context */
    oc = avformat_alloc_context();
    if (!oc) {
        log_write(LL_ERROR, "Error allocating media context");
        return -1;
    }
    oc->oformat = fmt;
    snprintf(oc->filename, sizeof(oc->filename), "%s", filename);

    frame_count = 0;

    return 0;
}

int ffmpeg_prepare_stream(int width, int height)
{
    if (oc == NULL) {
        log_write(LL_ERROR, "Output not open");
        return -1;
    }

    log_write(LL_DEBUG, "FFMPEG open stream (%d, %d)", width, height);

    /* add the audio and video streams using the default format codecs
       and initialize the codecs */
    video_st = NULL;
    audio_st = NULL;
    if (fmt->video_codec != CODEC_ID_NONE) {
        video_st = add_video_stream(oc, fmt->video_codec, width, height);
    }
    if (fmt->audio_codec != CODEC_ID_NONE) {
        audio_st = add_audio_stream(oc, fmt->audio_codec);
    }

    /* set the output parameters (must be done even if no
       parameters). */
    if (av_set_parameters(oc, NULL) < 0) {
        log_write(LL_ERROR, "Invalid output format parameters");
        return -1;
    }

    av_dump_format(oc, 0, oc->filename, 1);

    /* now that all the parameters are set, we can open the audio and
       video codecs and allocate the necessary encode buffers */
    if (video_st)
        open_video(oc, video_st);
    if (audio_st)
        open_audio(oc, audio_st);

    /* open the output file, if needed */
    if (!(fmt->flags & AVFMT_NOFILE)) {
        if (avio_open(&oc->pb, oc->filename, URL_WRONLY) < 0) {
            log_write(LL_ERROR, "Could not open '%s'", oc->filename);
            return -1;
        }
    }

    /* write the stream header, if any */
    av_write_header(oc);

    return 0;
}

/* input frame is RGBU (where U is unused), so 32 bits per pixel */
int ffmpeg_write_video(unsigned char * data, int width, int height)
{
    // double audio_pts, video_pts;
    int frame_written;

    if (oc == NULL || video_st == NULL) {
        log_write(LL_ERROR, "Output not open");
        return -1;
    }

#if 0
    /* compute current audio and video time */
    if (audio_st)
        audio_pts = (double)audio_st->pts.val * audio_st->time_base.num / audio_st->time_base.den;
    else
        audio_pts = 0.0;

    if (video_st)
        video_pts = (double)video_st->pts.val * video_st->time_base.num / video_st->time_base.den;
    else
        video_pts = 0.0;

    /* write interleaved audio and video frames */
    if (!video_st || (video_st && audio_st && audio_pts < video_pts)) {
        write_audio_frame(oc, audio_st);
    } else {
        write_video_frame(oc, video_st);
    }
#endif

    return write_video_frame(oc, video_st, data, &frame_written);
}

/* input is 16 bit PCM, 1 channel, 11025 sample rate */
int ffmpeg_write_audio(short * data, int samples)
{
    int i;
    int blocks;

    if (oc == NULL || audio_st == NULL) {
        log_write(LL_ERROR, "Output not open");
        return -1;
    }

    writeIntoBuffer(ringBuffer, data, samples);

    blocks = samplesInBuffer(ringBuffer) / audio_input_frame_size;
    short buffered[audio_input_frame_size];

    /* loop through supplied data */
    for (i = 0; i < blocks; i++)
    {
        readFromBuffer(ringBuffer, buffered, audio_input_frame_size);
        write_audio_frame(oc, audio_st, buffered);
    }

    return 0;
}

int ffmpeg_close()
{
    int i;
    int frame_written;

    if (oc == NULL || video_st == NULL) {
        return -1;
    }

    log_write(LL_DEBUG, "FFMPEG closing streams");

    /* flush output streams */
    write_audio_frame(oc, audio_st, NULL);

    for (frame_written = 1; frame_written; ) {
        write_video_frame(oc, video_st, NULL, &frame_written);
    }

    log_write(LL_DEBUG, "FFMPEG frames encoded: %d", frame_count);

    /* write the trailer, if any.  the trailer must be written
     * before you close the CodecContexts open when you wrote the
     * header; otherwise write_trailer may try to use memory that
     * was freed on av_codec_close() */
    av_write_trailer(oc);

    /* close each codec */
    if (video_st)
        close_video(oc, video_st);
    if (audio_st)
        close_audio(oc, audio_st);
    video_st = NULL;
    audio_st = NULL;

    /* free the streams */
    for(i = 0; i < oc->nb_streams; i++) {
        av_freep(&oc->streams[i]->codec);
        av_freep(&oc->streams[i]);
    }

    if (!(fmt->flags & AVFMT_NOFILE)) {
        /* close the output file */
        avio_close(oc->pb);
    }

    clearRingBuffer(ringBuffer);

    /* free the stream */
    av_free(oc);
    oc = NULL;

    return 0;
}

