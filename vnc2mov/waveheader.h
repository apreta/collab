
#ifndef WAVEHEADER_H
#define WAVEHEADER_H

#include <stdint.h>

typedef struct WAVEHDR{
    char      format[4];  // RIFF
    uint32_t  f_len;      // filelength
    char      wave_fmt[8];// WAVEfmt_
    uint32_t  fmt_len;    // format lenght
    uint16_t  fmt_tag;    // format Tag
    uint16_t  channel;    // Mono/Stereo
    uint32_t  samples_per_sec;
    uint32_t  avg_bytes_per_sec;
    uint16_t  blk_align;
    uint16_t  bits_per_sample;
    uint32_t  unknown;
    char      fact[4];
    uint32_t  fact_len;
    uint32_t  fact_size;
    char      data[4];    // data
    uint32_t  data_len;   // data size
} wavehdr;

#endif
