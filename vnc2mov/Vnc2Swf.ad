! Application defaults file for vnc2swf.
! $Id: Vnc2Swf.ad,v 1.1 2004-10-12 15:49:03 eric Exp $
!
!  This file contains the same resource settings ("app-defaults") as the
! "fallback resources" embedded in the executable.  You can copy this file to
! /usr/lib/X11/app-defaults/Vnc2Swf (or equivalent) and edit it for site-wide
! customisations.


!
! The title of the main window.  "%s" will be replaced by the desktop name.
! 

Vnc2Swf.title: VNC2SWF %s: %s


!
! Translations on the main window.
!

Vnc2Swf.translations:\
  <Enter>: SelectionToVNC()\n\
  <Leave>: SelectionFromVNC()


!
! Background around a small desktop in full-screen mode.
!

*form.background: black


!
! Use scrollbars on right and bottom for window smaller than desktop.
!

*viewport.allowHoriz: True
*viewport.allowVert: True
*viewport.useBottom: True
*viewport.useRight: True
*viewport*Scrollbar*thumb: None


!
! Default translations on desktop window.
!

*desktop.baseTranslations:\
  <Key>F8: ShowPopup()\n\
  <Key>F9: ToggleRecording()\n\
  <Key>F10: SwitchRecordingMethod()\n\
  <Key>F11: ToggleStatus()\n\
  <ButtonPress>: SendRFBEvent()\n\
  <ButtonRelease>: SendRFBEvent()\n\
  <Motion>: SendRFBEvent()\n\
  <KeyPress>: SendRFBEvent()\n\
  <KeyRelease>: SendRFBEvent()
*desktop*statusFont: -*-helvetica-bold-r-*-*-12-*-*-*-*-*-*-*
*desktop*statusColor: white

!
! Dialog boxes
!

*serverDialog.dialog.label: VNC server:
*serverDialog.dialog.value:
*serverDialog.dialog.value.translations: #override\n\
  <Key>Return: ServerDialogDone()

*passwordDialog.dialog.label: Password:
*passwordDialog.dialog.value:
*passwordDialog.dialog.value.AsciiSink.echo: False
*passwordDialog.dialog.value.translations: #override\n\
  <Key>Return: PasswordDialogDone()


!
! Popup window appearance
!

*popup.title: VNC2SWF popup
*popup*background: grey
*popup*font: -*-helvetica-bold-r-*-*-16-*-*-*-*-*-*-*
*popup.buttonForm.Command.borderWidth: 0
*popup.buttonForm.Toggle.borderWidth: 0

!
! Translations on popup window - send key presses through
!

*popup.translations: #override <Message>WM_PROTOCOLS: HidePopup()
*popup.buttonForm.translations: #override\n\
  <KeyPress>: SendRFBEvent() HidePopup()


!
! Popup buttons
!

*popupButtonCount: 12

*popup*button1.label: Dismiss Popup
*popup*button1.translations: #override\n\
  <Btn1Down>,<Btn1Up>: HidePopup()

*popup*button2.label: Quit Vnc2Swf
*popup*button2.translations: #override\n\
  <Btn1Down>,<Btn1Up>: Quit()

*popup*button3.label: Full Screen
*popup*button3.type: toggle
*popup*button3.translations: #override\n\
  <Visible>: SetFullScreenState()\n\
  <Btn1Down>,<Btn1Up>: ToggleFullScreen() HidePopup()

*popup*button4.label: Clipboard: local -> remote
*popup*button4.translations: #override\n\
  <Btn1Down>,<Btn1Up>: SelectionToVNC(always) HidePopup()

*popup*button5.label: Clipboard: local <- remote
*popup*button5.translations: #override\n\
  <Btn1Down>,<Btn1Up>: SelectionFromVNC(always) HidePopup()

*popup*button6.label: Send Ctrl-Alt-Del
*popup*button6.translations: #override\n\
  <Btn1Down>,<Btn1Up>: SendRFBEvent(keydown,Control_L)\
                       SendRFBEvent(keydown,Alt_L)\
                       SendRFBEvent(key,Delete)\
                       SendRFBEvent(keyup,Alt_L)\
                       SendRFBEvent(keyup,Control_L)\
                       HidePopup()

*popup*button7.label: Send F8
*popup*button7.translations: #override\n\
  <Btn1Down>,<Btn1Up>: SendRFBEvent(key,F8) HidePopup()

*popup*button8.label: Auto Select Format/Encoding
*popup*button8.type: toggle
*popup*button8.translations: #override\n\
  <Visible>: SetAutoState()\n\
  <Btn1Down>,<Btn1Up>: ToggleAuto() HidePopup()

*popup*button9.label: Use 8-bit colour
*popup*button9.type: toggle
*popup*button9.translations: #override\n\
  <Visible>: SetBGR233State()\n\
  <Btn1Down>,<Btn1Up>: ToggleBGR233() HidePopup()

*popup*button10.label: Recording
*popup*button10.type: toggle
*popup*button10.translations: #override\n\
  <Visible>: SetRecordingState()\n\
  <Btn1Down>,<Btn1Up>: ToggleRecording() HidePopup()

*popup*button11.label: Switch Recording Method
*popup*button11.translations: #override\n\
  <Btn1Down>,<Btn1Up>: SwitchMethod() HidePopup()

*popup*button12.label: Reset Recording
*popup*button12.translations: #override\n\
  <Btn1Down>,<Btn1Up>: ResetRecording() HidePopup()

