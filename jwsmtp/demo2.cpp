// This file is part of the jwSMTP library.
//
//  jwSMTP library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  jwSMTP library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with jwSMTP library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// jwSMTP library Version 1.23
//   http://johnwiggins.net
//   jwSMTP@johnwiggins.net
//
#include <iostream>
// Please note the jwsmtp library has to be installed for this next header to work
#include <jwsmtp/jwsmtp.h>

int main(int argc, char* argv[])
{
	//                 to     from     subject         message       smtp server
//	jwsmtp::mailer m("root", "root", "subject line", "message body", "localhost",
//                    jwsmtp::mailer::SMTP_PORT, false);
	//                 to     from     subject         message       smtp server
	jwsmtp::mailer m("dkatcher@comcast.net", "dkatcher@comcast.net", "test message", 
					"click on the link: (http://www.yahoo.com)", "smtp.comcast.net",
                    jwsmtp::mailer::SMTP_PORT, false);
	m.operator()(); // send the mail
	std ::cout << m.response() << "\n";
	return 0;
}
