// This file is part of the jwSMTP library.
//
//  jwSMTP library is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  jwSMTP library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with jwSMTP library; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// jwSMTP library Version 1.23
//   http://johnwiggins.net
//   jwSMTP@johnwiggins.net
//
// $Header: /home/mfassbinder/Desktop/conferencing/cvsroot/jwsmtp/mail.cpp,v 1.1.1.1 2004-02-10 22:08:19 eric Exp $
//
// http://www.boost.org
//#include <boost\thread\thread.hpp>

#include <iostream>
#include "mailer.h"

using std::cout;
using std::cin;
using std::string;

void Usage() {
	cout << "jwSMTP library demo program\n"
			"maildemo <email toaddress> <email fromaddress> <smtpserver>\n"
			"  e.g.\n"
			"    maildemo you@there.com me@here.com mail.here.com\n";
}

void testauth()
{
	// sample using imidio's server
	// to, from, sub, message, smtp, port, mxlookup
	string user;
	string pass;
	jwsmtp::mailer mail("dkatcher@comcast.net", "dkatcher@imidio.com", "test auth", "if you got this auth works",
				"mail.imidio.com", jwsmtp::mailer::SMTP_PORT, false);
	mail.addauthentication(user, pass);
	mail.operator()();
	cout << mail.response() << "\n\n";
}

int main(int argc, char* argv[])
{
	testauth();

	if(argc != 4) {
		Usage();
		return 0;
	}

	cout << "jwSMTP library demo program\n\n";
	string to(argv[1]);
	string from(argv[2]);
	string smtpserver(argv[3]);

	if(to.length() < 2 || from.length() < 2 || smtpserver.length() < 2) {
		Usage();
		return 0;
	}

	char str[2048];
	cout << "Please enter the subject of the mail\n";
	cin.getline(str, 500);	
	string subject(str);
	strcpy(str, "");

	cout << "Please enter the message body end with \".\" on a line by itself\n";
	string mailmessage;
	while(true) {
		cin.getline(str, 2048);
		if(!strcmp(str, "."))
			break;
		
		mailmessage += str;
		mailmessage += "\r\n";
		strcpy(str, "");
	}

	cout << "\nPlease wait sending mail\n";
	// This is how to tell the mailer class that we are using a direct smtp server
	// preventing the code from doing an MX lookup on 'smtpserver' i.e. passing
	// false as the last parameter.
	jwsmtp::mailer mail(to.c_str(), from.c_str(), subject.c_str(), mailmessage.c_str(),
				smtpserver.c_str(), jwsmtp::mailer::SMTP_PORT, false);

	// using a local file as opposed to a full path.
	mail.attach("attach.png");

	// add another recipient (carbon copy)
	//mail.addrecipient("someoneelse@somewhere.net", mailer::Cc);

	// set a new smtp server! This is the same as setting a nameserver.
	// this depends on the constructor call. i.e. in the constructor
	// If MXLookup is true this is a nameserver
	// If MXLookup is false this is an smtp server
	//mail.setserver("mail.somewherefun.com");
	// same again except using an IP address instead.
	//mail.setserver("192.168.0.1");

	// boost::thread thrd(mail);
	// thrd.join(); // optional
	// or:-

	mail.operator()();
	cout << mail.response() << "\n\n";

	//mail.reset(); // now we can mail someone else.
	//mail.setmessage("flibbletooting");
	//mail.setsubject("another message same object");
	//mail.attach("/home/user1/image.gif");
	// or a win example
	//mail.attach("C:\\image.gif");
	//mail.addrecipient("someoneelseagain@foobar.net");

	//mail.operator ()();
	//cout << mail.response() << "\n";

	return 0;
}
