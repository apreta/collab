# Apreta Conferencing

Based on the now defunct Kablink Conferencing project, Apreta Conferencing provides real time
communication services.

The primary feature is online meetings in which users can share documents and presentations,
share their desktop or applications from their desktop, chat, and talk (using a built-in softphone
and Askterisk server).

The project consists of a server which runs on Linux (RHEL or CentOS 6 are currently supported) and 
a desktop client (Windows and OS X are currently supported).

The server consists of several components (message router, address book, scheduler, meeting controller, voice
bridge, meeting archiver, etc.) that connect over an XMPP backbone provided by the message router.

Additional services can connect via the XMPP backbone, or by a Web API.

Build instructions for all are provided in BUILD.txt
