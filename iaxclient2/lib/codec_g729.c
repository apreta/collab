/*
 * iaxclient: a portable telephony toolkit
 *
 * Copyright (C) 2003-2004, Horizon Wimba, Inc.
 *
 * Steve Kann <stevek@stevek.com>
 *
 * This program is free software, distributed under the terms of
 * the GNU Lesser (Library) General Public License
 */

#include "codec_g729.h"
#include "iaxclient_lib.h"

#ifdef CODEC_G729A
#include <g729a_api.h>
#endif

struct state {
    plc_state_t plc;
};

// We should only allow one encoder & one decoder, but we can't tell which is being allocated
// at initialization time, so just keep a simple counter and allow two instances.
static int codec_use_count = 0;


static void destroy ( struct iaxc_audio_codec *c) {

    //struct state * encstate = (struct state *) c->encstate;
    //struct state * decstate = (struct state *) c->decstate;

	--codec_use_count;

    free(c->encstate);
    free(c->decstate);
    free(c);
}


static int decode ( struct iaxc_audio_codec *c, 
    int *inlen, unsigned char *in, int *outlen, short *out ) {

    struct state * decstate = (struct state *) c->decstate;

    /* use generic interpolation */
    if(*inlen == 0) {
        int interp_len = 160;
        if(*outlen < interp_len) interp_len = *outlen;
        plc_fillin(&decstate->plc,out,interp_len);
        *outlen -= interp_len;
        return 0;
    }

    /* need to decode minimum of 20 bytes to 160 byte output */
    while( (*inlen >= 20) && (*outlen >= 160) ) {

#ifdef CODEC_G729A
        g729a_decode_block(in, out);
        g729a_decode_block(in + 10, out + 80);
#endif

        /* push decoded data to interpolation buffer */
        plc_rx(&decstate->plc,out,160);

        /* we used 20 bytes of input, and 160 bytes of output */
        *inlen -= 20; 
        in += 20;
        *outlen -= 160;
        out += 160;
    }

    return 0;
}


static int encode ( struct iaxc_audio_codec *c, 
    int *inlen, short *in, int *outlen, unsigned char *out ) {

    //struct state * encstate = (struct state *) c->encstate;

    /* need to encode minimum of 160 bytes to 20 byte output */
    while( (*inlen >= 160) && (*outlen >= 20) ) {

#ifdef CODEC_G729A
        g729a_encode_block(in, out);
        g729a_encode_block(in + 80, out + 10);
#endif

        /* we used 160 bytes of input, and 20 bytes of output */
        *inlen -= 160; 
        in += 160;
        *outlen -= 20;
        out += 20;
    }

    return 0;
}

struct iaxc_audio_codec *codec_audio_g729a_new() {
  
  struct iaxc_audio_codec *c;
  
  if (codec_use_count >= 2) {
    fprintf(stderr, "codec_g729: codec already in use (only one g729 codec allowed)\n");
    return NULL;
  }
  
  c = calloc(sizeof(struct iaxc_audio_codec),1);
  
  if(!c) return c;

  strcpy(c->name,"g729a");
  c->format = IAXC_FORMAT_G729A;
  c->encode = encode;
  c->decode = decode;
  c->destroy = destroy;

  c->minimum_frame_size = 160;

  c->encstate = calloc(sizeof(struct state),1);
  c->decstate = calloc(sizeof(struct state),1);

  /* leaks a bit on no-memory */
  if(!(c->encstate && c->decstate)) 
      return NULL;

#ifdef CODEC_G729A
  g729a_init();

  ++codec_use_count;
#endif

  return c;
}

