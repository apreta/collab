/*
 * iaxclient: a portable telephony toolkit
 *
 * Copyright (C) 2003-2004, Horizon Wimba, Inc.
 *
 * Steve Kann <stevek@stevek.com>
 *
 * This program is free software, distributed under the terms of
 * the GNU Lesser (Library) General Public License
 */

#include "codec_g726.h"
#include "iaxclient_lib.h"
#include "g726/g726.h"

struct state {
	g726_state_t g726state;
    plc_state_t plc;
};


static void destroy ( struct iaxc_audio_codec *c) {
    free(c->encstate);
    free(c->decstate);
    free(c);
}


static int decode ( struct iaxc_audio_codec *c,
    int *inlen, unsigned char *in, int *outlen, short *out ) {
    struct state * decstate = (struct state *) c->decstate;

#if 0
    /* use generic interpolation */
    if(*inlen == 0) {
        int interp_len = 160;
        if(*outlen < interp_len) interp_len = *outlen;
        plc_fillin(&decstate->plc,out,interp_len);
        *outlen -= interp_len;
        return 0;
    }
#endif

    /* need to decode minimum of 80 bytes to 160 byte output */
    while( (*inlen >= 80) && (*outlen >= 160) ) {
        g726_decode(&decstate->g726state, out, in, 80);

#if 0
        /* push decoded data to interpolation buffer */
        plc_rx(&decstate->plc,out,160);
#endif
        /* we used 80 bytes of input, and 160 bytes of output */
        *inlen -= 80;
        in += 80;
        *outlen -= 160;
        out += 160;
    }

    return 0;
}

static int encode ( struct iaxc_audio_codec *c,
    int *inlen, short *in, int *outlen, unsigned char *out ) {

    struct state * encstate = (struct state *) c->encstate;


    /* need to encode minimum of 160 bytes to 80 byte output */
    while( (*inlen >= 160) && (*outlen >= 80) ) {
        g726_encode(&encstate->g726state, out, in, 160);

        /* we used 160 bytes of input, and 80 bytes of output */
        *inlen -= 160;
        in += 160;
        *outlen -= 80;
        out += 80;
    }

    return 0;
}

struct iaxc_audio_codec *codec_audio_g726_new() {

  struct state * encstate;
  struct state * decstate;
  struct iaxc_audio_codec *c = calloc(sizeof(struct iaxc_audio_codec),1);


  if(!c) return c;

  strcpy(c->name,"g726");
  c->format = IAXC_FORMAT_G726;
  c->encode = encode;
  c->decode = decode;
  c->destroy = destroy;

  c->minimum_frame_size = 160;

  c->encstate = calloc(sizeof(struct state),1);
  c->decstate = calloc(sizeof(struct state),1);

  /* leaks a bit on no-memory */
  if(!(c->encstate && c->decstate))
      return NULL;

  encstate = (struct state *) c->encstate;
  decstate = (struct state *) c->decstate;

  g726_init(&encstate->g726state, 32000, G726_ENCODING_LINEAR, G726_PACKING_LEFT);
  g726_init(&decstate->g726state, 32000, G726_ENCODING_LINEAR, G726_PACKING_LEFT);

  return c;
}

