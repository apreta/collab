
#define _GNU_SOURCE
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#define MAX_LOG_FILENAME 512

static FILE * log_file = NULL;

// If the current log is >= trigger_sz, put current log in history
// and push history down
static size_t trigger_sz = 1024 * 1024;

// The number of log files to keep before deleting the oldest
static unsigned history_len = 10;

void log_set_rotation_size(size_t _trigger_sz)
{
    trigger_sz = _trigger_sz;
}

void log_set_history_len(int _history_len)
{
    history_len = _history_len;
}

static char log_file_name[MAX_LOG_FILENAME];

static void log_rotate_if_needed()
{
  char newerfn[MAX_LOG_FILENAME];
  char olderfn[MAX_LOG_FILENAME];
  int i;
  long sz;

  fseek(log_file, 0, SEEK_END);
  sz = ftell(log_file);

  if (sz < trigger_sz) return;

  fclose(log_file);

  sprintf(newerfn, "%s.%u", log_file_name, history_len);
  unlink(newerfn);
  for (i = history_len-1; i > 0; i--) {
      strcpy(olderfn, newerfn);
      sprintf(newerfn, "%s.%u", log_file_name, i);
      if (rename(newerfn, olderfn) < 0) {
          if (errno != ENOENT) {
              log_file = fopen(log_file_name, "a+");
              fprintf(stderr, "Couldn't rotate logs: %s\n", strerror(errno));
          }
      }
  }
  strcpy(olderfn, newerfn);
  strcpy(newerfn, log_file_name);
  if (rename(newerfn, olderfn) < 0) {
    if (errno != ENOENT) {
      log_file = fopen(log_file_name, "a+");
      fprintf(stderr, "Couldn't rotate logs: %s\n", strerror(errno));
    }
  }

  log_file = fopen(log_file_name, "a+");
  if(log_file == NULL) {
    fprintf(stderr,
	      "couldn't open %s for append: %s\n"
	      "logging will go to stderr instead\n", log_file_name, strerror(errno));
  }
}

void log_write(int level, const char *msg)
{
    FILE * f;

	if (log_file) log_rotate_if_needed();

    f = log_file != NULL ? log_file : stderr;

    fprintf(f, "%s", msg);
    //fprintf(f, "\n");
    fflush(f);
}

void log_new(char *ident)
{
    snprintf(log_file_name, MAX_LOG_FILENAME, "%s.log", ident);

    log_file = fopen(log_file_name, "a+");
    if(log_file == NULL) {
        fprintf(stderr,
            "couldn't open %s for append: %s\n"
            "logging will go to stderr instead\n", log_file_name, strerror(errno));
    }
}


main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "Usage: logger <filename>\n");
		exit(-1);
	}
	
	log_new(argv[1]);	

	int size = 2048;
	char *buffer = malloc(size);
	int read;
	
	while (1) {
		read = getline(&buffer, &size, stdin);
		if (read < 0)
			exit(0);
		log_write(0, buffer);
	}
}
