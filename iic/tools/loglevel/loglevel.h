// loglevel.h : main header file for the LOGLEVEL application
//

#if !defined(AFX_LOGLEVEL_H__2316741E_0E75_4B37_B1F9_F15BCFAF613A__INCLUDED_)
#define AFX_LOGLEVEL_H__2316741E_0E75_4B37_B1F9_F15BCFAF613A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CLogLevelApp:
// See loglevel.cpp for the implementation of this class
//

class CLogLevelApp : public CWinApp
{
public:
	CLogLevelApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogLevelApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CLogLevelApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGLEVEL_H__2316741E_0E75_4B37_B1F9_F15BCFAF613A__INCLUDED_)
