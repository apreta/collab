extern CString async_msg;
extern int async_error;

int connect(const char * server, const char * user, const char * password);

int set_jabber_level(const char *server, int debug);
int set_level(const char * service, int level);

void shutdown();
