// log_set.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <time.h>

#include "../../jclient/jclient.h"
#include "../../iic/services/common/address.h"
#include "../../iic/services/common/common.h"
#include "../../iic//services/common/eventnames.h"


/* Other defines */
#define CHUNK_SIZE 50
#define LOOP 1

CString log_in_server;
CString log_in_user;
CString log_in_screen;
CString log_in_password;


char sid[64];
char userid[64];
char username[64];
char directoryid[64];
char groupid[64];

char c_sid[64];

int start;
int row_count = 0;
int groupnum = 0;
int usernum = 0;

CString async_msg;
int async_error;

JArray *directorylist = NULL;

class SEvent;
JSession *ses;
SEvent *event;


class SEvent : public JSessionEvent
{
	JSession *session;

	bool connected;

	long wait_count;
	HANDLE wait_event;
	
public:
	SEvent(JSession *_session)
	{
		session = _session;
		session->register_handler(this);
		wait_count = 0;
		connected = false;
		wait_event = CreateEvent(NULL, FALSE, FALSE, NULL);
	}

	void on_error(int err, const char *msg, int code, const char *id = NULL)
	{
		TRACE("Error: %d - %s [%s]\n", err, msg, id);
		if (err == ERR_AUTHFAILED)
			session->send_register();
		else
		{
			async_msg = msg;
			async_error = -3;
		}
	}

	void on_connected()
	{
		connected = true;
	}

	void on_presence_packet(ikspak *pak)
	{
		const char * type = iks_find_attrib(pak->x, "type");
		const char * status = iks_find_cdata(pak->x, "status");
	}

	void on_rpc_request(const char *id, const char *method, JResultSet *result)
	{
	}

	void on_rpc_result(const char *id, JResultSet *result)
	{
		if (!strcmp(id, "log_level"))
		{
			TRACE("RPC: completed, result = %d\n", result->get_status());
			async_error = result->get_status();
			async_msg = "Log level set.";
		}
		else if (!strcmp(id, "logon"))
		{
			char *_sid;
			result->get_output_values("(s*)", &_sid);
			TRACE("Client registered, session id is %s\n", _sid);
			strcpy(c_sid, _sid);
		}
		else if (!strcmp(id, "extlogon"))
		{
			char *_sid, *_url, *_dummy;
			int status;
			result->get_output_values("(sssi*)", &_sid, &_dummy, &_url, &status);
			strcpy(c_sid, _sid);
		}
		else if (!strcmp(id, "auth"))
		{
			//   SESSIONID, USERID, USERNAME, COMMUNITYID, 
			//   DIRECTORYID (for personal directory), DEFAULTGROUPID (for personal directory),
			//   USERTYPE, ISSUPERADMIN, ENABLEDFEATURES, PROFILEOPTIONS
			//   DEFPHONE, BUSPHONE, HOMEPHONE, MOBILEPHONE, OTHERPHONE, 
			//   DEFMEETING, FIRST, LAST, 
			//   SYSTEMPHONE (from community), SYSTEMURL (from community),
			//   If returned USERID is empty string, user was not found or password is incorrect.
			char *_sid, *_userid, *_username, *_communityid, *_directoryid, *_groupid;
			int type;
			result->get_output_values("(ssssssi*)", &_sid, &_userid, &_username, &_communityid, &_directoryid, &_groupid, &type);
			if (strlen(_userid) == 0)
			{
				async_error = -1;
				async_msg = "Authentication failed.";
			}
			else
			{
				async_error = 0;
				async_msg = "Ok";
			}
		}

		int wait = InterlockedDecrement(&wait_count);
		printf("%s completed, waiting = %d\n", id, wait);
		if (!wait)
			SetEvent(wait_event);
	}

	bool wait(int timeout)
	{
		int res = WaitForSingleObject(wait_event, timeout);
		if (res == WAIT_OBJECT_0)
			return true;
		return false;
	}

	void inc_wait(const char *id)
	{
		int wait = InterlockedIncrement(&wait_count);
		if (!wait)
			SetEvent(wait_event);
	}

	bool is_connected()
	{
		return connected;
	}
};


int connect(const char * server, const char * user, const char * password)
{
	JSession::initialize();
	async_error = 0;
	async_msg = "";

	log_in_server = server;
	log_in_user = user;
	log_in_password = password;

	CString SesID;
	SesID.Format("%s@%s/iic%d", log_in_user, log_in_server, GetCurrentProcessId());

	SetConsoleTitle(SesID);

	ses = new JSession(SesID, log_in_password, NULL);
	event = new SEvent(ses);

	if (ses->connect(FALSE))
	{
		async_msg = "Failed to connect.";
		async_error = -1;
		return async_error;
	}

	/* Not the way to build a real client, but wait for connection event */
	bool sent_reg = false;
	int count = 20;
	while (!event->is_connected())
	{
		Sleep(200);
		if (--count == 0)
		{
			async_msg = "Unable to authenticate with server.";
			async_error = -1;
			break;
		}
	}

	return async_error;
}

int set_jabber_level(const char *server, int debug)
{
	/* example:
	   <iq from="some component" to="loglevel.service@cluster" type="set">
		   <query xmlns="iic:loglevel">
			   <level>1</level>
		   </query>
	   </iq>
	 */
	char request[512], to[512];
	sprintf(to, "loglevel.service@%s", server);
	sprintf(request, "<level>%d</level>", debug);

	if (!ses->make_iq_call(IKS_TYPE_SET, "iic:loglevel", (char*)to, request))
	{
		async_msg = "Set log level call failed.";
		async_error = -2;
	}

	return async_error;
}

int set_level(const char * service, int level)
{
	event->inc_wait("log_level");
	if (ses->make_rpc_call("log_level", service, "log.set_level", "(i)", level))
	{
		async_msg = "RPC call failed.";
		async_error = -2;
		return async_error;
	}
	if (!event->wait(4000))
	{
		async_msg = "No response from service.";
		async_error = -2;
	}

	return async_error;
}

void shutdown()
{
	ses->disconnect(TRUE);
	delete ses;
	delete event;
}
