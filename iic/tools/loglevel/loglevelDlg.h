// loglevelDlg.h : header file
//

#if !defined(AFX_LOGLEVELDLG_H__5F6A093B_8EF9_4384_97EF_EF7E6623125F__INCLUDED_)
#define AFX_LOGLEVELDLG_H__5F6A093B_8EF9_4384_97EF_EF7E6623125F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CLogLevelDlg dialog

class CLogLevelDlg : public CDialog
{
// Construction
public:
	CLogLevelDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CLogLevelDlg)
	enum { IDD = IDD_LOGLEVEL_DIALOG };
	CEdit	m_Server;
	CEdit	m_Service;
	CEdit	m_Password;
	CComboBox	m_LogLevel;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLogLevelDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CLogLevelDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LOGLEVELDLG_H__5F6A093B_8EF9_4384_97EF_EF7E6623125F__INCLUDED_)
