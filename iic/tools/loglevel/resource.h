//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by loglevel.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_LOGLEVEL_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDC_PASSWORD                    1000
#define IDC_SERVICE                     1001
#define IDC_SERVER                      1002
#define IDC_LOGLEVEL                    1003
#define IDC_TYPE_JABBER                 1005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
