#!/usr/bin/env python
#Boa:PyApp:main

import sys;
import glob;
import pgdb;
import csv;

modules ={}

class User:
    def __init__(self,conn,userid):
        cursor = conn.cursor();
        cursor.execute("select contacts.screenname, firstname, lastname, email, communityname"
            " from contacts,communities,users where contacts.userid=%s and users.userid=contacts.userid and communities.communityid=users.communityid"
            , (userid, ) );
        row = cursor.fetchone();
        if row == None:
            self.screenname = "unknown";
            self.firstname = "unknown";
            self.lastname = "unknown";
            self.email = "unknown";
            self.community = "unknown";
            return
        self.screenname = row[0];
        self.firstname = row[1];
        self.lastname = row[2];
        self.email = row[3];
        self.community = row[4];
        
class CallReport:
    def main(self):
        self.conn = pgdb.connect("iic.homedns.org", "zon");
        
        for arg in sys.argv[1:]:
            for filename in glob.glob(arg):
                sys.stderr.write("Processing " + filename + "\n")               
                reader = csv.reader(open(filename, "rb"))
                for row in reader:
                    if row[3] == "3" or row[3] == "4":
                        user = User(self.conn, row[8]);
                        time = row[1]
                        duration = row[2];  # only for event 6
                        if row[4] == "":
                            info = "unknown"
                        else:
                            info = row[4]
                        print "%s: %s %s [%s; %s] to %s" % (time, user.firstname, user.lastname, user.email, user.community, info)
                
if __name__ == '__main__':
    app = CallReport()
    app.main()
