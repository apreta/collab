BEGIN	{ calls = 0; max = 0 }

$4 == "\"3\"" || $4 == "\"4\"" 	{ 
				  calls = calls + 1; 
				  if (calls > max) max = calls; 
				}

$4 == "\"6\""   { 
                  calls = calls - 1;
                  print "Duration: " $3 "\tHostID: " $9 "\tNumber: " $5
                }

END	{ print "Max calls: " max }


