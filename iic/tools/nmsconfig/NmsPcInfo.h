#ifndef NMSPCINFO_H
#define NMSPCINFO_H

#if defined (WIN32)
    #ifdef NMSPCINFOCLASS_EXPORTS
        #define _NMSPCINFOCLASS      __declspec( dllexport )
    #else
        #define _NMSPCINFOCLASS      __declspec( dllimport )
    #endif
#else
    #define _NMSPCINFOCLASS
#endif

#ifndef USE_PCIDEV
#define  HSBOARD_ID           0x06
#endif // not USE_PCIDEV


// drivers

#ifdef WIN32

#define AG_DRIVER_NAME  "aghw"
#define CX_DRIVER_NAME  "cxhw"
#define CG_DRIVER_NAME  "cg6k"
#define TX_DRIVER_NAME "FIXME"

#else

#ifdef _SunOS_

#define AG_DRIVER_NAME  "ag"
#define CX_DRIVER_NAME  "cx"
#define CG_DRIVER_NAME  "cg6k"
#define TX_DRIVER_NAME  "FIXME"

#else

#define AG_DRIVER_NAME  "aghw"
#define CX_DRIVER_NAME  "cx"
#define CG_DRIVER_NAME  "cg6k"
#define TX_DRIVER_NAME  "txpci"

#endif // Solaris
#endif // WIN32


/* Error codes */
#define ERRSCAN_NO_ECP        0x2000
#define ERRSCAN_NO_HSR        0x2001
#define ERRSCAN_INVALID_DEV   0x2002
#define ERRSCAN_UNKNOWN       0x2003

/* Definition of the size of each Structure */
#define S_STRING		255
#define PCI_CFG_SPACE	255

// Definition de NMS vendor ID
#define  NMS_VENDOR_ID        0x12b6  /* 4790 decimal */

struct DeviceInfoStr
{
	char  Nmsboardtype[S_STRING];
	DWORD bus;
	DWORD dev;
	DWORD fun;
	BYTE  pci_cfg [PCI_CFG_SPACE];

	DWORD pci_base_address_0;
	DWORD pci_base_address_1;
	DWORD pci_base_address_2;
	DWORD pci_base_address_3;
	DWORD pci_base_address_4;
	WORD  pci_command;

	WORD vendor_id;
	WORD sub_vendor_id;
	WORD device_id;
	WORD sub_system_id;

	BYTE pci_revision;
	BYTE interrupt;
	bool b_isHotswap;

    bool b_isExtraInfoAvailable;
    WORD NumDSPCores;          // number of DSP cores
    BYTE NumSwitch;            // number of switch
    BYTE NumLine;              // number of line
    BYTE NumEthernet;          // number of ethernet
    BYTE NumDaughterCard;	   // Non-zero indicates presence of a daughter card
    BYTE NumCPU;               // number of CPU
};

#ifdef USE_PCIDEV
class PciDevice;        // Forward declaration.. cpp includes this.
#endif // not USE_PCIDEV

#ifdef __cplusplus
extern "C" {
#endif
	class _NMSPCINFOCLASS cNmsPcInfo
	{
		
	public:
		cNmsPcInfo();
		~cNmsPcInfo();

		DWORD Initialize();
		DWORD Release();
		DWORD ScanAllPCIBus(DWORD vendorID, unsigned int &nb_devices );
		DWORD GetDeviceInfo(unsigned int devnumber, DeviceInfoStr *devinfo);

		DWORD BlinkLED(DWORD n_pciBus, DWORD n_logSlot, unsigned int duration);

        // Used to get some special board information. Those information are saved 
        // in the outputstring
		DWORD PrintBoardInfo(FILE *stream, DWORD n_pciBus, DWORD n_logSlot);

        // Used to retrieve error text from this DLL
		const char *GetPBSError (DWORD dwCode);

        // Lookup the board name, family and driver name

        DWORD LookupBoardName(int vendor_id, int subvendor_id, 
                              int sub_system_id, char* family,
                              char* name, int name_len, char* driver_name,
                              int driver_name_len );

	private:
		DWORD AllocateDeviceInfo();
		DWORD ReleaseDeviceInfo();
		char *GetProductName(int sub_system_id);
#ifdef USE_PCIDEV
        DWORD AllocateDevice(PciDevice & dev);
#else  // not USE_PCIDEV
        DWORD AllocateDevice(DWORD vendorID, BYTE *pci_cfg, DWORD n_bus, DWORD n_dev, DWORD n_fun);

		DWORD ScanPciBus(DWORD n_bus, DWORD vendorID, unsigned int brecursive);
#endif // not USE_PCIDEV

        DWORD ExtractNMSDirectory(char *nmsdir);

		DeviceInfoStr 	*m_pDeviceInfo;
		unsigned int 	 m_NbDevices;
		unsigned int 	 m_NbAllocatedDevices;
        char             m_errorstr[255];
	};

#ifdef __cplusplus
};
#endif

#endif      //NMSPCINFO_H

