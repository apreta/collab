
typedef struct _section {
	int numlines;
	int numvars;
	char *line[100];
}SectionConfig;

typedef struct _boardDSP {
	int totalDSP;
    int numTrunks;
	int confDSP;
	int ivrDSP;
	int ipDSP;
}BoardDSP;

typedef struct _recources {
	int confPerDSP;
	int ivrPerDSP;
	int ipPerDSP;
	int numBoards;
	BoardDSP Board[10];
}Resource;

// PCI board variants, CG6060 and CG6565 families
//  8 - 1100 mips - 2 trunks
// 12 - 1600 mips - 4,8
// 24 - 3200 mips - 4,8
// 32 - 4200 mips - 4,8
// 48 - 6400 mips - 4,8
//
// Assuming 4 IP channels per DSP to allow for iLBC, g.729 would be 5
//
// ToDo: 
// Started to add trunk info to this matrix, but am assuming 4 trunk
// variants for now, add more configs later so user can pick number
// of trunks to use

Resource IPOnlyResource = { 31, 16, 4, 5,
							{
								{8, 0, 1, 2, 5},    // 32 channels, 20 VoIP
								{12, 0, 2, 3, 7},   // 32 channels, 28 VoIP
								{24, 0, 4, 5, 15},  // 80 channels, 60 VoIP
								{32, 0, 4, 7, 21},  // 112 channels, 84 VoIP
								{48, 0, 6, 10, 32}  // 160 channels, 128 VoIP
							}
};

Resource IPTDMResource = { 31, 16, 4, 5,
							{
								{8, 2, 1, 5, 2},    // 60 channels, 8 VoIP
								{12, 4, 2, 6, 4},   // 96 channels, 16 VoIP
								{24, 4, 4, 8, 12},  // 128 channels, 48 VoIP
								{32, 4, 6, 10, 16}, // 160 channels, 64 VoIP
								{48, 4, 8, 16, 24}  // 256 channels, 96 VoIP
							}
};

char *genConfIndex(int numdsp, Resource res)
{
	int i;
	char index[50];
	char *ret;

	index[0] = '0';
    index[1] = 0;
	for (i=0; i<res.numBoards; i++)
	{
		if (res.Board[i].totalDSP == numdsp)
		{
			if (res.Board[i].confDSP != 1)
				sprintf(index,"0..%d",res.Board[i].confDSP - 1);
			break;
		}
	}
    ret = (char *) malloc(sizeof(char) * strlen(index)+1);
    strcpy(ret,index);
	return ret;

}

char *genIVRIndex(int numdsp, Resource res)
{
	int i,j;
	char num[50];
	char index[200];
	char *ret;
    int confdsp;
    int ivrdsp;

	index[0] = 0;
 
	for (i=0; i<res.numBoards; i++)
	{
		if (res.Board[i].totalDSP == numdsp)
		{
			confdsp = res.Board[i].confDSP;
			ivrdsp = res.Board[i].ivrDSP;

			for (j=0; j<ivrdsp; j++)
			{
				   sprintf(num,"%d ",j+confdsp);
		           strcat(index,num);		
			}
		}
	}
    ret = (char *) malloc(sizeof(char) * strlen(index) +1);
    strcpy(ret,index);
	return ret;

}

char *getIVRSize(int numdsp, Resource res)
{
	int i;
	char index[200];
	char *ret;
    int size;
    

	index[0] = 0;
 
	for (i=0; i<res.numBoards; i++)
	{
		if (res.Board[i].totalDSP == numdsp)
		{
			size = res.Board[i].ivrDSP * res.ivrPerDSP;
			sprintf(index,"%d",size);
		}
	}

    ret = (char *) malloc(sizeof(char) * strlen(index) +1);
    strcpy(ret,index);
	return ret;
}

char *genIPIndex(int numdsp, Resource res)
{
	int i,j;
	char num[50];
	char index[200];
	char *ret;
    int confdsp;
    int ivrdsp;
	int ipdsp;

	index[0] = 0;
 
	for (i=0; i<res.numBoards; i++)
	{
		if (res.Board[i].totalDSP == numdsp)
		{
			confdsp = res.Board[i].confDSP;
			ivrdsp = res.Board[i].ivrDSP;
			ipdsp = res.Board[i].ipDSP;

			for (j=0; j<ipdsp; j++)
			{
				   sprintf(num,"%d ",j+confdsp+ivrdsp);
		           strcat(index,num);		
			}
		}
	}
    ret = (char *) malloc(sizeof(char) * strlen(index) +1);
    strcpy(ret,index);
	return ret;

}

char *getIPSize(int numdsp, Resource res)
{
	int i;
	char index[200];
	char *ret;
    int size;
    

	index[0] = 0;
 
	for (i=0; i<res.numBoards; i++)
	{
		if (res.Board[i].totalDSP == numdsp)
		{
			size = res.Board[i].ipDSP * res.ipPerDSP;
			sprintf(index,"%d",size);
		}
	}

    ret = (char *) malloc(sizeof(char) * strlen(index) +1);
    strcpy(ret,index);
	return ret;
}

int openConferenceFile(FILE **fp, char *filename)
{
    *fp = fopen(filename, "wt");

    if (*fp == NULL)
    {
        fprintf (stderr, "\nCould not open/create conference config file %s.\n", filename);
        return 0;
    }

	fprintf(*fp,"[CONFERENCING]\n\nFlags = NO_ECHO_CANCEL | NO_COACHING\n\n");
	return 1;

}

static int addToConferenceFile(FILE *fp,int board, int numdsp, Resource res)
{


  int i;
  int confdsp = 0;
  static int resource = 0;
  
    
	for (i=0; i<res.numBoards; i++)
	{
		if (res.Board[i].totalDSP == numdsp)
		{
			confdsp = res.Board[i].confDSP;
			break;
		}
	}

    for (i=0; i<confdsp; i++)
	{

        fprintf(fp,"[RESOURCE %d]\n\n",resource);
		fprintf(fp,"Board = %d\n",board);
		fprintf(fp,"NumTimeSlots = 32\n");
		fprintf(fp,"DSP = %d\n\n",i);
		resource++;
	}

	return 1;
}

static int addToInfoFile(FILE *fp,int board, int numdsp, Resource res, int chansTrunk)
{
  int i;
  int confdsp = 0;
  static int resource = 0;
      
	for (i=0; i<res.numBoards; i++)
	{
		if (res.Board[i].totalDSP == numdsp)
		{
            int trunks = res.Board[i].numTrunks;
            int numpstn = chansTrunk * trunks;
            int numip = res.Board[i].ipDSP * res.ipPerDSP;

            // The extra channel is for music on hold.
            int maxchans = res.Board[i].ivrDSP * res.ivrPerDSP - 1;

			int maxcalls = numpstn + numip;
            if (maxcalls > maxchans)
                maxcalls = maxchans;

            int numrecorders = maxchans - numpstn - numip;
            if (numrecorders < 0)
                numrecorders = 0;

            fprintf(fp, "NUMTRUNKS%d=%d\n"
                        "MAXCALLS%d=%d\n"
                        "NUMRECORDERS%d=%d\n",
                        board, trunks, 
                        board, maxcalls, 
                        board, numrecorders);
            
			break;
		}
	}

    return 1;
}

SectionConfig ipConfig = { 6, 3,
							{	"IPC.AddRoute[0].DestinationAddress      = %s\n",
								"IPC.AddRoute[0].Mask                    = %s\n",
								"IPC.AddRoute[0].Interface               = 1\n",
								"IPC.AddRoute[1].DestinationAddress      = 0.0.0.0\n",
								"IPC.AddRoute[1].Mask                    = 0.0.0.0\n",
								"IPC.AddRoute[1].GatewayAddress          = %s\n\n"
							}
};

SectionConfig clockConfig = { 5, 4,
							{	"Clocking.HBus.ClockMode                          = %s\n",
								"Clocking.HBus.ClockSource                        = %s\n",
								"Clocking.HBus.ClockSourceNetwork                 = %s\n",
								"Clocking.HBus.FallBackClockSource                = %s\n",
								"Clocking.HBus.AutoFallBack		                  = YES\n"

							}
};

SectionConfig T1CASConfig = { 13, 3,
							{	"#****************************\n",
								"#  T1 configuration\n",
								"#******************************\n",
								"DSPStream.VoiceIdleCode[0..15]                    = 0x7F\n",    
								"DSPStream.SignalIdleCode[0..15]                   = 0x00\n\n",                                                                                          
								"NetworkInterface.T1E1[0..15].Type                 = T1\n", 
								"NetworkInterface.T1E1[0..15].Impedance            = %s\n",
								"NetworkInterface.T1E1[0..15].LineCode             = %s\n",
								"NetworkInterface.T1E1[0..15].FrameType            = %s\n",
								"NetworkInterface.T1E1[0..15].SignalingType        = CAS\n\n",  
								"DSP.C5x[0..95].Libs                               = cg6klibu f_shared\n",
								"DSP.C5x[0..95].XLaw                               = MU_LAW\n",   
								"#**********************************************************************\n\n"
							}
};

// Set network interface to T1 even for IP only, existing code likes to iterate through trunks of channels
// (should probably remodel in this case as a single trunk with x channels)
SectionConfig IPOnlyConfig = { 7, 0,
							{	"########################################################\n",
								"DSPStream.VoiceIdleCode[0..15]                    = 0x7F\n",    
								"DSPStream.SignalIdleCode[0..15]                   = 0x00\n\n",                                                                                          
								"NetworkInterface.T1E1[0..15].Type                 = T1\n", 
								"DSP.C5x[0..95].Libs                               = cg6klibu f_shared\n",
								"DSP.C5x[0..95].XLaw                               = MU_LAW\n",   
								"##########################################################\n\n"
							}
};

SectionConfig T1ISDNConfig = { 17, 3,
							{	"#****************************\n",
								"#  T1 configuration\n",
								"#******************************\n\n",
								"Hdlc[0..15].Boot                      = YES\n",
								"Hdlc[0..15].RxTimeSlot				  = 23\n",
							    "Hdlc[0..15].TxTimeSlot				  = 23\n\n",
								"DSPStream.VoiceIdleCode[0..15]                    = 0x7F\n",    
								"DSPStream.SignalIdleCode[0..15]                   = 0x00\n\n",                                                                                          
								"NetworkInterface.T1E1[0..15].Type                 = T1\n", 
								"NetworkInterface.T1E1[0..15].Impedance            = %s\n",
								"NetworkInterface.T1E1[0..15].LineCode             = %s\n",
								"NetworkInterface.T1E1[0..15].FrameType            = %s\n",
								"NetworkInterface.T1E1[0..15].SignalingType        = PRI\n",
								"NetworkInterface.T1E1[0..15].D_Channel            = ISDN\n\n",
								"DSP.C5x[0..95].Libs                               = cg6klibu f_shared\n",
								"DSP.C5x[0..95].XLaw                               = MU_LAW\n",   
								"#**********************************************************************\n\n"
							}
};

SectionConfig E1CASConfig = { 13, 3,
							{	"#****************************\n",
								"#  E1 configuration\n",
								"#******************************\n",
								"DSPStream.VoiceIdleCode[0..15]                    = 0xD5\n",    
								"DSPStream.SignalIdleCode[0..15]                   = 0x09\n\n",                                                                                          
								"NetworkInterface.T1E1[0..15].Type                 = E1\n", 
								"NetworkInterface.T1E1[0..15].Impedance            = %s\n",
								"NetworkInterface.T1E1[0..15].LineCode             = %s\n",
								"NetworkInterface.T1E1[0..15].FrameType            = %s\n",
								"NetworkInterface.T1E1[0..15].SignalingType        = CAS\n\n",  
								"DSP.C5x[0..95].Libs                               = cg6kliba f_shared\n",
								"DSP.C5x[0..95].XLaw                               = A_LAW\n",   
								"#**********************************************************************\n\n"
							}
};

SectionConfig E1ISDNConfig = { 17, 3,
							{	"#****************************\n",
								"#  E1 configuration\n",
								"#******************************\n\n",
								"Hdlc[0..15].Boot                      = YES\n",
								"Hdlc[0..15].RxTimeSlot				  = 16\n",
							    "Hdlc[0..15].TxTimeSlot				  = 16\n\n",
								"DSPStream.VoiceIdleCode[0..15]                    = 0xD5\n",    
								"DSPStream.SignalIdleCode[0..15]                   = 0x09\n\n",                                                                                          
								"NetworkInterface.T1E1[0..15].Type                 = E1\n", 
								"NetworkInterface.T1E1[0..15].Impedance            = %s\n",
								"NetworkInterface.T1E1[0..15].LineCode             = %s\n",
								"NetworkInterface.T1E1[0..15].FrameType            = %s\n",
								"NetworkInterface.T1E1[0..15].SignalingType        = PRI\n",
								"NetworkInterface.T1E1[0..15].D_Channel            = ISDN\n\n",
								"DSP.C5x[0..95].Libs                           = cg6kliba f_shared\n",
								"DSP.C5x[0..95].XLaw                              = A_LAW\n",   
								"#**********************************************************************\n\n"
							}
};


SectionConfig IVRResourceConfig = { 12, 3,
								 {	"Resource[0].Name              = IVR_PSTN\n",
									"Resource[0].Size              = %s\n",
									"Resource[0].StartTimeslot     = 0\n",
									"Resource[0].TCPs              = %s nocc\n",
									"Resource[0].Definitions = \\ \n",
									"  ( dtmf.det_all &  \\ \n",
									"    ( \\ \n",
									"      ( callp.gnc & ptf.det_4f ) | \\ \n",
									"      ( echo.ln20_apt25 & ( rvoice.rec_mulaw | rvoice.play_mulaw ) ) \\ \n",
									"    ) \\ \n",
									"  ) \n",
									"Resource[0].Dsps = %s\n\n"
								  }
};

SectionConfig IPResourceConfig = { 10, 2,
						{	"Resource[1].Name              = IP\n",
							"Resource[1].Size              = %s\n",
							"Resource[1].StartTimeslot     = %s \n",
							"Resource[1].TCPs              = nocc\n",
							"Resource[1].Definitions = \\ \n",
							"        ( (f_g711.cod_rfc2833 & f_g711.dec_rfc2833) | \\ \n",
							"          (f_g726.cod_rfc2833 & f_g726.dec_rfc2833) | \\ \n",
							"	  (f_ilbc_20.cod_rfc2833 & f_ilbc_20.dec_rfc2833) \\ \n", 
							"        )\n",
							"Resource[1].Dsps =  %s\n\n",
						}
};

// add for 729
//  "          (f_g729a.cod_rfc2833 & f_g729a.dec_rfc2833) | \\ \n",

SectionConfig ConferenceResourceConfig = { 11, 7,
						{	"DSP.C5x[%s].NumTxTimeSlots  =   32\n",
							"DSP.C5x[%s].NumRxTimeSlots  =   32\n\n",
							"DSP.C5x[%s].CmdQStart    = 0xE800\n", 
							"DSP.C5x[%s].DataInQStart = 0xF800\n",
							"DSP.C5x[%s].DspOutQStart = 0xFB00\n",
							"DSP.C5x[%s].CmdQSize     = 0x110\n",
							"DSP.C5x[%s].Files = cg6conf\n\n",
							"ConferencingStream.Enable = YES\n\n",
							"SwitchConnectMode =   AllConstantDelay\n",
							"Echo.EnableExternalPins = YES\n",
							"Echo.AutoSwitchingRefSource = NO\n\n"
						}
};

SectionConfig DLMConfig = { 4, 1,
						{	"################################################################\n",
							"# DOWNLOADABLE RUNTIME MODULES\n",
							"DLMFiles[0]        = %s\n",
							"################################################################\n\n"
						}

};

SectionConfig DLMISDNConfig = { 6, 3,
						{	"################################################################\n",
							"# DOWNLOADABLE RUNTIME MODULES\n",
							"DLMFiles[0]        = %s\n",
							"DLMFiles[1]        = %s\n",
							"DLMFiles[2]        = %s\n",
							"################################################################\n\n"
						}

};


void printConfig(SectionConfig conf, char *vars[])
{
	int	i;
	int j=0;
	for	(i=0; i<conf.numlines; i++)
	{
		if	(strstr(conf.line[i],"%s"))
		{
			if(vars[j])
			{
				printf(conf.line[i],vars[j++]);
			}
			else
				printf(conf.line[i],"ERROR\n");
		}
		else
		{
			printf(conf.line[i]);
		}

	}

}


void fprintConfig(FILE *fp, SectionConfig conf, char *vars[])
{
	int	i;
	int j=0;
	for	(i=0; i<conf.numlines; i++)
	{
		if	(strstr(conf.line[i],"%s"))
		{
			if(vars[j])
			{
				fprintf(fp,conf.line[i],vars[j++]);
			}
			else
				fprintf(fp,conf.line[i],"ERROR\n");
		}
		else
		{
			fprintf(fp,conf.line[i]);
		}

	}

}
