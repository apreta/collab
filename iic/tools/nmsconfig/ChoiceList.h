#include <ctype.h>

typedef struct _choice {
	char Name[150];
	char Value[50];
}choice;

typedef struct _choicelist {
	int size;
	choice ch[100];
}choicelist;

choicelist DSPList={
	5,
	
	 {
		{"1100 mips","8"},
		{"1600 mips","12"},
		{"3200 mips","24"},
		{"4200 mips","32"},	
		{"6400 mips","48"}


	}
};
choicelist CasProtocolList={
	9,
	
	 {
		{"Wink start protocol","wnk0"},
		{"Ground start protocol","gds0"},
		{"Loop Start protocol","lps0"},
		{"Feature group D protocol","fgd0"},	
		{"Off-premises station protocol","ops0"},
		{"Multi-frequency R2 protocol","mfc0"},
		{"European Digital CAS protocol","euc0"},
		{"Australian P2 protocol","ap20"},
		{"NEC PBX protocol","nec0"}


	}
};

choicelist T1FrameTypeList={
	2,
	 {
		{"Standard superframe formatting (D4)","D4"},
		{"Extended superframe formatting (ESF)","ESF"}
	}
};

choicelist E1ImpedanceList={
	2,
	 {
		{"G.703-standard 75 ohm cables.","G703_75_OHM"},
		{"G.703-standard 120 ohm cables.","G703_120_OHM"}
	}
};

choicelist T1LineCodeList={
	6,
	 {
		{"Binary 8-zero code suppression (B8ZS)","B8ZS"},
		{"Alternate mark inversion (AMI)","AMI"},
		{"AMI with jammed bit 7 zero code suppression","AMI_ZCS"},
		{"AMI with jammed bit 7 zero code suppression Bell Ver.","AMI_BELL"},
		{"AMI with zero data byte replaced with 1001 1000","AMI_DDS"},
		{"AMI with jammed bit 8 zero code suppression","AMI_GTE"}
	}
};

choicelist E1LineCodeList={
	2,
	 {
		{"High density bipolar 3 code (HDB3)","HDB3"},
		{"Alternate mark inversion (AMI)","AMI"}
		
	}
};

choicelist T1E1List={
	2,
	 {
		{"T1 Trunks ","T1"},
		{"E1 Trunks","E1"}
		
	}
};

choicelist CasIsdnList={
	2,
	 {
		{"Channel associated signaling (CAS)","CAS"},
		{"Primary-rate ISDN","PRI"}
		
	}
};

choicelist IPOnlyList={
	2,
	 {
		{"The bridge will only support IP clients","YES"},
		{"The bridge will support both IP and PSTN clients","NO"}
		
	}
};

void printChoicelist(choicelist *list)
{
int i;

	for (i=0; i < list->size; i++)
	{
		printf("%2d - %s\n",i+1,list->ch[i].Name);

	}
    printf("\n>>");
}

int validchoice(char *num, int max)
{
	int len;
    int choice;
    len = (int)strlen(num);

	if (len > 2 || len ==0 ) // we don't support more then 99 choices
		return -1;
	if (!isdigit(num[0]))
		return -1;
	if (len == 2)
	{
		if (!isdigit(num[1]))
			return -1;
	
		choice = (num[0] - 48) * 10 + (num[1] - 48);
	}
	else
		choice = (num[0] - 48);

	if (choice == 0)
		return -1;

	if (choice > max)
		return -1;

	return choice;
}
char * getChoice(char *banner,choicelist *list)
{
 
	char input[100];
	int inputlength;
    int inputchoice;

	inputlength =0;
	inputchoice = -1;


	while (inputchoice < 0)
	{
		printf("%s\n",banner);
		printChoicelist(list);

		scanf("%s",input);
		printf("\n");
		fflush(stdin);	
		inputlength =(int) strlen(input);
		inputchoice = validchoice(input, list->size);
		if (inputchoice < 0)
			printf("\nInvalid choice: (%s).  Please try again.\n\n",input);

	}
	return list->ch[inputchoice-1].Value;
}
int isDigitString(char *str)
{
	int i=0;
	int value = 0;
	if (str!=NULL)
	{
	  for (i=0;i< (int)strlen(str);i++)
	  {
       if (!isdigit(str[i]))
		   break;
	  }
	}
	if(i==strlen(str))
	{
		value = atoi(str);

		if (value < 0 || value > 255)
		{
			return 0;
		}

		return 1;
	}
	else
		return 0;

}
int validateIPAdress(char *ip)
{

	char delims[] = ".";
	char *result = NULL;
	result = strtok( ip, delims );
	int i = 0;
    int num=0;
    int valid = 1;

	while( result != NULL) {
	
      if (!isDigitString(result))
	  {
		    valid = 0;
			break;
	  }
       result = strtok( NULL, delims );
     i++;
	}        
   if (valid && (i==4))
		return 1;
   else
		return 0;
}
char *getIPAddress(char *banner)
{
int valid =0;
char input[100];
char *ip;

while (!valid)
{
	printf("%s\n>> ",banner);
	scanf("%s",input);
	printf("\n");
	fflush(stdin);
	ip = (char *)malloc(sizeof(char)*strlen(input)+1);
	strcpy(ip,input);
    valid = validateIPAdress(input);
	if (!valid)
	{
			printf("\nInvalid IP Address: (%s).  Please try again.\n\n",ip);
		    free(ip);
	        ip = NULL;
	
	}
}
   
   return ip;
}
