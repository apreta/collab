/*****************************************************************************
* Module Name:
*     novellgen.cpp
*
*  Description:
*     
*     This utility is used to locate all NMS PCI hardware installed in
*     a system and generate a OAM configuration file
*     and board configuration files to boot the chassis with.
*
* Environment:
*   Requires the NmsPcInfo.dll or libNmsPcInfo.so to run (installed by CTAccess package)
*
* Copyright (c) 2007 NMS Communications Corporation. All rights reserved.
******************************************************************************/

#ifdef WIN32
#include <windows.h>
#include <conio.h>
#else
#include <unistd.h>
#ifndef LINUX
#include <signal.h>
#include "hsagfd.h"
#endif
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "nmstypes.h"
#include "ctadef.h"
#include "NmsPcInfo.h"
#include "ChoiceList.h"
#include "Config.h"


#define OAM_FILENAME     "/opt/nms/oam/cfg/oamsys.cfg"
#define CNF_FILENAME     "/opt/nms/ctaccess/cfg/cnf.cfg"
#define BRD_FILENAME     "/opt/nms/cg/cfg/Board%d.cfg"

/*
*  Static functions declaration
*/

static int  addConfigBoard (FILE* pFile, DeviceInfoStr deviceinfo);
int  createBoardConfig (FILE *fp,FILE *cf,FILE *infof, DeviceInfoStr deviceinfo, int board);
static FILE *openConfigFile (void);
static void closeConfigFile (FILE* pFile);
int openBoardConfig (char *filename, FILE **fp);

static struct
{
	char *oamProductName;   // OAM product name for this board
	char *isdndlm;			//DLM for isdn
	char *imgtdlm;			//DLM for imgt
	char *fusiondlm;		//DLM for Fusion


} g_BoardTypeTable[] =

{
	{"CG_6565C",                   "c6565igen", "c6565imgt", "cg6565fusion"},
	{"CG_6565C_8",                 "c6565igen", "c6565imgt", "cg6565fusion"},
	{"CG_6565C_16",                "c6565igen", "c6565imgt", "cg6565fusion"},
	{"CG_6565",                    "c6565igen", "c6565imgt", "cg6565fusion"},
	{"CG_6565_4",                  "c6565igen", "c6565imgt", "cg6565fusion"},
	{"CG_6565_8",                  "c6565igen", "c6565imgt", "cg6565fusion"},
	{"CG_6060",                    "c6060igen", "c6060imgt", "cg6060fusion"},
	{"CG_6060_4",                  "c6060igen", "c6060imgt", "cg6060fusion"},
	{"CG_6060_8",                  "c6060igen", "c6060imgt", "cg6060fusion"},
	{"CG_6060C",                   "c6060igen", "c6060imgt", "cg6060fusion"},
	{"CG_6060C_4",                 "c6060igen", "c6060imgt", "cg6060fusion"},
	{"CG_6060C_8",                 "c6060igen", "c6060imgt", "cg6060fusion"},
	{"CG_6060C_16",                "c6060igen", "c6060imgt", "cg6060fusion"},
	{"UNKNOWN",                    "c6060igen", "c6060imgt", "cg6060fusion"}
};

#define NMS_BOARD_TABLE_SIZE \
	(sizeof g_BoardTypeTable/sizeof g_BoardTypeTable[0])




//****************************************************************************
// Main function
//****************************************************************************
int main (int argc, char *argv[])
{
	cNmsPcInfo            cDetect;
	unsigned            nb_devices;
	int                ret = SUCCESS;
	DWORD                i;
	char boardFilename[50];
	FILE *bf, *cf;
    FILE *infof;

	setvbuf( stdout, NULL, _IONBF, 0 );

	printf ("\nNMS - Conferencing configuration file generator\n\n");
	
	// Initialize the object

	if ( (ret = cDetect.Initialize() ) != SUCCESS )
	{

		printf("\nError: PCI bios driver cannot be started (ret=0x%X)", ret);
		return ret;
	}

	// Retrieve all the NMS boards
	if ( (ret = cDetect.ScanAllPCIBus(NMS_VENDOR_ID, nb_devices) ) != SUCCESS)
	{
		char *string_error = NULL;
		string_error = (char *) cDetect.GetPBSError (ret);
		if( string_error != NULL)
			printf("Error: %s\n\n", string_error);
		else
			printf("\nError when scaning the PCI bus (ret=0x%X)", ret);
		cDetect.Release();
		return ret;
	}

	// Open configuration file
	FILE *pFile = openConfigFile();
	if (pFile==NULL)
	{
		// Release the Board detection
		cDetect.Release();
		return 0;
	}

	if (!openConferenceFile(&cf, CNF_FILENAME))
	{
		printf("Error creating conference config file.....exiting.\n");
		cDetect.Release();
		return 0;
	}

    if (!openBoardConfig("voiceinfo.sh", &infof))
    {
		printf("Error creating conference info file.....exiting.\n");
		cDetect.Release();
		return 0;        
    }
    
	/* Loop in the cNmsPcInfo structure to look for all the devices */
	for (i=0; i<nb_devices; i++)
	{
		DeviceInfoStr tmp_devinfo;
		ret = cDetect.GetDeviceInfo(i, &tmp_devinfo);

		if (ret==SUCCESS)
		{
			sprintf(boardFilename,BRD_FILENAME,i);
			ret = addConfigBoard (pFile, tmp_devinfo);
			if (ret >= 0)
			{

				openBoardConfig(boardFilename,&bf);

				if (bf == NULL)
				{
					printf("Error creating board config file.....exiting.\n");
					return 0;
				}
                if (!tmp_devinfo.b_isExtraInfoAvailable)
                {
                   	tmp_devinfo.NumDSPCores =atoi(getChoice("Couldn't tell how many DSPs are on the board!\n\nPlease answer the following question by choosing\na number on the left hand side of the available choices\n\nHow many MIPS does the detected board have:",&DSPList));
                }

				createBoardConfig(bf,cf,infof,tmp_devinfo,ret);
				fclose(bf);
			}
		}
	}

	// Release the Board detection
	cDetect.Release();

	// CLose configuration file
	closeConfigFile (pFile);
    closeConfigFile (infof);
    
	if (nb_devices>0)
	{
        printf("\n Configuration written for %d boards\n", nb_devices);
	}
	else
	{
		printf("\n No NMS boards have been found. Please install the boards and re-run the script.");
	}
	return 0;
}

/*---------------------------------------------------------------------------
*  createBoardConfig
*
*  Creates a board configuration file based on the number of DSPs and type of board
*  Returns the file pointer (NULL on error)
*--------------------------------------------------------------------------*/

int openBoardConfig (char *filename, FILE **fp)
{
	*fp = fopen(filename, "wt");

	if (*fp == NULL)
	{
		fprintf (stderr, "\nCould not open/create config file %s.\n", filename);
		return 0;
	}

	return 1;
}

/*---------------------------------------------------------------------------
*  openConfigFile
*
*  Opens a sample bootable OAM configuration file.
*  Returns the file pointer (NULL on error)
*--------------------------------------------------------------------------*/

static FILE *openConfigFile (void)
{
	FILE *pFile = fopen(OAM_FILENAME, "wt");

	if (pFile == NULL)
	{
		fprintf (stderr, "\nCould not open/create config file %s.\n", OAM_FILENAME);
		return NULL;
	}

	fprintf(pFile, "#---------------------------------------------------------\n");
	fprintf(pFile, "# " OAM_FILENAME " \n");
	fprintf(pFile, "#\n");
	fprintf(pFile, "# This system configuration file was created using nmsconfig");
	fprintf(pFile, "# based on the NMS boards found in this chassis.\n");
	fprintf(pFile, "#\n");
	fprintf(pFile, "# To boot the NMS boards in this chassis, use the command:\n");
	fprintf(pFile, "#      oamsys -f " OAM_FILENAME "\n");
	fprintf(pFile, "#\n");
	fprintf(pFile, "#---------------------------------------------------------\n");

	return pFile;

} // end openConfigFile

/*---------------------------------------------------------------------------
*  closeConfigFile
*
*  Closes the sample bootable OAM configuration file
*--------------------------------------------------------------------------*/
static void closeConfigFile (FILE* pFile)
{

	fclose(pFile);


} // end closeConfigFile

/*---------------------------------------------------------------------------
*  addConfigBoard
*
*  Adds a configuration file board entry to the sample OAM configuration file
*--------------------------------------------------------------------------*/
static int addConfigBoard (FILE* pFile, DeviceInfoStr deviceinfo)
{
	static int sequenceNumber = 0;
	int i;

	// Linear search the table
	for (i=0; i<NMS_BOARD_TABLE_SIZE -1; i++)
	{
		if (!strcmp(deviceinfo.Nmsboardtype, g_BoardTypeTable[i].oamProductName) )
		{
			break;
		}
	}
	if (i==NMS_BOARD_TABLE_SIZE-1)
		return -1;

	//
	// Board type has been determinated successfully
	//

	// First set up default condition


	printf("\n...Inserting board %s (%d:%d)\n", g_BoardTypeTable[i].oamProductName, deviceinfo.bus, deviceinfo.dev);  
	fprintf(pFile, "[Name%d]\n", sequenceNumber);
	if( deviceinfo.b_isExtraInfoAvailable )
	{
		fprintf(pFile, "\tProduct = %s\t", g_BoardTypeTable[i].oamProductName);
		fprintf(pFile, "# Board with %d DSPs, %d Switchs, %d Lines, %d Ethernets, %d CPUs",
			deviceinfo.NumDSPCores,
			deviceinfo.NumSwitch,
			deviceinfo.NumLine,
			deviceinfo.NumEthernet,
			deviceinfo.NumCPU
			);
		if( deviceinfo.NumDaughterCard )
		{
			fprintf(pFile, " and a daughterboard.\n");
		}
		else
		{
			fprintf(pFile, ".\n");
		}
		// Now if we are using the c6nocc.cfg file we may have to use a variant.


	} /* if extra info available */
	else
	{
		fprintf(pFile, "\tProduct = %s\n", g_BoardTypeTable[i].oamProductName);
	}
	fprintf(pFile, "\tNumber = %d\n", sequenceNumber);
	fprintf(pFile, "\tBus = %d\n", deviceinfo.bus);
	fprintf(pFile, "\tSlot = %d\n", deviceinfo.dev);
	fprintf(pFile, "\tFile = Board%d.cfg\n", sequenceNumber);

	sequenceNumber++;

	fprintf(pFile, "\n\n");

	return sequenceNumber-1;

} // end addConfigBoard

/*---------------------------------------------------------------------------
*  createBoardConfig
*
*  Creates a board configuration file based on the user's input.
*--------------------------------------------------------------------------*/
int createBoardConfig (FILE *fp,FILE *cf, FILE* infof, DeviceInfoStr deviceinfo, int board)

{

	char *IPOnly;
	char *T1E1;
	char *CASISDN;
	char *FrameType;
	char *LineCode;
	char *Protocol;
	char *Impedance;
	char *IPAddress;
	char *SubnetAddress;
	char *GatewayAddress;
	char *ConfIndex;
	char *IPResIndex;
	char *IPResSize;
	char *IVRResIndex;
	char *IVRResSize;
	char *vars[10];
	int i;

	for (i=0; i<NMS_BOARD_TABLE_SIZE -1; i++)
	{
		if (!strcmp(deviceinfo.Nmsboardtype, g_BoardTypeTable[i].oamProductName) )
		{
			break;
		}
	}

	IPAddress= getIPAddress("Enter the detetcted board's IP address:");
	SubnetAddress = getIPAddress("Enter the detected board's subnet address:");
	GatewayAddress = getIPAddress("Enter the detected board's gateway address:");

	printf("\nIn order to confgure the detected board please answer the\nfollowing questions using the numbers on the left hand side of the questions.\n\n"); 

	IPOnly = getChoice("Which best describes the conference bridge:",&IPOnlyList);

	vars[0] = IPAddress;
	vars[1] = SubnetAddress;
	vars[2] = GatewayAddress;
	fprintConfig(fp,ipConfig,vars);
	if (board == 0) //Board 0 will drive the H.100 bus
	{
		vars[0] = "MASTER_A";
		vars[1] = "NETWORK";
		vars[2] = "1";
		vars[3] = "OSC";
	}
	else			// All others will be slaves.
	{
		vars[0] = "SLAVE";
		vars[1] = "A_CLOCK";
		vars[2] = "1";
		vars[3] = "OSC";
	}
	fprintConfig(fp,clockConfig,vars);

	fprintf(infof, "IPADDR%d=%s\n", board, IPAddress);

	if (strcmp(IPOnly,"NO") == 0)
	{
		ConfIndex = genConfIndex(deviceinfo.NumDSPCores,IPTDMResource);
		IVRResIndex = genIVRIndex(deviceinfo.NumDSPCores,IPTDMResource);
		IVRResSize = getIVRSize(deviceinfo.NumDSPCores,IPTDMResource);
		IPResIndex = genIPIndex(deviceinfo.NumDSPCores,IPTDMResource);
		IPResSize = getIPSize(deviceinfo.NumDSPCores,IPTDMResource);
		addToConferenceFile(cf,board,deviceinfo.NumDSPCores,IPTDMResource);

		T1E1 = getChoice("What type of trunks are you using:",&T1E1List);
		CASISDN = getChoice("What type of signalling wil the board use:",&CasIsdnList);
		if(strcmp(CASISDN,"CAS") == 0)
		{
			if(strcmp(T1E1,"T1") == 0) //T1 CAS
			{
                addToInfoFile(infof,board,deviceinfo.NumDSPCores,IPTDMResource, 24);
                
				FrameType = getChoice("What is the trunk frame type:",&T1FrameTypeList);
				LineCode = getChoice("What is the line coding do the trunks use:",&T1LineCodeList);
				Impedance = "DSX1";
				Protocol = getChoice("What protocol are the trunks running:",&CasProtocolList);


				vars[0] = Impedance;
				vars[1] = LineCode;
				vars[2] = FrameType;
				fprintConfig(fp,T1CASConfig,vars);

				vars[0] = IVRResSize;
				vars[1] = Protocol;
				vars[2] = IVRResIndex;
				fprintConfig(fp,IVRResourceConfig,vars);

				vars[0] = IPResSize;
				vars[1] = "256";
				vars[2] = IPResIndex;
				fprintConfig(fp,IPResourceConfig,vars);


				vars[0] = ConfIndex;
				vars[1] = ConfIndex;
				vars[2] = ConfIndex;
				vars[3] = ConfIndex;
				vars[4] = ConfIndex;
				vars[5] = ConfIndex;
				vars[6] = ConfIndex;

				fprintConfig(fp,ConferenceResourceConfig, vars);

				vars[0] = g_BoardTypeTable[i].fusiondlm;
				fprintConfig(fp,DLMConfig,vars);

			}
			else //E1 CAS
			{
                addToInfoFile(infof,board,deviceinfo.NumDSPCores,IPTDMResource,30);
                
				FrameType = "CEPT";
				LineCode = getChoice("What is the line coding do the trunks use:",&E1LineCodeList);
				Impedance = getChoice("What is the impedance of the trunks:",&E1ImpedanceList);
				Protocol = getChoice("What protocol are the trunks running:",&CasProtocolList);

				vars[0] = Impedance;
				vars[1] = LineCode;
				vars[2] = FrameType;
				fprintConfig(fp,E1CASConfig,vars);

				vars[0] = IVRResSize;
				vars[1] = Protocol;
				vars[2] = IVRResIndex;
				fprintConfig(fp,IVRResourceConfig,vars);

				vars[0] = IPResSize;
				vars[1] = "256";
				vars[2] = IPResIndex;
				fprintConfig(fp,IPResourceConfig,vars);


				vars[0] = ConfIndex;
				vars[1] = ConfIndex;
				vars[2] = ConfIndex;
				vars[3] = ConfIndex;
				vars[4] = ConfIndex;
				vars[5] = ConfIndex;
				vars[6] = ConfIndex;

				fprintConfig(fp,ConferenceResourceConfig, vars);

				vars[0] = g_BoardTypeTable[i].fusiondlm;
				fprintConfig(fp,DLMConfig,vars);
			}
		}
		else  // ISDN
		{
			if(strcmp(T1E1,"T1") == 0) //T1 ISDN
			{
                addToInfoFile(infof,board,deviceinfo.NumDSPCores,IPTDMResource,23);

				FrameType = getChoice("What is the trunk frame type:",&T1FrameTypeList);
				LineCode = getChoice("What is the line coding do the trunks use:",&T1LineCodeList);
				Impedance = "DSX1";
				Protocol = "isd0";

				vars[0] = Impedance;
				vars[1] = LineCode;
				vars[2] = FrameType;
				fprintConfig(fp,T1ISDNConfig,vars);

				vars[0] = IVRResSize;
				vars[1] = Protocol;
				vars[2] = IVRResIndex;
				fprintConfig(fp,IVRResourceConfig,vars);

				vars[0] = IPResSize;
				vars[1] = "256";
				vars[2] = IPResIndex;
				fprintConfig(fp,IPResourceConfig,vars);


				vars[0] = ConfIndex;
				vars[1] = ConfIndex;
				vars[2] = ConfIndex;
				vars[3] = ConfIndex;
				vars[4] = ConfIndex;
				vars[5] = ConfIndex;
				vars[6] = ConfIndex;

				fprintConfig(fp,ConferenceResourceConfig, vars);

				vars[0] = g_BoardTypeTable[i].fusiondlm;
				vars[1] = g_BoardTypeTable[i].isdndlm;
				vars[2] = g_BoardTypeTable[i].imgtdlm;
				fprintConfig(fp,DLMISDNConfig,vars);
			}
			else //E1 ISDN
			{
                addToInfoFile(infof,board,deviceinfo.NumDSPCores,IPTDMResource, 30);
                
				FrameType = "CEPT";
				LineCode = getChoice("What is the line coding do the trunks use:",&E1LineCodeList);
				Impedance = getChoice("What is the impedance of the trunks:",&E1ImpedanceList);
				Protocol = "isd0";

				vars[0] = Impedance;
				vars[1] = LineCode;
				vars[2] = FrameType;
				fprintConfig(fp,E1ISDNConfig,vars);

				vars[0] = IVRResSize;
				vars[1] = Protocol;
				vars[2] = IVRResIndex;
				fprintConfig(fp,IVRResourceConfig,vars);

				vars[0] = IPResSize;
				vars[1] = "256";
				vars[2] = IPResIndex;
				fprintConfig(fp,IPResourceConfig,vars);


				vars[0] = ConfIndex;
				vars[1] = ConfIndex;
				vars[2] = ConfIndex;
				vars[3] = ConfIndex;
				vars[4] = ConfIndex;
				vars[5] = ConfIndex;
				vars[6] = ConfIndex;

				fprintConfig(fp,ConferenceResourceConfig, vars);

				vars[0] = g_BoardTypeTable[i].fusiondlm;
				vars[1] = g_BoardTypeTable[i].isdndlm;
				vars[2] = g_BoardTypeTable[i].imgtdlm;
				fprintConfig(fp,DLMISDNConfig,vars);
			}
		}
	}
	else //IP Only
	{
		ConfIndex = genConfIndex(deviceinfo.NumDSPCores,IPOnlyResource);
		IVRResIndex = genIVRIndex(deviceinfo.NumDSPCores,IPOnlyResource);
		IVRResSize = getIVRSize(deviceinfo.NumDSPCores,IPOnlyResource);
		IPResIndex = genIPIndex(deviceinfo.NumDSPCores,IPOnlyResource);
		IPResSize = getIPSize(deviceinfo.NumDSPCores,IPOnlyResource);
		addToConferenceFile(cf,board,deviceinfo.NumDSPCores,IPOnlyResource);

        addToInfoFile(infof,board,deviceinfo.NumDSPCores,IPOnlyResource,0);

		fprintConfig(fp,IPOnlyConfig,vars);

		vars[0] = IVRResSize;
		vars[1] = "";
		vars[2] = IVRResIndex;
		fprintConfig(fp,IVRResourceConfig,vars);

		vars[0] = IPResSize;
		vars[1] = "256";
		vars[2] = IPResIndex;
		fprintConfig(fp,IPResourceConfig,vars);

		vars[0] = ConfIndex;
		vars[1] = ConfIndex;
		vars[2] = ConfIndex;
		vars[3] = ConfIndex;
		vars[4] = ConfIndex;
		vars[5] = ConfIndex;
		vars[6] = ConfIndex;

		fprintConfig(fp,ConferenceResourceConfig, vars);

		vars[0] = g_BoardTypeTable[i].fusiondlm;
		fprintConfig(fp,DLMConfig,vars);

	}

    fprintf(infof, "LINEPROTOCOL%d=%s\n", board, Protocol ? Protocol : "nocc" );

	return 1;
}

