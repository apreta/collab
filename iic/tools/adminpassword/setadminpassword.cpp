
#include <stdio.h>
#include "../../jcomponent/util/log.h"
#include "../../services/db/sqldb.h"

int main(int argc, char * argv[])
{
    char sql[512];
    
    if (argc != 2)
    {
        printf("Usage: %s <new admin password>\n\n", argv[0]);
        return -1;
    }
    
    log_set_level(0);
    
	DBManager *db = sqldb_get_manager("/opt/iic/conf/sqldb.xml");
	
	Connection *conn = db->get_connection();
	
    sprintf(sql, "UPDATE USERS SET PASSWORD='%s' WHERE SCREENNAME='admin'", argv[1]);
    
	ResultSet *rs = conn->execute(sql);
	if (rs->is_error())
    {
		printf("Unable to set password: %s\n", rs->get_error());
        return -1;
    }

	db->close();
    
    printf("Password updated.\n");
    
    return 0;
}
