
script_load = '''
      <DoAction>
        <actions>
          <Dictionary>
            <strings>
              <String value="vars"/>
              <String value="_level%d"/>
              <String value="_currentframe"/>
              <String value="FSCommand:current_frames"/>
              <String value="%d"/>
              <String value="FSCommand:current_file"/>
              <String value="flash%d.swf"/>
              <String value="FSCommand:current_level"/>
              <String value="/_level%d"/>
              <String value="flash%d.swf"/>
              <String value="_level%d"/>
              <String value="checkLoad"/>
              <String value="setInterval"/>
              <String value="getBytesLoaded"/>
              <String value="_visible"/>
              <String value="sv"/>
              <String value="Sound"/>
              <String value="setVolume"/>
              <String value="getBytesTotal"/>
              <String value="fvars"/>
              <String value="clearInterval"/>
              <String value="stop"/>
              <String value="flash_empty.swf"/>
            </strings>
          </Dictionary>
          <DeclareFunction name="checkLoad" argc="1" length="272">
            <args>
              <String value="fvars"/>
            </args>
          </DeclareFunction>
          <PushData>
            <items>
              <StackInteger value="0"/>
              <StackDictionaryLookup index="10"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackDictionaryLookup index="13"/>
            </items>
          </PushData>
          <CallMethod/>
          <LogicalNOT/>
          <LogicalNOT/>
          <BranchIfTrue byteOffset="5"/>
          <PushData>
            <items>
              <StackNull/>
            </items>
          </PushData>
          <Return/>
          <PushData>
            <items>
              <StackDictionaryLookup index="10"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackDictionaryLookup index="14"/>
              <StackBoolean value="0"/>
            </items>
          </PushData>
          <SetMember/>
          <PushData>
            <items>
              <StackDictionaryLookup index="15"/>
              <StackDictionaryLookup index="10"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackInteger value="1"/>
              <StackDictionaryLookup index="16"/>
            </items>
          </PushData>
          <New/>
          <SetVariable/>
          <PushData>
            <items>
              <StackInteger value="0"/>
              <StackInteger value="1"/>
              <StackDictionaryLookup index="15"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackDictionaryLookup index="17"/>
            </items>
          </PushData>
          <CallMethod/>
          <Pop/>
          <PushData>
            <items>
              <StackDictionaryLookup index="15"/>
              <StackNull/>
            </items>
          </PushData>
          <SetVariable/>
          <PushData>
            <items>
              <StackInteger value="0"/>
              <StackDictionaryLookup index="10"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackDictionaryLookup index="13"/>
            </items>
          </PushData>
          <CallMethod/>
          <PushData>
            <items>
              <StackInteger value="0"/>
              <StackDictionaryLookup index="10"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackDictionaryLookup index="18"/>
            </items>
          </PushData>
          <CallMethod/>
          <LessThanTyped/>
          <LogicalNOT/>
          <BranchIfTrue byteOffset="5"/>
          <PushData>
            <items>
              <StackNull/>
            </items>
          </PushData>
          <Return/>
          <PushData>
            <items>
              <StackDictionaryLookup index="19"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackInteger value="0"/>
            </items>
          </PushData>
          <GetMember/>
          <PushData>
            <items>
              <StackInteger value="1"/>
              <StackDictionaryLookup index="20"/>
            </items>
          </PushData>
          <CallFunction/>
          <Pop/>
          <PushData>
            <items>
              <StackInteger value="0"/>
              <StackDictionaryLookup index="10"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackDictionaryLookup index="21"/>
            </items>
          </PushData>
          <CallMethod/>
          <Pop/>
          <PushData>
            <items>
              <StackDictionaryLookup index="15"/>
              <StackDictionaryLookup index="10"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackInteger value="1"/>
              <StackDictionaryLookup index="16"/>
            </items>
          </PushData>
          <New/>
          <SetVariable/>
          <PushData>
            <items>
              <StackInteger value="100"/>
              <StackInteger value="1"/>
              <StackDictionaryLookup index="15"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackDictionaryLookup index="17"/>
            </items>
          </PushData>
          <CallMethod/>
          <Pop/>
          <PushData>
            <items>
              <StackDictionaryLookup index="15"/>
              <StackNull/>
            </items>
          </PushData>
          <SetVariable/>
          <PushData>
            <items>
              <StackInteger value="0"/>
              <StackDictionaryLookup index="19"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackInteger value="2"/>
            </items>
          </PushData>
          <GetMember/>
          <LessThanTyped/>
          <LogicalNOT/>
          <BranchIfTrue byteOffset="11"/>
          <PushData>
            <items>
              <StackDictionaryLookup index="22"/>
              <StackDictionaryLookup index="1"/>
            </items>
          </PushData>
          <GetURL2 method="0"/>
          <PushData>
            <items>
              <StackDictionaryLookup index="0"/>
              <StackInteger value="0"/>
            </items>
          </PushData>
          <DeclareArray/>
          <SetVariable/>
          <PushData>
            <items>
              <StackDictionaryLookup index="0"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackInteger value="0"/>
              <StackInteger value="0"/>
            </items>
          </PushData>
          <SetMember/>
          <PushData>
            <items>
              <StackDictionaryLookup index="0"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackInteger value="1"/>
              <StackInteger value="0"/>
            </items>
          </PushData>
          <SetMember/>
          <PushData>
            <items>
              <StackDictionaryLookup index="0"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackInteger value="2"/>
              <StackInteger value="%d"/>  <!-- level -->
            </items>
          </PushData>
          <SetMember/>
          <PushData>
            <items>
              <StackInteger value="%d"/>  <!-- 30 secs played -->
              <StackDictionaryLookup index="1"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackDictionaryLookup index="2"/>
            </items>
          </PushData>
          <GetMember/>
          <LessThanTyped/>
          <Duplicate/>
          <BranchIfTrue byteOffset="25"/>
          <Pop/>
          <PushData>
            <items>
              <StackDictionaryLookup index="0"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackInteger value="2"/>
            </items>
          </PushData>
          <GetMember/>
          <PushData>
            <items>
              <StackInteger value="0"/>
            </items>
          </PushData>
          <EqualTyped/>
          <LogicalNOT/>
          <BranchIfTrue byteOffset="84"/>
          <PushData>
            <items>
              <StackDictionaryLookup index="3"/>
              <StackDictionaryLookup index="4"/>
            </items>
          </PushData>
          <GetURL2 method="0"/>
          <PushData>
            <items>
              <StackDictionaryLookup index="5"/>
              <StackDictionaryLookup index="6"/>
            </items>
          </PushData>
          <GetURL2 method="0"/>
          <PushData>
            <items>
              <StackDictionaryLookup index="7"/>
              <StackDictionaryLookup index="8"/>
            </items>
          </PushData>
          <GetURL2 method="0"/>
          <PushData>
            <items>
              <StackDictionaryLookup index="9"/>
              <StackDictionaryLookup index="10"/>
            </items>
          </PushData>
          <GetURL2 method="0"/>
          <PushData>
            <items>
              <StackDictionaryLookup index="0"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackInteger value="0"/>
              <StackDictionaryLookup index="0"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackInteger value="100"/>
              <StackDictionaryLookup index="11"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackInteger value="3"/>
              <StackDictionaryLookup index="12"/>
            </items>
          </PushData>
          <CallFunction/>
          <SetMember/>
          <EndAction/>
        </actions>
      </DoAction>
'''

script_play = '''
      <DoAction>
        <actions>
          <Dictionary>
            <strings>
              <String value="FSCommand:current_frames"/>
              <String value="%d"/>
              <String value="FSCommand:current_file"/>
              <String value="flash%d.swf"/>
              <String value="FSCommand:current_level"/>
              <String value="/_level%d"/>
              <String value="_level%d"/>
              <String value="_visible"/>
              <String value="_level%d"/>
              <String value="sv"/>
              <String value="Sound"/>
              <String value="setVolume"/>
              <String value="gotoAndPlay"/>
            </strings>
          </Dictionary>
          <PushData>
            <items>
              <StackDictionaryLookup index="0"/>
              <StackDictionaryLookup index="1"/>
            </items>
          </PushData>
          <GetURL2 method="0"/>
          <PushData>
            <items>
              <StackDictionaryLookup index="2"/>
              <StackDictionaryLookup index="3"/>
            </items>
          </PushData>
          <GetURL2 method="0"/>
          <PushData>
            <items>
              <StackDictionaryLookup index="4"/>
              <StackDictionaryLookup index="5"/>
            </items>
          </PushData>
          <GetURL2 method="0"/>
          <PushData>
            <items>
              <StackDictionaryLookup index="6"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackDictionaryLookup index="7"/>
              <StackBoolean value="1"/>
            </items>
          </PushData>
          <SetMember/>
          <PushData>
            <items>
              <StackDictionaryLookup index="8"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackDictionaryLookup index="7"/>
              <StackBoolean value="0"/>
            </items>
          </PushData>
          <SetMember/>
          <PushData>
            <items>
              <StackDictionaryLookup index="9"/>
              <StackDictionaryLookup index="6"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackInteger value="1"/>
              <StackDictionaryLookup index="10"/>
            </items>
          </PushData>
          <New/>
          <SetVariable/>
          <PushData>
            <items>
              <StackInteger value="100"/>
              <StackInteger value="1"/>
              <StackDictionaryLookup index="9"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackDictionaryLookup index="11"/>
            </items>
          </PushData>
          <CallMethod/>
          <Pop/>
          <PushData>
            <items>
              <StackDictionaryLookup index="9"/>
              <StackNull/>
            </items>
          </PushData>
          <SetVariable/>
          <PushData>
            <items>
              <StackInteger value="1"/>
              <StackInteger value="1"/>
              <StackDictionaryLookup index="6"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackDictionaryLookup index="12"/>
            </items>
          </PushData>
          <CallMethod/>
          <Pop/>
          <EndAction/>
        </actions>
      </DoAction>
'''

script_end = '''
      <DoAction>
        <actions>
          <Dictionary>
            <strings>
              <String value="_level%d"/>
              <String value="stop"/>
              <String value="flash_empty.swf"/>
            </strings>
          </Dictionary>
          <PushData>
            <items>
              <StackInteger value="0"/>
              <StackDictionaryLookup index="0"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackDictionaryLookup index="1"/>
            </items>
          </PushData>
          <CallMethod/>
          <Pop/>
          <PushData>
            <items>
              <StackDictionaryLookup index="2"/>
              <StackDictionaryLookup index="0"/>
            </items>
          </PushData>
          <GetURL2 method="0"/>
          <EndAction/>
        </actions>
      </DoAction>
'''