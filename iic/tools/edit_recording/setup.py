# Requires wxPython.  This sample demonstrates:
#
# - single file exe using wxPython as GUI.

from distutils.core import setup
import py2exe
import sys
from glob import glob

# If run without args, build executables, in quiet mode.
if len(sys.argv) == 1:
    sys.argv.append("py2exe")
    sys.argv.append("-q")

class Target:
    def __init__(self, **kw):
        self.__dict__.update(kw)
        # for the versioninfo resources
        self.version = "1.1"
        self.company_name = "Apreta"
        self.copyright = "(c) 2010 Apreta"
        self.name = "Edit recording"

################################################################
# A program using wxPython

# The manifest will be inserted as resource into test_wx.exe.  This
# gives the controls the Windows XP appearance (if run on XP ;-)
#
# Another option would be to store it in a file named
# test_wx.exe.manifest, and copy it with the data_files option into
# the dist-dir.
#
manifest_template = '''
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<assembly xmlns="urn:schemas-microsoft-com:asm.v1" manifestVersion="1.0">
<assemblyIdentity
    version="5.0.0.0"
    processorArchitecture="x86"
    name="%(prog)s"
    type="win32"
/>
<description>%(prog)s Program</description>
<dependency>
	<dependentAssembly>
	  <assemblyIdentity
			type="win32"
			name="Microsoft.VC90.CRT"
			version="9.0.21022.8"
			processorArchitecture="x86"
			publicKeyToken="1fc8b3b9a1e18e3b" />
	</dependentAssembly>
</dependency>
<dependency>
    <dependentAssembly>
        <assemblyIdentity
            type="win32"
            name="Microsoft.Windows.Common-Controls"
            version="6.0.0.0"
            processorArchitecture="X86"
            publicKeyToken="6595b64144ccf1df"
            language="*"
        />
    </dependentAssembly>
</dependency>
</assembly>
'''

RT_MANIFEST = 24

EditApp = Target(
    # used for the versioninfo resource
    description = "Edit Recording",

    # what to build
    script = "EditApp.py",
    other_resources = [(RT_MANIFEST, 1, manifest_template % dict(prog="EditApp"))],
    icon_resources = [(1, "EditApp.ico")],
    dest_base = "EditApp")

################################################################

data_files = [("Microsoft.VC90.CRT", glob(r'ms-vc-runtime\*.*'))]

setup(
	data_files = data_files,
    options = {"py2exe": {"compressed": 1,
                          "optimize": 2,
                          "ascii": 1,
                          "bundle_files": 1,
                          }},
    zipfile = "library.zip",
    windows = [EditApp],
    )
