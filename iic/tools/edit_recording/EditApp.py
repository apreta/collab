#!/usr/bin/env python
#Boa:PyApp:main

import wx
import EditFrame

modules ={'EditFrame': [0, '', 'EditFrame.py'],
          'FlashFile': [0, '', 'FlashFile.py']}

class EditApp(wx.App):
    def OnInit(self):
        wx.InitAllImageHandlers()
        self.main = EditFrame.create(None)
        self.main.Show()
        self.SetTopWindow(self.main)
        return True

def main():
    application = EditApp(0)
    application.MainLoop()

if __name__ == '__main__':
    main()
