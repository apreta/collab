
import thread
import posixpath
import urllib
import os
import SimpleHTTPServer
import BaseHTTPServer
import SocketServer

port = 8001

class RequestHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
    basepath = "."
    running = False
    
    def translate_path(self, path):
        path = posixpath.normpath(urllib.unquote(path))
        words = path.split('/')
        words = filter(None, words)
        path = RequestHandler.basepath
        for word in words:
            drive, word = os.path.splitdrive(word)
            head, word = os.path.split(word)
            if word in (os.curdir, os.pardir): continue
            path = os.path.join(path, word)
        return path

    def log_message(self, format, *args):
        pass

def serverThread():
    try:
        handlerClass = RequestHandler
        serverAddress = ('', port)
        httpd = BaseHTTPServer.HTTPServer(serverAddress, handlerClass)
        sa = httpd.socket.getsockname()
        RequestHandler.running = True
        httpd.serve_forever()
    except Exception, e:
        print "Server exception: " + str(e)   
    RequestHandler.running = False
    
def startServer(path):
    RequestHandler.basepath = path
    if not RequestHandler.running:
        thread.start_new_thread(serverThread, ())

import time
if __name__ == '__main__':
    startServer(".")
    time.sleep(10)
    
