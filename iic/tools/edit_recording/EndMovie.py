
script = '''
      <DoAction>
        <actions>
          <Dictionary>
            <strings>
              <String value="_level%d"/>
              <String value="stop"/>
              <String value="vars"/>
              <String value="FSCommand:current_frames"/>
              <String value="0"/>
              <String value="FSCommand:current_file"/>
              <String value="flash0.swf"/>
              <String value="FSCommand:current_level"/>
              <String value="/_level1"/>
              <String value="FSCommand:stop_play"/>
              <String value="1"/>
              <String value="_level1"/>
              <String value="checkLoad"/>
              <String value="setInterval"/>
              <String value="getBytesLoaded"/>
              <String value="fvars"/>
              <String value="clearInterval"/>
              <String value="_visible"/>
              <String value="gotoAndStop"/>
            </strings>
          </Dictionary>
          <DeclareFunction name="checkLoad" argc="1" length="93">
            <args>
              <String value="fvars"/>
            </args>
          </DeclareFunction>
          <PushData>
            <items>
              <StackInteger value="0"/>
              <StackDictionaryLookup index="11"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackDictionaryLookup index="14"/>
            </items>
          </PushData>
          <CallMethod/>
          <LogicalNOT/>
          <LogicalNOT/>
          <BranchIfTrue byteOffset="5"/>
          <PushData>
            <items>
              <StackNull/>
            </items>
          </PushData>
          <Return/>
          <PushData>
            <items>
              <StackDictionaryLookup index="15"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackInteger value="0"/>
            </items>
          </PushData>
          <GetMember/>
          <PushData>
            <items>
              <StackInteger value="1"/>
              <StackDictionaryLookup index="16"/>
            </items>
          </PushData>
          <CallFunction/>
          <Pop/>
          <PushData>
            <items>
              <StackDictionaryLookup index="11"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackDictionaryLookup index="17"/>
              <StackBoolean value="0"/>
            </items>
          </PushData>
          <SetMember/>
          <PushData>
            <items>
              <StackInteger value="1"/>
              <StackInteger value="1"/>
              <StackDictionaryLookup index="11"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackDictionaryLookup index="18"/>
            </items>
          </PushData>
          <CallMethod/>
          <Pop/>
          <PushData>
            <items>
              <StackInteger value="0"/>
              <StackDictionaryLookup index="0"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackDictionaryLookup index="1"/>
            </items>
          </PushData>
          <CallMethod/>
          <Pop/>
          <PushData>
            <items>
              <StackDictionaryLookup index="2"/>
              <StackInteger value="0"/>
            </items>
          </PushData>
          <DeclareArray/>
          <SetVariable/>
          <PushData>
            <items>
              <StackDictionaryLookup index="2"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackInteger value="0"/>
              <StackInteger value="0"/>
            </items>
          </PushData>
          <SetMember/>
          <PushData>
            <items>
              <StackDictionaryLookup index="2"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackInteger value="1"/>
              <StackInteger value="1"/>
            </items>
          </PushData>
          <SetMember/>
          <PushData>
            <items>
              <StackDictionaryLookup index="3"/>
              <StackDictionaryLookup index="4"/>
            </items>
          </PushData>
          <GetURL2 method="0"/>
          <PushData>
            <items>
              <StackDictionaryLookup index="5"/>
              <StackDictionaryLookup index="6"/>
            </items>
          </PushData>
          <GetURL2 method="0"/>
          <PushData>
            <items>
              <StackDictionaryLookup index="7"/>
              <StackDictionaryLookup index="8"/>
            </items>
          </PushData>
          <GetURL2 method="0"/>
          <PushData>
            <items>
              <StackDictionaryLookup index="9"/>
              <StackDictionaryLookup index="10"/>
            </items>
          </PushData>
          <GetURL2 method="0"/>
          <PushData>
            <items>
              <StackDictionaryLookup index="6"/>
              <StackDictionaryLookup index="11"/>
            </items>
          </PushData>
          <GetURL2 method="0"/>
          <PushData>
            <items>
              <StackDictionaryLookup index="2"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackInteger value="0"/>
              <StackDictionaryLookup index="2"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackInteger value="100"/>
              <StackDictionaryLookup index="12"/>
            </items>
          </PushData>
          <GetVariable/>
          <PushData>
            <items>
              <StackInteger value="3"/>
              <StackDictionaryLookup index="13"/>
            </items>
          </PushData>
          <CallFunction/>
          <SetMember/>
          <EndAction/>
        </actions>
      </DoAction>
      <DoAction>
        <actions>
          <Dictionary>
            <strings>
              <String value="flash_empty.swf"/>
              <String value="_level%d"/>
            </strings>
          </Dictionary>
          <PushData>
            <items>
              <StackDictionaryLookup index="0"/>
              <StackDictionaryLookup index="1"/>
            </items>
          </PushData>
          <GetURL2 method="0"/>
          <EndAction/>
        </actions>
      </DoAction>
'''
