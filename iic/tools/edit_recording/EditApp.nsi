; EditApp.nsi
;

;--------------------------------

; The name of the installer
Name "Recording Editor"

; The file to write
OutFile "RecordingEditor.exe"

; set compressor to best compression
SetCompressor lzma

; The default installation directory
InstallDir "$PROGRAMFILES\Apreta Conferencing"

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\Imidio_RecordingEditor" "Install_Dir"

; Select desired styles for our dialog
XPStyle on
WindowIcon off
ShowInstDetails nevershow
SpaceTexts none

BrandingText "Conferencing Installer"
;UninstallText "This will uninstall the Recording Editor. Hit Uninstall to continue."

;--------------------------------

; Pages

;Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

; The stuff to install
Section "Recording Editor (required)"

  SectionIn RO
  
  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  
  ; Copy files
  File "dist\EditApp.exe"
  File "swfmill.exe"
  ;File "dist\MSVCR71.dll"
  File /r "dist\Microsoft.VC90.CRT"
  File "dist\library.zip"
  File "EditApp.ico"
  File "ShowMovie.html"
  File "Recording Editor.html"
  
  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\Imidio_RecordingEditor "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\RecordingEditor" "DisplayName" "Conferencing Recording Editor"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\RecordingEditor" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\RecordingEditor" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\RecordingEditor" "NoRepair" 1
  WriteUninstaller "uninstall.exe"
  
SectionEnd

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

  CreateDirectory "$SMPROGRAMS\Recording Editor"
  CreateShortCut "$SMPROGRAMS\Recording Editor\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
  CreateShortCut "$SMPROGRAMS\Recording Editor\Recording Editor.lnk" "$INSTDIR\EditApp.exe" "" "$INSTDIR\EditApp.exe" 0
  CreateShortCut "$SMPROGRAMS\Recording Editor\Help.lnk" "$INSTDIR\Recording Editor.html" "" "$INSTDIR\Recording Editor.html" 0
  
SectionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\RecordingEditor"
  DeleteRegKey HKLM SOFTWARE\RecordingEditor

  ; Remove files and uninstaller
  Delete $INSTDIR\EditApp.exe
  Delete $INSTDIR\EditApp.ico
  Delete $INSTDIR\swfmill.exe
  ;Delete $INSTDIR\MSVCR71.dll
  Delete $INSTDIR\Microsoft.VC90.CRT\*.*
  Delete $INSTDIR\library.zip
  Delete $INSTDIR\ShowMovie.html
  Delete "$INSTDIR\Recording Editor.html"
  Delete $INSTDIR\uninstall.exe

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\Recording Editor\*.*"

  ; Remove directories used
  RMDir "$SMPROGRAMS\Recording Editor"
  RMDir "$INSTDIR\Microsoft.VC90.CRT"
  RMDir "$INSTDIR"

SectionEnd