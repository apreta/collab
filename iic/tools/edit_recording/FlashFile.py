
import sys
import os
import re
import glob
import shutil
import sys, string
import struct
import xml.sax 
import xml.sax.handler
import subprocess
import EndMovie
import LoadMovie


# Sax handler to remove frames between indicated start & stop points
# Outputs an XML doc that is the same as original except:
# - All graphic data is left in place, but ShowFrame elements are removed
#   between start and end frames.
# - Sound Stream data from the selected frames is removed.
# - Move initial script to new second frame if needed.
class ClipDocumentHandler(xml.sax.handler.ContentHandler):

    def __init__(self, startFrame, endFrame):
        self.startFrame = startFrame
        self.endFrame = endFrame
        self.level = 0
        self.contentData = []

        # if removing clips from start, start in clip mode
        if startFrame == 0:
            self.inClip = True
            self.moveScript = True
        else:
            self.inClip = False
            self.moveScript = False
        self.currentFrame = 0
        self.skipToLevel = 0
        self.bufferToLevel = 0
        self.buffer = ''

    def SetOutFile(self, outFile):
        self.outFile = outFile

    def startDocument(self):
        pass

    def endDocument(self):
        pass

    #  <frame data><showframe> 0
    #  <frame data><showframe> 1
    #  <frame data><showframe> 2
    # clip from 0 to 1 -- clip when frame >=0 and <=1

    def startElement(self, name, attrs):
        self.level += 1
        if name == "ShowFrame":
            if self.currentFrame >= self.startFrame and self.currentFrame <= self.endFrame:
                self.inClip = True
            else:
                self.inClip = False
            self.currentFrame += 1

        # if in section being clipped out, don't copy show frame or sound elements
        if self.inClip:
            if self.moveScript and name == 'DoAction':
                self.outFile.write("<!-- Moving script -->")
                self.bufferToLevel = self.level
            if name == 'ShowFrame':
                self.outFile.write("<!-- ShowFrame skipped -->")
                self.skipToLevel = self.level
            if name == 'SoundStreamBlock':
                self.outFile.write("<!-- SoundStreamBlock skipped -->")
                self.skipToLevel = self.level

        if self.bufferToLevel > 0:
            self.buffer += '<%s' % name
            for attrName in attrs.keys():
                attrValue = attrs.get(attrName)
                self.buffer += ' %s="%s"' % (attrName, attrValue)
            self.buffer += '>'            
        elif self.skipToLevel == 0:
            self.outFile.write('<%s' % name)
            for attrName in attrs.keys():
                attrValue = attrs.get(attrName)
                # fix total number of frames in file
                if name == "Header" and attrName == "frames":
                    attrValue = str(int(attrValue) - (self.endFrame - self.startFrame + 1))
                self.outFile.write(' %s="%s"' % (attrName, attrValue))                
            self.outFile.write('>')

    def endElement(self, name):
        if self.bufferToLevel > 0:
            self.buffer += '\n</%s>' % name
            if self.level <= self.bufferToLevel:
                self.bufferToLevel = 0
        elif self.skipToLevel > 0:
            if self.level <= self.skipToLevel:
                self.skipToLevel = 0
        else:
            self.outFile.write('\n')
            self.printLevel()
            self.outFile.write('</%s>' % name)
            if self.moveScript and name == 'ShowFrame':
                self.outFile.write(self.buffer)
                self.moveScript = False
        self.level -= 1

    def characters(self, content):
        if self.bufferToLevel > 0:
            self.buffer += content
        elif self.skipToLevel == 0:
            self.outFile.write(content)

    def printLevel(self):
        for idx in range(self.level):
            self.outFile.write('  ')

# Sax handler to replace script in movie
class ScriptDocumentHandler(xml.sax.handler.ContentHandler):

    def __init__(self, script, startScript, endScript):
        self.level = 0
        self.contentData = []
        self.currentScript = 0
        self.skipToLevel = 0

        self.script = script
        self.startScript = startScript
        self.endScript = endScript

    def SetOutFile(self, outFile):
        self.outFile = outFile

    def startDocument(self):
        pass

    def endDocument(self):
        pass

    def startElement(self, name, attrs):
        self.level += 1
        if name == "DoAction":
            self.currentScript += 1
            if self.currentScript >= self.startScript and self.currentScript <= self.endScript:
                self.outFile.write("<!-- Script replaced -->\n")
                self.outFile.write(self.script)
                self.script = '' # only replace one occurence
                self.skipToLevel = self.level

        if self.skipToLevel == 0:
            self.outFile.write('<%s' % name)
            for attrName in attrs.keys():
                attrValue = attrs.get(attrName)
                self.outFile.write(' %s="%s"' % (attrName, attrValue))                
            self.outFile.write('>')

    def endElement(self, name):
        if self.skipToLevel > 0:
            if self.level <= self.skipToLevel:
                self.skipToLevel = 0
        else:
            self.outFile.write('\n')
            self.printLevel()
            self.outFile.write('</%s>' % name)
        self.level -= 1

    def characters(self, content):
        if self.skipToLevel == 0:
            self.outFile.write(content)

    def printLevel(self):
        for idx in range(self.level):
            self.outFile.write('  ')


# Operate on one segment of recorded movie
class FlashFile:

    def __init__(self, filename):
        parts = os.path.split(filename)
        self.path, self.filename = parts
        self.tempname = self.filename

        parse = re.search("^([A-Za-z]+)(\d+).swf$", self.filename)
        if not parse:
            raise Exception, "Could not parse filename, does not appear to be a recording file."
        
        self.basename = parse.group(1)
        self.segment = parse.group(2)
        
    def _ParseXML(self, handler, inFileName, outFileName):
        outFile = file(outFileName, 'w')
        handler.SetOutFile(outFile)
        parser = xml.sax.make_parser()
        parser.setContentHandler(handler)
        inFile = file(inFileName, 'r')
        parser.parse(inFile)
        inFile.close()
        outFile.close()

    # copy source to temp file if needed
    def _MakeTemp(self):
        if self.filename == self.tempname:
            self.tempname = "tmp_" + self.filename
            # will overwrite any existing temp file
            shutil.copyfile(os.path.join(self.path, self.filename),
                            os.path.join(self.path, self.tempname))

    # convert swf to xml
    def _MakeXML(self, xmlname):
        res = subprocess.call('swfmill swf2xml "%s" "%s"' % (os.path.join(self.path, self.tempname),
                              xmlname))
        if res != 0:
            raise Exception, "Error converting SWF to XML"
        
    # convert xml back to swf
    def _MakeSWF(self, outname):
        res = subprocess.call('swfmill xml2swf "%s" "%s"' % (outname, 
                              os.path.join(self.path, self.tempname)))
        if res != 0:
            raise Exception, "Error converting XML to SWF"

    # remove frames from movie segment
    def Clip(self, startFrame, endFrame):
        self._MakeTemp()
            
        xmlname = os.path.join(self.path, self.tempname) + ".xml"
        outname = os.path.join(self.path, self.tempname) + ".out"
        
        self._MakeXML(xmlname)
        
        # parse XML, removing frames
        handler = ClipDocumentHandler(startFrame, endFrame)
        self._ParseXML(handler, xmlname, outname)
        
        self._MakeSWF(outname)

    # replace actionscript block(s) with new script
    def UpdateScript(self, script, startScript, endScript):
        self._MakeTemp()

        xmlname = os.path.join(self.path, self.tempname) + ".xml"
        outname = os.path.join(self.path, self.tempname) + ".out"
        
        self._MakeXML(xmlname)
        
        # parse XML, replacing script
        handler = ScriptDocumentHandler(script, startScript, endScript)
        self._ParseXML(handler, xmlname, outname)
        
        self._MakeSWF(outname)

    # save editted segment
    def Save(self):
        # remove original and rename temp to replace original
        if not self.tempname == self.filename:
            os.unlink(os.path.join(self.path, self.filename))
            os.rename(os.path.join(self.path, self.tempname), 
                      os.path.join(self.path, self.filename))
            self.tempname = self.filename

    # save as a different segment
    def SaveAs(self, segment):
        if not self.tempname == self.filename:
            copyname = os.path.join(self.path, self.basename) + str(segment) + ".swf"
            try:
                os.unlink(copyname)
            except:
                pass
            os.rename(os.path.join(self.path, self.tempname), copyname)
            self.tempname = self.filename

    # discard current changes to segment
    def Revert(self):
        if not self.tempname == self.filename:
            try:
                files = glob.glob(os.path.join(self.path, "tmp_" + self.filename + "*"))
                for file in files:
                    os.unlink(file)
            except:
                pass
            self.tempname = self.filename

    # returns original file name before first edit, then
    # temporary file name after
    def GetFileName(self):
        return os.path.join(self.path, self.tempname)

    # returns original file name
    def GetOrigFileName(self):
        return os.path.join(self.path, self.filename)

    # returns original file name for display purpose (no path)
    def GetDisplayFileName(self):
        return self.filename

    def GetCurrentSegment(self):
        return int(self.segment)
    
    # generate movie object based on this segment
    def GetMovie(self):
        return FlashMovie(self.path, self.basename)

    def GetPath(self):
        return self.path


# Operate on entire movie    
class FlashMovie:
    overhead = 6    # 6 frames added to end of movie segment

    def __init__(self, path, basename):
        self.path = path
        self.basename = basename
            
    # remove entire movie segment
    # All movie segments after removed one must be processed to 
    # update script actions.
    def Remove(self, segment):
        processed = 0
        self.segment = segment + 1
        name = os.path.join(self.path, "%s%d.swf" % (self.basename, self.segment))

        while os.path.exists(name):
            processed += 1
            file = FlashFile(name)

            # Rewrite scripts in each segment
            fileNum = self.segment-1
            level = self.segment
            script = LoadMovie.script_load % ( level-1, 0, fileNum, level, fileNum+1, level+1, level-1, 5 )
            file.UpdateScript(script, 1, 1)                

            script = LoadMovie.script_play % ( 0, fileNum+1, level+1, level+1, level )
            file.UpdateScript(script, 2, 2)                

            script = LoadMovie.script_end % ( level )
            file.UpdateScript(script, 3, 3)                

            file.SaveAs(self.segment - 1)
        
            self.segment += 1
            name = os.path.join(self.path, "%s%d.swf" % (self.basename, self.segment))
            
        # remove last, unneeded segment
        lastname = os.path.join(self.path, "%s%d.swf" % (self.basename, self.segment-1))
        os.unlink(lastname)

        # Re-write end movie script in last segment
        if self.segment > 1:
            self.segment = self.segment - 2
            name = os.path.join(self.path, "%s%d.swf" % (self.basename, self.segment))
            file = FlashFile(name)
            script = EndMovie.script % (self.segment+1, self.segment+1)
            file.UpdateScript(script, 2, 3)
            file.Save()

    # The default movie load script assumes more then 30 frames per segment.  If a segment 
    # has less then 30, the script in the next segment must be rewritten.
    def ReplaceLoadScript(self, name, segment):
        file = FlashFile(name)
        
        fileNum = segment
        level = segment+1
        script = LoadMovie.script_load % ( level-1, 0, fileNum, level, fileNum+1, level+1, level-1, 5 )
        file.UpdateScript(script, 1, 1)                
        file.Save()

    # Replace ShowMovie.html with alternate slider logic to handle segments of
    # differing length
    def Rebuild(self):
        totalFrames = 0
        lenInit = ""
        segment = 0
        name = os.path.join(self.path, "%s%d.swf" % (self.basename, segment))
        frames = 120

        while os.path.exists(name):
            if frames < 30:
                self.ReplaceLoadScript(name, segment)

            input = open(name, 'rb')

            # read movie header
            header = input.read(32)
            input.close()

            format_1 = "3sBLB"  # [magic][ver][filelen][rectsize]
            size_1 = 9
            header_1 = struct.unpack(format_1, header[:size_1])
            
            if header_1[0] != "FWS" and header_1[0] != "CWS":
                raise Exception, "Not a valid flash file."
            
            # rectsize is a sequence of packed bits, figure out size in bytes
            rectbits = (header_1[3]>>3) * 4 + 5
            rectsize = rectbits / 8
            
            format_2 = "HH"     # [frame rate][frame count]
            size_2 = 4
            header_2 = struct.unpack(format_2, header[size_1 + rectsize : size_1 + rectsize + size_2])
            
            segment += 1
            name = os.path.join(self.path, "%s%d.swf" % (self.basename, segment))
            moreSegments = os.path.exists(name)
            
            frames = header_2[1] - (self.overhead if moreSegments else 2)
            totalFrames += frames
            if len(lenInit):
                lenInit += ","
            lenInit += str(frames)

        if totalFrames > 0:
            input = file("ShowMovie.html")
            template = input.read()
            input.close()
            
            out1 = re.sub(r"##LENS##", lenInit, template)
            out2 = re.sub(r"##TOTAL##", str(totalFrames), out1)
            
            output = open(os.path.join(self.path, "ShowMovie.html"), 'w')
            output.write(out2)
            output.close()

if __name__ == '__main__':
    test = FlashMovie("C:\\Temp\\243604-2006-03-01-16-25-21\\", "flash")
    test.Remove(2)
