#Boa:Frame:EditFrame

# ToDo: port to current flashin.FlashWindow class

import os
import wx
import wx.grid
from wx.lib.anchors import LayoutAnchors
if wx.Platform == '__WXMSW__':
    from wx.lib.flashwin_old import FlashWindow
    
import FlashFile
import EditServer
    
def create(parent):
    return EditFrame(parent)

[wxID_EDITFRAME, wxID_EDITFRAMECLIPEND, wxID_EDITFRAMECLIPSTART, 
 wxID_EDITFRAMEDELETESELECTED, wxID_EDITFRAMEENDFRAME, wxID_EDITFRAMEPANEL1, 
 wxID_EDITFRAMEPLAYBUTTON, wxID_EDITFRAMEREBUILDMOVIE, 
 wxID_EDITFRAMEREMOVESEGMENT, wxID_EDITFRAMEREVERTCLIP, wxID_EDITFRAMESLIDER, 
 wxID_EDITFRAMESTARTFRAME, wxID_EDITFRAMESTATICTEXT1, 
 wxID_EDITFRAMESTATICTEXT2, wxID_EDITFRAMESTOPBUTTON, 
] = [wx.NewId() for _init_ctrls in range(15)]

[wxID_EDITFRAMEFILEMENUEXIT, wxID_EDITFRAMEFILEMENUOPEN, 
 wxID_EDITFRAMEFILEMENUSAVE, wxID_EDITFRAMEFILEMENUTEST, 
] = [wx.NewId() for _init_coll_filemenu_Items in range(4)]


# Edit Recording main window
[wxID_EDITFRAMEHELPMENUHELP, wxID_EDITFRAMEHELPMENUHELPABOUT, 
] = [wx.NewId() for _init_coll_helpmenu_Items in range(2)]

[wxID_EDITFRAMETIMER] = [wx.NewId() for _init_utils in range(1)]

class EditFrame(wx.Frame):
    overhead = 6    # 6 frames added to end of movie segment
    length = 120
    dirty = False
    
    def _init_coll_menuBar1_Menus(self, parent):
        # generated method, don't edit

        parent.Append(menu=self.filemenu, title='File')
        parent.Append(menu=self.helpmenu, title='Help')

    def _init_coll_helpmenu_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_EDITFRAMEHELPMENUHELP,
              kind=wx.ITEM_NORMAL, text='Help')
        parent.Append(help='', id=wxID_EDITFRAMEHELPMENUHELPABOUT,
              kind=wx.ITEM_NORMAL, text='About...')
        self.Bind(wx.EVT_MENU, self.OnHelpmenuHelpaboutMenu,
              id=wxID_EDITFRAMEHELPMENUHELPABOUT)
        self.Bind(wx.EVT_MENU, self.OnHelpmenuHelpMenu,
              id=wxID_EDITFRAMEHELPMENUHELP)

    def _init_coll_filemenu_Items(self, parent):
        # generated method, don't edit

        parent.Append(help='', id=wxID_EDITFRAMEFILEMENUOPEN,
              kind=wx.ITEM_NORMAL, text='Open Segment...')
        parent.Append(help='', id=wxID_EDITFRAMEFILEMENUSAVE,
              kind=wx.ITEM_NORMAL, text='Save Segment')
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_EDITFRAMEFILEMENUTEST,
              kind=wx.ITEM_NORMAL, text='Test Movie in Browser')
        parent.AppendSeparator()
        parent.Append(help='', id=wxID_EDITFRAMEFILEMENUEXIT,
              kind=wx.ITEM_NORMAL, text='Exit')
        self.Bind(wx.EVT_MENU, self.OnFilemenuOpenMenu,
              id=wxID_EDITFRAMEFILEMENUOPEN)
        self.Bind(wx.EVT_MENU, self.OnFilemenuExitMenu,
              id=wxID_EDITFRAMEFILEMENUEXIT)
        self.Bind(wx.EVT_MENU, self.OnFilemenuSaveMenu,
              id=wxID_EDITFRAMEFILEMENUSAVE)
        self.Bind(wx.EVT_MENU, self.OnFilemenuTestMenu,
              id=wxID_EDITFRAMEFILEMENUTEST)

    def _init_utils(self):
        # generated method, don't edit
        self.menuBar1 = wx.MenuBar()

        self.filemenu = wx.Menu(title='')

        self.timer = wx.Timer(id=wxID_EDITFRAMETIMER, owner=self)
        self.Bind(wx.EVT_TIMER, self.OnTimerTimer, id=wxID_EDITFRAMETIMER)

        self.helpmenu = wx.Menu(title='')

        self._init_coll_menuBar1_Menus(self.menuBar1)
        self._init_coll_filemenu_Items(self.filemenu)
        self._init_coll_helpmenu_Items(self.helpmenu)

    def _init_ctrls(self, prnt):
        # generated method, don't edit
        wx.Frame.__init__(self, id=wxID_EDITFRAME, name='', parent=prnt,
              pos=wx.DefaultPosition, size=wx.Size(720, 671),
              style=wx.DEFAULT_FRAME_STYLE, title='Edit Recording')
        self._init_utils()
        self.SetClientSize(wx.Size(712, 637))
        self.SetAutoLayout(True)
        self.SetMenuBar(self.menuBar1)
        self.SetMinSize(wx.Size(200, 300))
        self.SetIcon(wx.Icon('EditApp.ico', wx.BITMAP_TYPE_ICO))
        self.Bind(wx.EVT_SIZE, self.OnEditFrameSize)
        self.Bind(wx.EVT_CLOSE, self.OnEditFrameClose)

        self.panel1 = wx.Panel(id=wxID_EDITFRAMEPANEL1, name='panel1',
              parent=self, pos=wx.Point(0, -16), size=wx.Size(720, 651),
              style=wx.TAB_TRAVERSAL)
        self.panel1.SetConstraints(LayoutAnchors(self.panel1, True, True, True,
              True))

        self.slider = wx.Slider(id=wxID_EDITFRAMESLIDER, maxValue=119,
              minValue=0, name='slider', parent=self.panel1, point=wx.Point(0,
              16), size=wx.Size(705, 60),
              style=wx.SL_HORIZONTAL|wx.SL_AUTOTICKS|wx.SL_LABELS, value=0)
        self.slider.SetConstraints(LayoutAnchors(self.slider, True, True, True,
              False))
        self.slider.Bind(wx.EVT_SCROLL, self.OnSliderScroll)

        self.PlayButton = wx.Button(id=wxID_EDITFRAMEPLAYBUTTON, label='Play',
              name='PlayButton', parent=self.panel1, pos=wx.Point(368, 518),
              size=wx.Size(75, 23), style=0)
        self.PlayButton.SetConstraints(LayoutAnchors(self.PlayButton, True,
              False, False, True))
        self.PlayButton.Bind(wx.EVT_BUTTON, self.OnPlayButtonButton,
              id=wxID_EDITFRAMEPLAYBUTTON)

        self.StopButton = wx.Button(id=wxID_EDITFRAMESTOPBUTTON, label='Stop',
              name='StopButton', parent=self.panel1, pos=wx.Point(456, 518),
              size=wx.Size(75, 23), style=0)
        self.StopButton.SetConstraints(LayoutAnchors(self.StopButton, True,
              False, False, True))
        self.StopButton.Bind(wx.EVT_BUTTON, self.OnStopButtonButton,
              id=wxID_EDITFRAMESTOPBUTTON)

        self.ClipEnd = wx.Button(id=wxID_EDITFRAMECLIPEND,
              label='Set From Current', name='ClipEnd', parent=self.panel1,
              pos=wx.Point(200, 550), size=wx.Size(104, 23), style=0)
        self.ClipEnd.SetConstraints(LayoutAnchors(self.ClipEnd, True, False,
              False, True))
        self.ClipEnd.Bind(wx.EVT_BUTTON, self.OnClipEndButton,
              id=wxID_EDITFRAMECLIPEND)

        self.staticText1 = wx.StaticText(id=wxID_EDITFRAMESTATICTEXT1,
              label='Start Frame:', name='staticText1', parent=self.panel1,
              pos=wx.Point(16, 523), size=wx.Size(64, 13), style=0)
        self.staticText1.SetConstraints(LayoutAnchors(self.staticText1, True,
              False, False, True))

        self.StartFrame = wx.TextCtrl(id=wxID_EDITFRAMESTARTFRAME,
              name='StartFrame', parent=self.panel1, pos=wx.Point(88, 519),
              size=wx.Size(100, 21), style=0, value='')
        self.StartFrame.SetConstraints(LayoutAnchors(self.StartFrame, True,
              False, False, True))

        self.staticText2 = wx.StaticText(id=wxID_EDITFRAMESTATICTEXT2,
              label='End Frame:', name='staticText2', parent=self.panel1,
              pos=wx.Point(16, 555), size=wx.Size(64, 13), style=0)
        self.staticText2.SetConstraints(LayoutAnchors(self.staticText2, True,
              False, False, True))

        self.EndFrame = wx.TextCtrl(id=wxID_EDITFRAMEENDFRAME, name='EndFrame',
              parent=self.panel1, pos=wx.Point(88, 551), size=wx.Size(100, 21),
              style=0, value='')
        self.EndFrame.SetConstraints(LayoutAnchors(self.EndFrame, True, False,
              False, True))

        self.ClipStart = wx.Button(id=wxID_EDITFRAMECLIPSTART,
              label='Set From Current', name='ClipStart', parent=self.panel1,
              pos=wx.Point(200, 518), size=wx.Size(104, 23), style=0)
        self.ClipStart.SetConstraints(LayoutAnchors(self.ClipStart, True, False,
              False, True))
        self.ClipStart.Bind(wx.EVT_BUTTON, self.OnClipStartButton,
              id=wxID_EDITFRAMECLIPSTART)

        self.DeleteSelected = wx.Button(id=wxID_EDITFRAMEDELETESELECTED,
              label='Delete Selected Clip', name='DeleteSelected',
              parent=self.panel1, pos=wx.Point(16, 588), size=wx.Size(112, 23),
              style=0)
        self.DeleteSelected.SetConstraints(LayoutAnchors(self.DeleteSelected,
              True, False, False, True))
        self.DeleteSelected.Bind(wx.EVT_BUTTON, self.OnDeleteSelectedButton,
              id=wxID_EDITFRAMEDELETESELECTED)

        self.RevertClip = wx.Button(id=wxID_EDITFRAMEREVERTCLIP,
              label='Undo Delete Clip', name='RevertClip', parent=self.panel1,
              pos=wx.Point(144, 588), size=wx.Size(112, 23), style=0)
        self.RevertClip.SetConstraints(LayoutAnchors(self.RevertClip, True,
              False, False, True))
        self.RevertClip.Bind(wx.EVT_BUTTON, self.OnRevertClipButton,
              id=wxID_EDITFRAMEREVERTCLIP)

        self.RemoveSegment = wx.Button(id=wxID_EDITFRAMEREMOVESEGMENT,
              label='Remove Entire Segment', name='RemoveSegment',
              parent=self.panel1, pos=wx.Point(368, 588), size=wx.Size(136, 23),
              style=0)
        self.RemoveSegment.SetConstraints(LayoutAnchors(self.RemoveSegment,
              True, False, False, True))
        self.RemoveSegment.Bind(wx.EVT_BUTTON, self.OnRemoveSegmentButton,
              id=wxID_EDITFRAMEREMOVESEGMENT)

        self.RebuildMovie = wx.Button(id=wxID_EDITFRAMEREBUILDMOVIE,
              label='Rebuild Movie Viewer', name='RebuildMovie',
              parent=self.panel1, pos=wx.Point(520, 588), size=wx.Size(136, 23),
              style=0)
        self.RebuildMovie.SetConstraints(LayoutAnchors(self.RebuildMovie, True,
              False, False, True))
        self.RebuildMovie.Bind(wx.EVT_BUTTON, self.OnRebuildMovieButton,
              id=wxID_EDITFRAMEREBUILDMOVIE)

    def __init__(self, parent):
        self._init_ctrls(parent)

        # Flash window is not auto-generated
        self.flash = FlashWindow(self, style=wx.SUNKEN_BORDER,
                      pos=wx.Point(5, 65), size=wx.Size(700, 425))
        self.flash.SetConstraints(LayoutAnchors(self.flash, True, True, True,
              True))
        self.flash.Bind(wx.lib.flashwin_old.EVT_ReadyStateChange, self.OnReadyStateChange)
        
        self.timer.Start(500)
        self.file = None

    def NewFlashWindow(self):
        self.flash.Destroy()
        self.flash = FlashWindow(self, style=wx.SUNKEN_BORDER,
                      pos=wx.Point(5, 65), size=wx.Size(700, 425))
        self.flash.SetConstraints(LayoutAnchors(self.flash, True, True, True,
              True))
        self.flash.Bind(wx.lib.flashwin_old.EVT_ReadyStateChange, self.OnReadyStateChange)
        self.SetTitle("Edit Recording")

    def OnPlayButtonButton(self, event):
        self.flash.Play()

    def OnStopButtonButton(self, event):
        self.flash.Stop()

    def _OpenFlash(self):
        wx.BeginBusyCursor()
        self.NewFlashWindow()
        self.length = 0
        self.flash.LoadMovie(0, 'file://' + self.file.GetFileName())
        wx.EndBusyCursor()

    def OnFilemenuOpenMenu(self, event):
        if self.dirty:
            dlg = wx.MessageDialog(self, 'Current movie has pending edits, save now?',
                                   'Save Changes?',
                                   wx.YES_NO|wx.CANCEL
                                   )
            result = dlg.ShowModal()
            dlg.Destroy()
            
            if result == wx.ID_YES:
                self.file.Save()
                self.dirty = False
            if result == wx.ID_CANCEL:
                return

        dlg = wx.FileDialog(self, wildcard="*.swf")

        if dlg.ShowModal() == wx.ID_OK:
            try:
                self.file = FlashFile.FlashFile(dlg.GetPath())
                self.dirty = False
                self._OpenFlash()
            except Exception, e:
                errdlg = wx.MessageDialog(self, "Error opening file: " + str(e),
                               'File Error',
                               wx.OK | wx.ICON_ERROR
                               )
                errdlg.ShowModal()
                errdlg.Destroy()

        dlg.Destroy()
        
    def OnFilemenuSaveMenu(self, event):
        if self.file:
            wx.BeginBusyCursor()
            self.file.Save()
            wx.EndBusyCursor()
        
            self.dirty = False
            self._OpenFlash()
            self.flash.Stop()
        
    def OnFilemenuTestMenu(self, event):
        if self.file:
            self.flash.Stop()
            EditServer.startServer(self.file.GetPath())
            os.startfile("http://localhost:%d/ShowMovie.html" % EditServer.port);

    def OnFilemenuExitMenu(self, event):
        self.Close()

    def OnReadyStateChange(self, event):
        wx.MessageDialog(self, "Event: %d" % event.newState);
        state = event.newState
        if state >= 2:
            try:
                self.length = self.flash.totalframes - self.overhead
                self.slider.SetRange(0, self.length - 1)
                self.SetTitle("Edit Recording [%s]" % self.file.GetDisplayFileName())
            except:
                errdlg = wx.MessageDialog(self, "Unable to read Flash file.",
                               'File Error',
                               wx.OK | wx.ICON_ERROR
                               )
                errdlg.ShowModal()
                errdlg.Destroy()
                self.length = 0
    
    def OnTimerTimer(self, event):
        # Sync slider with movie position
        pos = self.flash.CurrentFrame()
        if pos < 0:
            pos = 0

        if pos >= self.length:
            self.flash.Stop()
            pos = self.length - 1

        self.slider.SetValue(pos)

    def OnSliderScroll(self, event):
        # Sync movie with slider position
        wasPlaying = self.flash.IsPlaying()
        pos = self.slider.GetValue()
        self.flash.GotoFrame(pos)
        if wasPlaying:
            self.flash.Play()

    def OnEditFrameSize(self, event):
        # Fix repainting problem when flash window is resized
        self.Refresh(True)
        event.Skip()

    # Delete selected clip from movie segment
    def OnDeleteSelectedButton(self, event):
        if self.file == None:
            return

        startFrame = int(self.StartFrame.GetValue())
        endFrame = int(self.EndFrame.GetValue())

        # If deleting end of movie segment, leave an extra 2 seconds
        # for overlap between segments
        #if endFrame >= self.length:
        #    startFrame += 2
        wx.BeginBusyCursor()
        self.file.Clip(startFrame, endFrame)
        wx.EndBusyCursor()

        self.dirty = True
        self._OpenFlash()

    # Undo last delete clip
    def OnRevertClipButton(self, event):
        if self.file == None:
            return

        # Return to original movie segment
        wx.BeginBusyCursor()
        self.file.Revert()
        wx.EndBusyCursor()

        self.dirty = False
        self._OpenFlash()

    # Remove entire segment from movie sequence
    def OnRemoveSegmentButton(self, event):
        if self.file == None:
            dlg = wx.MessageDialog(self, 'No movie loaded.  Open segment to remove from movie.',
                                   'Error',
                                   wx.OK | wx.ICON_ERROR
                                   )
            result = dlg.ShowModal()
            dlg.Destroy()
            return

        movie = self.file.GetMovie()
        segment = self.file.GetCurrentSegment()
        wx.BeginBusyCursor()
        try:
            movie.Remove(segment)
            wx.EndBusyCursor()
        except Exception, e:
            wx.EndBusyCursor()
            errdlg = wx.MessageDialog(self, "Error removing movie segment: " + str(e),
                           'Error',
                           wx.OK | wx.ICON_ERROR
                           )
            errdlg.ShowModal()
            errdlg.Destroy()
            
        self.dirty = False
        
        # recreate flash window
        self.NewFlashWindow()
        self.file = None
    
    def OnRebuildMovieButton(self, event):
        if self.dirty:
            dlg = wx.MessageDialog(self, 'Current movie has pending edits, save now?',
                                   'Save Changes?',
                                   wx.YES_NO|wx.CANCEL
                                   )
            result = dlg.ShowModal()
            dlg.Destroy()
            
            if result == wx.ID_YES:
                self.file.Save()
                self.dirty = False
            if result == wx.ID_CANCEL:
                return

        if self.file == None:
            dlg = wx.MessageDialog(self, 'No movie loaded.  Open segment before rebuilding movie.',
                                   'Error',
                                   wx.OK | wx.ICON_ERROR
                                   )
            result = dlg.ShowModal()
            dlg.Destroy()
            return

        movie = self.file.GetMovie()
        wx.BeginBusyCursor()
        try:
            movie.Rebuild()
            wx.Sleep(2);
            wx.EndBusyCursor()
        except Exception, e:
            wx.EndBusyCursor()
            errdlg = wx.MessageDialog(self, "Error rebuilding movie viewer page: " + str(e),
                           'Error',
                           wx.OK | wx.ICON_ERROR
                           )
            errdlg.ShowModal()
            errdlg.Destroy()

    def OnClipStartButton(self, event):
        self.StartFrame.SetValue(`self.slider.GetValue()`)

    def OnClipEndButton(self, event):
        self.EndFrame.SetValue(`self.slider.GetValue()`)

    def OnHelpmenuHelpaboutMenu(self, event):
        dlg = wx.MessageDialog(self, '\nMeeting recording editor 1.1\n\n(c)2006 SiteScape\n(c)2010 Apreta',
                               'Edit Recording',
                               wx.OK
                               )
        dlg.ShowModal()
        dlg.Destroy()

    def OnHelpmenuHelpMenu(self, event):
        os.startfile("Recording Editor.html")
        
    def OnEditFrameClose(self, event):
        if self.dirty:
            dlg = wx.MessageDialog(self, 'Current movie has pending edits, save now?',
                                   'Save Changes?',
                                   wx.YES_NO|wx.CANCEL
                                   )
            result = dlg.ShowModal()
            dlg.Destroy()
            
            if result == wx.ID_YES:
                self.file.Save()
                self.dirty = False
            if result != wx.ID_CANCEL:
                self.Destroy()
        else:
            self.Destroy()


if __name__ == '__main__':
    app = wx.PySimpleApp()
    wx.InitAllImageHandlers()
    frame = create(None)
    frame.Show()

    app.MainLoop()
