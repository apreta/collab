EnterAcceptMeeting.wav	Please press 1 to join the meeting.
EnterDialNumber.wav	Please enter the number to dial followed by the pound key.
EnterMeetingPin.wav	Please enter the meeting pin number followed by the pound key.
EnterPassword.wav	Please enter the meeting password followed by the pound key.
Goodbye.wav		Goodbye
Greeting.wav		Welcome to the conference center.
HoldForModerator.wav	"Please hold, your meeting will start when a host arrives."
InvalidPassword.wav	"I'm sorry, that was not the correct password.  Please try again."
InvalidPin.wav		"I'm sorry, that was not a valid pin number.  Please try again."
JoiningMeeting.wav	Attaching your call to the meeting.
JoinMeetingError.wav	"I'm sorry, I wasn't able to join that meeting.  If you'd like to try again, please enter a meeting id number or hang up and try later."
MeetingEnded.wav	Meeting ended.
MenuFeatureUnavail.wav	I'm sorry, that feature is unavailable for this meeting.
MenuHandDown.wav	Your hand is now down.
MenuHandUp.wav		Your hand is now up.
MenuInvalidSelection.wav	"I'm sorry, I didn't understand that selection.  Press zero to hear a list of options or press star to exit the menu."
MenuLectureOff.wav	Lecture mode is now off.
MenuLectureOn.wav	Lecture mode is now on.
MenuLoudVolume.wav	Your volume is now loud.
MenuMeetingLocked.wav	The meeting is now locked.
MenuMeetingUnlocked.wav	The meeting is now unlocked.
MenuModeratorEntry1.wav <same as MenuModeratorHelp1>
MenuModeratorEntry2.wav	Press 1 to end the meeting and end all calls.  Press 2 to end the meeting and leave any calls.  Press star to cancel this menu.
MenuModeratorHelp0.wav	Press 1 to dial out.  Press 2 to turn call recording on or off.  Press 3 to turn entry and exit tones on or off.  Press 4 to lock the meeting.  Press 5 to unlock the meeting.  Press 6 to mute your line.  Press 7 to unmute your line.  Press 8 to increase or decrease your volume.  Press 9 to play a roll call.  Press pound for more options.
MenuModeratorHelp1.wav	Press 4 to raise or lower your hand.  Press 5 to turn on or off lecture mode.  Press 8 to end the meeting.
MenuModeratorHelp2.wav	Press 1 to join you and the caller to the main meeting.  Press 2 to abandon the call and rejoin the main meeting.
MenuMuteJoinLeave.wav	Entry and exit tones are now off.
MenuUnmuteJoinLeave.wav	Entry and exit tones are now on.
MenuMuteOff.wav	Your muting is now off.
MenuMuteOn.wav	Your muting is now on.
MenuNormalVolume.wav	Your volume is now normal.
MenuParticipantHelp0.wav	Press 6 to mute your line.  Press 7 to unmute your line.  Press 8 to increase or decrease your volume. Press pound 4 to raise or lower your hand.
MenuParticipantHelp1.wav	Press 4 to raise or lower your hand.
MenuRecordingOff.wav	Meeting recording is now off.
MenuRecordingOn.wav	Meeting recording is now on.
MenuTimeout.wav	Press 0 for a list of options or press star to exit the menu.
MissingPassword.wav	"I'm sorry, I didn't hear the password.  Please try again."
MissingPin.wav		"I'm sorry, I didn't hear your meeting pin.  Please try again."
OutboundGreeting.wav	You have been invited to a meeting at the conference center.
RollCallBegin.wav	The following people are present:
SayName.wav		Please say your name at the tone.
StartingMeeting.wav	Starting the meeting.  You are the first caller.
StartingMeetingWithHolding.wav	Starting the meeting.  There are already callers present.
