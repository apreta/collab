<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Remove a community
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Remove Community</title>
</head>
<body>

<?php showNavBar(); ?>

<?php
// if community id was passed in, fill it into the form
$commid = ($_GET['commid']);
if ($commid != "")
{
    $community_id = $commid;
}
?>
Enter a community id to remove the community<br>
<FORM METHOD="POST"><br>
<table>
<tr><td>Community Id:</td>
<td><INPUT NAME="community_id" VALUE="<?php print ${community_id};?>"></td></tr>
<tr><td colspan='2' align='right'><input type="submit" value="remove community" name="submit"></td></tr>
</table>
</FORM>

<?php
$sessionid = getSessionId();

if ($HTTP_POST_VARS["community_id"]!="")
{
    // get vals from form
    $community_id = $HTTP_POST_VARS["community_id"];

    $sessionid = getSessionId();

    $f=new xmlrpcmsg(WEBSVR_FN_REMOVE_COMMUNITY,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($community_id, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode()) 
    {
        $numrowsidobj = $v->arraymem(0);
        $numrows = $numrowsidobj->scalarval();
        if ($numrows == 1)
            print "Community removed";
        else
            print "Community " . $community_id . " not found";
    }
    else  
    {
        dumpFault($r);
    }
}
?>
<?php showFooter(); ?>
</body>
</html>
