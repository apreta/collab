<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Search returning a list of communities.
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Search for Communities</title>
</head>
<body>

<?php showNavBar(); ?>

Enter a search string for the name of the communities, a starting community id, and the number
of rows to fetch.<br>
<FORM METHOD="POST"><br>
<table>
<tr><td>Name:</td>
<td><INPUT NAME="search_str" VALUE="<?php print ${search_str};?>"></td></tr>
<tr><td>Starting ID:</td>
<td><INPUT NAME="starting_row_id" VALUE="<?php print ${starting_row_id};?>"></td></tr>
<tr><td>Total rows:</td>
<td><INPUT NAME="total_rows" VALUE="<?php print ${total_rows};?>"></td></tr>
<tr><td colspan='2' align='right'><input type="submit" value="search communities" name="submit"></td></tr>
</table>
</FORM>

<?php
$sessionid = getSessionId();

if ($HTTP_POST_VARS["search_str"]!="")
{
    $searchstr = $HTTP_POST_VARS["search_str"];
    $starting_row_id = $HTTP_POST_VARS["starting_row_id"];
    $total_rows = $HTTP_POST_VARS["total_rows"];

    $f=new xmlrpcmsg(WEBSVR_FN_SEARCH_COMMUNITIES,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($searchstr, "string"),
                           new xmlrpcval($starting_row_id, "int"),
                           new xmlrpcval($total_rows, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();
    if (!$r->faultCode()) {
        $comm_array = $v->arraymem(0);          // primary array
        echo("<table border=\"1\">");
        echo("<tr><th>Operations</th><th>Community ID</th><th>Community Name</th><th>Account Name</th><th>Contact Name</th><th>Contact Email</th><th>Contact Phone</th><th>User Name</th><th>User Password</th><th>Bridge Phone</th><th>System URL</th><th>Account Type</th><th>Account Status</th><th>Options</th></tr>");
        for ($i = 0; $i < $comm_array->arraysize(); $i++)
        {
            // retrieve communities (an array)
            $commobj = $comm_array->arraymem($i);  
            $commidobj = $commobj->arraymem(0);

            echo("<tr>");

            // operations
            echo("<td>");
            echo("<a href=\"fetchcommunity.php?commid=" . $commidobj->scalarval() . "\">view details</a><br>");
            echo("<a href=\"updatecommunity.php?commid=" . $commidobj->scalarval() . "\">update</a><br>");
            echo("<a href=\"removecommunity.php?commid=" . $commidobj->scalarval() . "\">remove</a><br>");
            echo("<a href=\"enablecommunity.php?commid=" . $commidobj->scalarval() . "\">enable</a><br>");
            echo("</td>");

            echo("<td>&nbsp;" . $commidobj->scalarval() . "</td>");
            $commnameobj = $commobj->arraymem(1);
            echo("<td>&nbsp;" . $commnameobj->scalarval() . "</td>");
            $acctnumobj = $commobj->arraymem(2);
            echo("<td>&nbsp;" . $acctnumobj->scalarval() . "</td>");
            $contactnameobj = $commobj->arraymem(3);
            echo("<td>&nbsp;" . $contactnameobj->scalarval() . "</td>");
            $contactemailobj = $commobj->arraymem(4);
            echo("<td>&nbsp;" . $contactemailobj->scalarval() . "</td>");
            $contactphoneobj = $commobj->arraymem(5);
            echo("<td>&nbsp;" . $contactphoneobj->scalarval() . "</td>");
            $usernameobj = $commobj->arraymem(6);
            echo("<td>&nbsp;" . $usernameobj->scalarval() . "</td>");
            $userpasswordobj = $commobj->arraymem(7);
            echo("<td>&nbsp;" . $userpasswordobj->scalarval() . "</td>");
            $bridgephoneobj = $commobj->arraymem(8);
            echo("<td>&nbsp;" . $bridgephoneobj->scalarval() . "</td>");
            $systemurlobj = $commobj->arraymem(9);
            echo("<td>&nbsp;" . $systemurlobj->scalarval() . "</td>");
            $accounttypeobj = $commobj->arraymem(10);
            echo("<td>&nbsp;" . $accounttypeobj->scalarval() . "</td>");
            $accountstatusobj = $commobj->arraymem(11);
            echo("<td>&nbsp;" . $accountstatusobj->scalarval() . "</td>");
            $optionsobj = $commobj->arraymem(12);
            echo("<td>&nbsp;" . $optionsobj->scalarval() . "</td>");
            
            echo("</tr>");
        }
        echo("</table>");
        // dump out remaining data
        echo("<table border=\"1\">");
        echo("<tr><th>First</th><th>Chunk Size</th><th>Total Rows</th><th>More Avail?</th></tr>");
        echo("<tr>");
        $first_obj = $v->arraymem(1);
        echo("<td>&nbsp;" . $first_obj->scalarval() . "</td>");
        $chunk_obj = $v->arraymem(2);
        echo("<td>&nbsp;" . $chunk_obj->scalarval() . "</td>");
        $total_obj = $v->arraymem(3);
        echo("<td>&nbsp;" . $total_obj->scalarval() . "</td>");
        $more_obj = $v->arraymem(4);
        echo("<td>&nbsp;" . $more_obj->scalarval() . "</td>");
        echo("</tr>");
        echo("</table>");
    } else {
        dumpFault($r);
    }
}
?>
<?php showFooter(); ?>
</body>
</html>
