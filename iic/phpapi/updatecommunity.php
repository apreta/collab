<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Update a community
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Update a Community</title>
</head>
<body>

<?php showNavBar(); ?>

<?php
// if community id was passed in, fetch the details of the community and
// populate form
$commid = ($_GET['commid']);
if ($commid != "")
{
    // do fetch
    $sessionid = getSessionId();

    $f=new xmlrpcmsg(WEBSVR_FN_FETCH_COMMUNITY,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($commid, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();    
    if (!$r->faultCode()) 
    {
        // read in values into vars that will populate form
        $comm_array = $v->arraymem(0);
        $commobj = $comm_array->arraymem(0);

        $commidobj = $commobj->arraymem(0);
        $community_id = $commidobj->scalarval();

        $commnameobj = $commobj->arraymem(1);
        $community_name = $commnameobj->scalarval();

        $acctnumobj = $commobj->arraymem(2);
        $account_number = $acctnumobj->scalarval();

        $contactnameobj = $commobj->arraymem(3);
        $contact_name = $contactnameobj->scalarval();

        $contactemailobj = $commobj->arraymem(4);
        $contact_email = $contactemailobj->scalarval();

        $contactphoneobj = $commobj->arraymem(5);
        $contact_phone = $contactphoneobj->scalarval();

        $usernameobj = $commobj->arraymem(6);
        $user_name = $usernameobj->scalarval();

        $userpasswordobj = $commobj->arraymem(7);
        $user_password = $userpasswordobj->scalarval();

        $bridgephoneobj = $commobj->arraymem(8);
        $bridge_phone = $bridgephoneobj->scalarval();

        $systemurlobj = $commobj->arraymem(9);
        $system_url = $systemurlobj->scalarval();

        $accounttypeobj = $commobj->arraymem(10);
        $accounttype = $accounttypeobj->scalarval();

        $accountstatusobj = $commobj->arraymem(11);
        $accountstatus = $accountstatusobj->scalarval();

        $optionsobj = $commobj->arraymem(12);
        $options = $optionsobj->scalarval();
    }
    else
    {
        // either dump fault or do nothing
    }
}

?>
Enter all fields to update a community<br>
<FORM METHOD="POST"><br>
<table>
<tr><td>Community ID:</td>
<td><INPUT NAME="community_id" VALUE="<?php print ${community_id};?>"></td></tr>
<tr><td>Community Name:</td>
<td><INPUT NAME="community_name" VALUE="<?php print ${community_name};?>"></td></tr>
<tr><td>Account Name: </td>
<td><INPUT NAME="account_number" VALUE="<?php print ${account_number};?>"></td></tr>
<tr><td>Contact Name: </td>
<td><INPUT NAME="contact_name" VALUE="<?php print ${contact_name};?>"></td></tr>
<tr><td>Contact Email: </td>
<td><INPUT NAME="contact_email" VALUE="<?php print ${contact_email};?>"></td></tr>
<tr><td>Contact Phone: </td>
<td><INPUT NAME="contact_phone" VALUE="<?php print ${contact_phone};?>"></td></tr>
<tr><td>Admin Name: </td>
<td><INPUT NAME="user_name" VALUE="<?php print ${user_name};?>"></td></tr>
<tr><td>Admin Password: </td>
<td><INPUT NAME="user_password" VALUE="<?php print ${user_password};?>"></td></tr>
<tr><td>Bridge Phone: </td>
<td><INPUT NAME="bridge_phone" VALUE="<?php print ${bridge_phone};?>"></td></tr>
<tr><td>System URL: </td>
<td><INPUT NAME="system_url" VALUE="<?php print ${system_url};?>"></td></tr>
<tr><td>Options: </td>
<td><INPUT NAME="options" VALUE="<?php print ${options};?>"></td></tr>
<tr><td colspan='2' align='right'><input type="submit" value="update community" name="submit"></td></tr>
</table>
</FORM>

<?php
$sessionid = getSessionId();

if ($HTTP_POST_VARS["community_id"]!="" && $commid != "")
{
    // clear commid
    $commid = "";

    // get vals from form
    $community_id = $HTTP_POST_VARS["community_id"];
    $community_name = $HTTP_POST_VARS["community_name"];
    $account_number = $HTTP_POST_VARS["account_number"];
    $contact_name = $HTTP_POST_VARS["contact_name"];
    $contact_email = $HTTP_POST_VARS["contact_email"];
    $contact_phone = $HTTP_POST_VARS["contact_phone"];
    $user_name = $HTTP_POST_VARS["user_name"];
    $user_password = $HTTP_POST_VARS["user_password"];
    $bridge_phone = $HTTP_POST_VARS["bridge_phone"];
    $system_url = $HTTP_POST_VARS["system_url"];
    $options = $HTTP_POST_VARS["options"];

    $sessionid = getSessionId();

    $f=new xmlrpcmsg(WEBSVR_FN_UPDATE_COMMUNITY,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($community_id, "int"),
                           new xmlrpcval($community_name, "string"),
                           new xmlrpcval($account_number, "string"),
                           new xmlrpcval($contact_name, "string"),
                           new xmlrpcval($contact_email, "string"),
                           new xmlrpcval($contact_phone, "string"),
                           new xmlrpcval($user_name, "string"),
                           new xmlrpcval($user_password, "string"),
                           new xmlrpcval($bridge_phone, "string"),
                           new xmlrpcval($system_url, "string"),
                           new xmlrpcval($options, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode()) 
    {
        $numrowsidobj = $v->arraymem(0);
        $numrows = $numrowsidobj->scalarval();
        if ($numrows == 1)
            print "Community updated";
        else
            print "Community " . $community_id . " not found";
    }
    else  
    {
        dumpFault($r);
    }
}
?>
<?php showFooter(); ?>
</body>
</html>
