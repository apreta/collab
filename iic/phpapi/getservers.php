<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Get servers.
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Get Servers</title>
</head>
<body>

<?php showNavBar(); ?>
<?php

print "Enter a service type to look up server details";
print "<FORM  METHOD=\"POST\">
<INPUT NAME=\"servertype\" VALUE=\"${servertype}\"><input type=\"submit\" value=\"go\" name=\"submit\">&nbsp;(1: voice, 2: appshare)</FORM><P>";

$sessionid = getSessionId();

if ($HTTP_POST_VARS["servertype"]!="") 
{
    $f=new xmlrpcmsg(WEBSVR_FN_GET_SERVERS,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($HTTP_POST_VARS["servertype"], "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $svr_array=$r->value();
    if (!$r->faultCode()) 
    {
        // structure is:
        //   array of svr details
        //     where details of each server is an array
        echo("<table border=\"1\">");
        echo("<tr><th>Server ID</th><th>Service Type</th><th>State</th></tr>");
        for ($i = 0; $i < $svr_array->arraysize(); $i++)
        {
            echo("<tr>");
            $svr = $svr_array->arraymem($i);  // retrieve servers (an array)
            $svridobj = $svr->arraymem(0);
            echo("<td>" . $svridobj->scalarval() . "&nbsp;</td>");
            $svrtypeobj = $svr->arraymem(1);
            echo("<td>" . $svrtypeobj->scalarval() . "&nbsp;</td>");
            $stateobj = $svr->arraymem(2);
            echo("<td>" . $stateobj->scalarval() . "&nbsp;</td>");
        }
        echo("</table>");
        
    }
    else 
    {
	print "Fault: ";
	print "Code: " . $r->faultCode() . " Reason '" .$r->faultString()."'<BR>";
    }
}
?>
<?php showFooter(); ?>
</body>
</html>
