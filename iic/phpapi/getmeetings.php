<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Get port usage.
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Get Meetings</title>
</head>
<body>

<?php showNavBar(); ?>
<?php

$sessionid = getSessionId();

if ($sessionid!="") 
{
    $f=new xmlrpcmsg(WEBSVR_FN_GET_MEETINGS,
                     array(new xmlrpcval($sessionid, "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();
    if (!$r->faultCode())
    {
        // structure is:
        // array of values
        //   array of mtg details
        //     where each mtg is an array
        //   start row
        //   number
        //   more
        //   total
        $mtg_array = $v->arraymem(0);          // primary array
        echo("<table border=\"1\">");
        echo("<tr><th>Mtg ID</th><th>Host ID</th><th>Type</th><th>Privacy</th><th>Title</th><th>Description</th><th>Duration</th><th>Time</th><th>Options</th><th>Num Invited</th><th>Num Connected</th><th>Appshare Server</th><th>Num Appshare Sessions</th><th>Voice Server</th><th>Num Calls</th></tr>");
        for ($i = 0; $i < $mtg_array->arraysize(); $i++)
        {
            echo("<tr>");
            $mtg = $mtg_array->arraymem($i);  // retrieve mtgs (an array)
            $mtgidobj = $mtg->arraymem(0);
            echo("<td>" . $mtgidobj->scalarval() . "</td>");
            $hostidobj = $mtg->arraymem(1);
            echo("<td>" . $hostidobj->scalarval() . "</td>");
            $typeobj = $mtg->arraymem(2);
            echo("<td>" . $typeobj->scalarval() . "</td>");
            $privobj = $mtg->arraymem(3);
            echo("<td>" . $privobj->scalarval() . "</td>");
            $mtgnameobj = $mtg->arraymem(4);
            echo("<td>" . $mtgnameobj->scalarval() . "</td>");
            $mtgdescobj = $mtg->arraymem(5);
            echo("<td>" . $mtgdescobj->scalarval() . "</td>");
            $mtgdurationobj = $mtg->arraymem(6);
            echo("<td>" . $mtgdurationobj->scalarval() . "</td>");
            $mtgtimeobj = $mtg->arraymem(7);
            echo("<td>" . $mtgtimeobj->scalarval() . "</td>");
            $mtgoptionsobj = $mtg->arraymem(8);
            echo("<td>" . $mtgoptionsobj->scalarval() . "</td>");
            $numinvitedobj = $mtg->arraymem(9);
            echo("<td>" . $numinvitedobj->scalarval() . "</td>");
            $numconnectedobj = $mtg->arraymem(10);
            echo("<td>" . $numconnectedobj->scalarval() . "</td>");
            $appserverobj = $mtg->arraymem(11);
            echo("<td>" . $appserverobj->scalarval() . "</td>");
            $numappshareobj = $mtg->arraymem(12);
            echo("<td>" . $numappshareobj->scalarval() . "</td>");
            $voiceserverobj = $mtg->arraymem(13);
            echo("<td>" . $voiceserverobj->scalarval() . "</td>");
            $numcallsobj = $mtg->arraymem(14);
            echo("<td>" . $numcallsobj->scalarval() . "</td>");
        }
        echo("</table>");
        
	print "<HR>Participant details (XML)<BR><PRE>" .
            htmlentities($r->serialize()). "</PRE><HR>\n";
    } 
    else
    {
	print "Fault: ";
	print "Code: " . $r->faultCode() . 
            " Reason '" .$r->faultString()."'<BR>";
    }
}
?>
<?php showFooter(); ?>
</body>
</html>
