<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Main menu - list of APIs that can be called.
//

include("common.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth(); ?>
<title>Main Menu</title>
</head>
<body>

<?php showNavBar(); ?>
<p><a href="findmeetingsbyhost.php">find meetings by host</a> (an example API not in supported documentation)
<?php 
if (isSuper()) {?>
<h2>Super Admin functions </h2>
<p><a href="addcommunity.php">add community</a>
<p><a href="removecommunity.php">remove community</a>
<p><a href="updatecommunity.php">update community</a>
<p><a href="fetchcommunitylist.php">fetch community list</a>
<p><a href="searchcommunities.php">search communities</a>
<p><a href="fetchcommunity.php">fetch community details</a>
<p><a href="adduser.php">add user</a>
<p><a href="searchusers.php">search users</a>
<p><a href="setpassword.php">set user password</a>
<p><a href="getmeetings.php">get meetings</a>
<p><a href="getports.php">get ports</a>
<p><a href="getservers.php">get servers</a>
<?php }?>
<p>
<?php
showFooter();
?>
</body>
</html>
