<?php

//
// Copyright 2004 Imidio, Inc.
//

//
// Constants and utilities
//

// webservice domain
//define(WEB_SERVICE_DOMAIN, "localhost");
define(WEB_SERVICE_DOMAIN, "10.0.1.18");

// webservice port
//define(WEB_SERVICE_PORT, 80);
define(WEB_SERVICE_PORT, 8000);

// webservice URI
// use this URI to look things up 
define(WEB_SERVICE_URI, "/imidio_api/find.php");
// this URI will return the xmlrpc request back as the response for debugging purposes
//define(WEB_SERVICE_URI, "/imidio_api/debug.php");

// cookie names
define(SESSION_ID_COOKIE_NAME, "imidiosessionid");
define(ISSUPER_COOKIE_NAME, "imidioissuper");

// verifies that the user has valid sessionid.  if not, redirects to login page
function checkAuth()
{
    $sessionid = $_COOKIE[SESSION_ID_COOKIE_NAME];
    if (!($sessionid > 0))
    {
        echo("<meta http-equiv='refresh' content='0;url=login.php'>");
    }
}

// sets the session cookie
function setSessionCookie($sessionid)
{
    $cookieset = setcookie (SESSION_ID_COOKIE_NAME, $sessionid, time()+3600, '/'); 
}

// sets the superadmin cookie
function setSuperAdminCookie($issuper)
{
    $cookieset = setcookie (ISSUPER_COOKIE_NAME, $issuper, time()+3600, '/');
}

function clearCookies()
{
    $cookieset = setcookie (SESSION_ID_COOKIE_NAME, "", time()-3600, '/');
    $cookieset = setcookie (ISSUPER_COOKIE_NAME, "", time()-3600, '/');
}

// retrieves the session id from the cookie
function getSessionId()
{
    return $_COOKIE[SESSION_ID_COOKIE_NAME];
}

// returns true if user is super admin
function isSuper()
{
    $issuper = $_COOKIE[ISSUPER_COOKIE_NAME];
    if ($issuper == "t")
        return True;
    return False;
}

// clears the browser cache
function clearCache()
{
    header("Expires: Mon, 26 Jul 2001 05:00:00 GMT");               // Date in the past
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");  // always modified
    header("Cache-Control: no-store, no-cache, must-revalidate");   // HTTP/1.1
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");                                     // HTTP/1.0
    header("Cache-control: private");                               // special magic
}

// render a navigation bar and beginning of a table cell for the rest of the 
// content
function showNavBar()
{
    echo("<table>
            <tr>
              <td width=100 valign=\"top\">
                <table bgcolor=\"#f0f0f0\">
                  <tr><td><a href=\"login.php?logout=1\">logout</a></td></tr>
                  <tr><td><a href=\"menu.php\">main menu</a></td></tr>
                </table>
              </td>
              <td width=\"400\" valign=\"top\">"
         );
}

// close out the content table cell and the main page table
function showFooter()
{
    echo("</td></tr>
         </table>");
}

// dumps out fault code and string
function dumpFault($r)
{
	print "Fault: ";
	print "Code: " . $r->faultCode() . "<BR>" .
            " Reason: '" .$r->faultString()."'<BR>";
}

// dumps out xmlrpc response
function dumpResponse($r)
{
	print "<HR>XMLRPC response (XML)<BR><PRE>" .
            htmlentities($r->serialize()). "</PRE><HR>\n";
}

///////////////////////////////////////////////////////////////////////
// WebService function definitions
///////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////
// authenticate_user
// @param       username
// @param       password
// @param       communityname (for superadmins only, ignored otherwise)
//
// @return      SESSIONID
// @return      USERID
// @return      USERNAME
// @return      COMMUNITYID
// @return      DIRECTORYID (for personal directory)
// @return      DEFAULTGROUPID (for personal directory)
// @return      USERTYPE
// @return      ISSUPERADMIN
// @return      ENABLEDFEATURES
// @return      PROFILEOPTIONS
// @return      DEFPHONE
// @return      BUSPHONE
// @return      HOMEPHONE
// @return      MOBILEPHONE
// @return      OTHERPHONE
// @return      INSTANTMEETINGID
// @return      FIRSTNAME
// @return      LASTNAME
// @return      SYSTEMPHONE
// @return      SYSTEMURL
// @note        If returned USERID is empty string, user was not found or
//              password is incorrect. 
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_AUTH_USER, 'addressbk.authenticate_user');

///////////////////////////////////////////////////////////////////////
// add_community
// @param       SESSIONID
// @param       COMMUNITYNAME
// @param       ACCOUNTNUMBER
// @param       CONTACTNAME
// @param       CONTACTEMAIL
// @param       CONTACTPHONE
// @param       USERNAME
// @param       USERPASSWORD
// @param       USERISADMIN (0 = normal user, 1 = admin)
// @param       BRIDGEPHONE
// @param       SYSTEMURL
// @param       OPTIONS
// @param       DEFAULTPROFILEID (blank uses internal defaults)
//
// @return      COMMUNITYID
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_ADD_COMMUNITY, 'addressbk.add_community');

///////////////////////////////////////////////////////////////////////
// remove_community
// @param       SESSIONID
// @param       COMMUNITYID
//
// @return      NUMBER OF ROWS AFFECTED
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_REMOVE_COMMUNITY, 'addressbk.remove_community');

///////////////////////////////////////////////////////////////////////
// update_community
// @param       SESSIONID
// @param       COMMUNITYID
// @param       COMMUNITYNAME
// @param       ACCOUNTNUMBER
// @param       CONTACTNAME
// @param       CONTACTEMAIL
// @param       CONTACTPHONE
// @param       USERNAME
// @param       USERPASSWORD
// @param       BRIDGEPHONE
// @param       SYSTEMURL
// @param       OPTIONS
// @param       DEFAULTPROFILEID (???)
//
// @return      NUMBER OF ROWS AFFECTED
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_UPDATE_COMMUNITY, 'addressbk.update_community');

///////////////////////////////////////////////////////////////////////
// enable_community
// @param       SESSIONID
// @param       COMMUNITYID
// @param       ENABLE (0 for disable, non-zero to enable)
//
// @return      NUMBER OF ROWS AFFECTED
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_ENABLE_COMMUNITY, 'addressbk.enable_community');

///////////////////////////////////////////////////////////////////////
// fetch_community_details
// @param      SESSIONID
// @param      COMMUNITYID
//
// @return     COMMUNITYID
// @return     COMMUNITYNAME
// @return     ACCOUNTNUMBER
// @return     CONTACTNAME
// @return     CONTACTEMAIL
// @return     CONTACTPHONE
// @return     ADMINNAME
// @return     ADMINPASSWORD
// @return     BRIDGEPHONE
// @return     SYSTEMURL
// @return     ACCOUNTTYPE
// @return     ACCOUNTSTATUS
// @return     OPTIONS
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_FETCH_COMMUNITY, 'addressbk.fetch_community_details');

///////////////////////////////////////////////////////////////////////
// fetch_community_list
// @param       SESSIONID
// @param       FIRST (# of first row to fetch)
// @param       CHUNKSIZE (# of rows to fetch)
// --- return array of community data ----
// @return     COMMUNITYID
// @return     COMMUNITYNAME
// @return     ACCOUNTNUMBER
// @return     CONTACTNAME
// @return     CONTACTEMAIL
// @return     CONTACTPHONE
// @return     USERNAME
// @return     USERPASSWORD
// @return     BRIDGEPHONE
// @return     SYSTEMURL
// @return     ACCOUNTTYPE
// @return     ACCOUNTSTATUS
// @return     OPTIONS
// ---  followed by ---
// @return     FIRST
// @return     CHUNKSIZE
// @return     TOTALROWS
// @return     MOREAVAILABLE
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_FETCH_COMMUNITY_LIST, 'addressbk.fetch_community_list');

///////////////////////////////////////////////////////////////////////
// search_communities
// @param       SESSIONID
// @param       SEARCHSTRING
// @param       FIRST (# of first row to fetch)
// @param       CHUNKSIZE (# of rows to fetch)
// --- return array of community data ----
// @return     COMMUNITYID
// @return     COMMUNITYNAME
// @return     ACCOUNTNUMBER
// @return     CONTACTNAME
// @return     CONTACTEMAIL
// @return     CONTACTPHONE
// @return     USERNAME
// @return     USERPASSWORD
// @return     BRIDGEPHONE
// @return     SYSTEMURL
// @return     ACCOUNTTYPE
// @return     ACCOUNTSTATUS
// @return     OPTIONS
// ---  followed by ---
// @return     FIRST
// @return     CHUNKSIZE
// @return     TOTALROWS
// @return     MOREAVAILABLE
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_SEARCH_COMMUNITIES, 'addressbk.search_communities');

///////////////////////////////////////////////////////////////////////
// add_user
// @param       SESSIONID
// @param       PASSWORD
// @param       COMMUNITYID
// @param       TITLE
// @param       FIRSTNAME
// @param       MIDDLENAME
// @param       LASTNAME
// @param       SUFFIX
// @param       COMPANY
// @param       JOBTITLE
// @param       ADDRESS1
// @param       ADDRESS2
// @param       STATE
// @param       COUNTRY
// @param       POSTALCODE
// @param       SCREENNAME
// @param       EMAIL
// @param       EMAIL2
// @param       EMAIL3
// @param       BUSPHONE
// @param       HOMEPHONE
// @param       MOBILEPHONE
// @param       OTHERPHONE
// @param       EXTENSION
// @param       AIMSCREEN
// @param       YAHOOSCREEN
// @param       MSNSCREEN
// @param       USER1
// @param       USER2
// @param       DEFPHONE:  0 for none, 1 for busphone, 2 for homephone, 
//              3 for mobilephone, 4 for otherphone
// @param       DEFEMAIL:  0 for none, 1 for email1, 2 for email2, 3 for email3
// @param       TYPE:  1 for registered user, 2 for admin
// @param       PROFILEID (blank for default)
//
// @return      USERID
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_ADD_USER, 'addressbk.add_user');

///////////////////////////////////////////////////////////////////////
// search_users
// @param      SESSIONID
// @param      COMMUNITYID
// @param      USERNAME
// @param      EMAIL
// @param      FIRST (# of first row to fetch)
// @param      CHUNKSIZE (# of rows to fetch)
// --- return array of user data ----
// @return     USERID
// @return     COMMUNITYID
// @return     SCREENNAME
// @return     ADMIN (t if admin)
// @return     SUPERADMIN (t if superadmin)
// @return     PASSWORD
// @return     USERTYPE
// @return     FIRSTNAME
// @return     LASTNAME
// @return     DEFPHONE
// @return     BUSPHONE
// @return     HOMEPHONE
// @return     MOBILEPHONE
// @return     OTHERPHONE
// @return     EMAIL (primary)
// ---  followed by ---
// @return     FIRST
// @return     CHUNKSIZE
// @return     TOTALROWS
// @return     MOREAVAILABLE
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_SEARCH_USERS, 'addressbk.search_users');

///////////////////////////////////////////////////////////////////////
// set_password
// @param      SESSIONID
// @param      USERID
// @param      OLDPASSWORD
// @param      NEWPASSWORD
// @param      ADMINOVERRIDE
//
// @return     CHANGED (true if password changed)
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_SET_PASSWORD, 'addressbk.set_password');


///////////////////////////////////////////////////////////////////////
// get_meetings
// @param      SESSIONID
//
// --- return array of user data ----
// @return     MEETINGID
// @return     HOSTID
// @return     TYPE (instant, scheduled)
// @return     PRIVACY (public, private, unlisted)
// @return     TITLE
// @return     DESCRIPTION
// @return     DURATION
// @return     TIME
// @return     OPTIONS
// @return     NUMBER_INVITED_PARTICIPANTS
// @return     NUMBER_CONNECTED_PARTICIPANTS
// @return     APPSHARE_SERVER
// @return     NUMBER_APPSHARE_SESSIONS
// @return     VOICE_SERVER
// @return     NUMBER_CALLS
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_GET_MEETINGS, 'controller.get_meetings');

///////////////////////////////////////////////////////////////////////
// get_servers
// @param      SESSIONID
// @param      SERVICETYPE (1 = voice, 2 = appshare)
//
// --- return array of user data ----
// @return     SERVERID
// @return     SERVICETYPE
// @return     STATE (1 = available, 2 = unknown)
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_GET_SERVERS, 'controller.get_servers');

///////////////////////////////////////////////////////////////////////
// get_ports
// @param      SESSIONID
// @param      SERVERID (VOICE)
//
// --- return array of user data ----
// @return     PORTID
// @return     STATE (1 = connected)
// @return     MEETINGID
// @return     PARTICIPANTID
// @return     SCREENNAME
// @return     NAME
// @return     LASTEVENTTIME
// @return     LASTEVENTNAME
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_GET_PORTS, 'controller.get_ports');

///////////////////////////////////////////////////////////////////////
// reset_port
// @param      SESSIONID
// @param      SERVERID
// @param      PORTID
// @param      PARTICIPANTID (if non-empty reset only if given participant is
// using it)
//
// --- return array of user data ----
// @return     number of ports affected
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_RESET_PORT, 'controller.reset_port');

?>
