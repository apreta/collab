<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Enable/disable a community
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Enable/Disable Community</title>
</head>
<body>

<?php showNavBar(); ?>

<?php
// if community id was passed in, fill it into the form
$commid = ($_GET['commid']);
if ($commid != "")
{
    $community_id = $commid;
}
?>
Enter a community id and choose to enable or disable the community<br>
<FORM METHOD="POST"><br>
<table>
<tr><td>Community Id:</td>
<td><INPUT NAME="community_id" VALUE="<?php print ${community_id};?>"></td></tr>
<tr><td>Enable:</td>
<td><INPUT NAME="enable" type="CHECKBOX" VALUE="1"></td></tr>
<tr><td colspan='2' align='right'><input type="submit" value="enable community" name="submit"></td></tr>
</table>
</FORM>

<?php
$sessionid = getSessionId();

if ($HTTP_POST_VARS["community_id"]!="")
{
    // get vals from form
    $community_id = $HTTP_POST_VARS["community_id"];
    $enable = $HTTP_POST_VARS["enable"];

    $sessionid = getSessionId();
    if ($enable == "")
    {
        $enable_i = 0;
        $enable_str = "disabled";
    }
    else
    {
        $enable_i = 1;
        $enable_str = "enabled";
    }

    $f=new xmlrpcmsg(WEBSVR_FN_ENABLE_COMMUNITY,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($community_id, "int"),
                           new xmlrpcval($enable_i, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode()) 
    {
        $numrowsidobj = $v->arraymem(0);
        $numrows = $numrowsidobj->scalarval();
        if ($numrows == 1)
            print "Community " . $enable_str;
        else
            print "Community " . $community_id . " not found";
    }
    else  
    {
        dumpFault($r);
    }
}
?>
<?php showFooter(); ?>
</body>
</html>
