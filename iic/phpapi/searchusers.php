<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Search returning a list of users.
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Search for Users</title>
</head>
<body>

<?php showNavBar(); ?>

Enter a community id, a search string for the username and/or an email address, 
a starting user id, and the number of rows to fetch.<br>
<FORM METHOD="POST"><br>
<table>
<tr><td>Community ID:</td>
<td><INPUT NAME="community_id" VALUE="<?php print ${community_id};?>"></td></tr>
<tr><td>Username:</td>
<td><INPUT NAME="username" VALUE="<?php print ${username};?>"></td></tr>
<tr><td>Email:</td>
<td><INPUT NAME="email" VALUE="<?php print ${email};?>"></td></tr>
<tr><td>Starting ID:</td>
<td><INPUT NAME="starting_row_id" VALUE="<?php print ${starting_row_id};?>"></td></tr>
<tr><td>Total rows:</td>
<td><INPUT NAME="total_rows" VALUE="<?php print ${total_rows};?>"></td></tr>
<tr><td colspan='2' align='right'><input type="submit" value="search users" name="submit"></td></tr>
</table>
</FORM>

<?php
$sessionid = getSessionId();

if ($HTTP_POST_VARS["community_id"]!="")
{
    $community_id = $HTTP_POST_VARS["community_id"];
    $username = $HTTP_POST_VARS["username"];
    $email = $HTTP_POST_VARS["email"];
    $starting_row_id = $HTTP_POST_VARS["starting_row_id"];
    $total_rows = $HTTP_POST_VARS["total_rows"];

    $f=new xmlrpcmsg(WEBSVR_FN_SEARCH_USERS,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($community_id, "string"),
                           new xmlrpcval($username, "string"),
                           new xmlrpcval($email, "string"),
                           new xmlrpcval($starting_row_id, "int"),
                           new xmlrpcval($total_rows, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();
    if (!$r->faultCode()) {
        $user_array = $v->arraymem(0);          // primary array
        echo("<table border=\"1\">");
        echo("<tr><th>Operations</th><th>User ID</th><th>Community ID</th><th>Screenname</th>");
        echo("<th>Is Admin</th><th>Is SuperAdmin</th><th>Password</th><th>User Type</th><th>First Name</th><th>Last Name</th>");
        echo("<th>Default Phone</th><th>Bus Phone</th><th>Home Phone</th><th>Mobile Phone</th><th>Other Phone</th>");
        echo("<th>Email</th></tr>");
        for ($i = 0; $i < $user_array->arraysize(); $i++)
        {
            // retrieve users (an array)
            $userobj = $user_array->arraymem($i);  
            $useridobj = $userobj->arraymem(0);

            echo("<tr>");

            // operations
            echo("<td>");
            echo("<a href=\"setpassword.php?userid=" . $useridobj->scalarval() . "\">set password</a><br>");
            echo("</td>");
            echo("<td>&nbsp;" . $useridobj->scalarval() . "</td>");

            $commidobj = $userobj->arraymem(1);
            echo("<td>&nbsp;" . $commidobj->scalarval() . "</td>");

            $usernameobj = $userobj->arraymem(2);
            echo("<td>&nbsp;" . $usernameobj->scalarval() . "</td>");

            $isadminobj = $userobj->arraymem(3);
            echo("<td>&nbsp;" . $isadminobj->scalarval() . "</td>");

            $issuperadminobj = $userobj->arraymem(4);
            echo("<td>&nbsp;" . $issuperadminobj->scalarval() . "</td>");

            $pwdobj = $userobj->arraymem(5);
            echo("<td>&nbsp;" . $pwdobj->scalarval() . "</td>");
            
            $typeobj = $userobj->arraymem(6);
            echo("<td>&nbsp;" . $typeobj->scalarval() . "</td>");
            
            $firstnameobj = $userobj->arraymem(7);
            echo("<td>&nbsp;" . $firstnameobj->scalarval() . "</td>");

            $lastnameobj = $userobj->arraymem(8);
            echo("<td>&nbsp;" . $lastnameobj->scalarval() . "</td>");

            $defphoneobj = $userobj->arraymem(9);
            echo("<td>&nbsp;" . $defphoneobj->scalarval() . "</td>");
            
            $busphoneobj = $userobj->arraymem(10);
            echo("<td>&nbsp;" . $busphoneobj->scalarval() . "</td>");
            
            $homephoneobj = $userobj->arraymem(11);
            echo("<td>&nbsp;" . $homephoneobj->scalarval() . "</td>");
            
            $mobilephoneobj = $userobj->arraymem(12);
            echo("<td>&nbsp;" . $mobilephoneobj->scalarval() . "</td>");
            
            $otherphoneobj = $userobj->arraymem(13);
            echo("<td>&nbsp;" . $otherphoneobj->scalarval() . "</td>");
            
            $emailobj = $userobj->arraymem(14);
            echo("<td>&nbsp;" . $emailobj->scalarval() . "</td>");
            
            echo("</tr>");
        }
        echo("</table>");
        // dump out remaining data
        echo("<table border=\"1\">");
        echo("<tr><th>First</th><th>Chunk Size</th><th>Total Rows</th><th>More Avail?</th></tr>");
        echo("<tr>");
        $first_obj = $v->arraymem(1);
        echo("<td>&nbsp;" . $first_obj->scalarval() . "</td>");
        $chunk_obj = $v->arraymem(2);
        echo("<td>&nbsp;" . $chunk_obj->scalarval() . "</td>");
        $total_obj = $v->arraymem(3);
        echo("<td>&nbsp;" . $total_obj->scalarval() . "</td>");
        $more_obj = $v->arraymem(4);
        echo("<td>&nbsp;" . $more_obj->scalarval() . "</td>");
        echo("</tr>");
        echo("</table>");
    } else {
        dumpFault($r);
    }
}
?>
<?php showFooter(); ?>
</body>
</html>
