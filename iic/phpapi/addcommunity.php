<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Add a community
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Add Community</title>
</head>
<body>

<?php showNavBar(); ?>

Enter all fields to add a community<br>
<FORM METHOD="POST"><br>
<table>
<tr><td>Community Name:</td>
<td><INPUT NAME="community_name" VALUE="<?php print ${community_name};?>"></td></tr>
<tr><td>Account Number: </td>
<td><INPUT NAME="account_number" VALUE="<?php print ${account_number};?>"></td></tr>
<tr><td>Contact Name: </td>
<td><INPUT NAME="contact_name" VALUE="<?php print ${contact_name};?>"></td></tr>
<tr><td>Contact Email: </td>
<td><INPUT NAME="contact_email" VALUE="<?php print ${contact_email};?>"></td></tr>
<tr><td>Contact Phone: </td>
<td><INPUT NAME="contact_phone" VALUE="<?php print ${contact_phone};?>"></td></tr>
<tr><td>User Name: </td>
<td><INPUT NAME="user_name" VALUE="<?php print ${user_name};?>"></td></tr>
<tr><td>User Password: </td>
<td><INPUT NAME="user_password" VALUE="<?php print ${user_password};?>"></td></tr>
<tr><td>User Screenname: </td>
<td><INPUT NAME="user_screen" VALUE="<?php print ${user_screen};?>"></td></tr>
<tr><td>User is admin: </td>
<td><INPUT NAME="user_is_admin" TYPE="CHECKBOX" VALUE="1"></td></tr>
<tr><td>Bridge Phone: </td>
<td><INPUT NAME="bridge_phone" VALUE="<?php print ${bridge_phone};?>"></td></tr>
<tr><td>System URL: </td>
<td><INPUT NAME="system_url" VALUE="<?php print ${system_url};?>"></td></tr>
<tr><td>Options: </td>
<td><INPUT NAME="options" VALUE="<?php print ${options};?>"></td></tr>
<tr><td>Default Profile ID: </td>
<td><INPUT NAME="default_profile_id" VALUE="<?php print ${default_profile_id};?>"></td></tr>
<tr><td colspan='2' align='right'><input type="submit" value="add community" name="submit"></td></tr>
</table>
</FORM>

<?php
$sessionid = getSessionId();

if ($HTTP_POST_VARS["community_name"]!="")
{
    // get vals from form
    $community_name = $HTTP_POST_VARS["community_name"];
    $account_number = $HTTP_POST_VARS["account_number"];
    $contact_name = $HTTP_POST_VARS["contact_name"];
    $contact_email = $HTTP_POST_VARS["contact_email"];
    $contact_phone = $HTTP_POST_VARS["contact_phone"];
    $user_name = $HTTP_POST_VARS["user_name"];
    $user_password = $HTTP_POST_VARS["user_password"];
    $user_screen = $HTTP_POST_VARS["user_screen"];
    $user_is_admin = $HTTP_POST_VARS["user_is_admin"];
    if ($user_is_admin == "")
        $user_is_admin = 0;
    $bridge_phone = $HTTP_POST_VARS["bridge_phone"];
    $system_url = $HTTP_POST_VARS["system_url"];
    $options = $HTTP_POST_VARS["options"];
    $default_profile_id = $HTTP_POST_VARS["default_profile_id"];

    $sessionid = getSessionId();

    $f=new xmlrpcmsg(WEBSVR_FN_ADD_COMMUNITY,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($community_name, "string"),
                           new xmlrpcval($account_number, "string"),
                           new xmlrpcval($contact_name, "string"),
                           new xmlrpcval($contact_email, "string"),
                           new xmlrpcval($contact_phone, "string"),
                           new xmlrpcval($user_name, "string"),
                           new xmlrpcval($user_password, "string"),
                           new xmlrpcval($user_screen, "string"),
                           new xmlrpcval($user_is_admin, "int"),
                           new xmlrpcval($bridge_phone, "string"),
                           new xmlrpcval($system_url, "string"),
                           new xmlrpcval($options, "int"),
                           new xmlrpcval($default_profile_id, "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode()) 
    {
        $commidobj = $v->arraymem(0);
        $commid = $commidobj->scalarval();
        print "Community: " . $community_name . "<br>New community id: " . $commid;
    }
    else  
    {
        dumpFault($r);
    }
}
?>
<?php showFooter(); ?>
</body>
</html>
