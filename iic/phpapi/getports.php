<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Get port usage.
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Get Ports</title>
</head>
<body>

<?php showNavBar(); ?>
<?php

print "Enter a VOICE server id to look up port details";
print "<FORM  METHOD=\"POST\">
<INPUT NAME=\"serverid\" VALUE=\"${serverid}\"><input type=\"submit\" value=\"go\" name=\"submit\"></FORM><P>";

$sessionid = getSessionId();

if ($HTTP_POST_VARS["serverid"]!="") 
{
    $f=new xmlrpcmsg(WEBSVR_FN_GET_PORTS,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($HTTP_POST_VARS["serverid"], "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $port_array=$r->value();
    if (!$r->faultCode()) 
    {
        // structure is:
        //   array of port details
        //     where details of each port is an array
        echo("<table border=\"1\">");
        echo("<tr><th>Port ID</th><th>State</th><th>Meeting ID</th><th>ParticipantID</th><th>Screenname</th><th>Name</th><th>Last Event Time</th><th>Last Event Name</th></tr>");
        for ($i = 0; $i < $port_array->arraysize(); $i++)
        {
            echo("<tr>");
            $port = $port_array->arraymem($i);  // retrieve port details (an array)
            $portidobj = $port->arraymem(0);
            echo("<td>" . $portidobj->scalarval() . "&nbsp;</td>");
            $stateobj = $port->arraymem(1);
            echo("<td>" . $stateobj->scalarval() . "&nbsp;</td>");
            $mtgidobj = $port->arraymem(2);
            echo("<td>" . $mtgidobj->scalarval() . "&nbsp;</td>");
            $partidobj = $port->arraymem(3);
            echo("<td>" . $partidobj->scalarval() . "&nbsp;</td>");
            $screenobj = $port->arraymem(4);
            echo("<td>" . $screenobj->scalarval() . "&nbsp;</td>");
            $nameobj = $port->arraymem(5);
            echo("<td>" . $nameobj->scalarval() . "&nbsp;</td>");
            $lasttimeobj = $port->arraymem(6);
            echo("<td>" . $lasttimeobj->scalarval() . "&nbsp;</td>");
            $lasteventobj = $port->arraymem(7);
            echo("<td>" . $lasteventobj->scalarval() . "&nbsp;</td>");
        }
        echo("</table>");
    } 
    else 
    {
	print "Fault: ";
	print "Code: " . $r->faultCode() . " Reason '" .$r->faultString()."'<BR>";
    }
}
?>
<?php showFooter(); ?>
</body>
</html>
