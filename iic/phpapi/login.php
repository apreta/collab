<?php

//
// Copyright 2004 Imidio, Inc.
//

//
// Basic login/authentication
//

include("xmlrpc.inc");
include("common.inc");

// verify user/password and redirect to main menu if passes
$redirect = 0;
$msg = "";

$logout = ($_GET['logout']);
if ($logout == 1)
{
    clearCookies();
}
if ($HTTP_POST_VARS["uname"]!="")
{
    $username = $HTTP_POST_VARS["uname"];
    $password = $HTTP_POST_VARS["pwd"];
    $community_name = $HTTP_POST_VARS["community_name"];
    $f=new xmlrpcmsg(WEBSVR_FN_AUTH_USER,
                     array(new xmlrpcval($username, "string"),
                           new xmlrpcval($password, "string"),
                           new xmlrpcval($community_name, "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode()) 
    {
        $sessobj = $v->arraymem(0);
        $sessionid = $sessobj->scalarval();
        if ($sessionid > 0)
        {
            $redirect = 1;
            
            // retrieve user name
            $usernameobj = $v->arraymem(2);
            $username = $usernameobj->scalarval();
            $msg = "Welcome " .  $username . ". Loading main menu ...";

            // store session id in cookie
            setSessionCookie($sessionid);

            // store super admin t/f in cookie
            $issuperobj = $v->arraymem(7);
            $issuper = $issuperobj->scalarval();
            setSuperAdminCookie($issuper);
        }
        else
        {
            $msg = "Invalid username or password.";
            // clear cookie
            clearCookies();
        }
    }
    else  
    {
        $msg = "Unable to connect to webservice.";
        // clear cookie
        clearCookies();
    }

}
?>

<html>
<head>
<?php
if ($redirect == 1) 
{
    echo("<meta http-equiv='refresh' content='2;url=menu.php'>");
}
?>
<title>Login</title></head>
<body>

Please log on:
<FORM  METHOD="POST">
<p>User: <INPUT NAME="uname"></p>
<p>Pass: <INPUT NAME="pwd" TYPE="PASSWORD"></p>
<p>Community Name (superadmin only): <INPUT NAME="community_name"></p>
<p><td align="right"><input type="submit" value="login" name="submit"></p>
</FORM>

<?php
print $msg;
?>

<P>

</body>
</html>
