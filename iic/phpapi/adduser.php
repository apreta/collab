<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Add a user
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Add User</title>
</head>
<body>

<?php showNavBar(); ?>

Enter all fields to add a new user<br>
<FORM METHOD="POST"><br>
<table>
<tr><td>User Password: </td>
<td><INPUT NAME="user_password" VALUE="<?php print ${user_password};?>"></td></tr>
<tr><td>Community Id:</td>
<td><INPUT NAME="community_id" VALUE="<?php print ${community_id};?>"></td></tr>
<tr><td>Title: </td>
<td><INPUT NAME="user_title" VALUE="<?php print ${user_title};?>"></td></tr>
<tr><td>First Name: </td>
<td><INPUT NAME="user_first" VALUE="<?php print ${user_first};?>"></td></tr>
<tr><td>Middle Name: </td>
<td><INPUT NAME="user_middle" VALUE="<?php print ${user_middle};?>"></td></tr>
<tr><td>Last Name: </td>
<td><INPUT NAME="user_last" VALUE="<?php print ${user_last};?>"></td></tr>
<tr><td>Suffix: </td>
<td><INPUT NAME="user_suffix" VALUE="<?php print ${user_suffix};?>"></td></tr>
<tr><td>Company: </td>
<td><INPUT NAME="company" VALUE="<?php print ${company};?>"></td></tr>
<tr><td>Job Title: </td>
<td><INPUT NAME="job_title" VALUE="<?php print ${job_title};?>"></td></tr>
<tr><td>Address1: </td>
<td><INPUT NAME="address1" VALUE="<?php print ${address1};?>"></td></tr>
<tr><td>Address2: </td>
<td><INPUT NAME="address2" VALUE="<?php print ${address2};?>"></td></tr>
<tr><td>State: </td>
<td><INPUT NAME="state" VALUE="<?php print ${state};?>"></td></tr>
<tr><td>Country: </td>
<td><INPUT NAME="country" VALUE="<?php print ${country};?>"></td></tr>
<tr><td>Postal Code: </td>
<td><INPUT NAME="postal_code" VALUE="<?php print ${postal_code};?>"></td></tr>
<tr><td>Screenname: </td>
<td><INPUT NAME="user_screen" VALUE="<?php print ${user_screen};?>"></td></tr>
<tr><td>Email1: </td>
<td><INPUT NAME="email1" VALUE="<?php print ${email1};?>"></td></tr>
<tr><td>Email2: </td>
<td><INPUT NAME="email2" VALUE="<?php print ${email2};?>"></td></tr>
<tr><td>Email3: </td>
<td><INPUT NAME="email3" VALUE="<?php print ${email3};?>"></td></tr>
<tr><td>Business Phone: </td>
<td><INPUT NAME="business_phone" VALUE="<?php print ${business_phone};?>"></td></tr>
<tr><td>Home Phone: </td>
<td><INPUT NAME="home_phone" VALUE="<?php print ${home_phone};?>"></td></tr>
<tr><td>Mobile Phone: </td>
<td><INPUT NAME="mobile_phone" VALUE="<?php print ${mobile_phone};?>"></td></tr>
<tr><td>Other Phone: </td>
<td><INPUT NAME="other_phone" VALUE="<?php print ${other_phone};?>"></td></tr>
<tr><td>Extension: </td>
<td><INPUT NAME="extension" VALUE="<?php print ${extension};?>"></td></tr>
<tr><td>AIM Screen Name: </td>
<td><INPUT NAME="aim_screen" VALUE="<?php print ${aim_screen};?>"></td></tr>
<tr><td>Yahoo Screen Name: </td>
<td><INPUT NAME="yahoo_screen" VALUE="<?php print ${yahoo_screen};?>"></td></tr>
<tr><td>MSN Screen Name: </td>
<td><INPUT NAME="msn_screen" VALUE="<?php print ${msn_screen};?>"></td></tr>
<tr><td>User1: </td>
<td><INPUT NAME="user1" VALUE="<?php print ${user1};?>"></td></tr>
<tr><td>User2: </td>
<td><INPUT NAME="user2" VALUE="<?php print ${user2};?>"></td></tr>
<tr><td>Default Phone: </td>
<td><INPUT NAME="default_phone" VALUE="<?php print ${default_phone};?>"></td></tr>
<tr><td>Default Email: </td>
<td><INPUT NAME="default_email" VALUE="<?php print ${default_email};?>"></td></tr>
<tr><td>Type: </td>
<td><INPUT NAME="type" VALUE="<?php print ${type};?>"></td></tr>
<tr><td>Profile ID: (blank for default)</td>
<td><INPUT NAME="profile_id" VALUE="<?php print ${profile_id};?>"></td></tr>

<tr><td colspan='2' align='right'><input type="submit" value="add user" name="submit"></td></tr>
</table>
</FORM>

<?php
$sessionid = getSessionId();

if ($HTTP_POST_VARS["community_id"]!="")
{
    // get vals from form
    $user_password = $HTTP_POST_VARS["user_password"];
    $community_id = $HTTP_POST_VARS["community_id"];
    $user_title = $HTTP_POST_VARS["user_title"];
    $user_first = $HTTP_POST_VARS["user_first"];
    $user_middle = $HTTP_POST_VARS["user_middle"];
    $user_last = $HTTP_POST_VARS["user_last"];
    $user_suffix = $HTTP_POST_VARS["user_suffix"];
    $company = $HTTP_POST_VARS["company"];
    $job_title = $HTTP_POST_VARS["job_title"];
    $address1 = $HTTP_POST_VARS["address1"];
    $address2 = $HTTP_POST_VARS["address2"];
    $state = $HTTP_POST_VARS["state"];
    $country = $HTTP_POST_VARS["country"];
    $postal_code = $HTTP_POST_VARS["postal_code"];
    $user_screen = $HTTP_POST_VARS["user_screen"];
    $email1 = $HTTP_POST_VARS["email1"];
    $email2 = $HTTP_POST_VARS["email2"];
    $email3 = $HTTP_POST_VARS["email3"];
    $business_phone = $HTTP_POST_VARS["business_phone"];
    $home_phone = $HTTP_POST_VARS["home_phone"];
    $mobile_phone = $HTTP_POST_VARS["mobile_phone"];
    $other_phone = $HTTP_POST_VARS["other_phone"];
    $extension = $HTTP_POST_VARS["extension"];
    $aim_screen = $HTTP_POST_VARS["aim_screen"];
    $yahoo_screen = $HTTP_POST_VARS["yahoo_screen"];
    $msn_screen = $HTTP_POST_VARS["msn_screen"];
    $user1 = $HTTP_POST_VARS["user1"];
    $user2 = $HTTP_POST_VARS["user2"];
    $default_phone = $HTTP_POST_VARS["default_phone"];
    if ($default_phone == "")
        $default_phone = 0;
    $default_email = $HTTP_POST_VARS["default_email"];
    if ($default_email == "")
        $default_email = 0;
    $type = $HTTP_POST_VARS["type"];
    if ($type == "")
        $type = 1;
    $profile_id = $HTTP_POST_VARS["profile_id"];

    $sessionid = getSessionId();

    $f=new xmlrpcmsg(WEBSVR_FN_ADD_USER,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($user_password, "string"),
                           new xmlrpcval($community_id, "string"),
                           new xmlrpcval($user_title, "string"),
                           new xmlrpcval($user_first, "string"),
                           new xmlrpcval($user_middle, "string"),
                           new xmlrpcval($user_last, "string"),
                           new xmlrpcval($user_suffix, "string"),
                           new xmlrpcval($company, "string"),
                           new xmlrpcval($job_title, "string"),
                           new xmlrpcval($address1, "string"),
                           new xmlrpcval($address2, "string"),
                           new xmlrpcval($state, "string"),
                           new xmlrpcval($country, "string"),
                           new xmlrpcval($postal_code, "string"),
                           new xmlrpcval($user_screen, "string"),
                           new xmlrpcval($email1, "string"),
                           new xmlrpcval($email2, "string"),
                           new xmlrpcval($email3, "string"),
                           new xmlrpcval($business_phone, "string"),
                           new xmlrpcval($home_phone, "string"),
                           new xmlrpcval($mobile_phone, "string"),
                           new xmlrpcval($other_phone, "string"),
                           new xmlrpcval($extension, "string"),
                           new xmlrpcval($aim_screen, "string"),
                           new xmlrpcval($yahoo_screen, "string"),
                           new xmlrpcval($msn_screen, "string"),
                           new xmlrpcval($user1, "string"),
                           new xmlrpcval($user2, "string"),
                           new xmlrpcval($default_phone, "string"),
                           new xmlrpcval($default_email, "string"),
                           new xmlrpcval($type, "string"),
                           new xmlrpcval($profile_id, "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode()) 
    {
        $useridobj = $v->arraymem(0);
        $userid = $useridobj->scalarval();
        print "User: " . $user_screen . "<br>New user id: " . $userid;
    }
    else  
    {
        dumpFault($r);
    }
}
?>
<?php showFooter(); ?>
</body>
</html>
