<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Find participant info based on PIN.
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Find Meetings by Host</title>
</head>
<body>

<?php showNavBar(); ?>
<?php

print "Enter a user id to look up invited meetings";
print "<FORM  METHOD=\"POST\">
<INPUT NAME=\"mid\" VALUE=\"${mid}\"><input type=\"submit\" value=\"go\" name=\"submit\"></FORM><P>";

$sessionid = getSessionId();

if ($HTTP_POST_VARS["mid"]!="") {
  $f=new xmlrpcmsg('schedule.find_meetings_by_host',
                   array(new xmlrpcval($sessionid, "string"),
                         new xmlrpcval($HTTP_POST_VARS["mid"], "string")));
  $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
  $r=$c->send($f);
  $v=$r->value();
  if (!$r->faultCode()) {
      // structure is:
      // array of values
      //   array of mtg details
      //     where each mtg is an array
      //   start row
      //   number
      //   more
      //   total
      $mtg_array = $v->arraymem(0);          // primary array
      echo("<table border=\"1\">");
      echo("<tr><th>Mtg ID</th><th>Host ID</th><th>Type</th><th>Privacy</th><th>Priority</th><th>Title</th></tr>");
      for ($i = 0; $i < $mtg_array->arraysize(); $i++)
      {
          echo("<tr>");
          $mtg = $mtg_array->arraymem($i);  // retrieve mtgs (an array)
          $mtgidobj = $mtg->arraymem(0);
          echo("<td>" . $mtgidobj->scalarval() . "</td>");
          $hostidobj = $mtg->arraymem(1);
          echo("<td>" . $hostidobj->scalarval() . "</td>");
          $typeobj = $mtg->arraymem(2);
          echo("<td>" . $typeobj->scalarval() . "</td>");
          $privobj = $mtg->arraymem(3);
          echo("<td>" . $privobj->scalarval() . "</td>");
          $priorityobj = $mtg->arraymem(4);
          echo("<td>" . $priorityobj->scalarval() . "</td>");
          $mtgnameobj = $mtg->arraymem(5);
          echo("<td>" . $mtgnameobj->scalarval() . "</td>");
      }
      echo("</table>");

	print "<HR>Participant details (XML)<BR><PRE>" .
	  htmlentities($r->serialize()). "</PRE><HR>\n";
  } else {
	print "Fault: ";
	print "Code: " . $r->faultCode() . 
	  " Reason '" .$r->faultString()."'<BR>";
  }
}
?>
<?php showFooter(); ?>
</body>
</html>
