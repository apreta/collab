<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Fetch community details
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Fetch Community</title>
</head>
<body>

<?php showNavBar(); ?>
<?php
// if community id was passed in, fill it into the form
$commid = ($_GET['commid']);
if ($commid != "")
{
    $community_id = $commid;
}
?>

Enter a community id to fetch the community details<br>
<FORM METHOD="POST"><br>
<table>
<tr><td>Community Id:</td>
<td><INPUT NAME="community_id" VALUE="<?php print ${community_id};?>"></td></tr>
<tr><td colspan='2' align='right'><input type="submit" value="fetch community" name="submit"></td></tr>
</table>
</FORM>

<?php
$sessionid = getSessionId();

if ($HTTP_POST_VARS["community_id"]!="")
{
    // get vals from form
    $community_id = $HTTP_POST_VARS["community_id"];

    $sessionid = getSessionId();

    $f=new xmlrpcmsg(WEBSVR_FN_FETCH_COMMUNITY,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($community_id, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $sessionid = getSessionId();

    $f=new xmlrpcmsg(WEBSVR_FN_FETCH_COMMUNITY,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($community_id, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode()) 
    {
        $comm_array = $v->arraymem(0);
        $commobj = $comm_array->arraymem(0);
        $commidobj = $commobj->arraymem(0);
        echo("<table>");
        echo("<tr><td>community id:</td><td>" . $commidobj->scalarval() . "</td></tr>");

        $commnameobj = $commobj->arraymem(1);
        echo("<tr><td>community name: </td><td>" . $commnameobj->scalarval() . "</td></tr>");

        $acctnumobj = $commobj->arraymem(2);
        echo("<tr><td>account number: </td><td>" . $acctnumobj->scalarval() . "</td></tr>");

        $contactnameobj = $commobj->arraymem(3);
        echo("<tr><td>contact name: </td><td>" . $contactnameobj->scalarval() . "</td></tr>");

        $contactemailobj = $commobj->arraymem(4);
        echo("<tr><td>contact email: </td><td>" . $contactemailobj->scalarval() . "</td></tr>");

        $contactphoneobj = $commobj->arraymem(5);
        echo("<tr><td>contact phone: </td><td>" . $contactphoneobj->scalarval() . "</td></tr>");

        $usernameobj = $commobj->arraymem(6);
        echo("<tr><td>user name: </td><td>" . $usernameobj->scalarval() . "</td></tr>");

        $userpasswordobj = $commobj->arraymem(7);
        echo("<tr><td>user password: </td><td>" . $userpasswordobj->scalarval() . "</td></tr>");

        $bridgephoneobj = $commobj->arraymem(8);
        echo("<tr><td>bridge phone: </td><td>" . $bridgephoneobj->scalarval() . "</td></tr>");

        $systemurlobj = $commobj->arraymem(9);
        echo("<tr><td>system url: </td><td>" . $systemurlobj->scalarval() . "</td></tr>");

        $accounttypeobj = $commobj->arraymem(10);
        echo("<tr><td>account type: </td><td>" . $accounttypeobj->scalarval() . "</td></tr>");

        $accountstatusobj = $commobj->arraymem(11);
        echo("<tr><td>account status: </td><td>" . $accountstatusobj->scalarval() . "</td></tr>");

        $optionsobj = $commobj->arraymem(12);
        echo("<tr><td>options: </td><td>" . $optionsobj->scalarval() . "</td></tr>");
        echo("<tr><td colspan=2><a href=\"updatecommunity.php?commid=" . $commidobj->scalarval() . "\">update this community</a></td></tr>");
        echo("</table>");
    }
    else  
    {
        dumpFault($r);
    }
}
?>
<?php showFooter(); ?>
</body>
</html>
