// maximizes the browser window
function maximize()
{
    if (window.width < screen.availWidth || window.height < screen.availHeight)
    {
        top.window.moveTo(0,0);
        if (document.all) 
        {
            top.window.resizeTo(screen.availWidth,screen.availHeight);
        }
        else if (document.layers||document.getElementById) 
        {
            if (top.window.outerHeight<screen.availHeight || 
                top.window.outerWidth<screen.availWidth)
            {
	        top.window.outerHeight = screen.availHeight;
                top.window.outerWidth = screen.availWidth;
            }
        }
    }
}
