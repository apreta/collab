<?php

//
// Copyright 2004 Imidio, Inc.
//

//
// Meeting decode module:
//  - read and decode args (meeting id and participant id)
//  - render Help html
//

include("inc/conf.inc");

//
// decode input
//  - form is frame_invite.php?mid=<base64encodedarg>
//
$args_valid = 0;
$arg = ($_GET['mid']);
if (strlen($arg) > 0)
{
    // base 64 decode arg
    $decoded_arg = base64_decode($arg);

    //
    // parse result into mtg id and participant id
    //

    // init
    $mid_delim = "m_id=";
    $pid_delim = "p_id=";
    $mid_pos = -1;
    $pid_pos = -1;
    $delim_pos = -1;

    // parse
    $mid_pos   = strpos($decoded_arg, $mid_delim);
    $pid_pos   = strpos($decoded_arg, $pid_delim);
    $delim_pos = strpos($decoded_arg, "&");

    if ($mid_pos >= 0 && $pid_pos > 0 && $delim_pos > 0)
    {
        $mid = substr($decoded_arg, $mid_pos + strlen($mid_delim), $delim_pos - $mid_pos - strlen($mid_delim));
        $pid = substr($decoded_arg, $delim_pos + 1 + strlen($pid_delim), strlen($decoded_arg) - $mid_pos);
        if (strlen($mid) > 0 && strlen($pid) > 0)
            $args_valid = 1;
    }
}

    $IS_LINUX    = stristr($_SERVER["HTTP_USER_AGENT"], "Linux ");
    $IS_64BIT	 = stristr($_SERVER["HTTP_USER_AGENT"], "x86_64");
    $IS_MAC      = stristr($_SERVER["HTTP_USER_AGENT"], "Macintosh");

    if (!$IS_LINUX && !$IS_MAC)
    {
        // dynamically generated URL for installer
        $DYNAMIC_INSTALLER = DYNAMIC_INSTALLER_STR . "?mid=" . $mid . "&pid=" . $pid;
        include("invite_help.html");
    }
    else 
    {
        // not dynamic for non-windows yet; user must enter pin
        if ($IS_MAC) 
        {
            $DYNAMIC_INSTALLER = DMG_URL_STR;
            include("invite_help_mac.html");
        }
        else
        {
            if ($IS_64BIT)
        	    $DYNAMIC_INSTALLER = RPM_64BIT_URL_STR;
            else
        	    $DYNAMIC_INSTALLER = RPM_URL_STR;
            include("invite_help_linux.html");
        }
    }
?>

