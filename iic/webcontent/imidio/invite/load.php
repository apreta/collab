<?php

//
// Copyright 1994 Imidio, Inc.
//

//
// Meeting decode module:
//  - determine availability of activex, downloads, and scripting
//  - make xmlrpc call to find_invite
//  - render html template page with results
//

// Insert header to turn off caching for this page
header("Expires: Mon, 26 Jul 2001 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("Cache-control: private");

//
// decode input
//  - form is load.php?mid=<base64encodedarg>
//
$args_valid = 0;
$arg = ($_GET['mid']);

include("inc/conf.inc");

$FRAME_INVITE = FRAME_INVITE_STR . "?mid=" . $arg;
$BRAND_SIZE = BRAND_PAGE_SIZE;
$BRAND_PAGE_URL = BRAND_PAGE_URL_STR;
?>
<HTML>
<HEAD>
    <TITLE>Meeting Invitation</TITLE>
</HEAD>

<SCRIPT LANGUAGE="JavaScript">
  function refreshFrame() {
      var ActiveX_Available = false;
      var Force_Upgrade = false;

      try
      {
          var objectPlugin = new ActiveXObject("IICPLUGIN.IICPluginCtrl.1");

          try
          {
              objectPlugin.InstallTest;
              Force_Upgrade = true;
          }
          catch (e_property_test)
          {
              ActiveX_Available = true;
          }

          delete objectPlugin;
      }
      catch(e)
      {
          // ActiveX is not installed, is disabled, or user didn't grant a permission
      }

      if (Force_Upgrade)
      {
          document.getElementById('frame2').src = "<?=$FRAME_INVITE?>&act=2";
      }
      else if (ActiveX_Available)
      {
          document.getElementById('frame2').src = "<?=$FRAME_INVITE?>&act=1";
      }    
  }  
</SCRIPT>

<FRAMESET id="FrameSet" rows="<?=$BRAND_SIZE?>,*" frameborder="0" border="0" onLoad="refreshFrame();">
    <FRAME NAME=frame1 SRC="<?=$BRAND_PAGE_URL?>" scrolling="no">
    <FRAME NAME=frame2 SRC="<?=$FRAME_INVITE?>&act=0">
</FRAMESET>
</HTML>
