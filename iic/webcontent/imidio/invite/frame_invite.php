<?php

//
// Copyright 2004 Imidio, Inc.
//

//
// Meeting decode module:
//  - read and decode args (meeting id and participant id)
//  - make xmlrpc call to find_invite
//  - render html
//

//
// decode input
//  - form is frame_invite.php?mid=<base64encodedarg>
//
$args_valid = 0;
$arg = ($_GET['mid']);
$arg2 = ($_GET['act']);
if (strlen($arg) > 0)
{
    // base 64 decode arg
    $decoded_arg = base64_decode($arg);

    //
    // parse result into mtg id and participant id
    //

    // init
    $mid_delim = "m_id=";
    $pid_delim = "p_id=";
    $mid_pos = -1;
    $pid_pos = -1;
    $delim_pos = -1;

    // parse
    $mid_pos   = strpos($decoded_arg, $mid_delim);
    $pid_pos   = strpos($decoded_arg, $pid_delim);
    $delim_pos = strpos($decoded_arg, "&");

    if ($mid_pos >= 0 && $pid_pos > 0 && $delim_pos > 0)
    {
        $mid = substr($decoded_arg, $mid_pos + strlen($mid_delim), $delim_pos - $mid_pos - strlen($mid_delim));
        $pid = substr($decoded_arg, $delim_pos + 1 + strlen($pid_delim), strlen($decoded_arg) - $mid_pos);
        if (strlen($mid) > 0)
            $args_valid = 1;
    }
}

//
// make xmlrpc call to find_invite
//
if ($args_valid == 1)
{
    include("inc/xmlrpc.inc");
    include("inc/common.inc");

    $meeting_found = 0;
    $f=new xmlrpcmsg('controller.find_invite',
                     array(new xmlrpcval("sys:invite", "string"),
                           new xmlrpcval($mid, "string"),
                           new xmlrpcval($pid, "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode())
    {
        //   "FOUND", "MEETINGID", "TITLE", "TIME", "HOST", "SERVER",
        //   "PARTICIPANTID", "SCREENNAME", "NAME", "OPTIONS", "DESCRIPTION",
        //   "MEETING INVITATION", "PASSWORD", "PARTICIPANT PHONE", "PARTICIPANT EMAIL"

        $meeting_found_obj = $v->arraymem(0);
        $meeting_found     = $meeting_found_obj->scalarval();

        // If found is 1 or 2, we are good to go
        if ($meeting_found != 0)
        {
            $mid_obj  = $v->arraymem(1);
            $mid      = $mid_obj->scalarval();

            $topic_obj = $v->arraymem(2);
            $topic     = $topic_obj->scalarval();

            $starttime_obj = $v->arraymem(3);
            $starttime     = $starttime_obj->scalarval();
            if ($starttime == "0")
                $starttime = "Instant";

            $host_obj = $v->arraymem(4);
            $host     = $host_obj->scalarval();

            $server_obj = $v->arraymem(5);
            $server     = $server_obj->scalarval();

            $pid_obj    = $v->arraymem(6);
            $pid        = $pid_obj->scalarval();

            $screen_obj = $v->arraymem(7);
            $screen     = $screen_obj->scalarval();

            $part_obj   = $v->arraymem(8);
            $part       = $part_obj->scalarval();

            $options_obj = $v->arraymem(9);
            $options     = $options_obj->scalarval();

            $desc_obj   = $v->arraymem(10);
            $desc       = $desc_obj->scalarval();

            $pwd_obj    = $v->arraymem(12);
            $pwd        = $pwd_obj->scalarval();

            $phone_obj  = $v->arraymem(13);
            $phone      = $phone_obj->scalarval();

            $email_obj  = $v->arraymem(14);
            $email      = $email_obj->scalarval();
        }
    }
    else
    {
        // error from xmlrpc call
        $meeting_found = -1;
    }
}


//
// render results
//

// if invalid args, error page
if ($args_valid != 1)
{
    include("error_nomeeting.html");
}
else if ($meeting_found == -1)
{
    // error with rpc call
    include("error_nomeeting.html");
}
else if ($meeting_found == 0)
{
    // meeting not found page
    include("error_nomeeting.html");
}
else
{
    include("inc/conf.inc");

    //
    // store values in template tags
    //
    $MID         = $mid;
    $TOPIC       = $topic;
    $START       = $starttime;
    $HOST        = $host;
    $SERVER      = $server;
    $PIN         = $pid;
    $SCREEN      = $screen;
    $PARTICIPANT = $part;
    $OPTIONS     = $options;
    $PASSWORD    = $pwd;
    $PHONE       = $phone;
    $EMAIL       = $email;

    $IS_IIC_INSTALLED = $arg2 == 1;
    $FORCE_UPGRADE = $arg2 == 2;

    if (!$FORCE_UPGRADE)
    {
        $SEARCH_PREFIX = "application/iic-";
        $SEARCH_TOKEN  = $SEARCH_PREFIX . SERVER_TOKEN;
        $ACCEPT_ARRAY = explode(",", $_SERVER["HTTP_ACCEPT"]);

        for($i=0; $i < count($ACCEPT_ARRAY); $i++)
        {
            if (strcasecmp(trim($ACCEPT_ARRAY[$i]),"application/iic") == 0)
            {
                // Obsolete accept type.  Force an upgrade.
                $FORCE_UPGRADE = true;
            }
            else
            {
                $POS = strpos(trim($ACCEPT_ARRAY[$i]),$SEARCH_PREFIX);
            
                if ($POS !== false)
                {
                    if (strcasecmp(trim($ACCEPT_ARRAY[$i]),$SEARCH_TOKEN) == 0)
                    {
                        $IS_IIC_INSTALLED = true;
                    }
                    else
                    {
                        $FORCE_UPGRADE = true;
                    }
                }
            }
        }
    }

    $IS_MSIE     = stristr($_SERVER["HTTP_USER_AGENT"], "MSIE ");
    $IS_LINUX    = stristr($_SERVER["HTTP_USER_AGENT"], "Linux ");
    $IS_64BIT	 = stristr($_SERVER["HTTP_USER_AGENT"], "x86_64");
    $IS_MAC      = stristr($_SERVER["HTTP_USER_AGENT"], "Macintosh");

    // activex object constants
    $CLASSID     = CLASS_ID_STR;
    $ACTIVEXCTL  = ACTIVEXCTRL_SOURCE_STR;
    $INSTALLER   = INSTALLER_URL_STR;

    if (!$IS_LINUX && !$IS_MAC)
    {
        // dynamically generated URL for installer
        $DYNAMIC_INSTALLER = DYNAMIC_INSTALLER_STR . "?mid=" . $MID . "&pid=" . $PIN;
    }
    else 
    {
        // not dynamic for non-windows yet; user must enter pin
        if ($IS_MAC) 
        {
            $DYNAMIC_INSTALLER = DMG_URL_STR;
        }
        else 
        {
            if ($IS_64BIT)
                $DYNAMIC_INSTALLER = RPM_64BIT_URL_STR;
            else
                $DYNAMIC_INSTALLER = RPM_URL_STR;
        }
    }
    
    // dynamically generate protocol launcher
    $DYNAMIC_PROTOCOL = "iic:launch?mid=" . $MID . "&pid=" . $PIN . "?screen="
    . $SCREEN . "?server=" . $SERVER . "?fullname=" . "&quot;" . $PARTICIPANT . "&quot;";

    // dynamically generate help url
    $DYNAMIC_HELP = INVITE_HELP . "?mid=" . $arg;

    // Insert header to turn off caching for this page
    header("Expires: Mon, 26 Jul 2001 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header("Cache-control: private");

    if ($FORCE_UPGRADE)
    {
        if ($IS_LINUX)
        {
           include("invite_help_linux.html");
        }
        else
        {
           include("invite_help.html");
        }
    }
    else if ($IS_MSIE && $arg2 == 1)
    {
        if ($IS_IIC_INSTALLED)
        {
            include("invite_installed_ie.html");
        }
        else
        {
            include("invite_uninstalled_ie.html");
        }
    }
    else
    {
        if ($IS_LINUX)
        {
            include("invite_linux.html");
        }
        else if ($IS_MAC)
        {
            include("invite_mac.html");            
        }
        else
        {
            include("invite_other.html");
        }
    }
}

?>

