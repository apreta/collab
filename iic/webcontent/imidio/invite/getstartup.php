<?php

//
// Copyright 1994 Imidio, Inc.
//

//
// Meeting decode module:
//  - authorize if needed
//  - read and decode args (meeting id and participant id)
//  - make xmlrpc call to find_invite
//  - render html template page with results
//

//
// authorize if needed
//

//
// decode input
//  - form is getstartup.php?mid=840&pid=841
//

$args_valid = 0;
$mid = ($_GET['mid']);
$pid = ($_GET['pid']);
$opts = (int)(@$_GET['opts']);
if (strlen($mid) > 0)
     $args_valid = 1;

//
// make xmlrpc call to find_invite
//
if ($args_valid == 1)
{
    include("inc/xmlrpc.inc");
    include("inc/common.inc");

    if ($opts)
    {
		$params = array(new xmlrpcval("sys:invite", "string"),
                           new xmlrpcval($mid, "string"),
                           new xmlrpcval($pid, "string"),
                           new xmlrpcval($opts, "i4"));
	}
	else 
	{
		$params = array(new xmlrpcval("sys:invite", "string"),
                           new xmlrpcval($mid, "string"),
                           new xmlrpcval($pid, "string"));
    }
		

    $f=new xmlrpcmsg('controller.find_invite', $params);
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    $meeting_found = 0;
    if (!$r->faultCode())
    {
        // return result in xml form
        $xmlresult = $r->serialize();

        header("Expires: Mon, 26 Jul 2001 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        header("Cache-control: private");
        header("Content-Length: " . strlen($xmlresult));

        print $xmlresult;
    }
}
?>
