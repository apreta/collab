<?php

//
// Copyright 2015 Apreta
//


$args_valid = 0;
$pin = (@$_GET['pin']);
$password = (@$_GET['pw']);
if ($pin && is_numeric($pin) && $password)
     $args_valid = 1;

//
// make xmlrpc call to find_user_by_pin
//
if ($args_valid == 1) {
    include("inc/xmlrpc.inc");
    include("inc/common.inc");

    $f=new xmlrpcmsg('addressbk.find_user_by_pin',
                     array(new xmlrpcval("sys:invite", "string"),
                           new xmlrpcval($pin, "string"),
                           new xmlrpcval($password, "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode())
    {
	    $v=$r->value();
		//print_r($v);
		$username = $v->arraymem(1)->scalarval();

        echo "FOUND:$username";
    } else {
		sleep(1);
		echo "NOTFOUND";
	}
} else {
	echo "ERROR";
}
?>
