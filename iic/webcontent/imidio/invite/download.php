<?php

//
// Copyright 1994 Imidio, Inc.
//

//
// Dynamic download module:
//  - forces download of installer file (imidio.exe) but
//  names it dynamically based on provided args (mid and pid)
//  - example: download.php?mid=840&pid=841 will return file name
//  imidio.840.841.exe
//

include("inc/conf.inc");

$MID_STR = ".";
$PID_STR = ".";
$mid = ($_GET['mid']);
$pid = ($_GET['pid']);

$download_directory = INSTALLER_DOWNLOAD_DIR;
$fileprefix = INSTALLER_DOWNLOAD_FILENAME;
$filesuffix = ".exe";
$filename = $fileprefix . $filesuffix;

// verify file existance
$system_error = 0;
if (!is_dir($download_directory))
{
    $system_error = 1;
    $system_message = "Error: bad download directory";
}
else if (!is_file($download_directory . $filename))
{
    $system_error = 1;
    $system_message = "Error: bad download file";
}

// download file if valid
if ($system_error == 0)
{
    $filesize = filesize($download_directory . $filename);

    // rename if args provided
    if (strlen($mid) > 0)
    {
        $new_filename = $fileprefix . $MID_STR . $mid . $PID_STR . $pid . $filesuffix;
    }
    else
    {
        $new_filename = $filename;
    }

    header("Expires: Mon, 26 Jul 2001 05:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    //    header("Pragma: no-cache");   <-- setting this flag causes downloads to fail when ssl is enabled
    header("Cache-control: private");
    header("Content-Length: " . $filesize);
    header("Content-Type: application/force-download");
    header("Content-Type: application/download");
    header("Content-Type: application/octet-stream");
    header("Content-Disposition: attachment; filename=$new_filename");

    @readfile($download_directory . $filename);
}
else
{
    echo($system_message . "<br>");
    echo("path is " . $download_directory . "<br>");
    echo("filename is " . $filename . "<br>");
}

?>
