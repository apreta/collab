<?php

//
// Copyright 2004 Imidio, Inc.
//

//
// Constants and utilities
//

// webservice domain
define("WEB_SERVICE_DOMAIN", "192.168.1.10");

// webservice port
define("WEB_SERVICE_PORT", 8000);

// webservice URI
// use this URI to look things up 
define("WEB_SERVICE_URI", "/imidio_api/find.php");
// this URI will return the xmlrpc request back as the response for debugging purposes
//define("WEB_SERVICE_URI", "/imidio_api/debug.php");

// cookie names
define("SESSION_ID_COOKIE_NAME", "imidiosessionid");
define("ISSUPER_COOKIE_NAME", "imidioissuper");

// default chunk size
define("DEFAULT_CHUNK_SIZE", 5);

// verifies that the user has valid sessionid.  if not, redirects to login page
function checkAuth()
{
    $sessionid = $_COOKIE[SESSION_ID_COOKIE_NAME];
    if (!($sessionid > 0))
    {
        echo("<meta http-equiv='refresh' content='0;url=/imidio/console/php/logout.php'>");
    }
}

// sets the session cookie
function setSessionCookie($sessionid)
{
    $cookieset = setcookie (SESSION_ID_COOKIE_NAME, $sessionid, time()+3600, '/'); 
}

// sets the superadmin cookie
function setSuperAdminCookie($issuper)
{
    $cookieset = setcookie (ISSUPER_COOKIE_NAME, $issuper, time()+3600, '/');
}

function clearCookies()
{
    $cookieset = setcookie (SESSION_ID_COOKIE_NAME, "", time()-3600, '/');
    $cookieset = setcookie (ISSUPER_COOKIE_NAME, "", time()-3600, '/');
}

// retrieves the session id from the cookie
function getSessionId()
{
    return $_COOKIE[SESSION_ID_COOKIE_NAME];
}

// returns true if user is super admin
function isSuper()
{
    $issuper = $_COOKIE[ISSUPER_COOKIE_NAME];
    if ($issuper == "t")
        return True;
    return False;
}

// clears the browser cache
function clearCache()
{
    header("Expires: Mon, 26 Jul 2001 05:00:00 GMT");               // Date in the past
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");  // always modified
    header("Cache-Control: no-store, no-cache, must-revalidate");   // HTTP/1.1
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");                                     // HTTP/1.0
    header("Cache-control: private");                               // special magic
}

function showNavBar1()
{
    echo("<table>
            <tr>
              <td width=12% valign=\"top\">
                <table bgcolor=\"#f0f0f0\">
                  <tr><td class=title>Cluster Status
                  <ul>
                  <li><a href=\"getstatussummary.php\">services</a>
                  <li><a href=\"getmeetings.php\">meetings</a>
                  </ul>
                  </td></tr>
                  <tr><td class=title><a href=\"login.php?logout=1\" target=dashboard>Logout</a></td></tr>
                </table>
              </td>
              <td width=\"400\" valign=\"top\">"
         );
}

function showNavBar2()
{
    echo("<table>
            <tr>
              <td width=12% valign=\"top\">
                <table bgcolor=\"#f0f0f0\">
                  <tr><td class=title>Communities
                  <ul>
                  <li><a href=\"fetchcommunitylist.php\">show all</a>
                  <li><a href=\"searchcommunities.php\">search</a>
                  <li><a href=\"addcommunity.php\">add</a>
                  <li><a href=\"removecommunity.php\">remove</a>
                  <li><a href=\"updatecommunity.php\">update</a>
                  <li><a href=\"fetchcommunity.php\">get details</a>
                  </ul>
                  </td></tr>
                  <tr><td class=title>Users
                  <ul>
                  <li><a href=\"searchusers.php\">search</a>
                  <li><a href=\"adduser.php\">add</a>
                  <li><a href=\"removeuser.php\">remove</a>
                  <li><a href=\"setpassword.php\">set password</a>
                  <li><a href=\"updateuser.php\">update</a>
                  </ul>
                  </td></tr>
                  <tr><td class=title>Meetings
                  <ul>
                  <li><a href=\"findmeetingsbyhost.php\">by host</a>
                  <li><a href=\"findmeetingsinvited.php\">invited</a>
                  <li><a href=\"findmeetingspublic.php\">public</a>
                  </ul>
                  </td></tr>
                  <tr><td class=title><a href=\"login.php?logout=1\" target=dashboard>Logout</a></td></tr>
                </table>
              </td>
              <td width=\"400\" valign=\"top\">"
         );
}

function showNavBar3()
{
    echo("<table>
            <tr>
              <td width=12% valign=\"top\">
                <table bgcolor=\"#f0f0f0\">
                  <tr><td class=title>Metrics
                  <ul>
                  <li><a href=\"getmetrics.php?type=1&number=100\">meetings</a>
                  <li><a href=\"getmetrics.php?type=2&number=100\">ports</a>
                  <li><a href=\"getmetrics.php?type=3&number=100\">users</a>
                  <li><a href=\"getmetrics.php?type=4&number=100\">share sessions</a>
                  </ul>
                  </td></tr>
                  <tr><td class=title><a href=\"login.php?logout=1\" target=dashboard>Logout</a></td></tr>
                </table>
              </td>
              <td width=\"400\" valign=\"top\">"
         );
}

// render a navigation bar and beginning of a table cell for the rest of the 
// content
function showNavBar()
{
    echo("<table>
            <tr>
              <td width=100 valign=\"top\">
                <table bgcolor=\"#f0f0f0\">
                  <tr><td><a href=\"login.php?logout=1\">logout</a></td></tr>
                  <tr><td><a href=\"menu.php\">main menu</a></td></tr>
                </table>
              </td>
              <td width=\"400\" valign=\"top\">"
         );
}

// close out the content table cell and the main page table
function showFooter()
{
    echo("</td></tr>
         </table>");
}

// dumps out fault code and string
function dumpFault($r)
{
	print "Fault: ";
	print "Code: " . $r->faultCode() . "<BR>" .
            " Reason: '" .$r->faultString()."'<BR>";
}

// dumps out xmlrpc response
function dumpResponse($r)
{
	print "<HR>XMLRPC response (XML)<BR><PRE>" .
            htmlentities($r->serialize()). "</PRE><HR>\n";
}

// date time conversion
function rfcdate ($tn) { 
// Translated from imap-4.7c/src/osdep/unix/env_unix.c 
// env-unix.c is Copyright 2000 by the University of Washington 
// localtime() not available in PHP3... 
//   $tn = time(0); 
   $zone = gmdate("H", $tn) * 60 + gmdate("i", $tn); 
   $julian = gmdate("z", $tn); 
   $t = getdate($tn); 
   $zone = $t[hours] * 60 + $t[minutes] - $zone; 

//   echo("tn: " . $tn . " zone: " . $zone . " julian " . $julian . " t " . $t . " zone " . $zone);

   // 
   // julian can be one of: 
   //  36x  local time is December 31, UTC is January 1, offset -24 hours 
   //    1  local time is 1 day ahead of UTC, offset +24 hours 
   //    0  local time is same day as UTC, no offset 
   //   -1  local time is 1 day behind UTC, offset -24 hours 
   // -36x  local time is January 1, UTC is December 31, offset +24 hours 
   // 
   if ($julian = $t[yday] - $julian) { 
   $zone += (($julian < 0) == (abs($julian) == 1)) ? -24*60 : 24*60; 
   } 

   return date('D, d M Y H:i:s ', $tn) . sprintf("%03d%02d", $zone/60, abs($zone) % 60) . " (" . strftime("%Z") . ")"; 
} 

///////////////////////////////////////////////////////////////////////
// WebService function definitions
///////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////
// authenticate_user
// @param       username
// @param       password
// @param       communityname (for superadmins only, ignored otherwise)
//
// @return      SESSIONID
// @return      USERID
// @return      USERNAME
// @return      COMMUNITYID
// @return      DIRECTORYID (for personal directory)
// @return      DEFAULTGROUPID (for personal directory)
// @return      USERTYPE
// @return      ISSUPERADMIN
// @return      ENABLEDFEATURES
// @return      PROFILEOPTIONS
// @return      DEFPHONE
// @return      BUSPHONE
// @return      HOMEPHONE
// @return      MOBILEPHONE
// @return      OTHERPHONE
// @return      INSTANTMEETINGID
// @return      FIRSTNAME
// @return      LASTNAME
// @return      SYSTEMPHONE
// @return      SYSTEMURL
// @note        If returned USERID is empty string, user was not found or
//              password is incorrect. 
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_AUTH_USER", 'addressbk.authenticate_user');

///////////////////////////////////////////////////////////////////////
// add_community
// @param       SESSIONID
// @param       COMMUNITYNAME
// @param       ACCOUNTNUMBER
// @param       CONTACTNAME
// @param       CONTACTEMAIL
// @param       CONTACTPHONE
// @param       USERNAME
// @param       USERPASSWORD
// @param       USERISADMIN (0 = normal user, 1 = admin)
// @param       BRIDGEPHONE
// @param       SYSTEMURL
// @param       OPTIONS
// @param       DEFAULTPROFILEID (blank uses internal defaults)
//
// @return      COMMUNITYID
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_ADD_COMMUNITY", 'addressbk.add_community');

///////////////////////////////////////////////////////////////////////
// remove_community
// @param       SESSIONID
// @param       COMMUNITYID
//
// @return      NUMBER OF ROWS AFFECTED
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_REMOVE_COMMUNITY", 'addressbk.remove_community');

///////////////////////////////////////////////////////////////////////
// update_community
// @param       SESSIONID
// @param       COMMUNITYID
// @param       COMMUNITYNAME
// @param       ACCOUNTNUMBER
// @param       CONTACTNAME
// @param       CONTACTEMAIL
// @param       CONTACTPHONE
// @param       USERNAME
// @param       USERPASSWORD
// @param       BRIDGEPHONE
// @param       SYSTEMURL
// @param       OPTIONS
// @param       DEFAULTPROFILEID (???)
//
// @return      NUMBER OF ROWS AFFECTED
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_UPDATE_COMMUNITY", 'addressbk.update_community');

///////////////////////////////////////////////////////////////////////
// enable_community
// @param       SESSIONID
// @param       COMMUNITYID
// @param       ENABLE (0 for disable, non-zero to enable)
//
// @return      NUMBER OF ROWS AFFECTED
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_ENABLE_COMMUNITY", 'addressbk.enable_community');

///////////////////////////////////////////////////////////////////////
// fetch_community_details
// @param      SESSIONID
// @param      COMMUNITYID
//
// @return     COMMUNITYID
// @return     COMMUNITYNAME
// @return     ACCOUNTNUMBER
// @return     CONTACTNAME
// @return     CONTACTEMAIL
// @return     CONTACTPHONE
// @return     ADMINNAME
// @return     ADMINPASSWORD
// @return     BRIDGEPHONE
// @return     SYSTEMURL
// @return     ACCOUNTTYPE
// @return     ACCOUNTSTATUS
// @return     OPTIONS
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_FETCH_COMMUNITY", 'addressbk.fetch_community_details');

///////////////////////////////////////////////////////////////////////
// fetch_community_list
// @param       SESSIONID
// @param       FIRST (# of first row to fetch)
// @param       CHUNKSIZE (# of rows to fetch)
// --- return array of community data ----
// @return     COMMUNITYID
// @return     COMMUNITYNAME
// @return     ACCOUNTNUMBER
// @return     CONTACTNAME
// @return     CONTACTEMAIL
// @return     CONTACTPHONE
// @return     USERNAME
// @return     USERPASSWORD
// @return     BRIDGEPHONE
// @return     SYSTEMURL
// @return     ACCOUNTTYPE     /* 0 = normal account, 1 = system account */
// @return     ACCOUNTSTATUS   /* 0 = disabled, 1 = enabled */
// @return     OPTIONS
// ---  followed by ---
// @return     FIRST
// @return     CHUNKSIZE
// @return     TOTALROWS
// @return     MOREAVAILABLE
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_FETCH_COMMUNITY_LIST", 'addressbk.fetch_community_list');

///////////////////////////////////////////////////////////////////////
// search_communities
// @param       SESSIONID
// @param       SEARCHSTRING
// @param       FIRST (# of first row to fetch)
// @param       CHUNKSIZE (# of rows to fetch)
// --- return array of community data ----
// @return     COMMUNITYID
// @return     COMMUNITYNAME
// @return     ACCOUNTNUMBER
// @return     CONTACTNAME
// @return     CONTACTEMAIL
// @return     CONTACTPHONE
// @return     USERNAME
// @return     USERPASSWORD
// @return     BRIDGEPHONE
// @return     SYSTEMURL
// @return     ACCOUNTTYPE
// @return     ACCOUNTSTATUS
// @return     OPTIONS
// ---  followed by ---
// @return     FIRST
// @return     CHUNKSIZE
// @return     TOTALROWS
// @return     MOREAVAILABLE
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_SEARCH_COMMUNITIES", 'addressbk.search_communities');

///////////////////////////////////////////////////////////////////////
// add_user
// @param       SESSIONID
// @param       PASSWORD
// @param       COMMUNITYID
// @param       TITLE
// @param       FIRSTNAME
// @param       MIDDLENAME
// @param       LASTNAME
// @param       SUFFIX
// @param       COMPANY
// @param       JOBTITLE
// @param       ADDRESS1
// @param       ADDRESS2
// @param       STATE
// @param       COUNTRY
// @param       POSTALCODE
// @param       SCREENNAME
// @param       EMAIL
// @param       EMAIL2
// @param       EMAIL3
// @param       BUSPHONE
// @param       HOMEPHONE
// @param       MOBILEPHONE
// @param       OTHERPHONE
// @param       EXTENSION
// @param       AIMSCREEN
// @param       YAHOOSCREEN
// @param       MSNSCREEN
// @param       USER1
// @param       USER2
// @param       DEFPHONE:  0 for none, 1 for busphone, 2 for homephone, 
//              3 for mobilephone, 4 for otherphone
// @param       DEFEMAIL:  0 for none, 1 for email1, 2 for email2, 3 for email3
// @param       TYPE:  1 for registered user, 2 for admin
// @param       PROFILEID (blank for default)
//
// @return      USERID
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_ADD_USER", 'addressbk.add_user');

///////////////////////////////////////////////////////////////////////
// find_user
// @param       SESSIONID
// @param       USERID
//
// @return      COMMUNITYID
// @return      SCREENNAME
// @return      IS_ADMIN (T|F)
// @return      IS_SUPERADMIN (T|F)
// @return      USERNAME
// @return      FIRSTNAME
// @return      LASTNAME
// @return      DEFPHONE
// @return      BUSPHONE
// @return      HOMEPHONE
// @return      MOBILEPHONE
// @return      OTHERPHONE
// @return      EXTENSION
// @return      DEFEMAIL
// @return      EMAIL
// @return      EMAIL2
// @return      EMAIL3
// @return      ENABLEDFEATURES
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_FIND_USER", 'addressbk.find_user');

///////////////////////////////////////////////////////////////////////
// search_users
// @param      SESSIONID
// @param      COMMUNITYID
// @param      USERNAME
// @param      EMAIL
// @param      FIRST (# of first row to fetch)
// @param      CHUNKSIZE (# of rows to fetch)
// --- return array of user data ----
// @return     USERID
// @return     COMMUNITYID
// @return     SCREENNAME
// @return     ADMIN (t if admin)
// @return     SUPERADMIN (t if superadmin)
// @return     PASSWORD
// @return     USERTYPE
// @return     FIRSTNAME
// @return     LASTNAME
// @return     DEFPHONE
// @return     BUSPHONE
// @return     HOMEPHONE
// @return     MOBILEPHONE
// @return     OTHERPHONE
// @return     EMAIL (primary)
// ---  followed by ---
// @return     FIRST
// @return     CHUNKSIZE
// @return     TOTALROWS
// @return     MOREAVAILABLE
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_SEARCH_USERS", 'addressbk.search_users');

///////////////////////////////////////////////////////////////////////
// set_password
// @param      SESSIONID
// @param      USERID
// @param      OLDPASSWORD
// @param      NEWPASSWORD
// @param      ADMINOVERRIDE
//
// @return     CHANGED (true if password changed)
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_SET_PASSWORD", 'addressbk.set_password');

///////////////////////////////////////////////////////////////////////
// remove_user
// @param       SESSIONID
// @param       USERID
//
// @return      NUMBER OF ROWS AFFECTED
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_REMOVE_USER", 'addressbk.remove_contact');

///////////////////////////////////////////////////////////////////////
// update_user
// @param       SESSIONID
// @param       PASSWORD
// @param       USERID
// @param       COMMUNITYID
// @param       DIRECTORYID
// @param       USERNAME
// @param       SERVER
// @param       TITLE
// @param       FIRSTNAME
// @param       MIDDLENAME
// @param       LASTNAME
// @param       SUFFIX
// @param       COMPANY
// @param       JOBTITLE
// @param       ADDRESS1
// @param       ADDRESS2
// @param       STATE
// @param       COUNTRY
// @param       POSTALCODE
// @param       SCREENNAME
// @param       reserved
// @param       EMAIL
// @param       EMAIL2
// @param       EMAIL3
// @param       BUSPHONE
// @param       reserved (BUSDIRECT)
// @param       HOMEPHONE
// @param       reserved (HOMEDIRECT)
// @param       MOBILEPHONE
// @param       reserved (MOBILEDIRECT)
// @param       OTHERPHONE
// @param       reserved (OTHERDIRECT)
// @param       EXTENSION
// @param       AIMSCREEN
// @param       YAHOOSCREEN
// @param       MSNSCREEN
// @param       USER1
// @param       USER2
// @param       TYPE:  1 for registered user, 2 for admin
// @param       PROFILEID (blank for default)
// @param       MAXSIZE (obsolete?)
// @param       MAXPRIORITY (obsolete?)
// @param       PRIORITYTYPE (obsolete?)
// @param       ENABLEDFEATURES (always 0?)
// @param       DISABLEDFEATURES (always 0?)
//
// @return      USERID of updated contact
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_UPDATE_USER", 'addressbk.update_contact');

///////////////////////////////////////////////////////////////////////
// find_meetings_by_host
// @param      SESSIONID
// @param      HOSTID
//
// --- return array of meeting data ----
// @return     MEETINGID
// @return     HOSTID
// @return     TYPE (1 = instant, 2 = scheduled)
// @return     PRIVACY (0 = public, 2 = private, 1 = unlisted)
// @return     PRIORITY (obsolete)
// @return     TITLE
// @return     DESCRIPTION
// @return     SIZE (obsolete)
// @return     DURATION
// @return     TIME
// @return     RECURRENCE  (0 = None, 1 = Daily, 2 = Weekly, 3 = Monthly)
// @return     RECUR_END
// @return     OPTIONS
// @return     STATUS     
// @return     START_TIME
// @return     END_TIME
// @return     PARTICIPANTID (of host)
// @return     ROLL (0 = Normal, 1 = Listener, 2 = Moderator, 3 = Host)
// @return     BRIDGEPHONE
// @return     SYSTEMURL
// @return     DISPLAY_ATTENDEES  (0 = None, 1 = Moderators Only, 2 = All)
// @return     INVITEE_CHAT_OPTIONS (Chat for non-mods: 0 = None, 1 = Mods Only, 2: All)
// @return     PASSWORD
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_FIND_MEETINGS_BY_HOST", 'schedule.find_meetings_by_host');

///////////////////////////////////////////////////////////////////////
// find_meetings_invited
// @param      SESSIONID
// @param      USERID
//
// --- return array of meeting data ----
// @return     MEETINGID
// @return     HOSTID
// @return     TYPE (1 = instant, 2 = scheduled)
// @return     PRIVACY (0 = public, 2 = private, 1 = unlisted)
// @return     PRIORITY (obsolete)
// @return     TITLE
// @return     DESCRIPTION
// @return     SIZE (obsolete)
// @return     DURATION
// @return     TIME
// @return     RECURRENCE  (0 = None, 1 = Daily, 2 = Weekly, 3 = Monthly)
// @return     RECUR_END
// @return     OPTIONS
// @return     STATUS     
// @return     START_TIME
// @return     END_TIME
// @return     PARTICIPANTID (of host)
// @return     ROLL (0 = Normal, 1 = Listener, 2 = Moderator, 3 = Host)
// @return     BRIDGEPHONE
// @return     SYSTEMURL
// @return     DISPLAY_ATTENDEES  (0 = None, 1 = Moderators Only, 2 = All)
// @return     INVITEE_CHAT_OPTIONS (Chat for non-mods: 0 = None, 1 = Mods Only, 2: All)
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_FIND_MEETINGS_INVITED", 'schedule.find_meetings_invited');

///////////////////////////////////////////////////////////////////////
// find_meetings_public
// @param      SESSIONID
// @param      COMMUNITYID
// @param      USERID
//
// --- return array of meeting data ----
// @return     MEETINGID
// @return     HOSTID
// @return     TYPE (1 = instant, 2 = scheduled)
// @return     PRIVACY (0 = public, 2 = private, 1 = unlisted)
// @return     PRIORITY (obsolete)
// @return     TITLE
// @return     DESCRIPTION
// @return     SIZE (obsolete)
// @return     DURATION
// @return     TIME
// @return     RECURRENCE  (0 = None, 1 = Daily, 2 = Weekly, 3 = Monthly)
// @return     RECUR_END
// @return     OPTIONS
// @return     STATUS     
// @return     START_TIME
// @return     END_TIME
// @return     BRIDGEPHONE
// @return     SYSTEMURL
// @return     DISPLAY_ATTENDEES  (0 = None, 1 = Moderators Only, 2 = All)
// @return     INVITEE_CHAT_OPTIONS (Chat for non-mods: 0 = None, 1 = Mods Only, // 2: All)
// @return     INVITE_URL
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_FIND_MEETINGS_PUBLIC", 'schedule.find_meetings_public');

///////////////////////////////////////////////////////////////////////
// remove_meeting
// @param       SESSIONID
// @param       MEETINGID
//
// @return      Ok
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_REMOVE_MEETING", 'schedule.remove_meeting');

///////////////////////////////////////////////////////////////////////
// get_meetings
// @param      SESSIONID
//
// --- return array of user data ----
// @return     MEETINGID
// @return     HOSTID
// @return     TYPE (instant, scheduled)
// @return     PRIVACY (public, private, unlisted)
// @return     TITLE
// @return     DESCRIPTION
// @return     DURATION
// @return     TIME
// @return     OPTIONS
// @return     NUMBER_INVITED_PARTICIPANTS
// @return     NUMBER_CONNECTED_PARTICIPANTS
// @return     APPSHARE_SERVER
// @return     NUMBER_APPSHARE_SESSIONS
// @return     VOICE_SERVER
// @return     NUMBER_CALLS
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_GET_MEETINGS", 'controller.get_meetings');

///////////////////////////////////////////////////////////////////////
// get_servers
// @param      SESSIONID
// @param      SERVICETYPE (1 = voice, 2 = appshare)
//
// --- return array of user data ----
// @return     SERVERID
// @return     SERVICETYPE
// @return     STATE (1 = available, 2 = unknown)
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_GET_SERVERS", 'controller.get_servers');

///////////////////////////////////////////////////////////////////////
// get_ports
// @param      SESSIONID
// @param      SERVERID (VOICE)
//
// --- return array of user data ----
// @return     PORTID
// @return     STATE (1 = connected)
// @return     MEETINGID
// @return     PARTICIPANTID
// @return     SCREENNAME
// @return     NAME
// @return     LASTEVENTTIME
// @return     LASTEVENTNAME
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_GET_PORTS", 'controller.get_ports');

///////////////////////////////////////////////////////////////////////
// reset_port
// @param      SESSIONID
// @param      SERVERID
// @param      PORTID
// @param      PARTICIPANTID (if non-empty reset only if given participant is
// using it)
//
// --- return array of user data ----
// @return     number of ports affected
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_RESET_PORT", 'controller.reset_port');

///////////////////////////////////////////////////////////////////////
// get_metrics
// @param      SESSIONID
// @param      TYPE       , where 0 (meetings), 1 (ports), 2 (users), 3 (share sessions)
// @param      NUMBER     of rows to retrieve
//
// --- return array of user data ----
// @return     TYPE       , where 0 (meetings), 1 (ports), 2 (users), 3 (share sessions)
// @return     AVERAGE
// @return     PEAK
// @return     LAST_TIME_MEASURED
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_GET_METRICS", 'controller.get_metrics');

///////////////////////////////////////////////////////////////////////
// get_health
// @param      SESSIONID
//
// --- return array of user data ----
// @return     ID
// @return     LOCATION
// @return     STATUS (0: unavailable, 1: available)
///////////////////////////////////////////////////////////////////////
define("WEBSVR_FN_GET_HEALTH", 'controller.get_health');

?>
