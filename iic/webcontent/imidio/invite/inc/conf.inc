<?php

//
// Copyright 2004 Imidio, Inc.
//

//
// Constants
//

// class id for active x control
define("CLASS_ID_STR", "clsid:29956D12-F6F5-446A-8C55-CB7347DE0DC0");

// location and version of active x control
define("ACTIVEXCTRL_SOURCE_STR", "http://192.168.1.10/imidio/downloads/IICPlugin.ocx#Version=1,0,0,9");

// url for installer exe
define("INSTALLER_URL_STR", "http://192.168.1.10/imidio/downloads/conferencing.exe");
define("RPM_URL_STR", "http://192.168.1.10/imidio/downloads/conferencing.rpm");
define("RPM_64BIT_URL_STR", "http://192.168.1.10/imidio/downloads/conferencing-64bit.rpm");
define("DMG_URL_STR", "http://192.168.1.10/imidio/downloads/conferencing.dmg");

// url for branding image and web page
define("BRAND_IMAGE_URL_STR", "http://192.168.1.10/imidio/images/apreta_logo.png");
define("BRAND_PAGE_URL_STR", "http://192.168.1.10/imidio/invite/frame_header.php");
define("BRAND_PAGE_SIZE", "110");

// misc constants 
define("REQUIRE_PASSWORD_CONSTANT", 32);

define("INVITATION_DATA_ONLY_CONSTANT", 16384);

// url for dymamically generated installer URL
define("DYNAMIC_INSTALLER_STR", "http://192.168.1.10/imidio/invite/download.php");

// url for meeting invite frame
define("FRAME_INVITE_STR", "http://192.168.1.10/imidio/invite/frame_invite.php");

// installer download directory
define("INSTALLER_DOWNLOAD_DIR", "/var/iic/htdocs/imidio/downloads/");

// installer filename suffix (prefix is exe)
define("INSTALLER_DOWNLOAD_FILENAME", "conferencing");

// help pages
define("INVITE_HELP", "http://192.168.1.10/imidio/invite/help.php");

define("SERVER_TOKEN", "1");
?>
