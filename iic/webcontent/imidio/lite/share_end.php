<?php

//
// Copyright 2004 Imidio, Inc.
//

include("xmlrpc.inc");
include("common.inc");

$msg = "";
$sessiontoken = getSessionId();
$meetingid = getSessionMeeting();

if ($sessiontoken > 0)
{
    $f=new xmlrpcmsg(WEBSVR_FN_END_SHARE,
                     array(new xmlrpcval($sessiontoken, "string"),
                           new xmlrpcval($meetingid, "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode())
    {
        $msg = "Meeting " . $meetingid . " (session " . $sessiontoken . ") has ended.";
    }
    else
    {
        $msg = $r->faultCode() . " - " . $r->faultString();
    }
}
?>

<html>
<head>
<title>End share</title></head>
<body>

<p>End share message: <?=$msg; ?></p>

<p><a href="index.html">Click here to return to start page</a></p>

</body>
</html>
