<?php

//
// Copyright 2004 Imidio, Inc.
//

include("xmlrpc.inc");
include("common.inc");

$redirect = 0;
$instructions = "";
$msg = "";
$enableflag = $HTTP_GET_VARS["enableflag"];

if ($enableflag != "1")
{
    $enableflag = 0;
    $instructions = "Enter a Participant ID to revoke remote control:";
}
else
{
    $instructions = "Enter a Participant ID to grant remote control:";
}

if ($HTTP_POST_VARS["remotepin"]!="")
{
    $remotepin = $HTTP_POST_VARS["remotepin"];
    $sessiontoken = getSessionId();
    $meetingid = getSessionMeeting();

    $f=new xmlrpcmsg(WEBSVR_FN_ENABLE_REMOTE_CONTROL,
                     array(new xmlrpcval($sessiontoken, "string"),
                           new xmlrpcval($meetingid, "string"),
                           new xmlrpcval($remotepin, "string"),
                           new xmlrpcval($enableflag, "boolean")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode())
    {
        $redirect = 1;

        if ($enableflag == "1")
        {
            $msg = "Remote control granted to " . $remotepin;
        }
        else
        {
            $msg = "Remote control revoked from " . $remotepin;
        }
    }
    else
    {
        $msg = "Unable to modify remote control status for participant " . $remotepin;
    }
}
else
{
    $msg = "";
}
?>

<html>
<head>
<?php
if ($redirect == 1)
{
    echo("<meta http-equiv='refresh' content='2;url=share_inprogress.php'>");
}
?>
<title>Remote Control Options</title></head>
<body>

<?php
print $instructions;
?>
<FORM  METHOD="POST">
<p>Participant PIN: <INPUT NAME="remotepin"></p>
<p><td align="right"><input type="submit" value="Change Remote Control" name="submit"></p>
</FORM>
<p><a href="share_inprogress.php">Click here to return to meeting options</a></p>


<?php
print $msg;
?>

<P>

</body>
</html>
