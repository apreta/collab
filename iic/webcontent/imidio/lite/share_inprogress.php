<?php

//
// Copyright 2004 Imidio, Inc.
//

include("xmlrpc.inc");
include("common.inc");

$allocatedserver = getSessionServer();
$docserver = getSessionDocServer();
$sessiontoken = getSessionId();
$meetingid = getSessionMeeting();
$participantid = getParticipantMeeting();

//
// sample url: iiclite://216.65.116.139::11000/token/1800/001/deskShare/presenter
//
$urlbase = SHARE_PROTOCOL . "://" . $allocatedserver . "::" . SHARE_PORT . "/" .
           $sessiontoken . "/" . $meetingid . "/" . $participantid . "/";

$docurlbase = SHARE_PROTOCOL . "://" . $docserver . "::" . DOCSHARE_PORT . "/" .
           $sessiontoken . "/" . $meetingid . "/" . $participantid . "/";

$startshare_desk =  $urlbase . "deskShare/presenter";
$startshare_app =  $urlbase . "appShare/presenter";
$startshare_ppt =  $docurlbase . "pptShare/presenter";
$startview_ppt =  $docurlbase . "pptShare/viewer";
$startshare_doc = $docurlbase . "docShare/presenter";
$startshare_whiteboard = $docurlbase . "whiteboard/presenter";
$startview_app =  $urlbase . "deskShare/viewer";
$startview_doc =  $docurlbase . "docShare/viewer";
$endshare = $urlbase . "endShare/presenter";

?>

<html>
<head>
<title>Meeting Information</title></head>
<body>

Current meeting information:<br>
<br>
Meeting PIN: <?=$meetingid; ?><br>
Participant PIN: <?=$participantid; ?><br>
Session: <?=$sessiontoken; ?><br>
App Share Server: <?=$allocatedserver; ?><br>
Doc Share Server: <?=$docserver; ?><br>
<br>
<a href="<?=$startshare_desk; ?>">Start desktop share</a>
<br>
<a href="<?=$startshare_app; ?>">Start application share</a>
<br>
<a href="<?=$startshare_ppt; ?>">Start PowerPoint share</a>
<br>
<a href="<?=$startview_app; ?>">View the desktop/application share</a>
<br>
<a href="<?=$startview_ppt; ?>">View PowerPoint share</a>
<br>
<a href="<?=$startshare_doc; ?>">Start document share</a>
<br>
<a href="<?=$startshare_whiteboard; ?>">Start whiteboard share</a>
<br>
<a href="<?=$startview_doc; ?>">View the document share</a>
<br>
<a href="<?=$endshare; ?>">Stop sharing</a>
<br>
<a href="share_enableremotecontrol.php?enableflag=1">Grant remote control</a>
<br>
<a href="share_enableremotecontrol.php?enableflag=0">Revoke remote control</a>
<br>
<a href="share_enablerecording.php?enableflag=1">Enable recording</a>
<br>
<a href="share_enablerecording.php?enableflag=0">Disable recording</a>
<br>
<a href="share_setrecording.php">Set recording audio download URL</a>
<br>
<a href="share_setoutput.php">Set recording output URL</a>
<br>
<p><a href="share_end.php">Click here to end meeting</a></p>

</body>
</html>
