<?php

//
// Copyright 2004 Imidio, Inc.
//

include("common.inc");

$allocatedserver = getSessionServer();
$docserver = getSessionDocServer();
$sessiontoken = getSessionId();
$meetingid = getSessionMeeting();
$participantid = getParticipantMeeting();

//
// sample url: iiclite://216.65.116.139::11000/token/1800/001/deskShare/presenter
//
$urlbase = SHARE_PROTOCOL . "://" . $allocatedserver . "::" . SHARE_PORT . "/" .
           $sessiontoken . "/" . $meetingid . "/" . $participantid . "/";

$docurlbase = SHARE_PROTOCOL . "://" . $docserver . "::" . DOCSHARE_PORT . "/" .
           $sessiontoken . "/" . $meetingid . "/" . $participantid . "/";


$startview_app =  $urlbase . "deskShare/viewer";
$startview_ppt =  $docurlbase . "pptShare/viewer";
$startshare_desk =  $urlbase . "deskShare/presenter";
$startshare_app =  $urlbase . "appShare/presenter";
$startshare_ppt =  $docurlbase . "pptShare/presenter";
$startshare_doc = $docurlbase . "docShare/presenter";
$startview_doc =  $docurlbase . "docShare/viewer";
$endshare = $urlbase . "endShare/presenter";
?>

<html>
<head>
<title>Meeting Information</title></head>
<body>

Current meeting information:<br>
<br>
Meeting PIN: <?=$meetingid; ?><br>
Participant PIN: <?=$participantid; ?><br>
Session: <?=$sessiontoken; ?><br>
Server: <?=$allocatedserver; ?><br>
<br>
<a href="<?=$startview_app; ?>">View desktop/application share</a>
<br>
<a href="<?=$startview_ppt; ?>">View PowerPoint share</a>
<br>
<a href="<?=$startview_doc; ?>">View document share</a>
<br>
<br>
<a href="<?=$startshare_desk; ?>">Start desktop share</a>
<br>
<a href="<?=$startshare_app; ?>">Start application share</a>
<br>
<a href="<?=$startshare_ppt; ?>">Start PowerPoint share</a>
<br>
<a href="<?=$startshare_doc; ?>">Start document share</a>
<br>
<a href="<?=$endshare; ?>">Stop sharing</a>
<br>
<p><a href="view_end.php">Click here to leave the meeting</a></p>

</body>
</html>
