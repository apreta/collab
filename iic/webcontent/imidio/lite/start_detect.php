<?php

//
// Copyright 2004 Imidio, Inc.
//

// Insert header to turn off caching for this page
header("Expires: Mon, 26 Jul 2001 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header("Cache-control: private");

include("common.inc");

$is_installed = stristr($_SERVER["HTTP_ACCEPT"],"application/iiclite");

if ($is_installed)
{
    include("start_options.html");
}
else
{
    include("start_download.html");
}

?>
