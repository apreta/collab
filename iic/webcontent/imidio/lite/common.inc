<?php

//
// Copyright 2004 Imidio, Inc.
//

//
// Constants and utilities
//

// webservice domain
define(WEB_SERVICE_DOMAIN, "localhost");

// webservice port
define(WEB_SERVICE_PORT, 2345);

// webservice URI
define(WEB_SERVICE_URI, "/RPC2");

// share lite http accept type
define(LITE_ACCEPT_TYPE, "application/iiclite");

// share lite protocol handler tag
define(SHARE_PROTOCOL, "iiclite");

// port for the share server
define(SHARE_PORT, "2182");
define(DOCSHARE_PORT, "5222");

// cookie names
define(SESSION_ID_COOKIE_NAME, "imidiosessionid");
define(SESSION_SERVER_COOKIE_NAME, "imidiosessionserver");
define(SESSION_DOCSERVER_COOKIE_NAME, "imidiosessiondocserver");
define(SESSION_MEETING_COOKIE_NAME, "imidiosessionmeeting");
define(SESSION_PARTICIPANT_COOKIE_NAME, "imidiosessionparticipant");

// default chunk size
define(DEFAULT_CHUNK_SIZE, 20);

// verifies that the user has valid sessionid.  if not, redirects to login page
function checkAuth()
{
    $sessionid = $_COOKIE[SESSION_ID_COOKIE_NAME];
    if (!($sessionid > 0))
    {
        echo("<meta http-equiv='refresh' content='0;url=/imidio/console/php/logout.php'>");
    }
}

// sets the session cookie
function setSessionCookie($sessionid)
{
    $cookieset = setcookie (SESSION_ID_COOKIE_NAME, $sessionid, time()+3600, '/'); 
}

// sets the server url cookie
function setServerCookie($sessionserver, $sessiondocserver)
{
    $cookieset = setcookie (SESSION_SERVER_COOKIE_NAME, $sessionserver, time()+3600, '/');
    $cookieset = setcookie (SESSION_DOCSERVER_COOKIE_NAME, $sessiondocserver, time()+3600, '/');
}

// sets the meetingid cookie
function setMeetingCookie($meetingid)
{
    $cookieset = setcookie (SESSION_MEETING_COOKIE_NAME, $meetingid, time()+3600, '/');
}

// sets the participantid cookie
function setParticipantCookie($participantid)
{
    $cookieset = setcookie (SESSION_PARTICIPANT_COOKIE_NAME, $participantid, time()+3600, '/');
}

function clearCookies()
{
    $cookieset = setcookie (SESSION_ID_COOKIE_NAME, "", time()-3600, '/');
    $cookieset = setcookie (SESSION_SERVER_COOKIE_NAME, "", time()-3600, '/');
    $cookieset = setcookie (SESSION_DOCSERVER_COOKIE_NAME, "", time()-3600, '/');
    $cookieset = setcookie (SESSION_MEETING_COOKIE_NAME, "", time()-3600, '/');
    $cookieset = setcookie (SESSION_PARTICIPANT_COOKIE_NAME, "", time()-3600, '/');
}

// retrieves the session id from the cookie
function getSessionId()
{
    return $_COOKIE[SESSION_ID_COOKIE_NAME];
}

// returns server url from the cookie
function getSessionServer()
{
    return $_COOKIE[SESSION_SERVER_COOKIE_NAME];
}

// returns doc server url from the cookie
function getSessionDocServer()
{
    return $_COOKIE[SESSION_DOCSERVER_COOKIE_NAME];
}

// returns meeting id from the cookie
function getSessionMeeting()
{
    return $_COOKIE[SESSION_MEETING_COOKIE_NAME];
}

// returns participant id from the cookie
function getParticipantMeeting()
{
    return $_COOKIE[SESSION_PARTICIPANT_COOKIE_NAME];
}

// clears the browser cache
function clearCache()
{
    header("Expires: Mon, 26 Jul 2001 05:00:00 GMT");               // Date in the past
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");  // always modified
    header("Cache-Control: no-store, no-cache, must-revalidate");   // HTTP/1.1
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");                                     // HTTP/1.0
    header("Cache-control: private");                               // special magic
}


///////////////////////////////////////////////////////////////////////
// WebService function definitions
///////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////
// allocate_share
// @param       meetingid    (string)
// @param       expectedsize (i4)
// @param       sharemode    (i4) (1 = application, 2 = document, 3 = both)
//
// --- return string array of session information ----
// @return      ALLOCATEDSERVER
// @return      SESSIONTOKEN
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_ALLOCATE_SHARE, 'allocate_share');

///////////////////////////////////////////////////////////////////////
// end_share
// @param       sessiontoken (string)
// @param       meetingid    (string)
//
// @return      Ok
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_END_SHARE, 'end_share');

///////////////////////////////////////////////////////////////////////
// find_share
// @param       meetingid (string)
//
// --- return string array of session information ----
// @return      ALLOCATEDSERVER
// @return      SESSIONTOKEN
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_FIND_SHARE, 'find_share');

///////////////////////////////////////////////////////////////////////
// enable_remote_control
// @param       sessiontoken (string)
// @param       meetingid (string)
// @param       participantid (string)
// @param       enableflag (boolean)
//
// @return      success (i4)
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_ENABLE_REMOTE_CONTROL, 'enable_remote_control');

///////////////////////////////////////////////////////////////////////
// enable_recording
// @param       sessiontoken (string)
// @param       meetingid (string)
// @param       participantid (string)
// @param       enableflag (boolean)
//
// @return      success (i4)
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_ENABLE_RECORDING, 'enable_recording');

///////////////////////////////////////////////////////////////////////
// get_recording_list
// @param       meetingid (string)
//
// @return      recordinglist(array)
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_GET_RECORDING_LIST, 'get_recording_list');

///////////////////////////////////////////////////////////////////////
// remove_recordings
// @param       meetingid (string)
// @param       recordingid (string)
//
// @return      success(i4)
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_REMOVE_RECORDINGS, 'remove_recordings');

///////////////////////////////////////////////////////////////////////
// get_doc_list
// @param       meetingid (string)
//
// @return      recordinglist(array)
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_GET_DOC_LIST, 'get_doc_list');

///////////////////////////////////////////////////////////////////////
// remove_docs
// @param       meetingid (string)
// @param       recordingid (string)
//
// @return      success(i4)
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_REMOVE_DOCS, 'remove_docs');

///////////////////////////////////////////////////////////////////////
// set_recording_audio
// @param       sessiontoken (string)
// @param       meetingid (string)
// @param       url (string)
// @param       format (string)
//
// @return      success(i4)
///////////////////////////////////////////////////////////////////////
define(WEBSVR_FN_SET_RECORDING_AUDIO, 'set_recording_audio');

define(WEBSVR_FN_SET_RECORDING_OUTPUT, 'set_recording_output');

?>
