<?php

//
// Copyright 2004 Imidio, Inc.
//

include("xmlrpc.inc");
include("common.inc");

$redirect = 0;
$msg = "";

if ($HTTP_POST_VARS["meetingid"]!="")
{
    $meetingid = $HTTP_POST_VARS["meetingid"];
    $participantid = $HTTP_POST_VARS["participantid"];
    $expectedsize = $HTTP_POST_VARS["expectedsize"];
    $sharemode = "3";

    $f=new xmlrpcmsg(WEBSVR_FN_ALLOCATE_SHARE,
                     array(new xmlrpcval($meetingid, "string"),
                           new xmlrpcval($expectedsize, "i4"),
                           new xmlrpcval($sharemode, "i4")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode())
    {
        $allocatedserverobj = $v->arraymem(0);
        $allocatedserver = $allocatedserverobj->scalarval();

        $allocateddocserverobj = $v->arraymem(1);
        $allocateddocserver = $allocateddocserverobj->scalarval();

        $sessiontokenobj = $v->arraymem(2);
        $sessiontoken = $sessiontokenobj->scalarval();

        if ($sessiontoken > 0)
        {
            $redirect = 1;
            setServerCookie($allocatedserver, $allocateddocserver);
            setSessionCookie($sessiontoken);
            setMeetingCookie($meetingid);
            setParticipantCookie($participantid);
            $msg = "Starting meeting...server is " . $allocatedserver . " - session is " . $sessiontoken;
        }
        else
        {
            $msg = "Invalid session token returned." ;
            clearCookies();
        }
    }
    else
    {
        $msg = "Unable to connect to webservice (" . $r->faultCode() . ": " . $r->faultString() . ")";
        clearCookies();
    }
}
else
{
    clearCookies();
}
?>

<html>
<head>
<?php
if ($redirect == 1)
{
    echo("<meta http-equiv='refresh' content='2;url=share_inprogress.php'>");
}
?>
<title>Enter meeting information</title></head>
<body>

Enter meeting information:
<FORM  METHOD="POST">
<p>Meeting PIN: <INPUT NAME="meetingid"></p>
<p>Participant PIN: <INPUT NAME="participantid"></p>
<p>Expected size: <INPUT NAME="expectedsize"></p>
<p><td align="right"><input type="submit" value="Start Meeting" name="submit"></p>
</FORM>

<?php
print $msg;
?>

<P>

</body>
</html>
