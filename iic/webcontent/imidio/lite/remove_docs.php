<?php

//
// Copyright 2004 Imidio, Inc.
//

include("xmlrpc.inc");
include("common.inc");

$redirect = 0;
$msg = "";

$sessiontoken = getSessionId();
$meetingid = getSessionMeeting();
$instance = $_GET["instance"];

$f=new xmlrpcmsg(WEBSVR_FN_REMOVE_DOCS,
                        array(new xmlrpcval($meetingid, "string"),
                        new xmlrpcval($instance, "string")));
$c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
$r=$c->send($f);
$v=$r->value();

if (!$r->faultCode())
{
    $redirect = 1;

    $msg = "Document removed";
}
else
{
    $msg = "Unable to remove document " . $instance . " (" . $r->faultString() . ")";
}
?>

<html>
<head>
<title>Remove document</title></head>
<body>

<?php
print $msg;
?>

<p><a href="view_docs.php?refresh=1">Click here to return to document list</a></p>

</body>
</html>
