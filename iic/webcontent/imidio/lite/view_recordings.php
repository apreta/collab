<?php

//
// Copyright 2004 Imidio, Inc.
//

include("xmlrpc.inc");
include("common.inc");

$msg = "";
$valid = 0;
$fetch = false;

if ($_POST["meetingid"] != "")
{
    $meetingid = $HTTP_POST_VARS["meetingid"];
    setMeetingCookie($meetingid);
    $fetch = true;
}
else if ($_GET["refresh"] == "1")
{
 	$meetingid = getSessionMeeting();
	$fetch = true;
}

if ($fetch)
{
    $f=new xmlrpcmsg(WEBSVR_FN_GET_RECORDING_LIST, array(new xmlrpcval($meetingid, "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode())
    {
    	  $msg = "Recordings fetched.";
    	  $valid = 1;
    }
    else
    {
        $msg = "Meeting not found (" . $r->faultString . ")";
    }
}

?>

<html>
<head>
<title>Fetch meeting recordings</title></head>
<body>

Enter meeting information:
<FORM  METHOD="POST">
<p>Meeting PIN: <INPUT NAME="meetingid"></p>
<p><td align="right"><input type="submit" value="View recordings" name="submit"></p>
</FORM>

<?php
print $msg;
?>

<P>

<?php
  if ($valid)
  {
    $have_folders = false;
    $have_files = false;
    
    echo "<table cellpadding=8>";
    
    $filelist = $v->arraymem(0);
  	for ($i = 0; $i < $filelist->arraysize(); $i++)
    {
		$file = $filelist->arraymem($i);
		$nameobj = $file->structmem("name");
		$typeobj = $file->structmem("type");
		$urlobj = $file->structmem("url");
		$type = $typeobj->scalarval();

		switch ($type)
		{
		case 0:
			$typename = "Data Share";
			break;
		case 1:
			$typename = "Audio recording";
			break;
		case 3:
			$typename = "Chat";
			break;
		default:
			$typename = "Unknown";
		}

		if ($type == -1)
		{
			if ($have_folders)
			{
				// End previous row
				if (!$have_files)
				{
					echo "No content";
				}

				echo "</td><td><a href=remove_recordings.php?instance=" . 
					 $name . ">Remove</a></tr>\n";
			}
			
			// Start current row
			$name = $nameobj->scalarval();
			echo "<tr><td>" . $name .  "</td><td>";
			$have_folders = true;
			$have_files = false;
		}
		else {
			// Output current file name
			$have_files = true;
			
			echo "<a href=" .  $urlobj->scalarval() . ">" . $typename . "</a><br>";
		} 
    }
    
	if ($have_folders)
	{
		// End last row
		if (!$have_files)
		{
			echo "No content";
		}
		echo "</td><td><a href=remove_recordings.php?instance=" . 
			 $name . ">Remove</a></tr>\n";
	}
  }     
	
  echo "</table>";
?>

<p><a href="index.html">Click here to return to start page</a></p>

</body>
</html>
