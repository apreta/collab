<?php

//
// Copyright 2004 Imidio, Inc.
//

include("xmlrpc.inc");
include("common.inc");

$redirect = 0;
$msg = "";
$fetch = false;

$sessiontoken = getSessionId();
$meetingid = getSessionMeeting();

if ($_POST["recordingurl"] != "")
{
    $recordingurl = $HTTP_POST_VARS["recordingurl"];
    $fetch = true;
}

if ($fetch)
{
    $f=new xmlrpcmsg(WEBSVR_FN_SET_RECORDING_OUTPUT,
                    array(new xmlrpcval($sessiontoken, "string"),
                        new xmlrpcval($meetingid, "string"),
                        new xmlrpcval($recordingurl, "string"),
                        new xmlrpcval("", "string"),
			new xmlrpcval("", "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode())
    {
        $redirect = 1;
		$msg = "Recording audio output URL set";
	}
	else
	{
		$msg = "Unable to set recording output URL (" . $r->faultString() . ")";
	}
}
?>

<html>
<head>
<?php
if ($redirect == 1)
{
    echo("<meta http-equiv='refresh' content='2;url=share_inprogress.php'>");
}
?>
<title>Set Recording Output URL</title></head>
<body>

<FORM  METHOD="POST">
<p>Recording Output URL: <INPUT NAME="recordingurl"></p>
<p><td align="right"><input type="submit" value="Set URL" name="submit"></p>
</FORM>

<?php
print $msg;
?>

<P>
<a href="share_inprogress.php">Click here to return to meeting options</a></p>

</body>
</html>
