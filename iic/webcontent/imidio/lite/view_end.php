<?php

//
// Copyright 2004 Imidio, Inc.
//

include("common.inc");

$allocatedserver = getSessionServer();
$sessiontoken = getSessionId();
$meetingid = getSessionMeeting();

//
// sample url: iiclite://216.65.116.139::11000/token/1800/001/deskShare/presenter
//
$urlbase = SHARE_PROTOCOL . "://" . $allocatedserver . "::" . SHARE_PORT . "/" .
           $sessiontoken . "/" . $meetingid . "/" . "001" . "/";
$endshare = $urlbase . "endShare/presenter";

clearCookies();
?>
<html>
<head>
    <title>Leave Meeting</title>
</head>
<frameset id="FrameSet" rows="*,0" frameborder="0" border="0">
    <frame name=frame1 src="view_end.html" scrolling="no">
    <frame name=frame2 src="<?=$endshare; ?>" scrolling="no">
</frameset>
</html>

