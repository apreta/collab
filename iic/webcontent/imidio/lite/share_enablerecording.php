<?php

//
// Copyright 2004 Imidio, Inc.
//

include("xmlrpc.inc");
include("common.inc");

$redirect = 0;
$instructions = "";
$msg = "";
$enableflag = $HTTP_GET_VARS["enableflag"];
$remotepin = "0";

$sessiontoken = getSessionId();
$meetingid = getSessionMeeting();

$f=new xmlrpcmsg(WEBSVR_FN_ENABLE_RECORDING,
                    array(new xmlrpcval($sessiontoken, "string"),
                        new xmlrpcval($meetingid, "string"),
                        new xmlrpcval($remotepin, "string"),
                        new xmlrpcval($enableflag, "boolean")));
$c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
$r=$c->send($f);
$v=$r->value();

if (!$r->faultCode())
{
    $redirect = 1;

    if ($enableflag == "1")
    {
        $msg = "Recording enabled";
    }
    else
    {
        $msg = "Recording disabled";
    }
}
else
{
    $msg = "Unable to change recording status (" . $r->faultString() . ")";
}
?>

<html>
<head>
<?php
if ($redirect == 1)
{
    echo("<meta http-equiv='refresh' content='2;url=share_inprogress.php'>");
}
?>
<title>Recording Enable/Disable</title></head>
<body>

<?php
print $msg;
?>
<P>
<a href="share_inprogress.php">Click here to return to meeting options</a></p>

</body>
</html>
