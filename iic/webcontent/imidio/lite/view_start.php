<?php

//
// Copyright 2004 Imidio, Inc.
//

include("xmlrpc.inc");
include("common.inc");

$redirect = 0;
$msg = "";

if ($HTTP_POST_VARS["meetingid"]!="")
{
    clearCookies();
    $meetingid = $HTTP_POST_VARS["meetingid"];
    $participantid = $HTTP_POST_VARS["participantid"];

    $f=new xmlrpcmsg(WEBSVR_FN_FIND_SHARE, array(new xmlrpcval($meetingid, "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode())
    {
        $allocatedserverobj = $v->arraymem(0);
        $allocatedserver = $allocatedserverobj->scalarval();

        $allocateddocserverobj = $v->arraymem(1);
        $allocateddocserver = $allocateddocserverobj->scalarval();

        $sessiontokenobj = $v->arraymem(2);
        $sessiontoken = $sessiontokenobj->scalarval();

        if ($sessiontoken > 0)
        {
            $redirect = 1;
            setServerCookie($allocatedserver, $allocateddocserver);
            setSessionCookie($sessiontoken);
            setMeetingCookie($meetingid);
            setParticipantCookie($participantid);
            $msg = "Joining meeting...server is " . $allocatedserver . " - session is " . $sessiontoken;
        }
        else
        {
            $msg = "Meeting not found.";
        }
    }
    else
    {
        $msg = "Meeting not found.";
    }
}

?>

<html>
<head>
<?php
if ($redirect == 1)
{
    echo("<meta http-equiv='refresh' content='2;url=view_inprogress.php'>");
}
?>
<title>Enter meeting information</title></head>
<body>

Enter meeting information:
<FORM  METHOD="POST">
<p>Meeting PIN: <INPUT NAME="meetingid"></p>
<p>Participant PIN: <INPUT NAME="participantid"></p>
<p><td align="right"><input type="submit" value="Join Meeting" name="submit"></p>
</FORM>

<?php
print $msg;
?>

<P>

</body>
</html>
