<?php

//
// Copyright 2004 Imidio, Inc.
//

include("xmlrpc.inc");
include("common.inc");

$redirect = 0;
$msg = "";

$sessiontoken = getSessionId();
$meetingid = getSessionMeeting();
$instance = $_GET["instance"];

$f=new xmlrpcmsg(WEBSVR_FN_REMOVE_RECORDINGS,
                        array(new xmlrpcval($meetingid, "string"),
                        new xmlrpcval($instance, "string")));
$c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
$r=$c->send($f);
$v=$r->value();

if (!$r->faultCode())
{
    $redirect = 1;

    $msg = "Recordings removed";
}
else
{
    $msg = "Unable to remove recordings " . $instance . " (" . $r->faultString() . ")";
}
?>

<html>
<head>
<?php
/*if ($redirect == 1)
{
    echo("<meta http-equiv='refresh' content='2;url=view_recordings.php?refresh=1'>");
}*/
?>
<title>Remove recordings</title></head>
<body>

<?php
print $msg;
?>

<p><a href="view_recordings.php?refresh=1">Click here to return to meeting list</a></p>

</body>
</html>
