<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Get port usage.
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Get Metrics</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<body>
<?php showNavBar3(); ?>
<div id="content">
<?php

// type and number if set
$type = ($_GET['type']);
if ($type == "")
{
    $type = $HTTP_POST_VARS["type"];
}

$number = ($_GET['number']);
if ($number == "")
{
    $number = $HTTP_POST_VARS["number"];
}

$sessionid = getSessionId();

if ($sessionid != "" && $type != "" && $number != "") 
{
    echo("<center>");
    echo("<b>");
    if ($type == 1)
        echo("Meetings<p>");
    else if ($type == 2)
        echo("Ports<p>");
    else if ($type == 3)
        echo("Users<p>");
    else if ($type == 4)
        echo("Share Sessions<p>");
    echo("</b>");
    $f=new xmlrpcmsg(WEBSVR_FN_GET_METRICS,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($type, "string"),
                           new xmlrpcval($number, "int"))); 
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();
    if (!$r->faultCode())
    {
//        echo(htmlentities($r->serialize()) . "</PRE><HR>\n");

        // structure is:
        // array of values
        //   array of metric details
        //     where each metric is an array
        $metric_array = $v->arraymem(0);          // primary array
        echo("<table border=\"1\">");
        echo("<tr><th class=title>Ave</th><th class=title>Peak</th><th class=title>Last Time Measured</th>");
//        print "<HR>Result details (XML)<BR><PRE>" .
//            htmlentities($r->serialize()). "</PRE><HR>\n";

        for ($i = 0; $i < $metric_array->arraysize(); $i++)
        {
            echo("<tr>");
            $metric = $metric_array->arraymem($i);  // retrieve mtgs (an array)
            // type in slot 0 -- if value 0 indicates init of controller
            $metric_type = $metric->arraymem(0);
            if ($metric_type == 0)
            {
                echo("<td class=value>Initialized</td>");
            }
            else
            {
                $aveobj = $metric->arraymem(1);
                echo("<td class=value>&nbsp;" . $aveobj->scalarval() . "</td>");
                $peakobj = $metric->arraymem(2);
                echo("<td class=value>&nbsp;" . $peakobj->scalarval() . "</td>");
                $timeobj = $metric->arraymem(3);
                echo("<td class=value>&nbsp;" . $timeobj->scalarval() . "</td>");
            }
        }

        echo("</table>");
        echo("</center>");
    } 
    else
    {
	print "Fault: ";
	print "Code: " . $r->faultCode() . 
            " Reason '" .$r->faultString()."'<BR>";
    }
}
?>

<!--
// Enter a type and the number of values to retrieve.
// <FORM METHOD="POST">
// <table>
// <tr><td class=value>Type [0 (meetings), 1 (ports), 2 (users), 3 (share sessions)]:</td>
// <td class=value><INPUT NAME="type" VALUE="<?php print ${type};?>"></td></tr>
// <tr><td class=value>Number:</td>
// <td class=value><INPUT NAME="number" VALUE="<?php print ${number};?>"></td></tr>
// <tr><td colspan='2' align='right'><input type="submit" value="get metrics" name="submit"></td></tr>
// </table>
// </FORM>
-->
</div>
<?php showFooter(); ?>
</body>
</html>
