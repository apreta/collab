<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Enable/disable a community
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Enable/Disable Community</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<body>
<?php showNavBar2(); ?>
<h3>Enable/Disable Community</h3>
<?php
// if community id was passed in, fill it into the form
$commid = ($HTTP_POST_VARS['commid']);
if ($commid == "")
{
    $commid = ($_GET['commid']);
}
$action = ($HTTP_POST_VARS['action']);
if ($action == "")
{
    $action = ($_GET['action']);
}
$sessionid = getSessionId();

if ($commid == "")
{
    selectCommunityId($sessionid, "Ok");
}
else if ($action == 'Ok' && $commid != "")
{
    // get enabled/disabled state of selected community

    $f=new xmlrpcmsg(WEBSVR_FN_FETCH_COMMUNITY,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($commid, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();    
    if (!$r->faultCode()) 
    {
        $comm_array = $v->arraymem(0);
        $commobj = $comm_array->arraymem(0);
        $accountstatusobj = $commobj->arraymem(11);
        $accountstatus = $accountstatusobj->scalarval();
        
        if ($accountstatus == "1")
        {
            $checked = "checked=\"checked\" ";
        }
        else
        {
            $checked = "";
        }
        echo("<FORM METHOD=\"POST\">
              <input type=hidden name='commid' value='" . $commid . "'>
              <table>
                 <tr><td>Enabled:</td>
                 <td><INPUT NAME=\"enable\" type=\"CHECKBOX\" " . $checked . "></td></tr>
                 <tr><td colspan='2' align='right'><input type=\"submit\" name=\"action\" value=\"Update\" name=\"submit\"></td></tr>
              </table>
              </FORM>");
    }
    else
    {
        dumpFault($r);
    }
}
else if ($action == 'Update' && $commid != "")
{
    $enable = $HTTP_POST_VARS["enable"];
    if ($enable == "")
    {
        $enable = ($_GET['enable']);
    }

    // get vals from form
    if ($enable == "")
    {
        $enable_i = 0;
        $enable_str = "disabled";
    }
    else
    {
        $enable_i = 1;
        $enable_str = "enabled";
    }

    $f=new xmlrpcmsg(WEBSVR_FN_ENABLE_COMMUNITY,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($commid, "int"),
                           new xmlrpcval($enable_i, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode()) 
    {
        $numrowsidobj = $v->arraymem(0);
        $numrows = $numrowsidobj->scalarval();
        if ($numrows == 1)
            print "Community " . $enable_str;
        else
            print "Community " . $commid . " not found";
    }
    else  
    {
        dumpFault($r);
    }
}
else
{
    echo("<b>Unhandled condition in enableommunity</b>");
}
?>
<?php showFooter(); ?>
</body>
</html>
