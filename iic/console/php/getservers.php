<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Get servers.
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Get Servers</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<body>

<?php showNavBar1(); ?>
<?php

// servertype if set
$servertype = ($_GET['servertype']);
if ($servertype == "")
{
    $servertype = $HTTP_POST_VARS["servertype"];
}

$sessionid = getSessionId();

if ($sessionid != "" && $servertype != "")
{
    echo("<center>");
    echo("<b>");
    if ($servertype == 1)
        echo("Voice Bridge<p>");
    else if ($servertype == 2)
        echo("App/Desktop Sharing<p>");
    echo("</b>");
    $f=new xmlrpcmsg(WEBSVR_FN_GET_SERVERS,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($servertype, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $svr_array=$r->value();
    if (!$r->faultCode()) 
    {
        // structure is:
        //   array of svr details
        //     where details of each server is an array
        echo("<table border=\"1\">");
        echo("<tr><th class=title>Operations</th><th class=title>Server ID</th><th class=title>State</th></tr>");
        for ($i = 0; $i < $svr_array->arraysize(); $i++)
        {
            echo("<tr>");
            $svr = $svr_array->arraymem($i);  // retrieve servers (an array)
            $isvoice = "";
            $svridobj = $svr->arraymem(0);
            $svrid = $svridobj->scalarval();
            $isvoice = strlen(strstr($svrid, "voice"));
            echo("<td class=value>&nbsp;");
            if ($isvoice > 0)
            {
                $idlen = strlen($svrid);
                $idstart = strpos($svrid, ":") + 1;
                $id = substr($svrid, $idstart, $idlen - $idstart);
                echo("<a href=\"getports.php?serverid=" . $id . "\">view ports</a>");
            }
            echo("</td>");
            echo("<td class=value>" . $svrid . "&nbsp;</td>");

            $stateobj = $svr->arraymem(2);
            $state = $stateobj->scalarval();
            if ($state == 1)
                echo("<td class=value>Available</td>");
            else
                echo("<td class=value>Unkown</td>");
        }
        echo("</table>");
        echo("</center>");        
    }
    else 
    {
	print "Fault: ";
	print "Code: " . $r->faultCode() . " Reason '" .$r->faultString()."'<BR>";
    }
}
?>

<!--
//<?php
//print "Enter a service type to look up server details";
//print "<FORM  METHOD=\"POST\">
//<INPUT NAME=\"servertype\" VALUE=\"${servertype}\"><input type=\"submit\" value=\"go\" name=\"submit\">&nbsp;(1: voice, 2: appshare)</FORM><P>";
//?>
-->
<?php showFooter(); ?>
</body>
</html>
