<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Find communities with a given name
//
include("common.inc");
include("functions.inc");
include("xmlrpc.inc");
clearCache();
?>
<html>
<head>
<?php checkAuth() ?>
<title>Find Communities</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<body>
<?php showNavBar2(); ?>
<div id="content">
<h3>Find Communities</h3>
<?php
$sessionid = getSessionId();

// retrieve search string if set
$search_str = ($_GET['search']);
if ($search_str == "")
{
    $search_str = $HTTP_POST_VARS["search_str"];
}

// retrieve start row if set
$startrowid = ($_GET['start']);
if ($startrowid != "")
{
    $starting_row_id = $startrowid;
}
else if ($HTTP_POST_VARS["starting_row_id"] != "")
{
    $starting_row_id = $HTTP_POST_VARS["starting_row_id"];
}
else
{
    $starting_row_id = 0;
}

// retrieve total rows if set
$totalrowsid = ($_GET['total']);
if ($totalrowsid != "")
{
    $total_rows = $totalrowsid;
}
else if ($HTTP_POST_VARS["total_rows"] != "")
{
    $total_rows = $HTTP_POST_VARS["total_rows"];
}
else
{
    $total_rows = 5; // hard code for now
}

if ($search_str != "")
{
    $f=new xmlrpcmsg(WEBSVR_FN_SEARCH_COMMUNITIES,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($search_str, "string"),
                           new xmlrpcval($starting_row_id, "int"),
                           new xmlrpcval($total_rows, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();
    if (!$r->faultCode()) {
        $comm_array = $v->arraymem(0);          // primary array
        echo("<table border=\"1\">");
        echo("<tr><th class=title>Operations</th><th class=title>Community ID</th><th class=title>Community Name</th><th class=title>Account Name</th><th class=title>Contact Name</th><th class=title>Contact Email</th><th class=title>Contact Phone</th><th class=title>User Name</th><th class=title>User Password</th><th class=title>Bridge Phone</th><th class=title>System URL</th><th class=title>Account Type</th><th class=title>Account Status</th></tr>");
        for ($i = 0; $i < $comm_array->arraysize(); $i++)
        {
            // retrieve communities (an array)
            $commobj = $comm_array->arraymem($i);  
            $commidobj = $commobj->arraymem(0);

            echo("<tr>");

            // operations
            echo("<td class=title>");
            echo("<a href=\"updatecommunity.php?commid=" . $commidobj->scalarval() . "\">update</a><br>");
            echo("<a href=\"removecommunity.php?action=Delete&commid=" . $commidobj->scalarval() . "\">remove</a><br>");
            echo("<a href=\"enablecommunity.php?action=Ok&commid=" . $commidobj->scalarval() . "\">enable</a><br>");
            echo("</td>");

            echo("<td class=value>&nbsp;" . $commidobj->scalarval() . "</td>");
            $commnameobj = $commobj->arraymem(1);
            echo("<td class=value>&nbsp;" . $commnameobj->scalarval() . "</td>");
            $acctnumobj = $commobj->arraymem(2);
            echo("<td class=value>&nbsp;" . $acctnumobj->scalarval() . "</td>");
            $contactnameobj = $commobj->arraymem(3);
            echo("<td class=value>&nbsp;" . $contactnameobj->scalarval() . "</td>");
            $contactemailobj = $commobj->arraymem(4);
            echo("<td class=value>&nbsp;" . $contactemailobj->scalarval() . "</td>");
            $contactphoneobj = $commobj->arraymem(5);
            echo("<td class=value>&nbsp;" . $contactphoneobj->scalarval() . "</td>");
            $usernameobj = $commobj->arraymem(6);
            echo("<td class=value>&nbsp;" . $usernameobj->scalarval() . "</td>");
            $userpasswordobj = $commobj->arraymem(7);
            echo("<td class=value>&nbsp;" . $userpasswordobj->scalarval() . "</td>");
            $bridgephoneobj = $commobj->arraymem(8);
            echo("<td class=value>&nbsp;" . $bridgephoneobj->scalarval() . "</td>");
            $systemurlobj = $commobj->arraymem(9);
            echo("<td class=value>&nbsp;" . $systemurlobj->scalarval() . "</td>");
            $accounttypeobj = $commobj->arraymem(10);
            echo("<td class=value>&nbsp;" . accountType2String($accounttypeobj->scalarval()) . "</td>");
            $accountstatusobj = $commobj->arraymem(11);
            if ($accountstatusobj->scalarval() == "1")
            {
                echo("<td class=value>&nbsp;Enabled</td>");
            }
            else
            {
                echo("<td class=value>&nbsp;Disabled</td>");
            }
            // options not displayed
            // $optionsobj = $commobj->arraymem(12);
            
            echo("</tr>");
        }
        echo("</table>");

        $first_obj = $v->arraymem(1);
        $chunk_obj = $v->arraymem(2);
        $total_obj = $v->arraymem(3);
        $more_obj = $v->arraymem(4);

        $first = $first_obj->scalarval();

        // actual chunk returned: if chunk < DEFAULT, round back up
        $chunk = $chunk_obj->scalarval();
        if ($chunk < DEFAULT_CHUNK_SIZE)
            $chunk = DEFAULT_CHUNK_SIZE;

        $more  = $more_obj->scalarval();

        echo("<table>");
        echo("<tr><td>&nbsp;");
        if ($first > 0)
        {
            $newfirst = $first-$chunk;
            if ($newfirst < 0)
                $newfirst = 0;
            echo("<a href=\"searchcommunities.php?start=" . $newfirst . "&total=" . DEFAULT_CHUNK_SIZE . "&search=" . $search_str . "\">Prev</a> &nbsp;");
        }
        if ($more == 1)
        {
            $next = $first+$chunk;
            echo("<a href=\"searchcommunities.php?start=" . $next . "&total=" . DEFAULT_CHUNK_SIZE . "&search=" . $search_str . "\">Next</a>");
        }
        echo("</td></tr>");
        echo("</table>");

    } 
    else 
    {
        dumpFault($r);
    }
}
?>
<FORM METHOD="POST">
<table>
<tr><td>Community name:</td>
<td><INPUT NAME="search_str" VALUE="<?php print ${search_str};?>"></td></tr>
<!-- <tr><td>Starting ID:</td>
<!-- <td><INPUT NAME="starting_row_id" VALUE="<?php print ${starting_row_id};?>"></td></tr>
<!-- <tr><td>Total rows:</td>
<!-- <td><INPUT NAME="total_rows" VALUE="<?php print ${total_rows};?>"></td></tr>
-->
<tr><td colspan='2' align='right'><input type="submit" value="Search" name="submit"></td></tr>
<tr><td colspan='2' align='left'>Use '%' to match zero or more characters</td></tr>
</table>
</FORM>
</div>
<?php showFooter(); ?>
</body>
</html>
