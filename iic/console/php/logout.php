<?php

//
// Copyright 2004 Imidio, Inc.
//

//
// Basic login/authentication
//

include("xmlrpc.inc");
include("common.inc");

$logout = ($_GET['logout']);
if ($logout == 1)
{
    clearCookies();
}
else
{
	$msg = "Your session has expired. ";
}

?>

<html>
<head>
<title>Logged Out</title></head>
<body>

<?=$msg?>Please <a href="/imidio/console/frames.html" target="_top">click</a> to log in again.

</body>
</html>
