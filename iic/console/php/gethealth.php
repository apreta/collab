<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Find participant info based on PIN.
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<meta http-equiv='refresh' content='60;url=/imidio/console/php/gethealth.php'>
<title>Get Health</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<body>

<?php showNavBar1(); ?>
<?php

$sessionid = getSessionId();

if ($sessionid != "")
{
    $f=new xmlrpcmsg(WEBSVR_FN_GET_HEALTH,
                     array(new xmlrpcval($sessionid, "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();
    if (!$r->faultCode())
    {
//	print "<HR>Participant details (XML)<BR><PRE>" .
//            htmlentities($r->serialize()). "</PRE><HR>\n";

        // structure is:
        // array of values
        //   array of metric details
        //     where each metric is an array
        $health_array = $v->arraymem(0);          // primary array
        echo("<center>");

        $serverdate = date("l, d F Y h:i a",time());
        echo("Server status as of: " . $serverdate);

        echo("<table border=\"1\">");
        echo("<tr><th class=title>Name</th><th class=title>ID</th><th class=title>Location</th><th class=title>Status</th>");
//        print "<HR>Result details (XML)<BR><PRE>" .
//            htmlentities($r->serialize()). "</PRE><HR>\n";

        for ($i = 0; $i < $health_array->arraysize(); $i++)
        {
            echo("<tr>");
            $health = $health_array->arraymem($i);  // retrieve mtgs (an array)
            $nameobj = $health->arraymem(0);
            echo("<td class=value>&nbsp;" . $nameobj->scalarval() . "</td>");
            $idobj = $health->arraymem(1);
            echo("<td class=value>&nbsp;" . $idobj->scalarval() . "</td>");
            $locationobj = $health->arraymem(2);
            echo("<td class=value>&nbsp;" . $locationobj->scalarval() . "</td>");
            $statusobj = $health->arraymem(3);
            $status = $statusobj->scalarval();
            if ($status == 0)
                echo("<td class=value bgcolor='#FF0000'>Offline</td>");
            else
                echo("<td class=value bgcolor='#00FF00'>Online</td>");
        }

        echo("</table>");
        echo("</center>");
    } 
    else 
    {
	print "Fault: ";
	print "Code: " . $r->faultCode() . 
            " Reason '" .$r->faultString()."'<BR>";
    }
}
?>
<?php showFooter(); ?>
</body>
</html>
