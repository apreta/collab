<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Set logging level for a component
//

include("common.inc");
include("functions.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Set Logging</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<body>

<?php showNavBar1(); ?>

<?php
$serverid = ($_GET['serverid']);
$sessionid = getSessionId();
?>

<FORM METHOD="POST">
<table>
<tr><td align=center colspan=2><b><?php echo ($serverid); ?></b></td></tr>
<tr><td class=value>Level:</td>
<td class=value><SELECT NAME="level"><OPTION VALUE=7>Debug</OPTION><OPTION VALUE=6>Info</OPTION><OPTION VALUE=3>Error</OPTION></SELECT></td></tr>
<tr><td align=left><a href="getstatussummary.php">Back</a></td><td align='right'><input type="submit" value="Set" name="submit"></td></tr>
</table>
</FORM>

<?php
$level = $HTTP_POST_VARS["level"];
if ($sessionid != "" && $serverid != "" && $level != "")
{
    $dest = $serverid . ".log.set_level";
    $f=new xmlrpcmsg($dest,
                     array(new xmlrpcval($level, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $port_array=$r->value();
    if (!$r->faultCode()) 
    {
        if (strcmp($level,"7")==0)
        {
            $lstr = "Debug";
        }
        else if (strcmp($level,"6")==0)
        {
            $lstr = "Info";
        }
        else if (strcmp($level,"3")==0)
        {
            $lstr = "Error";
        }
        else
        {
            $lstr = "Unknown";
        }
        
        echo("<div><em>Log level now set to ". $lstr . "</em></div>");
    } 
    else 
    {
        print "Fault: ";
        print "Code: " . $r->faultCode() . " Reason '" .$r->faultString()."'<BR>";
    }
}
?>

<?php showFooter(); ?>
</body>
</html>
