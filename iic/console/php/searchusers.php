<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Search returning a list of users.
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Search for Users</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<body>
<?php showNavBar2(); ?>
<h3>Find Users</h3>
<?php
$sessionid = getSessionId();

// retrieve community id if set
$community_id = ($_GET['communityid']);
if ($community_id == "")
{
    $community_id = $HTTP_POST_VARS["community_id"];
}

// retrieve search name if set
$search_name = ($_GET['searchname']);
if ($search_name == "")
{
    $search_name = $HTTP_POST_VARS["search_name"];
}

// retrieve search email if set
$search_email = ($_GET['searchemail']);
if ($search_email == "")
{
    $search_email = $HTTP_POST_VARS["search_email"];
}

// retrieve start row if set
$startrowid = ($_GET['start']);
if ($startrowid != "")
{
    $starting_row_id = $startrowid;
}
else if ($HTTP_POST_VARS["starting_row_id"] != "")
{
    $starting_row_id = $HTTP_POST_VARS["starting_row_id"];
}
else
{
    $starting_row_id = 0; // default
}

// retrieve total rows if set
$totalrowsid = ($_GET['total']);
if ($totalrowsid != "")
{
    $total_rows = $totalrowsid;
}
else if ($HTTP_POST_VARS["total_rows"] != "")
{
    $total_rows = $HTTP_POST_VARS["total_rows"];
}
else
{
    $total_rows = DEFAULT_CHUNK_SIZE;
}

if ($community_id != "" && ($search_name != "" || $search_email != ""))
{
    $f=new xmlrpcmsg(WEBSVR_FN_SEARCH_USERS,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($community_id, "string"),
                           new xmlrpcval($search_name, "string"),
                           new xmlrpcval($search_email, "string"),
                           new xmlrpcval($starting_row_id, "int"),
                           new xmlrpcval($total_rows, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();
    if (!$r->faultCode()) {
        $user_array = $v->arraymem(0);          // primary array
        echo("<table border=\"1\">");
        echo("<tr><th class=title>Operations</th><th class=title>User ID</th><th class=title>Comm. ID</th><th class=title>Screenname</th>");
        echo("<th class=title>Is Admin</th><th class=title>Is Super</th><th class=title>Password</th><th class=title>User Type</th><th class=title>First Name</th><th class=title>Last Name</th>");
        echo("<th class=title>Default Phone</th><th class=title>Bus Phone</th><th class=title>Home Phone</th><th class=title>Mobile Phone</th><th class=title>Other Phone</th>");
        echo("<th class=title>Email</th></tr>");
        for ($i = 0; $i < $user_array->arraysize(); $i++)
        {
            // retrieve users (an array)
            $userobj = $user_array->arraymem($i);  
            $useridobj = $userobj->arraymem(0);

            echo("<tr>");

            // operations
            echo("<td class=title>");
            echo("<a href=\"setpassword.php?userid=" . $useridobj->scalarval() . "\">set password</a><br>");
            echo("<a href=\"updateuser.php?userid=" . $useridobj->scalarval() . "\">update</a><br>");
            echo("<a href=\"removeuser.php?userid=" . $useridobj->scalarval() . "\">remove</a><br>");
            echo("<a href=\"findmeetingsbyhost.php?hostid=" . $useridobj->scalarval() . "\">meetings</a><br>");
            echo("</td>");
            echo("<td class=value>&nbsp;" . $useridobj->scalarval() . "</td>");

            $commidobj = $userobj->arraymem(1);
            echo("<td class=value>&nbsp;" . $commidobj->scalarval() . "</td>");

            $usernameobj = $userobj->arraymem(2);
            echo("<td class=value>&nbsp;" . $usernameobj->scalarval() . "</td>");

            $isadminobj = $userobj->arraymem(3);
            echo("<td class=value>&nbsp;" . $isadminobj->scalarval() . "</td>");

            $issuperadminobj = $userobj->arraymem(4);
            echo("<td class=value>&nbsp;" . $issuperadminobj->scalarval() . "</td>");

            $pwdobj = $userobj->arraymem(5);
            echo("<td class=value>&nbsp;" . $pwdobj->scalarval() . "</td>");
            
            $typeobj = $userobj->arraymem(6);
            echo("<td class=value>&nbsp;" . $typeobj->scalarval() . "</td>");
            
            $firstnameobj = $userobj->arraymem(7);
            echo("<td class=value>&nbsp;" . $firstnameobj->scalarval() . "</td>");

            $lastnameobj = $userobj->arraymem(8);
            echo("<td class=value>&nbsp;" . $lastnameobj->scalarval() . "</td>");

            $defphoneobj = $userobj->arraymem(9);
            echo("<td class=value>&nbsp;" . $defphoneobj->scalarval() . "</td>");
            
            $busphoneobj = $userobj->arraymem(10);
            echo("<td class=value>&nbsp;" . $busphoneobj->scalarval() . "</td>");
            
            $homephoneobj = $userobj->arraymem(11);
            echo("<td class=value>&nbsp;" . $homephoneobj->scalarval() . "</td>");
            
            $mobilephoneobj = $userobj->arraymem(12);
            echo("<td class=value>&nbsp;" . $mobilephoneobj->scalarval() . "</td>");
            
            $otherphoneobj = $userobj->arraymem(13);
            echo("<td class=value>&nbsp;" . $otherphoneobj->scalarval() . "</td>");
            
            $emailobj = $userobj->arraymem(14);
            echo("<td class=value>&nbsp;" . $emailobj->scalarval() . "</td>");
            
            echo("</tr>");
        }
        echo("</table>");

        $first_obj = $v->arraymem(1);
        $chunk_obj = $v->arraymem(2);
        $total_obj = $v->arraymem(3);
        $more_obj = $v->arraymem(4);

        $first = $first_obj->scalarval();

        // actual chunk returned: if chunk < DEFAULT, round back up
        $chunk = $chunk_obj->scalarval();
        if ($chunk < DEFAULT_CHUNK_SIZE)
            $chunk = DEFAULT_CHUNK_SIZE;

        $more  = $more_obj->scalarval();

        echo("<table>");
        echo("<tr><td>&nbsp;");
        if ($first > 0)
        {
            $newfirst = $first-$chunk;
            if ($newfirst < 0)
                $newfirst = 0;
            echo("<a href=\"searchusers.php?start=" . $newfirst . "&total=" . DEFAULT_CHUNK_SIZE . "&searchname=" . $search_name . "&searchemail=" . $search_email . "&communityid=" . $community_id . "\">Prev</a> &nbsp;");
        }
        if ($more == 1)
        {
            $next = $first+$chunk;
            echo("<a href=\"searchusers.php?start=" . $next . "&total=" . DEFAULT_CHUNK_SIZE . "&searchname=" . $search_name . "&searchemail=" . $search_email . "&communityid=" . $community_id . "\">Next</a>");
        }
        echo("</td></tr>");
        echo("</table>");

    } else {
        dumpFault($r);
    }
}
?>
<FORM METHOD="POST">
<table>
<tr><td class=value>Community ID:</td>
<td class=value><INPUT NAME="community_id" VALUE="<?php print ${community_id};?>"></td></tr>
<tr><td class=value>Username:</td>
<td class=value><INPUT NAME="search_name" VALUE="<?php print ${search_name};?>"></td></tr>
<tr><td class=value>Email:</td>
<td class=value><INPUT NAME="search_email" VALUE="<?php print ${search_email};?>"></td></tr>
<tr><td colspan='2' align='right'><input type="submit" value="Find" name="submit"></td></tr>
</table>
</FORM>

<?php showFooter(); ?>
</body>
</html>
