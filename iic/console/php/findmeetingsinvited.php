<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Find participant info based on PIN.
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Find Meetings Invited</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<body>

<?php showNavBar2(); ?>
<?php

$sessionid = getSessionId();

$hostid = ($_GET['hostid']);
if ($hostid != "")
{
    $host_id = $hostid;
}
else if ($HTTP_POST_VARS["host_id"] != "")
{
    $host_id = $HTTP_POST_VARS["host_id"];
}

if ($sessionid != "" && $host_id != "")
{
    $f=new xmlrpcmsg(WEBSVR_FN_FIND_MEETINGS_INVITED,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($host_id, "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();
    if (!$r->faultCode()) {
        // structure is:
        // array of values
        //   array of mtg details
        //     where each mtg is an array
        //   start row
        //   number
        //   more
        //   total
        $mtg_array = $v->arraymem(0);          // primary array
        echo("<table border=\"1\">");
        echo("<tr><th class=title>Topic</th><th class=title>Mtg ID</th><th class=title>Host ID</th><th class=title>Part ID</th><th class=title>Type</th><th class=title>Privacy</th><th class=title>Description</th><th class=title>Time</th><th class=title>Duration</th><th class=title>Status</th><th class=title>Dialin</th><th class=title>URL</th></tr>");
        for ($i = 0; $i < $mtg_array->arraysize(); $i++)
        {
            echo("<tr>");
            $mtg = $mtg_array->arraymem($i);  // retrieve mtgs (an array)

            $mtgtopicobj = $mtg->arraymem(5);
            echo("<td class=value>&nbsp;" . $mtgtopicobj->scalarval() . "</td>");
            $mtgidobj = $mtg->arraymem(0);
            echo("<td class=value>&nbsp;" . $mtgidobj->scalarval() . "</td>");
            $hostidobj = $mtg->arraymem(1);
            echo("<td class=value>&nbsp;" . $hostidobj->scalarval() . "</td>");
            $partidobj = $mtg->arraymem(16);
            echo("<td class=value>&nbsp;" . $partidobj->scalarval() . "</td>");

            $typeobj = $mtg->arraymem(2);
            if ($typeobj->scalarval() == 1)
                echo("<td class=value>Instant</td>");
            else
                echo("<td class=value>Scheduled</td>");

            $privobj = $mtg->arraymem(3);
            if ($privobj->scalarval == 0)
                echo("<td class=value>Public</td>");
            else if ($privobj->scalarval == 1)
                echo("<td class=value>Unlisted</td>");
            if ($privobj->scalarval == 2)
                echo("<td class=value>Private</td>");

            $mtgdescobj = $mtg->arraymem(6);
            echo("<td class=value>&nbsp;" . $mtgdescobj->scalarval() . "</td>");

            $timeobj = $mtg->arraymem(9);
            $time = $timeobj->scalarval();
            if ($time == 0)
                echo("<td class=value>Instant</td>");
            else
                echo("<td class=value>&nbsp;" . rfcdate($time) . "</td>");

            $duraobj = $mtg->arraymem(8);
            echo("<td class=value>&nbsp;" . $duraobj->scalarval() . "</td>");

            $statusobj = $mtg->arraymem(13);
            $status = $statusobj->scalarval();
            if ($status == 1)
                echo("<td class=value>In progress</td>");
            else
                echo("<td class=value>Not active</td>");

            $bridgephoneobj = $mtg->arraymem(18);
            echo("<td class=value>&nbsp;" . $bridgephoneobj->scalarval() . "</td>");
            $urlobj = $mtg->arraymem(19);
            echo("<td class=value>&nbsp;" . $urlobj->scalarval() . "</td>");
        }
        echo("</table>");
        
//        print "<HR>Participant details (XML)<BR><PRE>" .
//            htmlentities($r->serialize()). "</PRE><HR>\n";
    }
    else
    {
	print "Fault: ";
	print "Code: " . $r->faultCode() . 
            " Reason '" .$r->faultString()."'<BR>";
    }
}

echo("Enter a user id to look up invited meetings");
echo("<FORM  METHOD=\"POST\">");
echo("<INPUT NAME=\"host_id\" VALUE=\"${host_id}\"><input type=\"submit\" value=\"go\" name=\"submit\"></FORM><P>");

?>

<?php showFooter(); ?>
</body>
</html>
