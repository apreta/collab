<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Remove a meeting
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Remove Meeting</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<body>

<?php showNavBar2(); ?>

<?php
// if mtg id was passed in, fill it into the form
$mtgid = ($_GET['mtgid']);
if ($mtgid != "")
{
    $mtg_id = $mtgid;
}
else if ($HTTP_POST_VARS["mtg_id"] != "")
{
    $mtg_id = $HTTP_POST_VARS["mtg_id"] !
}
?>
Enter a mtg id to remove the meeting
<FORM METHOD="POST">
<table>
<tr><td>Meeting Id:</td>
<td><INPUT NAME="mtg_id" VALUE="<?php print ${mtg_id};?>"></td></tr>
<tr><td colspan='2' align='right'><input type="submit" value="remove meeting" name="submit"></td></tr>
</table>
</FORM>

<?php
$sessionid = getSessionId();

if ($HTTP_POST_VARS["mtg_id"] != "")
{
    // get vals from form
    $user_id = $HTTP_POST_VARS["mtg_id"];

    $sessionid = getSessionId();

    $f=new xmlrpcmsg(WEBSVR_FN_REMOVE_MEETING,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($mtg_id, "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode()) 
    {
        $numrowsidobj = $v->arraymem(0);
        $numrows = $numrowsidobj->scalarval();
        if ($numrows >= 1)
            print "Meeting removed";
        else
            print "Meeting " . $mtg_id . " not found";
    }
    else  
    {
        dumpFault($r);
    }
}
?>
<?php showFooter(); ?>
</body>
</html>
