<html> 
<head> 
<title>rfcdate() Test</title> 
</head> 
<body> 

<?php 

function rfcdate ($tn) { 
// Translated from imap-4.7c/src/osdep/unix/env_unix.c 
// env-unix.c is Copyright 2000 by the University of Washington 
// localtime() not available in PHP3... 
//   $tn = time(0); 
   $zone = gmdate("H", $tn) * 60 + gmdate("i", $tn); 
   $julian = gmdate("z", $tn); 
   $t = getdate($tn); 
   $zone = $t[hours] * 60 + $t[minutes] - $zone; 

   echo("tn: " . $tn . " zone: " . $zone . " julian " . $julian . " t " . $t . " zone " . $zone);

   // 
   // julian can be one of: 
   //  36x  local time is December 31, UTC is January 1, offset -24 hours 
   //    1  local time is 1 day ahead of UTC, offset +24 hours 
   //    0  local time is same day as UTC, no offset 
   //   -1  local time is 1 day behind UTC, offset -24 hours 
   // -36x  local time is January 1, UTC is December 31, offset +24 hours 
   // 
   if ($julian = $t[yday] - $julian) { 
   $zone += (($julian < 0) == (abs($julian) == 1)) ? -24*60 : 24*60; 
   } 

   return date('D, d M Y H:i:s ', $tn) . sprintf("%03d%02d", $zone/60, abs($zone) % 60) . " (" . strftime("%Z") . ")"; 
} 

   echo rfcdate(time(0)); 

?> 

</body> 
</html> 
