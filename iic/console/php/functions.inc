<?php

function accountType2String($type)
{
    if ($type == 2)
        return "reserved";
    else if ($type == 1)
        return "system";
    else if ($type == 0)
        return "normal";
    else
        return "unknown";
}

function mtgOptions2String($options)
{
    $option_str = array(
        "", // 1
        "Screen callers", // 2
        "Lecture",  // 4
        "No new participants", // 8
        "Allow meeting overruns", // 16
        "Has password", // 32
        "Continue without moderator", // 64
        "Expirationless invitations", // 128

        // Begin GUI modifier options ( don't convert these )
        "", // 256
        "", // 512
        "", // 1024
        "", // 2048
        "", // 4096
        "", // 8192
        "", // 16384
        // End GUI modifier options

        "Invitations valid before start", // 32768
        "Recording meeting", // 65536
        "No join/leave announcements", // 131072
        );

    $to_return = ""; 
    for ($i = 0, $bit = 1; $i < count($option_str); $i++, $bit <<= 1)
    {
        if (($options & $bit) && ($option_str[$i] != ""))
        {
	    if (strlen($to_return) > 0)
            {
                $to_return = $to_return . "<br>&nbsp;";
	    }
            $to_return = $to_return . $option_str[$i];
        }
    }
    return $to_return;
}

function get_voice_bridge_status($sessionid)
{
    $out = array();
    
    $f=new xmlrpcmsg(WEBSVR_FN_GET_SERVERS,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval(1, "int")));
    $c = new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r = $c->send($f);
    $svr_array = $r->value();
    if (!$r->faultCode()) 
    {
        for ($i = 0; $i < $svr_array->arraysize(); $i++)
        {
            $svr = $svr_array->arraymem($i);  // retrieve servers (an array)
            $svridobj = $svr->arraymem(0);
            $svrid = $svridobj->scalarval();
            $idlen = strlen($svrid);
            $idstart = strpos($svrid, ":") + 1;
            $id = substr($svrid, $idstart, $idlen - $idstart);	// strip off voice:
            $stateobj = $svr->arraymem(2);
            $state = $stateobj->scalarval();
            $out[$id] = $state;
        }
    }

    return $out;
}

function get_docshare_status($sessionid)
{
    $out = array();
    
    $f=new xmlrpcmsg(WEBSVR_FN_GET_SERVERS,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval(4, "int")));
    $c = new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r = $c->send($f);
    $svr_array = $r->value();
    if (!$r->faultCode()) 
    {
        for ($i = 0; $i < $svr_array->arraysize(); $i++)
        {
            $svr = $svr_array->arraymem($i);  // retrieve servers (an array)
            $svridobj = $svr->arraymem(0);
            $svrid = $svridobj->scalarval();

            $stateobj = $svr->arraymem(2);
            $state = $stateobj->scalarval();

            $out[] = array($svrid, $state);
        }
    }

    return $out;
}

function get_share_status($sessionid)
{
    $out = array();
    
    $f=new xmlrpcmsg(WEBSVR_FN_GET_SERVERS,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval(2, "int")));
    $c = new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r = $c->send($f);
    $svr_array = $r->value();
    if (!$r->faultCode()) 
    {
        for ($i = 0; $i < $svr_array->arraysize(); $i++)
        {
            $svr = $svr_array->arraymem($i);  // retrieve servers (an array)
            $svridobj = $svr->arraymem(0);
            $svrid = $svridobj->scalarval();

            $stateobj = $svr->arraymem(2);
            $state = $stateobj->scalarval();

            $out[] = array($svrid, $state);
        }
    }

    return $out;
}

function get_servers($sessionid, $servertype)
{
    if ($sessionid != "" && $servertype != "")
    {
        echo("<center>");
        echo("<b>");
        if ($servertype == 1)
            echo("Voice Bridge Status<p>");
        else if ($servertype == 2)
            echo("App/Desktop Sharing Status<p>");
        else if ($servertype == 4)
            echo("Document Sharing Status<p>");
        echo("</b>");
        $f=new xmlrpcmsg(WEBSVR_FN_GET_SERVERS,
                         array(new xmlrpcval($sessionid, "string"),
                               new xmlrpcval($servertype, "int")));
        $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
        $r=$c->send($f);
        $svr_array=$r->value();
        if (!$r->faultCode()) 
        {
            // structure is:
            //   array of svr details
            //     where details of each server is an array
            echo("<table border=\"1\">");
            echo("<tr><th class=title>Operations</th><th class=title>Server ID</th><th class=title>State</th></tr>");
            for ($i = 0; $i < $svr_array->arraysize(); $i++)
            {
                echo("<tr>");
                $svr = $svr_array->arraymem($i);  // retrieve servers (an array)
                $isvoice = "";
                $svridobj = $svr->arraymem(0);
                $svrid = $svridobj->scalarval();
                $isvoice = strlen(strstr($svrid, "voice"));
                echo("<td class=value>&nbsp;");
                if ($isvoice > 0)
                {
                    $idlen = strlen($svrid);
                    $idstart = strpos($svrid, ":") + 1;
                    $id = substr($svrid, $idstart, $idlen - $idstart);
                    echo("<a href=\"getports.php?serverid=" . $id . "\">view ports</a>");
                }
                echo("</td>");
                echo("<td class=value>" . $svrid . "&nbsp;</td>");
                
                $stateobj = $svr->arraymem(2);
                $state = $stateobj->scalarval();
                if ($state == 1)
                    echo("<td class=value>Available</td>");
                else
                    echo("<td class=value>Unkown</td>");
            }
            echo("</table>");
            echo("</center>");        
        }
        else 
        {
            print "Fault: ";
            print "Code: " . $r->faultCode() . " Reason '" .$r->faultString()."'<BR>";
        }
    }
}

// get meeting count
function get_meeting_count($sessionid)
{
    $total = 0;

    if ($sessionid!="") 
    {
        $f=new xmlrpcmsg(WEBSVR_FN_GET_MEETINGS,
                         array(new xmlrpcval($sessionid, "string")));
        $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
        $r=$c->send($f);
        $v=$r->value();
        if (!$r->faultCode())
        {
            // structure is:
            // array of values
            //   array of mtg details
            //     where each mtg is an array
            //   start row
            //   number
            //   more
            //   total
            $mtg_array = $v->arraymem(0);          // primary array
            $total = $mtg_array->arraysize();
        }
        else
        {
            print "Fault: ";
            print "Code: " . $r->faultCode() . 
                " Reason '" .$r->faultString()."'<BR>";
        }
    }
    return $total;
}

function selectCommunityId($sessionid, $selectAction)
{
    // retrieve start row if set
    if ($HTTP_POST_VARS["starting_row_id"] != "")
    {
        $starting_row_id = $HTTP_POST_VARS["starting_row_id"];
    }
    else
    {
        $starting_row_id = 0;
    }

    if ($action == "Next")
    {
        $chunk = $HTTP_POST_VARS["chunk"];
        $starting_row_id += $chunk;
    }
    if ($HTTP_POST_VARS["action"] == "Prev")
    {
        $chunk = $HTTP_POST_VARS["chunk"];
        $starting_row_id -= $chunk;
    }
    
    if ($starting_row_id >= 0)
    {
        $f=new xmlrpcmsg(WEBSVR_FN_FETCH_COMMUNITY_LIST,
                         array(new xmlrpcval($sessionid, "string"),
                               new xmlrpcval($starting_row_id, "int"),
                               new xmlrpcval(DEFAULT_CHUNK_SIZE, "int")));
        $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
        $r=$c->send($f);
        $v=$r->value();
        if (!$r->faultCode()) {
            echo("<FORM METHOD=\"POST\">");
            echo("<INPUT TYPE=hidden NAME=\"starting_row_id\" VALUE=\"" . $starting_row_id . "\">");
            echo("<INPUT TYPE=hidden name=chunk value=" . DEFAULT_CHUNK_SIZE . ">");
            echo("<SELECT name=\"commid\" size=\"" . DEFAULT_CHUNK_SIZE . "\">");
            $comm_array = $v->arraymem(0);          // primary array
            for ($i = 0; $i < $comm_array->arraysize(); $i++)
            {
                // retrieve communities (an array)
                $commobj = $comm_array->arraymem($i);  
                $commidobj = $commobj->arraymem(0);
                $commnameobj = $commobj->arraymem(1);
                
                echo("<OPTION NAME=commid VALUE=\"");
                echo($commidobj->scalarval());
                echo("\">");
                echo($commnameobj->scalarval());
                echo("</OPTION>");
            }
            echo("</SELECT>");

            $first_obj = $v->arraymem(1);
            $first     = $first_obj->scalarval();
            
            $chunk_obj = $v->arraymem(2);
            $chunk     = $chunk_obj->scalarval();
            
            $total_obj = $v->arraymem(3);
            
            $more_obj  = $v->arraymem(4);
            $more      = $more_obj->scalarval();
            
            if ($first > 0)
            {
                $newfirst = $first-$chunk;
                if ($newfirst >= 0)
                    echo("<input type=submit name=action value=\"Prev\">");
            }
            if ($more == 1)
            {
                echo("<input type=submit name=action value=\"Next\">");
            }
            echo("<br>");
            echo("<input type=submit name=action value=\"" . $selectAction . "\">");
            echo("</FORM>");
        }
        else
        {
            dumpFault($r);
        }
    }
}

function showNavBar1()
{
    $sessionid = getSessionId();
    echo("<div id=\"leftcol\">
          <h4>Status</h4>
          <ul>
            <li><a href=\"getstatussummary.php\">View Components</a>
            <li><a href=\"getmeetings.php\">View Active Meetings&nbsp;(" . get_meeting_count($sessionid) . ")</a>
          </ul>
          <a href=\"logout.php?logout=1\" target=dashboard>Logout</a>
          </div>"
         );
    
}
?>
