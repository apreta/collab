<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Add a user
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Add User</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<body>
<?php showNavBar2(); ?>
<h3>Add User</h3>
<FORM METHOD="POST"><br>
<table>
<tr><td class=value align=right>User Password: </td>
<td class=value><INPUT NAME="user_password" VALUE="<?php print ${user_password};?>"></td></tr>
<tr><td class=value align=right>Community Id:</td>
<td class=value><INPUT NAME="community_id" VALUE="<?php print ${community_id};?>"></td></tr>
<tr><td class=value align=right>Title: </td>
<td class=value><INPUT NAME="user_title" VALUE="<?php print ${user_title};?>"></td></tr>
<tr><td class=value align=right>First Name: </td>
<td class=value><INPUT NAME="user_first" VALUE="<?php print ${user_first};?>"></td></tr>
<tr><td class=value align=right>Middle Name: </td>
<td class=value><INPUT NAME="user_middle" VALUE="<?php print ${user_middle};?>"></td></tr>
<tr><td class=value align=right>Last Name: </td>
<td class=value><INPUT NAME="user_last" VALUE="<?php print ${user_last};?>"></td></tr>
<tr><td class=value align=right>Suffix: </td>
<td class=value><INPUT NAME="user_suffix" VALUE="<?php print ${user_suffix};?>"></td></tr>
<tr><td class=value align=right>Company: </td>
<td class=value><INPUT NAME="company" VALUE="<?php print ${company};?>"></td></tr>
<tr><td class=value align=right>Job Title: </td>
<td class=value><INPUT NAME="job_title" VALUE="<?php print ${job_title};?>"></td></tr>
<tr><td class=value align=right>Address1: </td>
<td class=value><INPUT NAME="address1" VALUE="<?php print ${address1};?>"></td></tr>
<tr><td class=value align=right>Address2: </td>
<td class=value><INPUT NAME="address2" VALUE="<?php print ${address2};?>"></td></tr>
<tr><td class=value align=right>State: </td>
<td class=value><INPUT NAME="state" VALUE="<?php print ${state};?>"></td></tr>
<tr><td class=value align=right>Country: </td>
<td class=value><INPUT NAME="country" VALUE="<?php print ${country};?>"></td></tr>
<tr><td class=value align=right>Postal Code: </td>
<td class=value><INPUT NAME="postal_code" VALUE="<?php print ${postal_code};?>"></td></tr>
<tr><td class=value align=right>Screenname: </td>
<td class=value><INPUT NAME="user_screen" VALUE="<?php print ${user_screen};?>"></td></tr>
<tr><td class=value align=right>Email1: </td>
<td class=value><INPUT NAME="email1" VALUE="<?php print ${email1};?>"></td></tr>
<tr><td class=value align=right>Email2: </td>
<td class=value><INPUT NAME="email2" VALUE="<?php print ${email2};?>"></td></tr>
<tr><td class=value align=right>Email3: </td>
<td class=value><INPUT NAME="email3" VALUE="<?php print ${email3};?>"></td></tr>
<tr><td class=value align=right>Business Phone: </td>
<td class=value><INPUT NAME="business_phone" VALUE="<?php print ${business_phone};?>"></td></tr>
<tr><td class=value align=right>Home Phone: </td>
<td class=value><INPUT NAME="home_phone" VALUE="<?php print ${home_phone};?>"></td></tr>
<tr><td class=value align=right>Mobile Phone: </td>
<td class=value><INPUT NAME="mobile_phone" VALUE="<?php print ${mobile_phone};?>"></td></tr>
<tr><td class=value align=right>Other Phone: </td>
<td class=value><INPUT NAME="other_phone" VALUE="<?php print ${other_phone};?>"></td></tr>
<tr><td class=value align=right>Extension: </td>
<td class=value><INPUT NAME="extension" VALUE="<?php print ${extension};?>"></td></tr>
<tr><td class=value align=right>AIM Screen Name: </td>
<td class=value><INPUT NAME="aim_screen" VALUE="<?php print ${aim_screen};?>"></td></tr>
<tr><td class=value align=right>Yahoo Screen Name: </td>
<td class=value><INPUT NAME="yahoo_screen" VALUE="<?php print ${yahoo_screen};?>"></td></tr>
<tr><td class=value align=right>MSN Screen Name: </td>
<td class=value><INPUT NAME="msn_screen" VALUE="<?php print ${msn_screen};?>"></td></tr>
<tr><td class=value align=right>User1: </td>
<td class=value><INPUT NAME="user1" VALUE="<?php print ${user1};?>"></td></tr>
<tr><td class=value align=right>User2: </td>
<td class=value><INPUT NAME="user2" VALUE="<?php print ${user2};?>"></td></tr>
<tr><td class=value align=right>Default Phone: </td>
<td class=value><INPUT NAME="default_phone" VALUE="<?php print ${default_phone};?>"></td></tr>
<tr><td class=value align=right>Default Email: </td>
<td class=value><INPUT NAME="default_email" VALUE="<?php print ${default_email};?>"></td></tr>
<tr><td class=value align=right>Type: </td>
<td class=value><INPUT NAME="type" VALUE="<?php print ${type};?>"></td></tr>
<tr><td class=value align=right>Profile ID: (blank for default)</td>
<td class=value><INPUT NAME="profile_id" VALUE="<?php print ${profile_id};?>"></td></tr>
<tr><td colspan='2' align='right'><input type="submit" value="Add" name="submit"></td></tr>
</table>
</FORM>

<?php
$sessionid = getSessionId();

if ($HTTP_POST_VARS["community_id"]!="")
{
    // get vals from form
    $user_password = $HTTP_POST_VARS["user_password"];
    $community_id = $HTTP_POST_VARS["community_id"];
    $user_title = $HTTP_POST_VARS["user_title"];
    $user_first = $HTTP_POST_VARS["user_first"];
    $user_middle = $HTTP_POST_VARS["user_middle"];
    $user_last = $HTTP_POST_VARS["user_last"];
    $user_suffix = $HTTP_POST_VARS["user_suffix"];
    $company = $HTTP_POST_VARS["company"];
    $job_title = $HTTP_POST_VARS["job_title"];
    $address1 = $HTTP_POST_VARS["address1"];
    $address2 = $HTTP_POST_VARS["address2"];
    $state = $HTTP_POST_VARS["state"];
    $country = $HTTP_POST_VARS["country"];
    $postal_code = $HTTP_POST_VARS["postal_code"];
    $user_screen = $HTTP_POST_VARS["user_screen"];
    $email1 = $HTTP_POST_VARS["email1"];
    $email2 = $HTTP_POST_VARS["email2"];
    $email3 = $HTTP_POST_VARS["email3"];
    $business_phone = $HTTP_POST_VARS["business_phone"];
    $home_phone = $HTTP_POST_VARS["home_phone"];
    $mobile_phone = $HTTP_POST_VARS["mobile_phone"];
    $other_phone = $HTTP_POST_VARS["other_phone"];
    $extension = $HTTP_POST_VARS["extension"];
    $aim_screen = $HTTP_POST_VARS["aim_screen"];
    $yahoo_screen = $HTTP_POST_VARS["yahoo_screen"];
    $msn_screen = $HTTP_POST_VARS["msn_screen"];
    $user1 = $HTTP_POST_VARS["user1"];
    $user2 = $HTTP_POST_VARS["user2"];
    $default_phone = $HTTP_POST_VARS["default_phone"];
    if ($default_phone == "")
        $default_phone = 0;
    $default_email = $HTTP_POST_VARS["default_email"];
    if ($default_email == "")
        $default_email = 0;
    $type = $HTTP_POST_VARS["type"];
    if ($type == "")
        $type = 1;
    $profile_id = $HTTP_POST_VARS["profile_id"];

    $user_id = "";

    $sessionid = getSessionId();

    $f=new xmlrpcmsg(WEBSVR_FN_ADD_USER,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($user_password, "string"),
                           new xmlrpcval($community_id, "string"),
                           new xmlrpcval($user_title, "string"),
                           new xmlrpcval($user_first, "string"),
                           new xmlrpcval($user_middle, "string"),
                           new xmlrpcval($user_last, "string"),
                           new xmlrpcval($user_suffix, "string"),
                           new xmlrpcval($company, "string"),
                           new xmlrpcval($job_title, "string"),
                           new xmlrpcval($address1, "string"),
                           new xmlrpcval($address2, "string"),
                           new xmlrpcval($state, "string"),
                           new xmlrpcval($country, "string"),
                           new xmlrpcval($postal_code, "string"),
                           new xmlrpcval($user_screen, "string"),
                           new xmlrpcval($email1, "string"),
                           new xmlrpcval($email2, "string"),
                           new xmlrpcval($email3, "string"),
                           new xmlrpcval($business_phone, "string"),
                           new xmlrpcval($home_phone, "string"),
                           new xmlrpcval($mobile_phone, "string"),
                           new xmlrpcval($other_phone, "string"),
                           new xmlrpcval($extension, "string"),
                           new xmlrpcval($aim_screen, "string"),
                           new xmlrpcval($yahoo_screen, "string"),
                           new xmlrpcval($msn_screen, "string"),
                           new xmlrpcval($user1, "string"),
                           new xmlrpcval($user2, "string"),
                           new xmlrpcval($default_phone, "string"),
                           new xmlrpcval($default_email, "string"),
                           new xmlrpcval($type, "string"),
                           new xmlrpcval($profile_id, "string"),
                           new xmlrpcval($user_id, "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode()) 
    {
        $statusobj = $v->arraymem(0); // status
        $status = $statusobj->scalarval();
        if ($status == 0)
            $status_str = "Added";
        else if ($status == -4013)
            $status_str = "Duplicate username";

        $useridobj = $v->arraymem(1); // userid
        $userid = $useridobj->scalarval();
        print "User: " . $user_screen . " status: " . $status_str . "<br>New user id: " . $userid;
    }
    else  
    {
        dumpFault($r);
    }
}
?>
<?php showFooter(); ?>
</body>
</html>
