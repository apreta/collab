<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Set logging level for jabber
//

include("common.inc");
include("functions.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Set XML Router Logging</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<body>

<?php showNavBar1(); ?>

<FORM METHOD="POST">
<table>
<tr><td align=center colspan=2><b>XML Router</b></td></tr>
<tr><td class=value>Level:</td>
<td class=value><SELECT NAME="level"><OPTION VALUE=0>Normal</OPTION><OPTION VALUE=1>Debug</OPTION></SELECT></td></tr>
<tr><td align=left><a href="getstatussummary.php">Back</a></td><td align='right'><input type="submit" value="Set" name="submit"></td></tr>
</table>
</FORM>

<?php
$sessionid = getSessionId();
$level = $HTTP_POST_VARS["level"];
if ($level != "")
{
    if (strcmp($level, "1") == 0)
    {
        $dest = "jabber.debug_on";
        $lstr = "debug";
    }
    else
    {
        $dest = "jabber.debug_off";
        $lstr = "normal";
    }

    $f=new xmlrpcmsg($dest, array(new xmlrpcval($level, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $port_array=$r->value();
    if (!$r->faultCode()) 
    {
        echo("<div><em>Log level is now set to " . $lstr . "</em></div>");
    } 
    else 
    {
        print "Fault: ";
        print "Code: " . $r->faultCode() . " Reason '" .$r->faultString()."'<BR>";
    }
}
?>

<?php showFooter(); ?>
</body>
</html>
