<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Set user password
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Set Password</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<body>

<?php showNavBar2(); ?>

<?php
// if user id was passed in, fill it into the form
$userid = ($_GET['userid']);
if ($userid != "")
{
    $user_id = $userid;
}
?>
Enter a user id and password
<FORM METHOD="POST">
<table>
<tr><td class=value>User Id:</td>
<td class=value><INPUT NAME="user_id" VALUE="<?php print ${user_id};?>"></td></tr>
<tr><td class=value>Old Password:</td>
<td class=value><INPUT NAME="old_password" VALUE="<?php print ${old_password};?>"></td></tr>
<tr><td class=value>New Password:</td>
<td class=value><INPUT NAME="new_password" VALUE="<?php print ${new_password};?>"></td></tr>
<tr><td class=value>Admin Override?:</td>
<td class=value><INPUT NAME="override" type="CHECKBOX" VALUE="1"></td></tr>
<tr><td colspan='2' align='right'><input type="submit" value="set password" name="submit"></td></tr>
</table>
</FORM>

<?php
$sessionid = getSessionId();

if ($HTTP_POST_VARS["user_id"]!="")
{
    // get vals from form
    $user_id = $HTTP_POST_VARS["user_id"];
    $old_password = $HTTP_POST_VARS["old_password"];
    $new_password = $HTTP_POST_VARS["new_password"];
    $override = $HTTP_POST_VARS["override"];

    $sessionid = getSessionId();
    if ($override == "")
    {
        $override_i = 0;
    }
    else
    {
        $override_i = 1;
    }

    $f=new xmlrpcmsg(WEBSVR_FN_SET_PASSWORD,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($user_id, "string"),
                           new xmlrpcval($old_password, "string"),
                           new xmlrpcval($new_password, "string"),
                           new xmlrpcval($override_i, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode()) 
    {
        $resultobj = $v->arraymem(0);
        $result = $resultobj->scalarval();
        if ($result == 1)
            print "Password changed";
        else
            print "Password not changed";
    }
    else  
    {
        dumpFault($r);
    }
}
?>
<?php showFooter(); ?>
</body>
</html>
