<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Fetch list of communities.
//

include("common.inc");
include("functions.inc");
include("xmlrpc.inc");
clearCache();
?>
<html>
<head>
<?php checkAuth() ?>
<title>Fetch List of Communities</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<BODY>
<?php showNavBar2(); ?>
<div id="content"/>
<h3>Community List</h3>
<?php
$sessionid = getSessionId();

// retrieve start row if set
$startrowid = ($_GET['start']);
if ($startrowid != "")
{
    $starting_row_id = $startrowid;
}
else if ($HTTP_POST_VARS["starting_row_id"] != "")
{
    $starting_row_id = $HTTP_POST_VARS["starting_row_id"];
}
else
{
    $starting_row_id = 0;
}

// retrieve total rows if set
$totalrowsid = ($_GET['total']);
if ($totalrowsid != "")
{
    $total_rows = $totalrowsid;
}
else if ($HTTP_POST_VARS["total_rows"] != "")
{
    $total_rows = $HTTP_POST_VARS["total_rows"];
}
else
{
    $total_rows = DEFAULT_CHUNK_SIZE; // hard code for now
}

if ($starting_row_id >= 0 && $total_rows > 0)
{
    $f=new xmlrpcmsg(WEBSVR_FN_FETCH_COMMUNITY_LIST,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($starting_row_id, "int"),
                           new xmlrpcval($total_rows, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();
    if (!$r->faultCode()) {
        $comm_array = $v->arraymem(0);          // primary array
        echo("<table border=\"1\">");
        echo("<tr><th class=title>Operations</th><th class=title>Community ID</th><th class=title>Community Name</th><th class=title>Account Number</th><th class=title>Contact Name</th><th class=title>Contact Email</th><th class=title>Contact Phone</th><th class=title>User Name</th><th class=title>Bridge Phone</th><th class=title>System URL</th><th class=title>Account Type</th><th class=title>Account Status</th></tr>");
        for ($i = 0; $i < $comm_array->arraysize(); $i++)
        {
            // retrieve communities (an array)
            $commobj = $comm_array->arraymem($i);  
            $commidobj = $commobj->arraymem(0);

            if ($commidobj->scalarval() == "-1")
                continue;
            
            echo("<tr>");

            // operations
            echo("<td class=title>");
            echo("<a href=\"updatecommunity.php?action=Edit&commid=" . $commidobj->scalarval() . "\">update</a><br>");
            echo("<a href=\"removecommunity.php?action=Delete&commid=" . $commidobj->scalarval() . "\">remove</a><br>");
            $accountstatusobj = $commobj->arraymem(11);
            $status = $accountstatusobj->scalarval();
            if ($status == 1)
                echo("<a href=\"enablecommunity.php?action=Update&enable&commid=" . $commidobj->scalarval() . "\">disable</a><br>");
            if ($status == 0)
                echo("<a href=\"enablecommunity.php?action=Update&enable=1&commid=" . $commidobj->scalarval() . "\">enable</a><br>");
            echo("</td>");

            echo("<td class=value>&nbsp;" . $commidobj->scalarval() . "</td>");
            $commnameobj = $commobj->arraymem(1);
            echo("<td class=value>&nbsp;" . $commnameobj->scalarval() . "</td>");
            $acctnumobj = $commobj->arraymem(2);
            echo("<td class=value>&nbsp;" . $acctnumobj->scalarval() . "</td>");
            $contactnameobj = $commobj->arraymem(3);
            echo("<td class=value>&nbsp;" . $contactnameobj->scalarval() . "</td>");
            $contactemailobj = $commobj->arraymem(4);
            echo("<td class=value>&nbsp;" . $contactemailobj->scalarval() . "</td>");
            $contactphoneobj = $commobj->arraymem(5);
            echo("<td class=value>&nbsp;" . $contactphoneobj->scalarval() . "</td>");
            $usernameobj = $commobj->arraymem(6);
            echo("<td class=value>&nbsp;" . $usernameobj->scalarval() . "</td>");
            $bridgephoneobj = $commobj->arraymem(8);
            echo("<td class=value>&nbsp;" . $bridgephoneobj->scalarval() . "</td>");
            $systemurlobj = $commobj->arraymem(9);
            echo("<td class=value>&nbsp;" . $systemurlobj->scalarval() . "</td>");
            
            $accounttypeobj = $commobj->arraymem(10);
            $type = $accounttypeobj->scalarval();
            $type_str = accountType2String($type);
            echo("<td class=value>&nbsp;" . $type_str . "</td>");

            $accountstatusobj = $commobj->arraymem(11);
            $status = $accountstatusobj->scalarval();
            if ($status == 1)
                $status_str = "enabled";
            if ($status == 0)
                $status_str = "disabled";
            echo("<td class=value>&nbsp;" . $status_str . "</td>");
            echo("</tr>");
        }
        echo("</table>");

        $first_obj = $v->arraymem(1);
        $chunk_obj = $v->arraymem(2);
        $total_obj = $v->arraymem(3);
        $more_obj = $v->arraymem(4);

        $first = $first_obj->scalarval();

        // actual chunk returned: if chunk < DEFAULT, round back up
        $chunk = $chunk_obj->scalarval();
        if ($chunk < DEFAULT_CHUNK_SIZE)
            $chunk = DEFAULT_CHUNK_SIZE;

        $more  = $more_obj->scalarval();

        echo("<table>");
        echo("<tr><td>&nbsp;");
        if ($first > 0)
        {
            $newfirst = $first-$chunk;
            if ($newfirst < 0)
                $newfirst = 0;
            echo("<a href=\"fetchcommunitylist.php?start=" . $newfirst . "&total=" . DEFAULT_CHUNK_SIZE . "\">Prev</a> &nbsp;");
        }
        if ($more == 1)
        {
            $next = $first+$chunk;
            echo("<a href=\"fetchcommunitylist.php?start=" . $next . "&total=" . DEFAULT_CHUNK_SIZE . "\">Next</a>");
        }
        echo("</td></tr>");
        echo("</table>");

    } 
    else 
    {
        dumpFault($r);
    }
}
?>
</div>
<?php showFooter(); ?>
</body>
</html>
