<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Get port usage.
//

include("common.inc");
include("functions.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Reset Port</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<body>

<?php showNavBar1(); ?>

<?php
// serverid if set
$serverid = ($_GET['serverid']);
if ($serverid == "")
{
    $serverid = $HTTP_POST_VARS["serverid"];
}

// portid if set
$portid = ($_GET['portid']);
if ($portid == "")
{
    $portid = $HTTP_POST_VARS["portid"];
}

$sessionid = getSessionId();

if ($sessionid != "" && $serverid != "" && $portid != "")
{
    $f=new xmlrpcmsg(WEBSVR_FN_RESET_PORT,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($serverid, "string"),
                           new xmlrpcval($portid, "string"),
                           new xmlrpcval("", "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $port_array=$r->value();
    if (!$r->faultCode()) 
    {
        echo("<div><em>Port has been reset</em></div>");
    } 
    else 
    {
	print "Fault: ";
	print "Code: " . $r->faultCode() . " Reason '" .$r->faultString()."'<BR>";
    }
}
?>

<!--
//<?php
//print "Enter a VOICE server id to look up port details";
//print "<FORM  METHOD=\"POST\">
//<INPUT NAME=\"serverid\" VALUE=\"${serverid}\"><input type=\"submit\" value=\"go\" name=\"submit\"></FORM><P>";
//?>
-->
<?php showFooter(); ?>
</body>
</html>
