<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Update a community
//
include("common.inc");
include("functions.inc");
include("xmlrpc.inc");
clearCache();
?>
<html>
<head>
<?php checkAuth() ?>
<title>Update a Community</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<body>
<?php showNavBar2(); ?>
<div id="content">
<h3>Update Community</h3>
<?php

$commid = $HTTP_POST_VARS["commid"];
if ($commid == "")
{
    $commid= ($_GET['commid']);
}

$action = $HTTP_POST_VARS["action"];
if ($action == "")
{
    $action= ($_GET['action']);
}

$sessionid = getSessionId();

if ($commid == "")
{
    selectCommunityId($sessionid, "Edit");
}
else if ($action == "Edit" && $commid != "") // i.e. selected an id and hit "Edit" button
{
    // do fetch
    $sessionid = getSessionId();
 
    $f=new xmlrpcmsg(WEBSVR_FN_FETCH_COMMUNITY,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($commid, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();    
    if (!$r->faultCode()) 
    {
        // read in values into vars that will populate form
        $comm_array = $v->arraymem(0);
        $commobj = $comm_array->arraymem(0);

        $commidobj = $commobj->arraymem(0);
        $commid = $commidobj->scalarval();

        $commnameobj = $commobj->arraymem(1);
        $community_name = $commnameobj->scalarval();

        $acctnumobj = $commobj->arraymem(2);
        $account_number = $acctnumobj->scalarval();

        $contactnameobj = $commobj->arraymem(3);
        $contact_name = $contactnameobj->scalarval();

        $contactemailobj = $commobj->arraymem(4);
        $contact_email = $contactemailobj->scalarval();

        $contactphoneobj = $commobj->arraymem(5);
        $contact_phone = $contactphoneobj->scalarval();

        $usernameobj = $commobj->arraymem(6);
        $user_name = $usernameobj->scalarval();

        $userpasswordobj = $commobj->arraymem(7);
        $user_password = $userpasswordobj->scalarval();

        $bridgephoneobj = $commobj->arraymem(8);
        $bridge_phone = $bridgephoneobj->scalarval();

        $systemurlobj = $commobj->arraymem(9);
        $system_url = $systemurlobj->scalarval();

        $accounttypeobj = $commobj->arraymem(10);
        $accounttype = $accounttypeobj->scalarval();

        $accountstatusobj = $commobj->arraymem(11);
        $accountstatus = $accountstatusobj->scalarval();

        echo("<FORM METHOD=\"POST\">");
        echo("<table>");
        echo("<input name=\"commid\" type=hidden value=\"" . $commid . "\">");
        echo("<tr><td class=value align=right>Community Name:</td>");
        echo("<td class=value><INPUT NAME=\"community_name\" VALUE=\"" . $community_name . "\"></td></tr>");
        echo("<tr><td class=value align=right>Account Name: </td>");
        echo("<td class=value><INPUT NAME=\"account_number\" VALUE=\"" . $account_number . "\"></td></tr>");
        echo("<tr><td class=value align=right>Contact Name: </td>");
        echo("<td class=value><INPUT NAME=\"contact_name\" VALUE=\"" . $contact_name . "\"></td></tr>");
        echo("<tr><td class=value align=right>Contact Email: </td>");
        echo("<td class=value><INPUT NAME=\"contact_email\" VALUE=\"" . $contact_email . "\"></td></tr>");
        echo("<tr><td class=value align=right>Contact Phone: </td>");
        echo("<td class=value><INPUT NAME=\"contact_phone\" VALUE=\"" . $contact_phone . "\"></td></tr>");
        echo("<tr><td class=value align=right>Admin Name: </td>");
        echo("<td class=value><INPUT NAME=\"user_name\" VALUE=\"" . $user_name . "\"></td></tr>");
        echo("<tr><td class=value align=right>Admin Password: </td>");
        echo("<td class=value><INPUT NAME=\"user_password\" VALUE=\"" . $user_password . "\"></td></tr>");
        echo("<tr><td class=value align=right>Bridge Phone: </td>");
        echo("<td class=value><SELECT NAME=\"bridge_phone\" SIZE=1>");
        for ($i = 0; $i < sizeof($bridge_numbers); $i++)
        {
            if ( strcmp($bridge_phone, $bridge_numbers[$i]) == 0 )
            {
                echo("<OPTION SELECTED>" . $bridge_numbers[$i] . "</OPTION>");
            }
            else
            {
                echo("<OPTION>" . $bridge_numbers[$i] . "</OPTION>");
            }
        }
        echo("</SELECT></td></tr>");
        echo("<tr><td class=value align=right>System URL: </td>");
        echo("<td class=value><INPUT NAME=\"system_url\" VALUE=\"" . $system_url . "\"></td></tr>");
        echo("<tr><td colspan='2' align='right'><input type=\"submit\" name=\"action\" value=\"Update\"></td></tr>");
        echo("</table>");
        echo("</FORM>");
    }
    else
    {
        dumpFault($r);
    }
}
else if ($action=="Update" && $commid != "") // i.e. Hit the "Update" submit button generated above
{
    // get vals from form
    $community_name = $HTTP_POST_VARS["community_name"];
    $account_number = $HTTP_POST_VARS["account_number"];
    $contact_name = $HTTP_POST_VARS["contact_name"];
    $contact_email = $HTTP_POST_VARS["contact_email"];
    $contact_phone = $HTTP_POST_VARS["contact_phone"];
    $user_name = $HTTP_POST_VARS["user_name"];
    $user_password = $HTTP_POST_VARS["user_password"];
    $bridge_phone = $HTTP_POST_VARS["bridge_phone"];
    $system_url = $HTTP_POST_VARS["system_url"];
    $options = $HTTP_POST_VARS["options"];

    $f=new xmlrpcmsg(WEBSVR_FN_UPDATE_COMMUNITY,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($commid, "int"),
                           new xmlrpcval($community_name, "string"),
                           new xmlrpcval($account_number, "string"),
                           new xmlrpcval($contact_name, "string"),
                           new xmlrpcval($contact_email, "string"),
                           new xmlrpcval($contact_phone, "string"),
                           new xmlrpcval($user_name, "string"),
                           new xmlrpcval($user_password, "string"),
                           new xmlrpcval($bridge_phone, "string"),
                           new xmlrpcval($system_url, "string"),
                           // Hardcode options for now
                           new xmlrpcval("0", "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode()) 
    {
        $numrowsidobj = $v->arraymem(0);
        $numrows = $numrowsidobj->scalarval();
        if ($numrows == 1)
            print "Community updated";
        else
            print "Community " . $commid . " not found";
    }
    else  
    {
        dumpFault($r);
    }
}
else
{
    echo("<b>Unexpected condition in updatecommunity</b>");
}
?>
<?php
?>
</div>
<?php showFooter(); ?>
</body>
</html>
