<?php
//
// Copyright 2004 Imidio, Inc.
//

//
// Summary of bunch of health status pages:
// - controller.get_health
// - controller.get_servers (voice)
// - controller.get_servers (share)
// - controller.get_servers (docshare)
// - #ongoing meetings
// - #users logged in (???)
//
include("common.inc");
include("functions.inc");
include("xmlrpc.inc");
clearCache();
?>
<html>
<head>
<?php checkAuth() ?>
<meta http-equiv='refresh' content='60;url=/imidio/console/php/getstatussummary.php'>
<title>Get Health</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<body>
<table>
<tr>
<td valign='top'>
<?php showNavBar1(); ?>
</td>
<td valign='top'>
<div id="content">
<?php

$sessionid = getSessionId();

if ($sessionid != "")
{
    $vce_status = get_voice_bridge_status($sessionid);
    $shr_status = get_share_status($sessionid);
    $doc_status = get_docshare_status($sessionid);

    $f=new xmlrpcmsg(WEBSVR_FN_GET_HEALTH,
                     array(new xmlrpcval($sessionid, "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();
    if (!$r->faultCode())
    {
        // structure is:
        // array of values
        //   array of metric details
        //     where each metric is an array
        $health_array = $v->arraymem(0);          // primary array
        echo("<center>");

        $serverdate = date("l, d F Y h:i a",time());
        echo("<b>Component Status</b><br>");
        echo("<table border=1 cellpadding=3>");
        echo("<tr><th class=title>Operations</th><th class=title>Service</th><th class=title>ID</th><th class=title>Via Router</th><th class=title>Last&nbsp;Host</th><th class=title>Status</th></tr>");
//        print "<HR>Result details (XML)<BR><PRE>" .
//            htmlentities($r->serialize()). "</PRE><HR>\n";
        $peer_status = array();
        for ($i = 0; $i < $health_array->arraysize(); $i++)
        {
            $health = $health_array->arraymem($i);  // retrieve mtgs (an array)

            $nameobj = $health->arraymem(0);
            $idobj = $health->arraymem(1);
            $locationobj = $health->arraymem(2);
            $lasthostobj = $health->arraymem(3);
            $statusobj = $health->arraymem(4);

            $name = $nameobj->scalarval();
            $id = $idobj->scalarval();
            $location = $locationobj->scalarval();
            if ($location == "") $location = "&nbsp;";
            $lasthost = $lasthostobj->scalarval();
            if ($lasthost == "") $lasthost = "&nbsp;";
            $status = $statusobj->scalarval();

            if ($name == "peer")
            {
                $peer_status[] = array($location, $id, $status);
                continue;
            }
            if ($name == "conf")
            {
                // conference services are built in to jabber
                // no operations and have no 'life' outside of
                // jabberd
                continue;
            }

            $isvoice = array_key_exists($name,$vce_status);

            echo("<tr>");
            echo("<td class=value>");
            if ($status == 0)
            {
                echo("Unavailable");
            }
            else
            {
                // Available; output applicable operations
                echo("<a href=\"setloglevel.php?serverid=" . $name . "\">set logging</a>");
                if ( $isvoice )
                {
                    echo("<br><a href=\"getports.php?serverid=" . $name . "\">view ports</a>");
                }
            }
            echo("</td>");
            echo("<td class=value>" . $name . "</td>");
            echo("<td class=value>" . $id . "</td>");
            echo("<td class=value>" . $location . "</td>");
            echo("<td class=value>" . $lasthost . "</td>");
            if ($status == 0 || ($isvoice && $vce_status[$name] == 0))
                echo("<td class=value bgcolor='#FF0000'>Offline</td>");
            else
                echo("<td class=value bgcolor='#00FF00'>Online</td>");
            echo("</tr>");
        }
        for ($i = 0; $i < count($shr_status); $i++)
        {
            echo("<tr>");
            $svr = $shr_status[$i];
            $id = $svr[0];
            $stat = $svr[1];
            echo("<td>None</td>"); // no operations
            echo("<td>share</td>");
            echo("<td>" . $id . "</td>");
            echo("<td>N/A</td>"); // no xml router
            echo("<td>N/A</td>"); // no last host
            if ($stat == 0)
                echo("<td class=value bgcolor='#FF0000'>Offline</td>");
            else
                echo("<td class=value bgcolor='#00FF00'>Online</td>");
            echo("</tr>");
        }
        for ($i = 0; $i < count($doc_status); $i++)
        {
            echo("<tr>");
            $svr = $doc_status[$i];
            $id = $svr[0];
            $stat = $svr[1];
            echo("<td>None</td>"); // no operations
            echo("<td>doc share</td>");
            echo("<td>" . $id . "</td>");
            echo("<td>N/A</td>"); // no xml router
            echo("<td>N/A</td>"); // no last host
            if ($stat == 0)
                echo("<td class=value bgcolor='#FF0000'>Offline</td>");
            else
                echo("<td class=value bgcolor='#00FF00'>Online</td>");
            echo("</tr>");
        }
        echo("</table>");
        echo("<br><b>XML Router Status</b>");
        echo("<br><table border=1 cellpadding=3>");
        echo("<tr><th class=title>Operations</th><th class=title>Location</th><th class=title>User Count</th><th class=title>Status</th>");
        for ($i = 0; $i < count($peer_status); $i++)
        {
            $peer = $peer_status[$i];
            $location = $peer[0];
            $usercount = $peer[1];
            $status = $peer[2];

            echo("<tr>");
            if ($status == 0)
            {
                echo("<td class=value>----</a></td>");
                echo("<td class=value>----</a></td>");
                echo("<td class=value bgcolor='#FF0000'>Offline</td>");
            }
            else
            {
                if ($i == 0)
                    echo("<td class=value rowspan=" . count($peer_status) . "><a href=\"setjabloglevel.php\">set logging</a></td>");
                echo("<td class=value>" . $location . "</td>");
                echo("<td class=value>" . $usercount . "</td>");
                echo("<td class=value bgcolor='#00FF00'>Online</td>");
            }
            echo("</tr>");
        }
        echo("</table>");
        echo("<br><em>Refreshed: " . $serverdate . "</em>");
        echo("</center>");
    }
    else
    {
        print "Fault: ";
        print "Code: " . $r->faultCode() .
            " Reason '" .$r->faultString()."'<BR>";
    }
}
?>
</td></tr></table>
<div id="content">
<?php showFooter(); ?>
</body>
</html>
