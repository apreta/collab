<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Remove a user
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Remove User</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<body>

<?php showNavBar2(); ?>

<?php
// if user id was passed in, fill it into the form
$userid = ($_GET['userid']);
if ($userid != "")
{
    $user_id = $userid;
}
?>
Enter a user id to remove the user
<FORM METHOD="POST">
<table>
<tr><td>User Id:</td>
<td><INPUT NAME="user_id" VALUE="<?php print ${user_id};?>"></td></tr>
<tr><td colspan='2' align='right'><input type="submit" value="remove user" name="submit"></td></tr>
</table>
</FORM>

<?php
$sessionid = getSessionId();

if ($HTTP_POST_VARS["user_id"]!="")
{
    // get vals from form
    $user_id = $HTTP_POST_VARS["user_id"];

    $sessionid = getSessionId();

    $f=new xmlrpcmsg(WEBSVR_FN_REMOVE_USER,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($user_id, "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode()) 
    {
        $numrowsidobj = $v->arraymem(0);
        $numrows = $numrowsidobj->scalarval();
        if ($numrows >= 1)
            print "User removed";
        else
            print "User " . $user_id . " not found";
    }
    else  
    {
        dumpFault($r);
    }
}
?>
<?php showFooter(); ?>
</body>
</html>
