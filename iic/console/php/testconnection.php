<?php

//
// Copyright 2004 Imidio, Inc.
//

//
// For test purposes to examine the response
// to basic login/authentication
//

include("xmlrpc.inc");
include("common.inc");

// verify user/password and redirect to main menu if passes
$redirect = 0;
$msg = "";

$logout = ($_GET['logout']);
if ($logout == 1)
{
    clearCookies();
}
if ($HTTP_POST_VARS["uname"]!="")
{
    $username = $HTTP_POST_VARS["uname"];
    $password = $HTTP_POST_VARS["pwd"];
    $f=new xmlrpcmsg(WEBSVR_FN_AUTH_USER,
                     array(new xmlrpcval($username, "string"),
                           new xmlrpcval($password, "string"),
                           new xmlrpcval("", "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode()) {
        print "<HR>Participant details (XML)<BR><PRE>" .
            htmlentities($r->serialize()). "</PRE><HR>\n";
    } else {
        print "Fault: ";
        print "Code: " . $r->faultCode() . 
	  " Reason '" .$r->faultString()."'<BR>";
    }
}
?>

<html>
<head>
<?php
if ($redirect == 1) 
{
    echo("<meta http-equiv='refresh' content='2;url=menu.php'>");
}
?>
<title>Login</title></head>
<body>

Please log on:
<FORM  METHOD="POST">
<p>User: <INPUT NAME="uname"></p>
<p>Pass: <INPUT NAME="pwd" TYPE="PASSWORD"></p>
<p><td align="right"><input type="submit" value="login" name="submit"></p>
</FORM>

<?php
print $msg;
?>

<P>

</body>
</html>
