<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Get port usage.
//

include("common.inc");
include("functions.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Get Ports</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<body>

<?php showNavBar1(); ?>

<?php
// serverid if set
$serverid = ($_GET['serverid']);
if ($serverid == "")
{
    $serverid = $HTTP_POST_VARS["serverid"];
}

$sessionid = getSessionId();

if ($sessionid != "" && $serverid != "")
{
    $f=new xmlrpcmsg(WEBSVR_FN_GET_PORTS,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($serverid, "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $port_array=$r->value();
    if (!$r->faultCode()) 
    {
        // structure is:
        //   array of port details
        //     where details of each port is an array
        echo("<table border=\"1\">");
        echo("<tr><th class=title>Operations</th><th class=title>ID</th><th class=title>State</th><th class=title>Meeting ID</th><th class=title>ParticipantID</th><th class=title>Screenname</th><th class=title>Name</th><th class=title>Last Event Time</th><th class=title>Last Event Name</th></tr>");
        for ($i = 0; $i < $port_array->arraysize(); $i++)
        {
            echo("<tr>");
            $port = $port_array->arraymem($i);  // retrieve port details (an array)
            $portidobj = $port->arraymem(0);
            echo("<td class=value><a href=\"resetport.php?portid=" . $portidobj->scalarval() . "&serverid=" . $serverid . "&sessionid=" . $sessionid . "\">reset</a></td>");
            echo("<td class=value>" . $portidobj->scalarval() . "&nbsp;</td>");

            $stateobj = $port->arraymem(1);
            if ($stateobj->scalarval() == "0")
                echo("<td class=value>no call&nbsp;</td>");
            else
                echo("<td class=value>connected&nbsp;</td>");
                
            $mtgidobj = $port->arraymem(2);
            echo("<td class=value>" . $mtgidobj->scalarval() . "&nbsp;</td>");
            $partidobj = $port->arraymem(3);
            echo("<td class=value>" . $partidobj->scalarval() . "&nbsp;</td>");
            $screenobj = $port->arraymem(4);
            echo("<td class=value>" . $screenobj->scalarval() . "&nbsp;</td>");
            $nameobj = $port->arraymem(5);
            echo("<td class=value>" . $nameobj->scalarval() . "&nbsp;</td>");
            $lasttimeobj = $port->arraymem(6);
            echo("<td class=value>" . $lasttimeobj->scalarval() . "&nbsp;</td>");
            $lasteventobj = $port->arraymem(7);
            echo("<td class=value>" . $lasteventobj->scalarval() . "&nbsp;</td>");
        }
        echo("</table>");
    } 
    else 
    {
	print "Fault: ";
	print "Code: " . $r->faultCode() . " Reason '" .$r->faultString()."'<BR>";
    }
}
?>

<!--
//<?php
//print "Enter a VOICE server id to look up port details";
//print "<FORM  METHOD=\"POST\">
//<INPUT NAME=\"serverid\" VALUE=\"${serverid}\"><input type=\"submit\" value=\"go\" name=\"submit\"></FORM><P>";
//?>
-->
<?php showFooter(); ?>
</body>
</html>
