<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Fetch community details
//
include("common.inc");
include("xmlrpc.inc");
clearCache();
?>
<html>
<head>
<?php checkAuth() ?>
<title>Fetch Community</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<body>
<?php showNavBar2(); ?>
<div id="content">
<h3>Community Details</h3>
<?php
// if community id was passed in, fill it into the form
$commid= ($_GET['commid']);
if ($commid == "")
{
    $commid = $HTTP_POST_VARS["commid"];
}
$action = ($HTTP_POST_VARS["action"]);
?>
<?php
$sessionid = getSessionId();

if ( $commid=="" || $action=="Next" || $action=="Prev" )
{
    // retrieve start row if set
    if ($HTTP_POST_VARS["starting_row_id"] != "")
    {
        $starting_row_id = $HTTP_POST_VARS["starting_row_id"];
    }
    else
    {
        $starting_row_id = 0;
    }

    if ($action == "Next")
    {
        $chunk = $HTTP_POST_VARS["chunk"];
        $starting_row_id += $chunk;
    }
    if ($HTTP_POST_VARS["action"] == "Prev")
    {
        $chunk = $HTTP_POST_VARS["chunk"];
        $starting_row_id -= $chunk;
    }
    
    if ($starting_row_id >= 0)
    {
        $f=new xmlrpcmsg(WEBSVR_FN_FETCH_COMMUNITY_LIST,
                         array(new xmlrpcval($sessionid, "string"),
                               new xmlrpcval($starting_row_id, "int"),
                               new xmlrpcval(DEFAULT_CHUNK_SIZE, "int")));
        $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
        $r=$c->send($f);
        $v=$r->value();
        if (!$r->faultCode()) {
            echo("<FORM METHOD=\"POST\">");
            echo("<INPUT TYPE=hidden NAME=\"starting_row_id\" VALUE=\"" . $starting_row_id . "\">");
            echo("<INPUT TYPE=hidden name=chunk value=" . DEFAULT_CHUNK_SIZE . ">");
            echo("<SELECT name=\"commid\" size=\"" . DEFAULT_CHUNK_SIZE . "\">");
            $comm_array = $v->arraymem(0);          // primary array
            for ($i = 0; $i < $comm_array->arraysize(); $i++)
            {
                // retrieve communities (an array)
                $commobj = $comm_array->arraymem($i);  
                $commidobj = $commobj->arraymem(0);
                $commnameobj = $commobj->arraymem(1);
                
                echo("<OPTION NAME=commid VALUE=\"");
                echo($commidobj->scalarval());
                echo("\">");
                echo($commnameobj->scalarval());
                echo("</OPTION>");
            }
            echo("</SELECT>");
            echo("<div>");
            $first_obj = $v->arraymem(1);
            $first     = $first_obj->scalarval();
            
            $chunk_obj = $v->arraymem(2);
            $chunk     = $chunk_obj->scalarval();
            
            $total_obj = $v->arraymem(3);
            
            $more_obj  = $v->arraymem(4);
            $more      = $more_obj->scalarval();
            
            if ($first > 0)
            {
                $newfirst = $first-$chunk;
                if ($newfirst >= 0)
                    echo("<input type=submit name=action value=\"Prev\">");
            }
            if ($more == 1)
            {
                echo("<input type=submit name=action value=\"Next\">");
            }
            echo("</div>");
            echo("<br>");
            echo("<div>");
            echo("<input type=submit name=action value=\"Get\">");
            echo("</div>");
            echo("</FORM>");
        }
        else
        {
            dumpFault($r);
        }
    }
}
else if ($commid!="")
{
    // get vals from form
    // $commid = $HTTP_POST_VARS["community_id"];
    // if ($commid == "")
    // $commid = $commid;

    $sessionid = getSessionId();

    $f=new xmlrpcmsg(WEBSVR_FN_FETCH_COMMUNITY,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($commid, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $sessionid = getSessionId();

    $f=new xmlrpcmsg(WEBSVR_FN_FETCH_COMMUNITY,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($commid, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();

    if (!$r->faultCode()) 
    {
        $comm_array = $v->arraymem(0);
        $commobj = $comm_array->arraymem(0);
        if ($commobj != "")
        {
            $commidobj = $commobj->arraymem(0);
            echo("<table border=\"1\">");
            echo("<tr><td class=value>community id:</td><td class=value>&nbsp;" . $commidobj->scalarval() . "</td></tr>");
            
            $commnameobj = $commobj->arraymem(1);
            echo("<tr><td class=value>community name: </td><td class=value>&nbsp;" . $commnameobj->scalarval() . "</td></tr>");
            
            $acctnumobj = $commobj->arraymem(2);
            echo("<tr><td class=value>account number: </td><td class=value>&nbsp;" . $acctnumobj->scalarval() . "</td></tr>");
            
            $contactnameobj = $commobj->arraymem(3);
            echo("<tr><td class=value>contact name: </td><td class=value>&nbsp;" . $contactnameobj->scalarval() . "</td></tr>");
            
            $contactemailobj = $commobj->arraymem(4);
            echo("<tr><td class=value>contact email: </td><td class=value>&nbsp;" . $contactemailobj->scalarval() . "</td></tr>");
            
            $contactphoneobj = $commobj->arraymem(5);
            echo("<tr><td class=value>contact phone: </td><td class=value>&nbsp;" . $contactphoneobj->scalarval() . "</td></tr>");
            
            $usernameobj = $commobj->arraymem(6);
            echo("<tr><td class=value>user name: </td><td class=value>&nbsp;" . $usernameobj->scalarval() . "</td></tr>");
            
            $userpasswordobj = $commobj->arraymem(7);
            echo("<tr><td class=value>user password: </td><td class=value>&nbsp;" . $userpasswordobj->scalarval() . "</td></tr>");
            
            $bridgephoneobj = $commobj->arraymem(8);
            echo("<tr><td class=value>bridge phone: </td><td class=value>&nbsp;" . $bridgephoneobj->scalarval() . "</td></tr>");
            
            $systemurlobj = $commobj->arraymem(9);
            echo("<tr><td class=value>system url: </td><td class=value>&nbsp;" . $systemurlobj->scalarval() . "</td></tr>");
            
            $accounttypeobj = $commobj->arraymem(10);
            echo("<tr><td class=value>account type: </td><td class=value>&nbsp;" . $accounttypeobj->scalarval() . "</td></tr>");
            
            $accountstatusobj = $commobj->arraymem(11);
            echo("<tr><td class=value>account status: </td><td class=value>&nbsp;" . $accountstatusobj->scalarval() . "</td></tr>");
            
            $optionsobj = $commobj->arraymem(12);
            echo("<tr><td class=value>options: </td><td class=value>&nbsp;" . $optionsobj->scalarval() . "</td></tr>");
            echo("<tr><td class=value colspan=2><a href=\"updatecommunity.php?commid=" . $commidobj->scalarval() . "\">update this community</a></td></tr>");
            echo("</table>");
        }
    }
    else  
    {
        dumpFault($r);
    }
    $commid = "";
}
?>
</div>
<?php showFooter(); ?>
</body>
</html>
