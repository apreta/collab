<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Remove a community
//

include("common.inc");
include("functions.inc");
include("xmlrpc.inc");
clearCache();
?>
<html>
<head>
<?php checkAuth() ?>
<title>Remove Community</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<body>
<?php showNavBar2(); ?>
<div id="content">
<h3>Remove Community</h3>
<?php
$sessionid = getSessionId();

// Fetch POST/GET params
$action       = ($HTTP_POST_VARS['action']);
if ($action == "")
{
    $action = ($_GET['action']);
}
$commid = ($HTTP_POST_VARS['commid']);
if ($commid == "")
{
    $commid = ($_GET['commid']);
}

// Four possible actions at this point:
//   1. If no community, select one and re-POST params
//   2. If community and delete action, confirm delete and re-POST params
//   3. If delete confirmed, do delete
//   4. If delete not confirmed, reassure user that it was not deleted
if ($commid == "")
{
    selectCommunityId($sessionid, "Delete");
}
else if ($action == "Delete" && $commid != "")
{
    echo("<FORM METHOD=post><b>Really remove this community?</b>");
    echo("<INPUT TYPE=hidden NAME=\"commid\" VALUE=\"" . $commid . "\">");
    echo("<INPUT TYPE=submit NAME=\"action\" VALUE=\"Yes\">");
    echo("<INPUT TYPE=submit NAME=\"action\" VALUE=\"No\">");
    echo("</FORM>");
}
else if ($action == "Yes" && $commid != "")
{
    $f=new xmlrpcmsg(WEBSVR_FN_REMOVE_COMMUNITY,
                     array(new xmlrpcval($sessionid, "string"),
                           new xmlrpcval($commid, "int")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();
    if (!$r->faultCode())
    {
        echo("<b>Community removed</b>");
    }
    else
    {
        dumpFault($r);
    }
}
else if ($action == "No")
{
    echo("<b>Not removed</b>");
}
else
{
    echo("<b>Unexpected condition in removecommunity</b>");
}
?>
</div>
<?php showFooter(); ?>
</body>
</html>
