<?php 
//
// Copyright 2004 Imidio, Inc.
//

//
// Find participant info based on PIN.
//

include("common.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Find Invite</title>
</head>
<body>

<?php showNavBar(); ?>
<?php

print "Enter a partipant PIN to look up participant meeting details";
print "<FORM  METHOD=\"POST\">
<INPUT NAME=\"mid\" VALUE=\"${mid}\"><input type=\"submit\" value=\"go\" name=\"submit\"></FORM><P>";

if ($HTTP_POST_VARS["mid"]!="") {
    $f=new xmlrpcmsg('controller.find_invite',
                     array(new xmlrpcval("sys:jabapi", "string"),
                           new xmlrpcval("", "string"),
                           new xmlrpcval($HTTP_POST_VARS["mid"], "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();
    if (!$r->faultCode()) {
	print "<HR>Participant details (XML)<BR><PRE>" .
            htmlentities($r->serialize()). "</PRE><HR>\n";
    } else {
	print "Fault: ";
	print "Code: " . $r->faultCode() . 
            " Reason '" .$r->faultString()."'<BR>";
    }
}
?>
<?php showFooter(); ?>
</body>
</html>
