<?php
//
// Copyright 2004 Imidio, Inc.
//

//
// Get port usage.
//

include("common.inc");
include("functions.inc");
include("xmlrpc.inc");
clearCache();
?>

<html>
<head>
<?php checkAuth() ?>
<title>Get Meetings</title>
<LINK REL=StyleSheet HREF="style.css" TYPE="text/css" MEDIA=screen>
</head>
<body>
<table>
<tr>
<td valign='top'>
<?php showNavBar1(); ?>
</td>
<td valign='top'>
<?php

$sessionid = getSessionId();

if ($sessionid!="")
{
    $f=new xmlrpcmsg(WEBSVR_FN_GET_MEETINGS,
                     array(new xmlrpcval($sessionid, "string")));
    $c=new xmlrpc_client(WEB_SERVICE_URI, WEB_SERVICE_DOMAIN, WEB_SERVICE_PORT);
    $r=$c->send($f);
    $v=$r->value();
    if (!$r->faultCode())
    {
        // structure is:
        // array of values
        //   array of mtg details
        //     where each mtg is an array
        //   start row
        //   number
        //   more
        //   total
        $mtg_array = $v->arraymem(0);          // primary array
        echo("<table border=\"1\">");
        echo("<tr><th class=title>Mtg ID</th><th class=title>Host ID</th><th class=title>Type</th><th class=title>Privacy</th><th class=title>Title</th><th class=title>Description</th><th class=title>Duration (mins)</th><th class=title>Time</th><th class=title>Options</th><th class=title>Num Invited</th><th class=title>Num Connected</th><th class=title>Appshare Server</th><th class=title>Num Appshare Sessions</th><th class=title>Voice Server</th><th class=title>Num Calls</th><th class=title>Doc Server</th></tr>");
        for ($i = 0; $i < $mtg_array->arraysize(); $i++)
        {
            echo("<tr>");
            $mtg = $mtg_array->arraymem($i);  // retrieve mtgs (an array)
            $mtgidobj = $mtg->arraymem(0);
            echo("<td class=value>&nbsp;" . $mtgidobj->scalarval() . "</td>");

            // host id is of form user:id -- just want id
            $hostidobj = $mtg->arraymem(1);
            $hostid = $hostidobj->scalarval();
            if (strpos($hostid, ":") > 0)
            {
                $idlen = strlen($hostid);
                $idstart = strpos($hostid, ":") + 1;
                $hostid = substr($hostid, $idstart, $idlen - $idstart);
            }
            echo("<td class=value>&nbsp;" . $hostid . "</td>");

            $typeobj = $mtg->arraymem(2);
            echo("<td class=value>&nbsp;" . $typeobj->scalarval() . "</td>");
            $privobj = $mtg->arraymem(3);
            echo("<td class=value>&nbsp;" . $privobj->scalarval() . "</td>");
            $mtgnameobj = $mtg->arraymem(4);
            echo("<td class=value>&nbsp;" . $mtgnameobj->scalarval() . "</td>");
            $mtgdescobj = $mtg->arraymem(5);
            echo("<td class=value>&nbsp;" . $mtgdescobj->scalarval() . "</td>");
            $mtgdurationobj = $mtg->arraymem(6);
            echo("<td class=value>&nbsp;" . $mtgdurationobj->scalarval() . "</td>");
            $mtgtimeobj = $mtg->arraymem(7);
            echo("<td class=value>&nbsp;" . $mtgtimeobj->scalarval() . "</td>");
            $mtgoptionsobj = $mtg->arraymem(8);
            echo("<td class=value>&nbsp;" . mtgOptions2String($mtgoptionsobj->scalarval()) . "</td>");
            $numinvitedobj = $mtg->arraymem(9);
            echo("<td class=value>&nbsp;" . $numinvitedobj->scalarval() . "</td>");
            $numconnectedobj = $mtg->arraymem(10);
            echo("<td class=value>&nbsp;" . $numconnectedobj->scalarval() . "</td>");
            $appserverobj = $mtg->arraymem(11);
            echo("<td class=value>&nbsp;" . $appserverobj->scalarval() . "</td>");
            $numappshareobj = $mtg->arraymem(12);
            echo("<td class=value>&nbsp;" . $numappshareobj->scalarval() . "</td>");
            $voiceserverobj = $mtg->arraymem(13);
            $voice = $voiceserverobj->scalarval();
            echo("<td class=value>&nbsp;<a href=\"getports.php?serverid=" . $voice . "\">" . $voice . "</a></td>");
            $numcallsobj = $mtg->arraymem(14);
            echo("<td class=value>&nbsp;" . $numcallsobj->scalarval() . "</td>");
            $docserver = $mtg->arraymem(15);
            echo("<td class=value>&nbsp;" . $docserver->scalarval() . "</td>");
        }
        echo("</table>");
    }
    else
    {
	print "Fault: ";
	print "Code: " . $r->faultCode() .
            " Reason '" .$r->faultString()."'<BR>";
    }
}
?>
</td></tr></table>
<?php showFooter(); ?>
</body>
</html>
