/* VNC Reflector
 * Copyright (C) 2001-2003 HorizonLive.com, Inc.  All rights reserved.
 *
 * This software is released under the terms specified in the file LICENSE,
 * included.  HorizonLive provides e-Learning and collaborative synchronous
 * presentation solutions in a totally Web-based environment.  For more
 * information about HorizonLive, please see our website at
 * http://www.horizonlive.com.
 *
 * This software was authored by Constantin Kaplinsky <const@ce.cctpu.edu.ru>
 * and sponsored by HorizonLive.com, Inc.
 *
 * $Id: client_io.h,v 1.9 2005-03-16 00:11:08 mike Exp $
 * Asynchronous interaction with VNC clients.
 */

#ifndef _REFLIB_CLIENT_IO_H
#define _REFLIB_CLIENT_IO_H

#include "host_io.h"

#define TYPE_CL_SLOT    1
#define TYPE_CL_LISTENING 5

#define HTTP_1_0 "HTTP/1.0"
#define HTTP_1_1 "HTTP/1.1"

#define HTTP_HEADERS "Content-Length: %d\r\nCache-Control: no-cache\r\n" \
                     "Pragma: no-cache\r\nContent-Type: application/octet-stream\r\n"

#define HTTP_CLOSE   "Content-Length: %d\r\nConnection: close\r\n" \
                     "Cache-Control: no-cache\r\n" \
                     "Pragma: no-cache\r\nContent-Type: application/octet-stream\r\n"

#define CLIENT_TIMEOUT 60000     /* 1 min */

/* Extension to AIO_SLOT structure to hold client state */
typedef struct _CL_SLOT {
  AIO_SLOT s;

  HOST_SLOT *host;      /* pointer to associated host if this is the active client slot */

  int connected;

  int command_len;
  char command[1024];
  int header_pos;		/* used when parsing HTTP header */

  int request_count;
  int http_1_0;         /* Is HTTP 1.0 ? */
  int keepalive;		/* try to keep connection alive, disabled if client using HTTP/1.0 */

  long in_sequence;
  long out_sequence;

} CL_SLOT;

int client_listen(int port, int max_requests);
void af_client_init(void);
void af_client_accept(void);

#endif /* _REFLIB_CLIENT_IO_H */
