/* VNC Reflector
 * Copyright (C) 2001-2003 HorizonLive.com, Inc.  All rights reserved.
 *
 * This software is released under the terms specified in the file LICENSE,
 * included.  HorizonLive provides e-Learning and collaborative synchronous
 * presentation solutions in a totally Web-based environment.  For more
 * information about HorizonLive, please see our website at
 * http://www.horizonlive.com.
 *
 * This software was authored by Constantin Kaplinsky <const@ce.cctpu.edu.ru>
 * and sponsored by HorizonLive.com, Inc.
 *
 * $Id: logging.c,v 1.2 2004-07-02 20:03:32 mike Exp $
 * Logging implementation
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>
#include <errno.h>
#ifdef LINUX
#include <sys/param.h>
#endif

#include "logging.h"

static FILE *log_fp = NULL;
static char *log_fname = NULL;
static int log_file_level = -1;
static int log_stderr_level = -1;
static int history_len = 5;
static size_t trigger_sz = 5000000;

static char log_lchar[] = "@!*+-: ";
static void log_rotate_if_needed(void);

/*******************************************************************
 *
 *  Open log file.
 *    filename: name for a file to append logs to.
 *    level: maximum log level to write messages at,
 *      -1 turns logging off.
 *  Returns 1 on success, 0 on failure.
 *
 *******************************************************************/

int log_open(char *filename, int file_level, int stderr_level)
{
  if (log_fname != NULL || log_fp != NULL) {
    log_write(LL_INTERR, "Trying to open already opened log");
    return 0;
  }

  log_stderr_level = stderr_level;

  log_fp = fopen(filename, "a");
  if (log_fp == NULL)
    return 0;

  log_fname = malloc(strlen(filename) + 1);
  if (log_fname == NULL) {
    fclose(log_fp);
    log_fp = NULL;
    return 0;
  }

  strcpy(log_fname, filename);
  log_file_level = file_level;

  fprintf(log_fp, "\n");
  fflush(log_fp);

  return 1;
}

/*******************************************************************
 *
 *  Reopen log file.
 *  Returns 1 on success, 0 on failure.
 *
 *******************************************************************/

int log_reopen(void)
{
  if (log_fname == NULL || log_fp == NULL)
    return 0;

  if (fclose(log_fp) == EOF) {
    free(log_fname);
    log_fname = NULL;
    log_fp = NULL;
    return 0;
  }

  log_fp = fopen(log_fname, "a");
  if (log_fp == NULL) {
    free(log_fname);
    log_fname = NULL;
    return 0;
  }

  return 1;
}

/*******************************************************************
 *
 *  Close log file.
 *  Returns 1 on success, 0 on failure.
 *
 *******************************************************************/

int log_close(void)
{
  int success = 1;

  if (log_fname != NULL) {
    free(log_fname);
    log_fname = NULL;
  }

  if (log_fp != NULL) {
    success = (fclose(log_fp) != EOF);
    log_fp = NULL;
  }

  log_file_level = -1;
  log_stderr_level = -1;
  return success;
}

/*******************************************************************
 *
 *  Write to the log file.
 *    level: log level, see LL_* constants;
 *    msg: the string to write to the log.
 *
 *******************************************************************/

void log_write(int level, char *format, ...)
{
  va_list arg_list;
  time_t now;
  char time_buf[32];
  char level_char = ' ';

  log_rotate_if_needed();
  
  va_start(arg_list, format);

  if ( (log_fp != NULL && level <= log_file_level) ||
       level <= log_stderr_level ) {
    now = time(NULL);
    strftime(time_buf, 31, "%d/%m/%y %H:%M:%S", localtime(&now));

    if (level >= 0 && level < sizeof(log_lchar) - 1)
      level_char = log_lchar[level];

    if (level <= log_file_level) {
      fprintf(log_fp, "%s %c ", time_buf, (int)level_char);
      vfprintf(log_fp, format, arg_list);
      fprintf(log_fp, "\n");
      fflush(log_fp);
    }
    if (level <= log_stderr_level) {
      fprintf(stderr, "%s %c ", time_buf, (int)level_char);
      vfprintf(stderr, format, arg_list);
      fprintf(stderr, "\n");
      fflush(stderr);
    }
  }

  va_end(arg_list);
}

static void log_rotate_if_needed()
{
#ifdef LINUX
    char newerfn[MAXPATHLEN];
    char olderfn[MAXPATHLEN];
#else
    char newerfn[_MAX_PATH];
    char olderfn[_MAX_PATH];
#endif
    int i;
    long sz;

    if (log_fname == NULL || log_fp == NULL)
        return;

    fseek(log_fp, 0, SEEK_END);
    sz = ftell(log_fp);

    if (sz < trigger_sz) return;

    fclose(log_fp);
    log_fp = NULL;
    
    sprintf(newerfn, "%s.%u", log_fname, history_len);
    unlink(newerfn);
    for (i = history_len-1; i > 0; i--) {
        strcpy(olderfn, newerfn);
        sprintf(newerfn, "%s.%u", log_fname, i);
        if (rename(newerfn, olderfn) < 0) {
            if (errno != ENOENT) {
                log_fp = fopen(log_fname, "a");
                return;
            }
        }
    }

    strcpy(olderfn, newerfn);
    strcpy(newerfn, log_fname);
    if (rename(newerfn, olderfn) < 0) {
        if (errno != ENOENT) {
            log_fp = fopen(log_fname, "a");
            return;
        }
    }
    
    log_fp = fopen(log_fname, "a");
}

void log_set_rotation_size(size_t _trigger_sz)
{
    trigger_sz = _trigger_sz;
}

void log_set_history_len(int _history_len)
{
  history_len = _history_len;
}
