/* VNC Reflector
 * Copyright (C) 2001-2003 HorizonLive.com, Inc.  All rights reserved.
 *
 * This software is released under the terms specified in the file LICENSE,
 * included.  HorizonLive provides e-Learning and collaborative synchronous
 * presentation solutions in a totally Web-based environment.  For more
 * information about HorizonLive, please see our website at
 * http://www.horizonlive.com.
 *
 * This software was authored by Constantin Kaplinsky <const@ce.cctpu.edu.ru>
 * and sponsored by HorizonLive.com, Inc.
 *
 * $Id: client_io.c,v 1.23 2007-08-24 17:04:00 mike Exp $
 * 
 * Asynchronous interaction with VNC clients.
 */

/*
 Protocol:

 Client requests URL of format: /data/<host#>/<<check>/<in-sequence>/<out-sequence>

 in-sequence = position in "up" stream of first byte in this request
 out-sequence = position in "down" stream that client expects to receive
                (= position in "down" stream of last byte in last response + 1)

 If client does not receive a response, it will re-send the same request with
 same sequence numbers again.  The server will ignore the request data if it
 has already sent data to host; and will generate a new response (which may
 include more data if more is pending for client).

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#ifdef LINUX
#include <sys/time.h>
#include <errno.h>
#else
#include <time.h>
#endif

#include "logging.h"
#include "async_io.h"
#include "proxy.h"
#include "host_connect.h"
#include "host_io.h"
#include "client_io.h"
#include "port.h"

/*
 * Prototypes for static functions
 */

static void cf_client(void);
static void rf_client_start(void);
static void rf_client_command(void);
static void rf_client_data(void);
static void wf_response_sent();
static void qf_client_check(void);
static void process_request();
static void connect_request();
static void data_request(long host, long check);
static void close_request(long host, long check);
static void find_host(long host);
static void send_result(int status, char *data, int len);
static void check_version();
static void check_keepalive();
static char *find_header(const char *name);
static void send_response();
static void tf_expire_client();

static int s_max_requests;

/*
 * Implementation
 */

int client_listen(int port, int max_requests)
{
  if (!aio_listen(port, af_client_init, af_client_accept,
                  sizeof(CL_SLOT))) {
    log_write(LL_ERROR, "Error creating listening socket: %s",
	     _strerror(errno));
    //aio_close(1);
    return 0;
  }
  s_max_requests = max_requests;
  log_write(LL_MSG, "Accepting client connections on port %d (max "
            "requests: %d)", port, s_max_requests);
  return 1;
}

void af_client_init(void)
{
  cur_slot->type = TYPE_CL_LISTENING;
}

void af_client_accept(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;

  cur_slot->type = TYPE_CL_SLOT;
  cl->connected = 0;
  cl->command_len = 0;
  cl->header_pos = 0;
  aio_setclose(cf_client);
  aio_set_queue_empty(qf_client_check);

  log_write(LL_MSG, "Accepted connection from %s", cur_slot->name);

  aio_setread(rf_client_start, NULL, 5);
}

static void cf_client(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;

  /* if this is the active client slot for host, reset it's pointer */
  if (cl->host != NULL && cl->host->client == cur_slot)
    cl->host->client = NULL;
  
  if (cur_slot->errread_f) {
    if (cur_slot->io_errno) {
      log_write(LL_WARN, "Error reading from %s: %s",
                cur_slot->name, strerror(cur_slot->io_errno));
    } else {
      log_write(LL_WARN, "Error reading from %s", cur_slot->name);
    }
  } else if (cur_slot->errwrite_f) {
    if (cur_slot->io_errno) {
      log_write(LL_WARN, "Error sending to %s: %s",
                cur_slot->name, strerror(cur_slot->io_errno));
    } else {
      log_write(LL_WARN, "Error sending to %s", cur_slot->name);
    }
  } else if (cur_slot->errio_f) {
    log_write(LL_WARN, "I/O error, client %s", cur_slot->name);
  }
  log_write(LL_MSG, "Closing client connection %s", cur_slot->name);
}

/* Read HTTP request */
static void rf_client_start(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  cl->command_len = 0;
  if (strncasecmp(cl->s.readbuf, "POST", 4) == 0) {
    log_write(LL_MSG, "Received header from %s", cur_slot->name);
    aio_setread(rf_client_command, NULL, 1);
  }
  else {
    log_write(LL_MSG, "Unexpected proxy command");
    aio_close(0);
  }
}

/* Read command from client one character at a time...a little inefficient,
   but just at connection set up */
static void rf_client_command(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  if (cl->command_len < sizeof(cl->command) - 1) {
    cl->command[cl->command_len++] = cur_slot->readbuf[0];
  }
  else {
    log_write(LL_ERROR, "Proxy command is too long, connection aborted");
    aio_close(0);
    return;
  }
  
  if ((cur_slot->readbuf[0] == '\r' || cur_slot->readbuf[0] == '\n')) {
	if (++cl->header_pos == 4) {
		/* header is complete */
		cl->command[cl->command_len++] = '\0';
		process_request();
	}
	else
		aio_setread(rf_client_command, NULL, 1);
  }
  else {
	cl->header_pos = 0;
    aio_setread(rf_client_command, NULL, 1);
  }
}

static void process_request()
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
 
  log_write(LL_MSG, "Received request: %s", cl->command);

  check_version();
  check_keepalive();

  if (strncasecmp(cl->command, "/connect", 8) == 0) {
    connect_request();
  }
  else if (strncasecmp(cl->command, "/data/", 6) == 0)
  {
    long host, check;
    if (sscanf(cl->command + 6, "%ld/%ld/%ld/%ld", &host, &check, &cl->in_sequence, &cl->out_sequence) < 3)
    {
      log_write(LL_ERROR, "Malformed request, ignoring");
	  aio_close(0);
    }

	data_request(host, check);
  }
  else if (strncasecmp(cl->command, "/close/", 7) == 0)
  {
    long host, check;
    sscanf(cl->command + 7, "%ld/%ld", &host, &check);

	close_request(host, check);
  }
  else
  {
	log_write(LL_ERROR, "Unknown proxy command");
	aio_close(0);
  }
}

static void connect_request()
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  char id[32];
    
  if (connect_to_host()) {
    /* Add time to host id to make sure it is unique */
    sprintf(id, "%d/%d", cl->host->s.sequence, cl->host->connect_time);
    send_result(200, id, strlen(id));
  }
  else {
    send_result(400, NULL, 0);
    return;
  }

  log_write(LL_MSG, "Waiting for next request");
  aio_setread(rf_client_start, NULL, 5);
}

static void data_request(long host, long check)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  char *length;

  find_host(host);
  
  if (cl->host == NULL || cl->host->connect_time != check) {
    log_write(LL_MSG, "Host for %d not found", host);
    send_result(404, "Connection closed", 17);
    return;
  }

  log_write(LL_MSG, "Request for client %d received", host);

  length = find_header("content-length:");
  if (length != NULL)
  {
    int bytes;
    sscanf(length, "%d", &bytes);
 
    if (bytes != 0) {
      aio_setread(rf_client_data, NULL, bytes);
    } else {
      cur_slot->bytes_ready = 0;
      rf_client_data();
    }
  } else {
    log_write(LL_ERROR, "No content-length in request: %s", cl->command);
    send_result(411, "Content-length required", 23);
  }
}

static void close_request(long host, long check)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;

  find_host(host);
  
  if (cl->host == NULL || cl->host->connect_time != check) {
	log_write(LL_MSG, "Host for %d not found", host);
    send_result(404, "Connection closed", 17);
  }
  else {
    log_write(LL_MSG, "Close for client %d received", host);
    send_result(205, "Connection closed", 17);
  }
}

/* N.B.: a single client slot may receive requests for multiple hosts */
static void find_host(long host)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;

  if (cl->host == NULL || cl->host->s.sequence != host) {
    if (cl->host != NULL)
      log_write(LL_ERROR, "New host for client %s", cl->s.name);
    HOST_SLOT * hs = (HOST_SLOT *)aio_find_slot(host);
    
    /* link this client with it's host */
    if (hs)
      hs->client = cur_slot;
    cl->host = hs;
  }
}

static void check_version()
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  char *p = strstr(cl->command, "HTTP/1.0");

  if (p)
  {
    cl->http_1_0 = 1;
    cl->keepalive = 0;
  }
  else
  {
    cl->http_1_0 = 0;
    cl->keepalive = 1;
  }
}

static void check_keepalive()
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  char *connection;
  
  /* see if default connection keep alive has been overriden by client */
  connection = find_header("connection:");
  if (connection != NULL)
  {
    char test[32];
    sscanf(connection, "%31s", test);
    if (strcasecmp(test, "close") == 0) {
      cl->keepalive = 0;
    }
  }
}

static char *find_header(const char *name)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  char *p = strchr(cl->command, '\n');

  while (p != NULL) {
    if (strncasecmp(p + 1, name, strlen(name)) == 0) {
      return p + strlen(name) + 1;
    }

    p = strchr(p + 1, '\n');
  }

  return NULL;
}

static void send_result(int status, char *data, int len)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  char result[256];

  if (status != 200) {
	/* close connection after error response */
    cl->keepalive = 0;
  }

  if (cl->keepalive) {
    sprintf(result, "%s %d OK\r\n" HTTP_HEADERS "\r\n", cl->http_1_0 ? HTTP_1_0 : HTTP_1_1, status, len);
  }
  else {
    sprintf(result, "%s %d OK\r\n" HTTP_CLOSE "\r\n", cl->http_1_0 ? HTTP_1_0 : HTTP_1_1, status, len);
  }  
  log_write(LL_MSG, "Sending response: %s", result);
  aio_write(NULL, result, strlen(result));

  if (!cl->keepalive) {
    aio_write(wf_response_sent, data, len);
  }
  else {
    aio_write(NULL, data, len);
    log_write(LL_MSG, "Setting timeout for %s", cur_slot->name);
    aio_set_timer(tf_expire_client, CLIENT_TIMEOUT);
  }

  /* unlink host slot from this client now that request has been processed */  
  if (cl->host != NULL) {
    if (cl->host->client == cur_slot)
      cl->host->client = NULL;
    cl->host = NULL;
  }
}

/* data request from client finished...send data and prepare response */
static void rf_client_data(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  HOST_SLOT *host = cl->host;

  /* Send read data to host slot (if we haven't seen this sequence already) */
  if (host != NULL)
  {
      if (cur_slot->bytes_ready > 0 && cl->in_sequence > host->host_sequence) {
        log_write(LL_MSG, "Sending %d bytes to host %d", cur_slot->bytes_ready, host->s.sequence);
        log_write(LL_DEBUG, "%.*s", cur_slot->bytes_ready, cur_slot->readbuf);
        aio_write_other((AIO_SLOT *)host, NULL, cur_slot->readbuf, cur_slot->bytes_ready);
        host->host_sequence = cl->in_sequence;
      }

      host_set_last_request(host);
  }

  /* Send pending server data back to client */
  send_response();

  log_write(LL_MSG, "Waiting for next request");
  aio_setread(rf_client_start, NULL, 5);
}

/* send pending server data back to client */
static void send_response()
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  BLOCK *cur, *next;
  char result[256];
  int len = 0;

  /* Host connection may have been closed before we finished request */
  if (cl->host == NULL) {
    log_write(LL_ERROR, "Host connection not found");
    send_result(400, "Host not found", 14);
    return;
  }

  /* Remove data up to requested starting position (we know client has received it now) */
  while ((cur = cl->host->queue) != NULL && cur->sequence < cl->out_sequence) {
	cl->host->queue = cur->next;
    free(cur);
  }

  /* Determine size of response */
  cur = cl->host->queue;
  while (cur != NULL) {
    len += cur->data_size;
	cur = cur->next;
  }

  /* Close connection after processing a bunch of requests */
  if (++cl->request_count >= s_max_requests) {
    sprintf(result, "%s 200 OK\r\n" HTTP_CLOSE "\r\n", cl->http_1_0 ? HTTP_1_0 : HTTP_1_1, len);
    cl->keepalive = 0;
  } else {
    sprintf(result, "%s 200 OK\r\n" HTTP_HEADERS "\r\n", cl->http_1_0 ? HTTP_1_0 : HTTP_1_1, len);
  }
  
  log_write(LL_MSG, "Sending response for host %d: %s", cl->host->s.sequence, result);

  /* Combine header with first block if it fits in our buffer */
  cur = cl->host->queue;
  len = strlen(result);
  if (cur != NULL && len + cur->data_size < sizeof(result)) {
    log_write(LL_MSG, "Sending %d bytes to client", cur->data_size);
    memcpy(result + len, cur->data, cur->data_size);
    len += cur->data_size;
    cl->host->queue = cur->next;
    free(cur);
  }
  aio_write(NULL, result, len);

  /* Send all pending data back to client */
  cur = cl->host->queue;
  while (cur != NULL)
  {
    log_write(LL_MSG, "Sending %d bytes to client", cur->data_size);
    log_write(LL_DEBUG, "%.*s", cur->data_size, cur->data);

    next = cur->next;
    aio_write(NULL, cur->data, cur->data_size);
    free(cur);
    cur = next;
  }
  cl->host->queue = NULL;
  
  if (!cl->keepalive)
    aio_write(wf_response_sent, NULL, 0);
  else {
    log_write(LL_MSG, "Setting timeout for %s", cur_slot->name);
    aio_set_timer(tf_expire_client, CLIENT_TIMEOUT);
  }

  /* unlink host slot from this client now that request has been processed */  
  if (cl->host != NULL) {
    if (cl->host->client == cur_slot)
      cl->host->client = NULL;
    cl->host = NULL;
  }
}

static void wf_response_sent()
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;

  log_write(LL_MSG, "Response sent and keep alive off");
  aio_close(0);
}

static void qf_client_check(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;

  /* All output data has been written */
}

static void tf_expire_client()
{
  log_write(LL_MSG, "No activity for client %s, closing", cur_slot->name);
  aio_close(0);
}
