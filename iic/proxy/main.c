/* IIC Proxy
 * Copyright (C) 2003 imidio.  All rights reserved.
 *
 * Adapted from code with following copyright:
 */

/*
 * VNC Reflector
 * Copyright (C) 2001-2003 HorizonLive.com, Inc.  All rights reserved.
 *
 * This software is released under the terms specified in the file LICENSE,
 * included.  HorizonLive provides e-Learning and collaborative synchronous
 * presentation solutions in a totally Web-based environment.  For more
 * information about HorizonLive, please see our website at
 * http://www.horizonlive.com.
 *
 * This software was authored by Constantin Kaplinsky <const@ce.cctpu.edu.ru>
 * and sponsored by HorizonLive.com, Inc.
 *
 * $Id: main.c,v 1.9 2004-07-02 20:03:32 mike Exp $
 * Main module
 */

/*
 * Proxy server listens on specified port and reads proxy command
 * from client.  Proxy command controlls how connection is completed:
 *
 * PRXY<address>:<port>\0
 *
 * The proxy opens a connection to this address (by name or ip address)
 * and streams data between the two endpoints.  The proxy command itself
 * is stripped from this stream.
 *
 * In addition, if HTTP detect mode is on, checks for requests that
 * start with "GET " or "POST" and connects to specified server.
 *
 * client_io handles connection with originating client, host_io
 * makes connection to actual server.
 *
 * To do:
 * Make sure resolving address of host and connecting to host do not block
 * other operations. (Perhaps solve resolve problem by forcing client to
 * send IP address.)
 * Add allowed list to control which servers can be reached through the
 * proxy.
 */

#include <stdio.h>
#include <stdlib.h>
#ifdef LINUX
#include <unistd.h>
#include <getopt.h>
#endif
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#ifdef _WIN32
#include <winsock2.h>
#include "getopt.h"
#endif

#include "async_io.h"
#include "logging.h"
#include "proxy.h"
#include "host_connect.h"
//#include "host_io.h"
#include "client_io.h"

extern int			opterr;				/* if error message should be printed */
extern int			optind;				/* index into parent argv vector */
extern int			optopt;				/* character checked for validity */
extern int			optreset;			/* reset getopt */
extern char			*optarg;			/* argument associated with option */

/*
 * Configuration options
 */

static int   opt_no_banner;
static int   opt_foreground;
static int   opt_cl_listen_port;
static char *opt_active_filename;
static char *opt_log_filename;
static int   opt_stderr_loglevel;
static int   opt_file_loglevel;
static char  opt_pid_file[256];
static char *opt_bind_ip;
static char *opt_host_ip;
static int   opt_max_requests;

char * __progname = "iic proxy";

/*
 * Global variables
 */

/*
 * Functions local to this file
 */

static void parse_args(int argc, char **argv);
static void report_usage(char *program_name);
static int write_pid_file(void);
static int remove_pid_file(void);

/*
 * Implementation
 */

int main(int argc, char **argv)
{

#ifdef _WIN32
  WSADATA wsaData;
  WORD wVersionRequested = MAKEWORD(2, 0);

  if (WSAStartup(wVersionRequested, &wsaData) != 0) 
  {
	fprintf(stderr, "Unable to initialize winsock: %d\n", WSAGetLastError());
	exit(-1);
  }
 #endif

  /* Parse command line, exit on error */
  parse_args(argc, argv);

  if (!opt_no_banner) {
    fprintf(stderr,
	"IIC Proxy Server %s - Copyright (C) 2003 imidio.com\n",
            VERSION);
  }

  if (!log_open(opt_log_filename, opt_file_loglevel,
                (opt_foreground) ? opt_stderr_loglevel : -1)) {
    fprintf(stderr, "%s: error opening log file (ignoring this error)\n",
            argv[0]);
  }

  log_write(LL_MSG, "Starting IIC Proxy Server %s", VERSION);

#ifdef LINUX
  /* Fork the process to the background if necessary */
  if (!opt_foreground) {
    if (!opt_no_banner) {
      fprintf(stderr, "Starting in the background, "
              "see the log file for errors and other messages.\n");
    }

    if (getpid() != 1) {
      signal(SIGTTIN, SIG_IGN);
      signal(SIGTTOU, SIG_IGN);
      signal(SIGTSTP, SIG_IGN);
      if (fork ())
        return 0;
      setsid();
    }
    close(0);
    close(1);
    close(2);
    log_write(LL_INFO, "Switched to the background mode");
  }
#endif

  /* Initialization */
  set_active_file(opt_active_filename);
  if (opt_host_ip == NULL || set_host_address(opt_host_ip) == 0)
  {
	  log_write(LL_ERROR, "Cannot resolve host name %s", opt_host_ip);
	  exit(1);
  }

  aio_init();
  if (opt_bind_ip != NULL) {
    if (aio_set_bind_address(opt_bind_ip)) {
      log_write(LL_INFO, "Would bind listening sockets to address %s",
                opt_bind_ip);
    } else {
      log_write(LL_WARN, "Illegal address to bind listening sockets to: %s",
                opt_bind_ip);
    }
  }

  /* Main work */
  if (client_listen(opt_cl_listen_port, opt_max_requests))
  {
    if (write_pid_file()) {
      set_control_signals();
      aio_mainloop();
      remove_pid_file();
    }
  }

  log_write(LL_MSG, "Terminating");

  /* Close logs */
  if (!log_close() && opt_foreground) {
    fprintf(stderr, "%s: error closing log file (ignoring this error)\n",
            argv[0]);
  }

  /* Done */
  exit(1);
}

static void parse_args(int argc, char **argv)
{
  int err = 0;
  int c;
  char *temp_pid_file = NULL;
  char temp_buf[32];            /* 32 bytes should be more than enough */

  opt_no_banner = 0;
  opt_foreground = 0;
  opt_stderr_loglevel = -1;
  opt_file_loglevel = -1;
  opt_active_filename = NULL;
  opt_cl_listen_port = -1;
  opt_pid_file[0] = '\0';
  opt_bind_ip = NULL;
  opt_host_ip = NULL;
  opt_max_requests = -1;

  while (!err &&
         (c = getopt(argc, argv, "hqv:f:c:g:l:i:b:n:r:o:")) != -1) {
    switch (c) {
    case 'h':
      err = 1;
      break;
    case 'q':
      opt_no_banner = 1;
      break;
    case 'v':
      if (opt_file_loglevel != -1)
        err = 1;
      else
        opt_file_loglevel = atoi(optarg);
      break;
    case 'f':
      opt_foreground = 1;
      if (opt_stderr_loglevel != -1)
        err = 1;
      else
        opt_stderr_loglevel = atoi(optarg);
      break;
    case 'n':
      if (opt_max_requests != -1)
        err = 1;
      else
        opt_max_requests = atoi(optarg);
      break;
    case 'g':
      if (opt_log_filename != NULL)
        err = 1;
      else
        opt_log_filename = optarg;
      break;
    case 'a':
      if (opt_active_filename != NULL)
        err = 1;
      else
        opt_active_filename = optarg;
      break;
    case 'l':
      if (opt_cl_listen_port != -1)
        err = 1;
      else {
        opt_cl_listen_port = atoi(optarg);
        if (opt_cl_listen_port <= 0)
          err = 1;
      }
      break;
    case 'i':
      if (temp_pid_file != NULL)
        err = 1;
      else
        temp_pid_file = optarg;
      break;
    case 'b':
      if (opt_bind_ip != NULL)
        err = 1;
      else
        opt_bind_ip = optarg;
      break;
    case 'c':
      if (opt_host_ip != NULL)
        err = 1;
      else
        opt_host_ip = optarg;
      break;
    case 'r':
        log_set_rotation_size((size_t)atoi(optarg));
        break;
    case 'o':
        log_set_history_len((size_t)atoi(optarg));
        break;
    default:
      err = 1;
    }
  }

  /* Print usage help on error */
  if (err || optind != argc) {
    report_usage(argv[0]);
    exit(1);
  }

  /* Provide reasonable defaults for some options */
  if (opt_file_loglevel == -1)
    opt_file_loglevel = LL_INFO;
  if (opt_log_filename == NULL)
    opt_log_filename = "proxy.log";
  if (opt_cl_listen_port == -1)
    opt_cl_listen_port = 80;
  if (opt_max_requests == -1)
    opt_max_requests = 64;

  /* Append listening port number to pid filename */
  if (temp_pid_file != NULL) {
    sprintf(temp_buf, "%d", opt_cl_listen_port);
    sprintf(opt_pid_file, "%.*s.%s", (int)(255 - strlen(temp_buf) - 1),
            temp_pid_file, temp_buf);
  }
}

static void report_usage(char *program_name)
{
  fprintf(stderr,
          "IIC Proxy Server %s.  Copyright (C) 2003 imidio.com"
          "\n\n",
          VERSION);

  fprintf(stderr, "Usage: %s [OPTIONS...]\n\n",
          program_name);

  fprintf(stderr,
          "Options:\n"
          "  -i PID_FILE     - write pid file, appending listening port"
          " to the filename\n"
          "  -l LISTEN_PORT  - port to listen for client connections"
          " [default: 80]\n"
          "  -b IP_ADDRESS   - bind listening sockets to a specific IP"
          " [default: any]\n");
  fprintf(stderr,
          "  -g LOG_FILE     - write logs to the specified file"
          " [default: proxy.log]\n"
          "  -v LOG_LEVEL    - set verbosity level for the log file (0..%d)"
          " [default: %d]\n"
          "  -f LOG_LEVEL    - run in foreground, show logs on stderr"
          " at the specified\n"
          "                    verbosity level (0..%d) [note: use %d for"
          " normal output]\n"
          "  -c IP_ADDRESS   - connect all incoming to this server\n"
          "  -n count        - close connection after count requests\n"
          "  -r size         - rotate logs when LOG_FILE reaches this size (in bytes)"
          " [default: 5000000]\n"
          "  -o length       - number of historical logs to keep when rotating logs"
          " [default: 5]\n"
          "  -q              - suppress printing copyright banner at startup\n"
          "  -h              - print this help message\n\n",
          LL_DEBUG, LL_INFO, LL_DEBUG, LL_MSG);

}

static int write_pid_file(void)
{
#ifdef LINUX
  int pid_fd, len;
  char buf[32];                 /* 32 bytes should be more than enough */

  if (opt_pid_file[0] == '\0')
    return 1;

  pid_fd = open(opt_pid_file, O_WRONLY | O_CREAT | O_EXCL, 0644);
  if (pid_fd == -1) {
    log_write(LL_ERROR, "Pid file exists, another instance may be running");
    return 0;
  }
  sprintf(buf, "%d\n", (int)getpid());
  len = strlen(buf);
  if (write(pid_fd, buf, len) != len) {
    close(pid_fd);
    log_write(LL_ERROR, "Error writing to pid file");
    return 0;
  }

  log_write(LL_DEBUG, "Wrote pid file: %s", opt_pid_file);

  close(pid_fd);
#endif
  return 1;
}

static int remove_pid_file(void)
{
#ifdef LINUX
  if (opt_pid_file[0] != '\0') {
    if (unlink(opt_pid_file) == 0) {
      log_write(LL_DEBUG, "Removed pid file", opt_pid_file);
    } else {
      log_write(LL_WARN, "Error removing pid file: %s", opt_pid_file);
      return 0;
    }
  }
#endif
  return 1;
}

