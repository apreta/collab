#include <stdio.h>
#ifdef LINUX
#include <unistd.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#else
#include <winsock2.h>
#include <time.h>
#endif

#include "port.h"

void _set_nonblock(int fd, int act)
{
#ifdef LINUX
	int flags;

	flags = fcntl(fd, F_GETFL, 0);
	if(flags < 0) return;

	if(act)
		flags |= O_NONBLOCK;
	else
		flags &= ~O_NONBLOCK;

	fcntl(fd, F_SETFL, flags);
#else // _WIN32
	long flag = act;
	ioctlsocket(fd, FIONBIO, &flag);
#endif
}

int _gettimeofday(struct timeval * tv)
{
#ifdef LINUX
	struct timezone tz;
	return gettimeofday(tv, &tz);
#else
	time_t now;

	time(&now);

	tv->tv_sec = now;
	tv->tv_usec = 0;
	return 0;
#endif
}

#ifdef _WIN32
int inet_aton(const char * cp, struct in_addr * ip)
{
	unsigned long l_ip;

	l_ip = inet_addr(cp);
	if (l_ip == INADDR_NONE)
		return -1;
	ip->S_un.S_addr = l_ip;
	return 0;
}

const char * local_strerror(int error_code)
{
	static char buffer[1024];

	buffer[0] = '\0';

	FormatMessage( 
		FORMAT_MESSAGE_FROM_SYSTEM | 
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		error_code,
		0, // Default language
		(LPTSTR) buffer,
		sizeof(buffer),
		NULL 
	);

	return buffer;
}

#endif
