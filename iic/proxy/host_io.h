/* VNC Reflector
 * Copyright (C) 2001-2003 HorizonLive.com, Inc.  All rights reserved.
 *
 * This software is released under the terms specified in the file LICENSE,
 * included.  HorizonLive provides e-Learning and collaborative synchronous
 * presentation solutions in a totally Web-based environment.  For more
 * information about HorizonLive, please see our website at
 * http://www.horizonlive.com.
 *
 * This software was authored by Constantin Kaplinsky <const@ce.cctpu.edu.ru>
 * and sponsored by HorizonLive.com, Inc.
 *
 * $Id: host_io.h,v 1.6 2005-03-16 00:11:08 mike Exp $
 * Asynchronous interaction with VNC host.
 */

#ifndef _REFLIB_HOST_IO_H
#define _REFLIB_HOST_IO_H

#define TYPE_HOST_LISTENING_SLOT   2
#define TYPE_HOST_CONNECTING_SLOT  3
#define TYPE_HOST_ACTIVE_SLOT      4

#define HOST_RESPONSE_WAIT 250  /* millis */
#define HOST_TIMEOUT 300000     /* 5 mins */

/* Data queued to be sent to client */
typedef struct _BLOCK {
  struct _BLOCK * next;

  long sequence;       /* position in "down" stream of first byte in this block */
  int data_size;
  char data[1];
} BLOCK;


/* Extension to AIO_SLOT structure to hold state for host connection */
typedef struct _HOST_SLOT {
  AIO_SLOT s;

  AIO_SLOT *client;    /* last associated client slot */
  
  BLOCK *queue;	       /* received data wenting to be sent to client */

  long last_request;   /* time of last request from client */
  long connect_time;   /* time we connected */

  int client_sequence; /* position in "down" stream of start of next block to be queued */
  int host_sequence;   /* position in "up" stream of the start of last request forwarded to host */

} HOST_SLOT;

extern void host_activate(void);
extern void host_close_hook(void);
void host_set_last_request(HOST_SLOT *hs);

#endif /* _REFLIB_HOST_IO_H */
