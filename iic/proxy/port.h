#ifndef PORT_H
#define PORT_H

// Abstractions for some network functions

#ifdef LINUX

void _set_nonblock(int fd, int act);
int _gettimeofday(struct timeval * tv);

#define _write(f,b,l) write(f,b,l)
#define _read(f,b,l) read(f,b,l)
#define _error errno
#define _close close
#define _strerror strerror

#define CLEAR_ERR (errno = 0)
#define CHECK_ERR errno

#define _EAGAIN EAGAIN
#define _EWOULDBLOCK EWOULDBLOCK  // really the same as EAGAIN
#define _EINTR EINTR
#define _EINPROGRESS EINPROGRESS

#endif

#ifdef _WIN32

void _set_nonblock(int fd, int act);
int _gettimeofday(struct timeval * tv);

#define _write(f,b,l) send(f,b,l,0)
#define _read(f,b,l) recv(f,b,l,0)
#define _error WSAGetLastError()
#define _close closesocket
#define _strerror local_strerror

#define CLEAR_ERR
#define CHECK_ERR GetLastError()

#define _EAGAIN WSAEWOULDBLOCK
#define _EWOULDBLOCK WSAEWOULDBLOCK
#define _EINTR WSAEINTR
#define _EINPROGRESS WSAEINPROGRESS

extern void __declspec(dllimport) __stdcall Sleep(unsigned long millis);
#define sleep(sec) Sleep(sec * 1000)
int inet_aton(const char * cp, struct in_addr * ip);
const char * local_strerror(int error_code);

#define strcasecmp stricmp
#define strncasecmp strnicmp

#endif

#endif
