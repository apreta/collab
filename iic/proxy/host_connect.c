/* VNC Reflector
 * Copyright (C) 2001-2003 HorizonLive.com, Inc.  All rights reserved.
 *
 * This software is released under the terms specified in the file LICENSE,
 * included.  HorizonLive provides e-Learning and collaborative synchronous
 * presentation solutions in a totally Web-based environment.  For more
 * information about HorizonLive, please see our website at
 * http://www.horizonlive.com.
 *
 * This software was authored by Constantin Kaplinsky <const@ce.cctpu.edu.ru>
 * and sponsored by HorizonLive.com, Inc.
 *
 * $Id: host_connect.c,v 1.5 2005-02-28 05:54:26 mike Exp $
 * Connecting to a VNC host
 */

#include <stdio.h>
#include <stdlib.h>
#ifdef LINUX
#include <unistd.h>
#endif
#include <string.h>
#include <errno.h>
#ifdef LINUX
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#else
#include <winsock2.h>
#endif
#include <ctype.h>

#include "proxy.h"
#include "logging.h"
#include "async_io.h"
#include "host_io.h"
#include "client_io.h"
#include "host_connect.h"
#include "port.h"

static void host_init_hook(void);
static void host_accept_hook(void);
static void rf_host_ver(void);
static void send_client_initmsg(void);

static char *s_host_name = NULL;
static struct sockaddr_in s_host_addr;


/*
 * Connect to a remote host
 */

int set_host_address(char *address)
{
  struct hostent *phe;
  char hostname[128];
  int port;
  char *p;

  s_host_name = address;

  p = strrchr(address, ':');
  if (p != NULL) {
    hostname[0] = '\0';
    strncat(hostname, address, p - address);
    port = atoi(p + 1);
  }
  else
  {
    log_write(LL_ERROR, "Invalid address %s received", address);
    return 0;
  }

  /* Forward reflector -> host connection */
  log_write(LL_MSG, "Connecting to %s, port %d", hostname, port);

  if ((s_host_addr.sin_addr.s_addr = inet_addr(hostname)) == INADDR_NONE) {
	phe = gethostbyname(hostname);
	if (phe == NULL) {
	  log_write(LL_ERROR, "Could not get host address: %s", _strerror(errno));
	  return 0;
	}
	memcpy(&s_host_addr.sin_addr.s_addr, phe->h_addr, phe->h_length);
  }

  s_host_addr.sin_family = AF_INET;
  s_host_addr.sin_port = htons((unsigned short)port);

  return 1;
}

int connect_to_host()
{
  int host_fd;
  AIO_SLOT *new_slot;

  host_fd = socket(AF_INET, SOCK_STREAM, 0);
  if (host_fd == -1) {
    log_write(LL_ERROR, "Could not create socket: %s", _strerror(errno));
    return 0;
  }

  if (connect(host_fd, (struct sockaddr *)&s_host_addr,
              sizeof(s_host_addr)) != 0) {
    log_write(LL_ERROR, "Could not connect: %s", _strerror(errno));
    _close(host_fd);
    return 0;
  }

  aio_add_slot(host_fd, NULL, host_init_hook, sizeof(HOST_SLOT), &new_slot);
  ((CL_SLOT *)cur_slot)->host = (HOST_SLOT *)new_slot;
  ((HOST_SLOT *)new_slot)->client = cur_slot;

  log_write(LL_MSG, "Connection established, host = %d", new_slot->sequence);

  return 1;
}

static void host_init_hook(void)
{
  cur_slot->type = TYPE_HOST_CONNECTING_SLOT;
  aio_setclose(host_close_hook);

  host_activate();
}

static void host_accept_hook(void)
{
  log_write(LL_MSG, "Accepted host connection from %s", cur_slot->name);

  host_init_hook();
}

static void rf_host_conn_failed(void)
{
  HOST_SLOT *hs = (HOST_SLOT *)cur_slot;

  log_write(LL_ERROR, "Proxy connection failed");
  aio_close(0);
}

