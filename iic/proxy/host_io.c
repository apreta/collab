/* VNC Reflector
 * Copyright (C) 2001-2003 HorizonLive.com, Inc.  All rights reserved.
 *
 * This software is released under the terms specified in the file LICENSE,
 * included.  HorizonLive provides e-Learning and collaborative synchronous
 * presentation solutions in a totally Web-based environment.  For more
 * information about HorizonLive, please see our website at
 * http://www.horizonlive.com.
 *
 * This software was authored by Constantin Kaplinsky <const@ce.cctpu.edu.ru>
 * and sponsored by HorizonLive.com, Inc.
 *
 * $Id: host_io.c,v 1.8 2005-03-16 00:11:08 mike Exp $
 * Asynchronous interaction with VNC host.
 */

#include <stdio.h>
#include <stdlib.h>
#ifdef LINUX
#include <unistd.h>
#include <sys/time.h>
#else
#include <time.h>
#endif
#include <string.h>
#include <sys/types.h>

#include "proxy.h"
#include "async_io.h"
#include "logging.h"
#include "client_io.h"
#include "host_connect.h"
#include "host_io.h"
#include "port.h"

static void rf_host_data(void);
static void tf_expire_host();

/*
 * Implementation
 */

/* Prepare host I/O slot for operating in main protocol phase */
void host_activate(void)
{
  HOST_SLOT *hs = (HOST_SLOT *)cur_slot;
  hs->host_sequence = -1;
  hs->connect_time = time(NULL);

  aio_setread(rf_host_data, NULL, -READ_SIZE);
}

/* On-close hook */
void host_close_hook(void)
{
  HOST_SLOT *hs = (HOST_SLOT *)cur_slot;

  /* invalidate client pointer to this host */
  if (hs->client != NULL) {
    CL_SLOT *cl = (CL_SLOT *)hs->client;
    log_write(LL_MSG, "Connected client %s dropped for host %d", cl->s.name , hs->s.sequence);
    cl->host = NULL;
  }
  
  /* delete queued data */
  BLOCK *cur = hs->queue, *next;
  while (cur != NULL)
  {
    next = cur->next;
    free(cur);
    cur = next;
  }
  hs->queue = NULL;

  if (cur_slot->errread_f) {
    if (cur_slot->io_errno) {
      log_write(LL_ERROR, "Host I/O error, read: %s",
                _strerror(cur_slot->io_errno));
    } else {
      log_write(LL_ERROR, "Host I/O error, read");
    }
  } else if (cur_slot->errwrite_f) {
    if (cur_slot->io_errno) {
      log_write(LL_ERROR, "Host I/O error, write: %s",
                _strerror(cur_slot->io_errno));
    } else {
      log_write(LL_ERROR, "Host I/O error, write");
    }
  } else if (cur_slot->errio_f) {
    log_write(LL_ERROR, "Host I/O error");
  }

  log_write(LL_WARN, "Closing connection to host");
  remove_active_file();
}

/* handle data read from host */
static void rf_host_data(void)
{
  HOST_SLOT *hs = (HOST_SLOT *)cur_slot;

  log_write(LL_MSG, "Received %d bytes from host %d", cur_slot->bytes_ready, cur_slot->sequence); 

  /* Queue data for client */
  if (cur_slot->bytes_ready > 0) {
    BLOCK * buff = malloc(sizeof(BLOCK) + cur_slot->bytes_ready);
    buff->next = NULL;
    buff->data_size = cur_slot->bytes_ready;
    memcpy(buff->data, cur_slot->readbuf, cur_slot->bytes_ready);
    buff->sequence = hs->client_sequence;
    hs->client_sequence += buff->data_size;

    if (hs->queue == NULL) {
      hs->queue = buff;
    } else {
      /* add to end of queue */
      BLOCK * cur = hs->queue;
      while (cur->next != NULL)
        cur = cur->next;
      cur->next = buff;
    }
  }
  aio_setread(rf_host_data, NULL, -READ_SIZE);
}

void host_set_last_request(HOST_SLOT *hs)
{
  AIO_SLOT *save_slot = cur_slot;
  cur_slot = (AIO_SLOT*)hs;
  aio_set_timer(tf_expire_host, HOST_TIMEOUT);
  cur_slot = save_slot;
}

static void tf_expire_host()
{
  log_write(LL_MSG, "No client activity for host %d, closing", cur_slot->sequence);
  aio_close(0);
}
