/* VNC Reflector
 * Copyright (C) 2001-2003 HorizonLive.com, Inc.  All rights reserved.
 *
 * This software is released under the terms specified in the file LICENSE,
 * included.  HorizonLive provides e-Learning and collaborative synchronous
 * presentation solutions in a totally Web-based environment.  For more
 * information about HorizonLive, please see our website at
 * http://www.horizonlive.com.
 *
 * This software was authored by Constantin Kaplinsky <const@ce.cctpu.edu.ru>
 * and sponsored by HorizonLive.com, Inc.
 *
 * $Id: proxy.h,v 1.2 2003-11-08 04:05:31 mike Exp $
 * Global include file
 */

#ifndef _PROXY_H
#define _PROXY_H

#define VERSION  "1.1.1"

/* active.c */

extern int write_active_file(void);
extern int remove_active_file(void);
extern int set_active_file(char *file_path);

/* control.c */

extern void set_control_signals(void);

// How many bytes to attempt to read at once
#define READ_SIZE 1500

#endif /* _PROXY_H */
