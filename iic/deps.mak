# Generate depedency info.
# Push down one level so KDevelop load tags will find the dependencies
DEPSDIR = deps/.deps

%.o: %.c
	@echo '$(CC) $(CFLAGS) -c $<'; \
	$(CC) $(CFLAGS) -Wp,-MD,$(DEPSDIR)/$(*F).pp -c $<
	@-cp $(DEPSDIR)/$(*F).pp $(DEPSDIR)/$(*F).P; \
	tr ' ' '\012' < $(DEPSDIR)/$(*F).pp \
	| sed -e 's/^\\$$//' -e '/^$$/ d' -e '/:$$/ d' -e 's/$$/ :/' \
	>> $(DEPSDIR)/$(*F).P; \
	rm $(DEPSDIR)/$(*F).pp

%.o: %.cpp
	@echo '$(CC) $(CPPFLAGS) -c $<'; \
	$(CC) $(CPPFLAGS) -Wp,-MD,$(DEPSDIR)/$(*F).pp -c $<
	@-cp $(DEPSDIR)/$(*F).pp $(DEPSDIR)/$(*F).P; \
	tr ' ' '\012' < $(DEPSDIR)/$(*F).pp \
	| sed -e 's/^\\$$//' -e '/^$$/ d' -e '/:$$/ d' -e 's/$$/ :/' \
	>> $(DEPSDIR)/$(*F).P; \
	rm $(DEPSDIR)/$(*F).pp
