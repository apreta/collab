#ifndef QUEUE_H
#define QUEUE_H

#define MAX_MSG_SIZE 8000 // under standard default (8192)
#define MAX_CLIENTS 100
#define MAX_QUEUE_BYTES 500000

struct msgbuffer
{
    long to;              // message destination (0 is server)
    long msg_size;
    long total_size;      // if larger then msg_size, message was split and more segments follow
    long from;            // message source
    long command;
    char data[MAX_MSG_SIZE];
};

// fixed bad_ptr
#define MSG_HEADER_SIZE (sizeof(long)*5)
#define MSG_CMD_NORMAL 0
#define MSG_CMD_ERROR 1

class MsgQueue
{
    int queue_id;
    IString queue_name;
    int client_id;
    
    msgbuffer send_buffer;
    
    msgbuffer recv_buffer;
        
public:
    MsgQueue(const char * name);

    int create_queue(int client_id, char key);
    int open_queue(int client_id, char key);
    
    int send_message(int to_id, const char *msg_data, int msg_size, int command);
    
    int get_message(int & from_id, int & total_size, int & command);
    int get_message_data(char *msg_data, int msg_size);
    
};


#endif
