/**
 * jabapi.cpp : entry point for web jabber api service
 *
 * (c)2004 imidio all rights reserved
 *
 */

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>

#include <xmlrpc-c/base.h>

#include "httpd.h"
#include "apr_strings.h"
#include "apr_thread_proc.h"

#include "jcomponent/util/util.h"
#include "jcomponent/jcomponent_ext.h"
#include "jcomponent/presence.h"
#include "jcomponent/rpc.h"
#include "jcomponent/util/rpcresult.h"
#include "util/ithread.h"
#include "util/istring.h"
#include "util/iobject.h"

#include "http_log.h"

#include "jabapi.h"
#include "queue.h"

#define JCOMPNAME "extapi"

#define EMPTY_RESPONSE_STR \
"<?xml version=\"1.0\"?>" \
"<methodResponse><params><param><array><data><value><i4>0</i4>" \
"</value></data></array></param></params></methodResponse>"

#define METHOD_NAME_TAG "<methodName>"

// Message queue
MsgQueue queue(".");

// mutex to protect the result hash table
IMutex table_mutex;

// hash table to stick requests and results
IStringHash resultTable;

static unsigned call_index = 0;
server_rec *g_server = NULL;

// class for stored requests and results
class ApiResults : public IObject
{
    long     from;
    void *   context;
    IString	 request;

public:
    ApiResults(long _from, void * _context = NULL)
    {
        from = _from;
        context = _context;
    }

    ApiResults(long _from, const char * _request, void * _context = NULL)
    {
        from = _from;
        request = _request;
        context = _context;
    }


    long GetFrom()                            { return from; }
    void * GetContext()                       { return context; }
    IString & GetRequest()					  { return request; }
    
    enum { TYPE_JABAPI = 567 };
    virtual int type()
        { return TYPE_JABAPI; }
    virtual unsigned int hash()
        { return 42; }
    virtual bool equals(IObject *obj)
        { return obj->type() == type(); }
};

// class for caching auth results
struct UserAuth : public IObject
{
    IString auth_request;
    long auth_time;

    UserAuth(const char *_auth_request) :
        auth_request(_auth_request), auth_time(time(NULL))
    {
    }

    enum { TYPE_USERAUTH = 3964 };
    virtual int type()
        { return TYPE_USERAUTH; }
    virtual unsigned int hash()
        { return auth_request.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && auth_request == ((UserAuth*)obj)->auth_request; }
};

IMutex authCacheMutex;
IHash authCache;


void _debug(char *str, ...)
{
    char out_string[1024];

    va_list args;
    va_start(args, str);
    vsprintf(out_string, str, args);
    va_end(args);

    ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, g_server, out_string);
}


// Cache auth result.
void cache_result(IString & auth_request)
{
	_debug("Caching %s", auth_request.get_string());

	IUseMutex lock(authCacheMutex);
	
	UserAuth * auth = (UserAuth *) authCache.get(&auth_request);
	if (auth == NULL)
	{
		auth = new UserAuth(auth_request);
		authCache.insert(new IString(auth_request), auth);
	}
	else
	{
		auth->auth_time = time(NULL);
	}
}

// Check cache for previous auth result and use if granted within the past minute.
bool cache_check(IString & auth_request)
{
	_debug("Checking cache for %s", auth_request.get_string());

	IUseMutex lock(authCacheMutex);

	UserAuth * auth = (UserAuth *) authCache.get(&auth_request);
	if (auth != NULL)
	{
		if (time(NULL) - auth->auth_time < 60)
		{
			_debug("Cached auth used");
			return true;
		}
		_debug("Cached auth expired");
		authCache.remove(&auth_request);
	}
	
	return false;
}


// Forward string back to request (result or error)
void return_string(int from, const char * data, int command)
{
    if (queue.send_message(from, data, strlen(data) + 1, command) < 0)
    {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, g_server, "mod_jabapi: error sending result back to client");
    }
    
}

// Remove pending result object
void remove_result(const char *id)
{
    table_mutex.lock();
    resultTable.remove(id, true);
    table_mutex.unlock();

}


// Did result indicate user authorized?
bool get_recording_auth_result(xmlrpc_env *env, xmlrpc_value *data)
{
    int ret;
    char *pstr = NULL;
    char *result;

    xmlrpc_parse_value(env, data, "(s)", &result);
    if (!strcmp(result,"OK"))
    {
        ret = true;
    }
    else
    {
        ret = false;
    }

    return ret;
}


// Did result indicate user authorized?
bool get_docshare_auth_result(xmlrpc_env *env, xmlrpc_value *data)
{
    int ret;
    char *pstr = NULL;
    int result;

    xmlrpc_parse_value(env, data, "(i)", &result);
    if (result == 0)
    {
        ret = true;
    }
    else
    {
        ret = false;
    }

    return ret;
}


// Callback from jcomponent on error
void RPC_error_raw(const char *id, char* data)
{
    // Handle, so clients are not waiting forever
    _debug("Error processing RPC request: %s", id);

    const char * end = strchr(id, ':');
    if (end == NULL)
        return;
    int len = end - id;

    // Use id to figure out which call failed & handle 
    if (!strncmp(id, "api", len))
    {
        // retrieve mtg_results structure from hash table
        table_mutex.lock();
        ApiResults* results = (ApiResults*) resultTable.get(id);
        table_mutex.unlock();

        if (results != NULL)
        {
            // Send results back to client
            int from = results->GetFrom();
            return_string(from, data, MSG_CMD_NORMAL);
        }
        else
            ap_log_error(APLOG_MARK, APLOG_WARNING, 0, g_server, "mod_jabapi: couldn't find result table entry for id %s", id);

    }
    else if (!strncmp(id, "authz", len))
    {
        // retrieve mtg_results structure from hash table
        table_mutex.lock();
        ApiResults* results = (ApiResults*) resultTable.get(id);
        table_mutex.unlock();
        if (results != NULL)
        {
            // Send results back to client
            int from = results->GetFrom();
            return_string(from, data, MSG_CMD_ERROR);
        }
        else
            ap_log_error(APLOG_MARK, APLOG_WARNING, 0, g_server, "mod_jabapi: couldn't find result table entry for id %s", id);

    }
    else if (!strncmp(id, "verify", len))
    {
        // retrieve mtg_results structure from hash table
        table_mutex.lock();
        ApiResults* results = (ApiResults*) resultTable.get(id);
        table_mutex.unlock();
        if (results != NULL)
        {
            // Send results back to client
            int from = results->GetFrom();
            return_string(from, data, MSG_CMD_ERROR);
        }
        else
            ap_log_error(APLOG_MARK, APLOG_WARNING, 0, g_server, "mod_jabapi: couldn't find result table entry for id %s", id);
    }
}

// Callback from jcomponent on request completed
void RPC_result_raw(const char * id, char* data)
{
    const char * end = strchr(id, ':');
    if (end == NULL)
        return;
    int len = end - id;

    if (!strncmp(id, "api", len))
    {
        // retrieve mtg_results structure from hash table
        table_mutex.lock();
        ApiResults* results = (ApiResults*) resultTable.get(id);
        table_mutex.unlock();

        if (results != NULL)
        {
            // Send results back to client
            int from = results->GetFrom();
            return_string(from, data, MSG_CMD_NORMAL); 
        }
        else
            ap_log_error(APLOG_MARK, APLOG_WARNING, 0, g_server, "mod_jabapi: couldn't find result table entry for id %s", id);

    }
    else if (!strncmp(id, "authz", len))
    {
        // retrieve mtg_results structure from hash table
        table_mutex.lock();
        ApiResults* results = (ApiResults*) resultTable.get(id);
        table_mutex.unlock();

        if (results != NULL)
        {
            xmlrpc_env env;
            xmlrpc_value *xretval;
            int from = results->GetFrom();
            
            xmlrpc_env_init(&env);

            xretval = xmlrpc_parse_response(&env, data, strlen(data));

			int status = get_recording_auth_result(&env, xretval) ? MSG_CMD_NORMAL : MSG_CMD_ERROR;
			
			if (status == MSG_CMD_NORMAL)
			{
				cache_result(results->GetRequest());
			}
			
            return_string(from, "DONE", status);
        }
        else
            ap_log_error(APLOG_MARK, APLOG_WARNING, 0, g_server, "mod_jabapi: couldn't find result table entry for id %s", id);

    }
    else if (!strncmp(id, "verify", len))
    {
        // retrieve mtg_results structure from hash table
        table_mutex.lock();
        ApiResults* results = (ApiResults*) resultTable.get(id);
        table_mutex.unlock();

        if (results != NULL)
        {
            xmlrpc_env env;
            xmlrpc_value *xretval;
            int from = results->GetFrom();
            
            xmlrpc_env_init(&env);

            xretval = xmlrpc_parse_response(&env, data, strlen(data));
                               
            int status = get_docshare_auth_result(&env, xretval) ? MSG_CMD_NORMAL : MSG_CMD_ERROR;
            
			if (status == MSG_CMD_NORMAL)
			{
				cache_result(results->GetRequest());
			}
			
            return_string(from, "DONE", status);
        }
        else
            ap_log_error(APLOG_MARK, APLOG_WARNING, 0, g_server, "mod_jabapi: couldn't find result table entry for id %s", id);

    }
    else
    {
        RPC_error_raw(id, data);
    }
}

// Callback for response to authentication request
void JC_session_auth(const char * jid, int ok, void * user)
{
    const char * id = strrchr(jid, '/');
    server_rec *s = (server_rec *) user;

    if (id == NULL)
        return;
    ++id;

    if (!strncmp(id, "api", 3))
    {
        // retrieve mtg_results structure from hash table
        table_mutex.lock();
        ApiResults* results = (ApiResults*) resultTable.get(id);
        table_mutex.unlock();

        if (results != NULL)
        {
            // Send results back to client
            int from = results->GetFrom();

			if (ok) 
			{
				cache_result(results->GetRequest());
			}

            return_string(from, "DONE", ok ? MSG_CMD_NORMAL : MSG_CMD_ERROR);

            end_session((session_t)results->GetContext());
            remove_result(id);
        }
        else
            ap_log_error(APLOG_MARK, APLOG_WARNING, 0, s, "mod_jabapi: couldn't find result table entry for id %s", id);
            
    }
}

// Parse request and find target service.
// We simply look for method name element, which should only occur once in request.
static int get_service(const char *data, char **outdata, char *service)
{
  const char * start;
  const char * end;
  const char * dot;
  int i;

  // Look for '<methodName>' in the data
  start = strstr(data, METHOD_NAME_TAG);
  if (start == NULL)
    return -1;

  // Look for for closing '<' of "methodName>service.methodname<"
  end = strchr(start+1, '<');
  if (end == NULL)
    return -1;

  // Look for for first '.' in "<methodName>service.methodname<"
  dot = strchr(start, '.');
  if (dot == NULL)
    return -1;

  // Get service name
  start += strlen(METHOD_NAME_TAG);
  for (i = 0; start + i < dot; i++)
    service[i] = start[i];
  service[i] = '\0';

  // If methodname == "log.set_level", get rid of service name.
  // This is just the special case for the log.set_level method for
  // all non-builtin services
  // bad_ptr
  /*
  if (strncmp(dot+1, "log.set_level", sizeof "log.set_level"-1) == 0)
  {
    memmove(start, dot+1, strlen(dot+1)+1);
  }
  */

  // Schedule APIs are implemented in address book service.
  if (strcmp(service, "schedule") == 0)
  {
    strcpy(service, "addressbk");
  }

  if (strcmp(service, "jabber") == 0)
  {
    if (strncmp(dot+1, "debug_on", sizeof "debug_on"-1) == 0)
    {
      *outdata = "<level>1</level>";
    }
    else if (strncmp(dot+1, "debug_off", sizeof "debug_off"-1) == 0)
    {
      *outdata = "<level>0</level>";
    }
    else
    {
      *outdata = (char *)data;
    }
  }
  else
  {
    *outdata = (char *)data;
  }

  return 0;
}

// Parse user name and password from request
// Simple format:  "AUTH:user@host##password"
static int parse_auth_request(char * data, char ** user, char ** password)
{
    *user = data;

    *password = strstr(*user, "##");
    if (*password == NULL || *password == *user)
        return -1;

    **password = '\0';
    (*password) += 2;

    return 0;
}

static int parse_recording_authorization_request(char * data, char ** user, char ** meetingid)
{
    *user = data;
    *meetingid = strstr(*user, "##");
    if (*meetingid == NULL || *meetingid == *user)
        return -1;

    **meetingid = '\0';
    (*meetingid) += 2;

    return 0;
}

// Forward request to jcomponent.
int forward_request(int from, char* data, server_rec *s)
{
    char service[64];
    char pbuf[64];
    char *pdata;
    int ret;


    // Handle auth request message
    if (strncmp(data, "AUTH:", 5) == 0)
    {
		IString cache_key(data);
		
		if (cache_check(cache_key))
		{
           return_string(from, "DONE", MSG_CMD_NORMAL);
           return 0;
		}
		
        char *user_jid, *password;
        session_t session;
        sprintf(pbuf, "api:%u", call_index++);

        if (parse_auth_request(data + 5, &user_jid, &password))
        {
            return_string(from, "Error parsing auth info", MSG_CMD_ERROR);
            return -1;
        }

        ap_log_error(APLOG_MARK, APLOG_WARNING, 0, s, "mod_jabapi: auth send %s", pbuf);
        
        session = create_auth_session( (void *) s);
        if (session == NULL)
        {
            return_string(from, "Error creating auth session", MSG_CMD_ERROR);
            return -1;
        }

        ApiResults* results = new ApiResults(from, cache_key, session);
        table_mutex.lock();
        resultTable.insert(pbuf, results);
        table_mutex.unlock();

        send_auth_session_request(session, user_jid, pbuf, password);

        return 0;
    }
    
    if (strncmp(data, "REC_AUTHZ:", 10) == 0)
    {
		IString cache_key(data);
		
		if (cache_check(cache_key))
		{
           return_string(from, "DONE", MSG_CMD_NORMAL);
           return 0;
		}
		
        char *userid, *meetingid;
        
        sprintf(pbuf, "authz:%u", call_index++);
        ret = parse_recording_authorization_request(data + 10, &userid, &meetingid);

        ret = rpc_make_call(pbuf, "addressbk", "addressbk.authorize_recording_access", "(ss)", userid, meetingid);

        ApiResults* results = new ApiResults(from, cache_key, NULL);
        table_mutex.lock();
        resultTable.insert(pbuf, results);
        table_mutex.unlock();
        return 0;

    }

    if (strncmp(data, "DOC_AUTHZ:", 10) == 0)
    {
		IString cache_key(data);
		
		if (cache_check(cache_key))
		{
           return_string(from, "DONE", MSG_CMD_NORMAL);
           return 0;
		}
		
        char *userid, *meetingid;

        sprintf(pbuf, "verify:%u", call_index++);
        ret = parse_recording_authorization_request(data + 10, &userid, &meetingid);

        ret = rpc_make_call(pbuf, "controller", "controller.verify_participant", "(sss)", "", userid, meetingid);

        ApiResults* results = new ApiResults(from, cache_key, NULL);
        table_mutex.lock();
        resultTable.insert(pbuf, results);
        table_mutex.unlock();
        return 0;

    }


    // Handle normal RPC request
    pdata = data;
    if (get_service(data, &pdata, service) < 0)
    {
        return_string(from, "Error parsing service and method", MSG_CMD_ERROR);
        return -1;
    }

    sprintf(pbuf, "api:%u", call_index++);

    if (strcmp(service, "jabber") != 0)
    {
        // create meeting results data structure and insert into hash table
        ApiResults* results = new ApiResults(from);
        table_mutex.lock();
        resultTable.insert(pbuf, results);
        table_mutex.unlock();

        ret = rpc_make_call_raw(pbuf, service, pdata, strlen(pdata));
        if (ret < 0)
        {
            switch (ret)
            {
                case RpcUnreachable:
                    return_string(from, "XML router unavailable; try later", MSG_CMD_ERROR);
                    break;
                case RpcFailed:
                    return_string(from, "RPC call failed", MSG_CMD_ERROR);
                    break;
                default:
                    return_string(from, "Error sending request to XML router", MSG_CMD_ERROR);
            }
            
            return ret;
        }
    }
    else
    {
        rpc_send_iq_request(pbuf, "loglevel", "iic:loglevel", pdata);
        
        return_string(from, EMPTY_RESPONSE_STR, MSG_CMD_ERROR);
    }

    return 0;
}

// background processing thread monitors shared memory for work 
static void *APR_THREAD_FUNC jabapi_worker(apr_thread_t *thd, void *data)
{
    server_rec *s = (server_rec *)data;
    int total_size;
    int pos;
    int from;
    int command;
    char *req;
    
    ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, s, "mod_jabapi: worker thread started");

    while (true)
    {
        if (queue.get_message(from, total_size, command) < 0)
        {
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "mod_jabapi: error retrieving request from queue");
            return NULL;
        }
        
        ap_log_error(APLOG_MARK, APLOG_WARNING, 0, s, "mod_jabapi: jabapi_worker get_message %d %d", from, command);
        
        /* can we use a pool for this? */
        req = (char *)malloc(total_size);
        pos = 0;
        
        while (pos < total_size)
        {
            int read;
            if ((read = queue.get_message_data(req + pos, total_size - pos)) < 0)
            {
                ap_log_error(APLOG_MARK, APLOG_ERR, 0, s, "mod_jabapi: error retrieving request data from queue");
                free(req);
                return NULL;
            }
            pos += read;
        }

        /* forward request to jabber */        
        forward_request(from, req, s);
        
        free(req);
    }
    
    return NULL;
}

/*
 * Initialize server process -- setup message queue & connection to jabber
 */
extern "C" int jabapi_initialize(server_rec* server, apr_pool_t *pconf, modjab_cfg *cfg)
{
    config_t conf;
    apr_status_t rs;
    apr_thread_t *thread;
    char * component = cfg->jabber_component;
    char qkey = (strcmp(component, "extapi") == 0) ? 'Q' : 'P';
 
    g_server = server;
    
    conf = config_new();
    config_load(conf, (char *)component, 0, "/opt/iic/conf/config.xml");

    if (jc_init(conf, (char *)component))
    {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, server, "mod_jabapi: error initializing");
        return -1;
    }

    rpc_set_callbacks_raw(RPC_result_raw, RPC_error_raw, NULL);
    jc_set_auth_callback(JC_session_auth);

    /* start jcomponent session (starts background thread to manage rpc calls) */
    if (jc_run(true))
    {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, server, "mod_jabapi: error connecting to sm");
        return -1;
    }
        
    if (queue.create_queue(1, qkey) < 0)
    {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, server, "mod_jabapi: error creating server queue");
        return -1;
    }
    
    /* start our worker thread */
    rs = apr_thread_create(&thread, NULL, jabapi_worker, server, pconf);
    if (rs != APR_SUCCESS)
        return rs;

    ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, server, "mod_jabapi: initialized");

    return 0;
}

/*
 * Initialize child process.
 */
extern "C" int jabapi_child_init(server_rec* server, apr_pool_t *p, modjab_cfg *cfg)
{
    char * component = cfg->jabber_component;
    char qkey = (strcmp(component, "extapi") == 0) ? 'Q' : 'P';

    g_server = server;
    
    if (queue.open_queue(getpid(), qkey) < 0)
    {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, server, "mod_jabapi: error opening client queue");
        return -1;
    }
    
    return 0;
}

/*
 * Send request to jabber.
 */
extern "C" int jabapi_send_request(jabapi_request_response* req, char* data, int jabber_timeout)
{
    int from;
    int total_size;
    int pos = 0;
    int command;
    
    /* send request message */
    if (queue.send_message(1, data, strlen(data) + 1, MSG_CMD_NORMAL) < 0)
    {
        ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, req->r->server, "Unable to post request to XML router");
        return -1;
    }

    ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, req->r->server, "mod_jabapi: jabapi_send_request send_message %d", from);

    /* wait for result message */
    if (queue.get_message(from, total_size, command) < 0)
    {
        ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, req->r->server, "Unable to get result from XML router");
        return -1;
    }

    ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, req->r->server, "mod_jabapi: jabapi_send_request get_message %d %d", from, command);
    
    req->result = (char *) apr_pcalloc(req->r->pool, total_size);
    if (command == MSG_CMD_NORMAL)
        req->is_error = FALSE;
    else
        req->is_error = TRUE;

    /* copy data from message */    
    while (pos < total_size)
    {
        int copied;
        
        if ((copied = queue.get_message_data(req->result + pos, total_size - pos)) < 0)
        {
            ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, req->r->server, "Error while transfering data from XML router");
            return -1;
        }
        pos += copied;
    }

    return 0;
}

