#include <stdio.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <errno.h>

#include "util/istring.h"
#include "queue.h"

/*
    Interprocess strategy:

    Parent process creates a message queue, and a worker thread to wait for incoming messages.
    Each message is tagged with from and to ids--1 is the parent process, other is the pid of child.
    Child process sends requests to parent process and blocks waiting for result message.
    Parent receives message, handles, and sends result back to child.
    Messages are broken into chunks so message size in queue is not too large.
    
    Issues:
    - Ordering is not ensured by current protocol--if multiple children simultaneously send
    messages large enough to be chunked the parent may process them out of order, resulting
    in corrupted messages.  (Chunked messages from parent to child should be safe.)
    One fix would be to use multiple ids for the parent, one per child.
    - If child goes away, messages could be left in queue.  Only queue cleanup currently is at startup.
    - Default limits for message queue size must be incremented on startup.   
 */

 
extern void _debug(char *str, ...);

 
MsgQueue::MsgQueue(const char * name) : queue_name(name)
{
     queue_id = -1;
}

int MsgQueue::create_queue(int id, char qkey)
{
    key_t key;
    struct msqid_ds queue_ds;
                
    _debug("creating request queue");

    client_id = id;
    
    /* Create unique key via call to ftok() */
    key = ftok(queue_name, qkey);

    /* Open the queue - create if necessary */
    if((queue_id = msgget(key, 0666)) >= 0) 
    {
        _debug("queue already exists, removing previous queue first");
        msgctl(queue_id, IPC_STAT, &queue_ds);
        msgctl(queue_id, IPC_RMID, &queue_ds);
    }
    
    /* Open the queue - create if necessary */
    if((queue_id = msgget(key, IPC_CREAT|IPC_EXCL|0666)) == -1) 
    {
        _debug("unable to create request queue");
        return -1;
    }
                
    msgctl(queue_id, IPC_STAT, &queue_ds);
    queue_ds.msg_qbytes = MAX_QUEUE_BYTES;
    if (msgctl(queue_id, IPC_SET, &queue_ds) == -1)
    {
        _debug("error resetting max queue size, web service may not work correctly");
    }
    
    _debug("queue opened");
    return 0;
}

int MsgQueue::open_queue(int id, char qkey)
{        
    key_t key;
                
    _debug("openning request queue");

    client_id = id;
    
    /* Create unique key via call to ftok() */
    key = ftok(queue_name, qkey);

    /* Open the queue - create if necessary */
    if((queue_id = msgget(key, IPC_CREAT|0666)) == -1) 
    {
        _debug("unable to open request queue");
        return -1;
    }
                
    _debug("queue opened");
    return 0;
}

int MsgQueue::send_message(int to, const char * msg_data, int msg_size, int command)
{
    int pos = 0;
    int to_write;

    /* Queue messages until all data is sent */    
    while (pos < msg_size)
    {
        to_write = msg_size - pos;
        if (to_write > MAX_MSG_SIZE) 
            to_write = MAX_MSG_SIZE;
        
        send_buffer.to = to;
        send_buffer.msg_size = to_write;
        send_buffer.total_size = msg_size;
        send_buffer.from = client_id;
        send_buffer.command = command;
        memcpy(send_buffer.data, msg_data + pos, to_write);
        
        if((msgsnd(queue_id, (struct msgbuf *)&send_buffer,
                to_write + MSG_HEADER_SIZE, 0)) ==-1)
            return -1;
        
        pos = pos + to_write;
    }
    
    return 0;
}

int MsgQueue::get_message(int & from, int & total_size, int & command)
{
    _debug("waiting for message");

    if ((msgrcv(queue_id, (struct msgbuf *)&recv_buffer, sizeof(recv_buffer), client_id, 0)) == -1)
    {
		_debug("msgrcv error %d", errno);
        return -1;
	}
    
    from = recv_buffer.from;
    total_size = recv_buffer.total_size;
    command = recv_buffer.command;
    
    _debug("got message from %d (%d)", from, total_size);
    
    return 0;
}

/* Expected usage is to read entire message in one operation.  If target buffer is not large
   enough, extra data from this message is discared. */
int MsgQueue::get_message_data(char *msg_data, int msg_size)
{
    int to_write;
    int pos = 0;       // position in complete message "stream"
    int copied = 0;    // data actually copied in this call
    
    while (true) 
    {
        /* copy data out of message */    
        to_write = msg_size - copied;
        if (to_write > recv_buffer.msg_size) to_write = recv_buffer.msg_size;
        
        if (to_write > 0)
        {
            memcpy(msg_data + copied, recv_buffer.data, to_write);
            copied += to_write;
        }
        pos += recv_buffer.msg_size;
        
        /* read next message segment if needed */
        if (pos < recv_buffer.total_size)
        {
            _debug("reading message");
            if ((msgrcv(queue_id, (struct msgbuf *)&recv_buffer, sizeof(recv_buffer), client_id, 0)) == -1)
                return -1;
        }
        else   
            break;
    }
        
    return copied;
}

