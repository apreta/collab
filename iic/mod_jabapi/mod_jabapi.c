/* 
 * Jabber API calling module - accepts XML-RPC calls, wraps them in jabber
 * packet, forwards and gives back response.
 */
#include <stdio.h>
#include <time.h>

#include "httpd.h"
#include "http_config.h"
#include "http_core.h"
#include "http_log.h"
#include "http_main.h"
#include "http_protocol.h"
#include "http_request.h"
#include "util_script.h"
#include "http_connection.h"

#include "apr_lib.h"            /* for apr_isspace */
#include "apr_strings.h"
 
#include <glib.h>

#include "jabapi.h"

#define JABBER_REQUEST_TIMEOUT 5000

/*
 * Declare ourselves so the configuration routines can find and know us.
 * We'll fill it in at the end of the module.
 */
module AP_MODULE_DECLARE_DATA jabapi_module;

// helpers
/*
 * Locate our directory configuration record for the current request.
 */
static modjab_cfg *our_dconfig(const request_rec *r)
{
    return (modjab_cfg *) ap_get_module_config(r->per_dir_config, &jabapi_module);
}

/*
 * Locate our server configuration record for the specified server.
 */
static modjab_cfg *our_sconfig(const server_rec *s)
{
    return (modjab_cfg *) ap_get_module_config(s->module_config, &jabapi_module);
}
#if 0
/*
 * Likewise for our configuration record for the specified request.
 */
static modjab_cfg *our_rconfig(const request_rec *r)
{
    return (modjab_cfg *) ap_get_module_config(r->request_config, &jabapi_module);
}

/*
 * Likewise for our configuration record for a connection.
 */
static modjab_cfg *our_cconfig(const conn_rec *c)
{
    return (modjab_cfg *) ap_get_module_config(c->conn_config, &jabapi_module);
}
#endif

/*
 * Connect to jabber sm
 */
static apr_status_t start_jabber_service(server_rec *s, apr_pool_t *pconf)
{
    modjab_cfg *scfg = our_sconfig(s);

    if (!scfg->is_jabber_connected)
    {
        /* connect to jabber sm */                        
        if (jabapi_initialize(s, pconf, scfg) != 0)
        {
            return APR_EGENERAL;
        }
            
        scfg->is_jabber_connected = TRUE;
    }
    
    return APR_SUCCESS;
}

/**
 * Loads jabber config file name -- stored in directory context.
 */
static const char *cmd_set_config_file(cmd_parms *cmd, void *mconfig, const char *arg)
{
    modjab_cfg *cfg = our_sconfig(cmd->server);

    strcpy(cfg->jabber_config_file, arg);

    return NULL;
}

/**
 * Loads jabber config component name -- stored in directory context.
 */
static const char *cmd_set_component_name(cmd_parms *cmd, void *mconfig, const char *arg)
{
    modjab_cfg *cfg = our_sconfig(cmd->server);

    strcpy(cfg->jabber_component, arg);

    return NULL;
}

/**
 * Sets optional timeout value for jabber requests.  Stored in directory context.
 * If not set, value defaults to JABBER_REQUEST_TIMEOUT.
 */
static const char *cmd_set_jabber_timeout(cmd_parms *cmd, void *mconfig, const char *arg)
{
    modjab_cfg *cfg = our_sconfig(cmd->server);

    if (arg != NULL)
        cfg->jabber_timeout = atoi(arg);

    return NULL;
}

/**
 * Loads jabber host name -- stored in directory context.
 */
static const char *cmd_set_jabber_host(cmd_parms *cmd, void *mconfig, const char *arg)
{
    modjab_cfg *cfg = our_sconfig(cmd->server);

    strcpy(cfg->jabber_host_name, arg);

    return NULL;
}

/**
 * Enables debug mode for the module, which prints a trace of server activity.  If
 * not set default is FALSE.
 */
static const char *cmd_enable_debug(cmd_parms *cmd, void *mconfig)
{
    modjab_cfg *cfg = our_sconfig(cmd->server);

    cfg->enable_debug = TRUE;

    return NULL;
}

static const char *cmd_set_mtg_record_prefix(cmd_parms *cmd, void *mconfig, const char *arg)
{
    modjab_cfg *cfg = our_sconfig(cmd->server);

    strcpy(cfg->mtg_record_uri_prefix, arg);

    return NULL;
}

static const char *cmd_set_mtg_record_suffix(cmd_parms *cmd, void *mconfig, const char *arg)
{
    modjab_cfg *cfg = our_sconfig(cmd->server);

    strcpy(cfg->mtg_record_uri_suffix, arg);

    return NULL;
}

static const char *cmd_set_doc_share_prefix(cmd_parms *cmd, void *mconfig, const char *arg)
{
    modjab_cfg *cfg = our_sconfig(cmd->server);

    strcpy(cfg->doc_share_uri_prefix, arg);

    return NULL;
}

static const char *cmd_set_doc_share_suffix(cmd_parms *cmd, void *mconfig, const char *arg)
{
    modjab_cfg *cfg = our_sconfig(cmd->server);

    strcpy(cfg->doc_share_uri_suffix, arg);

    return NULL;
}

// reads POST request body
int read_request(request_rec *r, char **rbuf)
{
    int rc;
    if ((rc = ap_setup_client_block(r, REQUEST_CHUNKED_ERROR)) != OK) 
    {
        return rc;
    }
    if (ap_should_client_block(r)) 
    {
        char argsbuffer[HUGE_STRING_LEN];
        int rsize, len_read, rpos=0;
        int length = (int) r->remaining;
        *rbuf = apr_pcalloc(r->pool, length + 1);
        while ((len_read =
                ap_get_client_block(r, argsbuffer, sizeof(argsbuffer))) > 0) 
        {
            if ((rpos + len_read) > length) 
            {
                rsize = length - rpos;
            }
            else 
            {
                rsize = len_read;
            }
            memcpy((char*)*rbuf + rpos, argsbuffer, rsize);
            rpos += rsize;
        }
    }
    return rc;
}

// quick and dirty removal of '<','>' for debug purposes
void escape_xml(const char* data)
{
    int ch = '<';
    char* pos;
    pos = strchr(data, ch);
    while (pos != NULL)
    {
        *pos = '_';
        pos = strchr(pos, ch);
    }

    // again for close brackets
    ch = '>';
    pos = strchr(data, ch);
    while (pos != NULL)
    {
        *pos = '_';
        pos = strchr(pos, ch);
    }
}

// writes out the passed data in an XML-RPC response
void echo_request(request_rec *r, char* data)
{
    char* data_esc;

    data_esc = (char *) apr_pcalloc(r->pool, strlen(data));
    strncpy(data_esc, data, strlen(data));
    escape_xml(data_esc);

    ap_rputs("<methodResponse>", r);
    ap_rputs("  <params>", r);
    ap_rputs("  <param><value><array><data>", r);
    ap_rputs("      <value><string>Got the following request</string></value>", r);
    ap_rputs("      <value><string>", r);
    ap_rputs(data_esc, r);
    ap_rputs("      </string></value>", r);
    ap_rputs("  </data></array></value></param>", r);
    ap_rputs("  </params>", r);
    ap_rputs("</methodResponse>", r);
}

void output_ping_page(request_rec *r)
{
    ap_rputs("<html><body><p>API server running</p></body></html>", r);
}

// writes out the passed data
void write_request_result(request_rec *r, const char* data)
{
    ap_rputs(data, r);
}

// writes the passed error in an XML-RPC fault response
void write_request_fault(request_rec *r, char* error)
{
    ap_rputs("<?xml version=\"1.0\"?>", r);
    ap_rputs("<methodResponse>", r);
    ap_rputs("   <fault>", r);
    ap_rputs("      <value>", r);
    ap_rputs("         <struct>", r);
    ap_rputs("            <member>", r);
    ap_rputs("               <name>faultCode</name>", r);
    ap_rputs("               <value><int>4</int></value>", r);
    ap_rputs("               </member>", r);
    ap_rputs("            <member>", r);
    ap_rputs("               <name>faultString</name>", r);
    ap_rputs("               <value><string>", r);
    ap_rputs(error, r);
    ap_rputs("</string></value>", r);
    ap_rputs("            </member>", r);
    ap_rputs("         </struct>", r);
    ap_rputs("      </value> ", r);
    ap_rputs("   </fault>", r);
    ap_rputs("</methodResponse>", r);
}

/**
 * XML-RPC request handler.
 */
static int jabapi_handler(request_rec *r)
{
    int rc = OK;
    int ret;
    char* data;
    jabapi_request_response* req_response = NULL;
    modjab_cfg *dcfg; // dir config
    modjab_cfg *scfg; // server config

    if (strcmp(r->handler, "jabapi-handler")) {
        return DECLINED;
    }

    dcfg = our_dconfig(r);
    scfg = our_sconfig(r->server);

    /*
     * If we're only supposed to send header information (HEAD request), we're
     * already there.
     */
    if (r->header_only) {
        return OK;
    }

    if (r->method_number == M_POST)
    {
        ap_set_content_type(r, "text/xml");
        
        if ((rc = read_request(r, &data)) != OK)
            return rc;  // unable to process post body

        // if we find debug in the URI, we spit back the request
        if (strstr(r->uri, "debug") != NULL) // special debug URI
        {
            // echo back request to client
            echo_request(r, data);
        }
        else
        {
            if (scfg->is_jabber_connected == TRUE)
            {
                req_response = (jabapi_request_response *) apr_pcalloc(r->pool, sizeof(jabapi_request_response));
                req_response->r = r;

                // data should now have body of XML request
                // call our jabber api to process
                ret = jabapi_send_request(req_response, data, scfg->jabber_timeout);
                switch (ret)
                {
                    case 0:
                        if (!req_response->is_error)
                            write_request_result(r, req_response->result);
                        else
                            write_request_fault(r, req_response->result);
                        break;
                    case -1:
                        ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server, "Error sending request to XML router");
                        return HTTP_INTERNAL_SERVER_ERROR;
                    case -2:
                        return HTTP_SERVICE_UNAVAILABLE;
                }
            }
            else
            {
                write_request_fault(r, "Failure connecting to XML router");
            }
        }
    }
    else if (r->method_number == M_GET &&
                (strstr(r->uri, "ping") != NULL))  { // special ping URI
        // Someone pinging from (presumably) a browser, send some
        // html back
        output_ping_page(r);
    }
    else
    {
        write_request_fault(r, "HTTP method not allowed; use POST");
    }

    /*
     * Always return OK so that Apache server doesn't squeeze out
     * some HTML to some unsuspecting XML interpreter.  Faults are reported
     * in an XML doc above
     */
    return OK;
}

static void note_basic_auth_failure(request_rec *r)
{
    apr_table_setn(r->err_headers_out,
                   (PROXYREQ_PROXY == r->proxyreq) ? "Proxy-Authenticate"
                                                   : "WWW-Authenticate",
                   apr_pstrcat(r->pool, "Basic realm=\"", ap_auth_name(r),
                               "\"", NULL));
}

#define NUM_ENC 9
static 
const char * enctable[NUM_ENC][2] = {
//    { "\\", "\\5c" },     // passed in string may already be encoded so don't re-encode slash
    { " ",  "\\20" },
    { "\"", "\\22" },
    { "&",  "\\26" },
    { "'",  "\\27" },
    { "/",  "\\2f" },
    { ":",  "\\3a" },
    { "<",  "\\3c" },
    { ">",  "\\3e" },
    { "@",  "\\40" },
};

static int get_number_encoded(char *str) {
    int num = 0;
    char *p = str;
    int i;
    
    while (*p) {
        for (i=0; i<NUM_ENC; i++) {
            if (*p == *enctable[i][0]) {
                ++num;
                break;
            }
        }
        ++p;
    }
    return num;
}

static char *encode_string(request_rec *r, char *in) {
    int extra = get_number_encoded(in);
    if (!extra)
        return in;

    // Need 2 extra bytes per encoded character
    char *out = apr_palloc(r->pool, strlen(in) + extra*2 + 1);
    char *p = out;
    int i;
    
    // Copy string, replacing characters that need encoding
    while (*in) {
        for (i=0; i<NUM_ENC; i++) {
            *p = *in;
            if (*in == *enctable[i][0]) {
                strcpy(p, enctable[i][1]);
                p += 2;
                break;
            }
        }
        ++p;
        ++in;
    }
    *p = '\0';
    
    return out;
}

static int get_basic_auth(request_rec *r, char **user,
                          char **pw)
{
    const char *auth_line;
    char *decoded_line;
    int length;

    /* Get the appropriate header */
    auth_line = apr_table_get(r->headers_in, (PROXYREQ_PROXY == r->proxyreq)
                                              ? "Proxy-Authorization"
                                              : "Authorization");

    if (!auth_line) {
        note_basic_auth_failure(r);
        return HTTP_UNAUTHORIZED;
    }

    if (strcasecmp(ap_getword(r->pool, &auth_line, ' '), "Basic")) {
        /* Client tried to authenticate using wrong auth scheme */
        ap_log_rerror(APLOG_MARK, APLOG_ERR, 0, r,
                      "client used wrong authentication scheme: %s", r->uri);
        note_basic_auth_failure(r);
        return HTTP_UNAUTHORIZED;
    }

    /* Skip leading spaces. */
    while (apr_isspace(*auth_line)) {
        auth_line++;
    }

    decoded_line = apr_palloc(r->pool, apr_base64_decode_len(auth_line) + 1);
    length = apr_base64_decode(decoded_line, auth_line);
    /* Null-terminate the string. */
    decoded_line[length] = '\0';

    //ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, r->server,  "mod_jabapi: %s", decoded_line);
	
    *user = ap_getword_nulls(r->pool, (const char**)&decoded_line, ':');
    *pw = decoded_line;

    /* unless for anonymous user, encode special jabber screen name 
       characters (e.g. @) */
    if (strncmp(*user, "anonymous", 9) != 0)
        *user = encode_string(r, *user);

    /* set the user, even though the user is unauthenticated at this point */
    r->user = (char *) *user;

    return OK;
}

/**
 * Authentication using Jabber as provider
 */
int modjab_authenticate_basic_user(request_rec * r)
{
    modjab_cfg *dcfg; // dir config
    modjab_cfg *scfg; // server config
    jabapi_request_response* req_response = NULL;
    char *val = NULL;
    char *sent_user,*sent_pw;
    int ret;
    char *user, *cmd;

    dcfg = our_dconfig(r);
    scfg = our_sconfig(r->server);

    ret = get_basic_auth(r, &sent_user, &sent_pw);
    if (ret) {
        return ret;
    }

    // Build auth lookup request for transmission in queue
    cmd = apr_palloc(r->pool, strlen(sent_user) + strlen(scfg->jabber_host_name) + 
                    strlen(sent_pw) + 12);
    sprintf(cmd, "AUTH:%s@%s##%s", sent_user, scfg->jabber_host_name, sent_pw);

    //ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, r->server, "Request: %s", cmd);

    if (scfg->is_jabber_connected == TRUE)
    {
        req_response = (jabapi_request_response *) apr_pcalloc(r->pool, sizeof(jabapi_request_response));
        req_response->r = r;

        ret = jabapi_send_request(req_response, cmd, scfg->jabber_timeout);
        switch (ret)
        {
            case 0:
                if (!req_response->is_error)
                    return OK;
                ap_note_basic_auth_failure(r);
                return HTTP_UNAUTHORIZED;
            case -1:
                ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server, "Error sending auth request to XML router");
                return HTTP_INTERNAL_SERVER_ERROR;
            case -2:
                return HTTP_SERVICE_UNAVAILABLE;
        }

        return HTTP_INTERNAL_SERVER_ERROR;
    }

    return DECLINED;
}


/*
 * This function gets called to create a per-directory configuration
 * record.  This will be called for the "default" server environment, and for
 * each directory for which the parser finds any of our directives applicable.
 * If a directory doesn't have any of our directives involved (i.e., they
 * aren't in the .htaccess file, or a <Location>, <Directory>, or related
 * block), this routine will *not* be called - the configuration for the
 * closest ancestor is used.
 *
 * The return value is a pointer to the created module-specific
 * structure.
 */
static void *modjab_create_dir_config(apr_pool_t *p, char *dirspec)
{
    modjab_cfg *cfg;
    char *dname = dirspec;

    /*
     * Allocate the space for our record from the pool supplied.
     */
    cfg = (modjab_cfg *) apr_pcalloc(p, sizeof(modjab_cfg));
 
    /*
     * Now fill in the defaults.  If there are any `parent' configuration
     * records, they'll get merged as part of a separate callback.
     */
    cfg->cmode = CONFIG_MODE_DIRECTORY;

    /*
     * Finally, add our trace to the callback list.
     */
    dname = (dname != NULL) ? dname : (char*) "";
    cfg->loc = apr_pstrcat(p, "DIR(", dname, ")", NULL);
    return (void *) cfg;
}

/*
 * This function gets called to merge two per-directory configuration
 * records.  This is typically done to cope with things like .htaccess files
 * or <Location> directives for directories that are beneath one for which a
 * configuration record was already created.  The routine has the
 * responsibility of creating a new record and merging the contents of the
 * other two into it appropriately.  If the module doesn't declare a merge
 * routine, the record for the closest ancestor location (that has one) is
 * used exclusively.
 *
 * The routine MUST NOT modify any of its arguments!
 *
 * The return value is a pointer to the created module-specific structure
 * containing the merged values.
 */
static void *modjab_merge_dir_config(apr_pool_t *p, 
                                  void *parent_conf,
                                  void *newloc_conf)
{

    modjab_cfg *merged_config = (modjab_cfg *) apr_pcalloc(p, sizeof(modjab_cfg));
    modjab_cfg *pconf = (modjab_cfg *) parent_conf;
    modjab_cfg *nconf = (modjab_cfg *) newloc_conf;
    char *note;

    /*
     * Some things get copied directly from the more-specific record, rather
     * than getting merged.
     */
    merged_config->loc = apr_pstrdup(p, nconf->loc);
    /*
     * If we're merging records for two different types of environment (server
     * and directory), mark the new record appropriately.  Otherwise, inherit
     * the current value.
     */
    merged_config->cmode =
        (pconf->cmode == nconf->cmode) ? pconf->cmode : CONFIG_MODE_COMBO;

    return (void *) merged_config;
}

/*
 * This function gets called to create a per-server configuration
 * record.  It will always be called for the "default" server.
 *
 * The return value is a pointer to the created module-specific
 * structure.
 */
static void *modjab_create_server_config(apr_pool_t *p, server_rec *s)
{

    modjab_cfg *cfg;
    char *sname = s->server_hostname;

    /*
     * As with the modjab_create_dir_config() reoutine, we allocate and fill
     * in an empty record.
     */
    cfg = (modjab_cfg *) apr_pcalloc(p, sizeof(modjab_cfg));
    cfg->cmode = CONFIG_MODE_SERVER;

    cfg->jabber_config_file = (char*) apr_pcalloc(p, 256);
    cfg->jabber_component = (char*) apr_pcalloc(p, 256);
    cfg->jabber_host_name = (char*) apr_pcalloc(p, 256);
    cfg->mtg_record_uri_prefix = (char*) apr_pcalloc(p, 256);
    cfg->mtg_record_uri_suffix = (char*) apr_pcalloc(p, 256);
    cfg->doc_share_uri_prefix = (char*) apr_pcalloc(p, 256);
    cfg->doc_share_uri_suffix = (char*) apr_pcalloc(p, 256);
    cfg->is_jabber_connected = FALSE;
    cfg->jabber_timeout = JABBER_REQUEST_TIMEOUT; // default value
    cfg->enable_debug = FALSE;

    return (void *) cfg;
}

/*
 * This function gets called to merge two per-server configuration
 * records.  This is typically done to cope with things like virtual hosts and
 * the default server configuration  The routine has the responsibility of
 * creating a new record and merging the contents of the other two into it
 * appropriately.  If the module doesn't declare a merge routine, the more
 * specific existing record is used exclusively.
 *
 * The routine MUST NOT modify any of its arguments!
 *
 * The return value is a pointer to the created module-specific structure
 * containing the merged values.
 */
static void *modjab_merge_server_config(apr_pool_t *p, void *server1_conf,
                                     void *server2_conf)
{

    modjab_cfg *merged_config = (modjab_cfg *) apr_pcalloc(p, sizeof(modjab_cfg));
    modjab_cfg *s1conf = (modjab_cfg *) server1_conf;
    modjab_cfg *s2conf = (modjab_cfg *) server2_conf;
    char *note;

    /*
     * Our inheritance rules are our own, and part of our module's semantics.
     * Basically, just note whence we came.
     */
    merged_config->cmode =
        (s1conf->cmode == s2conf->cmode) ? s1conf->cmode : CONFIG_MODE_COMBO;
    merged_config->loc = apr_pstrdup(p, s2conf->loc);

    merged_config->jabber_config_file = apr_pstrdup(p, s2conf->jabber_config_file);
    merged_config->jabber_component = apr_pstrdup(p, s2conf->jabber_component);
    merged_config->jabber_host_name = apr_pstrdup(p, s2conf->jabber_host_name);
    merged_config->mtg_record_uri_prefix = apr_pstrdup(p, s2conf->mtg_record_uri_prefix);
    merged_config->mtg_record_uri_suffix = apr_pstrdup(p, s2conf->mtg_record_uri_suffix);

    return (void *) merged_config;
}

/*
 * All our process-death routine does is add its trace to the log.
 */
static apr_status_t modjab_child_exit(void *data)
{
    char *note;
    server_rec *s = (server_rec*) data;
    char *sname = s->server_hostname;

    return APR_SUCCESS;
}

/*
 * Called by the parent process during server initialization.
 */
static int modjab_post_config(apr_pool_t *pconf, apr_pool_t *plog, 
                             apr_pool_t *ptemp, server_rec *s)
{
    void *data; /* These two help ensure that we only init once. */
    const char *userdata_key = "jabapi_init_module";
    apr_status_t rs;
        
    /* 
     * The following checks if this routine has been called before. 
     * This is necessary because the parent process gets initialized
     * a couple of times as the server starts up, and we don't want 
     * to create any more mutexes and shared memory segments than
     * we're actually going to use. 
     */ 
    apr_pool_userdata_get(&data, userdata_key, s->process->pool);
    if (!data) {
        apr_pool_userdata_set((const void *) 1, userdata_key, 
                              apr_pool_cleanup_null, s->process->pool);
        return OK;
    }
    
    /* connect to jabber server */
    rs = start_jabber_service(s, pconf);
    if (rs != APR_SUCCESS) {
        ap_log_error(APLOG_MARK, APLOG_CRIT, rs, s, "Unable to connect to xmlrouter");
        return HTTP_INTERNAL_SERVER_ERROR;
    }

    return OK;
}

/*
 * Called whenever the server spawns off a new child process.  Each child will
 * open it's own jabber connection.
 */
static void modjab_child_init(apr_pool_t *p, server_rec *s)
{
    char *note;
    char *sname = s->server_hostname;
    modjab_cfg *scfg = our_sconfig(s);

    /* set up shared memory/mutex for this process */
    jabapi_child_init(s, p, scfg);
    
    apr_pool_cleanup_register(p, s, modjab_child_exit, modjab_child_exit);
}


#define MAX_MEETING_ID_LENGTH 16

/**
 * Check user access to resource--either recording or document share directory.
 */
static int modjab_auth_checker(request_rec *r)
{
    modjab_cfg *dcfg; // dir config
    modjab_cfg *scfg; // server config
    jabapi_request_response* req_response = NULL;
    int ret;
    char meetingid[MAX_MEETING_ID_LENGTH];
    char *cmd;

    dcfg = our_dconfig(r);
    scfg = our_sconfig(r->server);

    if (r != NULL)
    {
        if (r->user != NULL && r->uri != NULL)
        {
            ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, r->server, 
                         "mod_jabapi:  authorize %s %s", r->user, r->uri);

            char *mtg_uri = strstr(r->uri, scfg->mtg_record_uri_prefix);
            if (mtg_uri != NULL) /* Trying to access a meeting recording */
            {
                // admin always granted access
                if (strcmp(r->user, "admin") == 0)
                    return OK;

                mtg_uri += strlen(scfg->mtg_record_uri_prefix);
                int mid_len = strstr(mtg_uri, scfg->mtg_record_uri_suffix) - mtg_uri;
                if (mid_len < MAX_MEETING_ID_LENGTH && mid_len > 0)
                {
                    memcpy(meetingid, mtg_uri, mid_len );
                    memset(meetingid + mid_len, 0, 1);

                    // Build authorization check request for transmission in queue
                    cmd = apr_palloc(r->pool, strlen(r->user) + strlen(scfg->jabber_host_name) + 
                                     strlen(meetingid) + 16);
                    sprintf(cmd, "REC_AUTHZ:%s@%s##%s", r->user, scfg->jabber_host_name, meetingid);
                }
                else
                {
                    ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server,
                                 "mod_jabapi:  auth_checker error extracting meetingid from mtgarchive uri '%s'", r->uri);
                    return HTTP_INTERNAL_SERVER_ERROR;
                }

            }
            else
            {
                char *doc_uri = strstr(r->uri, scfg->doc_share_uri_prefix);
                if (doc_uri != NULL)
                {
                    // admin always granted access
                    if (strcmp(r->user, "admin") == 0)
                        return OK;
    
                    doc_uri += strlen(scfg->doc_share_uri_prefix);
    
                    // ping file used to verify server is running, always allow access
                    if (strncmp(doc_uri, "ping.html", 9) == 0)
                        return OK;
					
                    int mid_len = strstr(doc_uri, scfg->doc_share_uri_suffix) - doc_uri;
                    if (mid_len < MAX_MEETING_ID_LENGTH && mid_len > 0)
                    {
                        memcpy(meetingid, doc_uri, mid_len );
                        memset(meetingid + mid_len, 0, 1);
                    }
                    else
                    {
                        ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server,
                                     "mod_jabapi:  auth_checker error extracting meetingid from mtgarchive uri '%s'", r->uri);
                        return HTTP_INTERNAL_SERVER_ERROR;
                    }

                    // Allow temporary upload directory for each user
                    // ToDo: move name to configuration
                    if (strcmp(meetingid, "TempUpload") == 0)
                    {
                        // Pull username component from path (after "TempUpload")
                        // and verify it matches current user
                        const char *path = doc_uri + mid_len + 1;
                        const char *path_end = strchr(path, '/');
                        if (path_end != NULL)
                        {
                            int path_len = path_end - path;
                            if (strncmp(path, r->user, path_len) == 0)
                                return OK;
                        }
    
                        return HTTP_UNAUTHORIZED;
                    }

                    // If host flag is set, compare against host stored in database 
                    // as we would for recordings.
                    const char *is_host = apr_table_get(r->headers_in, "X-iic-host");

                    // Build authorization check request for transmission in queue
                    cmd = apr_palloc(r->pool, strlen(r->user) + strlen(scfg->jabber_host_name) + 
                                     strlen(meetingid) + 16);
                    if (is_host != NULL && *is_host)
                        sprintf(cmd, "REC_AUTHZ:%s@%s##%s", r->user, scfg->jabber_host_name, meetingid);
                    else if (strncmp(r->user, "anonymous@", 10) == 0)
                        sprintf(cmd, "DOC_AUTHZ:%s##%s", r->user, meetingid);
                    else
                        sprintf(cmd, "DOC_AUTHZ:%s@%s##%s", r->user, scfg->jabber_host_name, meetingid);
                    
                }
                else if (strncmp(r->uri, "/repository/user/", 17) == 0)
                {
					char *username = r->uri + 17;
					if (strncmp(username, r->user, strlen(r->user)) == 0 && username[strlen(r->user)] == '/')
					{
						return OK;
					}
					else
	                    return HTTP_UNAUTHORIZED;				
				}
				else
                    return HTTP_UNAUTHORIZED;
            }
        }
        else
        {
            ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server,
                         "mod_jabapi:  missing needed authorization information");
            return HTTP_INTERNAL_SERVER_ERROR;
        }
    }
    else
    {
        ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server,
                     "mod_jabapi:  auth_checker called with NULL request_rec");
        return HTTP_INTERNAL_SERVER_ERROR;
    }


    if (scfg->is_jabber_connected == TRUE)
    {
        req_response = (jabapi_request_response *) apr_pcalloc(r->pool, sizeof(jabapi_request_response));
        req_response->r = r;

        ret = jabapi_send_request(req_response, cmd, scfg->jabber_timeout);
        switch (ret)
        {
            case 0:
                if (!req_response->is_error)
                    return OK;
                ap_note_basic_auth_failure(r);
                return HTTP_UNAUTHORIZED;
            case -1:
                ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server, "Error sending auth request to XML router");
                return HTTP_INTERNAL_SERVER_ERROR;
            case -2:
                return HTTP_SERVICE_UNAVAILABLE;
        }

        return HTTP_INTERNAL_SERVER_ERROR;
    }

    return DECLINED;
}

/** 
 * Register our functions to be called.
 */
static void modjab_register_hooks(apr_pool_t *p)
{
    ap_hook_post_config(modjab_post_config, NULL, NULL, APR_HOOK_MIDDLE);
    ap_hook_child_init(modjab_child_init, NULL, NULL, APR_HOOK_MIDDLE);
    ap_hook_handler(jabapi_handler, NULL, NULL, APR_HOOK_MIDDLE);
    ap_hook_check_user_id(modjab_authenticate_basic_user, NULL, NULL,
						  APR_HOOK_MIDDLE);
    ap_hook_auth_checker(modjab_auth_checker, NULL, NULL, APR_HOOK_MIDDLE);
}

/** 
 * List of directives specific to our module.
 */
static const command_rec modjab_cmds[] =
    {
        AP_INIT_NO_ARGS("EnableApiDebug",       // directive name
                        cmd_enable_debug,       // config action routine
                        NULL,                   // arg 
                        OR_OPTIONS,             // where avail
                        "Enable debug mode"     // description
                        ),
        AP_INIT_TAKE1("SetJabberApiConfig",     // directive name
                      cmd_set_config_file,      // config action routine
                      NULL,                     // arg 
                      OR_OPTIONS,               // where avail
                      "Set Jabber Config file <file name>" // description
                      ),
        AP_INIT_TAKE1("SetJabberApiComponent",  // directive name
                      cmd_set_component_name,   // config action routine
                      NULL,                     // arg 
                      OR_OPTIONS,               // where avail
                      "Set Jabber component name" // description
                      ),
        AP_INIT_TAKE1("SetJabberApiTimeout",    // directive name
                      cmd_set_jabber_timeout,   // config action routine
                      NULL,                     // arg 
                      OR_OPTIONS,               // where avail
                      "Set Jabber Request timout (optional)" // description
                      ),
        AP_INIT_TAKE1("SetJabberHostName",     // directive name
                      cmd_set_jabber_host,      // config action routine
                      NULL,                     // arg 
                      OR_OPTIONS,               // where avail
                      "Set Jabber host name" // description
                      ),
        AP_INIT_TAKE1("MtgRecordURIPrefix",     // directive name
                      cmd_set_mtg_record_prefix,      // config action routine
                      NULL,                     // arg 
                      OR_OPTIONS,               // where avail
                      "Set Meeting URI Prefix" // description
                      ),
        AP_INIT_TAKE1("MtgRecordURISuffix",     // directive name
                      cmd_set_mtg_record_suffix,      // config action routine
                      NULL,                     // arg 
                      OR_OPTIONS,               // where avail
                      "Set Meeting URI Suffix" // description
                      ),
        AP_INIT_TAKE1("DocShareURIPrefix",     // directive name
                      cmd_set_doc_share_prefix,      // config action routine
                      NULL,                     // arg 
                      OR_OPTIONS,               // where avail
                      "Set Whiteboard URI Prefix" // description
                      ),
        AP_INIT_TAKE1("DocShareURISuffix",     // directive name
                      cmd_set_doc_share_suffix,      // config action routine
                      NULL,                     // arg 
                      OR_OPTIONS,               // where avail
                      "Set Whiteboard URI Suffix" // description
                      ),
        {NULL}
    };

/*
 * Module definition for configuration.  If a particular callback is not
 * needed, replace its routine name below with the word NULL.
 */
module AP_MODULE_DECLARE_DATA jabapi_module =
    {
        STANDARD20_MODULE_STUFF,
        modjab_create_dir_config,    /* per-directory config creator */
        modjab_merge_dir_config,     /* dir config merger */
        modjab_create_server_config, /* server config creator */
        modjab_merge_server_config,  /* server config merger */
        modjab_cmds,                 /* command table */
        modjab_register_hooks,       /* set up other request processing hooks */
    };
