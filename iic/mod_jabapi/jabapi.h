/**
 * jabapi.h: C wrapper for jabber api calls.
 *
 * (c)2004 imidio all rights reserved
 *
 */

#ifndef __JAPBAPI_H__
#define __JAPBAPI_H__

/* Configuration */

#define CONFIG_MODE_SERVER 1
#define CONFIG_MODE_DIRECTORY 2
#define CONFIG_MODE_COMBO 3     /* Shouldn't ever happen. */

typedef int BOOL;

/**
 * Data structure used to maintain state between requests 
 * on the server.
 *
 * Note: right now same structure is used both for server and directory data.
 * Could easily split these into separate structures which might be a little
 * clearer and safer.  Would have to have adjust the trace_add method.
 */
typedef struct modjab_cfg {
    int cmode;                  /* Environment to which record applies
                                 * (directory, server, or combination).
                                 */
    BOOL enable_debug;          /* Enable debug for the module? */
    char *jabber_config_file;   /* Storage for the jabber config file */
    char *jabber_component;     /* Storage for the jabber component name */
    char *jabber_host_name;     /* Storage for the jabber host name */
    BOOL is_jabber_connected;   
    int  jabber_timeout;        /* Timeout value for waiting for response */
    char *trace;                /* Pointer to trace string. */
    char *loc;                  /* Location to which this record applies. */
    char *mtg_record_uri_prefix;   /* String that precedes the meeting id in 
                                   a recording URI */
    char *mtg_record_uri_suffix;  /* String that marks the end of of the meeting id
                                     in a recording URI */
    char *doc_share_uri_prefix;   /* String that precedes the meeting id in 
                                   a whiteboard URI */
    char *doc_share_uri_suffix;  /* String that marks the end of of the meeting id
                                     in a whiteboard URI */
} modjab_cfg;


/* per-request structure */

typedef struct jabapi_request_response
{
    request_rec* r;
    char*        result;
    BOOL         is_error;
} jabapi_request_response;

/* Internal APIs */

#ifdef __cplusplus
extern "C" 
{
#endif

extern int jabapi_initialize(server_rec* server,
                                apr_pool_t *pconf,
                                modjab_cfg *mconfig);

extern int jabapi_child_init(server_rec* server,
                                apr_pool_t *pconf,
                                modjab_cfg *cfg);

extern int jabapi_send_request(jabapi_request_response* req, 
                            char* data, 
                            int jabber_timeout);

#ifdef __cplusplus
}
#endif

#endif
