## current zon client version
client_version=

## Database connection parameters
db_host=localhost
db_name=iic
db_user=zon
db_pswd=""
db_ip_addr=( 10.1.1.1 )
db_netmask=( 255.255.255.0 )

## Hostname used by all zon clients to connect to XML router
external_hname="all.homedns.org"

##
## Parameters for Zon web portal URL
## e.g. http:://<portal_hname>/imidio/meetings/, etc.
##
## Meeting invitation processing, zon client downloads, document sharing
## etc. are done through the web portal.
##
portal_hname="zonportal.homedns.org"
portal_local_hname=( "test2.homedns.org" )
portal_ip=( "10.0.1.198" )

##
## Parameters for external API URL
## http//<extapi_hname>:<extapi_port>/imidio_api/
##
## The external API provides administrative operations
## to components external to Zon (e.g. web applications)
##
extapi_portal_hname="test2.homedns.org"
extapi_portal_local_hname="test2.homedns.org"
extapi_portal_ip=10.0.1.7
extapi_portal_port=8000

## jcomponent secret to authorize connection
## to jabber
service_secret="secret"

## password to authorize sessions on behalf of
## users
session_creation_secret="QAZXSW88"

## intra-cluster addrs for XML routers
## 1 or 2 xmlrouter hosts are currently supported
lcl_xmlrouter=( localhost )
lcl_xmlrouter_ip=( 10.0.1.7 )

##
## If you are using an external box like a load balancer to
## forward connections to xmlrouter/appshare fallback ports, set
## the following parameter to "no".  Otherwise, set it to "yes"
##
iptables_port_forwarding="yes"

##
## SERVICE PORT ALLOCATION
##
## As of Nov 04, ports 5230 through 5249 were unassigned by IANA except
## for HP services (probably not running on a targeted RH box!)
## and padl2sim services (of which there isn't much googlizable info)
##
## Ports 5320-5326 are in use below.  Start allocating addl ports at
## 5327
##

## Only 1 instance is currently supported
jadc2s_instances=1
## Set to any non-conflicting value (see "SERVICE PORT ALLOCATION" note above)
jadc2s_port=( 5320 )
iic_client_port=5222

## The following hostnames may be either localhost for a one-box
## solution, or the value that 'uname -n' would return on the
## host(s) assigned to run the daemon.

controller_host=( localhost )
## Set to any non-conflicting value (see "Service port allocation" note above)
controller_port=( 5321 )

voice_host=( localhost )
## Set to any non-conflicting value (see "Service port allocation" note above)
voice_port=( 5322 )
voice_phones=( "978-461-1170,978-555-1234" )

mtgarchiver_host=( localhost )
## The meeting archiver fetches wav files from the voice host
## however, the voice host may be behind a NAT router so that
## the hostname needed may be different from the voice hosts above
## Leave empty if the same as voice hosts above.
mtgarchiver_voice_host=( )
## Set to any non-conflicting value (see "Service port allocation" note above)
mtgarchiver_port=( 5323 )
mtgarchiver_prio=20

addressbk_host=( localhost )
## Set to any non-conflicting value (see "Service port allocation" note above)
addressbk_port=( 5324 )

mailer_host=( localhost )
## Set to any non-conflicting value (see "Service port allocation" note above)
mailer_port=( 5325 )

extapi_host=( localhost )
## Set to any non-conflicting value (see "Service port allocation" note above)
extapi_port=( 5326 )

## Only 1 instance is currently supported
exportal_instances=1
## Set to any non-conflicting value (see "SERVICE PORT ALLOCATION" note above)
extportal_port=( 5327 )

converter_host=( localhost )
## Set to any non-conflicting value (see "Service port allocation" note above)
converter_port=( 5328 )

share_host=( localhost )
share_local_host=( localhost )
share_ip=( - )
## IANA unassigned as of Nov-04
share_port=2182

## voice services provider.
## 'stub', 'nms' and 'ext' are currently supported values
voice_provider=stub

## Port the meeting archiver uses to
## fetch meeting recordings from a voice bridge.
#
## Unassigned Nov-04 IANA
wav_port=5311

## When a new user with an e-mail address is added to the system,
## the system will generate an e-mail message to them w/ their initial password
## and other download/logon instructions.  The following parameters
## control the content of that message.
##
## NOTE:
##	The e-mail body template is created either from a default
##      template supplied in the install package or by the system
##	admin in the file new-user-template.default in the staging directory.
new_user_from_address="newuser@sitescape.com"
new_user_from_display="New User"
new_user_subject="Welcome to SiteScape Zon"

## If your enterprise has a specific e-mail provider, the following
## values allow you to use that provider.  Use "localhost" if all
## machines in the cluster have SMTP servers configured.
smtp_host="localhost"
smtp_user=""
smtp_pswd=""

## When a system component needs to send e-mail, but doesn't
## specify the from address or display name, the following values
## will be used.
default_from_address="mailer@sitescape.com"
default_from_display="Mailer"

## When a user is invited to attend a meeting, the system will generate
## an invitation e-mail and/or an invitation instant message.  The following
## parameters control the content of that message.
##
## NOTE:
##	The e-mail/IM-body template is created either from a default
##      template supplied in the install package or by the system
##	admin in the file invitation-template.default in the staging directory.
invite_from_address="inviter@sitescape.com"
invite_from_display="Meeting Inviter"
invite_subject="Zon Meeting Invitation"

## When the meeting archiver produces a new meeting archive, the
## system will generate an invitation e-mail informing the meeting host
## of the fact.  The following parameters control the content of that
## message.
##
## NOTE:
##	The new-meeting-archive-body template is created either from a default
##      template supplied in the install package or by the system
##	admin in the file new-archive-template.default in the staging directory.
mtgarchiver_from_address="mtgarchiver@sitescape.com"
mtgarchiver_from_display="Meeting Archiver"
mtgarchiver_subject="New meeting archive is available"

## Various components of the system generate billable and other events
## that are logged to files in /var/iic/cdr on the machine that the component
## runs.  Every 24hrs the system will rotate these files.  The parameter
## below controls at what time of the day (GMT) the files are rotated.  The
## units of the value are minutes since midnight.
event_rotation_time=0

## Whenever a component of the system dies unexpectedly, a list of system
## administrators can be sent e-mail informing them of the fact.  In most
## cases, the process is automatically restarted.  However, when the process cannot
## be restarted (for instance if the max restart retry count has been exceeded)
## the e-mail will contain that information as well.
sysadm_email=( "joe@big.com" "frank@big.com" )

## run server processes as this user
iic_user=zon

## home dir for IIC services in general
##
## Do not modify this value; reserved for future use.
iic_home=/opt/iic

## home dir for the XML router
##
## Do not modify this value; reserved for future use.
jabber_home=/opt/iic/jabberd

## Database backup configuration
##
## Daily database backups will be done to /var/iic/db-backups on the
## db_host (see above) machine.  The time of day of the backup is
## specified using db_backup_hr and db_backup_min.  For instance, to do
## a backup at 11pm each day set db_backup_hr to 23 and db_backup_min to 0.
## When a backup is done, prior backups are rotated.  The number of days of
## history is controlled by db_backup_history
db_backup_dir=/var/iic/db-backups
db_backup_history=6		# days of backup history
db_backup_hr=4			# hr of backup time of day (0-23)
db_backup_min=0			# min of backup time of day (0-59)

##
## Meeting archive maintenance
##
mtgarchive_cleanup_hr=4		# hr of maintenance time of day (0-23)
mtgarchive_cleanup_min=0	# min of maintenance time of day (0-59)
mtgarchive_cleaning_threshold=1200 # take maintenance action when total archive size reaches this value
mtgarchive_cleanup_action=clean	# warn (sends e-mail to sysadm_email list) or clean
##
## Following parameters apply to "clean" action above
##
mtgarchive_target_sz=1000	# clean archives until this size (in meg) is reached
mtgarchive_cleanup_min_age=7	# Never clean files younger than this (in days) e-mail if <mtgarchive_target_sz> is
				# not reached due to this
mtgarchive_trash_folder=""	# Rather than delete old archives, move them here so they can be backed up.

# Log files rotated when current log reaches rotation_size
rotation_size=1000000
# This is the number of historical log files to keep
history_len=5
# The initial logging level for system components
# (valid settings are error, info and debug)
log_level=error

enable_rt_logs=no
rt_user_port=
rt_call_port=
rt_rsrv_port=

locale_setting="en_US"
iic_prodname="Conferencing"
