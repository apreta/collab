#!/bin/bash -f

. ./utils.sh
. ./global-config

function config_controller_instance
{
    local inst=1
    local mtgarchiver_svcs=""
    local svc=""
    while [ $inst -le ${#mtgarchiver_host[*]} ]; do
	if [ $inst -gt 1 ]; then
	    svc="<service>mtgarchiver$inst</service>"
        else
	    svc="<service>mtgarchiver</service>"
        fi
	mtgarchiver_svcs="$mtgarchiver_svcs$svc"
	inst=$((inst+1))
    done

    inst=0
    local phones=""
    local bridge_xml=""
    local voice_servers=""
    while [ $inst -lt ${#voice_host[*]} ]; do
	if [ $inst -gt 0 ]; then
	    svc="voice$((inst+1))"
        else
	    svc="voice"
        fi
	phones=${voice_phones[$inst]}
	bridge_xml=`sed -e "s+@@@SVC@@@+$svc+" -e "s+@@@PHONE_LIST@@@+$phones+" \
		        -e "s+@@@VOIP_ADDR@@@+${voip_host[$inst]}+" controller_voice_svrs.xml.in \
			| tr "\n" "\001"`
	voice_servers="$voice_servers$bridge_xml"
	inst=$((inst+1))
    done
    
    inst=0
    local share_xml=""
    local share_servers=""
    while [ $inst -lt ${#share_host[*]} ]; do
	share_xml=`sed -e "s+@@@SHARE_HOST@@@+${share_host[$inst]}+" -e "s+@@@SHARE_PORT@@@+$share_port+" controller_share_svrs.xml.in | tr "\n" "\001"`
	share_servers="$share_servers$share_xml"
	inst=$((inst+1))
    done

	local config_pcphone=$voip_pcphone
    if [ "$voice_provider" == "nms" -a "$enable_security" == "yes" ]; then
		config_pcphone="e"
	fi
	
    sed \
        -e "s+@@@PROTOCOL_PREFIX@@@+$protocol_prefix+" \
        -e "s+@@@PORTAL_HNAME@@@+$portal_hname+" \
        -e "s+@@@SHARE_SERVERS@@@+$share_servers+" \
        -e "s+@@@VOICE_SERVERS@@@+$voice_servers+" \
        -e "s+@@@MULTI_PHONE_BRIDGES@@@+$multi_phone_bridges+" \
        -e "s+@@@MTGARCHIVER_SERVICES@@@+$mtgarchiver_svcs+" \
        -e "s+@@@EVENT_ROTATION_TIME@@@+$event_rotation_time+" \
        -e "s+@@@VOICE_PROVIDER@@@+$voice_provider+" \
        -e "s+@@@VOICE_PCPHONE@@@+$config_pcphone+" \
        -e "s\\@@@LICENSE_KEY@@@\\$license_key\\" \
        controller.xml.in | \
    tr "\001" "\n" \
    >> config.xml
}

# Currently supporting a single share server and meeting archiver per install
function config_sharemgr_instance
{
    sed \
    	-e "s/@@@SHAREMGR_RPC_PORT@@@/$sharemgr_rpc_port/" \
	/opt/iic/conf/sharemgr/sharemgr-http.conf.in \
    > /opt/iic/conf/sharemgr/sharemgr-http.conf
    
    rm -f sharemgr_share_map.xml
    local i=0
    while [ $i -lt ${#share_ip[*]} ]; do
	sed \
	    -e "s/@@@SHAREMGR_HOST@@@/$sharemgr_host/" \
	    -e "s/@@@SHARE_HOST@@@/${share_host[$i]}/" \
	    -e "s/@@@EXTERNAL_HNAME@@@/$_external_hname/" \
        -e "s/@@@PROTOCOL_PREFIX@@@/$protocol_prefix/" \
	    -e "s/@@@PORTAL_HNAME@@@/$portal_hname/" \
	    sharemgr_share_map.xml.in \
	>> sharemgr_share_map.xml
	i=$((i+1))
    done
    
    sharemgr_share_map=$( tr "\n" "\b" < sharemgr_share_map.xml )
    
    sed \
	-e "s/@@@SHAREMGR_HOST@@@/$sharemgr_host/" \
	-e "s/@@@SHAREMGR_CONTROL_PORT@@@/$sharemgr_control_port/" \
	-e "s/@@@SHARE_PORT@@@/$share_port/" \
    -e "s/@@@PROTOCOL_PREFIX@@@/$protocol_prefix/" \
	-e "s/@@@PORTAL_HNAME@@@/$portal_hname/" \
	-e "s/@@@EVENT_ROTATION_TIME@@@/$event_rotation_time/" \
	-e "s+@@@SHAREMGR_SHARE_MAP@@@+$sharemgr_share_map+" \
	sharemgr.xml.in \
    | tr "\b" "\n" \
    >> config.xml
}

function config_voice_instance
{
    local inst=$(($1-1))
    
    case $voice_provider in
	stub)
	    sed \
		-e "/@@@PROVIDER_CONFIG@@@/ d" \
		-e "s+@@@WAV_PORT@@@+$wav_port+" \
		voice.xml.in >> config.xml
	    ;;
	appconf)
	    mkdir -p /var/iic/recording
	    chown zon /var/iic/recording

	    sed \
		-e "s+@@@PROVIDER_CONFIG@@@+<providerConfig>/opt/iic/conf/appconfconfig.xml</providerConfig>+" \
		-e "s+@@@WAV_PORT@@@+$wav_port+" \
		voice.xml.in >> config.xml

	    local this_host=$( uname -n )

	    if [ "${voice_host[$inst]}" == "$this_host" -o "${voice_host[$inst]}" == "localhost" ]; then
		sed \
		    -e "s+@@@ASTERISK_NAME@@@+${ast_main_name[$inst]}+" \
		    -e "s+@@@ASTERISK_IP@@@+${ast_main_addr[$inst]}+" \
		    -e "s+@@@ASTERISK_MGR_PORT@@@+${ast_mgr_port[$inst]}+" \
		    -e "s+@@@ASTERISK_USERNAME@@@+${ast_main_username[$inst]}+" \
		    -e "s+@@@ASTERISK_SECRET@@@+${ast_main_secret[$inst]}+" \
		    -e "s+@@@ASTERISK_MAX_PART@@@+${ast_main_max[$inst]}+" \
		    appconfconfig.xml.in > appconfconfig.xml
	    fi
	    ;;
	nms|ext)
	    mkdir -p /var/iic/recording
	    chown zon /var/iic/recording

	    sed \
		-e "s+@@@PROVIDER_CONFIG@@@+<providerConfig>/opt/iic/conf/voiceconfig.xml</providerConfig>+" \
		-e "s+@@@WAV_PORT@@@+$wav_port+" \
		voice.xml.in >> config.xml

	    local this_host=$( uname -n )

	    if [ "${voice_host[$inst]}" == "$this_host" -o "${voice_host[$inst]}" == "localhost" ]; then
	        sed \
		    -e "s+@@@VOIP_IP@@@+${voip_ip[$inst]}+" \
		    -e "s+@@@VOIP_HOST@@@+${voip_host[$inst]}+" \
		    -e "s+@@@NMS_BASE_IP@@@+${nms_base_ip[$inst]}+" \
		    -e "s+@@@DIAL_PREFIX@@@+$dial_prefix+" \
		    -e "s+@@@BOARD_PSTN_TRUNKS@@@+$board_pstn_trunks+" \
		    -e "s+@@@BOARD_MAX_CALLS@@@+$board_max_calls+" \
		    -e "s+@@@BOARD_NUM_RECORDERS@@@+$board_num_recorders+" \
                    -e "s+@@@SIP_DEFAULT_DOMAIN@@@+$sip_default_domain+" \
                    -e "s+@@@SIP_OUTBOUND_PROXY@@@+$sip_outbound_proxy+" \
                    -e "s+@@@LINE_PROTOCOL@@@+$pstn_line_protocol+" \
                    -e "s+@@@ISDN_OPERATOR@@@+$isdn_operator+" \
                    -e "s+@@@ISDN_COUNTRY@@@+$isdn_country+" \
		    voiceconfig.xml.in > voiceconfig.xml
	    fi
	    ;;
    esac
}

function config_mtgarchiver_instance
{
    mypath="../locale/$locale_setting/emails"
    if [ ! -e $mypath/new-mtgarchive-template.txt ]; then
    	cp -p $mypath/new-mtgarchive-template.default $mypath/new-mtgarchive-template.txt
    	echo "New meeting archive template has been updated"
    fi

    if [ ! -e $mypath/meeting-summary-template.txt ]; then
    	cp -p $mypath/meeting-summary-template.default $mypath/meeting-summary-template.txt
	   echo "New meeting summary template has been updated"
    fi

    local inst=1
    local name=""
    local service_map=""
    local -a hosts
    if [ ${#mtgarchiver_voice_host[*]} -gt 0 ]; then
	hosts=( ${mtgarchiver_voice_host[*]} )
    else
	hosts=( ${voice_host[*]} )
    fi
    for host in ${hosts[*]} ; do
	if [ $inst -eq 1 ]; then
	    name="voice"
	else
	    name="voice$inst"
	fi
	service_map="$service_map<name>$name</name><host>$host</host>\n"
	inst=$((inst+1))
    done

    sed \
        -e "s+@@@WAV_PORT@@@+$wav_port+" \
        -e "s+@@@PROTOCOL_PREFIX@@@+$protocol_prefix+" \
        -e "s+@@@PORTAL_HNAME@@@+$portal_hname+" \
        -e "s+@@@MTGARCHIVER_FROM_ADDRESS@@@+$mtgarchiver_from_address+" \
        -e "s+@@@MTGARCHIVER_FROM_DISPLAY@@@+$mtgarchiver_from_display+" \
        -e "s+@@@MTGARCHIVER_SUBJECT@@@+$mtgarchiver_subject+" \
        -e "s+@@@SERVICE_MAP@@@+$service_map+" \
        -e "s+@@@VOICE_PROVIDER@@@+$voice_provider+" \
        -e "s+@@@SYS_ADMIN_EMAIL@@@+$sysadm_email+" \
        -e "s+@@@MTGARCHIVER_RETRY_WAIT@@@+$mtgarchiver_retry_wait+" \
        -e "s+@@@MTGARCHIVER_MAX_RETRIES@@@+$mtgarchiver_max_retries+" \
        -e "s+@@@LOCALE_SETTING@@@+$locale_setting+" \
    	-e "s+@@@IIC_PRODNAME@@@+$iic_prodname+" \
        mtgarchiver.xml.in \
    >> config.xml

    sed \
        -e "s+@@@PROTOCOL_PREFIX@@@+$protocol_prefix+" \
        -e "s+@@@PORTAL_HNAME@@@+$portal_hname+" \
        dummy.html.in \
    > dummy.html

    sed \
        -e "s+@@@PROTOCOL_PREFIX@@@+$protocol_prefix+" \
        -e "s+@@@PORTAL_HNAME@@@+$portal_hname+" \
        dummy2.html.in \
    > dummy2.html

    if [ ! -e /var/iic/htdocs/imidio/mtgarchive ]; then
	mkdir -p /var/iic/htdocs/imidio/mtgarchive
	chown $iic_user /var/iic/htdocs/imidio/mtgarchive
    fi
}

function config_converter_instance
{
    local inst=1
    local name=""

    sed \
        -e "s+@@@WAV_PORT@@@+$wav_port+" \
        -e "s+@@@PROTOCOL_PREFIX@@@+$protocol_prefix+" \
        -e "s+@@@PORTAL_HNAME@@@+$portal_hname+" \
    	-e "s+@@@IIC_PRODNAME@@@+$iic_prodname+" \
        converter.xml.in \
    >> config.xml
}

function config_client_upgrade
{
    if [ "$product" == "zon-lite" ]; then
        client_installer_name="imidio_lite.exe"
    else
        client_installer_name="conferencing.exe"
    fi
    cuf_version=$(echo $client_version | sed -e 's+\.+, +g' -e 's+$+;+')
    sed \
	-e "s+@@@CLIENT_VERSION@@@+$cuf_version+" \
    -e "s+@@@PROTOCOL_PREFIX@@@+$protocol_prefix+" \
	-e "s+@@@PORTAL_HNAME@@@+$portal_hname+" \
	-e "s+@@@CLIENT_INSTALLER_NAME@@@+$client_installer_name+" \
	client_upgrade_file.imi.in \
    > client_upgrade_file.imi
}

function config_addressbk_instance
{
    mypath="../locale/$locale_setting/emails"
    if [ ! -e $mypath/new-user-template.txt ]; then
    	cp -p $mypath/new-user-template.default $mypath/new-user-template.txt
	   echo "New user template has been updated"
    fi

    sed \
        -e "s+@@@NEW_USER_FROM_ADDRESS@@@+$new_user_from_address+" \
        -e "s+@@@NEW_USER_FROM_DISPLAY@@@+$new_user_from_display+" \
        -e "s+@@@NEW_USER_SUBJECT@@@+$new_user_subject+" \
        -e "s+@@@EVENT_ROTATION_TIME@@@+$event_rotation_time+" \
        -e "s+@@@PORTAL_IP@@@+$portal_ip+" \
        -e "s+@@@PROTOCOL_PREFIX@@@+$protocol_prefix+" \
    	-e "s+@@@PORTAL_HNAME@@@+$portal_hname+" \
        -e "s+@@@LOCALE_SETTING@@@+$locale_setting+" \
    	-e "s+@@@IIC_PRODNAME@@@+$iic_prodname+" \
            addressbk.xml.in \
    >> config.xml

    if [ "$enable_ldap_sync" == "yes" ]; then
	echo "     <ldapConfig>/opt/iic/conf/ldap.xml</ldapConfig>" >> config.xml
    fi

    config_client_upgrade
}

function config_mailer_instance
{
    mypath="../locale/$locale_setting/emails"
    if [ ! -e $mypath/invitation-template.txt ]; then
        cp -p $mypath/invitation-template.default $mypath/invitation-template.txt
        echo "Meeting invitation template has been updated"
    fi

    sed \
        -e "s+@@@SMTP_HOST@@@+$smtp_host+" \
        -e "s+@@@SMTP_USER@@@+$smtp_user+" \
        -e "s+@@@SMTP_PSWD@@@+$smtp_pswd+" \
        -e "s+@@@DEFAULT_FROM_ADDRESS@@@+$default_from_address+" \
        -e "s+@@@DEFAULT_FROM_DISPLAY@@@+$default_from_display+" \
        -e "s+@@@INVITE_FROM_ADDRESS@@@+$invite_from_address+" \
        -e "s+@@@INVITE_FROM_DISPLAY@@@+$invite_from_display+" \
        -e "s+@@@INVITE_SUBJECT@@@+$invite_subject+" \
        -e "s+@@@LOCALE_SETTING@@@+$locale_setting+" \
    	-e "s+@@@IIC_PRODNAME@@@+$iic_prodname+" \
        mailer.xml.in \
    >> config.xml
}

function config_c2s_instance
{
    if [ "$enable_security" == "yes" ]; then
      sed \
      -e "s+@@@EXTERNAL_HNAME@@@+$external_hname+" \
      -e "s+@@@SERVER_CERTIFICATE@@@+$server_certificate+" \
      jadc2s_ssl.xml.in \
      >> config.xml
    else
      sed \
  	  -e "s+@@@EXTERNAL_HNAME@@@+$external_hname+" \
      jadc2s.xml.in \
      >> config.xml
    fi
}

function config_component_instance
{
    local svc=$1
    local inst=$2
    local port=$3
    local heartbeat=$4
    local threads=$5
    local dthread=$6

    local svcinst
    if [ $inst -eq 1 ]; then
	svcinst=$svc
    elif [ $inst -gt 1 ]; then
	svcinst=$svc$inst
    fi

    local heartbeat_action;
    if [ $heartbeat -lt 0 ]; then
        # use default
	heartbeat_action='/@@@HEARTBEAT@@@/ d'
    else
	heartbeat_action="s+@@@HEARTBEAT@@@+$heartbeat+"
    fi

    local threads_action;
    if [ $threads -lt 0 ]; then
        # use default
	threads_action='/@@@THREADS@@@/ d'
    else
	threads_action="s+@@@THREADS@@@+$threads+"
    fi

    local dthread_action;
    if [ $dthread -lt 0 ]; then
        # use default
	dthread_action='/@@@DISPATCH_THREAD@@@/ d'
    else
	dthread_action="s+@@@DISPATCH_THREAD@@@+$dthread+"
    fi

    overrides_action="/@@@SM_OVERRIDES@@@/ d"
    if [ "$svc" == "c2s" ]; then
	local this_host=$( uname -n )
	for h in ${lcl_xmlrouter[*]} ; do
	    if [ "$this_host" == "$h" -o "$h" == "localhost" ]; then
		overrides_action="s+@@@SM_OVERRIDES@@@+<host>$h</host><backup></backup>+"
	    fi
	done
    fi

    sed \
	-e "s+@@@SVC@@@+$svc+g" \
	-e "s+@@@SVCINST@@@+$svcinst+g" \
	-e "s+@@@PORT@@@+$port+" \
	-e "$heartbeat_action" \
	-e "$threads_action" \
	-e "$dthread_action" \
	-e "$overrides_action" \
	jcomp_begin.xml.in \
     >> config.xml

    # Configure the per-service parameters
    case $svc in 
	addressbk|c2s|controller|mailer|mtgarchiver|voice|sharemgr|converter)
	    config_${svc}_instance $inst
	    ;;

	# No configurable parameters, just append the template
	# as is.
	*)
	    cat $1.xml.in >> config.xml
    esac

    sed \
	-e "s+@@@SVC@@@+$svc+g" \
	jcomp_end.xml.in \
    >> config.xml
}

if [ "$product" == "zon-lite" ]; then
    _external_hname=$external_hname
    external_hname="share"
    hname1=${lcl_xmlrouter[0]}
    hname2_action='/LCL_XMLROUTER_HNAME2/ d'

    config_client_upgrade
else
    hname1=${lcl_xmlrouter[0]}
    if [ ${#lcl_xmlrouter[*]} -eq 1 ]; then
        hname2_action='/LCL_XMLROUTER_HNAME2/ d'
    else
        hname2_action="s+@@@LCL_XMLROUTER_HNAME2@@@+${lcl_xmlrouter[1]}+"
    fi
fi

sed \
    -e "s+@@@LCL_XMLROUTER_HNAME1@@@+$hname1+" \
    -e "$hname2_action" \
    -e "s:@@@SERVICE_SECRET@@@:$service_secret:" \
    -e "s+@@@EXTERNAL_HNAME@@@+$external_hname+g" \
    -e "s:@@@SESSION_CREATION_SECRET@@@:$session_creation_secret:" \
    -e "s+@@@ROTATION_SIZE@@@+$rotation_size+" \
    -e "s+@@@HISTORY_LEN@@@+$history_len+" \
    -e "s+@@@LOG_LEVEL@@@+$log_level+" \
    config_begin.xml.in \
> config.xml

inst=1
for port in $addressbk_port ; do
    config_component_instance addressbk $inst $port -1 -1 -1

    sed \
	-e "s+@@@DB_HOST@@@+$db_host+" \
	-e "s+@@@DB_NAME@@@+$db_name+" \
	-e "s+@@@DB_USER@@@+$db_user+" \
	-e "s+@@@DB_PSWD@@@+$db_pswd+" \
	< sqldb.xml.in \
    > sqldb.xml
done

inst=1
for port in $jadc2s_port ; do
    config_component_instance c2s $inst $port -1 -1 -1
done

inst=1
for port in $extportal_port ; do
    config_component_instance extportal $inst $port -1 -1 -1
done

inst=1
for port in $controller_port ; do
    config_component_instance controller $inst $port 8 -1 -1
done

inst=1
for port in $extapi_port ; do
    config_component_instance extapi $inst $port -1 5 1
done

inst=1
for port in $mailer_port ; do
    config_component_instance mailer $inst $port -1 -1 -1
done

inst=1
for port in $converter_port ; do
    config_component_instance converter $inst $port -1 -1 -1
done

inst=1
for port in $mtgarchiver_port ; do
    config_component_instance mtgarchiver $inst $port -1 -1 -1
done

inst=1
for port in $voice_port ; do
    config_component_instance voice $inst $port -1 0 -1
done

inst=1
for port in $sharemgr_port ; do
    config_component_instance sharemgr $inst $port -1 0 -1
done

cat config_end.xml.in >> config.xml
