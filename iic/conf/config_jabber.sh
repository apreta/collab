#! /bin/bash -f

function generate_service_section
{
    local template=$1
    local svc=$2
    local out=$3

    sed \
	-e "s/@@@ID@@@/${svc}/" \
	-e "s/@@@HNAME@@@/${svc}.iic/" \
	-e "s/@@@PORT@@@/$cur_port/" \
	-e "s:@@@SERVICE_SECRET@@@:$service_secret:" \
        $template \
    >> $out
}

function generate_mod_router_map_section
{
    local template=$1
    local svc=$2
    local out=$3

    sed \
	-e "s/@@@ID@@@/${svc}/" \
	-e "s/@@@HNAME@@@/${svc}.iic/" \
        $template \
    >> $out
}

function generate_jabber_xml
{
   local current_xmlrouter_hname=$1
   local current_peer_hname=$2
   cur_port=$svc_base_port

   # Generate a jabber.xml section for controller(s)
   local inst=1
   local suffix=""
   rm -f ./controller_map.xml ./controller.xml
   for host in $controller_host ; do
      cur_port=${controller_port[$((inst-1))]}
      generate_mod_router_map_section \
	  /opt/iic/jabberd/controller_map.xml.in \
	  controller$suffix \
          ./controller_map.xml
      generate_service_section \
	  /opt/iic/jabberd/service.xml.in \
	  controller$suffix \
          ./controller.xml
      inst=$((inst+1))
      suffix=inst
   done

   # Generate a jabber.xml section for voice bridge(s)
   inst=1
   suffix=""
   rm -f ./voice_map.xml ./voice.xml
   for host in $voice_host ; do
      cur_port=${voice_port[$((inst-1))]}
      generate_mod_router_map_section \
	  /opt/iic/jabberd/service_map.xml.in \
	  voice$suffix \
          ./voice_map.xml
      generate_service_section \
	  /opt/iic/jabberd/service.xml.in \
	  voice$suffix \
          ./voice.xml
      inst=$((inst+1))
      suffix=inst
   done

   # Generate a jabber.xml section for mtgarchiver.xml
   inst=1
   suffix=""
   rm -f ./mtgarchiver_map.xml ./mtgarchiver.xml
   for host in $mtgarchiver_host ; do
       cur_port=${mtgarchiver_port[$((inst-1))]}
       generate_mod_router_map_section \
	   /opt/iic/jabberd/service_map.xml.in \
	   mtgarchiver$suffix \
	   ./mtgarchiver_map.xml
       generate_service_section \
	   /opt/iic/jabberd/service.xml.in \
	   mtgarchiver$suffix \
	   ./mtgarchiver.xml
       inst=$((inst+1))
       suffix=inst
   done

   inst=1
   suffix=""
   rm -f ./jadc2s.xml
   while [ $inst -le $jadc2s_instances ]; do
      cur_port=${jadc2s_port[$((inst-1))]}
      generate_service_section \
	  /opt/iic/jabberd/service.xml.in \
	  c2s$suffix \
	  ./jadc2s.xml
      inst=$((inst+1))
      suffix=inst
   done

   inst=1
   suffix=""
   rm -f ./extportal.xml
   while [ $inst -le $extportal_instances ]; do
      cur_port=${extportal_port[$((inst-1))]}
      generate_service_section \
	  /opt/iic/jabberd/service.xml.in \
	  extportal$suffix \
	  ./extportal.xml
      inst=$((inst+1))
      suffix=inst
   done

   # translate all newlines into ^A's so that it is in one line for sed(1) below
   [ -e controller_map.xml ] && local controller_map=`tr "\n" "\001" < controller_map.xml`
   [ -e controller.xml ] && local controller_svc=`tr "\n" "\001" < controller.xml`
   [ -e voice_map.xml ] && local voice_map=`tr "\n" "\001" < voice_map.xml`
   [ -e voice.xml ] && local voice_svc=`tr "\n" "\001" < voice.xml`
   local mtgarchiver_map=`tr "\n" "\001" < mtgarchiver_map.xml`
   local mtgarchiver_svc=`tr "\n" "\001" < mtgarchiver.xml`
   local jadc2s_svc=`tr "\n" "\001" < jadc2s.xml`
   local extportal_svc=`tr "\n" "\001" < extportal.xml`
   local chat_logger_svc=`tr "\n" "\001" < /opt/iic/jabberd/clogger.xml`

   rm -f controller_map.xml voice_map.xml mtgarchiver_map.xml \
         controller.xml voice.xml mtgarchiver.xml jadc2s.xml extportal.xml

   if [ "$current_peer_hname" == "" ]; then
      peer_action="/@@@CURRENT_PEER_HNAME@@@/ d"
   else
      peer_action="s+@@@CURRENT_PEER_HNAME@@@+$current_peer_hname+"
   fi

   local debug
   if [ ! "$log_level" == "error" ]; then
       debug=1
   else
       debug=0
   fi
   
   if [ "$product" == "zon-lite" ]; then
     local jabbertemplate="/opt/iic/jabberd/jabber-lite.xml.in"
   else
     local jabbertemplate="/opt/iic/jabberd/jabber.xml.in"
   fi

   local xml
   local ldapSedCommand
    if [ "$enable_ldap_sync" == "yes" ]; then
	local server
	xml='<!-- Servers -->'
	for server in ${ldap_servers[@]}; do
	    build_ldap_auth_xml $server
	    xml="$xml""$strval"
	done
	ldapSedCommand='-e s/<!--mod_auth_ldap/<mod_auth_ldap/ -e s/mod_auth_ldap-->/mod_auth_ldap>/ -e s/<mod_auth_digest/<!--mod_auth_digest/ -e s+/mod_auth_digest>+/mod_auth_digest-->+'
    else
	xml='<!-- Disabled -->'
    fi

   if [ "$log_chat" == "yes" ]; then
       chat_logger_jsm_action="s+@@@LOAD_CHAT_LOGGER@@@+<mod_chatlog>./jsm/jsm.so</mod_chatlog>+"
       chat_logger_action="s+@@@CHAT_LOGGER_INSTANCE@@@+$chat_logger_svc+"
   else
       chat_logger_jsm_action="/@@@LOAD_CHAT_LOGGER@@@/ d"
       chat_logger_action="/@@@CHAT_LOGGER_INSTANCE@@@/ d"
   fi

   local rev=0
   sed \
       -e "s/@@@EXTERNAL_HNAME@@@/$external_hname/" \
       -e "s/@@@CURRENT_XMLROUTER_HNAME@@@/$current_xmlrouter_hname/" \
       -e "$peer_action" \
       -e "s:@@@SESSION_CREATION_SECRET@@@:$session_creation_secret:" \
       \
       -e "s+@@@CONTROLLER_MAP@@@+$controller_map+" \
       -e "s+@@@VOICE_MAP@@@+$voice_map+" \
       -e "s+@@@MTGARCHIVER_MAP@@@+$mtgarchiver_map+" \
       \
       -e "s:@@@CONTROLLER_SERVICES@@@:$controller_svc:" \
       -e "s:@@@VOICE_SERVICES@@@:$voice_svc:" \
       -e "s:@@@MTGARCHIVER_SERVICES@@@:$mtgarchiver_svc:" \
       -e "s:@@@JADC2S_SERVICES@@@:$jadc2s_svc:" \
       -e "s:@@@EXTPORTAL_SERVICES@@@:$extportal_svc:" \
       \
       -e "s:@@@SERVICE_SECRET@@@:$service_secret:g" \
       -e "s/@@@ADDRESSBK_PORT@@@/$addressbk_port/" \
       -e "s/@@@EXTAPI_PORT@@@/$extapi_port/" \
       -e "s/@@@MAILER_PORT@@@/$mailer_port/" \
       -e "s/@@@CONVERTER_PORT@@@/$converter_port/" \
       -e "s/@@@ROTATION_SIZE@@@/$rotation_size/" \
       -e "s/@@@HISTORY_LEN@@@/$history_len/" \
       -e "s/@@@DEBUG@@@/$debug/" \
       -e "s/@@@SHAREMGR_PORT@@@/$sharemgr_port/" \
       \
       -e "$chat_logger_jsm_action" \
       -e "$chat_logger_action" \
       \
        -e "/@@@SERVERS@@@/ c $xml" \
       $ldapSedCommand\
       $jabbertemplate \
   > ./jabber.xml.$rev
   rev=$((!rev))

   # make all newliney again
   tr "\001" "\n" < ./jabber.xml.$((!rev)) > ./jabber.xml.$rev
   rev=$((!rev))

   rm ./jabber.xml.$rev
   mv jabber.xml.$((!rev)) jabber.xml.$current_xmlrouter_hname
}

. ./utils.sh

# load configuration state
. /opt/iic/conf/global-config

this_host=`uname -n`
idx=0
while [ $idx -lt ${#lcl_xmlrouter[*]} ]; do
    if [ "${lcl_xmlrouter[$idx]}" == "$this_host" -o "${lcl_xmlrouter[$idx]}" == "localhost" ]; then
	# Running jabber on this host
	if [ $idx -eq 0 ]; then
	    if [ ${#lcl_xmlrouter[*]} -eq 1 ]; then
		# Single jabber server config
		generate_jabber_xml $this_host
	    else
		# Peer jabber servers config
		generate_jabber_xml $this_host ${lcl_xmlrouter[1]}
	    fi
	else
	    # Peer jabber servers config; second server
	    generate_jabber_xml $this_host ${lcl_xmlrouter[0]}
	fi

	mv jabber.xml.$this_host /opt/iic/jabberd/jabber.xml
	
	if [ "$db_pswd" == "" ]; then
	    pswd_action="/@@@DB_PSWD@@@/ d"
	else
	    pswd_action="s/@@@DB_PSWD@@@/$db_pswd/"
	fi
	
	sed \
	    -e "s/@@@DB_HOST@@@/$db_host/" \
	    -e "s/@@@DB_NAME@@@/$db_name/" \
	    -e "s/@@@DB_USER@@@/$db_user/" \
	    -e "$pswd_action" \
	    /opt/iic/jabberd/xdb_sql/xdb_sql.xml.in \
	    > /opt/iic/jabberd/xdb_sql/xdb_sql.xml
    fi

    idx=$(($idx+1))
done

save_config
