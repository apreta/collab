#!/bin/sh

if ! /usr/bin/psql -U $db_user --dbname $db_name -c "select count(*) from deleted_contacts;" >/dev/null 2>&1 ; then 

	echo "Upgrading database to support syncrhonization..."

	/usr/bin/createlang -U postgres PLPGSQL zon
	
	/usr/bin/psql -U $db_user --dbname $db_name <db_upgrade_23.sql

fi
