BEGIN { ooo = "true" ; }
/IfModule worker.c/ { ooo = "false"; }
/\/IfModule/ { if (ooo == "false") ooo = "nexttrue"; printf "<IfModule worker.c>\n\tStartServers  1\n\tServerLimit  1\n\tMaxClients  25\n\tMinSpareThreads 25\n\tMaxSpareThreads 75\n\tThreadsPerChild 25\n\tMaxRequestsPerChild 0\n</IfModule>\n"; }
ooo == "true" { print $0 }
ooo == "nexttrue" { ooo = "true" }
