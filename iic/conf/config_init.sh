#!/bin/sh

# This script:
#	- removes any prior /etc/sysconfig and /etc/init.d configuration
#	- configures parameters in /etc/sysconfig
#	- configuration for scripts under /etc/rc.d/init.d
#	- creates any directories needed by the daemons

. ./utils.sh
. ./global-config

function cleanup_init
{
    rm -f /etc/sysconfig/iicshare

    rm -f $startup_dir/iicaddress
    rm -f $startup_dir/iiccontroller
    rm -f $startup_dir/iicextapi
    rm -f $startup_dir/iichttpd
    rm -f $startup_dir/iicjabber
    rm -f $startup_dir/iicjadc2s
    rm -f $startup_dir/iicmailer
    rm -f $startup_dir/iicmtgarchiver
    rm -f $startup_dir/iicpgpid
    rm -f $startup_dir/iicportal
    rm -f $startup_dir/iicproxy
    rm -f $startup_dir/iicshare
    rm -f $startup_dir/iictransmitter
    rm -f $startup_dir/iicsharemgr
    rm -f $startup_dir/iicvoice
    rm -f $startup_dir/iicvoipgw
    rm -f $startup_dir/iiconverter
    
    rm -f /etc/rc.d/rc?.d/*iicaddress
    rm -f /etc/rc.d/rc?.d/*iiccontroller
    rm -f /etc/rc.d/rc?.d/*iicextapi
    rm -f /etc/rc.d/rc?.d/*iichttpd
    rm -f /etc/rc.d/rc?.d/*iicjabber
    rm -f /etc/rc.d/rc?.d/*iicjadc2s
    rm -f /etc/rc.d/rc?.d/*iicmailer
    rm -f /etc/rc.d/rc?.d/*iicmtgarchiver
    rm -f /etc/rc.d/rc?.d/*iicpgpid
    rm -f /etc/rc.d/rc?.d/*iicportal
    rm -f /etc/rc.d/rc?.d/*iicproxy
    rm -f /etc/rc.d/rc?.d/*iicshare
    rm -f /etc/rc.d/rc?.d/*iictransmitter
    rm -f /etc/rc.d/rc?.d/*iicsharemgr
    rm -f /etc/rc.d/rc?.d/*iicvoice
    rm -f /etc/rc.d/rc?.d/*iicvoipgw
    rm -f /etc/rc.d/rc?.d/*iicconverter
}

function install_port_forwarding
{
    if [ "$iptables_port_forwarding" == "yes" ]; then
	local dirty=0
	local idx
	local ip
	
	if [ "$sles_install" ]; then
	    IPTABLES=/usr/sbin/iptables
	else
	    IPTABLES=/sbin/iptables
	fi

	idx=0
	for host in ${share_local_host[*]} ; do
	    if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
		local this_ip=${share_ip[$idx]}
		
                cat >/etc/sysconfig/iicforward-share <<-text
		if ! $IPTABLES -n -t nat --list | grep $this_ip; then
		  $IPTABLES -t nat -A PREROUTING -d  $this_ip -p tcp -m tcp --dport 21 -j REDIRECT --to-ports $share_port
		  $IPTABLES -t nat -A PREROUTING -d  $this_ip -p tcp -m tcp --dport 443 -j REDIRECT --to-ports $share_port
		  $IPTABLES -t nat -A PREROUTING -d  $this_ip -p tcp -m tcp --dport 1270 -j REDIRECT --to-ports $share_port
		fi
		text
		dirty=1
	    fi
	    idx=$((idx+1))
	done

	idx=0
	for host in ${lcl_xmlrouter[*]} ; do
	    if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
		local this_ip=${lcl_xmlrouter_ip[$idx]}
		
		cat >/etc/sysconfig/iicforward-xmlrouter <<-text
		if ! $IPTABLES -n -t nat --list | grep $this_ip; then
		  $IPTABLES -t nat -A PREROUTING -d  $this_ip -p tcp -m tcp --dport 21 -j REDIRECT --to-ports $iic_client_port
		  $IPTABLES -t nat -A PREROUTING -d  $this_ip -p tcp -m tcp --dport 443 -j REDIRECT --to-ports $iic_client_port
		  $IPTABLES -t nat -A PREROUTING -d  $this_ip -p tcp -m tcp --dport 1270 -j REDIRECT --to-ports $iic_client_port
		fi
		text
		dirty=1
	    fi
	    idx=$((idx+1))
	done
    else
    	rm /etc/sysconfig/iicforward-share
    	rm /etc/sysconfig/iicforward-xmlrouter
    fi
}

function create_imidio_iic_sysconfig
{
    local email_args=""
    local e
    for e in ${sysadm_email[*]} ; do
	email_args="$email_args --sysadm '$e'"
    done

    local -a proxy_ip=()
    local -a proxy_port=()
    local -a this_share_ip=()
    local -a this_voip_ip=()
    this_host=`uname -n`
    local idx=0
    local ins=0
    for host in ${share_local_host[*]} ; do
	if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
	    this_share_ip[$ins]=${share_ip[$idx]}
	    proxy_ip[$ins]=${share_ip[$idx]}
	    proxy_port[$ins]=$share_port
	    ins=$((ins+1))
	fi
	idx=$((idx+1))
    done
    
    idx=0
    for host in ${lcl_xmlrouter[*]} ; do
	if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
	    proxy_ip[$ins]=${lcl_xmlrouter_ip[$idx]}
	    proxy_port[$ins]=$iic_client_port
	    ins=$((ins+1))
	fi
	idx=$((idx+1))
    done

    # Voice bridge arrays and voip arrays should be in same order
    idx=0
    for host in ${voice_host[*]} ; do
	if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
	    this_voip_ip=${voip_ip[$idx]}
	fi
	idx=$((idx+1))
    done

    sed \
	-e "s+@@@JABBER_HOME@@@+$jabber_home+" \
	-e "s+@@@IIC_USER@@@+$iic_user+" \
	-e "s+@@@IIC_HOME@@@+$iic_home+" \
	-e "s+@@@IIC_MTG_GROUP@@@+$iic_mtg_group+" \
	-e "s+@@@LOCALE_SETTING@@@+$locale_setting+" \
	-e "s+@@@IIC_PRODNAME@@@+$iic_prodname+" \
	-e "s+@@@VOICE_PROVIDER@@@+$voice_provider+" \
	-e "s+@@@PROXY_IP@@@+${proxy_ip[*]}+"\
	-e "s+@@@PROXY_PORT@@@+${proxy_port[*]}+"\
	-e "s+@@@SHARE_IP@@@+${this_share_ip[*]}+"\
	-e "s+@@@SHARE_PORT@@@+$share_port+"\
	-e "s+@@@SYSADM@@@+$email_args+" \
	-e "s+@@@MTGARCHIVER_PRIO@@@+$mtgarchiver_prio+" \
	-e "s+@@@VOICE_HOST@@@+$voice_host+" \
	-e "s+@@@VOIP_ADDR@@@+${this_voip_ip[*]}+" \
	sysconfig/imidio_iic_zon.in \
    > /etc/sysconfig/imidio_iic
}


function configure_addressbk_rt_ports
{
    if [ "$enable_rt_logs" == "yes" ]; then
	cp /etc/services /etc/services.bk
	grep -v "zon-rt-user" /etc/services > /tmp/services.$$
	grep -v "zon-rt-rsrv" /tmp/services.$$ > /tmp/services.1.$$
	echo -e "zon-rt-user\t$rt_user_port/tcp   # Zon real-time user log" >> /tmp/services.1.$$
	echo -e "zon-rt-rsrv\t$rt_rsrv_port/tcp   # Zon real-time reservation log" >> /tmp/services.1.$$
	mv /tmp/services.1.$$ /etc/services
	cp zon-rt-user /etc/xinetd.d
	cp zon-rt-rsrv /etc/xinetd.d
	$startup_dir/xinetd reload
	rm -f /tmp/services.1.$$ /tmp/services.$$
    else
	grep -v "zon-rt-user" /etc/services > /tmp/services.$$
	grep -v "zon-rt-rsrv" /tmp/services.$$ > /tmp/services.1.$$
	mv /tmp/services.1.$$ /etc/services
	rm -f /tmp/services.1.$$ /tmp/services.$$ \
	    /etc/xinet.d/zon-rt-user /etc/xinet.d/zon-rt-rsrv
	$startup_dir/xinetd reload
    fi
}

function configure_controller_rt_ports
{
    if [ "$enable_rt_logs" == "yes" ]; then
	cp /etc/services /etc/services.bk
	grep -v "zon-rt-call" /etc/services > /tmp/services.$$
	echo -e "zon-rt-call\t$rt_call_port/tcp   # Zon real-time call log" >> /tmp/services.$$
	mv /tmp/services.$$ /etc/services
	cp zon-rt-call /etc/xinetd.d
	$startup_dir/xinetd reload
    else
	grep -v "zon-rt-call" /etc/services > /tmp/services.$$
	mv /tmp/services.$$ /etc/services
	rm -f /tmp/services.$$ /etc/xinetd.d/zon-rt-call
	$startup_dir/xinetd reload
    fi
}

cleanup_init

create_imidio_iic_sysconfig

install_port_forwarding

cp init.d/iicutils $startup_dir

this_host=`uname -n`
for host in ${lcl_xmlrouter[*]} ; do
    if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
	install_init_script iicjabber
	install_init_script iicjadc2s
	mkdir -p /var/iic/chatlog
	chown -R zon /var/iic/chatlog
	echo "Installed xmlrouter"
    fi
done

for host in ${controller_host[*]} ; do
    if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
	install_init_script iiccontroller
	configure_controller_rt_ports
	mkdir -p /var/iic/chatlog
	chown -R zon /var/iic/chatlog
	echo "Installed controller"
    fi
done

for host in $addressbk_host ; do
    if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
	install_init_script iicaddress
	configure_addressbk_rt_ports
	echo "Installed addressbk"
    fi
done

for host in ${voice_host[*]} ; do
    if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
	mkdir -p /var/iic/recording
	install_init_script iicvoice
	echo "Installed voice"
    fi
done

for host in $mailer_host ; do
    if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
	install_init_script iicmailer
	echo "Installed mailer"
    fi
done

for host in $converter_host ; do
    if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
	install_init_script iicconverter
	echo "Installed converter"
    fi
done

for host in ${share_local_host[*]} ; do
    if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
	mkdir -p /var/iic/appshare-recording
	chown -R $iic_user /var/iic/appshare-recording
	install_init_script iicshare
	install_init_script iictransmitter
	if [ "$product" == "zon-lite" ]; then
	    echo  "OPTIONS=\"-u $sharemgr_control_port\"" > /etc/sysconfig/iicshare
	fi
	echo "Installed share/transmitter"
    fi
done

host=$sharemgr_host
if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
    install_init_script iicsharemgr
    echo "Installed sharemgr"
fi

for host in ${mtgarchiver_host[*]} ; do
    if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
	install_init_script iicmtgarchiver
	echo "Installed mtgarchiver"
    fi
done

if [ -e /opt/iic/conf/init.d/iicpgpid ]; then
    install_init_script iicpgpid
    echo "Installed pgpid"
fi

# Get a list of hosts that are unique.  Multiple share servers may be running
# on a machine, but install once
if [ "$multi_ip" == "y" ]; then
    proxy_host=$(echo ${lcl_xmlrouter[*]} ${share_local_host[*]} | tr " " "\n" | sort -u | tr "\n" " ")
    for host in $proxy_host ; do
        if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
	    install_init_script iicproxy
	    echo "Installed proxy"
        fi
    done
fi
