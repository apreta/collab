#!/bin/bash

function usage
{
    echo "usage: $0 [--initdb]"
}

initdb="false"
function parse_cmdline
{
    while [ $# -gt 0 ] ; do
	case $1 in
	    --initdb)
		initdb="true"
		shift
		;;
            --password)
                adminpwd=$2
                shift 2
                ;;
	    --help|-h)
		usage
		exit -1
		;;
	    *)
		echo "Unknown argument '$1' seen"
		usage
		exit -1
		;;
	esac
    done
}

parse_cmdline $*

. ./global-config
. ./utils.sh

if [ -e ./config_db.sh -a "$initdb" == "true" ] ; then
    ./config_db.sh --initdb $adminpwd
else
    ./config_db.sh
fi

[ -e ./questions-*.sh ]     && ./questions-*.sh

[ -e ./config_jabber.sh ]   && ./config_jabber.sh

# jabber components
[ -e ./config_jcomp.sh ]    && ./config_jcomp.sh

# boot scripts
./config_init.sh


# web portal and extapi
chown -R $iic_user /var/iic/htdocs
./config_web.sh

# heartbeat
[ ! -z $install_ha ] && ./config_ha.sh

# various batch processes; db-backups, file cleanup, etc.
./config_batch.sh

chown -R $iic_user /opt/iic /var/iic/appshare-recording /var/iic/cdr /var/iic/chatlog /var/iic/db-backups /var/iic/mtgspool /var/iic/recording /var/log/iic

chmod 640 /opt/iic/conf/global-config
chmod 640 /opt/iic/conf/config.xml
chmod 640 /opt/iic/jabberd/jabber.xml
