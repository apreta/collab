#!/bin/bash

. ./utils.sh
. ./global-config

ok="n"
while [ "$ok" == "n" ]; do
    echo -n "Under what user should system services run? [$iic_user] "
    read -e resp
    if [ ! -z "$resp" ]; then
	iic_user="$resp"
    fi

    if [ -z $iic_user ]; then
	echo "You must specify a value for this configuration item"
    else
	ok="y"
    fi
done

this_host=$( uname -n )

ok="n"
while [ "$ok" == "n" ]; do
    portal_local_hname="$this_host"
    share_host=( "$this_host" )
    sharemgr_host="$this_host"

    echo -n "What is the local host name for all client services? [$portal_local_hname] "
    read -e resp
    if [ ! -z "$resp" ]; then
	portal_local_hname="$resp"
	share_host=( "$resp" )
	sharemgr_host="$resp"
    fi

    if [ -z $portal_local_hname ]; then
	echo "You must specify a value for this configuration item"
    else
	ok="y"
    fi
done

ok="n"
while [ "$ok" == "n" ]; do
    echo -n "What hostname will be used for URL's to the web services? [$portal_hname] "
    read -e resp
    if [ ! -z "$resp" ]; then
	portal_hname="$resp"
    fi

    if [ -z $portal_hname ]; then
	echo "You must specify a value for this configuration item"
    else
	ok="y"
    fi
done

ok="n"
while [ "$ok" == "n" ]; do
    echo -n "What IP address should be used for web services? [$portal_ip] "
    read -e resp
    if [ ! -z "$resp" ]; then
	portal_ip="$resp"
    fi

    if [ -z $portal_ip ]; then
	echo "You must specify a value for this configuration item"
    else
	ok="y"
    fi
done

ok="n"
while [ "$ok" == "n" ]; do
    share_hname=( $this_host )
    echo -n "What hostname will be used for the non-web client services? [$share_hname] "
    read -e resp
    if [ ! -z "$resp" ]; then
	share_hname=("$resp")
    fi

    if [ ${#share_hname[*]} -eq 0 ]; then
	echo "You must specify a value for this configuration item"
    else
	ok="y"
    fi
done

ok="n"
while [ "$ok" == "n" ]; do
    echo -n "What IP address should be used for the remaining client services? [$share_ip] "
    read -e resp
    if [ ! -z "$resp" ]; then
	share_ip=( "$resp" )
    fi

    if [ ${#share_ip[*]} -eq 0 ]; then
	echo "You must specify a value for this configuration item"
    else
	ok="y"
    fi
done

echo "The remaining questions are optional"

echo -n "What port should be used for desktop/app sharing services? [$share_port] "
read -e resp
if [ ! -z "$resp" ]; then
    share_ip="$resp"
fi

echo -n "What port will be used for the share sesson manager? [$sharemgr_port] "
read -e resp
if [ ! -z "$resp" ]; then
    sharemgr_port="$resp"
fi

echo -n "Should the system send reports via e-mail when services exit unexpectedly? [y]"
read -e resp

if [ -z "$resp" ] || ["$resp" == "y"] || ["$resp" == "yes" ]; then
    echo -n "Enter a space separated list of system admin e-mail addresses: [${sysadm_email[*]}] "
    read -e -a resp
    if [ ${#resp[*]} -gt 0 ]; then
	sysadm_email=( ${resp[*]} )
    fi
else
    sysadm_email=( )
fi

save_config
