openssl req -newkey rsa:1024 -sha1 -keyout wserverkey.pem -out wserverreq.pem
openssl x509 -req -in wserverreq.pem -sha1 -extensions usr_cert -CA serverCA.pem -CAkey serverCA.pem -CAcreateserial -out wservercert.pem -days 10950
openssl rsa -in wserverkey.pem -out wserverkey.pem
cat wservercert.pem wserverkey.pem serverCAcert.pem > wserver.pem
openssl x509 -subject -issuer -noout -in wserver.pem

