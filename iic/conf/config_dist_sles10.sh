#!/bin/bash

# Environment variables needed to specify various parameters for the
# SuSE Linux Enterprise Server installation of Apache and PHP.

echo "Configuring Apache environment for SuSE Linux Enterprise Server 10"

apache_version="2.2"
apache_base=/etc/apache2
apache_exec=/usr/sbin/httpd2
apache_logdir=/var/log/apache2
apache_moddir=/usr/lib/apache2-prefork
apache_php_conf=conf.d/php5.conf
apache_doc_root=/srv/www/htdocs
apache_user=wwwrun
apache_group=www
apache_access_modname=authz_host
apache_magicfile=magic
php_version="5"
php_modname=php5
php_modfile=/usr/lib/apache2/mod_php5.so
