#!/bin/sh

. ./utils.sh
. ./global-config

this_host=`uname -n`

if [ "$db_host" != "$this_host" -a "$db_host" != "localhost" ]; then
    # this host isn't the db host; do nothing
    exit 0;
fi

# postgresql startup script is different between RedHat 9 and Enterprise Linux
if [ -f $startup_dir/postgresql ]; then
    db_startup=postgresql
else 
    if [ -f $startup_dir/rhdb ]; then
        db_startup=rhdb
    else 
        echo "Postgresql not installed on database host"
	exit -1
    fi
fi

idx=0
access=""
while [ $idx -lt ${#db_ip_addr[*]} ]; do
    access="$access host    all        all        ${db_ip_addr[$idx]}      ${db_netmask[$idx]}   trust\n"
    idx=$(($idx+1))
done
sed -e "s+@@@DB_HOST_ACCESS@@@+$access+" pg_hba.conf.in > /tmp/pg_hba.conf

if [ "$1" == "--initdb" ]; then

    adminpwd=$2
    if [ -z ${adminpwd} ]; then
        echo "No admin user password specified, aborting install"
        exit -1
    fi

# need to ssh to db_host and run the following
    $startup_dir/$db_startup stop
    rm -rf /var/lib/pgsql/data
    sudo -u postgres /usr/bin/initdb -U postgres --pgdata=/var/lib/pgsql/data
    if psql --version | grep "[[:space:]]7\." >/dev/null; then
        # TCP disabled by default in 7.x
        sudo -u postgres cp postgresql.conf /tmp/pg_hba.conf /var/lib/pgsql/data
    fi
    if [ "$sles_install" ]; then
        /sbin/chkconfig --set $db_startup 345
    else
        /sbin/chkconfig --level 345 $db_startup on
    fi
    $startup_dir/$db_startup start
    rm /tmp/pg_hba.conf
    
    echo -n "Waiting 30 sec for db to start..."
    sleep 30
    echo
    
    echo "CREATE USER " $db_user " CREATEDB;" | \
	sudo -u postgres /usr/bin/psql template1
    
    echo "CREATE DATABASE " $db_name ";"  | \
	sudo -u $db_user /usr/bin/psql template1
    
    sed \
		-e "s+@@@EXTERNAL_HNAME@@@+$external_hname+" \
        -e "s+@@@ADMINPWD@@@+$adminpwd+" \
        -e "s+@@@PROTOCOL_PREFIX@@@+$protocol_prefix+" \
	    -e "s+@@@PORTAL_HNAME@@@+$portal_hname+" \
	< database.sql.in \
	> database.sql
    
    sudo -u $db_user \
	/usr/bin/psql --dbname $db_name < database.sql
    
# some problem requires this to be done twice.  prolly
# should figure out why
    sudo -u $db_user \
	/usr/bin/psql --dbname $db_name < database.sql
    
    sed \
	-e "s+@@@EXTERNAL_HNAME@@@+$external_hname+" \
	< systemusers.sql.in \
	> systemusers.sql
    
    sudo -u $db_user \
	/usr/bin/psql --dbname $db_name < systemusers.sql
else
    sudo -u postgres cp /tmp/pg_hba.conf /var/lib/pgsql/data
    $startup_dir/$db_startup reload

    if [ -e db_upgrade.sh ]; then
	. ./db_upgrade.sh
    fi
fi
