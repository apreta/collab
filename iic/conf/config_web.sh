
. ./utils.sh
. ./global-config

function config_dist
{
    if [ -f /etc/redhat-release ]; then
	if grep -q "4\.[0-9]" /etc/redhat-release ; then
		. ./config_dist_rhel4.sh
	else	
		. ./config_dist_rhel5.sh
	fi
    else
	if [ -f /etc/SuSE-release ]; then
	    . ./config_dist_sles10.sh
	fi
    fi
}

function config_invite
{
    cp webcontent/zon-invite.conf /opt/iic/httpd/conf.d

    cp /opt/iic/locale/$locale_setting/web/invite/* /var/iic/htdocs/imidio/invite/

    sed \
	-e "s+@@@EXTAPI_PORTAL_IP@@@+$extapi_portal_ip+" \
	-e "s+@@@EXTAPI_PORTAL_PORT@@@+$extapi_portal_port+" \
	/var/iic/htdocs/imidio/invite/inc/common.inc.in \
    > /var/iic/htdocs/imidio/invite/inc/common.inc

    if [ -z $server_token ]; then 
        server_token=1
    fi

    sed \
    -e "s+@@@PROTOCOL_PREFIX@@@+$protocol_prefix+" \
	-e "s+@@@PORTAL_HNAME@@@+$portal_hname+" \
	-e "s+@@@BRAND_IMAGE@@@+$brand_image+" \
	-e "s+@@@SERVER_TOKEN@@@+$server_token+" \
	/var/iic/htdocs/imidio/invite/inc/conf.inc.in \
    > /var/iic/htdocs/imidio/invite/inc/conf.inc

    # Update the imidio/downloads dir w/ current info
    mkdir -p /var/iic/htdocs/imidio/downloads

    echo $client_version | \
	sed -e 's+\.+, +g' \
	    -e 's+$+;+' \
    > /var/iic/htdocs/imidio/downloads/iicversion.txt

    local cur_tarball="/opt/iic/conf/installers_$( echo $client_version | tr '.' '_' ).tar.gz"
    [ ! -e $cur_tarball ] && cur_tarball="/opt/iic/conf/installers.tar.gz"
    ( \
	cd /var/iic/htdocs/imidio/downloads ; \
	tar zxvf $cur_tarball
    )

    chown -R nobody:nobody /var/iic/htdocs/imidio/downloads

    echo "Meeting invitation handler configured"
}

function config_lite
{
    sed \
	-e "s+@@@SHAREMGR_HOST@@@+$sharemgr_host+" \
	-e "s+@@@SHAREMGR_RPC_PORT@@@+$sharemgr_rpc_port+" \
	-e "s+@@@SHARE_PORT@@@+$share_port+" \
	/var/iic/htdocs/imidio/lite/common.inc.in \
    > /var/iic/htdocs/imidio/lite/common.inc

    # Update the imidio/downloads dir w/ current info
    mkdir -p /var/iic/htdocs/imidio/downloads

    local cur_tarball="/opt/iic/conf/installers_$( echo $client_version | tr '.' '_' ).tar.gz"
    [ ! -e $cur_tarball ] && cur_tarball="/opt/iic/conf/installers.tar.gz"
    ( \
	cd /var/iic/htdocs/imidio/downloads ; \
	tar zxvf $cur_tarball
    )

    chown -R nobody:nobody /var/iic/htdocs/imidio/downloads

    echo "lite configured"
}

function config_console
{
    cp webcontent/zon-console.conf /opt/iic/httpd/conf.d

    local bridge_numbers
    local idx=0
    while [ $idx -lt ${#voice_phones[*]} ]; do
	local escaped_phone=$(echo ${voice_phones[$idx]} | sed -e 's+\"+\\\"+g')
	bridge_numbers="$bridge_numbers \"$escaped_phone\""
	(( idx++ ))
    done

    bridge_numbers=$(echo $bridge_numbers | sed -e 's+~+\", \"+g')

    sed \
	-e "s+@@@EXTAPI_PORTAL_IP@@@+$extapi_portal_ip+" \
	-e "s+@@@EXTAPI_PORTAL_PORT@@@+$extapi_portal_port+" \
    -e "s+@@@PROTOCOL_PREFIX@@@+$protocol_prefix+" \
	-e "s+@@@PORTAL_HNAME@@@+$portal_hname+" \
	-e "s+@@@BRIDGE_NUMBERS@@@+$bridge_numbers+" \
	/var/iic/htdocs/imidio/console/php/common.inc.in \
    > /var/iic/htdocs/imidio/console/php/common.inc

    echo "Admin console configured"
}

function config_docshare
{
    mkdir -p /var/iic/httpd/davlockdb
    chown $apache_user:$apache_group /var/iic/httpd/davlockdb

    /usr/sbin/groupadd $iic_mtg_group
    /usr/sbin/usermod -G $iic_mtg_group nobody
    /usr/sbin/usermod -G $iic_mtg_group apache
    /usr/sbin/usermod -G $iic_mtg_group $iic_user

    # not sure this is necessary
    /usr/sbin/usermod -G $apache_group $iic_user
    
    mkdir -p /var/iic/htdocs/repository
    mkdir -p /var/iic/htdocs/repository/mtgarchive
    chown -R $iic_user:$apache_group /var/iic/htdocs/repository/mtgarchive

    mkdir -p /var/iic/htdocs/repository/SharedDocuments
    chown -R $apache_user:$apache_group /var/iic/htdocs/repository/SharedDocuments
    touch /var/iic/htdocs/repository/SharedDocuments/ping.html
    chown $apache_user:$apache_group /var/iic/htdocs/repository/SharedDocuments/ping.html
    mkdir -p /var/iic/htdocs/repository/SharedDocuments/TempUpload
    chown -R $apache_user:$apache_group /var/iic/htdocs/repository/SharedDocuments/TempUpload
    mkdir -p /var/iic/htdocs/repository/user
    chown -R $apache_user:$apache_group /var/iic/htdocs/repository/user
    
    local db_pswd_action
    if [ -z "$db_pswd" ]; then
	db_pswd_action='/@@@DB_PSWD@@@/d'
    else
	db_pswd_action="s+@@@DB_PSWD@@@+$db_pswd+"
    fi

    if [ "$apache_version" == "2.2" ]; then
	auth_author_line="#AuthAuthoritative Off"
    else
	auth_author_line="AuthAuthoritative Off"
    fi
	

    if [ "$product" == "zon-lite" ]; then
      cp webcontent/zon-docshare-lite.conf /opt/iic/httpd/conf.d/zon-docshare-lite.conf

      # add password to configuration
      /usr/bin/htpasswd -cb /opt/iic/httpd/conf/dav_auth share password     
    else
      sed \
        -e "s+@@@EXTERNAL_HNAME@@@+$external_hname+" \
	-e "s+@@@DB_HOST@@@+$db_host+" \
	-e "s+@@@DB_NAME@@@+$db_name+" \
	-e "s+@@@DB_USER@@@+$db_user+" \
	-e "s+@@@AUTH_AUTHOR@@@+$auth_author_line+" \
	-e "$db_pswd_action" \
        webcontent/zon-docshare.conf.in \
      > /opt/iic/httpd/conf.d/zon-docshare.conf

   fi

    echo "Document sharing configured"
}

function config_portal
{
    local ip=$1

    if [ "$multi_ip" == "n" ]; then
        ip="*"
    fi

    mkdir -p /opt/iic/httpd/conf.d
    mkdir -p /opt/iic/httpd/conf

    cp webcontent/httpd.conf /opt/iic/httpd/conf

    cp webcontent/zon.conf /opt/iic/httpd/conf.d

    cp webcontent/groups /opt/iic/httpd/conf
    cp webcontent/passwd /opt/iic/httpd/conf

    mkdir -p /var/iic/httpd
    chown $apache_user:$apache_group /var/iic/httpd

    sed \
	-e "s+@@@PHP_MODNAME@@@+$php_modname+" \
	-e "s+@@@PHP_MODFILE@@@+$php_modfile+" \
	webcontent/zon-php.conf \
	> /opt/iic/httpd/conf.d/zon-php.conf

    if [ "$php_version" == "5" ]; then
	echo "php_flag register_long_arrays on" \
	    >> /opt/iic/httpd/conf.d/zon-php.conf
    fi

    if [ -f /opt/iic/httpd/conf/options ]; then
	rm /opt/iic/httpd/conf/options
    fi

    if [ "$enable_security" == "yes" ]; then

	sed \
	    -e "s+@@@WEB_SERVER_CERTIFICATE@@@+$web_server_certificate+" \
	    -e "s+@@@PORTAL_IP@@@+$ip+" \
	    -e "s+@@@PORTAL_HNAME@@@+$portal_hname+" \
	    -e "s+@@@MODULE_DIR@@@+$apache_moddir+" \
	    -e "s+@@@LOG_DIR@@@+$apache_logdir+" \
	    webcontent/ssl.conf \
	    > /opt/iic/httpd/conf.d/ssl.conf
	
	echo "-DSSL -f /opt/iic/httpd/conf/httpd.conf" >> /opt/iic/httpd/conf/options
    else
	echo "-f /opt/iic/httpd/conf/httpd.conf" >> /opt/iic/httpd/conf/options
	if [ -f /opt/iic/httpd/conf.d/ssl.conf ]; then
	    rm /opt/iic/httpd/conf.d/ssl.conf
	fi
    fi

    if [ "$apache_version" == "2.0" ]; then
	module_conf_file=webcontent/modules_20.conf
    else
	module_conf_file=webcontent/modules_22.conf
    fi

    sed \
	-e "s+@@@MODULE_DIR@@@+$apache_moddir+" \
	$module_conf_file \
	> /opt/iic/httpd/conf/modules.conf
 
    sed \
	-e "s+@@@PORTAL_IP@@@+$ip+" \
	-e "s+@@@SERVER_BASE@@@+$apache_base+" \
	-e "s+@@@PHP_CONF@@@+$apache_php_conf+" \
	-e "s+@@@APACHE_USER@@@+$apache_user+" \
	-e "s+@@@APACHE_GROUP@@@+$apache_group+" \
	-e "s+@@@LOG_DIR@@@+$apache_logdir+" \
	-e "s+@@@APACHE_MIME_MAGIC@@@+$apache_magicfile+" \
	/opt/iic/httpd/conf/httpd.conf \
    > /tmp/httpd.conf

    mv /tmp/httpd.conf /opt/iic/httpd/conf/httpd.conf

    if [ "$product" == "zon-lite" ]; then
	config_docshare
	config_lite
    else
	config_docshare
	config_invite
	config_console
	config_nagios
    fi

#    sed \
#	-e "s+@@@ROTATION_SIZE@@@+$rotation_size+" \
#	-e "s+@@@HISTORY_LEN@@@+$history_len+" \
#	-e "s+@@@LOG_DIR@@@+$apache_logdir+" \
#	zonportal.in \
#    > /etc/logrotate.d/zonportal

    sed \
	-e "s+@@@APACHE_EXEC@@@+$apache_exec+" \
	/opt/iic/conf/init.d/iicportal \
	> /tmp/iicportal
    mv /tmp/iicportal /opt/iic/conf/init.d/iicportal
    
    echo "web portal configured"

    install_init_script iicportal

    echo "Installed portal init.d script"
}

function config_nagios
{
    if [ -e /usr/local/nagios ] ; then
	cp webcontent/zon-nagios.conf /opt/iic/httpd/conf.d
	echo "Configured nagios"
    fi
}

function config_extapi_portal
{
    local listen_ip=$extapi_portal_ip
    if [ "$multi_ip" == "n" ]; then
        listen_ip="*"
    fi

    cp webcontent/httpd-extapi.conf /opt/iic/httpd/conf
    sed \
	-e "s+@@@EXTAPI_PORTAL_IP@@@+$listen_ip+" \
	-e "s+@@@EXTAPI_PORTAL_PORT@@@+$extapi_portal_port+" \
	-e "s+@@@SERVER_BASE@@@+$apache_base+" \
	-e "s+@@@MODULE_DIR@@@+$apache_moddir+" \
	-e "s+@@@PHP_CONF@@@+$apache_php_conf+" \
	-e "s+@@@APACHE_USER@@@+$apache_user+" \
	-e "s+@@@APACHE_GROUP@@@+$apache_group+" \
	-e "s+@@@LOG_DIR@@@+$apache_logdir+" \
	-e "s+@@@ACCESS_MODNAME@@@+$apache_access_modname+g" \
	/opt/iic/httpd/conf/httpd-extapi.conf \
    > /tmp/httpd-extapi.conf
    
    mv /tmp/httpd-extapi.conf /opt/iic/httpd/conf/httpd-extapi.conf
    cp webcontent/zon-extapi.conf /opt/iic/httpd/conf

    mkdir -p /var/iic/htdocs/imidio/api
    chown nobody:nobody /var/iic/htdocs/imidio/api

    cp sysconfig/iicextapi /etc/sysconfig


    sed \
	-e "s+@@@APACHE_EXEC@@@+$apache_exec+" \
	/opt/iic/conf/init.d/iicextapi \
	> /tmp/iicextapi
    mv /tmp/iicextapi /opt/iic/conf/init.d/iicextapi

    install_init_script iicextapi

#    sed \
#	-e "s+@@@ROTATION_SIZE@@@+$rotation_size+" \
#	-e "s+@@@HISTORY_LEN@@@+$history_len+" \
#	-e "s+@@@LOG_DIR@@@+$apache_logdir+" \
#	zonextapi.in \
#    > /etc/logrotate.d/zonextapi

    echo "Installed extapi portal init.d script"
}

this_host=`uname -n`

config_dist

idx=0
for host in ${portal_local_hname[*]} ; do
    if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
	config_portal ${portal_ip[$idx]}
    fi
    idx=$((idx+1))
done

idx=0
for host in $extapi_portal_local_hname ; do
    if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
	config_extapi_portal
    fi
done
