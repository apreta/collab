
if [ -f /etc/SuSE-release ]; then
	sles_install="y"
else
	sles_install=""
fi

if [ -d /etc/rc.d/init.d ]; then
	startup_dir=/etc/rc.d/init.d
else
	startup_dir=/etc/rc.d
fi

function echo_bash_literal
{
    echo -n "'"
    echo -n "$1" | sed -e "s+'+'\"'\"'+"
    echo -n "'"
}

function escape_xml
{
    strval=$(echo "$1" | sed -e 's/&/\&amp;/g' | sed -e 's/</\&lt;/g' -e 's/>/\&gt;/g')
}

function unescape_ldap_part
{
    local part=${1//=20/ }
    part=${part//=3B/;}
    strval=${part//=3D/=}
}

function escape_ldap_part
{
    local part=${1//=/=3D}
    part=${part//;/=3B}
    strval=${part// /=20}
}

function explode_ldap_string
{
    local parts=( $(echo $1 | tr ';' ' ' ) )

    unescape_ldap_part ${parts[0]}
    ldap_url=$strval

    unescape_ldap_part ${parts[1]}
    ldap_binddn=$strval

    unescape_ldap_part ${parts[2]}
    ldap_basedn=$strval

    unescape_ldap_part ${parts[3]}
    ldap_import_filter=$strval

    unescape_ldap_part ${parts[4]}
    ldap_auth_filter=$strval

    unescape_ldap_part ${parts[5]}
    ldap_community=$strval

    unescape_ldap_part ${parts[6]}
    ldap_bindpw=$strval
}

function build_ldap_auth_xml
{
    explode_ldap_string $1

    escape_xml $ldap_bindpw

    strval="\
<server>\
<url>$ldap_url</url>\
<binddn>$ldap_binddn</binddn>\
<bindpassword>$strval</bindpassword>\
<basedn>$ldap_basedn</basedn>\
<filter>$ldap_auth_filter</filter>\
<communityid>$ldap_community</communityid>\
</server>"
}

function save_config
{
    mv global-config config.sv
    echo "## See config.defaults for descriptions of these values" > global-config
    echo "config_version=1" >> global-config
    echo "product=\"$product\"" >> global-config
    echo "license_key=$license_key" >> global-config
    echo "brand_image=$brand_image" >> global-config
    echo "client_version=\"$client_version\"" >> global-config
    echo "iic_user=$iic_user" >> global-config
    echo "iic_mtg_group=$iic_mtg_group" >> global-config
    echo "iic_home=$iic_home" >> global-config
    echo "jabber_home=$jabber_home" >> global-config
    echo "db_host=$db_host" >> global-config
    echo "db_name=$db_name" >> global-config
    echo "db_user=$db_user" >> global-config
    echo "db_pswd=$db_pswd" >> global-config
    echo "db_ip_addr=$db_ip_addr" >> global-config
    echo "db_netmask=$db_netmask" >> global-config
    echo "svc_base_port=$svc_base_port" >> global-config
    echo "multi_ip=$multi_ip" >> global-config
    echo "external_hname=$external_hname" >> global-config
    echo "portal_hname=$portal_hname" >> global-config
    echo "portal_ip=( ${portal_ip[*]} )" >> global-config
    echo "portal_local_hname=( ${portal_local_hname[*]} )" >> global-config
    echo "extapi_portal_hname=$extapi_portal_hname" >> global-config
    echo "extapi_portal_local_hname=$extapi_portal_local_hname" >> global-config
    echo "extapi_portal_port=$extapi_portal_port" >> global-config
    echo "extapi_portal_ip=$extapi_portal_ip" >> global-config
    echo "service_secret=\"$service_secret\"" >> global-config
    echo "session_creation_secret=\"$session_creation_secret\"" >> global-config
    echo "lcl_xmlrouter=( ${lcl_xmlrouter[*]} )" >> global-config
    echo "lcl_xmlrouter_ip=( ${lcl_xmlrouter_ip[*]} )" >> global-config
    echo "iptables_port_forwarding=$iptables_port_forwarding" >> global-config
    echo "iic_client_port=$iic_client_port"  >> global-config
    echo "jadc2s_instances=$jadc2s_instances" >> global-config
    echo "jadc2s_port=( ${jadc2s_port[*]} )" >> global-config
    echo "extportal_instances=$extportal_instances" >> global-config
    echo "extportal_port=( ${extportal_port[*]} )" >> global-config
    echo "controller_host=( ${controller_host[*]} )" >> global-config
    echo "controller_port=( ${controller_port[*]} )" >> global-config
    echo "voice_host=( ${voice_host[*]} )" >> global-config
    echo "voice_port=( ${voice_port[*]} )" >> global-config
    echo "multi_phone_bridges=$multi_phone_bridges" >> global-config
    echo -n "voice_phones=( " >> global-config
    local idx=0
    while [ $idx -lt ${#voice_phones[*]} ]; do

	echo -n "\"" >> global-config
	# escape double quotes
	echo -n $( echo ${voice_phones[$idx]} | sed -e 's+"+\\"+g' ) \
	    >> global-config
	echo -n "\" " >> global-config

	idx=$(($idx+1))
    done
    echo ")" >> global-config
    echo "mtgarchiver_host=( ${mtgarchiver_host[*]} )" >> global-config
    echo "mtgarchiver_voice_host=( ${mtgarchiver_voice_host[*]} )" >> global-config
    echo "mtgarchiver_prio=$mtgarchiver_prio" >> global-config
    echo "mtgarchiver_port=( ${mtgarchiver_port[*]} )" >> global-config
    echo "addressbk_host=( ${addressbk_host[*]} )" >> global-config
    echo "addressbk_port=( ${addressbk_port[*]} )" >> global-config
    echo "mailer_host=( ${mailer_host[*]} )" >> global-config
    echo "mailer_port=( ${mailer_port[*]} )" >> global-config
    echo "converter_host=( ${converter_host[*]} )" >> global-config
    echo "converter_port=( ${converter_port[*]} )" >> global-config
    echo "extapi_host=( ${extapi_host[*]} )" >> global-config
    echo "extapi_port=( ${extapi_port[*]} )" >> global-config
    echo "share_host_equal=$share_host_equal" >> global-config
    echo "share_local_host=( ${share_local_host[*]} )" >> global-config
    echo "share_host=( ${share_host[*]} )" >> global-config
    echo "share_ip=( ${share_ip[*]} )" >> global-config
    echo "sharemgr_host=$sharemgr_host" >> global-config
    echo "sharemgr_port=$sharemgr_port" >> global-config
    echo "sharemgr_control_port=$sharemgr_control_port" >> global-config
    echo "sharemgr_rpc_port=$sharemgr_rpc_port" >> global-config
    echo "share_port=$share_port" >> global-config
    echo "voice_provider=\"$voice_provider\"" >> global-config
    echo "wav_port=$wav_port" >> global-config
    echo "locale_setting=$locale_setting" >> global-config
	echo "iic_prodname=\"$iic_prodname\"" >> global-config
    echo "new_user_from_address=$new_user_from_address" >> global-config
    echo "new_user_from_display=\"$new_user_from_display\"" >> global-config
    echo "new_user_subject=\"$new_user_subject\"" >> global-config
    echo "smtp_host=$smtp_host" >> global-config
    echo "smtp_user=$smtp_user" >> global-config
    echo "smtp_pswd=\"$smtp_pswd\"" >> global-config
    echo "default_from_address=$default_from_address" >> global-config
    echo "default_from_display=\"$default_from_display\"" >> global-config
    echo "invite_from_address=$invite_from_address" >> global-config
    echo "invite_from_display=\"$invite_from_display\"" >> global-config
    echo "invite_subject=\"$invite_subject\"" >> global-config
    echo "mtgarchiver_from_address=$mtgarchiver_from_address" >> global-config
    echo "mtgarchiver_from_display=\"$mtgarchiver_from_display\"" >> global-config
    echo "mtgarchiver_subject=\"$mtgarchiver_subject\"" >> global-config
    echo "event_rotation_time=\"$event_rotation_time\"" >> global-config
    echo "sysadm_email=(${sysadm_email[*]})" >> global-config
    echo "db_backup_dir=$db_backup_dir" >> global-config
    echo "db_backup_history=$db_backup_history" >> global-config
    echo "db_backup_hr=$db_backup_hr" >> global-config
    echo "db_backup_min=$db_backup_min" >> global-config

    echo "mtgarchive_cleaning_threshold=$mtgarchive_cleaning_threshold" >> global-config
    echo "mtgarchive_target_sz=$mtgarchive_target_sz" >> global-config
    echo "mtgarchive_cleanup_hr=$mtgarchive_cleanup_hr" >> global-config
    echo "mtgarchive_cleanup_min=$mtgarchive_cleanup_min" >> global-config
    echo "mtgarchive_cleanup_action=$mtgarchive_cleanup_action" >> global-config
    echo "mtgarchive_cleanup_min_age=$mtgarchive_cleanup_min_age" >> global-config
    echo "mtgarchive_trash_folder=$mtgarchive_trash_folder" >> global-config

    echo "docshare_cleaning_threshold=$mtgarchive_cleaning_threshold" >> global-config
    echo "docshare_target_sz=$docshare_target_sz" >> global-config
    echo "docshare_cleanup_hr=$docshare_cleanup_hr" >> global-config
    echo "docshare_cleanup_min=$docshare_cleanup_min" >> global-config
    echo "docshare_cleanup_action=$docshare_cleanup_action" >> global-config
    echo "docshare_cleanup_min_age=$docshare_cleanup_min_age" >> global-config
    echo "docshare_trash_folder=$docshare_trash_folder" >> global-config

    echo "rotation_size=$rotation_size" >> global-config
    echo "history_len=$history_len" >> global-config
    echo "log_level=$log_level" >> global-config

    [ -z "$enable_rt_logs" ] && enable_rt_logs="no"

    echo "enable_rt_logs=$enable_rt_logs" >> global-config
    echo "rt_user_port=$rt_user_port" >> global-config
    echo "rt_call_port=$rt_call_port" >> global-config
    echo "rt_rsrv_port=$rt_rsrv_port" >> global-config

    [ -z "$log_chat" ] && log_chat="no"

    echo "log_chat=$log_chat" >> global-config
    echo "log_chat_clean_action=$log_chat_clean_action" >> global-config
    echo "log_chat_clean_sz=$log_chat_clean_sz" >> global-config
    echo "log_chat_trash_folder=$log_chat_trash_folder" >> global-config

    echo "ha_primary=$ha_primary" >> global-config
    echo "ha_backup=$ha_backup" >> global-config
    echo "install_ha=$install_ha" >> global-config

    echo "enable_security=$enable_security" >> global-config
    echo "protocol_prefix=$protocol_prefix" >> global-config
    echo "server_certificate=$server_certificate" >> global-config
    echo "web_server_certificate=$web_server_certificate" >> global-config

    [ -z "$mtgarchiver_max_retries" ] && mtgarchiver_max_retries=3
    [ -z "$mtgarchiver_retry_wait" ] && mtgarchiver_retry_wait=30
    echo "mtgarchiver_max_retries=$mtgarchiver_max_retries" >> global-config
    echo "mtgarchiver_retry_wait=$mtgarchiver_retry_wait" >> global-config

    echo "voip_pcphone=$voip_pcphone" >> global-config
    echo "voip_ip=( ${voip_ip[*]} )" >> global-config
    echo "voip_host=( ${voip_host[*]} )" >> global-config
    echo "nms_base_ip=( ${nms_base_ip[*]} )" >> global-config

    echo "dial_prefix=$dial_prefix" >> global-config
    echo "board_pstn_trunks=$board_pstn_trunks" >> global-config
    echo "board_max_calls=$board_max_calls" >> global-config
    echo "board_num_recorders=$board_num_recorders" >> global-config
    echo "pstn_line_protocol=$pstn_line_protocol" >> global-config

    echo "ast_main_name=( ${ast_main_name[*]} )" >> global-config
    echo "ast_main_addr=( ${ast_main_addr[*]} )" >> global-config
    echo "ast_mgr_port=( ${ast_mgr_port[*]} )" >> global-config
    echo "ast_main_username=( ${ast_main_username[*]} )" >> global-config
    echo "ast_main_secret=( ${ast_main_secret[*]} )" >> global-config
    echo "ast_main_max=( ${ast_main_max[*]} )" >> global-config

    echo "sip_default_domain=$sip_default_domain" >> global-config
    echo "sip_outbound_proxy=$sip_outbound_proxy" >> global-config
    echo "isdn_operator=$isdn_operator" >> global-config
    echo "isdn_country=$isdn_country" >> global-config

    echo "enable_ldap_sync=$enable_ldap_sync" >> global-config
    echo -n "ldap_servers=( " >> global-config
    local server
    for server in ${ldap_servers[@]}; do
	echo_bash_literal "$server" >> global-config
	echo -n " " >> global-config
    done
    echo ")"  >> global-config
    echo "ldap_on_schedule=$ldap_on_schedule" >> global-config
    echo "ldap_sync_hour=$ldap_sync_hour" >> global-config
    echo "locale_setting=$locale_setting" >> global-config
}

function install_init_script
{
    local sname=$1

    cp init.d/$sname $startup_dir/$sname
    chmod +x $startup_dir/$sname
    /sbin/chkconfig --add $sname
    if [ "$sles_install" ]; then
        /sbin/chkconfig --set $sname 345
    else
        /sbin/chkconfig --levels 345 $sname on
    fi
}
