. ./utils.sh
. ./global-config

this_host=`uname -n`

if [ "$db_host" == "$this_host" -o "$db_host" == "localhost" ]; then
    # configure database backups
    # Create the backup dir if it doesn't already exist (e.g. already NFS
    # mounted)
    # Make sure it is owned by the $db_user (if NFS mounted, make sure
    #    the remote filesystem is owned by the $db_user)
    # Protect it so that only the db_user can access it

    mkdir -p $db_backup_dir
    chown -R $db_user $db_backup_dir
    chmod 700  $db_backup_dir
    crontab -u $db_user -l | \
	grep -v /opt/iic/bin/db-backup.sh | \
	grep -v '^#' \
	> /tmp/$db_user-crontab
    
    echo "$db_backup_min $db_backup_hr * * * /opt/iic/bin/db-backup.sh" \
	>> /tmp/$db_user-crontab
    
    crontab -u $db_user /tmp/$db_user-crontab
    
    rm -f /tmp/$db_user-crontab
fi

host=${addressbk_host}
if [ "$ldap_on_schedule" == "yes" ]; then
  if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
    crontab -u $iic_user -l | \
	grep -v ldap-sync.sh | \
	grep -v '^#' \
	> /tmp/$iic_user-crontab
    echo "0 $ldap_sync_hour * * * cd /opt/iic/bin; ./ldap-sync.sh ${portal_ip}" >> /tmp/$iic_user-crontab
    crontab -u $iic_user /tmp/$iic_user-crontab
  fi
fi

# mtg archivers use shared storage; just pick the first host
# to handle maintenance
host=${mtgarchiver_host[0]}
if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
    # List the iic user's crontab and strip out
    # comments and instances of this cleanup script
    # Append this cleanup script.
    crontab -u $iic_user -l | \
	grep -v /opt/iic/bin/maintain-archives.sh | \
	grep -v '^#' \
	> /tmp/$iic_user-crontab
    echo "$mtgarchive_cleanup_min $mtgarchive_cleanup_hr * * * /opt/iic/bin/maintain-archives.sh" >> /tmp/$iic_user-crontab
    crontab -u $iic_user /tmp/$iic_user-crontab
fi

host=${portal_local_hname[0]}
if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
    crontab -u root -l | \
	grep -v /opt/iic/bin/maintain-docshare.sh | \
	grep -v '^#' \
	> /tmp/root-crontab
    echo "$mtgarchive_cleanup_min $mtgarchive_cleanup_hr * * * /opt/iic/bin/maintain-docshare.sh" >> /tmp/root-crontab
    crontab -u root /tmp/root-crontab
fi

if [ "$voice_provider" == "nms" -o "$voice_provider" == "ext" -o "$voice_provider" == "appconf" ]; then
    for host in ${voice_host[*]} ; do
	if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
	    # List the iic user's crontab and strip out
	    # comments and instances of this cleanup script
	    # Append this cleanup script.
	    crontab -u root -l | \
		grep -v /opt/iic/bin/clean-bridge-recordings.sh | \
		grep -v '^#' \
		> /tmp/root-crontab
	    echo "45 3 * * * /opt/iic/bin/clean-bridge-recordings.sh" \
		>> /tmp/root-crontab
	    crontab -u root /tmp/root-crontab
	fi
    done
fi

for host in ${share_local_host[*]} ; do
    if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
	# List the iic user's crontab and strip out
	# comments and instances of this cleanup script
	# Append this cleanup script.
	crontab -u $iic_user -l | \
	    grep -v /opt/iic/bin/clean-appshare-recordings.sh | \
	    grep -v '^#' \
	    > /tmp/$iic_user-crontab
	echo "45 3 * * * /opt/iic/bin/clean-appshare-recordings.sh" \
	    >> /tmp/$iic_user-crontab
	crontab -u $iic_user /tmp/$iic_user-crontab
    fi
done

for host in ${mtgarchiver_host[*]} ; do
    if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
	# List the iic user's crontab and strip out
	# comments and instances of this cleanup script.
	# Append this cleanup script.
	crontab -u $iic_user -l | \
	    grep -v /opt/iic/bin/clean-mtgspool.sh | \
	    grep -v '^#' \
	    > /tmp/$iic_user-crontab
	echo "45 3 * * * /opt/iic/bin/clean-mtgspool.sh" \
	    >> /tmp/$iic_user-crontab
	crontab -u $iic_user /tmp/$iic_user-crontab
    fi
done

for host in ${controller_host[*]} ${addressbk_host[*]}; do
    if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
	# List the iic user's crontab and strip out
	# comments and instances of this cleanup script.
	# Append this cleanup script.
	crontab -u $iic_user -l | \
	    grep -v /opt/iic/bin/clean-cdr-records.sh | \
	    grep -v '^#' \
	    > /tmp/$iic_user-crontab
	echo "45 3 * * * /opt/iic/bin/clean-cdr-records.sh" \
	    >> /tmp/$iic_user-crontab
	crontab -u $iic_user /tmp/$iic_user-crontab
    fi
done

if [ "$log_chat" == "yes" ]; then
    for host in $( echo ${controller_host[*]} ${xmlrouter_host[*]} | sort -u ); do
	if [ "$host" == "$this_host" -o "$host" == "localhost" ]; then
	    crontab -u $iic_user -l | \
		grep -v /opt/iic/bin/maintain-chatlog.sh | \
		grep -v '^#' \
		> /tmp/$iic_user-crontab
	    echo "0 4 * * * /opt/iic/bin/maintain-chatlog.sh" >> /tmp/$iic_user-crontab
	    crontab -u $iic_user /tmp/$iic_user-crontab
	fi
    done
else
    # remove it if it exists
    crontab -u $iic_user -l | \
	grep -v /opt/iic/bin/maintain-chatlog.sh | \
	grep -v '^#' \
	> /tmp/$iic_user-crontab
    crontab -u $iic_user /tmp/$iic_user-crontab
fi
