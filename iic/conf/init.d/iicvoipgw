#!/bin/bash
#
# chkconfig: 345 99 99
# description: Controls startup/shutdown 
#
### BEGIN INIT INFO
# Provides:       iicvoipgw
# Required-Start: $network
# Required-Stop:
# Default-Start:  3 5
# Default-Stop:
# Description:    Conferencing server VoIP gateway
### END INIT INFO

# Source function library
. /etc/init.d/iicutils

if [ ! -f /etc/sysconfig/imidio_iic ]; then
    exit 0
fi

. /etc/sysconfig/imidio_iic

voipgwd=$IIC_HOME/bin/iaxgwd
prog="Conferencing VoIP gateway"
progd=iaxgwd
# 3 for standard error, 7 for debug
debugLevel=7  

start() {
    echo -n $"Starting $prog: "
    if not_running $progd; then
	$IIC_HOME/bin/nanny $NANNY_ARGS --user root --childpid  \
		${voipgwd} ${debugLevel} $VOIP_ADDR $VOIPGW_ARGS 
	RETVAL=$?
	[ $RETVAL -eq 0 ] && echo_success || echo_failure
	echo
	return $RETVAL
    else
	echo_success
        echo
        return 0
    fi
}

stop() {
    echo -n $"Stopping $prog: "

    killproc $progd -HUP
    RETVAL=$?
    echo
    [ $RETVAL = 0 ]
    return $RETVAL
}

status() {
    echo -n $"Status of $prog: "
    if is_running --checkchild $progd; then
        echo_status $"RUNNING"
    else
        echo_status $"NOT RUNNING"
        RETVAL=1
    fi
}

case "$1" in
    start)
	start
	;;
    stop)
	stop
	;;
    status)
	status
	;;
    *)
	echo $"Usage: $prog {start|stop|status}"
	exit 1
esac

exit $RETVAL
