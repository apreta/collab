#!/bin/bash
#
# Startup script for the Apache Web Server
#
# chkconfig: - 85 15
# description: Apache is a World Wide Web server.  It is used to serve \
#	       HTML files and CGI.
# processname: iicportal
# pidfile: /var/run/httpd.pid
# config: /opt/iic/httpd/conf/httpd.conf
#
### BEGIN INIT INFO
# Provides:       iicportal
# Required-Start: $network 
# Required-Stop:
# Default-Start:  3 5
# Default-Stop:
# Description:    Start the Conferencing portal web server
### END INIT INFO


OPTIONS=$(cat /opt/iic/httpd/conf/options)

# Source function library.
. /etc/init.d/iicutils

if [ -f /etc/sysconfig/iicportal ]; then
        . /etc/sysconfig/iicportal
fi

# This will prevent initlog from swallowing up a pass-phrase prompt if
# mod_ssl needs a pass-phrase from the user.
INITLOG_ARGS=""

# Set HTTPD=/usr/sbin/httpd.worker in /etc/sysconfig/httpd to use a server
# with the thread-based "worker" MPM; BE WARNED that some modules may not
# work correctly with a thread-based MPM; notably PHP will refuse to start.

# Path to the apachectl script, server binary, and short-form for messages.
apachectl=/usr/sbin/apachectl
httpd=${HTTPD-@@@APACHE_EXEC@@@}
prog="Conferencing web portal"
progd="portal"
RETVAL=0

# check for 1.3 configuration
check13 () {
	CONFFILE=/opt/iic/httpd/conf/httpd.conf
	GONE="(ServerType|BindAddress|Port|AddModule|ClearModuleList|"
	GONE="${GONE}AgentLog|RefererLog|RefererIgnore|FancyIndexing|"
	GONE="${GONE}AccessConfig|ResourceConfig)"
	if grep -Eiq "^[[:space:]]*($GONE)" $CONFFILE; then
		echo
		echo 1>&2 " Apache 1.3 configuration directives found"
		echo 1>&2 " please read /usr/share/doc/httpd-2.0.40/migration.html"
		failure "Apache 1.3 config directives test"
		echo
		exit 1
	fi
}

# The semantics of these two functions differ from the way apachectl does
# things -- attempting to start while running is a failure, and shutdown
# when not running is also a failure.  So we just do it the way init scripts
# are expected to behave here.
start() {
    echo -n $"Starting $prog: "
    if not_running $progd; then
	/opt/iic/bin/nanny --user root --termsig 15 --progd $progd --pidfn /var/run/httpd.pid --daemon $httpd $OPTIONS
	RETVAL=$?
	[ $RETVAL -eq 0 ] && echo_success || echo_failure
	echo
	return $RETVAL
    else
	echo_success
	echo
	return 0
    fi
}

stop() {
    echo -n $"Stopping $prog: "

    killproc $progd -HUP
    RETVAL=$?
    echo
    [ $RETVAL = 0 ]
    return $RETVAL
}

# See how we were called.
case "$1" in
    start)
	start
	;;

    stop)
	stop
	;;

    status)
	echo -n $"Status of $prog: "
	if is_running $progd; then
	    echo_status $"RUNNING"
	else
	    echo_status $"NOT RUNNING"
	fi
	;;

    *)
	echo $"Usage: $prog {start|stop|status}"
	exit 1
esac

exit $RETVAL
