#!/bin/bash
#
# chkconfig: 345 99 99
# description: Controls startup/shutdown 
#
### BEGIN INIT INFO
# Provides:       iicshare
# Required-Start: $network
# Required-Stop:
# Default-Start:  3 5
# Default-Stop:
# Description:    Conferencing server desktop share service
### END INIT INFO

# Source function library
. /etc/init.d/iicutils

if [ ! -f /etc/sysconfig/imidio_iic ]; then
    exit 0
fi

. /etc/sysconfig/imidio_iic

if [ -f /etc/sysconfig/iicshare ]; then
   . /etc/sysconfig/iicshare
fi

prog="Conferencing share"
progd=reflectord
reflectord=$IIC_HOME/bin/reflectord

start()
{
    echo -n $"Starting $prog: "

    if [ -f /etc/sysconfig/iicforward-share ]; then
      . /etc/sysconfig/iicforward-share >/dev/null
    fi

    if not_running $progd; then
	local idx=0
	local clearpids="--clearpids"

	for ip in $SHARE_IP; do
	    # -b $ip not yet supported
	    $IIC_HOME/bin/nanny $NANNY_ARGS --childpid $clearpids \
		${reflectord} \
		-v 4 -T 9 -z 6 -n 24 -m 255 -f -1 \
		-l $SHARE_PORT \
		-u $(($SHARE_PORT+1)) \
		-g /var/log/iic/reflectord.$ip.log \
		-p /opt/iic/conf/passwd \
		-q /opt/iic/conf/hosts \
		-s /var/iic/appshare-recording/ \
		$OPTIONS
	    # OPTIONS above are set in /etc/sysconfig/iicshare
	    RETVAL=$?
	    [ $RETVAL -ne 0 ] && echo_failure && echo && return $RETVAL
	    clearpids=""
	    idx=$((idx+1))
	done
	echo_success
	echo
    else
	echo_success
	echo
        return 0
    fi
}

stop() {
    echo -n $"Stopping $prog: "

    killproc $progd -HUP
    RETVAL=$?
    echo
    [ $RETVAL = 0 ]
    return $RETVAL
}

status() {
    echo -n $"Status of $prog: "
    if is_running --checkchild $progd; then
        echo_status $"RUNNING"
    else
        RETVAL=1
        echo_status $"NOT RUNNING"
    fi
}

case "$1" in
    start)
	start
	;;
    stop)
	stop
	;;
    status)
	status
	;;
    *)
	echo $"Usage: $prog {start|stop|status}"
	exit 1
esac

exit $RETVAL
