
. ./utils.sh
. ./global-config

function config_ha
{

    sed \
	-e "s+@@@HA_PRIMARY@@@+$ha_primary+" \
	-e "s+@@@HA_BACKUP@@@+$ha_backup+" \
	-e "s+@@@LCL_XMLROUTER_IP@@@+${lcl_xmlrouter_ip[0]}+" \
	-e "s+@@@SHARE_IP@@@+${share_ip[0]}+" \
	-e "s+@@@PORTAL_IP@@@+${portal_ip[0]}+" \
	/var/lib/heartbeat/crm/cib.xml.in \
    > /var/lib/heartbeat/crm/cib.xml

    sed \
	-e "s+@@@HA_PRIMARY@@@+$ha_primary+" \
	-e "s+@@@HA_BACKUP@@@+$ha_backup+" \
	/etc/ha.d/ha.cf.in \
    > /etc/ha.d/ha.cf

	# heartbeat won't start if these files are present from earlier install
    rm -f /var/lib/heartbeat/crm/cib.xml.last
    rm -f /var/lib/heartbeat/crm/cib.xml.sig
    rm -f /var/lib/heartbeat/crm/cib.xml.sig.last
	
	
    # find original heartbeat service & disable
    /sbin/chkconfig --levels 345 heartbeat off

    # create "iic" heartbeat service (so other scripts can find and start/stop)
    cp $startup_dir/heartbeat $startup_dir/iicheartbeat
    /sbin/chkconfig --levels 345 iicheartbeat on

    chmod 600 /etc/ha.d/authkeys
    chown -R hacluster /var/lib/heartbeat

    echo "Heartbeat configured"
}

function remove_service_startups
{
    rm -f /etc/rc.d/rc?.d/*iicaddress
    rm -f /etc/rc.d/rc?.d/*iiccontroller
    rm -f /etc/rc.d/rc?.d/*iicextapi
    rm -f /etc/rc.d/rc?.d/*iichttpd
    rm -f /etc/rc.d/rc?.d/*iicjabber
    rm -f /etc/rc.d/rc?.d/*iicjadc2s
    rm -f /etc/rc.d/rc?.d/*iicß
    rm -f /etc/rc.d/rc?.d/*iicmtgarchiver
    rm -f /etc/rc.d/rc?.d/*iicpgpid
    rm -f /etc/rc.d/rc?.d/*iicportal
    rm -f /etc/rc.d/rc?.d/*iicproxy
    rm -f /etc/rc.d/rc?.d/*iicshare
    rm -f /etc/rc.d/rc?.d/*iictransmitter
    rm -f /etc/rc.d/rc?.d/*iicsharemgr
    rm -f /etc/rc.d/rc?.d/*iicvoice
    rm -f /etc/rc.d/rc?.d/*iiconverter
}

this_host=`uname -n`

config_ha
remove_service_startups
