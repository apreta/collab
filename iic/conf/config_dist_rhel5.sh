#!/bin/bash

# Environment variables needed to specify various parameters for the
# Red Hat Enterprise Linux installation of Apache and PHP.

echo "Configuring Apache environment for Red Hat Enterprise Linux 5"

apache_version="2.2"
apache_base=/etc/httpd
apache_exec=/usr/sbin/httpd
apache_logdir=/var/log/httpd
apache_moddir=modules
apache_php_conf=conf.d/php.conf
apache_doc_root=/var/www/html
apache_user=apache
apache_group=apache
apache_access_modname=authz_host
apache_magicfile=conf/magic
php_version="5"
php_modname=php5
php_modfile=modules/libphp5.so
