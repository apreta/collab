/* VNC Reflector
 * Copyright (C) 2001-2003 HorizonLive.com, Inc.  All rights reserved.
 *
 * This software is released under the terms specified in the file LICENSE,
 * included.  HorizonLive provides e-Learning and collaborative synchronous
 * presentation solutions in a totally Web-based environment.  For more
 * information about HorizonLive, please see our website at
 * http://www.horizonlive.com.
 *
 * This software was authored by Constantin Kaplinsky <const@ce.cctpu.edu.ru>
 * and sponsored by HorizonLive.com, Inc.
 *
 * $Id: fbs_files.c,v 1.9 2005-03-01 21:58:01 binl Exp $
 * Saving "framebuffer streams" in files.
 */
/*
 * Code heavily modified by imidio, SiteScape, and Novell for ICEcore/Kablink conferencing.
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2011 Apreta.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifdef LINUX
#include <sys/time.h>
#include <sys/types.h>
#else
#include <winsock2.h>
#endif
#include <time.h>

#include "rfblib.h"
#include "reflector.h"
#include "logging.h"
#include "port.h"
#include "async_io.h"
#include "host_io.h"

static char *s_fbs_prefix = NULL;
static int s_join_sessions;

void fbs_set_prefix(char *fbs_prefix, int join_sessions)
{
  s_fbs_prefix = fbs_prefix;
  s_join_sessions = join_sessions;
}

void fbs_open_file()
{
  HOST_SLOT *host_slot = (HOST_SLOT*)cur_slot;

  int max_rect_size, w, h;
  CARD32 len;
  char fname[512];
  char fbs_header[256];
  char fbs_desktop_name[] = "RFB session archived by IIC Reflector";
  CARD8 fbs_newfbsize_msg[16] = {
    0, 0, 0, 1,                 /* msg header */
    0, 0, 0, 0, 0, 0, 0, 0,     /* x, y, w, h */
    0, 0, 0, 0                  /* encoding   */
  };
  struct tm *tm_start = localtime(&host_slot->start_time);
  char startstr[21];
  CARD32 recordtime = time(NULL);

  if (s_fbs_prefix == NULL)	// use this for spooling
    return;

  if (host_slot->fbs_fp)	// file is already opened
	  return;

  /* Prepare file name optionally suffixed with session number */
  len = strlen(s_fbs_prefix);
  if (len + 4 > 255) {
    log_write(LL_WARN, "FBS filename prefix too long");
    s_fbs_prefix = NULL;
    return;
  }
  strcpy(fname, s_fbs_prefix);
  strftime(startstr, 21, "-%Y-%m-%d-%H-%M-%S", tm_start);

  strcat(fname, cur_slot->id);
  strcat(fname, startstr);
  strcat(fname, ".dump");
  host_slot->fbs_filename = malloc(strlen(fname)+1);
  strcpy(host_slot->fbs_filename, fname);

  host_slot->fbs_fp = fopen(fname, "r");
  if (host_slot->fbs_fp == NULL) {	// new recording

    /* Open the file */
    host_slot->fbs_fp = fopen(fname, "w");
    if (host_slot->fbs_fp == NULL) {
      log_write(LL_WARN, "Could not open FBS file for writing");
      return;
    }
    log_write(LL_MSG, "Opened FBS file for writing: %s(%d)", fname, recordtime);

    /* Remember current time */
    _gettimeofday(&host_slot->fbs_start_time);

    /* Write file header */
    if (fwrite("FBS 001.000\n", 1, 12, host_slot->fbs_fp) != 12) {
      log_write(LL_WARN, "Could not write FBS file header");
      fclose(host_slot->fbs_fp);
      host_slot->fbs_fp = NULL;
      return;
    }

    /* Prepare stream header data */
    memcpy(fbs_header, "RFB 003.003\n", 12);
    buf_put_CARD32(&fbs_header[12], recordtime);	// the real record time
    buf_put_CARD16(&fbs_header[16], host_slot->fb_width);
    buf_put_CARD16(&fbs_header[18], host_slot->fb_height);
    buf_put_pixfmt(&fbs_header[20], &host_slot->screen_info.pixformat);
    if (!s_join_sessions) {
      len = g_screen_info.name_length;
      if (len > 192)
        len = 192;
      buf_put_CARD32(&fbs_header[36], len);
      memcpy(&fbs_header[40], g_screen_info.name, len);
    } else {
      len = sizeof(fbs_desktop_name) - 1;
      buf_put_CARD32(&fbs_header[36], len);
      memcpy(&fbs_header[40], fbs_desktop_name, len);
    }

    /* Write stream header data */
    fbs_write_data(fbs_header, 40 + len);

  } else {                      /* record file already exist, maybe reconnecting */
	// recover the recording time
	CARD8 data_buf[4];
	CARD32 padding;
	CARD32 start_time;
	fread(fbs_header, 1, 12, host_slot->fbs_fp);	// read FBS header
	fread(data_buf, 1, 4, host_slot->fbs_fp);	// read time offset
	fread(data_buf, 1, 4, host_slot->fbs_fp);	// read data length
	len = buf_get_CARD32(data_buf);
	padding = 3 - ((len - 1) & 0x03);
	if (padding > 0) {
		fread(data_buf, 1, padding, host_slot->fbs_fp);	// read padding
	}
	fread(data_buf, 1, 1, host_slot->fbs_fp);	// the new session flag
	fread(fbs_header, 1, len, host_slot->fbs_fp);	// read RFB header
	start_time = buf_get_CARD32(&fbs_header[12]);
	host_slot->fbs_start_time.tv_sec = start_time;

	fclose(host_slot->fbs_fp);
    /* Open the file for append */
    host_slot->fbs_fp = fopen(fname, "a");
    if (host_slot->fbs_fp == NULL) {
      log_write(LL_WARN, "Could not open FBS file for writing");
      s_fbs_prefix = NULL;
      return;
    }
    log_write(LL_MSG, "Re-opened FBS file for writing: %s", fname);
  }
}

void fbs_alloc_mem()
{
  HOST_SLOT *host_slot = (HOST_SLOT*)cur_slot;

  int max_rect_size, w, h;

  if (host_slot->fbs_buffer)
	  return;

  /* Allocate memory to hold one rectangle of maximum size */
  if (host_slot->fbs_fp != NULL) {
    w = (int)host_slot->fb_width;
    h = (int)host_slot->fb_height;
    max_rect_size = 12 + (w * h * 4) + ((w + 15) / 16) * ((h + 15) / 16);
    host_slot->fbs_buffer = malloc(max_rect_size);
    if (host_slot->fbs_buffer == NULL) {
      log_write(LL_WARN, "Memory allocation error, closing FBS file");
      fclose(host_slot->fbs_fp);
      host_slot->fbs_fp = NULL;
    } else {
      log_write(LL_DETAIL, "Allocated buffer to cache FBS data, %d bytes",
                max_rect_size);
      host_slot->fbs_buffer_ptr = host_slot->fbs_buffer;
    }
  }
}

void fbs_write_data(void *buf, size_t len)
{
  HOST_SLOT *host_slot = (HOST_SLOT*)cur_slot;
  CARD8 data_size_buf[4];
  CARD8 timestamp_buf[8];
  CARD32 timestamp;
  CARD8 new_session = host_slot->new_session;
  int padding;

  if (host_slot->fbs_fp == NULL)
    return;

  if (new_session) {
    host_slot->new_session = 0; // only need set once per session
  }

  /* Calculate current timestamp */
  _gettimeofday(&host_slot->fbs_time);
  if (host_slot->fbs_time.tv_sec < host_slot->fbs_start_time.tv_sec) {
    /* FIXME: not sure if this is correct. */
    host_slot->fbs_time.tv_sec += 60 * 60 * 24;
  }
  timestamp = (CARD32)((host_slot->fbs_time.tv_sec - host_slot->fbs_start_time.tv_sec) * 1000 +
                       (host_slot->fbs_time.tv_usec - host_slot->fbs_start_time.tv_usec) / 1000);

  log_write(LL_DETAIL, "Time offset %d.%03d", timestamp/1000, timestamp%1000);

  padding = 3 - (len & 0x03);   // the new_session flag adds one byte
  buf_put_CARD32(data_size_buf, (CARD32)(len+1));
  buf_put_CARD32(&timestamp_buf[0], timestamp);

  if (fwrite(timestamp_buf, 1, 4, host_slot->fbs_fp) != 4 ||
	  fwrite(data_size_buf, 1, 4, host_slot->fbs_fp) != 4 ||
      fwrite(timestamp_buf, 1, padding, host_slot->fbs_fp) != padding ||
      fwrite(&new_session, 1, 1, host_slot->fbs_fp) != 1 ||
      fwrite(buf, 1, len, host_slot->fbs_fp) != len) {
    log_write(LL_WARN, "Could not write FBS file data");
    fbs_close_file();
  }
}

void fbs_spool_byte(CARD8 b)
{
  HOST_SLOT *host_slot = (HOST_SLOT*)cur_slot;

  fbs_alloc_mem();

  if (host_slot->enable_recording && !host_slot->requested_update)
  {
    if (host_slot->fbs_fp != NULL)
      *host_slot->fbs_buffer_ptr++ = b;
  }
}

void fbs_spool_data(void *buf, size_t len)
{
  HOST_SLOT *host_slot = (HOST_SLOT*)cur_slot;

  fbs_alloc_mem();

  if (host_slot->enable_recording && !host_slot->requested_update)
  {
    if (host_slot->fbs_fp == NULL) {
      fbs_open_file();
	}
    if (host_slot->fbs_fp != NULL) {
      log_write(LL_DEBUG, "Spooling data: %d", len);
      memcpy(host_slot->fbs_buffer_ptr, buf, len);
      host_slot->fbs_buffer_ptr += len;
	}
  }
}

void fbs_spool_data2(void *buf, size_t len)
{
  HOST_SLOT *host_slot = (HOST_SLOT*)cur_slot;
  if (host_slot->fbs_fp == NULL) {
    fbs_open_file();
  }
  if (host_slot->fbs_fp != NULL) {
    if ((host_slot->fbs_buffer_ptr - host_slot->fbs_buffer)>0) {
      fbs_write_data(host_slot->fbs_buffer, host_slot->fbs_buffer_ptr - host_slot->fbs_buffer);
      host_slot->fbs_buffer_ptr = host_slot->fbs_buffer;
	}
    fbs_write_data(buf, len);
  }
}

/* Write spooled data to file (e.g. when rectangle data is completely spooled) */
void fbs_flush_data(void)
{
  HOST_SLOT *host_slot = (HOST_SLOT*)cur_slot;
  if (host_slot->enable_recording && !host_slot->requested_update)
  {
    if (host_slot->fbs_fp != NULL) {
      fbs_write_data(host_slot->fbs_buffer, host_slot->fbs_buffer_ptr - host_slot->fbs_buffer);
      host_slot->fbs_buffer_ptr = host_slot->fbs_buffer;
	}
  }
}

void fbs_close_file(void)
{
  HOST_SLOT *host_slot = (HOST_SLOT*)cur_slot;
  if (host_slot->fbs_fp != NULL) {
    log_write(LL_MSG, "fbs_close_file: closing file %s", host_slot->fbs_filename);
    if (fclose(host_slot->fbs_fp) != 0)
      log_write(LL_WARN, "Could not close FBS file");
    host_slot->fbs_fp = NULL;
    free(host_slot->fbs_buffer);
    if (!host_slot->recording_enabled) {	// BL -- 10/22/2004
      log_write(LL_MSG, "fbs_close_file: recording was not enabled, delete spool file.");
      unlink(host_slot->fbs_filename);
	}
    free(host_slot->fbs_filename);
  }
}

