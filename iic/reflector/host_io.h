/* VNC Reflector
 * Copyright (C) 2001-2003 HorizonLive.com, Inc.  All rights reserved.
 *
 * This software is released under the terms specified in the file LICENSE,
 * included.  HorizonLive provides e-Learning and collaborative synchronous
 * presentation solutions in a totally Web-based environment.  For more
 * information about HorizonLive, please see our website at
 * http://www.horizonlive.com.
 *
 * This software was authored by Constantin Kaplinsky <const@ce.cctpu.edu.ru>
 * and sponsored by HorizonLive.com, Inc.
 *
 * $Id: host_io.h,v 1.17 2008-01-31 19:26:10 mike Exp $
 * Asynchronous interaction with VNC host.
 */

#ifndef _REFLIB_HOST_IO_H
#define _REFLIB_HOST_IO_H

#ifdef LINUX
#include <sys/time.h>
#include <sys/types.h>
#else
#include <winsock2.h>
#include <time.h>
#endif
#include "update.h"

#define TYPE_HOST_LISTENING_SLOT   2
#define TYPE_HOST_CONNECTING_SLOT  3
#define TYPE_HOST_ACTIVE_SLOT      4

#define HOST_TIMEOUT 300000     /* 5 mins */

/* Extension to AIO_SLOT structure to hold state for host connection */
typedef struct _HOST_SLOT {
  AIO_SLOT s;

  int client_port;

  CARD32 temp_len;
  CARD16 fb_width;
  CARD16 fb_height;

  int buf_len;
  int buf_pos;
  CARD8 *rect_buf;

  // 2004.10.13 BL
  FILE* fbs_fp;
  CARD8 *fbs_buffer, *fbs_buffer_ptr, *fbs_filename;
  struct timeval fbs_start_time, fbs_time;
  time_t start_time;
  RFB_SCREEN_INFO screen_info;

  AIO_SLOT* controller;
  UPDATE first;

  /* Queue all updates received from this host until sent to all clients */
  /* Client must make sure it's sequence number is still valid before using
     stored update ptr; we may have deleted update if queue is too long */
  int update_sequence;
  UPDATE_QUEUE updates;
  unsigned need_fullscreen     :1;
  unsigned requested_fullscreen:1;
  unsigned activated           :1;	// IIC
  unsigned no_dispatch_close   :1;
  unsigned enable_recording    :1;	// IIC	-- current recroding state
  unsigned recording_enabled   :1;	// IIC	-- track if recording was ever enabled
  unsigned requested_update    :1;	// IIC	-- request a full update when recording is enabled
  unsigned update_received     :1;	// IIC	-- avoid getting full update at the beginning
  unsigned new_session         :1;	// IIC	-- in case previuos session was not complete, notify recorder to start a new session

} HOST_SLOT;

#define RECT_BUF_MIN_INC 1024     // Min size increase for allocated mem block
#define RECT_MAX_QUEUE 10000      // Max number of update rects to queue
#define RECT_MAX_QUEUE_BYTES 1000000 // Max number of bytes in queue
#define RECT_MAX_BACKTRACK 250000 // Max data to send to new client in bytes

HOST_SLOT *get_host_slot();

extern void host_activate(HOST_SLOT *host_slot);
extern void host_close_hook();
extern void host_free_queue();	// IIC

extern void pass_msg_to_host(AIO_SLOT *host_slot, CARD8 *msg, size_t len);
extern void pass_cuttext_to_host(AIO_SLOT *host_slot, CARD8 *text, size_t len);

extern void fill_fb_rect(FB_RECT *r, CARD32 color);
extern void fbupdate_rect_done(AIO_SLOT *host_slot);

extern void fbupdate_rect_start(AIO_SLOT *host_slot);
extern void fbupdate_rect_data(AIO_SLOT *host_slot, void *data, int size);

extern void fn_request_fullscreen(AIO_SLOT *slot);

/* decode_hextile.c */

extern void setread_decode_hextile(FB_RECT *r);

/* decode_tight.c */

extern void setread_decode_tight(FB_RECT *r);
extern void reset_tight_streams(void);

extern void host_set_formats(int init);	//IIC

extern void host_request_recording(int enable_recording, int start_time);	// IIC
extern void host_enable_recording(int enable_recording, int start_time);	// IIC
extern void request_update(int incr);	// IIC

#endif /* _REFLIB_HOST_IO_H */
