/* VNC Reflector
 * Copyright (C) 2001-2003 HorizonLive.com, Inc.  All rights reserved.
 *
 * This software is released under the terms specified in the file LICENSE,
 * included.  HorizonLive provides e-Learning and collaborative synchronous
 * presentation solutions in a totally Web-based environment.  For more
 * information about HorizonLive, please see our website at
 * http://www.horizonlive.com.
 *
 * This software was authored by Constantin Kaplinsky <const@ce.cctpu.edu.ru>
 * and sponsored by HorizonLive.com, Inc.
 *
 * $Id: control_io.h,v 1.4 2005-04-20 16:01:33 mike Exp $
 * Asynchronous interaction with VNC clients.
 */

#ifndef _REFLIB_CONTROL_IO_H
#define _REFLIB_CONTROL_IO_H

#include "region.h"
#include "update.h"
#include "host_io.h"

#define TYPE_CNTRL_SLOT 6
#define TYPE_CNTRL_LISTENING 7


/* Extension to AIO_SLOT structure to hold client state */
typedef struct _CNTRL_SLOT {
  AIO_SLOT s;

  int len;
  unsigned char *data;
  int pos;

  char *cmd_part_id;
  int cmd_flag;
  int cmd_time;

} CNTRL_SLOT;

int control_listen(int port, int max_requests);
void af_control_init(void);
void af_control_accept(void);

int prepare_control_message();
int send_control_message();
int send_string(const char * s);
int send_int(int x);
int get_string(char * s);
int get_int(int * x);
int is_controlled();
const char * get_host_id(const char *meetingid);

void control_change_host(const char * meetingid, const char * partid);
void control_end_session(const char * meetingid, int reason);
void control_reject_participant(const char * meetingid, const char * partid, int reason);
void control_enable_remote_control(const char * meetingid, const char * partid, int enable);
void control_enable_recording(const char * meetingid, int enable, int start_time);
int control_send_end_session(const char * meetingid, int reason);
int control_send_participant_joined(const char * token, const char * meetingid, const char * partid, int host_flag);
int control_send_participant_left(const char * meetingid, const char * partid, int reason);
int control_send_session_active(const char * meetingid);

#endif /* _REFLIB_CONTROL_IO_H */
