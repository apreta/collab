/* (c) 2004 imidio */
/*
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2011 Apreta.
 */

#include <stdio.h>
#include <stdlib.h>
#ifdef LINUX
#include <unistd.h>
#endif
#include <string.h>
#include <errno.h>
#ifdef LINUX
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <netinet/tcp.h>
#else
#include <winsock2.h>
#endif
#include <ctype.h>
#include <zlib.h>

#include "rfblib.h"
#include "reflector.h"
#include "logging.h"
#include "async_io.h"
#include "host_io.h"
#include "client_io.h"
#include "host_connect.h"
#include "port.h"
#include "omni_connect.h"
#include "control_io.h"

static char *s_host_info_file;
static int s_cl_listen_port;

static char s_hostname[256];
static int s_host_port;
static unsigned char s_host_password[9];
static int s_require_token = 0;

static int parse_host_info(void);
static void init_hook(void);
static void listen_init_hook(void);
static void accept_hook(void);
static void rfb_read_init(void);
static void rfb_read_sessiontoken(void);
static void rfb_read_meetingid(void);
static void rfb_read_participantid(void);
static void wf_client_no_host(void);

#define _max(a,b)    (((a) > (b)) ? (a) : (b))

void set_require_session_token(int flag)
{
    s_require_token = flag;
}

int omni_connect(char *host_info_file, int host_port, int cl_listen_port)
{
  int host_fd;
  struct hostent *phe;
  struct sockaddr_in host_addr;
  int nodelayval = 1;

  if (host_info_file != NULL)
    s_host_info_file = host_info_file;
  if (cl_listen_port != 0)
    s_cl_listen_port = cl_listen_port;

  /* Extract hostname, port and password from host info file */
  if (!parse_host_info())
    return 0;

  /* Optionally override host port from host info file */
  if (host_port)
	  s_host_port = host_port;

  if (strcmp(s_hostname, "*") != 0) {

    /* Forward reflector -> host connection */
    log_write(LL_MSG, "Connecting to %s, port %d", s_hostname, s_host_port);

    phe = gethostbyname(s_hostname);
    if (phe == NULL) {
      log_write(LL_ERROR, "Could not get host address: %s", _strerror(errno));
      return 0;
    }

    host_addr.sin_family = AF_INET;
    memcpy(&host_addr.sin_addr.s_addr, phe->h_addr, phe->h_length);
    host_addr.sin_port = htons((unsigned short)s_host_port);

    host_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (host_fd == -1) {
      log_write(LL_ERROR, "Could not create socket: %s", _strerror(errno));
      return 0;
    }

    // Disable Nagle algorithm
    setsockopt(host_fd, IPPROTO_TCP, TCP_NODELAY, (const char *) &nodelayval, sizeof(int));

    if (connect(host_fd, (struct sockaddr *)&host_addr,
                sizeof(host_addr)) != 0) {
      log_write(LL_ERROR, "Could not connect: %s", _strerror(errno));
      _close(host_fd);
      return 0;
    }

    log_write(LL_MSG, "Connection established");

    aio_add_slot(host_fd, NULL, init_hook, _max(sizeof(HOST_SLOT), sizeof(CL_SLOT)));

  } else {

    /* Reversed host -> reflector connection, start listening */

    if (!aio_listen(s_host_port, listen_init_hook, accept_hook,
                   _max(sizeof(HOST_SLOT), sizeof(CL_SLOT)))) {
      log_write(LL_ERROR, "Error creating listening socket: %s",
                _strerror(errno));
      return 0;
    }

    log_write(LL_MSG, "Listening for host connections on port %d",
              s_host_port);
  }

  return 1;
}

static int parse_host_info(void)
{
  FILE *fp;
  char buf[256];
  char *pos, *colon_pos, *space_pos;
  int len;

  fp = fopen(s_host_info_file, "r");
  if (fp == NULL) {
    log_write(LL_ERROR, "Cannot open host info file: %s", s_host_info_file);
    return 0;
  }

  /* Read the file into a buffer first */
  len = fread(buf, 1, 255, fp);
  buf[len] = '\0';
  fclose(fp);

  if (len == 0) {
    log_write(LL_ERROR, "Error reading host info file (is it empty?)");
    return 0;
  }

  /* Truncate at the end of first line, respecting MS-DOS end-of-lines */
  pos = strchr(buf, '\n');
  if (pos != NULL)
    *pos = '\0';
  pos = strchr(buf, '\r');
  if (pos != NULL)
    *pos = '\0';

  /* FIXME: parsing code below is primitive */

  space_pos = strchr(buf, ' ');
  if (space_pos != NULL) {
    strncpy((char *)s_host_password, space_pos + 1, 8);
    s_host_password[8] = '\0';
    *space_pos = '\0';
  } else {
    log_write(LL_WARN, "Host password not specified, assuming no auth");
    s_host_password[0] = '\0';
  }

  colon_pos = strchr(buf, ':');
  if (colon_pos != NULL) {
    if (!isdigit(colon_pos[1]) && colon_pos[1] != '-') {
      log_write(LL_ERROR, "Non-numeric host display number: %s",
                colon_pos + 1);
      return 0;
    }
    s_host_port = atoi(colon_pos + 1);
    *colon_pos = '\0';
  } else {
    log_write(LL_WARN, "Host display not specified, assuming display :0");
    s_host_port = 0;
  }

  strcpy(s_hostname, buf);
  if (strcmp(s_hostname, "*") != 0) {
    s_host_port += 5900;
  } else {
    if (s_host_port == 0)
      s_host_port = 5500;
  }

  return 1;
}

static void init_hook(void)
{
  cur_slot->type = TYPE_HOST_LISTENING_SLOT;
  aio_setread(rfb_read_init, NULL, 4);
}

static void listen_init_hook(void)
{
  cur_slot->type = TYPE_HOST_LISTENING_SLOT;
}

static void accept_hook(void)
{
  log_write(LL_MSG, "Accepted a connection from %s", cur_slot->name);

  init_hook();
}

static void rfb_read_init(void)
{
  CARD8 type;
  CARD16 len16;
  int read_session_id = 0;
  CARD8 session_header = OMNI_VALID;

  cur_slot->version = buf_get_CARD8(cur_slot->readbuf);
  type = buf_get_CARD8(&cur_slot->readbuf[1]);

  switch (type) {
  case 2:
    read_session_id = 1;
  case 0:	// host
	cur_slot->client_f = 0;
    break;
  case 3:
    read_session_id = 1;
  case 1:	// client
	cur_slot->client_f = 1;
	cur_slot->type = TYPE_CL_SLOT;
    break;
  default:
    log_write(LL_ERROR, "Unknown connection type, client may need upgrade.");
    aio_close(0);
    return;
  }

  len16 = buf_get_CARD16(&cur_slot->readbuf[2]);

  log_write(LL_MSG, "Connection type %d, id len %d", type, len16);

  if (cur_slot->version == OMNI_VER_1)
      aio_write(NULL, &session_header, 1);

  if (read_session_id) {
    /* Read session token + length of particpant id string (mode used for share lite) */
    aio_setread(rfb_read_sessiontoken, NULL, len16 + 2);
  }
  else {
    aio_setread(rfb_read_meetingid, NULL, len16);
  }
}

static void rfb_read_sessiontoken(void)
{
  CARD16 len16;

  // parse session token
  cur_slot->token = malloc(cur_slot->bytes_to_read+1);
  memcpy(cur_slot->token, cur_slot->readbuf, cur_slot->bytes_to_read);
  cur_slot->token[cur_slot->bytes_to_read] = '\0';
  log_write(LL_DETAIL, "Session token=%s.", cur_slot->token);

  // Read participant id + len of meeting id
  len16 = buf_get_CARD16(&cur_slot->readbuf[cur_slot->bytes_to_read - 2]);
  aio_setread(rfb_read_participantid, NULL, len16 + 2);
}

static void rfb_read_participantid(void)
{
  CARD16 len16;

  // parse participant id
  cur_slot->part_id = malloc(cur_slot->bytes_to_read+1);
  memcpy(cur_slot->part_id, cur_slot->readbuf, cur_slot->bytes_to_read);
  cur_slot->part_id[cur_slot->bytes_to_read] = '\0';
  log_write(LL_DETAIL, "Participant Id=%s.", cur_slot->part_id);

  // read meeting id
  len16 = buf_get_CARD16(&cur_slot->readbuf[cur_slot->bytes_to_read - 2]);
  aio_setread(rfb_read_meetingid, NULL, len16);
}

static void rfb_read_meetingid(void)
{
  CARD8 session_header;
  HOST_SLOT *hs;

  // parse meeting id
  cur_slot->id = malloc(cur_slot->bytes_to_read+1);
  memcpy(cur_slot->id, cur_slot->readbuf, cur_slot->bytes_to_read);
  cur_slot->id[cur_slot->bytes_to_read] = '\0';

  // check for participant id and token by looking for embedded nulls
  if (strlen(cur_slot->id) < cur_slot->bytes_to_read)
  {
     cur_slot->part_id = cur_slot->id + strlen(cur_slot->id) + 1;

     if (strlen(cur_slot->id) + strlen(cur_slot->part_id) + 1 < cur_slot->bytes_to_read)
        cur_slot->token = cur_slot->id + strlen(cur_slot->id) + strlen(cur_slot->part_id) + 2;

      log_write(LL_MSG, "Participant Id=%s, token=%s.", cur_slot->part_id, cur_slot->token);
  }

  // Make sure host is present for client slot
  if (cur_slot->client_f)
  {
	hs = get_host_slot(cur_slot->id);
	if (hs == NULL || !hs->activated)
	{
      log_write(LL_DETAIL, "No host for meeting %s", cur_slot->id);
      if (cur_slot->version == OMNI_VER_2) {
        session_header = OMNI_NO_HOST;
        aio_write(wf_client_no_host, &session_header, 1);
	  }
	  else {
		aio_close(0);
	  }
	  return;
    }
  }

  // Send joined message to control slot
  if (s_require_token) {
    if (cur_slot->part_id == NULL || cur_slot->token == NULL) {
      log_write(LL_ERROR, "Participant id or token not received");
      aio_close(0);
      return;
    }
    if (!control_send_participant_joined(cur_slot->token, cur_slot->id, cur_slot->part_id, cur_slot->client_f == 0)) {
      log_write(LL_ERROR, "No control session");
      aio_close(0);
      return;
    }
  }

  if (cur_slot->client_f) {
    log_write(LL_MSG, "Client meeting id=%s.", cur_slot->id);
    /* send back "success" flag */
    if (cur_slot->version == OMNI_VER_2) {
      session_header = OMNI_VALID;
      aio_write(NULL, &session_header, 1);
	}
	af_client_accept(hs);
  } else {
    log_write(LL_MSG, "Host meeting id=%s.", cur_slot->id);
    /* send back success flag for "omni" part of protocol */
    if (cur_slot->version == OMNI_VER_2) {
      session_header = OMNI_VALID;
      aio_write(NULL, &session_header, 1);
    }

    /* see if we can confirm this is the right host for this meeting */
    const char * hostid = get_host_id(cur_slot->id);
    if (hostid == NULL || strcmp(hostid, cur_slot->part_id) == 0) {
        host_accept_hook();
        // in case reconnect
        free_exiting_host_slot(cur_slot->id);
    }
    else {
        log_write(LL_ERROR, "Wrong host id");
        aio_close(0);
        return;
    }
  }
}

static void wf_client_no_host()
{
  aio_close(0);
}
