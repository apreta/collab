/* VNC Reflector
 * Copyright (C) 2001-2003 HorizonLive.com, Inc.  All rights reserved.
 *
 * This software is released under the terms specified in the file LICENSE,
 * included.  HorizonLive provides e-Learning and collaborative synchronous
 * presentation solutions in a totally Web-based environment.  For more
 * information about HorizonLive, please see our website at
 * http://www.horizonlive.com.
 *
 * This software was authored by Constantin Kaplinsky <const@ce.cctpu.edu.ru>
 * and sponsored by HorizonLive.com, Inc.
 *
 * $Id: host_connect.c,v 1.11 2005-06-09 21:00:26 binl Exp $
 * Connecting to a VNC host
 */

#include <stdio.h>
#include <stdlib.h>
#ifdef LINUX
#include <unistd.h>
#endif
#include <string.h>
#include <errno.h>
#ifdef LINUX
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#else
#include <winsock2.h>
#endif
#include <ctype.h>
#include <zlib.h>

#include "rfblib.h"
#include "reflector.h"
#include "logging.h"
#include "async_io.h"
#include "host_io.h"
//#include "translate.h"
#include "client_io.h"
//#include "encode.h"
#include "host_connect.h"
#include "port.h"

static int parse_host_info(void);
static void host_init_hook(void);
static void host_listen_init_hook(void);
//static void host_accept_hook(void);	// 2004.3.15 BL -- made it global
static void rf_host_ver(void);
static void rf_host_auth(void);
static void rf_host_conn_failed(void);
static void rf_host_crypt(void);
static void rf_host_auth_done(void);
static void send_client_initmsg(void);
static void rf_host_initmsg(void);
static void rf_host_set_formats(void);

int s_request_tight;
int s_tight_level;
int s_jpeg_qual;

static char *s_host_info_file;
static int s_cl_listen_port;

static char s_hostname[256];
static int s_host_port;
static unsigned char s_host_password[9];

/*
 * Set preferred encoding (Hextile or Tight) and compression level
 * for the Tight encoding.
 */

void set_host_encodings(int request_tight, int tight_level, int jpeg_qual)
{
  s_request_tight = request_tight;
  s_tight_level = tight_level;
  s_jpeg_qual = jpeg_qual;
}

/*
 * Connect to a remote RFB host
 */
#if 0
int connect_to_host(char *host_info_file, int host_port, int cl_listen_port)
{
  int host_fd;
  struct hostent *phe;
  struct sockaddr_in host_addr;

  /* Save arguments in static variables, for later use */
  if (host_info_file != NULL)
    s_host_info_file = host_info_file;
  if (cl_listen_port != 0)
    s_cl_listen_port = cl_listen_port;

  /* Extract hostname, port and password from host info file */
  if (!parse_host_info())
    return 0;

  /* Optionally override host port from host info file */
  if (host_port)
	  s_host_port = host_port;

  if (strcmp(s_hostname, "*") != 0) {

    /* Forward reflector -> host connection */
    log_write(LL_MSG, "Connecting to %s, port %d", s_hostname, s_host_port);

    phe = gethostbyname(s_hostname);
    if (phe == NULL) {
      log_write(LL_ERROR, "Could not get host address: %s", _strerror(errno));
      return 0;
    }

    host_addr.sin_family = AF_INET;
    memcpy(&host_addr.sin_addr.s_addr, phe->h_addr, phe->h_length);
    host_addr.sin_port = htons((unsigned short)s_host_port);

    host_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (host_fd == -1) {
      log_write(LL_ERROR, "Could not create socket: %s", _strerror(errno));
      return 0;
    }

    if (connect(host_fd, (struct sockaddr *)&host_addr,
                sizeof(host_addr)) != 0) {
      log_write(LL_ERROR, "Could not connect: %s", _strerror(errno));
      _close(host_fd);
      return 0;
    }

    log_write(LL_MSG, "Connection established");

    aio_add_slot(host_fd, NULL, host_init_hook, sizeof(HOST_SLOT));

  } else {

    /* Reversed host -> reflector connection, start listening */

    if (!aio_listen(s_host_port, host_listen_init_hook, host_accept_hook,
                   sizeof(HOST_SLOT))) {
      log_write(LL_ERROR, "Error creating listening socket: %s",
                _strerror(errno));
      return 0;
    }

    log_write(LL_MSG, "Listening for host connections on port %d",
              s_host_port);
  }

  return 1;
}

static int parse_host_info(void)
{
  FILE *fp;
  char buf[256];
  char *pos, *colon_pos, *space_pos;
  int len;

  fp = fopen(s_host_info_file, "r");
  if (fp == NULL) {
    log_write(LL_ERROR, "Cannot open host info file: %s", s_host_info_file);
    return 0;
  }

  /* Read the file into a buffer first */
  len = fread(buf, 1, 255, fp);
  buf[len] = '\0';
  fclose(fp);

  if (len == 0) {
    log_write(LL_ERROR, "Error reading host info file (is it empty?)");
    return 0;
  }

  /* Truncate at the end of first line, respecting MS-DOS end-of-lines */
  pos = strchr(buf, '\n');
  if (pos != NULL)
    *pos = '\0';
  pos = strchr(buf, '\r');
  if (pos != NULL)
    *pos = '\0';

  /* FIXME: parsing code below is primitive */

  space_pos = strchr(buf, ' ');
  if (space_pos != NULL) {
    strncpy((char *)s_host_password, space_pos + 1, 8);
    s_host_password[8] = '\0';
    *space_pos = '\0';
  } else {
    log_write(LL_WARN, "Host password not specified, assuming no auth");
    s_host_password[0] = '\0';
  }

  colon_pos = strchr(buf, ':');
  if (colon_pos != NULL) {
    if (!isdigit(colon_pos[1]) && colon_pos[1] != '-') {
      log_write(LL_ERROR, "Non-numeric host display number: %s",
                colon_pos + 1);
      return 0;
    }
    s_host_port = atoi(colon_pos + 1);
    *colon_pos = '\0';
  } else {
    log_write(LL_WARN, "Host display not specified, assuming display :0");
    s_host_port = 0;
  }

  strcpy(s_hostname, buf);
  if (strcmp(s_hostname, "*") != 0) {
    s_host_port += 5900;
  } else {
    if (s_host_port == 0)
      s_host_port = 5500;
  }

  return 1;
}

static void host_init_hook(void)
{
  cur_slot->type = TYPE_HOST_CONNECTING_SLOT;
  ((HOST_SLOT*)cur_slot)->client_port = s_cl_listen_port;
  aio_setclose(host_close_hook);
  aio_setread(rf_host_ver, NULL, 12);
}

static void host_listen_init_hook(void)
{
  cur_slot->type = TYPE_HOST_LISTENING_SLOT;
}
#endif

void host_accept_hook(void)
{
  log_write(LL_MSG, "Accepted host connection from %s (%s)", cur_slot->name, cur_slot->id);
  
  cur_slot->type = TYPE_HOST_CONNECTING_SLOT;
  aio_setclose(host_close_hook);
  aio_setread(rf_host_ver, NULL, 12);
}

void rf_host_ver(void)
{
  char *buf = (char *)cur_slot->readbuf;
  int major = 3, minor = 3;
  int remote_major, remote_minor;
  char ver_msg[12];

  if ( strncmp(buf, "RFB ", 4) != 0 || !isdigit(buf[4]) ||
       !isdigit(buf[4]) || !isdigit(buf[5]) || !isdigit(buf[6]) ||
       buf[7] != '.' || !isdigit(buf[8]) || !isdigit(buf[9]) ||
       !isdigit(buf[10]) || buf[11] != '\n' ) {
    log_write(LL_ERROR, "Wrong greeting data received from host");
    aio_close(0);
    return;
  }

  remote_major = atoi(&buf[4]);
  remote_minor = atoi(&buf[8]);
  log_write(LL_INFO, "Remote RFB Protocol version is %d.%d",
            remote_major, remote_minor);

  if (remote_major != major) {
    log_write(LL_ERROR, "Wrong protocol version, expected %d.x", major);
    aio_close(0);
    return;
  } else if (remote_minor != minor) {
    log_write(LL_WARN, "Protocol sub-version does not match (ignoring)");
  }

  sprintf(ver_msg, "RFB %03d.%03d\n", abs(major) % 999, abs(minor) % 999);
  aio_write(NULL, ver_msg, 12);
  aio_setread(rf_host_auth, NULL, 4);
}

static void rf_host_auth(void)
{
  HOST_SLOT *hs = (HOST_SLOT *)cur_slot;
  CARD32 value32;

  value32 = buf_get_CARD32(cur_slot->readbuf);

  switch (value32) {
  case 0:
    hs->temp_len = buf_get_CARD32(&cur_slot->readbuf[4]);
    aio_setread(rf_host_conn_failed, NULL, hs->temp_len);
    break;
  case 1:
    log_write(LL_MSG, "No authentication required at host side");
    send_client_initmsg();
    break;
  case 2:
    log_write(LL_DETAIL, "IIC authentication requested by host");
    aio_setread(rf_host_crypt, NULL, 16);
    break;
  default:
    log_write(LL_ERROR, "Unknown authentication scheme requested by host");
    aio_close(0);
    return;
  }
}

static void rf_host_conn_failed(void)
{
  HOST_SLOT *hs = (HOST_SLOT *)cur_slot;

  log_write(LL_ERROR, "IIC connection failed: %.*s",
            (int)hs->temp_len, cur_slot->readbuf);
  aio_close(0);
}

static void rf_host_crypt(void)
{
  unsigned char response[16];

  log_write(LL_DEBUG, "Received random challenge");

  rfb_crypt(response, cur_slot->readbuf, s_host_password);
  log_write(LL_DEBUG, "Sending DES-encrypted response");
  aio_write(NULL, response, 16);

  aio_setread(rf_host_auth_done, NULL, 4);
}

static void rf_host_auth_done(void)
{
  CARD32 value32;

  value32 = buf_get_CARD32(cur_slot->readbuf);

  switch(value32) {
  case 0:
    log_write(LL_MSG, "Authentication successful");
    send_client_initmsg();
    break;
  case 1:
    log_write(LL_ERROR, "Authentication failed");
    aio_close(0);
    break;
  case 2:
    log_write(LL_ERROR, "Authentication failed, too many tries");
    aio_close(0);
    break;
  default:
    log_write(LL_ERROR, "Unknown authentication result");
    aio_close(0);
  }
}

static void send_client_initmsg(void)
{
  CARD8 shared_session = 1;

  log_write(LL_DETAIL, "Requesting %s session",
            (shared_session != 0) ? "non-shared" : "shared");
  aio_write(NULL, &shared_session, 1);

  aio_setread(rf_host_initmsg, NULL, 24);
}

static void rf_host_initmsg(void)
{
  HOST_SLOT *hs = (HOST_SLOT *)cur_slot;

  hs->fb_width = buf_get_CARD16(cur_slot->readbuf);
  hs->fb_height = buf_get_CARD16(&cur_slot->readbuf[2]);
  log_write(LL_MSG, "Remote desktop geometry is %dx%d",
            (int)hs->fb_width, (int)hs->fb_height);

  hs->temp_len = buf_get_CARD32(&cur_slot->readbuf[20]);
  aio_setread(rf_host_set_formats, NULL, hs->temp_len);
}

static void rf_host_set_formats(void)
{
  HOST_SLOT *hs = (HOST_SLOT *)cur_slot;
  
  host_set_formats(1);

  host_activate(hs);
}

void host_set_formats(int init)
{
  HOST_SLOT *hs = (HOST_SLOT *)cur_slot;
  CARD8 *new_name;
  unsigned char setpixfmt_msg[4 + SZ_RFB_PIXEL_FORMAT];
  unsigned char setenc_msg[36] = {
    2,                          /* Message id */
    0,                          /* Padding -- not used */
    0, 0                        /* Number of encodings */
  };
  int setenc_msg_size;

  /* FIXME: Don't change g_screen_info while there is an active host
     connection! */

  if (init) {
      memcpy(&hs->screen_info, &g_screen_info, sizeof(RFB_SCREEN_INFO));
      hs->screen_info.name = NULL;	// 10/28/2004 BL
      
      log_write(LL_INFO, "Remote desktop name: %.*s",
            (int)hs->temp_len, cur_slot->readbuf);
	  new_name = malloc((size_t)hs->temp_len + 1);
	  if (new_name != NULL) {
		if (hs->screen_info.name != NULL)
		  free(hs->screen_info.name);

		hs->screen_info.name = new_name;
		hs->screen_info.name_length = hs->temp_len;
		memcpy(hs->screen_info.name, cur_slot->readbuf, hs->temp_len);
		hs->screen_info.name[hs->temp_len] = '\0';
	  }
  }

  log_write(LL_DETAIL, "Setting up pixel format");

  memset(setpixfmt_msg, 0, 4);
  buf_put_pixfmt(&setpixfmt_msg[4], &hs->screen_info.pixformat);
  log_write(LL_DEBUG, "Sending SetPixelFormat message");
  aio_write(NULL, setpixfmt_msg, sizeof(setpixfmt_msg));

  if (s_request_tight) {
    log_write(LL_DETAIL, "Requesting Tight encoding");
    buf_put_CARD32(&setenc_msg[4],  RFB_ENCODING_TIGHT);
    buf_put_CARD32(&setenc_msg[8],  RFB_ENCODING_HEXTILE);
    buf_put_CARD32(&setenc_msg[12], RFB_ENCODING_COPYRECT);
    buf_put_CARD32(&setenc_msg[16], RFB_ENCODING_RAW);
    buf_put_CARD32(&setenc_msg[20], RFB_ENCODING_LASTRECT);
    buf_put_CARD32(&setenc_msg[24], RFB_ENCODING_NEWFBSIZE);
    if (s_tight_level >= 0 && s_tight_level <= 9) {
      log_write(LL_DETAIL, "Requesting compression level %d", s_tight_level);
      buf_put_CARD32(&setenc_msg[28], (RFB_ENCODING_COMPESSLEVEL0 +
                                       (CARD32)s_tight_level));
	  if (s_jpeg_qual >= 0 && s_jpeg_qual <= 9) {
        log_write(LL_DETAIL, "Requesting jpeg quality %d", s_jpeg_qual);
        buf_put_CARD32(&setenc_msg[32], (RFB_ENCODING_QUALITYLEVEL0 +
                                         (CARD32)s_jpeg_qual));

        buf_put_CARD16(&setenc_msg[2], 8);
        setenc_msg_size = 36;
	  }
	  else {
        buf_put_CARD16(&setenc_msg[2], 7);
        setenc_msg_size = 32;
	  }
    } else {
      buf_put_CARD16(&setenc_msg[2], 6);
      setenc_msg_size = 28;
    }
  } else {
    log_write(LL_DETAIL, "Requesting Hextile encoding");
    buf_put_CARD32(&setenc_msg[4],  RFB_ENCODING_HEXTILE);
    buf_put_CARD32(&setenc_msg[8],  RFB_ENCODING_COPYRECT);
    buf_put_CARD32(&setenc_msg[12], RFB_ENCODING_RAW);
    buf_put_CARD32(&setenc_msg[16], RFB_ENCODING_NEWFBSIZE);
    buf_put_CARD16(&setenc_msg[2], 4);
    setenc_msg_size = 20;
  }

  log_write(LL_DEBUG, "Sending SetEncodings message");
  aio_write(NULL, setenc_msg, setenc_msg_size);

  if (!init)
  {
    UPDATE *upd;
    log_write(LL_INFO, "Free queue, meeting id = %s", cur_slot->id);
	/* Free queue */
	upd = hs->updates.head;
	while (upd != NULL)
	{
		UPDATE *cur = upd;
		upd = upd->next;
		free(cur);
	}
	hs->updates.head = NULL;
	hs->updates.tail = NULL;
    hs->updates.total_len = 0;
    hs->updates.size = 0;

    log_write(LL_INFO, "Encoding changed, Requesting full framebuffer update");
    request_update(0);
  }
}
