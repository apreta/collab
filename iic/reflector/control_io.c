/* (c) 2004 imidio */
/*
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2011 Apreta.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <zlib.h>
#ifdef LINUX
#include <sys/time.h>
#include <errno.h>
#include <search.h>
#else
#include <time.h>
#endif

#include "rfblib.h"
#include "logging.h"
#include "async_io.h"
#include "reflector.h"
#include "host_io.h"
#include "client_io.h"
#include "control_io.h"
#include "port.h"
#include "../util/asynccomm/comdefs.h"
#include "../services/common/sharecommon.h"

/*
 * Prototypes for static functions
 */

static void cf_control(void);
static void rf_control_header(void);
static void rf_control_cmdlen(void);
static void rf_control_cmd(void);
static void fn_control_close_host(AIO_SLOT *slot);
static void fn_control_drop_participant(AIO_SLOT *slot);
static void fn_control_enable_remote_control(AIO_SLOT *slot);
static void fn_control_enable_recording(AIO_SLOT *slot);

static int s_max_requests;
static CNTRL_SLOT * s_control_slot = NULL;

void * meeting_list = NULL;

struct meeting_st {
    const char *meeting_id;
    const char *host_id;
};
typedef struct meeting_st meeting;

struct node_st {
    meeting *mtg;
};
typedef struct node_st node;

static meeting *create_meeting(const char *mtg, const char *part)
{
    meeting *p = malloc(sizeof(struct meeting_st));
    p->meeting_id = strdup(mtg);
    p->host_id = strdup(part);
    return p;
}

static void delete_meeting(meeting *mtg)
{
    free((void*)mtg->meeting_id);
    free((void*)mtg->host_id);
    memset(mtg, 0, sizeof(meeting));
    free(mtg);
}

static int compare_meetings(const void *pa, const void *pb)
{
   meeting *a = (meeting*)pa;
   meeting *b = (meeting*)pb;
   return strcmp(a->meeting_id, b->meeting_id);
}

/*
 * Implementation
 */

int control_listen(int port, int max_requests)
{
  if (!aio_listen(port, af_control_init, af_control_accept,
                  sizeof(CNTRL_SLOT))) {
    log_write(LL_ERROR, "Error creating listening socket: %s",
	     _strerror(errno));
    //aio_close(1);
    return 0;
  }
  s_max_requests = max_requests;
  log_write(LL_MSG, "Accepting control connections on port %d (max "
            "requests: %d)", port, s_max_requests);
  return 1;
}

void af_control_init(void)
{
  cur_slot->type = TYPE_CNTRL_LISTENING;
}

void af_control_accept(void)
{
  CNTRL_SLOT *cl = (CNTRL_SLOT *)cur_slot;

  cur_slot->type = TYPE_CNTRL_SLOT;

  /* Remove old control connection */
  if (s_control_slot != NULL) {
      log_write(LL_MSG, "Accepting new control connection, closing previous");
      aio_close_other((AIO_SLOT *)s_control_slot, 0);
  }

  /* Set up new control connection */
  s_control_slot = cl;
  cl->data = malloc(MAX_MESSAGE);
  cl->len = 0;
  cl->pos = 0;

  aio_setclose(cf_control);

  log_write(LL_MSG, "Accepted control connection from %s (%s)", cur_slot->name, cur_slot->id);

  aio_write(NULL, HEADER, HEADER_LEN);
  aio_setread(rf_control_header, NULL, HEADER_LEN);
}

static void cf_control(void)
{
  CNTRL_SLOT *cl = (CNTRL_SLOT *)cur_slot;

  if (cl->data)
      free(cl->data);

  s_control_slot = NULL;

  if (cur_slot->errread_f) {
    if (cur_slot->io_errno) {
      log_write(LL_WARN, "Error reading from %s(%s): %s(0x%x)",
                cur_slot->name, cur_slot->id, strerror(cur_slot->io_errno), cur_slot->io_errno);
    } else {
      log_write(LL_WARN, "Error reading from %s(%s)", cur_slot->name, cur_slot->id);
    }
  } else if (cur_slot->errwrite_f) {
    if (cur_slot->io_errno) {
      log_write(LL_WARN, "Error sending to %s(%s): %s",
                cur_slot->name, cur_slot->id, strerror(cur_slot->io_errno));
    } else {
      log_write(LL_WARN, "Error sending to %s(%s)", cur_slot->name, cur_slot->id);
    }
  } else if (cur_slot->errio_f) {
    log_write(LL_WARN, "I/O error, client %s(%s)", cur_slot->name, cur_slot->id);
  }
  log_write(LL_MSG, "Closing client connection %s(%s)", cur_slot->name, cur_slot->id);
}

static void rf_control_header(void)
{
  CNTRL_SLOT *cl = (CNTRL_SLOT *)cur_slot;

  if (strncmp(cur_slot->readbuf, HEADER, HEADER_LEN) != 0)
  {
      log_write(LL_ERROR, "Invalid header received, closing connection");
      aio_close(0);
      return;
  }

  aio_setread(rf_control_cmdlen, NULL, 4);
}

static void rf_control_cmdlen(void)
{
  CNTRL_SLOT *cl = (CNTRL_SLOT *)cur_slot;

  int len = buf_get_CARD32(cur_slot->readbuf);

  if (len < 0 || len > MAX_MESSAGE)
  {
    log_write(LL_ERROR, "Invalid message len received: %d", len);
    aio_close(0);
    return;
  }

  log_write(LL_DETAIL, "Incoming command len: %d", len);
  cl->len = len;

  aio_setread(rf_control_cmd, NULL, len);
}

static void rf_control_cmd(void)
{
  CNTRL_SLOT *cl = (CNTRL_SLOT *)cur_slot;
  int cmd = -1;
  char token[MAX_STRING];
  char meetingid[MAX_STRING];
  char partid[MAX_STRING];
  int stat;
  int reason, flag;
  int start_time;

  cl->pos = 0;
  cl->len = cur_slot->bytes_to_read;
  memcpy(cl->data, cur_slot->readbuf, cl->len);

  /* Process command */
  get_int(&cmd);
  switch (cmd) {
    case CmdNoop:
		prepare_control_message();
		send_int(CmdNoop);
		send_control_message();
        break;

    case CmdChangePresenter:
        stat = get_string(meetingid);
        if (!stat)
            goto error;
        stat = get_string(partid);
        if (!stat)
            goto error;
        control_change_host(meetingid, partid);
        break;

    case CmdRejectParticipant:
        stat = get_string(meetingid);
        if (!stat)
            goto error;
        stat = get_string(partid);
        if (!stat)
            goto error;
        stat = get_int(&reason);
        if (!stat)
            goto error;
        control_reject_participant(meetingid, partid, reason);
        break;

    case CmdEndSession:
        stat = get_string(meetingid);
        if (!stat)
            goto error;
        stat = get_int(&reason);
        if (!stat)
            goto error;
        control_end_session(meetingid, reason);
        break;

    case CmdEnableRemoteControl:
        stat = get_string(meetingid);
        if (!stat)
            goto error;
        stat = get_string(partid);
        if (!stat)
            goto error;
        stat = get_int(&flag);
        if (!stat)
            goto error;
        control_enable_remote_control(meetingid, partid, flag);
        break;

	case CmdEnableRecording:
        stat = get_string(meetingid);
        if (!stat)
            goto error;
        stat = get_int(&flag);
        if (!stat)
            goto error;
        stat = get_int(&start_time);
        if (!stat)
            goto error;
        control_enable_recording(meetingid, flag, start_time);
        break;

    default:
        log_write(LL_ERROR, "Unknown command: %d", cmd);
        aio_close(0);
        return;
  }

  /* Read next command */
  aio_setread(rf_control_cmdlen, NULL, 4);
  return;

error:
  log_write(LL_ERROR, "Error parsing control message: %d", cmd);
  aio_close(0);
}

int prepare_control_message()
{
    if (s_control_slot == NULL)
        return 0;

    s_control_slot->len = 0;
    s_control_slot->pos = 0;
    return 1;
}

/* Send prepared message */
int send_control_message()
{
    int msg_len;
    char data[MAX_MESSAGE];
    unsigned char *p = data;

    if (s_control_slot == NULL)
        return 0;

    s_control_slot->len = s_control_slot->pos;

    msg_len = s_control_slot->len + 4;

    buf_put_CARD32(p, s_control_slot->len);
    p += 4;
    memcpy(p, s_control_slot->data, s_control_slot->len);

    aio_write_other((AIO_SLOT *)s_control_slot, NULL, data, msg_len);

    return 1;
}

int send_string(const char * s)
{
    int slen;

    if (s_control_slot == NULL)
        return 0;

    slen = strlen(s);
    if (s_control_slot->pos + 4 + slen > MAX_MESSAGE) {
        return 0;
    }
    buf_put_CARD32(&s_control_slot->data[s_control_slot->pos], slen);
    s_control_slot->pos += 4;
    memcpy(&s_control_slot->data[s_control_slot->pos], s, slen);
    s_control_slot->pos += slen;
    return 1;
}

int send_int(int x)
{
    if (s_control_slot == NULL || (s_control_slot->pos + 4 > MAX_MESSAGE))
        return 0;

    buf_put_CARD32(&s_control_slot->data[s_control_slot->pos], x);
    s_control_slot->pos += 4;
    return 1;
}

int get_string(char * s)
{
    int slen;

    if (s_control_slot == NULL || (s_control_slot->pos + 4 > s_control_slot->len))
        return 0;

    slen = buf_get_CARD32(&s_control_slot->data[s_control_slot->pos]);
    s_control_slot->pos += 4;

    if (slen + 1 > MAX_STRING || s_control_slot->pos + slen > s_control_slot->len) {
        return 0;
    }
    memcpy(s, &s_control_slot->data[s_control_slot->pos], slen);
    s[slen] = '\0';
    s_control_slot->pos += slen;
    return 1;
}

int get_int(int * x)
{
    if (s_control_slot == NULL || (s_control_slot->pos + 4 > s_control_slot->len))
        return 0;

    *x = buf_get_CARD32(&s_control_slot->data[s_control_slot->pos]);
    s_control_slot->pos += 4;
    return 1;
}

/* Is this reflector currently under control? */
int is_controlled()
{
	return (s_control_slot != NULL);
}

const char *get_host_id(const char *meetingid)
{
    meeting key = { meetingid, NULL };
    node *n = tfind(&key, &meeting_list, compare_meetings);
    if (n != NULL) {
        return n->mtg->host_id;
    }
    return NULL;
}

/* Receive change presenter command */
void control_change_host(const char * meetingid, const char * partid)
{
    log_write(LL_INFO, "Changing host to %s for meeting: %s", partid, meetingid);

    meeting key = { meetingid, NULL };
    node *n = tfind(&key, &meeting_list, compare_meetings);
    if (n == NULL) {
        meeting *mtg = create_meeting(meetingid, partid);
        node *added = tsearch(mtg, &meeting_list, compare_meetings);
    }
    else {
        free((void*)n->mtg->host_id);
        n->mtg->host_id = strdup(partid);
    }
}

/* Receive end session command */
void control_end_session(const char * meetingid, int reason)
{
    int count;
    log_write(LL_INFO, "Closing meeting: %s", meetingid);
    count = aio_walk_slots(fn_control_close_host, TYPE_HOST_ACTIVE_SLOT, meetingid);
    count += aio_walk_slots(fn_control_close_host, TYPE_HOST_CONNECTING_SLOT, meetingid);

    /* If no host connected, send back event now */
    if (count == 0) {
      control_send_end_session(meetingid, reason);
    }

    meeting key = { meetingid, NULL };
    node *n = tfind(&key, &meeting_list, compare_meetings);
    if (n != NULL) {
		meeting *mtg = n->mtg;
        tdelete(&key, &meeting_list, compare_meetings);
        delete_meeting(mtg);
    }
}

/* Receive reject participant command */
void control_reject_participant(const char * meetingid, const char * partid, int reason)
{
    log_write(LL_DEBUG, "Dropping participant %s from meeting: %s", partid, meetingid);
    if (s_control_slot == NULL)
        return;
    s_control_slot->cmd_part_id = (char *)partid;
    aio_walk_slots(fn_control_drop_participant, 0, meetingid);
    s_control_slot->cmd_part_id = NULL;
}

void control_enable_remote_control(const char * meetingid, const char * partid, int enable)
{
    log_write(LL_INFO, "%s remote control for %s in meeting: %s", enable ? "Enable" : "Disable", partid, meetingid);
    if (s_control_slot == NULL)
        return;
    s_control_slot->cmd_part_id = (char *)partid;
    s_control_slot->cmd_flag = enable;
    aio_walk_slots(fn_control_enable_remote_control, TYPE_CL_SLOT, meetingid);
    s_control_slot->cmd_part_id = NULL;
}

void control_enable_recording(const char * meetingid, int enable, int start_time)
{
    log_write(LL_INFO, "%s recording for meeting: %s", enable ? "Enable" : "Disable", meetingid);
    if (s_control_slot == NULL)
        return;
    s_control_slot->cmd_flag = enable;
	s_control_slot->cmd_time = start_time;
    aio_walk_slots(fn_control_enable_recording, TYPE_HOST_CONNECTING_SLOT, meetingid);
    aio_walk_slots(fn_control_enable_recording, TYPE_HOST_ACTIVE_SLOT, meetingid);
}


/* Send end session event to share manager */
int control_send_end_session(const char * meetingid, int reason)
{
    if (s_control_slot == NULL)
        return 0;

    log_write(LL_DEBUG, "Sending end session: %s", meetingid);
    prepare_control_message();
    send_int(CmdEndSession);
    send_string(meetingid);
    send_int(reason);
    return send_control_message();
}

int control_send_participant_joined(const char * token, const char * meetingid, const char * partid, int host_flag)
{
    if (s_control_slot == NULL)
        return 0;

    log_write(LL_DEBUG, "Sending participant joined: %s", partid);
    prepare_control_message();
    send_int(CmdParticipantJoined);
    send_string(token);
    send_string(meetingid);
    send_string(partid);
    send_int(host_flag);
    return send_control_message();
}

int control_send_participant_left(const char * meetingid, const char * partid, int reason)
{
    if (s_control_slot == NULL)
        return 0;

    log_write(LL_DEBUG, "Sending participant left: %s", partid);
    prepare_control_message();
    send_int(CmdParticipantLeft);
    send_string(meetingid);
    send_string(partid);
    send_int(reason);
    return send_control_message();
}

int control_send_session_active(const char * meetingid)
{
    if (s_control_slot == NULL)
        return 0;

    log_write(LL_DEBUG, "Sending session active: %s", meetingid);
    prepare_control_message();
    send_int(CmdSessionActive);
    send_string(meetingid);
    return send_control_message();
}

/* Close indicated slot */
static void fn_control_close_host(AIO_SLOT *slot)
{
    HOST_SLOT *hs = (HOST_SLOT *)slot;
    CARD8 msg[1] = { 14 };

    hs->no_dispatch_close = 1;

    log_write(LL_DEBUG, "Closing host session: %s", slot->id);

    aio_write_other(slot, NULL, msg, sizeof(msg));
    //aio_close_other(slot, 0);
}

static void fn_control_drop_participant(AIO_SLOT *slot)
{
    if (strcmp(slot->part_id, s_control_slot->cmd_part_id) == 0) {
        log_write(LL_DEBUG, "Dropping participant: %s", slot->part_id);
        aio_close_other(slot, 0);
    }
}

static void fn_control_enable_remote_control(AIO_SLOT *slot)
{
    CARD8 msg[2] = { 8, s_control_slot->cmd_flag };

    if (strcmp(slot->part_id, s_control_slot->cmd_part_id) == 0) {
        log_write(LL_DEBUG, "Enable remote control: %s (%d)", slot->part_id, s_control_slot->cmd_flag);
        aio_write_other(slot, NULL, msg, sizeof(msg));
    }
}

static void fn_control_enable_recording(AIO_SLOT *slot)
{
	AIO_SLOT *save = cur_slot;
	cur_slot = slot;
	host_request_recording(s_control_slot->cmd_flag, s_control_slot->cmd_time);
	cur_slot = save;
}
