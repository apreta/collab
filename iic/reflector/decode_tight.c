/* VNC Reflector
 * Copyright (C) 2001-2003 HorizonLive.com, Inc.  All rights reserved.
 * Copyright (C) 2000,2001 Constantin Kaplinsky.  All rights reserved.
 *
 * This software is released under the terms specified in the file LICENSE,
 * included.  HorizonLive provides e-Learning and collaborative synchronous
 * presentation solutions in a totally Web-based environment.  For more
 * information about HorizonLive, please see our website at
 * http://www.horizonlive.com.
 *
 * This software was authored by Constantin Kaplinsky <const@ce.cctpu.edu.ru>
 * and sponsored by HorizonLive.com, Inc.
 *
 * $Id: decode_tight.c,v 1.5 2005-03-11 16:34:27 mike Exp $
 * Decoding Tight-encoded rectangles.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <zlib.h>

#include "rfblib.h"
#include "reflector.h"
#include "async_io.h"
#include "logging.h"
#include "host_io.h"

/*
 * File-local data.
 */

#define COLOR_DEPTH (((HOST_SLOT*)cur_slot)->screen_info.pixformat.color_depth)
#define COLOR_LEN (((HOST_SLOT*)cur_slot)->screen_info.pixformat.color_depth / 8)

static void rf_host_tight_compctl(void);
static void rf_host_tight_fill(void);
static void rf_host_tight_filter(void);
static void rf_host_tight_numcolors(void);
static void rf_host_tight_palette(void);
static void rf_host_tight_raw(void);
static void rf_host_tight_indexed(void);
static void rf_host_tight_len1(void);
static void rf_host_tight_len2(void);
static void rf_host_tight_len3(void);
static void rf_host_tight_compressed(void);

static void tight_draw_truecolor_data(CARD8 *src);
static void tight_draw_indexed_data(CARD8 *src);
static void tight_draw_gradient_data(CARD8 *src);


void setread_decode_tight(FB_RECT *r)
{
  cur_slot->s_rect = *r;
  aio_setread(rf_host_tight_compctl, NULL, sizeof(CARD8));
}

static void rf_host_tight_compctl(void)
{
  CARD8 comp_ctl;

  fbs_spool_byte(cur_slot->readbuf[0] | cur_slot->s_reset_streams);
  fbupdate_rect_data(cur_slot, cur_slot->readbuf, 1);

  cur_slot->s_reset_streams = 0;

  /* Compression control byte */
  comp_ctl = cur_slot->readbuf[0];

  log_write(LL_DEBUG, "Tight compression type: %x", (unsigned)comp_ctl);
  
  /* Remove zlib stream flag bits */
  comp_ctl &= 0xF0;             /* clear bits 3..0 */

  if (comp_ctl == RFB_TIGHT_FILL) {
    aio_setread(rf_host_tight_fill, NULL, COLOR_LEN);
  }
  else if (comp_ctl == RFB_TIGHT_JPEG) {
    aio_setread(rf_host_tight_len1, NULL, 1);
  }
  else if (comp_ctl > RFB_TIGHT_MAX_SUBENCODING) {
    log_write(LL_ERROR, "Invalid sub-encoding in Tight-encoded data");
    aio_close(0);
    return;
  }
  else {                        /* "basic" compression */
    cur_slot->s_stream_id = (comp_ctl >> 4) & 0x03;
    if (comp_ctl & RFB_TIGHT_EXPLICIT_FILTER) {
      aio_setread(rf_host_tight_filter, NULL, 1);
    } else {
      cur_slot->s_filter_id = RFB_TIGHT_FILTER_COPY;
      cur_slot->s_uncompressed_size = ((cur_slot->s_rect.w * COLOR_DEPTH + 7) / 8) * cur_slot->s_rect.h;
      if (cur_slot->s_uncompressed_size < RFB_TIGHT_MIN_TO_COMPRESS) {
        aio_setread(rf_host_tight_raw, NULL, cur_slot->s_uncompressed_size);
      } else {
        aio_setread(rf_host_tight_len1, NULL, 1);
      }
    }
  }
}

static void rf_host_tight_fill(void)
{
  fbs_spool_data(cur_slot->readbuf, COLOR_LEN);
  fbupdate_rect_data(cur_slot, cur_slot->readbuf, COLOR_LEN);

  fbupdate_rect_done(cur_slot);
}

static void rf_host_tight_filter(void)
{
  fbs_spool_byte(cur_slot->readbuf[0]);
  fbupdate_rect_data(cur_slot, cur_slot->readbuf, 1);

  cur_slot->s_filter_id = cur_slot->readbuf[0];
  if (cur_slot->s_filter_id == RFB_TIGHT_FILTER_PALETTE) {
    aio_setread(rf_host_tight_numcolors, NULL, 1);
  } else {
    if (cur_slot->s_filter_id != RFB_TIGHT_FILTER_COPY &&
        cur_slot->s_filter_id != RFB_TIGHT_FILTER_GRADIENT) {
      log_write(LL_ERROR, "Unrecognized filter ID in the Tight decoder");
      aio_close(0);
      return;
    }
    cur_slot->s_uncompressed_size = ((cur_slot->s_rect.w * COLOR_DEPTH + 7) / 8) * cur_slot->s_rect.h;
    if (cur_slot->s_uncompressed_size < RFB_TIGHT_MIN_TO_COMPRESS) {
      aio_setread(rf_host_tight_raw, NULL, cur_slot->s_uncompressed_size);
    } else {
      aio_setread(rf_host_tight_len1, NULL, 1);
    }
  }
}

static void rf_host_tight_numcolors(void)
{
  fbs_spool_data(cur_slot->readbuf, 1);
  fbupdate_rect_data(cur_slot, cur_slot->readbuf, 1);

  cur_slot->s_num_colors = cur_slot->readbuf[0] + 1;
  aio_setread(rf_host_tight_palette, NULL, cur_slot->s_num_colors * COLOR_LEN);
}

static void rf_host_tight_palette(void)
{
  int row_size;

  fbs_spool_data(cur_slot->readbuf, cur_slot->s_num_colors * COLOR_LEN);
  fbupdate_rect_data(cur_slot, cur_slot->readbuf, cur_slot->s_num_colors * COLOR_LEN);

  row_size = (cur_slot->s_num_colors <= 2) ? (cur_slot->s_rect.w + 7) / 8 : cur_slot->s_rect.w;
  cur_slot->s_uncompressed_size = cur_slot->s_rect.h * row_size;
  if (cur_slot->s_uncompressed_size < RFB_TIGHT_MIN_TO_COMPRESS) {
    aio_setread(rf_host_tight_indexed, NULL, cur_slot->s_uncompressed_size);
  } else {
    aio_setread(rf_host_tight_len1, NULL, 1);
  }
}

static void rf_host_tight_raw(void)
{
  fbs_spool_data(cur_slot->readbuf, cur_slot->s_uncompressed_size);
  fbupdate_rect_data(cur_slot, cur_slot->readbuf, cur_slot->s_uncompressed_size);

  fbupdate_rect_done(cur_slot);
}

static void rf_host_tight_indexed(void)
{
  fbs_spool_data(cur_slot->readbuf, cur_slot->s_uncompressed_size);
  fbupdate_rect_data(cur_slot, cur_slot->readbuf, cur_slot->s_uncompressed_size);

  fbupdate_rect_done(cur_slot);
}

static void rf_host_tight_len1(void)
{
  fbs_spool_byte(cur_slot->readbuf[0]);
  fbupdate_rect_data(cur_slot, cur_slot->readbuf, 1);

  cur_slot->s_compressed_size = cur_slot->readbuf[0] & 0x7F;
  if (cur_slot->readbuf[0] & 0x80) {
    aio_setread(rf_host_tight_len2, NULL, 1);
  } else {
    aio_setread(rf_host_tight_compressed, NULL, cur_slot->s_compressed_size);
  }
}

static void rf_host_tight_len2(void)
{
  fbs_spool_byte(cur_slot->readbuf[0]);
  fbupdate_rect_data(cur_slot, cur_slot->readbuf, 1);

  cur_slot->s_compressed_size |= (cur_slot->readbuf[0] & 0x7F) << 7;
  if (cur_slot->readbuf[0] & 0x80) {
    aio_setread(rf_host_tight_len3, NULL, 1);
  } else {
    aio_setread(rf_host_tight_compressed, NULL, cur_slot->s_compressed_size);
  }
}

static void rf_host_tight_len3(void)
{
  fbs_spool_byte(cur_slot->readbuf[0]);
  fbupdate_rect_data(cur_slot, cur_slot->readbuf, 1);

  cur_slot->s_compressed_size |= (cur_slot->readbuf[0] & 0x7F) << 14;
  aio_setread(rf_host_tight_compressed, NULL, cur_slot->s_compressed_size);
}

static void rf_host_tight_compressed(void)
{
  /* Will handle zlib compressed or jpeg data */
  fbs_spool_data(cur_slot->readbuf, cur_slot->s_compressed_size);
  fbupdate_rect_data(cur_slot, cur_slot->readbuf, cur_slot->s_compressed_size);

  fbupdate_rect_done(cur_slot);
}

