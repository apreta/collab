/* VNC Reflector
 * Copyright (C) 2001-2003 HorizonLive.com, Inc.  All rights reserved.
 *
 * This software is released under the terms specified in the file LICENSE,
 * included.  HorizonLive provides e-Learning and collaborative synchronous
 * presentation solutions in a totally Web-based environment.  For more
 * information about HorizonLive, please see our website at
 * http://www.horizonlive.com.
 *
 * This software was authored by Constantin Kaplinsky <const@ce.cctpu.edu.ru>
 * and sponsored by HorizonLive.com, Inc.
 *
 * $Id: client_io.c,v 1.25 2008-01-31 19:26:10 mike Exp $
 * Asynchronous interaction with VNC clients.
 */
/*
 * Code heavily modified by imidio, SiteScape, and Novell for ICEcore/Kablink conferencing.
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2011 Apreta.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <zlib.h>

#include "rfblib.h"
#include "logging.h"
#include "async_io.h"
#include "reflector.h"
#include "omni_connect.h"
#include "host_io.h"
//#include "translate.h"
#include "client_io.h"
//#include "encode.h"
#include "../services/common/sharecommon.h"

static unsigned char *s_password;
static unsigned char *s_password_ro;

/*
 * Prototypes for static functions
 */

static void cf_client(void);
static void rf_client_ver(void);
static void rf_client_auth(void);
static void wf_client_auth_failed(void);
static void rf_client_initmsg(void);
static void rf_client_msg(void);
static void rf_client_pixfmt(void);
static void rf_client_colormap_hdr(void);
static void rf_client_colormap_data(void);
static void rf_client_encodings_hdr(void);
static void rf_client_encodings_data(void);
static void rf_client_updatereq(void);
static void wf_client_update_finished(void);
static void rf_client_keyevent(void);
static void rf_client_ptrevent(void);
static void rf_client_cuttext_hdr(void);
static void rf_client_cuttext_data(void);

static void set_trans_func(CL_SLOT *cl);
static void send_newfbsize(void);
static void send_queued_update();
static void send_update(void);
static void rf_client_speed(void);	// IIC

extern int s_request_tight;
extern int s_tight_level;
extern int s_jpeg_qual;

/*
 * Implementation
 */

void set_client_passwords(unsigned char *password, unsigned char *password_ro)
{
  s_password = password;
  s_password_ro = password_ro;
}

void af_client_init(void)
{
  cur_slot->type = TYPE_CL_LISTENING;
}

void af_client_accept(HOST_SLOT *hs)
{
  CARD8 session_header;
  CL_SLOT *cl = (CL_SLOT *)cur_slot;

  /* FIXME: Function naming is bad (client_accept_hook?). */

  cur_slot->type = TYPE_CL_SLOT;
  cl->connected = 0;
  cl->host_slot = hs;

  aio_setclose(cf_client);

  log_write(LL_MSG, "Accepted connection from %s (%s)", cur_slot->name, cur_slot->id);

  aio_write(NULL, "RFB 003.003\n", 12);
  aio_setread(rf_client_ver, NULL, 12);
}

static void cf_client(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;

  if (cl->host_slot!=NULL && cur_slot==cl->host_slot->controller) {	// 2004.4.14
      cl->host_slot->controller = NULL;
  }

  if (cur_slot->errread_f) {
    if (cur_slot->io_errno) {
      log_write(LL_WARN, "Error reading from %s(%s): %s(0x%x)",
                cur_slot->name, cur_slot->id, strerror(cur_slot->io_errno), cur_slot->io_errno);
    } else {
      log_write(LL_WARN, "Error reading from %s(%s)", cur_slot->name, cur_slot->id);
    }
  } else if (cur_slot->errwrite_f) {
    if (cur_slot->io_errno) {
      log_write(LL_WARN, "Error sending to %s(%s): %s",
                cur_slot->name, cur_slot->id, strerror(cur_slot->io_errno));
    } else {
      log_write(LL_WARN, "Error sending to %s(%s)", cur_slot->name, cur_slot->id);
    }
  } else if (cur_slot->errio_f) {
    log_write(LL_WARN, "I/O error, client %s(%s)", cur_slot->name, cur_slot->id);
  }
  log_write(LL_MSG, "Closing client connection %s(%s)", cur_slot->name, cur_slot->id);

  /* Let sharemgr know */
  if (!cl->no_dispatch_close)
      control_send_participant_left(cur_slot->id, cur_slot->part_id, ReasonDisconnected);

}

static void rf_client_ver(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  CARD8 msg[20];

  /* FIXME: Check protocol version. */

  /* FIXME: Functions like authentication should be available in
     separate modules, not in I/O part of the code. */
  /* FIXME: Higher level I/O functions should be implemented
     instead of things like buf_put_CARD32 + aio_write. */

  log_write(LL_DETAIL, "Client(%s) supports %.11s", cur_slot->id, cur_slot->readbuf);

  if (s_password[0]) {
    /* Request VNC authentication */
    buf_put_CARD32(msg, 2);

    /* Prepare "random" challenge */
    rfb_gen_challenge(cl->auth_challenge);
    memcpy(&msg[4], cl->auth_challenge, 16);

    /* Send both auth ID and challenge */
    aio_write(NULL, msg, 20);
    aio_setread(rf_client_auth, NULL, 16);
  } else {
    log_write(LL_WARN, "Not requesting authentication from %s(%s)",
              cur_slot->name, cur_slot->id);
    buf_put_CARD32(msg, 1);
    aio_write(NULL, msg, 4);
    aio_setread(rf_client_initmsg, NULL, 1);
  }
}

static void rf_client_auth(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  unsigned char resp_rw[16];
  unsigned char resp_ro[16];
  unsigned char msg[4];

  /* Place correct crypted responses to resp_rw, resp_ro */
  rfb_crypt(resp_rw, cl->auth_challenge, s_password);
  rfb_crypt(resp_ro, cl->auth_challenge, s_password_ro);

  /* Compare client response with correct ones */
  /* FIXME: Implement "too many tries" functionality some day. */
  if (memcmp(cur_slot->readbuf, resp_rw, 16) == 0) {
    cl->readonly = 0;
    log_write(LL_MSG, "Full-control authentication passed by %s(%s)",
              cur_slot->name, cur_slot->id);
  } else if (memcmp(cur_slot->readbuf, resp_ro, 16) == 0) {
    cl->readonly = 1;
    log_write(LL_MSG, "Read-only authentication passed by %s(%s)",
              cur_slot->name, cur_slot->id);
  } else {
    log_write(LL_WARN, "Authentication failed for %s(%s)", cur_slot->name, cur_slot->id);
    buf_put_CARD32(msg, 1);
    aio_write(wf_client_auth_failed, msg, 4);
    return;
  }

  buf_put_CARD32(msg, 0);
  aio_write(NULL, msg, 4);
  aio_setread(rf_client_initmsg, NULL, 1);
}

static void wf_client_auth_failed(void)
{
  aio_close(0);
}

static void rf_client_initmsg(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  HOST_SLOT *hs = cl->host_slot;

  unsigned char msg_server_init[24];

  if (cur_slot->readbuf[0] == 0) {
    log_write(LL_ERROR, "Non-shared session requested by %s(%s)", cur_slot->name, cur_slot->id);
    aio_close(0); //2004.3.16 should never get this
  }

  /* Save initial desktop geometry for this client */
  cl->fb_width = hs->screen_info.width;
  cl->fb_height = hs->screen_info.height;
  cl->enable_newfbsize = 0;

  /* Send ServerInitialisation message */
  /* N.B.  Due to incompatibility in tight encoding between vnc server and viewer,
           we always send original pixel format to new viewers.  The server ignores
           the RGB max values when encoding, but the viewer uses them when decoding. */
  buf_put_CARD16(msg_server_init, cl->fb_width);
  buf_put_CARD16(msg_server_init + 2, cl->fb_height);
  buf_put_pixfmt(msg_server_init + 4, &g_screen_info.pixformat);
  buf_put_CARD32(msg_server_init + 20, hs->screen_info.name_length);
  aio_write(NULL, msg_server_init, 24);
  aio_write(NULL, hs->screen_info.name, hs->screen_info.name_length);
  aio_setread(rf_client_msg, NULL, 1);

  /* Set up initial pixel format and encoders' parameters */
  memcpy(&cl->format, &g_screen_info.pixformat, sizeof(RFB_PIXEL_FORMAT));

  /* The client did not requested framebuffer updates yet */
  cl->update_requested = 0;
  cl->update_in_progress = 0;
  cl->newfbsize_pending = 0;

  /* need to request full screen update for this guy */
  cl->last_update = NULL;
  cl->last_sequence = 0;

  /* We are connected. */
  cl->connected = 1;
}

static void rf_client_msg(void)
{
  int msg_id;

  msg_id = (int)cur_slot->readbuf[0] & 0xFF;
  switch(msg_id) {
  case 0:                       /* SetPixelFormat */
    aio_setread(rf_client_pixfmt, NULL, 3 + sizeof(RFB_PIXEL_FORMAT));
    break;
  case 1:                       /* FixColourMapEntries */
    aio_setread(rf_client_colormap_hdr, NULL, 5);
    break;
  case 2:                       /* SetEncodings */
    aio_setread(rf_client_encodings_hdr, NULL, 3);
    break;
  case 3:                       /* FramebufferUpdateRequest */
    aio_setread(rf_client_updatereq, NULL, 9);
    break;
  case 4:                       /* KeyEvent */
    aio_setread(rf_client_keyevent, NULL, 7);
    break;
  case 5:                       /* PointerEvent */
    aio_setread(rf_client_ptrevent, NULL, 5);
    break;
  case 6:                       /* ClientCutText */
    aio_setread(rf_client_cuttext_hdr, NULL, 7);
    break;
  case 12:	// IIC request control
	{
      CL_SLOT *cl = (CL_SLOT *)cur_slot;
      CARD8 msg = 12;
      log_write(LL_INFO, "Client request remote control, %s", cur_slot->name);
      client_grant_control(cur_slot);
      pass_msg_to_host((AIO_SLOT*)cl->host_slot, &msg, 1);
	  aio_setread(rf_client_msg, NULL, 1);
	}
	break;
  case 13:	// IIC -- client connection speed
	{
      aio_setread(rf_client_speed, NULL, 2);
	}
	break;
  default:
    log_write(LL_ERROR, "Unknown client message type %d from %s(%s)",
              msg_id, cur_slot->name, cur_slot->id);
    aio_close(0);
  }
}

static void rf_client_speed(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  AIO_SLOT *saved_slot = cur_slot;
  HOST_SLOT* hs = cl->host_slot;

  CARD16 speed = buf_get_CARD16(&cur_slot->readbuf[1]);

//  RFB_SCREEN_INFO screen_info;	// save the original one
  int request_tight;
  int tight_level;
  int jpeg_qual;

  log_write(LL_INFO, "Client detect slow connection, %s - %dkbps",
        cur_slot->name, speed);

  request_tight = s_request_tight;
  tight_level = s_tight_level;
  jpeg_qual = s_jpeg_qual;

  hs->screen_info.pixformat.bits_pixel = 32;
  hs->screen_info.pixformat.color_depth = 24;
  hs->screen_info.pixformat.true_color = 1;
  hs->screen_info.pixformat.r_max = 255;	// iicshare will reduce color to 8 bit
  hs->screen_info.pixformat.g_max = 255;
  hs->screen_info.pixformat.b_max = 255;
  hs->screen_info.pixformat.r_shift = 16;
  hs->screen_info.pixformat.g_shift = 8;
  hs->screen_info.pixformat.b_shift = 0;

  s_jpeg_qual = 0;

  cur_slot = (AIO_SLOT*)cl->host_slot;
  host_set_formats(0);
  cur_slot = saved_slot;

  s_request_tight = request_tight;
  s_tight_level = tight_level;
  s_jpeg_qual = jpeg_qual;

  aio_setread(rf_client_msg, NULL, 1);
}

static void rf_client_pixfmt(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;

  buf_get_pixfmt(&cur_slot->readbuf[3], &cl->format);

  log_write(LL_DETAIL, "Pixel format (%d bpp) set by %s",
            cl->format.bits_pixel, cur_slot->name);

  aio_setread(rf_client_msg, NULL, 1);
}

static void rf_client_colormap_hdr(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;

  log_write(LL_WARN, "Ignoring FixColourMapEntries message from %s(%s)",
            cur_slot->name, cur_slot->id);

  cl->temp_count = buf_get_CARD16(&cur_slot->readbuf[3]);
  aio_setread(rf_client_colormap_data, NULL, cl->temp_count * 6);
}

static void rf_client_colormap_data(void)
{
  /* Nothing to do with FixColourMapEntries */
  aio_setread(rf_client_msg, NULL, 1);
}

static void rf_client_encodings_hdr(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;

  cl->temp_count = buf_get_CARD16(&cur_slot->readbuf[1]);
  aio_setread(rf_client_encodings_data, NULL, cl->temp_count * sizeof(CARD32));
}

static void rf_client_encodings_data(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  int i;
  int preferred_enc_set = 0;
  CARD32 enc;

  /* Reset encoding list (always enable raw encoding) */
  cl->enc_enable[RFB_ENCODING_RAW] = 1;
  cl->enc_prefer = RFB_ENCODING_RAW;
  cl->compress_level = -1;
  cl->jpeg_quality = -1;
  cl->enable_lastrect = 0;  /* better be true */
  cl->enable_newfbsize = 0;
  for (i = 1; i < NUM_ENCODINGS; i++)
    cl->enc_enable[i] = 0;

  /* Read and store encoding list supplied by the client */
  /* *** These settings are pretty much ignored */
  for (i = 0; i < (int)cl->temp_count; i++) {
    enc = buf_get_CARD32(&cur_slot->readbuf[i * sizeof(CARD32)]);
    if (!preferred_enc_set) {
      if ( enc == RFB_ENCODING_RAW ||
           enc == RFB_ENCODING_HEXTILE ||
           enc == RFB_ENCODING_TIGHT ) {
        cl->enc_prefer = (CARD8)enc;
        preferred_enc_set = 1;
      }
    }
    if (enc >= 0 && enc < NUM_ENCODINGS) {
      cl->enc_enable[enc] = 1;
    } else if (enc >= RFB_ENCODING_COMPESSLEVEL0 &&
               enc <= RFB_ENCODING_COMPESSLEVEL9 &&
               cl->compress_level == -1) {
      cl->compress_level = (int)(enc - RFB_ENCODING_COMPESSLEVEL0);
      log_write(LL_DETAIL, "Compression level %d requested by client %s",
                cl->compress_level, cur_slot->name);
    } else if (enc >= RFB_ENCODING_QUALITYLEVEL0 &&
               enc <= RFB_ENCODING_QUALITYLEVEL9 &&
               cl->jpeg_quality == -1) {
      cl->jpeg_quality = (int)(enc - RFB_ENCODING_QUALITYLEVEL0);
      log_write(LL_DETAIL, "JPEG quality level %d requested by client %s",
                cl->jpeg_quality, cur_slot->name);
    } else if (enc == RFB_ENCODING_LASTRECT) {
      log_write(LL_DETAIL, "Client %s supports LastRect markers",
                cur_slot->name);
      cl->enable_lastrect = 1;
    } else if (enc == RFB_ENCODING_NEWFBSIZE) {
      cl->enable_newfbsize = 1;
      log_write(LL_DETAIL, "Client %s supports desktop geometry changes",
                cur_slot->name);
    }
  }
  if (cl->compress_level < 0)
    cl->compress_level = 6;     /* default compression level */

  log_write(LL_DEBUG, "Encoding list set by %s", cur_slot->name);
  if (cl->enc_prefer == RFB_ENCODING_RAW) {
    log_write(LL_WARN, "Using raw encoding for client %s",
              cur_slot->name);
  } else if (cl->enc_prefer == RFB_ENCODING_TIGHT) {
    log_write(LL_DETAIL, "Using Tight encoding for client %s",
              cur_slot->name);
  } else if (cl->enc_prefer == RFB_ENCODING_HEXTILE) {
    log_write(LL_DETAIL, "Using Hextile encoding for client %s",
              cur_slot->name);
  }
  aio_setread(rf_client_msg, NULL, 1);
}

static void rf_client_updatereq(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;

/* Don't need this info currently
  BoxRec rect;

  rect.x1 = buf_get_CARD16(&cur_slot->readbuf[1]);
  rect.y1 = buf_get_CARD16(&cur_slot->readbuf[3]);
  rect.x2 = rect.x1 + buf_get_CARD16(&cur_slot->readbuf[5]);
  rect.y2 = rect.y1 + buf_get_CARD16(&cur_slot->readbuf[7]);
  cl->update_rect = rect;
*/
/*  CARD8 msg[10];
  msg[0] = 3;
  memcpy(&msg[1], cur_slot->readbuf, 9);
  if (msg[1]==0)	// get full screen update
  {
	pass_msg_to_host((AIO_SLOT*)cl->host_slot, msg, sizeof(msg));
  }
*/
  cl->update_requested = 1;

  if (!cur_slot->readbuf[0]) {
    log_write(LL_DEBUG, "Received framebuffer update request (full) from %s",
              cur_slot->name);
  } else {
    log_write(LL_DEBUG, "Received framebuffer update request from %s",
              cur_slot->name);
  }

  if (!cl->update_in_progress) {
    send_queued_update();
  }

  aio_setread(rf_client_msg, NULL, 1);
}

static void wf_client_update_finished(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;

  log_write(LL_DEBUG, "Finished sending framebuffer update to %s",
            cur_slot->name);

  /* All sends have completed */
  cl->update_in_progress = 0;

  /* If client already request more, start sending */
  if (cl->update_requested) {
    send_queued_update();
  }
  else {
    log_write(LL_DEBUG, "Waiting for update request from %s", cur_slot->name);
  }
}

static void rf_client_keyevent(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  CARD8 msg[8];

  if (!cl->readonly && cl->host_slot && cur_slot == cl->host_slot->controller) {	//IIC
    msg[0] = 4;                 /* KeyEvent */
    memcpy(&msg[1], cur_slot->readbuf, 7);
    pass_msg_to_host((AIO_SLOT*)cl->host_slot, msg, sizeof(msg));
  }

  aio_setread(rf_client_msg, NULL, 1);
}

static void rf_client_ptrevent(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  CARD16 x, y;
  CARD8 msg[6];

  if (!cl->readonly && cl->host_slot && cur_slot == cl->host_slot->controller) {
    msg[0] = 5;                 /* PointerEvent */
    msg[1] = cur_slot->readbuf[0];
    x = buf_get_CARD16(&cur_slot->readbuf[1]);
    y = buf_get_CARD16(&cur_slot->readbuf[3]);

    /* Pointer position should fit in the host screen */
    if (x >= cl->host_slot->screen_info.width)
      x = cl->host_slot->screen_info.width - 1;
    if (y >= cl->host_slot->screen_info.height)
      y = cl->host_slot->screen_info.height - 1;

    buf_put_CARD16(&msg[2], x);
    buf_put_CARD16(&msg[4], y);
    pass_msg_to_host((AIO_SLOT*)cl->host_slot, msg, sizeof(msg));
  }

  aio_setread(rf_client_msg, NULL, 1);
}

static void rf_client_cuttext_hdr(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;

  log_write(LL_DEBUG, "Receiving ClientCutText message from %s",
            cur_slot->name);

  cl->cut_len = (int)buf_get_CARD32(&cur_slot->readbuf[3]);
  aio_setread(rf_client_cuttext_data, NULL, cl->cut_len);
}

static void rf_client_cuttext_data(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;

  if (!cl->readonly && cl->host_slot && cur_slot == cl->host_slot->controller)	// IIC
    pass_cuttext_to_host((AIO_SLOT*)cl->host_slot, cur_slot->readbuf, cl->cut_len);

  aio_setread(rf_client_msg, NULL, 1);
}

/*
 * Functions called from host_io.c
 */


void fn_client_add_rect(AIO_SLOT *slot, FB_RECT *rect)
{
}

/* Called when update from host is complete */
void fn_client_send_rects(AIO_SLOT *slot)
{
  CL_SLOT *cl = (CL_SLOT *)slot;
  AIO_SLOT *saved_slot = cur_slot;

  if (!cl->update_in_progress && cl->update_requested) {
    cur_slot = slot;
    send_queued_update();
    cur_slot = saved_slot;
  }
}

void fn_client_send_cuttext(AIO_SLOT *slot, CARD8 *text, size_t len)
{
  CL_SLOT *cl = (CL_SLOT *)slot;
  AIO_SLOT *saved_slot = cur_slot;
  CARD8 svr_cuttext_hdr[8] = {
    3, 0, 0, 0, 0, 0, 0, 0
  };

  if (cl->connected) {
    cur_slot = slot;

    log_write(LL_DEBUG, "Sending ServerCutText message to %s", cur_slot->name);
    buf_put_CARD32(&svr_cuttext_hdr[4], (CARD32)len);
    aio_write(NULL, svr_cuttext_hdr, 8);
    if (len)
      aio_write(NULL, text, len);

    cur_slot = saved_slot;
  }
}

void fn_client_send_closesession(AIO_SLOT *slot)
{
  CL_SLOT *cl = (CL_SLOT *)slot;
  CARD8 msg[2] = {6, 0};

  if (cl->connected) {
    log_write(LL_DEBUG, "Sending close session message to %s", cur_slot->name);
    aio_write_other(slot, NULL, msg, 2);
  }
}

void fn_client_send_command(AIO_SLOT *slot, char * command, int len)
{
  CL_SLOT *cl = (CL_SLOT *)slot;
  CARD8 msg[8] = {0x10, 0, 0, 0};

  buf_put_CARD32(&msg[4], (CARD32)len);

  if (cl->connected) {
    log_write(LL_DEBUG, "Sending command to %s", cur_slot->name);
    aio_write_other(slot, NULL, msg, 8);
    aio_write_other(slot, NULL, command, len);
  }
}

/*
 * Non-callback functions
 */

static int put_rect_header(CARD8 *buf, FB_RECT *r)
{

  buf_put_CARD16(buf, r->x);
  buf_put_CARD16(&buf[2], r->y);
  buf_put_CARD16(&buf[4], r->w);
  buf_put_CARD16(&buf[6], r->h);
  buf_put_CARD32(&buf[8], r->enc);

  return 12;                    /* 12 bytes written */
}

static void send_newfbsize(void)
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  CARD8 msg_hdr[4] = {		/* msg id = 0 (framebuffer update) */
    0, 0, 0, 1
  };
  CARD8 rect_hdr[12];
  FB_RECT rect;

  log_write(LL_DEBUG, "Sending NewFBSize update (%dx%d) to %s",
            (int)cl->fb_width, (int)cl->fb_height, cur_slot->name);

  buf_put_CARD16(&msg_hdr[2], 1);
  aio_write(NULL, msg_hdr, 4);

  rect.x = 0;
  rect.y = 0;
  rect.w = cl->fb_width;
  rect.h = cl->fb_height;
  rect.enc = RFB_ENCODING_NEWFBSIZE;

  put_rect_header(rect_hdr, &rect);
  aio_write(wf_client_update_finished, rect_hdr, 12);

  /* Something has been queued for sending. */
  cl->update_in_progress = 1;
  cl->update_requested = 0;
}

/*
 * Send pending framebuffer update.
 */

/* Find last update sent to client */
static UPDATE *get_last_update()
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  HOST_SLOT *hs = cl->host_slot;
  UPDATE *upd = NULL;
  int count = 0;

  if (hs != NULL && hs->updates.head != NULL)
  {
  	/* Check for buffer underrun */
    if (cl->last_update != NULL && cl->last_sequence < hs->updates.head->sequence)
	  {
      cl->last_update = NULL;
	  }

	  /* See if new client (or underrun) */
	  if (cl->last_update == NULL)
	  {
      /* Unwind queue backwards looking for full screen update */
      /* If we can't find a candidate, request full screen */
      upd = hs->updates.tail;
      while (upd != NULL && !(upd->flags & UPDATE_FULLSCREEN) && (count <= RECT_MAX_BACKTRACK))
      {
          upd = upd->prev;
          if (upd!=NULL)
            count += upd->len;
      }

      if (upd == NULL || count > RECT_MAX_BACKTRACK)
      {
	      /* Need full screen update */
	      fn_request_fullscreen((AIO_SLOT *)hs);
	      upd = NULL;
      }
      else
      {
	      /* Point to sacrificial start of queue */
	      hs->first.next = upd;
	      upd = &hs->first;
      }
	  }
	  else
	  {
	    /* continue as normal */
	    upd = cl->last_update;
	  }
  }
  return upd;
}

/* Send rectangles from queue for this client */
static void send_queued_update()
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  CARD8 hdr[4] = { 0, 0, 0xFF, 0xFF };
  AIO_FUNCPTR fn = NULL;
  UPDATE *upd;
  CARD8 rect_hdr[12];
  FB_RECT rect;

  /* Process framebuffer size change. */
  if (cl->newfbsize_pending) {
    /* Update framebuffer size, clear newfbsize_pending flag. */
    cl->fb_width = cl->host_slot->screen_info.width;
    cl->fb_height = cl->host_slot->screen_info.height;
    cl->newfbsize_pending = 0;
    log_write(LL_DEBUG, "Applying new framebuffer size (%dx%d) to %s",
              (int)cl->fb_width, (int)cl->fb_height, cur_slot->name);

	/* If NewFBSize is supported by the client, send only NewFBSize
       pseudo-rectangle, pixel data will be sent in the next update. */
    if (cl->enable_newfbsize) {
//      host_free_queue();
      send_newfbsize();
      return;
    }
  }

  /* if no updates pending, wait */
  upd = get_last_update();
  if (upd == NULL || upd->next == NULL) {
	  log_write(LL_DEBUG, "Client update request pending updates from server");
	  return;
  }

  log_write(LL_DEBUG, "Sending framebuffer update to %s",
            cur_slot->name);

  /* Send header -- always using last rect encoding */
  aio_write(NULL, hdr, 4);

  /* For each of the usual pending rectangles: */

  upd = upd->next;	/* Stored pointer is to last sent, so advance to next */

  while (upd != NULL) {
    cl->last_update = upd;
    cl->last_sequence = upd->sequence;

	  if ((upd->flags & UPDATE_EMPTYRECT) == 0)
	  {
        log_write(LL_DETAIL, "Sending rectangle %dx%d at %d,%d to %s",
  		         (int)upd->rect.w, (int)upd->rect.h, (int)upd->rect.x, (int)upd->rect.y,
			     cur_slot->name);

// Skip this if sending last rect below
//	    if (cl->last_update->next == NULL)
//		    fn = wf_client_update_finished;

	    /* Copies data */
	    aio_write(fn, upd->data, upd->len);
	  }

	  upd = cl->last_update->next;
  }

  /* Send LastRect marker. */
  /* This rectangle is not stored in queue, so generate now */
  rect.x = rect.y = rect.w = rect.h = 0;
  rect.enc = RFB_ENCODING_LASTRECT;
  put_rect_header(rect_hdr, &rect);
  aio_write(wf_client_update_finished, rect_hdr, 12);

  /* Something has been queued for sending. */
  cl->update_in_progress = 1;

  cl->update_requested = 0;
}

void client_grant_control(AIO_SLOT* slot)
{
  AIO_SLOT* saved_slot = cur_slot;
  CARD8 msg[2];
  HOST_SLOT* host_slot = ((CL_SLOT*)cur_slot)->host_slot;
  if (host_slot && host_slot->controller!=NULL && slot != host_slot->controller) {
	msg[0] = 5;
	msg[1] = 0; // revoked
	cur_slot = host_slot->controller;
	aio_write(NULL, msg, 2);
	cur_slot = saved_slot;
  }

  msg[0] = 5;
  msg[1] = 1;	// granted
  cur_slot = slot;
  aio_write(NULL, msg, 2);
  cur_slot = saved_slot;
  if (host_slot) host_slot->controller = slot;
}

void fn_client_set_newfbsize_pending(AIO_SLOT *slot)
{
  CL_SLOT *cl = (CL_SLOT *)slot;
  cl->newfbsize_pending = 1;
}
