/* VNC Reflector
 * Copyright (C) 2001-2003 HorizonLive.com, Inc.  All rights reserved.
 *
 * This software is released under the terms specified in the file LICENSE,
 * included.  HorizonLive provides e-Learning and collaborative synchronous
 * presentation solutions in a totally Web-based environment.  For more
 * information about HorizonLive, please see our website at
 * http://www.horizonlive.com.
 *
 * This software was authored by Constantin Kaplinsky <const@ce.cctpu.edu.ru>
 * and sponsored by HorizonLive.com, Inc.
 *
 * $Id: host_io.c,v 1.34 2008-05-13 04:16:30 mike Exp $
 * Asynchronous interaction with VNC host.
 */
/*
 * Code heavily modified by imidio, SiteScape, and Novell for ICEcore/Kablink conferencing.
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2011 Apreta.
 */

#include <stdio.h>
#include <stdlib.h>
#ifdef LINUX
#include <unistd.h>
#endif
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <zlib.h>

#include "rfblib.h"
#include "reflector.h"
#include "async_io.h"
#include "logging.h"
//#include "translate.h"
#include "client_io.h"
#include "host_connect.h"
#include "host_io.h"
//#include "encode.h"
#include "port.h"
#include "../services/common/sharecommon.h"

static void host_really_activate(AIO_SLOT *slot);
static void fn_host_pass_newfbsize(AIO_SLOT *slot);

static void rf_host_msg(void);

static void rf_host_fbupdate_hdr(void);
static void rf_host_fbupdate_recthdr(void);
static void rf_host_fbupdate_raw(void);
static void rf_host_copyrect(void);

static void host_queue_rect(AIO_SLOT *host_slot);
static void host_tag_lastrect(AIO_SLOT *host_slot);
static void host_tag_fullscreen(AIO_SLOT *host_slot);
static void fn_host_add_client_rect(AIO_SLOT *slot);

static void rf_host_colormap_hdr(void);
static void rf_host_colormap_data(void);

static void rf_host_cuttext_hdr(void);
static void rf_host_cuttext_data(void);
static void fn_host_pass_cuttext(AIO_SLOT *slot);
static void fn_host_pass_closesession(AIO_SLOT *slot);
static void fn_host_pass_command(AIO_SLOT *slot);

static void reset_framebuffer(void);
void request_update(int incr);
static void fn_host_close_client(AIO_SLOT *slot, void *p);

static void rf_host_grantcontrol_hdr(void);
static void rf_host_closesession_hdr(void);
static void set_host(HOST_SLOT* host, char* meeting_id);
static void rf_host_enablerecording_hdr();	// 2004.10.13 BL
static void rf_host_command_hdr();
static void rf_host_command_msg();

static void set_last_update();
static void fn_expire_connection();

/*
 * Implementation
 */

/* Prepare host I/O slot for operating in main protocol phase */
void host_activate(HOST_SLOT* host_slot)
{
  if (!host_slot->activated) {
    /* Just activate */
    host_really_activate(cur_slot);
  } else {
    /* Let close hook do the work */
    log_write(LL_ERROR, "Tried to activate an existing host connection (%s)", cur_slot->id);
  }
}

/* On-close hook */
void host_close_hook()
{
  UPDATE *upd;
  HOST_SLOT *hs = (HOST_SLOT *)cur_slot;

  if (cur_slot->type == TYPE_HOST_ACTIVE_SLOT) {
    if (hs != NULL && hs->rect_buf != NULL) {
        free(hs->rect_buf);
        hs->rect_buf = NULL;
        hs->buf_len = 0;
    }

    /* Close session file if open  */
    fbs_close_file();

    log_write(LL_MSG, "Closing all the client connections");

    /* Close related clients */
    aio_walk_slots(fn_host_close_client, TYPE_CL_SLOT, cur_slot->id);
    aio_walk_slots(fn_host_close_client, TYPE_CL_LISTENING, cur_slot->id);

    log_write(LL_MSG, "Closed all the client connections, meeting id = %s", cur_slot->id);

    /* Free queue */
    upd = hs->updates.head;
    while (upd != NULL)
    {
        UPDATE *cur = upd;
        upd = upd->next;
        free(cur);
    }
    hs->updates.head = NULL;
    hs->updates.tail = NULL;
    hs->updates.total_len = 0;
    hs->updates.size = 0;

    /* No active slot exist */
    hs->activated = 0;
  }

  if (cur_slot->errread_f) {
    if (cur_slot->io_errno) {
      log_write(LL_ERROR, "Host(%s) I/O error, read: %s",
                cur_slot->id, _strerror(cur_slot->io_errno));
    } else {
      log_write(LL_ERROR, "Host(%s) I/O error, read", cur_slot->id);
    }
  } else if (cur_slot->errwrite_f) {
    if (cur_slot->io_errno) {
      log_write(LL_ERROR, "Host(%s) I/O error, write: %s",
                cur_slot->id, _strerror(cur_slot->io_errno));
    } else {
      log_write(LL_ERROR, "Host(%s) I/O error, write", cur_slot->id);
    }
  } else if (cur_slot->errio_f) {
    log_write(LL_ERROR, "Host(%s) I/O error", cur_slot->id);
  }

  log_write(LL_WARN, "Closing connection to host, meeting id=%s", cur_slot->id);
  remove_active_file();

  /* Let sharemgr know */
  if (!hs->no_dispatch_close)
      control_send_participant_left(cur_slot->id, cur_slot->part_id, ReasonDisconnected);
  control_send_end_session(cur_slot->id, ReasonNormal);

}

static void host_really_activate(AIO_SLOT *slot)
{
  AIO_SLOT *saved_slot = cur_slot;
  HOST_SLOT *hs = (HOST_SLOT *)slot;

  hs->activated = 1;
  hs->enable_recording = 0;
  hs->recording_enabled = 0;
  hs->update_received = 0;
  hs->new_session = 1;  // in case using the same dump file, and previous session was not complete

  log_write(LL_MSG, "Activating new host connection (%s)", cur_slot->id);
  slot->type = TYPE_HOST_ACTIVE_SLOT;

  set_host(hs, slot->id);	// 2004.3.15 BL -- in case viewer is connected before host in error reconnecting

  write_active_file();
  perform_action("host_activate");

  /* Set default desktop geometry for new client connections */
  hs->screen_info.width = hs->fb_width;
  hs->screen_info.height = hs->fb_height;

  cur_slot = slot;

  /* Request initial screen contents */
  log_write(LL_DETAIL, "Requesting full framebuffer update");
  request_update(0);
  aio_setread(rf_host_msg, NULL, 1);

  /* Notify clients about desktop geometry change */
  aio_walk_slots(fn_host_pass_newfbsize, TYPE_CL_SLOT, slot->id);

  control_send_session_active(cur_slot->id);

  cur_slot = saved_slot;
}

/*
 * Inform a client about new desktop geometry.
 */

static void fn_host_pass_newfbsize(AIO_SLOT *slot)
{
  HOST_SLOT *hs = (HOST_SLOT *)cur_slot;
  FB_RECT r;

  r.enc = RFB_ENCODING_NEWFBSIZE;
  r.x = r.y = 0;
  r.w = hs->fb_width;
  r.h = hs->fb_height;
  fn_client_add_rect(slot, &r);
}

/***************************/
/* Processing RFB messages */
/***************************/

static void rf_host_msg(void)
{
  int msg_id;

  /* set timeout */
  set_last_update();

  msg_id = (int)cur_slot->readbuf[0] & 0xFF;
  switch(msg_id) {
  case 0:                       /* FramebufferUpdate */
    aio_setread(rf_host_fbupdate_hdr, NULL, 3);
    break;
  case 1:                       /* SetColourMapEntries */
    aio_setread(rf_host_colormap_hdr, NULL, 5);
    break;
  case 2:                       /* Bell */
    log_write(LL_DETAIL, "Received Bell message from host");
//    fbs_write_data(cur_slot->readbuf, 1);	// 2004.10.13 no controll msg
    aio_setread(rf_host_msg, NULL, 1);
    break;
  case 3:                       /* ServerCutText */
    aio_setread(rf_host_cuttext_hdr, NULL, 7);
    break;
  case 5:	/* IIC grant control -- alway disable remote control*/
    log_write(LL_MSG, "Host is requiring control (%s)", cur_slot->id);
    aio_setread(rf_host_grantcontrol_hdr, NULL, 1);	// IIC
	break;
  case 6:	/* IIC close session -- host is letting clients know that session is over */
    log_write(LL_MSG, "Host is ending session (%s)", cur_slot->id);
    aio_setread(rf_host_closesession_hdr, NULL, 1);	// IIC
	break;
  case 7:	/* IIC enable recording */
    log_write(LL_MSG, "Enable/Disable recording (%s)", cur_slot->id);
    aio_setread(rf_host_enablerecording_hdr, NULL, 7);	// IIC
	break;
  case 0x10:  /* IIC command */
    log_write(LL_MSG, "Command message (%s)", cur_slot->id);
    aio_setread(rf_host_command_hdr, NULL, 7);
    break;
  default:
    log_write(LL_ERROR, "Unknown server message type: %d (%s)", msg_id, cur_slot->id);
    aio_close(0);
  }
}

/***********************************/
/* IIC                             */
/***********************************/

static void rf_host_grantcontrol_hdr()
{
  CARD8 msg[2];
  HOST_SLOT *host_slot = (HOST_SLOT*)cur_slot;
  msg[0] = 5;	// msgid
  msg[1] = 0;
  if (msg[1]==0 && host_slot!=NULL && host_slot->controller!=NULL) {
      /* pass message through to appropriate viewer */
	  AIO_SLOT* saved = cur_slot;
	  cur_slot = host_slot->controller;
	  aio_write(NULL, msg, 2);
	  cur_slot = saved;
	  host_slot->controller = NULL;
  }
  aio_setread(rf_host_msg, NULL, 1);
}

static void rf_host_enablerecording_hdr()
{
  HOST_SLOT *host_slot = (HOST_SLOT*)cur_slot;

  /* Get desired recording state */
  int enable_recording = cur_slot->readbuf[0]!=0?1:0;
  int start_time = buf_get_CARD32(&cur_slot->readbuf[3]);

  host_enable_recording(enable_recording, start_time);  // XXX not sure about start time

  /* Ignore unless recording state changed */
  aio_setread(rf_host_msg, NULL, 1);
}

void host_request_recording(int enable_recording, int start_time)
{
  char msg[8] = { 0x11, enable_recording };
  HOST_SLOT *host_slot = (HOST_SLOT*)cur_slot;

  buf_put_CARD32(&msg[4], start_time);

  log_write(LL_DEBUG, "Requesting record change: %s", enable_recording ? "enable" : "disable");
  aio_write(NULL, msg, sizeof(msg));

  // Also request full screen update if we want recording
  if (enable_recording && !host_slot->enable_recording && host_slot->update_received) {
    host_slot->requested_update = TRUE;
    fn_request_fullscreen((AIO_SLOT *)host_slot);
    request_update(0);	// send the request to host immediately
  }
}

void host_enable_recording(int enable_recording, int start_time)
{
  char msg[2];
  HOST_SLOT *host_slot = (HOST_SLOT*)cur_slot;

  /* The server will only send this message outside of the framebuffer update so
     it's safe to handle it immediately */
  if (enable_recording != host_slot->enable_recording) {
    host_slot->enable_recording = enable_recording;
    if (host_slot->enable_recording) {
        host_slot->recording_enabled = 1;
    }
    host_slot->start_time = start_time;
    msg[0] = 7;
    msg[1] = host_slot->enable_recording ? 1 : 0;
    fbs_spool_data2(msg, 2);
    log_write(LL_MSG, "Recording is %s (%s), timestamp=%u", (host_slot->enable_recording?"enabled":"disabled"), cur_slot->id, host_slot->start_time);
  }
}

static void rf_host_closesession_hdr()
{
  HOST_SLOT *host_slot = (HOST_SLOT*)cur_slot;

  /* Pass message to viewers */
  aio_walk_slots(fn_host_pass_closesession, TYPE_CL_SLOT, cur_slot->id);

  aio_setread(rf_host_msg, NULL, 1);
}

static void fn_host_pass_closesession(AIO_SLOT *slot)
{
  fn_client_send_closesession(slot);
}

static void rf_host_command_hdr()
{
  int cmd_len;
  if (cur_slot->readbuf[0]==0)	// idle msg
  {
    /* host has not sent any updates for 2(?) min, try to resume the protocol */
    log_write(LL_INFO, "Host is idle, requesting incremental framebuffer update");
    request_update(1);
  }

  cmd_len = buf_get_CARD32(&cur_slot->readbuf[3]);

  aio_setread(rf_host_command_msg, NULL, cmd_len);
}

static void rf_host_command_msg()
{
  HOST_SLOT *hs = (HOST_SLOT *)cur_slot;

  log_write(LL_DEBUG, "Command string received");

  /* Send command message to clients (essentially out-of-band, may need to
     provide option to queue some messages) */
  aio_walk_slots(fn_host_pass_command, TYPE_CL_SLOT, cur_slot->id);

  aio_setread(rf_host_msg, NULL, 1);
}

static void fn_host_pass_command(AIO_SLOT *slot)
{
  CL_SLOT *cl = (CL_SLOT *)slot;
  AIO_SLOT *host = (AIO_SLOT *)cl->host_slot;

  if (host != NULL)
  {
      fn_client_send_command(slot, host->readbuf, host->bytes_ready);
  }
}

/********************************/
/* Handling framebuffer updates */
/********************************/

static void rf_host_fbupdate_hdr(void)
{
  CARD8 hdr_buf[4];
  HOST_SLOT *hs = (HOST_SLOT *)cur_slot;

  hs->update_received = TRUE;

  cur_slot->rect_count = buf_get_CARD16(&cur_slot->readbuf[1]);

  if (cur_slot->rect_count == 0xFFFF) {
    log_write(LL_DETAIL, "Receiving framebuffer update");
  } else {
    log_write(LL_DETAIL, "Receiving framebuffer update, %d rectangle(s)",
              cur_slot->rect_count);
  }

  hdr_buf[0] = 0; /* msgid = 0 (framebuffer update) */
  memcpy(&hdr_buf[1], cur_slot->readbuf, 3);
  fbs_spool_data(hdr_buf, 4);
  /* Header itself is not queued */

  if (cur_slot->rect_count) {
    aio_setread(rf_host_fbupdate_recthdr, NULL, 12);
  } else {
	log_write(LL_DEBUG, "Requesting incremental framebuffer update");
	request_update(1);
    aio_setread(rf_host_msg, NULL, 1);
  }
}

static void rf_host_fbupdate_recthdr(void)
{
  HOST_SLOT *hs = (HOST_SLOT *)cur_slot;

  fbupdate_rect_start(cur_slot);

  cur_slot->cur_rect.x = buf_get_CARD16(cur_slot->readbuf);
  cur_slot->cur_rect.y = buf_get_CARD16(&cur_slot->readbuf[2]);
  cur_slot->cur_rect.w = buf_get_CARD16(&cur_slot->readbuf[4]);
  cur_slot->cur_rect.h = buf_get_CARD16(&cur_slot->readbuf[6]);
  cur_slot->cur_rect.enc = buf_get_CARD32(&cur_slot->readbuf[8]);

  fbs_spool_data(cur_slot->readbuf, 12);
  fbupdate_rect_data(cur_slot, cur_slot->readbuf, 12);

  /* Handle LastRect "encoding" first */
  if (cur_slot->cur_rect.enc == RFB_ENCODING_LASTRECT) {
    log_write(LL_DEBUG, "LastRect marker received from the host");
    cur_slot->cur_rect.x = cur_slot->cur_rect.y = 0;
    cur_slot->rect_count = 1;
    fbupdate_rect_done(cur_slot);
    return;
  }

  /* Ignore zero-size rectangles */
  if (cur_slot->cur_rect.h == 0 || cur_slot->cur_rect.w == 0) {
    log_write(LL_WARN, "Zero-size rectangle %dx%d at %d,%d (ignoring) (%s)",
              (int)cur_slot->cur_rect.w, (int)cur_slot->cur_rect.h,
              (int)cur_slot->cur_rect.x, (int)cur_slot->cur_rect.y, cur_slot->id);
    fbupdate_rect_done(cur_slot);
    return;
  }

  /* Handle NewFBSize "encoding", as a special case */
  if (cur_slot->cur_rect.enc == RFB_ENCODING_NEWFBSIZE) {
    UPDATE *upd;
    log_write(LL_INFO, "Free queue, meeting id = %s", cur_slot->id);
	/* Free queue */
	upd = hs->updates.head;
	while (upd != NULL)
	{
		UPDATE *cur = upd;
		upd = upd->next;
		free(cur);
	}
	hs->updates.head = NULL;
	hs->updates.tail = NULL;
    hs->updates.total_len = 0;
    hs->updates.size = 0;

    log_write(LL_INFO, "New host desktop geometry: %dx%d",
              (int)cur_slot->cur_rect.w, (int)cur_slot->cur_rect.h);
    hs->screen_info.width = hs->fb_width = cur_slot->cur_rect.w;
    hs->screen_info.height = hs->fb_height = cur_slot->cur_rect.h;

    cur_slot->cur_rect.x = cur_slot->cur_rect.y = 0;

	// IIC
    aio_walk_slots(fn_client_set_newfbsize_pending, TYPE_CL_SLOT, cur_slot->id);

	/* NewFBSize is always the last rectangle regardless of rect_count */
    cur_slot->rect_count = 1;
    fbupdate_rect_done(cur_slot);
    hs->requested_fullscreen = TRUE;
    return;
  }

  /* Ok, now the rectangle seems correct */
  log_write(LL_DETAIL, "Receiving rectangle %dx%d at %d,%d",
            (int)cur_slot->cur_rect.w, (int)cur_slot->cur_rect.h,
            (int)cur_slot->cur_rect.x, (int)cur_slot->cur_rect.y);

  switch(cur_slot->cur_rect.enc) {
  case RFB_ENCODING_RAW:
    log_write(LL_DEBUG, "Receiving raw data, expecting %d byte(s)",
              cur_slot->cur_rect.w * cur_slot->cur_rect.h * sizeof(CARD32));
    cur_slot->rect_cur_row = 0;
    aio_setread(rf_host_fbupdate_raw,
                NULL,
                cur_slot->cur_rect.w * sizeof(CARD32));
    break;
  case RFB_ENCODING_COPYRECT:
    log_write(LL_DEBUG, "Receiving CopyRect instruction");
    aio_setread(rf_host_copyrect, NULL, 4);
    break;
  case RFB_ENCODING_HEXTILE:
    log_write(LL_DEBUG, "Receiving Hextile-encoded data");
    setread_decode_hextile(&cur_slot->cur_rect);
    break;
  case RFB_ENCODING_TIGHT:
    log_write(LL_DEBUG, "Receiving Tight-encoded data");
    setread_decode_tight(&cur_slot->cur_rect);
    break;
  default:
    log_write(LL_ERROR, "Unknown encoding: 0x%08lX (%s)",
              (unsigned long)cur_slot->cur_rect.enc, cur_slot->id);
    aio_close(0);
  }
}

static void rf_host_fbupdate_raw(void)
{
  int idx;
  HOST_SLOT *hs = (HOST_SLOT *)cur_slot;

  fbs_spool_data(cur_slot->readbuf, cur_slot->cur_rect.w * sizeof(CARD32));
  fbupdate_rect_data(cur_slot, cur_slot->readbuf, cur_slot->cur_rect.w * sizeof(CARD32));

  if (++cur_slot->rect_cur_row < cur_slot->cur_rect.h) {
    /* Read next row */
	/* *** Width is correct below? */
    idx = (cur_slot->cur_rect.y + cur_slot->rect_cur_row) * (int)hs->screen_info.width + cur_slot->cur_rect.x;
    aio_setread(rf_host_fbupdate_raw, NULL, cur_slot->cur_rect.w * sizeof(CARD32));
  } else {
    /* Done with this rectangle */
    fbupdate_rect_done(cur_slot);
  }
}

static void rf_host_copyrect(void)
{

  fbs_spool_data(cur_slot->readbuf, 4);
  fbupdate_rect_data(cur_slot, cur_slot->readbuf, 4);

  fbupdate_rect_done(cur_slot);
}

/********************************/
/* Functions called by decoders */
/********************************/


void fbupdate_rect_start(AIO_SLOT *host_slot)
{
  HOST_SLOT * hs = (HOST_SLOT *)host_slot;
  if (hs != NULL) {
    hs->buf_pos = 0;
  }
}

void fbupdate_rect_data(AIO_SLOT *host_slot, void *data, int len)
{
  HOST_SLOT * hs = (HOST_SLOT *)host_slot;
  if (hs != NULL) {
    if (hs->buf_pos + len > hs->buf_len) {
	  hs->buf_len += len > RECT_BUF_MIN_INC ? len  : RECT_BUF_MIN_INC;
	  hs->rect_buf = realloc(hs->rect_buf, hs->buf_len);
	}
	memcpy(hs->rect_buf + hs->buf_pos, data, len);
	hs->buf_pos += len;
  }
}

static void spool_full_screen(HOST_SLOT * hs)
{
  UPDATE *upd = hs->updates.tail;
  if (upd != NULL)
  {
    // Simulate a full frame buffer update
    // We've just enabled recording and requested and received a full screen update
    CARD8 hdr[4] = { 0, 0, 0xFF, 0xFF };
	CARD8 buf[12];

    log_write(LL_DEBUG, "Spool full screen: %d", upd->len);

    fbs_spool_data(hdr, 4);
    fbs_spool_data(upd->data, upd->len);
  }
}

/*
 * This function is called by decoders after the whole rectangle
 * has been successfully decoded.
 */
void fbupdate_rect_done(AIO_SLOT *host_slot)
{
  HOST_SLOT *hs = (HOST_SLOT *)host_slot;

  if (cur_slot->cur_rect.w != 0 && cur_slot->cur_rect.h != 0) {
    log_write(LL_DEBUG, "Received rectangle ok");

    /* Save data in a file if necessary */
    fbs_flush_data();

	log_write(LL_DEBUG, "Received %d bytes for rect (%d, %d, %d, %d)", hs->buf_pos,
		cur_slot->cur_rect.x, cur_slot->cur_rect.y,	cur_slot->cur_rect.w, cur_slot->cur_rect.h);

	if (cur_slot->cur_rect.enc != RFB_ENCODING_LASTRECT && cur_slot->cur_rect.enc != RFB_ENCODING_NEWFBSIZE)
	{
	  host_queue_rect(host_slot);
	}

	if (hs->requested_fullscreen)
	{
      /* this update should contain a full screen */
      log_write(LL_DEBUG, "Received full screen update");
      host_tag_fullscreen(host_slot);
	  hs->requested_fullscreen = FALSE;
	  if (hs->requested_update)
	  {
		hs->requested_update = FALSE;
		spool_full_screen(hs);
	  }
	}
  }

  if (--cur_slot->rect_count) {
    aio_walk_slots(fn_client_send_rects, TYPE_CL_SLOT, host_slot->id);
    aio_setread(rf_host_fbupdate_recthdr, NULL, 12);
  } else {
    /* Done with the whole update */
	host_tag_lastrect(host_slot);
    aio_walk_slots(fn_client_send_rects, TYPE_CL_SLOT, host_slot->id);

	log_write(LL_DEBUG, "Requesting incremental framebuffer update");
	request_update(1);

    aio_setread(rf_host_msg, NULL, 1);
  }
}

/* Add latest rect to host queue.  Expunge old updates if necessary. */
static void host_queue_rect(AIO_SLOT *host_slot)
{
  HOST_SLOT *hs = (HOST_SLOT *)host_slot;
  UPDATE *upd;

  /* Make update descriptor */
  /* *** Just create AIO_BLOCK for data */
  upd = (UPDATE *)calloc(1, sizeof(UPDATE) + hs->buf_pos);
  upd->sequence = hs->update_sequence++;
  upd->len = hs->buf_pos;
  upd->rect = cur_slot->cur_rect;
  upd->next = NULL;
  upd->prev = NULL;
  memcpy(upd->data, hs->rect_buf, upd->len);

  if (hs->updates.tail != NULL)
  {
	upd->prev = hs->updates.tail;
	hs->updates.tail->next = upd;
  }
  else
  {
	hs->updates.head = upd;
  }
  hs->updates.tail = upd;

  if (upd->len > RECT_MAX_QUEUE_BYTES)
      log_write(LL_WARN, "Large update rectangle received (%d)", upd->len);

  hs->updates.total_len += upd->len;    // track the total size of the queue

  ++hs->updates.size;
  while (hs->updates.size > RECT_MAX_QUEUE || hs->updates.total_len > RECT_MAX_QUEUE_BYTES)
  {
    upd = hs->updates.head;
    if (upd == hs->updates.tail)
    {
        // Don't remove last rectanle (one we just added)
        log_write(LL_DETAIL, "Update queue emptied (total size: %d)", hs->updates.total_len);
        break;
    }
    hs->updates.total_len -= upd->len;
	hs->updates.head = upd->next;
    if (upd->next != NULL)
       upd->next->prev = NULL;
	free(upd);
	--hs->updates.size;
  }
}

/* Make last update in update stream */
static void host_tag_lastrect(AIO_SLOT *host_slot)
{
  HOST_SLOT *hs = (HOST_SLOT *)host_slot;
  UPDATE *upd = hs->updates.tail;
  if (upd != NULL)
  {
    upd->flags |= UPDATE_LASTRECT;
  }
}

/* Make last update in update stream */
static void host_tag_fullscreen(AIO_SLOT *host_slot)
{
  HOST_SLOT *hs = (HOST_SLOT *)host_slot;
  UPDATE *upd = hs->updates.tail;
  if (upd != NULL)
  {
    upd->flags |= UPDATE_FULLSCREEN;
  }
}

static void fn_host_add_client_rect(AIO_SLOT *slot)
{
  fn_client_add_rect(slot, &cur_slot->cur_rect);
}

/*****************************************/
/* Handling SetColourMapEntries messages */
/*****************************************/

static void rf_host_colormap_hdr(void)
{
  CARD16 num_colors;

  log_write(LL_WARN, "Ignoring SetColourMapEntries message (%s)", cur_slot->id);

  num_colors = buf_get_CARD16(&cur_slot->readbuf[3]);
  if (num_colors > 0)
    aio_setread(rf_host_colormap_data, NULL, num_colors * 6);
  else
    aio_setread(rf_host_msg, NULL, 1);
}

static void rf_host_colormap_data(void)
{
  /* Nothing to do with colormap */
  aio_setread(rf_host_msg, NULL, 1);
}

/***********************************/
/* Handling ServerCutText messages */
/***********************************/

static void rf_host_cuttext_hdr(void)
{
  log_write(LL_DETAIL,
            "Receiving ServerCutText message from host, %lu byte(s)",
            (unsigned long)cur_slot->cut_len);

  cur_slot->cut_len = (size_t)buf_get_CARD32(&cur_slot->readbuf[3]);
  if (cur_slot->cut_len > 0)
    aio_setread(rf_host_cuttext_data, NULL, cur_slot->cut_len);
  else
    rf_host_cuttext_data();
}

static void rf_host_cuttext_data(void)
{
  cur_slot->cut_text = cur_slot->readbuf;
  aio_walk_slots(fn_host_pass_cuttext, TYPE_CL_SLOT, cur_slot->id);
  aio_setread(rf_host_msg, NULL, 1);
}

static void fn_host_pass_cuttext(AIO_SLOT *slot)
{
  fn_client_send_cuttext(slot, cur_slot->cut_text, cur_slot->cut_len);
}

/*************************************/
/* Functions called from client_io.c */
/*************************************/

/* At least one client needs a full screen update */
void fn_request_fullscreen(AIO_SLOT *slot)
{
  HOST_SLOT *hs = (HOST_SLOT *)slot;

  if (slot->type == TYPE_HOST_ACTIVE_SLOT) {
    if (hs->need_fullscreen == FALSE) {
      log_write(LL_MSG, "Requesting full screen update for new client, (%s)", cur_slot->id);
      hs->need_fullscreen = TRUE;
    }
  }
}

/* FIXME: Function naming. Have to invent consistent naming rules. */

void pass_msg_to_host(AIO_SLOT *host_slot, CARD8 *msg, size_t len)
{
  AIO_SLOT *saved_slot = cur_slot;

  if (host_slot != NULL) {
    cur_slot = host_slot;
    aio_write(NULL, msg, len);
    cur_slot = saved_slot;
  }
}

void pass_cuttext_to_host(AIO_SLOT *host_slot, CARD8 *text, size_t len)
{
  AIO_SLOT *saved_slot = cur_slot;
  CARD8 client_cuttext_hdr[8] = {
    6, 0, 0, 0, 0, 0, 0, 0
  };

  if (host_slot != NULL) {
    buf_put_CARD32(&client_cuttext_hdr[4], (CARD32)len);

    cur_slot = host_slot;
    aio_write(NULL, client_cuttext_hdr, sizeof(client_cuttext_hdr));
    aio_write(NULL, text, len);
    cur_slot = saved_slot;
  }
}

/********************/
/* Helper functions */
/********************/

/*
 * Send a FramebufferUpdateRequest for the whole screen
 */

void request_update(int incr)
{
  HOST_SLOT *hs = (HOST_SLOT *)cur_slot;
  unsigned char fbupdatereq_msg[] = {
    3,                          /* Message id */
    0,                          /* Incremental if 1 */
    0, 0, 0, 0,                 /* X position, Y position */
    0, 0, 0, 0                  /* Width, height */
  };

  /* If a client needs a full screen update, override incremental update setting */
  if (hs->need_fullscreen)
  {
	  log_write(LL_DEBUG, "Client requested full screen update, changing request");
	  incr = 0;
	  hs->need_fullscreen = FALSE;
  }

  if (incr == 0)
  {
      hs->requested_fullscreen = TRUE;
  }

  fbupdatereq_msg[1] = (incr) ? 1 : 0;
  buf_put_CARD16(&fbupdatereq_msg[6], hs->fb_width);
  buf_put_CARD16(&fbupdatereq_msg[8], hs->fb_height);

  log_write(LL_DEBUG, "Sending FramebufferUpdateRequest message");
  aio_write(NULL, fbupdatereq_msg, sizeof(fbupdatereq_msg));
}

static void fn_host_close_client(AIO_SLOT *slot, void *param)
{
  CL_SLOT *cl = (CL_SLOT *)slot;

  // 2004.6.18 fixed the segment fault in cf_client
  if (cl->host_slot!=NULL) {
      cl->no_dispatch_close = cl->host_slot->no_dispatch_close;
	  cl->host_slot = NULL;
  }

  aio_close_other(slot, 0);
}

void host_free_queue()
{
  CL_SLOT *cl = (CL_SLOT *)cur_slot;
  HOST_SLOT *hs = cl->host_slot;
  UPDATE *upd;
  /* Free queue */
  upd = hs->updates.head;
  while (upd != NULL)
  {
 	UPDATE *cur = upd;
	upd = upd->next;
	free(cur);
  }
  hs->updates.head = NULL;
  hs->updates.tail = NULL;
  hs->updates.total_len = 0;
  hs->updates.size = 0;
}

static void set_host(HOST_SLOT* host, char* meeting_id)
{
	AIO_SLOT *slot = get_first_slot();

	while (slot)
	{
		if (slot->client_f && slot->id && strcmp(slot->id, meeting_id)==0)
		{
			CL_SLOT* cl = (CL_SLOT*) slot;
			cl->host_slot = host;
		}

		slot = slot->next;
	}
}

void set_last_update()
{
  aio_set_timer(fn_expire_connection, HOST_TIMEOUT);
}

static void fn_expire_connection()
{
  log_write(LL_MSG, "No client activity for host %s, closing", cur_slot->id);
  aio_close(0);
}
