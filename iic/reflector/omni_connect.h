#ifndef _OMNI_CONNECT_H
#define _OMNI_CONNECT_H

/* we send back byte 0x01 after accepting connection */
#define OMNI_VER_1  0   

/* we send back byte 0x01 after accepting connection for valid meeting,
   0xBB if host for meeting is not found */ 
#define OMNI_VER_2  1   

#define OMNI_VALID 1
#define OMNI_NO_HOST 0xBB

void set_require_session_token(int flag);
int omni_connect(char *host_info_file, int host_port, int cl_listen_port);

#endif /* _OMNI_CONNECT_H */
