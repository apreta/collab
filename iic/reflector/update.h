#ifndef UPDATE_H
#define UPDATE_H

#define UPDATE_EMPTYRECT 1
#define UPDATE_LASTRECT 2
#define UPDATE_FULLSCREEN 4

typedef struct _UPDATE {
  struct _UPDATE * prev;
  struct _UPDATE * next;

  int flags;
  int sequence;

  FB_RECT rect;

  int len;
  CARD8 data[1];

} UPDATE;


typedef struct _UPDATE_QUEUE {
  UPDATE *head;
  UPDATE *tail;
  int size;			/* Number of rectangles in queue */
  int total_len;    /* IIC -- total number of bytes */
} UPDATE_QUEUE;

#endif
