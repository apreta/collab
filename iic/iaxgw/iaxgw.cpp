/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
/* 
 * IAX2 to SIP Gateway
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <mcheck.h>

#include "../util/ithread.h"
#include "../jcomponent/jcomponent.h"
#include "../jcomponent/util/log.h"
#include "../util/asynccomm/port.h"
#include "../util/asynccomm/comm.h"
#include "callapi.h"

#define BASE_RTP_PORT 8000
#define MAX_IN_CHANNELS (MAX_CHANNELS/2)

IString s_target_addr;
IString s_bridge_ip;
IString s_sip_ip; 
CallStack * s_target_stack = NULL;
CallStack * s_iax_stack = NULL;

/*
   We use a simple channel allocation scheme so we can distinguish inbound from
   outbound events.  Inbound calls use a channel id less then MAX_IN_CHANNELS,
   and outbound calls use a higher channel id.  Each bridged call thus uses
   two channels.
*/

#define GET_IN_CHAN(channel)  ( (channel >= MAX_IN_CHANNELS) ? channel - MAX_IN_CHANNELS : channel )
#define GET_OUT_CHAN(channel)  ( (channel < MAX_IN_CHANNELS) ? channel  + MAX_IN_CHANNELS : channel )
#define IS_IN_CHAN(channel) ( (channel >= MAX_IN_CHANNELS) ? false : true )


struct GwCall
{
    IString m_from;

    // stack call originated from
    CallStack *m_stack;
    
    bool m_bridged;

    int m_active_legs;

    GwCall(const char * from, CallStack * stack)
    {
        m_from = from;
        m_stack = stack;
        m_bridged = false;
        m_active_legs = 0;
    }
};


class GwCallManager
{
    GwCall * m_calls[MAX_IN_CHANNELS];
    int m_last_channel;

public:
    GwCallManager()
    {
        for (int i=0; i<MAX_IN_CHANNELS; i++)
            m_calls[i] = NULL;
        m_last_channel = -1;
    }
    
    int find_idle_channel()
    {
        int i = m_last_channel + 1;
        while (i != m_last_channel)
        {
            if (m_calls[i] == NULL)
            {
                m_last_channel = i;
                return i;
            }
            if (++i >= MAX_IN_CHANNELS)
                i = 0;
        }
        return -1;
    }

    int add_call(GwCall * call)
    {
        int channel = find_idle_channel();
        if (channel >= 0)
        {
            log_debug(ZONE, "Allocated channel %d for call", channel);
            m_calls[channel] = call;
        }
        return channel;
    }
    
    GwCall* get_call(int channel)
    {
        return m_calls[channel];
    }
    
    void remove_call(int channel)
    {
        log_debug(ZONE, "Releasing channel %d", channel);
        delete m_calls[channel];
        m_calls[channel] = NULL;
    }
};


GwCallManager s_call_manager;


class GwHandler : public CallHandler
{
public:
    void set_stack(CallStack * stack)
    {
        m_stack = stack;
    }
    
    bool on_incoming(const char * from, RtpInfo & info, int & chan_id)
    {
        GwCall * call = new GwCall(from, m_stack);
        chan_id = s_call_manager.add_call(call);
        if (chan_id < 0)
        {
            delete call;
            return false;
        }
        
        info.local_address = s_sip_ip;
        info.local_port = BASE_RTP_PORT + 2*chan_id;

		RtpInfo out;
		out.codec = info.codec;
		for (unsigned i=0; i < info.available_codec_count; i++)
			out.available_codecs[i] = info.available_codecs[i];
		out.available_codec_count = info.available_codec_count;
		out.local_address = s_sip_ip;
		out.local_port = BASE_RTP_PORT + 2*(GET_OUT_CHAN(chan_id));

		s_target_stack->call(GET_OUT_CHAN(chan_id), call->m_from, s_target_addr, out);

        return true;
    }
    
    void on_connected(int chan_id, RtpInfo & info)
    {
        GwCall * call = s_call_manager.get_call(GET_IN_CHAN(chan_id));

        ++call->m_active_legs;
        
        if (IS_IN_CHAN(chan_id))
        {
			/*
            RtpInfo out;
            out.codec = info.codec;
            out.local_address = s_sip_ip;
            out.local_port = BASE_RTP_PORT + 2*(GET_OUT_CHAN(chan_id));

            s_target_stack->call(GET_OUT_CHAN(chan_id), call->m_from, s_target_addr, out);
            */
            call->m_bridged = true;
        }
        else
        {
			call->m_stack->answer(GET_IN_CHAN(chan_id), info);
        }
        
        m_stack->start_media(chan_id, info);
    }
    
    void on_disconnect(int chan_id, bool remote_hangup)
    {
        m_stack->stop_media(chan_id);

        GwCall * call = s_call_manager.get_call(GET_IN_CHAN(chan_id));
        int target_chan;
        CallStack * target_stack;

        if (IS_IN_CHAN(chan_id))
        {
            target_chan = GET_OUT_CHAN(chan_id);
            target_stack = s_target_stack;
        }
        else
        {
            target_chan = GET_IN_CHAN(chan_id);
            target_stack = call->m_stack;
        }
        
        target_stack->disconnect(target_chan);
    }
    
    void on_released(int chan_id)
    {
        GwCall * call = s_call_manager.get_call(GET_IN_CHAN(chan_id));
        if (call)
        {
            if (--call->m_active_legs == 0)
                s_call_manager.remove_call(GET_IN_CHAN(chan_id));
        }
    }
    
    void on_callprogress(int chan_id, int progress, bool start_tone)
    {
    }
    
    void on_received(int chan_id, char * data, int len, long time)
    {
        GwCall * call = s_call_manager.get_call(GET_IN_CHAN(chan_id));

        if (call->m_bridged)
        {
            int target_chan;
            CallStack * target_stack;

            if (IS_IN_CHAN(chan_id))
            {
                target_chan = GET_OUT_CHAN(chan_id);
                target_stack = s_target_stack;
            }
            else
            {
                target_chan = GET_IN_CHAN(chan_id);
                target_stack = call->m_stack;
            }
            
            target_stack->send(target_chan, data, len, time);
        }
    }
    
    void on_dtmf(int chan_id, int dtmf, int duration, long time)
    {
        GwCall * call = s_call_manager.get_call(GET_IN_CHAN(chan_id));

        if (call->m_bridged)
        {
            int target_chan;
            CallStack * target_stack;

            if (IS_IN_CHAN(chan_id))
            {
                target_chan = GET_OUT_CHAN(chan_id);
                target_stack = s_target_stack;
            }
            else
            {
                target_chan = GET_IN_CHAN(chan_id);
                target_stack = call->m_stack;
            }
            
            target_stack->send_dtmf(target_chan, dtmf, duration, time);
        }
    }
    
    // Logging
    enum LogLevel { Error, Status, Debug };
    void on_log(int level, const char * file, int line, const char * message)
    {
        switch (level)
        {
            case Error:
                log_error((char *)file, line, message);
                break;
            case Status:
                log_status((char *)file, line, message);
                break;
            case Debug:
            default:
                log_debug((char *)file, line, message);
        }
    }

private:
    CallStack * m_stack;
};

/*
void config_logger(char *jcomp_name, config_t config)
{
    char *log_file;
	char *log_rotation_sz;
	char *log_history_len;
	char *log_ident;
	char *log_init_level;

    log_file = config_get_one(config, jcomp_name, "log.file", 0);
	if (log_file != NULL)
		log_new(log_file);
	log_rotation_sz = config_get_one(config, jcomp_name, "log.rotationSize", 0);
	if (log_rotation_sz != NULL)
	  log_set_rotation_size(atoi(log_rotation_sz));
	log_history_len = config_get_one(config, jcomp_name, "log.historyLen", 0);
	if (log_history_len != NULL)
	  log_set_history_len(atoi(log_history_len));
    log_ident = config_get_one(config, jcomp_name, "log.ident", 0);
	if (log_ident != NULL)
		log_new(log_ident);

    log_init_level = config_get_one(config, jcomp_name, "log.level", 0);
    if (!j_strcmp(log_init_level, "debug")) { log_set_level(LOG_DEBUG); }
    else if (!j_strcmp(log_init_level, "info")) { log_set_level(LOG_INFO); }
    else if (!j_strcmp(log_init_level, "error")) { log_set_level(LOG_ERR); }
}
*/

/* 
   Get local IP address.
*/
char *get_local_address()
{
    struct hostent *hp;
    struct in_addr *addr;
    static char myname[MAXHOSTNAMELEN + 1];

    gethostname(myname,MAXHOSTNAMELEN);
    hp = gethostbyname(myname);
    if(hp != NULL)
    {
        addr = (struct in_addr *) *hp->h_addr_list;
        strcpy(myname, inet_ntoa(*addr));
        return myname;
    }
    
    return NULL;
}


/* 
   Command line is:
   $ iaxgwd <log level> <bridge ip> <encryption_type> [<sip ip> <host_domain> <sip port> <iax port>]
 */
int main(int argc, char * argv[])
{
    fprintf(stderr, "IAX2 Gateway   Copyright (C) 2011 Apreta\n\n");

    log_new("/var/log/iic/iaxgw");
    
    int log_level = 3;
    char * host_ip = NULL;
    char * sip_ip = NULL;
    char * host_domain = NULL;
    int iax_port = 4569;
    int sip_port = 5050;
    bool use_encrypt = false;

	if (argc <= 1) {
		fprintf(stderr, "\nUsage:\n\n"
			"iawgwd <loglevel> <gatewayIP> <encryptionType> <sipIP> <targetDomain> [<sipPort> <iaxPort>]\n"
			"logLevel: 0-7\n"
			"encryptionType: none, blowfish\n\n"
			"Usually gatewayIP and sipIP will be the IP address of the local server,\n"
			"and targetDomain will be the IP address or domain name of the target SIP server.\n\n"
			"sipPort defaults to 5050 (so the gateway can run on the same server as the target\n"
			"SIP server), and iaxPort defaults to 4569.\n\n");
		return 0;
	}

    if (argc > 1)
    {
        log_level = atoi(argv[1]);
    }

    if (argc > 2)
    {
        host_ip = argv[2];
    }
    else
    {
        host_ip = get_local_address();
        log_debug(ZONE, "Voice bridge address not specified, using %s", host_ip);
    }

    if (argc > 3)
    {
        if (strcmp(argv[3], "blowfish") == 0)
            use_encrypt = true;
    }
    
    if (argc > 4)
    {
        sip_ip = argv[4];
    }
    else
    {
        sip_ip = host_ip;
        log_debug(ZONE, "Voice sip address not specified, using %s", sip_ip);
    }
    
    if (argc > 5)
    {
        host_domain = argv[5];
    }
    else
    {
        host_domain = sip_ip;
    }

    if (argc > 6)
    {
        iax_port = atoi(argv[6]);
    }

    if (argc > 7)
    {
        sip_port = atoi(argv[7]);
    }
    
    if (argc > 8)
    {
		int size = atoi(argv[8]);
		if (size > 0)
			log_set_rotation_size(size);
	}

    log_set_level(log_level);
    s_bridge_ip = host_ip;
    s_sip_ip = sip_ip;
    s_target_addr = IString(host_domain);

	IThread::initialize();

    // Initialize async i/o subsystem used by RTP and IAX stacks
	CommProcess::initialize(false);

//    g_thread_set_priority(g_thread_self(), G_THREAD_PRIORITY_HIGH);

    bool debug = log_level >= LOG_DEBUG;

    GwHandler sip_handler;
    s_target_stack = create_sip_stack(&sip_handler, host_domain, sip_ip, sip_port, debug);
    sip_handler.set_stack(s_target_stack);
    
    GwHandler iax_handler;
    s_iax_stack = create_iax_stack(&iax_handler, host_domain, host_ip, iax_port, debug, use_encrypt);
    iax_handler.set_stack(s_iax_stack);
    
    while (true)
    {
        bool work = false;
        CommProcess::process();
        s_target_stack->process(work);
        s_iax_stack->process(work);
        if (!work)
            g_usleep(5000); // 5 ms
    }
    
    return 0;
}
