/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
/*  
 *  SIP stack for gateway implemeted using resiprocate.
 *
 */
#include "rutil/Log.hxx"
#include "rutil/Logger.hxx"
#include "resip/stack/Pidf.hxx"
#include "resip/stack/PlainContents.hxx"
#include "resip/dum/ClientAuthManager.hxx"
#include "resip/dum/ClientInviteSession.hxx"
#include "resip/dum/ServerInviteSession.hxx"
#include "resip/dum/ClientSubscription.hxx"
#include "resip/dum/ServerSubscription.hxx"
#include "resip/dum/ClientRegistration.hxx"
#include "resip/dum/ServerRegistration.hxx"
#include "resip/dum/ClientPublication.hxx"
#include "resip/dum/ServerPublication.hxx"

#include "sip.h"


using namespace resip;
using namespace std;

#define RESIPROCATE_SUBSYSTEM Subsystem::TEST

//#define RTP_BASE_PORT 8000

const Codec G721_8000("G721", 2, 8000); // a.k.a. G.726-32k
const Codec AMR_NB_8000("AMR", 98, 8000);
const Codec ILBC_20_8000("ILBC20", 99, 8000);
const Codec ILBC_30_8000("ILBC30", 100, 8000);

CallStack * create_sip_stack(CallHandler* appHandler, const char * host_address, const char * host_ip, unsigned port, bool debug)
{
    UserAgent * stack = new UserAgent(appHandler, host_address, host_ip, port, debug);
    return stack;
}


bool MyExternalLogger::operator()(Log::Level level,
                      const Subsystem& subsystem, 
                      const Data& appName,
                      const char* file,
                      int line,
                      const Data& message,
                      const Data& messageWithHeaders)
{
    int convert;
    switch (level)
    {
        case Log::Crit:
        case Log::Err:
            convert = CallHandler::Error;
            break;
        
        case Log::Warning:
        case Log::Info:
            convert = CallHandler::Status;
            break;
        
        case Log::Debug:
        default:
            convert = CallHandler::Debug;
            break;
    }

    mAppHandler->on_log(convert, file, line, message.c_str());
    
    return false;
}


UserAgent::UserAgent(CallHandler* appHandler, const char * hostDomain, const char * hostIp, unsigned port, bool debug) : 
   mProfile(new MasterProfile),
#if defined(USE_SSL)
   mSecurity(new Security(mCertPath)),
   mStack(mSecurity),
#else
   mSecurity(0),
   mStack(mSecurity),
#endif
   mDum(mStack),
   mStackThread(mStack),
   mAppHandler(appHandler),
   mRtpStack(appHandler)
{
    mLogger = new MyExternalLogger(appHandler);
    
    Log::initialize(Log::Cout, debug ? Log::Debug : Log::Warning, "iaxgwd", *mLogger);
    
    mHostDomain = hostDomain;
    mHostIp = hostIp;
    mUdpPort = port;
    mTcpPort = port;
    mRegisterDuration = 0;    
    mNoV4 = false;
    mNoV6 = true;

#if defined(USE_SSL)
   if (mGenUserCert)
   {
      mSecurity->generateUserCert(mAor.getAor());
   }
#endif

   addTransport(UDP, hostIp, mUdpPort);
   addTransport(TCP, hostIp, mTcpPort);
#if defined(USE_SSL)
   addTransport(TLS, hostIp, mTlsPort);
#endif
#if defined(USED_DTLS)
   addTransport(DTLS, hostIp, mDtlsPort);
#endif

/*   mProfile->setDefaultRegistrationTime(mRegisterDuration);
   mProfile->addSupportedMethod(NOTIFY);
   mProfile->validateAcceptEnabled() = false;
   mProfile->validateContentEnabled() = false;
   mProfile->addSupportedMimeType(NOTIFY, Pidf::getStaticType());
   mProfile->setDefaultFrom(NameAddr(mAor));
   mProfile->setDigestCredential(mAor.host(), mAor.user(), mPassword);
 
   if (!mContact.host().empty())
   {
      mProfile->setOverrideHostAndPort(mContact);
   }
*/
   // Enable session timer (causes reinvites or updates to be periodically sent)
   mProfile->addSupportedOptionTag(Token(Symbols::Timer));
   mProfile->setDefaultSessionTime(5*60);
   
   mProfile->setDefaultFrom(NameAddr(Data("sip:gw@") + hostDomain));
   mProfile->setUserAgent("zon/1.0");
   
   mDum.setMasterProfile(mProfile);
   mDum.setClientRegistrationHandler(this);
   mDum.addOutOfDialogHandler(OPTIONS, this);
   mDum.setClientAuthManager(std::auto_ptr<ClientAuthManager>(new ClientAuthManager));
   mDum.setInviteSessionHandler(this);

   mDum.setAppDialogSetFactory(auto_ptr<AppDialogSetFactory>(new SessionFactory));
   
   mCodecs.push_back(AMR_NB_8000);
   mCodecs.push_back(ILBC_20_8000);
   mCodecs.push_back(ILBC_30_8000);
   mCodecs.push_back(SdpContents::Session::Codec::GSM_8000);
   mCodecs.push_back(SdpContents::Session::Codec::ULaw_8000);
   mCodecs.push_back(G721_8000);
   mCodecs.push_back(SdpContents::Session::Codec::G729_8000);
   mCodecs.push_back(SdpContents::Session::Codec::G723_8000);
   
   for (int i=0; i<MAX_CHANNELS; i++)
      mCalls[i] = NULL;
   
   mStackThread.run(); 
}

UserAgent::~UserAgent()
{
   mStackThread.shutdown();
   mStackThread.join();
}

void
UserAgent::startup()
{
   if (mRegisterDuration)
   {
      InfoLog (<< "register for " << mAor);
      mDum.send(mDum.makeRegistration(NameAddr(mAor)));
   }

}

void
UserAgent::shutdown()
{
}

void
UserAgent::process(bool& work)
{
   static int check = 0;
   
   while (mDum.process());
   
   // Every so often, loop through active calls and send info request
   if (++check > 1000)
   {
      check = 0;
      
      // pingCalls();
   }
}

void 
UserAgent::answer(int chan_id, RtpInfo & info)
{
   // No-op in this case, call was already answered.
}

void 
UserAgent::disconnect(int chan_id)
{
  if (chan_id < 0 || chan_id >= MAX_CHANNELS)
  {
    InfoLog (<< "Disconnect on invalid channel " << chan_id);
    return;
  }
    
  SipSession * dlg = mCalls[chan_id];
  if (dlg && !dlg->terminated)
  {
    InviteSessionHandle& h = dlg->sesh;
    if (h.isValid())
    {
        if (!h->isTerminated())
        {
            // Send BYE
            h->end();
        }
    }
    else {
        // No session dialog was established so clean up now.

        // Send CANCEL
        dlg->end();

        dlg->terminated = true;
        mAppHandler->on_released(dlg->chan_id);
        
        mCalls[chan_id] = NULL;
    }
  }
}

void 
UserAgent::call(int chan_id, const char * from, const char * address, RtpInfo & info)
{
    SdpContents out;
    SipSession* dlg = new SipSession(mDum, ++sip_id);
    dlg->chan_id = chan_id;
    dlg->rtp_info = info;
    mCalls[chan_id] = dlg;

    buildNewSdp(out, dlg->rtp_info);

    if (from == NULL || *from == 0)
        from = "gw";

    IString full_address = IString("sip:") + IString(from) + "@" + address;
    NameAddr aor(full_address.get_string());

    SharedPtr<SipMessage> msg = mDum.makeInviteSession(aor, &out, dlg);
    mDum.send(msg);
    
    // send dialing notification
    mAppHandler->on_callprogress(chan_id, ProgressDialing, true);
}

bool 
UserAgent::start_media(int chan_id, RtpInfo & info)
{
  return mRtpStack.start_media(chan_id, info);
}

void 
UserAgent::stop_media(int chan_id)
{
  mRtpStack.stop_media(chan_id);    
}

void 
UserAgent::send(int chan_id, char * data, int size, long time)
{
  mRtpStack.send(chan_id, data, size, time);
}

void 
UserAgent::send_dtmf(int chan_id, int digit, int duration, long time)
{
  mRtpStack.send_dtmf(chan_id, digit, duration, time);
}

void 
UserAgent::set_outbound_proxy(const char * address)
{
  mProfile->setOutboundProxy(Uri(address));
}

void
UserAgent::addTransport(TransportType type, const char *ip_addr, int port)
{
  try
  {
     if (port)
     {
        if (!mNoV4)
        {
           mDum.addTransport(type, port, V4, ip_addr, mTlsDomain);
           return;
        }

        if (!mNoV6)
        {
           mDum.addTransport(type, port, V6, ip_addr, mTlsDomain);
           return;
        }
     }
  }
  catch (BaseException& e)
  {
     InfoLog (<< "Caught: " << e);
     WarningLog (<< "Failed to add " << Tuple::toData(type) << " transport on " << port);
  }
  throw Transport::Exception("Port already in use", __FILE__, __LINE__);
}

bool
UserAgent::findMatchingCodec(const SdpContents& in, resip::SdpContents::Session::Codec& codec)
{
   std::list<SdpContents::Session::Medium> media = in.session().media();
   for (std::list<SdpContents::Session::Medium>::iterator i=media.begin(); i != media.end(); ++i)
   {
      SdpContents::Session::Medium& m = *i;
      if (m.name() == "audio")
      {
         std::list<resip::SdpContents::Session::Codec> & codecs = m.codecs();
         std::list<resip::SdpContents::Session::Codec>::const_iterator i, j;

         for (i = codecs.begin(); i != codecs.end(); ++i)
         {
            for (j = mCodecs.begin(); j != mCodecs.end() ; ++j)
            {
               if (*i == *j)
               {
                  codec = *i;
                  return true;
               }
            }  
         }
      }
   }
   return false;
}

bool
UserAgent::buildAnswerSdp(SdpContents& in, SdpContents& out, RtpInfo& rtp)
{
   resip::SdpContents::Session::Codec codec = resip::SdpContents::Session::Codec::ULaw_8000;
    
   findMatchingCodec(in, codec);
   
   rtp.codec = codec.payloadType();
    
   out.session().version() = 0;
   out.session().origin().user() = "ZonBridge";
   out.session().origin().setAddress(mHostIp);
   out.session().name() = "-";
   out.session().connection().setAddress(rtp.local_address.get_string());
   
   SdpContents::Session::Medium medium;
   medium.name() = "audio";
   medium.port() = rtp.local_port;
   medium.protocol() = "RTP/AVP";
   medium.addCodec(codec);
   
   Codec telCodec = SdpContents::Session::Codec::TelephoneEvent;
   telCodec.parameters() = "0-15";
   medium.addCodec(telCodec);
   out.session().addMedium(medium);

   return true;
}

bool
UserAgent::buildNewSdp(SdpContents& out, RtpInfo& rtp)
{
   out.session().version() = 0;
   out.session().origin().user() = "ZonBridge";
   out.session().origin().setAddress(mHostIp);
   out.session().name() = "-";
   out.session().connection().setAddress(rtp.local_address.get_string());
   
   SdpContents::Session::Medium medium;
   medium.name() = "audio";
   medium.port() = rtp.local_port;
   medium.protocol() = "RTP/AVP";
   
   addCodec(medium, rtp.codec);
   for (unsigned i=0; i < rtp.available_codec_count; i++)
   {
	   if (rtp.codec != rtp.available_codecs[i])
	      addCodec(medium, rtp.available_codecs[i]);
   }

   /*
	medium.addCodec(AMR_NB_8000);
	medium.addCodec(ILBC_30_8000);
	medium.addCodec(resip::SdpContents::Session::Codec::ULaw_8000);
	medium.addCodec(G721_8000);
	medium.addCodec(resip::SdpContents::Session::Codec::G729_8000);
   */
      
   Codec telCodec = SdpContents::Session::Codec::TelephoneEvent;
   telCodec.parameters() = "0-15";
   medium.addCodec(telCodec);
   out.session().addMedium(medium);

   return true;
}

void
UserAgent::addCodec(SdpContents::Session::Medium & medium, unsigned codec)
{
   switch (codec)
   {
       case rtpPayloadG729:
            medium.addCodec(resip::SdpContents::Session::Codec::G729_8000);
            break;
        case rtpPayloadG726_32:
            medium.addCodec(G721_8000);
            break;
        case rtpPayloadPCMU:
            medium.addCodec(resip::SdpContents::Session::Codec::ULaw_8000);
            break;
        case rtpPayloadG723:
            medium.addCodec(resip::SdpContents::Session::Codec::G723_8000);
            break;
        case rtpPayloadGSM:
			medium.addCodec(resip::SdpContents::Session::Codec::GSM_8000);
			break;
        case 98:
            medium.addCodec(AMR_NB_8000);
            break;
        case 99:
            medium.addCodec(ILBC_20_8000);
            break;
        case 100:
            medium.addCodec(ILBC_30_8000);
            break;
    }
}

void
UserAgent::pingCalls()
{
   long now = time(NULL);
   
   for (int i=0; i<MAX_CHANNELS; i++)
   {
      if (mCalls[i] != NULL && mCalls[i]->next_ping < now)
      {
         InfoLog(<< "Send INFO request on channel " << i);

         SipSession * dlg = mCalls[i];
         dlg->next_ping = now + PingInterval;
         
         if (dlg && !dlg->terminated)
         {
            InviteSessionHandle& h = dlg->sesh;
            if (h.isValid() && !h->isTerminated())
            {
               h->info(PlainContents::Empty);
            }
         }
      }
   }
}

void
UserAgent::onNewSession(ClientInviteSessionHandle h, InviteSession::OfferAnswerType oat, const SipMessage& msg)
{
   InfoLog(<< h->myAddr().uri().user() << " 180 from  " << h->peerAddr().uri().user());

   SipSession* dlg = dynamic_cast<SipSession*>(h->getAppDialogSet().get());

   dlg->sesh = h->getSessionHandle();
   dlg->msg = msg;
   
   mAppHandler->on_callprogress(dlg->chan_id, ProgressRinging, true);
}

void
UserAgent::onNewSession(ServerInviteSessionHandle h, InviteSession::OfferAnswerType oat, const SipMessage& msg)
{
   InfoLog(<< h->myAddr().uri().user() << " INVITE from  " << h->peerAddr().uri().user());

   h->provisional(180);

   SipSession* dlg = dynamic_cast<SipSession*>(h->getAppDialogSet().get());
   
   dlg->sesh = h->getSessionHandle();
   dlg->msg = msg;
}

void
UserAgent::onFailure(ClientInviteSessionHandle h, const SipMessage& msg)
{
   InfoLog(<< h->myAddr().uri().user() 
           << " outgoing call failed " 
           << h->peerAddr().uri().user() 
           << " status=" << msg.header(h_StatusLine).statusCode());

   // Outbound call failed to complete
   SipSession* dlg = dynamic_cast<SipSession*>(h->getAppDialogSet().get());
   mAppHandler->on_callprogress(dlg->chan_id, 0, false);
}
      
void
UserAgent::onEarlyMedia(ClientInviteSessionHandle, const SipMessage&, const SdpContents&)
{
}

void
UserAgent::onProvisional(ClientInviteSessionHandle, const SipMessage& msg)
{
}

void
UserAgent::onConnected(ClientInviteSessionHandle h, const SipMessage& msg)
{
   // Outbound call connected
   SipSession* dlg = dynamic_cast<SipSession*>(h->getAppDialogSet().get());
   mAppHandler->on_connected(dlg->chan_id, dlg->rtp_info);
}

void
UserAgent::onConnected(InviteSessionHandle h, const SipMessage& msg)
{
   // Inbound call connected
   SipSession* dlg = dynamic_cast<SipSession*>(h->getAppDialogSet().get());
   mAppHandler->on_connected(dlg->chan_id, dlg->rtp_info);
}

void
UserAgent::onStaleCallTimeout(ClientInviteSessionHandle)
{
   WarningLog(<< "onStaleCallTimeout");
}

void
UserAgent::onTerminated(InviteSessionHandle h, InviteSessionHandler::TerminatedReason reason, const SipMessage* msg)
{
   bool remoteHangup;
   
   if (reason == InviteSessionHandler::RemoteBye || reason == InviteSessionHandler::RemoteCancel)
   {
      WarningLog(<< h->myAddr().uri().user() << " call terminated with " << h->peerAddr().uri().user());
      remoteHangup = true;
   }
   else
   {
      WarningLog(<< h->myAddr().uri().user() << " ended call with " << h->peerAddr().uri().user());
      remoteHangup = false;
   }
   
   SipSession* dlg = dynamic_cast<SipSession*>(h->getAppDialogSet().get());
   if (dlg->chan_id < 0 || dlg->chan_id > MAX_CHANNELS)
   {
     InfoLog(<< "SIP session terminated for invalid channel " << dlg->chan_id);
     return;
   }

   if (!dlg->terminated)
   {
     dlg->terminated = true;
     mAppHandler->on_disconnect(dlg->chan_id, remoteHangup);
     mAppHandler->on_released(dlg->chan_id);
   }
   
   mCalls[dlg->chan_id] = NULL;
}

void
UserAgent::onRedirected(ClientInviteSessionHandle, const SipMessage& msg)
{
   assert(false);
}

void
UserAgent::onAnswer(InviteSessionHandle h, const SipMessage& msg, const SdpContents& sdp)
{
   SipSession* dlg = dynamic_cast<SipSession*>(h->getAppDialogSet().get());
   
   dlg->rtp_info.remote_address = sdp.session().connection().getAddress().c_str();
   
   std::list<SdpContents::Session::Medium> media = sdp.session().media();
   for (std::list<SdpContents::Session::Medium>::iterator i=media.begin(); i != media.end(); ++i)
   {
      SdpContents::Session::Medium& m = *i;
      if (m.name() == "audio")
      {
          //info.remote_address = (*m.getConnections().begin()).getAddress().c_str();
          dlg->rtp_info.remote_port = m.port();
      }
   }

   resip::SdpContents::Session::Codec codec = resip::SdpContents::Session::Codec::ULaw_8000;
    
   findMatchingCodec(sdp, codec);
   
   dlg->rtp_info.codec = codec.payloadType();
   
   InfoLog(<< "Selected codec " << codec.payloadType());
   
   dlg->sesh = h->getSessionHandle();
   dlg->msg = msg;
}

void
UserAgent::onOffer(InviteSessionHandle h, const SipMessage& msg, const SdpContents& offer)
{
   SipSession* dlg = dynamic_cast<SipSession*>(h->getAppDialogSet().get());

   if (h->isAccepted())
   {
      // Handle session timers--period INVITEs sent by peer to determine if call is 
      // still valid.  Respond with same SDP info.
      InfoLog(<< h->myAddr().uri().user() << " re-INVITE from  " << h->peerAddr().uri().user());
      
      h->provideAnswer(h->getLocalSdp());
      return;
   }
 
   /*
   IString from = h->peerAddr().uri().user().data();
         
   SdpContents* sdp = dynamic_cast<SdpContents*>(msg.getContents());
   
   dlg->rtp_info.remote_address = sdp->session().connection().getAddress().c_str();
   
   std::list<SdpContents::Session::Medium> media = sdp->session().media();
   for (std::list<SdpContents::Session::Medium>::iterator i=media.begin(); i != media.end(); ++i)
   {
      SdpContents::Session::Medium& m = *i;
      if (m.name() == "audio")
      {
          //info.remote_address = (*m.getConnections().begin()).getAddress().c_str();
          dlg->rtp_info.remote_port = m.port();
      }
   }

   dlg->sesh = h->getSessionHandle();
   dlg->msg = msg;
   
   bool accept = mAppHandler->on_incoming(from, dlg->rtp_info, dlg->chan_id);
   if (accept && dlg->chan_id >= 0 && dlg->chan_id < MAX_CHANNELS)
   {
      mCalls[dlg->chan_id] = dlg;

      SdpContents sdpOut;
      buildAnswerSdp(*sdp, sdpOut, dlg->rtp_info);

      InfoLog (<< "Accepting SIP call on channel " << dlg->chan_id);
      
      h->provideAnswer(sdpOut);
      
      ServerInviteSession* serverh = dynamic_cast<ServerInviteSession*>(h.get());
      serverh->accept();
   }
   else 
   */
   {
      InfoLog (<< "Rejecting SIP call on channel " << dlg->chan_id);
      h->reject(400);
   } 
}

void
UserAgent::onOfferRequired(InviteSessionHandle h, const SipMessage& msg)
{
   // This most likely means we got a re-invite with no SDP
   if (h->isAccepted())
   {
      // Handle session timers--period INVITEs sent by peer to determine if call is 
      // still valid.  Respond with same SDP info.
      InfoLog(<< h->myAddr().uri().user() << " re-INVITE from  " << h->peerAddr().uri().user());
      
      h->provideAnswer(h->getLocalSdp());
      return;
   }
}

void
UserAgent::onOfferRejected(InviteSessionHandle, const SipMessage* msg)
{
    assert(0);
}

void
UserAgent::onDialogModified(InviteSessionHandle, InviteSession::OfferAnswerType oat, const SipMessage& msg)
{
    assert(0);
}

void
UserAgent::onInfo(InviteSessionHandle h, const SipMessage& msg)
{
   h->acceptNIT(200);
}

void
UserAgent::onInfoSuccess(InviteSessionHandle, const SipMessage& msg)
{
   InfoLog(<< "Info request succeeded");
}

void
UserAgent::onInfoFailure(InviteSessionHandle h, const SipMessage& msg)
{
   InfoLog(<< "Info request failed");
   SipSession* dlg = dynamic_cast<SipSession*>(h->getAppDialogSet().get());
   disconnect(dlg->chan_id);
}

void
UserAgent::onRefer(InviteSessionHandle, ServerSubscriptionHandle, const SipMessage& msg)
{
    assert(0);
}

void
UserAgent::onReferAccepted(InviteSessionHandle, ClientSubscriptionHandle, const SipMessage& msg)
{
   assert(false);
}

void
UserAgent::onReferRejected(InviteSessionHandle, const SipMessage& msg)
{
   assert(0);
}

void
UserAgent::onReferNoSub(InviteSessionHandle, const SipMessage& msg)
{
   assert(0);
}

void
UserAgent::onMessage(InviteSessionHandle, const SipMessage& msg)
{
}

void
UserAgent::onMessageSuccess(InviteSessionHandle, const SipMessage& msg)
{
}

void
UserAgent::onMessageFailure(InviteSessionHandle, const SipMessage& msg)
{
}


////////////////////////////////////////////////////////////////////////////////
// Registration Handler ////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void 
UserAgent::onSuccess(ClientRegistrationHandle h, const SipMessage& response)
{
}

void
UserAgent::onFailure(ClientRegistrationHandle h, const SipMessage& response)
{
}

void
UserAgent::onRemoved(ClientRegistrationHandle h, const SipMessage&)
{
}

int 
UserAgent::onRequestRetry(ClientRegistrationHandle h, int retryMinimum, const SipMessage& msg)
{
   //assert(false);
   return -1;
}


////////////////////////////////////////////////////////////////////////////////
// OutOfDialogHandler //////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void 
UserAgent::onSuccess(ClientOutOfDialogReqHandle, const SipMessage& response)
{
   InfoLog(<< response.header(h_CSeq).method() << "::OK: " << response );
}

void 
UserAgent::onFailure(ClientOutOfDialogReqHandle, const SipMessage& response)
{
   ErrLog(<< response.header(h_CSeq).method() << "::failure: " << response );
   if (response.exists(h_Warnings)) ErrLog  (<< response.header(h_Warnings).front());
}

void 
UserAgent::onReceivedRequest(ServerOutOfDialogReqHandle, const SipMessage& request)
{
}

void
UserAgent::onForkDestroyed(ClientInviteSessionHandle)
{
}

