/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
/*  
 *  Defines abstract SIP stack API.
 *
 */

#ifndef SIPAPI_H
#define SIPAPI_H



#define MAX_SIP_CHANNELS 4092


struct RtpInfo
{
    IString remote_address;
    unsigned remote_port;
    IString local_address;
    unsigned local_port;
    unsigned codec;
    
    RtpInfo() { remote_port = 0; local_port = 0; codec = 0; }
};    

enum ProgressReasons
{
	ProgressDialing = 100,
    ProgressRinging,
    ProgressBusy,
    ProgressReorder,
    ProgressSpecialInfo,
    ProgressAnswered,
	ProgressSpeech,
    ProgressRemoteHangup,
    ProgressError,
};

class SipHandler
{
public:
    virtual bool on_sip_incoming(IString & from, RtpInfo & info, int & chan_id) = 0;
    virtual void on_sip_connected(int chan_id, RtpInfo & info) = 0;
    virtual void on_sip_disconnect(int chan_id, bool remote_hangup) = 0;
    virtual void on_sip_released(int chan_id) = 0;
    
    virtual void on_sip_callprogress(int chan_id, int progress, bool start_tone) = 0;
    
    // Logging
    enum LogLevel { SipError, SipStatus, SipDebug };
    virtual void on_sip_log(int level, const char * file, int line, const char * message) = 0;
};


class SipApi
{
public:
      virtual void process() = 0;
      virtual void answer(int chan_id) = 0;
      virtual void disconnect(int chan_id) = 0;
      virtual void call(int chan_id, const char * address, RtpInfo & info) = 0;
      
      virtual void set_outbound_proxy(const char * address) = 0;
};


SipApi * create_sip_stack(SipHandler* appHandler, const char * host_address, const char * host_ip, unsigned port);


#endif
