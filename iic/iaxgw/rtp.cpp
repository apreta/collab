/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
/* 
 * Simple RTP stack.
 *
 * A bit too much data copying is going on due to code re-use.  Maybe fix later.
 *
 * To do:
 *   Check payload type.
 *   Make sure stream object gets destroyed.
 */

#include "rtp.h"
#include "../jcomponent/util/log.h"
#include "../util/asynccomm/port.h"
#include "../util/asynccomm/async_io.h"

#include <sstream>

#define LOG(level, msg) \
{ \
    std::stringstream ostr; \
    ostr << msg; \
    m_stack->m_handler->on_log(level, __FILE__, __LINE__, ostr.str().c_str()); \
}

#define LOGS(level, msg) \
{ \
    std::stringstream ostr; \
    ostr << msg; \
    m_handler->on_log(level, __FILE__, __LINE__, ostr.str().c_str()); \
}

RtpStream::RtpStream(RtpStack* stack, int chan_id, RtpInfo& info) :
    m_datagram(this),
    m_send(MAX_RTP_PACKET),
    m_recv(MAX_RTP_PACKET)
{
    m_rtp_info = info;
    m_chan_id = chan_id;
    m_stack = stack;
    m_send_seq = 0;
    m_ssrc = rand();
    m_dtmf_time = 0;
    
    int stat = m_datagram.initialize(info.local_address, info.local_port); 
    if (stat)
        LOG(CallHandler::Error, "Error starting RTP stream for port " << info.local_port);
        
    LOG(CallHandler::Debug, "Created RTP stream on port " << info.local_port << " for channel " << chan_id);
}

bool RtpStream::on_message(CommConnection * conn, CommMessage * msg)
{
    memcpy(m_recv.getHeader(), msg->get_data(), msg->get_len());
    m_recv.setTotalUsage(msg->get_len());
 
    if (m_recv.isValid())
    {
        //LOG(CallHandler::Debug, "RTP audio received: " << m_recv.getPayloadUsage() << " bytes at ts " << m_recv.getRtpTime());
        
        // Rtp clocking varies based on codec; should be based on sample rate for the audio codecs we are using.
        // Convert to millis.
        long ts = m_recv.getRtpTime() / 8;
        
        if (m_recv.getPayloadType() == rtpPayloadDTMF_RFC2833)
        {
            // DTMF packet
            RtpEventDTMFRFC2833* e = NULL;

            if (m_recv.getPayloadUsage() != 0)
                e = reinterpret_cast < RtpEventDTMFRFC2833* > (m_recv.getPayloadLoc());

            // Unfortunately some RTP stacks don't use edge and other values correctly
            // so we just figure new digit by seeing if no event for a period of time
            if (ts > m_dtmf_time + DTMF_INTERDIGIT)
            {
                LOG(CallHandler::Debug, "DTMF " << e->event << " detected [ " << m_chan_id << "]");
                m_stack->m_handler->on_dtmf(m_chan_id, e->event, 0, ts);
            }
            
            m_dtmf_time = ts;
        }
        else
        {
            // Normal audio packet
            m_stack->m_handler->on_received(m_chan_id, m_recv.getPayloadLoc(), m_recv.getPayloadUsage(), ts);
        }
    }
 
    return true;
}

bool RtpStream::on_error(CommConnection * conn)
{
    LOG(CallHandler::Error, "RTP stream error sending to " << m_rtp_info.remote_address.get_string());
    return true;
}

bool RtpStream::on_close(CommConnection * conn)
{
    LOG(CallHandler::Debug, "RTP stream closed");
    return true;
}

void RtpStream::send(char * data, int len, long time)
{
    memcpy(m_send.getPayloadLoc(), data, len);
    m_send.setPayloadUsage(len);
    
    m_send.setRtpTime(time *8 );
    m_send.setSequence(m_send_seq++);
    m_send.setPayloadType((RtpPayloadType)m_rtp_info.codec);
    m_last_sent = time;
    
    transmit();
}

void RtpStream::send_dtmf(int digit, int duration, long time)
{
    m_send.setPayloadType(rtpPayloadDTMF_RFC2833);
    m_send.setPayloadUsage( sizeof( RtpEventDTMFRFC2833 ) );

    RtpEventDTMFRFC2833* eventPayload = reinterpret_cast<RtpEventDTMFRFC2833*>
                                        ( m_send.getPayloadLoc() );

    // reset event payload
    eventPayload->event = digit;
    eventPayload->volume = 0x0A;
    eventPayload->reserved = 0;
    eventPayload->edge = 0;
    eventPayload->duration = 0x00A0;

    // send onedge packet
    m_send.setRtpTime(time * 8);
    m_send.setSequence(m_send_seq++);
    transmit();

/*  This would seem to be correct RTP, but not necessary for NMS    
    // send on packet
    eventPayload->edge = 1;
    transmit();

    // send offedge packet
    m_send.setPayloadUsage(0);
    transmit();
*/
    LOG(CallHandler::Debug, "DTMF " << digit << " sent [" << m_chan_id << "]");
}

void RtpStream::transmit()
{
    CommMessage msg;

    msg.set_message((unsigned char *)m_send.getPacketData(), m_send.getTotalUsage());
    
    int stat = m_datagram.send_message(m_rtp_info.remote_address, m_rtp_info.remote_port, &msg);
    if (stat)
    {
        LOG(CallHandler::Debug, "Error sending RTP to " << m_rtp_info.remote_address.get_string());
    }
}

void RtpStream::close()
{
    m_datagram.close(true);
}


RtpStack::RtpStack(CallHandler * handler)
{
    m_handler = handler;
    
    for (int i=0; i<MAX_CHANNELS; i++)
        m_streams[i] = NULL;   
        
    aio_set_poll_interval(5);
    
    srand(time(NULL));
}

bool RtpStack::start_media(int chan_id, RtpInfo & info)
{
    if (m_streams[chan_id] != NULL)
        return false;

    LOGS(CallHandler::Debug, "RTP starting for channel " << chan_id);
    m_streams[chan_id] = new RtpStream(this, chan_id, info);
    return true;
}

void RtpStack::stop_media(int chan_id)
{
    if (m_streams[chan_id] != NULL)
    {
        LOGS(CallHandler::Debug, "RTP stopping for channel " << chan_id);
        m_streams[chan_id]->close();
        m_streams[chan_id] = NULL;
    }
}

void RtpStack::send(int chan_id, char * data, int size, long time)
{
    if (m_streams[chan_id] != NULL)
    {
        m_streams[chan_id]->send(data, size, time);
    }
}

void RtpStack::send_dtmf(int chan_id, int digit, int duration, long time)
{
    if (m_streams[chan_id] != NULL)
    {
        m_streams[chan_id]->send_dtmf(digit, duration, time);
    }
}

void RtpStack::process()
{
    
}

