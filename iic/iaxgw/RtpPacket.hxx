#ifndef RTPPACKET_HXX
#define RTPPACKET_HXX

/* ====================================================================
 * The Vovida Software License, Version 1.0 
 * 
 * Copyright (c) 2000-2003 Vovida Networks, Inc.  All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 
 * 3. The names "VOCAL", "Vovida Open Communication Application Library",
 *    and "Vovida Open Communication Application Library (VOCAL)" must
 *    not be used to endorse or promote products derived from this
 *    software without prior written permission. For written
 *    permission, please contact vocal@vovida.org.
 *
 * 4. Products derived from this software may not be called "VOCAL", nor
 *    may "VOCAL" appear in their name, without prior written
 *    permission of Vovida Networks, Inc.
 * 
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND
 * NON-INFRINGEMENT ARE DISCLAIMED.  IN NO EVENT SHALL VOVIDA
 * NETWORKS, INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT DAMAGES
 * IN EXCESS OF $1,000, NOR FOR ANY INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 * USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 * 
 * ====================================================================
 * 
 * This software consists of voluntary contributions made by Vovida
 * Networks, Inc. and many individuals on behalf of Vovida Networks,
 * Inc.  For more information on Vovida Networks, Inc., please see
 * <http://www.vovida.org/>.
 *
 */


#include "rtpTypes.h"
#include  <assert.h>

/** 
    Data structure for RTP data packets.   Each RTP payload is encapsulated
    in one of these objects.

    @see RtpSession
*/
class RtpPacket
{
    public:
        /** Constructor that allocates RTP packetData
            @param newpayloadSize Payload size not including RTP header 
            @param npadSize Number of pad bytes (not implemented)
            @param csrc_count Number of contributing sources in packet
         **/
        RtpPacket (int newpayloadSize, int npadSize = 0, int csrc_count = 0);

        /** Constructor that uses already allocated memory as packetData
            @param memory already allocatd memory
            @param size includes rtpHeader and padbytes
        **/
        //RtpPacket (char* memory, int size);

        /// clones an existing packet to a new size
        RtpPacket (RtpPacket* clonePacket, int newpayloadSize);

        /// Destructor that deallocates RTP packetData memory
        ~RtpPacket ();


        /// Pointer to packet data
        char* getPacketData ()
        {
            return packetData;
        }

        /// Pointer to packet header
        RtpHeader* getHeader ()
        {
            return header;
        }

        /// Pointer to beginning of payload
        char* getPayloadLoc ();

        /// Maximum payload size
        int getPayloadSize ();

        /** Sets payload usage
            @param size doesn't include RTP header
         **/
        void setPayloadUsage (int size);

        /// Size of payload stored
        int getPayloadUsage ();

        /// Pointer to begnning of padbyte (Not implemented)
        char* getPadbyteLoc ();

        /// Sets size of payload (Not implemented)
        void setPadbyteSize (int size);

        /// Size of padbyte (Not fully implemented)
        int getPadbyteSize ();

        /// Entire size of RTP packet including header, unused, and padbyte
        int getPacketAlloc ()
        {
            return packetAlloc;
        }

        /// Size of unused memory
        int getUnused ()
        {
            return unusedSize;
        }

        /** Sets size of RTP packet including header and padbyte
            Extra memory will be set as unused memory
         **/
        void setTotalUsage (int size)
        {
            assert (size <= packetAlloc);
            unusedSize = packetAlloc - size;
        }

        /// Size of RTP packet not including unused memory
        int getTotalUsage ()
        {
            return packetAlloc - unusedSize;
        }


        ///
        void setPayloadType (RtpPayloadType payloadType);
        ///
        RtpPayloadType getPayloadType () const;

        ///
        void setSequence (RtpSeqNumber newseq);
        ///
        RtpSeqNumber getSequence ();

        ///
        RtpTime getRtpTime();
        ///
        void setRtpTime (RtpTime time);

        ///
        void setSSRC (RtpSrc src);
        ///
        RtpSrc getSSRC ();

        /// Gets number of contributing sources
        void setCSRCcount (int i);   // use with cuation
        int getCSRCcount ();

        ///
        void setCSRC (RtpSrc src, unsigned int index);

        /** index value less range 1 to csrc_count
            @param index value less range 1 to csrc_count
         **/
        RtpSrc getCSRC (unsigned int index);


        ///
        void setVersion (int i)
        {
            header->version = i;
        }
        int getVersion ()
        {
            return header->version;
        }
        ///
        void setPaddingFlag (int i)
        {
            header->padding = i;
        }
        int getPaddingFlag ()
        {
            return header->padding;
        }
        ///
        void setExtFlag (int i)
        {
            header->extension = i;
        }
        int getExtFlag ()
        {
            return header->extension;
        }
        ///
        void setMarkerFlag (int i)
        {
            header->marker = i;
        }
        int getMarkerFlag ()
        {
            return header->marker;
        }


        /// flags
        bool sequenceSet;
        bool timestampSet;

        /// valid check
        bool isValid();

        /// Print packet contents
        void printPacket ();

    private:
        /// Pointer to raw memory allocation
        char* packetData;

        /// Allocated packet size
        int packetAlloc;

        /// Size of unused memory
        int unusedSize;

        /// Easy access to header information
        RtpHeader* header;

};


#endif // RTPPACKET_HXX
