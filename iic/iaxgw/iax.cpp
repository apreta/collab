/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
/*  
 *  IAX2 stack implemeted using libiax2.
 *
 */

#include <stdlib.h>
#include <stdio.h>

#if defined(WIN32)
#include <winsock.h>
#include <process.h>
#include <stddef.h>
#include <time.h>
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <pthread.h>
#endif

#include <sstream>

#include "iax.h"
#include "../jcomponent/util/log.h"

static unsigned short tcp_portnum = 443;
static const char * tcp_header = "voipgw";
static const unsigned max_audio_packet = 512;

#define KEY_LEN 16
#define ENCRYPTED_KEY_LEN 64
int read_key(unsigned char *in, int inlen, unsigned char *key, int *keylen);

CallHandler * sLogHandler = NULL;
IaxStack * sIaxStack = NULL;

#define LOG(level, msg) \
{ \
    std::stringstream ostr; \
    ostr << msg; \
    mAppHandler->on_log(level, __FILE__, __LINE__, ostr.str().c_str()); \
}

#define LOG2(stack, level, msg) \
{ \
    std::stringstream ostr; \
    ostr << msg; \
    stack->mAppHandler->on_log(level, __FILE__, __LINE__, ostr.str().c_str()); \
}

extern "C" {
void iic_log(int debug, const char * file, int line, const char * msg)
{
    if (sLogHandler)
        sLogHandler->on_log(debug ? CallHandler::Debug : CallHandler::Error, file, line, msg);
}
}

CallStack * create_iax_stack(CallHandler* appHandler, const char * host_address, const char * host_ip, unsigned port, bool debug, bool encrypt_audio)
{
    sLogHandler = appHandler;
    sIaxStack = new IaxStack(appHandler, host_address, host_ip, port, debug, encrypt_audio);
    return sIaxStack;
}

int IaxCall::s_call_id = 0;


IaxCall::IaxCall(struct iax_session * session)
{ 
    m_session = session; 
    m_start.tv_sec = 0; m_start.tv_usec = 0; 
    m_predicted = 0;
    m_call_id = ++s_call_id;
    m_terminated = false;
    m_encrypt = false;
    m_key_received = false;
    log_debug(ZONE, "Call %d created", m_call_id);
}


IaxCall::~IaxCall()
{
    log_debug(ZONE, "Call %d deleted", m_call_id);
}

void IaxCall::enable_encryption(bool flag)
{
    m_encrypt = flag;
}

int IaxCall::decrypt_audio(unsigned char *in, int inlen, unsigned char *out, int * outlen)
{
    char key[KEY_LEN+1];
    int keylen;
    
	/* decrypt data */
    
    if (!m_key_received)
    {
        m_key_received = true;
        
        if (read_key(in, ENCRYPTED_KEY_LEN, (unsigned char *)key, &keylen))
        {
            LOG2(sIaxStack, CallHandler::Error, "Unable to decode session key");
            *outlen = 0;
            return 1;
        }        
        
        in += ENCRYPTED_KEY_LEN;
        inlen -= ENCRYPTED_KEY_LEN;
        
        key[KEY_LEN] = '\0';
		m_blowfish.SetKey(key);
        m_blowfish.Initialize(BLOWFISH_KEY, sizeof(BLOWFISH_KEY));
    }
    
	DWORD len = inlen - sizeof(DWORD);

	*outlen = *((DWORD *)in); // read actual length from packet

	if (len > max_audio_packet || *outlen > (int)len || *outlen < 0)
	{
		*outlen = 0;
		return 1;
	}

	m_blowfish.Decode(in + sizeof(DWORD), out, len);

	return 0;
}

int IaxCall::encrypt_audio(unsigned char *in, int inlen, unsigned char *out, int * outlen)
{
	/* encrypt data */
	DWORD len = inlen + (BLOWFISH_BLOCKSIZE - inlen % BLOWFISH_BLOCKSIZE);

	*((DWORD*)out) = inlen;	// track actual data length in packet

    if (!m_key_received)
    {
        *outlen = 0;
        return 1;
    }
    
	m_blowfish.Encode(in, out + sizeof(DWORD), len);
	*outlen = len + sizeof(DWORD);

	return 0;    
}

IaxStack::IaxStack(CallHandler* appHandler, const char * hostDomain, const char * hostIp, unsigned port, bool debug, bool encrypt) :
    mHostDomain(hostDomain),
    mHostIp(hostIp),
    mUdpConnection(this),
    mTcpServer(this)
{
    mAppHandler = appHandler;
    mPort = port;
    mMsg = NULL;
    mEncrypt = encrypt;

    for (int i = 0; i<MAX_CHANNELS; i++)
        mCalls[i] = NULL;

    CommConnection::set_header(tcp_header);
    CommConnection::set_message_len_size(2);
    mTcpServer.initialize(hostIp, tcp_portnum);
    mUdpConnection.initialize(hostIp, port);
        
    IUseMutex lock(mIaxLock);
    iax_set_networking(sendto_callback, recvfrom_callback);
    iax_init(port);
    
    if (debug)
        iax_enable_debug();
    else
        iax_disable_debug();
}

void IaxStack::process(bool & work)
{
    process_iax(work);
}

void IaxStack::process_iax(bool & work)
{
    iax_event * ev;

    IUseMutex lock(mIaxLock);
    ev = iax_get_event(0);
    lock.unlock();
    
    while (ev != NULL)
    {
        switch (ev->etype)
        {
            case IAX_EVENT_CONNECT:
                handle_incoming(ev);
                break;
            
            case IAX_EVENT_ANSWER:
                break;
                
            case IAX_EVENT_ACCEPT:
                break;
                
            case IAX_EVENT_HANGUP:
                handle_disconnect(ev);
                break;
                
            case IAX_EVENT_VOICE:
                handle_audio(ev);
                work = true;
                break;
                
            case IAX_EVENT_DTMF:
                handle_dtmf(ev);
                break;

            case IAX_EVENT_REJECT:
            case IAX_EVENT_BUSY:
            case IAX_EVENT_RINGA:
                handle_failed(ev);
                break;

            case IAX_EVENT_TIMEOUT:
                handle_timeout(ev);
                break;
                
            default:
                LOG(CallHandler::Debug, "Unhandled IAX event " << ev->etype);
       } 
       
       lock.lock();
       iax_event_free(ev);
       ev = iax_get_event(0);
       lock.unlock();
    }
}

int IaxStack::sendto_callback(int s, const void *buf, size_t len, int flags, const struct sockaddr *to_addr, socklen_t to_len)
{
    CommMessage msg;
    msg.set_message((unsigned char *)buf, len);

    unsigned long peer_addr = ((sockaddr_in *)to_addr)->sin_addr.s_addr;
    unsigned short peer_port = ntohs(((sockaddr_in *)to_addr)->sin_port);

    TcpPeer key(peer_addr, peer_port);
    TcpPeer * peer = (TcpPeer*) sIaxStack->mTcpPeers.get(&key);
    if (peer != NULL)
    {
        peer->mConnection->send_message(&msg);
    }
    else
    {
        sIaxStack->mUdpConnection.send_message(peer_addr, peer_port, &msg);
    }

    return 0;
}

int IaxStack::recvfrom_callback(int s, void *buf, size_t len, int flags, struct sockaddr *from_addr, socklen_t *from_len)
{
    if (sIaxStack->mMsg != NULL)
    {
        struct sockaddr_in sin;
        sin.sin_family = AF_INET;
        sin.sin_addr.s_addr = sIaxStack->mMsgAddr;
        sin.sin_port = htons((short)sIaxStack->mMsgPort);
        memcpy(from_addr, &sin, sizeof(sin));
        *from_len = sizeof(sin);

        int ret_len = sIaxStack->mMsg->get_len();
        if (ret_len > 0 && ret_len < (int)len)
        {
            memcpy(buf, sIaxStack->mMsg->get_data(), ret_len);
        }
        else
        {
            LOG2(sIaxStack, CallHandler::Error, "Recieved message too large for IAX buffer, ignoring (" << ret_len << ")");
            ret_len = -1;
        }
        
        sIaxStack->mMsg = NULL;
        
        return (int)ret_len;
    }

    errno = EAGAIN;  // WOULDBLOCK for Win32
    return -1;
}

bool IaxStack::on_message(CommConnection * conn, CommMessage * msg)
{
    bool flag;
    
    assert(mMsg == NULL);
    
    mMsg = msg;
    mMsgAddr = conn->get_peer_addr();
    mMsgPort = conn->get_peer_port();
    
    // iaxclient will check the network for read events and process
    while (mMsg != NULL)
        process_iax(flag);

    return true;
}

bool IaxStack::on_accept(CommConnection * conn)
{
    // Keep track of peer address
    TcpPeer * key = new TcpPeer(conn->get_peer_addr(), conn->get_peer_port());
    TcpPeer * new_peer = new TcpPeer(conn->get_peer_addr(), conn->get_peer_port(), conn);
    
    TcpPeer * old_peer = (TcpPeer*) mTcpPeers.get(key);
    if (old_peer != NULL)
    {
        old_peer->mConnection->close(true);
        mTcpPeers.remove(key, true);
    }
    
    mTcpPeers.insert(key, new_peer);

    LOG(CallHandler::Debug, "New TCP connection from " << conn->get_peer_addr());
    return true;
}

bool IaxStack::on_close(CommConnection * conn)
{
    // Remove peer address
    TcpPeer key(conn->get_peer_addr(), conn->get_peer_port());
    
    TcpPeer * peer = (TcpPeer*) mTcpPeers.get(&key);
    if (peer && peer->mConnection == conn)
        mTcpPeers.remove(&key, true);
    
    LOG(CallHandler::Debug, "Closed TCP connection from " << conn->get_peer_addr());
    return true;
}

void IaxStack::handle_incoming(iax_event * ev)
{
    LOG(CallHandler::Debug, "Incoming IAX call from " << ev->ies.calling_name);
    
    int codec = convert_iax_format(ev->ies.format);
    if (codec < 0)
    {
        LOG(CallHandler::Status, "IAX call rejected, could not use prefereded codec: " << ev->ies.format);
        IUseMutex lock(mIaxLock);
        iax_reject(ev->session, "Could not use preferred codec");
        return;
    }
    
    int channel;
    RtpInfo info;
    info.codec = codec;
    fill_available_codecs(ev->ies.capability & ~codec, info);
    
    IString from = ev->ies.called_number ? ev->ies.called_number : "unknown";

    bool accept = mAppHandler->on_incoming(from, info, channel);
    if (accept)
    {
        LOG(CallHandler::Debug, "Incoming IAX call on channel " << channel);

        iax_set_jitterbuffer(ev->session, 0, -1, -1);
        
        IaxCall * newCall = new IaxCall(ev->session);
        newCall->m_rtp_info = info;
        newCall->enable_encryption(mEncrypt);

        IUseMutex lock(mIaxLock);
        mCalls[channel] = newCall;

        // Associate our call id with IAX session
        iax_set_private(ev->session, (void*)newCall->m_call_id);
    }
    else
    {
        LOG(CallHandler::Status, "IAX call rejected, too many calls");
        IUseMutex lock(mIaxLock);
        iax_reject(ev->session, "Too many calls");
    }
}

void IaxStack::handle_connect(iax_event * ev)
{
    LOG(CallHandler::Debug, "IAX call connected: " << ev->ies.calling_name);

    int channel = get_channel(ev);
    if (channel < 0)
        return;

    IaxCall * call = mCalls[channel];
    mAppHandler->on_connected(channel, call->m_rtp_info);    
}

void IaxStack::handle_disconnect(iax_event * ev)
{
    LOG(CallHandler::Debug, "IAX call disconnected: " << ev->ies.calling_name);

    int channel = get_channel(ev);
    terminate_call(channel, true);
}

void IaxStack::handle_failed(iax_event * ev)
{
    LOG(CallHandler::Debug, "IAX call failed");

    int channel = get_channel(ev);
    terminate_call(channel, true);
}

void IaxStack::handle_timeout(iax_event * ev)
{
    int channel = get_channel(ev);

    // Call timeout--most likely due to network connectivity loss, so disconnect call
    LOG(CallHandler::Debug, "IAX call timeout on channel " << channel);

    disconnect(channel);
}

void IaxStack::handle_dtmf(iax_event * ev)
{
    LOG(CallHandler::Debug, "IAX dtmf: " << ev->subclass);

    int channel = get_channel(ev);
    if (channel >= 0)
    {
        int rtp_digit = convert_iax_digit(ev->subclass);
        if (rtp_digit == -1)
        {
            LOG(CallHandler::Status, "Unknown DTMF digit");
            return;
        }
        
        long time = ev->ts;
        mAppHandler->on_dtmf(channel, rtp_digit, 0, time);
    }
}

// Receive audio data from IAX client
void IaxStack::handle_audio(iax_event * ev)
{
    int channel = get_channel(ev);
    if (channel >= 0)
    {
        IaxCall * call = mCalls[channel];
        char * data;
        int len;
        char buff[max_audio_packet];
        
        if (mEncrypt)
        {
            call->decrypt_audio(ev->data, ev->datalen, (unsigned char *)buff, &len);
            data = buff;
        }
        else
        {
            data = (char *) ev->data;
            len = ev->datalen;
        }
        
#ifdef RECALC_TS
        int samples = calc_samples(call->m_rtp_info.codec, len);

        // reconstruct timestamps for audio
        long time = calc_timestamp(*call, samples);
#else
        // pass through timestamp (in millis) from iax
        long time = ev->ts;
#endif

        //LOG(CallHandler::Debug, "IAX audio received: " << len << " bytes at ts " << time);

        if (data == NULL)
        {
            LOG(CallHandler::Debug, "No audio data received on channel " << channel);
            return;
        }
        
        mAppHandler->on_received(channel, data, len, time);
    }
}

int IaxStack::get_channel(int call_id)
{
    for (int i=0; i<MAX_CHANNELS; i++)
        if (mCalls[i] && mCalls[i]->m_call_id == call_id)
            return i;

    LOG(CallHandler::Debug, "No call for id " << call_id);
    return -1;
}

int IaxStack::get_channel(iax_event * ev)
{
    int call_id = (int)(long)iax_get_private(ev->session);
    return get_channel(call_id);
}

IaxCall * IaxStack::get_call(int channel)
{
    if (channel >= 0 && channel < MAX_CHANNELS && mCalls[channel])
        return mCalls[channel];
        
    LOG(CallHandler::Debug, "No call for channel " << channel);
    return NULL;
}

void IaxStack::terminate_call(int channel, bool remote_hangup)
{
    if (channel < 0 || channel >= MAX_CHANNELS)
        return;
        
    IaxCall * call = mCalls[channel];
    if (call && !call->m_terminated)
    {   
        LOG(CallHandler::Debug, "IAX terminating call on channel " << channel);
        
        call->m_terminated = true;
        call->m_session = NULL;
        
        mAppHandler->on_disconnect(channel, remote_hangup);
        mAppHandler->on_released(channel);
        
        IUseMutex lock(mIaxLock);
        mCalls[channel] = NULL;
    }
}

int IaxStack::convert_iax_format(int iax_format)
{
    switch (iax_format)
    {
          case AST_FORMAT_ULAW:  
            return rtpPayloadPCMU;
          case AST_FORMAT_G726:      
            return rtpPayloadG726_32;
          case AST_FORMAT_G729A:
            return rtpPayloadG729;
          case AST_FORMAT_G723_1:
            return rtpPayloadG723;
          case AST_FORMAT_GSM:
			return rtpPayloadGSM;
          case AST_FORMAT_AMR_NB:
            return 98;
          case AST_FORMAT_ILBC:
            return 99; // ILBC 20
    }
    
    return -1;
}

void IaxStack::fill_available_codecs(int capability, RtpInfo &rtp)
{
	unsigned count = 0;
	
	if (capability & AST_FORMAT_ULAW) 
		rtp.available_codecs[count++] = rtpPayloadPCMU;
	if (capability & AST_FORMAT_G726)
        rtp.available_codecs[count++] =  rtpPayloadG726_32;
    if (capability &  AST_FORMAT_G729A)
		rtp.available_codecs[count++] =  rtpPayloadG729;
	if (capability &  AST_FORMAT_G723_1)
		rtp.available_codecs[count++] =  rtpPayloadG723;
	if (capability &  AST_FORMAT_GSM)
		rtp.available_codecs[count++] =  rtpPayloadGSM;
	if (capability &  AST_FORMAT_AMR_NB)
		rtp.available_codecs[count++] =  98;
	if (capability &  AST_FORMAT_ILBC)
		rtp.available_codecs[count++] =  99; // ILBC 20	
	
	rtp.available_codec_count = count;
}

int IaxStack::convert_rtp_format(int rtp_format)
{
    switch (rtp_format)
    {
        case rtpPayloadPCMU:
            return AST_FORMAT_ULAW;
        case rtpPayloadG726_32:
            return AST_FORMAT_G726;
        case rtpPayloadG729:
            return AST_FORMAT_G729A;
        case rtpPayloadG723:
            return AST_FORMAT_G723_1;
        case rtpPayloadGSM:
            return AST_FORMAT_GSM;
        case 98:
            return AST_FORMAT_AMR_NB;
        case 99: // ILBC 20
            return AST_FORMAT_ILBC;
    }
    
    return -1;
}

int IaxStack::convert_iax_digit(int digit)
{
    int out = -1;

    if (digit >= '0' && digit <= '9')
        out = digit - '0';
    else if (digit == '*')
        out = DTMFEventDigitStar;
    else if (digit == '#')
        out = DTMFEventDigitHash;
 
    return out;
}

int IaxStack::calc_samples(int codec, int size)
{
    switch (codec)
    {
        case rtpPayloadPCMU:
            return size;        /* 1 byte = 1 sample */
        case rtpPayloadG726_32:
            return size * 2;    /* 4 bites = 1 sample */
        case rtpPayloadG729:
            return size * 8;   /* 10 bytes = 80 samples */
        case rtpPayloadG723:
            return size * 8;   /* ??? */
        case 98:
            return size/31 * 160; /* 31 bytes = 160 samples (12.2 rate) */
        case 99:                /* ILBC 20: 38 bytes = 160 samples */
            return size/38 * 160;
        case 100:               /* ILBC 30: 50 bytes = 240 samples */
            return size/50 * 240;
    }
    
    return size;
}

static void add_ms(struct timeval *tv, int ms) 
{
  tv->tv_usec += ms * 1000;
  if(tv->tv_usec > 1000000) {
      tv->tv_usec -= 1000000;
      tv->tv_sec++;
  }
  if(tv->tv_usec < 0) {
      tv->tv_usec += 1000000;
      tv->tv_sec--;
  }
}

int IaxStack::calc_timestamp(IaxCall & call, int samples)
{
	int ms;
	struct timeval tv;	
	
	/* If this is the first packet we're sending, get our
	   offset now. */
	if (!call.m_start.tv_sec && !call.m_start.tv_usec)
		gettimeofday(&call.m_start, NULL);

	/* Otherwise calculate the timestamp from the current time */
	gettimeofday(&tv, NULL);
		
	/* Calculate the number of milliseconds since we sent the first packet */
	ms = (tv.tv_sec - call.m_start.tv_sec) * 1000 +
		 (tv.tv_usec - call.m_start.tv_usec) / 1000;

	if (ms < 0) 
		ms = 0;

    /* If we haven't most recently sent silence, and we're
     * close in time, use predicted time */
    if(abs(ms - call.m_predicted) <= 240) {
        /* Adjust our txcore, keeping voice and non-voice
         * synchronized */
        add_ms(&call.m_start, (int)(ms - call.m_predicted)/10);

        if(!call.m_predicted)
            call.m_predicted = ms; 

        ms = call.m_predicted; 
    } else {
        /* in this case, just use the actual time, since
         * we're either way off (shouldn't happen), or we're
         * ending a silent period -- and seed the next predicted
         * time.  Also, round ms to the next multiple of
         * frame size (so our silent periods are multiples
         * of frame size too) */
        int diff = ms % (samples / 8);
        if(diff)
            ms += samples/8 - diff;
        call.m_predicted = ms; 
    }

	/* set next predicted ts based on 8khz samples */
    call.m_predicted = call.m_predicted + samples / 8;

	return ms;
}

void IaxStack::answer(int chan_id, RtpInfo & info)
{
    LOG(CallHandler::Debug, "IAX answer call " << chan_id << " format " << info.codec);

    IaxCall * call = get_call(chan_id);
    if (call != NULL)
    {
        IUseMutex lock(mIaxLock);
        
        int format = convert_rtp_format(info.codec);
        iax_accept(call->m_session, format);
        iax_answer(call->m_session);
        
        lock.unlock();

        // No separate event in IAX library; the protocol has an ACK frame
        // but that's hidden from us.
		mAppHandler->on_connected(chan_id, call->m_rtp_info);  
	}
}

void IaxStack::disconnect(int chan_id)
{
    LOG(CallHandler::Debug, "IAX disconnect channel " << chan_id);
    
    IaxCall * call = get_call(chan_id);
    if (call != NULL)
    {
        IUseMutex lock(mIaxLock);

        if (!call->m_terminated)
        {
            iax_hangup(call->m_session, "Bye");
            call->m_session = NULL;
        }
      
        lock.unlock();

        terminate_call(chan_id, false);
    }
}

void IaxStack::call(int chan_id, const char * from, const char * address, RtpInfo & info)
{
    // No-op, inbound only for IAX calls    
}

bool IaxStack::start_media(int chan_id, RtpInfo & info)
{
    return true;
}

void IaxStack::stop_media(int chan_id)
{
}

// Handle audio data from other endpoint, will be called from RTP stream thread
void IaxStack::send(int chan_id, char * data, int size, long time)
{
    IUseMutex lock(mIaxLock);
    
    IaxCall * call = get_call(chan_id);
    if (call != NULL && !call->m_terminated)
    {
        int check = (int)(long)iax_get_private(call->m_session);
        if (check != call->m_call_id)
        {
            LOG(CallHandler::Debug, "Invalid IAX session for channel " << chan_id);
            return;
        }
        
        int format = convert_rtp_format(call->m_rtp_info.codec);
        int samples = calc_samples(call->m_rtp_info.codec, size);
        int datalen;
        char buff[max_audio_packet];
        
        if (mEncrypt)
        {
            call->encrypt_audio((unsigned char *)data, size, (unsigned char *)buff, &datalen);
            data = buff;
        }
        else
        {
            datalen = size;
        }

        if (data == NULL)
        {
            LOG(CallHandler::Debug, "No audio data to send on channel " << chan_id);
            return;
        }
        
        iax_send_voice_ts(call->m_session, format, (unsigned char *)data, datalen, samples, time);
    }
}

// Handle DTMFs from other endpoint
void IaxStack::send_dtmf(int chan_id, int digit, int duration, long time)
{
    IUseMutex lock(mIaxLock);
    
    IaxCall * call = get_call(chan_id);
    if (call != NULL && !call->m_terminated)
    {
        iax_send_dtmf(call->m_session, digit);
    }
}

void IaxStack::set_outbound_proxy(const char * address)
{
    
}
