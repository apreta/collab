/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

/*
 * Simple RTP stack.
 *
 * Uses asynccomm library which runs a separate background thread to process all
 * network events for established connections.
 */

#ifndef GW_RTP_H
#define GW_RTP_H

#include "callapi.h"
#include "../util/asynccomm/comm.h"
#include "RtpPacket.hxx"
#include <sys/time.h>

class RtpStack;

#define MAX_RTP_PACKET 2048
#define DTMF_INTERDIGIT 320  // In samples, this is 40 ms


// Bi-directional RTP stream.
class RtpStream : public CommEvent
{
public:
    RtpStream(RtpStack* stack, int chan_id, RtpInfo& info);
    virtual ~RtpStream() { }

    bool on_message(CommConnection * conn, CommMessage * msg);
    bool on_error(CommConnection * conn);
    bool on_close(CommConnection * conn);

    void send(char * data, int len, long time);
    void send_dtmf(int digit, int duration, long time);
    void transmit();
    void close();

private:
    CommDatagram m_datagram;
    RtpStack * m_stack;
    RtpInfo m_rtp_info;
    int m_chan_id;
    long m_ssrc;
    long m_send_seq;
    
    RtpPacket m_send;
    RtpPacket m_recv;

    unsigned m_dtmf_time;
    unsigned m_last_sent;

    CommMessage m_send_msg;
};


// Manage RTP streams.
class RtpStack
{
    friend class RtpStream;
    
public:
    RtpStack(CallHandler * handler);

    bool start_media(int chan_id, RtpInfo & info);
    void stop_media(int chan_id);

    void send(int chan_id, char * data, int size, long time);
    void send_dtmf(int chan_id, int digit, int duration, long time);
    
    void process();

private:
    CallHandler * m_handler;
    RtpStream * m_streams[MAX_CHANNELS];
};

#endif

