/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
/*  
 *  SIP stack implemeted using resiprocate.
 *
 */
#if !defined(SIP_H)
#define SIP_H

#include "resip/stack/StackThread.hxx"
#include "resip/dum/MasterProfile.hxx"
#include "resip/dum/RegistrationHandler.hxx"
#include "resip/dum/SubscriptionHandler.hxx"
#include "resip/dum/PublicationHandler.hxx"
#include "resip/dum/OutOfDialogHandler.hxx"
#include "resip/dum/InviteSessionHandler.hxx"
#include "resip/dum/DialogUsageManager.hxx"
#include "resip/dum/AppDialog.hxx"
#include "resip/dum/AppDialogSet.hxx"
#include "resip/dum/AppDialogSetFactory.hxx"

#include "callapi.h"
#include "rtp.h"

namespace resip
{

static int sip_id = 0;
const int PingInterval = 60;


class MyAppDialog : public AppDialog
{
public:
   MyAppDialog(HandleManager& ham, int id) : AppDialog(ham)
   {  
       this->id = id;
   }
   virtual ~MyAppDialog() 
   { 
   }
   int id;
};

class SipSession : public AppDialogSet
{
public:
   SipSession(DialogUsageManager& dum, int id) : AppDialogSet(dum)
   {  
       this->id = id;
       chan_id = -1;
       terminated = false;
       next_ping = time(NULL) + PingInterval;
   }
   virtual ~SipSession() 
   {  
   }
   virtual AppDialog* createAppDialog(const SipMessage& msg) 
   {  
      return new MyAppDialog(mDum, id);  
   }
   virtual SharedPtr<UserProfile> selectUASUserProfile(const SipMessage& msg) 
   { 
      return mDum.getMasterUserProfile(); 
   }
   int id;
   int chan_id;
   RtpInfo rtp_info;
   InviteSessionHandle sesh;
   SipMessage msg;
   bool terminated;
   long next_ping;
};

class SessionFactory : public AppDialogSetFactory
{
public:
   virtual AppDialogSet* createAppDialogSet(DialogUsageManager& dum, const SipMessage& msg) 
   {  return new SipSession(dum, ++sip_id);  }
};


/** Interface functor for external logging. */
class MyExternalLogger : public ExternalLogger
{
      CallHandler * mAppHandler;
      
   public:
      MyExternalLogger(CallHandler * handler)
      {
          mAppHandler = handler;
      }
      
      /** return true to also do default logging, false to supress default logging. */
      bool operator()(Log::Level level,
                      const Subsystem& subsystem, 
                      const Data& appName,
                      const char* file,
                      int line,
                      const Data& message,
                      const Data& messageWithHeaders);
};


class UserAgent : public ClientRegistrationHandler, 
                  public OutOfDialogHandler, 
                  public InviteSessionHandler,
                  public CallStack
{
   public:
      UserAgent(CallHandler* appHandler, const char * hostDomain, const char * hostIp, unsigned port, bool debug);
      virtual ~UserAgent();

      void startup();
      void shutdown();

      void process(bool& work);

      void answer(int chan_id, RtpInfo & info);
      void disconnect(int chan_id);
      void call(int chan_id, const char * from, const char * address, RtpInfo & info);

      bool start_media(int chan_id, RtpInfo & info);
      void stop_media(int chan_id);

      void send(int chan_id, char * data, int size, long time);
      void send_dtmf(int chan_id, int digit, int duration, long time);
      
      void set_outbound_proxy(const char * address);
      
   public:
      // Invite Session Handler /////////////////////////////////////////////////////
      virtual void onNewSession(ClientInviteSessionHandle h, InviteSession::OfferAnswerType oat, const SipMessage& msg);
      virtual void onNewSession(ServerInviteSessionHandle h, InviteSession::OfferAnswerType oat, const SipMessage& msg);
      virtual void onFailure(ClientInviteSessionHandle h, const SipMessage& msg);
      virtual void onEarlyMedia(ClientInviteSessionHandle, const SipMessage&, const SdpContents&);
      virtual void onProvisional(ClientInviteSessionHandle, const SipMessage& msg);
      virtual void onConnected(ClientInviteSessionHandle h, const SipMessage& msg);
      virtual void onConnected(InviteSessionHandle, const SipMessage& msg);
      virtual void onStaleCallTimeout(ClientInviteSessionHandle);
      virtual void onTerminated(InviteSessionHandle h, InviteSessionHandler::TerminatedReason reason, const SipMessage* msg);
      virtual void onRedirected(ClientInviteSessionHandle, const SipMessage& msg);
      virtual void onAnswer(InviteSessionHandle, const SipMessage& msg, const SdpContents&);
      virtual void onOffer(InviteSessionHandle handle, const SipMessage& msg, const SdpContents& offer);
      virtual void onOfferRequired(InviteSessionHandle, const SipMessage& msg);
      virtual void onOfferRejected(InviteSessionHandle, const SipMessage* msg);
      virtual void onDialogModified(InviteSessionHandle, InviteSession::OfferAnswerType oat, const SipMessage& msg);
      virtual void onInfo(InviteSessionHandle, const SipMessage& msg);
      virtual void onInfoSuccess(InviteSessionHandle, const SipMessage& msg);
      virtual void onInfoFailure(InviteSessionHandle, const SipMessage& msg);
      virtual void onRefer(InviteSessionHandle, ServerSubscriptionHandle, const SipMessage& msg);
      virtual void onReferAccepted(InviteSessionHandle, ClientSubscriptionHandle, const SipMessage& msg);
      virtual void onReferRejected(InviteSessionHandle, const SipMessage& msg);
      virtual void onReferNoSub(InviteSessionHandle, const SipMessage& msg);

      virtual void onMessage(InviteSessionHandle, const SipMessage& msg);
      virtual void onMessageSuccess(InviteSessionHandle, const SipMessage& msg);
      virtual void onMessageFailure(InviteSessionHandle, const SipMessage& msg);
      
      // Registration Handler ////////////////////////////////////////////////////////
      virtual void onSuccess(ClientRegistrationHandle h, const SipMessage& response);
      virtual void onFailure(ClientRegistrationHandle h, const SipMessage& response);
      virtual void onRemoved(ClientRegistrationHandle h, const SipMessage& response);
      virtual int onRequestRetry(ClientRegistrationHandle h, int retryMinimum, const SipMessage& msg);

      // OutOfDialogHandler //////////////////////////////////////////////////////////
      virtual void onSuccess(ClientOutOfDialogReqHandle, const SipMessage& response);
      virtual void onFailure(ClientOutOfDialogReqHandle, const SipMessage& response);
      virtual void onReceivedRequest(ServerOutOfDialogReqHandle, const SipMessage& request);
      virtual void onForkDestroyed(ClientInviteSessionHandle);

   protected:
      void addTransport(TransportType type, const char *ip_addr, int port);
      bool findMatchingCodec(const SdpContents& in, resip::SdpContents::Session::Codec& codec);
      bool buildAnswerSdp(SdpContents& in, SdpContents& out, RtpInfo& rtp);
      bool buildNewSdp(SdpContents& out, RtpInfo& rtp);
      void addCodec(SdpContents::Session::Medium & medium, unsigned codec);
      void pingCalls();
      
      int mUdpPort;
      int mTcpPort;
      int mRegisterDuration;
      bool mNoV4;
      bool mNoV6;
      Uri mAor;
      Data mTlsDomain;
      
   private:
      SharedPtr<MasterProfile> mProfile;
      Security* mSecurity;
      SipStack mStack;
      DialogUsageManager mDum;
      StackThread mStackThread;
      CallHandler* mAppHandler;
      std::list<Codec> mCodecs;
      SipSession* mCalls[MAX_CHANNELS];
      Data mHostDomain;
      Data mHostIp;
      MyExternalLogger* mLogger;
      RtpStack mRtpStack;
      
};
 
}

#endif

