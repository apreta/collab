/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */
/*  
 *  IAX2 stack implemeted using libiax2.
 *
 */

#ifndef IAX_H
#define IAX_H

#include "callapi.h"
#include <iax/iax-client.h>
#include "rtpTypes.h"
#include "../util/iobject.h"
#include "../util/ithread.h"
#include "../util/asynccomm/comm.h"
#include "../util/asynccomm/commsrvr.h"
#include "../util/blowfish/blowfish.h"


class TcpPeer : public IObject
{
public:
    unsigned long mPeerAddr;
    unsigned short mPeerPort;
    CommConnection * mConnection;

    TcpPeer(unsigned long addr, unsigned short port, CommConnection * conn = NULL)
    {
        mPeerAddr = addr;
        mPeerPort = port;
        mConnection = conn;
    }
    
    enum { TYPE_TCP_PEER = 8451 };
    virtual int type()
        { return TYPE_TCP_PEER; }
    virtual unsigned int hash() { return mPeerAddr; }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && mPeerAddr == ((TcpPeer*)obj)->mPeerAddr && mPeerPort == ((TcpPeer*)obj)->mPeerPort; }
};


class IaxCall
{
public:
    struct iax_session * m_session;
    RtpInfo m_rtp_info;
    int m_call_id;
    bool m_terminated;

    struct timeval m_start;
    long m_predicted;
    
    CBlowFish m_blowfish;
    bool m_encrypt;
    bool m_key_received;
    
    static int s_call_id;
    
    IaxCall(struct iax_session * session);
    ~IaxCall();

    void enable_encryption(bool flag);
    int decrypt_audio(unsigned char *in, int inlen, unsigned char *out, int * outlen);
    int encrypt_audio(unsigned char *in, int inlen, unsigned char *out, int * outlen);
};


class IaxStack : public CallStack, public CommEvent
{
    friend class IaxCall;
    
public:
    IaxStack(CallHandler* appHandler, const char * hostDomain, const char * hostIp, unsigned port, bool debug, bool encrypt_audio);
    virtual ~IaxStack() { }
    
    void process(bool & work);
    void process_iax(bool & work);

    void answer(int chan_id, RtpInfo & info);
    void disconnect(int chan_id);
    void call(int chan_id, const char * from, const char * address, RtpInfo & info);

    bool start_media(int chan_id, RtpInfo & info);
    void stop_media(int chan_id);

    void send(int chan_id, char * data, int size, long time);
    void send_dtmf(int chan_id, int digit, int duration, long time);
    
    void set_outbound_proxy(const char * address);

    static int sendto_callback(int s, const void *buf, size_t len, int flags, const struct sockaddr *to_addr, socklen_t to_len);
    static int recvfrom_callback(int s, void *buf, size_t len, int flags, struct sockaddr *from_addr, socklen_t *from_len);
    bool on_message(CommConnection * conn, CommMessage * msg);
    bool on_accept(CommConnection * conn);
    bool on_close(CommConnection * conn);

    void handle_incoming(iax_event * ev);
    void handle_connect(iax_event * ev);
    void handle_disconnect(iax_event * ev);
    void handle_failed(iax_event * ev);
    void handle_timeout(iax_event * ev);
    void handle_audio(iax_event * ev);
    void handle_dtmf(iax_event * ev);
    
    int get_channel(int call_id);
    int get_channel(iax_event * ev);
    IaxCall * get_call(int chan_id);
    void terminate_call(int channel, bool remote_hangup);
    int convert_iax_digit(int digit);
    int convert_iax_format(int iax_format);
	void fill_available_codecs(int capability, RtpInfo &rtp);
    int convert_rtp_format(int rtp_format);
    int calc_samples(int codec, int size);
    int calc_timestamp(IaxCall & call, int samples);
    
private:
    CallHandler* mAppHandler;
    IString mHostDomain;
    IString mHostIp;
    int mPort;
    IaxCall * mCalls[MAX_CHANNELS];
    IMutex mIaxLock;
    bool mEncrypt;
    
    CommDatagram mUdpConnection;
    CommServer mTcpServer;

    IHash mTcpPeers;
    
    CommMessage * mMsg;
    unsigned long mMsgAddr;
    unsigned short mMsgPort;
    
};



#endif

