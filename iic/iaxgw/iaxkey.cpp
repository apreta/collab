/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include <unistd.h>
#include <stdio.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <strings.h>
#include <stdlib.h>
#include "../jcomponent/util/log.h"

#define OPENSSL_NO_KRB5

#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <openssl/objects.h>
#include <openssl/x509.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
  	
EVP_PKEY *privKey = NULL;

void log_ssl_error()
{
    char buffer[1024];
    unsigned long err;
    
    for (err = ERR_get_error(); err != 0; err = ERR_get_error())
    {
        ERR_error_string_n(err, buffer, sizeof(buffer));
        log_error(ZONE, "SSL error: %s", buffer);
    }
}


EVP_PKEY * ReadPublicKey(const char *certfile)
{
  FILE *fp = fopen (certfile, "r");   
  X509 *x509;
  EVP_PKEY *pkey;

  if (!fp) 
  {
      log_error(ZONE, "Could not open public key file: %s", certfile);
      return NULL;
  }

  x509 = PEM_read_X509(fp, NULL, 0, NULL);

  if (x509 == NULL) 
  {  
     return NULL;   
  }

  fclose (fp);
  
  pkey=X509_extract_key(x509);

  X509_free(x509);

  return pkey; 
}

EVP_PKEY *ReadPrivateKey(const char *keyfile)
{
	FILE *fp = fopen(keyfile, "r");
	EVP_PKEY *pkey;

	if (!fp)
    {
        log_error(ZONE, "Could not open private key file: %s", keyfile);
		return NULL;
    }

	pkey = PEM_read_PrivateKey(fp, NULL, 0, NULL);

	fclose (fp);

	return pkey;
}

int read_key(unsigned char *in, int inlen, unsigned char *key, int *keylen)
{
    // Initialize and read private key if needed
    if (privKey == NULL)
    {
        ERR_load_crypto_strings();

        privKey = ReadPrivateKey("/opt/iic/conf/privkey.pem");
        if (!privKey) 
        {  
            log_ssl_error();
            return -1;
        }
    }

    // Decode session key sent by client
	int stat = RSA_private_decrypt(inlen, in, key, privKey->pkey.rsa, RSA_PKCS1_PADDING);
    if (stat < 0)
    {
        log_ssl_error();
        return -1;
    }
    *keylen = 16;
    
    return 0;
}

