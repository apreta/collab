// jcomp_test.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xmlrpc.h>
#include "../jcomponent/rpc.h"
#include "../jcomponent/jcomponent_ext.h"
#include "../jcomponent/util/rpcresult.h"
#include "../jcomponent/util/eventlog.h"

bool complete = false;

void RPC_error(const char *id, xmlrpc_int32 fault_code, const char * message)
{
    // Handle, so clients are not waiting forever
    printf("Error processing RPC request: %s: %d - %s\n", id, fault_code, message);

	char * end = strchr(id, ':');
	if (end == NULL)
		return;
	int len = end - id;

	// Use id to figure out which call failed & handle 
}

// Called from background thread, synchronize if necessary.
void RPC_result(const char * id, xmlrpc_value * resval)
{
    xmlrpc_env env;

    xmlrpc_env_init(&env);

	char * end = strchr(id, ':');
	if (end == NULL)
		return;
	int len = end - id;

	if (!strncmp(id, "pin", len))
	{
		RPCResultSet rs(&env, resval);

		// Figure how to handle result from id
		int found, options;
		char *meetingid, *participantid, *userid, *username, *email, *name;
		
		// Read result and handle
		//  "FOUND", "MEETINGID", "PARTICIPANTID", "USERID", "EMAIL", "USERNAME", "NAME", "OPTIONS"
		//  FOUND: 1 = meeting found, 2 = meeting and user found, 0 = not found
		if (!rs.get_output_values("(issssssi)", &found, &meetingid, &participantid, &userid, &email, &username, &name, &options))
		{
			printf("Error parsing info from pin lookup\n");
		}
		else
			printf("Found: %d, Meeting: %s, Name: %s\n", found, meetingid, name);

		complete = true;
	}

	if (env.fault_occurred)
	{
		char buffer[512];
		strcpy(buffer, "Result error: ");
		if (env.fault_string)
			strncat(buffer, env.fault_string, 512 - 16);
		else
			strcat(buffer, "unknown cause");
		RPC_error(id, env.fault_code, buffer);
	}

    xmlrpc_env_clean(&env);
}


int main(int argc, char* argv[])
{
	char pin[32];

    EventLog el(EvnLog_Type_Call);
}

