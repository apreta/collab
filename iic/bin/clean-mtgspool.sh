#!/bin/bash -f

find /var/iic/mtgspool/done -type f -mtime +7 -print | xargs rm -f
find /var/iic/mtgspool/done -type d -mindepth 1 -empty -print | xargs rmdir
