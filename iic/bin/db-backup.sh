#!/bin/bash

. /opt/iic/conf/global-config

function rotate_backups
{
    local dst=$(($db_backup_history))
    local src=$(($db_backup_history-1))
    while [ $src -gt 0 ]; do
	mv -f $db_backup_dir/db.tar.bz2.$src $db_backup_dir/db.tar.bz2.$dst
	src=$((src-1))
	dst=$((dst-1))
    done
    mv -f $db_backup_dir/db.tar.bz2   $db_backup_dir/db.tar.bz2.1
}

function backup_database
{
    pg_dump -b -F t -f $db_backup_dir/db.tar zon
    bzip2 $db_backup_dir/db.tar
}

function vacuum_database
{
    echo "VACUUM ANALYZE;" | psql zon
}

rotate_backups
backup_database
vacuum_database
