#!/bin/sh -f

. /opt/iic/conf/global-config

docshare_path=/var/iic/htdocs/repository/SharedDocuments

function remove_archives
{
    local amt_to_remove=$1
    local youngest=$2
    local archive=""
    local acc_time=""
    local size=""
    local stop=0
    local failed_trash=0

    until [ $stop -eq 1 ] || [ $failed_trash -eq 1 ]; do
	read archive acc_time size
	if [ $acc_time -lt $youngest -a $amt_to_remove -gt 0 ]; then
	    echo "Removing $archive"
	    if [ ! -z "$docshare_trash_folder" ]; then
		# archive name is encrypted form of meeting id and
		# time of day so likelihood of name collision is low.
		# esp. if the trash folder is emptied regularly
		if ! cp -r $archive $docshare_trash_folder ; then
		    failed_trash=1
		fi
	    fi
	    [ $failed_trash -eq 0 ] && rm -rf $archive
	    [ $failed_trash -eq 0 ] && amt_to_remove=$(( $amt_to_remove - $size ))
	else
	    stop=1
	fi
    done < /tmp/docshare-list

    if [ $failed_trash -eq 1 ]; then
	echo "Unable to clean archives because couldn't copy them to $docshare_trash_folder" | \
	    mail -s "Meeting archive size warning" ${sysadm_email[*]}
    fi

    if [ $acc_time -ge $youngest -a $amt_to_remove -gt 0 ]; then
	echo "Unable to clean ${amt_to_remove}k due to archives $docshare_cleanup_min_age days old or less" | \
	    mail -s "Meeting archive size warning" ${sysadm_email[*]}
    fi
}

function make_archive_list
{
    # get file w/ lines of <archive> <mod-time>
    stat --format="%n %X" $docshare_path/*/* > /tmp/docshare-accessed

    # get file w/ lines of <archive> <size>
    du -s -k $docshare_path/*/* | \
	gawk '{print $2,$1}' > /tmp/docshare-sizes

    # merge into file w/ lines of
    #     <archive> <mod-time> <size>
    # sorted by <mod-time>
    join /tmp/docshare-accessed /tmp/docshare-sizes | \
	sort -n +1 -2 \
    > /tmp/docshare-list
}

if [ ! -e $docshare_path ]; then
    echo "Meeting archives location $docshare_path does not exist" | \
	mail -s "Meeting archive does not exist" ${sysadm_email[*]}
    exit -1
fi

cur_sz=$( du -s --block-size=1M $docshare_path | gawk '{print $1}' )
now=$( date +"%s" )
if [ $cur_sz -gt $docshare_cleaning_threshold ]; then
    if [ "$docshare_cleanup_action" == "clean" ]; then
	make_archive_list
	
	remove_sz=$(( $(($cur_sz - $docshare_target_sz)) * 1024 ))
	minage=$(( $docshare_cleanup_min_age * 86400 )) # seconds
	youngest_removeable=$(( $now - $minage ))
	
	remove_archives $remove_sz $youngest_removeable
    elif [ "$docshare_cleanup_action" == "warn" ]; then
	echo "Meeting archives stored in $docshare_path now total ${cur_sz}M" | \
	    mail -s "Meeting archive size warning" ${sysadm_email[*]}
    fi
fi
