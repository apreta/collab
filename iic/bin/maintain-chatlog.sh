#!/bin/sh -f

. /opt/iic/conf/global-config

log_chat_path=/var/iic/chatlog

function remove_logs
{
    local amt_to_remove=$1
    local log=""
    local acc_time=""
    local size=""
    local failed_trash=0

    until [ $amt_to_remove -le 0 ] || [ $failed_trash -eq 1 ]; do
	read log acc_time size
	echo "Removing $log"
	if [ ! -z "$log_chat_trash_folder" ]; then
	    if ! cp -r $log $log_chat_trash_folder ; then
		failed_trash=1
	    fi
	fi
	[ $failed_trash -eq 0 ] && rm -rf $log
	[ $failed_trash -eq 0 ] && amt_to_remove=$(( $amt_to_remove - $size ))
    done < /tmp/log-chat-list

    if [ $failed_trash -eq 1 ]; then
	echo "Unable to clean logs because couldn't copy them to $log_chat_trash_folder" | \
	    mail -s "Chat log size warning" ${sysadm_email[*]}
    fi
}

function make_log_list
{
    # get file w/ lines of <log> <mod-time>
    find $log_chat_path -type f -print | \
	xargs stat --format="%n %Y" \
    > /tmp/log-chat-modified

    # get file w/ lines of <log> <size>
    find $log_chat_path -type f -print | \
	xargs du -s -k | \
	gawk '{print $2,$1}' \
    > /tmp/log-chat-sizes

    # merge into file w/ lines of
    #     <log> <mod-time> <size>
    # sorted by <mod-time>
    join /tmp/log-chat-modified /tmp/log-chat-sizes | \
	sort -n +1 -2 \
    > /tmp/log-chat-list  # used above in remove_logs()
}

if [ ! -e $log_chat_path ]; then
    echo "Chat logs location $log_chat_path does not exist" | \
	mail -s "Chat log path, $log_chat_path, does not exist" ${sysadm_email[*]}
    exit -1
fi

cur_sz=$( du -s --block-size=M $log_chat_path | gawk '{print $1}' | sed -e 's+M$++' )
if [ $cur_sz -gt $log_chat_clean_sz ]; then
    if [ "$log_chat_clean_action" == "clean" ]; then
	make_log_list
	# might want to make it something like remove log files
	# until 80% of log_chat_clean_sz is reached.
	remove_sz=$(( $(($cur_sz - $log_chat_clean_sz)) * 1024 ))
	remove_logs $remove_sz
    elif [ "$log_chat_clean_action" == "warn" ]; then
	echo "Chat logs stored in $log_chat_path on $(hostname) now total ${cur_sz}M" | \
	    mail -s "Chat log size warning" ${sysadm_email[*]}
    fi
fi
