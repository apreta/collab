#!/bin/sh -f

. /opt/iic/conf/global-config

archives_path=/var/iic/htdocs/repository/mtgarchive

function remove_archives
{
    local amt_to_remove=$1
    local youngest=$2
    local archive=""
    local mod_time=""
    local size=""
    local stop=0
    local failed_trash=0

    until [ $stop -eq 1 ] || [ $failed_trash -eq 1 ]; do
	read archive mod_time size
	if [ $mod_time -lt $youngest -a $amt_to_remove -gt 0 ]; then
	    echo "Removing $archive"
	    if [ ! -z "$mtgarchive_trash_folder" ]; then
		# archive name is encrypted form of meeting id and
		# time of day so likelihood of name collision is low.
		# esp. if the trash folder is emptied regularly
		if ! cp -r $archive $mtgarchive_trash_folder ; then
		    failed_trash=1
		fi
	    fi
	    [ $failed_trash -eq 0 ] && rm -rf $archive
	    [ $failed_trash -eq 0 ] && amt_to_remove=$(( $amt_to_remove - $size ))
	else
	    stop=1
	fi
    done < /tmp/mtgarc-list

    if [ $failed_trash -eq 1 ]; then
	echo "Unable to clean archives because couldn't copy them to $mtgarchive_trash_folder" | \
	    mail -s "Meeting archive size warning" ${sysadm_email[*]}
    fi

    if [ $mod_time -ge $youngest -a $amt_to_remove -gt 0 ]; then
	echo "Unable to clean ${amt_to_remove}k due to archives $mtgarchive_cleanup_min_age days old or less" | \
	    mail -s "Meeting archive size warning" ${sysadm_email[*]}
    fi
}

function make_archive_list
{
    # get file w/ lines of <archive> <access-time>
    stat --format="%n %X" $archives_path/*/* > /tmp/mtgarc-accessed

    # get file w/ lines of <archive> <size>
    du -s -k $archives_path/*/* | \
	gawk '{print $2,$1}' > /tmp/mtgarc-sizes

    # merge into file w/ lines of
    #     <archive> <access-time> <size>
    # sorted by <access-time>
    join /tmp/mtgarc-accessed /tmp/mtgarc-sizes | \
	sort -n +1 -2 \
    > /tmp/mtgarc-list
}

if [ ! -e $archives_path ]; then
    echo "Meeting archives location $archives_path does not exist" | \
	mail -s "Meeting archive does not exist" ${sysadm_email[*]}
    exit -1
fi

cur_sz=$( du -s --block-size=1M $archives_path | gawk '{print $1}' )
now=$( date +"%s" )
if [ $cur_sz -gt $mtgarchive_cleaning_threshold ]; then
    if [ "$mtgarchive_cleanup_action" == "clean" ]; then
	make_archive_list
	
	remove_sz=$(( $(($cur_sz - $mtgarchive_target_sz)) * 1024 ))
	minage=$(( $mtgarchive_cleanup_min_age * 86400 )) # seconds
	youngest_removeable=$(( $now - $minage ))
	
	remove_archives $remove_sz $youngest_removeable
    elif [ "$mtgarchive_cleanup_action" == "warn" ]; then
	echo "Meeting archives stored in $archives_path now total ${cur_sz}M" | \
	    mail -s "Meeting archive size warning" ${sysadm_email[*]}
    fi
fi
