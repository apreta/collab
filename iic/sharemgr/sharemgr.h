/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef SHAREMGR_H
#define SHAREMGR_H

/* Share Session Objects */

#include "../util/irefobject.h"
#include "istring.h"
#include "ithread.h"
#include "../services/common/sharecommon.h"

class Server;
class Session;
class Participant;
class Mode;
struct ServerStatus;
struct SessionStatus;

typedef IRefObjectPtr<Server> spServer;
typedef IRefObjectPtr<Session> spSession;
typedef IRefObjectPtr<Participant> spParticipant;


// Track active sessions and clients.
class SessionMgr : public IThread
{
protected:
    // List of share servers
    IList servers;

    // My server name
    IString server_name;
    int control_port;
	int share_port;
    IString default_callback;
	int idle_timeout;

    // Indices for sessions by session id and meeting id
    IHash sessions;
    IHash meetings;
	IHash clients;

    int session_id;

	Mode* modes[ShareModeMax];
	int start_events[ShareModeMax];
	int end_events[ShareModeMax];

    IMutex mgr_lock;

	bool shutdown_flag;

public:
    SessionMgr();

    int initialize();
    void shutdown();
	void reset();

    // External APIs
    int set_callback(const char * sessionid, const char * meetingid, const char * url);
    int allocate_session(const char * meetingid, int size, int mode, spSession &session);
    int end_session(const char * sessionid, const char * meetingid);
    int find_session(const char * meetingid, spSession &session);
    int enable_remote_control(const char * sessionid, const char * meetingid, const char * partid, bool enable);
    int change_presenter(const char * sessionid, const char * meetingid, const char * partid);
    int enable_recording(const char * sessionid, const char * meetingid, const char * partid, bool enable);
	int set_recording_audio(const char * sessionid, const char * meetingid, const char * url, const char * format);
	int set_recording_output(const char * sessionid, const char * meetingid, const char * url, const char * user, const char * pass);
	int get_server_status(ServerStatus list[], int & count);
	int get_server_details(const char * server, SessionStatus *&list, int & count);

    // Internal notification APIs
    void close_session(const char * meetingid, int mode, int reason);
    void participant_joined(const char * meetingid, const char * partid, int mode, bool host);
    void participant_left(const char * meetingid, const char * partid, int mode, int reason);
    void session_active(const char * meetingid, int mode);

	// Internal doc share APIs
	int register_client(const char * token, const char * meetingid, const char * partid, bool is_presenter,
					    const char * address, spSession & session, int share_mode);
	int remove_client(const char * address);

    // Housekeeping
    void add_server(const char * host, const char * reflector, const char * xmlrouter,
                    const char * web, const char * archiver, int max_users);
    spServer find_server(const char * name);

	int validate_session(const char * sessionid, const char * meetingid, spSession & session);
    spSession find_session(const char * sessionid);
    spSession find_session_by_meetingid(const char * meetingid);
    spSession find_session_by_token(const char * token, const char * meetingid);
	int check_session(const char * meetingid);

	spParticipant find_client_by_address(const char * address);
	int get_participants(Session * session, IList & list);

	Mode * get_mode(Session * session);
	Mode * get_mode(int mode);

    char * generate_id(char * buffer);

	// Dispatch meeting recording commands
	int archiver_reset(Server * server);
	int archiver_register(Session * session);
	int archiver_end(Session * session);
	int archiver_enable(Session * session, bool enable);
	int archiver_start(Session * session, int time);
	int archiver_stop(Session * session);
	int archiver_set_recording(Session * session);
	int archiver_set_recording_out(Session * session);

    const char * get_server_name()				{ return server_name; }
    void set_server_name(const char * name)		{ server_name = name; }

    void set_control_port(int port)             { control_port = port; }
    void set_share_port(int port)             	{ share_port = port; }
    int get_share_port()             			{ return share_port; }
	void set_idle_timeout(int timeout)			{ idle_timeout = timeout; }
	int get_idle_timeout()						{ return idle_timeout; }

    const char * get_default_callback()			{ return default_callback; }
    void set_default_callback(const char * url)	{ default_callback = url; }

	IMutex & get_lock()                         { return mgr_lock; }

	void run();
};


// Defines interface for command processors
class Mode
{
public:
	// Commands from session manager to mode.
    virtual int end_session(const char * token, const char * meetingid, int reason) { return Failed; }
    virtual int reject_participant(const char * sessionid, const char * meetingid, const char * partid, int reason) { return Failed; }
    virtual int enable_remote_control(const char * token, const char * meetingid, const char * partid, bool enable) { return Failed; }
	virtual int enable_recording(const char * sessionid, const char * meetingid, bool enable, int start) { return Failed; }

	// Notifications from session manager to mode.
    virtual void participant_joined(const char * meetingid, const char * partid) { }
    virtual void participant_left(const char * meetingid, const char * partid) { }
};


// Describes one Zon-lite server "stack".
class Server : public IReferencableObject
{
	IString name;

	IString reflector;
	IString xmlrouter;
	IString repository;
	IString archiver;

	long last_time;
	bool available;
	int max_users;
	int curr_users;    // Number of users assigned, may not be connected
	int curr_sessions; // Number of meetings assigned

	int user_count[ShareModeMax];

	IMutex ref_lock;

public:

	Server(const char *_name,
	       const char *_reflector,
	       const char *_xmlrouter,
	       const char *_repository,
	       const char *_archiver,
	       int _max_users);

    void lock();
    void unlock();

	int assign_session(spSession &session);
	int release_session(spSession &session);

	void add_user_count(int mode, int count);
	void remove_user_count(int mode, int count);

    const char * get_name()  	  { return name; }
    const char * get_reflector()  { return reflector; }
    const char * get_xmlrouter()  { return xmlrouter; }
    const char * get_repository() { return repository; }
    const char * get_archiver()   { return archiver; }
	bool get_available()		  { return available; }
	int get_curr_sessions()		  { return curr_sessions; }
	int get_curr_users()		  { return curr_users; }
	int get_max_users()			  { return max_users; }
	int get_capacity()			  { return max_users - curr_users; }
	int get_user_count(int mode)  { return user_count[mode]; }

    enum { TYPE_SERVER = 2234 };
    virtual int type()
        { return TYPE_SERVER; }
    virtual unsigned int hash()
        { return name.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && name == ((Server*)obj)->name; }
};


// Active meeting.
// No locking in these methods, session mgr should be locked when calling methods that update data.
class Session : public IReferencableObject
{
	IString sessionid;
	IString meetingid;
	long alloc_time;
	long idle_time;
	int alloc_size;
	int alloc_mode;
	int curr_size;
	IString callback_url;
    IString presenterid;

    IString token;

	// Server allocated for this meeting.
	spServer server;

	int state;
	int mode;
	int startup_mode;

	// Archiver allocated for meeting.
	IString archiver;
	bool is_recording;
	IString recording_url;
	IString recording_format;
	IString recording_out_url;
	IString recording_out_user;
	IString recording_out_password;

	// Clients connected to meeting.
	IList participants;

	// Doc share state
	IString docshare_url;

	IMutex ref_lock;

public:
	enum {
        StateNone,            // Session is idke
        StateActive,          // Session is active (at least host is present)
        StateChangePresenter, // Session is in process of presenter change
	    StateEnded };         // Session has been ended by API call


	Session(const char * id, const char * meetingid, int size, int mode);

    void lock();
    void unlock();

	int start_session(int mode);
	int end_session(bool final);
	int change_presenter();
	int add_participant_count();
	int remove_participant_count();
	int add_participant(const char * partid, bool is_presenter, const char * address, spParticipant& client);
	int remove_participant(spParticipant& client);
	spParticipant find_participant(const char * partid);
	int get_participants(IList& list);

	int get_mode()                         { return mode; }
    const char * get_sessionid()           { return sessionid; }
    const char * get_meetingid()           { return meetingid; }
    long get_alloc_time()				   { return alloc_time; }
    long get_idle_time()				   { return idle_time; }
    int get_alloc_size()				   { return alloc_size; }
	int get_curr_size()					   { return curr_size; }
	bool is_idle()						   { return state == StateNone; }
    bool is_ended()                        { return state == StateEnded; }
    bool is_active()                       { return state == StateActive; }
    bool is_change_presenter()             { return state == StateChangePresenter; }

	int get_startup_mode()                 { return startup_mode; }
	void set_startup_mode(int _mode)       { startup_mode = _mode; }

    const char * get_token()               { return token; }
    void set_token(const char * _token)    { token = _token; }

    const char * get_presenterid()         { return presenterid; }
    void set_presenterid(const char * _id) { presenterid = _id; }

    spServer get_server()                  { return server; }
	void set_server(Server *_server)       { server = _server; }

    const char * get_archiver()            { return archiver; }
	void set_archiver(const char * _str)   { archiver = _str; }

    const char * get_recording_url()       { return recording_url; }
	void set_recording_url(const char * _str) { recording_url = _str; }

    const char * get_recording_format()    { return recording_format; }
	void set_recording_format(const char * _str) { recording_format = _str; }

    const char * get_recording_out_url()   { return recording_out_url; }
	void set_recording_out_url(const char * _str) { recording_out_url = _str; }

    const char * get_recording_out_user()  { return recording_out_user; }
	void set_recording_out_user(const char * _str) { recording_out_user = _str; }

    const char * get_recording_out_password() { return recording_out_password; }
	void set_recording_out_password(const char * _str) { recording_out_password = _str; }

	bool get_recording()				   { return is_recording; }
	void set_recording(bool _recording)	   { is_recording = _recording; }

    const char * get_callback_url()        { return callback_url; }
    void set_callback_url(const char *cb)  { callback_url = cb; }

	const char * get_docshare_url()		   { return docshare_url; }
	void set_docshare_url(const char *u)   { docshare_url = u; }

    enum { TYPE_SESSION = 3735 };
    virtual int type()
        { return TYPE_SESSION; }
    virtual unsigned int hash()
        { return sessionid.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && sessionid == ((Session*)obj)->sessionid; }
};


// Session participant.
class Participant : public IReferencableObject
{
	spSession session;
	IString participantid;
	IString address;
	int client_type;

	IMutex ref_lock;

public:
	Participant(Session * session, const char * participantid, int type, const char * address);

	void lock();
    void unlock();

	spSession get_session()				{ return session; }
	const char * get_participantid()	{ return participantid; }
	const char * get_address()			{ return address; }
	int get_type()						{ return client_type; }
	int is_presenter()                  { return client_type == ShareRolePresenter; }

    enum { TYPE_PARTICIPANT = 9515 };
    virtual int type()
        { return TYPE_PARTICIPANT; }
    virtual unsigned int hash()
        { return address.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && address == ((Participant*)obj)->address; }
};


struct ServerStatus
{
	IString name;
	int user_count[ShareModeMax];

	ServerStatus()
	{
		memset(user_count, 0, sizeof(user_count));
	}
};


struct SessionStatus
{
	IString meetingid;
	int mode;
	int curr_size;

	SessionStatus()
	{
		mode = 0;
		curr_size = 0;
	}
};


#endif
