/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

/*
 * App/desktop share support
 *
 * A TCP connection to the reflector is opened at startup, and session
 * contol events are exchanged over this connection.
 *
 * If connection is not open, reflector denies incoming requests, otherwise
 * it signals us when it has accepted a connection, and we can reject it if
 * session token is not valid.
 *
 * We are also notified when participants leave or the session ends.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "../jcomponent/rpc.h"
#include "../jcomponent/util/eventlog.h"
#include "../jcomponent/util/log.h"
#include "../jcomponent/util/port.h"
#include "event.h"
#include "appshare.h"

AppShareMode::AppShareMode(SessionMgr& _mgr) :
    comm(this), mgr(_mgr)
{
}

// Connect to reflector.
int AppShareMode::initialize(const char * server, int port)
{
	int stat;
	CommClient::set_header(HEADER);
	stat = CommProcess::initialize();
	if (!stat)
		stat = comm.initialize(server, port, true);
	return stat;
}

int AppShareMode::shutdown()
{
	return CommProcess::shutdown();
}

// Send end session message to reflector.
int AppShareMode::end_session(const char * sessionid, const char * meetingid, int reason)
{
    log_debug(ZONE, "Ending meeting %s (%d)", meetingid, reason);
    CommMessage msg;
    msg.set_int(CmdEndSession);
    msg.set_string(meetingid);
    msg.set_int(reason);
    if (!msg.is_valid() || FAILED(comm.send_message(&msg, 0)))
    {
        log_debug(ZONE, "Could not end meeting %s", meetingid);
        return Failed;
    }
    return Ok;
}

// Send reject participant message to reflector.
int AppShareMode::reject_participant(const char * sessionid, const char * meetingid, const char * partid, int reason)
{
    log_debug(ZONE, "Dropping participant %s from %s (%d)", partid, meetingid, reason);
    CommMessage msg;
    msg.set_int(CmdRejectParticipant);
    msg.set_string(meetingid);
    msg.set_string(partid);
    msg.set_int(reason);
    if (!msg.is_valid() || FAILED(comm.send_message(&msg, 0)))
    {
        log_debug(ZONE, "Could not drop participant %s", partid);
        return Failed;
    }
    return Ok;
}

// Send enable remote control message to reflector.
int AppShareMode::enable_remote_control(const char * sessionid, const char * meetingid, const char * partid, bool enable)
{
    log_debug(ZONE, "Sending enable remote control to %s (%d)", meetingid, enable);
    CommMessage msg;
    msg.set_int(CmdEnableRemoteControl);
    msg.set_string(meetingid);
    msg.set_string(partid);
    msg.set_int(enable ? 1 : 0);
    if (!msg.is_valid() || FAILED(comm.send_message(&msg, 0)))
    {
        log_debug(ZONE, "Could not enable remote control %s", meetingid);
        return Failed;
    }
    return Ok;
}

// Send enable recording message to reflector.
int AppShareMode::enable_recording(const char * sessionid, const char * meetingid, bool enable, int start)
{
    log_debug(ZONE, "Sending app share enable recording (%d)", meetingid, enable);
    CommMessage msg;
    msg.set_int(CmdEnableRecording);
    msg.set_string(meetingid);
    msg.set_int(enable ? 1 : 0);
	msg.set_int(start);
    if (!msg.is_valid() || FAILED(comm.send_message(&msg, 0)))
    {
        log_debug(ZONE, "Could not enable recording %s", meetingid);
        return Failed;
    }
    return Ok;
}

// Notification from session manager that participant has joined.
void AppShareMode::participant_joined(const char * meetingid, const char * partid)
{
	// no-op in app share mode
}

// Notification from session manager that participant has left.
void AppShareMode::participant_left(const char * meetingid, const char * partid)
{
	// no-op in app share mode
}

// Handle reflector message that participant has joined.
void AppShareMode::on_participant_joined(const char * token, const char * meetingid, const char * partid, bool host)
{
	IUseMutex lock(mgr.get_lock());

	log_debug(ZONE, "AppShare participant %s joined %s", partid, meetingid);

    spSession session = mgr.find_session_by_token(token, meetingid);

	// Invalid session, reject this connection.
	if (session == NULL)
	{
        reject_participant(token, meetingid, partid, ReasonInvalidSession);
        return;
	}

	if (session->is_active())
	{
		// Allow presenter for doc share mode to connect for recording
		// (and if so, don't dispatch events)
		if (session->get_mode() == ShareModeDocument)
		{
			// Check that participant is presenter (should already be
			// registered from register_client call)
			spParticipant part = session->find_participant(partid);
			if (part && part->get_type() != ShareRolePresenter)
				reject_participant(token, meetingid, partid, ReasonInvalidSession);
			return;
		}
	}

	spParticipant part;
	session->add_participant(partid, host, NULL, part);

	int mode = session->get_startup_mode() == ShareModePPT ? ShareModePPT : ShareModeApplication;

	mgr.participant_joined(meetingid, partid, mode, host);
}

void AppShareMode::on_participant_left(const char * meetingid, const char * partid, int reason)
{
	log_debug(ZONE, "AppShare participant %s left %s", partid, meetingid);

    spSession session = mgr.find_session_by_meetingid(meetingid);
	if (session == NULL)
		return;

	// App share is used for recording in document mode.  Ignore these events.
	if (session->get_mode() == ShareModeDocument)
		return;

	spParticipant part = session->find_participant(partid);
	if (part != NULL)
		session->remove_participant(part);

	mgr.participant_left(meetingid, partid, session->get_mode(), reason);
}

void AppShareMode::on_end_session(const char * meetingid, int reason)
{
	IUseMutex lock(mgr.get_lock());

	log_debug(ZONE, "AppShare ended %s", meetingid);

    spSession session = mgr.find_session_by_meetingid(meetingid);
	if (session == NULL)
		return;

	// App share is used for recording in document mode.  Ignore these events.
	if (session->get_mode() == ShareModeDocument)
		return;

    mgr.close_session(meetingid, ShareModeApplication, reason);
}

// Handle control message from reflector.
bool AppShareMode::on_message(CommConnection * c, CommMessage * msg)
{
    const char *meetingid;
    const char *token;
    const char *partid;

    int cmd = msg->get_int();
    switch (cmd)
    {
        case CmdNoop:
        {
            break;
        }
        case CmdEndSession:
        {
            IString meetingid = msg->get_string();
            int reason = msg->get_int();
            if (!msg->is_valid())
                return false;
			on_end_session(meetingid, reason);
            break;
        }
        case CmdParticipantJoined:
        {
            IString token = msg->get_string();
            IString meetingid = msg->get_string();
            IString partid = msg->get_string();
            int host = msg->get_int();
            if (!msg->is_valid())
                return false;
            on_participant_joined(token, meetingid, partid, host != 0);
            break;
        }
        case CmdParticipantLeft:
        {
            IString meetingid = msg->get_string();
            IString partid = msg->get_string();
            int reason = msg->get_int();
            if (!msg->is_valid())
                return false;
            on_participant_left(meetingid, partid, reason);
            break;
        }
		case CmdSessionActive:
		{
			IString meetingid = msg->get_string();
			if (!msg->is_valid())
				return false;
			mgr.session_active(meetingid, ShareModeApplication);
			break;
		}
        default:
            return false;
    }

    return true;
}
