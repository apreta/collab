/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef _WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#ifdef LINUX
#include <time.h>
#include <sys/time.h>
#endif
#include "timer.h"
#include "../jcomponent/util/log.h"

#ifdef LINUX
IThread *CTimer::timer_thread = NULL;
int CTimer::interval = 0;
IList CTimer::running;
IMutex CTimer::timer_mutex;
#endif

//
// Timer for channel.
//

CTimer::CTimer(ITimeable * _timeable, int _id)
{
    timeable = _timeable;
    timer_id = _id;
    expires_at = -1;
    is_active = false;

#ifdef _WIN32
    // Callbacks will come on same thread only when thread is in alertable
    // state...which is just what we want here.
    timer = CreateWaitableTimer(NULL, FALSE, NULL);
#endif
}

CTimer::~CTimer()
{
#ifdef _WIN32
    if (timer != NULL)
        CloseHandle(timer);
#else
    cancel();
#endif
}


void CTimer::set_timeout(int _type, int ms, int interval)
{
#ifdef LINUX
    log_debug(ZONE, "Setting timer to %d", ms);

	IUseMutex lock(timer_mutex);
	expires_at = get_time() + ms;
    timer_type = _type;
	repeat_interval = interval;
    if (!is_active)
    {
        running.add(this);
        is_active = true;
    }
    set_timer(TIMER_INTERVAL);

#else // _WIN32

    LARGE_INTEGER pause; 
    pause.QuadPart = - (ms * 10000i64);	// 100 ns units
    log_debug(ZONE, "Setting timer to %i64d", pause);
    
    timer_type = _type;

    if (!SetWaitableTimer(timer, &pause, 0, timer_proc, this, FALSE))
    {
        printf("failed");
    }
#endif
}

void CTimer::cancel()
{
#ifdef LINUX
	IUseMutex lock(timer_mutex);
    expires_at = -1;
    running.remove(this, false);
    is_active = false;
    if (running.size() == 0)
        set_timer(0);
#else
    CancelWaitableTimer(timer);
#endif
}


int lcount = 0;

#ifdef LINUX

// Convert time into number of millis since....let's see...how about today?
int CTimer::get_time()
{
	struct timeval tm;
	struct timezone tz;

	gettimeofday(&tm, &tz);
	return (tm.tv_sec - 1081963000l) * 1000 + (tm.tv_usec / 1000);
}


/* Periodically check all timers for expiration */
void * CTimer::handler(void *p)
{
	IUseMutex lock(timer_mutex);
	
	while (true)
	{
		if (interval > 0)
		{
			int now = get_time();
			
			static int count = 10;
			if (!--count)
			{
				log_debug(ZONE, "Checking timers at %d", now);
				count = 10;
			}
	
			IListIterator iter(running);
			CTimer * timer = (CTimer*)iter.get_first();
	
			while (timer != NULL)
			{
				CTimer * cur = timer;
				timer = (CTimer*) iter.get_next();
	
				if (now > cur->expires_at)
				{
					if (cur->repeat_interval > 0)
						cur->expires_at = get_time() + cur->repeat_interval;
					else
						cur->cancel();

					lock.unlock();
					cur->timeable->on_timeout(cur->timer_type);
					lock.lock();
					
					// Must restart iteration as list may have changed
					timer = (CTimer*)iter.get_first();
				}
			}
		}

		lock.unlock();
		struct timespec tv = { interval / 1000, interval % 1000 * 1000000 };
		nanosleep(&tv, NULL);
		lock.lock();
	}
}

/* caller should own timer  mutex */
void CTimer::set_timer(int interval_ms)
{
    if (interval_ms)
	{
        log_debug(ZONE, "Enabling timer");
	}
    else
	{
        log_debug(ZONE, "Disabling timer");
	}

	interval = interval_ms;

	/* start thread on first use */
	if (interval_ms && timer_thread == NULL)
		timer_thread = new IThread(handler, NULL, false);
}

#else //_WIN32

// Simpler in win32 as we've scheduled a timer to expire just when we need it.
// We shouldn't have to worry about block/unblocking signals as we'll only
// get callback when in alertable state, like SleepEx.

void _stdcall CTimer::timer_proc(void *arg, DWORD high_time, DWORD low_time)
{
    CTimer * cur = (CTimer*)arg;
    cur->timeable->on_timeout(cur->timer_type);
}

#endif

