/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include <stdio.h>
#include <time.h>

#include "../jcomponent/rpc.h"
#include "../jcomponent/util/eventlog.h"
#include "../jcomponent/util/log.h"
#include "../jcomponent/util/port.h"
#include "sharemgrrpc.h"
#include "sharemgr.h"
#include "appshare.h"
#include "docshare.h"
#include "event.h"

/* Implementation notes

   Resources are reserved as soon as alloc_share is called.

   If we are restarted, we no longer are aware of sessions in progress, but
   we will not reset the reflector state.  However, if reflector is restarted,
   we will end any sessions that are in progress.

   We interact with the reflector over a TCP connection.
   We interact with doc share clients via Jabber server.
   The share manager provides doc share APIs and event dispatch to connected
   clients.
*/

extern SessionMgr mgr;
extern EventLog event_log;
extern IMutex event_mutex;

EventMgr dispatch;
Mode no_mode;
AppShareMode app_share_mode(mgr);
extern DocShareMode doc_share_mode;

static int sharemgr_epoch = time(NULL);

/*
 * Session Manager
 */

SessionMgr::SessionMgr()
{
	session_id = 1000;

	// Set up info for various modes.
	for (int i = 0; i < ShareModeMax; i++)
		modes[i] = &no_mode;
	modes[ShareModeApplication] = &app_share_mode;
	modes[ShareModeDocument] = &doc_share_mode;
	modes[ShareModePPT] = &app_share_mode;

	start_events[ShareModeApplication] = EventAppShareStarted;
	start_events[ShareModeDocument] = EventDocShareStarted;
	start_events[ShareModePPT] = EventPPTShareStarted;

	end_events[ShareModeApplication] = EventAppShareEnded;
	end_events[ShareModeDocument] = EventDocShareEnded;
	end_events[ShareModePPT] = EventPPTShareEnded;

	shutdown_flag = false;
}

// Read configuration.
int SessionMgr::initialize()
{
    int stat = app_share_mode.initialize(server_name, control_port);
    dispatch.initialize(default_callback);
	this->start();
	return stat;
}

void SessionMgr::shutdown()
{
	shutdown_flag = true;
    app_share_mode.shutdown();
}

void SessionMgr::reset()
{
	Server * server = find_server(server_name);
	if (server != NULL)
	{
		archiver_reset(server);
	}
	else
		log_error(ZONE, "Server %s is not configured correctly", server_name.get_string());
}

int SessionMgr::set_callback(const char * sessionid, const char * meetingid, const char * url)
{
    IUseMutex lock(mgr_lock);

    spSession session = find_session(sessionid);
    if (session == NULL)
        return InvalidSession;

    if (strcmp(meetingid, session->get_meetingid()) != 0)
        return SessionMeetingMismatch;

    if (strcmp(session->get_server()->get_name(), server_name) != 0)
        return WrongServer;

    session->set_callback_url(url);

    log_debug(ZONE, "Set event callback for session %s to %s", sessionid, url);

    return Ok;
}

// Allocate new share session on least loaded server.
int SessionMgr::allocate_session(const char * meetingid, int size, int mode, spSession &session)
{
    char buff[32];
    spServer curr, best;

    IUseMutex lock(mgr_lock);

    // Doesn't guarantee that reflector command will succeed, but it's a start.
    // *** if (!comm.is_connected())
    //    return NoReflector;

	log_debug(ZONE, "Allocating session for meeting %s (size = %d)", meetingid, size);

    // Make sure we aren't already handling a session for this meeting.
    session = find_session_by_meetingid(meetingid);
    if (session != NULL)
        return MeetingExists;

    // Find share server with largest remaining capacity.
    // Also finds recording server with lowest number of sessions.
    IListIterator iter(servers);
    curr = (Server *)iter.get_first();
    while (curr != NULL)
    {
        if (best == NULL && curr->get_available())
        {
            best = curr;
        }
        else
        {
            if (curr->get_available() && curr->get_capacity() < best->get_capacity())
                best = curr;
        }

        curr = (Server *)iter.get_next();
    }

    // Make sure there is enough capacity for this meeting.
    if (best == NULL || best->get_capacity() < size)
    {
		log_debug(ZONE, "No share server resources available (%d)", best ? best->get_capacity() : -1);
		return NoShareSession;
    }

    // Create new session.
    session = new Session(generate_id(buff), meetingid, size, mode);

    // Assign session to servers.
    if (FAILED(best->assign_session(session)))
    {
        session->dereference();
        return NoShareSession;
    }

    // Inform meeting archiver about new meeting
    archiver_register(session);

    // Add share session to indices.
    IString * id = new IString(session->get_sessionid());
    sessions.insert(id, session);

    IString * meeting = new IString(meetingid);
    meetings.insert(meeting, session);

	log_debug(ZONE, "Allocated session %s", session->get_sessionid());

    session->dereference();
    return Ok;
}

// End share session.
int SessionMgr::end_session(const char * sessionid, const char * meetingid)
{
    IUseMutex lock(mgr_lock);

    spSession session = find_session(sessionid);
    if (session == NULL)
        return InvalidSession;

    if (strcmp(meetingid, session->get_meetingid()) != 0)
        return SessionMeetingMismatch;

    if (strcmp(session->get_server()->get_name(), server_name) != 0)
        return WrongServer;

	log_debug(ZONE, "Ending session %s", sessionid);

	int mode = session->get_mode();

    int stat = session->end_session(true);

    if (!FAILED(stat))
    {
		if (mode != ShareModeNone)
		{
        	stat = get_mode(mode)->end_session(sessionid, meetingid, ReasonNormal);
		}

		// If doc share mode, also inform reflector as presenter has connected for recording purposes.
		if (mode == ShareModeDocument)
		{
			get_mode(ShareModeApplication)->end_session(sessionid, meetingid, ReasonNormal);
		}

		// Inform archiver.
		archiver_end(session);

		// If session was not active, or we couldn't end it, release it now.
		// Otherwise we will get notified after all participants leave.
        if (mode == ShareModeNone || FAILED(stat))
        {
            session->get_server()->release_session(session);

            // Remove session from indices.
            IString key(sessionid);
            sessions.remove(&key, true);

            key = meetingid;
            meetings.remove(&key, true);

            log_debug(ZONE, "Removed meeting %s", meetingid);
        }
    }

    return stat;
}

// Find session and make sure the supplied meeting id matches.
int SessionMgr::validate_session(const char * sessionid, const char * meetingid, spSession & session)
{
    session = find_session(sessionid);
    if (session == NULL)
        return InvalidSession;

    if (strcmp(meetingid, session->get_meetingid()) != 0)
        return SessionMeetingMismatch;

    if (strcmp(session->get_server()->get_name(), server_name) != 0)
        return WrongServer;

	return Ok;
}

// Find session by meeting id.
int SessionMgr::find_session(const char * meetingid, spSession & session)
{
    IUseMutex lock(mgr_lock);

    // Make sure we aren't already handling a session for this meeting.
    session = find_session_by_meetingid(meetingid);
    if (session != NULL)
        return Ok;

    return InvalidMeetingId;
}

// Enable/disable remote control for a participant
int SessionMgr::enable_remote_control(const char * sessionid, const char * meetingid, const char * partid, bool enable)
{
    IUseMutex lock(mgr_lock);

    spSession session = find_session(sessionid);
    if (session == NULL)
        return InvalidSession;

    if (strcmp(meetingid, session->get_meetingid()) != 0)
        return SessionMeetingMismatch;

    if (strcmp(session->get_server()->get_name(), server_name) != 0)
        return WrongServer;

    int stat = get_mode(session)->enable_remote_control(sessionid, meetingid, partid, enable);

    if (!FAILED(stat))
    {
        // Send event
        dispatch.add_event(EventRemoveControlChanged, session, partid, enable);
    }

    return stat;
}

// Change presenter (really just end current share sessions so they can be restarted with new presenter)
int SessionMgr::change_presenter(const char * sessionid, const char * meetingid, const char * partid)
{
    IUseMutex lock(mgr_lock);

    spSession session = find_session(sessionid);
    if (session == NULL)
        return InvalidSession;

    if (strcmp(meetingid, session->get_meetingid()) != 0)
        return SessionMeetingMismatch;

    if (strcmp(session->get_server()->get_name(), server_name) != 0)
        return WrongServer;

    int stat = session->change_presenter();
    if (!FAILED(stat))
    {
        stat = get_mode(session)->end_session(sessionid, meetingid, ReasonChangePresenter);

        if (!FAILED(stat))
        {
            session->set_presenterid(partid);

            // Event will be sent after service closes session.
        }
    }

    return stat;
}

// Enable/disable recording for a meeting
int SessionMgr::enable_recording(const char * sessionid, const char * meetingid, const char * partid, bool enable)
{
	int stat;
	int mode;

    IUseMutex lock(mgr_lock);

    spSession session = find_session(sessionid);
    if (session == NULL)
        return InvalidSession;

    if (strcmp(meetingid, session->get_meetingid()) != 0)
        return SessionMeetingMismatch;

    if (strcmp(session->get_server()->get_name(), server_name) != 0)
        return WrongServer;

    session->set_recording(enable);
	mode = session->get_mode();

    // Notify recorder.
    stat = archiver_enable(session, enable);

    // If session is already active (host connected), start recording now.
    if (!FAILED(stat) && session->is_active())
    {
        int start_time = time(NULL);
        log_debug(ZONE, "Session %s starting recording at %d", session->get_sessionid(), start_time);

        stat = get_mode(session)->enable_recording(sessionid, meetingid, enable, start_time);
        if (!FAILED(stat))
        {
            archiver_start(session, start_time);
        }

		// if session is doc share, still need to send message to reflector from here
		// because client enable message is blocked in sharelite
		if (mode == ShareModeDocument)
		{
			stat = get_mode(ShareModeApplication)->enable_recording(sessionid, meetingid, enable, start_time);
		}
    }

    if (!FAILED(stat))
    {
        // Send event
        dispatch.add_event(EventRecordingChanged, session, partid, enable);
    }

    return stat;
}

// Set location to fetch meeting audio from.
int SessionMgr::set_recording_audio(const char * sessionid, const char * meetingid, const char * url, const char * format)
{
	int stat = Ok;

    IUseMutex lock(mgr_lock);

    spSession session = find_session(sessionid);
    if (session == NULL)
        return InvalidSession;

    if (strcmp(meetingid, session->get_meetingid()) != 0)
        return SessionMeetingMismatch;

    if (strcmp(session->get_server()->get_name(), server_name) != 0)
        return WrongServer;

	log_debug(ZONE, "Setting recording url for meeting %s to %s", meetingid, url);

	session->set_recording_url(url);
	session->set_recording_format(format);
	stat = archiver_set_recording(session);

	return stat;
}

// Set location to send meeting archive to.
int SessionMgr::set_recording_output(const char * sessionid, const char * meetingid, const char * url, const char * user, const char * pass)
{
	int stat = Ok;

    IUseMutex lock(mgr_lock);

    spSession session = find_session(sessionid);
    if (session == NULL)
        return InvalidSession;

    if (strcmp(meetingid, session->get_meetingid()) != 0)
        return SessionMeetingMismatch;

    if (strcmp(session->get_server()->get_name(), server_name) != 0)
        return WrongServer;

	log_debug(ZONE, "Setting recording output for meeting %s to %s", meetingid, url);

	session->set_recording_out_url(url);
	session->set_recording_out_user(user);
	session->set_recording_out_password(pass);
	stat = archiver_set_recording_out(session);

	return stat;
}

int SessionMgr::get_server_status(ServerStatus list[], int & count)
{
	int pos = 0;

    IUseMutex lock(mgr_lock);

	IListIterator iter(servers);
	Server * server = (Server*) iter.get_first();

	while (server != NULL && pos < MAX_SHARE_SERVERS)
	{
		list[pos].name = server->get_name();
		for (int i = ShareModeMin; i < ShareModeMax; i++)
			list[pos].user_count[i] = server->get_user_count(i);

		++pos;
		server = (Server*) iter.get_next();
	}

	count = pos;
	return Ok;
}

struct SessionDetailsPos
{
	const char * server_name;
	SessionStatus * list;
	int current;
	int max;
};

void session_details_enum(IObject * key, IObject * value, void * user)
{
	SessionDetailsPos * pos = (SessionDetailsPos*) user;
	Session * session = (Session*) value;

	if (strcmp(pos->server_name, session->get_server()->get_name()) == 0)
	{
		if (pos->current < pos->max)
		{
			pos->list[pos->current].meetingid = session->get_meetingid();
			pos->list[pos->current].mode = session->get_mode();
			pos->list[pos->current].curr_size = session->get_curr_size();
			++pos->current;
		}
	}
}

int SessionMgr::get_server_details(const char * name, SessionStatus *&list, int & count)
{
	int pos = 0;

    IUseMutex lock(mgr_lock);

	spServer server = find_server(name);
    if (server == NULL)
		return InvalidServerName;

	count = server->get_curr_sessions();
	if (count > 0)
	{
		list = new SessionStatus[count];
		SessionDetailsPos pos = { name, list, 0, count };

		sessions.for_each(session_details_enum, &pos);
		count = pos.max;
	}

	return Ok;
}

// Service has closed a session (host left).
void SessionMgr::close_session(const char * meetingid, int mode, int reason)
{
    IUseMutex lock(mgr_lock);
    int event;

    spSession session = find_session_by_meetingid(meetingid);
    if (session == NULL)
        return;

    log_debug(ZONE, "Session %s closed by service", session->get_sessionid());

    // If closed because end_session API was called, remove session now.
    if (session->is_ended())
    {
        archiver_end(session);

        session->get_server()->release_session(session);

        // Remove session from indices.
        IString key(session->get_sessionid());
        sessions.remove(&key, true);

        key = meetingid;
        meetings.remove(&key, true);

        log_debug(ZONE, "Removed meeting %s", meetingid);

        event = end_events[mode];
    }
    // Otherwise send appropriate event, but session remains valid.
    else if (session->is_change_presenter())
    {
        session->end_session(false);
        event = EventPresenterChanged;

        if (session->get_recording())
            archiver_stop(session);
    }
    else
    {
        session->end_session(false);
        event = end_events[mode];

        if (session->get_recording())
            archiver_stop(session);
    }

    // Send event
    dispatch.add_event(event, session, session->get_presenterid(), 0);
}

// Participant has joined meeting.
void SessionMgr::participant_joined(const char * meetingid, const char * partid, int mode, bool host)
{
    IUseMutex lock(mgr_lock);

    spSession session = find_session_by_meetingid(meetingid);
    if (session == NULL)
        return;

    // Mark session started.
    if (!session->is_active())
    {
        log_debug(ZONE, "Session %s started", session->get_sessionid());
        session->start_session(mode);

        // Send event
		int event = start_events[mode];
        dispatch.add_event(event, session, partid, 0);
	}
	else if (mode != session->get_mode())
	{
		log_debug(ZONE, "Wrong participant mode for session %s", session->get_sessionid());
        get_mode(mode)->reject_participant(session->get_sessionid(), meetingid, partid, ReasonWrongShareMode);
        return;
	}

	session->add_participant_count();

    log_debug(ZONE, "Participant %s joined session %s", partid, session->get_sessionid());

	get_mode(mode)->participant_joined(meetingid, partid);

    // Send event
    dispatch.add_event(EventParticipantJoined, session, partid, 0);
}

// Participant has left meeting.
void SessionMgr::participant_left(const char * meetingid, const char * partid, int mode, int reason)
{
    IUseMutex lock(mgr_lock);

    spSession session = find_session_by_meetingid(meetingid);
    if (session == NULL)
        return;

	session->remove_participant_count();

    log_debug(ZONE, "Participant %s disconnected from session %s", partid, session->get_sessionid());

	// When host leaves, service should send end session message also.
	// Doc share server will notify other clients as needed.
	get_mode(mode)->participant_left(meetingid, partid);

    // Send event
    dispatch.add_event(EventParticipantLeft, session, partid, 0);
}

// Service has an active host connection for this meeting.
void SessionMgr::session_active(const char * meetingid, int mode)
{
    IUseMutex lock(mgr_lock);
    int stat;

    spSession session = find_session_by_meetingid(meetingid);
    if (session == NULL)
        return;

    log_debug(ZONE, "Session %s activated by service", session->get_sessionid());

	// Start recording if enabled.
	if (session->get_recording())
	{
		int start_time = time(NULL);
		log_debug(ZONE, "Session %s starting recording at %d", session->get_sessionid(), start_time);

		stat = get_mode(session)->enable_recording(session->get_sessionid(), meetingid, true, start_time);
		if (!FAILED(stat))
		{
			archiver_start(session, start_time);
		}
	}
}

// Register client (used for doc share clients connected through jabber)
// called also in ppt share mode for passing control commands around
int SessionMgr::register_client(const char * token, const char * meetingid, const char * partid, bool host,
                                const char * address, spSession & session, int share_mode)
{
    int stat;

    IUseMutex lock(mgr_lock);

    session = find_session_by_token(token, meetingid);
    if (session == NULL)
        return InvalidSession;

    if (strcmp(session->get_server()->get_name(), server_name) != 0)
        return WrongServer;

	// If session is already active in another mode, can't start doc share.
	if (session->is_active() && session->get_mode() != share_mode)
		return WrongShareMode;

	// Bit of a hack, but we have to keep track of desired share mode so
	// when client connects to reflector we remember it's ppt mode.
	session->set_startup_mode(share_mode);

	// Add client to session.
	spParticipant client;
	stat = session->add_participant(partid, host, address, client);
	if (stat)
		return stat;

	// Also add index from client address to client.
	IString *key = new IString(address);
	clients.insert(key, client);

	// Notify that participant has joined meeting
	if (share_mode == ShareModeDocument)
		participant_joined(meetingid, partid, share_mode, host);

	return stat;
}

// Handle disconnected jabber client.
int SessionMgr::remove_client(const char * address)
{
	int stat;

	IString key(address);

    IUseMutex lock(mgr_lock);

	spParticipant client = (Participant*)clients.get(&key);
	if (client == NULL)
		return InvalidClient;

	spSession session = client->get_session();

	// Notify that participant has left this meeting
	if (session->get_mode() == ShareModeDocument)
		participant_left(session->get_meetingid(), client->get_participantid(), ShareModeDocument, ReasonDisconnected);

	stat = client->get_session()->remove_participant(client);
	clients.remove(&key, true);

	return stat;
}

// Add configured share server.
void SessionMgr::add_server(const char * name, const char * reflector,
                            const char * xmlrouter, const char * repository,
                            const char * archiver, int max_users)
{
    IUseMutex lock(mgr_lock);

    spServer server = find_server(name);
    if (server == NULL)
    {
        server = new Server(name, reflector, xmlrouter, repository, archiver, max_users);
        servers.add(server);
    }
}

// Find server.
spServer SessionMgr::find_server(const char * name)
{
    spServer server;
    IListIterator iter(servers);

    // Find server with largest remaining capacity.
    server = (Server *)iter.get_first();
    while (server != NULL)
    {
        if (strcmp(name, server->get_name()) == 0)
            return server;
        server = (Server *)iter.get_next();
    }

    return NULL;
}

// Return mode processor for specified session
Mode * SessionMgr::get_mode(Session * session)
{
	int mode = session->get_mode();
	if (mode >= ShareModeMax || mode <= 0)
		return &no_mode;
	return modes[mode];
}

// Return mode processor for specified mode
Mode * SessionMgr::get_mode(int mode)
{
	if (mode >= ShareModeMax || mode <= 0)
		return &no_mode;
	return modes[mode];
}

// Create session id.
char * SessionMgr::generate_id(char * buff)
{
    sprintf(buff, "%d", session_id++);
    return buff;
}

// Register session with meeting recorder.
int SessionMgr::archiver_reset(Server * server)
{
	const char * secret = "zQQz98";
	const char * archiver = server->get_archiver();

	if (archiver == NULL || *archiver == '\0')
		return Ok;

	log_error(ZONE, "Resetting recorder service");

	if (rpc_make_call("arc_reset", archiver,
				  "mtgarchiver.reset", "(si)",
				  secret,
				  sharemgr_epoch))
	{
		log_error(ZONE, "Error resetting recorder service");
	}

	return Ok;
}

// Register session with meeting recorder.
int SessionMgr::archiver_register(Session * session)
{
	char id[64];
	char address[128];
	const char * archiver = session->get_archiver();
	if (archiver == NULL || *archiver == '\0')
		return Ok;

	sprintf(id, "arc_reg:%s", session->get_sessionid());
	sprintf(address, "%s:%d", session->get_server()->get_reflector(), share_port);

	if (rpc_make_call(id, archiver,
				  "mtgarchiver.register_meeting", "(sissssi)",
				  session->get_meetingid(),
				  session->get_alloc_time(),
				  "",
				  "",
				  address, 	// share server
				  "imidio",	//password
				  0))		//share options
	{
		log_error(ZONE, "Error notifying recorder service about meeting");
	}

	return Ok;
}

// Notify recorder that session is ended.
int SessionMgr::archiver_end(Session * session)
{
	char id[64];
	const char * archiver = session->get_archiver();
	if (archiver == NULL || *archiver == '\0')
		return Ok;

	sprintf(id, "arc_end:%s", session->get_sessionid());

	if (rpc_make_call(id, archiver,
		"mtgarchiver.stop_recording", "(ssi)", session->get_meetingid(), "", 0))
	{
		log_error(ZONE, "Error notifying recorder service about meeting end");
	}

	return Ok;
}

// Notify recorder that this session will be recorded.
int SessionMgr::archiver_enable(Session * session, bool enable)
{
	char id[64];
	const char * archiver = session->get_archiver();
	if (archiver == NULL || *archiver == '\0')
		return Ok;

	sprintf(id, "arc_enable:%s", session->get_sessionid());

	if (rpc_make_call(id, archiver,
		              "mtgarchiver.enable_recording", "(si)",
	                   session->get_meetingid(), enable ? 1 : 0))
	{
		return Failed;
	}

	return Ok;
}

// Notify recorder that we've started recording.
int SessionMgr::archiver_start(Session * session, int starttime)
{
	char id[64];
	const char * archiver = session->get_archiver();
	if (archiver == NULL || *archiver == '\0')
		return Ok;

	sprintf(id, "arc_start:%s", session->get_sessionid());

	if (rpc_make_call(id, archiver,
					  "mtgarchiver.toggle_recording", "(si)",
					  session->get_meetingid(), starttime))
	{
		return Failed;
	}

	return Ok;
}

// Notify recorder that we've stopped recording.
int SessionMgr::archiver_stop(Session * session)
{
	char id[64];
	const char * archiver = session->get_archiver();
	if (archiver == NULL || *archiver == '\0')
		return Ok;

	sprintf(id, "arc_stop:%s", session->get_sessionid());

	if (rpc_make_call(id, archiver,
					  "mtgarchiver.pause_recording", "(s)",
					  session->get_meetingid()))
	{
		return Failed;
	}

	return Ok;
}

// Notify recorder about audio file location.
int SessionMgr::archiver_set_recording(Session * session)
{
	char id[64];
	const char * archiver = session->get_archiver();
	if (archiver == NULL || *archiver == '\0')
		return Ok;

	sprintf(id, "arc_set:%s", session->get_sessionid());

	if (rpc_make_call(id, archiver,
					  "mtgarchiver.set_recording_audio", "(sss)",
					  session->get_meetingid(),
					  session->get_recording_url(),
					  session->get_recording_format()))
	{
		return Failed;
	}

	return Ok;
}

// Notify recorder about archive output location.
int SessionMgr::archiver_set_recording_out(Session * session)
{
	char id[64];
	const char * archiver = session->get_archiver();
	if (archiver == NULL || *archiver == '\0')
		return Ok;

	sprintf(id, "arc_set:%s", session->get_sessionid());

	if (rpc_make_call(id, archiver,
					  "mtgarchiver.set_recording_output", "(ssss)",
					  session->get_meetingid(),
					  session->get_recording_out_url(),
					  session->get_recording_out_user(),
					  session->get_recording_out_password()))
	{
		return Failed;
	}

	return Ok;
}

// Find session by session id.
spSession SessionMgr::find_session(const char * sessionid)
{
    IString key(sessionid);

    spSession session = (Session*)sessions.get(&key);
    return session;
}

// Find session by meeting id.
spSession SessionMgr::find_session_by_meetingid(const char * meetingid)
{
    IString key(meetingid);

    spSession session = (Session*)meetings.get(&key);
    return session;
}

// Find session by token (validates token first).
spSession SessionMgr::find_session_by_token(const char * token, const char * meetingid)
{
    char sessionid[256];

    if (!FAILED(parse_session_token(token, sessionid)))
    {
        log_debug(ZONE, "Checking token %s for meeting %s", sessionid, meetingid);

        IUseMutex lock(mgr_lock);

        spSession session = find_session(sessionid);
        if (session != NULL &&
            strcmp(meetingid, session->get_meetingid()) == 0 &&
            strcmp(session->get_server()->get_name(), server_name) == 0)
        {
            return session;
        }
    }

    log_debug(ZONE, "Session token %s is not valid", token);
    return NULL;
}

int SessionMgr::check_session(const char * meetingid)
{
    IUseMutex lock(mgr_lock);

    spSession session = find_session_by_meetingid(meetingid);
    if (session == NULL)
	{
	    log_debug(ZONE, "Meeting %s is not active, ending recording", meetingid);

		if (rpc_make_call("stoprecording",
							  "mtgarchiver", "mtgarchiver.stop_recording", "(ssi)", meetingid, "Not available", FALSE))  // no way to recover participants?
		{
			return Failed;
		}
	}

	return Ok;
}

// Find client by address it registered with.
spParticipant SessionMgr::find_client_by_address(const char * address)
{
    IUseMutex lock(mgr_lock);
	IString key(address);
	return (Participant*)clients.get(&key);
}

int SessionMgr::get_participants(Session * session, IList & list)
{
    IUseMutex lock(mgr_lock);
	return session->get_participants(list);
}

struct SessionIdlePos
{
	const char * server_name;
	int expire_time;
	IList expired;
};

void session_idle_enum(IObject * key, IObject * value, void * user)
{
	SessionIdlePos * pos = (SessionIdlePos*) user;
	Session * session = (Session*) value;

	if (strcmp(pos->server_name, session->get_server()->get_name()) == 0)
	{
		if (session->get_idle_time() != 0 && pos->expire_time > session->get_idle_time())
		{
			log_debug(ZONE, "Closing inactive session %s", session->get_sessionid());
			pos->expired.add(session);
		}
	}
}

void SessionMgr::run()
{
	int counter = 0;
	while (!shutdown_flag)
    {
		sleep(2);
		if (++counter % 30 == 0)
		{
			SessionIdlePos pos;
			pos.server_name = server_name;
			pos.expire_time = time(NULL) - idle_timeout;

			// Enumerate sessions and add expired to list
			IUseMutex lock(mgr_lock);
			sessions.for_each(session_idle_enum, &pos);

			// End sessions in list
			IListIterator iter(pos.expired);
			Session * session = (Session *)iter.get_first();
			while (session != NULL)
			{
				Session * to_delete = session;
				session = (Session *)iter.get_next();

				end_session(to_delete->get_sessionid(), to_delete->get_meetingid());
			}
		}
	}
}

/*
 * Server
 */

Server::Server(const char *_name,
	           const char *_reflector,
	           const char *_xmlrouter,
	           const char *_repository,
	           const char *_archiver,
	           int _max_users) :
    name(_name), reflector(_reflector), xmlrouter(_xmlrouter), repository(_repository),
    archiver(_archiver)
{
    last_time = 0;
    available = true;	// *** fix ***
    max_users = _max_users;
    curr_users = 0;
	curr_sessions = 0;
	for (int i = ShareModeMin; i < ShareModeMax; i++)
		user_count[i] = 0;
}

void Server::lock()
{
    ref_lock.lock();
}

void Server::unlock()
{
    ref_lock.unlock();
}

int Server::assign_session(spSession &session)
{
    curr_users += session->get_alloc_size();
	++curr_sessions;
	session->set_server(this);

	session->set_archiver(get_archiver());
	
	log_debug(ZONE, "Allocated %d users for meeting %s", session->get_alloc_size(), session->get_sessionid());
	return Ok;
}

int Server::release_session(spSession &session)
{
	--curr_sessions;
    if (session->get_server() == this)
    {
        curr_users -= session->get_alloc_size();
        session->set_server(NULL);
        session->set_archiver(NULL);
    }
	log_debug(ZONE, "Released %d users for meeting %s", session->get_alloc_size(), session->get_sessionid());
	return Ok;
}

void Server::add_user_count(int mode, int count)
{
	IUseMutex lock(ref_lock);
	user_count[mode] += count;
}

void Server::remove_user_count(int mode, int count)
{
	IUseMutex lock(ref_lock);
	user_count[mode] -= count;
}


/*
 * Session (a.k.a. meeting)
 */

Session::Session(const char * _id, const char * _meetingid, int _size, int _mode) :
	sessionid(_id), meetingid(_meetingid)
{
	alloc_size = _size;
	alloc_mode = _mode;
	alloc_time = time(NULL);
	idle_time = alloc_time;
	curr_size = 0;
	state = StateNone;
	is_recording = false;
	mode = ShareModeNone;
}

void Session::lock()
{
    ref_lock.lock();
}

void Session::unlock()
{
    ref_lock.unlock();
}

int Session::start_session(int _mode)
{
	state = StateActive;
	mode = _mode;
	return Ok;
}

int Session::end_session(bool final)
{
	server->remove_user_count(mode, curr_size);

	curr_size = 0;
	log_debug(ZONE, "Session %s is idle", sessionid.get_string());
	idle_time = time(NULL);

	state = final ? StateEnded : StateNone;
	startup_mode = ShareModeNone;

	// For now, leaving share mode set until a new session is started...

	return Ok;
}

int Session::change_presenter()
{
	server->remove_user_count(mode, curr_size);
	curr_size = 0;

	state = StateChangePresenter;
	return Ok;
}

int Session::add_participant_count()
{
	if (++curr_size == 1)
		log_debug(ZONE, "Session %s is not idle", sessionid.get_string());
	idle_time = 0;
	server->add_user_count(mode, 1);
	return Ok;
}

int Session::remove_participant_count()
{
	if (curr_size > 0)
	{
		if (--curr_size == 0)
		{
			log_debug(ZONE, "Session %s is idle", sessionid.get_string());
			idle_time = time(NULL);
		}
		server->remove_user_count(mode, 1);
	}
	return Ok;
}

int Session::add_participant(const char * partid, bool is_presenter, const char * address, spParticipant& client)
{
	client = find_participant(partid);
	if (client != NULL)
		return ParticipantExists;
	client = new Participant(this, partid, is_presenter ? ShareRolePresenter : ShareRoleViewer, address);
	participants.add(client);
	return Ok;
}

int Session::remove_participant(spParticipant& client)
{
	participants.remove(client, true);
	return Ok;
}

spParticipant Session::find_participant(const char * partid)
{
	IListIterator iter(participants);
	Participant * cl = (Participant*)iter.get_first();
	while (cl != NULL)
	{
		if (strcmp(cl->get_participantid(), partid) == 0)
			return cl;
		cl = (Participant*)iter.get_next();
	}
	return NULL;
}

int Session::get_participants(IList& list)
{
	participants.copy(list);
	return Ok;
}

/*
 * Participant
 */

Participant::Participant(Session * _session,
               const char * _participantid,
               int _type,
               const char * _address) :
	session(_session), participantid(_participantid), address(_address)
{
	client_type = _type;
}

void Participant::lock()
{
    ref_lock.lock();
}

void Participant::unlock()
{
    ref_lock.unlock();
}
