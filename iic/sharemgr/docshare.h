/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef DOCSHARE_H
#define DOCSHARE_H

#include "sharemgr.h"

class DocShareAction;

class DocShareMgr : public IThread
{
	SessionMgr& mgr;

	IAsyncQueue * queue;
    bool shutdown_flag;

    IMutex mgr_lock;

public:
	DocShareMgr(SessionMgr& mgr);

	int initialize();
    int shutdown();

	int start_doc_share(const char * sessionid, const char * meetingid, const char * from,
						const char * address, int share_starttime);
	int end_doc_share(const char * sessionid, const char * meetingid, const char * from);

	int download_document_page(const char * sessionid, const char * meetingid,
	                           const char * docid, const char * pageid);

	int configure_whiteboard(const char * sessionid, const char * meetingid, int options);

	int draw_on_whiteboard(const char * sessionid, const char * meetingid, const char * drawing);
	int on_ppt_remote_control(const char * sessionid, const char * meetingid, const char * action);

	// Dispatch actions to connected participants.
	int dispatch_action(Session * session, Participant * client, int event, const char * docid,
					    const char * pageid, const char * drawing, int wb_options);
    int dispatch_action(Session * session, Participant * client, int event);
	int dispatch_action(Session * session, Participant * client, int event, const char * _action);
	void send_action(DocShareAction * action);

	int lookup_session(const char * meetingid, const char * partid, spSession & session,
	                   spParticipant & client);
	void close_session(Session * session, int reason);

    // Main event processing loop.
    void run();
};


class DocShareAction
{
	spSession session;
	spParticipant client;
	int event;
	IString documentid;
	IString pageid;
	IString drawing;
	int options;

public:
	DocShareAction(Session * _session, Participant * _client, int _event, const char *_docid,
	               const char *_pageid, const char *_drawing, int _options);

	Session * get_session()			{ return session; }
    Participant * get_client()		{ return client; }
	int get_event()					{ return event; }
	const char * get_documentid()	{ return documentid; }
	const char * get_pageid()		{ return pageid; }
	const char * get_drawing()		{ return drawing; }
	int get_options()				{ return options; }
};

class DocShareMode : public Mode
{
	DocShareMgr& mgr;

public:
	DocShareMode(DocShareMgr& _mgr);

	// Implement doc share commands.
    int end_session(const char * sessionid, const char * meetingid, int reason);
    int reject_participant(const char * sessionid, const char * meetingid, const char * partid, int reason);
    int enable_remote_control(const char * sessionid, const char * meetingid, const char * partid, bool enable);
	int enable_recording(const char * sessionid, const char * meetingid, bool enable, int start);

    void participant_joined(const char * meetingid, const char * partid);
    void participant_left(const char * meetingid, const char * partid);
};


#endif
