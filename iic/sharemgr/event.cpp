/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include "../jcomponent/util/log.h"
#include "event.h"


/*
 * For handling RPC messages that are external notifications.
 */

static const char *event_names[] =
{
    "app_share_started",
    "app_share_ended",
    "doc_share_started",
    "doc_share_ended",
    "presenter_changed",
    "participant_joined",
    "participant_left",
    "remote_control_changed",
    "recording_changed",
	"ppt_share_started",
	"ppt_share_ended"
};

// Map event codes to event classes.
static int event_classes[] =
{
    Event::EventTypeSession,
    Event::EventTypeSession,
    Event::EventTypeSession,
    Event::EventTypeSession,
    Event::EventTypeSession,
    Event::EventTypeParticipant,
    Event::EventTypeParticipant,
    Event::EventTypeState,
    Event::EventTypeState,
    Event::EventTypeSession,
    Event::EventTypeSession
};

const char * Event::get_url(const char * default_url)
{
    const char * url = session->get_callback_url();

    /* Use URL assigned to meeting if available, otherwise use default. */
    if (*url == '\0')
    {
        url = default_url;
        if (*url == '\0')
        {
            log_debug(ZONE, "No callback url for session %s, not sending event %d", session->get_sessionid(), event);
            return NULL;
        }
    }

    return url;
}

Event * Event::create_event(int event, spSession & session, const char * partid, int flag)
{
    switch (event_classes[event])
    {
        case EventTypeSession:
            return new SessionEvent(event, session, partid, flag);
        case EventTypeParticipant:
            return new ParticipantEvent(event, session, partid, flag);
        case EventTypeState:
            return new StateEvent(event, session, partid, flag);
    }

    return NULL;
}

void SessionEvent::dispatch(xmlrpc_server_info *info)
{
    log_debug(ZONE, "Sending %s event for session %s", event_names[event], session->get_token());
    xmlrpc_client_call_server_asynch(info,
                                    (char *)event_names[event],
                                    EventMgr::request_callback,
                                    NULL,
                                    "(sss)",
                                    session->get_token(),
                                    session->get_meetingid(),
                                    participantid.get_string());
}

void ParticipantEvent::dispatch(xmlrpc_server_info *info)
{
    log_debug(ZONE, "Sending %s event for session %s, participant %s", event_names[event],
              session->get_token(), participantid.get_string());
    xmlrpc_client_call_server_asynch(info,
                                    (char *)event_names[event],
                                    EventMgr::request_callback,
                                    NULL,
                                    "(sss)",
                                    session->get_token(),
                                    session->get_meetingid(),
                                    participantid.get_string());
}

void StateEvent::dispatch(xmlrpc_server_info *info)
{
    log_debug(ZONE, "Sending %s event for session %s, participant %s, state %d", event_names[event],
              session->get_token(), participantid.get_string(), flag);
    xmlrpc_client_call_server_asynch(info,
                                    (char *)event_names[event],
                                    EventMgr::request_callback,
                                    NULL,
                                    "(sssi)",
                                    session->get_token(),
                                    session->get_meetingid(),
                                    participantid.get_string(),
                                    flag);
}

int EventMgr::initialize(const char *_url)
{
    queue = new IAsyncQueue();
    default_callback = _url;
    this->start();
    return Ok;
}

int EventMgr::shutdown()
{
    shutdown_flag = true;
    return Ok;
}

void EventMgr::add_event(int event, spSession & session, const char * partid, int flag)
{
    if (event < 0 || event >= EventMax)
    {
        log_debug(ZONE, "Can't dispatch unknown event %d", event);
        return;
    }

	log_debug(ZONE, "Adding event %s", event_names[event]);

    Event * ev = Event::create_event(event, session, partid, flag);
    queue->push(ev);
}

void EventMgr::send_message(IDispatchable * msg)
{
    xmlrpc_server_info* server_info = NULL;
    const char * url = msg->get_url(default_callback);
	if (url == NULL)
		return;

    /* Start up our XML-RPC client library. */
    server_info = setup_server(url, NULL, NULL);
    if (server_info == NULL)
    {
        log_debug(ZONE, "Failure setting up for event dispatch to %s", url);
        return;
    }

    /* Call our XML-RPC server. */
    msg->dispatch(server_info);

    xmlrpc_server_info_free(server_info);
}

void EventMgr::run()
{
    xmlrpc_client_init(XMLRPC_CLIENT_NO_FLAGS, NAME, VERSION);

    while (!shutdown_flag)
    {
        IDispatchable * msg;

        do
        {
            //log_debug(ZONE, "Checking for events");
            msg = (IDispatchable *)queue->pop(EVENT_POLL);
            if (msg != NULL)
            {
                send_message(msg);
                delete msg;
            }
        }
        while (msg != NULL);

        //log_debug(ZONE, "Waiting for async results");
        xmlrpc_client_event_loop_finish_asynch_timeout(EVENT_POLL);
    }

    xmlrpc_client_cleanup();
}

xmlrpc_server_info* EventMgr::setup_server(const char* server_url, const char* username, const char* password)
{
    xmlrpc_env env;
    xmlrpc_server_info* server_info = NULL;

    xmlrpc_env_init(&env);
    XMLRPC_FAIL_IF_FAULT(&env);

    server_info = xmlrpc_server_info_new(&env, (char *)server_url);
    XMLRPC_FAIL_IF_FAULT(&env);

    if (username != NULL && password != NULL)
    {
        xmlrpc_server_info_set_basic_auth(&env, server_info, (char *)username, (char *)password);
        XMLRPC_FAIL_IF_FAULT(&env);
    }

cleanup:
    if (env.fault_occurred)
    {
        xmlrpc_server_info_free(server_info);
        server_info = NULL;
    }
    xmlrpc_env_clean(&env);
    return server_info;
}

void EventMgr::request_callback(const char *server_url,
                                const char *method_name,
                                xmlrpc_value *param_array,
                                void *user_data,
                                xmlrpc_env *env,
                                xmlrpc_value *result)
{
    log_debug(ZONE, "Response for event %s received (%d: %s)", method_name, env->fault_occurred,
              env->fault_occurred ? env->fault_string : "no error");
}

