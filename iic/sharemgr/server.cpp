/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include <stdio.h>
#include <stdlib.h>
#ifdef LINUX
#include <signal.h>
#endif

#include "../jcomponent/jcomponent.h"
#include "../jcomponent/jcomponent_ext.h"
#include "../jcomponent/util/eventlog.h"
#include "../jcomponent/util/log.h"
#include "../jcomponent/util/nadutils.h"
#include "sharemgrrpc.h"
#include "docsharerpc.h"
#include "sharemgr.h"
#include "docshare.h"
#include "../services/librepository/repository.h"

#ifdef LINUX
static sighandler_t old_int_handler = NULL;
static sighandler_t old_term_handler = NULL;
#endif

#define JCOMPNAME "sharemgr"

int client_min_rev = 1;
int client_curr_rev = 1;
IString version_file_name;
IString recording_base_url;
IString doc_base_url;
IString repository_user;
IString repository_passwd;

SessionMgr mgr;
DocShareMgr docmgr(mgr);
CRepository sRepository;
EventLog event_log;
IMutex event_mutex;

int get_config(config_t conf);
int JC_connected();


// *** Initialization ***

int initialize(const char * config)
{
    char *config_file = "/opt/iic/conf/config.xml";
	int instance = 0;

    config_t conf = config_new();
    config_load(conf, JCOMPNAME, instance, config_file);

	if (jc_init(conf, JCOMPNAME))
	{
		log_error(ZONE, "Unable to initialize sharemgr service\n");
		return -1;
	}

	sRepository.init(IIC_REPOSITORY_INCLUDE_DOC_TYPE, NULL, NULL);
	
	int status = get_config(conf);
    if (status)
        return status;

	event_log.initialize(EvnLog_Type_Call);
    IThread::initialize();

	sharemgr_init(config);
	docshare_init();
	
    return status;
}

void shutdown()
{
	log_status(ZONE, "Shutting down");
    mgr.shutdown();
	docmgr.shutdown();
}

int get_config_int(const char *str)
{
	if (str == NULL)
		return 0;
	return atoi(str);
}

// Read configuration
int get_config(config_t conf)
{
    char * param_val;
    
    mgr.set_server_name(config_get_one(conf, JCOMPNAME, "serverName", 0));
    mgr.set_control_port(get_config_int(config_get_one(conf, JCOMPNAME, "controlPort", 0)));
    mgr.set_share_port(get_config_int(config_get_one(conf, JCOMPNAME, "sharePort", 0)));
    mgr.set_default_callback(config_get_one(conf, JCOMPNAME, "eventURL", 0));
    client_curr_rev = get_config_int(config_get_one(conf, JCOMPNAME, "clientCurrRev", 0));
    client_min_rev = get_config_int(config_get_one(conf, JCOMPNAME, "clientMinRev", 0));
    version_file_name = config_get_one(conf, JCOMPNAME, "upgradeClientFile", 0);

    recording_base_url = config_get_one(conf, JCOMPNAME, "recording_base_url", 0);
    doc_base_url = config_get_one(conf, JCOMPNAME, "doc_base_url", 0);
	repository_user = config_get_one(conf, JCOMPNAME, "repository_username", 0);
	repository_passwd = config_get_one(conf, JCOMPNAME, "repository_password", 0);
    sRepository.set_archive_name_key(config_get_one(conf, JCOMPNAME, "archive-name-key", 0));
		
    param_val = config_get_one(conf, JCOMPNAME, "eventLogDir", 0);
    event_log.set_location((param_val ? param_val : "."), "shares");

    param_val = config_get_one(conf, JCOMPNAME, "eventRotationTime", 0);
    event_log.set_rotation_time((param_val ? get_config_int(param_val) : 0));

    param_val = config_get_one(conf, JCOMPNAME, "idleSessionTimeout", 0);
    mgr.set_idle_timeout((param_val ? get_config_int(param_val) : 600));

	// Share server "stacks"...
    int nsvr = config_count(conf, JCOMPNAME, "shareServers.server.host");
    int i;
    for ( i = 0; i < nsvr; i++)
    {
        char *host = config_get_one(conf, JCOMPNAME, "shareServers.server.host", i);
        char *reflector = config_get_one(conf, JCOMPNAME, "shareServers.server.reflectorAddr", i);
        char *jabber = config_get_one(conf, JCOMPNAME, "shareServers.server.xmlRouterAddr", i);
        char *web = config_get_one(conf, JCOMPNAME, "shareServers.server.repositoryAddr", i);
        char *archiver = config_get_one(conf, JCOMPNAME, "shareServers.server.archiver", i);
        char *max = config_get_one(conf, JCOMPNAME, "shareServers.server.maxUsers", i);
        mgr.add_server(host, reflector, jabber, web, archiver, get_config_int(max));
    }

    return Ok;
}

#ifdef LINUX

void onSigInt(int signo)
{
    shutdown();
    if (old_int_handler)
        old_int_handler(signo);
}

void onSigTerm(int signo)
{
    shutdown();
    if (old_term_handler)
        old_term_handler(signo);
}

#endif


int main (int argc, char **argv)
{
	fprintf(stderr, "IIC Share Session Manager 1.0   Copyright (C) 2004 imidio.com\n\n");
    
	if (argc != 2) {
		fprintf(stderr, "Usage: sharemgr [sharemgr-http.conf]\n");
		exit(1);
    }
    
#ifdef LINUX
    // Actually I think the abyss signal handler ends up last registered, but I'll leave this here
    old_int_handler = signal(SIGINT, onSigInt);
    old_term_handler = signal(SIGTERM, onSigTerm);
#endif

	if (initialize(argv[1]))
	{
		fprintf(stderr, "Error during initialization\n");
		exit(1);
	}

	// Establish connection to Jabber server
	jc_run(TRUE);

	// Start XML-RPC API server.
    xmlrpc_server_abyss_run();

    /* We never reach this point. */
    return 0;
}
