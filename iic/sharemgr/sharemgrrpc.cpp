/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

/* 
 * Share Session Manager APIs exposed to web application server
 *
 * These APIs are currently exposed directly from this process using
 * the embedded Abyss server.  These may be moved to the external API
 * mechanism used by the full zon server.
 */

#include <stdio.h>

#include "../jcomponent/jcomponent.h"
#include "../jcomponent/jcomponent_ext.h"
#include "../jcomponent/rpc.h"
#include "../jcomponent/util/eventlog.h"
#include "../jcomponent/util/log.h"
#include "../jcomponent/util/nadutils.h"
#include "../jcomponent/util/rpcresult.h"
#include "../jcomponent/util/port.h"
#include "blowfish/blowfish.h"
#include "base64/Base64.h"
#include "sharemgrrpc.h"
#include "sharemgr.h"
#include "docshare.h"
#include "../services/librepository/repository.h"

extern SessionMgr mgr;
extern CRepository sRepository;
extern EventLog event_log;
CBlowFish blowfish_crypt;

extern IString recording_base_url;
extern IString doc_base_url;
extern IString repository_user;
extern IString repository_passwd;

unsigned char SHARE_KEY[16] = {0x33,0x4B,0xAB,0x43,0xC5,0x3F,0x41,0x46,0x8C,0x27,0x52,0x01,0xB6,0xC1,0xA6,0xDA};		// 128-bit GUID
#define MAGIC "ShareMgr"
#define MAGIC_LEN 8


// *** Utilities ***

// Todo:  add a bit more randomness to session token.  However, as it is now, 
// since the same session id most likely won't be reissued for the same meeting,
// replay attacks won't work.

int build_session_token(const char * sessionid, char * token)
{
#ifdef CRYPT_TOKEN
	char buff[256];
	int slen, blocklen;
	CBase64 base64;

	sprintf(buff, MAGIC":%s", sessionid);
	slen = strlen(buff);
	blocklen = (slen + 8) - (slen % 8);
	
	memset(buff + slen, 0, blocklen - slen);
	
	blowfish_crypt.Encode((unsigned char *)buff, (unsigned char *)buff, blocklen);
	
	char *enc = base64.Encode(buff, blocklen);

	strcpy(token, enc);
	delete [] enc;
#else
    strcpy(token, sessionid);
#endif

	return Ok;
}

int parse_session_token(const char * token, char * sessionid)
{
#ifdef CRYPT_TOKEN
	char buff[256];
	int blocklen;
	CBase64 base64;
	
	blocklen = base64.Decode(token, buff);
	
	blowfish_crypt.Decode((unsigned char *)buff, (unsigned char *)buff, blocklen);
	
	if (strncmp(buff, MAGIC, MAGIC_LEN) != 0)
	{
		log_debug(ZONE, "Invalid session token: %s", token);
		return InvalidSession;
	}

	strcpy(sessionid, buff + MAGIC_LEN + 1);
#else
    strcpy(sessionid, token);
#endif

	return Ok;
}

// Convert IList into output array.
xmlrpc_value *build_output_from_list(xmlrpc_env *env, IList *list, int columns, const char *format, const char **names)
{
    xmlrpc_value *val = NULL;
    xmlrpc_value *array = NULL;
    IListIterator iter(*list);
    IString * str = (IString *)iter.get_first();
    int size = list->size();
    int i = 0;
    xmlrpc_value *strings = NULL;

	array = xmlrpc_build_value(env, "()");
	XMLRPC_FAIL_IF_FAULT(env);

    while(str)
    {
		int column = i++ % columns;
        if (column == 0)
        {
            if (strings)
            {
                xmlrpc_array_append_item(env, array, strings);
                xmlrpc_DECREF(strings);
                XMLRPC_FAIL_IF_FAULT(env);
            }

            strings = xmlrpc_struct_new(env);
            XMLRPC_FAIL_IF_FAULT(env);
        }

		xmlrpc_value *value;
		switch (format[column])
		{
			case 's': value = xmlrpc_build_value(env, "s", str->get_string()); break;
			case 'i': value = xmlrpc_build_value(env, "i", atoi(str->get_string())); break;
			default: xmlrpc_env_set_fault(env, Failed, "Internal error: unsupported type conversion");

		}
        XMLRPC_FAIL_IF_FAULT(env);

		xmlrpc_struct_set_value(env, strings, names[column], value);
        xmlrpc_DECREF(value);
        XMLRPC_FAIL_IF_FAULT(env);

        str = (IString *)iter.get_next();
    }

    if (strings)
    {
        xmlrpc_array_append_item(env, array, strings);
        xmlrpc_DECREF(strings);
        XMLRPC_FAIL_IF_FAULT(env);
    }

    val = xmlrpc_build_value(env, "(V)", array);
    XMLRPC_FAIL_IF_FAULT(env);

    xmlrpc_INCREF(val);

cleanup:
    if (val)
        xmlrpc_DECREF(val);
    if (array)
        xmlrpc_DECREF(array);

    return val;
}


// *** RPC APIs. ***

xmlrpc_value *
register_callback (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	xmlrpc_value *output = NULL;
	char *token;
	char *meetingid;
	char *url;
	
    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(sss)", &token, &meetingid, &url);
    if (env->fault_occurred)
    {
    	log_debug(ZONE, "register_callback: invalid parameter list");
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to register_callback");
		return NULL;
    }

	// Get session id from session token.
	char sessionid[256];
	int stat = parse_session_token(token, sessionid);
	if (FAILED(stat))
	{
        xmlrpc_env_set_fault(env, stat, "Invalid session token");
		return NULL;
    }
	
	// Register callback with meeting
	stat = mgr.set_callback(sessionid, meetingid, url);
	if (FAILED(stat))
	{
		log_debug(ZONE, "register_callback error: %d", stat);
        xmlrpc_env_set_fault(env, stat, "Error registering session callback");
		return NULL;
    }

	
	output = xmlrpc_build_value(env, "i", 0);
    if (env->fault_occurred)
    {
    	xmlrpc_env_set_fault(env, Failed, "Error building output");
		return NULL;
    }
    
    return output;
}

xmlrpc_value *
allocate_share (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	xmlrpc_value *output = NULL;
	char *meetingid;
	int size;
	int share_mode;
	
    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(sii)", &meetingid, &size, &share_mode);
    if (env->fault_occurred)
    {
    	log_debug(ZONE, "allocate_share: invalid parameter list");
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to allocate_share");
		return NULL;
    }

	// Allocate session.
	spSession session;
	int stat = mgr.allocate_session(meetingid, size, share_mode, session);
	if (FAILED(stat))
	{
		log_debug(ZONE, "allocate_share error: %d", stat);
        xmlrpc_env_set_fault(env, stat, "Error allocating share session");
		return NULL;
    }

	// Create session token.
	char token[256];
	build_session_token(session->get_sessionid(), token);
    session->set_token(token);    // currently token is same for all participants, so cache it
	
	// Client is directed to xmlrouter for all share modes
	output = xmlrpc_build_value(env, "(sss)", session->get_server()->get_xmlrouter(), 
								session->get_server()->get_xmlrouter(), token);
    if (env->fault_occurred)
    {
    	xmlrpc_env_set_fault(env, Failed, "Error building output");
		return NULL;
    }
	
    return output;
}

xmlrpc_value *
end_share (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	xmlrpc_value *output = NULL;
	char *meetingid;
	char *token;
	
    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(ss)", &token, &meetingid);
    if (env->fault_occurred)
    {
    	log_debug(ZONE, "end_share: invalid parameter list");
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to end_share");
		return NULL;
    }

	// Get session id from session token.
	char sessionid[256];
	int stat = parse_session_token(token, sessionid);
	if (FAILED(stat))
	{
        xmlrpc_env_set_fault(env, stat, "Invalid session token");
		return NULL;
    }
	
	// Dispatch end session.
	stat = mgr.end_session(sessionid, meetingid);
	if (FAILED(stat))
	{
		log_debug(ZONE, "end_share error: %d", stat);
        xmlrpc_env_set_fault(env, stat, "Error ending share session");
		return NULL;
    }

	output = xmlrpc_build_value(env, "i", 0);
    if (env->fault_occurred)
    {
    	xmlrpc_env_set_fault(env, Failed, "Error building output");
		return NULL;
    }
	
    return output;
}

xmlrpc_value *
find_share (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	xmlrpc_value *output = NULL;
	char *meetingid;
	int size;
	int share_mode;
	
    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(s)", &meetingid);
    if (env->fault_occurred)
    {
    	log_debug(ZONE, "find_share: invalid parameter list");
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to find_share");
		return NULL;
    }

	// Find session.
	spSession session;
	int stat = mgr.find_session(meetingid, session);
	if (FAILED(stat))
	{
		log_debug(ZONE, "find_share error: %d", stat);
        xmlrpc_env_set_fault(env, stat, "Error finding share session");
		return NULL;
    }

	// Create session token.
	char token[256];
	build_session_token(session->get_sessionid(), token);
	
	// Client is directed to xmlrouter for all share modes
	output = xmlrpc_build_value(env, "(sss)", session->get_server()->get_xmlrouter(),
								session->get_server()->get_xmlrouter(), token);
    if (env->fault_occurred)
    {
    	xmlrpc_env_set_fault(env, Failed, "Error building output");
		return NULL;
    }
	
    return output;
}

xmlrpc_value *
enable_remote_control (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	xmlrpc_value *output = NULL;
	char *meetingid;
	char *token;
    char *partid;
    xmlrpc_bool enable;
	
    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(sssb)", &token, &meetingid, &partid, &enable);
    if (env->fault_occurred)
    {
    	log_debug(ZONE, "enable_remote_control: invalid parameter list");
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to enable_remote_control");
		return NULL;
    }

	// Get session id from session token.
	char sessionid[256];
	int stat = parse_session_token(token, sessionid);
	if (FAILED(stat))
	{
        xmlrpc_env_set_fault(env, stat, "Invalid session token");
		return NULL;
    }
	
	// Dispatch enable remote control
	stat = mgr.enable_remote_control(sessionid, meetingid, partid, enable);
	if (FAILED(stat))
	{
		log_debug(ZONE, "enable_remote_control error: %d", stat);
        xmlrpc_env_set_fault(env, stat, "Error enabling remote control");
		return NULL;
    }

	output = xmlrpc_build_value(env, "i", 0);
    if (env->fault_occurred)
    {
    	xmlrpc_env_set_fault(env, Failed, "Error building output");
		return NULL;
    }
	
    return output;
}

xmlrpc_value *
change_presenter (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	xmlrpc_value *output = NULL;
	char *meetingid;
	char *token;
    char *partid;
    xmlrpc_bool enable;
	
    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(sss)", &token, &meetingid, &partid);
    if (env->fault_occurred)
    {
    	log_debug(ZONE, "change_presenter: invalid parameter list");
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to change_presenter");
		return NULL;
    }

	// Get session id from session token.
	char sessionid[256];
	int stat = parse_session_token(token, sessionid);
	if (FAILED(stat))
	{
        xmlrpc_env_set_fault(env, stat, "Invalid session token");
		return NULL;
    }
	
	// Dispatch 
	stat = mgr.change_presenter(sessionid, meetingid, partid);
	if (FAILED(stat))
	{
		log_debug(ZONE, "change_presenter error: %d", stat);
        xmlrpc_env_set_fault(env, stat, "Error changing presenter");
		return NULL;
    }

	output = xmlrpc_build_value(env, "i", 0);
    if (env->fault_occurred)
    {
    	xmlrpc_env_set_fault(env, Failed, "Error building output");
		return NULL;
    }
	
    return output;
}

xmlrpc_value *
enable_recording (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	xmlrpc_value *output = NULL;
	char *meetingid;
	char *token;
    char *partid;
    xmlrpc_bool enable;
	
    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(sssb)", &token, &meetingid, &partid, &enable);
    if (env->fault_occurred)
    {
    	log_debug(ZONE, "enable_recording: invalid parameter list");
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to enable_recording");
		return NULL;
    }

	// Get session id from session token.
	char sessionid[256];
	int stat = parse_session_token(token, sessionid);
	if (FAILED(stat))
	{
        xmlrpc_env_set_fault(env, stat, "Invalid session token");
		return NULL;
    }
	
	// Dispatch enable remote control
	stat = mgr.enable_recording(sessionid, meetingid, partid, enable);
	if (FAILED(stat))
	{
		log_debug(ZONE, "enable_recording error: %d", stat);
        xmlrpc_env_set_fault(env, stat, "Error enabling recording");
		return NULL;
    }

	output = xmlrpc_build_value(env, "i", 0);
    if (env->fault_occurred)
    {
    	xmlrpc_env_set_fault(env, Failed, "Error building output");
		return NULL;
    }
	
    return output;
}

xmlrpc_value *
set_recording_audio (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	xmlrpc_value *output = NULL;
	char *meetingid;
	char *token;
    char *url, *format;
	
    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(ssss)", &token, &meetingid, &url, &format);
    if (env->fault_occurred)
    {
    	log_debug(ZONE, "set_recording_audio: invalid parameter list");
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to set_recording_audio");
		return NULL;
    }

	// Get session id from session token.
	char sessionid[256];
	int stat = parse_session_token(token, sessionid);
	if (FAILED(stat))
	{
        xmlrpc_env_set_fault(env, stat, "Invalid session token");
		return NULL;
    }
	
	stat = mgr.set_recording_audio(sessionid, meetingid, url, format);
	if (FAILED(stat))
	{
		log_debug(ZONE, "set_recording_audio error: %d", stat);
        xmlrpc_env_set_fault(env, stat, "Error setting recording audio URL");
		return NULL;
    }

	output = xmlrpc_build_value(env, "i", 0);
    if (env->fault_occurred)
    {
    	xmlrpc_env_set_fault(env, Failed, "Error building output");
		return NULL;
    }
	
    return output;
}

xmlrpc_value *
set_recording_output (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	xmlrpc_value *output = NULL;
	char *meetingid;
	char *token;
    char *url, *user, *pass;
	
    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(sssss)", &token, &meetingid, &url, &user, &pass);
    if (env->fault_occurred)
    {
    	log_debug(ZONE, "set_recording_output: invalid parameter list");
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to set_recording_output");
		return NULL;
    }

	// Get session id from session token.
	char sessionid[256];
	int stat = parse_session_token(token, sessionid);
	if (FAILED(stat))
	{
        xmlrpc_env_set_fault(env, stat, "Invalid session token");
		return NULL;
    }
	
	stat = mgr.set_recording_output(sessionid, meetingid, url, user, pass);
	if (FAILED(stat))
	{
		log_debug(ZONE, "set_recording_output error: %d", stat);
        xmlrpc_env_set_fault(env, stat, "Error setting recording audio URL");
		return NULL;
    }

	output = xmlrpc_build_value(env, "i", 0);
    if (env->fault_occurred)
    {
    	xmlrpc_env_set_fault(env, Failed, "Error building output");
		return NULL;
    }
	
    return output;
}

xmlrpc_value *
get_recording_list (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	xmlrpc_value *output = NULL;
	char *meetingid;
	static const char *names[] = { "name", "type", "url" };
	
    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(s)", &meetingid);
    if (env->fault_occurred)
    {
    	log_debug(ZONE, "get_recording_list: invalid parameter list");
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to get_recording_list");
		return NULL;
    }

	log_debug(ZONE, "Fetching recording list for meeting %s", meetingid);
	
	IList list;
	char * msg = NULL;
	int res;

	res = sRepository.get_recording_list(meetingid, recording_base_url, 
						repository_user, repository_passwd, &list, &msg);
	if (res==0)
	{
		output = build_output_from_list(env, &list, 3, "sis", names);
		if (env->fault_occurred)
		{
			xmlrpc_env_set_fault(env, Failed, "Error building output for get_recording_list");
			output = NULL;
		}
	}
	else
	{
		xmlrpc_env_set_fault(env, Failed, (char*)(msg?msg:"Error retrieving recording list"));
	}
	
	if (msg)
		free(msg);

	return output;
}

xmlrpc_value *
remove_recordings (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	xmlrpc_value *output = NULL;
	char *meetingid;
	char *recordingurl;
	
    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(ss)", &meetingid, &recordingurl);
    if (env->fault_occurred)
    {
    	log_debug(ZONE, "remove_recordings: invalid parameter list");
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to remove_recordings");
		return NULL;
    }

	IString absolute_url(recordingurl);
	char * msg = NULL;
	char proto[8];
	int res;

	memset(proto, 0, 8);
	strncpy(proto, absolute_url.get_string(), 7);

	// Pass in either complete URL of path, or encrypted "instance id", or
	// unencrypted "instance id"
	if (strcasecmp(proto, "http://"))
	{
		sRepository.build_url(recording_base_url, meetingid, recordingurl, absolute_url);
	}
	
	log_debug(ZONE, "Deleting recordings for meeting %s (%s)", meetingid, (const char *)absolute_url);
	
	res = sRepository.remove_recordings(meetingid, (const char*)absolute_url, 
						repository_user, repository_passwd, &msg);
	if (res==0)
	{
		output = xmlrpc_build_value(env, "(i)", res);
		if (env->fault_occurred)
		{
			xmlrpc_env_set_fault(env, Failed, "Error building output for get_recording_list");
			output = NULL;
		}
	}
	else
	{
		xmlrpc_env_set_fault(env, Failed, (char*)(msg?msg:"Error deleting recordings"));
	}

    if (msg)
        free (msg);
	
	return output;
}

xmlrpc_value *
get_doc_list (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	xmlrpc_value *output = NULL;
	char *meetingid;
	static const char *names[] = { "name", "type", "url" };
	
    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(s)", &meetingid);
    if (env->fault_occurred)
    {
    	log_debug(ZONE, "get_doc_list: invalid parameter list");
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to get_doc_list");
		return NULL;
    }

	log_debug(ZONE, "Fetching document list for meeting %s", meetingid);
	
	IList list;
	char * msg = NULL;
	int res;

	res = sRepository.get_doc_list(meetingid, doc_base_url, 
						repository_user, repository_passwd, &list, &msg);
	if (res==0)
	{
		output = build_output_from_list(env, &list, 3, "sis", names);
		if (env->fault_occurred)
		{
			xmlrpc_env_set_fault(env, Failed, "Error building output for get_doc_list");
			output = NULL;
		}
	}
	else
	{
		xmlrpc_env_set_fault(env, Failed, (char*)(msg?msg:"Error retrieving document list"));
	}
	
	if (msg)
		free(msg);

	return output;
}

xmlrpc_value *
remove_docs (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	xmlrpc_value *output = NULL;
	char *meetingid;
	char *recordingurl;
	
    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(ss)", &meetingid, &recordingurl);
    if (env->fault_occurred)
    {
    	log_debug(ZONE, "remove_docs: invalid parameter list");
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to remove_docs");
		return NULL;
    }

	IString absolute_url(recordingurl);
	char * msg = NULL;
	char proto[8];
	int res;

	memset(proto, 0, 8);
	strncpy(proto, absolute_url.get_string(), 7);

	// Pass in either complete URL of path, or encrypted "instance id", or
	// unencrypted "instance id"
	if (strcasecmp(proto, "http://"))
	{
		sRepository.build_url(doc_base_url, meetingid, recordingurl, absolute_url);
	}
	
	log_debug(ZONE, "Deleting docs for meeting %s (%s)", meetingid, (const char *)absolute_url);
	
	res = sRepository.remove_docs(meetingid, (const char*)absolute_url, 
						repository_user, repository_passwd, &msg);
	if (res==0)
	{
		output = xmlrpc_build_value(env, "(i)", res);
		if (env->fault_occurred)
		{
			xmlrpc_env_set_fault(env, Failed, "Error building output for get_doc_list");
			output = NULL;
		}
	}
	else
	{
		xmlrpc_env_set_fault(env, Failed, (char*)(msg?msg:"Error deleting docs"));
	}

    if (msg)
        free (msg);
	
	return output;
}

xmlrpc_value *
get_server_status (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	xmlrpc_value *output = NULL;
	xmlrpc_value *array;
	xmlrpc_value *server;
	int stat;
	int total_app_share = 0;
	int total_doc_share = 0;
	ServerStatus list[MAX_SHARE_SERVERS];
	int count;
	
    /* No arguments. */

	// Get current server status.
	stat = mgr.get_server_status(list, count);
	if (stat)
	{
		xmlrpc_env_set_fault(env, stat, "Unable to get server status");
		goto cleanup;
	}
	
	// Convert to XMLRPC output array.
	array = xmlrpc_build_value(env, "()");
	XMLRPC_FAIL_IF_FAULT(env);

	int i;
	for (i = 0; i < count; i++)
	{
		server = xmlrpc_build_value(env, "{s:s,s:i,s:i,s:i}", 
						"server_name", list[i].name.get_string(),
						"number_app_share", list[i].user_count[ShareModeApplication],
						"number_doc_share", list[i].user_count[ShareModeDocument],
						"number_ppt_share", list[i].user_count[ShareModePPT]);
		XMLRPC_FAIL_IF_FAULT(env);

		xmlrpc_array_append_item(env, array, server);
        xmlrpc_DECREF(server);
		
		total_app_share += list[i].user_count[ShareModeApplication];
		total_doc_share += list[i].user_count[ShareModeDocument];
	}

	output = xmlrpc_build_value(env, "(iiiV)", 1, total_app_share, total_doc_share, array);
	xmlrpc_DECREF(array);

cleanup:	
	return output;
}

xmlrpc_value *
get_server_details (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	int stat;
	xmlrpc_value *output = NULL;
	xmlrpc_value *session;
	char *server;
	SessionStatus * list;
	int count;
		
    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(s)", &server);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to get_server_details");
		goto cleanup;
	}

	// Get details for this server.
	stat = mgr.get_server_details(server, list, count);
	if (stat)
	{
		xmlrpc_env_set_fault(env, stat, "Unable to get server status");
		goto cleanup;
	}

	// Convert to XMLRPC output array.
	output = xmlrpc_build_value(env, "()");
	XMLRPC_FAIL_IF_FAULT(env);

	int i;
	for (i = 0; i < count; i++)
	{
		session = xmlrpc_build_value(env, "{s:s,s:i,s:i}", 
						"meeting_id", list[i].meetingid.get_string(),
						"type", list[i].mode,
						"number_participants", list[i].curr_size);
		XMLRPC_FAIL_IF_FAULT(env);

		xmlrpc_array_append_item(env, output, session);
        xmlrpc_DECREF(session);
	}
	
cleanup:	
	return output;
}

xmlrpc_value *
get_meeting_status (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	int stat;
	xmlrpc_value *output = NULL;
	xmlrpc_value *array;
	xmlrpc_value *part_struct;
	char *meetingid;
	spSession session;
	IList list;
	IListIterator iter(list);
	Participant * participant;
		
    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(s)", &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to get_meeting_status");
		goto cleanup;
	}
	
	session = mgr.find_session_by_meetingid(meetingid);
	if (session == NULL)
	{
        xmlrpc_env_set_fault(env, InvalidMeetingId, "Unable to get meeting status");
		goto cleanup;
	}

	// Get participants for this meeting.
	stat = mgr.get_participants(session, list);
	if (stat)
	{
		xmlrpc_env_set_fault(env, stat, "Unable to get meeting status");
		goto cleanup;
	}

	// Convert to XMLRPC output array.
	array = xmlrpc_build_value(env, "()");
	XMLRPC_FAIL_IF_FAULT(env);

	participant = (Participant*) iter.get_first();
	while (participant != NULL)
	{
		part_struct = xmlrpc_build_value(env, "{s:s,s:i}", 
						"participant_id", participant->get_participantid(),
						"role", participant->get_type());
		XMLRPC_FAIL_IF_FAULT(env);

		xmlrpc_array_append_item(env, array, part_struct);
        xmlrpc_DECREF(part_struct);
		
		participant = (Participant*) iter.get_next();
	}

	output = xmlrpc_build_value(env, "(ssiiV)", 
					session->get_server()->get_reflector(),
					session->get_server()->get_xmlrouter(),
					session->get_mode(),
					list.size(),
					array);
	xmlrpc_DECREF(array);
	
cleanup:	
	return output;
}


// *** RPC callbacks ***

void RPC_error(const char *id, xmlrpc_int32 fault_code, const char * message)
{
    log_error(ZONE, "*** Error processing RPC request: %s - %s\n", id, message);

	const char * end = strchr(id, ':');
	if (end == NULL)
    {
        log_error(ZONE, "RPC_error without context", id);
		return;
    }
}

void RPC_result(const char * id, xmlrpc_value * resval)
{
	xmlrpc_env env;

    xmlrpc_env_init(&env);

	const char * end = strchr(id, ':');
	if (end == NULL)
	{
        log_error(ZONE, "RPC_error without context", id);
		return;
	}
    
	int len = end - id;

	if (!strncmp(id, "arc_reg", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);
	}

cleanup:
    if (env.fault_occurred)
    {
        char buffer[512];
        strcpy(buffer, "Result error: ");
        if (env.fault_string)
            strncat(buffer, env.fault_string, 512 - 16);
        else
            strcat(buffer, "unknown cause");
        RPC_error(id, Failed, buffer);
    }
    xmlrpc_env_clean(&env);
}


int sharemgr_init(const char *config)
{
	rpc_set_callbacks(RPC_result, RPC_error, NULL);
	mgr.initialize();
	blowfish_crypt.Initialize(SHARE_KEY, sizeof(SHARE_KEY));

    xmlrpc_server_abyss_init(XMLRPC_SERVER_ABYSS_NO_FLAGS, const_cast<char *>(config));
    xmlrpc_server_abyss_add_method("register_callback", &register_callback, NULL);
    xmlrpc_server_abyss_add_method("allocate_share", &allocate_share, NULL);
    xmlrpc_server_abyss_add_method("end_share", &end_share, NULL);
    xmlrpc_server_abyss_add_method("find_share", &find_share, NULL);
    xmlrpc_server_abyss_add_method("enable_remote_control", &enable_remote_control, NULL);
    xmlrpc_server_abyss_add_method("change_presenter", &change_presenter, NULL);
    xmlrpc_server_abyss_add_method("enable_recording", &enable_recording, NULL);
    xmlrpc_server_abyss_add_method("set_recording_audio", &set_recording_audio, NULL);
    xmlrpc_server_abyss_add_method("set_recording_output", &set_recording_output, NULL);
    xmlrpc_server_abyss_add_method("get_recording_list", &get_recording_list, NULL);
    xmlrpc_server_abyss_add_method("remove_recordings", &remove_recordings, NULL);
    xmlrpc_server_abyss_add_method("get_doc_list", &get_doc_list, NULL);
    xmlrpc_server_abyss_add_method("remove_docs", &remove_docs, NULL);
    xmlrpc_server_abyss_add_method("get_server_status", &get_server_status, NULL);
    xmlrpc_server_abyss_add_method("get_server_details", &get_server_details, NULL);
    xmlrpc_server_abyss_add_method("get_meeting_status", &get_meeting_status, NULL);
	
	return 0;
}
