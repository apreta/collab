/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef SHR_EVENT_H
#define SHR_EVENT_H

#include "sharemgr.h"
#include "istring.h"
#include "ithread.h"

#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>
#include <xmlrpc-c/client.h>

#define EVENT_POLL 200

#define NAME       "zon-lite XML-RPC Client"
#define VERSION    "0.1"


class IDispatchable
{
public:
    virtual ~IDispatchable()
    { }
    
    virtual void dispatch(xmlrpc_server_info * srv) = 0;
    virtual const char * get_url(const char * default_url) = 0;
};

/*
 * For handling RPC messages that are external notifications.
 */

class Event : public IDispatchable
{
    friend class EventMgr;

protected:
    int event;
    spSession session;
    IString participantid;
    int flag;

public:
    enum { EventTypeSession, EventTypeParticipant, EventTypeState };
    
    Event(int _event, spSession & _session, const char * _partid, int _flag) :
        session(_session), participantid(_partid)
    {
        event = _event;
        flag = _flag;
    }
    
    const char * get_url(const char * default_url);
    
    static Event * create_event(int event, spSession & session, const char * partid, int flag);
};


class SessionEvent : public Event
{
public:
    SessionEvent(int _event, spSession & _session, const char * _partid, int _flag) :
        Event(_event, _session, _partid, _flag)
    {
    }
    
    void dispatch(xmlrpc_server_info * srv);
};


class ParticipantEvent : public Event
{
public:
    ParticipantEvent(int _event, spSession & _session, const char * _partid, int _flag) :
        Event(_event, _session, _partid, _flag)
    {
    }
    
    void dispatch(xmlrpc_server_info * srv);
};


class StateEvent : public Event
{
public:
    StateEvent(int _event, spSession & _session, const char * _partid, int _flag) :
        Event(_event, _session, _partid, _flag)
    {
    }
    
    void dispatch(xmlrpc_server_info * srv);
};

/*
 * For handling RPC messages that are internal commands (e.g. for archiver).
 */

class Command : public IDispatchable
{
    spSession session;

public:
    Command(spSession & _session) : session(_session)
    {
    }
    
    const char * get_url(const char * default_url);
};

// New session has been allocated.
class AllocateSessionCmd : public Command
{
public:
    AllocateSessionCmd(spSession & _session) : Command(_session)
    {
    }
    
    void dispatch(xmlrpc_server_info * srv);
};

// Session has been ended.
class EndSessionCmd : public Command
{
public:
    EndSessionCmd(spSession & _session) : Command(_session)
    {
    }

    void dispatch(xmlrpc_server_info * srv);
};

// API call to enable/disable recording for meeting.
class EnableRecordingCmd : public Command
{
    int flag;
    
public:
    EnableRecordingCmd(spSession & _session, int _flag) : Command(_session)
    {
        flag = _flag;
    }

    void dispatch(xmlrpc_server_info * srv);
};

// Session started/closed because presenter connected/disconnect.
class SessionStateCmd : public Command
{
    int state;
    
public:
    SessionStateCmd(spSession & _session, int _state) : Command(_session)
    {
        state = _state;
    }

    void dispatch(xmlrpc_server_info * srv);
};

/*
 * Generalized RPC event & command notification handler.
 */ 

class EventMgr : public IThread
{
    IAsyncQueue * queue;
    bool shutdown_flag;
    IString default_callback;
    
public:
    EventMgr()
    {
        shutdown_flag = false;
        queue = NULL;
    }

    int initialize(const char * default_callback);
    int shutdown();

    void add_event(int event, spSession & session, const char * partid, int flag);
    
    static void request_callback(const char *server_url, const char *method_name, xmlrpc_value *param_array, 
                                 void *user_data, xmlrpc_env *env, xmlrpc_value *result);
    
private:
    // Main event processing loop.    
    void run();
    
    void send_message(IDispatchable * msg);
    xmlrpc_server_info* setup_server(const char* server_url, const char* username, const char* password);
};

#endif
