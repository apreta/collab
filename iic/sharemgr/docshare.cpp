/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

/*
 * Extend session manager to support doc share.
 *
 * Basic flow when "iic:" link is clicked:
 *
 * Presenter
 *   Client connects to jabber and calls register_client.
 *      API returns session id if ok, or fault if rejected.
 *   	PresenterGranted event is dispatched to presenter.
 *   Presenter calls start_doc_share
 *   	DocShareStarted is dispatched to any connected clients
 *
 * Viewer
 *   Client connects to jabber and calls register_client.
 *      API returns session id if ok, or fault if rejected.
 *   	DocShareStarted is dispatched to client.
 *
 * Change presenter
 *   For simplicity, all clients disconnect and reconnect, same as for app
 *   share mode.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "../jcomponent/rpc.h"
#include "../jcomponent/util/eventlog.h"
#include "../jcomponent/util/log.h"
#include "../jcomponent/util/port.h"
#include "docsharerpc.h"
#include "docshare.h"
#include "event.h"

#define EXCLUDE_ERRORS
#include "../services/common/common.h"
#include "../services/common/eventnames.h"


extern DocShareMgr docmgr;
extern EventMgr dispatch;

DocShareMode doc_share_mode(docmgr);

/*
 * Doc Share Manager
 * Broadcasts doc share events to connected clients.
 */

DocShareMgr::DocShareMgr(SessionMgr& _mgr) :
	mgr(_mgr)
{
	shutdown_flag = false;
}

int DocShareMgr::initialize()
{
    queue = new IAsyncQueue();
    this->start();
    return Ok;
}

int DocShareMgr::shutdown()
{
	shutdown_flag = true;
	return Ok;
}

int DocShareMgr::start_doc_share(const char * sessionid, const char * meetingid, const char * from,
								 const char * address, int start_time)
{
	int status;
	spSession session;

    IUseMutex lock(mgr.get_lock());

	status = mgr.validate_session(sessionid, meetingid, session);
	if (status)
		return status;

	spParticipant client = mgr.find_client_by_address(from);
	if (client == NULL)
		return InvalidClient;

	// Start session (this logic belongs in session manager)
    if (!session->is_active())
    {
        log_debug(ZONE, "Session %s started", session->get_sessionid());
        session->start_session(ShareDocument);

		// Send API event
        dispatch.add_event(EventDocShareStarted, session, client->get_participantid(), 0);
    }

	// Dispatch events to doc share clients.
	session->set_docshare_url(address);
	dispatch_action(session, NULL, DocShareStarted);

	if (session->get_recording())
	{
		mgr.archiver_start(session, start_time);
	}

	return Ok;
}

int DocShareMgr::end_doc_share(const char * sessionid, const char * meetingid, const char * from)
{
	int status;
	spSession session;

    IUseMutex lock(mgr.get_lock());

	status = mgr.validate_session(sessionid, meetingid, session);
	if (status)
		return status;

	spParticipant client = mgr.find_client_by_address(from);
	if (client == NULL || !client->is_presenter())
		return InvalidClient;

	// Dispatch events to doc share clients
	dispatch_action(session, NULL, DocShareEnded);

	return Ok;
}

int DocShareMgr::download_document_page(const char * sessionid, const char * meetingid,
									    const char * docid, const char * pageid)
{
	int status;
	spSession session;

	status = mgr.validate_session(sessionid, meetingid, session);
	if (status)
		return status;

	// Dispatch events to clients, mainly to get this host into doc share mode.
	dispatch_action(session, NULL, DocShareDownloadPage, docid, pageid, NULL, 0);

	return Ok;
}

int DocShareMgr::configure_whiteboard(const char * sessionid, const char * meetingid, int options)
{
	int status;
	spSession session;

	status = mgr.validate_session(sessionid, meetingid, session);
	if (status)
		return status;

	dispatch_action(session, NULL, DocShareConfigure, NULL, NULL, NULL, options);

	return Ok;
}

int DocShareMgr::draw_on_whiteboard(const char * sessionid, const char * meetingid, const char * drawing)
{
	int status;
	spSession session;

	status = mgr.validate_session(sessionid, meetingid, session);
	if (status)
		return status;

	dispatch_action(session, NULL, DocShareDrawing, NULL, NULL, drawing, 0);

	return Ok;
}

int DocShareMgr::on_ppt_remote_control(const char * sessionid, const char * meetingid, const char * action)
{
	int status;
	spSession session;

	status = mgr.validate_session(sessionid, meetingid, session);
	if (status)
		return status;

	dispatch_action(session, NULL, PPTAction, action);

	return Ok;
}

int DocShareMgr::dispatch_action(Session * session, Participant * client, int event,
			const char * docid, const char * pageid, const char * drawing, int wb_options)
{
	DocShareAction * action = new DocShareAction(session, client, event, docid, pageid, drawing, wb_options);
	queue->push(action);
	return Ok;
}

int DocShareMgr::dispatch_action(Session * session, Participant * client, int event)
{
	DocShareAction * action = new DocShareAction(session, client, event, NULL, NULL, NULL, 0);
	queue->push(action);
	return Ok;
}

int DocShareMgr::dispatch_action(Session * session, Participant * client, int event, const char * _action)
{
	DocShareAction * action = new DocShareAction(session, client, event, NULL, NULL, _action, 0);
	queue->push(action);
	return Ok;
}

void DocShareMgr::send_action(DocShareAction * action)
{
    xmlrpc_env env;
	xmlrpc_value *out = NULL;
	spSession session = action->get_session();
	spServer server = session->get_server();
	int stat;

    xmlrpc_env_init(&env);

	log_debug(ZONE, "Starting event dispatch for meeting %s", session->get_meetingid());

	const char * partid = "";

	// Build action message.
	if (action->get_event()==PPTAction)
	{
		out = xmlrpc_build_value(&env, "(iiss)", SendPPTShare, action->get_event(),
				session->get_meetingid(),
				action->get_drawing());
	}
	else
	{
		out = xmlrpc_build_value(&env, "(iisssssis)", SendDocShare, action->get_event(),
				session->get_meetingid(),
				partid,
				session->get_docshare_url(),
				action->get_documentid(),
				action->get_pageid(),
				action->get_options(),
				action->get_drawing());
	}
	XMLRPC_FAIL_IF_FAULT(&env);

	// Broadcast message.
	if (action->get_client() == NULL)
	{
		// Get list of clients in meeting
		IList clients;
		stat = mgr.get_participants(session, clients);
		if (stat)
		{
			log_error(ZONE, "List of clients not available for meeting %s", session->get_meetingid());
			goto cleanup;
		}

		// Send message to each client
		IListIterator iter(clients);
		Participant * client = (Participant*)iter.get_first();
		while (client != NULL)
		{
			if (rpc_make_call_array("event", client->get_address(), event_names[action->get_event()], out))
			{
				log_error(ZONE, "Error sending entering doc share event to %s\n", client->get_address());
			}

			client = (Participant*)iter.get_next();
		}
	}
	else
	{
		Participant * client = action->get_client();
		if (rpc_make_call_array("event", client->get_address(), event_names[action->get_event()], out))
		{
			log_error(ZONE, "Error sending entering doc share event to %s\n", client->get_address());
		}
	}

cleanup:
	if (out)
		xmlrpc_DECREF(out);

	xmlrpc_env_clean(&env);
}

int DocShareMgr::lookup_session(const char * meetingid, const char * partid, spSession & session,
				                spParticipant & client)
{
	session = mgr.find_session_by_meetingid(meetingid);
	if (session == NULL)
		return InvalidMeetingId;

	if (partid && *partid)
	{
		client = session->find_participant(partid);
		if (client == NULL)
			return InvalidParticipantId;
	}
	else
		client = NULL;

	return Ok;
}

void DocShareMgr::close_session(Session * session, int reason)
{
	mgr.close_session(session->get_meetingid(), session->get_mode(), reason);
}

void DocShareMgr::run()
{
    while (!shutdown_flag)
    {
        DocShareAction * msg;

        do
        {
            //log_debug(ZONE, "Checking for events");
            msg = (DocShareAction *)queue->pop(EVENT_POLL);
            if (msg != NULL)
            {
                send_action(msg);
                delete msg;
            }
        }
        while (msg != NULL);

        //log_debug(ZONE, "Waiting for async results");
    }
}

/*
 * Doc share event.
 */

DocShareAction::DocShareAction(Session * _session, Participant * _client, int _event, const char *_docid,
                               const char *_pageid, const char *_drawing, int _options) :
   session(_session), client(_client), documentid(_docid), pageid(_pageid), drawing(_drawing)
{
	event = _event;
	options = _options;
}

/*
 * Doc share command processor.
 */

DocShareMode::DocShareMode(DocShareMgr& _mgr) :
	mgr(_mgr)
{
}

int DocShareMode::end_session(const char * sessionid, const char * meetingid, int reason)
{
    log_debug(ZONE, "Ending meeting %s (%d)", meetingid, reason);

	spSession session;
	spParticipant client;
	int stat;

	stat = mgr.lookup_session(meetingid, NULL, session, client);
	if (stat)
		return stat;

	// Dispatch events to clients.
	mgr.dispatch_action(session, NULL, DocShareEnded);

	// and to session manager.
	mgr.close_session(session, ReasonNormal);

    return Ok;
}

// Send reject participant message.
int DocShareMode::reject_participant(const char * sessionid, const char * meetingid, const char * partid, int reason)
{
    log_debug(ZONE, "Dropping participant %s from %s (%d)", partid, meetingid, reason);

	// *** think this is a no-op, we won't allow register of bad meeting ***

    return Ok;
}

// Send enable remote control message.
int DocShareMode::enable_remote_control(const char * sessionid, const char * meetingid, const char * partid, bool enable)
{
    log_debug(ZONE, "Sending enable remote control to %s (%d)", meetingid, enable);

	spSession session;
	spParticipant client;
	int stat;

	stat = mgr.lookup_session(meetingid, partid, session, client);
	if (stat)
		return InvalidMeetingId;

	mgr.dispatch_action(session, client, enable?ControlGranted:ControlRevoked);

    return Ok;
}

// Send enable recording message.
int DocShareMode::enable_recording(const char * sessionid, const char * meetingid, bool enable, int start)
{
    log_debug(ZONE, "Sending doc share enable recording (%d)", meetingid, enable);

	spSession session;
	spParticipant client;
	int stat;

	stat = mgr.lookup_session(meetingid, NULL, session, client);
	if (stat)
		return stat;

	mgr.dispatch_action(session, NULL, enable?RecordingEnabled:RecordingDisabled);

    return Ok;
}

// Notification from session manager that participant has joined.
void DocShareMode::participant_joined(const char * meetingid, const char * partid)
{
    log_debug(ZONE, "Participant %s joined meeting %s", partid, meetingid);

	spSession session;
	spParticipant client;
	int stat;

	stat = mgr.lookup_session(meetingid, partid, session, client);
	if (stat)
		return;

	// Send appropriate event to doc share client.
	if (client->get_type() == ShareRolePresenter)
	{
		mgr.dispatch_action(session, client, PresenterGranted);
	}
	else
	{
		mgr.dispatch_action(session, NULL, DocShareStarted);	// handle late joiner, dispatch event to presneter to reload
	}
}

// Notification from session manager that participant has left.
void DocShareMode::participant_left(const char * meetingid, const char * partid)
{
    log_debug(ZONE, "Participant %s left meeting %s", partid, meetingid);

	spSession session;
	spParticipant client;
	int stat;

	stat = mgr.lookup_session(meetingid, partid, session, client);
	if (stat)
		return;

	// If this was the host, stop sharing and inform session manager.
	if (client->get_type() == ShareRolePresenter)
	{
		mgr.dispatch_action(session, NULL, DocShareEnded);
		mgr.close_session(session, ReasonDisconnected);
	}
}
