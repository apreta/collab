/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

/*
 * Defines APIs exposed through XML Router.
 *
 * Client will connect to XML Router when invoked by doc share protocol.
 * All clients call register_client; host then calls start_doc_share.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
//#include <util.h>

#include "../jcomponent/jcomponent.h"
#include "../jcomponent/jcomponent_ext.h"
#include "../jcomponent/rpc.h"
#include "../jcomponent/util/eventlog.h"
#include "../jcomponent/util/nadutils.h"
#include "../jcomponent/util/log.h"
#include "../jcomponent/util/port.h"
#include "../jcomponent/presence.h"
#include "../util/istring.h"
#include "../util/ithread.h"
#include "../services/common/sharecommon.h"
#include "sharemgr.h"
#include "docshare.h"
#define EXCLUDE_ERRORS
#include "../services/common/common.h"

extern SessionMgr mgr;
extern DocShareMgr docmgr;

extern IString version_file_name;
extern IString recording_base_url;
extern IString doc_base_url;


// cache of info about the update client file
struct update_client_info
{
    IString file_name;          // name of the file (read from jcomponent.xml)
    time_t  last_modified;      // time of last mod
    IString version;            // string version of client available from install
    int     major;              // int values of the version
    int     minor;
    int     build;
    int     platform;
    IString installer_url;      // url to the client installer
    IString message;            // an optional message to be included in the user dialog
    int     type;               // type of upgrade (1=MANDATORY, 2=OPTIONAL)
};

update_client_info g_update_client_info;


// checks the client update file to make sure cache is up to date
// returns false if error
bool get_client_update_info()
{
    struct stat buf;
    int stat_result;
    char seps[] = "=;";
    char seps2[] = ",";
    char version_copy[80];

    stat_result = stat(g_update_client_info.file_name, &buf);
    if (stat_result == -1) // error
    {
        log_error(ZONE, "check_version: couldn't stat %s : %s\n", (const char *)g_update_client_info.file_name, strerror(errno));
        return false;
    }
    if (difftime(buf.st_mtime, g_update_client_info.last_modified) != 0.0)
    {
        // open file
        FILE *f;

        f = fopen(g_update_client_info.file_name, "r");
        if (f == NULL)
        {
            log_error(ZONE, "check_version: couldn't open %s for reading: %s\n", (const char *)g_update_client_info.file_name, strerror(errno));
            return false;
        }

        // read in version, message, and url
        g_update_client_info.type = UpgradeNotFound;
        char *token;

        char read_line[256];
        char *parse_buff = NULL;

        while (fgets(read_line, sizeof(read_line), f) != NULL)
        {
            token = strtok_r(read_line, seps, &parse_buff);
            if (strcmp(token, "UPGRADE_VERSION") == 0)
            {
                token = strtok_r(NULL, seps, &parse_buff);
                g_update_client_info.version = IString(token);
            }
            else if (strcmp(token, "UPGRADE_MSG") == 0)
            {
                token = strtok_r(NULL, seps, &parse_buff);
                g_update_client_info.message = IString(token);
            }
            else if (strcmp(token, "UPGRADE_URL") == 0)
            {
                token = strtok_r(NULL, seps, &parse_buff);
                g_update_client_info.installer_url = IString(token);
            }
            else if (strcmp(token, "UPGRADE_TYPE") == 0)
            {
                token = strtok_r(NULL, seps, &parse_buff);
                if (strcmp(token, "MANDATORY") == 0)
                    g_update_client_info.type = UpgradeMandatory;
                else if (strcmp(token, "OPTIONAL") == 0)
                    g_update_client_info.type = UpgradeOptional;
            }
        }

        fclose(f);

        // now parse version into major, minor, build, platform
        strcpy(version_copy,(const char*)g_update_client_info.version);

        // major
        token = strtok_r(version_copy, seps2, &parse_buff);
        if (token != NULL)
            g_update_client_info.major = atoi(token);
        else
        {
            log_error(ZONE, "check_version: error parsing major number\n");
            return false;
        }

        // minor
        token = strtok_r(NULL, seps2, &parse_buff);
        if (token != NULL)
            g_update_client_info.minor = atoi(token);
        else
        {
            log_error(ZONE, "check_version: error parsing minor number\n");
            return false;
        }

        // build
        token = strtok_r(NULL, seps2, &parse_buff);
        if (token != NULL)
            g_update_client_info.build = atoi(token);
        else
        {
            log_error(ZONE, "check_version: error parsing build number\n");
            return false;
        }

        // platform
        token = strtok_r(NULL, seps2, &parse_buff);
        if (token != NULL)
            g_update_client_info.platform = atoi(token);
        else
        {
            log_error(ZONE, "check_version: error parsing platform number\n");
            return false;
        }

        g_update_client_info.last_modified = buf.st_mtime;
    }

    return true;
}


// Verify that the client version is up to date.  Return info about
// possible upgrades.
// Parameters:
//   CLIENTVERSION
//
// Returns:
//   UPGRADEFOUND, UPGRADEURL, UPGRADEMESSAGE
//   UPGRADEFOUND: 1 = mandatory upgrade found, 2 = optional upgrade found, 0 = not found
xmlrpc_value *check_version(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *client_version;
    char *token;
    const char *seps = ",";
    int found = UpgradeNotFound;
    int major, minor, build, platform;
    xmlrpc_value *output = NULL;
	char *parse_buff = NULL;

    xmlrpc_parse_value(env, param_array, "(s)", &client_version);
    if (env->fault_occurred)
    {
		xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to addressbk.check_version");
        goto cleanup;
    }

    // read server's client version file
    g_update_client_info.file_name = version_file_name;
    if (g_update_client_info.file_name.length() == 0)
    {
        log_error(ZONE, "check_version: zero length upgrade file name\n");
        xmlrpc_env_set_fault(env, Failed, "Unable to read client version file parameter");
        goto cleanup;
    }

    if (!get_client_update_info())
    {
        xmlrpc_env_set_fault(env, Failed, "Unable to read upgrade file for check_version");
        goto cleanup;
    }

    // compare server version number to passed client version
    // major
    token = strtok_r(client_version, seps, &parse_buff);
    if (token != NULL)
        major = atoi(token);
    else
    {
        xmlrpc_env_set_fault(env, Failed, "Error parsing major number");
        goto cleanup;
    }

    // minor
    token = strtok_r(NULL, seps, &parse_buff);
    if (token != NULL)
        minor = atoi(token);
    else
    {
        xmlrpc_env_set_fault(env, Failed, "Error parsing minor number");
        goto cleanup;
    }

    // build
    token = strtok_r(NULL, seps, &parse_buff);
    if (token != NULL)
        build = atoi(token);
    else
    {
        xmlrpc_env_set_fault(env, Failed, "Error parsing build number");
        goto cleanup;
    }

    // platform
    token = strtok_r(NULL, seps, &parse_buff);
    if (token != NULL)
        platform = atoi(token);
    else
    {
        xmlrpc_env_set_fault(env, Failed, "Error parsing platform number");
        goto cleanup;
    }

    // compare versions.  this algorithm will not require an upgrade
    // if for some reason the version on the server is older than
    // the client version.
    if (g_update_client_info.major > major ||
        (g_update_client_info.major == major && g_update_client_info.minor > minor) ||
        (g_update_client_info.major == major && g_update_client_info.minor == minor && g_update_client_info.build > build) ||
        (g_update_client_info.major == major && g_update_client_info.minor == minor && g_update_client_info.build == build && g_update_client_info.platform > platform))
    {
        found = g_update_client_info.type;
    }

    // read in message to return
    // Create return value.
    output = xmlrpc_build_value(env, "(iss)", found,
                                (const char*) g_update_client_info.installer_url,
                                (const char*) g_update_client_info.message);
    XMLRPC_FAIL_IF_FAULT(env);


cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to check client version");

    return output;
}


// Register client
// Parameters:
//   TOKEN, MEETINGID, PARTICIPANTID, PRESENTER, MODE
// Returns:
//   SESSIONID, DOCSERVER, status (ok, rejected)
xmlrpc_value *register_client(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *token, *meetingid, *partid, *address;
    xmlrpc_value *output = NULL;
    int status, is_presenter, share_mode;
	spSession session;
	char reflector[128];
	const char * from;

    xmlrpc_parse_value(env, param_array, "(sssii*)", &token, &meetingid, &partid, &is_presenter, &share_mode);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.register_client");
        goto cleanup;
    }

	from = rpc_get_from_full((rpc_session_t)user_data);

	status = mgr.register_client(token, meetingid, partid, is_presenter, from, session, share_mode);
    if (status)
    {
        xmlrpc_env_set_fault(env, status, "Unable to register doc share client");
        goto cleanup;
    }

    // Create return value.
	sprintf(reflector, "%s::%d", session->get_server()->get_reflector(), mgr.get_share_port());
    output = xmlrpc_build_value(env, "(ssssi)", session->get_sessionid(),
		session->get_server()->get_repository(),
		reflector,
		"imidio",	// reflector password
		status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to register doc share client");

    return output;
}


// Start doc share session.
// Parameters:
//   SESSIONID, MEETINGID, ADDRESS (ip:port), START_TIME
// Returns:
//   MEETINGID, status (ok, pending, retry)
xmlrpc_value *start_doc_share(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, * address;
    xmlrpc_value *output = NULL;
    int status, share_starttime;
	const char * from;

    xmlrpc_parse_value(env, param_array, "(sssi)", &sessionid, &meetingid, &address, &share_starttime);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.start_doc_share");
        goto cleanup;
    }

	from = rpc_get_from_full((rpc_session_t)user_data);

    status = docmgr.start_doc_share(sessionid, meetingid, from, address, share_starttime);
    if (status)
    {
        xmlrpc_env_set_fault(env, status, "Unable to start doc share session");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to start doc share session");

    return output;
}


// End doc share session.
// Parameters:
//   SESSIONID, MEETINGID
// Returns:
//   status (ok, pending, retry)
xmlrpc_value *end_doc_share(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid;
    xmlrpc_value *output = NULL;
    int status;
	const char * from;

    xmlrpc_parse_value(env, param_array, "(ss)", &sessionid, &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.end_doc_share");
        goto cleanup;
    }

	from = rpc_get_from_full((rpc_session_t)user_data);

    status = docmgr.end_doc_share(sessionid, meetingid, from);
    if (status)
    {
        xmlrpc_env_set_fault(env, status, "Unable to end doc share session");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to end doc share session");

    return output;
}


// get_doc_share_server.
// Parameters:
//   SESSIONID, MEETINGID
// Returns:
//   SERVER, status (ok, pending, retry)
xmlrpc_value *get_doc_share_server(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid;
    xmlrpc_value *output = NULL;
    int status = Ok;
	const char* server;
	spSession session;

    xmlrpc_parse_value(env, param_array, "(ss)", &sessionid, &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.get_doc_share_server");
        goto cleanup;
    }

    server = doc_base_url;

    // Create return value.
    output = xmlrpc_build_value(env, "(is)", status, server);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to get_doc_share_server");

    return output;
}


// check_meeting_running
// Parameters:
//   MEETINGID
// Returns:
//   status (ok, pending, retry)
xmlrpc_value *check_meeting_running(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *meetingid;
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(s)", &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.check_meeting_running");
        goto cleanup;
    }

	status = mgr.check_session(meetingid);

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to check_meeting_running");

    return output;
}


// download_document_page.
// Parameters:
//   SESSIONID, MEETINGID, DOCID, PAGEID
// Returns:
//   status (ok, pending, retry)
xmlrpc_value *download_document_page(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *docid, *pageid;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(ssss)", &sessionid, &meetingid, &docid, &pageid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.download_document_page");
        goto cleanup;
    }

	status = docmgr.download_document_page(sessionid, meetingid, docid, pageid);
    if (status)
    {
        xmlrpc_env_set_fault(env, status, "Unable to download_document_page");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to download_document_page");

    return output;
}


// configure_whiteboard.
// Parameters:
//   SESSIONID, MEETINGID, options
// Returns:
//   MEETINGID, status (ok, pending, retry)
xmlrpc_value *configure_whiteboard(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid;
    xmlrpc_value *output = NULL;
	int options;
    int status;

    xmlrpc_parse_value(env, param_array, "(ssi)", &sessionid, &meetingid, &options);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.configure_whiteboard");
        goto cleanup;
    }

	status = docmgr.configure_whiteboard(sessionid, meetingid, options);
    if (status)
    {
        xmlrpc_env_set_fault(env, status, "Unable to configure_whiteboard");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to configure_whiteboard");

    return output;
}


// draw_on_whiteboard.
// Parameters:
//   SESSIONID, MEETINGID, DRAWING
// Returns:
//   MEETINGID, status (ok, pending, retry)
xmlrpc_value *draw_on_whiteboard(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *drawing;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(sss)", &sessionid, &meetingid, &drawing);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.configure_whiteboard");
        goto cleanup;
    }

    status = docmgr.draw_on_whiteboard(sessionid, meetingid, drawing);
    if (status)
    {
        xmlrpc_env_set_fault(env, status, "Unable to draw_on_whiteboard");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to draw_on_whiteboard");

    return output;
}


// on_ppt_remote_control
// Parameters:
//   SESSIONID, MEETINGID, ACTION
// Returns:
//   MEETINGID, status (ok, pending, retry)
xmlrpc_value *on_ppt_remote_control(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *action;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(sss)", &sessionid, &meetingid, &action);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.on_ppt_remote_control");
        goto cleanup;
    }

    status = docmgr.on_ppt_remote_control(sessionid, meetingid, action);
    if (status)
    {
        xmlrpc_env_set_fault(env, status, "on_ppt_remote_control failed");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "on_ppt_remote_control failed");

    return output;
}


// *** JComponent callbacks ***

int JC_connected()
{
    log_status(ZONE, "Connected to xml router");
	mgr.reset();
    return 0;
}

void RPC_namespace_callback(const char *ns, const char *id, s2s_t s2s, nad_t nad)
{
    log_debug(ZONE, "Namespace callback received");

    if (strcmp(ns, "iic:session") == 0)
    {
        int query, status;
        bool handled = false;

        query = nad_find_elem(nad, 0, "query", 1);
        if (query >= 0)
        {
            status = nad_find_first_child(nad, query);
            if (status >= 0 && NAD_ENAME_L(nad,status) > 0)
            {
                if (strncmp(NAD_ENAME(nad,status), "endsession", NAD_ENAME_L(nad,status)) == 0)
                {
                    int id_attr = nad_find_attr(nad, status, "id", NULL);

                    if (id_attr >= 0)
                    {
                        IString id(NAD_AVAL(nad, id_attr), NAD_AVAL_L(nad, id_attr));

                        log_debug(ZONE, "Session %s ended", (const char *)id);

						mgr.remove_client(id);
                        handled = true;
                    }
                }
            }
        }

        if (!handled)
            log_error(ZONE, "Invalid session presence packet received, ignoring");
    }
}

// ====
// External controller APIs
// ====

// Register service.
// Parameters:
//   SERVICE_ID, EPOCH
// Returns:
//   0
xmlrpc_value *register_service(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *service_id;
	int epoch;
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(si)", &service_id, &epoch);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.register_service");
        goto cleanup;
    }

    // register service...
//    status = master.register_service(service_id, epoch);
//    if (status != Ok)
//    {
//        xmlrpc_env_set_fault(env, status, "Unable to register service");
//        goto cleanup;
//    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", 0);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to register service");

    return output;
}

// *** Initialization ***

int docshare_init()
{
	jc_set_connected_callback(JC_connected);
    rpc_set_namespace_callback(RPC_namespace_callback);

	docmgr.initialize();

    rpc_add_method("addressbk.check_version",
                   &check_version, NULL);

    rpc_add_method("controller.register_client",
                   &register_client, NULL);
    rpc_add_method("controller.start_doc_share",
                   &start_doc_share, NULL);
    rpc_add_method("controller.end_doc_share",
                   &end_doc_share, NULL);
    rpc_add_method("controller.get_doc_share_server",
                   &get_doc_share_server, NULL);

    rpc_add_method("controller.download_document_page",
                   &download_document_page, NULL);
    rpc_add_method("controller.configure_whiteboard",
                   &configure_whiteboard, NULL);
    rpc_add_method("controller.draw_on_whiteboard",
                   &draw_on_whiteboard, NULL);

    rpc_add_method("controller.on_ppt_remote_control",
                   &on_ppt_remote_control, NULL);

    rpc_add_method("controller.check_meeting_running",
                   &check_meeting_running, NULL);

    rpc_add_method("controller.register_service", &register_service, NULL);

    return 0;
}
