/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef APPSHARE_H
#define APPSHARE_H

#include "../util/asynccomm/comm.h"
#include "sharemgr.h"

class AppShareMode : public Mode, public CommEvent
{
	SessionMgr& mgr;
    CommClient comm;

public:
	AppShareMode(SessionMgr& _mgr);
	
	int initialize(const char * server, int port);
	int shutdown();

	// Send commands to reflector.
    int end_session(const char * sessionid, const char * meetingid, int reason);
    int reject_participant(const char * sessionid, const char * meetingid, const char * partid, int reason);
    int enable_remote_control(const char * sessionid, const char * meetingid, const char * partid, bool enable);
	int enable_recording(const char * sessionid, const char * meetingid, bool enable, int start);	

	void participant_joined(const char * meetingid, const char * partid);
    void participant_left(const char * meetingid, const char * partid);

    // Handle events from reflector.
	void on_participant_joined(const char * token, const char * meetingid, const char * partid, bool host);
	void on_participant_left(const char * meetingid, const char * partid, int reason);
	void on_end_session(const char * meetingid, int reason);
    bool on_message(CommConnection * c, CommMessage * msg);
};

#endif
