archive=$1

bins="iic/bin/addressd iic/bin/controllerd iic/bin/nmsvoiced \
      iic/bin/reflectord iic/bin/mailerd iic/bin/nanny iic/bin/jadc2s \
      iic/bin/proxyd iic/bin/mod_meeting.so iic/bin/mod_jabapi.so"

files="iic/bin/*.xml iic/bin/client_upgrade_file.imi \
       iic/bin/hosts iic/bin/passwd iic/conf/* \
       iic/lib/libsqldb.so \
       iic/lib/libjwsmtp*.so.* iic/webcontent/* \
       iic/bin/imidio_new_user_template.txt \
       iic/prompt/production/*.wav"

if [ "$archive" == "" ]; then 
  archive=iic-2-services.tar
fi

echo "Creating staging area"
rm -f -R staging
mkdir staging
mkdir staging/iic
mkdir staging/iic/bin
mkdir staging/iic/log
mkdir staging/iic/cdr
mkdir staging/iic/lib
mkdir staging/iic/conf
mkdir staging/iic/webcontent
mkdir staging/iic/prompt
mkdir staging/iic/prompt/production

echo "Moving files"
for file in $bins; do
  cp ../$file staging/$file
  strip -d staging/$file
done

for file in $files; do
  path="`expr "$file" : '\(.*\)/'`"
  cp -r ../$file staging/$path/
done

cp -d /usr/local/lib/libxmlrpc*.so* staging/iic/lib
cp -d /usr/local/lib/libjwsmtp.so* staging/iic/lib
cp /opt/iic/httpd/modules/mod_meeting.so staging/iic/bin
cp /opt/iic/httpd/modules/mod_jabapi.so staging/iic/bin

echo "Creating archive $archive"
cd staging
rm -rf `find -name 'CVS' -type d -print`
tar -czf ../$archive.gz --exclude=CVS * 
cd ..

echo "Done."
