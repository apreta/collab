/* --------------------------------------------------------------------------
 *
 * License
 *
 * The contents of this file are subject to the Jabber Open Source License
 * Version 1.0 (the "JOSL").  You may not copy or use this file, in either
 * source code or executable form, except in compliance with the JOSL. You
 * may obtain a copy of the JOSL at http://www.jabber.org/ or at
 * http://www.opensource.org/.  
 *
 * Software distributed under the JOSL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the JOSL
 * for the specific language governing rights and limitations under the
 * JOSL.
 *
 * Copyrights
 * 
 * Portions created by or assigned to Jabber.com, Inc. are 
 * Copyright (c) 1999-2002 Jabber.com, Inc.  All Rights Reserved.  Contact
 * information for Jabber.com, Inc. is available at http://www.jabber.com/.
 *
 * Portions Copyright (c) 1998-1999 Jeremie Miller.
 * 
 * Acknowledgements
 * 
 * Special thanks to the Jabber Open Source Contributors for their
 * suggestions and support of Jabber.
 * 
 * Alternatively, the contents of this file may be used under the terms of the
 * GNU General Public License Version 2 or later (the "GPL"), in which case
 * the provisions of the GPL are applicable instead of those above.  If you
 * wish to allow use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the JOSL,
 * indicate your decision by deleting the provisions above and replace them
 * with the notice and other provisions required by the GPL.  If you do not
 * delete the provisions above, a recipient may use your version of this file
 * under either the JOSL or the GPL.
 * suggestions and support of Jabber.
 *
 * --------------------------------------------------------------------------*/

#ifdef _WIN32
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#endif

#include "jcomponent.h"
#include "util/log.h"

/* handle new elements */
void _connect_startElement(void *arg, const char* name, const char** atts)
{
    conn_t c = (conn_t)arg;
    int i = 0;
    char* xmlns_val = NULL;

    /* track how far down we are in the xml */
    c->depth++;

    /* process stream header first */
    if(c->depth == 1)
    {
        char buf[128];
        /* Extract stream ID and generate a key to hash */
        snprintf(buf,128,"<handshake>%s</handshake>",shahash(spools(c->p, j_attr(atts, "id"), c->s2s->secret, c->p)));
        /* write the handshake */
        log_debug(ZONE,"handshaking with sm: %s",buf);
        _write_actual(c, c->fd,buf,strlen(buf));
		
        return;
    }

    /* these are the top level packets, usually route, and where we really start processing the xml */
    if(c->depth == 2)
    {
        /* always check for the return handshake :) */
        if(c->state != state_OPEN)
        {
            if(j_strcmp(name,"handshake") == 0)
            {
                c->state = state_OPEN;
                log_debug(ZONE,"handshake accepted, we're connected to the sm");
                sm_connected(1);
            }
            return;
        }

		// Depending on packet type, create nad or write into chunk
		// *** Change to use nad in all cases
		if (j_strcmp(name, "route") == 0 || j_strcmp(name, "presence") == 0)
		{
			/* make a new nad if we don't already have one */
			if(c->nad == NULL)
				c->nad = nad_new(c->nad_cache);

			c->chunk = NULL;
		}
		else
		{
            // create both nad and a chunk for this packet
            // which one to use will be resolved when we read the xmlns attr
			if(c->nad == NULL)
				c->nad = nad_new(c->nad_cache);

			/* create a chunk for this packet */
			c->chunk = chunk_new(chunk_PACKET,j_attr(atts,"to"),j_attr(atts,"from"));
			c->chunk->id = pstrdup(c->chunk->p, j_attr(atts, "id"));

			/* flag it if it's an error */
			if(j_strcmp(j_attr(atts,"type"),"error") == 0)
				c->chunk->type = chunk_ERR;

			c->chunk->iq_type = pstrdup(c->chunk->p, j_attr(atts,"type"));

            // hold off on NULLing this out until xmlns is resolved
			// c->nad = NULL;
		}
    }

	/* at this point, we need to have a chunk and be inside a route to do anything */
	if(c->depth < 2) return;

    // if both nad and chunk are valid continue to add to both
    if (c->nad != NULL && c->chunk != NULL)
    {
		/* append new element data to nad */
		nad_append_elem(c->nad, (char *) name, c->depth - 1);
		i = 0;
		while(atts[i] != NULL)
		{
			nad_append_attr(c->nad, (char *) atts[i], (char *) atts[i + 1]);
			i += 2;
		}

		/* if there was a tag before us, close it properly */
		if(c->chunk->eflag == 1)
			spool_add(c->chunk->regen,">");
	
		/* recreate element and push onto chunk */
		spooler(c->chunk->regen,"<",name,c->chunk->regen);
        i = 0;
		while(atts[i] != '\0')
		{
			spool_add(c->chunk->regen," ");
			spool_add(c->chunk->regen,(char*)atts[i]);
			spool_add(c->chunk->regen,"='");
			spool_escape(c->chunk->regen,(char*)atts[i+1],strlen((char*)atts[i+1]));
			spool_add(c->chunk->regen,"'");
			i += 2;
		}
	
		/* somebody has to close this element */
		c->chunk->eflag = 1;

        /* let's see if there is an xmlns attr to help make the decision */
        xmlns_val = j_attr(atts, "xmlns");
        if (xmlns_val != NULL)
        {
            // see if it's type register
            if (strstr(xmlns_val, "register") || 
                strcmp(xmlns_val, "iic:service") == 0 ||
                strcmp(xmlns_val, "iic:health") == 0 ||
                strcmp(xmlns_val, "iic:session") == 0)
			{
				pool_free(c->chunk->p);
                c->chunk = NULL;
			}
            else
			{
				nad_free(c->nad);
                c->nad = NULL;
			}
        }
    }
    else if (c->nad != NULL)
	{
		/* append new element data to nad */
		nad_append_elem(c->nad, (char *) name, c->depth - 1);
		i = 0;
		while(atts[i] != NULL)
		{
			nad_append_attr(c->nad, (char *) atts[i], (char *) atts[i + 1]);
			i += 2;
		}
	}
	else if (c->chunk != NULL)
	{
		/* if there was a tag before us, close it properly */
		if(c->chunk->eflag == 1)
			spool_add(c->chunk->regen,">");
	
		/* recreate element and push onto chunk */
		spooler(c->chunk->regen,"<",name,c->chunk->regen);
		while(atts[i] != '\0')
		{
			spool_add(c->chunk->regen," ");
			spool_add(c->chunk->regen,(char*)atts[i]);
			spool_add(c->chunk->regen,"='");
			spool_escape(c->chunk->regen,(char*)atts[i+1],strlen((char*)atts[i+1]));
			spool_add(c->chunk->regen,"'");
			i += 2;
		}
	
		/* somebody has to close this element */
		c->chunk->eflag = 1;
	}
}

void _connect_endElement(void *arg, const char* name)
{
    conn_t c = (conn_t)arg;

    /* complete the chunk, if any */
    if(c->chunk != NULL && c->depth >= 2)
    {
        /* no cdata means tiny tag */
        if(c->chunk->eflag == 1)
            spool_add(c->chunk->regen,"/>");
        else
            spooler(c->chunk->regen,"</",name,">",c->chunk->regen);
        c->chunk->eflag = 0;
    }

    /* now we have to do something with our chunk */
    if(c->chunk != NULL && c->depth == 2)
    {
        log_debug(ZONE,"sm sent us a chunk for %s",jid_full(c->chunk->to));

        /* route it if it's a good element, else bounce */
        if(j_strcmp(name,"message") == 0 || j_strcmp(name,"iq") == 0)
		{
			process_packet(c->s2s, c->chunk);
		}
        else
            conn_chunk_write(c, chunk_prep(c->chunk, "Invalid Packet"));
        c->chunk = NULL;
    }

	
	if (c->nad != NULL && c->depth == 2)
	{
		if (!j_strcmp(name, "route"))
			process_route(c->s2s, c->nad);
		else if (!j_strcmp(name, "presence"))
			process_presence(c->s2s, c->nad);
        else if (!j_strcmp(name, "iq"))
            process_request(c->s2s, c->nad);
		else
		{
			log_debug(ZONE, "packet %s received and ignored", name);
			nad_free(c->nad);
		}
		c->nad = NULL;
	}
	
    /* going up for air */
    c->depth--;

    /* if we processed the closing stream root, flag to close l8r */
    if(c->depth == 0)
        c->depth = -1; /* we can't close here, expat gets free'd on close :) */
}


void _connect_charData(void *arg, const char *str, int len)
{
    conn_t c = (conn_t)arg;

    /* no chunk? no cdata */
//    if(c->chunk == NULL) return;

    if (c->chunk != NULL)
    {
        /* be sure to end the opening tag */
        if(c->chunk->eflag == 1)
        {
            spool_add(c->chunk->regen, ">");
            c->chunk->eflag = 0;
        }
        
        /* expat unescapes, we re-escape, *sigh* */
        spool_escape(c->chunk->regen, (char*)str, len);
    }
    
	if (c->nad != NULL)
    {
        nad_append_cdata(c->nad, (char*)str, len, c->depth);
    }
}

/* internal handler to read incoming data from the sm and parse/process it */
int _connect_io(mio_t m, mio_action_t a, int fd, void *data, void *arg)
{
    char buf[1024]; /* !!! make static when not threaded? move into conn_st? */
    int len, ret;
    s2s_t s2s = (s2s_t)arg;

    log_debug(ZONE,"io action %d with fd %d",a,fd);

    switch(a)
    {
    case action_READ:

        /* read as much data as we can from the sm */
        while(1)
        {
			//conn_lock(s2s->sm);
            len = _read_actual(fd, buf, 1024);
			//conn_unlock(s2s->sm);
            if((ret = conn_read(s2s->sm, buf, len)) != 1 || len < 1024) break;
        }
        return 1;

    case action_WRITE:

        /* let's break this into another function, it's a bit messy */
        return conn_write(s2s->sm);

    case action_CLOSE:

        /* if we're closing before we're open, we've got issues */
        if(s2s->sm->state != state_OPEN)
        {
            /* !!! handle this better */
            log_error(ZONE, "secret is wrong or sm kicked us off for some other reason");
        }

        /* clean up queue, re-delivering packets or dropping */
        conn_flush(s2s->sm);
        conn_free(s2s->sm);
        s2s->sm = NULL;
        break;

    case action_ACCEPT:
        break;
    }
    return 0;
}

void  connect_close(s2s_t s2s)
{
    if (s2s != NULL && s2s->sm != NULL)
        mio_close(s2s->mio, s2s->sm->fd);
}

/* create a new conn to the sm */
conn_t connect_new(s2s_t s2s, char *ip, int port, char *id)
{
    int fd;
    conn_t c;
    chunk_t chunk;
    char iphost[16];
    struct hostent *h;

    /* !!! should be tracking the number/timing of successive fails */
	static int last_attempt = 0;
	int now = time(NULL);
	if (now - last_attempt < CONNECT_RETRY_PAUSE)
	{
#ifdef LINUX
		log_debug(ZONE, "Sleeping before reconnect attempt");
		struct timespec tv = { CONNECT_RETRY_PAUSE - (now - last_attempt), 0 };
		nanosleep(&tv, NULL);
#else
		Sleep((CONNECT_RETRY_PAUSE - (now - last_attempt)) * 1000);
#endif
	}
	last_attempt = now;
	
    /* do we really want a blocking dns lookup here? */
    if((h = gethostbyname(ip)) == NULL)
    {
        log_error(ZONE, "dns lookup for %s failed: %d", ip, h_errno);
        exit(1);
    }
#ifdef LINUX
    inet_ntop(AF_INET, h->h_addr_list[0], iphost, 16);
#else // _WIN32
	strncpy(iphost, inet_ntoa(*(struct in_addr *)h->h_addr_list[0]), 16);
#endif
    log_debug(ZONE,"connecting to sm at %s:%d as %s",iphost,port,id);

    /* try to connect */
    if((fd = mio_connect(s2s->mio, port, iphost, _connect_io, (void*)s2s)) <= 0) return NULL;

    /* make our conn_t from this */
    c = conn_new(s2s, fd);
    mio_read(s2s->mio,fd);

    /* set up expat callbacks */
    XML_SetUserData(c->expat, (void*)c);
    XML_SetElementHandler(c->expat, (void*)_connect_startElement, (void*)_connect_endElement);
    XML_SetCharacterDataHandler(c->expat, (void*)_connect_charData);

    /* write header */
    chunk = chunk_new(chunk_HEAD,NULL,NULL);
    spooler(chunk->regen,"<stream:stream xmlns='jabber:component:accept' xmlns:stream='http://etherx.jabber.org/streams' to='",id,"'>",chunk->regen);
    conn_chunk_write(c,chunk_prep(chunk,NULL));

    return c;
}


