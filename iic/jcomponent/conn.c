/* --------------------------------------------------------------------------
 *
 * License
 *
 * The contents of this file are subject to the Jabber Open Source License
 * Version 1.0 (the "JOSL").  You may not copy or use this file, in either
 * source code or executable form, except in compliance with the JOSL. You
 * may obtain a copy of the JOSL at http://www.jabber.org/ or at
 * http://www.opensource.org/.  
 *
 * Software distributed under the JOSL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the JOSL
 * for the specific language governing rights and limitations under the
 * JOSL.
 *
 * Copyrights
 * 
 * Portions created by or assigned to Jabber.com, Inc. are 
 * Copyright (c) 1999-2002 Jabber.com, Inc.  All Rights Reserved.  Contact
 * information for Jabber.com, Inc. is available at http://www.jabber.com/.
 *
 * Portions Copyright (c) 1998-1999 Jeremie Miller.
 * 
 * Acknowledgements
 * 
 * Special thanks to the Jabber Open Source Contributors for their
 * suggestions and support of Jabber.
 * 
 * Alternatively, the contents of this file may be used under the terms of the
 * GNU General Public License Version 2 or later (the "GPL"), in which case
 * the provisions of the GPL are applicable instead of those above.  If you
 * wish to allow use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the JOSL,
 * indicate your decision by deleting the provisions above and replace them
 * with the notice and other provisions required by the GPL.  If you do not
 * delete the provisions above, a recipient may use your version of this file
 * under either the JOSL or the GPL. 
 * 
 * --------------------------------------------------------------------------*/

#ifdef _WIN32
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#endif

#include "jcomponent.h"
#include "util/log.h"
#include "util/port.h"

/* this file contains some simple utils for the conn_t data type */

/* create a new blank conn (!caller must set expat callbacks and mio afterwards) */
conn_t conn_new(s2s_t s2s, int fd)
{
    pool p;
    conn_t c;

    log_debug(ZONE,"making new conn on %d",fd);

    /* conns use small memory pools (for convenience) */
    p = pool_new();
    c = (conn_t)pmalloco(p,sizeof(struct conn_st));
    c->p = p;

    /* set up some basic defaults */
    c->s2s = s2s;
    c->fd = fd;
    c->state = state_NONE;
    c->start = c->activity = time(NULL);
    c->expat = XML_ParserCreate(NULL);

    g_static_rec_mutex_init(&c->queue_mutex);

    /* hash table used for tracking authorized dialback names per conn */
    c->db = xhash_new(7);

	c->nad_cache = nad_cache_new();
	
    return c;
}

/* free up memory (!caller must process anything in the writeq) */
void conn_free(conn_t c)
{
    log_debug(ZONE,"freeing old conn for %d",c->fd);

    /* other conn vars cleanup */
    if(c->chunk != NULL) pool_free(c->chunk->p);
    XML_ParserFree(c->expat);
    xhash_free(c->db);
	nad_cache_free(c->nad_cache);
    pool_free(c->p);
}

void conn_lock(conn_t c)
{
	log_debug(ZONE, "locking connection %d", c->fd);

    g_static_rec_mutex_lock(&c->queue_mutex);
}

void conn_unlock(conn_t c)
{
	log_debug(ZONE, "unlocking connection %d", c->fd);

    g_static_rec_mutex_unlock(&c->queue_mutex);
}

/* clean up writeq, re-delivering packets or dropping */
void conn_flush(conn_t c)
{
    chunk_t cur;

    conn_lock(c);
    while((cur = c->writeq) != NULL)
    {
        c->writeq = cur->next;

        if(cur->type == chunk_PACKET)
            route_app(c->s2s,chunk_prep(cur,NULL));
        else
            pool_free(cur->p);
    }
    conn_unlock(c);
}


/* create a new chunk */
chunk_t chunk_new(chunk_type_t type, char *to, char *from)
{
    pool p;
    chunk_t chunk;

    log_debug(ZONE,"creating new chunk %d",type);

    /* chunks use memory pools */
    p = pool_heap(1024);
    chunk = (chunk_t)pmalloco(p, sizeof(struct chunk_st));

    /* set up the vars for this new chunk */
    chunk->p = p;
    chunk->type = type;
    chunk->regen = spool_new(p);

    /* if a to/from were given, jidize them */
    chunk->to = jid_new(p,to);
    chunk->from = jid_new(p,from);

    /* make the key used for dialback stuff */
    if(chunk->to != NULL && chunk->from != NULL)
        chunk->dbkey = spools(p,chunk->to->server,"/",chunk->from->server,p);

    return chunk;
}

/* prepares a chunk to be written, optionally inserting an error */
chunk_t chunk_prep(chunk_t chunk, char *err)
{
    char *pkt, *chr, *type;

    /* first just serialize the spool */
    chunk->data = chunk->wcur = spool_print(chunk->regen);

    /* almost impossible, but just in case, bail */
    if((chunk->type == chunk_PACKET && (chunk->to == NULL || chunk->from == NULL)) || chunk->data == NULL)
    {
        pool_free(chunk->p);
        return NULL;
    }

    /* we have to bend over backwards to handle "error" bounces */
    if(err != NULL)
    {

        log_debug(ZONE,"preparing an error: %s",err);

        /* well, if it's already an error, we can't bounce, drop */
        if(chunk->type == chunk_ERR)
        {
            log_debug(ZONE,"already an error, dropping");
            pool_free(chunk->p);
            return NULL;
        }

        /* because the xml is just one big string, this really REALLY sucks, but is rare and works */
        /* !!! someone double-check this logic */

        /* make a bigger block copy that we have room to move around in */
        pkt = pmalloc(chunk->p,strlen(chunk->data) + strlen(err) + 128);
        memcpy(pkt,chunk->data,strlen(chunk->data) + 1);

        /* find the from and replace with a spacey to */
        chr = strstr(pkt," from='"); /* yes, it has to be there */
        memcpy(chr,"  to ",5);

        /* find the to now, make more space, and replace with the from */
        chr = strstr(pkt," to='"); /* yes, it has to be there */
        memmove(chr+2,chr,strlen(chr)+1);
        memcpy(chr," from",5);

        /* look for an existing type and change/insert */
        if((type = strstr(pkt," type='")) != NULL && type < strchr(pkt,'>'))
        {
            /* find the closing ', resize, and replace */
            chr = strchr(type+7,'\'');
            memmove(type+12,chr,strlen(chr)+1);
            memcpy(type+7,"error",5);
        }else{
            /* insert it before the from above */
            memmove(chr+13,chr,strlen(chr)+1);
            memcpy(chr," type='error'",13);
        }

        /* this is the harder part, work around /> vs >...</ elements */
        chr = strchr(pkt,'>'); /* yup, it's gotta be there too */
        if(*(chr-1) == '/')
        {
            int namelen;

            /* crap, have to close the tag too */
            namelen = (strchr(pkt,' ') - pkt) - 1;
            memmove(chr+namelen+2,chr,strlen(chr)+1);
            memcpy(chr-1,"></",3);
            memcpy(chr+2,pkt+1,namelen);
        }else{
            chr++;
        }

        /* just make space and plug it in */
        memmove(chr+strlen(err)+26,chr,strlen(chr)+1);
        memcpy(chr,"<error code='502'>",18);
        memcpy(chr+18,err,strlen(err));
        memcpy(chr+strlen(err)+18,"</error>",8);

        /* all done, error'd and addressed */
        chunk->data = chunk->wcur = pkt;
    }

    chunk->wlen = strlen(chunk->data);

    log_debug(ZONE,"chunk prepared: %s",chunk->data);

    return chunk;
}

/* write a chunk to a conn */
void conn_chunk_write(conn_t c, chunk_t chunk)
{
    if(chunk == NULL) return;

    if (c == NULL) 
    {
        log_debug(ZONE, "write to closed connection, ignored");
        pool_free(chunk->p);
        return;
    }

    log_debug(ZONE,"writing a chunk to %d",c->fd);

    /* make sure it's not going anywhere */
    chunk->next = NULL;

    conn_lock(c);

    /* append to the outgoing write queue, if any */
    if(c->qtail == NULL)
    {
        c->qtail = c->writeq = chunk;
    }else{
        c->qtail->next = chunk;
        c->qtail = chunk;
    }

    conn_unlock(c);

    /* tell mio to process write events on this fd */
    // mio_setwrite(c->s2s->mio, c->fd);   // ** SIG
    mio_write(c->s2s->mio, c->fd);
}

/* write errors out and close streams */
void conn_close(conn_t c, char *err)
{
    if(c == NULL) return;
    log_debug(ZONE,"closing stream with error: %s",err);
    _write_actual(c, c->fd, "<stream:error>",14);
    if(err != NULL)
        _write_actual(c, c->fd, err, strlen(err));
    else
        _write_actual(c, c->fd, "Unknown Error", 13);
    _write_actual(c, c->fd, "</stream:error></stream:stream>",31);
    mio_close(c->s2s->mio, c->fd); /* remember, c is gone after this, re-entrant */
}

/* process the xml data that's been read */
int conn_read(conn_t c, char *buf, int len)
{
    char *err = NULL;

    /* client gone */
    if(len == 0)
    {
        mio_close(c->s2s->mio, c->fd);
        return 0;
    }

    /* deal with errors */
    if(len < 0)
    {
        if(_error == _EWOULDBLOCK || _error == _EINTR || errno == EAGAIN)
            return 2; /* flag that we're blocking now */

        log_error(ZONE, "Unexpected connection error: %d - %d\n", errno, CHECK_ERR);

        mio_close(c->s2s->mio, c->fd);
        return 0;
    }

    log_debug(ZONE,"processing read data from %d: %.*s", c->fd, len, buf);

    /* parse the xml baby */
    if(!XML_Parse(c->expat, buf, len, 0))
    {
        err = (char *)XML_ErrorString(XML_GetErrorCode(c->expat));
    }else if(c->chunk != NULL && c->chunk->regen->len > MAXSIZE){
        err = MAXSIZE_ERR;
    }else if(c->depth > MAXDEPTH){
        err = MAXDEPTH_ERR;
    }

    /* oh darn */
    if(err != NULL)
    {
        conn_close(c, err);
        return 0;
    }

    /* if we got </stream:stream>, this is set */
    if(c->depth < 0)
    {
        _write_actual(c, c->fd, "</stream:stream>", 16);
        mio_close(c->s2s->mio, c->fd);
        return 0;
    }

    /* get more read events */
    return 1;
}

/* internally called by conn_write to pre-process the ->writeq */
/* connection must be locked on entry */
int _conn_writechunk(conn_t c)
{
    chunk_t cnew, cur, last, next;
    int db;

    if(c->writeq == NULL) return 0;

    /* first, is the current chunk already partially written? */
    if(c->writeq->wcur != c->writeq->data) return 1;

    /* only filter outgoing connections */
    if(c->state != state_OUT) return 1;

    log_debug(ZONE,"finding a new outgoing chunk to write");

    /* loop through looking for potential packets to send */
    for(cur = c->writeq; cur != NULL; cur = next)
    { /* last and next are set within, can't be used on first-pass! */

        /* cool, not a packet or dialback validated packet */
        if(cur->type != chunk_PACKET || (db = (int)xhash_get(c->db,cur->dbkey)) == 2)
        {
            /* if we're first */
            if(c->writeq == cur)
                return 1;

            /* if we're last */
            if((last->next = cur->next) == NULL)
                c->qtail = last;

            /* make us first */
            cur->next = c->writeq;
            c->writeq = cur;
            return 1;
        }

        /* db in progress already or no id to create a db yet, skip */
        if(db == 1 || c->id == NULL)
        {
            next = cur->next;
            last = cur;
            continue;
        }

        /* dialback failed, bounce this one */
        if(db == 3)
        {
            next = cur->next;

            /* if we're first or last */
            if(c->writeq == cur)
                c->writeq = next;
            else if(c->qtail == cur)
                c->qtail = last;

            /* make sure tail is updated */
            if(next == NULL)
                c->qtail = NULL;

            /* bounce chunk and continue ourselves */
            route_app(c->s2s,chunk_prep(cur,"Dialback Failed"));
            continue;
        }

        /* hmm, must be a packet that we have to send a db:result for yet, create and prepend it */
        xhash_put(c->db,pstrdup(c->p,cur->dbkey),(void*)1);
        cnew = chunk_new(chunk_DBRESULT,NULL,NULL);
        spooler(cnew->regen,"<db:result to='",cur->to->server,"' from='",cur->from->server,"'>",
            merlin(cnew->p,c->s2s->dbsecret,cur->to->server,c->id),"</db:result>",cnew->regen);
        chunk_prep(cnew,NULL);
        cnew->next = c->writeq;
        c->writeq = cnew;
        return 1;
    }

    return 0;
}

/* write chunks to this conn.  called (indirectly) by mio */
int conn_write(conn_t c)
{
    int len;
    chunk_t cur;
    int res = 0;

    /* try to write as much as we can */
    conn_lock(c);
    while(_conn_writechunk(c) && (cur = c->writeq) != NULL)
    {
        // log_debug(ZONE, "writing data to %d: %.*s", c->fd, cur->wlen, (char*)cur->wcur);

        // conn_unlock(c);  // ** SIG

        /* write a bit from the current buffer */
        len = _write_actual(c, c->fd, cur->wcur, cur->wlen);

        // conn_lock(c);  // ** SIG

        /* we had an error on the write */
        if(len < 0)
        {
            if(_error == _EWOULDBLOCK || _error == _EINTR || errno == EAGAIN)
            {
                res = 2; /* flag that we're blocking now */
                break;
            }

            mio_close(c->s2s->mio, c->fd);
            conn_unlock(c);
            return 0;
        }
        else if(len < cur->wlen) /* we didnt' write it all, move the current buffer up */
        { 
	    char *p = ((char *)cur->wcur) + len;
            cur->wcur = p;
            cur->wlen -= len;
            res = 1;
            break;
        }
        else /* we wrote the entire node, kill it and move on */
        {
            if((c->writeq = cur->next) == NULL)
                c->qtail = NULL;
            pool_free(cur->p);
        }
    }
    conn_unlock(c);
    return res;
}

