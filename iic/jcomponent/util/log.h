#ifndef LOG_H
#define LOG_H

#include "util.h"

#ifdef __cplusplus
extern "C" {
#endif

extern int log_level;

/* debug logging */
void debug_log(const char *file, int line, const char *msgfmt, ...);
#define ZONE __FILE__,__LINE__
#define MAX_DEBUG 1024

/* apps use these macros for logging */
#define log_debug if (log_level >= LOG_DEBUG) debug_log

#define log_status if (log_level >= LOG_INFO) debug_log

#define log_error if (log_level >= LOG_ERR) debug_log

#ifdef __cplusplus
}
#endif

#endif
