#include "eventlog.h"
#include "log.h"

#include <time.h>

#ifdef LINUX
#include <unistd.h>
#include <netdb.h>
#include <sys/stat.h>
#else
#include <windows.h>
#include <sys/types.h>
#include <sys/stat.h>
#endif
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

EventLog::EventLog()
{
    m_init = false;
	m_open_events = NULL;
	m_free_ids = NULL;
}

void EventLog::initialize(EventLogType type)
{
    size_t len = sizeof(m_hostname)/sizeof(m_hostname[0]);

    int res = gethostname(m_hostname, len);
    if (res < 0)
    {
        log_error(ZONE, "Unable to get hostname: %s", strerror(errno));

        return;
    }

    m_seq = (int)time(NULL) - 1060000000 /* shift the epoch to recent past */;

    m_allocsz = OPEN_EVENT_ALLOC_CHUNK;

    m_open_events = (CallEvent *)calloc(m_allocsz, sizeof(CallEvent));
    if (m_open_events == NULL)
    {
        log_error(ZONE, "Unable to alloc open events list");
        return;
    }

    m_free_ids = (int *)calloc(m_allocsz, sizeof(int));
    if (m_free_ids == NULL)
    {
        log_error(ZONE, "Unable to alloc free ids list");
        return;
    }

    for (unsigned i = 0; i < m_allocsz; i++)
    {
        m_free_ids[i] = i;
    }

    m_next_free = 0;

    m_my_type = type;

    m_init = true;
}

EventLog::~EventLog()
{
    if (m_free_ids)
        free(m_free_ids);

    if (m_open_events)
    {
        for (unsigned i = 0; i < m_allocsz; i++)
        {
            for (int j = 0; j < EvnLog_Field_Num; j++)
            {
                if (m_open_events[i].field_values[j])
                {
                    free((void *)(m_open_events[i].field_values[j]));
                }
            }
        }
        free(m_open_events);
    }
}

void EventLog::cleanup_event(CallEvent *evn)
{
    for (int j = 0; j < EvnLog_Field_Num; j++)
    {
        if (evn->field_values[j])
        {
            free((void *)evn->field_values[j]);
            evn->field_values[j] = NULL;
        }
    }
}

int EventLog::free_id(int id)
{
    if (m_next_free == 0)
    {
        log_error(ZONE, "Freeing more events than allocated!");

        return -1;
    }

    m_free_ids[--m_next_free] = id;

    return 0;
}

int EventLog::get_next_free_id()
{
    if (m_next_free == m_allocsz)
    {
        int newidx = m_allocsz;
        m_allocsz += OPEN_EVENT_ALLOC_CHUNK;
        CallEvent *new_events =
            (CallEvent *)realloc(m_open_events, m_allocsz*sizeof(CallEvent));
        if (new_events == NULL)
        {
            return -1;
        }

        m_open_events = new_events;
        int *new_ids = (int *)realloc(m_free_ids, m_allocsz*sizeof(CallEvent));
        if (new_ids == NULL)
        {
            return -1;
        }
        m_free_ids = new_ids;

        unsigned i, j;
        for (i = newidx; i < m_allocsz; i++)
        {
            m_free_ids[i] = i;
            for (j = 0; j < EvnLog_Field_Num; j++)
                m_open_events[i].field_values[j] = NULL;
        }
    }

    return m_free_ids[m_next_free++];
}

int EventLog::set_location(const char *directory,
                           const char *base_name)
{
    if (!m_init)
    {
        log_error(ZONE, "EventLog is not initialized");

        return -1;
    }

    struct stat s;

    if (stat(directory, &s) < 0)
    {
        log_error(ZONE, "Can't get attributes of directory '%s': %s",
                  directory, strerror(errno));

        return -1;
    }

    if (!(s.st_mode & S_IFDIR))
    {
        log_error(ZONE, "Log directory '%s' exists, but is not a directory", directory);

        return -1;
    }

    sprintf(m_filename_prefix, "%s/%s", directory, base_name);
    sprintf(m_filename, "%s/%s.elg", directory, base_name);

    m_init = true;

    return 0;
}

// minutes_since_midnight **GMT**
void EventLog::set_rotation_time(int minutes_since_midnight)
{
    if (minutes_since_midnight < 0)
    {
        log_error(ZONE, "set_rotation_time() given negative minutes!");

        return;
    }

    static int sec_per_day = 24 * 60 * 60;
    time_t now = time(NULL);
    time_t prev_midnight = now - (now % sec_per_day);

    int secs_since_midnight = minutes_since_midnight*60;

    m_next_rotation = prev_midnight + secs_since_midnight;
    if (m_next_rotation > now)
    {
        // it's in the future, so we are good to go
        return;
    }

    // rotation time already in the past, so bump to next day
    m_next_rotation += sec_per_day;
}

int EventLog::check_rotation()
{
    if (!m_init)
    {
        log_error(ZONE, "EventLog is not initialized");

        return -1;
    }

    time_t now = time(NULL);
    if (now < m_next_rotation)
    {
        log_debug(ZONE, "Not yet time for log rotation");

        return 0;
    }

    log_status(ZONE, "Rotating logs");

    char timestamp[21];
#ifdef LINUX
	struct tm timebuf;
    struct tm *tm_now = localtime_r(&now, &timebuf);
#else
    struct tm *tm_now = localtime(&now);
#endif

    strftime(timestamp, 20, "%Y-%m-%d-%H-%M-%S", tm_now);
    char rotfn[_MAXPATHLEN];

    strcpy(rotfn, m_filename_prefix);
    strcat(rotfn, "+");
    strcat(rotfn, timestamp);
    strcat(rotfn, ".elg");

    struct stat s;
    if (stat(m_filename, &s) < 0)
    {
        if (errno == ENOENT)
        {
            log_status(ZONE, "No logged events seen at log rotation time");

            return 0;
        }
    }


    if (rename(m_filename, rotfn) < 0)
    {
        log_error(ZONE, "Unable to rename %s as %s:  %s",
                  m_filename, rotfn, strerror(errno));

        return -1;
    }

    m_next_rotation += (24 * 60 * 60);	// i.e. update to tomorrow

    return 0;
}

int EventLog::log_reset()
{
    int status = 0;
    int i;
    time_t now = time(NULL);
    struct tm *tm_now = NULL;

    FILE *fp;
    fp = fopen(m_filename, "a");
    if (fp == NULL)
    {
        log_error(ZONE, "Unable to open event log %s: %s",
                  m_filename, strerror(errno));
        status = -1;
        goto cleanup;
    }

    // RecordID
    if (fprintf(fp, "\"%s-%u\"", m_hostname, m_seq++) < 0)
    {
        log_error(ZONE, "Unable to write to event log %s: %s",
                  m_filename, strerror(errno));
        status=-1;
        goto cleanup;
    }

    // Time
    char timestamp[21];
#ifdef LINUX
	struct tm timebuf;
    tm_now = localtime_r(&now, &timebuf);
#else
    tm_now = localtime(&now);
#endif
    strftime(timestamp, 20, "%Y-%m-%d-%H-%M-%S", tm_now);
    if (fprintf(fp, ",\"%s\"", timestamp) < 0)
    {
        log_error(ZONE, "Unable to write to event log %s: %s",
                  m_filename, strerror(errno));
        status=-1;
        goto cleanup;
    }

    // Duration
    if (fprintf(fp, ",\"0\"") < 0)
    {
        log_error(ZONE, "Unable to write to event log %s: %s",
                  m_filename, strerror(errno));
        status=-1;
        goto cleanup;
    }

    // Reset event
    if (fprintf(fp, ",\"0\"") < 0)
    {
        log_error(ZONE, "Unable to write to event log %s: %s",
                  m_filename, strerror(errno));
        status=-1;
        goto cleanup;
    }

    // Write out a bunch of empty fields
    for (i = 0; i < EvnLog_Field_Num; i++)
    {
        if (fprintf(fp, ",\"\"") < 0)
        {
            log_error(ZONE, "Unable to write to event log %s: %s",
                      m_filename, strerror(errno));
            status=-1;
            goto cleanup;
        }
    }

    if (fprintf(fp, "\n") < 0)
    {
        log_error(ZONE, "Unable to write to event log %s: %s",
                  m_filename, strerror(errno));
        status=-1;
        goto cleanup;
    }

  cleanup:
    if (fp) fclose(fp);

    return status;
}

int EventLog::log_start(EventLogEvent event,
                        EventLogField fields[],
                        const char *values[],
                        unsigned n_fields)
{
    int status = 0;
    int next_free = -1;
    struct tm *tm_now = NULL;
    CallEvent *evn = NULL;

    if (!m_init)
    {
        log_error(ZONE, "EventLog is not initialized");
        return -1;
    }

    next_free = get_next_free_id();
    if (next_free < 0)
    {
        log_error(ZONE, "Can't get a free call event");

        status=-1;
        goto cleanup;
    }

    evn = &m_open_events[next_free];
    cleanup_event(evn);
    evn->start_time = time(NULL);
    evn->event = event;

    FILE *fp;
    fp = fopen(m_filename, "a");
    if (fp == NULL)
    {
        log_error(ZONE, "Unable to open event log %s: %s",
                  m_filename, strerror(errno));

        status=-1;
        goto cleanup;
    }

    unsigned i;

    // make sure values are in order
    for (i = 0; i < n_fields; i++)
    {
        evn->field_values[(int)fields[i]] = strdup(values[i]);
        if (evn->field_values[(int)fields[i]] == NULL)
        {
            log_error(ZONE, "Unable to alloc space for values");
            status=-1;
            goto cleanup;
        }
    }

    // RecordID
    if (fprintf(fp, "\"%s-%u\"", m_hostname, m_seq++) < 0)
    {
        log_error(ZONE, "Unable to write to event log %s: %s",
                  m_filename, strerror(errno));
        status=-1;
        goto cleanup;
    }

    // Time
    char timestamp[21];
#ifdef LINUX
	struct tm timebuf;
    tm_now = localtime_r(&(evn->start_time), &timebuf);
#else
    tm_now = localtime(&(evn->start_time));
#endif
    strftime(timestamp, 20, "%Y-%m-%d-%H-%M-%S", tm_now);
    if (fprintf(fp, ",\"%s\"", timestamp) < 0)
    {
        log_error(ZONE, "Unable to write to event log %s: %s",
                  m_filename, strerror(errno));
        status=-1;
        goto cleanup;
    }

    // Duration
    if (fprintf(fp, ",\"0\"") < 0)
    {
        log_error(ZONE, "Unable to write to event log %s: %s",
                  m_filename, strerror(errno));
        status=-1;
        goto cleanup;
    }


    // Event
    switch (event)
    {
        case EvnLog_Event_Meeting:
            if (fprintf(fp, ",\"1\"") < 0)
            {
                log_error(ZONE, "Unable to write to event log %s: %s",
                          m_filename, strerror(errno));
                status=-1;
                goto cleanup;
            }

            break;

        case EvnLog_Event_Inbound_Call:
            if (fprintf(fp, ",\"3\"") < 0)
            {
                log_error(ZONE, "Unable to write to event log %s: %s",
                          m_filename, strerror(errno));
                status=-1;
                goto cleanup;
            }
            break;

        case EvnLog_Event_Outbound_Call:
            if (fprintf(fp, ",\"4\"") < 0)
            {
                log_error(ZONE, "Unable to write to event log %s: %s",
                          m_filename, strerror(errno));
                status=-1;
                goto cleanup;
            }
            break;

        case EvnLog_Event_IIC_Client:
            if (fprintf(fp, ",\"7\"") < 0)
            {
                log_error(ZONE, "Unable to write to event log %s: %s",
                          m_filename, strerror(errno));
                status=-1;
                goto cleanup;
            }

            break;

        case EvnLog_Event_App_Share:
            if (fprintf(fp, ",\"9\"") < 0)
            {
                log_error(ZONE, "Unable to write to event log %s: %s",
                          m_filename, strerror(errno));
                status=-1;
                goto cleanup;
            }

            break;

        case EvnLog_Event_App_Share_Viewer:
            if (fprintf(fp, ",\"11\"") < 0)
            {
                log_error(ZONE, "Unable to write to event log %s: %s",
                          m_filename, strerror(errno));
                status=-1;
                goto cleanup;
            }

            break;

        case EvnLog_Event_Conference_Recording:
            if (fprintf(fp, ",\"13\"") < 0)
            {
                log_error(ZONE, "Unable to write to event log %s: %s",
                          m_filename, strerror(errno));
                status=-1;
                goto cleanup;
            }

            break;

        case EvnLog_Event_Doc_Share:
            if (fprintf(fp, ",\"15\"") < 0)
            {
                log_error(ZONE, "Unable to write to event log %s: %s",
                          m_filename, strerror(errno));
                status=-1;
                goto cleanup;
            }

            break;

        case EvnLog_Event_Details:
            if (fprintf(fp, ",\"17\"") < 0)
            {
                log_error(ZONE, "Unable to write to event log %s: %s",
                          m_filename, strerror(errno));
                status=-1;
                goto cleanup;
            }

            break;
    }

    // Event Info, Community ID, Meeting ID, Host ID, Participant ID
    for (i = 0; i < EvnLog_Field_Num; i++)
    {
        if (evn->field_values[i] == NULL)
            evn->field_values[i] = strdup("");

        if (fprintf(fp, ",\"%s\"", evn->field_values[i]) < 0)
        {
            log_error(ZONE, "Unable to write to event log %s: %s",
                      m_filename, strerror(errno));
            status=-1;
            goto cleanup;
        }
    }

    if (fprintf(fp, "\n") < 0)
    {
        log_error(ZONE, "Unable to write to event log %s: %s",
                  m_filename, strerror(errno));
        status=-1;
        goto cleanup;
    }

  cleanup:
    if (status < 0)
    {
        if (next_free >= 0)
        {
            free_id(next_free);
        }

        cleanup_event(evn);
    }
    if (fp) fclose(fp);

    return next_free;
}

int EventLog::log_connected(int event_handle, EventLogEvent event)
{
    int status = 0;
    struct tm *tm_now = NULL;
    int duration;
    time_t now;

    if (!m_init)
    {
        log_error(ZONE, "EventLog is not initialized");

        return -1;
    }

    if ((unsigned)event_handle > m_allocsz || event_handle < 0)
    {
        log_error(ZONE, "Invalid event handle given to log_connected()");

        return -1;
    }

    CallEvent *evn = &m_open_events[event_handle];

    if (evn->event != event)
    {
        log_error(ZONE, "log_connected() expected event %u, but got %u",
                  evn->event, event);
        return -1;
    }

    FILE *fp;
    fp = fopen(m_filename, "a");
    if (fp == NULL)
    {
        log_error(ZONE, "Unable to open event log %s: %s",
                  m_filename, strerror(errno));
        status=-1;
        goto cleanup;
    }

    int i;

    // RecordID
    if (fprintf(fp, "\"%s-%u\"", m_hostname, m_seq++) < 0)
    {
        status=-1;
        goto cleanup;
    }

    // Time
    now = time(NULL);
    char timestamp[21];
#ifdef LINUX
	struct tm timebuf;
    tm_now = localtime_r(&now, &timebuf);
#else
    tm_now = localtime(&now);
#endif
    strftime(timestamp, 20, "%Y-%m-%d-%H-%M-%S", tm_now);
    if (fprintf(fp, ",\"%s\"", timestamp) < 0)
    {
        status=-1;
        goto cleanup;
    }


    // Duration
    duration = (int)now - (int)evn->start_time;
    if (fprintf(fp, ",\"%u\"",  duration) < 0)
    {
        status=-1;
        goto cleanup;
    }

    // Event
    if (fprintf(fp, ",\"5\"") < 0)
    {
        status=-1;
        goto cleanup;
    }

    // Event Info, Community ID, Meeting ID, Host ID, Participant ID
    for (i = 0; i < EvnLog_Field_Num; i++)
    {
        if (fprintf(fp, ",\"%s\"", evn->field_values[i]) < 0)
        {
            status=-1;
            goto cleanup;
        }
    }

    if (fprintf(fp, "\n") < 0)
    {

        status=-1;
        goto cleanup;
    }

  cleanup:
    if (fp) fclose(fp);

    return status;
}

time_t EventLog::log_get_start_time(int event_handle)
{
    if (!m_init)
    {
        log_error(ZONE, "EventLog is not initialized");

        return -1;
    }

    if ((unsigned)event_handle > m_allocsz)
    {
        log_error(ZONE, "Invalid event handle given to log_connected()");

        return -1;
    }

    CallEvent *evn = &m_open_events[event_handle];

    return evn->start_time;
}


int EventLog::log_get_field(int event_handle,
                            EventLogField field,
                            const char **value)
{
    if (!m_init)
    {
        log_error(ZONE, "EventLog is not initialized");

        return -1;
    }

    if ((unsigned)event_handle > m_allocsz)
    {
        log_error(ZONE, "Invalid event handle given to log_connected()");

        return -1;
    }

    CallEvent *evn = &m_open_events[event_handle];

    if (field >= EvnLog_Field_Num)
    {
        log_error(ZONE, "Invalid event field given");

        return -1;
    }

    *value = evn->field_values[(int)field];

    return 0;
}

int EventLog::log_end(int event_handle)
{
    int status = 0;
    int duration;
    time_t now;
    struct tm *tm_now;

    if (!m_init)
    {
        log_error(ZONE, "EventLog is not initialized");

        return -1;
    }

    if ((unsigned)event_handle > m_allocsz)
    {
        log_error(ZONE, "Invalid event handle given to log_connected()");

        return -1;
    }

    CallEvent *evn = &m_open_events[event_handle];

    FILE *fp = fopen(m_filename, "a");
    if (fp == NULL)
    {
        log_error(ZONE, "Unable to open event log %s: %s",
                  m_filename, strerror(errno));
        status = -1;
        goto cleanup;
    }

    int i;

    // RecordID
    if (fprintf(fp, "\"%s-%u\"", m_hostname, m_seq++) < 0)
    {
        log_error(ZONE, "Unable to write event log %s: %s",
                  m_filename, strerror(errno));
        status = -1;
        goto cleanup;
    }

    // Time
    now = time(NULL);
    char timestamp[21];
#ifdef LINUX
	struct tm timebuf;
    tm_now = localtime_r(&now, &timebuf);
#else
    tm_now = localtime(&now);
#endif
    strftime(timestamp, 20, "%Y-%m-%d-%H-%M-%S", tm_now);
    if (fprintf(fp, ",\"%s\"", timestamp) < 0)
    {
        log_error(ZONE, "Unable to write event log %s: %s",
                  m_filename, strerror(errno));
        status = -1;
        goto cleanup;
    }

    // Duration
    duration = (int)now - (int)evn->start_time;
    if (fprintf(fp, ",\"%u\"",  duration) < 0)
    {
        log_error(ZONE, "Unable to write event log %s: %s",
                  m_filename, strerror(errno));
        status = -1;
        goto cleanup;
    }

    // Event
    switch (evn->event)
    {
        case EvnLog_Event_Meeting:
            if (fprintf(fp, ",\"2\"") < 0)
            {
                log_error(ZONE, "Unable to write event log %s: %s",
                          m_filename, strerror(errno));
                status = -1;
                goto cleanup;
            }
            break;

        case EvnLog_Event_Inbound_Call:
        case EvnLog_Event_Outbound_Call:
            if (fprintf(fp, ",\"6\"") < 0)
            {
                log_error(ZONE, "Unable to write event log %s: %s",
                          m_filename, strerror(errno));
                status = -1;
                goto cleanup;
            }
            break;

        case EvnLog_Event_IIC_Client:
            if (fprintf(fp, ",\"8\"") < 0)
            {
                log_error(ZONE, "Unable to write event log %s: %s",
                          m_filename, strerror(errno));
                status = -1;
                goto cleanup;
            }
            break;

        case EvnLog_Event_App_Share:
            if (fprintf(fp, ",\"10\"") < 0)
            {
                log_error(ZONE, "Unable to write event log %s: %s",
                          m_filename, strerror(errno));
                status = -1;
                goto cleanup;
            }
            break;

        case EvnLog_Event_App_Share_Viewer:
            if (fprintf(fp, ",\"12\"") < 0)
            {
                log_error(ZONE, "Unable to write event log %s: %s",
                          m_filename, strerror(errno));
                status = -1;
                goto cleanup;
            }

            break;

        case EvnLog_Event_Conference_Recording:
            if (fprintf(fp, ",\"14\"") < 0)
            {
                log_error(ZONE, "Unable to write event log %s: %s",
                          m_filename, strerror(errno));
                status = -1;
                goto cleanup;
            }

            break;

        case EvnLog_Event_Doc_Share:
            if (fprintf(fp, ",\"16\"") < 0)
            {
                log_error(ZONE, "Unable to write event log %s: %s",
                          m_filename, strerror(errno));
                status = -1;
                goto cleanup;
            }
            break;

        case EvnLog_Event_Details:
            if (fprintf(fp, ",\"18\"") < 0)
            {
                log_error(ZONE, "Unable to write event log %s: %s",
                          m_filename, strerror(errno));
                status = -1;
                goto cleanup;
            }
            break;
    }

    // Event Info, Community ID, Meeting ID, Host ID, Participant ID
    for (i = 0; i < EvnLog_Field_Num; i++)
    {
        if (fprintf(fp, ",\"%s\"", evn->field_values[i]) < 0)
        {
            log_error(ZONE, "Unable to write event log %s: %s",
                      m_filename, strerror(errno));
            status = -1;
            goto cleanup;
        }
    }

    if (fprintf(fp, "\n") < 0)
    {
        log_error(ZONE, "Unable to write event log %s: %s",
                  m_filename, strerror(errno));
        status = -1;
        goto cleanup;
    }

  cleanup:
    if (fp) fclose(fp);

    cleanup_event(evn);
    free_id(event_handle);

    return status;
}

int EventLog::log_user_record(EventLogUserField fields[],
                              const char *values[],
                              int n_values)
{
	int i;
    int status = 0;

    if (!m_init)
    {
        log_error(ZONE, "EventLog is not initialized");

        return -1;
    }

    const char *inorder_values[EvnLog_UserField_Num];
    for (i = 0; i < n_values; i++)
    {
        inorder_values[fields[i]] = values[i];
    }

    FILE *fp = fopen(m_filename, "a");
    if (fp == NULL)
    {
        log_error(ZONE, "Unable to open event log '%s': %s",
                  m_filename, strerror(errno));

        status=-1;
        goto cleanup;
    }

    // RecordID
    if (fprintf(fp, "\"%s-%u\"", m_hostname, m_seq++) < 0)
    {
        status=-1;
        goto cleanup;
    }


    for (i = 0; i < EvnLog_UserField_Num; i++)
    {
        if (inorder_values[i] == NULL)
            inorder_values[i] = "";

        if (fprintf(fp, ",\"%s\"", inorder_values[i]) < 0)
        {
            status=-1;
            goto cleanup;
        }
    }

    if (fprintf(fp, "\n") < 0)
    {
        status=-1;
        goto cleanup;
    }

  cleanup:
    if (fp) fclose(fp);

    return status;
}

int EventLog::log_reservation_record(EventLogReservationEvent event,
                                     EventLogReservationField fields[],
                                     const char *values[],
                                     int n_values)
{
	int i;
    int status = 0;

    if (!m_init)
    {
        log_error(ZONE, "EventLog is not initialized");
        return -1;
    }

    const char *inorder_values[EvnLog_ReservationField_Num];
    for (i = 0; i < n_values; i++)
    {
        inorder_values[fields[i]] = values[i];
    }

    FILE *fp = fopen(m_filename, "a");
    if (fp == NULL)
    {
        log_error(ZONE, "Unable to open event log '%s': %s",
                  m_filename, strerror(errno));

        status=-1;
        goto cleanup;
    }

    // RecordID
    if (fprintf(fp, "\"%s-%u\"", m_hostname, m_seq++) < 0)
    {
        log_error(ZONE, "Unable to write to event log '%s': %s",
                  m_filename, strerror(errno));
        status=-1;
        goto cleanup;
    }


    for (i = 0; i < EvnLog_ReservationField_Num; i++)
    {
        if (fprintf(fp, ",\"%s\"", (inorder_values[i] ? inorder_values[i] : "")) < 0)
        {
            log_error(ZONE, "Unable to write to event log '%s': %s",
                      m_filename, strerror(errno));
            status=-1;
            goto cleanup;
        }
    }

    if (fprintf(fp, ",\"%d\"", (int)event) < 0)
    {
        log_error(ZONE, "Unable to write to event log '%s': %s",
                  m_filename, strerror(errno));
        status=-1;
        goto cleanup;
    }

    if (fprintf(fp, "\n") < 0)
    {
        log_error(ZONE, "Unable to write to event log '%s': %s",
                  m_filename, strerror(errno));
        status=-1;
        goto cleanup;
    }

  cleanup:
    if (fp) fclose(fp);
    return status;
}
