#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include "log.h"

#define TMPTEMP	"/tmp/bodyXXXXXX"

void send_email(const char *recips, const char *subject, const char *body)
{
    char tmpfn[sizeof(TMPTEMP)+1];
    FILE *tmpfp;
    char cmd[1024];

    if (recips == NULL || *recips == '\0')
        // No recipients
        return;

    strcpy(tmpfn, TMPTEMP);
    tmpfp = fdopen(mkstemp(tmpfn), "w");
    if (tmpfp == NULL)
    {
        log_error(ZONE, "Couldn't write temp file %s for e-mail", tmpfn);
        return;
    }
    fprintf(tmpfp, "%s\n", body);
    fclose(tmpfp);

    strcpy(cmd, "mail -s \"");
    strcat(cmd, subject);
    strcat(cmd, "\" ");
    strcat(cmd, recips);
    strcat(cmd, " < ");
    strcat(cmd, tmpfn);

    if (system(cmd) < 0)
    {
        log_error(ZONE, "Failure sending e-mail: %s", strerror(errno));
    }
    else
    {
        log_debug(ZONE, "Sent email to: %s", recips);
    }

    unlink(tmpfn);
}
