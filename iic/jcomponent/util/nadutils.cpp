
#include "../../util/istring.h"
#include "util.h"
#include "log.h"

static IString blank;

// Move this code into common library at some point...
// For now, just include into modules that need these functions.

IString get_attr_value(nad_t nad, int rnode, const char *attrib, const char *def = NULL)
{
	int node = nad_find_attr(nad, rnode, (char*)attrib, NULL);
	if (node < 0)
	{
		if (!def)
		{
			log_error(ZONE, "Unable to find configuration attribute: %s\n", attrib);
			return blank;
		}
		return IString(def);
	}

	IString temp(NAD_AVAL(nad, node), NAD_AVAL_L(nad, node));
	return temp;	
}

IString get_child_value(nad_t nad, int rnode, const char *child, const char *def = NULL)
{
	int node = nad_find_child_elem(nad, rnode, (char*)child, 1);
	if (node < 0)
	{
		if (!def)
		{
			log_error(ZONE, "Unable to find configuration item: %s\n", child);
			return blank;
		}
		return IString(def);
	}

	IString temp(NAD_CDATA(nad, node), NAD_CDATA_L(nad, node));
	return temp;
}

int get_child_value_i(nad_t nad, int rnode, const char *child, int def = 0)
{
	int node = nad_find_child_elem(nad, rnode, (char*)child, 1);
	if (node < 0)
	{
		return def;
	}

	return atoi(NAD_CDATA(nad, node));
}

IString get_value(nad_t nad, int node, const char *def = NULL)
{
	if (node < 0)
	{
		if (!def)
		{
			return blank;
		}
		return IString(def);
	}

	IString temp(NAD_CDATA(nad, node), NAD_CDATA_L(nad, node));
	return temp;
}

int get_root(nad_t nad, int root_depth, const char *root)
{
	int rnode = nad_find_elem(nad, 0, (char*)root, root_depth);
	if (rnode < 0)
	{
		log_error(ZONE, "Unable to find configuration item: %s\n", root);
	}
	return rnode;
}

