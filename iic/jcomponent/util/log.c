#ifdef LINUX
#include <pthread.h>
#endif
#ifdef _WIN32
#include <windows.h>
#endif
#include "util.h"
#include "log.h"
#include <glib.h>

/* Still in progress...not using log_xx functions */

#ifdef DEBUG
int log_level = LOG_DEBUG;
#else
int log_level = LOG_INFO;
#endif /*DEBUG*/

#ifndef USE_SYSLOG
static FILE * log_file = NULL;
#endif

static GStaticRecMutex writelock;

void log_set_level(int level)
{
	log_level = level;
	log_debug(ZONE, "log level set to %d", level);
}

// If the current log is >= trigger_sz, put current log in history
// and push history down
static size_t trigger_sz = 1024 * 1024;

// The number of log files to keep before deleting the oldest
static unsigned history_len = 10;

void log_set_rotation_size(size_t _trigger_sz)
{
    trigger_sz = _trigger_sz;
}

void log_set_history_len(int _history_len)
{
    history_len = _history_len;
}

#ifdef USE_SYSLOG

void log_new(char *ident)
{
    openlog(ident, LOG_PID, LOG_DAEMON);
}

void log_write(int level, const char *msgfmt, ...)
{
    va_list ap;

    va_start(ap, msgfmt);
    vsyslog(level, msgfmt, ap);
    va_end(ap);
}

void log_free()
{
    closelog();
}

#else

static char log_file_name[MAX_LOG_FILENAME];

static void log_rotate_if_needed()
{
  char newerfn[MAX_LOG_FILENAME];
  char olderfn[MAX_LOG_FILENAME];
  int i;
  long sz;

  fseek(log_file, 0, SEEK_END);
  sz = ftell(log_file);

  if (sz < trigger_sz) return;

  fclose(log_file);

  sprintf(newerfn, "%s.%u", log_file_name, history_len);
  unlink(newerfn);
  for (i = history_len-1; i > 0; i--) {
      strcpy(olderfn, newerfn);
      sprintf(newerfn, "%s.%u", log_file_name, i);
      if (rename(newerfn, olderfn) < 0) {
          if (errno != ENOENT) {
              log_file = fopen(log_file_name, "a+");
              log_write(4, "Couldn't rotate logs: %s", strerror(errno));
          }
      }
  }
  strcpy(olderfn, newerfn);
  strcpy(newerfn, log_file_name);
  if (rename(newerfn, olderfn) < 0) {
    if (errno != ENOENT) {
      log_file = fopen(log_file_name, "a+");
      log_write(4, "Couldn't rotate logs: %s", strerror(errno));
    }
  }

  log_file = fopen(log_file_name, "a+");
  if(log_file == NULL) {
    log_error(ZONE,
	      "couldn't open %s for append: %s\n"
	      "logging will go to stderr instead\n", log_file_name, strerror(errno));
  }
}

void log_new(char *ident)
{
    snprintf(log_file_name, MAX_LOG_FILENAME, "%s.log", ident);

    log_file = fopen(log_file_name, "a+");
    if(log_file == NULL) {
        log_error(ZONE,
            "couldn't open %s for append: %s\n"
            "logging will go to stderr instead\n", log_file_name, strerror(errno));
    }

    g_static_rec_mutex_init(&writelock);
}

static const char *log_level_names[] =
{
    "emergency",
    "alert",
    "critical",
    "error",
    "warning",
    "notice",
    "info",
    "debug"
};

void log_write(int level, const char *msgfmt, ...)
{
    FILE * f;
    va_list ap;
    char *pos, message[MAX_LOG_LINE];
    int sz;
    time_t t;
#ifdef LINUX
	char timestr[32];
#endif
    g_static_rec_mutex_lock(&writelock);

#ifndef USE_SYSLOG
	if (log_file) log_rotate_if_needed();
#endif

    /* timestamp */
    t = time(NULL);
#ifdef LINUX
    pos = ctime_r(&t, timestr);
#else
	pos = ctime(&t);
#endif
    sz = strlen(pos);
    /* chop off the \n */
    pos[sz-1]=' ';

    /* insert the header */
    snprintf(message, MAX_LOG_LINE, "%s[%s] ", pos, log_level_names[level]);

    /* find the end and attach the rest of the msg */
    for (pos = message; *pos != '\0'; pos++); //empty statement
    sz = pos - message;
    va_start(ap, msgfmt);
    vsnprintf(pos, MAX_LOG_LINE - sz, msgfmt, ap);
	va_end(ap);

    f = log_file != NULL ? log_file : stderr;

    fprintf(f, "%s", message);
    fprintf(f, "\n");

#ifdef DEBUG
    /* If we are in debug mode we want everything copied to the stdout */
    if (level != LOG_DEBUG)
        fprintf(stdout, "%s\n", message);
#endif /*DEBUG*/
    g_static_rec_mutex_unlock(&writelock);
}

void log_free()
{
	FILE * f = log_file != NULL ? log_file : stderr;
	fflush(f);
	if (log_file != NULL)
		fclose(log_file);
}
#endif

/* spit out log output */
void debug_log(const char *file, int line, const char *msgfmt, ...)
{
    va_list ap;
    char *pos, message[MAX_DEBUG];
	const char *name_pos;
    int sz;
    time_t t;
	int threadid;
#ifdef LINUX
	char timestr[32];
#endif
#ifndef USE_SYSLOG
	FILE * f = NULL;
#endif

    g_static_rec_mutex_lock(&writelock);

#ifndef USE_SYSLOG
    if (log_file) log_rotate_if_needed();
#endif

    /* timestamp */
    t = time(NULL);
#ifdef LINUX
    pos = ctime_r(&t, timestr);
#else
	pos = ctime(&t);
#endif
    sz = strlen(pos);
    /* chop off the \n */
    pos[sz-1]=' ';

	/* remove path from filename */
	name_pos = strrchr(file, '\\');
	if (name_pos != NULL)
        name_pos++;
    else
        name_pos = file;

    /* insert the header */
#ifdef LINUX
	threadid = pthread_self();
#else // _WIN32
	threadid = GetCurrentThreadId();
#endif
    snprintf(message, MAX_DEBUG, "[%d] %s%s:%d ", threadid, pos, name_pos, line);

    /* find the end and attach the rest of the msg */
    for (pos = message; *pos != '\0'; pos++); //empty statement
    sz = pos - message;
    va_start(ap, msgfmt);
    vsnprintf(pos, MAX_DEBUG - sz, msgfmt, ap);
	va_end(ap);

#ifndef USE_SYSLOG
    f = log_file != NULL ? log_file : stderr;

    fprintf(f, "%s", message);
    fprintf(f, "\n");
	fflush(f);
#else
    log_write(LOG_DEBUG, "%s", message);
#endif

    g_static_rec_mutex_unlock(&writelock);
}

