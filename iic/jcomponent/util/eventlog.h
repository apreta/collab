/*
 * [ CALL RECORD FORMAT ]
 *
 * CSV text file, one line per event.  Each server records events in a local file.
 *
 * Record ID, Time, Duration, Event, Event Info, Community ID, Meeting ID, Meeting Start, Host ID, Participant ID
 *
 *
 * Record ID:  incrementing sequence number
 * Event:
 *  0 = reset (any open sessions are closed)
 *	1 = meeting started
 *	2 = meeting ended
 *	3 = inbound call started
 *	4 = outbound call started
 *	5 = call connected
 *	6 = call ended
 *	7 = IIC client joined meeting
 *	8 = IIC client left meeting
 *	9 = App share started
 * 10 = App share ended
 * 11 = App share viewer connected
 * 12 = App share viewer disconnected
 * 13 = Conference recording started
 * 14 = Conference recording ended
 * 15 = Doc share session started
 * 16 = Doc share session ended
 *
 * Info:
 *	DNIS (inbound call)
 *	Dialed number (outbound call)
 *	User name (IIC client)
 *
 * [ USER RECORD FORMAT ]
 *
 * CSV text file, one line per event.  The database service outputs these records when users are added, updated, or deleted.
 *
 * Record ID, Community ID, User ID, Event,
 * Title, First Name, Middle Name, Last Name, Suffix, Company,
 * Job Title, Address 1, Address 2, State, Country, Postal Code,
 * IIC Screen Name, Emal 1, Email 2, Email 3, Business Phone,
 * Home Phone, Mobile Phone, Other Phone, Extension,
 * AIM Screen Name, Yahoo Screen Name, MSN Screen Name,
 * User 1, User 2, Default Phone, Default Email, Type,
 * Modified Time
 *
 * Record ID:  incrementing sequence number
 * Event:
 * 	1 = Added
 * 	2 = Updated
 * 	3 = Deleted
 * Type:
 *	1 for registered user,
 *	2 for admin
 * Default Phone:
 *	0 for none,
 *	1 for Business,
 *	2 for Home,
 *	3 for Mobile,
 *	4 for Other
 * Default Email:
 *	0 for none,
 *	1 for email 1,
 *	2 for email 2,
 *	3 for email 3
 */

#ifndef __EVENTLOG_H__
#define __EVENTLOG_H__

#include <time.h>
#ifdef LINUX
#include <sys/param.h>
#define _MAXPATHLEN MAXPATHLEN
#else
#include <stdlib.h>
#define _MAXPATHLEN _MAX_PATH
#endif

typedef enum {
    EvnLog_Type_Call,
    EvnLog_Type_User,
    EvnLog_Type_Reservation,
} EventLogType;

typedef enum {
  EvnLog_Event_Meeting,

  EvnLog_Event_IIC_Client,	/* use log_start() for join and log_end() for leave */

  EvnLog_Event_App_Share,
  EvnLog_Event_App_Share_Viewer,

  EvnLog_Event_Outbound_Call,  /* log_start() log_end() */
  EvnLog_Event_Inbound_Call,   /* log_start() log_end() */
  EvnLog_Event_Conference_Recording,
  EvnLog_Event_Doc_Share,

  EvnLog_Event_Details,
} EventLogEvent;

typedef enum {
    EvnLog_User_Event_Added=1,
    EvnLog_User_Event_Updated,
    EvnLog_User_Event_Deleted,
} EventLogUserEvent;

typedef enum {
    EvnLog_Reserve_Add=1,
    EvnLog_Reserve_Update,
    EvnLog_Reserve_Delete,
    EvnLog_Reserve_Add_Participant,
    EvnLog_Reserve_Delete_Participant,
    EvnLog_Reserve_Delete_All_Participants,
} EventLogReservationEvent;

typedef enum
{
  /* RecordID, Event, Time and Duration are computed fields */

    // these have to be consecutive starting at 0
    // Also they determine the order of the fields in the CSV log
    // file.
    //
    // There are 4 computed fields that appear before these in the
    // file.
    EvnLog_Field_EventInfo     = 0,	// 5th field
    EvnLog_Field_CommunityId,		// 6th field
    EvnLog_Field_MeetingId,			// ...
    EvnLog_Field_MeetingStart,
    EvnLog_Field_HostId,
    EvnLog_Field_ParticipantId,
    EvnLog_Field_Num				// must be last
} EventLogField;

typedef enum
{
    // The values for these fields must correspond to
    // the positions of the fields in the external API spec doc
    EvnLog_UserField_CommunityId = 0,
    EvnLog_UserField_UserId = 1,
    EvnLog_UserField_Event = 2,
    EvnLog_UserField_Title = 3,
    EvnLog_UserField_FirstName = 4,
    EvnLog_UserField_MiddleName = 5,
    EvnLog_UserField_LastName = 6,
    EvnLog_UserField_Suffix = 7,
    EvnLog_UserField_Company = 8,
    EvnLog_UserField_JobTitle = 9,
    EvnLog_UserField_Address1 = 10,
    EvnLog_UserField_Address2 = 11,
    EvnLog_UserField_State = 12,
    EvnLog_UserField_Country = 13,
    EvnLog_UserField_PostalCode = 14,
    EvnLog_UserField_IICScreenName = 15,
    EvnLog_UserField_Email1 = 16,
    EvnLog_UserField_Email2 = 17,
    EvnLog_UserField_Email3 = 18,
    EvnLog_UserField_BusinessPhone = 19,
    EvnLog_UserField_HomePhone = 20,
    EvnLog_UserField_MobilePhone = 21,
    EvnLog_UserField_OtherPhone = 22,
    EvnLog_UserField_Extension = 23,
    EvnLog_UserField_AIMScreenName = 24,
    EvnLog_UserField_YahooScreenName = 25,
    EvnLog_UserField_MSNScreenName = 26,
    EvnLog_UserField_User1 = 27,
    EvnLog_UserField_User2 = 28,
    EvnLog_UserField_DefaultPhone = 29,
    EvnLog_UserField_DefaultEmail = 30,
    EvnLog_UserField_Type = 31,
    EvnLog_UserField_ModifiedTime = 32,
    EvnLog_UserField_Num = 33,
} EventLogUserField;

typedef enum
{
    EvnLog_ReservationField_MeetingID=0,
    EvnLog_ReservationField_MeetingStart,
    EvnLog_ReservationField_MeetingEnd,
    EvnLog_ReservationField_CommunityID,
    EvnLog_ReservationField_HostID,
    EvnLog_ReservationField_MeetingOptions,
    EvnLog_ReservationField_ParticipantID,
    EvnLog_ReservationField_ParticipantPin,
    EvnLog_ReservationField_Num
} EventLogReservationField;

#define OPEN_EVENT_ALLOC_CHUNK	2000

class EventLog
{
  private:
    int    m_my_type;
    bool   m_init;
    time_t m_next_rotation;
    char   m_filename_prefix[_MAXPATHLEN];
    char   m_filename[_MAXPATHLEN];
    char   m_hostname[256];
    unsigned m_seq;


    typedef struct
    {
        EventLogEvent event;
        time_t start_time;
        const char *field_values[(int)EvnLog_Field_Num];
    } CallEvent;

    int       *m_free_ids;
    unsigned   m_next_free;
    CallEvent *m_open_events;
    unsigned   m_allocsz;

    int get_next_free_id();
    int free_id(int id);

    void cleanup_event(CallEvent *evn);

  public:
    /*
     * Log is written in <directory>/<basename>.elg
     *
     * Rotated log files are named as:
     *	<directory>/<basename>+<timestamp>.elg
     *
     * Computes the initial record id from the IP addr and construction
     * time.
     *
     * Computes the next rotation time using a default of midnight.
     */
    EventLog();
    ~EventLog();

	void initialize(EventLogType type);

    int set_location(const char *directory,	/* base directory for log files */
                     const char *basename);	/* base name for log files */

    /*
     * Changes the time when logs are rotated.  Rotations are done
     * every 24hrs at the given number of minutes since midnight GMT
     */
    void set_rotation_time(int minutes_since_midnight /* GMT */);

    int log_reset();

    /*
     * Writes a start-of-event record and returns an event handle to the caller
     * to be used in subsequent calls ( log_connected() and log_end() )
     */
    int log_start(EventLogEvent event,
                  EventLogField fields[],
                  const char *values[],
                  unsigned n_fields);

    // Get a field value from an event ( can't be used after log_end() )
    int log_get_field(int event_handle,
                      EventLogField field,
                      const char **value);

    time_t log_get_start_time(int event_handle);

    /*
     * Used to log when incoming or outgoing calls are connected.
     */
    int log_connected(int event_handle, EventLogEvent event);

    /*
     * Used to log when an event has ended.  Computes duration.
     */
    int log_end(int event_handle);

    // ??? Need an abort event if can't get event to log ???

    /*
     * Logs a user record event.  No computed fields for these records.
     *
     */
    int log_user_record(EventLogUserField fields[],
                        const char *values[],
                        int n_values);

    /*
     * Logs a reservation record.
     *
     */
    int log_reservation_record(EventLogReservationEvent event,
                               EventLogReservationField fields[],
                               const char *values[],
                               int n_values);

    /*
     * This method checks to see if log rotation is necessary.
     *
     * If the next rotation time has passed, rename <directory>/<basename>.elg
     * to <directory>/<basename>+<timestamp>.elg
     */
    int check_rotation();
};


#endif
