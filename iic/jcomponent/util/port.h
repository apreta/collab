#ifndef PORT_UTIL_H
#define PORT_UTIL_H

#ifdef _WIN32
#define strcasecmp stricmp
#define strncasecmp strnicmp
#define strtok_r(a,b,c) strtok(a,b)

extern void __declspec(dllimport) __stdcall Sleep(unsigned long millis);
#define sleep(sec) Sleep(sec * 1000)

#define CHECK_ERR GetLastError()

#define _EWOULDBLOCK WSAEWOULDBLOCK
#define _EINTR WSAEINTR
#define _EINPROGRESS WSAEINPROGRESS
#define _ECONNREFUSED WSAECONNREFUSED

#define stat _stat

#else // LINUX

#define CHECK_ERR errno

#define _EWOULDBLOCK EWOULDBLOCK
#define _EINTR EINTR
#define _EINPROGRESS EINPROGRESS
#define _ECONNREFUSED ECONNREFUSED

#endif

#endif
