
#include <stdio.h>
#include <string.h>
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>
#include "rpcresult.h"
#include "log.h"

RPCResultSet::RPCResultSet(xmlrpc_env *_env, xmlrpc_value *_value)
{
	fields = NULL;
	value = NULL;
	rows = NULL;

	rowset_flag = false;
	first_row = 0;
	chunk_size = 0;
	more_data = 0;
	total_rows = 0;

	env = _env;
	value = _value;

	if (value)
		xmlrpc_INCREF(value);

	parse();
}

RPCResultSet::~RPCResultSet()
{
	if (value)
		xmlrpc_DECREF(value);
}

void RPCResultSet::parse()
{
	if (xmlrpc_value_type(value) == XMLRPC_TYPE_ARRAY)
	{
		int more;

		xmlrpc_value *first = xmlrpc_array_get_item(env, value, 0);
		if (env->fault_occurred)
		{
			log_error(ZONE, "Error parsing RPC result\n");
			return;
		}

		// If first element is an array, assume this is a rowset.
		if (xmlrpc_value_type(first) == XMLRPC_TYPE_ARRAY)
		{
			// Row set encoding is chunk info and array of arrays.
			xmlrpc_parse_value(env, value, "(Aiiii)", &rows, &first_row, &chunk_size, &total_rows, &more);
			if (env->fault_occurred)
			{
				log_error(ZONE, "Error parsing RPC result\n");
				rows = NULL;
			}
			else
			{
				more_data = more != 0;
				rowset_flag = true;
			}
		}
	}
	else
	{
		log_error(ZONE, "Error parsing RPC result\n");
		rowset_flag = false;
	}
}

bool RPCResultSet::is_rowset()
{
	return rowset_flag;
}

int RPCResultSet::get_first_index()
{
	return first_row;
}

int RPCResultSet::get_chunk_size()
{
	return chunk_size;
}

int RPCResultSet::get_total_rows()
{
	return total_rows;
}

bool RPCResultSet::more_rows_available()
{
	return more_data;
}

int RPCResultSet::get_column(const char *field)
{
	int i;

	if (fields == NULL)
		return -1;

	for (i = 0; fields[i] != NULL; i++)
		if (!strcmp(field, fields[i]))
			return i;

	return -1;
}

void RPCResultSet::set_column_info(const char *_fields[])
{
	fields = _fields;
}

const char *RPCResultSet::get_value(int row_num, int col)
{
	const char *ret = NULL;

	if (rows == NULL)
	{
		log_error(ZONE, "No row data in result set\n");
		return NULL;
	}

	// Get array of field values from array of rows.
	xmlrpc_value *row_value = xmlrpc_array_get_item(env, rows, row_num);
	if (env->fault_occurred)
	{
		log_error(ZONE, "Error accessing row %d in result set\n", row_num);
		return NULL;
	}

	// Find specified field value.
	xmlrpc_value *col_val = xmlrpc_array_get_item(env, row_value, col);
	if (env->fault_occurred)
	{
		log_error(ZONE, "Error retrieving value for column %d\n", col);
		return NULL;
	}

	// Parse as string (only data type currently supported)
	xmlrpc_parse_value(env, col_val, "s", &ret);
	if (env->fault_occurred)
	{
		log_error(ZONE, "Error retrieving value for column %d\n", col);
		return NULL;
	}

	return ret;
}

// Get value from row and column.
// Note:  we shouldn't have to deref and values obtained herein as these
// accessors shouldn't increment the ref count.
const char *RPCResultSet::get_value(int row_num, const char *field)
{
	if (fields == NULL)
	{
		log_error(ZONE, "Column information not specified for result set\n");
		return NULL;
	}

	// Figure out column from field name
	int col = get_column(field);
	if (col < 0)
	{
		log_error(ZONE, "Invalid field %s requested\n", field);
		return NULL;
	}

	return get_value(row_num, col);
}

// Get multiple values from row.
bool RPCResultSet::get_values(int row_num, const char *format, ...)
{
	va_list args;
	va_start(args, format);

	if (rows == NULL)
	{
		log_error(ZONE, "No row data in result set\n");
		return false;
	}

	xmlrpc_value *row_value = xmlrpc_array_get_item(env, rows, row_num);
	if (env->fault_occurred)
	{
		log_error(ZONE, "Error accessing row %d in result set\n", row_num);
		return false;
	}

	xmlrpc_parse_value_va(env, row_value, format, args);
	if (env->fault_occurred)
	{
		log_error(ZONE, "Error parsing row %d in result set\n", row_num);
		return false;
	}

	va_end(args);
	return true;
}

int RPCResultSet::get_status()
{
	int status = -1;

	get_output_values("(i*)", &status);

	return status;
}

int RPCResultSet::get_output_count()
{
    int count = xmlrpc_array_size(env, value);
    if (env->fault_occurred)
    {
        log_error(ZONE, "Error getting output count\r\n");
        return -1;
    }
    return count;
}

bool RPCResultSet::get_output_values(const char *format, ...)
{
	va_list args;
	va_start(args, format);

	xmlrpc_parse_value_va(env, value, format, args);
	if (env->fault_occurred)
	{
		log_error(ZONE, "Error parsing output parameters\n");
		return false;
	}

	va_end(args);
	return true;
}


