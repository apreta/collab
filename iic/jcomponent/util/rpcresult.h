
#ifndef RPCRESULT_H
#define RPCRESULT_H

#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>

// Parses a result set from an RPC call
class RPCResultSet
{
	const char **fields;

	xmlrpc_env *env;
	xmlrpc_value *value;
	xmlrpc_value *rows;

	bool rowset_flag;

	int first_row;
	int chunk_size;
	int total_rows;
	bool more_data;

public:
	RPCResultSet(xmlrpc_env *_env, xmlrpc_value *_value);
	~RPCResultSet();

	// Does this result set contain row data (otherwise will contain a single
	// status value)?
	bool is_rowset();

	// Return chunking info.
	int get_first_index();
	int get_chunk_size();
	int get_total_rows();	// only accurate if server side query gets all rows
	bool more_rows_available();

	// If does not contain row data, may contain a status code or other values.
	int get_output_value(int index);

	// Set meta information for this result set.  Only needed if refering
	// to fields by name.
	void set_column_info(const char *fields[]);

	// Get data from indicated row and column.  Row is between 0 and chunk_size - 1.
	// Column indexes also start at 0.
    const char *get_value(int row, int col);
    const char *get_value(int row, const char *field);
	bool get_values(int row, const char *format, ...);

	// For APIs that don't return a result set, provide access to output parameters
	int get_status();
	int get_output_count();
	bool get_output_values(const char *format, ...);

private:
	void parse();
	int get_column(const char *field);
};

#endif
