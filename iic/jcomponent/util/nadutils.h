/* (c)2002 imidio all rights reserved */

#ifndef NADUTILS_H
#define NADUTILS_H

#include "util.h"

class IString;

IString get_attr_value(nad_t nad, int rnode, const char *attrib, const char *def = NULL);

IString get_child_value(nad_t nad, int rnode, const char *child, const char *def = NULL);

int get_child_value_i(nad_t nad, int rnode, const char *child, int def = 0);

IString get_value(nad_t nad, int node, const char *def = NULL);

int get_root(nad_t nad, int root_depth, const char *root);

#endif
