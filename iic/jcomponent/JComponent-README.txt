JComponent

 - Make jcomponent into a library
 - Use case:
       - Registers handlers for different messages
       - specifies configuration file
       - Invokes library run API

 - APIs:
     - Register handlers for packet types / namespaces
         - Option to build NAD (or dom) for specific message types
	 - Reads configuration from named xml file (host, ports, etc.)
     - Open connection
     - Close session
     - Send error
     - Run:  process IO & signals

 - Issues:
     - Doesn't work if jabber server isn't already running
     - Doesn't fail nicely if jabber server is shutdown
     - Doesn't recover in either case when server is started


 - Packages:
     - MIO
     - XMLParse (expat stuff)
     - Util
          - Memory pools
	  - String "spooling"
	  - NAD
	  - Hashing
	  - Logging
     - Main
