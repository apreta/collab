/* --------------------------------------------------------------------------
 *
 * License
 *
 * The contents of this file are subject to the Jabber Open Source License
 * Version 1.0 (the "JOSL").  You may not copy or use this file, in either
 * source code or executable form, except in compliance with the JOSL. You
 * may obtain a copy of the JOSL at http://www.jabber.org/ or at
 * http://www.opensource.org/.  
 *
 * Software distributed under the JOSL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the JOSL
 * for the specific language governing rights and limitations under the
 * JOSL.
 *
 * Copyrights
 * 
 * Portions created by or assigned to Jabber.com, Inc. are 
 * Copyright (c) 1999-2002 Jabber.com, Inc.  All Rights Reserved.  Contact
 * information for Jabber.com, Inc. is available at http://www.jabber.com/.
 *
 * Portions Copyright (c) 1998-1999 Jeremie Miller.
 * 
 * Acknowledgements
 * 
 * Special thanks to the Jabber Open Source Contributors for their
 * suggestions and support of Jabber.
 * 
 * Alternatively, the contents of this file may be used under the terms of the
 * GNU General Public License Version 2 or later (the "GPL"), in which case
 * the provisions of the GPL are applicable instead of those above.  If you
 * wish to allow use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the JOSL,
 * indicate your decision by deleting the provisions above and replace them
 * with the notice and other provisions required by the GPL.  If you do not
 * delete the provisions above, a recipient may use your version of this file
 * under either the JOSL or the GPL. 
 * 
 * 
 * --------------------------------------------------------------------------*/

#include "jcomponent.h"

/* 
On incoming connections, it's our job to validate any packets we receive on this server

We'll get:
    <db:result to=B from=A>...</db:result>
We verify w/ the dialback process, then we'll send back:
    <db:result type="valid" to=A from=B/>

*/

/* handle new elements */
void _in_startElement(void *arg, const char* name, const char** atts)
{
    conn_t c = (conn_t)arg;
    int i = 0;

    /* process stream header first */
    if(c->depth == 0)
    {
    	chunk_t chunk;

        /* !!! check incoming stream for validity! */
        c->state = state_OPEN;

        log_debug(ZONE,"incoming stream header processing");

        /* reply header */
        chunk = chunk_new(chunk_HEAD,NULL,NULL);
        spooler(chunk->regen,"<stream:stream xmlns='jabber:server' xmlns:stream='http://etherx.jabber.org/streams' xmlns:db='jabber:server:dialback' id='",c->id,"'>",chunk->regen);
        conn_chunk_write(c,chunk_prep(chunk,NULL));

        c->depth++;
        return;
    }

    /* making a new chunk, set the type if we have to */
    if(c->chunk == NULL)
    {
        log_debug(ZONE,"making a new %s chunk",name);

        c->chunk = chunk_new(chunk_PACKET,j_attr(atts,"to"),j_attr(atts,"from"));

        /* dialback protocol flagging */
        if(j_strcmp(name,"db:verify") == 0)
        {
            c->chunk->type = chunk_DBVERIFY;
            c->chunk->dbid = pstrdup(c->chunk->p,j_attr(atts,"id")); /* verifies have an id attrib too */
            c->chunk->cdstr = spool_new(c->chunk->p);
        }
        if(j_strcmp(name,"db:result") == 0)
        {
            c->chunk->type = chunk_DBRESULT;
            c->chunk->cdstr = spool_new(c->chunk->p);
        }

        /* flag special it if it's an error */
        if(j_strcmp(j_attr(atts,"type"),"error") == 0)
            c->chunk->type = chunk_ERR;
    }

    /* if there was a tag before us, close it properly */
    if(c->chunk->eflag == 1)
        spool_add(c->chunk->regen,">");

    /* recreate element and push onto chunk */
    spooler(c->chunk->regen,"<",name,c->chunk->regen);
    while(atts[i] != '\0')
    {
        spool_add(c->chunk->regen," ");
        spool_add(c->chunk->regen,(char*)atts[i]);
        spool_add(c->chunk->regen,"='");
        spool_escape(c->chunk->regen,(char*)atts[i+1],strlen((char*)atts[i+1]));
        spool_add(c->chunk->regen,"'");
        i += 2;
    }

    /* somebody has to close this element */
    c->chunk->eflag = 1;

    /* going deeper */
    c->depth++;
}


void _in_endElement(void *arg, const char* name)
{
    conn_t c = (conn_t)arg;

    /* complete the chunk, if any */
    if(c->chunk != NULL)
    {
        /* no cdata means tiny tag */
        if(c->chunk->eflag == 1)
            spool_add(c->chunk->regen,"/>");
        else
            spooler(c->chunk->regen,"</",name,">",c->chunk->regen);
        c->chunk->eflag = 0;

    }

    /* now we have to do something with our chunk */
    if(c->chunk != NULL && c->depth == 2)
    {
        chunk_t dbv;
        log_debug(ZONE,"got a chunk %s!",name);
        switch(c->chunk->type)
        {
        case chunk_PACKET:
        case chunk_ERR:
            /* make sure this packet's addressing has been validated first, or drop */
            if((int)xhash_get(c->db,c->chunk->dbkey) == 1)
                route_app(c->s2s, chunk_prep(c->chunk,NULL));
            else
                pool_free(c->chunk->p);
            break;
        case chunk_DBVERIFY: /* validate and respond */
            dbv = chunk_new(chunk_DBVERIFY,NULL,NULL);
            if(j_strcmp(spool_print(c->chunk->cdstr),merlin(c->chunk->p,c->s2s->dbsecret,jid_full(c->chunk->from),c->chunk->dbid)) == 0)
            {
                log_debug(ZONE,"dialback verify request validated to %s from %s",jid_full(c->chunk->to),jid_full(c->chunk->from));
                spooler(dbv->regen,"<db:verify type='valid' to='",jid_full(c->chunk->from),"' from='",jid_full(c->chunk->to),"' id='",c->chunk->dbid,"'/>",dbv->regen);
            }else{
                log_debug(ZONE,"dialback verify request failed to %s from %s",jid_full(c->chunk->to),jid_full(c->chunk->from));
                spooler(dbv->regen,"<db:verify type='invalid' to='",jid_full(c->chunk->from),"' from='",jid_full(c->chunk->to),"' id='",c->chunk->dbid,"'/>",dbv->regen);
            }
            conn_chunk_write(c,chunk_prep(dbv,NULL));
            pool_free(c->chunk->p);
            break;
        case chunk_DBRESULT: /* generate and send a new db verify for this request */
            dbv = chunk_new(chunk_DBVERIFY,jid_full(c->chunk->from),jid_full(c->chunk->to));
            spooler(dbv->regen,
                "<db:verify to='",jid_full(c->chunk->from),"' from='",jid_full(c->chunk->to),"' id='",c->id,"'>",
                spool_print(c->chunk->cdstr),
                "</db:verify>",dbv->regen);
            route(c->s2s,dbv);
            break;
        default: /* should be nothing */
            pool_free(c->chunk->p);
            break;
        }
        c->chunk = NULL;
    }

    /* going up for air */
    c->depth--;

    /* if we processed the closing stream root, flag to close l8r */
    if(c->depth == 0)
        c->depth = -1; /* we can't close here, expat gets free'd on close :) */
}


void _in_charData(void *arg, const char *str, int len)
{
    conn_t c = (conn_t)arg;

    /* if we're in the root of the stream the CDATA is irrelevant */
    if(c->chunk == NULL) return;

    /* be sure to end the opening tag */
    if(c->chunk->eflag == 1)
    {
        spool_add(c->chunk->regen, ">");
        c->chunk->eflag = 0;
    }

    /* expat unescapes, we re-escape, *sigh* */
    spool_escape(c->chunk->regen, (char*)str, len);

    /* additionally store this seperately for dialbacks */
    if(c->chunk->cdstr != NULL)
        spool_escape(c->chunk->cdstr, (char*)str, len);
}

/* handle the incoming client mio events */
int in_io(mio_t m, mio_action_t a, int fd, void *data, void *arg)
{
    char buf[1024]; /* !!! make static when not threaded? move into conn_st? */
    int len;
    conn_t c = (conn_t)arg;
    char id[64];
    chunk_t cur, next;

    log_debug(ZONE,"io action %d with fd %d",a,fd);

    switch(a)
    {
    case action_ACCEPT:
        log_debug(ZONE,"new client conn %d from ip %s",fd,(char*)data);

        if (connection_rate_check((s2s_t)arg, (char*)data))
        {
            /* We had a bad rate, dump them (send an error?) */
            log_debug(ZONE, "rate limit is bad for %s, closing", (char*)data);
            /* return 1 to get rid of this fd */
            return 1;
        }

        /* set up the new incoming conn */
        c = conn_new((s2s_t)arg, fd);
        mio_app(m, fd, in_io, (void*)c);

        /* set up expat callbacks */
        XML_SetUserData(c->expat, (void*)c);
        XML_SetElementHandler(c->expat, (void*)_in_startElement, (void*)_in_endElement);
        XML_SetCharacterDataHandler(c->expat, (void*)_in_charData);

        /* make random id and put in incoming hash */
        shahash_r(NULL,id);
        c->id = pstrdup(c->p,id);
        xhash_put(c->s2s->in,c->id,(void*)c);

        /* get read events */
        mio_read(m, fd);
        break;

    case action_READ:

        /* read a chunk at a time */
        len = _read_actual(c, fd, buf, 1024);
        return conn_read(c, buf, len);

    case action_WRITE:

        /* let's break this into another function, it's a bit messy */
        return conn_write(c);

    case action_CLOSE:

        /* get us out of the hashtable first */
        xhash_zap(c->s2s->in,c->id);

        /* !!! c->count logging here? */

        /* ditch anything in this writeq */
        for(cur = c->writeq; cur != NULL; cur = next)
        {
            next = cur->next;
            pool_free(cur->p);
        }

        conn_free(c);
        break;
    }
    return 0;
}

