/* --------------------------------------------------------------------------
 *
 * License
 *
 * The contents of this file are subject to the Jabber Open Source License
 * Version 1.0 (the "JOSL").  You may not copy or use this file, in either
 * source code or executable form, except in compliance with the JOSL. You
 * may obtain a copy of the JOSL at http://www.jabber.org/ or at
 * http://www.opensource.org/.  
 *
 * Software distributed under the JOSL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the JOSL
 * for the specific language governing rights and limitations under the
 * JOSL.
 *
 * Copyrights
 * 
 * Portions created by or assigned to Jabber.com, Inc. are 
 * Copyright (c) 1999-2002 Jabber.com, Inc.  All Rights Reserved.  Contact
 * information for Jabber.com, Inc. is available at http://www.jabber.com/.
 *
 * Portions Copyright (c) 1998-1999 Jeremie Miller.
 * 
 * Acknowledgements
 * 
 * Special thanks to the Jabber Open Source Contributors for their
 * suggestions and support of Jabber.
 * 
 * Alternatively, the contents of this file may be used under the terms of the
 * GNU General Public License Version 2 or later (the "GPL"), in which case
 * the provisions of the GPL are applicable instead of those above.  If you
 * wish to allow use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the JOSL,
 * indicate your decision by deleting the provisions above and replace them
 * with the notice and other provisions required by the GPL.  If you do not
 * delete the provisions above, a recipient may use your version of this file
 * under either the JOSL or the GPL. 
 * 
 * 
 * --------------------------------------------------------------------------*/

#include "jcomponent.h"

/* 
On outgoing connections, we need to send a result and any verifies, and watch for their responses 

We'll send:
    <db:result to=B from=A>...</db:result>
We'll get back:
    <db:result type="valid" to=A from=B/>

We'll send:
    <db:verify to=B from=A id=asdf>...</db:verify>
We'll get back:
    <db:verify type="valid" to=A from=B id=asdf/>

*/

/* handle new elements */
void _out_startElement(void *arg, const char* name, const char** atts)
{
    conn_t c = (conn_t)arg;
    int i = 0;

    if(c->depth < 0) return;

    /* process stream header first */
    if(c->depth == 0)
    {
        /* !!! check incoming stream for validity! */

        if(xhash_get(c->s2s->in,j_attr(atts,"id")) != NULL)
        {
            log_debug(ZONE,"mirror mirror, we've connected to ourselves!");
            c->depth = -1;
            return;
        }

        /* track the incoming id for db key generation */
	c->id = pstrdup(c->p,j_attr(atts,"id"));

        /* now that the id is set, there may be waiting chunks to send db requests for */
        mio_write(c->s2s->mio, c->fd);

        c->depth++;
        return;
    }

    /* making a new chunk, set the type if we have to */
    if(c->chunk == NULL)
    {
        c->chunk = chunk_new(chunk_PACKET,j_attr(atts,"to"),j_attr(atts,"from"));

        /* flag special it if it's an error */
        if(j_strcmp(j_attr(atts,"type"),"error") == 0)
            c->chunk->type = chunk_ERR;

        /* dialback protocol flagging */
        if(j_strcmp(name,"db:verify") == 0)
        {
            chunk_t res;
            conn_t cin;

            /* we have all that we need, forward on a result to the incoming conn, if any */
            log_debug(ZONE,"processing %s db:verify to %s from %s",j_attr(atts,"type"),jid_full(c->chunk->to),jid_full(c->chunk->from));
            if((cin = xhash_get(c->s2s->in,j_attr(atts,"id"))) != NULL)
            {
                res = chunk_new(chunk_DBRESULT,NULL,NULL);
                spooler(res->regen,"<db:result type='",j_attr(atts,"type"),"' to='",jid_full(c->chunk->from),"' from='",jid_full(c->chunk->to),"'/>",res->regen);
                conn_chunk_write(cin,chunk_prep(res,NULL));

                /* let the incoming conn accept packets from this domain */
                if(j_strcmp(j_attr(atts,"type"),"valid") == 0)
                    xhash_put(cin->db,pstrdup(cin->p,c->chunk->dbkey),(void*)1);

            }

            /* flag the incoming chunk so it can be dropped later :) */
            c->chunk->type = chunk_DBVERIFY;
        }
        if(j_strcmp(name,"db:result") == 0)
        {
            /* we really need the dbkey reversed, reset the temp chunk */
            pool_free(c->chunk->p);
            c->chunk = chunk_new(chunk_DBRESULT,j_attr(atts,"from"),j_attr(atts,"to"));

            /* depending on the validity, update the db state for this key */
            if(j_strcmp(j_attr(atts,"type"),"valid") == 0)
                xhash_put(c->db,pstrdup(c->p,c->chunk->dbkey),(void*)2);
            else
                xhash_put(c->db,pstrdup(c->p,c->chunk->dbkey),(void*)3);

            /*  try more write events for any waiting packets */
            mio_write(c->s2s->mio, c->fd);
        }

    }

    /* if there was a tag before us, close it properly */
    if(c->chunk->eflag == 1)
        spool_add(c->chunk->regen,">");

    /* recreate element and push onto chunk */
    spooler(c->chunk->regen,"<",name,c->chunk->regen);
    while(atts[i] != '\0')
    {
        spool_add(c->chunk->regen," ");
        spool_add(c->chunk->regen,(char*)atts[i]);
        spool_add(c->chunk->regen,"='");
        spool_escape(c->chunk->regen,(char*)atts[i+1],strlen((char*)atts[i+1]));
        spool_add(c->chunk->regen,"'");
        i += 2;
    }

    /* somebody has to close this element */
    c->chunk->eflag = 1;

    /* going deeper */
    c->depth++;
}


void _out_endElement(void *arg, const char* name)
{
    conn_t c = (conn_t)arg;

    /* complete the chunk, if any */
    if(c->chunk != NULL)
    {
        /* no cdata means tiny tag */
        if(c->chunk->eflag == 1)
            spool_add(c->chunk->regen,"/>");
        else
            spooler(c->chunk->regen,"</",name,">",c->chunk->regen);
        c->chunk->eflag = 0;

    }

    /* now we have to do something with our chunk */
    if(c->chunk != NULL && c->depth == 2)
    {
        if(c->chunk->type == chunk_PACKET)
        {
            log_debug(ZONE,"got a chunk, we really shouldn't be!");
            conn_chunk_write(c,chunk_prep(c->chunk,"Invalid Packet"));
        }else{
            pool_free(c->chunk->p);
        }
        c->chunk = NULL;
    }

    /* going up for air */
    c->depth--;

    /* if we processed the closing stream root, flag to close l8r */
    if(c->depth == 0)
        c->depth = -1; /* we can't close here, expat gets free'd on close :) */
}


void _out_charData(void *arg, const char *str, int len)
{
    conn_t c = (conn_t)arg;

    /* if we're in the root of the stream the CDATA is irrelevant */
    if(c->chunk == NULL) return;

    /* be sure to end the opening tag */
    if(c->chunk->eflag == 1)
    {
        spool_add(c->chunk->regen, ">");
        c->chunk->eflag = 0;
    }

    /* expat unescapes, we re-escape, *sigh* */
    spool_escape(c->chunk->regen, (char*)str, len);
}

/* create an outgoing connection */
conn_t out_conn(s2s_t s2s, char *ip)
{
    char *cport;
    int port, fd;
    conn_t c;
    chunk_t chunk;

    /* check ip and locate port in it */
    if(ip == NULL || (cport = strchr(ip,':')) == NULL) return NULL;
    port = atoi(cport+1);

    /* temporarily chop off ip and try to connect */
    *cport = '\0';
    fd = mio_connect(s2s->mio, port, ip, NULL, NULL);
    *cport = ':';
    if(fd <= 0) return NULL;

    log_debug(ZONE,"new outgoing connection %d to %s",fd,ip);

    /* ok, make new conn now */
    c = conn_new(s2s, fd);
    c->state = state_OUT;
    mio_app(s2s->mio, fd, out_io, (void*)c);

    /* set up expat callbacks */
    XML_SetUserData(c->expat, (void*)c);
    XML_SetElementHandler(c->expat, (void*)_out_startElement, (void*)_out_endElement);
    XML_SetCharacterDataHandler(c->expat, (void*)_out_charData);

    /* copy the ip and put in outgoing hash (caller checked there for an existing conn first!) */
    c->ip = pstrdup(c->p,ip);
    xhash_put(s2s->out,c->ip,(void*)c);

    /* write header */
    chunk = chunk_new(chunk_HEAD,NULL,NULL);
    spooler(chunk->regen,"<stream:stream xmlns='jabber:server' xmlns:stream='http://etherx.jabber.org/streams' xmlns:db='jabber:server:dialback'>",chunk->regen);
    conn_chunk_write(c,chunk_prep(chunk,NULL));

    /* get read events */
    mio_read(s2s->mio, fd);

    return c;
}

/* handle the incoming client mio events */
int out_io(mio_t m, mio_action_t a, int fd, void *data, void *arg)
{
    char buf[1024]; /* !!! make static when not threaded? move into conn_st? */
    int len;
    conn_t c = (conn_t)arg;

    log_debug(ZONE,"io action %d with fd %d",a,fd);

    switch(a)
    {
    case action_ACCEPT:
        break;

    case action_READ:

        /* read a chunk at a time */
        len = _read_actual(c, fd, buf, 1024);
        return conn_read(c, buf, len);

    case action_WRITE:

        /* let's break this into another function, it's a bit messy */
        return conn_write(c);

    case action_CLOSE:

        /* get us out of the hashtable first */
        xhash_zap(c->s2s->out,c->ip);

        /* !!! c->count logging here? */

        /* try to reroute the packets */
        if(c->writeq != NULL)
        {
            chunk_t cur, next;
            for(cur = c->writeq; cur != NULL; cur = next)
            {
                next = cur->next;
                if(cur->type == chunk_PACKET || cur->type == chunk_DBVERIFY || cur->type == chunk_ERR)
                    route(c->s2s,cur);
                else
                    pool_free(cur->p);
            }
        }

        conn_free(c);
        break;
    }
    return 0;
}


