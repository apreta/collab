/* --------------------------------------------------------------------------
 *
 * License
 *
 * The contents of this file are subject to the Jabber Open Source License
 * Version 1.0 (the "JOSL").  You may not copy or use this file, in either
 * source code or executable form, except in compliance with the JOSL. You
 * may obtain a copy of the JOSL at http://www.jabber.org/ or at
 * http://www.opensource.org/.  
 *
 * Software distributed under the JOSL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the JOSL
 * for the specific language governing rights and limitations under the
 * JOSL.
 *
 * Copyrights
 * 
 * Portions created by or assigned to Jabber.com, Inc. are 
 * Copyright (c) 1999-2002 Jabber.com, Inc.  All Rights Reserved.  Contact
 * information for Jabber.com, Inc. is available at http://www.jabber.com/.
 *
 * Portions Copyright (c) 1998-1999 Jeremie Miller.
 * 
 * Acknowledgements
 * 
 * Special thanks to the Jabber Open Source Contributors for their
 * suggestions and support of Jabber.
 * 
 * Alternatively, the contents of this file may be used under the terms of the
 * GNU General Public License Version 2 or later (the "GPL"), in which case
 * the provisions of the GPL are applicable instead of those above.  If you
 * wish to allow use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the JOSL,
 * indicate your decision by deleting the provisions above and replace them
 * with the notice and other provisions required by the GPL.  If you do not
 * delete the provisions above, a recipient may use your version of this file
 * under either the JOSL or the GPL. 
 * 
 * --------------------------------------------------------------------------*/

/*
   MIO -- Managed Input/Output
   ---------------------------
*/

#ifdef _WIN32
#include <winsock2.h>
#include <stdio.h>
#include <io.h>
#endif

#include "mio.h"
#ifdef MIO_POLL
#include "mio_poll.h"
#endif
#ifdef MIO_SELECT
#include "mio_select.h"
#endif

#ifdef LINUX
static pid_t run_pid = 0;
#endif

#include "../jcomponent.h"
#include "../util/log.h"
#include "../util/port.h"

/* our internal wrapper around a fd */
typedef enum { 
    type_CLOSED = 0x00, 
    type_NORMAL = 0x01, 
    type_LISTEN = 0x02, 
    type_CONNECT = 0x10, 
    type_CONNECT_READ = 0x11,
    type_CONNECT_WRITE = 0x12
} mio_type_t;

struct mio_fd_st
{
	int fd;
    mio_type_t type;
    /* app even handler and data */
    mio_handler_t app;
    void *arg;
};

/* now define our master data type */
struct mio_st
{
    struct mio_fd_st *fds;
    int maxfd;
    int highfd;
    MIO_VARS
};

/* lazy factor */
#ifdef LINUX
#define FD_TO_INDEX(m,f) f
#define INDEX_TO_FD(m,i) i
#else // _WIN32
/** Right now, we only need one connection */
#define FD_TO_INDEX(m,f) 0
#define INDEX_TO_FD(m,i) (m->fds[i].fd)
#endif

#define FD(m,f) m->fds[FD_TO_INDEX(m,f)]
#define ACT(m,f,a,d) (*(FD(m,f).app))(m,a,f,d,FD(m,f).arg)

/* set up functions to monitor sockets using select or poll */
MIO_FUNCS

void noop_interrupt(int sig)
{
    log_debug(ZONE, "mio interrupt");
}

void mio_set_nonblock(int fd, int act)
{
#ifdef LINUX
	int flags;

	flags = fcntl(fd, F_GETFL, 0);
	if(flags < 0) return;

	if(act)
		flags |= O_NONBLOCK;
	else
		flags &= ~O_NONBLOCK;

	fcntl(fd, F_SETFL, flags);
#else // _WIN32
	long flag = act;
	ioctlsocket(fd, FIONBIO, &flag);
#endif
}

/* internal close function */
void mio_close(mio_t m, int fd)
{
    log_debug(ZONE,"actually closing fd #%d",fd);

    /* take out of poll sets */
    MIO_REMOVE_FD(m, fd);

    /* let the app know, it must process any waiting write data it has and free it's arg */
    ACT(m, fd, action_CLOSE, NULL);

    /* close the socket, and reset all memory */
    _close(fd);
    memset(&FD(m,fd), 0, sizeof(struct mio_fd_st));
}

/* internally accept an incoming connection from a listen sock */
void _mio_accept(mio_t m, int fd)
{
    struct sockaddr_in serv_addr;
    size_t addrlen = sizeof(serv_addr);
    int newfd, dupfd;
    char ip[16];

    log_debug(ZONE, "accepting on fd #%d", fd);

    /* pull a socket off the accept queue and check */
    newfd = accept(fd, (struct sockaddr*)&serv_addr, (int*)&addrlen);
    if(newfd <= 0) return;

    snprintf(ip,16,"%s",inet_ntoa(serv_addr.sin_addr));
    log_debug(ZONE, "new socket accepted fd #%d, %s:%d", newfd, ip, ntohs(serv_addr.sin_port));

    /* set up the entry for this new socket */
    if(mio_fd(m, newfd, FD(m,fd).app, FD(m,fd).arg) < 0)
    {
        /* too high, try and get a lower fd */
        dupfd = dup(newfd);
        _close(newfd);

        if(dupfd < 0 || mio_fd(m, dupfd, FD(m,fd).app, FD(m,fd).arg) < 0) {
            log_debug(ZONE,"failed to add fd");
            if(dupfd >= 0) _close(dupfd);

            return;
        }

        newfd = dupfd;
    }

    /* tell the app about the new socket, if they reject it clean up */
    if (ACT(m, newfd, action_ACCEPT, ip))
    {
        log_debug(ZONE, "accept was rejected for %s:%d", ip, newfd);
        MIO_REMOVE_FD(m, newfd);

        /* close the socket, and reset all memory */
        _close(newfd);
        memset(&FD(m, newfd), 0, sizeof(struct mio_fd_st));
    }

    return;
}

/* internally change a connecting socket to a normal one */
void _mio_connect(mio_t m, int fd)
{
    mio_type_t type = FD(m,fd).type;

    log_debug(ZONE, "connect processing for fd #%d", fd);

    /* reset type and clear the "write" event that flags connect() is done */
    FD(m,fd).type = type_NORMAL;
    MIO_UNSET_WRITE(m,fd);

    /* if the app had asked to do anything in the meantime, do those now */
    if(type & type_CONNECT_READ) mio_read(m,fd);
    if(type & type_CONNECT_WRITE) mio_write(m,fd);
}

/* add and set up this fd to this mio */
int mio_fd(mio_t m, int fd, mio_handler_t app, void *arg)
{
    log_debug(ZONE, "adding fd #%d", fd);

    if(FD_TO_INDEX(m, fd) >= m->maxfd)
    {
        log_debug(ZONE,"fd to high");
        return -1;
    }

    /* ok to process this one, welcome to the family */
	FD(m,fd).fd = fd;
    FD(m,fd).type = type_NORMAL;
    FD(m,fd).app = app;
    FD(m,fd).arg = arg;
    MIO_INIT_FD(m, fd);

    /* set the socket to non-blocking */
	mio_set_nonblock(fd, 1);

    /* track highest used */
    if(FD_TO_INDEX(m,fd) > m->highfd) m->highfd = FD_TO_INDEX(m,fd);

    return fd;
}

/* reset app stuff for this fd */
void mio_app(mio_t m, int fd, mio_handler_t app, void *arg)
{
    FD(m,fd).app = app;
    FD(m,fd).arg = arg;
}

/* main select loop runner */
int mio_run(mio_t m, int timeout)
{
    int retval, fd, i;
    int interrupted = 0;

//    log_debug(ZONE, "mio running for %d", timeout);

    /* wait for a socket event */
    retval = MIO_CHECK(m, timeout);

    /* nothing to do */
    if(retval == 0) return 0;

    /* an error */
    if(retval < 0)
    {
        if (_error == _EINTR)
        {
            log_debug(ZONE, "mio interrupted for sending data");
            interrupted = 1;
        }
        else
        {
            log_debug(ZONE, "MIO_CHECK returned an error (%d)", _error);
            return 0;
        }
    }

//    log_debug(ZONE,"mio working: %d",retval);

    /* loop through the sockets, check for stuff to do */
	for(i = 0; i <= m->highfd; i++)
    {
		fd = INDEX_TO_FD(m,i);

//		log_debug(ZONE, "%d %d", FD(m,fd).type, m->pfds[fd].revents);
		
        /* skip dead slots */
        if(FD(m,fd).type == type_CLOSED) continue;

        /* new conns on a listen socket */
        if(FD(m,fd).type == type_LISTEN && MIO_CAN_READ(m, fd))
        {
            _mio_accept(m, fd);
            continue;
        }

        /* check for connecting sockets */
        if((FD(m,fd).type & type_CONNECT) && MIO_CAN_WRITE(m, fd))
        {
            _mio_connect(m, fd);
            continue;
        }

		/* check for failed connecting socket */
		if (FD(m,fd).type & type_CONNECT && MIO_CONN_ERR(m, fd))
		{
			log_debug(ZONE, "Unable to connect");
			mio_close(m, fd);
			continue;
		}
		
        /* read from ready sockets */
        if(FD(m,fd).type == type_NORMAL && MIO_CAN_READ(m, fd))
        {
            /* if they don't want to read any more right now */
            if(ACT(m, fd, action_READ, NULL) == 0)
                MIO_UNSET_READ(m, fd);
        }

        /* write to ready sockets */
        if(FD(m,fd).type == type_NORMAL && (MIO_CAN_WRITE(m, fd) || interrupted))
        {
            /* don't wait for writeability if nothing to write anymore */
            if(ACT(m, fd, action_WRITE, NULL) == 0)
                MIO_UNSET_WRITE(m, fd);
        }
    }
	
	return 1;
}

/* eve */
mio_t mio_new(int maxfd)
{
    mio_t m;

    /* allocate and zero out main memory */
    if((m = malloc(sizeof(struct mio_st))) == NULL) return NULL;
    if((m->fds = malloc(sizeof(struct mio_fd_st) * maxfd)) == NULL)
    {
        log_debug(ZONE,"internal error creating new mio");
        free(m);
        return NULL;
    }
    memset(m->fds, 0, sizeof(struct mio_fd_st) * maxfd);

    /* set up our internal vars */
    m->maxfd = maxfd;
    m->highfd = 0;

    MIO_INIT_VARS(m);

    return m;
}

/* adam */
void mio_free(mio_t m)
{
    MIO_FREE_VARS(m);

    free(m->fds);
    free(m);
}

/* start processing read events */
void mio_read(mio_t m, int fd)
{
    if(m == NULL || fd < 0) return;

    /* if connecting, do this later */
    if(FD(m,fd).type & type_CONNECT)
    {
        FD(m,fd).type |= type_CONNECT_READ;
        return;
    }

    MIO_SET_READ(m, fd);
}

/* try writing to the socket via the app */
void mio_write(mio_t m, int fd)
{
    if(m == NULL || fd < 0) return;

    /* if connecting, do this later */
    if(FD(m,fd).type & type_CONNECT)
    {
        FD(m,fd).type |= type_CONNECT_WRITE;
        return;
    }

    if(ACT(m, fd, action_WRITE, NULL) == 0) return;

    /* not all written, do more l8r */
    MIO_SET_WRITE(m, fd);
}

/* Use signal to dispatch write to main thread.
   Not using function currently ** SIG */
void mio_setwrite(mio_t m, int fd)
{
    if(m == NULL || fd < 0) return;

    /* if connecting, do this later */
    if(FD(m,fd).type & type_CONNECT)
    {
        FD(m,fd).type |= type_CONNECT_WRITE;
        return;
    }

#if LINUX
    // calling from worker thread, just signal main thread data is available
    kill(run_pid, SIGUSR1);
#endif
}

/* set up a listener in this mio w/ this default app/arg */
int mio_listen(mio_t m, int port, char *sourceip, mio_handler_t app, void *arg)
{
    int fd, flag = 1;
    unsigned long int ip = 0;
    struct sockaddr_in sa;

    if(m == NULL) return -1;

#ifdef LINUX
    run_pid = getpid();
    signal(SIGUSR1, noop_interrupt);
    siginterrupt(SIGUSR1, 1);
#endif

    log_debug(ZONE, "mio to listen on %d [%s]", port, sourceip);

    /* if we specified an ip to bind to */
    if(sourceip != NULL)
        ip = inet_addr(sourceip);

    /* attempt to create a socket */
    if((fd = socket(AF_INET,SOCK_STREAM,0)) < 0) return -1;
    if(setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char*)&flag, sizeof(flag)) < 0) return -1;

    /* set up and bind address info */
    memset(&sa, 0, sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    if(ip > 0) sa.sin_addr.s_addr = ip;
    if(bind(fd,(struct sockaddr*)&sa,sizeof(sa)) < 0)
    {
        _close(fd);
        return -1;
    }

    /* start listening with a max accept queue of 10 */
    if(listen(fd, 10) < 0)
    {
        _close(fd);
        return -1;
    }

    /* now set us up the bomb */
    if(mio_fd(m, fd, app, arg) < 0)
    {
        _close(fd);
        return -1;
    }

    FD(m,fd).type = type_LISTEN;
    /* by default we read for new sockets */
    mio_read(m,fd);

    return fd;
}

/* create an fd and connect to the given ip/port */
int mio_connect(mio_t m, int port, char *hostip, mio_handler_t app, void *arg)
{
    int fd, flag;
    unsigned long int ip = 0;
    struct sockaddr_in sa;

    if(m == NULL || port <= 0 || hostip == NULL) return -1;

#ifdef LINUX
    run_pid = getpid();
    signal(SIGUSR1, noop_interrupt);
    siginterrupt(SIGUSR1, 1);
#endif

    log_debug(ZONE, "mio connecting to %s:%d",hostip,port);

    /* convert the hostip */
    if((ip = inet_addr(hostip)) < 0) return -1;

    /* attempt to create a socket */
    if((fd = socket(AF_INET,SOCK_STREAM,0)) < 0) return -1;

    /* set the socket to non-blocking before connecting */
	mio_set_nonblock(fd, 1);

    /* set up address info */
    memset(&sa, 0, sizeof(sa));
    sa.sin_family = AF_INET;
    sa.sin_port = htons(port);
    sa.sin_addr.s_addr = ip;

    /* try to connect */
    flag = connect(fd,(struct sockaddr*)&sa,sizeof(sa));

    //log_debug(ZONE, "connect returned %d and %s", flag, strerror(errno));

    /* already connected?  great! */
    if(flag == 0 && mio_fd(m,fd,app,arg) == fd) return fd;

    /* gotta wait till later */
    if(flag == -1 && (_error == _EINPROGRESS || _error == _EWOULDBLOCK) && mio_fd(m,fd,app,arg) == fd)
    {
        log_debug(ZONE, "connect processing non-blocking mode");

        FD(m,fd).type = type_CONNECT;
        MIO_SET_WRITE(m,fd);
        return fd;
    }

    /* bummer dude */
    _close(fd);
    return -1;
}

