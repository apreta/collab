/* (c) 2002 imidio all rights reserved */

/* External APIs for users of jcomponent library */

#ifndef JCOMPONENT_EXT_H
#define JCOMPONENT_EXT_H

#ifdef __cplusplus
extern "C" {
#endif

typedef int (*jc_connected_fn)();

int jc_init(config_t config, const char *component_name);
int jc_run(int non_blocking);

void jc_set_connected_callback(jc_connected_fn);

#ifdef __cplusplus
}
#endif

#endif
