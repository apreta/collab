/* --------------------------------------------------------------------------
 *
 * License
 *
 * The contents of this file are subject to the Jabber Open Source License
 * Version 1.0 (the "JOSL").  You may not copy or use this file, in either
 * source code or executable form, except in compliance with the JOSL. You
 * may obtain a copy of the JOSL at http://www.jabber.org/ or at
 * http://www.opensource.org/.  
 *
 * Software distributed under the JOSL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the JOSL
 * for the specific language governing rights and limitations under the
 * JOSL.
 *
 * Copyrights
 * 
 * Portions created by or assigned to Jabber.com, Inc. are 
 * Copyright (c) 1999-2002 Jabber.com, Inc.  All Rights Reserved.  Contact
 * information for Jabber.com, Inc. is available at http://www.jabber.com/.
 *
 * Portions Copyright (c) 1998-1999 Jeremie Miller.
 * 
 * Acknowledgements
 * 
 * Special thanks to the Jabber Open Source Contributors for their
 * suggestions and support of Jabber.
 * 
 * Alternatively, the contents of this file may be used under the terms of the
 * GNU General Public License Version 2 or later (the "GPL"), in which case
 * the provisions of the GPL are applicable instead of those above.  If you
 * wish to allow use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the JOSL,
 * indicate your decision by deleting the provisions above and replace them
 * with the notice and other provisions required by the GPL.  If you do not
 * delete the provisions above, a recipient may use your version of this file
 * under either the JOSL or the GPL. 
 * 
 * 
 * --------------------------------------------------------------------------*/

#include "jcomponent.h"
#include <sys/wait.h>

/* this is in srv.c, does the actual SRV/A lookups */
char* srv_lookup(pool p, const char* service, const char* domain, char *port);

/* this is in route.c, how we notify when lookups return */
void route_ip(s2s_t s2s, char *name, char *ip);

/* child will just die after any signal */
void _dnsrvc_signal(int sig)
{
    log_debug(ZONE,"child exiting on signal %s",sig);
    exit(0);
}

/* process incoming requests to this child */
void _dnsrvc_startElement(void *arg, const char* name, const char** atts)
{
    char *host, *ip, *res;
    pool p;
    int fd = (int)arg;

    if((host = j_attr(atts,"x")) == NULL) return;

    log_debug(ZONE,"got a lookup requst %s",host);

    /* make a temp pool used by srv when building/returning the result */
    p = pool_new();
    if((ip = srv_lookup(p,"_jabber._tcp",host,"5269")) == NULL)
    {
        log_debug(ZONE,"SRV failed, trying A");
        if((ip = srv_lookup(p,NULL,host,"5269")) == NULL)
        {
            log_debug(ZONE,"resolution failed!");
        }
    }

    /* write out the response */
    res = spools(p,"<x x='",host,"' ip='",ip,"'/>",p);
    log_debug(ZONE,"writing response: %s",res);
    write(fd,res,strlen(res));

    pool_free(p);
}

void _dnsrvc_main(s2s_t s2s, int fd)
{
    int len;
    char buf[1024];
    XML_Parser expat;

    log_debug(ZONE,"child starting");

    /* set up a parser */
    expat = XML_ParserCreate(NULL);
    XML_SetUserData(expat, (void*)fd);
    XML_SetElementHandler(expat, (void*)_dnsrvc_startElement, NULL);

    /* Loop forever, processing data and parsing it */
    while (1)
    {
        len = read(fd, &buf, 1024);

        if(len <= 0 || !XML_Parse(expat,buf,len,0))
        {
            log_debug(ZONE,"read error on coprocess(%d): %d %s",getppid(),errno,strerror(errno));
            break;
        }
    }

    /* child is out of loop... normal exit so parent will start us again */
    log_debug(ZONE, "child out of loop.. exiting normal");
    
    return;
}

/* process data coming back from the child */
void _dnsrv_startElement(void *arg, const char* name, const char** atts)
{
    /* simple job here, process the returned name/ip attribs (if any) */
    route_ip((s2s_t)arg,j_attr(atts,"x"),j_attr(atts,"ip"));
}

/* read/parse data from the child */
int _dnsrv_io(mio_t m, mio_action_t a, int fd, void *data, void *arg)
{
    char buf[1024];
    int len, ret;
    s2s_t s2s = (s2s_t)arg;
    chunk_t cur, next;

    log_debug(ZONE,"child action %d with fd %d",a,fd);

    switch(a)
    {
    case action_READ:

        while(1)
        {
            len = read(fd, buf, 1024);
            if((ret = conn_read(s2s->dnsrv, buf, len)) != 1 || len < 1024) break;
        }
        return 1;

    case action_WRITE:

        return conn_write(s2s->dnsrv);

    case action_CLOSE:

        /* empty write queue */
        for(cur = s2s->dnsrv->writeq; cur != NULL; cur = next)
        {
            next = cur->next;
            pool_free(cur->p);
        }

        /* make sure child is dead, reap */
        if(s2s->cpid > 0)
        {
            kill(s2s->cpid,SIGKILL);
            waitpid(s2s->cpid, NULL, WNOHANG);
            s2s->cpid = 0;
        }

        conn_free(s2s->dnsrv);
        s2s->dnsrv = NULL;
        break;

    case action_ACCEPT:
        break;
    }
    return 0;
}


/* try to start a child */
int _dnsrv_child(s2s_t s2s)
{
    int fds[2];

    /* make a pair of sockets for us and the child, we're [0] and child is [1] */
    if(socketpair(AF_UNIX, SOCK_STREAM, 0, fds) != 0) return -1;

    /* first make sure we can track this fd */
    if(mio_fd(s2s->mio, fds[0], _dnsrv_io, (void*)s2s) < 0)
    {
        close(fds[0]);
        close(fds[1]);
        return -1;
    }

    /* set up */
    s2s->dnsrv = conn_new(s2s,fds[0]);
    XML_SetUserData(s2s->dnsrv->expat, (void*)s2s);
    XML_SetElementHandler(s2s->dnsrv->expat, (void*)_dnsrv_startElement, NULL);
    mio_read(s2s->mio,fds[0]);

    /* try to fork us a child */
    log_debug(ZONE,"testing %X",gethostbyname("jabber.to"));
    s2s->cpid = fork();
    if(s2s->cpid < 0)
    {   /* error -- could not fork the child */
        mio_close(s2s->mio, fds[0]);
        return -1;
    }else if(s2s->cpid == 0){
        /* child */
        write(fds[1],"<x>",3);
        signal(SIGHUP,_dnsrvc_signal);
        signal(SIGINT,_dnsrvc_signal);
        signal(SIGTERM,_dnsrvc_signal);
        _dnsrvc_main(s2s,fds[1]);
	exit(0);  /* terminating child */
    }						    
    
    /* parent */
    write(fds[0],"<x>",3);
    return 1;
}


/* lookup request */
void dnsrv_lookup(s2s_t s2s, char *name)
{
    chunk_t chunk;

    /* make sure we have a child to look up requests first, else bail right away */
    if(s2s->dnsrv == NULL && _dnsrv_child(s2s) < 0)
        route_ip(s2s,name,NULL);

    /* a bit heavy, but we'll just use the normal conn/chunk stuff to send to the child */
    chunk = chunk_new(chunk_HEAD,NULL,NULL);
    spooler(chunk->regen,"<x x='",name,"'/>",chunk->regen);
    conn_chunk_write(s2s->dnsrv,chunk_prep(chunk,NULL));
}


