/* --------------------------------------------------------------------------
 *
 * Process incoming iq packet, expeted to be RPC request.
 *
 * Copyrights
 * (C) 2002 - imidio
 *
 * --------------------------------------------------------------------------*/

#include "jcomponent.h"
#include "rpc.h"
#include "util/log.h"

static int packet_counter = 0;


/* Create async request */
async_packet_t create_request(s2s_t s2s, chunk_t chunk, async_packet_fn exec, async_packet_fn complete)
{
    async_packet_t pkt = (async_packet_t) malloc(sizeof(struct async_packet_st));
    if (pkt != NULL)
    {
        memset(pkt, 0, sizeof(struct async_packet_st));

        pkt->chunk = chunk;
        pkt->s2s = s2s;
        pkt->exec = exec;
        pkt->complete = complete;
        pkt->async = 0;
    }

    return pkt;
}

/* Free request object */
void free_request(async_packet_t pkt)
{
    // Free source chunk.  output chunk is freed after writing.
    if (pkt->chunk)
        pool_free(pkt->chunk->p);
    free(pkt);
}

/* Parse packet type (from namespace of iq packet)
   Also figure out where inner element starts and ends.
*/
char *parse_packet(chunk_t chunk)
{
	static char type[128];
	int type_len;
	char *start;
	char *end;
	char *data;

	data = spool_print(chunk->regen);
	if (data == NULL)
		return NULL;

	// Find start of query element
	start = strstr(data, "<query");
	if (start == NULL)
    {
        // check if this is message to be ignored
        start = strstr(data, "<message");
        if (start == NULL)
            return NULL;
        else
        {
            strncpy(type, "message", strlen("message"));
            return type;
        }
    }

	// Get namespace attribute
	start = strstr(start, "xmlns=");
	if (start == NULL)
		return NULL;

	while (*start != '\'' && *start != '"')
		start++;
	start++;
	end = start;
	while (*end != '\'' && *end != '"')
		end++;

	type_len = end - start;
	if (type_len >= sizeof(type))
		return NULL;

	strncpy(type, start, type_len);
	type[type_len] = '\0';

	// Find element inside query
	start = strchr(start, '>');
	if (start == NULL)
		return NULL;
	start++;

	// Find end of query element
	end = strstr(start, "</query>");
	if (end == NULL)
		return NULL;

	chunk->rdata = start;
	chunk->rlen = end - start;

	return type;
}

/* Called by worker thread to execute request */
void execute_request(void *_pkt, void *_user)
{
    if (_pkt != NULL)
    {
        async_packet_t pkt = (async_packet_t)_pkt;

        log_debug(ZONE,"begin processing chunk for %s",jid_full(pkt->chunk->from));

        pkt->exec(pkt);

        if (pkt->complete && pkt->async == 0)
            pkt->complete(pkt);
    }
}

/* Default request completion:  send output chunk to connection */
void request_complete(async_packet_t pkt)
{
    if (pkt->out_chunk)
        conn_chunk_write(pkt->s2s->sm, chunk_prep(pkt->out_chunk, NULL));

    log_debug(ZONE,"done processing chunk for %s",jid_full(pkt->chunk->from));

    free_request(pkt);
}

/* Figure out which service gets packet (for now, just rpc).
   Dispatches packet processing to background thread.
*/
void process_packet(s2s_t s2s, chunk_t chunk)
{
	char *type;

    log_debug(ZONE,"processing a chunk from %s",jid_full(chunk->from));

    /* almost impossible, but just in case, bail */
    if(chunk->to == NULL || chunk->to->server == NULL)
    {
        pool_free(chunk->p);
        return;
    }

    /* always make sure it's alone */
    chunk->next = NULL;

    /* set the expire time of a chunk */
    chunk->timeout = time(NULL) + s2s->timeout_packet;

    /* See if error (probably a bounced message b/c client went away) */
	if (chunk->type == chunk_ERR)
	{
		type = parse_packet(chunk);
		if (type != NULL && !j_strcmp(type, "iic:iq:rpc"))
		{
			process_rpc_error(chunk->id, RpcUnreachable, "");
			pool_free(chunk->p);
			return;
		}
	}

	if (chunk->type != chunk_PACKET)
    {
        log_debug(ZONE, "packet type %d received from %s, packet ignored", chunk->type, jid_full(chunk->from));
        pool_free(chunk->p);
        return;
    }

	/* Figure out what to do with this packet */
	type = parse_packet(chunk);

	if (type != NULL && !j_strcmp(type, "iic:iq:rpc"))
	{
		/* RPC request/result */
		async_packet_t pkt;

		/* See if this is a request or result */
		if (!j_strcmp(chunk->iq_type, "result"))
			pkt = create_request(s2s, chunk, process_rpc_result, request_complete);
		else
			pkt = create_request(s2s, chunk, process_rpc, request_complete);

		if (s2s->tpool)
			g_thread_pool_push(s2s->tpool, pkt, NULL);
		else
			execute_request(pkt, NULL);
	}
    else if (type != NULL && !j_strcmp(type, "message"))
    {
		/* someone sent us a message, swallow quietly */
        pool_free(chunk->p);
        return;
    }
	else if (type != NULL && !j_strcmp(type, "iic:heartbeat"))
	{
		/* heartbeat from SM (don't reply, SM assumes we're here unless it
		   gets a delivery error) */
		log_debug(ZONE, "heartbeat packet received");
		pool_free(chunk->p);
		return;
	}
	else
	{
		route_app(s2s, chunk_prep(chunk, "Unknown request type"));
	}

    packet_counter++; /* keep rough total */
}

/* Non-rpc request */
void process_request(s2s_t s2s, nad_t nad)
{
    int query, status;
    char* namespace_str = NULL;
    int namespace_strlen;

    char* id_str = NULL;
    int   id_len;
  
    query = nad_find_elem(nad, 0, "query", 1);
    if (query >= 0)
    {
        int xmlns = nad_find_attr(nad, query, "xmlns", NULL);
        if (xmlns < 0)
            goto bad_request;
        
        namespace_str = NAD_AVAL(nad, xmlns);
        namespace_strlen = NAD_AVAL_L(nad, xmlns);
        if (namespace_strlen == 0)
            goto bad_request;

        /* Check for service status request */
        if (strncmp("iic:service", namespace_str, namespace_strlen) == 0)
        {
            /* first child of query element is the status element */  
            status = nad_find_first_child(nad, query);
            if (status >= 0 && NAD_ENAME_L(nad,status) > 0)
            {
                char *status_str = calloc(1, NAD_ENAME_L(nad,status) + 1);
                strncat(status_str, NAD_ENAME(nad,status), NAD_ENAME_L(nad,status));
                rpc_status(status_str);
                free(status_str);
            }
        }
        else
        {
            char namespace[256], id[256];
			int iq;
            
            namespace[0] = '\0';
            strncat(namespace, namespace_str, namespace_strlen > sizeof(namespace) ? sizeof(namespace) : namespace_strlen);
            
            id[0] = '\0';
            
            iq = nad_find_attr(nad, 0, "id", NULL);
            if (iq >= 0)
            {
                id_str = NAD_AVAL(nad, iq);
                id_len = NAD_AVAL_L(nad, iq);
                
                strncat(id, id_str, id_len > sizeof(id) ? sizeof(id) : id_len);
            }
            
            rpc_namespace(namespace, id, s2s, nad);
        }
    }

bad_request:
    nad_free(nad);
}


/* Return packet to client.
*/
#ifndef ROUTER
void route_app(s2s_t s2s, chunk_t chunk)
{
    if(chunk == NULL) return;

    /* always make sure it's alone */
    chunk->next = NULL;

    log_debug(ZONE,"queuing chunk to %s",jid_full(chunk->to));

    /* catch the first time */
    if(s2s->appdata == NULL)
        s2s->appdata = chunk;
    else
        s2s->appdatat->next = chunk;

    /* always update tail pointer */
    s2s->appdatat = chunk;

}
#endif
