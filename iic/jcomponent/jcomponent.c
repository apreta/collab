/* --------------------------------------------------------------------------
 *
 * License
 *
 * The contents of this file are subject to the Jabber Open Source License
 * Version 1.0 (the "JOSL").  You may not copy or use this file, in either
 * source code or executable form, except in compliance with the JOSL. You
 * may obtain a copy of the JOSL at http://www.jabber.org/ or at
 * http://www.opensource.org/.
 *
 * Software distributed under the JOSL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the JOSL
 * for the specific language governing rights and limitations under the
 * JOSL.
 *
 * Copyrights
 *
 * Portions created by or assigned to Jabber.com, Inc. are
 * Copyright (c) 1999-2002 Jabber.com, Inc.  All Rights Reserved.  Contact
 * information for Jabber.com, Inc. is available at http://www.jabber.com/.
 *
 * Portions Copyright (c) 1998-1999 Jeremie Miller.
 *
 * Acknowledgements
 *
 * Special thanks to the Jabber Open Source Contributors for their
 * suggestions and support of Jabber.
 *
 * Alternatively, the contents of this file may be used under the terms of the
 * GNU General Public License Version 2 or later (the "GPL"), in which case
 * the provisions of the GPL are applicable instead of those above.  If you
 * wish to allow use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the JOSL,
 * indicate your decision by deleting the provisions above and replace them
 * with the notice and other provisions required by the GPL.  If you do not
 * delete the provisions above, a recipient may use your version of this file
 * under either the JOSL or the GPL.
 *
 *
 * --------------------------------------------------------------------------*/

/* Seek the .h, for it shall guide you. */

#ifdef _WIN32
#include <windows.h>
#endif

#include "jcomponent.h"
#include "jcomponent_ext.h"
#include "presence.h"
#include "register.h"
#include "util/log.h"

config_t config;
char * jcomp_name;
s2s_t s2s;
int connected_callback = 0;

int is_sm_connected = 0;
int sm_retry_time = 0;
#define SM_RETRY_DELAY 15  // seconds

static jc_connected_fn jc_connected = NULL;
jc_presence_fn jc_presence = NULL;
jc_auth_fn jc_auth = NULL;

void jc_set_connected_callback(jc_connected_fn con)
{
	jc_connected = con;
}

/* flag what this signal is for later handling */
static int last_sig = 0;
void onSignal(int sig)
{
    last_sig = sig;
}

/* jcomponent's main drag */
int jc_init(config_t _config, const char * _jcomp_name)
{
	char *log_file;
	char *log_rotation_sz;
	char *log_history_len;
	char *log_ident;
	char *log_init_level;
    char secret[41];

    /* startup stuff */
#ifdef _WIN32
	WORD wVersionRequested;
	WSADATA wsaData;
	int err;

	wVersionRequested = MAKEWORD( 2, 0 );

	err = WSAStartup( wVersionRequested, &wsaData );
	if (err)
	{
		log_error(ZONE, "Error initializing networking (%d)\n", GetLastError());
		exit(1);
	}
#endif

    config = _config;
    jcomp_name = _jcomp_name;

	if (!g_thread_supported())
		g_thread_init(NULL);

#ifdef LINUX
    signal(SIGINT, onSignal);
#endif

    log_file = config_get_one(config, jcomp_name, "log.file", 0);
	if (log_file != NULL)
		log_new(log_file);
	log_rotation_sz = config_get_one(config, jcomp_name, "log.rotationSize", 0);
	if (log_rotation_sz != NULL)
	  log_set_rotation_size(atoi(log_rotation_sz));
	log_history_len = config_get_one(config, jcomp_name, "log.historyLen", 0);
	if (log_history_len != NULL)
	  log_set_history_len(atoi(log_history_len));
    log_ident = config_get_one(config, jcomp_name, "log.ident", 0);
	if (log_ident != NULL)
		log_new(log_ident);

    log_init_level = config_get_one(config, jcomp_name, "log.level", 0);
    if (!j_strcmp(log_init_level, "debug")) { log_set_level(LOG_DEBUG); }
    else if (!j_strcmp(log_init_level, "info")) { log_set_level(LOG_INFO); }
    else if (!j_strcmp(log_init_level, "error")) { log_set_level(LOG_ERR); }

    /* require some things */
    if(config_get_one(config, jcomp_name, "sm.host",0) == NULL ||
       config_get_one(config, jcomp_name, "sm.port",0) == NULL ||
       config_get_one(config, jcomp_name, "sm.id",0) == NULL ||
       config_get_one(config, jcomp_name, "sm.secret",0) == NULL) {
		log_error(ZONE, "Jabber server host or port not found in configuration\n");

        return 1;
    }

    /* create our global stuff */
    s2s = (s2s_t)malloc(sizeof(struct s2s_st));
    memset(s2s, 0, sizeof(struct s2s_st));
    s2s->mio = mio_new(j_atoi(config_get_one(config, jcomp_name, "local.sockets",0),1023));
    s2s->timeout_ipcache = 3600;
    s2s->timeout_packet = j_atoi(config_get_one(config, jcomp_name, "local.timeouts.packet",0), 15);
    s2s->timeout_idle = j_atoi(config_get_one(config, jcomp_name, "local.timeouts.idle",0), 600);
    s2s->connection_rates = xhash_new(199);
    s2s->connection_rate_times = j_atoi(config_get_one(config, jcomp_name, "connection_limits.connects",0), 5);
    s2s->connection_rate_seconds = j_atoi(config_get_one(config, jcomp_name, "connection_limits.seconds",0), 10);

    // Create thread pool.  If multi-threading is disabled, tpool will be NULL.
	if (!g_thread_supported())
		g_thread_init(NULL);
	s2s->num_threads = j_atoi(config_get_one(config, jcomp_name, "init.threads",0), 5);
	if (s2s->num_threads > 0)
		s2s->tpool = g_thread_pool_new(execute_request, NULL, s2s->num_threads, TRUE, NULL);
	s2s->dispatch_thread = j_atoi(config_get_one(config, jcomp_name, "init.dispatch_thread",0), 0);

    /* get or create a secret for validating dialbacks */
    if((s2s->dbsecret = config_get_one(config, jcomp_name, "local.secret",0)) == NULL)
    {
        shahash_r(NULL,secret);
        secret[40] = '\0';
        s2s->dbsecret = secret;
    }

    /* set up hash tables */
    s2s->in = xhash_new(j_atoi(config_get_one(config, jcomp_name, "local.prime",0),4001));
    s2s->out = xhash_new(j_atoi(config_get_one(config, jcomp_name, "local.prime",0),4001));
    s2s->ipcache = xhash_new(j_atoi(config_get_one(config, jcomp_name, "local.prime",0),4001));

    /* start logging */
    log_status(ZONE, "starting up");

    /* initialize our services */
    {
        char * host = config_get_one(config, jcomp_name, "init.jid", 0);
		char * config_file = config_get_one(config, jcomp_name, "init.config", 0);
		char * secret = config_get_one(config, jcomp_name, "init.secret", 0);
		char * jsm = config_get_one(config, jcomp_name, "init.jsm", 0);
		char * service = config_get_one(config, jcomp_name, "init.service", 0);
		char * heartbeat = config_get_one(config, jcomp_name, "init.heartbeat", 0);
        rpc_init(s2s, host, jsm, service, heartbeat ? atoi(heartbeat) : 0, config_file);
		presence_init(s2s, host, secret);
        register_init(s2s, host);
    }

	return 0;
}

static int jc_run_internal(void *data)
{
    time_t last;

	/* connect to our sm */
    s2s->secret = config_get_one(config, jcomp_name, "sm.secret",0);
    s2s->sm = connect_new(s2s, config_get_one(config, jcomp_name, "sm.host", 0), atoi(config_get_one(config, jcomp_name, "sm.port", 0)), config_get_one(config, jcomp_name, "sm.id", 0));

    sm_connected(0);

    /* make sure we can listen */
#ifdef ROUTER
    if(mio_listen(s2s->mio, j_atoi(config_get_one(config, jcomp_name, "local.port",0),5269), NULL, in_io, (void*)s2s) < 0)
    {
        log_write(LOG_ERR, "failed to listen on %d!", j_atoi(config_get_one(config, jcomp_name, "local.port",0),5269));
        return 1;
    }
#endif

    /* just a matter of processing socket events now */
    time(&last);
    while(last_sig == 0)
    {
		int packet_status = 0;
		int rpc_status = 0;

		if (s2s->sm == NULL || s2s->sm->state != state_OPEN)
			connected_callback = 0;

        packet_status = process_packets(s2s);
		if (packet_status < 0)
            break;
		if (s2s->sm != NULL && s2s->sm->state == state_OPEN && !connected_callback)
		{
			rpc_connected(s2s);
            register_connected(s2s);
			if (jc_connected)
				jc_connected();
			connected_callback = 1;
		}

		rpc_status = rpc_run();
		if (rpc_status < 0)
            break;

		if (!packet_status && !rpc_status)
		{
			// No work found, sleep 20 ms
			// *** May want to poll network less often
#ifdef LINUX
			struct timespec req;
			req.tv_sec = 0;
			req.tv_nsec = 20 % 1000 * 1000000;
			nanosleep(&req, NULL);
#else // _WIN32
			// Sleep, allows timer proc to be called
			SleepEx(20, TRUE);
#endif
            // if connection open but not yet connected to sm, might need to rety
            if (s2s->sm != NULL && is_sm_connected == 0)
            {
                int now = time(NULL);
                log_debug(ZONE,"handshake retry time: %d now %d delta %d", sm_retry_time, now, now-sm_retry_time);
                if (now - sm_retry_time > SM_RETRY_DELAY)
                {
                    log_debug(ZONE,"handshake not received, disconnecting");
                    connect_close(s2s);
                }
            }
		}
    }

    log_status(ZONE, "shutting down");

    /* !!! nicely close conns */

    /* exiting, clean up */
	rpc_shutdown(s2s);
    register_shutdown(s2s);
	presence_shutdown(s2s);
    if (s2s->tpool)
		g_thread_pool_free(s2s->tpool, FALSE, TRUE);
    mio_free(s2s->mio);
    xhash_free(s2s->connection_rates);
    xhash_free(s2s->in);
    xhash_free(s2s->out);
    xhash_free(s2s->ipcache);
    free(s2s);
    xhash_free(config);

    return 0;
}

int jc_run(int non_blocking)
{
	/* If enabled, start main loop in separate dispatch thread */
	if (non_blocking)
	{
		g_thread_create((GThreadFunc)jc_run_internal, NULL, FALSE, NULL);

		return 0;
	}
	else
		return jc_run_internal(NULL);
}

void sm_connected(int connected)
{
    is_sm_connected = connected;
    if (connected == 0)
        sm_retry_time = time(NULL);
}

/* Process incoming and outgoing packets */
/* Returns -1 on error, 0 if no packets (or connetion not open), 1 if processed packets */
int process_packets(s2s_t s2s)
{
	int timeout = 0; //s2s->num_threads ? s2s->timeout_packet/2 : 0;
    chunk_t cur;
	int found = 0;
	static int retry = 0;
	char * host = config_get_one(config, jcomp_name, "sm.host",0);
	char * backup = config_get_one(config, jcomp_name, "sm.backup",0);

    found = mio_run(s2s->mio, timeout);

	host = (retry++ & 1) && (backup != NULL) ? backup : host;

    /* make sure sm conn didn't fail */
    if(s2s->sm == NULL)
    {
        log_debug(ZONE, "No sm: reconnecting");
        if ((s2s->sm = connect_new(s2s, host, atoi(config_get_one(config, jcomp_name, "sm.port",0)), config_get_one(config, jcomp_name, "sm.id",0))) == NULL)
        {
            log_error(ZONE, "failed to connect to sm!");
            return -1;
        }
        sm_connected(0);
    }

	if (s2s->sm->state == state_OPEN)
		retry = 0;

    /* shuffle packets to be written to the sm */
    while(s2s->sm->state == state_OPEN && (cur = s2s->appdata) != NULL)
    {
        s2s->appdata = cur->next;
        conn_chunk_write(s2s->sm,cur);
		found = 1;
    }

    /* every so often check for timed out pending conns */
#ifdef ROUTER
    if((time(NULL) - last) > s2s->timeout_packet)
    {
        route_expire(s2s);
        time(&last);
    }
#endif

    connection_rate_cleanup(s2s);

    return found;
}

int ignore_term_signals(void)
{
#ifdef LINUX

    /* !!! the list of signals needs to be reviewed */
    int sig[] = {SIGHUP, SIGQUIT, SIGTSTP, SIGTTIN, SIGTTOU};

    int num_sigs = (sizeof sig) / (sizeof sig[0]);
    int i;
    struct sigaction dae_action;

    dae_action.sa_handler = SIG_IGN;
    sigemptyset(&(dae_action.sa_mask));
    dae_action.sa_flags = 0;

    for (i = 0; i < num_sigs; ++i)
    	if (sigaction(sig[i], &dae_action, NULL) == -1) return -1;

#endif

    return 0;
}

int daemonize(void)
{
#ifdef LINUX
    pid_t pid;
    int i;

    log_error(ZONE, "Preparing to run in daemon mode ...\n");

    if ((pid = fork()) != 0)
       exit(0);

    /* become session leader */
    setsid();

    /* ignore the signals we want */
    ignore_term_signals();

    if ((pid = fork()) != 0)
       exit(0);

    /* !!! could change to our working directory */
    chdir("/");

    umask(0);

    /* close open descriptors */
    for (i=0; i < MAXFD; ++i)
       close(i);
#endif

    return 0;
}

/* convenience */
char *merlin(pool p, char *secret, char *to, char *challenge)
{
    static char res[41];

    shahash_r(secret,                       res);
    shahash_r(spools(p, res, to, p),        res);
    shahash_r(spools(p, res, challenge, p), res);

    log_debug(ZONE,"merlin casts his spell(%s+%s+%s) %s",secret,to,challenge,res);
    return res;
}
