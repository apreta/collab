# Microsoft Developer Studio Project File - Name="jcomponent" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=jcomponent - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "jcomponent.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "jcomponent.mak" CFG="jcomponent - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "jcomponent - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "jcomponent - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "jcomponent - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "../../xmlrpc/src" /D "NDEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "MIO_SELECT" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "jcomponent - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "jcomponent___Win32_Debug"
# PROP BASE Intermediate_Dir "jcomponent___Win32_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "../../xmlrpc/src" /D "_DEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "MIO_SELECT" /D "DEBUG" /FR /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "jcomponent - Win32 Release"
# Name "jcomponent - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\util\config.c
# End Source File
# Begin Source File

SOURCE=.\conn.c
# End Source File
# Begin Source File

SOURCE=.\connect.c
# End Source File
# Begin Source File

SOURCE=.\util\eventlog.cpp
# End Source File
# Begin Source File

SOURCE=.\xmlparse\hashtable.c
# End Source File
# Begin Source File

SOURCE=.\jcomponent.c
# End Source File
# Begin Source File

SOURCE=.\util\jid.c
# End Source File
# Begin Source File

SOURCE=.\util\log.c
# End Source File
# Begin Source File

SOURCE=.\mio\mio.c
# End Source File
# Begin Source File

SOURCE=.\util\nad.c
# End Source File
# Begin Source File

SOURCE=.\util\nadutils.cpp
# End Source File
# Begin Source File

SOURCE=.\util\pool.c
# End Source File
# Begin Source File

SOURCE=.\presence.c
# End Source File
# Begin Source File

SOURCE=.\process.c
# End Source File
# Begin Source File

SOURCE=.\rate.c
# End Source File
# Begin Source File

SOURCE=.\register.c
# End Source File
# Begin Source File

SOURCE=.\rpc.c
# End Source File
# Begin Source File

SOURCE=.\util\rpcresult.cpp
# End Source File
# Begin Source File

SOURCE=.\util\sha.c
# End Source File
# Begin Source File

SOURCE=.\util\snprintf.c
# End Source File
# Begin Source File

SOURCE=.\util\str.c
# End Source File
# Begin Source File

SOURCE=.\util\xhash.c
# End Source File
# Begin Source File

SOURCE=.\xmlparse\xmlparse.c
# End Source File
# Begin Source File

SOURCE=.\xmlparse\xmlrole.c
# End Source File
# Begin Source File

SOURCE=.\xmlparse\xmltok.c
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\util\eventlog.h
# End Source File
# Begin Source File

SOURCE=.\jcomponent.h
# End Source File
# Begin Source File

SOURCE=.\jcomponent_ext.h
# End Source File
# Begin Source File

SOURCE=.\util\log.h
# End Source File
# Begin Source File

SOURCE=.\presence.h
# End Source File
# Begin Source File

SOURCE=.\register.h
# End Source File
# Begin Source File

SOURCE=.\rpc.h
# End Source File
# Begin Source File

SOURCE=.\util\util.h
# End Source File
# End Group
# End Target
# End Project
