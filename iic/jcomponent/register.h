/* (c) 2004 imidio all rights reserved */

#ifndef REGISTER_H
#define REGISTER_H

#include <gmodule.h>
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct register_st * regist_t;

#define MAX_USER_LENGTH 64
#define MAX_KEY_LENGTH 256

// Register status codes.
enum RegisterStatus {
    RegisterOK    =  0,
    RegisterDone  = -1,
	RegisterError = -2
};

enum ServiceType
{
    ServiceAIM,
    ServiceMSN,
    ServiceYHO,
};

// announce register result
typedef void (*register_result_fn)(const char * id, regist_t * r);

// set callback for register result
void register_set_callback(register_result_fn);

// Start/end registration
regist_t register_component(const char* id, const char* service_jid, int service, const char* user, const char* password, const char* jab_user, const char* jab_host);
void end_registered_session(regist_t r);
int  is_connected(regist_t r);
void send_instant_message(regist_t r, const char* username, const char* body);

// Send a message through gateway
int send_message(regist_t r, const char* message);

#ifdef __cplusplus
}
#endif

#endif
