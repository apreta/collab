/* (c)2003 imidio */

#include "jcomponent.h"
#include "jcomponent_ext.h"
#include "presence.h" 
#include "util/log.h"

extern jc_presence_fn jc_presence;
extern jc_auth_fn jc_auth;

static s2s_t s2s = NULL;
static const char * host_jid = NULL;
static const char * secret = NULL;


enum SessionState 
{
	SessionNone,
	SessionCreate,
	SessionOpen,
	SessionClose,
};


struct session_st
{
	pool p;
	jid user_jid;
	jid sm_jid;
	jid my_jid;
	int available;
	int state;
	char status[MAX_STATUS_LENGTH];
	void * user;
    int auth;
};


xht sessions;

//====
// Presence subsystem startup/shutdown
//====

int presence_init(s2s_t _s2s, const char * _host_jid, const char * _secret)
{
	s2s = _s2s;
	host_jid = strdup(_host_jid);
	if (_secret)
		secret = strdup(_secret);

	sessions = xhash_new(MAX_SESSIONS);
	return 0;
}

void jc_set_auth_callback(jc_auth_fn auth)
{
	jc_auth = auth;
}

void jc_set_presence_callback(jc_presence_fn pres)
{
	jc_presence = pres;
}

void shutdown_walk(xht h, const char *key, void *val, void *arg)
{
	end_session(val);
}


int presence_shutdown(s2s_t s2s)
{
	xhash_walk(sessions, shutdown_walk, NULL);
	xhash_free(sessions);
	return 0;
}

//====
// Presence APIs
//====

/* Create new session for user. */
session_t create_session(const char * user_jid, const char * resource, void * user)
{
    chunk_t chunk;
	char id[32];

	pool p = pool_new();
	session_t s = (session_t)pmalloco(p, sizeof(struct session_st));
	s->state = SessionCreate;
	s->available = PresenceAvailable;
	s->p = p;
	s->sm_jid = NULL;
	s->user = user;
	s->user_jid = jid_new(p, (char*)user_jid);
	s->status[0] = '\0';
	jid_set(s->user_jid, (char*)resource, JID_RESOURCE);

	// bad_ptr
	sprintf(id, "%016lX", (long)s);
	s->my_jid = jid_new(p, (char*)host_jid);
	jid_set(s->my_jid, id, JID_USER);
	
	xhash_put(sessions, s->my_jid->user, s); 
	
	// Create auth packet to send to remote sm.
	chunk = chunk_new(chunk_PACKET, NULL, jid_full(s->user_jid));
	spooler(chunk->regen,
				"<route type='auth' to='", jid_full(s->user_jid), "'",
				" from='", jid_full(s->my_jid), "'>",
				"<iq type='set'>",
				"<query xmlns='jabber:iq:auth'><username>", user_jid, "</username>",
				"<key>", secret, "</key></query></iq>",
				"</route>",
				chunk->regen);

    chunk->data = chunk->wcur = spool_print(chunk->regen);
    chunk->wlen = strlen(chunk->data);

    log_debug(ZONE,"auth chunk prepared: %s", chunk->data);

    conn_chunk_write(s2s->sm, chunk);
	
	return s;
}

/* Create new session for user. */
session_t create_auth_session(void * user)
{
	char id[32];
    pool p;
    
    if (s2s->sm == NULL)
        return NULL;
    
    p = pool_new();
	session_t s = (session_t)pmalloco(p, sizeof(struct session_st));
	s->state = SessionCreate;
	s->available = PresenceAvailable;
	s->p = p;
	s->sm_jid = NULL;
	s->user = user;
	s->status[0] = '\0';
    s->auth = 1;
	
	// bad_ptr
	sprintf(id, "%016lX", (long)s);
	s->my_jid = jid_new(p, (char*)host_jid);
	jid_set(s->my_jid, id, JID_USER);
	
	xhash_put(sessions, s->my_jid->user, s); 
	
	return s;
}

int send_auth_session_request(session_t s, const char *user_jid, const char * resource, const char * password)
{
    chunk_t chunk;

	s->user_jid = jid_new(s->p, (char*)user_jid);
	jid_set(s->user_jid, (char*)resource, JID_RESOURCE);

	// Create auth packet to send to remote sm.
	chunk = chunk_new(chunk_PACKET, NULL, jid_full(s->user_jid));
	spooler(chunk->regen,
				"<route type='auth' to='", jid_full(s->user_jid), "'",
				" from='", jid_full(s->my_jid), "'>",
				"<iq type='set'>",
				"<query xmlns='jabber:iq:auth'><username>", user_jid, "</username>",
				"<password>", password, "</password></query></iq>",
				"</route>",
				chunk->regen);

    chunk->data = chunk->wcur = spool_print(chunk->regen);
    chunk->wlen = strlen(chunk->data);

    log_debug(ZONE,"auth chunk prepared: %s", chunk->data);

    conn_chunk_write(s2s->sm, chunk);

    return 0;
} 

session_t find_session(const char * key)
{
	session_t s = (session_t)xhash_get(sessions, key);
	return s;
}

void end_session(session_t s)
{
    chunk_t chunk;

	// ?? Send unavailable status
	if (s->state == SessionOpen)
	{
		chunk = chunk_new(chunk_PACKET, NULL, NULL);
		spooler(chunk->regen,
					"<route type='error' to='", jid_full(s->sm_jid), "'",
					" from='", jid_full(s->my_jid), "'>",
					"</route>",
					chunk->regen);
	
		chunk->data = chunk->wcur = spool_print(chunk->regen);
		chunk->wlen = strlen(chunk->data);
	
		log_debug(ZONE,"end session chunk prepared: %s", chunk->data);
	
		conn_chunk_write(s2s->sm, chunk);
		
		xhash_zap(sessions, s->my_jid->user);
		pool_free(s->p);
	}
	else
		s->state = SessionClose;
}

void send_presence_message(session_t s)
{
	chunk_t chunk;
	
	chunk = chunk_new(chunk_PACKET, NULL, NULL);
	spooler(chunk->regen,
				"<route to='", jid_full(s->sm_jid), "'",
				" from='", jid_full(s->my_jid), "'>",
				"<presence from='", jid_full(s->user_jid),"'><status>", s->status, "</status></presence>",
				"</route>",
				chunk->regen);

	chunk->data = chunk->wcur = spool_print(chunk->regen);
	chunk->wlen = strlen(chunk->data);

	log_debug(ZONE,"end session chunk prepared: %s", chunk->data);

	conn_chunk_write(s2s->sm, chunk);
}

void send_route_message(session_t s, const char* msg)
{
    chunk_t chunk;

	chunk = chunk_new(chunk_PACKET, NULL, NULL);
	spooler(chunk->regen,
				"<route to='", jid_full(s->sm_jid), "'",
				" from='", jid_full(s->my_jid), "'>",
                msg,
				"</route>",
				chunk->regen);
	chunk->data = chunk->wcur = spool_print(chunk->regen);
	chunk->wlen = strlen(chunk->data);

	log_debug(ZONE,"end session chunk prepared: %s", chunk->data);

	conn_chunk_write(s2s->sm, chunk);
}

void send_presence(session_t s, const char * status)
{
	// Save latest status.
	s->status[0] = '\0';
	if (status)
		strncat(s->status, status, MAX_STATUS_LENGTH);

	if (s->state == SessionOpen)
	{
		send_presence_message(s);
	}
}

void send_session_message(s2s_t s2s, session_t s)
{
    chunk_t chunk;

	if (s->state == SessionCreate)
	{
		chunk = chunk_new(chunk_PACKET, NULL, NULL);
		spooler(chunk->regen,
					"<route type='session' to='", jid_full(s->user_jid),"'",
					" from='", jid_full(s->my_jid), "'>",
					"</route>",
					chunk->regen);

		chunk->data = chunk->wcur = spool_print(chunk->regen);
		chunk->wlen = strlen(chunk->data);

		log_debug(ZONE,"chunk prepared: %s",chunk->data);

		conn_chunk_write(s2s->sm, chunk);
	}
}

/* We receive route to initiate session, then receive route with presence packets.
   We only need info from first route packet and can ignore the rest.
*/
void process_route(s2s_t s2s, nad_t nad)
{
	char id[32];
    int is_auth = 0;
	session_t s = NULL;
	int elem = 0, attr = 0;
    char * buf = NULL;
    int len;

#ifdef DEBUG
	nad_print(nad, 0, &buf, &len);
	log_debug(ZONE, "route packet received: %.*s", len, buf);
#endif

    // get type of message to see if this is 'auth' route
    attr = nad_find_attr(nad, elem, "type", NULL);
    if (attr >= 0)
    {
        if (j_strncmp(NAD_AVAL(nad, attr), "auth", NAD_AVAL_L(nad, attr)) == 0)
            is_auth = 1;
    }

	// User name part of to address is our session id
	attr = nad_find_attr(nad, elem, "to", NULL);
	if (attr < 0 || NAD_AVAL_L(nad, attr) < 16)
	{
		log_debug(ZONE, "routed packet has no to address, ignoring");
		goto error;
	}

	// to id will contain session id
	//sscanf(NAD_AVAL(nad, attr), "%8s", id);
	// bad_ptr
	strncpy(id, NAD_AVAL(nad, attr), 16);
	id[16] = '\0';
	s = find_session(id);
	if (s == NULL)
	{
		log_debug(ZONE, "route packet received for invalid session %s, ignored", id);
		goto error;
	}

	if (nad->ecur == 1 && s->sm_jid == NULL)
	{
		// Empty route packet...get destination address of sm
		attr = nad_find_attr(nad, 0, "from", NULL);
		if (attr < 0)
		{
			log_debug(ZONE, "routed packet has no from address, ignoring");
			goto error;
		}
		
		s->sm_jid = jid_newx(s->p, NAD_AVAL(nad, attr), NAD_AVAL_L(nad, attr));
		if (s->state == SessionClose)
		{
			s->state = SessionOpen;
			end_session(s);
		}
		else
		{
			s->state = SessionOpen;
            send_presence_message(s);
            process_register_session_open(jid_full(s->user_jid));
			log_debug(ZONE, "session established for user %s", jid_full(s->user_jid));
		}
	}
	else if ((elem = nad_find_elem(nad, 0, "iq", 1)) >= 0)
	{
        char iqtype[32];
        int len;

		// Figure out if authentication suceeded.
		attr = nad_find_attr(nad, elem, "type", NULL);
		if (attr < 0)
			goto error;

        len = NAD_AVAL_L(nad, attr);
        strncpy(iqtype, NAD_AVAL(nad, attr), len);
        iqtype[len] = '\0';

        // is this a register query?
        if (!is_auth && (elem = nad_find_elem(nad, 0, "query", 2)) >= 0)
        {
            process_register_query(s2s, nad);
        }
        else // not a query - either error, result or session
        {
            if (j_strncmp(NAD_AVAL(nad, attr), "error", NAD_AVAL_L(nad, attr)) == 0)
            {
                log_debug(ZONE, "unable to create session for %s", jid_full(s->user_jid));
                if (s->auth)
                {
                    if (jc_auth)
                        jc_auth(jid_full(s->user_jid), 0, s->user);
                }
                goto error;
            }
            else if (j_strncmp(NAD_AVAL(nad, attr), "result", NAD_AVAL_L(nad, attr)) == 0)
            {
                if (is_auth)
                {
                    // this is a successful authentication, tell sm to create session
                    log_debug(ZONE, "session for %s authenticated", jid_full(s->user_jid));
                    
                    if (s->auth)
                    {
                        // created for purposes of validating password, no session needed
                        if (jc_auth)
                            jc_auth(jid_full(s->user_jid), 1, s->user);
                    }
                    else
                    {
                        send_session_message(s2s, s);
                    }
                }
                else
                {
                    process_register_connected(jid_full(s->user_jid));
                }
            }
            
        }
	}
    else if ((elem = nad_find_elem(nad, 0, "presence", 1)) >= 0)
    {
        elem = nad_find_elem(nad, elem, "status", 1);
        if (elem >= 0)
        {
            if (j_strncmp(NAD_CDATA(nad, elem), "Connected", NAD_CDATA_L(nad, elem)) == 0)
                process_register_connected(jid_full(s->user_jid));
            else if (j_strncmp(NAD_CDATA(nad, elem), "Disconnected", NAD_CDATA_L(nad, elem)) == 0)
                process_register_disconnected(jid_full(s->user_jid));
        }
    }

error:
	nad_free(nad);
}

/* Looking for unavailable presence on a resource we know about.
*/
void process_presence(s2s_t s2s, nad_t nad)
{
	int elem = 0, attr = 0;
    char * buf = NULL;
	int len;
	char from_jid[512];
	char status[128] = {'\0'};
	int available;

#ifdef DEBUG
	nad_print(nad, 0, &buf, &len);
	log_debug(ZONE, "presence packet received: %.*s", len, buf);
#endif

	attr = nad_find_attr(nad, elem, "type", NULL);
	if (attr >= 0 && j_strncmp(NAD_AVAL(nad, attr), "unavailable", NAD_AVAL_L(nad, attr)) == 0)
		available = 0;
	else
		available = 1;
	
	attr = nad_find_attr(nad, elem, "from", NULL);
	if (attr < 0)
		goto error;
	
	len = NAD_AVAL_L(nad, attr);
	if (len > sizeof(from_jid) - 1)
		len = sizeof(from_jid) - 1;
	strncpy(from_jid, NAD_AVAL(nad, attr), len);
	from_jid[len] = '\0';
	
	elem = nad_find_elem(nad, elem, "status", 1);
	if (elem >= 0)
	{
		len = NAD_ENAME_L(nad, elem);
		if (len > sizeof(status) - 1)
			len = sizeof(status) - 1;
		strncat(status, NAD_ENAME(nad, elem), len);
	}

	if (jc_presence)
		jc_presence(from_jid, available, status);

	log_debug(ZONE, "presence for %s read", from_jid);
	
error:
	nad_free(nad);
}

