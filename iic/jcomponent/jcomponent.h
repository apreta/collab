#ifndef JCOMPONENT_H
#define JCOMPONENT_H

#ifdef OLD
#include <pthread.h>
#else
#include <glib.h>
#endif
#include "mio/mio.h"
#include "xmlparse/xmlparse.h"
#include "util/util.h"

/*

jcomponent -- external jabber component support

It is very similar to jads2s, all of the system I/O event handling is in mio/,
all XML parsing (expat) in xml/, and the rest of the common utilities (from 1.4) are in util/.

Just like jads2s, we have two basic data types, a conn(ection) and a chunk.
The connection wraps the data we need for every client.  The chunk wraps the xml as
it's being parsed from or written to a connection.  The common conn/chunk utilities
are in conn.c.

*/

#define CONNECT_RETRY_PAUSE	20		// seconds


/* forward decls */
typedef struct conn_st *conn_t;
typedef struct s2s_st *s2s_t;
typedef struct async_packet_st *async_packet_t;

typedef void (*cleanup_fn)(void *);


/* chunk definition */
typedef enum { chunk_PACKET, chunk_DBVERIFY, chunk_DBRESULT, chunk_ERR, chunk_HEAD } chunk_type_t;
typedef struct chunk_st
{
    pool p;

    /* data summary */
    chunk_type_t type;
    time_t timeout;

    /* raw data vars when writing */
    void *data, *wcur;
    int wlen;

    /* vars used for creating the chunk on the fly */
    spool regen, cdstr;
    int eflag;
    jid to, from;
    char *dbid, *dbkey; /* dialback uses these, dbkey is "to/from" jid host combination */
    
    char *id;
    char *iq_type;

    char *rdata;
    int rlen;

    /* for linking chunks together */
    struct chunk_st *next;
} *chunk_t;


typedef void (*async_packet_fn)(async_packet_t);


/* Pointer to packet destined for background processing */
struct async_packet_st
{
    chunk_t chunk;
    s2s_t s2s;
    chunk_t out_chunk;
    async_packet_fn exec;
    async_packet_fn complete;
    int async;
};


/* connection data */
typedef enum { state_NONE, state_OPEN, state_OUT, state_IN } jconn_state_t;
struct conn_st
{
    s2s_t s2s;

    /* vars for this conn */
    pool p;
    int fd, count;
    jconn_state_t state;
    time_t start, activity;
    char *ip;

    /* dialback state */
    xht db;

    /* chunks being written */
    /* If multithreading, access to queue must be protected using mutex */
    chunk_t writeq, qtail;
#ifdef OLD
    pthread_mutex_t queue_mutex;
#else
    GStaticRecMutex queue_mutex;
#endif

    /* parser stuff */
    XML_Parser expat;
    int depth;
    chunk_t chunk;

    nad_cache_t nad_cache;
	nad_t nad;

    /* stream stuff */
    char *dbns;
    char *id;
};

/* conn happy/sad */
conn_t conn_new(s2s_t s2s, int fd);
void conn_free(conn_t c);
void conn_flush(conn_t c);

/* write a chunk to a conn, optinally make it an error */
void conn_chunk_write(conn_t c, chunk_t chunk);

/* read/write wrappers for a conn */
int conn_read(conn_t c, char *buf, int len);
int conn_write(conn_t c);

/* maximum number of xml children in a chunk (checked by conn_read) */
#define MAXDEPTH 10000
#define MAXDEPTH_ERR "maximum node depth reached"

/* maximum byte size of any given chunk (checked by conn_read) */
#define MAXSIZE 500000
#define MAXSIZE_ERR "maximum node size reached"

/* maximum number of fd for daemonize */
#define MAXFD 255

/** IP Connection Rate Limit Functions **/
int connection_rate_check(s2s_t s2s, const char* ip);
void connection_rate_cleanup(s2s_t s2s);

/* s2s master data type */
struct s2s_st
{
    /* globals */
    mio_t mio;

    /* setup vars */
    int timeout_packet, timeout_idle, timeout_ipcache;
    char *dbsecret; /* our dialback secret, server farms */

	int dispatch_thread;	   /* Start main loop on separate thread */
	int num_threads;
	GThreadPool * tpool;
    chunk_t appdata, appdatat; /* chunks to be processed by app */

    /* incoming connection rates */
    xht connection_rates; /* our current rate limit checks */
    int connection_rate_times;
    int connection_rate_seconds;

    /* hashes for connection checking */
    xht out; /* all outgoing connections, key is ip:port */
    xht in; /* all the incoming connections, stream id attrib is key */
    xht ipcache; /* host->ip lookup cache */

    /* dnsrv stuff */
    conn_t dnsrv;
    int cpid;

    /* app stuff */
    conn_t sm;
    char *secret;

};

/* the handler for incoming/outgoing connection mio events */
int in_io(mio_t m, mio_action_t a, int fd, void *data, void *arg);
int out_io(mio_t m, mio_action_t a, int fd, void *data, void *arg);

/* create outgoing connection to the given ip:port */
conn_t out_conn(s2s_t s2s, char *ip);

/* create a sm connection */
conn_t connect_new(s2s_t s2s, char *host, int port, char *id);
void   connect_close(s2s_t s2s);
void   sm_connected(int connected);

/* dialback key generator */
char* merlin(pool p, char *secret, char *to, char *challenge);

/* new blank chunk, initalize to/from and dbkey if avail */
chunk_t chunk_new(chunk_type_t type, char *to, char *from);

/* prep a chunk to be written */
chunk_t chunk_prep(chunk_t chunk, char *err);

/* process packets for session */
int process_packets(s2s_t s2s);
void execute_request(void *, void *);

/* process rpc messages */
void process_packet(s2s_t s2s, chunk_t chunk);
void process_rpc(async_packet_t pkt);
void process_rpc_result(async_packet_t pkt);
void process_rpc_error(const char * id, int code, const char * msg);
void process_request(s2s_t s2s, nad_t nad);

/* process sm messages */
void process_route(s2s_t s2s, nad_t nad);
void process_presence(s2s_t s2s, nad_t nad);

/* process register messages */
void process_register_query(s2s_t s2s, nad_t nad);
void process_register_connected(const char* jid);
void process_register_disconnected(const char* jid);
void process_register_session_open(const char *jid);

/* get a chunk to where it should be (existing or lookup and connect new) */
void route(s2s_t s2s, chunk_t chunk);

/* expire any waiting chunk(s) */
void route_expire(s2s_t s2s);

/* send packets back to the app */
void route_app(s2s_t s2s, chunk_t chunk);

int rpc_init(s2s_t s2s, const char *host, const char * jsm, const char * service, int heartbeat, const char * config_file);
int rpc_connected(s2s_t s2s);
int rpc_shutdown(s2s_t s2s);
int rpc_run();
void rpc_status(const char *status);

int presence_init(s2s_t s2s, const char * host_jid, const char * secret);
int presence_shutdown(s2s_t s2s);

int register_init(s2s_t s2s, const char * host_jid);
int register_connected(s2s_t s2s);
int register_shutdown(s2s_t s2s);

// Abstractions for some network functions

#ifdef LINUX
#define _write_actual(c,f,b,l) write(f,b,l)
#define _read_actual(f,b,l) read(f,b,l)
#define _error errno
#define _close close

#endif

#ifdef _WIN32
#define _write_actual(c,f,b,l) send(f,b,l,0)
#define _read_actual(f,b,l) recv(f,b,l,0)
#define _error WSAGetLastError()
#define _close closesocket

#endif
#endif
