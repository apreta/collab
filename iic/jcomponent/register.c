/* (c) 2003 imidio all rights reserved */

#include <stdio.h>
#ifdef LINUX
#include <dlfcn.h>
#endif

#include <glib.h>
#ifdef OLD
#include <gmodule.h>
#endif

#include "jcomponent.h"
#include "register.h"
#include "presence.h"
#include "util/log.h"

static s2s_t register_s2s;
static const char * host_jid;

static register_result_fn register_result = NULL;

enum RegisterState 
{
	RegisterRequest,
	RegisterSubmitted,
	RegisterActive,
    RegisterClosed,
};

struct register_st
{
	pool p;
    session_t session;  // jabber session
    int  service;       // type of service
    int  state;
    jid  service_jid;
    jid  my_jid;
    char id[32];
    char user[MAX_USER_LENGTH];
    char pwd[MAX_USER_LENGTH];
    char key[MAX_KEY_LENGTH];
    int  index;
};

// hash table of register sessions
#define MAX_REGISTER_SESSIONS 67
xht register_sessions;

/* Initialize register subsystem. */
int register_init(s2s_t s2s, const char *host)
{
    register_s2s = s2s;

    //??? needed?
    host_jid = strdup(host);

    register_sessions = xhash_new(MAX_SESSIONS);
	return 0;
}

/* Set up our callbacks */
void register_set_callback(register_result_fn res)
{
	register_result = res;
}

/* called when session manager is avail */
int register_connected(s2s_t s2s)
{
    register_s2s = s2s;

    return 0;
}

/* clean up clean up everybody clean up */
void end_registered_session(regist_t r)
{
	log_status(ZONE, "end_register_session");
    
	if (r->state == RegisterActive)
	{
        char msg[512];
        sprintf(msg, 
                "<iq type='set' to='%s' from='%s'><query xmlns='jabber:iq:register'><remove/></query></iq>",
                jid_full(r->service_jid), jid_full(r->my_jid));
        send_route_message(r->session, msg);
	}

    if (r->session != NULL)
        end_session(r->session);
    return;
}

void register_shutdown_walk(xht h, const char *key, void *val, void *arg)
{
	end_registered_session(val);
}

int register_shutdown(s2s_t s2s)
{
	xhash_walk(register_sessions, register_shutdown_walk, NULL);
	xhash_free(register_sessions);

	return 0;
}

/* Send register query */
void send_register_query(regist_t r)
{
	if (r->state == RegisterRequest)
	{
        char msg[512];
        sprintf(msg, 
                "<iq type='get' to='%s' from='%s'><query xmlns='jabber:iq:register'></query></iq>",
                jid_full(r->service_jid), jid_full(r->my_jid));
        send_route_message(r->session, msg);
	}
}

/* Send login values */
void send_register_result(regist_t r, int is_disconnect)
{
	if (r->state == RegisterRequest)
	{
        char msg[512];

        if (is_disconnect == 1)
        {
            // have to send <remove/> tag to get AIM to disconnect
            // will get <presence>Disconnected</presence> in reply
            sprintf(msg,
                    "<iq type='set' to='%s' from='%s'><query xmlns='jabber:iq:register'><remove/><username>%s</username><password>%s</password><key>%s</key></query></iq>",
                    jid_full(r->service_jid), jid_full(r->my_jid), r->user, r->pwd, r->key);
        }
        else
        {
            sprintf(msg,
                    "<iq type='set' to='%s' from='%s'><query xmlns='jabber:iq:register'><username>%s</username><password>%s</password><key>%s</key></query></iq>",
                    jid_full(r->service_jid), jid_full(r->my_jid), r->user, r->pwd, r->key);
            r->state = RegisterSubmitted;
        }

        send_route_message(r->session, msg);
	}
}

regist_t find_register_session(const char * key)
{
	regist_t r = (regist_t)xhash_get(register_sessions, key);
	return r;
}

void get_user_jid(const char *jid, char * buff, int buff_size)
{
	int len = buff_size;
	char *pos = strchr(jid, '/');
	
	if (pos != NULL)
	{
		len = pos - jid;
		if (len > buff_size)
			len = buff_size;
	}

	*buff = '\0';
	strncat(buff, jid, len);
}

void process_register_session_open(const char *jid)
{
	char id[512];
    regist_t r = NULL;

	get_user_jid(jid, id, sizeof(id));
    r = find_register_session(id);

    if (r != NULL)
    {
        send_register_query(r);
    }
}

void process_register_connected(const char *jid)
{
	char id[512];
    regist_t r = NULL;

	get_user_jid(jid, id, sizeof(id));
    r = find_register_session(id);

    if (r != NULL)
    {
        if (r->state == RegisterSubmitted)
        {
            r->state = RegisterActive;
        }
    }
}

void process_register_disconnected(const char *jid)
{
	char id[512];
    regist_t r = NULL;

	get_user_jid(jid, id, sizeof(id));
    r = find_register_session(id);

    if (r != NULL)
    {
        // reset state and start register sequence again
        r->state = RegisterRequest;
        send_register_query(r);
    }
}

/* return true if connected to gateway */
int is_connected(regist_t r)
{
    return (r->state == RegisterActive);
}

/* send an IM */
void send_instant_message(regist_t r, const char* username, const char* body)
{
    char* msg;

    log_debug(ZONE, "send_IM: user %s registerid %s register state %d ", username, r->id, r->state);

    if (r->state == RegisterActive && username != NULL && body != NULL)
    {
        msg = (char*) malloc(strlen(body) + 512);

        sprintf(msg, 
                "<message id='%s_%d' to='%s@%s' from='%s'><body>%s</body></message>", 
                r->id, r->index++, username, jid_full(r->service_jid), jid_full(r->my_jid), body);
        send_route_message(r->session, msg);
        free( msg );
    }
}

/* Start a registration */
regist_t register_component(const char* id, 
                            const char* service_jid, 
                            int service, 
                            const char* user,               // transport user name
                            const char* password,           // transport password
                            const char* jab_user,           // jabber user name
                            const char* jab_host)           // jabber host name
{
    char new_jid[128];
    pool p;
	regist_t r = NULL;
    jid key_jid;

	log_debug(ZONE, "register_component: service_type: %d service_id: %s user: %s host: %s", 
               service, service_jid, user, jab_host);

    // create register struct
	p = pool_new();
    r = (regist_t)pmalloco(p, sizeof(struct register_st));
	r->p = p;
    strcpy(r->id, id);
    r->state = RegisterRequest;
    r->service = service;
    strcpy(r->user, user);
    strcpy(r->pwd, password);
    r->index = 0;

    r->service_jid = jid_new(p, (char*)service_jid);

    sprintf(new_jid, "%s@%s", jab_user, jab_host);

    r->my_jid = jid_new(r->p, (char*)new_jid);
    jid_set(r->my_jid, "imtransport", JID_RESOURCE);

    r->session = create_session(new_jid, "imtransport", NULL);

    key_jid = jid_new(r->p, (char*)new_jid);

	xhash_put(register_sessions, jid_full(key_jid), r);

    return r;
}

void end_register_session(regist_t r)
{
	pool_free(r->p);
}

/* process result of register query or register auth
*/
void process_register_query(s2s_t s2s, nad_t nad)
{
	int elem = 0, attr = 0;
	int len;
	char id[512];
    char key[512];
    regist_t r;
	int result;
    char* buf = NULL;
    char* pos = NULL;
    int root = 0;
    int is_query = 0;
    int is_disconnect = 0;

#ifdef DEBUG
	nad_print(nad, 0, &buf, &len);
	log_debug(ZONE, "register packet received: %.*s", len, buf);
#endif

    // read 'from' attr
	attr = nad_find_attr(nad, elem, "from", NULL);
    if (attr < 0)
	{
		log_debug(ZONE, "routed packet has no to address, ignoring");
		goto error;
	}
	
	len = NAD_AVAL_L(nad, attr);
	if (len > sizeof(id) - 1)
		len = sizeof(id) - 1;
	strncpy(id, NAD_AVAL(nad, attr), len);
	id[len] = '\0';

    // strip off resource
    pos = strchr(id, '/');
    if (pos != NULL)
        *pos = '\0';

	r = find_register_session(id);
	if (r == NULL)
	{
		log_debug(ZONE, "route packet received for invalid session %s, ignored", id);
		goto error;
	}

    // is this result packet?
	attr = nad_find_attr(nad, elem, "type", NULL);
	if (attr >= 0 && j_strncmp(NAD_AVAL(nad, attr), "result", NAD_AVAL_L(nad, attr)) == 0)
		result = 1;
	else
		result = 0;
	
    // read key and other attrs from query
    root = 0;
    is_query = 0;
    is_disconnect = 0;

    root = nad_find_elem(nad, 0, "query", 2);
    if (root > 0)
    {
        attr = nad_find_child_elem(nad, root, "key", 1);
        if (attr > 0)
        {
            len = NAD_CDATA_L(nad, attr);
            if (len > sizeof(key) - 1) // ??? don't think we need this 
                len = sizeof(key - 1);
            strncpy(key, NAD_CDATA(nad, attr), len);
            key[len] = '\0';
        }
        attr = nad_find_child_elem(nad, root, "username", 1);
        if (attr > 0)
        {
            // will be username if session exists
            len = NAD_CDATA_L(nad, attr);
            // not used
        }
        attr = nad_find_child_elem(nad, root, "password", 1);
        if (attr > 0)
        {
            // should be blank if query response
            len = NAD_CDATA_L(nad, attr);
            if (len == 0)
                is_query = 1;
        }
        attr = nad_find_child_elem(nad, root, "registered", 1);
        if (attr > 0)
        {
            // already registered - we'll have to disconnect first
            is_disconnect = 1;
        }
    }

    // store results in session val

    // if query response send login response
    if (is_query)
    {
        strncpy(r->key, key, strlen(key));
        send_register_result(r, is_disconnect);
    }
	
	log_debug(ZONE, "register for %s read", id);
	
error:
    ;
}

