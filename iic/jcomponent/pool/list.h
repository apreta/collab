
#ifndef _QLIST_H
#define _QLIST_H

typedef struct queueNode
{
  dispatch_fn func_to_dispatch;
  void * func_arg;
  dispatch_fn cleanup_func;
  void * cleanup_arg;
  struct queueNode * next;
  struct queueNode * prev;
} queueNode;

typedef struct queueHead
{
	struct queueNode * head;
	struct queueNode * tail;
	struct queueNode * freeHead;
	struct queueNode * freeTail;
	int capacity;
	int max_capacity;
}queueHead;


struct queueHead * makeQueue(int initial_cap);

void addWorkOrder(queueHead * theQueue, dispatch_fn func1, void * arg1, dispatch_fn func2, void * arg2);

void getWorkOrder(queueHead * theQueue, dispatch_fn * func1, void ** arg1, dispatch_fn * func2, void ** arg2);

int canAcceptWork(struct queueHead * theQueue);

int isJobAvailable(struct queueHead * theQueue);

int count(struct queueHead *theQueue);

#endif
