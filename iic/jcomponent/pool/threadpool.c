/**
 * threadpool.c
 * Port to GLIB thread pool (should be portable to Win32)
 */

#ifdef LINUX
#include <stdarg.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/time.h>
#else
#include <time.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "threadpool.h"
#include "list.h"
#include "../util/log.h"


typedef void (*cleanup_fn)(void *);

typedef enum {
  ALL_RUN, ALL_EXIT
} poolstate_t;


typedef struct _threadpool_st {
  struct timeval  created;    // When the threadpool was created.
  pthread_t      *array;      // The threads themselves.

  pthread_mutex_t mutex;      // protects all vars declared below.
  pthread_cond_t  job_posted; // dispatcher: "Hey guys, there's a job!"
  pthread_cond_t  job_taken;  // a worker: "Got it!"
  
  poolstate_t     state;      // Threads check this before getting job.
  int             arrsz;      // Number of entries in array.
  int             live;       // Number of live threads in pool (when
                              //   pool is being destroyed, live<=arrsz)

  queueHead      *theQueue;      // queue of work orders
} _threadpool;


// Main function for worker thread
void * do_work(void * owning_pool)
{

  // Convert pointer to owning pool to proper type.
  _threadpool *pool = (_threadpool *) owning_pool;
  
  // Remember my creation sequence number
//  int myid = pool->live;

  // When we get a posted job, we copy it into these local vars.
  dispatch_fn  myjob;
  void        *myarg;  
  dispatch_fn  mycleaner;
  void        *mycleanarg;

  pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS,NULL);
  pthread_cleanup_push((cleanup_fn)pthread_mutex_unlock, (void *) &pool->mutex);

  // Grab mutex so we can begin waiting for a job
  if (0 != pthread_mutex_lock(&pool->mutex)) 
  {
      perror("\nMutex lock failed!:");
      exit(EXIT_FAILURE);
  }
  
  // Main loop: wait for job posting, do job(s) ... forever
  for( ; ; )
  {
    
    while(isJobAvailable(pool->theQueue)==0 && pool->state != ALL_EXIT)
    {
	  pthread_cond_wait(&pool->job_posted, &pool->mutex);
    }

    // We've just woken up and we have the mutex.  Check pool's state
    if (ALL_EXIT == pool->state)
      break;

    // while we find work to do
    getWorkOrder(pool->theQueue, &myjob, &myarg, &mycleaner, &mycleanarg);
    pthread_cond_signal(&pool->job_taken);

//    log_error(ZONE, "[%d] Unqueue request, size = %d\n", pthread_self(), count(pool->theQueue));

    // Yield mutex so other jobs can be posted
    if (0 != pthread_mutex_unlock(&pool->mutex)) 
    {
      perror("\n\nMutex unlock failed!:");
      exit(EXIT_FAILURE);
    }
    
    // Run the job we've taken
    if(mycleaner != NULL)
    {
      pthread_cleanup_push(mycleaner, mycleanarg);
      myjob(myarg);
      pthread_cleanup_pop(1);
    }
    else
    {
      myjob(myarg);
    }

    // Grab mutex so we can grab posted job, or (if no job is posted)
    //   begin waiting for next posting.
    if (0 != pthread_mutex_lock(&pool->mutex))
    {
      perror("\n\nMutex lock failed!:");
      exit(EXIT_FAILURE);
    }
  }
  
  // If we get here, we broke from loop because state is ALL_EXIT.
  --pool->live;
  

  // We're not really taking a job ... but this signals the destroyer
  //   that one thread has exited, so it can keep on destroying.
  pthread_cond_signal(&pool->job_taken);

  if (0 != pthread_mutex_unlock(&pool->mutex))
  {
    perror("\n\nMutex unlock failed!:");
    exit(EXIT_FAILURE);
  }
  pthread_cleanup_pop(0);
  return NULL;
}  

// Create thread pool
threadpool create_threadpool(int num_threads_in_pool)
{
  _threadpool *pool;  // pool we create and hand back
  int i;              // work var

  // sanity check the argument
  if ((num_threads_in_pool <= 0) || (num_threads_in_pool > MAXT_IN_POOL))
    return NULL;

  // create the _threadpool struct
  pool = (_threadpool *) malloc(sizeof(_threadpool));
  if (pool == NULL)
  {
    log_error(ZONE, "Out of memory creating a new threadpool\n");
    return NULL;
  }

  // initialize everything but the array and live thread count
  pthread_mutex_init(&(pool->mutex), NULL);
  pthread_cond_init(&(pool->job_posted), NULL);
  pthread_cond_init(&(pool->job_taken), NULL);
  pool->arrsz = num_threads_in_pool;
  pool->state = ALL_RUN;
  pool->theQueue = makeQueue(num_threads_in_pool);
  gettimeofday(&pool->created, NULL);

  // create the array of threads within the pool
  pool->array = (pthread_t *) malloc (pool->arrsz * sizeof(pthread_t));
  if (NULL == pool->array)
  {
    log_error(ZONE, "Out of memory allocating thread array\n");
    free(pool);
    pool = NULL;
    return NULL;
  }

  // bring each thread to life (update counters in loop so threads can
  //   access pool->live to find out their ID#
  for (i = 0; i < pool->arrsz; ++i)
  {
    if (0 != pthread_create(pool->array + i, NULL, do_work, (void *) pool))
    {
      perror("\n\nThread creation failed:");
      exit(EXIT_FAILURE);
    }
    pool->live = i+1;
    pthread_detach(pool->array[i]);  // automatic cleanup when thread exits.
  }

  return (threadpool) pool;
}


// Dispatch function call to worker thread.
void dispatch(threadpool from_me, dispatch_fn dispatch_to_here, void *arg)
{
  _threadpool *pool = (_threadpool *) from_me;

  pthread_cleanup_push((cleanup_fn)pthread_mutex_unlock, (void *) &pool->mutex);

  // Grab the mutex
  if (0 != pthread_mutex_lock(&pool->mutex))
  {
    perror("Mutex lock failed (!!):");
    exit(-1);
  }

  while(!canAcceptWork(pool->theQueue))
  {
    pthread_cond_signal(&pool->job_posted);
    pthread_cond_wait(&pool->job_taken,&pool->mutex);
  }

  // Finally, there's room to post a job. Do so and signal workers.
  addWorkOrder(pool->theQueue,dispatch_to_here,arg,NULL,NULL);

//  log_error(ZONE, "[%d] Queue request, size = %d\n", pthread_self(), count(pool->theQueue));

  pthread_cond_signal(&pool->job_posted);

  // Yield mutex so a worker can pick up the job
  if (0 != pthread_mutex_unlock(&pool->mutex))
  {
      perror("\n\nMutex unlock failed!:");
      exit(EXIT_FAILURE);
  }
  pthread_cleanup_pop(0);
}


// Dispatch to worker thread, with cleanup function
// Cleanup function is called if thread exits or is cancelled.
void dispatch_with_cleanup(threadpool from_me, dispatch_fn dispatch_to_here,
	      void *arg, dispatch_fn cleaner_func, void * cleaner_arg)
{
  _threadpool *pool = (_threadpool *) from_me;

  pthread_cleanup_push((cleanup_fn)pthread_mutex_unlock, (void *) &pool->mutex);

  // Grab the mutex
  if (0 != pthread_mutex_lock(&pool->mutex))
  {
    perror("Mutex lock failed (!!):");
    exit(-1);
  }

  while(!canAcceptWork(pool->theQueue))
  {
    pthread_cond_signal(&pool->job_posted);
    pthread_cond_wait(&pool->job_taken,&pool->mutex);
  }

  // Finally, there's room to post a job. Do so and signal workers.
  addWorkOrder(pool->theQueue,dispatch_to_here,arg,cleaner_func,cleaner_arg);

  pthread_cond_signal(&pool->job_posted);

  // Yield mutex so a worker can pick up the job
  if (0 != pthread_mutex_unlock(&pool->mutex))
  {
    perror("\n\nMutex unlock failed!:");
    exit(EXIT_FAILURE);
  }
  pthread_cleanup_pop(0);
}

// Destroy thread pool.  Pending jobs are canceled.
void destroy_threadpool(threadpool destroyme)
{
  _threadpool *pool = (_threadpool *) destroyme;
  int oldtype;

  log_error(ZONE, "Shutting down thread pool\n");

  pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, &oldtype);
  pthread_cleanup_push((cleanup_fn)pthread_mutex_unlock, (void *) &pool->mutex);

  // Cause all threads to exit. Because they were detached when created,
  //   the underlying memory for each is automatically reclaimed.

  // Grab the mutex
  if (0 != pthread_mutex_lock(&pool->mutex))
  {
    perror("Mutex lock failed (!!):");
    exit(-1);
  }

  pool->state = ALL_EXIT;
  
  while (pool->live > 0)
  {
    // get workers to check in ...
    pthread_cond_signal(&pool->job_posted);
    // ... and wake up when they check out.
    pthread_cond_wait(&pool->job_taken, &pool->mutex);
  }
  
  // Null-out entries in pool's thread array; free array.
  memset(pool->array, 0, pool->arrsz * sizeof(pthread_t));
  free(pool->array);
  
  // Destroy the mutex and condition variables in the pool.

  pthread_cleanup_pop(0);  
  if (0 != pthread_mutex_unlock(&pool->mutex))
  {
    perror("\n\nMutex unlock failed!:");
    exit(EXIT_FAILURE);
  }
  
  if (0 != pthread_mutex_destroy(&pool->mutex))
  {
    perror("\nMutex destruction failed!:");
    exit(EXIT_FAILURE);
  }

  if (0 != pthread_cond_destroy(&pool->job_posted))
  {
    perror("\nCondition Variable 'job_posted' destruction failed!:");
    exit(EXIT_FAILURE);
  }
  
  if (0 != pthread_cond_destroy(&pool->job_taken))
  {
    perror("\nCondition Variable 'job_taken' destruction failed!:");
    exit(EXIT_FAILURE);
  }
  
  // Zero out all bytes of the pool
  memset(pool, 0, sizeof(_threadpool));
  
  // Free the pool and null out the pointer to it
  free(pool);
  pool = NULL;
  destroyme = NULL;
}

void destroy_threadpool_immediately(threadpool destroymenow)
{
  _threadpool *pool = (_threadpool *) destroymenow;
  int oldtype,i;

  pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, &oldtype);

  pthread_cleanup_push((cleanup_fn)pthread_mutex_unlock, (void *) &pool->mutex);
  pthread_mutex_lock(&pool->mutex);

  
  for(i=0;i<pool->arrsz;i++)
  {
    if(0 != pthread_cancel(pool->array[i]))
    {
      perror("Destruction of thread failed!\n");
    }
  }

  pthread_cleanup_pop(0);

  if (0 != pthread_mutex_destroy(&pool->mutex))
  {
    perror("\nMutex destruction failed!:");
    exit(EXIT_FAILURE);
  }

  if (0 != pthread_cond_destroy(&pool->job_posted))
  {
    perror("\nCondition Variable 'job_posted' destruction failed!:");
    exit(EXIT_FAILURE);
  }
  
  if (0 != pthread_cond_destroy(&pool->job_taken))
  {
    perror("\nCondition Variable 'job_taken' destruction failed!:");
    exit(EXIT_FAILURE);
  }

  // Zero out all bytes of the pool
  memset(pool, 0, sizeof(_threadpool));
  
  // Free the pool and null out the pointer to it
  free(pool);
  pool = NULL;
  destroymenow = NULL;
}

