/**
 * threadpool.h
 *
 */

#ifndef _THREADPOOL_H
#define _THREADPOOL_H

// maximum number of threads allowed in a pool
#define MAXT_IN_POOL 200
#define THREAD_STACK_SIZE 32767

typedef void *threadpool;

typedef void (*dispatch_fn)(void *);

threadpool create_threadpool(int num_threads_in_pool);

void dispatch(threadpool from_me, dispatch_fn dispatch_to_here, void *arg);

void dispatch_with_cleanup(threadpool from_me, dispatch_fn dispatch_to_here,
			   void * arg, dispatch_fn cleaner_func, void* cleaner_arg);

void destroy_threadpool(threadpool destroyme);

void destroy_threadpool_immediately(threadpool destroymenow);

#endif
