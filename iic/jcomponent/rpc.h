/* (c) 2002 imidio all rights reserved */

#ifndef RPC_H
#define RPC_H

#include <gmodule.h>
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>


#include "jcomponent.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void * rpc_session_t;  // Alias for internal type async_packet_t

#define MAX_ID_LENGTH 64

// Methods to set up global rpc registry.
void rpc_add_method (const char *method_name,
				     xmlrpc_method method,
				     void *user_data);

void rpc_add_method_w_doc (const char *method_name,
				      xmlrpc_method method,
				      void *user_data,
				      const char *signature,
				      const char *help);

// Allow user sessions to call exposed APIs
void rpc_set_user_callable(int flag);


// If method returns this status, we assume that the actual result will be returned
// later.  No response is sent to caller until then.
#define RPC_PENDING ((xmlrpc_value*) -1)


// RPC status codes...keep distinct from API status codes.
enum RpcResult {
    RpcOk = 0,
    RpcFailed = -1,
	RpcUnreachable = -2,
	RpcInternalAPI = -3
};


typedef void (*rpc_result_fn)(const char * id, xmlrpc_value * res);

// Returns fault code from API, or from RPC layer if unreachable
typedef void (*rpc_error_fn)(const char * id, xmlrpc_int32 fault_code, const char * error);

// returns result in raw xmlrpc form
typedef void (*rpc_result_raw_fn)(const char * id, char* res);

// returns fault code in raw xmlrpc form
typedef void (*rpc_error_raw_fn)(const char * id, char* res);

// Callback is called periodically during socket processing
typedef int (*rpc_run_fn)();

// JSM has sent us status about this service
typedef void (*rpc_status_fn)(const char *status);

void rpc_set_callbacks(rpc_result_fn, rpc_error_fn, rpc_run_fn);
void rpc_set_status_callback(rpc_status_fn);

typedef void (*namespace_callback_fn)(const char* namespace_str, const char* id, s2s_t s2s, nad_t nad);
void rpc_set_namespace_callback(namespace_callback_fn);

// Callbacks set using raw xmlrpc functions
void rpc_set_callbacks_raw(rpc_result_raw_fn res, rpc_error_raw_fn err, rpc_run_fn run);

// Get source address of packet
const char *rpc_get_from(rpc_session_t ses);
const char *rpc_get_from_full(rpc_session_t ses);

// Allow this component to call other components.
int rpc_make_call(const char *id, const char *service, const char *method, const char *format, ...);

int rpc_make_call_array(const char *id, const char *service, const char *method, xmlrpc_value * arg_array);

// Make call with already formatted xml-rpc payload
int rpc_make_call_raw(const char *id, const char *service, char* data, int size);

// Send async result back to caller.
void rpc_send_result(rpc_session_t ses, xmlrpc_value * result);
void rpc_send_result_va(rpc_session_t ses, const char * format, ...);
void rpc_send_fault(rpc_session_t ses, xmlrpc_int32 fault_code, const char * msg);

// handler for namespace callbacks
void rpc_namespace(const char* namespace_str, const char* id, s2s_t s2s, nad_t nad);

// Send iq message to service
void rpc_send_iq_request(const char *id, const char *service,
                         const char *request_namespace, const char *request);

// Send IM to user
void rpc_send_im(const char *id, const char *from, const char *to, const char *message);

// Send async health request to jabber
void rpc_send_health_request(const char* id);

/* RPC methods must have following signature.  user_data actually contains
   an rpc_session_t when invoked.

   typedef xmlrpc_value *
   (*xmlrpc_method) (xmlrpc_env *env,
		  xmlrpc_value *param_array,
		  void *user_data);
*/

#ifdef __cplusplus
}
#endif

#endif
