/* --------------------------------------------------------------------------
 *
 * License
 *
 * The contents of this file are subject to the Jabber Open Source License
 * Version 1.0 (the "JOSL").  You may not copy or use this file, in either
 * source code or executable form, except in compliance with the JOSL. You
 * may obtain a copy of the JOSL at http://www.jabber.org/ or at
 * http://www.opensource.org/.  
 *
 * Software distributed under the JOSL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the JOSL
 * for the specific language governing rights and limitations under the
 * JOSL.
 *
 * Copyrights
 * 
 * Portions created by or assigned to Jabber.com, Inc. are 
 * Copyright (c) 1999-2002 Jabber.com, Inc.  All Rights Reserved.  Contact
 * information for Jabber.com, Inc. is available at http://www.jabber.com/.
 *
 * Portions Copyright (c) 1998-1999 Jeremie Miller.
 * 
 * Acknowledgements
 * 
 * Special thanks to the Jabber Open Source Contributors for their
 * suggestions and support of Jabber.
 * 
 * Alternatively, the contents of this file may be used under the terms of the
 * GNU General Public License Version 2 or later (the "GPL"), in which case
 * the provisions of the GPL are applicable instead of those above.  If you
 * wish to allow use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the JOSL,
 * indicate your decision by deleting the provisions above and replace them
 * with the notice and other provisions required by the GPL.  If you do not
 * delete the provisions above, a recipient may use your version of this file
 * under either the JOSL or the GPL. 
 * 
 * --------------------------------------------------------------------------*/

#include "jcomponent.h"

/* we call this function out of dnsrv.c, who calls our route_ip() back */
void dnsrv_lookup(s2s_t s2s, char *name);

#define MAXIPS 3 /* maximum ips stored to retry to */
/* we use this in our name lookup cache/table */
typedef struct rcache_st
{
    char *name; /* host */
    int total; /* total chunks tried to this host */
    char ips[3][22]; /* if multiple ips are returned, this is the array of them */
    int ip; /* index to the ips array of the last used ip */
    time_t timeout; /* when the ips should expire */
    time_t lastconn; /* when we last created a conn */
    chunk_t q, tail; /* any chunks queued for an ip */
} *rcache_t;

/* reset stuff for this rcache entry */
void _route_rczap(rcache_t rc)
{
    memset(rc->ips,0,sizeof(rc->ips));
    rc->ip = -1;
    rc->timeout = 0;
}

/* internal util to get or create rcache */
rcache_t _route_rcache(s2s_t s2s, char *name)
{
    rcache_t rc;

    if((rc = xhash_get(s2s->ipcache,name)) != NULL)
        return rc;

    log_debug(ZONE,"creating new route cache for %s",name);

    /* gotta create one now */
    rc = (rcache_t)pmalloco(xhash_pool(s2s->ipcache),sizeof(struct rcache_st));
    rc->name = pstrdup(xhash_pool(s2s->ipcache),name);
    xhash_put(s2s->ipcache,rc->name,(void*)rc);

    /* prime it's settings */
    _route_rczap(rc);

    return rc;
}

/* find/start connection for current ip, and dump the cache if we get one */
conn_t _route_conn(s2s_t s2s, rcache_t rc)
{
    conn_t c = NULL;
    chunk_t cur, next;

    /* first, we need an ip */
    if(rc->ip < 0) return NULL;

    log_debug(ZONE,"finding/starting a new outgoing connection for %s to %s",rc->name,rc->ips[rc->ip]);

    /* find or start a new connection */
    if((c = xhash_get(s2s->out,rc->ips[rc->ip])) == NULL)
    {
        time_t now = time(NULL);
        /* don't try again _too_ soon */
        if((now - rc->lastconn) < 30 || (c = out_conn(s2s,rc->ips[rc->ip])) == NULL)
            return NULL;
        /* remember when we last connected */
        rc->lastconn = now;
    }

    /* send the cache to the conn */
    for(cur = rc->q; cur != NULL; cur = next)
    {
        next = cur->next;
        conn_chunk_write(c,chunk_prep(cur,NULL));
    }

    rc->q = rc->tail = NULL;
    return c;
}

/* set and return the index to the current ip in ips, if any, or ask for some */
int _route_getip(s2s_t s2s, rcache_t rc)
{
    int nextip = rc->ip + 1;

    log_debug(ZONE,"looking for an ip for %s",rc->name);

    /* no ip yet, ask for one */
    if(*(rc->ips[0]) == '\0')
    {
        dnsrv_lookup(s2s,rc->name);
        _route_rczap(rc);
        return -1;
    }

    /* if we're out of ips to try, bounce */
    if(nextip >= MAXIPS)
    {
        chunk_t cur, next;
        /* !!! we could just try dnsrv_lookup(s2s,rc->name) again here, but that tends to make things work /too/ hard to get s2s conn up */
        for(cur = rc->q; cur != NULL; cur = next)
        {
            next = cur->next;
            route_app(s2s,chunk_prep(cur,"Server Connection Failed"));
        }
        rc->q = rc->tail = NULL;
        _route_rczap(rc);
        return -1;
    }

    /* just set and return the next one */
    return ++rc->ip;
}

/* tries to find another connection for this rcache */
void _route_retry(s2s_t s2s, rcache_t rc)
{
    /* just keep trying ips and conns until something happens */
    while(_route_getip(s2s,rc) >= 0 && _route_conn(s2s,rc) == NULL);
}

/* this is just a simple router to figure out which conn a chunk goes to */
void route(s2s_t s2s, chunk_t chunk)
{
    rcache_t rc;

    log_debug(ZONE,"route processing a chunk to %s",jid_full(chunk->to));

    /* almost impossible, but just in case, bail */
    if(chunk->to == NULL || chunk->to->server == NULL)
    {
        pool_free(chunk->p);
        return;
    }

    /* always make sure it's alone */
    chunk->next = NULL;

    /* set the expire time of a chunk */
    chunk->timeout = time(NULL) + s2s->timeout_packet;

    rc = _route_rcache(s2s,chunk->to->server);
    rc->total++; /* keep rough total */

    /* plug into the rcache queue */
    if(rc->tail != NULL)
    {
        /* existing queue means a dns lookup is in progress */
        rc->tail->next = chunk;
        return;
    }

    /* this chunk is the first */
    rc->q = rc->tail = chunk;

    /* try finding an existing connection first */
    if(_route_conn(s2s,rc) != NULL) return;

    /* nothing existing, try harder */
    _route_retry(s2s,rc);
}

/* dnsrv.c calls this when it gets an IP back for the given name */
void route_ip(s2s_t s2s, char *name, char *ip)
{
    rcache_t rc;
    char *comma;
    int cip;

    if(name == NULL) return;

    log_debug(ZONE,"processing resolved ip %s for %s",ip,name);

    /* get the rcache entry first */
    rc = _route_rcache(s2s, name);

    /* reset any old ip */
    _route_rczap(rc);

    /* oh, there really wasn't an ip, bounce the queue then */
    if(j_strlen(ip) < 9)
    {
        chunk_t cur, next;
        for(cur = rc->q; cur != NULL; cur = next)
        {
            next = cur->next;
            route_app(s2s,chunk_prep(cur,"Server Lookup Failed"));
        }
        rc->q = rc->tail = NULL;
        return;
    }

    /* reset timeout */
    rc->timeout = time(NULL) + s2s->timeout_ipcache;

    /* parse out "1.2.3.4:1234,2.3.4.5:2345" format into our array */
    for(cip = 0; cip < MAXIPS && ip != NULL; cip++)
    {
        if((comma = strchr(ip,',')) != NULL)
        {
            *comma = '\0';
            comma++;
        }
        snprintf(rc->ips[cip],22,"%s",ip);
        ip = comma;
    }

    /* chunks, start/send to a conn */
    if(rc->q != NULL)
        _route_retry(s2s,rc);
}

/* this checks for bad conns that have timed out */
void _walk_pending(xht pending, const char *key, void *val, void *arg)
{
    conn_t c = (conn_t)val;
    time_t now = (time_t)arg;

    /* !!! do more here of course */
    if((now - c->start) > c->s2s->timeout_idle)
        mio_close(c->s2s->mio, c->fd);
}

/* expire chunks from the xhashes for ipcache and out */
void route_expire(s2s_t s2s)
{
    log_debug(ZONE,"expire checking chunks everywhere");
    /* !!! walk the hashes */
}

void route_app(s2s_t s2s, chunk_t chunk)
{
    if(chunk == NULL) return;

    /* always make sure it's alone */
    chunk->next = NULL;

    /* catch the first time */
    if(s2s->appdata == NULL)
        s2s->appdata = chunk;
    else
        s2s->appdatat->next = chunk;

    /* always update tail pointer */
    s2s->appdatat = chunk;
}
