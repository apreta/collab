/* (c) 2002 imidio all rights reserved */

#include <stdio.h>
#ifdef LINUX
#include <dlfcn.h>
#endif
#ifdef _WIN32
#include <winsock2.h>
#endif

#include <glib.h>

#define XMLRPC_WANT_INTERNAL_DECLARATIONS
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>

#ifndef XMLRPC_VERSION
#include <xmlrpc-c/abyss.h>
#else
#include <xmlrpc-c/base_int.h>
#endif

#include "jcomponent.h"
#include "rpc.h"
#include "presence.h"
#include "util/log.h"

/* RPC method registry */
static xht rpc_methods = NULL;

static s2s_t rpc_s2s;
static const char * host_jid = NULL;
static const char * service_name = NULL;
static const char * jsm_jid = NULL;
static int heartbeat_interval = 0;
static time_t heartbeat_last = 0;
static int user_callable = 0;

static rpc_result_fn rpc_result = NULL;
static rpc_error_fn rpc_error = NULL;
static rpc_run_fn rpc_lib_run = NULL;
static rpc_result_raw_fn rpc_result_raw = NULL;
static rpc_error_raw_fn rpc_error_raw = NULL;
static rpc_status_fn rpc_status_cb = NULL;
static namespace_callback_fn rpc_namespace_cb = NULL;


/*
  Quicky RPC implementation

  To do:
	Allow callback API to be specified when making RPC call (so results are dispatched
	to specific handler rather then generic result handler)
*/


/* Exits on configuration errors.
*/
static void die_if_fault_occurred (xmlrpc_env *env)
{
    if (env->fault_occurred) {
        log_error(ZONE, "Unexpected XML-RPC fault: %s (%d)\n",
                env->fault_string, env->fault_code);
        exit(1);
    }
}

xmlrpc_value *
rpc_log_level(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    xmlrpc_int32 level;

    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(i)", &level);
    if (env->fault_occurred)
		return NULL;

	log_set_level(level);

    /* Return our result. */
    return xmlrpc_build_value(env, "(i)", 0);
}

xmlrpc_value *
sample_add (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    xmlrpc_int32 x, y, z;

    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(ii)", &x, &y);
    if (env->fault_occurred)
		return NULL;

    /* Add our two numbers. */
    z = x + y;

    /* Return our result. */
    return xmlrpc_build_value(env, "(i)", z);
}

struct sample_session
{
    rpc_session_t session;
    int sum;
};

#ifdef TEST_ASYNC
void * sample_async(void * data)
{
    xmlrpc_env env;
    xmlrpc_value * result = NULL;
    struct sample_session *ses = (struct sample_session *)data;

    sleep(10);

    xmlrpc_env_init(&env);

    result = xmlrpc_build_value(&env, "(i)", ses->sum);

    rpc_send_result(ses->session, result);

    xmlrpc_DECREF(result);
    xmlrpc_env_clean(&env);
    free(ses);

    return NULL;
}

xmlrpc_value *
sample_add_async (xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    xmlrpc_int32 x, y, z;
    struct sample_session * ses;
    pthread_t thread;

    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(ii)", &x, &y);
    if (env->fault_occurred)
	    return NULL;

    /* Add our two numbers. */
    z = x + y;

    /* Dispatch to background thread to send actual response later */
    ses = (struct sample_session *)malloc(sizeof(struct sample_session));
    ses->session = user_data;
    ses->sum = z;

    pthread_create(&thread, NULL, sample_async, (void *) ses);
    pthread_detach(thread);

    /* We return special status */
    return RPC_PENDING;
}
#endif

/* Build service address given name and jsm address.
 */
static const char * get_service_address(const char * service_name, char * buffer)
{
	if (strchr(service_name, '@') == NULL)
		sprintf(buffer, "%s.service@%s", service_name, jsm_jid);
	else
		strcpy(buffer, service_name);
	return buffer;
}

/* Set up our callbacks */
void rpc_set_callbacks(rpc_result_fn res, rpc_error_fn err, rpc_run_fn run)
{
	rpc_result = res;
	rpc_error = err;
	rpc_lib_run = run;

	rpc_result_raw = NULL;
	rpc_error_raw = NULL;
}

void rpc_set_user_callable(int flag)
{
    user_callable = flag;
}

void rpc_set_status_callback(rpc_status_fn status)
{
    rpc_status_cb = status;
}

void rpc_set_namespace_callback(namespace_callback_fn callback)
{
    rpc_namespace_cb = callback;
}

/* Set up our callbacks to use raw xmlrpc callbacks */
void rpc_set_callbacks_raw(rpc_result_raw_fn res, rpc_error_raw_fn err, rpc_run_fn run)
{
	rpc_result_raw = res;
	rpc_error_raw = err;
	rpc_lib_run = run;
	rpc_result = NULL;
	rpc_error = NULL;
}

/* A quick & easy shorthand for adding a method. */
void rpc_add_method (const char *method_name,
				     xmlrpc_method method,
				     void *user_data)
{
    xhash_put(rpc_methods, method_name, method);
}

void rpc_add_method_w_doc (const char *method_name,
				      xmlrpc_method method,
				      void *user_data,
				      const char *signature,
				      const char *help)
{
    xhash_put(rpc_methods, method_name, method);
}

/* Lifted internal function from xmlrpc.  Perhaps a later version will
   expose this useful API also. */
static xmlrpc_value *
dispatch_call(xmlrpc_env *env, const char *method_name, xmlrpc_value *param_array, void * user)
{
    xmlrpc_value *result;
    xmlrpc_method method;

    result = NULL;

    /* Get address of implementation method
	   (xhash should be safe for multi-threaded read only access) */
    method = xhash_get(rpc_methods, method_name);
    if (method == NULL) {
        XMLRPC_FAIL1(env, XMLRPC_NO_SUCH_METHOD_ERROR, "Method %s not defined", method_name);
    } else {
        /* And call it... */
        result = (*method)(env, param_array, user);
        XMLRPC_FAIL_IF_FAULT(env);
    }

 cleanup:
    if (env->fault_occurred) {
        if (result)
            xmlrpc_DECREF(result);
        return NULL;
    }
    return result;
}

/* Initialize rpc subsystem. */
int rpc_init (s2s_t s2s, const char *host, const char * jsm, const char * service, int heartbeat, const char *config)
{
    //xmlrpc_env env;

    rpc_s2s = s2s;
    host_jid = host;
    jsm_jid = jsm;
    service_name = service;
    heartbeat_interval = heartbeat;

    rpc_methods = xhash_new(517);

    /* Set up test API */
    rpc_add_method("log.set_level", &rpc_log_level, NULL);
    rpc_add_method("sample.add", &sample_add, NULL);
#ifdef ASYNC_TEST
    rpc_add_method("sample.async", &sample_add_async, NULL);
#endif
    return 0;
}

int rpc_shutdown(s2s_t s2s)
{
	return 0;
}

int rpc_connected(s2s_t s2s)
{
	/* if service name was specified, send location message to JSM */
	if (service_name != NULL)
	{
		chunk_t chunk;
		char location_service[256];
		char my_service[256];
		char epoch[32];
		char lasthost[256];

		get_service_address("location", location_service);
		get_service_address(service_name, my_service);
		sprintf(epoch, "%d", (int)time(NULL));
		gethostname(lasthost, sizeof lasthost);

    	chunk = chunk_new(chunk_PACKET, (char *)location_service, (char *)my_service);
        spooler(chunk->regen,
                    "<iq type='set' to='", location_service,"' from='", my_service,
    				"' id='reg'>",
    				"<query xmlns='iic:location'>",
					"<available service='", service_name,
					"' host='", host_jid, "' epoch='",
					epoch,
		                        "' lasthost='", lasthost, "'/>",
					"</query></iq>",
    				chunk->regen);
		chunk->data = chunk->wcur = spool_print(chunk->regen);
		chunk->wlen = strlen(chunk->data);

		log_debug(ZONE,"register component %s", chunk->data);

		conn_chunk_write(s2s->sm, chunk);
	}

	return 0;
}

void rpc_status(const char * string)
{
	log_debug(ZONE,"status message %s", string);
    if (rpc_status_cb)
        rpc_status_cb(string);
}

void rpc_namespace(const char* namespace, const char* id, s2s_t s2s, nad_t nad)
{
	log_debug(ZONE,"namespace callback");
    if (rpc_namespace_cb)
        rpc_namespace_cb(namespace, id, s2s, nad);
}


// Send message to service.
void rpc_send_iq_request(const char *id, const char *service,
                         const char *namespace, const char *request)
{
	chunk_t chunk;
	char to_service[256];
	char my_service[256];

	get_service_address(service, to_service);
	get_service_address(service_name, my_service);

    chunk = chunk_new(chunk_PACKET, (char *)to_service, (char *)my_service);
    spooler(chunk->regen,
                "<iq type='set' to='", to_service,"' from='", my_service,
    			"' id='", id, "'>",
    			"<query xmlns='", namespace, "'>",
                request,
				"</query></iq>",
    			chunk->regen);
	chunk->data = chunk->wcur = spool_print(chunk->regen);
	chunk->wlen = strlen(chunk->data);

	log_debug(ZONE,"iq message sent to %s: %s", to_service, chunk->data);

	conn_chunk_write(rpc_s2s->sm, chunk);
}


// Send IM to user.
void rpc_send_im(const char *id, const char *from, const char *to, const char *message)
{
	chunk_t chunk;

    chunk = chunk_new(chunk_PACKET, (char *)to, (char *)from);
    spooler(chunk->regen,
                "<message type='chat' to='", to,"' from='", from,
    			"' id='", id, "'>",
    			"<body>",
                message,
				"</body></message>",
    			chunk->regen);
	chunk->data = chunk->wcur = spool_print(chunk->regen);
	chunk->wlen = strlen(chunk->data);

	log_debug(ZONE,"im message sent to %s: %s", to, chunk->data);

	conn_chunk_write(rpc_s2s->sm, chunk);
}


// send a health request
int send_health(const char* id, s2s_t s2s)
{
	chunk_t chunk;
	char location_service[256];
	char my_service[256];

	get_service_address("health-service", location_service);
	get_service_address(service_name, my_service);

    chunk = chunk_new(chunk_PACKET, (char *)location_service, (char *)my_service);
    spooler(chunk->regen,
                "<iq type='set' to='", location_service,"' from='", my_service,
    			"' id='", id, "'>",
    			"<query xmlns='iic:health'>",
				"</query></iq>",
    			chunk->regen);
	chunk->data = chunk->wcur = spool_print(chunk->regen);
	chunk->wlen = strlen(chunk->data);

	log_debug(ZONE,"health request for component %s", chunk->data);

	conn_chunk_write(s2s->sm, chunk);

	return 0;
}

void rpc_send_health_request(const char* id)
{
    send_health(id, rpc_s2s);
}

int send_heartbeat(s2s_t s2s)
{
	if (service_name != NULL)
	{
		chunk_t chunk;
		char location_service[256];
		char my_service[256];

		get_service_address("heartbeat-service", location_service);
		get_service_address(service_name, my_service);

    	chunk = chunk_new(chunk_PACKET, (char *)location_service, (char *)my_service);
        spooler(chunk->regen,
                    "<iq type='set' to='", location_service,"' from='", my_service,
    				"' id='reg'>",
    				"<query xmlns='iic:heartbeat'>",
					"<beat service='", service_name,
					"' host='", host_jid,"'/>",
					"</query></iq>",
    				chunk->regen);
		chunk->data = chunk->wcur = spool_print(chunk->regen);
		chunk->wlen = strlen(chunk->data);

		log_debug(ZONE,"heartbeat for component %s", chunk->data);

		conn_chunk_write(s2s->sm, chunk);
	}

	return 0;
}

/* Called during main loop, allows library to perform periodic processing
   if required. Return -1 on error, 0 if no work found, 1 if work done */
int rpc_run()
{
	static int count = 0;

	int res = 0;
    if (rpc_lib_run)
        res = rpc_lib_run();

	/* main loop sleeps 20ms if no work available */
	if (heartbeat_interval > 0 && res == 0)
	{
		if (count++ >= 50)
		{
			time_t now = time(NULL);
			if (now - heartbeat_last > heartbeat_interval)
			{
				/* send message */
				send_heartbeat(rpc_s2s);

				heartbeat_last = now;
				count = 0;
			}
		}
	}

    return res;
}

/* Process incoming rpc call */
void process_rpc(async_packet_t pkt)
{
    xmlrpc_env env;
    xmlrpc_env fault;
    xmlrpc_mem_block *output = NULL;
    char *method_name = NULL;
    xmlrpc_value *param_array = NULL;
    xmlrpc_value *result = NULL;
    chunk_t out_chunk;
    char *pi;
    char *data;
    int size;
    chunk_t src_chunk = pkt->chunk;

    xmlrpc_env_init(&env);
    xmlrpc_env_init(&fault);

    if (!user_callable && strstr(jid_full(src_chunk->from), ".service@") == NULL)
    {
        xmlrpc_env_set_fault(&fault, -1, "APIs unavailable from user sessions");
    }

    /* Parse the call. */
    if (!fault.fault_occurred)
        xmlrpc_parse_call(&fault, src_chunk->rdata, src_chunk->rlen, (const char **)&method_name, &param_array);

    /* Perform the call. */
    if (!fault.fault_occurred)
        result = dispatch_call(&fault, method_name, param_array, pkt);

    if (fault.fault_occurred)
    {
        output = xmlrpc_mem_block_new(&env, 0);
        die_if_fault_occurred(&env);

	    xmlrpc_serialize_fault(&env, output, &fault);
        XMLRPC_FAIL_IF_FAULT(&env);
    }
    else if (result != RPC_PENDING)
    {
        output = xmlrpc_mem_block_new(&env, 0);
        die_if_fault_occurred(&env);

        xmlrpc_serialize_response(&env, output, result);
        XMLRPC_FAIL_IF_FAULT(&env);
    }
    else
    {
        log_debug(ZONE, "Request %s executing async, result is pending", src_chunk->id);
        pkt->async = 1;
        result = NULL;
    }

    if (output)
    {
        data = XMLRPC_TYPED_MEM_BLOCK_CONTENTS(char, output);
        size = XMLRPC_TYPED_MEM_BLOCK_SIZE(char, output);

    	// Remove xml pi
    	pi = strstr(data, "?>");
    	if (pi != NULL)
    	{
    		pi += 2;
    		size -= (pi - data);
    		data = pi;
    	}

    	/* send result */
    	out_chunk = chunk_new(chunk_PACKET, jid_full(src_chunk->from), jid_full(src_chunk->to));
        spooler(out_chunk->regen,
                    "<iq type='result' to='", jid_full(src_chunk->from),"' from='", jid_full(src_chunk->to),
    				"' id='",src_chunk->id,"'>",
    				"<query xmlns='iic:iq:rpc'>",
    				out_chunk->regen);
    	spool_add_buffer(out_chunk->regen, data, size);
        spool_add(out_chunk->regen, "</query></iq>");

        // Actual write is delegated to caller
        pkt->out_chunk = out_chunk;
    }

cleanup:
	/* clean up */
    if (output)
		xmlrpc_mem_block_free(output);
    if (method_name)
	   	free(method_name);
    if (param_array)
	    xmlrpc_DECREF(param_array);
    if (result)
	    xmlrpc_DECREF(result);

    xmlrpc_env_clean(&env);
    xmlrpc_env_clean(&fault);
}

/* Get source address of request */
const char *rpc_get_from(rpc_session_t ses)
{
    async_packet_t pkt = (async_packet_t)ses;
    chunk_t src_chunk = pkt->chunk;

    return jid_full(jid_user(src_chunk->from));
}

const char *rpc_get_from_full(rpc_session_t ses)
{
    async_packet_t pkt = (async_packet_t)ses;
    chunk_t src_chunk = pkt->chunk;

    return jid_full(src_chunk->from);
}


/* Send result from async call back to caller.
   The rpc_session_t handle (currently pointer to async_packet_t) is passed to rpc functions
   in the user_data parameter.
   The rpc method returns RPC_PENDING to indicate that the response will be sent at a later
   time.
 */
void rpc_send_result(rpc_session_t ses, xmlrpc_value * result)
{
    xmlrpc_env env;
    xmlrpc_mem_block *output;
    chunk_t out_chunk;
    char *pi;
    char *data;
    int size;
    async_packet_t pkt = (async_packet_t)ses;
    chunk_t src_chunk = pkt->chunk;

    log_debug(ZONE, "Sending result for request %s", src_chunk->id);

	if (rpc_s2s->sm == NULL)
	{
		log_error(ZONE, "Connection to SM not available");
		return;
	}

    xmlrpc_env_init(&env);

    output = xmlrpc_mem_block_new(&env, 0);
    XMLRPC_FAIL_IF_FAULT(&env);

    /* Pass call to RPC layer */
    xmlrpc_serialize_response(&env, output, result);
    XMLRPC_FAIL_IF_FAULT(&env);

    data = XMLRPC_TYPED_MEM_BLOCK_CONTENTS(char, output);
    size = XMLRPC_TYPED_MEM_BLOCK_SIZE(char, output);

	// Remove xml pi
	pi = strstr(data, "?>");
	if (pi != NULL)
	{
		pi += 2;
		size -= (pi - data);
		data = pi;
	}

	/* send result */
	out_chunk = chunk_new(chunk_PACKET, jid_full(src_chunk->from), jid_full(src_chunk->to));
    spooler(out_chunk->regen,
                "<iq type='result' to='", jid_full(src_chunk->from),"' from='", jid_full(src_chunk->to),
				"' id='",src_chunk->id,"'>",
				"<query xmlns='iic:iq:rpc'>",
				out_chunk->regen);
	spool_add_buffer(out_chunk->regen, data, size);
    spool_add(out_chunk->regen, "</query></iq>");

    /* Send packet */
    conn_chunk_write(rpc_s2s->sm, chunk_prep(out_chunk, NULL));

    /* Out chunk will be freed after sending. We free the rest now. */
    if (pkt->complete)
        pkt->complete(pkt);

cleanup:
	/* clean up */
    if (output)
		xmlrpc_mem_block_free(output);

    xmlrpc_env_clean(&env);
}

void rpc_send_result_va(rpc_session_t ses, const char * format, ...)
{
    xmlrpc_env env;
    xmlrpc_mem_block *output;
    chunk_t out_chunk;
    char *pi;
    char *data;
    int size;
    async_packet_t pkt = (async_packet_t)ses;
    chunk_t src_chunk = pkt->chunk;
    xmlrpc_value *result = NULL;
	va_list args;
	va_start(args, format);
#ifdef XMLRPC_VERSION
	const char * suffix;
#endif

    log_debug(ZONE, "Sending result for request %s", src_chunk->id);

	if (rpc_s2s->sm == NULL)
	{
		log_error(ZONE, "Connection to SM not available");
		return;
	}

    xmlrpc_env_init(&env);

	/* Build output result using format */
#ifdef XMLRPC_VERSION
	xmlrpc_build_value_va(&env, format, args, &result, &suffix);
#else
	result = xmlrpc_build_value_va(&env, (char *)format, args);
#endif
	if (env.fault_occurred)
	{
		log_error(ZONE, "Error building result for request\n", src_chunk->id);
		goto cleanup;
	}

	va_end(args);

    output = xmlrpc_mem_block_new(&env, 0);
    XMLRPC_FAIL_IF_FAULT(&env);

    /* Pass call to RPC layer to serialize into XML */
    xmlrpc_serialize_response(&env, output, result);
    XMLRPC_FAIL_IF_FAULT(&env);

    data = XMLRPC_TYPED_MEM_BLOCK_CONTENTS(char, output);
    size = XMLRPC_TYPED_MEM_BLOCK_SIZE(char, output);

	// Remove xml pi
	pi = strstr(data, "?>");
	if (pi != NULL)
	{
		pi += 2;
		size -= (pi - data);
		data = pi;
	}

	/* send result */
	out_chunk = chunk_new(chunk_PACKET, jid_full(src_chunk->from), jid_full(src_chunk->to));
    spooler(out_chunk->regen,
                "<iq type='result' to='", jid_full(src_chunk->from),"' from='", jid_full(src_chunk->to),
				"' id='",src_chunk->id,"'>",
				"<query xmlns='iic:iq:rpc'>",
				out_chunk->regen);
	spool_add_buffer(out_chunk->regen, data, size);
    spool_add(out_chunk->regen, "</query></iq>");

    /* Send packet */
    conn_chunk_write(rpc_s2s->sm, chunk_prep(out_chunk, NULL));

    /* Out chunk will be freed after sending. We free the rest now. */
    if (pkt->complete)
        pkt->complete(pkt);

cleanup:
	/* clean up */
	if (result)
		xmlrpc_DECREF(result);
    if (output)
		xmlrpc_mem_block_free(output);

    xmlrpc_env_clean(&env);
}

void rpc_send_fault(rpc_session_t ses, xmlrpc_int32 fault_code, const char * msg)
{
    xmlrpc_env env;
	xmlrpc_env fault_env;
    xmlrpc_mem_block *output;
    chunk_t out_chunk;
    char *pi;
    char *data;
    int size;
    async_packet_t pkt = (async_packet_t)ses;
    chunk_t src_chunk = pkt->chunk;

    log_debug(ZONE, "Sending fault for request %s", src_chunk->id);

	if (rpc_s2s->sm == NULL)
	{
		log_error(ZONE, "Connection to SM not available");
		return;
	}

    xmlrpc_env_init(&env);
	xmlrpc_env_init(&fault_env);

    output = xmlrpc_mem_block_new(&env, 0);
    XMLRPC_FAIL_IF_FAULT(&env);

    /* Pass call to RPC layer */
    xmlrpc_env_set_fault(&fault_env, fault_code, msg);

    xmlrpc_serialize_fault(&env, output, &fault_env);
    XMLRPC_FAIL_IF_FAULT(&env);

    data = XMLRPC_TYPED_MEM_BLOCK_CONTENTS(char, output);
    size = XMLRPC_TYPED_MEM_BLOCK_SIZE(char, output);

	// Remove xml pi
	pi = strstr(data, "?>");
	if (pi != NULL)
	{
		pi += 2;
		size -= (pi - data);
		data = pi;
	}

	/* send result */
	out_chunk = chunk_new(chunk_PACKET, jid_full(src_chunk->from), jid_full(src_chunk->to));
    spooler(out_chunk->regen,
                "<iq type='result' to='", jid_full(src_chunk->from),"' from='", jid_full(src_chunk->to),
				"' id='",src_chunk->id,"'>",
				"<query xmlns='iic:iq:rpc'>",
				out_chunk->regen);
	spool_add_buffer(out_chunk->regen, data, size);
    spool_add(out_chunk->regen, "</query></iq>");

    /* Send packet */
    conn_chunk_write(rpc_s2s->sm, chunk_prep(out_chunk, NULL));

    /* Out chunk will be freed after sending. We free the rest now. */
    if (pkt->complete)
        pkt->complete(pkt);

cleanup:
	/* clean up */
    if (output)
		xmlrpc_mem_block_free(output);

    xmlrpc_env_clean(&env);
    xmlrpc_env_clean(&fault_env);
}

/* Allow component to make rpc calls */
int rpc_make_call(const char *id, const char *service, const char *method, const char *format, ...)
{
    int err = RpcOk;
    chunk_t out_chunk;
    va_list args;
    xmlrpc_env env;
    xmlrpc_value *arg_array = NULL;
    xmlrpc_mem_block *req = NULL;
    char *pi;
    char *data;
    int size;
	char to_jid[256];
	char from_jid[256];
#ifdef XMLRPC_VERSION
	const char *suffix;
#endif

	if (rpc_s2s->sm == NULL)
	{
		log_error(ZONE, "Connection to SM not available");
		return RpcUnreachable;
	}

    xmlrpc_env_init(&env);

    XMLRPC_ASSERT_PTR_OK(format);

    /* Build our argument array. */
    va_start(args, format);
#ifdef XMLRPC_VERSION
	xmlrpc_build_value_va(&env, format, args, &arg_array, &suffix);
#else
    arg_array = xmlrpc_build_value_va(&env, (char *)format, args);
#endif

    va_end(args);
    XMLRPC_FAIL_IF_FAULT(&env);

    /* Serialize parameters */
    req = xmlrpc_mem_block_new(&env, 0);
    XMLRPC_FAIL_IF_FAULT(&env);
    xmlrpc_serialize_call(&env, req, (char *)method, arg_array);
    XMLRPC_FAIL_IF_FAULT(&env);

    data = xmlrpc_mem_block_contents(req);
    size = xmlrpc_mem_block_size(req);

	// Remove xml pi
	pi = strstr(data, "?>");
	if (pi != NULL)
	{
		pi += 2;
		size -= (pi - data);
		data = pi;
	}

	get_service_address(service, to_jid);
	get_service_address(service_name, from_jid);

    /* Make packet */
    out_chunk = chunk_new(chunk_PACKET, (char *)to_jid, (char *)from_jid);
    spooler(out_chunk->regen,
                "<iq type='set' to='", jid_full(out_chunk->to),"' from='", jid_full(out_chunk->from),
                "' id='",id,"'>",
                "<query xmlns='iic:iq:rpc'>",
                out_chunk->regen);
    spool_add_buffer(out_chunk->regen, data, size);
    spool_add(out_chunk->regen, "</query></iq>");

    /* Send packet */
    conn_chunk_write(rpc_s2s->sm, chunk_prep(out_chunk, NULL));

    /* Chunk will be freed after sending. */

cleanup:
    if (env.fault_occurred)
    {
        err = RpcFailed;
    }
    if (req)
        xmlrpc_mem_block_free(req);
    if (arg_array)
        xmlrpc_DECREF(arg_array);
    xmlrpc_env_clean(&env);

    return err;
}

// Make call with already prepared argument list.
int rpc_make_call_array(const char *id, const char *service, const char *method, xmlrpc_value * arg_array)
{
    int err = RpcOk;
    chunk_t out_chunk;
    xmlrpc_env env;
    xmlrpc_mem_block *req = NULL;
    char *pi;
    char *data;
    int size;
	char to_jid[256];
	char from_jid[256];

	if (rpc_s2s->sm == NULL)
	{
		log_error(ZONE, "Connection to SM not available");
		return RpcUnreachable;
	}

    xmlrpc_env_init(&env);

    /* Serialize parameters */
    req = xmlrpc_mem_block_new(&env, 0);
    XMLRPC_FAIL_IF_FAULT(&env);
    xmlrpc_serialize_call(&env, req, (char *)method, arg_array);
    XMLRPC_FAIL_IF_FAULT(&env);

    data = xmlrpc_mem_block_contents(req);
    size = xmlrpc_mem_block_size(req);

	// Remove xml pi
	pi = strstr(data, "?>");
	if (pi != NULL)
	{
		pi += 2;
		size -= (pi - data);
		data = pi;
	}

	get_service_address(service, to_jid);
	get_service_address(service_name, from_jid);

    /* Make packet */
    out_chunk = chunk_new(chunk_PACKET, (char *)to_jid, (char *)from_jid);
    spooler(out_chunk->regen,
                "<iq type='set' to='", jid_full(out_chunk->to),"' from='", jid_full(out_chunk->from),
                "' id='",id,"'>",
                "<query xmlns='iic:iq:rpc'>",
                out_chunk->regen);
    spool_add_buffer(out_chunk->regen, data, size);
    spool_add(out_chunk->regen, "</query></iq>");

    /* Send packet */
    conn_chunk_write(rpc_s2s->sm, chunk_prep(out_chunk, NULL));

    /* Chunk will be freed after sending. */

cleanup:
    if (env.fault_occurred)
    {
        err = RpcFailed;
    }
    if (req)
        xmlrpc_mem_block_free(req);

	xmlrpc_env_clean(&env);

    return err;
}

// Make call with already formatted xml-rpc payload
int rpc_make_call_raw(const char *id, const char *service, char* data, int size)
{
    int err = RpcOk;
    chunk_t out_chunk;
    xmlrpc_env env;
    xmlrpc_value *arg_array = NULL;
    xmlrpc_mem_block *req = NULL;
    char *pi;
	char to_jid[256];
	char from_jid[256];

	if (rpc_s2s->sm == NULL)
	{
		log_error(ZONE, "Connection to SM not available");
		return RpcUnreachable;
	}

    xmlrpc_env_init(&env);
    XMLRPC_FAIL_IF_FAULT(&env);

	// Remove xml pi
	pi = strstr(data, "?>");
	if (pi != NULL)
	{
		pi += 2;
		size -= (pi - data);
		data = pi;
	}

	get_service_address(service, to_jid);
	get_service_address(service_name, from_jid);

    /* Make packet */
    out_chunk = chunk_new(chunk_PACKET, (char *)to_jid, (char *)from_jid);
    spooler(out_chunk->regen,
                "<iq type='set' to='", jid_full(out_chunk->to),"' from='", jid_full(out_chunk->from),
                "' id='",id,"'>",
                "<query xmlns='iic:iq:rpc'>",
                out_chunk->regen);
    spool_add_buffer(out_chunk->regen, data, size);
    spool_add(out_chunk->regen, "</query></iq>");

    /* Send packet */
    conn_chunk_write(rpc_s2s->sm, chunk_prep(out_chunk, NULL));

    /* Chunk will be freed after sending. */

cleanup:
    if (env.fault_occurred)
    {
        err = RpcFailed;
    }
    if (req)
        xmlrpc_mem_block_free(req);
    if (arg_array)
        xmlrpc_DECREF(arg_array);
    xmlrpc_env_clean(&env);

    return err;
}

// strip out trailing jabber
void process_rpc_result_raw(const char* id, char* result)
{
    char* query;

    // Remove trailing jabber </query></iq>
    query = strstr(result, "</query>");
    if (query != NULL)
    {
    	*query = '\0';
    }
    rpc_result_raw(id, result);
}

void process_rpc_error_raw(const char* id, char* result)
{
    char* query;

    // Remove trailing jabber </query></iq>
    query = strstr(result, "</query>");
    if (query != NULL)
    {
    	*query = '\0';
    }
    rpc_error_raw(id, result);
}

/* Handle error packet.
*/
void process_rpc_error(const char * id, int error_code, const char * msg)
{
	if (rpc_error)
		rpc_error(id, error_code, msg);
    else if (rpc_error_raw)
        process_rpc_error_raw(id, (char*) msg);
}

/* Handle incoming rpc result.
   Parse and dispatch to handler in loaded library. */
void process_rpc_result(async_packet_t pkt)
{
    xmlrpc_value *retval = NULL;
    xmlrpc_env env;
    chunk_t src_chunk = pkt->chunk;

    xmlrpc_env_init(&env);

    retval = xmlrpc_parse_response(&env, src_chunk->rdata, src_chunk->rlen);
    XMLRPC_FAIL_IF_FAULT(&env);

    // send result along...
    if (rpc_result != NULL)
        rpc_result(src_chunk->id, retval);
    else if (rpc_result_raw != NULL)
        process_rpc_result_raw(src_chunk->id, src_chunk->rdata);
    else
        log_error(ZONE, "Result received for RPC request, but no handler defined\n");

cleanup:
    if (env.fault_occurred && (rpc_error != NULL || rpc_error_raw != NULL))
    {
        char buffer[512];
        strcpy(buffer, "Server error: ");
        if (env.fault_string)
            strncat(buffer, env.fault_string, 512 - 16);
        else
            strcat(buffer, "unknown cause");
        if (rpc_error != NULL)
            rpc_error(src_chunk->id, env.fault_code, buffer);
        else if (rpc_error_raw != NULL)
            process_rpc_error_raw(src_chunk->id, src_chunk->rdata);
        else
            log_error(ZONE, "Fault received for RPC request, but no handler defined\n");
    }
    if (retval)
        xmlrpc_DECREF(retval);
    xmlrpc_env_clean(&env);
}
