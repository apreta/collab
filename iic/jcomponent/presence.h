/* (c) 2002 imidio all rights reserved */

#ifndef PRESENCE_H
#define PRESENCE_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct session_st * session_t;

#define MAX_ID_LENGTH 64
#define MAX_STATUS_LENGTH 64
#define MAX_SESSIONS 11003

enum PresenceStatus
{
	PresenceUnavailable = 0,
	PresenceAvailable
};

// Announce presence info for session
typedef void (*jc_presence_fn)(const char * jid, int available, const char * status);

typedef void (*jc_auth_fn)(const char * jid, int ok, void * user);

// Start/end session for user.
session_t create_session(const char * jid, const char * resource, void * user);
session_t create_auth_session(void * user);
int send_auth_session_request(session_t s, const char * jid, const char * resource, const char * password);
void end_session(session_t s); 

void send_route_message(session_t s, const char* msg);

void jc_set_auth_callback(jc_auth_fn);

// Send presence for indicated session
void jc_set_presence_callback(jc_presence_fn);
void send_presence(session_t s, const char * status);

#ifdef __cplusplus
}
#endif

#endif
