��          �            x     y  �   �  *     ;   J  >   �  -   �  -   �  (   !     J     O     _     l     {     �  ]   �  5  �     ,  �   H  0   �  C   "  K   f  5   �  1   �  L        g     k     �     �  
   �     �  ~   �                                                      
                  	       %a %b %d, %Y at %I:%M:%S %p Although you have turned on recording, there is no voice/data has been recorded.  No movie or mp3 file is produced for this meeting (%s). Click the following link for meeting chat: Click the following link for meeting recording (data-only): Click the following link for meeting recording (voice & data): Click the following link for meeting summary: Click the following link for voice recording: Failed to process your meeting recording From Meeting summary No recording None specified Now (%s) RE: Preview request There is no recording for this meeting (%s), no movie or mp3 file is produced for previewing. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-04-26 18:10-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 %a %b %d, %Y at %I:%M:%S %p Obwohl die Aufzeichnung aktiviert wurde, sind keine Sprach-/Datenaufzeichnungen vorhanden. Für diese Besprechung (%s) wird weder ein Film noch eine MP3-Datei erstellt. Folgenden Link für Besprechungs-Chat anklicken: Folgenden Link für Besprechungsaufzeichnung (nur Daten) anklicken: Folgenden Link für Besprechungsaufzeichnung (Sprache und Daten) anklicken: Folgenden Link für Besprechungsübersicht anklicken: Folgenden Link für Sprachaufzeichnung anklicken: Bei der Verarbeitung der Besprechungsaufzeichnung ist ein Fehler aufgetreten Von Besprechungsübersicht Keine Aufzeichnung Nicht angegeben Jetzt (%s) Betreff: Vorschauanforderung Von dieser Besprechung (%s) gibt es keine Aufzeichnung. Es wird weder ein Film noch eine MP3-Datei für die Vorschau erstellt. 