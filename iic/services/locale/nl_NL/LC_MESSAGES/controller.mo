��          �   %   �      @     A     a     p     �  	   �     �  7   �     �     �               &     4     =     O  )   ]     �  !   �  %   �     �     �      �          !  5  '  !   ]          �     �     �     �  <   �               *     C     W     h     p     �  0   �  !   �  .   �  1     
   A     L  /   T     �     �                                                                         	                      
                               

Did not attend:
------------
 
------------
 
Attended:
------------
 %b %d, %Y %#I:%M %p %Z Anonymous Description:  Dial-in number (provided if system does not call you):  Host:  Meeting ID:  Meeting Password:  None specified Not available Now (%s) Participant PIN:  Participant:  Phone number where system will call you:  This is a data only meeting. This is a voice and data meeting. This is a voice only conference call. Time:  Title:  To join the meeting click here:  data voice Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-04-26 18:10-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 

Niet deelgenomen:
------------
 
------------
 
Deelgenomen:
------------
 %b %d, %Y %#I:%M %p %Z Anoniem Beschrijving:  Inbelnummer (voor het geval dat het systeem u niet opbelt):  Host:  Vergader-id:  Wachtwoord vergadering:  Geen gespecificeerd Niet beschikbaar Nu (%s) Deelnemers-pin:  Deelnemer:  Telefoonnummer waar het systeem u zal opbellen:  Dit is een data-only vergadering. Dit is een vergadering met stemgeluid en data. Dit is een conference call met alleen stemgeluid. Tijdstip:  Titel:  Klik hier om deel te nemen aan de vergadering:  gegevens stem 