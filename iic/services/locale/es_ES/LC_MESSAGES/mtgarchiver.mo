��          �            x     y  �   �  *     ;   J  >   �  -   �  -   �  (   !     J     O     _     l     {     �  ]   �  5  �     ,  v   H  H   �  I     I   R  J   �  6   �  .        M     P     g     y  
   �     �  `   �                                                      
                  	       %a %b %d, %Y at %I:%M:%S %p Although you have turned on recording, there is no voice/data has been recorded.  No movie or mp3 file is produced for this meeting (%s). Click the following link for meeting chat: Click the following link for meeting recording (data-only): Click the following link for meeting recording (voice & data): Click the following link for meeting summary: Click the following link for voice recording: Failed to process your meeting recording From Meeting summary No recording None specified Now (%s) RE: Preview request There is no recording for this meeting (%s), no movie or mp3 file is produced for previewing. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-04-26 18:10-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 %a %b %d, %Y at %I:%M:%S %p Aunque ha activado la grabación, no se han grabado voz/datos.  No hay película ni archivo mp3 de esta reunión (%s). Haga clic en el vínculo siguiente para la conversación de la reunión: Haga clic en el vínculo siguiente para grabar la reunión (sólo datos): Haga clic en el vínculo siguiente para grabar la reunión (voz y datos): Haga clic en el vínculo siguiente para obtener un resumen de la reunión: Haga clic en el vínculo siguiente para grabar la voz: Error al procesar la grabación de la reunión De Resumen de la reunión No hay grabación Sin especificar Ahora (%s) RE: Solicitud de presentación No hay grabación de esta reunión (%s), no se puede presentar ninguna película ni archivo mp3. 