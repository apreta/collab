��          �   %   �      @     A     a     p     �  	   �     �  7   �     �     �               &     4     =     O  )   ]     �  !   �  %   �     �     �      �          !  5  '     ]     z     �     �     �     �  >   �               .     K     [  
   o     z     �  4   �     �     �  +        =  	   D  ,   N     {     �                                                                         	                      
                               

Did not attend:
------------
 
------------
 
Attended:
------------
 %b %d, %Y %#I:%M %p %Z Anonymous Description:  Dial-in number (provided if system does not call you):  Host:  Meeting ID:  Meeting Password:  None specified Not available Now (%s) Participant PIN:  Participant:  Phone number where system will call you:  This is a data only meeting. This is a voice and data meeting. This is a voice only conference call. Time:  Title:  To join the meeting click here:  data voice Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-04-26 18:10-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 

No asistió:
------------
 
------------
 
Asistió:
------------
 %b %d, %Y %#I:%M %p %Z Anónimo Descripción:  Número de marcado (proporcionado si el sistema no le llama):  Anfitrión:  ID de reunión:  Contraseña de la reunión:  Sin especificar No está disponible Ahora (%s) PIN de participante:  Asistente:  Número de teléfono al que el sistema le llamará:  Es una reunión sólo de datos. Es una reunión de voz y datos. Es una llamada de conferencia sólo de voz. Hora:  Título:  Para unirse a la reunión, haga clic aquí:  datos voz 