��          �   %   �      @     A     a     p     �  	   �     �  7   �     �     �               &     4     =     O  )   ]     �  !   �  %   �     �     �      �          !  5  '  #   ]     �     �     �     �     �  /   �               #     7     K     [     d     w  2   �     �     �  #   �            -        D     I                                                                         	                      
                               

Did not attend:
------------
 
------------
 
Attended:
------------
 %b %d, %Y %#I:%M %p %Z Anonymous Description:  Dial-in number (provided if system does not call you):  Host:  Meeting ID:  Meeting Password:  None specified Not available Now (%s) Participant PIN:  Participant:  Phone number where system will call you:  This is a data only meeting. This is a voice and data meeting. This is a voice only conference call. Time:  Title:  To join the meeting click here:  data voice Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-04-26 18:10-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 

Non ha partecipato:
------------
 
------------
 
Ha partecipato:
------------
 %b %d, %Y %#I:%M %p %Z Anonimo Descrizione:  Numero da chiamare (se il sistema non chiama):  Ospite:  ID riunione:  Password riunione:  Nessuno specificato Non disponibile Ora (%s) PIN partecipante:  Partecipante:  Numero telefonico che il sistema dovrà chiamare:  Riunione solo dati. Riunione voce e dati. Chiamata in conferenza solo vocale. Ora:  Titolo:  Fare clic qui per partecipare alla riunione:  dati voce 