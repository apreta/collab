��          �            x     y  �   �  *     ;   J  >   �  -   �  -   �  (   !     J     O     _     l     {     �  ]   �  5  �     ,  �   H  >   �  O     Q   i  E   �  ;     I   =     �     �     �     �  
   �     �  Q   �                                                      
                  	       %a %b %d, %Y at %I:%M:%S %p Although you have turned on recording, there is no voice/data has been recorded.  No movie or mp3 file is produced for this meeting (%s). Click the following link for meeting chat: Click the following link for meeting recording (data-only): Click the following link for meeting recording (voice & data): Click the following link for meeting summary: Click the following link for voice recording: Failed to process your meeting recording From Meeting summary No recording None specified Now (%s) RE: Preview request There is no recording for this meeting (%s), no movie or mp3 file is produced for previewing. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-04-26 18:10-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 %a %b %d, %Y at %I:%M:%S %p Choć zapis został włączony, to nie zostały zapisane żadne dane ani dźwięk. Nie ma dla tego spotkania żadnych plików wideo ani mp3 (%s). Kliknij na poniższe łącze, by przejść do czatu spotkania. Kliknij na poniższe łącze, by przejść do zapisu ze spotkania (tylko dane). Kliknij na poniższe łącze, by przejść do zapisu ze spotkania (audio i dane). Kliknij na poniższe łącze, by przejść do podsumowania spotkania. Kliknij na poniższe łącze, by przejść do zapisu audio. Przetwarzanie zarejestrowanego materiału ze spotkania nie powiodło się Od Podsumowanie spotkania Nie ma zapisanych materiałów Nie określono Teraz (%s) DOT.: Podgląd żądania Nie ma zapisu tego spotkania (%s), nie ma plików wideo ani mp3 do przeglądania. 