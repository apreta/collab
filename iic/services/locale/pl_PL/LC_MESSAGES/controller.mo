��          �   %   �      @     A     a     p     �  	   �     �  7   �     �     �               &     4     =     O  )   ]     �  !   �  %   �     �     �      �          !  5  '  "   ]     �     �     �  	   �     �  1   �                    0     ?  
   L     W     h  #   t      �  ,   �  /   �            3   &     Z     _                                                                         	                      
                               

Did not attend:
------------
 
------------
 
Attended:
------------
 %b %d, %Y %#I:%M %p %Z Anonymous Description:  Dial-in number (provided if system does not call you):  Host:  Meeting ID:  Meeting Password:  None specified Not available Now (%s) Participant PIN:  Participant:  Phone number where system will call you:  This is a data only meeting. This is a voice and data meeting. This is a voice only conference call. Time:  Title:  To join the meeting click here:  data voice Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-04-26 18:10-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 

Nie uczestniczyli:
------------
 
------------
 
Uczestniczyli:
------------
 %b %d, %Y %#I:%M %p %Z Anonimowy Opis:  Numer do wybrania (gdyby system nie zadzwonił):  Host:  ID spotkania:  Hasło spotkania:  Nie określono Niedostępne Teraz (%s) PIN uczestnika:  Uczestnik:  Numer, pod który zadzwoni system:  Jedynie dane dla tego spotkania. To jest spotkanie audio z przesyłem danych. To jest połączenie konferencyjne tylko audio. Czas:  Tytuł:  Kliknij tutaj, by przyłączyć się do spotkania:  dane audio 