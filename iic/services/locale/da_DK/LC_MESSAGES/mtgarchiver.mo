��          �            x     y  �   �  *     ;   J  >   �  -   �  -   �  (   !     J     O     _     l     {     �  ]   �  -  �     $  �   @  ,   �  <   �  B   1  1   t  0   �  &   �     �               #     0     8  d   R                                                      
                  	       %a %b %d, %Y at %I:%M:%S %p Although you have turned on recording, there is no voice/data has been recorded.  No movie or mp3 file is produced for this meeting (%s). Click the following link for meeting chat: Click the following link for meeting recording (data-only): Click the following link for meeting recording (voice & data): Click the following link for meeting summary: Click the following link for voice recording: Failed to process your meeting recording From Meeting summary No recording None specified Now (%s) RE: Preview request There is no recording for this meeting (%s), no movie or mp3 file is produced for previewing. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-04-26 18:10-0400
PO-Revision-Date: 2007-11-29 14:58-0500
Last-Translator: JEP <foo@bar.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 %a %b %d, %Y at %I:%M:%S %p Selv om du har aktiveret optagelse, er der ikke optaget stemme/data. Der er ikke produceret film eller mp3-fil for dette mÃ¸de (%s). Klik pÃ¥ fÃ¸lgende link for mÃ¸dechat: Klik pÃ¥ fÃ¸lgende link for mÃ¸deoptagelse (kun data): Klik pÃ¥ fÃ¸lgende link for mÃ¸deoptagelse (stemme og data): Klik pÃ¥ fÃ¸lgende link for mÃ¸deresumÃ©: Klik pÃ¥ fÃ¸lgende link for stemmeoptagelse: Kan ikke behandle din mÃ¸deoptagelse Fra MÃ¸deresumÃ© Ingen optagelse Ikke angivet Nu (%s) Ang: Anmodning om visning Der er ingen optagelse for dette mÃ¸de (%s) og ikke produceret film eller mp3-fil til fremvisning. 