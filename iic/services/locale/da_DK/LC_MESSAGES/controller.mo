��          �   %   �      @     A     a     p     �  	   �     �  7   �     �     �               &     4     =     O  )   ]     �  !   �  %   �     �     �      �          !  -  '     U     r     �     �     �     �  <   �  	                  -     :     N     V  
   j  5   u  !   �  '   �  ,   �     "     2  $   :     _     d                                                                         	                      
                               

Did not attend:
------------
 
------------
 
Attended:
------------
 %b %d, %Y %#I:%M %p %Z Anonymous Description:  Dial-in number (provided if system does not call you):  Host:  Meeting ID:  Meeting Password:  None specified Not available Now (%s) Participant PIN:  Participant:  Phone number where system will call you:  This is a data only meeting. This is a voice and data meeting. This is a voice only conference call. Time:  Title:  To join the meeting click here:  data voice Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-04-26 18:10-0400
PO-Revision-Date: 2007-11-29 14:57-0500
Last-Translator: JEP <foo@bar.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 

Deltog ikke:
------------
 
------------
 
Deltog:
------------
 %b %d, %Y %#I:%M %p %Z Anonym Beskrivelse:  Opkaldsnummer (angivet, hvis systemet ikke ringer til dig):  VÃ¦rt:  MÃ¸de-id:  MÃ¸deadgangskode:  Ikke angivet Ikke tilgÃ¦ngelig Nu (%s) Deltager-PIN-kode:  Deltager:  Telefonnummer, som systemet vil ringe til dig pÃ¥:  Dette er et mÃ¸de med kun data. Dette er et mÃ¸de med stemme og data. Dette er et konferenceopkald med kun stemme. KlokkeslÃ¦t:  Titel:  Klik her for at deltage i mÃ¸det:  data stemme 