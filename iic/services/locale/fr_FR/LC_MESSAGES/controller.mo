��          �   %   �      @     A     a     p     �  	   �     �  7   �     �     �               &     4     =     O  )   ]     �  !   �  %   �     �     �      �          !  5  '  '   ]     �      �     �     �     �  C   �  	   (     2     B     b     x     �     �     �  =   �  <     1   >  7   p  	   �  	   �  /   �     �     �                                                                         	                      
                               

Did not attend:
------------
 
------------
 
Attended:
------------
 %b %d, %Y %#I:%M %p %Z Anonymous Description:  Dial-in number (provided if system does not call you):  Host:  Meeting ID:  Meeting Password:  None specified Not available Now (%s) Participant PIN:  Participant:  Phone number where system will call you:  This is a data only meeting. This is a voice and data meeting. This is a voice only conference call. Time:  Title:  To join the meeting click here:  data voice Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-04-26 18:10-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 

N'ont pas participé :
------------
 
------------
 
Ont participé :
------------
 %b %d, %Y %#I:%M %p %Z Anonyme Description :  Numéro à composer (fourni si le système ne vous appelle pas) :  Hôte :  ID réunion :  Mot de passe de la réunion :  Pas de spécification Pas disponible Maintenant (%s) Code PIN du participant :  Participant :  Numéro de téléphone auquel le système va vous appeler :  Cette réunion est une réunion de type données uniquement. Cette réunion est une réunion voix et données. Cet appel de conférence est un appel vocal uniquement. Heure :  Titre :  Pour participer à la réunion, cliquez ici :  données voix 