��          �   %   �      @     A     a     p     �  	   �     �  7   �     �     �               &     4     =     O  )   ]     �  !   �  %   �     �     �      �          !  5  '     ]     z     �     �     �  
   �  4   �                 	   0  	   :     D     P     b  +   p     �  !   �     �  
   �  
     %        2     9                                                                         	                      
                               

Did not attend:
------------
 
------------
 
Attended:
------------
 %b %d, %Y %#I:%M %p %Z Anonymous Description:  Dial-in number (provided if system does not call you):  Host:  Meeting ID:  Meeting Password:  None specified Not available Now (%s) Participant PIN:  Participant:  Phone number where system will call you:  This is a data only meeting. This is a voice and data meeting. This is a voice only conference call. Time:  Title:  To join the meeting click here:  data voice Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-04-26 18:10-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 

未出席：
------------
 
------------
 
已出席：
------------
 %b %d, %Y %#I:%M %p %Z 匿名 說明：  撥接號碼 (系統沒有通知給您時提供)：  召集人：  會議 ID：  會議密碼：  無指定 不可用 現在 (%s) 與會者 PIN：  與會者：  系統將會撥打給您的電話號碼：  此會議只傳輸資料。 此會議傳輸語音和資料。 此會議只傳輸語音。 時間：  標題：  要加入會議，按一下這裡：  資料 語音 