��          �            x     y  �   �  *     ;   J  >   �  -   �  -   �  (   !     J     O     _     l     {     �  ]   �  5  �     ,  w   H  0   �  <   �  B   .  0   q  0   �     �     �     �  	     	             &  L   <                                                      
                  	       %a %b %d, %Y at %I:%M:%S %p Although you have turned on recording, there is no voice/data has been recorded.  No movie or mp3 file is produced for this meeting (%s). Click the following link for meeting chat: Click the following link for meeting recording (data-only): Click the following link for meeting recording (voice & data): Click the following link for meeting summary: Click the following link for voice recording: Failed to process your meeting recording From Meeting summary No recording None specified Now (%s) RE: Preview request There is no recording for this meeting (%s), no movie or mp3 file is produced for previewing. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-04-26 18:10-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 %a %b %d, %Y at %I:%M:%S %p 雖然您已經開啟錄音，但是尚未錄製任何語音/資料。此會議沒有產生影片或 mp3 檔案 (%s)。 按一下下面的連結以進行會議聊天： 按一下下面的連結以進行會議錄音 (僅資料)： 按一下下面的連結以進行會議錄音 (語音與資料)： 按一下下面的連結以進行會議摘要： 按一下下面的連結以進行語音錄音： 無法處理您的會議錄音 來自 會議摘要 無錄音 無指定 現在 (%s) 回覆：預覽請求 此會議沒有錄音 (%s)，沒有影片或 mp3 檔案產生可供預覽。 