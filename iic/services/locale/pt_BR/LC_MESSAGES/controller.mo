��          �   %   �      @     A     a     p     �  	   �     �  7   �     �     �               &     4     =     O  )   ]     �  !   �  %   �     �     �      �          !  5  '      ]     ~     �     �     �     �  L   �     #     0     A     U     g  
   z     �     �  ;   �  '   �  +     2   :     m  	   t  )   ~     �     �                                                                         	                      
                               

Did not attend:
------------
 
------------
 
Attended:
------------
 %b %d, %Y %#I:%M %p %Z Anonymous Description:  Dial-in number (provided if system does not call you):  Host:  Meeting ID:  Meeting Password:  None specified Not available Now (%s) Participant PIN:  Participant:  Phone number where system will call you:  This is a data only meeting. This is a voice and data meeting. This is a voice only conference call. Time:  Title:  To join the meeting click here:  data voice Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-04-26 18:10-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 

Não compareceu:
------------
 
------------
 
Compareceu:
------------
 %b %d, %Y %#I:%M %p %Z Anônimo Descrição:  Número de discagem de  acesso (fornecido caso o sistema não chame você):  Hospedeiro:  ID da reunião:  Senha da reunião:  Nada especificado (Não disponível) Agora (%s) PIN de participante:  Participante:  Número de telefone pelo qual o sistema irá chamar você:  Esta é uma reunião apenas para dados. Esta é uma conferência de dados e de voz. Esta é uma chamada de conferência apenas de voz. Hora:  Título:  Para ingressar na reunião, clique aqui:  dados voz 