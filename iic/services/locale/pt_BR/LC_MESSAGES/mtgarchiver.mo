��          �            x     y  �   �  *     ;   J  >   �  -   �  -   �  (   !     J     O     _     l     {     �  ]   �  5  �     ,  �   H  5   �  A     @   R  .   �  -   �  5   �     &     )     <     P  
   b  *   m  w   �                                                      
                  	       %a %b %d, %Y at %I:%M:%S %p Although you have turned on recording, there is no voice/data has been recorded.  No movie or mp3 file is produced for this meeting (%s). Click the following link for meeting chat: Click the following link for meeting recording (data-only): Click the following link for meeting recording (voice & data): Click the following link for meeting summary: Click the following link for voice recording: Failed to process your meeting recording From Meeting summary No recording None specified Now (%s) RE: Preview request There is no recording for this meeting (%s), no movie or mp3 file is produced for previewing. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-04-26 18:10-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 %a %b %d, %Y at %I:%M:%S %p Embora a gravação tenha sido ativada, não há voz ou dados registrados. Nenhum arquivo de vídeo ou mp3 foi produzido para essa reunião (%s). Clique no link a seguir da conversação da reunião: Clique no link a seguir da gravação da reunião (apenas dados): Clique no link a seguir da gravação da reunião (voz e dados): Clique no link a seguir do resumo da reunião: Clique no link a seguir da gravação de voz: Não foi possível processar a gravação da reunião De Resumo da reunião Não há gravação Nada especificado Agora (%s) RE: Requisição de visualização prévia Não há gravação dessa reunião (%s) e nenhum arquivo de vídeo ou mp3 foi produzido para visualização antecipada. 