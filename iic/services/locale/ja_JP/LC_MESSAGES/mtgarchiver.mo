��          �            x     y  �   �  *     ;   J  >   �  -   �  -   �  (   !     J     O     _     l     {     �  ]   �  5  �     ,  �   H  U   
  c   `  l   �  R   1  B   �  9   �  	             (  	   5     ?  "   N  �   q                                                      
                  	       %a %b %d, %Y at %I:%M:%S %p Although you have turned on recording, there is no voice/data has been recorded.  No movie or mp3 file is produced for this meeting (%s). Click the following link for meeting chat: Click the following link for meeting recording (data-only): Click the following link for meeting recording (voice & data): Click the following link for meeting summary: Click the following link for voice recording: Failed to process your meeting recording From Meeting summary No recording None specified Now (%s) RE: Preview request There is no recording for this meeting (%s), no movie or mp3 file is produced for previewing. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-04-26 18:10-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 %a %b %d, %Y at %I:%M:%S %p 記録をオンにしましたが、音声やデータの記録は行われませんでした。このミーティング（%s）に対する動画や mp3 ファイルは生成されません。 ミーティング チャットについては、以下のリンクをクリック： ミーティング記録（データのみ）については、以下のリンクをクリック： ミーティング記録（音声およびデータ）については、以下のリンクをクリック： ミーティング サマリについては、以下のリンクをクリック： 音声記録については、以下のリンクをクリック： ミーティング記録の処理に失敗しました。 発信元 ミーティング サマリ 記録なし 未指定 現在（%s） RE:プレビュー リクエスト このミーティング（%s）に対する記録は存在しません。プレビュー用の動画や mp3 ファイルは生成されません。 