��          �            x     y  �   �  *     ;   J  >   �  -   �  -   �  (   !     J     O     _     l     {     �  ]   �  5  �     ,  �   H  ,   �  ?   �  B   >  5   �  0   �  '   �               +     <     L     T  q   n                                                      
                  	       %a %b %d, %Y at %I:%M:%S %p Although you have turned on recording, there is no voice/data has been recorded.  No movie or mp3 file is produced for this meeting (%s). Click the following link for meeting chat: Click the following link for meeting recording (data-only): Click the following link for meeting recording (voice & data): Click the following link for meeting summary: Click the following link for voice recording: Failed to process your meeting recording From Meeting summary No recording None specified Now (%s) RE: Preview request There is no recording for this meeting (%s), no movie or mp3 file is produced for previewing. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-04-26 18:10-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 %a %b %d, %Y at %I:%M:%S %p Trots att du har aktiverat inspelning har ingen röst/data spelats in. Ingen video- eller mp3-fil har skapats för det här mötet (%s). Klicka på följande länk för möteschatt: Klicka på följande länk för mötesinspelning (enbart data): Klicka på följande länk för mötesinspelning (röst och data): Klicka på följande länk för mötessammanfattning: Klicka på följande länk för röstinspelning: Kunde inte bearbeta mötesinspelningen. Från Mötessammanfattning Ingen inspelning Ej specificerat Nu (%s) Ang.: Förhandsgranskning Det finns ingen inspelning för det här mötet (%s), ingen video- eller mp3-fil skapas för förhandsgranskning. 