��          �   %   �      @     A     a     p     �  	   �     �  7   �     �     �               &     4     =     O  )   ]     �  !   �  %   �     �     �      �          !  5  '     ]     z     �     �     �     �  5   �          
          *     :     L     T     c  .   o  (   �  +   �  3   �     '     -  *   5     `     e                                                                         	                      
                               

Did not attend:
------------
 
------------
 
Attended:
------------
 %b %d, %Y %#I:%M %p %Z Anonymous Description:  Dial-in number (provided if system does not call you):  Host:  Meeting ID:  Meeting Password:  None specified Not available Now (%s) Participant PIN:  Participant:  Phone number where system will call you:  This is a data only meeting. This is a voice and data meeting. This is a voice only conference call. Time:  Title:  To join the meeting click here:  data voice Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-04-26 18:10-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 

Deltog inte:
------------
 
------------
 
Deltog:
------------
 %b %d, %Y %#I:%M %p %Z Anonym Beskrivning:  Uppringningsnummer (ifall systemet inte ringer dig):  Värd:  Mötes-ID:  Mötets lösenord:  Ej specificerat Inte tillgänglig Nu (%s) Deltagar-PIN:  Deltagare:  Telefonnummer som systemet kan ringa dig på:  Det här är ett möte för enbart data. Det här är ett möte för röst och data. Det här är ett konferenssamtal för enbart röst. Tid:  Titel:  Anslut till mötet genom att klicka här:  data röst 