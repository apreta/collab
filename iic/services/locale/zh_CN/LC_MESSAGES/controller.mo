��          �   %   �      @     A     a     p     �  	   �     �  7   �     �     �               &     4     =     O  )   ]     �  !   �  %   �     �     �      �          !  5  '     ]     z     �     �     �  
   �  1   �                  	   -  	   7     A     M     _  (   m     �  !   �     �  
   �  
   �  %        ,     3                                                                         	                      
                               

Did not attend:
------------
 
------------
 
Attended:
------------
 %b %d, %Y %#I:%M %p %Z Anonymous Description:  Dial-in number (provided if system does not call you):  Host:  Meeting ID:  Meeting Password:  None specified Not available Now (%s) Participant PIN:  Participant:  Phone number where system will call you:  This is a data only meeting. This is a voice and data meeting. This is a voice only conference call. Time:  Title:  To join the meeting click here:  data voice Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-04-26 18:10-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 

未出席：
------------
 
------------
 
已出席：
------------
 %b %d, %Y %#I:%M %p %Z 匿名 说明：  拨入号码（系统未呼叫您时提供）：  召集人：  会议 ID：  会议密码：  未指定 不可用 现在 (%s) 与会者 PIN：  与会者：  系统呼叫您所用的电话号码：  此会议仅传输数据。 此会议传输语音和数据。 此会议仅传输语音。 时间：  主题：  要参加会议，请单击此处：  数据 语音 