��          �            x     y  �   �  *     ;   J  >   �  -   �  -   �  (   !     J     O     _     l     {     �  ]   �  5  �     ,  �   H     �  3   �  9   #  *   ]     �     �     �     �     �  	   �     �     �  O                                                         
                  	       %a %b %d, %Y at %I:%M:%S %p Although you have turned on recording, there is no voice/data has been recorded.  No movie or mp3 file is produced for this meeting (%s). Click the following link for meeting chat: Click the following link for meeting recording (data-only): Click the following link for meeting recording (voice & data): Click the following link for meeting summary: Click the following link for voice recording: Failed to process your meeting recording From Meeting summary No recording None specified Now (%s) RE: Preview request There is no recording for this meeting (%s), no movie or mp3 file is produced for previewing. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2007-04-26 18:10-0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
 %a %b %d, %Y at %I:%M:%S %p 您虽已打开录制功能，但未能录制任何语音/数据。因此，本次会议 (%s) 没有生成任何影片或 mp3 文件。 单击以下链接可聊天： 单击以下链接可录制会议（仅数据）： 单击以下链接可录制会议（语音和数据）： 单击以下链接可获取会议摘要： 单击以下链接可录音： 处理会议录制失败 来自 会议摘要 没有录制 未指定 现在 (%s) 回复：预览请求 未录制本次会议 (%s)。因此，没有影片或 mp3 文件可供预览。 