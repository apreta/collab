#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#ifdef LINUX
#include <unistd.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/resource.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <getopt.h>
#else
#include <winsock2.h>
#include <io.h>
#include "getopt.h"
#endif
#include <sys/types.h>
#include <fcntl.h>
#include <signal.h>
#include <errno.h>

#include "logging.h"
#include "rfblib.h"

#include "../util/istring.h"
#include "../util/ithread.h"

static void parse_args(int argc, char **argv);
static int read_password_file(void);
static void report_usage(char *program_name);

extern int			opterr;				/* if error message should be printed */
extern int			optind;				/* index into parent argv vector */
extern int			optopt;				/* character checked for validity */
extern int			optreset;			/* reset getopt */
extern char			*optarg;			/* argument associated with option */

const char * __progname = "iic transmitter";

/*
 * Configuration options
 */

static int   opt_no_banner;
static int   opt_cl_listen_port;
static char *opt_log_filename;
static char *opt_passwd_filename;
static int   opt_foreground;
static int   opt_stderr_loglevel;
static int   opt_file_loglevel;
static char  opt_pid_file[256];
static const char *opt_fbs_prefix;
static int   opt_join_sessions;

static unsigned char opt_client_password[9];
static unsigned char opt_client_ro_password[9];

#ifdef LINUX
#define _close close
#endif

unsigned char opt_challenge[17] = {0};

timeval s_timeout = {600, 0};
timeval r_timeout = {30, 0};

void * worker(void * sock)
{
    int sockfd = (int)(long) sock;
    int len, total_sent=0;
    char buff[1024];

	// RFB Protocol
	strcpy(buff, "RFB 003.003\n");
    if (send(sockfd, buff, 12, 0) == -1) {
        _close(sockfd);
        log_write(LL_ERROR, "worker: send rfb version failed, %s, errno=%d.", strerror(errno), errno);
        return NULL;
    }
    if (recv(sockfd, buff, 12, 0) == -1) {
        _close(sockfd);
        log_write(LL_ERROR, "worker: recv rfb version failed, %s, errno=%d.", strerror(errno), errno);
        return NULL;
    }

	if (opt_client_password[0]) 
	{
		rfb_gen_challenge(opt_challenge);	
		buf_put_CARD32(buff, 2);
		memcpy(&buff[4], opt_challenge, 16);
		if (send(sockfd, buff, 20, 0) == -1) {
			_close(sockfd);
			log_write(LL_ERROR, "worker: send rfb challenge failed, %s, errno=%d.", strerror(errno), errno);
			return NULL;
		}
		
		unsigned char resp_rw[16];
		unsigned char resp_ro[16];

		/* Place correct crypted responses to resp_rw, resp_ro */
		rfb_crypt(resp_rw, opt_challenge, opt_client_password);
		rfb_crypt(resp_ro, opt_challenge, opt_client_ro_password);

		// authentication
		if (recv(sockfd, buff, 16, 0) == -1) {
			_close(sockfd);
			log_write(LL_ERROR, "worker: recv failure, %s, errno=%d.", strerror(errno), errno);
			return NULL;
		}

		if (memcmp(buff, resp_rw, 16) == 0) {
			log_write(LL_MSG, "Full-control authentication accepted");
		} else if (memcmp(buff, resp_ro, 16) == 0) {
			log_write(LL_MSG, "Read-only authentication accepted");
		} else {
			log_write(LL_WARN, "Authentication failed.");
			buf_put_CARD32(buff, 1);
			send(sockfd, buff, 4, 0);
			_close(sockfd);
			return NULL;
		}

		// acknowledge
		buf_put_CARD32(buff, 0);
		send(sockfd, buff, 4, 0);

		// get meeting id
		if (recv(sockfd, (char*)&len, 4, 0) == -1) {
			_close(sockfd);
			log_write(LL_ERROR, "worker: recv failure, %s, errno=%d.", strerror(errno), errno);
			return NULL;
		}
		len = ntohl(len);
		if (recv(sockfd, buff, len, 0) == -1) {
			_close(sockfd);
			log_write(LL_ERROR, "worker: recv failure, %s, errno=%d.", strerror(errno), errno);
			return NULL;
		}
		buff[len]='\0';
    
		IString meetingid = buff;
		time_t start_time;
		int option;

		// get start time
		if (recv(sockfd, (char*)&start_time, 4, 0) == -1) 
		{
			_close(sockfd);
			log_write(LL_ERROR, "worker: recv failure, %s, errno=%d.", strerror(errno), errno);
			return NULL;
		}
		start_time = ntohl(start_time);

		log_write(LL_INFO, "Session ID, %d.\n", start_time);    

		// get option
		if (recv(sockfd, (char*)&option, 4, 0) == -1) 
		{
			_close(sockfd);
			log_write(LL_ERROR, "worker: recv failure, %s, errno=%d.", strerror(errno), errno);
			return NULL;
		}
		option = ntohl(option);
		log_write(LL_INFO, "Option: %d.\n", option);    

		struct tm *tm_start = localtime(&start_time);
		char startstr[21];
		strftime(startstr, 21, "-%Y-%m-%d-%H-%M-%S", tm_start);

		IString fn = opt_fbs_prefix;        
		fn = fn + meetingid;
		fn = fn + startstr;
		fn = fn + ".dump";

		log_write(LL_INFO, "sending recording file, %s.\n", fn.get_string());    

		switch(option)
		{
		case 0:	// send then delete
		case 1:	// send no delete
		{
			FILE * fp = fopen(fn.get_string(), "rb");
			if (fp!=NULL) 
			{
				fseek(fp, 0, SEEK_END);
				int file_size = ftell(fp);
				fseek(fp, 0, SEEK_SET);
				len = htonl(file_size);
				if (send(sockfd, &len, 4, 0) == -1) {
					log_write(LL_ERROR, "worker: send failure, %s, errno=%d.", strerror(errno), errno);
					fclose(fp);
					_close(sockfd);
					return NULL;
				}

				// send recording
				while (!feof(fp)) 
				{
					len = fread(buff, 1, sizeof(buff), fp);
					if (send(sockfd, buff, len, 0) == -1) {
						log_write(LL_ERROR, "worker: send failure, %s, errno=%d.", strerror(errno), errno);
						fclose(fp);
						_close(sockfd);
						return NULL;
					}
					total_sent += len;
				}
				fclose(fp);
				log_write(LL_INFO, "Complete sending recording file, %s.\n", fn.get_string());    
// receiver will send delete cmd afterward
//				if (option==0 && file_size==total_sent)
//				{
//					unlink(fn.get_string());
//					log_write(LL_INFO, "Deleted recording file, %s.\n", fn.get_string());    
//				}
				char empty[8];
				memset(empty, 0, 8);
				if (send(sockfd, empty, 8, 0)!=-1)
				{
					char c;
					while (recv(sockfd, &c, 1, 0) > 0) 
					{
						usleep(100);
					}
				}
				log_write(LL_INFO, "End sending recording file, %s.\n", fn.get_string());    
			}
			else 
			{
				log_write(LL_ERROR, "worker: could not open meeting recording file, %s.", fn.get_string());

				len = 0;
				if (send(sockfd, (char*)&len, 4, 0) == -1) 
				{
					_close(sockfd);
					log_write(LL_ERROR, "worker: send failure, %s, errno=%d.", strerror(errno), errno);
					return NULL;
				}
			}
			break;
		}
		case 2:
			unlink(fn.get_string());
			log_write(LL_INFO, "Deleted recording file, %s.\n", fn.get_string());    
			break;
		}
	}

	_close(sockfd);
    return NULL;
}

void send_recording(short port)
{
	int sockfd, new_fd;  // listen on sock_fd, new connection on new_fd
	struct sockaddr_in my_addr;    // my address information
	struct sockaddr_in client_addr; // connector's address information
	unsigned int sin_size;
	int yes=1;

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
			log_write(LL_ERROR, "send_recording: socket creation failure.");
			return;
	}

	if (setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,(char*)&yes,sizeof(int)) == -1) {
			log_write(LL_ERROR, "send_recording: setsockopt failure.");
			return;
	}
    
	my_addr.sin_family = AF_INET;                // host byte order
	my_addr.sin_port = htons(port);                 // short, network byte order
	my_addr.sin_addr.s_addr = INADDR_ANY; // automatically fill with my IP
	memset(&(my_addr.sin_zero), '\0', 8);       // zero the rest of the struct

	if (bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) == -1) {
			log_write(LL_ERROR, "send_recording: bind failure, %s, errno=%d.", strerror(errno), errno);
			return;
	}

	if (listen(sockfd, SOMAXCONN) == -1) {
			log_write(LL_ERROR, "send_recording: listen failure, %s, errno=%d.", strerror(errno), errno);
			return;
	}

	log_write(LL_INFO, "send_recording: listening @%d. waiting for request...", port);

	while(1) {  // main accept() loop
		sin_size = sizeof(struct sockaddr_in);
		if ((new_fd = accept(sockfd, (struct sockaddr *)&client_addr,
													   &sin_size)) == -1) {
			log_write(LL_ERROR, "send_recording: accept failure, %s, errno=%d.", strerror(errno), errno);
			continue;
		}

		log_write(LL_INFO, "send_recording: accept request from %s.", inet_ntoa(client_addr.sin_addr));

		setsockopt(new_fd, SOL_SOCKET, SO_RCVTIMEO, (char*)&r_timeout, sizeof(r_timeout));
		setsockopt(new_fd, SOL_SOCKET, SO_SNDTIMEO, (char*)&s_timeout, sizeof(s_timeout));

		IThread(worker, (void*)new_fd);
	}

	close(sockfd);
    
}

/*
 * Implementation
 */

int main(int argc, char **argv)
{

#ifdef _WIN32
  WSADATA wsaData;
  WORD wVersionRequested = MAKEWORD(2, 0);

  if (WSAStartup(wVersionRequested, &wsaData) != 0) 
  {
	fprintf(stderr, "Unable to initialize winsock: %d\n", WSAGetLastError());
	exit(-1);
  }
#endif

  IThread::initialize(); // must be initialized first

  setpriority(PRIO_PROCESS, 0, 10);

  /* Parse command line, exit on error */
  parse_args(argc, argv);

  if (!opt_no_banner) {
    fprintf(stderr,
	"IIC Transmitter 1.0.  Copyright (C) 2003 imidio.com\n");
  }

  if (!log_open(opt_log_filename, opt_file_loglevel,
                (opt_foreground) ? opt_stderr_loglevel : -1)) {
      fprintf(stderr, "%s: error opening log file %s (ignoring this error)\n",
              argv[0], opt_log_filename);
  }

  log_write(LL_MSG, "Starting IIC Transmitter 1.0");

#ifdef LINUX
  /* Fork the process to the background if necessary */
  if (!opt_foreground) {
    if (!opt_no_banner) {
      fprintf(stderr, "Starting in the background, "
              "see the log file for errors and other messages.\n");
    }

    if (getpid() != 1) {
      signal(SIGTTIN, SIG_IGN);
      signal(SIGTTOU, SIG_IGN);
      signal(SIGTSTP, SIG_IGN);
      if (fork ())
        return 0;
      setsid();
    }
    close(0);
    close(1);
    close(2);
    log_write(LL_INFO, "Switched to the background mode");
  }
#endif

  /* Initialization */
  read_password_file();
  if (!opt_client_password[0])
  {
    log_write(LL_INFO, "No passwd specified.");
	return -1;
  }
//  set_control_signals();
  /* Main work */

  send_recording(opt_cl_listen_port);

  log_write(LL_MSG, "Terminating");

  /* Close logs */
  if (!log_close() && opt_foreground) {
    fprintf(stderr, "%s: error closing log file (ignoring this error)\n",
            argv[0]);
  }

  /* Done */
  exit(1);
  return 0;
}

static void parse_args(int argc, char **argv)
{
  int err = 0;
  int c;
  char *temp_pid_file = NULL;
  char temp_buf[32];            /* 32 bytes should be more than enough */

  opt_no_banner = 0;
  opt_foreground = 0;
  opt_stderr_loglevel = -1;
  opt_file_loglevel = -1;
  opt_passwd_filename = NULL;
  opt_log_filename = NULL;
  opt_cl_listen_port = -1;
  opt_pid_file[0] = '\0';
  opt_fbs_prefix = NULL;

  while (!err &&
         (c = getopt(argc, argv, "hqjv:f:p:a:c:g:l:i:s:b:tT:z:n:m:x:r:o:")) != -1) {
    switch (c) {
    case 'h':
      err = 1;
      break;
    case 'q':
      opt_no_banner = 1;
      break;
    case 'j':
      opt_join_sessions = 1;
      break;
    case 'v':
      if (opt_file_loglevel != -1)
        err = 1;
      else
        opt_file_loglevel = atoi(optarg);
      break;
    case 'f':
      opt_foreground = 1;
      if (opt_stderr_loglevel != -1)
        err = 1;
      else
        opt_stderr_loglevel = atoi(optarg);
      break;
    case 'p':
      if (opt_passwd_filename != NULL)
        err = 1;
      else
        opt_passwd_filename = optarg;
      break;
    case 'g':
      if (opt_log_filename != NULL)
        err = 1;
      else
        opt_log_filename = optarg;
      break;
    case 'l':
      if (opt_cl_listen_port != -1)
        err = 1;
      else {
        opt_cl_listen_port = atoi(optarg);
        if (opt_cl_listen_port <= 0)
          err = 1;
      }
      break;
    case 'i':
      if (temp_pid_file != NULL)
        err = 1;
      else
        temp_pid_file = optarg;
      break;
    case 's':
      if (opt_fbs_prefix != NULL)
        err = 1;
      else
        opt_fbs_prefix = optarg;
      break;
    case 'r':
        log_set_rotation_size((size_t)atoi(optarg));
        break;
    case 'o':
        log_set_history_len((size_t)atoi(optarg));
        break;

    default:
      err = 1;
    }
  }

  /* Print usage help on error */
  if (err || optind != argc) {
    report_usage(argv[0]);
    exit(1);
  }

  /* Provide reasonable defaults for some options */
  if (opt_file_loglevel == -1)
    opt_file_loglevel = LL_INFO;
  if (opt_passwd_filename == NULL)
    opt_passwd_filename = "/opt/iic/conf/passwd";
  if (opt_log_filename == NULL)
    opt_log_filename = "transmitter.log";
  if (opt_cl_listen_port == -1)
    opt_cl_listen_port = 11002;

  /* Append listening port number to pid filename */
  if (temp_pid_file != NULL) {
      sprintf(temp_buf, "%d", opt_cl_listen_port);
      sprintf(opt_pid_file, "%.*s.%s", (int)(255 - strlen(temp_buf) - 1),
              temp_pid_file, temp_buf);
  }
}

static void report_usage(char *program_name)
{
  fprintf(stderr,
          "IIC Transmitter.  Copyright (C) 2003 imidio.com"
          "\n\n");

  fprintf(stderr, "Usage: %s [OPTIONS...]\n\n",
          program_name);

  fprintf(stderr,
          "Options:\n"
          "  -i PID_FILE     - write pid file, appending listening port"
          " to the filename\n"
          "  -p PASSWD_FILE  - read a plaintext client password file"
          " [default: passwd]\n"
          "  -l LISTEN_PORT  - port to listen for client connections"
          " [default: 5999]\n");
  fprintf(stderr,
          "  -s FBS_PREFIX   - save host sessions in rfbproxy-compatible"
          " files\n"
          "                    (optionally appending 3-digit session IDs"
          " to the\n"
          "                    filename prefix, only if used without the"
          " -j option)\n"
          "  -j              - join saved sessions (see -s option) in one"
          " session file\n"
          "  -t              - use Tight encoding for host communications"
          " if possible\n"
          "  -T COMPR_LEVEL  - like -t, but use the specified compression"
          " level (1..9)\n");
  fprintf(stderr,
          "  -g LOG_FILE     - write logs to the specified file"
          " [default: reflector.log]\n"
          "  -r size         - rotate logs when LOG_FILE reaches this size (in bytes)"
          " [default: 5000000]\n"
          "  -o length       - number of historical logs to keep when rotating logs"
          " [default: 5]\n"
          "  -v LOG_LEVEL    - set verbosity level for the log file (0..%d)"
          " [default: %d]\n"
          "  -f LOG_LEVEL    - run in foreground, show logs on stderr"
          " at the specified\n"
          "                    verbosity level (0..%d) [note: use %d for"
          " normal output]\n"
          "  -q              - suppress printing copyright banner at startup\n"
          "  -h              - print this help message\n\n",
          LL_DEBUG, LL_INFO, LL_DEBUG, LL_MSG);

  fprintf(stderr,
          "Please refer to the README file for a description of the file"
          " formats for\n"
          "  HOST_INFO_FILE and PASSWD_FILE files mentioned above in this"
          " help text.\n\n");
}

static int read_password_file(void)
{
  FILE *passwd_fp;
  unsigned char *password_ptr = opt_client_password;
  int line = 0, len = 0;
  int c;

  /* Fill passwords with zeros */
  memset(opt_client_password, 0, 9);
  memset(opt_client_ro_password, 0, 9);

  log_write(LL_DETAIL, "Looking for passwords in the file \"%s\"",
            opt_passwd_filename);

  passwd_fp = fopen(opt_passwd_filename, "r");
  if (passwd_fp == NULL) {
      passwd_fp = fopen("passwd", "r");
      log_write(LL_WARN,
                "No client password file, assuming no authentication");
      return 1;
  }

  /* Read password file */
  while (line < 2) {
    c = getc(passwd_fp);
    if (c == '\r')
      c = getc(passwd_fp);      /* Handle MS-DOS-style end of line */
    if (c != '\n' && c != EOF && len < 8) {
      password_ptr[len++] = c;
    } else {
      password_ptr[len] = '\0';
      /* Truncate long passwords */
      if (len == 8 && c != '\n' && c != EOF) {
        log_write(LL_WARN, "Using only 8 first bytes of a longer password");
        do {
          c = getc(passwd_fp);
        } while (c != '\n' && c != EOF);
      }
      /* End of file */
      if (c == EOF)
        break;
      /* Empty password means no authentication */
      if (len == 0) {
        log_write(LL_WARN, "Got empty client password, hoping no auth is ok");
      }
      /* End of line */
      if (++line == 1) {
        password_ptr = opt_client_ro_password;
      }
      len = 0;
    }
  }
  if (len == 0) {
    if (line == 0) {
      log_write(LL_WARN, "Client password not specified, assuming no auth");
    } else {
      line--;
    }
  }

  log_write(LL_DEBUG, "Got %d password(s) from file, including empty ones",
            line + 1);

  /* Provide reasonable defaults if not all two passwords set */
  if (line == 0) {
    log_write(LL_DETAIL, "Read-only client password not specified");
    strcpy((char *)opt_client_ro_password, (char *)opt_client_password);
  }

  fclose(passwd_fp);
  return 1;
}

static int write_pid_file(void)
{
#ifdef LINUX
  int pid_fd, len;
  char buf[32];                 /* 32 bytes should be more than enough */

  if (opt_pid_file[0] == '\0')
    return 1;

  pid_fd = open(opt_pid_file, O_WRONLY | O_CREAT | O_EXCL, 0644);
  if (pid_fd == -1) {
    log_write(LL_ERROR, "Pid file exists, another instance may be running");
    return 0;
  }
  sprintf(buf, "%d\n", (int)getpid());
  len = strlen(buf);
  if (write(pid_fd, buf, len) != len) {
    close(pid_fd);
    log_write(LL_ERROR, "Error writing to pid file");
    return 0;
  }

  log_write(LL_DEBUG, "Wrote pid file: %s", opt_pid_file);

  close(pid_fd);
#endif
  return 1;
}

static int remove_pid_file(void)
{
#ifdef LINUX
  if (opt_pid_file[0] != '\0') {
    if (unlink(opt_pid_file) == 0) {
      log_write(LL_DEBUG, "Removed pid file", opt_pid_file);
    } else {
      log_write(LL_WARN, "Error removing pid file: %s", opt_pid_file);
      return 0;
    }
  }
#endif
  return 1;
}

