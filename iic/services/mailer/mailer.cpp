/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef WIN32
#pragma warning(disable:4786)
#endif

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <locale.h>
#include <string.h>

#include <libintl.h>
#define _(String) gettext(String)

#include "mailer.h"
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>
#include "../../jcomponent/util/util.h"
#include "../../jcomponent/jcomponent_ext.h"
#include "../../jcomponent/rpc.h"
#include "../common/mailer.h"
#include "../common/common.h"
#include "../common/profile.h"
#include "../../jcomponent/util/port.h"
#include "../../jcomponent/util/log.h"
#include "../../jcomponent/util/nadutils.h"
#include "../../util/istring.h"

#include "../common/invites.inl"

#define MAXSUBJECTSIZE 255
#define MAXEMAILSIZE 256
#define JCOMPNAME "mailer"

int set_config(config_t conf);

IString g_server_name;
IString g_smtp_uri;
IString g_smtp_user;
IString g_smtp_pass;
IString g_smtp_from;
IString g_smtp_from_display;
IString g_location;
int g_use_reply_to = 0;

// cache invite email template file
struct invite_template_info
{
    IString file_name;            // name of the file (read from mailer.xml)
    time_t  last_modified;        // time of last mod
    char*   buf;                  // storage for file
    IString from_address;         // 'from' address for email msg
    IString from_display;         // 'display from' address for email msg
    IString from_display_address; // address and display, combined in proper fashion
    IString subject;              // 'subject' field for email message
};

invite_template_info g_invite_template_info;

extern "C"
{
#include <ical.h>
#include <uuid/uuid.h>
}

int mailer_init(config_t conf)
{
	log_status(ZONE, "Mailer service starting");

    set_zone_directory("/opt/iic/share/libical/zoneinfo");
    
	// Read configuration
	int status = set_config(conf);
	if (status)
		return status;

    const char *zonlang = getenv("ZONLANG");
    if (zonlang == NULL)
        zonlang = "";	// use system default

    log_debug(ZONE, "Setting locale to %s", zonlang);

    // initialize locale and timezone for display purposes (email generation)
    setlocale( LC_ALL, zonlang );
    tzset();
    
	bindtextdomain(JCOMPNAME, "/opt/iic/locale");
    textdomain(JCOMPNAME);
    char    *pBTC = bind_textdomain_codeset(JCOMPNAME, "UTF-8");
//    log_debug(ZONE, "bind_textdomain_codeset( ) returned %s", pBTC);

	return 0;
}

int set_config(config_t conf)
{
    g_server_name = config_get_one(conf, JCOMPNAME, "iicServer", 0);
    g_smtp_uri = config_get_one(conf, JCOMPNAME, "smtpServerURI", 0);
    g_smtp_user = config_get_one(conf, JCOMPNAME, "smtpServerUser", 0);
    g_smtp_pass = config_get_one(conf, JCOMPNAME, "smtpServerPassword", 0);
    g_smtp_from = config_get_one(conf, JCOMPNAME, "smtpFromAddress", 0);
    g_smtp_from_display = config_get_one(conf, JCOMPNAME, "smtpFromDisplayAddress", 0);
    g_invite_template_info.file_name = config_get_one(conf, JCOMPNAME, "inviteTemplateFile", 0);
    g_invite_template_info.from_address = config_get_one(conf, JCOMPNAME, "inviteFromAddress", 0);
    g_invite_template_info.from_display = config_get_one(conf, JCOMPNAME, "inviteFromDisplay", 0);
    g_invite_template_info.subject = config_get_one(conf, JCOMPNAME, "inviteSubject", 0);
    g_location = config_get_one(conf, JCOMPNAME, "icalLocation", 0);
    if (g_location.length() == 0)
        g_location = "Zon";
	const char *useReplyTo = config_get_one(conf, JCOMPNAME, "useReplyTo", 0);
	if (useReplyTo && strcmp(useReplyTo, "y") == 0)
		g_use_reply_to = 1;

    // Generate "from" address::
    //           user@domain  or
    //           Display Name <user@domain>
    if (g_invite_template_info.from_display.length() > 0)
    {
        g_invite_template_info.from_display_address = g_invite_template_info.from_display;
        g_invite_template_info.from_display_address.append(" <");
        g_invite_template_info.from_display_address.append(g_invite_template_info.from_address);
        g_invite_template_info.from_display_address.append(">");
    }
    else
    {
        g_invite_template_info.from_display_address = g_invite_template_info.from_address;
    }

    return Ok;
}

const char* get_smtp_uri()
{
    return g_smtp_uri;
}

const char* get_smtp_user()
{
    return g_smtp_user;
}

const char* get_smtp_pass()
{
    return g_smtp_pass;
}

const char* get_smtp_from()
{
    return g_smtp_from;
}

const char* get_smtp_from_display()
{
    return g_smtp_from_display;
}

// Send a message
// Parameters:
//   TO, FROM, FROM_DISPLAY, SUBJECT, BODY.  If FROM is blank, default from is used (and
//   default display name.  If FROM is not blank, FROM_DISPLAY is used if provided.
// Returns:
//   SUCCESS_CODE: 0 = ok, otherwise failure code
xmlrpc_value *send_message(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *to, *from, *from_display, *subject, *body;
    char* res_s;
    IString ifrom, ifrom_display;
    bool use_display;
    std::string response;
    jwsmtp::mailer* mail = NULL;
        
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(sssss)", 
                       &to,
                       &from,
                       &from_display,
                       &subject,
                       &body
                       );
    XMLRPC_FAIL_IF_FAULT(env);

    // valid addresses:
    //           user@domain  or
    //           Display Name <user@domain>

    // if no from display address, use default from display address
    ifrom_display = (strlen(from_display) == 0) ? (char*) get_smtp_from_display() : from_display;

    // format if display name provided
    use_display = (ifrom_display.length() > 0);
    if (use_display)
    {
        ifrom.append(ifrom_display.get_string());
        ifrom.append(" <");
    }

    // if no from address, just use default from
    ifrom.append( (strlen(from) == 0) ? get_smtp_from() : from);

    if (use_display)
        ifrom.append(">");

    if (strlen(to) == 0)
    {
        log_error(ZONE, "Empty To: field for e-mail from %s about %s",
                  to, subject);
    }
        
    // send message
    // mail(to, from, subject, message, smtp address, port, MXLookup
    mail = new jwsmtp::mailer(to,
                        ifrom.get_string(),
                        subject,
                        body,
                        get_smtp_uri(), 
                        jwsmtp::mailer::SMTP_PORT, 
                        false);
    
    mail->addauthentication(get_smtp_user(), get_smtp_pass());
    mail->operator()();

    // check results
    response = mail->response();
    res_s = (char*) response.c_str();

    // log result
    log_debug(ZONE, "Email sent to: %s response: %s", to, res_s);

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Send message failure");

    if (mail != NULL)
        delete mail;

    return output;
}

//
// internal_get_invite_template_info
//
// checks the new user template file to make sure cache is up to date.
// stores in memory if changed.
// returns false if error (can't find file, can't open).
//
bool internal_get_invite_template_info()
{
    struct stat buf;
    int stat_result;
    int fread_size;

    stat_result = stat(g_invite_template_info.file_name, &buf);

    if (stat_result == -1) // error
    {
        log_error(ZONE, "get_invite_template: couldn't stat %s : %s\n", (const char *)g_invite_template_info.file_name, strerror(errno));
        return false;
    }

    if (difftime(buf.st_mtime, g_invite_template_info.last_modified) != 0.0)
    {
        long int fsize;

        // free previous memory for file storage
        if (g_invite_template_info.buf != NULL)
        {
            delete[] g_invite_template_info.buf;
            g_invite_template_info.buf = NULL;
        }

        // allocate memory for file
        fsize = buf.st_size;
        g_invite_template_info.buf = new char[fsize+1];
        memset(g_invite_template_info.buf, 0, fsize+1);

        // open file for reading
        FILE *f;
        f = fopen(g_invite_template_info.file_name, "r");
        if (f == NULL)
        {
            log_error(ZONE, "get_invite_template: couldn't open %s for reading: %s\n", (const char *)g_invite_template_info.file_name, strerror(errno));
            return false;
        }

        // convert to char array
        fread_size = fread(g_invite_template_info.buf, sizeof(char), fsize, f);
        if (fread_size > fsize)
        {
            log_error(ZONE, "get_invite_template: error reading file %s\n", (const char *)g_invite_template_info.file_name);
            return false;
        }

        // terminate with null char
        g_invite_template_info.buf[fread_size] = 0;

        fclose(f);

        g_invite_template_info.last_modified = buf.st_mtime;
    }

    return true;
}

//
// API mailer.get_invite_template - Get the invite email template
//
// Parameters:
//   none
// Returns:
//   SUCCESS_CODE: 0 = ok, otherwise failure code
//   TEMPLATE: If success, the invite template, otherwise an empty string
//
xmlrpc_value *get_invite_template(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    xmlrpc_value *output = NULL;
    int status = Ok;

    if (internal_get_invite_template_info())
    {
        output = xmlrpc_build_value(env, "(is)", status, g_invite_template_info.buf);
        XMLRPC_FAIL_IF_FAULT(env);
    }
    else
    {
        output = xmlrpc_build_value(env, "(is)", status, "");
        XMLRPC_FAIL_IF_FAULT(env);
    }

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Get invite template failure");

    return output;
}

int mk_invite_ical_attachment(char * fn, int start, int end, const char *title, const char *body, const char *email_to, const char *email_from)
{
    uuid_t id;
    uuid_generate(id);
    char id_str[38];
    uuid_unparse(id, id_str);
    IString email_to_str = IString("mailto:") + email_to;
    IString email_from_str = IString("mailto:") + email_from;

    strcpy(fn, "/tmp/");
    strcat(fn, id_str);
    if (mkdir(fn, 0700) < 0)
    {
        log_error(ZONE, "Unable to create ical attachment temp dir %s: %s",
                  fn, strerror(errno));
        return -1;
    }

    strcat(fn, "/MakeCalendarAppointment.ics");

    log_debug(ZONE, "Saving appointment %s", fn);
    
    struct icalperiodtype mtime;

    mtime.start = icaltime_from_timet_with_zone( (time_t)start, 0, icaltimezone_get_utc_timezone() );
    mtime.end = icaltime_from_timet_with_zone( (time_t)end, 0, icaltimezone_get_utc_timezone() );

    struct icaltimetype now = icaltime_current_time_with_zone( icaltimezone_get_utc_timezone() );
    
    icalcomponent * mtgcal =
        icalcomponent_vanew(
            ICAL_VCALENDAR_COMPONENT,
            icalproperty_new_version("2.0"),
            icalproperty_new_prodid("-//SiteScape//NONSGML Zon//EN"),
            icalproperty_new_method(ICAL_METHOD_REQUEST),
            icalcomponent_vanew(
                ICAL_VEVENT_COMPONENT,
                icalproperty_new_uid(id_str),
                icalproperty_vanew_organizer(
                    email_from_str.get_string(),
                    icalparameter_new_role(ICAL_ROLE_CHAIR),
                    NULL),
                icalproperty_vanew_attendee(
                    email_to_str.get_string(),
                    icalparameter_new_role(ICAL_ROLE_REQPARTICIPANT),
                    icalparameter_new_rsvp(ICAL_RSVP_TRUE),
                    icalparameter_new_cutype(ICAL_CUTYPE_GROUP),
                    NULL),
                icalproperty_new_summary(title),
                icalproperty_new_description(body),
                icalproperty_new_location(g_location),
                icalproperty_new_status(ICAL_STATUS_TENTATIVE),
                icalproperty_new_class(ICAL_CLASS_PUBLIC),
                icalproperty_new_dtstart(mtime.start),
                icalproperty_new_dtend(mtime.end),
                icalproperty_new_dtstamp(now),
                icalcomponent_vanew(
                    ICAL_VALARM_COMPONENT,
                    icalproperty_new_action(ICAL_ACTION_DISPLAY),
                    icalproperty_new_description("Reminder"),
                    icalproperty_new_trigger(icaltriggertype_from_int(15*60)),
                    NULL),
                NULL),
            NULL);

    FILE * fp = fopen(fn, "w");
    if (fp)
    {
        fprintf(fp, "%s", icalcomponent_as_ical_string(mtgcal));
        fclose(fp);
    }
    else
    {
        log_error(ZONE, "Unable to write %s: %s",
                  fn, strerror(errno));

        return -1;
    }

    icalcomponent_free(mtgcal);  // works recursively

    return 0;
}

void fix_newlines(IString& str)
{
	int pos = 0;
	while (pos != -1) 
	{
		pos = str.find("\n", pos);
		if (pos != -1)
		{
			if (pos == 0 || str.get_string()[pos-1] != '\r')
			{
				str.insert(pos, "\r");
				pos += 2;
			}
		}
	}
	
}

//
// API mailer.send_invite_email - Send a meeting invitation
//
// Parameters:
//  EMAIL
//  MEETINGID, 
//  MEETINGTITLE,
//  HOSTNAME,
//  PARTICIPANTID,
//  PARTICIPANTNAME,
//  MEETINGDESCRIPTION,
//  INVITEMESSAGE,
//  MEETINGPASSORD,
//  PARTICIPANTPHONE,
//  BRIDGEPHONE,
//  SYSTEMURL,
//  MEETINGOPTIONS
//  MEETINGTYPE
//  PRIVACY
//  SCHEDULEDTIME,
//  SCHEDULEDENDTIME,
//  SCHEDULING,
//  SENTBY
//
// Returns:
//   SUCCESS_CODE: 0 = ok, otherwise failure code
//   TEMPLATE: If success, the invite template, otherwise an empty string
//
// Notes:
//  The SCHEDULING parameter controls how a SCHEDULEDTIME of 0 will be displayed.
//  If SCHEDULING is 1, a SCHEDULEDTIME of 0 will be displayed as Not Specified.
//  If SCHEDULING is 0, it will displayed as Now (current time in paranthesis).
// 
xmlrpc_value *send_invite_email(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *email, *meetingid, *meetingtitle, *hostname, *participantid, 
        *participantname, *meetingdescription, *invitemessage, *meetingpassord, 
        *participantphone, *bridgephone, *systemurl, *sentby;
    int meetingoptions, type, privacy, scheduledtime, scheduledendtime, scheduling;
    char email_subject[255];
    char email_sentby[MAXEMAILSIZE];
    char email_replyto[MAXEMAILSIZE];
    IString strEmailInvite;
    char* res_s;
    std::string response;
    jwsmtp::mailer* mail = NULL;
    bool use_system_email = true;
    int status = Ok;
    xmlrpc_value *output = NULL;

    xmlrpc_parse_value(env, param_array, "(ssssssssssssiiiiiis)", 
        &email, &meetingid, &meetingtitle, &hostname, &participantid, 
        &participantname, &meetingdescription, &invitemessage, 
        &meetingpassord, &participantphone, &bridgephone, &systemurl, 
        &meetingoptions, &type, &privacy, &scheduledtime, &scheduledendtime, &scheduling, &sentby);
    XMLRPC_FAIL_IF_FAULT(env);

    if (internal_get_invite_template_info())
    {
		IString strDescrip = meetingdescription;
		IString strInviteMsg = invitemessage;
		
		// User supplied text may have newlines, needs to be \r\n to be SMTP compliant
		fix_newlines(strDescrip);
		fix_newlines(strInviteMsg);
		
        // Construct the email invitation
        internal_get_invite_email(strEmailInvite, g_invite_template_info.buf,
                                  meetingid, meetingtitle, hostname, participantid, participantname, 
                                  strDescrip.get_string(), strInviteMsg.get_string(), meetingpassord, participantphone,
                                  bridgephone, systemurl, g_server_name, meetingoptions, type, privacy,
                                  scheduledtime, false, (scheduling == 1), gettext("%b %d, %Y %#I:%M %p %Z"), false);

        // Append the meeting title to the system's subject message
        snprintf(email_subject, MAXSUBJECTSIZE, "%s: %s",
                 g_invite_template_info.subject.get_string(), meetingtitle);

        // Use the passed sentby email address if not empty, otherwise
        // use the system's invite email address.
        if (strlen(sentby) != 0)
        {
            // Validation that email address has at least an '@' and a '.'
            char *char_at = strchr(sentby, '@');  // look for '@' starting at beginning of string
            char *char_period = strrchr(sentby, '.');  // look for '.' starting from end of string

            if (char_at != NULL && char_period != NULL)
            {
                int position1 = char_at - sentby + 1;
                int position2 = char_period - sentby + 1; 

                if (position1 < position2)
                {
                    use_system_email = false;
                    snprintf(email_sentby, MAXEMAILSIZE, "%s", sentby);
					strcpy(email_replyto, email_sentby);
                }
            }
        }

        if (use_system_email)
        {
            snprintf(email_sentby, MAXEMAILSIZE, "%s", g_invite_template_info.from_display_address.get_string());
			strcpy(email_replyto, email_sentby);
        }
    
		if (g_use_reply_to)
		{
            snprintf(email_sentby, MAXEMAILSIZE, "%s", g_invite_template_info.from_display_address.get_string());
		}

        log_debug(ZONE, "Sent by: %s", sentby);

        if (strlen(email) == 0)
        {
            log_error(ZONE, "Empty To: field for e-mail from %s about %s",
                      email_sentby, email_subject);
        }
        
        // send message
        // mail(to, from, subject, message, smtp address, port, MXLookup
        mail = new jwsmtp::mailer(email,
                            email_sentby,
                            email_subject,
                            strEmailInvite.get_string(),
                            get_smtp_uri(), 
                            jwsmtp::mailer::SMTP_PORT, 
                            false);

		mail->setreplyto(email_replyto);    
        mail->addauthentication(get_smtp_user(), get_smtp_pass());
        char tmp_fn[128];
        if (scheduledtime > 0 && scheduledendtime > 0)
        {
            IString strIcalInvite;
            // Need to regenerate with '\n' instead of '\n\r'
            internal_get_invite_email(strIcalInvite, g_invite_template_info.buf,
                                  meetingid, meetingtitle, hostname, participantid, participantname, 
                                  meetingdescription, invitemessage, meetingpassord, participantphone,
                                  bridgephone, systemurl, g_server_name, meetingoptions, type, privacy,
                                  scheduledtime, false, (scheduling == 1), gettext("%b %d, %Y %#I:%M %p %Z"), true);

            strcpy(tmp_fn, "/tmp/ical_XXXXXX");
            mk_invite_ical_attachment(tmp_fn,
                                      scheduledtime, scheduledendtime,
                                      email_subject /* meetingtitle */, strIcalInvite.get_string(), email, email_replyto);
            mail->attach(tmp_fn);
        }

        mail->operator()();

        // check results
        response = mail->response();
        res_s = (char*) response.c_str();

        if (scheduledtime > 0 && scheduledendtime > 0)
        {
            if (unlink(tmp_fn) < 0)
            {
                log_error(ZONE, "Unable to remove %s: %s",
                          tmp_fn, strerror(errno));
            }
            char * sl = strrchr(tmp_fn, '/');
            if (sl) *sl = '\0';
            if (rmdir(tmp_fn) < 0)
            {
                log_error(ZONE, "Unable to remove dir %s: %s",
                          tmp_fn, strerror(errno));
            }
        }
        
        // log result
        log_debug(ZONE, "Email sent to: %s response: %s", email, res_s);
    }
    else
    {
        status = Failed;
        log_error(ZONE, "Invitation email not sent: unable to read invite template");
    }

    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Get invite template failure");

    return output;
}

/* Initialization as jabber component--call mailer_init() if using library
   directly. */
int main()
{
    config_t conf = config_new();
    config_load(conf, JCOMPNAME, 0, "/opt/iic/conf/config.xml");

	if (jc_init(conf, JCOMPNAME))
	{
		log_error(ZONE, "Unable to initialize controller\n");
		return -1;
	}

    if (mailer_init(conf))
        return -1;

    // Add methods to RPC server
    rpc_add_method("mailer.send_message", &send_message, NULL);
    rpc_add_method("mailer.get_invite_template", &get_invite_template, NULL);
    rpc_add_method("mailer.send_invite_email", &send_invite_email, NULL);
    
    jc_run(false);
        
    return 0;
}


