/* (c)2012 apreta.org */

#include <magick/api.h>

#include "../../jcomponent/util/util.h"
#include "../../jcomponent/util/log.h"
#include "../../util/istring.h"

int initializeImages(char * path)
{
   	InitializeMagick(path);
   	return 0;
}

Image * getImageInfo(const char *name)
{
	Image *image = NULL;
	ImageInfo *image_info;

	ExceptionInfo exception;
    GetExceptionInfo(&exception);

	image_info = CloneImageInfo((ImageInfo *) NULL);

    strcpy(image_info->filename, name);

    image = PingImage(image_info, &exception);
	if (exception.severity != UndefinedException) {
		CatchException(&exception);
		image = NULL;
	}

	DestroyImageInfo(image_info);
	return image;
}

int writeIndexFile(int numPages, IString output, IString type)
{
	IString pageName = "Page";

	if (type == "presentation")
		pageName = "Slide";
	else if (type == "spreadsheet")
		pageName = "Sheet";

	//  Create the index file
	IString indexText;
	indexText.append("<index>\n");

	int i;
	for(i=0; i< numPages; i++)
	{
		char bufPNG[ 100 ];
		sprintf(bufPNG, "Page-%03d.png", i+1);

		bool fitToWidth = true;
		Image * img = getImageInfo(output + bufPNG);
		if (img != NULL)
		{
			fitToWidth = img->magick_rows > img->magick_columns;
			DestroyImage(img);
		}

		char bufT[ 300 ];
		sprintf(bufT, "<page title=\"%s %d\" file=\"%s\" altfile=\"%s\" scale=\"1.0\" altscale=\"1.0\" fittowidth=\"%s\"/>\n",
				pageName.get_string(), i+1, bufPNG, bufPNG, fitToWidth ? "T" : "F");
		indexText.append(bufT);
	}

	indexText.append("</index>\n");

	IString indexFile = output + "index.xml";

	fprintf(stderr, "Writing index file: %s\n", indexFile.get_string());

	FILE    *f;
	f = fopen(indexFile.get_string(), "w");
	if (f == NULL) {
		fprintf(stderr, "Failed to open index file\n");
		return 1;
	}
	fputs(indexText.get_string(), f);
	fclose(f);

	return 0;
}

/**
 * Use GraphicMagick to convert PDF to PNG images.
 */
int convertImage(IString input, IString output, void(*on_page)(void * session, const char *, int page), void * session)
{
	Image *image;
	ImageInfo *image_info;
	int i;

	ExceptionInfo exception;
    GetExceptionInfo(&exception);

	image_info = CloneImageInfo((ImageInfo *) NULL);
	fprintf(stderr, "Reading PDF: %s\n", input.get_string());

    strcpy(image_info->filename, input.get_string());

	// Figure out number of pages.
	image = PingImage(image_info, &exception);
	if (exception.severity != UndefinedException) {
		CatchException(&exception);
		return -1;
	}
	int numPages = GetImageListLength(image);
	DestroyImage(image);
	fprintf(stderr, "PDF contains %d pages\n", numPages);

	image_info->density = strdup("150x150");
	image_info->units = PixelsPerInchResolution;
	image_info->adjoin = 0;

	// Convert a page at a time to keep memory usage under control.
	for (i = 0; i < numPages; i++) {
		char pageNum[16];
		sprintf(pageNum, "[%d]", i);

		strcpy(image_info->filename, input.get_string());
		strcat(image_info->filename, pageNum);
		image = ReadImage(image_info, &exception);
		if (exception.severity != UndefinedException) {
			CatchException(&exception);
			return -1;
		}

		char outName[32];
		sprintf(outName, "Page-%03d.png", i+1);
		IString outPage = output + outName;

		WriteImages(image_info, image, outPage.get_string(), &exception);
		if (exception.severity != UndefinedException) {
			CatchException(&exception);
			return -1;
		}

		fprintf(stderr, "Output page %d\n", i);
		DestroyImage(image);

		on_page(session, "PageOutput", i);
	}

	DestroyImageInfo(image_info);
	DestroyExceptionInfo(&exception);

	return numPages;
}
