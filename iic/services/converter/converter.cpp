/* (c)2012 apreta.org */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <locale.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <dirent.h>
#include <gmodule.h>
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>
#include <locale.h>
#include <libintl.h>
#include <json/json.h>
#define _(String) gettext(String)
#include "../../jcomponent/util/util.h"
#include "../../jcomponent/jcomponent_ext.h"
#include "../../jcomponent/rpc.h"
#include "../common/common.h"
#include "../../jcomponent/util/port.h"
#include "../../jcomponent/util/log.h"
#include "../../jcomponent/util/nadutils.h"
#include "../../jcomponent/util/eventlog.h"
#include "../../util/ithread.h"
#include "../../util/filetransfer/transfer.h"
#include "../librepository/repository.h"

#define JCOMPNAME	"converter"

EventLog event_log;

CRepository sRepository;
IThreadPool* g_tpool = NULL;
IMutex* g_loader_lock = NULL;

IString g_server_name;
IString g_host_portal;
IString g_spool_path;
IString g_recording_base_url;
IString g_docshare_base_url;
IString g_converter_cmd;
IString g_repo_username;
IString g_repo_password;
IString g_admin_emails;
int g_converter_uid;
int g_batch_threads;
int g_task_id = 0;

int g_counter = 0;

int g_loader_start = 0;
int g_warning_time = 120;   // 120 seconds

void process_document(void * instance_data, void * init_data);
void on_convert_result(void * session, int result);
void on_convert_event(void * session, const char * event, int arg1);

int initializeImages(char * path);
int convertImage(IString input, IString output, void(*)(void * session, const char *, int page), void * session);
int writeIndexFile(int numPages, IString output, IString type);


/**
 * A request to convert an uploaded document.
 */
class ConversionTask : public IReferencableObject
{
	void * rpc_session;
	IString id;
	IString meeting_id;
	IString document_name;
	IString source_file;

	IMutex ref_lock;

public:
	ConversionTask(void * session, const char * _meeting_id, const char * _document_name, const char * _source_file) :
		rpc_session(session), meeting_id(_meeting_id), document_name(_document_name), source_file(_source_file)
		{ char buff[16]; sprintf(buff, "%d", ++g_task_id); id = buff; }

	void* getRPCSession()	{ return rpc_session; }
    const char * getMeetingId() { return meeting_id; }
    const char * getDocument() { return document_name; }
    const char * getSourceFile() { return source_file; }

    void lock();
    void unlock();

    enum { TYPE_TASK = 6491 };
    virtual int type()
        { return TYPE_TASK; }
    virtual unsigned int hash()
        { return id.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && id == ((ConversionTask*)obj)->id; }

};

void ConversionTask::lock()
{
    ref_lock.lock();
}

void ConversionTask::unlock()
{
    ref_lock.unlock();
}


/**
 * Watchdog thread to monitor for stuck conversions and send warning email.
 * Should watchdog also attempt to kill converter?
 */
class Monitor : public IThread
{
public:
    void run();
};

void Monitor::run()
{
    int interval = 30*1000; // 30 secs in msecs
    bool reported = false;

    while (true)
    {
        // sleep for interval in msecs
#ifdef LINUX
		struct timespec tv = { interval / 1000, interval % 1000 * 1000000 };
		nanosleep(&tv, NULL);
#else
        Sleep(interval);
#endif
        if (g_loader_start != 0 && time(NULL) - g_loader_start > g_warning_time)
        {
            if (!reported)
            {
                char msg[512];
                sprintf(msg, "A conversion has been in process for %d seconds.", (int)(time(NULL) - g_loader_start));
                send_email(g_admin_emails, (char*)"Converter report", msg);
                log_error(ZONE, msg);
                reported = true;
            }
        }
        else
        {
            if (reported)
            {
                char msg[512];
                sprintf(msg, "Conversion processing has resumed.");
                send_email(g_admin_emails, (char*)"Converter report", msg);
                log_debug(ZONE, msg);
                reported = false;
            }
        }
    }
}


static int init(config_t conf)
{
	log_status(ZONE, "Conversion service starting");

    g_server_name = config_get_one(conf, JCOMPNAME, "iicServer", 0);
    g_host_portal = config_get_one(conf, JCOMPNAME, "hostPortal", 0);
    g_batch_threads = atoi(config_get_one(conf, JCOMPNAME, "batch-threads", 0));

    g_converter_uid = atoi(config_get_one(conf, JCOMPNAME, "convert-uid", 0));
    g_converter_cmd = config_get_one(conf, JCOMPNAME, "convert-command", 0);
    g_spool_path = config_get_one(conf, JCOMPNAME, "spooling-root", 0);
    g_recording_base_url = config_get_one(conf, JCOMPNAME, "mtg-repository-url", 0);
    g_docshare_base_url = config_get_one(conf, JCOMPNAME, "docshare-repository-url", 0);
    sRepository.set_archive_name_key(config_get_one(conf, JCOMPNAME, "archive-name-key", 0));
    g_repo_username = config_get_one(conf, JCOMPNAME, "repo-username", 0);
    g_repo_password = config_get_one(conf, JCOMPNAME, "repo-password", 0);
    g_admin_emails = config_get_one(conf, JCOMPNAME, "admin-emails", 0);
    const char *warning_time = config_get_one(conf, JCOMPNAME, "warning-time", 0);
    if (warning_time && *warning_time)
        g_warning_time = atoi(warning_time);

	IThread::initialize(); // ok since a singleton class
	g_tpool = new IThreadPool(&process_document, NULL, g_batch_threads);

    g_loader_lock = new IMutex();

    const char *zonlang = getenv("ZONLANG");
    if (zonlang == NULL)
        zonlang = "";	// use system default

    log_debug(ZONE, "Setting locale to %s", zonlang);

    // initialize locale and timezone for display purposes (email generation)
    setlocale( LC_ALL, zonlang );
    tzset();

	bindtextdomain(JCOMPNAME, "/opt/iic/locale");
    textdomain(JCOMPNAME);
    char    *pBTC = bind_textdomain_codeset(JCOMPNAME, "UTF-8");
    log_debug(ZONE, "bind_textdomain_codeset( ) returned %s", pBTC);

	return Ok;
}


// Get string value from xmlrpc value, optionally checking that string
// only contains alphanumeric characters.
char *get_string_value(xmlrpc_env *env, xmlrpc_value *val, bool validate)
{
    char *pstr = NULL;
    xmlrpc_parse_value(env, val, "s", &pstr);
    XMLRPC_FAIL_IF_FAULT(env);

    if (validate)
    {
        char *p2 = pstr;
        for (;*p2;p2++)
        {
            if (!isalnum(*p2) && *p2 != '.')
            {
                xmlrpc_env_set_fault(env, ParameterParseError, "Error parsing function input:  illegal characters in field name");
                goto cleanup;
            }
        }
    }

cleanup:
    return pstr;
}

int run_converter(IString src, IString out)
{
    pid_t pid = 0;
    int status = 0;

    g_loader_lock->lock();
    g_loader_start = time(NULL);

    pid = fork();
    if (pid == 0)
    {
        // Probably need something like this to run multiple instances of OO...
        //char * environ[] = { "HOME=/var/www", NULL };
        //setuid(g_converter_uid);
        //log_debug(ZONE, "Process uid %d\n", getuid());

        log_debug(ZONE, "Process args %s, %s\n", src.get_string(), out.get_string());
        freopen("/var/log/iic/documentloader.log", "w", stderr);
        execl(g_converter_cmd, "documentloader", src.get_string(), out.get_string(), NULL);
    }
    else
    {
        waitpid(pid, &status, 0);
    }

    g_loader_start = 0;
    g_loader_lock->unlock();

    log_debug(ZONE, "Conversion status: %d", status);
    return status;
}

int delete_folder(const char *dirname)
{
    DIR *dir;
    struct dirent *entry;
    char path[PATH_MAX];

    if (path == NULL)
    {
        return -1;
    }
    dir = opendir(dirname);
    if (dir == NULL)
    {
        return -1;
    }

    while ((entry = readdir(dir)) != NULL)
    {
        if (strcmp(entry->d_name, ".") && strcmp(entry->d_name, ".."))
        {
            snprintf(path, (size_t) PATH_MAX, "%s/%s", dirname, entry->d_name);
            if (entry->d_type == DT_DIR)
            {
                delete_folder(path);
            }
            else
            {
                unlink(path);
            }
        }
    }
    closedir(dir);

    rmdir(dirname);

    return 0;
}

int get_source(ConversionTask * task, IString srcUrl, IString & localPath)
{
    char path[PATH_MAX];

    snprintf(path, sizeof(path), "/tmp/%d-%s", ++g_counter, task->getDocument());
    delete_folder(path);
    mkdir(path, 0700);

    localPath = IString(path);

    IString fileUrl = srcUrl + "/" + task->getSourceFile();

    log_debug(ZONE, "Downloading %s", fileUrl.get_string());

    int status = fetch_file(fileUrl, localPath + "/" + task->getSourceFile(), g_repo_username, g_repo_password);

    return status;
}

int store_result(ConversionTask * task, IString localPath, IString srcUrl)
{
    DIR *dir;
    struct dirent *entry;
    char path[PATH_MAX];

    if (path == NULL)
    {
        return -1;
    }
    dir = opendir(localPath);
    if (dir == NULL)
    {
        return -1;
    }

    while ((entry = readdir(dir)) != NULL)
    {
        if (strcmp(entry->d_name, ".") && strcmp(entry->d_name, ".."))
        {
            snprintf(path, (size_t) PATH_MAX, "%s/%s", localPath.get_string(), entry->d_name);
            if (entry->d_type != DT_DIR)
            {
                log_debug(ZONE, "Uploading %s", path);
                upload_file(path, srcUrl + "/" + entry->d_name, g_repo_username, g_repo_password);
            }
        }
    }
    closedir(dir);

    return 0;
}

IString get_extension(IString& path)
{
    const char *p = strrchr(path, '.');
    const char *s = strrchr(path, '/');

    if (p == NULL || p < s)
        return "";

    IString ext(p+1);
    ext.to_lower();
    return ext;
}

void process_document(void * instance_data, void * init_data)
{
	ConversionTask *task = (ConversionTask*)instance_data;
	log_error(ZONE, "Conversion request for doc %s\n", (const char *) task->getDocument());

    IString localPath;
    IString srcFile;
    IString pdfFile;
    int status = -1;
    bool remote;

    IString srcUrl = g_docshare_base_url + "/" + task->getMeetingId() + "/";
    if (srcUrl.find("http:") == 0)
    {
        srcUrl += task->getDocument();
        remote = true;
        status = get_source(task, srcUrl, localPath);
    }
    else if (srcUrl.find("file:") == 0)
    {
        srcUrl += task->getDocument();
        localPath = srcUrl;
        localPath.erase(0,5);
        remote = false;
        status = 0;
    }
    if (!status)
    {
        IString filePath = localPath + "/" + task->getSourceFile();
        localPath = localPath + "/out";
        mkdir(localPath, 0700);
        if (get_extension(filePath) != "pdf")
        {
            status = run_converter(filePath, localPath + "/");
            if (status)
            {
                log_error(ZONE, "Conversion of %s failed: %d", (const char *) task->getDocument(), status);
            }
            pdfFile = localPath + "/document.pdf";
            on_convert_event(task->getRPCSession(), "PDFRendered", 0);
        }
        else
        {
            log_debug(ZONE, "File has PDF extension, converting directly to images");
            pdfFile = filePath;
        }
    }
    if (!status)
    {
        int numPages = convertImage(pdfFile, localPath + "/", on_convert_event, task->getRPCSession());
        if (numPages <= 0)
        {
            log_error(ZONE, "Conversion to PNG images failed");
            status = -1;
        }
        else
        {
            writeIndexFile(numPages, localPath + "/", "document");
        }
    }
    if (!status && remote)
    {
        status = store_result(task, localPath, srcUrl);
    }

    on_convert_result(task->getRPCSession(), status);
}


// Convert document
// Parameters:
//      USERNAME, MEETINGID, DOCUMENT_NAME, SOURCE_FILE
//      (Document name and source file should be URI escaped)
//
xmlrpc_value *convert_document(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	char * username, * meeting_id, * document_name, * source_file;
    xmlrpc_value *output = NULL;
	ConversionTask *task;

    xmlrpc_parse_value(env, param_array, "(ssss)", &username, &meeting_id, &document_name, &source_file);
    if (env->fault_occurred)
    {
		xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to conversion.convert_document");
        goto cleanup;
    }

	task = new ConversionTask(user_data, meeting_id, document_name, source_file);
	g_tpool->push((void *)task);

	return RPC_PENDING;

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to cconvert document");

    return output;
}

void on_convert_result(void *session, int result)
{
    xmlrpc_env env;
    xmlrpc_env_init(&env);

    xmlrpc_value * output = xmlrpc_build_value(&env, "(i)", result);
    rpc_send_result(session, output);

    if (output)
    {
        xmlrpc_DECREF(output);
    }
}

void on_convert_event(void *session, const char * type, int arg)
{
    xmlrpc_env env;
    xmlrpc_env_init(&env);

    const char * from = rpc_get_from_full(session);

    xmlrpc_value * output = xmlrpc_build_value(&env, "(si)", type, arg);

    if (rpc_make_call_array("event", from, "ConversionEvent", output))
    {
        log_error(ZONE, "Error sending entering meeting event to %s\n", from);
    }

    if (output)
    {
        xmlrpc_DECREF(output);
    }
}

int RPC_run()
{
	static unsigned counter = 0;

	if ((++counter % 30000) == 0)
	{
		event_log.check_rotation();
	}

	return 0;
}


/* Initialization as jabber component. */
int main(int argc, char ** argv)
{
	fprintf(stderr, "IIC Conversion Server 1.0   Copyright (C) 2012 mercuri.ca\n\n");

    initializeImages(*argv);

    config_t conf = config_new();
    config_load(conf, JCOMPNAME, 0, "/opt/iic/conf/config.xml");
    init(conf);
	if (jc_init(conf, JCOMPNAME))
	{
		log_error(ZONE, "Unable to initialize converter\n");

		return -1;
	}

    rpc_set_callbacks(NULL, NULL, RPC_run);
    rpc_set_user_callable(1);

	// Add methods to RPC server
    rpc_add_method("converter.convert_document", &convert_document, NULL);

    Monitor monitor;
    monitor.start();

	jc_run(false);

	return 0;
}


