CREATE INDEX meetingstatus_index_1 ON meetingstatus(meetingid);
CREATE INDEX meetingstatus_index_2 ON meetingstatus(start_time, status);
CREATE INDEX participants_index_3 ON participants(roll);
CREATE INDEX MEETINGS_INDEX_2 ON MEETINGS(COMMUNITYID);
CREATE INDEX MEETINGS_INDEX_3 ON MEETINGS(TIME);
