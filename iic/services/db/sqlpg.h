#ifndef SQLPG_HEADER
#define SQLPG_HEADER

#include <libpq-fe.h>
#include "sqldb.h"

const int MaxInactivity = 600;

class PGConnection : public Connection
{
    PGconn *connection;
    time_t last_usage;

    bool handle_error(int & retry);
    
public:
    PGConnection(DBManager *mgr);
    ~PGConnection();
    
    int connect(const char *host, const char *port, const char *user, const char *pass, const char *db);
    int is_connected();
    void reconnect();
    void disconnect();

    ResultSet *execute(const char *sql);
	char *escape(const char *in, char *buff, int buff_len);
    
    const char *get_error();
};

class PGResultSet : public ResultSet
{
	int rows_affected;
    PGresult *result;
    IString local_error;

    int check_resultset();
    
public:
    PGResultSet(PGresult *_result);
    PGResultSet(int rows_affected);
	PGResultSet(const char *error);
    ~PGResultSet();

	bool is_update();
	int get_num_affected();
    
    int get_num_fields();
    const char *get_field_name(int col);
    int get_field_index(const char *name);
    int get_field_type(int col);

    int get_num_rows();
    const char *get_value(int row, int col);
    const char *get_value(int row, const char *field);
    
    void close();
	
	bool is_error();
    const char *get_error();
    int get_result_status();
};

#endif

