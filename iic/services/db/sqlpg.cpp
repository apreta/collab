// TODO:
//   Logging
//   Data type handling
//   Connection pooling
//   Threading
//   Set callbacks for notifications

#include <stdio.h>
#include <util.h>
#include "sqlpg.h"
#include "../../jcomponent/util/log.h"


void sleep_millis(int ms)
{
	struct timespec tv = { ms / 1000, ms % 1000 * 1000000 };
	nanosleep(&tv, NULL);
}


PGConnection::PGConnection(DBManager *mgr) :
  Connection(mgr)
{
    connection = NULL;
}

PGConnection::~PGConnection()
{
    disconnect();
}

int PGConnection::connect(const char *host, const char *port, const char *user, const char *pass, const char *db)
{
    if (port && (atoi(port) == 0))
        port = NULL;
    
    last_usage = time(NULL);
    connection = PQsetdbLogin(host, port, NULL, NULL, db, user, pass);

//	PQsetnonblocking(self->connection, 1);
    
    return (connection == NULL);
}

void PGConnection::reconnect()
{
    last_usage = time(NULL);
    if (connection != NULL)
        PQreset(connection);
}

/*
 * True if connected to a pgsql db.
 */
int PGConnection::is_connected()
{
    if (connection == NULL || PQstatus(connection) != CONNECTION_OK)
        return 0;
    return 1;
}

/*
 * Disconnect from the pgsql db.
 */
void PGConnection::disconnect()
{
    if (!is_connected())
        return;
    PQfinish(connection);
    connection = NULL;
}

// Log error and see if we should retry operation.
bool PGConnection::handle_error(int & retry)
{
    if (++retry < 5)
    {
        log_debug(ZONE, "Unexpected DB error, reconnect and retry query [%s]", get_error());
        reconnect();
        sleep_millis(100);
        return true;
    }

    return false;
}

/*
 * Issue a query to the pgsql db we are connected to.
 */
ResultSet *PGConnection::execute(const char *query)
{
    PGresult    *r;
	ResultSet   *rs = NULL;
    ExecStatusType st;
    int retry = 0;
    
    /* Execute in synchronous mode
       Postgres has an async mode, but I suspect we'll use threads instead..
    */
    while (true)
    {
        if (!is_connected())
        {
            log_debug(ZONE, "DB connection is bad, reconnecting first");
            reconnect();
        }

        if (time(NULL) - last_usage > MaxInactivity)
        {
            log_debug(ZONE, "DB connection appears stale, reconnecting first");
            reconnect();
        }
        
        last_usage = time(NULL);
        r = PQexec(connection, query);
    
        if (!r)
        {
            if (handle_error(retry))
                continue;
            return new PGResultSet(get_error());
        }

        st = PQresultStatus(r);

        if (st == PGRES_TUPLES_OK)
            rs = new PGResultSet(r);
        else if (st == PGRES_COMMAND_OK)
        {
            int rows = 0;
            char *rowstr;
            rowstr = PQcmdTuples(r);
            if (strlen(rowstr) > 0)
                rows = atoi(rowstr);
            rs = new PGResultSet(rows);
            PQclear(r);
        }
        else
        {
            if (handle_error(retry))
                continue;
            rs = new PGResultSet(get_error());
            PQclear(r);
        }

        return rs;
    }
}

char *PGConnection::escape(const char *in, char *buff, int buff_size)
{
	int len = strlen(in);
	if ((len * 2) + 1 > buff_size)
		len = (buff_size / 2) - 1;
	PQescapeString(buff, in, len);
	return buff;
}

const char *PGConnection::get_error()
{
    return PQerrorMessage(connection);
}

PGResultSet::PGResultSet(PGresult *_result)
{
    result = _result;
	rows_affected = 0;
}

PGResultSet::PGResultSet(int _rows_affected)
{
    result = NULL;
	rows_affected = _rows_affected;
}

PGResultSet::PGResultSet(const char *_error) :
	local_error(_error)
{
	if (!local_error.length())
		local_error = "No error message available";
	result = NULL;
	rows_affected = 0;
}

PGResultSet::~PGResultSet()
{
    close();
}

int PGResultSet::check_resultset()
{
    if (!result)
    {
        local_error = "Database result set is not open";
        return -1;
    }
    else
    {
        local_error = NULL;
        return 0;
    }
}

bool PGResultSet::is_update()
{
	if (result == NULL)
		return true;
	else
		return false;
}

int PGResultSet::get_num_affected()
{
	return rows_affected;
}

int PGResultSet::get_num_rows()
{
    if (check_resultset())
        return -1;
    return PQntuples(result);
}

const char *PGResultSet::get_value(int row, int col)
{
    if (check_resultset())
        return NULL;
    return PQgetvalue(result, row, col);
}

const char *PGResultSet::get_value(int row, const char *field_name)
{
    if (check_resultset())
        return NULL;
        
    int field = get_field_index(field_name);
    if (field == -1)
    {
        local_error = IString("Requested field ") + field_name + " not found in result set";
        return NULL;
    }
    else
        return PQgetvalue(result, row, field);    
}

int PGResultSet::get_num_fields()
{
    if (check_resultset())
        return -1;
    return PQnfields(result);
}

const char *PGResultSet::get_field_name(int col)
{
    if (check_resultset())
        return NULL;
    return PQfname(result, col);
}

int PGResultSet::get_field_index(const char *name)
{
    if (check_resultset())
        return -1;
    return PQfnumber(result, name);
}


int PGResultSet::get_field_type(int col)
{
    if (check_resultset())
        return -1;

    // Todo:  translate to locally defined type code
    return PQftype(result, col);
}

void PGResultSet::close()
{
	if (result)
        PQclear(result);
    result = NULL;
}

bool PGResultSet::is_error()
{
	if (!result && local_error.length())
		return true;
	return false;
}

const char *PGResultSet::get_error()
{
    if (local_error)
        return local_error;
    return PQresultErrorMessage(result);
}

int PGResultSet::get_result_status()
{
    return PQresultStatus(result);
}

