BEGIN;
ALTER TABLE communities ADD COLUMN bridgephone_new varchar(128);
UPDATE communities SET bridgephone_new = bridgephone;
ALTER TABLE communities RENAME COLUMN bridgephone TO bridgephone_old;
ALTER TABLE communities RENAME COLUMN bridgephone_new TO bridgephone;
ALTER TABLE communities DROP COLUMN bridgephone_old;
COMMIT;
