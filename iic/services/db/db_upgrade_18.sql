/*  Cascade delete of user to rosterusers table jid column */

/* drop table first to remove any users that have already been 
   deleted from users table but still remain.  have to do this 
   allow key restraint */
DROP TABLE rosterusers CASCADE;
CREATE TABLE rosterusers (
  username VARCHAR(256) NOT NULL,
  jid VARCHAR(255) NOT NULL,
  nick VARCHAR(255),
  subscription CHAR(1) NOT NULL,  /* 'N', 'T', 'F', or 'B' */
  ask CHAR(1) NOT NULL,           /* '-', 'S', or 'U' */
  server CHAR(1) NOT NULL,         /* 'Y' or 'N' */
  subscribe VARCHAR(10),
  type VARCHAR(64)
);
CREATE INDEX I_rosteru_username ON rosterusers (username);

alter table rosterusers add constraint rosterusers_user
    foreign key(username) references users(username) on delete cascade;

/* new */
alter table rosterusers add constraint rosterusers_jid
foreign key(jid) references users(username) on delete cascade;

/*GRANT ALL on rosterusers to "Administrator"; */
