/* (c)2002 imidio all rights reserved */

#ifndef SQLDB_HEADER
#define SQLDB_HEADER

#include <glib.h>
#include <gmodule.h>
#include "../../util/istring.h"

#define MAX_FIELD_WIDTH 4096


class DBManager;
class MethodList;
class Connection;
class ResultSet;

// API
DBManager *sqldb_get_manager(const char *file);

// TODO:
// Define parameter types--for now we assume all parameters are c strings.

class ParamDef : public IString
{
   bool encode;

public:
    ParamDef(IString &str, bool _encode) : IString(str)
    {
        encode = _encode;
    }

    bool get_encode()
    {
        return encode;
    }
};


class QueryDef : public IObject
{
	enum { TYPE_QUERYDEF = 723 };
	IString name;
	IString sql;

	IVector params;

public:
	QueryDef(IString &_name, IString &_sql) :
		name(_name), sql(_sql) 
	{ }

	void add_parameter(IString &param, bool encode)
	{
		params.add(new ParamDef(param, encode));
	}
	
	int get_parameter_count()
	{
		return params.size();
	}
	
	IString *get_parameter(int i)
	{
		return (IString *)params.get(i);
	}
	
    bool get_encode(int i)
    {
        return ((ParamDef *)params.get(i))->get_encode();
    }
    
	IString &get_sql()
	{
		return sql;
	}
	
	int type()
	{
		return TYPE_QUERYDEF;
	}
	
	unsigned hash()
	{
		return name.hash();
	}
	
	bool equals(IObject *o)
	{
		if (o->type() != TYPE_QUERYDEF)
			return false;
		return name.equals((QueryDef*)o);
	}
};


class ParamList
{
    int allocated;
    int in_use;
    const char **params;

public:    
    ParamList(int est_size);
    ~ParamList();

    void resize(int size);
    
    void add(const char *str);
    void set(int pos, const char *str);
    
    int size()
    {
        return in_use;
    }
    
    const char *get(int index)
    {
        if (index > in_use)
            return "";
        return params[index];
    }
};


class DBManager
{
	bool initialized;
	
	IStringHash queries;

    IString host;
    IString port;
    IString user;
    IString password;
    IString db;

    IHash connections;

    void check_for_sys_params(IString &);
    void build_sql(IString &, Connection *c, QueryDef *def, va_list &args);
    void build_sql(IString &, Connection *c, QueryDef *def, ParamList *params);
	void build_sql(IString &, Connection *c, va_list &args);
	
public:
    DBManager();
	virtual ~DBManager();
	
    int initialize(const char *config);

    Connection *create_connection();
    Connection *get_connection();
    void free_connection(Connection *c);
	
	ResultSet *execute_sql(const char *sql, ...);
	ResultSet *execute(const char *operation, ...);
	ResultSet *execute(const char *operation, ParamList *params);

	char *escape(const char *in, char *buff, int buff_len);
	
    void closeResults(ResultSet *&rs);

	void close();
};


class Connection : public IObject
{
	enum { TYPE_CONNECTION = 1593 };

public:
	Connection(DBManager *mgr) {}
	virtual ~Connection() {}
    virtual int  connect(const char *host, const char *port, const char *user, const char *pass, const char *db) = 0;
    virtual void reconnect() = 0;
    virtual int  is_connected() = 0;
    virtual void disconnect() = 0;

    virtual ResultSet *execute(const char *sql) = 0;

	virtual char *escape(const char *in, char *buff, int buff_len) = 0;

    virtual const char *get_error() = 0;

	int type()
	{
		return TYPE_CONNECTION;
	}
	
    // Insufficient to use in collection other then list
	unsigned hash()
	{
		return 0;
	}
	
	bool equals(IObject *o)
	{
        return (o == this);
	}
};

class ResultSet
{
public:
	ResultSet() {}
	virtual ~ResultSet() {}

	virtual bool is_update() = 0;
	virtual int get_num_affected() = 0;
	
    virtual int get_num_fields() = 0;
    virtual const char *get_field_name(int col) = 0;
    virtual int get_field_index(const char *name) = 0;
    virtual int get_field_type(int col) = 0;

    virtual int get_num_rows() = 0;
    virtual const char *get_value(int row, int col) = 0;
    virtual const char *get_value(int row, const char *field) = 0;
    
    virtual void close() = 0;
	
	virtual bool is_error() = 0;
    virtual const char *get_error() = 0;
    virtual int get_result_status() = 0;
};

#endif

