BEGIN;

ALTER TABLE communities ADD COLUMN contactphone_new varchar(256);
UPDATE communities SET contactphone_new = contactphone;
ALTER TABLE communities RENAME COLUMN contactphone TO contactphone_old;
ALTER TABLE communities RENAME COLUMN contactphone_new TO contactphone;
ALTER TABLE communities DROP COLUMN contactphone_old;

ALTER TABLE contacts ADD COLUMN busphone_new varchar(256);
UPDATE contacts SET busphone_new = busphone;
ALTER TABLE contacts RENAME COLUMN busphone TO busphone_old;
ALTER TABLE contacts RENAME COLUMN busphone_new TO busphone;
ALTER TABLE contacts DROP COLUMN busphone_old;

ALTER TABLE contacts ADD COLUMN homephone_new varchar(256);
UPDATE contacts SET homephone_new = homephone;
ALTER TABLE contacts RENAME COLUMN homephone TO homephone_old;
ALTER TABLE contacts RENAME COLUMN homephone_new TO homephone;
ALTER TABLE contacts DROP COLUMN homephone_old;

ALTER TABLE contacts ADD COLUMN mobilephone_new varchar(256);
UPDATE contacts SET mobilephone_new = mobilephone;
ALTER TABLE contacts RENAME COLUMN mobilephone TO mobilephone_old;
ALTER TABLE contacts RENAME COLUMN mobilephone_new TO mobilephone;
ALTER TABLE contacts DROP COLUMN mobilephone_old;

ALTER TABLE contacts ADD COLUMN otherphone_new varchar(256);
UPDATE contacts SET otherphone_new = otherphone;
ALTER TABLE contacts RENAME COLUMN otherphone TO otherphone_old;
ALTER TABLE contacts RENAME COLUMN otherphone_new TO otherphone;
ALTER TABLE contacts DROP COLUMN otherphone_old;

ALTER TABLE participants ADD COLUMN phone_new varchar(256);
UPDATE participants SET phone_new = phone;
ALTER TABLE participants RENAME COLUMN phone TO phone_old;
ALTER TABLE participants RENAME COLUMN phone_new TO phone;
ALTER TABLE participants DROP COLUMN phone_old;

COMMIT;
