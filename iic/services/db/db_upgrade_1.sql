/*  Changes to support tracking of instant invitations */

alter table PARTICIPANTS add column TYPE integer;
alter table PARTICIPANTS add column EXPIRES integer;
