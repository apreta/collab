
#include <stdio.h>
#include "sqldb.h"

void dump(ResultSet *rs)
{
	for (int i=0; i<rs->get_num_rows(); i++)
	{
		for (int j=0; j<rs->get_num_fields(); j++)
		{
			printf("%s  ", rs->get_value(i, j));
		}
		printf("\n");
	}
}

int main()
{
	DBManager *db = sqldb_get_manager("sqldb.xml");
	
	Connection *conn = db->get_connection();
	
	ResultSet *rs = conn->execute("SELECT * FROM TESTzyx");
	if (rs->is_error())
		printf("%s\n", rs->get_error());

	rs = conn->execute("SELECT * FROM TEST");
	if (rs->is_error())
		printf("%s\n", rs->get_error());
	else
		dump(rs);
	db->free_connection(conn);
	
	rs = db->execute_sql("SELECT * FROM $$TABLE$$", "USERS");
	if (rs->is_error())
		printf("%s\n", rs->get_error());
	else
		dump(rs);

	rs = db->execute("add-user", "first", "last", "friendly", "somewhere@here.com", "4450455");
	if (rs->is_error())
		printf("%s\n", rs->get_error());
	else
		printf("Rows inserted: %d\n", rs->get_num_affected());
	
	rs = db->execute("get-users");
	if (rs->is_error())
		printf("%s\n", rs->get_error());
	else
		dump(rs);
		
	db->close();
}
