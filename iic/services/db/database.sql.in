/* Create the Jabber database */

/* $Id: database.sql.in,v 1.11 2007-10-21 03:42:57 mike Exp $ */

/* Notes:
	  - CamelCase names refer to enums defined in iic/services/common/common.h.
      - In most cases, table relationship can be identified by matching key names (e.g. userid, meetingid).
      - In a few cases, hostid or ownerid are used to refer to a userid to make the nature
	    of the relationship clearer.
	  - There is some duplication of data between tables and fields (e.g. having screen name and user name as
	    separate columns even though the screen name is contained in the user name)--this is either to make
		the SQL queries easier or more efficient, or just due to development time pressures.
*/

CREATE LANGUAGE PLPGSQL;

DROP SEQUENCE COMMUNITYID_SEQ;
CREATE SEQUENCE COMMUNITYID_SEQ START 3;

DROP SEQUENCE USERID_SEQ;
CREATE SEQUENCE USERID_SEQ START 10;

DROP SEQUENCE DIRECTORYID_SEQ;
CREATE SEQUENCE DIRECTORYID_SEQ START 10;

DROP SEQUENCE DIRSUBSID_SEQ;
CREATE SEQUENCE DIRSUBSID_SEQ START 10;

DROP SEQUENCE MEMBERID_SEQ;
CREATE SEQUENCE MEMBERID_SEQ START 10;

DROP SEQUENCE PROFILE_SEQ;
CREATE SEQUENCE PROFILE_SEQ START 10;

DROP SEQUENCE ROSTERID_SEQ;
CREATE SEQUENCE ROSTERID_SEQ;

DROP SEQUENCE PIN_SEQ;
CREATE SEQUENCE PIN_SEQ START 100000;

/* Community table...allows multiple tenants in a single db. */
/* May be companies, etc.  Users in one community do not see */
/* data from another community.                              */
DROP TABLE COMMUNITIES CASCADE;
CREATE TABLE COMMUNITIES (
  COMMUNITYID INTEGER PRIMARY KEY DEFAULT nextval('COMMUNITYID_SEQ'),
  COMMUNITYNAME VARCHAR(256) NOT NULL UNIQUE, /* Display name for a community */
  ACCOUNTNUMBER VARCHAR(32),    /* Community's account number (not used internally) */
  CONTACTNAME VARCHAR(256),     /* Name of contact for a community */
  CONTACTEMAIL VARCHAR(256),    /* Email of contact for a community */
  CONTACTPHONE VARCHAR(256),     /* Phone of contact for a community */
  ADMINNAME  VARCHAR(256),      /* Name of default admin that will be created for this community */
  ADMINPASSWORD VARCHAR(128),   /* Password of default admin that will be created for this community */
  BRIDGEPHONE VARCHAR(256),     /* Phone number of the bridge that will service this community */
  SYSTEMURL VARCHAR(256),       /* URL that will service requests for this community */
  ACCOUNTTYPE INTEGER,          /* 0 = normal account, 1 = system account (CommunityAccountType) */
  ACCOUNTSTATUS INTEGER,        /* 0 = disabled, 1 = enabled (CommunityAccountStatus) */
  OPTIONS INTEGER,              /* Bitmask for community options (unused) */
  LASTMODIFIED INTEGER			/* Unix time of last modification */
);

/* Authenticated system user.  Will have corresponding contact record. */
DROP TABLE USERS CASCADE;
CREATE TABLE USERS (
  USERID INTEGER PRIMARY KEY DEFAULT nextval('USERID_SEQ'),
  COMMUNITYID INTEGER REFERENCES COMMUNITIES ON DELETE CASCADE,
  SCREENNAME VARCHAR(256),		/* Screen name for user */
  USERNAME VARCHAR(256) NOT NULL UNIQUE,	/* Full jabber id for user (screenname@hostname) */
  PASSWORD VARCHAR(128),		/* User password (may be in clear or SHA-1 encoded) */
  STATE INTEGER,				/* not used */
  LASTLOGIN INTEGER,			/* not used */
  LASTMODIFIED INTEGER,			/* not used */
  ADMIN BOOLEAN,				/* Is user an admin? */
  SUPERADMIN BOOLEAN			/* Is user a super-admin? */
);

CREATE INDEX USERS_INDEX_1 ON USERS(SCREENNAME);

/* A list of contacts. */
/* DirectoryID 0 is reserved for global address book. */
/* DirectoryID 1 is reserved for outlook. */
/* DirectoryID 2 is temporarily reserved for sample LDAP */
DROP TABLE DIRECTORIES CASCADE;
CREATE TABLE DIRECTORIES (
  DIRECTORYID INTEGER PRIMARY KEY DEFAULT nextval('DIRECTORYID_SEQ'),
  COMMUNITYID INTEGER REFERENCES COMMUNITIES ON DELETE CASCADE,
  OWNERID INTEGER REFERENCES USERS ON DELETE CASCADE,		/* userid of owner */
  NAME VARCHAR(128),			/* Directory name */
  TYPE INTEGER,					/* 1 = CAB, 3 = PAB, other values are deprecated */
  LASTMODIFIED INTEGER
);

CREATE INDEX DIRECTORIES_INDEX_1 ON DIRECTORIES(OWNERID);

/* Keep track of which directories each user wants to download. */
/* Currently not used--preferences for which directories to download are stored on the client. */
DROP TABLE DIRECTORY_SUBSCRIPTIONS CASCADE;
CREATE TABLE DIRECTORY_SUBSCRIPTIONS (
  DIRSUBSID INTEGER PRIMARY KEY DEFAULT nextval('DIRSUBSID_SEQ'),
  OWNERID INTEGER REFERENCES CONTACTS ON DELETE CASCADE,	/* userid of owner */
  DIRECTORYID INTEGER REFERENCES DIRECTORIES ON DELETE CASCADE
);

CREATE INDEX DIRECTORY_SUBSCRIPTIONS_INDEX_1 ON DIRECTORY_SUBSCRIPTIONS(OWNERID);

/* Profile/Permissions table.  Stores system wide permissions. */
/* Just using a single record right now...updated by an admin. */
DROP TABLE PROFILES CASCADE;
CREATE TABLE PROFILES (
  PROFILEID INTEGER PRIMARY KEY DEFAULT nextval('PROFILE_SEQ'),
  COMMUNITYID INTEGER REFERENCES COMMUNITIES ON DELETE CASCADE,
  PROFILENAME VARCHAR(256),		/* Profile display name */
  ENABLEDFEATURES INTEGER,		/* Feature mask (ProfileFlags) */
  PROFILEOPTIONS VARCHAR(2000), /* number of options, max voice size, max data size */
  LASTMODIFIED INTEGER
);

/* Contact info. */
DROP TABLE CONTACTS CASCADE;
CREATE TABLE CONTACTS (
  USERID INTEGER PRIMARY KEY DEFAULT nextval('USERID_SEQ'),
  DIRECTORYID INTEGER NOT NULL REFERENCES DIRECTORIES ON DELETE CASCADE,

  USERNAME VARCHAR(256),		/* Full jabber id if contact is a user*/
  SERVER VARCHAR(256),			/* Server name if contact is a user (for future expansion to multiple domains) */

  /* Contact info */
  TITLE VARCHAR(32),
  FIRSTNAME VARCHAR(256),
  MIDDLENAME VARCHAR(256),
  LASTNAME VARCHAR(256),
  SUFFIX VARCHAR(32),
  COMPANY VARCHAR(256),
  JOBTITLE VARCHAR(256),
  ADDRESS1 VARCHAR(256),
  ADDRESS2 VARCHAR(256),
  STATE VARCHAR(256),
  COUNTRY VARCHAR(256),
  POSTALCODE VARCHAR(32),
  SCREENNAME VARCHAR(256),
  AIMNAME VARCHAR(64),			/* not used */
  EMAIL VARCHAR(256),
  EMAIL2 VARCHAR(256),
  EMAIL3 VARCHAR(256),
  BUSPHONE VARCHAR(256),
  BUSDIRECT BOOLEAN,			/* not used */
  HOMEPHONE VARCHAR(256),
  HOMEDIRECT BOOLEAN,			/* not used */
  MOBILEPHONE VARCHAR(256),
  MOBILEDIRECT BOOLEAN,			/* not used */
  OTHERPHONE VARCHAR(256),
  OTHERDIRECT BOOLEAN,			/* not used */
  EXTENSION VARCHAR(256),
  USER1 VARCHAR(128),			/* MSN screen */
  USER2 VARCHAR(128),			/* Yahoo screen */
  USER3 VARCHAR(128),			/* AOL screen */
  USER4 VARCHAR(128),			/* User 1 */
  USER5 VARCHAR(128),			/* User 2 */
  DEFPHONE INTEGER,				/* Follow me phone: 0 = None, 1 = Bus, 2 = Home, 3 = Mobile, 4 = Other (SelPhone) */
  DEFEMAIL INTEGER,				/* not used */
  DEFMEETING INTEGER,			/* not used */

  TYPE INTEGER,             	/* 0 = contact, 1 = user, 2 = admin (UserType) */
  PROFILEID INTEGER,			/* Profile ID (set for users) */

  /* Deprecated -- replaced by profile */
  MAXSIZE INTEGER,
  MAXPRIORITY INTEGER,
  PRIORITYTYPE INTEGER,
  DISABLEDFEATURES INTEGER,
  ENABLEDFEATURES INTEGER,

  LASTMODIFIED INTEGER			/* Unix time of last modification */
);

CREATE INDEX CONTACTS_INDEX_1 ON CONTACTS(SCREENNAME);
CREATE INDEX CONTACTS_INDEX_2 ON CONTACTS(DIRECTORYID);
CREATE INDEX CONTACTS_INDEX_3 ON CONTACTS(LASTMODIFIED);

/* Deleted contacts */
DROP TABLE DELETED_CONTACTS;
CREATE TABLE DELETED_CONTACTS (
  USERID INTEGER PRIMARY KEY,
  DIRECTORYID INTEGER NOT NULL REFERENCES DIRECTORIES ON DELETE CASCADE,
  LASTMODIFIED INTEGER			/* Unix time of deletion */
);

CREATE INDEX DELETED_CONTACTS_INDEX_1 ON DELETED_CONTACTS(DIRECTORYID);
CREATE INDEX DELETED_CONTACTS_INDEX_2 ON DELETED_CONTACTS(LASTMODIFIED);

/* Contact table trigger to move deleted contacts to deleted table */
CREATE OR REPLACE FUNCTION delete_contact() RETURNS TRIGGER AS $SQL$
DECLARE
    deltime INTEGER := CAST(EXTRACT('epoch' FROM NOW()) as INTEGER);
BEGIN
    INSERT INTO DELETED_CONTACTS (USERID,DIRECTORYID,LASTMODIFIED)
    VALUES (OLD.USERID,OLD.DIRECTORYID,deltime);
    RETURN NULL;
END;
$SQL$ LANGUAGE plpgsql;

CREATE TRIGGER contact_trigger AFTER DELETE ON CONTACTS
    FOR EACH ROW EXECUTE PROCEDURE delete_contact();

/* A group of contacts or groups. Each user has at least one default group that may contain groups or contacts. */
/* A contact may be in more then one group.  */
/* Group name "All" is reserved for a directories ALL group, which */
/* contains all contact records in a directory.  The All group cannot */
/* be deleted while the directory exists. */
DROP TABLE GROUPS CASCADE;
CREATE TABLE GROUPS (
  GROUPID INTEGER PRIMARY KEY DEFAULT nextval('USERID_SEQ'),
  DIRECTORYID INTEGER NOT NULL REFERENCES DIRECTORIES ON DELETE CASCADE,
  NAME VARCHAR(128),			/* Group display name */
  TYPE INTEGER,   				/* 1 = All, 2 = Other (GroupType) */
  LASTMODIFIED INTEGER			/* Unix time of last modification (of group itself or members) */
);

CREATE INDEX GROUPS_INDEX_1 ON GROUPS(DIRECTORYID);
CREATE INDEX GROUPS_INDEX_2 ON GROUPS(LASTMODIFIED);

/* Deleted groups */
DROP TABLE DELETED_GROUPS;
CREATE TABLE DELETED_GROUPS (
  GROUPID INTEGER PRIMARY KEY,
  DIRECTORYID INTEGER NOT NULL REFERENCES DIRECTORIES ON DELETE CASCADE,
  LASTMODIFIED INTEGER			/* Unix time of last deletion */
);

CREATE INDEX DELETED_GROUPS_INDEX_1 ON DELETED_GROUPS(DIRECTORYID);
CREATE INDEX DELETED_GROUPS_INDEX_2 ON DELETED_GROUPS(LASTMODIFIED);

/* Group table trigger to move deleted groups to deleted table */
CREATE OR REPLACE FUNCTION delete_group() RETURNS TRIGGER AS $SQL$
DECLARE
    deltime INTEGER := CAST(EXTRACT('epoch' FROM NOW()) as INTEGER);
BEGIN
    INSERT INTO DELETED_GROUPS (USERID,DIRECTORYID,LASTMODIFIED)
    VALUES (OLD.USERID,OLD.DIRECTORYID,deltime);
    RETURN NULL;
END;
$SQL$ LANGUAGE plpgsql;

CREATE TRIGGER group_trigger AFTER DELETE ON GROUPS
    FOR EACH ROW EXECUTE PROCEDURE delete_group();

/* Member of a group.  UserID may contain user id or group id (not sure if this is supported). */
DROP TABLE MEMBERS CASCADE;
CREATE TABLE MEMBERS
(
  MEMBERID INTEGER PRIMARY KEY DEFAULT nextval('MEMBERID_SEQ'),
  GROUPID INTEGER NOT NULL REFERENCES GROUPS ON DELETE CASCADE,
  USERID INTEGER NOT NULL REFERENCES CONTACTS ON DELETE CASCADE
);

CREATE INDEX MEMBERS_INDEX_1 ON MEMBERS(GROUPID);

/* External member of group.  Allows user to place local contacts (e.g. outlook) into a group. */
DROP TABLE MEMBERS_EXTERNAL CASCADE;
CREATE TABLE MEMBERS_EXTERNAL
(
  MEMBERID INTEGER PRIMARY KEY DEFAULT nextval('MEMBERID_SEQ'),
  GROUPID INTEGER NOT NULL REFERENCES GROUPS ON DELETE CASCADE,
  EXTERNALID VARCHAR(200) NOT NULL
);

CREATE INDEX MEMBERS_EXTERNAL_INDEX_1 ON MEMBERS_EXTERNAL(GROUPID);

/* Each user has a roster, which specifies for which contacts presence will automatically be detected. */
/* (not used, see xdb_sql tables below) */
DROP TABLE ROSTER CASCADE;
CREATE TABLE ROSTER (
  ROSTERID INTEGER PRIMARY KEY DEFAULT nextval('ROSTERID_SEQ'),
  OWNERID INTEGER NOT NULL REFERENCES USERS ON DELETE CASCADE,
  USERID INTEGER NOT NULL REFERENCES CONTACTS ON DELETE CASCADE
);

/* PIN table - allows us to create PINs of specific length instead of */
/* using a generated PIN from a sequence. */
DROP TABLE MEETING_PINS CASCADE;
CREATE TABLE MEETING_PINS (
   PINID BIGINT PRIMARY KEY
);

/* Meetings */
DROP TABLE MEETINGS CASCADE;
CREATE TABLE MEETINGS (
  MEETINGID BIGINT PRIMARY KEY,
  COMMUNITYID INTEGER REFERENCES COMMUNITIES ON DELETE CASCADE,
  HOSTID INTEGER NOT NULL REFERENCES CONTACTS ON DELETE CASCADE,
  TYPE INTEGER,             	/* 0 = Disabled, 1 = Instant, 2 = Scheduled (MeetingType) */
  PRIVACY INTEGER,          	/* 0 = Public, 1 = Unlisted, 2 = Private (MeetingPrivacy) */
  PRIORITY INTEGER,				/* not used */
  TITLE VARCHAR(256),			/* Meeting title */
  DESCRIPTION VARCHAR(256),		/* Additional info about meeting */
  SIZE INTEGER,					/* Number of participants (not used) */
  DURATION INTEGER,         	/* deprecated */
  TIME INTEGER,             	/* Scheduled meeting start time (unix time) */
  RECURRENCE INTEGER,       	/* 0 = None, 1 = Daily, 2 = Weekly, 3 = Monthly (not implemented) */
  RECUR_END INTEGER,			/* End of recurrence (not implemented) */
  OPTIONS INTEGER,				/* 1 = Wait For Moderator, 2 = Announce/Screen Callers, 4 = Lecture Mode,  8 = Lock Participants, 16 = Auto time extend, 32 = Password (MeetingOptions) */
  PASSWORD VARCHAR(32),     	/* Meeting password */
  END_TIME INTEGER,             /* Scheduled meeting end time (unix time) */
  INVITE_EXPIRATION INTEGER,    /* Number of hours after meeting start that invites are valid for... */
  INVITE_VALID_BEFORE INTEGER,  /* Number of minutes before the meeting start that invites are valid for... */
  INVITE_MESSAGE TEXT,          /* Default message to be sent with invitations */
  DISPLAY_ATTENDEES INTEGER,    /* 0 = None, 1 = Moderators Only, 2 = All (MeetingDisplay) */
  INVITEE_CHAT_OPTIONS INTEGER  /* Chat privileges for non-moderators 0 = None, 1 = To Moderators Only, 2 = To All (MeetingChat) */
);

CREATE INDEX MEETINGS_INDEX_1 ON MEETINGS(HOSTID);
CREATE INDEX MEETINGS_INDEX_2 ON MEETINGS(COMMUNITYID);
CREATE INDEX MEETINGS_INDEX_3 ON MEETINGS(TIME);

/* Meeting Status (updated as meeting is started/stopped) */
DROP TABLE MEETINGSTATUS CASCADE;
CREATE TABLE MEETINGSTATUS (
  MEETINGID BIGINT NOT NULL REFERENCES MEETINGS ON DELETE CASCADE,
  TIME INTEGER,					/* Time status last changed */
  START_TIME INTEGER,			/* Time meeting last started */
  END_TIME INTEGER,				/* Time meeting last ended */
  STATUS INTEGER            	/* 0 = Not Started, 1 = In Progress, 2 = Completed (MeetingStatus) */
);

CREATE INDEX meetingstatus_index_1 ON meetingstatus(meetingid);
CREATE INDEX meetingstatus_index_2 ON meetingstatus(start_time, status);

/* Participants */
/* Holds both scheduled participants and instant invitees */
DROP TABLE PARTICIPANTS CASCADE;
CREATE TABLE PARTICIPANTS (
  PARTICIPANTID BIGINT PRIMARY KEY,
  MEETINGID BIGINT NOT NULL REFERENCES MEETINGS ON DELETE CASCADE,
  TYPE INTEGER,					/* 0 = Normal, 1 = Instant invitee (ParticipantType) */
  ROLL INTEGER,             	/* 0 = Normal, 1 = Listener, 2 = Moderator, 3 = Host (ParticipantRoll) */
  USERID INTEGER,           	/* -1 = No contact record, otherwise references contacts */
  USERNAME VARCHAR(256),		/* Full jabber id if participant is a user (avoids extra query during PIN lookup) */
  NAME VARCHAR(256),			/* Display name for meeting participant */
  DESCRIPTION VARCHAR(256),		/* Additional info for participant (not used) */
  SELPHONE INTEGER,				/* 0 = this phone, 1 = bus, 2 = mobile, 3 = home, 4 = other, -1 = default (SelectedPhone) */
  PHONE VARCHAR(256),			/* Participant phone number */
  SELEMAIL INTEGER,				/* 0 = this email, 1,2,3 = email 1,2,3, -1 = default (SelectedEmail) */
  EMAIL VARCHAR(256),			/* Participant email */
  SCREENNAME VARCHAR(64),		/* Screen name if known */
  AIMNAME VARCHAR(64),			/* not used */
  NOTIFYTYPE INTEGER,       	/* 0 = NONE, 1 = EMail, 2 = IIC, 4 = Phone, 8 = Reminder EMail, 16 = AIM (NotifyType)*/

  EXPIRES INTEGER,				/* When instant invitation expires */
  START_TIME INTEGER			/* Start time of meeting instance participant was invited to (for instant invitee) */
);

CREATE INDEX participants_index_1 ON participants(userid, type);
CREATE INDEX participants_index_2 ON participants(meetingid);
CREATE INDEX participants_index_3 ON participants(roll);

/* ----------------- start xdb_sql definitions -----------*/
/* See jabberd/xdb_sql/database_documentation.txt for more info */

CREATE INDEX I_users_login ON users (username);

/* not used */
DROP TABLE users0k CASCADE;
CREATE TABLE users0k (
  username VARCHAR(256) NOT NULL PRIMARY KEY,
  hash     VARCHAR(41) NOT NULL,
  token    VARCHAR(10) NOT NULL,
  sequence VARCHAR(3)  NOT NULL
);

DROP TABLE last CASCADE;
CREATE TABLE last (
  username VARCHAR(256) NOT NULL PRIMARY KEY,
  seconds  VARCHAR(32) NOT NULL,
  state	   VARCHAR(32)
);

/* User resource mappings */
/* not used */
DROP TABLE userres CASCADE;
CREATE TABLE userres (
  username VARCHAR(256) NOT NULL,
  resource VARCHAR(32) NOT NULL
);
CREATE UNIQUE INDEX PK_userres ON userres (username, resource);

/* Roster listing */
DROP TABLE rosterusers CASCADE;
CREATE TABLE rosterusers (
  username VARCHAR(256) NOT NULL,
  jid VARCHAR(255) NOT NULL,
  nick VARCHAR(255),
  subscription CHAR(1) NOT NULL,  /* 'N', 'T', 'F', or 'B' */
  ask CHAR(1) NOT NULL,           /* '-', 'S', or 'U' */
  server CHAR(1) NOT NULL,         /* 'Y' or 'N' */
  subscribe VARCHAR(10),
  type VARCHAR(64)
);
CREATE INDEX I_rosteru_username ON rosterusers (username);

/* not used */
DROP TABLE rostergroups CASCADE;
CREATE TABLE rostergroups
(
  username VARCHAR(256) NOT NULL,
  jid VARCHAR(255) NOT NULL,
  grp VARCHAR(64) NOT NULL
);
CREATE UNIQUE INDEX PK_rosterg_user_jid ON rostergroups (username, jid);

/* Spooled offline messages */
/* not used */
DROP TABLE spool CASCADE;
CREATE TABLE spool (
  username VARCHAR(256) NOT NULL,
  receiver VARCHAR(255) NOT NULL,
  sender VARCHAR(255) NOT NULL,
  id VARCHAR(255),
  date TIMESTAMP,
  priority INT,
  type VARCHAR(32),
  thread VARCHAR(255),
  subject VARCHAR(255),
  message TEXT,
  extension TEXT
);
CREATE INDEX I_despool on spool (username, date);

/* not used */
DROP TABLE filters CASCADE;
CREATE TABLE filters (
  username     VARCHAR(256),
  unavailable  VARCHAR(1),
  sender       VARCHAR(255),
  resource     VARCHAR(32),
  subject      VARCHAR(255),
  body         TEXT,
  show_state   VARCHAR(8),
  type         VARCHAR(8),
  offline      VARCHAR(1),
  forward      VARCHAR(32),
  reply        TEXT,
  continue     VARCHAR(1),
  settype      VARCHAR(8)
);

/* not used */
DROP TABLE vcard CASCADE;
CREATE TABLE vcard (
  username   VARCHAR(256) PRIMARY KEY,
  full_name  VARCHAR(65),
  first_name VARCHAR(32),
  last_name  VARCHAR(32),
  nick_name  VARCHAR(32),
  url        VARCHAR(255),
  address1   VARCHAR(255),
  address2   VARCHAR(255),
  locality   VARCHAR(32),
  region     VARCHAR(32),
  pcode      VARCHAR(32),
  country    VARCHAR(32),
  telephone  VARCHAR(32),
  email      VARCHAR(127),
  orgname    VARCHAR(32),
  orgunit    VARCHAR(32),
  title      VARCHAR(32),
  role       VARCHAR(32),
  b_day      DATE,
  descr      TEXT
);

/* not used */
DROP TABLE yahoo CASCADE;
CREATE TABLE yahoo (
  username   VARCHAR(256) PRIMARY KEY,
  id         VARCHAR(100) NOT NULL,
  pass       VARCHAR(32) NOT NULL
);

/* not used */
DROP TABLE icq CASCADE;
CREATE TABLE icq (
  username   VARCHAR(256) PRIMARY KEY,
  id         VARCHAR(100) NOT NULL,
  pass       VARCHAR(32) NOT NULL
);

/* not used */
DROP TABLE aim CASCADE;
CREATE TABLE aim (
  username   VARCHAR(256) PRIMARY KEY,
  id         VARCHAR(100) NOT NULL,
  pass       VARCHAR(32) NOT NULL
);

/* XXX : add tables for aim and icq roster */

alter table users0k add constraint users0k_user
    foreign key(username) references users(username) on delete cascade;

alter table last add constraint last_user
    foreign key(username) references users(username) on delete cascade;

alter table userres add constraint userres_user
    foreign key(username) references users(username) on delete cascade;

alter table rosterusers add constraint rosterusers_user
    foreign key(username) references users(username) on delete cascade;

alter table rosterusers add constraint rosterusers_jid
    foreign key(jid) references users(username) on delete cascade;

alter table rostergroups add constraint rostergroups_user
    foreign key(username) references users(username) on delete cascade;

alter table spool add constraint spool_user
    foreign key(username) references users(username) on delete cascade;

alter table filters add constraint filters_user
    foreign key(username) references users(username) on delete cascade;

alter table vcard add constraint vcard_user
    foreign key(username) references users(username) on delete cascade;

alter table yahoo add constraint yahoo_user
    foreign key(username) references users(username) on delete cascade;

alter table icq add constraint icq_user
    foreign key(username) references users(username) on delete cascade;

alter table aim add constraint aim_user
    foreign key(username) references users(username) on delete cascade;

/* Grant privileges to some users */
/*GRANT ALL ON users, users0k, last, userres, rosterusers, rostergroups, spool, filters, vcard, yahoo, icq, aim TO jabber;*/
GRANT ALL ON users, users0k, last, userres, rosterusers, rostergroups, spool, filters, vcard, yahoo, icq, aim
  TO "Administrator";

/* ----------------- end xdb_sql definitions -------------*/

/* ----------------- Set up initial system data ----------------- */

/* Insert internal community (ID=-1) to house anonymous user and special directories */
INSERT INTO COMMUNITIES
  (COMMUNITYID, COMMUNITYNAME, ACCOUNTNUMBER, CONTACTNAME, CONTACTEMAIL, CONTACTPHONE, ADMINNAME, ADMINPASSWORD, BRIDGEPHONE, SYSTEMURL, ACCOUNTTYPE, ACCOUNTSTATUS, OPTIONS)
   VALUES (-1, 'Anonymous', 'Anonymous', '', '', '', '', '', '', '', 2, 1, 0);

/* Insert primary system community (ID=1) */
INSERT INTO COMMUNITIES
  (COMMUNITYID, COMMUNITYNAME, ACCOUNTNUMBER, CONTACTNAME, CONTACTEMAIL, CONTACTPHONE, ADMINNAME, ADMINPASSWORD, BRIDGEPHONE, SYSTEMURL, ACCOUNTTYPE, ACCOUNTSTATUS, OPTIONS)
   VALUES (1, 'System', 'System', '', '', '', 'admin', 'admin', 'none', '@@@PROTOCOL_PREFIX@@@://@@@PORTAL_HNAME@@@/imidio/invite/load', 1, 1, 0);

/* Set up Anonymous user - id must be 1 */
INSERT INTO USERS (USERID, COMMUNITYID, USERNAME, SCREENNAME, PASSWORD, LASTMODIFIED, ADMIN, SUPERADMIN) VALUES
    (1, -1, 'anonymous@@@@EXTERNAL_HNAME@@@', 'anonymous', 'password', 0, false, false);

/* Set up system community administrator:
      - USER record
	  - GAL Directory,
	  - ALL Group
	  - Contact record
	  - Subscribe admin to GAL
	  - Add member record (place admin contact as member of GAL directory)

	  User id must be 2 for these sql statements to work correctly
*/
INSERT INTO USERS (USERID, COMMUNITYID, USERNAME, SCREENNAME, PASSWORD, LASTMODIFIED, ADMIN, SUPERADMIN) VALUES
    (2, 1, 'admin@@@@EXTERNAL_HNAME@@@', 'admin', '@@@ADMINPWD@@@', 0, true, true);
INSERT INTO DIRECTORIES (DIRECTORYID, COMMUNITYID, OWNERID, NAME, TYPE, LASTMODIFIED) VALUES
    (0, 1, 2, 'Community Address Book', 1, 0);
INSERT INTO GROUPS (GROUPID, DIRECTORYID, NAME, TYPE, LASTMODIFIED) VALUES
    (0, 0, 'All', 1, 0);
INSERT INTO CONTACTS
   (USERID,USERNAME,SERVER,FIRSTNAME,LASTNAME,
    DIRECTORYID,TYPE,SCREENNAME,
    MAXSIZE,MAXPRIORITY,PRIORITYTYPE,ENABLEDFEATURES,DISABLEDFEATURES,
    PROFILEID,LASTMODIFIED)
  VALUES
   (2,'admin@@@@EXTERNAL_HNAME@@@','@@@EXTERNAL_HNAME@@@','admin','',
    0,2,'admin',
	5, -1, 0, 0, 0,
	0, 0);
INSERT INTO DIRECTORY_SUBSCRIPTIONS (OWNERID, DIRECTORYID) VALUES (2, 0);
INSERT INTO MEMBERS (GROUPID, USERID) VALUES (0, 2);

/* Create directory and group for Outlook contacts */
INSERT INTO DIRECTORIES (OWNERID, COMMUNITYID, NAME, TYPE, LASTMODIFIED) VALUES
    (1, -1, 'Outlook', 2, 0);
/*INSERT INTO GROUPS (GROUPID, DIRECTORYID, NAME, TYPE, LASTMODIFIED) VALUES
    (1, 1, 'All', 1, 0);*/

/* Create directories for 3rd party IM systems */
INSERT INTO DIRECTORIES (OWNERID, COMMUNITYID, NAME, TYPE, LASTMODIFIED) VALUES
    (1, -1, 'Messenger', 5, 0);
INSERT INTO DIRECTORIES (OWNERID, COMMUNITYID, NAME, TYPE, LASTMODIFIED) VALUES
    (1, -1, 'Yahoo!', 7, 0);
INSERT INTO DIRECTORIES (OWNERID, COMMUNITYID, NAME, TYPE, LASTMODIFIED) VALUES
    (1, -1, 'AIM', 6, 0);

/* Insert default profile for system community */
INSERT INTO PROFILES (PROFILEID, COMMUNITYID, PROFILENAME, ENABLEDFEATURES, PROFILEOPTIONS) VALUES (0, '1', 'System', 507263, '000000000200000000100000000020');

/* Create instant meeting for admin */
INSERT INTO MEETINGS(MEETINGID,COMMUNITYID,HOSTID,TYPE,PRIVACY,PRIORITY,TITLE,SIZE,DURATION,TIME,RECURRENCE,RECUR_END,OPTIONS,END_TIME,INVITE_EXPIRATION,INVITE_VALID_BEFORE,INVITE_MESSAGE,DISPLAY_ATTENDEES,INVITEE_CHAT_OPTIONS) VALUES
	(13579,1,2,1,1,5,'Admin\'s Meeting',5,60,0,0,0,1,0,0,0,'',2,2);
INSERT INTO PARTICIPANTS(PARTICIPANTID, MEETINGID, ROLL, USERID, USERNAME, NAME, PHONE, EMAIL, SCREENNAME, NOTIFYTYPE, TYPE) VALUES
	(233445,13579,3,2,'admin@@@@EXTERNAL_HNAME@@@','admin','','','admin', 0, 0);

/* Insert first user community (ID=2) */
INSERT INTO COMMUNITIES
  (COMMUNITYID, COMMUNITYNAME, ACCOUNTNUMBER, CONTACTNAME, CONTACTEMAIL, CONTACTPHONE, ADMINNAME, ADMINPASSWORD, BRIDGEPHONE, SYSTEMURL, ACCOUNTTYPE, ACCOUNTSTATUS, OPTIONS)
   VALUES (2, 'Users', 'Users', '', '', '', 'useradmin', 'useradmin', 'none', '@@@PROTOCOL_PREFIX@@@://@@@PORTAL_HNAME@@@/imidio/invite/load', 1, 1, 0);

/* Set up first user administrator */
INSERT INTO USERS (USERID, COMMUNITYID, USERNAME, SCREENNAME, PASSWORD, LASTMODIFIED, ADMIN, SUPERADMIN) VALUES
    (3, 2, 'useradmin@@@@EXTERNAL_HNAME@@@', 'useradmin', '@@@ADMINPWD@@@', 0, true, false);
INSERT INTO DIRECTORIES (DIRECTORYID, COMMUNITYID, OWNERID, NAME, TYPE, LASTMODIFIED) VALUES
    (1, 2, 3, 'Community Address Book', 1, 0);
INSERT INTO GROUPS (GROUPID, DIRECTORYID, NAME, TYPE, LASTMODIFIED) VALUES
    (1, 1, 'All', 1, 0);
INSERT INTO CONTACTS
   (USERID,USERNAME,SERVER,FIRSTNAME,LASTNAME,
    DIRECTORYID,TYPE,SCREENNAME,
    MAXSIZE,MAXPRIORITY,PRIORITYTYPE,ENABLEDFEATURES,DISABLEDFEATURES,
    PROFILEID,LASTMODIFIED)
  VALUES
   (3,'useradmin@@@@EXTERNAL_HNAME@@@','@@@EXTERNAL_HNAME@@@','useradmin','',
    1,2,'useradmin',
	5, -1, 0, 0, 0,
	1, 0);
INSERT INTO DIRECTORY_SUBSCRIPTIONS (OWNERID, DIRECTORYID) VALUES (3, 1);
INSERT INTO MEMBERS (GROUPID, USERID) VALUES (1, 3);

/* Insert default profile for user community */
INSERT INTO PROFILES (PROFILEID, COMMUNITYID, PROFILENAME, ENABLEDFEATURES, PROFILEOPTIONS) VALUES (1, '2', 'System', 507263, '000000000200000000100000000020');

/* Create instant meeting for useradmin */
INSERT INTO MEETINGS(MEETINGID,COMMUNITYID,HOSTID,TYPE,PRIVACY,PRIORITY,TITLE,SIZE,DURATION,TIME,RECURRENCE,RECUR_END,OPTIONS,END_TIME,INVITE_EXPIRATION,INVITE_VALID_BEFORE,INVITE_MESSAGE,DISPLAY_ATTENDEES,INVITEE_CHAT_OPTIONS) VALUES
	(24680,2,3,1,1,5,'Admin\'s Meeting',5,60,0,0,0,1,0,0,0,'',2,2);
INSERT INTO PARTICIPANTS(PARTICIPANTID, MEETINGID, ROLL, USERID, USERNAME, NAME, PHONE, EMAIL, SCREENNAME, NOTIFYTYPE, TYPE) VALUES
	(344556,24680,3,3,'useradmin@@@@EXTERNAL_HNAME@@@','useradmin','','','useradmin', 0, 0);


