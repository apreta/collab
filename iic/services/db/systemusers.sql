/* Create the Jabber database */


/* Set up special system users (mainly so jabber can look them up when processing packets)
*/
INSERT INTO USERS (COMMUNITYID, USERNAME, SCREENNAME, PASSWORD, LASTMODIFIED, ADMIN, SUPERADMIN) VALUES
    (1, 'controller.service@all.homedns.org', 'controller.service', 'none', 0, false, false);
INSERT INTO USERS (COMMUNITYID, USERNAME, SCREENNAME, PASSWORD, LASTMODIFIED, ADMIN, SUPERADMIN) VALUES
    (1, 'controller2.service@all.homedns.org', 'controller.service', 'none', 0, false, false);
INSERT INTO USERS (COMMUNITYID, USERNAME, SCREENNAME, PASSWORD, LASTMODIFIED, ADMIN, SUPERADMIN) VALUES
    (1, 'mtgarchiver.service@all.homedns.org', 'mtgarchiver.service', 'none', 0, false, false);
INSERT INTO USERS (COMMUNITYID, USERNAME, SCREENNAME, PASSWORD, LASTMODIFIED, ADMIN, SUPERADMIN) VALUES
    (1, 'mtgarchiver2.service@all.homedns.org', 'mtgarchiver.service', 'none', 0, false, false);
INSERT INTO USERS (COMMUNITYID, USERNAME, SCREENNAME, PASSWORD, LASTMODIFIED, ADMIN, SUPERADMIN) VALUES
    (1, 'mtgarchiver3.service@all.homedns.org', 'mtgarchiver.service', 'none', 0, false, false);
INSERT INTO USERS (COMMUNITYID, USERNAME, SCREENNAME, PASSWORD, LASTMODIFIED, ADMIN, SUPERADMIN) VALUES
    (1, 'mtgarchiver4.service@all.homedns.org', 'mtgarchiver.service', 'none', 0, false, false);
INSERT INTO USERS (COMMUNITYID, USERNAME, SCREENNAME, PASSWORD, LASTMODIFIED, ADMIN, SUPERADMIN) VALUES
    (1, 'voice.service@all.homedns.org', 'voice.service', 'none', 0, false, false);
INSERT INTO USERS (COMMUNITYID, USERNAME, SCREENNAME, PASSWORD, LASTMODIFIED, ADMIN, SUPERADMIN) VALUES
    (1, 'voice2.service@all.homedns.org', 'voice.service', 'none', 0, false, false);
INSERT INTO USERS (COMMUNITYID, USERNAME, SCREENNAME, PASSWORD, LASTMODIFIED, ADMIN, SUPERADMIN) VALUES
    (1, 'voice3.service@all.homedns.org', 'voice.service', 'none', 0, false, false);
INSERT INTO USERS (COMMUNITYID, USERNAME, SCREENNAME, PASSWORD, LASTMODIFIED, ADMIN, SUPERADMIN) VALUES
    (1, 'location.service@all.homedns.org', 'location.service', 'none', 0, false, false);
INSERT INTO USERS (COMMUNITYID, USERNAME, SCREENNAME, PASSWORD, LASTMODIFIED, ADMIN, SUPERADMIN) VALUES
    (1, 'addressbk.service@all.homedns.org', 'addressbk.server', 'none', 0, false, false);
INSERT INTO USERS (COMMUNITYID, USERNAME, SCREENNAME, PASSWORD, LASTMODIFIED, ADMIN, SUPERADMIN) VALUES
    (1, 'mailer.service@all.homedns.org', 'mailer.service', 'none', 0, false, false);
INSERT INTO USERS (COMMUNITYID, USERNAME, SCREENNAME, PASSWORD, LASTMODIFIED, ADMIN, SUPERADMIN) VALUES
    (1, 'heartbeat-sm.service@all.homedns.org', 'heartbeat-sm.service', 'none', 0, false, false);
INSERT INTO USERS (COMMUNITYID, USERNAME, SCREENNAME, PASSWORD, LASTMODIFIED, ADMIN, SUPERADMIN) VALUES
    (1, 'heartbeat-service.service@all.homedns.org', 'heartbeat-service.service', 'none', 0, false, false);
