/*  Increase username columns to 256 */

update pg_attribute set atttypmod = 260 
where attrelid = ( select oid from pg_class where relname = 'users0k' ) 
and attname = 'username';

update pg_attribute set atttypmod = 260 
where attrelid = ( select oid from pg_class where relname = 'last' ) 
and attname = 'username';

update pg_attribute set atttypmod = 260 
where attrelid = ( select oid from pg_class where relname = 'userres' ) 
and attname = 'username';

update pg_attribute set atttypmod = 260 
where attrelid = ( select oid from pg_class where relname = 'rosterusers' ) 
and attname = 'username';

update pg_attribute set atttypmod = 260 
where attrelid = ( select oid from pg_class where relname = 'rostergroups' ) 
and attname = 'username';

update pg_attribute set atttypmod = 260 
where attrelid = ( select oid from pg_class where relname = 'spool' ) 
and attname = 'username';

update pg_attribute set atttypmod = 260 
where attrelid = ( select oid from pg_class where relname = 'filters' ) 
and attname = 'username';

update pg_attribute set atttypmod = 260 
where attrelid = ( select oid from pg_class where relname = 'vcard' ) 
and attname = 'username';

update pg_attribute set atttypmod = 260 
where attrelid = ( select oid from pg_class where relname = 'yahoo' ) 
and attname = 'username';

update pg_attribute set atttypmod = 260 
where attrelid = ( select oid from pg_class where relname = 'icq' ) 
and attname = 'username';

update pg_attribute set atttypmod = 260 
where attrelid = ( select oid from pg_class where relname = 'aim' ) 
and attname = 'username';

