/*  Changes to meeting definitions */

ALTER TABLE MEETINGSTATUS ADD COLUMN START_TIME INTEGER;
ALTER TABLE MEETINGSTATUS ADD COLUMN END_TIME INTEGER;

DELETE FROM MEETINGSTATUS;
