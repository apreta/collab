/* (c)2002 imidio all rights reserved */

// TODO:
//   Logging
//   Data type handling
//   Connection pooling
//   Threading
//   Set callbacks for notifications

#include <glib.h>
#include <stdio.h>
#include <time.h>
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>
#include <util.h>
#include "sqldb.h"
#include "sqlpg.h"

#include "../../jcomponent/util/nadutils.h"
#include "../../jcomponent/util/log.h"
#include "ithread.h"
#ifdef LINUX
#include <pthread.h>
#endif

//static pthread_mutex_t db_mutex = PTHREAD_MUTEX_INITIALIZER;
static IMutex db_mutex;


static char *i_itoa(int i, char *buffer)
{
    sprintf(buffer, "%d", i);
    return buffer;
}


ParamList::ParamList(int est_size)
{
  allocated = est_size;
  params = new const char *[est_size];
  in_use = 0;
}

ParamList::~ParamList()
{
    delete [] params;
}

void ParamList::resize(int size)
{
    if (size > allocated)
    {
        // Gotta copy.
        const char **newlist = new const char *[size];
        for (int i=0; i<in_use; i++)
            newlist[i] = params[i];
        delete [] params;
        params = newlist;
        allocated = size;
    }
}

void ParamList::add(const char *value)
{
    if (in_use >= allocated)
    {
        resize(in_use + 20);
    }
    params[in_use++] = value;
}

void ParamList::set(int pos, const char *value)
{
    if (pos >= allocated)
    {
        resize(pos+1);
        in_use = pos+1;
    }
    params[pos] = value;
}

DBManager *sqldb_get_manager(const char *config)
{
	DBManager *mgr = new DBManager();
    if (mgr->initialize(config))
    {
        delete mgr;
        return NULL;
    }
	return mgr;
}

DBManager::DBManager()
{
	initialized = false;
}

DBManager::~DBManager()
{
    // connections will get closed when container is destroyed
}
	
int DBManager::initialize(const char *config_file)
{
	if (initialized)
		return 0;
	
	nad_t nad;

	if (nad_load((char *)config_file, &nad))
    {
        log_error(ZONE, "no config loaded, aborting\n");
		return -1;
    }

    /* Read host, user, etc. */
	int root = get_root(nad, 1, "connection");
	if (root < 0)
	{
		log_error(ZONE, "unable to read database configuration\n");
		return -1;
	}
	
	host = get_child_value(nad, root, "host"); 
	port = get_child_value(nad, root, "port", "");
	db = get_child_value(nad, root, "db");
	user = get_child_value(nad, root, "user");
	password = get_child_value(nad, root, "password");

	/* Read SQL statements */
	root = nad_find_elem(nad, 0, "queries", 1);
	if (root > 0)
	{
		int def = nad_find_elem(nad, root, "querydef", 1);
		while (def > 0)
		{
			IString name = get_attr_value(nad, def, "name");
			IString sql = get_child_value(nad, def, "sql");
			
			QueryDef *qdef = new QueryDef(name, sql);
			queries.insert(new IString(name), qdef);

			// Read parameter list
			int param = nad_find_child_elem(nad, def, "bindparam", 1);
			while (param > 0)
			{
				// TODO: add datatype info to parameter (for now, assume strings)
                IString encode = get_attr_value(nad, param, "encode", "true");
				IString pval = get_value(nad, param);
				qdef->add_parameter(pval, encode == "true");
				param = nad_find_child_elem(nad, param, "bindparam", 0);
			}
			
			def = nad_find_elem(nad, def, "querydef", 0);
		}
	}
	
	nad_free(nad);

	initialized = true;
	return 0;
}

Connection *DBManager::create_connection()
{
    Connection *c;
    c = new PGConnection(this);
    int stat = c->connect(host, port, user, password, db);
    if (stat)
    {
        delete c;
        c = NULL;
    }

    return c;
}

// Simple "pooling" strategy:  one connection is created for every thread that
// uses this manager instance.  The presumption is that we're being called from
// a thread pool that limits the number of simultaneous connections.
Connection *DBManager::get_connection()
{
	int thread_id;

#ifdef LINUX
	thread_id = pthread_self();
#else // _WIN32
	thread_id = GetCurrentThreadId();
#endif

    IUnsigned index(thread_id);

	IUseMutex lock(db_mutex);
    Connection * c = (Connection *)connections.get(&index);
    if (c == NULL)
    {
        c = create_connection();
        if (c != NULL)
        {
            IUnsigned *pindex = new IUnsigned(thread_id);
            connections.insert(pindex, c);
        }
    }
    return c;
}

void DBManager::free_connection(Connection *connection)
{

}

void DBManager::check_for_sys_params(IString &sql)
{
    static IString curtime("$$CURTIME$$");
    
    int start = sql.find(curtime);
    if (start >= 0)
    {
        char buf2[32];
        i_itoa(time(NULL), buf2);
        sql.erase(start, curtime.length());
        sql.insert(start, buf2);
    }
}

// TODO: add data types to parameter lists and handle incoming params.
void DBManager::build_sql(IString &sql, Connection *conn, QueryDef *q, va_list &args)
{
	char buffer[2*MAX_FIELD_WIDTH];
	sql = q->get_sql();

	for (int i=0; i<q->get_parameter_count(); i++)
	{
		IString *p = q->get_parameter(i);

        char *param_val = va_arg(args, char*);

        if (q->get_encode(i))
        {
            conn->escape(param_val, buffer, sizeof(buffer));
            param_val = buffer;
        }
        
		int start = sql.find(*p);
		
		while (start >= 0)
		{
			sql.erase(start, p->length());
			sql.insert(start, param_val);
            
            start = sql.find(*p, start + 1);
		}
	}
    
    check_for_sys_params(sql);
}

void DBManager::build_sql(IString &sql, Connection *conn, QueryDef *q, ParamList *params)
{
	char buffer[2*MAX_FIELD_WIDTH];
	sql = q->get_sql();
    
	for (int i=0; i<q->get_parameter_count(); i++)
	{
		IString *p = q->get_parameter(i);

        const char *param_val = params->get(i);

        if (q->get_encode(i))
        {
            conn->escape(param_val, buffer, sizeof(buffer));
            param_val = buffer;
        }
        
		int start = sql.find(*p);
		
		while (start >= 0)
		{
			sql.erase(start, p->length());
			sql.insert(start, param_val);
            
            start = sql.find(*p, start + 1);
		}
	}
	
    check_for_sys_params(sql);
}

// TODO:  add format string and use to convert datatypes of parameters.
void DBManager::build_sql(IString& sql, Connection *conn, va_list &args)
{
	int pos = 0;
	
	while (pos >= 0)
	{
		pos = sql.find("$$", pos);
		if (pos >= 0)
		{
			int end = sql.find("$$", pos + 2);
			if (end < 0)
			{
				log_error(ZONE, "Unmatched parameter markers in SQL: %s\n", (const char*)sql);
				break;
			}
			
			sql.erase(pos, end-pos+2);
			sql.insert(pos, va_arg(args, char*));
			
			pos = end + 2;
		}
	}
    
    check_for_sys_params(sql);
}

// Execute SQL statement.  Parameters must be in same order as in SQL statement.
ResultSet *DBManager::execute_sql(const char *in_sql, ...)
{
	ResultSet *rs = NULL;
	va_list args;
	va_start(args, in_sql);
	
	Connection *conn = get_connection();
	if (conn == NULL)
	{
		log_error(ZONE, "Database connection not available\n");
		return NULL;
	}
	
	IString sql(in_sql);
    build_sql(sql, conn, args);

    log_debug(ZONE, "Executing: %s\n", (const char*)sql);
	
	rs = conn->execute(sql);
    if (rs->is_error())
    {
        log_error(ZONE, "SQL error: %s\n", rs->get_error());
        if (!conn->is_connected())
        {
            // attempt reconnect for next time
        	log_debug(ZONE, "Attempting reconnect\n");
            conn->reconnect();
        }
    }

    log_debug(ZONE, "Execution complete\n");

	free_connection(conn);
	
	va_end(args);
	return rs;
}

// Execute predefined SQL statement.  Parameters must be in order defined in
// querydef configuration
ResultSet *DBManager::execute(const char *operation, ...)
{
	ResultSet *rs = NULL;
	va_list args;
	va_start(args, operation);

	QueryDef *q = (QueryDef*)queries.get(operation);
	if (q == NULL)
	{
		log_error(ZONE, "Definition for operation %s not found\n", operation);
		return NULL;
	}
	
	Connection *conn = get_connection();
	if (conn == NULL)
	{
		log_error(ZONE, "Database connection not available\n");
		return NULL;
	}
	
	IString sql;
    build_sql(sql, conn, q, args);

	log_debug(ZONE, "Executing: %s\n", (const char*)sql);
	
	rs = conn->execute(sql);
    if (rs->is_error())
    {
        log_error(ZONE, "SQL error: %s\n", rs->get_error());
        if (!conn->is_connected())
        {
            // attempt reconnect for next time
        	log_debug(ZONE, "Attempting reconnect\n");
            conn->reconnect();
        }
    }

    log_debug(ZONE, "Execution complete\n");

	free_connection(conn);
	
	va_end(args);
	return rs;
}

// Execute predefined SQL statement.  Parameters must be in order defined in
// querydef configuration
ResultSet *DBManager::execute(const char *operation, ParamList *params)
{
	ResultSet *rs = NULL;

	QueryDef *q = (QueryDef*)queries.get(operation);
	if (q == NULL)
	{
		log_error(ZONE, "Definition for operation %s not found\n", operation);
		return NULL;
	}
	
	Connection *conn = get_connection();
	if (conn == NULL)
	{
		log_error(ZONE, "Database connection not available\n");
		return NULL;
	}
    
    if (params->size() < q->get_parameter_count())
    {
        log_error(ZONE, "Insufficient parameters supplied to operation %s\n", operation);
        return NULL;
    }
	
	IString sql;
    build_sql(sql, conn, q, params);
    log_debug(ZONE, "Executing: %s\n", (const char*)sql);
	
	rs = conn->execute(sql);
    if (rs->is_error())
    {
        log_error(ZONE, "SQL error: %s\n", rs->get_error());
        if (!conn->is_connected())
        {
            // attempt reconnect for next time
        	log_debug(ZONE, "Attempting reconnect\n");
            conn->reconnect();
        }
    }
	
	free_connection(conn);
	
	return rs;
}

char *DBManager::escape(const char *in, char *buff, int buff_len)
{
   Connection *c = get_connection();
   char *esc_str = c->escape(in, buff, buff_len);
   free_connection(c);
   return esc_str;
}

void DBManager::close()
{
	delete this;
}

void DBManager::closeResults(ResultSet *&rs)
{
    if (rs != NULL)
        delete rs;
    rs = NULL;
}
