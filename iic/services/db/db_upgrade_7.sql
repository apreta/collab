/*  Add Community table */

DROP SEQUENCE COMMUNITYID_SEQ;
CREATE SEQUENCE COMMUNITYID_SEQ;

/* Community table...allows multiple tenants in a single db. */
/* May be companies, etc.  Users in one community do not see */
/* data from another community.                              */
DROP TABLE COMMUNITIES CASCADE;
CREATE TABLE COMMUNITIES (
  COMMUNITYID INTEGER PRIMARY KEY DEFAULT nextval('COMMUNITYID_SEQ'),
  COMMUNITYNAME VARCHAR(256),   /* Displayed name for a community   */
  ACCOUNTNUMBER VARCHAR(32),    /* Community's account number       */
  CONTACTNAME VARCHAR(256),     /* Name of contact for a community  */
  CONTACTEMAIL VARCHAR(256),    /* Email of contact for a community */
  CONTACTPHONE VARCHAR(32),     /* Phone of contact for a community */
  ADMINNAME  VARCHAR(256),      /* Name of default admin that will be created for this community     */
  ADMINPASSWORD VARCHAR(128),   /* Password of default admin that will be created for this community */
  BRIDGEPHONE VARCHAR(32),      /* Phone number of the bridge that will service this community */
  SYSTEMURL VARCHAR(256),       /* URL that will service requests for this community */
  ACCOUNTTYPE INTEGER,          /* 0 = normal account, 1 = system account */
  ACCOUNTSTATUS INTEGER,        /* 0 = enabled, 1 = disabled (community users and admins cannot use the system) */
  OPTIONS INTEGER,              /* Bitmask for community options (enabled/disabled, etc. */
  LASTMODIFIED INTEGER
);

/* Insert primary system community (ID=1) */
INSERT INTO COMMUNITIES 
  (COMMUNITYNAME, ACCOUNTNUMBER, CONTACTNAME, CONTACTEMAIL, CONTACTPHONE, ADMINNAME, ADMINPASSWORD, BRIDGEPHONE, SYSTEMURL, ACCOUNTTYPE, ACCOUNTSTATUS, OPTIONS)
   VALUES ('System', 'System', '', '', '', 'admin', 'admin', '', '', 1, 0, 32769);

/*  Add SUPERADMIN field to USERS.  Also, update existing user records */

ALTER TABLE USERS ADD COLUMN SUPERADMIN BOOLEAN;

UPDATE USERS SET SUPERADMIN=false;

UPDATE USERS SET 
    SCREENNAME='admin', PASSWORD='admin', SUPERADMIN=true 
    WHERE USERID=0;

