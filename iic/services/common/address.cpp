/* (c)2003 imidio all rights reserved */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define ADDRESS_CPP

#include "common.h"
#include "address.h"
#include "../../util/istring.h"

// Return address type code from specified address
int get_address_type(const char * address)
{
	if (address == NULL)
		return -1;

    const char *p = strchr(address, ':');
    if (p == NULL)
        return -1;

    unsigned int len = (p - address);
    int i;

    for (i = 0; types[i] != NULL; i++)
    {
        if (strlen(types[i]) == len && !strncmp(address, types[i], len))
            break;
    }

    if (types[i] != NULL)
        return i;
    else
        return -1;
}

// Return media type.
// MediaData for IIC client, MediaVoice for IIC voice, MediaNone for others, -1 on error.
int get_media_type(const char * address)
{
    int addr_type = get_address_type(address);
    if (addr_type >= 0)
        return media_type_map[addr_type];
    else
        return -1;
}

int parse_address(const char * address, Address * addr)
{
	addr->id[0] = '\0';
	addr->service[0] = '\0';
	addr->type = -1;

    addr->type = get_address_type(address);
    if (addr->type < 0)
        return InvalidAddress;

    const char * id = strchr(address, ':');
    if (id == NULL)
        return InvalidAddress;
	id++;

    unsigned len = strlen(id);
    if (len > MAX_ADDR_ID_LEN)
        return InvalidAddress;

    strcpy(addr->id, id);

    const char * svc = strchr(id, '@');
    if (svc != NULL)
    {
		svc++;
        len = strlen(svc);
        if (len > MAX_SERVICE_ID_LEN)
            return InvalidAddress;

        strcpy(addr->service, svc);
    }
    else
        addr->service[0] = '\0';

    return Ok;
}

void make_id_string(IString & result, const char * prefix, const char * suffix)
{
	result = IString(prefix) + IString(suffix);
}

bool isdigits(const char *buffer)
{
    bool bIsDigits = buffer[0] != '\0';
    int  index = 0;

    while (buffer[index] != '\0' && bIsDigits)
    {
        if (!isdigit(buffer[index]))
        {
            bIsDigits = FALSE;
        }

        index++;
    }

    return bIsDigits;
}

