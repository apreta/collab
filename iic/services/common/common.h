/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

// API versions:
//   1 or unspecified (ICEcore version 1.0): original
//	 2 (1.0 SP1): Password passed to addressbook authneticate API

#ifndef COMMON_H
#define COMMON_H

#define MAX_PHONE_LEN 256
#define MAX_MESSAGE_SIZE_MESSENGER 400
#define MAX_MESSAGE_SIZE_AOL 7800
#define MAX_MESSAGE_SIZE_YAHOO 800

#define MIN_DATE 0
#define MAX_DATE 2147483647

#define AOL_SERVICE_KEY   "aim"
#define MSN_SERVICE_KEY   "msn"
#define YAHOO_SERVICE_KEY "yahoo"

// number of names we get back if screenname for new user is taken
#define MAX_ALTERNATIVE_NAMES 3

//
// Common status codes.
// 0 is success, >0 is success with info or non-permanent failure
// <0 is failure
//
#define IS_FAILURE(code) ((code) < 0)

#ifndef EXCLUDE_ERRORS
//
// Success codes are 0 or greater.
//
// Error codes are negative.
//   Server error codes may be -1000 and beyond.
//   Error codes between -1 and -999 are reserved for the client (see jclient.h)
//
//   Server error codes should not be changed,
//   as they may be exposed in public apis
//
enum iicStatusCodes {
    Ok = 0,
    Pending,
	Retry,		// Meeting start still pending, try later
	WaitingForModerator,

	// Common error codes
    Failed                = -1000,
	InvalidPIN            = -1001,
	InvalidSessionID      = -1002,
    InvalidMeetingID      = -1003,
    InvalidSubmeetingID   = -1004,
	InvalidUserID         = -1005,
	InvalidParticipantID  = -1006,
	InvalidServiceID      = -1007,
    DuplicateID           = -1008,
	InvalidParameter      = -1009,
	ParameterParseError   = -1010,
    DuplicateUserName     = -1011,
    PINAllocationError    = -1012,
    NoClientConnection    = -1013,
    PINExpired            = -1014,
    PINNotValidYet        = -1015,
    InvalidCommunityID    = -1016,

	// Error codes from controller
	InvalidAddress        = -2000,
    NotAuthorized         = -2001,
	NotMember             = -2002,
    InsufficientResources = -2003,
	NotLoaded             = -2004,
    AlreadyConnected      = -2005,
	NotConnected          = -2006,
	NotActive             = -2007,
	NoPhoneNumber         = -2008,
	InAnotherMeeting      = -2009,
	MeetingExists         = -2010,
	InvalidPassword       = -2011,
    Disconnected          = -2012,
    NotWhileLecture       = -2013,
	MeetingLocked         = -2014,
	PhoneNumberNotFound   = -2015,
	MeetingStartTimeout   = -2016,
	NoPort				  = -2017,
	MtgArchiverTimeout    = -2018,
	MeetParamNotFound     = -2019,

	// Error codes from voice service
	InvalidCallID              = -3000,
	NotConferenced             = -3001,
	MissingPIN                 = -3002,
	NoAvailResource            = -3003,

	// Error codes from address book/schedule
	DBAccessError              = -4000,
	AddressBookError           = -4001,
	DuplicateContact           = -4002,
	ProfileSystemDeleteError   = -4003,
	ProfileInUseError          = -4004,
	UnknownGroup               = -4005,
    CommunitySystemDeleteError = -4006,
    CommunityNotFound          = -4007,
    CommunityStatusNotFound    = -4008,
    CommunityDisabled          = -4009,
    CommunityNameNotUnique     = -4010,
    DirectoryNotFound          = -4011,
    ProfileNotFound            = -4012,
	DuplicateUsername          = -4013,
    MaxMeetingsExceeded        = -4014,
    ProfileNameNotUnique       = -4015,
    InstantMeetingNotFound     = -4016,
    UserNotFound               = -4017,
    MaxPresenceExceeded        = -4018,
    InvalidScreenName          = -4019,
    InvalidScreenPassword      = -4020,
    CommunityOwnerDeleteError  = -4021,
    GroupNotFound              = -4022,


	// Error from app share
	NoShareSession             = -5000,
	ShareFailed                = -5001,

};
#endif

enum CommunityAccountType {
	AccountTypeNormal = 0,      // Normal community
	AccountTypeSystem = 1,      // System community (for superadmins)
	AccountTypeAnonymous = 2    // Special community, contains anonymous user amd special directories (IM, LDAP, etc.)
};

enum CommunityAccountStatus {
	AccountStatusDisabled = 0,  // Users cannot use the community
	AccountStatusEnabled = 1    // User can use the community
};

enum UserType {
	UserTypeContact = 0,
	UserTypeUser    = 1,
	UserTypeAdmin   = 2
};

enum PingResult {
    PingResultOK = 1,
    PingResultInvalidSession = 2
};

enum UpgradeResult {
    UpgradeNotFound = 0,
    UpgradeMandatory = 1,
    UpgradeOptional = 2
};

// Call types are mutually exclusive, but specified as separate bits b/c exiting code expects to test for
// individual bits.
enum CallOptions {
	CallNormal = 0,				// Normal call
	CallDirect = 1,				// Callee is assumed to have joined meeting already so is not prompted
	CallSupervised = 2,			// Call is connected directly to conference; other participants can hear call progress
	CallTypeMask = 7,

	CallScreen = 8,
	CallMaster = 16,

	// Internal options
	CallOutbound = 1024,
	SkipUserLookup = 2048,
	CallPassword = 4096,
	CallBypassHold = 8192,
	CallPCPhone = 16384,
};

// Meeting option flags are stored in database so don't change existing definitions.
enum MeetingOptions {
	WaitForModerator         = 1,       // ** Not used (see continue without moderator)
	ScreenCallers            = 2,       // Record caller names
	LectureMode              = 4,       // Host & moderators may speak, normal participant just listen
	LockParticipants         = 8,       // Don't allow new particiants to join
	AutoTimeExtension        = 16,      // Extend meeting if runs long
	RequirePassword          = 32,      // Absence of password may be sufficient indication
    ContinueWithoutModerator = 64,      // Meeting can continue without a moderator present
    InviteNeverExpires       = 128,     // Meeting never expires...ingore Meeting tables expiration value
    Prefer1To1               = 256,     // Set the 1 To 1 call check in meeting setup dialog
    DefaultCallUsers         = 512,     // Set the Call Users check in meeting setup dialog
    DefaultSendEmail         = 1024,    // Set the Send EmailInvites check in meeting setup dialog
    DefaultSendIM            = 2048,    // Set the Send IM Invite check in meeting setup dialog
    DefaultModerator         = 4096,    // Set the Is Moderator check in meeting setup dialog
    InvitationVoiceOnly      = 8192,    // Meeting setup type set to Voice Only
    InvitationDataOnly       = 16384,   // Meeting setup type set to Data Only
    InviteAlwaysValidBeforeStart = 32768,  // Invites work any time prior to meeting, otherwise use specific value
	RecordingMeeting         = 65536,   // Voice is recording the meeting
	MuteJoinLeave            = 131072,  // Don't play entry/exit tones/prompts when participants enter/exit
    ModeratorMayEdit         = 262144,  // Moderator (in addition to creator) may edit a meeting
    InvitationPCPhone        = 524288,  // Include PC Phone instructions in invitations
    HoldMode                 = 1048576, // All calls in meeting are on hold
    IncludeDocsInRecording   = 2097152, // Upload documents are included in archive2097152 directory
    ChatMode                 = 4194304, // Meeting is hosting a chat-only session
};

enum SubMeetingOptions {
    AnnounceJoinExit = 1, // For lecture mode, the main submeeting shouldn't play tones/prompts.  also mtg option
		                  // to turn off these.
    RecordingVoice = 2,
    EnableLectureMode = 4,
    LockSubmeeting = 8,
    EnableHoldMode = 16,
    EnableChatMode = 32,
    EnableScreenCallers = 64,
};

enum MeetingUpdateType {
	UpdateMeetingOnly = 1,
	UpdateMeetingAndParticipants = 2,
};

enum MeetingStartOptions {
	StartNormal = 0,
	StartThirdParty = 1,        // Start meeting on behalf of host
	StartSync = 2,              // API waits for meeting to start before returning
};

enum MeetingType {
	TypeInstant = 1,
	TypeScheduled = 2,
};

enum MeetingPrivacy {
	PrivacyPublic = 0,          // Meeting is listed and anonymous callers can join
	PrivacyUnlisted = 1,        // Meeting is not listed but anonymous callers can join
	PrivacyPrivate = 2          // Meeting is not listed and only listed participants can join
};

enum MeetingDisplay {           // Controls what participants a non-moderator will see
	DisplayNone = 0,            // No participants displayed
	DisplayModerators = 1,      // Only moderators
	DisplayAll = 2              // All participants
};

enum MeetingChat {              // Controls what chat options a non-moderator will have
	ChatNone = 0,               // No chat
	ChatModerators = 1,         // Can chat to moderators
	ChatAll = 2                 // Can chat to all participants
};

enum MeetingStatus {
	StatusNotStarted = 0,
	StatusInProgress = 1,
	StatusCompleted = 2
};

enum SendEmailOptions {         // Control whether emails should be sent when update a meeting
	SendEmailAll = 0,           // Send emails to all meeting participants
	SendEmailNew = 1,           // Only send to new participants
	SendEmailNone = 2           // Don't send any emails
};

enum ParticipantType {
	ParticipantNormal = 0,		// Participant is scheduled invitee
	ParticipantInvitee = 1		// Participant is instant invitee
};

// Oops! I should learn how to spell
enum ParticipantRoll {
	RollNormal = 0,
	RollListener,                // Not used (was to be for lecture mode)
	RollModerator,
	RollHost
};

enum MuteType {
    MuteOff = 0,                // Mute is off
    MuteOn = 1,                 // Normal mute (mic off) or lecture mode
    MuteHold = 2,               // Hold mute (mic and speaker off, bridge may play hold music)
};

// For backwards compatibility, SelPhoneThis with no number is same as
// SelPhoneNone
enum SelectedPhone {
	SelPhoneNone = -2,
	SelPhoneDefault = -1,
	SelPhoneThis = 0,			// Use value passed to this API
	SelPhoneBus,
	SelPhoneHome,
	SelPhoneMobile,
	SelPhoneOther,
	SelPhonePC,
};

// For backwards compatibility, SelEmailThis with no email is same as
// SelEmailNone
enum SelectedEmail {
	SelEmailNone = -2,
	SelEmailDefault = -1,
	SelEmailThis = 0,			// Use value passed to this API
	SelEmail1,
	SelEmail2,
	SelEmail3
};

enum NotifyType {
	NotifyNone = 0,
	NotifyEmail = 1,            // Sent at schedule time
	NotifyIIC = 2,              // Invite at meeting start
	NotifyPhone = 4,            // Call at meeting start
	NotifyReminderEmail = 8,    // Send email at meeting start
	NotifyAIM = 16,             // Send AIM message at meeting start
	NotifyYahoo = 32,           // Send Yahoo message at meeting start
	NotifyMessenger = 64,       // Send Messenger message at meeting start
	NotifyIMAny = 128,          // Send IM via any available IM
	NotifyIMMask = 0x72
};

enum InviteType {
	InviteNormal = 0,
	InvitePCPhone,
	InvitePCPhoneDirect
};

enum EventTypes {
	MeetingPending,
	MeetingModified,
	MeetingEnded,
	MeetingJoined,
	MeetingLeft,
	MeetingInvite,
	SubmeetingAdded,
	SubmeetingRemoved,
	ParticipantAdded,
	ParticipantRemoved,
	ParticipantMoved,
	ParticipantJoined,
	ParticipantLeft,
	ParticipantState,
	UserState,
	ConnectionState,
	AppShareStarted,
	AppShareEnded,
	PresenterGranted,
	PresenterRevoked,
	ControlGranted,
	ControlRevoked,
	Error,
	MeetingStarted,
	MeetingReset,
	MeetingChat,
	DocShareStarted,
	DocShareEnded,
	DocShareDownloadPage,
	DocShareConfigure,
	DocShareDrawing,
	PPTAction,
	RecordingNotAvailable,
	MeetingWaiting,
	RecordingEnabled,
	RecordingDisabled,
	MeetingInviteCancel,
};

enum SendTo {
    ToOneParticipant,
    ToAllParticipants,
    ToNonModeratorParticipants,
    ToModeratorParticipants,
    ToHostParticipant
};

enum SendType {
    SendEventOnly,
    SendMeetingState,
    SendMeetingInfo,
    SendParticipantState,
	SendSubmeetingState,
	SendInvite,
	SendAppShare,
    SendMeetingChat,
	SendDocShare,
	SendPPTShare,
};

// Current participant status for reporting
enum StatusFlags {
	StatusOffline = 0,
	StatusData = 1,
	StatusVoice = 2,
	StatusAppShare = 4,
	StatusInMeeting = 8,
	StatusPresenter = 32,
	StatusControl = 64,		// Participant can control share session
	StatusHandUp = 128,
	StatusTalking = 256,
	StatusSelf = 512,		// Only set when sent to single participant

	StatusDialing = 1024,
	StatusRinging = 2048,
	StatusBusy = 4096,
	StatusAnswered = 8192,
	StatusSpecialInfo = 16384,
	StatusIMSent = 32768,
    StatusTelephonyError = 65536,
    StatusRemoteHangup = 131072,
    StatusBridgeRequest = 262144,
    StatusBroadcast = 524288,
};

// Special invite expiration values.
// Stored in db so must not change!
// Must be negative!  (positive indications a time value)
enum InviteExpirationTypes {
    InviteExpiresImmediately = -1,  // Invite is only good while specific meeting is running!
};

// Jabber screen name for external user
#define ANONYMOUS_PASSWORD "vwee44tregwewefvll4442l22nfdnd"
#define ANONYMOUS_NAME "anonymous"
#define ANONYMOUS_NAME_LEN 9

// distinguished user ids
// these must match the user ids assigned in db table Users
#define ANONYMOUS_USER_ID "1"
#define ANONYMOUS_COMMUNITY_ID "1"

// Directory information
#define DEFAULT_GLOBAL_DIRECTORY "Community Address Book"
#define DEFAULT_PERSONAL_DIRECTORY "Personal"

enum DirectoryType
{
    dtGlobal = 1,
    dtOutlook = 2,      // deprecated
    dtPersonal = 3,
    dtLDAP = 4,         // deprecated
    dtMessenger = 5,    // deprecated
    dtAOL = 6,          // deprecated
    dtYahoo = 7,        // deprecated
    dtSearch = 8,
    dtUser = 9,
    dtMaxDirTypes

    // the deprecated types may be referred to in directory subscriptions in old Zon installs.
};

// Group Information
#define DEFAULT_GROUP_NAME "All"
#define DEFAULT_BUDDY_GROUP_NAME "Buddies"

#define GROUPTYPE_ALL "1"
#define GROUPTYPE_BUDDY "2"

enum GroupType
{
    dtAll = 1,
    dtBuddies,
    dtOther
};

// Profile flag for system privileges
enum ProfileFlags {
	ProfileNone                      = 0,
	ProfileUserSavePassword          = 1,          // bit 0  - Allow passwords to be saved
	ProfileUserUpdateGAL             = 2,          // bit 1  - Allow user to update their own GAL entry - obsolete, all users allowed to update own gal entry
	ProfileUserDefinedScreen         = 4,          // bit 2  - Allow user to change their own screen name
	ProfileUserIICIM                 = 8,          // bit 3  - Allow use of IIC IM and Chat
	ProfileCallAllowOutdialing       = 16,         // bit 4  - Allow the user to call
	ProfileCallScheduleOutdialing    = 32,         // bit 5  - Obsolete - Allow "Schedule Outdialing"
	ProfileCallMe                    = 64,         // bit 6  - Enable Call Me feature
	ProfileCallRestrictExternal      = 128,        // bit 7  - Obsolete - Restrict external calls, only allow internal dialing
	ProfileMeetingAutoTimeExtend     = 256,        // bit 8  - Allow Automatic Time Extension for meetings
	ProfileMeetingRequirePassword    = 512,        // bit 9  - Require meetings to have a password
	ProfileMeetingNoModeratorNeeded  = 1024,       // bit 10 - Allow meetings to continue without a Moderator
	ProfileMeetingPublic             = 2048,       // bit 11 - Allow Public Meetings
	ProfileMeetingDataShare          = 4096,       // bit 12 - Allow Data Sharing
	ProfileCallAllowTwoParty         = 8192,       // bit 13 - Allow Two Party Calls
    ProfileRequestProjectCode        = 16384,      // bit 14 - Eequest project code at meeting start
    ProfileReserved15                = 32768,      // bit 15 -
    ProfileReserved16                = 65536,      // bit 16 -
    ProfileReserved17                = 131072,     // bit 17 -
    ProfileReserved18                = 262144,     // bit 18 -
    ProfileReserved19                = 524288,     // bit 19 -
	ProfileDisableMeetingDownload    = 1048576,    // bit 20 -
	ProfileDisableCommunityDownload  = 2097152,    // bit 21 -
    ProfileDisableShareDesktop       = 4194304,    // bit 22 -
    ProfileDisableShareApplication   = 8388608,    // bit 23 -
    ProfileDisableSharePowerpoint    = 16777216,   // bit 24 -
    ProfileDisableShareDocument      = 33554432,   // bit 25 -
    ProfileDisableShareWhiteboard    = 67108864,   // bit 26 -
    ProfileDisableShareRemoteControl = 134217728,  // bit 27 -
    ProfileReserved28                = 268435456,  // bit 28 -
    ProfileReserved29                = 536870912,  // bit 29 -
    ProfileReserved30                = 1073741824, // bit 30 -
};

enum IICServiceType {
    ServiceTypeVoice = 1,
    ServiceTypeAppShare = 2,
    ServiceTypeDocShare = 4,
};

#define SYSTEM_PROFILEID "0"
#define CONTACT_PROFILEID "-1"

// Default Profile Values - be sure to keep in sync with ProfileFlags above, and expected ProfileOptions
//#define DEFAULT_PROFILE_FLAGS "15743"
#define DEFAULT_PROFILE_FLAGS "1031551"
#define DEFAULT_PROFILE_OPTIONS "000000000200000000100000000010"

#ifdef __cplusplus
#define PROFILE_ALLOWTWOPARTY_DEFAULT true
#else
#define PROFILE_ALLOWTWOPARTY_DEFAULT TRUE
#endif

#define PROFILE_MAXVOICE_DEFAULT 10
#define PROFILE_MAXVOICE_MIN 0
#define PROFILE_MAXVOICE_MAX 10000 // OK not really - what's reasonable?

#define PROFILE_MAXDATA_DEFAULT 10
#define PROFILE_MAXDATA_MIN 0
#define PROFILE_MAXDATA_MAX 10000  // OK not really - what's reasonable?

#define PROFILE_PRESENCE_DEFAULT 300
#define PROFILE_PRESENCE_MIN 0
#define PROFILE_PRESENCE_MAX 500

#define PROFILE_PINLENGTH_DEFAULT 7
#define PROFILE_PINLENGTH_MIN 3
#define PROFILE_PINLENGTH_MAX 16

#define PROFILE_MEETINGIDLENGTH_DEFAULT 6
#define PROFILE_MEETINGIDLENGTH_MIN 3
#define PROFILE_MEETINGIDLENGTH_MAX 16

#define PROFILE_PASSWORDLENGTH_DEFAULT 5
#define PROFILE_PASSWORDLENGTH_MIN 3
#define PROFILE_PASSWORDLENGTH_MAX 10

// this should be profile setting
#define PROFILE_SCHEDULED_MEETINGS_DEFAULT 20
#define PROFILE_SCHEDULED_MEETINGS_MIN 0
#define PROFILE_SCHEDULED_MEETINGS_MAX 1000

#define PROFILE_IDLE_MINUTES_DEFAULT 30
#define PROFILE_IDLE_MINUTES_MIN 0
#define PROFILE_IDLE_MINUTES_MAX 6000

#define PROFILE_MAX_CONNECTION_MINUTES_DEFAULT 600
#define PROFILE_MAX_CONNECTION_MINUTES_MIN 0
#define PROFILE_MAX_CONNECTION_MINUTES_MAX 6000

// unique name for the IIC main class
// - used to locate the client executable
#define IIC_MAIN_CLASS_NAME "IICMainClass"

// unique title for Login dialog.
// - this must match the title for CDialogLogin
// - used to locate the client login dialog
#define CLIENT_LOGIN_TITLE_STR "-- Sign On --"

enum IICShareOptions
{
	ShowMeetingWindow	= 0x1,
	NormalScreen		= 0x2,
	ShareDocument		= 0x4,
	SharePPT			= 0xB,		// 8|2|1
	SharePPTnoEraser	    = 0x10,
	ShareAutoDetect		= 0x20
};

enum RecordingOptions {
    PreviewRecording = 1,       // Produce a movie based on current recording spool data w/o deleting them, so that a complete movie can be generated later
    ResetRecording = 2,         // Wipe out prevoius recording
};

enum ForumOptions {
	ForumNone                     = 0,
	ForumScheduleMeeting          = 1,
	ForumReserved1                = 2,
	ForumReserved2                = 4,
	ForumReserved3                = 8,
	ForumReserved4                = 16,
	ForumReserved5                = 32,
	ForumReserved6                = 64,
	ForumReserved7                = 128,
	ForumReserved8                = 256,
	ForumReserved9                = 512
};

enum TelephonyResult {
	TelephonyAvailable	= 1,
	VOIPAvailable = 2,
	VOIPEncrypted = 4
};

#endif

