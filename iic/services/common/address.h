/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef ADDRESS_H
#define ADDRESS_H

//
// Address identifiers include type, unique identifier, and optionally the service
// identifier.
//
// For example:
//
// iic:mike@host        IIC client, JID for user
// user:1234            User, db identifier
// anon:john_smith      Generated for external user
// voice:12             Caller on channel 12
//

#define MAX_ADDR_ID_LEN 400
#define MAX_SERVICE_ID_LEN 200

#ifdef __cplusplus
class IString;
#endif

enum AddressType {
	AddressIIC,
	AddressVoice,
	AddressEmail,
    AddressMeeting,
    AddressUser,
    AddressAnonUser,
	AddressPartUser,
    AddressPhone,
    AddressAIM,
	AddressPIN,
    AddressMtgArc,
	AddressMax = AddressMtgArc
};

enum MediaType {
	MediaAny = -1,
    MediaNone = 0,
    MediaVoice,
    MediaData,
    MediaAppShare,
    MediaMax = MediaAppShare
};

#ifdef ADDRESS_CPP

static const char * types[] = {
        "iic", "voice", "email", "meeting", "user", "anon", "part",
        "phone", "aim", "pin", "mtgarc", NULL
};

static const int media_type_map[] = {

        MediaData, MediaVoice, MediaNone, MediaNone, MediaNone, MediaNone, MediaNone,
        MediaNone, MediaNone, MediaNone, MediaNone
};

#endif


struct Address
{
    int type;
    char id[MAX_ADDR_ID_LEN];
    char service[MAX_SERVICE_ID_LEN];
};


int get_address_type(const char * address);

int get_media_type(const char * address);

int parse_address(const char * address, struct Address * addr);

#ifdef __cplusplus
void make_id_string(IString &result, const char * prefix, const char * suffix);

bool isdigits(const char *buffer);
#endif

#endif
