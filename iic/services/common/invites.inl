/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include "common.h"
#include "datetime.h"
//#include "strings.h"
#include "../../util/base64/Base64.h"
#include "../../util/base64/MIMECode.h"

#ifdef WIN32
    #include "../../util/utf8_utils.h"
#endif

#ifdef USE_CSTRING

#define IICSTRING CStringCS
#define VALIDSECTION "IM"

//
//  CStringCS (Client/Server string compatibility
//
class CStringCS: public CString
{
	public:
		CStringCS(void) {};
		virtual ~CStringCS() {};
		CStringCS& operator=(const CString& s) {CString::operator=(s); return *this;};

        int find(const char *str, int start=0) const {return this->Find(CUNICODEString(str), start);};
        void clear() {this->Empty();};
        void erase(int start, int len) {this->Delete(start, len);};
        void insert(int pos, const char *str, int len) {this->Insert(pos, (LPCTSTR)CUNICODEString(str));};
        CStringCS& append(const CString& s) {CString::operator+=(s); return *this;};
        const TCHAR *get_string() {return *this;};
};

#else
#define IICSTRING IString
#define VALIDSECTION "EMAIL"
#define CUTF8String(x) x
#define CUNICODEString(x) x
#endif

char *g_keyword_echo = "echo ";
char g_keyword_section[] = "#SECTION ";
char g_keyword_section_valid[] = VALIDSECTION;
char g_keyword_section_end[] = "#SECTION END";
char g_keyword_condition[] = "%IF ";
char g_keyword_condition_end[] = "%IF END";
char g_keyword_condition_not[] = "!";
char g_keyword_condition_callin[] = "CALL_IN";
char g_keyword_condition_callout[] = "CALL_OUT";
char g_keyword_condition_callpcphone[] = "CALL_PCPHONE";
char g_keyword_condition_description[] = "DESCRIPTION";
char g_keyword_condition_message[] = "MESSAGE";
char g_keyword_condition_password[] = "PASSWORD";
char g_keyword_condition_private[] = "PRIVATE";

char IICTITLE[]                = "<TITLE>";
char IICHOST[]                 = "<HOST>";
char IICTIME[]                 = "<TIME>";
char IICDESCRIPTION[]          = "<DESCRIPTION>";
char IICMESSAGE[]              = "<MESSAGE>";
char IICINVITEENAME[]          = "<INVITEE-NAME>";
char IICINVITEEPHONE[]         = "<INVITEE-PHONE>";
char IICINVITEEPIN[]           = "<INVITEE-PIN>";
char IICMEETINGURL[]           = "<MEETING-URL>";
char IICMEETINGURLANONYMOUS[]  = "<ANONYMOUS-URL>";
char IICMEETINGPHONE[]         = "<MEETING-PHONE>";
char IICMEETINGPIN[]           = "<ANONYMOUS-PIN>";
char IICMEETINGPASSWORD[]      = "<PASSWORD>";
char IICSERVER[]               = "<SERVER>";
char IICNEWLINE[]              = "<NEWLINE>";

char HTMLNEWLINE[]             = "<BR>";
char TEXTNEWLINE[]             = "\r\n";
char ICALNEWLINE[]             = "\n";
char INTERNALNEWLINE[]         = "<CR>"; // Used in Description and Invite Message

//
// internal_substitute_iic_token
//
// searches for the passed token_name in the passed token.
// if found, replaces token_name with the passed substitution and stores the value in
// the passed string.
//
bool internal_substitute_iic_token(IICSTRING &token, const char* token_name, const char *substitution)
{
    int pos;
    int lastPos = -1;
    bool found = false;

    while (((pos = token.find(token_name)) != -1) && (pos != lastPos))
    {
        lastPos = -1;
        token.erase(pos, strlen(token_name));
        token.insert(pos, substitution, strlen(substitution));

        found = true;
    }

    return found;
}

//
// internal_is_echo
//
int internal_is_echo(const char* token)
{
    int past_echo_pos = -1;
    const char* iicpos;

    if ((iicpos = strstr(token, g_keyword_echo)) != NULL)
    {
        past_echo_pos = iicpos - token + strlen(g_keyword_echo);
    }

    return past_echo_pos;
}

//
// internal_is_keyword
//
bool internal_is_keyword(const char* token, const char *keyword)
{
    const char* iicpos = strstr(token, keyword);
    return (iicpos != NULL);
}

//
// internal_check_condition
//
bool internal_check_condition(const char *token,
                              bool bCallIn,
                              bool bCallOut,
                              bool bDescription,
                              bool bMessage,
                              bool bPassword,
                              bool bPrivate,
                              bool bCallPCPhone)
{
    bool expected_result = !internal_is_keyword(token, g_keyword_condition_not);

    return ((internal_is_keyword(token, g_keyword_condition_callin) && bCallIn == expected_result) ||
            (internal_is_keyword(token, g_keyword_condition_callout) && bCallOut == expected_result) ||
            (internal_is_keyword(token, g_keyword_condition_callpcphone) && bCallPCPhone == expected_result) ||
            (internal_is_keyword(token, g_keyword_condition_description) && bDescription  == expected_result) ||
            (internal_is_keyword(token, g_keyword_condition_message) && bMessage == expected_result) ||
            (internal_is_keyword(token, g_keyword_condition_password) && bPassword == expected_result) ||
            (internal_is_keyword(token, g_keyword_condition_private) && bPrivate == expected_result));
}

//
// internal_get_invite_email
//
// retrieves the new user email in the passed string, substituting in the passed
// parameters
//
bool internal_get_invite_email(IICSTRING &aEmailInvite,
                               char* template_buffer,
                               const char* meetingid,
                               const char* meetingtitle,
                               const char* hostname,
                               const char* participantid,
                               const char* participantname,
                               const char* meetingdescription,
                               const char* invitemessage,
                               const char* meetingpassword,
                               const char* participantphone,
                               const char* bridgephone,
                               const char* systemurl,
                               const char* servername,
                               int meetingoptions,
                               int type,
                               int privacy,
                               int scheduledtime,
                               bool usehtml,
                               bool scheduling = false,
			       const char* dateformat = NULL,
			       bool forical = false)
{
    char newline[32];
    sprintf(newline, "%s", usehtml ? HTMLNEWLINE : (forical ? ICALNEWLINE : TEXTNEWLINE));

    bool bCallIn = !(meetingoptions & InvitationDataOnly) && strlen(participantphone) == 0;
    bool bCallOut = !(meetingoptions & InvitationDataOnly) && strlen(participantphone) != 0;
    bool bCallPCPhone = !(meetingoptions & InvitationDataOnly) && (meetingoptions & InvitationPCPhone);
    bool bDescription = strlen(meetingdescription) != 0;
    bool bMessage = strlen(invitemessage) != 0;
    bool bPassword = meetingoptions & RequirePassword && strlen(meetingpassword) > 0;
    bool bPrivate = privacy == PrivacyPrivate;

    if (strcmp(bridgephone, "none") == 0)
    {
        bCallIn = bCallOut = false;
    }

    char meetingurl[4096];
    char anonymousurl[4096];
    char meetingtime[256];

    if (scheduling && scheduledtime == 0)
    {
        sprintf(meetingtime, "%s",  _("None specified"));
    }
    else
    {
        date_time_decode(scheduledtime, meetingtime, dateformat);
    }

    if (template_buffer != NULL)
    {
        // construct URLs
        char participant_urlargs[2048];
        char anonymous_urlargs[2048];
        sprintf(participant_urlargs, "m_id=%s&p_id=%s", meetingid, participantid);
        sprintf(anonymous_urlargs, "m_id=%s&p_id=", meetingid);

        CBase64 participant_encoder;
        const char* participant_encodedargs = participant_encoder.Encode(participant_urlargs, strlen(participant_urlargs));
        sprintf(meetingurl, "%s?mid=%s", systemurl, participant_encodedargs);

        CBase64 anonymous_encoder;
        const char* anonymous_encodedargs = anonymous_encoder.Encode(anonymous_urlargs, strlen(anonymous_urlargs));
        sprintf(anonymousurl, "%s?mid=%s", systemurl, anonymous_encodedargs);

        // If using html, we need to convert the message
        // and description's newline characters...
        IICSTRING iMessage;
        iMessage = CUNICODEString(invitemessage);
        IICSTRING iDescription;
        iDescription = CUNICODEString(meetingdescription);

        if (usehtml)
        {
            internal_substitute_iic_token(iMessage, INTERNALNEWLINE, HTMLNEWLINE);
            internal_substitute_iic_token(iDescription, INTERNALNEWLINE, HTMLNEWLINE);
        }
        else
        {
            internal_substitute_iic_token(iMessage, INTERNALNEWLINE, TEXTNEWLINE);
            internal_substitute_iic_token(iDescription, INTERNALNEWLINE, TEXTNEWLINE);
        }

        //
        // Process email template...
        //
        int maxsize = 4096;
        char* token = new char[maxsize+1];
        char* copybuf = template_buffer;

        // parse buffer line by line
        char* pdest;
        int size;

        bool bSkippingSection = false;
        bool bSkippingCondition = false;
        int condition_depth = 0;
        int echo_position;
        char* echo_token;

        while ((pdest = strchr(copybuf, '\n')) != NULL)
        {
            // get a line of input
            size = pdest - copybuf;
            if (size > maxsize)
            {
                delete[] token;
                token = new char[size+1];
                maxsize = size;
            }
            // copy the next line, without the trailing '\n'
            strncpy(token, copybuf, size);
            token[size] = 0;
            copybuf += size + 1;

            if (!bSkippingSection && internal_is_keyword(token, g_keyword_section))
            {
                bSkippingSection = !internal_is_keyword(token, g_keyword_section_valid);
            }
            else if (!internal_is_keyword(token, g_keyword_condition_end) &&
                      internal_is_keyword(token, g_keyword_condition))
            {
                if (bSkippingCondition)
                {
                    condition_depth++;
                }
                else
                {
                    bSkippingCondition = !internal_check_condition(token, bCallIn, bCallOut, bDescription, bMessage, bPassword, bPrivate, bCallPCPhone);
                }
            }

            if (!bSkippingSection &&
                !bSkippingCondition &&
                (echo_position = internal_is_echo(token)) != -1)
            {
                echo_token = token + echo_position;
                IICSTRING itoken;
                itoken = echo_token;

                internal_substitute_iic_token(itoken, TEXTNEWLINE, "");
                internal_substitute_iic_token(itoken, IICNEWLINE, newline);
                internal_substitute_iic_token(itoken, IICTITLE, meetingtitle);
                internal_substitute_iic_token(itoken, IICHOST, hostname);
                internal_substitute_iic_token(itoken, IICTIME, meetingtime);
                internal_substitute_iic_token(itoken, IICDESCRIPTION, CUTF8String(iDescription.get_string()));
                internal_substitute_iic_token(itoken, IICMESSAGE, CUTF8String(iMessage.get_string()));
                internal_substitute_iic_token(itoken, IICINVITEENAME, participantname);
                internal_substitute_iic_token(itoken, IICINVITEEPHONE, participantphone);
                internal_substitute_iic_token(itoken, IICINVITEEPIN, participantid);
                internal_substitute_iic_token(itoken, IICMEETINGURL, meetingurl);
                internal_substitute_iic_token(itoken, IICMEETINGURLANONYMOUS, anonymousurl);
                internal_substitute_iic_token(itoken, IICMEETINGPHONE, bridgephone);
                internal_substitute_iic_token(itoken, IICMEETINGPIN, meetingid);
                internal_substitute_iic_token(itoken, IICMEETINGPASSWORD, meetingpassword);
                internal_substitute_iic_token(itoken, IICSERVER, servername);

                aEmailInvite.append(itoken.get_string());
            }

            if (bSkippingSection && internal_is_keyword(token, g_keyword_section_end))
            {
                bSkippingSection = false;
            }
            else if (bSkippingCondition && internal_is_keyword(token, g_keyword_condition_end))
            {
                if (condition_depth > 0)
                {
                    condition_depth--;
                }
                else
                {
                    bSkippingCondition = false;
                }
            }
        }

        if (token != NULL)
            delete[] token;

        return true;
    }
    return false;
}

