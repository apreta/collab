/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef SHARE_COMMON_H
#define SHARE_COMMON_H

/* Simple control protocol:

   HEADER         "CNTRL####"
   MESSAGE        <repeats>

   MESSAGE format:

   MSGLEN         <4 bytes>
   MSGDATA        <msglen bytes>

   MSGDATA format:

   CMD            <4 bytes>
   CMDARGS        <msglen - 4 bytes>

*/

// share--reflector protocol
#define HEADER "CNTRL001"
#define HEADER_LEN 8
//#define MAX_MESSAGE 512
//#define MAX_STRING 128

// Max share servers in a single install
#define MAX_SHARE_SERVERS 32


enum ShareCommands
{
    CmdNoop = 0,
    CmdStartSession,
    CmdEndSession,
    CmdChangePresenter,
    CmdParticipantJoined,
    CmdParticipantLeft,
    CmdRejectParticipant,
    CmdEnableRemoteControl,
	CmdEnableRecording,
	CmdSessionActive,
    CmdMax
};

enum ShareEvents
{
    EventAppShareStarted,
    EventAppShareEnded,
    EventDocShareStarted,
    EventDocShareEnded,
    EventPresenterChanged,
    EventParticipantJoined,
    EventParticipantLeft,
    EventRemoveControlChanged,
    EventRecordingChanged,
	EventPPTShareStarted,
	EventPPTShareEnded,
    EventMax
};

enum ShareEndReasons
{
    ReasonNormal = 0,
    ReasonInvalidSession = 1,
    ReasonDisconnected = 2,
    ReasonChangePresenter = 3,
	ReasonWrongShareMode = 4,
};

#ifndef NO_SHAREMGR_ERRORS

enum ShareMgrErrors
{
	/* Fault codes from Share Mgr API */
    Ok = 0,
    Failed = -10000,
    InvalidSession = -10001,
    InvalidMeetingId = -10002,
    InvalidParticipantId = -10003,
    NoShareSession = -10004,
    InvalidParameter = -10005,
    ParameterParseError = -10006,
    RecordingAudioError = -10007,
    InvalidServerName = -10008,
    ArchiveError = -10009,
    DocAccessError = -10010,
    RecordingAccessError = -10011,
    SessionMeetingMismatch = -10012,
    MeetingExists = -10013,
    WrongServer = -10014,
    NoReflector = -10015,
    NoRecorder = -10016,

	/* Internal errors */
	InvalidClient = -20000,
	ParticipantExists = -20001,
	WrongShareMode = -20002,
};

#endif

#define FAILED(stat)	(stat < 0)


enum ShareModes
{
    ShareModeMin = 0,
    ShareModeNone = 0,
    ShareModeApplication = 1,
    ShareModeDocument = 2,
    ShareModePPT = 3,
    ShareModeMax = 4
};

enum ShareRoles
{
	ShareRolePresenter = 1,
	ShareRoleViewer = 2
};

#endif
