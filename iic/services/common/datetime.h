/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef _DATE_TIME_H_
#define _DATE_TIME_H_

#include <time.h>
//#include "strings.h"

#define DATETIMESIZE 256

#ifndef LINUX
#define snprintf _snprintf
#define _(string) string
#endif

// utility method for converting to string
// NOTE: assumes that locale and timezone are set correctly with 
// setlocale() and tzset().
void date_time_decode(int a_time, char* result, const char* format = NULL)
{
    bool time_converted = false;
    bool time_is_now = false;
    time_t time_value = (time_t)a_time;
    char tmpbuf[DATETIMESIZE];

    if (a_time == 0)
    {
        time_is_now = true;
        time(&time_value);
    }

#ifdef LINUX
	struct tm timebuf;
    struct tm *local = localtime_r(&time_value, &timebuf);
#else
    struct tm *local = localtime(&time_value);
#endif

    if (local != NULL)
    {
        if (format == NULL)
            format = "%b %d, %Y %#I:%M %p %Z";

        int num_bytes = strftime(tmpbuf, DATETIMESIZE, format, local);

        if (num_bytes > 0)
        {
            time_converted = true;
        }
    }
	
    if (time_converted == true)
    {
        if (time_is_now)
        {
            snprintf(result, DATETIMESIZE, _("Now (%s)"), tmpbuf);
        }
        else
        {
            strncpy(result, tmpbuf, DATETIMESIZE);
        }
    }
    else
    {
        strcpy(result, "Unknown");
    }
}

#endif
