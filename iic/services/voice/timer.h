/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef TIMER_H
#define TIMER_H

#include "../../util/iobject.h"

#define TIMER_INTERVAL 100  // 100 ms

class IChannel;

class ChannelTimer : public IObject
{
#ifdef LINUX
    static IList running;
#endif

    IChannel * channel;
    int channel_id;

    int remaining;
    bool is_active;

	int timer_type;

#ifdef _WIN32
	HANDLE timer;
#endif

public:
    ChannelTimer(IChannel * chan, int id);
	~ChannelTimer();

    void set_timeout(int type, int ms);
    void cancel();
    void cancel_in_handler();

private:

#ifdef LINUX
    static void block();
    static void unblock();
    static bool is_running();
    static void set_timer(int ms);
    static void handler(int sig);
#else //_WIN32
	static void __stdcall timer_proc(LPVOID arg, DWORD dwTimerLowValue, DWORD dwTimerHighValue);
#endif

public:
    enum { TYPE_CTIMER = 8331 };
    virtual int type()
        { return TYPE_CTIMER; }
    virtual unsigned int hash()
        { return channel_id; }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && channel_id == ((ChannelTimer*)obj)->channel_id; }

};

#endif
