
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>

#include "stdio.h"
#include "stdlib.h"
#include "voiceapi.h"

void acSleep(int);


extern "C" void rpc_add_method (char *method_name,
				     xmlrpc_method method,
				     void *user_data)
{
}

extern "C" int rpc_make_call(const char *id, const char *jid, const char *method, const char *format, ...)
{
    return 0;
}


class TestApp : public IApplication
{
    IConference * conf;
    
public:
    void run();

    virtual void on_initialized();
    virtual void on_new_call(ICall * call);
    virtual void on_event(IEvent * event);
    virtual void on_error(IChannel * chan, int code, const char * msg);

};

TestApp app;
IProvider * provider;

void TestApp::on_initialized()
{
    printf("Initialized event received\n");
}

void TestApp::on_new_call(ICall * call)
{
    printf("Call received\n");
    call->answer();

    if (conf == NULL)
        provider->create_conference(4, conf);
}

void TestApp::on_event(IEvent * event)
{
    switch (event->event)
    {
        case EventConnected:
        {
            printf("%s call is connected\n", event->reason == ConnectedInbound ? "Inbound" : "Outbound");
            if (event->reason == ConnectedInbound || ((int)event->channel->get_user()) < 100)
            {
                IString name("SayName.wav");
                event->channel->play(name, TermDTMFAny);
                event->channel->set_user((void *)1);
            }
            else
            {
                printf("Bridging direct calls\n");
                IChannel * orig = (IChannel *)event->channel->get_user();
                orig->connect(ConnectPSTN, event->channel, ConnectPSTN, true);

                orig->set_user(NULL);
                event->channel->set_user(NULL);
            }
            break;
        }
        case EventDisconnected:
        {
            printf("Caller hung up\n");
            int state = (int)event->channel->get_user();
            if (state == 5)
            {
                conf->remove(event->channel);
            }
            else if (state > 100)
            {
                ICall * outcall = (ICall*)state;
                outcall->disconnect();
                event->channel->set_user(NULL);
            }
            break;
        }
        case EventTerminated:
        {
            int state = (int)event->channel->get_user();

            printf("API complete: %d\n", event->reason);

            char filename[128];
            sprintf(filename, "record%d.wav", event->channel->get_id());

            if (state == 1)
            {
                event->channel->record(filename, true, 4000, 2500, 400000, TermDTMFAny);
                event->channel->set_user((void *)3);
            }
            if (state == 2)
            {
                int rc = event->channel->play(filename, TermDTMFAny);
                if (rc != OK)
                    state = 3;
                event->channel->set_user((void *)3);
            }
            if (state == 3)
            {
                event->channel->clear_digits();
                event->channel->entry("EnterMeetingNumber.wav", 12, 4000, 2500, TermDTMFPound);
                event->channel->set_user((void *)4);
            }
            if (state == 4)
            {
                bool supervised = false;
                char buff[32];
                event->channel->get_digits(buff, 32);
                printf("Digits: %s\n", buff);
                if (strlen(buff) >= 2)
                {
                    if (buff[0] == '9')
                    {
                        if (buff[strlen(buff) - 1] == '#')
                            buff[strlen(buff) - 1] = '\0';
                        ICall * call;
                        provider->create_call(buff + 1, call);
                        call->get_channel()->set_user((void*)10);
                    }
                    else if (buff[0] == '8')
                    {
                        ICall * call;
                        provider->create_call(buff + 1, call);
                        call->get_channel()->connect(ConnectPSTN, event->channel, ConnectPSTN, false);
                        event->channel->set_user(call);
                        call->get_channel()->set_user(event->channel);
                        supervised = true;
                    }
                }

                // We'll actually need to queue up these conf entries
                if (!supervised)
                {
                    int num = conf->get_number_participants();
                    for (int i = 0; i<num; i++)
                    {
                        IChannel * chan = conf->get_participant(i);
                        event->channel->connect(ConnectDSP, chan, ConnectPSTN, false);
                    }

                    if (num)
                    {
                        int rc = event->channel->play(filename, TermNone);
                        if (rc != OK)
                            state = 5;
                    }
                    else
                    {
                        state = 5;
                    }
                    event->channel->set_user((void *)5);
                }

            }
            if (state == 5)
            {
                printf("Connecting to conference\n");
                int num = conf->get_number_participants();
                event->channel->reset();
                for (int i = 0; i<num; i++)
                {
                    IChannel * chan = conf->get_participant(i);
                    chan->reset();
                }
                
                if (!(event->channel->get_last_terminators() & TermDisconnect))
                {
                    conf->add(event->channel);
                }
            }

            break;
        }
    }
}

void TestApp::on_error(IChannel * chan, int code, const char * msg)
{
    printf("Error from channel %d (%s)\n", (int)chan, msg);
}

void TestApp::run()
{


}

int main()
{
    printf("Starting test\n");
    provider = voice_get_provider("AudioCodes", app);
    provider->initialize("");

    getchar();

    provider->shutdown();

    return 0;
}
