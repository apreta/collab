/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

/* Implementation for voice service APIs */

#ifdef _WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "voice.h"
#include "voiceapi.h"
#include "../../jcomponent/rpc.h"
#include "../../jcomponent/util/rpcresult.h"
#include "../../jcomponent/util/log.h"

#define USE_CONF_VOL

extern Voice voice;
extern IProvider * provider;

//
// Call
//

Call::Call(const char * id, ICall * _call, const char * _user_id, const char * _username)
    : CallBase(id), user_id(_user_id), username(_username)
{
    call = _call;
    submeeting = NULL;
	ringcount = 0;
    entry_attempts = 0;
    cur_participant = -1;
    options = 0;
    state = -1;
	screened = false;
    moderator = false;
    hold = false;
    mute   = -1;
    volume = -1;
    gain   = -1;
    roll_call_prompt[0] = '\0';
}

Call::~Call()
{
	log_debug(ZONE, "Deleted call %s", get_id());
	if (call)
	{
		disconnect();
	}
}

int Call::disconnect()
{
	if (call)
	{
		// Call object will be deleted when it's released.
		call->disconnect();
	}

	return Ok;
}

void Call::on_release()
{
	if (submeeting)
		submeeting->remove_call(this);

	if (call)
	{
		call->get_channel()->close();
		call = NULL;
	}
}

void Call::set_submeeting(SubMeeting *sm)	
{ 
	submeeting = sm; 
}

IChannel * Call::get_channel()
{
    if (call == NULL)
        return NULL;

    // Make sure channel is still associated with this call.
    if (call->get_channel() != NULL && call->get_channel()->get_current_call() != call)
        return NULL;
        
    return call->get_channel();
}

bool Call::next_roll_call_participant()
{
    IChannel * req_chan = call->get_channel();
    IConference * conf = req_chan->get_conference();
    if (conf == NULL)
    {
        log_debug(ZONE, "No next roll call participant");

        roll_call_prompt[0] = '\0';
        return false;
    }
    
    IChannel * next_chan = NULL;
    do {
        ++cur_participant;
        next_chan = conf->get_participant(cur_participant);
    } while (next_chan != NULL && next_chan == req_chan /* skip self */);
    
    if (next_chan)
    {
        sprintf(roll_call_prompt, "record%d.wav", next_chan->get_id());
        
        log_debug(ZONE, "Next roll call participant prompt %s", roll_call_prompt);
        return true;
    }
    else
    {
        log_debug(ZONE, "No next roll call participant");
        roll_call_prompt[0] = '\0';
        
        return false;
    }
}

//
// Submeeting
// Corresponds to voice conference.
//

SubMeeting::SubMeeting(const char * id, Meeting * _owner, int _expected_participants, bool _main, const char *_recording_id)
        : SubMeetingBase(id), meeting(_owner), expected_participants(_expected_participants), main(_main), recording_id(_recording_id)
{
    conference = NULL;
	state = MeetingStateWaiting;
    options = AnnounceJoinExit;
}

SubMeeting::~SubMeeting()
{
	log_debug(ZONE, "Deleted submeeting %s", get_id());
	close();
}

int SubMeeting::create()
{
    int status = Ok;

    if (conference == NULL)
    {
        status = provider->create_conference(expected_participants,
                                             conference,
                                             main,
                                             (const char *)recording_id);

    }

    return status;
}

void SubMeeting::close()
{
	state = MeetingStateClosed;
	
	// Remove all calls first
	IListIterator iter(calls);
	
	Call * c = (Call*) iter.get_first();
	while (c != NULL)
	{
		remove_call(c);
		
		c = (Call*) iter.get_first();
	}

	iter.set_list(waiting_calls);

	c = (Call*) iter.get_first();
	while (c != NULL)
	{
		remove_call(c);
		
		c = (Call*) iter.get_first();
	}

	iter.set_list(outbound_calls);

	c = (Call*) iter.get_first();
	while (c != NULL)
	{
		remove_call(c);
		
		c = (Call*) iter.get_first();
	}

	// Call objects themselves deleted when released by voice API

    if (conference != NULL)
    {
		// This will remove calls from conference.
        conference->close();
		delete conference;
        conference = NULL;
	}

	log_debug(ZONE, "Closed submeeting %s", get_id());
}

void SubMeeting::reset()
{
	state = MeetingStateClosed;

    // Active calls are re-reported...
    	
	IListIterator iter(calls);
	
	Call * c = (Call*) iter.get_first();
	while (c != NULL)
	{
        remove_call(c, false);

        // Reset call options flags--since we've got 'em on the phone already, treat as supervised outbound
        // (should skip inbound prompts such as password and name recording).
        c->set_options((c->get_options() & ~(CallTypeMask)) | CallSupervised | CallOutbound);
        voice.report_call(c);
		
		c = (Call*) iter.get_first();
	}

    // Waiting calls are closed...
    
	iter.set_list(waiting_calls);

	c = (Call*) iter.get_first();
	while (c != NULL)
	{
		remove_call(c);
		
		c = (Call*) iter.get_first();
	}
    
    // Outbound calls are closed...
    
	iter.set_list(outbound_calls);

	c = (Call*) iter.get_first();
	while (c != NULL)
	{
		remove_call(c);
		
		c = (Call*) iter.get_first();
	}

	// Call objects themselves deleted when released by voice API

    if (conference != NULL)
    {
		// This will remove calls from conference.
        conference->close();
		delete conference;
        conference = NULL;
	}

	log_debug(ZONE, "Reset submeeting %s", get_id());
}

// Add call to outbound call list.
int SubMeeting::add_outbound_call(Call *p)
{
	int status = Ok;
	
    if (p == NULL)
        return InvalidCallID;

	if (state == MeetingStateClosed)
		return remove_call(p);

	p->set_submeeting(this);
	if (outbound_calls.find(p) == NULL)
		outbound_calls.add(p);

	// Flag as outbound call
	p->set_options(p->get_options() | CallOutbound);

	log_debug(ZONE, "Added outbound call %s to submeeting %s", p->get_id(), get_id());

	return status;
}

// Add call to active call list.
int SubMeeting::add_call(Call *p)
{
	int status = Ok;
	
    if (p == NULL)
        return InvalidCallID;

	if (state == MeetingStateClosed)
		return remove_call(p);

	p->set_submeeting(this);
	if (calls.find(p) == NULL)
		calls.add(p);

	// May have been queued or outbound.
	waiting_calls.remove(p);
	outbound_calls.remove(p);
	
	log_debug(ZONE, "Added call %s to submeeting %s", p->get_id(), get_id());

    return status;
}

// Connect call to conference.
int SubMeeting::connect_call(Call * p)
{
    IChannel *chan = p->get_channel();
	int status = Ok;

    if (p == NULL)
        return InvalidCallID;

	if (state == MeetingStateClosed)
		return remove_call(p);

    bool was_created = false;
	if (conference == NULL)
	{
		status = create();
		if (status)
			return status;

        was_created = true;
        
        conference->set_announce_entry_exit(options & AnnounceJoinExit);
	}

	// Add call to conference
	p->set_state(AppStateConference);
    chan->grant_moderator(p->is_moderator());
    status = conference->add(chan);
    if (status != OK)
    {
        log_error(ZONE, "Failed to connect %s to submeeting %s", p->get_id(), get_id());
        return status;
    }
    
    int v, g, m;
    p->get_volume(v, g, m);
    conference->set_volume(chan, v, g);
    if (m != -1)
        conference->set_mute(chan, m);
    
    if (was_created && (options & RecordingVoice))
    {
        conference->start_recording();
    }

	log_debug(ZONE, "Connected call %s to submeeting %s", p->get_id(), get_id());
    
	return status;
}

// Add call to holding queue.
int SubMeeting::queue_call(Call * p)
{
	int status = Ok;
	
    if (p == NULL)
        return InvalidCallID;

	if (state == MeetingStateClosed)
		return remove_call(p);

	// May have been outbound.
	outbound_calls.remove(p);

	// Set call in holding state.
	p->set_state(AppStateHolding);

	p->set_submeeting(this);
	if (waiting_calls.find(p) == NULL)
		waiting_calls.add(p);

	log_debug(ZONE, "Queued call %s in submeeting %s", p->get_id(), get_id());

    return status;
}

// Remove call from any list, and from voice conference.
int SubMeeting::remove_call(spCall p, bool prompt)
{
	// Remove call from conference...will be ignored if we never actually made it into conference
	calls.remove(p);
	waiting_calls.remove(p);
	outbound_calls.remove(p);

	p->set_submeeting(NULL);

    IChannel *chan = p->get_channel();
    if (p == NULL)
	{
		log_debug(ZONE, "Removing call for invalid channel");
        return InvalidCallID;
	}

	// Stop any active operation.
	if (chan != NULL)
		chan->stop();
    
	if (conference != NULL)
	{
        if (chan != NULL)
            conference->remove(chan);

        
        // only delete an empty conference if it has no recorder
		if (get_num_connected() == 0 && !conference->has_recorder())
        {
            conference->close();
            delete conference;
            conference = NULL;
        }
	}

	// May not be best place for this...
	// Will disconnect call when prompt completes
	if (p->get_call() && prompt)
		voice.handle_state(p, AppStateEndingPrompt);

	log_debug(ZONE, "Removed call %s from submeeting %s", p->get_id(), get_id());

    return Ok;
}

int SubMeeting::get_num_connected()
{
	return calls.size();
}

int SubMeeting::get_num_queued()
{
	return waiting_calls.size();
}

// Release one waiting call and start announcement.
void SubMeeting::release_first()
{
	Call * c = (Call*) waiting_calls.remove_head();
	if (c != NULL)
	{
		IChannel * channel = c->get_channel();
		char filename[128];
		sprintf(filename, "record%d.wav", channel->get_id());
		
		add_call(c);
		play_announcement(c, filename);
	}
}

// Complete connections for holding calls.
void SubMeeting::release_holding()
{
	IListIterator iter(waiting_calls);
	
	Call * c = (Call*) iter.get_first();
	while (c != NULL)
	{
		add_call(c);
		connect_call(c);
		
		c = (Call*) iter.get_first(); // because calls are removed from waiting list by add_call
	}
}

bool SubMeeting::is_call_conferenced(Call * p)
{
	return (calls.find(p) != NULL);
}

// Play announcement to all channels in conference, using channel
// from surrogate call.
int SubMeeting::play_announcement(Call * c, const char * filename)
{
    IChannel *play_chan = c->get_channel();

	if (conference == NULL || play_chan == NULL)
		return Failed;

	// Call already in submeeting, connect to conference.
	if (is_call_conferenced(c))
		connect_call(c);

    if (conference->play_announcement(play_chan, filename) != OK)
    {
        on_announcement_end(c);
        
        return Failed;
    }
    
	log_debug(ZONE, "Playing announcement on submeeting %s for call %s", get_id(), c->get_id());

    state = MeetingStateAnnouncing;
    c->set_state(AppStateAnnounce);
    
	return Ok;
}

// Restore conference.
void SubMeeting::on_announcement_end(Call * c)
{
	if (conference == NULL)
		return;

	log_debug(ZONE, "Reconnecting calls to submeeting %s", get_id());

	state = MeetingStateActive;
}

int SubMeeting::set_volume(Call * call, int volume, int gain, int mute)
{
	log_debug(ZONE, "Setting volume (vol=%d, gain=%d, mute=%d) for call %s",
              volume, gain, mute, call->get_id());

    call->set_volume(volume, gain, mute);
    
	if (conference == NULL)
    {
		return Ok;
    }

	IChannel * chan = call->get_channel();

#ifdef USE_CONF_VOL
	int s1 = conference->set_volume(chan, volume, gain);
#else
	int s1 = chan->set_volume(volume, gain);
#endif

	int s2 = mute >= 0 ? conference->set_mute(chan, mute) : Ok;

	return s1 != Ok ? s1 : s2;
}

int SubMeeting::get_volume(Call * call, int & out_volume, int & out_gain, int & out_mute)
{
	if (conference == NULL)
    {
        call->get_volume(out_volume, out_gain, out_mute);
        
		return NotConferenced;
    }

	log_debug(ZONE, "Getting volume for call %s", call->get_id());

	IChannel * chan = call->get_channel();

#ifdef USE_CONF_VOL
	int s1 = conference->get_volume(chan, out_volume, out_gain);
#else
	int s1 = chan->get_volume(out_volume, out_gain);
#endif

	int s2 = conference->get_mute(chan, out_mute);

	return s1 != Ok ? s1 : s2;
}

int SubMeeting::grant_moderator(Call * call, int grant)
{
	call->set_is_moderator(grant != 0);

	if (conference == NULL)
		return NotConferenced;

	IChannel * chan = call->get_channel();
    if (chan)
    {
        chan->grant_moderator(call->is_moderator());
    }

    return Ok;
}

Call * SubMeeting::find_call(const char * id)
{
	CallBase find(id);
	
	Call * c = (Call*)calls.find(&find);
	return c;
}

Call * SubMeeting::find_waiting_call(const char * id)
{
	CallBase find(id);
	
	Call * c = (Call*)waiting_calls.find(&find);
	return c;
}

void SubMeeting::set_options(unsigned _options)
{
    unsigned changed_options = options ^ _options;

    options = _options;
    
    if (changed_options & AnnounceJoinExit)
    {
        if (conference != NULL)
        {
            conference->set_announce_entry_exit(options & AnnounceJoinExit);
        }
    }

    if ((changed_options & RecordingVoice) && conference != NULL)
    {
        if (options & RecordingVoice)
        {
            conference->start_recording();
        }
        else
        {
            conference->stop_recording();
        }
    }
}

void SubMeeting::reset_recording()
{
    if (conference != NULL)
        conference->restart_recording();
}

//
// Meeting
// A collection of related submeetings.
//

Meeting::Meeting(const char * id, int _expected_participants, int _priority, int _flags, int _start_time)
        : meeting_id(id), expected_participants(_expected_participants), priority(_priority), flags(_flags), start_time(_start_time)
{
	closed = false;
}

Meeting::~Meeting()
{
	log_debug(ZONE, "Deleted meeting %s", get_id());
	close();
}

void Meeting::close()
{
    IListIterator iter(submeetings);
    SubMeeting * s = (SubMeeting *) iter.get_first();
    while (s != NULL)
    {
        s->close();
        s = (SubMeeting *) iter.get_next();
    }

	closed = true;

	// Now delete.
	submeetings.remove_all();
}

void Meeting::reset()
{
    IListIterator iter(submeetings);
    SubMeeting * s = (SubMeeting *) iter.get_first();
    while (s != NULL)
    {
        s->reset();
        s = (SubMeeting *) iter.get_next();
    }
    
    submeetings.remove_all();    
}

int Meeting::register_submeeting(const char * id, int expected_participants, bool main)
{
    SubMeeting * s = find_submeeting(id);
    if (s == NULL)
    {
        char recording_id[40];
        strcpy(recording_id, (const char *)meeting_id);
        strcat(recording_id, "-");
        struct tm *tm_start = localtime((time_t *)&start_time);
        strftime(&recording_id[strlen(recording_id)], 20, "%Y-%m-%d-%H-%M-%S", tm_start);

        s = new SubMeeting(id, this, expected_participants, main, recording_id);
        submeetings.add(s);
		s->dereference();
		return Ok;
    }

    return DuplicateID;
}

int Meeting::modify_submeeting(const char * id, unsigned options)
{
    SubMeeting * s = find_submeeting(id);
    if (s != NULL)
    {
        s->set_options(options);
        
        return Ok;
    }

    return InvalidSubmeetingID;
}

int Meeting::reset_recording(const char * id)
{
    SubMeeting * s = find_submeeting(id);
    if (s != NULL)
    {
        s->reset_recording();
        
        return Ok;
    }

    return InvalidSubmeetingID;
}

int Meeting::close_submeeting(const char * id)
{
    SubMeeting * s = find_submeeting(id);
    if (s != NULL)
    {
        s->close();
        submeetings.remove(s);
		return Ok;
    }

    return InvalidSubmeetingID;
}

SubMeeting * Meeting::find_submeeting(const char * id)
{
    SubMeetingBase temp(id);
    SubMeeting * sm = (SubMeeting*)submeetings.find(&temp);
    return sm;
}

SubMeeting * Meeting::find_mainmeeting()
{
    IListIterator iter(submeetings);

    SubMeeting * m = (SubMeeting *) iter.get_first();
    while (m != NULL)
    {
        if (m->is_main())
            break;
        m = (SubMeeting *) iter.get_next();
    }

    return m;
}


//
// Voice interface
//

/*
	Meeting entry notes.
	
	Holding callers:
	  Record name
	  Play holding for host message
	  <-- wait for meeting start -->
	  Play meeting starting message
	  <-- attach -->
	  Play names of all present on start (roll call)
	
	First host/moderator joining:
	  Record name
	  Play meeting starting message
	  <-- attach -->
	  Play roll call if others holding
	  
	Subsequent callers joining:
	  Record name
	  Play joining meeting
	  Announce caller's name
	  <-- attach -->
	
	May just require host to request roll call rather
	then provide automatically.
*/

int Voice::anon_id_seq = 900000;
int Voice::call_id_seq = 0;

Voice::Voice()
{
}

void Voice::set_service_name(const char *name)
{
	service_name = name;
}

int Voice::set_parameter(IString & param, IString & value)
{
	log_status(ZONE, "Setting parameter %s to %s", (const char *)param, (const char *)value);

	if (param == "DelayAfterDial")
		delay_after_dial = atoi(value);
	if (param == "JoinMeetingTimeout")
		join_meeting_timeout = atoi(value);

	return Ok;
}

// Controller has called reset API, and we've detected that controller has been restarted.
int Voice::reset()
{
    IVector meeting_list;
    
    meetings.values_of(meeting_list);
    
    IVectorIterator iter(meeting_list);
    Meeting * m = (Meeting *)iter.get_first();
    while (m != NULL)
    {   
        m->reset();
        m = (Meeting *)iter.get_next();
    }

    meetings.remove_all();
        
	return Ok;
}

int Voice::register_meeting(const char * id, int participants, int priority, int start_time)
{
    Meeting * m = find_meeting(id);
	if (m != NULL)
	{
        // Meeting already exists, leave it as it is...
        return Ok;
	}
	
	m = new Meeting(id, participants, priority, 0, start_time);

	IString * key = new IString(id);
	meetings.insert(key, m);

	m->dereference();

	return Ok;
}

int Voice::close_meeting(const char * id)
{
    Meeting * m = find_meeting(id);
    if (m != NULL)
    {
        m->close();

		IString key(id);
        meetings.remove(&key);

        return Ok;
    }

    return InvalidMeetingID;
}

int Voice::register_submeeting(const char * meeting, const char * sub_id, int expected_participants, bool main)
{
    Meeting * m = find_meeting(meeting);
    if (m != NULL)
    {
        int status = m->register_submeeting(sub_id, expected_participants, main);

        return status;
    }

    return InvalidSubmeetingID;
}

int Voice::modify_submeeting(const char * meeting, const char * sub_id, int options)
{
    Meeting * m = find_meeting(meeting);
    if (m != NULL)
    {
        int status = m->modify_submeeting(sub_id, (unsigned)options);

        return status;
    }
    
    return InvalidSubmeetingID;
}

int Voice::reset_recording(const char * meeting, const char * sub_id)
{
    Meeting * m = find_meeting(meeting);
    if (m != NULL)
    {
        int status = m->reset_recording(sub_id);

        return status;
    }
    
    return InvalidSubmeetingID;
}

int Voice::close_submeeting(const char * meeting, const char * sub_id)
{
    Meeting * m = find_meeting(meeting);
    if (m != NULL)
    {
        int status = m->close_submeeting(sub_id);

        return status;
    }

    return InvalidSubmeetingID;
}

// Hold means the call is held until a non-held call is connected, at which point all
// calls are connected to the conference.
int Voice::connect_call(const char * meeting, const char *sub_id, const char *call_id, bool hold, bool grant_moderator, bool mute)
{
    Meeting * m = find_meeting(meeting);
    if (m == NULL)
        return InvalidMeetingID;

    SubMeeting * s = m->find_submeeting(sub_id);
    if (s == NULL)
        return InvalidSubmeetingID;

    Call * c = find_call(call_id);
    if (c == NULL)
        return InvalidCallID;

    if (s->is_call_conferenced(c))
    {
        // sometimes the controller can command us to
        // connect a call more than once.  Protect against
        // it here.
		log_debug(ZONE, "Call %s already in conference", call_id);
        //return Ok;
    }
    
	// Save info
	c->set_hold(hold);
	c->set_meetingid(meeting);
	c->set_submeetingid(sub_id);
    c->set_is_moderator(grant_moderator);
    c->set_mute(mute);
    
	if (c->get_options() & CallSupervised)
	{
		// No prompt, just connect call right to conference
		s->add_call(c);
		handle_state(c, AppStateConference);
	}
	else if ((c->get_options() & CallScreen) && !c->get_screened())
	{
		handle_state(c, AppStateRecordNamePrompt);
	}
	else if (hold)
	{
		s->queue_call(c);
		handle_state(c, AppStateHoldingPrompt);
	}
	else if (s->get_num_connected() == 0)
	{
		s->add_call(c);
		handle_state(c, AppStateStartingPrompt);
	}
	else
	{
		s->add_call(c);
		handle_state(c, AppStateJoiningPrompt);
	}

	// Call is added after prompt finishes playing
	return Ok;
}

// Connect call to preset meeting & submeeting.
int Voice::connect_call(Call * c)
{
    Meeting * m = find_meeting(c->get_meetingid());
    if (m == NULL)
        return InvalidMeetingID;

    SubMeeting * s = m->find_submeeting(c->get_submeetingid());
    if (s == NULL)
        return InvalidSubmeetingID;

	// If this call was supposed to be held, but another call has been
	// connected, we can join this call now--otherwise, honor hold flag
	// from original request.
	if ((c->get_options() & CallScreen) && !c->get_screened())
	{
		handle_state(c, AppStateRecordNamePrompt);
	}
	else if (c->get_hold() && s->get_num_connected() == 0)
	{
		s->queue_call(c);
		handle_state(c, AppStateHoldingPrompt);
	}
	else if (s->get_num_connected() == 0)
	{
		s->add_call(c);
		handle_state(c, AppStateStartingPrompt);
	}
	else
	{
		s->add_call(c);
		handle_state(c, AppStateJoiningPrompt);
	}

	// Call is added after prompt finishes playing
	return Ok;
}

int Voice::move_call(const char * meeting, const char *sub_id, const char *call_id, const char *new_sub_id)
{
    Meeting * m = find_meeting(meeting);
    if (m == NULL)
        return InvalidMeetingID;

    SubMeeting * s = m->find_submeeting(sub_id);
    if (s == NULL)
        return InvalidSubmeetingID;

    Call * c = find_call(call_id);
    if (c == NULL)
        return InvalidCallID;

    SubMeeting * new_s = m->find_submeeting(new_sub_id);
    if (new_s == NULL)
        return InvalidSubmeetingID;

	c->reference();
	int status = s->remove_call(c, false);
	if (status == Ok)
	{
		new_s->add_call(c);
		new_s->connect_call(c);
        if (new_s->is_main() && c->is_moderator() &&
            new_s->get_num_queued() > 0)
        {
            // moderator is entering the main meeting when
            // holding calls are present
            
            new_s->release_holding();
        }
	}
	c->dereference();

	return status;
}

int Voice::disconnect_call(const char * meeting, const char *sub_id, const char *call_id)
{
    SubMeeting *s = NULL;

    if (strlen(meeting) > 0 && strlen(sub_id) > 0)
    {
        Meeting * m = find_meeting(meeting);
        if (m == NULL)
            return InvalidMeetingID;

        s = m->find_submeeting(sub_id);
        if (s == NULL)
            return InvalidSubmeetingID;
    }
    
    Call * c = find_call(call_id);
    if (c == NULL)
        return InvalidCallID;

    if (s != NULL)
        return s->remove_call(c);
    else
        return c->disconnect();
}

// Make call to indicated phone number.
// Username is reported back to controller.
int Voice::make_call(const char * user_id,
                     const char * username,
                     const char * meeting,
                     const char * sub_id,
                     const char * address,
                     const char * extension,
                     int options,
                     const char * call_id,
                     const char * part_id)
{
    Meeting * m = find_meeting(meeting);
    if (m == NULL)
        return InvalidMeetingID;

    SubMeeting * sm = m->find_submeeting(sub_id);
    if (sm == NULL)
        return InvalidSubmeetingID;

	// Find supervising call, if any.
    Call * super= NULL;
	if (*call_id)
	{
		super = find_call(call_id);
		if (super == NULL)
			return InvalidCallID;
	}

	// Create physical call
	ICall * call;
	int status = provider->create_call(address, extension, call, options);
	if (status)
		return status;

	// Create wrapper for voice call
    char buff[128];
    sprintf(buff, "voice:out-%d@%s", ++call_id_seq, service_name.get_string());

    Call * c = new Call(buff, call, user_id, username);
	c->set_meetingid(meeting);
	c->set_options(options | CallOutbound);
    c->set_participantid(part_id);

	const char * pin = strchr(part_id, ':');
	if (pin)
		c->set_pin(pin + 1);

	// Associate voice call with call wrapper
    call->get_channel()->set_user(c);

	sm->add_outbound_call(c);

    IString * key = new IString(buff);
    calls.insert(key, c);

	c->dereference();

	if (options & CallSupervised)
	{
		// Start timer & report call once timer completes
		// (Once call is reported, we will also join the meeting.)
		handle_state(c, AppStateDialing);
	}
	else 
	{
		// Normal or direct call -- report to controller
		// (Meeting is not joined when report completes; we need to prompt
		// once someone answers.)
		handle_state(c, AppStateProceeding);
	}

    return Ok;
}

int Voice::set_volume(const char * call_id,  const char * meeting, const char * sub_id, int volume, int gain, int mute)
{
    Call * c = find_call(call_id);
    if (c == NULL)
        return InvalidCallID;

    Meeting * m = find_meeting(meeting);
    if (m == NULL)
        return InvalidMeetingID;

    SubMeeting * s = m->find_submeeting(sub_id);
    if (s == NULL)
        return InvalidSubmeetingID;

	int status = s->set_volume(c, volume, gain, mute);
	return status;
}

int Voice::get_volume(const char * call_id,  const char * meeting, const char * sub_id, int & out_vol, int & out_gain, int & out_mute)
{
    Call * c = find_call(call_id);
    if (c == NULL)
        return InvalidCallID;

    Meeting * m = find_meeting(meeting);
    if (m == NULL)
        return InvalidMeetingID;

    SubMeeting * s = m->find_submeeting(sub_id);
    if (s == NULL)
        return InvalidSubmeetingID;

	int status = s->get_volume(c, out_vol, out_gain, out_mute);
	return status;
}

int Voice::roll_call(const char * call_id,
                     const char * meeting)
{
    Call * c = find_call(call_id);
    if (c == NULL)
        return InvalidCallID;

    SubMeeting * sm = c->get_submeeting();
    if (sm == NULL)
    {
        return Ok;
    }
        
    SubMeeting * msm = (sm->is_main() ? sm : NULL);
    if (msm == NULL)
    {
        Meeting * m = sm->get_meeting();
        msm = m->find_mainmeeting();
    }

    if ((msm->get_options() & ScreenCallers) ||
        (c->get_options() & CallScreen))
    {
        handle_state(c, AppStateRollCallPrompt);
    }
    else
    {
        IChannel * ch = c->get_channel();
        
        if (ch != NULL)
        {
            IConference * conf = ch->get_conference();
            if (conf != NULL)
            {
                conf->hold(ch, false);	// switch voice back to dsp
                ch->menu_completion("MenuFeatureUnavail.wav");
            }
        }
    }
    
    return Ok;
}

int Voice::grant_moderator(const char * call_id,
                           const char * meeting,
                           const char *sub_id,
                           int grant)
{
    Call * c = find_call(call_id);
    if (c == NULL)
        return InvalidCallID;

    Meeting * m = find_meeting(meeting);
    if (m == NULL)
        return InvalidMeetingID;

    SubMeeting * s = m->find_submeeting(sub_id);
    if (s == NULL)
        return InvalidSubmeetingID;
    
	int status = s->grant_moderator(c, grant);

    return status;
}


int Voice::error(const char * call_id, const char * error_id, int error, const char * msg)
{
    Call * c = find_call(call_id);
    if (c == NULL)
        return InvalidCallID;

	// Could not join meeting for some reason
	if (strcmp(error_id, "join") == 0)
	{
		if (error == InvalidPassword)
			handle_state(c, AppStateInvalidPassword);
		else
			handle_state(c, AppStateError);
	}

	return Ok;
}

void Voice::on_initialized()
{
    log_debug(ZONE, "Initialized event received");

    // Register service with controller
	// Rev. 1 will rely on well known address.
    
}

void Voice::on_new_call(ICall * call, const char * pin, int options)
{
    log_debug(ZONE, "Call received");
    
    char buff[128];
    sprintf(buff, "voice:in-%d@%s", ++call_id_seq, service_name.get_string());

    Call * c = new Call(buff, call);

    IString * key = new IString(buff);
    calls.insert(key, c);

	c->dereference();

    call->get_channel()->set_user(c);
    
	if (pin != NULL)
	{
		// Received pin from inbound call already
		c->set_pin(pin);

		// Change call options as requested.
		if (options)
        	c->set_options((c->get_options() & ~(CallTypeMask)) | options);
	}
	
  	handle_state(c, AppStateAnswering);
}

// Call has been released by voice API.
void Voice::on_release_call(ICall * call)
{
    IChannel * channel = call->get_channel();
    Call * c = (channel != NULL) ? (Call*)channel->get_user() : NULL;

	if (c)
		c->on_release();

    call->get_channel()->set_user(NULL);
		
	IString key(c->get_id());
	calls.remove(&key);
}

void Voice::on_event(IEvent * event)
{
    IChannel * channel = event->channel;
    Call * call = (channel != NULL) ? (Call*)channel->get_user() : NULL;

	if (call == NULL)
	{
		log_debug(ZONE, "Call already released for channel %d", channel->get_id());
		return;
	}
    
    switch (event->event)
    {
        case EventConnected:
        {
            log_debug(ZONE, "%s call is connected",
                      event->reason == ConnectedInbound ? "Inbound" : "Outbound");
            if (event->reason == ConnectedInbound)
            {
				if (*call->get_pin())
					handle_state(call, AppStateCheckPin);
				else
                	handle_state(call, AppStateGreeting);
            }
			else
			{
                // Outbound call
                if (call->get_call()->has_extension() && !(call->get_options() & CallSupervised))
                {
                    handle_state(call, AppStateDialExtension);
                }
                else if (call->get_options() & CallDirect)
				{
					// Join meeting once connected on call me
					handle_state(call, AppStateJoinMeeting);
				}
				else if (call->get_options() & CallSupervised)
				{
					// Call was already reported & conferenced
					log_debug(ZONE, "Supervised call connected");
				}
				else
				{
					// Notify callee of invitation to meeting
					handle_state(call, AppStateOutboundGreeting);
				}
			}
            break;
        }
        case EventDisconnected:
        {
            log_debug(ZONE, "Caller hung up");

			if (call->get_submeeting())
				call->get_submeeting()->remove_call(call);

            report_disconnect(call);
            break;
        }
        case EventTerminated:
        {
			// A voice API has completed, figure out what to do next.
            log_debug(ZONE, "API complete: %d", event->reason);

			int state = next_state(call);
            if ((event->reason == TerminatedStoppedDTMF) &&
                (state == AppStateRollCallPrompt || state == AppStateParticipantPrompt))
            {
                state = AppStateConference;
            }
            
			if (state >= 0)
				handle_state(call, state);

            break;
        }
		case EventProgress:
		{
			// Outgoing call progress event.
			report_progress(call, event->reason);
			break;
		}
		case EventProgressEnd:
		{
			// Ringing ended.  Call must not have been answered by remote end yet, so restart outbound greeting.
			if (event->reason == ProgressRinging && (call->get_state() == AppStateOutboundGreeting || call->get_state() == AppStateEnterAccept)
				&& call->add_ring() < 3)
			{
				log_debug(ZONE, "** Ringing after answer, restarting greeting");
				handle_state(call, AppStateRestartOutbound);
			}
			else if (event->reason == ProgressSpeech)
			{
				report_progress(call, 0);
			}
			break;
		}
		case EventMenuCommand:
        {
            switch (event->reason)
            {
                case MenuActionToggleRaiseHand:
                    toggle_hand(call);
                    channel->set_timeout(TimerApp, menu_command_timeout);
                    break;

                case MenuActionToggleVolume:
                    toggle_volume(call);
                    channel->set_timeout(TimerApp, menu_command_timeout);
                    break;

                case MenuActionToggleLectureMode:
                    toggle_lecture_mode(call);
                    channel->set_timeout(TimerApp, menu_command_timeout);
                    break;

                case MenuActionLockMeeting:
                    lock_meeting(call, 1);
                    channel->set_timeout(TimerApp, menu_command_timeout);
                    break;

                case MenuActionUnlockMeeting:
                    lock_meeting(call, 0);
                    channel->set_timeout(TimerApp, menu_command_timeout);
                    break;

                case MenuActionMuteOn:
                    mute(call, 1);
                    channel->set_timeout(TimerApp, menu_command_timeout);
                    break;

                case MenuActionMuteOff:
                    mute(call, 0);
                    channel->set_timeout(TimerApp, menu_command_timeout);
                    break;

                case MenuActionEndMeeting:
                    end_meeting(call);
                    channel->set_timeout(TimerApp, menu_command_timeout);
                    break;

                case MenuActionRollCall:
                {
                    SubMeeting * sm = call->get_submeeting();
                    SubMeeting * msm = (sm->is_main() ? sm : NULL);
                    if (msm == NULL)
                    {
                        Meeting * m = sm->get_meeting();
                        msm = m->find_mainmeeting();
                    }
                    if ((msm->get_options() & ScreenCallers) ||
                        (call->get_options() & CallScreen))
                    {
                        handle_state(call, AppStateRollCallPrompt);
                    }
                    else
                    {
                        channel->menu_completion("MenuFeatureUnavail.wav");
                    }
                }
                break;
                    
                case MenuActionDialNumber:
                    handle_state(call, AppStateEnterDialNumber);
                    break;

                case MenuActionToggleRecordMeeting:
                    toggle_record_meeting(call);
                    channel->set_timeout(TimerApp, menu_command_timeout);
                    break;

                case MenuActionToggleMuteJoinLeave:
                    toggle_mute_join_leave(call);
                    channel->set_timeout(TimerApp, menu_command_timeout);
                    break;

                case MenuActionEndSubMeetingJoinMain:
                    remove_submeeting(call, false);
                    channel->set_timeout(TimerApp, menu_command_timeout);
                    break;

                case MenuActionEndSubMeetingEndCalls:
                    remove_submeeting(call, true);
                    channel->set_timeout(TimerApp, menu_command_timeout);
                    break;

                default:
                    log_error(ZONE, "Unhandled menu action %d", event->reason);
                    break;
            }
            break;
            
            case EventTimeout:
            {
                channel->on_timeout(event->reason);
            }
            break;
            default:
                log_error(ZONE, "Unhandled event %d", event->event);
                break;
        }
    }
}

void Voice::on_error(IChannel * chan, int code, const char * msg)
{
    log_error(ZONE, "Error from channel %ld (%s)", (long)chan, msg);
	// *** may need to inform channel
}

void Voice::on_timeout(IChannel * channel)
{
	Call * call = (channel != NULL) ? (Call*)channel->get_user() : NULL;
	if (call)
	{
		if (call->get_state() == AppStateDialing)
		{
			log_debug(ZONE, "Connecting call %s to conference", call->get_id());
			int state = next_state(call);
			if (state >= 0)
				handle_state(call, state);
		}
		else
		{
			log_error(ZONE,
                      "No response received from controller for call: %s",
                      call->get_id());
			handle_state(call, AppStateError);
		}
	}
}

// App state machine

// Called on voice api termination to determine next state.
int Voice::next_state(Call * call)
{
	int next = -1;
	
	switch (call->get_state())
	{
		case AppStateGreeting:
		case AppStateInvalidPin:
		case AppStateMissingPin:
			next = AppStateEnterPin;
            break;

		case AppStateEnterPin:
            if (call->too_many_attempts())
            {
				report_progress(call, 0);
                next = AppStateGoodbye;
            }
            else
            {
                next = AppStateCheckPin;
            }
			break;

		case AppStateInvalidPassword:
		case AppStateMissingPassword:
                next = AppStateEnterPassword;
			break;
		case AppStateEnterPassword:
            if (call->too_many_attempts())
            {
				report_progress(call, 0);
                next = AppStateGoodbye;
            }
            else
            {
                next = AppStateCheckPassword;
            }
			break;
		case AppStateRecordNamePrompt:
			next = AppStateRecordName;
			break;
		case AppStateRecordName:
			next = AppStateConnectCall;
			break;
		case AppStateJoiningPrompt:
			// Joining already active meeting.
			if (call->get_options() & CallScreen)
				next = AppStateAnnounce;
			else
				next = AppStateConference;
			break;

        case AppStateRollCallPrompt:
        case AppStateParticipantPrompt:
            if (call->next_roll_call_participant())
            {
                next = AppStateParticipantPrompt;
            }
            else
            {
                next = AppStateConference;
            }
            break;
            
        case AppStateEnterDialNumber:
            // TODO: cancel case
            next = AppStateDialNumber;	// now that we have it, do it.
            break;
            
		case AppStateStartingPrompt:
			// If first caller, or first moderator and wait option is selected.
			next = AppStateStarting;
			break;

		case AppStateHoldingPrompt:
			// Waiting for host to join.
			next = AppStateHolding;
			break;
		case AppStateAnnounce:
			next = AppStateEndAnnounce;
			break;
		case AppStateOutboundGreeting:
			next = AppStateEnterAccept;
			break;
		case AppStateEnterAccept:
			if (!check_accept(call))
			{
                if (call->too_many_attempts())
                {
                    report_progress(call, 0);
                    next = AppStateGoodbye;
                }
                else
                {
                    next = AppStateEnterAccept;
                }
			}
			else
            {
                call->reset_attempts();
                next = AppStateJoinMeeting;
            }
			break;
		case AppStateRestartOutbound:
			next = AppStateOutboundGreeting;
			break;
		case AppStateDialing:
			next = AppStateReportCall;
			break;
        case AppStateDialExtension:
            if (call->get_options() & CallDirect)
                next = AppStateJoinMeeting;
            else
                next = AppStateOutboundGreeting;
            break;
            
		case AppStateError:
			// next = AppStateGoodbye;
			next = AppStateEnterPin;
			break;

		case AppStateGoodbye:
		case AppStateEndingPrompt:
			next = AppStateDisconnect;
			break;
	}			 

	log_debug(ZONE, "%s: Moving from state %d to %d", call->get_id(), call->get_state(), next);

	return next;	
}

void Voice::handle_state(Call * call, int state)
{
    IChannel * channel = call->get_channel();
    if (channel == NULL)
    {
        log_error(ZONE, "handling state of a call w/o a channel!!!  Ignoring...");
        
        return;
    }
    
	SubMeeting * sm = call->get_submeeting();

	call->set_state(state);

	switch (state)
	{
		case AppStateAnswering:
			call->get_call()->answer();
			channel->clear_digits();
			break;
		case AppStateGreeting:
			channel->play("Greeting.wav", TermDTMFAny);
			break;
		case AppStateRecordNamePrompt:
			channel->play("SayName.wav", TermDTMFAny);
			break;
		case AppStateRecordName:
        {
			call->set_screened(true);

            char filename[128];
            sprintf(filename, "record%d.wav", channel->get_id());
            
			channel->record(filename, true, 4000, 2500, 400000, TermDTMFAny);
        }
        break;
		case AppStateEnterPin:
			channel->entry("EnterMeetingPin.wav", 16, 6000, 6000, TermDTMFPound);
			break;
		case AppStateCheckPin:
			if (find_pin(channel) == MissingPIN)
				handle_state(call, AppStateMissingPin);
			break;
		case AppStateInvalidPin:
			channel->clear_digits();
			channel->play("InvalidPin.wav", TermDTMFAny);
			break;
		case AppStateMissingPin:
			channel->play("MissingPin.wav", TermDTMFAny);
			break;
		case AppStateEnterPassword:
			channel->entry("EnterPassword.wav", 12, 6000, 6000, TermDTMFPound);
			break;
		case AppStateCheckPassword:
			if (check_password(channel) == MissingPIN)
				handle_state(call, AppStateMissingPassword);
			break;
		case AppStateInvalidPassword:
			channel->clear_digits();
			channel->play("InvalidPassword.wav", TermDTMFAny);
			break;
		case AppStateMissingPassword:
			channel->play("MissingPassword.wav", TermDTMFAny);
			break;
		case AppStateReportCall:
            call->reset_attempts(); // good to go; reset attempt count
			report_call(call);
            // Start timer so we don't wait indefinitely for server response
			channel->set_timeout(TimerApp, join_meeting_timeout);
			break;
		case AppStateJoinMeeting:
			join_meeting(call);
            // Start timer so we don't wait indefinitely for server response
			channel->set_timeout(TimerApp, join_meeting_timeout);
			break;
		case AppStateConnectCall:
			connect_call(call);
			break;
		case AppStateHoldingPrompt:
			channel->play("HoldForModerator.wav", TermNone);
			break;
		case AppStateStartingPrompt:
			if (sm)
			{
				if (sm->get_num_queued())
					channel->play("StartingMeetingWithHolding.wav", TermNone);
				else
					channel->play("StartingMeeting.wav", TermNone);
			}
			break;
		case AppStateJoiningPrompt:
			channel->play("JoiningMeeting.wav", TermNone);
			break;
		case AppStateHolding:
            channel->connect_music_on_hold();
			break;
		case AppStateAnnounce:
			if (sm)
			{
                char filename[128];
                sprintf(filename, "record%d.wav", channel->get_id());
            
				if (sm->is_announcing())
				{
					// We've got to wait for another announcement to finish, so we move call info
					// holding queue
					sm->remove_call(call, false);
					sm->queue_call(call);
				}
				else if (sm->play_announcement(call, filename) != Ok)
				{
					// Play failed to start, just conference.
					sm->connect_call(call);
				}
			}
			break;
		case AppStateEndAnnounce:
			if (sm)
			{
				// Complete connectiong of this call to conference
				sm->on_announcement_end(call);
				
				// Release one holding call, if any
				sm->release_first();
			}
			break;
		case AppStateStarting:
			if (sm)
			{
				// Release all calls (may want to do roll call here)
				sm->release_holding();
			}
			// Fall through...
		case AppStateConference:
			if (sm)
			{
				sm->connect_call(call);
			}
			break;
		case AppStateEndingPrompt:
			channel->play("MeetingEnded.wav", TermNone);
			break;
		case AppStateError:
			channel->clear_digits();
			channel->play("JoinMeetingError.wav", TermDTMFAny);
			break;
		case AppStateGoodbye:
			channel->play("Goodbye.wav", TermNone);
			break;
		case AppStateDisconnect:
			call->disconnect();
			break;
		case AppStateDialing:
			// Wait specified time for dialing to complete
			// *** Move into provider and provide event for dialing complete
			channel->set_timeout(TimerApp, delay_after_dial);
			break;
		case AppStateProceeding:
			// Call has already already started
			// Report now, but don't join conference until call is connected
			report_call(call);
			break;
		case AppStateOutboundGreeting:
			channel->play("OutboundGreeting.wav", TermDTMFAny);
			break;
		case AppStateEnterAccept:
			channel->entry("EnterAcceptMeeting.wav", 1, 6000, 6000, TermDTMFAny);
			break;
		case AppStateRestartOutbound:
			if (channel->stop() == Ok)
				handle_state(call, AppStateOutboundGreeting);
			break;

        case AppStateRollCallPrompt:
            if (sm)
            {
                call->reset_roll_call();
                IConference * conf = channel->get_conference();
                conf->hold(channel, false);	// switch voice back to dsp
                channel->play("RollCallBegin.wav", TermDTMFAny);
            }
            
			break;

        case AppStateParticipantPrompt:
            channel->play(call->get_roll_call_prompt(), TermDTMFAny);
            break;

        case AppStateEnterDialNumber:
            // Use 20 digits for now (e.g. 011 + 5-digit country code + 10-digit native number + a couple extra)
			channel->entry("EnterDialNumber.wav", 20, 6000, 6000, TermDTMFPound|TermDTMFStar);
			break;

        case AppStateDialNumber:
            place_call(call);
            break;
            
        case AppStateDialExtension:
        {
            IString ext;
            ext = ",";
            ext = ext + call->get_call()->get_extension();
            channel->play_dtmf((const char *)ext, true);
        }
        break;
    }
}

int Voice::place_call(Call * call)
{
    IChannel * channel = call->get_channel();
    char callee_id[16];
    char dial_number[32];

    sprintf(callee_id, "anon:%d", ++anon_id_seq);
    channel->get_digits(dial_number, sizeof(dial_number)-1);

    if (strlen(dial_number) > 0 && dial_number[strlen(dial_number)-1] == '*')
    {
        log_status(ZONE, "User aborted dial");

        channel->cancel();
        handle_state(call, AppStateConference);
        
        return Ok;
    }

    if (strlen(dial_number) > 0 && dial_number[strlen(dial_number)-1] == '#')
    {
        dial_number[strlen(dial_number)-1] = '\0';
    }

    if (strlen(dial_number) == 0)
    {
        // Empty number; ignore
        channel->cancel();
        handle_state(call, AppStateConference);

        return Ok;
    }
    
	log_debug(ZONE, "Dialing out to %s at %s", callee_id, dial_number);

    IString rpccallid("placecall:");
    rpccallid = rpccallid + call->get_id();
    
    channel->set_timeout(TimerApp, menu_command_timeout);

    int status = rpc_make_call(rpccallid,
                               "controller", "controller.place_call", "(ssss)",
                               call->get_userid(), call->get_meetingid(), callee_id, dial_number);
    if (status != 0)
	{
        log_error(ZONE, "Unable to dispatch call %s to controller", call->get_id());
		on_controller_error(call->get_id(), Failed);
	}

    return status;
}

int Voice::on_place_call_complete(const char * callid, RPCResultSet * rs)
{
	Call * call;
	IChannel * channel;
	
	call = find_call(callid);
	channel = (call != NULL) ? call->get_channel() : NULL;
	
	if (call == NULL || channel == NULL)
	{
		return Failed;
	}

	// Stop timer
	channel->cancel();

	int status;
	if (!rs->get_output_values("(i)", &status))
	{
		log_error(ZONE, "Error parsing result from lock_meeting");

        return Failed;
	}

    if (IS_FAILURE(status))
    {
        // play some kind of failure prompt
		return status;
    }
    
    handle_state(call, AppStateConference);

    return Ok;
}


bool Voice::check_accept(Call * call)
{
    IChannel * channel = call->get_channel();

	char buff[8];
	channel->get_digits(buff, sizeof(buff) - 1);
	log_debug(ZONE, "Accept: %s", buff);
	
	return *buff == '1';
}

int Voice::check_password(IChannel * channel)
{
    Call * call = (channel != NULL) ? (Call*)channel->get_user() : NULL;

	char buff[32];
	channel->get_digits(buff, sizeof(buff) - 1);
	log_debug(ZONE, "Pin: %s", buff);

	// Strip terminator if present
	if (buff[strlen(buff) - 1] == '#')
		buff[strlen(buff) - 1] = '\0';

	if (strlen(buff) == 0)
		return MissingPIN;

	call->set_password(buff);

	// Have password, time to report call (we may want to fetch password and compare ourselves)
	handle_state(call, AppStateReportCall);

	return Ok;
}

int Voice::find_pin(IChannel * channel)
{
    Call * call = (channel != NULL) ? (Call*)channel->get_user() : NULL;

	char buff[32];

	if (*call->get_pin())
	{	
		// PIN already set, probably figured it out from calling address
		strcpy(buff, call->get_pin());
	}
	else
	{
		channel->get_digits(buff, sizeof(buff) - 1);
	}
	
	log_debug(ZONE, "Pin: %s", buff);

	// Strip terminator if present
	if (buff[strlen(buff) - 1] == '#')
		buff[strlen(buff) - 1] = '\0';
       
 	call->set_pin(buff);

	if (strlen(buff) == 0)
		return MissingPIN;

	char id[MAX_ID_LENGTH];
	sprintf(id, "find:%s", call->get_id());

	if (rpc_make_call(id, "controller", "controller.find_pin", "(ss)", "sys:voice", buff))
	{
		log_error(ZONE, "Unable to lookup meeting for call %s", call->get_id());
		return Failed;
	}

	return Ok;
}

int Voice::on_pin_found(const char * callid, RPCResultSet * rs)
{
	Call * call;
	IChannel * channel;
	
	call = find_call(callid);
	channel = (call != NULL) ? call->get_channel() : NULL;
	
	if (call == NULL || channel == NULL)
	{
		return Failed;
	}
	
	int found, options;
	char *meetingid, *participantid, *userid, *username, *email, *name;
	char id[MAX_ID_LENGTH];
	
	// Read controller.find_pin result and handle
    //   "FOUND", "MEETINGID", "PARTICIPANTID", "USERID", "EMAIL", 
    //   "USERNAME", "NAME", "OPTIONS", "SCHEDULED_TIME", "INVITE_VALID_BEFORE", 
    //   "INVITE_EXPIRATION", "TYPE", "INVITED_TIME"
	//  FOUND: 1 = meeting found, 2 = meeting and user found, 0 = not found
	if (!rs->get_output_values("(issssssi*)", &found, &meetingid, &participantid, &userid, &email, &username, &name, &options))
	{
		log_error(ZONE, "Error parsing info from pin lookup");
		return Failed;
	}
	
	switch (found)
	{
		case 0:
			// Invalid pin
			call->set_pin("");
			handle_state(call, AppStateInvalidPin);
			return Ok;
		
		case 1:
			// Anonymous meeting pin.
			call->set_meetingid(meetingid);
			sprintf(id, "anon:%d", ++anon_id_seq);
			call->set_userid(id);
			if (*participantid)
			  call->set_participantid(participantid);
			else
			  call->set_participantid("");
            call->reset_attempts();
			break;
			
		case 2:
			// Personal pin
			// User id may be specified, if not use participant id
			call->set_meetingid(meetingid);
			if (strchr(userid, ':') > 0)
				strcpy(id, userid);
			else if (strchr(participantid, ':') > 0)
				strcpy(id, participantid);
			else if (atoi(userid) > 0)
				sprintf(id, "user:%s", userid);
			else
				sprintf(id, "pin:%s", participantid);
			call->set_userid(id);
            if (*participantid)
                call->set_participantid(participantid);
			call->set_username(username);
            call->reset_attempts();
			break;
	}

	if (call->get_options() & CallOutbound)
	{
		// Special inbound call needs to be treated as outbound
		handle_state(call, AppStateReportCall);
	}
	else
	{
		// Normal inbound call, convert meeting options to call options
		int call_options = (options & ScreenCallers ? CallScreen : 0) 
	        | (options & RequirePassword ? CallPassword : 0);
	
		call->set_options(call_options);
	
		if (call_options & CallPassword)
		{
			// Need password.
			handle_state(call, AppStateEnterPassword);
		}
		else
		{
			// No password required, just report.
			handle_state(call, AppStateReportCall);
		}
	}
	
	return Ok;
}

void Voice::on_setup_error(const char * callid, const char * message)
{
	Call * call;
	IChannel * channel;
	
	log_error(ZONE, "Unexpected error setting up for call %s: %s", callid, message);
	
	call = find_call(callid);
	channel = (call != NULL) ? call->get_channel() : NULL;
	if (call != NULL && channel != NULL)
	{
		handle_state(call, AppStateError);
	}
}

void Voice::on_controller_error(const char * callid, int fault_code)
{
	Call * call;
	IChannel * channel;
	
	log_error(ZONE, "Recieved error code %d for call %s", fault_code, callid);
	
	call = find_call(callid);
	channel = (call != NULL) ? call->get_channel() : NULL;
	
	if (call != NULL && channel != NULL)
	{
		switch (fault_code)
		{
			case InvalidPassword:
				handle_state(call, AppStateInvalidPassword);
				break;
			case NotAuthorized:
			case InsufficientResources:
            case PINExpired:
            case PINNotValidYet:
			default:
				handle_state(call, AppStateError);
		}

        report_disconnect(call);
	}
}

// Register call with controller.
void Voice::report_call(Call * call)
{
	char id[MAX_ID_LENGTH];
	sprintf(id, "report:%s", call->get_id());

	const char *userid = call->get_userid();
	IString pin;

	if (*call->get_pin())
	{
		// Yes, we looked up a user id already...but someone else may have registered
		// using the same pin already, so we let controller have a chance to convert it
		// to an anonymous participant.
		pin = IString("pin:") + call->get_pin();
		userid = pin;
	}

    const char *address;
    if ( call->get_options() & CallOutbound )
    {
        address = call->get_call()->get_called_address();
    }
    else
    {
        address = call->get_call()->get_calling_address();
    }
    
    int status = rpc_make_call(id, "controller", "controller.register_user", "(ssssss)",
                               userid, call->get_username(), "", call->get_id(), 
                               "", address);
    if (status != 0)
	{
        log_error(ZONE, "Unable to dispatch call %s to controller", call->get_id());
		on_controller_error(call->get_id(), Failed);
	}
}

void Voice::on_report_complete(const char * callid, RPCResultSet *rs)
{
	Call * call;
	IChannel * channel;
	
	call = find_call(callid);
	channel = (call != NULL) ? call->get_channel() : NULL;
	
	if (call == NULL || channel == NULL)
	{
		return;
	}

	// Stop timer
	channel->cancel();

	char *userid, *partid, *weburl;
	int status;
	
	// User may have become an anonymous user during registration
	rs->get_output_values("(sssi)", &userid, &partid, &weburl, &status);
	if (*userid)
		call->set_userid(userid);
	call->set_participantid(partid);

	// Join to meeting if this is an inbound call, or a supervised outbound call.
	// The controller will then direct us to complete the connection.
	// (Normal outbound are not joined as caller must be prompted.)
	int options = call->get_options();
	if ((options & CallOutbound) == 0 || (options & CallPCPhone) != 0 ||
		((options & CallOutbound) != 0 && (options & CallTypeMask) == CallSupervised))
		join_meeting(call);
}

void Voice::join_meeting(Call * call)
{
	char id[MAX_ID_LENGTH];
	sprintf(id, "join:%s", call->get_id());
	
    int status = rpc_make_call(id, "controller", "controller.join_meeting", "(sssss)",
        call->get_userid(), call->get_id(), call->get_meetingid(), call->get_participantid(), call->get_password());
    if (status != 0 && status != Pending)
	{
        log_error(ZONE, "Unable to dispatch call %s to controller", call->get_id());
		on_controller_error(call->get_id(), Failed);
	}
}

int Voice::on_join(const char * callid, RPCResultSet * rs)
{
	Call * call;
	IChannel * channel;
	
	call = find_call(callid);
	channel = (call != NULL) ? call->get_channel() : NULL;
	
	if (call == NULL || channel == NULL)
	{
		return Failed;
	}

	// Stop timer
	channel->cancel();

	int status;
	char *participantid;
	
	// N.B. controller does not report participant ID anymore
	if (!rs->get_output_values("(si)", &participantid, &status))
	{
		log_error(ZONE, "Error parsing result from join meeting");
		return Failed;
	}
    
	if (status == Retry)
	{
		join_meeting(call);
		return Ok;
	}

	log_debug(ZONE, "Controller reports call %s joined to meeting", callid);

	return Ok;
}

void Voice::toggle_volume(Call * call)
{
    if (call == NULL)
    {
        log_error(ZONE, "toggle_volume given a NULL call argument");
        return;
    }

    SubMeeting * sm = call->get_submeeting();
    if (sm == NULL)
    {
        log_status(ZONE, "toggle_volume call not in submeeting; skipping");
        return;
    }

    int vol, gain, mute;
    
    sm->get_volume(call, vol, gain, mute);

    int outvol;
    if (vol <= 3)
    {
        outvol = 7;
    }
    else
    {
        outvol = 3;
    }
    
    IString rpccallid("togglevol:");
    rpccallid = rpccallid + call->get_id();
    
    int status = rpc_make_call(rpccallid,
                               "controller", "controller.set_participant_volume", "(sssiii)",
                               call->get_userid(),
                               call->get_meetingid(),
                               call->get_participantid(),
                               outvol, -1, -1);
    if (status != 0)
	{
        log_error(ZONE, "Unable to dispatch call %s to controller", call->get_id());
		on_controller_error(call->get_id(), Failed);
	}
}

int Voice::on_toggle_volume_complete(const char * callid, RPCResultSet * rs)
{
	Call * call;
	IChannel * channel;
	
	call = find_call(callid);
	channel = (call != NULL) ? call->get_channel() : NULL;
	
	if (call == NULL || channel == NULL)
	{
		return Failed;
	}

	// Stop timer for server response
	channel->cancel();

	int status;
	if (!rs->get_output_values("(i)", &status))
	{
		log_error(ZONE, "Error parsing result from set_participant_volume");
		return Failed;
	}

    SubMeeting * sm = call->get_submeeting();
    if (sm != NULL)
    {
        int vol;
        int ign;
        
        sm->get_volume(call, vol, ign, ign);
        IChannel * channel = call->get_channel();
        channel->menu_completion(vol <= 3 ? "MenuNormalVolume.wav" : "MenuLoudVolume.wav" );
    }
    else
    {
        log_status(ZONE, "Call no longer in a submeeting");
    }
    
    return Ok;
}

void Voice::toggle_hand(Call * call)
{
    if (call == NULL)
    {
        log_error(ZONE, "toggle_hand given a NULL call argument");
        return;
    }

    IString rpccallid("togglehand:");
    rpccallid = rpccallid + call->get_id();
    
    int status = rpc_make_call(rpccallid,
                               "controller", "controller.set_handup", "(sssi)",
                               call->get_userid(),
                               call->get_meetingid(),
                               call->get_participantid(),
                               2 /* toggle hand */);
    if (status != 0)
	{
        log_error(ZONE, "Unable to dispatch call %s to controller", call->get_id());
		on_controller_error(call->get_id(), Failed);
	}
}

int Voice::on_toggle_hand_complete(const char * callid, RPCResultSet * rs)
{
	Call * call;
	IChannel * channel;
	
	call = find_call(callid);
	channel = (call != NULL) ? call->get_channel() : NULL;
	
	if (call == NULL || channel == NULL)
	{
		return Failed;
	}

	// Stop timer
	channel->cancel();

	int status;
    int handup;
    
	if (!rs->get_output_values("(ii)", &status, &handup))
	{
		log_error(ZONE, "Error parsing result from set_handup");
		return Failed;
	}

	log_debug(ZONE, "Controller says %s hand is %s", callid, (handup ? "up" : "down"));

    channel->menu_completion(handup ? "MenuHandUp.wav" : "MenuHandDown.wav" );

    return Ok;
}

void Voice::remove_submeeting(Call * call, bool remove_parts)
{
    if (call == NULL)
    {
        log_error(ZONE, "toggle_hand given a NULL call argument");
        return;
    }

    IString rpccallid("removesubmeeting:");
    rpccallid = rpccallid + call->get_id();
    
    int status = rpc_make_call(rpccallid,
                               "controller", "controller.remove_submeeting", "(sssi)",
                               call->get_userid(),
                               call->get_meetingid(),
                               call->get_submeeting()->get_id(),
                               (int)remove_parts);
    if (status != 0)
	{
        log_error(ZONE, "Unable to dispatch call %s to controller", call->get_id());
		on_controller_error(call->get_id(), Failed);
	}
}

int Voice::on_remove_submeeting_complete(const char * callid, RPCResultSet * rs)
{
	Call * call;
	IChannel * channel;
    int status;
    
	call = find_call(callid);
	channel = (call != NULL) ? call->get_channel() : NULL;
	
	if (call == NULL || channel == NULL)
	{
		return Failed;
	}

	// Stop timer
	channel->cancel();

	if (!rs->get_output_values("(i)", &status))
	{
		log_error(ZONE, "Error parsing result from remove_submeeting");
		return Failed;
	}

    if (status)
        channel->menu_completion("MenuRejoinedMain.wav");
    else
        channel->menu_completion("MenuFailed.wav");
    
    return Ok;
}

void Voice::toggle_lecture_mode(Call * call)
{
    if (call == NULL)
    {
        log_error(ZONE, "toggle_lecture_mode given a NULL call argument");
        return;
    }

    IString rpccallid("togglelecture:");
    rpccallid = rpccallid + call->get_id();
    
    int status = rpc_make_call(rpccallid,
                               "controller", "controller.toggle_lecture_mode", "(ss)",
                               call->get_userid(),
                               call->get_meetingid());
    if (status != 0)
	{
        log_error(ZONE, "Unable to dispatch call %s to controller", call->get_id());
		on_controller_error(call->get_id(), Failed);
	}
}

int Voice::on_toggle_lecture_mode_complete(const char * callid, RPCResultSet * rs)
{
	Call * call;
	IChannel * channel;
	
	call = find_call(callid);
	channel = (call != NULL) ? call->get_channel() : NULL;
	
	if (call == NULL || channel == NULL)
	{
		return Failed;
	}

	// Stop timer
	channel->cancel();

	int status, lecture_mode;
	if (!rs->get_output_values("(ii)", &status, &lecture_mode))
	{
		log_error(ZONE, "Error parsing result from toggle_lecture_mode");
		return Failed;
	}

    log_debug(ZONE, "Lecture mode is now %s", (lecture_mode ? "on" : "off"));
    
    channel->menu_completion(lecture_mode ? "MenuLectureOn.wav" : "MenuLectureOff.wav" );
    
    return Ok;
}

void Voice::lock_meeting(Call * call, int state)
{
    if (call == NULL)
    {
        log_error(ZONE, "lock_meeting given a NULL call argument");
        return;
    }

    IString rpccallid("lockmtg:");
    rpccallid = rpccallid + call->get_id();
    
    int status = rpc_make_call(rpccallid,
                               "controller", "controller.lock_meeting", "(ssi)",
                               call->get_userid(),
                               call->get_meetingid(),
                               state);
    if (status != 0)
	{
        log_error(ZONE, "Unable to dispatch call %s to controller", call->get_id());
		on_controller_error(call->get_id(), Failed);
	}
}

int Voice::on_lock_meeting_complete(const char * callid, RPCResultSet * rs)
{
	Call * call;
	IChannel * channel;
	
	call = find_call(callid);
	channel = (call != NULL) ? call->get_channel() : NULL;
	
	if (call == NULL || channel == NULL)
	{
		return Failed;
	}

	// Stop timer
	channel->cancel();

	int status, lock_state;
	if (!rs->get_output_values("(ii)", &status, &lock_state))
	{
		log_error(ZONE, "Error parsing result from lock_meeting");
		return Failed;
	}

    channel->menu_completion(lock_state ? "MenuMeetingLocked.wav" : "MenuMeetingUnlocked.wav" );
    
    return Ok;
}

void Voice::mute(Call * call, int state)
{
    if (call == NULL)
    {
        log_error(ZONE, "mute given a NULL call argument");
        return;
    }

    IString rpccallid("mute:");
    rpccallid = rpccallid + call->get_id();
    int status = rpc_make_call(rpccallid,
                               "controller",
                               "controller.set_participant_volume", "(sssiii)",
                               call->get_userid(),
                               call->get_meetingid(),
                               call->get_participantid(),
                               -1, -1, state);
    if (status != 0)
	{
        log_error(ZONE, "Unable to dispatch call %s to controller", call->get_id());
		on_controller_error(call->get_id(), Failed);
	}
}

int Voice::on_mute_complete(const char * callid, RPCResultSet * rs)
{
	Call * call;
	IChannel * channel;
	
	call = find_call(callid);
	channel = (call != NULL) ? call->get_channel() : NULL;
	if (call == NULL || channel == NULL)
	{
		return Failed;
	}

	// Stop timer
	channel->cancel();

	int status;
	if (!rs->get_output_values("(i)", &status))
	{
		log_error(ZONE, "Error parsing result from set_participant_volume");
		return Failed;
	}

    SubMeeting * sm = call->get_submeeting();
    if (sm != NULL)
    {
        int mute;
        int ign;
        
        sm->get_volume(call, ign, ign, mute);
        IChannel * channel = call->get_channel();
        channel->menu_completion(mute ? "MenuMuteOn.wav" : "MenuMuteOff.wav" );
    }
    else
    {
        log_status(ZONE, "Call no longer in a submeeting");
    }
    
    return Ok;
}

void Voice::end_meeting(Call * call)
{
    if (call == NULL)
    {
        log_error(ZONE, "end_meeting given a NULL call argument");
        return;
    }

    IString rpccallid("endmeeting:");
    rpccallid = rpccallid + call->get_id();
    
    int status = rpc_make_call(rpccallid,
                               "controller", "controller.end_meeting", "(ss)",
                               call->get_userid(),
                               call->get_meetingid());
    if (status != 0)
	{
        log_error(ZONE, "Unable to dispatch call %s to controller", call->get_id());
		on_controller_error(call->get_id(), Failed);
	}

	IChannel * channel = (call != NULL) ? call->get_channel() : NULL;
    if (channel)
        channel->menu_completion("MenuExit.wav");
    // ending the meeting should cause the end of meeting prompt to be
    // played
}

int Voice::on_end_meeting_complete(const char * callid, RPCResultSet * rs)
{
	Call * call;
	IChannel * channel;
	
	call = find_call(callid);
	channel = (call != NULL) ? call->get_channel() : NULL;
	
	if (call == NULL || channel == NULL)
	{
		return Failed;
	}

	// Stop timer
	channel->cancel();

	int status;
	if (!rs->get_output_values("(i)", &status))
	{
		log_error(ZONE, "Error parsing result from end meeting");
		return Failed;
	}

	log_debug(ZONE, "Controller end_meeting() == %d", status);

    handle_state(call, AppStateEndingPrompt);
    
    return Ok;
}

Meeting * Voice::find_meeting(const char * id)
{
    IString str(id);

    Meeting * m = (Meeting*)meetings.get(&str);
    return m;
}

void Voice::toggle_record_meeting(Call * call)
{
    if (call == NULL)
    {
        log_error(ZONE, "toggle_record_meeting given a NULL call argument");
        return;
    }

    IString rpccallid("togglerecord:");
    rpccallid = rpccallid + call->get_id();
    
    int status = rpc_make_call(rpccallid,
                               "controller", "controller.toggle_record_meeting",
                               "(ss)",
                               call->get_userid(),
                               call->get_meetingid());
    if (status != 0)
	{
        log_error(ZONE, "Unable to dispatch call %s to controller", call->get_id());
		on_controller_error(call->get_id(), Failed);
	}
}

int Voice::on_toggle_record_meeting_complete(const char * callid, RPCResultSet * rs)
{
	Call * call;
	IChannel * channel;
	
	call = find_call(callid);
	channel = (call != NULL) ? call->get_channel() : NULL;
	
	if (call == NULL || channel == NULL)
	{
		return Failed;
	}

	// Stop timer
	channel->cancel();

	int status, record_state;
	if (!rs->get_output_values("(ii)", &status, &record_state))
	{
		log_error(ZONE, "Error parsing result from toggle record meeting");
        
		return Failed;
	}
    
	log_debug(ZONE, "Controller toggle_record_meeting() == %d", status);
    
    channel->menu_completion(record_state ?
                             "MenuRecordingOn.wav" : "MenuRecordingOff.wav" );
    return Ok;
}

void Voice::toggle_mute_join_leave(Call * call)
{
    if (call == NULL)
    {
        log_error(ZONE, "toggle_mute_join_leave given a NULL call argument");
        return;
    }

    IString rpccallid("togglejoinleave:");
    rpccallid = rpccallid + call->get_id();
    
    int status = rpc_make_call(rpccallid,
                               "controller", "controller.toggle_mute_join_leave",
                               "(ss)",
                               call->get_userid(),
                               call->get_meetingid());
    if (status != 0)
	{
        log_error(ZONE, "Unable to dispatch call %s to controller", call->get_id());
		on_controller_error(call->get_id(), Failed);
	}
}

int Voice::on_toggle_mute_join_leave_complete(const char * callid, RPCResultSet * rs)
{
	Call * call;
	IChannel * channel;
	
	call = find_call(callid);
	channel = (call != NULL) ? call->get_channel() : NULL;
	
	if (call == NULL || channel == NULL)
	{
		return Failed;
	}

	// Stop timer
	channel->cancel();

	int status, state;
	if (!rs->get_output_values("(ii)", &status, &state))
	{
		log_error(ZONE, "Error parsing result from toggle mute join leave");
        
		return Failed;
	}
    
    channel->menu_completion(state ?
                             "MenuMuteJoinLeave.wav" :
                             "MenuUnmuteJoinLeave.wav" );

    return Ok;
}

Call * Voice::find_call(const char * id)
{
    IString str(id);

    Call * c = (Call*)calls.get(&str);
    return c;
}

int Voice::get_port_status(IVector * ports)
{
    if (provider)
        return provider->get_port_status(ports);
    else
        return Ok;
}

int Voice::reset_port(const char * port_id, const char * part_id)
{
    return provider->reset_port(atoi(port_id), part_id);
}


// Parameters

int Voice::join_meeting_timeout = 5000;
int Voice::menu_command_timeout = 5000;
int Voice::delay_after_dial = 4000;
