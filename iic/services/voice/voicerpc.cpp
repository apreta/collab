/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

/* Implementation for voice service APIs */

#ifdef _WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>
#include "../../jcomponent/rpc.h"
#include "../../jcomponent/jcomponent_ext.h"
#include "../../jcomponent/util/rpcresult.h"
#include "voice.h"
#include "voiceapi.h"

//#include <util.h>
#include "../../jcomponent/util/nadutils.h"
#include "../../jcomponent/util/log.h"

#ifndef OSS
#include "../common/license.inl"
#endif

#define JCOMPNAME "voice"

static int epoch = time(NULL);
static int controller_epoch = 0;
Voice voice;

short g_voice_server_port=5122;
IThread* g_sending_thread = NULL;

bool stub = FALSE;
IProvider * provider = NULL;
IHash parameters;

void *test(void *param);
int read_config(config_t conf);
extern "C" int JC_connected();

int voice_init(config_t conf)
{
	int status;

    log_status(ZONE, "Starting voice service");

	// Read configuration
    status = read_config(conf);
    if (status) return status;

    if (!stub) {
        status = provider->initialize();
        if (status) return status;
    }

    return 0;
}

// Read voice parameters
int read_config(config_t conf)
{
    const char * val = config_get_one(conf, JCOMPNAME, "voiceServerPort", 0);
    g_voice_server_port = (unsigned short)(val ? atoi(val) : 5211);

    val = config_get_one(conf, JCOMPNAME, "init.service", 0);
    if (val == NULL) val = "";
    voice.set_service_name((const char *)val);
    
    char * conf_file = config_get_one(conf, JCOMPNAME, "providerConfig", 0);
    if (conf_file != NULL)
    {
        nad_t nad;

        if (nad_load(conf_file, &nad))
        {
            log_error(ZONE, "unable to load voice parameters, using defaults\n");
            
            return Failed;
        }
        
        int root = 0;
        
        root = nad_find_elem(nad, 0, "app", 1);
        if (root > 0)
        {
            int def = nad_find_first_child(nad, root);
            while (def > 0)
            {
                IString param_name(NAD_ENAME(nad, def), NAD_ENAME_L(nad, def));
                IString param_val(NAD_CDATA(nad, def), NAD_CDATA_L(nad, def));
                
                // dispatch to voice app
                voice.set_parameter(param_name, param_val);
                
                def = nad_find_next_sibling(nad, def);
            }
        }
        
        root = nad_find_elem(nad, 0, "provider", 1);
        if (root > 0)
        {
            int def = nad_find_first_child(nad, root);
            
            // Create a voice provider based on the provider name
            if (def > 0)
            {
                IString provider_name_param(NAD_ENAME(nad, def), NAD_ENAME_L(nad, def));
                IString provider_name_val(NAD_CDATA(nad, def), NAD_CDATA_L(nad, def));
                
                provider = voice_get_provider(provider_name_val,  voice);
                if (provider == NULL) {
                    return -1;
                }
            
                provider->set_parameter(provider_name_param, provider_name_val);
            }

            def = nad_find_next_sibling(nad, def);
            
            // Set any voice provider parameters
            while (def > 0)
            {
                IString param_name(NAD_ENAME(nad, def), NAD_ENAME_L(nad, def));
                IString param_val(NAD_CDATA(nad, def), NAD_CDATA_L(nad, def));
                
                // dispatch to provider
                provider->set_parameter(param_name, param_val);
                
                def = nad_find_next_sibling(nad, def);
            }
        }
        
        nad_free(nad);
    }

#ifndef OSS
    int licenseResult = ValidateLicenseAndCheckOption("/opt/iic/conf/license-key.xml", ReadLicenseTimestamp(),
						      "com.sitescape.conference.module.AdvancedTelephony");
    if(licenseResult < 0)
    {
	return Failed;
    }

    if (provider)
    {
        IString param, val;
        
        param = "PromptDir";
        val = config_get_one(conf, JCOMPNAME, "promptDir", 0);
        if (val)
            provider->set_parameter(param, val);
        
        param = "RecordingDir";
        val = config_get_one(conf, JCOMPNAME, "recordingDir", 0);
        if (val)
            provider->set_parameter(param, val);

	if(licenseResult == 0) {
	    param = "AdvancedTelephony";
	    val = "1";
	    provider->set_parameter(param, val);
	}
    }
    else
#endif
    {
        log_status(ZONE, "No <provider> section; using the 'stub' provider");
            
        stub = true;
            
        IThread::initialize();
        IThread thread(test, NULL);
    }
    

    return Ok;
}

char *i_itoa(int i, char *buffer)
{
    sprintf(buffer, "%d", i);
    return buffer;
}

// Get string value from xmlrpc value, optionally checking that string
// only contains alphanumeric characters.
char *get_string_value(xmlrpc_env *env, xmlrpc_value *val, bool validate)
{
    char *pstr = NULL;
    xmlrpc_parse_value(env, val, "s", &pstr);
    XMLRPC_FAIL_IF_FAULT(env);

    if (validate)
    {
        char *p2 = pstr;
        for (;*p2;p2++)
        {
            if (!isalnum(*p2) && *p2 != '.')
            {
                xmlrpc_env_set_fault(env, ParameterParseError, "Error parsing function input:  illegal characters in field name");
                goto cleanup;
            }
        }
    }
    
cleanup:
    return pstr;
}


// Reset
// Parameters:
//   SECRET, CONTROLLEREPOCH
// Returns:
//   0
xmlrpc_value *reset(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	char * secret;
	int new_epoch;
    xmlrpc_value *output = NULL;
    int status = OK;

    xmlrpc_parse_value(env, param_array, "(si)", &secret, &new_epoch);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.reset");
        goto cleanup;
    }

	log_status(ZONE, "Voice service reset requested (%d)", new_epoch);

	// Get secret from config
	if (!strcmp(secret, "zQQz98"))
	{
		if (new_epoch != controller_epoch)
		{
			log_status(ZONE, "Resetting service");
			controller_epoch = new_epoch;
			
			// Like a reset, so set epoch
			epoch = time(NULL);

            // Register with controller.
			JC_connected();

            // Re-register all active calls.
			if (!stub)
				status = voice.reset();
		}
	}
	else
		status = Failed;

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to reset voice service");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to reset voice");

    return output;
}

// Ping
// 
// Parameters:
//    none
// Returns:
//    0 - if accepting requests
xmlrpc_value *ping(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    xmlrpc_value *output = NULL;

	log_debug(ZONE, "Voice pinged");

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", 0);
    XMLRPC_FAIL_IF_FAULT(env);
    
cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to ping voice");

    return output;
}

// Register meeting.
// Parameters:
//   MEETINGID, EXPECTEDPARTICIPANTS, PRIORITY, MAINSUBMEETINGID, CALLSUBID
// Returns:
//   0
xmlrpc_value *register_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *meetingid, *mainsubid, *callsubid;
    int participants, priority, start_time;
    xmlrpc_value *output = NULL;
    int status = OK;

    xmlrpc_parse_value(env, param_array, "(siissi)", &meetingid, &participants, &priority, &mainsubid, &callsubid, &start_time);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.register_meeting");
        goto cleanup;
    }

	log_debug(ZONE, "Register meeting %s", meetingid);

	if (!stub)
	{
		status = voice.register_meeting(meetingid, participants, priority, start_time);
		if (status == OK)
			status = voice.register_submeeting(meetingid, mainsubid, participants, true);
		if (status == OK && *callsubid)
			status = voice.register_submeeting(meetingid, callsubid, 2, false);
	}
	
    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(is)", 0, "");
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to register meeting");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to register meeting");

    return output;
}

// Close meeting.
// Parameters:
//   MEETINGID
// Returns:
//   0
xmlrpc_value *close_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *meetingid;
    xmlrpc_value *output = NULL;
    int status = OK;

    xmlrpc_parse_value(env, param_array, "(s)", &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.close_meeting");
        goto cleanup;
    }

	log_debug(ZONE, "Close meeting %s", meetingid);

	if (!stub)
		status = voice.close_meeting(meetingid);
	
    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to close meeting");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to close meeting");

    return output;
}

// Register submeeting.
// Parameters:
//   MEETINGID, SUBMEETINGID, EXPECTEDPARTICIPANTS
// Returns:
//   0
xmlrpc_value *register_submeeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *meetingid, *subid;
    int participants;
    xmlrpc_value *output = NULL;
    int status = OK;

    xmlrpc_parse_value(env, param_array, "(ssi)", &meetingid, &subid, &participants);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.register_submeeting");
        goto cleanup;
    }

	log_debug(ZONE, "Register submeeting %s in %s", subid, meetingid);

	if (!stub)
		status = voice.register_submeeting(meetingid, subid, participants, false);

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to register submeeting");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to register submeeting");

    return output;
}

// Modify submeeting.
// Parameters:
//   MEETINGID, SUBMEETINGID, OPTIONS
// Returns:
//   0
xmlrpc_value *modify_submeeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *meetingid, *subid;
    int options;
    int status = Ok;
    xmlrpc_value *output = NULL;
    
    xmlrpc_parse_value(env, param_array, "(ssi)", &meetingid, &subid, &options);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.modify_submeeting");
        goto cleanup;
    }

	log_debug(ZONE, "Modify submeeting %s in %s w/ options=%d",
              subid, meetingid, options);

	if (!stub)
		status = voice.modify_submeeting(meetingid, subid, options);
	
    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to modify submeeting");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to modify submeeting");

    return output;
}

// Reset recording.
// Parameters:
//   MEETINGID, SUBMEETINGID
// Returns:
//   0
xmlrpc_value *reset_recording(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *meetingid, *subid;
    int status = Ok;
    xmlrpc_value *output = NULL;
    
    xmlrpc_parse_value(env, param_array, "(ss)", &meetingid, &subid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.reset_recording");
        goto cleanup;
    }

	log_debug(ZONE, "Reset recording for submeeting %s in %s",
              subid, meetingid);

	if (!stub)
		status = voice.reset_recording(meetingid, subid);
	
    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to reset recording");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to reset recording");

    return output;
}

// Close submeeting.
// Parameters:
//   MEETINGID, SUBMEETINGID
// Returns:
//   0
xmlrpc_value *close_submeeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *meetingid, *submeetingid;
    xmlrpc_value *output = NULL;
    int status = OK;

    xmlrpc_parse_value(env, param_array, "(ss)", &meetingid, &submeetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.close_submeeting");
        goto cleanup;
    }

	log_debug(ZONE, "Close submeeting %s in %s", submeetingid, meetingid);

	if (!stub)
		status = voice.close_submeeting(meetingid, submeetingid);
	
    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to close submeeting");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to close submeeting");

    return output;
}


// Connect call to submeeting.
// Parameters:
//   CALLID, MEETINGID, SUBMEETINGID, HOLD
// Returns:
//   0
xmlrpc_value *connect_call(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *callid, *meetingid, *submeetingid;
	xmlrpc_bool hold;
	xmlrpc_bool grant_moderator;
    int mute;
    xmlrpc_value *output = NULL;
    int status = OK;

    xmlrpc_parse_value(env, param_array, "(sssbbi)", &callid, &meetingid, &submeetingid, &hold, &grant_moderator, &mute);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.connect_call");
        goto cleanup;
    }

	log_debug(ZONE, "Connect call %s to %s", callid, meetingid);

	if (!stub)
		status = voice.connect_call(meetingid, submeetingid, callid, hold != 0, grant_moderator != 0, mute != 0);

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to connect call");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to connect call");

    return output;
}


// Make call.
// Parameters:
//   USERID, USERNAME, MEETINGID, SUBMEETINGID, PHONE, EXTENSION, OPTIONS, SUPERVISINGCALLID, PARTICIPANTID
// Returns:
//   0
xmlrpc_value *make_call(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *userid, *username, *callid, *meetingid, *submeetingid, *phone, *extension, *partid;
	int options;
    xmlrpc_value *output = NULL;
    int status = OK;

    xmlrpc_parse_value(env, param_array, "(ssssssiss)", &userid, &username, &meetingid, &submeetingid, &phone, &extension, &options, &callid, &partid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.make_call");
        goto cleanup;
    }

	log_debug(ZONE, "Make call to %s for %s (%d)", phone, userid, options);

	if (!stub)
		status = voice.make_call(userid, username, meetingid, submeetingid, phone, extension, options, callid, partid);

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
        switch (status)
        {
            case NoResource:
                xmlrpc_env_set_fault(env, NoAvailResource, "No available system resources to make call");
                break;
                
            default:
                xmlrpc_env_set_fault(env, Failed, "Unable to make call");
                break;
        }
        
	}

  cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to make call");

    return output;
}

xmlrpc_value *notify_participant(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *userid, *username, *mtgid, *partid;
    int is_moderator, notify_by_phone, call_options;
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ssssiii)",
                       &userid, &username, &mtgid, &partid,
                       &is_moderator, &notify_by_phone, &call_options);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.notify_participant");
        goto cleanup;
    }

    log_debug(ZONE, "notify_participant(userid= %s, username= %s, mtgid= %s called", userid, username, mtgid);
    
    // Place any voice bridge specific calls here

    // Create return value.
    
    output = xmlrpc_build_value(env, "(i)", status);

  cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to notify_participant");
    
    return output;
}

// Disconnect call from submeeting.
// Parameters:
//   CALLID, MEETINGID, SUBMEETINGID
// Returns:
//   0
xmlrpc_value *disconnect_call(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *callid, *meetingid, *submeetingid;
    xmlrpc_value *output = NULL;
    int status = OK;

    xmlrpc_parse_value(env, param_array, "(sss)", &callid, &meetingid, &submeetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.disconnect_call");
        goto cleanup;
    }

	log_debug(ZONE, "Disconnect call %s", callid);

	if (!stub)
		status = voice.disconnect_call(meetingid, submeetingid, callid);

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to disconnect call");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to disconnect call");

    return output;
}


// Move call to another submeeting.
// Parameters:
//   CALLID, MEETINGID, SUBMEETINGID, NEWSUBMEETINGID
// Returns:
//   0
xmlrpc_value *move_call(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *callid, *meetingid, *submeetingid, *newsubid;
    xmlrpc_value *output = NULL;
    int status = OK;

    xmlrpc_parse_value(env, param_array, "(ssss)", &callid, &meetingid, &submeetingid, &newsubid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.move_call");
        goto cleanup;
    }

	log_debug(ZONE, "Move call %s to submeeting %s", callid, newsubid);

	if (!stub)
		status = voice.move_call(meetingid, submeetingid, callid, newsubid);

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to move call");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to move call");

    return output;
}


// Set volue for call
// Paramters:
//    CALLID, MEETINGID, SUBMEETINGID, VOLUME, GAIN, MUTE
// Result:
//    VOLUME, GAIN, MUTE, 0
xmlrpc_value *set_volume(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *callid, *meetingid, *submeetingid;
	int volume, gain, mute;
    xmlrpc_value *output = NULL;
    int status = OK;

    xmlrpc_parse_value(env, param_array, "(sssiii)", &callid, &meetingid, &submeetingid, &volume, &gain, &mute);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.set_volume");
        goto cleanup;
    }

	log_debug(ZONE, "Setting volume for %s", callid);

	if (!stub)
		status = voice.set_volume(callid, meetingid, submeetingid, volume, gain, mute);

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to set volume");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to set volume");

    return output;
}

// Grant/revoke moderator status for call
// Paramters:
//    CALLID, MEETINGID, SUBMEETINGID, GRANT
// Result:
//    0
xmlrpc_value *grant_moderator(xmlrpc_env *env,
                              xmlrpc_value *param_array,
                              void *user_data)
{
    char *callid;
    char *meetingid;
    char *submeetingid;
    int grant;
    int status = Ok;
    xmlrpc_value *output = NULL;
    
    xmlrpc_parse_value(env, param_array,
                        "(sssi)",
                        &callid, &meetingid, &submeetingid, &grant);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError,
                             "Unable to parse argument to voice.grant_moderator");
        goto cleanup;
    }

	log_debug(ZONE, "%s moderator for %s",
              (grant ? "Grant" : "Revoke"), callid);

	if (!stub)
		status = voice.grant_moderator(callid, meetingid, submeetingid,
                                       grant);
    if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to grant/revoke moderator status");
	}
cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to grant/revoke moderator status");

    return output;
}

// Get volue for call
// Paramters:
//    CALLID, MEETINGID, SUBMEETINGID
// Result:
//    VOLUME, GAIN, MUTE, 0
xmlrpc_value *get_volume(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *callid, *meetingid, *submeetingid, *participantid;
	int volume, gain, mute;
    xmlrpc_value *output = NULL;
    int status = OK;

    xmlrpc_parse_value(env, param_array, "(ssss)", &callid, &meetingid, &submeetingid, &participantid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.get_volume");
        goto cleanup;
    }

	log_debug(ZONE, "Getting volume for %s", callid);

	if (!stub)
		status = voice.get_volume(callid, meetingid, submeetingid, volume, gain, mute);

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(iiiis)", volume, gain, mute, 0, participantid);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to get volume");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to get volume");

    return output;
}


// Roll call for meeting
// Parameters:
//   CALLID, MEETING
// Returns:
//   0
xmlrpc_value *roll_call(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *callid, *meeting;
    int status = Ok;
    xmlrpc_value *output = NULL;
    
    xmlrpc_parse_value(env, param_array, "(ss)", &callid, &meeting);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.modify_submeeting");
        goto cleanup;
    }

	log_debug(ZONE, "Start roll call for meeting %s on call %s",
              meeting, callid);

	if (!stub)
		status = voice.roll_call(callid, meeting);
	
    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to modify submeeting");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to modify submeeting");

    return output;
}

xmlrpc_value *get_ports(xmlrpc_env *env,
                        xmlrpc_value *param_array,
                        void *user_data)
{
    xmlrpc_value * output = NULL;
    
    IVector * ports = new IVector();
    
    voice.get_port_status(ports);
    
    IVectorIterator iter(*ports);

    xmlrpc_value * array = xmlrpc_build_value(env, "()");
    int crow = 0;
    for (IPortStatus * ps = (IPortStatus *)iter.get_first(); ps; ps = (IPortStatus *)iter.get_next())
    {

        char id[64];
        sprintf(id, "%d", ps->get_port_id());
        xmlrpc_value * outstatus = xmlrpc_build_value(env, "(ssbsi)",
                                                      id,
                                                      ps->get_part_id(),
                                                      ps->is_connected(),
                                                      ps->get_last_event(),
                                                      ps->get_last_event_time());
        xmlrpc_array_append_item(env, array, outstatus);
        xmlrpc_DECREF(outstatus);
        outstatus = NULL;
        ++crow;
    }

    output = xmlrpc_build_value(env, "(Viiii)", array, 0, crow, crow, 0);
    xmlrpc_DECREF(array);
    
    if (ports)
        delete ports;
    
    return output;
}

xmlrpc_value *reset_port(xmlrpc_env *env,
                         xmlrpc_value *param_array,
                         void *user_data)
{
    xmlrpc_value * output = NULL;
    const char *port_id;
    const char *part_id;
    int n_affected;

    xmlrpc_parse_value(env, param_array, "(ss)", &port_id, &part_id);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.reset_port");
        goto cleanup;
    }

    n_affected = voice.reset_port(port_id, part_id);

    output = xmlrpc_build_value(env, "(i)", n_affected);

  cleanup:
    return output;
}


// Controller reports error
// Paramters:
//	 CALLID, ERRORID, STATUS, MESSAGE
// Result:
//   0
xmlrpc_value *error(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *callid, *errorid, *msg;
	int error;
    xmlrpc_value *output = NULL;
    int status = OK;

    xmlrpc_parse_value(env, param_array, "(ssis)", &callid, &errorid, &error, &msg);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.get_volume");
        goto cleanup;
    }

	log_debug(ZONE, "Error for call %s: %s", callid, msg);

	if (!stub)
		status = voice.error(callid, errorid, error, msg);

    // Create return value.
	output = xmlrpc_build_value(env, "(i)", 0);
	XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable handle error report");

    return output;
}


void report_disconnect(Call *call)
{
    int status = rpc_make_call("report_disconnect", "controller", "controller.unregister_user", "(sss)",
        call->get_userid(), call->get_id(), call->get_meetingid());
    if (status != 0)
        log_error(ZONE, "Unable to dispatch call %s to controller", call->get_id());
}


void report_progress(Call *call, int progress)
{
	int ext = -1;
	
	switch (progress)
	{
		case 0:
			ext = 0;
			break;
		case ProgressDialing:
			ext = StatusDialing;
			break;
		case ProgressRinging:
			ext = StatusRinging;
			break;
		case ProgressBusy:
		case ProgressReorder:
			ext = StatusBusy;
			break;
		case ProgressSpecialInfo:
			ext = StatusSpecialInfo;
			break;
		case ProgressAnswered:
			ext = StatusAnswered;
			break;
		case ProgressSpeech:
			ext = StatusTalking;
			break;
	}

	SubMeeting * sm = call->get_submeeting();
	Meeting * m = sm ? sm->get_meeting() : NULL;

	if (ext >= 0 && m)
	{
		int status = rpc_make_call("report_progress", "controller", "controller.call_progress", "(sssi)",
                                   call->get_userid(), m->get_id(), call->get_id(), ext);
		if (status != 0)
			log_error(ZONE, "Unable to dispatch progress for call %s to controller", call->get_id());
	}
}

// ====
// JComponent callbacks.
// ====

extern "C" int JC_connected()
{
	// Let controller know we just started or reconnected.
	char buff[128];
	sprintf(buff, "voice:%s", voice.get_service_name());
	rpc_make_call("reg_service", "controller", "controller.register_service", "(si)", buff, epoch);

	return 0;
}

extern "C" void RPC_error(const char *id, xmlrpc_int32 fault_code, const char * message)
{
    log_error(ZONE, "*** Error processing RPC request: %s\n", message);

	const char * end = strchr(id, ':');
	if (end == NULL)
		return;
	int len = end - id;

	if (!strncmp(id, "find", len))
	{
		voice.on_setup_error(end + 1, message);
	} 
	else if (!strncmp(id, "report", len) ||
             !strncmp(id, "join", len) ||
             !strncmp(id, "togglehand", len) ||
             !strncmp(id, "togglevol", len) ||
             !strncmp(id, "togglelecture", len) ||
             !strncmp(id, "togglerecord", len) ||
             !strncmp(id, "togglejoinleave", len) ||
             !strncmp(id, "lockmtg", len) ||
             !strncmp(id, "mute", len) ||
             !strncmp(id, "placecall", len) ||
             !strncmp(id, "removesubmeeting", len) ||
             !strncmp(id, "endmeeting", len))
	{
		voice.on_controller_error(end + 1, fault_code);
	}
}

extern "C" void RPC_result(const char * id, xmlrpc_value * resval)
{
	int status = Ok;
	xmlrpc_env env;
	
    xmlrpc_env_init(&env);

	const char * end = strchr(id, ':');
	if (end == NULL)
		return;
	int len = end - id;

	if (!strncmp(id, "find", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);
		
		status = voice.on_pin_found(end + 1, &rs);
		if (status != Ok)
			voice.on_setup_error(end + 1, "Error setting up connection to meeting");
	}
	else if (!strncmp(id, "report", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);
		
		log_debug(ZONE, "Controller reports call %s registered", end + 1);

		voice.on_report_complete(end+1, &rs);
	}
	else if (!strncmp(id, "join", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

		status = voice.on_join(end+1, &rs);
		if (status != Ok)
		{
			voice.on_controller_error(end+1, status);
		}
	}
	else if (!strncmp(id, "togglehand", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

		status = voice.on_toggle_hand_complete(end+1, &rs);
		if (status != Ok)
		{
			voice.on_controller_error(end+1, status);
		}
	}
	else if (!strncmp(id, "togglevol", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

		status = voice.on_toggle_volume_complete(end+1, &rs);
		if (status != Ok)
		{
			voice.on_controller_error(end+1, status);
		}
	}
	else if (!strncmp(id, "togglelecture", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

		status = voice.on_toggle_lecture_mode_complete(end+1, &rs);
		if (status != Ok)
		{
			voice.on_controller_error(end+1, status);
		}
	}
	else if (!strncmp(id, "togglerecord", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

		status = voice.on_toggle_record_meeting_complete(end+1, &rs);
		if (status != Ok)
		{
			voice.on_controller_error(end+1, status);
		}
	}
	else if (!strncmp(id, "togglejoinleave", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

		status = voice.on_toggle_mute_join_leave_complete(end+1, &rs);
		if (status != Ok)
		{
			voice.on_controller_error(end+1, status);
		}
	}
	else if (!strncmp(id, "placecall", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

		status = voice.on_place_call_complete(end+1, &rs);
		if (status != Ok)
		{
			voice.on_controller_error(end+1, status);
		}
	}
	else if (!strncmp(id, "lockmtg", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

		status = voice.on_lock_meeting_complete(end+1, &rs);
		if (status != Ok)
		{
			voice.on_controller_error(end+1, status);
		}
	}
	else if (!strncmp(id, "mute", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

		status = voice.on_mute_complete(end+1, &rs);
		if (status != Ok)
		{
			voice.on_controller_error(end+1, status);
		}
	}
	else if (!strncmp(id, "endmeeting", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

		status = voice.on_end_meeting_complete(end+1, &rs);
		if (status != Ok)
		{
			voice.on_controller_error(end+1, status);
		}
	}
	else if (!strncmp(id, "removesubmeeting", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

		status = voice.on_remove_submeeting_complete(end+1, &rs);
		if (status != Ok)
		{
			voice.on_controller_error(end+1, status);
		}
	}
	
cleanup:
    if (env.fault_occurred)
    {
        char buffer[512];
        strcpy(buffer, "Result error: ");
        if (env.fault_string)
            strncat(buffer, env.fault_string, 512 - 16);
        else
            strcat(buffer, "unknown cause");
        RPC_error(id, Failed, buffer);
    }
    xmlrpc_env_clean(&env);
}

extern "C" int RPC_run()
{
	int status = Ok;
	
	if (!stub)
		status = provider->run();
	
	return status;
}

#ifndef _WIN32
void * worker(void * sock)
{
    int sockfd = (int)(long) sock;
    int len;
    char buff[1024];

    //  Get iicCmd keyword.  This ensures we don't try to interpret a ping to our port as a command...
    char   cIicCmd[ ] = {"iic:fetchmtg"};
    memset( buff, 0, 1024 );
    if (recv(sockfd, buff, 12, 0) == -1) {
        close(sockfd);
        log_error(ZONE, "worker: expecting iicCmd keyword - recv failure, errno=%d.", errno);
        return NULL;
    }
    if( strcmp( buff, cIicCmd )  !=  0 )
    {
        close(sockfd);
        log_error(ZONE, "worker: invalid iicCmd keyword received.");
        return NULL;
    }

    //  get the length of the name of the recording
    if (recv(sockfd, &len, 4, 0) == -1) {
        close(sockfd);
        log_error(ZONE, "worker: recv failure, errno=%d.", errno);
        return NULL;
    }
    len = ntohl(len);

    //  Sanity check of the length just received
    if( len <= 0  ||  len >= 1023 )
    {
        close(sockfd);
        log_error(ZONE, "worker: received invalid transmission length." );
        return NULL;
    }
    
    // get recording id
    if (recv(sockfd, buff, len, 0) == -1) {
        close(sockfd);
        log_error(ZONE, "worker: recv failure, errno=%d.", errno);
        return NULL;
    }

    buff[len]='\0';

    IString fn;        
    const char *recording_dir = provider->get_recording_dir();
    if (strlen(recording_dir) > 0)
    {
        fn = recording_dir;
        fn = fn + "/";
    }
    fn = fn + "conf-";
    fn = fn + buff;
    fn = fn + ".wav";

    log_debug(ZONE, "sending recording file, %s.\n", fn.get_string());    
    
    FILE * wavefile = fopen(fn.get_string(), "rb");
    if (wavefile!=NULL) {
        fseek(wavefile, 0, SEEK_END);
        len = ftell(wavefile);
        fseek(wavefile, 0, SEEK_SET);
        len = htonl(len);
        if (send(sockfd, &len, 4, 0) == -1) {
            close(sockfd);
            log_error(ZONE, "worker: send failure, errno=%d.", errno);
            fclose(wavefile);

            return NULL;
        }
        // send voice recording
        while (!feof(wavefile)) {
            len = fread(buff, 1, sizeof(buff), wavefile);
            if (send(sockfd, buff, len, 0) == -1) {
                log_error(ZONE, "worker: send failure, errno=%d.", errno);
                break;
            }
        }
        fclose(wavefile);
    }
    else {
        log_error(ZONE, "worker: could not open meeting recording file, %s.", fn.get_string());

        len = 0;
        if (send(sockfd, &len, 4, 0) == -1) {
            close(sockfd);
            log_error(ZONE, "worker: send failure, errno=%d.", errno);
            return NULL;
        }
    }

    close(sockfd);

    return NULL;
}

void * send_recording(void * pvoid)
{
    short port = (short)(long)pvoid;
    int sockfd, new_fd;  // listen on sock_fd, new connection on new_fd
    struct sockaddr_in my_addr;    // my address information
    struct sockaddr_in client_addr; // connector's address information
    socklen_t sin_size;
    int yes=1;

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
            log_error(ZONE, "send_recording: socket creation failure.");
            return NULL;
    }

    if (setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int)) == -1) {
            log_error(ZONE, "send_recording: setsockopt failure.");
            return NULL;
    }
        
    my_addr.sin_family = AF_INET;                // host byte order
    my_addr.sin_port = htons(port);                 // short, network byte order
    my_addr.sin_addr.s_addr = INADDR_ANY; // automatically fill with my IP
    memset(&(my_addr.sin_zero), '\0', 8);       // zero the rest of the struct

    if (bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) == -1) {
            log_error(ZONE, "send_recording: bind failure, errno=%d.", errno);
            return NULL;
    }

    if (listen(sockfd, SOMAXCONN) == -1) {
            log_error(ZONE, "send_recording: listen failure, errno=%d.", errno);
            return NULL;
    }

    while(1) {  // main accept() loop
        sin_size = sizeof(struct sockaddr_in);
        if ((new_fd = accept(sockfd, (struct sockaddr *)&client_addr,
                                                       &sin_size)) == -1) {
            log_error(ZONE, "send_recording: accept failure, errno=%d.", errno);
            continue;
        }

        IThread(worker, (void*)new_fd);
//        worker((void*)new_fd);
//        close(new_fd);
    }
    close(sockfd);

    return 0;
    
}
#endif

int main(int argc, char * argv[])
{
	fprintf(stderr, "IIC Voice Server 1.0   Copyright (C) 2007 SiteScape\n\n");

    int instance = 0;
    
	if (argc > 1)
		instance = atoi(argv[1]);
    
    config_t conf = config_new();

    config_load(conf, JCOMPNAME, instance, "/opt/iic/conf/config.xml");
    
	if (jc_init(conf, JCOMPNAME))
	{
		log_error(ZONE, "Unable to initialize voice\n");
		return -1;
	}

    if (voice_init(conf))
    {
        return -1;
    }
    
    jc_set_connected_callback(JC_connected);
	rpc_set_callbacks(RPC_result, RPC_error, RPC_run);

    IThread::initialize();
#ifndef _WIN32
    g_sending_thread = new IThread(send_recording, (void*)(int)g_voice_server_port, true);
#endif

	// Add methods to RPC server
    rpc_add_method("voice.reset", &reset, NULL);
    rpc_add_method("voice.register_meeting", &register_meeting, NULL);
    rpc_add_method("voice.register_submeeting", &register_submeeting, NULL);
    rpc_add_method("voice.close_meeting", &close_meeting, NULL);
    rpc_add_method("voice.close_submeeting", &close_submeeting, NULL);
    rpc_add_method("voice.connect_call", &connect_call, NULL);
    rpc_add_method("voice.disconnect_call", &disconnect_call, NULL);
    rpc_add_method("voice.move_call", &move_call, NULL);
    rpc_add_method("voice.make_call", &make_call, NULL);
    rpc_add_method("voice.notify_participant", &notify_participant, NULL);
    rpc_add_method("voice.set_volume", &set_volume, NULL);
    rpc_add_method("voice.get_volume", &get_volume, NULL);
    rpc_add_method("voice.error", &error, NULL);
    rpc_add_method("voice.ping", &ping, NULL);
    rpc_add_method("voice.grant_moderator", &grant_moderator, NULL);
    rpc_add_method("voice.modify_submeeting", &modify_submeeting, NULL);
    rpc_add_method("voice.roll_call", &roll_call, NULL);
    rpc_add_method("voice.get_ports", &get_ports, NULL);
    rpc_add_method("voice.reset_port", &reset_port, NULL);
    rpc_add_method("voice.reset_recording", &reset_recording, NULL);

	jc_run(false);

    if (!stub)
        provider->shutdown();

	return 0;
}

// ====
// Voice stub
// ====

void *test(void *param)
{
	fprintf(stderr, "*** Test mode, waiting for commands...\n");
	
	int ch;
	
	while (true)
	{
		ch = getchar();
		
		if (ch == 'q' || ch == -1)
			break;
		else if (ch == 'f')
		{
			char pin[MAX_ID_LENGTH];

			scanf("%s", pin);

			if (rpc_make_call("find_pin", "controller", "controller.find_pin", "(ss)", "sys:voice", pin))
				log_error(ZONE, "Unable to lookup pin %s", pin);
		}
		else if (ch == 'c')
		{
			char callid[MAX_ID_LENGTH];
			char userid[MAX_ID_LENGTH];
			char username[MAX_ID_LENGTH];
			
			scanf("%s %s %s", userid, callid, username);
            
			if (strcmp(username, "*") == 0) username[0] = '\0';

			int status = rpc_make_call("report_call", "controller", "controller.register_user", "(ssssss)",
				userid, username, "", callid, "", "");
			if (status != 0)
				log_error(ZONE, "Unable to dispatch call %s to controller", callid);
		}
		else if (ch == 'j')
		{
			char userid[MAX_ID_LENGTH];
			char callid[MAX_ID_LENGTH];
			char meetingid[MAX_ID_LENGTH];
			char partid[MAX_ID_LENGTH];

			scanf("%s %s %s %s", userid, callid, meetingid, partid);

			if (strcmp(partid, "*") == 0) partid[0] = '\0';

		    int status = rpc_make_call("join_call", "controller", "controller.join_meeting", "(sssss)",
				userid, callid, meetingid, partid, "");
			if (status != 0)
				log_error(ZONE, "Unable to join meeting %s to controller", meetingid);
		}
		else if (ch == 'd')
		{
			char callid[MAX_ID_LENGTH];
			char userid[MAX_ID_LENGTH];
			
			scanf("%s %s", userid, callid);
			
			int status = rpc_make_call("report_disconnect", "controller", "controller.unregister_user", "(sss)",
				userid, callid, "");
			if (status != 0)
				log_error(ZONE, "Unable to dispatch call %s to controller", callid);
		}
		else if (ch == 'p')
		{
			char callid[MAX_ID_LENGTH];
			char userid[MAX_ID_LENGTH];
			char meetingid[MAX_ID_LENGTH];
			int progress;
			int ext = -1;
			
			scanf("%s %s %s %d", userid, callid, meetingid, &progress);
			
			switch (progress)
			{
				case 0:
					ext = 0;
					break;
				case ProgressDialing:
					ext = StatusDialing;
					break;
				case ProgressRinging:
					ext = StatusRinging;
					break;
				case ProgressBusy:
				case ProgressReorder:
					ext = StatusBusy;
					break;
				case ProgressSpecialInfo:
					ext = StatusSpecialInfo;
					break;
				case ProgressAnswered:
					ext = StatusAnswered;
					break;
			}
			
			int status = rpc_make_call("report_progress", "controller", "controller.call_progress", "(sssi)",
				userid, meetingid, callid, ext);
			if (status != 0)
				log_error(ZONE, "Unable to dispatch progress for call %s to controller", callid);
		}
	}

    delete g_sending_thread;
    return NULL;
}

