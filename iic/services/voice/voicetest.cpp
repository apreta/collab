#include "voicetest.h"

#include "../../jcomponent/util/log.h"

void VoiceTest::set_parameter(const char *param_name, const char *param_val)
{
}

void VoiceTest::on_initialized()
{
        // shouldn't ever happen since we aren't connected to jabber
    log_debug(ZONE, "Initialized event received");
}

void VoiceTest::on_new_call(ICall * call)
{
    log_debug(ZONE, "Call received");
}

void VoiceTest::on_release_call(ICall * call)
{
}

void VoiceTest::on_event(IEvent * event)
{
}

void VoiceTest::on_error(IChannel * chan, int error, const char *message)
{
    log_error(ZONE, "Error from channel %d (%s)", (int)chan, message);
}

void VoiceTest::on_timeout(IChannel * chan)
{
}


