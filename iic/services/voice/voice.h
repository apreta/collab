/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef VOICE_HEADER
#define VOICE_HEADER

#include "voiceapi.h"
#include "../../util/irefobject.h"
#include "../../util/istring.h"
#include "../../util/ithread.h"
#include "timer.h"

#include "../common/common.h"

class SubMeeting;
class Meeting;
class Call;
class RPCResultSet;

typedef IRefObjectPtr<Call> spCall;
typedef IRefObjectPtr<SubMeeting> spSubMeeting;
typedef IRefObjectPtr<Meeting> spMeeting;

//void report_call(Call *call);
void report_disconnect(Call *call);
void report_progress(Call *call, int progress);


//
// Threading model notes:
//    Runs on single thread.  We get time slice periodically from RPC layer to check for voice events.
//    RPC commands and voice events are processed on same thread, so locking is not necessary.
//


// App states are tracked in call object
enum {
    AppStateAnswering,
    AppStateGreeting,
    AppStateRecordNamePrompt,
    AppStateRecordName,
	AppStateEnterPin,
	AppStateCheckPin,
	AppStateInvalidPin,
	AppStateMissingPin,
	AppStateReportCall,
    AppStateHoldingPrompt,
	AppStateJoiningPrompt,
	AppStateStartingPrompt,
	AppStateHolding,
	AppStateStarting,
	AppStateConference,
	AppStateError,
	AppStateGoodbye,
	AppStateDisconnect,
	AppStateAnnounce,
	AppStateEndAnnounce,
	AppStateEndingPrompt,
	AppStatePasswordPropmpt,
	AppStateEnterPassword,
	AppStateInvalidPassword,
	AppStateMissingPassword,
	AppStateCheckPassword,
	AppStateJoinMeeting,
	AppStateConnectCall,
	
	AppStateDialing,
	AppStateProceeding,
	AppStateOutboundGreeting,
	AppStateEnterAccept,
	AppStateRestartOutbound,

    // Roll call sequence
    AppStateRollCallPrompt,
    AppStateParticipantPrompt,

    // Outdialing sequence
    AppStateEnterDialNumber,
    AppStateDialNumber,

    AppStateDialExtension,
};

// Submeeting states.
enum {
	MeetingStateClosed = 0,
	MeetingStateWaiting,
	MeetingStateActive,
	MeetingStateAnnouncing,
};


class CallBase : public IReferencableObject
{
protected:
    // Externally referenced call id, e.g. voice:in-12 or voice:out-11.
	IString call_id;
	
public:
	CallBase(const char * id) :  call_id(id)
		{ }

    void lock()		{}
    void unlock()	{}

    enum { TYPE_CALL = 3451 };
    virtual int type()
        { return TYPE_CALL; }
    virtual unsigned int hash()
        { return call_id.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && call_id == ((CallBase*)obj)->call_id; }
};

// Participant connected by telephone (inbound or outbound call).
class Call : public CallBase
{
    ICall * call;

	IString pin;
    IString user_id;
    IString participant_id;
	IString username;
    IString meeting_id;
    IString sm_id;
	IString password;
    int options;
	bool hold;
	bool screened;
    bool moderator;
    int  mute;
    int  volume;
    int  gain;
    
    spSubMeeting submeeting;

    int state;
    int cur_participant; // for roll call
    char roll_call_prompt[32];
	int ringcount;
    int entry_attempts;

public:
    Call(const char * id, ICall * call = NULL, const char * user_id = NULL, const char * username = NULL);
    ~Call();

	int disconnect();

	void on_release();

    int get_state()                     { return state; }
    void set_state(int _s)              { state = _s; }

	SubMeeting * get_submeeting()		{ return submeeting;}
	void set_submeeting(SubMeeting *sm);
	
    const char * get_pin()				{ return pin; }
    void set_pin(const char * _pin)		{ pin = _pin; }

    const char * get_userid()			{ return user_id; }
    void set_userid(const char * id)	{ user_id = id; }

    const char * get_participantid()    { return participant_id; }
    void set_participantid(const char * id)	{
        participant_id = id;
        if (call) call->set_participantid(id);
    }

    const char * get_username()			{ return username; }
    void set_username(const char * id)	{ username = id; }

    const char * get_meetingid()        { return meeting_id; }
    void set_meetingid(const char * id) { meeting_id = id; }

    const char * get_submeetingid()			{ return sm_id; }
    void set_submeetingid(const char * id)	{ sm_id = id; }

	const char * get_password()			{ return password; }
	void set_password(const char * pw)	{ password = pw; }

	int get_options()					{ return options; }
	void set_options(int _options)
        {
            options = _options;
            if (call) call->set_options(_options);
        }

    bool next_roll_call_participant();
    const char * get_roll_call_prompt() { return roll_call_prompt; }
    void reset_roll_call() { cur_participant = -1; }

	bool get_hold()						{ return hold; }
	void set_hold(bool _hold)			{ hold = _hold; }
	
	bool get_screened()					{ return screened; }
	void set_screened(bool f)			{ screened = f; }
	
	bool is_moderator()					{ return moderator; }
	void set_is_moderator(bool f)		{ moderator = f; }

    bool is_muted()						{ return (mute == 1); }
    void set_mute(bool f)				{ mute = (f ? 1 : 0); }
	
	int add_ring()						{ return ++ringcount; }

    // Makes sure this call is still the current call on channel.
    IChannel * get_channel();

    ICall * get_call()					{ return call; }
    const char * get_id()               { return call_id; }

    bool too_many_attempts()
        {
            bool toomany = ++entry_attempts > MAX_ENTRY_ATTEMPTS;
            if (toomany)
                entry_attempts = 0;
            return toomany;
        }
    void reset_attempts() { entry_attempts = 0; }

    void set_volume(int _volume, int _gain, int _mute)
        {
            if (_volume != -1) volume = _volume;
            if (_gain != -1) gain = _gain;
            if (_mute != -1) mute = _mute;
        }

    void get_volume(int & _volume, int & _gain, int & _mute)
        {
            _volume = volume;
            _gain = gain;
            _mute = mute;
        }
};


class SubMeetingBase : public IReferencableObject
{
protected:
    IString sub_id;
	
public:
	SubMeetingBase(const char * id) : sub_id(id)
		{ }

    void lock()		{}
    void unlock()	{}

    enum { TYPE_SMEETING = 3453 };
    virtual int type()
        { return TYPE_SMEETING; }
    virtual unsigned int hash()
        { return sub_id.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && sub_id == ((SubMeetingBase*)obj)->sub_id; }
};


// Submeeting within meeting.
// Each submeeting corresponds to a voice conference.
class SubMeeting : public SubMeetingBase
{
    spMeeting meeting;
    int expected_participants;
    unsigned options;
    bool main;
    IString recording_id;

    IConference * conference;
 

    IList calls;
	IList waiting_calls;
	IList outbound_calls;

	int state;

public:
    SubMeeting(const char *id, Meeting * owner, int expected_participants, bool main, const char *recording_id);
	~SubMeeting();

    int create();
    void close();
    void reset();

	int add_outbound_call(Call *p);

    int add_call(Call *p);
	int connect_call(Call * p);
    int remove_call(spCall p, bool prompt = true);
	int get_num_connected();

	int queue_call(Call * p);
	void release_first();
	void release_holding();
	int get_num_queued();

	int play_announcement(Call * call, const char * filename);
	void on_announcement_end(Call * call);

    int menu_hold(Call * call);
    int menu_release_hold(Call * call);

	int set_volume(Call * call, int volume, int gain, int mute);
	int get_volume(Call * call, int & volume, int & gain, int &mute);

    void set_options(unsigned _options);
    int  get_options()    		 	{ return options; }
	void reset_recording();

	int grant_moderator(Call * call, int grant);

    const char *get_id()    { return sub_id; }
    Meeting *get_meeting()  { return meeting; }
	int get_state()			{ return state; }
	bool is_active()		{ return state >= MeetingStateActive; }
	bool is_announcing()	{ return state == MeetingStateAnnouncing; }
    bool is_main()          { return main; }
	bool is_call_conferenced(Call * c);

	Call * find_call(const char * id);
	Call * find_waiting_call(const char * id);
};

// Active meeting.
class Meeting : public IReferencableObject
{
	IString meeting_id;

    int expected_participants;
	int priority;
    int flags;
    int start_time;
    
    IHash all_participants;

    IList submeetings;
	bool closed;

public:
    Meeting(const char *id, int max_participants, int priority, int flags, int start_time);
	~Meeting();
	
    void lock()		{}
    void unlock()	{}

    void close();
    void reset();
    
    // Manage submeetings.
    int register_submeeting(const char * id, int expected_participants, bool main);
    int modify_submeeting(const char * id, unsigned options);
    int close_submeeting(const char * id);
	int reset_recording(const char * id);

    const char * get_id()   { return meeting_id; }
    bool is_closed()		{ return closed; }

    SubMeeting * find_submeeting(const char * id);
    SubMeeting * find_mainmeeting();

    enum { TYPE_MEETING = 3452 };
    virtual int type()
        { return TYPE_MEETING; }
    virtual unsigned int hash()
        { return meeting_id.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && meeting_id == ((Meeting*)obj)->meeting_id; }
};


// Main voice interface.
class Voice : public IApplication
{
	IString service_name;
    IHash meetings;
    IHash calls;

    static int call_id_seq;
	static int anon_id_seq;

	static int join_meeting_timeout;
	static int menu_command_timeout;
	static int delay_after_dial;

public:
    Voice();       
    virtual ~Voice()  { };

	void set_service_name(const char *name);
    const char * get_service_name() 
        { return (const char *)service_name; }

	int set_parameter(IString & param, IString & value);

    // External voice service API
	int reset();
	
    int register_meeting(const char * id, int participants, int priority, int start_time);
    int close_meeting(const char * id);

    int register_submeeting(const char * meeting, const char * sub_id, int expected_participants, bool main);
    int modify_submeeting(const char * meeting, const char * sub_id, int options);
    int close_submeeting(const char * meeting, const char * sub_id);
	int reset_recording(const char * meeting, const char * sub_id);

    int connect_call(const char * meeting, const char *sub_id, const char *call_id, bool hold, bool grant_moderator, bool mute);
	int connect_call(Call * c);
    int disconnect_call(const char * meeting, const char *sub_id, const char *call_id);
	int move_call(const char * meeting, const char *sub_id, const char *call_id, const char *new_sub_id);

    int make_call(const char * user_id, const char * username, const char * meeting, const char * sub_id, const char * address, const char * extension, int options, const char * call_id, const char *part_id);

	int set_volume(const char * call_id,  const char * meeting, const char * sub_id, int volume, int gain, int mute);
	int get_volume(const char * call_id,  const char * meeting, const char * sub_id, int & out_vol, int & out_gain, int & out_mute);

    int grant_moderator(const char * call_id, const char * meeting, const char *sub_id, int grant);

    int roll_call(const char * call_id, const char * meeting);

    int get_port_status(IVector * ports);
    
	int error(const char * call_id, const char * error_id, int error, const char * msg);

	// Application state machine
	int next_state(Call * call);
	void handle_state(Call * call, int state);
	bool check_accept(Call * call);
	int check_password(IChannel * channel);
	int find_pin(IChannel * channel);
	int on_pin_found(const char * callid, RPCResultSet * rs);
	void on_setup_error(const char * callid, const char * message);
	void on_controller_error(const char * callid, int fault_code);

	void join_meeting(Call * call);
	int on_join(const char * callid, RPCResultSet * rs);

	void report_call(Call * call);
	void on_report_complete(const char * callid, RPCResultSet * rs);

	void toggle_hand(Call * call);
	int on_toggle_hand_complete(const char * callid, RPCResultSet * rs);

	void mute(Call * call, int state);
	int  on_mute_complete(const char * callid, RPCResultSet * rs);

	void toggle_volume(Call * call);
	int on_toggle_volume_complete(const char * callid, RPCResultSet * rs);

	void toggle_lecture_mode(Call * call);
	int on_toggle_lecture_mode_complete(const char * callid, RPCResultSet * rs);

	void lock_meeting(Call * call, int state);
	int on_lock_meeting_complete(const char * callid, RPCResultSet * rs);

	void end_meeting(Call * call);
	int on_end_meeting_complete(const char * callid, RPCResultSet * rs);

    int place_call(Call * call);
    int on_place_call_complete(const char * callid, RPCResultSet * rs);

	void toggle_record_meeting(Call * call);
	int on_toggle_record_meeting_complete(const char * callid, RPCResultSet * rs);

	void toggle_mute_join_leave(Call * call);
	int on_toggle_mute_join_leave_complete(const char * callid, RPCResultSet * rs);

	void remove_submeeting(Call * call, bool remove_parts);
	int on_remove_submeeting_complete(const char * callid, RPCResultSet * rs);

    int reset_port(const char * port_id, const char * part_id);

    // Handle voice events
    void on_initialized();
    void on_new_call(ICall * call, const char * pin, int options);
    void on_release_call(ICall * call);
    void on_event(IEvent * event);
    void on_error(IChannel * chan, int code, const char * msg);
	void on_timeout(IChannel * chan);

    Meeting * find_meeting(const char * id);
    Call * find_call(const char * id);
};

#endif
