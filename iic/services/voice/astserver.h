#include <astxx/manager.h>
#include <ithread.h>
#include <istring.h>
#include <irefobject.h>

#ifndef _ASTSERVER_H
#define _ASTSERVER_H

#define AST_SUCCESS 0
#define AST_FAIL -1

#define PROVIDER_SIP 0
#define PROVIDER_IAX 1
#define PROVIDER_ZAP 2

#define PARTICIPANT_MASK_MUTE   0x0001
#define PARTICIPANT_MASK_FIRST  0x0002
#define PARTICIPANT_MASK_MOVE   0x0004
#define PARTICIPANT_MASK_RECORD 0x0008


IString generateNameRecordFilename(const char *astChanID);

class AsteriskEventHandler
{
 public:
    AsteriskEventHandler(){};
    virtual ~AsteriskEventHandler(){};

    virtual void onAsteriskNewchannel(void *userData, const char *channelID, const char *callID) = 0;
    virtual void onAsteriskNewstate(void *userData, const char *channelID, int chanState) = 0;
    virtual void onAsteriskHangup(void *userData, const char *channelID) = 0;
    virtual void onAsteriskConferenceJoin(void *userData, const char *channelID, const char *conferenceID) = 0;
    virtual void onAsteriskConferenceState(void *userData, const char *channelID, int confState) = 0;
    virtual void onAsteriskConferenceDTMF(void *userData, const char *channelID, char dtmf) = 0;
    virtual void onAsteriskConferenceLeave(void *userData, const char *channelID, const char *conferenceID) = 0;
    virtual void onAsteriskConferencePin(void *userData, const char *channelID, const char *conferencePIN) = 0;
    virtual void onAsteriskOutgoingAccept(void *userData, const char *channelID, const char *iicChanID) = 0;
    virtual void onAsteriskConferenceSoundComplete(void *userData, const char *channelID, const char *fileName) = 0;
    virtual void onAsteriskServerTransfer(void *userData, const char *channelID, const char *userPin) = 0;
};

class AsteriskServer : public IReferencableObject
{
 public:
    AsteriskServer(AsteriskEventHandler *eventHandler, int externalProvider, int maxParticipants, const char * ipAddr, unsigned long port=5038);  
    ~AsteriskServer();

    IString name;

    int SetParam(const char * paramName, const char * paramValue);
    void Initialize(void);
    int Connect(const char * username, const char * secret);
    
    int Disconnect(void);
    
    int AppConfCreateConference(const char * confID);
    int AppConfDeleteConference(const char * confID);
    int AppConfAddToConference(const char * astChanID, const char * confID, unsigned int participantMask);
    int AppConfRemoveFromConference(const char * astChanID, const char * confID);
    int AppConfPlayToConference(const char * confPIN, const char * fileName);
    int AppConfPlayToParticipant(const char * astChanID, const char * fileName);
    int AppConfMuteParticipant(const char * astChanID, const char * confID);
    int AppConfUnmuteParticipant(const char * astChanID, const char * confID);
    int AppConfMakeCall(const char * dialString, const char *iicCallID);
    int AppConfPinLookupFail(const char *astChanID);
    int AppConfInvalidPin(const char *astChanID);
    int AppConfPreConfHold(const char *astChanID);
    int AppConfStartRecording(const char *confID, const char *fileName);
    int AppConfSuspendRecording(const char *recordChannelID);
    int AppConfResumeRecording(const char *recordChannelID);
    int AppConfServerTransfer(const char *astChanID, const char *userPin, const char *ipAddr);
    IString AppConfGetConferenceList(void);
    IString AppConfGetParticipantList(const char *confID);
    int Hangup(const char *astChanID);

    IString GetChannelVariable(const char *astChanID, const char *variableName);
    int SetChannelVariable(const char *astChanID, const char *key, const char *value);

    int GetMaxParticipants(void) { return m_maxParticipants; };
    int GetNumParticipants(void) { return m_numParticipants; };

    astxx::manager::connection m_connection;
    IThread *m_eventThread;
    AsteriskEventHandler *m_eventHandler;

    int m_extProvider;
    IMutex m_connMutex;

    int m_maxParticipants;
    int m_numParticipants;
    IString m_ipAddress;
    
    void processEvents(void);
    IString sendCommand(const char *cmdString);
    void astxxEventHandler(astxx::manager::message::event e);
    IString GetIPAddress(void) { return m_ipAddress; };
    
    void lock()		{}
    void unlock()	{}

    enum { TYPE_ASTERISKSERVER = 3454 };
    virtual int type()
        { return TYPE_ASTERISKSERVER; }
    virtual unsigned int hash()
        { return name.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && name == ((AsteriskServer*)obj)->name; }

};

#endif
