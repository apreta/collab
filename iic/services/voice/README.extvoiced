OVERVIEW
--------
This directory contains the following files:

	Makefile.extvoiced
	extvoiced.cpp

The file extvoiced.cpp contains a stub implementation
of the voice bridge RPC interface.  The RPC handlers all
return success and do nothing except log the procedure name
and the arguments given by the remote caller.  This file also
contains a simple command line interpreter that can notify
other system components of voice bridge events (i.e. inbound
call connected, call progress, etc.).

The file Makefile.extvoiced is used to build the stub voice
bridge (see BUILDING below).

BUILDING
--------

1. On a development machine, install iic-extbridge-devel-<version>-1.i386.rpm using
	sudo rpm -Uhv iic-xmlrpc-<version>-1.i386.rpm iic-extbridge-devel-<version>-1.i386.rpm
2. Copy the directory /opt/iic/src/extbridge someplace writeable by you (e.g. your home dir)
3. In the extbridge directory, build the voice bridge API stub server:
	make -f Makefile.extvoiced
4. You should now find an executable file 'extvoiced' in this directory.

TESTING
-------

1. The hostname of the development machine must be iic.homedns.org and
   the XML router must be iic.homedns.org as well.  The web portal name is
   iic-portal.homedns.org as well.  The developer will have to override
   the host mappings of these hosts on the development machine and any Windows
   machines that will run the Zon client.
2. Unpack the Zon distribution tarball
3. Copy the 'extvoiced' binary to sitescape-zon/cluster-prototype
4. Run ./install.sh to install the Zon servers with the external bridge API
     - SiteScape recommends you change the logging level to 'debug' so that
       you can get detailed information about RPC calls in the system
       log files in /var/log/iic
5. To access the command line interface to the voice bridge:
	- Determine the status of the voice bridge on the voice bridge hosts using:
		sudo /etc/rc.d/init.d/iicvoice status
	- If the voice bridge is running, stop it using:
		sudo /etc/rc.d/init.d/iicvoice stop
	- Start the voice bridge from your terminal window using:
		/opt/iic/bin/extvoiced
        - You should now see some greeting text followed by a prompt ">>> "

  NOTE:
   - In order to observe the operation of the meeting controller, you
     can set its log level to 'debug' via the admin console.

ZON CLIENT NOTES:

API USAGE OVERVIEW
------------------

For details about the voice.*() and controller.*() API calls,
see the comments in extvoiced.cpp.  The controller.*() API calls
outlined below are initiated by commands given on a command
line interface implemented in extvoiced.

In the descriptions below, "-->" indicates a call to the voice
bridge.  "<--" indicates a call by the voice bridge to other
system components.

Active Meeting Lifecycle
------------------------

The meeting controller bounds the lifetime of an active
meeting using the following two calls.

	--> voice.register_meeting()
		.
		.
		.
		.
	--> voice.close_meeting()

Initiating an Outbound Conference Call
--------------------------------------
	--> voice.make_call()		// call is initiated by meeting controller
	<-- controller.register_user()	// dialing is complete
	<-- controller.call_progress()  // call progress state is reported (ring, busy, SIT, answer, etc.)
			.	
			.	
			.	
        <-- controller.join_meeting()	// call is ready to be joined to the meeting/submeeting
        --> voice.connect_call()	// controller confirms that call is connected to meeting/submeeting

Ending a Call
-------------
	// Call disconnected on bridge
	<-- controller.unregister_user()	// notify controller of dropped call

	// Call disconnected by Zon client
	--> voice.disconnect_call()		// controller sends disconnect call notification
	<-- controller.unregister_user()	// voice bridge confirms disconnect with controller

Initiating a 1-to-1 Call
------------------------
	--> voice.make_call()		// call is initiated by meeting controller
	<-- controller.register_user()	// dialing is started; confirm call with controller
	<-- controller.join_meeting()   // dialing delay has elapsed
	--> voice.connect_call()        // confirmation that the calls is joined to
					// the submeeting

Submeeting Lifecycle
--------------------
	--> voice.register_submeeting()
		.
	        .
	        .
	--> voice.close_submeeting()

    NOTE:
	The register_meeting() call also specifies the
	main submeeting for the conference and optionally
	the submeeting of the 1-to-1 call that started
	the meeting.  The controller will not call
	voice.register_submeeting() for these.

SubMeeting Management
---------------------
	--> voice.move_call		// move a joined call from one submeeting to another
	--> voice.modify_submeeting	// change the attributes of a submeeting
					// (for example whether it is being recorded or not)
	--> voice.reset_recording	// clears any audio recording associated with the submeeting

Call Attribute Management
-------------------------
	--> voice.set_volume		// set conference volume and muting
	--> voice.grant_moderator	// grant/revoke moderator functionality
	--> voice.get_volume		// get conference volume and muting

Administrative Interface
------------------------
	--> voice.get_ports
	--> voice.reset_port

Miscellaneous
-------------
	--> voice.ping  // informs caller whether the voice bridge is accepting commands
	                // sent periodically by the meeting controller
	--> voice.reset // informs voice bridge about whether the meeting controller
			// state has been reset (i.e. whether the meeting controller
			// has the set of active meetings)



EXAMPLE USAGE OF THE STUB EXTERNAL VOICE BRIDGE
-----------------------------------------------

1. Start the Zon client on a Windows machine (download from http://<portal-host>/imidio/downloads/imidiolaunch.exe)
2. Exit the Zon client w/o signing on
3. Edit /Program Files/Imidio/iic.opt and change IICServer and URL's for your development system
4. Re-start the Zon client and log on as 'admin' with password 'admin'


$ sudo /etc/rc.d/init.d/iicvoice status
$ sudo /etc/rc.d/init.d/iicvoice stop    # if status reports the bridge as running
$ rm -f /var/log/iic/voice.log*          # remove logs owned by root so that you can write them
$ /opt/iic/bin/extvoiced
[16384] Wed Apr  6 19:47:14 2005 extvoiced.cpp:1906 voice bridge starting
voice bridge starting
Setting service name to voice
unable to load voice parameters, using defaults
Running the command line interpreter
Test mode (type 'h' for a list of commands)
>>> h
COMMAND OVERVIEW
        q -
                quit voice bridge
        f <pin> (controller.find_pin) -
                Ask controller to validate a pin
        c <participant-pin> <callid> <username> (controller.register_user) -
                register the given call under the user's id and name
        j <sessid> <callid> <meetingid> <partid> (controller.join_meeting) -
                Join the given participant to the meeting
        d <sessid> <callid> <meetingid> (controller.unregister_user) -
                Report a disconnected call to the meeting controller
        p <sessid> <callid> <mtgid> <progress> (controller.call_progress) -
                Report call progress for a call
                Valid <progress> values:
                        clear status = 0
                        dialing      = 100
                        ringing      = 101
                        busy         = 102
                        reorder      = 103
                        special info = 104
                        answered     = 105
                        speech       = 106
                        remote hangup= 107
                        error        = 108
>>> 

****      IN THE ZON CLIENT
****
****       Start the admin's instant meeting
****

voice.register_meeting(mtgid= 10, mainsubmtgid= submeeting:1, callsubmtgid= , start= 1112831323) called
>>>
voice.make_call(userid= user:2, username= admin@test3.homedns.org, mtgid= 10, submtgid= submeeting:1, phone= 9784502236,
ext= , options= 1, supcallid= , partid=part:11) called
>>> c pin:11 voice:out-2@voice admin@test3.homedns.org
Reporting call for pinid= pin:11 callid= voice:out-2@voice username=admin@test3.homedns.org
>>>
controller reports user given in report:1 as registered.
>>>
User sessid pin:11 for partid part:11
>>> p pin:11 voice:out-2@voice 10 100
Reporting call progress: sessid= pin:11 callid= voice:out-2@voice meetingid= 10 progress= 100

****   NOTICE:
****	Phone icon changes in the Zon client meeting window (has an exclamation point)
****	If you make the meeting window wider, you can also see the extended status info

>>> p pin:11 voice:out-2@voice 10 101
Reporting call progress: sessid= pin:11 callid= voice:out-2@voice meetingid= 10 progress= 101
>>> p pin:11 voice:out-2@voice 10 105
Reporting call progress: sessid= pin:11 callid= voice:out-2@voice meetingid= 10 progress= 105

****   NOTICE:
****	Phone icon changes in the Zon client meeting window to solid green

>>> j pin:11 voice:out-2@voice 10 part:11
Reporting join meeting sessid= pin:11 callid= voice:out-2@voice meetingid=10 partid=part:11
>>>
voice.connect_call(callid= voice:out-2@voice, mtgid= 10, submtgid= submeeting:2, hold= 0, grant_moderator= 1, mute= 0
>>> mtgcontroller reports status 0 while joining participant
>>> d pin:11 voice:out-1@voice 10
Reporting disconnect for sessid= pin:11 callid= voice:out-1@voice meetingid=10
>>>

****      IN THE ZON CLIENT
****
****       End the admin's instant meeting
****

voice.close_meeting(mtgid= 10) called
>>>
