/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef VDRIVER_H
#define VDRIVER_H

#include <time.h>
#include "../../util/iobject.h"
#include "../../util/irefobject.h"
#include "../../util/istring.h"
#include "timer.h"

//
// Exposed from service are connection (call) id & conference (session) id
// All functions will be non-blocking
//

// Don't want to constantly create and free objects..
// Unique call & conference objects ok, channel per board channel
// Application controlled by one object--events processed by app object


class IProvider;
class IApplication;
class IChannel;
class ICall;

IProvider * voice_get_provider(const char * driver_name, IApplication & app);

class DigitBuffer
{
    IString buffer;
    time_t last_time;

public:
    DigitBuffer();
    void clear();
	void add_digit(char digit);

	bool get_digits(char * buffer, int max);

	const char * peek_digits()
	{
	    return (const char *)buffer;
	}

	int length()
	{
		return buffer.length();
	}
};

enum IEventCodes
{
    EventIncoming = 1,
    EventConnected,
    EventDisconnected,
    EventProgress,
	EventProgressEnd,

    EventTerminated,
    EventDigits,

	EventInternal,

    EventMenuCommand,

    EventTimeout,
};

enum IConnectedReasons
{
    ConnectedInbound,
    ConnectedOutbound
};

enum IProgressReasons
{
	ProgressDialing = 100,
    ProgressRinging,
    ProgressBusy,
    ProgressReorder,
    ProgressSpecialInfo,
    ProgressAnswered,
	ProgressSpeech,
    ProgressRemoteHangup,
    ProgressError,
};

enum ITerminationReasons
{
    TerminatedError = 200,
    TerminatedStopped,
    TerminatedStoppedDTMF,
    TerminatedEndOfData,
    TerminatedMaxLength,
    TerminatedInitialSilence,
    TerminatedSilence,
    TerminatedDigit,
    TerminatedMaxDigits,
    TerminatedDisconnected
};

enum IErrorCodes
{
    OK = 0,
    PendingOp = 300,
    FailedOp,
    NoBoard,
    OpenFailed,
    NoResource,
    ChannelError,
    ChannelBusy,
	ChannelDisconnected,
    FileError,
    ConferenceError,
    ChannelNotConnected,
    BadParam
};

enum TerminationMasks
{
    TermNone  = 0,
	TermDTMF1 = 1,
	TermDTMF2 = 2,
	TermDTMF3 = 4,
	TermDTMF4 = 8,
	TermDTMF5 = 16,
	TermDTMF6 = 32,
	TermDTMF7 = 64,
	TermDTMF8 = 128,
	TermDTMF9 = 256,
	TermDTMF0 = 512,
    TermDTMFPound = 1024,
    TermDTMFStar = 2048,
    TermDTMFAny = 4095,
    TermVoice = 8192,
    TermSilence = 16384,         // Can't disable
    TermDisconnect = 32768,      // Can't disable
	TermStopped = 65536			 // Can't disable
};

enum ConnectBusType
{
    ConnectDSP, 
    ConnectPSTN,
    ConnectExtBus
};

enum ConferenceMode
{
    ConferenceNormal = 0,
    ConferenceModerator = 1,
    ConferenceListener = 2
};

enum TimerType
{
	TimerVoice = 1,
	TimerApp = 2,
	Timer3 = 3,
	Timer4 = 4,
    TimerMenu = 5,
    TimerMenuArmed = 6
};

#define MAX_ENTRY_ATTEMPTS	4
#define MENU_DIGIT_TIMEOUT	3000 // # millis to wait for a menu option
#define MENU_INTER_STAR_TIMEOUT 750

struct IEvent
{
    IChannel * channel;
    int event;
    int reason;
	
    IEvent(IChannel * _chan, int _ev, int _reason = 0)
    {
        channel = _chan; event = _ev, reason = _reason;
    }
};

class IConference : public IObject
{
protected:
    IProvider * provider;
    int conf_id;
    bool main;	// conference associated w/ the main meeting
    IString recording_id;
    bool announce_entry_exit;	// controls whether entry/exit tones/prompts are played

public:
    IConference(IProvider * provider, int id, bool main, const char *recording_id);
    virtual ~IConference();

    virtual int add(IChannel * channel, int mode = ConferenceNormal) = 0;
    virtual int remove(IChannel * channel) = 0;
    virtual int close() = 0;

	// Vol, gain is from 0 to 7
	virtual int set_volume(IChannel * channel, int vol, int gain) = 0;
	virtual int get_volume(IChannel * channel, int & out_vol, int & out_gain) = 0;

    virtual int set_mute(IChannel * channel, int mute) = 0;
    virtual int get_mute(IChannel * channel, int & out_mute) = 0;

    virtual int get_number_participants() = 0;
    virtual IChannel * get_participant(int index) = 0;
    virtual int find_participant(IChannel * channel) = 0;

    // put this channel on hold (i.e. temporarily remove them from the conference, but
    // don't give up their "seat").  Optionally play music
    virtual int hold(IChannel * channel, bool hold = false) = 0;
    virtual int release_hold(IChannel * channel) = 0;

    // Controls whether entry/exit tones/prompts are played when channels are joined
    void set_announce_entry_exit(bool announce) { announce_entry_exit = announce; }
    
    virtual int play_announcement(IChannel * channel, const char *file) = 0;

    virtual int start_recording() = 0;
    virtual int stop_recording() = 0;
    virtual int restart_recording() = 0;
    
    bool is_main() { return main; }

    virtual bool has_recorder() = 0;
    
    enum { TYPE_CONF = 6295 };
    virtual int type()
        { return TYPE_CONF; }
    virtual unsigned int hash()
        { return conf_id; }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && conf_id == ((IConference*)obj)->conf_id; }
};

enum RootMenuType
{
    MenuParticipant   = 0,
    MenuModerator     = 1,
};

enum
{
    // common menu actions
    MenuActionExit = -4,
    MenuActionArmed = -3,	// One star has been pressed;
                            // waiting for another "soon";
                            // if not, menu goes inactive

    MenuActionSubmenu = -2,	// a submenu was selected
    MenuActionNone = -1,	// an internal state change happened w/o a command

	// Conference participant commands
    MenuActionHelp = 0,	    // Play the menu options to the user
    MenuActionToggleMute,	// Mute the user's channel into the conference
    MenuActionMuteOn,		// Turn muting on for user's channel
    MenuActionMuteOff,		// Turn muting off for user's channel
    MenuActionVolumeUp,		// Raise the users input volume to the conference
    MenuActionVolumeDown,	// Lower the user's input volume to the conference
    MenuActionToggleVolume,	// Switch between normal and loud
    MenuActionToggleRaiseHand,

	// Moderator commands
    MenuActionRollCall,
    MenuActionToggleRecordMeeting,
    MenuActionToggleLock,
    MenuActionEndMeeting,
    MenuActionPlaceCall,
    MenuActionToggleLectureMode,
    MenuActionLockMeeting,
    MenuActionUnlockMeeting,
    MenuActionDialNumber,
    MenuActionToggleMuteJoinLeave,
    MenuActionInvalidSelection,
    MenuActionEndSubMeetingJoinMain,
    MenuActionEndSubMeetingEndCalls,
};

struct MenuNextAction
{
    int next_action;
    int alt_action;
    int next_menu;
};

struct MenuPage
{
    const char *entry_prompt;
    const char *help_prompt;
    const char *timeout_prompt;
    MenuNextAction m_action[12]; // options for digits 0..9 * #
};

class Menu
{
  protected:
    MenuPage * m_current_page;
    MenuPage * m_root_page;
    int        m_action;
    int        m_tmo;
    int        m_star;
    bool       m_active;
	bool       m_alt_selected;
    int        m_for_option;		     // if select option is this and alt_select is true, do the alt action

    static MenuPage s_participant_menu_pages[];
    static MenuPage s_moderator_menu_pages[];
    // ... do you think they'll be more menus? maybe

  public:
    Menu() {
        m_root_page = m_current_page = NULL;
        m_action = MenuActionNone;
        m_star = 0;
        m_tmo = 0;
        m_active = false;
		m_alt_selected = false;
		m_for_option = -1;
    }

    bool is_active() { return m_active; }

    void make_undefined() { m_root_page = NULL; reset(); }
    bool is_defined()  { return m_root_page != NULL; }

    void disarm() {
        m_star = 0;
    }

    void set_alt_selected(bool state, char key) 
        {
            if (m_current_page != NULL)
            {
                m_alt_selected = state;

                if (key >= '0' && key <= '9')
                    m_for_option = key - (int)'0';
                else if (key == '*')
                    m_for_option = 10;
                else if (key == '#')
                    m_for_option = 11;
                else
                    m_for_option = -1;
            }
        }
    

    void reset() {
        m_tmo = m_star = 0;
        m_action = MenuActionNone;
        m_current_page = m_root_page;
        //if (m_current_page){
            m_alt_selected = false;
            m_for_option = -1;
        //}
        m_active = false;
    }

    bool is_armed() { return m_star == 1; }

    void select_menu(int type) {
        switch (type)
        {
            case MenuModerator:
                m_root_page = &s_moderator_menu_pages[0];
                break;
                
            case MenuParticipant:
                m_root_page = &s_participant_menu_pages[0];
                break;
        }
    }
    
    int on_timeout() {
        m_tmo++;
            
        if (m_tmo <= 3)
        {
            return (m_action = MenuActionNone);
        }
        else
        {
            // Too many timeouts
            // Reset menu to initial inactive state
            m_active = false;
            m_star = 0;
            m_current_page = m_root_page;
            
            return (m_action = MenuActionExit);
        }
    }
        
    int get_action() { return m_action; }
    
    int on_digit(int digit);

    const char *get_entry_prompt();
    const char *get_help_prompt();
    const char *get_timeout_prompt();
};

class IChannel : public IObject
{
protected:
    int channel_id;
    ICall * current_call;
    IProvider * provider;
	IConference * conference;
    Menu confmenu;

    void * user;

    ChannelTimer timer;

    bool is_moderator;

    IString last_event;
    time_t last_event_time;

public:
    IChannel(IProvider * provider, int id);
    virtual ~IChannel();

    virtual int open() = 0;
    virtual int close() = 0;

    // Switching
    virtual int connect(int in_type, IChannel * out_chan, int out_type, bool full_duplex=true) = 0;
    virtual int reset() = 0;
    virtual int connect_music_on_hold() = 0;

    // Media functions
    virtual int play(const char * file, int term_mask) = 0;
    virtual int record(const char * file, bool beep, int initial_silence, int silence, int max_length, int term_mask) = 0;
    virtual int entry(const char * file, int max_digits, int initial_silence, int silence, int term_mask) = 0;
    virtual int menu() = 0;
    virtual int menu_completion(const char * prompt) = 0;
    virtual int stop() = 0;

    virtual int play_dtmf(const char * digits, bool await_voice = false) = 0;
    
    virtual bool get_digits(char * buffer, int max) = 0;
    virtual void clear_digits() = 0;

	virtual int set_volume(int vol_adj, int gain_adj) = 0;
	virtual int get_volume(int & out_vol, int & out_gain) = 0;

    virtual int set_mute(int mute) = 0;
	virtual int get_mute(int & out_mute) = 0;

    virtual int get_last_terminators() = 0;

    virtual void on_timeout_signal(int timer_type) = 0;
    virtual void on_timeout(int timer_type);
    virtual void set_timeout(int timer_type, int ms);
    virtual void cancel();

    IProvider * get_provider()  { return provider; }
    
    ICall * get_current_call();
    void set_current_call(ICall * call);

    void set_user(void * data);
    void *get_user();

	void set_conference(IConference * conference);
	IConference * get_conference();

    void grant_moderator(int grant);

    void select_menu() {
        if (is_moderator)
            confmenu.select_menu(MenuModerator);
        else
            confmenu.select_menu(MenuParticipant);
    }

    void make_menu_undefined() { confmenu.make_undefined(); }
    void reset_menu() { confmenu.reset(); }
    void disarm_menu() { confmenu.disarm(); }
    void is_menu_armed() { confmenu.is_armed(); }
    bool is_menu_active() { return confmenu.is_active(); }
    bool is_menu_defined() { return confmenu.is_defined(); }
    int on_menu_timeout() { return confmenu.on_timeout(); }
    int on_menu_digit(int digit) { return confmenu.on_digit(digit); }
    const char *get_menu_timeout_prompt() { return confmenu.get_timeout_prompt(); }
    const char *get_menu_entry_prompt() { return confmenu.get_entry_prompt(); }
    const char *get_menu_help_prompt() { return confmenu.get_help_prompt(); }
    int get_menu_action() { return confmenu.get_action(); }
    void set_menu_alt_selected(bool state, char key) { confmenu.set_alt_selected(state, key); }

    void set_last_event(const char * event_name) 
        { last_event = event_name; last_event_time = time(NULL); }
    const char * get_last_event() { return (const char *)last_event; }
    time_t get_last_event_time() { return last_event_time; }
    
    int get_id() { return channel_id; }

    enum { TYPE_CHANNEL = 3854 };
    virtual int type()
        { return TYPE_CHANNEL; }
    virtual unsigned int hash()
        { return channel_id; }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && channel_id == ((IChannel*)obj)->channel_id; }
};


class ICall : public IObject
{
protected:
    int call_id;
    IChannel * channel;
    bool disconnected;
    int m_options;
    IString calling_address;	// from caller-id (if avail)
    IString calling_name;		// from caller-id (if avail)
    IString called_address;
    IString extension;
    IString participant_id;

public:
    ICall(IChannel * chan, int id);
    virtual ~ICall();

    // PSTN functions
    virtual int call(const char * address, const char * extension) = 0;
    virtual int answer() = 0;
    virtual int disconnect() = 0;

    virtual IChannel * get_channel();

    virtual bool is_connected();

    void set_options(int _options) { m_options = _options; }
    int  get_options()             { return m_options; }

    void set_participantid(const char * _participant_id)
        { participant_id = _participant_id; }
    const char *get_participantid() 
        { return (const char *)participant_id; }
    bool is_participant(const char * _participant_id)
        { return (( _participant_id != NULL ) &&
              ( strlen(_participant_id) > 0 ) &&
              !strcmp(_participant_id,participant_id)); }

    virtual int get_call_status() = 0;

    const char *get_calling_address() { return calling_address; };
    const char *get_calling_name()    { return calling_name; };
    const char *get_called_address()  { return called_address; };
    const char *get_extension()       { return extension; };
    bool has_extension()              { return extension.length() > 0; };

    int get_id()
        { return call_id; }

    enum { TYPE_CALL = 7259 };
    virtual int type()
        { return TYPE_CALL; }
    virtual unsigned int hash()
        { return call_id; }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && call_id == ((ICall*)obj)->call_id; }
};


class IProvider
{
protected:
    IApplication * handler;
    static IString s_prompt_dir;
    static IString s_recording_dir;
    static IString s_announcement_recording_dir;
    
public:
    IProvider(IApplication & app);

    virtual int initialize() = 0;
    virtual int shutdown() = 0;
	virtual int set_parameter(IString & param, IString & value) = 0;

    virtual int create_conference(int max_participants,
                                  IConference *& conf,
                                  bool is_main,
                                  const char *recording_id) = 0;

    virtual int create_call(const char * address, const char * extension, ICall *& call, int options=0) = 0;

    virtual int get_port_status(IVector * ports) = 0;

    virtual int reset_port(int port_id, const char * part_id) = 0;

	virtual int run() = 0;

    static const char *get_prompt_dir() { return (const char *)s_prompt_dir; }
    static const char *get_recording_dir()
        { return (const char *)s_recording_dir; }

	IApplication * get_application()	{ return handler; }
};


class IApplication
{

public:
    virtual void on_initialized() = 0;

    virtual void on_new_call(ICall * call, const char * pin, int options) = 0;

    virtual void on_release_call(ICall * call) = 0;

    virtual void on_event(IEvent * event) = 0;

    virtual void on_error(IChannel * chan, int error, const char * message) = 0;
    
	virtual void on_timeout(IChannel * chan) = 0;
};

#define TYPE_PORT_STATUS	3455

class IPortStatus : public IObject
{
    unsigned m_port_id;
    IString m_part_id;
    bool m_connected;
    IString m_last_event;
    unsigned int  m_last_event_time;
    
  public:
    IPortStatus(int port_id,
                const char * part_id,
                bool connected,
                const char * last_event,
                unsigned int last_event_time) :
        m_port_id(port_id),
        m_part_id(part_id),
        m_connected(connected),
        m_last_event(last_event),
        m_last_event_time(last_event_time) { }

    int get_port_id() { return m_port_id; }
    const char * get_part_id() { return m_part_id; }
    bool is_connected() { return m_connected; }
    const char * get_last_event() { return m_last_event; }
    int get_last_event_time() { return m_last_event_time; }

    virtual int type() { return TYPE_PORT_STATUS; }
    virtual unsigned int hash() { return m_port_id; }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && m_port_id == ((IPortStatus *)obj)->m_port_id; }

};

#endif
