#include <voiceapi.h>
#include <voicetest.h>
#include <nmsvoice.h>

#include "../../jcomponent/util/nadutils.h"
#include "../../jcomponent/util/log.h"

#include "../../util/istring.h"

IProvider *provider;
VoiceTest  voice;

int read_config()
{
    nad_t nad;

    if (nad_load("voicetest.xml", &nad))
    {
        log_error(ZONE, "unable to load voice parameters, using defaults\n");

        return -1;
    }

    int root = 0;
	
    root = nad_find_elem(nad, 0, "app", 1);
    if (root > 0)
    {
        int def = nad_find_first_child(nad, root);
        while (def > 0)
        {
            IString param_name(NAD_ENAME(nad, def), NAD_ENAME_L(nad, def));
            IString param_val(NAD_CDATA(nad, def), NAD_CDATA_L(nad, def));
            
            // dispatch to voice app
            voice.set_parameter(param_name, param_val);

            def = nad_find_next_sibling(nad, def);
        }
    }

    root = nad_find_elem(nad, 0, "provider", 1);
    if (root > 0)
    {
        int def = nad_find_first_child(nad, root);
		
        // Create a voice provider based on the provider name
        if (def > 0)
        {
            IString provider_name_param(NAD_ENAME(nad, def), NAD_ENAME_L(nad, def));
            IString provider_name_val(NAD_CDATA(nad, def), NAD_CDATA_L(nad, def));

            if (provider_name_param == "Name") {
                provider = voice_get_provider(provider_name_val,  voice);
            }
            else {
                log_error(ZONE, "First provider parameter is required to be 'Name'");
            }
        }

        if (provider == NULL) {
            return -1;
        }

        def = nad_find_next_sibling(nad, def);

            // Set any voice provider parameters
        while (def > 0)
        {
            IString param_name(NAD_ENAME(nad, def), NAD_ENAME_L(nad, def));
            IString param_val(NAD_CDATA(nad, def), NAD_CDATA_L(nad, def));

                // dispatch to provider
            provider->set_parameter(param_name, param_val);

            def = nad_find_next_sibling(nad, def);
        }
    }
	
    nad_free(nad);
	
    return 0;
}

int main(int argc, char * argv[])
{
	printf("IIC Voice Test Harness 1.0   Copyright (C) 2004 imidio.com\n\n");

    
}
