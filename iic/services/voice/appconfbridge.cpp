#include <stdio.h>
#include "../common/common.h"
#include "../../jcomponent/rpc.h"
#include "../../jcomponent/util/rpcresult.h"

#include "appconfbridge.h"

enum ast_channel_state {
	AST_STATE_DOWN,			/*!< Channel is down and available */
	AST_STATE_RESERVED,		/*!< Channel is down, but reserved */
	AST_STATE_OFFHOOK,		/*!< Channel is off hook */
	AST_STATE_DIALING,		/*!< Digits (or equivalent) have been dialed */
	AST_STATE_RING,			/*!< Line is ringing */
	AST_STATE_RINGING,		/*!< Remote end is ringing */
	AST_STATE_UP,			/*!< Line is up */
	AST_STATE_BUSY,			/*!< Line is busy */
	AST_STATE_DIALING_OFFHOOK,	/*!< Digits (or equivalent) have been dialed while offhook */
	AST_STATE_PRERING,		/*!< Channel has detected an incoming call and is waiting for ring */

	AST_STATE_MUTE = (1 << 16),	/*!< Do not transmit voice data */
};

#define BridgeError(format, ...) fprintf(stderr,"(%s:%d)",__FILE__,__LINE__);fprintf(stderr, format, ## __VA_ARGS__);fprintf(stderr,"\n")
#define BridgeWarning(format, ...) fprintf(stderr, format, ## __VA_ARGS__);fprintf(stderr,"\n")

#define MAX_CALLID_SIZE 64

void AppConfBridge::onAsteriskNewchannel(void *userData, const char *channelID, const char *callID)
{
    AsteriskServer *server = (AsteriskServer *)userData;
    spAsteriskCall call;

    if (!strncmp("Local/RECORD", channelID, 12) || !strncmp("Local/PLAY", channelID, 10))
    {
        // Local channels created for purposes of playing/recording conference audio
        // Nothing to be done here.
        return;
    }

    if (NULL == (call = findCallFromAstID(channelID, server)))
    {

        // If this is a call bridged from the main incoming asterisk box to
        // another asterisk media server, do not create a new call object. We
        // will re-use the existing call object, updating it with data from the
        // new server when we get a ServerTransfer user-defined event from the
        // Asterisk Manager API.

#ifdef SERVER_BRIDGE
        if (strstr(channelID, m_xferChannelString.get_string()))
        {
            BridgeWarning("Newchannel event received for bridged channel %s on server %s",
                          channelID, server->name.get_string());
            return;
        }
#endif

        // If this is not an outbound call, create a new call object.
        if (!strstr(channelID, "sip-provider") &&
            !strstr(channelID, "iax-provider") &&
            !strstr(channelID, "zap-provider"))
        {
            call = new AsteriskCall;
            call->astID = channelID;
            call->server = server;
            call->iicID = generateNewCallID("in");
            call->participantMask = 0;
            call->bDeleteOnLeave = false;
            call->bInConf = false;
            server->SetChannelVariable(channelID, "iicID", call->iicID);
            m_callList.add(call);
            call->dereference();
        }
        else if (callID && *callID)
        {
            BridgeWarning("Newchannel event for outgoing call %s", callID);
            call = findCallFromIICID(callID);
            if (call != NULL)
            {
                call->astID = channelID;
                sendRPCRegisterUser(call);
            }
        }
    }
    else
    {
        BridgeWarning("Received Newchannel event for astchan %s server %s for pre-existing channel",
                      channelID, server->name.get_string());
    }
}

void AppConfBridge::onAsteriskNewstate(void *userData, const char *channelID, int chanState)
{
    AsteriskServer *server = (AsteriskServer *)userData;
    spAsteriskCall call = findCallFromAstID(channelID, server);
    if (call != NULL)
    {
        switch (chanState)
        {
        case AST_STATE_DOWN:
        case AST_STATE_RESERVED:
            break;

        case AST_STATE_OFFHOOK:
            break;

        case AST_STATE_DIALING:
        case AST_STATE_DIALING_OFFHOOK:
            sendRPCCallProgress(call, StatusDialing);
            break;

        case AST_STATE_RING:
            break;

        case AST_STATE_RINGING:
            sendRPCCallProgress(call, StatusRinging);
            break;

        case AST_STATE_UP:
            if (strstr(call->astID, "sip-provider"))
                sendRPCCallProgress(call, StatusAnswered);
            break;

        case AST_STATE_BUSY:
            sendRPCCallProgress(call, StatusBusy);
            break;

        case AST_STATE_PRERING:
            break;

        case AST_STATE_MUTE:
            break;
        }


    }
    else
    {
        BridgeWarning("onAsteriskNewstate: Cannot find call for chan %s",
                      channelID);
    }
}

void AppConfBridge::onAsteriskHangup(void *userData, const char *channelID)
{
    AsteriskServer *server = (AsteriskServer *)userData;
    spAsteriskCall call = findCallFromAstID(channelID, server);

    if (call != NULL)
    {
        sendRPCUnregisterUser(call);
        if (call->bInConf)
        {
            m_callList.remove(call, false);
            call->bDeleteOnLeave = true;
        }
        else
            m_callList.remove(call, true);
    }

}

void AppConfBridge::onAsteriskConferenceJoin(void *userData, const char *channelID, const char *conferenceID)
{

    if (!strcmp(conferenceID, "RecordSuspend"))
    {
        BridgeWarning("Recording suspended on asterisk chan %s.  Recording silence.", channelID);
        return;
    }

    BridgeMeeting *subMeeting = findMeetingFromID(conferenceID);

    if (subMeeting != NULL)
    {

        if (!strncmp("Local/RECORD", channelID, 12))
        {
            subMeeting->onRecordStarted(channelID);

            time_t recordStart = time(NULL) + 1;
            struct stat fs;
            if (!stat(subMeeting->recordFilename, &fs))
            {
                // If file inode modification time is reasonable close to current time, its probably
                // an accurate account of the file creating time, so use it.
                if (abs(fs.st_ctime - recordStart) < 10)
                {
                    recordStart = fs.st_ctime;
                }
            }

            sendRPCRecordingStarted(subMeeting->iicMeetingID, recordStart);
            return;
        }

        if (!strncmp("Local/PLAY", channelID, 10))
        {
            subMeeting->onPlayStarted();
            return;
        }

        AsteriskServer *server = (AsteriskServer *)userData;
        spAsteriskCall call = findCallFromAstID(channelID, server);

        if (call == NULL)
        {
            BridgeError("ConferenceJoin event rcvd for ast ID %s with NULL channel", channelID);
            return;
        }

        subMeeting->AddParticipant(call);
        call->subMeetingID = subMeeting->id;
        call->bInConf = true;

        m_numTotalParticipants++;
    }
}

void AppConfBridge::onAsteriskConferenceDTMF(void *userData, const char *channelID, char dtmf)
{

}

void AppConfBridge::onAsteriskConferenceState(void *userData, const char *channelID, int confState)
{
    AsteriskServer *server = (AsteriskServer *)userData;
    spAsteriskCall call = findCallFromAstID(channelID, server);

    if (call != NULL)
    {
        if (confState)
        {
            sendRPCCallProgress(call, StatusTalking);
        }
        else
        {
            sendRPCCallProgress(call, 0);
        }
    }
}

void AppConfBridge::onAsteriskConferenceLeave(void *userData, const char *channelID, const char *conferenceID)
{
    BridgeMeeting *subMeeting = findMeetingFromID(conferenceID);

    if (subMeeting)
    {
        spAsteriskCall call = subMeeting->FindParticipantFromAstID(channelID);
        if (call)
        {
            subMeeting->RemoveParticipant(call);
//             if (call->bDeleteOnLeave)
//                 delete call;
            m_numTotalParticipants--;
        }
        else
        {
            BridgeError("ConferenceLeave received for ast chan %s with NULL call", channelID);
        }
    }
    else
    {
        BridgeError("ConferenceLeave received for conf ID %s with NULL submeeting", conferenceID);
    }
}

void AppConfBridge::onAsteriskConferencePin(void *userData, const char *channelID, const char *conferencePIN)
{
    AsteriskServer *server = (AsteriskServer *) userData;
    spAsteriskCall call = findCallFromAstID(channelID, server);
    if (call != NULL)
    {
        if (0 != rpc_make_call(generateRPCID(call, "find").get_string(),
                               "controller", "controller.find_pin", "(ss)",
                               "sys:voice", conferencePIN))
        {
            BridgeError("RPC failure for controller.find_pin %s", conferencePIN);
            server->AppConfPinLookupFail(channelID);
        }

        call->pin = IString("pin:") + IString(conferencePIN);
    }
}

void AppConfBridge::onAsteriskOutgoingAccept(void *userData, const char *channelID, const char *iicChanID)
{
    spAsteriskCall call = findCallFromIICID(iicChanID);


    if (call != NULL)
    {
        call->astID = channelID;
        sendRPCJoinMeeting(call);
    }

}

void AppConfBridge::onAsteriskConferenceSoundComplete(void *userData, const char *channelID, const char *fileName)
{
    AsteriskServer *server = (AsteriskServer *)userData;
    spAsteriskCall call = findCallFromAstID(channelID, server);

    if (call != NULL)
    {
        call->PlayNext();
    }
}

void AppConfBridge::onAsteriskServerTransfer(void *userData, const char *channelID, const char *userPin)
{
    AsteriskServer *server = (AsteriskServer *)userData;
    spAsteriskCall call = findCallFromPin(userPin);
    BridgeMeeting *subMeeting;

    if (call != NULL)
    {
        call->astID = channelID;
        call->server = server;
        subMeeting = findMainSubmeeting(call->meetingID);

        if (call->server == subMeeting->rootServer)
        {
            addToConference(call, subMeeting);
        }
        else
        {
            BridgeError("Server Transfer call server %s doesn't match meeting server %s.",
                        call->server->name.get_string(), subMeeting->rootServer->name.get_string());
        }
    }

}

void AppConfBridge::sendRPCCallProgress(spAsteriskCall call, int callProgress)
{

    IString id = generateRPCID(call, "progress");
    if (0 != rpc_make_call(id.get_string(), "controller", "controller.call_progress", "(sssi)",
                           call->userID.get_string(), call->meetingID.get_string(), call->iicID.get_string(), callProgress))
    {
        BridgeError("Error sending call_progress RPC for %s/%s", call->userID.get_string(), call->meetingID.get_string());
    }

}

void AppConfBridge::sendRPCJoinMeeting(spAsteriskCall call)
{

    IString id = generateRPCID(call, "join");
    if (0 != rpc_make_call(id, "controller", "controller.join_meeting", "(sssss)",
                           call->userID.get_string(), call->iicID.get_string(), call->meetingID.get_string(), call->partID.get_string(), ""))
    {
        BridgeError("Error sending join_meeting RPC for %s/%s", call->userID.get_string(), call->meetingID.get_string());
    }


}

void AppConfBridge::sendRPCRegisterUser(spAsteriskCall call)
{

    IString id = generateRPCID(call, "report");

    if (0 != rpc_make_call(id, "controller", "controller.register_user", "(ssssss)",
                           call->pin.get_string(), call->userName.get_string(), "", call->iicID.get_string(),
                           "", ""))
    {
        BridgeError("Error sending register_user RPC message for %s/%s", call->userName.get_string(), call->iicID.get_string());
	}

}

void AppConfBridge::sendRPCUnregisterUser(spAsteriskCall call)
{

    IString id = generateRPCID(call, "disconnect");

    if (0 != rpc_make_call(id, "controller", "controller.unregister_user", "(sss)",
                           call->userID.get_string(), call->iicID.get_string(), call->meetingID.get_string()))
    {
        BridgeError("Error sending unregister_user RPC message for %s/%s", call->iicID.get_string(), call->meetingID.get_string());
    }

}

void AppConfBridge::sendRPCRecordingStarted(const char *conferenceid, time_t time)
{

    IString id = "setvoicedelay";

    if (0 != rpc_make_call(id, "mtgarchiver", "mtgarchiver.set_voice_recording_delay", "(si)",
                           conferenceid, time))
    {
        BridgeError("Error sending set_voice_recording_delay RPC message for %s", conferenceid);
    }

}

void AppConfBridge::RPCResultFindPin(const char *iicCallID, RPCResultSet * rs)
{
    spAsteriskCall call = findCallFromIICID(iicCallID);
    int found, options;
    char *meetingID, *participantID, *userID, *email, *userName, *name;

    if (call == NULL)
    {
        BridgeError("RPCResultFindPin: Unable to find call for ID %s", iicCallID);
        return;
    }

	// Read controller.find_pin result and handle
    //   "FOUND", "MEETINGID", "PARTICIPANTID", "USERID", "EMAIL",
    //   "USERNAME", "NAME", "OPTIONS", "SCHEDULED_TIME", "INVITE_VALID_BEFORE",
    //   "INVITE_EXPIRATION", "TYPE", "INVITED_TIME"
	//  FOUND: 1 = meeting found, 2 = meeting and user found, 0 = not found

    if (!rs->get_output_values("(issssssi*)", &found, &meetingID, &participantID, &userID, &email, &userName, &name, &options))
    {
        call->server->AppConfPinLookupFail(call->astID);
        return;
    }

    call->userName = userName;
    call->partID = participantID;
    call->meetingID = meetingID;
    call->meetingOptions = options;

    switch (found)
    {
    case 0:  //Invalid pin
        call->server->AppConfInvalidPin(call->astID);
        break;
    case 1:  // Anonymous meeting pin
        sendRPCRegisterUser(call);
        break;
    case 2:  // Personal pin, user id may be spec'd, if not use part id
        sendRPCRegisterUser(call);
        break;
    }

}

void AppConfBridge::RPCResultReport(const char *iicCallID, RPCResultSet *rs)
{

    spAsteriskCall call = findCallFromIICID(iicCallID);
    char *userID, *partID, *webURL;
    int status;

    if (call == NULL)
    {
        BridgeError("RPCResultReport: Unable to find call for ID %s", iicCallID);
        return;
    }

    if (!rs->get_output_values("(sssi)", &userID, &partID, &webURL, &status))
    {
        call->server->AppConfPinLookupFail(call->astID);
        return;
    }

    if (*userID)
        call->userID = userID;
    call->partID = partID;

    // If an incoming call, send controller.join_meeting.  If outbound, we'll
    // send when callee answers and acks
    if (!strstr(call->astID, "sip-provider"))
        sendRPCJoinMeeting(call);

}

void AppConfBridge::RPCResultJoinMeeting(const char *iicCallID, RPCResultSet *rs)
{
    spAsteriskCall call = findCallFromIICID(iicCallID);
    char *partID;
    int status;

    if (call == NULL)
    {
        BridgeError("RPCResultReport: Unable to find call for ID %s", iicCallID);
        return;
    }

    if (!rs->get_output_values("(si)", &partID, &status))
    {
        call->server->AppConfPinLookupFail(call->astID);
        return;
    }

    if (*partID)
        call->partID = partID;
}

AppConfBridge::AppConfBridge() :
    m_maxTotalParticipants(0),
    m_numTotalParticipants(0)
{
}

AppConfBridge::~AppConfBridge()
{
}

void AppConfBridge::AddServer(AsteriskServer * server)
{
    m_serverList.add(server);
    m_numServers++;
    m_maxTotalParticipants += server->GetMaxParticipants();

    if (m_serverList.size() == 1)
    {
        // The first server entry in the config file defines the
        // server where all incoming calls from the PBX/provider
        // are sent to.  When we transfer an incoming  call to a meeting on
        // a different server, it will generate a Newchannel event
        // with a channel of the form "SIP/<ip address of first server>".
        // We will be ignoring these Newchannel events, so we will need to
        // construct the channel string of the channels to filter out.

        m_xferChannelString = IString("SIP/") + server->m_ipAddress;
    }

    restoreServerState(server);
}

void AppConfBridge::RemoveServer(AsteriskServer *server)
{
    server->Disconnect();
    m_maxTotalParticipants -= server->GetMaxParticipants();
    m_serverList.remove(server);
    m_numServers--;
}

void AppConfBridge::Reset(void)
{
    IListIterator iter(m_meetingList);
    BridgeMeeting *subMeeting;

    while (NULL != (subMeeting = (BridgeMeeting *)m_meetingList.remove_head()))
    {
        subMeeting->rootServer->AppConfDeleteConference(subMeeting->id);
        delete subMeeting;
    }
}

int AppConfBridge::RegisterMeeting(const char * meetingID, const char * mainSubMeetingID, const char * callSubMeetingID, int maxParticipants, int startTime, char *iaxIPAddr)
{
    int astRtn;
    BridgeMeeting *subMeeting;

    // If we find the meeting, it means that this register request is coming after a restart of the voice service
    // so we need to update with controller related info
    subMeeting = findMeetingFromID(mainSubMeetingID);
    if (subMeeting != NULL)
    {
        subMeeting->isMain = true;
        subMeeting->iicMeetingID = meetingID;
        subMeeting->meetingStartTime = startTime;
        BridgeWarning("RegisterMeeting attempt to register pre-existing meeting %s", meetingID);
        return AST_SUCCESS;
    }

    // Otherwise, create a new meeting

    AsteriskServer *server = allocateRootServer(maxParticipants);

    if (server != NULL)
    {
        if (AST_SUCCESS == (astRtn = server->AppConfCreateConference(mainSubMeetingID)))
        {
            subMeeting = new BridgeMeeting(maxParticipants, server, mainSubMeetingID, true, meetingID, startTime);
            m_meetingList.add(subMeeting);
        }
        else
        {
            BridgeError("Unable to create conference %s on server %s", mainSubMeetingID, server->name.get_string());
            return AST_FAIL;
        }
    }
    else
    {
        BridgeError("Unable to allocate conference %s on an available server", mainSubMeetingID);
        return AST_FAIL;
    }

    // Return a string containing the IP address of the allocated asterisk server so that pcphone clients
    // will know where to connect

    strncpy(iaxIPAddr, server->GetIPAddress().get_string(), 255);

    return AST_SUCCESS;
}

int AppConfBridge::RegisterSubmeeting(const char * meetingID, const char * subMeetingID, int maxParticipants)
{
    int astRtn;
    BridgeMeeting *subMeeting;
    AsteriskServer *server = allocateRootServer(maxParticipants);

    if (server != NULL)
    {
        if (AST_SUCCESS == (astRtn = server->AppConfCreateConference(subMeetingID)))
        {
            subMeeting = new BridgeMeeting(maxParticipants, server, subMeetingID, false, meetingID);
            m_meetingList.add(subMeeting);
        }
        else
        {
            BridgeError("Unable to create conference %s on server %s", subMeetingID, server->name.get_string());
            return AST_FAIL;
        }
    }
    else
    {
        BridgeError("Unable to allocate conference %s on an available server", subMeetingID);
        return AST_FAIL;
    }

    return AST_SUCCESS;
}

int AppConfBridge::CloseMeeting(const char * meetingID)
{
    // Find all submeetings operating under this meetingID and kill them

    IListIterator iter(m_meetingList);

    BridgeMeeting *subMeeting = (BridgeMeeting *)iter.get_first();

    while (subMeeting != NULL)
    {
        if (subMeeting->iicMeetingID == meetingID)
        {
            BridgeMeeting *toDelete = subMeeting;
            AsteriskServer *server = subMeeting->rootServer;
            IString smID = subMeeting->id;
            subMeeting->KillRecording();
            subMeeting = (BridgeMeeting *)iter.get_next();
            m_meetingList.remove(toDelete, true);
            server->AppConfDeleteConference(smID.get_string());
        }
        else
        {
            subMeeting = (BridgeMeeting *)iter.get_next();
        }
    }

    return AST_SUCCESS;
}

int AppConfBridge::CloseSubmeeting(const char * meetingID, const char * subMeetingID)
{
    BridgeMeeting *subMeeting = findMeetingFromID(subMeetingID);
    if (subMeeting != NULL)
    {
        AsteriskServer *server = subMeeting->rootServer;
        m_meetingList.remove(subMeeting, true);  // remove first to ensure subsequent ConferenceLeave events don't generate race condition
        int astReturn = server->AppConfDeleteConference(subMeetingID);
        if (astReturn != AST_SUCCESS)
        {
            BridgeWarning("DeleteConference failure %d trying to delete conference %s on server %s",
                          astReturn, subMeetingID, server->name.get_string());
        }
    }
    else
    {
        BridgeWarning("Unable to find meeting %s slated for deletion", subMeetingID);
    }

    return AST_SUCCESS;
}

int AppConfBridge::ConnectCall(const char * iicCallID, const char * meetingID, const char * subMeetingID, bool bHold, bool bModerator, bool bMute)
{
    spAsteriskCall astCall = findCallFromIICID(iicCallID);
    BridgeMeeting *subMeeting = findMeetingFromID(subMeetingID);

    if (astCall == NULL)
    {
        BridgeError("Unable to find call with iic call id %s", iicCallID);
        return AST_FAIL;
    }

    if (subMeeting == NULL)
    {
        BridgeError("Unable to find submeeting with id %s", subMeetingID);
        return AST_FAIL;
    }

    if (bMute)
    {
        astCall->participantMask |= PARTICIPANT_MASK_MUTE;
        astCall->bMuted = true;
    }
    else
    {
        astCall->bMuted = false;
    }

    if (astCall->meetingOptions & PARTICIPANT_OPTION_SCREEN)
    {
        astCall->participantMask |= PARTICIPANT_MASK_RECORD;
        astCall->rosterFilename = generateNameRecordFilename(astCall->astID);
    }

    astCall->bHold = bHold;

    if (astCall->server == subMeeting->rootServer)
    {
        addToConference(astCall, subMeeting);
        return AST_SUCCESS;
    }
    else
    {
        char pin[20];
        strncpy(pin, astCall->pin.get_string(), 20);

        astCall->server->AppConfServerTransfer(astCall->astID, pin+4, subMeeting->rootServer->GetIPAddress().get_string());
    }

    return AST_SUCCESS;

}

int AppConfBridge::DisconnectCall(const char * iicCallID, const char * meetingID, const char * subMeetingID)
{
    spAsteriskCall astCall = findCallFromIICID(iicCallID);
    BridgeMeeting *subMeeting;


    if (astCall == NULL)
    {
        BridgeError("Unable to find call for iic ID %s", iicCallID);
        return AST_FAIL;
    }

    if (subMeetingID == NULL || *subMeetingID == 0)
    {
        subMeeting = findMainSubmeeting(meetingID);
    }
    else
    {
        subMeeting = findMeetingFromID(subMeetingID);
    }


    if (subMeeting == NULL)
    {
        BridgeError("Unable to find submeeting for submeeting ID %s", subMeetingID);
        return AST_FAIL;
    }

    if (subMeeting->rootServer == astCall->server)
    {
        int astRtn = subMeeting->rootServer->Hangup(astCall->astID);
        return astRtn;
    }
    else
    {
        BridgeError("Call %s not on same server as meeting %s", iicCallID, subMeetingID);
        return AST_FAIL;
    }
}

int AppConfBridge::MoveCall(const char * iicCallID, const char * meetingID, const char * oldSubmeetingID, const char * newSubmeetingID)
{
    int astRtn;
    spAsteriskCall astCall = findCallFromIICID(iicCallID);
    BridgeMeeting *newSubmeeting = findMeetingFromID(newSubmeetingID);

    //    astRtn = oldSubmeeting->rootServer->AppConfRemoveFromConference(astCall->astID, oldSubmeetingID);

    if (astCall == NULL)
    {
        BridgeError("Unable to find call for iic ID %s", iicCallID);
        return AST_FAIL;
    }

    if (newSubmeeting == NULL)
    {
        BridgeError("Unable to find new submeeting %s to move call to", newSubmeetingID);
        return AST_FAIL;
    }

    astCall->participantMask = PARTICIPANT_MASK_MOVE;
    astRtn = newSubmeeting->rootServer->AppConfAddToConference(astCall->astID, newSubmeetingID, astCall->participantMask);
    astCall->subMeetingID = newSubmeetingID;

    return astRtn;

}

int AppConfBridge::MakeCall(const char * meetingID, const char * subMeetingID, const char * phoneNumber, const char * iicCallID, const char * partID, int callOptions)
{
    int astRtn = 0;
    IString newCallID;

    BridgeMeeting *subMeeting = findMeetingFromID(subMeetingID);

    if (*iicCallID == 0)
        newCallID = generateNewCallID("out");
    else
        newCallID = iicCallID;

    if (AST_SUCCESS == (astRtn = subMeeting->rootServer->AppConfMakeCall(phoneNumber, newCallID.get_string())))
    {
        spAsteriskCall call = new AsteriskCall;
        call->iicID = newCallID;
        call->server = subMeeting->rootServer;
        call->partID = partID;
        call->pin = IString("pin:") + IString(partID+5);
        call->meetingID = meetingID;
        call->subMeetingID = subMeetingID;
        call->meetingOptions = callOptions;
        call->participantMask = 0;
        call->bDeleteOnLeave = false;
        call->bInConf = false;
        m_callList.add(call);
        call->dereference();
    }

    return astRtn;
}


int AppConfBridge::NotifyParticipant(const char *meetingID, const char * participantID, int isModerator, int outDial, int callOptions)
{

    return AST_SUCCESS;
}

int AppConfBridge::SetVolume(const char * meetingID, const char *subMeetingID, const char * iicCallID, int volume, int gain, int mute)
{
    spAsteriskCall call = findCallFromIICID(iicCallID);

    if (call == NULL)
    {
        BridgeError("Cannot find call object for iic call id %s", iicCallID);
        return AST_FAIL;
    }

    if (call->subMeetingID == subMeetingID)
    {
        if (mute)
        {
            call->server->AppConfMuteParticipant(call->astID, call->subMeetingID);
            call->bMuted = true;
        }
        else if (!mute && call->bMuted)
        {
            call->server->AppConfUnmuteParticipant(call->astID, call->subMeetingID);
            call->bMuted = false;
        }
    }

    return AST_SUCCESS;
}

int AppConfBridge::GetVolume(const char * meetingID, const char * subMeetingID, const char *iicCallID, const char * participantID)
{

    return AST_SUCCESS;
}

int AppConfBridge::Error(const char * iicCallID, const char * errorID, int error, const char *msg)
{
    return AST_SUCCESS;
}

int AppConfBridge::Ping()
{
    return AST_SUCCESS;
}

int AppConfBridge::GrantModerator(const char * meetingID, const char * subMeetingID, const char * iicCallID, int grant)
{
    return AST_SUCCESS;
}

int AppConfBridge::ModifySubmeeting(const char * meetingID, const char * subMeetingID, int options)
{
    BridgeMeeting *subMeeting = findMeetingFromID(subMeetingID);

    if (subMeeting == NULL)
    {
        BridgeError("Cannot find submeeting %s", subMeetingID);
        return AST_FAIL;
    }

    subMeeting->options = options;

    if (!subMeeting->isHolding)
    {
        if ((options & MEETING_OPTION_RECORD))
        {
            subMeeting->StartRecording();
        }
        else
        {
            subMeeting->StopRecording();
        }
    }

    return AST_SUCCESS;
}

int AppConfBridge::RollCall(const char * meetingID, const char * iicCallID)
{
    BridgeMeeting *subMeeting = findMainSubmeeting(meetingID);
    spAsteriskCall call = findCallFromIICID(iicCallID);

    if (subMeeting != NULL)
    {
        subMeeting->PlayRollCall(call);
    }

    return AST_SUCCESS;
}

int AppConfBridge::GetPorts()
{
    return AST_SUCCESS;
}

int AppConfBridge::ResetPort()
{
    return AST_SUCCESS;
}

int AppConfBridge::ResetRecording(const char * meetingID, const char *subMeetingID)
{
    return AST_SUCCESS;
}


// Allocate a new conference with the specified number of participants to
// an available server
AsteriskServer * AppConfBridge::allocateRootServer(int maxParticipants)
{

    float expectedUtilization, actualUtilization;
    int roomLeft;

    if (m_serverList.size() == 1 || m_numTotalParticipants == 0)
        return (AsteriskServer *)m_serverList.get(0);

    // Find the first server that is under its "expected utilization".
    IListIterator iter(m_serverList);

    AsteriskServer *curServer = (AsteriskServer *) iter.get_first();
    while (curServer != NULL)
    {
        expectedUtilization = (float) curServer->GetMaxParticipants() / (float) m_maxTotalParticipants;
        actualUtilization = (float) curServer->GetNumParticipants() / (float) m_numTotalParticipants;
        roomLeft = curServer->GetMaxParticipants() - curServer->GetNumParticipants();
        if ((actualUtilization <= expectedUtilization) &&
            (maxParticipants <= roomLeft))
        {
            break;
        }
        else
        {
            curServer = (AsteriskServer*) iter.get_next();
        }
    }

    return curServer;
}

IString AppConfBridge::generateNewCallID(const char * direction)
{
    char callID[MAX_CALLID_SIZE];
    int rtn = snprintf(callID, MAX_CALLID_SIZE, "voice:%s-%d@voice", direction, m_callIDIndex++);
    if (rtn == MAX_CALLID_SIZE || rtn == -1)
    {
        BridgeError("Unable to create IIC call ID for voice:%s-%d@voice", direction, m_callIDIndex-1);
        return IString("");
    }

    return IString(callID);
}

IString AppConfBridge::generateRPCID(spAsteriskCall call, const char * base)
{
    char rpcID[MAX_CALLID_SIZE];

    if (call != NULL)
    {
        int rtn = snprintf(rpcID, MAX_CALLID_SIZE, "%s:%s", base, call->iicID.get_string());
        if (rtn == MAX_CALLID_SIZE || rtn == -1)
        {
            BridgeError("Unable to create RPC call ID for %s:%s", base, call->iicID.get_string());
            return IString("");
        }

        return IString(rpcID);
    }
    else
        BridgeError("Trying to create RPC call ID for NULL call object");
        return IString("");


}

BridgeMeeting * AppConfBridge::findMeetingFromID(const char * subMeetingID)
{
    BridgeMeeting *rtnMeeting = NULL;
    IListIterator iter(m_meetingList);

    BridgeMeeting *curMeeting = (BridgeMeeting *)iter.get_first();

    while (rtnMeeting == NULL && curMeeting != NULL)
    {
        if (curMeeting->id == subMeetingID)
            rtnMeeting = curMeeting;
        else
            curMeeting = (BridgeMeeting *)iter.get_next();
    }

    return rtnMeeting;
}

spAsteriskCall  AppConfBridge::findCallFromIICID(const char * iicCallID)
{
    AsteriskCall *rtnCall = NULL;
    IListIterator iter(m_callList);

    AsteriskCall *curCall = (AsteriskCall *)iter.get_first();
    while (rtnCall == NULL && curCall != NULL)
    {
        if (curCall->iicID == iicCallID)
            rtnCall = curCall;
        else
            curCall = (AsteriskCall *)iter.get_next();
    }

    return spAsteriskCall(rtnCall);


}

spAsteriskCall  AppConfBridge::findCallFromAstID(const char * astChanID, AsteriskServer *server)
{

   AsteriskCall * rtnCall = NULL;
   IListIterator iter(m_callList);

   AsteriskCall * curCall = (AsteriskCall * )iter.get_first();
   while (rtnCall == NULL && curCall != NULL)
   {
       if (curCall->astID == astChanID && curCall->server == server)
           rtnCall = curCall;
       else
           curCall = (AsteriskCall * )iter.get_next();
   }

   return spAsteriskCall(rtnCall);

}

spAsteriskCall  AppConfBridge::findCallFromPin(const char * userPin)
{

   AsteriskCall * rtnCall = NULL;
   IListIterator iter(m_callList);
   IString comparePin = IString("pin:") + IString(userPin);

   AsteriskCall * curCall = (AsteriskCall * )iter.get_first();
   while (rtnCall == NULL && curCall != NULL)
   {
       if (curCall->pin == comparePin)
           rtnCall = curCall;
       else
           curCall = (AsteriskCall * )iter.get_next();
   }

   return spAsteriskCall(rtnCall);

}

BridgeMeeting *  AppConfBridge::findMainSubmeeting(const char * iicMeetingID)
{
    BridgeMeeting *rtnMeeting = NULL;
    IListIterator iter(m_meetingList);

    BridgeMeeting *curMeeting = (BridgeMeeting *)iter.get_first();

    while (rtnMeeting == NULL && curMeeting != NULL)
    {
        if (curMeeting->isMain && curMeeting->iicMeetingID == iicMeetingID)
            rtnMeeting = curMeeting;
        else
            curMeeting = (BridgeMeeting *)iter.get_next();
    }

    return rtnMeeting;

}


void AppConfBridge::addToConference(spAsteriskCall call, BridgeMeeting *subMeeting)
{
    AsteriskCall *curCall;

    if (!call->bHold)
    {
        subMeeting->rootServer->AppConfAddToConference(call->astID, subMeeting->id, call->participantMask);

        // Put any participants who previously connected on hold into conference
        IListIterator iter(subMeeting->holdList);
        curCall = (AsteriskCall *)iter.get_first();

        while (curCall != NULL)
        {
            spAsteriskCall safeCall(curCall);
            if (safeCall->server == subMeeting->rootServer)
            {
                subMeeting->rootServer->AppConfAddToConference(safeCall->astID, subMeeting->id, safeCall->participantMask);
                safeCall->bHold = false;
            }
            curCall = (AsteriskCall * )iter.get_next();
        }

        if (subMeeting->isHolding)
        {
            subMeeting->isHolding = false;
            if (subMeeting->options & RecordingVoice)
            {
                subMeeting->StartRecording();
            }
        }
    }
    else
    {
        subMeeting->holdList.add(call);
        call->server->AppConfPreConfHold(call->astID);
    }
}

void AppConfBridge::restoreServerState(AsteriskServer *server)
{
    BridgeMeeting *subMeeting;
    IString confListString = server->AppConfGetConferenceList();
    const char *line = confListString.get_string();
    char subMeetingID[128];
    int numParticipants;

    while (*line != '\0')
    {
        sscanf(line, "%s %d", subMeetingID, &numParticipants);
        subMeeting = new BridgeMeeting(0, server, subMeetingID, true, "", 0);
        m_meetingList.add(subMeeting);
        BridgeWarning("Restoring submeeting id %s from ast server %s", subMeetingID, server->name.get_string());
        restoreMeetingParticipants(subMeeting);

        line = strchr(line, '\n');
        if (line == NULL)
            break;
        else
            line++;
    }

}

void AppConfBridge::restoreMeetingParticipants(BridgeMeeting *subMeeting)
{
    AsteriskServer *server = subMeeting->rootServer;
    IString partListString = server->AppConfGetParticipantList(subMeeting->id.get_string());
    const char *p_line = partListString.get_string();
    char channelID[128];
    spAsteriskCall call;

    while (*p_line != '\0')
    {
        sscanf(p_line, "%s", channelID);
        call = new AsteriskCall;
        call->astID = channelID;
        call->server = server;
        call->iicID = server->GetChannelVariable(channelID, "iicID");
        subMeeting->RestoreParticipant(call);
        m_callList.add(call);
        BridgeWarning("Restoring iic user %s on ast chan %s in submeeting %s",
                      call->iicID.get_string(), call->astID.get_string(), subMeeting->id.get_string());
        p_line = strchr(p_line, '\n');
        if (p_line == NULL)
            break;
        else
            p_line++;
    }
}


BridgeMeeting::BridgeMeeting(int maxParticipants, AsteriskServer *server, const char *subMeetingID, bool bIsMain, const char *meetingID, int startTime /* = 0 */) :
    m_maxParticipants(maxParticipants), rootServer(server), id(subMeetingID), isMain(bIsMain), iicMeetingID(meetingID), meetingStartTime(startTime),
    m_numParticipants(0), isHolding(bIsMain), bRecording(false)
{

    if (isMain)
    {
        char time_string[20];
        struct tm *tm_start = localtime((time_t *) &meetingStartTime);
        strftime(time_string, sizeof(time_string), "%Y-%m-%d-%H-%M-%S", tm_start);

        recordFilename = "/var/iic/recording/conf-";
        recordFilename.append(iicMeetingID);
        recordFilename.append("-");
        recordFilename.append(time_string);
        recordFilename.append(".wav");

    }
}

int BridgeMeeting::StartRecording()
{
    if (!bRecording)
    {
        bRecording = true;
        rootServer->AppConfStartRecording(id.get_string(), recordFilename.get_string());
    }
    else
    {
        rootServer->AppConfResumeRecording(recordChannel.get_string());
    }

    return AST_SUCCESS;

}

int BridgeMeeting::StopRecording()
{
    if (bRecording)
    {
        rootServer->AppConfSuspendRecording(recordChannel.get_string());
    }

    return AST_SUCCESS;
}

int BridgeMeeting::ResetRecording()
{
    if (bRecording)
    {
        rootServer->Hangup(recordChannel.get_string());
    }

    rootServer->AppConfStartRecording(id.get_string(), recordFilename.get_string());

    return AST_SUCCESS;

}

int BridgeMeeting::KillRecording()
{
    if (bRecording)
    {
        rootServer->Hangup(recordChannel.get_string());
        bRecording = false;
    }
    return AST_SUCCESS;
}

void BridgeMeeting::AddParticipant(spAsteriskCall call)
{

    m_participantList.add(call);
    m_numParticipants++;

    if (m_numParticipants == 1)
    {
        call->Play("conf-onlyperson");
    }
    else
    {
        if ((options & MEETING_OPTION_TONES) || (call->meetingOptions & PARTICIPANT_OPTION_TONES))
        {
            call->server->AppConfPlayToConference(call->astID, "beep");
        }
    }

}

void BridgeMeeting::RestoreParticipant(spAsteriskCall call)
{
    m_participantList.add(call);
    m_numParticipants++;
}

void BridgeMeeting::RemoveParticipant(spAsteriskCall call)
{
    m_participantList.remove(call, false);
    m_numParticipants--;
}

void BridgeMeeting::PlayRollCall(spAsteriskCall listenerCall)
{
    IListIterator iter(m_participantList);

    AsteriskCall *curCall = (AsteriskCall *)iter.get_first();

    while (curCall != NULL)
    {
        spAsteriskCall safeCall(curCall);
        listenerCall->Play(curCall->rosterFilename.get_string());
        curCall = (AsteriskCall * )iter.get_next();
    }
}

spAsteriskCall  BridgeMeeting::FindParticipantFromAstID(const char *astChanID)
{

   AsteriskCall *rtnCall = NULL;
   IListIterator iter(m_participantList);

   AsteriskCall *curCall = (AsteriskCall * )iter.get_first();
   while (rtnCall == NULL && curCall != NULL)
   {
       if (curCall->astID == astChanID)
           rtnCall = curCall;
       else
           curCall = (AsteriskCall *)iter.get_next();
   }

   return spAsteriskCall(rtnCall);

}

void AsteriskCall::Play(const char *fileName, bool bFlush)
{

    if (bFlush)
    {
        playList.remove_all();
    }

    if (bFlush || !bPlaying)
    {
        server->AppConfPlayToParticipant(astID, fileName);
    }
    else
    {
        IString *playListEntry = new IString(fileName);
        playList.add(playListEntry);
    }

    bPlaying = true;
}


void AsteriskCall::PlayNext()
{
    IString *nextPlay = (IString *)playList.remove_head();

    if (nextPlay != NULL)
    {
        server->AppConfPlayToParticipant(astID, nextPlay->get_string());
        bPlaying = true;
        delete nextPlay;
    }
    else
    {
        bPlaying = false;
    }
}
