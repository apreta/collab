/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef _WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#ifdef LINUX
#include <sys/time.h>
#endif
#include "timer.h"
#include "voiceapi.h"
#include "../../jcomponent/util/log.h"

#ifdef LINUX
IList ChannelTimer::running;
#endif

//
// Timer for channel.
//

ChannelTimer::ChannelTimer(IChannel * _chan, int _id)
{
    channel = _chan;
    channel_id = _id;
    remaining = -1;
    is_active = false;

#ifdef _WIN32
	// Callbacks will come on same thread only when thread is in alertable
	// state...which is just what we want here.
	timer = CreateWaitableTimer(NULL, FALSE, NULL);
#endif
}

ChannelTimer::~ChannelTimer()
{
#ifdef _WIN32
	if (timer != NULL)
		CloseHandle(timer);
#endif
}

void ChannelTimer::set_timeout(int _type, int ms)
{
#ifdef LINUX
    block();
	log_debug(ZONE, "Setting timer to %d", ms);
    remaining = ms;
	timer_type = _type;
    if (!is_active)
    {
        running.add(this);
        is_active = true;
    }
    if (!is_running())
        set_timer(TIMER_INTERVAL);
    unblock();

#else // _WIN32

	LARGE_INTEGER pause; 
	pause.QuadPart = - (ms * 10000i64);	// 100 ns units
	log_debug(ZONE, "Setting timer to %i64d", pause);

	timer_type = _type;

	SetWaitableTimer(timer, &pause, 0, timer_proc, this, FALSE);
#endif
}

void ChannelTimer::cancel()
{
#ifdef LINUX
    block();
	cancel_in_handler();
    unblock();
#else
	CancelWaitableTimer(timer);
#endif
}

void ChannelTimer::cancel_in_handler()
{
#ifdef LINUX
    remaining = -1;
    running.remove(this, false);
    is_active = false;
    if (running.size() == 0)
        set_timer(0);
#endif
}

int lcount = 0;

#ifdef LINUX

// Basic strategy is to run through all timers & decrement remaining time.
// If timer is thus expired, timeout handler is called.
void ChannelTimer::handler(int signal)
{
    IListIterator iter(running);
    ChannelTimer * timer = (ChannelTimer*)iter.get_first();

    while (timer != NULL)
    {
        ChannelTimer * cur = timer;
        timer = (ChannelTimer*) iter.get_next();

        if (cur->remaining != -1)
        {
            cur->remaining -= TIMER_INTERVAL;
            if (cur->remaining <= 0)
            {
                cur->cancel_in_handler();
                cur->channel->on_timeout_signal(cur->timer_type);
            }
        }
    }
}

bool ChannelTimer::is_running()
{
    struct itimerval interval;
    getitimer(ITIMER_REAL, &interval);

    return ((interval.it_interval.tv_usec != 0) &&
            (interval.it_value.tv_usec != 0));
}

void ChannelTimer::block()
{
    sigset_t sigset;

    // Block alarm signal
    sigemptyset(&sigset);
    sigaddset(&sigset, SIGALRM);
    sigprocmask(SIG_BLOCK, &sigset, NULL);

	if (++lcount != 1)
		log_debug(ZONE, "** Re-entered timer block");
}

void ChannelTimer::unblock()
{
	--lcount;

    sigset_t sigset;

    // Unblock alarm signal
    sigemptyset(&sigset);
    sigaddset(&sigset, SIGALRM);
    sigprocmask(SIG_UNBLOCK, &sigset, NULL);
}

void ChannelTimer::set_timer(int interval_ms)
{
    struct sigaction act;

	if (interval_ms)
	{
		log_debug(ZONE, "Enabling timer SIGALRM's");
	}
	
    // Set handler
    act.sa_handler = handler;
    sigemptyset(&act.sa_mask);
    sigaddset(&act.sa_mask, SIGALRM);
    act.sa_flags = 0;
    sigaction(SIGALRM, &act, NULL);

    // Set timer
    struct itimerval interval;
    interval.it_value.tv_sec = 0;
    interval.it_value.tv_usec = interval_ms * 1000; // 100 ms
    interval.it_interval.tv_sec = 0;
    interval.it_interval.tv_usec = interval_ms * 1000; // 100 ms

    if (setitimer(ITIMER_REAL, &interval, NULL) < 0)
    {
        log_error(ZONE, "Couldn't set interval timer (%s)!",
                  strerror(errno));
    }
}

#else //_WIN32

// Simpler in win32 as we've scheduled a timer to expire just when we need it.
// We shouldn't have to worry about block/unblocking signals as we'll only
// get callback when in alertable state, like SleepEx.

void _stdcall ChannelTimer::timer_proc(void *arg, DWORD high_time, DWORD low_time)
{
	ChannelTimer * cur = (ChannelTimer*)arg;
    cur->channel->on_timeout(cur->timer_type);
}

#endif

