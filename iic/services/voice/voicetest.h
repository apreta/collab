#include <voiceapi.h>

class VoiceTest : public IApplication
{
  public:
    void set_parameter(const char *param_name, const char *param_val);
    void on_initialized();
    void on_new_call(ICall * call);
    void on_release_call(ICall * call);
    void on_event(IEvent * event);
    void on_error(IChannel * chan, int error, const char * message);
    void on_timeout(IChannel * chan);
};
        
