#!/bin/bash

[ $# -eq 1 ] && destdir=$1

sed -e 's+\\$++' -e 's+:++' extvoiced.deps | \
    tr ' ' '\n' | \
    grep -v /usr/include | \
    grep -v /usr/lib | \
    grep -v extvoiced | \
    gawk '{ if (NF>0) print $0,$0 }' | \
    sed -e "s+ \.\./\.\./+ $destdir/opt/iic/include/+" -e "s+ \.\./+ $destdir/opt/iic/include/+" -e 's+^+cp +'
