#ifndef _APPCONFBRIDGE_H
#define _APPCONFBRIDGE_H

#include "astserver.h"
#include <istring.h>
#include <irefobject.h>

typedef enum {
    CP_CLEAR         = 0,
    CP_DIALING       = 100,
    CP_RINGING       = 101,
    CP_BUSY          = 102,
    CP_REORDER       = 103,
    CP_SPECIAL_INFO  = 104,
    CP_ANSWERED      = 105,
    CP_SPEECH        = 106,
    CP_REMOTE_HANGUP = 107,
    CP_ERROR         = 108
} CP_TYPE;

#define PARTICIPANT_OPTION_SCREEN     0x00002
#define PARTICIPANT_OPTION_TONES      0x20000

#define MEETING_OPTION_TONES   0x1
#define MEETING_OPTION_RECORD  0x2
#define MEETING_OPTION_LECTURE 0x4
#define MEETING_OPTION_LOCK    0x8

class AsteriskCall;
typedef IRefObjectPtr<AsteriskCall> spAsteriskCall;

class BridgeMeeting : public IReferencableObject
{
 public:

    BridgeMeeting(int maxParticipants, AsteriskServer *server, const char *subMeetingID, bool bIsMain, const char *meetingID, int startTime=0);

    int StartRecording(void);
    int StopRecording(void);
    int ResetRecording(void);
    int KillRecording(void);
    void PlayRollCall(spAsteriskCall listenerCall);

    void AddParticipant(spAsteriskCall call);
    void RemoveParticipant(spAsteriskCall call);
    void onRecordStarted(const char *channelID) { bRecording = true; recordChannel = channelID; };
    void onPlayStarted(void) { bPlaying = true; };
    void RestoreParticipant(spAsteriskCall call);
    spAsteriskCall FindParticipantFromAstID(const char *astChanID);
    int m_maxParticipants;

    AsteriskServer *rootServer;
    IString id;
    bool isMain;
    IString iicMeetingID;
    int meetingStartTime;

    int m_numParticipants;
    IList m_participantList;

    IList overflowServerList;
    IList holdList;
    bool isHolding;
    int options;

    bool bRecording;
    IString recordChannel;
    IString recordFilename;

    bool bPlaying;

    void lock()		{}
    void unlock()	{}

    enum { TYPE_BRIDGEMEETING = 3452 };
    virtual int type()
        { return TYPE_BRIDGEMEETING; }
    virtual unsigned int hash()
        { return id.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && id == ((BridgeMeeting*)obj)->id; }
};

class AsteriskCall : public IReferencableObject
{
 public:

 AsteriskCall(void) : bPlaying(false) {};

    void Play(const char *fileName, bool bFlush = false);
    void PlayNext(void);

    AsteriskServer *server;
    IString astID;

    IString iicID;
    IString pin;
    IString meetingID;
    IString userID;
    IString partID;
    IString userName;
    IString subMeetingID;

    IString rosterFilename;

    bool bMuted;
    bool bHold;
    bool bInConf;
    bool bDeleteOnLeave;

    int participantMask;
    int meetingOptions;

    IList playList;
    bool bPlaying;

    void lock()		{}
    void unlock()	{}

    enum { TYPE_ASTERISKCALL = 3453 };
    virtual int type()
        { return TYPE_ASTERISKCALL; }
    virtual unsigned int hash()
        { return astID.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && astID == ((AsteriskCall*)obj)->astID; }
};

class AppConfBridge : public AsteriskEventHandler
{
 public:
    AppConfBridge();
    ~AppConfBridge();

    void AddServer(AsteriskServer *server);
    void RemoveServer(AsteriskServer *server);

    void Reset();
    int RegisterMeeting(const char * meetingID, const char * mainSubMeetingID, const char * callSubMeetingID, int maxParticipants, int startTime, char *iaxIPAddr);
    int RegisterSubmeeting(const char * meetingID, const char * subMeetingID, int maxParticipants);
    int CloseMeeting(const char * meetingID);
    int CloseSubmeeting(const char * meetingID, const char * subMeetingID);
    int ConnectCall(const char * iicCallID, const char * meetingID, const char * subMeetingID, bool bHold, bool bModerator, bool bMute);
    int DisconnectCall(const char * iicCallID, const char * meetingID, const char * subMeetingID);
    int MoveCall(const char * iicCallID, const char * meetingID, const char * oldSubmeetingID, const char * newSubmeetingID);
    int MakeCall(const char * meetingID, const char * subMeetingID, const char * phoneNumber, const char * iicCallID, const char * partID, int callOptions);
    int NotifyParticipant(const char *meetingID, const char * participantID, int isModerator, int outDial, int callOptions);
    int SetVolume(const char * meetingID, const char *subMeetingID, const char * iicCallID, int volume, int gain, int mute);
    int GetVolume(const char * meetingID, const char * subMeetingID, const char *iicCallID, const char * participantID);
    int Error(const char * iicCallID, const char * errorID, int error, const char *msg);
    int Ping();
    int GrantModerator(const char * meetingID, const char * subMeetingID, const char * iicCallID, int grant);
    int ModifySubmeeting(const char * meetingID, const char * subMeetingID, int options);
    int RollCall(const char * meetingID, const char * iicCallID);
    int GetPorts();
    int ResetPort();
    int ResetRecording(const char * meetingID, const char *subMeetingID);

    // Implementation of AsteriskEventHandler interface
    void onAsteriskNewchannel(void *userData, const char *channelID, const char *callID);
    void onAsteriskNewstate(void *userData, const char *channelID, int chanState);
    void onAsteriskHangup(void *userData, const char *channelID);
    void onAsteriskConferenceJoin(void *userData, const char *channelID, const char *conferenceID);
    void onAsteriskConferenceState(void *userData, const char *channelID, int confState);
    void onAsteriskConferenceDTMF(void *userData, const char *channelID, char dtmf);
    void onAsteriskConferenceLeave(void *userData, const char *channelID, const char *conferenceID);
    void onAsteriskConferencePin(void *userData, const char *channelID, const char *conferencePin);
    void onAsteriskOutgoingAccept(void *userData, const char *channelID, const char *iicChanID);
    void onAsteriskConferenceSoundComplete(void *userData, const char *channelID, const char *fileName);
    void onAsteriskServerTransfer(void *userData, const char *channelID, const char *userPin);

    // Methods to generate appropriate RPC results in response to RPC commands received
    // from meeting controller
    void RPCResultFindPin(const char *iicCallID, RPCResultSet * rs);
    void RPCResultReport(const char *iicCallID, RPCResultSet *rs);
    void RPCResultJoinMeeting(const char *iicCallID, RPCResultSet *rs);

 private:
    IList m_serverList;
    IList m_meetingList;
    IList m_callList;
    unsigned int m_numServers;
    unsigned int m_callIDIndex;

    IString m_xferChannelString;

    unsigned int m_maxTotalParticipants;
    unsigned int m_numTotalParticipants;

    // Methods to generate appropriate RPC commands to send to meeting controller
    void sendRPCRegisterUser(spAsteriskCall call);
    void sendRPCCallProgress(spAsteriskCall call, int callProgress);
    void sendRPCJoinMeeting(spAsteriskCall call);
    void sendRPCUnregisterUser(spAsteriskCall call);
    void sendRPCRecordingStarted(const char *conferenceID, time_t time);


    // Miscellaneous utility methods
    IString generateNewCallID(const char *direction);
    IString generateRPCID(spAsteriskCall call, const char * base);
    AsteriskServer * allocateRootServer(int maxParticipants);
    BridgeMeeting * findMeetingFromID(const char * subMeetingID);
    BridgeMeeting *  findMainSubmeeting(const char * iicMeetingID);
    spAsteriskCall  findCallFromIICID(const char * iicCallID);
    spAsteriskCall  findCallFromAstID(const char * astChanID, AsteriskServer *server);
    spAsteriskCall  findCallFromPin(const char *userPin);
    int startRecording(BridgeMeeting *subMeeting);
    int stopRecording(BridgeMeeting *subMeeting);
    int resetRecording(BridgeMeeting *subMeeting);
    void setRecordFilename(BridgeMeeting *subMeeting);
    void addToConference(spAsteriskCall call, BridgeMeeting *subMeeting);
    void restoreServerState(AsteriskServer *server);
    void restoreMeetingParticipants(BridgeMeeting *subMeeting);
};

#endif
