/*
 * Zon Voice Bridge API
 *
 * OVERVIEW:
 *
 * This file provides a framework for integrating other voice bridges
 * into the Zon server environment.  It contains a 'stub'
 * implementation of the voice bridge RPC interface.  The stub
 * routines can be used as a starting point for bridge integration.
 *
 * This file also contains a simple command line interpreter that allows a
 * developer to generate voice brige events that are processes by the
 * rest of the system.
 *
 * The RPC calls implemented by this stub routine are registered using
 * the rpc_add_method() calls in main() below.
 *
 * Copyright (C) 2005 SiteScape, Inc.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <common/common.h>
#include <util/ithread.h>
#include <util/istring.h>
#include <jcomponent/rpc.h>
#include <jcomponent/jcomponent_ext.h>
#include <jcomponent/util/rpcresult.h>
#include <jcomponent/util/nadutils.h>
#include <jcomponent/util/log.h>

#define JCOMPNAME "voice"

static time_t   s_epoch = time(NULL);
static int      s_controller_epoch = 0;
static char *   s_service_name = NULL;
static bool     s_daemon;

static short    s_voice_server_port=5122;
static IThread* s_sending_thread = NULL;

static void *test(void *param);
static void help(void);

static void output_join_meeting_results(RPCResultSet * rs);
static void output_register_user_results(RPCResultSet * rs);
static void output_find_pin_results(RPCResultSet * rs);

int read_config(config_t conf);
extern "C" int JC_connected();

//
// int read_config(config_t conf)
//
// Read voice parameters
//
// The configuration for this daemon is contained in the file
// /opt/iic/conf/config.xml on the host running this daemon.  The
// <voice> section of that XML file contains the configuration for
// this daemon.
//
// config.xml will contain a section like:
//
//   <voice>
//      .
//      .
//      .
//      <init>
//          .
//          .
//          .
//          <service>voice</service>
//          .
//          .
//          .
//      </init>
//      <voiceServerPort>5311</voiceServerPort>
//      <providerConfig>/opt/iic/conf/voiceconfig.xml</providerConfig>
//      .
//      .
//      .
//   </voice>
//
//   You can modify /opt/iic/conf/voiceconfig.xml to contain any
//   configuration parameters for your bridge.  By default, the
//   structure of that file is just:
//
//   <?xml version="1.0" encoding="utf-8"?>
//   <voiceConfig>
//       <param1>value1</param1>
//       <param2>value2</param2>
//       .
//       .
//       .
//   </voiceConfig>
//
#define MAX_PARAM 1024
int read_config(config_t conf)
{
    // This is the port that the meeting archiver uses to
    // fetch 8kHz sampled mu-law wav files of any recorded
    // meeting audio
    char * val = config_get_one(conf, JCOMPNAME, "voiceServerPort", 0);
    s_voice_server_port = (unsigned short)(val ? atoi(val) : 5211);

    // This is the Jabber service name used when this service
    // connects to Jabber.
    val = config_get_one(conf, JCOMPNAME, "init.service", 0);
    if (val == NULL) val = "";

    s_service_name=strdup(val);
    if (!s_daemon)
    {
        printf("Setting service name to %s\n", s_service_name);
    }

    // This is the name of a file that contains any bridge specific
    // configuration.
    char * conf_file = config_get_one(conf, JCOMPNAME, "providerConfig", 0);
    if (conf_file != NULL)
    {
        nad_t nad;

        if (nad_load(conf_file, &nad))
        {
            log_error(ZONE, "unable to load voice parameters, using defaults\n");
            if (!s_daemon)
            {
                fprintf(stderr, "unable to load voice parameters, using defaults\n");
                fprintf(stderr, "Running the command line interpreter\n");
            }

            IThread::initialize();
            IThread thread(test, NULL);

            return Ok;
        }

        int root = 0;

        // The following code iterates through the subsections
        // under <voiceConfig> ... </voiceConfig> in voiceconfig.xml
        // Presumably you would give these parameters to some kind
        // of parameter setter function in your implementation.
        root = nad_find_elem(nad, 0, "voiceConfig", 1);
        if (root > 0)
        {
            int def = nad_find_first_child(nad, root);
            while (def > 0)
            {
                const char *ename = NAD_ENAME(nad, def);
                int ename_length = NAD_ENAME_L(nad, def);
                const char *cdata = NAD_CDATA(nad, def);
                int cdata_length = NAD_CDATA_L(nad, def);
                char param_name[MAX_PARAM];
                char value[MAX_PARAM];

                strcpy(param_name, ename);
                param_name[ename_length] = '\0';

                strcpy(value, cdata);
                value[cdata_length] = '\0';

                if (!s_daemon)
                    printf("Setting %s to %s\n", param_name, value);
                log_debug(ZONE, "Setting %s to %s", param_name, value);

                // Insert your parameter setting code here

                def = nad_find_next_sibling(nad, def);
            }
        }

        nad_free(nad);

        return Ok;
    }

    log_status(ZONE, "No <providerConfig> section; running the command line interpreter");
    if (!s_daemon)
    {
        printf("No <providerConfig> configuration section; running the command line interpreter\n");
    }

    IThread::initialize();
    IThread thread(test, NULL);

    return Ok;
}

char *i_itoa(int i, char *buffer)
{
    sprintf(buffer, "%d", i);
    return buffer;
}

//
// RPC handler: voice.reset
//
// The meeting controller will call this procedure when it is restarted.
// When the meeting controller is restarted, it has no active meetings or
// calls.
//
// Parameters:
//   SECRET - Should match a predefined 'secret' for validation.
//   CONTROLLEREPOCH - This epoch value is updated whenever the meeting
//                     controller state has been reset.
//
// Returns:
//   0k, Failed
xmlrpc_value *reset(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	char * secret;
	int new_epoch;
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(si)", &secret, &new_epoch);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.reset");
        goto cleanup;
    }

	log_status(ZONE, "reset(epoch= %d) called", new_epoch);
    if (!s_daemon)
    {
        printf("\nreset(epoch= %d) called\n>>> ", new_epoch);
        fflush(stdout);
    }


	// Get secret from config
	if (!strcmp(secret, "zQQz98"))
	{
		if (new_epoch != s_controller_epoch)
		{
			log_status(ZONE, "New epoch; resetting");
            if (!s_daemon)
            {
                printf("\nNew epoch; resetting\n>>> ");
            }

			s_controller_epoch = new_epoch;

			// Like a reset, so set epoch
			s_epoch = time(NULL);

            // Register with controller.
			JC_connected();

            // Place any voice bridge specific calls here
            //
            // The NMS Zon bridge terminates all active calls
            // and meetings.
            //
		}
	}
	else
		status = Failed;

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to reset voice service");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to reset voice");

    return output;
}

//
// RPC handler: voice.ping
//
// A ping request is sent when a system component wants to determine
// whether the bridge is reachable and accepting requests.
//
// Parameters:
//    none
//
// Returns:
//    0k - if accepting requests
xmlrpc_value *ping(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    xmlrpc_value *output = NULL;

    if (!s_daemon)
    {
        printf("\nvoice.ping() called\n>>> ");
    }
	log_debug(ZONE, "ping() called");

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", Ok);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to ping voice");

    return output;
}

//
// RPC handler: voice.register_meeting
//
// When the meeting controller starts a meeting, this procedure is called
// to notify the voice bridge that the meeting is started.
//
// The ID's of the main submeeting (voice conference where calls are
// placed by default) and the one-to-one call submeeting id (non-empty
// when a one-to-one call starts the conference).  If the meeting is
// started by a one-to-one call, the initial calls are placed in the
// CALLSUBID submeeting.  Otherwise, calls are placed in MAINSUBMEETINGID
// submeeting.
//
// Parameters:
//   MEETINGID - The meeting ID of the active meeting
//   EXPECTEDPARTICIPANTS - reserved
//   PRIORITY - reserved
//   MAINSUBMEETINGID - The submeeting ID of the main submeeting
//   CALLSUBID - The submeeting ID of any 1-to-1 call submeeting
//
// Returns:
//   0k, Failed
xmlrpc_value *register_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *mtgid, *mainsubmtgid, *callsubmtgid;
    int participants, priority, start_time;
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(siissi)", &mtgid, &participants, &priority, &mainsubmtgid, &callsubmtgid, &start_time);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.register_meeting");
        goto cleanup;
    }

	log_debug(ZONE, "register_meeting(mtgid= %s, mainsubmtgid= %s, callsubmtgid= %s, start= %d) called",
              mtgid, mainsubmtgid, callsubmtgid, start_time);
    if (!s_daemon)
    {
        printf("\nvoice.register_meeting(mtgid= %s, mainsubmtgid= %s, callsubmtgid= %s, start= %d) called\n>>> ",
               mtgid, mainsubmtgid, callsubmtgid, start_time);
        fflush(stdout);
    }


    // Place any voice bridge specific calls here
    //
    // The NMS Zon bridge accepts the meeting id
    // and submeeting id(s) in preparation for the
    // first call.

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to register meeting");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to register meeting");

    return output;
}

//
// RPC handler: voice.close_meeting
//
// The meeting controller calls this procedure when the active meeting
// has ended.
//
// Parameters:
//   MEETINGID - the meeting ID of the ended meeting.
//
// Returns:
//   Ok, Failed
xmlrpc_value *close_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *mtgid;
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(s)", &mtgid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.close_meeting");
        goto cleanup;
    }

	log_debug(ZONE, "close_meeting(mtgid= %s) called", mtgid);
    if (!s_daemon)
    {
        printf("\nvoice.close_meeting(mtgid= %s) called\n>>> ", mtgid);
        fflush(stdout);
    }


    // Place any voice bridge specific calls here
    //
    // The NMS Zon bridge prompts any callers
    // that the meeting has ended and terminates
    // the calls.

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to close meeting");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to close meeting");

    return output;
}

//
// RPC handler: voice.register_submeeting
//
// When the meeting controller creates a new submeeting for participants
// (e.g. when a moderate initiates a one-to-one call while in the meeting)
// the voice bridge is notified that the submeeting will be in use.  Additionally,
// a meeting moderator can select meeting participants in a meeting window
// and move them to a new submeeting.  This call would be invoked in that
// case as well.
//
// Parameters:
//   MEETINGID - The ID of the meeting with a new submeeting
//   SUBMEETINGID - The ID of the new submeeting in meeting MEETINGID.
//   EXPECTEDPARTICIPANTS - reserved
//
// Returns:
//   0k, Failed
xmlrpc_value *register_submeeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *mtgid, *submtgid;
    int participants;
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ssi)", &mtgid, &submtgid, &participants);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.register_submeeting");
        goto cleanup;
    }

	log_debug(ZONE, "register_submeeting(submtgid=%s mtgid=%s) called", submtgid, mtgid);
    if (!s_daemon)
    {
        printf("\nvoice.register_submeeting(submtgid=%s mtgid=%s) called\n>>> ", submtgid, mtgid);
        fflush(stdout);
    }


    // Place any voice bridge specific calls here
    //
    // The NMS Zon bridge accepts the meeting id
    // and submeeting id(s) in preparation for the
    // first call.

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to register submeeting");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to register submeeting");

    return output;
}

//
// RPC handler: voice.modify_submeeting
//
// This call is used by the meeting controller to change the options
// for a submeeting.  OPTIONS is an integer with a number of bit fields.
// This routine would be called when a meeting moderator chooses to
// turn on meeting recording or when join/leave tones are turned on/off
//
// Parameters:
//   MEETINGID - The ID of the meeting that contains the submeeting to modify
//   SUBMEETINGID - The ID of the submeeting to modify
//   OPTIONS - The meeting option flags to apply to the given submeeting
//      The OPTIONS value is formed by OR'ing the following:
//   		1 - Play join/leave tones
//   		2 - Record the submeeting
//		4 - Enable lecture mode
//		8 - Lock the submeeting
//		16 - Enable hold mode
//      32 - Enable chat node
//      64 - Enable screen callers
//
// Returns:
//   0k, Failed
//
xmlrpc_value *modify_submeeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *mtgid, *submtgid;
    int options;
    int status = Ok;
    xmlrpc_value *output = NULL;

    xmlrpc_parse_value(env, param_array, "(ssi)", &mtgid, &submtgid, &options);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.modify_submeeting");
        goto cleanup;
    }

	log_debug(ZONE, "modify_submeeting(mtgid= %s, submtgid= %s, options= %d) called",
              mtgid, submtgid, options);
    if (!s_daemon)
    {
        printf("\nvoice.modify_submeeting(mtgid= %s, submtgid= %s, options= %d) called\n>>> ",
               mtgid, submtgid, options);
        fflush(stdout);
    }


    // Place any voice bridge specific calls here
    //
    // The NMS Zon bridge changes the options on the
    // indicated submeeting

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", Ok);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to modify submeeting");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to modify submeeting");

    return output;
}

//
// RPC handler: voice.reset_recording
//
// This procedure is used to clear any existing voice recording for the conference.
// This is called when a moderator uses the reset recording feature of the Zon client.
//
// Parameters:
//   MEETINGID - The ID of the meeting that contains the recording to reset
//   SUBMEETINGID - The ID of the submeeting being recorded.
//
// Returns:
//   0k, Failed
xmlrpc_value *reset_recording(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *mtgid, *submtgid;
    int status = Ok;
    xmlrpc_value *output = NULL;

    xmlrpc_parse_value(env, param_array, "(ss)", &mtgid, &submtgid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.reset_recording");
        goto cleanup;
    }

	log_debug(ZONE, "reset_recording(mtgid= %s, submtgid= %s) called",
              mtgid, submtgid);
    if (!s_daemon)
    {
        printf("\nvoice.reset_recording(mtgid= %s, submtgid= %s) called\n>>> ",
               mtgid, submtgid);
        fflush(stdout);
    }


    // Place any voice bridge specific calls here
    //
    // The NMS Zon bridge

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", Ok);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to reset recording");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to reset recording");

    return output;
}

//
// RPC handler: voice.close_submeeting
//
// This procedure is called by the meeting controller when the given
// submeeting is no longer in use.
//
// Parameters:
//   MEETINGID - The ID of the meeting that contains the submeeting of interest.
//   SUBMEETINGID - The ID of the submeeting to close
//
// Returns:
//   0k, Failed
xmlrpc_value *close_submeeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *mtgid, *submtgid;
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ss)", &mtgid, &submtgid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.close_submeeting");
        goto cleanup;
    }

	log_debug(ZONE, "reset_recording(mtgid= %s, submtgid= %s) called",
              mtgid, submtgid);
    if (!s_daemon)
    {
        printf("\nvoice.reset_recording(mtgid= %s, submtgid= %s) called\n>>> ",
               mtgid, submtgid);
        fflush(stdout);
    }

    // Place any voice bridge specific calls here

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to close submeeting");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to close submeeting");

    return output;
}


//
// RPC handler: voice.connect_call
//
// This procedure is called when the meeting controller places a call
// into the given submeeting.  It is called during the
// controller.join_meeting processing if the call can be successfully
// joined to the meeting.
//
// The HOLD parameter is a boolean that indicates whether the call
// should be placed in the holding calls for the submeeting (i.e. on
// hold).  The first call with HOLD == 0 (false) should cause any
// holding calls to be released from hold.
//
// The GRANT_MODERATOR flag indicates whether the call should have
// moderator functions available or not.
//
// The MUTE parameter controls whether the call is muted when it is
// placed in the submeeting.
//
// Parameters:
//   CALLID - The ID of the call given on the controller.register_user call.
//   MEETINGID - The ID of the meeting that will contain the connected call.
//   SUBMEETINGID - The ID of the submeeting that will contain the connected call.
//   HOLD - If true, the call should be placed on hold.  The first call
//          that is not holding must cause the holding calls to be released.
//   GRANT_MODERATOR - If true, the call should have moderator available
//   MUTE (0=not muted, 1=normal or lecture mute, 2=hold mute)
//
// Returns:
//   0k, Failed
xmlrpc_value *connect_call(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *callid, *mtgid, *submtgid;
	xmlrpc_bool hold;
	xmlrpc_bool grant_moderator;
    int mute;
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(sssbbi)", &callid, &mtgid, &submtgid, &hold, &grant_moderator, &mute);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.connect_call");
        goto cleanup;
    }

    log_debug(ZONE, "connect_call(callid= %s, mtgid= %s, submtgid= %s, hold= %d, grant_moderator= %d, mute= %d",
              callid, mtgid, submtgid, hold, grant_moderator, mute);
    if (!s_daemon)
    {
        printf("\nvoice.connect_call(callid= %s, mtgid= %s, submtgid= %s, hold= %d, grant_moderator= %d, mute= %d\n>>> ",
               callid, mtgid, submtgid, hold, grant_moderator, mute);
        fflush(stdout);
    }

    // Place any voice bridge specific calls here

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to connect call");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to connect call");

    return output;
}


//
// RPC handler: voice.make_call
//
// This procedure is called by the meeting controller to place an outbound
// call.
//
// Parameters:
//   USERID
//   USERNAME - The XML router user name for the user (e.g. "jsmith@myzon.com")
//   MEETINGID - The ID of the meeting that will contain the new call.
//   SUBMEETINGID - The ID of the submeeting that will contain the new call.
//   PHONE - The phone number that the bridge should dial (see dialing.xml)
//   EXTENSION - Any extension to call
//   OPTIONS - Any call options
//       The following values may be OR'ed together:
//           1  - Callee is assumed to have joined meeting already, so is not prompted when call answers
//           2  - Call is connected to the submeeting before call progress has ended so that supervisor
//                call can hear the call progress tones and/or auto-attendant prompts.
//           4  - Reserved
//           8  - The callee should be prompted for their name for roll-call purposes
//           16 - Call is the master call for a conference.  The meeting controller uses
//                this flag to determine what to do when a master call is dropped.
//
//   SUPERVISINGCALLID - If a one-to-one call, this will contain the call that
//       should 'supervise' this call.  If the supervising call is disconnected,
//       this call should be disconnected.
//   PARTICIPANTID - The participant ID of the called party (may be empty)
//
// Returns:
//   0k, Failed
xmlrpc_value *make_call(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *userid, *username, *callid, *mtgid, *submtgid, *phone, *extension, *partid;
	int options;
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ssssssiss)", &userid, &username, &mtgid, &submtgid, &phone, &extension, &options, &callid, &partid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.make_call");
        goto cleanup;
    }

    log_debug(ZONE, "make_call(userid= %s, username= %s, mtgid= %s, submtgid= %s, phone= %s, ext= %s, options= %d, supcallid= %s, partid=%s called",
              userid, username, mtgid, submtgid, phone, extension, options, callid, partid);
    if (!s_daemon)
    {
        printf("\nvoice.make_call(userid= %s, username= %s, mtgid= %s, submtgid= %s, phone= %s, ext= %s, options= %d, supcallid= %s, partid=%s) called\n>>> ",
               userid, username, mtgid, submtgid, phone, extension, options, callid, partid);
        fflush(stdout);
    }

    // Place any voice bridge specific calls here

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
        switch (status)
        {
            case NoAvailResource:
                xmlrpc_env_set_fault(env, NoAvailResource, "No available system resources to make call");
                break;

            default:
                xmlrpc_env_set_fault(env, Failed, "Unable to make call");
                break;
        }

	}

  cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to make call");

    return output;
}

//
// voice.notify_participant
//
// Notify bridge that a new participant has been invited to the meeting.
//
// Parameters:
//   USERID
//   USERNAME - The XML router user name for the participant (e.g. "jsmith@myzon.com")
//   MEETINGID
//   PARTICIPANTID - The ID of the new participant
//   IS_MODERATOR (0, 1)
//   OUTDIAL (0=no call requested, 1=invitation includes phone notification)
//   OPTIONS (see make_call OPTIONS)
//
// Returns:
//   0k, Failed
xmlrpc_value *notify_participant(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *userid, *username, *mtgid, *partid;
    int is_moderator, outdial, call_options;
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ssssiii)",
		       &userid, &username, &mtgid, &partid, &is_moderator, &outdial, &call_options);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.notify_participant");
        goto cleanup;
    }

    log_debug(ZONE, "notify_participant(userid= %s, username= %s, mtgid= %s, partid=%s is_moderator=%d outdial=%d call_options=%x) called",
	      userid, username, mtgid, partid, is_moderator, outdial, call_options);
    if (!s_daemon)
    {
       printf("\nvoice.notify_participant(userid= %s, username= %s, mtgid= %s, partid=%s is_moderator=%d outdial=%d call_options=%x) called\n>>> ",
	      userid, username, mtgid, partid, is_moderator, outdial, call_options);
        fflush(stdout);
    }

    // Place any voice bridge specific calls here

    // Create return value.

    output = xmlrpc_build_value(env, "(i)", status);

  cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to make call");

    return output;
}

//
// RPC handler: voice.disconnect_call
//
// Disconnect call from submeeting.
//
// Parameters:
//     CALLID - The ID of the call to disconnected (registered with controller.register_user)
//     MEETINGID - The ID of the meeting containing the call
//     SUBMEETINGID - The ID of the submeeting containing the call
//
// Returns:
//   0
xmlrpc_value *disconnect_call(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *callid, *mtgid, *submtgid;
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(sss)", &callid, &mtgid, &submtgid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.disconnect_call");
        goto cleanup;
    }

    log_debug(ZONE, "disconnect_call(callid= %s, mtgid= %s, submtgid= %s) called",
              callid, mtgid, submtgid);
    if (!s_daemon)
    {
        printf("\nvoice.disconnect_call(callid= %s, mtgid= %s, submtgid= %s) called\n>>> ",
               callid, mtgid, submtgid);
        fflush(stdout);
    }

    // Place any voice bridge specific calls here

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to disconnect call");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to disconnect call");

    return output;
}

//
// RPC handler: voice.move_call
//
// Move call to another submeeting.
// Parameters:
//     CALLID
//     MEETINGID - The ID of the meeting containing the call
//     SUBMEETINGID
//     NEWSUBMEETINGID
// Returns:
//   0
xmlrpc_value *move_call(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *callid, *mtgid, *submtgid, *newsubmtgid;
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ssss)", &callid, &mtgid, &submtgid, &newsubmtgid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.move_call");
        goto cleanup;
    }

    log_debug(ZONE, "move_call(mtgid= %s, submtgid= %s, newsubmtgid= %s) called",
              mtgid, submtgid, newsubmtgid);
    if (!s_daemon)
    {
        printf("\nvoice.move_call(mtgid= %s, submtgid= %s, newsubmtgid= %s) called\n>>> ",
               mtgid, submtgid, newsubmtgid);
        fflush(stdout);
    }

    // Place any voice bridge specific calls here

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to move call");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to move call");

    return output;
}


//
// RPC handler: voice.set_volume
//
// Set volue for call
//
// Paramters:
//     CALLID
//     MEETINGID - The ID of the meeting containing the call
//     SUBMEETINGID
//     VOLUME
//     GAIN
//     MUTE (0=not muted, 1=normal or lecture mute, 2=hold mute)
//
// Result:
//    VOLUME, GAIN, MUTE, 0
xmlrpc_value *set_volume(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *callid, *mtgid, *submtgid;
	int volume, gain, mute;
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(sssiii)", &callid, &mtgid, &submtgid, &volume, &gain, &mute);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.set_volume");
        goto cleanup;
    }

    log_debug(ZONE, "set_volume(callid= %s, mtgid= %s, submtgid= %s, volume= %d, gain= %d, mute= %d) called",
              callid, mtgid, submtgid, volume, gain, mute);
    if (!s_daemon)
    {
        printf("\nvoice.set_volume(callid= %s, mtgid= %s, submtgid= %s, volume= %d, gain= %d, mute= %d) called\n>>> ",
               callid, mtgid, submtgid, volume, gain, mute);
        fflush(stdout);
    }

    // Place any voice bridge specific calls here

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to set volume");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to set volume");

    return output;
}

//
// RPC handler: voice.grant_moderator
//
// Grant/revoke moderator status for call
//
// Paramters:
//     CALLID
//     MEETINGID - The ID of the meeting containing the call
//     SUBMEETINGID
//     GRANT
//
// Result:
//    0
xmlrpc_value *grant_moderator(xmlrpc_env *env,
                              xmlrpc_value *param_array,
                              void *user_data)
{
    char *callid;
    char *mtgid;
    char *submtgid;
    int grant;
    int status = Ok;
    xmlrpc_value *output = NULL;

    xmlrpc_parse_value(env, param_array,
                        "(sssi)",
                        &callid, &mtgid, &submtgid, &grant);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError,
                             "Unable to parse argument to voice.grant_moderator");
        goto cleanup;
    }

    log_debug(ZONE, "grant_moderator(callid= %s, mtgid= %s, submtgid= %s, grant= %d) called",
              callid, mtgid, submtgid, grant);
    if (!s_daemon)
    {
        printf("\nvoice.grant_moderator(callid= %s, mtgid= %s, submtgid= %s, grant= %d) called\n>>> ",
               callid, mtgid, submtgid, grant);
        fflush(stdout);
    }


    // Place any voice bridge specific calls here

    if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to grant/revoke moderator status");
	}
cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to grant/revoke moderator status");

    return output;
}

//
// RPC handler: voice.get_volume
//
// Get volume for call
//
// Paramters:
//     CALLID
//     MEETINGID - The ID of the meeting containing the call
//     SUBMEETINGID
//
// Result:
//    VOLUME, GAIN, MUTE, 0
xmlrpc_value *get_volume(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *callid, *mtgid, *submtgid, *participantid;
	int volume=3, gain=3, mute=0;
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ssss)", &callid, &mtgid, &submtgid, &participantid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.get_volume");
        goto cleanup;
    }

    log_debug(ZONE, "get_volume(callid= %s, mtgid= %s, submtgid= %s, partid= %s) called",
              callid, mtgid, submtgid, participantid);
    if (!s_daemon)
    {
        printf("\nvoice.get_volume(callid= %s, mtgid= %s, submtgid= %s, partid= %s) called\n>>> ",
               callid, mtgid, submtgid, participantid);
        fflush(stdout);
    }

    // Place any voice bridge specific calls here

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(iiiis)", volume, gain, mute, 0, participantid);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to get volume");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to get volume");

    return output;
}

//
// RPC handler: voice.roll_call
//
// Roll call for meeting
//
// Parameters:
//     CALLID -
//     MEETINGID - The ID of the meeting containing the call
//
// Returns:
//   0
xmlrpc_value *roll_call(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *callid, *mtgid;
    int status = Ok;
    xmlrpc_value *output = NULL;

    xmlrpc_parse_value(env, param_array, "(ss)", &callid, &mtgid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.roll_call");
        goto cleanup;
    }

    log_debug(ZONE, "roll_call(callid= %s, mtgid= %s) called",
              callid, mtgid);
    if (!s_daemon)
    {
        printf("\nvoice.roll_call(callid= %s, mtgid= %s) called\n>>> ",
               callid, mtgid);
        fflush(stdout);
    }

    // Place any voice bridge specific calls here

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to request roll call");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to request roll call");

    return output;
}

//
// RPC handler: voice.bridge_request
//
// Send special request to bridge
//
// Paramters:
//     CALLID
//     MEETINGID - The ID of the meeting containing the call
//     SUBMEETINGID
//     CODE - request code, 0 clears last request
//     MSG - custom message for request
//
// Result:
//    0
xmlrpc_value *bridge_request(xmlrpc_env *env,
                             xmlrpc_value *param_array,
                             void *user_data)
{
    char *callid;
    char *mtgid;
    char *submtgid;
    int code;
    char *msg;
    int status = Ok;
    xmlrpc_value *output = NULL;

    xmlrpc_parse_value(env, param_array,
                        "(sssis)",
                        &callid, &mtgid, &submtgid, &code, &msg);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError,
                             "Unable to parse argument to voice.bridge_request");
        goto cleanup;
    }

    log_debug(ZONE, "bridge_request(callid= %s, mtgid= %s, submtgid= %s, code= %d) called",
              callid, mtgid, submtgid, code);
    if (!s_daemon)
    {
        printf("\nvoice.bridge_request(callid= %s, mtgid= %s, submtgid= %s, code= %d) called\n>>> ",
               callid, mtgid, submtgid, code);
        fflush(stdout);
    }


    // Place any voice bridge specific calls here

/*  How to send status back to controller:  
    status = rpc_make_call("bridgereq",
                            "controller",
                            "controller.bridge_request_progress",
                            "(sssis)",
                            userid, mtgid, callid, 0, "completed");
*/


    if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to handle bridge request");
	}
cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to handle bridge request");

    return output;
}

//
// RPC: voice.get_ports
//
// This RPC handler is called when an administrator clicks the "view ports"
// link in the administator's console.
//
xmlrpc_value *get_ports(xmlrpc_env *env,
                        xmlrpc_value *param_array,
                        void *user_data)
{
    xmlrpc_value * output = NULL;
    int row_count = 0;

    // This array contains the port status info for the bridge's voice ports
    xmlrpc_value * array = xmlrpc_build_value(env, "()");

    // For each available voice port on the bridge
    {
        // Append the port status information for a particular port to the array
        xmlrpc_value * outstatus = xmlrpc_build_value(env, "(ssbsi)",
                                                      "", // identifier for the voice port
                                                      "", // Participant ID
                                                      0,  // 0 - not connected; 1 - connected
                                                      "some event name",
                                                      0); // seconds since Jan 1, 1970 GMT

        xmlrpc_array_append_item(env, array, outstatus);
        xmlrpc_DECREF(outstatus);
        outstatus = NULL;
        row_count++;
    }

    output = xmlrpc_build_value(env, "(Viiii)",
                                array, 0, row_count, row_count, 0);
    xmlrpc_DECREF(array);

    return output;
}

//
// RPC: voice.reset_port
//
// This RPC handler is used to restore a given voice port to service.
// It is called when an administrator clicks the "reset" link in the
// administrator's console.
//
xmlrpc_value *reset_port(xmlrpc_env *env,
                         xmlrpc_value *param_array,
                         void *user_data)
{
    xmlrpc_value * output = NULL;
    const char *port_id;
    const char *part_id;
    int n_affected = 0;

    xmlrpc_parse_value(env, param_array, "(ss)", &port_id, &part_id);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.reset_port");
        goto cleanup;
    }

    // Place any voice bridge specific calls here

    output = xmlrpc_build_value(env, "(i)", n_affected);

  cleanup:
    return output;
}


//
// RPC handler: voice.error
//
// This RPC call is used by the controller to report error conditions
//
// Paramters:
//     CALLID - The ID of the call associated with the error.
//     ERRORID - The ID of the error.
//     STATUS - A status code (see the end of this file).
//     MESSAGE - A text message associated with the error.
//
// Result:
//     0k
//
xmlrpc_value *error(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *callid, *errorid, *msg;
	int error;
    xmlrpc_value *output = NULL;

    xmlrpc_parse_value(env, param_array, "(ssis)", &callid, &errorid, &error, &msg);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to voice.get_volume");
        goto cleanup;
    }

    log_debug(ZONE, "error(callid= %s, errorid= %s, error= %d, msg= %s) called",
              callid, errorid, error, msg);
    if (!s_daemon)
    {
        printf("\nvoice.error(callid= %s, errorid= %s, error= %d, msg= %s) called\n>>> ",
               callid, errorid, error, msg);
        fflush(stdout);
    }

    // Place any voice bridge specific calls here

    // Create return value.
	output = xmlrpc_build_value(env, "(i)", 0);
	XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable handle error report");

    return output;
}

//
// int JC_connected()
//
// This routine is called when this daemon connects with the XML
// router.  The jcomponent (jc) library will automatically try
// to re-establish a connection to the XML router in the event
// that it is lost.
//
extern "C" int JC_connected()
{
	// Let controller know we just started or reconnected.
	char name[128];
	sprintf(name, "voice:%s", s_service_name);
	rpc_make_call("reg_service",
                  "controller", "controller.register_service",
                  "(si)", name, s_epoch);
	return 0;
}

//
// void RPC_error()
//
// This routine is called when it is not possible to get a result from an RPC
// call because of some error condition.
//
// Parameters:
//     id         - The id of the call given in rpc_make_call(id, ... )
//     fault_code - XMLRPC fault-code
//     message    - An error message
//
extern "C" void RPC_error(const char *id, xmlrpc_int32 fault_code, const char * message)
{
    log_error(ZONE, "*** Error processing RPC request %s: %s\n", id, message);
    if (!s_daemon)
    {
        fprintf(stderr, "\n*** Error processing RPC request %s: %s\n>>> ", id, message);
        fflush(stdout);
    }
}

//
// int RPC_run()
//
// This function is called approx once every 20 msec
// to allow the implementer to poll for events.  It may
// be called more frequently that 20 msec if there is
// data to be processed on the socket to the XML router
//
extern "C" int RPC_run()
{
	int status = Ok;

	return status;
}

//
// void RPC_result()
//
// The results from outbound RPC calls are given to this function.
//
// Arguments:
//     id     - the id given to rpc_make_call(id, ...)
//     resval - the results; see the output_*_results() functions above
//                for some examples of parsing result values
//
extern "C" void RPC_result(const char * id, xmlrpc_value * resval)
{
	xmlrpc_env env;
    xmlrpc_env_init(&env);

	const char * end = strchr(id, ':');
	if (end == NULL)
		return;
	int len = end - id;

	if (!strncmp(id, "find", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

        output_find_pin_results(&rs);
	}
	else if (!strncmp(id, "report", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

		log_debug(ZONE, "controller reports user given in report:%s as registered.", end + 1);
        if (!s_daemon)
        {
            printf("\ncontroller reports user given in report:%s as registered.\n>>> ", end + 1);
            fflush(stdout);
        }

        output_register_user_results(&rs);
	}
	else if (!strncmp(id, "join", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

        output_join_meeting_results(&rs);
	}
    //
    // The remainder of the results below are as a result of the voice
    // bridge executing IVR menu commands ( accessed via DTMF "**" on
    // the NMS bridge implementation )
    //
	else if (!strncmp(id, "togglehand", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

        printf("NOT YET IMPLEMENTED; toggle hand results\n");
	}
	else if (!strncmp(id, "togglevol", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

        printf("NOT YET IMPLEMENTED; toggle volume results\n");
	}
	else if (!strncmp(id, "togglelecture", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

        printf("NOT YET IMPLEMENTED; toggle lecture results\n");
	}
	else if (!strncmp(id, "togglerecord", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

        printf("NOT YET IMPLEMENTED; toggle record results\n");
	}
	else if (!strncmp(id, "togglejoinleave", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

        printf("NOT YET IMPLEMENTED; toggle join/leave results\n");
	}
	else if (!strncmp(id, "placecall", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

        printf("NOT YET IMPLEMENTED; toggle place call results\n");
	}
	else if (!strncmp(id, "lockmtg", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

        printf("NOT YET IMPLEMENTED; lock meeting results\n");
	}
	else if (!strncmp(id, "mute", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

        printf("NOT YET IMPLEMENTED; mute results\n");
	}
	else if (!strncmp(id, "endmeeting", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

        printf("NOT YET IMPLEMENTED; end meeting results\n");
	}
	else if (!strncmp(id, "removesubmeeting", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

        printf("NOT YET IMPLEMENTED; remove submeeting results\n");
	}

cleanup:
    if (env.fault_occurred)
    {
        char buffer[512];
        strcpy(buffer, "Result error: ");
        if (env.fault_string)
            strncat(buffer, env.fault_string, 512 - 16);
        else
            strcat(buffer, "unknown cause");
        RPC_error(id, Failed, buffer);
    }
    xmlrpc_env_clean(&env);
}

static void decode_meeting_options(int options)
{
    const char * option_str[] =
        {
            "",               // 1 - Reserved for future use
            "Screen callers", // 2 - Record caller names and provide roll call
            "Lecture",        // 4 - Lecture mode; moderators unmuted.  Others muted.
            "No new participants", // 8
            "Allow meeting overruns", // 16 - Reserved for future use
            "Has password",           // 32 - A meeting password has been set.
            "Continue without moderator", // 64 - Meeting can run if no moderator is present
            "Expirationless invitations", // 128 - Invite valid at any time

            // Begin GUI specific options
            "Prefer1to1", // 256 - Set the 1 To 1 call check in meeting setup dialog
            "DefaultCallusers", // 512 - Set the Call Users check in meeting setup dialog
            "DefaultSendEmail", // 1024 - Set the Send EmailInvites check in meeting setup dialog
            "DefaultSendIM", // 2048 - Set the Send IM Invite check in meeting setup dialog
            "DefaultModerator", // 4096 - Set the Is Moderator check in meeting setup dialog
            "InvitationVoiceOnly", // 8192 - Meeting setup type set to Voice Only
            "InvitationDataOnly", // 16384 - Meeting setup type set to Data Only
            // End GUI specific options

            "Invitations valid before start", // 32768 - Invites work prior to start of meeting
            "Recording meeting",              // 65536 - Meeting is currently being recorded
            "No join/leave announcements",    // 131072 - Don't play participant join/leave tones
        };
    int len = sizeof(option_str) / sizeof(char *);
    int idx=0;
    int bit=1;

    for (idx = 0; idx < len; idx++, bit <<= 1)
    {
        if (options & bit)
        {
            if (strlen(option_str[idx]))
                printf("\t%s\n", option_str[idx]);
        }
    }
}

//
// void output_join_meeting_results(RPCResultSet * rs)
//
// This routine outputs the results of a controller.join_meeting
// RPC call
//
static void output_join_meeting_results(RPCResultSet * rs)
{
	char *participantid;
	int status;

	// N.B. controller does not report participant ID anymore
	if (!rs->get_output_values("(si)", &participantid, &status))
	{
		log_error(ZONE, "Error parsing result from controller.join_meeting");
		return;
	}

    if (status < 0)
    {
        printf("mtgcontroller reports error status %d while joining participant\n>>> ", status);
        fflush(stdout);
    }
    else
    {
        printf("mtgcontroller reports status %d while joining participant\n>>> ", status);
        fflush(stdout);
    }
}

//
// void output_register_user_results(RPCResultSet * rs)
//
// This routine outputs the results of a controller.register_user
// RPC call
//
static void output_register_user_results(RPCResultSet * rs)
{
	char *sessid, *partid, *weburl;
	int status;

	// User may have become an anonymous user during registration
	if (!rs->get_output_values("(sssi)", &sessid, &partid, &weburl, &status))
	{
		log_error(ZONE, "Error parsing result from controller.register_user()");
		return;
	}

    if (status >= 0)
    {
        if (strlen(partid))
        {
            printf("\nUser sessid %s for partid %s\n>>> ", sessid, partid);
            fflush(stdout);
        }
        else
        {
            printf("\nAnonymous user sessid %s\n>>> ", sessid);
            fflush(stdout);
        }
    }
    else
    {
        printf("\nmtgcontroller reports error %d while registering participant\n>>> ", status);
        fflush(stdout);
    }
}

//
// void output_find_pin_results(RPCResultSet * rs)
//
// This routine outputs the results of a controller.find_pin
// RPC call
//
static void output_find_pin_results(RPCResultSet * rs)
{
	int found, options;
	char *meetingid, *participantid, *userid, *username, *email, *name;
    char effective_partid[32];
    char effective_userid[32];

    if (!rs->get_output_values("(issssssi*)", &found, &meetingid, &participantid, &userid, &email, &username, &name, &options))
	{
		log_error(ZONE, "Error parsing info from pin lookup");
        return;
	}

    printf("---------------\n");
    printf("Find pin results: ");
    switch (found)
    {
        case 1:
            printf("meeting found\n");
            break;
        case 2:
            printf("meeting and participant found\n");
            break;
        case 0:
            printf("NOT FOUND!\n");
            printf("---------------\n>>> ");
            fflush(stdout);
            return;
    }

    if (strchr(participantid, ':'))
    {
        strcpy(effective_partid, participantid);
    }
    else
    {
        if (strlen(participantid)>0)
        {
            strcpy(effective_partid, "part:");
            strcat(effective_partid, participantid);
        }
        else
        {
            effective_partid[0]='\0';
        }
    }

    if (strchr(userid, ':'))
    {
        strcpy(effective_userid, userid);
    }
    else
    {
        if (strlen(userid)>0)
        {
            strcpy(effective_userid, "user:");
            strcat(effective_userid, userid);
        }
        else
        {
            effective_partid[0]='\0';
        }
    }

    printf("Meeting id: %s\n", meetingid);
    printf("Participant id: %s\n", effective_partid);
    if (strlen(effective_partid))
    {
        printf("Session id: %s%s\n", "pin", strchr(effective_partid, ':'));
    }
    printf("User id: %s\n", effective_userid);
    printf("E-mail: %s\n", email);
    printf("User name: %s\n", username);
    printf("Full name: %s\n", name);
    printf("Options:\n");
    decode_meeting_options(options);
    printf("---------------\n>>> ");
    fflush(stdout);
}

#ifndef _WIN32
void * worker(void * sock)
{
    int sockfd = (int)(long) sock;
    int len;
    char buff[1024];

    //  Get iicCmd keyword.  This ensures we don't try to interpret a ping to our port as a command...
    char   cIicCmd[ ] = {"iic:fetchmtg"};
    memset( buff, 0, 1024 );
    if (recv(sockfd, buff, 12, 0) == -1) {
        close(sockfd);
        log_error(ZONE, "worker: expecting iicCmd keyword - recv failure, errno=%d.", errno);
        return NULL;
    }
    if( strcmp( buff, cIicCmd )  !=  0 )
    {
        close(sockfd);
        log_error(ZONE, "worker: invalid iicCmd keyword received.");
        return NULL;
    }

    //  get the length of the name of the recording
    if (recv(sockfd, &len, 4, 0) == -1) {
        close(sockfd);
        log_error(ZONE, "worker: recv failure, errno=%d.", errno);
        return NULL;
    }
    len = ntohl(len);

    //  Sanity check of the length just received
    if( len <= 0  ||  len >= 1023 )
    {
        close(sockfd);
        log_error(ZONE, "worker: received invalid transmission length." );
        return NULL;
    }

    // get recording id
    if (recv(sockfd, buff, len, 0) == -1) {
        close(sockfd);
        log_error(ZONE, "worker: recv failure, errno=%d.", errno);
        if (!s_daemon)
        {
            fprintf(stderr, "\nworker: recv failure, errno=%d.\n>>> ", errno);
            fflush(stdout);
        }

        return NULL;
    }

    buff[len]='\0';

    IString fn;
    const char *recording_dir = "/var/iic/recording";
    if (strlen(recording_dir) > 0)
    {
        fn = recording_dir;
        fn = fn + "/";
    }
    fn = fn + "conf-";
    fn = fn + buff;
    fn = fn + ".wav";

    log_debug(ZONE, "sending recording file, %s.\n", fn.get_string());
    if (!s_daemon)
    {
        printf("\nsending recording file, %s.\n>>> ", fn.get_string());
        fflush(stdout);
    }


    FILE * wavefile = fopen(fn.get_string(), "rb");
    if (wavefile!=NULL) {
        fseek(wavefile, 0, SEEK_END);
        len = ftell(wavefile);
        fseek(wavefile, 0, SEEK_SET);
        len = htonl(len);
        if (send(sockfd, &len, 4, 0) == -1) {
            close(sockfd);
            log_error(ZONE, "worker: send failure, errno=%d.", errno);
            if (!s_daemon)
            {
                fprintf(stderr, "\nworker: send failure, errno=%d.\n>>> ", errno);
                fflush(stdout);
            }

            fclose(wavefile);

            return NULL;
        }
        // send voice recording
        while (!feof(wavefile)) {
            len = fread(buff, 1, sizeof(buff), wavefile);
            if (send(sockfd, buff, len, 0) == -1) {
                log_error(ZONE, "worker: send failure, errno=%d.", errno);
                if (!s_daemon)
                {
                    fprintf(stderr, "\nworker: send failure, errno=%d.\n>>> ", errno);
                    fflush(stdout);
                }

                break;
            }
        }
        fclose(wavefile);
    }
    else {
        log_error(ZONE, "worker: could not open meeting recording file, %s.", fn.get_string());
        if (!s_daemon)
        {
            fprintf(stderr, "\nworker: could not open meeting recording file, %s.\n>>> ", fn.get_string());
            fflush(stdout);
        }


        len = 0;
        if (send(sockfd, &len, 4, 0) == -1) {
            close(sockfd);
            log_error(ZONE, "worker: send failure, errno=%d.", errno);
            if (!s_daemon)
            {
                fprintf(stderr, "\nworker: send failure, errno=%d.\n>>> ", errno);
                fflush(stdout);
            }

            return NULL;
        }
    }

    close(sockfd);

    return NULL;
}

void * send_recording(void * pvoid)
{
    unsigned port = (unsigned)(long) pvoid;
    int sockfd, new_fd;  // listen on sock_fd, new connection on new_fd
    struct sockaddr_in my_addr;    // my address information
    struct sockaddr_in client_addr; // connector's address information
    socklen_t sin_size;
    int yes=1;

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
            log_error(ZONE, "send_recording: socket creation failure.");
            if (!s_daemon)
            {
                fprintf(stderr, "\nsend_recording: socket creation failure.\n>>> ");
            }

            return NULL;
    }

    if (setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int)) == -1) {
            log_error(ZONE, "send_recording: setsockopt failure.");
            if (!s_daemon)
            {
                fprintf(stderr, "\nsend_recording: setsockopt failure.\n>>> ");
            }

            return NULL;
    }

    my_addr.sin_family = AF_INET;                // host byte order
    my_addr.sin_port = htons(port);                 // short, network byte order
    my_addr.sin_addr.s_addr = INADDR_ANY; // automatically fill with my IP
    memset(&(my_addr.sin_zero), '\0', 8);       // zero the rest of the struct

    if (bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) == -1) {
            log_error(ZONE, "send_recording: bind failure, errno=%d.", errno);
            if (!s_daemon)
            {
                fprintf(stderr, "\nsend_recording: bind failure, errno=%d.\n>>> ", errno);
                fflush(stdout);
            }

            return NULL;
    }

    if (listen(sockfd, SOMAXCONN) == -1) {
            log_error(ZONE, "send_recording: listen failure, errno=%d.", errno);
            if (!s_daemon)
            {
                fprintf(stderr, "\nsend_recording: listen failure, errno=%d.\n>>> ", errno);
                fflush(stdout);
            }

            return NULL;
    }

    while(1) {  // main accept() loop
        sin_size = sizeof(struct sockaddr_in);
        if ((new_fd = accept(sockfd, (struct sockaddr *)&client_addr,
                                                       &sin_size)) == -1) {
            log_error(ZONE, "send_recording: accept failure, errno=%d.", errno);
            if (!s_daemon)
            {
                fprintf(stderr, "\nsend_recording: accept failure, errno=%d.\n>>> ", errno);
                fflush(stdout);
            }

            continue;
        }

        IThread(worker, (void*)new_fd);
//        worker((void*)new_fd);
//        close(new_fd);
    }
    close(sockfd);

    return 0;

}
#endif

int main(int argc, char * argv[])
{
    int status;

    pid_t pg = getpgrp();
    if (pg > 0)
    {
        s_daemon = false;
    }
    else if (pg == 0)
    {
        s_daemon = true;
    }
    else
    {
        fprintf(stderr, "Can't determine whether running as a daemon: %s\n",
                strerror(errno));
    }

    log_debug(ZONE, "voice bridge starting");
    if (!s_daemon)
    {
        printf("voice bridge starting\n");
    }

    int instance = 0;

	if (argc > 1)
		instance = atoi(argv[1]);

    config_t conf = config_new();

    // reads the <instance>-th "voice" section from config.xml
    config_load(conf, JCOMPNAME, instance, "/opt/iic/conf/config.xml");

    // initialize the common daemon configuration
	if (jc_init(conf, JCOMPNAME))
	{
		log_error(ZONE, "Unable to initialize voice\n");
        if (!s_daemon)
        {
            fprintf(stderr, "Unable to initialize voice\n");
        }

		return -1;
	}

    // initialize this daemon's configuration
    if (read_config(conf))
    {
        return -1;
    }

    // JC_connected() is called whenever the connection to the XML router
    // is (re)established
    jc_set_connected_callback(JC_connected);

    // RPC_result is called when the called RPC returns a result
    //
    // RPC_error is called when an error prevents RPC from returning a result
    //
    // RPC_run is called periodically when a timer expires (every 20 msec) or
    //      when there is network activity over the jabber socket.   The intent
    //      is to give the implementer a chance to check for application events/poll/etc.
    //
	rpc_set_callbacks(RPC_result, RPC_error, RPC_run);

    IThread::initialize();
#ifndef _WIN32
    // This thread is used to service requests for meeting audio from the
    // meeting archiver
    s_sending_thread = new IThread(send_recording, (void*)s_voice_server_port, true);
#endif

	// Add methods to RPC server
    rpc_add_method("voice.reset", &reset, NULL);
    rpc_add_method("voice.register_meeting", &register_meeting, NULL);
    rpc_add_method("voice.register_submeeting", &register_submeeting, NULL);
    rpc_add_method("voice.close_meeting", &close_meeting, NULL);
    rpc_add_method("voice.close_submeeting", &close_submeeting, NULL);
    rpc_add_method("voice.connect_call", &connect_call, NULL);
    rpc_add_method("voice.disconnect_call", &disconnect_call, NULL);
    rpc_add_method("voice.move_call", &move_call, NULL);
	rpc_add_method("voice.make_call", &make_call, NULL);
	rpc_add_method("voice.notify_participant", &notify_participant, NULL);
	rpc_add_method("voice.set_volume", &set_volume, NULL);
	rpc_add_method("voice.get_volume", &get_volume, NULL);
	rpc_add_method("voice.error", &error, NULL);
    rpc_add_method("voice.ping", &ping, NULL);
    rpc_add_method("voice.grant_moderator", &grant_moderator, NULL);
    rpc_add_method("voice.modify_submeeting", &modify_submeeting, NULL);
    rpc_add_method("voice.roll_call", &roll_call, NULL);
    rpc_add_method("voice.bridge_request", &bridge_request, NULL);
    rpc_add_method("voice.get_ports", &get_ports, NULL);
    rpc_add_method("voice.reset_port", &reset_port, NULL);
    rpc_add_method("voice.reset_recording", &reset_recording, NULL);

	jc_run(false); // This routine contains an event loop that invokes the JC_*() and RPC_*()
                   // callbacks defined above

	return 0;
}

// =======================
// Voice stub command line
// =======================

void help()
{
    const char *help_txt =
        "COMMAND OVERVIEW\n"
        "\tq -\n"
        "\t\tquit voice bridge\n"
        "\tf <pin> (controller.find_pin) -\n"
        "\t\tAsk controller to validate a pin\n"
        "\tc <participant-pin> <callid> <username> (controller.register_user) -\n"
        "\t\tregister the given call under the user's id and name\n"
        "\tj <sessid> <callid> <meetingid> <partid> (controller.join_meeting) -\n"
        "\t\tJoin the given participant to the meeting\n"
        "\td <sessid> <callid> <meetingid> (controller.unregister_user) -\n"
        "\t\tReport a disconnected call to the meeting controller\n"
        "\tp <sessid> <callid> <mtgid> <progress> (controller.call_progress) -\n"
        "\t\tReport call progress for a call\n"
        "\t\tValid <progress> values:\n"
        "\t\t\tclear status = 0\n"
        "\t\t\tdialing      = 100\n"
        "\t\t\tringing      = 101\n"
        "\t\t\tbusy         = 102\n"
        "\t\t\treorder      = 103\n"
        "\t\t\tspecial info = 104\n"
        "\t\t\tanswered     = 105\n"
        "\t\t\tspeech       = 106\n"
        "\t\t\tremote hangup= 107\n"
        "\t\t\terror        = 108\n"
        ;
    printf("%s", help_txt);
}

void mk_call_id(char *out_call_id, char *base)
{
    static int rpc_call_seq=1;

    sprintf(out_call_id, "%s:%d", base, rpc_call_seq++);
}

void *test(void *param)
{
    char rpc_call_id[MAX_ID_LENGTH];

    printf("Test mode (type 'h' for a list of commands)\n");
	int ch;

	while (true)
	{
        printf(">>> ");

		while (isspace(ch = getchar()));

		if (ch == 'q' || ch == -1)
			break;
		else if (ch == 'f')
		{
            // OVERVIEW
            //
            //     The 'f' command (controller.find_pin) asks the meeting
            //     controller to validate the a pin code entered via DTMF
            //     codes by a participant.  If your bridge does PIN validation
            //     internally, you will not use this call.
            //
            //     The pin code can be either a participant pin code
            //     or a meeting pin code.  The result returned by
            //     controller.find_pin will have other details about
            //     the caller (e.g. system user id, participant id,
            //     etc.) and whether the pin is a meeting pin
            //     (anonymous user) or a participant pin
            //
            // Parameters:
            //     "sys:voice" - Identifies the RPC initiator as the voice bridge
            //     <pin code>  - The DTMF digits entered by the participant
            //
			char pin[MAX_ID_LENGTH];
			scanf("%s", pin);

            mk_call_id(rpc_call_id, "find");
			if (rpc_make_call(rpc_call_id,
                              "controller", "controller.find_pin", "(ss)",
                              "sys:voice", pin))
            {
				log_error(ZONE, "Unable to lookup pin %s", pin);
                if (!s_daemon)
                {
                    fprintf(stderr, "Unable to lookup pin %s", pin);
                }
            }
		}
		else if (ch == 'c')
		{
            // USAGE
            //
            //     c <pinid> <callid> <username>
            //
            // OVERVIEW
            //
            //     The 'c' command (controller.register_user) is used
            //     to notify the controller that a new call has entered
            //     the system.
            //
            //     For outbound calls, the RPC call is made during
            //     voice.make_call processing.  The RPC call is done
            //     prior to any voice call progress indication.
            //
            //     For inbound calls, the RPC call is made after the
            //     participant is identified by the voice bridge.
            //
            //
            // Parameters:
            //
            //     <pinid> - This is a valid meeting id or participant id.
            //          Must have a prefix of "pin:".  For outbound calls,
            //          the participant id is given on the voice.make_call()
            //          RPC call.   For inbound calls, the participant id is
            //          obtained using the controller.find_pin() call or is
            //          provided by bridge if pin validation is handled
            //          externally.
            //
            //     <callid> - The call id is a unique identifier of all
            //          inbound/outbound calls reported by the voice bridge.
            //          It has the following form:
            //
            //              voice:<dir>-<seq>@<service>
            //
            //         <dir> is either "in" or "out" depending on whether the call is
            //              inbound vs. outbound.
            //
            //         <seq> is a sequence number to make the id unique within a bridge
            //
            //         <service> is the service name of the bridge (e.g. voice, voice1,
            //              voice2, ... etc.)
            //         For instance, voice:in-343@voice is a well-formed call id
            //
            //     <username> - The username of the caller or '*' if not available.
            //         The syntax of the user name is <screenname>@<xml router host>.
            //         For instance, "admin@ourzon.mega.com". For outbound calls,
            //         the user name is in the voice.make_call() RPC call.
			char pinid[MAX_ID_LENGTH];
			char callid[MAX_ID_LENGTH];
			char username[MAX_ID_LENGTH];

			scanf("%s %s %s", pinid, callid, username);
            printf("Reporting call for pinid= %s callid= %s username=%s\n",
                   pinid, callid, username);

			if (strcmp(username, "*") == 0) username[0] = '\0';

            mk_call_id(rpc_call_id, "report");
			int status = rpc_make_call(rpc_call_id,
                                       "controller", "controller.register_user", "(ssssss)",
                                       pinid, username, "", callid, "", "");
			if (status != 0)
            {
				log_error(ZONE, "Unable to dispatch call %s to controller", callid);
                if (!s_daemon)
                {
                    fprintf(stderr, "Unable to dispatch call %s to controller", callid);
                }
            }
		}
		else if (ch == 'j')
		{
            // USAGE
            //
            //     j <sessid> <callid> <meetingid> <partid>
            //
            // OVERVIEW
            //
            // The 'j' command (controller.join_meeting) informs the
            // meeting controller that a call is ready to be joined to
            // the meeting. Join meeting is used after the participant
            // has been registered using controller.register_user ('c'
            // command).
            //
            // Once the controller joins the call to the conference, it
            // will issue the voice.connect_call() RPC call to confirm the
            // join.
            //
            // Parameters:
            //    <sessid> - The session ID returned by the meeting controller
            //        in response to the controller.register_user() RPC call.
            //        see output_register_user_results() for more info.
            //    <callid> - The same call id used in the 'c' command
            //        (controller.register_user).
            //    <meetingid> - The ID of the meeting to join
            //    <partid> - Either '*' for anonymous participants or the value
            //        returned by the meeting controller in response to the
            //        controller.register_user() RPC call.
            //
			char sessid[MAX_ID_LENGTH];
			char callid[MAX_ID_LENGTH];
			char meetingid[MAX_ID_LENGTH];
			char partid[MAX_ID_LENGTH];

			scanf("%s %s %s %s", sessid, callid, meetingid, partid);

			if (strcmp(partid, "*") == 0) partid[0] = '\0';

            mk_call_id(rpc_call_id, "join");
		    int status = rpc_make_call(rpc_call_id,
                                       "controller", "controller.join_meeting", "(sssss)",
                                       sessid, callid, meetingid, partid, "" /* meeting password */);
            printf("Reporting join meeting sessid= %s callid= %s meetingid=%s partid=%s\n",
                   sessid, callid, meetingid, partid);
			if (status != 0)
            {
				log_error(ZONE, "Unable to join meeting %s to controller", meetingid);
                if (!s_daemon)
                {
                    fprintf(stderr, "Unable to join meeting %s to controller", meetingid);
                }
            }
		}
		else if (ch == 'd')
		{
            // USAGE
            //
            //     d <sessid> <callid> <meetingid>
            //
            // OVERVIEW
            //
            //     The 'd' command (controller.unregister_user) is called when
            //     a previously registered call (via 'c' command) terminates.
            //
            // Parameters:
            //
            //    <sessid> - The session ID returned by the meeting controller
            //         in response to the controller.register_user() RPC call.
            //    <callid> - The same call id used in the 'c' command
            //        (controller.register_user).
            //    <meetingid> - The meeting ID for the call.
            //
			char sessid[MAX_ID_LENGTH];
			char callid[MAX_ID_LENGTH];
            char meetingid[MAX_ID_LENGTH];

			scanf("%s %s %s", sessid, callid, meetingid);

            if (strcmp(meetingid, "*") == 0)
            {
                meetingid[0]='\0';
            }

            mk_call_id(rpc_call_id, "disconnect");
			int status = rpc_make_call(rpc_call_id, "controller", "controller.unregister_user", "(sss)",
				sessid, callid, meetingid);
            printf("Reporting disconnect for sessid= %s callid= %s meetingid=%s\n",
                   sessid, callid, meetingid);
			if (status != 0)
            {
				log_error(ZONE, "Unable to dispatch call %s to controller", callid);
                if (!s_daemon)
                {
                    fprintf(stderr, "Unable to dispatch call %s to controller", callid);
                }
            }
		}
		else if (ch == 'p')
		{
            // USAGE
            //
            //     p <sessid> <callid> <meetingid> <progress>
            //
            // OVERVIEW
            //
            //    The 'p' (controller.call_progress) command reports call
            //    progress and other status events for a participant to
            //    the meeting controller.  It must be used after the 'c'
            //    (controller.register_user) command.  Code 107 (Talking)
            //    can be used after the participant is joined to the
            //    meeting (turns on the talk bubble icon beside the
            //    participant in the meeting window).  To clear all call
            //    progress/status indicators, use Code 0.
            //
            // Parameters:
            //
            //    <sessid> - The session ID returned by the meeting controller
            //         in response to the controller.register_user() RPC call.
            //    <callid> - The same call id used in the 'c' command
            //        (controller.register_user).
            //    <meetingid> - The ID of the meeting to join
            //    <progress> - The call progress code to send
            //			clear status = 0
            //			dialing      = 100
            //			ringing      = 101
            //			busy         = 102
            //			reorder      = 103
            //			special info = 104
            //			answered     = 105
            //			speech       = 106
            //			remote hangup= 107
            //			error        = 108
			char sessid[MAX_ID_LENGTH];
			char callid[MAX_ID_LENGTH];
			char meetingid[MAX_ID_LENGTH];
			int progress;
			int ext = -1;

			scanf("%s %s %s %d",
                  sessid, callid, meetingid, &progress);
            printf("Reporting call progress: sessid= %s callid= %s meetingid= %s progress= %d\n",
                   sessid, callid, meetingid, progress);

			switch (progress)
			{
				case 0:
					ext = 0;
					break;
				case 100:
					ext = StatusDialing;
					break;
				case 101:
					ext = StatusRinging;
					break;
				case 102:
				case 103:
					ext = StatusBusy;
					break;
				case 104:
					ext = StatusSpecialInfo;
					break;
				case 105:
					ext = StatusAnswered;
					break;
				case 106:
					ext = StatusTalking;
					break;
				case 107:
					ext = StatusRemoteHangup;
					break;
				case 108:
					ext = StatusTelephonyError;
					break;
                default:
                    fprintf(stderr, "Unknown status %d", progress);
                    break;
			}

            mk_call_id(rpc_call_id, "progress");
			int status = rpc_make_call(rpc_call_id,
                                       "controller",
                                       "controller.call_progress",
                                       "(sssi)",
                                       sessid, meetingid, callid, ext);
			if (status != 0)
            {
				log_error(ZONE, "Unable to dispatch controller RPC call");
                if (!s_daemon)
                {
                    fprintf(stderr, "Unable to dispatch controller RPC call");
                }
            }
		}
        else if (ch == 'h')
        {
            help();
        }
        else
        {
            printf("Unknown command char '%c' seen!\n", ch);
            help();
        }
	}

    exit(0);

    return NULL;
}

/*
 * RPC call status codes
 *
 *  Non-error status codes
 *	------------------
 *  Ok                         : 0
 *  Pending                    : 1
 *  Retry                      : 2
 *  WaitingForModerator        : 3
 *
 *  Common error codes
 *	------------------
 *  Failed                     : -1000
 *	InvalidPIN                 : -1001
 *	InvalidSessionID           : -1002
 *  InvalidMeetingID           : -1003
 *  InvalidSubmeetingID        : -1004
 *	InvalidUserID              : -1005
 *	InvalidParticipantID       : -1006
 *	InvalidServiceID           : -1007
 *  DuplicateID                : -1008
 *	InvalidParameter           : -1009
 *	ParameterParseError        : -1010
 *  DuplicateUserName          : -1011
 *  PINAllocationError         : -1012
 *  NoClientConnection         : -1013
 *  PINExpired                 : -1014
 *  PINNotValidYet             : -1015
 *
 *	Error codes from controller
 *	---------------------------
 *	InvalidAddress             : -2000
 *  NotAuthorized              : -2001
 *	NotMember                  : -2002
 *  InsufficientResources      : -2003
 *	NotLoaded                  : -2004
 *  AlreadyConnected           : -2005
 *	NotConnected               : -2006
 *	NotActive                  : -2007
 *	NoPhoneNumber              : -2008
 *	InAnotherMeeting           : -2009
 *	MeetingExists              : -2010
 *	InvalidPassword            : -2011
 *  Disconnected               : -2012
 *  NotWhileLecture            : -2013
 *	MeetingLocked              : -2014
 *	PhoneNumberNotFound        : -2015
 *	MeetingStartTimeout        : -2016
 *	NoPort				       : -2017
 *	MtgArchiverTimeout         : -2018
 *
 *	Error codes from voice service
 *	------------------------------
 *  InvalidCallID              : -3000
 *	NotConferenced             : -3001
 *	MissingPIN                 : -3002
 *  NoAvailResource            : -3003
 *
 *	Error codes from address book/schedule
 *	--------------------------------------
 *	DBAccessError              : -4000
 *	AddressBookError           : -4001
 *	DuplicateContact           : -4002
 *	ProfileSystemDeleteError   : -4003
 *	ProfileInUseError          : -4004
 *	UnknownGroup               : -4005
 *  CommunitySystemDeleteError : -4006
 *  CommunityNotFound          : -4007
 *  CommunityStatusNotFound    : -4008
 *  CommunityDisabled          : -4009
 *  CommunityNameNotUnique     : -4010
 *  DirectoryNotFound          : -4011
 *  ProfileNotFound            : -4012
 *	DuplicateUsername          : -4013
 *  MaxMeetingsExceeded        : -4014
 *  ProfileNameNotUnique       : -4015
 *  InstantMeetingNotFound     : -4016
 *  UserNotFound               : -4017
 *  MaxPresenceExceeded        : -4018
 *  InvalidScreenName          : -4019
 *  InvalidScreenPassword      : -4020
 *  CommunityOwnerDeleteError  : -4021
 */

