#include <astxx/manager.h>
#include <ithread.h>
#include <istring.h>
#include <stdio.h>
#include "astserver.h"

#define ServerError(format, ...) fprintf(stderr,"(%s:%d)",__FILE__,__LINE__);fprintf(stderr, format, ## __VA_ARGS__);fprintf(stderr,"\n")
#define ServerDebug(format, ...) fprintf(stderr, format, ## __VA_ARGS__);fprintf(stderr,"\n")

#if 0
#define ServerError(format, args...) fprintf(stderr, format, args)
#define ServerDebug(format, args...) fprintf(stdout, format, args)
#endif

static void astxxSleep(int ms)
{
	struct timespec req;
	req.tv_sec = 0;
	req.tv_nsec = ms % 1000 * 1000000;
	nanosleep(&req, NULL);
}

void * astxxProcessEvents(void *param)
{

    AsteriskServer *server = (AsteriskServer *) param;

    while (1)
    {

        server->m_connMutex.lock();

        try
        {
            server->m_connection.pump_messages();
            server->m_connection.process_events();
        }
        catch (const std::exception& e)
        {
            ServerError("Exception in event processing thread:  %s", e.what());
        }

        server->m_connMutex.unlock();
        astxxSleep(20);

    }


    return NULL;
}

AsteriskServer::AsteriskServer(AsteriskEventHandler *eventHandler, int externalProvider, int maxParticipants, const char * ipAddr, unsigned long port) :
    m_connection(ipAddr),
    m_eventHandler(eventHandler),
    m_extProvider(externalProvider),
    m_maxParticipants(maxParticipants),
    m_numParticipants(0),
    m_ipAddress(ipAddr)
{
}

AsteriskServer::~AsteriskServer()
{
}

void AsteriskServer::Initialize(void)
{

    boost::function<void (astxx::manager::message::event)> f;
    f = std::bind1st(std::mem_fun(&AsteriskServer::astxxEventHandler), this);
    m_connection.register_event("", f);

    // create a thread to read data from

    m_eventThread = new IThread(astxxProcessEvents, (void *)this);

}

int AsteriskServer::Connect(const char * username, const char * secret)
{
    astxx::manager::action::login login(username, secret);
    m_connMutex.lock();
    login(m_connection);
    m_connMutex.unlock();


    return AST_SUCCESS;
}

int AsteriskServer::Disconnect(void)
{
    m_connMutex.lock();
    m_connection(astxx::manager::action::logoff());
    m_connMutex.unlock();


    return AST_SUCCESS;
}

int AsteriskServer::AppConfCreateConference(const char * confID)
{

    // Dynamically add a couple of extensions for each conference
    // into the dialplan:
    //
    // RECORD_<confID> -> used for originating calls to the Record()
    //                    application when recording the conference
    //
    // PLAY_<confID>   -> used for originating calls to the Playback()
    //                    application when playing to the conference
    //

    IString command_string = "dialplan add extension ";
    command_string.append("RECORD_");
    command_string.append(confID);
    command_string.append(",1,Conference,");
    command_string.append(confID);
    command_string.append("/RN into iic-conferences");
    sendCommand(command_string);

    command_string = "dialplan add extension ";
    command_string.append("PLAY_");
    command_string.append(confID);
    command_string.append(",1,Conference,");
    command_string.append(confID);
    command_string.append("/RN into iic-conferences");
    sendCommand(command_string);

    return AST_SUCCESS;
}

int AsteriskServer::AppConfDeleteConference(const char * confID)
{
    IString command_string = "conference end ";
    command_string.append(confID);
    sendCommand(command_string);

    command_string = "dialplan remove extension ";
    command_string.append("PLAY_");
    command_string.append(confID);
    command_string.append("@iic-conferences");
    sendCommand(command_string);

    command_string = "dialplan remove extension ";
    command_string.append("RECORD_");
    command_string.append(confID);
    command_string.append("@iic-conferences");
    sendCommand(command_string);

    return AST_SUCCESS;
}

int AsteriskServer::AppConfAddToConference(const char * astChanID, const char * confID, unsigned int participantMask)
{
    IString confParticipantOptions = "RN";

    if (participantMask & PARTICIPANT_MASK_MUTE)
        confParticipantOptions.append("L");

    astxx::manager::action::setvar setConfID(astChanID, "confID", confID);
    m_connMutex.lock();
    setConfID(m_connection);
    m_connMutex.unlock();

    astxx::manager::action::setvar setOptions(astChanID, "confOptions", confParticipantOptions.get_string());
    m_connMutex.lock();
    setOptions(m_connection);
    m_connMutex.unlock();

    if (participantMask & PARTICIPANT_MASK_RECORD)
    {
        IString recordFileName = generateNameRecordFilename(astChanID);

        astxx::manager::action::setvar setRecordNameFile(astChanID, "recordNameFile", recordFileName.get_string());
        m_connMutex.lock();
        setRecordNameFile(m_connection);
        m_connMutex.unlock();

        astxx::manager::action::redirect redirect(astChanID, "iic-callflow", "recordname", 1);
        m_connMutex.lock();
        redirect(m_connection);
        m_connMutex.unlock();
    }
    else if (participantMask & PARTICIPANT_MASK_MOVE)
    {
        astxx::manager::action::redirect redirect(astChanID, "iic-callflow", "confentrymoved", 1);
        m_connMutex.lock();
        redirect(m_connection);
        m_connMutex.unlock();
    }
    else
    {
        astxx::manager::action::redirect redirect(astChanID, "iic-callflow", "confentry", 1);
        m_connMutex.lock();
        redirect(m_connection);
        m_connMutex.unlock();
    }

    return AST_SUCCESS;
}

int AsteriskServer::AppConfRemoveFromConference(const char * astChanID, const char * confID)
{
    IString command_string = "conference kickchannel ";
    command_string.append(confID);
    command_string.append(" ");
    command_string.append(astChanID);
    sendCommand(command_string);




    return AST_SUCCESS;
}

int AsteriskServer::AppConfPlayToConference(const char * confID, const char * fileName)
{
    // Playing audio into a conference requires us to originate a "call"
    // containing the audio into the conference
    IString channel = "Local/PLAY_";
    channel.append(confID);
    channel.append("@iic-conferences");

    astxx::manager::action::originate originate(channel.get_string(), "Playback", fileName);
    m_connMutex.lock();
    originate(m_connection);
    m_connMutex.unlock();

    return AST_SUCCESS;
}

int AsteriskServer::AppConfPlayToParticipant(const char * astChanID, const char * fileName)
{
    IString command_string = "conference play sound ";
    command_string.append(astChanID);
    command_string.append(" ");
    command_string.append(fileName);
    sendCommand(command_string);

    return AST_SUCCESS;
}

int AsteriskServer::AppConfMuteParticipant(const char * astChanID, const char * confID)
{
    IString command_string = "conference mutechannel ";
    command_string.append(astChanID);
    sendCommand(command_string);

    return AST_SUCCESS;
}

int AsteriskServer::AppConfUnmuteParticipant(const char * astChanID, const char * confID)
{

    IString command_string = "conference unmutechannel ";
    command_string.append(astChanID);
    sendCommand(command_string);

    return AST_SUCCESS;
}

int AsteriskServer::AppConfMakeCall(const char * dialString, const char * iicCallID)
{
    std::map<std::string, std::string> vars;
    IString astChannel;
    vars["iicID"] = iicCallID;

    switch (m_extProvider)
    {
    case PROVIDER_SIP:
        astChannel = "SIP/";
        astChannel.append(dialString);
        astChannel.append("@sip-provider");
        break;
    default:
        ServerError("Trying to make call using unsupported external provider %d", m_extProvider);
        return AST_FAIL;
    }

    astxx::manager::action::originate originate(astChannel.get_string(), "iic-callflow", "outgoingstart", 1);
    originate.variables(vars);
    ServerDebug("%s", originate.action().format().c_str());
    m_connMutex.lock();
    astxx::manager::message::response response = originate(m_connection);
    m_connMutex.unlock();
    ServerDebug("%s", response.format().c_str());
    if (response == "Error")
    {
        return AST_FAIL;
    }

    return AST_SUCCESS;
}

int AsteriskServer::AppConfPinLookupFail(const char *astChanID)
{

    astxx::manager::action::redirect redirect(astChanID, "iic-callflow", "systemerror", 1);
    m_connMutex.lock();
    redirect(m_connection);
    m_connMutex.unlock();

    return AST_SUCCESS;
}

int AsteriskServer::AppConfInvalidPin(const char *astChanID)
{
    astxx::manager::action::redirect redirect(astChanID, "iic-callflow", "invalidpin", 1);
    m_connMutex.lock();
    redirect(m_connection);
    m_connMutex.unlock();

    return AST_SUCCESS;
}

int AsteriskServer::AppConfPreConfHold(const char *astChanID)
{
    astxx::manager::action::redirect redirect(astChanID, "iic-callflow", "confhold", 1);
    m_connMutex.lock();
    redirect(m_connection);
    m_connMutex.unlock();

    return AST_SUCCESS;
}

int AsteriskServer::AppConfStartRecording(const char *confID, const char *fileName)
{
    IString channel = "Local/RECORD_";
    channel.append(confID);
    channel.append("@iic-conferences");

    astxx::manager::action::originate originate(channel.get_string(), "Record", fileName);
    m_connMutex.lock();
    originate(m_connection);
    m_connMutex.unlock();

    return AST_SUCCESS;
}

int AsteriskServer::AppConfSuspendRecording(const char *recordChannelID)
{
    astxx::manager::action::redirect redirect(recordChannelID, "iic-callflow", "recordpause", 1);
    m_connMutex.lock();
    redirect(m_connection);
    m_connMutex.unlock();
    return AST_SUCCESS;

}

int AsteriskServer::AppConfResumeRecording(const char *recordChannelID)
{
    IString conf_extension = (recordChannelID + strlen("Local/"));
    int len = conf_extension.find("@");
    conf_extension.truncate(len);

    astxx::manager::action::redirect redirect(recordChannelID, "iic-conferences", conf_extension.get_string(), 1);
    m_connMutex.lock();
    redirect(m_connection);
    m_connMutex.unlock();

    return AST_SUCCESS;


}

int AsteriskServer::AppConfServerTransfer(const char *astChanID, const char *userPIN, const char *ipAddr)
{
    IString xferDestination = "xfer";
    xferDestination.append(userPIN);
    xferDestination.append("@");
    xferDestination.append(ipAddr);

    astxx::manager::action::setvar setTransferDestination(astChanID, "xferDestination", xferDestination.get_string());
    m_connMutex.lock();
    setTransferDestination(m_connection);
    m_connMutex.unlock();

    astxx::manager::action::redirect redirect(astChanID, "iic-callflow", "newserver", 1);
    m_connMutex.lock();
    redirect(m_connection);
    m_connMutex.unlock();

    return AST_SUCCESS;
}

IString AsteriskServer::AppConfGetConferenceList(void)
{
    IString result;
    IString rtn_string;
    IString command_string = "conference list";
    const char *list;

    result = sendCommand(command_string);

    // Discard the first line
    if (result.find("Name") != -1)
    {
        list = result.get_string();
        list = strchr(list, '\n');
        list++;
        rtn_string = list;
    }
    else
        rtn_string = "";

    return rtn_string;
}

IString AsteriskServer::AppConfGetParticipantList(const char *astChanID)
{
    IString result;
    IString command_string = "conference list ";
    command_string.append(astChanID);
    result = sendCommand(command_string);
    char chanID[128];
    IString rtn_string;

    const char *line = result.get_string();

    line = strstr(line, "Channel:");

    while (line != NULL)
    {
        line += strlen("Channel: ");
        sscanf(line, "%s", chanID);
        rtn_string.append(chanID);
        rtn_string.append("\n");
        line = strstr(line, "Channel:");
    }

    return rtn_string;
}

int AsteriskServer::Hangup(const char *astChanID)
{
    astxx::manager::action::hangup hangup(astChanID);
    m_connMutex.lock();
    hangup(m_connection);
    m_connMutex.unlock();
    return AST_SUCCESS;
}

IString AsteriskServer::GetChannelVariable(const char *astChanID, const char *variableName)
{
    astxx::manager::action::getvar getvar(astChanID, variableName);
    m_connMutex.lock();
    getvar(m_connection);
    m_connMutex.unlock();

    IString rtn_string = getvar.value().c_str();
    return rtn_string;
}

int AsteriskServer::SetChannelVariable(const char *astChanID, const char *key, const char *value)
{
    astxx::manager::action::setvar setVariable(astChanID, key, value);
    m_connMutex.lock();
    setVariable(m_connection);
    m_connMutex.unlock();

    return AST_SUCCESS;
}

void AsteriskServer::astxxEventHandler(astxx::manager::message::event e)
{
    std::string logString = "==== ASTERISK EVENT ";
    logString += m_ipAddress.get_string();
    logString += " ====\n";
    logString += e.format();
    ServerDebug("%s", logString.c_str());

    if (e["Event"] == "Newchannel")
    {
        std::string chanId = e["Channel"].c_str();

        astxx::manager::action::getvar getvar(chanId.c_str(), "iicID");
        m_connMutex.lock();

        // Asterisk generates a Newchannel event even in cases where an outbound call
        // generated by an Originate fails because the number doesn't exist.  If that
        // is the case, the following getvar() will trip an exception in astxx which we will
        // catch (and ignore) right here

        try
        {
            getvar(m_connection);
            m_connMutex.unlock();
            m_eventHandler->onAsteriskNewchannel((void *)this, e["Channel"].c_str(), getvar.value().c_str());
        }
        catch (const std::exception& e)
        {
            m_connMutex.unlock();
        }

    }
    else if (e["Event"] == "Newstate")
    {
        m_eventHandler->onAsteriskNewstate((void *)this, e["Channel"].c_str(), atoi(e["ChannelState"].c_str()));
    }
    else if (e["Event"] == "Hangup")
    {
        if (e["Cause"] != "1")  // If this hangup is due to trying to dial a bad number, we don't want to report it
            m_eventHandler->onAsteriskHangup((void *)this, e["Channel"].c_str());
    }
    else if (e["Event"] == "ConferenceJoin")
    {
        m_numParticipants++;
        m_eventHandler->onAsteriskConferenceJoin((void *)this, e["Channel"].c_str(), e["ConferenceName"].c_str());
    }
    else if (e["Event"] == "ConferenceDTMF")
    {
        m_eventHandler->onAsteriskConferenceDTMF((void *)this, e["Channel"].c_str(), e["Key"].data()[0]);
    }
    else if (e["Event"] == "ConferenceLeave")
    {
        m_numParticipants--;
        m_eventHandler->onAsteriskConferenceLeave((void *)this, e["Channel"].c_str(), e["ConferenceName"].c_str());
    }
    else if (e["Event"] == "ConferenceState")
    {
        int state = (e["State"] == "speaking" ? 1 : 0);
        m_eventHandler->onAsteriskConferenceState((void *)this, e["Channel"].c_str(), state);
    }
    else if (e["Event"] == "ConferenceSoundComplete")
    {
        m_eventHandler->onAsteriskConferenceSoundComplete((void *)this, e["Channel"].c_str(), e["Sound"].c_str());
    }
    else if (e["Event"] == "UserEvent")
    {
        if (e["UserEvent"] == "ConferencePin")
        {
            m_eventHandler->onAsteriskConferencePin((void *)this, e["Channel"].c_str(), e["Pin"].c_str());
        }
        if (e["UserEvent"] == "OutgoingAccept")
        {
            m_eventHandler->onAsteriskOutgoingAccept((void *)this, e["Channel"].c_str(), e["IICID"].c_str());
        }
        if (e["UserEvent"] == "ServerTransfer")
        {
            m_eventHandler->onAsteriskServerTransfer((void *)this, e["Channel"].c_str(), e["Pin"].c_str());
        }
    }
}

IString AsteriskServer::sendCommand(const char *cmdString)
{
    astxx::manager::action::command command(cmdString);
    m_connMutex.lock();
    command(m_connection);
    m_connMutex.unlock();

    return command.result().c_str();
}


IString generateNameRecordFilename(const char *chanID)
{
    char temp[256];
    char *index = temp;
    strncpy(temp, chanID, 255);

    while (*index != 0)
    {
        if (*index == '/')
            *index = '_';
        index++;
    }

    return (IString("/var/lib/asterisk/sounds/roster/") + IString(temp));
}
