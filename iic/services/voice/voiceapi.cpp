/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef _WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include "voiceapi.h"
#include "../../jcomponent/util/log.h"

//
// DigitBuffer
//

DigitBuffer::DigitBuffer()
{
    last_time = 0;
}

void DigitBuffer::clear()
{
    buffer.clear();
}

void DigitBuffer::add_digit(char digit)
{
    buffer.append(digit);
    last_time = time(NULL);
}

// Get digits, also erasing from buffer.
bool DigitBuffer::get_digits(char * out, int max)
{
	strncpy(out, buffer, max - 1);
	out[max-1] = '\0';
	
	buffer.erase(0, strlen(out));
	return (buffer.length() > 0);
}

//
// Conference
//

IConference::IConference(IProvider * _provider, int _id, bool _main, const char *_recording_id)
        : provider(_provider), conf_id(_id), main(_main), recording_id(_recording_id)
{
    announce_entry_exit = true;
}

IConference::~IConference()
{

}

//
// Channel
//

IChannel::IChannel(IProvider * _provider, int _id)
    : timer(this, _id)
{
    channel_id = _id;
    provider = _provider;
    current_call = NULL;
    user = NULL;
    conference = NULL;
}

IChannel::~IChannel()
{
}

void IChannel::on_timeout(int timer_type)
{
	if (timer_type == TimerApp)
	{
		provider->get_application()->on_timeout(this);
	}
}

void IChannel::set_timeout(int timer_type, int ms)
{
    timer.set_timeout(timer_type, ms);
}

void IChannel::cancel()
{
    timer.cancel();
}

ICall * IChannel::get_current_call()
{
    return current_call;
}

void IChannel::set_current_call(ICall * call)
{
    current_call = call;
}

void IChannel::set_user(void * data)
{
    user = data;
}

void *IChannel::get_user()
{
    return user;
}

void IChannel::set_conference(IConference * _conference)
{
	conference = _conference;
}

IConference * IChannel::get_conference()
{
	return conference;
}

MenuPage Menu::s_participant_menu_pages[] =
{
    {
        "", // no toplevel prompt, just the menu entry tone
        "MenuParticipantHelp0.wav",
        "MenuTimeout.wav",
        //false,
        //-1,
        {
            // Defined actions for digits 0-9,*,#
            /* 0      */ { MenuActionHelp,             MenuActionInvalidSelection, -1},
            /* 1      */ { MenuActionInvalidSelection, MenuActionInvalidSelection, -1},
            /* 2 ABC  */ { MenuActionInvalidSelection, MenuActionInvalidSelection, -1},
            /* 3 DEF  */ { MenuActionInvalidSelection, MenuActionInvalidSelection, -1},
            /* 4 GHI  */ { MenuActionInvalidSelection, MenuActionInvalidSelection, -1},
            /* 5 JKL  */ { MenuActionInvalidSelection, MenuActionInvalidSelection, -1},
            /* 6 MNO  */ { MenuActionMuteOn,           MenuActionInvalidSelection, -1},
            /* 7 PQRS */ { MenuActionMuteOff,          MenuActionInvalidSelection, -1},
            /* 8 TUV  */ { MenuActionToggleVolume,     MenuActionInvalidSelection, -1},
            /* 9 WXYZ */ { MenuActionInvalidSelection, MenuActionInvalidSelection, -1},
            /* '*'    */ { MenuActionExit,             MenuActionInvalidSelection, -1},
            /* '#'    */ { MenuActionSubmenu,          MenuActionInvalidSelection, 1},
        },
    },
    {
        "", // no toplevel prompt, just the menu entry tone
        "MenuParticipantHelp1.wav",
        "MenuTimeout.wav",
        //false,
        //-1,
        {
            // Defined actions for digits 0-9,*,#
            /* 0      */ { MenuActionHelp,             MenuActionInvalidSelection, -1},
            /* 1      */ { MenuActionInvalidSelection, MenuActionInvalidSelection, -1},
            /* 2 ABC  */ { MenuActionInvalidSelection, MenuActionInvalidSelection, -1},
            /* 3 DEF  */ { MenuActionInvalidSelection, MenuActionInvalidSelection, -1},
            /* 4 GHI  */ { MenuActionToggleRaiseHand,  MenuActionInvalidSelection, -1},
            /* 5 JKL  */ { MenuActionInvalidSelection, MenuActionInvalidSelection, -1},
            /* 6 MNO  */ { MenuActionInvalidSelection, MenuActionInvalidSelection, -1},
            /* 7 PQRS */ { MenuActionInvalidSelection, MenuActionInvalidSelection, -1},
            /* 8 TUV  */ { MenuActionInvalidSelection, MenuActionInvalidSelection, -1},
            /* 9 WXYZ */ { MenuActionInvalidSelection, MenuActionInvalidSelection, -1},
            /* '*'    */ { MenuActionExit,             MenuActionInvalidSelection, -1},
            /* '#'    */ { MenuActionInvalidSelection, MenuActionInvalidSelection, -1},
        },
    },
};

MenuPage Menu::s_moderator_menu_pages[] =
{
    {
        "", // no toplevel prompt, just the menu entry tone
        "MenuModeratorHelp0.wav",
        "MenuTimeout.wav",
        //false,
        //-1,
        { 
            // Defined actions for digits 0-9,*,#
            /* 0      */ { MenuActionHelp,               MenuActionInvalidSelection,-1},
            /* 1      */ { MenuActionDialNumber,         MenuActionSubmenu,          2},
            /* 2 ABC  */ { MenuActionToggleRecordMeeting,MenuActionInvalidSelection,-1},
            /* 3 DEF  */ { MenuActionToggleMuteJoinLeave,MenuActionInvalidSelection,-1},
            /* 4 GHI  */ { MenuActionLockMeeting,        MenuActionInvalidSelection,-1},
            /* 5 JKL  */ { MenuActionUnlockMeeting,      MenuActionInvalidSelection,-1},
            /* 6 MNO  */ { MenuActionMuteOn,             MenuActionInvalidSelection,-1},
            /* 7 PQRS */ { MenuActionMuteOff,            MenuActionInvalidSelection,-1},
            /* 8 TUV  */ { MenuActionToggleVolume,       MenuActionInvalidSelection,-1},
            /* 9 WXYZ */ { MenuActionRollCall,		     MenuActionInvalidSelection,-1},
            /* '*'    */ { MenuActionExit,               MenuActionInvalidSelection,-1},
            /* '#'    */ { MenuActionSubmenu,            MenuActionInvalidSelection, 1},
        },
    },
    {
        "MenuModeratorEntry1.wav",
        "MenuModeratorHelp1.wav",
        "MenuTimeout.wav",
        //false,
        //-1,
        { 
            /* 0      */ { MenuActionHelp,             MenuActionInvalidSelection,-1},
            /* 1      */ { MenuActionInvalidSelection, MenuActionInvalidSelection,-1},
            /* 2 ABC  */ { MenuActionInvalidSelection, MenuActionInvalidSelection,-1},
            /* 3 DEF  */ { MenuActionInvalidSelection, MenuActionInvalidSelection,-1},
            /* 4 GHI  */ { MenuActionToggleRaiseHand,  MenuActionInvalidSelection,-1},
            /* 5 JKL  */ { MenuActionToggleLectureMode,MenuActionInvalidSelection,-1},
            /* 6 MNO  */ { MenuActionInvalidSelection, MenuActionInvalidSelection,-1},
            /* 7 PQRS */ { MenuActionInvalidSelection, MenuActionInvalidSelection,-1},
            /* 8 TUV  */ { MenuActionEndMeeting,       MenuActionInvalidSelection,-1},
            /* 9 WXYZ */ { MenuActionInvalidSelection, MenuActionInvalidSelection,-1},
            /* '*'    */ { MenuActionExit,             MenuActionInvalidSelection,-1},
            /* '#'    */ { MenuActionInvalidSelection, MenuActionInvalidSelection,-1},
        },
    },
    {
        "MenuModeratorEntry2.wav",
        "MenuModeratorHelp2.wav",
        "MenuTimeout.wav",
        //false,
        //-1,
        {
            /* 0      */ { MenuActionHelp,                  MenuActionInvalidSelection,-1},
            /* 1      */ { MenuActionEndSubMeetingJoinMain, MenuActionInvalidSelection,-1},
            /* 2 ABC  */ { MenuActionEndSubMeetingEndCalls, MenuActionInvalidSelection,-1},
            /* 3 DEF  */ { MenuActionInvalidSelection,      MenuActionInvalidSelection,-1},
            /* 4 GHI  */ { MenuActionInvalidSelection,      MenuActionInvalidSelection,-1},
            /* 5 JKL  */ { MenuActionInvalidSelection,      MenuActionInvalidSelection,-1},
            /* 6 MNO  */ { MenuActionInvalidSelection,      MenuActionInvalidSelection,-1},
            /* 7 PQRS */ { MenuActionInvalidSelection,      MenuActionInvalidSelection,-1},
            /* 8 TUV  */ { MenuActionInvalidSelection,      MenuActionInvalidSelection,-1},
            /* 9 WXYZ */ { MenuActionInvalidSelection,      MenuActionInvalidSelection,-1},
            /* '*'    */ { MenuActionExit,                  MenuActionInvalidSelection,-1},
            /* '#'    */ { MenuActionExit,                  MenuActionInvalidSelection,-1},
        },
    },
};

//
// Call
//

ICall::ICall(IChannel * chan, int id)
{
    call_id = id;
    channel = chan;
    disconnected = false;
    m_options = 0;
}

ICall::~ICall()
{
}

IChannel * ICall::get_channel()
{
    return channel;
}

void
IChannel::grant_moderator(int grant)
{
    is_moderator = (grant != 0);
    select_menu();

    log_debug(ZONE, "%s moderator privileges",
              (is_moderator ? "Allowing" : "Denying"));
}
bool ICall::is_connected()
{
    return !disconnected;
}


//
// Provider
//

IProvider::IProvider(IApplication & app)
{
    handler = &app;
}

IString IProvider::s_prompt_dir;
IString IProvider::s_recording_dir;

//
// Channel menu
//
const char *
Menu::get_timeout_prompt()
{
    if (m_current_page != NULL)
    {
        return m_current_page->timeout_prompt;
    }
    else
        return "";
}

const char *
Menu::get_entry_prompt()
{
    if (m_current_page != NULL)
    {
        return m_current_page->entry_prompt;
    }
    else
        return "";
}

    
const char *
Menu::get_help_prompt()
{
    if (m_current_page != NULL)
    {
        return m_current_page->help_prompt;
    }
    else
        return "";
}

int
Menu::on_digit(int digit)
{
    if (digit != '\0')
        // they entered something
        m_tmo = 0;
    
    if (digit == '*' && !m_active)
    {
        // leading stars
        
        ++m_star;
        if (m_star == 1)
        {
            return MenuActionArmed;
        }
        else if (m_star == 2)
        {
            m_star = 0;  // until a valid action is selected, m_star < 0
            m_active = true;
            m_current_page = m_root_page;
            
            log_debug(ZONE, "Menu active");

            return MenuActionNone;
        }
    }

    int index;
    if (digit >= '0' && digit <= '9')
    {
        index = digit - '0';
    }
    else if (digit == '*')
    {
        index = 10;
    }
    else if (digit == '#')
    {
        index = 11;
    }
    else
    {
        log_error(ZONE, "Unhandled menu option seen %d", digit);
        
        m_action = MenuActionNone;
        index = -1;
    }
    
    if (index >= 0)
    {
        if (m_alt_selected && index == m_for_option)
        {
            m_action = m_current_page->m_action[index].alt_action;
            m_alt_selected = false;  // i.e. one-shot
            m_for_option = -1; // don't match anything
        }
        else
        {
            m_action = m_current_page->m_action[index].next_action;
        }
    }
    
    if (m_action == MenuActionSubmenu)
    {
        m_current_page = &m_root_page[m_current_page->m_action[index].next_menu];

        // we're moving off the current page, so clear any alt action stuff
        // before we go.
        m_alt_selected = false;
        m_for_option = -1;
    }

    switch (m_action)
    {
        case MenuActionNone:
        case MenuActionHelp:
        case MenuActionSubmenu:
        case MenuActionInvalidSelection:
            // keep active true after these types
            return m_action;

        default:
            // Took an action that takes us out of menu mode;
            // mark menu as inactive
            m_active = false;
            log_debug(ZONE, "Deactivating menu");
            return m_action;
    }
    
    // will never get here
    return m_action;
}
