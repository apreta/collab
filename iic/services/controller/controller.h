/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef CONTROLLER_HEADER
#define CONTROLLER_HEADER

#include <time.h>

#include "../../util/irefobject.h"
#include "../../util/istring.h"
#include "../../util/ithread.h"
#include "../common/common.h"
#include "../common/address.h"
#include "events.h"
#include <string.h>

class Service;
class User;
class Port;
class PhoneNumber;
class Meeting;
class SubMeeting;
class Participant;
struct Event;
class RPCResultSet;
class DispatchCall;

typedef IRefObjectPtr<Service> spService;
typedef IRefObjectPtr<User> spUser;
typedef IRefObjectPtr<Port> spPort;
typedef IRefObjectPtr<Meeting> spMeeting;
typedef IRefObjectPtr<SubMeeting> spSubMeeting;
typedef IRefObjectPtr<Participant> spParticipant;

typedef struct session_st * session_t;

#define RPCSESSION void*

#define MAX_ID_LENGTH 64
#define MAX_PARTICIPANTS 512


// Master conference controller.
class Controller
{
    IMutex controller_lock;

    IHash users;
    IList services;
    IHash meetings;
	IHash participants;
	IHash submeetings;

	IHash user_addresses;

	IString server_name;
	IString webstart_url;

	IHash voice_bridge_map;

	IString voice_provider;
    int pcphone_type;

	static int anon_id_seq;
    static int mailer_id_seq;

    bool multi_phone;

    // For license logging
    IString license_company;      // String used to identify a company
    IString license_alert_email;  // Address for email if license is exceeded
    int license_max;              // Number of licenses
    time_t time_license_email;    // Last time an license email was sent

public:
    Controller();

	// Service management
    int register_service(const char * serviceid, int epoch);
    int unregister_service(const char * serviceid);
	void reset_services(int epoch);
    	// returns false if idx is beyond the end of list
    bool is_voice_available();
    bool get_service_status(int idx, IString * service_id, int * type, int * available);
    bool get_appshare_status(int idx, IString * service_id, int * available);
    bool get_docshare_status(int idx, IString * service_id, int * available);
	void reset(bool reset_db);

    // Add/remove port for user
    int register_client(const char * userid, const char * username, const char * name, const char * port_address, const char *network_address, spUser & user);
    int unregister_client(const char * sessionid, const char * port_address, bool is_disconnect = false);
	void on_presence(const char * port_address, int avail, const char * status);

	int create_meeting(const char * sessionid, const char *id, int type, int privacy, int priority, const char *title,
                       const char * descrip, int expected_size, int expected_duration, const char *password, int options,
                       int end_time, int invite_expiration, int invite_valid_before, const char * invite_message, int display_attendees,
                       int invitee_chat_options, IList * participants, const char * key);

    int toggle_lecture_mode(const char * sessionid, const char *id, int * mode_on);
    int toggle_hold_mode(const char * sessionid, const char *id, int * mode_on);
    int toggle_record_meeting(const char * sessionid, const char *id, int * mode_on);
    int toggle_mute_join_leave(const char * sessionid, const char *id, int * mode_on);
    int enable_data_recording(const char * sessionid, const char *id,  int  enabled);
    int lock_meeting(const char * sessionid, const char * id, int state);
    int modify_meeting(const char * sessionid, const char *id, int type, int privacy, int priority,
                       const char *title, const char * descrip, const char *password, int options,
                       int invite_expiration, int invite_valid_before, const char * invite_message, int display_attendees,
                       int invitee_chat_options);
	int modify_recording(const char * sessionid, const char *id, int cmd, const char* email);

    // Start/end meeting
    int start_meeting(const char * sessionid, const char * meeting_id, const char * key=NULL);
    int end_meeting(const char * sessionid, const char * meeting_id);

    // Join/leave meeting.
    int join_meeting(const char *sessionid, const char *addr, const char *meeting_id, const char *pin, const char *password);
    int leave_meeting(const char *sessionid, const char *addr, const char *meeting_id);

    int reload_meeting(const char *sessionid, const char *meeting_id);

    int get_active_meetings(const char * sessionid, IVector * result);

	int get_meeting_status(const char * sessionid, const char * id, int options, int &state, int &m_opt, int &sm_opt);

    int is_invite_valid(bool is_anonymous, const char * meeting_id, int options,
                        int scheduled_time, int invite_valid_before,
                        int invite_expiration, int type, int invited_time);

	// Add/remove participant from meeting
    // Adding participant enables access, but does not directly connect user to meeting.
    int add_participants(const char * sessionid, const char * meetingid, IList * participants);
    int add_participant(const char * sessionid, const char * meetingid, const char * sub_id,
				const char * user_id, const char * username, int roll,
				const char * name, int sel_phone, const char * phone,
				int sel_email, const char * email, const char * screen, const char * description,
				int notify_type, const char * message, int call_type, int expires);
    int remove_participant(const char * sessionid, const char * meetingid, const char * participant_id);
    int call_participant(const char * sessionid, const char * meetingid, const char * participant_id,
						int sel_call_phone, const char * call_phone, int options);
	int notify_participant(const char * sessionid, const char * meetingid, const char * participant_id,
						int notify_type, const char * message);
	int disconnect_participant(const char * sessionid, const char * meetingid, const char * participant_id, int media_type);

	// Outbound calls
	int call_progress(const char * sessionid, const char * meeting_id, const char * call_id, int progress);

    // Manage submeetings
    int add_submeeting(const char * sessionid, const char * meetingid, int expected_participants, spSubMeeting & sm);
    int remove_submeeting(const char * sessionid, const char * meetingid, const char * sub_id, bool remove_parts);
    int move_participant(const char * sessionid, const char * meetingid, const char * participant_id, const char * sub_id);

	int check_participant(const char * sessionid, const char * meetingid, const char * participant_id, spParticipant & p, bool & moderator, bool & self);
    int set_participant_volume(const char * sessionid, const char * meetingid, const char * participant_id, RPCSESSION rpcsess,
                               int volume, int gain, int mute);
    int get_participant_volume(const char * sessionid, const char * meetingid, const char * participant_id, RPCSESSION rpcsess);

	int find_pin(const char * pin, spMeeting & meeting, spParticipant & participant);

	// App share sessions
	int start_app_share(const char * sessionid, const char * meetingid, const char * address, const char * password,
		int share_options, int share_starttime);
	int end_app_share(const char * sessionid, const char * meetingid);
	int request_share_server(const char * sessionid, const char * meetingid);
	int change_presenter(const char * sessionid, const char * meetingid, const char * partid);
	int grant_control(const char * sessionid, const char * meetingid, const char * partid, int grant);

	int set_handup(const char * sessionid, const char * meetingid, const char * partid, int & handup);
	int grant_moderator(const char * sessionid, const char * meetingid, const char * partid, int grant);
	int roll_call(const char * sessionid, const char * meetingid);
	int place_call(const char * sessionid, const char * meetingid, const char * calleeid, const char * dial_number);

	// document sharing
	int start_doc_share(const char * sessionid, const char * meetingid, const char * address, int share_starttime);
	int end_doc_share(const char * sessionid, const char * meetingid);
	int download_document_page(const char * sessionid, const char * meetingid, const char * docid, const char * pageid);
	int configure_whiteboard(const char * sessionid, const char * meetingid, int options);
	int draw_on_whiteboard(const char * sessionid, const char * meetingid, const char * drawing);

	int on_ppt_remote_control(const char * sessionid, const char * meetingid, const char * action);

	// Mtg Archivers
	void add_mtg_archiver(const char * service);

	// Voice bridges
	void add_bridge(const char * service, const char * voip_address);
	void add_bridge_phone(const char * phone, const char * service);
	int  find_bridge_by_phone(const char * phone, IString & service);
    void set_multi_phone(bool state) { multi_phone = state; }

    // Send custom request to voice bridge
    int send_bridge_request(const char * sessionid, const char * meetingid, const char * partid, int code, const char * msg);
    int bridge_request_progress(const char * sessionid, const char * meeting_id, const char * call_id, int code, const char * msg);

    // In-Meeting Chat
    int send_chat_message(const char * sessionid,
                          const char * meetingid,
                          int send_to,
                          const char * message,
                          const char * from_participantid,
                          const char * to_participantid);

	// IM
	int send_im(const char * sessionid, const char * fromuser, const char * touser, const char * message);

	// Track all participants & submeetings
	void register_participant(Participant * p);
	void unregister_participant(Participant * p);
	void register_submeeting(SubMeeting * sm);
	void unregister_submeeting(SubMeeting * sm);
	void unregister_meeting(Meeting * m);

    // Resolve ids
    int get_meetings(IVector * vec);
    int get_users(IVector * vec);
    spUser find_user(const char * sessionid);
    spService find_service(const char * serviceid);
    spMeeting find_meeting(const char * meetingid);
	spParticipant find_participant(const char * participantid);
	spSubMeeting find_submeeting(const char * subid);
	spUser find_user_by_address(const char * address);
	int find_client_address(const char * userid, IString &addr);
	int find_participant_address(const char * particpantid, int media_type, IString &addr);

    spUser create_user(const char * userid, const char * username = NULL, const char *name = NULL);

	void set_server_name(const char * name)	{ server_name = name; }
	const char* get_server_name()			{ return server_name; }

	spService get_archiver();
	void release_archiver(spService & arch);

	const char* get_doc_share_server(const char* meetingid);

	void set_webstart_url(const char * url)	{ webstart_url = url; }
	const char* get_webstart_url()			{ return webstart_url; }

	void set_voice_provider(const char * name)	{ voice_provider = name; }
	const char* get_voice_provider()			{ return voice_provider; }
	void set_pcphone_type(int type)        		{ pcphone_type = type; }
	int get_pcphone_type()                    	{ return pcphone_type; }

	bool check_id(const char * id);

    int get_next_mailer_id()                { return ++mailer_id_seq; }

	// Internal functions
	int start_and_join(spUser &actor, spPort &port, const char * meeting_id,
		const char * part_id, const char * password, bool release, const char * key=NULL);
	void on_join_failed(spUser &actor, spPort &port, int status, const char * meeting_id);
	void reset_users(IUseMutex & lock, spService & service);
	void reset_meetings(IUseMutex & lock, spService & service);

    // metrics
    void initialize();
    int  set_location(const char *directory);	/* base directory for metrics files */
    void set_rotation_time(int minutes_since_midnight /* GMT */);
    void set_rotation_interval(int days);
    void started(bool started);
    void sample(int time);
    void rotate();
    int  get_samples(int type, IListIterator & iter);

    // License
    int  log_license(int num_licenses);
    void set_license_company(const char * name) { license_company = name; }
    void set_license_email(const char * email) { license_alert_email = email; }
    void set_license_max(int max) { license_max = max; }

	// recording
	int number_meetings() { return meetings.size(); }

};


// Registered service.
class Service : public IReferencableObject
{
    IString service_id;
    int epoch;
	bool available;
	Address address;
    IString external_address;   // External address for service, e.g. voip

	IMutex ref_lock;

	int usage_count;

public:
    Service(const char * service_id, bool available);

    void lock();
    void unlock();

    const char * get_service_id()  	{ return service_id; }
    int get_epoch()                	{ return epoch; }
	void set_epoch(int _epoch)	   	{ epoch = _epoch; }
	int get_type();
	const char * get_name();
	void set_available(bool f)		{ available = f; }
	bool is_available()				{ return available; }
    const char * get_ext_address()  { return external_address; }
    void set_ext_address(const char *addr)
                                    { external_address = addr; }

	int add_usage_count();
	int remove_usage_count();
	int get_usage_count()			{ return usage_count; }

    enum { TYPE_SERVICE = 3461 };
    virtual int type()
        { return TYPE_SERVICE; }
    virtual unsigned int hash()
        { return service_id.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && service_id == ((Service*)obj)->service_id; }
};


// A logged in user (aka session).
// A particular user instance may have one voice, one data, and one app share port.
// If a user connects again, he must connect using an anonymous id (thereby creating
// a new user instance).
class User : public IReferencableObject
{
    IString user_id;
    IString name;		// display name

	IString username;	// IIC internal username

	IString community_id;
	bool admin;
	bool superadmin;

	// Current phone & email addresses
	int def_phone;
	IVector phones;

	int def_email;
	IVector emails;

	IMutex ref_lock;
    IMutex user_lock;

    // Enabled features for this user (from the user's profile)
    int enabledfeatures;

	// List of user connections
    IList ports;

	// Presence session handle (for voice connections)
	session_t session;

	// Queue call for completion after user info loaded
	DispatchCall * dispatch_call;

    time_t last_ping_time;
    time_t reverse_ping_time;

public:
    User(const char * user_id, const char * username = NULL, const char * name = NULL);

    void lock();
    void unlock();

	IMutex & get_lock()			{ return user_lock; }

	void load(DispatchCall * disp);
	void on_load_result(RPCResultSet * rs);
	void on_load_failed();

	void reset_user(Service * service);

	int connect_port(Participant * participant, Port * port, bool waiting=false);
	int disconnect_port(Participant * participant, Port * port, bool notify_service=true, bool notify_presence=true);
	int move_connections(Meeting * meeting, Participant * part, SubMeeting * curr_sm, SubMeeting * new_sm);

	void send_presence();

    // ping support
    void set_last_ping_time(time_t time)    { last_ping_time = time; }
    time_t get_last_ping_time()             { return last_ping_time; }
    void set_reverse_ping_time(time_t time) { reverse_ping_time = time; }
    time_t get_reverse_ping_time()          { return reverse_ping_time; }

    void send_ping(Port * port, const char * id, int status);
	void send_error(Port * port, const char * id, int status, const char * message, const char * meetingid, const char * partid);
	void send_error(const char * id, int status, const char * message, const char * meetingid, const char * partid);

    int add_port(const char * port, const char * network_address);
    void remove_port(const char * port, bool is_disconnect = false);

	int number_ports()						{ return ports.size(); }

	spPort get_port(Meeting * meeting, int media_type);
	spPort get_port(const char *address);
	spPort get_client();
	spPort get_voice();

	void set_phone(int type, const char * phone, bool direct);
	PhoneNumber* get_phone(int type);
	int resolve_phone(int selected, const char * phone_in, char * phone_out, char * extension_out, bool & direct_out);

	void set_default_phone(int type)		{ def_phone = type; }
	int get_default_phone()					{ return def_phone; }

	void set_email(int type, const char * email);
	const char * get_email(int type);
        const char * find_email_address();

	void set_default_email(int type)		{ def_email = type; }
	int get_default_email()					{ return def_email; }

    const char * get_id()       			{ return user_id; }

    const char * get_name() 				{ return name; }
	void set_name(const char * _name)		{ name = _name; }

	const char * get_username()				{ return username; }
	void set_username(const char * name)	{ username = name; }

    bool can_create_meeting()  				{ return true; }
    bool is_in_meeting(Meeting * meeting, int media_type = MediaAny);
	bool is_connected(int media_type);
	bool is_user();
	bool is_client_jid(const char *username = NULL);

	void set_community_id(const char *_id)	{ community_id = _id; }
	const char * get_community_id()			{ return community_id; }

	void set_admin(bool _admin)				{ admin = _admin; }
	bool is_admin()							{ return admin; }

	void set_superadmin(bool _admin)		{ superadmin = _admin; }
	bool is_superadmin()					{ return superadmin; }

    void set_enabledfeatures(int _enabledfeatures)  { enabledfeatures = _enabledfeatures; }
    int  get_enabledfeatures()                      { return enabledfeatures; }

    enum { TYPE_USER = 3460 };
    virtual int type()
        { return TYPE_USER; }
    virtual unsigned int hash()
        { return user_id.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && user_id == ((User*)obj)->user_id; }
};


// Users connect through ports (voice, data, etc.).
class Port : public IReferencableObject
{
	IString port_id;    	// address
    IString network_address;	// e.g. dialed telephone number or caller ID #, hostname of client
	int port_type;
	bool connected;

    spUser owner;

	// If port has been associated with a meeting
	spParticipant participant;

	IMutex ref_lock;

public:
    Port(const char * port_id, const char * network_address, int port_type, User * owner);
	~Port();

    void lock();
    void unlock();

	spParticipant get_participant();
	void set_participant(Participant * p);

	bool is_connected()			{ return connected; }
	void set_connected(bool f)	{ connected = f; }

	User * get_owner()			{ return owner; }
    const char * get_id()       { return port_id; }
	int get_port_type()			{ return port_type; }
    const char * get_network_address() { return network_address; }

	bool is_in_meeting(Meeting * m);
    bool is_outbound_call() {
        return (port_type & MediaVoice) &&
            (strstr((const char *)port_id, "voice:out-") != NULL);
    }

    enum { TYPE_PORT = 3462 };
    virtual int type()
        { return TYPE_PORT; }
    virtual unsigned int hash()
        { return port_id.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && port_id == ((Port*)obj)->port_id; }
};


extern Controller master;

#endif

