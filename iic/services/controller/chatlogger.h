/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include "../../util/iobject.h"
#include "../../util/ithread.h"
#include "../../jcomponent/util/log.h"
#include <glib.h>

class ChatLogger
{
private:
    IThreadPool * m_pool;

    static void output_msg(void * data, void * init_data)
	{
	    log_debug(ZONE, "log chat %s", (char *)data);

	    FILE *fp = fopen("/var/iic/chatlog/mtgchat.log", "a");
	    if (fp != NULL)
	    {
		fprintf(fp, "%s\n", (char *)data);
		fclose(fp);
	    }
	    else
	    {
		log_error(ZONE, "Couldn't open mtgchat.log: %s",
			  strerror(errno));
	    }
	    free(data);
	}

public:
    ChatLogger()
	{
	    if (!g_thread_supported()) g_thread_init(NULL);
	    m_pool = new IThreadPool(&output_msg, NULL, 1);
	};
    ~ChatLogger() { delete m_pool; };

    void log_chat(const char *from_id,
		  const char *from_name,
		  int send_to,
		  const char *to_id,
		  const char *to_name,
		  const char *message) {
	IString msg;
	char now_gmt_str[32];
	time_t now = time(NULL);
	struct tm now_gmt;
	gmtime_r(&now, &now_gmt);
	strftime(now_gmt_str, sizeof now_gmt_str, "%FT%T", &now_gmt);
	msg.append("\"");
	msg.append(now_gmt_str);
	msg.append("\",\"");
	msg.append(from_id);
	msg.append("\",\"");
	msg.append(from_name);
	msg.append("\",\"");
        switch (send_to)
	{
	case ToOneParticipant:
	    msg.append("ToOne");
	    break;
	    
	case ToAllParticipants:
	    msg.append("ToAll");
	    break;
	    
	case ToNonModeratorParticipants:
	    msg.append("ToNonModerators");
	    break;

	case ToModeratorParticipants:
	    msg.append("ToModerators");
	    break;

	case ToHostParticipant:
	    msg.append("ToHost");
	    break;
	}
	msg.append("\",\"");
	msg.append(to_id);
	msg.append("\",\"");
	msg.append(to_name);
	msg.append("\",\"");
	IString justmsg = message;
	const char * esc_message = justmsg.get_string_escaped_string();
	msg.append(esc_message);
	g_free((gchar *)esc_message);
	msg.append("\"");

	m_pool->push(strdup(msg.get_string()));
    };
};
