/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include <stdio.h>
#include <stdlib.h>

#include "phonenumber.h"
#include "../common/common.h"

#include <util.h>
#include "../../jcomponent/util/nadutils.h"
#include "../../jcomponent/util/log.h"


int PhoneNumber::country;
int PhoneNumber::dialing_mode;
bool PhoneNumber::default_exchange;
bool PhoneNumber::default_area_code;
IString PhoneNumber::area_code;
IString PhoneNumber::external_prefix;
IString PhoneNumber::toll_prefix;
IString PhoneNumber::i18n_prefix;
IString PhoneNumber::countrycode;
IHash PhoneNumber::exchanges;
IHash PhoneNumber::area_codes;

/* Sample config

 <dialing>
 	<areaCode>781</areaCode>
 	<tollPrefix>1</tollPrefix>
	<externalPrefix>9</externalPrefix>
	<i18nPrefix>01</i18nPrefix>
	<country>US</country>
	<countrycode>1</countrycode>
	<dialingMode>normal|dialAreaCodeOnLD|dialAreaCodeAlways</dialingMode>
	<exchanges default="ld|local">
		<local>333</local>
		<ld>444</ld>
	</exchanges>
	<areaCodes default="ld|local">
		<local>333</local>
		<ld>555</ld>
	</areaCodes>
 </dialing>

*/

NumberSegment::NumberSegment(const char * digits, bool is_ld)
	: segment(digits)
{
	ld = is_ld;
}

int PhoneNumber::initialize(const char * config_file)
{
	nad_t nad;

	if (nad_load((char *)config_file, &nad))
    {
        log_error(ZONE, "no dialing config loaded, aborting\n");
		return Failed;
    }

    /* Read host, user, etc. */
	int root = 0;

	area_code = get_child_value(nad, root, "areaCode");
	external_prefix = get_child_value(nad, root, "externalPrefix");
	toll_prefix = get_child_value(nad, root, "tollPrefix", "1");
	i18n_prefix = get_child_value(nad, root, "i18nPrefix", "01");
	IString dial_mode_str = get_child_value(nad, root, "dialingMode", "dial10Digits");
	IString country_str = get_child_value(nad, root, "country", "US");
	countrycode = get_child_value(nad, root, "countrycode", "1");
	log_status(ZONE, "Dialing - external: %s toll: %s country: %s mode: %s", external_prefix.get_string(),
               toll_prefix.get_string(), country_str.get_string(), dial_mode_str.get_string());

	if (country_str != "US")
		log_error(ZONE, "Invalid country specified in dialing configuration, defaulting to US rules\n");
	country = US;

	if (dial_mode_str == "normal")
		dialing_mode = NormalDialing;
	else if (dial_mode_str == "dialAreaCodeOnLD")
		dialing_mode = LDAreaCodeDialing;
	else if (dial_mode_str == "dialAreaCodeAlways")
		dialing_mode = TenDigitDialing;
	else
	{
		log_error(ZONE, "Invalid dialing mode specified in dialing configuration, defaulting to 10 digit dialing\n");
		dialing_mode = TenDigitDialing;
	}

	/* Read exchanges */
	root = nad_find_elem(nad, 0, "exchanges", 1);
	if (root > 0)
	{
		IString def_attr = get_attr_value(nad, root, "default", "ld");
		if (def_attr == "local")
			default_exchange = false;
		else
			default_exchange = true;

		int def = nad_find_first_child(nad, root);
		while (def > 0)
		{
			if (!strncmp(NAD_ENAME(nad, def), "local", NAD_ENAME_L(nad, def)))
			{
				IString * key = new IString(NAD_CDATA(nad, def), NAD_CDATA_L(nad, def));
				NumberSegment * temp = new NumberSegment(key->get_string(), false);
				exchanges.insert(key, temp);
				log_status(ZONE, "Dialing - local exchange %s", key->get_string());
			}
			else if (!strncmp(NAD_ENAME(nad, def), "ld", NAD_ENAME_L(nad, def)))
			{
				IString * key = new IString(NAD_CDATA(nad, def), NAD_CDATA_L(nad, def));
				NumberSegment * temp = new NumberSegment(key->get_string(), true);
				exchanges.insert(key, temp);
				log_status(ZONE, "Dialing - ld exchange %s", key->get_string());
			}
			else
			{
				log_error(ZONE, "Unexpected element in dialing configuration file\n");
				return Failed;
			}

			def = nad_find_next_sibling(nad, def);
		}
	}

	/* Read area codes */
	root = nad_find_elem(nad, 0, "areaCodes", 1);
	if (root > 0)
	{
		IString def_attr = get_attr_value(nad, root, "default", "ld");
		if (def_attr == "local")
			default_area_code = false;
		else
			default_area_code = true;

		int def = nad_find_first_child(nad, root);
		while (def > 0)
		{
			if (!strncmp(NAD_ENAME(nad, def), "local", NAD_ENAME_L(nad, def)))
			{
				IString * key = new IString(NAD_CDATA(nad, def), NAD_CDATA_L(nad, def));
				NumberSegment * temp = new NumberSegment(key->get_string(), false);
				area_codes.insert(key, temp);
				log_status(ZONE, "Dialing - local area code %s", key->get_string());
			}
			else if (!strncmp(NAD_ENAME(nad, def), "ld", NAD_ENAME_L(nad, def)))
			{
				IString * key = new IString(NAD_CDATA(nad, def), NAD_CDATA_L(nad, def));
				NumberSegment * temp = new NumberSegment(key->get_string(), true);
				area_codes.insert(key, temp);
				log_status(ZONE, "Dialing - ld area code %s", key->get_string());
			}
			else
			{
				log_error(ZONE, "Unexpected element in dialing configuration file\n");
				return Failed;
			}

			def = nad_find_next_sibling(nad, def);
		}
	}

	nad_free(nad);

	return Ok;
}

PhoneNumber::PhoneNumber(int _type, const char * _phone, bool _direct)
	: phone_type(_type), direct(_direct)
{
	i18n = false;
	has_country_code = false;
	normalized = false;
	long_distance = false;

	normalize(_phone);
}

PhoneNumber::PhoneNumber(const char * _phone, bool _normalize)
{
    direct=true;
    phone_type=SelPhoneDefault;
	i18n = false;
	has_country_code = false;
	normalized = false;
	long_distance = false;

    if (_normalize)
        normalize(_phone);
    else
        phone = _phone;
}

const char *PhoneNumber::digit_charset = "0123456789*#";
const char *PhoneNumber::control_charset = ",";
const char *PhoneNumber::extension_delimiter = "xX";

void PhoneNumber::normalize(const char * in_phone)
{
	char buffer[MAX_PHONE_LEN+1];
	const char *read;
	char *write;
	int size = 0;
	bool special = false;

    // Skip leading whitespace; if any
    for (read = in_phone; *read && isspace(*read); read++);

    if (strncasecmp(read, "sip:", 4) == 0)
    {
        // sip URI; keep as-is

        phone = read;
        normalized = false;

        return;
    }

    // if prefixed w/ tel:, just ignore the tel: prefix and normalize
    // the remainder

	// Remove all non-digits from string.
	for (write = buffer; *read != '\0' && size < MAX_PHONE_LEN - 1; read++)
	{
		if (strchr(digit_charset, *read) != NULL)
		{
            // digit
			*write++ = *read;
			size++;
		}
		else if (strchr(control_charset, *read) != NULL)
		{
			special = true;
			*write++ = *read;
			size++;
		}
        else if (*read == '+' && write == buffer /* i.e. 1st 'non-garbage' char */)
        {
            has_country_code = true;
        }
		else if (strchr(extension_delimiter, *read) != NULL)
		{
            char ebuffer[MAX_PHONE_LEN];
            unsigned esize;

            for (++read /* skip delimiter */, write = ebuffer, esize = 0;
                 *read != '\0' && esize < MAX_PHONE_LEN - 1; read++)
            {
                if (strchr(digit_charset, *read) != NULL)
                {
                    // digit
                    *write++ = *read;
                    esize++;
                }
            }

            ebuffer[esize] = '\0';
            extension = ebuffer;

			break;
		}
	}
	buffer[size] = '\0';

	if (special)
	{
		phone = buffer;
		normalized = false;
	}
	else if (country == US)
	{
		// US dialing - normalize to 10 digit string.

		if (!strncmp(buffer, i18n_prefix, i18n_prefix.length()))
		{
            // mark as international and skip prefix so it isn't dialed twice
            phone = buffer + i18n_prefix.length();
            i18n = true;
			normalized = false;
		}
        else if (has_country_code && strncmp(buffer,countrycode,strlen(countrycode)))
        {
            // leave as-is for international dialing
            phone = buffer;
            i18n = true;
            normalized = false;
        }
		else if (size == 7 && area_code.length() > 0)
		{
			phone = area_code + buffer;
			normalized = true;
		}
		else if (size == 8 && *buffer == '1' && area_code.length() > 0)
		{
            // get rid of toll prefix
			phone = area_code + (buffer+1);
			normalized = true;
		}
		else if (size == 10)
		{
			phone = buffer;
			normalized = true;
		}
		else if (size == 11 && *buffer == '1')
		{
            // get rid of toll prefix
			phone = buffer + 1;
			normalized = true;
		}
		else
		{
			// Non-standard length, leave as is.
			phone = buffer;
			normalized = false;
		}
	}

	// See if local or long distance.
	if (normalized)
	{
		IString this_area_code(phone.get_string(), 3);
		if (this_area_code == area_code)
		{
			// Area code is same as ours, check exchange.
			char this_exchange[4];
			strncpy(this_exchange, phone.get_string() + 3, 3);
			this_exchange[3] = '\0';

			long_distance = find_number(exchanges, this_exchange, default_exchange);
		}
		else
		{
			// Check area code.  Only can have local area codes if 10 digit dialing is active.
			if (dialing_mode == TenDigitDialing)
				long_distance = find_number(area_codes, this_area_code, default_area_code);
			else
				long_distance = true;
		}
	}
}

// Return true if number is long distance, false if local.
bool PhoneNumber::find_number(IHash & list, const char * check, bool def)
{
	IString key(check);
	NumberSegment * s = (NumberSegment*)list.get(&key);
	if (s == NULL)
		return def;
	else
		return s->is_ld();
}

void PhoneNumber::get_converted_phone(char * out)
{
	*out = '\0';

	if (normalized && country == US)
	{
		switch (dialing_mode)
		{
            default:
            case NormalDialing:
                // Traditional
                if (long_distance)
                {
                    if (strncmp(phone, area_code, 3))
                    {
                        strncat(out, toll_prefix, MAX_PHONE_LEN);
                        strncat(out, get_phone() + 3, MAX_PHONE_LEN);
                    }
                    else
                    {
                        strncat(out, toll_prefix, MAX_PHONE_LEN);
                        strncat(out, get_phone(), MAX_PHONE_LEN);
                    }
                }
                else
                {
                    strncat(out, get_phone() + 3, MAX_PHONE_LEN);
                }
                break;

            case LDAreaCodeDialing:
                // Dial area code if long distance
                if (long_distance)
                {
                    strncat(out, toll_prefix, MAX_PHONE_LEN);
                    strncat(out, get_phone(), MAX_PHONE_LEN);
                }
                else
                {
                    strncat(out, get_phone() + 3, MAX_PHONE_LEN);
                }
                break;

            case TenDigitDialing:
                // 10 digit dialing
                if (long_distance)
                {
                    strncat(out, toll_prefix, MAX_PHONE_LEN);
                    strncat(out, get_phone(), MAX_PHONE_LEN);
                }
                else
                {
                    strncat(out, get_phone(), MAX_PHONE_LEN);
                }
                break;
		}
	}
	else if (i18n)
    {
        strncat(out, i18n_prefix, MAX_PHONE_LEN);
        strncat(out, get_phone(), MAX_PHONE_LEN);
    }
    else
	{
		strncat(out, get_phone(), MAX_PHONE_LEN);
	}

	log_status(ZONE, "Dialing - converted %s to %s", phone.get_string(), out);
}
