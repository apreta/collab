/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

/*
 *
 * Manage participant groups
 *
 */

#include "meetparam.h"

MeetParam::MeetParam(const char * id)
{
    m_id = id;
    m_creation = time(NULL);

    log_debug(ZONE, "MeetParam(%s) c'tor %ld",
              (const char *)m_id, m_creation);
}

MeetParam::~MeetParam()
{
    m_plist.remove_all(true /* delete elements */);

    log_debug(ZONE, "MeetParam(%s) destroyed", (const char *)m_id);
}

void MeetParam::add_participant(const char *screen_name, 
                                const char *display_name,
                                const char *phone,
                                const char *email,
                                unsigned im_type,
                                const char *im_screen_name,
                                unsigned moderator)
{
    MeetParamPart * p = new MeetParamPart(screen_name, 
                                          display_name,
                                          phone,
                                          email,
                                          im_type,
                                          im_screen_name,
                                          moderator);
    m_plist.add(p);
    p->dereference();
}

void MeetParam::set_iterator(IListIterator & iter)
{
    iter.set_list(m_plist);
}

/* static */ unsigned int MeetParamTable::m_seq;
/* static */ IMutex MeetParamTable::m_id_lock;

/* static */ void MeetParamTable::mk_id(char *id)
{
    IUseMutex uil(m_id_lock);

    sprintf(id, "%d", m_seq++);
};

void MeetParamTable::add_group(spMeetParam & pg)
{
    IUseMutex uil(m_instance_lock);

    char id[16];
		
    mk_id(id);
    pg = new MeetParam(id);
    m_pglist.add(pg);
    m_pgtable.insert(new IString(id), pg);
    pg->dereference();
}

void MeetParamTable::get_group(spMeetParam & pg, const char *key)
{
    IUseMutex uil(m_instance_lock);

    IString iskey = key;
    pg = (MeetParam *)m_pgtable.get(&iskey);
}

void MeetParamTable::check_expire()
{
    IUseMutex uil(m_instance_lock);

    IListIterator pg_iter(m_pglist);
    
    time_t now = time(NULL);
    spMeetParam pg = (MeetParam *)pg_iter.get_first();    
    while (pg && pg->expired(now))
    {
        IString key = pg->get_id();

        log_debug(ZONE, "%s expired", (const char *)key);
        
        m_pglist.remove_head();
        pg->dereference();
        m_pgtable.remove(&key, true);

        pg = (MeetParam *)pg_iter.get_first();
    }
}
