/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef _WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "controllerrpc.h"
#include "events.h"
#include "controller.h"
#include "meeting.h"
#include "appshare.h"
#include "../common/address.h"
#include "../common/common.h"
#include "../common/eventnames.h"
#include "../../jcomponent/util/log.h"


// Most events are sent only to participants that are actively connected
// to a meeting (directed events such as an invitation are sent to the
// participant regardless of his meeting status).  Currently we do not
// preload meetings or announce that a scheduled meeting is due.
//
// Todo:
//  - Generate RPC block seperately, and re-use when sending to multiple recipients
//  - Ensure events for meeting are sent in proper order.  For now, using one dispatch
//    thread to ensure order.
//

EventDispatch dispatcher;

Event::Event(int _event, int _send_type, int _to_type, Meeting * _meeting, SubMeeting * _sm, Participant * _source, Participant * _target, const char * _message, int invite_type)
	: message(_message), meeting(_meeting), submeeting(_sm), source(_source), target(_target)
{
	event = _event;
	send_type = _send_type;
	to_type = _to_type;
	options = invite_type;
}

// for docshare
Event::Event(int _event, int _send_type, int _to_type, Meeting * _meeting, SubMeeting * _sm, const char* _doc_id, const char* _page_id, int _options, const char* _drawings)
	: message(NULL), meeting(_meeting), submeeting(_sm), source(NULL), target(NULL),
	doc_id(_doc_id), page_id(_page_id), options(_options), drawings(_drawings)
{
	event = _event;
	send_type = _send_type;
	to_type = _to_type;
}

Event::~Event()
{
	// We needed to keep meeting data until now, to make sure we could dispatch to all participants.
	// Now we can release.
	if (event == MeetingEnded && meeting != NULL)
		meeting->release();
	else if (event == SubmeetingRemoved && submeeting != NULL)
		submeeting->release();
}

EventDispatch::EventDispatch()
{
	pool = NULL;
}

EventDispatch::~EventDispatch()
{
	if (pool)
		delete pool;
}

void EventDispatch::startup()
{
	pool = new IThreadPool(dispatch, this, NUM_DISPATCH_THREADS);
}

void EventDispatch::shutdown()
{
	if (pool)
	{
		delete pool;
		pool = NULL;
	}
}

// Add event to event queue.  Will be sent by worker thread.
void EventDispatch::send(int event, int send_type, int to_type, Meeting * meeting, SubMeeting * sm, Participant * source, Participant * target, const char * message, int invite_type)
{
	Event *ev = new Event(event, send_type, to_type, meeting, sm, source, target, message, invite_type);
	pool->push(ev);
}

// for docshare
void EventDispatch::send(int event, int send_type, int to_type, Meeting * meeting, SubMeeting * sm, 
						 const char* doc_id, const char* page_id, int options, const char* drawings)
{
	Event *ev = new Event(event, send_type, to_type, meeting, sm, doc_id, page_id, options, drawings);
	pool->push(ev);
}

// Worker thread for completing send of event.
void EventDispatch::dispatch(void *data, void *dispatch_ptr)
{
    bool bNeedSend = true;
    xmlrpc_env env;
	xmlrpc_value * msg = NULL;
	Event * ev = (Event*)data;
	EventDispatch * ths = (EventDispatch *) dispatch_ptr;

	log_debug(ZONE, "Starting event dispatch for meeting %s", ev->meeting->get_id());

    xmlrpc_env_init(&env);

	// Figure out what info is being sent
	if (ev->send_type == SendMeetingState)
	{
        bNeedSend = false;
        if (!ths->send_meeting_state(&env, ev))
        {
			log_error(ZONE, "Unable to build meeting status for meeting %s\n", ev->meeting->get_id());
            goto cleanup;
        }
	}
	else if (ev->send_type == SendMeetingInfo)
	{
		msg = ths->get_meeting_info(&env, ev, true, NULL);
		if (msg == NULL)
		{
			log_error(ZONE, "Unable to build meeting info for meeting %s\n", ev->meeting->get_id());
			goto cleanup;
		}
	}
	else if (ev->send_type == SendParticipantState)
	{
        bNeedSend = false;
		msg = ths->get_participant_info(&env, ev);
		if (msg == NULL)
		{
			log_error(ZONE, "Unable to build participant state for meeting %s\n", ev->meeting->get_id());
			goto cleanup;
		}
        else
        {
            ths->send_participant_state(&env, ev, msg);
        }
	}
	else if (ev->send_type == SendSubmeetingState)
	{
		msg = ths->get_submeeting_info(&env, ev);
		if (msg == NULL)
		{
			log_error(ZONE, "Unable to build info for submeeting %s\n", ev->submeeting->get_id());
			goto cleanup;
		}
	}
	else if (ev->send_type == SendInvite)
	{
		msg = ths->get_invite_info(&env, ev);
		if (msg == NULL)
		{
			log_error(ZONE, "Unable to build meeting invite for meeting %s\n", ev->meeting->get_id());
			goto cleanup;
		}
	}
	else if (ev->send_type == SendAppShare)
	{
		msg = ths->get_app_share_info(&env, ev);
		if (msg == NULL)
		{
			log_error(ZONE, "Unable to build app share info for meeting %s\n", ev->meeting->get_id());
			goto cleanup;
		}
	}
	else if (ev->send_type == SendDocShare)
	{
		msg = ths->get_doc_share_info(&env, ev);
		if (msg == NULL)
		{
			log_error(ZONE, "Unable to build doc share info for meeting %s\n", ev->meeting->get_id());
			goto cleanup;
		}
	}
	else if (ev->send_type == SendPPTShare)
	{
		msg = ths->get_ppt_share_action(&env, ev);
		if (msg == NULL)
		{
			log_error(ZONE, "Unable to build ppt action for meeting %s\n", ev->meeting->get_id());
			goto cleanup;
		}
	}
	else if (ev->send_type == SendMeetingChat)
	{
		msg = ths->get_chat_message(&env, ev);
		if (msg == NULL)
		{
			log_error(ZONE, "Unable to build chat message for meeting %s\n", ev->meeting->get_id());
			goto cleanup;
		}
	}
	else
	{
		// Event only...send meeting id and source.
		// "SENDTYPE", "EVENT", "MEETINGID", "PARTICIPANTID", "APPSHARE_KEY"
		msg = xmlrpc_build_value(&env, "(iisss)", SendEventOnly, ev->event, ev->meeting->get_id(), ev->source ? ev->source->get_id() : "", ev->meeting->get_appshare_key());
		XMLRPC_FAIL_IF_FAULT(&env);
	}

	// If the event still hasn't been sent, 
    // Then figure out where to send...
    if (bNeedSend)
    {
	    if (ev->to_type == ToOneParticipant)
	    {
		    ths->send_to_one(&env, ev, ev->target, msg);
	    }
	    else if (ev->to_type == ToHostParticipant)
	    {
		    ths->send_to_host(&env, ev, msg);
	    }
	    else if (ev->to_type == ToAllParticipants ||
                 ev->to_type == ToNonModeratorParticipants ||
                 ev->to_type == ToModeratorParticipants)
	    {
		    ths->send_to_many(&env, ev, msg);
	    }
    }

	if (ev->event >= 0 && ev->event <= (int)NUMBER_EVENT_NAMES)
	{
		log_debug(ZONE, "Dispatched event %s for meeting %s", event_names[ev->event], ev->meeting->get_id());
	}
	else
	{
		log_debug(ZONE, "Dispatched event %d for meeting %s", ev->event, ev->meeting->get_id());
	}
	
cleanup:
	if (msg)
		xmlrpc_DECREF(msg);
	if (ev)
		delete ev;

	xmlrpc_env_clean(&env);
}


// Builds result set with meeting description
// Row 1: "SENDTYPE", "EVENT", "MEETINGID", "HOSTID", "TYPE", "PRIVACY", "PRIORITY", "TITLE", "DESCRIPTION", "SIZE", "DURATION", "TIME", 
// "RECURRENCE", "OPTIONS", "END_TIME", "INVITE_EXPIRATION", "INVITE_VALID_BEFORE", "INVITE_MESSAGE", "DISPLAY_ATTENDEES", "INVITEE_CHAT_OPTIONS"
// "ENABLED_FEATURES", "MEETING_INSTANCE_TIME"
// Remaining rows: "PARTICIPANTID", "SUBMEETINGID", "ROLL", "USERID", "NAME", "DESCRIPTION", "PHONE", "EMAIL", "SCREENNAME", "STATUS", "NOTIFY_TYPE"
xmlrpc_value * EventDispatch::get_meeting_info(xmlrpc_env * env, Event *event, bool meeting_only, Participant * recipient)
{
    xmlrpc_value *val = NULL;
    xmlrpc_value *array = NULL;
	xmlrpc_value *out = NULL;
	IRefObjectPtr<Meeting> meeting = event->meeting;
    IRefObjectPtr<SubMeeting> sm;
    IListIterator iter;
    int count = 0;

    // Meeting is locked while copying info.
	IUseMutex lock(meeting->get_lock());

    // Copy from result set into array.
    array = xmlrpc_build_value(env, "()");
    XMLRPC_FAIL_IF_FAULT(env);

	// First row describes event and meeting.
	// "SENDTYPE", "EVENT", "MEETINGID", "HOSTID", "TYPE", "PRIVACY", "PRIORITY", "TITLE", "DESCRIPTION", "SIZE", "DURATION", "TIME", 
    // "RECURRENCE", "OPTIONS", "END_TIME", "INVITE_EXPIRATION", "INVITE_VALID_BEFORE", "INVITE_MESSAGE", "PASSWORD", "DISPLAY_ATTENDEES", "INVITEE_CHAT_OPTIONS"
    // "ENABLED_FEATURES", "MEETING_INSTANCE_TIME", "BRIDGE_ADDRESS", "BRIDGE_PHONE"
    val = xmlrpc_build_value(env, "(iissiiissiiiiiiiissiiiiss)", SendMeetingState, event->event, 
			meeting->get_id(), meeting->get_host()->get_id(), meeting->get_type(), meeting->get_privacy(), meeting->get_priority(), 
			meeting->get_title(), meeting->get_description(), meeting->get_expected_participants(), 
			meeting->get_duration(), meeting->get_start(), meeting->get_recurrence(), 
			meeting->get_options(), meeting->get_end(), meeting->get_invite_expiration(), meeting->get_invite_valid_before(),
            meeting->get_invite_message(), meeting->get_password(), meeting->get_display_attendees(), meeting->get_invitee_chat_options(),
            meeting->get_hosts_enabled_features(), meeting->get_actual_start(),
            meeting->get_bridge_address(), meeting->get_bridge_phone());
    xmlrpc_array_append_item(env, array, val);
    XMLRPC_FAIL_IF_FAULT(env);
	count++;

    if (!meeting_only)
    {
	    // Remainder of rows contain participants
        meeting->get_submeetings(iter);
        sm = (SubMeeting *)iter.get_first();

        while (sm != NULL)
        {
		    // First add submeeting info to list
		    // "SUBMEETINGID", "ISMAIN", "TITLE"
		    xmlrpc_value *sm_val = xmlrpc_build_value(env, "(sbs)", sm->get_id(), (xmlrpc_bool)sm->is_main(), sm->get_title());
		    XMLRPC_FAIL_IF_FAULT(env);

            xmlrpc_array_append_item(env, array, sm_val);
            xmlrpc_DECREF(sm_val);
            XMLRPC_FAIL_IF_FAULT(env);

            count++;

		    // Then add all participants in the submeeting
		    IListIterator iter2;

            sm->get_participants(iter2);

            Participant * p = (Participant *)iter2.get_first();

            while (p != NULL)
            {
			    // Only include normal participant, invited participants are hidden
			    // unless they join meeting.
			    if (p->get_participant_type() == ParticipantNormal &&
                    ((recipient == NULL) ||
                     (recipient->is_moderator()) ||
                     (meeting->get_display_attendees() == DisplayAll) ||
                     (meeting->get_display_attendees() == DisplayModerators && p->is_moderator()) ||
                     (p->equals(recipient))))
			    {
				    User * u = p->get_user();

				    // When first joining meeting, flag target participant with self flag.
				    int status = p->get_status();
				    if (event->event == MeetingJoined && event->target == p)
					    status |= StatusSelf;

				    // "PARTICIPANTID", "SUBMEETINGID", "ROLL", "USERID", "NAME", "DESCRIPTION", "PHONE", "EMAIL", "SCREENNAME", "STATUS", "NOTIFY_TYPE"
				    xmlrpc_value *p_value = xmlrpc_build_value(env, "(ssisssisissii)",
					    p->get_id(), sm->get_id(), p->get_roll(), u->get_id(), p->get_name(), p->get_description(), 
                        p->get_sel_phone(), p->get_phone(), p->get_sel_email(), p->get_email(), 
                        p->get_screen(), status, p->get_notify_type());
				    XMLRPC_FAIL_IF_FAULT(env);

				    xmlrpc_array_append_item(env, array, p_value);
				    xmlrpc_DECREF(p_value);
				    XMLRPC_FAIL_IF_FAULT(env);

				    count++;
			    }

                p = (Participant *)iter2.get_next();
            }

            sm = (SubMeeting *)iter.get_next();
        }
    }

    // Create return parameter array.
    out = xmlrpc_build_value(env, "(Viiii)", array, 0, count, count, 0);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:

	if (val)
		xmlrpc_DECREF(val);
	if (array)
		xmlrpc_DECREF(array);

	return out;
}


// Builds details about one participant.
// "SENDTYPE", "EVENT", "MEETINGID", "PARTICIPANTID", "SUBMEETINGID", "ROLL", "USERID", "NAME", "DESCRIPTION", "PHONE", 
// "EMAIL", "SCREENNAME", "STATUS", "NOTIFY_TYPE", "VOLUME", "GAIN", "MUTE"
xmlrpc_value * EventDispatch::get_participant_info(xmlrpc_env * env, Event *event)
{
	xmlrpc_value *out = NULL;
	IRefObjectPtr<Meeting> meeting = event->meeting;
	IRefObjectPtr<Participant> p = event->source;

	IUseMutex lock(meeting->get_lock());

    User * u = p->get_user();
 	SubMeeting * sm = event->submeeting;

	assert(u != NULL && sm != NULL);
	
	if (u == NULL || sm == NULL)
		return NULL;

	// "SENDTYPE", "EVENT", "MEETINGID", "PARTICIPANTID", "SUBMEETINGID", "ROLL", "USERID", "NAME", "DESCRIPTION", "PHONE", "EMAIL", "SCREENNAME", "STATUS", "NOTIFY_TYPE"
	out = xmlrpc_build_value(env, "(iisssisssisissiiiii)", SendParticipantState, event->event,
                             meeting->get_id(), p->get_id(), sm->get_id(), p->get_roll(), u->get_id(), p->get_name(), p->get_description(), 
                             p->get_sel_phone(), p->get_phone(), p->get_sel_email(), p->get_email(), 
                             p->get_screen(), p->get_status(), p->get_notify_type(), p->get_volume(), p->get_gain(), p->get_mute());
	XMLRPC_FAIL_IF_FAULT(env);

cleanup:

	return out;
}


// Submeeting state:
// "SENDTYPE", "EVENT", "MEETINGID", "SUBMEETINGID", "ISMAIN", "TITLE"
xmlrpc_value * EventDispatch::get_submeeting_info(xmlrpc_env * env, Event *event)
{
	xmlrpc_value *out = NULL;
	IRefObjectPtr<Meeting> meeting = event->meeting;
	IRefObjectPtr<SubMeeting> sm = event->submeeting;

	IUseMutex lock(meeting->get_lock());

	assert(sm != NULL);

	out = xmlrpc_build_value(env, "(iissbs)", SendSubmeetingState, event->event,
		meeting->get_id(), sm->get_id(), sm->is_main(), sm->get_title());
	XMLRPC_FAIL_IF_FAULT(env);

cleanup:

	return out;
}


// Build invite
// "SENDTYPE", "EVENT", "MEETINGID", "HOSTID", "TYPE", "PRIVACY", "PRIORITY", "TITLE", "SIZE", "DURATION", "TIME", "RECURRENCE", "OPTIONS", 
// "FROMNAME", "MESSAGE", "PARTICIPANTID", "PARTICIPANTNAME", "INVITETYPE"
xmlrpc_value * EventDispatch::get_invite_info(xmlrpc_env * env, Event *event)
{
	xmlrpc_value *out = NULL;
	IRefObjectPtr<Meeting> meeting = event->meeting;
	IRefObjectPtr<Participant> p = event->target;

	IUseMutex lock(meeting->get_lock());

	// Meeting invite
	// "SENDTYPE", "EVENT", "MEETINGID", "HOSTID", "TYPE", "PRIVACY", "PRIORITY", "TITLE", "SIZE", "DURATION", "TIME", "RECURRENCE", "OPTIONS", 
	// "FROMNAME", "MESSAGE", "PARTICIPANTID", "PARTICIPANTNAME", "INVITETYPE"
	out = xmlrpc_build_value(env, "(iissiiissiiiiissssi)", SendInvite, event->event, 
			meeting->get_id(), meeting->get_host()->get_id(), meeting->get_type(), meeting->get_privacy(), meeting->get_priority(), 
			meeting->get_title(), meeting->get_description(), meeting->get_expected_participants(), 
			meeting->get_duration(), meeting->get_start(), meeting->get_recurrence(), 
			meeting->get_options(), event->source ? event->source->get_name() : "", 
			event->message.get_string(), event->target ?  p->get_id() : "",
			event->target ? p->get_name() : "",
			event->options);
	XMLRPC_FAIL_IF_FAULT(env);

cleanup:

	return out;
}


// Build app share
// "SENDTYPE", "EVENT", "MEETINGID", "PRESENTERID", "ADDRESS", "PASSWORD", "STATUS"
xmlrpc_value * EventDispatch::get_app_share_info(xmlrpc_env * env, Event *event)
{
	xmlrpc_value *out = NULL;
	spMeeting meeting = event->meeting;
	spParticipant part = event->target;
	const char * share_address, * share_password;
	const char * presenter_id;
	int share_options = meeting->get_share_options();

	IUseMutex lock(meeting->get_lock());

	spShareServer server = meeting->get_share_server();
	if (server == NULL)
	{
		log_debug(ZONE, "App share event dispatched, but no share server available (%s)", meeting->get_id());
		return NULL;
	}

	if (event->event == PresenterGranted)
	{
		// Send address that presenter should use to create reverse
		// connection to reflector
		share_address = server->get_address();
		share_password = server->get_password();
	}
	else
	{
		// Send address for other clients to connect at
		if (strlen(meeting->get_share_address()))
		{
			// Client specified a peer address
			share_address = meeting->get_share_address();
			share_password = meeting->get_share_password();
		}
		else
		{
			// Send reflector address
			share_address = server->get_address();
			share_password = server->get_password();
		}
	}

	if (meeting->get_presenter() != NULL)
		presenter_id = meeting->get_presenter()->get_id();
	else
		presenter_id = "";

	out = xmlrpc_build_value(env, "(iissssiis)", SendAppShare, event->event, 
			meeting->get_id(), presenter_id, 
			share_address, share_password, 0, share_options, meeting->get_appshare_key());
	XMLRPC_FAIL_IF_FAULT(env);

cleanup:

	return out;
}

// Build doc share
// "SENDTYPE", "EVENT", "MEETINGID", "PRESENTERID", "ADDRESS", "DOCID", "PAGEID", "Options", "DRAWING"
xmlrpc_value * EventDispatch::get_doc_share_info(xmlrpc_env * env, Event *event)
{
	xmlrpc_value *out = NULL;
	spMeeting meeting = event->meeting;
	spParticipant part = event->target;
	const char * address;
	const char * presenter_id;

	IUseMutex lock(meeting->get_lock());

	if (event->event == PresenterGranted)
	{
		// Send address that presenter should use to create reverse
		// connection to Doc Share Server
		address = meeting->get_doc_share_server();
	}
	else
	{
		// Send address for other clients to connect at
		if (strlen(meeting->get_doc_share_address()))
		{
			// Client specified a peer address
			address = meeting->get_doc_share_address();
		}
		else
		{
			// Send Doc Share Server address
			address = meeting->get_doc_share_server();
		}
	}

	if (meeting->get_presenter() != NULL)
		presenter_id = meeting->get_presenter()->get_id();
	else
		presenter_id = "";
	if (address == NULL)
		address = "";

	out = xmlrpc_build_value(env, "(iisssssis)", SendDocShare, event->event, 
                             meeting->get_id(), presenter_id, address, 
                             (const char *)event->doc_id,
                             (const char *)event->page_id,
                             event->options,
                             (const char *)event->drawings);

	XMLRPC_FAIL_IF_FAULT(env);

cleanup:

	return out;
}

// Build PPT share
// "SENDTYPE", "EVENT", "MEETINGID", "ACTION"
xmlrpc_value * EventDispatch::get_ppt_share_action(xmlrpc_env * env, Event *event)
{
	xmlrpc_value *out = NULL;
	spMeeting meeting = event->meeting;

	IUseMutex lock(meeting->get_lock());

	out = xmlrpc_build_value(env, "(iiss)", SendPPTShare, event->event, 
			meeting->get_id(), 
			(const char*)event->message);

	XMLRPC_FAIL_IF_FAULT(env);

cleanup:

	return out;
}

// Build chat message
// "SENDTYPE", "EVENT", "MEETINGID", "FROMID", "TOTYPE", "MESSAGE"
xmlrpc_value * EventDispatch::get_chat_message(xmlrpc_env * env, Event *event)
{
	xmlrpc_value *out = NULL;
	spMeeting meeting = event->meeting;
	spParticipant part = event->source;

	IUseMutex lock(meeting->get_lock());

	out = xmlrpc_build_value(env, "(iisssiis)", 
                SendMeetingChat, 
                event->event, 
                meeting->get_id(), 
                part->get_id(), 
                part->get_name(),
                part->get_roll(),
                event->to_type, 
                event->message.get_string());
	XMLRPC_FAIL_IF_FAULT(env);

cleanup:

	return out;
}


// Pushes out meeting state events to proper recipients 
// based on meeting settings for DisplayAttendees
bool EventDispatch::send_meeting_state(xmlrpc_env * env, Event *event)
{
    bool bResult = true;

	// First get all appropriate participants.
	Participant * participants[MAX_PARTICIPANTS];
	IRefObjectPtr<Meeting> meeting = event->meeting;
	IUseMutex lock(meeting->get_lock());
    int count = 0;

    if (event->to_type == ToOneParticipant)
    {
		participants[count++] = event->target;
		event->target->reference();
    }
    else if (event->to_type == ToAllParticipants ||
             event->to_type == ToNonModeratorParticipants)
    {
        IRefObjectPtr<SubMeeting> sm;
        IListIterator iter;
	    
        meeting->get_submeetings(iter);
        sm = (SubMeeting *)iter.get_first();

        while (sm != NULL)
        {
            IListIterator iter2;
            sm->get_participants(iter2);
            Participant * p = (Participant *)iter2.get_first();

            while (p != NULL && count < MAX_PARTICIPANTS)
            {
                if (event->to_type == ToAllParticipants ||
                    (event->to_type == ToNonModeratorParticipants && !p->is_moderator()))
                {
    			    participants[count++] = p;
	    		    p->reference();
                }

                p = (Participant *)iter2.get_next();
            }
            sm = (SubMeeting *)iter.get_next();
        }
    }

	lock.unlock();
	
	// Now send to all connected participants
	xmlrpc_value * msg = NULL;

	for (int i=0; i<count; i++)
	{
	    msg = get_meeting_info(env, event, false, participants[i]);

	    if (msg == NULL)
	    {
		    log_error(ZONE, "Unable to build meeting info for meeting %s\n", event->meeting->get_id());
		    bResult = false;
	    }
        else
        {
		    send_to_one(env, event, participants[i], msg);
    		xmlrpc_DECREF(msg);
        }

		participants[i]->dereference();
	}

    return bResult;
}


void EventDispatch::send_participant_state(xmlrpc_env * env, Event * event, xmlrpc_value * msg)
{
	// First get all appropriate participants.
	Participant * participants[MAX_PARTICIPANTS];
	IRefObjectPtr<Meeting> meeting = event->meeting;
	IUseMutex lock(meeting->get_lock());
    int count = 0;

    if (event->to_type == ToOneParticipant)
    {
        if ((event->target->is_moderator()) ||
            (meeting->get_display_attendees() == DisplayAll) ||
            (meeting->get_display_attendees() == DisplayModerators && event->source->is_moderator()) ||
            (event->target->equals(event->source)))
        {
		    participants[count++] = event->target;
		    event->target->reference();
        }
    }
    else if (event->to_type == ToAllParticipants ||
             event->to_type == ToNonModeratorParticipants)
    {
        IRefObjectPtr<SubMeeting> sm;
        IListIterator iter;
	    
        meeting->get_submeetings(iter);
        sm = (SubMeeting *)iter.get_first();

        while (sm != NULL)
        {
            IListIterator iter2;
            sm->get_participants(iter2);
            Participant * p = (Participant *)iter2.get_first();

            while (p != NULL && count < MAX_PARTICIPANTS)
            {
                if ((event->to_type == ToNonModeratorParticipants && !p->is_moderator()) ||
                    (event->to_type != ToNonModeratorParticipants && p->is_moderator()) ||
                    (meeting->get_display_attendees() == DisplayAll) ||
                    (meeting->get_display_attendees() == DisplayModerators && event->source->is_moderator()) ||
                    (p->equals(event->source)))
                {
			        participants[count++] = p;
			        p->reference();
                }

                p = (Participant *)iter2.get_next();
            }
            sm = (SubMeeting *)iter.get_next();
        }
    }

	lock.unlock();
	
	// Now send to all appropriate participants
    // (those determined to have proper privileges above...)
	for (int i=0; i<count; i++)
	{
		send_to_one(env, event, participants[i], msg);
		participants[i]->dereference();
	}
}


void EventDispatch::send_to_one(xmlrpc_env * env, Event * event, Participant * p, xmlrpc_value * msg)
{	
    // Send message to new participant.
    Address addr;
    const char *addr_str;

	// !!! Originally included any sent 'ToOneParticipant'
	bool special_event = (event->send_type == SendInvite || event->event == MeetingEnded || event->event == MeetingLeft);

	if (!p->is_connected() && !special_event)
		return;  // Not connected to this meeting

	spPort port = p->get_data_port();
	if (port == NULL && special_event)
		port = p->get_user()->get_client();
	if (port == NULL || !port->is_connected())
		return;

    addr_str = port->get_id();
	if (addr_str == NULL || strlen(addr_str) == 0)
		return;	// Not connected from client

    if (parse_address(addr_str, &addr) != Ok)
	{
		log_error(ZONE, "Unable to parse address: %s\n", addr_str);
		return;
	}

	if (event->event < 0 || event->event > (int)NUMBER_EVENT_NAMES)
	{
		log_error(ZONE, "Unable to dispatch unknown event %d", event->event);
		return;
	}

    if (rpc_make_call_array("event", addr.id, event_names[event->event], msg))
    {
        log_error(ZONE, "Error sending entering meeting event to %s\n", addr_str);
    }
	
	log_debug(ZONE, "Sent event to %s", addr_str);
}


void EventDispatch::send_to_host(xmlrpc_env * env, Event * event, xmlrpc_value * msg)
{
	IRefObjectPtr<Meeting> meeting = event->meeting;
    IRefObjectPtr<SubMeeting> sm;
	
	// Try to get the host participant.
    IRefObjectPtr<Participant> p = meeting->find_participant_by_userid(meeting->get_host()->get_id());

    // Send message to host participant
    if (p != NULL)
    {
		send_to_one(env, event, p, msg);
    }
}


void EventDispatch::send_to_many(xmlrpc_env * env, Event * event, xmlrpc_value * msg)
{
	Participant * participants[MAX_PARTICIPANTS];
	IRefObjectPtr<Meeting> meeting = event->meeting;
    IRefObjectPtr<SubMeeting> sm;
    IListIterator iter;
    int count = 0;
	
	// First get all current participants.
	IUseMutex lock(meeting->get_lock());
    meeting->get_submeetings(iter);
    sm = (SubMeeting *)iter.get_first();

    while (sm != NULL)
    {
        IListIterator iter2;
        sm->get_participants(iter2);
        Participant * p = (Participant *)iter2.get_first();

        while (p != NULL && count < MAX_PARTICIPANTS)
        {
            if ((event->to_type == ToAllParticipants) ||
                (event->to_type == ToNonModeratorParticipants && !p->is_moderator()) ||
                (event->to_type == ToModeratorParticipants && p->is_moderator()))
            {
			    participants[count++] = p;
			    p->reference();
            }

            p = (Participant *)iter2.get_next();
        }
        sm = (SubMeeting *)iter.get_next();
    }
	lock.unlock();
	
	// Now send to all connected participants
	for (int i=0; i<count; i++)
	{
		send_to_one(env, event, participants[i], msg);
		participants[i]->dereference();
	}
}

