/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef PHONENUM_H
#define PHONENUM_H

#include "../../util/istring.h"

class NumberSegment : public IObject
{
	IString segment;
	bool ld;

public:
	NumberSegment(const char * digits, bool is_ld);

	const char * get_segment()	{ return segment; }
	bool is_ld()				{ return ld; }

    enum { TYPE_SEGMENT = 3464 };
    virtual int type()
        { return TYPE_SEGMENT; }
    virtual unsigned int hash()
        { return segment.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && segment == ((NumberSegment*)obj)->segment; }
};


class PhoneNumber : public IObject
{
	static int country;
	static int dialing_mode;
	static bool default_exchange;
	static bool default_area_code;
	static IString area_code;
	static IString external_prefix;
	static IString toll_prefix;
	static IString i18n_prefix;
    static IString countrycode;
	static IHash exchanges;
	static IHash area_codes;
	
	int phone_type;
	IString phone;
    IString extension;
	bool direct;
	
	bool i18n;
    bool has_country_code;
	bool normalized;
	bool long_distance;

    static const char *digit_charset;
    static const char *control_charset;
    static const char *extension_delimiter;

public:
	enum Country {
		US = 0
	};
	
	enum DialingModes {
		NormalDialing,
		LDAreaCodeDialing,
		TenDigitDialing
	};

	PhoneNumber(int _type, const char * _phone, bool _direct);

    // used for voice bridge numbers
	PhoneNumber(const char * _phone, bool _normalize);

	static int initialize(const char * config);

	void get_converted_phone(char * out);

	int get_type()				{ return phone_type; }
	const char * get_phone()	{ return phone; }
    const char * get_extension(){ return extension; }
    
	bool is_direct()			{ return direct; }

	bool is_normalized()		{ return normalized; }
	bool is_ld()				{ return long_distance; }
	bool is_i18n()				{ return i18n; }

    enum { TYPE_PHONE = 3463 };
    virtual int type()
        { return TYPE_PHONE; }
    virtual unsigned int hash()
        { return phone_type; }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && phone_type == ((PhoneNumber*)obj)->phone_type; }

private:
	void normalize(const char * number);
	bool find_number(IHash &list, const char * check, bool def);
	
};

#endif
