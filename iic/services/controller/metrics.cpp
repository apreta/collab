/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include <time.h>

#ifdef LINUX
#include <unistd.h>
#include <netdb.h>
#include <sys/stat.h>
#else
#include <windows.h>
#include <sys/types.h>
#include <sys/stat.h>
#endif
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include "metrics.h"
#include "../common/common.h"
#include "../../jcomponent/util/log.h"

//====
// Sample
//====

Sample::Sample(const char* id)
    : m_sample_id(id)
{
    m_ave = 0.0;
    m_total = 0;
    m_peak = 0;
    m_count = 0;
    m_last_time = time(NULL);
}

// record the sample value, updating the peak and total
void Sample::record(int value, int current_time)
{
    m_total += value;
    if (m_peak < value)
        m_peak = value;
    m_last_time = current_time;
    m_count++;
}

// compute the ave at the end of the interval
void Sample::rotate()
{
    if (m_count > 0)
    {
        m_ave = (double)m_total / (double)m_count;
//        log_error(ZONE, "Sample: total: %d count: %d ave %f \n", total, count, ave);
    }
}


//====
// Samples
//====

Samples::Samples()
{
    m_sample_count = 0;

    char id[32];
    sprintf(id, "%d", m_sample_count++);

    m_logger = NULL;

    m_current_sample = new Sample(id);

    m_started = false;
}

Samples::~Samples()
{
    if (m_current_sample != NULL)
        delete m_current_sample;
}

void Samples::started(bool started)
{
    m_started = started;

    // write init to log
    if (m_logger != NULL)
        m_logger->log_init();
}

void Samples::rotate()
{
    if (m_started)
    {
        // compute results for latest
        m_current_sample->rotate();

        // stick latest at top of list
        m_samples.prepend(m_current_sample);

        // if exceeded max remove from bottom of list
        if (m_samples.size() > MAX_SAMPLES_CACHED)
        {
            m_samples.remove_tail();
        }

        // log to file if set
        if (m_logger != NULL)
            m_logger->log_sample(m_current_sample);

        char id[32];
        sprintf(id, "%d", m_sample_count++);
        m_current_sample = new Sample(id);
    }
}

void Samples::record(int value, int current_time)
{
    if (m_started && m_current_sample != NULL)
        m_current_sample->record(value, current_time);
}

void Samples::get_samples(IListIterator & iter)
{
    iter.set_list(m_samples);
}


//====
// SampleLog
//====

SampleLog::SampleLog()
{
    m_init = false;
    m_rotation_interval = 1;
}

void SampleLog::initialize(SampleType type)
{
    size_t len = sizeof(m_hostname)/sizeof(m_hostname[0]);

    int res = gethostname(m_hostname, len);
    if (res < 0)
    {
        log_error(ZONE, "Unable to get hostname: %s", strerror(errno));

        return;
    }

    m_my_type = type;

    m_init = true;
}

SampleLog::~SampleLog()
{
}

int SampleLog::set_location(const char *directory,
                            const char *base_name)
{
    if (!m_init)
    {
        log_error(ZONE, "SampleLog is not initialized");
        
        return -1;
    }
    
    struct stat s;

    if (stat(directory, &s) < 0)
    {
        log_error(ZONE, "Can't get attributes of directory '%s': %s",
                  directory, strerror(errno));
        
        return -1;
    }

    if (!(s.st_mode & S_IFDIR))
    {
        log_error(ZONE, "Log directory '%s' exists, but is not a directory", directory);

        return -1;
    }

    sprintf(m_filename_prefix, "%s/%s", directory, base_name);
    sprintf(m_filename, "%s/%s.elg", directory, base_name);

    m_init = true;

    return 0;
}

void SampleLog::set_rotation_interval(int days)
{
    if (days < 1)
    {
        log_error(ZONE, "set_rotation_interval() - interval must be at least 1 day!");

        return;
    }

    m_rotation_interval = days;
}

// minutes_since_midnight **GMT**
void SampleLog::set_rotation_time(int minutes_since_midnight)
{
    if (minutes_since_midnight < 0)
    {
        log_error(ZONE, "set_rotation_time() given negative minutes!");

        return;
    }

    time_t now = time(NULL);
    time_t prev_midnight = now - (now % SECS_PER_DAY);

    int secs_since_midnight = minutes_since_midnight*60;

    m_next_rotation = prev_midnight + secs_since_midnight;
    if (m_next_rotation > now)
    {
        // it's in the future, so we are good to go
        return;
    }
    
    // rotation time already in the past, so bump to next day/interval
    m_next_rotation += SECS_PER_DAY * m_rotation_interval;
}

int SampleLog::check_rotation()
{
    if (!m_init)
    {
        log_error(ZONE, "SampleLog is not initialized");
        
        return -1;
    }
    
    time_t now = time(NULL);
    if (now < m_next_rotation)
    {
        log_debug(ZONE, "Not yet time for log rotation");

        return 0;
    }

    log_status(ZONE, "Rotating logs");

    char timestamp[21];
#ifdef LINUX
	struct tm timebuf;
    struct tm *tm_now = localtime_r(&now, &timebuf);
#else
    struct tm *tm_now = localtime(&now);
#endif

    strftime(timestamp, 20, "%Y-%m-%d-%H-%M-%S", tm_now);
    char rotfn[_MAXPATHLEN];

    strcpy(rotfn, m_filename_prefix);
    strcat(rotfn, "+");
    strcat(rotfn, timestamp);
    strcat(rotfn, ".elg");

    struct stat s;
    if (stat(m_filename, &s) < 0)
    {
        if (errno == ENOENT)
        {
            log_status(ZONE, "No logged samples seen at log rotation time");

            return 0;
        }
    }
    

    if (rename(m_filename, rotfn) < 0)
    {
        log_error(ZONE, "Unable to rename %s as %s:  %s",
                  m_filename, rotfn, strerror(errno));
        
        return -1;
    }
    
    m_next_rotation += SECS_PER_DAY * m_rotation_interval;	// i.e. update to tomorrow

    return 0;
}

int SampleLog::log_init()
{
    FILE *fp = fopen(m_filename, "a");
    if (fp == NULL)
    {
        log_error(ZONE, "Unable to open sample log %s: %s",
                  m_filename, strerror(errno));

        return -1;
    }
    
    // write init
    if (fprintf(fp, "\"%d\"", SampleTypeInit) < 0)
        return -1;

    if (fprintf(fp, "\n") < 0)
        return -1;
    
    fclose(fp);
    
    return 0;
}

int SampleLog::log_sample(Sample* sample)
{
    FILE *fp = fopen(m_filename, "a");
    if (fp == NULL)
    {
        log_error(ZONE, "Unable to open sample log %s: %s",
                  m_filename, strerror(errno));

        return -1;
    }
    
    // Sample
    if (fprintf(fp, "\"%d\",\"%f\",\"%d\",\"%u\"", 
                m_my_type, 
                sample->get_ave(), 
                sample->get_peak(), 
                sample->get_time()) < 0)
        return -1;

    if (fprintf(fp, "\n") < 0)
        return -1;
    
    fclose(fp);
    
    return 0;
}
