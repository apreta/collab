/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef TRANSPORT_H
#define TRANSPORT_H

#include "../../util/istring.h"
#include "../../util/ithread.h"
#include "../../util/irefobject.h"
#include "../../jcomponent/register.h"
#include "../../jcomponent/util/nadutils.h"


class Transport;
class AOLTransport;
class TransportPool;

/*
  Manages Pool of IM Transport agents.
   
  - Creates pool at initialization
  - Shuts down pool at close
  - Retrieves next available agent at runtime

*/

typedef IRefObjectPtr<Transport> spTransport;

// Creates a jabber session and sends IM to an IM Transport (gateway)
class Transport : public IReferencableObject
{
    IMutex ref_lock;
    int service_type;
    IString transport_id;

protected:
    regist_t registration;

    virtual int create_session(const char* id, 
                               const char* hostname, 
                               const char* username, 
                               const char* password,
                               const char* jabuser,
                               const char* jabhost) = 0;

public:
    Transport(const char * _id, int _service_type);
    virtual ~Transport();

    void lock();
    void unlock();

    const char * get_id()           { return transport_id; }
	void set_id(const char *_id)	{ transport_id = _id; }

    int get_service_type() { return service_type; }

    virtual int send_instant_message(const char* username, const char* body) = 0;
    bool is_connected();
	
    enum { TYPE_TRANSPORT = 3471 };
    virtual int type()
        { return TYPE_TRANSPORT; }
    virtual unsigned int hash()
        { return transport_id.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && transport_id == ((Transport*)obj)->transport_id; }
};

// Transport specific to AOL
// ??? Not positive we need a separate subclass for each individual type transport
class AOLTransport : public Transport
{

    int create_session(const char* id, 
                       const char* hostname, 
                       const char* username, 
                       const char* password,
                       const char* jabuser,
                       const char* jabhost);

public:
    AOLTransport(const char * _id, 
                 const char* hostname, 
                 const char* username, 
                 const char* password, 
                 const char* jabuser,
                 const char* jabhost);
    ~AOLTransport();

    int send_instant_message(const char* username, const char* body);
    
};

// Transport specific to MSN
class MSNTransport : public Transport
{

    int create_session(const char* id, 
                       const char* hostname, 
                       const char* username, 
                       const char* password,
                       const char* jabuser,
                       const char* jabhost);

public:
    MSNTransport(const char * _id, 
                 const char* hostname, 
                 const char* username, 
                 const char* password, 
                 const char* jabuser,
                 const char* jabhost);
    ~MSNTransport();

    int send_instant_message(const char* username, const char* body);
    
};

// a list of Transports for a particular service.  
// maintains an index to the next Transport to be used
class TransportList : public IObject
{
    IList transport_list;
    IString list_id;
    int index;

public:
    TransportList(const char* _id);
    ~TransportList();

    void     add(IObject* obj) { transport_list.add(obj); }
    IObject* get(int n)        { return transport_list.get(n); }
    int      size()            { return transport_list.size(); }
    int      get_index()       { return index; }
    int      increment_index();

    enum { TYPE_TRANSPORTLIST = 3472 };
    virtual int type()
        { return TYPE_TRANSPORTLIST; }
    virtual unsigned int hash()
        { return list_id.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && list_id == ((TransportList*)obj)->list_id; }
};

// the pool (a hash of lists)
class TransportPool
{
    // hash table of gateways
    IHash gateways;

    int read_config(const char* config_file, const char* jabhost);
    Transport* create_transport(const char* type, 
                                const char* hostname, 
                                const char* username, 
                                const char* password,
                                const char* jabuser,
                                const char* jabhost);

public:
    TransportPool(const char* config_file, const char* jabhost);
    ~TransportPool();

    Transport* get_transport(const char* type);
};

extern TransportPool* transport_pool;

#endif
