/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef _WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "controllerrpc.h"
#include "controller.h"
#include "meeting.h"
#include "appshare.h"
#include "../../jcomponent/util/log.h"
#include "../../jcomponent/util/port.h"

#define NO_SHAREMGR_ERRORS
#include "../common/sharecommon.h"

char *i_itoa(int i, char *buffer);

AppShare appshare;


AppShare::AppShare()
{
	monitor = NULL;
}

// Add app share server
void AppShare::add_server(const char * ip, int port)
{
	spShareServer server = new ShareServer(ip, port);
	server->initialize(ip, port + 1);

	servers.add(server);

	server->dereference();

	if (monitor == NULL)
	{
        CommClient::set_header(HEADER);
        int stat = CommProcess::initialize();
        if (stat)
            log_error(ZONE, "Initialization of app share monitor service failed (%d)", stat);

		monitor = new IThread(monitor_thread, this);
	}
}

// Allocate server session
spShareServer AppShare::allocate_session(int expected_size, const char *meeting_id)
{
	ShareServer * ses = NULL;
	int min = MAX_SHARE_SESSIONS;

	IUseMutex lock(share_lock);

	for (int i=0; i < servers.size(); i++)
	{
		ShareServer * check = (ShareServer *)servers.get(i);

		if (check->is_available() && check->get_usage_count() < min)
		{
			ses = check;
			min = ses->get_usage_count();
		}
	}

	if (ses != NULL)
		ses->open_session(meeting_id);

	return ses;
}

void AppShare::release_session(spShareServer & ses, const char *meeting_id)
{
	IUseMutex lock(share_lock);

	ses->close_session(meeting_id);
}

void * AppShare::monitor_thread(void *p)
{
	AppShare * _this = (AppShare *)p;
	bool done = false;
	int start, pause;

    sleep(5);

	while (!done)
	{
		start = time(NULL);

		log_debug(ZONE, "Checking app share servers");

		IVectorIterator iter(_this->servers);
		spShareServer server = (ShareServer *)iter.get_first();

		while (server != NULL && !done)
		{
			server->ping();

			server = (ShareServer *)iter.get_next();
		}

		pause = PING_INTERVAL - (time(NULL) - start);
		if (pause > 0)
			sleep(pause);
	}

	return NULL;
}

bool AppShare::get_server_status(int idx, IString * id, int * available)
{
    if (idx >= servers.size()) return false;

    spShareServer server = (ShareServer *)servers.get(idx);

    *id = server->get_address();
    *available = server->is_available();

    return true;
}

ShareServer::ShareServer(const char * _ip, int _port)
	: ip(_ip), port(_port), comm(this)
{
	usage_count = 0;
	available = false;
	last_ping = 0;
}

void ShareServer::lock()
{
    server_lock.lock();
}

void ShareServer::unlock()
{
    server_lock.unlock();
}

int ShareServer::open_session(const char *meeting_id)
{
	IUseMutex lock(server_lock);
	usage_count++;

	password = "imidio";

	// For now, assume reflector is running where it says it is.

	return 0;
}

int ShareServer::close_session(const char *meeting_id)
{
    {
        IUseMutex lock(server_lock);
        usage_count--;
    }

	send_end_session(meeting_id, ReasonNormal);

	return 0;
}

IString & ShareServer::get_address()
{
	char buff[12];

	if (address.length() == 0)
	{
		address = ip + "::" + i_itoa(port, buff);
	}

	return address;
}

// Initialize--does not connect; we want to do that from the ping method
int ShareServer::initialize(const char * server, int port)
{
	int stat = comm.initialize(server, port, false);
	if (stat)
		log_error(ZONE, "Initialization of share ping service failed (%s, %d)", server, stat);
	return stat;
}

int ShareServer::shutdown()
{
	return CommProcess::shutdown();
}

int ShareServer::ping()
{
    int res = -1;

	long current_last_ping = last_ping;

	if (!comm.is_connected())
		comm.connect();

	if (comm.is_connected())
	{
        CommMessage msg;
        msg.set_int(CmdNoop);
        if (msg.is_valid() && !FAILED(comm.send_message(&msg, 0)))
        {
            int count = 10;
            while (--count)
            {
                sleep(1);

                {
                    IUseMutex lock(server_lock);
                    if (last_ping != current_last_ping)
                    {
                        res = 1;
                        break;
                    }
                }
            }
        }

        if (res != 1)
        {
            log_debug(ZONE, "Could not ping app share %s (%d, %d)", ip.get_string(), current_last_ping, last_ping);
            comm.close(false);
        }
	}

	IUseMutex lock(server_lock);
	if (res == 1)
	{
		if (available == false)
		{
			log_status(ZONE, "App share server %s is now available", ip.get_string());
			available = true;
		}
	}
	else {
		if (available == true)
		{
			log_status(ZONE, "App share server %s not responding, setting unavailable", ip.get_string());
			available = false;
		}
	}

    return res;
}

// Send presenter participant id to reflector.
int ShareServer::send_presenter_id(const char * meetingid, const char * presenter_id)
{
    log_debug(ZONE, "Sending presenter id %s for %s", presenter_id, meetingid);
    CommMessage msg;
    msg.set_int(CmdChangePresenter);
    msg.set_string(meetingid);
    msg.set_string(presenter_id);
    if (!msg.is_valid() || FAILED(comm.send_message(&msg, 0)))
    {
        log_debug(ZONE, "Could not send presenter id %s", presenter_id);
        return Failed;
    }
    return Ok;
}

// Send end session message to reflector.
int ShareServer::send_end_session(const char * meetingid, int reason)
{
    log_debug(ZONE, "Ending meeting %s (%d)", meetingid, reason);
    CommMessage msg;
    msg.set_int(CmdEndSession);
    msg.set_string(meetingid);
    msg.set_int(reason);
    if (!msg.is_valid() || FAILED(comm.send_message(&msg, 0)))
    {
        log_debug(ZONE, "Could not end meeting %s", meetingid);
        return Failed;
    }
    return Ok;
}

// Send reject participant message to reflector.
int ShareServer::send_reject_participant(const char * meetingid, const char * partid, int reason)
{
    log_debug(ZONE, "Dropping participant %s from %s (%d)", partid, meetingid, reason);
    CommMessage msg;
    msg.set_int(CmdRejectParticipant);
    msg.set_string(meetingid);
    msg.set_string(partid);
    msg.set_int(reason);
    if (!msg.is_valid() || FAILED(comm.send_message(&msg, 0)))
    {
        log_debug(ZONE, "Could not drop participant %s", partid);
        return Failed;
    }
    return Ok;
}

// Send enable remote control message to reflector.
int ShareServer::send_remote_control(const char * meetingid, const char * partid, bool enable)
{
    log_debug(ZONE, "Sending enable remote control to %s (%d)", meetingid, enable);
    CommMessage msg;
    msg.set_int(CmdEnableRemoteControl);
    msg.set_string(meetingid);
    msg.set_string(partid);
    msg.set_int(enable ? 1 : 0);
    if (!msg.is_valid() || FAILED(comm.send_message(&msg, 0)))
    {
        log_debug(ZONE, "Could not enable remote control %s", meetingid);
        return Failed;
    }
    return Ok;
}

// Send enable recording message to reflector.
int ShareServer::send_recording(const char * meetingid, bool enable, int start)
{
    log_debug(ZONE, "Sending app share enable recording (%d)", meetingid, enable);
    CommMessage msg;
    msg.set_int(CmdEnableRecording);
    msg.set_string(meetingid);
    msg.set_int(enable ? 1 : 0);
	msg.set_int(start);
    if (!msg.is_valid() || FAILED(comm.send_message(&msg, 0)))
    {
        log_debug(ZONE, "Could not enable recording %s", meetingid);
        return Failed;
    }
    return Ok;
}

// Notification from session manager that participant has joined.
void ShareServer::participant_joined(const char * meetingid, const char * partid)
{
	// no-op in app share mode
}

// Notification from session manager that participant has left.
void ShareServer::participant_left(const char * meetingid, const char * partid)
{
	// no-op in app share mode
}

// Handle reflector message that participant has joined.
void ShareServer::on_participant_joined(const char * token, const char * meetingid, const char * partid, bool host)
{
	log_debug(ZONE, "AppShare participant %s joined %s", partid, meetingid);

	spMeeting meeting = master.find_meeting(meetingid);
	if (meeting != NULL)
	{
		spParticipant part = meeting->find_participant(partid);

		if (part != NULL && part->is_presenter() == host /*&& meeting->is_share_active()*/)
        {
			return;
        }
	}

	log_debug(ZONE, "AppShare participant %s rejected", partid);
	send_reject_participant(meetingid, partid, ReasonInvalidSession);
}

void ShareServer::on_participant_left(const char * meetingid, const char * partid, int reason)
{
	log_debug(ZONE, "AppShare participant %s left %s", partid, meetingid);
}

void ShareServer::on_start_session(const char * meetingid)
{
    log_debug(ZONE, "AppShare started %s", meetingid);

	spMeeting meeting = master.find_meeting(meetingid);
	if (meeting != NULL)
	{
        meeting->on_share_started();
    }
}

void ShareServer::on_end_session(const char * meetingid, int reason)
{
	log_debug(ZONE, "AppShare ended %s", meetingid);

	spMeeting meeting = master.find_meeting(meetingid);
	if (meeting != NULL)
	{
        meeting->on_share_ended();
    }
}

// Handle control message from reflector.
bool ShareServer::on_message(CommConnection * c, CommMessage * msg)
{
//    const char *meetingid;
//    const char *token;
//    const char *partid;

    int cmd = msg->get_int();
    switch (cmd)
    {
        case CmdNoop:
        {
			IUseMutex lock(server_lock);
			last_ping = time(NULL);
            break;
        }
        case CmdEndSession:
        {
            IString meetingid = msg->get_string();
            int reason = msg->get_int();
            if (!msg->is_valid())
                return false;
			on_end_session(meetingid, reason);
            break;
        }
        case CmdParticipantJoined:
        {
            IString token = msg->get_string();
            IString meetingid = msg->get_string();
            IString partid = msg->get_string();
            int host = msg->get_int();
            if (!msg->is_valid())
                return false;
            on_participant_joined(token, meetingid, partid, host != 0);
            break;
        }
        case CmdParticipantLeft:
        {
            IString meetingid = msg->get_string();
            IString partid = msg->get_string();
            int reason = msg->get_int();
            if (!msg->is_valid())
                return false;
            on_participant_left(meetingid, partid, reason);
            break;
        }
		case CmdSessionActive:
		{
			IString meetingid = msg->get_string();
			if (!msg->is_valid())
				return false;
			// Host has connected
			on_start_session(meetingid);
			break;
		}
        default:
            return false;
    }

    return true;
}
