/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef _WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "controllerrpc.h"
#include "heartbeat.h"
#include "controller.h"
#include "../common/common.h"
#include "../../jcomponent/util/log.h"

HeartBeat heartbeat;

HeartBeat::HeartBeat()
{
}

HeartBeat::~HeartBeat()
{
}

static bool heartbeat_done = false;

static int heartbeat_internal(void* data)
{
    log_status(ZONE, "starting up");
    int interval = 60*1000; // 60 secs in msecs

    while (!heartbeat_done)
    {
        // sleep for interval in msecs
#ifdef LINUX
		struct timespec tv = { interval / 1000, interval % 1000 * 1000000 };
		nanosleep(&tv, NULL);
#else
        Sleep(interval);
#endif

        // get list of users
        log_debug(ZONE, "pinging dead users");

        IVector * user_vec =  new IVector();
        IVectorIterator iter(*user_vec);
        
        master.get_users(user_vec);
        
        iter.set_vector(*user_vec);

        // ping inactive ones
        User * user;
        for (user = (User *)iter.get_first(); user; user = (User *)iter.get_next())
        {
            time_t delay = time(NULL) - user->get_last_ping_time();
            if (delay > CLIENT_PING_INTERVAL && user->get_reverse_ping_time() != user->get_last_ping_time())
            {
                log_debug(ZONE, "user -- %s -- getting reverse ping", user->get_username());
                user->send_ping(user->get_client(), "reverse_ping", Ok);
                
                // Set last reverse ping time to last ping time so we know that we've already
                // send a reverse ping for this loss of contact
                user->set_reverse_ping_time(user->get_last_ping_time());
            }
        }

        if (user_vec != NULL)
        {
            delete user_vec;
        }

    }

    log_status(ZONE, "shutting down");

    return 0;
}

int HeartBeat::startup()
{
    heartbeat_done = false;

    // start thread and return
	g_thread_create((GThreadFunc)heartbeat_internal, NULL, FALSE, NULL);

	return 0;
}

void HeartBeat::shutdown()
{
    heartbeat_done = true;
}

