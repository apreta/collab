/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef _WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>

#include "controllerrpc.h"
#include "controller.h"
#include "meeting.h"
#include "apidispatch.h"
#include "../common/address.h"
#include "../../jcomponent/util/log.h"
#include "../../jcomponent/util/rpcresult.h"


extern char *i_itoa(int i, char *buffer);

#define log_id(obj)  (obj != NULL ? obj->get_id() : "")

int DispatchQueue::dispatch_id_seq = 0;


const char * DispatchQueue::add_dispatch(DispatchCall * call)
{
	IUseMutex lock(queue_lock);

	char id[32];
	IString * idx = new IString(i_itoa(dispatch_id_seq++, id));

	queue.insert(idx, call);

	log_debug(ZONE, "Queued call %s:%s", idx->get_string(), call->get_name());

	return idx->get_string();
}

void DispatchQueue::cancel_dispatch(const char * id)
{
	IString idx(id);

	IUseMutex lock(queue_lock);

	log_debug(ZONE, "Canceled call %s", idx.get_string());

	queue.remove(&idx, true);
}

void DispatchQueue::process_completed(const char * id, bool success, RPCResultSet *res)
{
	IUseMutex lock(queue_lock);

	IString idx(id);
	DispatchCall * call = (DispatchCall*)queue.get(&idx);

	if (call != NULL)
	{
		queue.remove(&idx, false);
		lock.unlock();

		if (success)
			call->execute(res);
		else
			call->failed();

		log_debug(ZONE, "Dispatched call %s:%s", id, call->get_name());

		call->dereference();
	}
}

void DispatchQueue::process_completed(const char * id, bool success, s2s_t s2s, nad_t nad)
{
	IUseMutex lock(queue_lock);

	IString idx(id);
	DispatchCall * call = (DispatchCall*)queue.get(&idx);

	if (call != NULL)
	{
		queue.remove(&idx, false);
		lock.unlock();

		if (success)
			call->execute(s2s, nad);
		else
			call->failed();

		log_debug(ZONE, "Dispatched call %s:%s", id, call->get_name());

		call->dereference();
	}
}

void PendingQueue::add_pending(DispatchCall * call)
{
	IUseMutex lock(queue_lock);

	queue.add(call);

	log_debug(ZONE, "Pending call %s", call->get_name());
}

void PendingQueue::process_all()
{
	IUseMutex lock(queue_lock);

    DispatchCall * call = (DispatchCall*)queue.remove_head();
    while (call != NULL)
    {
        // In case pending operation wants to add to queue again, unlock before executing
        lock.unlock();
        call->execute();
        lock.lock();

        call = (DispatchCall*)queue.remove_head();
    }
}

void PendingQueue::cancel_all()
{
	IUseMutex lock(queue_lock);

    DispatchCall * call = (DispatchCall*)queue.remove_head();
    while (call != NULL)
    {
        call = (DispatchCall*)queue.remove_head();
    }
}

DispatchCall::~DispatchCall()
{
	log_debug(ZONE, "Deleted dispatch call");
}

// default does nothing - override if desired
void DispatchCall::execute(s2s_t s2s, nad_t nad)
{
    log_error(ZONE, "Called to execute NAD not implemented");
}

// default does nothing - override if desired
void DispatchCall::execute()
{
    log_error(ZONE, "Called to execute pending not implemented");
}

CallParticipantParameters::CallParticipantParameters(spMeeting & _meeting, spUser & _user, spParticipant & _part,
		int _sel_call_phone, const char * _call_phone, int _options)
		: meeting(_meeting), user(_user), part(_part), call_phone(_call_phone)
{
	sel_call_phone = _sel_call_phone;
	options = _options;
}

// Call participant
void CallParticipantParameters::execute(RPCResultSet *res)
{
	int status = meeting->call_participant_internal(user, part, sel_call_phone, call_phone, options);
	if (IS_FAILURE(status))
		log_error(ZONE, "Call participant failed (user: %s status: %d)\n", log_id(user), status);
}

void CallParticipantParameters::failed()
{
	meeting->on_call_failed(user, part, Failed);
}

AddParticipantParameters::AddParticipantParameters(User * _actor, Meeting * _meeting, User * _user, SubMeeting * _sm,
		int _role, const char * _name, int _sel_phone, const char * _phone,
		int _sel_email, const char * _email, const char * _screen, const char * _description,
		const char * _message, int _notify_type, int _call_type)
		: actor(_actor), sm(_sm), name(_name), phone(_phone), email(_email), screen(_screen),
		  description(_description), message(_message)
{
	role = _role;
	meeting = _meeting;
	user = _user;
	sel_phone = _sel_phone;
	sel_email = _sel_email;
	notify_type = _notify_type;
	call_type = _call_type;
}

void AddParticipantParameters::execute(RPCResultSet *res)
{
	char * part_id;
	res->get_output_values("(s*)", &part_id);

	IString id;
	make_id_string(id, "part:", part_id);

	spParticipant dummy;
	int status = meeting->add_participant_internal(actor, sm, user, id, ParticipantNormal, role,
                                                   name, sel_phone, phone, sel_email, email, screen, description,
                                                   notify_type, call_type, true, dummy);
	if (IS_FAILURE(status))
		log_error(ZONE, "Add participant failed (user: %s status: %d)\n", log_id(user), status);
}

void AddParticipantParameters::failed()
{
	log_error(ZONE, "Add participant failed (user: %s)\n", log_id(user));
}

RegisterDispatch::RegisterDispatch(void * _session, const char *_userid, const char * _username, const char * _name,
		const char * _email, const char * _address, const char * _network_address)
		: userid(_userid), username(_username), name(_name), email(_email), address(_address), network_address(_network_address)
{
	session = _session;
}

void register_exec(RPCResultSet &rs, RegisterDispatch * info);
void register_error(RegisterDispatch *info);

void RegisterDispatch::execute(RPCResultSet * res)
{
	register_exec(*res, this);
}

void RegisterDispatch::failed()
{
	register_error(this);
}

// Join meeting dispatch
JoinMeetingDispatch::JoinMeetingDispatch(User * actor, Port * _port, const char * _meetingid, const char *_partid, const char * _password)
	: user(actor), port(_port), meetingid(_meetingid), participantid(_partid), password(_password)
{
}

void JoinMeetingDispatch::execute(RPCResultSet * rs)
{
	int status = Ok;
	int found, options, scheduled_time, invite_valid_before, invite_expiration, type, invited_time;
	char *meetingid, *partid, *userid, *email, *username, *name;
    bool is_anonymous = false;
	spParticipant part;
	IString join_part_id;

    // schedule.find_pin
    //   "FOUND", "MEETINGID", "PARTICIPANTID", "USERID", "EMAIL",
    //   "USERNAME", "NAME", "OPTIONS", "SCHEDULED_TIME", "INVITE_VALID_BEFORE",
    //   "INVITE_EXPIRATION", "TYPE", "INVITED_TIME"
	// FOUND: 1 = meeting found, 2 = meeting and user found, 0 = not found
	if (!rs->get_output_values("(issssssiiiiii*)",
                &found, &meetingid, &partid, &userid, &email, &username,
                &name, &options, &scheduled_time, &invite_valid_before,
                &invite_expiration, &type, &invited_time))
	{
		log_error(ZONE, "Error parsing result from fetch PIN\n");
		status = Failed;
	}
	else
		switch (found)
		{
			case 0:
				// PIN not found, return error.
				status = InvalidPIN;
				break;
			case 1:
				// Anonymous meeting pin
                is_anonymous = true;
				break;
			case 2:
				// Participant id
                is_anonymous = false;
				make_id_string(join_part_id, "part:", partid);
				break;
		}

	if (status == Ok)
    {
        // Check to see if joining too soon, or if invite is expired
        status = master.is_invite_valid(
                            is_anonymous,
                            meetingid, options, scheduled_time,
                            invite_valid_before, invite_expiration,
                            type, invited_time);

        if (status == Ok)
        {
            status = master.start_and_join(user, port, meetingid, join_part_id, password, false);
            if (status == Retry)
            {
                participantid = join_part_id;
            }
        }
    }

	if (IS_FAILURE(status))
		master.on_join_failed(user, port, status, meetingid);
}

void JoinMeetingDispatch::failed()
{
	log_error(ZONE, "Join meeting failed for user %s", log_id(user));
	master.on_join_failed(user, port, Failed, meetingid);
}

// Join meeting dispatch
JoinUserDispatch::JoinUserDispatch(User * actor, Port * _port, Meeting * _meeting, const char *_partid, const char * _password, bool _release)
	: user(actor), port(_port), meeting(_meeting), participantid(_partid), password(_password)
{
	release = _release;
}

// Join user call pending until database join_participant completes
void JoinUserDispatch::execute(RPCResultSet * rs)
{
	int status = Ok;
	char *partid;
	int type, role;
	spParticipant part;

	// PARTICIPANTID, TYPE, ROLE
	if (!rs->get_output_values("(sii)", &partid, &type, &role))
	{
		log_error(ZONE, "Error parsing result from join participant\n");
		status = Failed;
	}

	// N.B. we fetch the participant role, but we don't use it--this participant
	// came from a previous instance of meeting, so we demote it to a normal participant.
	IString id;
	make_id_string(id, "part:", partid);

	if (status == Ok)
		status = meeting->join_user_internal(user, port, part, id, password, release, role);

	if (IS_FAILURE(status))
		meeting->on_join_failed(user, status);
}

// Join user call pending until meeting load completes
void JoinUserDispatch::execute()
{
	int status = Ok;

    status = meeting->join_user(user, port, participantid, password, release);

	if (IS_FAILURE(status))
		meeting->on_join_failed(user, status);
}

void JoinUserDispatch::failed()
{
	log_error(ZONE, "Join user failed for user %s", log_id(user));
	meeting->on_join_failed(user, Failed);
}

AddParticipantsDispatch::AddParticipantsDispatch(User * _actor, Meeting * _meeting, IList * _participants, SubMeeting * _sm)
        : user(_actor), meeting(_meeting), sm(_sm)
{
	participants = _participants;
}

void AddParticipantsDispatch::execute(RPCResultSet * res)
{
	int status = meeting->add_participants(user, participants, sm);
	if (IS_FAILURE(status))
		log_error(ZONE, "Unable to add participant list for user %s", user->get_id());
}

void AddParticipantsDispatch::failed()
{
	log_error(ZONE, "Unable to create submeeting for new participants for user %s", user->get_id());
	if (participants != NULL)
	{
		delete participants;
	}
}

PlaceCallDispatch::PlaceCallDispatch(User * _actor, Meeting * _meeting, Participant * _participant, User * _callee, const char *_phone)
        : user(_actor), meeting(_meeting), participant(_participant), callee(_callee)
{
    phone = _phone;
    submeeting = NULL;
}

void PlaceCallDispatch::set_submeeting(SubMeeting * _submeeting)
{
    submeeting = _submeeting;
}

void PlaceCallDispatch::execute(RPCResultSet * rs)
{
	int status;

    if (submeeting != NULL)
        status = meeting->move_participant_internal(participant, submeeting);
    else
        status = Failed;

    if (status != Ok)
    {
        log_error(ZONE, "Place call failed w/ status %u", status);
        return;
    }

    status = meeting->add_participant(user,
                                      submeeting->get_id(),
                                      callee,
                                      ParticipantNormal,
                                      RollNormal,
                                      "",
                                      SelPhoneThis, phone,
                                      SelEmailDefault, "",
                                      "", "", NotifyPhone,
                                      "", CallSupervised|CallBypassHold, 0);
}

void PlaceCallDispatch::failed()
{
	log_error(ZONE, "Place call for %s failed", participant->get_id());
}

void GetPortsDispatch::execute(RPCResultSet * res)
{
    on_get_ports_result(m_session, res);
}

void GetPortsDispatch::failed()
{
	log_error(ZONE, "GetServers failed");

    on_get_ports_result(m_session, NULL);
}

void ResetPortDispatch::execute(RPCResultSet * res)
{
    on_reset_port_result(m_session, res);
}

void ResetPortDispatch::failed()
{
	log_error(ZONE, "GetServers failed");

    on_reset_port_result(m_session, NULL);
}

void HealthDispatch::execute(s2s_t s2s, nad_t nad)
{
    int root = 0, attr = 0, elem = 0;
    char name[32], service_id[32], location[32], available[32], lasthost[32], usercount[32];
    int status = Failed;

    // form an rpc style reply
    xmlrpc_env env;
    xmlrpc_env_init(&env);

    xmlrpc_value *sample_array = NULL;
    xmlrpc_value *sample_xml   = NULL;
    xmlrpc_value *output       = xmlrpc_build_value(&env, "()");
    xmlrpc_value *val;

    log_status(ZONE, "Processing health result");

    // process <RESULT><SERVICES> tags
    root = nad_find_elem(nad, 0, "result", 2);
    if (root > 0)
    {
        root = nad_find_child_elem(nad, root, "services", 1);
        if (root > 0)
        {
            sample_array = xmlrpc_build_value(&env, "()");

            // loop through services forming something else
            elem = nad_find_child_elem(nad, root, "service", 1);
            while (elem > 0)
            {
                sample_xml = xmlrpc_build_value(&env, "()");

                // process name, id, location, available attrs
                if ((attr = get_attribute(nad, elem, "name", name, 32)) < 0)
                    goto cleanup;

                val = xmlrpc_build_value(&env, "s", name);
                xmlrpc_array_append_item(&env, sample_xml, val);
                xmlrpc_DECREF(val);
                val = NULL;

                if ((attr = get_attribute(nad, elem, "id", service_id, 32)) < 0)
                    goto cleanup;

                val = xmlrpc_build_value(&env, "s", service_id);
                xmlrpc_array_append_item(&env, sample_xml, val);
                xmlrpc_DECREF(val);
                val = NULL;

                if ((attr = get_attribute(nad, elem, "location", location, 32)) < 0)
                    goto cleanup;

                val = xmlrpc_build_value(&env, "s", location);
                xmlrpc_array_append_item(&env, sample_xml, val);
                xmlrpc_DECREF(val);
                val = NULL;

                if ((attr = get_attribute(nad, elem, "lasthost", lasthost, 32)) < 0)
                    goto cleanup;

                val = xmlrpc_build_value(&env, "s", lasthost);
                xmlrpc_array_append_item(&env, sample_xml, val);
                xmlrpc_DECREF(val);
                val = NULL;

                if ((attr = get_attribute(nad, elem, "available", available, 32)) < 0)
                    goto cleanup;

                val = xmlrpc_build_value(&env, "s", available);
                xmlrpc_array_append_item(&env, sample_xml, val);
                xmlrpc_DECREF(val);
                val = NULL;

                xmlrpc_array_append_item(&env, sample_array, sample_xml);
                xmlrpc_DECREF(sample_xml);
                sample_xml = NULL;

                elem = nad_find_elem(nad, elem, "service", 0);
            } //while

			// Loop through jabberd peers
            elem = nad_find_child_elem(nad, root, "peer", 1);
            while (elem > 0)
            {
                sample_xml = xmlrpc_build_value(&env, "()");

                val = xmlrpc_build_value(&env, "s", "peer");
                xmlrpc_array_append_item(&env, sample_xml, val);
                xmlrpc_DECREF(val);
                val = NULL;

				if ((attr = get_attribute(nad, elem, "usercount", usercount, 32)) < 0)
					goto cleanup;

                val = xmlrpc_build_value(&env, "s", usercount);
                xmlrpc_array_append_item(&env, sample_xml, val);
                xmlrpc_DECREF(val);
                val = NULL;

                if ((attr = get_attribute(nad, elem, "location", location, 32)) < 0)
                    goto cleanup;

                val = xmlrpc_build_value(&env, "s", location);
                xmlrpc_array_append_item(&env, sample_xml, val);
                xmlrpc_DECREF(val);
                val = NULL;

                val = xmlrpc_build_value(&env, "s", "");
                xmlrpc_array_append_item(&env, sample_xml, val);
                xmlrpc_DECREF(val);
                val = NULL;

                if ((attr = get_attribute(nad, elem, "available", available, 32)) < 0)
                    goto cleanup;

                val = xmlrpc_build_value(&env, "s", available);
                xmlrpc_array_append_item(&env, sample_xml, val);
                xmlrpc_DECREF(val);
                val = NULL;

                xmlrpc_array_append_item(&env, sample_array, sample_xml);
                xmlrpc_DECREF(sample_xml);
                sample_xml = NULL;

                elem = nad_find_elem(nad, elem, "peer", 0);
            } //while

            output = xmlrpc_build_value(&env, "(V)", sample_array);
            xmlrpc_DECREF(sample_array);
            sample_array = NULL;

            status = Ok;
        }
    }

cleanup:
    if (status == Failed || env.fault_occurred)
    {
        rpc_send_fault(m_session, env.fault_code, env.fault_string);
    }
    else
    {
        rpc_send_result(m_session, output);
    }

    xmlrpc_DECREF(output);
    output = NULL;

    xmlrpc_env_clean(&env);

    return;
}

// retrieve the attribute corresponding to the passed key
// returns > 0 if OK, < 0 if failed
int HealthDispatch::get_attribute(nad_t nad, int elem, const char* key, char* value, int max_size)
{
    int attr = 0;
    int len = 0;
    int result = -1;

	attr = nad_find_attr(nad, elem, key, NULL);
    if (attr >= 0)
    {
        len = NAD_AVAL_L(nad, attr);
        if (len < max_size)
        {
            memset(value, 0, max_size);
            strncpy(value, NAD_AVAL(nad, attr), len);
            result = 1;
        }
    }

    return attr;
}
