/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include <time.h>
#include "../../util/iobject.h"
#include "../../util/irefobject.h"
#include "../../util/istring.h"
#include "../../util/ithread.h"
#include "../../jcomponent/util/log.h"

class MeetParamPart : public IReferencableObject
{
    IString m_screen_name;
    IString m_display_name;
    IString m_phone;
    IString m_email;
    unsigned m_im_type;
    IString m_im_screen_name;
    unsigned m_moderator;
    IMutex m_instance_lock;
    
  public:
    MeetParamPart(const char *screen_name, 
                  const char *display_name,
                  const char *phone,
                  const char *email,
                  unsigned im_type,
                  const char *im_screen_name,
                  unsigned moderator
          )
        {
            m_screen_name = screen_name;
            m_display_name = display_name;
            m_phone = phone;
            m_email = email;
            m_im_type = im_type;
            m_im_screen_name = im_screen_name;
            m_moderator = moderator;
        };

    ~MeetParamPart()
        {
            log_debug(ZONE, "MeetParamPart(%s,%s) deleted",
                      (const char *)m_screen_name,
                      (const char *)m_display_name);
        };

    enum { TYPE_PGROUP_PART = 3465 };

    const char * get_screen_name() const
        {
            return (const char *)m_screen_name;
        };
    
    const char * get_display_name() const
        {
            return (const char *)m_display_name;
        };

    const char * get_phone() const
        {
            return (const char *)m_phone;
        };

   const char * get_email() const
        {
            return (const char *)m_email;
        };

    unsigned get_im_type() const
        {
            return m_im_type;
        };

    const char * get_im_screen_name() const
        {
            return (const char *)m_im_screen_name;
        };

    unsigned get_moderator() const
        {
            return m_moderator;
        };

   virtual int type() { return TYPE_PGROUP_PART; };
        
    virtual void lock() { m_instance_lock.lock(); };
    virtual void unlock() { m_instance_lock.unlock(); };
    	
    virtual unsigned int hash()
        { return m_screen_name.hash() ^ m_display_name.hash() ; }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() &&
              m_screen_name == ((MeetParamPart*)obj)->m_screen_name &&
              m_display_name == ((MeetParamPart*)obj)->m_display_name;
        };
};

#define PGROUP_LIFETIME	1800 /* number of seconds until group expires */

class MeetParam : public IReferencableObject
{
    IString m_id;
    IString m_title;
    IString m_description;
    IString m_message;
    IString m_password;
    int m_schedule_time;
    IString m_forum_token;
    unsigned m_forum_options;
    unsigned m_options_set;
    unsigned m_options_clear;
    time_t  m_creation;
    IList   m_plist;
    IMutex  m_instance_lock;

   public:
    MeetParam(const char * id);
    ~MeetParam();
    
    const char * get_id() { return (const char *)m_id; };
    enum { TYPE_PGROUP = 3466 };
    virtual int type() { return TYPE_PGROUP; };
        
    virtual void lock() { m_instance_lock.lock(); };
    virtual void unlock() { m_instance_lock.unlock(); };
    	
    virtual unsigned int hash() { return m_id.hash() ; }
    virtual bool equals(IObject *obj)
        {
            return obj->type() == type() && m_id == ((MeetParam*)obj)->m_id;
        };
    
    void add_participant(const char *screen_name, 
                         const char *display_name,
                         const char *phone,
                         const char *email,
                         unsigned im_type,
                         const char *im_screen_name,
                         unsigned moderator);

    void set_iterator(IListIterator & iter);
    bool expired(time_t now) { return (m_creation + PGROUP_LIFETIME) < now; }

    void set_title(const char *title) { m_title = title; }
    const char * get_title() { return (const char *)m_title; }
    
    void set_description(const char *description) { m_description = description; }
    const char * get_description() { return (const char *)m_description; }
    
    void set_message(const char *message) { m_message = message; }
    const char * get_message() { return (const char *)m_message; }

    void set_password(const char *password) { m_password = password; }
    const char * get_password() { return (const char *)m_password; }

    void set_schedule_time(int schedule_time) { m_schedule_time = schedule_time; }
    int get_schedule_time() { return m_schedule_time; }

    void set_forum_token(const char *forum_token) { m_forum_token = forum_token; }
    const char * get_forum_token() { return (const char *)m_forum_token; }

    void set_forum_options(unsigned forum_options) { m_forum_options = forum_options; }
    unsigned get_forum_options() { return m_forum_options; }

    void set_options_set(unsigned options_set) { m_options_set = options_set; }
    unsigned get_options_set() { return m_options_set; }

    void set_options_clear(unsigned options_clear) { m_options_clear = options_clear; }
    unsigned get_options_clear() { return m_options_clear; }
};

typedef IRefObjectPtr<MeetParam> spMeetParam;

class MeetParamTable
{
private:
	IList m_pglist;
	IHash m_pgtable;
    IMutex m_instance_lock;
	static unsigned int m_seq;
	static IMutex m_id_lock;

    /* generate a new participant group id */
	static void mk_id(char *id);
	
public:
    MeetParamTable() { };
    ~MeetParamTable() { };

    /* Adds a participant group; use get_id() method to get its id */
	void add_group(spMeetParam & pg);

    /* Gets the participant group indicated by the id */
    void get_group(spMeetParam & pg, const char *id);

    /* Checks for expired pgroups and dereferences them */
	void check_expire();

    /* Number of participant groups contained */
    size_t size() { return m_pglist.size(); }
};
