/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef EVENT_H
#define EVENT_H

#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>
#include "../../util/irefobject.h"
#include "../../util/istring.h"
#include "../../util/ithread.h"
#include "../common/common.h"

#define NUM_DISPATCH_THREADS 1

class Meeting;
class SubMeeting;
class Participant;

// RPC formats
//
// Event only...send meeting id and source:
// 	 "SENDTYPE", "EVENT", "MEETINGID", "PARTICIPANTID"
//
// Meeting state...send row set:
//   First row describes event and meeting:
//   "SENDTYPE", "EVENT", "MEETINGID", "HOSTID", "TYPE", "PRIVACY", "PRIORITY", "TITLE", "SIZE", "DURATION", "TIME", "RECURRENCE", "OPTIONS"
//   "END_TIME", "INVITE_EXPIRATION", "INVITE_VALID_BEFORE", "INVITE_MESSAGE", "DISPLAY_ATTENDEES", "INVITEE_CHAT_OPTIONS"
//   "ENABLED_FEATURES", "MEETING_INSTANCE_TIME", "BRIDGE_ADDRESS", "BRIDGE_PHONE"
//	 For each submeeting:
//   "SUBMEETINGID", "ISMAIN", "TITLE"
//   Row for each participant in submeeting:
// 	 "PARTICIPANTID", "SUBMEETINGID", "ROLL", "USERID", "NAME", "PHONE", "EMAIL", "SCREENNAME", "STATUS"
//
// Participant state:
//   "SENDTYPE", "EVENT", "MEETINGID", "PARTICIPANTID", "SUBMEETINGID", "ROLL", "USERID", "NAME", "PHONE", "EMAIL", "SCREENNAME", "STATUS",
//
// Submeeting state:
//   "SENDTYPE", "EVENT", "MEETINGID", "SUBMEETINGID", "ISMAIN", "TITLE"
//
// Invite:
//   "SENDTYPE", "EVENT", "MEETINGID", "HOSTID", "TYPE", "PRIVACY", "PRIORITY", "TITLE", "SIZE", "DURATION", "TIME", "RECURRENCE", "OPTIONS", 
//   "FROMNAME", "MESSAGE", "INVITETYPE"
//    
// App Share:
//   "SENDTYPE", "EVENT", "MEETINGID", "PRESENTERID", "ADDRESS", "PASSWORD", 
//   "STATUS"
//
// Doc share:
//   "SENDTYPE", "EVENT", "MEETINGID", "PRESENTERID", "ADDRESS", "DOCID", "PAGEID", "Options", "DRAWING"
//
// Chat message:
//   "SENDTYPE", "EVENT", "MEETINGID", "FROMID", "TOTYPE", "MESSAGE"
//
// PPT share:
//   "SENDTYPE", "EVENT", "MEETINGID", "ACTION"


struct Event
{
	int event;
    int send_type;
    int to_type;
	IString message;

    IRefObjectPtr<Meeting> meeting;
    IRefObjectPtr<SubMeeting> submeeting;
    IRefObjectPtr<Participant> source;	// if event is about single participant, this is the participant
    IRefObjectPtr<Participant> target;	// if event is to be sent to one participant, this is him

    // for doc share
    IString doc_id;
    IString page_id;
    int options;
    IString drawings;
    
    Event(int event, int send_type, int to_type, Meeting * meeting, SubMeeting * sm, Participant * source, Participant * target, const char * message, int invite_type);

    // for doc share
    Event(int event, int send_type, int to_type, Meeting * meeting, SubMeeting * sm, 
		const char* doc_id, const char* page_id, int options, const char* drawings);

    ~Event();

};


class EventDispatch
{
	IThreadPool * pool;	

public:
	EventDispatch();
	~EventDispatch();

	void startup();
	void shutdown();
	
    void send(int event, int send_type, int to_type, Meeting * meeting, SubMeeting * sm, Participant * source, Participant * target, const char * message = NULL, int invite_type = 0);

    // for doc share
    void send(int event, int send_type, int to_type, Meeting * meeting, SubMeeting * sm, 
		const char* doc_id, const char* page_id, int options, const char* drawings);

	xmlrpc_value * get_meeting_info(xmlrpc_env * env, Event * event, bool meeting_only, Participant * recipient);
	xmlrpc_value * get_participant_info(xmlrpc_env * env, Event * event);
	xmlrpc_value * get_submeeting_info(xmlrpc_env * env, Event *event);
	xmlrpc_value * get_invite_info(xmlrpc_env * env, Event *event);
	xmlrpc_value * get_app_share_info(xmlrpc_env * env, Event *event);
	xmlrpc_value * get_doc_share_info(xmlrpc_env * env, Event *event);
	xmlrpc_value * get_chat_message(xmlrpc_env * env, Event *event);
	xmlrpc_value * get_ppt_share_action(xmlrpc_env * env, Event *event);

    bool send_meeting_state(xmlrpc_env * env, Event *event);
	void send_participant_state(xmlrpc_env * env, Event * event, xmlrpc_value * msg);
	void send_to_one(xmlrpc_env * env, Event * event, Participant * p, xmlrpc_value * msg);
	void send_to_host(xmlrpc_env * env, Event * event, xmlrpc_value * msg);
	void send_to_many(xmlrpc_env * env, Event * event, xmlrpc_value * msg);

	static void dispatch(void *data, void *pool_ptr);
};


extern EventDispatch dispatcher;

#endif
