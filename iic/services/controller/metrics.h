/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

/*
 * [ METRICS RECORD FORMAT ]
 * 
 * CSV text file, one line per measurement. Each server that stores metrics records them to a local file.
 * 
 * Format per line: "Type", "Ave", "Peak", "Time of Measurement"
 *
 * Types:
 *   Meetings:          0
 *   Ports:             1
 *   Users:             2
 *   Share Sessions:    3
 *
 */

#ifndef METRICS_H
#define METRICS_H

#include <time.h>
#ifdef LINUX
#include <sys/param.h>
#define _MAXPATHLEN MAXPATHLEN
#else
#include <stdlib.h>
#define _MAXPATHLEN _MAX_PATH
#endif

#include "../../util/iobject.h"
#include "../../util/istring.h"
#include "../../util/irefobject.h"
#include "../common/common.h"

typedef enum {
    SampleTypeInit          = 0,
    SampleTypeMeetings      = 1,
    SampleTypePorts         = 2,
    SampleTypeUsers         = 3,
    SampleTypeShareSessions = 4,
} SampleType;
 

#define SAMPLING_FREQUENCY 60       // once per minute
#define SAMPLING_INTERVAL  30       // average across 30 minutes
#define MAX_SAMPLES_CACHED 48*7     // 1 week's worth of 1/2 hour samples
#define SECS_PER_DAY       60*60*24

class SampleLog;

// a single Sample representative of a number of measurements in a sample interval
class Sample : public IReferencableObject
{
    double m_ave;      // average value across interval
    int m_total;       // running total of measurement
    int m_peak;        // largest peak per period
    int m_count;       // number of samples taken this period
    int m_last_time;   // time of last sample

    IString m_sample_id;

public:
	Sample(const char* id);

	void record(int value, int current_time);
	void rotate();

    double  get_ave()  { return m_ave; }
    int     get_peak() { return m_peak; }
    int     get_time() { return m_last_time; }

    void lock() {};
    void unlock() {};

    enum { TYPE_SAMPLE = 3481 };
    virtual int type()
        { return TYPE_SAMPLE; }
    virtual unsigned int hash()
        { return m_sample_id.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && m_sample_id == ((Sample*)obj)->m_sample_id; }

};

// a list of Samples cached in memory
// - filled in at startup from a SampleLog file
// - performs write through on every sample at sample rotation time
class Samples
{
    bool        m_started;
    Sample*     m_current_sample;
    int         m_sample_count;
    IList       m_samples;
    SampleLog*  m_logger;

public:
    Samples();
    ~Samples();

    void set_logger(SampleLog* logger) { m_logger = logger; }
    void started(bool started);
	void record(int value, int current_time);
	void rotate();
    void get_samples(IListIterator & iter);
};

//
// a file log of Samples
// 
class SampleLog
{
private:
    int    m_my_type;
    bool   m_init;
    time_t m_next_rotation;
    int    m_rotation_interval;
    char   m_filename_prefix[_MAXPATHLEN];
    char   m_filename[_MAXPATHLEN];
    char   m_hostname[256];

  public:
    /*
     * Log is written in <directory>/<basename>.elg
     *
     * Rotated log files are named as:
     *	<directory>/<basename>+<timestamp>.elg
     *
     * Computes the initial record id from the IP addr and construction
     * time.
     *
     * Computes the next rotation time using a default of midnight.
     */
    SampleLog();
    ~SampleLog();

	void initialize(SampleType type);

    /*
     * Set base directory and name for file
     */
    int set_location(const char *directory,	/* base directory for metrics files */
                     const char *basename);	/* base name for metrics files */

    /*
     * Set number of days between rotations.  Samples are expected to be
     * stored every 1/2 hour, creating just 48 samples a day.  This value is
     * expected to be 30 days.
     */
    void set_rotation_interval(int days = 1);

    /*
     * Changes the time when logs are rotated.  Rotations are done
     * every 24hrs at the given number of minutes since midnight GMT
     */
    void set_rotation_time(int minutes_since_midnight /* GMT */);
    
    /*
     * This method checks to see if log rotation is necessary.
     *
     * If the next rotation time has passed, rename <directory>/<basename>.elg
     * to <directory>/<basename>+<timestamp>.elg
     */
    int check_rotation();

    /*
     * Write init to the file
     */
    int log_init();

    /*
     * Log a sample to the file.
     */
    int log_sample(Sample* sample);  
};

#endif
