
#include "controller.h"

extern "C"
{
#include "rpc.h"
}

extern Controller master;
extern EventDispatch dispatch;

int main()
{
	master.register_service("test", "iic:test2");
	return 0;
}
