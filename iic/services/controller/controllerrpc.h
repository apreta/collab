/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef CONTROLLERRPC_H
#define CONTROLLERRPC_H

#include "../../jcomponent/rpc.h"
#include "../../jcomponent/util/rpcresult.h"

class User;
class Meeting;
class Participant;
class Request;

// Isolate RPC specific code into separate layer.  Probably should wrap in classes.
// (e.g. RemoteSchedule, RemoteService, Broadcast, etc.).

int log_event_start(int event, const char * community_id, const char * meeting_id, time_t actual_start,
					const char * host_id, const char * part_id, const char * info);
void log_event_end(int event_handle);
int log_get_start_time(int event_handle);

void build_get_servers_result(xmlrpc_env * env, xmlrpc_value * output,
                              int voice_avail, int appshare_avail);

void on_get_servers_result(void *session, int appshare_avail, int voice_avail);

void on_get_ports_result(void *session, RPCResultSet * res);

void on_reset_port_result(void *session, RPCResultSet * res);

int ping_appshare(const char * host, int port);

#endif
