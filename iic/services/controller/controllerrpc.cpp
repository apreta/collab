/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 *
 *
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */

/* Implementation for controller APIs */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <util.h>

#ifdef _WIN32
#include <windows.h>
#include <winsock2.h>
#endif

#ifdef LINUX
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#endif

#include <locale.h>
#include <libintl.h>

#include "../../jcomponent/jcomponent.h"
#include "../../jcomponent/jcomponent_ext.h"
#include "../../jcomponent/rpc.h"
#include "../../jcomponent/util/rpcresult.h"
#include "../../jcomponent/util/eventlog.h"
#include "../../jcomponent/util/nadutils.h"
#include "../../jcomponent/util/log.h"
#include "../../jcomponent/util/port.h"
#include "../../jcomponent/presence.h"
#include "../../util/istring.h"
#include "../../util/ithread.h"
#include "../common/address.h"
#include "../common/common.h"
#include "../../util/base64/Base64.h"
#include "../../util/blowfish/blowfish.h"
#ifndef OSS
#include "../common/license.inl"
#endif
#include "controller.h"
#include "meeting.h"
#include "appshare.h"
#include "controllerrpc.h"
#include "phonenumber.h"
#include "apidispatch.h"
#include "transport.h"
#include "heartbeat.h"
#include "metrics.h"
#include "meetparam.h"

/* Contains APIs exposed through Jabber XMLRPC mechanism.  The intention is to keep
   these functions as small as possible, primarily parsing arguments before handing
   off the request to the controller object.

   Notes on identifiers

   Ids start with prefix that indicates type of id:
	user:xx			- user id (xx matches userid in users table)
	anon:xx			- user id for anonymous user
	part:xx	 		- participant id (xx matches participantid in participants table)
	submeting:xx	- submeeting id
	voice:xx		- voice address
	iic:jid			- iic address (jabber id)
	pin:xx			- participant id or meeting id (look up in db to resolve)
	xx				- meeting id (currently has no prefix)

	Presence statuses:
	iic:voice		- session is a voice session (also has resource "/voice")
	iic:meeting		- session is in a meeting
	iic:			- no IIC-specific presence

*/

#define JCOMPNAME "controller"

int get_config(config_t conf);

static bool running = false;
static int controller_epoch = time(NULL);
static int sample_time = time(NULL);

EventLog event_log;
IMutex event_mutex;

int controller_init(config_t conf)
{
    log_status(ZONE, "Starting meeting controller");

    IThread::initialize();

	PhoneNumber::initialize("/opt/iic/conf/dialing.xml");

	event_log.initialize(EvnLog_Type_Call);

    master.initialize();

	// Read configuration
	int status = get_config(conf);
	if (status)
		return status;

    event_log.log_reset();

	dispatcher.startup();

    heartbeat.startup();

    const char *zonlang = getenv("ZONLANG");
    if (zonlang == NULL)
        zonlang = "";	// use system default

    log_debug(ZONE, "Setting locale to %s", zonlang);

    // initialize locale and timezone for display purposes (email generation)
    setlocale( LC_ALL, zonlang );
    tzset();

	bindtextdomain(JCOMPNAME, "/opt/iic/locale");
    textdomain(JCOMPNAME);
    char *pBTC = bind_textdomain_codeset(JCOMPNAME, "UTF-8");
    log_debug(ZONE, "Codeset is %s", pBTC);

    return 0;
}

int get_config(config_t conf)
{
    char * param_val;

    master.set_server_name(config_get_one(conf, JCOMPNAME, "iicServer", 0));
    master.set_webstart_url(config_get_one(conf, JCOMPNAME, "webStartUrl", 0));

    param_val = config_get_one(conf, JCOMPNAME, "eventLogDir", 0);
    event_log.set_location((param_val ? param_val : "."), "calls");

    param_val = config_get_one(conf, JCOMPNAME, "eventRotationTime", 0);
    event_log.set_rotation_time((param_val ? atoi(param_val) : 0));

    param_val = config_get_one(conf, JCOMPNAME, "metricsLogDir", 0);
    master.set_location((param_val ? param_val : "."));

    param_val = config_get_one(conf, JCOMPNAME, "metricsRotationInterval", 0);
    master.set_rotation_interval((param_val ? atoi(param_val) : 0));

    param_val = config_get_one(conf, JCOMPNAME, "metricsRotationTime", 0);
    master.set_rotation_time((param_val ? atoi(param_val) : 0));

    int nsvr = config_count(conf, JCOMPNAME, "shareServers.server.ip");
    int i;
    for ( i = 0; i < nsvr; i++)
    {
        char *ip = config_get_one(conf, JCOMPNAME, "shareServers.server.ip", i);
        char *port = config_get_one(conf, JCOMPNAME, "shareServers.server.port", i);
        appshare.add_server(ip, atoi(port));
    }

    nsvr = config_count(conf, JCOMPNAME, "docServers.server.url");
	if (nsvr>0)
	{
		for ( i = 0; i < nsvr; i++)
		{
			char * url = config_get_one(conf, JCOMPNAME, "docServers.server.url", i);
			char * username = config_get_one(conf, JCOMPNAME, "docServers.server.username", i);
			char * password = config_get_one(conf, JCOMPNAME, "docServers.server.password", i);
	        docshare.add_server(url, username, password);
		}
	}
	else
	{
		docshare.add_server(config_get_one(conf, JCOMPNAME, "docShareServer", 0), "anonymous", "password");
	}

    master.set_voice_provider(config_get_one(conf, JCOMPNAME, "voiceProvider", 0));
    param_val = config_get_one(conf, JCOMPNAME, "voicePCPhone", 0);
    if (strcmp(param_val, "y") == 0)
    	master.set_pcphone_type(VOIPAvailable);
    else if (strcmp(param_val, "e") == 0)
    	master.set_pcphone_type(VOIPAvailable|VOIPEncrypted);
    else
    	master.set_pcphone_type(VOIPAvailable|0);

    nsvr = config_count(conf, JCOMPNAME, "mtgArchivers.service");
    for ( i = 0; i < nsvr; i++)
    {
        master.add_mtg_archiver(config_get_one(conf, JCOMPNAME, "mtgArchivers.service", i));
    }

    nsvr = config_count(conf, JCOMPNAME, "bridgePhones.bridge.service");

    bool mp = false;
    if (nsvr > 0)
    {
        char * mp_str = config_get_one(conf, JCOMPNAME, "multiPhoneBridges", 0);
        mp = (mp_str != NULL) && (strcmp(mp_str, "yes") == 0);
        master.set_multi_phone(mp);
        for ( i = 0; i < nsvr; i++)
        {
            char * bridge_service = config_get_one(conf, JCOMPNAME, "bridgePhones.bridge.service", i);
            char * bridge_voip = config_get_one(conf, JCOMPNAME, "bridgePhones.bridge.voipAddress", i);
            master.add_bridge(bridge_service, bridge_voip);
            int j = 0;
            char * tsvphones = config_get_one(conf, JCOMPNAME, "bridgePhones.bridge.phonelist", i);
            gchar ** phones = g_strsplit(tsvphones, "~", -1 /* no limit to # of splits */);
            for (j = 0; phones[j] != NULL; j++)
            {
                master.add_bridge_phone(phones[j], bridge_service);
            }
            g_strfreev(phones);
        }
    }

#ifndef OSS
    if (LoadLicense("/opt/iic/conf/license-key.xml"))
    {
        return Failed;
    }
#endif

    return Ok;
}

char *i_itoa(int i, char *buffer)
{
    sprintf(buffer, "%d", i);
    return buffer;
}


// Get string value from xmlrpc value, optionally checking that string
// only contains alphanumeric characters.
char *get_string_value(xmlrpc_env *env, xmlrpc_value *val, bool validate)
{
    char *pstr = NULL;
    xmlrpc_parse_value(env, val, "s", &pstr);
    XMLRPC_FAIL_IF_FAULT(env);

    if (validate)
    {
        char *p2 = pstr;
        for (;*p2;p2++)
        {
            if (!isalnum(*p2) && *p2 != '.')
            {
                xmlrpc_env_set_fault(env, ParameterParseError, "Error parsing function input:  illegal characters in field name");
                goto cleanup;
            }
        }
    }

cleanup:
    return pstr;
}


// Create xmlrpc array from list of strings.
xmlrpc_value *create_array(int count, const char **values)
{
    xmlrpc_env env;
	xmlrpc_value *array = NULL;
	xmlrpc_value *member = NULL;
	int i;

    xmlrpc_env_init(&env);

	array = xmlrpc_build_value(&env, "()");
    XMLRPC_FAIL_IF_FAULT(&env);

	for (i=0; (count > 0 && i < count) || (count == 0 && values[i]) ; i++)
	{
		member = xmlrpc_build_value(&env, "s", values[i]);
		XMLRPC_FAIL_IF_FAULT(&env);

		xmlrpc_array_append_item(&env, array, member);
		XMLRPC_FAIL_IF_FAULT(&env);

		xmlrpc_DECREF(member);
		member = NULL;
	}

cleanup:
	if (env.fault_occurred)
	{
		if (array != NULL)
			xmlrpc_DECREF(array);
		array = NULL;

		if (member != NULL)
			xmlrpc_DECREF(member);
	}

	return array;
}

bool check_is_from_service(void *rpc_session)
{
    const char * from_jid = rpc_get_from((rpc_session_t)rpc_session);
    if (strstr(from_jid, ".service@"))
        return true;
    return false;
}

// Check if Jabber user id matches session id
// Return true if this request if from an internal system service, false if user session.
bool check_session(xmlrpc_env *env, void *rpc_session, const char *session_id)
{
    // Allow trusted system services
    if (check_is_from_service(rpc_session))
        return true;

    // Use full jabber id for anonymous users; just user@server for registered users
    const char * from_jid = rpc_get_from((rpc_session_t)rpc_session);
    if (strncmp(from_jid, ANONYMOUS_NAME, ANONYMOUS_NAME_LEN) == 0)
        from_jid = rpc_get_from_full((rpc_session_t)rpc_session);

    if (session_id == NULL)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "API not available for user sessions");
        return false;
    }

    spUser user = master.find_user_by_address(from_jid);
    if (user == NULL )
    {
        xmlrpc_env_set_fault(env, InvalidSessionID, "Session ID doesn't match XMPP session - User == NULL");
    }
    else if (user == NULL || strcmp(user->get_id(), session_id))
    {
        xmlrpc_env_set_fault(env, InvalidSessionID, "Session ID doesn't match XMPP session");
    }

    return false;
}


void load_admins()
{
    // Load user records for admins using well known ids
    spUser admin = master.create_user("user:2");
    admin->load(NULL);
    spUser useradmin = master.create_user("user:3");
    useradmin->load(NULL);
}

// ====
// External controller APIs
// ====

// Register service.
// Parameters:
//   SERVICE_ID, EPOCH
// Returns:
//   0
xmlrpc_value *register_service(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *service_id;
	int epoch;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(si)", &service_id, &epoch);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.register_service");
        goto cleanup;
    }

    check_session(env, user_data, NULL);
    XMLRPC_FAIL_IF_FAULT(env);

    // register service...
    status = master.register_service(service_id, epoch);
    if (status != Ok)
    {
        xmlrpc_env_set_fault(env, status, "Unable to register service");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", 0);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to register service");

    return output;
}

// Unregister service.
// Parameters:
//   ADDRESS
// Returns:
//   1 if service was unregisterd
xmlrpc_value *unregister_service(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *address;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(s)", &address);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.unregister_service");
        goto cleanup;
    }

    check_session(env, user_data, NULL);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.unregister_service(address);
    if (status != Ok && status !=  InvalidServiceID)
    {
        xmlrpc_env_set_fault(env, status, "Unable to unregister service");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status == Ok ? 1 : 0);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to unregister service");

    return output;
}


// Ping controller
// Parameters:
//   SESSIONID
// Returns:
//   SUCCESS_CODE: 1 = ok, 2 = sessionid not recognized
xmlrpc_value *ping(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid;
    xmlrpc_value *output = NULL;
	IRefObjectPtr<User> user;
    int status;

    xmlrpc_parse_value(env, param_array, "(s)", &sessionid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.ping");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    // verify session id
    user = master.find_user(sessionid);
    if (user != NULL && user->get_client() != NULL)
    {
        status = PingResultOK;
        user->set_last_ping_time(time(NULL));
    }
    else
        status = PingResultInvalidSession;

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Ping failure");

    return output;
}


// Register user.  Currently session id will be same as user id, but this may change.  If meeting id is
// specified, that meeting is joined.
// Notes:
//   - Determines if first parameter is user id or pin by address prefix (user: or pin:)
//   - If pin is used, we lookup and convert to user id.
// Parameters:
//   USERID/PIN, USERNAME (if known), NAME, ADDRESS (iic:JABBER ID for client, voice:CALLID for voice), MEETINGID,
//   NETWORK_ADDRESS (empty for GUI, calling number for inbound voice call, called number for outbound voice call)
// Returns:
//   SESSIONID, PARTICIPANTID, WEBSTARTURL, status (ok, pending, retry)
xmlrpc_value *register_user(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *userid, *username, *name, *address, *meetingid, *network_address;
    const char *email;
    xmlrpc_value *output = NULL;
	spUser user;
	int status = Ok;
	const char *partid = NULL;
	spMeeting meeting;
	spParticipant participant;
	const char *from_jid;

    xmlrpc_parse_value(env, param_array, "(ssssss)", &userid, &username, &name, &address, &meetingid, &network_address);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.register_user");
        goto cleanup;
    }
	email = "";		// *** change param list

    if (!check_is_from_service(user_data))
    {
        // Compare API specified username with actual jabber username
        from_jid = rpc_get_from((rpc_session_t)user_data);
        if (strncmp(from_jid, ANONYMOUS_NAME, ANONYMOUS_NAME_LEN) == 0)
            from_jid = rpc_get_from_full((rpc_session_t)user_data);

        if (strcmp(username, from_jid))
        {
            xmlrpc_env_set_fault(env, NotAuthorized, "Wrong username specified");
            goto cleanup;
        }
    }
    else
    {
        // The resource part of username only makes sense if it came from a client connection
        char *res = strrchr(username, '/');
        if (res)
            *res = '\0';
    }

    if (!master.is_voice_available())
    {
        // Hack: after controller start, we don't get notified that we're active until
        // someone sends us a message...so we may simply need to wait until we're full
        // active so find a voice server.
#ifdef LINUX
		struct timespec tv = { 0, 100 * 1000000 };
		nanosleep(&tv, NULL);
#endif

        if (!master.is_voice_available())
        {
            xmlrpc_env_set_fault(env, Failed, "Voice service not available");
            goto cleanup;
        }
    }

	if (get_address_type(userid) == AddressPIN)
	{
		// Caller gave us a pin--attempt to validate.
		char *pin = strchr(userid, ':') + 1;

		status = master.find_pin(pin, meeting, participant);
		if (status == InvalidPIN)
		{
			// Save info from this call.
			RegisterDispatch * disp = new RegisterDispatch(user_data, userid, username, name, email, address, network_address);

			char id[32];
			const char * dispatch_id = dispatch_queue.add_dispatch(disp);
			sprintf(id, "dispatch:%s", dispatch_id);

			if (rpc_make_call(id, "addressbk", "schedule.find_pin", "(ss)", "sys:controller", pin))
			{
				xmlrpc_env_set_fault(env, DBAccessError, "Unable to lookup pin in schedule db");
				goto cleanup;
			}

			// Result will be returned when db call completes.
			output = RPC_PENDING;
			goto cleanup;
		}
		else
		{
			// If we found a participant, use the same user id.
			// Otherwise controller will create an anonymous id.
			if (participant != NULL)
			{
				IString addr;
				int media_type = get_media_type(address);

				status = master.find_participant_address(participant->get_id(), media_type, addr);
				if (status == NotConnected || (media_type == MediaData && addr == address))
				{
                    // New tactic:  keep user id as 'pin:xxx'

					// Participant not connected with this media type yet, so go ahead
					partid = participant->get_id();
				}
				else
				{
					// Someone already used this pin to enter meeting, create new pin for
					// this one
					log_debug(ZONE, "User for PIN %s already connected, creating new pin\n", pin);
					userid = NULL;
					partid = NULL;
				}
			}
			else
			{
				// Must have been meeting pin.
				log_debug(ZONE, "Registered with meeting pin %s, creating new pin\n", pin);
				userid = NULL;
				partid = NULL;
			}
		}
    }
    else if (get_address_type(userid) == AddressAnonUser)
    {
        // Make sure this user still exists
        User* user = master.find_user(userid);
		if (user == NULL)
			goto cleanup;
    }

	status = master.register_client(userid, username, name, address, network_address, user);
	if (status != Ok)
	{
		xmlrpc_env_set_fault(env, status, "Unable to register client");
		goto cleanup;
	}

	if (meetingid != NULL && strlen(meetingid) > 0)
	{
		status = master.join_meeting(user->get_id(), address, meetingid, NULL, NULL);
		if (IS_FAILURE(status))
		{
			log_error(ZONE, "Client registered, but unable to join desired meeting\n");
			xmlrpc_env_set_fault(env, status, "Unable to join meeting");
			goto cleanup;
		}
	}

	// Create return value.
	output = xmlrpc_build_value(env, "(sssi)", user->get_id(), partid != NULL ? partid : "",
					master.get_webstart_url(), status);
	XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to register client");

    return output;
}


// Unregister IIC client.  If meeting id is specified, and this is a voice address, a final call
// progress is reported.
// Parameters:
//    SESSIONID, ADDRESS, MEETINGID
// Returns:
//    1 if session was removed
xmlrpc_value *unregister_user(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *address, *meetingid;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(sss)", &sessionid, &address, &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.unregister_client");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

	if (*meetingid != '\0' && get_media_type(address) == MediaVoice)
	{
	    master.call_progress(sessionid, meetingid, address, 0);
	}

    status = master.unregister_client(sessionid, address);
    if (status != Ok && status != InvalidSessionID)
    {
        xmlrpc_env_set_fault(env, status, "Unable to unregister client");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status == Ok ? 1 : 0);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to remove user session");

    return output;
}


// Start meeting.  Meeting info is loaded from schedule database.
// Parameters:
//   SESSIONID, MEETINGID
// Returns:
//   MEETINGID, status (ok, pending, retry)
xmlrpc_value *start_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *key=NULL;
    xmlrpc_value *output = NULL;
    int status;

    int num_params = xmlrpc_array_size(env, param_array);
	if (num_params==3)
		xmlrpc_parse_value(env, param_array, "(sss)", &sessionid, &meetingid, &key);
	else
		xmlrpc_parse_value(env, param_array, "(ss*)", &sessionid, &meetingid);

    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.start_meeting");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.start_meeting(sessionid, meetingid, key);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to start meeting");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(si)", meetingid, status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to start meeting");

    return output;
}


// Create (instant) meeting.
// Notes:
//  - The meeting id must be in schedule assigned to this user; all other settings can be overriden.
//  - Be careful to use participant IDs assigned by scheduler, or leave blank and let controller create new IDs.
// Parameters:
//   SESSIONID, MEETINGID, HOSTID, TYPE, PRIVACY, START_TIME, TITLE, DESCRIPTION, SIZE, DURATION, PASSWORD, OPTIONS
//   END_TIME, INVITE_EXPIRATION, INVITE_VALID_BEFORE, INVITE_MESSAGE, DISPLAY_ATTENDEES, INVITEE_CHAT_OPTIONS
//   PARTICIPANT_LIST (array):
//	   PARTICIPANTID, USERID, USERNAME, int ROLL, NAME, SEL_PHONE, PHONE, SEL_EMAIL, EMAIL, SCREEN, DESCRIPTION,
//     int NOTIFYTYPE, MESSAGE, CALLOPTIONS
// Returns:
//   status (ok, pending, retry)
xmlrpc_value *create_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	char *sessionid, *meetingid, *hostid, *title, *descrip, *password, *invite_message, *key;
	int type, privacy, start_time, meeting_size, duration, options;
    int end_time, invite_expiration, invite_valid_before, display_attendees, invitee_chat_options;
	xmlrpc_value *participants;
	IList * participant_list;
    xmlrpc_value *output = NULL;
	int status = Ok;
	int size;
	int i;

    int num_params = xmlrpc_array_size(env, param_array);
	if (num_params==19)
	    xmlrpc_parse_value(env, param_array, "(sssiiissiisiiiisiiA)", &sessionid, &meetingid, &hostid, &type, &privacy, &start_time, &title, &descrip,
			&meeting_size, &duration, &password, &options, &end_time, &invite_expiration, &invite_valid_before, &invite_message, &display_attendees, &invitee_chat_options, &participants);
	else
	    xmlrpc_parse_value(env, param_array, "(sssiiissiisiiiisiiAs*)", &sessionid, &meetingid, &hostid, &type, &privacy, &start_time, &title, &descrip,
			&meeting_size, &duration, &password, &options, &end_time, &invite_expiration, &invite_valid_before, &invite_message, &display_attendees, &invitee_chat_options, &participants, &key);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.create_meeting");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    size = xmlrpc_array_size(env, participants);
    XMLRPC_FAIL_IF_FAULT(env);

	// FIXME: participant_list may get lost if an error occurs
	participant_list = new IList;

    for (i=0; i<size; i++)
    {
    	char *partid, *description, *message;
		int roll, notify_type, sel_phone, sel_email, call_options;
		char *user_id, *username, *name, *phone, *email, *screen;

        xmlrpc_value *val = xmlrpc_array_get_item(env, participants, i);
        XMLRPC_FAIL_IF_FAULT(env);

		xmlrpc_parse_value(env, val, "(sssisisisssisi)", &partid, &user_id, &username, &roll, &name, &sel_phone, &phone,
					&sel_email, &email, &screen, &description, &notify_type, &message, &call_options);
		if (env->fault_occurred)
		{
			xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.create_meeting");
			goto cleanup;
		}

		spUser target_user = master.create_user(user_id, username, name);
		if (target_user == NULL)
		{
			xmlrpc_env_set_fault(env, InvalidUserID, "Invalid user id received by controller.create_meeting");
			goto cleanup;
		}

		Participant * p = new Participant(partid, NULL, target_user, ParticipantInvitee, roll, name, sel_phone, phone, sel_email, email, screen, description, notify_type, call_options);
		participant_list->add(p);
		p->dereference();
	}

	status = master.create_meeting(sessionid, meetingid, type, privacy, start_time, title, descrip,
						meeting_size, duration, password, options, end_time, invite_expiration, invite_valid_before,
                        invite_message, display_attendees, invitee_chat_options, participant_list, key);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to create meeting");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

	// *** To do:
	// 1.  Change register user to load user info
	// 2.  Change load user to also get user profile like max conf size & duration
	// 3.  Lookup specified meeting PIN, make sure it belongs to this user
	// 4.  Validate participant ids from here against schedule info; only allow participant pin to be specified if matches

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to create meeting");

    return output;
}


// Modify settings for a meeting in progress
// Notes:
//  - User must be host or moderator.
// Parameters:
//   SESSIONID, MEETINGID, HOSTID, TYPE, PRIVACY, PRIORITY, TITLE, DESCRIPTION, PASSWORD, OPTIONS
//   INVITE_EXPIRATION, INVITE_VALID_BEFORE, INVITE_MESSAGE, DISPLAY_ATTENDEES, INVITEE_CHAT_OPTIONS
// Returns:
//   status (ok, pending)
xmlrpc_value *modify_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	spMeeting meeting;
	char *sessionid, *meetingid, *hostid, *title, *descrip, *password, *invite_message;
	int type, privacy, priority, options, invite_expiration, invite_valid_before, display_attendees, invitee_chat_options;
    xmlrpc_value *output = NULL;
	int status = Ok;

    xmlrpc_parse_value(env, param_array, "(sssiiisssiiisii)", &sessionid, &meetingid, &hostid, &type, &privacy, &priority, &title, &descrip,
                       &password, &options, &invite_expiration, &invite_valid_before, &invite_message, &display_attendees, &invitee_chat_options);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.modify_meeting");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

	status = master.modify_meeting(sessionid, meetingid, type, privacy, priority, title, descrip,
						password, options, invite_expiration, invite_valid_before,
                        invite_message, display_attendees, invitee_chat_options);

    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to modify meeting");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to modify meeting");

    return output;
}

// Modify settings for recording
// Notes:
//  - User must be host or moderator.
// Parameters:
//   SESSIONID, MEETINGID, COMMAND
// Returns:
//   status (ok, pending)
xmlrpc_value *modify_recording(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	spMeeting meeting;
	char *sessionid, *meetingid, *email;
	int cmd;
    xmlrpc_value *output = NULL;
	int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ssis)", &sessionid, &meetingid, &cmd, &email);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.modify_recording");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

	status = master.modify_recording(sessionid, meetingid, cmd, email);

    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to modify recording");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to modify recording");

    return output;
}

// Toggle lecture mode
// Notes:
//  - User must be host or moderator.
// Parameters:
//   SESSIONID, MEETINGID
//
// Returns:
//   status (ok, pending), lecture mode state
xmlrpc_value *toggle_lecture_mode(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	spMeeting meeting;
	char *sessionid, *meetingid;
    xmlrpc_value *output = NULL;
	int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ss)", &sessionid, &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError,
                             "Unable to parse arguments to controller.toggle_lecture_mode");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    int mode_on;
	status = master.toggle_lecture_mode(sessionid, meetingid, &mode_on);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to toggle lecture mode");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(ii)", status, mode_on);
    XMLRPC_FAIL_IF_FAULT(env);

  cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to toggle lecture mode");

    return output;
}

// Toggle hold mode
// Notes:
//  - User must be host or moderator.
// Parameters:
//   SESSIONID, MEETINGID
//
// Returns:
//   status (ok, pending), hold mode state
xmlrpc_value *toggle_hold_mode(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	spMeeting meeting;
	char *sessionid, *meetingid;
    xmlrpc_value *output = NULL;
	int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ss)", &sessionid, &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError,
                             "Unable to parse arguments to controller.toggle_hold_mode");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    int mode_on;
	status = master.toggle_hold_mode(sessionid, meetingid, &mode_on);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to toggle hold mode");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(ii)", status, mode_on);
    XMLRPC_FAIL_IF_FAULT(env);

  cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to toggle hold mode");

    return output;
}

// Toggle record meeting
// Notes:
//  - User must be host or moderator.
// Parameters:
//   SESSIONID, MEETINGID
//
// Returns:
//   status (ok, pending), recording state
xmlrpc_value *toggle_record_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	spMeeting meeting;
	char *sessionid, *meetingid;
    xmlrpc_value *output = NULL;
	int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ss)", &sessionid, &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError,
                             "Unable to parse arguments to controller.toggle_record_meeting");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    int mode_on;
	status = master.toggle_record_meeting(sessionid, meetingid, &mode_on);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to toggle record meeting");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(ii)", status, mode_on);
    XMLRPC_FAIL_IF_FAULT(env);

  cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to toggle record meeting");

    return output;
}

// Enable data recording
// Notes:
//  - User must be host or moderator.
// Parameters:
//   SESSIONID, MEETINGID, ENABLED
//
// Returns:
//   status (ok, pending), recording state
xmlrpc_value *enable_data_recording(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    spMeeting meeting;
    char *sessionid, *meetingid;
    int enabled;
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ssi)", &sessionid, &meetingid, &enabled);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError,
                             "Unable to parse arguments to controller.enable_data_recording");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.enable_data_recording(sessionid, meetingid, enabled);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to enable data recording");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to enable data recording");

    return output;
}

// Toggle mute join/leave
// Notes:
//  - User must be host or moderator.
// Parameters:
//   SESSIONID, MEETINGID
//
// Returns:
//   status (ok, pending), recording state
xmlrpc_value *toggle_mute_join_leave(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	spMeeting meeting;
	char *sessionid, *meetingid;
    xmlrpc_value *output = NULL;
	int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ss)", &sessionid, &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError,
                             "Unable to parse arguments to controller.toggle_mute_join_leave");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    int mode_on;
	status = master.toggle_mute_join_leave(sessionid, meetingid, &mode_on);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to toggle mute join leave");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(ii)", status, mode_on);
    XMLRPC_FAIL_IF_FAULT(env);

  cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to toggle mute join leave");

    return output;
}


// Lock meeting
// Notes:
//  - User must be host or moderator.
// Parameters:
//   SESSIONID, MEETINGID, STATE (0: unlock meeting, 1: lock meeting)
//
// Returns:
//   status (ok, pending), lock state
xmlrpc_value *lock_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	spMeeting meeting;
	char *sessionid, *meetingid;
    int state;
    xmlrpc_value *output = NULL;
	int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ssi)", &sessionid, &meetingid, &state);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError,
                             "Unable to parse arguments to controller.lock_meeting");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

	status = master.lock_meeting(sessionid, meetingid, state);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to change meeting lock state");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(ii)", status, state);
    XMLRPC_FAIL_IF_FAULT(env);

  cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to change meeting lock state");

    return output;
}


// End meeting.
// Parameters:
//   SESSIONID, MEETINGID
// Returns:
//   status (ok)
xmlrpc_value *end_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid;
    xmlrpc_value *output = NULL;
    spMeeting meeting;
    int status;

    xmlrpc_parse_value(env, param_array, "(ss)", &sessionid, &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.end_meeting");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.end_meeting(sessionid, meetingid);
    if (status != Ok)
    {
        xmlrpc_env_set_fault(env, status, "Unable to end meeting");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to end meeting");

    return output;
}


// Add/modify list of participants.
// Parameters:
//   SESSIONID, MEETINGID
//   PARTICIPANT_LIST (array):
//	   PARTICIPANTID, USERID, USERNAME, int ROLL, NAME, SEL_PHONE, PHONE, SEL_EMAIL, EMAIL, SCREEN, DESCRIPTION,
//     int NOTIFYTYPE, MESSAGE, CALLOPTIONS
// Returns:
//   status (ok, pending, retry)
xmlrpc_value *add_participants(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	char *sessionid, *meetingid;
	xmlrpc_value *participants;
	IList * participant_list;
    xmlrpc_value *output = NULL;
	int status = Ok;
	int size;
	int i;

    xmlrpc_parse_value(env, param_array, "(ssA)", &sessionid, &meetingid, &participants);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.create_meeting");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    size = xmlrpc_array_size(env, participants);
    XMLRPC_FAIL_IF_FAULT(env);

	// FIXME: participant_list may get lost if an error occurs
	participant_list = new IList;

    for (i=0; i<size; i++)
    {
    	char *partid, *description, *message;
		int roll, notify_type, sel_phone, sel_email, call_options;
		char *user_id, *username, *name, *phone, *email, *screen;

        xmlrpc_value *val = xmlrpc_array_get_item(env, participants, i);
        XMLRPC_FAIL_IF_FAULT(env);

		xmlrpc_parse_value(env, val, "(sssisisisssisi)", &partid, &user_id, &username, &roll, &name, &sel_phone, &phone,
					&sel_email, &email, &screen, &description, &notify_type, &message, &call_options);
		if (env->fault_occurred)
		{
			xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.add_participants");
			goto cleanup;
		}

		spUser target_user = master.create_user(user_id, username, name);
		if (target_user == NULL)
		{
			xmlrpc_env_set_fault(env, InvalidUserID, "Invalid user id received by controller.add_participants");
			goto cleanup;
		}

		Participant * p = new Participant(partid, NULL, target_user, ParticipantInvitee, roll, name, sel_phone, phone, sel_email, email, screen, description, notify_type, call_options);
		participant_list->add(p);
		p->dereference();
	}

	status = master.add_participants(sessionid, meetingid, participant_list);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to add participants");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);


cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to add participants");

    return output;
}

// Add participant to meeting.
// Notes:
//  - Suceeds if participant is already in meeting.
//  - If particpant is present, but in a different submeeting, participant is moved.
//  - Notify, phone, and email options can be modified for an existing participant.
// Parameters:
//   SESSIONID, MEETINGID, SUBMEETINGID, USERID, USERNAME, ROLL, NAME, SEL_PHONE, PHONE, SEL_EMAIL, EMAIL, SCREEN, DESCRIPTION,
//   NOTIFY_TYPE, MESSAGE, CALL_TYPE, EXPIRES
// Returns:
//   "", status (ok)
xmlrpc_value *add_participant(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meeting_id, *sm_id, *description, *message;
    int roll, notify_type, sel_phone, sel_email;
	int call_type, expires;
    char *user_id, *username, *name, *phone, *email, *screen;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(sssssisisisssisii)", &sessionid, &meeting_id, &sm_id, &user_id, &username,
                       &roll, &name, &sel_phone, &phone, &sel_email, &email, &screen, &description, &notify_type,
					   &message, &call_type, &expires);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.add_participant");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.add_participant(sessionid, meeting_id, sm_id, user_id, username, roll, name,
                                    sel_phone, phone, sel_email, email, screen, description,
                                    notify_type, message, call_type, expires);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to add participant to meeting");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(si)", "", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to add participant");

    return output;
}


// Remove participant from meeting.
// Parameters:
//   SESSIONID, MEETINGID, PARTICIPANTID
// Returns:
//   status (ok)
xmlrpc_value *remove_participant(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meeting_id, *participant_id;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(sss)", &sessionid, &meeting_id, &participant_id);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.remove_participant");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.remove_participant(sessionid, meeting_id, participant_id);
    if (status != Ok)
    {
        xmlrpc_env_set_fault(env, status, "Unable to remove participant from meeting");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to remove participant");

    return output;
}


// Call participant.
// Notes:
//  - If user is on phone and in this meeting, no new call is placed.
//  - If user is on phone in another meeting, and he calls this API, he joins the meeting and his phone call is moved.
// Parameters:
//   SESSIONID, MEETINGID, PARTICIPANTID, TARGETSELECTEDPHONE, TARGETPHONE, OPTIONS
// Returns:
//   status (ok, pending, retry)
xmlrpc_value *call_participant(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meeting_id, *part_id;
	char *targetphone;
	int targetselphone, options;
    xmlrpc_value *output = NULL;
    spParticipant p;
    int status;

    xmlrpc_parse_value(env, param_array, "(sssisi)", &sessionid, &meeting_id, &part_id, &targetselphone, &targetphone, &options);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.call_participant");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.call_participant(sessionid, meeting_id, part_id, targetselphone, targetphone, options);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to call participant");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to call participant");

    return output;
}


// Modify participant.
// Notes:
//  - Currently is the SAME as add participant -- will add or modify participant
//  - If NotifyPhone is not specified, participant is not called.
//  - If participant is already present, call portion is still completed.
//  - If participant is already present, but a different submeeting is specified, the participant is moved before the call is placed.
//  - See additional notes in add participant and call participant APIs.
// Parameters:
//   SESSIONID, MEETINGID, SUBMEETINGID, USERID, USERNAME, ROLL, NAME, SELPHONE, PHONE, SELEMAIL, EMAIL, SCREEN, DESCRIPTION, NOTIFYTYPE, MESSAGE,
//   CALLOPTIONS, EXPIRES
// Returns:
//   "",status (ok, pending, retry)
xmlrpc_value *modify_participant(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meeting_id, *sm_id, *description, *message;
    int roll, notify_type, sel_phone, sel_email;
    char *user_id, *username, *name, *phone, *email, *screen;
	int options, expires;
    xmlrpc_value *output = NULL;
    spParticipant p;
	spUser user;
    int status;

    xmlrpc_parse_value(env, param_array, "(sssssisisisssisii)", &sessionid, &meeting_id, &sm_id, &user_id, &username,
                       &roll, &name, &sel_phone, &phone, &sel_email, &email, &screen, &description, &notify_type, &message,
					   &options, &expires);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.modify_participant");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.add_participant(sessionid, meeting_id, sm_id, user_id, username, roll, name,
						sel_phone, phone, sel_email, email, screen, description,
						notify_type, message, options, expires);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to add participant to meeting");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(si)", "", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to add participant");

    return output;
}


// Re-invite participant to meeting.
// Notes:
//  - Participant must already be in meeting roster.
// Parameters:
//   SESSIONID, MEETINGID, PARTICIPANTID, NOTIFYTYPE, MESSAGE
// Returns:
//   status (ok)
xmlrpc_value *notify_participant(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meeting_id, *part_id, *message;
    int notify_type;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(sssis)", &sessionid, &meeting_id, &part_id, &notify_type, &message);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.notify_participant");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.notify_participant(sessionid, meeting_id, part_id, notify_type, message);
    if (status != Ok)
    {
        xmlrpc_env_set_fault(env, status, "Unable to notify participant to meeting");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to notify participant");

    return output;
}


// Add submeeting
// Parameters:
//   SESSIONID, MEETINGID, TITLE (ignored), EXPECTEDPARTICIPANTS
// Returns:
//   SUBMEETINGID
xmlrpc_value *add_submeeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *title;
	int expected_participants;
    xmlrpc_value *output = NULL;
	spSubMeeting sm;
    int status;

    xmlrpc_parse_value(env, param_array, "(sssi)", &sessionid, &meetingid, &title, &expected_participants);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.add_submeeting");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.add_submeeting(sessionid, meetingid, expected_participants, sm);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to add submeeting");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(s)", sm ? sm->get_id() : "");
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to add submeeting");

    return output;
}


// Remove submeeting.
// Notes:
//  - Either all participants are moved to main submeeting (if REMOVE_PARTS is false)
//    or all participants except for the caller are removed from the meeting and just the caller is
//    moved to the main meeting.
//
// Parameters:
//   SESSIONID, MEETINGID, SUBMEETINGID, REMOVE_PARTS
// Returns:
//   status
xmlrpc_value *remove_submeeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *smid;
    xmlrpc_value *output = NULL;
    int status;
    int remove_parts;

    xmlrpc_parse_value(env, param_array, "(sssi)", &sessionid, &meetingid, &smid, &remove_parts);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.remove_submeeting");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.remove_submeeting(sessionid, meetingid, smid, remove_parts != 0);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to add submeeting");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to remove submeeting");

    return output;
}


// Move participant
// Parameters:
//   SESSIONID, MEETINGID, PARTICIPANTID, SUBMEETINGID
// Returns:
//   status
xmlrpc_value *move_participant(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *partid, *smid;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(ssss)", &sessionid, &meetingid, &partid, &smid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.move_participant");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.move_participant(sessionid, meetingid, partid, smid);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to move participant");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to move participant");

    return output;
}


// Join meeting.
// Notes:
//  - PIN is optional, can be participant id or meeting id.
//  - If user is in another meeting, he will be removed from that meeting first.
//  - If participant is connected to another user, we attempt to merge this user with
//    that user so he shows up as a single meeting participant.
//  - Leave meeting id & pin blank to use values user registered with
//    (external users)
// Parameters:
//   SESSIONID, ADDRESS, MEETINGID, PIN, PASSWORD
// Returns:
//   "" status (ok, pending, retry)
xmlrpc_value *join_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *address, *meetingid, *pin, *password;
    xmlrpc_value *output = NULL;
	IString partid;
	IString dispatchid;
	spMeeting meeting;
	spParticipant part;
    int status;

    xmlrpc_parse_value(env, param_array, "(sssss)", &sessionid, &address, &meetingid, &pin, &password);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.join_meeting");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.join_meeting(sessionid, address, meetingid, pin, password);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to join meeting");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(si)", "", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to join meeting");

    return output;
}


// Leave meeting.
// Parameters:
//   SESSIONID, ADDRESS, MEETINGID
// Returns:
//   status (ok, pending, retry)
xmlrpc_value *leave_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *address, *meetingid;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(sss)", &sessionid, &address, &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.leave_meeting");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.leave_meeting(sessionid, address, meetingid);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to leave meeting");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to leave meeting");

    return output;
}


// Reload meeting from schedule if currently active.
// Parameters:
//   SESSIONID, MEETINGID
// Returns:
//   status (ok, pending, retry)
xmlrpc_value *reload_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(ss)", &sessionid, &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.reload_meeting");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.reload_meeting(sessionid, meetingid);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to reload meeting");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to reload meeting");

    return output;
}


// Lookup PIN.
// Notes:
//   - PIN can be meeting id or participant id.
// Parameters:
//   SESSIONID, PIN[, OPTIONS]
// Returns:
//   "FOUND", "MEETINGID", "PARTICIPANTID", "USERID", "EMAIL",
//   "USERNAME", "NAME", "OPTIONS", "SCHEDULED_TIME", "INVITE_VALID_BEFORE",
//   "INVITE_EXPIRATION", "TYPE", "INVITED_TIME"
//   FOUND: 1 = meeting found, 2 = meeting and user found, 0 = not found
xmlrpc_value *find_pin(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *pin;
    int options = 0;
    int role = RollNormal;
    xmlrpc_value *output = NULL;
    const char *out_encode;
	spMeeting meeting;
	spParticipant participant;
    int status;

    int params = xmlrpc_array_size(env, param_array);
    XMLRPC_FAIL_IF_FAULT(env);

    if (params < 3) {
        xmlrpc_parse_value(env, param_array, "(ss)", &sessionid, &pin);
    } else {
        xmlrpc_parse_value(env, param_array, "(ssi)", &sessionid, &pin, &options);
    }
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.find_pin");
        goto cleanup;
    }

    // Option 1: include participant role in output
    out_encode = options & 1 ? "(issssssiiiiiii)" : "(issssssiiiiii)";

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.find_pin(pin, meeting, participant);
	if (status == InvalidPIN)
	{
		// Can't find it here...must look up in db
		// bad_ptr
		char id[32];
		sprintf(id, "pin:%ld", (long)user_data);

		if (rpc_make_call(id, "addressbk", "schedule.find_pin", "(ssi)", "sys:controller", pin, options))
		{
			xmlrpc_env_set_fault(env, DBAccessError, "Unable to lookup pin in schedule db");
			goto cleanup;
		}

		output = RPC_PENDING;
	}
	else
	{
		//   "FOUND", "MEETINGID", "PARTICIPANTID", "USERID", "EMAIL", "USERNAME", "NAME", "OPTIONS"
		//   FOUND: 1 = meeting found, 2 = meeting and user found, 0 = not found
		if (participant != NULL && participant->get_submeeting())
		{
			const char *userid;
			spUser user = participant->get_user();
			if (get_address_type(user->get_id()) == AddressUser)
				userid = user->get_id();
			else
				userid = "-1";
			meeting = participant->get_meeting();
		    role = participant->get_roll();
			output = xmlrpc_build_value(env, out_encode, 2, meeting->get_id(), participant->get_id(), userid,
						participant->get_email(), participant->get_user()->get_username(), participant->get_user()->get_name(),
						meeting->get_options(), meeting->get_start(), meeting->get_invite_valid_before(), meeting->get_invite_expiration(),
                        participant->get_participant_type(), 0, role);
		}
		else if (participant == NULL)
		{
			// Anonymous meeting pin
			output = xmlrpc_build_value(env, out_encode, 1, meeting->get_id(), "", "", "", "", "", meeting->get_options(), 0, 0, 0, 0, 0, role);
		}
		else
		{
			// PIN was found, but was for participant removed from meeting.
			output = xmlrpc_build_value(env, out_encode, 0, "", "", "", "", "", "", 0, 0, 0, 0, 0, 0, role);
		}

		XMLRPC_FAIL_IF_FAULT(env);
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to find PIN information");

    return output;
}


// Lookup Invite info
// Notes:
//   - If invite is anonymous (personal pins not generated for meeting), set PIN to empty string.
//   - If PIN is specified, we must find a matching participant or it is rejected
// Parameters:
//   SESSIONID, MID, PIN[, OPTIONS]
// Returns:
//   "FOUND", "MEETINGID", "TITLE", "TIME", "HOST", "SERVER", "PARTICIPANTID", "SCREENNAME", "NAME", "OPTIONS",
//   "DESCRIPTION", "MEETING INVITATION", "PASSWORD", "PARTICIPANT PHONE", "PARTICIPANT EMAIL"
//   FOUND: 1 = meeting found, 2 = meeting and user found, 0 = not found
xmlrpc_value *find_invite(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *mid, *pin;
    int options = 0;
    xmlrpc_value *output = NULL;
    const char *out_encode;
	spMeeting meeting;
	spParticipant participant;
    int status;

    int params = xmlrpc_array_size(env, param_array);
    XMLRPC_FAIL_IF_FAULT(env);

    if (params < 4) {
        xmlrpc_parse_value(env, param_array, "(sss)", &sessionid, &mid, &pin);
    } else {
        xmlrpc_parse_value(env, param_array, "(sssi)", &sessionid, &mid, &pin, &options);
    }
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.find_invite");
        goto cleanup;
    }

    // Option 1: include participant role in output
    out_encode = options & 1 ? "(issssssssisssssi)" : "(issssssssisssss)";

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

	if (*pin == '\0')
	{
		// No pin specified, lookup meeting id.
		meeting = master.find_meeting(mid);
		if (meeting != NULL)
		{
			char time_str[32];
			i_itoa(meeting->get_start(), time_str);

			// Found matching meeting
			output = xmlrpc_build_value(env, out_encode, 1,
                                        meeting->get_id(),
                                        meeting->get_title(),
                                        time_str,
                                        meeting->get_host()->get_name(),
                                        master.get_server_name(),
                                        "", ANONYMOUS_NAME, "",
                                        meeting->get_options(),
                                        meeting->get_description(),
                                        meeting->get_invite_message(),
                                        meeting->get_password(),
                                        "", "",
                                        meeting->get_type());
			XMLRPC_FAIL_IF_FAULT(env);
		}
	}
	else
	{
		// Lookup by pin
	    status = master.find_pin(pin, meeting, participant);
		if (status != Ok && status != InvalidPIN)
			goto cleanup;

		if (meeting != NULL)
		{
			// Found meeting, make sure it belongs to meeting we think it should.
			if (*mid != '\0' && strcmp(meeting->get_id(), mid) != 0)
			{
				xmlrpc_env_set_fault(env, InvalidPIN, "Specified pin does not match meeting");
				goto cleanup;
			}

            // get start time
            char time_str[32];
			i_itoa(meeting->get_start(), time_str);

            IString displayname;
			IString username;
			IString pin;
            IString email;
            IString phone;

            // Include participant info if available
            if (participant != NULL)
            {
                // We used to retrieve participant screenname, but
                // screenname stores the name the host provides in the case
                // of an adhoc invite involving IM.
                // We now retrieve the underlying user and parse IIC screen name from username.
                username = participant->get_user()->get_username();
                int pos = username.find("@");
                if (pos > 0)
                    username.erase(pos, username.length() - pos);

                // Get pin
                if (strncmp(participant->get_id(), "part:", 5) == 0)
                    pin = participant->get_id() + 5;

                email = participant->get_email();
                phone = participant->get_phone();
                displayname = participant->get_invited_name();

            }
            if (username.length() == 0)
                username = ANONYMOUS_NAME;

	        // "FOUND", "MEETINGID", "TITLE", "TIME", "HOST", "SERVER", "PARTICIPANTID", "SCREENNAME", "NAME", "OPTIONS",
            // "DESCRIPTION", "MEETING INVITE", "PASSWORD", "PARTICIPANT PHONE", "PARTICIPANT EMAIL"
			// FOUND: 1 = meeting found, 2 = meeting and user found, 0 = not found
			output = xmlrpc_build_value(env, out_encode, 2,
										meeting->get_id(),
										meeting->get_title(),
										time_str,
										meeting->get_host()->get_name(),
										master.get_server_name(),
										pin.get_string(),
										username.get_string(),
										displayname.get_string(),
										meeting->get_options(),
                                        meeting->get_description(),
                                        meeting->get_invite_message(),
                                        meeting->get_password(),
                                        phone.get_string(),
                                        email.get_string(),
                                        meeting->get_type());
			XMLRPC_FAIL_IF_FAULT(env);
		}
	}

	if (output == NULL)
	{
		// Can't find it here...must look up in db
		// bad_ptr
		char id[32];
		sprintf(id, "invite:%ld", (long)user_data);

		if (rpc_make_call(id, "addressbk", "schedule.find_invite", "(sssi)", "sys:controller", mid, pin, options))
		{
			xmlrpc_env_set_fault(env, DBAccessError, "Unable to lookup invite in schedule db");
			goto cleanup;
		}

		output = RPC_PENDING;
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to find INVITE information");

    return output;
}


// Call progress information
// Parameters:
//   SESSIONID, MEETINGID, CALLID, int PROGRESS
// Returns:
//   0
xmlrpc_value *call_progress(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *callid;
	int progress;
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(sssi)", &sessionid, &meetingid, &callid, &progress);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.call_progress");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

	// We don't add this call to user's port list until call is exported.  In the mean time,
	// the call progress is stored in the participant object and broadcast to meeting.
    master.call_progress(sessionid, meetingid, callid, progress);

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to report call progress");

    return output;
}


// Disconnect participant
// Parameters:
//   SESSIONID, MEETINGID, PARTICIPANTID, MEDIATYPE
// Returns:
//   status (ok, pending, retry)
xmlrpc_value *disconnect_participant(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *participantid;
	int media_type;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(sssi)", &sessionid, &meetingid, &participantid, &media_type);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.disconnect_participant");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.disconnect_participant(sessionid, meetingid, participantid, media_type);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to disconnect participant");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to disconnect participant");

    return output;
}


// Set participant volume
// Notes:
//  - Use -1 to leave setting unchanged
// Parameters:
//   SESSIONID, MEETINGID, PARTICIPANTID, VOLUME (0-7), GAIN (0-7), MUTE (0,1,2)
// Returns:
//   status (ok, pending, retry)
xmlrpc_value *set_participant_volume(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *participantid;
	int volume, gain, mute;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(sssiii)", &sessionid, &meetingid, &participantid, &volume, &gain, &mute);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.set_participant_volume");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.set_participant_volume(sessionid, meetingid, participantid, user_data, volume, gain, mute);
    if (status != Ok)
    {
        goto cleanup;
    }

    return RPC_PENDING;

cleanup:
    if (IS_FAILURE(status))
        xmlrpc_env_set_fault(env, status, "Unable to set participant volume");

    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to set participant volume");

    return output;
}


// Get participant volume
// Parameters:
//   SESSIONID, MEETINGID, PARTICIPANTID
// Returns:
//   VOLUME, GAIN, MUTE, status (ok, pending, retry), PARTICIPANTID
xmlrpc_value *get_participant_volume(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *participantid;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(sss)", &sessionid, &meetingid, &participantid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.get_participant_volume");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.get_participant_volume(sessionid, meetingid, participantid, user_data);
    if (status != Ok)
    {
        goto cleanup;
    }

    return RPC_PENDING;

cleanup:
	if (IS_FAILURE(status))
        xmlrpc_env_set_fault(env, status, "Unable to get participant volume");

    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to get participant volume");

    return output;
}


// Start app share session.
// Parameters:
//   SESSIONID, MEETINGID, ADDRESS (ip::port), PASSWORD, SHARE_OPTIONS
// Returns:
//   MEETINGID, status (ok, pending, retry)
xmlrpc_value *start_app_share(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *address, *password;
    xmlrpc_value *output = NULL;
    spMeeting meeting;
    int status;
	int share_options;
	int share_starttime;

    xmlrpc_parse_value(env, param_array, "(ssssii)", &sessionid, &meetingid, &address, &password, &share_options, &share_starttime);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.start_app_share");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.start_app_share(sessionid, meetingid, address, password, share_options, share_starttime);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to start app share session");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to start app share session");

    return output;
}


// End app share session.
// Parameters:
//   SESSIONID, MEETINGID
// Returns:
//   MEETINGID, status (ok, pending, retry)
xmlrpc_value *end_app_share(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid;
    xmlrpc_value *output = NULL;
    spMeeting meeting;
    int status;

    xmlrpc_parse_value(env, param_array, "(ss)", &sessionid, &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.end_app_share");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.end_app_share(sessionid, meetingid);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to end app share session");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to end app share session");

    return output;
}


// Get a new (valid) app share server.
// Parameters:
//   SESSIONID, MEETINGID
// Returns:
//   status (ok)
xmlrpc_value *request_share_server(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid;
    xmlrpc_value *output = NULL;
    spMeeting meeting;
    int status;

    xmlrpc_parse_value(env, param_array, "(ss)", &sessionid, &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.request_share_server");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.request_share_server(sessionid, meetingid);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to get new server address");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to get new server address");

    return output;
}


// Change presenter
// Parameters:
//   SESSIONID, MEETINGID, PARTICIPANTID
// Returns:
//   status (ok, pending, retry)
xmlrpc_value *change_presenter(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *participantid;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(sss)", &sessionid, &meetingid, &participantid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.change_presenter");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.change_presenter(sessionid, meetingid, participantid);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to change presenter");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to change presenter");

    return output;
}


// Change participant handup
// Parameters:
//   SESSIONID, MEETINGID, PARTICIPANTID, HANDUP
// Returns:
//   status (ok, pending, retry), HANDUP
xmlrpc_value *set_handup(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *participantid;
	int handup;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(sssi)", &sessionid, &meetingid, &participantid, &handup);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.set_handup");
        goto cleanup;
    }

    status = master.set_handup(sessionid, meetingid, participantid, handup);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to set handup");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(ii)", status, handup);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to set handup");

    return output;
}


// Grant app share remote control
// Parameters:
//   SESSIONID, MEETINGID, PARTICIPANTID, GRANT
// Returns:
//   status (ok, pending, retry)
xmlrpc_value *grant_app_share_control(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *participantid;
	int grant;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(sssi)", &sessionid, &meetingid, &participantid, &grant);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.grant_app_share_control");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.grant_control(sessionid, meetingid, participantid, grant);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to grant app share control");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to grant app share control");

    return output;
}

// Start doc share session.
// Parameters:
//   SESSIONID, MEETINGID, ADDRESS (ip:port)
// Returns:
//   MEETINGID, status (ok, pending, retry)
xmlrpc_value *start_doc_share(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, * address;
    xmlrpc_value *output = NULL;
    spMeeting meeting;
    int status, share_starttime;

    xmlrpc_parse_value(env, param_array, "(sssi)", &sessionid, &meetingid, &address, &share_starttime);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.start_doc_share");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.start_doc_share(sessionid, meetingid, address, share_starttime);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to start doc share session");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to start doc share session");

    return output;
}


// End doc share session.
// Parameters:
//   SESSIONID, MEETINGID
// Returns:
//   status (ok, pending, retry)
xmlrpc_value *end_doc_share(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid;
    xmlrpc_value *output = NULL;
    spMeeting meeting;
    int status;

    xmlrpc_parse_value(env, param_array, "(ss)", &sessionid, &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.end_doc_share");
        goto cleanup;
    }

    status = master.end_doc_share(sessionid, meetingid);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to end doc share session");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to end doc share session");

    return output;
}

// get_doc_share_server.
// Parameters:
//   SESSIONID, MEETINGID
// Returns:
//   SERVER, status (ok, pending, retry)
xmlrpc_value *get_doc_share_server(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid;
    xmlrpc_value *output = NULL;
    spMeeting meeting;
    int status = Ok;
	const char* server;

    xmlrpc_parse_value(env, param_array, "(ss)", &sessionid, &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.get_doc_share_server");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    server = master.get_doc_share_server(meetingid);

    // Create return value.
    output = xmlrpc_build_value(env, "(is)", status, server ? server : "");
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to get_doc_share_server");

    return output;
}

// check_telephony_avail
// Parameters:
//   USERNAME
// Returns:
//   status (true, false)
xmlrpc_value *check_telephony_avail(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *userid;
    xmlrpc_value *output = NULL;
    const char* voice_provider;
    int avail = 0;

    xmlrpc_parse_value(env, param_array, "(s)", &userid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.check_telephony_avail");
        goto cleanup;
    }

    voice_provider = master.get_voice_provider();
    if (strlen(voice_provider) > 0 && strcmp(voice_provider, "stub") != 0)
    {
        avail = TelephonyAvailable | master.get_pcphone_type();
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", avail);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to check_telephony_avail");

    return output;
}

static MeetParamTable s_mptable;

//
// add_meeting_param
//
// Parameters:
//   SESSIONID
//   PARTICIPANTS (ARRAY (see below))
//   TITLE
//   DESCRIPTION
//   MESSAGE
//   PASSWORD
//   SCHEDULE_TIME
//   FORUM_TOKEN
//   FORUM_OPTIONS (bitmask, 1=set for schedule)
//   OPTIONS_SET
//   OPTIONS_CLEAR
//
// Each PARTICIPANTS array contains:
//   SCREENNAME
//   DISPLAYNAME
//   PHONE
//   EMAIL
//   IM_SYSTEM
//   IM_SCREENNAME
//   MODERATOR (0=Normal, 1=Moderator)
//
// Returns:
//   STATUS, PARAMTOKEN
//
xmlrpc_value *add_meeting_param(xmlrpc_env *env,
                                xmlrpc_value *param_array,
                                void *user_data)
{
    xmlrpc_value *output = NULL;
    int status = Ok;
    const char *paramtoken = "";
    char *sessionid;
    xmlrpc_value *participants;
    char *title, *description, *message, *password, *forum_token;
    int schedule_time;
    unsigned forum_options, options_set, options_clear;
    spMeetParam mparam;
    int i, size;

    xmlrpc_parse_value(env, param_array, "(sAssssisiii)",
                       &sessionid,
                       &participants,
                       &title,
                       &description,
                       &message,
                       &password,
                       &schedule_time,
                       &forum_token,
                       &forum_options,
                       &options_set,
                       &options_clear);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.add_meeting_param");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    size = xmlrpc_array_size(env, participants);
    XMLRPC_FAIL_IF_FAULT(env);

    s_mptable.add_group(mparam);
    if (size > 0)
    {
        for (i=0; i<size; i++)
        {
            char *screen_name, *display_name, *phone, *email, *im_screen_name;
            unsigned im_type, moderator;
            xmlrpc_value *row = xmlrpc_array_get_item(env, participants, i);
            XMLRPC_FAIL_IF_FAULT(env);
            xmlrpc_parse_value(env, row, "(ssssisi)",
                               &screen_name,
                               &display_name,
                               &phone,
                               &email,
                               &im_type,
                               &im_screen_name,
                               &moderator);
            XMLRPC_FAIL_IF_FAULT(env);

            mparam->add_participant(screen_name, display_name, phone, email, im_type, im_screen_name, moderator);
        }
    }
    mparam->set_title(title);
    mparam->set_description(description);
    mparam->set_message(message);
    mparam->set_password(password);
    mparam->set_schedule_time(schedule_time);
    mparam->set_forum_token(forum_token);
    mparam->set_forum_options(forum_options);
    mparam->set_options_set(options_set);
    mparam->set_options_clear(options_clear);

    paramtoken = mparam->get_id();

    output = xmlrpc_build_value(env, "(is)", status, paramtoken);

cleanup:
    mparam = NULL;

    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to check_meeting_running");

    return output;
}

//
// get_meeting_param
//
// Parameters:
//   SESSIONID, PARAMTOKEN
//
// Returns:
//   STATUS
//   PARTICIPANTS (ARRAY (see below))
//   TITLE
//   DESCRIPTION
//   MESSAGE
//   PASSWORD
//   SCHEDULE_TIME
//   FORUM_TOKEN
//   FORUM_OPTIONS (bitmask, 1=set for schedule)
//   OPTIONS_SET
//   OPTIONS_CLEAR
//
// Each PARTICIPANTS array contains:
//   SCREENNAME
//   DISPLAYNAME
//   PHONE
//   EMAIL
//   IM_SYSTEM
//   IM_SCREENNAME
//   MODERATOR (0=Normal, 1=Moderator)
//
xmlrpc_value *get_meeting_param(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *paramtoken;
    xmlrpc_value *participants;
    spMeetParam mparam;
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ss)", &sessionid, &paramtoken);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.create_meeting");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    s_mptable.get_group(mparam, paramtoken);

    participants = xmlrpc_build_value(env, "()");
    if (mparam != NULL)
    {
        IListIterator iter;
        mparam->set_iterator(iter);
        MeetParamPart *part = (MeetParamPart *)iter.get_first();
        while (part)
        {
            xmlrpc_value * row = xmlrpc_build_value(env, "(ssssisi)",
                                                    part->get_screen_name(),
                                                    part->get_display_name(),
                                                    part->get_phone(),
                                                    part->get_email(),
                                                    part->get_im_type(),
                                                    part->get_im_screen_name(),
                                                    part->get_moderator());
            xmlrpc_array_append_item(env, participants, row);
            xmlrpc_DECREF(row);
            row = NULL;

            part = (MeetParamPart *)iter.get_next();
        }
    }
    else
    {
        status = MeetParamNotFound;
    }

    output = xmlrpc_build_value(env, "(iVssssisiii)",
                                status,
                                participants,
                                (status == Ok ? mparam->get_title() : ""),
                                (status == Ok ? mparam->get_description() : ""),
                                (status == Ok ? mparam->get_message() : ""),
                                (status == Ok ? mparam->get_password() : ""),
                                (status == Ok ? mparam->get_schedule_time() : 0),
                                (status == Ok ? mparam->get_forum_token() : ""),
                                (status == Ok ? mparam->get_forum_options() : 0),
                                (status == Ok ? mparam->get_options_set() : 0),
                                (status == Ok ? mparam->get_options_clear() : 0));
    xmlrpc_DECREF(participants);
    participants = NULL;

cleanup:
    mparam = NULL;

    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to check_meeting_running");

    return output;
}

// download_document_page.
// Parameters:
//   SESSIONID, MEETINGID, DOCID, PAGEID
// Returns:
//   status (ok, pending, retry)
xmlrpc_value *download_document_page(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *docid, *pageid;
    xmlrpc_value *output = NULL;
    spMeeting meeting;
    int status;

    xmlrpc_parse_value(env, param_array, "(ssss)", &sessionid, &meetingid, &docid, &pageid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.download_document_page");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.download_document_page(sessionid, meetingid, docid, pageid);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to download_document_page");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to download_document_page");

    return output;
}

// configure_whiteboard.
// Parameters:
//   SESSIONID, MEETINGID, options
// Returns:
//   MEETINGID, status (ok, pending, retry)
xmlrpc_value *configure_whiteboard(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid;
    xmlrpc_value *output = NULL;
    spMeeting meeting;
	int options;
    int status;

    xmlrpc_parse_value(env, param_array, "(ssi)", &sessionid, &meetingid, &options);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.configure_whiteboard");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.configure_whiteboard(sessionid, meetingid, options);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to configure_whiteboard");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to configure_whiteboard");

    return output;
}

// draw_on_whiteboard.
// Parameters:
//   SESSIONID, MEETINGID, DRAWING
// Returns:
//   MEETINGID, status (ok, pending, retry)
xmlrpc_value *draw_on_whiteboard(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *drawing;
    xmlrpc_value *output = NULL;
    spMeeting meeting;
    int status;

    xmlrpc_parse_value(env, param_array, "(sss)", &sessionid, &meetingid, &drawing);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.configure_whiteboard");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.draw_on_whiteboard(sessionid, meetingid, drawing);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to draw_on_whiteboard");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to draw_on_whiteboard");

    return output;
}

// on_ppt_remote_control
// Parameters:
//   SESSIONID, MEETINGID, ACTION
// Returns:
//   MEETINGID, status (ok, pending, retry)
xmlrpc_value *on_ppt_remote_control(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *action;
    xmlrpc_value *output = NULL;
    spMeeting meeting;
    int status;

    xmlrpc_parse_value(env, param_array, "(sss)", &sessionid, &meetingid, &action);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.download_document_page");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.on_ppt_remote_control(sessionid, meetingid, action);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "on_ppt_remote_control failed");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "on_ppt_remote_control failed");

    return output;
}

// Grant moderator privilege
// Parameters:
//   SESSIONID, MEETINGID, PARTICIPANTID, GRANT
// Returns:
//   status (ok, pending, retry)
xmlrpc_value *grant_moderator_privilege(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *participantid;
	int grant;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(sssi)", &sessionid, &meetingid, &participantid, &grant);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.grant_moderator_privilege");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.grant_moderator(sessionid, meetingid, participantid, grant);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to grant moderator");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to grant moderator");

    return output;
}

// Play a roll call
// Parameters:
//   SESSIONID, MEETINGID
// Returns:
//   status (ok, pending, retry)
xmlrpc_value *roll_call(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(ss)", &sessionid, &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.roll_call");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.roll_call(sessionid, meetingid);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to play roll call");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to play roll call");

    return output;
}

// Place a call.
//
// Parameters:
//   SESSIONID, MEETINGID, CALLEEID, PHONENUMBER
// Returns:
//   status (ok, pending, retry)
xmlrpc_value *place_call(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *targetid, *dialnumber;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(ssss)", &sessionid, &meetingid, &targetid, &dialnumber);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.place_call");

        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.place_call(sessionid, meetingid, targetid, dialnumber);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to place call");

        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to place call");

    return output;
}

// Send bridge request--send custom info to voice bridge.
// Parameters:
//   SESSIONID, MEETINGID, PARTICIPANTID, CODE (0=clear request, any other=set request), MSG
// Returns:
//   status (ok, pending, retry)
xmlrpc_value *send_bridge_request(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *participantid, *msg;
	int code;
    xmlrpc_value *output = NULL;
    int status;

    xmlrpc_parse_value(env, param_array, "(sssis)", &sessionid, &meetingid, &participantid, &code, &msg);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.set_bridge_request");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.send_bridge_request(sessionid, meetingid, participantid, code, msg);
    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to set bridge request");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to set bridge request");

    return output;
}

// Bridge request progress
// Parameters:
//   SESSIONID, MEETINGID, CALLID, int RESULT, MSG
// Returns:
//   0
xmlrpc_value *bridge_request_progress(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *callid, *msg;
	int code;
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(sssis)", &sessionid, &meetingid, &callid, &code, &msg);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.bridge_request_progress");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    master.bridge_request_progress(sessionid, meetingid, callid, code, msg);

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to report bridge request progress");

    return output;
}

// Get meeting status
// Parameters:
//   SESSIONID, MEETINGID, OPTIONS
//   Options bit 1: dispatch voice.modify_submeeting for main submeeting
// Returns:
//   STATUS, MEETING STATE, MEETING OPTIONS, MAIN SUBMEETING OPTIONS
xmlrpc_value *get_meeting_status(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid;
	int options;
    xmlrpc_value *output = NULL;
    int status = Ok;
    int m_opt, sm_opt, state;

    xmlrpc_parse_value(env, param_array, "(ssi)", &sessionid, &meetingid, &options);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.get_meeting_status");
        goto cleanup;
    }

    check_session(env, user_data, NULL);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.get_meeting_status(sessionid, meetingid, options, state, m_opt, sm_opt);

    // Create return value.
    output = xmlrpc_build_value(env, "(iiii)", status, state, m_opt, sm_opt);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to report meeting status");

    return output;
}


// ====
// Real time monitoring APIs
// ====

//
// Get active meetings
//
// Parameters:
//   SESSIONID
//
// Results:
//   Array of:
//      MEETINGID, HOSTID, TYPE, PRIVACY, TITLE, DESCRIPTION,
//      DURATION, TIME, OPTIONS,
//      NUMBER_INVITED_PARTICIPANTS, NUMBER_CONNECTED_PARTICIPANTS
//      APPSHARE_SERVER, NUMBER_APPSHARE_SESSIONS,
//      VOICE_SERVER, NUMBER_CALLS, DOCSERVER
xmlrpc_value *get_meetings(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    xmlrpc_value *output = NULL;
    xmlrpc_value *mtgarray = NULL;
    xmlrpc_value *mtgxml = NULL;
    const char * sessionid;
    int status;
    int count = 0;

    IVector * mtgvec =  new IVector();
    IVectorIterator iter(*mtgvec);

    xmlrpc_parse_value(env, param_array, "(s)", &sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, NULL);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.get_active_meetings(sessionid, mtgvec);
    if (status != Ok)
    {
        xmlrpc_env_set_fault(env, status, "Failed");
        goto cleanup;
    }

    mtgarray = xmlrpc_build_value(env, "()");

    iter.set_vector(*mtgvec);

    Meeting * mtg;
    for (mtg = (Meeting *)iter.get_first(); mtg; mtg = (Meeting *)iter.get_next())
    {
        mtgxml = xmlrpc_build_value(env, "()");

        xmlrpc_value *val = xmlrpc_build_value(env, "s", mtg->get_id());
        xmlrpc_array_append_item(env, mtgxml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        spUser host = mtg->get_host();
        val = xmlrpc_build_value(env, "s",
                                 (host ? host->get_id() : ""));
        xmlrpc_array_append_item(env, mtgxml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        switch (mtg->get_type())
        {
            case TypeInstant:
                val = xmlrpc_build_value(env, "s", "instant");
                break;

            case TypeScheduled:
                val = xmlrpc_build_value(env, "s", "scheduled");
                break;

            default:
                val = xmlrpc_build_value(env, "s", "unknown");
        }
        xmlrpc_array_append_item(env, mtgxml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        switch (mtg->get_privacy())
        {
            case PrivacyPublic:
                val = xmlrpc_build_value(env, "s", "public");
                break;

            case PrivacyUnlisted:
                val = xmlrpc_build_value(env, "s", "unlisted");
                break;

            case PrivacyPrivate:
                val = xmlrpc_build_value(env, "s", "private");
                break;

            default:
                val = xmlrpc_build_value(env, "s", "unknown");
        }
        xmlrpc_array_append_item(env, mtgxml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        const char *title = mtg->get_title();
        val = xmlrpc_build_value(env, "s", (title ? title : ""));
        xmlrpc_array_append_item(env, mtgxml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        const char *description = mtg->get_description();
        val = xmlrpc_build_value(env, "s",
                                 (description ? description : ""));
        xmlrpc_array_append_item(env, mtgxml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        time_t ttt = (time_t)mtg->get_actual_start();
        time_t now = time(NULL);

        char dbuf[16];
        if (ttt > 0 && now > ttt)
        {
            sprintf(dbuf, "%ld", (now - ttt)/60);
        }
        else
        {
            dbuf[0] = '\0';
        }
        val = xmlrpc_build_value(env, "s", dbuf);
        xmlrpc_array_append_item(env, mtgxml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        char tbuf[16];
        if (ttt > 0)
        {
            strftime(tbuf, sizeof(tbuf), "%Y%m%d%H%M%S", gmtime(&ttt));
        }
        else
        {
            tbuf[0] = '\0';
        }

        val = xmlrpc_build_value(env, "s", tbuf);
        xmlrpc_array_append_item(env, mtgxml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        val = xmlrpc_build_value(env, "i", mtg->get_options());
        xmlrpc_array_append_item(env, mtgxml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        int num_connected, num_moderators, num_invited, num_calls, num_app_share, num_licenses;
        mtg->get_meeting_counts(num_connected,
                                num_moderators,
                                num_invited,
                                num_calls,
                                num_app_share,
                                num_licenses,
                                false /* connected both voice&data */);

        val = xmlrpc_build_value(env, "i", num_invited);
        xmlrpc_array_append_item(env, mtgxml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        val = xmlrpc_build_value(env, "i", num_connected);
        xmlrpc_array_append_item(env, mtgxml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        spShareServer sharesvr = mtg->get_share_server();
        const char * addr = (sharesvr ? (const char *)sharesvr->get_address() : "");
        val = xmlrpc_build_value(env, "s", (addr ? addr : ""));
        xmlrpc_array_append_item(env, mtgxml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        val = xmlrpc_build_value(env, "i", num_app_share);
        xmlrpc_array_append_item(env, mtgxml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        val = xmlrpc_build_value(env, "s", (const char *)mtg->get_voice_service()); // voice server
        xmlrpc_array_append_item(env, mtgxml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        val = xmlrpc_build_value(env, "i", num_calls);
        xmlrpc_array_append_item(env, mtgxml, val);
        xmlrpc_DECREF(val);
        val = NULL;

		addr = mtg->get_doc_share_server();
        val = xmlrpc_build_value(env, "s", (addr ? addr : ""));
		xmlrpc_array_append_item(env, mtgxml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        xmlrpc_array_append_item(env, mtgarray, mtgxml);
        xmlrpc_DECREF(mtgxml);
        mtgxml = NULL;
        ++count;
    }

    output = xmlrpc_build_value(env, "(Viiii)", mtgarray, 0, count, count, 0);
    xmlrpc_DECREF(mtgarray);
    mtgarray = NULL;

  cleanup:
    if (mtgvec != NULL)
    {
        delete mtgvec;
    }

    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to get active meetings");

    return output;
}

// Get server status
// Parameters:
//    SESSSIONID, SERVICETYPE (bit field, 1 == voice, 2 == appshare)
//
// Results:
//    Array of rows:
//      SERVERID, SERVICETYPE, STATE (1 - available, 0 - unknown)
xmlrpc_value *get_servers(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    xmlrpc_value *output = NULL;
    const char *sessionid;
    int req_service_types;
    IString service_id;
    int type;
    int available;
    int idx;

    xmlrpc_parse_value(env, param_array, "(si)", &sessionid, &req_service_types);

    check_session(env, user_data, NULL);
    XMLRPC_FAIL_IF_FAULT(env);

    output = xmlrpc_build_value(env, "()");
    for (idx = 0; master.get_service_status(idx, &service_id, &type, &available); idx++)
    {
        if ((req_service_types & ServiceTypeVoice) && type == AddressVoice)
        {
            xmlrpc_value * server = xmlrpc_build_value(env, "()");
            xmlrpc_value * field = xmlrpc_build_value(env, "s", (const char *)service_id);
            xmlrpc_array_append_item(env, server, field);
            xmlrpc_DECREF(field);

            field = xmlrpc_build_value(env, "i", ServiceTypeVoice);
            xmlrpc_array_append_item(env, server, field);
            xmlrpc_DECREF(field);

            field = xmlrpc_build_value(env, "i", available);
            xmlrpc_array_append_item(env, server, field);
            xmlrpc_DECREF(field);
            field = NULL;

            xmlrpc_array_append_item(env, output, server);
            xmlrpc_DECREF(server);
        }
    }

    if (req_service_types & ServiceTypeAppShare)
    {
        for (idx = 0; master.get_appshare_status(idx, &service_id, &available); idx++)
        {

            xmlrpc_value * server = xmlrpc_build_value(env, "()");
            xmlrpc_value * field = xmlrpc_build_value(env, "s", (const char *)service_id);
            xmlrpc_array_append_item(env, server, field);
            xmlrpc_DECREF(field);

            field = xmlrpc_build_value(env, "i", ServiceTypeAppShare);
            xmlrpc_array_append_item(env, server, field);
            xmlrpc_DECREF(field);

            field = xmlrpc_build_value(env, "i", available);
            xmlrpc_array_append_item(env, server, field);
            xmlrpc_DECREF(field);
            field = NULL;

            xmlrpc_array_append_item(env, output, server);
            xmlrpc_DECREF(server);
        }
    }

    if (req_service_types & ServiceTypeDocShare)
    {
        for (idx = 0; master.get_docshare_status(idx, &service_id, &available); idx++)
        {

            xmlrpc_value * server = xmlrpc_build_value(env, "()");
            xmlrpc_value * field = xmlrpc_build_value(env, "s", (const char *)service_id);
            xmlrpc_array_append_item(env, server, field);
            xmlrpc_DECREF(field);

            field = xmlrpc_build_value(env, "i", ServiceTypeDocShare);
            xmlrpc_array_append_item(env, server, field);
            xmlrpc_DECREF(field);

            field = xmlrpc_build_value(env, "i", available);
            xmlrpc_array_append_item(env, server, field);
            xmlrpc_DECREF(field);
            field = NULL;

            xmlrpc_array_append_item(env, output, server);
            xmlrpc_DECREF(server);
        }
    }

cleanup:
    return output;
}


// Get ports.
// Parameters:
//    SESSIONID, SERVERID
//
// Returns:
//    Array of rows:
//         PORTID, STATE, MEETINGID, PARTICIPANTID, SCREENNAME, NAME, LASTEVENTTIME, LASTEVENTTIME
xmlrpc_value *get_ports(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    xmlrpc_value *output = NULL;
    const char * sessionid;
    const char * serverid;
    char id[32];
    const char * dispatch_id;
    GetPortsDispatch * disp;

    xmlrpc_parse_value(env, param_array,
                       "(ss)", &sessionid, &serverid);

    check_session(env, user_data, NULL);
    XMLRPC_FAIL_IF_FAULT(env);

    disp = new GetPortsDispatch(user_data);
    dispatch_id = dispatch_queue.add_dispatch(disp);
    sprintf(id, "dispatch:%s", dispatch_id);
    if (rpc_make_call(id, serverid, "voice.get_ports", "()"))
    {
        xmlrpc_env_set_fault(env, Failed, "Unable to call voice server");

        goto cleanup;
    }
    else
    {
        output = RPC_PENDING;
    }

  cleanup:
    return output;
}

void on_get_ports_result(void *session, RPCResultSet *res)
{
    if (res == NULL)
    {
		rpc_send_fault(session, Failed, "Unable to get port status from voice server");
        return;
    }

    xmlrpc_env env;
    xmlrpc_env_init(&env);

    xmlrpc_value * output = xmlrpc_build_value(&env, "()");

    int sz = res->get_total_rows();
    int ix;
    for (ix = 0; ix < sz; ix++)
    {
        const char *port_id = "";
        const char *part_id = "";
        const char *last_event_name = "";
        xmlrpc_int32 levnt;
        xmlrpc_bool connected;
        res->get_values(ix, "(ssbsi)",
                        &port_id, &part_id, &connected,
                        &last_event_name, &levnt);

        char last_event_time[16];
        if (levnt > 0)
        {
            time_t ttt = levnt;
            strftime(last_event_time, sizeof(last_event_time),
                     "%Y%m%d%H%M%S", gmtime(&ttt));
        }
        else
        {
            last_event_time[0] = '\0';
        }

        const char *mtg_id = "";
        const char *screenname = "";
        const char *name = "";
        spParticipant part = master.find_participant(part_id);
        if (part)
        {
            spMeeting mtg = part->get_meeting();
            mtg_id = mtg->get_id();
            screenname = part->get_screen();
            name = part->get_name();
        }

        // TBD
        xmlrpc_value * row = xmlrpc_build_value(&env, "(sissssss)",
                                                port_id, (connected ? 1 : 0),
                                                mtg_id,
                                                part_id, screenname, name,
                                                last_event_time, last_event_name);
        xmlrpc_array_append_item(&env, output, row);
        xmlrpc_DECREF(row);
        row = NULL;
    }

    if (env.fault_occurred)
    {
        rpc_send_fault(session, env.fault_code, env.fault_string);
    }
    else
    {
        rpc_send_result(session, output);
    }

    xmlrpc_DECREF(output);
    output = NULL;

    xmlrpc_env_clean(&env);
}


// Reset Port.
// Parameters:
//    SESSIONID, SERVERID, PORTID, PARTID
// Returns:
//    number ports affected
xmlrpc_value *reset_port(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    xmlrpc_value *output = NULL;
    const char * sessionid;
    const char * serverid;
    const char * portid;
    const char * partid;
    ResetPortDispatch * disp;
    char id[32];
    const char * dispatch_id;

    xmlrpc_parse_value(env, param_array,
                       "(ssss)",
                       &sessionid, &serverid, &portid, &partid);

    check_session(env, user_data, NULL);
    XMLRPC_FAIL_IF_FAULT(env);

    disp = new ResetPortDispatch(user_data);
    dispatch_id = dispatch_queue.add_dispatch(disp);
    sprintf(id, "dispatch:%s", dispatch_id);
    if (rpc_make_call(id, serverid, "voice.reset_port", "(ss)", portid, partid))
    {
        xmlrpc_env_set_fault(env, Failed, "Unable to call voice server");

        goto cleanup;
    }
    else
    {
        output = RPC_PENDING;
    }

  cleanup:
    return output;
}

void on_reset_port_result(void *session, RPCResultSet *res)
{
    xmlrpc_env env;
    xmlrpc_env_init(&env);

    int num_ports_affected;
    if (!res->get_output_values("(i)", &num_ports_affected))
    {
        log_error(ZONE, "Error parsing result from reset_port");
        rpc_send_fault(session, Failed, "Problems parsing response from voice");
        return;
    }

    xmlrpc_value * output = xmlrpc_build_value(&env, "(i)", num_ports_affected);
    rpc_send_result(session, output);

    if (output)
    {
        xmlrpc_DECREF(output);
    }
}

//
// Get presence of all users connected to controller.  If community_id is supplied, searches
// in the specified community only.  Must be super admin.
//
// Parameters:
//   SESSIONID, COMMUNITYID (can be blank)
//
// Results:
//   Array of:
//      USERID, DISPLAYNAME, USERNAME, PRESENCE_MASK
//          where PRESENCE_MASK values are: 1 - is on line
//                                          2 - is on phone
//                                          4 - is in appshare
//                                          8 - is in meeting
xmlrpc_value *get_presence_all(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    xmlrpc_value *output = NULL;
    xmlrpc_value *user_array = NULL;
    xmlrpc_value *user_xml = NULL;
    const char * sessionid;
    const char * communityid;
    bool use_community = false;
    int status;
    IVectorIterator iter;
    IVector user_vec;

    xmlrpc_parse_value(env, param_array, "(ss)", &sessionid, &communityid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, NULL);
    XMLRPC_FAIL_IF_FAULT(env);

    use_community = strlen(communityid) > 0 ? true : false;

    // get vector of users
    status = master.get_users(&user_vec);
    if (status != Ok)
    {
        xmlrpc_env_set_fault(env, status, "Failed");
        goto cleanup;
    }
    iter.set_vector(user_vec);

    user_array = xmlrpc_build_value(env, "()");

    // iterate through users and build array of values
    User * user;
    for (user = (User *)iter.get_first(); user; user = (User *)iter.get_next())
    {
        // if not checking in a specific community or the community id for the
        // user matches the passed community id AND user is internal user
        if ((!use_community || strcmp(communityid, user->get_community_id()) == 0) &&
            user->is_user())
        {

            user_xml = xmlrpc_build_value(env, "()");

            // userid
            xmlrpc_value *val = xmlrpc_build_value(env, "s", user->get_id());
            xmlrpc_array_append_item(env, user_xml, val);
            xmlrpc_DECREF(val);
            val = NULL;

            // display name
            val = xmlrpc_build_value(env, "s", user->get_name());
            xmlrpc_array_append_item(env, user_xml, val);
            xmlrpc_DECREF(val);
            val = NULL;

            // username
            val = xmlrpc_build_value(env, "s", user->get_username());
            xmlrpc_array_append_item(env, user_xml, val);
            xmlrpc_DECREF(val);
            val = NULL;

            // presence mask
            int presence = 0;

            // online
            presence = user->is_connected(MediaData) ? StatusData : 0;
            // phone
            presence |= user->is_connected(MediaVoice) ? StatusVoice : 0;
            // app share
            presence |= user->is_connected(MediaAppShare) ? StatusAppShare : 0;
            // meeting
            presence |= user->is_in_meeting(NULL, MediaAny) ? StatusInMeeting : 0;

            val = xmlrpc_build_value(env, "i", presence);
            xmlrpc_array_append_item(env, user_xml, val);
            xmlrpc_DECREF(val);
            val = NULL;

            // append to array
            xmlrpc_array_append_item(env, user_array, user_xml);
            xmlrpc_DECREF(user_xml);
            user_xml = NULL;
        }
    }

    output = xmlrpc_build_value(env, "(V)", user_array);
    xmlrpc_DECREF(user_array);
    user_array = NULL;

  cleanup:

    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to retrieve users");

    return output;
}

//
// Get presence of the passed users from the controller. USERIDFLAG indicates if the
// passed users should be searched by userid or screenname.  If a user is not
// found, return fields will be empty strings for that userid or username.  Must be super admin.
//
// Parameters:
//   SESSIONID, USERS, USERIDFLAG
//         where USERS is array of strings, one for user to query.  String is either
//                     USERID or USERNAME (e.g., user:12 or ted@all.homedns.org)
//                     for all users
//               USERIDFLAG = 0 (userid) or 1 (screenname)
//
// Results:
//   Array of:
//      USERID, DISPLAYNAME, USERNAME, PRESENCE_MASK
//          where PRESENCE_MASK values are: 1 - is on line
//                                          2 - is on phone
//                                          4 - is in appshare
//                                          8 - is in meeting
xmlrpc_value *get_presence_users(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	xmlrpc_value *users;
    xmlrpc_value *output = NULL;
    xmlrpc_value *user_array = NULL;
    xmlrpc_value *user_xml = NULL;

    const char * sessionid;
    const char * userkey;
    int useridflag;
    int size;
    int i;

    xmlrpc_parse_value(env, param_array, "(sAi)", &sessionid, &users, &useridflag);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, NULL);
    XMLRPC_FAIL_IF_FAULT(env);

    // init
    user_array = xmlrpc_build_value(env, "()");

    size = xmlrpc_array_size(env, users);
    XMLRPC_FAIL_IF_FAULT(env);

    for (i=0; i<size; i++)
    {
        xmlrpc_value *val = xmlrpc_array_get_item(env, users, i);
        xmlrpc_parse_value(env, val, "s", &userkey);
        if (env->fault_occurred)
        {
			xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.get_presence_users");
			goto cleanup;
        }

        User* user = useridflag == 0 ? master.find_user(userkey) : master.find_user_by_address(userkey);

        user_xml = xmlrpc_build_value(env, "()");

        // userid
        val = xmlrpc_build_value(env, "s", user != NULL ? user->get_id() : (useridflag == 0 ? userkey : ""));
        xmlrpc_array_append_item(env, user_xml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        // display name
        val = xmlrpc_build_value(env, "s", user != NULL ? user->get_name() : "");
        xmlrpc_array_append_item(env, user_xml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        // username
        val = xmlrpc_build_value(env, "s", user != NULL ? user->get_username() : (useridflag == 1 ? userkey : ""));
        xmlrpc_array_append_item(env, user_xml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        // presence mask
        int presence = 0;

        // online
        presence = user != NULL ? (user->is_connected(MediaData) ? StatusData : 0) : 0;
        // phone
        presence |= user != NULL ? (user->is_connected(MediaVoice) ? StatusVoice : 0) : 0;
        // app share
        presence |= user != NULL ? (user->is_connected(MediaAppShare) ? StatusAppShare : 0) : 0;
        // meeting
        presence |= user != NULL ? (user->is_in_meeting(NULL, MediaAny) ? StatusInMeeting : 0) : 0;

        val = xmlrpc_build_value(env, "i", presence);
        xmlrpc_array_append_item(env, user_xml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        // append to array
        xmlrpc_array_append_item(env, user_array, user_xml);
        xmlrpc_DECREF(user_xml);
        user_xml = NULL;
    }

    output = xmlrpc_build_value(env, "(V)", user_array);
    xmlrpc_DECREF(user_array);
    user_array = NULL;

  cleanup:

    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to retrieve presence");

    return output;
}

//
// Get metrics
//
// Parameters:
//   SESSIONID, TYPE, NUMBER
//   where TYPE = 0 (init), 1 (meetings), 2 (ports), 3 (users), 4 (share sessions)
//
// Results:
//   Array of:
//      TYPE, AVERAGE, PEAK, LAST_TIME_MEASURED
xmlrpc_value *get_metrics(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    xmlrpc_value *output = NULL;
    xmlrpc_value *sample_array = NULL;
    xmlrpc_value *sample_xml = NULL;
    const char * sessionid;
    const char * type;
    int type_value;
    int number;
    int count = 0;

    int status;

    IListIterator iter;

    xmlrpc_parse_value(env, param_array, "(ssi)", &sessionid, &type, &number);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, NULL);
    XMLRPC_FAIL_IF_FAULT(env);

    type_value = atoi(type);
    status = master.get_samples(type_value, iter);
    if (status != Ok)
    {
        xmlrpc_env_set_fault(env, status, "Failed");
        goto cleanup;
    }

    sample_array = xmlrpc_build_value(env, "()");

    Sample * sample;
    for (sample = (Sample *)iter.get_first(); sample && (count++ < number); sample = (Sample *)iter.get_next())
    {
        sample_xml = xmlrpc_build_value(env, "()");

        xmlrpc_value *val = xmlrpc_build_value(env, "s", type);
        xmlrpc_array_append_item(env, sample_xml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        val = xmlrpc_build_value(env, "d", sample->get_ave());
        xmlrpc_array_append_item(env, sample_xml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        val = xmlrpc_build_value(env, "i", sample->get_peak());
        xmlrpc_array_append_item(env, sample_xml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        time_t sample_time = sample->get_time();
        char timestamp[21];
        struct tm *tm_now = localtime(&(sample_time));
        strftime(timestamp, 20, "%Y-%m-%d-%H-%M-%S", tm_now);

        val = xmlrpc_build_value(env, "s", timestamp);
        xmlrpc_array_append_item(env, sample_xml, val);
        xmlrpc_DECREF(val);
        val = NULL;

        xmlrpc_array_append_item(env, sample_array, sample_xml);
        xmlrpc_DECREF(sample_xml);
        sample_xml = NULL;
    }

    output = xmlrpc_build_value(env, "(V)", sample_array);
    xmlrpc_DECREF(sample_array);
    sample_array = NULL;

  cleanup:

    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to retrieve samples");

    return output;
}

//
// Get health of services running jabber
//
// Parameters:
//   SESSIONID
//
// Results:
//   Array of services:
//      NAME, ID, LOCATION, STATUS (0: unavailable, 1: available)
xmlrpc_value *get_health(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    xmlrpc_value *output = NULL;
    const char * sessionid;
    HealthDispatch * disp = NULL;
	char id[32];
    const char * dispatch_id = NULL;

    xmlrpc_parse_value(env, param_array, "(s)", &sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, NULL);
    XMLRPC_FAIL_IF_FAULT(env);

	// Save info from this call.
	disp = new HealthDispatch(user_data);
//	dispatch_queue.add_dispatch(disp, "iic:health");

    dispatch_id = dispatch_queue.add_dispatch(disp);
	sprintf(id, "dispatch:%s", dispatch_id);

    rpc_send_health_request(id);

    output = RPC_PENDING;

cleanup:

    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to retrieve health");

    return output;
}

// Send an in-meeting chat message to specified participants.
// Parameters:
//   SESSIONID, MEETINGID, SEND_TO, MESSAGE, FROM_PARTICIPANTID, TO_PARTICIPANTSID
// Returns:
//   status (ok)
xmlrpc_value *send_chat_message(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	spMeeting meeting;
	char *sessionid, *meetingid, *message, *from_participantid, *to_participantid;
	int send_to;
    xmlrpc_value *output = NULL;
	int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ssisss)",
        &sessionid,
        &meetingid,
        &send_to,
        &message,
        &from_participantid,
        &to_participantid);

    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.send_chat_message");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.send_chat_message(
                        sessionid, meetingid, send_to,
                        message, from_participantid, to_participantid);

    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to send meeting chat message");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to send meeting chat message");

    return output;
}

// Send an IM message on behalf of user.
// Parameters:
//   SESSIONID, FROMUSERNAME, TOUSERNAME, MESSAGE
// Returns:
//   status (ok)
xmlrpc_value *send_im(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	spMeeting meeting;
	char *sessionid, *fromuser, *touser, *message;
    xmlrpc_value *output = NULL;
	int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ssss)",
        &sessionid,
		&fromuser,
		&touser,
		&message);

    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.send_im");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    status = master.send_im(sessionid, fromuser, touser, message);

    if (IS_FAILURE(status))
    {
        xmlrpc_env_set_fault(env, status, "Unable to send IM");
        goto cleanup;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to send IM");

    return output;
}

// Verify that the specified user is a participant in the specified meeting
// Parameters:
//   SESSIONID, USERNAME, MEETINGID
// Returns:
//   status (ok)
xmlrpc_value *verify_participant(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{

    char *sessionid, *meetingid, *username;
    spMeeting meeting;
    spUser user;
    int status = NotMember;
    xmlrpc_value *output = NULL;

    // sessionid is not used for now
    xmlrpc_parse_value(env, param_array, "(sss)",
                       &sessionid, &username, &meetingid);

    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.verify_participant");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    user = master.find_user_by_address(username);
    meeting = master.find_meeting(meetingid);

    if (user == NULL || meeting == NULL)
        status = NotMember;
    else
    {
        if (meeting->is_participant(user))
            status = Ok;
        else
            status = NotMember;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to send IM");

    return output;
}

// Log custom meeting event
// Parameters:
//   SESSIONID, MEETINGID, INFO
// Returns:
//   status (ok, pending, retry)
xmlrpc_value *log_event(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *meetingid, *info;
    xmlrpc_value *output = NULL;
    int status = Failed;
    spMeeting meeting;

    xmlrpc_parse_value(env, param_array, "(sss*)", &sessionid, &meetingid, &info);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to controller.log_event");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    // ToDo: validate user is moderator

    meeting = master.find_meeting(meetingid);
    if (meeting != NULL)
    {
        log_event_start(EvnLog_Event_Details,
                         meeting->get_community_id(),
                         meeting->get_id(),
                         (time_t)meeting->get_actual_start(),
                         meeting->get_host()->get_id(),
                         "",
                         info);
        status = Ok;
    }

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to play roll call");

    return output;
}

// ====
// Client register support functions
// ====

void register_exec(RPCResultSet &rs, RegisterDispatch * info)
{
	int found, options;
	char *meetingid, *partid, *userid, *email, *username, *name;
	xmlrpc_env env;
    xmlrpc_value *output = NULL;
	char pid[MAX_ID_LENGTH];
	spUser user;
	int status = Ok;

	xmlrpc_env_init(&env);

	// Use returned info to figure out best user id and which meeting to join.
    // schedule.find_pin result:
	// "FOUND", "MEETINGID", "PARTICIPANTID", "USERID", "EMAIL", "USERNAME", "NAME", "OPTIONS"
	// FOUND: 1 = meeting found, 2 = meeting and user found, 0 = not found
	if (!rs.get_output_values("(issssssi*)", &found, &meetingid, &partid, &userid, &email, &username, &name, &options))
	{
		log_error(ZONE, "Error parsing result from fetch PIN\n");
		goto cleanup;
	}
	switch (found)
	{
		case 0:
			// PIN not found, return error.
            status = InvalidPIN;
			xmlrpc_env_set_fault(&env, status, "Invalid PIN specified");
			goto cleanup;
			break;
		case 1:
			// Anonymous meeting pin entered, generate anonymous user id later
			userid = NULL;
			partid = NULL;
			break;
		case 2:

		{
			int media_type = get_media_type(info->address);

            // New tactic: use pin:xxx form as user id
			userid = (char*)info->userid.get_string();

			if (atoi(partid) > 0)
			{
				sprintf(pid, "part:%s", partid);
				partid = pid;

				// If user indicated by pin is already connected, generate anonymous
				// user id & part id for this connection.
				IString addr;
				status = master.find_participant_address(partid, media_type, addr);
				if (status == Ok)
				{
					userid = NULL;
					partid = NULL;
				}
			}
			else
			{
				// Shouldn't happen, db should have valid participant id
				partid = NULL;
			}

			// Save display name and user name if not in registration request.
			if (info->name.length() == 0)
				info->name = name;
			if (info->username.length() == 0)
				info->username = username;

			break;
		}
	}

	// Finish registration
	status = master.register_client(userid, info->username, info->name, info->address, info->network_address, user);
	if (status != Ok)
	{
		xmlrpc_env_set_fault(&env, status, "Unable to register client");
		goto cleanup;
	}

	output = xmlrpc_build_value(&env, "(sssi)", user->get_id(), partid != NULL ? partid : "",
					master.get_webstart_url(), status);
	XMLRPC_FAIL_IF_FAULT(&env);

cleanup:
	if (env.fault_occurred)
	{
		rpc_send_fault(info->session, env.fault_code, env.fault_string);
	}
	else if (output == NULL)
	{
		rpc_send_fault(info->session, Failed, "Unable to register client");
	}
	else
	{
		rpc_send_result(info->session, output);
        xmlrpc_DECREF(output);
        output = NULL;
	}

	// Error is taken care of.
	xmlrpc_env_clean(&env);
}

void register_error(RegisterDispatch *info)
{
	rpc_send_fault(info->session, Failed, "Unable to register client");
}

// ====
// Event logging
// ====

int log_event_start(int event, const char * community_id, const char * meeting_id, time_t meeting_start,
					const char * host_id, const char * part_id, const char * info)
{
	IUseMutex lock(event_mutex);

	const char * _host_id = strchr(host_id, ':');
	const char * _part_id = strchr(part_id, ':');
    char _meeting_start[32];
    sprintf(_meeting_start, "%d", (int)meeting_start);

	EventLogField fields[] = { EvnLog_Field_EventInfo,
                               EvnLog_Field_CommunityId,
                               EvnLog_Field_MeetingId,
                               EvnLog_Field_MeetingStart,
                               EvnLog_Field_HostId,
                               EvnLog_Field_ParticipantId };
	const char * values[] = { info,
                              community_id,
                              meeting_id,
                              _meeting_start,
                              _host_id ? _host_id + 1 : "",
							  _part_id ?  _part_id + 1 : "" };

	return event_log.log_start((EventLogEvent)event,
                               fields,
                               values,
                               sizeof(fields) / sizeof(EventLogField));
}

int log_get_start_time(int event)
{
	IUseMutex lock(event_mutex);

    return (int)event_log.log_get_start_time(event);
}

void log_event_end(int event_handle)
{
	IUseMutex lock(event_mutex);

	event_log.log_end(event_handle);
}

// ====
// Process incoming results
// Isolated here to reduce dependency on xmlrpc.
// ====

// Response from RPC call placed during meeting set up
void process_meeting_result(xmlrpc_env * env, xmlrpc_value * res, const char *id)
{
	IRefObjectPtr<Meeting> meeting;
	RPCResultSet rs(env, res);
	const char * meetingid;
	int status = Ok;

	meetingid = strchr(id, ':');
	if (meetingid == NULL)
		return;
	int len = meetingid - id;
	meetingid++;

    meeting = master.find_meeting(meetingid);
    if (meeting == NULL)
    {
        log_error(ZONE, "Received info for meeting %s, but meeting not found\n", meetingid);
    }
	else
	{
		// Dispatch to appropriate handler.
		if (!strncmp(id, "load", len))
			status = meeting->on_load_result(&rs);
		else if (!strncmp(id, "create", len))
			status = meeting->on_create_result(&rs);
        else if (!strncmp(id, "reset", len))
            meeting->on_reset_result(&rs);
        else if (!strncmp(id, "reload", len))
            meeting->on_reload_result(&rs);
	}

	if (IS_FAILURE(status) && meeting)
		meeting->on_start_failed(status);
}

void process_submeeting_result(xmlrpc_env * env, xmlrpc_value * res, const char *id)
{
	spMeeting meeting;
	spSubMeeting submeeting;
	RPCResultSet rs(env, res);
	const char * subid;

	subid = strchr(id, ':');
	if (subid == NULL)
		return;
	int len = subid - id;
	subid++;

    submeeting = master.find_submeeting(subid);
    if (submeeting == NULL)
    {
        log_error(ZONE, "Received info for submeeting %s, but submeeting not found\n", subid);
    }
	else
	{
		// Dispatch to appropriate handler.
		if (!strncmp(id, "addsm", len))
			submeeting->on_start_result(&rs);
		else if (!strncmp(id, "closesm", len))
			submeeting->on_end_result(&rs);
		else if (!strncmp(id, "resetsm", len))
			submeeting->on_reset_result(&rs);
	}
}

void process_user_result(xmlrpc_env * env, xmlrpc_value * res, const char *id)
{
	spUser user;
	RPCResultSet rs(env, res);
	const char *uid;

	uid = strchr(id, ':');
	if (uid == NULL)
		return;
	//int len = uid - id;
	uid++;

	user = master.find_user(uid);
	if (user != NULL)
	{
		user->on_load_result(&rs);
	}
}

// *** Provide mechanism for allowing other service to send response directly on our behalf.
// Send result from pin lookup.
void process_pin_result(xmlrpc_env * env, xmlrpc_value * res, void *session)
{
	rpc_send_result(session, res);
}

// Send result from pin lookup.
void process_invite_result(xmlrpc_env * env, xmlrpc_value * res, void *session)
{
	rpc_send_result(session, res);
}

void process_volume_result(xmlrpc_env * env, xmlrpc_value * res, void *session)
{
	rpc_send_result(session, res);
}

// *** Need to extend jcomponent to signal error on timeout
void process_meeting_error(const char * meetingid, int fault_code, const char * message)
{
	IRefObjectPtr<Meeting> meeting;

	meeting = master.find_meeting(meetingid);
    if (meeting == NULL)
    {
        log_error(ZONE, "Received error about meeting %s, but meeting not found\n", meetingid);
    }
	else
	{
		log_error(ZONE, "Meeting %s failed to start: %s\n", meetingid, message);
		meeting->on_start_failed(Failed);
	}
}

void process_recording_result(xmlrpc_env * env, xmlrpc_value * res, const char *id)
{
	IRefObjectPtr<Meeting> meeting;
	RPCResultSet rs(env, res);
	const char * meetingid;
	int status = Ok;

	meetingid = strchr(id, ':');
	if (meetingid == NULL)
		return;
	meetingid++;

	meeting = master.find_meeting(meetingid);
	if (meeting == NULL)
	{
		log_error(ZONE, "Received info for meeting %s, but meeting not found\n", meetingid);
	}
	else
	{
		meeting->on_recording_result(status);
	}
}

void process_submeeting_error(const char * subid, int fault_code, const char * message)
{
	IRefObjectPtr<SubMeeting> submeeting;

	submeeting = master.find_submeeting(subid);
    if (submeeting == NULL)
    {
        log_error(ZONE, "Received error about submeeting %s, but meeting not found\n", subid);
    }
	else
	{
		log_error(ZONE, "Meeting %s failed to start: %s\n", subid, message);
		submeeting->on_start_failed();
	}
}

void process_user_error(const char * userid, int fault_code, const char * message)
{
	spUser user;

	user = master.find_user(userid);
	if (user == NULL)
	{
		log_error(ZONE, "Received error about user %s, but user not found\n", userid);
	}
	else
	{
		log_error(ZONE, "Failed to load info for user %s: %s\n",userid, message);
		user->on_load_failed();
	}
}

void process_pin_error(void * session, xmlrpc_int32 fault_code, char *message)
{
	rpc_send_fault(session, fault_code, message);
}

void process_invite_error(void * session, xmlrpc_int32 fault_code, char *message)
{
	rpc_send_fault(session, fault_code, message);
}

void process_call_error(const char * id, xmlrpc_int32 fault_code, const char *message)
{
	spUser user;
	spParticipant participant;
	spMeeting meeting;
	IString partid;
	IString userid;

	const char * pos = strchr(id, '.');
	if (pos == NULL)
		goto notfound;

	userid = IString(id, pos - id);
	partid = IString(pos + 1);

	user = master.find_user(userid);
	if (user == NULL)
		goto notfound;

	participant = master.find_participant(partid);
	if (participant == NULL)
		goto notfound;

	log_error(ZONE, "Failed to place call for participant %s: %s",
              (const char *)partid, message);
	meeting = participant->get_meeting();
	if (meeting != NULL)
		meeting->on_call_failed(user, participant, fault_code);

	return;

notfound:
	log_error(ZONE, "Received call error for %s, but user or participant not found", id);
}

// ====
// JComponent callbacks.
// ====

int JC_connected()
{
	if (!running)
	{
		running = true;

		if (transport_pool == NULL)
			transport_pool = new TransportPool("/opt/iic/conf/transport.xml",
											   master.get_server_name());
	}

    // If I had to reconnect to sm, I'm not sure of the system state anymore (e.g.
    // I may not be the selected controller).  Just to be sure, set up for complete reset
    // (we won't reset db or voice services unless we get "started" status from JSM).
    controller_epoch = time(NULL);
    master.reset(false);

    // reset sample time
    sample_time = time(NULL);

    // reset sample logging (enabled once we're activated)
    master.started(false);

    load_admins();

    return 0;
}

void JC_presence(const char * jid, int available, const char * status)
{
	master.on_presence(jid, available, status);
}

void RPC_status(const char *status)
{
	if (strcmp(status, "started") == 0)
	{
		log_status(ZONE, "Service status received: %s", status);

        // enable metrics sampling
        master.started(true);

        // We've been informed that we're live, reset any state appropriately.
		master.reset(true);

		master.reset_services(controller_epoch);
	}
}

void RPC_namespace_callback(const char *ns, const char *id, s2s_t s2s, nad_t nad)
{
    log_debug(ZONE, "Namespace callback received");

    if (strcmp(ns, "iic:session") == 0)
    {
        int query, status;
        bool handled = false;

        query = nad_find_elem(nad, 0, "query", 1);
        if (query >= 0)
        {
            status = nad_find_first_child(nad, query);
            if (status >= 0 && NAD_ENAME_L(nad,status) > 0)
            {
                if (strncmp(NAD_ENAME(nad,status), "endsession", NAD_ENAME_L(nad,status)) == 0)
                {
                    int id_attr = nad_find_attr(nad, status, "id", NULL);

                    if (id_attr >= 0)
                    {
                        IString id(NAD_AVAL(nad, id_attr), NAD_AVAL_L(nad, id_attr));

                        log_debug(ZONE, "Session %s ended", (const char *)id);

                        master.on_presence(id, PresenceUnavailable, NULL);
                        handled = true;
                    }
                }
            }
        }

        if (!handled)
            log_error(ZONE, "Invalid session presence packet received, ignoring");
    }
    else if (strcmp(ns, "iic:health") == 0)
    {
        const char * end = strchr(id, ':');
        if (end == NULL)
            return;
        int len = end - id;

        if (!strncmp(id, "dispatch", len))
        {
            dispatch_queue.process_completed(end+1, true, s2s, nad);
        }
    }
}

static void expire_mparams()
{
    if (s_mptable.size() > 0)
    {
        s_mptable.check_expire();
    }
}

int RPC_run()
{
    static unsigned invocation_counter = 0;

    ++invocation_counter;

	if ((invocation_counter % 30000) == 0)
	{
		event_log.check_rotation();
	}

    if ((invocation_counter % 500) == 0) // about every 10 seconds or so
    {
        expire_mparams();
    }

    if ((invocation_counter % 100) == 0) // attempt to check every 2 seconds
    {
        int now = time(NULL);
        if (now - sample_time >= SAMPLING_FREQUENCY)  // sample every SAMPLING_FREQUENCY secs
        {
            log_debug(ZONE, "Sampling");

            sample_time = now;
            master.sample(now);

            static unsigned sample_counter = 0;

            sample_counter++;
            if (sample_counter >= SAMPLING_INTERVAL) // store every SAMPLING_INTERVAL samples
            {
                sample_counter = 0;
                log_debug(ZONE, "Storing samples");
                master.rotate();
            }
        }
    }

	return 0;
}

void RPC_error(const char *id, xmlrpc_int32 fault_code, const char * message)
{
    // Handle, so clients are not waiting forever
    log_error(ZONE, "Error processing RPC request: %s: %d - %s\n", id, fault_code, message);

	const char * end = strchr(id, ':');
	if (end == NULL)
		return;
	int len = end - id;

	if (!strncmp(id, "create", len) || !strncmp(id, "load", len))
	{
		process_meeting_error(end + 1, fault_code, message);
	}
	else if (!strncmp(id, "addsm", len))
	{
		process_submeeting_error(end + 1, fault_code, message);
	}
	else if (!strncmp(id, "pin", len))
	{
		// bad_ptr
		process_pin_error((void*)atol(end+1), fault_code, (char *)message);
	}
	else if (!strncmp(id, "invite", len))
	{
		// bad_ptr
		process_invite_error((void*)atol(end+1), fault_code, (char *)message);
	}
	else if (!strncmp(id, "call", len))
	{
		process_call_error(end + 1, fault_code, message);
	}
	else if (!strncmp(id, "lduser", 6))
	{
		process_user_error(end + 1, fault_code, message);
	}
	else if (!strncmp(id, "dispatch", len))
	{
		dispatch_queue.process_completed(end+1, false);
	}
}


void RPC_result(const char * id, xmlrpc_value * resval)
{
    xmlrpc_env env;

    xmlrpc_env_init(&env);

	const char * end = strchr(id, ':');
	if (end == NULL)
		return;
	int len = end - id;

	if (!strncmp(id, "create", len) || !strncmp(id, "load", len) || !strncmp(id, "reset", len) || !strncmp(id, "reload", len))
	{
		process_meeting_result(&env, resval, id);
	}
	else if (!strncmp(id, "addsm", len) || !strncmp(id, "closesm", len) || !strncmp(id, "resetsm", len))
	{
		process_submeeting_result(&env, resval, id);
	}
	else if (!strncmp(id, "pin", 3))
	{
		// bad_ptr
		process_pin_result(&env, resval, (void*)atol(end+1));
	}
	else if (!strncmp(id, "invite", 6))
	{
		// bad_ptr
		process_invite_result(&env, resval, (void*)atol(end+1));
	}
	else if (!strncmp(id, "lduser", 6))
	{
		process_user_result(&env, resval, id);
	}
	else if (!strncmp(id, "volume", len))
	{
		// bad_ptr
		process_volume_result(&env, resval, (void*)atol(end+1));
	}
	else if (!strncmp(id, "dispatch", len))
	{
        RPCResultSet rs(&env, resval);
		dispatch_queue.process_completed(end+1, true, &rs);
	}
/*	else if (!strncmp(id, "call", len))
	{
		process_call_result(&env, resval, id);
	}
*/
	else if (!strncmp(id, "enable_recording", len))
	{
		process_recording_result(&env, resval, id);
	}

    if (env.fault_occurred)
    {
        char buffer[512];
        strcpy(buffer, "Result error: ");
        if (env.fault_string)
            strncat(buffer, env.fault_string, 512 - 16);
        else
            strcat(buffer, "unknown cause");
        RPC_error(id, env.fault_code, buffer);
    }
    xmlrpc_env_clean(&env);
}

int main(int argc, char * argv[])
{
	fprintf(stderr, "IIC Meeting Controller 1.0   Copyright (C) 2007 sitescape.com\n\n");

    int instance = 0;

	if (argc > 1)
		instance = atoi(argv[1]);

    config_t conf = config_new();
    config_load(conf, JCOMPNAME, instance, "/opt/iic/conf/config.xml");

	if (jc_init(conf, JCOMPNAME))
	{
		log_error(ZONE, "Unable to initialize controller\n");
		return -1;
	}

    if (controller_init(conf))
        return -1;

    jc_set_connected_callback(JC_connected);
	jc_set_presence_callback(JC_presence);
	rpc_set_callbacks(RPC_result, RPC_error, RPC_run);
    rpc_set_status_callback(RPC_status);
    rpc_set_namespace_callback(RPC_namespace_callback);
    rpc_set_user_callable(1);

	// Add methods to RPC server
    rpc_add_method("controller.register_service", &register_service, NULL);
    rpc_add_method("controller.unregister_service", &unregister_service, NULL);
    rpc_add_method("controller.register_user", &register_user, NULL);
    rpc_add_method("controller.unregister_user", &unregister_user, NULL);
    rpc_add_method("controller.create_meeting", &create_meeting, NULL);
    rpc_add_method("controller.start_meeting", &start_meeting, NULL);
    rpc_add_method("controller.end_meeting", &end_meeting, NULL);
    rpc_add_method("controller.modify_meeting", &modify_meeting, NULL);
    rpc_add_method("controller.modify_recording", &modify_recording, NULL);
    rpc_add_method("controller.toggle_lecture_mode", &toggle_lecture_mode, NULL);
    rpc_add_method("controller.toggle_hold_mode", &toggle_hold_mode, NULL);
    rpc_add_method("controller.lock_meeting", &lock_meeting, NULL);
    rpc_add_method("controller.add_participants", &add_participants, NULL);
    rpc_add_method("controller.add_participant", &add_participant, NULL);
    rpc_add_method("controller.call_participant", &call_participant, NULL);
    rpc_add_method("controller.modify_participant", &modify_participant, NULL);
    rpc_add_method("controller.notify_participant", &notify_participant, NULL);
    rpc_add_method("controller.remove_participant", &remove_participant, NULL);
    rpc_add_method("controller.add_submeeting", &add_submeeting, NULL);
    rpc_add_method("controller.remove_submeeting", &remove_submeeting, NULL);
    rpc_add_method("controller.move_participant", &move_participant, NULL);
    rpc_add_method("controller.join_meeting", &join_meeting, NULL);
    rpc_add_method("controller.leave_meeting", &leave_meeting, NULL);
    rpc_add_method("controller.reload_meeting", &reload_meeting, NULL);
    rpc_add_method("controller.find_pin", &find_pin, NULL);
    rpc_add_method("controller.find_invite", &find_invite, NULL);
    rpc_add_method("controller.start_app_share", &start_app_share, NULL);
    rpc_add_method("controller.end_app_share", &end_app_share, NULL);
    rpc_add_method("controller.request_share_server", &request_share_server, NULL);
    rpc_add_method("controller.change_presenter", &change_presenter, NULL);
    rpc_add_method("controller.grant_app_share_control", &grant_app_share_control, NULL);
    rpc_add_method("controller.call_progress", &call_progress, NULL);
    rpc_add_method("controller.disconnect_participant", &disconnect_participant, NULL);
	rpc_add_method("controller.get_participant_volume", &get_participant_volume, NULL);
	rpc_add_method("controller.set_participant_volume", &set_participant_volume, NULL);
	rpc_add_method("controller.set_handup", &set_handup, NULL);
    rpc_add_method("controller.ping", &ping, NULL);
	rpc_add_method("controller.grant_moderator_privilege", &grant_moderator_privilege, NULL);
    rpc_add_method("controller.get_meeting_status", &get_meeting_status, NULL);
    rpc_add_method("controller.get_meetings", &get_meetings, NULL);
    rpc_add_method("controller.get_servers", &get_servers, NULL);
    rpc_add_method("controller.get_ports", &get_ports, NULL);
    rpc_add_method("controller.reset_port", &reset_port, NULL);
    rpc_add_method("controller.send_chat_message", &send_chat_message, NULL);
    rpc_add_method("controller.send_im", &send_im, NULL);
    rpc_add_method("controller.verify_participant", &verify_participant, NULL);
    rpc_add_method("controller.log_event", &log_event, NULL);

    // Voice server IVR menu calls
	rpc_add_method("controller.roll_call", &roll_call, NULL);
	rpc_add_method("controller.place_call", &place_call, NULL);
    rpc_add_method("controller.toggle_record_meeting",
                   &toggle_record_meeting, NULL);
    rpc_add_method("controller.toggle_mute_join_leave",
                   &toggle_mute_join_leave, NULL);

    rpc_add_method("controller.send_bridge_request", &send_bridge_request, NULL);
    rpc_add_method("controller.bridge_request_progress", &bridge_request_progress, NULL);

    rpc_add_method("controller.get_presence_all",   &get_presence_all, NULL);
    rpc_add_method("controller.get_presence_users", &get_presence_users, NULL);
    rpc_add_method("controller.get_metrics", &get_metrics, NULL);
    rpc_add_method("controller.get_health", &get_health, NULL);
    rpc_add_method("controller.enable_data_recording",
                   &enable_data_recording, NULL);
    rpc_add_method("controller.start_doc_share",
                   &start_doc_share, NULL);
    rpc_add_method("controller.end_doc_share",
                   &end_doc_share, NULL);
    rpc_add_method("controller.get_doc_share_server",
                   &get_doc_share_server, NULL);

    rpc_add_method("controller.download_document_page",
                   &download_document_page, NULL);
    rpc_add_method("controller.configure_whiteboard",
                   &configure_whiteboard, NULL);
    rpc_add_method("controller.draw_on_whiteboard",
                   &draw_on_whiteboard, NULL);

    rpc_add_method("controller.on_ppt_remote_control",
                   &on_ppt_remote_control, NULL);

    rpc_add_method("controller.check_telephony_avail",
                   &check_telephony_avail, NULL);

    rpc_add_method("controller.add_meeting_param",
                   &add_meeting_param, NULL);
    rpc_add_method("controller.get_meeting_param",
                   &get_meeting_param, NULL);

	jc_run(false);

	dispatcher.shutdown();
    heartbeat.shutdown();
	log_status(ZONE, "Shut down event dispatch");
	return 0;
}
