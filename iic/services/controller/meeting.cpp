/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 *
 *
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */

#ifdef WIN32
#pragma warning(disable:4786)
#endif

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include <libintl.h>
#define _(String) gettext(String)

#include "../common/mailer.h"
#include "../common/address.h"
#include "../common/common.h"
#include "../common/datetime.h"
#include "../../util/base64/Base64.h"
#include "../../util/base64/MIMECode.h"
#include "../../util/ithread.h"
#include "../../jcomponent/util/rpcresult.h"
#include "../../jcomponent/util/port.h"
#include "../../jcomponent/util/log.h"
#include "../../jcomponent/util/eventlog.h"
#include "controller.h"
#include "meeting.h"
#include "appshare.h"
#include "controllerrpc.h"
#include "events.h"
#include "transport.h"


// Locking:
// - Decided to separate ref. counting lock and normal usage lock so actions that
//   update ref count (copying smart pointer, etc.) will not get the real object lock.
// - Meeting object may be locked when calling back to controller, so controller
//   should not hold lock when calling meeting objects.


extern EventLog event_log;


bool i_atob(char *buffer)
{
   if (!strcasecmp(buffer, "true"))
      return true;
   else
      return false;
}

//====
// Participant
//====

Participant::Participant(const char * _id, SubMeeting * _submeeting, User * _user, int _type, int _roll, const char * _name,
                         int _sel_phone, const char * _phone, int _sel_email, const char * _email, const char * _screen, const char * _description,
                         int _notify_type, int _call_options)
   : participant_id(_id), user(_user), name(_name), phone(_phone), email(_email), screen(_screen),
     description(_description), submeeting(_submeeting)
{
   sel_phone = _sel_phone;
   sel_email = _sel_email;
   invited_name = _name;
   participant_type = _type;
   roll = _roll;
   notify_type = _notify_type;
   call_options = _call_options;
   outbound_progress = 0;
   bridge_request = 0;
   mute = MuteOff;
   volume = gain = 3;
   remote_control = false;
   sent_im = false;
   handup = false;
   voice_event = -1;
   data_event = -1;

   joined_by_voice = false;
   joined_by_data = false;
}

Participant::~Participant()
{
   log_debug(ZONE, "Deleting participant %s", get_id());
}

void Participant::update_info(int _role, const char * _name, int _sel_phone, const char * _phone, int _sel_email, const char * _email,
                              const char * _screen, const char * _description, int _notify_type, int _call_options)
{
   roll = _role;
   name = _name;
   sel_phone = _sel_phone;
   phone = _phone;
   sel_email = _sel_email;
   email = _email;
   screen = _screen;
   description = _description;
   notify_type = _notify_type;
   call_options = _call_options;
}

void Participant::lock()
{
   ref_lock.lock();
}

void Participant::unlock()
{
   ref_lock.unlock();
}

void Participant::release()
{
   set_submeeting(NULL);
   master.unregister_participant(this);
}

// Participant is joining meeting.
int Participant::connect_port(Port * port, bool wait)
{
   int status = Ok;
   spUser port_user = port->get_owner();

   // We should be associated with a submeeting & meeting
   spMeeting meeting = get_meeting();
   if (port_user == NULL || submeeting == NULL || meeting == NULL)
      return NotConnected;

   // Store port in appropriate slot.
   switch (port->get_port_type())
   {
      case MediaData:
         if (data_port != NULL && data_port != port)
            return AlreadyConnected;
         data_port = port;
         break;
      case MediaVoice:
         if (voice_port != NULL && voice_port != port)
            return AlreadyConnected;
         voice_port = port;
         break;
   }

   // Complete actual connection
   status = port_user->connect_port(this, port, wait);

   // Log
   const char * addr = port->get_id();
   switch (port->get_port_type())
   {
      case MediaData:
         if (addr != NULL && data_event == -1)
         {
            const char * info = strchr(addr, ':') + 1;
            data_event = log_event_start(EvnLog_Event_IIC_Client,
                                         meeting->get_community_id(),
                                         meeting->get_id(),
                                         (time_t)meeting->get_actual_start(),
                                         meeting->get_host()->get_id(),
                                         get_id(),
                                         info);
            joined_by_data = true;
         }
         break;

      case MediaVoice:
         if (addr != NULL && voice_event == -1)
         {
            const char * info = strchr(addr, ':') + 1;
            const char * phone = port->get_network_address();
            int event;
            if (strncmp(info, "in", 2) == 0)
            {
               event = EvnLog_Event_Inbound_Call;
            }
            else
            {
               event = EvnLog_Event_Outbound_Call;
               phone = get_phone();
            }
            voice_event = log_event_start(event,
                                          meeting->get_community_id(),
                                          meeting->get_id(),
                                          (time_t)meeting->get_actual_start(),
                                          meeting->get_host()->get_id(),
                                          get_id(), phone);
            joined_by_voice = true;
         }
         break;
   }

   return status;
}

// Participant is leaving the meeting.
int Participant::disconnect_port(Port * port, bool notify_services, bool notify_presence)
{
   int status = Ok;
   spUser port_user = port->get_owner();

   // We should be associated with a submeeting & meeting
   spMeeting meeting = get_meeting();
   if (port_user == NULL || meeting == NULL)
      return NotConnected;

   // Complete disconnect.
   status = port_user->disconnect_port(this, port, notify_services, notify_presence);

   // Log
   switch (port->get_port_type())
   {
      case MediaData:
         outbound_progress = 0;
         bridge_request = 0;
         if (data_event != -1)
         {
            log_event_end(data_event);
            data_event = -1;
         }
         break;

      case MediaVoice:
         if (voice_event != -1)
         {
            log_event_end(voice_event);
            voice_event = -1;
         }
         break;
   }

   // Clear port from appropriate slot.
   switch (port->get_port_type())
   {
      case MediaData:
         if (data_port != port)
            return NotConnected;
         data_port = NULL;
         break;
      case MediaVoice:
         if (voice_port != port)
            return NotConnected;
         voice_port = NULL;
         break;
   }

   return status;
}

// Disconnect all connections to this meeting.
int Participant::disconnect(bool notify_services)
{
   int status = Ok;

   if (data_port != NULL)
   {
      status = disconnect_port(data_port, notify_services, false);
      data_port = NULL;
   }

   if (voice_port != NULL)
   {
      disconnect_port(voice_port, notify_services, false);
      voice_port = NULL;
   }

   return status;
}

// Save last call progress status for this participant.
void Participant::set_progress(int progress)
{
   outbound_progress = progress;
}

// Change role.
void Participant::set_moderator(bool f)
{
   if (f)
   {
      roll |= RollModerator;
   }
   else
   {
      roll &= ~RollModerator;
   }
}

// See if user is connected to this meeting.
bool Participant::is_connected()
{
   spMeeting meeting = get_meeting();

   if (meeting != NULL)
   {
      if (voice_port != NULL && voice_port->is_in_meeting(meeting))
         return true;
      if (data_port != NULL && data_port->is_in_meeting(meeting))
         return true;
   }
   return false;
}

bool Participant::is_user_attached(User * user)
{
   if (get_user() == user)
      return true;
   if (voice_port != NULL && voice_port->get_owner() == user)
      return true;
   if (data_port != NULL && data_port->get_owner() == user)
      return true;

   return false;
}

bool Participant::is_presenter()
{
   if (user != NULL && submeeting != NULL)
   {
      spMeeting meeting = get_meeting();
      if (meeting && user->is_in_meeting(meeting) && user == meeting->get_presenter())
      {
         return true;
      }
   }
   return false;
}

// Get participant status for reporting in events.
int Participant::get_status()
{
   int status = 0;

   if (get_voice_port())
      status |= StatusVoice;

   if (get_data_port())
      status |= StatusData;

   if (is_connected())
      status |= StatusInMeeting;

   if (is_presenter())
      status |= StatusPresenter;

   if (is_remote_control())
      status |= StatusControl;

   if (is_im_sent())
      status |= StatusIMSent;

   if (is_handup())
      status |= StatusHandUp;

   // If muting happens when a talker is speaking
   // make sure the talking flag is clear
   if (get_mute() != 0)
      outbound_progress &= ~StatusTalking;

   if (get_bridge_request())
      status |= StatusBridgeRequest;

   status |= outbound_progress;

   return status;
}

void Participant::set_submeeting(SubMeeting * sm)
{
   IUseMutex lock(participant_lock);
   submeeting = sm;
}

spMeeting Participant::get_meeting()
{
   Meeting * meeting = NULL;

   IUseMutex lock(participant_lock);

   if (get_submeeting() != NULL)
      meeting = get_submeeting()->get_meeting();

   return spMeeting(meeting);
}

spUser Participant::get_user()
{
   IUseMutex lock(participant_lock);
   return user;
}

// Change user associated with this participant.
// Warning:  this is a dangerous operation
void Participant::set_user(User * _user)
{
   IUseMutex lock(participant_lock);
   user = _user;
}

spPort Participant::get_port(int media_type)
{
   switch (media_type)
   {
      case MediaVoice:
         if (voice_port != NULL)
            return voice_port;
         else
         {
            // not yet in the conference
            // see if the voice port for the user is registered
            Address addr;
            IString pin;

            parse_address(get_id(), &addr);
            make_id_string(pin, "pin:", addr.id);

            spUser user = master.find_user(pin);
            if (user != NULL)
               return user->get_voice();
         }
         break;

      case MediaData:
         return data_port;
   }

   return NULL;
}

// Turn on mute for participant if in lecture mode (and participant is not moderator) or hold mode
int Participant::check_mute(bool * did_change)
{
   *did_change = false;

   // Lots of reasons to do nothing w/ mute setting
   spMeeting mtg = get_meeting();
   if (!(mtg->get_options() & (LectureMode|HoldMode)))
   {
      // Not in lecture or hold mode; do nothing
      return Ok;
   }

   if (is_moderator())
   {
      // Moderators are free to talk in lecture or hold mode
      return Ok;
   }

   // Figure out which mute setting should be enabled based on meeting settings
   int new_mute = MuteOn;
   if (mtg->get_options() & HoldMode)
	   new_mute = MuteHold;

   spSubMeeting sm = get_submeeting();
   if (sm == NULL)
   {
      return Ok;
   }

   if (sm->is_main() && mute == new_mute)
   {
      // Nothing changed
      return Ok;
   }
   else if (!sm->is_main() && mute != 0)
   {
      // Take of mute if put in submeeting
      new_mute = MuteOff;
   }

   *did_change = true;
   set_mute(new_mute);

   log_debug(ZONE, "participant %s %s", get_id(), (mute == MuteHold ? "hold muted" : (mute == MuteOn ? "muted" : "not muted")));

   return Ok;
}

//====
// Submeeting
//====

SubMeeting::SubMeeting(const char *id, Meeting * _meeting, int _expected_participants, bool _main, const char * _title)
   : sub_id(id), title(_title), meeting(_meeting)
{
   main = _main;
   state = StateNone;
   expected_participants = _expected_participants;
   dispatch_call = NULL;
   options = AnnounceJoinExit;
}

SubMeeting::~SubMeeting()
{
   log_debug(ZONE, "Deleting submeeting %s", get_id());
}

void SubMeeting::lock()
{
   ref_lock.lock();
}

void SubMeeting::unlock()
{
   ref_lock.unlock();
}

// Release all participants
void SubMeeting::release()
{
   IUseMutex lock(sm_lock);

   IListIterator iter;
   get_participants(iter);

   Participant * p = (Participant *)iter.get_first();
   while (p)
   {
      p->release();
      p = (Participant *)iter.get_next();
   }

   participants.remove_all();
   meeting = NULL;

   master.unregister_submeeting(this);
}

void SubMeeting::reset()
{
   IUseMutex lock(sm_lock);

   // Register with voice service
   // N.B. main submeeting registered when meeting is registered.
   if (!is_main())
   {
      char id[32];

      //   MEETINGID, SUBMEETINGID, EXPECTEDPARTICIPANTS
      sprintf(id, "resetsm:%s", get_id());
      if (rpc_make_call(id, meeting->get_voice_service(), "voice.register_submeeting", "(ssi)", meeting->get_id(), get_id(), expected_participants))
      {
         log_error(ZONE, "Error notifying voice service about submeeting %s\n", get_id());
      }
   }
   else
   {
      on_reset_result(NULL);
   }
}

void SubMeeting::on_reset_result(RPCResultSet * rs)
{
   IUseMutex lock(sm_lock);

   // Reset submeeting options
   IString id;
   make_id_string(id, "call:", get_id());
   if (rpc_make_call(id,
                     meeting->get_voice_service(), "voice.modify_submeeting", "(ssi)",
                     get_meeting()->get_id(), get_id(), options))
   {
      log_error(ZONE, "Error notifying voice service about submeeting %s\n", get_id());
   }

   IListIterator iter;
   get_participants(iter);

   // Re-notify voice service about all current participants.
   Participant * p = (Participant *)iter.get_first();
   while (p)
   {
      log_debug(ZONE, "Notifying voice service about %s", p->get_id());
      meeting->voice_notify_participant(NULL, p);
      p = (Participant *)iter.get_next();
   }

   log_debug(ZONE, "Voice conference reset");
}

int SubMeeting::start(DispatchCall * dispatch)
{
   IUseMutex lock(sm_lock);

   if (state == StateActive)
      return Ok;

   state = StateRegister;

   // Register with voice service
   // N.B. main submeeting registered when meeting is registered.
   if (!is_main())
   {
      char id[32];

      //   MEETINGID, SUBMEETINGID, EXPECTEDPARTICIPANTS
      sprintf(id, "addsm:%s", get_id());
      if (rpc_make_call(id, meeting->get_voice_service(), "voice.register_submeeting", "(ssi)", meeting->get_id(), get_id(), expected_participants))
      {
         log_error(ZONE, "Error notifying voice service about submeeting %s\n", get_id());
         return Failed;
      }

      dispatch_call = dispatch;
      return Pending;
   }

   return Ok;
}

void SubMeeting::on_start_result(RPCResultSet * rs, bool dispatch_event)
{
   IUseMutex lock(sm_lock);

   state = StateActive;

   // Dispatch event
   if (dispatch_event)
      dispatcher.send(SubmeetingAdded, SendSubmeetingState, ToAllParticipants, get_meeting(), this, NULL, NULL);

   if (dispatch_call)
   {
      if (!strcmp(dispatch_call->get_name(), "PlaceCall"))
      {
         PlaceCallDispatch *d = (PlaceCallDispatch *)dispatch_call;
         d->set_submeeting(this);
      }
      dispatch_call->execute(NULL);
      delete dispatch_call;
      dispatch_call = NULL;
   }

   log_debug(ZONE, "Voice conference created");
}

void SubMeeting::on_start_failed()
{
   IUseMutex lock(sm_lock);

   log_error(ZONE, "Failed to start submeeting %s\n", get_id());
   state = StateNone;

   if (dispatch_call)
   {
      dispatch_call->failed();
      delete dispatch_call;
      dispatch_call = NULL;
   }
}

int SubMeeting::end(bool notify_service)
{
   IUseMutex lock(sm_lock);

   IListIterator iter;
   get_participants(iter);

   // Disconnect all participants
   Participant * p = (Participant *)iter.get_first();
   while (p)
   {
      p->disconnect(notify_service);
      p = (Participant *)iter.get_next();
   }

   state = StateNone;

   // Unregister with voice service
   if (notify_service)
   {
      char id[32];

      //   MEETINGID, SUBMEETINGID
      sprintf(id, "closesm:%s", get_id());
      if (rpc_make_call(id, meeting->get_voice_service(), "voice.close_submeeting", "(ss)", meeting->get_id(), get_id()))
      {
         log_error(ZONE, "Error notifying voice service about submeeting %s\n", get_id());
         return Failed;
      }
   }

   if (notify_service)
      dispatcher.send(SubmeetingRemoved, SendSubmeetingState, ToAllParticipants, get_meeting(), this, NULL, NULL);

   log_debug(ZONE, "Closed submeeting %s", get_id());

   return Ok;
}

void SubMeeting::on_end_result(RPCResultSet * rs)
{
   log_debug(ZONE, "Voice conference closed");
}

void SubMeeting::add_participant(Participant *p)
{
   IUseMutex lock(sm_lock);
   participants.add(p);
   p->set_submeeting(this);
}

void SubMeeting::remove_participant(Participant *p, bool release)
{
   IUseMutex lock(sm_lock);
   participants.remove(p);
   if (release)
      p->release();
}

void SubMeeting::on_user_disconnect(Participant * participant, Port * port)
{
   if (port->get_port_type() != MediaVoice)
      return;

   IUseMutex lock(sm_lock);

   spParticipant master = participant->get_call_options() & CallMaster ?
      participant : NULL;

   // Simple: when master call in submeeting hangs up, other calls are terminated also
   if (master)
   {
      log_debug(ZONE, "Master call disconnected in submeeting %s, disconnecting other calls", get_id());

      IListIterator iter(participants);

      spParticipant p = (Participant *)iter.get_first();
      while (p != NULL)
      {
         if ((p != master) && p->get_call_options() & CallSupervised)
         {
            log_debug(ZONE, "Disconnecting voice for participant %s", p->get_id());
            spPort port = p->get_port(MediaVoice);
            if (port != NULL)
               p->disconnect_port(port);
         }

         p = (Participant *)iter.get_next();
      }
   }
}

// Meeting must be locked while iterating.
void SubMeeting::get_participants(IListIterator & iter)
{
   iter.set_list(participants);
}

spParticipant SubMeeting::find_participant(const char * id)
{
   IUseMutex lock(sm_lock);

   IListIterator iter(participants);

   Participant * p = (Participant *)iter.get_first();
   while (p != NULL)
   {
      if (!strcmp(p->get_id(), id))
         return spParticipant(p);

      p = (Participant *)iter.get_next();
   }

   return NULL;
}

int SubMeeting::set_options(unsigned _options)
{
   unsigned changed_options = options ^ _options;
   options = _options;

   if (changed_options & (AnnounceJoinExit|RecordingVoice|EnableLectureMode|LockSubmeeting|EnableHoldMode|EnableChatMode))
   {
      IString id;
      make_id_string(id, "call:", get_id());
      if (rpc_make_call(id,
                        meeting->get_voice_service(), "voice.modify_submeeting", "(ssi)",
                        get_meeting()->get_id(), get_id(), options))
      {
         return Failed;
      }
   }

   return Ok;
}

int SubMeeting::reset_recording()
{
   IString id;
   make_id_string(id, "call:", get_id());
   if (rpc_make_call(id,
                     meeting->get_voice_service(), "voice.reset_recording", "(ss)",
                     get_meeting()->get_id(), get_id()))
   {
      return Failed;
   }

   return Ok;
}

void SubMeeting::set_meeting(Meeting * m)
{
   meeting = m;
}


//====
// Chat
//====

Chat::Chat(const char *_chat_id, spParticipant& _from, int _send_to,spParticipant& _to, const char *_message)
    : chat_id(_chat_id), from(_from), send_to(_send_to), to(_to), message(_message)
{
    sent_time = time(NULL);
}

Chat::~Chat()
{

}

void Chat::lock()
{
   ref_lock.lock();
}

void Chat::unlock()
{
   ref_lock.unlock();
}


//====
// Meeting
//====

int Meeting::submeeting_id_seq = 0;
int Meeting::participant_id_seq = 0;
int Meeting::timer_id_seq = 0;

ChatLogger Meeting::s_clogger;

#define PRESENCE_LOST_TIMEOUT 600000 // 10 min to reconnect after presence loss
#define START_MEETING_TIMEOUT 20000  // 20 sec to complete meeting start
#define MTGARCHIVER_TIMEOUT 5000     // 5 sec to get result back from mtgarchiver
#define END_MEETING_TIMEOUT 10000    // 10 sec to end meeting


Meeting::Meeting(const char *id, const char * _title, int _type, int _privacy, const char * key)
    : meeting_id(id), title(_title)
{
   actual_start_time = 0;
   meeting_type = _type;
   privacy = _privacy;
   priority = 0;
   max_participants = 0;
   max_duration = 0;
   state = StateNone;
   options = 0;
   overide_schedule = false;
   pending_participants = NULL;
   start_time = 0;
   end_time = 0;
   invite_expiration = 0;
   invite_valid_before = 0;
   recurrence = 0;
   recur_end = 0;
   display_attendees = 0;
   invitee_chat_options = 0;
   share_active = false;
   share_connected = false;
   share_document = false;
   submeeting_index = 0;
   starting_release = false;
   meeting_event = -1;
   appshare_event = -1;
   recording_event = -1;
   docshare_event = -1;
   hold_notifications = false;
   chat_index = 0;

   timer = new CTimer(this, timer_id_seq++);
   timer_set = false;

   share_options = 0; // full screen, no meeting window
   share_start_time = 0;

   mtgarc_timer = new CTimer(this, timer_id_seq++);

   appshare_key = key;
}

Meeting::Meeting(spUser & _host, const char *id, int _type, int _privacy, int _priority,
                        const char *_title, const char * _descrip, int _expected_size, int _expected_duration,
                        const char *_password, int _options, int _end_time, int _invite_expiration, int _invite_valid_before,
            const char * _invite_message, int _display_attendees, int _invitee_chat_options, const char * key)
        : meeting_id(id), title(_title), description(_descrip), invite_message(_invite_message), password(_password), host(_host), presenter(_host)
{
    actual_start_time = 0;
    meeting_type = _type;
    privacy = _privacy;
    priority = _priority;
    max_participants = _expected_size;
    max_duration = _expected_duration;
    options = _options;
    state = StateNone;
    overide_schedule = false;
    pending_participants = NULL;
    start_time = 0;
    end_time = 0;
    invite_expiration = _invite_expiration;
    invite_valid_before = _invite_valid_before;
    recurrence = 0;
    recur_end = 0;
    display_attendees = _display_attendees;
    invitee_chat_options = _invitee_chat_options;
    share_active = false;
    share_connected = false;
    share_document = false;
    submeeting_index = 0;
    starting_release = false;
    appshare_event = -1;
    recording_event = -1;
    meeting_event = -1;
    docshare_event = -1;
    hold_notifications = false;
    chat_index = 0;

    timer = new CTimer(this, timer_id_seq++);
    timer_set = false;

    share_options = 0;    // full screen, no meeting window
    share_start_time = 0;

    mtgarc_timer = new CTimer(this, timer_id_seq++);
    docserver = NULL;

    appshare_key = key;
}

Meeting::~Meeting()
{
   log_debug(ZONE, "Deleting meeting %s", get_id());
   call_sm = NULL;
   if (pending_participants != NULL)
      delete pending_participants;
   if (timer != NULL)
      delete timer;
   if (mtgarc_timer != NULL) {
      delete mtgarc_timer;
   }

}

// Save meeting info.
int Meeting::update_meeting_info(int _type, int _privacy, int _priority,
                                 const char * _title, const char * _descrip, int _expected_participants, int _expected_duration,
                                 int _scheduled_start, int _recurrence, int _recur_end, int _options, const char * _password,
                                 int _scheduled_end, int _invite_expiration, int _invite_valid_before, const char * _invite_message,
                                 int _display_attendees, int _invitee_chat_options)
{
   IUseMutex lock(meeting_lock);

   meeting_type = _type;
   privacy = _privacy;
   priority = _priority;
   title = _title;
   description = _descrip;
   invite_message = _invite_message;
   max_participants = _expected_participants;
   max_duration = _expected_duration;
   start_time = _scheduled_start;
   end_time = _scheduled_end;
   invite_expiration = _invite_expiration;
   invite_valid_before = _invite_valid_before,
   recurrence = _recurrence;
   recur_end = _recur_end;
   options = _options;
   display_attendees = _display_attendees;
   invitee_chat_options = _invitee_chat_options;
   password = _password;

   return Ok;
}

int Meeting::toggle_lecture_mode(User * user, int * mode_on)
{
   IUseMutex lock(meeting_lock);

   if (!is_moderator(user))
      return NotAuthorized;

   options ^= LectureMode;

   *mode_on = (options & LectureMode);

   spSubMeeting main_sm = find_mainmeeting();
   dispatcher.send(MeetingModified, SendMeetingInfo, ToAllParticipants, this, main_sm, NULL, NULL);

   int main_opt = main_sm->get_options();
   if (options & LectureMode)
   {
      main_opt = (main_opt & ~AnnounceJoinExit) | EnableLectureMode;
   }
   else
   {
      main_opt = (main_opt & ~EnableLectureMode) | AnnounceJoinExit;
   }
   main_sm->set_options(main_opt);

   int res = set_lecture_mute(main_sm, options & LectureMode);
   if (res != Ok)
      return res;

   return Ok;
}

int Meeting::toggle_hold_mode(User * user, int * mode_on)
{
   IUseMutex lock(meeting_lock);

   if (!is_moderator(user))
      return NotAuthorized;

   options ^= HoldMode;

   *mode_on = (options & HoldMode);

   spSubMeeting main_sm = find_mainmeeting();
   dispatcher.send(MeetingModified, SendMeetingInfo, ToAllParticipants, this, main_sm, NULL, NULL);

   int main_opt = main_sm->get_options();
   if (options & HoldMode)
   {
      main_opt = (main_opt & ~AnnounceJoinExit) | EnableHoldMode;
      if (options & RecordingMeeting)
         main_opt &= ~RecordingVoice;
   }
   else
   {
      main_opt = (main_opt & ~EnableHoldMode) | AnnounceJoinExit;
      if (options & RecordingMeeting)
         main_opt |= RecordingVoice;
   }
   main_sm->set_options(main_opt);

   int res = set_hold_mute(main_sm, options & HoldMode);
   if (res != Ok)
      return res;

   if (options & RecordingMeeting)
   {
      enable_data_recording(user, !*mode_on);
   }

   return Ok;
}

int Meeting::toggle_record_meeting(User * user, int * mode_on)
{
   IUseMutex lock(meeting_lock);

   if (!is_moderator(user))
      return NotAuthorized;

   options ^= RecordingMeeting;

   User * host = get_host();
   if (options & RecordingMeeting)
   {
      recording_event = log_event_start(EvnLog_Event_Conference_Recording,
                                        get_community_id(),
                                        get_id(),
                                        (time_t)get_actual_start(),
                                        (host ? host->get_id() : ""),
                                        (user ? user->get_id() : ""),
                                        "" /* no extra info */);
   }
   else
   {
      if (recording_event != -1)
         log_event_end(recording_event);
      else
      {
         log_error(ZONE, "Ended recording event w/o starting it!");
      }
      recording_event = -1;
   }

   *mode_on = (options & RecordingMeeting);

   spSubMeeting targ_sm;
   if (call_sm)
   {
      spParticipant p = find_participant_by_userid(user->get_id());
      targ_sm = p->get_submeeting();
   }
   else
   {
      targ_sm = find_mainmeeting();
   }

   // Don't start recording if in hold mode
   if (*mode_on && options & HoldMode)
      return Ok;

   int targ_opt = targ_sm->get_options();
   targ_opt = (*mode_on ?
               targ_opt|RecordingVoice : targ_opt & ~RecordingVoice);
   targ_sm->set_options(targ_opt);

   dispatcher.send(MeetingModified, SendMeetingInfo, ToAllParticipants, this, targ_sm, NULL, NULL);

   // ----------------------------------------   record data sharing  -------------------------------------
   int status = enable_data_recording(user, *mode_on);
   if (status)
      return status;
   // ----------------------------------------   record data sharing  -------------------------------------

   return Ok;
}

int Meeting::enable_recording(int enabled)
{
   char id[64];
   sprintf(id, "enable_recording:%s", get_id());
   if (rpc_make_call(id, mtgarchiver->get_name(),
                     "mtgarchiver.enable_recording", "(si)", get_id(), enabled))
   {
      return Failed;
   }
   mtgarc_timer->cancel();
   mtgarc_timer->set_timeout(TimerMtgArchiverSending, MTGARCHIVER_TIMEOUT+master.number_meetings()*10);

   if (share_server)
   {
       share_server->send_recording(get_id(), enabled, share_start_time);
   }

   return Ok;
}

int Meeting::enable_data_recording(User * user, int  enabled)
{
   IUseMutex lock(meeting_lock);

   if (!is_moderator(user))
      return NotAuthorized;

   // ----------------------------------------   record data sharing  -------------------------------------
   if (mtgarchiver==NULL) {
      mtgarchiver = master.get_archiver();
   }
   if (mtgarchiver)
   {
      int status = enable_recording(enabled);
      if (status==Failed)
         return Failed;
   }
   else if (enabled)
   {
      spParticipant p = find_participant_by_userid(user->get_id());
      if (p)
      {
         dispatcher.send(RecordingNotAvailable, SendEventOnly, ToOneParticipant, this, p->get_submeeting(), p, p);
      }
   }
   // ----------------------------------------   record data sharing  -------------------------------------

   return Ok;
}

int Meeting::toggle_mute_join_leave(User * user, int * mode_on)
{
   IUseMutex lock(meeting_lock);

   if (!is_moderator(user))
      return NotAuthorized;

   options ^= MuteJoinLeave;

   *mode_on = (options & MuteJoinLeave);

   spSubMeeting main_sm = find_mainmeeting();
   if (main_sm)
   {
      int main_opt = main_sm->get_options();
      main_opt = (*mode_on ?
                  main_opt & ~AnnounceJoinExit : main_opt | AnnounceJoinExit);

      main_sm->set_options(main_opt);

      dispatcher.send(MeetingModified, SendMeetingInfo, ToAllParticipants, this, main_sm, NULL, NULL);
   }

   return Ok;
}

int Meeting::lock_meeting(User * user, int state)
{
   IUseMutex lock(meeting_lock);

   if (!is_moderator(user))
      return NotAuthorized;

   if (state)
   {
      options |= LockParticipants;
   }
   else
   {
      options &= ~LockParticipants;
   }

   spSubMeeting main_sm = find_mainmeeting();

   // Dispatch to voice bridge
   if (main_sm)
   {
      int main_opt = main_sm->get_options();
      main_opt = (state ?
          main_opt | LockSubmeeting : main_opt & ~LockSubmeeting);

      main_sm->set_options(main_opt);
  }

   dispatcher.send(MeetingModified, SendMeetingInfo, ToAllParticipants, this, main_sm, NULL, NULL);

   return Ok;
}


int Meeting::modify_meeting(User * user, int _type, int _privacy, int _priority, const char *_title, const char * _descrip,
                            const char *_password, int _options, int _invite_expiration, int _invite_valid_before,
                            const char * _invite_message, int _display_attendees, int _invitee_chat_options, const char * key)
{
   IUseMutex lock(meeting_lock);

   int  changed_options = options ^ _options;
   bool bDisplayChanged = display_attendees != _display_attendees;

   if (!is_moderator(user))
      return NotAuthorized;

   meeting_type = _type;
   privacy = _privacy;
   priority = _priority;
   title = _title;
   description = _descrip;
   password = _password;
   options = _options;
   invite_expiration = _invite_expiration;
   invite_valid_before = _invite_valid_before;
   invite_message = _invite_message;
   display_attendees = _display_attendees;
   invitee_chat_options = _invitee_chat_options;
   if (key)
      appshare_key = key;

   // Broadcast event to clients (hold until moderator joins?)
   spSubMeeting main_sm = find_mainmeeting();

   // Notify everyone about the modified meeting
   dispatcher.send(MeetingModified, SendMeetingInfo, ToAllParticipants, this, main_sm, NULL, NULL);

   if (bDisplayChanged)
   {
      // Display options changed, reset non-moderator clients with appropriate meeting/participant view
      dispatcher.send(MeetingReset, SendMeetingState, ToNonModeratorParticipants, this, main_sm, NULL, NULL);
   }

   int main_opt = 0, prev_main_opt;
   prev_main_opt = main_opt = main_sm->get_options();
   if (main_sm && (changed_options & (LectureMode|MuteJoinLeave|LockParticipants|HoldMode)))
   {
      // lecture mode changes affect only the main submeeting
      if (changed_options & LectureMode)
      {
         int res = set_lecture_mute(main_sm, options & LectureMode);
         if (res != Ok)
            return res;
      }

      if (options & LectureMode)
      {
         main_opt |= EnableLectureMode;
      }
      else
      {
         main_opt &= ~EnableLectureMode;
      }

      // hold mode handled same way as lecture more
      if (changed_options & HoldMode)
      {
         int res = set_hold_mute(main_sm, options & HoldMode);
         if (res != Ok)
            return res;
      }

      if (options & HoldMode)
      {
         main_opt |= EnableHoldMode;
      }
      else
      {
         main_opt &= ~EnableHoldMode;
      }

      if (options & (LectureMode|HoldMode|MuteJoinLeave))
      {
         // don't announce entry/exit of participants
         // either when in lecture mode or when the mute join/leave
         // flag is set.
         main_opt &= ~AnnounceJoinExit;
      }
      else
      {
         main_opt |= AnnounceJoinExit;
      }

      if (options & LockParticipants)
      {
         main_opt |= LockSubmeeting;
      }
      else
      {
         main_opt &= ~LockSubmeeting;
      }
   }

   if (changed_options & ChatMode)
   {
      log_debug(ZONE, "Chat mode %s", (options & ChatMode) ? "enabled" : "disabled");
      hold_notifications = options & ChatMode;

      if (options & ChatMode)
         main_opt |= EnableChatMode;
      else
         main_opt &= ~EnableChatMode;
   }

   spSubMeeting targ_sm = NULL;
   int targ_opt, prev_targ_opt;
   if (changed_options & RecordingMeeting)
   {
      int enabled = options & RecordingMeeting;

      if (enabled)
      {
         recording_event = log_event_start(EvnLog_Event_Conference_Recording,
                                           get_community_id(),
                                           get_id(),
                                           (time_t)get_actual_start(),
                                           (host ? host->get_id() : ""),
                                           (user ? user->get_id() : ""),
                                           "" /* no extra info */);
      }
      else
      {
         if (recording_event != -1)
            log_event_end(recording_event);
         else
         {
            log_error(ZONE, "Ended recording event w/o starting it!");
         }
         recording_event = -1;
      }

      if (call_sm)
      {
         // 1-to-1 call; use the submeeting the caller is in
         spParticipant p = find_participant_by_userid(user->get_id());
         targ_sm = p->get_submeeting();
         if (targ_sm)
         {
            prev_targ_opt = targ_opt = targ_sm->get_options();
         }
      }
      else
      {
         // "normal" meeting; record the main meeting
         targ_sm = main_sm;
         targ_opt = main_opt;
      }

      if (targ_sm)
      {
         if (enabled)
         {
            // Only enable recording if not in hold mode.
            if (!(options & HoldMode))
                targ_opt |= RecordingVoice;
         }
         else
         {
            targ_opt &= ~RecordingVoice;
         }
         if (targ_sm == main_sm)
         {
            main_opt = targ_opt;
         }
      }

      // -------------   record data sharing  -----------------
      if (!enabled || (enabled && !(options & HoldMode)))
      {
         // Either disabling recording, or enabling while not in hold mode
         int status = enable_data_recording(user, enabled);
         if (status)
            return status;
      }
      // -------------   record data sharing  ------------------
   }

   // If we are entering or exiting hold mode we may need to stop or restart recording.
   // However, if we also changed recording options then recording should have been set up above.
   if ((changed_options & HoldMode) && !(changed_options & RecordingMeeting) && (options & RecordingMeeting))
   {
       log_debug(ZONE, "%s recording due to hold mode", (options & HoldMode) ? "pausing" : "resuming");
       if (options & HoldMode)
       {
          // entering hold mode, turn recording off
          enable_data_recording(user, 0);
          main_opt &= ~RecordingVoice;
       }
       else
       {
          // leaving hold mode, turn recording back on
          enable_data_recording(user, 1);
          main_opt |= RecordingVoice;
       }
   }

   if (changed_options & ScreenCallers)
   {
      if (options & ScreenCallers)
         main_opt |= EnableScreenCallers;
      else
         main_opt &= ~EnableScreenCallers;
   }

   if (main_sm && (prev_main_opt ^ main_opt /* i.e. some changed */))
   {
      main_sm->set_options(main_opt);
   }

   if (targ_sm && targ_sm != main_sm && (prev_targ_opt ^ targ_opt))
   {
      targ_sm->set_options(targ_opt);
   }

   return Ok;
}


int Meeting::modify_recording(User * user, int _cmd, const char* email) // Stateless command
{
   IUseMutex lock(meeting_lock);

   if (!is_moderator(user))
      return NotAuthorized;

   if (_cmd == PreviewRecording)
   {
      if (mtgarchiver==NULL) {
         mtgarchiver = master.get_archiver();
      }
      if (mtgarchiver)
      {
         if (rpc_make_call("preview_recording", mtgarchiver->get_name(),
                           "mtgarchiver.preview_recording", "(ss)", get_id(), email))
         {
            return Failed;
         }
      }
   }
   else if (_cmd == ResetRecording)
   {
      // Broadcast event to clients (hold until moderator joins?)
      spSubMeeting main_sm = find_mainmeeting();
      spSubMeeting targ_sm = NULL;

      if (call_sm)
      {
         // 1-to-1 call; use the submeeting the caller is in
         spParticipant p = find_participant_by_userid(user->get_id());
         targ_sm = p->get_submeeting();
      }
      else
      {
         // "normal" meeting; record the main meeting
         targ_sm = main_sm;
      }

      if (targ_sm)
      {
         targ_sm->reset_recording();
      }

      if (mtgarchiver==NULL) {
         mtgarchiver = master.get_archiver();
      }
      if (mtgarchiver)
      {
         if (rpc_make_call("reset_recording", mtgarchiver->get_name(),
                           "mtgarchiver.reset_recording", "(s)", get_id()))
         {
            return Failed;
         }
      }
   }

   return Ok;
}


int Meeting::send_chat_message(const char * sessionid,
                               const char * meetingid,
                               int send_to,
                               const char * message,
                               const char * from_participantid,
                               const char * to_participantid)
{
   IUseMutex lock(meeting_lock);

   // See if sending user is in this meeting.
   spParticipant to = find_participant(to_participantid);
   spParticipant from = find_participant(from_participantid);
   if (from == NULL || (to == NULL && send_to == ToOneParticipant))
      return NotMember;

   spSubMeeting main_sm = find_mainmeeting();

   // Notify everyone about the meeting chat
   dispatcher.send(MeetingChat, SendMeetingChat, send_to, this, main_sm, from, to, message);

   if (options & ChatMode)
   {
       char buff[32];
       sprintf(buff, "%d", ++chat_index);

       Chat *chat = new Chat(buff, from, send_to, to, message);
       chats.add(chat);
       // ToDo: limit size of chat buffer
   }

   static bool check_log = false;
   static bool log_chat = false;
   if (!check_log)
   {
      struct stat st;
      if (stat("/var/iic/chatlog", &st) == 0)
      {
         if (S_ISDIR(st.st_mode))
         {
            log_chat = true;
         }
         else
         {
            log_error(ZONE, "/var/iic/chatlog is not a directory");
         }
      }
      else if (errno != ENOENT)
      {
         log_error(ZONE, "Couldn't see if /var/iic/chatlog exists: %s",
                   strerror(errno));
      }
      check_log = true;
   }

   if (log_chat)
   {
      s_clogger.log_chat(from->get_id(),
                         from->get_invited_name(),
                         send_to,
                         (to != NULL ? to->get_id() : ""),
                         (to != NULL ? to->get_invited_name() : ""),
                         message);
   }

   // ----------------------------------------   record data sharing  -------------------------------------
   if (send_to == ToAllParticipants || send_to == ToHostParticipant || send_to == ToModeratorParticipants ||
       from->is_moderator() || (to && to->is_moderator()))
   {
      const char *log_to = "";
      switch (send_to) {
          case ToHostParticipant:
            log_to = _("host");
            break;
          case ToModeratorParticipants:
            log_to = _("host and moderators");
            break;
          case ToAllParticipants:
            break;
          default:
            if (to)
              log_to = to->get_name();
      }
      if (mtgarchiver)
      {
         if (rpc_make_call("recordchat", mtgarchiver->get_name(),
                           "mtgarchiver.on_chat", "(ssss)",
                           get_id(), message, from->get_name(), log_to))
         {
            return Failed;
         }
      }
   }
   // ----------------------------------------   record data sharing  -------------------------------------

   return Ok;
}

void Meeting::dispatch_chats(const char *participant_id)
{
   IUseMutex lock(meeting_lock);

   spSubMeeting main_sm = find_mainmeeting();
   spParticipant dispatch_to = find_participant(participant_id);

   IListIterator iter(chats);
   Chat *chat = (Chat*)iter.get_first();
   while (chat)
   {
      // Send chat log to new user if it involves him in any way
      if (chat->to == dispatch_to ||
          chat->send_to == ToAllParticipants ||
          (chat->send_to == ToHostParticipant && dispatch_to->get_roll() == RollHost) ||
          (chat->send_to == ToNonModeratorParticipants && !dispatch_to->is_moderator()) ||
          (chat->send_to == ToModeratorParticipants && dispatch_to->is_moderator()))
      {
         char time_str[32];
         sprintf(time_str, "s[%d,%d]:", chat->sent_time, chat->send_to);
         IString msg = IString(time_str) + chat->message;
         dispatcher.send(MeetingChat, SendMeetingChat, ToOneParticipant, this, main_sm, chat->from, dispatch_to, msg);
      }

      chat = (Chat*)iter.get_next();
   }
}

void Meeting::lock()
{
   ref_lock.lock();
}

void Meeting::unlock()
{
   ref_lock.unlock();
}

void Meeting::release(bool release_immediate)
{
   if (release_immediate)
   {
       release_internal();
       return;
   }

   IUseMutex lock(meeting_lock);

   // We keep the meeting data around for a bit longer to allow last presenter
   // to upload data if needed (web server authenticates access to meeting)
   timer->set_timeout(TimerMeetingEnd, END_MEETING_TIMEOUT);
}

// Release all submeetings and unregister from controller.
void Meeting::release_internal()
{
   IUseMutex lock(meeting_lock);

   IListIterator iter(submeetings);
   SubMeeting *sm = (SubMeeting*)iter.get_first();
   while (sm)
   {
      sm->release();
      sm = (SubMeeting*)iter.get_next();
   }

   submeetings.remove_all();

   if (share_server != NULL)
   {
      appshare.release_session(share_server, meeting_id);
      share_server = NULL;
   }

   if (mtgarchiver != NULL)
   {
      master.release_archiver(mtgarchiver);
      mtgarchiver = NULL;
   }

   if (docserver != NULL)
   {
      docshare.release_session(docserver);
      docserver = NULL;
   }

   share_active = false;
   share_document = false;
   share_connected = false;

   state = StateDead;

   timer->cancel();
   mtgarc_timer->cancel();

   log_debug(ZONE, "Meeting %s released", get_id());

   master.unregister_meeting(this);
}

// A service was restarted, see if it effects this meeting.
void Meeting::reset(Service * service)
{
   Address service_addr;

   if (service == NULL)
   {
      // We remove our notion of this meeting, but do not notify voice server.
      // Voice will re-register these calls when it realizes the controller has been reset.
      log_debug(ZONE, "Meeting %s is being ended by service reset", get_id());
      end_internal(NULL, false);
      release(true);
      return;
   }

   if (parse_address(service->get_service_id(), &service_addr) != Ok)
   {
      log_debug(ZONE, "Invalid service address, can't reset meeting: %s", service->get_service_id());
      return;
   }

   if (service_addr.type == AddressVoice && voice_service == service_addr.id)
   {
      log_error(ZONE, "Voice bridge associated with meeting %s has been restarted", get_id());

      // Reregister meeting (& submeetings) with voice server
      if (voice_register("reset:", NULL))
        return;
   }

   if (service_addr.type == AddressMtgArc /* && mtgarc_service == service_addr.id */)
   {
      log_error(ZONE, "Resetting meeting %s on archiver", get_id());
      const char *host_email = "";
      if (host != NULL)
      {
         host_email = host->get_email(SelEmail1);
         if (host_email == NULL || *host_email == '\0')
         {
            host_email = host->get_email(SelEmail2);
         }
         if (host_email == NULL || *host_email == '\0')
         {
            host_email = host->get_email(SelEmail3);
         }
         if (host_email == NULL || *host_email == '\0')
         {
            host_email = "";
         }
      }
      const char * r = "";
      const char * p = "";
      if (share_server)
      {
         r = share_server->get_address().get_string();
         p = share_server->get_password().get_string();
      }
      if (mtgarchiver==NULL) {
         mtgarchiver = master.get_archiver();
      }
      if (mtgarchiver)
      {
         if (rpc_make_call("mtgarcreg", mtgarchiver->get_name(),
                           "mtgarchiver.register_meeting", "(sissssis)",
                           get_id(),
                           actual_start_time,
                           host_email,
                           (const char *)voice_service,
                           r,
                           p,
                           share_options,
                           (const char *)appshare_key))
         {
            log_error(ZONE,
                      "Error notifying recorder service about meeting %s",
                      get_id());
         }
      }
   }
}

void Meeting::on_reset_result(RPCResultSet * rs)
{
   log_debug(ZONE, "Meeting re-registered with voice");

   IListIterator iter;
   get_submeetings(iter);

   SubMeeting *sm = (SubMeeting*)iter.get_first();
   while (sm)
   {
      sm->reset();
      sm = (SubMeeting*)iter.get_next();
   }
}

// Starts sequence for starting new meeting.
// If participant id is specified, it must be validated by caller first.
int Meeting::start(User * user, Port * port, const char * participant_id, const char * password, IList * participants, bool create_meeting, bool release)
{
   IUseMutex lock(meeting_lock);
   IString id;

   if (state == StateActive || state == StateHolding)
   {
      return Ok;
   }
   else if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateNone)
   {
      return MeetingExists;
   }

   if (create_meeting)
   {
      // Save participant list & start loading meeting.
      pending_participants = participants;
      overide_schedule = true;
   }

   starting_user = user;
   starting_port = port;
   starting_participant_id = participant_id;
   starting_password = password;
   starting_release = release;
   state = StateLoading;

   timer->set_timeout(TimerMeetingStart, START_MEETING_TIMEOUT);

   // Start loading info from schedule db
   make_id_string(id, "load:", get_id());
   if (rpc_make_call(id, "addressbk", "schedule.find_meeting_details", "(ss)", user->get_id(), get_id()))
   {
      log_error(ZONE, "Error fetching meeting info for %s\n", get_id());
      return Failed;
   }

   // Allocate data share session
   share_active = false;
   share_document = false;
   share_connected = false;
   share_server = appshare.allocate_session(max_participants, meeting_id);

   mtgarchiver = master.get_archiver();

   docserver = docshare.allocate_session(max_participants);

   return Pending;
}

// Meeting info loaded
// Meeting is stopped if error status returned
int Meeting::on_load_result(RPCResultSet * rs)
{
   int num_rows = 0;
   IString hostid;
   IString id;
   char *meetingid, *dbhostid, *type, *privacy, *priority, *title, *descrip, *size, *duration, *start_time, *recurrence, *recur_end, *options, *password;
   char *end_time, *invite_expiration, *invite_valid_before, *invite_message, *display_attendees, *invitee_chat_options, *communityid;
   char *partid, *ptype, *roll, *userid, *username, *name, *descrip2, *selphone, *phone, *selemail, *email, *screen, *aim, *notify;
   char *community_phone, *community_url, *enabled_features;
   bool res;
   char buff[32];
   bool is_mod;

   IUseMutex lock(meeting_lock);

   if (state != StateLoading)
   {
      log_error(ZONE, "Meeting info loaded, but was not in correct state");
      return Ok;
   }

   num_rows = rs->get_chunk_size();

   // Extra row in result set is meeting descriptor
   // "MEETINGID", "HOSTID", "TYPE", "PRIVACY", "PRIORITY", "TITLE", "DESCRIPTION" ,"SIZE", "DURATION", "TIME", "RECURRENCE", "RECUR_END", "OPTIONS", "PASSWORD"
   // "END_TIME", "INVITE_EXPIRATION", "INVITE_MESSAGE", "DISPLAY_ATTENDEES", "INVITEE_CHAT_OPTIONS", "COMMUNITYID",
   // "BRIDGEPHONE", "SYSTEMURL", "ENABLED_FEATURES"
   res = rs->get_values(num_rows, "(ssssssssssssssssssssssss*)", &meetingid, &dbhostid, &type, &privacy, &priority, &title, &descrip, &size,
                        &duration, &start_time, &recurrence, &recur_end, &options, &password,
                        &end_time, &invite_expiration, &invite_valid_before, &invite_message, &display_attendees,
                        &invitee_chat_options, &communityid, &community_phone, &community_url, &enabled_features);
   if (!res)
   {
      log_error(ZONE, "Error parsing result from fetch meeting info\n");
      return Failed;
   }
   make_id_string(hostid, "user:", dbhostid);

   // Check user's access to this meeting (always determined from stored schedule)
   is_mod = starting_user == host || check_access(rs, starting_user);

   // Prevent user from starting another user's meeting (and hijacking their pin)
   if (overide_schedule == true && !is_mod)
   {
      log_error(ZONE, "User %s attempted to start meeting %s\n, but is not a moderator", starting_user->get_id(), meetingid);
      return NotAuthorized;
   }

   // Create main submeeting
   spSubMeeting main_sm = find_mainmeeting();
   if (main_sm == NULL)
   {
      sprintf(buff, "submeeting:%d", ++submeeting_id_seq);

      main_sm = new SubMeeting(buff, this, atoi(size), true, "Main");
      submeetings.add(main_sm);

      master.register_submeeting(main_sm);

      main_sm->dereference();
   }

   // Create submeeting for direct call if needed.
   if (is_direct_call(pending_participants))
   {
      char sm_title[32];

      sprintf(buff, "submeeting:%d", ++submeeting_id_seq);
      sprintf(sm_title, "Submeeting %d", ++submeeting_index);

      call_sm = new SubMeeting(buff, this, 2, false, sm_title);
      submeetings.add(call_sm);

      master.register_submeeting(call_sm);

      call_sm->dereference();
   }

   // Load participants from schedule.
   // (No participants are returned if we are overiding schedule settings)
   for (int i = 0; !overide_schedule && (i < num_rows); i++)
   {
      // "PARTICIPANTID", "TYPE", "ROLL", "USERID", "USERNAME", "NAME", "DESCRIPTION", "SELPHONE", "PHONE", "SELEMAIL", "EMAIL",
      // "SCREENNAME", "AIMNAME", "NOTIFYTYPE"
      if (!rs->get_values(i, "(ssssssssssssss)",
                          &partid, &ptype, &roll, &userid, &username, &name, &descrip2, &selphone, &phone, &selemail, &email, &screen, &aim, &notify))
      {
         log_error(ZONE, "Error parsing result from fetch meeting info\n");
         return Failed;
      }

      // Figure out best user id
      char buff[32];
      bool use_userid = atoi(userid) > 0;
      sprintf(buff, "%s:%s", use_userid ? "user" : "part", use_userid ? userid : partid);

      spUser target_user = master.create_user(buff, username, name);
      if (target_user == NULL)
      {
         log_error(ZONE, "Unable to add user %s to meeting %s\n", buff, meetingid);
         return Failed;
      }

      // Add participant
      char pid[32];
      sprintf(pid, "part:%s", partid);

      spParticipant dummy;
      add_participant_internal(starting_user, main_sm, target_user, pid, atoi(ptype), atoi(roll), name, atoi(selphone), phone,
                               atoi(selemail), email, screen, descrip2, atoi(notify), 0, false, dummy);

      log_debug(ZONE, "Added participant %s to meeting %s", userid, meetingid);
   }

   // Find host.
   host = master.create_user(hostid);

   // The starting user must at least be a participant in this meeting.
   if (!overide_schedule && !is_participant(starting_user) && (this->privacy != PrivacyPrivate))
   {
      log_error(ZONE, "User %s attempted to start meeting %s but is not a participant\n", starting_user->get_id(), meetingid);
      return NotAuthorized;
   }

   // Starting user is default presenter
   if (overide_schedule)
      presenter = starting_user;
   else
      presenter = host;

   // See if starting user is a chat client
   int extra_options = 0;
   if (starting_port && starting_port->get_port_type() == MediaData)
   {
      const char * addr = starting_port->get_id();
      const char * rsrc = strrchr(addr, '/');
      if (rsrc != NULL && strncmp(rsrc, "/cht", 4) == 0)
      {
         log_debug(ZONE, "Chat client is starting meeting");
         extra_options |= ChatMode;
      }
   }

   // Update meeting with loaded info
   community_id = communityid;
   bridge_phone = community_phone;
   system_url = community_url;
   hosts_enabled_features = atoi(enabled_features);
   if (!overide_schedule)
   {
      update_meeting_info(atoi(type), atoi(privacy), atoi(priority), title, descrip, atoi(size),
                          atoi(duration), atoi(start_time), atoi(recurrence), atoi(recur_end), atoi(options)|extra_options, password,
                          atoi(end_time), atoi(invite_expiration), atoi(invite_valid_before), invite_message, atoi(display_attendees), atoi(invitee_chat_options));
   }

   // Figure out which voice bridge is configured for this meeting.
   int status = master.find_bridge_by_phone(bridge_phone, voice_service);
   if (IS_FAILURE(status))
   {
      log_error(ZONE, "No voice bridge configured for phone number %s", bridge_phone.get_string());
      return status;
   }

    // Find VOIP address for bridge, if configured (may get changed by voice service later)
    spService bridge = master.find_service(IString("voice:") + voice_service);
    if (bridge != NULL)
       set_bridge_address(bridge->get_ext_address());


   log_debug(ZONE, "Completed loading info for meeting %s", meetingid);

   state = StateNotify;

    // Log
    actual_start_time = time(NULL);
    meeting_event = log_event_start(EvnLog_Event_Meeting,
                                get_community_id(),
                                get_id(),
                                (time_t)get_actual_start(),
                                get_host()->get_id(),
                                starting_participant_id.get_string(),
                                "");

   // Now start call to notify voice service
   if (voice_register("create:", call_sm))
      return Failed;

    // ----------------------------------------   record data sharing  -------------------------------------
    const char *host_email = "";
    if (host != NULL)
    {
        host_email = host->get_email(SelEmail1);
        if (host_email == NULL || *host_email == '\0')
        {
            host_email = host->get_email(SelEmail2);
        }
        if (host_email == NULL || *host_email == '\0')
        {
            host_email = host->get_email(SelEmail3);
        }
        if (host_email == NULL || *host_email == '\0')
        {
            host_email = "";
        }
    }
    const char * r = "";
    const char * p = "";
    if (share_server)
    {
        r = share_server->get_address().get_string();
        p = share_server->get_password().get_string();
    }

    if (mtgarchiver)
    {
            if (rpc_make_call("setvoicerecordingid", mtgarchiver->get_name(),
                                              "mtgarchiver.register_meeting", "(sissssis)",
                                              get_id(),
                                              actual_start_time,
                                              host_email,
                                              (const char *)voice_service,
                                              r,
                                              p,
                                              share_options,
                      (const char *)appshare_key))
            {
                    log_error(ZONE, "Error notifying recorder service about meeting %s", get_id());
                    return Failed;
            }
    }
    // ----------------------------------------   record data sharing  -------------------------------------

   return Ok;
}

// Voice conference registered.
// Meeting is stopped if error status returned.
int Meeting::on_create_result(RPCResultSet * rs)
{
   IString id;
   char *voice_bridge_ip = "";
   int status = Ok;
   bool parsed;

   IUseMutex lock(meeting_lock);

   if (state != StateNotify)
   {
      log_error(ZONE, "Meeting registered, but was not in correct state");
      return Ok;
   }

   // Allow older voice bridge API
   if (rs->get_output_count() == 1)
   {
      parsed = rs->get_output_values("(i)", &status);
   }
   else
   {
      parsed = rs->get_output_values("(is)", &status, &voice_bridge_ip);
   }

   if (!parsed)
   {
      log_error(ZONE, "Error parsing result from register meeting\n");
      return Failed;
   }

   if (status != Ok)
   {
      log_error(ZONE, "Error creating voice conference (%d)\n", status);
      return Failed;
   }

   // Main submeeting registration was piggy backed on meeting start request.
   spSubMeeting main_sm = find_mainmeeting();
   main_sm->on_start_result(NULL, false);
   if (call_sm)
      call_sm->on_start_result(NULL, false);

   log_debug(ZONE, "Voice conference created for meeting %s", get_id());

   // Set the address of the voip server if the value returned from the
   // register_meeting message is non-empty.
   if (voice_bridge_ip[0] != '\0')
       set_bridge_address(voice_bridge_ip);

   // Meeting is now holding for moderator
   state = StateHolding;

   timer->cancel();

   // Broadcast event to clients
   dispatcher.send(MeetingPending, SendMeetingState, ToAllParticipants, this, main_sm, NULL, NULL);

   // Notify schedule that this meeting is in progress
   make_id_string(id, "statusstarted:", get_id());
   log_debug(ZONE, "Make status call");
   int meetingstatus = StatusInProgress;
   time_t currenttime = time(NULL);
   actual_start_time = (int)currenttime;

   if (rpc_make_call(id, "addressbk", "schedule.insert_meeting_status", "(siii)", get_id(), meetingstatus, actual_start_time, 0))
   {
      log_error(ZONE, "Error notifying schedule service that meeting is in progress %s\n", get_id());
   }

   // Notify voice bridge about scheduled participants
   if (!overide_schedule)
   {
      IListIterator iter;
	  main_sm->get_participants(iter);
	  Participant * part = (Participant *) iter.get_first();
	  while (part != NULL)
      {
         voice_notify_participant(starting_user, part);
         part = (Participant *) iter.get_next();
      }
   }

   // Add any queued participnants
   if (pending_participants != NULL)
   {
      add_participants(starting_user, pending_participants, call_sm);
      // todo: shouldn't these be deleted?
      pending_participants = NULL;
   }

   // Turn on hold or lecture mode if requested
   if (options & HoldMode)
   {
      int res = set_hold_mute(main_sm, options & HoldMode);
      if (res != Ok)
         log_error(ZONE, "Failed to set hold mode (%d)", res);
      main_sm->set_options((main_sm->get_options() & ~AnnounceJoinExit) | EnableHoldMode);
   }
   else if (options & LectureMode)
   {
      int res = set_lecture_mute(main_sm, options & LectureMode);
      if (res != Ok)
         log_error(ZONE, "Failed to set lecture mode (%d)", res);
      main_sm->set_options((main_sm->get_options() & ~AnnounceJoinExit) | EnableLectureMode);
   }

   if (options & ChatMode)
   {
      main_sm->set_options(main_sm->get_options() | EnableChatMode);
   }

   // Join starting user.
   status = join_user(starting_user, starting_port, starting_participant_id, starting_password, starting_release);

   // Release any pending calls (probably other joins)
   pending_queue.process_all();

   // Make sure meeting is valid, we want to see at least one connected participant.
   int num_connected, num_moderators, num_invited, num_calls, num_app_share, num_licenses;
   get_meeting_counts(num_connected, num_moderators, num_invited, num_calls, num_app_share, num_licenses);

   // No one got in, stop meeting now.
   if (num_connected == 0)
      return status;
   else
      return Ok;
}

void Meeting::on_start_failed(int status)
{
   IString id;
   IString id2;

   IUseMutex lock(meeting_lock);

   state = StateEnding;

   timer->cancel();

   // If someone was waiting for us to start, let them know about failure (voice or data).
   if (starting_user != NULL)
   {
      starting_user->send_error(starting_port, "join", status, "Unable to start meeting", get_id(), "");
      starting_user = NULL;
   }

   // Main submeeting registration was piggy backed on meeting start request.
   spSubMeeting main_sm = find_mainmeeting();
   if (main_sm)
      main_sm->on_start_failed();
   if (call_sm)
      call_sm->on_start_failed();

   // Remove voice conference.
   voice_close();

   // meeting is ended, notify the mtgarchiver to shutdown recorder
   // ----------------------------------------   record data sharing  -------------------------------------
   if (rpc_make_call("stoprecording",
                     "mtgarchiver", "mtgarchiver.stop_recording", "(ssii)", get_id(), "Meeting Failed to start", FALSE, FALSE))  // no way to recover participants?
   {
      log_error(ZONE, "Error notifying mtgarchiver about closed meeting %s\n", get_id());
   }
   // ----------------------------------------   record data sharing  -------------------------------------

   // Notify that we failed
   dispatcher.send(Error, SendEventOnly, ToAllParticipants, this, NULL, NULL, NULL);

   // Notify schedule that this meeting is completed
   make_id_string(id2, "statuscompleted:", get_id());
   log_debug(ZONE, "Make status completed call");
   int meetingstatus = StatusCompleted;
   time_t currenttime = time(NULL);

   if (rpc_make_call(id2, "addressbk", "schedule.insert_meeting_status", "(siii)", get_id(), meetingstatus, actual_start_time, (int)currenttime))
   {
      log_error(ZONE, "Error notifying schedule service that meeting is completed: %s\n", get_id());
   }

   // Cancel any pending calls.
   pending_queue.cancel_all();

   // Log
   if (meeting_event != -1)
   {
      log_event_end(meeting_event);
      meeting_event = -1;
   }

   lock.unlock();

   release();
}

void Meeting::on_recording_result(int status)
{
   IUseMutex lock(meeting_lock);

   mtgarc_timer->cancel();

   if (IS_FAILURE(status))
   {
      if (presenter != NULL)
      {
         spParticipant cur_presenter = find_participant_by_userid(presenter->get_id());
         if (cur_presenter != NULL)
         {
            dispatcher.send(RecordingNotAvailable, SendAppShare, ToOneParticipant, this, cur_presenter->get_submeeting(), cur_presenter, cur_presenter);

         }
      }
   }

   lock.unlock();
}

// Verify that user has moderator access to scheduled meeting.
bool Meeting::check_access(RPCResultSet * rs, User * user)
{
   char *partid, *ptype, *roll, *userid;
   int num_rows = rs->get_chunk_size();
   int i_roll, i_userid;
   bool result = false;
   char buff[32];

   for (int i = 0; i < num_rows; i++)
   {
      // "PARTICIPANTID", "TYPE", "ROLL", "USERID", "USERNAME", "NAME", "DESCRIPTION", "SELPHONE", "PHONE", "SELEMAIL", "EMAIL",
      // "SCREENNAME", "AIMNAME", "NOTIFYTYPE"
      if (!rs->get_values(i, "(ssss*)", &partid, &ptype, &roll, &userid))
      {
         log_error(ZONE, "Error parsing result from fetch meeting info\n");
         return false;
      }

      i_roll = atoi(roll);
      i_userid = atoi(userid);

      if (i_userid > 0)
      {
         sprintf(buff, "user:%d", i_userid);

         if (strcmp(buff, user->get_id()) == 0)
         {
            return (i_roll == RollModerator || i_roll == RollHost);
         }
      }
   }

   return result;
}

void Meeting::build_summary(IString &mtgparts)
{
   mtgparts = _("\nAttended:\n------------\n");
   IString mtgparts_not_attend = _("\n\nDid not attend:\n------------\n");
   IListIterator sm_iter(submeetings);
   SubMeeting * sm = (SubMeeting * )sm_iter.get_first();
   bool has_no_attended = false;
   while (sm != NULL)
   {
      IListIterator p_iter;
      sm->get_participants(p_iter);

      Participant * part = (Participant*)p_iter.get_first();
      while (part != NULL)
      {
         if (part->voice_joined() || part->data_joined())
         {
            // Name
            mtgparts.append(part->get_name());
            mtgparts.append(" (");
            if (part->voice_joined())
            {
               mtgparts.append(_("voice"));
               if (part->data_joined())
               {
                  mtgparts.append(", ");
                  mtgparts.append(_("data"));
               }
            }
            else
            {
               mtgparts.append(_("data"));
            }
            mtgparts.append(")");
            mtgparts.append("\n");
            // Phone
            mtgparts.append(part->get_phone());
            mtgparts.append("\n");
            // Email
            mtgparts.append(part->get_email());
            mtgparts.append(_("\n------------\n"));
         }
         else
         {
            has_no_attended = true;
            // Name
            mtgparts_not_attend.append(part->get_name());
            mtgparts_not_attend.append("\n");
            // Phone
            mtgparts_not_attend.append(part->get_phone());
            mtgparts_not_attend.append("\n");
            // Email
            mtgparts_not_attend.append(part->get_email());
            mtgparts_not_attend.append("\n------------\n");
         }
         part = (Participant*)p_iter.get_next();
      }
      sm = (SubMeeting*)sm_iter.get_next();
   }
   if (has_no_attended)
   {
      mtgparts.append(mtgparts_not_attend);
   }
}

// End meeting
int Meeting::end(User * user)
{
   IUseMutex lock(meeting_lock);

   if (!is_moderator(user))
      return NotAuthorized;

   if (state == StateLoading || state == StateNotify)
      return Retry;

   if (state == StateEnding || state == StateDead)
      return Ok;

   int status = end_internal(user, true);

   // Broadcast event to clients
   dispatcher.send(MeetingEnded, SendEventOnly, ToAllParticipants, this, NULL, NULL, NULL);

   return status;
}

// Caller should either dispatch meeting end event or call Meeting::release().
int Meeting::end_internal(User * user, bool notify_services)
{
   IString id;
   IString id2;

   // Remove active participants and connections from meeting
   IListIterator iter;
   get_submeetings(iter);

   SubMeeting *sm = (SubMeeting*)iter.get_first();
   while (sm)
   {
      sm->end(false);
      sm = (SubMeeting*)iter.get_next();
   }

   // Remove voice conference.
   // This will disconnect all callers.
   if (notify_services)
   {
      voice_close();
   }

   // ----------------------------------------   record data sharing  -------------------------------------
   if (mtgarchiver)
   {
      IString mtgparts;
      build_summary(mtgparts);

      if (rpc_make_call("stoprecording", mtgarchiver->get_name(),
                        "mtgarchiver.stop_recording", "(ssii)", get_id(), mtgparts.get_string(), get_type()==TypeScheduled, (get_options()&IncludeDocsInRecording)!=0))
      {
         log_error(ZONE, "Couldn't stop recording for ending meeting %s", get_id());
      }
   }
   // ----------------------------------------   record data sharing  -------------------------------------

   if (recording_event != -1)
   {
      log_event_end(recording_event);
      recording_event = -1;
   }

   state = StateEnding;

   // Notify schedule that this meeting is completed
   make_id_string(id2, "statuscompleted:", get_id());
   log_debug(ZONE, "Make status completed call");
   int meetingstatus = StatusCompleted;
   time_t currenttime = time(NULL);

   if (rpc_make_call(id2, "addressbk", "schedule.insert_meeting_status", "(siii)", get_id(), meetingstatus, actual_start_time, (int)currenttime))
   {
      log_error(ZONE, "Error notifying schedule service that meeting is completed: %s\n", get_id());
   }

   share_active = false;
   share_document = false;
   share_connected = false;

   // Log
   if (appshare_event != -1)
   {
      log_event_end(appshare_event);
      appshare_event = -1;
   }
   if (meeting_event != -1)
   {
      log_event_end(meeting_event);
      meeting_event = -1;
   }
   if (docshare_event != -1)
   {
      log_event_end(docshare_event);
      docshare_event = -1;
   }

   log_debug(ZONE, "Meeting %s ended", get_id());

   return Ok;
}

// Reload meeting
int Meeting::reload(User * user)
{
   IString id;
   IUseMutex lock(meeting_lock);

   if (!is_moderator(user))
      return NotAuthorized;

   if (state == StateLoading || state == StateNotify)
      return Retry;

   if (state == StateEnding || state == StateDead)
      return NotActive;

   // Start loading info from schedule db
   make_id_string(id, "reload:", get_id());
   if (rpc_make_call(id, "addressbk", "schedule.find_meeting_details", "(ss)", user->get_id(), get_id()))
   {
      log_error(ZONE, "Error fetching meeting info for %s\n", get_id());
      return Failed;
   }

   return Pending;
}

void Meeting::on_reload_result(RPCResultSet * rs)
{
   int num_rows = 0;
   IString id;
   char *meetingid, *dbhostid, *type, *privacy, *priority, *title, *descrip, *size, *duration, *start_time, *recurrence, *recur_end, *options, *password;
   char *end_time, *invite_expiration, *invite_valid_before, *invite_message, *display_attendees, *invitee_chat_options, *communityid;
   char *partid, *ptype, *roll, *userid, *username, *name, *descrip2, *selphone, *phone, *selemail, *email, *screen, *aim, *notify;
   char *community_phone, *community_url, *enabled_features;
   bool res;

   IUseMutex lock(meeting_lock);

   if (state != StateActive && state != StateHolding)
   {
      log_error(ZONE, "Meeting info reloaded, but was not in correct state");
      return;
   }

   num_rows = rs->get_chunk_size();

   // Extra row in result set is meeting descriptor
   // "MEETINGID", "HOSTID", "TYPE", "PRIVACY", "PRIORITY", "TITLE", "DESCRIPTION" ,"SIZE", "DURATION", "TIME", "RECURRENCE", "RECUR_END", "OPTIONS", "PASSWORD"
   // "END_TIME", "INVITE_EXPIRATION", "INVITE_MESSAGE", "DISPLAY_ATTENDEES", "INVITEE_CHAT_OPTIONS", "COMMUNITYID",
   // "BRIDGEPHONE", "SYSTEMURL", "ENABLED_FEATURES"
   res = rs->get_values(num_rows, "(ssssssssssssssssssssssss*)", &meetingid, &dbhostid, &type, &privacy, &priority, &title, &descrip, &size,
                        &duration, &start_time, &recurrence, &recur_end, &options, &password,
                        &end_time, &invite_expiration, &invite_valid_before, &invite_message, &display_attendees,
                        &invitee_chat_options, &communityid, &community_phone, &community_url, &enabled_features);
   if (!res)
   {
      log_error(ZONE, "Error parsing result from fetch meeting info\n");
      return;
   }

   spSubMeeting main_sm = find_mainmeeting();

   // Load participants from schedule.
   for (int i = 0; i < num_rows; i++)
   {
      // "PARTICIPANTID", "TYPE", "ROLL", "USERID", "USERNAME", "NAME", "DESCRIPTION", "SELPHONE", "PHONE", "SELEMAIL", "EMAIL",
      // "SCREENNAME", "AIMNAME", "NOTIFYTYPE"
      if (!rs->get_values(i, "(ssssssssssssss)",
                          &partid, &ptype, &roll, &userid, &username, &name, &descrip2, &selphone, &phone, &selemail, &email, &screen, &aim, &notify))
      {
         log_error(ZONE, "Error parsing result from fetch meeting info\n");
         continue;
      }

      // Figure out best user id
      char buff[32];
      bool use_userid = atoi(userid) > 0;
      sprintf(buff, "%s:%s", use_userid ? "user" : "part", use_userid ? userid : partid);

      // Add participant if not already present
      spParticipant p = find_participant_by_userid(buff);
      if (p == NULL)
      {
          spUser target_user = master.create_user(buff, username, name);
          if (target_user == NULL)
          {
             log_error(ZONE, "Unable to add user %s to meeting %s\n", buff, meetingid);
             continue;
          }

          char pid[32];
          sprintf(pid, "part:%s", partid);

          // Add participant with event dispatch but without notifications
          spParticipant dummy;
          add_participant_internal(starting_user, main_sm, target_user, pid, atoi(ptype), atoi(roll), name, atoi(selphone), phone,
                                   atoi(selemail), email, screen, descrip2, 0, 0, true, dummy);

          log_debug(ZONE, "Added participant %s to meeting %s", userid, meetingid);
      }
   }

}

int Meeting::set_lecture_mute(spSubMeeting main_sm, int state)
{
   IListIterator iter;
   main_sm->get_participants(iter);

   int new_mute = state ? MuteOn : MuteOff;
   if (options & HoldMode)
	   new_mute = MuteHold;

   Participant * p = (Participant *)iter.get_first();
   while (p != NULL)
   {
      if (!p->is_moderator() && p->get_mute() != new_mute)
      {
		 if (set_participant_mute(p, new_mute))
			 continue;

         dispatcher.send(ParticipantState, SendParticipantState, ToAllParticipants, this, main_sm, p, NULL);
      }

      p = (Participant *)iter.get_next();
   }

   return Ok;
}

int Meeting::set_hold_mute(spSubMeeting main_sm, int state)
{
   IListIterator iter;
   main_sm->get_participants(iter);

   int new_mute = state ? MuteHold : MuteOff;
   if (new_mute == MuteOff && options & LectureMode)
	   new_mute = MuteOn;

   log_debug(ZONE, "Meeting %s %s hold mode", get_id(), state ? "entering" : "exiting");

   Participant * p = (Participant *)iter.get_first();
   while (p != NULL)
   {
      if (!p->is_moderator() && p->get_mute() != new_mute)
	  {
		 if (set_participant_mute(p, new_mute))
			 continue;

         dispatcher.send(ParticipantState, SendParticipantState, ToAllParticipants, this, main_sm, p, NULL);
	  }

      p = (Participant *)iter.get_next();
   }

   return Ok;
}

int Meeting::set_participant_mute(Participant * p, int new_mute)
{
   p->set_mute(new_mute);

   log_debug(ZONE, "turned mute %s for participant %s", (new_mute == MuteHold ? "hold" : new_mute == MuteOn ? "on" : "off"), p->get_id());

   spPort port = p->get_voice_port();
   if (port != NULL && port->is_connected())
   {
	   IString id;
	   make_id_string(id, "call:", p->get_id());
	   if (rpc_make_call(id,
					  get_voice_service(), "voice.set_volume", "(sssiii)",
					  port->get_id(), get_id(), p->get_submeeting()->get_id(),
					  -1, -1, p->get_mute()))
	  {
	     log_error(ZONE, "Error notifying voice service about volume change\n");

	     return Failed;
	  }
   }

   return Ok;
}

// Send custom request to voice bridge
int Meeting::send_bridge_request(User * actor, const char * partid, int code, const char *msg)
{
   int status = Ok;

   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   // See if user is in this meeting.
   spParticipant p = find_participant(partid);
   if (p == NULL)
      return NotMember;

   // Send request to bridge
   spPort port = p->get_voice_port();
   if (port != NULL && port->is_connected())
   {
      log_debug(ZONE, "Sending bridge request (%d, %s)", code, msg);

      p->set_bridge_request(code);

      IString call_id;
      make_id_string(call_id, "call:", p->get_id());
      if (rpc_make_call(call_id, get_voice_service(), "voice.bridge_request", "(sssis)",
                       port->get_id(), get_id(), p->get_submeeting()->get_id(), code, msg))
      {
         log_error(ZONE, "Error making call to voice.bridge_request");
         return Failed;
      }

      if (state == StateActive)
        dispatcher.send(ParticipantState, SendParticipantState, ToAllParticipants, this, p->get_submeeting(), p, p);
   }

   return status;
}

int Meeting::bridge_request_progress(User * user, int code, const char *msg)
{
   IUseMutex lock(meeting_lock);

   IRefObjectPtr<Participant> p = find_participant_by_userid(user->get_id());
   if (p == NULL)
      return NotMember;

   p->set_bridge_request(code);

   if (state == StateActive)
      dispatcher.send(ParticipantState, SendParticipantState, ToAllParticipants, this, p->get_submeeting(), p, p);

   return Ok;
}

// Send initial meeting notifications.
void Meeting::send_start_notifications()
{
   log_debug(ZONE, "Sending initial meeting notifications for %s", get_id());

   // Meeting is open for business.
   dispatcher.send(MeetingStarted, SendEventOnly, ToAllParticipants, this, NULL, NULL, NULL);

   // Perform requested notifications for each participant.
   IListIterator sm_iter(submeetings);
   SubMeeting * sm = (SubMeeting * )sm_iter.get_first();
   while (sm != NULL)
   {
      IListIterator p_iter;
      sm->get_participants(p_iter);

      Participant * part = (Participant*)p_iter.get_first();
      while (part != NULL)
      {
         if (part->get_participant_type() == ParticipantNormal &&
             part->get_notify_type())
         {
            notify_participant_internal(starting_user, part, part->get_notify_type(), "");
         }
         part = (Participant*)p_iter.get_next();
      }
      sm = (SubMeeting*)sm_iter.get_next();
   }
}

// Add pending users to new meeting.
int Meeting::add_participants(User * actor, IList * participant_list, SubMeeting * _direct_sm)
{
   spSubMeeting direct_sm = _direct_sm;
   int status = Ok;

   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   // Create submeeting for 1-1 call if needed.
   if (is_direct_call(participant_list) && direct_sm == NULL)
   {
      char buff[32];
      sprintf(buff, "submeeting:%d", ++submeeting_id_seq);

      char title[32];
      sprintf(title, "Submeeting %d", ++submeeting_index);

      direct_sm = new SubMeeting(buff, this, 2, false, title);
      submeetings.add(direct_sm);

      master.register_submeeting(direct_sm);

      direct_sm->dereference();

      // Will get dispatched when result comes back.
      DispatchCall * dispatch = new AddParticipantsDispatch(actor, this, participant_list, direct_sm);
      return direct_sm->start(dispatch);
   }
   else
   {
      status = add_participants_internal(actor, participant_list, direct_sm);
   }

   return status;
}

int Meeting::voice_register(const char *rpc_id, SubMeeting *call_sm)
{
   spSubMeeting main_sm = find_mainmeeting();
   if (main_sm != NULL) {
       IString id;
       make_id_string(id, rpc_id, get_id());
       if (rpc_make_call(id, get_voice_service(),
                      "voice.register_meeting", "(siissi)",
                      get_id(), get_expected_participants(), get_priority(),
                      main_sm->get_id(),
                      call_sm != NULL ? call_sm->get_id() : "",
                      actual_start_time))
       {
           log_error(ZONE, "Error notifying voice service about meeting %s\n", get_id());
           return Failed;
       }
   }
   return Ok;
}

int Meeting::voice_close()
{
   IString id;
   make_id_string(id, "remove:", get_id());
   if (rpc_make_call(id, get_voice_service(), "voice.close_meeting", "(s)", get_id()))
   {
      log_error(ZONE, "Error notifying voice service about closed meeting %s\n", get_id());
      return Failed;
   }

   return Ok;
}

// Notify voice bridge about new participant PIN
int Meeting::voice_notify_participant(User * actor, Participant * participant)
{
    if (hold_notifications) return Ok;

    IString id;
    IString actor_id = (actor != NULL ? actor->get_id() : "0");
    make_id_string(id, "notify:", actor_id);
    id = id + IString(".") + participant->get_id();

    if (rpc_make_call(id, get_voice_service(),
                      "voice.notify_participant", "(ssssiii)",
                      participant->get_user()->get_id(),
                      participant->get_user()->get_username(),
                      get_id(),
                      participant->get_id(),
                      (participant->is_moderator() ? 1 : 0),
                      (participant->get_notify_type() & NotifyPhone ? 1 : 0),
                      participant->get_call_options()))
    {
        log_error(ZONE, "Error calling voice bridge to notify of non-called participant");
    }

    return Ok;
}

int Meeting::add_participants_internal(User * actor, IList * participant_list, SubMeeting * _direct_sm)
{
   spSubMeeting main_sm = find_mainmeeting();
   spSubMeeting direct_sm = _direct_sm;

   IUseMutex lock(meeting_lock);

   if (state != StateActive && state != StateHolding)
      return NotActive;

   if (participant_list != NULL)
   {
      IListIterator iter(*participant_list);

      Participant * p = (Participant *)iter.get_first();
      while (p)
      {
         SubMeeting * target_sm = main_sm;
         if (direct_sm != NULL && (p->get_notify_type() & NotifyPhone) && (p->get_call_options() & CallSupervised))
         {
            target_sm = direct_sm;
         }

         if (*p->get_id() == '\0')
         {
            // No id was assigned, so must get one through normal process.
            // (Would be more efficient to send batch in one add, but add one by one for now.)
            add_participant(actor, target_sm->get_id(), p->get_user(), ParticipantNormal, p->get_roll(),
                            p->get_name(), p->get_sel_phone(), p->get_phone(), p->get_sel_email(), p->get_email(),
                            p->get_screen(), p->get_description(), p->get_notify_type(), "", p->get_call_options(), 0);
         }
         else
         {
            // Have id, add participant directly.
            spParticipant dummy;
            add_participant_internal(actor, target_sm, p->get_user(), p->get_id(), ParticipantNormal, p->get_roll(),
                                     p->get_name(), p->get_sel_phone(), p->get_phone(), p->get_sel_email(), p->get_email(),
                                     p->get_screen(), p->get_description(), p->get_notify_type(), p->get_call_options(), true, dummy);
         }
         p = (Participant *)iter.get_next();
      }

      delete participant_list;
   }

   return Ok;
}

// See if meeting has a direct call component (i.e. someone requesting supervised call)
bool Meeting::is_direct_call(IList * participants)
{
   if (participants != NULL)
   {
      IListIterator iter(*participants);

      Participant * p = (Participant *)iter.get_first();
      while (p)
      {
         if ((p->get_notify_type() & NotifyPhone) && (p->get_call_options() & CallSupervised))
            return true;
         p = (Participant *)iter.get_next();
      }
   }

   return false;
}

// Add participant to meeting.
// Participant may already exist, in which case this is treated as an update.
int Meeting::add_participant(User * actor, const char * sub_id, User * target, int ptype,
                             int roll, const char * name, int sel_phone, const char * phone,
                             int sel_email, const char * email, const char * screen, const char * description,
                             int notify_type, const char * message,
                             int call_type, int expires)
{
   spSubMeeting sm;
   spParticipant participant;

   log_debug(ZONE, "Adding user %s to meeting %s", target->get_id(), get_id());

   IUseMutex lock(meeting_lock);

   // Figure out submeeting to join.
   if (sub_id == NULL || strlen(sub_id) == 0)
      sm = find_mainmeeting();
   else
      sm = find_submeeting(sub_id);
   if (sm == NULL)
      return InvalidSubmeetingID;

   if (state == StateLoading || state == StateNotify)
      return Retry;
   else if (state != StateActive && state != StateHolding)
      return NotActive;

   // Check access--only moderator can add new participant
   if (actor != NULL && !is_moderator(actor))
      return NotAuthorized;

   // *** Check permission and conf. size (N.B. participant may be in meeting)

   // If participant isn't already registered, we must add participant to db
   // before continuing here.  DB will assign unique participant id for us.
   participant = find_participant_by_userid(target->get_id());
   if (participant == NULL)
   {
      AddParticipantParameters * addp = new AddParticipantParameters(actor, this, target, sm,
                                                                     roll, name, sel_phone, phone, sel_email, email, screen, description, message,
                                                                     notify_type, call_type);

      char id[32];
      const char * dispatch_id = dispatch_queue.add_dispatch(addp);
      sprintf(id, "dispatch:%s", dispatch_id);

      // Convert user id to form acceptable for db
      Address addr;
      if (parse_address(target->get_id(), &addr) == Ok)
      {
         if (addr.type != AddressUser || !isdigits(addr.id))
            strcpy(addr.id, "-1");
      }

      // Determine correct value for invite expiration for this participant.
      // (Sorry, the meeting option flag is poorly named...we check InviteNeverExires
      // to determine if it should expire immediately)
      int actual_expiration = (options & InviteNeverExpires) ? InviteExpiresImmediately : invite_expiration;

      // SESSIONID, MEETINGID, USERID, USERNAME, TYPE, ROLE, NAME, SEL_PHONE, PHONE, SEL_EMAIL, EMAIL,
      // SCREENNAME, DESCRIPTION, NOTIFYTYPE, MESSAGE, EXPIRES, STARTTIME
      if (rpc_make_call(id, "addressbk", "schedule.add_participant", "(ssssiisisisssisii)", "sys:controller",
                        get_id(), addr.id, target->get_username(), ParticipantInvitee, roll, name, sel_phone, phone,
                        sel_email, email, screen, description, notify_type, message, actual_expiration, actual_start_time))
      {
         log_error(ZONE, "Error registering participant for meeting %s\n", get_id());
         return Failed;
      }

      return Pending;
   }

   // Otherwise, we can complete update immediately
   int status = add_participant_internal(actor, sm, target, NULL, ptype, roll, name, sel_phone, phone,
                                         sel_email, email, screen, description, notify_type, call_type, true,
                                         participant);

   return status;
}

// Add participant without checking meeting state.
int Meeting::add_participant_internal(User * actor, SubMeeting * sm, User * target, const char * part_id,
                                      int ptype, int roll, const char * name, int sel_phone, const char * phone,
                                      int sel_email, const char * email, const char * screen, const char * description,
                                      int notify_type, int call_type, bool dispatch_event, spParticipant & participant)
{
   int status = Ok;

   IUseMutex lock(meeting_lock);

   if (state != StateActive && state != StateHolding && state != StateLoading)
      return NotActive;

   if (participant == NULL)
      participant = find_participant(part_id);

   // See if user is already in meeting.
   if (participant == NULL)
      participant = find_participant_by_userid(target->get_id());

   // *** Verify participant ID requested is unique??

   if (participant == NULL)
   {
      // Make sure host is host.
      if (target == host)
         roll = RollHost;

      // Insert into target submeeting
      participant = new Participant(part_id, sm, target, ptype, roll, name, sel_phone, phone,
                                    sel_email, email, screen, description, notify_type, call_type);
      sm->add_participant(participant);

      // and insert into master list for meeting.
      add_index(target, participant);

      master.register_participant(participant);

      participant->dereference();

      if (dispatch_event)
      {
         // Broadcast event to clients
         dispatcher.send(ParticipantAdded, SendParticipantState, ToAllParticipants, this, participant->get_submeeting(), participant, NULL);
         voice_notify_participant(actor, participant);
      }
   }
   else
   {
      IString curr_name;

      // User may have changed name while joining from client, so don't change.
      if (participant->get_data_port() != NULL)
      {
         curr_name = participant->get_name();
         name = curr_name;
      }

      // Validate that role is sensible for this participant.
      if (participant->is_user_attached(host))
         roll = RollHost;
      else if (roll == RollHost)
         roll = RollModerator;

      participant->update_info(roll, name, sel_phone, phone, sel_email, email, screen, description, notify_type, call_type);

      if (strcmp(participant->get_submeeting()->get_id(), sm->get_id()) != 0)
      {
         // Participant wants to be moved to another submeeting.
         // (Broadcasts event internally)
         status = move_participant_internal(participant, sm);
      }
      else if (dispatch_event)
      {
         // Broadcast event to clients
         dispatcher.send(ParticipantAdded, SendParticipantState, ToAllParticipants, this, participant->get_submeeting(), participant, NULL);
         voice_notify_participant(actor, participant);
      }

      log_debug(ZONE, "Updated info for participant %s: role = %d, name = %s", participant->get_id(), roll, name);
   }

   if (dispatch_event)
   {
      // Do notifications/invites now if we already sent initial batch
      if (state == StateActive)
      {
         if (notify_type)
         {
            notify_participant_internal(actor, participant, notify_type & ~NotifyPhone, NULL);
         }
         if (notify_type & NotifyPhone)
         {
            call_participant(actor, participant->get_id(), sel_phone, phone, call_type);
         }
      }

      // Also allow outbound call if bypass option is selected
      if (state == StateHolding &&
          (notify_type & NotifyPhone) && (call_type & CallBypassHold))
      {
         call_participant(actor, participant->get_id(), sel_phone, phone, call_type);
      }
   }

   log_debug(ZONE, "Added participant %s", participant->get_id());

   return Ok;
}

int Meeting::remove_participant(User * actor, const char * participant_id)
{
   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
      return Retry;
   else if (state != StateActive && state != StateHolding)
      return NotActive;

   // Check access--only moderator can add/remove participants
   if (actor != NULL && !is_moderator(actor))
      return NotAuthorized;

   // See if user is in this meeting.
   spParticipant p = find_participant(participant_id);
   if (p == NULL)
      return NotMember;

   return remove_participant_internal(p);
}

int Meeting::remove_participant_internal(Participant * p)
{
   int status = Ok;

   // Disconnect from meeting if connected.
   if (p->is_connected())
   {
      status = leave_user(p->get_user(), NULL, false);
   }

   // Remove participant
   spSubMeeting sm = p->get_submeeting();
   if (sm != NULL)
      sm->remove_participant(p);

   remove_index(p);

   // Remove the partipant from the database.
   //
   // Convert participant id to pin suitable for db.
   //
   // TJL - Only remove "instant" participants from the db,
   //       leave scheduled participants intact.
   Address addr;
   if (p->get_participant_type() == ParticipantInvitee && parse_address(p->get_id(), &addr) == Ok)
   {
      if (rpc_make_call("removep", "addressbk", "schedule.remove_participant", "(sss)", "sys:controller", get_id(), addr.id))
      {
         log_error(ZONE, "Error removing participant %s from schedule db\n", p->get_id());
      }
   }

   dispatcher.send(ParticipantRemoved, SendEventOnly, ToAllParticipants, this, sm, p, NULL);

   return status;
}

int Meeting::call_participant(User * actor, const char * participant_id, int sel_call_phone, const char * call_phone, int options)
{
   log_debug(ZONE, "Calling participant %s in meeting %s", participant_id, get_id());

   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
      return Retry;
   else if (state != StateActive && state != StateHolding)
      return NotActive;

   // See if user is in this meeting.
   spParticipant p = find_participant(participant_id);
   if (p == NULL)
      return NotMember;

   // Check access--only moderator can call participant
   if (actor != NULL && !is_moderator(actor))
      if (!p->is_user_attached(actor))
         return NotAuthorized;

   // See if already on phone in this meeting.
   spPort port = p->get_voice_port();
   if (port != NULL && port->is_connected())
   {
      log_debug(ZONE, "User %s is already on phone", p->get_user()->get_id());
      return AlreadyConnected;
   }

   // If this is a system defined user, and we are using a phone number from the user's contact record,
   // we've got to load the user's contact record first.
   spUser user = p->get_user();
   if ((options & SkipUserLookup) == 0 && get_address_type(user->get_id()) == AddressUser && sel_call_phone != 0)
   {
      spMeeting meeting = this;
      spUser sp_actor = actor;
      log_debug(ZONE, "Queueing call to %s until user info loaded", user->get_id());
      DispatchCall * disp = new CallParticipantParameters(meeting, sp_actor, p, sel_call_phone, call_phone, options);
      user->load(disp);
      return Pending;
   }

   return call_participant_internal(actor, p, sel_call_phone, call_phone, options);
}

int Meeting::call_participant_internal(User * actor, Participant * p, int sel_call_phone, const char * call_phone, int _options)
{
   IString id;
   char resolve_phone[MAX_PHONE_LEN];
   char resolve_extension[MAX_PHONE_LEN];
   bool direct = true;

   if (hold_notifications) return Ok;

   IUseMutex lock(meeting_lock);

   if (state != StateActive && state != StateHolding)
      return NotActive;

   // See if already on phone in this meeting.
   spPort port = p->get_voice_port();
   if (port != NULL && port->is_connected())
   {
      log_debug(ZONE, "User %s is already on phone", p->get_user()->get_id());
      return AlreadyConnected;
   }

   if (options & ScreenCallers)
   {
      _options |= CallScreen;
   }
   p->set_call_options(_options);

   if (sel_call_phone == SelPhonePC ||
       (sel_call_phone == SelPhoneDefault && p->get_user()->get_default_phone() == SelPhonePC))
   {
      spUser target_user = p->get_user();
      spPort port = target_user->get_client();

      // See if user is connected
      if (port != NULL && port->is_connected())
      {
         // Place PC phone call to participant.
         Participant * actor_part = actor != NULL ? find_participant_by_userid(actor->get_id()) : NULL;
         int invite_type = (_options & (CallMaster|CallSupervised)) ? InvitePCPhoneDirect: InvitePCPhone;
         dispatcher.send(MeetingInvite, SendInvite, ToOneParticipant, this, p->get_submeeting(), actor_part, p, "", invite_type);

         _options |= CallPCPhone;
         p->set_call_options(_options);

         // Simulate call progress
         call_progress(p->get_user(), StatusRinging);

         log_debug(ZONE, "Sent call invite to %s", p->get_id());
      }
      else
      {
         // Not connected, let client know.
         port = actor->get_client();
         actor->send_error(port, "im", NotConnected, "Invite not sent", get_id(), p->get_user()->get_id());
      }
   }
   else
   {
       int status = p->get_user()->resolve_phone(sel_call_phone, call_phone, resolve_phone, resolve_extension, direct);
       if (status)
          return status;

       p->set_phone(resolve_phone);

       // Id string is "call:actorid.participantid"
       IString actor_id = actor != NULL ? actor->get_id() : "0";
       make_id_string(id, "call:", actor_id);
       id = id + IString(".") + p->get_id();

       //  USERID, USERNAME, MEETINGID, SUBMEETINGID, PHONE, EXTENSION, OPTIONS, SUPERVISINGCALLID, PARTICIPANTID
       if (rpc_make_call(id, get_voice_service(), "voice.make_call", "(ssssssiss)", p->get_user()->get_id(), p->get_user()->get_username(),
                     get_id(), p->get_submeeting()->get_id(), resolve_phone, resolve_extension, _options, "", p->get_id()))
       {
          log_error(ZONE, "Error making call to %s\n", resolve_phone);
          return Failed;
       }

       log_debug(ZONE, "Started call to %s", resolve_phone);
   }

   return Pending;
}

void Meeting::on_call_failed(spUser & user, spParticipant & participant, int fault_code)
{
   // Report error to client.
   if (user)
      user->send_error("call", fault_code, "Error starting call", get_id(), participant->get_id());
   log_debug(ZONE, "Failed to start call for %s", participant->get_id());
}

int Meeting::notify_participant(User * actor, const char * participant_id, int notify_type, const char * message)
{
   int status = Ok;

   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
      return Retry;
   else if (state != StateActive && state != StateHolding)
      return NotActive;

   // Check access--only moderator can add new participant
   if (actor != NULL && !is_moderator(actor))
      return NotAuthorized;

   // See if user is in this meeting.
   spParticipant p = find_participant(participant_id);
   if (p == NULL)
      return NotMember;

   status = notify_participant_internal(actor, p, notify_type, message);

   return status;
}

int Meeting::notify_participant_internal(User * actor,
                                         Participant * p,
                                         int notify_type,
                                         const char * message)
{
   if (notify_type == 0 || hold_notifications) return Ok;

   // Send desired notification.
   if ((notify_type & NotifyIIC || notify_type & NotifyIMAny) && p->get_data_port() == NULL)
   {
      spUser target_user = p->get_user();
      spPort port = target_user->get_client();

      // See if user is connected
      if (port != NULL && port->is_connected())
      {
         const char * addr_str = port->get_id();
         if (addr_str != NULL && *addr_str)
         {
            Participant * host_part = find_participant_by_userid(host->get_id());
            dispatcher.send(MeetingInvite, SendInvite, ToOneParticipant, this, p->get_submeeting(), host_part, p, message);
            log_debug(ZONE, "Sent invite to %s", p->get_id());
            p->set_im_sent(true);
         }
      }
      else /*if (notify_type & NotifyIMAny)*/
      {
         // Not connected, let client know.
         port = actor->get_client();
         actor->send_error(port, "im", NotConnected, "Invite not sent", get_id(), p->get_user()->get_id());
      }
   }

   /*
     if (notify_type & NotifyAIM || notify_type & NotifyMessenger)
     {
     im_participant(actor, p, notify_type);
     }
   */

   if (notify_type & NotifyPhone)
   {
      call_participant(actor, p->get_id(), p->get_sel_phone(), p->get_phone(), p->get_call_options());
   }

   if (notify_type & NotifyEmail)
   {
      email_participant(actor, p);
   }

   return Ok;
}

void truncate_string(IString &aString, int aMaxSize)
{
   IString strEllipses = "...";

   if (aString.length() > aMaxSize)
   {
      aString.truncate(aMaxSize - strEllipses.length());
      aString.append(strEllipses);
   }
}

void Meeting::get_invite_details(IString &msg1,
                                 IString &msg2,
                                 IString &msg3,
                                 const char *aMeetingId,
                                 const char *aMeetingTitle,
                                 const char *aHostName,
                                 const char *aParticipantId,
                                 const char *aParticipantName,
                                 const char *aMeetingDescription,
                                 const char *aInviteMessage,
                                 const char *aMeetingPassord,
                                 const char *aParticipantPhone,
                                 const char *aBridgePhone,
                                 const char *aSystemUrl,
                                 int aMeetingType,
                                 int aScheduledTime,
                                 bool aUseHTML,
                                 int aMaxMessageSize
   )
{
   // assemble message and URL
   IString aInviteMsg;
   IString aInviteDescription;
   IString aInviteBody;

   IString AND = "&";
   IString UNKNOWN = "unknown";
   IString CR = aUseHTML ? "<BR>" : "\n";
   IString TAB = "\t";
   IString QUESTION = "?";

   IString paramVal;
   IString URLargs;

   // mtg id
   IString meetingIdStr = "m_id=";                 // this->meeting_id
   IString meetingId = aMeetingId;
   URLargs = meetingIdStr + meetingId;

   // participant id
   IString participantIdStr = "p_id=";             // p->user->name
   IString participantId;
   if (strlen(aParticipantId) > 0)
      participantId = aParticipantId;
   else
      participantId = UNKNOWN;
   URLargs.append(AND + participantIdStr + participantId);

   // participant name
   IString participantName = aParticipantName;

   // mtg title
   paramVal = (aMeetingTitle != NULL && strlen(aMeetingTitle) > 0) ? IString(aMeetingTitle) : UNKNOWN;
   IString meetingTitle = paramVal;

   // meeting time
   char timeStr[256];
   date_time_decode(aScheduledTime, timeStr, _("%b %d, %Y %#I:%M %p %Z"));

   // host name
   IString mtgHost = (aHostName != NULL && strlen(aHostName) > 0) ? IString(aHostName) : UNKNOWN;

   // description
   IString mtgDescription = aMeetingDescription;

   // password
   IString mtgPassword;
   if (aMeetingType & RequirePassword && strlen(aMeetingPassord) > 0)
      mtgPassword = aMeetingPassord;

   // meeting type (V, V+D, D)
   IString mtgType;
   if (aMeetingType & InvitationDataOnly)
      mtgType = _("This is a data only meeting.");
   else if (aMeetingType & InvitationVoiceOnly)
      mtgType = _("This is a voice only conference call.");
   else
      mtgType = _("This is a voice and data meeting.");

   bool isVoiceCall = !(aMeetingType & InvitationDataOnly);

   // phone numbers
   // see if participant has phone number set - if so display with text
   // so they know they'll be able to change
   IString participantPhone;
   if (isVoiceCall) // anything but data, include participant phone
      participantPhone = strlen(aParticipantPhone) > 0 ? aParticipantPhone : _("None specified");

   // see if dial in number is provided - if so display with text
   IString systemDialinNumber;
   if (isVoiceCall) // anything but data, include dial in
      systemDialinNumber = aBridgePhone;  // hard coded for now

   // encode args
   CBase64 encoder;
   const char* encodedArgs = encoder.Encode((LPCTSTR)URLargs, URLargs.length());

   // assemble msg
   IString mtgURL  = aSystemUrl;
   IString meeting = "mid=";

   truncate_string(meetingTitle, 32);
   truncate_string(mtgHost, 32);
   truncate_string(participantName, 32);

   // body
   aInviteBody.append(_("Title: ") + TAB + TAB + meetingTitle + CR);
   aInviteBody.append(_("Host: ")  + TAB + TAB + mtgHost + CR);
   aInviteBody.append(_("Time: ")  + TAB + TAB + timeStr + CR);
   aInviteBody.append(_("Participant: ") + TAB + participantName + CR);
   aInviteBody.append(_("Meeting ID: ")  + TAB + meetingId + CR);
   if (mtgPassword.length() > 0)
      aInviteBody.append(_("Meeting Password: ")  + mtgPassword + CR);

   // participant call number
   if (participantPhone.length() > 0)
   {
      aInviteBody.append(CR);
      aInviteBody.append(_("Phone number where system will call you: ") + TAB + participantPhone + CR);
   }

   // dialin number
   aInviteBody.append(CR);
   if (systemDialinNumber.length() > 0)
      aInviteBody.append(_("Dial-in number (provided if system does not call you): ") + TAB + systemDialinNumber + CR);
   aInviteBody.append(_("Participant PIN: ")  + TAB + participantId + CR);

   aInviteBody.append(CR);
   aInviteBody.append(_("To join the meeting click here: ") + CR);
   aInviteBody.append(mtgURL + QUESTION);
   aInviteBody.append(meeting + encodedArgs);

   // invite
   aInviteMsg.append(aInviteMessage);

   // description
   if (mtgDescription.length() > 0)
      aInviteDescription = _("Description: ") + CR + mtgDescription;

   // Should really be returning a vector
   msg1.clear();
   msg2.clear();
   msg3.clear();

   // Now try to send invite in as few messages as possible (max of 3)...
   //
   // The number added to the lengths in the conditional below is equal to
   // the number of CRs we add to the for string inserted into the message array...
   if (aInviteMsg.length() + aInviteDescription.length() + aInviteBody.length() + 3 < aMaxMessageSize)
   {
      aInviteBody.append(CR + CR);

      if (aInviteMsg.length() > 0)
         aInviteMsg.append(CR + CR);

      msg1 = aInviteBody + aInviteMsg + aInviteDescription;
   }
   else if (aInviteMsg.length() + aInviteDescription.length() + 1 < aMaxMessageSize)
   {
      if (aInviteMsg.length() > 0 && !aInviteDescription.length() == 0)
      {
         aInviteMsg.append(CR + CR);
      }

      truncate_string(aInviteBody, aMaxMessageSize);
      msg1 = aInviteBody;
      msg2 = aInviteMsg + aInviteDescription;
   }
   else
   {
      truncate_string(aInviteMsg, aMaxMessageSize);
      truncate_string(aInviteBody, aMaxMessageSize);
      truncate_string(aInviteDescription, aMaxMessageSize);
      msg1 = aInviteBody;
      msg2 = aInviteMsg;
      msg3 = aInviteDescription;
   }

}

int Meeting::im_participant(User* actor, Participant* p, int notify_type)
{
   //
   // assemble parameters for IM and send msg to participant
   //
   if (p->get_screen() != NULL)
   {
      // mtg id
      IString meetingId = this->get_id();

      // participant id
      IString participantId;
      const char* pid = p->get_id();
      const char* pos = strchr(pid, ':');
      if (pos != NULL && (unsigned int)(pos + 1 - pid) < strlen(pid))
         participantId = ++pos;
      else
         participantId = "";

      IString participantName = p->get_name();

      // mtg title
      const char* title = this->get_title();

      // meeting time
      int mtime = this->get_start();

      // host name
      const char* host = this->get_host()->get_name();

      // description
      IString mtgDescription = this->get_description();

      // invite message
      IString customInviteMessage = get_invite_message();

      // password
      IString mtgPassword = this->password;

      // meeting type (V, V+D, D)
      int aMtgType = this->options;

      IString aPhone = p->get_phone();

      IString aDialin = bridge_phone;

      // assemble msg
      IString aSystemURL = system_url.length() ? system_url : IString(master.get_webstart_url());

      IString msg1;
      IString msg2;
      IString msg3;

      int max_message_size = 0;

      if (notify_type & NotifyAIM)
         max_message_size = MAX_MESSAGE_SIZE_AOL;
      else if (notify_type & NotifyMessenger)
         max_message_size = MAX_MESSAGE_SIZE_MESSENGER;

      get_invite_details(msg1, msg2, msg3,
                         meetingId,
                         title,
                         host,
                         participantId,
                         participantName,
                         mtgDescription,
                         customInviteMessage,
                         mtgPassword,
                         aPhone,
                         aDialin,
                         aSystemURL,
                         aMtgType,
                         mtime,
                         FALSE,
                         max_message_size);


      // get transport per service type from pool
      if (notify_type & NotifyAIM)
      {
         log_error(ZONE, "notify via aim-t");
         Transport* aimt = transport_pool->get_transport(AOL_SERVICE_KEY);

         if (aimt != NULL)
         {
            // send message(s)
            if (msg1.length() > 0)
               aimt->send_instant_message(p->get_screen(), msg1);
            if (msg2.length() > 0)
               aimt->send_instant_message(p->get_screen(), msg2);
            if (msg3.length() > 0)
               aimt->send_instant_message(p->get_screen(), msg3);
         }
      }

      // get transport per service type from pool
      else if (notify_type & NotifyMessenger)
      {
         log_error(ZONE, "notify via msn-t");
         Transport* msnt = transport_pool->get_transport(MSN_SERVICE_KEY);

         if (msnt != NULL)
         {
            // send message(s)
            if (msg1.length() > 0)
               msnt->send_instant_message(p->get_screen(), msg1);
            if (msg2.length() > 0)
               msnt->send_instant_message(p->get_screen(), msg2);
            if (msg3.length() > 0)
               msnt->send_instant_message(p->get_screen(), msg3);
         }
      }
   }

   return Ok;
}

int Meeting::email_participant(User* actor, Participant* p)
{
   char id[32];
   const char * mailer_id = "controller_mail";
   int status = Ok;

   //
   // assemble parameters for email and send msg to participant
   //

   const char* email = p->get_email();
   if (NULL != email && strlen(email) > 0)
   {
      // to:  comes from p->email
      IString to = email;

      // mtg id
      IString meetingId = this->get_id();

      // participant id
      IString participantId;
      const char* pid = p->get_id();
      const char* pos = strchr(pid, ':');
      if (pos != NULL && (unsigned int)(pos + 1 - pid) < strlen(pid))
         participantId = ++pos;
      else
         participantId = "";

      IString participantName = p->get_name();

      // mtg title
      const char* title = this->get_title();

      // meeting time
      int mtime = this->get_start();

      // host name
      const char* host = this->get_host()->get_name();

      // host's email address
      IString sentby = this->get_host()->find_email_address();

      // description
      IString mtgDescription = this->get_description();

      // invite message
      IString customInviteMessage = get_invite_message();

      // password
      IString mtgPassword = this->password;

      // meeting type (V, V+D, D)
      //int aMtgType = this->options;

      IString aPhone;
      if (p->get_notify_type() & NotifyPhone)
      {
         aPhone  = p->get_phone();
      }

      IString aDialin = bridge_phone;

      // url
      IString aSystemURL = system_url.length() ? system_url : IString(master.get_webstart_url());

      sprintf(id, "%s:%d", mailer_id, master.get_next_mailer_id());

      if (rpc_make_call(id, "mailer", "mailer.send_invite_email",
                        "(ssssssssssssiiiiiis)",
                        email, meetingId.get_string(), title, host, participantId.get_string(),
                        participantName.get_string(), mtgDescription.get_string(), customInviteMessage.get_string(),
                        mtgPassword.get_string(), aPhone.get_string(), aDialin.get_string(),
                        aSystemURL.get_string(), this->options, this->meeting_type,
                        this->privacy, mtime, 0, 0, sentby.get_string()))
      {
         status = Failed;
      }
   }
   return status;
}

int Meeting::disconnect_participant(User * actor, const char * participant_id, int media_type)
{
   int status = Ok;

   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
      return Retry;
   else if (state != StateActive && state != StateHolding)
      return NotActive;

   // See if user is in this meeting.
   spParticipant p = find_participant(participant_id);
   if (p == NULL)
      return NotMember;

   // Check access--user can disconnect himself; moderator can disconnect anyone
   if (actor != NULL && !is_moderator(actor))
   {
      if (!p->is_user_attached(actor))
         return NotAuthorized;
   }

   spPort port = p->get_port(media_type);
   if (port == NULL)
   {
      if (p->get_call_options() & CallPCPhone)
      {
          // A PC phone call was placed to participant, cancel now.
          Participant * actor_part = actor != NULL ? find_participant_by_userid(actor->get_id()) : NULL;
          dispatcher.send(MeetingInviteCancel, SendInvite, ToOneParticipant, this, p->get_submeeting(), actor_part, p, "");

          p->set_call_options(p->get_call_options() & ~CallPCPhone);

          call_progress(p->get_user(), 0);

          return status;
      }
      return NotConnected;
   }

   status = p->disconnect_port(port);

   // *** Send event to client?

   return status;
}

// Get submeeting list.  Meeting should be locked while list is iterated.
void Meeting::get_submeetings(IListIterator & iter)
{
   iter.set_list(submeetings);
}

// Create and start new submeeting.
int Meeting::add_submeeting(User * actor, int expected_participants, spSubMeeting & sm, DispatchCall * therest)
{
   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   // Check access--only moderator can add new participant
   if (actor != NULL && !is_moderator(actor))
      return NotAuthorized;

   char buff[32];
   sprintf(buff, "submeeting:%d", ++submeeting_id_seq);

   char title[32];
   sprintf(title, "Submeeting %d", ++submeeting_index);

   sm = new SubMeeting(buff, this, expected_participants, false, title);
   submeetings.add(sm);

   master.register_submeeting(sm);

   sm->dereference();

   return sm->start(therest);
}

// Removes submeeting, after either moving any participants to the main submeeting
// or removing the participants from the meeting
int Meeting::remove_submeeting(User * actor, const char * sub_id, bool remove_parts)
{
   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   // Check access--only moderator can add new participant
   if (actor != NULL && !is_moderator(actor))
      return NotAuthorized;

   spSubMeeting sm = find_submeeting(sub_id);
   if (sm == NULL)
      return InvalidSubmeetingID;

   spSubMeeting main_sm = find_mainmeeting();

   // Don't allow main meeting to be removed.
   if (main_sm == sm)
      return NotAuthorized;

   IListIterator iter;
   sm->get_participants(iter);

   // Move participants to main meeting.
   Participant * p = (Participant *)iter.get_first();
   spParticipant self = find_participant_by_userid(actor->get_id());
   while (p != NULL)
   {
      if (!remove_parts || (p == self))
         move_participant_internal(p, main_sm);
      else
         remove_participant_internal(p);

      p = (Participant *)iter.get_first();
   }

   // start recording the main submeeting if meeting recording
   // is on and the main submeeting is not recording voice and
   // the submeeting we are deleting has recording turned on
   bool record_main = (options & RecordingMeeting) &&
      (sm->get_options() & RecordingVoice) &&
      !(main_sm->get_options() & RecordingVoice);

   submeetings.remove(sm);

   int status = sm->end(true);

   if (record_main) {
      main_sm->set_options(main_sm->get_options() | RecordingVoice);
   }

   return status;
}

int Meeting::move_participant(User * actor, const char * participant_id, const char * sub_id)
{
   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   // Check access--only moderator can add new participant
   if (actor != NULL && !is_moderator(actor))
      return NotAuthorized;

   spSubMeeting sm = find_submeeting(sub_id);
   if (sm == NULL)
      return InvalidSubmeetingID;

   spParticipant p = find_participant(participant_id);
   if (p == NULL)
      return InvalidParticipantID;

   return move_participant_internal(p, sm);
}

int Meeting::move_participant_internal(Participant * p, SubMeeting * sm)
{
   int status = Ok;

   spSubMeeting curr_sm = p->get_submeeting();
   if (curr_sm != NULL && curr_sm != sm)
   {
      curr_sm->remove_participant(p, false);
      if (sm->is_main())
      {
         // If target submeeting is main, make sure they are not
         // call master;  O/W when they leave, all others are hung
         // up.
         p->set_call_options(p->get_call_options() & (~CallMaster));
      }
      sm->add_participant(p);

      if (p->is_connected())
         status = p->get_user()->move_connections(this, p, curr_sm, sm);

      if (status == Ok)
      {
         bool changed_mute;
         p->check_mute(&changed_mute);
         if (changed_mute)
         {
            log_debug(ZONE, "Turned mute %s for participant %s",
                      (p->get_mute() ? "on" : "off"), p->get_id());

            spPort port = p->get_voice_port();
            if (port != NULL && port->is_connected())
            {
               IString id;
               make_id_string(id, "call:", p->get_id());
               if (rpc_make_call(id,
                                 get_voice_service(), "voice.set_volume", "(sssiii)",
                                 port->get_id(), get_id(), p->get_submeeting()->get_id(),
                                 -1, -1, p->get_mute()))
               {
                  log_error(ZONE, "Error notifying voice service about volume change\n");

                  return Failed;
               }
            }
         }

         dispatcher.send(ParticipantMoved, SendParticipantState, ToAllParticipants, this, sm, p, p);
      }
   }

   return status;
}

int Meeting::start_app_share(User * actor, const char * address, const char * password, int options, int starttime)
{
   int status = Ok;

   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   // Check access--only presenter can start app share
   if (actor != NULL && !is_presenter(actor))
      return NotAuthorized;

   if (share_server == NULL)
      return InsufficientResources;

   share_active = true;
   share_address = address;
   share_password = password;
   share_options = options;
   share_start_time = starttime;

   spSubMeeting main_sm = find_mainmeeting();

   dispatcher.send(AppShareStarted, SendAppShare, ToAllParticipants, this, main_sm, NULL, NULL);

   // Log
   spParticipant part;
   if (actor)
      part = find_participant_by_userid(actor->get_id());
   appshare_event = log_event_start(EvnLog_Event_App_Share,
                                    get_community_id(),
                                    get_id(),
                                    (time_t)get_actual_start(),
                                    get_host()->get_id(),
                                    part != NULL ? part->get_id() : "", "");

   if (mtgarchiver)
   {
      // ----------------------------------------   record data sharing  -------------------------------------
      if (rpc_make_call("togglerecording", mtgarchiver->get_name(),
                        "mtgarchiver.start_recording", "(si)", get_id(), starttime))
      {
         return Failed;
      }
      // ----------------------------------------   record data sharing  -------------------------------------
   }

   // Host connection to share server completed before we got this notification.
   // Notify reflector now that we have the start time.
   if (share_connected && is_recording() && !is_holding())
   {
      share_server->send_recording(get_id(), true, share_start_time);
   }

   return status;
}

int Meeting::end_app_share(User * actor)
{
   int status = Ok;

   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   // Check access--only presenter can stop app share
   if (actor != NULL && !is_presenter(actor))
      return NotAuthorized;

   share_active = false;
   share_address.clear();
   share_password.clear();

   spSubMeeting main_sm = find_mainmeeting();

   dispatcher.send(AppShareEnded, SendAppShare, ToAllParticipants, this, main_sm, NULL, NULL);

   // Log
   if (appshare_event != -1)
   {
      log_event_end(appshare_event);
      appshare_event = -1;
   }

   if (mtgarchiver)
   {
      // ----------------------------------------   record data sharing  -------------------------------------
      if (rpc_make_call("pauserecording", mtgarchiver->get_name(),
                        "mtgarchiver.pause_recording", "(s)", get_id()))
      {
         return Failed;
      }
      // ----------------------------------------   record data sharing  -------------------------------------
   }

   return status;
}

int Meeting::request_share_server(User * actor)
{
   int status = Ok;

   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   // Check access--only presenter can request a new server...
   if (actor != NULL && !is_presenter(actor))
      return NotAuthorized;

   // TODO: should we release any existing session first??

   // Request a new share server
   share_server = appshare.allocate_session(max_participants, meeting_id);

   // Notify presenter about new server
   spParticipant p = find_participant_by_userid(presenter->get_id());

   if (share_server != NULL && p != NULL)
   {
      // Grant control to new presenter.
      dispatcher.send(PresenterGranted, SendAppShare, ToOneParticipant, this, p->get_submeeting(), p, p);
   }

   return status;
}

// Change presenter to a different participant.
int Meeting::change_presenter(User * actor, const char * participant_id)
{
   int status = Ok;

   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   // Only moderator can change presenter
   if (actor != NULL && !is_moderator(actor))
      return NotAuthorized;

   // Find new presenter...if id is blank, we are disabling share.
   spParticipant p;
   if (participant_id != NULL && *participant_id)
   {
      p = find_participant(participant_id);
      if (p == NULL)
         return NotMember;

      if (p->get_user() == presenter)
         return Ok;
   }

   // Stop existing session if any
   if (share_active)
      end_app_share(NULL);

   if (share_document)
   {
      end_doc_share(NULL);
   }

   // Revoke presenter status from current presenter
   if (presenter != NULL)
   {
      spParticipant cur_presenter = find_participant_by_userid(presenter->get_id());
      if (cur_presenter != NULL)
      {
         dispatcher.send(PresenterRevoked, SendAppShare, ToOneParticipant, this, cur_presenter->get_submeeting(), cur_presenter, cur_presenter);

         // Broadcast event to clients
         dispatcher.send(ParticipantState, SendParticipantState, ToAllParticipants, this, cur_presenter->get_submeeting(), cur_presenter, NULL);
      }
   }

   if (p != NULL)
   {
      // Set new presenter.
      presenter = p->get_user();
      if (share_server)
         share_server->send_presenter_id(meeting_id, p->get_id());

      dispatcher.send(ParticipantState, SendParticipantState, ToAllParticipants, this, p->get_submeeting(), p, NULL);

      // Grant control to new presenter.
      dispatcher.send(PresenterGranted, SendAppShare, ToOneParticipant, this, p->get_submeeting(), p, p);
      dispatcher.send(PresenterGranted, SendDocShare, ToOneParticipant, this, p->get_submeeting(), p, p);

      // It's up to new presenter to start sharing his desktop...
   }
   else
      presenter = NULL;

   return status;
}

// Grant or revoke remote control from a participant.
int Meeting::grant_control(User * actor, const char * part_id, int grant)
{
   int status = Ok;

   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   // Only moderator can grant control
   if (actor != NULL && !is_moderator(actor))
      return NotAuthorized;

   // See if user is in this meeting.
   spParticipant p = find_participant(part_id);
   if (p == NULL)
      return NotMember;

   // Enable/clear flag in participant.
   p->set_remote_control(grant != 0);

   if (grant)
      dispatcher.send(ControlGranted, SendAppShare, ToOneParticipant, this, p->get_submeeting(), p, p);
   else
      dispatcher.send(ControlRevoked, SendAppShare, ToOneParticipant, this, p->get_submeeting(), p, p);

   dispatcher.send(ParticipantState, SendParticipantState, ToAllParticipants, this, p->get_submeeting(), p, NULL);

   return status;
}

// App share server has reported host is connected
void Meeting::on_share_started()
{
   IUseMutex lock(meeting_lock);

   share_connected = true;

   if (share_active || share_document)
   {
      // Notify share server if recording should be on
      if (is_recording() && !is_holding())
         share_server->send_recording(get_id(), true, share_start_time);
   }
}

// App share server has reported host is disconnected
void Meeting::on_share_ended()
{
   IUseMutex lock(meeting_lock);

   share_connected = false;
}

// Raise/lower participant's hand.
//
// handup - 0   lower hand
// handup - 1   raise hand
// handup - 2   toggle hand
int Meeting::set_handup(User * actor, const char * part_id, int & handup)
{
   int status = Ok;

   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   // See if user is in this meeting.
   spParticipant p = find_participant(part_id);
   if (p == NULL)
      return NotMember;

   if (handup == 2)
   {
      // toggle hand up; set caller handup state to the new state
      handup = !p->is_handup();
   }
   // Enable/clear flag in participant.
   p->set_handup(handup != 0);

   dispatcher.send(ParticipantState, SendParticipantState, ToAllParticipants, this, p->get_submeeting(), p, NULL);

   return status;
}

int Meeting::set_participant_volume(User * actor, const char * part_id, RPCSESSION rpcsess,
                                    int volume, int gain, int mute)
{
   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   spParticipant p = find_participant(part_id);
   if (p == NULL)
      return NotMember;

   if (actor == NULL)
   {
      // Need to ID the actor; DENIED!
      return NotAuthorized;
   }

   spSubMeeting main_sm = find_mainmeeting();
   bool is_main_sm = (main_sm == p->get_submeeting());

   if (!is_moderator(actor))
   {
      // Not a moderator; need to do some checking to
      // see whether they are allowed

      if (!p->is_user_attached(actor))
      {
         // You can't control me;  DENIED!
         return NotAuthorized;
      }
      else
      {
         if ((options & (LectureMode|HoldMode)) && (mute == MuteOff) && is_main_sm)
         {
            // Turning off mute during lecture mode while in main meeting?  DENIED!
            return NotAuthorized;
         }
      }
   }

   // Reset mute according to meeting options if necessary
   if (mute && is_main_sm)
   {
       mute = MuteOn;
       if (options & HoldMode)
           mute = MuteHold;
   }

   // Must be connected via voice too
   spPort port = p->get_voice_port();
   if (port == NULL || !port->is_connected())
   {
      return NotConnected;
   }

   char id[64];
   sprintf(id, "volume:%d", rpcsess);
   if (rpc_make_call(id,
                     get_voice_service(), "voice.set_volume", "(sssiii)",
                     port->get_id(), get_id(), p->get_submeeting()->get_id(),
                     volume, gain, mute))
   {
      log_error(ZONE, "Error notifying voice service about volume change\n");

      return Failed;
   }

   if (volume != -1)
      p->set_volume(volume);
   if (gain != -1)
      p->set_gain(gain);
   if (mute != -1)
      p->set_mute(mute);

   // SendParticipantState
   dispatcher.send(ParticipantState, SendParticipantState, ToAllParticipants, this, p->get_submeeting(), p, NULL);

   return Ok;
}

int Meeting::get_participant_volume(User * actor, const char * part_id, RPCSESSION rpcsess)
{
   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   spParticipant p = find_participant(part_id);
   if (p == NULL)
      return NotMember;

   if (actor == NULL)
   {
      // Need to ID the actor; DENIED!
      return NotAuthorized;
   }

   if (!is_moderator(actor))
   {
      // Not a moderator; need to do some checking to
      // see whether they are allowed

      if (!p->is_user_attached(actor))
      {
         // You can't control me;  DENIED!
         return NotAuthorized;
      }
   }

   // Must be connected via voice too
   spPort port = p->get_voice_port();
   if (port == NULL || !port->is_connected())
   {
      return NotConnected;
   }

   char id[64];
   sprintf(id, "volume:%d", rpcsess);
   if (rpc_make_call(id,
                     get_voice_service(), "voice.get_volume", "(sss)",
                     port->get_id(), get_id(), p->get_submeeting()->get_id()))
   {
      log_error(ZONE, "Error requesting volume from voice service\n");

      return Failed;
   }

   return Ok;
}

// Grant or revoke moderator privilege from a participant.
int Meeting::grant_moderator(User * actor, const char * part_id, int grant)
{
   int status = Ok;

   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }


   // Only moderator can grant control
   if (actor != NULL && !is_moderator(actor))
      return NotAuthorized;

   // See if user is in this meeting.
   spParticipant p = find_participant(part_id);
   if (p == NULL)
      return NotMember;

   // Enable/clear flag in participant.
   p->set_moderator(grant != 0);

   // SendParticipantState
   dispatcher.send(ParticipantState, SendParticipantState, ToAllParticipants, this, p->get_submeeting(), p, NULL);

   // Reset the participant's view of the world
   dispatcher.send(MeetingReset, SendMeetingState, ToOneParticipant, this, p->get_submeeting(), p, p, NULL);

   // If meeting display is allowing moderators only (for non-moderators)
   // then reset the participant view for non-moderators too...
   if (display_attendees == DisplayModerators)
   {
      dispatcher.send(MeetingReset, SendMeetingState, ToNonModeratorParticipants, this, p->get_submeeting(), p, p, NULL);
   }

   // Voice cares about this now too
   spPort port = p->get_voice_port();
   if (port != NULL && port->is_connected())
   {
      IString call_id;
      make_id_string(call_id, "call:", p->get_id());
      if (rpc_make_call(call_id, get_voice_service(),
                        "voice.grant_moderator",
                        "(sssi)",
                        port->get_id(),
                        get_id(), p->get_submeeting()->get_id(), grant))
      {
         log_error(ZONE, "Error making call to voice.grant_moderator");

         return Failed;
      }
   }

   return status;
}

// Grant or revoke moderator privilege from a participant.
int Meeting::roll_call(User * actor)
{
   int status = Ok;

   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   // Only moderator can employ the roll call mojo
   if (actor != NULL && !is_moderator(actor))
      return NotAuthorized;

   // See if user is in this meeting.
   spParticipant p = find_participant_by_userid(actor->get_id());
   if (p == NULL)
      return NotMember;

   // Tell voice to start belching out the participants
   spPort port = p->get_voice_port();
   if (port != NULL && port->is_connected())
   {
      IString call_id;
      make_id_string(call_id, "call:", p->get_id());
      if (rpc_make_call(call_id, get_voice_service(),
                        "voice.roll_call",
                        "(ss)",
                        port->get_id(),
                        get_id()))
      {
         log_error(ZONE, "Error making call to voice.roll_call");

         return Failed;
      }
   }

   return status;
}

// Place call for the actor
int Meeting::place_call(User * actor, User * callee, const char * callee_number)
{
   int status = Ok;

   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   // Only moderator can employ the roll call mojo
   if (actor != NULL && !is_moderator(actor))
      return NotAuthorized;

   // See if user is in this meeting.
   spParticipant p = find_participant_by_userid(actor->get_id());
   if (p == NULL)
      return NotMember;

   PlaceCallDispatch * disp = new PlaceCallDispatch(actor, this, p, callee, callee_number);

   spSubMeeting sm;
   status = add_submeeting(actor, 2, sm, disp);

   return status;
}

// When user joins from client, reassign participant to that user if different.
int Meeting::reassign_participant(spParticipant& participant, spUser& user, spPort& port)
{
   spUser old_user = participant->get_user();

   if (old_user == user)
      return Ok;

   IUseMutex lock(user->get_lock());

   // If new user or old user is already in meeting, we'll need to create a new participant.
   if (old_user->is_in_meeting(this, port->get_port_type()) ||
       user->is_in_meeting(this, port->get_port_type()))
      return Failed;

   if (*user->get_name() == '\0')
      user->set_name(old_user->get_name());

   if (port->get_port_type() == MediaData)
   {
      // Change participant to point to user associated with data session.
      log_debug(ZONE, "Reassigning participant %s to user %s", participant->get_id(), user->get_id());

      // Reset index
      remove_index(participant);

      participant->set_user(user);
   }

   // Add new index.
   add_index(user, participant);

   return Ok;
}

// Join user to meeting.
// If participant is specified, that's who the user should be joined to meeting as.
int Meeting::join_user(User * _user, Port * _port, const char * participant_id, const char * password, bool release)
{
   int status = Ok;
   spUser user(_user);
   spPort port(_port);
   spParticipant participant;
   spPort curr_port;
   int role = RollNormal;

   if (port == NULL)
   {
      log_debug(ZONE, "Attemtped to join user %s with no port", user->get_id());
      return NoPort;
   }

   if (is_locked())     // don't let anyone else into a locked mtg.
   {
      log_debug(ZONE, "Meeting %s locked; not joining %s", get_id(), user->get_id());

      return MeetingLocked;
   }

   log_debug(ZONE, "Joining user %s to meeting %s", user->get_id(), get_id());

   IUseMutex lock(meeting_lock);

   if (state == StateNone)
   {
      // Start meeting; this user will be joined once meeting is running.
      return start(user, port, participant_id, password, NULL, false, release);
   }
   else if (state == StateLoading || state == StateNotify)
   {
      log_debug(ZONE, "Unable to join user, queueing until meeting %s is loaded", get_id());
      JoinUserDispatch *disp = new JoinUserDispatch(user, port, this, participant_id, password, release);
      pending_queue.add_pending(disp);
      return Pending;
   }

   // See if a participant id was specified--if it was, we will try to connect as that
   // participant.  We assume that this id has been validated by caller already.
   if (participant_id != NULL && *participant_id != '\0')
   {
      participant = find_participant(participant_id);
      if (participant != NULL)
      {
         spUser curr_user = participant->get_user();

         // Check for password (allow starting user to join without password)
         if (!release &&
             (options & RequirePassword) != 0 &&
             !port->is_outbound_call() &&
             this->password != IString(password))
         {
            status = InvalidPassword;
            goto exit;
         }

         // See if another user is already attached to this participant
         if (reassign_participant(participant, user, port) != Ok)
         {
            // Make sure pariticipant remains a moderator even if pin is reused.
            if (participant->is_moderator())
               role = RollModerator;
            participant_id = NULL;
            participant = NULL;
         }
      }
   }

   // Also try search using user id (may need to check that user isn't already
   // connected here also)
   if (participant == NULL)
      participant = find_participant_by_userid(user->get_id());

   // Make sure participant isn't already connected.
   if (participant != NULL)
   {
      curr_port = participant->get_port(port->get_port_type());
      if (curr_port != NULL && curr_port != port && curr_port->is_connected())
      {
         // Make sure pariticipant remains a moderator even if pin is reused.
         if (participant->is_moderator())
            role = RollModerator;
         participant_id = NULL;
         participant = NULL;
      }
   }

   if (participant == NULL)
   {
      // Previously unknown participant...password must match.
      if ((options & RequirePassword) != 0 &&
          !port->is_outbound_call() &&
          this->password != IString(password))
      {
         status = InvalidPassword;
         goto exit;
      }

      // Add participant if we can.
      if (!is_private() || is_moderator(user))
      {
         spSubMeeting sm = find_mainmeeting();

         // Have to register with db to get pin
         JoinUserDispatch * disp = new JoinUserDispatch(user, port, this, participant_id, password, release);

         char id[32];
         const char * dispatch_id = dispatch_queue.add_dispatch(disp);
         sprintf(id, "dispatch:%s", dispatch_id);

         // Convert user id and participant to form acceptable for db
         Address addr;
         if (parse_address(user->get_id(), &addr) == Ok)
         {
            if (addr.type != AddressUser || !isdigits(addr.id))
               strcpy(addr.id, "-1");
         }

         Address part_addr;
         parse_address(participant_id, &part_addr);

         // Determine correct value for invite expiration for this participant.
         // (Sorry, the meeting option flag is poorly named...we check InviteNeverExires
         // to determine if it should expire immediately)
         int actual_expiration = (options & InviteNeverExpires) ? InviteExpiresImmediately : invite_expiration;

         // SESSIONID, MEETINGID, PARTID, USERID, USERNAME, TYPE, ROLE, NAME, SEL_PHONE, PHONE, SEL_EMAIL, EMAIL,
         // SCREENNAME, DESCRIPTION, NOTIFYTYPE, MESSAGE, EXPIRES, STARTTIME
         if (rpc_make_call(id, "addressbk", "schedule.join_participant", "(sssssiisisisssisii)", "sys:controller",
                           get_id(), part_addr.id, addr.id, user->get_username(), ParticipantInvitee, user == host ? RollHost : role,
                           user->get_name(), 0, "", 0, "", "", "", 0, "", actual_expiration, actual_start_time))
         {
            log_error(ZONE, "Error registering participant for meeting %s\n", get_id());
            return Failed;
         }

         return Pending;
      }
      else
      {
         // Cannot join private meeting...moderator must explicitly add
         status = NotAuthorized;
         log_debug(ZONE, "User %s is not authorized to join meeting %s", user->get_id(), get_id());
         goto exit;
      }
   }

   if (status == Ok)
      status = join_user_internal(user, port, participant, participant_id, password, release, role);

  exit:
   return status;
}

int Meeting::join_user_internal(spUser & user, spPort & port, spParticipant & participant, const char * part_id, const char * password, int release, int role)
{
   int status = Ok;
   int num_connected, num_moderators, num_invited, num_calls, num_app_share, num_licenses;
   spSubMeeting sm;
   spUser assigned_user;
   bool wait = false;
   bool in_meeting = false;

   IUseMutex lock(meeting_lock);

   if (state != StateActive && state != StateHolding && state != StateLoading)
      return NotActive;

   if (participant == NULL)
      participant = find_participant_by_userid(user->get_id());

   // We may have requested PIN from database, but not created a participant yet.
   if (participant == NULL)
   {
      sm = find_mainmeeting();

      status = add_participant_internal(NULL, sm, user, part_id, ParticipantNormal, user == host ? RollHost : role,
                                        user->get_name(), 0, "", 0, "", "", "", 0, 0, false, participant);
      if (status != Ok)
         goto exit;

      // We didn't dispatch events when adding participant, so let voice bridge know now
      voice_notify_participant(user, participant);
   }

   // Check for password (allow starting user to join without password)
   if (!release &&
       (options & RequirePassword) != 0  &&
       !port->is_outbound_call() &&
       this->password != IString(password))
   {
      status = InvalidPassword;
      goto exit;
   }

   // Make sure participant is actually in this meeting.
   sm = participant->get_submeeting();
   if (sm == NULL || sm->get_meeting() != this)
   {
      log_error(ZONE, "Participant %s not set up correctly for meeting %s\n", participant->get_id(), get_id());
      return Failed;
   }

   if (!sm->is_active())
   {
      log_error(ZONE, "User %s attempted to join inactive submeeting %s\n", user->get_id(), sm->get_id());
      return NotActive;
   }

   // Convert to normal participant--if this was an invited participant, it was hidden.
   participant->set_participant_type(ParticipantNormal);

   // Update display name for participant.
   assigned_user = participant->get_user();
   if (*assigned_user->get_name() != '\0')
      participant->set_name(assigned_user->get_name());

   // If user does not match host, assume PIN was used by another person and
   // reset to moderator role.
   if (participant->get_roll() == RollHost && !participant->is_user_attached(host))
      participant->set_roll(RollModerator);

   // Leave current meeting before joining this meeting.
   if (port->get_participant() != NULL)
   {
      spMeeting other = port->get_participant()->get_meeting();
      if (other && other != this)
         other->leave_user(user, port, false);
   }

   // See if we are new to this meeting before we set up new connection.
   in_meeting = user->is_in_meeting(this);

   // See how participant should be connected
   get_meeting_counts(num_connected, num_moderators, num_invited, num_calls, num_app_share, num_licenses, true);
   if (!participant->is_moderator() && ((this->options & ContinueWithoutModerator) == 0) &&  num_moderators == 0)
   {
      wait = true;
   }

   // Call is complete so we can clear any intermediate call progress
   if (participant->get_voice_port() != NULL)
      participant->set_progress(0);

   bool changed_mute;
   participant->check_mute(&changed_mute);

   // Complete connection
   status = participant->connect_port(port, wait);
   if (IS_FAILURE(status))
      goto exit;

   dispatcher.send(MeetingJoined, SendMeetingState, ToOneParticipant,
                   this, sm, participant, participant);

   // Data client needs to know if anything interesting is already going on
   if (port->get_port_type() == MediaData)
   {
      // If this is a presenter, send some additional info
      if (user == presenter)
      {
         dispatcher.send(PresenterGranted, SendAppShare, ToOneParticipant, this, sm, participant, participant);
         dispatcher.send(PresenterGranted, SendDocShare, ToOneParticipant, this, sm, participant, participant);
      }

      // If app share session already in progress, send that info out also
      if (share_active)
      {
         dispatcher.send(AppShareStarted, SendAppShare, ToOneParticipant, this, sm, participant, participant);
      }
      if (share_document)
      {
         dispatcher.send(DocShareStarted, SendDocShare, ToOneParticipant, this, sm, participant, participant);
      }
   }
   else if (port->get_port_type() == MediaVoice)
   {
      if (participant->get_call_options() & CallPCPhone)
      {
         call_progress(user, StatusAnswered);
      }
   }

   if (!in_meeting)
   {
      // Send join event to new participant.
      dispatcher.send(ParticipantJoined, SendParticipantState, ToAllParticipants,
                      this, sm, participant, participant);
   }
   else
   {
      // User was already in meeting, this is a just a new connection.
      dispatcher.send(ParticipantState, SendParticipantState, ToAllParticipants,
                      this, sm, participant, participant);
   }

   // Either waiting for anyone to join or a moderator to join before going "active" and sending notifications.
   if (release && (state == StateHolding))
   {
      state = StateActive;

      send_start_notifications();

      if ((options & RecordingMeeting) && !(options & HoldMode))
      {
         // Bit of a hack to make sure we get recording into the right mode for this newly started meeting
         int mode;
         toggle_record_meeting(user, &mode);
         if (!mode)
            toggle_record_meeting(user, &mode);
      }
   }
   else if (state == StateActive)
   {
      dispatcher.send(MeetingStarted, SendEventOnly, ToOneParticipant, this, sm, participant, participant);
   }
   else if (participant->is_moderator() || user->is_superadmin())
   {
      dispatcher.send(MeetingWaiting, SendEventOnly, ToOneParticipant, this, sm, participant, participant);
   }

   if (share_server && user == presenter)
   {
      share_server->send_presenter_id(meeting_id, participant->get_id());
   }

   if (options & ChatMode)
   {
       dispatch_chats(participant->get_id());
   }

  exit:
   return status;
}

void Meeting::on_join_failed(User * user, int status)
{
   IUseMutex lock(meeting_lock);

   // Inform client of failure.
   user->send_error("join", status, "Error joining user", get_id(), user->get_id());
   log_debug(ZONE, "Failed to join user", user->get_id());

   // If we couldn't join the starting user, then the meeting must be ended.
   if (user == starting_user)
   {
      int num_connected, num_moderators, num_invited, num_calls, num_app_share, num_licenses;
      get_meeting_counts(num_connected, num_moderators, num_invited, num_calls, num_app_share, num_licenses);

      if (num_connected == 0)
         on_start_failed(status);
   }
}

// sets a timeout via a timer to check presence drop-outs
void Meeting::set_moderator_presence_timeout()
{
   if (!timer_set)
   {
      timer->set_timeout(TimerPresenceLost, PRESENCE_LOST_TIMEOUT);
      timer_set = true;
   }
}

void Meeting::on_timeout(int timer_id)
{
   IUseMutex lock(meeting_lock);

   if (timer_id == TimerPresenceLost)
   {
      timer_set = false;
      leave_user_timeout();
   }
   else if (timer_id == TimerMeetingStart)
   {
      log_error(ZONE, "Meeting start failed due to timeout");
      on_start_failed(MeetingStartTimeout);
   }
   else if (timer_id == TimerMtgArchiverSending)
   {
      log_error(ZONE, "Sending request to mtgarchiver failed due to timeout");
      on_recording_result(MtgArchiverTimeout);
   }
   else if (timer_id == TimerMeetingEnd)
   {
       release_internal();
   }
}

// Disconnect one port for user from meeting.
int Meeting::leave_user(User * user, Port * port, bool is_disconnect)
{
   int status = Ok;
   spParticipant p;

   IUseMutex lock(meeting_lock);

   if (port != NULL)
      p = port->get_participant();
   if (p == NULL)
      p = find_participant_by_userid(user->get_id());
   if (p == NULL)
      return NotMember;

   spSubMeeting sm = p->get_submeeting();

   // Complete disconnect
   if (port != NULL)
      status = p->disconnect_port(port, true);
   else
      status = p->disconnect(true);
   if (status)
      return status;

   // See if meeting must be stopped.
   int num_connected, num_moderators, num_invited, num_calls, num_app_share, num_licenses;
   get_meeting_counts(num_connected, num_moderators, num_invited, num_calls, num_app_share, num_licenses);

   if (num_connected == 0 ||
       (((this->options & ContinueWithoutModerator) == 0) && num_moderators == 0 && state != StateHolding))
   {
      if (is_disconnect) // delay meeting end to allow for network interruption
      {
         // set timer
         set_moderator_presence_timeout();

         // Send meeting state to this participant.
         dispatcher.send(MeetingLeft, SendEventOnly, ToOneParticipant, this, sm, p, p);
         dispatcher.send(ParticipantLeft, SendParticipantState, ToAllParticipants, this, sm, p, p);
      }
      else // end immediately
      {
         status = end_internal(user, true);

         dispatcher.send(MeetingLeft, SendEventOnly, ToOneParticipant, this, NULL, p, p);
         dispatcher.send(MeetingEnded, SendEventOnly, ToAllParticipants, this, NULL, NULL, NULL);
      }
   }
   else
   {
      if (p->is_connected())
      {
         // Participant is still connected to meeting, send state change.
         dispatcher.send(ParticipantState, SendParticipantState, ToAllParticipants, this, sm, p, p);
      }
      else
      {
         // Participant has left meeting.
         dispatcher.send(MeetingLeft, SendEventOnly, ToOneParticipant, this, sm, p, p);
         dispatcher.send(ParticipantLeft, SendParticipantState, ToAllParticipants, this, sm, p, p);
      }
   }

   return status;
}

int Meeting::leave_user_timeout()
{
   int status = Ok;

   IUseMutex lock(meeting_lock);

   // See if meeting must be stopped.
   int num_connected, num_moderators, num_invited, num_calls, num_app_share, num_licenses;
   get_meeting_counts(num_connected, num_moderators, num_invited, num_calls, num_app_share, num_licenses);

   if (num_connected == 0 ||
       (((this->options & ContinueWithoutModerator) == 0) && num_moderators == 0))
   {
      status = end_internal(NULL, true);
      dispatcher.send(MeetingEnded, SendEventOnly, ToAllParticipants, this, NULL, NULL, NULL);
   }

   return status;
}

int Meeting::state_changed(User * user)
{
   IUseMutex lock(meeting_lock);

   IRefObjectPtr<Participant> p = find_participant_by_userid(user->get_id());
   if (p == NULL)
      return NotMember;

   p->set_progress(0);

   if (state == StateActive)
      dispatcher.send(ParticipantState, SendParticipantState, ToAllParticipants, this, p->get_submeeting(), p, p);

   return Ok;
}

int Meeting::call_progress(User * user, int progress)
{
   IUseMutex lock(meeting_lock);

   IRefObjectPtr<Participant> p = find_participant_by_userid(user->get_id());
   if (p == NULL)
      return NotMember;

   p->set_progress(progress);

   if (state == StateActive)
      dispatcher.send(ParticipantState, SendParticipantState, ToAllParticipants, this, p->get_submeeting(), p, p);

   return Ok;
}

// See if user has moderator privilege.
// Note that an admin for the same community, or super admin will also have moderator priv.
bool Meeting::is_moderator(User * actor)
{
   IUseMutex lock(meeting_lock);

   if (actor == host)
      return true;

   // *** Admin should only be moderator if meeting is in his community.
   if (actor->is_admin() || actor->is_superadmin())
      return true;

   IRefObjectPtr<Participant> p = find_participant_by_userid(actor->get_id());
   if (p == NULL || p->is_moderator() != true)
      return false;
   return true;
}

bool Meeting::is_participant(User * actor)
{
   IUseMutex lock(meeting_lock);

   if (actor == host)
      return true;

   IRefObjectPtr<Participant> p = find_participant_by_userid(actor->get_id());
   if (p != NULL)
      return true;
   return false;
}

spSubMeeting Meeting::find_submeeting(const char * id)
{
   IUseMutex lock(meeting_lock);

   IListIterator iter(submeetings);

   SubMeeting * m = (SubMeeting *) iter.get_first();
   while (m != NULL)
   {
      if (!strcmp(id, m->get_id()))
         break;
      m = (SubMeeting *) iter.get_next();
   }

   return spSubMeeting(m);
}

spSubMeeting Meeting::find_mainmeeting()
{
   IUseMutex lock(meeting_lock);

   IListIterator iter(submeetings);

   SubMeeting * m = (SubMeeting *) iter.get_first();
   while (m != NULL)
   {
      if (m->is_main())
         break;
      m = (SubMeeting *) iter.get_next();
   }

   return spSubMeeting(m);
}

spParticipant Meeting::find_participant(const char * id)
{
   IUseMutex lock(meeting_lock);

   spParticipant p;

   IListIterator iter(submeetings);

   // Don't user controller to look up id as getting controller lock
   // while meeting is locked may cause problems.
   SubMeeting * m = (SubMeeting *) iter.get_first();
   while (p == NULL && m != NULL)
   {
      p = m->find_participant(id);
      m = (SubMeeting *) iter.get_next();
   }

   return p;
}

spParticipant Meeting::find_participant_by_userid(const char * id)
{
   IUseMutex lock(meeting_lock);

   IString str(id);
   Participant * p = (Participant*) all_participants.get(&str);

   return spParticipant(p);
}

void Meeting::add_index(User * user, Participant * participant)
{
   IString * key = new IString(user->get_id());
   all_participants.insert(key, participant);

   // also add index for 'pin:xxx' user id.
   if (strncmp(user->get_id(), "pin:", 4) != 0 &&
       strncmp(participant->get_id(), "part:", 5) == 0)
   {
      key = new IString();
      make_id_string(*key, "pin:", participant->get_id() + 5);
      all_participants.insert(key, participant);
   }
}

void Meeting::remove_index(Participant *p)
{
   spUser user = p->get_user();

   IString key(user->get_id());
   all_participants.remove(&key);

   if (strncmp(key, "pin:", 4) != 0 &&
       strncmp(p->get_id(), "part:", 5) == 0)
   {
      make_id_string(key, "pin:", p->get_id() + 5);
      all_participants.remove(&key);
   }
}

// Get counts of participants connected to meeting, optionally only counting
// participants connected by voice.
int Meeting::get_meeting_counts(int & num_connected,
                                int & num_moderators,
                                int & num_invited,
                                int & num_calls,
                                int & num_app_share,
                                int & num_licenses,
                                bool voice_only)
{
   IUseMutex lock(meeting_lock);

   SubMeeting * sm;
   IListIterator iter;

   bool bVoice = false;
   bool bData = false;
   int connected_voiceonly = 0;
   int connected_dataonly = 0;

   num_connected = 0;
   num_moderators = 0;
   num_invited = 0;
   num_calls = 0;
   num_app_share = 0;
   num_licenses = 0;

   get_submeetings(iter);
   sm = (SubMeeting *)iter.get_first();

   while (sm != NULL)
   {
      IListIterator iter2;
      sm->get_participants(iter2);
      Participant * p = (Participant *)iter2.get_first();

      while (p != NULL)
      {
         if ( (!voice_only && p->is_connected()) ||
              (voice_only && (p->get_status() & StatusVoice) != 0) )
         {
            num_connected++;
            if (p->is_moderator())
               num_moderators++;
         }

         spPort data = p->get_data_port();
         bData = data && data->is_connected();
         if (bData && share_active)
            num_app_share++;

         spPort voice = p->get_voice_port();
         bVoice = voice && voice->is_connected();
         if (bVoice)
            num_calls++;

         if (bData && bVoice)
         {
            num_licenses++;
         }
         else if (bData)
         {
            connected_dataonly++;
         }
         else if (bVoice)
         {
            connected_voiceonly++;
         }

         num_invited++;

         p = (Participant *)iter2.get_next();
      }
      sm = (SubMeeting *)iter.get_next();
   }

   if (connected_dataonly > connected_voiceonly)
   {
      num_licenses += connected_dataonly;
   }
   else
   {
      num_licenses += connected_voiceonly;
   }

   return Ok;
}

int Meeting::get_status(int options, int & state, int & m_opt, int & sm_opt)
{
   IUseMutex lock(meeting_lock);

   spSubMeeting main_sm = find_mainmeeting();

   state = get_state();
   m_opt = get_options();
   sm_opt = main_sm->get_options();

   if (options & 1)
   {
      IString id;
      make_id_string(id, "call:", get_id());
      if (rpc_make_call(id,
                         get_voice_service(), "voice.modify_submeeting", "(ssi)",
                         get_id(), main_sm->get_id(), main_sm->get_options()))
      {
         log_error(ZONE, "Error notifying voice service about submeeting %s\n", main_sm->get_id());
      }
   }

   return Ok;
}

int Meeting::start_doc_share(User * actor, const char * address, int share_starttime)
{
   int status = Ok;

   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   // Check access--only presenter can start app share
   if (actor != NULL && !is_presenter(actor))
      return NotAuthorized;

   if (docserver == NULL)
      return InsufficientResources;

   share_document = true;
   doc_share_address = address;
   share_start_time = share_starttime;

   spSubMeeting main_sm = find_mainmeeting();

   dispatcher.send(DocShareStarted, SendDocShare, ToAllParticipants, this, main_sm, NULL, NULL);

   // Log
   spParticipant part;
   if (actor)
      part = find_participant_by_userid(actor->get_id());
   docshare_event = log_event_start(EvnLog_Event_Doc_Share,
                                    get_community_id(),
                                    get_id(),
                                    (time_t)get_actual_start(),
                                    get_host()->get_id(),
                                    part != NULL ? part->get_id() : "", "");

   if (mtgarchiver)
   {
      // ----------------------------------------   record data sharing  -------------------------------------
      if (rpc_make_call("togglerecording", mtgarchiver->get_name(),
                        "mtgarchiver.start_recording", "(si)", get_id(), share_starttime))
      {
         return Failed;
      }
      // ----------------------------------------   record data sharing  -------------------------------------
   }

   // Host connection to share server completed before we got this notification.
   // Notify reflector now that we have the start time.
   if (share_connected && is_recording() && !is_holding())
   {
      share_server->send_recording(get_id(), true, share_start_time);
   }

   return status;
}

int Meeting::end_doc_share(User * actor)
{
   int status = Ok;

   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   // Check access--only presenter can stop app share
   if (actor != NULL && !is_presenter(actor))
      return NotAuthorized;

   share_document = false;
   doc_share_address.clear();

   spSubMeeting main_sm = find_mainmeeting();

   dispatcher.send(DocShareEnded, SendDocShare, ToAllParticipants, this, main_sm, NULL, NULL);

   // Log
   if (docshare_event != -1)
   {
      log_event_end(docshare_event);
      docshare_event = -1;
   }

   if (mtgarchiver)
   {
      // ----------------------------------------   record data sharing  -------------------------------------
      if (rpc_make_call("pauserecording", mtgarchiver->get_name(),
                        "mtgarchiver.pause_recording", "(s)", get_id()))
      {
         return Failed;
      }
      // ----------------------------------------   record data sharing  -------------------------------------
   }

   return status;
}

int Meeting::download_document_page(User * actor, const char * docid, const char * pageid)
{
   int status = Ok;

   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   doc_share_document_id = docid;
   doc_share_page_id = pageid;

   spSubMeeting main_sm = find_mainmeeting();

   dispatcher.send(DocShareDownloadPage, SendDocShare, ToAllParticipants, this, main_sm,
                   doc_share_document_id, doc_share_page_id, 0, NULL);

   return status;
}

int Meeting::configure_whiteboard(User * actor, int options)
{
   int status = Ok;

   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   // Check access--only presenter can configure app share
   if (actor != NULL && !is_presenter(actor))
      return NotAuthorized;

   doc_share_options = options;

   spSubMeeting main_sm = find_mainmeeting();

   dispatcher.send(DocShareConfigure, SendDocShare, ToAllParticipants, this, main_sm,
                   doc_share_document_id, doc_share_page_id, doc_share_options, NULL);

   return status;
}

int Meeting::draw_on_whiteboard(User * actor, const char * drawing)
{
   int status = Ok;

   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   doc_share_drawing = drawing;

   spSubMeeting main_sm = find_mainmeeting();

   dispatcher.send(DocShareDrawing, SendDocShare, ToAllParticipants, this, main_sm,
                   doc_share_document_id, doc_share_page_id, 0, drawing);


   return status;
}

int Meeting::on_ppt_remote_control(User * actor, const char * action)
{
   int status = Ok;

   IUseMutex lock(meeting_lock);

   if (state == StateLoading || state == StateNotify)
   {
      return Retry;
   }
   else if (state != StateActive && state != StateHolding)
   {
      return NotActive;
   }

   spSubMeeting main_sm = find_mainmeeting();

   dispatcher.send(PPTAction, SendPPTShare, ToAllParticipants, this, main_sm, NULL, NULL, action);

   return status;
}

// The client will request doc server if it has not got one yet, the current one does not work
// the client will restart the doc share anyway, so it is ok not to dispatch the new server to
// the all participants
const char* Meeting::get_doc_share_server()
{
   IUseMutex lock(meeting_lock);

   if (docserver != NULL)
   {
      docshare.release_session(docserver);
      docserver = NULL;
   }

   docserver = docshare.allocate_session(max_participants);

   if (docserver!=NULL)
      return docserver->get_address();

   return NULL;
}
