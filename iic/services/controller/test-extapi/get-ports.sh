#!/bin/sh -f

sed -e "s/@@@SESSIONID@@@/$1/" \
    -e "s/@@@SERVERID@@@/$2/" \
< get-ports-src.xml > get-ports.xml

curl --data @get-ports.xml http://sniper.homedns.org:8000/imidio_api
