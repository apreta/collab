import getopt, sys
from xmlrpclib import ServerProxy, Error

host = "localhost"
user = "admin"
password = "admin"
first = 1000
count = 1

def usage():
	print "Ends meeting.\n\nUsage:"
	print "endmeeting --server=[host name] --user=[admin username] --password=[admin password] --pin=xxx"
	

try:
	opts, args = getopt.getopt(sys.argv[1:], "hs:u:p:f:", ["help", "server=", "user=", "password=","pin="])
except getopt.GetoptError, err:
	# print help information and exit:
	print str(err) # will print something like "option -a not recognized"
	usage()
	sys.exit(2)

for o, a in opts:
	if o in ("-h", "--help"):
		usage()
		sys.exit()
	elif o in ("-s", "--server"):
		host = a
	elif o in ("-u", "--user"):
		user = a
	elif o in ("-s", "--password"):
		password = a
	elif o in ("-f", "--pin"):
		pin = a
	else:
		assert False, "unhandled option"


server = ServerProxy("http://%s:8000/imidio_api/" % (host)) # local server


try:
	res = server.addressbk.authenticate_user(user, password, "")

	sessionid = res[0]
	userid = res[1]
	commid = res[3]

	res = server.controller.register_user("user:"+userid,
			"", "", "iic:admin", "", "")
	sessionid = res[0]

	res = server.controller.end_meeting(sessionid, pin)

	print res

except Error, v:
    print "ERROR", v
