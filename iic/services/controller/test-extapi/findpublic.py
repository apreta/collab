import getopt, sys
from xmlrpclib import ServerProxy, Error

host = "localhost"
user = "admin"
password = "admin"
first = 1000
community = "2"

def usage():
	print "Fetch public meetings from community\n\nUsage:"
	print "findpublic --server=[host name] --community=[id]"
	

try:
	opts, args = getopt.getopt(sys.argv[1:], "hs:u:p:c:", ["help", "server=", "user=", "password=", "community="])
except getopt.GetoptError, err:
	# print help information and exit:
	print str(err) # will print something like "option -a not recognized"
	usage()
	sys.exit(2)

for o, a in opts:
	if o in ("-h", "--help"):
		usage()
		sys.exit()
	elif o in ("-s", "--server"):
		host = a
	elif o in ("-u", "--user"):
		user = a
	elif o in ("-s", "--password"):
		password = a
	elif o in ("-c", "--community"):
		community = a
	else:
		assert False, "unhandled option"


server = ServerProxy("http://%s:8000/imidio_api/" % (host)) # local server


try:
	res = server.addressbk.authenticate_user(user, password, "")

	sessionid = res[0]
	userid = res[1]

	if sessionid == "":
		raise Exception,"Log in failed"

	res = server.schedule.find_meetings_public_url(sessionid, community, "")

	print res


except Error, v:
    print "ERROR", v
