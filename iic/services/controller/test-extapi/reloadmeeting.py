import getopt, sys
from xmlrpclib import ServerProxy, Error

host = "localhost"
user = "useradmin"
password = "admin"
first = 1000
count = 1
id = 0

def usage():
	print "Reload meeting from schedule.\n\nUsage:"
	print "reloadmeeting --server=[host name] --user=[admin username] --password=[admin password] --id=xxx"
	

try:
	opts, args = getopt.getopt(sys.argv[1:], "hs:u:p:i:", ["help", "server=", "user=", "password=","id="])
except getopt.GetoptError, err:
	# print help information and exit:
	print str(err) # will print something like "option -a not recognized"
	usage()
	sys.exit(2)

for o, a in opts:
	if o in ("-h", "--help"):
		usage()
		sys.exit()
	elif o in ("-s", "--server"):
		host = a
	elif o in ("-u", "--user"):
		user = a
	elif o in ("-p", "--password"):
		password = a
	elif o in ("-i", "--id"):
		id = a
	else:
		assert False, "unhandled option"


server = ServerProxy("http://%s:8000/imidio_api/" % (host)) # local server


try:
	res = server.addressbk.authenticate_user(user, password, "")

	sessionid = res[0]
	userid = res[1]
	commid = res[3]

	res = server.controller.register_user("user:"+userid,
			"", "", "iic:admin", "", "")
	sessionid = res[0]
	
	print "Session id: %s" % sessionid

	res = server.controller.reload_meeting(sessionid, id)

	print res

except Error, v:
    print "ERROR", v
