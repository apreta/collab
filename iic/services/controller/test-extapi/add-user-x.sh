#!/bin/sh -f

finit="j"
fname="john"
lname="doe"
mtgid=""
hostid=""

phost="test3-portal.homedns.org"

while [ $# -gt 0 ] ; do
    case $1 in
	--finit)
	    finit=$2
	    shift 2
	    ;;

	--fname)
	    fname=$2
	    shift 2
	    ;;

	--lname)
	    lname=$2
	    shift 2
	    ;;

	--mtgid)
	    mtgid=$2
	    shift 2
	    ;;

	--hostid)
	    hostid=$2
	    shift 2
	    ;;

	*)
	    echo "Unknown option $1; see $0 for more info"
	    exit
	    ;;
    esac
done

sed \
    -e "s/@@@FINIT@@@/$finit/" \
    -e "s/@@@FNAME@@@/$fname/" \
    -e "s/@@@LNAME@@@/$lname/" \
    -e "s/@@@MTGID@@@/$mtgid/" \
    -e "s/@@@HOSTID@@@/$hostid/" \
    add-user-x-src.xml \
> add-user-x.xml

curl --data @add-user-x.xml http://$phost:8000/imidio_api/
