#!/bin/sh -f

sed -e "s/@@@SESSIONID@@@/$1/" \
    -e "s/@@@SERVICETYPE@@@/$2/" \
< get-servers-src.xml > get-servers.xml

curl --data @get-servers.xml http://localhost/imidio_api/
