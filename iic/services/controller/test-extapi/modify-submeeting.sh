#!/bin/sh -f

meetingid=""
submeetingid=""
options=""

function usage
{
    echo "$0 --mtgid <mtgid> --submtgid <submtgid> --options <options>"
    exit -1
}

while [ $# -gt 0 ] ; do
    case $1 in
	--mtgid)
	    meetingid=$2
	    shift 2
	    ;;
	--submtgid)
	    submeetingid=$2
	    shift 2
	    ;;
	--options)
	    options=$2
	    shift 2
	    ;;
	-h|--help)
	    usage
	    ;;
	*)
	    echo "Unknown option $1"
	    usage
	    ;;
    esac
done

if [ ! -z "$meetingid" -a ! -z "$submeetingid" -a ! -z "$options" ]; then
    sed -e "s/@@@MEETINGID@@@/$meetingid/" \
	-e "s/@@@SUBMEETINGID@@@/$submeetingid/" \
	-e "s/@@@OPTIONS@@@/$options/" \
    < modify-submeeting-src.xml > modify-submeeting.xml
fi

#cat modify-submeeting.xml
curl --data @modify-submeeting.xml http://iic-portal.homedns.org:8000/imidio_api/
