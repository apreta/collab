import getopt, sys
from xmlrpclib import ServerProxy, Error

host = "localhost"
user = "useradmin"
password = "admin"
first = 1000
count = 1

def usage():
	print "Finds pin.\n\nUsage:"
	print "findpin --server=[host name] --user=[admin username] --password=[admin password] --pin=xxx"
	

try:
	opts, args = getopt.getopt(sys.argv[1:], "hs:u:p:f:", ["help", "server=", "user=", "password=","pin="])
except getopt.GetoptError, err:
	# print help information and exit:
	print str(err) # will print something like "option -a not recognized"
	usage()
	sys.exit(2)

for o, a in opts:
	if o in ("-h", "--help"):
		usage()
		sys.exit()
	elif o in ("-s", "--server"):
		host = a
	elif o in ("-u", "--user"):
		user = a
	elif o in ("-p", "--password"):
		password = a
	elif o in ("-f", "--pin"):
		pin = a
	else:
		assert False, "unhandled option"


server = ServerProxy("http://%s:8000/imidio_api/" % (host)) # local server


try:
	res = server.addressbk.authenticate_user(user, password, "")

	sessionid = res[0]
	userid = res[1]
	commid = res[3]

	# option 1: include role
	res = server.controller.find_pin(sessionid, pin, 1)

	print res

except Error, v:
    print "ERROR", v
