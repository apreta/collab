import getopt, sys
from xmlrpclib import ServerProxy, Error

host = "localhost"
user = "useradmin"
password = "admin"
first = 1000
count = 1

def usage():
	print "Creates test users with screen names in the form: test####\n\nUsage:"
	print "adduser --server=[host name] --user=[admin username] --password=[admin password] --first=[first user] --count=[number of users]"
	

try:
	opts, args = getopt.getopt(sys.argv[1:], "hs:u:p:f:c:", ["help", "server=", "user=", "password=","first=","count="])
except getopt.GetoptError, err:
	# print help information and exit:
	print str(err) # will print something like "option -a not recognized"
	usage()
	sys.exit(2)

for o, a in opts:
	if o in ("-h", "--help"):
		usage()
		sys.exit()
	elif o in ("-s", "--server"):
		host = a
	elif o in ("-u", "--user"):
		user = a
	elif o in ("-s", "--password"):
		password = a
	elif o in ("-f", "--first"):
		first = int(a)
	elif o in ("-c", "--count"):
		count = int(a)
	else:
		assert False, "unhandled option"


server = ServerProxy("http://%s:8000/imidio_api/" % (host)) # local server


try:
	res = server.addressbk.authenticate_user(user, password, "")

	sessionid = res[0]
	userid = res[1]
	commid = res[3]
	
	for i in range(first,first + count):
		first = "first%d" % i
		last = "last%d" % i
		screen = "test%04d" % i
		passwd = "test"
		email = "flast%d@someco.com" % i
		
#	xmlrpc_parse_value(env, param_array, "(ssssssssssssssssssssssssssssssssss)",
#                       &sid, &pwd, &communityid, &title, &first, &middle, &last, &suffix,
#                       &company, &jobtitle, &address1, &address2, &state, &country, &postalcode,
#                       &screenname, &email1, &email2, &email3, &busphone, &homephone, &mobilephone, &otherphone, &ext,
#                       &aim, &yahoo, &msn, &user1, &user2, &defphone, &defemail, &type, &profileid, &userid);

		server.addressbk.add_user(sessionid, passwd, commid, "", first, "", last, "",
			"", "", "", "", "", "", "", 
			screen, email, "", "", "", "", "", "", "",
			"", "", "", "", "", "0", "0", "1", "", "")

		print "added " + screen


except Error, v:
    print "ERROR", v
