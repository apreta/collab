import getopt, sys
from xmlrpclib import ServerProxy, Error

host = "localhost"
id = 0
option = 1

def usage():
	print "Get meeting status.\n\nUsage:"
	print "meetingstatus --server=[host name] --id=xxx --option=xxx"
	

try:
	opts, args = getopt.getopt(sys.argv[1:], "hs:i:o:", ["help", "server=", "id=", "option="])
except getopt.GetoptError, err:
	# print help information and exit:
	print str(err) # will print something like "option -a not recognized"
	usage()
	sys.exit(2)

for o, a in opts:
	if o in ("-h", "--help"):
		usage()
		sys.exit()
	elif o in ("-s", "--server"):
		host = a
	elif o in ("-i", "--id"):
		id = a
	elif o in ("-o", "--option"):
		option = int(a)
	else:
		assert False, "unhandled option"


server = ServerProxy("http://%s:8000/imidio_api/" % (host)) # local server


try:
	sessionid = "sys:voice"

	res = server.controller.get_meeting_status(sessionid,id,option)

	print res

except Error, v:
    print "ERROR", v
