/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef MEETING_H
#define MEETING_H

#include "../../util/istring.h"
#include "../../util/ithread.h"
#include "../common/timer.h"
#include "apidispatch.h"

#include "appshare.h"
#include "docshare.h"

class Meeting;
class SubMeeting;
//class Hub;
class User;
class Chat;
class RPCResultSet;

typedef IRefObjectPtr<SubMeeting> spSubMeeting;
typedef IRefObjectPtr<Participant> spParticipant;
typedef IRefObjectPtr<Chat> spChat;
typedef IRefObjectPtr<Meeting> spMeeting;

enum TimerType
{
	TimerPresenceLost = 1,
	TimerMeetingStart = 2,
	TimerMtgArchiverSending = 3,
	TimerMeetingEnd = 4,
};

/*
   Meetings, submeeting, participants are managed within this service.

   Outgoing actions:
      Submeeting creation --> voice hub is created
      Call --> call is initiated to target

*/


// Connect a user (or enable to connect) to a meeting.
// Participant ids are generated internally.
class Participant : public IReferencableObject
{
	IString participant_id;

	// User link may be changed when user joins meeting as participant.
	// Also, voice port may be connected to a different user.
    spUser user;

	spPort voice_port;
	spPort data_port;

	IString name;
    IString invited_name;
	int participant_type;
    int roll;
	int sel_phone;
	IString phone;
	int sel_email;
	IString email;
	IString screen;
	int notify_type;
	IString description;
	int call_options;
    int volume;
    int gain;
	int mute;
	bool remote_control;
	bool sent_im;
	bool handup;
	bool joined_by_voice;	// just to track if the participant has ever joined the meeting via voice
	bool joined_by_data;	// just to track if the participant has ever joined the meeting via data

	int outbound_progress;	// call progress
	int bridge_request;

	// Event log handles
	int voice_event;
	int data_event;

	// Parent submeeting
    spSubMeeting submeeting;

	IMutex ref_lock;
    IMutex participant_lock;

public:
    Participant(const char * participant_id, SubMeeting * submeeting, User * user, int type, int roll, const char * name,
				int sel_phone, const char * phone, int sel_email, const char * email, const char * screen, const char * description,
				int notify_type, int call_options);
    ~Participant();

	void update_info(int role, const char * name, int sel_phone,
                     const char * phone, int sel_email, const char * email,
                     const char * screen, const char * description,
                     int notify_type, int call_options);

    void lock();
    void unlock();

	void release();

	int connect_port(Port * port, bool wait);
	int disconnect_port(Port * port, bool notify_services=true, bool notify_presence=true);
	int disconnect(bool notify_services=true);

    int check_mute(bool * did_change);	// turns muting on/off (as necessary) for lecture or hold mode

	void set_progress(int progress);

	// Moderators or host has moderator privilege.
	// Does not give moderator status to admin or super admins.
	bool is_moderator()
	{
		return roll == RollModerator || roll == RollHost;
	}
	void set_moderator(bool f);

	// See if participant is connected to this meeting.
	bool is_connected();

	// See if user is attached to this participant
	bool is_user_attached(User * user);

	bool is_presenter();

    int get_status();

    SubMeeting * get_submeeting()   { return submeeting; }
	void set_submeeting(SubMeeting * sm);

	spMeeting get_meeting();

    spUser get_user();
	void set_user(User * user);

	spPort get_voice_port()			{ return voice_port; }
	void set_voice_port(Port *p)	{ voice_port = p; }
	spPort get_data_port()			{ return data_port; }
	void set_data_port(Port *p)		{ data_port = p; }
	spPort get_port(int media_type);

	int get_participant_type()		{ return participant_type; }
	void set_participant_type(int type)	{ participant_type = type; }

    const char * get_id()           { return participant_id; }
	void set_id(const char *_id)	{ participant_id = _id; }
    int get_roll()                  { return roll; }
	void set_roll(int _roll)		{ roll = _roll; }
	const char * get_name()			{ return name; }
    const char * get_invited_name() { return invited_name; }
	void set_name(const char *_nm)	{ name = _nm; }
    void set_invited_name(const char *_inm) { invited_name = _inm; }
	int get_sel_phone()				{ return sel_phone; }
	void set_sel_phone(int _p)		{ sel_phone = _p; }
	const char * get_phone()		{ return phone; }
	void set_phone(const char *_p)	{ phone = _p; }
	int get_sel_email()				{ return sel_email; }
	void set_sel_email(int _e)		{ sel_email = _e; }
	const char * get_email()		{ return email; }
	void set_email(const char *_e)	{ email = _e; }
	const char * get_screen()		{ return screen; }
	int get_notify_type()			{ return notify_type; }
	void set_notify_type(int _n)	{ notify_type = _n; }
	int get_call_options()			{ return call_options; }
	void set_call_options(int opt)	{ call_options = opt; }
	const char * get_description()	{ return description; }
	int get_mute()					{ return mute; }
	void set_mute(int _mute)		{ mute = _mute; }
    int get_volume()				{ return volume; }
    void set_volume(int _volume)    { volume = _volume; }
    int get_gain()					{ return gain; }
    void set_gain(int _gain)		{ gain = _gain; }

	bool is_remote_control()		{ return remote_control; }
	void set_remote_control(bool f)	{ remote_control = f; }
	bool is_im_sent()				{ return sent_im;}
	void set_im_sent(bool f)		{sent_im = f;}
	bool is_handup()				{ return handup; }
	void set_handup(bool f)			{ handup = f; }
	int get_bridge_request()        { return bridge_request; }
	void set_bridge_request(int c)  { bridge_request = c; }
	bool voice_joined()				{ return joined_by_voice; }
	bool data_joined()				{ return joined_by_data; }

    enum { TYPE_PARTICIPANT = 3451 };
    virtual int type()
        { return TYPE_PARTICIPANT; }
    virtual unsigned int hash()
        { return participant_id.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && participant_id == ((Participant*)obj)->participant_id; }
};


// Submeeting within meeting.
// Submeeting ids are generated.
class SubMeeting : public IReferencableObject
{
    IString sub_id;
	IString title;

    spMeeting meeting;
    bool main;
    unsigned options;
	int expected_participants;

	int state;

    IList participants;

	IMutex ref_lock;
    IMutex sm_lock;

	DispatchCall * dispatch_call;

public:
	enum SubMeetingState {
		StateNone,
		StateRegister,
		StateActive,
		StateEnding
	};

    SubMeeting(const char *id, Meeting * meeting, int expected_participants, bool main=false, const char * title=NULL);
	~SubMeeting();

    void lock();
    void unlock();

	void release();
	void reset();

    int start(DispatchCall * dispatch);
    int end(bool notify_services=true);

	void on_start_result(RPCResultSet * rs, bool dispatch_event=true);
	void on_start_failed();
	void on_end_result(RPCResultSet * rs);
	void on_reset_result(RPCResultSet * rs);

    void add_participant(Participant *p);
    void remove_participant(Participant *p, bool release=true);

	void on_user_disconnect(Participant *p, Port * port);

    void get_participants(IListIterator & iter);

	spParticipant find_participant(const char * participant_id);

	int reset_recording();
    int set_options(unsigned _options);
    unsigned get_options()  { return options; }

    bool is_main()          { return main; }
	bool is_active()		{ return state == StateActive; }

    Meeting *get_meeting()  { return meeting; }
	void set_meeting(Meeting * m);

    const char *get_id()    { return sub_id; }
	const char *get_title()	{ return title; }

    enum { TYPE_SMEETING = 3453 };
    virtual int type()
        { return TYPE_SMEETING; }
    virtual unsigned int hash()
        { return sub_id.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && sub_id == ((SubMeeting*)obj)->sub_id; }
};


// Extend iterator by locking & unlocking
class ISubMeetingIterator : public IListIterator
{
    Meeting * meeting;

public:
    ISubMeetingIterator();
    ~ISubMeetingIterator();

    void set_list(Meeting * meeting);
};


class Chat : public IReferencableObject
{
public:
    IString chat_id;
    spParticipant from;
    int send_to;
    spParticipant to;
    IString to_name;
    IString message;
    int sent_time;

private:
    IMutex ref_lock;

public:
    Chat(const char* chat_id, spParticipant& from, int send_to, spParticipant& to, const char *message);
    ~Chat();

    void lock();
    void unlock();

	IMutex &get_lock()		{ return ref_lock; }

    enum { TYPE_CHAT = 7931 };
    virtual int type()
        { return TYPE_CHAT; }
    virtual unsigned int hash()
        { return chat_id.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && chat_id == ((Chat*)obj)->chat_id; }
};


#include "chatlogger.h"

// Active meeting.
// Meeting id comes from schedule db.
class Meeting : public IReferencableObject, ITimeable
{
    friend class PlaceCallDispatch;

	IString meeting_id;
	IString title;
	IString description;
    IString invite_message;
	IString community_id;
	IString bridge_phone;
    IString bridge_voip_addr;
	IString system_url;
	IString voice_service;

    int hosts_enabled_features;

	int state;
    int actual_start_time;

    int meeting_type;
    int privacy;
    int priority;
    int start_time;
    int end_time;
    int invite_expiration;
    int invite_valid_before;
    int max_participants;
    int max_duration;
    int recurrence;
	int recur_end;
	IString password;
    int options;
    int display_attendees;
    int invitee_chat_options;

    spUser host;
	spUser presenter;

	IMutex ref_lock;
    IMutex meeting_lock;

    IHash all_participants;

    IList submeetings;
	int submeeting_index;

	// App share session
	spShareServer share_server;
	bool share_active;
	IString share_address;
	IString share_password;
	int share_options;
	int share_start_time;
	bool share_connected;

	// Starting meeting info
	spUser starting_user;
	spPort starting_port;
	IString starting_participant_id;
	IString starting_password;
	bool starting_release;
	bool overide_schedule;
	IList * pending_participants;
    IString logged_start_time;
	spSubMeeting call_sm;	// submeeting for 1-1 call

    // Operations held up by meeting start
    PendingQueue pending_queue;

    // Events for CDRs
	int meeting_event;
	int appshare_event;
    int recording_event;
	int docshare_event;

    static int submeeting_id_seq;
    static int participant_id_seq;

    // timer for testing moderator breakoffs & meeting start timeouts
	static int timer_id_seq;
    CTimer* timer;
    bool timer_set;

    // Document share session
	bool share_document;
    IString doc_share_document_id;
	IString doc_share_page_id;
	IString doc_share_drawing;
	int doc_share_options;
	IString doc_share_address;

	spService mtgarchiver;
	CTimer* mtgarc_timer;

	spDocServer docserver;

    bool hold_notifications;

    IList chats;
    int chat_index;

	static ChatLogger s_clogger;

	IString appshare_key;

public:
	enum MeetingState {
		StateNone = 0,
		StateLoading = 1,
		StateNotify = 2,
		StateHolding = 3,
		StateActive = 4,
		StateEnding = 5,
		StateDead = 6
	};

    Meeting(const char *id, const char * title, int type, int privacy, const char * key);
	Meeting(spUser & _host, const char *id, int _type, int _privacy, int _priority,
			const char *_title, const char * _descrip, int _expected_size, int _expected_duration,
			const char *_password, int _options, int _end_time, int _invite_expiration, int _invite_valid_before,
            const char * _invite_message, int _display_attendees, int _invitee_chat_options, const char * key);

	~Meeting();

    int update_meeting_info(int type, int privacy, int priority,
				const char * title, const char * descrip, int expected_participants, int expected_duration,
				int scheduled_start, int recurrence, int recur_end, int options, const char * password,
                int _scheduled_end, int _invite_expiration, int _invite_valid_before, const char * _invite_message,
                int _display_attendees, int _invitee_chat_options);

    int toggle_lecture_mode(User * user, int * mode_on);
    int toggle_hold_mode(User * user, int * mode_on);
    int toggle_record_meeting(User * user, int * mode_on);
    int toggle_mute_join_leave(User * user, int * mode_on);
    int enable_data_recording(User * user, int  enabled);
    int lock_meeting(User * user, int state);
    int modify_meeting(User * user, int _type, int _privacy, int _priority, const char *_title, const char * _descrip,
                            const char *_password, int _options, int _invite_expiration, int _invite_valid_before,
                            const char * _invite_message, int _display_attendees, int _invitee_chat_options, const char * key);
    int modify_recording(User * user, int _cmd, const char* email);

    void lock();
    void unlock();
	IMutex &get_lock()		{ return meeting_lock; }

	void release(bool release_immediate = false);
	void reset(Service * service);

    int start(User * user, Port * port, const char * participant_id, const char * password, IList * participants, bool create_meeting, bool release);
    int end(User * user);
    int reload(User * user);
	void send_start_notifications();
	int add_participants(User * user, IList * participants, SubMeeting * direct_sm);
	bool is_direct_call(IList * participants);

	int on_load_result(RPCResultSet * rs);
	int on_create_result(RPCResultSet * rs);
	void on_reload_result(RPCResultSet * rs);
	void on_reset_result(RPCResultSet * rs);
	void on_start_failed(int status);
	void on_recording_result(int status);
	void on_sm_add_result(SubMeeting * sm, RPCResultSet * rs);
	void on_sm_remove_result(SubMeeting * sm, RPCResultSet * rs);
	void on_call_failed(spUser & user, spParticipant & participant, int fault_code);
	void on_join_failed(User * user, int status);
	bool check_access(RPCResultSet * rs, User * user);

    // Add/remove participant from meeting
    int add_participant(User * actor, const char * sub_id, User * target, int ptype,
				int roll, const char * name, int sel_phone, const char * phone,
				int sel_email, const char * email, const char * screen, const char * description,
				int notify_type, const char * message, int call_type, int expires);
    int remove_participant(User * actor, const char * participant_id);
    int call_participant(User * actor, const char * participant_id, int sel_call_phone, const char * call_phone, int options);
	int notify_participant(User * actor, const char * participant_id, int notify_type, const char * message);
	int disconnect_participant(User * actor, const char * participant_id, int media_type);

    void get_submeetings(IListIterator & iter);

    // Manage submeetings.
    int add_submeeting(User * actor, int expected_participants, spSubMeeting & meeting, DispatchCall * therest);
    int remove_submeeting(User * actor, const char * sub_id, bool remove_parts);
    int move_participant(User * actor, const char * participant_id, const char * sub_id);

	// App sharing session
	int start_app_share(User * actor, const char * address, const char * password, int share_options, int share_starttime);
	int end_app_share(User * actor);
	int request_share_server(User * actor);
	int change_presenter(User * actor, const char * partid);
	int grant_control(User * actor, const char * partid, int grant);

    void on_share_started();
    void on_share_ended();

	// document sharing
	int start_doc_share(User * actor, const char * address, int share_starttime);
	int end_doc_share(User * actor);
	int download_document_page(User * actor, const char * docid, const char * pageid);
	int configure_whiteboard(User * actor, int options);
	int draw_on_whiteboard(User * actor, const char * drawing);

	int on_ppt_remote_control(User * actor, const char * action);

    // In-Meeting Chat
    int send_chat_message(const char * sessionid,
                          const char * meetingid,
                          int send_to,
                          const char * message,
                          const char * from_participantid,
                          const char * to_participantid);
    void dispatch_chats(const char *participant_id);

    // timer management
    void on_timeout(int timer_id);
    void set_moderator_presence_timeout();

    // Connect user to meeting.
    int join_user(User * user, Port * port, const char * participant_id, const char * password, bool release);
    int leave_user(User * user, Port * port, bool is_disconnect);
    int leave_user_timeout();
	int state_changed(User * user);
	int call_progress(User * user, int progress);

	int set_participant_volume(User * actor, const char * partid, RPCSESSION rpcsess, int volume, int gain, int mute);
	int get_participant_volume(User * actor, const char * partid, RPCSESSION rpcsess);
	int set_handup(User * actor, const char * partid, int & handup);
	int grant_moderator(User * actor, const char * partid, int grant);
	int roll_call(User * actor);
	int place_call(User * actor, User * callee, const char * callee_number);

    // Lecture mode support
    int set_lecture_mute(spSubMeeting main_sm, int state);

    // Hold mode support
    int set_hold_mute(spSubMeeting main_sm, int state);

    int send_bridge_request(User * actor, const char * partid, int code, const char *msg);
    int bridge_request_progress(User * actor, int code, const char *msg);

    bool is_moderator(User * actor);
    bool is_participant(User * actor);
	bool is_presenter(User * actor)	{ return presenter == actor; }

	bool is_share_active()		{ return share_active; }

	bool is_private()			{ return privacy == PrivacyPrivate; }
	bool is_unlisted()			{ return privacy == PrivacyUnlisted; }

    bool is_locked()			{ return (options & LockParticipants) != 0; }
    bool is_recording()         { return (options & RecordingMeeting) != 0; }
    bool is_holding()           { return (options & HoldMode) != 0; }

	int get_state()				{ return state; }
    const char * get_id()       { return meeting_id; }
    const char * get_title()    { return title; }
	const char * get_description() { return description; }
    const char * get_invite_message() { return invite_message; }
    const char * get_password() { return password; }
	const char * get_community_id()	{ return community_id; }
	int get_hosts_enabled_features()	{ return hosts_enabled_features; }
	const char * get_voice_service() { return voice_service; }
    User * get_host()           { return host; }
	User * get_presenter()		{ return presenter; }
    int get_type()              { return meeting_type; }
    int get_privacy()           { return privacy; }
    int get_start()             { return start_time; }
    int get_actual_start()      { return actual_start_time; }
    int get_end()               { return end_time; }
    int get_invite_expiration() { return invite_expiration; }
    int get_invite_valid_before() { return invite_valid_before; }
	int get_expected_participants()	{ return max_participants; }
	int get_priority()			{ return priority; }
	int get_duration()			{ return max_duration; }
	int get_recurrence()		{ return recurrence; }
	int get_options()			{ return options; }
    int get_display_attendees() { return display_attendees; }
    int get_invitee_chat_options() { return invitee_chat_options; }
	spShareServer get_share_server()  { return share_server; }
	const char * get_share_address()  { return share_address; }
	const char * get_share_password() { return share_password; }
	int get_share_options() { return share_options; }
	const char * get_doc_share_address()  { return doc_share_address; }
	const char * get_doc_share_document_id()  { return doc_share_document_id; }
	const char * get_doc_share_page_id()  { return doc_share_page_id; }
	const char * get_doc_share_drawing()  { return doc_share_drawing; }
	int get_doc_share_options() { return doc_share_options; }
	const char * get_doc_share_server();
    const char * get_bridge_address() { return this->bridge_voip_addr; }
    void set_bridge_address(const char *addr) { bridge_voip_addr = addr; }
    const char * get_bridge_phone() { return this->bridge_phone; }
	const char * get_appshare_key()  { return appshare_key; }

    spSubMeeting find_submeeting(const char * id);
    spSubMeeting find_mainmeeting();

	spParticipant find_participant(const char * id);
    spParticipant find_participant_by_userid(const char * id);

	int get_meeting_counts(int & num_connected,
                               int & num_moderators,
                               int & num_invited,
                               int & num_voice,
                               int & num_app_share,
                               int & num_licenses,
                               bool voice_only = false);
    int get_status(int options, int & state, int & m_opt, int & sm_opt);

    enum { TYPE_MEETING = 3452 };
    virtual int type()
        { return TYPE_MEETING; }
    virtual unsigned int hash()
        { return meeting_id.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && meeting_id == ((Meeting*)obj)->meeting_id; }


	int call_participant_internal(User * actor, Participant * p, int sel_call_phone, const char * call_phone, int options);
	int end_internal(User * actor, bool notify_services);
	int add_participant_internal(User * actor, SubMeeting *sm, User * target,
				const char * part_id, int part_type, int roll, const char * name, int sel_phone, const char * phone,
                int sel_email, const char * email, const char * screen, const char * description, int notify_type,
				int call_type, bool dispatch_event, spParticipant & participant);
	int join_user_internal(spUser & user, spPort & port, spParticipant & part, const char * part_id, const char * password, int release, int role);

    int voice_register(const char *rpc_id, SubMeeting *call_sm);
    int voice_close();
    int voice_notify_participant(User * actor, Participant * participant);

private:
    void get_invite_details(IString &msg1, IString &msg2, IString &msg3,
                              const char *aMeetingId,
                              const char *aMeetingTitle,
                              const char *aHostName,
                              const char *aParticipantId,
                              const char *aParticipantName,
                              const char *aMeetingDescription,
                              const char *aInviteMessage,
                              const char *aMeetingPassord,
                              const char *aParticipantPhone,
                              const char *aBridgePhone,
                              const char *aSystemUrl,
                              int aMeetingType,
                              int aScheduledTime,
                              bool aUseHTML,
                              int aMaxMessageSize
                              );
	int email_participant(User * actor, Participant * p);
	int im_participant(User * actor, Participant * p, int notify_type);
	int move_participant_internal(Participant * p, SubMeeting * sm);
	int remove_participant_internal(Participant * p);
	int notify_participant_internal(User * actor, Participant * p, int notify_type, const char * message);
	int add_participants_internal(User * actor, IList * participant_list, SubMeeting * _direct_sm);
    int reassign_participant(spParticipant& participant, spUser& user, spPort& port);
    void release_internal();
	int set_participant_mute(Participant * p, int new_mute);

	void add_index(User * user, Participant * p);
	void remove_index(Participant * p);
	int enable_recording(int enabled);
	void build_summary(IString& string);
};


#endif
