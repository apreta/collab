/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 *
 *
 * All modifications and additions to ICEcore/Kablink sources are Copyright (c) 2009 Michael Tinglof.
 */

#ifdef _WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <libintl.h>
#define _(String) gettext(String)

#include "controllerrpc.h"
#include "../../jcomponent/presence.h"
#include "../../jcomponent/util/log.h"
#include "../../jcomponent/util/port.h"
#include "../../jcomponent/util/rpcresult.h"
#include "controller.h"
#include "meeting.h"
#include "appshare.h"
#include "phonenumber.h"
#include "apidispatch.h"
#include "transport.h"
#include "metrics.h"


char *i_itoa(int i, char *buffer);
bool i_atob(char *buffer);


// Locking and dispatch:
//  - Events are queued to another thread for dispatch to clients
//  - APIs gets appropriate locks for action and returns quickly -- locks are held
//    while events are queued.
//  - If an API can't complete immediately, it will return Pending (operation
//    has been started), or Retry (caller must retry later)
//  - Most objects are ref counted and are deleted when ref count goes to 0
//  - Don't hold controller lock OR user lock when calling meeting API (to avoid deadlock)
//
// IDs:
//  - see notes in controllerrpc.cpp
//  - Add epoch to generated ids (so restarting controller gets new ids)


Controller master;
DispatchQueue dispatch_queue;
TransportPool* transport_pool;

/*
 * Metrics: file and cache for each sample type
 */
Samples meetings_sample;
Samples users_sample;
Samples ports_sample;
Samples shares_sample;

SampleLog meetings_sample_log;
SampleLog users_sample_log;
SampleLog ports_sample_log;
SampleLog shares_sample_log;

//====
// Controller
//====

int Controller::anon_id_seq = 700000;
int Controller::mailer_id_seq = 0;

Controller::Controller()
{
    // Initialize the number of licenses.  Allow a small
    // number of licenses for testing.  Will be set to
    // actual value from config file
    license_max = 5;
    time_license_email = 0;
}

int Controller::register_service(const char * service_id, int epoch)
{
    int status = Ok;

    IUseMutex lock(controller_lock);

	log_debug(ZONE, "Registering service %s", service_id);

    spService service;

    int type_code = get_address_type(service_id);
    if (type_code < 0)
    {
        log_error(ZONE, "Unknown service type: %s\n", service_id);
        status = InvalidServiceID;
        goto exit;
    }

    service = find_service(service_id);
    if (service == NULL)
    {
		service = new Service(service_id, true);
		if (service != NULL)
		{
			services.add(service);
			service->dereference();
		}
	}
	else
		service->set_available(true);

	// If server is freshly started (i.e. not a restart), reset local state.
	if (epoch != service->get_epoch())
	{
        log_error(ZONE, "Service %s started, resetting local references\n", service_id);

       	if (type_code == AddressVoice)
		{
			// Reset users that appear to have calls in progress.
			reset_users(lock, service);
        }
        if (type_code == AddressVoice || type_code == AddressMtgArc)
        {
			reset_meetings(lock, service);
		}

		service->set_epoch(epoch);
    }

exit:
    return status;
}

// Callback to remove all user connections associated with service
// (or NULL to remove all user client connects).
void Controller::reset_users(IUseMutex & lock, spService & service)
{
	IVector user_list;

	users.values_of(user_list);

	lock.unlock();

	IVectorIterator iter(user_list);
	User * user = (User *) iter.get_first();

	while (user != NULL)
	{
		user->reset_user(service);

		user = (User *) iter.get_next();
	}

	lock.lock();
}

// Re-register meetings that were associated with service.
// If no service specified, just end all meetings.
void Controller::reset_meetings(IUseMutex & lock, spService & service)
{
	IVector meeting_list;

	meetings.values_of(meeting_list);

	lock.unlock();

	IVectorIterator iter(meeting_list);
	Meeting * meeting = (Meeting *) iter.get_first();

	while (meeting != NULL)
	{
		meeting->reset(service);

		meeting = (Meeting *) iter.get_next();
	}

	lock.lock();
}

int Controller::unregister_service(const char * service_id)
{
    int status = Ok;

    IUseMutex lock(controller_lock);

    IRefObjectPtr<Service> service;

    service = find_service(service_id);
    if (service == NULL)
    {
        log_error(ZONE, "Service %s is not registered\n", service_id);
        status = InvalidServiceID;
        goto exit;
    }

    if (service != NULL)
    {
		service->set_available(false);
    }

exit:

    return status;
}

// Let other services know that we have restarted.
void Controller::reset_services(int epoch)
{
    IUseMutex lock(controller_lock);

	IListIterator iter(services);
	Service * service = (Service *)iter.get_first();

	while (service != NULL)
	{
		if (service->get_type() == AddressVoice)
		{
			// Voice service will only reset it's state if it has a different stored
			// controller epoch.
			rpc_make_call("reset", service->get_name(), "voice.reset", "(si)", "zQQz98", epoch);
		}
        if (service->get_type() == AddressMtgArc)
        {
            // Same deal about epoch applies here as above
			rpc_make_call("reset", service->get_name(), "mtgarchiver.reset", "(si)", "zQQz98", epoch);
        }

		service = (Service *)iter.get_next();
	}
}

spService Controller::get_archiver()
{
	Service * archiver = NULL;
	int min = 32767;

    IUseMutex lock(controller_lock);

	IListIterator iter(services);
	Service * service = (Service *)iter.get_first();

	while (service != NULL)
	{
        if (service->get_type() == AddressMtgArc)
        {
            if (service->is_available() && service->get_usage_count() < min)
			{
                archiver = service;
                min = service->get_usage_count();
			}
        }

		service = (Service *)iter.get_next();
	}

	if (archiver != NULL)
		archiver->add_usage_count();
	else
        log_error(ZONE, "MtgArchiver service is not available\n");

	return archiver;
}

void Controller::release_archiver(spService & archiver)
{
    IUseMutex lock(controller_lock);

	archiver->remove_usage_count();
}

const char* Controller::get_doc_share_server(const char* meetingid)
{
	spMeeting meeting;

	IUseMutex lock(controller_lock);

    // Lookup meeting id...
    meeting = find_meeting(meetingid);
	if (meeting != NULL)
	{
		if (meeting->get_state() == Meeting::StateActive)
		{
			return meeting->get_doc_share_server();
		}
	}

	spDocServer docserver = docshare.get_docserver();
	if (docserver!=NULL)
		return docserver->get_address();

	return NULL;
}

bool Controller::is_voice_available()
{
    IUseMutex lock(controller_lock);

    bool voice_available = false;
    Service * service = NULL;

    for (int idx=0; idx < services.size() && !voice_available; idx++)
    {
        service = (Service *)services.get(idx);
        voice_available = service->get_type() == AddressVoice && service->is_available();
    }

    return voice_available;
}

bool Controller::get_service_status(int idx, IString * service_id, int * type, int * available)
{
    IUseMutex lock(controller_lock);

    if (idx >= services.size()) return false;

    Service * service = (Service *)services.get(idx);

    *service_id = service->get_service_id();
    *type = service->get_type();
    *available = service->is_available();

    return true;
}

bool Controller::get_appshare_status(int idx, IString * service_id, int * available)
{
    IUseMutex lock(controller_lock);

    return appshare.get_server_status(idx, service_id, available);
}

bool Controller::get_docshare_status(int idx, IString * service_id, int * available)
{
    IUseMutex lock(controller_lock);

    return docshare.get_server_status(idx, service_id, available);
}

void Controller::reset(bool reset_db)
{
	spService none;

    IUseMutex lock(controller_lock);

    if (reset_db)
    {
        // Reset status for any meetings previously in progress
        rpc_make_call("resetstatus", "addressbk", "schedule.reset_meeting_status", "(ss)",
                      "sys:controller",
                      "controller");
    }

	// Close any active meetings.
	reset_meetings(lock, none);

	// Drop any existing user client sessions.
	reset_users(lock, none);
}

// Register client
int Controller::register_client(const char * userid, const char * username, const char * name, const char * port_address, const char *network_address,
							    spUser & user)
{
	char id[MAX_ID_LENGTH];
    int status = Ok;

	user = NULL;

    IUseMutex lock(controller_lock);

	// Create anonymous user id if needed
	if (userid == NULL || *userid == '\0')
	{
		// Create anonymous user id if no id specified
		sprintf(id, "anon:%d", ++anon_id_seq);
		userid = id;
		if (get_address_type(port_address) != AddressIIC || *name == '\0')
			name = _("Anonymous");
        if (username == NULL || *username == '\0')
            username = ANONYMOUS_NAME;
	}
	else if (get_address_type(userid) == -1)
	{
		return InvalidUserID;
	}

    // Find or create session for this user
    user = create_user(userid, username, name);
    if (user != NULL)
    {
		bool was_connected = user->number_ports() > 0;

        // Add port
        status = user->add_port(port_address, network_address);

		if (!was_connected && get_address_type(userid) == AddressUser)
		{
			// New, authenticated user; load his information.
			user->load(NULL);
		}

		// Only index user for client connections
		if (get_address_type(userid) == AddressUser || user->is_client_jid(username))
		{
			IString * key = new IString(username);
			user_addresses.insert(key, user);
		}

		log_debug(ZONE, "Port %s registered for %s", port_address, userid);
    }

    return status;
}

// Unregister client
// is_disconnect indicates client shutdown was not graceful (e.g. network error)
int Controller::unregister_client(const char * sessionid, const char * port_address, bool is_disconnect)
{
	int status = Ok;

    IUseMutex lock(controller_lock);

    // Currently session is simply userid
    IRefObjectPtr<User> user = find_user(sessionid);
    if (user != NULL)
    {
		lock.unlock();

		user->remove_port(port_address, is_disconnect);

		// *** Remove user if last connection was removed
		log_debug(ZONE, "Port %s unregistered from %s", port_address, sessionid);
	}

    return status;
}


// Received presence from jabber sm.  Address is of form user@host/resoure.
void Controller::on_presence(const char * port_address, int avail, const char *status)
{
	char jid[512];
	const char * p;
	unsigned len;

	// If for anonymous user, leave resource on; otherwise remove for search.
	if (strncmp(port_address, ANONYMOUS_NAME, strlen(ANONYMOUS_NAME)) != 0)
	{
		p = strrchr(port_address, '/');
		if (p != NULL)
        {
            // Only interested in presence from clients
            if (strncmp(p, "/iic", 4) != 0 && strncmp(p, "/mtg", 4) != 0 && strncmp(p, "/cht", 4) !=0)
                return;
			len = p - port_address;
        }
		else
			len = strlen(port_address);
	}
	else
		len = strlen(port_address);
	if (len > sizeof(jid) - 1)
		len = sizeof(jid) - 1;

	strncpy(jid, port_address, len);
	jid[len] = '\0';

    //IUseMutex lock(controller_lock);

	if (avail != PresenceAvailable)
	{
        log_debug(ZONE, "Presence lost for jid: %s", jid);

		// User has left.  See if he is a registered client.
		spUser u = find_user_by_address(jid);
		if (u != NULL)
		{
			IString addr = IString("iic:") + port_address;
			unregister_client(u->get_id(), addr, true);
		}
	}
}

// Create meeting based on passed info rather then schedule.
// Returns pending if meeting is being started, ok if already active.
int Controller::create_meeting(const char * sessionid, const char *id, int type, int privacy, int start_time, const char *title,
        const char * descrip, int expected_size, int expected_duration, const char *password, int options,
        int end_time, int invite_expiration, int invite_valid_before, const char * invite_message, int display_attendees,
        int invitee_chat_options, IList * participants, const char * key)
{
	spUser user;
	spMeeting meeting;
	IString * id_str;
    int status = Ok;

	IUseMutex lock(controller_lock);

	user = find_user(sessionid);
    if (user == NULL)
    {
        log_error(ZONE, "Invalid session id %s\n", sessionid);
        status = InvalidSessionID;
        goto exit;
    }

    // Lookup meeting id...
    meeting = find_meeting(id);
	if (meeting == NULL)
	{
		// Create new meeting
		log_debug(ZONE, "Creating meeting %s", id);

		if (!check_id(id))
		{
			status = InvalidMeetingID;
			goto exit;
		}

		meeting = new Meeting(user, id, type, privacy, 0, title, descrip, expected_size,
                              expected_duration, password, options, end_time, invite_expiration,
                              invite_valid_before, invite_message, display_attendees,
                              invitee_chat_options, key);

		// Call update meeting info to set the
		// start and end time for the meeting
		meeting->update_meeting_info(
			type, privacy, 0, title, descrip, expected_size,
			expected_duration, start_time, 0, 0, options, password,
			end_time, invite_expiration, invite_valid_before,
			invite_message, display_attendees, invitee_chat_options);

		id_str = new IString(id);
		meetings.insert(id_str, meeting);

		meeting->dereference();
	}

	lock.unlock();

	status = meeting->start(user, user->get_client(), NULL, password, participants, true, true);
	if (status == Ok)
	{
		// Meeting is running...change options to match what caller wants.
		status = meeting->modify_meeting(user, type, privacy, 0, title, descrip, password, options,
                                     invite_expiration, invite_valid_before, invite_message, display_attendees,
                                     invitee_chat_options, key);
		if (status == Ok)
		{
			// Now add new participants to meeting.
			status = meeting->add_participants(user, participants, NULL);
			if (status != Ok)
			{
				log_error(ZONE, "Unable to add participants to meeting %s", id);
			}

			// And join requesting user...
			status = meeting->join_user(user, user->get_client(), NULL, NULL, true);
		}
	}

exit:

	return status;
}

int Controller::toggle_lecture_mode(const char * sessionid, const char *id, int * mode_on)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

	IUseMutex lock(controller_lock);

	user = find_user(sessionid);

    if (user == NULL)
    {
        log_error(ZONE, "Invalid session id %s\n", sessionid);
        status = InvalidSessionID;
        goto exit;
    }

    // Lookup meeting id...
    meeting = find_meeting(id);

	if (meeting != NULL)
	{
		lock.unlock();

		// Modify the meeting info
		log_debug(ZONE, "Toggling lecture mode for %s", id);
        status = meeting->toggle_lecture_mode(user, mode_on);
    }
	else
	{
        status = InvalidMeetingID;
		log_debug(ZONE, "User attempted to modify meeting %s, but it is not in progress", id);
	}

exit:

	return status;
}

int Controller::toggle_hold_mode(const char * sessionid, const char *id, int * mode_on)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

	IUseMutex lock(controller_lock);

	user = find_user(sessionid);

    if (user == NULL)
    {
        log_error(ZONE, "Invalid session id %s\n", sessionid);
        status = InvalidSessionID;
        goto exit;
    }

    // Lookup meeting id...
    meeting = find_meeting(id);

	if (meeting != NULL)
	{
		lock.unlock();

		// Modify the meeting info
		log_debug(ZONE, "Toggling hold mode for %s", id);
        status = meeting->toggle_hold_mode(user, mode_on);
    }
	else
	{
        status = InvalidMeetingID;
		log_debug(ZONE, "User attempted to modify meeting %s, but it is not in progress", id);
	}

exit:

	return status;
}


int Controller::toggle_record_meeting(const char * sessionid,
                                      const char *id, int * mode_on)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

	IUseMutex lock(controller_lock);

	user = find_user(sessionid);

    if (user == NULL)
    {
        log_error(ZONE, "Invalid session id %s\n", sessionid);
        status = InvalidSessionID;
        goto exit;
    }

    // Lookup meeting id...
    meeting = find_meeting(id);

	if (meeting != NULL)
	{
		lock.unlock();

		// Modify the meeting info
		log_debug(ZONE, "Toggling lecture mode for %s", id);
        status = meeting->toggle_record_meeting(user, mode_on);
    }
	else
	{
        status = InvalidMeetingID;
		log_debug(ZONE, "User attempted to modify meeting %s, but it is not in progress", id);
	}

exit:

	return status;
}

int Controller::toggle_mute_join_leave(const char * sessionid, const char *id, int * mode_on)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

	IUseMutex lock(controller_lock);

	user = find_user(sessionid);

    if (user == NULL)
    {
        log_error(ZONE, "Invalid session id %s\n", sessionid);
        status = InvalidSessionID;
        goto exit;
    }

    // Lookup meeting id...
    meeting = find_meeting(id);

	if (meeting != NULL)
	{
		lock.unlock();

		// Modify the meeting info
		log_debug(ZONE, "Toggling lecture mode for %s", id);
        status = meeting->toggle_mute_join_leave(user, mode_on);
    }
	else
	{
        status = InvalidMeetingID;
		log_debug(ZONE, "User attempted to modify meeting %s, but it is not in progress", id);
	}

exit:

	return status;
}

int Controller::enable_data_recording(const char * sessionid,
                                      const char *id, int enabled)
{
    spUser user;
    spMeeting meeting;
    int status = Ok;

    IUseMutex lock(controller_lock);

    if (user == NULL)
    {
        log_error(ZONE, "Invalid session id %s\n", sessionid);
        status = InvalidSessionID;
        goto exit;
    }

    // Lookup meeting id...
    meeting = find_meeting(id);

    if (meeting != NULL)
    {
        lock.unlock();

        // Modify the meeting info
        log_debug(ZONE, "enable data recording for %s", id);
        status = meeting->enable_data_recording(user, enabled);
    }
    else
    {
        status = InvalidMeetingID;
        log_debug(ZONE, "User attempted to modify meeting %s, but it is not in progress", id);
 }

exit:

    return status;
}

int Controller::lock_meeting(const char * sessionid, const char *id, int state)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

	IUseMutex lock(controller_lock);

	user = find_user(sessionid);

    if (user == NULL)
    {
        log_error(ZONE, "Invalid session id %s\n", sessionid);
        status = InvalidSessionID;
        goto exit;
    }

    // Lookup meeting id...
    meeting = find_meeting(id);
	if (meeting != NULL)
	{
		lock.unlock();

		// Modify the meeting info
		log_debug(ZONE, "locking meeting %s for %s", id, sessionid);
        status = meeting->lock_meeting(user, state);
    }
	else
	{
        status = InvalidMeetingID;
		log_debug(ZONE, "User attempted to modify meeting %s, but it is not in progress", id);
	}

exit:
	return status;
}

// Modify meeting info for an in-progress meeting.
// ok if already active.  InvalidMeetingID if meeting not found or not in progress
int Controller::modify_meeting(const char * sessionid, const char *id, int type, int privacy, int priority,
                               const char *title, const char * descrip, const char *password, int options,
                               int invite_expiration, int invite_valid_before, const char * invite_message, int display_attendees,
                               int invitee_chat_options)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

	IUseMutex lock(controller_lock);

	user = find_user(sessionid);

    if (user == NULL)
    {
        log_error(ZONE, "Invalid session id %s\n", sessionid);
        status = InvalidSessionID;
        goto exit;
    }

    // Lookup meeting id...
    meeting = find_meeting(id);

	if (meeting != NULL)
	{
		lock.unlock();

		// Modify the meeting info
		log_debug(ZONE, "Modifying meeting %s", id);

		status = meeting->modify_meeting(user, type, privacy, priority, title,
                                         descrip, password, options,
                                         invite_expiration,
                                         invite_valid_before, invite_message,
                                         display_attendees,
                                         invitee_chat_options,
                                         NULL);
	}
	else
	{
        status = InvalidMeetingID;
		log_debug(ZONE, "User attempted to modify meeting %s, but it is not in progress", id);
	}

exit:

	return status;
}

// Modify meeting recording.
int Controller::modify_recording(const char * sessionid, const char *id, int cmd, const char* email)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

	IUseMutex lock(controller_lock);

	user = find_user(sessionid);

    if (user == NULL)
    {
        log_error(ZONE, "Invalid session id %s\n", sessionid);
        status = InvalidSessionID;
        goto exit;
    }

    // Lookup meeting id...
    meeting = find_meeting(id);

	if (meeting != NULL)
	{
		lock.unlock();

		// Modify the meeting info
		log_debug(ZONE, "Modifying meeting recording %s", id);

		status = meeting->modify_recording(user, cmd, email);
	}
	else
	{
        status = InvalidMeetingID;
		log_debug(ZONE, "User attempted to modify meeting recording %s, but it is not in progress", id);
	}

exit:

	return status;
}

// Starts meeting according to schedule and joins user.
// Returns pending if meeting was just started, retry if was in process of starting
// already (client must retry operation to join), or ok if already active.
// User is joined to meeting once it is started.
int Controller::start_meeting(const char * sessionid, const char * meeting_id, const char * key)
{
	spUser user;
    spPort port;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        log_error(ZONE, "Invalid session id %s\n", sessionid);
        status = InvalidSessionID;
        goto exit;
    }

    port = user->get_client();
	status = start_and_join(user, port, meeting_id, NULL, NULL, true);

exit:
	return status;
}


// Starts meeting if not running, and joins.  We always want at least one user in meeting.
int Controller::start_and_join(spUser &actor, spPort &port, const char * meeting_id,
	const char * part_id, const char * password, bool release, const char * key)
{
	spMeeting meeting;
    IString * id;
    int status = Ok;

	IUseMutex lock(controller_lock);

	// Create meeting if not found
    meeting = find_meeting(meeting_id);
    if (meeting == NULL)
    {
		log_debug(ZONE, "Creating meeting %s", meeting_id);

		if (!check_id(meeting_id))
		{
			status = InvalidMeetingID;
			goto exit;
		}

        // Create new meeting.
        meeting = new Meeting(meeting_id, "Preparing", TypeInstant, PrivacyPrivate, key);

        id = new IString(meeting_id);
        meetings.insert(id, meeting);

		meeting->dereference();
    }

	lock.unlock();

	// This user may not have privledge to start meeting, but we won't know until
	// we load info for meeting.
	status = meeting->join_user(actor, port, part_id, password, release);

exit:
	if (IS_FAILURE(status))
	{
		log_error(ZONE, "Unable to start meeting %s (%d)", meeting_id, status);
	}

    return status;
}


int Controller::end_meeting(const char * sessionid, const char * meeting_id)
{
	IRefObjectPtr<User> user;
    IRefObjectPtr<Meeting> meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        log_error(ZONE, "Invalid session id %s\n", sessionid);
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meeting_id);
    if (meeting != NULL)
    {
		log_debug(ZONE, "Ending meeting %s", meeting_id);

        status = meeting->end(user);
		if (status == NotAuthorized)
			meeting->leave_user(user, user->get_client(), false);
    }
    else
        status = InvalidMeetingID;

exit:

    return status;
}

// Join meeting, starting first if necessary.
// The owner of the specified session is added to meeting.
int Controller::join_meeting(const char *sessionid, const char *addr, const char *meeting_id, const char *pin, const char *password)
{
	spUser user;
	spMeeting meeting;
	spPort port;
	spParticipant participant;
	IString join_meeting_id;
	IString join_part_id;

    int status = Ok;

	log_debug(ZONE, "User %s joining meeting %s", sessionid, meeting_id);

	IUseMutex lock(controller_lock);

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

	port = user->get_port(addr);
	if (port == NULL)
	{
		status = InvalidAddress;
		goto exit;
	}

	if (pin != NULL && *pin)
	{
		// Find numeric portion
		const char *pin_number = strchr(pin, ':');
		if (pin_number == NULL)
			pin_number = pin;
		else
			pin_number++;

		// See if we can validate locally
		status = find_pin(pin_number, meeting, participant);
		if (status == Ok)
		{
			join_meeting_id = meeting->get_id();
			if (participant != NULL)
				join_part_id = participant->get_id();
		}
		// Otherwise look up in db
		else if (status == InvalidPIN)
		{
			JoinMeetingDispatch * dispatch = new JoinMeetingDispatch(user, port, meeting_id, pin, password);

			char id[32];
			const char * dispatch_id = dispatch_queue.add_dispatch(dispatch);
			sprintf(id, "dispatch:%s", dispatch_id);

			if (rpc_make_call(id, "addressbk", "schedule.find_pin", "(ss)", "sys:controller", pin_number))
			{
				status = Failed;
				goto exit;
			}

			status = Pending;
			goto exit;
		}
		else
			goto exit;
	}
	else
	{
		join_meeting_id = meeting_id;
	}

	lock.unlock();

	// Makes sure meeting is running, then joins user.
	status = start_and_join(user, port, join_meeting_id, join_part_id, password, false);

exit:

    return status;
}


// Notify user we couldn't join his meeting.
void Controller::on_join_failed(spUser &actor, spPort &port, int status, const char *meeting_id)
{
	actor->send_error(port, "join", status, "Unable to join meeting", meeting_id, "");
	log_debug(ZONE, "Failed to join meeting for %s", actor->get_id());
}

// User is leaving meeting.
int Controller::leave_meeting(const char *sessionid, const char *addr, const char *meeting_id)
{
	spUser user;
    spMeeting meeting;
	spPort port;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meeting_id);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	port = user->get_port(addr);
	if (port == NULL)
	{
		status = InvalidAddress;
		goto exit;
	}

	// Disconnect user from meeting now.
	status = meeting->leave_user(user, port, false);

exit:

    return status;
}

int Controller::reload_meeting(const char * sessionid, const char * meeting_id)
{
	IRefObjectPtr<User> user;
    IRefObjectPtr<Meeting> meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        log_error(ZONE, "Invalid session id %s\n", sessionid);
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meeting_id);
    if (meeting != NULL)
    {
		log_debug(ZONE, "Reloading meeting %s", meeting_id);

        status = meeting->reload(user);
    }
    else
        status = InvalidMeetingID;

exit:

    return status;
}

// Add list of participants to meeting.
int Controller::add_participants(const char * session_id, const char * meeting_id, IList * participants)
{
	IRefObjectPtr<User> user;
    IRefObjectPtr<Meeting> meeting;
    int status = Ok;

    user = find_user(session_id);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meeting_id);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	// Add to meeting.
    status = meeting->add_participants(user, participants, NULL);

exit:

    return status;
}

// Add participant to meeting
int Controller::add_participant(const char * session_id, const char * meeting_id, const char * sub_id,
                        const char * user_id, const char * username, int roll, const char * name,
						int sel_phone, const char * phone, int sel_email, const char * email,
						const char * screen, const char * description, int notify_type,
						const char * message, int call_type, int expires)
{
	IRefObjectPtr<User> user;
	IRefObjectPtr<User> target_user;
    IRefObjectPtr<Meeting> meeting;
    int status = Ok;

    user = find_user(session_id);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meeting_id);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	// Create user wrapper for new user--may be known internal user or external user
    target_user = create_user(user_id, username, name);
    if (target_user == NULL)
    {
        status = InvalidUserID;
        goto exit;
    }

	// Add to meeting.
    status = meeting->add_participant(user, sub_id, target_user, ParticipantNormal, roll, name, sel_phone, phone,
                                      sel_email, email, screen, description, notify_type, message, call_type, expires);

exit:

    return status;
}

int Controller::remove_participant(const char * session_id, const char * meeting_id, const char * participant_id)
{
	IRefObjectPtr<User> user;
    IRefObjectPtr<Meeting> meeting;
    int status = Ok;

    user = find_user(session_id);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meeting_id);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	// Remove from meeting.
    status = meeting->remove_participant(user, participant_id);

exit:

    return status;
}

// Initiate call to new participant
int Controller::call_participant(const char * session_id, const char * meeting_id, const char * part_id,
						int sel_call_phone, const char * call_phone, int options)
{
	IRefObjectPtr<User> user;
    IRefObjectPtr<Meeting> meeting;
    int status = Ok;

    user = find_user(session_id);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meeting_id);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	// Add to meeting.
	status = meeting->call_participant(user, part_id, sel_call_phone, call_phone, options);

exit:

    return status;
}

int Controller::notify_participant(const char * session_id, const char * meeting_id, const char * participant_id,
						int notify_type, const char * message)
{
	IRefObjectPtr<User> user;
    IRefObjectPtr<Meeting> meeting;
    int status = Ok;

    user = find_user(session_id);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meeting_id);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	// Perform notification.
    status = meeting->notify_participant(user, participant_id, notify_type, message);

exit:

    return status;
}


// Disconnects, but does not remove participant from meeting
int Controller::disconnect_participant(const char * session_id, const char * meeting_id, const char * participant_id, int media_type)
{
	spUser user;
    spMeeting meeting;
    int status = Ok;

    user = find_user(session_id);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meeting_id);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	// Perform notification.
    status = meeting->disconnect_participant(user, participant_id, media_type);

exit:

    return status;
}


// Report call progress to participants.
int Controller::call_progress(const char * session_id, const char * meeting_id, const char * call_id, int progress)
{
	IRefObjectPtr<User> user;
    IRefObjectPtr<Meeting> meeting;
    int status = Ok;

    user = find_user(session_id);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meeting_id);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	status = meeting->call_progress(user, progress);

exit:

	return status;
}


// Manage hubs.  Currently only multiple voice hubs are supported in a meeting.
int Controller::add_submeeting(const char * sessionid, const char * meetingid, int expected_participants, spSubMeeting & sm)
{
	IRefObjectPtr<User> user;
    IRefObjectPtr<Meeting> meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	// Disconnect user from meeting now.
	status = meeting->add_submeeting(user, expected_participants, sm, NULL);

exit:

    return status;
}

int Controller::remove_submeeting(const char * sessionid, const char * meetingid, const char * sub_id, bool remove_parts)
{
	IRefObjectPtr<User> user;
    IRefObjectPtr<Meeting> meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	// Disconnect user from meeting now.
	status = meeting->remove_submeeting(user, sub_id, remove_parts);

exit:

    return status;
}

int Controller::move_participant(const char * sessionid, const char * meetingid, const char * participant_id, const char * sub_id)
{
	IRefObjectPtr<User> user;
    IRefObjectPtr<Meeting> meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	// Move user to submeeting
	status = meeting->move_participant(user, participant_id, sub_id);

exit:

    return status;
}

int Controller::check_participant(const char * sessionid, const char * meetingid, const char * participant_id, spParticipant & p, bool & moderator, bool & self)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	p = meeting->find_participant(participant_id);
	if (p == NULL)
	{
		status = NotMember;
		goto exit;
	}

	moderator = meeting->is_moderator(user);
	self = (user == p->get_user());

exit:

    return status;
}

// See if we can find pin associated with meeting or participant.
// If not, we also need to check database for scheduled meetings.
int Controller::find_pin(const char * pin, spMeeting & meeting, spParticipant & participant)
{
	int status = Ok;

	IString id;

	if (!check_id(pin))
		return InvalidPIN;

	make_id_string(id, "part:", pin);

	participant = find_participant(id);
	if (participant == NULL)
	{
		// Meeting ids don't have a type prefix...should fix someday
		// sprintf(id, "meeting:%s", pin);

		meeting = find_meeting(pin);
		if (meeting == NULL)
			status = InvalidPIN;
	}
	else
	{
		meeting = participant->get_meeting();
	}

	return status;
}

int Controller::start_app_share(const char * sessionid, const char * meetingid,
								const char * address, const char * password,
								int share_options, int share_starttime)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	status = meeting->start_app_share(user, address, password, share_options, share_starttime);

exit:

	return status;
}

int Controller::end_app_share(const char * sessionid, const char * meetingid)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	status = meeting->end_app_share(user);

exit:

	return status;
}

int Controller::request_share_server(const char * sessionid, const char * meetingid)
{
    spUser user;
    spMeeting meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

    status = meeting->request_share_server(user);

exit:

	return status;
}

int Controller::start_doc_share(const char * sessionid, const char * meetingid,
								const char * address, int share_starttime)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	status = meeting->start_doc_share(user, address, share_starttime);

exit:

	return status;
}

int Controller::end_doc_share(const char * sessionid, const char * meetingid)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	status = meeting->end_doc_share(user);

exit:

	return status;
}

int Controller::download_document_page(const char * sessionid, const char * meetingid,
									   const char * docid, const char * pageid)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	status = meeting->download_document_page(user, docid, pageid);

exit:

	return status;
}

int Controller::configure_whiteboard(const char * sessionid, const char * meetingid, int options)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	status = meeting->configure_whiteboard(user, options);

exit:

	return status;
}

int Controller::draw_on_whiteboard(const char * sessionid, const char * meetingid, const char * drawing)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	status = meeting->draw_on_whiteboard(user, drawing);

exit:

	return status;
}

int Controller::on_ppt_remote_control(const char * sessionid, const char * meetingid, const char * action)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	status = meeting->on_ppt_remote_control(user, action);

exit:

	return status;
}

int Controller::change_presenter(const char * sessionid, const char * meetingid, const char * partid)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	status = meeting->change_presenter(user, partid);

exit:

	return status;
}

int Controller::grant_control(const char * sessionid, const char * meetingid, const char * partid, int grant)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	status = meeting->grant_control(user, partid, grant);

exit:

	return status;
}

int Controller::set_handup(const char * sessionid, const char * meetingid, const char * partid, int & handup)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	status = meeting->set_handup(user, partid, handup);

exit:

	return status;
}

int Controller::set_participant_volume(const char * sessionid, const char * meetingid, const char * partid, RPCSESSION rpcsess,
                                       int volume, int gain, int mute)
{
    int status;

    spUser user;
    spMeeting meeting;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	status = meeting->set_participant_volume(user, partid, rpcsess, volume, gain, mute);

  exit:
    return status;
}

int Controller::get_participant_volume(const char * sessionid, const char * meetingid, const char * partid, RPCSESSION rpcsess)
{
    int status;

    spUser user;
    spMeeting meeting;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	status = meeting->get_participant_volume(user, partid, rpcsess);

  exit:
    return status;
}

int Controller::grant_moderator(const char * sessionid, const char * meetingid, const char * partid, int grant)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	status = meeting->grant_moderator(user, partid, grant);

exit:

	return status;
}

int Controller::place_call(const char * sessionid, const char * meetingid, const char * calleeid, const char * phone_number)
{
	spUser user;
	spMeeting meeting;
    spUser callee;
    int status = Ok;
    const char *name = _("Anonymous");	// Display name
    const char *user_name = ANONYMOUS_NAME;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

    callee = create_user(calleeid, user_name, name);
    if (callee == NULL)
    {
        status = InvalidUserID;

        goto exit;
    }

	status = meeting->place_call(user, callee, phone_number);

exit:
	return status;
}

int Controller::roll_call(const char * sessionid, const char * meetingid)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	status = meeting->roll_call(user);

exit:

	return status;
}

void Controller::add_mtg_archiver(const char * name)
{
  IString service_id;
  spService service;

  service_id = IString("mtgarc:") + IString(name);

  service = find_service(service_id);
  if (service == NULL)
    {
      service = new Service(service_id, false);
      if (service != NULL)
	{
	  services.add(service);
	  service->dereference();
	}
    }
}

void Controller::add_bridge(const char * name, const char * voip_address)
{
	IString service_id;
	spService service;

	service_id = IString("voice:") + IString(name);

    service = find_service(service_id);
    if (service == NULL)
    {
		service = new Service(service_id, false);
		if (service != NULL)
		{
            service->set_ext_address(voip_address);
			services.add(service);
			service->dereference();
		}
	}
}

void Controller::add_bridge_phone(const char * phone, const char * service)
{
	char converted[MAX_PHONE_LEN];
	PhoneNumber number(phone, !multi_phone);

	// Normalize phone number
	number.get_converted_phone(converted);

	IString * name_str = new IString(service);
	IString * phone_str = new IString(converted);

	// Add to phone-bridge map
    IUseMutex lock(controller_lock);
	voice_bridge_map.insert(phone_str, name_str);
}

int Controller::find_bridge_by_phone(const char * phone, IString & service)
{
	char converted[MAX_PHONE_LEN];
	PhoneNumber number(phone, !multi_phone);

	// Normalize phone number
	number.get_converted_phone(converted);
	IString temp(converted);

	// Find in phone-bridge map
    IUseMutex lock(controller_lock);
	IString * found = (IString *)voice_bridge_map.get(&temp);
	if (found != NULL)
	{
		service = *found;
		return Ok;
	}
	else
	{
		service.clear();
		return PhoneNumberNotFound;
	}
}

int Controller::send_bridge_request(const char * sessionid, const char * meetingid, const char * partid, int code, const char * msg)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

    user = find_user(sessionid);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meetingid);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	status = meeting->send_bridge_request(user, partid, code, msg);

exit:

	return status;
}

int Controller::bridge_request_progress(const char * session_id, const char * meeting_id, const char * call_id, int code, const char * msg)
{
	IRefObjectPtr<User> user;
    IRefObjectPtr<Meeting> meeting;
    int status = Ok;

    user = find_user(session_id);
    if (user == NULL)
    {
        status = InvalidSessionID;
        goto exit;
    }

    meeting = find_meeting(meeting_id);
    if (meeting == NULL)
    {
        status = InvalidMeetingID;
        goto exit;
    }

	status = meeting->bridge_request_progress(user, code, msg);

exit:

	return status;
}

int Controller::send_chat_message(const char * sessionid,
                                  const char * meetingid,
                                  int send_to,
                                  const char * message,
                                  const char * from_participantid,
                                  const char * to_participantid)
{
	spUser user;
	spMeeting meeting;
    int status = Ok;

	IUseMutex lock(controller_lock);

	user = find_user(sessionid);

    if (user == NULL)
    {
        log_error(ZONE, "Invalid session id %s\n", sessionid);
        status = InvalidSessionID;
        goto exit;
    }

    // Lookup meeting id...
    meeting = find_meeting(meetingid);

	if (meeting != NULL)
	{
		lock.unlock();

		// Modify the meeting info
		log_debug(ZONE, "Sending chat message for meeting %s", meetingid);

		status = meeting->send_chat_message(
                        sessionid, meetingid, send_to,
                        message, from_participantid, to_participantid);
	}
	else
	{
        status = InvalidMeetingID;
		log_debug(ZONE, "Chat message for meeting that is not in progress: %s", meetingid);
	}

exit:

	return status;
}

int Controller::send_im(const char * sessionid, const char * fromuser, const char * touser, const char * message)
{
	// Find user session associated with the user name so we can
	// validate that they are online via client and get the full jabber id.
	spUser user = find_user_by_address(touser);
	if (user == NULL)
	{
		log_debug(ZONE, "User address %s not found", touser);
		return NotConnected;
	}

	spPort port = user->get_client();
	if (port == NULL || !port->is_connected())
	{
		log_debug(ZONE, "Data port for %s not found", touser);
		return NotConnected;
	}

	Address addr;
	const char *addr_str;

	addr_str = port->get_id();
	if (addr_str == NULL || strlen(addr_str) == 0)
		return NotConnected;	// Not connected from client

	if (parse_address(addr_str, &addr) != Ok)
	{
		log_error(ZONE, "Unable to parse address: %s\n", addr_str);
		return Failed;
	}

	IString message_str(message);

	rpc_send_im("im", fromuser, addr.id, message_str.get_escaped_string());
	return Ok;
}

void Controller::register_participant(Participant * p)
{
    IUseMutex lock(controller_lock);

	participants.insert(new IString(p->get_id()), p);
}

// In most cases we are not unregistering the PIN...in the case a participant
// is removed, we need to be able to look up all PINs.
void Controller::unregister_participant(Participant * p)
{
    IUseMutex lock(controller_lock);

	IString str(p->get_id());
	participants.remove(&str, true);
}

void Controller::register_submeeting(SubMeeting * sm)
{
    IUseMutex lock(controller_lock);

	submeetings.insert(new IString(sm->get_id()), sm);
}

void Controller::unregister_submeeting(SubMeeting * sm)
{
    IUseMutex lock(controller_lock);

	IString str(sm->get_id());
	submeetings.remove(&str, true);
}

void Controller::unregister_meeting(Meeting * m)
{
	IUseMutex lock(controller_lock);

	IString str(m->get_id());
	meetings.remove(&str, true);
}

int Controller::get_meetings(IVector * vec)
{
    IUseMutex lock(controller_lock);
    meetings.values_of(*vec);
    return Ok;
}

int Controller::get_users(IVector * vec)
{
    IUseMutex lock(controller_lock);
    users.values_of(*vec);
    return Ok;
}

spUser Controller::find_user(const char * sessionid)
{
    IUseMutex lock(controller_lock);
    IString str(sessionid);
    User * user = (User*)users.get(&str);

    return spUser(user);
}

int Controller::get_active_meetings(const char * sessionid, IVector * result)
{
	IUseMutex lock(controller_lock);

    // TODO: At some point, sessionid is going to be real, so deal w/ the
    // checking then.

    meetings.values_of(*result);

    return Ok;
}

int Controller::get_meeting_status(const char * sessionid, const char * meeting_id, int options, int &state, int &m_opt, int &sm_opt)
{
	int status = Ok;
	spMeeting meeting;

	IUseMutex lock(controller_lock);
    meeting = find_meeting(meeting_id);
	if (meeting != NULL)
	{
		lock.unlock();
        return meeting->get_status(options, state, m_opt, sm_opt);
    }
	else
	{
	    state = m_opt = sm_opt = 0;
        status = InvalidMeetingID;
	}

    return status;
}

int Controller::is_invite_valid(bool is_anonymous, const char * meeting_id, int options,
                                int scheduled_time, int invite_valid_before,
                                int invite_expiration, int type, int invited_time)
{
	int status = Ok;
	spMeeting meeting;
    int current_time = (int)time(NULL);

	IUseMutex lock(controller_lock);
    meeting = find_meeting(meeting_id);

    // If meeting is in progress, use its options...otherwise use the scheduled options that were passed in.
    int meeting_options = (meeting == NULL) ? options : meeting->get_options();

    // See if we need to check to see if this invite has expired...
    bool is_instant_invitee = type == ParticipantInvitee;
    bool is_timed_expiration = invite_expiration != -1;
    bool is_current_invitee = (meeting != NULL) && (meeting->get_actual_start() == invited_time);

    if (!is_anonymous &&
        is_instant_invitee &&
        !is_current_invitee)
    {
        // Check to see if an instant invite has expired...
        int expiration_time = invited_time + invite_expiration;
        bool is_time_expired = current_time > expiration_time;

        if (!is_timed_expiration || is_time_expired)
        {
            // Instant invitation has expired.
            status = PINExpired;
        }
    }

    // If no errors yet and meeting not started, see if the invitee is too soon...
    if (status == Ok &&
        meeting == NULL &&
        ((meeting_options & InviteAlwaysValidBeforeStart) == 0) &&
        (current_time < (scheduled_time - invite_valid_before)))
    {
        // Pin hasn't expired, but too soon to join the meeting
        status = PINNotValidYet;
    }

    return status;
}

spUser Controller::create_user(const char * userid, const char * username, const char * name)
{
    User * user = NULL;

    IUseMutex lock(controller_lock);

    // Find existing session for this user, if any.
    user = find_user(userid);
    if (user == NULL)
    {
        // Create new session
        user = new User(userid, username, name);

        IString * userid_str = new IString(userid);
        users.insert(userid_str, user);

		user->dereference();
    }
	else
	{
		// User exists, but may not have a username associated already.
		if (username != NULL && *username != '\0' &&
			(*user->get_username() == '\0' || !user->is_user()))
		{
			user->set_username(username);
		}

		if (name != NULL && *name != '\0')
		{
			user->set_name(name);
		}
	}

    return spUser(user);
}

spService Controller::find_service(const char * service_id)
{
	IString temp(service_id);

    IUseMutex lock(controller_lock);

    IListIterator iter(services);
    Service * service = (Service*)iter.get_first();
    while (service != NULL)
    {
        if (temp == service->get_service_id())
        {
            break;
        }

        service = (Service*)iter.get_next();
    }

    return spService(service);
}

spMeeting Controller::find_meeting(const char * meetingid)
{
    IUseMutex lock(controller_lock);

    IString str(meetingid);
    Meeting * m = (Meeting*)meetings.get(&str);

    return spMeeting(m);  // Locks meeting, may cause problem
}

spParticipant Controller::find_participant(const char * participantid)
{
	IUseMutex lock(controller_lock);

	IString str(participantid);
	Participant * p = (Participant*)participants.get(&str);

	return spParticipant(p);
}

spSubMeeting Controller::find_submeeting(const char * subid)
{
	IUseMutex lock(controller_lock);

	IString str(subid);
	SubMeeting * sm = (SubMeeting*)submeetings.get(&str);

	return spSubMeeting(sm);
}

spUser Controller::find_user_by_address(const char * address)
{
	IUseMutex lock(controller_lock);

	IString str(address);
	User * u = (User*)user_addresses.get(&str);

	return spUser(u);
}

int Controller::find_client_address(const char * userid, IString & addr_str)
{
	IUseMutex lock(controller_lock);

	addr_str.clear();

	spUser user = find_user(userid);
	if (user == NULL)
		return NotConnected;

	spPort port = user->get_client();
	if (port == NULL || !port->is_connected())
		return NotConnected;

	addr_str = port->get_id();
	return Ok;
}

int Controller::find_participant_address(const char * pid, int media_type, IString & addr_str)
{
	IUseMutex lock(controller_lock);

	addr_str.clear();

	spParticipant p = find_participant(pid);
	if (p == NULL)
		return InvalidParticipantID;

	spPort port = p->get_port(media_type);
	if (port == NULL)
		return NotConnected;

	const char * addr = port->get_id();
	if (addr == NULL)
		return NotConnected;

	addr_str = addr;
	return Ok;
}

bool Controller::check_id(const char * id)
{
	if (strlen(id) > PROFILE_PINLENGTH_MAX)
		return false;

	for (unsigned i = 0; i < strlen(id); i++)
	{
		if (id[i] < '0' || id[i] > '9')
			return false;
	}

	return true;
}

void Controller::initialize()
{
    meetings_sample_log.initialize(SampleTypeMeetings);
    ports_sample_log.initialize(SampleTypePorts);
    users_sample_log.initialize(SampleTypeUsers);
    shares_sample_log.initialize(SampleTypeShareSessions);

    meetings_sample.set_logger(&meetings_sample_log);
    ports_sample.set_logger(&ports_sample_log);
    users_sample.set_logger(&users_sample_log);
    shares_sample.set_logger(&shares_sample_log);
}

int Controller::set_location(const char *directory)	/* base directory for metrics files */
{
    int result = 0;

    if (meetings_sample_log.set_location(directory, "meetings") != 0)
        result = -1;
    if (ports_sample_log.set_location(directory, "ports") != 0)
        result = -1;
    if (users_sample_log.set_location(directory, "users") != 0)
        result = -1;
    if (shares_sample_log.set_location(directory, "share") != 0)
        result = -1;

    return result;
}

void Controller::set_rotation_time(int minutes_since_midnight /* GMT */)
{
    meetings_sample_log.set_rotation_time(minutes_since_midnight);
    ports_sample_log.set_rotation_time(minutes_since_midnight);
    users_sample_log.set_rotation_time(minutes_since_midnight);
    shares_sample_log.set_rotation_time(minutes_since_midnight);
}

void Controller::set_rotation_interval(int days)
{
    meetings_sample_log.set_rotation_interval(days);
    ports_sample_log.set_rotation_interval(days);
    users_sample_log.set_rotation_interval(days);
    shares_sample_log.set_rotation_interval(days);
}

void Controller::started(bool started)
{
    meetings_sample.started(started);
    ports_sample.started(started);
    users_sample.started(started);
    shares_sample.started(started);
}

int Controller::log_license(int num_licenses)
{
    // Open the license log file
    FILE *fp = fopen("/var/iic/.eula", "a");
    if (fp == NULL)
    {
        log_error(ZONE, "Unable to open eula file: %s", strerror(errno));
        return -1;
    }

    // Get the current time string
    time_t now = time(NULL);
    char timestamp[21];
    char logentry[512];
#ifdef LINUX
    struct tm timebuf;
    struct tm *tm_now = localtime_r(&now, &timebuf);
#else
    struct tm *tm_now = localtime(&now);
#endif
    strftime(timestamp, 20, "%Y-%m-%d-%H-%M-%S", tm_now);
    sprintf(logentry, "\"%s\",\"%s\",\"%d\"\n", license_company.get_string(), timestamp, num_licenses);

    // Log the license information to file
    if (fprintf(fp, logentry) < 0)
    {
        log_error(ZONE, "Unable to write to eula file: %s", strerror(errno));
        fclose(fp);
        return -1;
    }

    fclose(fp);

    // Send notification email if appropriate

    // <= 1 because the license key may use a single
    // space character to indicate "no email"
    if (license_alert_email.length() <= 1)
    {
        // No notification email specified.  This will be the case if
        // user's license agreement doesn't allow us to "phone home"
        log_debug(ZONE, "Max exceeded.  No notification configured");
    }
    else if (time_license_email != 0 && ((time_license_email + SECS_PER_DAY) > now))
    {
        // time_license_email indicates that we sent an email within the last 24
        // hours, so don't send another one right now.  Only send once a day.
        log_debug(ZONE, "Max exceeded.  Not yet time for email.");
    }
    else if (rpc_make_call("notify", "mailer", "mailer.send_message",  "(sssss)",
                            license_alert_email.get_string(),
                            license_alert_email.get_string(),
                            license_alert_email.get_string(),
                            "Zon Server Notification",
	                    logentry))
    {
        log_error(ZONE, "Unable to send notification email.  Error sending to mailer.");
    }
    else
    {
        // Email has been successfully sent to the mailer.
        // Update our "last email time"...
        time_license_email = now;
    }

    return 0;
}

// store samples for meetings, ports, users, and share sessions
void Controller::sample(int current_time)
{
    IVectorIterator iter;

    //
    // meeting simply records total
    //
    meetings_sample.record(meetings.size(), current_time);

    //
    // get number of share sessions
    //

    // retrieve meetings
    IVector mtg_vec;
    get_meetings(&mtg_vec);
    iter.set_vector(mtg_vec);

    // count up users in-meeting and on-line
    int share_total = 0;
    int license_total = 0;
    Meeting * mtg;
    for (mtg = (Meeting *)iter.get_first(); mtg; mtg = (Meeting *)iter.get_next())
    {
        int num_connected, num_moderators, num_invited, num_calls, num_app_share, num_licenses;
        mtg->get_meeting_counts(num_connected,
		                          num_moderators,
		                          num_invited,
		                          num_calls,
		                          num_app_share,
		                          num_licenses,
		                          false /* connected both voice&data */);
        share_total += num_app_share;
        license_total += num_licenses;
    }
    shares_sample.record(share_total, current_time);

    if (license_total > license_max)
    {
       log_license(license_total);
    }

    //
    // get users on phone and on-line
    //

    // retrieve users
    IVector user_vec;
    get_users(&user_vec);
    iter.set_vector(user_vec);

    // count up users on phone and online
    int total_online = 0;
    int total_onphone = 0;
    User* user;
    for (user = (User *)iter.get_first(); user; user = (User *)iter.get_next())
    {
        if (user->is_connected(MediaData))
            total_online++;
        if (user->is_connected(MediaVoice))
            total_onphone++;
    }

    users_sample.record(total_online, current_time);
    ports_sample.record(total_onphone, current_time);
}

// rotate latest samples to sample file
void Controller::rotate()
{
    meetings_sample.rotate();
    shares_sample.rotate();
    users_sample.rotate();
    ports_sample.rotate();
}

// retrieves iterator on specified list of samples
int Controller::get_samples(int type, IListIterator & iter)
{
    int found = Failed;
    if (type == SampleTypeMeetings)
    {
        found = Ok;
        meetings_sample.get_samples(iter);
    }
    else if (type == SampleTypeUsers)
    {
        found = Ok;
        users_sample.get_samples(iter);
    }
    else if (type == SampleTypePorts)
    {
        found = Ok;
        ports_sample.get_samples(iter);
    }
    else if (type == SampleTypeShareSessions)
    {
        found = Ok;
        shares_sample.get_samples(iter);
    }
    return found;
}


//====
// Service
//====

Service::Service(const char * _service_id, bool _available)
    : service_id(_service_id), available(_available)
{
	parse_address(service_id, &address);
	usage_count = 0;
}

void Service::lock()
{
    ref_lock.lock();
}

void Service::unlock()
{
    ref_lock.unlock();
}

int Service::get_type()
{
	return address.type;
}

const char * Service::get_name()
{
	return address.id;
}

int Service::add_usage_count()
{
	IUseMutex l(ref_lock);

	usage_count++;

	return usage_count;
}

int Service::remove_usage_count()
{
	IUseMutex l(ref_lock);

	usage_count--;

	return usage_count;
}

//====
// User
//====

User::User(const char * _user_id, const char * _username, const char * _name)
    : user_id(_user_id), name(_name), username(_username)
{
	session = NULL;
	dispatch_call = NULL;
	admin = false;
	superadmin = false;
    enabledfeatures = 0;
    last_ping_time = time(NULL);
    reverse_ping_time = 0;
}

// Use separate lock for reference counts
void User::lock()
{
    ref_lock.lock();
}

void User::unlock()
{
    ref_lock.unlock();
}

void User::load(DispatchCall * call)
{
	IString id;

	// Someone is depending on this data...remember who.
    IUseMutex lock(user_lock);
	if (dispatch_call != NULL)
	{
		log_debug(ZONE, "** Queued call abandoned: %s", dispatch_call->get_name());
		delete dispatch_call;
	}
	dispatch_call = call;
	lock.unlock();

	// Start loading info from schedule db
	// SESSIONID, USERID, FIELDS (array)

	Address addr;

	if (parse_address(get_id(), &addr))
	{
		log_debug(ZONE, "Invalid user id, user info not loaded: %s", get_id());
		on_load_failed();
		return;
	}

	// Only info for type "user" stored in contact db
	if (addr.type != AddressUser)
	{
		on_load_failed();
		return;
	}

	make_id_string(id, "lduser:", get_id());
	if (rpc_make_call(id, "addressbk", "addressbk.find_user", "(ss)", addr.id, addr.id))
	{
		on_load_failed();
		log_error(ZONE, "Error fetching user info for %s\n", get_id());
	}
}

void User::on_load_result(RPCResultSet * rs)
{
    IUseMutex lock(user_lock);

	char *communityid, *screenname, *is_admin, *is_superadmin, *username;
	char *first, *last;
	char *busphone, *homephone, *mobilephone, *otherphone, *ext;
	char *email1, *email2, *email3;
	int defphone, defemail, enabledfeatures;

	//   COMMUNITYID, SCREENNAME, IS_ADMIN (T|F), IS_SUPERADMIN (T|F),
	//   USERNAME, FIRSTNAME, LASTNAME, DEFPHONE, BUSPHONE, HOMEPHONE,
	//	 MOBILEPHONE, OTHERPHONE, EXTENSION, DEFEMAIL, EMAIL, EMAIL2, EMAIL3
    //   ENABLEDFEATURES
	if (!rs->get_output_values("(sssssssisssssisssi)", &communityid, &screenname, &is_admin,
				&is_superadmin, &username, &first, &last, &defphone, &busphone, &homephone,
				&mobilephone, &otherphone, &ext, &defemail, &email1, &email2, &email3,
                &enabledfeatures))
	{
		log_error(ZONE, "Error parsing result from fetch user info\n");
		on_load_failed();
		return;
	}

	set_community_id(communityid);
	set_admin(strcasecmp(is_admin, "T") == 0);
	set_superadmin(strcasecmp(is_superadmin, "T") == 0);
	set_username(username);
	set_phone(SelPhoneBus, busphone, true);
	set_phone(SelPhoneHome, homephone, true);
	set_phone(SelPhoneMobile, mobilephone, true);
	set_phone(SelPhoneOther, otherphone, true);
//	set_phone(SelPhoneExtension, ext, true);  Ext never used
	set_email(SelEmail1, email1);
	set_email(SelEmail2, email2);
	set_email(SelEmail3, email3);
	set_default_phone(defphone);
	set_default_email(defemail);
    set_enabledfeatures(enabledfeatures);

	DispatchCall * call = dispatch_call;
	dispatch_call = NULL;

	lock.unlock();

	log_debug(ZONE, "Info for user %s loaded", get_id());

	if (call)
	{
		// We were waiting for user info to be loaded before dispatching this call
		call->execute(NULL);
		delete call;
	}
}

void User::on_load_failed()
{
    IUseMutex lock(user_lock);

	DispatchCall * call = dispatch_call;
	dispatch_call = NULL;

	lock.unlock();

	// If there is a pending call, clean up now.
	if (call != NULL)
	{
		call->failed();
		delete call;
		call = NULL;
	}
}

void User::reset_user(Service * service)
{
	Address service_addr;

	IUseMutex lock(user_lock);

	if (service == NULL)
	{
		Port * p = this->get_client();
		if (p != NULL)
		{
			lock.unlock();

			log_debug(ZONE, "Remove client port for user %s", p->get_id());
			remove_port(p->get_id());
		}
		return;
	}

	if (parse_address(service->get_service_id(), &service_addr) != Ok)
	{
		log_debug(ZONE, "Invalid service address, can't reset user ports: %s", service->get_service_id());
		return;
	}

	// Copy list so we can unlock this user (and avoid deadlocks)
	IList check_ports;
	ports.copy(check_ports);

	lock.unlock();

	IListIterator iter(check_ports);

	Port * p = (Port *)iter.get_first();
	while (p != NULL)
	{
		if (p->get_port_type() == MediaVoice && p->is_connected())
		{
			Address port_addr;

			if (parse_address(p->get_id(), &port_addr) == Ok)
			{
				if (strcmp(service_addr.id, port_addr.service) == 0)
						remove_port(p->get_id());
			}
		}

		p = (Port *)iter.get_next();
	}
}

int User::connect_port(Participant * part, Port * port, bool waiting)
{
	IString id;

    IUseMutex lock(user_lock);

	if (!port->is_connected())
	{
		log_debug(ZONE, "Port %s already disconnected, not added to meeting", port->get_id());
		return NotConnected;
	}

	spMeeting meeting = part->get_meeting();
	spSubMeeting sm = part->get_submeeting();

	port->set_participant(part);

	// First connect voice
	switch (port->get_port_type())
	{
		case MediaVoice:
			// CALLID, MEETINGID, SUBMEETINGID
			make_id_string(id, "connect:", get_id());
			if (rpc_make_call(id,
							  meeting->get_voice_service(), "voice.connect_call",
							  "(sssbbi)",
							  port->get_id(), meeting->get_id(), sm->get_id(),
							  waiting, meeting->is_moderator(this), part->get_mute()))
			{
				log_error(ZONE, "Error connecting user to voice conference %s\n", get_id());
				return Failed;
			}
			break;

		case MediaData:
			// !!! Connect client
			break;
	}

	send_presence();

	log_debug(ZONE, "Connected %s to meeting %s", port->get_id(), meeting->get_id());

	return Ok;
}

int User::disconnect_port(Participant * part, Port * port, bool notify_service, bool notify_presence)
{
	IString id;

    IUseMutex lock(user_lock);

	spMeeting meeting = part->get_meeting();
	spSubMeeting sm = part->get_submeeting();

	port->set_participant(NULL);

	if (notify_service && port->is_connected())
	{
		switch (port->get_port_type())
		{
			case MediaVoice:
            {
				port->set_connected(false);

				//   CALLID, MEETINGID, SUBMEETINGID
				make_id_string(id, "disc:", get_id());
                const char *sub_id = (sm == NULL ? sm->get_id() : "");
				if (rpc_make_call(id,
								  meeting->get_voice_service(), "voice.disconnect_call", "(sss)",
								  port->get_id(), meeting->get_id(), sub_id))
				{
					log_error(ZONE, "Error disconnecting user from voice conference %s\n", get_id());

					return Failed;
				}
            }
				break;

			case MediaData:
				// !!! Disconnect client
				break;
		}
	}

	if (notify_presence)
		send_presence();

	log_debug(ZONE, "Disconnected %s from submeeting %s", port->get_id(), sm->get_id());

	return Ok;
}

int User::move_connections(Meeting * meeting, Participant * part, SubMeeting * curr_sm, SubMeeting * new_sm)
{
	IString id;

    IUseMutex lock(user_lock);

	spPort port;

	// Move voice connection.
	port = part->get_voice_port();
	if (port != NULL && port->is_connected())
	{
		//   CALLID, MEETINGID, SUBMEETINGID, NEWSUBMEETINGID
		make_id_string(id, "move:", get_id());
		if (rpc_make_call(id, meeting->get_voice_service(), "voice.move_call", "(ssss)",
						  port->get_id(), meeting->get_id(), curr_sm->get_id(), new_sm->get_id()))
		{
			log_error(ZONE, "Error connecting user to voice conference %s\n", get_id());
			return Failed;
		}
	}

	log_debug(ZONE, "Moved %s to submeeting %s", get_id(), new_sm->get_id());

	return Ok;
}

void User::send_presence()
{
	// Create session on SM to track user presence
	if (is_connected(MediaVoice) && is_user())
	{
		if (session == NULL)
			session = create_session(get_username(), "voice", this);

		const char * status = is_in_meeting(NULL) ? "iic:voice,meeting" : "iic:voice";
		::send_presence(session, status);
	}

	// We can only send voice presence.  client must send in meeting presence for data.
	// Voice presence is cleared when call is unregistered
}

// Send ping to specific port - data side only
void User::send_ping(Port * port, const char * id, int status)
{
	if (port != NULL)
	{
		Address addr;

        // send ping
        if (port->is_connected())
		{
			switch (port->get_port_type())
			{
				case MediaData:
					// Send error to client
					if (parse_address(port->get_id(), &addr) == Ok)
					{
						if (rpc_make_call(id, addr.id, "reverse_ping", "(si)", id, status))
						{
							log_error(ZONE, "Error sending reverse_ping to user %s", get_id());
						}
                    	log_debug(ZONE, "Send reverse_ping status %s:%d to user %s", id, status, get_id());
					}
					break;

                default:
					break;
			}
		}
	}

}

// Send error to specific port.
void User::send_error(Port * port, const char * id, int status, const char * message, const char * meetingid, const char * partid)
{
	if (port != NULL)
	{
		Address addr;
		const char *addr_str;

		// If port is connected to meeting, send error along.
		if (port->is_connected())
		{
			switch (port->get_port_type())
			{
				case MediaData:
					// Send error to client
					if (parse_address(port->get_id(), &addr) == Ok)
					{
						if (rpc_make_call(id, addr.id, "error", "(sisss)", id, status, message, meetingid, partid))
						{
							log_error(ZONE, "Error sending status to user %s", get_id());
						}
					}
					break;

				case MediaVoice:
					// Send error to voice service
					addr_str = port->get_id();
					spMeeting meeting = master.find_meeting(meetingid);
					if (addr_str != NULL && strlen(addr_str) != 0 && meeting != NULL)
					{
						if (rpc_make_call(id, meeting->get_voice_service(), "voice.error", "(ssis)",
										  addr_str, id, status, message))
						{
							log_error(ZONE, "Error sending status to voice for %s", get_id());
						}
					}
					break;
			}
		}
	}

	log_debug(ZONE, "Send status %s:%d to user %s", id, status, get_id());
}

// Send error to all ports in specified meeting.
void User::send_error(const char * id, int status, const char * message, const char * meetingid, const char * partid)
{
    IUseMutex lock(user_lock);

	IListIterator iter(ports);

	spPort port = (Port *)iter.get_first();
	while (port != NULL)
	{
		spParticipant part = port->get_participant();

		// If port is connected to meeting, send error along.
		if (part != NULL && strcmp(part->get_meeting()->get_id(), meetingid) == 0)
		{
			send_error(port, id, status, message, meetingid, partid);
		}

		port = (Port *)iter.get_next();
	}

	log_debug(ZONE, "Send status %s:%d to user %s", id, status, get_id());
}

// Register new port address for user.
int User::add_port(const char *port, const char *network_address)
{
	IUseMutex lock(user_lock);

    int media_type = get_media_type(port);
    if (media_type >= 0)
    {
		// User can only register from one client at a time.
		if (media_type == MediaData && is_connected(MediaData))
		{
            // get port that is currently connected
            spPort disconnect_port = get_client();

			// if not same address, disconnect current port to make way for
			// new port.
			if (strcmp(disconnect_port->get_id(), port) != 0)
			{
				lock.unlock();

	            // send disconnected error to it
	            send_error(disconnect_port, "logout", Disconnected, "Joined elsewhere", "", "");

	            // remove port - set is_disconnect to true because this could be a rejoin
	            remove_port(disconnect_port->get_id(), true);

				lock.lock();
			}
		}

        if (media_type == MediaData)
        {
            last_ping_time = time(NULL);
            reverse_ping_time = 0;
        }

		// Otherwise, check for port and create if necessary.
		spPort p = get_port(port);
		if (p == NULL)
		{
			p = new Port(port, network_address, media_type, this);
			ports.add(p);
			p->dereference();
		}
		else
		{
			// Have this address, make sure it is marked as connected.
			p->set_connected(true);
		}
    }

	// Send voice presence if necessary.
	if (media_type == MediaVoice && is_user())
	{
		send_presence();
	}

    if (media_type < 0)
    {
        log_error(ZONE, "Address with invalid type received for user %s : %s\n", (const char *)user_id, port);
        return InvalidAddress;
    }

    return Ok;
}

// Remove registered port address.
void User::remove_port(const char *port, bool is_disconnect)
{
	IUseMutex lock(user_lock);

	spPort p = get_port(port);
	if (p != NULL)
	{
		// Connection is already disconnected (port may not be deleted right away)
		p->set_connected(false);

		spParticipant participant = p->get_participant();
		if (participant != NULL)
		{
			//p->set_participant(NULL);

			spSubMeeting sm = participant->get_submeeting();
			if (sm != NULL)
			{
				// Submeeting may discconect other participants b/c of this action
				sm->on_user_disconnect(participant, p);
			}

			spMeeting meeting = participant->get_meeting();
			if (meeting != NULL)
			{
				lock.unlock();

				// Disconnect port from meeting.
				meeting->leave_user(this, p, is_disconnect);

				lock.lock();
			}

		}

		p->set_participant(NULL);
		ports.remove(p, true);
	}

	if (!is_connected(MediaVoice) && session != NULL)
	{
		// Removing session will inform SM this user has disconnected
		end_session(session);
		session = NULL;
	}
}

spPort User::get_port(Meeting * meeting, int media_type)
{
	IUseMutex lock(user_lock);

	IListIterator iter(ports);

	Port * p = (Port *)iter.get_first();
	while (p != NULL)
	{
		if (p->get_port_type() == media_type && p->is_connected())
		{
			spParticipant participant = p->get_participant();
			if (participant != NULL &&
				(meeting == NULL || participant->get_meeting() == meeting))
				return p;
		}

		p = (Port *)iter.get_next();
	}

	return false;
}

spPort User::get_port(const char * address)
{
	IUseMutex lock(user_lock);

	IListIterator iter(ports);

	Port * p = (Port *)iter.get_first();
	while (p != NULL)
	{
		if (strcmp(p->get_id(), address) == 0)
			return p;
		p = (Port *)iter.get_next();
	}

	return NULL;
}

spPort User::get_client()
{
	IUseMutex lock(user_lock);

	IListIterator iter(ports);

	Port * p = (Port *)iter.get_first();
	while (p != NULL)
	{
		if (p->get_port_type() == MediaData)
			return p;
		p = (Port *)iter.get_next();
	}

	return NULL;
}

spPort User::get_voice()
{
	IUseMutex lock(user_lock);

	IListIterator iter(ports);

	Port * p = (Port *)iter.get_first();
	while (p != NULL)
	{
		if (p->get_port_type() == MediaVoice)
			return p;
		p = (Port *)iter.get_next();
	}

	return NULL;
}

// See if user has any connections to indicated meeting.
// (If meeting is NULL, see if user is in any meeting.)
bool User::is_in_meeting(Meeting * meeting, int media_type)
{
	IUseMutex lock(user_lock);

	IListIterator iter(ports);

	Port * p = (Port *)iter.get_first();
	while (p != NULL)
	{
		if ((media_type == MediaAny || p->get_port_type() == media_type) && p->is_connected())
		{
			spParticipant participant = p->get_participant();
			if (participant != NULL &&
				(meeting == NULL || participant->get_meeting() == meeting))
				return true;
		}

		p = (Port *)iter.get_next();
	}

	return false;
}

bool User::is_connected(int media_type)
{
	IUseMutex lock(user_lock);

	IListIterator iter(ports);

	Port * p = (Port *)iter.get_first();
	while (p != NULL)
	{
		if ((p->get_port_type() == media_type || media_type == MediaAny) &&
			p->is_connected())
			return true;

		p = (Port *)iter.get_next();
	}

	return false;
}

void User::set_phone(int type, const char * phone, bool direct)
{
    IUseMutex lock(user_lock);

	PhoneNumber * p = (PhoneNumber *)phones.get(type);
	if (p != NULL)
		delete p;

	p = new PhoneNumber(type, phone, direct);
	phones.set(type, p);
}

PhoneNumber * User::get_phone(int type)
{
    IUseMutex lock(user_lock);

	return (PhoneNumber *)phones.get(type);
}

int User::resolve_phone(int sel_type, const char * phone_in, char * phone_out, char * extension_out, bool & direct_out)
{
	PhoneNumber * p = NULL;

	*phone_out = '\0';

    IUseMutex lock(user_lock);

	// Caller is overriding phone with specified number.
	if (sel_type == SelPhoneThis)
	{
		PhoneNumber temp(SelPhoneThis, phone_in, true);
		temp.get_converted_phone(phone_out);
        strcpy(extension_out, (const char *)temp.get_extension());
		direct_out = true;
		return Ok;
	}

	// Use default phone setting from contact record (read when meeting was started)
	if (sel_type == SelPhoneDefault)
	{
		sel_type = get_default_phone();
	}

	p = get_phone(sel_type);

	if (p != NULL)
	{
		p->get_converted_phone(phone_out);
        strcpy(extension_out, (const char *)p->get_extension());
		direct_out = p->is_direct();
	}

	// Make sure we found something
	if (*phone_out == '\0')
		return NoPhoneNumber;

	return Ok;
}

void User::set_email(int type, const char * email)
{
    IUseMutex lock(user_lock);

	IString * e = (IString *)emails.get(type);
	if (e != NULL)
		delete e;

	e = new IString(email);
	emails.set(type, e);
}

const char * User::get_email(int type)
{
    IUseMutex lock(user_lock);

    IString * p = (IString*)emails.get(type);
    if (p != NULL)
        return p->get_string();
    return NULL;
}


const char * User::find_email_address()
{
    const char *email_address = "";

    email_address = this->get_email(SelEmail1);
    if (email_address == NULL || *email_address == '\0')
    {
        email_address = this->get_email(SelEmail2);
    }
    if (email_address == NULL || *email_address == '\0')
    {
        email_address = this->get_email(SelEmail3);
    }
    if (email_address == NULL || *email_address == '\0')
    {
        email_address = "";
    }

    return email_address;
}


bool User::is_user()
{
	// Look at user name...if not "anonymous", is normal user
	// If not specified, we don't know so assume not user
	if (username.length() == 0)
		return false;

	if (strncmp(username, ANONYMOUS_NAME, strlen(ANONYMOUS_NAME)) == 0)
		return false;

	return true;
}

bool User::is_client_jid(const char *_username)
{
	const char *rsrc = strrchr(_username ? _username : (const char *)username, '/');

	if (rsrc != NULL)
        return strncmp(rsrc, "/iic", 4) == 0 || strncmp(rsrc, "/mtg", 4 || strncmp(rsrc, "/cht", 4)) == 0;

    return false;
}

//====
// Port
//====

Port::Port(const char * _port_id, const char * _network_address, int _port_type, User * _owner)
        : port_id(_port_id), network_address(_network_address)
{
	port_type = _port_type;
    owner = _owner;
	connected = true;
}

Port::~Port()
{
	log_debug(ZONE, "Deleting port %s", get_id());
}

void Port::lock()
{
    ref_lock.lock();
}

void Port::unlock()
{
    ref_lock.unlock();
}

spParticipant Port::get_participant()
{
	IUseMutex lock(ref_lock);
	return spParticipant(participant);
}

void Port::set_participant(Participant * p)
{
	IUseMutex lock(ref_lock);

	participant = p;
}

bool Port::is_in_meeting(Meeting * m)
{
	IUseMutex lock(ref_lock);

	if (connected && participant != NULL && participant->get_meeting() == m)
		return true;

	return false;
}
