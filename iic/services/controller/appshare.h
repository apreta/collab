/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef APPSHARE_HEADER
#define APPSHARE_HEADER

#include "../../util/irefobject.h"
#include "../../util/istring.h"
#include "../../util/ithread.h"
#include "../../util/asynccomm/comm.h"

#include "events.h"
#include "../common/common.h"

// Move to separate service (so it can be hosted on separate
// server).

// Allocate slot on meeting start (port and password), inform initial
// presenter (meeting host)
// Presenter can connect to share server at designated slot, then
// call app share started API
// Other clients connect to share server

// Simple allocation strategy:  assign to server handling fewest
// meetings (to do: consider number of users in meeting also)


#define MAX_SHARE_SESSIONS 250	// Max number of meetings we'll try to handle
#define PING_INTERVAL 30

class ShareServer;
typedef IRefObjectPtr<ShareServer> spShareServer;


class AppShare
{
	IMutex share_lock;

	IVector servers;

	IThread * monitor;

public:
	AppShare();

	void add_server(const char * ip, int port);

	spShareServer allocate_session(int expected_size, const char *meeting_id);
	void release_session(spShareServer & session, const char *meeting_id);

    bool get_server_status(int idx, IString * id, int * available);

	static void * monitor_thread(void *);
};


class ShareServer : public IReferencableObject, public CommEvent
{
	IMutex server_lock;

	IString ip;
	int port;				// normal client connection
	IString password;

	IString address;		// address::port

	int usage_count;

	bool available;

    // Link to reflector
    CommClient comm;

	long last_ping;

public:
	ShareServer(const char * ip, int port);
	virtual ~ShareServer() { }

	int initialize(const char * server, int port);
	int shutdown();

    void lock();
    void unlock();

	int open_session(const char * meeting_id);
	int close_session(const char * meeting_id);

	int ping();

	// Send commands to reflector.
	int send_presenter_id(const char * meetingid, const char * partid);
    int send_end_session(const char * meetingid, int reason);
    int send_reject_participant(const char * meetingid, const char * partid, int reason);
    int send_remote_control(const char * meetingid, const char * partid, bool enable);
	int send_recording(const char * meetingid, bool enable, int start);

	void participant_joined(const char * meetingid, const char * partid);
    void participant_left(const char * meetingid, const char * partid);

    // Handle events from reflector.
	void on_participant_joined(const char * token, const char * meetingid, const char * partid, bool host);
	void on_participant_left(const char * meetingid, const char * partid, int reason);
    void on_start_session(const char * meetingid);
	void on_end_session(const char * meetingid, int reason);
    bool on_message(CommConnection * c, CommMessage * msg);


	bool is_available()				{ return available; }
	IString & get_server()			{ return ip; }
	IString & get_password()		{ return password; }
	int get_port()					{ return port; }
	int get_usage_count()			{ return usage_count; }

	IString & get_address();

    enum { TYPE_SHARESESSION = 1903 };
    virtual int type()
        { return TYPE_SHARESESSION; }
    virtual unsigned int hash()
        { return ip.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && ip == ((ShareServer*)obj)->ip; }
};


extern AppShare appshare;

#endif
