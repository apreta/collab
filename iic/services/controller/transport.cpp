/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef WIN32
#pragma warning(disable:4786)
#endif

#include <stdio.h>
#include <stdlib.h>
#include "../common/strings.h"
#include "../common/common.h"
#include "../../util/iobject.h"
#include "../../util/base64/Base64.h"
#include "../../util/base64/MIMECode.h"
#include "../../jcomponent/util/log.h"
#include "transport.h"

/**
   Sample config:

<transport>
<!-- 0 or more transport services -->
<!-- acceptable types are 'aim', 'msn', and 'yahoo' -->
              <gateway type='aim' hostname='aim.iic'>
                  <transportagent>
                      <jabuser>Imidio1</jabuser>
                      <username>Imidio1</username>
                      <password>imidio2003</password>
                  </transportagent>
              </gateway>
              <gateway type='msn' hostname='msn.iic'>
                  <transportagent>
                      <jabuser>Imidio4</jabuser>
                      <username>services@imidio.com</username>
                      <password>fredfred</password>
                  </transportagent>
              </gateway>
              <gateway type='yahoo'>
              </gateway>
</gateways>

**/

//====
// Transport
//====

Transport::Transport(const char * _id, int _service_type)
    : transport_id(_id)
{
    service_type = _service_type;
	log_debug(ZONE, "Creating transport %s for service type %d", get_id(), service_type);
}

Transport::~Transport()
{
	log_debug(ZONE, "Deleting transport %s", get_id());
}

void Transport::lock()
{
    ref_lock.lock();
}

void Transport::unlock()
{
    ref_lock.unlock();
}

bool Transport::is_connected()
{
    return (::is_connected(registration) > 0);
}

//====
// AOLTransport
//====
AOLTransport::AOLTransport(const char * _id, 
                           const char* hostname, 
                           const char* username, 
                           const char* password, 
                           const char* jabuser,
                           const char* jabhost)
    : Transport(_id, ServiceAIM)
{
    // TBD: move to base class?
    create_session(_id, hostname, username, password, jabuser, jabhost);
}

AOLTransport::~AOLTransport()
{
    // TBD - might belong in base class?
    end_registered_session(registration);
}

int AOLTransport::create_session(const char* _id, 
                                 const char* hostname, 
                                 const char* username, 
                                 const char* password,
                                 const char* jabuser,
                                 const char* jabhost)
{
    registration = register_component(_id,
                                      hostname,
                                      ServiceAIM, 
                                      username,
                                      password,
                                      jabuser,
                                      jabhost);
    
    return Ok;
}

int AOLTransport::send_instant_message(const char* username, const char* body)
{
    log_debug(ZONE, "AIM: send msg: ");
    ::send_instant_message(registration, username, body);

    return Ok;
}

//====
// MSNTransport
//====
MSNTransport::MSNTransport(const char * _id, 
                           const char* hostname, 
                           const char* username, 
                           const char* password, 
                           const char* jabuser,
                           const char* jabhost)
    : Transport(_id, ServiceMSN)
{
    // TBD: move to base class?
    create_session(_id, hostname, username, password, jabuser, jabhost);
}

MSNTransport::~MSNTransport()
{
    // TBD - might belong in base class?
    end_registered_session(registration);
}

int MSNTransport::create_session(const char* _id, 
                                 const char* hostname, 
                                 const char* username, 
                                 const char* password,
                                 const char* jabuser,
                                 const char* jabhost)
{
    registration = register_component(_id,
                                      hostname,
                                      ServiceMSN, 
                                      username,
                                      password,
                                      jabuser,
                                      jabhost);
    
    return Ok;
}

int MSNTransport::send_instant_message(const char* username, const char* body)
{
    log_debug(ZONE, "MSN: send msg: ");
    ::send_instant_message(registration, username, body);

    return Ok;
}

//====
// TransportList
//====
TransportList::TransportList(const char * _id)
    : list_id(_id)
{
}

TransportList::~TransportList()
{
    transport_list.remove_all();
}

int TransportList::increment_index()
{
    index++; 
    if (index >= transport_list.size())
        index = 0;
    return index;
}

//====
// TransportPool
//====
TransportPool::TransportPool(const char* config_file, const char* jabhost)
{
    // reads and creates
    read_config(config_file, jabhost);
}

TransportPool::~TransportPool()
{
    // ??? needed? should be destroyed by hash destroy
    IString key = AOL_SERVICE_KEY;
    TransportList* tlist = (TransportList*) gateways.get(&key);
    if (tlist != NULL)
        delete tlist;

    key = MSN_SERVICE_KEY;
    tlist = (TransportList*) gateways.get(&key);
    if (tlist != NULL)
        delete tlist;

    key = YAHOO_SERVICE_KEY;
    tlist = (TransportList*) gateways.get(&key);
    if (tlist != NULL)
        delete tlist;
}

bool verify_service_type(const char* type)
{
    return ((strcmp(type, AOL_SERVICE_KEY) == 0) ||
            (strcmp(type, MSN_SERVICE_KEY) == 0) ||
            (strcmp(type, YAHOO_SERVICE_KEY) == 0));
}

// parse the nad config file
int TransportPool::read_config(const char * config_file, const char* jabhost)
{
    log_debug(ZONE, "Read config.");
    nad_t nad;

    if (config_file != NULL)
    {
        int root = 0;
        int def = 0;

        if (nad_load((char *)config_file, &nad))
        {
            log_error(ZONE, "error reading transport, aborting\n");
            return Failed;
        }

        // find first gateway
        def = nad_find_child_elem(nad, root, "gateway", 1);
        while (def > 0)
        {
            bool valid_transport = true;

            log_debug(ZONE, "Parse transport element");
            IString type = get_attr_value(nad, def, "type", NULL);
            IString hostname = get_attr_value(nad, def, "hostname", NULL);

            log_debug(ZONE, "Service type: %s hostname: %s", 
                      type.get_string(), 
                      hostname.get_string());

            if (!verify_service_type(type))
            {
                log_error(ZONE, "Unrecognized service type %s", type.get_string());
                valid_transport = false;
            }

            if (hostname.length() == 0)
            {
                log_error(ZONE, "Hostname not found");
                valid_transport = false;
            }

            if (valid_transport)
            {
                // create list for this gateway and store
                TransportList * transport_list = new TransportList(type.get_string());
                IString * key = new IString(type);
                gateways.insert(key, transport_list);
            
                // read list of transport agents, storing info for each
                int elem = nad_find_child_elem(nad, def, "transportagent", 1);
                while (elem > 0)
                {
                    IString jabuser = get_child_value(nad, elem, "jabuser", NULL);
                    IString username = get_child_value(nad, elem, "username", NULL);
                    IString password = get_child_value(nad, elem, "password", NULL);

                    if (jabuser.length() > 0 && username.length() > 0 && password.length() > 0)
                    {
                        // call factory
                        Transport* transport = create_transport(type, 
                                                                hostname,
                                                                username,
                                                                password, 
                                                                jabuser,
                                                                jabhost);
                        // add to list
                        transport_list->add(transport);
                    }
                    elem = nad_find_next_sibling(nad, elem);
                }
            }
            
            // find more at this level
            def = nad_find_next_sibling(nad, def);
        }
        log_debug(ZONE, "Done with config.");
    }
    nad_free(nad);

    return Ok;
}

// factory method to create Transports of different types
Transport* TransportPool::create_transport(const char* type,
                                           const char* hostname,
                                           const char* username, 
                                           const char* password, 
                                           const char* jabuser,
                                           const char* jabhost)
{
    Transport* transport = NULL;
    char id[32];

    if (strcmp(type, AOL_SERVICE_KEY) == 0)
    {
        static int aim_index = 0;
        sprintf(id, "%s%d", AOL_SERVICE_KEY, aim_index++);
        transport = new AOLTransport(id, hostname, username, password, jabuser, jabhost);
    }
    else if (strcmp(type, MSN_SERVICE_KEY) == 0)
    {
        static int aim_index = 0;
        sprintf(id, "%s%d", MSN_SERVICE_KEY, aim_index++);
        transport = new MSNTransport(id, hostname, username, password, jabuser, jabhost);
    }
    // TBD - other transport types

    return transport;
}

Transport* TransportPool::get_transport(const char* type)
{
    Transport* transport = NULL;
    IString key(type);

    TransportList* tlist = (TransportList*) gateways.get(&key);

    // if a list for that service exists
    if (tlist != NULL)
    {
        // walk through the list, getting the available Transport
        int original_index = tlist->get_index();
        int index = original_index;
        bool done = false;

        while (!done)
        {
            // get next transport and increment index
            transport = (Transport*) tlist->get(index);
            index = tlist->increment_index();

            if (transport == NULL || transport->is_connected())
                done = true;
            else if (index == original_index) // if we've wrapped around
            {
                transport = NULL;
                done = true;
            }
        }
    }

    return transport;
}

