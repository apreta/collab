/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef _WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "controllerrpc.h"
#include "controller.h"
#include "docshare.h"
#include "../common/common.h"
#include "../../jcomponent/util/log.h"
#include "../../jcomponent/util/port.h"

#ifdef LINUX
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>

extern "C"
{
#ifndef HAVE_STRERROR
#define HAVE_STRERROR
extern const char * const sys_errlist[];
extern int sys_nerr;
#endif
}

#include "../../util/base64/Base64.h"

#endif


DocShare docshare;

//////////////////////////////////////////////////////////////////////////////
DocShare::DocShare()
{
	monitor = NULL;
}

// Add doc share server
void DocShare::add_server(const char * addr, const char * username, const char * password)
{
    if (strcmp(username, ANONYMOUS_NAME) == 0)
        password = ANONYMOUS_PASSWORD;

	spDocServer server = new DocServer(addr, username, password);

	servers.add(server);

	server->dereference();

	if (monitor == NULL)
		monitor = new IThread(docserver_monitor_thread, this);
}

// Allocate server session
spDocServer DocShare::allocate_session(int expected_size)
{
	DocServer * ses = NULL;
	int min = MAX_DOCSHARE_SESSIONS;

	IUseMutex lock(share_lock);

	for (int i=0; i < servers.size(); i++)
	{
		DocServer * check = (DocServer *)servers.get(i);

		if (check->is_available() && check->get_usage_count() < min)
		{
			ses = check;
			min = ses->get_usage_count();
		}
	}

	if (ses != NULL)
		ses->open_session();

	return ses;
}

void DocShare::release_session(spDocServer & ses)
{
	IUseMutex lock(share_lock);

	ses->close_session();
}

// for annaotation w/o starting a meeting
// no need to addref
spDocServer DocShare::get_docserver()
{
	DocServer * ses = NULL;
	int min = MAX_DOCSHARE_SESSIONS;

	IUseMutex lock(share_lock);

	for (int i=0; i < servers.size(); i++)
	{
		DocServer * check = (DocServer *)servers.get(i);

		if (check->is_available() && check->get_usage_count() < min)
		{
			ses = check;
			min = ses->get_usage_count();
		}
	}

	return ses;
}

void * DocShare::docserver_monitor_thread(void *p)
{
        DocShare * _this = (DocShare *)p;
	bool done = false;
	int start, pause;

	while (!done)
	{
		start = time(NULL);

		log_debug(ZONE, "Checking doc share servers");

		IVectorIterator iter(_this->servers);
		spDocServer server = (DocServer *)iter.get_first();

		while (server != NULL && !done)
		{
			server->ping();

			server = (DocServer *)iter.get_next();
		}

		pause = DOCSHARE_PING_INTERVAL - (time(NULL) - start);
		if (pause > 0)
		  sleep(pause);
	}

	return NULL;
}

bool DocShare::get_server_status(int idx, IString * id, int * available)
{
    if (idx >= servers.size()) return false;

    spDocServer server = (DocServer *)servers.get(idx);

    *id = server->get_address();
    *available = server->is_available();
    
    return true;
}

DocServer::DocServer(const char * addr, const char * _username, const char * _password)
	: address(addr), username(_username), password(_password)
{
	usage_count = 0;
	available = false;

	curl = curl_easy_init();
    headers = NULL;
    headers = curl_slist_append(headers, "Connect: close");

	ping_url = addr;
	ping_url.append("ping.html");
	curl_easy_setopt(curl, CURLOPT_URL, ping_url.get_string());
	
	userpwd = _username;
	userpwd.append(":");
	userpwd.append(_password);
	curl_easy_setopt(curl, CURLOPT_USERPWD, userpwd.get_string());

	curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1);
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, DOCSHARE_PING_INTERVAL/2);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
}

void DocServer::lock()
{
    server_lock.lock();
}

void DocServer::unlock()
{
    server_lock.unlock();
}

int DocServer::open_session()
{
	IUseMutex lock(server_lock);

	usage_count++;

	return 0;
}

int DocServer::close_session()
{
	IUseMutex lock(server_lock);

	usage_count--;

	return 0;
}

int DocServer::ping()
{
  CURLcode res = CURLE_OK;

  if (curl)  // initialized in c'tor
    {
      log_status(ZONE, "pinging doc share %s", address.get_string());

      res = curl_easy_perform(curl);
      if (res == CURLE_OK)
	{
	  available=true;
	  log_status(ZONE, "ping ok; %s is available", address.get_string());
	}
      else
	{
	  available=false;
	  log_status(ZONE, "ping failed %s (res=%d); making unavailable",
		    address.get_string(), res);
	}
    }
  else
    {
      log_error(ZONE, "curl context failed to initialize");
    }
  
  return 0;
}
