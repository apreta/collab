/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef APIDISPATCH
#define APIDISPATCH


class RPCResultSet;
typedef struct s2s_st *s2s_t;
typedef struct nad_st *nad_t;


// Queue for calls waiting for database operation to complete or XML message to be received.
class DispatchQueue
{
	IMutex queue_lock;
	IHash queue;

	static int dispatch_id_seq;

public:
	// Add call to dispatch list--use return value as RPC id so that
	// item is dispatched when rpc result is received.
	const char * add_dispatch(DispatchCall *call);

	void cancel_dispatch(const char * id);

    // Database operation completed
	void process_completed(const char * id, bool success, RPCResultSet * res=NULL);

    // XML response received
    void process_completed(const char * id, bool success, s2s_t s2s, nad_t nad);
};


// Queue for calls waiting for a specific operation (e.g. meeting start).
class PendingQueue
{
    IMutex queue_lock;
    IList queue;

public:
    void add_pending(DispatchCall *call);

    int get_number_waiting()    { return queue.size(); }

    void process_all();
    void cancel_all();
};


class DispatchCall : public IObject
{
	void * session;

public:
	virtual ~DispatchCall();
	virtual void execute(RPCResultSet * res) = 0;
    virtual void execute(s2s_t s2s, nad_t nad);
    virtual void execute();
	virtual const char * get_name() = 0;
	virtual void failed() = 0;

    enum { TYPE_DISPATCH = 4356 };
    virtual int type()
        { return TYPE_DISPATCH; }
    virtual unsigned int hash()
        { return 0; }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && this == ((DispatchCall*)obj); }
};


// Store call info while user is looked up.
class CallParticipantParameters : public DispatchCall
{
	spMeeting meeting;
	spUser user;
	spParticipant part;
	int sel_call_phone;
	IString call_phone;
	int options;

public:
	CallParticipantParameters(spMeeting & _meeting, spUser & _user, spParticipant & _part,
				int _sel_call_phone, const char * _call_phone, int _options);
	const char * get_name() { return "CallParticipant"; }
	void execute(RPCResultSet * res);
	void failed();
};


// Store participant info while schedule is updated.
class AddParticipantParameters : public DispatchCall
{
	spUser actor;
	spMeeting meeting;
	spSubMeeting sm;
	spUser user;		/* target */
	int role;
	IString name;
	int sel_phone;
	IString phone;
	int sel_email;
	IString email;
	IString screen;
	IString description;
	IString message;
	int notify_type;
	int call_type;

public:
	AddParticipantParameters(User * _actor, Meeting * _meeting, User * _user,
				SubMeeting * _sm, int _role, const char * name,
				int _sel_phone, const char * _phone,
				int _sel_email, const char * email, const char * screen,
				const char * description, const char * message,
				int notify_type, int call_type);
	const char * get_name() { return "AddParticipant"; }
	void execute(RPCResultSet * res);
	void failed();
};


// Store registration info while pin is looked up.
class RegisterDispatch : public DispatchCall
{
public:
	void * session;
	IString userid;
	IString username;
	IString name;
	IString email;
	IString address;
    IString network_address;

	RegisterDispatch(void * session, const char *userid, const char * username, const char * name,
                     const char * email, const char * address, const char * network_address);
	const char * get_name() { return "RegisterDispatch"; }
	void execute(RPCResultSet * res);
	void failed();
};


// Store join meeting info until pin is looked up.
class JoinMeetingDispatch : public DispatchCall
{
	spUser user;
	spPort port;
	IString meetingid;
	IString participantid;
	IString password;

public:
	JoinMeetingDispatch(User * actor, Port * port, const char * meetingid, const char * partid, const char * password);
	const char * get_name() { return "JoinMeeting"; }
	void execute(RPCResultSet * res);
	void failed();
};


// Store join meeting info until pin is looked up.
class JoinUserDispatch : public DispatchCall
{
	spUser user;
	spPort port;
	spMeeting meeting;
	IString participantid;
	IString password;
	bool release;

public:
	JoinUserDispatch(User * actor, Port * port, Meeting * meeting, const char * partid, const char * password, bool release);
	const char * get_name() { return "JoinUser"; }
	void execute(RPCResultSet * res);
    void execute();
	void failed();
};


// Waiting for submeeting to start before adding participants
class AddParticipantsDispatch : public DispatchCall
{
	spUser user;
	spMeeting meeting;
	IList * participants;
	spSubMeeting sm;

public:
	AddParticipantsDispatch(User * actor, Meeting * meeting, IList * participants, SubMeeting * sm);
	const char * get_name() { return "AddParticipants"; }
	void execute(RPCResultSet * res);
	void failed();
};

// Store join meeting info until pin is looked up.
class PlaceCallDispatch : public DispatchCall
{
	spUser user;
	spMeeting meeting;
    spParticipant participant;
    spUser callee;
    spSubMeeting submeeting;
    IString phone;

public:
	PlaceCallDispatch(User * actor, Meeting * _meeting, Participant * _participant, User * _callee, const char *phone);
	const char * get_name() { return "PlaceCall"; }
    void set_submeeting(SubMeeting * _submeeting);

	void execute(RPCResultSet * res);
	void failed();
};

class GetPortsDispatch : public DispatchCall
{
    void *m_session;

  public:

    GetPortsDispatch(void * session) : m_session(session) { }
	const char * get_name() { return "GetPorts"; }

	void execute(RPCResultSet * res);
	void failed();
};

class ResetPortDispatch : public DispatchCall
{
    void *m_session;

  public:

    ResetPortDispatch(void * session) : m_session(session) { }
	const char * get_name() { return "ResetPort"; }

	void execute(RPCResultSet * res);
	void failed();
};


class HealthDispatch : public DispatchCall
{
	void * m_session;
    int get_attribute(nad_t nad, int elem, const char* key, char* value, int max_size);

public:
    HealthDispatch(void * session) : m_session(session) {}
	const char * get_name() { return "Health"; }

    void execute(RPCResultSet * res) {} // don't handle
    void failed() {}                    // don't handle
    void execute(s2s_t s2s, nad_t nad);
};



extern DispatchQueue dispatch_queue;

#endif
