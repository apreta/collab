/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <util.h>

#ifdef _WIN32
#include <windows.h>
#include <winsock2.h>
#endif

#ifdef LINUX
#include <openssl/des.h>

#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#endif

extern "C"
{
#include "WWWLib.h"
#include "WWWInit.h"
#include "WWWApp.h"
#include "HTDAV.h"                /* here - WebDAV high level functions*/
#include "xmlparse.h"
}
#include "../../util/istring.h"
#include "../../jcomponent/util/log.h"
#include "../../util/base64/Base64.h"
#include "repository.h"

const int skip_flag = 0;//IIC_REPOSITORY_SKIP_LIBWWW_INIT;

static DES_key_schedule s_keysched;

// the following functions cannot put in the header file, because the calling file may not include right headers

// called when request complete
static int terminate_handler (HTRequest *request,
			HTResponse *response,
			void *param,
			int status);
// create a generic request object
static HTRequest * create_request (void);
// called when error occurs during request
static int error_callback (HTRequest * request, HTResponse * response,void * param, int status);
// setup authentication for the specified request
static int set_basic_auth (HTRequest *request, const char *username, const char *password);

/*=========================================================================
**  call_info
**=========================================================================
**  This data structure holds all the information about a particular call,
**  including the lower-level HTTP information.
*/

typedef struct {

    /* These fields are used when performing synchronous calls. */
    int is_done;
    int http_status;
    char* output;

    /* Low-level information used by libwww. */
    HTRequest   *request;
    HTChunk     *response_data;
} call_info;

////////////////////////////////////////////////////////////////////////////
CRepository::CRepository ()
{
    m_inited = FALSE;
}

CRepository::~CRepository ()
{
    cleanup();
}

/*=========================================================================
**  printer
**=========================================================================
**  log debug output
*/
int CRepository::printer (const char * fmt, va_list pArgs)
{
	char msg[1024];
	int r = vsnprintf(msg, sizeof(msg), fmt, pArgs);
	log_debug(ZONE, msg);
	return r;
}

/*=========================================================================
**  tracer
**=========================================================================
**  log error output
*/
int CRepository::tracer (const char * fmt, va_list pArgs)
{
	char msg[1024];
	int r = vsnprintf(msg, sizeof(msg), fmt, pArgs);
	log_error(ZONE, msg);
	return r;
}

/*=========================================================================
**  init
**=========================================================================
**  init the library
*/
int CRepository::init(int flags, const char *appname, const char *appversion)
{
    if (m_inited) 
        return 0;

    m_inited = TRUE;

    m_saved_flags = flags;
    if (!(m_saved_flags & IIC_REPOSITORY_SKIP_LIBWWW_INIT)) {  // for sharelite, we may skip libwww init
        log_debug(ZONE, "Init libwww");
	    HTProfile_newNoCacheClient(appname, appversion);
	    HTPrint_setCallback(printer);
	    HTTrace_setCallback(tracer);
    }

    /* Set trace messages and alert messages*/
#if 0
    HTSetTraceMessageMask("spolc");
#endif

    HTHost_setEventTimeout(10000);

    return 0;
}

/*=========================================================================
**  cleanup
**=========================================================================
**  cleanup the library
*/
void CRepository::cleanup()
{
    if (m_inited)
    {
        if (!(m_saved_flags & IIC_REPOSITORY_SKIP_LIBWWW_INIT)) {
            log_debug(ZONE, "Uninit libwww");
	        HTProfile_delete();
        }
        m_inited = FALSE;
    }
}

/*=========================================================================
**  terminate_handler
**=========================================================================
**  This handler gets called when a synchronous request is completed.
*/

static int terminate_handler (HTRequest *request,
				    HTResponse *response,
				    void *param,
				    int status)
{
    call_info *info;

    if (response) {
        log_debug (ZONE, "Status:%d\n\tContent-length:%d\n\tIs Cachable:%c\n\tis Cached:%c\n\tReason: %s\n",
                  status, HTResponse_length(response),
                  (HTResponse_isCachable(response))?'Y':'N',
                  (HTResponse_isCached(response,YES))?'Y':'N', 
                  (HTResponse_reason(response))?HTResponse_reason(response):"NULL");
        log_debug(ZONE, "Format : %s \n", (char *)HTAtom_name(HTResponse_format(response))); 
    }
    else 
        log_error (ZONE, "Response NULL\n");

    /* Fetch the call_info structure describing this call. */
    info = (call_info*) HTRequest_context(request);

    info->is_done = 1;
    info->http_status = status;
	if (status < 0) info->http_status = -status;

    HTEventList_stopLoop();

    return HT_ERROR;
}

/*=========================================================================
**  error_callback
**=========================================================================
** error_callback : global after filter to treat the request's errors
** Function's type : HTNetAfter
*/
int error_callback (HTRequest * request, HTResponse * response,
                                          void * param, int status) {

    HTList * error_list = NULL;
    HTError * error = NULL;
        
    log_debug (ZONE, "ERROR CALLBACK\n");
    log_debug(ZONE, "request %s \n\tresponse %s \n\tstatus %d\n", \
                     (request)?"OK":"NULL",\
                     (response)?"OK":"NULL",status);
        
    if (request) {
        error_list = HTRequest_error (request);
        while (error_list && (error = (HTError *) HTList_nextObject(error_list))) {
            log_error (ZONE, "\tError location %s\n",HTError_location(error));
            switch (HTError_severity(error)) {
                case ERR_UNKNOWN :
                        log_error (ZONE, "\tSeverity : UNKNOWN\n");
                        break;
                        
                case ERR_FATAL :
                        log_error (ZONE, "\tSeverity : FATAL\n");
                        break;
                        
                case ERR_NON_FATAL :
                        log_error (ZONE, "\tSeverity : NON FATAL\n");
                        break;
                        
                case ERR_WARN :
                        log_error (ZONE, "\tSeverity : WARN\n");
                        break;
                        
                case ERR_INFO :
                        log_error (ZONE, "\tSeverity : INFO\n");
                        break;

                default :
                        log_error (ZONE, "\tSeverity : %Xd\n",HTError_severity(error));
                        break;
            }        
        }
    }

    return HT_OK;
}

CBase64     m_encorder;

void CRepository::set_archive_name_key(const char * keystr)
{
    char key[16];

    IUseMutex lock(m_lock);

    if (strlen(keystr) > 16)
    {
        log_error(ZONE, "archive name key too long!");
        return;
    }
    
    m_encorder.Decode(keystr, (LPTSTR)&key);
    if (DES_set_key((DES_cblock *)key, &s_keysched) < 0)
    {
        log_error(ZONE, "DES_set_key() returns error; bogus key?");
    }
}

void CRepository::encrypt(IString * b64ciphertxt, const char * cleartxt)
{
    unsigned int clrlen = strlen(cleartxt)+1; // include null char for sanity
    unsigned int nblock = clrlen/8;
    if (clrlen % 8) ++nblock;	// account for partial block

    DES_cblock *ciptxt = (DES_cblock *)calloc(nblock, sizeof(DES_cblock) /* 8 */);
    DES_cblock *clrtxt = (DES_cblock *)calloc(nblock, sizeof(DES_cblock) /* 8 */);
    DES_cblock ivec = { 0 };

    strcpy((char *)clrtxt, cleartxt);
    unsigned int ii;

    DES_pcbc_encrypt((unsigned char *)clrtxt,
                     (unsigned char *)ciptxt,
                     nblock*sizeof(DES_cblock),
                     &s_keysched, &ivec, 1 /* encrypt */);
    
    CBase64 b64;
    char * b64out = b64.Encode((const char *)ciptxt, nblock*sizeof(DES_cblock));
    for (ii = 0; ii < strlen(b64out); ii++)
    {
        if (b64out[ii] == '/')
            b64out[ii] = '_';
    }
    
    *b64ciphertxt = b64out;

    delete b64out;
    free((void *)clrtxt);
    free((void *)ciptxt);
}

int CRepository::decrypt(IString * cleartxt, const char * b64str)
{
    // Decode the b64 ciphertxt
    char * bin = (char *)malloc(strlen(b64str)+1);
    char * in  = (char *)malloc(strlen(b64str)+1);
    unsigned int ii;

    strcpy(in, b64str);
    for (ii = 0; ii < strlen(b64str); ii++)
    {
        if (b64str[ii] == '_')
            in[ii] = '/';
        else
            in[ii] = b64str[ii];
    }
    
    int len = m_encorder.Decode(in, bin);
    assert((len % 8) == 0);
    unsigned int nblock = len/8;

    // Decrypt
    DES_cblock *ciptxt = (DES_cblock *)bin;
    DES_cblock *clrtxt = (DES_cblock *)calloc(nblock, sizeof(DES_cblock));
    DES_cblock ivec = { 0 };
    
    DES_pcbc_encrypt((unsigned char *)ciptxt,
                     (unsigned char *)clrtxt,
                     nblock*sizeof(DES_cblock),
                     &s_keysched, &ivec, 0 /* decrypt */);
    
    // Is there a null char somewhere in the output?
    for (ii = 0; ii < nblock*8 && (((const char *)clrtxt)[ii] != '\0'); ii++);
    if (ii == (nblock*8))
    {
        log_error(ZONE, "No null found in unencrypted text; garbage input?");
        
        free((void *)clrtxt);
        free((void *)bin);
        free((void *)in);
        
        return -1;
    }

    *cleartxt = (const char *)clrtxt;

    free((void *)clrtxt);
    free((void *)bin);
    free((void *)in);
    
    return 0;
}

/*=========================================================================
**  set_basic_auth
**=========================================================================
*/
static int set_basic_auth (HTRequest *request,
					const char *username,
					const char *password)
{
    size_t username_len, password_len, raw_token_len;
    char *raw_token;
    char *token_data, *auth_type, *auth_header;
    size_t token_len, auth_type_len, auth_header_len;

    log_debug(ZONE, "Set authentication header");

    raw_token = NULL;
    auth_header = NULL;

    /* Calculate some lengths. */
    username_len = strlen(username);
    password_len = strlen(password);
    raw_token_len = username_len + password_len + 1;

    /* Build a raw token of the form 'username:password'. */
    raw_token = (char*) malloc(raw_token_len + 1);
    if (!raw_token)
    {
        log_error(ZONE,	"Couldn't allocate memory for auth token");
        return -1;
    }
    strcpy(raw_token, username);
    raw_token[username_len] = ':';
    strcpy(&raw_token[username_len + 1], password);

    /* Encode our raw token using Base64. */
    token_data = m_encorder.Encode(raw_token, raw_token_len);
    token_len = raw_token_len * 2;

    /* Build our actual header value. (I hate string processing in C.) */
    auth_type = "Basic ";
    auth_type_len = strlen(auth_type);
    auth_header_len = auth_type_len + token_len;
    auth_header = (char*) malloc(auth_header_len + 1);
    if (!auth_header)
    {
	    free(raw_token);
        log_error(ZONE,	"Couldn't allocate memory for auth header");
        return -1;
    }
    memcpy(auth_header, auth_type, auth_type_len);
    memcpy(&auth_header[auth_type_len], token_data, token_len);
    auth_header[auth_header_len] = '\0';

    HTRequest_addCredentials(request, "Authorization", auth_header);

    free(raw_token);
    free(auth_header);	

    return 0;
}

/*=========================================================================
**  replaceChar
**=========================================================================
*/
void CRepository::replaceChar(char * message, const char * markup, const char cr)
{
    char * tok;
    while (1) {
        tok = strstr(message, markup);
        if (!tok) {
            break;
        }

        if (cr!='\0')
        {
            *tok = cr;
            strcpy(tok+1, tok+strlen(markup));
        }
        else
        {
            strcpy(tok, tok+strlen(markup));
        }
    }
}

/*=========================================================================
**  Expat Event Handler Functions
**=========================================================================
*/
void CRepository::start_element (void *user_data, XML_Char *name, XML_Char **atts)
{
    parse_context *context;
    /* Get our context and see if an error has already occured. */
    context = (parse_context*) user_data;

    if (strcmp(name, "D:href")==0)
    {
        context->found_href = TRUE;
    }
}

void CRepository::end_element (void *user_data, XML_Char *name)
{
}

void CRepository::character_data (void *user_data, XML_Char *s, int len)
{
    parse_context *context;

    /* Get our context and see if an error has already occured. */
    context = (parse_context*) user_data;

    if (context->found_href)
    {
        context->found_href = FALSE;
        char* href = (char*)malloc(len + 1);
        memset(href, 0, len+1);
        strncpy(href, s, len);
        if (context->base_url.length()==0)
        {
            context->base_url = href;  // keep the base url to filter out it from the list
        }
        else
        {
            replaceChar(href, context->base_url, 0);
		    if (href[strlen(href)-1]=='/')
            {
			    href[strlen(href)-1]=0;
            }

            context->items.add(new IString(href));     // the IString will be destroyed when the vector is freed
        }
        free(href);
    }
}


/*=========================================================================
**  Expat Driver
**=========================================================================
**  XXX - We should allow the user to specify the encoding of our xml_data.
*/

int CRepository::xml_parse (parse_context* context, char *xml_data, int xml_len)
{
    XML_Parser parser;
    int ok;

    /* Set up our error-handling preconditions. */
    parser = NULL;

    /* Set up our XML parser. */
    parser = XML_ParserCreate(NULL);
    if (parser)
    {    
        XML_SetUserData(parser, context);
        XML_SetElementHandler(parser,
            (XML_StartElementHandler) start_element,
            (XML_EndElementHandler) end_element);
        XML_SetCharacterDataHandler(parser, (XML_CharacterDataHandler) character_data);

        /* Parse our data. */
        ok = XML_Parse(parser, xml_data, xml_len, 1);
        if (!ok)
        {
            log_error(ZONE, (char*) XML_ErrorString(XML_GetErrorCode(parser)));
        }

        XML_ParserFree(parser);
    }
    else
    {
        log_error(ZONE, "Could not create expat parser");
    }

    return TRUE;
}

/*=========================================================================
**  create_request
**=========================================================================
*/
static HTRequest * create_request (void) 
{
    HTRequest * request = HTRequest_new();

    /* Install our request handler. */
    HTRequest_addAfter(request, terminate_handler, NULL, NULL, HT_ALL,
		       HT_FILTER_LAST, NO);

    HTRequest_addAfter (request,error_callback, NULL, NULL, \
                         HT_ERROR, HT_FILTER_LAST, NO); 
    return request;       
}

/*=========================================================================
**  propfind_request
**=========================================================================
*/
int CRepository::propfind_request (	
    const char* url,
    const char* userid,
    const char* passwd,
	const char* depth,
    void* output
) 
{
    int status = 0;
    int http_status = 0;
    call_info *retval = (call_info*)output;
    HTDAVHeaders * headers = HTDAVHeaders_new();
    HTRequest * request = create_request ();
    HTAnchor * dst = HTAnchor_findAddress(url);
	char xmlbody[] = "<?xml version=\"1.0\"?>\n"
		"<A:propfind xmlns:A=\"DAV:\">\n"
		"<A:prop>\n"
		"<A:displayname/>\n"
		"</A:prop>\n"
		"</A:propfind>\n";

    /* Allocate our structure. */
    retval->request = request;

    HTStream* target_stream = HTStreamToChunk(request,
				    &retval->response_data, 0);
    HTRequest_setOutputStream(request, target_stream);    
    HTRequest_setOutputFormat(request,HTAtom_for("text/xml"));

    /* Install ourselves as the request context. */
    HTRequest_setContext(request, retval);

    /* Send an authorization header. */
	if (*userid || *passwd)
    	set_basic_auth(request, userid, passwd);

    HTDAV_setDepthHeader (headers,depth);

    status = HTPROPFINDAnchor (request,dst,xmlbody,headers);
    if (status) 
    {
        while (!retval->is_done)
	        HTEventList_newLoop();

        retval->output = HTChunk_toCString(retval->response_data);
        http_status = retval->http_status;
    }

    HTDAVHeaders_delete(headers);
//    HTChunk_delete(retval->response_data);
    HTRequest_delete(retval->request);

    log_debug(ZONE, "Return code = %d.", http_status);
    return http_status;
}

/*=========================================================================
**  delete_request
**=========================================================================
*/
int CRepository::delete_request (	
    const char* url,
    const char* userid,
    const char* passwd,
    void* output
) 
{
    int status = 0;
    int http_status = 0;
    call_info *retval = (call_info*)output;
    HTRequest * request = create_request ();
    HTAnchor * dst = HTAnchor_findAddress(url);

    retval->request = request;

    HTStream* target_stream = HTStreamToChunk(request,
				    &retval->response_data, 0);
    HTRequest_setOutputStream(request, target_stream);    
    HTRequest_setOutputFormat(request,HTAtom_for("text/xml"));

    /* Install ourselves as the request context. */
    HTRequest_setContext(request, retval);

    /* Send an authorization header. */
	if (*userid || *passwd)
    	set_basic_auth(request, userid, passwd);

    status = HTDeleteAnchor(dst, request);
    if (status) 
    {
        while (!retval->is_done)
	        HTEventList_newLoop();

        if (retval->response_data)
        {
            retval->output = HTChunk_toCString(retval->response_data);
        }
        http_status = retval->http_status;
    }

    HTRequest_delete(retval->request);
    log_debug(ZONE, "Return code = %d.", http_status);
    return http_status;
}

/*=========================================================================
**  get_recording_list
**=========================================================================
*/
int CRepository::get_recording_list(
	const char* meetingid,
	const char* recordingurl,
	const char* userid,
	const char* passwd,
    IList* list_recordings,
	char** output)
{
    int return_code = -1;

    IUseMutex lock(m_lock);

    call_info *retval = NULL;
    IString out_string;

    /* Allocate our structure. */
    retval = (call_info*) malloc(sizeof(call_info));
    memset(retval, 0, sizeof(call_info));

    init(skip_flag, "ZonRepositoryClient", "1.0");

    log_debug(ZONE, "Sending request to %s(%s)", recordingurl, meetingid);

    IString url = recordingurl;
	url.append(meetingid);
    url.append("/");
    
    int status = propfind_request(url, userid, passwd, "2", retval);
    if (status==200 || status==207)
    {
        parse_context context;
        context.found_href = FALSE;

        log_debug(ZONE, "response: %s", retval->output);

        // parse the result
        if (xml_parse(&context, retval->output, strlen(retval->output))) 
        {
            /* Clean up the request */
            if (retval->output) HT_FREE(retval->output);
//            HTChunk_delete(retval->response_data);
            free(retval);

            // retrieve individual recording info
            IListIterator recording_i(context.items);
            IString * recording = (IString *)recording_i.get_first();
            IString newUrl = url;
            IString real_folder_name;
            while (recording)
            {
                IString type = "-1";
                IString file_name = recording->get_string();
                int index = file_name.find("/");
                if (index == -1)  // it is a folder
                {
                    // create a base url for this archive
                    newUrl = url + recording->get_string();
                    newUrl.append("/");

                    // see if the name needs to be decrypted
                    const char *dash = strchr(recording->get_string(), '-');
                    if (dash==NULL)  // if the folder is encrypted, decrypt it
                    {
                        IString tmp;
                        decrypt(&tmp, recording->get_string());
                        gchar * tt = strdup(tmp);
                        g_strreverse(tt);
                        real_folder_name = tt;   // folder name is decrypted
                        free(tt);
                    }
                    else
                    {
                        real_folder_name = file_name;	// in case it is not
                    }
                }
                else    // check if the folder contains any file
                {
                    if (file_name.find("chat.txt")>0)
                    {
                        type = "3";
                    }
                    else if (file_name.find("audio.mp3")>0)
                    {
                        type = "1";
                    }
                    else if (file_name.find("flash2.html")>0)
                    {
                        type = "0";
                    }
                    else if (file_name.find("summary.txt")>0)
                    {
                        type = "4";
                    }
                    else    // other files not interested in
                    {
                        recording = (IString *)recording_i.get_next();
                        continue;
                    }

                    // the file name contains the folder name, remove it
                    file_name.erase(0, index+1);	// remove the '/' too
                }

                // name
                list_recordings->add(new IString(real_folder_name.get_string()));
                // type
                list_recordings->add(new IString(type));
                // url
                if (index == -1)
                {
                    list_recordings->add(new IString(newUrl));
                }
                else
                {
                    list_recordings->add(new IString(newUrl + file_name.get_string()));
                }

                recording = (IString *)recording_i.get_next();
            }
            if (output)
            {
                out_string = "Succeeded";
                *output = (char*)malloc(out_string.length() + 1);
                strcpy(*output, (const char*)out_string);
            }

            context.items.remove_all();
            return 0;
        }
        else
        {
            out_string = "Parse error: ";
            if (retval->output!=NULL)
            {
                out_string.append(retval->output);
            }
            log_error(ZONE, out_string);
            if (output)
            {
                *output = (char*)malloc(out_string.length() + 1);
                strcpy(*output, out_string);
            }
        }
    }
    else
    {
        out_string = "Get recordings failed: ";
        get_error_string(status, out_string);
        log_error(ZONE, out_string);
        if (retval->output!=NULL)
        {
            log_error(ZONE, "%s", retval->output);
        }
        if (status == 404)  // dont report 404 error, return empty is fine
        {
            return_code = 0;
        }
        else
        {
            if (output)
            {
                *output = (char*)malloc(out_string.length() + 1);
                strcpy(*output, out_string);
            }
        }
    }

    /* Clean up the request */
    if (retval->output) HT_FREE(retval->output);
//    HTChunk_delete(retval->response_data);
    free(retval);

    return return_code;
}
/*=========================================================================
**  remove_recordings
**=========================================================================
*/
int CRepository::remove_recordings(
	const char* meetingid,
	const char* recordingurl,
	const char* userid,
	const char* passwd,
    char** output
	)
{
    IUseMutex lock(m_lock);

    call_info *retval = NULL;

    /* Allocate our structure. */
    retval = (call_info*) malloc(sizeof(call_info));
    memset(retval, 0, sizeof(call_info));

    init(skip_flag, "ZonRepositoryClient", "1.0");

    log_debug(ZONE, "Sending request to %s(%s)", recordingurl, meetingid);

    int status = delete_request(recordingurl, userid, passwd, retval);
    if (status==200 || status==204)
    {
        log_debug(ZONE, "response: %s", retval->output);
        log_debug(ZONE, "remove_recordings succeeded");

        if (retval->output) HT_FREE(retval->output);
//        if (retval->response_data) HTChunk_delete(retval->response_data);
        free(retval);
        
        return 0;
    }
    else
    {
        IString out_string = "Remove recording request failed: ";
        get_error_string(status, out_string);
        log_error(ZONE, out_string);
        if (retval->output!=NULL)
        {
            log_error(ZONE, "%s", retval->output);
        }
        if (output)
        {
            *output = (char*)malloc(out_string.length() + 1);
            strcpy(*output, out_string);
        }
    }

    if (retval->output) HT_FREE(retval->output);
//    if (retval->response_data) HTChunk_delete(retval->response_data);
    free(retval);

    return -1;
}

/*=========================================================================
**  get_doc_list
**=========================================================================
*/
int CRepository::get_doc_list(
	const char* meetingid,
	const char* docurl,
	const char* userid,
	const char* passwd,
    IList* list_documents,
	char** output)
{
    IUseMutex lock(m_lock);

    call_info *retval = NULL;

    /* Allocate our structure. */
    retval = (call_info*) malloc(sizeof(call_info));
    memset(retval, 0, sizeof(call_info));
    
    init(skip_flag, "ZonRepositoryClient", "1.0");

    log_debug(ZONE, "Sending request to %s(%s)", docurl, meetingid);

    IString url = docurl;
	url.append(meetingid);
    url.append("/");
    
    int status = propfind_request(url, userid, passwd, "1", retval);
    log_debug(ZONE, "response: %s", retval->output);
    if (status==200 || status==207)
    {
        parse_context context;
        context.found_href = FALSE;

        // parse the result
        if (xml_parse(&context, retval->output, strlen(retval->output))) 
        {
            IString out_string;
            // retrieve individual recording info
            for (int i = 0; i < context.items.size(); i++)
            {
                const char * doc = *(IString *)context.items.get(i);
                IString newUrl = url + doc;
                newUrl.append("/");

                list_documents->add(new IString(doc));
                list_documents->add(new IString(newUrl));
            }
            if (output)
            {
                out_string = "Succeeded";
                *output = (char*)malloc(out_string.length() + 1);
                strcpy(*output, out_string);
            }

            if (retval->output) HT_FREE(retval->output);
//            if (retval->response_data) HTChunk_delete(retval->response_data);
            free(retval);

            return 0;
        }
    }
    else
    {
        IString out_string = "Get documents request failed: ";
        get_error_string(status, out_string);
        log_error(ZONE, out_string);
        if (retval->output!=NULL)
        {
            log_error(ZONE, "%s", retval->output);
        }
        if (output)
        {
            *output = (char*)malloc(out_string.length() + 1);
            strcpy(*output, out_string);
        }
    }

    if (retval->output) HT_FREE(retval->output);
//    if (retval->response_data) HTChunk_delete(retval->response_data);
    free(retval);

    return -1;
}

/*=========================================================================
**  remove_docs
**=========================================================================
*/
int CRepository::remove_docs(
        const char* meetingid,
        const char* docurl,
        const char* userid,
        const char* passwd,
        char** output
    )
{
    IUseMutex lock(m_lock);

    call_info *retval = NULL;

    /* Allocate our structure. */
    retval = (call_info*) malloc(sizeof(call_info));
    memset(retval, 0, sizeof(call_info));

    init(skip_flag, "ZonRepositoryClient", "1.0");

    log_debug(ZONE, "Sending request to %s(%s)", docurl, meetingid);

    int status = delete_request(docurl, userid, passwd, retval);
    if (status==200 || status==204)
    {
        log_debug(ZONE, "response: %s", retval->output);
        log_debug(ZONE, "remove_docs succeeded");

        if (retval->output) HT_FREE(retval->output);
//        if (retval->response_data) HTChunk_delete(retval->response_data);
        free(retval);
        
        return 0;
    }
    else
    {
        IString out_string = "Remove documents request failed: ";
        get_error_string(status, out_string);
        log_error(ZONE, out_string);
        if (retval->output!=NULL)
        {
            log_error(ZONE, "%s", retval->output);
        }
        if (output)
        {
            *output = (char*)malloc(out_string.length() + 1);
            strcpy(*output, out_string);
        }
    }

    if (retval->output) HT_FREE(retval->output);
//    if (retval->response_data) HTChunk_delete(retval->response_data);
    free(retval);

    return -1;
}

/*=========================================================================
**  get_error_string
**=========================================================================
*/
void CRepository::get_error_string(int status, IString& output)
{
    char status_str[32];
    snprintf(status_str, 32, "(%d)", status);

    switch(status)
    {
    case 100:   // HTTP_STATUS_CONTINUE
    case 101:   // HTTP_STATUS_SWITCH_PROTOCOLS
    case 200:   // HTTP_STATUS_OK:              // (200)
    case 201:   // HTTP_STATUS_CREATED:         // (201)
    case 202:   // HTTP_STATUS_ACCEPTED:        // (202)
    case 203:   // HTTP_STATUS_PARTIAL:         // (203)
    case 204:   // HTTP_STATUS_NO_CONTENT:      // (204)
    case 205:   // HTTP_STATUS_RESET_CONTENT:   // (205)
    case 206:   // HTTP_STATUS_PARTIAL_CONTENT: // (206)
    case 207:   // HTTP_MULTI_STATUS:           // 207 
        output.append("Request succeeded.");
        break;

    case 300:   // HTTP_STATUS_AMBIGUOUS:           // (300)
    case 301:   // HTTP_STATUS_MOVED:               // (301)
    case 302:   // HTTP_STATUS_REDIRECT:            // (302)
    case 303:   // HTTP_STATUS_REDIRECT_METHOD:     // (303)
    case 304:   // HTTP_STATUS_NOT_MODIFIED:        // (304)
    case 305:   // HTTP_STATUS_USE_PROXY:           // (305)
    case 306:   // HTTP_STATUS_REDIRECT_KEEP_VERB:  // (307)
     output.append("The Document Server is experiencing difficulty.\nPlease try your action again or contact your system administrator ");
     break;

    case 400:   // HTTP_STATUS_BAD_REQUEST:      // (400)
        output.append("The Document Server has received an invalid request.\nPlease try your action again or contact your system administator.");
        break;

    case 401:   // HTTP_STATUS_DENIED:              // (401)
    case 402:   // HTTP_STATUS_PAYMENT_REQ:         // (402)
    case 403:   // HTTP_STATUS_FORBIDDEN:           // (403)
        output.append("Access is denied. Please try your action again or contact your system administrator. ");
        break;

    case 404:   // HTTP_STATUS_NOT_FOUND:           // (404)
        output.append("The requested document is not found. Refresh your document list.\nIf problem persists contact your system administrator.");
        break;

    case 423: // HTTP_LOCKED   423 
        output.append("The file is locked by another user.  Please try your action again later.");
        break;

    case 424: // HTTP_FAILED_DEPENDENCY   424 
        output.append("The file maybe still in use, or locked by another user.\nPlease try your action again later.");
        break;

    case 405:   // HTTP_STATUS_BAD_METHOD:          // (405)
    case 406:   // HTTP_STATUS_NONE_ACCEPTABLE:     // (406)
    case 407:   // HTTP_STATUS_PROXY_AUTH_REQ:      // (407)
    case 408:   // HTTP_STATUS_REQUEST_TIMEOUT:     // (408)
    case 409:   // HTTP_STATUS_CONFLICT:            // (409)
    case 410:   // HTTP_STATUS_GONE:                // (410)
    case 411:   // HTTP_STATUS_LENGTH_REQUIRED:     // (411)
    case 412:   // HTTP_STATUS_PRECOND_FAILED:      // (412)
    case 413:   // HTTP_STATUS_REQUEST_TOO_LARGE:   // (413)
    case 414:   // HTTP_STATUS_URI_TOO_LONG:        // (414)
    case 415:   // HTTP_STATUS_UNSUPPORTED_MEDIA:   // (415)
    case 416:   // HTTP_RANGE_NOT_SATISFIABLE       416 
    case 417:   // HTTP_EXPECTATION_FAILED          417 
    case 422:   // HTTP_UNPROCESSABLE_ENTITY        422 
    case 426:   // HTTP_UPGRADE_REQUIRED            426  
    case 449:   // HTTP_STATUS_RETRY_WITH:          // (449)
    case 500:   // HTTP_STATUS_SERVER_ERROR:        // (500)
    case 501:   // HTTP_STATUS_NOT_SUPPORTED:       // (501)
    case 502:   // HTTP_STATUS_BAD_GATEWAY:         // (502)
    case 503:   // HTTP_STATUS_SERVICE_UNAVAIL:     // (503)
    case 504:   // HTTP_STATUS_GATEWAY_TIMEOUT:     // (504)
    case 505:   // HTTP_STATUS_VERSION_NOT_SUP:     // (505)
    default:
         output.append("The Document Server has received an invalid request.\nPlease try your action again or contact your system administrator.");
         break;
    }	
    output.append(status_str);

}

/*=========================================================================
**  build_url
**=========================================================================
* Build encrypted URL out of meeting id and meeting instance timestamp.
*/
int CRepository::build_url(
	const char* recording_base_url,
	const char* meetingid,
	const char* folder,
	IString& output
	)
{
	// see if the name needs to be encrypted
    IString real_folder_name;
	const char *dash = strchr(folder, '-');
	if (dash!=NULL)
	{
		// looks like date-time string, encrypt name first
		IString tmp;
		gchar * tt = strdup(folder);
		g_strreverse(tt);
		encrypt(&real_folder_name, tt);
		free(tt);
	}
	else
	{
		real_folder_name = folder;
	}		

	// Build folder path
	output = recording_base_url;
	output.append(meetingid);
	output.append("/");
	output.append(real_folder_name);
	output.append("/");
	
	return 0;
}



#if 0
int main(int argc, char * argv[])
{
    if (argc < 4)
    {
        printf("Usage:\n"
            "meetingid url userid passwd\n");
    }

    skip_flag = 0;

    CRepository repository;
    char* output = NULL;
    repository.get_recording_list(
    	argv[1],argv[2], argv[3], argv[4], &output);

    if (output)
    {
        printf("Result: %s", output);
        free(output);
    }

    return 0;
}
#endif
