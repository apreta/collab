/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <util.h>

#ifdef _WIN32
#include <windows.h>
#include <winsock2.h>
#endif

#ifdef LINUX
#include <openssl/des.h>

#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#endif

#include <ne_session.h>
#include <ne_socket.h>
#include <ne_auth.h>
#include <ne_redirect.h>
#include <string.h>
#include <ne_props.h>
#include <ne_basic.h>
#include <ne_string.h>

#include "../../util/istring.h"
#include "../../jcomponent/util/log.h"
#include "../../util/base64/Base64.h"
#include "repository.h"

const int skip_flag = 0;//IIC_REPOSITORY_SKIP_LIBWWW_INIT;

static DES_key_schedule s_keysched;


/*=========================================================================
**  call_info
**=========================================================================
*/

struct call_info
{
	call_info()
	{
		output = NULL;
		username = NULL;
		password = NULL;
	}

    /* These fields are used when performing synchronous calls. */
    char* output;

	/* Authentication info */
	const char* username;
	const char* password;

	/* Property parsing info */
	IString base_url;
    IList items;

};

////////////////////////////////////////////////////////////////////////////
CRepository::CRepository ()
{
    m_inited = FALSE;
}

CRepository::~CRepository ()
{
    cleanup();
}

/*=========================================================================
**  printer
**=========================================================================
**  log debug output
*/
int CRepository::printer (const char * fmt, va_list pArgs)
{
	char msg[1024];
	int r = vsnprintf(msg, sizeof(msg), fmt, pArgs);
	log_debug(ZONE, msg);
	return r;
}

/*=========================================================================
**  tracer
**=========================================================================
**  log error output
*/
int CRepository::tracer (const char * fmt, va_list pArgs)
{
	char msg[1024];
	int r = vsnprintf(msg, sizeof(msg), fmt, pArgs);
	log_error(ZONE, msg);
	return r;
}

/*=========================================================================
**  init
**=========================================================================
**  init the library
*/
int CRepository::init(int flags, const char *appname, const char *appversion)
{
    if (m_inited)
        return 0;

    m_inited = TRUE;

    m_saved_flags = flags;
	if (ne_sock_init()) {
		log_error(ZONE, "Unable to initialize socket for WebDAV");
		return -1;
	}

    return 0;
}

/*=========================================================================
**  cleanup
**=========================================================================
**  cleanup the library
*/
void CRepository::cleanup()
{
    if (m_inited)
    {
        m_inited = FALSE;
    }
}


static int auth_handler(void *userdata, const char *realm, int attempt,
					char *username, char *password)
{
  call_info *c = (call_info *)userdata;
  strncpy(username, c->username, NE_ABUFSIZ);
  strncpy(password, c->password, NE_ABUFSIZ);
  return attempt;
}

#if 0
static int iterator(void *userdata, const ne_propname *pname, const char *value,
		     		const ne_status *status) {
  printf("propfind nspace=%s", pname->nspace);
  printf(" name=%s", pname->name);
  printf(" value=%s\n", value);

  return 0;
}
#endif

#ifndef NEW_NEON_PROPS

static void property_handler(void *userdata, const char *href,
					const ne_prop_result_set *results)
{
    //printf("propfind callback href = %s\n", href);
    //int x = ne_propset_iterate(results, iterator, userdata);

	call_info *call = (call_info *)userdata;

	int len = strlen(href);
	if (len > 0)
	{
	    // First result will be the meeting folder itself
		if (call->base_url.length() == 0)
			call->base_url = href;
		else
		{
			// Remove base folder name from rest of the results
			int copy = strlen(href) - call->base_url.length();
			if (href[len - 1] == '/')
				copy--;  // Remove trailing backslash from folder name
			if (copy)
			{
				call->items.add(new IString(href + call->base_url.length(), copy));
			}
		}
	}
}

#else

static void property_handler(void *userdata, const ne_uri *uri,
					const ne_prop_result_set *results)
{
	call_info *call = (call_info *)userdata;
	char *href = ne_uri_unparse(uri);

	int len = strlen(href);
	if (len > 0)
	{
	    // First result will be the meeting folder itself
		if (call->base_url.length() == 0)
			call->base_url = href;
		else
		{
			// Remove base folder name from rest of the results
			int copy = strlen(href) - call->base_url.length();
			if (href[len - 1] == '/')
				copy--;  // Remove trailing backslash from folder name
			if (copy)
			{
				call->items.add(new IString(href + call->base_url.length(), copy));
			}
		}
	}

	free(href);
}

#endif

CBase64     m_encorder;

void CRepository::set_archive_name_key(const char * keystr)
{
    char key[16];

    IUseMutex lock(m_lock);

    if (strlen(keystr) > 16)
    {
        log_error(ZONE, "archive name key too long!");
        return;
    }

    m_encorder.Decode(keystr, (LPTSTR)&key);
    if (DES_set_key((DES_cblock *)key, &s_keysched) < 0)
    {
        log_error(ZONE, "DES_set_key() returns error; bogus key?");
    }
}

void CRepository::encrypt(IString * b64ciphertxt, const char * cleartxt)
{
    unsigned int clrlen = strlen(cleartxt)+1; // include null char for sanity
    unsigned int nblock = clrlen/8;
    if (clrlen % 8) ++nblock;	// account for partial block

    DES_cblock *ciptxt = (DES_cblock *)calloc(nblock, sizeof(DES_cblock) /* 8 */);
    DES_cblock *clrtxt = (DES_cblock *)calloc(nblock, sizeof(DES_cblock) /* 8 */);
    DES_cblock ivec = { 0 };

    strcpy((char *)clrtxt, cleartxt);
    unsigned int ii;

    DES_pcbc_encrypt((unsigned char *)clrtxt,
                     (unsigned char *)ciptxt,
                     nblock*sizeof(DES_cblock),
                     &s_keysched, &ivec, 1 /* encrypt */);

    CBase64 b64;
    char * b64out = b64.Encode((const char *)ciptxt, nblock*sizeof(DES_cblock));
    for (ii = 0; ii < strlen(b64out); ii++)
    {
        if (b64out[ii] == '/')
            b64out[ii] = '_';
    }

    *b64ciphertxt = b64out;

    delete b64out;
    free((void *)clrtxt);
    free((void *)ciptxt);
}

int CRepository::decrypt(IString * cleartxt, const char * b64str)
{
    // Decode the b64 ciphertxt
    char * bin = (char *)malloc(strlen(b64str)+1);
    char * in  = (char *)malloc(strlen(b64str)+1);
    unsigned int ii;

    strcpy(in, b64str);
    for (ii = 0; ii < strlen(b64str); ii++)
    {
        if (b64str[ii] == '_')
            in[ii] = '/';
        else
            in[ii] = b64str[ii];
    }

    int len = m_encorder.Decode(in, bin);
    assert((len % 8) == 0);
    unsigned int nblock = len/8;

    // Decrypt
    DES_cblock *ciptxt = (DES_cblock *)bin;
    DES_cblock *clrtxt = (DES_cblock *)calloc(nblock, sizeof(DES_cblock));
    DES_cblock ivec = { 0 };

    DES_pcbc_encrypt((unsigned char *)ciptxt,
                     (unsigned char *)clrtxt,
                     nblock*sizeof(DES_cblock),
                     &s_keysched, &ivec, 0 /* decrypt */);

    // Is there a null char somewhere in the output?
    for (ii = 0; ii < nblock*8 && (((const char *)clrtxt)[ii] != '\0'); ii++);
    if (ii == (nblock*8))
    {
        log_error(ZONE, "No null found in unencrypted text; garbage input?");

        free((void *)clrtxt);
        free((void *)bin);
        free((void *)in);

        return -1;
    }

    *cleartxt = (const char *)clrtxt;

    free((void *)clrtxt);
    free((void *)bin);
    free((void *)in);

    return 0;
}

// Name mismatch is a problem we have to ignore right now
static int my_verify(void *userdata, int failures, const ne_ssl_certificate *cert)
{
   if (failures & NE_SSL_IDMISMATCH) {
      log_error(ZONE, "Server name does not match the name on certificate, connection may have been intercepted!\n");
   }
   if (failures & NE_SSL_EXPIRED) {
      log_error(ZONE, "Server certificate is already expired!\n");
      return 1;
   }

   return 0; /* trust certificate */
}

/*=========================================================================
**  propfind_request
**=========================================================================
*/
int CRepository::propfind_request (
    const char* url,
    const char* userid,
    const char* passwd,
	const char* depth,
    void* output
)
{
    int status = 0;
    int http_status = 0;
    call_info *retval = (call_info*)output;

	retval->username = userid;
	retval->password = passwd;

	/* Parse the url. */
	ne_uri uri;
	if (ne_uri_parse(url, &uri) || uri.path == NULL || uri.host == NULL) {
		printf("nget: Invalid URI `%s'.\n", url);
		return -1;
	}

	if (uri.scheme == NULL)
		uri.scheme = "http";
	if (uri.port == 0)
		uri.port = ne_uri_defaultport(uri.scheme);

	/* Create the session */
	ne_session *sess;
	sess = ne_session_create(uri.scheme, uri.host, uri.port);
	ne_set_server_auth(sess, auth_handler, retval);

	ne_ssl_set_verify(sess, my_verify, NULL);

	/* Query all properties under this url */
	ne_propfind_handler *prop_handler;
    prop_handler = ne_propfind_create(sess, uri.path, NE_DEPTH_INFINITE);
    ne_request *request = ne_propfind_get_request(prop_handler);

    status = ne_propfind_allprop(prop_handler, property_handler, retval);
	http_status = ne_get_status(request)->code;

    ne_propfind_destroy(prop_handler);
    ne_session_destroy(sess);

    log_debug(ZONE, "Return code = %d.", http_status);
    return http_status;
}

/*=========================================================================
**  delete_request
**=========================================================================
*/
int CRepository::delete_request (
    const char* url,
    const char* userid,
    const char* passwd,
    void* output
)
{
    int status = 0;
    int http_status = 0;
    call_info *retval = (call_info*)output;

	retval->username = userid;
	retval->password = passwd;

	/* Parse the url. */
	ne_uri uri;
	if (ne_uri_parse(url, &uri) || uri.path == NULL || uri.host == NULL) {
		printf("nget: Invalid URI `%s'.\n", url);
		return -1;
	}

	if (uri.scheme == NULL)
		uri.scheme = "http";
	if (uri.port == 0)
		uri.port = ne_uri_defaultport(uri.scheme);

	/* Create the session */
	ne_session *sess;
	sess = ne_session_create(uri.scheme, uri.host, uri.port);
	ne_set_server_auth(sess, auth_handler, retval);

	ne_ssl_set_verify(sess, my_verify, NULL);

	status = ne_delete(sess, url);
    http_status = atoi(ne_get_error(sess));

    ne_session_destroy(sess);

    log_debug(ZONE, "Return code = %d.", http_status);
    return http_status;
}

/*=========================================================================
**  get_recording_list
**=========================================================================
*/
int CRepository::get_recording_list(
	const char* meetingid,
	const char* recordingurl,
	const char* userid,
	const char* passwd,
    IList* list_recordings,
	char** output)
{
    int return_code = -1;

//    IUseMutex lock(m_lock);

    call_info retval;
    IString out_string;

    init(skip_flag, "ZonRepositoryClient", "1.0");

    log_debug(ZONE, "Sending request to %s(%s)", recordingurl, meetingid);

    IString url = recordingurl;
	url.append(meetingid);
    url.append("/");

    int status = propfind_request(url, userid, passwd, "2", &retval);
    if (status==200 || status==207)
    {
		// retrieve individual recording info
		IListIterator recording_i(retval.items);
		IString * recording = (IString *)recording_i.get_first();
		IString newUrl = url;
		IString real_folder_name;
		while (recording)
		{
			IString type = "-1";
			IString file_name = recording->get_string();
			int index = file_name.find("/");
			if (index == -1)  // it is a folder
			{
				// create a base url for this archive
				newUrl = url + recording->get_string();
				newUrl.append("/");

				// see if the name needs to be decrypted
				const char *dash = strchr(recording->get_string(), '-');
				if (dash==NULL)  // if the folder is encrypted, decrypt it
				{
					IString tmp;
					decrypt(&tmp, recording->get_string());
					gchar * tt = strdup(tmp);
					g_strreverse(tt);
					real_folder_name = tt;   // folder name is decrypted
					free(tt);
				}
				else
				{
					real_folder_name = file_name;	// in case it is not
				}
			}
			else    // check if the folder contains any file
			{
				if (file_name.find("chat.txt")>0)
				{
					type = "3";
				}
				else if (file_name.find("audio.mp3")>0)
				{
					type = "1";
				}
				else if (file_name.find("video.html")>0 || file_name.find("flash2.html")>0)
				{
					type = "0";
				}
				else if (file_name.find("summary.txt")>0)
				{
					type = "4";
				}
				else    // other files not interested in
				{
					recording = (IString *)recording_i.get_next();
					continue;
				}

				// the file name contains the folder name, remove it
				file_name.erase(0, index+1);	// remove the '/' too
			}

			// name
			list_recordings->add(new IString(real_folder_name.get_string()));
			// type
			list_recordings->add(new IString(type));
			// url
			if (index == -1)
			{
				list_recordings->add(new IString(newUrl));
			}
			else
			{
				list_recordings->add(new IString(newUrl + file_name.get_string()));
			}

			recording = (IString *)recording_i.get_next();
		}
		if (output)
		{
			out_string = "Succeeded";
			*output = (char*)malloc(out_string.length() + 1);
			strcpy(*output, (const char*)out_string);
		}

		retval.items.remove_all();
		return 0;
    }
    else
    {
        out_string = "Get recordings failed: ";
        get_error_string(status, out_string);
        log_error(ZONE, out_string);
        if (retval.output!=NULL)
        {
            log_error(ZONE, "%s", retval.output);
        }
        if (status == 404)  // dont report 404 error, return empty is fine
        {
            return_code = 0;
        }
        else
        {
            if (output)
            {
                *output = (char*)malloc(out_string.length() + 1);
                strcpy(*output, out_string);
            }
        }
    }

    return return_code;
}

/*=========================================================================
**  remove_recordings
**=========================================================================
*/
int CRepository::remove_recordings(
	const char* meetingid,
	const char* recordingurl,
	const char* userid,
	const char* passwd,
    char** output
	)
{
//    IUseMutex lock(m_lock);

    call_info retval;

    init(skip_flag, "ZonRepositoryClient", "1.0");

    log_debug(ZONE, "Sending request to %s(%s)", recordingurl, meetingid);

    int status = delete_request(recordingurl, userid, passwd, &retval);
    if (status==200 || status==204)
    {
        log_debug(ZONE, "response: %s", retval.output);
        log_debug(ZONE, "remove_recordings succeeded");

        return 0;
    }
    else
    {
        IString out_string = "Remove recording request failed: ";
        get_error_string(status, out_string);
        log_error(ZONE, out_string);
        if (retval.output!=NULL)
        {
            log_error(ZONE, "%s", retval.output);
        }
        if (output)
        {
            *output = (char*)malloc(out_string.length() + 1);
            strcpy(*output, out_string);
        }
    }

    return -1;
}

/*=========================================================================
**  get_doc_list
**=========================================================================
*/
int CRepository::get_doc_list(
	const char* meetingid,
	const char* docurl,
	const char* userid,
	const char* passwd,
    IList* list_documents,
	char** output)
{
//    IUseMutex lock(m_lock);

    call_info retval;
    IString out_string;

    init(skip_flag, "ZonRepositoryClient", "1.0");

    log_debug(ZONE, "Sending request to %s(%s)", docurl, meetingid);

    IString url = docurl;
	url.append(meetingid);
    url.append("/");

    int status = propfind_request(url, userid, passwd, "1", &retval);
    log_debug(ZONE, "response: %s", retval.output);
    if (status==200 || status==207)
    {
		IString type;

		// retrieve individual recording info
		for (int i = 0; i < retval.items.size(); i++)
		{
			IString *doc = (IString *)retval.items.get(i);
			IString newUrl = url + *doc;
			//newUrl.append("/");

			int index = doc->find("/");
			if (index == -1)
			{
				type = "-1";
				newUrl.append("/");
			}
			else
			{
				type = "0";
			}

			list_documents->add(new IString(*doc));
			if (m_saved_flags & IIC_REPOSITORY_INCLUDE_DOC_TYPE)
				list_documents->add(new IString(type));
			list_documents->add(new IString(newUrl));
		}
		if (output)
		{
			out_string = "Succeeded";
			*output = (char*)malloc(out_string.length() + 1);
			strcpy(*output, out_string);
		}

		return 0;
    }
    else
    {
        IString out_string = "Get documents request failed: ";
        get_error_string(status, out_string);
        log_error(ZONE, out_string);
        if (retval.output!=NULL)
        {
            log_error(ZONE, "%s", retval.output);
        }
        if (status == 404)  // dont report 404 error, return empty is fine
        {
            return 0;
        }
		else
		{
			if (output)
			{
				*output = (char*)malloc(out_string.length() + 1);
				strcpy(*output, out_string);
			}
		}
    }

    return -1;
}

/*=========================================================================
**  remove_docs
**=========================================================================
*/
int CRepository::remove_docs(
        const char* meetingid,
        const char* docurl,
        const char* userid,
        const char* passwd,
        char** output
    )
{
//    IUseMutex lock(m_lock);

    call_info retval;

    init(skip_flag, "ZonRepositoryClient", "1.0");

    log_debug(ZONE, "Sending request to %s(%s)", docurl, meetingid);

    int status = delete_request(docurl, userid, passwd, &retval);
    if (status==200 || status==204)
    {
        log_debug(ZONE, "response: %s", retval.output);
        log_debug(ZONE, "remove_docs succeeded");

        return 0;
    }
    else
    {
        IString out_string = "Remove documents request failed: ";
        get_error_string(status, out_string);
        log_error(ZONE, out_string);
        if (retval.output!=NULL)
        {
            log_error(ZONE, "%s", retval.output);
        }
        if (output)
        {
            *output = (char*)malloc(out_string.length() + 1);
            strcpy(*output, out_string);
        }
    }

    return -1;
}

/*=========================================================================
**  get_error_string
**=========================================================================
*/
void CRepository::get_error_string(int status, IString& output)
{
    char status_str[32];
    snprintf(status_str, 32, "(%d)", status);

    switch(status)
    {
	case 0:  	// Status never set--must not have reached server.
    	output.append("The Document Server is temporarily unavailable.\nPlease try your action again or contact your system administrator ");
    	break;

    case 100:   // HTTP_STATUS_CONTINUE
    case 101:   // HTTP_STATUS_SWITCH_PROTOCOLS
    case 200:   // HTTP_STATUS_OK:              // (200)
    case 201:   // HTTP_STATUS_CREATED:         // (201)
    case 202:   // HTTP_STATUS_ACCEPTED:        // (202)
    case 203:   // HTTP_STATUS_PARTIAL:         // (203)
    case 204:   // HTTP_STATUS_NO_CONTENT:      // (204)
    case 205:   // HTTP_STATUS_RESET_CONTENT:   // (205)
    case 206:   // HTTP_STATUS_PARTIAL_CONTENT: // (206)
    case 207:   // HTTP_MULTI_STATUS:           // 207
        output.append("Request succeeded.");
        break;

    case 300:   // HTTP_STATUS_AMBIGUOUS:           // (300)
    case 301:   // HTTP_STATUS_MOVED:               // (301)
    case 302:   // HTTP_STATUS_REDIRECT:            // (302)
    case 303:   // HTTP_STATUS_REDIRECT_METHOD:     // (303)
    case 304:   // HTTP_STATUS_NOT_MODIFIED:        // (304)
    case 305:   // HTTP_STATUS_USE_PROXY:           // (305)
    case 306:   // HTTP_STATUS_REDIRECT_KEEP_VERB:  // (307)
     output.append("The Document Server is experiencing difficulty.\nPlease try your action again or contact your system administrator ");
     break;

    case 400:   // HTTP_STATUS_BAD_REQUEST:      // (400)
        output.append("The Document Server has received an invalid request.\nPlease try your action again or contact your system administator.");
        break;

    case 401:   // HTTP_STATUS_DENIED:              // (401)
    case 402:   // HTTP_STATUS_PAYMENT_REQ:         // (402)
    case 403:   // HTTP_STATUS_FORBIDDEN:           // (403)
        output.append("Access is denied. Please try your action again or contact your system administrator. ");
        break;

    case 404:   // HTTP_STATUS_NOT_FOUND:           // (404)
        output.append("The requested document is not found. Refresh your document list.\nIf problem persists contact your system administrator.");
        break;

    case 423: // HTTP_LOCKED   423
        output.append("The file is locked by another user.  Please try your action again later.");
        break;

    case 424: // HTTP_FAILED_DEPENDENCY   424
        output.append("The file maybe still in use, or locked by another user.\nPlease try your action again later.");
        break;

    case 405:   // HTTP_STATUS_BAD_METHOD:          // (405)
    case 406:   // HTTP_STATUS_NONE_ACCEPTABLE:     // (406)
    case 407:   // HTTP_STATUS_PROXY_AUTH_REQ:      // (407)
    case 408:   // HTTP_STATUS_REQUEST_TIMEOUT:     // (408)
    case 409:   // HTTP_STATUS_CONFLICT:            // (409)
    case 410:   // HTTP_STATUS_GONE:                // (410)
    case 411:   // HTTP_STATUS_LENGTH_REQUIRED:     // (411)
    case 412:   // HTTP_STATUS_PRECOND_FAILED:      // (412)
    case 413:   // HTTP_STATUS_REQUEST_TOO_LARGE:   // (413)
    case 414:   // HTTP_STATUS_URI_TOO_LONG:        // (414)
    case 415:   // HTTP_STATUS_UNSUPPORTED_MEDIA:   // (415)
    case 416:   // HTTP_RANGE_NOT_SATISFIABLE       416
    case 417:   // HTTP_EXPECTATION_FAILED          417
    case 422:   // HTTP_UNPROCESSABLE_ENTITY        422
    case 426:   // HTTP_UPGRADE_REQUIRED            426
    case 449:   // HTTP_STATUS_RETRY_WITH:          // (449)
    case 500:   // HTTP_STATUS_SERVER_ERROR:        // (500)
    case 501:   // HTTP_STATUS_NOT_SUPPORTED:       // (501)
    case 502:   // HTTP_STATUS_BAD_GATEWAY:         // (502)
    case 503:   // HTTP_STATUS_SERVICE_UNAVAIL:     // (503)
    case 504:   // HTTP_STATUS_GATEWAY_TIMEOUT:     // (504)
    case 505:   // HTTP_STATUS_VERSION_NOT_SUP:     // (505)
    default:
         output.append("The Document Server has received an invalid request.\nPlease try your action again or contact your system administrator.");
         break;
    }
    output.append(status_str);

}

/*=========================================================================
**  build_url
**=========================================================================
* Build encrypted URL out of meeting id and meeting instance timestamp.
*/
int CRepository::build_url(
	const char* recording_base_url,
	const char* meetingid,
	const char* folder,
	IString& output
	)
{
	// see if the name needs to be encrypted
    IString real_folder_name;
	const char *dash = strchr(folder, '-');
	if (dash!=NULL)
	{
		// looks like date-time string, encrypt name first
		IString tmp;
		gchar * tt = strdup(folder);
		g_strreverse(tt);
		encrypt(&real_folder_name, tt);
		free(tt);
	}
	else
	{
		real_folder_name = folder;
	}

	// Build folder path
	output = recording_base_url;
	output.append(meetingid);
	output.append("/");
	output.append(real_folder_name);
	output.append("/");

	return 0;
}



#if 0
int main(int argc, char * argv[])
{
    if (argc < 4)
    {
        printf("Usage:\n"
            "meetingid url userid passwd\n");
    }

    skip_flag = 0;

    CRepository repository;
    char* output = NULL;
    repository.get_recording_list(
    	argv[1],argv[2], argv[3], argv[4], &output);

    if (output)
    {
        printf("Result: %s", output);
        free(output);
    }

    return 0;
}
#endif
