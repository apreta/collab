/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef _LIBREPOSITORY_H
#define _LIBREPOSITORY_H

#include "../../util/base64/Base64.h"
#include "../../util/istring.h"
#include "../../util/irefobject.h"
#include "../../util/ithread.h"

#define DWORD  		unsigned int
#define WORD  		unsigned short
#define BYTE  		unsigned char

#define HT_DAV 1

#define IIC_REPOSITORY_NO_FLAGS         (0)
#define IIC_REPOSITORY_SKIP_LIBWWW_INIT (1)
#define IIC_REPOSITORY_INCLUDE_DOC_TYPE (2)

/*=========================================================================
**  parse_context
**=========================================================================
**  This data structure holds the temporary information for the xml parser
*/
typedef struct {
    IString base_url;
    IList items;

    int found_href;
} parse_context;


/*=========================================================================
**  CRepository
**=========================================================================
*/

class CRepository
{
    IMutex m_lock;

    int         m_saved_flags;
    int         m_inited;

    // make propfind request to retrieve folders
    int propfind_request (
        const char* url,
        const char* userid,
        const char* passwd,
		const char* depth,
        void* callinfo
    );
    // delete a specified folder/doc
    int delete_request (
        const char* url,
        const char* userid,
        const char* passwd,
        void * callinfo
    );
    // for debug output
    static int printer (const char * fmt, va_list pArgs);
    // for error output
    static int tracer (const char * fmt, va_list pArgs);
    // util function to replace a substring w/ a character
    static void replaceChar(char * message, const char * markup, const char cr);

#ifdef LIBWWW
    // sax parser handles
    static void start_element (void *user_data, XML_Char *name, XML_Char **atts);
    static void end_element (void *user_data, XML_Char *name);
    static void character_data (void *user_data, XML_Char *s, int len);
    int xml_parse (parse_context* context, char *xml_data, int xml_len);
#endif

    // convert status code to a readable string
    void get_error_string(int status, IString& output);

	void encrypt(IString * b64ciphertxt, const char * cleartxt);
    int decrypt(IString * cleartxt, const char * b64str);

public:
    CRepository () ;
    ~CRepository () ;

    int init(int flags, const char *appname, const char *appversion);
    void cleanup();
    void set_archive_name_key(const char* key);

    // retrieve a list files from the specified url
    int get_recording_list(
        const char* meetingid,
        const char* recording_base_url,
        const char* userid,
        const char* passwd,
        IList* list_recordings,
        char** output);

    // delete files
    int remove_recordings(
        const char* meetingid,
        const char* recordingurl,
        const char* userid,
        const char* passwd,
        char** output
        );

    // retrieve a list files from the specified url
    int get_doc_list(
        const char* meetingid,
        const char* docshare_base_url,
        const char* userid,
        const char* passwd,
        IList* list_documents,
        char** output);

    // delete files
    int remove_docs(
        const char* meetingid,
        const char* docurl,
        const char* userid,
        const char* passwd,
        char** output
        );

	int build_url(
		const char* recording_base_url,
		const char* meetingid,
		const char* folder,
		IString& output
		);
} ;

#endif
