/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef ADDRESS_HEADER
#define ADDRESS_HEADER

#include "../../util/iobject.h"
#include "../../util/istring.h"

class ResultSet;
typedef struct _xmlrpc_env xmlrpc_env;
typedef struct _xmlrpc_value xmlrpc_value;

//#define API_VERSION 2     // allow password to be passed to authenticate for user sessions
//#define API_VERSION 3     // add "packed" format for contact data
#define API_VERSION 4       // added fetch deleted APIs

struct UserSession : public IObject
{
    IString user_id;
    IString community_id;
    IString jid;
    IString screen;
    IString password;

    UserSession(const char *_user_id, const char * _community_id, const char * _jid, const char * _screen, const char * _password) :
        user_id(_user_id), community_id(_community_id), jid(_jid), screen(_screen), password(_password)
        {
        }

    enum { TYPE_USERSESSION = 3364 };
    virtual int type()
        { return TYPE_USERSESSION; }
    virtual unsigned int hash()
        { return user_id.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && user_id == ((UserSession*)obj)->user_id; }
};

UserSession * lookup_session(xmlrpc_env *env, void *rpc_session);

int get_next_mailer_id();

bool validate_screen_name(const char * screen);
bool build_string_list(xmlrpc_env *env, xmlrpc_value *fields, IString &list, bool no_quote, const char *field_prefix);
xmlrpc_value *build_output(xmlrpc_env *env, ResultSet *rs, int report_first, int actual_first, int chunk_size, const char *api);
bool check_session(xmlrpc_env *env, void *rpc_session, const char *session_id);
int check_superadmin(xmlrpc_env *env, const char * sid);
int check_admin(xmlrpc_env *env, const char * sid, const char *community_id);
bool find_email_by_userid(xmlrpc_env *env, const char *userid, char *email_address);
bool isdigits(const char *buffer);


#endif

