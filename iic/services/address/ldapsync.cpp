/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include <stdio.h>
#include <stdlib.h>
#include <sched.h>

#define LDAP_DEPRECATED 1
#include <lber.h>
#include <ldap.h>
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>

#include "../../util/istring.h"
#include "../../jcomponent/util/nadutils.h"
#include "../../jcomponent/util/log.h"
#include "../../jcomponent/util/eventlog.h"
#include "../db/sqldb.h"
#include "../common/common.h"
#include "address.h"
#include "schedule.h"

bool g_initialized = false;

extern DBManager *db;
extern IString g_server_name;

struct ldap_config_t
{
	IString ldap_url;
	IString bind_dn;
	IString bind_password;
	IString base_dn;
	IString search_filter;
	int search_scope;
	int timeout;
	IString community_id;

	ldap_config_t() { search_scope = 0; timeout = 0; }
	};

struct ldap_connection_t
{
	LDAP *ldap;
	int bound;                          /* Flag to indicate whether this connection is bound yet */
	IString reason;
	ldap_config_t *config;

	ldap_connection_t() { ldap = 0; bound = 0; config = 0; }
};

// Storage area for holding contact info read from ldap server
struct contact_t
{
	IString communityid;
	IString userid;
	IString username;
	IString server;
	IString title;
	IString firstname;
	IString middlename;
	IString lastname;
	IString suffix;
	IString company;
	IString jobtitle;
	IString address1;
	IString address2;
	IString state;
	IString country;
	IString postalcode;
	IString screenname;
	IString email;
	IString email2;
	IString email3;
	IString busphone;
	IString homephone;
	IString mobilephone;
	IString otherphone;
	IString extension;
	IString aimscreen;		// user3
	IString yahooscreen;	// user2
	IString msnscreen;		// user1
	IString user1;			// user4
	IString user2;			// user5

	IString type;
	IString directoryid;
	IString profileid;
};

// Define the contact fields we know about
struct contact_field_t
{
	const char *field;
	const char *dbfield;
	IString contact_t::*field_ptr;
	int type;   // 0 = system, 1 = string, 2 = int
};

contact_field_t g_field_list[] =
{
	{ "userid", "userid", &contact_t::userid, 0 },
	{ "communityid", "communityid", 0, 0 },
	{ "directoryid", "directoryid", &contact_t::directoryid, 1 },
	{ "username", "username", &contact_t::username, 0 },
	{ "server", "server", &contact_t::server, 1 },
	{ "title", "title", &contact_t::title, 1 },
	{ "firstname", "firstname", &contact_t::firstname, 1 },
	{ "middlename", "middlename", &contact_t::middlename, 1 },
	{ "lastname", "lastname", &contact_t::lastname, 1 },
	{ "suffix", "suffix", &contact_t::suffix, 1 },
	{ "company", "company", &contact_t::company, 1 },
	{ "jobtitle", "jobtitle", &contact_t::jobtitle, 1 },
	{ "address1", "address1", &contact_t::address1, 1 },
	{ "address2", "address2", &contact_t::address2, 1 },
	{ "city", NULL, 0, 0 },
	{ "state", "state", &contact_t::state, 1 },
	{ "country", "country", &contact_t::country, 1 },
	{ "postalcode", "postalcode", &contact_t::postalcode, 1 },
	{ "screenname", "screenname", &contact_t::screenname, 0 },
	{ "aimname", "aimname", 0 , 0 },
	{ "email", "email", &contact_t::email, 1 },
	{ "email2", "email2", &contact_t::email2, 1 },
	{ "email3", "email3", &contact_t::email3, 1 },
	{ "busphone", "busphone", &contact_t::busphone, 1 },
	{ "homephone", "homephone", &contact_t::homephone, 1 },
	{ "mobilephone", "mobilephone", &contact_t::mobilephone, 1 },
	{ "otherphone", "otherphone", &contact_t::otherphone, 1 },
	{ "extension", "extension", 0, 0 },
	{ "msnscreen", "user1", &contact_t::msnscreen, 1 },
	{ "yahooscreen", "user2", &contact_t::yahooscreen, 1 },
	{ "aimscreen", "user3", &contact_t::aimscreen, 1 },
	{ "user1", "user4", &contact_t::user1, 1 },
	{ "user2", "user5", &contact_t::user2, 1 },
	{ "defphone", "defphone", 0, 2 },

	// Only set by admins
	{ "profileid", "profileid", &contact_t::profileid, 1 },
	{ "", "type", &contact_t::type, 2 },
	{ 0, 0, 0 }
};

// Map LDAP attributes to our fields as configured by user
struct attribute_map_t
{
	IString attribute;
	IString field;
	int field_id;
	IString contact_t::*field_ptr;
};


#define MAX_SERVERS 128
#define MAX_FIELDS 64

int g_ldap_count = 0;
ldap_config_t g_ldap[MAX_SERVERS];

int g_attribute_count = 0;
attribute_map_t g_attributes[MAX_FIELDS];
char *g_attribute_names[MAX_FIELDS+1];
int g_screenname_attribute = -1;

#define CONNECT_TIMEOUT 20
#define RESULT_TIMEOUT 60    /* 60 sec */
#define RETRY_INTERVAL 120  /* 120 seconds */


/*
* Destroys an LDAP connection by unbinding and closing the connection to
* the LDAP server.
*/
int util_ldap_connection_unbind(ldap_connection_t *ldc)
{

	if (ldc) {
		if (ldc->ldap) {
			ldap_unbind_s(ldc->ldap);
			ldc->ldap = NULL;
		}
		ldc->bound = 0;
	}

	return LDAP_SUCCESS;
}

/*
* Wait for LDAP operation to complete and return result.
*/
int util_ldap_wait_result(ldap_connection_t *ldc, int msgid, bool all, LDAPMessage **res)
{
    struct timeval timeout = { RESULT_TIMEOUT, 0 };
    int result;
    LDAPMessage *localres;

    if (msgid < 0) {
        ldc->reason = "LDAP: operation was not started";
        return msgid;
    }

    result = ldap_result(ldc->ldap, msgid, all ? 1 : 0, &timeout, &localres);
    if (result < 0) {
        ldc->reason = "LDAP: failed to wait for result";
        ldap_abandon(ldc->ldap, msgid);
        return result;
    }
    else if (result > 0) {
        if (res)
            *res = localres;
        if (result == LDAP_RES_SEARCH_ENTRY)
            return LDAP_SUCCESS;
        else
            return ldap_result2error(ldc->ldap, localres, res == NULL);
    }

    return LDAP_TIMEOUT;
}


/*
* Connect to the LDAP server and binds. Does not connect if already
* connected (i.e. ldc->ldap is non-NULL.) Does not bind if already bound.
*
* Returns LDAP_SUCCESS on success; and an error code on failure
*/
int util_ldap_connection_open(ldap_connection_t *ldc)
{
    int result = 0;
    int version  = LDAP_VERSION3;
    int rc = LDAP_SUCCESS;
    struct timeval timeOut = {CONNECT_TIMEOUT,0};    /* 20 second connection timeout */
    int msgid;

    /* If the connection is already bound, return
    */
    if (ldc->bound)
    {
        return LDAP_SUCCESS;
    }

    /* create the ldap session handle
    */
    if (NULL == ldc->ldap)
    {
        /* clear connection requested */
        result = ldap_initialize(&ldc->ldap, ldc->config->ldap_url);
        if (result == LDAP_SUCCESS)
        {
            if (strncmp(ldc->config->ldap_url, "ldaps:", 6) == 0)
            {
                int SSLmode = LDAP_OPT_X_TLS_HARD;
                result = ldap_set_option(ldc->ldap, LDAP_OPT_X_TLS, &SSLmode);
                if (LDAP_SUCCESS != result)
                {
                    ldap_unbind_s(ldc->ldap);
                    ldc->reason = "LDAP: ldap_set_option - LDAP_OPT_X_TLS_HARD failed";
                    ldc->ldap = NULL;
                }
            }
    }

    if (NULL == ldc->ldap)
    {
        ldc->bound = 0;
        if (NULL == ldc->reason)
        ldc->reason = "LDAP: ldap initialization failed";
        return(-1);
    }

    /* Set the alias dereferencing option */
    //ldap_set_option(ldc->ldap, LDAP_OPT_DEREF, &(ldc->deref));

    /* always default to LDAP V3 */
    ldap_set_option(ldc->ldap, LDAP_OPT_PROTOCOL_VERSION, &version);

    /* set network timeout or initial connect may block */
    if (ldc->config->timeout > 0) {
        timeOut.tv_sec = ldc->config->timeout;
    }

    rc = ldap_set_option(ldc->ldap, LDAP_OPT_NETWORK_TIMEOUT, (void *)&timeOut);
        if (LDAP_SUCCESS != rc) {
            // log "LDAP: Could not set the connection timeout" );
        }
    }

    /* try to connect & bind
    */
    msgid = ldap_simple_bind(ldc->ldap, ldc->config->bind_dn, ldc->config->bind_password);
    result = util_ldap_wait_result(ldc, msgid, true, NULL);

    /* free the handle if there was an error
    */
    if (LDAP_SUCCESS != result)
    {
        ldap_unbind_s(ldc->ldap);
        ldc->ldap = NULL;
        ldc->bound = 0;
        ldc->reason = "LDAP: bind to directory failed";
    }
    else {
        ldc->bound = 1;
        ldc->reason = "LDAP: connection open";
    }

    return(result);
}


/*
* Initiate search
*/
int util_ldap_search(ldap_connection_t *ldc, char *search_filter,char ** attrs,
	    int *msgid, LDAPMessage **res)
{
    int result = 0;
    int failures = 0;

    /*
    * If any LDAP operation fails due to LDAP_SERVER_DOWN, control returns here.
    */
start_over:
    if (failures++ > 10) {
        return result;
    }
    if (LDAP_SUCCESS != (result = util_ldap_connection_open(ldc))) {
        return result;
    }

    /* try do the search */
    result = ldap_search_ext(ldc->ldap,
                    ldc->config->base_dn, ldc->config->search_scope,
                    search_filter, attrs, 0,
                    NULL, NULL, NULL, -1, msgid);

    result = util_ldap_wait_result(ldc, *msgid, false, res);
    if (result == LDAP_SERVER_DOWN) {
        ldc->reason = "LDAP: search failed with server down";
        util_ldap_connection_unbind(ldc);
        goto start_over;
    }

    /* if there is an error (including LDAP_NO_SUCH_OBJECT) return now */
    if (result != LDAP_SUCCESS) {
        ldc->reason = "LDAP: search failed";
        return result;
    }

    return result;
}

// Find ldap attribute based on name
static int find_attribute(const char *name)
{
    for (int i=0; i<g_attribute_count; i++)
    {
        if (strcmp(g_attributes[i].field, name) == 0)
        {
            return i;
        }
    }

    return -1;
}

// Find db field definition based on name
static int find_dbfield(const char *name)
{
    for (int i=0; g_field_list[i].field; i++)
    {
        if (strcmp(name, g_field_list[i].field) == 0)
            return i;
    }
    return -1;
}

static void set_field(contact_t &contact, int field, const char *value, bool &updated)
{
    IString contact_t::*field_ptr = g_attributes[field].field_ptr;

    IString temp(value);
    if (g_field_list[g_attributes[field].field_id].type == 0) {
        temp.to_lower();
    }

    if (strcmp(contact.*field_ptr, temp)) {
        contact.*field_ptr = temp;
        updated = true;
    }
}

static void set_dbfield(contact_t &contact, int dbfield, const char *value)
{
    IString contact_t::*field_ptr = g_field_list[dbfield].field_ptr;

    if (field_ptr != 0)
        contact.*field_ptr = value;
}

static int read_contact(contact_t &contact, const char *screenname, bool &found)
{
    IString search = IString("'") + screenname + "'";
    ResultSet *rs = NULL, *rs2 = NULL;

    rs = db->execute("find-users-by-screenname", (const char*) search);
    if (!rs || rs->is_error())
    {
        log_error(ZONE, "Database error finding user");
        goto cleanup;
    }

    if (rs->get_num_rows() > 0)
    {
        const char *userid = rs->get_value(0, "USERID");

        rs2 = db->execute("find-contact-all-by-userid", userid);
        if (!rs || rs->is_error())
            goto cleanup;

        for (int dbfield = 0; g_field_list[dbfield].field; dbfield++)
        {
            if (g_field_list[dbfield].dbfield)
            {
                const char *val = rs2->get_value(0, g_field_list[dbfield].dbfield);
                if (val)
                    set_dbfield(contact, dbfield, val);
            }
        }

        found = true;
    }

    return 0;

cleanup:
    if (rs != NULL)
    {
        db->closeResults(rs);
    }
    if (rs2 != NULL)
    {
        db->closeResults(rs2);
    }

    return -1;
}

static int setup_user(const char *communityid, contact_t &contact)
{
    int stat = -1;
    ResultSet *rs = NULL;
    int type_code = atoi(contact.type);

    if (!validate_screen_name(contact.screenname.get_string()))
    {
        log_debug(ZONE, "Invalid screen name %s found in directory", contact.screenname.get_string());
        return stat;
    }

    // Add, update, or remove user record depending on type of contact.
    switch (type_code)
    {
    case 0:
        // Normal contact
        rs = db->execute("remove-user", contact.userid.get_string());
        if (!rs || rs->is_error())
            goto cleanup;
        break;
    case 1:
        // System user
        rs = db->execute("add-user",
                    contact.userid.get_string(),
                    contact.username.get_string(),
                    contact.screenname.get_string(),
                    "password",		// ToDo: remove
                    communityid,
                    "false",
                    "false"
                    );
        if (!rs || rs->is_error())
            goto cleanup;
        db->closeResults(rs);

        // subscribe the user to the GAL directory
        rs = db->execute("subscribe-to-gal", contact.userid.get_string(), communityid);
        if (!rs || rs->is_error())
            goto cleanup;
        break;
    case 2:
        // Admin or SuperAdmin
        break;
    default:
        log_error(ZONE, "Invalid contact type specified for added or updated contact");
    }

    stat = 0;

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            log_error(ZONE, "LDAP error creating new user, username %s", contact.username.get_string());
        }
        db->closeResults(rs);
    }

    return stat;
}


int setup_default_meeting(const char *communityid, contact_t &contact, const char *name)
{
    ResultSet *rs = NULL;
    const char *userid = contact.userid.get_string();
    char mpin[32];
    char hpin[32];
    char selphone[32];
    char countstring[32];
    int meetingcount;
    int meeting_pin_length = 0;
    int participant_pin_length = 0;
    int max_meetings = 0;
    int stat = -1;

    sprintf(selphone, "%d", SelPhoneNone);

    // Check if this user has an instant meeting defined...
    rs = db->execute("find-instant-by-host-count", contact.userid.get_string());
    if (!rs || rs->is_error())
        goto cleanup;
    strcpy(countstring, rs->get_value(0, 0));
    meetingcount = atoi(countstring);
    db->closeResults(rs);

    // If there is already an instant meeting we are done...
    if (meetingcount > 0)
    {
        stat = 0;
        goto cleanup;
    }

    // If we get here...we need to create an instant meeting for this user...

    // fetch pin lengths (to be used to generate the right size pins)
    if (fetch_pin_lengths(userid, meeting_pin_length, participant_pin_length, max_meetings) == Failed)
    {
        log_error(ZONE, "Error reading pin lengths");
        goto cleanup;
    }

    // allocate meeting pin based on length
    if (!allocate_pin(mpin, meeting_pin_length))
    {
        log_error(ZONE, "Error allocating new meeting id");
        goto cleanup;
    }

    /* Create meeting
        <bindparam>$$MEETINGID$$</bindparam>
        <bindparam>$$COMMUNITYID$$</bindparam>
    <bindparam>$$HOSTID$$</bindparam>
    <bindparam>$$TITLE$$</bindparam> */
    rs = db->execute("add-instant-meeting", mpin, communityid, contact.userid.get_string(), "My Meeting");
    if (!rs || rs->is_error())
        goto cleanup;
    db->closeResults(rs);

    log_reservation(EvnLog_Reserve_Add,
            mpin,
            "",
            "",
            communityid,
            userid,
            "0",
            "",  // partid
            ""); // pin


    // allocate participant pin based on length
    if (!allocate_pin(hpin, participant_pin_length))
    {
        log_error(ZONE, "Error allocating new pin");
        goto cleanup;
    }

    /* Create host participant.
        <bindparam>$$PARTICIPANTID$$</bindparam>
        <bindparam>$$MEETINGID$$</bindparam>
        <bindparam>$$TYPE$$</bindparam>
        <bindparam>$$ROLL$$</bindparam>
        <bindparam>$$USERID$$</bindparam>
        <bindparam>$$USERNAME$$</bindparam>
        <bindparam>$$NAME$$</bindparam>
        <bindparam>$$SEL_PHONE$$</bindparam>
        <bindparam>$$PHONE$$</bindparam>
        <bindparam>$$SEL_EMAIL$$</bindparam>
        <bindparam>$$EMAIL$$</bindparam>
        <bindparam>$$SCREENNAME$$</bindparam>
        <bindparam>$$NOTIFY$$</bindparam>
        <bindparam>$$EXPIRES$$</bindparam>
        <bindparam>$$START_TIME$$</bindparam>
     */
    rs = db->execute("add-participant", hpin, mpin, "0", "3" /* RollHost */, userid, contact.username.get_string(), name,
            "0", "", "0", "", contact.screenname.get_string(), "0", "0", "0");
    if (!rs || rs->is_error())
        goto cleanup;

    log_reservation(EvnLog_Reserve_Add_Participant,
            mpin,
            "", "", "", "", "",
            hpin, hpin);

    stat = 0;

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            log_error(ZONE, "Error adding instant meeting: %s", (char*)rs->get_error());
        }
        db->closeResults(rs);
    }

    return stat;
}


static int add_contact(ldap_connection_t *ldc, contact_t &contact)
{
    const char *communityid = ldc->config->community_id.get_string();
    IString name;
    ResultSet *rs = NULL;
    char dirid[32];
    char newid[32];
    char allgroupid[32];
    char profileid_s[32];
    int stat = -1;

    // find directory id of Global Address Book for passed community
    rs = db->execute("find-directory-by-community", communityid, DEFAULT_GLOBAL_DIRECTORY);
    if (!rs || rs->is_error())
        goto cleanup;
    if (rs->get_num_rows() == 1)
    {
        strcpy(dirid, rs->get_value(0, 0));
    }
    else
    {
        log_error(ZONE, "Unable to find directory for LDAP sync");
        goto cleanup;
    }
    db->closeResults(rs);

#if 0
    // verify contact name to be formed from passed info is unique
    rs = db->execute("find-contact-by-name", dirid, first, middle, last);
    if (!rs || rs->is_error())
        goto cleanup;
    if (rs->get_num_rows() > 0)
    {
        goto cleanup;
    }
    db->closeResults(rs);
#endif

    // We're creating a user
    contact.type = "1";

    // get groupid of all group
    rs = db->execute("find-all-group", dirid);
    if (!rs || rs->is_error())
        goto cleanup;
    if (rs->get_num_rows() == 1)
    {
        strcpy(allgroupid, rs->get_value(0, 0));
    }
    else
    {
        log_error(ZONE, "Unable to find all group for LDAP sync");
        goto cleanup;
    }
    db->closeResults(rs);

    // verify profile or get default if none specified
    if (contact.profileid.length())
    {
        rs = db->execute("find-profile-by-id", contact.profileid.get_string());
        if (!rs || rs->is_error())
            goto cleanup;
        if (rs->get_num_rows() == 0)
        {
            log_error(ZONE, "Unable to find profile for LDAP sync");
            goto cleanup;
        }
        else
        {
            strcpy(profileid_s, contact.profileid);
        }
        db->closeResults(rs);
    }
    else // retrieve default profile
    {
        rs = db->execute("find-default-profile", communityid);
        if (!rs || rs->is_error())
            goto cleanup;
        if (rs->get_num_rows() == 0)
        {
            log_error(ZONE, "Unable to find default profile for LDAP sync");
            goto cleanup;
        }
        else
        {
            strcpy(profileid_s, rs->get_value(0, 0));
        }
        db->closeResults(rs);
    }

    // if no userid, generate
    if (contact.userid.length() == 0)
    {
        rs = db->execute("allocate-userid");
        if (!rs || rs->is_error())
            goto cleanup;
        strcpy(newid, rs->get_value(0, 0));
        db->closeResults(rs);
        contact.userid = newid;
    }
    else
    {
        strcpy(newid, contact.userid);
    }

    // create username from screen and server
    contact.username = contact.screenname;
    contact.username.append("@" + g_server_name);

#if 0
    // verify username to be formed from passed info is unique in our DB
    rs = db->execute("find-user-by-name", (const char*) username);
    if (!rs || rs->is_error())
        goto cleanup;
    if (rs->get_num_rows() > 0)
    {
        status = DuplicateUsername;
    }
    db->closeResults(rs);
#endif

    // Add user record
    if (setup_user(communityid, contact))
        goto cleanup;

    // Add contact to directory.
    // Create CONTACT record
    // USERID, DIRECTORYID, USERNAME, SERVER,
    // TITLE, FIRSTNAME, MIDDLENAME, LASTNAME, SUFFIX,
    // COMPANY, JOBTITLE, ADDRESS1, ADDRESS2, STATE, COUNTRY, POSTALCODE,
    // SCREENNAME, AIMNAME, EMAIL, EMAIL2, EMAIL3, BUSPHONE, BUSDIRECT,
    // HOMEPHONE, HOMEDIRECT, MOBILEPHONE, MOBILEDIRECT, OTHERPHONE, OTHERDIRECT,
    // EXTENSION, USER1, USER2, USER3, USER4, USER5,
    // DEFPHONE, DEFEMAIL,
    // TYPE, PROFILEID, MAXSIZE, MAXPRIORITY, PRIORITYTYPE,
    // ENABLEDFEATURES, DISABLEDFEATURES
    rs = db->execute("add-contact",
                newid,
                dirid,
                contact.username.get_string(),
                g_server_name.get_string(),
                contact.title.get_string(),
                contact.firstname.get_string(),
                contact.middlename.get_string(),
                contact.lastname.get_string(),
                contact.suffix.get_string(),
                contact.company.get_string(),
                contact.jobtitle.get_string(),
                contact.address1.get_string(),
                contact.address2.get_string(),
                contact.state.get_string(),
                contact.country.get_string(),
                contact.postalcode.get_string(),
                contact.screenname.get_string(),
                "", // aim
                contact.email.get_string(),
                contact.email2.get_string(),
                contact.email3.get_string(),
                contact.busphone.get_string(),
                "false",
                contact.homephone.get_string(),
                "false",
                contact.mobilephone.get_string(),
                "false",
                contact.otherphone.get_string(),
                "false",
                contact.extension.get_string(),
                contact.user1.get_string(),
                contact.user2.get_string(),
                "",
                "",
                "",
                "0", // defphone
                "0", // defemail
                contact.type.get_string(),
                profileid_s,
                "0",
                "0",
                "0",
                "0",
                "0");
    if (!rs || rs->is_error())
        goto cleanup;
    db->closeResults(rs);

    // Add new user to All group
    rs = db->execute("add-to-group", allgroupid, newid);
    if (!rs || rs->is_error())
        goto cleanup;
    db->closeResults(rs);

    rs = db->execute("set-group-modified", allgroupid);
    if (!rs || rs->is_error())
        goto cleanup;
    db->closeResults(rs);

    // Add default instant meeting for user
    name = contact.firstname + " " + contact.lastname;

    if (setup_default_meeting(communityid, contact, name.get_string()))
        goto cleanup;

    // ToDo: log event

    stat = 0;

cleanup:
    if (rs)
    {
        if (rs->is_error())
        {
            log_error(ZONE, "LDAP error adding user: %s", (char*)rs->get_error());
        }
        db->closeResults(rs);
    }

    return stat;
}

static int update_contact(ldap_connection_t *ldc, contact_t &contact)
{
    int stat = -1;
    ResultSet *rs = NULL;

    rs = db->execute("update-contact",
                    contact.userid.get_string(),
                    contact.username.get_string(),
                    contact.server.get_string(),
                    contact.title.get_string(),
                    contact.firstname.get_string(),
                    contact.middlename.get_string(),
                    contact.lastname.get_string(),
                    contact.suffix.get_string(),
                    contact.company.get_string(),
                    contact.jobtitle.get_string(),
                    contact.address1.get_string(),
                    contact.address2.get_string(),
                    contact.state.get_string(),
                    contact.country.get_string(),
                    contact.postalcode.get_string(),
                    contact.screenname.get_string(),
                    "",		// aimname
                    contact.email.get_string(),
                    contact.email2.get_string(),
                    contact.email3.get_string(),
                    contact.busphone.get_string(),
                    "false",
                    contact.homephone.get_string(),
                    "false",
                    contact.mobilephone.get_string(),
                    "false",
                    contact.otherphone.get_string(),
                    "false",
                    contact.extension.get_string(),
                    contact.msnscreen.get_string(),
                    contact.yahooscreen.get_string(),
                    contact.aimscreen.get_string(),
                    contact.user1.get_string(),
                    contact.user2.get_string(),
                    contact.type.get_string(),
                    contact.profileid.get_string(),
                    "0",
                    "0",
                    "0",
                    "0",
                    "0"
                );
    if (!rs || rs->is_error())
    {
        log_error(ZONE, "Database error updating contact");
        goto cleanup;
    }

    // ToDo:  log event

    stat = 0;

cleanup:
    if (rs != NULL)
    {
        db->closeResults(rs);
    }

    return stat;
}

static char *ldapsync_make_safe_screenname(char *original, char *safe)
{
    if(strpbrk(original,"@:'\"<>&") != NULL) {
        return NULL;
    }

    strcpy(safe, original);
    g_ascii_strup(g_strdelimit(safe, " ", '+'), -1);
    return safe;
}

static int ldapsync_process_entry(ldap_connection_t *ldc, int msgid, LDAPMessage *res)
{
    LDAPMessage *entry;
    char *dn;
    bool found_user = false;
    bool update = false;
    int result;
    int contactsProcessed = 0;
    char safeName[1024];

    for(entry = ldap_first_entry(ldc->ldap, res);
        entry;
        entry = ldap_next_entry(ldc->ldap, res)) {
    	contact_t contact;

        /* Get dn of target user */
        dn = ldap_get_dn(ldc->ldap, entry);
        log_debug(ZONE, "Checking dn %s", dn);

        /* Get screenanme and lookup existing user */
        char **values = ldap_get_values(ldc->ldap, entry, g_attribute_names[g_screenname_attribute]);
        if (values)
        {
	        if (values[0])
	        {
                if(!ldapsync_make_safe_screenname(values[0], safeName)) {
                    log_debug(ZONE, "Skipping entry with unsafe screen name: %s", values[0]);
                    ldap_value_free(values);
                    continue;
                }

                IString screen(safeName);
                result = read_contact(contact, screen, found_user);
                if (result < 0)
                {
                    ldap_value_free(values);
                    return -1;
                }
            }
            set_field(contact, g_screenname_attribute, safeName, update);
            ldap_value_free(values);
        }

        /*
         * Get values for the provided attributes.
         */
        for (int i=0; i<g_attribute_count; i++)
        {
            values = ldap_get_values(ldc->ldap, entry, g_attribute_names[i]);

            if ((i != g_screenname_attribute) && values)
            {
                if (values[0])
                    set_field(contact, i, values[0], update);

                ldap_value_free(values);
            }
        }

        if (found_user)
        {
            if (update)
                update_contact(ldc, contact);
        }
        else
        {
            add_contact(ldc, contact);
        }
	contactsProcessed++;

        ldap_memfree(dn);

        sched_yield();

    }

    return contactsProcessed;
}


int ldapsync_do_sync()
{
    ldap_connection_t ldap_conn;
    int result;
    LDAPMessage *res;
    int count = 0;
    int entriesProcessed = 0;
    int msgid;

    while (count < g_ldap_count)
    {
        ldap_conn.config = &g_ldap[count];

        log_debug(ZONE, "Checking %s", ldap_conn.config->ldap_url.get_string());

        result = util_ldap_search(&ldap_conn, (char *)ldap_conn.config->search_filter.get_string(), g_attribute_names, &msgid, &res);

        while (result == LDAP_SUCCESS)
        {
            result = ldapsync_process_entry(&ldap_conn, msgid, res);
	    if(result >= 0) {
	        entriesProcessed += result;
	    }

            bool more = ldap_msgtype(res) == LDAP_RES_SEARCH_ENTRY;
            ldap_msgfree(res);

            if (result >= 0 && more) {
                result = util_ldap_wait_result(&ldap_conn, msgid, false, &res);
            }
            else {
            	break;
            }
        }

        ++count;
    }

    if(result != LDAP_SUCCESS) {
	log_debug(ZONE, "LDAP error: %d (%s)",result, ldap_conn.reason.get_string() ? ldap_conn.reason.get_string() : "");
	return -1;
    }
    return entriesProcessed;
}


int ldapsync_init(const char *config_file)
{
	if (g_initialized)
		return 0;

   	nad_t nad;

    IString scope_str;
    ldap_config_t *ldap_cfg;
    IString ca_certfile;
    IString ca_certdir;

	if (nad_load((char *)config_file, &nad))
    {
        log_error(ZONE, "no config loaded, aborting\n");
		return -1;
    }

    /* Read global config options */
	int root = 0;

	ca_certfile = get_child_value(nad, root, "cacertfile");
	ca_certdir = get_child_value(nad, root, "cacertdir");

	/* Read LDAP servers */
	root = nad_find_elem(nad, 0, "servers", 1);
	if (root > 0)
	{
		int def = nad_find_elem(nad, root, "server", 1);
		while (def > 0 && g_ldap_count < MAX_SERVERS)
		{
            ldap_cfg = &g_ldap[g_ldap_count++];

            ldap_cfg->search_scope = LDAP_SCOPE_SUBTREE;
            ldap_cfg->timeout = 0;

            ldap_cfg->ldap_url = get_child_value(nad, def, "url");
            ldap_cfg->bind_dn = get_child_value(nad, def, "binddn");
            ldap_cfg->bind_password = get_child_value(nad, def, "bindpassword");
            ldap_cfg->base_dn = get_child_value(nad, def, "basedn");
            ldap_cfg->search_filter = get_child_value(nad, def, "filter");
            ldap_cfg->timeout = get_child_value_i(nad, def, "timeout", 0);
            ldap_cfg->community_id = get_child_value(nad, def, "communityid", "0");

            scope_str = get_child_value(nad, def, "scope");
            if (scope_str != "") {
                if (strcmp(scope_str, "one") == 0)
                    ldap_cfg->search_scope = LDAP_SCOPE_ONELEVEL;
                else if (strcmp(scope_str, "sub") != 0) {
                    log_error(ZONE, "Invalid config option for search scope, ignoring");
                }
            }

            if (ldap_cfg->ldap_url == "" || ldap_cfg->base_dn == "" || ldap_cfg->search_filter == "" ||
				ldap_cfg->bind_dn == "" || ldap_cfg->bind_password == "")
            {
                log_error(ZONE, "configuration item missing, not initializing");
                return -1;
            }

            log_debug(ZONE, "added server %s to config", ldap_cfg->ldap_url.get_string());

			def = nad_find_elem(nad, def, "server", 0);
		}
	}

    /* Read attribute to db field mapping */
	root = nad_find_elem(nad, 0, "attributes", 1);
	if (root > 0)
	{
		int def = nad_find_elem(nad, root, "attribute", 1);
		while (def > 0 && g_attribute_count < MAX_FIELDS)
		{
            attribute_map_t *map = &g_attributes[g_attribute_count];

            map->attribute = get_child_value(nad, def, "name");
            map->field = get_child_value(nad, def, "field");

            int field = find_dbfield(map->field);
            if (field >= 0)
            {
            	map->field_id = field;
            	map->field_ptr = g_field_list[field].field_ptr;
            }
            else
            {
            	log_error(ZONE, "Unknown field %s referenced in ldap config", map->attribute.get_string());
            	return -1;
            }

            g_attribute_names[g_attribute_count++] = strdup(map->attribute);

			def = nad_find_elem(nad, def, "attribute", 0);
        }

        g_attribute_names[g_attribute_count] = NULL;
    }

	g_screenname_attribute = find_attribute("screenname");
	if (g_screenname_attribute < 0)
	{
		log_error(ZONE, "Must have attribute mapping for screenname field");
		return -1;
	}

    /* set ca cert file--this option must be set globally */
    if (ca_certfile.length())
    {
        int result;
        result = ldap_set_option(NULL, LDAP_OPT_X_TLS_CACERTFILE, ca_certfile);
        if (LDAP_SUCCESS != result)
        {
            log_error(ZONE, "Error setting CA cert file");
        }
        else
        {
        	log_debug(ZONE, "CA Cert file: %s", ca_certfile.get_string());
        }
    }
    if (ca_certdir.length())
    {
        int result;
        result = ldap_set_option(NULL, LDAP_OPT_X_TLS_CACERTDIR, ca_certdir);
        if (LDAP_SUCCESS != result)
        {
            log_error(ZONE, "Error setting CA cert dir");
        }
        else
        {
        	log_debug(ZONE, "CA Cert dir: %s", ca_certdir.get_string());
        }
    }

	nad_free(nad);

	g_initialized = true;

	//ldapsync_do_sync();

	return 0;
}


xmlrpc_value *ldap_sync(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	char *sid;
    xmlrpc_value *output = NULL;
    int result;

	xmlrpc_parse_value(env, param_array, "(s)", &sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!check_superadmin(env, sid))
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "You must be a community administrator to invoke LDAP sync");
        goto cleanup;
    }

    result = ldapsync_do_sync();
    if (result < 0)
    {
        xmlrpc_env_set_fault(env, result, "LDAP synchronization failed");
        goto cleanup;
    }

    output = xmlrpc_build_value(env, "(i)", result);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

