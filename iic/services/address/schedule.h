/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef SCHEDULE_H
#define SCHEDULE_H

bool allocate_pin(char* pin, int pin_length, const char* test_mid = NULL);
int fetch_pin_lengths(const char* userid, int &meeting_pin_length, int &personal_pin_length, int &max_scheduled_meetings);
void setup_default_meeting(xmlrpc_env * env, const char *sid, const char *communityid, const char *userid, 
                           const char *username, const char *name, const char *screen,
                           IString& new_meeting_id,
                           IString& new_participant_id,
                           bool use_follow_me,
                           const char *test_meetingid = NULL
                           );
int log_reservation(int event,
                    const char *meetingid,
                    const char *start_time,
                    const char *end_time,
                    const char *communityid,
                    const char *hostid,
                    const char *options,
                    const char *partid,
                    const char *pin);

xmlrpc_value * add_participant(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * join_participant(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * remove_participant(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * find_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * find_meeting_by_userid(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * find_meetings_by_host(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * find_meetings_invited(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * find_meetings_public(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * list_meetings_by_host(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * list_meetings_invited(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * list_meetings_public(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * search_meetings(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * find_participants(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * find_meeting_details(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * find_pin(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * find_invite(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * find_meetings_by_host(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * insert_meeting_status(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * reset_meeting_status(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * create_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * update_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * remove_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * find_meetings_public_url(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * find_meetings_invited_url(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);
xmlrpc_value * find_invite_url(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data);

#endif
