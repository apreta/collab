// test_validatedll.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>
#include <gmodule.h>
#include "../testdll/Appheader.h"
#include "../validate/validate_screenname.h"
#include "../../common/common.h"

typedef int (*InitializeMethod) (const char*, const char*, const char*);
typedef int (*NamesCall) (validate_screenname_st* screennames, int num);

int main(int argc, char* argv[])
{
    int res;
    char screenname[32];

	printf("Enter a name: ");
    gets(screenname);

    if (!g_module_supported())
    {
        printf("Dynamic module loading not supported\n");
    }
    else
    {
        GModule* module = g_module_open("validate.dll", G_MODULE_BIND_LAZY);

        if (module != NULL)
        {
            InitializeMethod init_symbol;
            NamesCall get_symbol;

            validate_screenname_st names;
            strcpy(names.lookup_name, screenname);

            if (g_module_symbol(module, "initialize", (gpointer*)&init_symbol))
            {
                printf("Symbol loaded\n");
                init_symbol("dummy url", "dummy user", "dummy pass");
            }
            else
            {
                printf("Can't load symbol: %s\n", g_module_error());
            }

            if (g_module_symbol(module, "get_alternatives", (gpointer*)&get_symbol))
            {
                printf("Symbol loaded\n");
                res = get_symbol(&names, 3);
                printf("result is %d\n",res);
                if (res == DuplicateUsername)
                    printf("%s %s %s\n", names.names[0], names.names[1], names.names[2]);
                else if (res == Ok)
                    printf("Ok username\n");
                else
                    printf("Failed username\n");
            }
            else
            {
                printf("Can't load symbol: %s\n", g_module_error());
            }
        }
    }

	return 0;
}

