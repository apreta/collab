/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <locale.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <gmodule.h>
#include "address.h"
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>
#include <locale.h>
#include <libintl.h>
#include <json/json.h>
#define _(String) gettext(String)
#include "../../jcomponent/util/util.h"
#include "../../jcomponent/jcomponent_ext.h"
#include "../../jcomponent/rpc.h"
#include "../common/common.h"
#include "../common/profile.h"
#include "../db/sqldb.h"
#include "schedule.h"
#include "../../jcomponent/util/port.h"
#include "../../jcomponent/util/log.h"
#include "../../jcomponent/util/nadutils.h"
#include "../../jcomponent/util/eventlog.h"
#include "../../jcomponent/util/util.h"
#include "../../util/ithread.h"
#include "validate/validate_screenname.h"
#include "namecache.h"
#include "../librepository/repository.h"
#include "ldapsync.h"

#define JCOMPNAME	"addressbk"

// Todo:
// - Check user id in all operations to make sure user has rights to perform operation
// - Add nightly maintenance function
//     - Remove orphaned contacts

EventLog event_log;
extern EventLog g_rsrv_event_log;
IMutex event_mutex;
NameCache name_cache;

IHash user_sessions;
IMutex sessions_lock;

// although the class itself is thread safe, has to test extensively if the w3c libwww is
CRepository sRepository;

int log_user(EventLogUserEvent event,
             const char *communityid,
             const char *userid,
             const char *title,
             const char *firstname,
             const char *middlename,
             const char *lastname,
             const char *suffix,
             const char *company,
             const char *jobtitle,
             const char *address1,
             const char *address2,
             const char *state,
             const char *country,
             const char *postalcode,
             const char *screenname,
             const char *email1,
             const char *email2,
             const char *email3,
             const char *businessphone,
             const char *homephone,
             const char *mobilephone,
             const char *otherphone,
             const char *extension,
             const char *aimscreenname,
             const char *yahooscreenname,
             const char *msnscreenname,
             const char *user1,
             const char *user2,
             const char *defaultphone,
             const char *defaultemail,
             int type)
{
    char time_txt[21];
    char event_txt[4];
    char type_txt[4];
    EventLogUserField fields[] =
        {
            EvnLog_UserField_CommunityId,
            EvnLog_UserField_UserId,
            EvnLog_UserField_Event,
            EvnLog_UserField_Title,
            EvnLog_UserField_FirstName,
            EvnLog_UserField_MiddleName,
            EvnLog_UserField_LastName,
            EvnLog_UserField_Suffix,
            EvnLog_UserField_Company,
            EvnLog_UserField_JobTitle,
            EvnLog_UserField_Address1,
            EvnLog_UserField_Address2,
            EvnLog_UserField_State,
            EvnLog_UserField_Country,
            EvnLog_UserField_PostalCode,
            EvnLog_UserField_IICScreenName,
            EvnLog_UserField_Email1,
            EvnLog_UserField_Email2,
            EvnLog_UserField_Email3,
            EvnLog_UserField_BusinessPhone,
            EvnLog_UserField_HomePhone,
            EvnLog_UserField_MobilePhone,
            EvnLog_UserField_OtherPhone,
            EvnLog_UserField_Extension,
            EvnLog_UserField_AIMScreenName,
            EvnLog_UserField_YahooScreenName,
            EvnLog_UserField_MSNScreenName,
            EvnLog_UserField_User1,
            EvnLog_UserField_User2,
            EvnLog_UserField_DefaultPhone,
            EvnLog_UserField_DefaultEmail,
            EvnLog_UserField_Type,
            EvnLog_UserField_ModifiedTime,
        };
    const char *values[] =
        {
            communityid,
            userid,
            event_txt,
            title,
            firstname,
            middlename,
            lastname,
            suffix,
            company,
            jobtitle,
            address1,
            address2,
            state,
            country,
            postalcode,
            screenname,
            email1,
            email2,
            email3,
            businessphone,
            homephone,
            mobilephone,
            otherphone,
            extension,
            aimscreenname,
            yahooscreenname,
            msnscreenname,
            user1,
            user2,
            defaultphone,
            defaultemail,
            type_txt,
            time_txt,
        };

    time_t now = time(NULL);
    struct tm *tm_now = localtime(&now);
    strftime(time_txt, 20, "%Y-%m-%d-%H-%M-%S", tm_now);
    sprintf(event_txt, "%u", (int)event);
    sprintf(type_txt, "%u", (int)type);

    IUseMutex lock(event_mutex);

    return event_log.log_user_record(fields, values, (int)(sizeof(fields)/sizeof(fields[0])));
}

// cache of info about the update client file
struct update_client_info
{
    IString file_name;          // name of the file (read from jcomponent.xml)
    time_t  last_modified;      // time of last mod
    IString version;            // string version of client available from install
    int     major;              // int values of the version
    int     minor;
    int     build;
    int     platform;
    IString installer_url;      // url to the client installer
    IString message;            // an optional message to be included in the user dialog
    int     type;               // type of upgrade (1=MANDATORY, 2=OPTIONAL)
};

update_client_info g_update_client_info;

// cache new user email template file
struct new_user_template_info
{
    IString file_name;          // name of the file (read from jcomponent.xml)
    time_t  last_modified;      // time of last mod
    char*   buf;                // storage for file
    IString from_address;       // 'from' address for email msg
    IString from_display;       // 'display from' address for email msg
    IString subject;            // 'subject' field for email message
};

new_user_template_info g_new_user_template_info;

IString g_server_name;
IString g_host_portal;
IString g_recording_base_url;
IString g_docshare_base_url;

typedef int (*InitializeMethod) (const char*, const char*, const char*);
typedef int (*ValidateMethod) (validate_screenname_st* screennames, int num);

ValidateMethod add_contact_validate_method = NULL;
ValidateMethod add_user_validate_method = NULL;

DBManager *db = NULL;

int mailer_id_seq;

int get_next_mailer_id() { return ++mailer_id_seq; }

void load_validate_library(config_t conf,
                           const char *svc,
                           const char *prefix,
                           ValidateMethod* validate_method)
{
    if (!g_module_supported())
    {
        log_error(ZONE, "Dynamic module loading not supported\n");

        return;
    }
    else
    {
        IString param = prefix;
        param.append(".library");
        char * val = config_get_one(conf, svc, (char *)(const char *)param, 0);
        if (val == NULL)
            return;	// nothing to do; no lib

        GModule* module = g_module_open(val, G_MODULE_BIND_LAZY);
        if (module != NULL)
        {
            param = prefix;
            param.append(".initializeMethod");
            val = config_get_one(conf, svc, (char *)(const char *)param, 0);
            if (val == NULL)
            {
                log_error(ZONE, "No initialize method specified for library");

                return;
            }

            InitializeMethod init_method;
            if (g_module_symbol(module, val, (gpointer*)&init_method))
                {
                log_status(ZONE, "Add user/contact initialize symbol loaded");

                param = prefix;
                param.append(".initializeURL");
                char * url_val = config_get_one(conf, svc, (char *)(const char *)param, 0);

                param = prefix;
                param.append(".initializeUser");
                char * user_val = config_get_one(conf, svc, (char *)(const char *)param, 0);

                param = prefix;
                param.append(".initializePass");
                char * pass_val = config_get_one(conf, svc, (char *)(const char *)param, 0);

                if (init_method(url_val, user_val, pass_val) == Ok)
                {
                    log_status(ZONE, "Initialized library");

                    param = prefix;
                    param.append(".validateMethod");
                    val = config_get_one(conf, svc, (char *)(const char *)param, 0);
                    if (val == NULL)
                    {
                        log_error(ZONE, "No validate method defined");

                        return;
                    }

                    if (g_module_symbol(module, val, (gpointer*)validate_method))
                    {
                        log_status(ZONE, "Validate symbol loaded");
                    }
                    else
                    {
                        validate_method = NULL;
                        log_error(ZONE, "Can't load symbol %s error %s",
                                  val, g_module_error());
                    }
                }
                else
                {
                    log_error(ZONE, "Error initializing library");
                }
            }
            else
            {
                log_error(ZONE,
                          "Can't load init symbol %s error %s ",
                          val, g_module_error());
            }
        }
        else
        {
            log_error(ZONE,
                      "Unable to open shared library %s error %s ",
                      val, g_module_error());
        }
    }
}

static int init(config_t conf)
{
	log_status(ZONE, "Address service starting");

    g_server_name = config_get_one(conf, JCOMPNAME, "iicServer", 0);
    g_host_portal = config_get_one(conf, JCOMPNAME, "hostPortal", 0);

    // make the following community-based later
    g_recording_base_url = config_get_one(conf, JCOMPNAME, "recording_base_url", 0);
    g_docshare_base_url = config_get_one(conf, JCOMPNAME, "docshare_base_url", 0);
    sRepository.set_archive_name_key(config_get_one(conf, JCOMPNAME, "archive-name-key", 0));

    g_update_client_info.file_name = config_get_one(conf, JCOMPNAME, "upgradeClientFile", 0);
    g_new_user_template_info.file_name = \
        config_get_one(conf, JCOMPNAME, "newUserTemplateFile", 0);
    g_new_user_template_info.buf = NULL;
    g_new_user_template_info.from_address = config_get_one(conf, JCOMPNAME, "newUserFromAddress", 0);
    g_new_user_template_info.from_display = config_get_one(conf, JCOMPNAME, "newUserFromDisplay", 0);
    g_new_user_template_info.subject = config_get_one(conf, JCOMPNAME, "newUserFromSubject", 0);
	event_log.initialize(EvnLog_Type_User);
    g_rsrv_event_log.initialize(EvnLog_Type_Reservation);
    char * eld = config_get_one(conf, JCOMPNAME, "eventLogDir", 0);
    event_log.set_location((eld ? eld : "."), "user");
    g_rsrv_event_log.set_location((eld ? eld : "."), "rsrv");

    char * elrt = config_get_one(conf, JCOMPNAME, "eventLogRotationTime", 0);
    event_log.set_rotation_time((elrt? atoi(elrt) : 0));
    g_rsrv_event_log.set_rotation_time((elrt? atoi(elrt) : 0));
    load_validate_library(conf, JCOMPNAME, "load.addUserValidate",
                          &add_user_validate_method);
    load_validate_library(conf, JCOMPNAME, "load.addContactValidate",
                          &add_contact_validate_method);
	if (db == NULL)
	{
		db = sqldb_get_manager("/opt/iic/conf/sqldb.xml");
		if (db == NULL)
		{
			log_error(ZONE, "Unable to read address book configuration\n");
			return -1;
		}
	}
    mailer_id_seq = 0;

    char *ldapConfig = config_get_one(conf, JCOMPNAME, "ldapConfig", 0);
    if (ldapConfig != NULL)
    {
        ldapsync_init(ldapConfig);
    }

    const char *zonlang = getenv("ZONLANG");
    if (zonlang == NULL)
        zonlang = "";	// use system default

    log_debug(ZONE, "Setting locale to %s", zonlang);

    // initialize locale and timezone for display purposes (email generation)
    setlocale( LC_ALL, zonlang );
    tzset();

	bindtextdomain(JCOMPNAME, "/opt/iic/locale");
    textdomain(JCOMPNAME);
    char    *pBTC = bind_textdomain_codeset(JCOMPNAME, "UTF-8");
    log_debug(ZONE, "bind_textdomain_codeset( ) returned %s", pBTC);

	return Ok;
}

char *i_itoa(int i, char *buffer)
{
    sprintf(buffer, "%d", i);
    return buffer;
}

bool isdigits(const char *buffer)
{
    bool bIsDigits = buffer[0] != '\0';
    int  index = 0;

    while (buffer[index] != '\0' && bIsDigits)
    {
        if (!isdigit(buffer[index]))
        {
            bIsDigits = FALSE;
        }

        index++;
    }

    return bIsDigits;
}

// See if value appears to be an SHA hash.
int check_hash(const char * str)
{
    // Todo: check for characters 0-F only
    return strlen(str) == 40;
}

// Get string value from xmlrpc value, optionally checking that string
// only contains alphanumeric characters.
char *get_string_value(xmlrpc_env *env, xmlrpc_value *val, bool validate)
{
    char *pstr = NULL;
    xmlrpc_parse_value(env, val, "s", &pstr);
    XMLRPC_FAIL_IF_FAULT(env);

    if (validate)
    {
        char *p2 = pstr;
        for (;*p2;p2++)
        {
            if (!isalnum(*p2) && *p2 != '.')
            {
                xmlrpc_env_set_fault(env, ParameterParseError, "Error parsing function input:  illegal characters in field name");
                goto cleanup;
            }
        }
    }

cleanup:
    return pstr;
}

// Convert record set into output array.
// report_first is what we tell calling app.  actual_first is where we begin processing the passed result set.
xmlrpc_value *build_output(xmlrpc_env *env, ResultSet *rs, int report_first, int actual_first, int chunk_size, const char *api)
{
    xmlrpc_value *val = NULL;
    xmlrpc_value *array = NULL;
	int i;

    // Figure out how many rows to return.
    int more = 1;
    int end = rs->get_num_rows();
    if (end - actual_first > chunk_size)
        end = actual_first + chunk_size;
    else
    {
        chunk_size = end - actual_first;
        more = 0;
    }

    // Copy from result set into array.
    // Build an array of array of strings (structures resulted in much larger XML encoding).
    // We could pass a list of field names in addition to values, but for now the client must
    // understand the ordering.
	array = xmlrpc_build_value(env, "()");
	XMLRPC_FAIL_IF_FAULT(env);

    for (i=actual_first; i<end; i++)
    {
        xmlrpc_value *strings = xmlrpc_build_value(env, "()");
        XMLRPC_FAIL_IF_FAULT(env);

        for (int j=0; j < rs->get_num_fields(); j++)
        {
            const char *valstr = rs->get_value(i, j);
            if (valstr == NULL)
            {
                xmlrpc_env_set_fault(env, DBAccessError, "Error reading from database result set");
                goto cleanup;
            }
            xmlrpc_value *value = xmlrpc_build_value(env, "s", valstr);
            XMLRPC_FAIL_IF_FAULT(env);

            xmlrpc_array_append_item(env, strings, value);
            xmlrpc_DECREF(value);
            XMLRPC_FAIL_IF_FAULT(env);
        }

        xmlrpc_array_append_item(env, array, strings);
        xmlrpc_DECREF(strings);
        XMLRPC_FAIL_IF_FAULT(env);
    }

    // Create return parameter array.
    // ( row array, int start, int number, int more, int total )
    val = xmlrpc_build_value(env, "(Viiii)", array, report_first, chunk_size, rs->get_num_rows(), more);
    XMLRPC_FAIL_IF_FAULT(env);

    xmlrpc_INCREF(val);

cleanup:
    if (val)
        xmlrpc_DECREF(val);
    if (array)
        xmlrpc_DECREF(array);

    return val;
}

// Convert record set into packed output.
// report_first is what we tell calling app.  actual_first is where we begin processing the passed result set.
xmlrpc_value *build_packed_output(xmlrpc_env *env, ResultSet *rs, int report_first, int actual_first, int chunk_size, const char *api)
{
    xmlrpc_value *strval = NULL;
    xmlrpc_value *structval = NULL;
    xmlrpc_value *val = NULL;
    struct json_object *rows = NULL;
    struct json_object *row = NULL;
	int i;

    // Figure out how many rows to return.
    int more = 1;
    int end = rs->get_num_rows();
    if (end - actual_first > chunk_size)
        end = actual_first + chunk_size;
    else
    {
        chunk_size = end - actual_first;
        more = 0;
    }

    rows = json_object_new_array();

    // Copy from result set into json object.
    for (i=actual_first; i<end; i++)
    {
        row = json_object_new_array();

        for (int j=0; j < rs->get_num_fields(); j++)
        {
            const char *valstr = rs->get_value(i, j);
            if (valstr == NULL)
            {
                xmlrpc_env_set_fault(env, DBAccessError, "Error reading from database result set");
                goto cleanup;
            }

            json_object_array_add(row, json_object_new_string((char*)valstr));
        }

        json_object_array_add(rows, row);
    }

    strval = xmlrpc_build_value(env, "s", json_object_to_json_string(rows));
    XMLRPC_FAIL_IF_FAULT(env);

    // Data is put into a structure to make it easy for client to differentiate between a row array
    // and a json string.
    structval =  xmlrpc_struct_new(env);
    XMLRPC_FAIL_IF_FAULT(env);

    xmlrpc_struct_set_value(env, structval, "data", strval);
    XMLRPC_FAIL_IF_FAULT(env);

    // Create return parameter array.
    // ( row data in string, int start, int number, int more, int total )
    val = xmlrpc_build_value(env, "(Siiii)", structval, report_first, chunk_size, rs->get_num_rows(), more);
    XMLRPC_FAIL_IF_FAULT(env);

    xmlrpc_INCREF(val);

cleanup:
    if (val)
        xmlrpc_DECREF(val);
    if (strval)
        xmlrpc_DECREF(strval);
    if (structval)
        xmlrpc_DECREF(structval);
    if (rows)
        json_object_put(rows);

    return val;
}

// Convert IList into output array.
xmlrpc_value *build_output_from_list(xmlrpc_env *env, IList *list, int columns)
{
    xmlrpc_value *val = NULL;
    xmlrpc_value *array = NULL;
    IListIterator iter(*list);
    IString * str = (IString *)iter.get_first();
    int size = list->size();
    int i = 0;
    xmlrpc_value *strings = NULL;

	array = xmlrpc_build_value(env, "()");
	XMLRPC_FAIL_IF_FAULT(env);


    while(str)
    {
        if ((i++ % columns)==0)
        {
            if (strings)
            {
                xmlrpc_array_append_item(env, array, strings);
                xmlrpc_DECREF(strings);
                XMLRPC_FAIL_IF_FAULT(env);
            }

            strings = xmlrpc_build_value(env, "()");
            XMLRPC_FAIL_IF_FAULT(env);
        }

        xmlrpc_value *value = xmlrpc_build_value(env, "s", str->get_string());
        XMLRPC_FAIL_IF_FAULT(env);

        xmlrpc_array_append_item(env, strings, value);
        xmlrpc_DECREF(value);
        XMLRPC_FAIL_IF_FAULT(env);

        str = (IString *)iter.get_next();
    }

    if (strings)
    {
        xmlrpc_array_append_item(env, array, strings);
        xmlrpc_DECREF(strings);
        XMLRPC_FAIL_IF_FAULT(env);
    }

    val = xmlrpc_build_value(env, "(Viiii)", array, 0, size/columns, size/columns, 0);
    XMLRPC_FAIL_IF_FAULT(env);

    xmlrpc_INCREF(val);

cleanup:
    if (val)
        xmlrpc_DECREF(val);
    if (array)
        xmlrpc_DECREF(array);

    return val;
}

// Build parameter list from input parameters
ParamList *build_params(xmlrpc_env *env, xmlrpc_value *params, int start = 0)
{
    ParamList *list = NULL;
    int size = 0;

    // Make sure we've got right type.
	if (xmlrpc_value_type(params) != XMLRPC_TYPE_ARRAY)
    {
		xmlrpc_env_set_fault(env, ParameterParseError, "Error parsing function input:  arguments must be in array");
        goto cleanup;
    }

    // Copy strings to parameter list.
    size = xmlrpc_array_size(env, params);
    XMLRPC_FAIL_IF_FAULT(env);
    if (start < size)
    {
        list = new ParamList(size);

        for (int i=start; i<size; i++)
        {
            xmlrpc_value *val = xmlrpc_array_get_item(env, params, i);
            XMLRPC_FAIL_IF_FAULT(env);

            char *pstr;
            xmlrpc_parse_value(env, val, "s", &pstr);
            XMLRPC_FAIL_IF_FAULT(env);

            list->add(pstr);
        }
    }

cleanup:
    if (env->fault_occurred)
    {
        delete list;
        list = NULL;
    }

    return list;
}

// Take input array and build string suitable for inclusion in sql query.
bool build_string_list(xmlrpc_env *env, xmlrpc_value *fields, IString &list, bool no_quote, const char *field_prefix)
{
    bool done = false;
    int size;
	int i;

    // Make sure we've got right type.
	if (xmlrpc_value_type(fields) != XMLRPC_TYPE_ARRAY)
    {
		xmlrpc_env_set_fault(env, ParameterParseError, "Error parsing function input:  field list must be array");
        goto cleanup;
    }

    // Copy strings to parameter list.
    size = xmlrpc_array_size(env, fields);
    XMLRPC_FAIL_IF_FAULT(env);

    for (i=0; i<size; i++)
    {
        xmlrpc_value *val = xmlrpc_array_get_item(env, fields, i);
        XMLRPC_FAIL_IF_FAULT(env);

        char *pstr;
        xmlrpc_parse_value(env, val, "s", &pstr);
        XMLRPC_FAIL_IF_FAULT(env);

        if (no_quote)
        {
            // Make sure bad characters aren't allowed into SQL string
            char *p2 = pstr;
            for (;*p2;p2++)
            {
                if (!isalnum(*p2) && *p2 != '.')
                {
                    xmlrpc_env_set_fault(env, ParameterParseError, "Error parsing function input:  illegal characters in field name");
                    goto cleanup;
                }
            }
            if (i)
                list.append(",");

            if (field_prefix != NULL)
            {
                list.append(field_prefix);
            }

            list.append(pstr);
        }
        else
        {
            char valuebuffer[2*MAX_FIELD_WIDTH];

            // Quote and escape string values
            if (i)
                list.append(",");
            list.append("'");
            list.append(db->escape(pstr, valuebuffer, sizeof(valuebuffer)));
            list.append("'");
        }
    }

    done = true;

cleanup:
    if (env->fault_occurred)
    {
        done = false;
        list.clear();
    }

    return done;
}

bool add_condition(xmlrpc_env *env, IString &list, const char *field_name, const char *op,
                   const char *value, const char *field_prefix)
{
    bool done = false;
    bool nocase = !strcasecmp(op, "like-nocase");

    if (nocase)
    {
        list.append("LOWER(");
    }

    if (field_prefix != NULL)
    {
        list.append(field_prefix);
    }

    list.append(field_name);

    if (nocase)
    {
        list.append(")");
    }

    // Add operator
    if (strcmp(op, "=") && strcmp(op, "<>") && strcmp(op, "<") && strcmp(op, ">") &&
        strcasecmp(op, "like") && strcasecmp(op, "like-nocase"))
    {
        xmlrpc_env_set_fault(env, InvalidParameter, "Unknown operator specified in search condition");
        goto cleanup;
    }

    if (nocase)
    {
        list.append(" LIKE ");
    }
    else
    {
        list.append(" ");
        list.append(op);
        list.append(" ");
    }

    // Add quoted string values
    if (nocase)
    {
      list.append("LOWER(");
    }

    char valuebuffer[2*MAX_FIELD_WIDTH];
    list.append("'");
    list.append(db->escape(value, valuebuffer, sizeof(valuebuffer)));
    list.append("'");

    if (nocase)
    {
        list.append(")");
    }

    done = true;

cleanup:
    if (env->fault_occurred)
    {
        done = false;
        list.clear();
    }

    return done;
}

// Take input array and build string suitable for inclusion in sql query.
bool build_condition(xmlrpc_env *env, IString &list, xmlrpc_value *conds, xmlrpc_value *ops,
        xmlrpc_value *values, const char *field_prefix)
{
    bool done = false;
    int size;
	int i;

    size = xmlrpc_array_size(env, conds);
    if (size != xmlrpc_array_size(env, ops) || size != xmlrpc_array_size(env, values))
    {
        xmlrpc_env_set_fault(env, InvalidParameter, "Number of conditions, operators, and values must match");
        goto cleanup;
    }

    list.append(" (");

    for (i=0; i<size; i++)
    {
        // Get field name
        xmlrpc_value *cond = xmlrpc_array_get_item(env, conds, i);
        XMLRPC_FAIL_IF_FAULT(env);

        char *pstrField;
        pstrField = get_string_value(env, cond, true);
        XMLRPC_FAIL_IF_FAULT(env);

        // Get operator
        xmlrpc_value *op = xmlrpc_array_get_item(env, ops, i);
        XMLRPC_FAIL_IF_FAULT(env);

        char *pstrOp;
        pstrOp = get_string_value(env, op, false);
        XMLRPC_FAIL_IF_FAULT(env);

        // Get comparison value
        xmlrpc_value *value = xmlrpc_array_get_item(env, values, i);
        XMLRPC_FAIL_IF_FAULT(env);

        char *pstrValue;
        pstrValue = get_string_value(env, value, false);
        XMLRPC_FAIL_IF_FAULT(env);

        // Add field name
        if (i)
            list.append(" AND ");

        if (!strcasecmp(pstrField, "emails"))
        {
            list.append("((");
            if (!add_condition(env, list, "email", pstrOp, pstrValue, field_prefix)) goto cleanup;
            list.append(") OR (");
            if (!add_condition(env, list, "email2", pstrOp, pstrValue, field_prefix)) goto cleanup;
            list.append(") OR (");
            if (!add_condition(env, list, "email3", pstrOp, pstrValue, field_prefix)) goto cleanup;
            list.append("))");
        }
        else if (!strcasecmp(pstrField, "phones"))
        {
            list.append("((");
            if (!add_condition(env, list, "busphone", pstrOp, pstrValue, field_prefix)) goto cleanup;
            list.append(") OR (");
            if (!add_condition(env, list, "homephone", pstrOp, pstrValue, field_prefix)) goto cleanup;
            list.append(") OR (");
            if (!add_condition(env, list, "mobilephone", pstrOp, pstrValue, field_prefix)) goto cleanup;
            list.append(") OR (");
            if (!add_condition(env, list, "otherphone", pstrOp, pstrValue, field_prefix)) goto cleanup;
            list.append("))");
        }
        else
        {
	    if (!add_condition(env, list, pstrField, pstrOp, pstrValue, field_prefix))
	        goto cleanup;
        }
    }

    list.append(") ");
    done = true;

cleanup:
    if (env->fault_occurred)
    {
        done = false;
        list.clear();
    }

    return done;
}

// Build a list of groups (suitable for use in an sql "IN" clause) from input arrays,
// and return output arrays with the group items removed.
bool extract_group_list(xmlrpc_env *env, IString &grouplist,
                        xmlrpc_value *in_conds, xmlrpc_value *in_ops, xmlrpc_value *in_values,
                        xmlrpc_value *out_conds, xmlrpc_value *out_ops, xmlrpc_value *out_values)
{
    bool done = false;
    int size;
	int i;

    size = xmlrpc_array_size(env, in_conds);
    if (size != xmlrpc_array_size(env, in_ops) || size != xmlrpc_array_size(env, in_values))
    {
        xmlrpc_env_set_fault(env, InvalidParameter, "Number of conditions, operators, and values must match");
        goto cleanup;
    }

    for (i=0; i < size; i++)
    {
        // Get field name
        xmlrpc_value *cond = xmlrpc_array_get_item(env, in_conds, i);
        XMLRPC_FAIL_IF_FAULT(env);

        char *pstrField;
        pstrField = get_string_value(env, cond, true);
        XMLRPC_FAIL_IF_FAULT(env);

        // Get operator
        xmlrpc_value *op = xmlrpc_array_get_item(env, in_ops, i);
        XMLRPC_FAIL_IF_FAULT(env);

        char *pstrOp;
        pstrOp = get_string_value(env, op, false);
        XMLRPC_FAIL_IF_FAULT(env);

        // Get comparison value
        xmlrpc_value *value = xmlrpc_array_get_item(env, in_values, i);
        XMLRPC_FAIL_IF_FAULT(env);

        char *pstrValue;
        pstrValue = get_string_value(env, value, false);
        XMLRPC_FAIL_IF_FAULT(env);

        if (!strcasecmp(pstrField, "group"))
        {
            if (!strcasecmp(pstrOp, "="))
            {
                if (grouplist.length() != 0)
                {
                  grouplist.append(",");
                }

                grouplist.append("'");
                grouplist.append(pstrValue);
                grouplist.append("'");
            }
            else
            {
                xmlrpc_env_set_fault(env, InvalidParameter, "Group operator must be '='");
                goto cleanup;
            }
        }
        else
        {
            xmlrpc_array_append_item(env, out_conds, cond);
            XMLRPC_FAIL_IF_FAULT(env);

            xmlrpc_array_append_item(env, out_ops, op);
            XMLRPC_FAIL_IF_FAULT(env);

            xmlrpc_array_append_item(env, out_values, value);
            XMLRPC_FAIL_IF_FAULT(env);
        }
    }

    done = true;

cleanup:
    if (env->fault_occurred)
    {
        done = false;
        grouplist.clear();
    }

    return done;
}

// Find community id for the given community name
void find_communityid_by_name(xmlrpc_env *env, const char *communityname, char *communityid)
{
    ResultSet *rs = NULL;

    strcpy(communityid, "");

    rs = db->execute("find-communityid-by-name", communityname);
    if (!rs || rs->is_error())
        goto cleanup;

    if (rs->get_num_rows() > 0)
    {
        // Get the communityid
        const char *val = rs->get_value(0, 0);
        if (val != NULL)
        {
            strcpy(communityid, val);
        }
        db->closeResults(rs);
    }
    else
    {
        log_error(ZONE, "Unable to find specified community: %s\n", communityname);
        xmlrpc_env_set_fault(env, CommunityNotFound, "Unable to find specified community");
    }

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}

// Find community id for authenticated user
void find_user_community(xmlrpc_env *env, const char *userid, char *buffer)
{
    ResultSet *rs = NULL;

    strcpy(buffer, "-1");

    rs = db->execute("find-user-community", userid);
    if (!rs || rs->is_error())
        goto cleanup;

    if (rs->get_num_rows() > 0)
    {
        // Get the user's communityid
        const char *val = rs->get_value(0, 0);
        if (val != NULL)
        {
            strcpy(buffer, val);
        }
        db->closeResults(rs);

        // Check that the community is enabled
        rs = db->execute("find-community-status", buffer);
        if (!rs || rs->is_error())
            goto cleanup;

        if (rs->get_num_rows() > 0)
        {
            // Get the communities status
            const char *status_s = rs->get_value(0, 0);
            if (status_s != NULL)
            {
                int status = atoi(status_s);

                if (status != AccountStatusEnabled)
                {
                    log_error(ZONE, "The specified community is disabled %s\n", buffer);
                    xmlrpc_env_set_fault(env, CommunityDisabled, "The specified community is disabled");
                }
            }
            db->closeResults(rs);
        }
        else
        {
            log_error(ZONE, "Unable to find community status for this user %s\n", userid);
            xmlrpc_env_set_fault(env, CommunityStatusNotFound, "Unable to find community status for specified user");
        }
    }
    else
    {
        log_error(ZONE, "Unable to find community for this user %s\n", userid);
        xmlrpc_env_set_fault(env, CommunityNotFound, "Unable to find community for specified user");
    }

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}

// Find directory contact belongs to
void find_contact_directory(xmlrpc_env *env, const char *userid, char *buffer)
{
    ResultSet *rs = NULL;

    strcpy(buffer, "-1");

    rs = db->execute("find-contact-directory", userid);
    if (!rs || rs->is_error())
        goto cleanup;

    if (rs->get_num_rows() > 0)
    {
        // Get the contact's directory
        const char *val = rs->get_value(0, 0);
        if (val != NULL)
        {
            strcpy(buffer, val);
        }
        db->closeResults(rs);
    }
    else
    {
        log_error(ZONE, "Unable to find directory for this contact %s\n", userid);
        xmlrpc_env_set_fault(env, DirectoryNotFound, "Unable to find directory for specified contact");
    }

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}

// Find directory group belongs to
void find_group_directory(xmlrpc_env *env, const char *groupid, char *buffer)
{
    ResultSet *rs = NULL;

    strcpy(buffer, "-1");

    rs = db->execute("find-group-directory", groupid);
    if (!rs || rs->is_error())
        goto cleanup;

    if (rs->get_num_rows() > 0)
    {
        // Get the contact's directory
        const char *val = rs->get_value(0, 0);
        if (val != NULL)
        {
            strcpy(buffer, val);
        }
        db->closeResults(rs);
    }
    else
    {
        log_error(ZONE, "Unable to find directory for this group %s\n", groupid);
        xmlrpc_env_set_fault(env, GroupNotFound, "Unable to find directory for specified group");
    }

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}

// Find directory id for personal directory for this user.
void find_user_directory(xmlrpc_env *env, const char *communityid, const char *userid, char *buffer)
{
    ResultSet *rs = NULL;

    strcpy(buffer, "-1");

    rs = db->execute("find-directory", userid, DEFAULT_PERSONAL_DIRECTORY);
    if (!rs || rs->is_error())
        goto cleanup;

    if (rs->get_num_rows() > 0)
    {
        const char *val = rs->get_value(0, 0);
        if (val != NULL)
            strcpy(buffer, val);
        goto cleanup;
    }
    db->closeResults(rs);

    rs = db->execute("allocate-directoryid");
    if (!rs || rs->is_error())
        goto cleanup;
    strcpy(buffer, rs->get_value(0,0));
    db->closeResults(rs);

    // Automatically subscribe user to new directory.
    char typestr[32];
    rs = db->execute("create-directory", buffer, communityid, userid, DEFAULT_PERSONAL_DIRECTORY, i_itoa(dtPersonal,typestr));
    if (!rs || rs->is_error())
        goto cleanup;
    db->closeResults(rs);

    rs = db->execute("subscribe-directory", userid, buffer);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}

// Find directory id for personal directory for this user.
void find_default_group(xmlrpc_env *env, const char *directoryid, char *buffer)
{
    ResultSet *rs = NULL;
    int retry = 2;

    strcpy(buffer, "-1");

    while (retry--)
    {
        rs = db->execute("find-group", directoryid, DEFAULT_GROUP_NAME);
        if (!rs || rs->is_error())
            goto cleanup;

        if (rs->get_num_rows() > 0)
        {
            const char *val = rs->get_value(0, 0);
            if (val != NULL)
                strcpy(buffer, val);
            goto cleanup;
        }
        db->closeResults(rs);

        char typestr[32];
        rs = db->execute("create-group", directoryid, DEFAULT_GROUP_NAME,  i_itoa(dtAll,typestr));
        if (!rs || rs->is_error())
            goto cleanup;
        db->closeResults(rs);
        rs = NULL;
    }

    log_error(ZONE, "Unable to find or create default group for directory id %s\n", directoryid);
    xmlrpc_env_set_fault(env, AddressBookError, "Unable to find default group for user");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}

// Find groupid for the personal directory's buddy group.
// Try to create if not found.
void find_buddy_group(xmlrpc_env *env, const char *directoryid, char *buffer)
{
    ResultSet *rs = NULL;
    int retry = 2;

    strcpy(buffer, "-1");

    while (retry--)
    {
        rs = db->execute("find-group", directoryid, DEFAULT_BUDDY_GROUP_NAME);
        if (!rs || rs->is_error())
            goto cleanup;

        if (rs->get_num_rows() > 0)
        {
            const char *val = rs->get_value(0, 0);
            if (val != NULL)
                strcpy(buffer, val);
            goto cleanup;
        }
        db->closeResults(rs);

        char typestr[32];
        rs = db->execute("create-group", directoryid, DEFAULT_BUDDY_GROUP_NAME,  i_itoa(dtBuddies,typestr));
        if (!rs || rs->is_error())
            goto cleanup;
        db->closeResults(rs);
        rs = NULL;
    }

    log_error(ZONE, "Unable to find or create buddy group for directory id  %s\n", directoryid);
    xmlrpc_env_set_fault(env, AddressBookError, "Unable to find buddy group for user");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}

// Given a communityid, return the community's phone and url
void find_community_info(xmlrpc_env *env,
                         const char *communityid,
                         char *communityphone,
                         char *communityurl)
{
    ResultSet *rs = NULL;
    strcpy(communityphone, "");
    strcpy(communityurl, "");

    rs = db->execute("find-community-info", communityid);
    if (!rs || rs->is_error())
        goto cleanup;

    if (rs->get_num_rows() > 0)
    {
        const char *bridgephone = rs->get_value(0, "BRIDGEPHONE");
        if (bridgephone != NULL)
        {
            strcpy(communityphone, bridgephone);
        }

        const char *systemurl = rs->get_value(0, "SYSTEMURL");
        if (systemurl != NULL)
        {
            strcpy(communityurl, systemurl);
        }

        goto cleanup;
    }

    log_error(ZONE, "Unable to specified community: %s\n", communityid);
    xmlrpc_env_set_fault(env, CommunityNotFound, "Unable to find specified community");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}

// Given the instant meetingid for a given user
void find_instant_meetingid(xmlrpc_env *env,
                            const char *userid,
                            char *meetingid)
{
    ResultSet *rs = NULL;
    strcpy(meetingid, "");

    rs = db->execute("find-instant-meeting-by-host", userid);
    if (!rs || rs->is_error())
        goto cleanup;

    if (rs->get_num_rows() > 0)
    {
        const char *s_meetingid = rs->get_value(0, "MEETINGID");
        if (s_meetingid != NULL)
        {
            strcpy(meetingid, s_meetingid);
        }
        goto cleanup;
    }

    log_error(ZONE, "Unable to find instant meeting for user: %s\n", userid);
    xmlrpc_env_set_fault(env, InstantMeetingNotFound, "Unable to find instant meeting for user");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}

void find_user_passwd(xmlrpc_env *env,
                            const char *userid,
                            char *passwd)
{
    ResultSet *rs = NULL;
    *passwd = 0;

    rs = db->execute("find-screenname-passwd-by-userid", userid);
    if (!rs || rs->is_error())
        goto cleanup;

    if (rs->get_num_rows() > 0)
    {
        char *pw = (char*)rs->get_value(0, "PASSWORD");
        if (pw != NULL)
        {
            char *real_pw = shahash(pw);
            strcpy(passwd, real_pw);
        }
        goto cleanup;
    }

    log_error(ZONE, "Unable to find password for user: %s\n", userid);
    xmlrpc_env_set_fault(env, Failed, "Unable to find password for user");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}

void find_instant_participant_pin(xmlrpc_env *env,
                            const char *meetingid,
                            char *pin)
{
    ResultSet *rs = NULL;
    *pin = 0;

    rs = db->execute("find-participants", meetingid);
    if (!rs || rs->is_error())
        goto cleanup;

    if (rs->get_num_rows() > 0)
    {
        const char *partid = rs->get_value(0, "PARTICIPANTID");
        if (partid != NULL)
        {
            strcpy(pin, partid);
        }
        goto cleanup;
    }

    log_error(ZONE, "Unable to find participant pin for meeting: %s\n", meetingid);
    xmlrpc_env_set_fault(env, Failed, "Unable to find participant pin for instant meeting");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}

// Try to find an email for the specified userid
//
// Returns true if an email was found.  Env parameter will indicate a fault if
// database access fails or returns an error.
//
bool find_email_by_userid(xmlrpc_env *env, const char *userid, char *email_address)
{
    ResultSet *rs = NULL;
    strcpy(email_address, "");

    rs = db->execute("find-emails-by-userid", userid);
    if (!rs || rs->is_error())
        goto cleanup;

    if (rs->get_num_rows() > 0)
    {
        for (int loop=0; loop < 3 && (strlen(email_address) == 0); loop++)
        {
            const char *s_email = rs->get_value(0, loop);
            if (s_email != NULL && (strlen(s_email) != 0))
            {
                strcpy(email_address, s_email);
            }
        }
    }

    log_debug(ZONE, "Returning email '%s' for user %s", email_address, userid);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }

    return (strlen(email_address) != 0);
}


// Check if Jabber user id matches session id
// Return true if this request if from an internal system service, false if user session.
bool check_session(xmlrpc_env *env, void *rpc_session, const char *session_id)
{
    const char * from_jid = rpc_get_from((rpc_session_t)rpc_session);

    // Allow trusted system services
    if (strstr(from_jid, ".service@"))
        return true;

    IUseMutex lock(sessions_lock);
    IString temp(from_jid);

    UserSession *sess = (UserSession*)user_sessions.get(&temp);
    if (sess == NULL || session_id == NULL || sess->user_id != session_id)
    {
        xmlrpc_env_set_fault(env, InvalidSessionID, "Session ID doesn't match XMPP session");
    }

    return false;
}

UserSession * lookup_session(xmlrpc_env *env, void *rpc_session)
{
    const char * from_jid = rpc_get_from((rpc_session_t)rpc_session);

    IUseMutex lock(sessions_lock);
    IString temp(from_jid);
    UserSession *session = (UserSession*)user_sessions.get(&temp);

    if (session == NULL)
        xmlrpc_env_set_fault(env, InvalidSessionID, "User session not found");

    return session;
}

// See if session belongs to admin user.
int check_admin(xmlrpc_env *env, const char * sid, const char * community_id)
{
    int is_admin = 0;

    ResultSet *rs = NULL;

    rs = db->execute("find-user", sid);
    if (!rs || rs->is_error())
        goto cleanup;

    if (rs->get_num_rows() > 0)
    {
        const char *admin = rs->get_value(0, 0);
        const char *superadmin = rs->get_value(0, 2);
        const char *community = rs->get_value(0, 3);
        if (admin != NULL && superadmin != NULL && community != NULL)
        {
            if (!strcmp(admin, "t") && (!strcmp(superadmin, "t") || !strcmp(community_id, community) || community_id == NULL))
                is_admin = 1;
        }
        goto cleanup;
    }

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }

    return is_admin;
}

// See if session belongs to superadmin user.
int check_superadmin(xmlrpc_env *env, const char * sid)
{
    int is_superadmin = 0;

    ResultSet *rs = NULL;

    rs = db->execute("find-admin", sid);
    if (!rs || rs->is_error())
        goto cleanup;

    if (rs->get_num_rows() > 0)
    {
        const char *val = rs->get_value(0, 1);
        if (val != NULL)
            if (!strcmp(val, "t"))
                is_superadmin = 1;
        goto cleanup;
    }

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }

    return is_superadmin;
}

// See if session belongs to superadmin user.
int check_communityadmin(xmlrpc_env *env, const char *sid)
{
    // TODO: TBD when user records contain communityids...
    return 0;
}

// See if the specified community is the "system" community
int check_system_community(xmlrpc_env *env, const char *communityid)
{
    int is_system = 0;

    ResultSet *rs = NULL;

    rs = db->execute("find-community-type", communityid);
    if (!rs || rs->is_error())
        goto cleanup;

    if (rs->get_num_rows() > 0)
    {
        const char *val = rs->get_value(0, 0);

        if (val != NULL)
        {
            if (atoi(val) == AccountTypeSystem)
            {
                is_system = 1;
            }
        }
    }

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }

    return is_system;
}

// See if session has access to specified community and directory.
// If "allow_cab" is true, user will be granted access to his community address book even if not admin.
// Returns true if directory is community address book, false if personal address book.
bool check_directory(xmlrpc_env *env, const char *sid, const char * community_id, const char *directory_id, bool allow_cab)
{
    bool is_global = false;

    ResultSet *rs = NULL;

    rs = db->execute("find-directory-info", directory_id);
    if (!rs || rs->is_error())
        goto cleanup;

    if (rs->get_num_rows() > 0)
    {
        //  COMMUNITYID, OWNERID, TYPE
        const char *db_cid = rs->get_value(0, 0);
        const char *db_owner = rs->get_value(0, 1);
        const char *db_type = rs->get_value(0, 2);

        if (db_type != NULL)
        {
            if (atoi(db_type) == dtGlobal)
            {
                is_global = true;
            }
        }

        if (community_id && strcmp(community_id, db_cid))
        {
            xmlrpc_env_set_fault(env, CommunityNotFound, "Requesting user doesn't belong to community");
            goto cleanup;
        }

        if (is_global)
        {
            // Make sure user is administrator of this community
            if (!allow_cab && !check_admin(env, sid, db_cid))
            {
                xmlrpc_env_set_fault(env, NotAuthorized, "You must be an administrator to update the global directory");
                goto cleanup;
            }
        }
        else
        {
            // Make sure user owns this directory
            if (strcmp(sid, db_owner))
            {
                xmlrpc_env_set_fault(env, NotAuthorized, "Requesting user doesn't own directory");
                goto cleanup;
            }
        }
    }
    else
    {
        xmlrpc_env_set_fault(env, DirectoryNotFound, "Directory not found");
        goto cleanup;
    }

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }

    return is_global;
}

// See if session has read access to specified directories
void check_directories(xmlrpc_env *env, const char *sid, IString& dirlist, int dircount)
{
    ResultSet *rs = NULL;

    rs = db->execute("check-directory-access", sid, (const char *)dirlist);
    if (!rs || rs->is_error())
        goto cleanup;

    if (rs->get_num_rows() < dircount)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "Directory not accessible");
        goto cleanup;
    }

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}


#if 0

// See if the specified directory is a GAL
bool is_global_directory(xmlrpc_env *env, char *directoryid)
{
    bool is_global = false;

    ResultSet *rs = NULL;

    rs = db->execute("find-directory-type", directoryid);
    if (!rs || rs->is_error())
        goto cleanup;

    if (rs->get_num_rows() > 0)
    {
        const char *val = rs->get_value(0, 0);

        if (val != NULL)
        {
            if (atoi(val) == dtGlobal)
            {
                is_global = true;
            }
        }
    }

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }

    return is_global;
}

#endif

// Make sure screen name will be acceptable to jabber.
bool validate_screen_name(const char * screen)
{
    bool valid = screen != NULL && strlen(screen) > 0 && strlen(screen) < 65;

    if (strstr(screen, ".service") != NULL ||
        strstr(screen, ".iic") != NULL ||
        strstr(screen, "anonymous") != NULL)
        valid = false;

    for (unsigned char *character = (unsigned char *)screen; valid && (*character != '\0'); character++)
    {
        if (*character <= 32   ||
            *character == '@'  ||
            *character == ':'  ||
            *character == '\'' ||
            *character == '"'  ||
            *character == '<'  ||
            *character == '>'  ||
            *character == '&'  ||
            *character == '\n' ||
            *character == '\t' ||
            *character == '\r')
        {
            valid = false;
        }
    }

    return valid;
}

// Make sure password is not too long for database
bool validate_password(const char * password)
{
    return strlen(password) < 129;
}

// Depending on specified contact type, we may also need to create a record in the
// user table.
void setup_user(xmlrpc_env * env, const char * sid, const char * type, const char * userid, const char * username,
				const char * screen, const char * pwd, const char * communityid, const char * server)
{
    ResultSet *rs = NULL;
    int type_code = atoi(type);
    char superadmin[32];
    char hash_pwd[41];

    // Make sure this is an administrator.
    if (!check_admin(env, sid, communityid))
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "You must be an administrator to update the global directory");
        goto cleanup;
    }

    strcpy(hash_pwd, pwd);

    // Validate username and password if necessary
    if (type_code == UserTypeUser || type_code == UserTypeAdmin)
    {
        if (!validate_screen_name(screen))
        {
            xmlrpc_env_set_fault(env, InvalidScreenName, "Invalid Screen Name");
            goto cleanup;
        }

        if (!validate_password(pwd))
        {
            xmlrpc_env_set_fault(env, InvalidScreenPassword, "Invalid Password");
            goto cleanup;
        }

        // If password was specified, make sure it is hashed before storing
        if (strlen(pwd) > 0 && !check_hash(pwd))
            shahash_r(pwd, hash_pwd);
    }

    // Add, update, or remove user record depending on type of contact.
    switch (type_code)
    {
        case 0:
            // Normal contact
            rs = db->execute("remove-user", userid);
            if (!rs || rs->is_error())
                goto cleanup;
            break;
        case 1:
            // System user
            if (strlen(hash_pwd) > 0)
                rs = db->execute("modify-user-password", userid, screen, hash_pwd, "false", "false");
            else
                rs = db->execute("modify-user", userid, screen, "false", "false");
            if (!rs || rs->is_error())
                goto cleanup;
            if (rs->get_num_affected() == 0)
            {
                db->closeResults(rs);

                // verify username to be added unique
                rs = db->execute("find-user-by-name", (const char*) username);
                if (!rs || rs->is_error())
                    goto cleanup;
                if (rs->get_num_rows() > 0)
                {
                    xmlrpc_env_set_fault(env, DuplicateUsername, "User with same name already exists");
                    goto cleanup;
                }
                db->closeResults(rs);

                // add the user
                rs = db->execute("add-user", userid, username, screen, hash_pwd, communityid, "false", "false");
                if (!rs || rs->is_error())
                    goto cleanup;
                db->closeResults(rs);

                // subscribe the user to the GAL directory
                rs = db->execute("subscribe-to-gal", userid, communityid);
                if (!rs || rs->is_error())
                    goto cleanup;
            }
            break;
        case 2:
            // Admin or SuperAdmin
            if (check_system_community(env, communityid))
                strcpy(superadmin, "true");
            else
                strcpy(superadmin, "false");

            if (strlen(hash_pwd) > 0)
                rs = db->execute("modify-user-password", userid, screen, hash_pwd, "true", superadmin);
            else
                rs = db->execute("modify-user", userid, screen, "true", superadmin);
            if (!rs || rs->is_error())
                goto cleanup;
            if (rs->get_num_affected() == 0)
            {
                db->closeResults(rs);

                // verify username to be added unique
                rs = db->execute("find-user-by-name", (const char*) username);
                if (!rs || rs->is_error())
                    goto cleanup;
                if (rs->get_num_rows() > 0)
                {
                    xmlrpc_env_set_fault(env, DuplicateUsername, "User with same name already exists");
                    goto cleanup;
                }
                db->closeResults(rs);

                // add the user
                rs = db->execute("add-user", userid, username, screen, hash_pwd, communityid, "true", superadmin);
                if (!rs || rs->is_error())
                    goto cleanup;
                db->closeResults(rs);

                // subscribe the user to the GAL directory
                rs = db->execute("subscribe-to-gal", userid, communityid);
                if (!rs || rs->is_error())
                    goto cleanup;
            }

            break;
        default:
            xmlrpc_env_set_fault(env, InvalidParameter, "Invalid contact type specified for added or updated contact");
    }

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}

// set lastmodified time for group to current time
void set_group_modified(xmlrpc_env *env, const char *groupid)
{
    ResultSet *rs = NULL;

    rs = db->execute("set-group-modified", groupid);
    if (!rs || rs->is_error())
        goto cleanup;

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            log_error(ZONE, "Failed to update last modified time for group %s", groupid);
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}

// Add user/contact to specified group
void add_user_to_group(xmlrpc_env *env, const char *groupid, const char *userid)
{
    ResultSet *rs = NULL;

    rs = db->execute("add-to-group", groupid, userid);
    if (!rs || rs->is_error())
        goto cleanup;

    set_group_modified(env, groupid);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}

// checks the client update file to make sure cache is up to date
// returns false if error
bool get_client_update_info()
{
    struct stat buf;
    int stat_result;
    char seps[] = "=;";
    char seps2[] = ",";
    char version_copy[80];

    stat_result = stat(g_update_client_info.file_name, &buf);
    if (stat_result == -1) // error
    {
        log_error(ZONE, "check_version: couldn't stat %s : %s\n", (const char *)g_update_client_info.file_name, strerror(errno));
        return false;
    }
    if (difftime(buf.st_mtime, g_update_client_info.last_modified) != 0.0)
    {
        // open file
        FILE *f;

        f = fopen(g_update_client_info.file_name, "r");
        if (f == NULL)
        {
            log_error(ZONE, "check_version: couldn't open %s for reading: %s\n", (const char *)g_update_client_info.file_name, strerror(errno));
            return false;
        }

        // read in version, message, and url
        g_update_client_info.type = UpgradeNotFound;
        char *token;

        char read_line[256];
        char *parse_buff = NULL;

        while (fgets(read_line, sizeof(read_line), f) != NULL)
        {
            token = strtok_r(read_line, seps, &parse_buff);
            if (strcmp(token, "UPGRADE_VERSION") == 0)
            {
                token = strtok_r(NULL, seps, &parse_buff);
                g_update_client_info.version = IString(token);
            }
            else if (strcmp(token, "UPGRADE_MSG") == 0)
            {
                token = strtok_r(NULL, seps, &parse_buff);
                g_update_client_info.message = IString(token);
            }
            else if (strcmp(token, "UPGRADE_URL") == 0)
            {
                token = strtok_r(NULL, seps, &parse_buff);
                g_update_client_info.installer_url = IString(token);
            }
            else if (strcmp(token, "UPGRADE_TYPE") == 0)
            {
                token = strtok_r(NULL, seps, &parse_buff);
                if (strcmp(token, "MANDATORY") == 0)
                    g_update_client_info.type = UpgradeMandatory;
                else if (strcmp(token, "OPTIONAL") == 0)
                    g_update_client_info.type = UpgradeOptional;
            }
        }

        fclose(f);

        // now parse version into major, minor, build, platform
        strcpy(version_copy,(const char*)g_update_client_info.version);

        // major
        token = strtok_r(version_copy, seps2, &parse_buff);
        if (token != NULL)
            g_update_client_info.major = atoi(token);
        else
        {
            log_error(ZONE, "check_version: error parsing major number\n");
            return false;
        }

        // minor
        token = strtok_r(NULL, seps2, &parse_buff);
        if (token != NULL)
            g_update_client_info.minor = atoi(token);
        else
        {
            log_error(ZONE, "check_version: error parsing minor number\n");
            return false;
        }

        // build
        token = strtok_r(NULL, seps2, &parse_buff);
        if (token != NULL)
            g_update_client_info.build = atoi(token);
        else
        {
            log_error(ZONE, "check_version: error parsing build number\n");
            return false;
        }

        // platform
        token = strtok_r(NULL, seps2, &parse_buff);
        if (token != NULL)
            g_update_client_info.platform = atoi(token);
        else
        {
            log_error(ZONE, "check_version: error parsing platform number\n");
            return false;
        }

        g_update_client_info.last_modified = buf.st_mtime;
    }

    return true;
}

// Verify that the client version is up to date.  Return info about
// possible upgrades.
// Parameters:
//   CLIENTVERSION
//
// Returns:
//   UPGRADEFOUND, UPGRADEURL, UPGRADEMESSAGE, API_VERSION
//   UPGRADEFOUND: 1 = mandatory upgrade found, 2 = optional upgrade found, 0 = not found
xmlrpc_value *check_version(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *client_version;
    char *token;
    const char *seps = ",";
    int found = UpgradeNotFound;
    int major, minor, build, platform;
    xmlrpc_value *output = NULL;
	char *parse_buff = NULL;

    xmlrpc_parse_value(env, param_array, "(s)", &client_version);
    if (env->fault_occurred)
    {
		xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to addressbk.check_version");
        goto cleanup;
    }

    // read server's client version file
    if (g_update_client_info.file_name.length() == 0)
    {
        log_error(ZONE, "check_version: zero length upgrade file name\n");
        xmlrpc_env_set_fault(env, Failed, "Unable to read client version file parameter");
        goto cleanup;
    }

    if (!get_client_update_info())
    {
        xmlrpc_env_set_fault(env, Failed, "Unable to read upgrade file for addressbk.check_version");
        goto cleanup;
    }

    // compare server version number to passed client version
    // major
    token = strtok_r(client_version, seps, &parse_buff);
    if (token != NULL)
        major = atoi(token);
    else
    {
        xmlrpc_env_set_fault(env, Failed, "Error parsing major number");
        goto cleanup;
    }

    // minor
    token = strtok_r(NULL, seps, &parse_buff);
    if (token != NULL)
        minor = atoi(token);
    else
    {
        xmlrpc_env_set_fault(env, Failed, "Error parsing minor number");
        goto cleanup;
    }

    // build
    token = strtok_r(NULL, seps, &parse_buff);
    if (token != NULL)
        build = atoi(token);
    else
    {
        xmlrpc_env_set_fault(env, Failed, "Error parsing build number");
        goto cleanup;
    }

    // platform
    token = strtok_r(NULL, seps, &parse_buff);
    if (token != NULL)
        platform = atoi(token);
    else
    {
        xmlrpc_env_set_fault(env, Failed, "Error parsing platform number");
        goto cleanup;
    }

    // compare versions.  this algorithm will not require an upgrade
    // if for some reason the version on the server is older than
    // the client version.
    if (g_update_client_info.major > major ||
        (g_update_client_info.major == major && g_update_client_info.minor > minor) ||
        (g_update_client_info.major == major && g_update_client_info.minor == minor && g_update_client_info.build > build) ||
        (g_update_client_info.major == major && g_update_client_info.minor == minor && g_update_client_info.build == build && g_update_client_info.platform > platform))
    {
        found = g_update_client_info.type;
    }

    // read in message to return
    // Create return value.
    output = xmlrpc_build_value(env, "(issi)", found,
                                (const char*) g_update_client_info.installer_url,
                                (const char*) g_update_client_info.message, API_VERSION);
    XMLRPC_FAIL_IF_FAULT(env);


cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to check client version");

    return output;
}


// Check for user in database.  Return significant info about user.
// Parameters:
//   SCREENNAME, PASSWORD, COMMUNITYNAME
//
// NOTE: COMMUNITYNAME is only used by superadmins to authenticate into
//       any community in the system...
//
// Returns:
//   SESSIONID, USERID, USERNAME, COMMUNITYID,
//   DIRECTORYID (for personal directory), DEFAULTGROUPID (for personal directory),
//   USERTYPE, ISSUPERADMIN, ENABLEDFEATURES, PROFILEOPTIONS
//   DEFPHONE, BUSPHONE, HOMEPHONE, MOBILEPHONE, OTHERPHONE,
//   DEFMEETING, FIRST, LAST,
//   SYSTEMPHONE (from community), SYSTEMURL (from community),
//   If returned USERID is empty string, user was not found or password is incorrect.
xmlrpc_value *authenticate_user(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    ResultSet *rs = NULL;
    char *screen, *password, *communityname;
    xmlrpc_value *output = NULL;
    bool bSucceeded = false;
    char hash_password[41];

    xmlrpc_parse_value(env, param_array, "(sss)", &screen, &password, &communityname);
    if (env->fault_occurred)
    {
		xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to addressbk.authenticate_user");
        goto cleanup;
    }

    if (strstr(screen, "anonymous@"))
    {
		xmlrpc_env_set_fault(env, NotAuthorized, "Anonymous user not allowed");
        goto cleanup;
    }

    // Hash user supplied password if needed.
    if (*password == '\0' || check_hash(password))
        strcpy(hash_password, password);
    else
        shahash_r(password, hash_password);

    // Fetch one more row then requested so we can report on wether more data is available
    rs = db->execute("authenticate-user", screen);
	if (!rs || rs->is_error())
       goto cleanup;

    if (rs->get_num_rows() > 0)
    {
        const char *user = rs->get_value(0, "USERID");
        const char *username = rs->get_value(0, "USERNAME");
        const char *fetched_password = rs->get_value(0, "PASSWORD");
        char communityid[33];
        char communityphone[257];
        char communityurl[257];
        char dir[32];
        char group[32];
        char buddygroup[32];
        char meetingid[32];
        char hash_fetched[41];
        const char * from_jid = rpc_get_from((rpc_session_t)user_data);

        // If authentication request doesn't match jabber session, check password,
        // otherwise assume Jabber has checked password already.
        // Note that we can't always check user password as we may be checking against
        // an LDAP directory.
        if (strcmp(from_jid, username) != 0)
        {
            // Hash fetched password if necessary
            if (check_hash(fetched_password))
                strcpy(hash_fetched, fetched_password);
            else
                shahash_r(fetched_password, hash_fetched);

            // Compare incoming password with db password
            if (strcmp(hash_fetched, hash_password) != 0)
                goto done;
        }

        // clear name cache
        name_cache.clear(user);

        // Find the communityid for the authenticating user.
        // Usually this is simply the community in which the user
        // resides, but superadmins may provide the name of any
        // community that they wish to log in to...
        strcpy(communityid, "");

        if ((strlen(communityname) > 0) && check_superadmin(env, user))
        {
            find_communityid_by_name(env, communityname, communityid);
            XMLRPC_FAIL_IF_FAULT(env);
        }
        else
        {
            find_user_community(env, user, communityid);
            XMLRPC_FAIL_IF_FAULT(env);
        }

        if (strlen(communityid) > 0)
        {
            // Now find remaining user information
            find_community_info(env, communityid, communityphone, communityurl);
            XMLRPC_FAIL_IF_FAULT(env);

            find_user_directory(env, communityid, user, dir);
            XMLRPC_FAIL_IF_FAULT(env);

            find_default_group(env, dir, group);
            XMLRPC_FAIL_IF_FAULT(env);

            find_buddy_group(env, dir, buddygroup);
            XMLRPC_FAIL_IF_FAULT(env);

            find_instant_meetingid(env, user, meetingid);
            XMLRPC_FAIL_IF_FAULT(env);

            const char *superadmin = rs->get_value(0, "SUPERADMIN");
            const char *type = rs->get_value(0, "TYPE");
            const char *enabled = rs->get_value(0, "ENABLEDFEATURES");
            const char *profileoptions = rs->get_value(0, "PROFILEOPTIONS");
		    const char *defphone = rs->get_value(0, "DEFPHONE");
		    const char *busphone = rs->get_value(0, "BUSPHONE");
		    const char *homephone = rs->get_value(0, "HOMEPHONE");
		    const char *mobilephone = rs->get_value(0, "MOBILEPHONE");
		    const char *otherphone = rs->get_value(0, "OTHERPHONE");
//		    const char *defmeeting = rs->get_value(0, "DEFMEETING");
		    const char *first = rs->get_value(0, "FIRSTNAME");
		    const char *last = rs->get_value(0, "LASTNAME");

            // Create return value.
            output = xmlrpc_build_value(env, "(ssssssisisisssssssss)", user, user, username, communityid, dir, group, atoi(type), superadmin, atoi(enabled), profileoptions,
						    atoi(defphone), busphone, homephone, mobilephone, otherphone, meetingid,
						    first, last, communityphone, communityurl);
            XMLRPC_FAIL_IF_FAULT(env);

            IUseMutex lock(sessions_lock);
            user_sessions.insert(new IString(from_jid), new UserSession(user,communityid,from_jid,screen,password));
            log_debug(ZONE, "Associate jabber id %s with session %s", from_jid, user);

            bSucceeded = true;
        }
    }

done:
    if (!bSucceeded)
    {
        output = xmlrpc_build_value(env, "(ssssssisisisssssssss)", "", "", "", "", "", "", 0, "", 0, "", 0, "", "", "", "", "", "", "", "", "");
        XMLRPC_FAIL_IF_FAULT(env);
    }

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Find user info
// Parameters:
//   SESSIONID, USERID
// Returns:
//   COMMUNITYID, SCREENNAME, IS_ADMIN (T|F), IS_SUPERADMIN (T|F),
//   USERNAME, FIRSTNAME, LASTNAME, DEFPHONE, BUSPHONE, HOMEPHONE,
//	 MOBILEPHONE, OTHERPHONE, EXTENSION, DEFEMAIL, EMAIL, EMAIL2, EMAIL3
//   ENABLEDFEATURES
xmlrpc_value *find_user(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *userid;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    IString fieldstr;
    bool is_service;

	xmlrpc_parse_value(env, param_array, "(ss)", &sid, &userid);
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!is_service)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "API not valid for user sessions");
        XMLRPC_FAIL_IF_FAULT(env);
    }

    rs = db->execute("find-user-info", userid);
    if (!rs || rs->is_error())
        goto cleanup;

	if (rs->get_num_rows() > 0)
	{
		const char *communityid = rs->get_value(0, "COMMUNITYID");
		const char *screenname = rs->get_value(0, "SCREENNAME");
		const char *admin = rs->get_value(0, "ADMIN");
		const char *superadmin = rs->get_value(0, "SUPERADMIN");
		const char *username = rs->get_value(0, "USERNAME");
		const char *first = rs->get_value(0, "FIRSTNAME");
		const char *last = rs->get_value(0, "LASTNAME");
		const char *defphone = rs->get_value(0, "DEFPHONE");
		const char *busphone = rs->get_value(0, "BUSPHONE");
		const char *homephone = rs->get_value(0, "HOMEPHONE");
		const char *mobilephone = rs->get_value(0, "MOBILEPHONE");
		const char *otherphone = rs->get_value(0, "OTHERPHONE");
		const char *ext = rs->get_value(0, "EXTENSION");
		const char *defemail = rs->get_value(0, "DEFEMAIL");
		const char *email1 = rs->get_value(0, "EMAIL");
		const char *email2 = rs->get_value(0, "EMAIL2");
		const char *email3 = rs->get_value(0, "EMAIL3");
		const char *enabledfeatures = rs->get_value(0, "ENABLEDFEATURES");

        // Create return value.
        output = xmlrpc_build_value(env, "(sssssssisssssisssi)",
						communityid, screenname, admin, superadmin, username, first, last,
						atoi(defphone), busphone, homephone, mobilephone, otherphone, ext,
						atoi(defemail), email1, email2, email3,
                        atoi(enabledfeatures));
        XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, InvalidUserID, "User not found");
		goto cleanup;
	}

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Find user info
// Parameters:
//   SESSIONID, PIN, PASSWORD
// Returns:
//   COMMUNITYID, SCREENNAME, IS_ADMIN (T|F), IS_SUPERADMIN (T|F),
//   USERNAME, FIRSTNAME, LASTNAME, DEFPHONE, BUSPHONE, HOMEPHONE,
//	 MOBILEPHONE, OTHERPHONE, EXTENSION, DEFEMAIL, EMAIL, EMAIL2, EMAIL3
//   ENABLEDFEATURES
xmlrpc_value *find_user_by_pin(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *pin, *password;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    IString fieldstr;
    bool is_service;
    char hash_password[41];

	xmlrpc_parse_value(env, param_array, "(sss)", &sid, &pin, &password);
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!is_service)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "API not valid for user sessions");
        XMLRPC_FAIL_IF_FAULT(env);
    }

    // Hash passwords if needed.
    if (check_hash(password))
        strcpy(hash_password, password);
    else
        shahash_r(password, hash_password);

    rs = db->execute("find-user-by-pin", pin);
    if (!rs || rs->is_error())
        goto cleanup;

	if (rs->get_num_rows() > 0)
	{
		const char *communityid = rs->get_value(0, "COMMUNITYID");
		const char *screenname = rs->get_value(0, "SCREENNAME");
		const char *admin = rs->get_value(0, "ADMIN");
		const char *superadmin = rs->get_value(0, "SUPERADMIN");
		const char *username = rs->get_value(0, "USERNAME");
		const char *first = rs->get_value(0, "FIRSTNAME");
		const char *last = rs->get_value(0, "LASTNAME");
		const char *defphone = rs->get_value(0, "DEFPHONE");
		const char *busphone = rs->get_value(0, "BUSPHONE");
		const char *homephone = rs->get_value(0, "HOMEPHONE");
		const char *mobilephone = rs->get_value(0, "MOBILEPHONE");
		const char *otherphone = rs->get_value(0, "OTHERPHONE");
		const char *ext = rs->get_value(0, "EXTENSION");
		const char *defemail = rs->get_value(0, "DEFEMAIL");
		const char *email1 = rs->get_value(0, "EMAIL");
		const char *email2 = rs->get_value(0, "EMAIL2");
		const char *email3 = rs->get_value(0, "EMAIL3");
		const char *enabledfeatures = rs->get_value(0, "ENABLEDFEATURES");
		const char *fetched_password = rs->get_value(0, "PASSWORD");

        char hash_fetched[41];

        // Hash fetched password if necessary
        if (check_hash(fetched_password))
            strcpy(hash_fetched, fetched_password);
        else
            shahash_r(fetched_password, hash_fetched);

        // Compare incoming password with db password
        if (strcmp(hash_fetched, hash_password) != 0)
        {
            xmlrpc_env_set_fault(env, UserNotFound, "User not found");
            goto cleanup;
        }

        // Create return value.
        output = xmlrpc_build_value(env, "(sssssssisssssisssi)",
						communityid, screenname, admin, superadmin, username, first, last,
						atoi(defphone), busphone, homephone, mobilephone, otherphone, ext,
						atoi(defemail), email1, email2, email3,
                        atoi(enabledfeatures));
        XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, UserNotFound, "User not found");
		goto cleanup;
	}

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Change user password.
// Parameters:
//   SESSIONID, USERID, OLDPASSWORD, NEWPASSWORD, ADMINOVERIDE
// Returns:
//   Changed, 1 if changed, 0 if failed.
xmlrpc_value *set_password(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    ResultSet *rs = NULL;
    char *sid, *userid, *old, *password;
    int overide;
    xmlrpc_value *output = NULL;
    char hash_password[41];
    char hash_old[41];

    xmlrpc_parse_value(env, param_array, "(ssssi)", &sid, &userid, &old, &password, &overide);
    if (env->fault_occurred)
    {
		xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to addressbk.change_password");
        goto cleanup;
    }

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    // Hash passwords if needed.
    if (check_hash(password))
        strcpy(hash_password, password);
    else
        shahash_r(password, hash_password);
    if (check_hash(old))
        strcpy(hash_old, old);
    else
        shahash_r(old, hash_old);

    if (overide)
    {
        char communityid[64] = "";

        find_user_community(env, userid, communityid);
        XMLRPC_FAIL_IF_FAULT(env);

        if (!check_admin(env, sid, communityid))
        {
    		xmlrpc_env_set_fault(env, NotAuthorized, "Only admin can change password without current password");
            goto cleanup;
        }
        rs = db->execute("admin-set-password", userid, hash_password);
    }
    else
        rs = db->execute("set-password", userid, hash_old, hash_password);

    if (!rs || rs->is_error())
       goto cleanup;

    output = xmlrpc_build_value(env, "(i)", rs->get_num_affected());
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Set user default phone.
// Parameters:
//   SESSIONID, USERID, DEFAULTPHONE
// Returns:
//   Changed, 1 if changed, 0 if failed.
xmlrpc_value *set_default_phone(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    ResultSet *rs = NULL;
    char *sid, *userid;
    int default_phone;
    xmlrpc_value *output = NULL;
	char buff[32];
    char communityid[64];

    xmlrpc_parse_value(env, param_array, "(ssi)", &sid, &userid, &default_phone);
    if (env->fault_occurred)
    {
		xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to addressbk.set_default_phone");
        goto cleanup;
    }

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

	if (default_phone < SelPhoneNone || default_phone > SelPhonePC)
	{
		xmlrpc_env_set_fault(env, InvalidParameter, "Invalid default phone selected");
		goto cleanup;
	}

    if (strcmp(userid, sid))
    {
        find_user_community(env, userid, communityid);
        XMLRPC_FAIL_IF_FAULT(env);

        if (!check_admin(env, sid, communityid))
        {
    		xmlrpc_env_set_fault(env, NotAuthorized, "Only admin can modify another user");
            goto cleanup;
        }
    }

    rs = db->execute("set-default-phone", userid, i_itoa(default_phone, buff));
    if (!rs || rs->is_error())
       goto cleanup;

    output = xmlrpc_build_value(env, "(i)", rs->get_num_affected());
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Set user default meeting.
// Parameters:
//   SESSIONID, USERID, DEFMEETINGID
// Returns:
//   Changed, 1 if changed, 0 if failed.
xmlrpc_value *set_default_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    ResultSet *rs = NULL;
    char *sid, *userid, *def_meeting;
    xmlrpc_value *output = NULL;
    char communityid[64];

    xmlrpc_env_set_fault(env, Failed, "API not available");
    XMLRPC_FAIL_IF_FAULT(env);

    xmlrpc_parse_value(env, param_array, "(sss)", &sid, &userid, &def_meeting);
    if (env->fault_occurred)
    {
		xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to addressbk.set_default_meeting");
        goto cleanup;
    }

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (strcmp(userid, sid))
    {
        find_user_community(env, userid, communityid);
        XMLRPC_FAIL_IF_FAULT(env);

        if (!check_admin(env, sid, communityid))
        {
    		xmlrpc_env_set_fault(env, NotAuthorized, "Only admin can modify another user");
            goto cleanup;
        }
    }

    rs = db->execute("set-default-meeting", userid, def_meeting);
    if (!rs || rs->is_error())
       goto cleanup;

    output = xmlrpc_build_value(env, "(i)", rs->get_num_affected());
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Set user default email.
// Parameters:
//   SESSIONID, USERID, DEFAULTEMAIL
// Returns:
//   Changed, 1 if changed, 0 if failed.
xmlrpc_value *set_default_email(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    ResultSet *rs = NULL;
    char *sid, *userid;
    int default_email;
    xmlrpc_value *output = NULL;
    char communityid[64];

    xmlrpc_env_set_fault(env, Failed, "API not available");
    XMLRPC_FAIL_IF_FAULT(env);

    xmlrpc_parse_value(env, param_array, "(ssi)", &sid, &userid, &default_email);
    if (env->fault_occurred)
    {
		xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to addressbk.set_default_email");
        goto cleanup;
    }

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    rs = db->execute("set-default-email", userid, default_email);
    if (!rs || rs->is_error())
       goto cleanup;

	if (default_email < SelEmailNone || default_email > SelEmail3)
	{
		xmlrpc_env_set_fault(env, InvalidParameter, "Invalid default email selected");
		goto cleanup;
	}

    if (strcmp(userid, sid))
    {
        find_user_community(env, userid, communityid);
        XMLRPC_FAIL_IF_FAULT(env);

        if (!check_admin(env, sid, communityid))
        {
    		xmlrpc_env_set_fault(env, NotAuthorized, "Only admin can modify another user");
            goto cleanup;
        }
    }

    output = xmlrpc_build_value(env, "(i)", rs->get_num_affected());
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

// checks the new user template file to make sure cache is up to date.
// stores in memory if changed.
// returns false if error (can't find file, can't open).
bool get_new_user_template_info()
{
    struct stat buf;
    int stat_result;
    int fread_size;

    //
    // tbd ... should (linux only) do flock on file prior to stat
    //

    stat_result = stat(g_new_user_template_info.file_name, &buf);
    if (stat_result == -1) // error
    {
        log_error(ZONE, "add_user: couldn't stat %s : %s\n", (const char *)g_new_user_template_info.file_name, strerror(errno));
        return false;
    }

    if (difftime(buf.st_mtime, g_new_user_template_info.last_modified) != 0.0)
    {
        long int fsize;

        // free previous memory for file storage
        if (g_new_user_template_info.buf != NULL)
        {
            delete[] g_new_user_template_info.buf;
            g_new_user_template_info.buf = NULL;
        }

        // allocate memory for file
        fsize = buf.st_size;
        g_new_user_template_info.buf = new char[fsize+1];
        memset(g_new_user_template_info.buf, 0, fsize+1);

        // open file for reading
        FILE *f;
        f = fopen(g_new_user_template_info.file_name, "r");
        if (f == NULL)
        {
            log_error(ZONE, "add_user: couldn't open %s for reading: %s\n", (const char *)g_new_user_template_info.file_name, strerror(errno));
            return false;
        }

        // convert to char array
        fread_size = fread(g_new_user_template_info.buf, sizeof(char), fsize, f);
        if (fread_size > fsize)
        {
            log_error(ZONE, "add_user: error reading file %s\n", (const char *)g_new_user_template_info.file_name);
            return false;
        }

        // terminate with null char
        g_new_user_template_info.buf[fread_size] = 0;

        fclose(f);

        g_new_user_template_info.last_modified = buf.st_mtime;
    }

    return true;
}

// searches for the passed token_name in the passed token.
// if found, replaces token_name with the passed substitution and stores the value in
// the passed IString.
bool substitute_iic_token(const char* token, const char* token_name, const char* substitution, IString &aresult_str)
{
    const char* iicpos;
    bool found = false;
    if ((iicpos = strstr(token, token_name)) != NULL)
    {
        int pos = iicpos - token;

        aresult_str.clear();
        aresult_str = token;
        aresult_str.erase(pos, strlen(token_name));
        aresult_str.insert(pos, substitution, strlen(substitution));
        found = true;
    }
    return found;
}

// retrieves the new user email in the passed IString, substituting in the passed
// parameters
bool get_new_user_email(IString &aEmailInvite,
                        const char* name,
                        const char* username,
                        const char* password,
                        const char* communityname,
                        const char* dialin,
                        const char* pin,
                        const char* mid)
{
    char IICPORTAL[]        = "<IICPORTAL>";
    char IICNAME[]          = "<IICNAME>";
    char IICUSERNAME[]      = "<IICUSERNAME>";
    char IICPASSWORD[]      = "<IICPASSWORD>";
    char IICCOMMUNITYNAME[] = "<IICCOMMUNITYNAME>";
    char IICDIALIN[]        = "<IICDIALIN>";
    char IICPERSONALPIN[]   = "<IICPERSONALPIN>";
    char IICMEETINGID[]     = "<IICMEETINGID>";
    char ZONLANG[]          = "<ZONLANG>";
    char IICPRODNAME[]      = "<IICPRODNAME>";

    const char *zonlang  = getenv("ZONLANG");
    const char *prodname = getenv("IIC_PRODNAME");

    if (g_new_user_template_info.buf != NULL)
    {
        int maxsize = 4096;
        char* token = new char[maxsize];
        char* copybuf = g_new_user_template_info.buf;

        // parse buffer line by line
        char* pdest;
        int size;

        while ((pdest = strchr(copybuf, '\n')) != NULL)
        {
            // get a line of input
            size = pdest - copybuf + 1;
            if (size > maxsize)
            {
                delete[] token;
                token = new char[size];
                maxsize = size;
            }
            strncpy(token, copybuf, size);
            token[size] = 0;
            copybuf += size;

            IString itoken;
            //  IICPRODNAME may occur several times in a single line - AND - it may occur in a line along with another token
            bool    fMore = true;
            while( fMore )
            {
                if (substitute_iic_token(token, IICPRODNAME, prodname, itoken))
                {
                    token[0] = 0;
                    strcpy(token, itoken.get_string( ) );
                }
                else
                    fMore = false;
            }

            //  ZONLANG may occur in a line along with another token
            if (substitute_iic_token(token, ZONLANG, zonlang, itoken))
            {
                token[0] = 0;
                strcpy(token, itoken.get_string( ) );
            }

            if (substitute_iic_token(token, IICNAME, name, itoken))
            {
                aEmailInvite.append(itoken.get_string());
            }
            else if (substitute_iic_token(token, IICUSERNAME, username, itoken))
            {
                aEmailInvite.append(itoken.get_string());
            }
            else if (substitute_iic_token(token, IICCOMMUNITYNAME, communityname, itoken))
            {
                aEmailInvite.append(itoken.get_string());
            }
            else if (substitute_iic_token(token, IICPASSWORD, password, itoken))
            {
                aEmailInvite.append(itoken.get_string());
            }
            else if (substitute_iic_token(token, IICDIALIN, dialin, itoken))
            {
                aEmailInvite.append(itoken.get_string());
            }
            else if (substitute_iic_token(token, IICPERSONALPIN, pin, itoken))
            {
                aEmailInvite.append(itoken.get_string());
            }
            else if (substitute_iic_token(token, IICMEETINGID, mid, itoken))
            {
                aEmailInvite.append(itoken.get_string());
            }
            else if (substitute_iic_token(token, IICPORTAL, g_host_portal, itoken))
            {
                aEmailInvite.append(itoken.get_string());
            }
            else
            {
                // write out line as is
                aEmailInvite.append(token);
            }
        }

        // if anything left in buffer, probably did not end in newline - log error
        if (strlen(copybuf) > 0)
        {
            log_error(ZONE, "add_user: error reading end of buffer\n");
        }

        if (token != NULL)
            delete[] token;

        return true;
    }
    return false;
}

// send email to the passed user.
bool send_new_user_email(xmlrpc_env *env,
                         const char* communityid,
                         const char* email_address,
                         const char* name,
                         const char* iicscreen,
                         const char* password,
                         const char* new_participant_id,
                         const char* new_meeting_id)
{
    ResultSet *rs = NULL;
    bool result = true;

    IString email_invite;
    IString bridge_phone;
    IString communityname;
    const char * mailer_id = "schedule_mail";
    char id[32];

    // retrieve bridgephone from this user's community
    rs = db->execute("fetch-community-details", communityid);
    if (!rs || rs->is_error())
        goto cleanup;
    if (rs->get_num_rows() == 1)
    {
         communityname = rs->get_value(0, "COMMUNITYNAME");
         bridge_phone = gettext(rs->get_value(0,  "BRIDGEPHONE"));
    }
    else
    {
        xmlrpc_env_set_fault(env, CommunityNotFound, "Unable to find specified community");
        result = false;
        goto cleanup;
    }
    db->closeResults(rs);

    email_invite = "";
    if (get_new_user_email(email_invite,
                           name,
                           iicscreen,
                           password,
                           communityname,
                           bridge_phone,
                           new_participant_id,
                           new_meeting_id
                           ) == true)
    {
        sprintf(id, "%s:%d", mailer_id, get_next_mailer_id());

        IString subject = g_new_user_template_info.subject.length() > 0 ?
            g_new_user_template_info.subject : _("Welcome");

        if (rpc_make_call(id, "mailer", "mailer.send_message", "(sssss)",
                          email_address,
                          g_new_user_template_info.from_address.get_string(),
                          g_new_user_template_info.from_display.get_string(),
                          subject.get_string(),
                          email_invite.get_string()))
        {
            // if unable to send email simply log the error
            log_error(ZONE, "Problem sending new user confirmation email");
            result = false;
	}
    }

cleanup:
    if (rs)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
            result = false;
        }
        db->closeResults(rs);
    }

    return result;
}

// send additional email to user about his account
// Parameters:
//   "SESSIONID", "USERID", "PASSWORD"
// Returns:
//   STATUS (Ok)
xmlrpc_value *send_new_user_confirmation(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *userid, *passwd;
    xmlrpc_value *output = NULL;
    ResultSet *rs1 = NULL;
    ResultSet *rs2 = NULL;
    char hash_password[41];

	xmlrpc_parse_value(env, param_array, "(sss)", &sid, &userid, &passwd);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    rs1 = db->execute("find-user-info", userid);
    if (!rs1 || rs1->is_error())
        goto cleanup;

	if (rs1->get_num_rows() > 0)
	{
		const char *communityid = rs1->get_value(0, "COMMUNITYID");
		const char *screenname = rs1->get_value(0, "SCREENNAME");
		const char *first = rs1->get_value(0, "FIRSTNAME");
		const char *last = rs1->get_value(0, "LASTNAME");
		const char *email1 = rs1->get_value(0, "EMAIL");
        IString name = first;
        if (name.length()>0 && last)
        {
            name.append(" ");
        }
        if (last)
        {
            name.append(last);
        }

        // send confirmation email if email address is set for user
        // and a template file exists
        if (strlen(email1) > 0 && get_new_user_template_info())
        {
            int status;
            char meeting_id[32], part_pin[32];

            // Hash passwords if needed.
            if (check_hash(passwd))
	            strcpy(hash_password, passwd);
            else
	            shahash_r(passwd, hash_password);
            if (!check_admin(env, sid, communityid) && strcmp(sid, userid))   // admin or user himself can change password
            {
    		    xmlrpc_env_set_fault(env, NotAuthorized, "Only admin can send email confirmation to other users");
                goto cleanup;
            }
            rs2 = db->execute("admin-set-password", userid, hash_password);

            find_instant_meetingid(env, userid, meeting_id);
            XMLRPC_FAIL_IF_FAULT(env);

            find_instant_participant_pin(env, meeting_id, part_pin);
            XMLRPC_FAIL_IF_FAULT(env);

            status =  send_new_user_email(env,
                        communityid,
                        email1,
                        name.get_string(),
                        screenname,
                        passwd,
                        part_pin, // pin number
                        meeting_id);
            // Create return value.
            output = xmlrpc_build_value(env, "(i)", status);
            XMLRPC_FAIL_IF_FAULT(env);
        }
	    else
	    {
		    xmlrpc_env_set_fault(env, Failed, "Cannot send email confirmation, email1 is not specified.");
	    }
	}
	else
	{
		xmlrpc_env_set_fault(env, InvalidUserID, "User not found");
	}


cleanup:
    if (rs1 != NULL)
    {
        if (rs1->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs1->get_error());
        }
        db->closeResults(rs1);
    }
    if (rs2 != NULL)
    {
        if (rs2->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs2->get_error());
        }
        db->closeResults(rs2);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

xmlrpc_value *_add_user(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data, bool return_instant)
{
    char *sid, *pwd, *communityid, *title, *first, *middle, *last, *suffix, *company, *jobtitle;
    char *address1, *address2, *state, *country, *postalcode, *screenname, *email1, *email2, *email3;
    char *busphone, *homephone, *mobilephone, *otherphone, *ext, *aim, *yahoo, *msn, *user1, *user2;
    char *defphone, *defemail, *type, *profileid, *userid;
    char *mtgid_hint, *hostid_hint;

    IString username;
    IString name;
    IString new_meeting_id;
    IString new_participant_id;
    ResultSet *rs = NULL;
    xmlrpc_value *output = NULL;

    char dirid[32];
    char newid[32];
    char allgroupid[32];
    char profileid_s[32];

    bool bUseFollowMe = false;

    int type_code;
    int defemailcode;
    int defphonecode;
    int status = Ok;

   if (return_instant)
   {
      xmlrpc_parse_value(env, param_array, "(ssssssssssssssssssssssssssssssssssss)",
			 &sid, &pwd, &communityid, &title, &first, &middle, &last, &suffix,
			 &company, &jobtitle, &address1, &address2, &state, &country, &postalcode,
			 &screenname, &email1, &email2, &email3, &busphone, &homephone, &mobilephone, &otherphone, &ext,
			 &aim, &yahoo, &msn, &user1, &user2, &defphone, &defemail, &type, &profileid, &userid, &mtgid_hint, &hostid_hint);
      XMLRPC_FAIL_IF_FAULT(env);

      new_meeting_id = mtgid_hint;
      new_participant_id = hostid_hint;
   }
   else
   {
	xmlrpc_parse_value(env, param_array, "(ssssssssssssssssssssssssssssssssss)",
                       &sid, &pwd, &communityid, &title, &first, &middle, &last, &suffix,
                       &company, &jobtitle, &address1, &address2, &state, &country, &postalcode,
                       &screenname, &email1, &email2, &email3, &busphone, &homephone, &mobilephone, &otherphone, &ext,
                       &aim, &yahoo, &msn, &user1, &user2, &defphone, &defemail, &type, &profileid, &userid);
    XMLRPC_FAIL_IF_FAULT(env);
   }

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!check_admin(env, sid, communityid))
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "Only admin can add users");
        goto cleanup;
    }

    // find directory id of Global Address Book for passed community
    rs = db->execute("find-directory-by-community", communityid, DEFAULT_GLOBAL_DIRECTORY);
    if (!rs || rs->is_error())
        goto cleanup;
    if (rs->get_num_rows() == 1)
    {
        strcpy(dirid, rs->get_value(0, 0));
    }
    else
    {
        xmlrpc_env_set_fault(env, DirectoryNotFound, "Unable to find directory");
        goto cleanup;
    }
    db->closeResults(rs);

    // verify contact name to be formed from passed info is unique
    rs = db->execute("find-contact-by-name", dirid, first, middle, last);
    if (!rs || rs->is_error())
        goto cleanup;
    if (rs->get_num_rows() > 0)
    {
        xmlrpc_env_set_fault(env, DuplicateContact, "Contact with same name already exists");
        goto cleanup;
    }
    db->closeResults(rs);

    // verify user type
    type_code = atoi(type);
    if (type_code != 1 && type_code != 2)
    {
        xmlrpc_env_set_fault(env, InvalidParameter, "Invalid user type");
        goto cleanup;
    }

    // verify email code
    defemailcode = atoi(defemail);
    if (defemailcode < -2 || defemailcode > 3)
    {
        xmlrpc_env_set_fault(env, InvalidParameter, "Invalid default email type");
        goto cleanup;
    }

    // verify phone code
    defphonecode = atoi(defphone);
    if (defphonecode < -2 || defphonecode > 4)
    {
        xmlrpc_env_set_fault(env, InvalidParameter, "Invalid default phone type");
        goto cleanup;
    }

    // See if default phone is set to something other than None
    // If so, set flag so that instant meeting uses Follow Me...
    bUseFollowMe = (defphonecode != SelPhoneNone);

    // get groupid of all group
    rs = db->execute("find-all-group", dirid);
    if (!rs || rs->is_error())
        goto cleanup;
    if (rs->get_num_rows() == 1)
    {
        strcpy(allgroupid, rs->get_value(0, 0));
    }
    else
    {
        xmlrpc_env_set_fault(env, UnknownGroup, "Unable to find specified group");
        goto cleanup;
    }
    db->closeResults(rs);

    // verify profile or get default if none specified
    if (strlen(profileid) > 0)
    {
        rs = db->execute("find-profile-by-id", profileid);
        if (!rs || rs->is_error())
            goto cleanup;
        if (rs->get_num_rows() == 0)
        {
            xmlrpc_env_set_fault(env, ProfileNotFound, "Profile ID not found");
            goto cleanup;
        }
        else
        {
            strcpy(profileid_s, profileid);
        }
        db->closeResults(rs);
    }
    else // retrieve default profile
    {
        rs = db->execute("find-default-profile", communityid);
        if (!rs || rs->is_error())
            goto cleanup;
        if (rs->get_num_rows() == 0)
        {
            xmlrpc_env_set_fault(env, ProfileNotFound, "Default profile for community not found");
            goto cleanup;
        }
        else
        {
            strcpy(profileid_s, rs->get_value(0, 0));
        }
        db->closeResults(rs);
    }

    // if no userid, generate
    if (strlen(userid) == 0)
    {
        rs = db->execute("allocate-userid");
        if (!rs || rs->is_error())
            goto cleanup;
        strcpy(newid, rs->get_value(0, 0));
        db->closeResults(rs);
    }
    else
    {
        strcpy(newid, userid);
    }

    // create username from screen and server
    username = screenname;
    username.append("@" + g_server_name);

    // verify username to be formed from passed info is unique in our DB
    rs = db->execute("find-user-by-name", (const char*) username);
    if (!rs || rs->is_error())
        goto cleanup;
    if (rs->get_num_rows() > 0)
    {
        status = DuplicateUsername;
    }
    db->closeResults(rs);

    // if 3rd party service requires check, do query for alternatives
    if (add_user_validate_method != NULL)
    {
        int alternative_status = Ok;

        // first check to see if passed name is already stored in our name cache
        if (!name_cache.contains(newid, screenname))
        {
            // storage for alternative names
            validate_screenname_st names;
            int count = MAX_ALTERNATIVE_NAMES;

            // call 3rd party get_alternatives validate method
            // if result is DuplicateUsername build output
            strcpy(names.lookup_name, screenname);
            strcpy(names.userid, newid);
            alternative_status = add_user_validate_method(&names, count);

            // eval results
            if (alternative_status == Failed)
            {
                // report error and bail
                xmlrpc_env_set_fault(env, Failed, "System error getting alternative usernames");
                goto cleanup;
            }
            else if (alternative_status == DuplicateUsername) // build output and cache results
            {
                xmlrpc_value *name = NULL;
                xmlrpc_value *name_vals = NULL;
                name_vals = xmlrpc_build_value(env, "()");
                // build results from alternatives
                for (int i = 0; i < count; i++)
                {
                    name_cache.add(newid, names.names[i]);
                    name = xmlrpc_build_value(env, "s", names.names[i]);
                    xmlrpc_array_append_item(env, name_vals, name);
                    xmlrpc_DECREF(name);
                    name = NULL;
                }
                output = xmlrpc_build_value(env, "(isV)", alternative_status, newid, name_vals);
                XMLRPC_FAIL_IF_FAULT(env);
            }
        }

        if (alternative_status == Ok && status == DuplicateUsername)
        {
            // if we get here, we're confused.  name exists in our DB but is
            // ok in the vendor DB - not a happy state.  treating this as an
            // error
            xmlrpc_env_set_fault(env, Failed, "System conflict getting alternative usernames");
            goto cleanup;
        }

        status = alternative_status;

    } // end if 3rd party service

    // else no 3rd party service, but duplicate in our DB
    // return DuplicateUsername with empty array (no alternatives)
    else if (status == DuplicateUsername)
    {
        xmlrpc_value *name_vals = NULL;
        name_vals = xmlrpc_build_value(env, "()"); // empty array
        output = xmlrpc_build_value(env, "(isV)", status, newid, name_vals);
        XMLRPC_FAIL_IF_FAULT(env);
    }

    if (status == Ok)
    {
        // Add user record
        setup_user(env, sid, type, newid, username, screenname, pwd, communityid, g_server_name);
        XMLRPC_FAIL_IF_FAULT(env);

        // Add contact to directory.
        // Create CONTACT record
        // USERID, DIRECTORYID, USERNAME, SERVER,
        // TITLE, FIRSTNAME, MIDDLENAME, LASTNAME, SUFFIX,
        // COMPANY, JOBTITLE, ADDRESS1, ADDRESS2, STATE, COUNTRY, POSTALCODE,
        // SCREENNAME, AIMNAME, EMAIL, EMAIL2, EMAIL3, BUSPHONE, BUSDIRECT,
        // HOMEPHONE, HOMEDIRECT, MOBILEPHONE, MOBILEDIRECT, OTHERPHONE, OTHERDIRECT,
        // EXTENSION, USER1, USER2, USER3, USER4, USER5,
        // DEFPHONE, DEFEMAIL,
        // TYPE, PROFILEID, MAXSIZE, MAXPRIORITY, PRIORITYTYPE,
        // ENABLEDFEATURES, DISABLEDFEATURES
        rs = db->execute("add-contact",
            newid, dirid, (const char *)username, g_server_name.get_string(),
            title, first, middle, last, suffix,
            company, jobtitle, address1, address2, state, country, postalcode,
            screenname, aim, email1, email2, email3, busphone, "false",
            homephone, "false", mobilephone, "false", otherphone, "false",
            ext, user1, user2, "", "", "",
            defphone, defemail,
            type, profileid_s, "0", "0", "0",
            "0", "0");
	    if (!rs || rs->is_error())
            goto cleanup;
        db->closeResults(rs);

        // Add new user to All group
        add_user_to_group(env, allgroupid, newid);
        XMLRPC_FAIL_IF_FAULT(env);

	    // Add default instant meeting for user
	    name = IString(first) + " " + last;
	    setup_default_meeting(env, sid, communityid, newid, username, name, screenname, new_meeting_id, new_participant_id, bUseFollowMe);
	    XMLRPC_FAIL_IF_FAULT(env);

        // send confirmation email if email address is set for user
        // and a template file exists
        if (strlen(email1) > 0 && get_new_user_template_info())
        {
            send_new_user_email(env,
                                communityid,
                                email1,
                                name.get_string(),
                                screenname,
                                pwd,
                                new_participant_id,
                                new_meeting_id);
        }

        xmlrpc_value *name_vals = NULL;
        name_vals = xmlrpc_build_value(env, "()"); // empty array
      if (!return_instant)
      {
        output = xmlrpc_build_value(env, "(isV)", status, newid, name_vals);
      }
      else
      {
	 output = xmlrpc_build_value(env, "(isVss)", status, newid, name_vals, new_meeting_id.get_string(), new_participant_id.get_string());
      }
        XMLRPC_FAIL_IF_FAULT(env);
    }

  cleanup:
    if (rs)
    {
        if (rs->is_error())
      {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
      }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    /* Return our result. */
    return output;
}

// Create new user.
// For now, we assume two address books:  one for the requesting user, and one global.
// Parameters:
//   "SESSIONID", "PASSWORD", "COMMUNITYID",
//   "TITLE", "FIRSTNAME", "MIDDLENAME", "LASTNAME", "SUFFIX",
//   "COMPANY", "JOBTITLE", "ADDRESS1", "ADDRESS2", "STATE", "COUNTRY", "POSTALCODE",
//   "SCREENNAME", "EMAIL", "EMAIL2", "EMAIL3", "BUSPHONE",
//   "HOMEPHONE", "MOBILEPHONE", "OTHERPHONE",
//	 "EXTENSION", "AIMSCREEN", "YAHOOSCREEN", "MSNSCREEN", "USER1", "USER2",
//   "DEFPHONE", "DEFEMAIL", "TYPE", "PROFILEID", "USERID"
//   TYPE = 1 for registered user, 2 for admin
//   PROFILEID (blank for default)
//   USERID (blank to generate new one)
// Returns:
//   STATUS (Ok or DuplicateUsername)
//   USERID of new user/contact (or existing one if passed in)
//   USERNAMES (array of usernames if duplicate - may be empty)
xmlrpc_value *add_user(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    return _add_user(env, param_array, user_data, false /* instant mtg info */);
}

// Create new user.
// For now, we assume two address books:  one for the requesting user, and one global.
// Parameters:
//   "SESSIONID", "PASSWORD", "COMMUNITYID",
//   "TITLE", "FIRSTNAME", "MIDDLENAME", "LASTNAME", "SUFFIX",
//   "COMPANY", "JOBTITLE", "ADDRESS1", "ADDRESS2", "STATE", "COUNTRY", "POSTALCODE",
//   "SCREENNAME", "EMAIL", "EMAIL2", "EMAIL3", "BUSPHONE",
//   "HOMEPHONE", "MOBILEPHONE", "OTHERPHONE",
//	 "EXTENSION", "AIMSCREEN", "YAHOOSCREEN", "MSNSCREEN", "USER1", "USER2",
//   "DEFPHONE", "DEFEMAIL", "TYPE", "PROFILEID", "USERID"
//   TYPE = 1 for registered user, 2 for admin
//   PROFILEID (blank for default)
//   USERID (blank to generate new one)
// Returns:
//   STATUS (Ok or DuplicateUsername)
//   USERID of new user/contact (or existing one if passed in)
//   USERNAMES (array of usernames if duplicate - may be empty)
//   INSTANTMTGID (instant meeting id for new user)
//   INSTANTHOSTID (instant meeting host id for new user)
xmlrpc_value *add_user_x(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    return _add_user(env, param_array, user_data, true /* instant mtg info */);
}

// Create new contact.
// For now, we assume two address books:  one for the requesting user, and one global.
// Parameters:
//   "SESSIONID", "PASSWORD", "COMMUNITYID", "GROUPID", "DIRECTORYID", "USERNAME", "SERVER",
//   "TITLE", "FIRSTNAME", "MIDDLENAME", "LASTNAME", "SUFFIX",
//   "COMPANY", "JOBTITLE", "ADDRESS1", "ADDRESS2", "STATE", "COUNTRY", "POSTALCODE",
//   "SCREENNAME", "AIMNAME", "EMAIL", "EMAIL2", "EMAIL3", "BUSPHONE", "BUSDIRECT",
//   "HOMEPHONE", "HOMEDIRECT", "MOBILEPHONE", "MOBILEDIRECT", "OTHERPHONE", "OTHERDIRECT",
//	 "EXTENSION", "USER1", "USER2", "USER3", "USER4", "USER5", "DEFPHONE", "DEFEMAIL",
//   "TYPE", "PROFILEID", "MAXSIZE", "MAXPRIORITY", "PRIORITYTYPE",
//   "ENABLEDFEATURES", "DISABLEDFEATURES", "USERID"
//   DIRECTORYID = personal directory id, or 0 for global directory
//   TYPE = 0 for normal contact, 1 for registered user, 2 for admin
//   USERID (blank to generate new one)
// Returns:
//   STATUS (Ok or DuplicateUsername)
//   USERID of new contact (or existing one if passed in)
//   USERNAMES (array of usernames if duplicate - may be empty)
xmlrpc_value *add_contact(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *contactpassword, *communityid, *contactgroupid,
        *directoryid, *contactusername, *server, *title, *firstname, *middlename, *lastname, *suffix, *company, *jobtitle, *address1,
        *address2, *state, *country, *postalcode, *iicscreen, *aimscreen, *email1, *email2, *email3, *businessphone, *businessdirect,
        *homephone, *homedirect, *mobilephone, *mobiledirect, *otherphone, *otherdirect,
        *extension,
        *user1,	// MSN screen name
        *user2,	// Yahoo!
        *user3,	// AIM screenname
        *user4, // User1
        *user5, // User2
        *defphone, *defemail, *usertype, *profileid, *adminmaxsize, *adminmaxpriority, *adminprioritytype,
        *adminenabledfeatures, *admindisabledfeatures, *userid;
    ResultSet *rs = NULL;
    xmlrpc_value *output = NULL;
    char newid[32];
    char allgroupid[32];
    bool bAddToAllGroup = false;
    bool bIsGlobalDirectory = false;
    bool bUseFollowMe = false;
    int type_code;
    int status = Ok;
    bool bIsService;

    xmlrpc_parse_value(env, param_array, "(ssssssssssssssssssssssssssssssssssssssssssssssss)",
                       &sessionid, &contactpassword, &communityid, &contactgroupid, &directoryid,
                       &contactusername, &server, &title, &firstname, &middlename, &lastname, &suffix,
                       &company, &jobtitle, &address1, &address2, &state, &country, &postalcode,
                       &iicscreen, &aimscreen, &email1, &email2, &email3, &businessphone, &businessdirect,
                       &homephone, &homedirect, &mobilephone, &mobiledirect, &otherphone, &otherdirect, &extension,
                       &user1, &user2, &user3, &user4, &user5,
                       &defphone, &defemail,
                       &usertype, &profileid,
                       &adminmaxsize, &adminmaxpriority, &adminprioritytype,
                       &adminenabledfeatures, &admindisabledfeatures, &userid);
    XMLRPC_FAIL_IF_FAULT(env);

    bIsService = check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    // Determines directory type and user's access to directory
    bIsGlobalDirectory = check_directory(env, sessionid, communityid, directoryid, false);
    XMLRPC_FAIL_IF_FAULT(env);

    // If this is a User, see if we can set a default phone for them
    if (bIsGlobalDirectory && atoi(usertype) > 0)
    {
        int new_defaultphone = SelPhoneNone;

        if (strlen(businessphone) > 0)
        {
            new_defaultphone = SelPhoneBus;
        }
        else if (strlen(mobilephone) > 0)
        {
            new_defaultphone = SelPhoneMobile;
        }
        else if (strlen(otherphone) > 0)
        {
            new_defaultphone = SelPhoneOther;
        }

        if (new_defaultphone != SelPhoneNone)
        {
            sprintf(defphone, "%d", new_defaultphone);
            bUseFollowMe = true;
        }
    }

    // Check to see if we additionally need to add
    // this user to the All group.
    rs = db->execute("find-all-group", directoryid);
    if (!rs || rs->is_error())
        goto cleanup;
    if (rs->get_num_rows() == 1)
    {
        strcpy(allgroupid, rs->get_value(0, 0));
        bAddToAllGroup = (strcmp(allgroupid, contactgroupid) != 0);
    }
    else
    {
        xmlrpc_env_set_fault(env, UnknownGroup, "Unable to find specified group");
        goto cleanup;
    }
    db->closeResults(rs);

    // if no userid, generate (only allow system service to select userid)
    if (strlen(userid) == 0 || !bIsService)
    {
        rs = db->execute("allocate-userid");
        if (!rs || rs->is_error())
            goto cleanup;
        strcpy(newid, rs->get_value(0, 0));
        db->closeResults(rs);
    }
    else
    {
        strcpy(newid, userid);
    }

    //
    // if this is new user, check that it's not duplicate record
    //
    type_code = atoi(usertype);

    if (bIsGlobalDirectory && (type_code == UserTypeUser || type_code == UserTypeAdmin))
    {
        // first, verify username is unique in our system
        rs = db->execute("find-user-by-name", (const char*) contactusername);
        if (!rs || rs->is_error())
            goto cleanup;
        if (rs->get_num_rows() > 0)
        {
            status = DuplicateUsername;
        }
        db->closeResults(rs);

        // second, if 3rd party service requires check, do query for alternatives
        if (add_contact_validate_method != NULL)
        {
            int alternative_status = Ok;

            // first check to see if passed name is already stored in our name cache
            if (!name_cache.contains(newid, iicscreen))
            {
                // storage for alternative names
                validate_screenname_st names;
                int count = MAX_ALTERNATIVE_NAMES;

                // call 3rd party get_alternatives validate method
                // if result is DuplicateUsername build output
                strcpy(names.lookup_name, iicscreen);
                strcpy(names.userid, newid);
                alternative_status = add_contact_validate_method(&names, count);
                if (alternative_status == Failed)
                {
                    // report error and bail
                    xmlrpc_env_set_fault(env, Failed, "System error getting alternative usernames");
                    goto cleanup;
                }
                else if (alternative_status == DuplicateUsername) // build output
                {
                    xmlrpc_value *name = NULL;
                    xmlrpc_value *name_vals = NULL;
                    name_vals = xmlrpc_build_value(env, "()");
                    // build results from alternatives and cache results
                    for (int i = 0; i < count; i++)
                    {
                        name_cache.add(newid, names.names[i]);
                        name = xmlrpc_build_value(env, "s", names.names[i]);
                        xmlrpc_array_append_item(env, name_vals, name);
                        xmlrpc_DECREF(name);
                        name = NULL;
                    }
                    output = xmlrpc_build_value(env, "(isV)", alternative_status, newid, name_vals);
                    XMLRPC_FAIL_IF_FAULT(env);
                }
            } // end if in cache

            if (alternative_status == Ok && status == DuplicateUsername)
            {
                // if we get here, we're confused.  name exists in our DB but is
                // ok in the vendor DB - not a happy state.  treating this as an
                // error
                xmlrpc_env_set_fault(env, Failed, "System conflict getting alternative usernames");
                goto cleanup;
            }

            status = alternative_status;
        } // end if 3rd party

        // else if no 3rd party service, but duplicate in our DB
        // return DuplicateUsername with empty array (no alternatives)
        else if (status == DuplicateUsername)
        {
            xmlrpc_value *name_vals = NULL;
            name_vals = xmlrpc_build_value(env, "()"); // empty array
            output = xmlrpc_build_value(env, "(isV)", status, newid, name_vals);
            XMLRPC_FAIL_IF_FAULT(env);
        }
    }

    if (status == Ok)
    {
        if (bIsGlobalDirectory) // add user
        {
            setup_user(env, sessionid, usertype, newid, contactusername, iicscreen, contactpassword, communityid, server);
            XMLRPC_FAIL_IF_FAULT(env);

            if (log_user(EvnLog_User_Event_Added,
                         communityid, newid,
                         title, firstname, middlename, lastname, suffix,
                         company, jobtitle,
                         address1, address2, state, country, postalcode,
                         iicscreen,
                         email1, email2, email3,
                         businessphone, homephone, mobilephone, otherphone, extension,
                         user3 /* aim */, user2 /* yahoo */, user1 /* msn */,
                         user4 /* user1 */, user5 /* user2 */,
                         defphone, defemail, atoi(usertype)) < 0)
            {
                log_error(ZONE, "Problem writing user record");
            }
        }

        // Add contact to directory.
	    rs = db->execute("add-contact",
                       newid, directoryid,
                       contactusername, server, title, firstname, middlename, lastname, suffix,
                       company, jobtitle, address1, address2, state, country, postalcode,
                       iicscreen, aimscreen, email1, email2, email3, businessphone, businessdirect,
                       homephone, homedirect, mobilephone, mobiledirect, otherphone, otherdirect, extension,
                       user1, user2, user3, user4, user5,
                       defphone, defemail,
                       usertype, profileid,
                       adminmaxsize, adminmaxpriority, adminprioritytype,
                       adminenabledfeatures, admindisabledfeatures);

	    if (!rs || rs->is_error())
            goto cleanup;
        db->closeResults(rs);

        // Also add to appropriate group.
        add_user_to_group(env, contactgroupid, newid);
        XMLRPC_FAIL_IF_FAULT(env);

        // Add to All group if necessary
        if (bAddToAllGroup)
        {
            add_user_to_group(env, allgroupid, newid);
            XMLRPC_FAIL_IF_FAULT(env);
        }

        // Add default instant meeting for users
        if (bIsGlobalDirectory && atoi(usertype) > 0)
	    {
		    char *mid = NULL;
		    if (!strncmp(user5, "$$mid:", 6))
			    mid = user5 + 6;

		    IString name = IString(firstname) + " " + lastname;
            IString new_meeting_id;
            IString new_participant_id;
		    setup_default_meeting(env, sessionid, communityid, newid, contactusername, name, iicscreen, new_meeting_id, new_participant_id, bUseFollowMe, mid);
	        XMLRPC_FAIL_IF_FAULT(env);

            // send confirmation email if email address is set for user
            // and a template file exists

            if (strlen(email1) > 0 && get_new_user_template_info())
            {
                send_new_user_email(env,
                                    communityid,
                                    email1,
                                    name.get_string(),
                                    iicscreen,
                                    contactpassword,
                                    new_participant_id,
                                    new_meeting_id);
            }
	    }

        xmlrpc_value *name_vals = NULL;
        name_vals = xmlrpc_build_value(env, "()"); // empty array
        output = xmlrpc_build_value(env, "(isV)", status, newid, name_vals);
        XMLRPC_FAIL_IF_FAULT(env);
    }

cleanup:
    if (rs)
    {
        if (rs->is_error())
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    /* Return our result. */
    return output;
}


// Import a contact
//
// For now, we assume two address books:  one for the requesting user, and one global.
//
// Parameter 0:
// Indicates how to add the contact.  There are two options: replace and insert.
// For replace, we first look for an existing contact with the same name information.  If found
// we update that record, otherwise we insert a new record.  For insert, we simply add the
// contact as new, disregarding the fact that this may result in duplicates contacts.
//
// Remaining Parameters:
//   "SESSIONID", "PASSWORD", "COMMUNITYID", "GROUPID", "DIRECTORYID", "USERNAME", "SERVER",
//   "TITLE", "FIRSTNAME", "MIDDLENAME", "LASTNAME", "SUFFIX",
//   "COMPANY", "JOBTITLE", "ADDRESS1", "ADDRESS2", "STATE", "COUNTRY", "POSTALCODE",
//   "SCREENNAME", "AIMNAME", "EMAIL", "EMAIL2", "EMAIL3", "BUSPHONE", "BUSDIRECT",
//   "HOMEPHONE", "HOMEDIRECT", "MOBILEPHONE", "MOBILEDIRECT", "OTHERPHONE", "OTHERDIRECT",
//	 "EXTENSION", "USER1", "USER2", "USER3", "USER4", "USER5",
//   "DEFPHONE", "DEFEMAIL",
//   "TYPE", "PROFILEID", "MAXSIZE", "MAXPRIORITY", "PRIORITYTYPE",
//   "ENABLEDFEATURES", "DISABLEDFEATURES"
//   DIRECTORYID = personal directory id, or 0 for global directory
// Returns:
//   The following information for the new or updated contact
//   "USERID", "GROUPID", "DIRECTORYID", "USERNAME", SERVER,
//   "TITLE", "FIRSTNAME", "MIDDLENAME", "LASTNAME", "SUFFIX",
//   "COMPANY", "JOBTITLE", "ADDRESS1", "ADDRESS2", "STATE", "COUNTRY", "POSTALCODE",
//   "SCREENNAME", "AIMNAME", "EMAIL", "EMAIL2", "EMAIL3", "BUSPHONE", "BUSDIRECT",
//   "HOMEPHONE", "HOMEDIRECT", "MOBILEPHONE", "MOBILEDIRECT", "OTHERPHONE", "OTHERDIRECT",
//	 "EXTENSION", "USER1", "USER2", "USER3", "USER4", "USER5",
//   "DEFPHONE", "DEFEMAIL",
//   "TYPE", "PROFILEID", "MAXSIZE", "MAXPRIORITY", "PRIORITYTYPE",
//   "ENABLEDFEATURES", "DISABLEDFEATURES"
#if 0
xmlrpc_value *import_contact(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *method, *sid, *pwd, *communityid, *group, *dirid, *username, *server,
         *title, *first, *middle, *last, *suffix,
         *company, *jobtitle, *address1, *address2, *state, *country, *postalcode,
         *screen, *aimname, *email, *email2, *email3, *busphone, *busdirect,
         *homephone, *homedirect, *mobilephone, *mobiledirect, *otherphone, *otherdirect,
		 *extension,*user1, *user2, *user3, *user4, *user5,
		 *defphone, *defemail,
         *type, *profileid, *maxsize, *maxpriority, *prioritytype,
         *enabledfeatures, *disabledfeatures;

    // char *screen, *type;
    ParamList *list = NULL;
    ResultSet *rs = NULL;
    xmlrpc_value *output = NULL;
    // xmlrpc_value *screen_val = NULL;
    // xmlrpc_value *type_val = NULL;
    char userid[32];
    char allgroupid[32];
    bool bAddToAllGroup = false;
    bool bInsert = true;
    bool bIsGlobalDirectory = false;

    xmlrpc_parse_value(env, param_array, "(ssssssssssssssssssssssssssssssssssssssssssssssss)",
                                         &method, &sid, &pwd, &communityid, &group, &dirid, &username, &server,
                                         &title, &first, &middle, &last, &suffix,
                                         &company, &jobtitle, &address1, &address2, &state, &country, &postalcode,
                                         &screen, &aimname, &email, &email2, &email3, &busphone, &busdirect,
                                         &homephone, &homedirect, &mobilephone, &mobiledirect, &otherphone, &otherdirect,
										 &extension, &user1, &user2, &user3, &user4, &user5,
										 &defphone, &defemail,
                                         &type, &profileid, &maxsize, &maxpriority, &prioritytype,
                                         &enabledfeatures, &disabledfeatures);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    // Determines directory type and user's access to directory
    bIsGlobalDirectory = check_directory(env, sid, communityid, dirid, false);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!strcmp(method, "replace"))
    {
        rs = db->execute("find-contact-by-name", dirid, first, middle, last);
        if (!rs || rs->is_error())
            goto cleanup;

        if (rs->get_num_rows() > 0)
        {
            bInsert = false;
            strcpy(userid, rs->get_value(0, 0));
        }
        db->closeResults(rs);
    }

    if (bInsert)
    {
        // Copy xml parameters into list.
        // We'll copy the group id, but replace with new user id below.
        list = build_params(env, param_array, 4);
        if (env->fault_occurred || list == NULL)
            goto cleanup;

        // Check to see if we additionally need to add
        // this user to the All group.
        rs = db->execute("find-all-group", dirid);
        if (!rs || rs->is_error())
            goto cleanup;
        if (rs->get_num_rows() == 1)
        {
            strcpy(allgroupid, rs->get_value(0, 0));
            bAddToAllGroup = (strcmp(allgroupid, group) != 0);
        }
        else
        {
            xmlrpc_env_set_fault(env, UnknownGroup, "Unable to find specified group");
            goto cleanup;
        }
        db->closeResults(rs);

        // Get id for new record.
        rs = db->execute("allocate-userid");
        if (!rs || rs->is_error())
            goto cleanup;
        strcpy(userid, rs->get_value(0, 0));
        list->set(0, userid);
        db->closeResults(rs);

        // Add user record too, if needed.
        if (bIsGlobalDirectory)
        {
            setup_user(env, sid, type, userid, username, screen, pwd, communityid, server);
            XMLRPC_FAIL_IF_FAULT(env);
        }

        // Add contact to directory.
        rs = db->execute("add-contact", list);
        if (!rs || rs->is_error())
            goto cleanup;
        db->closeResults(rs);

        // Also add to appropriate group.
        add_user_to_group(env, group, userid);
        XMLRPC_FAIL_IF_FAULT(env);

        // Add to All group if necessary
        if (bAddToAllGroup)
        {
            add_user_to_group(env, allgroupid, userid);
            XMLRPC_FAIL_IF_FAULT(env);
        }
        db->closeResults(rs);
    }
    else
    {
        // May need to update user record also.
        if (bIsGlobalDirectory)
        {
            setup_user(env, sid, type, userid, username, screen, pwd, communityid, server);
            XMLRPC_FAIL_IF_FAULT(env);
        }

        // Copy xml parameters into list.
        // We'll copy the directory id, but replace with new user id below.
        list = build_params(env, param_array, 5);
        if (env->fault_occurred || list == NULL)
            goto cleanup;

        list->set(0, userid);

        // Add contact to directory.
        rs = db->execute("update-contact", list);
        if (!rs || rs->is_error())
        {
            log_error(ZONE, "Database error updating contact during import\n");
            goto cleanup;
        }
        db->closeResults(rs);
    }

	// Add default instant meeting for users
	if (bIsGlobalDirectory && atoi(type) > 0)
	{
		IString name = IString(first) + " " + last;
        IString new_meeting_id;
        IString new_participant_id;
		setup_default_meeting(env, sid, communityid, userid, username, name, screen, new_meeting_id, new_participant_id, false);
	    XMLRPC_FAIL_IF_FAULT(env);
	}

    output = xmlrpc_build_value(env, "(sssssssssssssssssssssssssssssssssssssssssssss)",
                userid, group, dirid, username, server,
                title, first, middle, last, suffix,
                company, jobtitle, address1, address2, state, country, postalcode,
                screen, aimname, email, email2, email3, busphone, busdirect,
                homephone, homedirect, mobilephone, mobiledirect, otherphone, otherdirect,
				extension, user1, user2, user3, user4, user5,
				defphone, defemail,
                type, profileid, maxsize, maxpriority, prioritytype,
                enabledfeatures, disabledfeatures);

cleanup:
    if (!list)
		xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to addressbk.import_contact");
    else
        delete list;

    if (rs)
    {
        if (rs->is_error())
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");


    /* Return our result. */
    return output;
}
#endif


// Update a contact
//
// For now, we assume two address books:  one for the requesting user, and one global.
//
// Parameters:
//   "SESSIONID", "PASSWORD", "USERID", "COMMUNITYID", "DIRECTORYID", "USERNAME", "SERVER"
//   "TITLE", "FIRSTNAME", "MIDDLENAME", "LASTNAME", "SUFFIX",
//   "COMPANY", "JOBTITLE", "ADDRESS1", "ADDRESS2", "STATE", "COUNTRY", "POSTALCODE",
//   "SCREENNAME", "AIMNAME", "EMAIL", "EMAIL2", "EMAIL3", "BUSPHONE", "BUSDIRECT",
//   "HOMEPHONE", "HOMEDIRECT", "MOBILEPHONE", "MOBILEDIRECT", "OTHERPHONE", "OTHERDIRECT",
//	 "EXTENSION", "USER1", "USER2", "USER3", "USER4", "USER5",
//   "TYPE", "PROFILEID", "MAXSIZE", "MAXPRIORITY", "PRIORITYTYPE",
//   "ENABLEDFEATURES", "DISABLEDFEATURES"
//   DIRECTORYID = personal directory id, or 0 for global directory
// Returns:
//   USERID of new or updated contact
xmlrpc_value *update_contact(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sessionid, *contactpassword, *userid, *communityid, *directoryid,
        *contactusername, *server, *title, *firstname, *middlename, *lastname,
        *suffix, *company, *jobtitle, *address1,
        *address2, *state, *country, *postalcode,
        *iicscreen, *aimscreen, *email1, *email2,
        *email3, *businessphone, *businessdirect,
        *homephone, *homedirect, *mobilephone, *mobiledirect, *otherphone, *otherdirect, *extension,
        *user1,	// MSN screen name
        *user2,	// Yahoo!
        *user3,	// AIM screenname
        *user4, // User1
        *user5, // User2
        *usertype, *profileid, *adminmaxsize, *adminmaxpriority, *adminprioritytype,
        *adminenabledfeatures, *admindisabledfeatures;
    ParamList *list = NULL;
    ResultSet *rs = NULL;
    xmlrpc_value *output = NULL;
    bool bIsGlobalDirectory = false;
    bool bIsAdmin = false;
    bool bParsing = true;
    bool bEditSelf = false;
    char curr_dir_id[32];

    xmlrpc_parse_value(env, param_array, "(sssssssssssssssssssssssssssssssssssssssssssss)",
                       &sessionid, &contactpassword, &userid, &communityid, &directoryid,
                       &contactusername, &server, &title, &firstname, &middlename, &lastname,
                       &suffix, &company, &jobtitle, &address1,
                       &address2, &state, &country, &postalcode,
                       &iicscreen, &aimscreen, &email1, &email2,
                       &email3, &businessphone, &businessdirect,
                       &homephone, &homedirect, &mobilephone, &mobiledirect, &otherphone, &otherdirect, &extension,
                       &user1, // MSN screen name
                       &user2, // Yahoo!
                       &user3, // AIM screenname
                       &user4, // User1
                       &user5, // User2
                       &usertype, &profileid, &adminmaxsize, &adminmaxpriority, &adminprioritytype,
                       &adminenabledfeatures, &admindisabledfeatures);
    XMLRPC_FAIL_IF_FAULT(env);

    bParsing = false;

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    find_contact_directory(env, userid, curr_dir_id);
    XMLRPC_FAIL_IF_FAULT(env);

    if (strcmp(userid, sessionid) == 0)
        bEditSelf = true;

    if (strcmp(curr_dir_id, directoryid))
    {
		xmlrpc_env_set_fault(env, InvalidParameter, "Invalid directory id specified");
        goto cleanup;
    }

    // Determines directory type and user's access to directory
    bIsGlobalDirectory = check_directory(env, sessionid, communityid, directoryid, bEditSelf);
    XMLRPC_FAIL_IF_FAULT(env);

    if (bIsGlobalDirectory)
    {
        bIsAdmin = check_admin(env, sessionid, communityid);
        XMLRPC_FAIL_IF_FAULT(env);

        if (bIsAdmin)
        {
            // Admin can edit all user fields
            bEditSelf = false;
        }
        else
        {
            if (!bEditSelf)
            {
                xmlrpc_env_set_fault(env, NotAuthorized, "You must be an administrator to update the global directory");
                goto cleanup;
            }
        }
    }

    // May need to update user record also.
    // Skip setup_user if user is updating their own contact record.
    if (bIsGlobalDirectory && !bEditSelf)
    {
        setup_user(env, sessionid, usertype, userid, contactusername, iicscreen, contactpassword, communityid, server);
        XMLRPC_FAIL_IF_FAULT(env);
    }

    // Copy xml parameters into list.
    // We'll copy the directory id, but replace with user id.
    list = build_params(env, param_array, 4);
    if (env->fault_occurred || list == NULL)
        goto cleanup;
    list->set(0, userid);

    // Update contact to directory.
    if (bEditSelf)
        rs = db->execute("update-contact-self", list);
    else
        rs = db->execute("update-contact", list);
    if (!rs || rs->is_error())
    {
        log_error(ZONE, "Database error updating contact\n");

        goto cleanup;
    }

	// Add default instant meeting for users
	if (bIsGlobalDirectory && !bEditSelf && atoi(usertype) > 0)
	{
		IString name = IString(firstname) + " " + lastname;
        IString new_meeting_id;
        IString new_participant_id;
		setup_default_meeting(env, sessionid, communityid, userid, contactusername, name, iicscreen, new_meeting_id, new_participant_id, false);
	    XMLRPC_FAIL_IF_FAULT(env);

        if (log_user(EvnLog_User_Event_Updated,
                     communityid, userid,
                     title, firstname, middlename, lastname, suffix,
                     company, jobtitle,
                     address1, address2, state, country, postalcode,
                     iicscreen,
                     email1, email2, email3,
                     businessphone, homephone, mobilephone, otherphone, extension,
                     user3 /* aim */, user2 /* yahoo */, user1 /* msn */,
                     user4 /* user1 */, user5 /* user2 */,
                     "0", "0",
                     atoi(usertype)) < 0)
        {
            log_error(ZONE, "Problem writing user record");
        }
	}

    output = xmlrpc_build_value(env, "(i)", rs->get_num_affected());
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!list)
    {
        if (bParsing)
        {
		    xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to addressbk.update_contact");
        }
    }
    else
        delete list;

    if (rs)
    {
        if (rs->is_error())
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    /* Return our result. */
    return output;
}


// Remove contact.
// Parameters:
//   SESSIONID, USERID
// Returns:
//   Number of rows deleted
xmlrpc_value *remove_contact(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *userid;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    bool bIsGlobalDirectory = false;
    char dir_id[32];

	xmlrpc_parse_value(env, param_array, "(ss)", &sid, &userid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    find_contact_directory(env, userid, dir_id);
    XMLRPC_FAIL_IF_FAULT(env);

    // Determines directory type and user's access to directory
    bIsGlobalDirectory = check_directory(env, sid, NULL, dir_id, false);
    XMLRPC_FAIL_IF_FAULT(env);

    // if user is community owner cannot delete
    rs = db->execute("find-community-owner", userid);
    if (!rs || rs->is_error())
        goto cleanup;
    if (rs->get_num_rows() > 0)
    {
        db->closeResults(rs);
        xmlrpc_env_set_fault(env, CommunityOwnerDeleteError, "You cannot delete the community owner");
        goto cleanup;
    }
	db->closeResults(rs);

    bool should_log;
    rs = db->execute("find-user", userid);
    if (!rs || rs->is_error())
        goto cleanup;
    should_log = (rs->get_num_rows() > 0);
	db->closeResults(rs);

    // Cascading delete removes matching group member rows
    rs = db->execute("remove-contact", userid);
    if (!rs || rs->is_error())
        goto cleanup;
    output = xmlrpc_build_value(env, "(i)", rs->get_num_affected());
	db->closeResults(rs);

	rs = db->execute("remove-user", userid);
    if (!rs || rs->is_error())
        goto cleanup;

    // only log if *really* deleted
    if (should_log)
    {
        if (log_user(EvnLog_User_Event_Deleted,
                     "", userid, "3", "", "", "", "", "", "", "", "",
                     "", "", "", "", "", "", "", "", "", "", "", "",
                     "", "", "", "", "", "0", "0", 0) < 0)
        {
            log_error(ZONE, "Problem writing user record");
        }
    }

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Add new group.
// Parameters:
//   SESSIONID, DIRECTORYID, NAME, TYPE
// Returns:
//   GROUPID, DIRECTORYID, NAME, CREATED
//   Created = 1 if group created, 0 if group existed
xmlrpc_value *add_group(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *dirid, *name, *type;
    ResultSet *rs = NULL;
    int retry = 2;
    xmlrpc_value *val = NULL;

	xmlrpc_parse_value(env, param_array, "(ssss*)", &sid, &dirid, &name, &type);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_directory(env, sid, NULL, dirid, false);
    XMLRPC_FAIL_IF_FAULT(env);

    while (retry--)
    {
        rs = db->execute("find-group", dirid, name);
        if (!rs || rs->is_error())
            goto cleanup;

        if (rs->get_num_rows() > 0)
        {
            val = xmlrpc_build_value(env, "(sssi)", rs->get_value(0, 0), dirid, name, retry < 1);
            goto cleanup;
        }
        db->closeResults(rs);

        char typestr[32];
        rs = db->execute("create-group", dirid, name, i_itoa(dtPersonal,typestr));
        if (!rs || rs->is_error())
            goto cleanup;
        db->closeResults(rs);
    }

    log_error(ZONE, "Unable to find or create group %s in directory %s\n", name, dirid);
    xmlrpc_env_set_fault(env, AddressBookError, "Unable to create group");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && val == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return val;
}


// Update group.
// Parameters:
//   SESSIONID, DIRECTORYID, GROUPID, NAME, TYPE
// Returns:
//   Number of rows deleted.
xmlrpc_value *update_group(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *dirid, *groupid, *name, *type;
    ResultSet *rs = NULL;
    xmlrpc_value *val = NULL;
    xmlrpc_value *output = NULL;
    int count = 0;
    char db_dirid[32];

	xmlrpc_parse_value(env, param_array, "(sssss*)", &sid, &dirid, &groupid, &name, &type);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    find_group_directory(env, groupid, db_dirid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_directory(env, sid, NULL, db_dirid, false);
    XMLRPC_FAIL_IF_FAULT(env);

    rs = db->execute("rename-group", groupid, name);
    if (!rs || rs->is_error())
        goto cleanup;
    count =  rs->get_num_affected();

    output = xmlrpc_build_value(env, "(i)", count);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && val == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return val;
}

// Remove group.
// Parameters:
//   SESSIONID, GROUPID
// Returns:
//   Number of rows deleted.
xmlrpc_value *remove_group(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *group;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    int count = 0;
    char dirid[32];

	xmlrpc_parse_value(env, param_array, "(ss)", &sid, &group);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    find_group_directory(env, group, dirid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_directory(env, sid, NULL, dirid, false);
    XMLRPC_FAIL_IF_FAULT(env);

    // Delete is cascaded to members table.
    rs = db->execute("remove-group", group);
    if (!rs || rs->is_error())
        goto cleanup;
    count =  rs->get_num_affected();

    // Orphaned contact records may be left behind--that's ok as the fetch operations
    // will not return contacts that are not in a group, and we can periodically find
    // and remove all orphaned contacts.

    output = xmlrpc_build_value(env, "(i)", count);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Add contact to group.
// Parameters:
//   SESSIONID, GROUPID, USERID
// Returns:
//   MEMBERID, CREATED
//   Created = 1 if added to group, 0 if already in group
xmlrpc_value *add_to_group(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *groupid, *groupeduserid;
    ResultSet *rs = NULL;
    int retry = 2;
    xmlrpc_value *val = NULL;
    char dirid[32];
    char contactdirid[32];
    char allgroupid[32];
    bool bAddToAllGroup = false;
    IString queryfind;
    IString queryadd;

	xmlrpc_parse_value(env, param_array, "(sss*)", &sid, &groupid, &groupeduserid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    find_group_directory(env, groupid, dirid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_directory(env, sid, NULL, dirid, false);
    XMLRPC_FAIL_IF_FAULT(env);

    // Make sure contact and group belong to same directory
    find_contact_directory(env, groupeduserid, contactdirid);
    XMLRPC_FAIL_IF_FAULT(env);
    if (strcmp(dirid, contactdirid))
    {
        xmlrpc_env_set_fault(env, AddressBookError, "Group members must be from same directory as group");
        goto cleanup;
    }

    if (isdigits(groupeduserid))
    {
        queryfind = "find-group-member";
        queryadd = "add-to-group";
    }
    else
    {
        queryfind = "find-group-member-external";
        queryadd = "add-to-group-external";
    }

    // Check to see if we need to add
    // this user to the All group.
    rs = db->execute("find-related-all-group", groupid);
    if (!rs || rs->is_error())
        goto cleanup;
    if (rs->get_num_rows() == 1)
    {
        strcpy(allgroupid, rs->get_value(0, 0));
        bAddToAllGroup = (strcmp(allgroupid, groupid) != 0);
    }
    db->closeResults(rs);

    // Additionally add to the All group if necessary
    if (bAddToAllGroup)
    {
        rs = db->execute(queryfind, allgroupid, groupeduserid);
        if (!rs || rs->is_error())
            goto cleanup;

        if (rs->get_num_rows() == 0)
        {
            db->closeResults(rs);

            rs = db->execute(queryadd, allgroupid, groupeduserid);
            if (!rs || rs->is_error())
                goto cleanup;

            set_group_modified(env, allgroupid);
            XMLRPC_FAIL_IF_FAULT(env);
        }

        db->closeResults(rs);
    }

    // Now add to the requested group
    while (retry--)
    {
        rs = db->execute(queryfind, groupid, groupeduserid);
        if (!rs || rs->is_error())
            goto cleanup;

        if (rs->get_num_rows() > 0)
        {
            val = xmlrpc_build_value(env, "(si)", rs->get_value(0, 0), retry < 1);
            goto cleanup;
        }
        db->closeResults(rs);

        rs = db->execute(queryadd, groupid, groupeduserid);
        if (!rs || rs->is_error())
            goto cleanup;
        db->closeResults(rs);

        set_group_modified(env, groupid);
        XMLRPC_FAIL_IF_FAULT(env);
    }

    log_error(ZONE, "Unable to add %s to group %s\n", groupeduserid, groupid);
    xmlrpc_env_set_fault(env, AddressBookError, "Unable to add contact to group");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && val == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return val;
}


// Remove contact from group.
// Parameters:
//   SESSIONID, GROUPID, USERID
// Returns:
//   Rows deleted
xmlrpc_value *remove_from_group(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *groupid, *groupeduserid;
    ResultSet *rs = NULL;
    xmlrpc_value *val = NULL;
    char dirid[32];
    IString queryremove;

	xmlrpc_parse_value(env, param_array, "(sss*)", &sid, &groupid, &groupeduserid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    find_group_directory(env, groupid, dirid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_directory(env, sid, NULL, dirid, false);
    XMLRPC_FAIL_IF_FAULT(env);

    if (isdigits(groupeduserid))
    {
        queryremove = "remove-from-group";
    }
    else
    {
        queryremove = "remove-from-group-external";
    }

    rs = db->execute(queryremove, groupid, groupeduserid);
    if (!rs || rs->is_error())
        goto cleanup;

    set_group_modified(env, groupid);
    XMLRPC_FAIL_IF_FAULT(env);

    val = xmlrpc_build_value(env, "(i)", rs->get_num_affected());
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && val == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return val;
}

// Internal add profile function
//
// Returns:
//    new profile id
//
// Use XMLRPC_FAIL_IF_FAULT(env) to detect errors
//
int add_profile_internal(xmlrpc_env *env, char *communityid, char *profilename)
{
    ResultSet *rs = NULL;
    int newid;
    char newid_s[32];

    rs = db->execute("find-profile-by-name", profilename);
    if (!rs || rs->is_error())
        goto cleanup;
    if (rs->get_num_rows() > 0)
    {
        xmlrpc_env_set_fault(env, ProfileNameNotUnique, "Policy with same name already exists");
        goto cleanup;
    }
    db->closeResults(rs);

    // Get id for new record.
    rs = db->execute("allocate-profileid");
    if (!rs || rs->is_error())
        goto cleanup;
    strcpy(newid_s, rs->get_value(0, 0));
    db->closeResults(rs);

    // Add profile.
	rs = db->execute("add-profile", newid_s, communityid, profilename, DEFAULT_PROFILE_FLAGS, DEFAULT_PROFILE_OPTIONS);
	if (!rs || rs->is_error())
        goto cleanup;
    db->closeResults(rs);

    newid = atoi(newid_s);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }

    return newid;
}

// Create a new Profile (System Preferences/Priviledges)
//
// Parameters:
//   "SESSIONID", "COMMUNITYID", "PROFILENAME"
//
// Returns:
//   "PROFILEID", "PROFILENAME", "ENABLEDFEATURES", "PROFILEOPTIONS"
xmlrpc_value *add_profile(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *communityid, *profilename;
    xmlrpc_value *output = NULL;
    int newid;
    char newid_s[32];

	xmlrpc_parse_value(env, param_array, "(sss)", &sid, &communityid, &profilename);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    // Make sure this is an administrator.
    if (!check_admin(env, sid, communityid))
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "You must be an administrator to update profile settings");
        goto cleanup;
    }

    newid = add_profile_internal(env, communityid, profilename);
    XMLRPC_FAIL_IF_FAULT(env);

    i_itoa(newid, newid_s);

    output = xmlrpc_build_value(env, "(ssss)", newid_s, profilename, DEFAULT_PROFILE_FLAGS, DEFAULT_PROFILE_OPTIONS);

cleanup:

    return output;
}

// Internal copy profile
//
// Returns: void
//          Use XMLRPC_FAIL_IF_FAULT(env) to detect errors
//
void copy_profile_internal(xmlrpc_env *env, char* profileid,
                           char* copyfeatures, char* copyoptions,
                           char* copyprofileid, char* communityid, char* profilename)
{
    ResultSet *rs = NULL;

    // Get the record to copy.
    rs = db->execute("find-profile-by-id", profileid);
    if (!rs || rs->is_error())
        goto cleanup;

    // make sure id is valid
    if (rs->get_num_rows() == 0)
    {
       xmlrpc_env_set_fault(env, ProfileNotFound, "Profile ID not found");
       goto cleanup;
    }

    // ToDo: Make sure community id doesn't change

    strcpy(copyfeatures, rs->get_value(0, 1));
    strcpy(copyoptions, rs->get_value(0, 2));
    db->closeResults(rs);

    // Get id for new record.
    rs = db->execute("allocate-profileid");
    if (!rs || rs->is_error())
        goto cleanup;
    strcpy(copyprofileid, rs->get_value(0, 0));
    db->closeResults(rs);

    // Add profile.
	rs = db->execute("add-profile", copyprofileid, communityid, profilename, copyfeatures, copyoptions);
	if (!rs || rs->is_error())
        goto cleanup;
    db->closeResults(rs);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}

// Copy an existing Profile (System Preferences/Priviledges) into a new Profile
//
// Parameters:
//   "SESSIONID", "PROFILEID", "COMMUNITYID", "NEWPROFILENAME"
//
// Returns:
//   "PROFILEID", "PROFILENAME", "ENABLEDFEATURES", "PROFILEOPTIONS"
xmlrpc_value *copy_profile(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *profileid, *communityid, *newprofilename;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    char copyprofileid[32];
    char copyfeatures[32];
    char copyoptions[2000];

	xmlrpc_parse_value(env, param_array, "(ssss)", &sid, &profileid, &communityid, &newprofilename);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    // Make sure this is an administrator.
    if (!check_admin(env, sid, communityid))
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "You must be an administrator to update profile settings");
        goto cleanup;
    }

    rs = db->execute("find-profile-by-name", newprofilename);
    if (!rs || rs->is_error())
        goto cleanup;
    if (rs->get_num_rows() > 0)
    {
        xmlrpc_env_set_fault(env, ProfileNameNotUnique, "Policy with same name already exists");
        goto cleanup;
    }
    db->closeResults(rs);

    copy_profile_internal(env, profileid, copyfeatures, copyoptions, copyprofileid, communityid,  newprofilename);
    XMLRPC_FAIL_IF_FAULT(env);

    output = xmlrpc_build_value(env, "(ssss)", copyprofileid, newprofilename, copyfeatures, copyoptions);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Update a Profile (Preferences/Priviledges)
//
// Parameters:
//   "SESSIONID", "PROFILEID", "PROFILENAME",
//   "ENABLEDFEATURES", "PROFILEOPTIONS"
//
// Returns:
//   Number of rows updated
xmlrpc_value *update_profile(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid;
    ParamList *list = NULL;
    ResultSet *rs = NULL;
    xmlrpc_value *output = NULL;
    const char *profileid;
    char community_id[64];

    xmlrpc_parse_value(env, param_array, "(ss*)", &sid, &profileid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    // Get the record to copy.
    rs = db->execute("find-profile-by-id", profileid);
    if (!rs || rs->is_error())
        goto cleanup;

    // make sure id is valid
    if (rs->get_num_rows() == 0)
    {
       xmlrpc_env_set_fault(env, ProfileNotFound, "Profile ID not found");
       goto cleanup;
    }
    strcpy(community_id, rs->get_value(0, 3));
    db->closeResults(rs);

    // Make sure this is an administrator.
    if (!check_admin(env, sid, community_id))
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "You must be an administrator to update profile settings");
        goto cleanup;
    }

    // Copy xml parameters into list.
    // Skip the sid
    list = build_params(env, param_array, 1);
    if (env->fault_occurred || list == NULL)
        goto cleanup;

    // Update contact to directory.
    rs = db->execute("update-profile", list);
    if (!rs || rs->is_error())
    {
        log_error(ZONE, "Database error updating profile\n");
        goto cleanup;
    }

    output = xmlrpc_build_value(env, "(i)", rs->get_num_affected());
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!list)
		xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to addressbk.update_profile");
    else
        delete list;

    if (rs)
    {
        if (rs->is_error())
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    /* Return our result. */
    return output;
}


// Remove an existing Profile (System Preferences/Priviledges)
//
// Parameters:
//   "SESSIONID", "PROFILEID"
//
// Returns:
//   "PROFILEID"
xmlrpc_value *remove_profile(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *profileid;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    char community_id[64];

    char countstring[32];
    int count = 0;

	xmlrpc_parse_value(env, param_array, "(ss)", &sid, &profileid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    // cannot delete system profile
    if (!strcmp(profileid, SYSTEM_PROFILEID))
    {
        xmlrpc_env_set_fault(env, ProfileSystemDeleteError, "Cannot delete System policy");
        goto cleanup;
    }

    // Get the record to remove.
    rs = db->execute("find-profile-by-id", profileid);
    if (!rs || rs->is_error())
        goto cleanup;

    // make sure id is valid
    if (rs->get_num_rows() == 0)
    {
       xmlrpc_env_set_fault(env, ProfileNotFound, "Profile ID not found");
       goto cleanup;
    }
    strcpy(community_id, rs->get_value(0, 3));
    db->closeResults(rs);

    // Make sure this is an administrator.
    if (!check_admin(env, sid, community_id))
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "You must be an administrator to update profile settings");
        goto cleanup;
    }

    // check that profile is not in use
    rs = db->execute("find-profile-usage", profileid);
    if (!rs || rs->is_error())
        goto cleanup;
    strcpy(countstring, rs->get_value(0, 0));
    count = atoi(countstring);
    db->closeResults(rs);

    if (count > 0)
    {
        xmlrpc_env_set_fault(env, ProfileInUseError, "Cannot delete policies that are in use");
        goto cleanup;
    }

    // delete the profile
    rs = db->execute("remove-profile", profileid);
    if (!rs || rs->is_error())
        goto cleanup;
    db->closeResults(rs);

    output = xmlrpc_build_value(env, "(s)", profileid);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Clean directory (removes all contacts and groups in specified directory).
// Parameters:
//   SESSIONID, DIRECTORYID
// Returns:
//   Number of rows deleted.
xmlrpc_value *clean_directory(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *dirid;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    int count = 0;
    bool is_service;

	xmlrpc_parse_value(env, param_array, "(ss)", &sid, &dirid);
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!is_service)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "API not valid for user sessions");
        XMLRPC_FAIL_IF_FAULT(env);
    }

    rs = db->execute("remove-all-groups", dirid, GROUPTYPE_ALL, GROUPTYPE_BUDDY);
    if (!rs || rs->is_error())
        goto cleanup;
    count += rs->get_num_affected();
    db->closeResults(rs);

    rs = db->execute("remove-all-contacts", dirid);
    if (!rs || rs->is_error())
        goto cleanup;
    count += rs->get_num_affected();

    // Orphaned contact records may be left behind--that's ok as the fetch operations
    // will not return contacts that are not in a group, and we can periodically find
    // and remove all orphaned contacts.

    output = xmlrpc_build_value(env, "(i)", count);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Subscribe to directory.
// Parameters:
//   SESSIONID, USERID, DIRECTORYID
// Returns:
//   DIRSUBID
//   Created = 1 if added to group, 0 if already in group
xmlrpc_value *subscribe_to_directory(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *userid, *dirid;
    ResultSet *rs = NULL;
    int retry = 2;
    xmlrpc_value *val = NULL;

	xmlrpc_parse_value(env, param_array, "(sss*)", &sid, &userid, &dirid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_directory(env, sid, NULL, dirid, false);
    XMLRPC_FAIL_IF_FAULT(env);

    while (retry--)
    {
        rs = db->execute("find-directory-subscription", sid, dirid);
        if (!rs || rs->is_error())
            goto cleanup;

        if (rs->get_num_rows() > 0)
        {
            val = xmlrpc_build_value(env, "(si)", rs->get_value(0, 0), retry < 1);
            goto cleanup;
        }
        db->closeResults(rs);

        rs = db->execute("subscribe-directory", sid, dirid);
        if (!rs || rs->is_error())
            goto cleanup;
        db->closeResults(rs);
    }

    log_error(ZONE, "Unable to subscribe to directory %s\n", dirid);
    xmlrpc_env_set_fault(env, AddressBookError, "Unable to subscribe to directory");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && val == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return val;
}


// Unsubscribe from directory.
// Parameters:
//   SESSIONID, USERID, DIRECTORYID
// Returns:
//   Rows deleted
xmlrpc_value *unsubscribe_from_directory(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *userid, *dirid;
    ResultSet *rs = NULL;
    xmlrpc_value *val = NULL;

	xmlrpc_parse_value(env, param_array, "(sss*)", &sid, &userid, &dirid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_directory(env, sid, NULL, dirid, false);
    XMLRPC_FAIL_IF_FAULT(env);

    rs = db->execute("unsubscribe-directory", sid, dirid);
    if (!rs || rs->is_error())
        goto cleanup;

    val = xmlrpc_build_value(env, "(i)", rs->get_num_affected());
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && val == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return val;
}


// Fetch directories (public and personal for specified person).
// Parameters:
//   SESSIONID, USERID, COMMUNITYID, FIRST, CHUNKSIZE
// Returns:
//   NAME, DIRECTORYID, TYPE, SUBSCRIBED
//   SUBSCRIBED = "t" if user is subscribed
xmlrpc_value *fetch_directories(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *userid, *communityid;
    int first, chunksize;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    char chunk_size_s[32], first_s[32];
    char usercommunityid[32];

	xmlrpc_parse_value(env, param_array, "(sssii)", &sid, &userid, &communityid, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    i_itoa(chunksize + 1, chunk_size_s);
    i_itoa(first, first_s);

    // Different query for superadmins logging on to a community
    // that is not their own...
    find_user_community(env, userid, usercommunityid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (strcmp(usercommunityid, communityid) != 0)
    {
        if (check_superadmin(env, sid))
        {
            rs = db->execute("fetch-directories-for-superadmin", communityid, chunk_size_s, first_s);
        }
        else
        {
            xmlrpc_env_set_fault(env, NotAuthorized, "Wrong community");
            goto cleanup;
        }
    }
    else
    {
        rs = db->execute("fetch-directories", communityid, sid, chunk_size_s, first_s);
    }
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, first, 0, chunksize, "fetch_directories");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch groups in specified directories.
// Parameters:
//   SESSIONID, DIRECTORIES, FIRST, CHUNKSIZE
// Returns:
//   Rows of NAME, GROUPID, DIRECTORYID, TYPE
xmlrpc_value *fetch_groups(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid;
    int first, chunksize;
    xmlrpc_value *directory_array;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    char chunk_size_s[32], first_s[32];
    IString directorystr;

	xmlrpc_parse_value(env, param_array, "(sAii)", &sid, &directory_array, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!build_string_list(env, directory_array, directorystr, false, NULL))
        goto cleanup;

    check_directories(env, sid, directorystr, xmlrpc_array_size(env, directory_array));
    XMLRPC_FAIL_IF_FAULT(env);

    i_itoa(chunksize + 1, chunk_size_s);
    i_itoa(first, first_s);

    rs = db->execute("fetch-groups", (const char*)directorystr, chunk_size_s, first_s);
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, first, 0, chunksize, "fetch_groups");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch groups in specified directories and return in packed format.
// Parameters:
//   SESSIONID, DIRECTORIES, LASTMODFIED, FIRST, CHUNKSIZE
// Returns:
//   Rows of NAME, GROUPID, DIRECTORYID, TYPE, LASTMODIFIED
xmlrpc_value *fetch_groups_2(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid;
    int first, chunksize, lastmodified;
    xmlrpc_value *directory_array;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    char chunk_size_s[32], first_s[32], lastmodified_s[32];
    IString directorystr;

	xmlrpc_parse_value(env, param_array, "(sAiii)", &sid, &directory_array, &lastmodified, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!build_string_list(env, directory_array, directorystr, false, NULL))
        goto cleanup;

    check_directories(env, sid, directorystr, xmlrpc_array_size(env, directory_array));
    XMLRPC_FAIL_IF_FAULT(env);

    i_itoa(chunksize + 1, chunk_size_s);
    i_itoa(first, first_s);
    i_itoa(lastmodified, lastmodified_s);

    rs = db->execute("fetch-groups-modified", (const char*)directorystr, lastmodified_s, chunk_size_s, first_s);
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_packed_output(env, rs, first, 0, chunksize, "fetch_groups_2");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch groups in specified directories and return in packed format.
// Parameters:
//   SESSIONID, DIRECTORIES, LASTMODFIED, FIRST, CHUNKSIZE
// Returns:
//    Array of deleted groups:
//       GROUPID, DIRECTORYID, LASTMODIFIED
//
//   followed by:
//       FIRST, CHUNKSIZE, TOTALROWS, MOREAVAILABLE
//
xmlrpc_value *fetch_deleted_groups(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid;
    int first, chunksize, lastmodified;
    xmlrpc_value *directory_array;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    char chunk_size_s[32], first_s[32], lastmodified_s[32];
    IString directorystr;

	xmlrpc_parse_value(env, param_array, "(sAiii)", &sid, &directory_array, &lastmodified, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!build_string_list(env, directory_array, directorystr, false, NULL))
        goto cleanup;

    check_directories(env, sid, directorystr, xmlrpc_array_size(env, directory_array));
    XMLRPC_FAIL_IF_FAULT(env);

    i_itoa(chunksize + 1, chunk_size_s);
    i_itoa(first, first_s);
    i_itoa(lastmodified, lastmodified_s);

    rs = db->execute("fetch-groups-deleted", (const char*)directorystr, lastmodified_s, chunk_size_s, first_s);
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_packed_output(env, rs, first, 0, chunksize, "fetch_deleted_groups");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch unique values for specfied field in directories.
// Parameters:
//   SESSIONID, FIELD, DIRECTORIES, FIRST, CHUNKSIZE
// Returns:
//   NAME
xmlrpc_value *fetch_group_by_values(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *field;
    int first, chunksize;
    xmlrpc_value *directory_array;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    char chunk_size_s[32], first_s[32];
    IString directorystr;

	xmlrpc_parse_value(env, param_array, "(ssAii)", &sid, &field, &directory_array, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!build_string_list(env, directory_array, directorystr, false, NULL))
        goto cleanup;

    check_directories(env, sid, directorystr, xmlrpc_array_size(env, directory_array));
    XMLRPC_FAIL_IF_FAULT(env);

    i_itoa(chunksize + 1, chunk_size_s);
    i_itoa(first, first_s);

    rs = db->execute("fetch-group-by-value", field, (const char *)directorystr, chunk_size_s, first_s);
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, first, 0, chunksize, "fetch_group_by_values");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch group members.
// Parameters:
//   SESSIONID, DIRECTORIES, FIRST, CHUNKSIZE
// Returns:
//   GROUP ID, USER ID
xmlrpc_value *fetch_group_members(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid;
    int first, chunksize;
    xmlrpc_value *directory_array;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    char chunk_size_s[32], first_s[32];
    IString directorystr;

	xmlrpc_parse_value(env, param_array, "(sAii)", &sid, &directory_array, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!build_string_list(env, directory_array, directorystr, false, NULL))
        goto cleanup;

    check_directories(env, sid, directorystr, xmlrpc_array_size(env, directory_array));
    XMLRPC_FAIL_IF_FAULT(env);

    i_itoa(chunksize + 1, chunk_size_s);
    i_itoa(first, first_s);

    rs = db->execute("fetch-group-members", (const char *)directorystr, chunk_size_s, first_s);
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, first, 0, chunksize, "fetch_group_members");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch group members and return in packed format.
// Parameters:
//   SESSIONID, DIRECTORIES, LASTMODIFIED, FIRST, CHUNKSIZE
// Returns:
//   GROUP ID, USER ID
xmlrpc_value *fetch_group_members_2(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid;
    int first, chunksize, lastmodified;
    xmlrpc_value *directory_array;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    char chunk_size_s[32], first_s[32], lastmodified_s[32];
    IString directorystr;

	xmlrpc_parse_value(env, param_array, "(sAiii)", &sid, &directory_array, &lastmodified, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!build_string_list(env, directory_array, directorystr, false, NULL))
        goto cleanup;

    check_directories(env, sid, directorystr, xmlrpc_array_size(env, directory_array));
    XMLRPC_FAIL_IF_FAULT(env);

    i_itoa(chunksize + 1, chunk_size_s);
    i_itoa(first, first_s);
    i_itoa(lastmodified, lastmodified_s);

    rs = db->execute("fetch-group-members-modified", (const char *)directorystr, lastmodified_s, chunk_size_s, first_s);
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_packed_output(env, rs, first, 0, chunksize, "fetch_group_members_2");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch profile records
// Parameters:
//   SESSIONID, COMMUNITYID, FIRST, CHUNKSIZE
//
// Returns:
//   Array with rows of profile data, each row contains:
//       PROFILEID, PROFILENAME, ENABLEDFEATURES, PROFILEOPTIONS
//
//   followed by:
//       FIRST, CHUNKSIZE, TOTALROWS, MOREAVAILABLE
//
xmlrpc_value *fetch_profiles(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *communityid;
    int first, chunksize;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    char chunk_size_s[32], first_s[32];

	xmlrpc_parse_value(env, param_array, "(ssii)", &sid, &communityid, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    // Currently allowing read access to any profile

    i_itoa(chunksize + 1, chunk_size_s);
    i_itoa(first, first_s);

    rs = db->execute("fetch-profiles", communityid, chunk_size_s, first_s);
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, first, 0, chunksize, "fetch_profiles");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch contacts.
// Parameters:
//   SESSIONID, DIRECTORIES (array), GROUPING, SORT, FIELDS (array), FIRST, CHUNKSIZE
// Returns:
//   values for selected fields
xmlrpc_value *fetch_contacts(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *grouping, *sort;
    int first, chunksize;
    xmlrpc_value *directory_array;
    xmlrpc_value *output = NULL;
    xmlrpc_value *fields = NULL;
    ResultSet *rs = NULL;
    char chunk_size_s[32], first_s[32];
    IString querynamestr;
    IString directorystr;
    IString fieldstr;
    IString sortstr;

	xmlrpc_parse_value(env, param_array, "(sAssAii)", &sid, &directory_array, &grouping,
            &sort, &fields, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!build_string_list(env, directory_array, directorystr, false, NULL))
        goto cleanup;

    check_directories(env, sid, directorystr, xmlrpc_array_size(env, directory_array));
    XMLRPC_FAIL_IF_FAULT(env);

    if (!build_string_list(env, fields, fieldstr, true, "CONTACTS."))
        goto cleanup;

    querynamestr = "fetch-contacts";

    // Group by:  directory, company, group, none
    // Sort specifies secondary sort order within grouping
    if (!strcmp(grouping, "group"))
    {
        querynamestr = "fetch-contacts-in-groups";
        sortstr = "GROUPS.NAME";
    }
    else if (!strcmp(grouping, "directory"))
        sortstr = "DIRECTORIES.NAME";
    else if (!strcmp(grouping, "company"))
        sortstr = "CONTACTS.COMPANY";
    else
    {
        sortstr = "CONTACTS.LASTNAME, CONTACTS.FIRSTNAME";
    }

    if (strlen(sort))
        sortstr.append(", CONTACT.").append(sort);

    rs = db->execute(querynamestr, (const char *)fieldstr, (const char *)directorystr,
            (const char *)sortstr, i_itoa(chunksize + 1, chunk_size_s), i_itoa(first, first_s));
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, first, 0, chunksize, "fetch_contacts");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch contacts and return in packed format.
// Parameters:
//   SESSIONID, DIRECTORIES (array), FIELDS (array), LASTMODIFIED, FIRST, CHUNKSIZE
// Returns:
//   values for selected fields
xmlrpc_value *fetch_contacts_2(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid;
    int first, chunksize, lastmodified;
    xmlrpc_value *directory_array;
    xmlrpc_value *output = NULL;
    xmlrpc_value *fields = NULL;
    ResultSet *rs = NULL;
    char chunk_size_s[32], first_s[32], lastmodified_s[32];
    IString directorystr;
    IString fieldstr;

	xmlrpc_parse_value(env, param_array, "(sAAiii)", &sid, &directory_array, &fields,
            &lastmodified, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!build_string_list(env, directory_array, directorystr, false, NULL))
        goto cleanup;

    check_directories(env, sid, directorystr, xmlrpc_array_size(env, directory_array));
    XMLRPC_FAIL_IF_FAULT(env);

    if (!build_string_list(env, fields, fieldstr, true, "CONTACTS."))
        goto cleanup;

    rs = db->execute("fetch-contacts-modified", (const char *)fieldstr, (const char *)directorystr,
            i_itoa(lastmodified, lastmodified_s), i_itoa(chunksize + 1, chunk_size_s), i_itoa(first, first_s));
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_packed_output(env, rs, first, 0, chunksize, "fetch_contacts_2");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch deleted contacts and return in packed format.
// Parameters:
//   SESSIONID, DIRECTORIES (array), LASTMODIFIED, FIRST, CHUNKSIZE
// Returns:
//    Array of deleted contacts:
//       USERID, DIRECTORYID, LASTMODIFIED
//
//   followed by:
//       FIRST, CHUNKSIZE, TOTALROWS, MOREAVAILABLE
//
xmlrpc_value *fetch_deleted_contacts(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid;
    int first, chunksize, lastmodified;
    xmlrpc_value *directory_array;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    char chunk_size_s[32], first_s[32], lastmodified_s[32];
    IString directorystr;

	xmlrpc_parse_value(env, param_array, "(sAiii)", &sid, &directory_array,
            &lastmodified, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!build_string_list(env, directory_array, directorystr, false, NULL))
        goto cleanup;

    check_directories(env, sid, directorystr, xmlrpc_array_size(env, directory_array));
    XMLRPC_FAIL_IF_FAULT(env);

    rs = db->execute("fetch-contacts-deleted", (const char *)directorystr,
            i_itoa(lastmodified, lastmodified_s), i_itoa(chunksize + 1, chunk_size_s), i_itoa(first, first_s));
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_packed_output(env, rs, first, 0, chunksize, "fetch_deleted_contacts");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch a single contact.
// Parameters:
//   SESSIONID, USERID, FIELDS (array)
// Returns:
//   values for selected fields
xmlrpc_value *fetch_contact(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *userid;
    xmlrpc_value *output = NULL;
    xmlrpc_value *fields = NULL;
    ResultSet *rs = NULL;
    IString fieldstr;
    char dirid[32];

	xmlrpc_parse_value(env, param_array, "(ssA)", &sid, &userid, &fields);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    // ToDo: build access checks into fetch to avoid extra SQL executions

    find_contact_directory(env, userid, dirid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_directory(env, sid, NULL, dirid, true);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!build_string_list(env, fields, fieldstr, true, "CONTACTS."))
        goto cleanup;

    rs = db->execute("fetch-contact-by-userid", userid, (const char *)fieldstr);
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, 0, 0, 1, "fetch_contact");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Search for contact.
// Parameters:
//   SESSIONID, DIRECTORIES (array), SORT, FIELDS (array)
//   CONDITIONS (array), OPERATORS (array), VALUES (array),
//   FIRST, CHUNKSIZE
//   Operators are =, >, <, <>, LIKE, LIKE_NOCASE
// Returns:
//   USERID, DIRECTORYID, all contact fields
xmlrpc_value *search_contacts(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *sort;
    int first, chunksize;
    xmlrpc_value *output = NULL;
    xmlrpc_value *directory_array;
    xmlrpc_value *fields = NULL;
    xmlrpc_value *conditions = NULL;
    xmlrpc_value *ops = NULL;
    xmlrpc_value *values = NULL;
    xmlrpc_value *contact_conditions = NULL;
    xmlrpc_value *contact_ops = NULL;
    xmlrpc_value *contact_values = NULL;
    ResultSet *rs = NULL;
    char chunk_size_s[32], first_s[32];
    IString directorystr;
    IString searchstr;
    IString groupstr;
    IString fieldstr;
    IString sortstr;

	xmlrpc_parse_value(env, param_array, "(sAsAAAAii)", &sid, &directory_array, &sort,
            &fields, &conditions, &ops, &values, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    // Create arrays to hold search criteria for contact fields
	contact_conditions = xmlrpc_build_value(env, "()");
	XMLRPC_FAIL_IF_FAULT(env);
	contact_ops = xmlrpc_build_value(env, "()");
	XMLRPC_FAIL_IF_FAULT(env);
	contact_values = xmlrpc_build_value(env, "()");
	XMLRPC_FAIL_IF_FAULT(env);

    // Build SQL statement using fields, ops, and values as conditions
    if (!build_string_list(env, directory_array, directorystr, false, NULL))
        goto cleanup;

    check_directories(env, sid, directorystr, xmlrpc_array_size(env, directory_array));
    XMLRPC_FAIL_IF_FAULT(env);

    if (!build_string_list(env, fields, fieldstr, true, "CONTACTS."))
        goto cleanup;

    if (!extract_group_list(env, groupstr, conditions, ops, values, contact_conditions, contact_ops, contact_values))
        goto cleanup;

    if (xmlrpc_array_size(env, contact_conditions) != 0)
    {
        searchstr.append(" AND ");

        if (!build_condition(env, searchstr, contact_conditions, contact_ops, contact_values, "CONTACTS."))
            goto cleanup;
    }

    if (strlen(sort) == 0)
        sortstr = "CONTACTS.LASTNAME, CONTACTS.FIRSTNAME";
    else
        sortstr = IString("CONTACTS.") + sort;

    if (groupstr.length() == 0)
    {
        rs = db->execute("find-contacts", (const char *)fieldstr, (const char *)directorystr,
                (const char *)searchstr, (const char *)sortstr,
                i_itoa(chunksize + 1, chunk_size_s), i_itoa(first, first_s));
    }
    else
    {
        rs = db->execute("find-contacts-in-group", (const char *)fieldstr, (const char *)directorystr,
                (const char *)groupstr, (const char *)searchstr, (const char *)sortstr,
                i_itoa(chunksize + 1, chunk_size_s), i_itoa(first, first_s));
    }

    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, first, 0, chunksize, "fetch_contacts");

cleanup:
    if (contact_conditions != NULL)
    {
        xmlrpc_DECREF(contact_conditions);
    }

    if (contact_ops != NULL)
    {
        xmlrpc_DECREF(contact_ops);
    }

    if (contact_values != NULL)
    {
        xmlrpc_DECREF(contact_values);
    }

    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Find current presence subscription
//
// Returns true if the subscription info was found.
//
bool find_presence_subscription(xmlrpc_env *env, const char *username, const char *jid, char *subscription)
{
    ResultSet *rs = NULL;
    bool bFound = FALSE;

    rs = db->execute("presence-get-subscription", username, jid);
    if (!rs || rs->is_error())
        goto cleanup;
    if (rs->get_num_rows() > 0)
    {
        bFound = TRUE;
        strcpy(subscription, rs->get_value(0, 0));
    }
    else
    {
        strcpy(subscription, "N");
    }
    db->closeResults(rs);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }

    return bFound;
}

// Set current presence subscription
void set_presence_subscription(xmlrpc_env *env, bool insert, const char *username, const char *jid, const char *subscription)
{
	ResultSet *rs = NULL;

    if (insert)
    {
        rs = db->execute("presence-insert-subscription", username, jid, subscription);
    }
    else
    {
        rs = db->execute("presence-update-subscription", username, jid, subscription);
    }
    if (!rs || rs->is_error())
        goto cleanup;
    db->closeResults(rs);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}

// Update trustee list for subscribee to add/remove subscribed user
void update_roster(const char * username, const char * jid, const char * action)
{
    IString iq;
    int subscribe;

	if (strcmp(action, "S") == 0)
		subscribe = 1;
	else
		subscribe = 0;

    // send message to adjust trustee list for this user
	iq = IString("<") + IString(subscribe ? "add" : "remove") + IString(" user='") +
	     username + IString("' subscriber='") + jid + IString("'/>");
	rpc_send_iq_request("roster", "roster", "iic:roster", iq.get_string());
}

// Update the XDB_SQL rosterusers table to set up a Presence subscription.
//
// Parameters:
//   SUBSCRIBER_USERNAME, SUBSCRIBEE_USERNAME, ACTION
//
// Returns:
//   New subscription value for the subscriber (N, F, T, or B)
//
// NOTES
//   SUBSCRIBER_USERNAME is a JID
//
//   SUBSCRIBEE_USERNAME is a JID
//
//   If action is S (subscribe), the subscriber wants to
//   receive presence notifications from the subscribee.
//
//   If action is U (unsubscribe), the subscriber does not want to
//   receive presence notifications from the subscribee.
//
xmlrpc_value *update_presence_subcription(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    ResultSet *rs = NULL;
    bool bIsSubscriberInsert = FALSE;
    bool bIsSubscribeeInsert = FALSE;
    char *sid, *subscriber_username, *subscribee_username, *action;
    char subscriber_subscription[32], subscribee_subscription[32];
    xmlrpc_value *output = NULL;
    UserSession *session;
    bool is_service;

	xmlrpc_parse_value(env, param_array, "(ssss)", &sid, &subscriber_username, &subscribee_username, &action);
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    session = lookup_session(env, user_data);
    XMLRPC_FAIL_IF_FAULT(env);

    // Verify user is updating his own roster
    if (strcmp(session->jid, subscriber_username) && !is_service)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "User may only update his own roster");
        goto cleanup;
    }

    // Get the current subscription values
    bIsSubscriberInsert = !find_presence_subscription(env, subscriber_username, subscribee_username, subscriber_subscription);
    XMLRPC_FAIL_IF_FAULT(env);
    bIsSubscribeeInsert = !find_presence_subscription(env, subscribee_username, subscriber_username, subscribee_subscription);
    XMLRPC_FAIL_IF_FAULT(env);

    // Figure out the new subscription values...
    if (!strcmp(action, "S"))
    {
        // Action is subscribe...
        if (!strcmp(subscriber_subscription, "N"))
        {
            strcpy(subscriber_subscription, "T");
            strcpy(subscribee_subscription, "F");
        }
        else if (!strcmp(subscriber_subscription, "F"))
        {
            strcpy(subscriber_subscription, "B");
            strcpy(subscribee_subscription, "B");
        }
        else if (!strcmp(subscriber_subscription, "T"))
        {
            strcpy(subscribee_subscription, "F");
        }
        else if (!strcmp(subscriber_subscription, "B"))
        {
            strcpy(subscribee_subscription, "B");
        }
    }
    else
    {
        // Action is unsubscribe...
        if (!strcmp(subscriber_subscription, "N"))
        {
            strcpy(subscribee_subscription, "N");
        }
        if (!strcmp(subscriber_subscription, "T"))
        {
            strcpy(subscriber_subscription, "N");
            strcpy(subscribee_subscription, "N");
        }
        else if (!strcmp(subscriber_subscription, "F"))
        {
            strcpy(subscribee_subscription, "T");
        }
        else if (!strcmp(subscriber_subscription, "B"))
        {
            strcpy(subscriber_subscription, "F");
            strcpy(subscribee_subscription, "T");
        }
    }

    set_presence_subscription(env, bIsSubscriberInsert, subscriber_username, subscribee_username, subscriber_subscription);
    XMLRPC_FAIL_IF_FAULT(env);
    set_presence_subscription(env, bIsSubscribeeInsert, subscribee_username, subscriber_username, subscribee_subscription);
    XMLRPC_FAIL_IF_FAULT(env);

    update_roster(subscribee_username, subscriber_username, action);

    output = xmlrpc_build_value(env, "(s)", subscriber_subscription);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

// Update jabber roster information to suscribe presence for the given users
void update_presence_subcription_internal(xmlrpc_env *env, const char *subscriber_username, const char *subscribee_username, const char *action, char *new_subscription)
{
    ResultSet *rs = NULL;
    bool bIsSubscriberInsert = FALSE;
    bool bIsSubscribeeInsert = FALSE;
    char subscriber_subscription[32], subscribee_subscription[32];

    // Get the current subscription values
    bIsSubscriberInsert = !find_presence_subscription(env, subscriber_username, subscribee_username, subscriber_subscription);
    XMLRPC_FAIL_IF_FAULT(env);
    bIsSubscribeeInsert = !find_presence_subscription(env, subscribee_username, subscriber_username, subscribee_subscription);
    XMLRPC_FAIL_IF_FAULT(env);

    // Figure out the new subscription values...
    if (!strcmp(action, "S"))
    {
        // Action is subscribe...
        if (!strcmp(subscriber_subscription, "N"))
        {
            strcpy(subscriber_subscription, "T");
            strcpy(subscribee_subscription, "F");
        }
        else if (!strcmp(subscriber_subscription, "F"))
        {
            strcpy(subscriber_subscription, "B");
            strcpy(subscribee_subscription, "B");
        }
        else if (!strcmp(subscriber_subscription, "T"))
        {
            strcpy(subscribee_subscription, "F");
        }
        else if (!strcmp(subscriber_subscription, "B"))
        {
            strcpy(subscribee_subscription, "B");
        }
    }
    else
    {
        // Action is unsubscribe...
        if (!strcmp(subscriber_subscription, "N"))
        {
            strcpy(subscribee_subscription, "N");
        }
        if (!strcmp(subscriber_subscription, "T"))
        {
            strcpy(subscriber_subscription, "N");
            strcpy(subscribee_subscription, "N");
        }
        else if (!strcmp(subscriber_subscription, "F"))
        {
            strcpy(subscribee_subscription, "T");
        }
        else if (!strcmp(subscriber_subscription, "B"))
        {
            strcpy(subscriber_subscription, "F");
            strcpy(subscribee_subscription, "T");
        }
    }

    set_presence_subscription(env, bIsSubscriberInsert, subscriber_username, subscribee_username, subscriber_subscription);
    XMLRPC_FAIL_IF_FAULT(env);
    set_presence_subscription(env, bIsSubscribeeInsert, subscribee_username, subscriber_username, subscribee_subscription);
    XMLRPC_FAIL_IF_FAULT(env);

    update_roster(subscribee_username, subscriber_username, action);

    strcpy(new_subscription, subscriber_subscription);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}

// retrieves the max number of presence monitors allowed as set in the passed
// users profile
int fetch_max_monitors(const char* userid, int &max_presence_monitors)
{
    int status = Ok;
    ResultSet *rs = NULL;
    const char *profile_options;
    IString profile_options_s;

    // find user's profile record based on their userid
    if (strlen(userid) > 0)
    {
        rs = db->execute("find-profile-by-userid", userid);
        if (!rs || rs->is_error())
            goto cleanup;
        profile_options = rs->get_value(0, "PROFILEOPTIONS");
        profile_options_s = profile_options;
        db->closeResults(rs);

        // decode profile into meeting PIN length and personal (invitee) PIN length
        unsigned int enabled_features;
        int max_voice_size, max_data_size, password_length;
        int personal_pin_length, meeting_pin_length;
        IString bridge_phone_str, system_url_str;
        int max_scheduled_meetings;

        if (!DecodeProfileOptions(enabled_features,
                                  profile_options_s,
                                  max_voice_size,
                                  max_data_size,
                                  max_presence_monitors,
                                  personal_pin_length,
                                  meeting_pin_length,
                                  password_length,
                                  bridge_phone_str,
                                  system_url_str,
                                  max_scheduled_meetings))
        {
            status = Failed;
            goto cleanup;
        }
    }
    else
    {
        status = Failed;
        goto cleanup;
    }

cleanup:
	if (rs != NULL)
		db->closeResults(rs);

    return status;
}

// Add a buddy
// Parameters:
//   SESSIONID, SUBSCRIBEE_USERID, SUBSCRIBER_USERNAME, SUBSCRIBEE_USERID, SUBSCRIBEE_USERNAME, SUBSCRIBEE_SCREENNAME,
//   SUBSCRIBEE_FIRSTNAME, SUBSCRIBEE_LASTNAME, RESTRICT_DIRECTORY, ACTION
// Returns:
//   Row 1 - USERID, DIRECTORYID, all contact fields
//   Row 2 - USERID (of inter-community buddy record, or blank otherwise)
xmlrpc_value *add_buddy(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    ResultSet *rs = NULL;
    char *sid, *subscriber_userid, *subscriber_username, *subscribee_username, *subscribee_userid;
    char *subscribee_screen, *subscribee_first, *subscribee_last, *restrict_directory, *action;
    xmlrpc_value *output = NULL;

	xmlrpc_value *outputarray = NULL;
	xmlrpc_value *buddyrow = NULL;
    xmlrpc_value *buddyvalue = NULL;

    bool bRestrictDirectory = true;
    bool bExistsInMyDirectory = false;
    bool bExistsInAnyDirectory = false;
    char communityid[32];
    char dir[32];
    char defaultgroup[32];
    char buddygroup[32];
    char buddyuserid[32];
    char interbuddyuserid[32] = {0};  // used if the buddy resides in another community
    char new_subscription[32];
    char countstring[32];
    int current_presence_monitors;
    int max_presence_monitors;
    UserSession *session;
    bool is_service;

    char usertype[32];
    sprintf(usertype, "%d", UserTypeContact);

    char profileid[32];
    sprintf(profileid, "%s", CONTACT_PROFILEID);

    int num_params = xmlrpc_array_size(env, param_array);
	if (num_params == 9)
	{
		xmlrpc_parse_value(env, param_array, "(sssssssss)",
                        	 &sid,
                        	 &subscriber_username,
                        	 &subscribee_userid,
                        	 &subscribee_username,
                        	 &subscribee_screen,
                        	 &subscribee_first,
                        	 &subscribee_last,
                        	 &restrict_directory,
                        	 &action
                        	 );
		subscriber_userid = sid;
	}
	else
	{
		xmlrpc_parse_value(env, param_array, "(ssssssssss)",
                        	 &sid,
							 &subscriber_userid,
                        	 &subscriber_username,
                        	 &subscribee_userid,
                        	 &subscribee_username,
                        	 &subscribee_screen,
                        	 &subscribee_first,
                        	 &subscribee_last,
                        	 &restrict_directory,
                        	 &action
                        	 );
	}
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    session = lookup_session(env, user_data);
    XMLRPC_FAIL_IF_FAULT(env);

    // Verify user is updating his own roster
    if (strcmp(session->jid, subscriber_username) && !is_service)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "User may only update his own roster");
        goto cleanup;
    }

    // Verify user is within profile max
    if (fetch_max_monitors(subscriber_userid, max_presence_monitors) == Failed)
    {
		xmlrpc_env_set_fault(env, ProfileNotFound, "Error reading max presence monitors");
        goto cleanup;
    }

    // figure out how many monitors are in place for this user
    rs = db->execute("find-presence-subscription-count", subscriber_username);
    if (!rs || rs->is_error())
        goto cleanup;
    strcpy(countstring, rs->get_value(0, 0));
    current_presence_monitors = atoi(countstring);
    db->closeResults(rs);

    if (current_presence_monitors >= max_presence_monitors)
    {
		xmlrpc_env_set_fault(env, MaxPresenceExceeded, "Max presence monitors exceeded");
        goto cleanup;
    }

    // Now find user information
    find_user_community(env, subscriber_userid, communityid);
    XMLRPC_FAIL_IF_FAULT(env);

    find_user_directory(env, communityid, subscriber_userid, dir);
    XMLRPC_FAIL_IF_FAULT(env);

    find_default_group(env, dir, defaultgroup);
    XMLRPC_FAIL_IF_FAULT(env);

    find_buddy_group(env, dir, buddygroup);
    XMLRPC_FAIL_IF_FAULT(env);

    if (strcmp(restrict_directory, "f") == 0)
        bRestrictDirectory = false;

    // search in user's directory to see if contact exists
    rs = db->execute("find-buddy-from-screen", dir, subscribee_screen);
    if (!rs || rs->is_error())
        goto cleanup;
    if (rs->get_num_rows() > 0)
    {
        const char *val = rs->get_value(0, 0);
        if (val != NULL)
        {
            strcpy(buddyuserid, val);
            bExistsInMyDirectory = true;
        }
    }
    db->closeResults(rs);

    // if not restricted to own directory search across all directories to see if contact exists
    if (!bExistsInMyDirectory && !bRestrictDirectory)
    {
        rs = db->execute("find-buddy-from-screen-all", subscribee_screen);
        if (!rs || rs->is_error())
            goto cleanup;
        if (rs->get_num_rows() > 0)
        {
            const char *val = rs->get_value(0, 0);
            if (val != NULL)
            {
                strcpy(buddyuserid, val);
                strcpy(interbuddyuserid, val);
                bExistsInAnyDirectory = true;
            }
        }
        db->closeResults(rs);
    }

    // if not restricted and no contact, return error
    if (!bRestrictDirectory && !bExistsInMyDirectory && !bExistsInAnyDirectory)
    {
        xmlrpc_env_set_fault(env, UserNotFound, "Unable to find user");
        goto cleanup;
    }

    // if contact doesn't exist in user's directory, create copy
    if (!bExistsInMyDirectory)
    {
        // Create new contact and add it to the default and buddy directories
        // Create CONTACT record

        // Get new user
        rs = db->execute("allocate-userid");
        if (!rs || rs->is_error())
            goto cleanup;
        strcpy(buddyuserid, rs->get_value(0, 0));
        db->closeResults(rs);

        // USERID, DIRECTORYID, USERNAME, SERVER,
        // TITLE, FIRSTNAME, MIDDLENAME, LASTNAME, SUFFIX,
        // COMPANY, JOBTITLE, ADDRESS1, ADDRESS2, STATE, COUNTRY, POSTALCODE,
        // SCREENNAME, AIMNAME, EMAIL, EMAIL2, EMAIL3, BUSPHONE, BUSDIRECT,
        // HOMEPHONE, HOMEDIRECT, MOBILEPHONE, MOBILEDIRECT, OTHERPHONE, OTHERDIRECT,
        // EXTENSION, USER1, USER2, USER3, USER4, USER5,
        // DEFPHONE, DEFEMAIL,
        // TYPE, PROFILEID, MAXSIZE, MAXPRIORITY, PRIORITYTYPE,
        // ENABLEDFEATURES, DISABLEDFEATURES
        rs = db->execute("add-contact",
            buddyuserid, dir, "", "",
            "" /* title */, subscribee_first, "", subscribee_last, "",
            "" /* company */, "", "", "", "", "", "",
            subscribee_screen, "", "", "", "", "", "false",
            "" /* homephone */, "false", "", "false", "", "false",
            "" /* extension */, "", "", "", "", "",
            "0" /* defphone */, "0",
            usertype, profileid, "0", "0", "0",
            "0", "0");
        db->closeResults(rs);

        // Add new contact to the personal default group
        add_user_to_group(env, defaultgroup, buddyuserid);
        XMLRPC_FAIL_IF_FAULT(env);

        // Add new contact to the personal buddy group
        add_user_to_group(env, buddygroup, buddyuserid);
        XMLRPC_FAIL_IF_FAULT(env);
    }
    else
    {
        // Add buddy user to buddy group if not already there
        rs = db->execute("find-group-member", buddygroup, buddyuserid);
        if (!rs || rs->is_error())
            goto cleanup;
        if (rs->get_num_rows() == 0)
        {
            db->closeResults(rs);

            add_user_to_group(env, buddygroup, buddyuserid);
            XMLRPC_FAIL_IF_FAULT(env);
        }
        db->closeResults(rs);
    }

    // Update the jabber roster
    update_presence_subcription_internal(env, subscriber_username, subscribee_username, action, new_subscription);
    XMLRPC_FAIL_IF_FAULT(env);

    // Now get the actual buddy record so we can return it...
    rs = db->execute("find-contact-all-by-userid", buddyuserid);
    if (!rs || rs->is_error())
        goto cleanup;

    // Will have two rows, the resultset and our extra
    // inter-community buddy we add below...
    output = build_output(env, rs, 0, 0, 1, "add_buddy");

    // Create our extra row to return data for a inter-community buddy...
	buddyrow = xmlrpc_build_value(env, "()");
	XMLRPC_FAIL_IF_FAULT(env);

    buddyvalue = xmlrpc_build_value(env, "s", interbuddyuserid);
    XMLRPC_FAIL_IF_FAULT(env);

    xmlrpc_array_append_item(env, buddyrow, buddyvalue);
    xmlrpc_DECREF(buddyvalue);
    XMLRPC_FAIL_IF_FAULT(env);

    // Append the row to the output array
	outputarray = xmlrpc_array_get_item(env, output, 0);
    XMLRPC_FAIL_IF_FAULT(env);

    xmlrpc_array_append_item(env, outputarray, buddyrow);
	xmlrpc_DECREF(buddyrow);
	XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

// Fetch user records for buddies in other communities.
// Parameters:
//   SESSIONID, USERNAME, COMMUNITYID, FIELDS (array), FIRST, CHUNKSIZE
// Returns:
//   values for selected fields
xmlrpc_value *fetch_intercommunity_buddies(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *username, *communityid;
    int first, chunksize;
    xmlrpc_value *fields = NULL;
    ResultSet *rs = NULL;
    char chunk_size_s[32], first_s[32];
    IString fieldstr;
    xmlrpc_value *output = NULL;
    UserSession *session;

	xmlrpc_parse_value(env, param_array, "(sssAii)",
        &sid, &username, &communityid, &fields, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    session = lookup_session(env, user_data);
    XMLRPC_FAIL_IF_FAULT(env);

    if (session->jid != username || session->community_id != communityid)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "User may only fetch his own buddies");
        goto cleanup;
    }

    if (!build_string_list(env, fields, fieldstr, true, "CONTACTS."))
        goto cleanup;

    rs = db->execute("fetch-intercommunity-buddies",
        username,
        communityid,
        (const char *)fieldstr,
        i_itoa(chunksize + 1, chunk_size_s),
        i_itoa(first, first_s));

    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, first, 0, chunksize, "fetch_intercommunity_buddies");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

// Fetch user records for buddies in our community.
// Parameters:
//   SESSIONID, USERNAME, COMMUNITYID, FIELDS (array), FIRST, CHUNKSIZE
// Returns:
//   values for selected fields
xmlrpc_value *fetch_buddy_contacts(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *username, *communityid;
    int first, chunksize;
    xmlrpc_value *fields = NULL;
    ResultSet *rs = NULL;
    char chunk_size_s[32], first_s[32];
    IString fieldstr;
    xmlrpc_value *output = NULL;
    UserSession *session;

    xmlrpc_parse_value(env, param_array, "(sssAii)",
                       &sid, &username, &communityid,
                       &fields, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    session = lookup_session(env, user_data);
    XMLRPC_FAIL_IF_FAULT(env);

    if (session->jid != username || session->community_id != communityid)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "User may only fetch his own buddies");
        goto cleanup;
    }

    if (!build_string_list(env, fields, fieldstr, true, "CONTACTS."))
        goto cleanup;

    rs = db->execute("fetch-buddy-contacts",
        username,
        communityid,
        (const char *)fieldstr,
        i_itoa(chunksize + 1, chunk_size_s),
        i_itoa(first, first_s));

    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, first, 0, chunksize, "fetch_buddy_contacts");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

// Add a community - super admin only
//
// Parameters:
//     "SESSIONID", "COMMUNITYNAME", "ACCOUNTNUMBER",
//     "CONTACTNAME", "CONTACTEMAIL", "CONTACTPHONE",
//     "USERNAME", "USERPASSWORD", "USERSCREEN",
//     "USERISADMIN" (0=normal contact, 1=admin),
//     "BRIDGEPHONE", "SYSTEMURL", "OPTIONS"
//     "DEFAULTPROFILEID"
//
//   Note: DEFAULTPROFILEID specified a community that will be copied
//         into the new created community.  If none is specified, a
//         profile will be created using internal defaults.
//
// Returns:
//     COMMUNITYID of newly added community
//
xmlrpc_value *add_community(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *communityname, *accountnumber;
    char *contactname, *contactemail, *contactphone;
    char *username, *userpassword, *userscreen;
    char *bridgephone, *systemurl, *defaultprofileid;
    int userisadmin, options;
    char userisadmin_s[32], options_s[32];
    int profileid;
    int newusertype = UserTypeUser;
    char copyfeatures[32];
    char copyoptions[2000];
    char newusertype_s[32];
    char newcommunityid[32];
    char newprofileid[32];
    char newprofilename[255];
    char newuserid[32];
    char newusername[256];
    char newdirid[32];
    char newdirtype[32];
    char newgroupid[32];
    char newgrouptype[32];
    ResultSet *rs = NULL;
    xmlrpc_value *output = NULL;
    IString new_meeting_id;
    IString new_participant_id;
    char hash_password[41];

    // Parse request parameters
	xmlrpc_parse_value(env, param_array, "(sssssssssissis)",
        &sid, &communityname, &accountnumber, &contactname, &contactemail,
        &contactphone, &username, &userpassword, &userscreen, &userisadmin,
        &bridgephone, &systemurl, &options, &defaultprofileid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    // Must be superadmin to make this request...
    if (!check_superadmin(env, sid))
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "You must be a community administrator to add a community");
        goto cleanup;
    }

    // Validate screen name and password if necessary
    if (!validate_screen_name(userscreen))
    {
        xmlrpc_env_set_fault(env, InvalidScreenName, "Invalid Screen Name");
        goto cleanup;
    }

    if (!validate_password(userpassword))
    {
        xmlrpc_env_set_fault(env, InvalidScreenPassword, "Invalid Password");
        goto cleanup;
    }

    if (check_hash(userpassword))
        strcpy(hash_password, userpassword);
    else
        shahash_r(userpassword, hash_password);

    // Check USERISADMIN parameter and set user type accordingly
    if (userisadmin == 0)
    {
        sprintf(userisadmin_s, "false");
        newusertype = UserTypeUser;
    }
    else if (userisadmin == 1)
    {
        sprintf(userisadmin_s, "true");
        newusertype = UserTypeAdmin;
    }
    else
    {
        xmlrpc_env_set_fault(env, InvalidParameter, "USERISADMIN parameter not in valid range");
        goto cleanup;
    }

    // Get community id for new record.
    rs = db->execute("allocate-communityid");
    if (!rs || rs->is_error())
        goto cleanup;
    strcpy(newcommunityid, rs->get_value(0, 0));
    db->closeResults(rs);

    // Get new user
    rs = db->execute("allocate-userid");
    if (!rs || rs->is_error())
        goto cleanup;
    strcpy(newuserid, rs->get_value(0, 0));
    db->closeResults(rs);

    // Get directory id for this community's Global directory
    rs = db->execute("allocate-directoryid");
    if (!rs || rs->is_error())
        goto cleanup;
    strcpy(newdirid, rs->get_value(0,0));
    db->closeResults(rs);

    // Get group for for Global / All
    rs = db->execute("allocate-userid");
    if (!rs || rs->is_error())
        goto cleanup;
    strcpy(newgroupid, rs->get_value(0, 0));
    db->closeResults(rs);

    // Attempt to add the community...
    rs = db->execute("add-community",
            newcommunityid, communityname, accountnumber,
            contactname, contactemail, contactphone,
            username, userpassword,
            bridgephone, systemurl,
            i_itoa(options, options_s));
    if (!rs || rs->is_error())
        goto cleanup;

    // create name for profile
    sprintf(newprofilename, "%s profile", communityname);

    // Create default profile for this community if profile id not supplied
    if (strlen(defaultprofileid) == 0)
    {
        profileid = add_profile_internal(env, newcommunityid, newprofilename);
        XMLRPC_FAIL_IF_FAULT(env);
        sprintf(newprofileid, "%d", profileid);
    }
    else // attempt copy from supplied profile id
    {
        copy_profile_internal(env, defaultprofileid, copyfeatures, copyoptions, newprofileid, newcommunityid, newprofilename);
        XMLRPC_FAIL_IF_FAULT(env);
    }
    db->closeResults(rs);

    // Create USER record
    sprintf(newusername, "%s@%s", userscreen, g_server_name.get_string());
    rs = db->execute("add-user", newuserid, newusername, userscreen, hash_password, newcommunityid, userisadmin_s, "false");
    if (!rs || rs->is_error())
        goto cleanup;
    db->closeResults(rs);

    // Create DIRECTORY record
    sprintf(newdirtype, "%d", dtGlobal);
    rs = db->execute("create-directory", newdirid, newcommunityid, newuserid, DEFAULT_GLOBAL_DIRECTORY, newdirtype);
    if (!rs || rs->is_error())
        goto cleanup;
    db->closeResults(rs);

    // Create GROUP record
    sprintf(newgrouptype, "%d", dtAll);
    rs = db->execute("create-group-with-id", newgroupid, newdirid, DEFAULT_GROUP_NAME, newgrouptype);
    if (!rs || rs->is_error())
        goto cleanup;
    db->closeResults(rs);

    // Create CONTACT record
    // USERID, DIRECTORYID, USERNAME, SERVER,
    // TITLE, FIRSTNAME, MIDDLENAME, LASTNAME, SUFFIX,
    // COMPANY, JOBTITLE, ADDRESS1, ADDRESS2, STATE, COUNTRY, POSTALCODE,
    // SCREENNAME, AIMNAME, EMAIL, EMAIL2, EMAIL3, BUSPHONE, BUSDIRECT,
    // HOMEPHONE, HOMEDIRECT, MOBILEPHONE, MOBILEDIRECT, OTHERPHONE, OTHERDIRECT,
    // EXTENSION, USER1, USER2, USER3, USER4, USER5,
    // DEFPHONE, DEFEMAIL,
    // TYPE, PROFILEID, MAXSIZE, MAXPRIORITY, PRIORITYTYPE,
    // ENABLEDFEATURES, DISABLEDFEATURES
    rs = db->execute("add-contact",
        newuserid, newdirid, newusername, g_server_name.get_string(),
        "" /* title */, userscreen, "", "", "",
        "" /* company */, "", "", "", "", "", "",
        userscreen, "", "", "", "", "", "false",
        "" /* homephone */, "false", "", "false", "", "false",
        "" /* extension */, "", "", "", "", "",
        "0" /* defphone */, "0",
        i_itoa(newusertype, newusertype_s), newprofileid, "0", "0", "0",
        "0", "0");
    db->closeResults(rs);

    // Subscribe the user to the new global directory
    rs = db->execute("subscribe-to-gal", newuserid, newcommunityid);
    if (!rs || rs->is_error())
        goto cleanup;
    db->closeResults(rs);

    // Add new contact to the global "all" group
    add_user_to_group(env, newgroupid, newuserid);
	XMLRPC_FAIL_IF_FAULT(env);

    // Add an Instant Meeting for the new user
	setup_default_meeting(env, sid, newcommunityid, newuserid, newusername, userscreen, userscreen, new_meeting_id, new_participant_id, false);
	XMLRPC_FAIL_IF_FAULT(env);

    db->closeResults(rs);

    output = xmlrpc_build_value(env, "(ss)", newcommunityid, newuserid);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

// Remove a community - super admin only
//
// Parameters:
//      "SESSIONID", "COMMUNITYID"
//
// Returns:
//     number of rows affected
//
xmlrpc_value *remove_community(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid;
    int communityid;
    char communityid_s[32];
    ResultSet *rs = NULL;
    xmlrpc_value *output = NULL;

    // Parse request parameters
	xmlrpc_parse_value(env, param_array, "(si)", &sid, &communityid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    // Must be superadmin to make this request...
    if (!check_superadmin(env, sid))
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "You must be a super administrator to remove a community");
        goto cleanup;
    }

    // convert communityid to string
    sprintf(communityid_s, "%d", communityid);

    // Cannot delete the "system" community
    if (check_system_community(env, communityid_s))
    {
        xmlrpc_env_set_fault(env, CommunitySystemDeleteError, "You cannot remove the system community");
        goto cleanup;
    }

    // Attempt to remove the community...
    rs = db->execute("remove-community", communityid_s);
    if (!rs || rs->is_error())
        goto cleanup;
    output = xmlrpc_build_value(env, "(i)", rs->get_num_affected());

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

// Update all fields in community record - super admin only
//
// Parameters:
//     "SESSIONID", "COMMUNITYID", "COMMUNITYNAME", "ACCOUNTNUMBER",
//     "CONTACTNAME", "CONTACTEMAIL", "CONTACTPHONE",
//     "USERNAME", "USERPASSWORD",
//     "BRIDGEPHONE", "SYSTEMURL", "OPTIONS"
//
// Returns:
//     number of rows affected
//
xmlrpc_value *update_community(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *communityname, *accountnumber;
    char *contactname, *contactemail, *contactphone;
    char *adminname, *adminpassword, *bridgephone, *systemurl;
    int communityid;
    char communityid_s[32];
    int options;
    char options_s[32];
    ResultSet *rs = NULL;
    xmlrpc_value *output = NULL;

    // Parse request parameters
	xmlrpc_parse_value(env, param_array, "(sisssssssssi)",
        &sid, &communityid, &communityname, &accountnumber, &contactname, &contactemail,
        &contactphone, &adminname, &adminpassword, &bridgephone, &systemurl,
        &options);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    // Must be superadmin to make this request...
    if (!check_superadmin(env, sid))
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "You must be a super administrator to update all community data");
        goto cleanup;
    }

    // Attempt to update the community...
    rs = db->execute("update-community-all",
            i_itoa(communityid, communityid_s),
            communityname, accountnumber,
            contactname, contactemail, contactphone,
            adminname, adminpassword,
            bridgephone, systemurl,
            i_itoa(options, options_s));
    if (!rs || rs->is_error())
        goto cleanup;

    output = xmlrpc_build_value(env, "(i)", rs->get_num_affected());

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

// Update "community" fields in community record - both normal and super admins
// Intended to be user to update fields that are not restricted to super admins
// only, so that normal admins can update there own contact info etc...
//
// Will fail if a normal admin tries to update a community to
// which they do not belong
//
// Parameters:
//     "COMMUNITYID",
//     "CONTACTNAME", "CONTACTEMAIL", "CONTACTPHONE",
//     "BRIDGEPHONE", "SYSTEMURL", "OPTIONS"
//
// NOTE: OPTIONS is currently ignored for this update type, but is
//       reserved for future use (at this time there are no options
//       that can be modified by non-superadmins)
//
// Returns:
//     number of rows affected
//
xmlrpc_value *update_community_info(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid;
    char *contactname, *contactemail, *contactphone;
    char *bridgephone, *systemurl;
    int communityid;
    char communityid_s[32];
    int options;
    ResultSet *rs = NULL;
    xmlrpc_value *output = NULL;

    // Parse request parameters
	xmlrpc_parse_value(env, param_array, "(sisssssi)",
        &sid, &communityid, &contactname, &contactemail, &contactphone,
        &bridgephone, &systemurl, &options);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    // Must be superadmin or normal admin for specified
    // community to make this request...
    if (!(check_superadmin(env, sid) || check_communityadmin(env, sid)))
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "You must be a community administrator to update all community data");
        goto cleanup;
    }

    // Attempt to update the community...
    rs = db->execute("update-community-info",
            i_itoa(communityid, communityid_s),
            contactname, contactemail, contactphone,
            bridgephone, systemurl);
    if (!rs || rs->is_error())
        goto cleanup;

    output = xmlrpc_build_value(env, "(i)", rs->get_num_affected());

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

// Disable a community - super admin only
//
// Community record remains intact, but community users cannot sign in.
//
// Parameters:
//    "SESSIONID", "COMMUNITYID", "ENABLE" (0 for disable, non-zero to enable)
//
// Returns:
//    number of rows affected
//
xmlrpc_value *enable_community(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid;
    int communityid, enable;
    char communityid_s[32];
    char enable_s[32];
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;

	xmlrpc_parse_value(env, param_array, "(sii)", &sid, &communityid, &enable);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    // Must be superadmin to make this request...
    if (!check_superadmin(env, sid))
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "You must be a super administrator to enable/disable a community");
        goto cleanup;
    }

    // Make sure enable flag is in valid range...
    if ((enable != AccountStatusDisabled) && (enable != AccountStatusEnabled))
    {
        xmlrpc_env_set_fault(env, InvalidParameter, "ENABLE parameter not in valid range");
        goto cleanup;
    }

    i_itoa(communityid, communityid_s);
    i_itoa(enable, enable_s);

    rs = db->execute("set-community-status", communityid_s, enable_s);
    if (!rs || rs->is_error())
        goto cleanup;

    output = xmlrpc_build_value(env, "(i)", rs->get_num_affected());

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

// Fetch all fields from a community record - super admin only
//
// Parameters:
//     "COMMUNITYID"
//
// Returns:
//   0..1 rows containing the community data.  0 rows if given community id not found.
//
//   Each row contains:
//      COMMUNITYID, COMMUNITYNAME, ACCOUNTNUMBER,
//      CONTACTNAME, CONTACTEMAIL, CONTACTPHONE,
//      ADMINNAME, ADMINPASSWORD,
//      BRIDGEPHONE, SYSTEMURL,
//      ACCOUNTTYPE, ACCOUNTSTATUS,
//      OPTIONS
//
xmlrpc_value *fetch_community_details(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid;
    int communityid;
    char communityid_s[32];
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    IString fieldstr;

	xmlrpc_parse_value(env, param_array, "(si)", &sid, &communityid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    // Must be superadmin or normal admin for specified
    // community to make this request...
    if (!(check_superadmin(env, sid) || check_communityadmin(env, sid)))
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "You must be a community administrator to fetch community details");
        goto cleanup;
    }

    // Attempt to fetch the specified community
    rs = db->execute("fetch-community-details", i_itoa(communityid, communityid_s));
    if (!rs || rs->is_error())
        goto cleanup;

    // Should have a single community row in the result set.
    output = build_output(env, rs, 0, 0, 1, "fetch_community_details");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

// Fetch communities in the system - super admin only
//
// Parameters:
//    SESSIONID, FIRST, CHUNKSIZE
//
// Returns:
//    Array of community data:
//       COMMUNITYID, COMMUNITYNAME, ACCOUNTNUMBER
//       CONTACTNAME, CONTACTEMAIL, CONTACTPHONE,
//       ADMINNAME, ADMINPASSWORD,
//       BRIDGEPHONE, SYSTEMURL,
//       ACCOUNTTYPE, ACCOUNTSTATUS,
//       OPTIONS
//
//   followed by:
//       FIRST, CHUNKSIZE, TOTALROWS, MOREAVAILABLE
//
xmlrpc_value *fetch_community_list(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    int first, chunksize;
    char chunk_size_s[32], first_s[32];

	xmlrpc_parse_value(env, param_array, "(sii)", &sid, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    // Must be superadmin to make this request...
    if (!check_superadmin(env, sid))
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "You must be a super administrator to fetch the community list");
        goto cleanup;
    }

    i_itoa(chunksize + 1, chunk_size_s);
    i_itoa(first, first_s);

    rs = db->execute("fetch-community-list", chunk_size_s, first_s);
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, first, 0, chunksize, "fetch_community_list");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

// Returns list of communities in the system where the community
// name starts with the specified string.  List is alphabetized
// by community name
//
// Parameters:
//    SESSIONID, SEARCHSTRING, FIRST, CHUNKSIZE
//
// Returns:
//    Array of community data:
//       COMMUNITYID, COMMUNITYNAME, ACCOUNTNUMBER
//       CONTACTNAME, CONTACTEMAIL, CONTACTPHONE,
//       ADMINNAME, ADMINPASSWORD,
//       BRIDGEPHONE, SYSTEMURL,
//       ACCOUNTTYPE, ACCOUNTSTATUS,
//       OPTIONS
//
//   followed by:
//       FIRST, CHUNKSIZE, TOTALROWS, MOREAVAILABLE
//
xmlrpc_value *search_communities(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *searchstring;
    int first, chunksize;
    char chunk_size_s[32], first_s[32];
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;

	xmlrpc_parse_value(env, param_array, "(ssii)", &sid, &searchstring, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    // Must be superadmin to make this request...
    if (!check_superadmin(env, sid))
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "You must be a super administrator to search communities");
        goto cleanup;
    }

    i_itoa(chunksize + 1, chunk_size_s);
    i_itoa(first, first_s);

    rs = db->execute("search-communities", searchstring, chunk_size_s, first_s);
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, first, 0, chunksize, "search_communities");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

// Returns list of users in the system that match the passed criteria (contacts email
// string or lastname). List is alphabetized by screen name.
//
// Parameters:
//    SESSIONID, COMMUNITYID, SEARCHUSERNAME, SEARCHEMAIL, FIRST, CHUNKSIZE
//
// Returns:
//    Array of user data:
//       USERID, COMMUNITYID, SCREENNAME, ISADMIN, ISSUPERADMIN, PASSWORD, USERTYPE, FIRSTNAME, LASTNAME,
//       DEFPHONE, BUSPHONE, HOMEPHONE, MOBILEPHONE, OTHERPHONE, EMAIL (primary)
//
//   followed by:
//       FIRST, CHUNKSIZE, TOTALROWS, MOREAVAILABLE
//
xmlrpc_value *search_users(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *community_id, *searchusername, *searchemail;
    IString searchusername_str, searchemail_str;
    int first, chunksize;
    char chunk_size_s[32], first_s[32];
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    char wildcard[] = "%";

	xmlrpc_parse_value(env, param_array, "(ssssii)", &sid, &community_id, &searchusername, &searchemail, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (strlen(community_id) == 0)
    {
        xmlrpc_env_set_fault(env, InvalidParameter, "Community id must not be blank");
        goto cleanup;
    }

    // Must be community admin to make this request...
    if (!check_admin(env, sid, community_id))
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "You must be a community administrator to search users");
        goto cleanup;
    }

    // if community id is different from requesting user's community, must be super admin
    rs = db->execute("find-user-info", sid);
    if (!rs || rs->is_error())
        goto cleanup;

	if (rs->get_num_rows() > 0)
	{
		const char *user_communityid = rs->get_value(0, "COMMUNITYID");
        if ((strcmp(user_communityid, community_id) != 0) && !check_superadmin(env, sid))
        {
            xmlrpc_env_set_fault(env, NotAuthorized, "You must be a super administrator to request info outside your community");
            goto cleanup;
        }
    }
	db->closeResults(rs);

    // if username or email blank do not use in query. otherwise use wildcard
    // if both are blank retrieve all users in community
    searchusername_str = searchusername;
    searchemail_str = searchemail;
    if (searchusername_str.length() == 0 && searchemail_str.length() == 0)
    {
        searchusername_str.append(wildcard);
        searchemail_str.append(wildcard);
    }
    else if (searchusername_str.length() > 0)
    {
        searchusername_str.prepend(wildcard);
        searchusername_str.append(wildcard);
    }
    else if (searchemail_str.length() > 0)
    {
        searchemail_str.prepend(wildcard);
        searchemail_str.append(wildcard);
    }

    i_itoa(chunksize + 1, chunk_size_s);
    i_itoa(first, first_s);

    rs = db->execute("search-users", community_id, (const char*) searchemail_str, (const char*) searchusername_str, chunk_size_s, first_s);
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, first, 0, chunksize, "search_users");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Looks up user info by screen name.
//
// Parameters:
//    SESSIONID, COMMUNITYID, ARRAY of SCREENNAMES
//    If community id is blank, searches all communities
//
// Returns:
//    Array of user data:
//       SCREENNAME, USERID, COMMUNITYID, USERNAME, ISADMIN, ISSUPERADMIN, PASSWORD, USERTYPE, FIRSTNAME, LASTNAME,
//       DEFPHONE, BUSPHONE, HOMEPHONE, MOBILEPHONE, OTHERPHONE, EMAIL1, EMAIL2, EMAIL3
//
//   followed by:
//       FIRST, CHUNKSIZE, TOTALROWS, MOREAVAILABLE
//
xmlrpc_value *find_users_by_screenname(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *community_id;
	xmlrpc_value *screennames;
    IString search_str;
    int first, chunksize;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
	IString searchstr;

	xmlrpc_parse_value(env, param_array, "(ssA)", &sid,  &community_id, &screennames);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    // Must be community admin to make this request...
    if (!check_admin(env, sid, community_id))
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "You must be a community administrator to search users");
        goto cleanup;
    }

    // if community id is different from requesting user's community, must be super admin
    rs = db->execute("find-user-info", sid);
    if (!rs || rs->is_error())
        goto cleanup;

	if (rs->get_num_rows() > 0)
	{
		const char *user_communityid = rs->get_value(0, "COMMUNITYID");
        if ((strcmp(user_communityid, community_id) != 0) && !check_superadmin(env, sid))
        {
            xmlrpc_env_set_fault(env, NotAuthorized, "You must be a super administrator to request info outside your community");
            goto cleanup;
        }
    }
	db->closeResults(rs);

	// Parse list of screen names into list for SQL statement.
    if (!build_string_list(env, screennames, searchstr, false, NULL))
        goto cleanup;

	// Now request data for specified screen names.
	if (*community_id == '\0')
		rs = db->execute("find-users-by-screenname", (const char*) searchstr);
	else
		rs = db->execute("find-users-by-screenname-in-community", (const char*) searchstr, community_id);
    if (!rs || rs->is_error())
        goto cleanup;

	// and build output.
	first = 0;
	chunksize = rs->get_num_rows();
    output = build_output(env, rs, first, 0, chunksize, "find_users");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

// Add list of buddies by screenname.
// Parameters:
//   SESSIONID, SUBSCRIBEE_USERID, ARRAY of SCREENNAMES, RESTRICT_DIRECTORY, ACTION
// Returns:
//   number added
xmlrpc_value *add_buddies(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    ResultSet *rs = NULL;
    char *sid, *subscriber_userid, *subscriber_username, *restrict_directory, *action;
	xmlrpc_value *screennames;
    xmlrpc_value *output = NULL;
    bool bRestrictDirectory = true;
    bool is_service;

    char communityid[32];
    char dir[32];
    char defaultgroup[32];
    char buddygroup[32];
    char countstring[32];
    int current_presence_monitors;
    int max_presence_monitors;
    int count = 0;
    int i, num_names;

    char usertype[32];
    sprintf(usertype, "%d", UserTypeContact);

    char profileid[32];
    sprintf(profileid, "%s", CONTACT_PROFILEID);

	xmlrpc_parse_value(env, param_array, "(sssssA)",
                        	 &sid,
							 &subscriber_userid,
                        	 &subscriber_username,
							 &restrict_directory,
							 &action,
							 &screennames);
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!is_service)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "API not valid for user sessions");
        goto cleanup;
    }

    num_names = xmlrpc_array_size(env, screennames);
    XMLRPC_FAIL_IF_FAULT(env);

    // Verify user is within profile max
    if (fetch_max_monitors(subscriber_userid, max_presence_monitors) == Failed)
    {
		xmlrpc_env_set_fault(env, ProfileNotFound, "Error reading max presence monitors");
        goto cleanup;
    }

    // figure out how many monitors are in place for this user
    rs = db->execute("find-presence-subscription-count", subscriber_username);
    if (!rs || rs->is_error())
        goto cleanup;
    strcpy(countstring, rs->get_value(0, 0));
    current_presence_monitors = atoi(countstring);
    db->closeResults(rs);

    // Now find user information
    find_user_community(env, subscriber_userid, communityid);
    XMLRPC_FAIL_IF_FAULT(env);

    find_user_directory(env, communityid, subscriber_userid, dir);
    XMLRPC_FAIL_IF_FAULT(env);

    find_default_group(env, dir, defaultgroup);
    XMLRPC_FAIL_IF_FAULT(env);

    find_buddy_group(env, dir, buddygroup);
    XMLRPC_FAIL_IF_FAULT(env);

    if (strcmp(restrict_directory, "f") == 0)
        bRestrictDirectory = false;

    for (i=0; i<num_names; i++)
    {
        bool bExistsInMyDirectory = false;
        bool bExistsInAnyDirectory = false;
        char buddyuserid[32];
        char interbuddyuserid[32] = {0};  // used if the buddy resides in another community
        char new_subscription[32];

		char *subscribee_screen;
        IString subscribee_username, subscribee_userid;
		IString subscribee_first, subscribee_last;

        xmlrpc_value *val = xmlrpc_array_get_item(env, screennames, i);
        XMLRPC_FAIL_IF_FAULT(env);

        xmlrpc_parse_value(env, val, "s", &subscribee_screen);
        XMLRPC_FAIL_IF_FAULT(env);

        log_debug(ZONE, "Adding buddy %s", subscribee_screen);

        if (current_presence_monitors++ >= max_presence_monitors)
        {
            xmlrpc_env_set_fault(env, MaxPresenceExceeded, "Max presence monitors exceeded");
            goto cleanup;
        }

		// search in user's directory to see if contact exists
		rs = db->execute("find-buddy-from-screen", dir, subscribee_screen);
		if (!rs || rs->is_error())
			goto cleanup;
		if (rs->get_num_rows() > 0)
		{
			const char *val = rs->get_value(0, 0);
			if (val != NULL)
			{
				strcpy(buddyuserid, val);
				bExistsInMyDirectory = true;

                // username is blank in personal contact
                subscribee_username = subscribee_screen;
                subscribee_username.append("@" + g_server_name);

                subscribee_first = rs->get_value(0, 2);
                subscribee_last = rs->get_value(0, 3);
			}
		}
		db->closeResults(rs);

		// if not restricted to own directory search across all directories to see if contact exists
		if (!bExistsInMyDirectory && !bRestrictDirectory)
		{
			rs = db->execute("find-buddy-from-screen-all", subscribee_screen);
			if (!rs || rs->is_error())
				goto cleanup;
			if (rs->get_num_rows() > 0)
			{
				const char *val = rs->get_value(0, 0);
				if (val != NULL)
				{
					strcpy(buddyuserid, val);
					strcpy(interbuddyuserid, val);
					bExistsInAnyDirectory = true;

                    subscribee_username = rs->get_value(0, 1);
                    subscribee_first = rs->get_value(0, 2);
                    subscribee_last = rs->get_value(0, 3);
				}
			}
			db->closeResults(rs);
		}

		// if not restricted and no contact, return error
		if (!bRestrictDirectory && !bExistsInMyDirectory && !bExistsInAnyDirectory)
		{
			xmlrpc_env_set_fault(env, UserNotFound, "Unable to find user");
			goto cleanup;
		}

		// if contact doesn't exist in user's directory, create copy
		if (!bExistsInMyDirectory)
		{
			// Create new contact and add it to the default and buddy directories
			// Create CONTACT record

			// Get new user
			rs = db->execute("allocate-userid");
			if (!rs || rs->is_error())
				goto cleanup;
			strcpy(buddyuserid, rs->get_value(0, 0));
			db->closeResults(rs);

			// USERID, DIRECTORYID, USERNAME, SERVER,
			// TITLE, FIRSTNAME, MIDDLENAME, LASTNAME, SUFFIX,
			// COMPANY, JOBTITLE, ADDRESS1, ADDRESS2, STATE, COUNTRY, POSTALCODE,
			// SCREENNAME, AIMNAME, EMAIL, EMAIL2, EMAIL3, BUSPHONE, BUSDIRECT,
			// HOMEPHONE, HOMEDIRECT, MOBILEPHONE, MOBILEDIRECT, OTHERPHONE, OTHERDIRECT,
			// EXTENSION, USER1, USER2, USER3, USER4, USER5,
			// DEFPHONE, DEFEMAIL,
			// TYPE, PROFILEID, MAXSIZE, MAXPRIORITY, PRIORITYTYPE,
			// ENABLEDFEATURES, DISABLEDFEATURES
			rs = db->execute("add-contact",
				buddyuserid, dir, "", "",
				"" /* title */, subscribee_first.get_string(), "", subscribee_last.get_string(), "",
				"" /* company */, "", "", "", "", "", "",
				subscribee_screen, "", "", "", "", "", "false",
				"" /* homephone */, "false", "", "false", "", "false",
				"" /* extension */, "", "", "", "", "",
				"0" /* defphone */, "0",
				usertype, profileid, "0", "0", "0",
				"0", "0");
			db->closeResults(rs);

			// Add new contact to the personal default group
			add_user_to_group(env, defaultgroup, buddyuserid);
            XMLRPC_FAIL_IF_FAULT(env);

			// Add new contact to the personal buddy group
			add_user_to_group(env, buddygroup, buddyuserid);
            XMLRPC_FAIL_IF_FAULT(env);
		}
		else
		{
			// Add buddy user to buddy group if not already there
			rs = db->execute("find-group-member", buddygroup, buddyuserid);
			if (!rs || rs->is_error())
				goto cleanup;
			if (rs->get_num_rows() == 0)
			{
				db->closeResults(rs);

				add_user_to_group(env, buddygroup, buddyuserid);
                XMLRPC_FAIL_IF_FAULT(env);
			}
			db->closeResults(rs);
		}

		// Update the jabber roster
		update_presence_subcription_internal(env, subscriber_username, subscribee_username, action, new_subscription);
        XMLRPC_FAIL_IF_FAULT(env);

        count++;
	}

    output = xmlrpc_build_value(env, "(i)", count);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

int get_host_userid_passwd(const char * meetingid, char** userid, char** passwd)
{
    ResultSet *rs = NULL;
    IString hostid, screenname, password;
    int status = Failed;

   // retrieve meeting host - use that hostid to get screen name & password
	rs = db->execute("find-meeting-host", meetingid);
	if (!rs || rs->is_error())
		goto cleanup;

	if (rs->get_num_rows() > 0)
	{
		// Return meeting id
		hostid = rs->get_value(0, "HOSTID");
    }
    else
    {
        goto cleanup;
    }
    db->closeResults(rs);


	rs = db->execute("find-screenname-passwd-by-userid", (const char*)hostid);
	if (!rs || rs->is_error())
		goto cleanup;

	if (rs->get_num_rows() > 0)
	{
		// Return meeting id
		screenname = rs->get_value(0, "SCREENNAME");
		password = rs->get_value(0, "PASSWORD");

        *userid = (char*)malloc(screenname.length()+1);
        strcpy(*userid, screenname);

        *passwd = (char*)malloc(password.length()+1);
        strcpy(*passwd, password);

        status = Ok;
    }
    else
    {
        goto cleanup;
    }

cleanup:
    if (rs != NULL)
    {
        db->closeResults(rs);
    }

    return status;
}


// ++++++++++++++++++

// Get the list of recordings for a specified meeting.
// Parameters:
//   MEETINGID
// Returns:
//   array of structured data
xmlrpc_value *get_recording_list(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	xmlrpc_value *output = NULL;
	char * meetingid;
	char * msg = NULL;
	int res = -1;
    UserSession *session;

    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(s)", &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to get_recording_list");
		return NULL;
    }

    session = lookup_session(env, user_data);
    if (env->fault_occurred)
        return NULL;

    IList list;

    res = sRepository.get_recording_list(meetingid, g_recording_base_url, session->screen, session->password, &list, &msg);

    if (res==0)
    {
        output = build_output_from_list(env, &list, 3);
        if (env->fault_occurred)
        {
            xmlrpc_env_set_fault(env, Failed, "Error building output for get_recording_list");
            output = NULL;
        }
    }
    else
    {
        xmlrpc_env_set_fault(env, Failed, (char*)(msg?msg:"Error retrieve recording_list"));
    }

    if (msg)
    {
        free (msg);
    }

    return output;
}

xmlrpc_value *remove_recordings(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	xmlrpc_value *output = NULL;
	char * meetingid;
	int res;
    char * msg = NULL;
    char * recordingurl;
    UserSession *session;

    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(ss)", &meetingid, &recordingurl);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to get_recording_list");
		return NULL;
    }

    session = lookup_session(env, user_data);
    if (env->fault_occurred)
        return NULL;

    IString absolute_url = recordingurl;
    char proto[8];
    memset(proto, 0, 8);
    strncpy(proto, absolute_url.get_string(), 7);

    if (stricmp(proto, "http://") && stricmp(proto, "https:/"))   // when deleteing a folder, recordingurl maybe the folder name
    {
        absolute_url = g_recording_base_url;
        absolute_url.append(meetingid);
        absolute_url.append("/");
        absolute_url.append(recordingurl);
        absolute_url.append("/");
    }
    res = sRepository.remove_recordings(meetingid, (const char*)absolute_url, session->screen, session->password, &msg);

    if (res==0)
    {
        output = xmlrpc_build_value(env, "(i)", res);
        if (env->fault_occurred)
        {
            xmlrpc_env_set_fault(env, Failed, "Error building output for get_recording_list");
            output = NULL;  // memory leak?
        }
    }
    else
    {
        xmlrpc_env_set_fault(env, Failed, (char*)(msg?msg:"Error to delete recording"));
    }

    if (msg)
    {
        free (msg);
    }

    return output;
}

// Get the list of documents for a specified meeting.
// Parameters:
//   MEETINGID
// Returns:
//   array of structured data
xmlrpc_value *get_doc_list(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	xmlrpc_value *output = NULL;
	char * meetingid;
	char * msg = NULL;
	int res;
    UserSession *session;

    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(s)", &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to get_doc_list");
		return NULL;
    }

    session = lookup_session(env, user_data);
    if (env->fault_occurred)
        return NULL;

    IList list;

    res = sRepository.get_doc_list(meetingid, g_docshare_base_url, session->screen, session->password, &list, &msg);

    if (res==0)
    {
        output = build_output_from_list(env, &list, 2);
        if (env->fault_occurred)
        {
            xmlrpc_env_set_fault(env, Failed, "Error building output for get_doc_list");
            output = NULL;
        }
    }
    else
    {
        xmlrpc_env_set_fault(env, Failed, (char*)(msg?msg:"Error retrieve document list"));
    }

    if (msg)
    {
        free (msg);
    }

    return output;
}

xmlrpc_value *remove_docs(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	xmlrpc_value *output = NULL;
	char * meetingid;
	char * docurl;
	int res;
    char * msg = NULL;
    UserSession *session;

    /* Parse our argument array. */
    xmlrpc_parse_value(env, param_array, "(ss)", &meetingid, &docurl);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to remove_docs");
		return NULL;
    }

    session = lookup_session(env, user_data);
    if (env->fault_occurred)
        return NULL;

    res = sRepository.remove_docs(meetingid, docurl, session->screen, session->password, &msg);

    if (res==0)
    {
        output = xmlrpc_build_value(env, "(i)", res);
        if (env->fault_occurred)
        {
            xmlrpc_env_set_fault(env, Failed, "Error building output for remove_docs");
            output = NULL;
        }
    }
    else
    {
        xmlrpc_env_set_fault(env, Failed, (char*)(msg?msg:"Error to delete document"));
    }

    if (msg)
    {
        free (msg);
    }

    return output;
}

xmlrpc_value *authorize_recording_access(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	xmlrpc_value *output = NULL;
    char *meetingid;
    char *userid;
    char *hostid, *passwd;
    char valstr[32];

    xmlrpc_parse_value(env, param_array, "(ss)", &userid, &meetingid);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to remove_docs");
		return NULL;
    }

    log_debug(ZONE, "Attempting to authorize access for user:%s meeting:%s", userid, meetingid);

    if (get_host_userid_passwd(meetingid, &hostid, &passwd) != Ok)
    {
        xmlrpc_env_set_fault(env, Failed, "Error get host screen name and passwd for remove_recordings");
        return NULL;
    }

    if (strncmp(userid, hostid, strlen(hostid)))
    {
        log_debug(ZONE, "User %s does not match host %s for mtg %s: NOT AUTHORIZED", userid, hostid, meetingid);
        sprintf(valstr, "NOT AUTHORIZED");
    }
    else
    {
        log_debug(ZONE, "User %s is host for mtg %s: AUTHORIZED", userid, meetingid);
        sprintf(valstr, "OK");
    }

    output = xmlrpc_build_value(env, "(s)", valstr);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, Failed, "Error building output for remove_docs");
        output = NULL;
    }

    return output;
}

int RPC_run()
{
	static unsigned counter = 0;

	if ((++counter % 30000) == 0)
	{
		event_log.check_rotation();
	}

	return 0;
}

/* Initialization as jabber component--call addressbook_init() if using library
   directly. */
int main()
{
	fprintf(stderr, "IIC Addressbook Server 1.0   Copyright (C) 2003 imidio.com\n\n");

    config_t conf = config_new();
    config_load(conf, JCOMPNAME, 0, "/opt/iic/conf/config.xml");
    init(conf);
	if (jc_init(conf, JCOMPNAME))
	{
		log_error(ZONE, "Unable to initialize address\n");

		return -1;
	}

    rpc_set_callbacks(NULL, NULL, RPC_run);
    rpc_set_user_callable(1);

	// Add methods to RPC server
    rpc_add_method("addressbk.check_version", &check_version, NULL);
    rpc_add_method("addressbk.authenticate_user", &authenticate_user, NULL);
    rpc_add_method("addressbk.find_user", &find_user, NULL);
    rpc_add_method("addressbk.find_user_by_pin", &find_user_by_pin, NULL);
    rpc_add_method("addressbk.add_user", &add_user, NULL);
    rpc_add_method("addressbk.add_user_x", &add_user_x, NULL);
    rpc_add_method("addressbk.add_contact", &add_contact, NULL);
    //rpc_add_method("addressbk.import_contact", &import_contact, NULL);
    rpc_add_method("addressbk.update_contact", &update_contact, NULL);
    rpc_add_method("addressbk.fetch_directories", &fetch_directories, NULL);
    rpc_add_method("addressbk.fetch_groups", &fetch_groups, NULL);
    rpc_add_method("addressbk.fetch_groups_2", &fetch_groups_2, NULL);
    rpc_add_method("addressbk.fetch_deleted_groups", &fetch_deleted_groups, NULL);
    rpc_add_method("addressbk.fetch_group_by_values", &fetch_group_by_values, NULL);
    rpc_add_method("addressbk.fetch_group_members", &fetch_group_members, NULL);
    rpc_add_method("addressbk.fetch_group_members_2", &fetch_group_members_2, NULL);
    rpc_add_method("addressbk.fetch_profiles", &fetch_profiles, NULL);
    rpc_add_method("addressbk.fetch_contacts", &fetch_contacts, NULL);
    rpc_add_method("addressbk.fetch_contacts_2", &fetch_contacts_2, NULL);
    rpc_add_method("addressbk.fetch_deleted_contacts", &fetch_deleted_contacts, NULL);
    rpc_add_method("addressbk.fetch_contact", &fetch_contact, NULL);
    rpc_add_method("addressbk.fetch_buddy_contacts", &fetch_buddy_contacts, NULL);
    rpc_add_method("addressbk.fetch_intercommunity_buddies", &fetch_intercommunity_buddies, NULL);
    rpc_add_method("addressbk.search_contacts", &search_contacts, NULL);
    rpc_add_method("addressbk.add_group", &add_group, NULL);
    rpc_add_method("addressbk.update_group", &update_group, NULL);
    rpc_add_method("addressbk.remove_group", &remove_group, NULL);
    rpc_add_method("addressbk.remove_contact", &remove_contact, NULL);
    rpc_add_method("addressbk.add_to_group", &add_to_group, NULL);
    rpc_add_method("addressbk.remove_from_group", &remove_from_group, NULL);
    rpc_add_method("addressbk.add_profile", &add_profile, NULL);
    rpc_add_method("addressbk.copy_profile", &copy_profile, NULL);
    rpc_add_method("addressbk.update_profile", &update_profile, NULL);
    rpc_add_method("addressbk.remove_profile", &remove_profile, NULL);
    rpc_add_method("addressbk.clean_directory", &clean_directory, NULL);
    rpc_add_method("addressbk.subscribe_directory", &subscribe_to_directory, NULL);
    rpc_add_method("addressbk.unsubscribe_directory", &unsubscribe_from_directory, NULL);
    rpc_add_method("addressbk.set_password", &set_password, NULL);
    rpc_add_method("addressbk.set_default_phone", &set_default_phone, NULL);
    rpc_add_method("addressbk.set_default_meeting", &set_default_meeting, NULL);
    rpc_add_method("addressbk.set_default_email", &set_default_email, NULL);
    rpc_add_method("addressbk.update_presence_subcription", &update_presence_subcription, NULL);
    rpc_add_method("addressbk.add_buddy", &add_buddy, NULL);

    rpc_add_method("addressbk.add_community", &add_community, NULL);
    rpc_add_method("addressbk.remove_community", &remove_community, NULL);
    rpc_add_method("addressbk.update_community", &update_community, NULL);
    rpc_add_method("addressbk.update_community_info", &update_community_info, NULL);
    rpc_add_method("addressbk.enable_community", &enable_community, NULL);
    rpc_add_method("addressbk.fetch_community_details", &fetch_community_details, NULL);
    rpc_add_method("addressbk.fetch_community_list", &fetch_community_list, NULL);
    rpc_add_method("addressbk.search_communities", &search_communities, NULL);
    rpc_add_method("addressbk.search_users", &search_users, NULL);
    rpc_add_method("addressbk.find_users_by_screenname", &find_users_by_screenname, NULL);
    rpc_add_method("addressbk.add_buddies", &add_buddies, NULL);
    rpc_add_method("addressbk.send_new_user_confirmation", &send_new_user_confirmation, NULL);

	// new API for repository
    rpc_add_method("addressbk.get_recording_list", &get_recording_list, NULL);
    rpc_add_method("addressbk.remove_recordings", &remove_recordings, NULL);
    rpc_add_method("addressbk.get_doc_list", &get_doc_list, NULL);
    rpc_add_method("addressbk.remove_docs", &remove_docs, NULL);
    rpc_add_method("addressbk.authorize_recording_access", &authorize_recording_access, NULL);

    rpc_add_method("addressbk.ldap_sync", &ldap_sync, NULL);

    rpc_add_method("schedule.add_participant", &add_participant, NULL);
    rpc_add_method("schedule.join_participant", &join_participant, NULL);
    rpc_add_method("schedule.remove_participant", &remove_participant, NULL);
    rpc_add_method("schedule.find_meeting", &find_meeting, NULL);
    rpc_add_method("schedule.find_meeting_by_userid", &find_meeting_by_userid, NULL);
    rpc_add_method("schedule.find_meetings_by_host", &find_meetings_by_host, NULL);
    rpc_add_method("schedule.find_meetings_invited", &find_meetings_invited, NULL);
    rpc_add_method("schedule.find_meetings_public", &find_meetings_public, NULL);
    rpc_add_method("schedule.list_meetings_by_host", &list_meetings_by_host, NULL);
    rpc_add_method("schedule.list_meetings_invited", &list_meetings_invited, NULL);
    rpc_add_method("schedule.list_meetings_public", &list_meetings_public, NULL);
    rpc_add_method("schedule.search_meetings", &search_meetings, NULL);
    rpc_add_method("schedule.find_participants", &find_participants, NULL);
    rpc_add_method("schedule.find_meeting_details", &find_meeting_details, NULL);
    rpc_add_method("schedule.find_pin", &find_pin, NULL);
    rpc_add_method("schedule.find_invite", &find_invite, NULL);
    rpc_add_method("schedule.insert_meeting_status", &insert_meeting_status, NULL);
    rpc_add_method("schedule.reset_meeting_status", &reset_meeting_status, NULL);
    rpc_add_method("schedule.create_meeting", &create_meeting, NULL);
    rpc_add_method("schedule.update_meeting", &update_meeting, NULL);
    rpc_add_method("schedule.remove_meeting", &remove_meeting, NULL);
    rpc_add_method("schedule.find_meetings_public_url", &find_meetings_public_url, NULL);
    rpc_add_method("schedule.find_meetings_invited_url", &find_meetings_invited_url, NULL);

	jc_run(false);

	return 0;
}


