/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef _NAMECACHELIST_
#define _NAMECACHELIST_

#include "../../util/iobject.h"
#include "../../util/istring.h"

class NameCacheList : public IObject
{
    IString m_userid;
    IList   m_list;

public:
    NameCacheList(const char* userid);

    // adds the name to the list
	void add(const char* name);

    // returns true if list contains the name
    bool contains(const char* name);

    enum { TYPE_NAMECACHELIST = 3551 };
    virtual int type()
        { return TYPE_NAMECACHELIST; }
    virtual unsigned int hash()
        { return m_userid.hash(); }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && m_userid == ((NameCacheList*)obj)->m_userid; }
};

// a hash of lists, indexed by userid
class NameCache
{
    IStringHash* m_hash;

public:
    NameCache();
    ~NameCache();

    // adds the name to the cache indexed by userid
    void add(const char* userid, const char* name);

    // clears the cache of all names stored by the userid
    void clear(const char* userid);

    // returns true if cache contains the passed name indexed by the passed userid
    bool contains(const char* userid, const char* name);
};

#endif
