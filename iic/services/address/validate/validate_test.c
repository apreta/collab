#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gmodule.h>
#include "validate_screenname.h"

#define SO_FILE "../../../bin/validate_eci.so"
#define SERVER_URL "http://localhost:3333/imidio/RPC2"
#define AUTH_NAME  "137006000"
#define AUTH_PASS  "imidio@eci"

typedef int (*InitializeCall) (const char*, const char*, const char*);
typedef int (*NamesCall) (validate_screenname_st* screenname, int num);

int main (int argc, char** argv)
{
    int result;
    validate_screenname_st names;
    
    char username[32];
    char userid[32];
    
    printf("Enter a name to lookup: ");
    scanf("%s", username);
    
    printf("Enter a userid: ");
    scanf("%s", userid);
    
    strcpy(names.lookup_name, username);
    strcpy(names.userid, userid);
    
    // test gmodule
    GModule* module = g_module_open(SO_FILE, G_MODULE_BIND_LAZY);
    
    if (module != NULL)
    {
        InitializeCall init_symbol;
        NamesCall get_symbol;
        
        if (g_module_symbol(module, "initialize", (gpointer*) &init_symbol))
        {
            if (g_module_symbol(module, "get_alternatives", (gpointer*) &get_symbol))
            {
                printf("Symbols loaded\n");
                printf("Initializing\n");
                result = init_symbol(SERVER_URL, AUTH_NAME, AUTH_PASS);
                if (result == 0)
                {
                    printf("initialization complete\n");
                    
                    result = get_symbol(&names, 3);
                    printf("Result is %d\n", result);
                    
                    if (result == 0)
                        printf ("Username OK\n");
                    else if (result == -4013)
                    {
                        printf("Names: %s %s %s\n", names.names[0], names.names[1], names.names[2]);
                    }
                    else 
                        printf("Failed\n");
                }
            }
            else
                printf("Can't load get symbol\n");
        }
        else
            printf("Can't load init symbol\n");
    }
    else
        printf("Error loading library: %s\n", g_module_error());
    
    return 0;
}

