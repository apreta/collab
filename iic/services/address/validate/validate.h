#include "validate_screenname.h"

#ifdef __cplusplus
extern "C" 
{
#endif

#ifdef VALIDATEDLL
#define DLL __declspec(dllexport)
#else
#define DLL __declspec(dllimport)
#endif

DLL int initialize(const char*, const char*, const char*);
DLL int get_alternatives(validate_screenname_st* screennames, int num);

#ifdef __cplusplus
}
#endif
