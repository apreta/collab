/* (c)2003 imidio, all rights reserved */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>
#include <xmlrpc-c/client.h>
#include "../../common/common.h"
#include "../../../jcomponent/util/log.h"
#include "validate_screenname.h"

#define NAME       "XML-RPC Validate Screenname Client"
#define VERSION    "0.1"
#define SERVER_URL "http://localhost:3333/imidio/RPC2"
#define AUTH_NAME  "137006000"
#define AUTH_PASS  "imidio@eci"

// opens connection to the passed url
xmlrpc_server_info* open_client_connection(xmlrpc_env* env, 
                                           char* server_url,
                                           char* username,
                                           char* password)
{
    xmlrpc_server_info* server_info = NULL;

    xmlrpc_client_init(XMLRPC_CLIENT_NO_FLAGS, NAME, VERSION);
    xmlrpc_env_init(env);
    XMLRPC_FAIL_IF_FAULT(env);
    
    server_info = xmlrpc_server_info_new(env, server_url);
    XMLRPC_FAIL_IF_FAULT(env);
    
    xmlrpc_server_info_set_basic_auth(env, server_info, username, password);
    XMLRPC_FAIL_IF_FAULT(env);
    
cleanup:
    if (env->fault_occurred)
    {
        xmlrpc_server_info_free(server_info);
        server_info = NULL;
    }
    return server_info;
}

// close the connection
int close_client_connection(xmlrpc_env* env,
                            xmlrpc_server_info* server_info)
{
    xmlrpc_server_info_free(server_info);
    
    /* Shutdown our XML-RPC client library. */
    xmlrpc_env_clean(env);
    xmlrpc_client_cleanup();
    
    return Ok;
}

#define NAME_SIZE 256

static char _server_url[NAME_SIZE];
static char _auth_name[NAME_SIZE];
static char _auth_pass[NAME_SIZE];

// initialization entry point
int initialize(const char* server_url, const char* auth_name, const char* auth_pass)
{
    if (server_url == NULL || strlen(server_url) > NAME_SIZE)
        return Failed;
    if (auth_name == NULL  || strlen(auth_name) > NAME_SIZE)
        return Failed;
    if (auth_pass == NULL || strlen(auth_pass) > NAME_SIZE)
        return Failed;
    
    strcpy(_server_url, server_url);
    strcpy(_auth_name, auth_name);
    strcpy(_auth_pass, auth_pass);
    
    return Ok;
}

// main entry point
int get_alternatives(validate_screenname_st* validate_st, int num)
{
    xmlrpc_server_info* server_info = NULL; 
    xmlrpc_env env;
    xmlrpc_value *result;
    xmlrpc_value *array;
    char* username;
    char num_str[32];
    char userid[32];
    int i, size;
    int found = Failed;
    
    if (validate_st == NULL || validate_st->userid == NULL || validate_st->lookup_name == NULL)
        return Failed;

    // prepend userid going to ECI's DB with "iic"
    sprintf(userid, "iic%s", validate_st->userid);

    sprintf(num_str, "%d", num);
    
    /* Start up our XML-RPC client library. */
    server_info = open_client_connection(&env, _server_url, _auth_name, _auth_pass);
    if (server_info == NULL)
    {
        xmlrpc_env_set_fault(&env, Failed, "Failure opening connection");
        goto cleanup;
    }
    
    /* Call our XML-RPC server. */
    result = xmlrpc_client_call_server(&env, server_info,
                                       "userinfo.validateScreenname", 
                                       "(sss)",
                                       userid,
                                       validate_st->lookup_name,
                                       num_str);
    
    XMLRPC_FAIL_IF_FAULT(&env);
    
    /* Parse our result value. */
    xmlrpc_parse_value(&env, result, "A",
                       &array);
    XMLRPC_FAIL_IF_FAULT(&env);
    
    size = xmlrpc_array_size(&env, array);
    XMLRPC_FAIL_IF_FAULT(&env);
    
    // parse case where size is 1 and value returned is 'true'
    if (size == 1)
    {
        xmlrpc_value *val = xmlrpc_array_get_item(&env, array, 0);
        XMLRPC_FAIL_IF_FAULT(&env);
        
        xmlrpc_parse_value(&env, val, "s", &username);
        XMLRPC_FAIL_IF_FAULT(&env);
        
        if (strcmp(username, "true") == 0)
            found = Ok;
    }
    else
    {
        for (i = 0; i < size; i++)
        {
            xmlrpc_value *val = xmlrpc_array_get_item(&env, array, i);
            XMLRPC_FAIL_IF_FAULT(&env);
            
            xmlrpc_parse_value(&env, val, "s", &username);
            XMLRPC_FAIL_IF_FAULT(&env);
            
            strcpy(validate_st->names[i], username);
        }
        
        found = DuplicateUsername; // duplicate code
    }
    
    /* Dispose of our result value. */
    xmlrpc_DECREF(result);
    
    close_client_connection(&env, server_info);
    
cleanup:
    if (env.fault_occurred)
    {
        xmlrpc_server_info_free(server_info);
        server_info = NULL;
    }
    return found;
}

// test program for calling directly (not as .so)
int main (int argc, char** argv)
{
    int i, result;
    int count = 3;
    validate_screenname_st validate_st;
    char username[32];
    char userid[32];
    
    printf("Enter a name to lookup: ");
    scanf("%s", username);
    
    printf("Enter a userid: ");
    scanf("%s", userid);
    
    initialize(SERVER_URL, AUTH_NAME, AUTH_PASS);
    
    strcpy(validate_st.lookup_name, username);
    strcpy(validate_st.userid, userid);
    result = get_alternatives(&validate_st, count);
    
    if (result == -1) // error
        printf("Error calling service\n");
    else if (result == 0)
        printf("Valid name\n");
    else // duplicate
    {
        for (i = 0; i < count; i++)
        {
            printf("Name is %s\n", validate_st.names[i]);
        }
    }
    
    return 0;
}
