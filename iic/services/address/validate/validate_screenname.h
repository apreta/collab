#ifndef _VALIDATE_SCREENNAME_H_
#define _VALIDATE_SCREENNAME_H_

#include "../../common/common.h"

// structure holds up to 3 screenname alternatives
typedef struct validate_screenname
{
    char userid[32];
    char lookup_name[256];
    char names[MAX_ALTERNATIVE_NAMES][256];
} validate_screenname_st;

#endif
