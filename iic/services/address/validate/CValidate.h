#include <stdio.h>
#include <stdlib.h>
#include "../../common/common.h"
#include "validate_screenname.h"

extern "C" _declspec(dllexport) int initialize(const char* server_url, const char* user, const char* pass)
{
    return Ok;
}

// stub method
// returns 3 alternatives (which may be in our db or not)
// #define PASSTEST to pass every time
extern "C" _declspec(dllexport) int get_alternatives(validate_screenname_st* screennames, int num)
{
    if (screennames == NULL || screennames->lookup_name == NULL || num > MAX_ALTERNATIVE_NAMES)
        return Failed;

//#define PASSTEST
#ifdef PASSTEST
    return Ok;
#else
    for (int i = 0; i < num; i++)
    {
        sprintf(screennames->names[i], "%s%d", screennames->lookup_name, i);
    }

    return DuplicateUsername;
#endif
}
