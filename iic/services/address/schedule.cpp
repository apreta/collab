/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef WIN32
#pragma warning(disable:4786)
#endif

#include <stdio.h>
#include <stdlib.h>
#include "../common/mailer.h"
#include "../../util/base64/Base64.h"
#include "../../util/base64/MIMECode.h"
#include <time.h>
#include <ctype.h>
#include "address.h"
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>
#include <math.h>

#include <libintl.h>
#define _(String) gettext(String)

#include "../../jcomponent/rpc.h"
#include "../../jcomponent/util/log.h"
#include "../../jcomponent/util/eventlog.h"
#include "../../util/ithread.h"
#include "../common/common.h"
#include "../common/profile.h"
#include "../common/datetime.h"
#include "../db/sqldb.h"

#include "address.h"

extern char *i_itoa(int i, char *buffer);
extern DBManager *db;
extern IString g_server_name;
static bool s_bseeded = false;

EventLog g_rsrv_event_log;
IMutex   g_rsrv_event_mutex;

int log_reservation(int event,
                    const char *meetingid,
                    const char *start_time,
                    const char *end_time,
                    const char *communityid,
                    const char *hostid,
                    const char *options,
                    const char *partid,
                    const char *pin)
{
    EventLogReservationField fields[] =
        {
            EvnLog_ReservationField_MeetingID,
            EvnLog_ReservationField_MeetingStart,
            EvnLog_ReservationField_MeetingEnd,
            EvnLog_ReservationField_CommunityID,
            EvnLog_ReservationField_HostID,
            EvnLog_ReservationField_MeetingOptions,
            EvnLog_ReservationField_ParticipantID,
            EvnLog_ReservationField_ParticipantPin
        };
    const char *values[] =
        {
            meetingid,
            start_time,
            end_time,
            communityid,
            hostid,
            options,
            partid,
            pin
        };

    IUseMutex lock(g_rsrv_event_mutex);

    return g_rsrv_event_log.log_reservation_record((EventLogReservationEvent)event,
                                                   fields,
                                                   values,
                                                   sizeof(fields)/sizeof(fields[0]));
}

void gen_pin(char* abuf, int pin_length)
{
    char* buf_p = abuf;
    char buff[2];
    int randint, i;
    for (i = 0; i < pin_length; i++)
    {
        // get rand num from 0 <= X < 10 (but do not allow leading 0)
        randint = i == 0 ? (int)(1.0 + 9.0 * ((double) rand() / ((double) RAND_MAX + 1.0))) :
                           (int)(10.0 * ((double)rand() / ((double) RAND_MAX + 1.0)));
        i_itoa(randint, buff);
        strncpy(buf_p, buff, 1);
        buf_p++;
    }
    memset(buf_p, '\0', 1);
}

#define kMaxTries 20

// allocates a pin from the database within the range determined by the passed pin length
bool allocate_pin(char* pin, int pin_length, const char* test_mid = NULL)
{
    int status = Ok;
    ResultSet *rs = NULL;
    char pin_buff[32];
    char* pin_p = pin_buff;
    char inc_buff[2];
    bool found = false;
    int num_tries = 0; // num of tries per random number
    int counter = 0;

    inc_buff[1] = '\0';

    // seed random very first time this is called
    if (!s_bseeded)
    {
        srand( (unsigned)time( NULL ) );
        s_bseeded = true;
    }

    do
    {
        // generate random number in the range specified by pin_length
        if (num_tries == 0)
        {
			if (test_mid != NULL)
			{
				// Use supplied meeting pin, for setting up test environments
				strncpy(pin_buff, test_mid, sizeof(pin_buff));
				pin_buff[sizeof(pin_buff)-1] = '\0';
			}
			else
			{
				gen_pin((char*)pin_buff, pin_length);
			}
        }

        // attempt to insert
        rs = db->execute("add-pin", pin_buff);

        // if success, we're done
        if (rs->get_num_affected() > 0)
        {
            found = true;
            strcpy(pin,pin_buff);
        }
        else if (!rs || rs->is_error())
        {
			db->closeResults(rs);

            // if error need to see if was duplicate key error so do select.
            // if that comes back with an error assume a different DB error
            rs = db->execute("find-pin", pin_buff);
            if (!rs || rs->is_error())
            {
                status = Failed;
                goto cleanup;
            }
        }
		db->closeResults(rs);

        if (!found)
        {
            // if we've been incrementing for a while or we need to wrap, generate new
            // random pin at top of loop
            num_tries++;
            if (num_tries == kMaxTries)
            {
                num_tries = 0;
            }
            else
            {
                // increment a digit in the pin
                strncpy(inc_buff, pin_p, 1);
                int randint = atoi(inc_buff);
                randint = (randint + 1) % 10;
                if (counter == 0 && randint == 0) // leading digit can't be 0
                    randint++;
                i_itoa(randint, inc_buff);
                strncpy(pin_p, inc_buff, 1);

                // increment counter and pointer
                pin_p++;
                counter++;
                if (counter == pin_length)
                {
                    // wrap to beginning
                    counter = 0;
                    pin_p = pin_buff;
                }
            }
        }
    }
    while (!found);

cleanup:
	if (rs != NULL)
		db->closeResults(rs);

    return (status == Ok);
}

// retrieves the lengths of the meeting pin and participant pin as set in the passed
// users profile
int fetch_pin_lengths(const char* userid, int &meeting_pin_length, int &personal_pin_length, int &max_scheduled_meetings)
{
    int status = Ok;
    ResultSet *rs = NULL;
    const char *profile_options;
    IString profile_options_s;

    // find user's profile record based on their userid
    if (strlen(userid) > 0)
    {
        rs = db->execute("find-profile-by-userid", userid);
        if (!rs || rs->is_error())
            goto cleanup;
        profile_options = rs->get_value(0, "PROFILEOPTIONS");
        profile_options_s = profile_options;
        db->closeResults(rs);

        // decode profile into meeting PIN length and personal (invitee) PIN length
        unsigned int enabled_features;
        int max_voice_size, max_data_size, max_presence_monitors, password_length;
        IString bridge_phone_str, system_url_str;

        if (!DecodeProfileOptions(enabled_features,
                                  profile_options_s,
                                  max_voice_size,
                                  max_data_size,
                                  max_presence_monitors,
                                  personal_pin_length,
                                  meeting_pin_length,
                                  password_length,
                                  bridge_phone_str,
                                  system_url_str,
                                  max_scheduled_meetings))
        {
            status = Failed;
            goto cleanup;
        }
    }
    else
    {
        status = Failed;
        goto cleanup;
    }

cleanup:
	if (rs != NULL)
		db->closeResults(rs);

    return status;
}

void get_invite_url(IString &aUrl,
                    const char *aSystemUrl,
                    const char *aMeetingId,
                    const char *aParticipantId)
{
    IString URLargs;
    IString AND = "&";
    IString UNKNOWN = "unknown";
    IString QUESTION = "?";

    // mtg id
    IString meetingIdStr = "m_id=";                 // this->meeting_id
    IString meetingId = aMeetingId;
    URLargs = meetingIdStr + meetingId;

    // participant id
    IString participantIdStr = "p_id=";             // p->user->name
    IString participantId;
    if (strlen(aParticipantId) > 0)
        participantId = aParticipantId;
    else
        participantId = UNKNOWN;
    URLargs.append(AND + participantIdStr + participantId);

    // encode args
    CBase64 encoder;
    const char* encodedArgs = encoder.Encode((LPCTSTR)URLargs, URLargs.length());

    // assemble msg
    aUrl = aSystemUrl + QUESTION + "mid=" + encodedArgs;
}

// Create instant meeting for this user.
void setup_default_meeting(xmlrpc_env * env, const char *sid, const char *communityid, const char *userid,
                           const char *username, const char *name, const char *screen,
                           IString& new_meeting_id,
                           IString& new_participant_id,
                           bool use_follow_me,
                           const char *test_mid)
{
    ResultSet *rs = NULL;
    char mpin[32];
    char hpin[32];
    char selphone[32];
    char countstring[32];
    int meetingcount;
    int meeting_pin_length = 0;
    int participant_pin_length = 0;
    int max_meetings = 0;

    // Set host participant's selected phone to Follow Me if needed
    if (use_follow_me)
    {
        sprintf(selphone, "%d", SelPhoneDefault);
    }
    else
    {
        sprintf(selphone, "%d", SelPhoneNone);
    }

    // Check if this user has an instant meeting defined...
    rs = db->execute("find-instant-by-host-count", userid);
    if (!rs || rs->is_error())
        goto cleanup;
    strcpy(countstring, rs->get_value(0, 0));
    meetingcount = atoi(countstring);
    db->closeResults(rs);

    // If there is already an instant meeting we are done...
    if (meetingcount > 0)
    {
        goto cleanup;
    }

    // If we get here...we need to create an instant meeting for this user...

    // fetch pin lengths (to be used to generate the right size pins)
    if (fetch_pin_lengths(userid, meeting_pin_length, participant_pin_length, max_meetings) == Failed)
    {
		xmlrpc_env_set_fault(env, ProfileNotFound, "Error reading pin lengths");
        goto cleanup;
    }

   if (new_meeting_id.length() == 0)
   {
    // allocate meeting pin based on length
	if (!allocate_pin(mpin, meeting_pin_length, test_mid))
    {
		xmlrpc_env_set_fault(env, PINAllocationError, "Error allocating PIN");
        goto cleanup;
    }
   }
   else
   {
      strcpy(mpin, new_meeting_id.get_string());
   }

	/* Create meeting
	    <bindparam>$$MEETINGID$$</bindparam>
	    <bindparam>$$COMMUNITYID$$</bindparam>
        <bindparam>$$HOSTID$$</bindparam>
        <bindparam>$$TITLE$$</bindparam> */
	rs = db->execute("add-instant-meeting", mpin, communityid, userid, "My Meeting");
    if (!rs || rs->is_error())
        goto cleanup;
    db->closeResults(rs);

    log_reservation(EvnLog_Reserve_Add,
                    mpin,
                    "",
                    "",
                    communityid,
                    userid,
                    "0",
                    "",  // partid
                    ""); // pin


   if (new_participant_id.length() == 0)
   {
    // allocate participant pin based on length
    if (!allocate_pin(hpin, participant_pin_length))
    {
		xmlrpc_env_set_fault(env, PINAllocationError, "Error allocating PIN");
        goto cleanup;
    }
   }
   else
   {
      strcpy(hpin, new_participant_id.get_string());
   }

	/* Create host participant.
		<bindparam>$$PARTICIPANTID$$</bindparam>
        <bindparam>$$MEETINGID$$</bindparam>
		<bindparam>$$TYPE$$</bindparam>
		<bindparam>$$ROLL$$</bindparam>
		<bindparam>$$USERID$$</bindparam>
		<bindparam>$$USERNAME$$</bindparam>
		<bindparam>$$NAME$$</bindparam>
		<bindparam>$$SEL_PHONE$$</bindparam>
		<bindparam>$$PHONE$$</bindparam>
		<bindparam>$$SEL_EMAIL$$</bindparam>
		<bindparam>$$EMAIL$$</bindparam>
        <bindparam>$$SCREENNAME$$</bindparam>
		<bindparam>$$NOTIFY$$</bindparam>
		<bindparam>$$EXPIRES$$</bindparam>
		<bindparam>$$START_TIME$$</bindparam>
	 */
	rs = db->execute("add-participant", hpin, mpin, "0", "3" /* RollHost */, userid, username, name,
			selphone, "", "0", "", screen, "0", "0", "0");
    if (!rs || rs->is_error())
        goto cleanup;

    log_reservation(EvnLog_Reserve_Add_Participant,
                    mpin,
                    "", "", "", "", "",
                    hpin, hpin);

    new_meeting_id = mpin;
    new_participant_id = hpin;

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}


bool check_meeting_access(xmlrpc_env * env, const char *sid, const char *meetingid, const char *communityid)
{
    bool access = false;
    ResultSet *rs = NULL;

	rs = db->execute("find-meeting-access", meetingid, sid);
    if (!rs || rs->is_error())
        goto cleanup;

	if (rs->get_num_rows() > 0)
	{
	    int roll = atoi(rs->get_value(0, "ROLL"));
	    int options = atoi(rs->get_value(0, "OPTIONS"));

	    if (roll == RollHost || (roll == RollModerator && (options & ModeratorMayEdit) != 0) ||
            strcmp(sid, rs->get_value(0, "HOSTID")) == 0)
        {
            access = true;
        }
	}
	if (!access)
	{
        access = check_admin(env, sid, communityid);
    }

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    return access;
}


// Add participant/invite
// Parameters:
//   SESSIONID, MEETINGID, USERID, USERNAME, TYPE, ROLE, NAME, SEL_PHONE, PHONE, SEL_EMAIL, EMAIL,
//   SCREENNAME, DESCRIPTION, NOTIFYTYPE, MESSAGE, EXPIRES, STARTTIME
// Returns:
//   PARTICIPANTID
xmlrpc_value *add_participant(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	char *sid;
	char *meetingid, *userid, *username, *name, *phone, *email, *screen, *descrip, *msg;
	int type, role, sel_phone, sel_email, notify, expires, start_time;
	char pin[32];
	char buff_type[32];
	char buff_role[32];
	char buff_sel_phone[32];
	char buff_sel_email[32];
	char buff_notify[32];
	char buff_expires[32];
	char buff_start_time[32];
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    int meeting_pin_length = 0;
    int participant_pin_length = 0;
    int max_meetings = 0;
    const char *hostid;
    bool is_service;

	xmlrpc_parse_value(env, param_array, "(ssssiisisisssisii)", &sid, &meetingid, &userid,
		&username, &type, &role, &name, &sel_phone, &phone, &sel_email, &email, &screen,
		&descrip, &notify, &msg, &expires, &start_time);
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!is_service)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "API not valid for user sessions");
        XMLRPC_FAIL_IF_FAULT(env);
    }

    // retrieve meeting host - use that hostid to get default pin lengths
	rs = db->execute("find-meeting-host", meetingid);
	if (!rs || rs->is_error())
		goto cleanup;

	if (rs->get_num_rows() > 0)
	{
		// Return meeting id
		hostid = rs->get_value(0, "HOSTID");
    }
    else
    {
		xmlrpc_env_set_fault(env, Failed, "Error finding meeting host");
        goto cleanup;
    }

    // fetch pin lengths (to be used to generate the right size pins)
    if (fetch_pin_lengths(hostid, meeting_pin_length, participant_pin_length, max_meetings) == Failed)
    {
		xmlrpc_env_set_fault(env, ProfileNotFound, "Error reading pin lengths");
        goto cleanup;
    }

    // allocate participant pin based on length
    if (!allocate_pin(pin, participant_pin_length))
    {
		xmlrpc_env_set_fault(env, PINAllocationError, "Error allocating PIN");
        goto cleanup;
    }
	db->closeResults(rs);

	/* Create new participant.
		<bindparam>$$PARTICIPANTID$$</bindparam>
        <bindparam>$$MEETINGID$$</bindparam>
		<bindparam>$$TYPE$$</bindparam>
		<bindparam>$$ROLL$$</bindparam>
		<bindparam>$$USERID$$</bindparam>
		<bindparam>$$USERNAME$$</bindparam>
		<bindparam>$$NAME$$</bindparam>
		<bindparam>$$SEL_PHONE$$</bindparam>
		<bindparam>$$PHONE$$</bindparam>
		<bindparam>$$SEL_EMAIL$$</bindparam>
		<bindparam>$$EMAIL$$</bindparam>
        <bindparam>$$SCREENNAME$$</bindparam>
		<bindparam>$$NOTIFY$$</bindparam>
		<bindparam>$$EXPIRES$$</bindparam>
		<bindparam>$$START_TIME$$</bindparam>
	 */
	rs = db->execute("add-participant", pin, meetingid, i_itoa(type, buff_type),
				i_itoa(role, buff_role), userid, username, name,
				i_itoa(sel_phone, buff_sel_phone), phone,
				i_itoa(sel_email, buff_sel_email), email, screen,
				i_itoa(notify, buff_notify), i_itoa(expires, buff_expires),
                i_itoa(start_time, buff_start_time));
    if (!rs || rs->is_error())
        goto cleanup;

    log_reservation(EvnLog_Reserve_Add_Participant,
                    meetingid,
                    "", "", "", "", "",
                    pin,
                    pin);

    output = xmlrpc_build_value(env, "(s)", pin);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

	return output;
}


// Join participant -- find participant if invited, otherwise add invite now.
// Parameters:
//   SESSIONID, MEETINGID, PARTID, USERID, USERNAME, TYPE, ROLE, NAME, SEL_PHONE, PHONE, SEL_EMAIL, EMAIL,
//   SCREENNAME, DESCRIPTION, NOTIFYTYPE, MESSAGE, EXPIRES, STARTTIME
// Returns:
//   PARTICIPANTID, TYPE, ROLE
xmlrpc_value *join_participant(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	char *sid;
	char *meetingid, *partid, *userid, *username, *name, *phone, *email, *screen, *descrip, *msg;
	int type, role, sel_phone, sel_email, notify, expires, start_time;
	char pin[32];
	char buff_type[32];
	char buff_role[32];
	char buff_sel_phone[32];
	char buff_sel_email[32];
	char buff_notify[32];
	char buff_expires[32];
	char buff_start_time[32];
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    int meeting_pin_length = 0;
    int participant_pin_length = 0;
    int max_meetings = 0;
    const char *hostid;
    bool is_service;

	xmlrpc_parse_value(env, param_array, "(sssssiisisisssisii)", &sid, &meetingid, &partid, &userid,
		&username, &type, &role, &name, &sel_phone, &phone, &sel_email, &email, &screen,
		&descrip, &notify, &msg, &expires, &start_time);
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!is_service)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "API not valid for user sessions");
        XMLRPC_FAIL_IF_FAULT(env);
    }

	// See if we can find an invite for this participant
	if (*partid)
	{
		// <sql>SELECT PARTICIPANTID,MEETINGID,TYPE,ROLL,USERID,NAME,PHONE,EMAIL,
		//		SCREENNAME,AIMNAME,NOTIFYTYPE,USERNAME
		//	    FROM PARTICIPANTS WHERE PARTICIPANTID='$$PARTICIPANTID$$'</sql>
		rs = db->execute("find-participant-by-id", partid);
		if (!rs || rs->is_error())
			goto cleanup;

		if (rs->get_num_rows() > 0)
		{
			// Found participant, return info.
		    output = xmlrpc_build_value(env, "(sii)", partid, atoi(rs->get_value(0, "TYPE")),
				atoi(rs->get_value(0, "ROLL")));
			XMLRPC_FAIL_IF_FAULT(env);

			goto cleanup;
		}

		db->closeResults(rs);
	}

	// Otherwise create new participant
    // retrieve meeting host - use that hostid to get default pin lengths
	rs = db->execute("find-meeting-host", meetingid);
	if (!rs || rs->is_error())
		goto cleanup;

	if (rs->get_num_rows() > 0)
	{
		// Return meeting id
		hostid = rs->get_value(0, "HOSTID");
    }
    else
    {
		xmlrpc_env_set_fault(env, Failed, "Error finding meeting host");
        goto cleanup;
    }

    // fetch pin lengths (to be used to generate the right size pins)
    if (fetch_pin_lengths(hostid, meeting_pin_length, participant_pin_length, max_meetings) == Failed)
    {
		xmlrpc_env_set_fault(env, ProfileNotFound, "Error reading pin lengths");
        goto cleanup;
    }

    // allocate participant pin based on length
    if (!allocate_pin(pin, participant_pin_length))
    {
		xmlrpc_env_set_fault(env, PINAllocationError, "Error allocating PIN");
        goto cleanup;
    }
	db->closeResults(rs);

	/* Create new participant.
		<bindparam>$$PARTICIPANTID$$</bindparam>
        <bindparam>$$MEETINGID$$</bindparam>
		<bindparam>$$TYPE$$</bindparam>
		<bindparam>$$ROLL$$</bindparam>
		<bindparam>$$USERID$$</bindparam>
		<bindparam>$$USERNAME$$</bindparam>
		<bindparam>$$NAME$$</bindparam>
		<bindparam>$$SEL_PHONE$$</bindparam>
		<bindparam>$$PHONE$$</bindparam>
		<bindparam>$$SEL_EMAIL$$</bindparam>
		<bindparam>$$EMAIL$$</bindparam>
        <bindparam>$$SCREENNAME$$</bindparam>
		<bindparam>$$NOTIFY$$</bindparam>
		<bindparam>$$EXPIRES$$</bindparam>
		<bindparam>$$START_TIME$$</bindparam>
	 */
	rs = db->execute("add-participant", pin, meetingid, i_itoa(type, buff_type),
				i_itoa(role, buff_role), userid, username, name,
				i_itoa(sel_phone, buff_sel_phone), phone,
				i_itoa(sel_email, buff_sel_email), email, screen,
				i_itoa(notify, buff_notify), i_itoa(expires, buff_expires),
                i_itoa(start_time, buff_start_time));
    if (!rs || rs->is_error())
        goto cleanup;

    log_reservation(EvnLog_Reserve_Add_Participant,
                    meetingid,
                    "", "", "", "", "",
                    pin, pin);

    output = xmlrpc_build_value(env, "(sii)", pin, type, role);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

	return output;
}


// Remove participant/invite
// Parameters:
//   SESSIONID, MEETINGID, PARTICIPANTID
// Returns:
//   number of participants deleted
xmlrpc_value *remove_participant(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	char *sid;
	char *meetingid, *participantid;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    bool is_service;

	xmlrpc_parse_value(env, param_array, "(sss)", &sid, &meetingid, &participantid);
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!is_service)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "API not valid for user sessions");
        XMLRPC_FAIL_IF_FAULT(env);
    }

	rs = db->execute("remove-participant", meetingid, participantid);
    if (!rs || rs->is_error())
        goto cleanup;

    log_reservation(EvnLog_Reserve_Delete_Participant,
                    meetingid, "", "", "", "", "", participantid, "");

    output = xmlrpc_build_value(env, "(i)", rs->get_num_affected());
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

	return output;
}


// Fetch meeting info.
// Parameters:
//   SESSIONID, MEETINGID
// Returns:
//   "MEETINGID", "HOSTID", "TYPE", "PRIVACY", "PRIORITY", "TITLE",
//   "DESCRIPTION", "SIZE", "DURATION", "TIME", "RECURRENCE", "RECUR_END",
//   "OPTIONS", "PASSWORD", "END_TIME", "INVITE_EXPIRATION", "INVITE_VALID_BEFORE",
//   "INVITE_MESSAGE", "DISPLAY_ATTENDEES", "INVITEE_CHAT_OPTIONS",
//   "COMMUNITYID", "BRIDGEPHONE", "SYSTEMURL"

xmlrpc_value *find_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *meetingid;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    bool is_service;

	xmlrpc_parse_value(env, param_array, "(ss)", &sid, &meetingid);
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    rs = db->execute("find-meeting", meetingid);
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, 0, 0, 1, "find_meeting");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch meeting info for the given meetingid and userid.
//
// Parameters:
//   SESSIONID, MEETINGID, USERID
//
// Returns:
//    "MEETINGID", "COMMUNITYID", "HOSTID",
//    "TYPE", "PRIVACY", "PRIORITY",
//    "TITLE", "DESCRIPTION", "SIZE",
//    "DURATION", "TIME", "RECURRENCE",
//    "RECUR_END", "OPTIONS", "END_TIME",
//    "INVITE_EXPIRATION", "INVITE_VALID_BEFORE",
//    "INVITE_MESSAGE", "DISPLAY_ATTENDEES",
//    "INVITEE_CHAT_OPTIONS",
//    "PASSWORD",
//    "BRIDGEPHONE", "SYSTEMURL",
//    "STATUS", "LAST_START_TIME", "LAST_END_TIME",
//    "PARTICIPANTID", "PARTICIPANT_ROLL"
//
xmlrpc_value *find_meeting_by_userid(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *meetingid, *userid;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    bool is_service;

    xmlrpc_parse_value(env, param_array, "(sss)", &sid, &meetingid, &userid);
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!is_service && strcmp(userid, sid))
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "User may only fetch meetings he is invited to");
        goto cleanup;
    }

    rs = db->execute("find-meeting-by-userid", meetingid, userid);
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, 0, 0, 1, "find_meeting_by_userid");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch my meetings.
// Parameters:
//   SESSIONID, HOSTID
// Returns:
//    MEETINGS.MEETINGID, MEETINGS.HOSTID, MEETINGS.TYPE, MEETINGS.PRIVACY,
//    MEETINGS.PRIORITY, MEETINGS.TITLE, MEETINGS.DESCRIPTION, MEETINGS.SIZE,
//    MEETINGS.DURATION, MEETINGS.TIME, MEETINGS.RECURRENCE, MEETINGS.RECUR_END,
//    MEETINGS.OPTIONS, MEETINGSTATUS.STATUS, MEETINGSTATUS.START_TIME,
//    MEETINGSTATUS.END_TIME, PARTICIPANTS.PARTICIPANTID, PARTICIPANTS.ROLL,
//    COMMUNITIES.BRIDGEPHONE, COMMUNITIES.SYSTEMURL,
//    MEETINGS.DISPLAY_ATTENDEES, MEETINGS.INVITEE_CHAT_OPTIONS,
//    MEETINGS.PASSWORD
// **DEPRECATED**
xmlrpc_value *find_meetings_by_host(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *hostid;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    bool is_service;

	xmlrpc_parse_value(env, param_array, "(ss)", &sid, &hostid);
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!is_service)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "API not valid for user sessions");
        goto cleanup;
    }

    rs = db->execute("find-meetings-by-host", hostid);
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, 0, 0, 100, "find_meetings_by_host");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch all meetings that the given user is invited to but is not the host.
// Parameters:
//   SESSIONID, USERID
// Returns:
//    MEETINGID, HOSTID, TYPE, PRIVACY, PRIORITY, TITLE, DESCRIPTION, SIZE,
//    DURATION, TIME, RECURRENCE, RECUR_END, OPTIONS, STATUS, START_TIME,
//    END_TIME, PARTICIPANTID, ROLL, BRIDGEPHONE, SYSTEMURL, DISPLAY_ATTENDEES,
//    INVITEE_CHAT_OPTIONS
// **DEPRECATED**
xmlrpc_value *find_meetings_invited(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *userid;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    bool is_service;

	xmlrpc_parse_value(env, param_array, "(ss)", &sid, &userid);
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!is_service)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "API not valid for user sessions");
        goto cleanup;
    }

    if (strcmp(userid, sid))
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "User may only fetch his own meetings");
        goto cleanup;
    }

    rs = db->execute("find-meetings-invited", userid, userid);
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, 0, 0, 100, "find_meetings_invited");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch all public meetings (except where given user is the host).
// Parameters:
//   SESSIONID COMMUNITYID USERID
// Returns:
//   MEETINGID, HOSTID, TYPE, PRIVACY, PRIORITY, TITLE, DESCRIPTION, SIZE, DURATION, TIME,
//   RECURRENCE, RECUR_END, OPTIONS, STATUS, START_TIME, END_TIME, BRIDGEPHONE, SYSTEMURL,
//   DISPLAY_ATTENDEES, INVITEE_CHAT_OPTIONS
// **DEPRECATED**
xmlrpc_value *find_meetings_public(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *communityid, *userid;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    bool is_service;

	xmlrpc_parse_value(env, param_array, "(sss)", &sid, &communityid, &userid);
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!is_service)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "API not valid for user sessions");
        goto cleanup;
    }

    if (strcmp(userid, sid))
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "User may only fetch his own meetings");
        goto cleanup;
    }

    if (check_admin(env, sid, communityid))
    {
        rs = db->execute("find-meetings-all", communityid, userid);
    }
    else
    {
        rs = db->execute("find-meetings-public", communityid, userid);
    }
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, 0, 0, 100, "find_meetings_public");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch "list" info for my meetings.
//
// Parameters:
//   SESSIONID, HOSTID, FIRST, CHUNKSIZE
//
// Returns:
//    MEETINGS.MEETINGID, MEETINGS.HOSTID, MEETINGS.TYPE,
//    MEETINGS.PRIVACY, MEETINGS.TITLE, MEETINGS.TIME,
//    MEETINGS.OPTIONS, MEETINGSTATUS.STATUS,
//    PARTICIPANTS.PARTICIPANTID, PARTICIPANTS.ROLL,
//    HOSTFIRSTNAME, HOSTLASTNAME
//
xmlrpc_value *list_meetings_by_host(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    int first, chunksize;
    char chunk_size_s[32], first_s[32];
    char *sid, *hostid;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    UserSession *session;

    xmlrpc_parse_value(env, param_array, "(ssii)", &sid, &hostid, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    session = lookup_session(env, user_data);
    XMLRPC_FAIL_IF_FAULT(env);

    if (session->user_id != hostid)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "User may only fetch his own meetings");
        goto cleanup;
    }

    rs = db->execute("list-meetings-by-host",
                     hostid,
                     i_itoa(chunksize + 1, chunk_size_s),
                     i_itoa(first, first_s));

    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, first, 0, chunksize, "list_meetings_by_host");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch "list" info for all meetings that the given user is invited to but is not the host.
//
// Parameters:
//   SESSIONID, USERID, FIRST, CHUNKSIZE
//
// Returns:
//    MEETINGS.MEETINGID, MEETINGS.HOSTID, MEETINGS.TYPE,
//    MEETINGS.PRIVACY, MEETINGS.TITLE, MEETINGS.TIME,
//    MEETINGS.OPTIONS, MEETINGSTATUS.STATUS,
//    PARTICIPANTS.PARTICIPANTID, PARTICIPANTS.ROLL,
//    HOSTFIRSTNAME, HOSTLASTNAME
//
xmlrpc_value *list_meetings_invited(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    int first, chunksize;
    char chunk_size_s[32], first_s[32];
    char *sid, *userid;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    UserSession *session;

    xmlrpc_parse_value(env, param_array, "(ssii)", &sid, &userid, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    session = lookup_session(env, user_data);
    XMLRPC_FAIL_IF_FAULT(env);

    if (session->user_id != userid)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "User may only fetch his own meetings");
        goto cleanup;
    }

    rs = db->execute("list-meetings-invited",
                     userid,
                     userid,
                     i_itoa(chunksize + 1, chunk_size_s),
                     i_itoa(first, first_s));

    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, first, 0, chunksize, "list_meetings_invited");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch "list" info for all public meetings (except where given user is the host).
//
// Parameters:
//   SESSIONID, COMMUNITYID, USERID, FIRST, CHUNKSIZE
//
// Returns:
//    MEETINGS.MEETINGID, MEETINGS.HOSTID, MEETINGS.TYPE,
//    MEETINGS.PRIVACY, MEETINGS.TITLE, MEETINGS.TIME,
//    MEETINGS.OPTIONS, MEETINGSTATUS.STATUS,
//    PARTICIPANTS.PARTICIPANTID, PARTICIPANTS.ROLL,
//    HOSTFIRSTNAME, HOSTLASTNAME
//
xmlrpc_value *list_meetings_public(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    int first, chunksize;
    char chunk_size_s[32], first_s[32];
    char *sid, *communityid, *userid;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    UserSession *session;

    xmlrpc_parse_value(env, param_array, "(sssii)", &sid, &communityid, &userid, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    session = lookup_session(env, user_data);
    XMLRPC_FAIL_IF_FAULT(env);

    if (session->user_id != userid || session->community_id != communityid)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "User may only fetch his own meetings");
        goto cleanup;
    }

    if (check_admin(env, sid, communityid))
    {
        rs = db->execute("list-meetings-all",
                         communityid,
                         userid,
                         i_itoa(chunksize + 1, chunk_size_s),
                         i_itoa(first, first_s));
    }
    else
    {
        rs = db->execute("list-meetings-public",
                         communityid,
                         userid,
                         i_itoa(chunksize + 1, chunk_size_s),
                         i_itoa(first, first_s));
    }

    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, first, 0, chunksize, "list_meetings_public");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


//
//
// Parameters:
//    SESSIONID, COMMUNITYID, IN_PROGRESS_ONLY,
//    TITLE, DATE_RANGE_LOW, DATE_RANGE_HIGH, MEETINGID,
//    HOST_FIRSTNAME, HOST_LASTNAME,
//    FIRST, CHUNKSIZE
//
// Returns:
//    MEETINGS.MEETINGID, MEETINGS.HOSTID, MEETINGS.TYPE,
//    MEETINGS.PRIVACY, MEETINGS.TITLE, MEETINGS.TIME,
//    MEETINGS.OPTIONS, MEETINGSTATUS.STATUS,
//    CONTACTS.FIRSTNAME, CONTACTS.LASTNAME
//
xmlrpc_value *search_meetings(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    int first, chunksize;
    char date_range_low_s[32], date_range_high_s[32], chunk_size_s[32], first_s[32];
    char *sid, *communityid, *userid, *title, *meetingid, *firstname, *lastname;
    int in_progress_only, date_range_low, date_range_high;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    IString privacy_list;
    IString status_list;
    IString meetingid_clause;
    UserSession *session;

    xmlrpc_parse_value(env, param_array, "(sssisiisssii)", &sid, &communityid, &userid, &in_progress_only, &title,
                       &date_range_low, &date_range_high, &meetingid, &firstname, &lastname, &first, &chunksize);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    session = lookup_session(env, user_data);
    XMLRPC_FAIL_IF_FAULT(env);

    // Validate that meetingid (if provided) is numeric
    if (isdigits(meetingid))
    {
        // Meetingid is valid, so build a where clause for it
        meetingid_clause = "MEETINGS.MEETINGID=";
        meetingid_clause.append(meetingid);
        meetingid_clause.append(" AND");
    }
    else if (strlen(meetingid) > 0)
    {
        // meetingid was not digits and was not an empty string, so report error
        xmlrpc_env_set_fault(env, InvalidMeetingID, "Meeting ID must be numeric or empty");
        goto cleanup;
    }


    // If session is for an admin, search all meeting types...otherwise only search public
    if (check_admin(env, sid, communityid))
    {
        privacy_list = "0,1,2";  // All privacy types
    }
    else
    {
        if (session->community_id != communityid)
        {
            xmlrpc_env_set_fault(env, NotAuthorized, "User may only search his community");
            goto cleanup;
        }

        privacy_list = "0";  // Public meetings only
    }

    // Set the list of values for either "in progress only" meeting, or all meetings
    if (in_progress_only == 1)
    {
        status_list = "1";  // "In Progress" meeting status
    }
    else
    {
        status_list = "0,1,2";  // All meeting status
    }

    rs = db->execute("search-meetings",
                     communityid,
                     userid,
                     (const char *)privacy_list,
                     (const char *)status_list,
                     title,
                     (const char *)meetingid_clause,
                     i_itoa(date_range_low, date_range_low_s),
                     i_itoa(date_range_high, date_range_high_s),
                     firstname,
                     lastname,
                     i_itoa(chunksize + 1, chunk_size_s),
                     i_itoa(first, first_s));

    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, first, 0, chunksize, "search_meetings");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch meeting participants.
// Parameters:
//   SESSIONID, MEETINGID
// Returns:
//   "PARTICIPANTID", "TYPE", "ROLL", "USERID", "USERNAME", "NAME", "DESCRIPTION", "SELPHONE", "PHONE", "SELEMAIL", "EMAIL",
//	 "SCREENNAME", "AIMNAME", "NOTIFYTYPE"
// **DEPRECATED**
xmlrpc_value *find_participants(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *meetingid;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    bool is_service;

	xmlrpc_parse_value(env, param_array, "(ss)", &sid, &meetingid);
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!is_service)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "API not valid for user sessions");
        goto cleanup;
    }

    rs = db->execute("find-participants", meetingid);
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, 0, 0, 1000, "find_participants");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch meeting details
// Parameters:
//   SESSIONID, MEETINGID
// Returns:
//   Result set with:
//   Rows 0 -> chunksize-1:  "PARTICIPANTID", "TYPE", "ROLL", "USERID", "USERNAME", "NAME", "DESCRIPTION", "SELPHONE", "PHONE", "SELEMAIL", "EMAIL",
//		"SCREENNAME", "AIMNAME", "NOTIFYTYPE"
//   Row chunksize: "MEETINGID", "HOSTID", "TYPE", "PRIVACY", "PRIORITY", "TITLE",
//      "DESCRIPTION", "SIZE", "DURATION", "TIME", "RECURRENCE", "RECUR_END",
//      "OPTIONS", "PASSWORD", "END_TIME", "INVITE_EXPIRATION", "INVITE_VALID_BEFORE",
//      "INVITE_MESSAGE", "DISPLAY_ATTENDEES", "INVITEE_CHAT_OPTIONS", "COMMUNITYID",
//		"BRIDGEPHONE", "SYSTEMURL", "ENABLED_FEATURES"
//   Actual number of rows will be chunksize + 1
xmlrpc_value *find_meeting_details(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *meetingid;
    xmlrpc_value *output = NULL;
	xmlrpc_value * array = NULL;
	xmlrpc_value * row = NULL;
    ResultSet *rs = NULL;
    IString fieldstr;
    bool is_service;

	xmlrpc_parse_value(env, param_array, "(ss)", &sid, &meetingid);
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    // <sql>SELECT PARTICIPANTID,TYPE,ROLL,PARTICIPANTS.USERID,PARTICIPANTS.USERNAME,NAME,DESCRIPTION,SELPHONE,PHONE,SELEMAIL,PARTICIPANTS.EMAIL,
	//	 PARTICIPANTS.SCREENNAME,PARTICIPANTS.AIMNAME,NOTIFYTYPE
	//	 FROM PARTICIPANTS WHERE MEETINGID='$$MEETINGID$$'</sql>
    rs = db->execute("find-participants", meetingid);
    if (!rs || rs->is_error())
        goto cleanup;

    output = build_output(env, rs, 0, 0, 1000, "find_meeting_details");
    db->closeResults(rs);

    //  <sql>SELECT MEETINGS.MEETINGID,MEETINGS.HOSTID,MEETINGS.TYPE,MEETINGS.PRIVACY,
    //              MEETINGS.PRIORITY,MEETINGS.TITLE,MEETINGS.DESCRIPTION,MEETINGS.SIZE,
    //              MEETINGS.DURATION,MEETINGS.TIME,MEETINGS.RECURRENCE,
    //              MEETINGS.RECUR_END,MEETINGS.OPTIONS,MEETINGS.PASSWORD,
	//              MEETINGS.END_TIME,MEETINGS.INVITE_EXPIRATION,MEETINGS.INVITE_VALID_BEFORE,
    //              MEETINGS.INVITE_MESSAGE,MEETINGS.DISPLAY_ATTENDEES,MEETINGS.INVITEE_CHAT_OPTIONS,
    //	            MEETINGS.COMMUNITYID, COMMUNITIES.BRIDGEPHONE, COMMUNITIES.SYSTEMURL,
    //              PROFILES.ENABLEDFEATURES
	//              FROM MEETINGS
	//       JOIN COMMUNITIES ON MEETINGS.COMMUNITYID=COMMUNITIES.COMMUNITYID
    //       JOIN CONTACTS ON MEETINGS.HOSTID=CONTACTS.USERID
    //       JOIN PROFILES ON CONTACTS.PROFILEID=PROFILES.PROFILEID
	//		 WHERE MEETINGID='$$MEETINGID$$'</sql>
    rs = db->execute("find-meeting-details", meetingid);
    if (!rs || rs->is_error())
        goto cleanup;

	row = xmlrpc_build_value(env, "()");
	XMLRPC_FAIL_IF_FAULT(env);

    if (rs->get_num_rows())
    {
        for (int j=0; j < rs->get_num_fields(); j++)
        {
            const char *valstr = rs->get_value(0, j);
            if (valstr == NULL)
            {
                xmlrpc_env_set_fault(env, DBAccessError, "Error reading from database result set");
                goto cleanup;
            }
            xmlrpc_value *value = xmlrpc_build_value(env, "s", valstr);
            XMLRPC_FAIL_IF_FAULT(env);

            xmlrpc_array_append_item(env, row, value);
            xmlrpc_DECREF(value);
            XMLRPC_FAIL_IF_FAULT(env);
        }
    }
    else
    {
		xmlrpc_env_set_fault(env, InvalidMeetingID, "Meeting not found");
		goto cleanup;
    }

	// Add meeting row to result.
	array = xmlrpc_array_get_item(env, output, 0);
    XMLRPC_FAIL_IF_FAULT(env);

    xmlrpc_array_append_item(env, array, row);
	xmlrpc_DECREF(row);
	XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Lookup PIN.
// Parameters:
//   SESSIONID, PIN[, OPTIONS]
// Returns:
//   "FOUND", "MEETINGID", "PARTICIPANTID", "USERID", "EMAIL",
//   "USERNAME", "NAME", "OPTIONS", "SCHEDULED_TIME", "INVITE_VALID_BEFORE",
//   "INVITE_EXPIRATION", "TYPE", "INVITED_TIME"
//   FOUND: 1 = meeting found, 2 = meeting and user found, 0 = not found
xmlrpc_value *find_pin(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *pin;
    int options = 0;
    int role = RollNormal;
    xmlrpc_value *output = NULL;
    const char *out_encode;
    ResultSet *rs = NULL;
    ResultSet *rs2 = NULL;
    IString fieldstr;

    int params = xmlrpc_array_size(env, param_array);
    XMLRPC_FAIL_IF_FAULT(env);

    if (params < 3) {
        xmlrpc_parse_value(env, param_array, "(ss)", &sid, &pin);
    } else {
        xmlrpc_parse_value(env, param_array, "(ssi)", &sid, &pin, &options);
    }
    XMLRPC_FAIL_IF_FAULT(env);

    // Option 1: include participant role in output
    out_encode = options & 1 ? "(issssssiiiiiii)" : "(issssssiiiiii)";

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

	// First check meeting table.
    rs = db->execute("find-meeting", pin);
    if (!rs || rs->is_error())
        goto cleanup;

	if (rs->get_num_rows() > 0)
	{
		// Return meeting id
        output = xmlrpc_build_value(env, out_encode, 1, rs->get_value(0, "MEETINGID"), "", "", "", "", "",
						atoi(rs->get_value(0, "OPTIONS")),
                        atoi(rs->get_value(0, "TIME")),
                        atoi(rs->get_value(0, "INVITE_VALID_BEFORE")),
                        atoi(rs->get_value(0, "INVITE_EXPIRATION")),
                        0, 0, role);
        XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
    	db->closeResults(rs);

		// Now check participant table.
		rs = db->execute("find-participant-by-id", pin);
		if (!rs || rs->is_error())
			goto cleanup;

		if (rs->get_num_rows() > 0)
		{
			// We got meeting id, but we also want to look up meeting and return option flags.
			const char * meetingid = rs->get_value(0, "MEETINGID");
			role = atoi(rs->get_value(0, "ROLL"));

			rs2 = db->execute("find-meeting", meetingid);
			if (!rs2 || rs2->is_error())
				goto cleanup;

			if (rs2->get_num_rows() > 0)
			{
				// Return meeting id and participant id
				output = xmlrpc_build_value(env, out_encode, 2, meetingid, rs->get_value(0, "PARTICIPANTID"),
							rs->get_value(0, "USERID"), rs->get_value(0, "EMAIL"), rs->get_value(0, "USERNAME"),
							rs->get_value(0, "NAME"), atoi(rs2->get_value(0, "OPTIONS")),
                            atoi(rs2->get_value(0, "TIME")),
                            atoi(rs2->get_value(0, "INVITE_VALID_BEFORE")),
                            atoi(rs->get_value(0, "EXPIRES")),
                            atoi(rs->get_value(0, "TYPE")),
                            atoi(rs->get_value(0, "START_TIME")),
                            role
                            );
				XMLRPC_FAIL_IF_FAULT(env);
			}
		}
	}

	if (output == NULL)
	{
		// Nothing found
		output = xmlrpc_build_value(env, "(issssssiiiiii)", 0, "", "", "", "", "", "", 0, 0, 0, 0, 0, 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (rs2 != NULL)
    {
        if (rs2->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs2->get_error());
        }
        db->closeResults(rs2);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

// Lookup INVITE.
// Parameters:
//   SESSIONID, MID, PIN[, OPTIONS]
// Returns:
//   "FOUND", "MEETINGID", "TITLE", "TIME", "HOST", "SERVER", "PARTICIPANTID", "SCREENNAME", "NAME", "OPTIONS",
//   "DESCRIPTION", "MEETING INVITATION", "PASSWORD", "PARTICPANT PHONE", "PARTICIPANT EMAIL"
//   FOUND: 1 = meeting found, 2 = meeting and user found, 0 = not found
xmlrpc_value *find_invite(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *mid, *pin;
    int options = 0;
    xmlrpc_value *output = NULL;
    const char *out_encode;
    ResultSet *rs = NULL;
    ResultSet *rs2 = NULL;
    ResultSet *rs3 = NULL;
    IString fieldstr;

    int params = xmlrpc_array_size(env, param_array);
    XMLRPC_FAIL_IF_FAULT(env);

    if (params < 4) {
        xmlrpc_parse_value(env, param_array, "(sss)", &sid, &mid, &pin);
    } else {
        xmlrpc_parse_value(env, param_array, "(sssi)", &sid, &mid, &pin, &options);
    }
    XMLRPC_FAIL_IF_FAULT(env);

    // Option 1: include meeting type in output
    out_encode = options & 1 ? "(issssssssisssssi)" : "(issssssssisssss)";

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (*pin != '\0')
	{
		// PIN was specified, look up.
		rs = db->execute("find-participant-by-id", pin);
		if (!rs || rs->is_error())
			goto cleanup;

		if (rs->get_num_rows() > 0)
		{
			// We got meeting id, but we also want to look up meeting and return option flags.
			const char * meetingid = rs->get_value(0, "MEETINGID");

			rs2 = db->execute("find-meeting", meetingid);
			if (!rs2 || rs2->is_error())
				goto cleanup;

			if (rs2->get_num_rows() > 0)
			{
				int pos;
				IString hostfullname, userfullname, username;

                // fetch hostname (first last)
                const char *hostid = rs2->get_value(0, "HOSTID");
                rs3 = db->execute("fetch-contact-by-userid", hostid, "FIRSTNAME,LASTNAME");
 				if (!rs3 || rs3->is_error())
					goto cleanup;

                hostfullname = IString(rs3->get_value(0, "FIRSTNAME")) + " " + rs3->get_value(0, "LASTNAME");

                // fetch participant full name (first last)
				userfullname = rs->get_value(0, "NAME");

                // We used to retrieve screen name but if participant is invited via
                // ad hoc path screenname is used to store their IM screenname
                // Now we retrieve username and parse IIC screen name from it.
			    username = rs->get_value(0, "USERNAME");
				pos = username.find("@");
			    if (pos > 0)
				    username.erase(pos, username.length() - pos);
                if (username.length() == 0)
                    username = ANONYMOUS_NAME;

                // retrieve display name (first last) for participant from user id

	            // "FOUND", "MEETINGID", "TITLE", "TIME", "HOST", "SERVER", "PARTICIPANTID", "SCREENNAME", "NAME", "OPTIONS"
                // "DESCRIPTION", "MEETING INVITATION", "PASSWORD", "PARTICIPANT PHONE", "PARTICIPANT EMAIL"
				output = xmlrpc_build_value(env, out_encode, 2,
                            meetingid,
                            rs2->get_value(0, "TITLE"),
                            rs2->get_value(0, "TIME"),
                            (const char *)hostfullname,
                            (const char *)g_server_name,
                            rs->get_value(0, "PARTICIPANTID"),
                            (const char *)username,
							(const char *)userfullname,
                            atoi(rs2->get_value(0, "OPTIONS")),
                            rs2->get_value(0, "DESCRIPTION"),
                            rs2->get_value(0, "INVITE_MESSAGE"),
                            rs2->get_value(0, "PASSWORD"),
                            rs->get_value(0, "PHONE"),
                            rs->get_value(0, "EMAIL"),
                            atoi(rs->get_value(0, "MTG_TYPE")));
				XMLRPC_FAIL_IF_FAULT(env);
			}
		}
	}

    // If we have a pin and no meeting id, and did not find the pin, try using the pin
    // as the meeting id.
    if (*pin != '\0' && *mid == '\0')
    {
        mid = pin;
    }

	if (output == NULL && *mid != '\0')
	{
		// PIN not found or not specified, look up meeting info
		rs = db->execute("find-meeting", mid);
		if (!rs || rs->is_error())
			goto cleanup;

		if (rs->get_num_rows() > 0)
		{
			// Return meeting id
			const char *hostid = rs->get_value(0, "HOSTID");
			IString fullname;

            // lookup host name
			rs2 = db->execute("fetch-contact-by-userid", hostid, "FIRSTNAME,LASTNAME");
			if (!rs2 || rs2->is_error())
				goto cleanup;

			// strcat first and last
			fullname = IString(rs2->get_value(0, "FIRSTNAME")) + " " + rs2->get_value(0, "LASTNAME");

			// "FOUND", "MEETINGID", "TITLE", "TIME", "HOST", "SERVER", "PARTICIPANTID", "SCREENNAME", "NAME", "OPTIONS",
            // "DESCRIPTION", "MEETING INVITATION", "PASSWORD", "PARTICPANT PHONE", "PARTICIPANT EMAIL"
			output = xmlrpc_build_value(env, out_encode, 1,
										rs->get_value(0, "MEETINGID"),
										rs->get_value(0, "TITLE"),
										rs->get_value(0, "TIME"),
										(const char *)fullname,
										(const char *)g_server_name,
										"",
                                        ANONYMOUS_NAME,
                                        "",
										atoi(rs->get_value(0, "OPTIONS")),
                                        rs->get_value(0, "DESCRIPTION"),
                                        rs->get_value(0, "INVITE_MESSAGE"),
                                        rs->get_value(0, "PASSWORD"),
                                        "",
                                        "",
                                        atoi(rs->get_value(0, "TYPE")));
			XMLRPC_FAIL_IF_FAULT(env);
		}
    }


	if (output == NULL)
	{
		// Nothing found--bad ids, or invite has expired.
		output = xmlrpc_build_value(env, out_encode, 0, "", "", "", "", "", "", "", "", 0, "", "", "", "", "", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (rs2 != NULL)
    {
        if (rs2->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs2->get_error());
        }
        db->closeResults(rs2);
    }
    if (rs3 != NULL)
    {
        if (rs3->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs3->get_error());
        }
        db->closeResults(rs3);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

// Insert a meeting status, using current time
//
// Parameters:
//   "MEETINGID", "STATUS", "STARTTIME", "ENDTIME"
// Returns:
//   Status, 1 if inserted, 0 if failed.
xmlrpc_value *insert_meeting_status(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    time_t currenttime = time(NULL);
    int intCurrentTime = (int)currenttime;

    char *meetingid;
    int status, starttime, endtime;
    ResultSet *rs = NULL;
    xmlrpc_value *output = NULL;
	char buffTime[32];
	char buffStartTime[32];
	char buffEndTime[32];
	char buffStatus[32];
	bool is_service;

    // Init start and end time to zero.
    int intStartTime = 0;
    int intEndTime = 0;
    sprintf(buffStartTime, "%d", intStartTime);
    sprintf(buffEndTime, "%d", intEndTime);

    // Parse the given parameters
	xmlrpc_parse_value(env, param_array, "(siii*)", &meetingid, &status, &starttime, &endtime);
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, NULL);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!is_service)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "API not valid for user sessions");
        XMLRPC_FAIL_IF_FAULT(env);
    }

	// First see if we have an existing record.  Get existing
    // start and end times if one is found...
    rs = db->execute("find-meeting-status", meetingid);
    if (!rs || rs->is_error())
        goto cleanup;

	if (rs->get_num_rows() > 0)
	{
        strcpy(buffStartTime, rs->get_value(0, "START_TIME"));
        strcpy(buffEndTime, rs->get_value(0, "END_TIME"));
	}
	db->closeResults(rs);

    sprintf(buffTime, "%d", intCurrentTime);
    sprintf(buffStatus, "%d", status);

    // Set start and end times as appropriate for the current_status
    if (status == StatusInProgress)
    {
        intStartTime = intCurrentTime;
        sprintf(buffStartTime, "%d", intStartTime);
        intEndTime = 0;
        sprintf(buffEndTime, "%d", intEndTime);
    }
    else if (status == StatusCompleted)
    {
        intEndTime = intCurrentTime;
        sprintf(buffEndTime, "%d", intEndTime);

        // Erase "old" expired instant invitations.
    	char buffActualStartTime[32];
        sprintf(buffActualStartTime, "%d", starttime);

        rs = db->execute("cleanup-instant-invitations", meetingid, buffActualStartTime, buffTime);
        if (!rs || rs->is_error())
            goto cleanup;
        db->closeResults(rs);
    }

    // Erase old status entry for this meeting.
    rs = db->execute("delete-meeting-status", meetingid);
    if (!rs || rs->is_error())
        goto cleanup;
    db->closeResults(rs);

    // Insert current meeting status
    rs = db->execute("insert-meeting-status",
                        meetingid,
                        buffTime,
                        buffStartTime,
                        buffEndTime,
                        buffStatus);
    if (!rs || rs->is_error())
        goto cleanup;

    output = xmlrpc_build_value(env, "(i)", rs->get_num_affected());
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (rs)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    /* Return our result. */
    return output;
}

// Reset meeting status.
// Parameters:
//   SESSIONID, SERVER
// Returns:
//   number rows
xmlrpc_value *reset_meeting_status(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *server;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    bool is_service;

	xmlrpc_parse_value(env, param_array, "(ss)", &sid, &server);
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!is_service)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "API not valid for user sessions");
        XMLRPC_FAIL_IF_FAULT(env);
    }

    rs = db->execute("reset-meeting-status");
    if (!rs || rs->is_error())
        goto cleanup;

    output = xmlrpc_build_value(env, "(i)", rs->get_num_affected());
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

void find_host_info(xmlrpc_env *env,
                    const char *hostid,
                    char *host_displayname,
                    char *host_username,
                    char *host_screenname)
{
    ResultSet *rs = NULL;
    *host_displayname = 0;
    *host_username = 0;
    *host_screenname = 0;

    rs = db->execute("find-contact-all-by-userid", hostid);
    if (!rs || rs->is_error())
        goto cleanup;

    if (rs->get_num_rows() > 0)
    {
        const char *username = (char*)rs->get_value(0, "USERNAME");
        const char *firstname = (char*)rs->get_value(0, "FIRSTNAME");
        const char *lastname = (char*)rs->get_value(0, "LASTNAME");
        const char *screenname = (char*)rs->get_value(0, "SCREENNAME");

        if (username != NULL &&
            firstname != NULL &&
            lastname != NULL &&
            screenname != NULL)
	{
	    sprintf(host_displayname, "%s %s", firstname, lastname);
            sprintf(host_username, "%s", username);
            sprintf(host_screenname, "%s", screenname);
            goto cleanup;
        }
    }

    log_error(ZONE, "Unable to find meeting host: %s\n", hostid);
    xmlrpc_env_set_fault(env, Failed, "Unable to find meeting host");

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}

// Common code for creating or updating meeting.
void schedule_meeting(xmlrpc_env *env, int update_type, int meetingcount, int email_type,
        char *email_host_name,  char *email_bridge_phone, char *email_system_url,
        char *communityid, char *meetingid, char *hostid, int type, int privacy,
        int priority, char *title, char *descrip, int meeting_size, int duration, char *password,
        int options, int start_time, int end_time, int invite_expiration, int invite_valid_before,
        char *invite_message, int display_attendees, int invitee_chat_options,
        xmlrpc_value *participants, char *out_mid)
{
	char buff_type[32];
	char buff_privacy[32];
	char buff_priority[32];
	char buff_size[32];
	char buff_duration[32];
	char buff_options[32];
	char buff_start_time[32];
	char buff_end_time[32];
	char buff_invite_expiration[32];
    char buff_invite_valid_before[32];
	char buff_display_attendees[32];
	char buff_invitee_chat_options[32];
    char part_pin[32];
    char meet_pin[32];
	char buff_parttype[32];
	char buff_role[32];
	char buff_sel_phone[32];
	char buff_sel_email[32];
	char buff_notify[32];
	char buff_expires[32];
    char host_displayname[513];
    char host_username[256];
    char host_screenname[256];
    ResultSet *rs = NULL;
    int loop = 0;
    int size = 0;
    bool bHostFound = false;  // Make sure we have a host participant...
    bool bNewMeeting = false;
    bool bSendEmail = false;
    IString email_invite;
    char countstring[32];
    int meeting_pin_length = 0;
    int participant_pin_length = 0;
    int max_meetings = 0;

    // fetch pin lengths (to be used to generate the right size pins)
    if (fetch_pin_lengths(hostid, meeting_pin_length, participant_pin_length, max_meetings) == Failed)
    {
		xmlrpc_env_set_fault(env, ProfileNotFound, "Error reading pin lengths");
        goto cleanup;
    }

    // If we were not given a meeting id, or the meeting id does not
    // exist, then get a new one and insert the record, otherwise
    // update the existing meeting record...
    if (meetingcount == 0)
    {
        bNewMeeting = true;

        // count number of meetings this host already has
        rs = db->execute("find-scheduled-by-host-count", hostid);
        if (!rs || rs->is_error())
            goto cleanup;
        strcpy(countstring, rs->get_value(0, 0));
        meetingcount = atoi(countstring);
        db->closeResults(rs);

        if (meetingcount >= max_meetings)
        {
    		xmlrpc_env_set_fault(env, MaxMeetingsExceeded, "Host has maximum number of meetings scheduled");
            goto cleanup;
        }

        // Use id if specified, otherwise allocate meeting pin based on length
        if ((int)strlen(meetingid) >= meeting_pin_length)
        {
        	strcpy(meet_pin, meetingid);
        }
        else if (!allocate_pin(meet_pin, meeting_pin_length))
        {
    		xmlrpc_env_set_fault(env, PINAllocationError, "Error allocating PIN");
            goto cleanup;
        }
        if (out_mid != NULL)
        	strcpy(out_mid, meet_pin);

        // Insert meeting
        rs = db->execute("insert-meeting", hostid, communityid, i_itoa(type, buff_type),
                         i_itoa(privacy, buff_privacy), i_itoa(priority, buff_priority),
                         title, descrip,	i_itoa(meeting_size, buff_size),
                         i_itoa(duration, buff_duration), password, i_itoa(options, buff_options),
                         i_itoa(start_time, buff_start_time), i_itoa(end_time, buff_end_time),
                         i_itoa(invite_expiration, buff_invite_expiration),
                         i_itoa(invite_valid_before, buff_invite_valid_before),
                         invite_message, i_itoa(display_attendees, buff_display_attendees),
                i_itoa(invitee_chat_options, buff_invitee_chat_options), meet_pin);
        if (!rs || rs->is_error())
        {
            goto cleanup;
        }
        db->closeResults(rs);

        log_reservation(EvnLog_Reserve_Add,
                        meet_pin,
                        buff_start_time,
                        buff_end_time,
                        communityid,
                        hostid,
                        buff_options,
                        "",  // partid
                        ""); // pin

    }
    else // update existing
    {
    	strcpy(meet_pin, meetingid);
        if (out_mid != NULL)
        	strcpy(out_mid, meet_pin);

        // Delete any existing meeting status
        /* User may update meeting schedule while meeting is in progress--don't reset status.
        rs = db->execute("delete-meeting-status", meet_pin);
        if (!rs || rs->is_error())
            goto cleanup;
        db->closeResults(rs);*/

        // Update meeting
        rs = db->execute("update-meeting", hostid, i_itoa(type, buff_type),
                i_itoa(privacy, buff_privacy), i_itoa(priority, buff_priority),
                title, descrip,	i_itoa(meeting_size, buff_size),
                i_itoa(duration, buff_duration), password, i_itoa(options, buff_options),
                i_itoa(start_time, buff_start_time), i_itoa(end_time, buff_end_time),
                i_itoa(invite_expiration, buff_invite_expiration),
                i_itoa(invite_valid_before, buff_invite_valid_before),
                invite_message, i_itoa(display_attendees, buff_display_attendees),
                i_itoa(invitee_chat_options, buff_invitee_chat_options), meet_pin);
        if (!rs || rs->is_error())
        {
            goto cleanup;
        }
        db->closeResults(rs);

        log_reservation(EvnLog_Reserve_Update,
                        meetingid,
                        buff_start_time,
                        buff_end_time,
                        communityid,
                        hostid,
                        buff_options,
                        "",  // partid
                        ""); // pin

    }

    if (update_type == UpdateMeetingAndParticipants)
    {
        // Find email for Host if possible, otherwise use system email
        char sent_by_email[256];
        find_email_by_userid(env, hostid, sent_by_email);
        XMLRPC_FAIL_IF_FAULT(env);

        // Remove existing participant records
	    rs = db->execute("remove-scheduled-participants", meet_pin);
        if (!rs || rs->is_error())
            goto cleanup;
		db->closeResults(rs);

        log_reservation(EvnLog_Reserve_Delete_All_Participants,
                        meet_pin, "", "", "", "", "", "", "");

        // Insert new participant records
        size = xmlrpc_array_size(env, participants);
        XMLRPC_FAIL_IF_FAULT(env);

        for (loop=0; loop < size; loop++)
        {
            char *participantid, *partmeetingid, *userid, *username, *name;
            char *description, *phone, *email, *iicscreenname;
            int  participanttype, role, selphone, selemail, notifytype;
            int  expires = 0;
            char buffer_userid[32];

            xmlrpc_value *val = xmlrpc_array_get_item(env, participants, loop);
            XMLRPC_FAIL_IF_FAULT(env);

		    xmlrpc_parse_value(env, val, "(ssiissssisissi)",
			    &participantid, &partmeetingid, &participanttype, &role, &userid,
                &username, &name, &description, &selphone, &phone, &selemail,
                &email, &iicscreenname, &notifytype);

		    if (env->fault_occurred)
		    {
			    xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments for new participant");
			    goto cleanup;
		    }

            // Get the participant id, or create a new one if necessary...
            if (strlen(participantid) && bNewMeeting == false)
            {
                strcpy(part_pin, participantid);
                bSendEmail = email_type == SendEmailAll;
            }
            else
            {
                // allocate participant pin based on length
                if (!allocate_pin(part_pin, participant_pin_length))
                {
            		xmlrpc_env_set_fault(env, PINAllocationError, "Error allocating PIN");
                    goto cleanup;
                }

                bSendEmail = email_type != SendEmailNone;
            }

            if (isdigits(userid))
            {
                strcpy(buffer_userid, userid);
            }
            else
            {
                strcpy(buffer_userid, "-1");
            }

            if (strcmp(hostid, buffer_userid) == 0 && !bHostFound)
            {
                bHostFound = true;
                role = RollHost;
            }

	        rs = db->execute("add-participant", part_pin, meet_pin,
                     i_itoa(participanttype, buff_parttype), i_itoa(role, buff_role),
                     buffer_userid, username, name, i_itoa(selphone, buff_sel_phone),
                     phone, i_itoa(selemail, buff_sel_email), email, iicscreenname,
				     i_itoa(notifytype, buff_notify), i_itoa(expires, buff_expires), "0");
            if (!rs || rs->is_error())
                goto cleanup;
			db->closeResults(rs);

            log_reservation(EvnLog_Reserve_Add_Participant,
                            meet_pin,
                            "", "", "", "", "",
                            part_pin, part_pin);

            if ((bSendEmail) &&
                (selemail == SelEmailThis) &&
                (strlen(email) > 0))
            {
            	const char * mailer_id = "schedule_mail";
            	char id[32];

			    sprintf(id, "%s:%d", mailer_id, get_next_mailer_id());

                if (rpc_make_call(id, "mailer", "mailer.send_invite_email",
                                  "(ssssssssssssiiiiiis)",
                                  email, meet_pin, title, email_host_name, part_pin,
                                  name, descrip, invite_message,
                                  password, phone, email_bridge_phone,
                                  email_system_url, options, type, privacy, start_time, end_time,
                                  1, sent_by_email))
			    {
			        log_error(ZONE, "Unable to send email notification");
			    }
            }
        }

        // Make sure there is a host record...add if necessary
        if (!bHostFound)
        {
            // allocate participant pin based on length
            if (!allocate_pin(part_pin, participant_pin_length))
            {
                xmlrpc_env_set_fault(env, PINAllocationError, "Error allocating host PIN");
                goto cleanup;
            }

            // Lookup additional info from the host's user record
            find_host_info(env, hostid, host_displayname, host_username, host_screenname);
            XMLRPC_FAIL_IF_FAULT(env);

            // Add a new participant record for the host
            rs = db->execute("add-participant", part_pin, meet_pin,
                     i_itoa(ParticipantNormal, buff_parttype), i_itoa(RollHost, buff_role),
                     hostid, host_username, host_displayname, i_itoa(SelPhoneNone, buff_sel_phone),
                     "", i_itoa(SelEmailNone, buff_sel_email), "", host_screenname,
                     i_itoa(NotifyNone, buff_notify), "0", "0");
            if (!rs || rs->is_error())
                goto cleanup;
            db->closeResults(rs);
            log_reservation(EvnLog_Reserve_Add_Participant,
                            meet_pin, "", "", "", "", "", part_pin, part_pin);
        }
    }

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
}

// Create meeting.
// Notes:
//   If meeting id is specified, meeting must not already exist.  Can be blank to
//   allocate new id.
// Parameters:
//   SESSIONID, EMAIL_TYPE, EMAIL_HOST_NAME, EMAIL_BRIDGE_PHONE, EMAIL_SYSTEM_URL,
//   COMMUNITYID, MEETINGID, HOSTID, TYPE, PRIVACY, PRIORITY, TITLE,
//   DESCRIPTION, SIZE, DURATION, PASSWORD, OPTIONS, START_TIME, END_TIME,
//   INVITE_EXPIRATION, INVITE_VALID_BEFORE, INVITE_MESSAGE, DISPLAY_ATTENDEES,
//   INVITE_CHAT_OPTIONS, PARTICIPANTS
// Returns:
//   MEETINGID, status (ok, pending)
xmlrpc_value *create_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	char *sessionid, *communityid, *meetingid, *hostid, *title, *descrip, *password, *invite_message;
    char *email_host_name, *email_bridge_phone, *email_system_url;
	int email_type,type, privacy, priority, meeting_size, duration, options;
    int start_time, end_time, invite_expiration, invite_valid_before, display_attendees, invitee_chat_options;
	xmlrpc_value *participants;
    ResultSet *rs = NULL;
    xmlrpc_value *output = NULL;
    char countstring[32];
    char meet_id[32];
    int meetingcount = 0;
	int status = Ok;

    // Parse meeting info...
    xmlrpc_parse_value(env, param_array, "(sissssssiiissiisiiiiisiiA)",
        &sessionid, &email_type,
        &email_host_name, &email_bridge_phone, &email_system_url,
        &communityid, &meetingid, &hostid, &type, &privacy,
        &priority, &title, &descrip, &meeting_size, &duration, &password,
        &options, &start_time, &end_time, &invite_expiration, &invite_valid_before,
        &invite_message, &display_attendees, &invitee_chat_options,
        &participants);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to schedule.create_meeting");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    // If we were given a meeting id, see if the meeting record
    // already exists
    if (strlen(meetingid) > 0)
    {
        rs = db->execute("find-meeting-count", meetingid);
        if (!rs || rs->is_error())
            goto cleanup;
        strcpy(countstring, rs->get_value(0, 0));
        meetingcount = atoi(countstring);
        db->closeResults(rs);

        if (meetingcount > 0)
        {
	        xmlrpc_env_set_fault(env, DuplicateID, "Meeting Id already exists");
    	    goto cleanup;
        }
    }

	schedule_meeting(env, UpdateMeetingAndParticipants, meetingcount, email_type,
        			email_host_name, email_bridge_phone, email_system_url,
			        communityid, meetingid, hostid, type, privacy,
			        priority, title, descrip, meeting_size, duration, password,
			        options, start_time, end_time, invite_expiration, invite_valid_before,
			        invite_message, display_attendees, invitee_chat_options,
			        participants, meet_id);
    if (env->fault_occurred)
	{
		goto cleanup;
	}

    // Create return value.
    output = xmlrpc_build_value(env, "(si)", meet_id, status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, status, "Unable to create meeting");
    return output;
}

// Update meeting.
// Notes:
// Parameters:
//   SESSIONID, UPDATETYPE, EMAIL_TYPE, EMAIL_HOST_NAME, EMAIL_BRIDGE_PHONE, EMAIL_SYSTEM_URL,
//   COMMUNITYID, MEETINGID, HOSTID, TYPE, PRIVACY, PRIORITY, TITLE,
//   DESCRIPTION, SIZE, DURATION, PASSWORD, OPTIONS, START_TIME, END_TIME,
//   INVITE_EXPIRATION, INVITE_VALID_BEFORE, INVITE_MESSAGE, DISPLAY_ATTENDEES,
//   INVITE_CHAT_OPTIONS, PARTICIPANTS
// Returns:
//   status (ok, pending)
xmlrpc_value *update_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
	char *sessionid, *communityid, *meetingid, *hostid, *title, *descrip, *password, *invite_message;
    char *email_host_name, *email_bridge_phone, *email_system_url;
	int update_type,email_type,type, privacy, priority, meeting_size, duration, options;
    int start_time, end_time, invite_expiration, invite_valid_before, display_attendees, invitee_chat_options;
	xmlrpc_value *participants;
    ResultSet *rs = NULL;
    xmlrpc_value *output = NULL;
	int status = Ok;
    int meetingcount = 0;

    // Parse meeting info...
    xmlrpc_parse_value(env, param_array, "(siissssssiiissiisiiiiisiiA)",
        &sessionid, &update_type, &email_type,
        &email_host_name, &email_bridge_phone, &email_system_url,
        &communityid, &meetingid, &hostid, &type, &privacy,
        &priority, &title, &descrip, &meeting_size, &duration, &password,
        &options, &start_time, &end_time, &invite_expiration, &invite_valid_before,
        &invite_message, &display_attendees, &invitee_chat_options,
        &participants);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to schedule.update_meeting");
        goto cleanup;
    }

    check_session(env, user_data, sessionid);
    XMLRPC_FAIL_IF_FAULT(env);

    // If we were given a meeting id, see if the meeting record
    // already exists and user is host
    if (strlen(meetingid) > 0)
    {
        bool access = check_meeting_access(env, sessionid, meetingid, communityid);
        XMLRPC_FAIL_IF_FAULT(env);

        if (!access)
        {
            xmlrpc_env_set_fault(env, NotAuthorized, "User does not have access to modify meeting");
            goto cleanup;
        }

        meetingcount = 1;
    }

	schedule_meeting(env, update_type, meetingcount, email_type,
        			email_host_name, email_bridge_phone, email_system_url,
			        communityid, meetingid, hostid, type, privacy,
			        priority, title, descrip, meeting_size, duration, password,
			        options, start_time, end_time, invite_expiration, invite_valid_before,
			        invite_message, display_attendees, invitee_chat_options,
			        participants, NULL);
    if (env->fault_occurred)
	{
		goto cleanup;
	}

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, status, "Unable to update meeting");
    return output;
}

// Remove meeting.
// Parameters:
//   SESSIONID, MEETINGID
// Returns:
//   status (ok)
xmlrpc_value *remove_meeting(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *meetingid;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
	int status = Ok;
	int meetingcount;

	xmlrpc_parse_value(env, param_array, "(ss)", &sid, &meetingid);
    XMLRPC_FAIL_IF_FAULT(env);

    check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    // Check user is host
    rs = db->execute("find-meeting-host", meetingid);
    if (!rs || rs->is_error())
        goto cleanup;
    meetingcount = rs->get_num_rows();

    if (meetingcount == 0)
    {
        xmlrpc_env_set_fault(env, InvalidMeetingID, "Meeting not found");
        goto cleanup;
    }

    if (strcmp(rs->get_value(0, 0), sid) != 0)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "Only meeting host may delete meeting");
        goto cleanup;
    }
    db->closeResults(rs);

    rs = db->execute("remove-meeting", meetingid);
    if (!rs || rs->is_error())
        goto cleanup;

    log_reservation(EvnLog_Reserve_Delete,
                    meetingid, "", "", "", "", "", "", "");

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}


// Fetch all public meetings and generate proper URL for invite.
// Parameters:
//   SESSIONID, COMMUNITYID, USERID
// Returns:
//   MEETINGID, HOSTID, TYPE, PRIVACY, PRIORITY, TITLE, DESCRIPTION, SIZE, DURATION, TIME,
//   RECURRENCE, RECUR_END, OPTIONS, STATUS, START_TIME, END_TIME, BRIDGEPHONE, SYSTEMURL,
//   DISPLAY_ATTENDEES, INVITEE_CHAT_OPTIONS, INVITE_URL
xmlrpc_value *find_meetings_public_url(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *communityid, *userid;
    ResultSet *rs = NULL;
    xmlrpc_value *val = NULL;
    xmlrpc_value *array = NULL;
	int i, end;
	IString invite_url;
	bool is_service;

	xmlrpc_parse_value(env, param_array, "(sss)", &sid, &communityid, &userid);
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!is_service)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "API not valid for user sessions");
        XMLRPC_FAIL_IF_FAULT(env);
    }

    rs = db->execute("find-meetings-public", communityid, "0");
    if (!rs || rs->is_error())
        goto cleanup;

	array = xmlrpc_build_value(env, "()");
	XMLRPC_FAIL_IF_FAULT(env);

    end = rs->get_num_rows();
    for (i=0; i<end; i++)
    {
		const char *systemurl, *meetingid;

        xmlrpc_value *strings = xmlrpc_build_value(env, "()");
        XMLRPC_FAIL_IF_FAULT(env);

        for (int j=0; j < rs->get_num_fields(); j++)
        {
            const char *valstr = rs->get_value(i, j);
            if (valstr == NULL)
            {
                xmlrpc_env_set_fault(env, DBAccessError, "Error reading from database result set");
                goto cleanup;
            }
            xmlrpc_value *value = xmlrpc_build_value(env, "s", valstr);
            XMLRPC_FAIL_IF_FAULT(env);

            xmlrpc_array_append_item(env, strings, value);
            xmlrpc_DECREF(value);
            XMLRPC_FAIL_IF_FAULT(env);

			// Catch the values we need to build invitation URL
			switch (j)
			{
			case 0:
				meetingid = valstr;
				break;
			case 17:
				systemurl = valstr;
				break;
			}
        }

		// Add invite URL
		get_invite_url(invite_url, systemurl, meetingid, "");
        xmlrpc_value *value = xmlrpc_build_value(env, "s", (const char *)invite_url);
        XMLRPC_FAIL_IF_FAULT(env);

        xmlrpc_array_append_item(env, strings, value);
        xmlrpc_DECREF(value);
        XMLRPC_FAIL_IF_FAULT(env);

        xmlrpc_array_append_item(env, array, strings);
        xmlrpc_DECREF(strings);
        XMLRPC_FAIL_IF_FAULT(env);
    }

    // Create return parameter array.
    // ( row array, int start, int number, int more, int total )
    val = xmlrpc_build_value(env, "(Viiii)", array, 0, end, end, 0);
    XMLRPC_FAIL_IF_FAULT(env);

    xmlrpc_INCREF(val);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && val == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    if (val)
        xmlrpc_DECREF(val);
    if (array)
        xmlrpc_DECREF(array);

    return val;
}


// Fetch all meetings that the given user is invited to and generate URL for invite.
// Parameters:
//   SESSIONID, USERID
// Returns:
//    MEETINGID, HOSTID, TYPE, PRIVACY, PRIORITY, TITLE, DESCRIPTION, SIZE,
//    DURATION, TIME, RECURRENCE, RECUR_END, OPTIONS, STATUS, START_TIME,
//    END_TIME, PARTICIPANTID, ROLL, BRIDGEPHONE, SYSTEMURL, DISPLAY_ATTENDEES,
//    INVITEE_CHAT_OPTIONS, INVITE_URL
xmlrpc_value *find_meetings_invited_url(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *userid;
    ResultSet *rs = NULL;
    xmlrpc_value *val = NULL;
    xmlrpc_value *array = NULL;
	int i, end;
	IString invite_url;
    bool is_service;

	xmlrpc_parse_value(env, param_array, "(ss)", &sid, &userid);
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!is_service)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "API not valid for user sessions");
        XMLRPC_FAIL_IF_FAULT(env);
    }

    rs = db->execute("find-meetings-invited", userid, "0");
    if (!rs || rs->is_error())
        goto cleanup;

	array = xmlrpc_build_value(env, "()");
	XMLRPC_FAIL_IF_FAULT(env);

    end = rs->get_num_rows();
    for (i=0; i<end; i++)
    {
		const char *systemurl, *meetingid, *partid;

        xmlrpc_value *strings = xmlrpc_build_value(env, "()");
        XMLRPC_FAIL_IF_FAULT(env);

        for (int j=0; j < rs->get_num_fields(); j++)
        {
            const char *valstr = rs->get_value(i, j);
            if (valstr == NULL)
            {
                xmlrpc_env_set_fault(env, DBAccessError, "Error reading from database result set");
                goto cleanup;
            }
            xmlrpc_value *value = xmlrpc_build_value(env, "s", valstr);
            XMLRPC_FAIL_IF_FAULT(env);

            xmlrpc_array_append_item(env, strings, value);
            xmlrpc_DECREF(value);
            XMLRPC_FAIL_IF_FAULT(env);

			// Catch the values we need to build invitation URL
			switch (j)
			{
			case 0:
				meetingid = valstr;
				break;
			case 16:
				partid = valstr;
				break;
			case 19:
				systemurl = valstr;
				break;
			}
        }

		// Add invite URL
		get_invite_url(invite_url, systemurl, meetingid, partid);
        xmlrpc_value *value = xmlrpc_build_value(env, "s", (const char *)invite_url);
        XMLRPC_FAIL_IF_FAULT(env);

        xmlrpc_array_append_item(env, strings, value);
        xmlrpc_DECREF(value);
        XMLRPC_FAIL_IF_FAULT(env);

        xmlrpc_array_append_item(env, array, strings);
        xmlrpc_DECREF(strings);
        XMLRPC_FAIL_IF_FAULT(env);
    }

    // Create return parameter array.
    // ( row array, int start, int number, int more, int total )
    val = xmlrpc_build_value(env, "(Viiii)", array, 0, end, end, 0);
    XMLRPC_FAIL_IF_FAULT(env);

    xmlrpc_INCREF(val);

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && val == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    if (val)
        xmlrpc_DECREF(val);
    if (array)
        xmlrpc_DECREF(array);

    return val;
}

// Lookup invite
// Parameters:
//   SESSIONID, MID, PIN
//   If PIN is blank, gets meeting info only.  If MID is blank, meeting id
//   is assumed based on PIN.
// Returns:
//   FOUND, MEETINGID, TITLE, DESCRIPTION, STARTTIME, ENDTIME
//   PARTICIPANTID, PARTICIPANTTYPE, ROLE, USERID, USERNAME, NAME,
//   SELPHONE, PHONE, SELEMAIL, EMAIL, SCREENNAME, NOTIFYTYPE
//   INVITE_URL
//   FOUND: 1 = meeting found, 2 = meeting and user found, 0 = not found
xmlrpc_value *find_invite_url(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid, *mid, *pin;
    xmlrpc_value *output = NULL;
    ResultSet *rs = NULL;
    IString fieldstr;
    bool is_service;

	xmlrpc_parse_value(env, param_array, "(sss)", &sid, &mid, &pin);
    XMLRPC_FAIL_IF_FAULT(env);

    is_service = check_session(env, user_data, sid);
    XMLRPC_FAIL_IF_FAULT(env);

    if (!is_service)
    {
        xmlrpc_env_set_fault(env, NotAuthorized, "API not valid for user sessions");
        XMLRPC_FAIL_IF_FAULT(env);
    }

	if (*pin == '\0')
	{
		// PIN is blank, look up meeting info
		rs = db->execute("find-meeting-details", mid);
		if (!rs || rs->is_error())
			goto cleanup;

		if (rs->get_num_rows() > 0)
		{
			// We got meeting id, but we also want to look up meeting and return option flags.
			const char * meetingid = rs->get_value(0, "MEETINGID");

			// Figure out URL for joining this meeting.
		    const char * systemurl = rs->get_value(0, "SYSTEMURL");
		    IString invite_url;

			get_invite_url(invite_url, systemurl, meetingid, "");

			//   FOUND, MEETINGID, TITLE, DESCRIPTION, STARTTIME, ENDTIME
			//   PARTICIPANTID, PARTICIPANTTYPE, ROLE, USERID, USERNAME, NAME,
			//   SELPHONE, PHONE, SELEMAIL, EMAIL, SCREENNAME, NOTIFYTYPE
			//   INVITE_URL
			output = xmlrpc_build_value(env, "(isssiisiisssisissis)", 1,
							meetingid,
							rs->get_value(0, "TITLE"),
							rs->get_value(0, "DESCRIPTION"),
							atoi(rs->get_value(0, "TIME")),
							atoi(rs->get_value(0, "END_TIME")),
							"",
							0,
							0,
							"",
							"",
                            ANONYMOUS_NAME,
                            0,
                            "",
                            0,
                            "",
							"",
							0,
							(const char *)invite_url);
			XMLRPC_FAIL_IF_FAULT(env);
		}
    }
	else
	{
		// PIN was specified, look up.
		rs = db->execute("find-invite-details", pin);
		if (!rs || rs->is_error())
			goto cleanup;

		if (rs->get_num_rows() > 0)
		{
			// We got meeting id, but we also want to look up meeting and return option flags.
			const char * meetingid = rs->get_value(0, "MEETINGID");

			// Figure out URL for joining this meeting.
		    const char * systemurl = rs->get_value(0, "SYSTEMURL");
		    const char * partid = rs->get_value(0, "PARTICIPANTID");
		    IString invite_url;

			get_invite_url(invite_url, systemurl, meetingid, partid);

            // We used to retrieve screen name but if participant is invited via
            // ad hoc path screenname is used to store their IM screenname
            // Now we retrieve username and parse IIC screen name from it.
		    IString screenname = rs->get_value(0, "USERNAME");
			int pos = screenname.find("@");
		    if (pos > 0)
			    screenname.erase(pos, screenname.length() - pos);
            if (screenname.length() == 0)
                screenname = ANONYMOUS_NAME;

			//   FOUND, MEETINGID, TITLE, DESCRIPTION, STARTTIME, ENDTIME
			//   PARTICIPANTID, PARTICIPANTTYPE, ROLE, USERID, USERNAME, NAME,
			//   SELPHONE, PHONE, SELEMAIL, EMAIL, SCREENNAME, NOTIFYTYPE
			//   INVITE_URL
			output = xmlrpc_build_value(env, "(isssiisiisssisissis)", 2,
	                        meetingid,
	                        rs->get_value(0, "TITLE"),
	                        rs->get_value(0, "DESCRIPTION"),
	                        atoi(rs->get_value(0, "TIME")),
	                        atoi(rs->get_value(0, "END_TIME")),
	                        partid,
	                        atoi(rs->get_value(0, "TYPE")),
	                        atoi(rs->get_value(0, "ROLL")),
	                        rs->get_value(0, "USERID"),
	                        rs->get_value(0, "USERNAME"),
							rs->get_value(0, "NAME"),
	                        atoi(rs->get_value(0, "SELPHONE")),
							rs->get_value(0, "PHONE"),
	                        atoi(rs->get_value(0, "SELEMAIL")),
							rs->get_value(0, "EMAIL"),
	                        (const char *)screenname,
	                        atoi(rs->get_value(0, "NOTIFYTYPE")),
	                        (const char *)invite_url);
			XMLRPC_FAIL_IF_FAULT(env);
		}
	}

	if (output == NULL)
	{
		// Nothing found--bad ids, or invite has expired.
		output = xmlrpc_build_value(env, "(isssiisiisssisissis)", 0,
					"", "", "", 0, 0, "", 0, 0,"", "", "", 0, "", 0, "", "", 0, "");
		XMLRPC_FAIL_IF_FAULT(env);
	}

cleanup:
    if (rs != NULL)
    {
        if (rs->is_error())
        {
            xmlrpc_env_set_fault(env, DBAccessError, (char*)rs->get_error());
        }
        db->closeResults(rs);
    }
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Internal error");

    return output;
}

