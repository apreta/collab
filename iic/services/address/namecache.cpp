/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include <stdio.h>
#include <stdlib.h>

#include "namecache.h"
#include "../common/common.h"


NameCacheList::NameCacheList(const char* userid)
{
	m_userid = userid;
}

void NameCacheList::add(const char* name)
{
    m_list.add(new IString(name));
}

bool NameCacheList::contains(const char* name)
{
    IString val(name);
    return (m_list.find(&val) != NULL);
}

NameCache::NameCache()
{
    m_hash = new IStringHash();
}

NameCache::~NameCache()
{
    delete m_hash;
}

void NameCache::add(const char* userid, const char* name)
{
    NameCacheList* list = (NameCacheList*) m_hash->get(userid);

    if (list == NULL)
    {
        list = new NameCacheList(userid);
        m_hash->insert(userid, list);
    }

    list->add(name);
}

void NameCache::clear(const char* userid)
{
    m_hash->remove(userid, true);
}

bool NameCache::contains(const char* userid, const char* name)
{
    bool result = false;

    NameCacheList* list = (NameCacheList*) m_hash->get(userid);

    if (list != NULL)
    {
        result = list->contains(name);
    }

    return result;
}
