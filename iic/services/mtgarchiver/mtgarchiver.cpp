/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

 /* Notes:
    Data session start timmes are stored in spoolfiles.txt.  vnc2swf/vnc2mov uses these times to pull the
    recording data from the reflector.

    The voice recording is expected to start at the time sent to set_voice_recording_delay and continue
    until the end of the meeting.  (There is some code to support multiple voice recordings per meeting
    but it is not complete.)

 */

#include "mtgarchiver.h"

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>

#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>

#include <netdb.h>

#include <errno.h>
#include <sys/poll.h>

#include <libintl.h>
#define _(String) gettext(String)

#include "../../jcomponent/util/util.h"
#include "../../jcomponent/util/log.h"
#include "../../jcomponent/rpc.h"
#include "../../util/base64/Base64.h"
#include "../../util/filetransfer/transfer.h"
#include "../common/common.h"
#include "sync.h"

#define USE_VIDEO 1
#define CONVERTER "./vnc2mov"

#define FILEOFFSET long
#define WV_DWORD unsigned int

const char *
MtgArchive::s_arc_state_str[MtgArchive::NUM_ARC_STATE] = {
    "active", "converting", "done"
};
void getRawText(const char * message, char * out);

int MtgArchiver::s_share_lite = 0;
int MtgArchiver::s_sample_size = 1;
int MtgArchiver::s_audio_format = 7;

IString MtgArchiver::s_mtg_repository_url;
IString MtgArchiver::s_docshare_repository_url;
IString MtgArchiver::s_spooling_root;
unsigned short MtgArchiver::s_vbridge_xfer_port = 5122;
DES_key_schedule MtgArchiver::s_keysched;

IString MtgArchiver::s_email_from;
IString MtgArchiver::s_email_from_display;
IString MtgArchiver::s_email_subject;

IString  MtgArchiver::s_email_template;
time_t   MtgArchiver::s_email_template_modified = 0;
gchar ** MtgArchiver::s_parsed_email_template = NULL;

IString  MtgArchiver::s_summary_template;
time_t   MtgArchiver::s_summary_template_modified = 0;
gchar ** MtgArchiver::s_parsed_summary_template = NULL;

IString MtgArchiver::s_mtg_repository_portal;

IString MtgArchiver::s_dummy_mp3_file;
IString MtgArchiver::s_swf_html_template;   // ShowMovie.html
IString MtgArchiver::s_video_html;     // video.html
int     MtgArchiver::s_frames_per_movie = 900;
int     MtgArchiver::s_frame_rate = 1;
int     MtgArchiver::s_video_bit_rate = 800000;
IString MtgArchiver::s_video_format = "h264";
IString MtgArchiver::s_file_type = "mp4";
IString MtgArchiver::s_post_command;
int     MtgArchiver::s_vnc2swf_log_level = 4;
int     MtgArchiver::s_batch_threads = 5;
int     MtgArchiver::s_keep_raw_data = 0;

int     MtgArchiver::s_has_voice = 1;

IString MtgArchiver::s_sys_admin_email;

IThreadPool * MtgArchiver::m_tpool;
IThreadPool * MtgArchiver::m_tpool_preview;

int MtgArchiver::check_exists(const char *dir)
{
    struct stat buf;
    if (stat(dir, &buf) < 0)
    {
        if (errno == ENOENT)
        {
            log_error(ZONE, "%s doesn't exist", dir);

            return -1;
        }
        else
        {
            log_error(ZONE, "Unable to get the status of %s: %s",
                      dir, strerror(errno));
            return -1;
        }
    }

    return 0;
}

int MtgArchiver::restore_archives(int state, const char * path)
{
    DIR * d;
    d = opendir(path);
    if (d == NULL)
    {
        log_error(ZONE, "Couldn't open directory %s: %s",
                  (const char *)path, strerror(errno));
        return -1;
    }

    struct dirent * ent = NULL;
    for ( ent = readdir(d); ent != NULL; ent = readdir(d) )
    {
        if (strcmp(ent->d_name, ".") == 0)
            continue;

        if (strcmp(ent->d_name, "..") == 0)
            continue;

        MtgArchive * arc = new MtgArchive(state, path, ent->d_name);
        if (state == MtgArchive::ARC_STATE_ACTIVE)
        {
            log_debug(ZONE, "Restore meeting %s from active queue", (const char *)arc->get_mtg_id());

            m_active_mtg.insert(arc->get_mtg_id(), arc);
            m_restored_active.add(arc);
            arc->dereference();
        }
        else if (state == MtgArchive::ARC_STATE_CONVERTING)
        {
		    update_email_template();
		    update_summary_template();
			arc->prev_state();	// if it is left converting, we should perform converting again, otherwise it will be simply cleaned
            log_debug(ZONE, "Push meeting %s to the thread pool", (const char *)arc->get_mtg_id());
            m_tpool->push((void *)arc);
        }
	else
	{
	   log_error(ZONE, "Tried to restore an archive in an unhandled state!");
	   arc->dereference();
	}
    }

    closedir(d);

    return 0;
}

// Check if spool directories are there
int MtgArchiver::check_spooling()
{
    IString dir;

    for (int ii = MtgArchive::ARC_STATE_ACTIVE; ii < MtgArchive::ARC_STATE_DONE; ii++)
    {
        dir = s_spooling_root;
        dir.append("/");
        dir.append(MtgArchive::s_arc_state_str[ii]);

        if (check_exists(dir) < 0)
        {
            log_error(ZONE, "%s must exist before spooling is possible",
                      (const char *)dir);
            return -1;
        }
        else
        {
            if (restore_archives(ii, dir) < 0)
                return -1;
        }
    }

    m_initialized = true;

    return 0;
}

void
MtgArchiver::service_registered()
{
    log_debug(ZONE, "There are  %d meetings in the active queue", m_restored_active.size());

    // Called after meeting controller confirms that this service has been registered.
    // As part of registering this service the controller will also re-register any active meetings,
    // so any meetings left in this list are no longer running.
    while (m_restored_active.size() > 0)
    {
        MtgArchive * arc = (MtgArchive *)m_restored_active.remove_head();

        log_debug(ZONE, "Meeting %s no longer running, stopping recording",arc->get_mtg_id());

        stop_recording(arc->get_mtg_id(), _("Not available"), FALSE, FALSE);
        arc->dereference();
    }
}

void
MtgArchiver::queue_archive_func(IObject * key, IObject * value, void * user)
{
    IThreadPool * tp = (IThreadPool *)user;
    MtgArchive * arc = (MtgArchive *)value;

    if (arc == NULL)
    {
        log_error(ZONE, "Null active meeting seen!");
        return;
    }

    log_debug(ZONE, "Pushing mtg %s onto thread queue",
              (const char *)arc->get_mtg_id());

    tp->push((void *)arc);
    arc->reference(); // thread pool doesn't know about ref counts
}

int
MtgArchiver::reset()
{
    IUseMutex lock(m_lock);

    log_debug(ZONE,
              "MtgArchiver reset; processing %d active mtgs",
              m_active_mtg.size());

    // put all active meetings on the work queue
    m_active_mtg.for_each(queue_archive_func, (void *)m_tpool);
    m_active_mtg.remove_all();

    return 0;
}

void
MtgArchiver::set_archive_name_key(const char * keystr)
{
    char key[16];
    CBase64 b64;

    if (strlen(keystr) > 16)
    {
        log_error(ZONE, "archive name key too long!");
        return;
    }

    b64.Decode(keystr, (LPTSTR)&key);
    if (DES_set_key((DES_cblock *)key, &s_keysched) < 0)
    {
        log_error(ZONE, "DES_set_key() returns error; bogus key?");
    }
}

void
MtgArchiver::encrypt(IString * b64ciphertxt, const char * cleartxt)
{
    unsigned int clrlen = strlen(cleartxt)+1; // include null char for sanity
    unsigned int nblock = clrlen/8;
    if (clrlen % 8) ++nblock;	// account for partial block

    DES_cblock *ciptxt = (DES_cblock *)calloc(nblock, sizeof(DES_cblock) /* 8 */);
    DES_cblock *clrtxt = (DES_cblock *)calloc(nblock, sizeof(DES_cblock) /* 8 */);
    DES_cblock ivec = { 0 };

    strcpy((char *)clrtxt, cleartxt);
    unsigned int ii;

    DES_pcbc_encrypt((unsigned char *)clrtxt,
                     (unsigned char *)ciptxt,
                     nblock*sizeof(DES_cblock),
                     &s_keysched, &ivec, 1 /* encrypt */);

    CBase64 b64;
    char * b64out = b64.Encode((const char *)ciptxt, nblock*sizeof(DES_cblock));
    for (ii = 0; ii < strlen(b64out); ii++)
    {
        if (b64out[ii] == '/')
            b64out[ii] = '_';
    }

    *b64ciphertxt = b64out;

    delete b64out;
    free((void *)clrtxt);
    free((void *)ciptxt);
}

int
MtgArchiver::decrypt(IString * cleartxt, const char * b64str)
{
    // Decode the b64 ciphertxt
    char * bin = (char *)malloc(strlen(b64str)+1);
    char * in  = (char *)malloc(strlen(b64str)+1);
    CBase64 b64;
    unsigned int ii;

    strcpy(in, b64str);
    for (ii = 0; ii < strlen(b64str); ii++)
    {
        if (b64str[ii] == '_')
            in[ii] = '/';
        else
            in[ii] = b64str[ii];
    }

    int len = b64.Decode(in, bin);
    assert((len % 8) == 0);
    unsigned int nblock = len/8;

    // Decrypt
    DES_cblock *ciptxt = (DES_cblock *)bin;
    DES_cblock *clrtxt = (DES_cblock *)calloc(nblock, sizeof(DES_cblock));
    DES_cblock ivec = { 0 };

    DES_pcbc_encrypt((unsigned char *)ciptxt,
                     (unsigned char *)clrtxt,
                     nblock*sizeof(DES_cblock),
                     &s_keysched, &ivec, 0 /* decrypt */);

    // Is there a null char somewhere in the output?
    for (ii = 0; ii < nblock*8 && (((const char *)clrtxt)[ii] != '\0'); ii++);
    if (ii == (nblock*8))
    {
        log_error(ZONE, "No null found in unencrypted text; garbage input?");

        free((void *)clrtxt);
        free((void *)bin);
        free((void *)in);

        return -1;
    }

    *cleartxt = (const char *)clrtxt;

    free((void *)clrtxt);
    free((void *)bin);
    free((void *)in);

    return 0;
}

int MtgArchiver::register_meeting(const char *meeting_id,
                                  int start_time,
                                  const char *host_email,
                                  const char *voice_service,
                                  const char *reflector_host,
                                  const char *password,
                                  int options,
                                  const char * key)
{
	IUseMutex lock(m_lock);

    const char * vbridge_host = NULL;
    if (voice_service && strlen(voice_service) > 0)
    {
        int ii;
        for (ii = 0; ii < m_num_vbridge; ii++)
        {
            if (strcmp(voice_service, m_vbridge_service[ii]) == 0)
            {
                vbridge_host = m_vbridge_host[ii];
                log_debug(ZONE, "Setting vbridge host to %s",
                          (const char *)m_vbridge_host[ii]);
                break;
            }
            if (ii == m_num_vbridge)
            {
                log_error(ZONE, "Couldn't find host for voice service %s!",
                          voice_service);
            }
        }
    }
    else
    {
        log_debug(ZONE, "No voice service set for this meeting");
    }

    MtgArchive * arc = (MtgArchive *)m_active_mtg.get(meeting_id);
    if (arc == NULL)
    {
        arc = new MtgArchive(meeting_id, start_time, reflector_host, password);
        arc->perform_op(); // creates folder

        // find the voice bridge host for this meeting

        if (vbridge_host != NULL)
        {
            if (arc->set_vbridge_host(vbridge_host) < 0)
            {
                arc->dereference();
                return -1;
            }
        }

        if (host_email && strlen(host_email) > 0)
        {
            if (arc->set_host_email(host_email) < 0)
            {
                arc->dereference();
                return -1;
            }
        }
        m_active_mtg.insert(meeting_id, arc);
        arc->dereference();
    }
    else
    {
        if (m_restored_active.size() > 0)
        {
            // mtg controller told us about it, so remove
            // it from the list of restored active archives.
            // After service registration, any items on the
            // list are converted.
            IListIterator ires(m_restored_active);
            MtgArchive *rarc = (MtgArchive *)ires.get_first();
            while (rarc)
            {
                if (rarc->equals((IObject *)arc))
                {
                    log_debug(ZONE, "Archive still in progress %s", rarc->get_folder());

                    m_restored_active.remove(ires);

                    break;
                }
                rarc = (MtgArchive *)ires.get_next();
            }
        }

        // mtg already active.
        if (vbridge_host != NULL)
        {
            if (arc->set_vbridge_host(vbridge_host) < 0)
            {
                return -1;
            }
        }

        if (host_email && strlen(host_email) > 0)
        {
            if (arc->set_host_email(host_email) < 0)
            {
                return -1;
            }
        }

        // reset the reflector address, it may be changed (although if it has, some of the recording
        // will be missing)
        arc->set_reflector(reflector_host, password);
    }

	arc->set_appshare_key(key);

    arc->start_meeting();

    return 0;
}

// voice will reset its recording
// here we can simply dellete the spool files, and
int MtgArchiver::reset_recording(const char *meeting_id)
{
	IUseMutex lock(m_lock);

    log_debug(ZONE, "reset_recording(%s)", meeting_id);

    MtgArchive * arc = (MtgArchive *)m_active_mtg.get(meeting_id);
    if (arc == NULL)
    {
        log_error(ZONE, "No archive available for mtg %s; creation error?",
                  meeting_id);
        return -1;
    }

    int ret = arc->reset_recording();

    return ret;
}

int MtgArchiver::preview_recording(const char *meeting_id, const char* email)
{
	IUseMutex lock(m_lock);

    log_debug(ZONE, "preview_recording(%s)", meeting_id);

    MtgArchive * arc = (MtgArchive *)m_active_mtg.get(meeting_id);
    if (arc == NULL)
    {
        log_error(ZONE, "No archive available for mtg %s; creation error?",
                  meeting_id);
        return -1;
    }

	arc->set_host_email(email);

    arc->reference(); // keep obj so it can be pushed on the thread pool stack
    update_email_template();
    update_summary_template();

    m_tpool_preview->push((void *)arc);  // doesn't inc ref count

    return 0;
}

int MtgArchiver::set_voice_recording_delay(const char *meeting_id, int delay)
{
	IUseMutex lock(m_lock);

    log_debug(ZONE, "set_voice_recording_delay(%s, %u)", meeting_id, delay);

    MtgArchive * arc = (MtgArchive *)m_active_mtg.get(meeting_id);
    if (arc == NULL)
    {
        log_error(ZONE, "No archive available for mtg %s; creation error?",
                  meeting_id);
        return -1;
    }

    int ret = arc->set_voice_recording_delay(delay);

    return ret;
}

int MtgArchiver::set_recording_audio(const char *meeting_id, const char *url, const char *format)
{
	IUseMutex lock(m_lock);

    log_debug(ZONE, "set_recording_audio(%s, %u)", meeting_id, url);

    MtgArchive * arc = (MtgArchive *)m_active_mtg.get(meeting_id);
    if (arc == NULL)
    {
        log_error(ZONE, "No archive available for mtg %s; creation error?",
                  meeting_id);
        return -1;
    }

    int ret = arc->set_recording_audio(url, format);

	return ret;
}

int MtgArchiver::enable_recording(const char *meeting_id, int enabled)
{
	IUseMutex lock(m_lock);

    log_debug(ZONE, "enable_recording(%s, %d)", meeting_id, enabled);

    MtgArchive * arc = (MtgArchive *)m_active_mtg.get(meeting_id);
    if (arc == NULL)
    {
        log_error(ZONE, "No archive available for mtg %s; creation error?",
                  meeting_id);
        return -1;
    }

    arc->enable(enabled);

    return 0;
}

// Pause recording (data share was stopped)
int MtgArchiver::pause_recording(const char *meeting_id)
{
	IUseMutex lock(m_lock);

    log_debug(ZONE, "pause_recording(%s)", meeting_id);

    MtgArchive * arc = (MtgArchive *)m_active_mtg.get(meeting_id);
    if (arc == NULL)
    {
        log_error(ZONE, "No archive available for mtg %s; creation error?",
                  meeting_id);
        return -1;
    }

    arc->pause_recording();

    return 0;
}

// Start data share
int MtgArchiver::start_recording(const char *meeting_id, int start_time)
{
	IUseMutex lock(m_lock);

    log_debug(ZONE, "start_data_recording(%s)", meeting_id);

    MtgArchive * arc = (MtgArchive *)m_active_mtg.get(meeting_id);
    if (arc == NULL)
    {
        log_error(ZONE, "No archive available for mtg %s; creation error?",
                  meeting_id);
        return -1;
    }

    arc->start_recording(start_time);

    return 0;
}

// Called for each meeting chat message
int MtgArchiver::on_chat(const char *meeting_id, const char *message, const char *from, const char *to)
{
	IUseMutex lock(m_lock);

    log_debug(ZONE, "on_chat(%s,%s,%s,%s)", meeting_id, message, from, to);

    MtgArchive * arc = (MtgArchive *)m_active_mtg.get(meeting_id);
    if (arc == NULL)
    {
        log_error(ZONE, "No archive available for mtg %s; creation error?",
                  meeting_id);
        return -1;
    }

    arc->log_chat_message(message, from, to);

    return 0;
}

// Called when the meeting ends
int MtgArchiver::stop_recording(const char *meeting_id, const char *participants, int email_summary, int include_docs)
{
	IUseMutex lock(m_lock);

    log_debug(ZONE, "stop_recording(%s)", meeting_id);

    MtgArchive * arc = (MtgArchive *)m_active_mtg.get(meeting_id);
    if (arc == NULL)
    {
        log_error(ZONE, "No archive available for mtg %s; creation error?",
                  meeting_id);
        return -1;
    }

    arc->reference(); // keep obj so it can be pushed on the thread pool stack
    m_active_mtg.remove(meeting_id);
    update_email_template();
    update_summary_template();

    arc->stop_recording(participants, email_summary, include_docs);

    m_tpool->push((void *)arc);  // doesn't inc ref count

    return 0;
}

int MtgArchiver::s_max_retries = 3;

void
MtgArchiver::process_archive(void * instance_data, void * init_data)
{
	MtgArchive * arc = (MtgArchive *)instance_data;

	while (arc->next_state())
	{
		if (arc->perform_op() < 0)
		{
			if (arc->add_retry_count()> s_max_retries)
			{
				arc->dereference();
			}
			else
			{
				arc->prev_state();	// put it back to active
				m_tpool->push((void *)arc);  // doesn't inc ref count
			}

			return;
		}
	}
	arc->dereference();
}

void
MtgArchiver::preview_archive(void * instance_data, void * init_data)
{
    MtgArchive * arc = (MtgArchive *)instance_data;

    if (arc->next_state())	// converting
    {
        arc->perform_op(true);

		arc->prev_state();	// put it back to active
    }
}

int MtgArchive::s_retry_wait = 30;	// default retry wait in seconds

int MtgArchive::s_socket_timeout = 60;	// default socket timeout in seconds

MtgArchive::MtgArchive(const char * meeting_id,
                       time_t start_time,
                       const char* reflector, const char * password)
{
    m_state = ARC_STATE_ACTIVE;
    m_mtg_id = meeting_id;
    m_mtg_start_time = start_time;
    m_reflector = reflector;
    m_passwd = password;
    m_recordingStarted = false;
    m_include_docs = false;

    // for video recording
    m_started = -1;
    m_enabled = 0;
    m_recording_thread = NULL;
    m_status = RS_NONE;
//    perform_op();	// m_retries not set at this point, and we call perform_op after new anyway

    m_enable_offset = 0;
	m_local_enable_time = 0;
    m_local_vrstart_time = 0;
    m_local_drstart_time = 0;
    m_local_start_time = time(NULL);

	m_retries = 0;
	m_nTotalWaveSize = 0;
    m_upload_summary = false;

    m_nAdditionalStartTime = 0;
    memset(m_AdditionalStartTime, 0, sizeof(m_AdditionalStartTime));

}

#define FCOMP_MTG_ID  0
#define FCOMP_YEAR    1
#define FCOMP_MON     2
#define FCOMP_MDAY    3
#define FCOMP_HOUR    4
#define FCOMP_MIN     5
#define FCOMP_SEC     6
#define FCOMP_LEN     7

MtgArchive::MtgArchive(int initial_state,
                       const char *full_path,
                       const char *folder)
{
    int ii;

    m_state = initial_state;

    m_full_path = full_path;
    m_full_path.append("/");
    m_full_path.append(folder);

    m_folder = folder;

    gchar ** attr = g_strsplit(folder, "-", -1);
    for (ii = 0; attr[ii]; ii++);
    if (ii != FCOMP_LEN)
    {
        log_error(ZONE, "Unable to set attributes due to mangled folder name %s",
                  folder);
        g_strfreev(attr);
        return;
    }

    m_mtg_id = attr[FCOMP_MTG_ID];

    log_debug(ZONE, "restoring %s %s from spooling dir",
              s_arc_state_str[m_state], (const char *)m_folder);

    struct tm st;
    st.tm_year = atoi(attr[FCOMP_YEAR]) - 1900;
    st.tm_mon  = atoi(attr[FCOMP_MON]) - 1;
    st.tm_mday = atoi(attr[FCOMP_MDAY]);
    st.tm_hour = atoi(attr[FCOMP_HOUR]);
    st.tm_min  = atoi(attr[FCOMP_MIN]);
    st.tm_sec  = atoi(attr[FCOMP_SEC]);

    g_strfreev(attr);

    m_mtg_start_time = mktime(&st);

    m_enable_offset = 0;

    m_nAdditionalStartTime = 0;
    memset(m_AdditionalStartTime, 0, sizeof(m_AdditionalStartTime));

    restore("vbridge_host", &m_vbridge_host);
	restore("share_server",&m_reflector);
	restore("share_passwd",&m_passwd);
    restore("host_email", &m_host_email);
    IString val;
    restore("mtgstart", &val);
    if (val.length() > 0)
    {
        m_local_start_time = atoi(val);
    }
    else
    {
        m_local_start_time = time(NULL);
    }

    restore("enableStart", &val);
    if (val.length() > 0)
    {
        m_local_enable_time = atoi(val);
    }
    else
    {
        m_local_enable_time = 0;
    }

    restore("vrstart", &val);
    if (val.length() > 0)
    {
        m_local_vrstart_time = atoi(val);
    }
    else
    {
        m_local_vrstart_time = 0;
    }
    restore("drstart", &val);
    if (val.length() > 0)
    {
        m_local_drstart_time = atoi(val);
    }
    else
    {
        m_local_drstart_time = 0;
    }

    restore_array("mtg_start_time", m_AdditionalStartTime, &m_nAdditionalStartTime, MAX_FAILURE);

    // for video recording
    m_started = -1;
    m_recording_thread = NULL;
    m_enabled = 0;
    m_status = RS_NONE;

    restore("recordingStarted", &val);
    if (val.length() > 0)
    {
        m_recordingStarted = true;
        m_enabled = 1;	// if client restarts, this flag should be false, but this is called when mtgarchiver restarts
    }
    else
    {
        m_recordingStarted = false;
    }

    restore("recordingOffset", &val);
    if (val.length() > 0)
    {
        m_enable_offset = atoi(val);
    }
    else
    {
        m_enable_offset = 0;
    }

    restore("recordingState", &val);
    if (val.length() > 0)
    {
        m_status = atoi(val);
    }
    else
    {
        m_status = 0;
    }

    restore("includeDocs", &val);
    if (val.length() > 0)
    {
        m_include_docs = atoi(val);
    }
    else
    {
        m_include_docs = 0;
    }

	restore("recordurl", &m_recording_url);
	restore("recordformat", &m_recording_format);

	restore("appshare_key", &m_appshare_key);

	m_retries = 0;
	m_nTotalWaveSize = 0;
    m_upload_summary = false;
}

MtgArchive::~MtgArchive()
{
    log_debug(ZONE, "destroying MtgArchive(%s)",
              (const char *)m_folder);

    if (m_recording_thread)
    {
      delete m_recording_thread;
    }
}

int MtgArchive::perform_op(bool preview)
{
	int ret = 0;

	if (m_retries!=0)
	{
		sleep(s_retry_wait);	// wait a bit
	}

	lock();
    switch (m_state)
    {
        case ARC_STATE_ACTIVE:
        {
            ret = create_archive_folder();

            char temp[21];
            sprintf(temp, "%ld", m_local_start_time);
            save("mtgstart", temp);
            break;
        }

        case ARC_STATE_CONVERTING:
			if (!m_recordingStarted)	// recording was not enabled
			{
                if (m_upload_summary)   // if it is a scheduled meeting, upload it anyway
                {
                    ret = upload_archive();	// upload the summary if it is there
                }
				log_error(ZONE, "Recording was not enabled, no convertion needed.");
				break;
			}

			ret = fetch_mtg_recording();
			if (ret < 0 && MtgArchiver::s_has_voice!=0)
			{
				log_error(ZONE, "Failed to retrieve voice recording, try it again later.");

				break;  // retry it later
			}

			ret = convert(-1);	// make a big mp3 file for user
			if (ret < 0)
			{
				log_error(ZONE, "Failed to convert wave file to mp3, try it again later.");

				break;  // retry it later
			}

			if (m_local_drstart_time!=0)	// there are data recording too
			{
#ifndef USE_VIDEO
				ret = splitAndConvert();	// make small mp3 files for movies
				if (ret < 0)
				{
					log_error(ZONE, "Failed to split & convert wave file to mp3, try it again later.");

					break;  // retry it later
				}
#endif

				// stop the vnc2swf in any case:
				ret = generate_movie(preview);
				if (ret < 0)
					break;

				generate_html();
			}

			ret = upload_archive();
			if (ret < 0)
				break;
			if (!MtgArchiver::is_share_lite())
			{
				ret = send_email(preview);
			}
			else
			{
				// >>> share-lite: send message <<<
			}
			break;

        case ARC_STATE_DONE:
            cleanup();
            break;

        default:
            log_error(ZONE, "Unaccounted for state %d in perform_op()",
                      m_state);
            break;
    }

    unlock();
    return ret;
}

bool MtgArchive::next_state()
{
    if (m_state == ARC_STATE_DONE)
        return false;
    else
    {
        if (move(m_state+1) < 0)
            return false;

        return true;
    }
}

bool MtgArchive::prev_state()
{
    if (m_state == ARC_STATE_ACTIVE)
        return false;
    else
    {
        if (move(m_state-1) < 0)
            return false;

        return true;
    }
}

int MtgArchive::make_dir(const char * dir, int mode)
{
    struct stat s;

    if (stat(dir, &s) < 0)
    {
        if (errno == ENOENT)
        {
            if (mkdir(dir, mode) < 0)
            {
                log_error(ZONE, "Unable to make the directory %s: %s",
                          dir, strerror(errno));
                return -1;
            }
        }
        else
        {
            log_error(ZONE, "Unable to determine the existence of %s",
                      strerror(errno));
        }
    }

    return 0;
}


int MtgArchive::create_archive_folder()
{
    char startstr[21];
    struct tm tm_start;
    localtime_r(&m_mtg_start_time, &tm_start);
    strftime(startstr, 21, "-%Y-%m-%d-%H-%M-%S", (const struct tm *)&tm_start);

    m_folder = (const char *)m_mtg_id;
    m_folder.append(startstr);

    m_full_path = MtgArchiver::s_spooling_root;
    m_full_path.append("/");
    m_full_path.append(s_arc_state_str[m_state]);
    m_full_path.append("/");
    m_full_path.append(m_folder);

    if (make_dir(m_full_path, 0700 /* don't let others read it */) < 0)
        return -1;

    return 0;
}

int MtgArchive::move(int next_state)
{
    assert(next_state < NUM_ARC_STATE);

    log_debug(ZONE, "archive %s moving from %s to %s",
              (const char *)m_folder,
              s_arc_state_str[m_state], s_arc_state_str[next_state]);

    IString next_path = (const char *)MtgArchiver::s_spooling_root;
    next_path.append("/");
    next_path.append(s_arc_state_str[next_state]);
    next_path.append("/");
    next_path.append(m_folder);

    if (rename(m_full_path, next_path) < 0)
    {
        log_error(ZONE, "Couldn't move %s to %s: %s\n",
                  (const char *)m_full_path, (const char *)next_path, strerror(errno));
        return -1;
    }

    m_state = next_state;
    m_full_path = next_path;

    return 0;
}

typedef struct WAVEHDR{
    char            format[4];  // RIFF
    WV_DWORD    	f_len;      // filelength
    char            wave_fmt[8];// WAVEfmt_
    WV_DWORD    	fmt_len;    // format lenght
    unsigned short  fmt_tag;    // format Tag
    unsigned short  channel;    // Mono/Stereo
    WV_DWORD    	samples_per_sec;
    WV_DWORD    	avg_bytes_per_sec;
    unsigned short  blk_align;
    unsigned short  bits_per_sample;
    WV_DWORD   		unknown;
    char            fact[4];
    WV_DWORD    	fact_len;
    WV_DWORD    	fact_size;
    char            data[4];    // data
    WV_DWORD    	data_len;   // data size
} wavehdr;


// Create wave file for output and write header.
// Also adds silence at start if needed to align audio with data recording.
FILE *
MtgArchive::create_wav_file(size_t data_sz)
{
    IString wavfn;
    wavfn = m_full_path;
    wavfn.append("/audio.wav");

    wavehdr hdr =
        {
            {'R','I','F','F'},
            0,
            {'W','A','V','E','f','m','t',' '},
            0x14,
            MtgArchiver::s_audio_format,
            1,
            0x1F40,
            0x1F40 * MtgArchiver::s_sample_size,
            1 * MtgArchiver::s_sample_size,
            8 * MtgArchiver::s_sample_size,
            0,
            {'f','a','c','t'},
            4,
            0,
            {'d','a','t','a'},
            0
        };

    log_debug(ZONE, "archive %s creating wav file %s",
              (const char *)m_folder, (const char *)wavfn);

    FILE *wavfp = fopen((const char *)wavfn, "w+b");
    if (wavfp == NULL)
    {
        log_error(ZONE, "Couldn't open %s: %s",
                  (const char *)wavfn, strerror(errno));
        return NULL;
    }

    fwrite(&hdr, 1, sizeof(hdr), wavfp);
    void* p = malloc(sample_count());
	int vrdelay = get_delay_time(true);
    log_debug(ZONE, "Add voice delay: %d secs", vrdelay);
    memset(p, 0, sample_count());
    for (int i=0; i<vrdelay; i++)
    {
        fwrite(p, 1, sample_count(), wavfp);
    }
    free(p);

    // Compute the new wave file header fields
    int offset = sample_count() * vrdelay + data_sz - 8 /* sizeof("RIFF") + sizeof(len) */;
    fseek(wavfp, 4, SEEK_SET);
    fwrite(&offset, 1, 4, wavfp);
    offset -= sizeof(wavehdr);
    offset += 8;
    fseek(wavfp, ((char*)&hdr.fact_size-(char*)&hdr), SEEK_SET);
    fwrite(&offset, 1, 4, wavfp);
    fseek(wavfp, 4, SEEK_CUR);
    fwrite(&offset, 1, 4, wavfp);
    fseek(wavfp, 0, SEEK_END);
    m_nTotalWaveSize = offset;

    return wavfp;
}

void MtgArchive::increase_file_size(FILE* wavfp, int size)
{
    int newsize;
    wavehdr hdr;

    if (wavfp==NULL || size==0) return;

    fseek(wavfp, 4, SEEK_SET);
    fread(&newsize, 1, 4, wavfp);
    newsize += size;
    fseek(wavfp, 4, SEEK_SET);
    fwrite(&newsize, 1, 4, wavfp);

    log_debug(ZONE, "Alter the wave file size to %d", newsize);

    fseek(wavfp, ((char*)&hdr.fact_size-(char*)&hdr), SEEK_SET);
    fread(&newsize, 1, 4, wavfp);
    newsize += size;
    fseek(wavfp, ((char*)&hdr.fact_size-(char*)&hdr), SEEK_SET);
    fwrite(&newsize, 1, 4, wavfp);

    fseek(wavfp, 4, SEEK_CUR);
    fwrite(&newsize, 1, 4, wavfp);
    fseek(wavfp, 0, SEEK_END);

	m_nTotalWaveSize = newsize;
}

void write_more_delay(FILE* wavfp, int vrdelay, int sample_size)
{
    if (wavfp==NULL) return;

    log_debug(ZONE, "Added extra %d s delay", vrdelay);

    int samples_per_sec = 8000 * sample_size;

    void* p = malloc(samples_per_sec);
    memset(p, 0, samples_per_sec);
    for (int i=0; i<vrdelay; i++)
    {
        fwrite(p, 1, samples_per_sec, wavfp);
    }
    free(p);
}

int MtgArchive::recv_full(int sock, char * buf, size_t len, int flags)
{
    size_t n_want = len;
    size_t n_got = 0;
    int ret;

    while (n_want > 0)
    {
        ret = recv(sock, &buf[n_got], n_want, flags);
        if (ret < 0)
        {
            log_error(ZONE, "Unable to receive data: %s", strerror(errno));
            return -1;
        }
        if (ret == 0)
        {
            struct timespec req_sleep = {0};
            req_sleep.tv_nsec = 500000000; // nanosecs
            nanosleep(&req_sleep, NULL);
        }

        n_got += ret;
        n_want -= ret;
    }

    return 0;
}

int MtgArchive::merge_external_audio(FILE **wavfp, const char * filename, time_t start_time, time_t & prev_time)
{
	int res = 0;
	IString infile = m_full_path + "/download/" + filename;
	IString outfile = m_full_path + "/audio.raw";

	// Not the most efficient, but convert each given file to match format generated by our
	// voice bridge, then add to single audio file with silence between segments.
	IString samples_per_sec = MtgArchiver::s_sample_size == 1 ? "8000" : "16000";
	IString cmd = "sox " + infile + " -t .raw -r " + samples_per_sec + " -U -c 1 " + outfile + " >/tmp/sox.log 2>&1";
	res = system(cmd);
	if (WIFSIGNALED(res) || WEXITSTATUS(res) != 0)
	{
		log_error(ZONE, "Error converting source wave file: %s", cmd.get_string());
		return res;
	}

	// Get size of raw data file.
	struct stat info;
	stat(outfile, &info);

	if (*wavfp == NULL)
	{
		if (m_local_vrstart_time <= 0)
			set_voice_recording_delay(start_time);
		*wavfp = create_wav_file(info.st_size + sizeof(wavehdr));
		prev_time = start_time + info.st_size/sample_count();
	}
	else
	{
		// Pad output if needed.
		if (prev_time > 0 && start_time > prev_time)
		{
			int pad = start_time - prev_time;
			write_more_delay(*wavfp, pad, MtgArchiver::s_sample_size);
			info.st_size += pad * sample_count();
		}
		else
		{
			log_error(ZONE, "Overlapping audio files supplied; audio data will be appended to end");
		}

		// Update size in wave header
		increase_file_size(*wavfp, info.st_size);
		prev_time = start_time + info.st_size/sample_count();
	}

	// Copy from temp file to our wave file
	FILE *copy = fopen(outfile, "rb");
	if (copy)
	{
		char buffer[1024];
		int read;
		while ((read = fread(buffer, 1, sizeof(buffer), copy)) > 0)
		{
			fwrite(buffer, 1, read, *wavfp);
		}

		fclose(copy);
	}
	else
	{
		log_error(ZONE, "Error reading downloaded audio data (%d)", errno);
		res = -1;
	}

	return res;
}

int MtgArchive::fetch_external_recording()
{
	int stat = 0;

	if (m_recording_format == "wave")
	{
		/* Single audio recording */
		int retries = 0;
		IString wavfn;
		wavfn = m_full_path;
		wavfn.append("/audio.wav");

		do {
			stat = fetch_file(m_recording_url, wavfn, NULL, NULL);
			if (stat)
				sleep(30);
		} while (stat != 0 && retries++ < 3);
	}
	else if (m_recording_format == "wave/zip")
	{
		/* Archive with multiple audio files */
		int retries = 0;
		IString arc_fn = m_full_path + "/audio.zip";
		IString arc_path = m_full_path + "/download";
		do {
			stat = fetch_archive(m_recording_url, arc_fn, arc_path);
			if (stat)
				sleep(30);
		} while (stat != 0 && retries++ < 3);

		/* expecting a meta file with list of audio files & start times */
		if (stat == 0)
		{
			IString meta_fn = arc_path + "/meta.xml";

			nad_t nad;
			FILE * wavfp = NULL;
			time_t prev_time = 0;

			stat = nad_load((char *)meta_fn.get_string(), &nad);
			if (!stat)
			{
				/* format:  <meta><recording><file>xxx</file><startTime>xxx</startTime></recording></meta> */
				int elem = nad_find_first_child(nad, 0);
				while (elem > 0)
				{
					int file_elem = nad_find_child_elem(nad, elem, "file", 1);
					int time_elem = nad_find_child_elem(nad, elem, "startTime", 1);
					if (file_elem < 0 || time_elem < 0)
					{
						log_error(ZONE, "Unable to pass meta.xml for archive %s", m_recording_url.get_string());
						stat = Failed;
						break;
					}
					IString file_str(NAD_CDATA(nad, file_elem), NAD_CDATA_L(nad, file_elem));
					IString time_str(NAD_CDATA(nad, time_elem), NAD_CDATA_L(nad, time_elem));

					// Get current time to set up timezone info in structure
					struct tm file_time;
					time_t now = time(NULL);
					file_time = *localtime(&now);

					// Now parse specified file time
					int year, month, day, hour, min, sec;
					if (sscanf(time_str, "%4d-%2d-%2d-%2d-%2d-%2d", &year, &month, &day, &hour, &min, &sec) < 6)
					{
						log_error(ZONE, "Unable to parse recording start time: %s", time_str.get_string());
						stat = Failed;
						break;
					}
					file_time.tm_year = year - 1900;
					file_time.tm_mon = month - 1;
					file_time.tm_mday =  day;
					file_time.tm_hour = hour;
					file_time.tm_min = min;
					file_time.tm_sec = sec;
					time_t start_time = mktime(&file_time);

					stat = merge_external_audio(&wavfp, file_str, start_time, prev_time);
					if (stat)
						break;

					elem = nad_find_next_sibling(nad, elem);
				}
			}

			if (wavfp != NULL)
				fclose(wavfp);
		}
	}
	else
	{
		log_error(ZONE, "Unknown recording format: %s", m_recording_format.get_string());
		stat = Failed;
	}

	return stat;
}

int MtgArchive::fetch_mtg_recording()
{
    log_debug(ZONE, "archive %s@%s: fetching meeting recording",
              (const char *)m_folder, s_arc_state_str[m_state]);

    // If recording url was specified, fetch audio from there instead of from our voice bridge.
	if (m_recording_url.length())
	{
		int stat = fetch_external_recording();
		if (stat)
		{
			log_error(ZONE, "Unable to retrieve recording from %s", m_recording_url.get_string());
			return -1;
		}
		return 0;
	}

    struct hostent *hsent;
    if ((hsent = gethostbyname(m_vbridge_host)) == NULL)
    {
        // get the host info
        log_error(ZONE, "Unable to get host info for %s: %s\n",
                  (const char *)m_vbridge_host, strerror(errno));
        return -1;
    }

    IString folder = m_folder;
    int nStartTimeIndex = -1;
    int nAdditionalTotalBytes = 0;
    FILE * wavfp = NULL;
    int recved = 0;

    do {
      int sock;
      if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1)
	  {
        log_error(ZONE, "Unable to open socket for voice file: %s\n",
                  strerror(errno));
        return -1;
	  }

      struct sockaddr_in addr;
      addr.sin_family = AF_INET;    // host byte order
      addr.sin_port = htons(MtgArchiver::s_vbridge_xfer_port);  // short, network byte order
      addr.sin_addr = *((struct in_addr *)hsent->h_addr);
      memset(&(addr.sin_zero), '\0', 8);  // zero the rest of the struct

	  timeval r_timeout = {s_socket_timeout, 0};
	  setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char*)&r_timeout, sizeof(r_timeout));

      int retries = 0;
      int err;
      while ((err = connect(sock, (const sockaddr*)&addr, sizeof(addr))) < 0 &&
           retries < 3) //60) // too long for voice stub
	  {
        bool bail = false;
        switch (errno)
        {
            case ECONNREFUSED:
                // Maybe restarting and hasn't opened a listen port; try later
            case ETIMEDOUT:
                // Too much load? ; try later
            case ENETUNREACH:
                // Router config issue; try later
            case EAGAIN:
                // Local resource issue.; try later
                log_debug(ZONE, "Retrying connect due to %s", strerror(errno));
                break;

            default:
                bail = true;
                break;
        }

        if (bail) break;

        sleep(1);	//30);	// 30 sec?
        ++retries;
	  }

      if (err < 0)
	  {
        log_error(ZONE, "Unable to connect to %s: %s",
                  (const char *)m_vbridge_host, strerror(errno));

        return -1;
	  }

      if (nStartTimeIndex!=-1) {
        char startstr[21];
        struct tm *tm_start = localtime((time_t *)&m_AdditionalStartTime[nStartTimeIndex]);
        strftime(startstr, 21, "-%Y-%m-%d-%H-%M-%S", tm_start);

        folder = (const char *)m_mtg_id;
        folder.append(startstr);

        // TODO: use current file size and additional voice time to figure out how much
        // silence to add.
        int vrdelay = 0;

        write_more_delay(wavfp, vrdelay, MtgArchiver::s_sample_size);
        nAdditionalTotalBytes += sample_count() * vrdelay;	// need it to modify the header
      }

      //  The voice bridge now requires this command string...
      char   cIicCmd[ ] = {"iic:fetchmtg"};
      IString istrIicCmd = cIicCmd;
      if (send(sock, (const char *)istrIicCmd, strlen((const char *)istrIicCmd), 0) == -1)
      {
        log_error(ZONE, "Couldn't send iicCmd string to voice bridge: %s", strerror(errno));
        close(sock);
        return -1;
      }

      int size = htonl(strlen((const char *)folder));

      if (send(sock, &size, 4, 0) == -1) {
        log_error(ZONE, "Couldn't send to voice bridge: %s", strerror(errno));

        close(sock);
        return -1;
      }

      log_debug(ZONE, "fetching meeting recording %s", folder.get_string());
      if (send(sock, (const char *)folder, strlen((const char *)folder), 0) == -1) {
        log_error(ZONE, "Couldn't send to voice bridge: %s", strerror(errno));

        close(sock);
        return -1;
      }

      if (recv_full(sock, (char *)&size, 4, 0) == -1) {
        log_error(ZONE, "Couldn't receive from voice bridge: %s", strerror(errno));

        close(sock);
        return -1;
      }

      size = ntohl(size);
      if (size == 0)
      {
        log_debug(ZONE, "Zero length wav file; skipping");

        ++nStartTimeIndex;
        if (nStartTimeIndex < m_nAdditionalStartTime)
		{
          close(sock);
          continue;	// continue to retrieve the next voice file, in case client crash, or other problems that cause meeting restarted
		}
        return 0;
      }

      if (wavfp == NULL)	// only create wave file once
        wavfp = create_wav_file(size);
      if (wavfp == NULL)
	  {
        close(sock);
        return -1;
	  }

      log_debug(ZONE, "receiving %d bytes", size);

      char buff[4192];

      // ignore the header
      if (recv_full(sock, buff, sizeof(wavehdr), 0) < 0)
      {
        log_error(ZONE, "Couldn't receive from voice bridge: %s",
                  strerror(errno));

        fclose(wavfp);
        close(sock);
        return -1;
      }
      size -= sizeof(wavehdr);

      while (size > 0 && (recved = recv(sock, buff, sizeof(buff), 0)) >= 0)
      {
        if (recved > 0)
        {
            int ret = fwrite(buff, 1, recved, wavfp);
            if (ret < 0)
            {
                log_error(ZONE, "Unable to write wav data: %s\n", strerror(errno));

                fclose(wavfp);
                close(sock);

                return -1;
            }
            size -= recved;
        }
        else if (recved == 0)
        {
            sleep(100);
        }
      }

      ++nStartTimeIndex;

      close(sock);
    } while(nStartTimeIndex < m_nAdditionalStartTime);

    increase_file_size(wavfp, nAdditionalTotalBytes);

    fclose(wavfp);

    return 0;
}

int MtgArchive::splitAndConvert()
{
	int ret = 0;
	IString iwavfn = (const char *)m_full_path;
    int TwoAndHalfHour = (sample_count() * MtgArchiver::s_frames_per_movie);
	int OverlapTime = sample_count() * 4 / 3; //12000;	// 1.5 sec;

	iwavfn.append("/audio.wav");
	if (m_nTotalWaveSize > TwoAndHalfHour)
	{
		FILE* wavf = fopen(iwavfn.get_string(), "rb");
		if (wavf)
		{
			int index = 0;
			char buf[sample_count()];
			wavehdr hdr;
			fread(&hdr, 1, sizeof(wavehdr), wavf);

			while (m_nTotalWaveSize>0)
			{
				FILE* wavf2;
				IString iwavfn2 = (const char *)m_full_path;
				int size = (m_nTotalWaveSize>TwoAndHalfHour)?(TwoAndHalfHour+OverlapTime):m_nTotalWaveSize;	// add 2 sec overlap

				m_nTotalWaveSize -= TwoAndHalfHour;

				sprintf(buf, "/audio%d.wav", index);
				iwavfn2.append(buf);

                log_debug(ZONE, "Splt and convert: %s, %d left, %d chuck size\n", buf, m_nTotalWaveSize, size);

				wavf2 = fopen(iwavfn2.get_string(), "wb+");
				if (wavf2)
				{
					hdr.f_len = size + sizeof(wavehdr) - 4;
					hdr.data_len = size;
					hdr.fact_size = size;

					fwrite(&hdr, 1, sizeof(wavehdr), wavf2);

					while(size > 0)
					{
						int count = fread(buf, 1, (size>sample_count() ? sample_count() : size), wavf);
						if (count<=0)
							break;
						fwrite(buf, 1, count, wavf2);
						size -= count;
					}
					fclose(wavf2);

					ret = convert(index++);
					if (ret < 0)
					{
						break;  // retry it later
					}


					if (m_nTotalWaveSize>0)	// rewind 2 sec for overlapping
					{
						fseek(wavf, -OverlapTime, SEEK_CUR);
					}
				}
			}
			fclose(wavf);
		}
	}
	else
	{
		IString iwavfn2 = (const char *)m_full_path;
		iwavfn2.append("/audio0.wav");
		IString mvcmd = "mv ";
		mvcmd.append(iwavfn);
		mvcmd.append(" ");
		mvcmd.append(iwavfn2);
		int ret = system(mvcmd.get_string());
		if (WIFSIGNALED(ret) || WEXITSTATUS(ret) != 0)
		{
		  log_error(ZONE, "Error executing %s", mvcmd.get_string());
		}

		ret = convert(0);
	}
	return ret;
}

int MtgArchive::convert(int index)
{
    log_debug(ZONE, "archive %s@%s: converting mtg wav into mp3",
              (const char *)m_folder, s_arc_state_str[m_state]);

    struct stat s;
    char buf[32];

    IString iwavfn = (const char *)m_full_path;
	if (index >= 0) {
		sprintf(buf, "/audio%d.wav", index);
		iwavfn.append(buf);
	} else {
		iwavfn.append("/audio.wav");
	}

    if (stat(iwavfn, &s) < 0)
    {
        if (errno == ENOENT)
        {
            log_debug(ZONE, "No audio; nothing to do");

            return 0;
        }
        else
        {
            log_error(ZONE, "Couldn't lookup info about %s: %s",
                      (const char *)iwavfn, strerror(errno));

            return -1;
        }
    }

    IString owavfn = (const char *)m_full_path;
    owavfn.append("/audio-lin.wav");

    IString cmdln = "sox ";
    cmdln.append(iwavfn);
    cmdln.append(" -s -2 -r 11025 ");	// ming can only work with 5, 11, 22, 44k
    cmdln.append(owavfn);
    cmdln.append("> /tmp/sox.log 2>&1");
    int ret = system(cmdln);
    if (WIFSIGNALED(ret) || WEXITSTATUS(ret) != 0)
    {
        log_error(ZONE, "sox conversion failed");

        return -1;
    }

    IString omp3fn = (const char *)m_full_path;
    if (index >= 0) {
		sprintf(buf, "/audio%d.mp3", index);
	    omp3fn.append(buf);
	}
	else {
	    omp3fn.append("/audio.mp3");
	}
    cmdln = "lame --silent -h -m m -b 16 ";	// --preset phone will fix sample rate at 8k
    cmdln.append(owavfn);
    cmdln.append(" ");
    cmdln.append(omp3fn);
    ret = system(cmdln);
    if (WIFSIGNALED(ret) || WEXITSTATUS(ret) != 0)
    {
        log_error(ZONE, "notlame conversion failed");

        return -1;
    }

    log_debug(ZONE, "Converted %s to %s",
              (const char *)iwavfn, (const char *)omp3fn);

    return 0;
}

int MtgArchive::generate_pdf(IString& ipath, const char* doc_name)
{
    int res = 0;
    int pages = 0;

    IString index_file = ipath + "/" + doc_name + "/index.xml";

    IString pdf_cmd = "cd \"" + ipath + "/" + doc_name + "\"; gm convert ";
    nad_t nad;

    res = nad_load((char *)index_file.get_string(), &nad);
    if (!res)
    {
        /* Format: <index><page title="Slide 1" file="Slide1.EMZ" altfile="Slide1.PNG" scale="1.0" altscale="4.0" fittowidth="F"/></index> */
        int elem = nad_find_first_child(nad, 0);
        while (elem > 0)
        {
            int file_elem = nad_find_attr(nad, elem, "file", NULL);
            int altfile_elem = nad_find_attr(nad, elem, "altfile", NULL);
            if (file_elem < 0 || altfile_elem < 0)
            {
                log_error(ZONE, "Unable to parse index.xml for document %s", doc_name);
                res = -1;
                break;
            }
            IString file_str(NAD_AVAL(nad, file_elem), NAD_AVAL_L(nad, file_elem));
            IString altfile_str(NAD_AVAL(nad, altfile_elem), NAD_AVAL_L(nad, altfile_elem));

            if (altfile_str.length() > 0)
            {
                pdf_cmd = pdf_cmd + "\"" + altfile_str + "\" ";
                ++pages;
            }

#if 0
            // Merged files upload from client currently are incomplete
            IString merged_str = file_str + ".jpg.merged";

            struct stat sb;
            if (stat(ipath + "/" + doc_name + "/" + merged_str.get_string(), &sb) == 0)
            {
                pdf_cmd = pdf_cmd + "\"" + merged_str + "\" ";
            }
#endif

            elem = nad_find_next_sibling(nad, elem);
        }
    }

    if (pages == 0)
    {
        log_debug(ZONE, "No content found in %s, skipping", doc_name);
        res = -1;
    }

    if (!res)
    {
        log_debug(ZONE, "Generate pdf: %s", pdf_cmd.get_string());
        pdf_cmd = pdf_cmd + " \"" + m_full_path + "/" + doc_name + ".pdf\"";
        res = system(pdf_cmd);
        if (WIFSIGNALED(res) || WEXITSTATUS(res) != 0)
        {
            log_error(ZONE, "Error executing %s", pdf_cmd.get_string());
        }
    }

    return res;
}

int MtgArchive::package_docs()
{
    struct stat sb;
    bool have_doc = false;

    if (strncmp("file:", MtgArchiver::s_docshare_repository_url, strlen("file:")) != 0)
    {
        log_error(ZONE, "Only file: URL's currently supported");
        return -1;
    }

    IString ipath = (const char *)(MtgArchiver::s_docshare_repository_url) + strlen("file:");
    ipath = ipath + "/" + m_mtg_id;
    if (stat(ipath.get_string(), &sb) < 0)
    {
        log_debug(ZONE, "no shared documents found for %s", m_mtg_id.get_string());
        return -1;
    }

    IString zip_cmd = "cd " + m_full_path + "; zip docs.zip";

    // Find all subdirectories with "index.xml" and send images
    // Probably should read index.xml in case pages have been rearranged (use nad)
    DIR * d;
    d = opendir(ipath);
    if (d == NULL)
    {
        log_error(ZONE, "Couldn't open directory %s: %s", (const char *)ipath, strerror(errno));
        return -1;
    }

    struct dirent * ent = NULL;
    for ( ent = readdir(d); ent != NULL; ent = readdir(d) )
    {
        if (strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0)
            continue;

        IString doc_dir = ipath + "/" + ent->d_name;

        if (stat(doc_dir + "/index.xml", &sb) < 0)
            continue;

        zip_cmd = zip_cmd + " \"" + ent->d_name + ".pdf\"";

        if (!generate_pdf(ipath, ent->d_name))
            have_doc = true;
    }

    closedir(d);

    if (have_doc)
    {
        log_debug(ZONE, "Package slides: %s", zip_cmd.get_string());
        int res = system(zip_cmd);
        if (WIFSIGNALED(res) || WEXITSTATUS(res) != 0)
        {
            log_error(ZONE, "Error executing %s", zip_cmd.get_string());
        }
    }

    return 0;
}

int MtgArchive::upload_archive()
{
    struct stat sb;
    int ret;
	char buf[4096];

    if (strncmp("file:", MtgArchiver::s_mtg_repository_url, strlen("file:")) != 0)
    {
        log_error(ZONE, "Only file: URL's currently supported");
        return -1;
    }

    gchar * fs = strdup(m_folder);
    g_strreverse(fs);
    MtgArchiver::encrypt(&m_encrypt_folder, fs);
    free(fs);

    if (m_include_docs)
    {
        package_docs();
    }

	generate_download_html();

    IString opath = (const char *)(MtgArchiver::s_mtg_repository_url) + strlen("file:");
    if (make_dir(opath, 0775) < 0)
        return -1;

    if (stat(opath.get_string(), &sb) < 0)
    {
        log_error(ZONE, "Couldn't get info for %s: %s",
                  (const char *)opath, strerror(errno));
        return -1;
    }

    opath.append("/");
    opath.append(m_mtg_id);
    if (make_dir(opath, 0775) < 0)
        return -1;
    chown(opath.get_string(), (uid_t)-1, sb.st_gid);
    chmod(opath.get_string(), 0775);	// make sure group can delete it

    opath.append("/");
    opath.append(m_encrypt_folder);
    if (make_dir(opath, 0775) < 0)
        return -1;

    if (chown(opath.get_string(), (uid_t)-1, sb.st_gid)!=0)
    {
        if (errno == EPERM)
        {
            log_error(ZONE, "chown is restricted: %s.\n", opath.get_string());
        }
        else
        {
            log_error(ZONE, "Unexpected chown error. %d\n", errno);
        }
    }
    chmod(opath.get_string(), 0775);	// make sure group can delete it

    IString ofile = (const char *)opath;
    ofile.append("/summary.txt");
    IString ifile = (const char *)m_full_path;
    ifile.append("/summary.txt");
    copy_files(ofile, ifile);

    ofile = (const char *)opath;
    ofile.append("/audio.mp3");
    ifile = (const char *)m_full_path;
    ifile.append("/audio.mp3");
    copy_files(ofile, ifile);

    ifile = (const char *)m_full_path;
    ifile.append("/chat.txt");
    ofile = (const char *)opath;
    ofile.append("/chat.txt");
    copy_files(ofile, ifile);

    ifile = (const char *)m_full_path;
    ifile.append("/video.");
    ifile.append(MtgArchiver::s_file_type);
    ofile = (const char *)opath;
    ofile.append("/video.");
    ofile.append(MtgArchiver::s_file_type);
    sprintf(buf, "cp %s %s", ifile.get_string(), ofile.get_string());
    ret = system(buf);
    if (WIFSIGNALED(ret) || WEXITSTATUS(ret) != 0)
    {
        log_error(ZONE, "Error executing %s", buf);
    }

    ifile = (const char *)m_full_path;
    ifile.append("/docs.zip");
    ofile = (const char *)opath;
    ofile.append("/docs.zip");
    copy_files(ofile, ifile);

    ifile = (const char *)m_full_path;
    ifile.append("/*.pdf");
    ofile = (const char *)opath;
    sprintf(buf, "cp %s %s", ifile.get_string(), ofile.get_string());
    ret = system(buf);
    if (WIFSIGNALED(ret) || WEXITSTATUS(ret) != 0)
    {
        log_error(ZONE, "Error executing %s", buf);
    }

    ifile = (const char *)m_full_path;
    ifile.append("/video.");
    ifile.append(MtgArchiver::s_file_type);
    if (MtgArchiver::check_exists(ifile)==0)
    {
        ifile = (const char *)m_full_path;
        ifile.append("/ShowMovie.html");
        ofile = (const char *)opath;
        ofile.append("/ShowMovie.html");
        copy_files(ofile, ifile);

        ifile = (const char *)m_full_path;
        ifile.append("/video.html");
        ofile = (const char *)opath;
        ofile.append("/video.html");
        copy_files(ofile, ifile);
    }

    if (MtgArchiver::s_post_command.length() > 0)
    {
        sprintf(buf, "%s %s %s %s/%s >/var/log/iic/post.txt 2>&1", MtgArchiver::s_post_command.get_string(), MtgArchiver::s_file_type.get_string(), m_full_path.get_string(),
                m_mtg_id.get_string(), m_encrypt_folder.get_string());
        ret = system(buf);
        if (WIFSIGNALED(ret) || WEXITSTATUS(ret) != 0)
        {
            log_error(ZONE, "Error executing %s", buf);
        }
        else
        {
            log_debug(ZONE, "Executed post command: %s", buf);
        }
    }

    log_debug(ZONE, "archive %s@%s: uploaded mtg archive to repository: %s",
              (const char *)m_folder,
              s_arc_state_str[m_state],
              (const char *)opath);

    return 0;
}

int MtgArchive::copy_files(const char *ofile, const char *ifile)
{
    FILE *ifp = fopen(ifile, "rb");
    if (ifp == NULL)
    {
        if (errno == ENOENT)
        {
            log_debug(ZONE, "Skipping file %s: %s",
                      (const char *)ifile, strerror(errno));
            return 0;
        }

        log_error(ZONE, "Unable to copy %s: %s",
                  (const char *)ifile, strerror(errno));
        return -1;
    }

    FILE *ofp = fopen(ofile, "wb");
    if (ofp == NULL)
    {
        log_error(ZONE, "Unable to open output file %s: %s",
                  (const char *)ofile, strerror(errno));
        fclose(ifp);
        return -1;
    }

    char buf[8192];
    long total_size = 0;
    while (!feof(ifp))
    {
        size_t i_actual = fread(buf, sizeof(char), sizeof(buf), ifp);
        total_size += i_actual;
        if (i_actual == sizeof(buf) || !ferror(ifp))
        {
            size_t o_actual = fwrite(buf, sizeof(char), i_actual, ofp);
            if (i_actual != o_actual)
            {
                log_error(ZONE, "Error while writing to %s: %s",
                          (const char *)ofile, strerror(errno));
                fclose(ifp);
                fclose(ofp);

                return -1;
            }
        }
    }

    log_debug(ZONE, "Uploaded file %s(%d)", (const char *)ifile, total_size);

    fclose(ifp);
    fclose(ofp);

    chmod(ofile, 0644);	// make sure it's world readable

    return 0;
}


int MtgArchive::cleanup()
{
    // Don't do any cleanup whatsoever if keep_raw_data is set
	if (MtgArchiver::s_keep_raw_data)
        return 0;

    log_debug(ZONE, "archive %s@%s: cleaning up spool folder",
              (const char *)m_folder, s_arc_state_str[m_state]);

    m_enabled = 0;
    m_started = -1;
    m_status = RS_NONE;

    DIR * d;
    d = opendir(m_full_path);
    if (d == NULL)
    {
        log_error(ZONE, "Couldn't open directory %s: %s",
                  (const char *)m_full_path, strerror(errno));
        return -1;
    }

    reset_recording();

    bool err = false;

    struct dirent * ent = NULL;
    for ( ent = readdir(d); ent != NULL; ent = readdir(d) )
    {
        if (strcmp(ent->d_name, ".") == 0)
            continue;

        if (strcmp(ent->d_name, "..") == 0)
            continue;

        IString path = (const char *)m_full_path;
        path.append("/");
        path.append(ent->d_name);

        if (unlink((const char *)path) < 0)
        {
            log_error(ZONE, "Couldn't clean up %s: %s", ent->d_name, strerror(errno));
            // don't return here; try to clean up as much stuff as we can.
            // set an err flag tho :-)
            err = true;
        }
    }

    closedir(d);

    if (rmdir(m_full_path) < 0)
    {
        log_error(ZONE, "Couldn't clean up %s: %s",
                  (const char *)m_full_path, strerror(errno));

        return -1;
    }

    return (!err ? 0 : -1);
}

int
MtgArchiver::refresh_email_template()
{
    log_debug(ZONE, "Refreshing e-mail template");

    long len;
    FILE *fp = fopen(s_email_template, "r");
    if (fp == NULL)
    {
        log_error(ZONE, "Couldn't open %s for reading: %s",
                  (const char *)s_email_template, strerror(errno));
        return -1;
    }

    fseek(fp, 0L, SEEK_END);
    len = ftell(fp);
    rewind(fp);

    gchar * buf = (char *)malloc(len+1);
    if (fread(buf, sizeof(char), len, fp) != (size_t)len)
    {
        log_error(ZONE, "Couldn't read data from %s: %s",
                  (const char *)s_email_template, strerror(errno));
        fclose(fp);
        free(buf);
        return -1;
    }
    buf[len] = '\0';

    buf = g_strdelimit(buf, "<>", '\001');

    if (s_parsed_email_template)
        g_strfreev(s_parsed_email_template);

    s_parsed_email_template =
        g_strsplit(buf, "\001", -1 /* no limit to # of splits */);

    free(buf);
    fclose(fp);

    return 0;
}

int
MtgArchiver::update_email_template()
{
    struct stat sb;

    if (stat(s_email_template, &sb) < 0)
    {
        log_error(ZONE, "Couldn't get info for %s: %s",
                  (const char *)s_email_template, strerror(errno));
        return -1;
    }

    time_t mtime = sb.st_mtime;
    if (mtime > s_email_template_modified)
    {
        // Updated; refresh
        return refresh_email_template();
    }

    return 0;
}

int
MtgArchiver::refresh_summary_template()
{
    log_debug(ZONE, "Refreshing summary template");

    long len;
    FILE *fp = fopen(s_summary_template, "r");
    if (fp == NULL)
    {
        log_error(ZONE, "Couldn't open %s for reading: %s",
                  (const char *)s_summary_template, strerror(errno));
        return -1;
    }

    fseek(fp, 0L, SEEK_END);
    len = ftell(fp);
    rewind(fp);

    gchar * buf = (char *)malloc(len+1);
    if (fread(buf, sizeof(char), len, fp) != (size_t)len)
    {
        log_error(ZONE, "Couldn't read data from %s: %s",
                  (const char *)s_summary_template, strerror(errno));
        fclose(fp);
        free(buf);
        return -1;
    }
    buf[len] = '\0';

    buf = g_strdelimit(buf, "<>", '\001');

    if (s_parsed_summary_template)
        g_strfreev(s_parsed_summary_template);

    s_parsed_summary_template =
        g_strsplit(buf, "\001", -1 /* no limit to # of splits */);

    free(buf);
    fclose(fp);

    return 0;
}

int
MtgArchiver::update_summary_template()
{
    struct stat sb;

    if (stat(s_summary_template, &sb) < 0)
    {
        log_error(ZONE, "Couldn't get info for %s: %s",
                  (const char *)s_summary_template, strerror(errno));
        return -1;
    }

    time_t mtime = sb.st_mtime;
    if (mtime > s_summary_template_modified)
    {
        // Updated; refresh
        return refresh_summary_template();
    }

    return 0;
}

int
MtgArchive::which_token(gchar * fragment)
{
    if (strcmp(fragment, MTGARC_TOKEN_MTGID_STR) == 0)
    {
        return MTGARC_TOKEN_MTGID;
    }
    else if (strcmp(fragment, MTGARC_TOKEN_START_DATE_TIME_STR) == 0)
    {
        return MTGARC_TOKEN_START_DATE_TIME;
    }
    else if (strcmp(fragment, MTGARC_TOKEN_PORTAL_URL_STR) == 0)
    {
        return MTGARC_TOKEN_PORTAL_URL;
    }
    else if (strcmp(fragment, MTGARC_TOKEN_DURATION_STR) == 0)
    {
        return MTGARC_TOKEN_DURATION;
    }
    else if (strcmp(fragment, MTGARC_TOKEN_VOICERECORDING_STR) == 0)
    {
        return MTGARC_TOKEN_VOICERECORDING;
    }
    else if (strcmp(fragment, MTGARC_TOKEN_DATARECORDING_STR) == 0)
    {
        return MTGARC_TOKEN_DATARECORDING;
    }
    else if (strcmp(fragment, MTGARC_TOKEN_PARTICIPANTS_STR) == 0)
    {
        return MTGARC_TOKEN_PARTICIPANTS;
    }
    else
        return MTGARC_TOKEN_NONE;
}

void MtgArchive::mk_email_start_time(IString * dtm)
{
    char dtm_str[64];

    struct tm the_time;
    localtime_r(&m_mtg_start_time, &the_time);
    strftime(dtm_str, sizeof(dtm_str),
             _("%a %b %d, %Y at %I:%M:%S %p"),  (const struct tm *)&the_time);
    *dtm = dtm_str;
}

void MtgArchive::mk_portal_url(IString * dtm)
{
    bool has_voice = FALSE;
    *dtm = "";
    IString ifile = (const char *)m_full_path;
    ifile.append("/audio.mp3");

    if (MtgArchiver::check_exists(ifile)==0) {
      has_voice = TRUE;
      dtm->append(_("Click the following link for voice recording:"));
      dtm->append("\n");
      dtm->append((const char *)(MtgArchiver::s_mtg_repository_portal));
      dtm->append("/");
      dtm->append(m_mtg_id);
      dtm->append("/");
      dtm->append(m_encrypt_folder);
      dtm->append("/audio.mp3");
      dtm->append("\n\n");
    }

    ifile = (const char *)m_full_path;
    ifile.append("/chat.txt");
    if (MtgArchiver::check_exists(ifile)==0) {
      dtm->append(_("Click the following link for meeting chat:"));
      dtm->append("\n");
      dtm->append((const char *)(MtgArchiver::s_mtg_repository_portal));
      dtm->append("/");
      dtm->append(m_mtg_id);
      dtm->append("/");
      dtm->append(m_encrypt_folder);
      dtm->append("/chat.txt");
      dtm->append("\n\n");
    }

    ifile = (const char *)m_full_path;
    ifile.append("/video.");
    ifile.append(MtgArchiver::s_file_type);
    if (MtgArchiver::check_exists(ifile)==0) {
      if (has_voice)
        dtm->append(_("Click the following link for meeting recording (voice & data):"));
      else
        dtm->append(_("Click the following link for meeting recording (data-only):"));
      dtm->append("\n");
      dtm->append((const char *)(MtgArchiver::s_mtg_repository_portal));
      dtm->append("/");
      dtm->append(m_mtg_id);
      dtm->append("/");
      dtm->append(m_encrypt_folder);
      dtm->append("/video.html");
      dtm->append("\n\n");
    }

    ifile = (const char *)m_full_path;
    ifile.append("/summary.txt");
    if (MtgArchiver::check_exists(ifile)==0) {
      dtm->append(_("Click the following link for meeting summary:"));
      dtm->append("\n");
      dtm->append((const char *)(MtgArchiver::s_mtg_repository_portal));
      dtm->append("/");
      dtm->append(m_mtg_id);
      dtm->append("/");
      dtm->append(m_encrypt_folder);
      dtm->append("/summary.txt");
      dtm->append("\n\n");
    }

    ifile = (const char *)m_full_path;
    ifile.append("/docs.zip");
    if (MtgArchiver::check_exists(ifile)==0) {
      dtm->append(_("Click the following link for shared presentations:"));
      dtm->append("\n");
      dtm->append((const char *)(MtgArchiver::s_mtg_repository_portal));
      dtm->append("/");
      dtm->append(m_mtg_id);
      dtm->append("/");
      dtm->append(m_encrypt_folder);
      dtm->append("/docs.zip");
      dtm->append("\n\n");
    }
}

int MtgArchive::call_send_email(const char* to_email, const char* subj, const char* body)
{
    static unsigned int cid = 0;
    char id[16];
    sprintf(id, "email:%u", cid++);
    if (rpc_make_call(id, "mailer",
                      "mailer.send_message", "(sssss)",
                      (const char *)to_email,
                      (const char *)MtgArchiver::s_email_from,
                      (const char *)MtgArchiver::s_email_from_display,
                      (const char *)subj,
                      (const char *)body))
    {
        log_error(ZONE, "Error sending e-mail call to mailer");
    }

	return 0;
}

int MtgArchive::send_email(bool preview)
{
    if (!m_recordingStarted) {
        log_debug(ZONE, "archive %s@%s: no recording.",
              (const char *)m_folder, s_arc_state_str[m_state]);

        if (preview)
        {
          char buff[256];
          sprintf(buff, _("There is no recording for this meeting (%s), no movie or mp3 file is produced for previewing."),
	          m_mtg_id.get_string());
          strcat(buff, "\n");
          call_send_email(m_host_email, _("RE: Preview request"), buff);
          return 0;
        }
    }

    log_debug(ZONE, "archive %s@%s: sending e-mail to host",
              (const char *)m_folder, s_arc_state_str[m_state]);

    if (MtgArchiver::s_parsed_email_template == NULL)
    {
        log_error(ZONE, "No e-mail template set; no e-mail sent!");

        return 0;
    }

    IString email_body;

    int ii;
    for (ii = 0; MtgArchiver::s_parsed_email_template[ii] != NULL; ii++)
    {
        gchar * frag = MtgArchiver::s_parsed_email_template[ii];
        int tok = which_token(frag);
        switch (tok)
        {
          case MTGARC_TOKEN_NONE:
            email_body.append(frag);
            break;

          case MTGARC_TOKEN_MTGID:
            email_body.append(m_mtg_id);
            break;

          case MTGARC_TOKEN_START_DATE_TIME:
            {
                IString dtm;
                mk_email_start_time(&dtm);
                email_body.append(dtm);
            }
            break;

          case MTGARC_TOKEN_PORTAL_URL:
            {
                IString url;
			    char buff[256];
                mk_portal_url(&url);
                if (url.length()==0) {
                    log_error(ZONE, "No content for url!");
					if (preview)
					{
 						sprintf(buff, _("There is no recording for this meeting (%s), no movie or mp3 file is produced for previewing."),
							m_mtg_id.get_string());
                        strcat(buff, "\n");
						call_send_email(m_host_email, _("RE: Preview request"), buff);
					}
					else
					{
 						sprintf(buff, _("Although you have turned on recording, there is no voice/data has been recorded.  No movie or mp3 file is produced for this meeting (%s)."),
							m_mtg_id.get_string());
                        strcat(buff, "\n");
						call_send_email(m_host_email, _("No recording"), buff);
					}
                    return 0;	// return successful, the functions called previously would have caught the error
                }
                email_body.append(url);
            }
            break;

          default:
                log_error(ZONE, "Unhandled email token %d seen!", tok);
        }
    }

	return call_send_email(m_host_email, (preview?_("RE: Preview request"):MtgArchiver::s_email_subject.get_string()),
		email_body.get_string());
}

DIR *
MtgArchiver::get_first(const char *mtgid, int pos)
{
    DIR * d;
    IString path;

    if (pos < 0)
    {
        log_error(ZONE, "Invalid pos argument %d", pos);
        return NULL;
    }

    if (strncmp("file:", MtgArchiver::s_mtg_repository_url, strlen("file:")) == 0)
    {
        path = (const char *)(MtgArchiver::s_mtg_repository_url) + strlen("file:");
    }
    else
    {
        log_error(ZONE, "Currently, mtg-archive-url must begin with 'file:', but is %s",
                  (const char *)MtgArchiver::s_mtg_repository_url);

        return NULL;
    }

    path = (const char *)MtgArchiver::s_mtg_repository_url + strlen("file:");
    path.append("/");
    path.append(mtgid);

    d = opendir(path);
    if (d == NULL)
    {
        log_error(ZONE, "Couldn't open directory %s: %s",
                  (const char *)path, strerror(errno));
        return NULL;
    }

    struct dirent * ent = NULL;

    // skip . and .. entries (not counted)
    while ((ent = readdir(d)) &&
           (strcmp(ent->d_name, ".") == 0));

    if (pos == 0)
    {
        // already at first; nothing to do
        return d;
    }

    for ( ; pos > 0 && (ent = readdir(d)) != NULL; --pos);

    if (ent == NULL)
    {
        log_error(ZONE, "too few items for offset");
        closedir(d);
        d = NULL;
    }

    return d;
}

DIR *
MtgArchiver::next_mtg_info(DIR *dir, IString * start_time, IString * path)
{
    struct dirent * ent = NULL;

    ent = readdir(dir);
    if (ent == NULL)
    {
        closedir(dir);
        return NULL;
    }
    else
    {
        IString tmp;
        decrypt(&tmp, ent->d_name);
        gchar * tt = strdup(tmp);
        g_strreverse(tt);
        const char *dash = strchr(tt, '-');
        if (dash)
        {
            *start_time = dash+1;
            free(tt);
        }
        else
        {
            log_error(ZONE, "Unable to decrypt archive directory!");
            closedir(dir);
            free(tt);
            return NULL;
        }
        *path = ent->d_name;

        log_debug(ZONE, "Got (%s %s)", (const char *)*start_time, (const char *)*path);
    }

    return dir;
}

int
MtgArchiver::find_remaining(DIR * dir)
{

    if (dir == NULL)
        return 0;

    int remaining;
    for (remaining = 0; readdir(dir); remaining++);

    closedir(dir);

    return remaining;
}

int MtgArchiver::get_archives(const char *mtgid,
                              int first,
                              int chunksz,
                              xmlrpc_env *env,
                              xmlrpc_value **output)
{
    int actsz;

    xmlrpc_value * array = NULL;
    xmlrpc_value * result = NULL;

    DIR *dir = get_first(mtgid, first);
    if (dir == NULL)
    {
        log_error(ZONE, "less that %d archived meetings in the repository",
                  first);
        return InvalidParameter;
    }

    int remain;
    IString start_time;
    IString path;

    array = xmlrpc_build_value(env, "()");
	XMLRPC_FAIL_IF_FAULT(env);
    int ii;
    for (ii = 0; ii < chunksz && (dir = next_mtg_info(dir, &start_time, &path)) != NULL;
         ii++)
    {
        xmlrpc_value *row = xmlrpc_build_value(env, "(sss)",
                                               (const char *)mtgid, (const char *)start_time, (const char *)path);
        xmlrpc_array_append_item(env, array, row);
        xmlrpc_DECREF(row);
    }
    if (ii == 0)
    {
        log_error(ZONE, "less that %d archived meetings in the repository",
                  first+1);
        return InvalidParameter;
    }

    actsz = ii;
    remain = find_remaining(dir);

    result = xmlrpc_build_value(env, "(Viiii)", array, first, chunksz, first + actsz + remain, (remain > 0 ? 1 : 0));
    XMLRPC_FAIL_IF_FAULT(env);

    xmlrpc_INCREF(result);

  cleanup:
    if (result)
        xmlrpc_DECREF(result);

    if (array)
        xmlrpc_DECREF(array);

    *output = result;

    return 0;
}

int MtgArchive::log_chat_message(const char * message,
                                 const char * from,
                                 const char * to)
{
    if (!m_enabled)	// recording was not enabled
      return -1;

    IString ofn = (const char *)m_full_path;
    ofn.append("/chat.txt");
    FILE * fp = fopen(ofn, "a");
    if (fp == NULL)
    {
        log_error(ZONE, "Unable to open host e-mail file %s: %s",
                  (const char *)ofn, strerror(errno));
        return -1;
    }

    time_t t;
    time(&t);
    char temp[16];
    struct tm *tm_start = localtime(&t);
    strftime(temp, 16, "%-l:%M:%S %p", tm_start);

    char * raw_message = (char*)malloc(strlen(message)+1);
	if (raw_message)
	{
		memset(raw_message, 0, strlen(message)+1);
		getRawText(message, raw_message);

        bool use_to = to && *to;
		fprintf(fp, "[%s] %s%s%s%s: %s\n", temp, from, use_to?_(" (to "):"", use_to?to:"", use_to?")":"", raw_message);

		free(raw_message);
	}

	fclose(fp);

    return 0;
}

int MtgArchive::delete_file(const char *attribute)
{
    IString ofn = (const char *)m_full_path;
    ofn.append("/");
    ofn.append(attribute);
    ofn.append(".txt");

    log_debug(ZONE, "delete_file(%s)", ofn.get_string());

    return unlink(ofn.get_string());;
}

int MtgArchive::save(const char *attribute, const char *value)
{
    IString ofn = (const char *)m_full_path;
    ofn.append("/");
    ofn.append(attribute);
    ofn.append(".txt");
    FILE * fp = fopen(ofn, "w");
    if (fp == NULL)
    {
        log_error(ZONE, "Unable to open attribute file %s: %s",
                  (const char *)ofn, strerror(errno));
        return -1;
    }
    fprintf(fp, "%s\n", value);
    fclose(fp);

    return 0;
}

int MtgArchive::restore(const char *attribute, IString * value)
{
    *value = "";

    IString ifn = (const char *)m_full_path;
    ifn.append("/");
    ifn.append(attribute);
    ifn.append(".txt");
    FILE * fp = fopen(ifn, "r");
    if (fp == NULL)
    {
        if (errno != ENOENT)
        {
            log_error(ZONE, "Unable to open attribute file %s: %s",
                      (const char *)ifn, strerror(errno));
            return -1;
        }
        else
        {
            // Doesn't exit; just use empty value
            return 0;
        }
    }
    char tmpbuf[128];
    int items = fscanf(fp, "%s\n", tmpbuf);
    fclose(fp);

    if (items == 1)
        *value = tmpbuf;

    return 0;
}

// save integer to a file
// somethings the controller sends multiple start_data_recording message when clicked call me after share started
// and caused vnc2swf asks for the same data recording multiple times
// fixed this function to avoid same timestamp saved more than once
int MtgArchive::save_array(const char *attribute, int value)
{
	int values[32];
	int *pvalues = values;
	int num, i;
    int extra = restore_array(attribute, pvalues, &num, 32);
	if (extra > 0)
	{
		pvalues = (int*)malloc((32 + extra) * sizeof (int));
		restore_array(attribute, pvalues, &num, 32+extra);
	}
	for (i=0; i< num; i++)
	{
		if (pvalues[i] == value)	// already saved
		{
			if (num>32)
			{
				free (pvalues);
			}
			return 0;
		}
	}
	if (num>32)
	{
		free (pvalues);
	}

    IString ofn = (const char *)m_full_path;
    ofn.append("/");
    ofn.append(attribute);
    ofn.append(".txt");
    FILE * fp = fopen(ofn, "a");
    if (fp == NULL)
    {
        log_error(ZONE, "Unable to open attribute file %s: %s",
                  (const char *)ofn, strerror(errno));
        return -1;
    }
    fprintf(fp, "%d ", value);
    fclose(fp);

    return 0;
}

int MtgArchive::restore_array(const char *attribute, int * array, int * num, int max_num)
{
    *num = 0;

    IString ifn = (const char *)m_full_path;
    ifn.append("/");
    ifn.append(attribute);
    ifn.append(".txt");
    FILE * fp = fopen(ifn, "r");
    if (fp == NULL)
    {
        if (errno != ENOENT)
        {
            log_error(ZONE, "Unable to open attribute file %s: %s",
                      (const char *)ifn, strerror(errno));
            return -1;
        }
        else
        {
            // Doesn't exit; just use empty value
            return 0;
        }
    }
    char tmpbuf[128];
    while(!feof(fp))
    {
      fscanf(fp, "%s ", tmpbuf);
      if (array)
	  {
		  array[*num] = atoi(tmpbuf);
	  }
      *num += 1;
      if (*num == max_num) break;
    }
	int extra = 0;
    while(!feof(fp))	// find out how many left
    {
      fscanf(fp, "%s ", tmpbuf);
	  extra ++;
    }

    fclose(fp);

    return extra;
}

int MtgArchive::set_mtg_id(const char * arg)
{
    m_mtg_id = arg;

    // Represented in the folder name, no need to save
    return 0;
}

int MtgArchive::set_mtg_start_time(time_t arg)
{
    m_mtg_start_time = arg;

    // Represented in the folder name, no need to save
    return 0;
}

int MtgArchive::set_vbridge_host(const char * arg)
{
    m_vbridge_host = arg;
    return save("vbridge_host", arg);
}

int MtgArchive::set_host_email(const char * arg)
{
	if (arg!=NULL && strlen(arg)>0)
	{
		m_host_email = arg;
		return save("host_email", arg);
	}
	return 0;
}

// The voice bridge should send the time at which it started recording.
int MtgArchive::set_voice_recording_delay(int arg)
{
    char tmpbuf[16];
	if (m_local_vrstart_time==0||m_local_vrstart_time==-1)
	{
		m_local_vrstart_time = arg;	//	time(NULL);	assume we are using a time server to sync all the server components
        sprintf(tmpbuf, "%ld", m_local_vrstart_time);
        return save("vrstart", tmpbuf);
	}
	else {	// for additional record sessions
        if (m_nAdditionalStartTime < MAX_FAILURE-1) {
          m_AdditionalStartTime[m_nAdditionalStartTime++] = arg;
          save_array("mtg_start_time", arg);
        }
        log_debug(ZONE, "Set additional voice start at %d", arg);
    }

    return 0;
}


// Set location to download audio for this meeting.
int MtgArchive::set_recording_audio(const char * url, const char * format)
{
	m_recording_url = url;
	m_recording_format = format;
	save("recordurl", url);
	save("recordformat", format);
	log_debug(ZONE, "Set voice recording url to %s (%s)", url, format);
	return 0;
}

//////////////////////////////////////////////////////////////////////////////
bool MtgArchive::check_alive(int process)
{
    if (process == 0)
        return false;

    int fd;
    char buff[256];
    sprintf(buff, "/proc/%d/status", process);

    if ((fd = open(buff, O_RDONLY)) == -1) {
        log_error(ZONE, "Process %d is not running", process);
        return false;
    }
    close(fd);
    return true;
}

// Hack -- strip out markups
void removeMarkup(char * message, const char * markup)
{
    char * tok;
    while (1) {
        tok = strcasestr(message, markup);
        if (!tok) {
            break;
        }
        strcpy(tok, tok+strlen(markup));
    }
}

void replaceChar(char * message, const char * markup, const char cr)
{
    char * tok;
    while (1) {
        tok = strcasestr(message, markup);
        if (!tok) {
            break;
        }
        *tok = cr;
        strcpy(tok+1, tok+strlen(markup));
    }
}

void getRawText(const char * message, char * out)
{
	char* raw_message = (char*)malloc(strlen(message)+1);
    const char * p = raw_message;

	if (!raw_message)
		return;

	strcpy(raw_message, message);

	replaceChar(raw_message, "&amp;",  '&');
	replaceChar(raw_message, "&lt;",   '<');
	replaceChar(raw_message, "&gt;",   '>');
	replaceChar(raw_message, "&apos;", '\'');
	replaceChar(raw_message, "&quot;", '\"');
	replaceChar(raw_message, "<LF>",  '\r');
	replaceChar(raw_message, "<CR>",  '\n');
	replaceChar(raw_message, "<BR>",  '\n');

    while (1) {
        const char * opentok = strcasestr(p, "<FONT");
        if (opentok) {
            if (opentok!=p) {
                strncpy(out, p, opentok-p);
                p = opentok+5;
            }
            const char * closetok = strchr(opentok, '>');
            if (closetok) {
                p = closetok+1;
            }
            const char * tok = strcasestr(p, "</FONT>");
            if (tok) {
                strncat(out, p, tok - p);
                p = tok+7;
            }
        } else {
            strcat(out, p);
            break;
        }
    }

    removeMarkup(out, "<B>");
    removeMarkup(out, "<I>");
    removeMarkup(out, "<U>");
    removeMarkup(out, "</B>");
    removeMarkup(out, "</I>");
    removeMarkup(out, "</U>");

	free(raw_message);
}

int MtgArchive::start_meeting()
{
    char tmpbuf[16];
    log_debug(ZONE, "Start meeting\n");
    sprintf(tmpbuf, "%d", m_status);
    save("recordingState", tmpbuf);
    return 0;
}

// bad name, it really just start data share
int MtgArchive::start_recording(int start_time)
{
    char tmpbuf[16];
    log_debug(ZONE, "Start data recording\n");
	if (m_local_drstart_time==0)
	{
		m_local_drstart_time = time(NULL);
        sprintf(tmpbuf, "%ld", m_local_drstart_time);
        save("drstart", tmpbuf);
        save("share_server", m_reflector.get_string());
        save("share_passwd", m_passwd.get_string());
	}
    m_status = m_enabled?RS_RECORDING:RS_PAUSE;
    if (!m_recordingStarted && m_status==RS_RECORDING) {
        m_recordingStarted = true;
        save("recordingStarted", "true");
    }

    sprintf(tmpbuf, "%d", m_status);
    save("recordingState", tmpbuf);

    save_array("spoolfiles", start_time);

    return 0;
}

int MtgArchive::pause_recording()
{
    log_debug(ZONE, "Pause recording\n");
    m_status = RS_PAUSE;
    char tmpbuf[16];
    sprintf(tmpbuf, "%d", m_status);
    save("recordingState", tmpbuf);
    return 0;
}

int MtgArchive::save_summary(const char * participants, int email_summary)
{
    if (MtgArchiver::s_parsed_summary_template == NULL)
    {
        log_error(ZONE, "No summary template set; no e-mail sent!");

        return 0;
    }

    IString email_body;

    int ii;
    for (ii = 0; MtgArchiver::s_parsed_summary_template[ii] != NULL; ii++)
    {
        gchar * frag = MtgArchiver::s_parsed_summary_template[ii];
        int tok = which_token(frag);
        switch (tok)
        {
          case MTGARC_TOKEN_NONE:
            email_body.append(frag);
            break;

          case MTGARC_TOKEN_MTGID:
            email_body.append(m_mtg_id);
            break;

          case MTGARC_TOKEN_START_DATE_TIME:
            {
                IString dtm;
                mk_email_start_time(&dtm);
                email_body.append(dtm);
            }
            break;
          case MTGARC_TOKEN_DURATION:
            {
                int dur = time(NULL) - m_local_start_time;
                int h = dur/3600;
                int m = (dur%3600) / 60;
                int s = dur%60;
                char buff[64];
                sprintf(buff, "%d:%02d:%02d", h, m, s);
                email_body.append(buff);
            }
            break;
          case MTGARC_TOKEN_VOICERECORDING:
            email_body.append((m_recordingStarted && m_local_vrstart_time>0)?"YES":"NO");
            break;
          case MTGARC_TOKEN_DATARECORDING:
            email_body.append((m_recordingStarted && m_local_drstart_time>0)?"YES":"NO");
            break;
          case MTGARC_TOKEN_PARTICIPANTS:
              if (participants)
              {
                email_body.append(participants);
              }
              break;

          default:
                log_error(ZONE, "Unhandled email token %d seen!", tok);
        }
    }

    save("summary", email_body.get_string());

    if (email_summary)
    {
      m_upload_summary = true;

      log_debug(ZONE, "meeting %s: sending summary to host", (const char *)m_mtg_id);

      call_send_email(m_host_email, _("Meeting summary"), email_body.get_string());
    }

    return 0;
}

int MtgArchive::stop_recording(const char * participants, int email_summary, int include_docs)
{
    log_debug(ZONE, "Stop recording\n");
    m_status = RS_STOPPED;
    char tmpbuf[16];
    sprintf(tmpbuf, "%d", m_status);
    save("recordingState", tmpbuf);

    m_include_docs = include_docs;
    sprintf(tmpbuf, "%d", include_docs);
    save("includeDocs", tmpbuf);

    if (participants)    // meeting summary
    {
        save_summary(participants, email_summary);
    }

    return 0;
}

// video.html
int MtgArchive::generate_download_html()
{
    IString movie = "video." + MtgArchiver::s_file_type;
    FILE* handle = fopen(MtgArchiver::s_video_html,"rb");
    if (handle) {
		IString opath= m_mtg_id;
		opath.append("/");
		opath.append(m_encrypt_folder);

		fseek(handle, 0, SEEK_END);
        int size = ftell(handle);
        fseek(handle, 0, SEEK_SET);
        char * htmlbuf = (char*)malloc(size+1);
        fread(htmlbuf, 1, size, handle);
        htmlbuf[size]=0;
        IString strHtml(htmlbuf);
        while (1) {
            int index = strHtml.find("$PATH$");
            if (index >=0) {
               strHtml.erase(index, 6);
               strHtml.insert(index, opath.get_string());
            } else {
                break;
            }
        }
        while (1) {
            int index = strHtml.find("$SWF$");
            if (index >=0) {
               strHtml.erase(index, 5);
               strHtml.insert(index, movie.get_string());
            } else {
                break;
            }
        }
        fclose(handle);

        IString html = m_full_path;
        html.append('/');
        html.append("video.html");
        handle = fopen(html.get_string(), "w+");
        if (handle) {
            fwrite(strHtml.get_string(), 1, strHtml.length(), handle);
            fclose(handle);
        }

        free(htmlbuf);

		log_debug(ZONE, "create html file %s", html.get_string());

		return 0;
    }

	log_error(ZONE, "failed to open template file %s", (const char*)MtgArchiver::s_video_html);
	return -1;
}

void MtgArchive::getmoviesize(int& w, int& h)
{
	IString val;
    restore("flash_width", &val);
    if (val.length() > 0)
    {
        w = atoi(val);
    }
    else
    {
        w = 1024;
    }

    restore("flash_height", &val);
    if (val.length() > 0)
    {
        h = atoi(val);
    }
    else
    {
        h = 768;
    }
}

// ShowMovie.html
int MtgArchive::generate_html()
{
    IString html;
    FILE * handle = fopen(MtgArchiver::s_swf_html_template,"rb");
    if (handle) {
        html = "video." + MtgArchiver::s_file_type;

        fseek(handle, 0, SEEK_END);
        int size = ftell(handle);
        fseek(handle, 0, SEEK_SET);
        char * htmlbuf = (char*)malloc(size+1);
        fread(htmlbuf, 1, size, handle);
        htmlbuf[size]=0;
        IString strHtml(htmlbuf);
        while (1) {
            int index = strHtml.find("$SWF$");
            if (index >=0) {
               strHtml.erase(index, 5);
               strHtml.insert(index, html.get_string());
            } else {
                break;
            }
        }
        int w, h;
        char temp[32];
        getmoviesize(w, h);
        if (w<=0 || w > 2048) w = 1024;
        if (h<=0 || h > 2048) h = 768;
        sprintf(temp, "%d", w);
        while (1) {
            int index = strHtml.find("$SWF_WIDTH$");
            if (index >=0) {
               strHtml.erase(index, 11);
               strHtml.insert(index, temp);
            } else {
                break;
            }
        }

        sprintf(temp, "%d", h);
        while (1) {
            int index = strHtml.find("$SWF_HEIGHT$");
            if (index >=0) {
               strHtml.erase(index, 12);
               strHtml.insert(index, temp);
            } else {
                break;
            }
        }

        fclose(handle);

        html = m_full_path;
        html.append('/');
        html.append("ShowMovie.html");
        handle = fopen(html.get_string(), "w+");
        if (handle) {
            fwrite(strHtml.get_string(), 1, strHtml.length(), handle);
            fclose(handle);
        }

        free(htmlbuf);
    }

    log_debug(ZONE, "create html file %s", html.get_string());

    return 0;
}

void MtgArchive::enable(int enabled)
{
    char tmpbuf[16];
    m_enabled = enabled;

	if (m_local_enable_time==0)
	{
		m_local_enable_time = time(NULL);
		sprintf(tmpbuf, "%ld", m_local_enable_time);
		save("enableStart", tmpbuf);
		if (m_local_vrstart_time==-1)
		{
			// this will be the time voice recording restared after reset
			m_local_vrstart_time = m_local_enable_time;
			save("vrstart", tmpbuf);
		}
	}

    if (m_enable_offset==0) {
      m_enable_offset = time(NULL) - m_local_start_time;
      sprintf(tmpbuf, "%d", m_enable_offset);
      save("recordingOffset", tmpbuf);
    }

    log_debug(ZONE, "Enable recording: %s\n", enabled?"True":"False");

    m_status = m_enabled?RS_RECORDING:RS_PAUSE;
    if (!m_recordingStarted && m_status==RS_RECORDING) {
      m_recordingStarted = true;
      save("recordingStarted", "true");
    }
    sprintf(tmpbuf, "%d", m_status);
    save("recordingState", tmpbuf);
}

int MtgArchive::generate_movie(bool preview)
{
    char buff[4096]={0};
    char address[256];
    strcpy(address, m_reflector);
    char* p = strchr(address, ':');
    if (p!=NULL)
    {
        *p = '\0';
        char* p1 = strchr(p+1, ':');
        if (p1!=NULL) {
          p = p1;
        }
        int port = atoi(p+1) + 2;	// TODO: port number maybe configurable

        // If we have an appshare key, add the cipher parameter
        const char *cipher = (m_appshare_key.length() != 0) ? "-cipher" : "";

        sprintf(buff, CONVERTER " -%s -stdlevel %d -loglevel %d -startrecording "
                "-meetingId %s -password %s -recordtime %d -frames %d -framerate %d %s %s "
                "-input %s/spoolfiles.txt -soundfile %s/audio-lin -out %s/video -container %s "
                "-format %s -listen %s:%d >/var/log/iic/vnc2mov.txt 2>&1",
				(preview?"preview":"replay"),
                0, MtgArchiver::s_vnc2swf_log_level, m_mtg_id.get_string(),
                m_passwd.get_string(), (int)get_adjusted_start(), MtgArchiver::s_frames_per_movie,
                MtgArchiver::s_frame_rate, cipher, (const char *) m_appshare_key,
                m_full_path.get_string(), m_full_path.get_string(), m_full_path.get_string(),
                MtgArchiver::s_file_type.get_string(), MtgArchiver::s_video_format.get_string(),
                address, port);
restart:
        int retry = 100;
        int ret = system(buff);
        if (WIFSIGNALED(ret) || WEXITSTATUS(ret) != 0)
		{
		    if (WEXITSTATUS(ret) == 2 && --retry > 0)
		    {
                // Magic code to restart
                log_debug(ZONE, "Restarting generation");
                goto restart;
		    }
		    log_error(ZONE, "Failed to generate movie: %s, return code=%d\n", buff, ret);
			return -1;
		}
    }
	else
	{
		log_error(ZONE, "No share server specified.\n");
		return -1;
	}
    log_debug(ZONE, "Generate movie: %s\n", buff);
	return 0;
}

/* calculate voice/data delay time

t0 -- enable recording
t1 -- voice started
t2 -- data started
          vrdelay  drdelay
t0 t1 t2   t1-t0    t2-t0	// add blank data frames & mute
t0 t2 t1   t1-t0    t2-t0	// insert mute voice & blank
---t0 t1 t2   0        t2-t1	// add blank data frames
---t0 t2 t1   t1-t2    0		// insert mute voice
t1 t0 t2   0        t2-t0	// add black data frames
t1 t2 t0   0        t2-t0	// don't need to skip anything, but make sure the gap between first & second gap is calc'd
t2 t0 t1   t1-t0    t2-t0	// insert mute voice, don't need to skip anything, but make sure the gap between first & second gap is calc'd
t2 t1 t0   0        t2-t0	// don't need to skip anything, but make sure the gap between first & second gap is calc'd
   t0 t2            0
   t2 t0			t2-t0
*/
int MtgArchive::get_delay_time(bool voice)
{
    int enable_time = get_adjusted_start();

	if (voice)
	{
		if (m_local_vrstart_time <= enable_time || m_local_drstart_time==0)
		{
			return 0;
		}
		else
		{
			return m_local_vrstart_time-enable_time;
		}
	}
	else
	{
		if ((m_local_vrstart_time == 0 && enable_time <= m_local_drstart_time))
			return 0;
		else
		{
			return m_local_drstart_time-enable_time;
		}
	}
}

// Figure out earliest component of recording (data or voice) and use that as the
// recording start time.
int MtgArchive::get_adjusted_start()
{
    int adjusted = m_local_enable_time;

    if (m_local_vrstart_time != 0 && m_local_vrstart_time != -1)
    {
        if (m_local_drstart_time != 0)
            adjusted = m_local_vrstart_time < m_local_drstart_time ? m_local_vrstart_time : m_local_drstart_time;
        else
            adjusted = m_local_vrstart_time;
    }
    else
    {
        if (m_local_drstart_time != 0)
            adjusted = m_local_drstart_time;
    }

    if (adjusted < m_local_enable_time)
        adjusted = m_local_enable_time;

    log_debug(ZONE, "Adjusted recording start offset: %d", adjusted - m_local_enable_time);
    return adjusted;
}

int MtgArchive::reset_recording()
{
	char buff[4096];
	lock();

	m_local_enable_time = 0;
    m_local_vrstart_time = -1;	// will be set to m_local_enable_time because conference object has already created
    m_local_drstart_time = 0;

    delete_file("enableStart");
	delete_file("vrstart");
    delete_file("drstart");

    char address[256];
    strcpy(address, m_reflector);
	// tell reflector to delete spool files
    char* p = strchr(address, ':');
    if (p!=NULL)
    {
        *p = '\0';
        char* p1 = strchr(p+1, ':');
        if (p1!=NULL) {
          p = p1;
        }
        int port = atoi(p+1) + 2;	// TODO: port number maybe configurable
        sprintf(buff, CONVERTER " -deletefile -stdlevel %d -loglevel %d "
                "-meetingId %s -password %s "
                "-input %s/spoolfiles.txt "
                "-listen %s:%d >/var/log/iic/vnc2mov.txt 2>&1",
                0, MtgArchiver::s_vnc2swf_log_level, m_mtg_id.get_string(),
                m_passwd.get_string(),
                m_full_path.get_string(),
				address, port);
        int ret = system(buff);
        if (WIFSIGNALED(ret) || WEXITSTATUS(ret) != 0)
        {
          log_error(ZONE, "Error executing: %s", buff);
        }
        log_debug(ZONE, "Delete remote spool: %s (%d)\n", buff, ret);
    }
    delete_file("spoolfiles");

	m_recordingStarted = false;
    delete_file("recordingStarted");

    m_nAdditionalStartTime = 0;
    delete_file("mtg_start_time");

	unlock();

	return 0;
}

int MtgArchive::add_retry_count()
{
	++m_retries;

	if (m_retries==1)
	{
		char buff[256];
		sprintf(buff, "We are having trouble processing the recording for meeting %s.  Please check mtgarchiver.log for more info.\n",
				  m_mtg_id.get_string());

        if (MtgArchiver::s_sys_admin_email.length() > 0)    // send to sysadmin
        {
            char * emails = strdup(MtgArchiver::s_sys_admin_email);
            char * tok = strtok(emails, "(, ;:)");
            char * p = emails;
            while (tok)
            {
                *tok = 0;

                if (strlen(p) > 2)    // at lease a@c
                {
                    call_send_email(p, _("Failed to process meeting recording"), buff);
                }

                p = tok + 1;
                if (*p == 0) break;
                tok = strtok(p, "(, ;:)");
            }

            if (p && strlen(p) > 2)  // send email to the last item in the email list
            {
                call_send_email(p, _("Failed to process meeting recording"), buff);
            }
        }
	}

	return m_retries;
}
