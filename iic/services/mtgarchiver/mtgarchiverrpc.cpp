/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifdef WIN32
#pragma warning(disable:4786)
#endif

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <locale.h>
#include <libintl.h>
#include "mtgarchiver.h"
#include "../common/common.h"
#include "../../jcomponent/util/log.h"
#include "../../jcomponent/rpc.h"
#include "../../jcomponent/util/util.h"
#include "../../jcomponent/jcomponent_ext.h"
#include "../../jcomponent/util/rpcresult.h"
#include "../../util/filetransfer/transfer.h"

#define JCOMPNAME "mtgarchiver"

int set_config(config_t conf);

static MtgArchiver s_archiver;

// for resets and restarts
static IString s_service_name;
static time_t  s_epoch = time(NULL);
static time_t  s_controller_epoch = 0;

int mtgarchiver_init(config_t conf)
{
	log_status(ZONE, "Meeting archiver service starting");

	// Read configuration
	int status = set_config(conf);
	if (status)
		return status;

    if (s_archiver.check_spooling() < 0)
        return -1;

    const char *zonlang = getenv("ZONLANG");
    if (zonlang == NULL)
        zonlang = "";	// use system default

    log_debug(ZONE, "Setting locale to %s", zonlang);

    // initialize locale and timezone for display purposes (email generation)
    setlocale( LC_ALL, zonlang );
    tzset();

	bindtextdomain(JCOMPNAME, "/opt/iic/locale");
    textdomain(JCOMPNAME);
    char    *pBTC = bind_textdomain_codeset(JCOMPNAME, "UTF-8");
//    log_debug(ZONE, "bind_textdomain_codeset( ) returned %s", pBTC);

    return 0;
}

int set_config(config_t conf)
{
    s_service_name = config_get_one(conf, JCOMPNAME, "init.service", 0);
    s_archiver.set_spooling_root(config_get_one(conf, JCOMPNAME, "spooling-root", 0));
    s_archiver.set_mtg_repository_url(config_get_one(conf, JCOMPNAME, "mtg-repository-url", 0));
    s_archiver.set_docshare_repository_url(config_get_one(conf, JCOMPNAME, "docshare-repository-url", 0));
    int n_svc = config_count(conf, JCOMPNAME, "map.service.name");
    for (int i = 0; i < n_svc; i++)
    {
        s_archiver.add_service(config_get_one(conf, JCOMPNAME, "map.service.name", i),
                               config_get_one(conf, JCOMPNAME, "map.service.host", i));
    }
    s_archiver.set_vbridge_xfer_port(atoi(config_get_one(conf, JCOMPNAME, "vbridge-xfer-port", 0)));
    s_archiver.set_archive_name_key(config_get_one(conf, JCOMPNAME, "archive-name-key", 0));
    s_archiver.set_email_from(config_get_one(conf, JCOMPNAME, "email-from", 0));
    s_archiver.set_email_from_display(config_get_one(conf, JCOMPNAME, "email-from-display", 0));
    s_archiver.set_email_subject(config_get_one(conf, JCOMPNAME, "email-subject", 0));
    s_archiver.set_email_template(config_get_one(conf, JCOMPNAME, "email-template", 0));
    s_archiver.set_mtg_repository_portal(config_get_one(conf, JCOMPNAME, "mtg-repository-portal", 0));
    s_archiver.set_dummy_mp3_file(config_get_one(conf, JCOMPNAME, "dummy-mp3-file", 0));
    s_archiver.set_swf_html_template(config_get_one(conf, JCOMPNAME, "swf-html-template", 0));
    s_archiver.set_video_html(config_get_one(conf, JCOMPNAME, "video-html", 0));

    s_archiver.set_frames_per_movie(atoi(config_get_one(conf, JCOMPNAME, "frames-per-movie", 0)));
    s_archiver.set_vnc2swf_log_level(atoi(config_get_one(conf, JCOMPNAME, "vnc2swf-log-level", 0)));
    s_archiver.set_batch_threads(atoi(config_get_one(conf, JCOMPNAME, "batch-threads", 0)));
    s_archiver.set_keep_raw_data(atoi(config_get_one(conf, JCOMPNAME, "keep-raw-data", 0)));
    s_archiver.set_summary_template(config_get_one(conf, JCOMPNAME, "meeting-summary-template", 0));
    s_archiver.set_has_voice(strcmp(config_get_one(conf, JCOMPNAME, "voice-provider", 0), "stub")!=0);
    s_archiver.set_sys_admin_email(config_get_one(conf, JCOMPNAME, "sys-admin-email", 0));

    char *ttt = config_get_one(conf, JCOMPNAME, "max-retries", 0);
    if (ttt != NULL)
       MtgArchiver::s_max_retries = atoi(ttt);

    ttt = config_get_one(conf, JCOMPNAME, "retry-wait", 0);
    if (ttt != NULL)
       MtgArchive::s_retry_wait = atoi(ttt);

    ttt = config_get_one(conf, JCOMPNAME, "socket-timeout", 0);
    if (ttt != NULL)
       MtgArchive::s_socket_timeout = atoi(ttt);

	const char * share_lite = config_get_one(conf, JCOMPNAME, "share-lite", 0);
	if (share_lite != NULL)
	    s_archiver.set_share_lite(atoi(share_lite));

	const char * sampl_size = config_get_one(conf, JCOMPNAME, "sample-size", 0);
	if (sampl_size != NULL)
	    s_archiver.set_sample_size(atoi(sampl_size));

	const char * audio_format = config_get_one(conf, JCOMPNAME, "audio-format", 0);
	if (audio_format != NULL)
	    s_archiver.set_audio_format(atoi(audio_format));

    ttt = config_get_one(conf, JCOMPNAME, "frame-rate", 0);
    if (ttt != NULL)
        s_archiver.set_frame_rate(atoi(ttt));

    ttt = config_get_one(conf, JCOMPNAME, "video-bit-rate", 0);
    if (ttt != NULL)
        s_archiver.set_video_bit_rate(atoi(ttt));

    ttt = config_get_one(conf, JCOMPNAME, "video-format", 0);
    if (ttt != NULL)
        s_archiver.set_video_format(ttt);

    ttt = config_get_one(conf, JCOMPNAME, "file-type", 0);
    if (ttt != NULL)
        s_archiver.set_file_type(ttt);

    ttt = config_get_one(conf, JCOMPNAME, "post-process-command", 0);
    if (ttt != NULL)
        s_archiver.set_post_command(ttt);

    return Ok;
}

xmlrpc_value *register_meeting(xmlrpc_env *env,
                               xmlrpc_value *param_array,
                               void *user_data)
{
    char *meeting_id;
    int start_time;
    char *host_email;
    char *voice_service;
    char *reflector_host;
    char *password;
    int options;
    char *key;

    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(sissssis)",
                       &meeting_id,
                       &start_time,
                       &host_email,
                       &voice_service,
                       &reflector_host,
                       &password,
                       &options,
                       &key);
    XMLRPC_FAIL_IF_FAULT(env);

    // log result
    log_debug(ZONE, "Register meeting %s", meeting_id);

    status = s_archiver.register_meeting(meeting_id,
                                         start_time,
                                         host_email,
                                         voice_service,
                                         reflector_host,
                                         password,
                                         options, key);
    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Register meeting failure");

    return output;
}

// Enable recording
// User has enabled/disabled recording for meeting.
xmlrpc_value *enable_recording(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *meeting_id;
    int enabled;

    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(si)",
                       &meeting_id,
                       &enabled);
    XMLRPC_FAIL_IF_FAULT(env);

    // log result
    log_debug(ZONE, "Enable recording %s", meeting_id);

   status = s_archiver.enable_recording(meeting_id, enabled);

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Enable recording failure");

    return output;
}

// Start recording
// A share session has started for this meeting, may need recording.
xmlrpc_value *start_recording(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *meeting_id;
    int start_time;

    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(si)", &meeting_id, &start_time);
    XMLRPC_FAIL_IF_FAULT(env);

    // log result
    log_debug(ZONE, "Start data recording %s", meeting_id);

    status = s_archiver.start_recording(meeting_id, start_time);

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Start recording failure");

    return output;
}

xmlrpc_value *set_voice_recording_delay(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *meeting_id;
    int delay;

    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(si)",
                       &meeting_id,
                       &delay
                       );
    XMLRPC_FAIL_IF_FAULT(env);

    // log result
    log_debug(ZONE, "set_voice_recording_delay %s", meeting_id);

   status = s_archiver.set_voice_recording_delay(meeting_id, delay);

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "set_voice_recording_delay failure");

    return output;
}

// Pause recording (share session has stopped)
// Parameters:
//   MEETING ID
// Returns:
//   SUCCESS_CODE: 0 = ok, otherwise failure code
xmlrpc_value *pause_recording(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *meeting_id;

    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(s)",
                       &meeting_id
                       );
    XMLRPC_FAIL_IF_FAULT(env);

    // log result
    log_debug(ZONE, "Pause recording %s", meeting_id);

	status = s_archiver.pause_recording(meeting_id);

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Pause recording failure");

    return output;
}

// Stop recording (meeting has ended)
// Parameters:
//   MEETING ID
// Returns:
//   SUCCESS_CODE: 0 = ok, otherwise failure code
xmlrpc_value *stop_recording(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *meeting_id, *summary;
    int email_summary, include_docs;

    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ssii)",
                       &meeting_id, &summary, &email_summary, &include_docs
                       );
    XMLRPC_FAIL_IF_FAULT(env);

    // log result
    log_debug(ZONE, "Stop recording %s", meeting_id);

	status = s_archiver.stop_recording(meeting_id, summary, email_summary, include_docs);

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Stop recording failure");

    return output;
}

// On receiving chat message
// Parameters:
//   MEETING ID, MSG, FROM, TO
// Returns:
//   SUCCESS_CODE: 0 = ok, otherwise failure code
xmlrpc_value *on_chat(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *meeting_id, *msg,  *from_participantid, *to_participantid;

    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ssss)",
                       &meeting_id,
                       &msg,
                       &from_participantid,
                       &to_participantid
                       );
    XMLRPC_FAIL_IF_FAULT(env);

    // log result
    log_debug(ZONE, "On receiving chat message: %s-%s-%s-%s", meeting_id, msg,
                from_participantid, to_participantid);

    status = s_archiver.on_chat(meeting_id, msg, from_participantid, to_participantid);

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Failed to process chat message.");

    return output;
}

// External API call to get meeting paths
// Parameters:
//   SESSIONID, MEETINGID, FIRST, CHUNKSIZE
// Returns:
//   Array of rows w:
//      MEETINGID, MEETINGDATETIME, ARCHIVEPATH
//   followed by
//      FIRST, CHUNKSIZE, TOTALROWS, MOREAVAILABLE
xmlrpc_value *get_archives(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *sid;
    char *mtgid;
    char *first;
    char *chksz;

    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ssss)", &sid, &mtgid, &first, &chksz);
    XMLRPC_FAIL_IF_FAULT(env);

    // log result
    log_debug(ZONE, "get_archives(%s, %s, %s, %s)", sid, mtgid, first, chksz);

    status = s_archiver.get_archives(mtgid, atoi(first), atoi(chksz), env, &output);
    if (status != Ok)
    {
        xmlrpc_env_set_fault(env, status, "Unable to get archive chunk");
        goto cleanup;
    }


    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Failed to process chat message.");

    return output;
}

extern "C" int JC_connected();

extern "C" int JC_connected()
{
    char buff[128];
    char idbuff[128];
    static int reg = 0;

    sprintf(idbuff, "regsvc:%d", reg++);
    if (s_archiver.is_initialized())
    {
        sprintf(buff, "mtgarc:%s", (const char *)s_service_name);
        rpc_make_call(idbuff, "controller",
                      "controller.register_service", "(si)", buff, s_epoch);
    }
    else
    {
        log_error(ZONE, "mtg archiver not initialized; not registering w/ controller");
    }

    return 0;
}

// Reset
// Parameters:
//   SECRET, CONTROLLEREPOCH
// Returns:
//   0
xmlrpc_value *reset(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char * secret;
    int new_epoch;
    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(si)", &secret, &new_epoch);
    if (env->fault_occurred)
    {
        xmlrpc_env_set_fault(env, ParameterParseError, "Unable to parse arguments to mtgarchiver.reset");
        goto cleanup;
    }

	log_status(ZONE, "Mtgarchiver service reset requested (%d)", new_epoch);

	// Get secret from config
	if (!strcmp(secret, "zQQz98"))
	{
		if (new_epoch != s_controller_epoch)
		{
			log_status(ZONE, "Resetting service");
            		status = s_archiver.reset();
			s_controller_epoch = s_epoch;

			// Like a reset, so set epoch
			s_epoch = time(NULL);
			JC_connected();
		}
	}
	else
		status = Failed;

    // Create return value.
	if (status == Ok)
	{
		output = xmlrpc_build_value(env, "(i)", 0);
		XMLRPC_FAIL_IF_FAULT(env);
	}
	else
	{
		xmlrpc_env_set_fault(env, status, "Unable to reset mtgarchiver service");
	}

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "Unable to reset mtgarchiver");

    return output;
}

// Preview Recording
// Parameters:
//   MEETING ID
// Returns:
//   0
xmlrpc_value *preview_recording(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *meeting_id, *email;

    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(ss)", &meeting_id, &email);
    XMLRPC_FAIL_IF_FAULT(env);

    // log result
    log_debug(ZONE, "preview_recording %s", meeting_id);

   status = s_archiver.preview_recording(meeting_id, email);

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "preview_recording failure");

    return output;
}

// Reset Recording
// Parameters:
//   MEETING ID
// Returns:
//   0
xmlrpc_value *reset_recording(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *meeting_id;

    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(s)", &meeting_id);
    XMLRPC_FAIL_IF_FAULT(env);

    // log result
    log_debug(ZONE, "reset_recording %s", meeting_id);

   status = s_archiver.reset_recording(meeting_id);

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "reset_recording failure");

    return output;
}

// Set Recording Audio
// Parameters:
//   MEETING ID, URL, FORMAT
// Returns:
//   0
xmlrpc_value *set_recording_audio(xmlrpc_env *env, xmlrpc_value *param_array, void *user_data)
{
    char *meeting_id, *url, *format;

    xmlrpc_value *output = NULL;
    int status = Ok;

    xmlrpc_parse_value(env, param_array, "(sss)", &meeting_id, &url, &format);
    XMLRPC_FAIL_IF_FAULT(env);

    // log result
    log_debug(ZONE, "set_recording_audio %s: %s", meeting_id, url);

    status = s_archiver.set_recording_audio(meeting_id, url, format);

    // Create return value.
    output = xmlrpc_build_value(env, "(i)", status);
    XMLRPC_FAIL_IF_FAULT(env);

cleanup:
    if (!env->fault_occurred && output == NULL)
        xmlrpc_env_set_fault(env, Failed, "set_recording_audio failure");

    return output;
}

extern "C" void RPC_error(const char *id, xmlrpc_int32 fault_code, const char * message)
{
    log_error(ZONE, "*** Error processing RPC request: %s\n", message);

	const char * end = strchr(id, ':');
	if (end == NULL)
    {
        log_error(ZONE, "RPC_error w/ id w/o colon '%s'", id);

		return;
    }

//	int len = end - id;
}


extern "C" void RPC_result(const char * id, xmlrpc_value * resval)
{
	xmlrpc_env env;

    	xmlrpc_env_init(&env);

	const char * end = strchr(id, ':');
	if (end == NULL)
	{
		log_error(ZONE, "RPC_result w/ id w/o colon '%s'", id);

		return;
	}

	int len = end - id;

	if (!strncmp(id, "regsvc", len))
	{
		RPCResultSet rs(&env, resval);
		XMLRPC_FAIL_IF_FAULT(&env);

		s_archiver.service_registered();

		log_debug(ZONE, "Service registered w/ controller");
	}

cleanup:
    if (env.fault_occurred)
    {
        char buffer[512];
        strcpy(buffer, "Result error: ");
        if (env.fault_string)
            strncat(buffer, env.fault_string, 512 - 16);
        else
            strcat(buffer, "unknown cause");
        RPC_error(id, Failed, buffer);
    }
    xmlrpc_env_clean(&env);
}

/* Initialization as jabber component--call mailer_init() if using library
   directly. */
int main(int argc, char *argv[])
{
    fprintf(stderr,
            "IIC Meeting Archiver Server 1.0   Copyright (C) 2003 imidio.com\n\n");

    signal( SIGPIPE, SIG_IGN);  // should be fine to ignore it?

    int instance = 0;
    if (argc > 1)
        instance = atoi(argv[1]);

    const char *config_file = "/opt/iic/conf/config.xml";
    if (argc > 2)
    {
        config_file = argv[2];
    }

    config_t conf = config_new();
    config_load(conf, JCOMPNAME, instance, config_file);

	if (jc_init(conf, JCOMPNAME))
	{
		log_error(ZONE, "Unable to initialize controller\n");
		return -1;
	}

    if (mtgarchiver_init(conf))
        return -1;

	jc_set_connected_callback(JC_connected);
	rpc_set_callbacks(RPC_result, RPC_error, NULL);

	rpc_add_method("mtgarchiver.enable_recording", &enable_recording, NULL);
	rpc_add_method("mtgarchiver.register_meeting", &register_meeting, NULL);
	rpc_add_method("mtgarchiver.start_recording", &start_recording, NULL);
	rpc_add_method("mtgarchiver.pause_recording", &pause_recording, NULL);
	rpc_add_method("mtgarchiver.stop_recording", &stop_recording, NULL);
	rpc_add_method("mtgarchiver.on_chat", &on_chat, NULL);
	rpc_add_method("mtgarchiver.set_voice_recording_delay", &set_voice_recording_delay, NULL);

	rpc_add_method("mtgarchiver.get_archives", &get_archives, NULL);

	// Called on mtg controller restarts
	rpc_add_method("mtgarchiver.reset", &reset, NULL);

	rpc_add_method("mtgarchiver.preview_recording", &preview_recording, NULL);
	rpc_add_method("mtgarchiver.reset_recording", &reset_recording, NULL);
	rpc_add_method("mtgarchiver.set_recording_audio", &set_recording_audio, NULL);

	jc_run(false);

    return 0;
}
