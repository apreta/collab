/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef __MTGARCHIVER_H__
#define __MTGARCHIVER_H__

#include "../../util/istring.h"
#include "../../util/irefobject.h"
#include "../../util/ithread.h"
#include "../../jcomponent/util/log.h"

#include <openssl/des.h>
#include <xmlrpc-c/base.h>
#include <xmlrpc-c/server.h>

#include <dirent.h>
#include <unistd.h>
#include <time.h>

#define MAX_BRIDGE 10  // why not?

#define MTGARC_TOKEN_MTGID_STR			"MeetingID"
#define MTGARC_TOKEN_START_DATE_TIME_STR	"StartDateTime"
#define MTGARC_TOKEN_PORTAL_URL_STR		"PortalURL"
#define MTGARC_TOKEN_DURATION_STR		"Duration"
#define MTGARC_TOKEN_VOICERECORDING_STR	"VoiceRecording"
#define MTGARC_TOKEN_DATARECORDING_STR	"DataRecording"
#define MTGARC_TOKEN_PARTICIPANTS_STR	"MeetingParticipants"

#define TIMEOUT 10
#define MAX_FAILURE 20

class MtgArchive : public IReferencableObject
{
    IMutex m_ref_lock;

    // Meeting id of the meeting
    IString m_mtg_id;
	IString m_appshare_key;

    // Start time of the meeting
    time_t  m_mtg_start_time;
    time_t  m_local_start_time;
    time_t  m_local_enable_time;
    time_t  m_local_vrstart_time;
    time_t  m_local_drstart_time;
    int     m_enable_offset;

    int     m_nAdditionalStartTime;
    int     m_AdditionalStartTime[MAX_FAILURE];

    // E-mail address of the host of the meeting
    IString m_host_email;

	// Voice recording download location (share-lite)
	IString m_recording_url;
	IString m_recording_format;

    // The host of the voice bridge for this meeting.  The voice recording
    // for the meeting is fetched from here via a socket connect.
    IString m_vbridge_host;

    // The spool folder of this archive.  Of the form <MTGID>-<YR>-<MO>-<DAY>-<HR>-<MIN>-<SEC>
    IString m_folder;

    // Encrypted value of m_folder encoded into base64 for displayability
    IString m_encrypt_folder;

    // Include shared documents in archive
    int m_include_docs;

    // The current state of the archive.  See ARC_STATE enum below
    int m_state;

    // The full path of the archive in the spool folder
    //    i.e. <MtgArchiver::s_spooling_root>/<state>/<MTGID>-<YR>-<MO>-<DAY>-<HR>-<MIN>-<SEC>
    IString m_full_path;

    // Creates a wav file w/ header info for the recorded audio
    FILE * create_wav_file(size_t data_sz);

    // change the file size in the wave header
    void increase_file_size(FILE* wavfp, int size);

    // Creates a full buffer of data from the socket.  Will block until done.
    int recv_full(int sock, char * buf, size_t len, int flags);

    // Move from the current state to the given next state.  Involves moving the folder
    // to the appropriate spool directory
    int move(int next_state);

    // Create a folder in the current spool directory to hold
    // the saved components of the meeting archive
    int create_archive_folder();

    // Handle externally generated audio files by downloading and mergining into a
    // single wave file.
    int merge_external_audio(FILE **wavfp, const char * filename, time_t file_time, time_t & prev_time);
    int fetch_external_recording();

    // Contact the voice bridge and ask for the recorded audio
    int fetch_mtg_recording();

    // Convert the recorded audio into an MP3 file
    int convert(int index);

	// split wave file into 2.5 hour a piece and convert
	int splitAndConvert();

    // Copy/convert shared documents
    int generate_pdf(IString& ipath, const char* doc_name);
    int package_docs();

    // Upload the archive to the web repository
    int upload_archive();

    // Send e-mail to the host of the meeting that the meeting archive is ready
    int send_email(bool preview);
	int call_send_email(const char * to_email, const char* subj, const char* body);

    int save_summary(const char * participants, int email_summary);    // save meeting summary & send out an email

    // Remove any work products in the current spool directory
    int cleanup();

    int make_dir(const char *dir, int mode);

    // Convert the start time into a string to insert in an e-mail message
    void mk_email_start_time(IString * out_str);

    // Convert the meeting id and encrypted folder name into an URL that
    // is valid to the web repository.
    void mk_portal_url(IString * dtm);

    // Returns either MTGARC_TOKEN_NONE if the given arg is *NOT* one of the
    // defined replacement tokens in the e-mail or one of the values in the
    // enum below if it is.
    int which_token(gchar * arg);

    enum
        {
            MTGARC_TOKEN_NONE,
            MTGARC_TOKEN_MTGID,
            MTGARC_TOKEN_START_DATE_TIME,
            MTGARC_TOKEN_PORTAL_URL,
            MTGARC_TOKEN_DURATION,
            MTGARC_TOKEN_VOICERECORDING,
            MTGARC_TOKEN_DATARECORDING,
            MTGARC_TOKEN_PARTICIPANTS
        };

    int copy_files(const char *outfile,
                   const char *infile);

    int save(const char * attribute,
             const char * value);

    int restore(const char * attribute,
                IString * value);

    int save_array(const char *attribute, int value);
    int restore_array(const char *attribute, int * array, int * num, int max_num);

    // the following are used by flash recording
    IThread* m_recording_thread;
    int m_started;
    enum {RS_NONE, RS_RECORDING, RS_STOPPED, RS_PAUSE, RS_SOUNDREADY, RS_CHAT, RS_DROP};
    IString m_reflector;
    IString m_passwd;
    int m_status;
    int m_enabled;
    bool m_recordingStarted;
	int m_retries;
	int m_nTotalWaveSize;

    bool m_upload_summary;  // decide if need to upload the archive if meeting is not recorded

  public:
    static int s_retry_wait;
    static int s_socket_timeout;
    enum
        {
            ARC_STATE_ACTIVE=0,		// meeting is in progress on the mtg controller
            ARC_STATE_CONVERTING,	// meeting has ended and conversion needs to be done
            ARC_STATE_DONE,			// nothing left to do but clean up
            NUM_ARC_STATE
        };
    static const char *s_arc_state_str[];	// string values for the enum values above

    MtgArchive(const char *meeting_id, time_t start, const char* reflector, const char * password);
    MtgArchive(int initial_state,
               const char * full_path,
               const char * folder);
    ~MtgArchive();

    enum
        { MY_TYPE = 4985 };

    int set_mtg_id(const char * id);
    const char * get_mtg_id() const
        { return m_mtg_id; }

    int set_mtg_start_time(time_t t);
    time_t get_mtg_start_time() const
        { return m_mtg_start_time; }

    const char * get_folder() const
        {
            return m_folder;
        }


    int set_vbridge_host(const char * hostname);
    int set_voice_recording_delay(int delay);
	int set_recording_audio(const char * url, const char * format);
    int set_host_email(const char * addr);

	int add_retry_count();

    int log_chat_message(const char *message, const char *from, const char *to);

    int  perform_op(bool preview=false);	// perform current state's operation
    bool next_state();  // move archive to next state
	bool prev_state();  // move archive to prev state

    // IObject obligations
    int type()
        { return MY_TYPE; }
	unsigned int hash()
        { return m_mtg_id.hash() ^ (unsigned int)m_mtg_start_time; }
	bool equals(IObject *obj)
        {
            bool ret = ((obj->type() == MY_TYPE) &&
                        (strcmp(m_mtg_id, ((MtgArchive *)obj)->get_mtg_id()) == 0) &&
                        (((MtgArchive *)obj)->get_mtg_start_time() == m_mtg_start_time));

            log_debug(ZONE, "mtgarchive equals: %s %s %s",
                      (const char *)m_folder, (ret ? "==" : "!="),
                      (const char *)(((MtgArchive *)obj)->get_folder()));

            return ret;
        }
    void lock()
        {
            m_ref_lock.lock();
        }
    void unlock()
        {
            m_ref_lock.unlock();
        }

    void reference()
        {
            IReferencableObject::reference();
            log_debug(ZONE, "%s referenced count %d",
                      (const char *)m_folder, IReferencableObject::ref_count);
        }

    void dereference()
        {
            log_debug(ZONE, "%s dereferenced count %d",
                      (const char *)m_folder, IReferencableObject::ref_count);
            IReferencableObject::dereference();
        }

    // flash recording ops
    int start_meeting();
    int start_recording(int start_time);
    int pause_recording();
    int stop_recording(const char* participants, int email_summary, int include_docs);
    bool check_alive(int process);
    void enable(int enabled);
    int generate_html();
	int generate_download_html();
    void sendCmd(const char * meeting_id, int cmd, const char * data, int* pid);
    void sendCmdWithChat(const char * meeting_id, int cmd,
                const char * message, const char * from_participant, int * pid);
    int generate_movie(bool preview);
    void set_reflector(const char* reflector, const char* passwd) {
      m_reflector = reflector;
      m_passwd = passwd;
    }
	int get_delay_time(bool voice);
	int get_adjusted_start();
	int reset_recording();
	int delete_file(const char *attribute);

	void set_appshare_key(const char * key) { m_appshare_key = key; save("appshare_key", key); }

	void getmoviesize(int& w, int& h);

	int sample_count();
};

class MtgArchiver
{
    friend class MtgArchive;

    IMutex m_lock;

    static int            s_share_lite;
    static int            s_sample_size;
    static int            s_audio_format;
    static IString        s_spooling_root;
    static IString        s_mtg_repository_url;
    static IString        s_docshare_repository_url;
    static unsigned short s_vbridge_xfer_port;
    static DES_key_schedule s_keysched;

    static IString        s_email_from;
    static IString        s_email_from_display;
    static IString        s_email_subject;
    static IString        s_email_template;
    static time_t         s_email_template_modified;
    static gchar **       s_parsed_email_template;
    static IString        s_mtg_repository_portal;
    static IString        s_dummy_mp3_file;
    static IString        s_swf_html_template;
    static IString        s_video_html;
    static int            s_frames_per_movie;
    static int            s_frame_rate;
    static int            s_video_bit_rate;
    static IString        s_video_format;
    static IString        s_file_type;
    static int            s_vnc2swf_log_level;
    static int            s_batch_threads;
    static int            s_keep_raw_data;
    static IString        s_summary_template;
    static time_t         s_summary_template_modified;
    static gchar **       s_parsed_summary_template;
    static int            s_has_voice;     // if it is voice stub, don't bother to check the error when retrieving voice
    static IString        s_sys_admin_email;    // send failed message to sysadmin only
    static IString        s_post_command;

    IStringHash m_active_mtg;	// meetingid --> MtgArchive for all active meetings
    IList m_restored_active;

    static IThreadPool * m_tpool;        // workers to process archives that are no longer
                                // associated with an active meeting
    static IThreadPool * m_tpool_preview;        // workers to process archives for preview

    IString m_vbridge_service[MAX_BRIDGE];
    IString m_vbridge_host[MAX_BRIDGE];
    int m_num_vbridge;

    bool m_initialized;

    static int check_exists(const char * filename);

    DIR * get_first(const char * mtgid, int pos);
    DIR * next_mtg_info(DIR * dir, IString * mtgid, IString * path);
    int   find_remaining(DIR * dir);

    static int update_email_template();
    static int refresh_email_template();
    static int update_summary_template();
    static int refresh_summary_template();
    static void queue_archive_func(IObject * key, IObject * value, void * user);
  public:
    static int s_max_retries;

    static void process_archive(void * init_data, void * instance_data);
    static void preview_archive(void * init_data, void * instance_data);
    static void encrypt(IString * b64ciphertxt, const char * cleartxt);
    static int  decrypt(IString * cleartxt,     const char * b64ciphertxt);

    MtgArchiver()
        {
            IThread::initialize(); // ok since a singleton class
            m_tpool = new IThreadPool(&process_archive, NULL, s_batch_threads);
            m_tpool_preview = new IThreadPool(&preview_archive, NULL, s_batch_threads);
            m_num_vbridge = 0;
            m_initialized = false;
        };
    ~MtgArchiver()
        {
            log_debug(ZONE, "destroying MtgArchiver");
            delete m_tpool;
            delete m_tpool_preview;
        }

    // called in response to a controller restart
    int reset();

    // Restore any archives that are in a partially processed state
    // in a spool directory.  Re-start processing any partially
    // processed star.
    int restore_archives(int state, const char * path);

    // See if the spool root exists and the state directories exist.
    // If they do, check to see if they contains any unfishished meeting archives.
    // Put unfinished meeting archive into either the active list or restart conversion
    // processing.
    int check_spooling();
    bool is_initialized()
        {
            return m_initialized;
        }

    void service_registered();

    static void set_share_lite(int flag)
        { s_share_lite = flag; }

    static int is_share_lite()
        {
            return s_share_lite;
        }

    static void set_sample_size(int ss)
        { s_sample_size = ss; }

    static int get_sample_size()
        {
            return s_sample_size;
        }

    static void set_audio_format(int fmt)
        { s_audio_format = fmt; }

    static int get_audio_format()
        {
            return s_audio_format;
        }

    static void set_spooling_root(const char *arg)
        { s_spooling_root = arg; }

    static void set_mtg_repository_url(const char *arg)
        { s_mtg_repository_url = arg; }

    static void set_docshare_repository_url(const char *arg)
        { s_docshare_repository_url = arg; }

    static void set_vbridge_xfer_port(unsigned short arg)
        { s_vbridge_xfer_port = arg; }

    static void set_email_from(const char * arg)
        { s_email_from = arg; }

    static void set_email_from_display(const char * arg)
        { s_email_from_display = arg; }

    static void set_email_subject(const char * arg)
        { s_email_subject = arg; }

    static void set_email_template(const char * arg)
        { s_email_template = arg; }

    static void set_summary_template(const char * arg)
        { s_summary_template = arg; }

    static void set_has_voice(int arg)
        { s_has_voice = arg; }

    static void set_sys_admin_email(const char * arg)
        { s_sys_admin_email = arg; }

    static void set_mtg_repository_portal(const char * arg)
        { s_mtg_repository_portal = arg; }

    static void set_dummy_mp3_file(const char * arg)
        { s_dummy_mp3_file = arg; }

    static void set_swf_html_template(const char * arg)
        { s_swf_html_template = arg; }

    static void set_video_html(const char * arg)
        { s_video_html = arg; }

    static void set_frames_per_movie(int arg)
        { s_frames_per_movie = arg; }

    static void set_frame_rate(int arg)
        { s_frame_rate = arg; }

    static void set_video_bit_rate(int arg)
        { s_video_bit_rate = arg; }

    static void set_video_format(const char * arg)
        { s_video_format = arg; }

    static void set_file_type(const char * arg)
        { s_file_type = arg; }

    static void set_post_command(const char * arg)
        { s_post_command = arg; }

    static void set_vnc2swf_log_level(int arg)
        { s_vnc2swf_log_level = arg; }

    static void set_batch_threads(int arg)
        { s_batch_threads = arg; }

	static void set_keep_raw_data(int arg)
        { s_keep_raw_data = arg; }

    void add_service(const char * service_name, const char * service_host)
        {
            if (m_num_vbridge == MAX_BRIDGE)
            {
                log_error(ZONE, "Too many bridges; increase MAX_BRIDGE");
            }

            m_vbridge_service[m_num_vbridge] = service_name;
            m_vbridge_host[m_num_vbridge] = service_host;
            m_num_vbridge++;
        }

    int register_meeting(const char *meeting_id,
                         int         start_time,
                         const char *host_email,
                         const char *voice_service,
                         const char *reflector_host,
                         const char *password,
                         int options,
                         const char * key);

    // may need add_participant() for ad-hoc deallies sometime
    int set_voice_recording_delay(const char *meeting_id,
                                  int delay);

	// Set URL from which to download audio for this meeting (share-lite)
	int set_recording_audio(const char *meetingid, const char *url, const char *format);

	// produce a temp movie file for reviewing
    int preview_recording(const char *meeting_id, const char* email);

	// wipe out current recording
    int reset_recording(const char *meeting_id);

    // Enable/disable recording
    int enable_recording(const char *meeting_id, int enabled);

    // Stop data share
    int pause_recording(const char *meeting_id);

    // Start data share
    int start_recording(const char *meeting_id, int start_time);

    // Called for each meeting chat message so it can be archived
    int on_chat(const char *meeting_id, const char *message,
                const char *from, const char *to);

    // Called when the meeting ends
    int stop_recording(const char *meeting_id, const char * participants, int email_summary, int include_docs);

    // Implements the external API call to get the meeting archives for
    // a given meeting id.
    int get_archives(const char * meeting_id,
                     int first,
                     int chunksize,
                     xmlrpc_env * env,
                     xmlrpc_value **output);

    // Set the base64 encoded 64-bit DES key for encrypting the meeting
    // repository directory name.  Make them hard to guess (web server
    // is configured to not give directory listings).
    void set_archive_name_key(const char * keystr);

};

inline int  MtgArchive::sample_count() { return 8000 * MtgArchiver::s_sample_size; }

#endif // __MTGARCHIVER_H__
