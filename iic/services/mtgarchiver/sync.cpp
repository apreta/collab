/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

/*
 * shm.cpp - code to provide shared memory access
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include "sync.h"
#include "../../jcomponent/util/log.h"

bool opensem(int *sid, key_t key)
{
        /* Open the semaphore set - do not create! */
        if((*sid = semget(key, 0, 0666)) == -1) 
        {
                log_error(ZONE, "Semaphore set does not exist!\n");
                return false;
        }
        
        return true;
}

bool createsem(int *sid, key_t key)
{
        if((*sid = semget(key, 1, IPC_CREAT|IPC_EXCL|0666)) == -1) 
        {
                log_debug(ZONE, "Semaphore set already exists!\n");
		        if((*sid = semget(key, 0, 0666)) == -1) 
		        {
		                log_error(ZONE, "Semaphore set does not exist!\n");
		                return false;
		        }
        }

        /* Initialize all member */        
        semctl(*sid, 0, SETVAL, 1);

        log_debug(ZONE, "Created Semaphore %d\n", key);
        
        return true;
}

bool locksem(int sid)
{
        struct sembuf sem_lock={ 0, -1, 0};	//IPC_WAIT};
        
        if((semop(sid, &sem_lock, 1)) == -1)
        {
                log_error(ZONE, "Lock failed\n");
                return false;
        }

		return true;
}

bool unlocksem(int sid)
{
        struct sembuf sem_unlock={ 0, 1, 0};	//IPC_WAIT};

        /* Attempt to lock the semaphore set */
        if((semop(sid, &sem_unlock, 1)) == -1)
        {
                log_error(ZONE, "Unlock failed\n");
                return false;
        }

		return true;
}

void removesem(int sid)
{
        semctl(sid, 0, IPC_RMID, 0);
        log_debug(ZONE, "Semaphore removed\n");
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool openshm(int *shmid, char  **segptr, key_t key)
{
        bool create_new = false;
        if((*shmid = shmget(key, SHM_SIZE, IPC_CREAT|IPC_EXCL|0666)) == -1) 
        {
                log_debug(ZONE, "Shared memory segment exists - opening as client\n");

                /* Segment probably already exists - try as a client */
                if((*shmid = shmget(key, SHM_SIZE, 0)) == -1) 
                {
                        log_error(ZONE, "shmget");
                        return false;
                }
        }
        else
        {
                log_debug(ZONE, "Creating new shared memory segment, %d\n", key);
                create_new = true;
        }

        /* Attach (map) the shared memory segment into the current process */
        *segptr = (char*)shmat(*shmid, 0, 0);        
        if((int)*segptr == -1)
        {
                log_error(ZONE, "shmat");
                return false;
        }
        if (create_new) {
            memset(*segptr, 0, SHM_SIZE);
        }
        
        return true;
}

bool writeshm(int shmid, char *segptr, const char *mid, char cmd, const char * message)
{
    if (!segptr) return false;

    if (segptr[0]==0) {
        segptr[0] = 1;		// mark it unread
        segptr[1] = cmd;		
        strcpy(segptr+RESERVED_SIZE, mid);
        if (message!=NULL) {
           strncpy(segptr + strlen(mid) + RESERVED_SIZE+1, message, SHM_SIZE- strlen(mid) - RESERVED_SIZE - 2);
        }        
        return true;
    }
    return false;
}

bool readshm(int shmid, char *segptr, const char* mid, int *cmd, char * message)
{
    if (!segptr) return false;
    if (segptr[0]==0) {		// cmd already read
       return false;
    }
		
    if (strcmp(segptr+RESERVED_SIZE, mid)!=0) {	// not for this mid
       return false;
    }
		
    * cmd = segptr[1];
    segptr[0] = 0;	// mark it read
		
    log_debug(ZONE, "Read cmd for meeting: %s\n", segptr+RESERVED_SIZE);
    if (message!=NULL) {
        strcpy(message, segptr+strlen(mid)+RESERVED_SIZE+1);
    }
        
    return true;
}

void removeshm(int shmid)
{
    shmctl(shmid, IPC_RMID, 0);
    log_debug(ZONE, "Shared memory segment marked for deletion\n");
}

void writepid(int shmid, char *segptr, int pid)
{
    memcpy(segptr+2, (char*)&pid, 4);
}

void readpid(int shmid, char *segptr, int * pid)
{
    *pid = *(int*)(segptr+2);
}

void readmoviesize(int shmid, char *segptr, int* w, int* h)
{
    *w = *(int*)(segptr+6);
    *h = *(int*)(segptr+10);
}
