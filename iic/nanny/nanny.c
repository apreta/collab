/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/param.h>
#include <sys/resource.h>
#include <pwd.h>
#include <grp.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <errno.h>
#include <stdlib.h>

#define MAX_FAST_DEATH_TIME		30	// if child proc lived less that this time
									// it will be consider a "fast" death
#define MAX_FAST_DEATHS  		5	// this number of consecutive fast deaths 
									// will be considered a "burst"
#define MAX_FAST_DEATH_BURSTS   20	// Only do this number of
                                    // consecutive bursts
#define FAST_DEATH_BURST_WAIT	60	// wait this number of seconds before
									// retrying after a burst of
									// "fast" subprocess deaths
static pid_t child = -1;

#define TMPTEMP	"/tmp/bodyXXXXXX"
#define MAX_ADMIN				10

#define NO_CHANGE_PRIO			-999

#define PROCFS "/proc"

static char *progd = NULL;
static char *sysadm[MAX_ADMIN+1] = {NULL};

void send_email(char **recips, char *body)
{
    char tmpfn[sizeof(TMPTEMP)+1];
    FILE *tmpfp;
    char cmd[1024];
    char *to;
    
    if (*recips == NULL)
        // No recipients
        return;
    
    strcpy(tmpfn, TMPTEMP);
    tmpfp = fdopen(mkstemp(tmpfn), "w");
    if (tmpfp == NULL)
    {
        syslog(LOG_ERR, "Couldn't write temp file %s for e-mail", tmpfn);
        return;
    }
    fprintf(tmpfp, "%s\n", body);
    fclose(tmpfp);

    strcpy(cmd, "mail -s \"nanny report\"");
    for (to = *recips; to != NULL; to = *(++recips))
    {
        strcat(cmd, " ");
        strcat(cmd, to);
    }
    strcat(cmd, " < ");
    strcat(cmd, tmpfn);

    if (system(cmd) < 0)
    {
        syslog(LOG_ERR, "Failure sending e-mail: %s", strerror(errno));
    }
    
    unlink(tmpfn);
}

static int term_sig = SIGKILL;

void sighup_handler(int arg)
{
   int status;

   if (child > 0)
      kill(child, term_sig);

   exit(0);
}

void usage(char * cmd)
{
    fprintf(stderr, "usage: %s [--pidfn <childpidfile>] [--verbose] [--fg] [--clearpids] [--childpid] [--user <iic_user>] [--group <grpname>] [--prio <procprio>] <program_path> <arg0> <arg1> ...\n"
            "\t--pidfn <childpidfile>  clean up this pid file before starting child\n"
            "\t--verbose               verbose output\n"
            "\t--sysadm <email>        add an e-mail address to the error report recipient list\n"
            "\t--fg                    stay in foreground\n"
            "\t--clearpids             clear pids from run file before adding pid\n"
            "\t--childpid              output pid from child process to pid file\n"
            "\t--user <username>       run child process as this user [default: root]\n"
            "\t--group <grpname>       run child process as this group [default:user's group]\n"
            "\t--prio <procprio>       run child process as this scheduling priority [default: nochange]\n"
            "\t--termsig <sig>         Use signal <sig> to terminate the child process\n"
            "\t--progd <id>            Use /var/run/<progd>.pid rather than parsing argv[0]\n"
            "\t--daemon                Use <pidfn> to find what the 'real' child process is.  Expect child to fork and parent to exit (e.g. Apache HTTPD).\n"
            "\t<program_path> <arg0> <arg1> ...  run this command line as a managed child process\n",
            cmd);
}

static int fast_deaths = 0;
static int fast_death_bursts = 0;
void fast_death_handler(int status, char * command, char * hostname)
{
    char body[2048];

    // The daemon process should be a long lived process.
    // If it died this soon, something wacky may be going on.
    // Be conservative and don't keep buzzing indefinitely in this
    // state.

    // Log the condition
    if (WIFEXITED(status))
    {
        syslog(LOG_ERR, "fast %s exit w/ exit status == %d\n",
               command,
               WEXITSTATUS(status));
    }
    else if (WIFSIGNALED(status))
    {
        syslog(LOG_ERR,
               "fast %s exit due to uncaught signal %d\n",
               command,
               WTERMSIG(status));
    }
    else
    {
        syslog(LOG_ERR,
               "fast %s abnormal exit (0x%x).  Restarting...\n",
               command,
               status);
    }
    
    // figure out whether nanny should exit now
    if (++fast_deaths > MAX_FAST_DEATHS)
    {
        // fast death burst
        if (++fast_death_bursts > MAX_FAST_DEATH_BURSTS)
        {
            sprintf(body,
                    "Event on host %s:\n"
                    "\tToo many fast death bursts of %s.  Exiting!",
                    hostname, command);
            syslog(LOG_ERR, "%s", body);

            // notify on first fast death
            send_email(sysadm, body);

            exit (-1);
        }
        else
        {
            sprintf(body,
                    "Event on host %s:\n"
                    "\tFast death burst for %s; Restarting after pause",
                    hostname,
                    command);
            
            // reset fast deaths; sleep a while and try again
            fast_deaths = 0;
            syslog(LOG_ERR, "%s", body);
            if (fast_death_bursts == 1)            
            {
                send_email(sysadm, body);
            }
            
            sleep(FAST_DEATH_BURST_WAIT);
        }
    }
}

void death_handler(int status, char * command, char * hostname)
{
    char body[2048];

    if (WIFEXITED(status))
    {
        sprintf(body,
                "Event on host %s:\n"
                "\t%s exited w/ status == %d; Restarting...",
                hostname, command, WEXITSTATUS(status));
        syslog(LOG_ERR, "%s", body);
        send_email(sysadm, body);
    }
    else if (WIFSIGNALED(status))
    {
        sprintf(body,
                "Event on host %s:\n"
                "\t%s exited w/ uncaught signal %d; Restarting...",
                hostname, command, WTERMSIG(status));
        syslog(LOG_ERR, "%s", body);
        send_email(sysadm, body);
    }
    else
    {
        sprintf(body,
                "Event on host %s:\n"
                "\t%s exited; Restarting...", hostname, command);
        syslog(LOG_ERR, "%s", body);
        send_email(sysadm, body);
    }
}

char * mk_pid_fn(char * command)
{
    char *pidfn;
    char cwd[MAXPATHLEN];

    pidfn = strrchr(command, '/');
    if (pidfn)
    {
        strncpy(cwd, command, pidfn - command);
        cwd[pidfn-command] = '\0';
        if (chdir(cwd) < 0)
        {
            syslog(LOG_ERR, "Couldn't change directory to %s", cwd);
            exit(-1);
        }
        
        ++pidfn;
    }
    else
    {
        pidfn = command;
    }

    return pidfn;
}


void allow_core_files()
{
    struct rlimit lim;

    lim.rlim_cur = lim.rlim_max = RLIM_INFINITY;
    setrlimit(RLIMIT_CORE, &lim);
}

void exec_child(int prio, gid_t gid, uid_t uid, const char * hostname, char *args[])
{
   int status;
   char body[2048];

   // we are the child; so start the given command
   // set priority before we potentially downgrade
   // our status
   if (prio != NO_CHANGE_PRIO)
   {
      if (setpriority(PRIO_PROCESS, 0 /* self */, prio) < 0)
      {
	 sprintf(body, "Unable to set process priority to %d: %s",
		 prio, strerror(errno));
	 syslog(LOG_ERR, "%s", body);
      }
   }
   
   if (setgid(gid) < 0)
   {
      sprintf(body, "Couldn't set group to %d: %s",
	      gid, strerror(errno));
      syslog(LOG_ERR, "%s", body);
   }
    
   if (setuid(uid) < 0)
   {
      sprintf(body, "Couldn't set user to %s: %s",
	      uid, strerror(errno));
      syslog(LOG_ERR, "%s", body);
   }

//   syslog(LOG_ERR, "LANG=%s", getenv("LANG"));
   
   status = execv(args[0], args);
   
   sprintf(body,
	   "Event on host %s:\n"
	   "\tCouldn't start child %s (status==%d). Exiting",
	   hostname, args[0], status);
   syslog(LOG_ERR, "%s", body);
   exit(status);
}

int exec_daemon(const char *pidfn,
		int prio,
		gid_t gid,
		uid_t uid,
		const char *hostname,
		char *args[])
{
   int child;
   int status;
   FILE *pidfp;
   int tries = 0;
   char body[2048];
      
   child = fork();
   if (child < 0)
   {
      sprintf(body,
	      "Event on host %s:\n"
	      "\tCouldn't fork new %s err=%d. EXITING!",
	      hostname, args[0], errno);
      send_email(sysadm, body);
      syslog(LOG_ERR, "%s", body);
      
      return -1;
   }
   else if (child == 0)
   {
      syslog(LOG_ERR, "Executing %s", args[0]);

      exec_child(prio, gid, uid, hostname, args);
   }
   
   // child should exit soon since it's a
   // daemon.  If the child doesn't exit, the
   // caller shouldn't have specified --daemon!
   wait(&status);
   
   // Try to get a pid for the new parent process
   do
   {
      sleep(5);	// HACK: some daemons rewrite the pid file after the parent exits
      pidfp = fopen(pidfn, "r");
   }
   while (errno == ENOENT && pidfp == NULL && tries < 24);

   if (pidfp == NULL)
   {
      sprintf(body,
	      "Couldn't read pidfn %s to get child pid: %s.  Startup issue?",
	      pidfn, strerror(errno));
      send_email(sysadm, body);
      syslog(LOG_ERR, "%s", body);
      
      return -1;
   }
   
   fscanf(pidfp, "%d", &child);
   fclose(pidfp);
   
   return child;
}

int is_procid(const char *procent)
{
   int ii;
   
   if (procent == NULL) return 0;

   for (ii = 0; ii < strlen(procent); ii++)
   {
      if (!isdigit(procent[ii]))
	 return 0;
   }

   return 1;
}

void cleanup_children(pid_t target)
{
   DIR *d;
   struct dirent *de;

   d = opendir(PROCFS);
   if (d == NULL)
   {
      syslog(LOG_ERR, "Can't open %s for inspection",
	     PROCFS);
      return;
   }

   while (de = readdir(d))
   {
      if (is_procid(de->d_name))
      {
	 char statfn[256];
	 FILE *statfp;
	 char cmd[256];
	 pid_t pid;
	 char state;
	 pid_t ppid, ppgp;
	 
	 sprintf(statfn, "%s/%s/stat", PROCFS, de->d_name);
	 
	 statfp = fopen(statfn, "r");
	 if (statfp == NULL)
	 {
	    syslog(LOG_ERR, "Unable to open %s: %s",
		   statfn, strerror(errno));
	    continue;
	 }
	 if (fscanf(statfp, "%d %s %c %d %d", &pid, cmd, &state, &ppid, &ppgp) != 5)
	 {
	    syslog(LOG_ERR, "stat file not in expected format!");
	    continue;
	 }
	 
	 if (ppgp == target)
	 {
	    // Paydirt!
	    kill(pid, term_sig);
	 }
      }
   }

   closedir(d);
}

void write_pid(int pid, const char * progd, int clearpids)
{
    FILE *flagfp;
    char flagfn[MAXPATHLEN];
    char body[2048];

    sprintf(flagfn, "/var/run/%s.pid", progd);
    flagfp = fopen(flagfn, (clearpids ? "w" : "a"));
    if (flagfp != NULL)
    {
       fprintf(flagfp, " %d", pid);
       fclose(flagfp);
    }
    else
    {
       sprintf(body, "Couldn't open %s: %s", flagfn, strerror(errno));
       syslog(LOG_ERR, "%s", body);
       closelog();
       
       exit(-1);
    }
}

int main(int argc, char * argv[], char * envp[])
{
    int status;
    time_t last_fork_time;
    char **args;
    int i;
    char flagfn[MAXPATHLEN];
    char wdir[MAXPATHLEN];
    FILE *flagfp;
    struct passwd *pwent = NULL;
    struct group *grent = NULL;
    char *childpidfn = NULL;
    char *user = "root";
    uid_t uid;
    char *grp= "root";
    gid_t gid;
    int cur_opt, start_opt;
    pid_t pid;
    int prio = NO_CHANGE_PRIO;
    int verbose = 0;
    int fg = 0;
    int clearpids = 0;	// only clear pids if --clearpids arg is given
    int daemonchild = 0;
    int childpid = 0;   // optionally write child pid to pid file
    int nadm = 0;
    char body[2048];
    int fd;
    char hostname[128];
    
    if (argc == 1)
    {
        usage(argv[0]);
        
        exit(-1);
    }

    cur_opt = 1;
    do {
        if (strcmp(argv[cur_opt], "--pidfn") == 0)
        {
            childpidfn = argv[cur_opt+1];
            cur_opt += 2;
        }
        else if (strcmp(argv[cur_opt], "--user") == 0)
        {
            user = argv[cur_opt+1];
            cur_opt += 2;
        }
        else if (strcmp(argv[cur_opt], "--group") == 0)
        {
            grp = argv[cur_opt+1];
            cur_opt += 2;
        }
        else if (strcmp(argv[cur_opt], "--prio") == 0)
        {
            prio = atoi(argv[cur_opt+1]);
            if (prio < -20 || prio > 20)
            {
                fprintf(stderr, "Invalid priority %d; check --prio", prio);
                exit(-1);
            }
            cur_opt += 2;
        }
        else if (strcmp(argv[cur_opt], "--termsig") == 0)
        {
            term_sig = atoi(argv[cur_opt+1]);
            cur_opt += 2;
        }
        else if (strcmp(argv[cur_opt], "--progd") == 0)
        {
	    progd = argv[cur_opt+1];
	    cur_opt += 2;
        }
        else if (strcmp(argv[cur_opt], "--daemon") == 0)
        {
	   daemonchild = 1;
	   cur_opt += 1;
        }
        else if (strcmp(argv[cur_opt], "--verbose") == 0)
        {
            verbose = 1;
            ++cur_opt;
        }
        else if (strcmp(argv[cur_opt], "--sysadm") == 0)
        {
            if (nadm < MAX_ADMIN)
            {
                sysadm[nadm++] = argv[cur_opt+1];
            }
            else
            {
                fprintf(stderr, "Too many admins;  ignoring %s", argv[cur_opt+1]);
            }
            cur_opt += 2;
        }
        else if (strcmp(argv[cur_opt], "--fg") == 0)
        {
            fg = 1;

            ++cur_opt;
        }
        else if (strcmp(argv[cur_opt], "--clearpids") == 0)
        {
            clearpids = 1;

            ++cur_opt;
        }
        else if (strcmp(argv[cur_opt], "--childpid") == 0)
        {
            childpid = 1;

            ++cur_opt;
        }
        else if (strncmp(argv[cur_opt], "--", 2) == 0)
        {
            fprintf(stderr, "Unknown option %s seen\n", argv[cur_opt]);
            usage(argv[0]);
            
            exit(-1);
        }
        else
        {
            break;  // non-option arg; assume it's the pgm to run
        }
    } while (cur_opt < argc && strncmp(argv[cur_opt], "--", 2) == 0);
    
    sysadm[nadm] = NULL;
    
    if (cur_opt == argc)
    {
        fprintf(stderr, "Need to specify a daemon to run\n");
        usage(argv[0]);

        exit (-1);
    }
    
    pwent = getpwnam(user);
    if (pwent == NULL)
    {
        fprintf(stderr, "User %s not found\n", user);

        exit(-1);
    }
    uid = pwent->pw_uid;
    
    grent = getgrnam(grp);
    if (grent == NULL)
    {
        fprintf(stderr, "Group %s not found\n", grp);

        exit(-1);
    }
    gid = grent->gr_gid;

    if (daemonchild && !childpidfn)
    {
       fprintf(stderr, "--daemon requires --childpidfn so parent daemon pid can be determined");
       exit(-1);
    }
    
    if (verbose)
    {
       printf("Running %s as %s:%s\n", argv[cur_opt], user, grp);
    }
    
    if (verbose)
    {
       printf("Will run: [%d] ", argc-cur_opt+1);
    }

    strcpy(hostname, "<unknown>");
    gethostname(hostname, sizeof hostname);

    args = (char **)calloc(argc-cur_opt+1, sizeof(char *));
    for (start_opt = cur_opt; cur_opt < argc; cur_opt++)
    {
        if (verbose)
        {
            printf(" %s", argv[cur_opt]);
        }

        args[cur_opt-start_opt] = argv[cur_opt];
    }
    if (verbose)
        printf("\n");
    
    args[cur_opt-start_opt] = NULL;

    if (!fg)
    {
        pid = fork();
        
        if (pid > 0)
        {
            if (verbose)
                printf("nanny in background\n");
            exit(0);
        }
        else if (pid < 0)
        {
            fprintf(stderr, "Couldn't fork to put nanny in background\n");
            
            exit(-1);
        }
    }
    else
    {
        if (verbose)
            printf("Running in foreground\n");
    }

    openlog("nanny", LOG_PID, LOG_DAEMON);
    
    signal(SIGHUP, &sighup_handler);
    
    if (childpidfn)
        unlink(childpidfn);	// do this once as root to be sure it's gone

    allow_core_files();
    
    // If two or more nanny's manage the same command, use
    // --progd to make the nanny pid files distinct
    if (progd == NULL)
       progd = mk_pid_fn(args[0]);

    // Put parent's pid in the run file so that killproc function can
    // send nanny the shutdown HUP signal
    write_pid((int)getpid(), progd, clearpids);

    fd = open("/dev/null", O_RDONLY);
    if (fd < 0) {
      sprintf(body, "Couldn't open /dev/null: %s", strerror(errno));
      syslog(LOG_ERR, "%s", body);
      closelog();

      exit(-1);
    }
    if (dup2(fd, 0) < 0) {
      sprintf(body, "Couldn't set stdin to /dev/null: %s", strerror(errno));
      syslog(LOG_ERR, "%s", body);
      closelog();

      exit(-1);
    }

    fd = open("/dev/null", O_WRONLY);
    if (fd < 0) {
      sprintf(body, "Couldn't open /dev/null: %s", strerror(errno));
      syslog(LOG_ERR, "%s", body);
      closelog();

      exit(-1);
    }
    if (dup2(fd, 1) < 0) {
      sprintf(body, "Couldn't set stdout to /dev/null: %s", strerror(errno));
      syslog(LOG_ERR, "%s", body);
      closelog();

      exit(-1);
    }

    fd = open("/dev/null", O_WRONLY);
    if (fd < 0) {
      sprintf(body, "Couldn't open /dev/null: %s", strerror(errno));
      syslog(LOG_ERR, "%s", body);
      closelog();

      exit(-1);
    }
    if (dup2(fd, 2) < 0) {
      sprintf(body, "Couldn't set stderr to /dev/null: %s", strerror(errno));
      syslog(LOG_ERR, "%s", body);
      closelog();

      exit(-1);
    }

    for (;;)
    {
       int ret;

       if (childpidfn != NULL)
	  unlink(childpidfn);	// clean out the child pid fn (assume that nanny is always the manager)
        
       if (!daemonchild)
       {
	  child = fork();
	  if (child < 0)
	  {
	     sprintf(body,
		     "Event on host %s:\n"
		     "\tCouldn't fork new %s err=%d. EXITING!",
		     hostname, args[0], errno);
	     send_email(sysadm, body);
	     syslog(LOG_ERR, "%s", body);
	     closelog();
	     exit(-1);
	  }
	  else if (child == 0)
	  {
	     initgroups(user, gid);
	     exec_child(prio, gid, uid, hostname, args);
	  }
          else
          { 
             if (childpid != 0)
                write_pid(child, progd, 0);
          }
       }
       else
       {
	  do
	  {
	     child = exec_daemon(childpidfn, prio, gid, uid, hostname, args);

	     sleep(5);
	  } while (child < 0);
       }

       // parent
       last_fork_time = time(NULL);

       if (!daemonchild)
	  ret = waitpid(child, &status, 0);
       else
       {
	  struct stat st;
	  char procfn[64];
	  sprintf(procfn, "%s/%d", PROCFS, child);

	  while (stat(procfn, &st) == 0)
	     sleep(2);

	  cleanup_children(child);

	  death_handler(0, args[0], hostname);
	  
	  continue;
       }
            
       // Child exited for some reason; determine what to do
       if (WIFSIGNALED(status))
       {
	  int sig = WTERMSIG(status);
	   
	  if ((sig == SIGKILL) || (sig == SIGTERM))
	  {
	     sprintf(body, "%s killed; nanny exiting", args[0]);
	      
	     syslog(LOG_ERR, "%s", body);
	      
	     // died due to a termination signal sent to it
	     // so assume the person/system knows what they are doing
	     closelog();
	      
	     exit(0);
	  }
       }

       if ((time(NULL) - last_fork_time) < MAX_FAST_DEATH_TIME)
       {
	  fast_death_handler(status, args[0], hostname);
       }
       else
       {
	  fast_death_bursts = fast_deaths = 0;

	  death_handler(status, args[0], hostname);
       }
    }
}
