/* C++ interface for asynchronous network library
 * (c)2005 SiteScape
 */

#ifndef COMMSVR_H
#define COMMSVR_H

#include "../istring.h"
#include "../ithread.h"
#include "comm.h"

#define MAX_CLIENTS 32


class CommServer : public CommConnection, public CommEvent
{
	IString my_addr;
	int port;
	
public:
	CommServer(CommEvent * handler);
	
	int initialize(const char * my_addr, int port);

	bool on_message(CommConnection * c, CommMessage * msg);
	bool on_close(CommConnection * c);
	bool on_error(CommConnection * c);
	bool on_accept(CommConnection * c);
};

#endif
