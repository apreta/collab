/* C++ interface for asynchronous network library
 * (c)2005 SiteScape
 */

#ifndef COMM_H
#define COMM_H

#include "../istring.h"
#include "../ithread.h"
#include "async_io.h"
#include "comdefs.h"


class CommConnection;
class CommClient;

enum CommErrors
{
	CommOk = 0,
	CommNotConnected = -1,
	CommConnectFailed = -2,
	CommListenFailed = -3,
	CommDatagramFailed = -4,
	CommMessageTooLong = -5,
};


// Incoming/outgoing message to reflector.
class CommMessage
{
    int len;
    unsigned char data[MAX_MESSAGE];
    char buffer[MAX_STRING];
    int pos;
    bool valid;

public:
    CommMessage();

    void set_message(unsigned char *data, int len);

    int get_len()                 { return len; }
    unsigned char * get_data()    { return data; }
    bool is_valid()               { return valid; }

    int get_int();
    void set_int(int x);
    const char * get_string();
    void set_string(const char *s);
    
	int peek_int();
};


// AIO_SLOT structure.
struct CommSlot
{
    AIO_SLOT s;
    CommConnection *client;
};


// Interface for dispatching connection events.
class CommEvent
{
public:
    virtual ~CommEvent() { }
    
    // Incoming message.  msg is not valid after the handler returns.
    virtual bool on_message(CommConnection * conn, CommMessage * msg) = 0;

    // Other network events.
    virtual bool on_close(CommConnection * conn)  { return true; }
    virtual bool on_error(CommConnection * conn)  { return true; }
    virtual bool on_accept(CommConnection * conn) { return true; }
    virtual bool on_timer(CommConnection * conn)  { return true; }
    virtual bool on_empty(CommConnection * conn)  { return true; }
};


// AIO main loop wrapper.
class CommProcess : public IThread
{
    static bool thread_flag;
	static bool shutdown_flag;
	
public:
	CommProcess();

	static int initialize(bool threaded = true);	
	static int shutdown();

	void run();

    static void process();
	
	static bool is_shutdown()	{ return shutdown_flag; }
};


// Abstraction for managed AIO slot.
class CommConnection
{
protected:
    CommEvent * handler;
    int state;

    CommSlot * slot;

	int send_timeout;

	static const char * header;
    static int message_len_size;

public:
    enum { CommNone = 0, CommConnected = 1 };
    enum { CommStream = 0, CommListen = 1, CommAccepted = 2, CommDatagram = 3 };
    CommConnection(CommEvent * handler);
    virtual ~CommConnection() { }

    bool is_connected();
    int send_message(CommMessage * msg, int timeout = -1);

	void set_timer(int timeout);
    void cancel_timer();

    void close(bool remove);

    unsigned long get_peer_addr();
    unsigned short get_peer_port();
    
    static void set_header(const char * header);
    static void set_message_len_size(int bytes);

protected:
	void on_timer();
	void on_empty();

    static void init_hook();
    static void listen_hook();
	static void datagram_hook();
    static void accept_hook();
    static void connection_closed();
    static void header_read();
    static void message_len_read();
    static void message_read();
    static void message_sent();
    static void queue_empty();
    static void timer_hook();
};


// Manage communication with server.
class CommClient : public CommConnection, public CommEvent
{
    CommEvent * handler;

    IString server;
    int port;
	bool reconnect;

public:
    CommClient(CommEvent * handler);

    int initialize(const char * server, int port, bool reconnect);

	int connect();

	bool on_message(CommConnection * c, CommMessage * msg);
	bool on_close(CommConnection * c);
	bool on_error(CommConnection * c);
	bool on_timer(CommConnection * c);
};


// Manage datagram communications
class CommDatagram : public CommConnection, public CommEvent
{
    CommEvent * handler;

	IString my_addr;
    int port;

public:
    CommDatagram(CommEvent * handler);

    int initialize(const char * my_addr, int port);

	int send_message(const char * addr, int port, CommMessage * msg, int timeout = -1);
	int send_message(unsigned long addr, int port, CommMessage * msg, int timeout = -1);

	bool on_message(CommConnection * c, CommMessage * msg);
	bool on_close(CommConnection * c);
	bool on_error(CommConnection * c);
	bool on_timer(CommConnection * c);
	bool on_empty(CommConnection * c);
};


#endif
