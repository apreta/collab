#ifndef RING_H
#define RING_H

#include "../iobject.h"
#include "../asynccomm/comm.h"

/*
 Control ring

 On wire:
	int	Command (0 = normal message, otherwise control message
	<message specific fields>

*/


//#define MAX_RING_SIZE 12

#define JOIN_ACK_TIMEOUT 5000
#define JOIN_RESEND_TIMEOUT 10000

#define TOKEN_ACK_TIMEOUT 1000
#define TOKEN_LOST_TIMEOUT 20000

#define MSG_BATCH_SIZE 500

#define TOKEN_PASS_PAUSE 200  // Pause before passing token when system is idle

class ControlRing;
class ControlMessage;
class BroadcastMessage;
class BroadcastQueue;


class IMessage
{
public:
	virtual void encode(CommMessage * msg) = 0;
	virtual void decode(CommMessage * msg) = 0;
};


// Broadcast control and app messages to servers in ring
class ControlLink : public CommEvent
{
	CommDatagram comm;
	ControlRing * ring;
	IString my_addr;
	IString broadcast_addr;
	int broadcast_port;
	
public:
	ControlLink(ControlRing * ring);

	int initialize(const char * my_addr, const char * broadcast_addr, int port);

	int send_message(IMessage * msg, int timeout = -1);
	void set_timer(int timeout);
	void cancel_timer();

	bool on_message(CommConnection * c, CommMessage * msg);	
	bool on_timer(CommConnection * conn);
    bool on_empty(CommConnection * conn);

    bool on_close(CommConnection * conn);
    bool on_error(CommConnection * conn);
};


// Control messages (tokens, nacks, etc.)
class ControlMessage : public IObject, public IMessage
{
	int command;
	IString from_ip;
	IString to_ip;
	int write_sequence;
	int read_sequence;
	IList members;

public:
	enum { CntlMin = 100, CntlToken = 100, CntlAck, CntlJoin, CntlError, CntlMax };
	ControlMessage();

	void set_command(int _cmd)			{ command = _cmd; }
	int get_command()					{ return command; }
	void set_from(const char *_ip)		{ from_ip = _ip; }
	IString& get_from()					{ return from_ip; }
	void set_to(const char *_ip)		{ to_ip = _ip; }
	IString& get_to()					{ return to_ip; }
	void set_read_sequence(int _seq)	{ read_sequence = _seq; }
	int get_read_sequence()				{ return read_sequence; }
	void set_write_sequence(int _seq)	{ write_sequence = _seq; }
	int get_write_sequence()			{ return write_sequence; }
	int number_members()				{ return members.size(); }

	void add_member(IString & addr);
	void remove_member(IString & addr);
	bool is_member(IString & addr);
	int index_of_member(IString & addr);
	IList *get_members()				{ return &members; }
	
	IString * next_member(IString & master);

	void encode(CommMessage * msg);
	void decode(CommMessage * msg);
	
	const char * dump(char * buffer, int buff_len);

    enum { TYPE_CNTRL_MSG = 7742 };
    virtual int type()
        { return TYPE_CNTRL_MSG; }
    virtual unsigned int hash()
        { return (unsigned)(long)this; }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && this == (ControlMessage *)obj; }
};


// Manage ring state
class ControlRing
{
	// Datagram connection
	ControlLink link;

	// Message Queue
	BroadcastQueue * queue;

	IString my_addr;
	
	int ring_state;
	int retry;
	bool delay_flag;

	bool shutdown_flag;
	ICondVar shutdown_signal;

	// Last received token
	ControlMessage * token;

	IList join_requests;

	enum { StateNone, StateMember, StateMaster, StatePassing, StateAlone, StateStopping };

public:
	ControlRing(BroadcastQueue * queue);

	int initialize(const char * my_addr, const char * broadcast_addr, int port);
	void shutdown();

	void send_message(IMessage * msg);

	int send_join(bool first_attempt = false);
	void send_ack(const char * addr);
	void build_token();
	void send_token(bool first_attempt = false);
	void set_token(ControlMessage * token);
	
	void process_message(ControlMessage * msg);	
	void process_ack(ControlMessage * msg);	
	void process_token(ControlMessage * msg);	
	void process_join(ControlMessage * msg);
	void process_queued_joins();
	
	void process_nack(ControlMessage * msg);
	void process_query(ControlMessage * msg);
	
	void set_no_delay()			{ delay_flag = false; }

	bool on_message(CommMessage * msg);	
	void on_timer();
	void on_empty();
};


// Application messages
class BroadcastMessage : public IObject, public IMessage
{
	int command;
	IString from_ip;
	IString to_ip;
	int sequence;
    int len;
    unsigned char data[MAX_MESSAGE];

public:
	enum { BcastMin = 200, BcastMessage = 200, BcastQuery, BcastMax };
	BroadcastMessage();
	virtual ~BroadcastMessage()			{ }

	void set_command(int _cmd)			{ command = _cmd; }
	int get_command()					{ return command; }
	void set_from(const char *_ip)		{ from_ip = _ip; }
	IString& get_from()					{ return from_ip; }
	void set_to(const char *_ip)		{ to_ip = _ip; }
	IString& get_to()					{ return to_ip; }
	void set_sequence(int _seq)			{ sequence = _seq; }
	int get_sequence()					{ return sequence; }

	void encode(CommMessage * msg);
	void decode(CommMessage * msg);
	
	const char * dump(char * buffer, int buff_len);

    enum { TYPE_BCAST_MSG = 7743 };
    virtual int type()
        { return TYPE_BCAST_MSG; }
    virtual unsigned int hash()
        { return sequence; }
    virtual bool equals(IObject *obj)
        { return obj->type() == type() && sequence == ((BroadcastMessage*)obj)->sequence; }
};


// Event handler interface for upper layers
class BroadcastEvent
{
public:
	virtual BroadcastMessage * create_message(CommMessage * link_msg) { return new BroadcastMessage(); }
	
	// Handle app messages
	virtual void on_message(BroadcastMessage * msg) = 0;
	virtual void on_query(BroadcastMessage * search) { }
	
	// Inform app of ring state transitions	
	virtual void on_ring_lost() { }
	virtual void on_ring_joined() { }
	virtual void on_join(const char * new_addr) { }
	virtual void on_left(const char * left_addr) { }
};


// Main interface for upper layers
// Hold outgoing messages till we are master; hold incoming till we have proper sequencing
class BroadcastQueue
{
	IList incoming;
	IList outgoing;
	
	BroadcastEvent * handler;

	ControlRing ring;

	IString my_addr;
	
	// Only need to lock sending queue as all other operations use 
	// the AIO thread.
	IMutex queue_lock;

	int last_dispatched;
	int last_read;

public:
	BroadcastQueue(BroadcastEvent * _handler);

	// External queue APIs
	int initialize(const char * my_addr, const char * broadcast_addr, int port);
	void shutdown();
	int send_message(BroadcastMessage * msg);

	// Internal methods
	
	// Handle incoming broadcast messages
	void reset_incoming(int starting_dispatch);
	void queue_incoming(BroadcastMessage * msg, bool allow_self = false);
	void remove_incoming(int before_sequence);

	// Callbacks from control ring
	void on_message(CommMessage * link_msg, int command);
	void on_ring_joined();
	void on_ring_lost();
	void on_node_left(IString & addr);
	void on_node_joined(IString & addr);

	// We are master; handle token
	void process_token(ControlMessage * token);

	// Process incoming messages
	void dispatch_incoming(ControlMessage * token);
	BroadcastMessage * find_last_dispatchable(int from_seq);

	// Process outgoing messages
	void send_outgoing(ControlMessage * token);
	void resend_messages(ControlMessage * token, int from_seq, int to_seq);
};

#endif
