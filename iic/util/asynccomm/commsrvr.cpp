
#include "../../jcomponent/util/log.h"
#include "commsrvr.h"
#include "port.h"

/*
 * CommServer
 */

CommServer::CommServer(CommEvent * _handler) :
	CommConnection(this)
{
	handler = _handler;
}

int CommServer::initialize(const char * _my_addr, int _port)
{
	CommSlot * slot;
	
	my_addr = _my_addr;
    port = _port;
	
	aio_set_bind_address(my_addr);

    if (aio_listen(port, listen_hook, accept_hook, sizeof(CommSlot), (AIO_SLOT**)&slot) != 1)
	{
		log_error(ZONE, "Unable to listen on port %d", port);
		return CommListenFailed;
	}
	
	slot->client = this;

	return CommOk;
}


bool CommServer::on_message(CommConnection * c, CommMessage * msg)
{
	return handler->on_message(c, msg);
}

bool CommServer::on_close(CommConnection * c)
{
	bool res = handler->on_close(c);
	
	return res;
}

bool CommServer::on_error(CommConnection * c)
{
	return handler->on_error(c);
}

bool CommServer::on_accept(CommConnection * c)
{
	bool res = handler->on_accept(c);

	return res;
}
