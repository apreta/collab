#include <stdio.h>

#include "../../jcomponent/util/log.h"
#include "ring.h"
#include "../asynccomm/port.h"

/*
   To do:  
     - add ring id to broadcasts so multiple rings can run on same lan segment (or make sure to use different ports)

     - look at handling loss of master more

     - add membership version so we can tell if membership has changed without checking

     - assumption:  network is reasonably reliable, only will fail to recover if node is missing
       (could we track who sent the data so we could inform app about which node's data is missing?)

     - non-response to nack may still cause problem
*/

/*
 * ControlLink
 */

ControlLink::ControlLink(ControlRing * _ring) :
	comm(this)
{
	ring = _ring;
}

// Open datagram socket on specified port.
int ControlLink::initialize(const char * _my_addr, const char * _broadcast_addr, int _port)
{
	my_addr = _my_addr;
	broadcast_addr = _broadcast_addr;
	broadcast_port = _port;

	return comm.initialize(_my_addr, _port);
}

// Send message to broadcast address.
int ControlLink::send_message(IMessage * msg, int timeout)
{
	CommMessage link_msg;

	msg->encode(&link_msg);

	return comm.send_message(broadcast_addr, broadcast_port, &link_msg, timeout);
}

// (must be called in context of message handler)
void ControlLink::set_timer(int timeout)
{
	comm.set_timer(timeout);
}

// (must be called in context of message handler)
void ControlLink::cancel_timer()
{
	comm.cancel_timer();
}

// Handle received message.
bool ControlLink::on_message(CommConnection * c, CommMessage * link_msg)
{
	ring->on_message(link_msg);
	return true;
}

bool ControlLink::on_timer(CommConnection * conn)
{
	ring->on_timer();
	return true;
}

bool ControlLink::on_empty(CommConnection * conn)
{
	ring->on_empty();
	return true;
}

bool ControlLink::on_close(CommConnection * conn)
{

	return true;
}

bool ControlLink::on_error(CommConnection * conn)
{

	return true;
}

/*
 * ControlMessage
 *   command
 *   from
 *   to
 *   write sequence
 *   number members
 *   member addrs[1..number  members]
 *
 * ControlMessage is also used to store current ring state as defined by last token received.
 */

ControlMessage::ControlMessage()
{
	command = -1;
	write_sequence = 0;
	read_sequence = 0;
}
	
void ControlMessage::add_member(IString & addr)
{
	if (members.find(&addr) == NULL)
		members.add(new IString(addr));
}

void ControlMessage::remove_member(IString & addr)
{
	IString * tmp = (IString *)members.find(&addr);
	if (tmp != NULL)
	{
		members.remove(tmp);
	}
}
	
bool ControlMessage::is_member(IString & addr)
{
	if (members.find(&addr) != NULL)
		return true;
	else
		return false;
}
	
// Find node after current master, wrapping back to start if needed.
IString * ControlMessage::next_member(IString & master)
{
	IString * prev = NULL;
	IString * temp = NULL;
	
	IListIterator iter(members);
	IString * addr = (IString *)iter.get_first();
	while (addr)
	{
		if (master == *addr)
			prev = temp;
		temp = addr;
		addr = (IString *)iter.get_next();
	}
	
	if (prev == NULL)
		prev = temp;
		
	return prev;
}

int ControlMessage::index_of_member(IString & addr)
{
	int index = 0;
	IListIterator iter(members);
	IString * check = (IString *)iter.get_first();
	while (check)
	{
		if (addr == *check)
			return index;
		check = (IString *)iter.get_next();
	}
	return -1;
}

// Encode message contents.
void ControlMessage::encode(CommMessage * msg)
{
	msg->set_int(command);
	msg->set_string(from_ip);
	msg->set_string(to_ip);
	msg->set_int(write_sequence);
	msg->set_int(read_sequence);
	msg->set_int(members.size());
	
	IListIterator iter(members);
	IString * addr = (IString *)iter.get_first();
	while (addr)
	{
		msg->set_string(addr->get_string());
		addr = (IString *)iter.get_next();
	}
	
	char buffer[MAX_MESSAGE];
	log_debug(ZONE, "Encoded message: %s", dump(buffer, sizeof(buffer)));
}

// Decode message contents.
// Message will be flagged invalid if we can't properly parse it.
void ControlMessage::decode(CommMessage * msg)
{
	command = msg->get_int();
	from_ip = msg->get_string();
	to_ip = msg->get_string();
	write_sequence = msg->get_int();
	read_sequence = msg->get_int();
	int number_members = msg->get_int();
	
	for (int i=0; i<number_members; i++)
	{
		const char * addr = msg->get_string();
		if (addr != NULL)
			members.add(new IString(addr));
	}
	
	char buffer[MAX_MESSAGE];
	log_debug(ZONE, "Decoded message: %s", dump(buffer, sizeof(buffer)));
}

const char * ControlMessage::dump(char * buffer, int buff_len)
{
	snprintf(buffer, buff_len, "%d, %s, %s, %d, %d", command, from_ip.get_string(),
		to_ip.get_string(), write_sequence, read_sequence);
	return buffer;
}

/*
 * ControlRing
 */

ControlRing::ControlRing(BroadcastQueue * _queue) :
	link(this)
{
	queue = _queue;
	ring_state = StateNone;
	retry = 0;
	shutdown_flag = false;
	token = NULL;
	delay_flag = true;
}

int ControlRing::initialize(const char * _my_addr, const char * broadcast_addr, int port)
{
	my_addr = _my_addr;

	int stat = link.initialize(my_addr, broadcast_addr, port);
	if (stat)
	{
		log_debug(ZONE, "Failed to initialize control link (%d)", stat);
		return stat;
	}

	return send_join();
}

void ControlRing::shutdown()
{
	log_debug(ZONE, "Shutting down broadcast queue");
	shutdown_flag = true;
	if (ring_state != StateNone && ring_state != StateAlone)
	{
		log_debug(ZONE, "Waiting for us to become master to complete shutdown");
		shutdown_signal.timed_wait(15000);
	}
	CommProcess::shutdown();
}

void ControlRing::send_message(IMessage * msg)
{
	link.send_message(msg);
}

// Send join message; set timer for result
int ControlRing::send_join(bool first_attempt)
{
	if (first_attempt)
		retry = 0;
	else
		++retry;

	ControlMessage msg;
	msg.set_command(ControlMessage::CntlJoin);
	msg.set_from(my_addr);

	int timeout = ring_state == StateNone ? JOIN_ACK_TIMEOUT : JOIN_RESEND_TIMEOUT;	
	int stat = link.send_message(&msg, timeout);
	if (stat)
	{
		log_error(ZONE, "Failed to send cluster join message (%d)", stat);
		return stat;
	}

	return CommOk;
}

// Send ack message
void ControlRing::send_ack(const char * addr)
{
	ControlMessage msg;
	msg.set_command(ControlMessage::CntlAck);
	msg.set_from(my_addr);
	msg.set_to(addr);
	msg.set_write_sequence(token->get_write_sequence());

	int stat = link.send_message(&msg);
	if (stat)
	{
		log_error(ZONE, "Failed to send ack to %s (%d)", addr, stat);
	}
}

// Build initial token (just has our node to start)
void ControlRing::build_token()
{
	if (token != NULL)
		delete token;
	token = new ControlMessage();
	token->set_command(ControlMessage::CntlToken);
	token->add_member(my_addr);
	log_debug(ZONE, "Created initial token for %s", my_addr.get_string());
}

// Set current token (to address in token is current master)
void ControlRing::set_token(ControlMessage * new_token)
{
	if (token != NULL)
	{
		// Nodes that are in stored token but not new token have left the ring
		IListIterator iter(*token->get_members());
		IString * addr = (IString *)iter.get_first();
		while (addr != NULL)
		{
			if (new_token->get_members()->find(addr) == NULL)
				queue->on_node_left(*addr);
			addr = (IString *)iter.get_next();
		}
		
		// Nodes that are in new token but not stored token have joined the ring
		iter.set_list(*new_token->get_members());
		addr = (IString *)iter.get_first();
		while (addr != NULL)
		{
			if (token->get_members()->find(addr) == NULL)
				queue->on_node_joined(*addr);
			addr = (IString *)iter.get_next();
		}

		delete token;
	}
	else
	{
		IListIterator iter(*new_token->get_members());
		IString * addr = (IString *)iter.get_first();
		while (addr != NULL)
		{
			if (*addr != my_addr)
				queue->on_node_joined(*addr);
			addr = (IString *)iter.get_next();
		}
	}	

	token = new_token;
}

// Send token, assigning to next node, if any.
// (should always be called from main loop processing so we can set timer if needed)
void ControlRing::send_token(bool first_attempt)
{
	IString * next = token->next_member(my_addr);
	if (next == NULL)
	{
		// Shouldn't happen, but just in case, re-create initial state.
		log_debug(ZONE, "Invalid ring, rebuilding");
		build_token();
		ring_state = StateAlone;
	}
	else if (*next == my_addr)
	{
		// We think we are the only node, switch to alone state and send join query
		log_debug(ZONE, "No peers, switching to alone state");
		if (ring_state != StateAlone)
		{
			ring_state = StateAlone;
			queue->on_ring_lost();
		}
	}
	else
	{	
		ring_state = StatePassing;
	}

	if (ring_state == StatePassing)
	{
		log_debug(ZONE, "Passing token to %s", next->get_string());
		
		if (first_attempt)
			retry = 0;
		else
			++retry;

		// If shutting down, remove this node from token first
		if (shutdown_flag)
		{
			log_debug(ZONE, "Removing myself from ring token");
			token->remove_member(my_addr);
			ring_state = StateStopping;
		}
	
		token->set_command(ControlMessage::CntlToken);
		token->set_from(my_addr);
		token->set_to(*next);
		// sequence set when outgoing queue processed
	
		int stat = link.send_message(token, TOKEN_ACK_TIMEOUT);
		if (stat)
		{
			queue->on_ring_lost();
			log_error(ZONE, "Network error, unable to send token to %s (%d)", *next->get_string(), stat);
		}
	}
	else
	{
		// Not in ring any more, try resending join instead
		if (shutdown_flag)
			shutdown_signal.signal();
		else
			send_join(true);
	}
}

// Dispatch control message.
void ControlRing::process_message(ControlMessage * msg)
{
	switch (msg->get_command())
	{
	case ControlMessage::CntlAck:
		process_ack(msg);
		break;
	case ControlMessage::CntlToken:
		process_token(msg);
		break;	
	case ControlMessage::CntlJoin:
		process_join(msg);
		break;
	default:
		delete msg;
	}
}

// Process ack message
void ControlRing::process_ack(ControlMessage * msg)
{
	// Acks always sent to specific address, process if for me
	if (my_addr == msg->get_to())
	{
		if (ring_state == StateNone || ring_state == StateAlone)
		{
			log_debug(ZONE, "Ring membership request acked by %s", msg->get_from().get_string());
			ring_state = StateMember;
			link.cancel_timer();
			
			queue->reset_incoming(msg->get_write_sequence());
		
			queue->on_ring_joined();
		}
		else if (ring_state == StatePassing)
		{
			// Normal token pass, set watchdog timer (uses index of myself in ring so nodes don't all detect
			// master loss at same time)
			log_debug(ZONE, "Ring token acked by %s", msg->get_from().get_string());
			ring_state = StateMember;	
			
			int timeout = (token->index_of_member(my_addr) + 1) * TOKEN_LOST_TIMEOUT;
			link.set_timer(timeout);
		}
		else if (ring_state == StateStopping)
		{
			// Now that token is passed, finish requested shutdown.
			log_debug(ZONE, "Token passed, shutdown will complete");
			shutdown_signal.signal();
		}
	}
	
	delete msg;
}

// See if we've received a token
void ControlRing::process_token(ControlMessage * msg)
{
	if (ring_state == StateAlone)
	{	
		// Looks like there is a ring, try to join.
		if (msg->get_from() != my_addr)
		{
			log_debug(ZONE, "Detected token message from %s, trying to join ring", msg->get_from().get_string());
			ring_state = StateNone;
			
			send_join(true);
		}
	}
	
	if (ring_state == StateMaster)
	{
		if (msg->get_from() == my_addr)
		{
			log_debug(ZONE, "Token assigned to me while master, resetting timers");
			link.cancel_timer();
			ring_state = StateMember;
		}
	}

	if (ring_state == StateMember)
	{
		// Keep message as definition of ring state	
		set_token(msg);

		// See if we have become master
		if (my_addr == msg->get_to())
		{
			ring_state = StateMaster;
			delay_flag = true;

			log_debug(ZONE, "We are master, sending ack");
			send_ack(msg->get_from());

			// Check for queued join requests
			process_queued_joins();
			
			// Process queues while we are master
			queue->process_token(token);
				
			// Pause short if have been receiving messages, longer otherwise.
			if (delay_flag)
				sleep_millis(TOKEN_PASS_PAUSE);

			send_token(true);
		}
		else
		{
			// Verify we are still in ring list, rejoin if not.
			if (!token->is_member(my_addr))
			{
				log_debug(ZONE, "We have been removed from ring, try rejoining");
				
				ring_state = StateNone;
				queue->on_ring_lost();
				
				send_join(true);
			}
			else
			{
				// Normal token pass to another peer, reset our watchdog timer
				int timeout = (token->index_of_member(my_addr) + 1) * TOKEN_LOST_TIMEOUT;
				link.set_timer(timeout);
			}
		}
	}
	else
		delete msg;
}

void ControlRing::process_join(ControlMessage * msg)
{
	if (msg->get_from() != my_addr)
	{
		if (ring_state == StateMaster || ring_state == StateAlone)
		{
			log_debug(ZONE, "Received join request from %s", msg->get_from().get_string());
			send_ack(msg->get_from());
			
			token->add_member(msg->get_from());	
			
			// Was alone, now have a peer, pass token
			if (ring_state == StateAlone)
			{
				ring_state = StateMaster;
				queue->on_ring_joined();
				send_token(true);
			}
			
			queue->on_node_joined(msg->get_from());
		}
		else if (ring_state == StateMember || ring_state == StatePassing)
		{
			log_debug(ZONE, "Queued join request from %s", msg->get_from().get_string());
			join_requests.add(msg);
			return;
		}
	}
	
	delete msg;
}

void ControlRing::process_queued_joins()
{
	ControlMessage * msg = (ControlMessage *)join_requests.remove_head();
	while (msg != NULL)
	{
		if (!token->is_member(msg->get_from()))
		{
			log_debug(ZONE, "Responding to join request from %s", msg->get_from().get_string());
			send_ack(msg->get_from());
			
			token->add_member(msg->get_from());	
			queue->on_node_joined(msg->get_from());
		}
		delete msg;
		msg = (ControlMessage *)join_requests.remove_head();
	}
}

// Process message
bool ControlRing::on_message(CommMessage * link_msg)
{
	int command = link_msg->peek_int();

	// From type of message, construct appropriate message object and send to handler.
	if (command >= ControlMessage::CntlMin && command <= ControlMessage::CntlMax)
	{
		ControlMessage * msg = new ControlMessage();
		msg->decode(link_msg);
		if (link_msg->is_valid())
			process_message(msg);
		else
			log_error(ZONE, "Invalid control message received, dropped");
	}
	else if (command >= BroadcastMessage::BcastMin && command <= BroadcastMessage::BcastMax)
	{
		queue->on_message(link_msg, command);
	}
	else
	{
		log_debug(ZONE, "Invalid control message received (%d), dropped", command);
		return false;
	}

	return true;
}

void ControlRing::on_timer()
{
	if (ring_state == StateNone)
	{
		if (retry <= 2)
		{
			log_debug(ZONE, "No response to ring join request, retrying");
			send_join();
		}
		else
		{
			log_debug(ZONE, "No response to ring join request, assuming we are only node");
			ring_state  = StateAlone;
			build_token();
			
			link.set_timer(JOIN_RESEND_TIMEOUT);
		}
	}
	else if (ring_state == StateMaster || ring_state == StatePassing)
	{
		if (retry <= 2)
		{
			log_debug(ZONE, "No response to token pass, retrying");
			send_token();
		}
		else
		{
			// Remove non-responsive node from ring.
			if (token->get_to().length() > 0 && token->get_to() != my_addr)
			{
				log_debug(ZONE, "No response to token pass, removing %s from ring", token->get_to().get_string());
				token->remove_member(token->get_to());
				queue->on_node_left(token->get_to());
			}
			
			// And try pass to pass token again.
			send_token(true);
		}
	}
	else if (ring_state == StateAlone)
	{
		log_debug(ZONE, "Resending join request");
		send_join(true);	
	}
	else if (ring_state == StateMember)
	{
		log_debug(ZONE, "Token timeout, resending last token");

		// Remove non-responsive node from ring.
		if (token->get_to().length() > 0 && token->get_to() != my_addr)
		{
			log_debug(ZONE, "No response to token pass, ring master %s might have failed", token->get_to().get_string());
			token->remove_member(token->get_to());
			queue->on_node_left(token->get_to());
		}
		
		// And try pass to pass token again.
		send_token(true);
	}
}

void ControlRing::on_empty()
{
}

/*
 * BroadcastMessage
 *   command
 *   from
 *   sequence
 *   <app data>
 *
 * Derived class defines encode/decode methods to add app specific data to network message.
 */

BroadcastMessage::BroadcastMessage()
{ 
	command = BcastMessage;
	sequence = 0;
	len = 0;
	data[0] = '\0';
}

// Encode message contents.
void BroadcastMessage::encode(CommMessage * msg)
{
	msg->set_int(command);
	msg->set_string(from_ip);
	msg->set_int(sequence);
	msg->set_int(len);
	
	char buffer[MAX_MESSAGE];
	log_debug(ZONE, "Encoded message: %s", dump(buffer, sizeof(buffer)));
}

// Decode message contents.
// Message will be flagged invalid if we can't properly parse it.
void BroadcastMessage::decode(CommMessage * msg)
{
	command = msg->get_int();
	from_ip = msg->get_string();
	sequence = msg->get_int();
	len = msg->get_int();
	
	char buffer[MAX_MESSAGE];
	log_debug(ZONE, "Decoded message: %s", dump(buffer, sizeof(buffer)));
}

const char * BroadcastMessage::dump(char * buffer, int buff_len)
{
	snprintf(buffer, buff_len, "%d, %s, %d", command, from_ip.get_string(), sequence);
	return buffer;
}

/*
 * BroadcastQueue
 */

BroadcastQueue::BroadcastQueue(BroadcastEvent * _handler) :
	ring(this)
{
	handler = _handler;
	last_dispatched = 0;
	last_read = -1;
}

int BroadcastQueue::initialize(const char * _my_addr, const char * broadcast_addr, int port)
{
	my_addr =_my_addr;
	
	int stat = ring.initialize(_my_addr, broadcast_addr, port);
	if (stat)
		return stat;
}

void BroadcastQueue::shutdown()
{
	log_debug(ZONE, "Broadcast queue shutting down");
	ring.shutdown();
}

// Add outgoing message.  Assumes ownership of message (caller must not delete).
int BroadcastQueue::send_message(BroadcastMessage * msg)
{
	IUseMutex lock(queue_lock);
	
	// Add to sending queue
	outgoing.add(msg);
	
	return CommOk;
}

// Reset incoming message status.
// We've joined an existing ring, set the initial state to current state of ring.
void BroadcastQueue::reset_incoming(int starting_dispatch)
{
	// Clear message queues
	log_debug(ZONE, "Resetting incoming queues");
	incoming.remove_all(true);
	last_dispatched = starting_dispatch;
	last_read = starting_dispatch;
}

// Place incoming message in appropriate place in queue.
void BroadcastQueue::queue_incoming(BroadcastMessage * msg, bool allow_self)
{
	int in_sequence = msg->get_sequence();
	
	if (msg->get_from() != my_addr || allow_self)
	{
		// Already dispatched to application, ignore.
		if (in_sequence <= last_dispatched)
		{
			log_debug(ZONE, "Duplicate sequence %d received, dropped", in_sequence);
			return;
		}
	
		IListIterator iter(incoming);
	
		int last = -1;
		BroadcastMessage * check = (BroadcastMessage *)iter.get_first();
		while (check != NULL)
		{
			last = check->get_sequence();
	
			if (check->get_sequence() >= in_sequence)
			{
				log_debug(ZONE, "Queued sequence %d", in_sequence);
				incoming.insert_before(iter, msg);
	
				// We take latest received version of message as correct version.
				if (check->get_sequence() == in_sequence)
				{
					log_debug(ZONE, "Duplicate sequence %d, removing orginal", in_sequence);
					incoming.remove(iter);
				}
	
				return;
			}
	
			check = (BroadcastMessage *)iter.get_next();
		}
	
		if (last == -1 || last < in_sequence)
		{
			// Highest seen sequence, or queue is empty, so add at end
			log_debug(ZONE, "Queued sequence %d at end", in_sequence);
			incoming.add(msg);
		}
	}
}

void BroadcastQueue::on_message(CommMessage * link_msg, int command)
{
	switch (command)
	{
	case BroadcastMessage::BcastMessage:
		{
			BroadcastMessage * msg = handler->create_message(link_msg);
			msg->decode(link_msg);
			if (link_msg->is_valid())
			{	
				// Add to receiving queue if not duplicate
				queue_incoming(msg);
			}
			else
				log_error(ZONE, "Invalid broadcast message received, dropped");
			break;
		}
	
	case BroadcastMessage::BcastQuery:
		{
			BroadcastMessage * msg = new BroadcastMessage();
			msg->decode(link_msg);
			if (link_msg->is_valid())
			{
				// Process query
			}
			else
				log_error(ZONE, "Invalid broadcast message received, dropped");
			break;
		}
	}
}

void BroadcastQueue::on_ring_joined()
{
	log_debug(ZONE, "Ring is joined");
	handler->on_ring_joined();
}

void BroadcastQueue::on_ring_lost()
{
	log_debug(ZONE, "Ring is lost");
	handler->on_ring_lost();
}

void BroadcastQueue::on_node_joined(IString & addr)
{
	log_debug(ZONE, "Node %s has joined", addr.get_string());
	handler->on_join(addr);
}

void BroadcastQueue::on_node_left(IString & addr)
{
	log_debug(ZONE, "Node %s has left", addr.get_string());
	handler->on_left(addr);
}

// We have become master, process current token.
// Note that we don't dispatch until we think we have received all available data.
void BroadcastQueue::process_token(ControlMessage * token)
{
	int stat;
	int read_sequence = token->get_read_sequence();
	int write_sequence = token->get_write_sequence();
	int last_sequence = last_dispatched;

	if (last_dispatched == -1)
		last_dispatched = read_sequence;

	// Find last consecutive message in queue
	BroadcastMessage * last = find_last_dispatchable(last_dispatched + 1);
	if (last != NULL)
		last_sequence = last->get_sequence();
	log_debug(ZONE, "Last dispatchable is %d", last_sequence);

	// We've got less data then previous node, reset read sequence and pass token
	if (last_sequence < read_sequence)
	{
		token->set_read_sequence(last_sequence);
	}
	else // last sequence >= read_sequence
	{
		// We may have some data that the previous node needs, resend and pass token
		if (read_sequence < write_sequence) 
		{
			resend_messages(token, read_sequence + 1, last_sequence);
		}

		// Life is good, we've got all data available
		if (read_sequence == write_sequence)
		{
			dispatch_incoming(token);
			
			send_outgoing(token);
		}
	}

	// Clean data from queue that we should not need anymore.
	if (last_read < read_sequence && last_read < last_dispatched)
	{
		remove_incoming(last_read);
		last_read = read_sequence;
	}
}

// Process incoming messages
void BroadcastQueue::dispatch_incoming(ControlMessage * token)
{
	int stat;

	// Dispatch as many as we can, in order.
	IListIterator iter(incoming);
	BroadcastMessage * msg = (BroadcastMessage *)iter.get_first();
	while (msg != NULL)
	{
		if (last_dispatched == -1 || msg->get_sequence() > last_dispatched)
		{
			// See if message is passed current end of queue (and hence invalid)
			if (msg->get_sequence() > token->get_write_sequence())
			{
				log_debug(ZONE, "Message %d sequence is invalid, removing", msg->get_sequence());
				IListIterator delete_iter = iter;
				msg = (BroadcastMessage *)iter.get_next();
				incoming.remove(delete_iter);
				continue;
			}
			// Don't notify about my own messages
			if (msg->get_from() != my_addr)
			{
				log_debug(ZONE, "Dispatching %d", msg->get_sequence());
				handler->on_message(msg);
			}

			last_dispatched = msg->get_sequence();
		}

		msg = (BroadcastMessage *)iter.get_next();
	}
}

// Find last message in incoming queue that we could dispatch to application.
BroadcastMessage * BroadcastQueue::find_last_dispatchable(int from_seq)
{
	BroadcastMessage * last_msg = NULL;
	
	IListIterator iter(incoming);
	
	// Move past messages in queue that were already dispatched.
	BroadcastMessage * msg = (BroadcastMessage *)iter.get_first();
	while (msg != NULL && msg->get_sequence() < from_seq)
	{
		msg = (BroadcastMessage *)iter.get_next();
	}

	// See if we have a run of messages that are deliverable.
	while (msg != NULL)
	{
		if (msg->get_sequence() == from_seq)
			last_msg = msg;
		
		++from_seq;
		msg = (BroadcastMessage *)iter.get_next();
	}

	return last_msg;
}

// Remove old messages from incoming queue.
void BroadcastQueue::remove_incoming(int before_sequence)
{
	log_debug(ZONE, "Removing messages older then %d", before_sequence);
	
	BroadcastMessage * msg = (BroadcastMessage *)incoming.get(0);
	while (msg != NULL && msg->get_sequence() < before_sequence)
	{
		msg = (BroadcastMessage *)incoming.remove_head();
		delete msg;
		msg = (BroadcastMessage *)incoming.get(0);
	}	
}

// Send outgoing messages.
// Messages sent are moved to incoming queue.
void BroadcastQueue::send_outgoing(ControlMessage * token)
{
	int last_sequence = token->get_write_sequence();
	int sent = 0;
	
	IUseMutex lock(queue_lock);
	
	BroadcastMessage * msg = (BroadcastMessage *)outgoing.remove_head();
	while (msg != NULL)
	{
		lock.unlock();
		
		// Assign message sequence (a resend will have sequence already set, so don't reset)
		if (msg->get_sequence() == 0)
		{
			msg->set_sequence(++last_sequence);
		}
		msg->set_from(my_addr);
		ring.send_message(msg);
		log_debug(ZONE, "Sent message %d", msg->get_sequence());

		lock.lock();
		
		// Add to our own incoming queue
		queue_incoming(msg, true);

		// Stop and pass token
		if (++sent == MSG_BATCH_SIZE)
		{
			break;
		}
		
		msg = (BroadcastMessage *)outgoing.remove_head();
		
		ring.set_no_delay();
	}
	
	token->set_read_sequence(last_sequence);
	token->set_write_sequence(last_sequence);
}

// Respond to nacks if we have the data.
// Even if original sender left ring, we may be able to supply data.
void BroadcastQueue::resend_messages(ControlMessage * token, int from_seq, int to_seq)
{
	IListIterator iter(incoming);	// Once sent, messages are moved here
	BroadcastMessage * msg = (BroadcastMessage *)iter.get_first();
	while (msg != NULL)
	{
		if (msg->get_sequence() >= from_seq)
		{
			log_debug(ZONE, "Resend %d because of nack", msg->get_sequence());
			ring.send_message(msg);
			
			token->set_read_sequence(msg->get_sequence());
		}
		else if (msg->get_sequence() > to_seq)
			break;
		
		msg = (BroadcastMessage *)iter.get_next();
	}	
}

