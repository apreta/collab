#ifdef _WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include "commsrvr.h"
#include "../../jcomponent/jcomponent.h"
#include "../../jcomponent/jcomponent_ext.h"
#include "port.h"

int recv_count = 0;

class Handler : public CommEvent
{
	bool on_accept(CommConnection * c);
	bool on_message(CommConnection * c, CommMessage * msg);	
	
};

int main()
{
	IThread::initialize();
	
	log_new("test");

	CommProcess::initialize();
    CommConnection::set_header("test");

	Handler handler;
	CommServer server(&handler);

	server.initialize("*", 5666);	
	
//#if 0
	CommClient client1(&handler);
	client1.initialize("localhost", 5666, true);
	
	CommMessage msg1;
	msg1.set_string("Hello 1");
	
	client1.send_message(&msg1, -1);
	
	CommClient client2(&handler);
	client2.initialize("localhost", 5666, true);
	
	CommMessage msg2;
	msg2.set_string("Hello 2");
	
	client2.send_message(&msg2, -1);

	sleep(2);

	printf("Done stream test.\n");
//#endif
	
	CommDatagram dg1(&handler);
	CommDatagram dg2(&handler);
	
	dg1.initialize("*", 5667);
	dg2.initialize("*", 5668);
	
	CommMessage msg3;
	msg3.set_string("Datagram 1");
	
	dg1.send_message("127.0.0.255", 5668, &msg3);

	CommMessage msg4;
	msg4.set_string("Datagram 2");
	
	dg2.send_message("127.0.0.1", 5667, &msg4);

	sleep(2);

	printf("Done datagram test.\n");
	
	CommProcess::shutdown();
	
	if (recv_count == 6)
		printf("PASSED\n");
	else
		printf("FAILED\n");
	
	return 0;	
}

bool Handler::on_accept(CommConnection * c)
{
	printf("Accepted connection.\n");
	
	CommMessage msg;
	msg.set_string("Goodbye");

	c->send_message(&msg, -1);

	return true;	
}

bool Handler::on_message(CommConnection * c, CommMessage * m)
{
    struct in_addr peer;
    peer.s_addr = c->get_peer_addr();
    
	printf("Recieved: %s (%s:%d)\n", m->get_string(), inet_ntoa(peer), c->get_peer_port());
	++recv_count;

	return true;	
}
