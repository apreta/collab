/* Asynchronous network library
 * (c)2005 SiteScape
 * 
 * Based on code with the following copyright:
 *
 * VNC Reflector
 * Copyright (C) 2001-2003 HorizonLive.com, Inc.  All rights reserved.
 *
 * This software is released under the terms specified in the file LICENSE,
 * included.  HorizonLive provides e-Learning and collaborative synchronous
 * presentation solutions in a totally Web-based environment.  For more
 * information about HorizonLive, please see our website at
 * http://www.horizonlive.com.
 *
 * This software was authored by Constantin Kaplinsky <const@ce.cctpu.edu.ru>
 * and sponsored by HorizonLive.com, Inc.
 *
 * $Id: async_io.h,v 1.7 2006-07-28 17:14:10 mike Exp $
 * Asynchronous file/socket I/O
 */

#ifndef _REFLIB_ASYNC_IO_H
#define _REFLIB_ASYNC_IO_H


/*
 * Data types
 */

#ifdef __cplusplus
extern "C" {
#endif

/* Just a pointer to function returning void */
typedef void (*AIO_FUNCPTR)();

typedef unsigned char AIO_BYTE;

/* This structure is used as a part of output queue */
typedef struct _AIO_BLOCK {
  struct _AIO_BLOCK *next;      /* Next block or NULL for the last block   */
  AIO_FUNCPTR func;             /* A function to call after sending block  */
  unsigned int ip_addr;         /* Address to send datagram to             */
  unsigned short ip_port;       /* Port to send datagram to                */
  size_t data_size;             /* Data size in this block                 */
  AIO_BYTE data[1];             /* Beginning of the data buffer            */
} AIO_BLOCK;

/* This structure holds the data associated with a file/socket */
typedef struct _AIO_SLOT {
  int type;                     /* To be used by the application to mark   */
                                /*   certain type of file/socket           */
  int fd;                       /* File/socket descriptor                  */
  int idx;                      /* Index in the array of pollfd structures */
  int sequence;

  char *name;                   /* Allocated string containing slot name   */
                                /*   (currently, IP address for sockets)   */

  AIO_FUNCPTR readfunc;         /* Function to process data read,          */
                                /*   to be set with aio_setread(), or with */
                                /*   aio_listen for listening socket       */
  AIO_BYTE *readbuf;            /* Pointer to an input buffer,             */
                                /*   to be set with aio_setread()          */
  int bytes_to_read;            /* Bytes to read into the input buffer     */
                                /*   to be set with aio_setread(), or size */
                                /*   of the slot structure to allocate, if */
                                /*   this is a lostening slot              */
  int bytes_ready;              /* Bytes ready in the input buffer         */
  AIO_BYTE buf1500[1500];       /* Built-in input buffer                   */

  AIO_BLOCK *outqueue;          /* First block of the output queue or NULL */
  AIO_BLOCK *outqueue_last;     /* Last block of the output queue or NULL  */
  size_t bytes_written;         /* Number of bytes written from that block */

  AIO_FUNCPTR queuefunc;        /* Called when output queue is empty       */
  AIO_FUNCPTR closefunc;        /* To be called before close, may be NULL  */

  AIO_FUNCPTR timefunc;         /* Called periodically                     */
  int release_time;

  unsigned long peer_addr;
  unsigned short peer_port;
  
  unsigned listening_f :1;      /* 1 if this slot is listening one         */
  unsigned datagram_f  :1;		/* 1 if this is a datagram socket          */
  unsigned alloc_f     :1;      /* 1 if buffer has to be freed with free() */
  unsigned close_f     :1;      /* 1 if the slot is about to be closed     */
  unsigned fd_closed_f :1;      /* 1 if fd has been closed already         */
  unsigned errio_f     :1;      /* 1 if there was an I/O problem           */
  unsigned errread_f   :1;      /* 1 if there was a problem reading data   */
  unsigned errwrite_f  :1;      /* 1 if there was a problem writing data   */

  int io_errno;                 /* Error code if errread_f or errwrite_f   */

  struct _AIO_SLOT *next;       /* To make a list of AIO_SLOT structures   */
  struct _AIO_SLOT *prev;       /* To make a list of AIO_SLOT structures   */

} AIO_SLOT;


typedef void (*AIO_WALKFUNCPTR)(AIO_SLOT *);

/*
 * Global variables
 */

extern AIO_SLOT *cur_slot;
extern AIO_SLOT *listen_slot;

/*
 * Public functions
 */

void aio_init(void);
void aio_stop(void);
int aio_set_bind_address(const char *bind_ip);
void aio_set_poll_interval(int interval);
int aio_add_slot(int fd, char *name, AIO_FUNCPTR initfunc, size_t slot_size, AIO_SLOT **new_slot);
int aio_listen(int port, AIO_FUNCPTR initfunc, AIO_FUNCPTR acceptfunc,
               size_t slot_size, AIO_SLOT **new_slot);
int aio_connect(const char * server, int port, AIO_FUNCPTR initfunc, size_t slot_size, 
               AIO_SLOT **new_slot);
int aio_datagram(int port, AIO_FUNCPTR initfunc, size_t slot_size, 
               AIO_SLOT **new_slot);
int aio_walk_slots(AIO_WALKFUNCPTR fn, int type);
AIO_SLOT *aio_find_slot(int sequence);
void aio_call_func(AIO_FUNCPTR fn, int fn_type);
void aio_close(int fatal_f);
void aio_close_other(AIO_SLOT *slot, int fatal_f);
void aio_mainloop(int block);
void aio_setread(AIO_FUNCPTR fn, void *inbuf, int bytes_to_read);
void aio_write(AIO_FUNCPTR fn, void *outbuf, int bytes_to_write);
void aio_write_other(AIO_SLOT *slot, AIO_FUNCPTR fn, void *outbuf, int bytes_to_write);
void aio_write_datagram(AIO_SLOT *slot, AIO_FUNCPTR fn, unsigned long addr, int port, 
               void *outbuf, int bytes_to_write);
void aio_write_nocopy(AIO_FUNCPTR fn, AIO_BLOCK *block);
void aio_setclose(AIO_FUNCPTR closefunc);
void aio_set_queue_empty(AIO_FUNCPTR queuefunc);
void aio_set_timer(AIO_FUNCPTR timerfunc, int interval);
int aio_time();
void aio_lock();
void aio_unlock();

#ifdef __cplusplus
}
#endif

#endif /* _REFLIB_ASYNC_IO_H */
