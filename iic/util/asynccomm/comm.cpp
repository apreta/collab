#ifdef _WIN32
#include <windows.h>
#endif

#ifdef LINUX
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

#include "../../jcomponent/util/log.h"
#include "comm.h"
#include "port.h"

CommProcess comm_thread;
bool CommProcess::shutdown_flag = false;
bool CommProcess::thread_flag = false;


/*
 * CommMessage
 */

CommMessage::CommMessage()
{
    len = 0;
    pos = 0;
    valid = true;
}

void CommMessage::set_message(unsigned char *_data, int _len)
{
    pos = 0;
    if (len <= MAX_MESSAGE)
    {
        valid = true;
        len = _len;
        memcpy(data, _data, _len);
    }
    else
    {
        len = 0;
        valid = false;
    }
}

int CommMessage::get_int()
{
    if (pos + 4 > len)
    {
        valid = false;
        return -1;
    }
    int val = buf_get_CARD32(&data[pos]);
    pos += 4;
    return val;
}

void CommMessage::set_int(int x)
{
    if (pos + 4 > MAX_MESSAGE)
    {
        valid = false;
        return;
    }
    buf_put_CARD32(&data[pos], x);
    pos += 4;
    len = pos;
}

const char * CommMessage::get_string()
{
    if (pos + 4 > len)
    {
        valid = false;
        return NULL;
    }
    int slen = buf_get_CARD32(&data[pos]);
    pos += 4;

    if (slen + 1 > MAX_STRING || pos + slen > len)
    {
        valid = false;
        return NULL;
    }
    memcpy(buffer, &data[pos], slen);
    buffer[slen] = '\0';
    pos += slen;
    return buffer;
}

void CommMessage::set_string(const char *s)
{
    int slen = strlen(s);
    if (pos + 4 + slen > MAX_MESSAGE)
    {
        valid = false;
        return;
    }
    buf_put_CARD32(&data[pos], slen);
    pos += 4;
    memcpy(&data[pos], s, slen);
    pos += slen;
    len = pos;
}

int CommMessage::peek_int()
{
    if (pos + 4 > len)
    {
        valid = false;
        return -1;
    }
    int val = buf_get_CARD32(&data[pos]);
    return val;
}

/*
 * CommProcess
 */

CommProcess::CommProcess()
	: IThread(false, true)
{
	shutdown_flag = false;
	aio_init();
}

int CommProcess::initialize(bool threaded)
{
    thread_flag = threaded;
    if (thread_flag)
        // Communication with server handled by a separate thread.
        comm_thread.start();
    return CommOk;
}

int CommProcess::shutdown()
{
    shutdown_flag = true;
    aio_stop();
    if (thread_flag)
        comm_thread.join();
    else
        aio_mainloop(1);
    return CommOk;
}

void CommProcess::run()
{
    while (!shutdown_flag)
    {
        // Process I/O
        aio_mainloop(1);
    }
}

void CommProcess::process()
{
    aio_mainloop(0);
}


/*
 * CommConnection
 */

CommConnection::CommConnection(CommEvent * _handler)
{
    handler = _handler;
    state = CommNone;
    slot = NULL;
    send_timeout = -1;
}

bool CommConnection::is_connected()
{
    bool result;
    aio_lock();
    result = slot == NULL ? false : true;
    aio_unlock();
    return result;
}

// Queue message and optionally set timer.
// (timer will be sent once message is written to socket)
int CommConnection::send_message(CommMessage * msg, int timeout)
{
    char buff[MAX_MESSAGE + 4];
    int status = CommOk;

    aio_lock();

    if (slot != NULL)
    {
        send_timeout = timeout;

        if (message_len_size == 4)
        {
            buf_put_CARD32(buff, msg->get_len());
        }
        else if (message_len_size == 2)
        {
            buf_put_CARD16(buff, msg->get_len());
        }

        memcpy(buff + message_len_size, msg->get_data(), msg->get_len());
        aio_write_other((AIO_SLOT *)slot, message_sent, buff, msg->get_len() + message_len_size);
    }
    else
    {
        status = CommNotConnected;
    }

    aio_unlock();

    return status;
}

// Set connection timer event.
// (must be called in context of main loop callback)
void CommConnection::set_timer(int timeout)
{
	send_timeout = timeout;
	aio_set_timer(timer_hook, timeout);
}

// Cancel connection timer event.
// (must be called in context of main loop callback)
void CommConnection::cancel_timer()
{
    send_timeout = -1;
    aio_set_timer(NULL, 0);
}

// Close connection.
// *** delete if remote = true ***
void CommConnection::close(bool remove)
{
    aio_close_other((AIO_SLOT *)slot, 0);
}

// Get address of peer.
// N.B. for unbound datagram sockets will return peer for last message received.
unsigned long CommConnection::get_peer_addr()
{
    return slot->s.peer_addr;
}

unsigned short CommConnection::get_peer_port()
{
    return slot->s.peer_port;
}

void CommConnection::set_header(const char * _header)
{
	CommConnection::header = _header;
}

void CommConnection::set_message_len_size(int size)
{
    CommConnection::message_len_size = size;
}

void CommConnection::on_timer()
{
	if (send_timeout > -1)
		handler->on_timer(this);
}

void CommConnection::on_empty()
{
	handler->on_empty(this);
}

void CommConnection::init_hook()
{
    int header_len = strlen(header);
    cur_slot->type = CommStream;
    aio_setclose(connection_closed);
    aio_set_queue_empty(queue_empty);
    aio_write(NULL, (void *)header, header_len);
    aio_setread(header_read, NULL, header_len);
}

void CommConnection::listen_hook()
{
    cur_slot->type = CommListen;
}

void CommConnection::datagram_hook()
{
    cur_slot->type = CommDatagram;
    aio_setclose(connection_closed);
    aio_set_queue_empty(queue_empty);
    aio_setread(message_read, NULL, -MAX_DATAGRAM);
}

void CommConnection::accept_hook()
{
    CommSlot * slot = (CommSlot *)listen_slot;

	CommConnection * c = new CommConnection(slot->client->handler);
	c->slot = (CommSlot *)cur_slot;
	c->slot->client = c;

	// Set up reads/write for header exchange
	init_hook();
	cur_slot->type = CommAccepted;

	// Let upper layers know about new connection
	slot->client->handler->on_accept(c);
}

void CommConnection::connection_closed(void)
{
    CommSlot * slot = (CommSlot *)cur_slot;

    slot->client->state = CommNone;

    /* >> delete queued data << */

    if (cur_slot->errread_f) {
        if (cur_slot->io_errno) {
            log_debug(ZONE, "Host I/O error, read: %s", _strerror(cur_slot->io_errno));
        } else {
            log_debug(ZONE, "Host I/O error, read");
        }
    } else if (cur_slot->errwrite_f) {
        if (cur_slot->io_errno) {
            log_debug(ZONE, "Host I/O error, write: %s", _strerror(cur_slot->io_errno));
        } else {
            log_debug(ZONE, "Host I/O error, write");
        }
    } else if (cur_slot->errio_f) {
        log_debug(ZONE, "Host I/O error");
    }

    log_debug(ZONE, "Closing connection to host");

	slot->client->handler->on_close(slot->client);

    // Make sure we're not sending
    slot->client->slot = NULL;
	slot->client->state = CommNone;

    if (cur_slot->type == CommAccepted)
    {
        delete slot->client;
        slot->client = NULL;
    }
}

void CommConnection::header_read()
{
  int header_len = strlen(header);
  if (strncmp((const char *)cur_slot->readbuf, header, header_len) != 0)
  {
      log_error(ZONE, "Invalid header received, closing connection");
      aio_close(0);
      return;
  }

  aio_setread(message_len_read, NULL, message_len_size);
}

void CommConnection::message_len_read()
{
    int len = 0;

    if (message_len_size == 4)
    {
        len = buf_get_CARD32(cur_slot->readbuf);
    }
    else if (message_len_size == 2)
    {
        len = buf_get_CARD16(cur_slot->readbuf);
    }

    if (len < 0 || len > MAX_MESSAGE)
    {
        log_error(ZONE, "Invalid message len received: %d", len);
        aio_close(0);
        return;
    }

    aio_setread(message_read, NULL, len);
}

void CommConnection::message_read()
{
    CommMessage msg;
    CommSlot * slot = (CommSlot *)cur_slot;

    /* Dispatch command */
    msg.set_message(cur_slot->readbuf, cur_slot->bytes_ready);

    /* No other input will be processed until this function exits.
       However, other threads may need to send before we finish this message 
       so release lock while in handler. */
    aio_unlock();
    bool res = slot->client->handler->on_message(slot->client, &msg);
    aio_lock();
    
    if (!res)
    {
        log_error(ZONE, "Command processing error, connection closed");
        aio_close(0);
        return;
    }

	if (cur_slot->type == CommDatagram)
		aio_setread(message_read, NULL, -MAX_DATAGRAM);
	else
		aio_setread(message_len_read, NULL, message_len_size);
}

void CommConnection::message_sent(void)
{
    CommMessage msg;
    CommSlot * slot = (CommSlot *)cur_slot;
	int timeout = slot->client->send_timeout;

	if (timeout > -1)
	{
		aio_set_timer(timer_hook, timeout);
	}
}

void CommConnection::queue_empty(void)
{
    CommSlot * slot = (CommSlot *)cur_slot;
	if (slot->client)
		slot->client->on_empty();
}

void CommConnection::timer_hook(void)
{
    CommSlot * slot = (CommSlot *)cur_slot;
	if (slot->client)
		slot->client->on_timer();
}

const char * CommConnection::header = NULL;
int CommConnection::message_len_size = 4;   // 4 bytes is default


/*
 * CommClient
 */

CommClient::CommClient(CommEvent * _handler) :
	CommConnection(this)
{
	handler = _handler;
}

int CommClient::initialize(const char * _server, int _port, bool _reconnect)
{
    server = _server;
    port = _port;
	reconnect = _reconnect;

    if (reconnect)
        return connect();
    return CommOk;
}

int CommClient::connect()
{
	while (!CommProcess::is_shutdown())
	{
		if (aio_connect(server, port, init_hook, sizeof(CommSlot), (AIO_SLOT **)&slot) == 1)
			break;

		if (!reconnect)
			return CommConnectFailed;

		sleep(2);
	}

	slot->client = this;
	state = CommConnected;

    return CommOk;

}

bool CommClient::on_message(CommConnection * c, CommMessage * msg)
{
	return handler->on_message(c, msg);
}

bool CommClient::on_close(CommConnection * c)
{
	bool res = handler->on_close(c);

	// We've been notified that previous connection failed, so try to reconnect.
    if (reconnect && !CommProcess::is_shutdown())
        connect();

	return res;
}

bool CommClient::on_error(CommConnection * c)
{
	return handler->on_error(c);
}

bool CommClient::on_timer(CommConnection * c)
{
	return handler->on_timer(c);
}

/*
 * CommDatagram
 */

CommDatagram::CommDatagram(CommEvent * _handler) :
	CommConnection(this)
{
	handler = _handler;
}

int CommDatagram::initialize(const char * _my_addr, int _port)
{
	my_addr = _my_addr;
    port = _port;

	aio_set_bind_address(my_addr);

	if (aio_datagram(port, datagram_hook, sizeof(CommSlot), (AIO_SLOT**)&slot) != 1)
		return CommDatagramFailed;

	slot->client = this;
	state = CommConnected;

	return CommOk;
}

int CommDatagram::send_message(const char * addr, int port, CommMessage * msg, int timeout)
{
    unsigned long ip = inet_addr(addr);
    return send_message(ip, port, msg, timeout);
}

int CommDatagram::send_message(unsigned long addr, int port, CommMessage * msg, int timeout)
{
    int status = CommOk;

	if (msg->get_len() > MAX_DATAGRAM)
		return CommMessageTooLong;

    aio_lock();

    if (slot != NULL)
    {
        send_timeout = timeout;

        aio_write_datagram((AIO_SLOT *)slot, message_sent, addr, port, msg->get_data(), msg->get_len());
    }
    else
    {
        status = CommNotConnected;
    }

    aio_unlock();

    return status;
}

bool CommDatagram::on_message(CommConnection * c, CommMessage * msg)
{
	return handler->on_message(c, msg);
}

bool CommDatagram::on_close(CommConnection * c)
{
	// Shouldn't  happen on datagram socket except on error.
	bool res = handler->on_close(c);
	return res;
}

bool CommDatagram::on_error(CommConnection * c)
{
	return handler->on_error(c);
}

bool CommDatagram::on_timer(CommConnection * c)
{
	return handler->on_timer(c);
}

bool CommDatagram::on_empty(CommConnection * c)
{
	return handler->on_empty(c);
}

