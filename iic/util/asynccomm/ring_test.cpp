#ifdef _WIN32
#include <windows.h>
#endif

#include <stdio.h>
#include "ring.h"
#include "../../jcomponent/jcomponent.h"
#include "../../jcomponent/jcomponent_ext.h"
#include "port.h"

int sent_count = 0;
int recv_count = 0;

BroadcastQueue * g_queue = NULL;

class Handler : public BroadcastEvent
{
	void on_query(BroadcastMessage * msg)
	{
	
	}
	
	void on_message(BroadcastMessage * msg)
	{
		printf("Message %d recieved\n", msg->get_sequence());
		recv_count++ ;
	}
	
	void on_ring_lost() 
	{ 
		printf("Left ring\n");
	}
	void on_ring_joined() 
	{
		printf("Joined ring\n"); 
	}
	void on_join(const char * new_addr)
	{ 
		printf("%s joined\n", new_addr);
	}
	void on_left(const char * left_addr)
	{ 
		printf("%s left\n", left_addr);
	}
};

int main(int argc, char * argv[])
{
	IThread::initialize();
	
	char * log = argv[1];
	char * my_addr = argv[2];
	char * bcast_addr = argv[3];
	
	if (argc == 1)
	{
		log = "test";
		my_addr = "192.168.254.1";
		bcast_addr = "192.168.254.255";
	}
	
	log_new(log);
	//log_set_level(0);
	
	Handler handler;
	
	CommProcess::initialize();
	
	BroadcastQueue queue(&handler);
	g_queue = &queue;
	
	queue.initialize(my_addr , bcast_addr, 5666);

	int ch;
	printf("Press any key to send a message, 'q' to exit\n");
	while ((ch = getchar()) != 'q')
	{
		if (ch == 'm')
		{
			int i;
			for (i = 0; i < 1000; i++)
			{
				BroadcastMessage * msg = new BroadcastMessage();
				queue.send_message(msg);
			}
			printf("%d Messages sent\n", i);
		}
		else
		{
			BroadcastMessage * msg = new BroadcastMessage();
			queue.send_message(msg);
			printf("Message sent\n");
		}
	}
	
	printf("Done broadcast test.\n");
	
	queue.shutdown();
	// CommProcess::shutdown();
	
	return 0;	
}
