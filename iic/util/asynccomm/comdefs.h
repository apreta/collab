#ifndef COMDEFS_H
#define COMDEFS_H

#define MAX_DATAGRAM 512
#define MAX_MESSAGE 512
#define MAX_STRING 128


#ifndef CARD32

#ifdef LINUX

#define CARD8   u_int8_t
#define CARD16  u_int16_t
#define CARD32  u_int32_t

#else

typedef unsigned long CARD32;
typedef unsigned short CARD16;
typedef unsigned char  CARD8;

#endif

#endif /* CARD32 */


#define buf_get_CARD8(buf)                      \
  (*((CARD8 *)buf))

#define buf_get_CARD16(buf)                     \
  ((CARD16)((CARD8 *)buf)[0] << 8 |             \
   (CARD16)((CARD8 *)buf)[1])

#define buf_get_CARD32(buf)                     \
  ((CARD32)((CARD8 *)buf)[0] << 24 |            \
   (CARD32)((CARD8 *)buf)[1] << 16 |            \
   (CARD32)((CARD8 *)buf)[2] << 8  |            \
   (CARD32)((CARD8 *)buf)[3]);

#define buf_put_CARD8(buf, value)               \
  *((CARD8 *)buf) = (CARD8)value;

#define buf_put_CARD16(buf, value)              \
{                                               \
  ((CARD8 *)buf)[0] = (CARD8)(value >> 8);      \
  ((CARD8 *)buf)[1] = (CARD8)value;             \
}

#define buf_put_CARD32(buf, value)              \
{                                               \
  ((CARD8 *)buf)[0] = (CARD8)(value >> 24);     \
  ((CARD8 *)buf)[1] = (CARD8)(value >> 16);     \
  ((CARD8 *)buf)[2] = (CARD8)(value >> 8);      \
  ((CARD8 *)buf)[3] = (CARD8)value;             \
}


#endif
