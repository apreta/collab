/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef IOBJECT_H
#define IOBJECT_H

#include <assert.h>
#include <glib.h>

class IList;
class IVector;

// Yes, the C++ world uses templates now.  But I can't stand the
// code bloat that results from using templates.

// Base object class.
class IObject
{
public:
	IObject() {}
	virtual ~IObject() {}

	virtual int type() = 0;
	virtual unsigned int hash() = 0;
	virtual bool equals(IObject *obj) = 0;
    virtual void reference()
    {
    }
    virtual void dereference()
    {
        delete this;
    }
	
	// For use in GLIB functions
    static guint shash(const void *pobj)
	{ 
		return ((IObject*)pobj)->hash();
	}
	
    static gboolean sequals(const void *pobj1, const void *pobj2)
	{
		return ((IObject*)pobj1)->equals((IObject*)pobj2);
	}
	
	static void sdestroy(void *str)
	{
        ((IObject*)str)->dereference();
	}
};

class IUnsigned : public IObject
{
    enum { TYPE_UNSIGNED = 5425 };
    unsigned self;

public:
    IUnsigned(unsigned _self)
    {
        self = _self;
    }

	int type()
	{
		return TYPE_UNSIGNED;
	}
	
    // Insufficient to use in collection other then list
	unsigned int hash()
	{
		return self;
	}
	
	bool equals(IObject *o)
	{
        if (o->type() == TYPE_UNSIGNED && self == ((IUnsigned *)o)->self)
            return true;
        return false;
	}
};

class IListIterator
{
    friend class IList;
    IList *list;
    GList *pos;

public:
    IListIterator()
    {
        list = NULL;
        pos = NULL;
    }
    IListIterator(IList & _list)
    {
        list = &_list;
        pos = NULL;
    }

    void set_list(IList & _list)
    {
        list = &_list;
        pos = NULL;
    }
        
    IObject * get_first();
    IObject * get_next();
};

// Not suitable for sorting
inline gint scompare(const void * a, const void * b)
{
	return ((IObject*)a)->equals((IObject*)b) ? 0 : 1;
}

class IList
{
protected:
    friend class IListIterator;
    GList *list;

public:
    IList()
    {
        list = NULL;
    }

    ~IList()
    {
		remove_all();
    }

	void copy(IList &copy)
	{
		copy.remove_all();

        if (list != NULL)
        {
			// Walk through and copy this list to target list
			GList *elem = g_list_first (list);
			while (elem != NULL)
			{
				GList * next = g_list_next(elem); 
				IObject *to_copy = (IObject *) elem->data;
				copy.add(to_copy);
				elem = next; 
			}
		}
    }

    void remove_all(bool del_elements = true)
    {
        if (list != NULL)
        {
            // Walk through and remove elements.
			if (del_elements)
			{
				GList *elem = g_list_first (list);
				while (elem != NULL)
				{
					GList * next = g_list_next(elem); 
					((IObject *) elem->data)->dereference();
					elem = next; 
				}
			}

            g_list_free(list);
			list = NULL;
        }
    }

    void add(IObject *obj)
    {
        list = g_list_append(list, obj);
        obj->reference();
    }

    void prepend(IObject *obj)
    {
        list = g_list_prepend(list, obj);
        obj->reference();
    }

	void insert_before(IListIterator & iter, IObject *obj)
	{
		GList *elem = (GList *) iter.pos;
		list = g_list_insert_before(list, elem, obj);
		obj->reference();
	}

    IObject * get(int n)
    {
        if (list == NULL)
            return NULL;

        return (IObject*) g_list_nth_data(list, n);
    }
	
    IObject * find(IObject *obj)
    {
		if (list == NULL)
			return NULL;
		
        GList *elem = g_list_find_custom(list, obj, scompare);
        return elem != NULL ? (IObject*) elem->data : NULL;
    }

    bool remove(IObject *obj, bool del_element = true)
    {
		if (list == NULL)
			return false;
		
        GList *elem = g_list_find(list, obj);
        if (elem != NULL)
        {
            list = g_list_remove_link(list, elem);
            g_list_free_1(elem);
			if (del_element)
				obj->dereference();
            return true;
        }
        else
            return false;
    }

    void remove(IListIterator & iter)
    {
        GList *elem = (GList *) iter.pos;
        list = g_list_remove_link(list, elem);
        ((IObject*)elem->data)->dereference();
        g_list_free_1(elem);
    }

    IObject * remove_head()
    {
		if (list == NULL)
			return NULL;
		
        GList *head = g_list_first(list);
        if (head != NULL)
        {
            IObject *obj = (IObject *)head->data;
            list = g_list_remove(list, obj);
            return obj;
        }
        return NULL;
    }

    IObject * remove_tail()
    {
		if (list == NULL)
			return NULL;
		
        GList *tail = g_list_last(list);
        if (tail != NULL)
        {
            IObject *obj = (IObject *)tail->data;
            list = g_list_remove(list, obj);
            return obj;
        }
        return NULL;
    }

    bool empty()
    {
        return list == NULL;
    }

    int size()
    {
        return g_list_length(list);
    }
};

class IVectorIterator
{
    IVector * vector;
    int pos;
    
public:
    IVectorIterator()
    {
        vector = NULL;
        pos = 0;
    }

    IVectorIterator(IVector & _vector)
    {
        set_vector(_vector);
    }

    void set_vector(IVector & _vector)
    {
        vector = &_vector;
        pos = 0;
    }

    IObject * get_first();
    IObject * get_next();
    IObject * get(int idx);
};

class IVector
{
protected:
	GPtrArray *array;

public:
	IVector()
	{
		array = g_ptr_array_new();
	}
	
    ~IVector()
	{
        remove_all();
	}

    void remove_all()
    {
		for (unsigned i=0; i<array->len; i++)
		{
			if (g_ptr_array_index(array, i) != NULL)
				((IObject*)g_ptr_array_index(array, i))->dereference();
		}
		g_ptr_array_free(array, TRUE);
    }

	void add(IObject *obj)
	{
		g_ptr_array_add(array, obj);
        obj->reference();
	}
	
	bool remove(int idx)
	{
		assert(idx >= 0);
		IObject *val = (IObject*)g_ptr_array_index(array, idx);
		bool fnd = g_ptr_array_remove_index(array, idx) != NULL;
		if (fnd)
			val->dereference();
		return fnd;
	}

    void set(int idx, IObject *obj)
    {
		assert (idx >= 0);
        if (idx >= size())
            g_ptr_array_set_size(array, idx+1);
        g_ptr_array_index(array, idx) = obj;
        if (obj)
            obj->reference();
    }

	IObject *get(int idx)
	{
		assert (idx >= 0);
		if (idx >= size() || idx < 0)
			return NULL;
		return (IObject *)g_ptr_array_index(array, idx);
	}
	
	int size()
	{
		return array->len;
	}
	
	void clear()
	{
		for (unsigned i=0; i<array->len; i++)
        {
            IObject * obj = (IObject*)g_ptr_array_index(array, i);
            if (obj)
                obj->dereference();
        }
		g_ptr_array_set_size(array, 0);
	}
};

typedef void (*IEnumFunc)(IObject * key, IObject * obj, void * user_param);

struct IEnumInfo
{
	IEnumFunc func;
	void * user_param;
};

inline void hash_enum_func(gpointer key, gpointer value, gpointer user)
{
	IEnumInfo * info = (IEnumInfo *) user;
	IEnumFunc func = info->func;
    
	func((IObject *)key, (IObject *)value, info->user_param);
}

inline void values_of_enum_func(gpointer key, gpointer value, gpointer user)
{
	IVector * result = (IVector *) user;
    
    result->add((IObject *)value);
}

inline void keys_of_enum_func(gpointer key, gpointer value, gpointer user)
{
	IVector * result = (IVector *) user;
    
    result->add((IObject *)key);
}

class IHash
{
  GHashTable *table;
 
public:
  IHash()
  {
    table = g_hash_table_new_full(IObject::shash, IObject::sequals, IObject::sdestroy, IObject::sdestroy);
  }
  
  ~IHash()
  {
	// This should cleanup all contained data, by calling destroy functions
	g_hash_table_destroy(table);
  }

  // Remove and delete/deference all contained objects.
  void remove_all()
  {
	g_hash_table_destroy(table);
    table = g_hash_table_new_full(IObject::shash, IObject::sequals, IObject::sdestroy, IObject::sdestroy);
  }
  
  void insert(IObject *key, IObject *value)
  {
    g_hash_table_replace(table, key, value);	// Replace uses new key in place of old key if matching key is found
    key->reference();
    value->reference();
  }
  
  IObject *get(IObject *key)
  {
    return (IObject*) g_hash_table_lookup(table, key);
  }

  void remove(IObject *key, bool del_element = true)
  {
    IObject *orig_key, *value;
    
    if (g_hash_table_lookup_extended(table, key, (void**)&orig_key, (void**)&value))
    {
        g_hash_table_steal(table, key);
        orig_key->dereference();
		if (del_element)
			value->dereference();
    }
  }

  int size()
  {
	  return g_hash_table_size(table);
  }

  void for_each(IEnumFunc func, void * user_param)
  {
	  IEnumInfo info = { func, user_param };
	  g_hash_table_foreach(table, hash_enum_func, (void*)&info);
  }

  void values_of(IVector & vec) { g_hash_table_foreach(table, values_of_enum_func, (void *)&vec); }

  void keys_of(IVector & vec) { g_hash_table_foreach(table, keys_of_enum_func, (void *)&vec); }
};


inline IObject *IListIterator::get_first()
{
    if (list->list == NULL)
        return NULL;

    pos =  g_list_first(list->list);
    return (IObject *)pos->data;
}

inline IObject *IListIterator::get_next()
{
    if (pos == NULL)
        return NULL;
    pos = g_list_next(pos);
    if (pos == NULL)
        return NULL;

    return (IObject *)pos->data;
}

inline IObject *IVectorIterator::get_first()
{
    if (vector == NULL)
        return NULL;

    pos = 0;
    return vector->get(pos);
}

inline IObject *IVectorIterator::get_next()
{
    if (vector == NULL || pos + 1 >= vector->size())
        return NULL;

   return vector->get(++pos);
}

inline IObject *IVectorIterator::get(int idx)
{
    if (vector == NULL)
        return NULL;

    return vector->get(idx);
}

#endif
