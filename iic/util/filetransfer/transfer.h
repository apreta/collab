#ifndef FILETRANSFER_H
#define FILETRANSFER_H

#ifdef __cplusplus
extern "C" {
#endif

/* Fetches single file */
int fetch_file(const char * url, const char * filename, const char * username, const char * passwd);

/* Fetches archive and unzips into directory */
int fetch_archive(const char * url, const char * filename, const char * out_path);

/* Sends single file to destination */
int upload_file(const char * filename, const char * url, const char * username, const char * passwd);

#ifdef __cplusplus
}
#endif

#endif
