
#include <stdio.h>

#include <curl/curl.h>
#include <curl/types.h>
#include <curl/easy.h>
#include <linux/limits.h>

#include "../../jcomponent/util/log.h"
#include "transfer.h"


size_t
write_response(void *ptr, size_t size, size_t nmemb, void *data)
{
  FILE *writehere = (FILE *)data;
  return fwrite(ptr, size, nmemb, writehere);
}

int fetch_file(const char * url, const char * filename, const char * username, const char * passwd)
{
  CURL *curl;
  CURLcode res = 0;
  FILE *outfile;
#ifdef LOG_HEADERS
  FILE *respfile;
#endif

  /* local file name to store the file as */
  outfile = fopen(filename, "wb");
  if (outfile == NULL)
  {
  	log_error(ZONE, "Failed to open file: %s", filename);
  	return -1;
  }

  /* local file name to store the server's response lines in */
#ifdef LOG_HEADERS
  respfile = fopen("file-responses", "wb");
#endif

  log_debug(ZONE, "Fetching file from %s", url);

  curl = curl_easy_init();
  if(curl)
  {
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_FILE, outfile);
#ifdef LOG_HEADERS
    curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, write_response);
    curl_easy_setopt(curl, CURLOPT_WRITEHEADER, respfile);
#endif
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1);
	curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 20);	// 20 seconds
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, 240);	// 4 minutes
	if (username)
	{
        curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_easy_setopt(curl, CURLOPT_USERNAME, username);
        curl_easy_setopt(curl, CURLOPT_PASSWORD, passwd);
	}

    res = curl_easy_perform(curl);
	if (res)
	{
	    log_error(ZONE, "Failed to fetch file from: %s (%d)", url, res);
	}
	else
	{
		log_debug(ZONE, "Fetch complete, stored in %s", filename);
	}

    /* always cleanup */
    curl_easy_cleanup(curl);
  }
  else
  {
	  log_error(ZONE, "Error initializing libcurl, fetch failed");
	  res = -1;
  }

  fclose(outfile); /* close the local file */
#ifdef LOG_HEADERS
  fclose(respfile); /* close the response file */
#endif

  return res;
}

/* fetch zip file and unarchive into indicated directory */
int fetch_archive(const char * url, const char * filename, const char * out_path)
{
	int res = 0;
	char buffer[2*PATH_MAX+32];

	res = fetch_file(url, filename, NULL, NULL);
	if (res == 0)
	{
		log_debug(ZONE, "Fetch ok, unzipping archive into %s", out_path);
		sprintf(buffer, "/usr/bin/unzip -o %s -d %s", filename, out_path);
		res = system(buffer);
		if (res)
		{
			log_error(ZONE, "Unable to unzip archive %s", filename);
		}
	}

	return res;
}

int copy_file(const char * ifile, const char * ofile)
{
    FILE *ifp = fopen(ifile, "rb");
    if (ifp == NULL)
    {
        return -1;
    }

    FILE *ofp = fopen(ofile, "wb");
    if (ofp == NULL)
    {
        fclose(ifp);
        return -1;
    }

    char buf[8192];
    long total_size = 0;
    while (!feof(ifp))
    {
        size_t i_actual = fread(buf, sizeof(char), sizeof(buf), ifp);
        total_size += i_actual;
        if (i_actual == sizeof(buf) || !ferror(ifp))
        {
            size_t o_actual = fwrite(buf, sizeof(char), i_actual, ofp);
            if (i_actual != o_actual)
            {
                fclose(ifp);
                fclose(ofp);

                return -1;
            }
        }
    }

    fclose(ifp);
    fclose(ofp);

    return 0;
}

/* Sends single file to destination */
int upload_file(const char * filename, const char * url, const char * username, const char * password)
{
  CURL *curl;
  CURLcode res = 0;
  FILE *outfile;
#ifdef LOG_HEADERS
  FILE *respfile;
#endif

  if (strncmp(url, "file://", 7) == 0)
  {
	  return copy_file(filename, url + 7);
  }

  /* local file name */
  outfile = fopen(filename, "rb");
  if (outfile == NULL)
  {
  	log_error(ZONE, "Failed to open file: %s", filename);
  	return -1;
  }

  /* local file name to store the server's response lines in */
#ifdef LOG_HEADERS
  respfile = fopen("file-responses", "wb");
#endif

  log_debug(ZONE, "Sending file to %s", url);

  curl = curl_easy_init();
  if(curl)
  {
    curl_easy_setopt(curl, CURLOPT_UPLOAD, 1);
    curl_easy_setopt(curl, CURLOPT_URL, url);
	if (username)
	{
        curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_easy_setopt(curl, CURLOPT_USERNAME, username);
        curl_easy_setopt(curl, CURLOPT_PASSWORD, password);
	}
    curl_easy_setopt(curl, CURLOPT_READDATA, outfile);
#ifdef LOG_HEADERS
    curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, write_response);
    curl_easy_setopt(curl, CURLOPT_WRITEHEADER, respfile);
#endif
	curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1);
	curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1);
	curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 20);	// 20 seconds
	curl_easy_setopt(curl, CURLOPT_TIMEOUT, 60);

    res = curl_easy_perform(curl);
	if (res)
	{
	    log_error(ZONE, "Failed to send file to: %s (%d)", url, res);
	}
	else
	{
		log_debug(ZONE, "Send complete: %s", filename);
	}

    /* always cleanup */
    curl_easy_cleanup(curl);
  }
  else
  {
	  log_error(ZONE, "Error initializing libcurl, upload failed");
	  res = -1;
  }

  fclose(outfile); /* close the local file */
#ifdef LOG_HEADERS
  fclose(respfile); /* close the response file */
#endif

  return res;
}
