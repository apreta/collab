/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef ITHREAD_H
#define ITHREAD_H

#include <glib.h>

#include "iobject.h"


typedef void *(*IThreadFunc)(void *);
typedef void (*IThreadPoolFunc)(void *, void *);


// Not tested yet.
class IProcess
{
	int pid;
	GError * error;

public:
	bool spawn(const char * directory, char ** argv)
	{
		gboolean status = g_spawn_async_with_pipes(directory,
                                             argv,
                                             NULL,
                                             (GSpawnFlags)0,		// Flags
                                             NULL,
                                             NULL,
                                             &pid,
                                             NULL,	// Std In
                                             NULL,  // Std Out
                                             NULL,	// Std Err
                                             &error);
		return status != 0;
	}

	const char * get_error()		{ return error->message; }
};


class IThread : IObject
{
    enum { TYPE_THREAD = 5489 };

    GThread *thread;
    bool joinable;

    static void * thread_func(void * data)
    {
        IThread * _this = (IThread *)data;
        _this->run();
        return NULL;
    }
    
public:
	enum ThreadPriority { LOW, NORMAL, HIGH };

    IThread(IThreadFunc func, void * data, bool _joinable = FALSE)
    {
        joinable = _joinable;
        thread = g_thread_create(func, data, joinable, NULL);
    }
    
    IThread(bool _start = FALSE, bool _joinable = FALSE)
    {
        joinable = _joinable;
        if (_start)
            thread = g_thread_create(thread_func, this, joinable, NULL);
        else
            thread = NULL;
    }

    // Only for classes derived from IThread.
    void start()
    {
        if (thread == NULL)
            thread = g_thread_create(thread_func, this, joinable, NULL);
    }
        
    // Returns when thread exits.
    void *join()
    {
        if (thread != NULL && joinable)
            return g_thread_join(thread);
        else
            return NULL;
    }

	void set_priority(int priority)
	{
		GThreadPriority set = G_THREAD_PRIORITY_NORMAL;
		switch (priority)
		{
		case LOW:
			set = G_THREAD_PRIORITY_LOW;
			break;
		case HIGH:
			set = G_THREAD_PRIORITY_HIGH;
			break;
		case NORMAL:
		default:
			set = G_THREAD_PRIORITY_NORMAL;
			break;
		}

		if (thread != NULL)
			g_thread_set_priority(thread, set);
	}

    virtual void run()
    {
    }
        
    // Call at least once during system startup.
    static void initialize()
    {
		if (!g_thread_supported())
			g_thread_init(NULL);
    }

    int type()
    {
        return TYPE_THREAD;
    }

    unsigned int hash()
    {
        return (unsigned int)(long)thread;
    }

    bool equals(IObject *thread)
    {
        if (thread->type() != TYPE_THREAD)
            return false;
        return (((IThread*)thread)->thread == this->thread);
    }
};

class IThreadPool
{
	GThreadPool * pool;

public:
	IThreadPool(IThreadPoolFunc func, void * data, int num_threads)
	{
		pool = g_thread_pool_new(func, data, num_threads, TRUE, NULL);
	}
	~IThreadPool()
	{
		stop();
	}

	void push(void * data)
	{
		g_thread_pool_push(pool, data, NULL);
	}

	void stop()
	{
		g_thread_pool_free(pool,/*immediate*/ FALSE, /*wait*/ TRUE);
	}
	
};

class IMutex
{
    GStaticRecMutex mutex;

public:
    IMutex()
    {
        g_static_rec_mutex_init(&mutex);
    }

    ~IMutex()
    {
        g_static_rec_mutex_unlock_full(&mutex);
        g_static_rec_mutex_free(&mutex);
    }

    void lock()
    {
        g_static_rec_mutex_lock(&mutex);
    }

    void unlock()
    {
        g_static_rec_mutex_unlock(&mutex);
    }
};


class IUseMutex
{
    IMutex & mutex;
    bool locked;

public:
    IUseMutex(IMutex & _mutex) : mutex(_mutex)
    {
        mutex.lock();
        locked = true;
    }

    ~IUseMutex()
    {
        if (locked)
            mutex.unlock();
    }

    void lock()
    {
        if (!locked)
        {
            mutex.lock();
            locked = true;
        }
    }

    void unlock()
    {
        if (locked)
        {
            mutex.unlock();
            locked = false;
        }
    }
};


class IAsyncQueue
{
    GAsyncQueue * queue;

public:
    IAsyncQueue()
    {
        queue = g_async_queue_new();
    }

    // Create reference to same queue.
    IAsyncQueue(IAsyncQueue & copy)
    {
        queue = copy.queue;
        g_async_queue_ref(queue);
    }

    ~IAsyncQueue()
    {
        g_async_queue_unref(queue);
    }

    void push(void *obj)
    {
        g_async_queue_push(queue, obj);
    }

    void * pop(int timeout)
    {
        if (timeout == -1)
            return g_async_queue_pop(queue);
        else if (timeout == 0)
            return g_async_queue_try_pop(queue);
        else {
            GTimeVal time;
            g_get_current_time(&time);
            g_time_val_add(&time, timeout * 1000);
            return g_async_queue_timed_pop(queue, &time);
        }
    }

};

/**
 * Provides conditional variable that threads can block on.  Also includes
 * built-in mutex that can be locked/unlocked.  The wait() and timed_wait()
 * methods unlock this mutex just prior to blocking.
 * Note: thread init must be called prior to using this 
 */
class ICondVar
{
    GCond* cond;
    GMutex* mutex;

public:
    ICondVar()
    {
        if (!g_thread_supported())
            g_thread_init(NULL);
        cond = g_cond_new();
        mutex = g_mutex_new();
    }

    ~ICondVar()
    {
        g_cond_free(cond);
        g_mutex_free(mutex);
    }

    void signal()
    {
        g_cond_signal(cond);
    }

    void broadcast()
    {
        g_cond_broadcast(cond);
    }

    /**
     * Blocks the thread on the condition. 
     * Unlocks the built-in mutex just prior to blocking.
     */
    void wait()
    {
        g_cond_wait(cond, mutex);
    }

    /**
     * Waits for thread to be woken up but no longer
     * than millisecs time. Unlocks the built-in
     * mutex just prior to blocking. 
     * Returns true if woken up in time.
     */
    bool timed_wait(int millisecs)
    {
        GTimeVal gtime;
        g_get_current_time(&gtime);
        g_time_val_add(&gtime, millisecs*1000); // convert to microsecs
        return (g_cond_timed_wait(cond, mutex, &gtime) > 0);
    }

    void lock()
    {
        g_mutex_lock(mutex);
    }

    void unlock()
    {
        g_mutex_unlock(mutex);
    }
};

#endif
