// Base64.cpp: implementation of the CBase64 class.
// Author: Wes Clyburn (clyburnw@enmu.edu)
// 
// This code was practically stolen from:
// Dr. Dobb's Journal, September 1995, 
// "Mime and Internet Mail", by Tim Kientzle
//////////////////////////////////////////////////////////////////////

#include "Base64.h"
#include <string.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#endif


// Static Member Initializers

// The 7-bit alphabet used to encode binary information
char CBase64::m_sBase64Alphabet[] = 
"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

int CBase64::m_nMask[] = { 0, 1, 3, 7, 15, 31, 63, 127, 255 };

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CBase64::CBase64()
{
    m_nBitsRemaining = 0;
    m_lBitStorage = 0;
}


CBase64::~CBase64()
{
}

char* CBase64::Encode(LPCTSTR szEncoding, int nSize)
{
    m_nBitsRemaining = 0;
    m_lBitStorage = 0;

    char* sOutput = NULL;
    char* sOutputPtr = NULL;
    int nNumBits = 6;
    UINT nDigit;
    int lp = 0;

    if( szEncoding == NULL )
	return sOutput;

    sOutput = new char[2*nSize];
    memset(sOutput, 0, 2*nSize);
    sOutputPtr = sOutput;

    m_szInput = szEncoding;
    m_nInputSize = nSize;

    m_nBitsRemaining = 0;
    nDigit = read_bits( nNumBits, &nNumBits, lp );
    int totalRead = 0;
    while( nNumBits > 0 )
    {
	    totalRead++;
	    *sOutput++ = m_sBase64Alphabet[ (int)nDigit ];
	    nDigit = read_bits( nNumBits, &nNumBits, lp );
    }
    // Pad with '=' as per RFC 1521
    while( totalRead % 4 != 0 )
    {
	    totalRead++;
	    *sOutput++ = '=';
    }
    return sOutputPtr;
}

// The size of the output buffer must not be less than
// 3/4 the size of the input buffer. For simplicity,
// make them the same size.
int CBase64::Decode(LPCTSTR szDecoding, LPTSTR szOutput)
{
    m_nBitsRemaining = 0;
    m_lBitStorage = 0;

    char* sInput = NULL;
    int c, lp =0;
    int i;
    int nDigit;
    int nDecode[ 256 ];

    if( szOutput == NULL )
    	return 0;
    if( szDecoding == NULL )
	    return 0;
    int size = strlen(szDecoding);
    if( size == 0 )
    	return 0;

    // Build Decode Table
    //
    for(i = 0; i < 256; i++ ) 
    {
	    nDecode[i] = -2; // Illegal digit
    }
    for( i=0; i < 64; i++ )
    {
	    nDecode[ m_sBase64Alphabet[ i ] ] = i;
	    nDecode[ m_sBase64Alphabet[ i ] | 0x80 ] = i; // Ignore 8th bit
	    nDecode[ '=' ] = -1; 
	    nDecode[ '=' | 0x80 ] = -1; // Ignore MIME padding char
    }

    // Clear the output buffer
    memset( szOutput, 0, size + 1 );

    // Decode the Input
    for( lp = 0, i = 0; lp < size; lp++ )
    {
	    c = szDecoding[ lp ];
	    nDigit = nDecode[ c & 0x7F ];
	    if( nDigit < -1 ) 
	    {
	        return 0;
	    } 
	    else if( nDigit >= 0 ) 
	    {
	        // i (index into output) is incremented by write_bits()
	        write_bits( nDigit & 0x3F, 6, szOutput, i );
	    }
    }	
    return i;
}

UINT CBase64::read_bits(int nNumBits, int * pBitsRead, int& lp)
{
    ULONG lScratch;
    while( ( m_nBitsRemaining < nNumBits ) && 
	       ( lp < m_nInputSize ) ) 
    {
    	int c = m_szInput[ lp++ ];
        m_lBitStorage <<= 8;
        m_lBitStorage |= (c & 0xff);
    	m_nBitsRemaining += 8;
    }
    if( m_nBitsRemaining < nNumBits ) 
    {
	    lScratch = m_lBitStorage << ( nNumBits - m_nBitsRemaining );
	    *pBitsRead = m_nBitsRemaining;
	    m_nBitsRemaining = 0;
    } 
    else 
    {
	    lScratch = m_lBitStorage >> ( m_nBitsRemaining - nNumBits );
	    *pBitsRead = nNumBits;
	    m_nBitsRemaining -= nNumBits;
    }
    return (UINT)lScratch & m_nMask[nNumBits];
}


void CBase64::write_bits(UINT nBits,
			 int nNumBits,
			 LPTSTR szOutput,
			 int& i)
{
    UINT nScratch;

    m_lBitStorage = (m_lBitStorage << nNumBits) | nBits;
    m_nBitsRemaining += nNumBits;
    while( m_nBitsRemaining > 7 ) 
    {
	    nScratch = m_lBitStorage >> (m_nBitsRemaining - 8);
	    szOutput[ i++ ] = nScratch & 0xFF;
	    m_nBitsRemaining -= 8;
    }
}

