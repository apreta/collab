/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef IREFOBJECT_H
#define IREFOBJECT_H

#include <assert.h>
#include "iobject.h"


// A referencable (and lockable) object.  Object is automatically deleted when 
// reference count drops to 0.
class IReferencableObject : public IObject
{
 protected:
    int ref_count;

 public:
    IReferencableObject()
    {
        ref_count = 1;
    }

    virtual void lock() = 0;
    virtual void unlock() = 0;
    virtual bool referencable() { return true; }
    virtual void reference()
    {
        lock();
        ref_count++;
		assert(ref_count < 10000 && ref_count > 0);
        unlock();
    }
    virtual void dereference()
    {
        lock();
        int temp = --ref_count;
		assert(ref_count < 10000 && ref_count >= 0);
        unlock();

        if (temp == 0)
            delete this;
    }
};

// Smart pointer to referencable object.
// (Hard to implement smart pointer usefully without templates.)
template<class T>
class IRefObjectPtr
{
	T * obj;
	
public:
	IRefObjectPtr(T * _obj = NULL)
	{
		obj = _obj;
		if (obj)
			obj->reference();
	}

	IRefObjectPtr(const IRefObjectPtr & copy)
	{
		obj = copy.obj;
		if (obj)
			obj->reference();
	}

	~IRefObjectPtr()
	{
		if (obj)
			obj->dereference();
	}

	T* operator->() const
	{
		return obj;
	}
	
	T& operator*() const
	{
		return *obj;
	}
	
	// This could be dangerous as pointer looses its "smartness".
	operator T*() const
	{
		return obj;
	}
	
	IRefObjectPtr& operator=(IRefObjectPtr &ptr)
	{
		if (obj)
			obj->dereference();
		obj = ptr.obj;
		if (obj)
			obj->reference();
		return *this;
	}

	IRefObjectPtr& operator=(T *_obj)
	{
		if (obj)
			obj->dereference();
		obj = _obj;
		if (obj)
			obj->reference();
		return *this;
	}


	bool operator==(IRefObjectPtr &ptr) const
	{
		return (ptr.obj == this->obj);
	}

	bool operator==(T *obj) const
	{
		return (obj == this->obj);
	}
	
	bool operator!=(IRefObjectPtr &ptr) const
	{
		return (ptr.obj != this->obj);
	}

	bool operator!=(T *obj) const
	{
		return (obj != this->obj);
	}
	
};

// Reference object while iterator exists (presumably to lock down source object
// while iterating through a contained list).
class IRefListIterator : IListIterator
{
    friend class IList;
    IReferencableObject * obj;

public:
    IRefListIterator()
    {
        obj = NULL;
    }
    IRefListIterator(IReferencableObject & _obj, IList & _list) : IListIterator(_list)
    {
        set_list(_obj, _list);
    }
    ~IRefListIterator()
    {
        if (obj != NULL)
            obj->dereference();
    }

    void set_list(IReferencableObject & _obj, IList & _list)
    {
        if (obj != NULL)
            obj->dereference();
        obj = &_obj;
        obj->reference();
        IListIterator::set_list(_list);
    }
};

// Reference object while iterator exists (presumably to lock down source object
// while iterating through a contained list).
class IRefVectorIterator : public IVectorIterator
{
    IReferencableObject * obj;

public:
    IRefVectorIterator()
    {
    }

    IRefVectorIterator(IReferencableObject & _obj, IVector & _vector)
        : IVectorIterator(_vector)
    {
        set_vector(_obj, _vector);
    }

    ~IRefVectorIterator()
    {
        if (obj != NULL)
            obj->dereference();
    }

    void set_vector(IReferencableObject & _obj, IVector & _vector)
    {
        if (obj != NULL)
            obj->dereference();
        obj = &_obj;
        obj->reference();
        IVectorIterator::set_vector(_vector);
    }

    IObject * get_first();
    IObject * get_next();
    IObject * get(int idx);
};

#endif
