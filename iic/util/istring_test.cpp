#include <stdio.h>
#include "istring.h"

int main()
{
	IString s1("test1");
	IString s2("test2");

	printf("%d %d\n", s1==s2, s1!=s2);
	printf("%s\n", (const char *)(s1 + ":" + s2));
	
	IStringHash hash;
	hash.insert("1", new IString(s1));
	hash.insert("2", new IString(s2));

	IString *res = (IString*)hash.get("1");
	printf("%s\n", (const char *)*res);
	printf("%s\n", hash.getString("2"));

	// By default, vector does not take ownership of pointers, so
	// values are not deleted.
	IVector vector;
	
	vector.add(&s2);
	vector.add(&s1);
	
	for (int i=0; i<vector.size(); i++)
		printf("%d = %s\n", i, (const char*)*(IString*)vector.get(i));
	
	vector.clear();
}
