/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 *
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 *
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 *
 *
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 *
 *
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#ifndef ISTRING_HEADER
#define ISTRING_HEADER

#include <string.h>
#include "iobject.h"

#define TYPE_ISTRING 529

class IString : public IObject
{
	GString *val;

public:
	IString()
	{
		val = g_string_new("");
	}

	IString(const char *str)
	{
		if (str == NULL)
			val = g_string_new("");
		else
			val = g_string_new(str);
	}

	IString(const char *str, int len)
	{
		if (str == NULL)
			val = g_string_new("");
		else
			val = g_string_new_len(str, len);
	}

	IString(const IString &copy)
	{
		val = g_string_new(copy);
	}

	~IString()
	{
		g_string_free(val, TRUE);
	}

    const char *get_string() const
	{
		return val->str;
	}

	const char *get_escaped_string() const
	{
		// Return string probably needs to be freed.
		return g_markup_escape_text(val->str, val->len);
	}

	const char *get_string_escaped_string(const char *exceptions="") const
    {
		// gfree() result
		return g_strescape(val->str, exceptions);
	}

	int length() const
	{
		return val->len;
	}

	IString& append(const char *str)
	{
		g_string_append(val, str);
		return *this;
	}

	IString& append(const char c)
	{
		g_string_append_c(val, c);
		return *this;
	}

	IString& prepend(const char *str)
	{
		g_string_prepend(val, str);
		return *this;
	}

	IString& prepend(const char c)
	{
		g_string_prepend_c(val, c);
		return *this;
	}

	IString& strdelimit(const char *delimiters, gchar replacement)
	{
	  g_strdelimit(val->str, delimiters, replacement);
	  return *this;
	}

	void clear()
	{
		g_string_set_size(val, 0);
	}

	int contains(const char *str) const
	{
	  return (strpbrk(val->str, str) != NULL);
	}

	int find(const char *str, int start=0) const
	{
		if (start >= (int)val->len)
			return -1;

		char *p = strstr(val->str + start, str);
		if (p)
			return p - val->str;
		return -1;
	}

	int rfind(const char *str) const
	{
		char *p = g_strrstr(val->str, str);
		if (p)
			return p - val->str;
		return -1;
	}

	bool has_prefix(const char *prefix)
	{
        return g_str_has_prefix(val->str, prefix) == TRUE;
	}

	bool has_suffix(const char *suffix)
	{
        return g_str_has_suffix(val->str, suffix) == TRUE;
	}

	void erase(int start, int len)
	{
		g_string_erase(val, start, len);
	}

    void truncate(int len)
    {
        g_string_truncate(val, len);
    }

	void insert(int pos, const char *str)
	{
		g_string_insert(val, pos, str);
	}

	void insert(int pos, const char *str, int len)
	{
		g_string_insert_len(val, pos, str, len);
	}

	void to_lower()
	{
		gchar *lower = g_utf8_strdown(val->str, -1);
		g_string_assign(val, lower);
		g_free(lower);
	}

	void to_upper()
	{
		gchar *up = g_utf8_strup(val->str, -1);
		g_string_assign(val, up);
		g_free(up);
	}

    void uri_escape(bool allow_utf8=true)
    {
        gchar *esc = g_uri_escape_string(val->str, NULL, allow_utf8);
		g_string_assign(val, esc);
		g_free(esc);
    }

    void uri_unescape()
    {
        gchar *uesc = g_uri_unescape_string(val->str, NULL);
		g_string_assign(val, uesc);
		g_free(uesc);
    }

	// Whole bunch more methods to add from glib
	// insert, erase, append, printf, etc.

	IString& operator=(IString &str)
	{
		g_string_assign(val, str);
		return *this;
	}

	IString& operator=(const char *str)
	{
		if (str == NULL)
			clear();
		else
        {
            // if same str ptr, make copy
            // glib will destroy otherwise on g_string_assign
            // when it copies onto itself
            if (str == val->str)
            {
                GString *original_val = val;
                val = g_string_new(str);
                g_string_free(original_val, TRUE);
            }
            else
    			g_string_assign(val, str);
        }
		return *this;
	}

	operator const char *() const
	{
		return val->str;
	}

	bool operator==(const char *str) const
	{
		return strcmp(val->str, str) == 0;
	}

	bool operator==(IString &str) const
	{
		return g_string_equal(val, str.val) != 0;
	}

	bool operator!=(const char *str) const
	{
		return strcmp(val->str, str) != 0;
	}

	bool operator!=(IString &str) const
	{
		return g_string_equal(val, str.val) == 0;
	}

	int type()
	{
		return TYPE_ISTRING;
	}

    unsigned int hash()
	{
		return g_string_hash(this->val);
	}

    bool equals(IObject *str)
	{
		if (str->type() != TYPE_ISTRING)
			return false;
		return g_string_equal(this->val, ((IString*)str)->val) != 0;
	}
};

inline IString operator+(const IString &str1, const IString &str2)
{
	IString temp(str1);
	temp.append(str2);
	return temp;
}

inline IString& operator+=(IString &str1, const IString &str2)
{
	str1.append(str2);
	return str1;
}

// Convenience wrapper for string-indexed hash
class IStringHash : public IHash
{
  friend class IString;
  GHashTable *table;

public:
  IStringHash()
  {
  }

  ~IStringHash()
  {
  }

  void insert(const char *key, IObject *value)
  {
	IHash::insert(new IString(key), value);
  }

  void insert(IString *key, IObject *value)
  {
    IHash::insert(key, value);
  }

  IObject *get(const char* key)
  {
	IString test(key);
    return (IObject*) IHash::get(&test);
  }

  IObject *get(IString *key)
  {
    return (IObject*) IHash::get(key);
  }

  void remove(const char* key, bool del_element = true)
  {
  	  IString keystr(key);
      IHash::remove(&keystr, del_element);
  }

  const char *getString(const char *key)
  {
	IString test(key);
	IString *val = (IString*)IHash::get(&test);
	if (val == NULL || val->type() != TYPE_ISTRING)
		return NULL;
    return  (const char*)*val;
  }
};

#endif

