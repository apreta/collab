/**
 * The contents of this file are subject to the Common Public Attribution License Version 1.0 (the "CPAL");
 * you may not use this file except in compliance with the CPAL. You may obtain a copy of the CPAL at
 * http://www.opensource.org/licenses/cpal_1.0. The CPAL is based on the Mozilla Public License Version 1.1
 * but Sections 14 and 15 have been added to cover use of software over a computer network and provide for
 * limited attribution for the Original Developer. In addition, Exhibit A has been modified to be
 * consistent with Exhibit B.
 * 
 * Software distributed under the CPAL is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY KIND,
 * either express or implied. See the CPAL for the specific language governing rights and limitations
 * under the CPAL.
 * 
 * The Original Code is ICEcore. The Original Developer is SiteScape, Inc. All portions of the code
 * written by SiteScape, Inc. are Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * 
 * 
 * Attribution Information
 * Attribution Copyright Notice: Copyright (c) 1998-2007 SiteScape, Inc. All Rights Reserved.
 * Attribution Phrase (not exceeding 10 words): [Powered by ICEcore]
 * Attribution URL: [www.icecore.com]
 * Graphic Image as provided in the Covered Code [powered_by_icecore.png].
 * Display of Attribution Information is required in Larger Works which are defined in the CPAL as a
 * work which combines Covered Code or portions thereof with code not governed by the terms of the CPAL.
 * 
 * 
 * SITESCAPE and the SiteScape logo are registered trademarks and ICEcore and the ICEcore logos
 * are trademarks of SiteScape, Inc.
 */

#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <string.h>

#define BUFFSZ	4192

static int s_fd = -1;
static int s_this_ino;
static char *s_fname;

void check_rename(void);

void output_to_eof()
{
    char buf[BUFFSZ];
    int n_rd;
    do {
        n_rd = read(s_fd, buf, BUFFSZ);
        if (n_rd > 0)
        {
            write(0, buf, n_rd);
        }
    } while (n_rd > 0);
}

void reopen()
{
    if (s_fd != -1)
    {
        close(s_fd);
        s_fd = -1;
    }

    while ((s_fd = open(s_fname, O_RDONLY)) < 0)
        sleep(1);

    struct stat sbuf;
    if (fstat(s_fd, &sbuf) < 0)
    {
        fprintf(stderr, "Couldn't stat %s: %s",
                s_fname, strerror(errno));
        return;
    }

    s_this_ino = sbuf.st_ino;

    output_to_eof();
}

void check_rename()
{
    struct stat sbuf;
    if (stat(s_fname, &sbuf) < 0)
    {
        if (errno != ENOENT)
        {
            fprintf(stderr, "Couldn't stat file: %s\n",
                    strerror(errno));
        }
        return;
    }
    if (sbuf.st_ino != s_this_ino)
    {
        reopen();
    }
}

void sigio_handler(int arg)
{
    output_to_eof();	// make sure prior file is fully outputted
    check_rename();		// opens new file if necessary and outputs all of it
}

void install_sigio_handler()
{
    struct sigaction act;
    act.sa_handler = sigio_handler;
    sigemptyset(&act.sa_mask);
    sigaddset(&act.sa_mask, SIGIO);
    act.sa_flags = 0;
    sigaction(SIGIO, &act, NULL);
}

void block_sigio()
{
    sigset_t sigset;
    sigemptyset(&sigset);
    sigaddset(&sigset, SIGIO);
    sigprocmask(SIG_BLOCK, &sigset, NULL);
}

void unblock_sigio()
{
    sigset_t sigset;
    sigemptyset(&sigset);
    sigaddset(&sigset, SIGIO);
    sigprocmask(SIG_UNBLOCK, &sigset, NULL);
}


main(int argc, char * argv[])
{
    
    if (argc != 2)
    {
        fprintf(stderr, "usage: %s <.elgfn>\n", argv[0]);
        return -1;
    }

    s_fname = argv[1];

    char dir[MAXPATHLEN];
    strcpy(dir, s_fname);
    char * sl = strrchr(dir, '/');
    if (sl)
    {
        *sl = '\0';
    }

    DIR *d = opendir(dir);
    if (d == NULL)
    {
        fprintf(stderr, "Couldn't open dir %s\n", dir);
        return -1;
    }
    
    install_sigio_handler();
    block_sigio();
    
    int dfd = dirfd(d);
    if ( fcntl( dfd, F_SETOWN, getpid() ) < 0)
    {
        fprintf(stderr, "Couldn't set F_SETOWN: %s\n",
                strerror(errno));
    }

    if ( fcntl( dfd, F_NOTIFY, DN_MODIFY|DN_RENAME|DN_MULTISHOT ) < 0)
    {
        fprintf(stderr, "Couldn't set F_NOTIFY: %s\n",
                strerror(errno));
    }

    reopen(); // do initial open and output whatever is there
    unblock_sigio();

    for (;;)
    {
        struct timeval tmo = { 10L, 0L };
        
        select(0, NULL, NULL, NULL, &tmo);

        // In case we lose a signal, check once in a
        // while

        block_sigio();
        sigio_handler(0);
        unblock_sigio();
    }
}
