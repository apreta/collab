/*
 *  Copyright (C) 1999 AT&T Laboratories Cambridge.  All Rights Reserved.
 *
 *  This is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this software; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 *  USA.
 */

/*
 * vncviewer.c - the Xt-based VNC viewer.
 */

#include "vncviewer.h"
#ifdef IIC
#include <netlib/IICvncMsg.h>
#include <pthread.h>
#endif

char *programName;
XtAppContext appContext;
Display* dpy;

Widget toplevel;

#ifdef IIC

// Mutex used when processing remote control messages
static pthread_mutex_t msg_mutex = PTHREAD_MUTEX_INITIALIZER;

// globals common to MessageThread and watch_loop
volatile Bool	got_message = FALSE;
volatile Bool	msg_thread_quit = FALSE;
IIC_id	id;
int	wmCmd, wParam;
void*	lParam;

void MsgSemaGet()
{
    pthread_mutex_lock(&msg_mutex);
}

void MsgSemaRelease()
{
    pthread_mutex_unlock(&msg_mutex);
}

void *MessageThread(void *p)
{
  printf("MessageThread - MsgQueueOpen\n");
    
  MsgQueueOpen(IIC_vncViewer);

  if( strcmp( appData.useEncryptKey, "yes" )  ==  0 )
  {
    // IIC - tell meeting to send the encryptkey NOW!!
    printf("MessageThread - SendMessage(IIC_Meeting, CB_GetEncryptKey)\n");
    uint msgT = RegisterWindowMessage("IICViewer.GetEncryptKey");
    SendMessage(IIC_Meeting, msgT, CB_GetEncryptKey, 0L);
  }

  while(!msg_thread_quit)
  {
    GetMessage(&id, &wmCmd, &wParam, &lParam);
    printf("MessageThread GotMessage  wmCmd:%d wParam:%d lParam:%08X\n",wmCmd,wParam,lParam);
    fflush(stdout);

    //  Special case handling for GetEncryptKey
    if( wmCmd  ==  WM_COPYDATA )
    {
      printf("MessageThread - WM_COPYDATA\n");
      COPYDATASTRUCT  *data   = (COPYDATASTRUCT*) lParam;
      if( (int)data->dwData  ==  GetEncryptKey )
      {
	appData.encryptKey = strdup(data->lpData);
	printf("MessageThread - Got Encrypt Key \"%s\"\n", appData.encryptKey);
	DelMessage( wmCmd, wParam, lParam );    // done with it
      }
      continue;
    }

    // Have to handle some messages here, like EnableRemoteControl, else viewer cursor won't change until something
    // comes thru from rfb server.  Don't set got_message if we handle it here
    if(wmCmd == msgEnableRemoteControl)
    {
      printf("MessageThread - msgEnableRemoteControl\n");
      MsgSemaGet();
      // set ViewOnly and Controller, set cursor
      if(appData.viewOnly != (wParam == 0))
      {
	appData.viewOnly = (wParam == 0);             // viewOnly is opposite of enable control
	if(appData.viewOnly)
	  appData.controller = FALSE;
	ChangeCursor();
	printf("  viewOnly=%d,controller=%d\n",appData.viewOnly,appData.controller);  // TEMP LS
	//fflush(stdout);
      }
      MsgSemaRelease();
      usleep(10000);          // sleep 10ms so other thread runs
    }
    else
      got_message = TRUE;

    while(got_message)
      usleep(20*1000);	// 20ms I hope
  } // end while(!meg_thread_quit)

  printf("MessageThread - Ended\n");

  return 0;
}

void StartMessageThread()
{
  pthread_t thread;
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  int ret = pthread_create(&thread, &attr, MessageThread, NULL);
}
#endif


int
main(int argc, char **argv)
{
  BOOL shut_down = FALSE;
  int i;
  programName = argv[0];
  Window my_window;

#ifdef USE_NETLIB
  printf("Starting: vncviewer ");
  for( i = 0; i < argc; ++i )
  {
    if( argv[i] )
      printf("%s ",argv[i]);
  }
  printf("\n");

  {
    char buff[1024];
    sprintf(buff, "%s/.meeting", getenv("HOME"));
    net_initialize(buff);

    // NOTE: For Curl use, the netInitializeLivrary() call s/b made here as well ??? (PLF)
  }
#endif

  /* The -listen option is used to make us a daemon process which listens for
     incoming connections from servers, rather than actively connecting to a
     given server. The -tunnel and -via options are useful to create
     connections tunneled via SSH port forwarding. We must test for the
     -listen option before invoking any Xt functions - this is because we use
     forking, and Xt doesn't seem to cope with forking very well. For -listen
     option, when a successful incoming connection has been accepted,
     listenForIncomingConnections() returns, setting the listenSpecified
     flag. */
  for (i = 1; i < argc; i++) 
  {
    if (strcmp(argv[i], "-listen") == 0) {
      listenForIncomingConnections(&argc, argv, i);
      break;
    }
    if (strcmp(argv[i], "-tunnel") == 0 || strcmp(argv[i], "-via") == 0) {
      if (!createTunnel(&argc, argv, i))
	exit(1);
      break;
    }
  }

  /* Call the main Xt initialisation function.  It parses command-line options,
     generating appropriate resource specs, and makes a connection to the X
     display. */

  toplevel = XtVaAppInitialize(&appContext, "Vncviewer",
			       cmdLineOptions, numCmdLineOptions,
			       &argc, argv, fallback_resources,
			       XtNborderWidth, 0, NULL);

  dpy = XtDisplay(toplevel);

  /* Interpret resource specs and process any remaining command-line arguments
     (i.e. the VNC server name).  If the server name isn't specified on the
     command line, getArgsAndResources() will pop up a dialog box and wait
     for one to be entered. */

  GetArgsAndResources(argc, argv);

#ifdef IIC
  printf("vncviewer - Calling StartMessageThread()\n");
  StartMessageThread();       // listen for messages from meeting
#endif

  /* Unless we accepted an incoming connection, make a TCP connection to the
     given VNC server */

  if (!listenSpecified) 
  {
    if (!ConnectToRFBServer(vncServerHost, vncServerPort)) 
    {
      printf("vncviewer - ConnectToRFBServer() falied. exit(1)\n");
      exit(1);
    }
  }

  /* Initialise the VNC connection, including reading the password */

  if (!InitialiseRFBConnection())
  {
    printf("vncviewer - InitializeRFBConnection() failed. exit(1)\n");
    exit(1);
  }

  /* Create the "popup" widget - this won't actually appear on the screen until
     some user-defined event causes the "ShowPopup" action to be invoked */

  CreatePopup();

  /* Find the best pixel format and X visual/colormap to use */

  SetVisualAndCmap();

  /* Create the "desktop" widget, and perform initialisation which needs doing
     before the widgets are realized */

  ToplevelInitBeforeRealization();

  DesktopInitBeforeRealization();

  /* "Realize" all the widgets, i.e. actually create and map their X windows */

  XtRealizeWidget(toplevel);

  /* Perform initialisation that needs doing after realization, now that the X
     windows exist */

  InitialiseSelection();

  ToplevelInitAfterRealization();

  DesktopInitAfterRealization();

  /* ICE begin */

  printf("ICECore/Conferencing - Initialization start\n");

  if (strcmp( appData.useEncryptKey, "yes" )  ==  0)
  {
    int nKeyWait_ms = 50;
    int nKeyWait_retrys = 100;
    float fTimeout = (float)(nKeyWait_ms * nKeyWait_retrys);
    printf("vncviewer - Waiting for %f seconds for the encrypt key.\n", fTimeout );

    int i=nKeyWait_retrys;
    while (!*appData.encryptKey && --i)
      usleep(50*1000);
    
    if( !*appData.encryptKey )
    {
      printf("vncviewer - WARNING: The encrypt key was never received!\n");
    }
  }

#ifdef BLOWFISH
  printf("vncviewer - Initializing blowfish key = \"%s\"\n",appData.encryptKey);
  if( !*appData.encryptKey )
  {
    printf("WARNING! The blowfish key is nul.\n");
  }
  InitBlowfish(appData.encryptKey);
#endif

  /* IIC if -embed then reparent as child of root
            if -embedW <window> then reparent as child of <window>
            if embedW 1 then don't reparent,
  */
  //appData.embed = 0; //%%%PLF - Turn this off for now.

  if (appData.embed) 
  {
    XtVaSetValues(viewport, XtNforceBars, False, NULL);

    if(appData.embedW != 0)
    {
      printf("embedW %d\n",appData.embedW);
      if(appData.embedW != 1)
      {
	//XtResizeWidget(toplevel, 100, 100, 0);
	XReparentWindow(dpy, XtWindow(toplevel), appData.embedW, 0, 0); // reparent into callers window
      }
    }
    else
    {
      // Reparent to toplevel
      XReparentWindow(dpy, XtWindow(toplevel), DefaultRootWindow(dpy), 0, 0);
    }
    XStoreName(dpy, XtWindow(toplevel), "embedViewer");
    my_window = XtWindow(toplevel);
    printf("my_window=%d, embedW %d\n",my_window,appData.embedW);

    // IIC    We are connected, tell meeting and include window id
    printf("\nfrom=%d, to=%d, msgViewerCreated=%d\n",IIC_vncServer,IIC_Meeting,msgViewerCreated);
    PostMessage(IIC_Meeting, msgViewerCreated, my_window, 0L);
    fflush(stdout);
    fflush(stderr);

    /* Just for testing, so we don't cover the entire desktop */
// for testing
//    XReparentWindow(dpy, XtWindow(toplevel), DefaultRootWindow(dpy), 0, 0);
//    XtResizeWidget(toplevel, 100, 100, 0);
  }

  printf("ICE/Conferencing - Initialization done.\n");

  /* ICE end */

  /* Tell the VNC server which pixel format and encodings we want to use */

  SetFormatAndEncodings();

  /* Now enter the main loop, processing VNC messages.  X events will
     automatically be processed whenever the VNC connection is idle. */
  while (!shut_down) {
    if (!HandleRFBServerMessage())
      break;
#ifdef IIC	// messages from invoking program are most important, handle first
		if(got_message)
		{
            printf("Handling got_message\n");
			switch(wmCmd)
			{
			case WM_COPYDATA:
				{
					COPYDATASTRUCT * data = (COPYDATASTRUCT*)lParam;
					switch((int)data->dwData)
					{
					case SetPassword:
						printf("Set pw\n");
						break;
					case SetPorts:
						break;
					case SetCallbackInfo:
						break;
					case ShareAddlApps:
						break;
					case EnableDisableTransparent:
						break;
					case SetPwrPointWndHandle:	// ??
						break;
					case SetColorDeduction:
						break;
					case EnableDisableRecording:
						break;
					case ShareAllInstances:
						break;
					case AddClientByName:
						break;
					case SetAgentMode:
						break;
					case SetNagleAlgorithm:
						break;
					default:			// do what ?
						printf("unexpected WM_COPYDATA cmd %d\n",data->dwData);
						break;
					}
				}
				break;
            case WM_CLOSE:
                printf("WM_CLOSE msg\n");
                shut_down = 1;
                break;
            default:    // got error using msgEnableRemoteControl as case label: "does not reduce to integer constant", WTF?
                {
                    if(wmCmd == msgEnableRemoteControl)
                    {
                        // set ViewOnly and Controller, set cursor
                        // UGH, see what happens without FORCE
                        if(appData.viewOnly != (wParam == 0))
                        {
                            appData.viewOnly = (wParam == 0);             // viewOnly is opposite of enable control
                            if(appData.viewOnly)
                                appData.controller = FALSE;
                            ChangeCursor();
                            printf("  viewOnly=%d,controller=%d\n",appData.viewOnly,appData.controller);  // TEMP LS
                            fflush(stdout);
                            // control will be requested when I have something to send.
                        }
                        break;
                    }
                    else if( (wmCmd == msgEnableFullScreen) ||
                    (wmCmd == msgEnableDebugMode) )
                    {
                        printf("TODO: wmCmd %u\n",wmCmd);
                        break;
                    }
            //case msgToggleTestingMode:
            //case msgVNCRefresh:
                    else
                    {
                        printf("WM_CLOSE=%u,msgEnableRemoteControl=%u, UNEXPECTED msg %u, %u, %u\n",WM_CLOSE,msgEnableRemoteControl,wmCmd, wParam, lParam);
                        fflush(stdout);
                    }
                    break;
                }
			}   // end switch(wmCmd)

			DelMessage(wmCmd, wParam, lParam);	// done with it
			got_message = FALSE;
		}
#endif
  }

  Cleanup();

  return 0;
}
