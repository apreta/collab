
extern "C" {

#include "vncviewer.h"

}

#include "blowfish.h"

CBlowFish blowfish;

extern "C" {

void InitBlowfish(const char * key)
{
	blowfish.SetKey(key);
	blowfish.Initialize(BLOWFISH_KEY, sizeof(BLOWFISH_KEY));
}

Bool ReadEncrypted(char *lpBuffer, unsigned lSize)
{
	static BYTE buffer[4096];
	Bool fNeedFree = FALSE;
	BYTE* pEncrypted = buffer;

	if (lSize > 4096) {
		pEncrypted = (BYTE*)malloc(lSize);
		fNeedFree = TRUE;
	}
	if (!ReadFromRFBServer((char*)pEncrypted, lSize)) return False;

    blowfish.Decode((BYTE*)pEncrypted, (BYTE*)lpBuffer, lSize);

	if (fNeedFree) free(pEncrypted);

	return True;
}

}
