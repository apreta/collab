.PHONY: all
all:
	@make
	@echo Executing Post Build commands ...
	@cp ggivnc ../iic-client/bin/mtgviewer
	@echo Done
