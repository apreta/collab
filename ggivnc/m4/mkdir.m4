AC_DEFUN([PR_FUNC_MKDIR],
[
AC_CHECK_FUNCS([mkdir])
if test "x$ac_cv_func_mkdir" != xyes; then
	AC_CHECK_FUNCS([_mkdir])
fi

AC_CACHE_CHECK(
[if mkdir takes two arguments],
[pr_cv_two_arg_mkdir],
[
AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[
#ifdef HAVE_SYS_STAT_H
#include <sys/stat.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
]],[[
mkdir("", 0);
]])],
[pr_cv_two_arg_mkdir=yes],
[pr_cv_two_arg_mkdir=no])])

if test "x$pr_cv_two_arg_mkdir" != xno; then
	AC_DEFINE([HAVE_TWO_ARG_MKDIR], [1],
		[Define to 1 if you have a two argument mkdir.])
fi
])
