dnl find turbojpeg.dll

AC_DEFUN([PR_FIND_TURBOJPEG_DLL],
[
AC_CACHE_CHECK([for turbojpeg.dll], [pr_cv_path_turbojpeg_dll], [
pr_cv_path_turbojpeg_dll=no
save_IFS=$IFS
IFS=$PATH_SEPARATOR
for dir in $PATH
do
	IFS=$save_IFS
	if test -f "$dir/turbojpeg.dll"; then
		pr_cv_path_turbojpeg_dll=$dir/turbojpeg.dll
		break
	fi
done
IFS=$save_IFS
])])
