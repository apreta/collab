dnl Check how to handle getaddrinfo

AC_DEFUN([PR_FUNC_GETADDRINFO],
[

AC_SEARCH_LIBS(getaddrinfo, [socket])

if test "$ac_cv_search_getaddrinfo" != no; then
	AC_DEFINE([HAVE_GETADDRINFO], 1,
			[Define to 1 if you have the `getaddrinfo' function.])
	AC_REPLACE_FUNCS(gai_strerror)
else
	AC_CHECK_FUNC(inet_aton,
		[AC_DEFINE([HAVE_INET_ATON], 1,
			[Define to 1 if you have the `inet_aton' function.])])
	AC_LIBOBJ(getaddrinfo)
	AC_LIBOBJ(gai_strerror)
fi

])
