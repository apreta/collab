#include "util.h"

#ifdef USE_SYSLOG
log_t log_new(char *ident)
{
    openlog(ident, LOG_PID, LOG_LOCAL7);

    return NULL;
}

void log_write(log_t l, int level, const char *msgfmt, ...)
{
    va_list ap;

    va_start(ap, msgfmt);
    vsyslog(level, msgfmt, ap);
    va_end(ap);
}

void log_free(log_t l)
{
    closelog();
}

#else

#define MAX_DEBUG 1024

static FILE *log_file = NULL;
static char log_file_name[MAX_LOG_FILENAME];
int trigger_sz = 1000000;
int history_len = 8;
int log_level = LOG_DEBUG;

void log_set_level(int level)
{
	log_level = level;
}

static void log_rotate_if_needed()
{
  char newerfn[MAX_LOG_FILENAME];
  char olderfn[MAX_LOG_FILENAME];
  int i;
  long sz;

  fseek(log_file, 0, SEEK_END);
  sz = ftell(log_file);

  if (sz < trigger_sz) return;

  fclose(log_file);
  log_file = NULL;

  sprintf(newerfn, "%s.%u", log_file_name, history_len);
  unlink(newerfn);
  for (i = history_len-1; i > 0; i--) {
      strcpy(olderfn, newerfn);
      sprintf(newerfn, "%s.%u", log_file_name, i);
      if (rename(newerfn, olderfn) < 0) {
          if (errno != ENOENT) {
              log_file = fopen(log_file_name, "a+");
              log_write(log_file, 4, "Couldn't rotate logs: %s", strerror(errno));
          }
      }
  }
  strcpy(olderfn, newerfn);
  strcpy(newerfn, log_file_name);
  if (rename(newerfn, olderfn) < 0) {
    if (errno != ENOENT) {
      log_file = fopen(log_file_name, "a+");
      log_write(log_file, 4, "Couldn't rotate logs: %s", strerror(errno));
    }
  }

  if (log_file == NULL)
    log_file = fopen(log_file_name, "a+");
  if(log_file == NULL) {
    log_write(log_file, 4,
	      "couldn't open %s for append: %s\n"
	      "logging will go to stderr instead\n", log_file_name, strerror(errno));
  }
}

log_t log_new(char *ident)
{
    snprintf(log_file_name, MAX_LOG_FILENAME, "/var/log/iic/%s.log", ident);

    log_file = fopen(log_file_name, "a+");
    if(log_file == NULL) {
        log_write(log_file, 4,
            "couldn't open %s for append: %s\n"
            "logging will go to stderr instead\n", log_file_name, strerror(errno));
    }

    return log_file;
}

static const char *log_level_names[] =
{
    "emergency",
    "alert",
    "critical",
    "error",
    "warning",
    "notice",
    "info",
    "debug"
};

void log_write(log_t l, int level, const char *msgfmt, ...)
{
    va_list ap;
    char *pos, message[MAX_LOG_LINE];
    int sz;
    time_t t;

	if (log_file) log_rotate_if_needed();

    FILE *f = (FILE *) log_file ? log_file : stderr;
    
    /* timestamp */
    t = time(NULL);
    pos = ctime(&t);
    sz = strlen(pos);
    /* chop off the \n */
    pos[sz-1]=' ';

    /* insert the header */
    snprintf(message, MAX_LOG_LINE, "%s[%s] ", pos, log_level_names[level]);

    /* find the end and attach the rest of the msg */
    for (pos = message; *pos != '\0'; pos++); //empty statement
    sz = pos - message;
    va_start(ap, msgfmt);
    vsnprintf(pos, MAX_LOG_LINE - sz, msgfmt, ap);
    va_end(ap);
    fprintf(f,"%s", message);
    fprintf(f, "\n");

#if 0
#ifdef DEBUG
    /* If we are in debug mode we want everything copied to the stdout */
    if (level != LOG_DEBUG)
        fprintf(stdout, "%s\n", message);
#endif /*DEBUG*/
#endif
}

void log_free(log_t l)
{
    FILE *f = (FILE *) l;

    if(log_file)
    {
        fclose(log_file);
        log_file = NULL;
    }
}

/* spit out debug output */
void debug_log(char *file, int line, const char *msgfmt, ...)
{
    va_list ap;
    char *pos, message[MAX_DEBUG];
    int sz;
    time_t t;
    if (log_level == LOG_DEBUG)
    {
        if (log_file) log_rotate_if_needed();

        FILE *f = (FILE *) log_file ? log_file : stderr;

        /* timestamp */
        t = time(NULL);
        pos = ctime(&t);
        sz = strlen(pos);
        /* chop off the \n */
        pos[sz-1]=' ';

        /* insert the header */
        snprintf(message, MAX_DEBUG, "%s%s:%d ", pos, file, line);

        /* find the end and attach the rest of the msg */
        for (pos = message; *pos != '\0'; pos++); //empty statement
        sz = pos - message;
        va_start(ap, msgfmt);
        vsnprintf(pos, MAX_DEBUG - sz, msgfmt, ap);
        va_end(ap);
        fprintf(f,"%s", message);
        fprintf(f, "\n");
    }
}

#endif
