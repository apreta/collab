
/* our internal wrapper around a fd */
typedef enum { 
    type_CLOSED = 0x00, 
    type_NORMAL = 0x01, 
    type_LISTEN = 0x02, 
    type_CONNECT = 0x10, 
    type_CONNECT_READ = 0x11,
    type_CONNECT_WRITE = 0x12
} mio_type_t;
struct mio_fd_st
{
    mio_type_t type;
    /* app even handler and data */
    mio_handler_t app;
    time_t last_activity;
    void *arg;
};

/* now define our master data type */
struct mio_st
{
    struct mio_fd_st *fds;
    int maxfd;
    int highfd;
    time_t last_idle_check;
    MIO_VARS;
};

/* lazy factor */
#define FD(m,f) m->fds[f]
#define ACT(m,f,a,d) (*(FD(m,f).app))(m,a,f,d,FD(m,f).arg)

#ifdef OLD_MIO_LOG
#define ZONE __LINE__
#ifndef DEBUG
#define DEBUG 0
#endif
#define mio_debug if(DEBUG) _mio_debug
void _mio_debug(int line, const char *msgfmt, ...);
#endif

#define ZONE __FILE__,__LINE__
#define mio_debug debug_log
