#include <sys/epoll.h>

#define MAX_EVENTS 32

struct epoll_fd {
	unsigned int enabled;
	unsigned int set;
};

int _mio_poll(mio_t m, int t);
int _mio_next_fd(mio_t m);
void _mio_init_fd(mio_t m, int fd);
void _mio_remove_fd(mio_t m, int fd);
void _mio_set_read(mio_t m, int fd, int enable);
void _mio_set_write(mio_t m, int fd, int enable);


#define MIO_FUNCS \
    static void _mio_pfds_init(mio_t m)                                 \
    {                                                                   \
        int fd;                                                         \
        for(fd = 0; fd < m->maxfd; fd++) {                              \
            m->pfds[fd].enabled = 0;                                    \
            m->pfds[fd].set = 0;                                    	\
	}								\
    }

#define MIO_VARS 							\
    int pollfd;								\
    struct epoll_fd *pfds;						\
    struct epoll_event events[MAX_EVENTS];				\
    int cur_fd;								\
    int num_fds;

#define MIO_INIT_VARS(m) \
    do {                                                                \
        m->pollfd = epoll_create(maxfd);				\
	if (m->pollfd < 0)						\
	{								\
	    mio_debug(ZONE, "error creating epoll socket");		\
	    return NULL;						\
	}								\
        m->pfds = malloc(sizeof(struct epoll_fd) * maxfd);   			\
	_mio_pfds_init(m);						\
	m->cur_fd = -1;							\
	m->num_fds = 0;							\
    } while(0)
#define MIO_FREE_VARS(m)        close(m->pollfd); free(m->pfds)

#define MIO_INIT_FD(m, pfd)     _mio_init_fd(m, pfd)

#define MIO_REMOVE_FD(m, pfd)   _mio_remove_fd(m, pfd)

#define MIO_CHECK(m, t)         _mio_poll(m, t)

#define MIO_SET_READ(m, fd)     _mio_set_read(m, fd, 1)
#define MIO_SET_WRITE(m, fd)    _mio_set_write(m, fd, 1)

#define MIO_UNSET_READ(m, fd)   _mio_set_read(m, fd, 0)
#define MIO_UNSET_WRITE(m, fd)  _mio_set_write(m, fd, 0)

#define MIO_CAN_READ(m, fd)     (m->pfds[fd].set & (EPOLLIN|EPOLLERR|EPOLLHUP))
#define MIO_CAN_WRITE(m, fd)    (m->pfds[fd].set & EPOLLOUT)

#define MIO_ERROR(m)            errno
