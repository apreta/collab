#ifdef MIO_EPOLL

#include "mio.h"
#include "mio_epoll.h"
#include "mio_internal.h"

/*
 * epoll support for mio
 *
 * Functions are a little crufty, but they fit into the select/poll models
 * already in place in the mio code.
 */

int _mio_poll(mio_t m, int t)
{
	if (m->cur_fd < 0 || m->cur_fd >= m->num_fds)
	{
		m->cur_fd = -1; // first call is next
		m->num_fds = epoll_wait(m->pollfd, m->events, MAX_EVENTS, t*1000);
		return m->num_fds;
	}

	return m->num_fds - m->cur_fd - 1;
}

int _mio_next_fd(mio_t m)
{
	int next_fd;

	if (++m->cur_fd >= m->num_fds)
		return -1;

	next_fd = m->events[m->cur_fd].data.fd;
	m->pfds[next_fd].set = 	m->events[m->cur_fd].events;

	mio_debug(ZONE, "Event %d received on fd %d", m->pfds[next_fd].set, next_fd);

	return next_fd;
}

void _mio_init_fd(mio_t m, int fd)
{
	struct epoll_event ev;
	memset(&ev, 0, sizeof(ev));
	ev.data.fd = fd;

	m->pfds[fd].enabled = 0;
	m->pfds[fd].set = 0;

	if (epoll_ctl(m->pollfd, EPOLL_CTL_ADD, fd, &ev))
	{
		mio_debug(ZONE, "Unable to monitor socket (%d)", errno);
	}
}

void _mio_remove_fd(mio_t m, int fd)
{
	struct epoll_event ev;
	memset(&ev, 0, sizeof(ev));
	ev.data.fd = fd;

	m->pfds[fd].enabled = 0;
	m->pfds[fd].set = 0;

	if (epoll_ctl(m->pollfd, EPOLL_CTL_DEL, fd, &ev))
	{
		mio_debug(ZONE, "Unable to remove socket (%d)", errno);
	}
}

void _mio_set_read(mio_t m, int fd, int enable)
{
	struct epoll_event ev;
	memset(&ev, 0, sizeof(ev));
	ev.data.fd = fd;

	if (m->fds[fd].type == type_CLOSED)
		return;

	if (enable)
		m->pfds[fd].enabled |= EPOLLIN;
	else
		m->pfds[fd].enabled &= ~EPOLLIN;

	ev.events = m->pfds[fd].enabled;
	
	if (epoll_ctl(m->pollfd, EPOLL_CTL_MOD, fd, &ev))
	{
		mio_debug(ZONE, "Unable to monitor socket reads (%d)", errno);
	}
}

void _mio_set_write(mio_t m, int fd, int enable)
{
	struct epoll_event ev;
	memset(&ev, 0, sizeof(ev));
	ev.data.fd = fd;

	if (m->fds[fd].type == type_CLOSED)
		return;

	if (enable)
		m->pfds[fd].enabled |= EPOLLOUT;
	else
		m->pfds[fd].enabled &= ~EPOLLOUT;

	ev.events = m->pfds[fd].enabled;
	
	if (epoll_ctl(m->pollfd, EPOLL_CTL_MOD, fd, &ev))
	{
		mio_debug(ZONE, "Unable to monitor socket writes (%d)", errno);
	}
}

#endif
