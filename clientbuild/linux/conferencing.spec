
%define _prefix /opt/conferencing/client
%define _libdir /opt/conferencing/client/lib

Summary: Conferencing Client
Name: conferencing
Version: 1.0
Release: 1
License: Sitescape
Group: Applications/Communication
URL: http://www.sitescape.com
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
BuildRequires: gcc-c++ gtk2-devel curl-devel gnome-vfs2-devel libjpeg-devel xorg-x11-devel
Requires: gtk2 curl gnome-vfs2

%if "%{_vendor}" == "redhat"
# vnc requirements
BuildRequires: libXtst-devel libXt-devel libXaw-devel
Requires: libXtst libXt
%endif
%if "%{_vendor}" == "suse"
BuildRequires: xorg-x11-devel
%endif

%description
This package contains the Conferencing client

%package devel
Group: Applications/Communication
Summary: Development headers, documentation, and libraries for conferencing

%description devel
Development headers, documentation, and libraries for conferencing

%if "%{_vendor}" == "suse"
%debug_package
%endif

%prep
%setup -q
if [ ! -e iic/clients/bin ]; then
	mkdir iic/clients/bin
fi

%build
( cd wxGTK; ./configure --enable-debug --enable-monolithic --enable-unicode --prefix=%{_prefix}; make )
( cd apr; ./configure --enable-threads --prefix=%{_prefix}; make )
( cd iic/clients/netlib; ./configure --prefix=%{_prefix} && make )
( cd xmlrpc2; ./configure --disable-libwww-client --disable-cplusplus --disable-abyss-server --prefix=%{_prefix} && make )
( cd json-c; ./configure  --prefix=%{_prefix} && make )
( cd iksemel; autoreconf --force --install && ./configure --prefix=%{_prefix} --infodir=%{_infodir} && make )
( cd cryptlib; ./configure --prefix=%{_prefix} && make )
( cd iic/clients/iiclib; ./configure --prefix=%{_prefix} && make )
( cd iaxclient/iaxclient/lib; make )
( cd iic/clients/iaxlib; ./configure --prefix=%{_prefix}; make )
( cd iic/clients/sharelib; ./configure --prefix=%{_prefix}; make )
( cd iic/clients/meeting; ./configure --prefix=%{_prefix}; make )
( cd x11vnc; ./configure --enable-debug; make )
( cd vnc_unixsrc; xmkmf; make World )

%install
rm -rf $RPM_BUILD_ROOT
( cd wxGTK; make DESTDIR=$RPM_BUILD_ROOT install )
( cd apr; make DESTDIR=$RPM_BUILD_ROOT install )
( cd iic/clients/netlib; make DESTDIR=$RPM_BUILD_ROOT install )
( cd xmlrpc2; make DESTDIR=$RPM_BUILD_ROOT install )
( cd json-c; make DESTDIR=$RPM_BUILD_ROOT install )
( cd iksemel; make DESTDIR=$RPM_BUILD_ROOT install )
( cd cryptlib; make DESTDIR=$RPM_BUILD_ROOT install )
( cd iic/clients/iiclib; make DESTDIR=$RPM_BUILD_ROOT install )
( cd iic/clients/iaxlib; make DESTDIR=$RPM_BUILD_ROOT install )
( cd iic/clients/sharelib; make DESTDIR=$RPM_BUILD_ROOT install )
( cd iic/clients/meeting; make DESTDIR=$RPM_BUILD_ROOT install )
( cd x11vnc; cp x11vnc/x11vnc $RPM_BUILD_ROOT/%{_prefix}/bin/mtgshare )
( cd vnc_unixsrc; cp vncviewer/vncviewer $RPM_BUILD_ROOT/%{_prefix}/bin/mtgviewer )

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
   # wxGTK
   %{_libdir}/libwx_gtk2ud-2.8.so
   %{_libdir}/libwx_gtk2ud-2.8.so.0
   %{_libdir}/libwx_gtk2ud-2.8.so.0.4.0

   # APR
   %{_libdir}/libapr-1.so
   %{_libdir}/libapr-1.so.0
   %{_libdir}/libapr-1.so.0.2.8

   # iksemel
   %{_libdir}/libiksemel.so
   %{_libdir}/libiksemel.so.3
   %{_libdir}/libiksemel.so.3.0.0

   # xmlrpc2
   %{_libdir}/libxmlrpc.so
   %{_libdir}/libxmlrpc.so.3
   %{_libdir}/libxmlrpc.so.3.4.0
   %{_libdir}/libxmlrpc_client.so
   %{_libdir}/libxmlrpc_client.so.3
   %{_libdir}/libxmlrpc_client.so.3.4.0
   %{_libdir}/libxmlrpc_server.so
   %{_libdir}/libxmlrpc_server.so.3
   %{_libdir}/libxmlrpc_server.so.3.4.0
   %{_libdir}/libxmlrpc_server_abyss.a
   %{_libdir}/libxmlrpc_server_abyss.la
   %{_libdir}/libxmlrpc_server_abyss.so
   %{_libdir}/libxmlrpc_server_abyss.so.3
   %{_libdir}/libxmlrpc_server_abyss.so.3.4.0
   %{_libdir}/libxmlrpc_server_cgi.so
   %{_libdir}/libxmlrpc_server_cgi.so.3
   %{_libdir}/libxmlrpc_server_cgi.so.3.4.0
   %{_libdir}/libxmlrpc_xmlparse.so
   %{_libdir}/libxmlrpc_xmlparse.so.3
   %{_libdir}/libxmlrpc_xmlparse.so.3.4.0
   %{_libdir}/libxmlrpc_xmltok.so
   %{_libdir}/libxmlrpc_xmltok.so.3
   %{_libdir}/libxmlrpc_xmltok.so.3.4.0

   # json
   %{_libdir}/libjson.so
   %{_libdir}/libjson.so.0
   %{_libdir}/libjson.so.0.0.1

   # cryptlib
   %{_libdir}/libcryptlib.so
   %{_libdir}/libcryptlib.so.1
   %{_libdir}/libcryptlib.so.1.0.0

   # conferencing   
   %{_bindir}/meeting
   %{_bindir}/mtgshare
   %{_bindir}/mtgviewer
   %{_datadir}/meeting
   %{_libdir}/libiaxlib.so
   %{_libdir}/libiaxlib.so.1
   %{_libdir}/libiaxlib.so.1.0.0
   %{_libdir}/libsharelib.so
   %{_libdir}/libsharelib.so.1
   %{_libdir}/libsharelib.so.1.0.0
   %{_libdir}/libiiclib.so
   %{_libdir}/libiiclib.so.1
   %{_libdir}/libiiclib.so.1.0.0
   %{_libdir}/libnetlib.so
   %{_libdir}/libnetlib.so.1
   %{_libdir}/libnetlib.so.1.0.0

	
%files devel
%defattr(-,root,root,-)
   # wxGTK
   %{_bindir}/wx-config
   %{_bindir}/wxrc
   %{_bindir}/wxrc-2.8
   %{_libdir}/wx/config/gtk2-unicode-debug-2.8
   %{_libdir}/wx/include/gtk2-unicode-debug-2.8/wx/setup.h
   %{_datadir}/aclocal/wxwin.m4
   %{_datadir}/bakefile/presets/wx.bkl
   %{_datadir}/bakefile/presets/wx_unix.bkl
   %{_datadir}/bakefile/presets/wx_win32.bkl
   %{_prefix}/include/wx-2.8/wx

   # APR
   %{_includedir}/apr-1
   %{_bindir}/apr-1-config
   %{_libdir}/apr.exp
   %{_libdir}/libapr-1.a
   %{_libdir}/libapr-1.la
   %{_libdir}/pkgconfig/apr-1.pc
   %{_prefix}/build-1/apr_rules.mk
   %{_prefix}/build-1/libtool
   %{_prefix}/build-1/make_exports.awk
   %{_prefix}/build-1/make_var_export.awk
   %{_prefix}/build-1/mkdir.sh

   # iksemel
%if %{suse_version} < 1100
   %{_libdir}/pkgconfig/iksemel.pc
   %{_infodir}/iksemel
%endif
   %{_includedir}/iksemel.h
   %{_libdir}/libiksemel.a
   %{_libdir}/libiksemel.la

   # xmlrpc
   %{_bindir}/xmlrpc-c-config
   %{_includedir}/xmlrpc-c
   %{_libdir}/libxmlrpc.a
   %{_libdir}/libxmlrpc.la
   %{_libdir}/libxmlrpc_client.a
   %{_libdir}/libxmlrpc_client.la
   %{_libdir}/libxmlrpc_server.a
   %{_libdir}/libxmlrpc_server.la
   %{_libdir}/libxmlrpc_server_cgi.a
   %{_libdir}/libxmlrpc_server_cgi.la
   %{_libdir}/libxmlrpc_xmlparse.a
   %{_libdir}/libxmlrpc_xmlparse.la
   %{_libdir}/libxmlrpc_xmltok.a
   %{_libdir}/libxmlrpc_xmltok.la

   # json
   %{_libdir}/libjson.a
   %{_libdir}/libjson.la
   %{_includedir}/json/arraylist.h
   %{_includedir}/json/bits.h
   %{_includedir}/json/debug.h
   %{_includedir}/json/json.h
   %{_includedir}/json/json_object.h
   %{_includedir}/json/json_tokener.h
   %{_includedir}/json/json_util.h
   %{_includedir}/json/linkhash.h
   %{_libdir}/pkgconfig/json.pc

   # cryptlib
   %{_includedir}/cryptlib

   # conferencing   
   %{_includedir}/iiclib
   %{_includedir}/netlib
   %{_includedir}/VoipApi.h
   %{_includedir}/VoipService.h
   %{_includedir}/common.h
   %{_includedir}/eventnames.h
   %{_includedir}/share_api.h

%post
# This is just to allow the pidgin client distributed with conferencing 1.0
# to run
echo "%{_libdir}" >/etc/ld.so.conf.d/conferencing.conf
/sbin/ldconfig

cp %{_prefix}/share/meeting/meeting.desktop /usr/share/applications/
cp %{_prefix}/share/meeting/res/zon.png /usr/share/pixmaps/meeting.png

SERVERTOKEN=0
if [ -d /usr/lib/firefox/defaults/pref ]; then
    cp %{_prefix}/share/meeting/zon.js /usr/lib/firefox/defaults/pref
    sed -i "s/SERVERTOKEN/$SERVERTOKEN/g" /usr/lib/firefox/defaults/pref/zon.js
fi

SERVER=@@@SERVER@@@
HELPPATH=@@@HELPURL@@@
LOOKUPURL=@@@LOOKUPURL@@@
SECURE=@@@SECURE@@@
sed -i -e "s+\"server\",\".*\"+\"server\",\"$SERVER\"+" \
       -e "s+\"HelpPath\",\".*\"+\"HelpPath\",\"$HELPPATH\"+" \
       -e "s+\"EnableSecurity\",\".*\"+\"EnableSecurity\",\"$SECURE\"+" \
       -e "s+\"MeetingLookupURL\",\".*\"+\"MeetingLookupURL\",\"$LOOKUPURL\"+" \
       %{_datadir}/meeting/zon-def.opt

RUNNOW=n
if [ "$RUNNOW" == "y" ]; then
  # assume logged in user is installing us and start client for him
  USER=( `who | grep :0[^.]` )
  if [ ! -z "$USER" ]; then
    sudo -u $USER bash -c "export DISPLAY=:0; %{_bindir}/meeting" >/dev/null 2>&1 &
    if [ -d /home/$USER/Desktop ]; then
      cp %{_datadir}/meeting/meeting.desktop /home/$USER/Desktop/
      chown "$USER" /home/$USER/Desktop/meeting.desktop
    fi
  fi
fi

%postun
if [ "$1" = 0 ]; then
  rm /usr/share/applications/meeting.desktop
  rm /usr/share/pixmaps/meeting.png
  if [ -f /usr/lib/firefox/defaults/pref/zon.js ]; then
      rm /usr/lib/firefox/defaults/pref/zon.js
  fi
  if [ -f /etc/ld.so.conf.d/conferencing.conf ]; then
  	rm /etc/ld.so.conf.d/conferencing.conf
	/sbin/ldconfig
  fi
fi

%changelog
* Mon Aug 11 2008 builder <builder@novell>
- Initial opensource build
