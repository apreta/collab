
%if %{suse_version} < 1030
	%define _prefix /opt/gnome
%endif

Summary: ICEcore protocol library
Name: libicecore
Version: 1.0
Release: 1
License: Sitescape
Group: Applications/Communication
URL: http://www.sitescape.com
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot

BuildRequires: gcc-c++ curl-devel gnome-vfs2-devel gnutls-devel
Requires: curl gnome-vfs2 gnutls

%if "%{_vendor}" == "suse"
BuildRequires: libapr1-devel gconf2-devel
Requires: libapr1 gconf2 
%endif

%if "%{_vendor}" == "redhat"
BuildRequires: apr-devel GConf2-devel
Requires: apr GConf2
%endif

%description
This package contains the libraries for connecting to an ICEcore server.

%package -n libiiclib1
Group: Applications/Communication
Summary: Protocol library

%description -n libiiclib1
Protocol library for connecting to an ICEcore server.

%package -n libnetlib1
Group: Applications/Communication
Summary: Network library

%description -n libnetlib1
Network library used by libicecore package

%package -n libiksemel3
Group: Applications/Communication
Summary: XMPP library

%description -n libiksemel3
XMPP library used by libicecore package

%package -n libxmlrpc3
Group: Applications/Communication
Summary: XMLRPC library

%description -n libxmlrpc3
XMLRPC library used by libicecore package

%package -n libjson0
Group: Applications/Communication
Summary: JSON library

%description -n libjson0
JSON library used by libicecore package

%package devel
Group: Applications/Communication
Summary: Development headers, documentation, and libraries for libicecore
Requires: libiiclib1 libnetlib1 libiksemel3 libxmlrpc3 libjson0
%description devel
Development headers, documentation, and libraries for libicecore

%if "%{_vendor}" == "suse"
%debug_package
%endif

%prep
%setup -q
mkdir iic/clients/bin

# for conferencing, we build and include libapr, but for
# the plugin, we'll use the system-supplied version.  But the
# makefiles expect apr-1-config to be in our source tree, so 
# create a link to the system one.
if [ ! -f apr/apr-1-config ]; then
	ln -s /usr/bin/apr-1-config apr/apr-1-config
fi

%build
( cd json-c; ./configure --prefix=%{_prefix} --libdir=%{_libdir} && make )
( cd iic/clients/netlib; ./configure --prefix=%{_prefix} --libdir=%{_libdir} && make )
( cd xmlrpc2; ./configure --disable-libwww-client --disable-cplusplus --disable-abyss-server --prefix=%{_prefix} --libdir=%{_libdir} && make )
( cd iksemel; autoreconf --force --install && ./configure --prefix=%{_prefix} --libdir=%{_libdir} --infodir=%{_infodir} && make )
( cd iic/clients/iiclib; ./configure --prefix=%{_prefix} --libdir=%{_libdir} && make )

%install
rm -rf $RPM_BUILD_ROOT
mkdir %buildroot
( cd json-c; make DESTDIR=$RPM_BUILD_ROOT install )
( cd iic/clients/netlib; make DESTDIR=$RPM_BUILD_ROOT install; rm $RPM_BUILD_ROOT/%{_includedir}/netlib/IICvncMsg.h )
( cd xmlrpc2; make DESTDIR=$RPM_BUILD_ROOT install )
( cd iksemel; make DESTDIR=$RPM_BUILD_ROOT install )
( cd iic/clients/iiclib; make DESTDIR=$RPM_BUILD_ROOT install )

%clean
rm -rf $RPM_BUILD_ROOT

%files

%files -n libiiclib1
%defattr(-,root,root,-)
   %{_libdir}/libiiclib.so.1
   %{_libdir}/libiiclib.so.1.0.0

%files -n libnetlib1
%defattr(-,root,root,-)
   %{_libdir}/libnetlib.so.1
   %{_libdir}/libnetlib.so.1.0.0

%files -n libiksemel3
%defattr(-,root,root,-)
   %{_libdir}/libiksemel.so.3
   %{_libdir}/libiksemel.so.3.0.0

%files -n libxmlrpc3
%defattr(-,root,root,-)
   %{_libdir}/libxmlrpc.so.3
   %{_libdir}/libxmlrpc.so.3.4.0
   %{_libdir}/libxmlrpc_client.so.3
   %{_libdir}/libxmlrpc_client.so.3.4.0
   %{_libdir}/libxmlrpc_server.so.3
   %{_libdir}/libxmlrpc_server.so.3.4.0
   %{_libdir}/libxmlrpc_server_abyss.so.3
   %{_libdir}/libxmlrpc_server_abyss.so.3.4.0
   %{_libdir}/libxmlrpc_server_cgi.so.3
   %{_libdir}/libxmlrpc_server_cgi.so.3.4.0
   %{_libdir}/libxmlrpc_xmlparse.so.3
   %{_libdir}/libxmlrpc_xmlparse.so.3.4.0
   %{_libdir}/libxmlrpc_xmltok.so.3
   %{_libdir}/libxmlrpc_xmltok.so.3.4.0

%files -n libjson0
%defattr(-,root,root,-)
   %{_libdir}/libjson.so.0
   %{_libdir}/libjson.so.0.0.1

%files devel
%defattr(-,root,root,-)
%if %{suse_version} < 1100
   %{_libdir}/pkgconfig/iksemel.pc
%if %{suse_version} < 1030
   %{_infodir}/iksemel
%else
   %{_infodir}/iksemel.gz
%endif
%endif
   %{_libdir}/libiksemel.a
   %{_libdir}/libiksemel.la
   %{_includedir}/iksemel.h
   
   %{_includedir}/iiclib/addressbk.h
   %{_includedir}/iiclib/common.h
   %{_includedir}/iiclib/controller.h
   %{_includedir}/iiclib/eventnames.h
   %{_includedir}/iiclib/ipc.h
   %{_includedir}/iiclib/options.h
   %{_includedir}/iiclib/schedule.h
   %{_includedir}/iiclib/session_api.h
   %{_includedir}/iiclib/utils.h
   %{_includedir}/netlib/log.h
   %{_includedir}/netlib/net.h
   %{_includedir}/netlib/nethttp.h
   %{_includedir}/netlib/port.h

   %{_libdir}/libiiclib.so
   %{_libdir}/libnetlib.so
   %{_libdir}/libiksemel.so
   %{_libdir}/libxmlrpc.so
   %{_libdir}/libxmlrpc_client.so
   %{_libdir}/libxmlrpc_server.so
   %{_libdir}/libxmlrpc_server_abyss.so
   %{_libdir}/libxmlrpc_server_cgi.so
   %{_libdir}/libxmlrpc_xmlparse.so
   %{_libdir}/libxmlrpc_xmltok.so
   %{_libdir}/libjson.so

   %{_libdir}/libxmlrpc.a
   %{_libdir}/libxmlrpc.la
   %{_libdir}/libxmlrpc_client.a
   %{_libdir}/libxmlrpc_client.la
   %{_libdir}/libxmlrpc_server.a
   %{_libdir}/libxmlrpc_server.la
   %{_libdir}/libxmlrpc_server_abyss.a
   %{_libdir}/libxmlrpc_server_abyss.la
   %{_libdir}/libxmlrpc_server_cgi.a
   %{_libdir}/libxmlrpc_server_cgi.la
   %{_libdir}/libxmlrpc_xmlparse.a
   %{_libdir}/libxmlrpc_xmlparse.la
   %{_libdir}/libxmlrpc_xmltok.a
   %{_libdir}/libxmlrpc_xmltok.la
   %{_bindir}/xmlrpc-c-config
   %{_includedir}/xmlrpc-c/abyss.h
   %{_includedir}/xmlrpc-c/base.h
   %{_includedir}/xmlrpc-c/base_int.h
   %{_includedir}/xmlrpc-c/client.h
   %{_includedir}/xmlrpc-c/oldxmlrpc.h
   %{_includedir}/xmlrpc-c/server.h
   %{_includedir}/xmlrpc-c/server_abyss.h
   %{_includedir}/xmlrpc-c/server_cgi.h
   %{_includedir}/xmlrpc-c/server_w32httpsys.h
   %{_includedir}/xmlrpc-c/transport.h

   %{_libdir}/libjson.a
   %{_libdir}/libjson.la
   %{_includedir}/json/arraylist.h
   %{_includedir}/json/bits.h
   %{_includedir}/json/debug.h
   %{_includedir}/json/json.h
   %{_includedir}/json/json_object.h
   %{_includedir}/json/json_tokener.h
   %{_includedir}/json/json_util.h
   %{_includedir}/json/linkhash.h
   %{_libdir}/pkgconfig/json.pc

#%post
#/sbin/ldconfig

%post -n libiiclib1
/sbin/ldconfig

%post -n libxmlrpc3 
/sbin/ldconfig

%post -n libiksemel3
/sbin/ldconfig

%post -n libjson0
/sbin/ldconfig

%post -n libnetlib1
/sbin/ldconfig

#%postun
#/sbin/ldconfig

%postun -n libiiclib1
/sbin/ldconfig

%postun -n libxmlrpc3 
/sbin/ldconfig

%postun -n libiksemel3
/sbin/ldconfig

%postun -n libjson0
/sbin/ldconfig

%postun -n libnetlib1
/sbin/ldconfig

%changelog
* Mon Aug 11 2008 builder <builder@novell>
- Initial opensource build
