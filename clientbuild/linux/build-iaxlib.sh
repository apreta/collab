#!/bin/bash

. build-common.sh

pushd ../../iic/clients/iaxlib >/dev/null

./configure --prefix=$prefix || exit -1

make || exit -1

sudo make install || exit -1

popd >/dev/null
