#!/bin/bash

. build-common.sh

prefix=/usr

pushd ../../iic/clients/netlib >/dev/null

./configure --prefix=$prefix || exit -1

make || exit -1

sudo make install || exit -1

popd >/dev/null
