#!/bin/sh

# Generate source package for libkablink by copying files from
# local source tree

NAME=libkablink
SRC_ROOT=`pwd`/../..

VERSION=$1
BUILD_NUM=$2

ARCHIVE=$NAME-$VERSION

mkdir -p tempdir/$ARCHIVE 2>/dev/null
rm -rf tempdir/$ARCHIVE/*

WORK_DIR=`pwd`/tempdir/$ARCHIVE

echo "Packaging sources..."

SUBDIRS="apr iic/clients/netlib iic/clients/iiclib iic/services/common iksemel xmlrpc2 json-c"

pushd $SRC_ROOT >/dev/null
tar -c --exclude="CVS*" --exclude="*.a" --exclude="*.la" --exclude="*.o" \
   --exclude="*.lo" --exclude="*.so" --exclude="*.spec" --exclude=".deps" \
   --exclude="*.so.*"  --exclude="*.gch" --exclude=".svn" \
   $SUBDIRS | \
   tar -x -C $WORK_DIR
popd >/dev/null

rm $WORK_DIR/iic/services/common/license*

cp libkablink.spec $WORK_DIR
sed -i -e "s/Version: .*/Version: $VERSION/" $WORK_DIR/libkablink.spec
sed -i -e "s/Release: .*/Release: $BUILD_NUM/" $WORK_DIR/libkablink.spec

tar -zcf $ARCHIVE.tar.gz -C $WORK_DIR/.. $ARCHIVE
