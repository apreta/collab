#!/bin/bash

. build-common.sh

pushd ../../apr >/dev/null

if [ ! "$1" == "--make" ]; then
	./configure --enable-threads --prefix=$prefix
fi

make || exit -1

sudo make install || exit -1

popd >/dev/null
