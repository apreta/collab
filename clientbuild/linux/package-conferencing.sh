#!/bin/sh

# Generate source package for conferencing by copying files from
# local source tree

NAME=conferencing
SRC_ROOT=`pwd`/../..

VERSION=$1
BUILD_NUM=$2
BRAND=$3

if [ -n "$BRAND" ]; then
	if [ ! -e branding/$BRAND/brand.sh ]; then
		echo "Brand $BRAND not found."
		exit -1
	fi
	. branding/$BRAND/brand.sh
fi

ARCHIVE=$NAME-$VERSION

SUBDIRS="wxGTK apr iksemel xmlrpc2 json-c cryptlib iic/clients iic/services/common x11vnc vnc_unixsrc iaxclient"

mkdir -p tempdir/$ARCHIVE 2>/dev/null
rm -rf tempdir/$ARCHIVE/*

WORK_DIR=`pwd`/tempdir/$ARCHIVE

echo "Packaging sources..."

pushd $SRC_ROOT >/dev/null
tar -c --exclude="CVS*" --exclude="*.a" --exclude="*.la" --exclude="*.o" \
   --exclude="*.lo" --exclude="*.so" --exclude="*.spec" --exclude=".deps" \
   --exclude="bld*" --exclude="samples" --exclude="*.so.*" \
   --exclude="*.Po" --exclude="*.gch"  --exclude=".svn" \
   $SUBDIRS | \
   tar -x -C $WORK_DIR
popd >/dev/null

rm $WORK_DIR/iic/services/common/license*

if [ -n "$BRAND" ]; then
	if [ -f branding/$BRAND/meeting.desktop ]; then
	  cp branding/$BRAND/meeting.desktop $WORK_DIR/iic/clients/share/meeting
	fi

	if [ -f branding/$BRAND/zon-def.opt ]; then
	  cp branding/$BRAND/zon-def.opt $WORK_DIR/iic/clients/share/meeting
	fi

	if [ -f branding/$BRAND/about*xrc ]; then
	  cp branding/$BRAND/about*xrc $WORK_DIR/iic/clients/share/meeting
	fi

	if [ -d branding/$BRAND/res ]; then
		echo "Copying branding resources..."
		cp -r branding/$BRAND/res $WORK_DIR/iic/clients/meeting
		rm -rf  $WORK_DIR/iic/clients/meeting/res/CVS
	fi
fi

cp conferencing.spec $WORK_DIR/$NAME.spec
sed -i -e "s/Name: .*/Name: $NAME/" $WORK_DIR/$NAME.spec
sed -i -e "s/Version: .*/Version: $VERSION/" $WORK_DIR/$NAME.spec
sed -i -e "s/Release: .*/Release: $BUILD_NUM/" $WORK_DIR/$NAME.spec
sed -i -e "s/BUILD_NUM=.*/BUILD_NUM=$BUILD_NUM/" $WORK_DIR/$NAME.spec
sed -i -e "s/SERVERTOKEN=.*/SERVERTOKEN=$SERVERTOKEN/" $WORK_DIR/$NAME.spec

sed -i -e "s/VERSION ".*"/VERSION \"$VERSION\"/" \
       -e "s/BUILD_NUM ".*"/BUILD_NUM \"$BUILD_NUM\"/" $WORK_DIR/iic/clients/meeting/version.h

tar -zcf $ARCHIVE.tar.gz -C $WORK_DIR/.. $ARCHIVE
