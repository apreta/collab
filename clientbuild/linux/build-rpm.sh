#!/bin/sh

NAME=conferencing
INSTALL_DIR=opt/conferencing

SERVER=$1
PORTAL=$2
SECURE="no"
DOWNLOAD=/var/iic/htdocs/imidio/downloads
VERSION=9.0.0
BUILD_NUM=1
ARCHIVE=$NAME-$VERSION
ARCH=32
DOWNLOAD=conferencing
SERVERTOKEN=@@@SERVERTOKEN@@@

if [ "$3" == "--secure" ]; then
	SECURE="yes"
fi

if [ -f /etc/SuSE-release ]; then
	PKG_DIR=/usr/src/packages/SOURCES/
	RPM_DIR=/usr/src/packages/RPMS/
	CPU=i586
else
	PKG_DIR=/usr/src/redhat/SOURCES/
	RPM_DIR=/usr/src/redhat/RPMS/
	CPU=i386
fi

if [ "$ARCH" == "64" ]; then
	CPU=x86_64
	DOWNLOAD="conferencing-64bit"
fi

if [ "$SECURE" != "yes" ]; then
	HELPPATH=http://$PORTAL/imidio/client-manual/
	LOOKUPURL=http://$PORTAL/imidio/invite/getstartup.php
else
	HELPPATH=https://$PORTAL/imidio/client-manual/
	LOOKUPURL=https://$PORTAL/imidio/invite/getstartup.php
fi

if [ ! -f $ARCHIVE.tar.gz ]; then
	echo "Build source package first (build-client.sh)"
	exit -1
fi

cp $ARCHIVE.tar.gz $PKG_DIR

sed -i -e "s/@@@SERVERTOKEN@@@/$SERVERTOKEN/g" zon-binary.spec

if [ ! -z "$1" ]; then
	sed -i -e "s+SERVER=.*+SERVER=$SERVER+" \
		   -e "s+HELPPATH=.*+HELPPATH=$HELPPATH+" \
		   -e "s+SECURE=.*+SECURE=$SECURE+" \
		   -e "s+LOOKUPURL=.*+LOOKUPURL=$LOOKUPURL+" \
		zon-binary.spec
fi

if [ ! -z $VERSION ]; then
	sed -i -e "s/Version: .*/Version: $VERSION/" zon-binary.spec
fi

echo "Building RPM..."
rpmbuild -ba zon-binary.spec &>zonmeeting-rpmbuild.log && echo "Ok" || echo "FAILED"

echo "Copying RPM..."
if [ -f $DOWNLOAD ]; then
	cp $RPM_DIR/$CPU/$NAME-$VERSION-$BUILD_NUM.$CPU.rpm $DOWNLOAD/$DOWNLOAD.rpm
else
	echo "$NAME must be copied to the download directory on the portal server."
fi
cp $RPM_DIR/$CPU/$NAME-$VERSION-$BUILD_NUM.$CPU.rpm .
cp $RPM_DIR/$CPU/$NAME-$VERSION-$BUILD_NUM.$CPU.rpm $DOWNLOAD.rpm

