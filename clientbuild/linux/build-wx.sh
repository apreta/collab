#!/bin/bash

# Linux only so far, use include Makefile.mingw on Windows

. build-common.sh

WXGTK=wxGTK
BUILD=debug

if [ "$BUILD" == "debug" ]; then
    conf_option="--enable-debug"
fi

cd ../../$WXGTK

#if [ ! -e bld$BUILD ]; then
#    mkdir bld$BUILD
#fi

#cd bld$BUILD
#../configure  $conf_option --enable-monolithic --enable-unicode --prefix=$prefix

./configure  $conf_option --enable-monolithic --enable-unicode --prefix=$prefix

make
sudo make install
