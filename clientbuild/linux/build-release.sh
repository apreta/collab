#!/bin/bash

VERSION=$1
BUILD_NUM=$2
BRAND=$3
OPTION=$4
HOST=conf.imidio.com
PORTAL=conf.imidio.com

if [ -f /etc/SuSE-release ]; then
	sles_install="y"
else
	sles_install=""
fi

if [ $# -ne 3 -a $# -ne 4 ]; then
	echo "Usage: ./build-release.sh <version> <buildnum> <brand>"
	echo "Version is #.#.#, buildnum is single number"
	exit -1
fi

echo "Building binaries"

./build-client.sh $VERSION $BUILD_NUM $OPTION || exit -1

echo "Creating package kit"

./package-client.sh $VERSION $BUILD_NUM $BRAND || exit -1

echo "Build RPM"

( cd zon-package-kit; ./build-rpm.sh || exit -1 )

echo "Done."
