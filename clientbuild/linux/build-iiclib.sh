#!/bin/bash

. build-common.sh

prefix=/usr

pushd ../../iic/clients/iiclib >/dev/null

./configure --prefix=$prefix || exit -1

make || exit -1

sudo make install || exit -1

popd >/dev/null
