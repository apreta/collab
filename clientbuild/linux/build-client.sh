#!/bin/sh

# Builds all client binaries and installs into build area
# Also updates binaries in source tree (iic-client/bin)

INSTALL_DIR=opt/conferencing/client
BUILD_ROOT=`pwd`/builddir
CLIENT_ROOT=iic-client

VERSION=$1
BUILD_NUM=$2
OPTIONS=

PREFIX=/opt/conferencing/client

pushd ../.. >/dev/null

if [ "$3" != "--make" ]; then 
	echo "Scrubbing IIC (warning: doesn't clean third party libs)"
	( cd $CLIENT_ROOT; make clean )
	( cd x11vnc; make clean )
	( cd vnc_unixsrc; make clean )
else
    echo "Make mode"
	MAKE="y"
fi

if [ "$3" == "--skipwx" ]; then
	SKIPWX="y"
fi

#
# Build
#

echo "Updating version..."
if [ ! -z "$VERSION" ]; then
( cd $CLIENT_ROOT/meeting; \
  sed -i -e "s/VERSION ".*"/VERSION \"$VERSION\"/" \
         -e "s/BUILD_NUM ".*"/BUILD_NUM \"$BUILD_NUM\"/" version.h )
fi

echo "Building wx..."
( cd wxGTK
  if [ -z "$SKIPWX" ]; then
    if [ -z "$MAKE" ]; then
      ./configure --enable-debug --enable-monolithic --enable-unicode --prefix=$PREFIX || exit -1
    fi
    make || exit -1
  fi
  cp --preserve=all lib/lib*so* ../$CLIENT_ROOT/bin
)

echo "Building APR..."
( cd apr
  if [ -z "$MAKE" ]; then
	./configure --enable-threads --prefix=$PREFIX || exit -1
  fi
  make || exit -1
  cp --preserve=all .libs/lib*so* ../$CLIENT_ROOT/bin 
)

echo "Building netlib..."
( cd iic-client/netlib;
  if [ -z "$MAKE" ]; then
    ./configure --prefix=$PREFIX || exit -1
  fi
  make || exit -1 
)

echo "Building xmlrpc..."
( cd xmlrpc2
  if [ -z "$MAKE" ]; then
    ./configure --disable-libwww-client --disable-cplusplus --disable-abyss-server --prefix=$PREFIX || exit -1
  fi
  make || exit -1
  cp --preserve=all src/.libs/lib*so* ../$CLIENT_ROOT/bin   
)

echo "Building json-c..."
( cd json-c;
  if [ -z "$MAKE" ]; then
    ./configure --prefix=$PREFIX || exit -1
  fi
  make || exit -1 
  cp --preserve=all .libs/lib*so* ../$CLIENT_ROOT/bin
)

echo "Building iksemel..."
( cd iksemel
  if [ -z "$MAKE" ]; then
    autoreconf --force --install || exit -1
    ./configure --prefix=$PREFIX || exit -1
  fi
  make || exit -1
  cp --preserve=all src/.libs/lib*so* ../$CLIENT_ROOT/bin       
)

echo "Building cryptlib..."
( cd iic-client/cryptlib
  if [ -z "$MAKE" ]; then
    ./configure --prefix=$PREFIX || exit -1
  fi
  make || exit -1 
  cp --preserve=all lib*so* ../$CLIENT_ROOT/bin
)

echo "Building iiclib..."
( cd iic-client/iiclib
  if [ -z "$MAKE" ]; then
    ./configure --prefix=$PREFIX || exit -1
  fi
  make || exit -1 
)

echo "Building speex..."
( cd speex
  if [ -z "$MAKE" ]; then
    ./configure --prefix=$PREFIX || exit -1
  fi
  make || exit -1 
  cp --preserve=all libspeex/.libs/lib*so* ../$CLIENT_ROOT/bin
)

echo "Building portaudio..."
( cd portaudio
  if [ -z "$MAKE" ]; then
    ./configure --prefix=$PREFIX || exit -1
  fi
  make || exit -1 
  cp --preserve=all lib/.libs/lib*so* ../$CLIENT_ROOT/bin
)


echo "Building iaxclient..."
( cd iaxclient2
  if [ -z "$MAKE" ]; then
    export PORTAUDIO_CFLAGS=-I`pwd`/../portaudio/include
    export PORTAUDIO_LIBS="-L`pwd`/../portaudio/lib/.libs -lportaudio"
    export SPEEX_CFLAGS=-I`pwd`/../speex/include
    export SPEEX_LIBS="-L`pwd`/../speex/libspeex/.libs -lspeex"
    export SPEEXDSP_CFLAGS=-I`pwd`/../speex/include
    export SPEEXDSP_LIBS="-L`pwd`/../speex/libspeex/.libs -lspeexdsp"
    ./configure --prefix=$PREFIX --disable-clients --disable-video || exit -1
  fi
  make|| exit -1 
  cp --preserve=all lib/.libs/lib*so* ../$CLIENT_ROOT/bin
)

echo "Building iaxlib..."
( cd iic-client/iaxlib
  if [ -z "$MAKE" ]; then
    ./configure --prefix=$PREFIX || exit -1
  fi
  make|| exit -1 
)

echo "Building sharelib..."
( cd iic-client/sharelib; 
  if [ -z "$MAKE" ]; then
    ./configure --prefix=$PREFIX || exit -1
  fi
  make|| exit -1 
)

echo "Building meeting..."
( cd iic-client/meeting
  if [ -z "$MAKE" ]; then
   ./configure --prefix=$PREFIX || exit -1
  fi 
  make || exit -1 
)

echo "Building vnc components..."
( cd x11vnc; 
  if [ -z "$MAKE" ]; then
    ./configure --enable-debug || exit -1
  fi
  make || exit -1
  cp x11vnc/x11vnc ../$CLIENT_ROOT/bin/mtgshare
)
( cd vnc_unixsrc;
  if [ -z "$MAKE" ]; then
   xmkmf || exit -1
  fi
  make World || exit -1
  cp vncviewer/vncviewer ../$CLIENT_ROOT/bin/mtgviewer
)

#
# Install 
#

echo "Installing..."

if [ -e $BUILD_ROOT ]; then
	rm -rf $BUILD_ROOT
	mkdir -p $BUILD_ROOT/opt/conferencing/client
fi

( cd wxGTK; make DESTDIR=$BUILD_ROOT install )
( cd apr; make DESTDIR=$BUILD_ROOT install )
( cd iic-client/netlib; make DESTDIR=$BUILD_ROOT install )
( cd xmlrpc2; make DESTDIR=$BUILD_ROOT install )
( cd json-c; make DESTDIR=$BUILD_ROOT install )
( cd iksemel; make DESTDIR=$BUILD_ROOT install )
( cd portaudio; make DESTDIR=$BUILD_ROOT install )
( cd speex; make DESTDIR=$BUILD_ROOT install )
( cd iaxclient2; make DESTDIR=$BUILD_ROOT install )
( cd $CLIENT_ROOT/cryptlib; make DESTDIR=$BUILD_ROOT install )
( cd $CLIENT_ROOT/iiclib; make DESTDIR=$BUILD_ROOT install )
( cd $CLIENT_ROOT/iaxlib; make DESTDIR=$BUILD_ROOT install )
( cd $CLIENT_ROOT/sharelib; make DESTDIR=$BUILD_ROOT install )
( cd $CLIENT_ROOT/meeting; make DESTDIR=$BUILD_ROOT install )
( cd x11vnc; cp x11vnc/x11vnc $BUILD_ROOT/$PREFIX/bin/mtgshare )
( cd vnc_unixsrc; cp vncviewer/vncviewer $BUILD_ROOT/$PREFIX/bin/mtgviewer )

echo "DONE"

popd >/dev/null
