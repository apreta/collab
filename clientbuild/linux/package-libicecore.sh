#!/bin/sh

# Generate source package for libicecore by copying files from
# local source tree

NAME=libicecore
SRC_ROOT=`pwd`/../..

VERSION=$1
BUILD_NUM=$2

ARCHIVE=$NAME-$VERSION

mkdir -p tempdir/$ARCHIVE 2>/dev/null
rm -rf tempdir/$ARCHIVE/*

WORK_DIR=`pwd`/tempdir/$ARCHIVE

echo "Packaging sources..."

SUBDIRS="apr iic/clients/netlib iic/clients/iiclib iic/services/common iksemel xmlrpc2 json-c"

pushd $SRC_ROOT >/dev/null
tar -c --exclude="CVS*" --exclude="*.a" --exclude="*.la" --exclude="*.o" \
   --exclude="*.lo" --exclude="*.so" --exclude="*.spec" --exclude=".deps" \
   --exclude="*.so.*"  --exclude="*.gch" \
   $SUBDIRS | \
   tar -x -C $WORK_DIR
popd >/dev/null

rm $WORK_DIR/iic/services/common/license*

cp libicecore.spec $WORK_DIR
sed -i -e "s/Version: .*/Version: $VERSION/" $WORK_DIR/libicecore.spec
sed -i -e "s/Release: .*/Release: $BUILD_NUM/" $WORK_DIR/libicecore.spec
#sed -i -e "s/Name: .*/Name: $NAME/" $WORK_DIR/libicecore.spec
#sed -i -e "s/VERSION=.*/VERSION=$VERSION/" $WORK_DIR/libicecore.spec
#sed -i -e "s/BUILD_NUM=.*/BUILD_NUM=$BUILD_NUM/" $WORK_DIR/libicecore.spec
#sed -i -e "s/NAME=.*/NAME=$NAME/" $WORK_DIR/libicecore.spec

tar -zcf $ARCHIVE.tar.gz -C $WORK_DIR/.. $ARCHIVE
