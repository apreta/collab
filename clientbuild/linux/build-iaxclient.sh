#!/bin/bash

. build-common.sh

pushd ../../iaxclient >/dev/null

cd iaxclient/lib

make || exit -1

popd >/dev/null
