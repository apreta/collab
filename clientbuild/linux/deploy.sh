#!/bin/sh

VERSION=$1
BUILD_NUM=$2
BRAND=$3

DEPLOY=mike@dbs.homedns.org
PLATFORM=sled10

NAME=conferencing

. branding/$BRAND/brand.sh

if [ -f /etc/SuSE-release ]; then
	PKG_DIR=/usr/src/packages/SOURCES/
	RPM_DIR=/usr/src/packages/RPMS/
	CPU=i586
else
	PKG_DIR=/usr/src/redhat/SOURCES/
	RPM_DIR=/usr/src/redhat/RPMS/
	CPU=i386
fi

if [ "`uname -i`" == "x86_64" ]; then
	ARCH=64
	CPU=x86_64
	DOWNLOAD="conferencing-64bit.rpm"
else
	ARCH=32
	DOWNLOAD="conferencing.rpm"
fi

RPM=$NAME-$VERSION-$BUILD_NUM.$CPU.rpm

echo Copying $DOWNLOAD to $DEPLOY/installers-$VERSION
scp zon-package-kit/$DOWNLOAD $DEPLOY:installers-$VERSION/

echo Copying $RPM to $DEPLOY/downloads-$VERSION/downloads/$PLATFORM
scp zon-package-kit/$RPM $DEPLOY:downloads-$VERSION/downloads/$PLATFORM/
