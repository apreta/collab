Summary: Conferencing Client
Name: zon-binary
Version: 9.0.2
Release: 1
License: Sitescape
Group: Applications/Communication
URL: http://www.sitescape.com
Source0: %{name}-%{version}.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
%ifnarch x86_64
BuildArch: i586
%endif
Requires: gnutls gtk2
Provides: libiiclib.so

%description
This package contains the conferencing client.

%debug_package

%prep
%setup -q

%build

%install
rm -rf $RPM_BUILD_ROOT
INSTALL=opt/conferencing/client
mkdir -p $RPM_BUILD_ROOT/$INSTALL/lib
mkdir -p $RPM_BUILD_ROOT/$INSTALL/bin
mkdir -p $RPM_BUILD_ROOT/$INSTALL/share/meeting
( tar -c $INSTALL/lib $INSTALL/bin $INSTALL/share/meeting | tar -x -C $RPM_BUILD_ROOT )

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/opt/conferencing/client/bin
/opt/conferencing/client/lib
/opt/conferencing/client/share/meeting

%post
INSTALL=/opt/conferencing/client

# This is just to allow the pidgin client distributed with conferencing 1.0
# to run
echo "/opt/conferencing/client/lib" >/etc/ld.so.conf.d/conferencing.conf
/sbin/ldconfig

cp $INSTALL/share/meeting/meeting.desktop /usr/share/applications/
cp $INSTALL/share/meeting/res/zon.png /usr/share/pixmaps/meeting.png

SERVERTOKEN=@@@SERVERTOKEN@@@
if [ -d /usr/lib/firefox/defaults/pref ]; then
    cp $INSTALL/share/meeting/zon.js /usr/lib/firefox/defaults/pref
    sed -i "s/SERVERTOKEN/$SERVERTOKEN/g" /usr/lib/firefox/defaults/pref/zon.js
fi

SERVER=@@@SERVER@@@
HELPPATH=@@@HELPURL@@@
LOOKUPURL=@@@LOOKUPURL@@@
SECURE=@@@SECURE@@@
sed -i -e "s+\"server\",\".*\"+\"server\",\"$SERVER\"+" \
       -e "s+\"HelpPath\",\".*\"+\"HelpPath\",\"$HELPPATH\"+" \
       -e "s+\"EnableSecurity\",\".*\"+\"EnableSecurity\",\"$SECURE\"+" \
       -e "s+\"MeetingLookupURL\",\".*\"+\"MeetingLookupURL\",\"$LOOKUPURL\"+" \
       $INSTALL/share/meeting/zon-def.opt

RUNNOW=n
if [ "$RUNNOW" == "y" ]; then
  # assume logged in user is installing us and start client for him
  USER=( `who | grep :0[^.]` )
  if [ ! -z "$USER" ]; then
    sudo -u $USER bash -c "export DISPLAY=:0; $INSTALL/bin/meeting" >/dev/null 2>&1 &
    if [ -d /home/$USER/Desktop ]; then
      cp $INSTALL/share/meeting/meeting.desktop /home/$USER/Desktop/
      chown "$USER" /home/$USER/Desktop/meeting.desktop
    fi
  fi
fi

%postun
if [ "$1" = 0 ]; then
  rm /usr/share/applications/meeting.desktop
  rm /usr/share/pixmaps/meeting.png
  if [ -f /usr/lib/firefox/defaults/pref/zon.js ]; then
      rm /usr/lib/firefox/defaults/pref/zon.js
  fi
  if [ -f /etc/ld.so.conf.d/conferencing.conf ]; then
  	rm /etc/ld.so.conf.d/conferencing.conf
	/sbin/ldconfig
  fi
fi

%changelog
* Thu Oct  7 2004 root <root@dbs.homedns.org> 1.0.0-1
- Initial build.

