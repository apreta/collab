#!/bin/sh

# Generate source package for pidgin conferencing plugin by copying files from
# local source tree

NAME=pidgin-kablink
SRC_ROOT=`pwd`/../../iic/clients/pidgin/plugin

VERSION=$1
BUILD_NUM=$2

ARCHIVE=$NAME-$VERSION

mkdir -p tempdir/$ARCHIVE 2>/dev/null
rm -rf tempdir/$ARCHIVE/*

WORK_DIR=`pwd`/tempdir/$ARCHIVE

echo "Packaging sources..."

pushd $SRC_ROOT >/dev/null
tar -c --exclude="CVS*" --exclude="*.a" --exclude="*.la" --exclude="*.o" \
   --exclude="*.lo" --exclude="*.so" --exclude=".deps" \
   --exclude="*.so.*" --exclude=".svn" --exclude="*icecore*" \
   * | \
   tar -x -C $WORK_DIR
popd >/dev/null

sed -i -e "s/Version: .*/Version: $VERSION/" $WORK_DIR/pidgin-kablink.spec
sed -i -e "s/Release: .*/Release: $BUILD_NUM/" $WORK_DIR/pidgin-kablink.spec
#sed -i -e "s/Name: .*/Name: $NAME/" $WORK_DIR/libkablink.spec
#sed -i -e "s/VERSION=.*/VERSION=$VERSION/" $WORK_DIR/libkablink.spec
#sed -i -e "s/BUILD_NUM=.*/BUILD_NUM=$BUILD_NUM/" $WORK_DIR/libkablink.spec
#sed -i -e "s/NAME=.*/NAME=$NAME/" $WORK_DIR/libkablink.spec

tar -zcf $ARCHIVE.tar.gz -C $WORK_DIR/.. $ARCHIVE
