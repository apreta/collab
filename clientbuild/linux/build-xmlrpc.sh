#!/bin/bash

. build-common.sh

prefix=/usr

pushd ../../xmlrpc2 >/dev/null

if [ ! "$1" == "--make" ]; then
	./configure --disable-libwww-client --disable-cplusplus --disable-abyss-server --prefix=$prefix
fi

make all || exit -1

sudo make install || exit -1

popd >/dev/null
