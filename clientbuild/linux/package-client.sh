#!/bin/sh

# Alternative RPM build procedure:
# Packages binaries of prebuilt client into source archive; source archive
# and .spec can be distributed with server so that RPM with correct server
# info can be built during server install.

NAME=conferencing
INSTALL_DIR=opt/conferencing/client
BUILD_ROOT=`pwd`/builddir

VERSION=$1
BUILD_NUM=$2
BRAND=$3

if [ -f branding/$BRAND/brand.sh ]; then
    . branding/$BRAND/brand.sh
fi

ARCHIVE=$NAME-$VERSION
ARCH=32

if [ "`uname -i`" == "x86_64" ]; then
	ARCH=64
fi

mkdir -p tempdir/$ARCHIVE 2>/dev/null
rm -rf tempdir/$ARCHIVE/*

WORK_DIR=`pwd`/tempdir/$ARCHIVE

echo "Packaging binaries..."

tar -c --exclude="CVS*" --exclude="*.a" --exclude="*.la" --exclude="*.h" \
   -C $BUILD_ROOT $INSTALL_DIR/lib $INSTALL_DIR/bin $INSTALL_DIR/share/meeting | 
   tar -x -C $WORK_DIR

if [ "$4" == "--strip" ]; then
  echo "Stripping debug info..."
  ( cd $WORK_DIR/$INSTALL_DIR/bin; strip meeting mtgshare mtgviewer )
  ( cd $WORK_DIR/$INSTALL_DIR/lib; strip *.so )
fi

if [ -f branding/$BRAND/meeting.desktop ]; then
  cp branding/$BRAND/meeting.desktop $WORK_DIR/$INSTALL_DIR/share/meeting
fi

if [ -f branding/$BRAND/zon-def.opt ]; then
  cp branding/$BRAND/zon-def.opt $WORK_DIR/$INSTALL_DIR/share/meeting
fi

if [ -f branding/$BRAND/about*xrc ]; then
  cp branding/$BRAND/about*xrc $WORK_DIR/$INSTALL_DIR/share/meeting
fi

if [ -d branding/$BRAND/res ]; then
	echo "Copying branding resources..."
	cp -r branding/$BRAND/res $WORK_DIR/$INSTALL_DIR/share/meeting
	rm -rf  $WORK_DIR/$INSTALL_DIR/share/meeting/res/CVS
fi

echo "Building package kit..."

rm -rf zon-package-kit
mkdir zon-package-kit

tar -zcf zon-package-kit/$ARCHIVE.tar.gz -C $WORK_DIR/.. $ARCHIVE
cp zon-binary.spec zon-package-kit
cp build-rpm.sh zon-package-kit
sed -i -e "s/Version: .*/Version: $VERSION/" zon-package-kit/zon-binary.spec
sed -i -e "s/Release: .*/Release: $BUILD_NUM/" zon-package-kit/zon-binary.spec
sed -i -e "s/Name: .*/Name: $NAME/" zon-package-kit/zon-binary.spec
sed -i -e "s/@@@SERVERTOKEN@@@/$SERVERTOKEN/g" zon-package-kit/zon-binary.spec
sed -i -e "s/VERSION=.*/VERSION=$VERSION/" zon-package-kit/build-rpm.sh
sed -i -e "s/BUILD_NUM=.*/BUILD_NUM=$BUILD_NUM/" zon-package-kit/build-rpm.sh
sed -i -e "s/NAME=.*/NAME=$NAME/" zon-package-kit/build-rpm.sh
sed -i -e "s/ARCH=.*/ARCH=$ARCH/" zon-package-kit/build-rpm.sh
sed -i -e "s/@@@SERVERTOKEN@@@/$SERVERTOKEN/g" zon-package-kit/build-rpm.sh

tar -zcf zon-package-kit-$VERSION.tar.gz zon-package-kit
