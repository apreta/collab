#!/bin/bash

VER=(`echo $1 | tr "." " "`)
BUILD_NUM=$2
FILE="../../iic-client/meeting/versionrc.h"

RC="${VER[0]},${VER[1]},${VER[2]},$BUILD_NUM"
RC_STR="\"${VER[0]}, ${VER[1]}, ${VER[2]}, $BUILD_NUM\""

echo "#define VERSION_RC $RC" >$FILE
echo "#define VERSION_RC_STR $RC_STR" >>$FILE
