@echo off

set VERSION=%1
set BUILDNUM=%2
set BRAND=%3
set OPTION=%4

if /I .%BRAND%==./make set OPTION=%BRAND%
if /I .%BRAND%==./make set BRAND=apreta
if /I .%BRAND%==. set BRAND=apreta

xcopy /Y branding\%BRAND%\res\zon.ico ..\..\iic-client\meeting\res\

call build-client %VERSION% %BUILDNUM% %OPTION%
if %ERRORLEVEL% EQU 0 goto package
echo "Failed to build client"
exit /b 1

:package
call package-client %VERSION% %BUILDNUM% %BRAND%

pushd "Zon Package Kit"
call build-installer
if %ERRORLEVEL% EQU 0 goto done
echo "Failed to build installer"
popd
exit /b 1

:done
popd

copy "Zon Package Kit\conferencing.exe" %BRAND%-%VERSION%-%BUILDNUM%.exe

echo "Done"
