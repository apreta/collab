
set WXWIN=\wxWidgets-2.8.11
set WXBUILD=debug
set OUT=gcc_dll\wxmsw28ud_gcc.dll

pushd %WXWIN%\build\msw

if .%1 == ./make goto BUILD
	mingw32-make SHARED=1 UNICODE=1 MONOLITHIC=1 OFFICIAL_BUILD=1 USE_GDIPLUS=1 BUILD=%WXBUILD% -f makefile.gcc clean

:BUILD

mingw32-make SHARED=1 UNICODE=1 MONOLITHIC=1 OFFICIAL_BUILD=1 USE_GDIPLUS=1 BUILD=%WXBUILD% -f makefile.gcc

popd

copy %WXWIN%\lib\%OUT% ..\..\iic-client\bin
