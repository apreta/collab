;;
;; branding.nsi
;;
;; Copyright (c) 2005 SiteScape, Inc.
;; 
;; Defines information needed to build a branded version of the installers
;;

!define product_name              "Apreta Conferencing"
!define product_folder      	  "Apreta" 
!define installer_registry_key 	  "Apreta_client"
!define server_token              "1"
!define include_g729