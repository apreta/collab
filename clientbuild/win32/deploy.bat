@echo off

set VERSION=%1
set BUILDNUM=%2
set BRAND=%3

set DEPLOY=mike@dbs.homedns.org

echo Copying conferencing.exe to %DEPLOY%:installers-%VERSION%
pscp "Zon Package Kit\conferencing.exe" %DEPLOY%:installers-%VERSION%/
