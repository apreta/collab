@ECHO OFF

rem Usage: build-client VER BUILD [/make]
rem 
rem To build VS.NET portions you'll need to run vcvars32.bat first to
rem set up environment variables.
rem 
rem CodeBlocks should be installed at \CodeBlocks.
rem MSYS should be installed at \msys.
rem \mingw\bin should be on your path.

SET VER=%1%
SET BUILD_NUM=%2%

IF EXIST "\Program Files\CodeBlocks\codeblocks.exe" SET CODEBLOCKS="\Program Files\CodeBlocks\codeblocks"
IF EXIST "\Program Files (x86)\CodeBlocks\codeblocks.exe" SET CODEBLOCKS="\Program Files (x86)\CodeBlocks\codeblocks"
IF EXIST "\codeblocks\codeblocks.exe" SET CODEBLOCKS="\codeblocks\codeblocks"
SET MSYS=\msys\bin
SET MINGW=\mingw\bin

echo %CODEBLOCKS%

SET BUILD=Release
SET BUILD_COMMAND=/rebuild
SET BUILD_COMMAND_CB=--rebuild
SET OLDPATH=%PATH%

IF /I .%3%==./make SET BUILD_COMMAND=/build
IF /I .%3%==./make SET BUILD_COMMAND_CB=--build

IF .%3%.==./skipwx. goto bldclient 

echo Building wxWidgets
PATH %MINGW%
call build-wx %3%
if %errorlevel% NEQ 0 GOTO ERROR

:bldclient

PUSHD .
cd ..\..

PATH %OLDPATH%

rem BUILD TARGET: IICPlugin.ocx
echo Building ActiveX control
cd iic-client\IICPlugin
devenv  iicplugin.sln %BUILD_COMMAND% "Unicode %BUILD% Compatible" /project "IICPlugin"
cd ..\..

rem BUILD TARGET: mtgviewer.exe
echo Building VNC components
cd ultravnc\vncviewer
devenv vncviewer.sln %BUILD_COMMAND% "%BUILD%" /project "vncviewer"
cd ..\..

rem BUILD TARGET: mtgshare.exe
rem BUILD TARGET: mtghooks.dll
cd ultravnc\winvnc
devenv winvnc.sln %BUILD_COMMAND% "%BUILD%" /project "winvnc"
cd ..\..

rem BUILD TARGET: oo_impress.dll
rem echo Building DocShare upload component oo_impress.dll
rem cd iic-client\docshare\oo_impress
rem devenv oo_impress.sln %BUILD_COMMAND% "Debug" /project "oo_impress"
rem cd ..\..\..\..

rem BUILD TARGET: ms_outlook.dll
echo Building ms_outlook.dll
cd iic-client\connectivity\ms_outlook
devenv ms_outlook.sln %BUILD_COMMAND% "%BUILD%" /project "ms_outlook"
cd ..\..\..

POPD

PATH %MSYS%
bash make-version.sh %VER% %BUILD_NUM%

PATH %MINGW%;%MSYS%
rem BUILD meeting libs & client
echo Building libraries
%CODEBLOCKS% /na /nd %BUILD_COMMAND_CB% --target=Debug ..\..\iic-client\client-libs.workspace >build-libs.log
if %errorlevel% NEQ 0 GOTO ERROR
echo Building meeting client
%CODEBLOCKS% /na /nd %BUILD_COMMAND_CB% --target=Debug ..\..\iic-client\clients.workspace >build-client.log
if %errorlevel% NEQ 0 GOTO ERROR

echo *** Build completed ***

goto DONE

:ERROR

echo *** Build failed ***

:DONE

PATH %OLDPATH%
