@echo off
rem -------------------------------------------------------------------------
rem 
rem  build-installer.bat
rem 
rem  Create the Zon installer
rem
rem  Copyright (c) 2005 SiteScape, Inc.
rem
rem  Usage: build-installer <xmlrouter-hname> <portal-hname>
rem
rem -------------------------------------------------------------------------

set install_compiler=..\Bin\makensis.exe

rem ----- Check Parameters and display usage if necessary

@echo.

SET branding=custom
if "%1"=="" goto ENTERPRISE

SET server=%1
SET portal=%2

goto START

:ENTERPRISE
set NOBRAND=yes

:START

@echo Starting Zon package build:

DATE /TT
TIME /T 
@echo.
DATE /TT >> package.log
TIME /T >> package.log

rem ----- Delete any old build artifacts

@echo Removing previous installers
@echo Removing previous installers >> package.log

del /Q /S .\Temp\locale\*.* 2>> package.log
del /Q .\Temp\res\*.* 2>> package.log
del /Q .\Temp\*.* 2>> package.log

rem ----- Copy core files to staging location

@echo Copying core files
@echo Copying core files >> package.log

xcopy /Y ".\Core Files\*.*" .\Temp\ >> package.log
xcopy /Y ".\Core Files\res\*.*" .\Temp\res\ >> package.log
xcopy /EY ".\Core Files\locale\*.*" .\Temp\locale\ >> package.log
xcopy /EY ".\Core Files\docshare\*.*" .\Temp\docshare\ >> package.log
xcopy /EY ".\Core Files\connectivity\*.*" .\Temp\connectivity\ >> package.log

rem ----- Copy Trusted Certificate files to staging location

@echo Copying Trusted Certificate files
@echo Copying Trusted Certificate files >> package.log

copy ".\Certificates\*.pem" .\Temp >> package.log

rem ----- Copy build scripts to staging location

@echo Copying package scripts
@echo Copying package scripts >> package.log

copy ".\Package Scripts\*.*" .\Temp >> package.log

@echo Creating configuration files 
@echo Creating configuration files >> package.log

rem @echo "server","%server%" >> .\temp\zon-def.opt
rem @echo "HelpPath","%helppath%" >> .\temp\zon-def.opt
rem @echo "EnableSecurity","%secure%" >> .\temp\zon-def.opt

if .%NOBRAND%==.yes goto CREATEVERSION

@echo !define property_iicserver "%server%"  >> .\temp\branding.nsi
@echo !define property_meetinglookupurl "http://%portal%/imidio/invite/getstartup.php"  >> .\temp\branding.nsi
@echo !define property_helppath "http://%portal%/imidio/client-manual/"  >> .\temp\branding.nsi
@echo !define property_enable_security "no"  >> .\temp\branding.nsi


rem ----- Create a Version.txt file for use by the build scripts

:CREATEVERSION

cd .\Temp >> package.log
cd >> ..\package.log

@echo Compiling version information
@echo Compiling version information >> ..\package.log
"%install_compiler%" create_version.nsi >> ..\package.log
.\GetVersion.exe >> ..\package.log

cd .. >> ..\package.log
cd >> package.log

if not exist .\Temp\Version.txt goto VERSIONERROR
goto COMPILE

:VERSIONERROR

@echo.
@echo ERROR: Unable to get product version.
@echo.

@echo ERROR: Unable to get product version.>> package.log

goto EXIT


rem ----- Build the install executables

:COMPILE

cd .\Temp >> package.log
cd >> ..\package.log

@echo Compiling conferencing.exe
@echo Compiling conferencing.exe >> ..\package.log
"%install_compiler%" zon.nsi >> ..\package.log

cd .. >> ..\package.log
cd >> package.log

:CHECK

if not exist .\Temp\conferencing.exe goto COMPILE_ERROR

if not exist ".\Core Files\signing.pfx" goto DONE

@echo Signing installer
bin\signtool.exe sign -i "comodo" .\Temp\conferencing.exe
bin\signtool.exe timestamp  /tr http://timestamp.comodoca.com /td sha256 .\Temp\conferencing.exe

goto DONE

:COMPILE_ERROR

@echo.
@echo ERROR: The installers were not built successfully. 
@echo        Check package.log for specific errors.
@echo.

@echo ERROR: One or more of the install exectuables are missing >> package.log
@echo        (conferencing.exe) >> package.log

goto EXIT

:DONE
copy .\temp\conferencing.exe .

@echo *** Installer has been generated successfully ***

:EXIT
pause
