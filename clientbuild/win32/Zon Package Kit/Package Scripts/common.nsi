;;
;; common.nsi
;;
;; Copyright (c) 2007 SiteScape, Inc.
;; 
;; Utilities that define the files copied on install, registry key settings, etc.
;; 
;;

;;--------------------------------
;; Set common installer attributes
;;--------------------------------

; Add version information
VIProductVersion "${Version}"
VIAddVersionKey "CompanyName" "Install"
VIAddVersionKey "FileVersion" "${Version}"
VIAddVersionKey "FileDescription" "Install"
VIAddVersionKey "InternalName" "Install"
VIAddVersionKey "LegalCopyright" "Install"
VIAddVersionKey "LegalTrademarks" "Install"
VIAddVersionKey "OriginalFilename" "Install"
VIAddVersionKey "ProductName" "Install"
VIAddVersionKey "ProductVersion" "${Version}"

!ifdef property_iicserver
    VIAddVersionKey "IICServer" "${property_iicserver}"
!else
    VIAddVersionKey "IICServer" "${default_iicserver}"
!endif

!ifdef property_meetinglookupurl
    VIAddVersionKey "LookupUrl" "${property_meetinglookupurl}"
!else
    VIAddVersionKey "LookupUrl" "${default_meetinglookupurl}"
!endif

!ifdef property_helppath
    VIAddVersionKey "HelpPath" "${property_helppath}"
!else
    VIAddVersionKey "HelpPath" "${default_helppath}"
!endif

;;--------------------------------
;; Installer Section
;;--------------------------------
Section "Client (required)"
  SectionIn RO
  
  ; Set output path to the installation directory.
  SetOutPath $INSTDIR

  ; Copy files
  Call CopyInstallFiles
  
  ; If firefox is installed, setup accept type for iic
  Call ConfigureFirefox
  
  ; Automatically configure host names in IICDef.opt
  Call ConfigureHostNames

  ; Write Registry Keys
  StrCmp $R9 "LIMITED" SKIP_REGISTRY
      Call WriteRegistryKeys
  SKIP_REGISTRY:

  ; Write out uninstaller
  WriteUninstaller "uninstall.exe"
SectionEnd

;;--------------------------------
;; Set install directory and shortcuts
;; Note: this is called automatically
;;--------------------------------
Section "Start Menu Shortcuts (Required)"
  SectionIn RO
  StrCmp $RunOnce "TRUE" SKIPSHORTCUT
      CreateDirectory "$SMPROGRAMS\${product_name}"
      CreateShortCut "$SMPROGRAMS\${product_name}\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\uninstall.exe" 0
      CreateShortCut "$SMPROGRAMS\${product_name}\${product_name}.lnk" "$INSTDIR\meeting.exe" "" "$INSTDIR\shortcut.ico" 0
  SKIPSHORTCUT:
SectionEnd

;;--------------------------------
;; Uninstaller Section
;;--------------------------------
UninstallText "This will uninstall ${product_name}. Hit Uninstall to continue."

Section "Uninstall"
  StrCmp $R9 "LIMITED" SKIP_UNREGISTRY
      Call un.DeleteRegistryKeys
  SKIP_UNREGISTRY:

  Call un.ConfigureFirefox
  Call un.DeleteInstallFiles
SectionEnd

;;---------------------------------
; GetWindowsVersion
;;---------------------------------
; Based on Yazno's function, http://yazno.tripod.com/powerpimpit/
; Updated by Joost Verburg
; Only return 'NT' for 'NT x.x' by Bin Liu
;
; Returns on top of stack
;
; Windows Version (95, 98, ME, NT x.x, 2000, XP, 2003)
; or
; '' (Unknown Windows Version)
;
; Usage:
;   Call GetWindowsVersion
;   Pop $R0
;   ; at this point $R0 is "NT 4.0" or whatnot

Function GetWindowsVersion
 
  Push $R0
  Push $R1
 
  ClearErrors
 
  ReadRegStr $R0 HKLM \
  "SOFTWARE\Microsoft\Windows NT\CurrentVersion" CurrentVersion

  IfErrors 0 lbl_winnt
   
  ; we are not NT
  ReadRegStr $R0 HKLM \
  "SOFTWARE\Microsoft\Windows\CurrentVersion" VersionNumber
 
  StrCpy $R1 $R0 1
  StrCmp $R1 '4' 0 lbl_error
 
  StrCpy $R1 $R0 3
 
  StrCmp $R1 '4.0' lbl_win32_95
  StrCmp $R1 '4.9' lbl_win32_ME lbl_win32_98
 
lbl_win32_95:
    StrCpy $R0 '95'
  Goto lbl_done
 
lbl_win32_98:
    StrCpy $R0 '98'
  Goto lbl_done
 
lbl_win32_ME:
    StrCpy $R0 'ME'
  Goto lbl_done
 
lbl_winnt:
 
  StrCpy $R1 $R0 1
 
  StrCmp $R1 '3' lbl_winnt_x
  StrCmp $R1 '4' lbl_winnt_x
 
  StrCpy $R1 $R0 3
 
  StrCmp $R1 '5.0' lbl_winnt_2000
  StrCmp $R1 '5.1' lbl_winnt_XP
  StrCmp $R1 '5.2' lbl_winnt_2003 lbl_error
 
lbl_winnt_x:
    StrCpy $R0 "NT"
  Goto lbl_done
 
lbl_winnt_2000:
    Strcpy $R0 '2000'
  Goto lbl_done
 
lbl_winnt_XP:
    Strcpy $R0 'XP'
  Goto lbl_done
 
lbl_winnt_2003:
    Strcpy $R0 '2003'
  Goto lbl_done
 
lbl_error:
    Strcpy $R0 ''
  lbl_done:
 
  Pop $R1
  Exch $R0
 
FunctionEnd

Function un.GetWindowsVersion
 
  Push $R0
  Push $R1
 
  ClearErrors
 
  ReadRegStr $R0 HKLM \
  "SOFTWARE\Microsoft\Windows NT\CurrentVersion" CurrentVersion

  IfErrors 0 lbl_winnt
   
  ; we are not NT
  ReadRegStr $R0 HKLM \
  "SOFTWARE\Microsoft\Windows\CurrentVersion" VersionNumber
 
  StrCpy $R1 $R0 1
  StrCmp $R1 '4' 0 lbl_error
 
  StrCpy $R1 $R0 3
 
  StrCmp $R1 '4.0' lbl_win32_95
  StrCmp $R1 '4.9' lbl_win32_ME lbl_win32_98
 
lbl_win32_95:
    StrCpy $R0 '95'
  Goto lbl_done
 
lbl_win32_98:
    StrCpy $R0 '98'
  Goto lbl_done
 
lbl_win32_ME:
    StrCpy $R0 'ME'
  Goto lbl_done
 
lbl_winnt:
 
  StrCpy $R1 $R0 1
 
  StrCmp $R1 '3' lbl_winnt_x
  StrCmp $R1 '4' lbl_winnt_x
 
  StrCpy $R1 $R0 3
 
  StrCmp $R1 '5.0' lbl_winnt_2000
  StrCmp $R1 '5.1' lbl_winnt_XP
  StrCmp $R1 '5.2' lbl_winnt_2003 lbl_error
 
lbl_winnt_x:
    StrCpy $R0 "NT"
  Goto lbl_done
 
lbl_winnt_2000:
    Strcpy $R0 '2000'
  Goto lbl_done
 
lbl_winnt_XP:
    Strcpy $R0 'XP'
  Goto lbl_done
 
lbl_winnt_2003:
    Strcpy $R0 '2003'
  Goto lbl_done
 
lbl_error:
    Strcpy $R0 ''
  lbl_done:
 
  Pop $R1
  Exch $R0
 
FunctionEnd

;;--------------------------------
;; Copies files to install directory
;;--------------------------------
Function CopyInstallFiles

  ;; simply copy exe's
  File "meeting.exe"
  File "meeting.exe.Manifest"
  File "mtgviewer.exe"
  File "mtgshare.exe"

  ;; these dll's do not hook and can be copied
  File "gnutls.dll"
  File "iiclib.dll"
  File "iaxlib.dll"
  File "iksemel.dll"
  File "libapr.dll"
  File "libgcrypt.dll"
  File "libgpg-error.dll"
  File "libiconv2.dll"
  File "libintl3.dll"
  File "libtasn1.dll"
  File "mingwm10.dll"
  File "mtghooks.dll"
  File "netlib.dll"
  File "sharelib.dll"
  File "wxmsw28ud_gcc.dll"
  File "xmlrpc.dll"
  File "zlib1.dll"
  File "json-c.dll"
  File "cryptlib.dll"
  File /nonfatal "crashlib.dll"
!ifdef include_g729
  File /nonfatal "g729a.dll"
!endif
;  File /nonfatal "IICSL*.dll"

  ;; these hook so need to be upgraded
  ;; the macro will update the dll based on version num and will do things like
  ;; force reboot if unable to copy over the dll
  !insertmacro UpgradeDLL "mtghooks.dll" "$INSTDIR\mtghooks.dll" "$INSTDIR"

  ; xrc files
  File "*.xrc"

  ; docshare import DLLs
  File /r "docshare"

  ; contact import DLLs
  File /r "connectivity"
  
  ; resources
  File /r "res"
  
  ; language support
  File /r "locale"
  
  ; icons
  File "*.ico"

  ; opt files
  File "*.opt"
  
  ; CA files
  File "*.pem"

  ; IIC Plugin ActiveX control
  SetOverwrite try
  File "IICPlugin2.ocx"
  SetOverwrite on
  RegDLL $INSTDIR\IICPlugin2.ocx

lbl_Done:

FunctionEnd


;;--------------------------------
;; Automatically configure host names in zon-def.opt
;;--------------------------------

Var InstallerName

Function ConfigureHostNames
  ; Get the name and location of the installer executable
  System::Call 'kernel32::GetModuleFileNameA(i 0, t .R0, i 1024) i r1' 
  strcpy $InstallerName $R0
  ;MessageBox MB_OK "Installer: $InstallerName"

  ; Get the IICServer Version property 
  FileProp::GetUserDefined "$InstallerName" "IICServer"
  Pop $1
  StrCpy $PropertyIICServer "$1"
  Push $PropertyIICServer
  Call TrimSpaces
  Pop $PropertyIICServer

  ; Get the MeetingLookupURL Version property 
  FileProp::GetUserDefined "$InstallerName" "LookupUrl"
  Pop $1
  StrCpy $PropertyMeetingLookupURL "$1"
  Push $PropertyMeetingLookupURL
  Call TrimSpaces
  Pop $PropertyMeetingLookupURL

  ; Get the HelpPath Version property 
  FileProp::GetUserDefined "$InstallerName" "HelpPath"
  Pop $1
  StrCpy $PropertyHelpPath "$1"
  Push $PropertyHelpPath
  Call TrimSpaces
  Pop $PropertyHelpPath

  ; Append the properties to the IICDef file  
  FileOpen $R0 ".\zon-def.opt" a
  FileSeek $R0 0 END
  FileWrite $R0 '"Server","$PropertyIICServer"'
  FileWrite $R0 '$\r$\n"MeetingLookupURL","$PropertyMeetingLookupURL"'
  FileWrite $R0 '$\r$\n"HelpPath","$PropertyHelpPath"'
  FileWrite $R0 '$\r$\n"ServerToken","${server_token}"'
  FileClose $R0
FunctionEnd

;;--------------------------------
;; Delete files from install directory
;;--------------------------------
Function un.DeleteInstallFiles
  ; remove files
  Delete $INSTDIR\meeting.exe
  Delete $INSTDIR\meeting.exe.Manifest
  Delete $INSTDIR\mtgviewer.exe
  Delete $INSTDIR\mtgshare.exe
  Delete $INSTDIR\mtghooks.dll
  Delete $INSTDIR\gnutls.dll
  Delete $INSTDIR\iiclib.dll
  Delete $INSTDIR\iaxlib.dll
  Delete $INSTDIR\iksemel.dll
  Delete $INSTDIR\libapr.dll
  Delete $INSTDIR\libgcrypt.dll
  Delete $INSTDIR\libgpg-error.dll
  Delete $INSTDIR\libiconv-2.dll
  Delete $INSTDIR\libintl-3.dll
  Delete $INSTDIR\libtasn1.dll
  Delete $INSTDIR\mingwm10.dll
  Delete $INSTDIR\mtghooks.dll
  Delete $INSTDIR\netlib.dll
  Delete $INSTDIR\sharelib.dll
  Delete $INSTDIR\wxmsw28ud_gcc.dll
  Delete $INSTDIR\xmlrpc.dll
  Delete $INSTDIR\libiconv2.dll
  Delete $INSTDIR\libintl3.dll
  Delete $INSTDIR\zlib1.dll
  Delete $INSTDIR\cryptlib.dll
  Delete $INSTDIR\json-c.dll
  Delete $INSTDIR\crashlib.dll
!ifdef include_g729
  Delete $INSTDIR\g729a.dll
!endif

  ; remove uninstaller
  Delete $INSTDIR\uninstall.exe

  ; xrc files
  Delete $INSTDIR\*.xrc

  ; docshare import dlls
  RMDir /r $INSTDIR\docshare

  ; contact import dlls
  RMDir /r $INSTDIR\connectivity

  ; resourecs
  RMDir /r $INSTDIR\res

  ; language support
  RMDir /r $INSTDIR\locale

  ; icons
  Delete $INSTDIR\*.ico

  ; .opt files
  Delete $INSTDIR\zon.opt
  Delete $INSTDIR\zon-def.opt

  ; log files
  Delete $INSTDIR\*.log
  Delete $INSTDIR\*.0
  Delete $INSTDIR\*.1
  Delete $INSTDIR\*.2

  ; iic files
  Delete $INSTDIR\*.iic
  
  ; CA files
  Delete $INSTDIR\*.pem

   ; Remove IIC Plugin ActiveX control
  UnRegDLL $INSTDIR\IICPlugin2.ocx
  Delete $INSTDIR\IICPlugin2.ocx
 
lbl_Done:

  ; remove shortcuts, if any
  Delete "$SMPROGRAMS\${product_name}\*.*"

  ; remove directories used
  RMDir "$SMPROGRAMS\${product_name}"
  RMDir "$INSTDIR"
FunctionEnd

;;--------------------------------
;; Writes registry keys
;;--------------------------------
Function WriteRegistryKeys
  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\${installer_registry_key} "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${product_name}" "DisplayName" "${product_name} (remove only)"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${product_name}" "UninstallString" '"$INSTDIR\uninstall.exe"'

  ; Write out Protocol handler registry keys
  WriteRegStr HKCR "iic" "" "URL:IIC Protocol"
  WriteRegStr HKCR "iic" "URL Protocol" ""
  WriteRegStr HKCR "iic\DefaultIcon" "" "meeting.exe"
  WriteRegStr HKCR "iic\shell\open\command" "" "$INSTDIR\meeting.exe %1"

  ;Write out MIME Type registry keys
  WriteRegStr HKCR ".iic" "" "zon_file"
  WriteRegStr HKCR "zon_file" "" "Instant Collaborator"
  WriteRegBin HKCR "zon_file" "EditFlags" "00000100"
  WriteRegStr HKCR "zon_file\shell\open\command" "" "$INSTDIR\meeting.exe /mime %1"
  WriteRegStr HKCR "MIME\Database\Content Type\application/iic" "" ""
  WriteRegStr HKCR "MIME\Database\Content Type\application/iic" "Extension" ".iic"
  
  ; Add IIC to IE's Accept/Type Header
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Accepted Documents" "iic" "application/iic-${server_token}"
FunctionEnd

;;--------------------------------
;; remove registry keys
;;--------------------------------
Function un.DeleteRegistryKeys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Zon"
  DeleteRegKey HKLM SOFTWARE\Zon_Client
  DeleteRegKey HKCR "iic"
  DeleteRegKey HKCR ".iic"
  DeleteRegKey HKCR "iic_file"
  DeleteRegKey HKCR "MIME\Database\Content Type\application/iic"
  DeleteRegValue HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Internet Settings\Accepted Documents" "iic"
FunctionEnd

;;--------------------------------
; TrimSpaces
; input, top of stack  (e.g. "whatever   ")
; output, top of stack (replaces, with e.g. "whatever")
; modifies no other variables.

 Function TrimSpaces
   Exch $R0
   Push $R1
   Push $R2
   StrCpy $R1 0
 
 loop:
   IntOp $R1 $R1 - 1
   StrCpy $R2 $R0 1 $R1
   StrCmp $R2 " " loop
   IntOp $R1 $R1 + 1
   IntCmp $R1 0 no_trim_needed
   StrCpy $R0 $R0 $R1
 
 no_trim_needed:
   Pop $R2
   Pop $R1
   Exch $R0
 FunctionEnd

;;--------------------------------
;; LaunchClient function - remove registry keys
;;--------------------------------

VAR cmdname
VAR strlen
VAR mid
VAR pid

;; include strtok routine
!include "strtok.nsi"

; Automatically launch IIC after install
Function LaunchClient

    ;; read system var $CMDLINE which contains the name of the installer as it's
    ;; actually invoked on the installing machine.  strategy is to parse
    ;; the name and look for a form <someprefix>/imidio.840.841.exe, where
    ;; 840 is the meeting id and 841 is the participant id.

;    MessageBox MB_OK "Cmd line: $CMDLINE"

    ;; remove containing quotes around cmd line
    StrLen $strlen $CMDLINE
    IntOp $strlen $strlen - 3
    StrCpy $cmdname $CMDLINE $strlen 1

;    MessageBox MB_OK "Cmd line (no quotes): $cmdname"

;;  remove the path portion of the name
;;  form will be something like "c:\temporary files\..\etc\imidio.840.841.exe"
PATH:
    Push $cmdname
    Push "\"    ; path delimeter
    Call StrTok
    Pop $R0     ; R0 contains path portion if any
    Pop $R1     ; R1 is remainder
;    MessageBox MB_OK "PATH parsing: $R0 and $R1"
    StrLen $strlen $R0
    IntCmp 0 $strlen CONTINUE
    StrCpy $cmdname $R1
    Goto PATH   ; more path to go

CONTINUE:

;;  parse the name into component parts
;;  name will be of form imidio.mtgid.partid.exe or imidio.exe
;;  if it contains mtgid and partid these are passed to IIC.exe
;;  at startup
    Push $cmdname
    Push "."
    Call StrTok
    Pop $R0 ; R0 now contains file name prefix (imidio)
    Pop $R1 ; R1 is now suffix

    ;; if suffix is .exe or .tmp, we're done (no contained info)
    StrCmp "exe" $R1 DONE
    StrCmp "tmp" $R1 DONE

    ;; get meeting id
    Push $R1 ; continue parsing the same string
    Push "."
    Call StrTok
    Pop $R0 ; R0 now contains meeting id
    Pop $R1 ; R1 is suffix

    StrCpy $mid $R0
    
    ;; if suffix is .exe or .tmp, we're done (not all params present)
    StrCmp "exe" $R1 DONE
    StrCmp "tmp" $R1 DONE

    ;; get participant id
    Push $R1 ; continue parsing the same string
    Push ".["
    Call StrTok
    Pop $R0 ; R0 now contains participant id
    Pop $R1 ; R1 is suffix

    StrCpy $pid $R0

;    MessageBox MB_OK "Mid: $mid"
;    MessageBox MB_OK "Pid: $pid"

    ;; verify mtg id and part id are valid size
    StrLen $strlen $mid
    IntCmp 0 $strlen DONE

    ;StrLen $strlen $pid
    ;IntCmp 0 $strlen DONE

    ;; make the call
    UAC::Exec '' '"$INSTDIR\meeting.exe"' '"iic:launch?mid=$mid&pid=$pid"' ''
    Goto REALLYDONE

DONE:
;    MessageBox MB_OK "Function End"
    ;; launch client with no args
    UAC::Exec '' '"$INSTDIR\meeting.exe"' '' ''

REALLYDONE:

FunctionEnd


;;--------------------------------
;; This macro is used to create two functions, one for install 
;; and one for uninstall.
;;
;; ConfigureFirefox
;;    Check registry for install location for the Mozilla Firefox browser
;;    If found, copy a js file to Firefox's preference folder to add 
;;    "application/iic-${server_token}" to the HTTP accept type
;;
;; un.ConfigureFirefox
;;    Check registry for install location for the Mozilla Firefox browser
;;    If found, delete the js file in Firefox's preference folder that adds 
;;    "application/iic-${server_token}" to the HTTP accept type
;;
;;--------------------------------

!macro FirefoxFunction un
Function ${un}ConfigureFirefox
   
   ; save the values of the variables 
   ; we will use in this function
   Push $4
   Push $3
   Push $2
   Push $1
   Push $0
   Push $R0

   ; Check for firefox's "Current Version" registry entry.
   ; If it's empty, assume firefox is not installed.
   ;
   StrCpy $0 "SOFTWARE\Mozilla\Mozilla Firefox"
   ReadRegStr $1  HKLM "$0" 'CurrentVersion'
   StrCmp $1 "" FIREFOX_NOT_INSTALLED
   
   ; Use "Current Version" info to look up the "Install Directory".
   ; If it's empy, firefox's registry entries have changed: abort.
   ;
   StrCpy $2 "$0\\$1\\Main"
   ReadRegStr $3 HKLM $2 'Install Directory'
   StrCmp $3 "" FIREFOX_NO_INSTALL_DIR

   ; finish build our preference file's path and name
   ;
   StrCpy $4 "$3\defaults\pref\zon.js"

   ; determine if this is an installer or an uninstaller
   ; and proceed accordingly
   StrCmp "${un}" "un." UNINSTALL
   
   ; Installer code
   ;
   ; Write our "zon.js" file into firefox's default preference directory
   ;
   FileOpen $R0 "$4" w
   FileWrite $R0 'pref("network.http.accept.default","text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,application/iic-${server_token},*/*;q=0.5");'
   FileClose $R0
   
   goto DONE

   UNINSTALL:
      ; Uninstaller code - remove our "zon.js" file from firefox's preference directory
      Delete $4
      goto DONE
      
   NO_SERVER_TOKEN:
      ;LogText "Firefox config error: server token not found"
      goto DONE
      
   FIREFOX_NOT_INSTALLED:
      ;LogText "Firefox not installed"
      goto DONE
   
   FIREFOX_NO_INSTALL_DIR:
      ;LogText "Unable to resolve Firefox install directory"
      goto DONE
   
   DONE:
   
   ; restore the values of the variables 
   ; we used in this function
   Pop $R0
   Pop $0
   Pop $1
   Pop $2
   Pop $3
   Pop $4
   
FunctionEnd
!macroend

!insertmacro FirefoxFunction ""
!insertmacro FirefoxFunction "un."

