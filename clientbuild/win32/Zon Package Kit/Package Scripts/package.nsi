;; package.nsi
;;
;; Copyright (c) 2005 SiteScape, Inc.
;; 
;; This script builds the Zon Install Package installer
;;

; The name of the installer
Name "Zon Package Kit"

; The file to write
OutFile "..\ZonPackageKit.exe"

; set compressor to best compression
SetCompressor lzma

; Show the Choose a location page
DirText "" "" "" ""
Page directory
Page instfiles

; The default installation directory
InstallDir "$PROGRAMFILES\Zon Package Kit"

; The text to prompt the user to enter a directory
ComponentText "This will install the Zon Package Kit onto your computer."

; Select desired styles for our dialog
XPStyle on
WindowIcon off
ShowInstDetails nevershow
SpaceTexts none

; automatically close window after install (no Close button)
AutoCloseWindow false

; text in window
BrandingText "Zon Package Kit Installer"

;;--------------------------------
;; Installer Section
;;--------------------------------
Section "Package Kit (required)"
  SectionIn RO
  
  ; Set output path to the installation directory.
  SetOutPath "$INSTDIR"

  ; Create directory structure
  CreateDirectory "$INSTDIR\Authenticode Certificate"
  CreateDirectory "$INSTDIR\Certificates"
  CreateDirectory "$INSTDIR\Bin"
  CreateDirectory "$INSTDIR\Bin\Include"
  CreateDirectory "$INSTDIR\Bin\Plugins"
  CreateDirectory "$INSTDIR\Branding"
  CreateDirectory "$INSTDIR\Branding\Default"
  ; CreateDirectory "$INSTDIR\Branding\Default-Lite"
  CreateDirectory "$INSTDIR\Branding\Custom"
  CreateDirectory "$INSTDIR\Core Files"
  CreateDirectory "$INSTDIR\Core Files\res"
  CreateDirectory "$INSTDIR\Package Scripts"
  ; CreateDirectory "$INSTDIR\Package Scripts Lite"
  CreateDirectory "$INSTDIR\Temp"
  CreateDirectory "$INSTDIR\Zon Install Package"
  ; CreateDirectory "$INSTDIR\Image Printer Driver"
  
  ; Copy files

  File "..\Package Kit.doc"
  File "..\package.bat"
  ; File "..\package-lite.bat"
  File "..\build-installer.bat"
  File "..\README.txt"

  SetOutPath "$INSTDIR\Certificates"
  File "..\Certificates\*.*"
  
  SetOutPath "$INSTDIR\Bin"
  File "..\Bin\*.*"

  SetOutPath "$INSTDIR\Bin\Include"
  File "..\Bin\Include\*.*"

  SetOutPath "$INSTDIR\Bin\Plugins"
  File "..\Bin\Plugins\*.*"

  SetOutPath "$INSTDIR\Branding\Default"
  File "..\Branding\Default\*.*"

  SetOutPath "$INSTDIR\Branding\Custom"
  File "..\Branding\Custom\*.*"

  SetOutPath "$INSTDIR\Core Files"
  File "..\Core Files\*.*"

  SetOutPath "$INSTDIR\Core Files\res"
  File "..\Core Files\res\*.*"
  
  SetOutPath "$INSTDIR\Package Scripts"
  File "..\Package Scripts\*.*"

  ; SetOutPath "$INSTDIR\Image Printer Driver"
  ; File "..\Image Printer Driver\*.*"
  
  ; Copy files for zon-lite
  
  ; SetOutPath "$INSTDIR\Package Scripts Lite"
  ; File "..\Package Scripts Lite\*.*"

  ; SetOutPath "$INSTDIR\Branding\Default-Lite"
  ; File "..\Branding\Default-Lite\*.*"
   
SectionEnd


; Automatically launch IIC after install
Function .onInstSuccess
    Exec "notepad $INSTDIR\README.txt"
FunctionEnd

