;; create_version.nsi
;;

OutFile "GetVersion.exe"

!define File ".\meeting.exe"

Function .onInit
 GetDllVersion "${File}" $R0 $R1
    IntOp $R2 $R0 / 0x00010000
    IntOp $R3 $R0 & 0x0000FFFF
    IntOp $R4 $R1 / 0x00010000
    IntOp $R5 $R1 & 0x0000FFFF
 StrCpy $R1 "$R2.$R3.$R4.$R5"

 FileOpen $R0 ".\Version.txt" w
 FileWrite $R0 '!echo "Product version: $R1"$\r$\n!define Version "$R1"'
 FileClose $R0

 StrCpy $R1 "$R2_$R3_$R4_$R5"
 
 FileOpen $R0 ".\TarVersion.bat" w
 FileWrite $R0 'ren installers.tar.gz installers_$R1.tar.gz'
 FileClose $R0
 
 Abort
FunctionEnd

Section
SectionEnd