@echo off
rem -------------------------------------------------------------------------
rem 
rem  package-installer.bat
rem 
rem  Package the Zon installer kit
rem
rem  Copyright (c) 2005 SiteScape, Inc.
rem
rem -------------------------------------------------------------------------

set VERSION=%1
set BUILDNUM=%2
set BRAND=%3

set MINGW=\mingw\bin

set ARCHIVE=ZonPackageKit-%VERSION%-%BUILDNUM%.zip
set OUT="Zon Package Kit\Core Files"
set SRC="..\..\iic-client"

xcopy /Y "..\..\iic-client\IICPlugin\Unicode Release Compatible\IICPlugin2.ocx" %OUT%\
xcopy /Y %SRC%\meeting\*.xrc %OUT%\
xcopy /Y %SRC%\meeting\shortcut.ico %OUT%\
xcopy /Y %SRC%\meeting\res\*.* %OUT%\res\
xcopy /Y %SRC%\bin\m*.exe %OUT%\
xcopy /Y %SRC%\bin\*.Manifest %OUT%\
xcopy /Y %SRC%\bin\*.dll %OUT%\
xcopy /Y %SRC%\bin\docshare\ms_ppt.dll %OUT%\docshare\
xcopy /Y %SRC%\bin\connectivity\ms_outlook.dll %OUT%\connectivity\
xcopy /Y %SRC%\bin\connectivity\OutlookImport.csv %OUT%\connectivity\
copy %SRC%\bin\zon-def.opt %OUT%\zon-def.opt
xcopy /E /Y %SRC%\meeting\locale\*.* %OUT%\locale\
copy \mingw\bin\libiconv2.dll %OUT%
copy \mingw\bin\libintl3.dll %OUT%
copy \mingw\bin\mingwm10.dll %OUT%
copy \mingw\bin\zlib1.dll %OUT%

if .%BRAND%==. goto NOBRAND
xcopy /E /Y branding\%BRAND%\*.* %OUT%
copy branding\%BRAND%\shortcut.ico %OUT%\res\zon.ico

if exist branding\%BRAND%\signing.pfx goto NOBRAND
del %OUT%\signing.pfx

:NOBRAND

PUSHD %OUT%

%MINGW%\strip xmlrpc.dll
%MINGW%\strip sharelib.dll
%MINGW%\strip netlib.dll
%MINGW%\strip libtasn1.dll
%MINGW%\strip libgpg-error.dll
%MINGW%\strip libgcrypt.dll
%MINGW%\strip libapr.dll
%MINGW%\strip iksemel.dll
%MINGW%\strip iiclib.dll
%MINGW%\strip gnutls.dll
%MINGW%\strip iaxlib.dll
%MINGW%\strip json-c.dll
%MINGW%\strip cryptlib.dll
%MINGW%\strip wx*.dll
%MINGW%\strip meeting.exe
%MINGW%\strip docshare\ms_ppt.dll

POPD

zip -r "%ARCHIVE%" -@ <files.lst
