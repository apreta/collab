@echo off

rem Store primary executables as built to preserve symbols

set VERSION=%1
set BUILDNUM=%2
set BRAND=%3

set WORKDIR=%CD%
set ARCHIVE=%WORKDIR%\build-%BRAND%-%VERSION%-%BUILDNUM%.zip

set SRC="..\.."

PUSHD %SRC%

%WORKDIR%\zip %ARCHIVE% iic-client\bin\meeting.exe iic-client\bin\wx*dll iic-client\meeting\meeting.map

POPD
