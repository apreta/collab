#!/bin/bash

# Installing and building wxWidets (wxMac-2.8.7):
# Starting from the wxMac-2.8.7 folder:
# mkdir mac-osx
# cd mac-osx
# ../configure --prefix=/usr/local \
# 	     --enable-monolithic \
# 	     --enable-unicode \
# 	     --enable-official_build \
# 	     --with-png=builtin \
# 	     --with-jpeg=builtin \
# 	     --with-tiff=builtin \
# 	     --with-expat=builtin \
# 	     --with-macosx-sdk=/Devloper/SDKs/MacOSX10.4u.sdk

# make
# sudo make install

# NOTE: I opted against building universal binaries. A build will need to be run on a Mac PPC system as well as a Mac Intel system.
# Building universal binaries implies that ALL of the various support libraries would also need to be build as universal binaries.
# This would make building the conferencing client from source on a new Mac system very difficult.

WXMAC=wxMac

SDK_VER="10.6"
# SDK="/Applications/Developer/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.6.sdk"
SDk="/Developer/SDKs"

cd ../../$WXMAC

mkdir macosx
cd macosx

# Configure wxWidgets / wxMac for use with Apreta
arch_flags="-arch i386"
../configure CFLAGS="$arch_flags" CXXFLAGS="$arch_flags" CPPFLAGS="$arch_flags" \
            LDFLAGS="$arch_flags" OBJCFLAGS="$arch_flags" OBJCXXFLAGS="$arch_flags" \
            --prefix=/usr/local \
            --enable-debug \
            --enable-monolithic \
            --enable-unicode \
            --enable-std_string \            
            --enable-official_build \
            --with-png=builtin \
            --with-jpeg=builtin \
            --with-tiff=builtin \
            --with-expat=builtin \
            --with-zlib \
            --with-macosx-sdk=$SDK \
            --with-macosx-version-min=$SDK_VER
            
make
sudo make install
