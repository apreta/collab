#!/bin/bash

WXGTK=wxGTK-2.8.6
BUILD=release
PREFIX=/opt/codeblocks

if [ "$BUILD" == "debug" ]; then
    conf_option="--enable-debug"
fi

# Build unicode release version of wxWGTK

#echo "Building wxWidgets"

#cd ../../$WXGTK

#if [ ! -e bld$BUILD ]; then
#    mkdir bld$BUILD
#fi

#cd bld$BUILD
#../configure  $conf_option --with-gtk --enable-monolithic --enable-unicode --prefix=$PREFIX
#if [ ! $? ]; then
#	exit -1;
#fi

#make || exit -1
#sudo make install

echo "Building codeblocks"

cd ~/tools/codeblocks

export ACLOCAL_FLAGS="-I $PREFIX/share/aclocal"

if [ ! -f configure ]; then
	./bootstrap
fi

./configure --with-contrib-plugins=all --enable-contrib --with-wx-config=$PREFIX/bin/wx-config --prefix=$PREFIX
if [ ! $? ]; then
	exit -1;
fi

make || exit -1
sudo make install
