#!/bin/bash

VERSION=$1
BUILD_NUM=$2
BRAND=$3
OPTION=$4
HOST=conf.imidio.com
PORTAL=conf.imidio.com

if [ $# -lt 2 ]; then
	echo "Usage: ./build-release.sh <version> <buildnum>"
	echo "Version is #.#.#, buildnum is single number"
	exit -1
fi

echo "Building binaries"

./build-client.sh $VERSION $BUILD_NUM $OPTION || exit -1

echo "Creating package"

./package-client.sh $VERSION $BUILD_NUM $BRAND || exit -1

echo "Creating disk image"

./build-dmg.sh $VERSION $BUILD_NUM $BRAND || exit -1

echo "Done."
