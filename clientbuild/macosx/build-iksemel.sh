#!/bin/bash

. build-common.sh

pushd ../../iksemel >/dev/null

if [ "$1" == "--reconf" ]; then
	autoreconf
fi

if [ ! "$1" == "--make" ]; then
	./configure --prefix=$prefix
fi

make || exit -1

sudo make install || exit -1

popd >/dev/null
