#!/bin/bash

. build-common.sh

pushd ../../apr >/dev/null

if [ ! "$1" == "--make" ]; then
	./configure --enable-threads --prefix=$prefix
fi

make || exit -1

sudo make install || exit -1
pushd $prefix/bin
sudo cp apr-1-config apr-config
popd

popd >/dev/null
