#!/bin/bash

. build-common.sh

pushd ../../iaxclient2 >/dev/null

./configure --prefix=$prefix --disable-clients --disable-video || exit 1

make clean

make || exit -1

sudo make install || exit 1

popd >/dev/null
