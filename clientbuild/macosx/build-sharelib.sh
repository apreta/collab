#!/bin/bash

. build-common.sh

pushd ../../iic/clients/sharelib >/dev/null

bakefile -f autoconf sharelib.bkl
./configure --prefix=$prefix || exit -1

make || exit -1

sudo make install || exit -1

pushd proctools>/dev/null
bsdmake .MAIN PREFIX=$prefix install
sudo bsdmake .MAIN PREFIX=$prefix install
cp ./pgrep/pgrep ../../bin
cp ./pkill/pkill ../../bin
cp ./pfind/pfind ../../bin
popd >/dev/null

popd >/dev/null
