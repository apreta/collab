#!/bin/bash

. build-common.sh

pushd ../../iic/clients/meeting >/dev/null

bakefile -f autoconf meeting.bkl
./configure --prefix=$prefix || exit -1

make || exit -1

sudo make install || exit -1
./meeting_post_build.sh

popd >/dev/null
