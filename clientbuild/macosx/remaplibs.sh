#!/bin/bash
# Copyright 2007 David Johnson
# The author grants unlimited permission to
# copy, distribute and modify this script
#
# Modified for KaBlink by Paul Fossey , Sep 2008
#    Handle remapping library references for Next bundles and console apps.
#

if [ "" = "$1" ]
then
    echo "Missing application name parameter"
    echo "Usage: remaplibs.sh appname"
    echo "Where:"
    echo "   appname is either the name of a Next Bundle or console app."
    exit
fi

APPNAME=$1
BINNAME=$2
COMMON_LIBRARY=Library

if [ "" = "$3" ] ; then
    BUILDDMG=N
else
    if [ "-dmg" = "$3" ] ; then
	BUILDDMG=Y
    fi
fi

# Check to see if APPNAME references a bundle
BUNDLE=$APPNAME.app
APPBIN="$BUNDLE/Contents/MacOS/$BINNAME"
RELPATH="@executable_path/../Frameworks"
COMMON_LIBRARY=./$BUNDLE/Contents/Frameworks
if [ ! -d $BUNDLE ]
then
    echo "\"$APPNAME\" is not a bundle."
    if [ -x $APPNAME ]
    then
        echo "\"$APPNAME\" is a console app."
        APPBIN=$APPNAME
	RELPATH="@executable_path/Library"
	COMMON_LIBRARY=./Library
	BUNDLE="N"
    else
	echo "ERROR: \"$APPNAME\" is neither a console app or a bundle."
	exit
    fi
else
    if [ ! -f $APPBIN ]
    then
	echo "ERROR: cannot find application binary in the bundle. Did you forget to run make?"
	exit
    fi
fi

echo "Preparing application for distribution:"
echo "application: $APPNAME"
echo "bundle:      $BUNDLE"
echo "binary:      $APPBIN"
echo "relpath:     $RELPATH"
echo "library:     $COMMON_LIBRARY"

# echo debug exit
# exit 

### Make sure there is a common library folder ############
mkdir -p $COMMON_LIBRARY

### query binary for frameworks #####################################

# Create a list of the .dylib libraries that are linked to the application.
# each line of the file looks like this: name libname (blah blah blah)
if [ -f pkfdeploy.list ] ; then
    rm pkfdeploy.list
fi
otool -l $APPBIN | grep "dylib">>pkfdeploy.list
while read aline; do
   # "name libpath (blah bla)" becomes "libpath (blah blah)"
   libpath=${aline##name }
   # "libpath (blah blah)" becomes "libpath"
   libpath=${libpath%% (*}

   #strip off the path to get the name alone
   libname=${libpath##/*/}

   copyfrom=$libpath
   if [ $libpath = $libname ] ; then
       copyfrom=$QTDIR/lib/$libname
   fi

   # Ignore /usr/lib and /System/Library entries
   usr_lib=${libpath:0:8}
   #echo usr_lib $usr_lib
   system_library=${libpath:0:15}
   #echo system_library $system_library

   # Process this entry if it's not assumed to be part of the standard Mac OS X installation.
   if [ $usr_lib = "/usr/lib" ] || [ $system_library = "/System/Library" ] ; then
       echo .
   else
       echo -----------------------
       echo Library path:    $libpath
       echo Library name:    $libname

       #######################################################
       echo "Copy $libname to the bundle Frameworks folder..."
       cp -fH $copyfrom $COMMON_LIBRARY
       chmod +wx $COMMON_LIBRARY/$libname

       ##################################################
       echo "Stripping debug information..."
       # strip libs (-x is max allowable for shared libs'
       strip -x $COMMON_LIBRARY/$libname

       #################################################
       echo install_name_tool -id $RELPATH/$libname $BUNDLE/Contents/Frameworks/$libname 
       install_name_tool -id $RELPATH/$libname $BUNDLE/Contents/Frameworks/$libname

       ###################################################
       echo install_name_tool -change $libpath $RELPATH/$libname $APPBIN
       install_name_tool      -change $libpath $RELPATH/$libname $APPBIN

       ################################################################
       # Now we need to remap the library references for this library
       echo Changing the location of bundled libraries ...

       if [ "$BUNDLE" = "N" ]
       then
	   libbin=./Library/$libname
       else
	   libbin=$BUNDLE/Contents/Frameworks/$libname
       fi

       if [ -f pkflibdeploy.list ] ; then
	   rm pkflibdeploy.list
       fi
       echo Searching libbin: $libbin
       otool -L $libbin | grep "dylib">>pkflibdeploy.list
       while read aline1; do
           # "name libpath (blah bla)" becomes "libpath (blah blah)"
	   libpath=${aline1##name }
           # "libpath (blah blah)" becomes "libpath"
	   libpath=${libpath%% (*}

           #strip off the path to get the name alone
	   libname=${libpath##/*/}

           # Ignore /usr/lib and /System/Library entries
	   usr_lib=${libpath:0:8}
           #echo usr_lib $usr_lib
	   system_library=${libpath:0:15}
           #echo system_library $system_library

           # Process this entry if it's not assumed to be part of the standard Mac OS X installation.
           if [ $usr_lib = "/usr/lib" ] || [ $system_library = "/System/Library" ] ; then
               echo .
           else
	       ##################################################################
	       echo install_name_tool -change $libbin $RELPATH/$libname $libbin
	       install_name_tool -change $libpath $RELPATH/$libname $libbin
	   fi

       # end of inner loop
       done < "pkflibdeploy.list" 
   fi

# end of outer while loop 
done < "pkfdeploy.list"

if [ "$BUILDDMG" = "Y" ] ; then

    echo "Building disk image file for $APPNAME"

    imagedir="/tmp/$APPNAME.$$"
    mkdir $imagedir
    cp -R $BUNDLE $imagedir

    # TODO: copy over additional files, if any
    hdiutil create -ov -srcfolder $imagedir -format UDBZ -volname "$APPNAME" "$APPNAME.dmg"
    hdiutil internet-enable -yes "$APPNAME.dmg"
    rm -rf $imagedir
    ls -l "$APPNAME.dmg"
fi

echo "Done"

exit


