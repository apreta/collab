#!/bin/sh

# This script packages the Apreta client for distribution on Mac OS X.
# Currently this bundle will only support Intel Macs.
#

NAME=Apreta
APPNAME=Conferencing
SRC_DIR=`pwd`/../../iic-client/meeting
BIN_DIR=`pwd`/../../iic-client/bin

VERSION=$1
BUILD_NUM=$2
BRAND=$3

if [ "" == "$VERSION" ]
then
    VERSION="1.0"
    echo "Missing version parameter, defaulting to version $VERSION"
fi

if [ "" == "$BUILD_NUM" ]
then
    BUILD_NUM="1"
    echo "Missing build number parameter, defaulting to build number $BUILD_NUM"
fi

if [ "" == "$BRAND" ]
then
    echo "Missing brand parameter. Using Defaults."
    BRAND_DIR=`pwd`/branding/missing
else
	BRAND_DIR=`pwd`/branding/$BRAND
fi

if [ -f branding/$BRAND/brand.sh ]; then
    . branding/$BRAND/brand.sh
fi

if [ -d $NAME ]
then
    rm -rf $NAME
fi
mkdir $NAME
pushd $NAME

mkdir $APPNAME.app
mkdir $APPNAME.app/Contents
mkdir $APPNAME.app/Contents/MacOS
mkdir $APPNAME.app/Contents/MacOS/docshare
mkdir $APPNAME.app/Contents/Resources
mkdir $APPNAME.app/Contents/Resources/res
mkdir $APPNAME.app/Contents/Resources/locale
mkdir $APPNAME.app/Contents/SharedSupport

echo Loadiong package desc into the bundle
cp $SRC_DIR/Info.plist $APPNAME.app/Contents
cp $SRC_DIR/PkgInfo $APPNAME.app/Contents
if [ -f $BRAND_DIR/Info.plist ]; then
	cp $BRAND_DIR/Info.plist $APPNAME.app/Contents
fi
sed -i -e s/@@VER@@/$VERSION/g "$APPNAME.app/Contents/Info.plist"
sed -i -e s/@@BUILD@@/$BUILD_NUM/g "$APPNAME.app/Contents/Info.plist"

echo Loading the meeting executables into the bundle.
cp $BIN_DIR/meeting $APPNAME.app/Contents/MacOS
cp $BIN_DIR/mtgviewer $APPNAME.app/Contents/MacOS
cp $BIN_DIR/mtgshare-10.5 $APPNAME.app/Contents/MacOS
cp $BIN_DIR/mtgshare $APPNAME.app/Contents/MacOS
cp $BIN_DIR/docshare/mac_pdf.dylib $APPNAME.app/Contents/MacOS/docshare
#strip -S $APPNAME.app/Contents/MacOS/*

cp $BIN_DIR/SharedSupport/*.sh $APPNAME.app/Contents/SharedSupport

echo Loading default options file into app bundle Contents/SharedSupport folder.
cp $BIN_DIR/zon-def.opt $APPNAME.app/Contents/SharedSupport
if [ -f $BRAND_DIR/zon-def.opt ]; then
	cp $BRAND_DIR/zon-def.opt $APPNAME.app/Contents/SharedSupport
fi

echo Loading the x509 security file into app bundle Contents/SharedSupport folder.
cp $SRC_DIR/serverCA.pem $APPNAME.app/Contents/SharedSupport

echo Loading the app bundle icon
cp $SRC_DIR/res/zon.icns $APPNAME.app/Contents/Resources
if [ -f $BRAND_DIR/res/zon.icns ]; then
	cp $BRAND_DIR/res/zon.icns $APPNAME.app/Contents/Resources
fi

echo Loading gui defs
cp $SRC_DIR/*.xrc $APPNAME.app/Contents/Resources
if [ -d $BRAND_DIR ]; then
	cp $BRAND_DIR/*.xrc $APPNAME.app/Contents/Resources
fi

echo Loading pixmaps
cp $SRC_DIR/res/* $APPNAME.app/Contents/Resources/res
if [ -d $BRAND_DIR/res ]; then
	cp $BRAND_DIR/res/* $APPNAME.app/Contents/Resources/res
fi

echo Loading translations
for file in $SRC_DIR/locale/*; do
	if [ -d $file ]; then
		file=${file##*/locale/}
		dest=$APPNAME.app/Contents/Resources/locale/$file
		mkdir $dest
		cp $SRC_DIR/locale/$file/*.mo $dest
		cp $SRC_DIR/locale/$file/*.html $dest		
	fi
done

echo Copying branded translations
for file in $BRAND_DIR/locale/*; do
	if [ -d $file ]; then
		file=${file##*/locale/}
		dest=$APPNAME.app/Contents/Resources/locale/$file
		mkdir $dest
		cp $BRAND_DIR/locale/$file/*.mo $dest
		cp $BRAND_DIR/locale/$file/*.html $dest		
	fi
done

echo Done

echo "Meeting Contents:"
ls -l $APPNAME.app/Contents

echo "Meeting app:"
ls -l $APPNAME.app/Contents/MacOS
echo " "

echo "Bundle Shared Support:"
ls -l $APPNAME.app/Contents/SharedSupport
echo " "

echo "Bundle resources:"
ls -l $APPNAME.app/Contents/Resources
echo " "

echo Remapping library references for $APPNAME.app
#( cd Apreta; ../remaplibs.sh meeting meeting )
dylibbundler -od -b -x ./$APPNAME.app/Contents/MacOS/meeting -d ./$APPNAME.app/Contents/libs/
dylibbundler -x ./$APPNAME.app/Contents/MacOS/docshare/mac_pdf.dylib -d ./$APPNAME.app/Contents/libs/
#strip -S ./$APPNAME.app/Contents/libs/*dylib

echo Remapping library references for mtgviewer
#( cd Apreta; ../remaplibs.sh meeting mtgviewer )
dylibbundler -od -of -b -x ./$APPNAME.app/Contents/MacOS/mtgviewer -d ./$APPNAME.app/Contents/viewer-libs/ -p @executable_path/../viewer-libs/
#strip -S ./$APPNAME.app/Contents/viewer-libs/*dylib

echo Remapping library references for mtgshare
#( cd Apreta; ../remaplibs.sh mtgshare )
dylibbundler -od -of -b -x ./$APPNAME.app/Contents/MacOS/mtgshare-10.5 -i /usr/X11/lib -d ./$APPNAME.app/Contents/share-libs/ -p @executable_path/../share-libs/
#strip -S ./$APPNAME.app/Contents/share-libs/*dylib
dylibbundler -od -of -b -x ./$APPNAME.app/Contents/MacOS/mtgshare -i /usr/X11/lib -d ./$APPNAME.app/Contents/share-libs/ -p @executable_path/../share-libs/

if [ -n "$SIGNCERT" ]; then
	codesign -s "$SIGNCERT" -f --deep $APPNAME.app
fi

popd

exit

