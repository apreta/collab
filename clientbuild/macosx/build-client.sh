#!/bin/sh

# Builds all client binaries and installs into build area
# Also updates binaries in source tree (iic-client/bin)

# To change /usr/include, /usr/lib, etc.
# "-isysroot /Developer/SDKs/MacOSX10.5.sdk -mmacosx_version_min=10.5"
# (should we use -sysroot instead?)

VERSION=$1
BUILD_NUM=$2
OPTIONS=

CURL_VER="7.21.2"
CURL_DIR="curl-$CURL_VER"

WX_VER="2.8.11"
WX_DIR="wxMac-$WX_VER"

JPEG_DIR="jpeg-8b"

# -- No 10.6 SDK in new Xcode, and wx 2.8 doesn't build with newer SDKs
# SDK_ROOT="/Applications/Developer/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs"
SDK_ROOT="/Xcode4/SDKs"

if ! echo $PATH | grep "^/usr/local/bin"; then 
  export PATH=/usr/local/bin:$PATH
  gcc --version
fi

#PREFIX=/usr/local
PREFIX=/opt/conferencing

# modify path so our wxWidgets is picked up first
export PATH=$PREFIX/bin:$PATH
export PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig/:$PKG_CONFIG_PATH

export CC=gcc-4.2
export CXX=g++-4.2


function set_build_env {
	osx_sdk=$1
		
	arch_flags="-arch i386"
	gcc_flags="-g -isysroot $SDK_ROOT/MacOSX${osx_sdk}.sdk -mmacosx-version-min=${osx_sdk} -headerpad_max_install_names"

	export MACOSX_DEPLOYMENT_TARGET="10.6"
	export CFLAGS="$arch_flags $gcc_flags"
	export CXXFLAGS="$arch_flags $gcc_flags"
	export CPPFLAGS="$arch_flags $gcc_flags"
	export LDFLAGS="$arch_flags $gcc_flags"
	export OBJCFLAGS="$arch_flags"
	export OBJCXXFLAGS="$arch_flags"
}

function rebuild_configure {
    bakefilize --copy
    bakefile -f autoconf $1.bkl
    aclocal -I $PREFIX/share/aclocal
    autoreconf -fi
}

function check_env {
	if [ ! -d $WX_DIR ]; then
		echo "wxWidgets source not found, please place in wxMac"
		return -1
	fi 
	if [ ! -e wxMac ]; then
		ln -s $WX_DIR wxMac 
	fi
	if [ ! -d $CURL_DIR ]; then
		echo "libcurl source not found, please place in $CURL_DIR"
		return -1
	fi 
	if [ ! -d $JPEG_DIR ]; then
		echo "libjpeg source not found, please place in $JPEG_DIR"
		return -1
	fi 
	if [ ! -d gnutls-libs/gnutls-2.8.6 ]; then
		echo "gnutls source not found, please place in gnutls/gnutls-2.8.6"
		return -1
	fi

	return 0
}

set_build_env "10.6"

if [ "$1" == "--setenv" ]; then
	exit 0
fi

pushd ../.. >/dev/null

shift 2

until [ -z "$1" ]
  do
    case $1 in
     --make)  MAKE="y" ;;
     --skipwx) SKIPWX="y" ;;
     --skipggi) SKIPGGI="y" ;;
     *) echo "Unknown option $1"; exit -1 ;;
    esac
    shift
  done  
 
check_env || exit -1

if [ -z "$MAKE" ]; then 
	echo "Scrubbing IIC (warning: doesn't clean third party libs)"
	( cd iic-client; make clean )
	( cd x11vnc; make clean )
	( cd ggivnc; make clean )
else
    echo "Make mode"
fi

#
# Build
#

echo "Updating version..."
if [ ! -z "$VERSION" ]; then
( cd iic-client/meeting; \
  sed -i bck -e "s/VERSION ".*"/VERSION \"$VERSION\"/" \
         -e "s/BUILD_NUM ".*"/BUILD_NUM \"$BUILD_NUM\"/" version.h )
fi

echo "Building wx..."
( cd wxMac
  if [ -z "$SKIPWX" ]; then
    if [ -z "$MAKE" ]; then
      mkdir macosx
      cd macosx
      ../configure --prefix=$PREFIX \
            --enable-debug \
            --enable-monolithic \
            --enable-unicode \
            --enable-std_string \
            --enable-official_build \
            --with-png=builtin \
            --with-jpeg=builtin \
            --with-tiff=builtin \
            --with-expat=builtin \
            --with-zlib \
            --with-macosx-sdk=$SDK_ROOT/MacOSX10.6.sdk \
            --with-macosx-version-min=10.6
    else
      cd macosx
    fi
    make || exit -1
    make install
  fi
)

echo "Build apr..."
( cd apr
  if [ -z "$MAKE" ]; then
    ./configure --without-sendfile --enable-threads --prefix=$PREFIX --enable-debug || exit -1
  fi
  make || exit -1
  make install
)

echo "Build libcurl..."
( cd curl-$CURL_VER
  if [ -z "$MAKE" ]; then
    ./configure --prefix=$PREFIX --enable-debug || exit -1
  fi
  make || exit -1
  make install
)

echo "Build libjpeg…"
( cd $JPEG_DIR
  if [ -z "$MAKE" ]; then
    ./configure --prefix=$PREFIX || exit -1
  fi
  make || exit -1
  make install
)

echo "Build gnutls..."
( cd gnutls-libs
  if [ -z "$MAKE" ]; then
    ./build.sh || exit -1
  else
    ./build.sh --make || exit -1
  fi
)

echo "Building cryptlib..."
( cd iic-client/cryptlib
  if [ -z "$MAKE" ]; then
    rebuild_configure cryptlib
    ./configure --prefix=$PREFIX
  fi
  make || exit -1
  make install
)

echo "Building json-c..."
(
 cd json-c;
 if [ -z "$MAKE" ]; then
   ./configure --prefix=$PREFIX
 fi
 make || exit -1
 make install
)

echo "Building netlib..."
( cd iic-client/netlib;
  if [ -z "$MAKE" ]; then
    rebuild_configure netlib
    ./configure --prefix=$PREFIX
  fi
  make || exit -1 
  make install
)

echo "Building xmlrpc..."
( cd xmlrpc2
  if [ -z "$MAKE" ]; then
    export CADD="$CFLAGS" 
    export LADD="$LDFLAGS"
    ./configure --disable-libwww-client --disable-cplusplus --disable-abyss-server --prefix=$PREFIX
  fi
  make || exit -1
  make install
)

echo "Building iksemel..."
( cd iksemel
  if [ -z "$MAKE" ]; then
    autoreconf --force --install
    ./configure --prefix=$PREFIX || exit -1
  fi
  make || exit -1
  make install
)

echo "Building portaudio..."
( cd portaudio
  if [ -z "$MAKE" ]; then
  	autoconf
    ./configure --prefix=$PREFIX
  fi
  make || exit -1
  make install
)

echo "Building speex..."
( cd speex
   if [ -z "$MAKE" ]; then
      ./configure --prefix=$PREFIX --disable-ogg
    fi
    make || exit -1
    make install
)

echo "Building iaxclient..."
( cd iaxclient2
  if [ -z "$MAKE" ]; then
    autoreconf -fi
    CFLAGS="$CFLAGS -I `pwd`/lib/gsm/inc" ./configure --prefix=$PREFIX --enable-local-gsm --disable-clients  \
    	--disable-video --with-ffmpeg=no --with-theora=no --with-ogg=no || exit -1
  fi
  make || exit -1 
  make install
)

echo "Building iiclib..."
( cd iic-client/iiclib
  if [ -z "$MAKE" ]; then
    rebuild_configure iiclib
    ./configure --prefix=$PREFIX
  fi
  make || exit -1 
  make install
)

echo "Building iaxlib..."
( cd iic-client/iaxlib
  if [ -z "$MAKE" ]; then
    rebuild_configure iaxlib
    ./configure --prefix=$PREFIX
  fi
  make || exit -1 
  make install
)

echo "Building sharelib..."
( cd iic-client/sharelib
  if [ -z "$MAKE" ]; then
    rebuild_configure sharelib
    ./configure --prefix=$PREFIX
  fi
  make || exit -1 
  make install
)

echo "Building meeting..."
( cd iic-client/meeting
  if [ -z "$MAKE" ]; then
   rebuild_configure meeting
   ./configure --prefix=$PREFIX
  fi 
  make || exit -1 
  ./meeting_post_build.sh
)

echo "Building pdf plugin..."
( cd iic-client/docshare/mac_pdf
  /Developer/usr/bin/xcodebuild -target mac_pdf -configuration Release || exit -1
)

echo "Building vnc components..."
( cd ggi-libs
  if [ -z "$SKIPGGI" ]; then
    if [ -z "$MAKE" ]; then
      ./build.sh || exit -1
    else
      ./build.sh --make || exit -1
    fi
  fi
)

( cd ggivnc;
  if [ -z "$MAKE" ]; then
    autoconf -fi
    ./configure --enable-debug --prefix=$PREFIX
  fi
  make || exit -1
  cp ggivnc ../iic-client/bin/mtgviewer
)

#( cd x11vnc; 
#  if [ -z "$MAKE" ]; then
#  	autoreconf -fi
#    ./configure --enable-debug --prefix=$PREFIX --with-jpeg=$PREFIX --without-x
#  fi
#  make || exit -1
#  cp x11vnc/x11vnc ../iic-client/bin/mtgshare-10.5
#)

( cd x11vnc; 
  make clean
  set_build_env "10.6" 
  if [ -z "$MAKE" ]; then
  	autoreconf -fi
    ./configure --enable-debug --prefix=$PREFIX --with-jpeg=$PREFIX --without-x
  fi
  make || exit -1
  cp x11vnc/x11vnc ../iic-client/bin/mtgshare
)

echo "DONE"

popd >/dev/null
