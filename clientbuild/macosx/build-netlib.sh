#!/bin/bash
. build-common.sh
pushd ../../iic-client/netlib >/dev/null
bakefile -f autoconf netlib.bkl
./configure --prefix=$prefix || exit -1
make || exit -1
sudo make install || exit -1
popd >/dev/null
