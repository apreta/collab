#!/bin/sh

# This script can use a template disk image from branding directory.  Template
# should be build with no partitions, and should have background image and application
# bundle set up as desired.  This script will replace the application bundle with the
# new binaries and create a new downloadable disk image.
#

NAME=Apreta
APPNAME=Conferencing

VERSION=$1
BUILD_NUM=$2
BRAND=$3

if [ "" == "$BRAND" ]
then
    echo "Missing brand parameter. Using Defaults."
else
	BRAND_DIR=`pwd`/branding/$BRAND
fi

if [ -f branding/$BRAND/brand.sh ]; then
    . branding/$BRAND/brand.sh
fi

if [ "`uname -p`" == "powerpc" ]
then
    ARCHIVE=$NAME-$VERSION-$BUILD_NUM-PPC.dmg
else
    ARCHIVE=$NAME-$VERSION-$BUILD_NUM-Intel.dmg
fi

echo "Building disk image for $NAME Version $VERSION"
rm -f "$ARCHIVE"

if [ -f $BRAND_DIR/image.dmg ]; then
	cp $BRAND_DIR/image.dmg work.dmg
	mounted=`hdiutil mount -puppetstrings work.dmg`
	imagedev=`echo $mounted | cut -d " " -f 1`
	imagedir=`echo $mounted | cut -d " " -f 2-`
	echo "$imagedir"
	rm -rf "$imagedir"/*.app
	cp -R $NAME/*.app "$imagedir"
	diskutil rename "$imagedev" "$NAME $APPNAME"
	hdiutil detach "$imagedev"
	hdiutil resize -sectors min work.dmg
	hdiutil convert -imagekey zlib-level=9 -format UDZO -o "$ARCHIVE" work.dmg
	rm work.dmg
else
	imagedir="/tmp/$NAME.$$"
	rm -rf $imagedir
	mkdir $imagedir
	cp -R $NAME/*.app $imagedir
	ln -s /Applications $imagedir/Applications
	echo "hdiutil create -ov -srcfolder $imagedir -format UDBZ -volname \"$APPNAME\" \"$ARCHIVE\""
	hdiutil create -ov -srcfolder $imagedir -format UDBZ -volname "$APPNAME" "$ARCHIVE"
fi

# Intenet-enable causes Safari to automatically unpack the app into the download directory.
# echo "hdiutil internet-enable -yes \"$ARCHIVE\""
# hdiutil internet-enable -yes "$ARCHIVE"

rm -rf $imagedir

ls -l "$ARCHIVE"

exit
