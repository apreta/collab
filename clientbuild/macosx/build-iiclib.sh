#!/bin/bash

. build-common.sh

pushd ../../iic/clients/iiclib >/dev/null

bakefile -f autoconf iiclib.bkl
./configure --prefix=$prefix || exit -1

make || exit -1

sudo make install || exit -1

popd >/dev/null
