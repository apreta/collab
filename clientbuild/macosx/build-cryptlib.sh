#!/bin/bash

. build-common.sh

pushd ../../cryptlib >/dev/null

bakefile -f autoconf cryptlib.bkl
./configure --prefix=$prefix || exit 1

make || exit -1

sudo make install || exit -1

popd >/dev/null
