#!/bin/bash

. ./build-common.sh

pushd ../../x11vnc >/dev/null

echo "Building x11vnc"

if [ ! "$1" == "--make" ]; then
	./configure --enable-debug || exit -1
fi

make || exit -1

cp x11vnc/x11vnc ../iic/clients/bin/mtgshare
sudo cp x11vnc/x11vnc $prefix/bin/mtgshare

cd ../vnc_unixsrc

echo "Building vncviewer"

if [ ! "$1" == "--make" ]; then
	make clean
fi

xmkmf
make World || exit -1;
#make || exit -1;

cp vncviewer/vncviewer ../iic/clients/bin/mtgviewer
sudo cp vncviewer/vncviewer $prefix/bin/mtgviewer

popd >/dev/null
