%define name xdb_sql
%define version HEAD
%define release idx
  
Summary: Jabber transport for Jabber
Name: %{name}
Version: %{version}
Release: %{release}
Copyright: GPL
Group: Applications/Internet
Source: xdb_sql.tgz
#BuildRoot: /var/tmp/%{name}-%{version}-%{release}
 
%description
This an xdb implementation for Jabber 1.4 and later using an SQL database.
 

#%prep
#%setup

#%build
#./configure
#make

#%install
#mkdir -p $RPM_BUILD_ROOT
#make DESTDIR=$RPM_BUILD_ROOT install

#%clean
#rm -rf $RPM_BUILD_ROOT

%post
echo "Voir le répertoire /usr/share/doc/jabber"
echo "pour les informations concernant"
echo "  - le modules xdb_sql"

#%defattr(-,root,root)
%files
/opt/jabber/xdb_sql/xdb_sql.so
/opt/jabber/xdb_sql/xdb_sql.xml

/usr/share/doc/jabber/README.xdb_sql
/usr/share/doc/jabber/sample_database.pg.sql
/usr/share/doc/jabber/sample_database.sql
