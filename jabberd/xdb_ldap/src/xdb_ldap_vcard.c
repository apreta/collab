/**********************************************************************
 *  
 * This code was developped by IDEALX (http://IDEALX.org/) and
 * contributors (their names can be found in the AUTHORS file). It
 * is released under the terms of the JOSL license 
 * (http://www.opensource.org/licenses/jabberpl.html)
 *
 *********************************************************************/

#include "xdb_ldap.h"

/* to store LDAP request to be executed after having retrieved Vcard data */
typedef struct XdbLdapRequest
{
    LDAPMod *attr;
    struct XdbLdapRequest *next;
} XdbLdapRequest;

/* mapping functions from LDAP to XML Vcard */
static xmlnode xdb_ldap2vcard_n(char *attr, char **vals, xmlnode x);
static xmlnode xdb_ldap2vcard_nickname(char *attr, char **vals, xmlnode x);
static xmlnode xdb_ldap2vcard_url(char *attr, char **vals, xmlnode x);
static xmlnode xdb_ldap2vcard_org(char *attr, char **vals, xmlnode x);
static xmlnode xdb_ldap2vcard_title(char *attr, char **vals, xmlnode x);
static xmlnode xdb_ldap2vcard_tel(char *attr, char **vals, xmlnode x);
static xmlnode xdb_ldap2vcard_adr(char *attr, char **vals, xmlnode x);
static xmlnode xdb_ldap2vcard_email(char *attr, char **vals, xmlnode x);

/* mapping functions from XML Vcard to LDAP */
static XdbLdapRequest *xdb_vcard2ldap_n(XdbLdapRequest *cur, xmlnode x);
static XdbLdapRequest *xdb_vcard2ldap_nickname(XdbLdapRequest *cur, xmlnode x);
static XdbLdapRequest *xdb_vcard2ldap_url(XdbLdapRequest *cur, xmlnode x);
static XdbLdapRequest *xdb_vcard2ldap_org(XdbLdapRequest *cur, xmlnode x);
static XdbLdapRequest *xdb_vcard2ldap_title(XdbLdapRequest *cur, xmlnode x);
static XdbLdapRequest *xdb_vcard2ldap_tel(XdbLdapRequest *cur, xmlnode x);
static XdbLdapRequest *xdb_vcard2ldap_adr(XdbLdapRequest *cur, xmlnode x);
static XdbLdapRequest *xdb_vcard2ldap_email(XdbLdapRequest *cur, xmlnode x);

/* create the whole LDAP request and executes it */
static int xdbldap_vcard_record(XdbLdapDatas *self, XdbLdapConnList *curr_conn, struct XdbLdapRequest *req);
/* counts the number of attributes in the request */
static int xdbldap_count_attrs(XdbLdapRequest *req);
/* create the <FN> tag from <FAMILY> and <GIVEN> */
static int xdbldap_create_fn(xmlnode vcard);
/* add an attribute in the global request */
static XdbLdapRequest *xdbldap_add_attr(XdbLdapRequest *req, LDAPMod *attr);

/* group many attributes in one function */
/* permits to deal with nested xml tags */
static XdbLdap2Vcard static_map_ldap[] =
{
    { "givenName", xdb_ldap2vcard_n },
    { "sn", xdb_ldap2vcard_n },
    { "displayName", xdb_ldap2vcard_nickname },
    { "labeledURI", xdb_ldap2vcard_url },
    { "o", xdb_ldap2vcard_org },
    { "ou", xdb_ldap2vcard_org },
    { "title", xdb_ldap2vcard_title },
    { "homePhone", xdb_ldap2vcard_tel },
    { "street", xdb_ldap2vcard_adr },
    { "l", xdb_ldap2vcard_adr },
    { "st", xdb_ldap2vcard_adr },
    { "postalCode", xdb_ldap2vcard_adr },
    { "c", xdb_ldap2vcard_adr },
    { "mail", xdb_ldap2vcard_email },
    { NULL, NULL}
};

/* FN is GIVEN + FAMILY  so do not handle it */
static XdbVcard2Ldap static_map_vcard[] =
{
    { "N", xdb_vcard2ldap_n},
    { "NICKNAME", xdb_vcard2ldap_nickname},
    { "URL", xdb_vcard2ldap_url },
    { "ORG", xdb_vcard2ldap_org },
    { "TITLE", xdb_vcard2ldap_title },
    { "TEL", xdb_vcard2ldap_tel },
    { "ADR", xdb_vcard2ldap_adr },
    { "EMAIL", xdb_vcard2ldap_email },
    { NULL, NULL}
};

xmlnode xdbldap_vcard_get(XdbLdapDatas *self, XdbLdapConnList *curr_conn)
{
  LDAPMessage   *current_result;
  char            *current_attr;
  BerElement               *ber;
  int         rc, entries_count;
  XdbLdap2Vcard        *handled;
  xmlnode          vcard = NULL;
  XdbLdapEvtResult     *evt_res;     /* for asynchronous request */
  pth_event_t               evt;

  /* log_debug(ZONE,"[xdbldap_vcard_get] filter : %s", filter); */

  /* to wait for the results */
  evt_res = (XdbLdapEvtResult *) malloc(sizeof(XdbLdapEvtResult));
  evt_res->ld = curr_conn->ld;

  /* get user info from LDAP */
  if ((rc = ldap_search_ext(evt_res->ld, self->base, LDAP_SCOPE_SUBTREE, 
			    curr_conn->entry, NULL, 0, NULL, NULL, NULL, 
			    LDAP_NO_LIMIT, &(evt_res->msgid))) != LDAP_SUCCESS) { 
      log_error(ZONE,"[xdbldap_vcard_get] search error : %s", ldap_err2string(rc)); 
      return NULL; 
  } 
    
  /* wait for the operation to be terminated, poll every second */
  evt = pth_event(PTH_EVENT_FUNC, &xdbldap_wait_result, (void *) evt_res,
		  pth_time(1, 0));
  pth_wait(evt);

  if ((entries_count = ldap_count_entries(evt_res->ld,evt_res->result)) != 1) {
    log_error(ZONE,"[xdbldap_vcard_get] there are %d entries !", entries_count);
    return NULL;
  }

  /* prepare the XML result */
  vcard = xmlnode_new_tag("vCard");
  xmlnode_put_attrib(vcard, "xmlns", "vcard-temp");

  current_result = ldap_first_entry(evt_res->ld, evt_res->result); 
  /* step through each attribute in objectclass */
  for (current_attr = ldap_first_attribute(evt_res->ld, current_result, &ber);
       current_attr != NULL;
       current_attr = ldap_next_attribute(evt_res->ld, current_result, ber)) {
      /* and look for its associated callback */
      for (handled = static_map_ldap; handled->attrname != NULL; handled++) {
	  if (strcmp(handled->attrname, current_attr) == 0) {
	      /* log_debug(ZONE,"[xdbldap_vcard_get] found callback for attr : %s", current_attr); */
	      char **vals = ldap_get_values(evt_res->ld, current_result, current_attr);
	      vcard = (handled->set)(current_attr, vals, vcard);
	      ldap_value_free(vals);
	      break;
	  }
      }
      /* recommended way by C API draft-ietf-... */
      /* by BO, 2001/03/13 */
      ldap_memfree(current_attr);
  }

  /* recommended way by C API draft-ietf-... */
  /* by BO, 2001/03/13 */
  if (ber != NULL)
      ber_free(ber,0);
  
  /* don't forget to free the next attribute */
  ldap_memfree(current_attr);
  ldap_msgfree(evt_res->result);  
  free(evt_res);

  /* FN is tag is composed of GIVEN + FAMILY, create it last */
  xdbldap_create_fn(vcard);
  log_debug(ZONE,"[xdbldap_vcard_get] returning %s", xmlnode2str(vcard));
  return vcard;
}

int xdbldap_create_fn(xmlnode vcard)
{
  xmlnode n,fn;
  char *family,*given,*fn_str;

  /*    log_debug(ZONE,"[xdbldap_create_fn] vcard : %s", xmlnode2str(vcard)); */
  fn = xmlnode_new_tag("FN");
  n = xmlnode_get_tag(vcard,"N");
  family = xmlnode_get_tag_data(n,"FAMILY");
  given = xmlnode_get_tag_data(n,"GIVEN");

  if ((family == NULL) && (given == NULL)) {
      log_debug(ZONE,"[xdbldap_create_fn] no data, returning");
      xmlnode_insert_tag_node(vcard,fn);
      return 1;
  }

  if (family != NULL) {
      fn_str = (char *) malloc(sizeof(char)*(strlen(family)+2));
      sprintf(fn_str, "%s ", family);
  }
  if (given != NULL) {
      fn_str = (char *) realloc(fn_str,sizeof(char)*(strlen(family)+2+strlen(given)));
      strcat(fn_str,given);
  }

  xmlnode_insert_cdata(fn,fn_str,strlen(fn_str));
  xmlnode_insert_tag_node(vcard,fn);
  free(fn_str);

  return 1;
}

int xdbldap_vcard_set(XdbLdapDatas *self, XdbLdapConnList *curr_conn, xmlnode data)
{
  xmlnode child;                      /* to step through xmlnode children */
  XdbLdapRequest *ldap_req = NULL;    /* linked list of attributes to set/modify */
  XdbVcard2Ldap *handled;
  int attr_handled = 0;               /* temp ... just for test reasons */

  log_debug(ZONE,"[xdbldap_vcard_set] received xmlnode %s", xmlnode2str(data));

  if (data == NULL) {
      log_error(ZONE,"[xdbldap_vcard_set] no xml data");
      return 0;
  }
  
  /* step through each tag in the vcard request */
  for (child = xmlnode_get_firstchild(data);
       child != NULL;
       child = xmlnode_get_nextsibling(child)) {
    /* and look for its callback */
    for (handled = static_map_vcard; handled->tagname != NULL; handled++) {
      if (strcmp(handled->tagname, xmlnode_get_name(child)) == 0) {
	ldap_req = (handled->set)(ldap_req, child);
	attr_handled = 1;
	break;
      }
    }
    if (attr_handled == 0) {
      log_debug(ZONE,"[xdbldap_vcard_set] unhandled tag : %s", xmlnode2str(child));
    }
  }

  /* now, we can made the modifications in LDAP */
  return xdbldap_vcard_record(self, curr_conn, ldap_req);
}

int xdbldap_vcard_record(XdbLdapDatas *self, XdbLdapConnList *curr_conn, struct XdbLdapRequest *req)
{
  LDAPMod           **attrs;
  int                     i;
  int                 nbmod;
  XdbLdapRequest   *cur_req, *cur_temp;
  XdbLdapEvtResult *evt_res;
  pth_event_t evt;

  if (req == NULL) {
    log_debug(ZONE,"[xdbldap_vcard_record] nothing to modify");
    return 1;
  }

  nbmod = xdbldap_count_attrs(req);

  /* log_debug(ZONE,"[xdbldap_vcard_record] gonna record %d fields for %s", nbmod, curr_conn->binddn); */

  /* Construct the array of LDAPMod structures representing the attributes 
     of the new entry. */ 
  attrs = (LDAPMod **) malloc((nbmod+1) * sizeof(LDAPMod *)); 
  if (attrs == NULL) { 
    log_error(ZONE,"[xdbldap_vcard_record] unable to allocate memory"); 
    return 0; 
  } 
  for (i = 0; i < nbmod; i++) { 
      if ((attrs[i] = (LDAPMod *) malloc(sizeof(LDAPMod))) == NULL) { 
	  log_error(ZONE,"[xdb_ldap_auth_set_new] unable to allocate memory");
	  return -1; 
      } 
  } 	

  cur_req = req;
  for (i = 0; i < nbmod; i++) {
      memcpy(attrs[i],cur_req->attr,sizeof(LDAPMod)); 
      cur_req = cur_req->next;
  }
  attrs[nbmod] = NULL;

  /* to wait for the results */
  evt_res = (XdbLdapEvtResult *) malloc(sizeof(XdbLdapEvtResult));
  evt_res->ld = curr_conn->ld;

  if ((evt_res->rc = ldap_modify_ext(evt_res->ld, curr_conn->binddn, 
			   attrs, NULL, NULL, &(evt_res->msgid))) != LDAP_SUCCESS) {
    log_error(ZONE,"[xdbldap_vcard_record] modification error : %s",
	      ldap_err2string(evt_res->rc));
    return 0;
  }

  /* wait for the operation to be terminated, poll every second */
  evt = pth_event(PTH_EVENT_FUNC, &xdbldap_wait_result, (void *) evt_res,
		  pth_time(1, 0));
  pth_wait(evt);

  ldap_msgfree(evt_res->result);  
  free(evt_res);

  cur_req = req;
  for (i = 0; i < nbmod; i++) { 
      cur_temp = cur_req;
      cur_req = cur_req->next;

      free(attrs[i]);
      free(cur_temp->attr->mod_values[0]);
      free(cur_temp->attr->mod_values);
      free(cur_temp->attr);
      free(cur_temp);
  } 
  free(attrs); 

  log_debug(ZONE,"[xdbldap_vcard_record] vcard successfully modified in LDAP");
  
  return 1;
}

/* count the number of LDAPMod in the structure */
int xdbldap_count_attrs(XdbLdapRequest *req)
{
  int nb_mod = 0;
  XdbLdapRequest *temp;

  for (temp = req; temp != NULL; temp = temp->next) 
    nb_mod++;

  return nb_mod;
}

XdbLdapRequest *xdbldap_add_attr(XdbLdapRequest *req, LDAPMod *attr)
{
  /* log_debug(ZONE,"[xdb_ldap_add_attr] gonna add an entry"); */
  if (req == NULL) {
    if ((req = (XdbLdapRequest *) malloc(sizeof(XdbLdapRequest))) == NULL) {
      log_error(ZONE,"[xdb_ldap_add_attr] unable to allocate memory");
      return NULL; 
    } 
    req->attr = attr;
    req->next = NULL;
  } else {
    XdbLdapRequest *temp;
    XdbLdapRequest *new_req;

    if ((new_req = (XdbLdapRequest *) malloc(sizeof(XdbLdapRequest))) == NULL) {
      log_error(ZONE,"[xdb_ldap_add_attr] unable to allocate memory");
      return NULL; 
    } 
    new_req->attr = attr;
    new_req->next = NULL;
    temp = req;
    while (temp->next != NULL)
      temp = temp->next;
    temp->next = new_req;
  }

  return req;
}


/*****************************/
/*  LDAP -> Vcard functions  */
/*****************************/

xmlnode xdb_ldap2vcard_n(char *attr, char **vals, xmlnode x)
{
  xmlnode n;
  int first = 0;
  /* log_debug(ZONE,"[xdb_ldap2vcard_n] got attr : %s", attr); */

  n = xmlnode_get_tag(x,"N");
  if (n == NULL) {
    n = xmlnode_new_tag("N");
    first = 1;
  } 

  if (strcmp(attr,"sn") == 0) {
    xmlnode cn = xmlnode_new_tag("FAMILY");
    xmlnode_insert_cdata(cn, vals[0], strlen(vals[0]));
    xmlnode_insert_tag_node(n,cn);
  } else if (strcmp(attr,"givenName") == 0) {
    xmlnode givenname = xmlnode_new_tag("GIVEN");
    xmlnode_insert_cdata(givenname, vals[0], strlen(vals[0]));
    xmlnode_insert_tag_node(n,givenname);
  }

  if (first)
    xmlnode_insert_tag_node(x,n);
  /* log_debug(ZONE,"[xdb_ldap2vcard_n] returning %s", xmlnode2str(x)); */
  return x;
}

xmlnode xdb_ldap2vcard_nickname(char *attr, char **vals, xmlnode x)
{
  xmlnode nick;
  /* log_debug(ZONE,"[xdb_ldap2vcard_nickname] got attr : %s", attr); */

  nick = xmlnode_new_tag("NICKNAME");
  xmlnode_insert_cdata(nick, vals[0], strlen(vals[0]));
  if (x != NULL) {
    xmlnode_insert_tag_node(x,nick);
    return x;
  }
  else 
    return nick;
}

xmlnode xdb_ldap2vcard_url(char *attr, char **vals, xmlnode x)
{
    xmlnode url;
    /* log_debug(ZONE,"[xdb_ldap2vcard_url] got attr : %s", attr); */

    url = xmlnode_new_tag("URL");
    xmlnode_insert_cdata(url, vals[0], strlen(vals[0]));
    if (x != NULL) {
	xmlnode_insert_tag_node(x,url);
	return x;
    }
    else 
	return url;
}

xmlnode xdb_ldap2vcard_org(char *attr, char **vals, xmlnode x)
{
    xmlnode org;
    int first = 0;
    /* log_debug(ZONE,"[xdb_ldap2vcard_org] got attr : %s", attr); */

    org = xmlnode_get_tag(x,"ORG");
    if (org == NULL) {
	org = xmlnode_new_tag("ORG");
	first = 1;
    } 

    if (strcmp(attr,"o") == 0) {
	xmlnode oname = xmlnode_new_tag("ORGNAME");
	xmlnode_insert_cdata(oname, vals[0], strlen(vals[0]));
	xmlnode_insert_tag_node(org,oname);
    } else if (strcmp(attr,"ou") == 0) {
	xmlnode ounit = xmlnode_new_tag("ORGUNIT");
	xmlnode_insert_cdata(ounit, vals[0], strlen(vals[0]));
	xmlnode_insert_tag_node(org,ounit);
    }

    if (first)
	xmlnode_insert_tag_node(x,org);
    /* log_debug(ZONE,"[xdb_ldap2vcard_org] returning %s", xmlnode2str(x)); */
    return x;
}

xmlnode xdb_ldap2vcard_title(char *attr, char **vals, xmlnode x)
{
    xmlnode title;
    /* log_debug(ZONE,"[xdb_ldap2vcard_title] got attr : %s", attr); */

    title = xmlnode_new_tag("TITLE");
    xmlnode_insert_cdata(title, vals[0], strlen(vals[0]));
    if (x != NULL) {
	xmlnode_insert_tag_node(x,title);
	return x;
    }
    else 
	return title;
}

xmlnode xdb_ldap2vcard_tel(char *attr, char **vals, xmlnode x)
{
    xmlnode tel;
    /* log_debug(ZONE,"[xdb_ldap2vcard_tel] got attr : %s", attr); */

    tel = xmlnode_new_tag("TEL");
    xmlnode_insert_cdata(tel, vals[0], strlen(vals[0]));
    if (x != NULL) {
	xmlnode_insert_tag_node(x,tel);
	return x;
    }
    else 
	return tel;
}

xmlnode xdb_ldap2vcard_adr(char *attr, char **vals, xmlnode x)
{
    xmlnode adr;
    int first = 0;
    /* log_debug(ZONE,"[xdb_ldap2vcard_adr] got attr : %s", attr); */

    adr = xmlnode_get_tag(x,"ADR");
    if (adr == NULL) {
	adr = xmlnode_new_tag("ADR");
	first = 1;
    } 

    if (strcmp(attr,"street") == 0) {
	xmlnode street = xmlnode_new_tag("STREET");
	xmlnode_insert_cdata(street, vals[0], strlen(vals[0]));
	xmlnode_insert_tag_node(adr,street);
    } else if (strcmp(attr,"l") == 0) {
	xmlnode l = xmlnode_new_tag("LOCALITY");
	xmlnode_insert_cdata(l, vals[0], strlen(vals[0]));
	xmlnode_insert_tag_node(adr,l);
    } else if (strcmp(attr,"st") == 0) {
	xmlnode st = xmlnode_new_tag("REGION");
	xmlnode_insert_cdata(st, vals[0], strlen(vals[0]));
	xmlnode_insert_tag_node(adr,st);
    } else if (strcmp(attr,"postalCode") == 0) {
	xmlnode pc = xmlnode_new_tag("PCODE");
	xmlnode_insert_cdata(pc, vals[0], strlen(vals[0]));
	xmlnode_insert_tag_node(adr,pc);
    } else if (strcmp(attr,"c") == 0) {
	xmlnode c = xmlnode_new_tag("COUNTRY");
	xmlnode_insert_cdata(c, vals[0], strlen(vals[0]));
	xmlnode_insert_tag_node(adr,c);
    }

    if (first)
	xmlnode_insert_tag_node(x,adr);
    /* log_debug(ZONE,"[xdb_ldap2vcard_adr] returning %s", xmlnode2str(x)); */
    return x;
}

xmlnode xdb_ldap2vcard_email(char *attr, char **vals, xmlnode x)
{
    xmlnode email;
    /* log_debug(ZONE,"[xdb_ldap2vcard_email] got attr : %s", attr); */

    email = xmlnode_new_tag("EMAIL");

    /* to correct a _temp_ bug : see BUGS */
    vals[0][strlen(vals[0])] = '\0';
    xmlnode_insert_cdata(email, vals[0], strlen(vals[0]));
    if (x != NULL) {
	xmlnode_insert_tag_node(x,email);
	return x;
    }
    else 
	return email;
}

/*****************************/
/*  Vcard -> LDAP functions  */
/*****************************/

XdbLdapRequest *xdb_vcard2ldap_n(XdbLdapRequest *cur, xmlnode x)
{
  char *data;
  xmlnode child;

/*    log_debug(ZONE,"[xdb_vcard2ldap_n] received tag : %s", xmlnode2str(x));  */

  for (child = xmlnode_get_firstchild(x);
       child != NULL; child = xmlnode_get_nextsibling(child)) {

    if ((data = xmlnode_get_data(child)) != NULL) {
	LDAPMod *mod;

/*  	log_debug(ZONE,"[xdb_vcard2ldap_n] got data : %s", data); */

	if ((mod = (LDAPMod *) malloc(sizeof(LDAPMod))) == NULL) {
	    log_error(ZONE,"[xdb_vcard2ldap_n] unable to allocate memory");
	    return 0;
	}

	mod->mod_op = LDAP_MOD_REPLACE;
	if (strcmp(xmlnode_get_name(child),"FAMILY") == 0)
	    mod->mod_type = "sn";
	else if (strcmp(xmlnode_get_name(child),"GIVEN") == 0)
	    mod->mod_type = "givenName";
	else {
	    log_debug(ZONE,"[xdb_vcard2ldap_n] unhandled tag name : %s", xmlnode_get_name(child));
	    free(mod);
	    continue;
	}
	    
	mod->mod_values = (char **) malloc(2 * sizeof(char*)); 
	mod->mod_values[0] = (char *) malloc((strlen(data)+1)*sizeof(char));
	strcpy(mod->mod_values[0],data);
	mod->mod_values[1] = NULL;

	cur = xdbldap_add_attr(cur,mod);
    }
  }

  return cur;
}

XdbLdapRequest *xdb_vcard2ldap_nickname(XdbLdapRequest *cur, xmlnode x)
{
  char *data;

  /* log_debug(ZONE,"[xdb_vcard2ldap_nickname] received tag : %s", xmlnode2str(x)); */

  if ((data = xmlnode_get_data(x)) != NULL) {
    LDAPMod *mod;

    if ((mod = (LDAPMod *) malloc(sizeof(LDAPMod))) == NULL) {
      log_error(ZONE,"[xdb_vcard2ldap_n] unable to allocate memory");
      return 0;
    }

    mod->mod_op = LDAP_MOD_REPLACE;
    if (strcmp(xmlnode_get_name(x),"NICKNAME") == 0)
      mod->mod_type = "displayName";
    else {
      log_debug(ZONE,"[xdb_vcard2ldap_nickname] unhandled tag name : %s", xmlnode_get_name(x));
      free(mod);
      return cur;
    }
	    
    mod->mod_values = (char **) malloc(2 * sizeof(char*)); 
    mod->mod_values[0] = (char *) malloc((strlen(data)+1)*sizeof(char));
    strcpy(mod->mod_values[0],data);
    mod->mod_values[1] = NULL;

    cur = xdbldap_add_attr(cur,mod);
  }

  return cur;
}

XdbLdapRequest *xdb_vcard2ldap_url(XdbLdapRequest *cur, xmlnode x)
{
  char *data;

  /* log_debug(ZONE,"[xdb_vcard2ldap_url] received tag : %s", xmlnode2str(x)); */

  if ((data = xmlnode_get_data(x)) != NULL) {
    LDAPMod *mod;

    if ((mod = (LDAPMod *) malloc(sizeof(LDAPMod))) == NULL) {
      log_error(ZONE,"[xdb_vcard2ldap_url] unable to allocate memory");
      return 0;
    }

    mod->mod_op = LDAP_MOD_REPLACE;
    if (strcmp(xmlnode_get_name(x),"URL") == 0)
      mod->mod_type = "labeledURI";
    else {
      log_debug(ZONE,"[xdb_vcard2ldap_url] unhandled tag name : %s", xmlnode_get_name(x));
      free(mod);
      return cur;
    }
	    
    mod->mod_values = (char **) malloc(2 * sizeof(char*)); 
    mod->mod_values[0] = (char *) malloc((strlen(data)+1)*sizeof(char));
    strcpy(mod->mod_values[0],data);
    mod->mod_values[1] = NULL;

    cur = xdbldap_add_attr(cur,mod);
  }

  return cur;
}

XdbLdapRequest *xdb_vcard2ldap_org(XdbLdapRequest *cur, xmlnode x)
{
  char *data;
  xmlnode child;

  /* log_debug(ZONE,"[xdb_vcard2ldap_org] received tag : %s", xmlnode2str(x)); */

  for (child = xmlnode_get_firstchild(x);
       child != NULL; child = xmlnode_get_nextsibling(child)) {
    
    if ((data = xmlnode_get_data(child)) != NULL) {
      LDAPMod *mod;

      if ((mod = (LDAPMod *) malloc(sizeof(LDAPMod))) == NULL) {
	log_error(ZONE,"[xdb_vcard2ldap_org] unable to allocate memory");
	return 0;
      }

      mod->mod_op = LDAP_MOD_REPLACE;
      if (strcmp(xmlnode_get_name(child),"ORGNAME") == 0)
	mod->mod_type = "o";
      else if (strcmp(xmlnode_get_name(child),"ORGUNIT") == 0)
	mod->mod_type = "ou";
      else {
	free(mod);
	continue;
      }
	    
      mod->mod_values = (char **) malloc(2 * sizeof(char*)); 
      mod->mod_values[0] = (char *) malloc((strlen(data)+1)*sizeof(char));
      strcpy(mod->mod_values[0],data);
      mod->mod_values[1] = NULL;

      cur = xdbldap_add_attr(cur,mod);
    }
  }
  
  return cur;
}

XdbLdapRequest *xdb_vcard2ldap_title(XdbLdapRequest *cur, xmlnode x)
{
  char *data;

  /* log_debug(ZONE,"[xdb_vcard2ldap_title] received tag : %s", xmlnode2str(x)); */

  if ((data = xmlnode_get_data(x)) != NULL) {
    LDAPMod *mod;

    if ((mod = (LDAPMod *) malloc(sizeof(LDAPMod))) == NULL) {
      log_error(ZONE,"[xdb_vcard2ldap_title] unable to allocate memory");
      return 0;
    }

    mod->mod_op = LDAP_MOD_REPLACE;
    if (strcmp(xmlnode_get_name(x),"TITLE") == 0)
      mod->mod_type = "title";
    else {
      log_debug(ZONE,"[xdb_vcard2ldap_title] unhandled tag name : %s", xmlnode_get_name(x));
      free(mod);
      return cur;
    }
	    
    mod->mod_values = (char **) malloc(2 * sizeof(char*)); 
    mod->mod_values[0] = (char *) malloc((strlen(data)+1)*sizeof(char));
    strcpy(mod->mod_values[0],data);
    mod->mod_values[1] = NULL;

    cur = xdbldap_add_attr(cur,mod);
  }

  return cur;
}

XdbLdapRequest *xdb_vcard2ldap_tel(XdbLdapRequest *cur, xmlnode x)
{
  char *data;

  /* log_debug(ZONE,"[xdb_vcard2ldap_tel] received tag : %s", xmlnode2str(x)); */

  if ((data = xmlnode_get_data(x)) != NULL) {
    LDAPMod *mod;

    if ((mod = (LDAPMod *) malloc(sizeof(LDAPMod))) == NULL) {
      log_error(ZONE,"[xdb_vcard2ldap_tel] unable to allocate memory");
      return 0;
    }

    mod->mod_op = LDAP_MOD_REPLACE;
    if (strcmp(xmlnode_get_name(x),"TEL") == 0)
      mod->mod_type = "homePhone";
    else {
      log_debug(ZONE,"[xdb_vcard2ldap_tel] unhandled tag name : %s", xmlnode_get_name(x));
      free(mod);
      return cur;
    }
	    
    mod->mod_values = (char **) malloc(2 * sizeof(char*)); 
    mod->mod_values[0] = (char *) malloc((strlen(data)+1)*sizeof(char));
    strcpy(mod->mod_values[0],data);
    mod->mod_values[1] = NULL;

    cur = xdbldap_add_attr(cur,mod);
  }

  return cur;
}

XdbLdapRequest *xdb_vcard2ldap_adr(XdbLdapRequest *cur, xmlnode x)
{
  char *data;
  xmlnode child;

  /* log_debug(ZONE,"[xdb_vcard2ldap_adr] received tag : %s", xmlnode2str(x)); */

  for (child = xmlnode_get_firstchild(x);
       child != NULL; child = xmlnode_get_nextsibling(child)) {

    if ((data = xmlnode_get_data(child)) != NULL) {
      LDAPMod *mod;

      if ((mod = (LDAPMod *) malloc(sizeof(LDAPMod))) == NULL) {
	log_error(ZONE,"[xdb_vcard2ldap_adr] unable to allocate memory");
	return 0;
      }

      mod->mod_op = LDAP_MOD_REPLACE;
      if (strcmp(xmlnode_get_name(child),"STREET") == 0)
	mod->mod_type = "street";
      else if (strcmp(xmlnode_get_name(child),"LOCALITY") == 0)
	mod->mod_type = "l";
      else if (strcmp(xmlnode_get_name(child),"REGION") == 0)
	mod->mod_type = "st";
      else if (strcmp(xmlnode_get_name(child),"PCODE") == 0)
	mod->mod_type = "postalCode";
      else if (strcmp(xmlnode_get_name(child),"COUNTRY") == 0)
	mod->mod_type = "c";
      else {
	log_debug(ZONE,"[xdb_vcard2ldap_adr] unhandled tag name : %s", xmlnode_get_name(child));
	free(mod);
	continue;
      }
	    
      mod->mod_values = (char **) malloc(2 * sizeof(char*)); 
      mod->mod_values[0] = (char *) malloc((strlen(data)+1)*sizeof(char));
      strcpy(mod->mod_values[0],data);
      mod->mod_values[1] = NULL;

      cur = xdbldap_add_attr(cur,mod);
    }
  }

  return cur;
}

XdbLdapRequest *xdb_vcard2ldap_email(XdbLdapRequest *cur, xmlnode x)
{
  char *data;

  /* log_debug(ZONE,"[xdb_vcard2ldap_email] received tag : %s", xmlnode2str(x)); */
	
  if ((data = xmlnode_get_data(x)) != NULL) {
    LDAPMod *mod;

    if ((mod = (LDAPMod *) malloc(sizeof(LDAPMod))) == NULL) {
      log_error(ZONE,"[xdb_vcard2ldap_email] unable to allocate memory");
      return 0;
    }

    mod->mod_op = LDAP_MOD_REPLACE;
    if (strcmp(xmlnode_get_name(x),"EMAIL") == 0) {
	mod->mod_type = "mail";
    } else {
	log_debug(ZONE,"[xdb_vcard2ldap_email] unhandled tag name : %s", xmlnode_get_name(x));
	free(mod);
	return cur;
    }
	    
    mod->mod_values = (char **) malloc(2 * sizeof(char*)); 
    mod->mod_values[0] = (char *) malloc((strlen(data)+1)*sizeof(char));
    strcpy(mod->mod_values[0],data);
    (mod->mod_values[0])[strlen(data)] = '\0';
    mod->mod_values[1] = NULL;

    cur = xdbldap_add_attr(cur,mod);
  }

  return cur;
}



