/**********************************************************************
 *  
 * This code was developped by IDEALX (http://IDEALX.org/) and
 * contributors (their names can be found in the AUTHORS file). It
 * is released under the terms of the JOSL license 
 * (http://www.opensource.org/licenses/jabberpl.html)
 *
 *********************************************************************/

#include "xdb_ldap.h"

int xdbldap_wait_result(void *arg) 
{
    XdbLdapEvtResult *evt_res = (XdbLdapEvtResult *) arg;
    LDAPMessage *result;
    int rc;

    /* log_debug(ZONE,"[xdbldap_wait_result] entering pth event func (msgid : %d)", evt_res->msgid); */

    rc = ldap_result(evt_res->ld,evt_res->msgid,1,NULL,&result);
    if (rc == -1) {
        log_error(ZONE,"[xdbldap_wait_result] result error %d",ldap_err2string(rc));
        evt_res->result = NULL;
        evt_res->rc = -1;
        return 1;
    } else if ((rc == LDAP_RES_ADD) || (rc == LDAP_RES_MODIFY) 
	       || (rc == LDAP_RES_SEARCH_RESULT) || (rc == LDAP_RES_SEARCH_ENTRY)
	       || (rc == LDAP_RES_DELETE)) {
        /* log_debug(ZONE,"[xdbldap_wait_result] got result for msg id %d",evt_res->msgid); */
        evt_res->result = result;  
        evt_res->rc = rc;
        return 1;   
    } else 
	return 0;   /* still waiting */
}

/* get user password from LDAP */
/* caller must free the returned string */
char *xdb_ldap_getpasswd(XdbLdapDatas *self, char *user)
{
    char *data = NULL;    /* return data from this function */
    LDAPMessage      *e;    /* pointer to LDAP result */
    char **vals;
    char *filter;
    int rc, entries_count;
    XdbLdapEvtResult *evt_res;
    pth_event_t evt;
    char *attrs[2] = {"userPassword", NULL};

    filter = malloc(sizeof(char)*(strlen(self->uniqattr)+strlen(user)+2));
    if (filter == NULL) {
	log_error(ZONE,"[xdb_ldap_getpasswd] unable to allocate memory");
	return NULL;
    }
    sprintf(filter,"%s=%s",self->uniqattr,user);
    fprintf(stderr,"[xdb_ldap_getpasswd] filter is %s\n", filter);

    /* to wait for the results */
    evt_res = malloc(sizeof(XdbLdapEvtResult));
    if (evt_res == NULL) {
	log_error(ZONE,"[xdb_ldap_getpasswd] unable to allocate memory");
	return NULL;
    }
    evt_res->ld = self->master_conn->ld;

    /* Search for the entry. */ 
    log_debug(ZONE,"[xdb_ldap_getpasswd] retrieving data for %s",user);

    if ((evt_res->rc = ldap_search_ext(evt_res->ld, self->base, LDAP_SCOPE_ONELEVEL, 
				filter, attrs, 0, NULL, NULL, NULL, 
				LDAP_NO_LIMIT, &(evt_res->msgid))) != LDAP_SUCCESS) { 
      log_error(ZONE,"[xdb_ldap_getpasswd] search error : %s", ldap_err2string(rc)); 
      return NULL; 
    } 

    /* wait for the operation to be terminated, poll every second */
    evt = pth_event(PTH_EVENT_FUNC, &xdbldap_wait_result, (void *) evt_res,
		    pth_time(1, 0));
    pth_wait(evt);

    free(filter);
    entries_count = ldap_count_entries(evt_res->ld,evt_res->result);
    if (entries_count == 0) {
	free(evt_res);
	log_debug(ZONE,"[xdb_ldap_getpasswd] user does not exist");
	return NULL;
    } else if (entries_count > 1) {
	free(evt_res);
	log_warn(ZONE,"[xdb_ldap_getpasswd] more than one user found");
	return NULL;
    }
    e = ldap_first_entry(evt_res->ld, evt_res->result); 
    
    if ((vals = ldap_get_values(evt_res->ld, e, "userPassword")) != NULL) {
	/* build the result set */
	data = (char *) malloc(sizeof(char)*(strlen(vals[0])+1));
	strcpy(data,vals[0]);
	ldap_value_free(vals);
    }
    else {
      log_debug(ZONE,"[xdb_ldap_getpasswd] user has no password !");
      data = (char *) malloc(sizeof(char));
      strcpy(data,"");
    }
    
    ldap_msgfree(evt_res->result);  
    free(evt_res);
    return data;
}
