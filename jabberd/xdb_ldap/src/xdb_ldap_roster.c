/**********************************************************************
 *  
 * This code was developped by IDEALX (http://IDEALX.org/) and
 * contributors (their names can be found in the AUTHORS file). It
 * is released under the terms of the JOSL license 
 * (http://www.opensource.org/licenses/jabberpl.html)
 *
 *********************************************************************/

#include "xdb_ldap.h"

/* attributes we're searching for */
static char *attrs[8] = {"jid", "sn", "subscription", "ask", "server", "subscribe", "type", NULL};

/* retrieve roster of users */
xmlnode xdbldap_roster_get(XdbLdapDatas *self, XdbLdapConnList *curr_conn)
{
    char                  *filter;     /* filter used to retrieve user vcard */
    char              *user_abook;     /* base for user address book */
    LDAPMessage   *current_result;
    char            *current_attr;
    BerElement               *ber;
    int         rc, entries_count;
    xmlnode         roster = NULL;
    XdbLdapEvtResult     *evt_res;     /* for asynchronous request */
    pth_event_t               evt;

    filter = (char *) malloc((strlen(curr_conn->binddn)+12)*sizeof(char));
    sprintf(filter,"(entowner=%s)", curr_conn->binddn);
    user_abook = (char *) malloc((strlen(self->abook_base)+strlen(curr_conn->user)+12)*sizeof(char));
    sprintf(user_abook, "abookName=%s,%s", curr_conn->user, self->abook_base);

/*     log_debug(ZONE,"[xdbldap_roster_get] filter : %s", filter); */
/*     log_debug(ZONE,"[xdbldap_roster_get] base : %s", user_abook); */

    /* to wait for the results */
    evt_res = (XdbLdapEvtResult *) malloc(sizeof(XdbLdapEvtResult));
    evt_res->ld = curr_conn->ld;

    /* get user info from LDAP */
    if ((rc = ldap_search_ext(evt_res->ld, self->abook_base, LDAP_SCOPE_SUBTREE, 
			      filter, attrs, 0, NULL, NULL, NULL, 
			      LDAP_NO_LIMIT, &(evt_res->msgid))) != LDAP_SUCCESS) { 
	log_error(ZONE,"[xdbldap_roster_get] search error : %s", ldap_err2string(rc)); 
	return NULL; 
    } 
    
    /* wait for the operation to be terminated, poll every second */
    evt = pth_event(PTH_EVENT_FUNC, &xdbldap_wait_result, (void *) evt_res,
		    pth_time(1, 0));
    pth_wait(evt);

    /* prepare the XML result */
    roster = xmlnode_new_tag("query");
    xmlnode_put_attrib(roster, "xmlns", "jabber:iq:roster");

    if ((entries_count = ldap_count_entries(evt_res->ld,evt_res->result)) == 0) {
	log_debug(ZONE,"[xdbldap_roster_get] the user's roster is empty !");
	ldap_msgfree(evt_res->result);  
	free(evt_res);
	free(filter);
	return roster;
    }

    for (current_result = ldap_first_entry(evt_res->ld, evt_res->result); 
	 current_result != NULL;
	 current_result = ldap_next_entry(evt_res->ld, current_result)) {

	/* for now, we suppose we can only have roster of type item */
	/* XXX : handle the case where we store conferences bookmarks */
	/* by BO, 2002/07/23 */
	xmlnode item = xmlnode_new_tag("item");

	/* retrieve attributes */
	for (current_attr = ldap_first_attribute(evt_res->ld, current_result, &ber);
	     current_attr != NULL; 
	     current_attr = ldap_next_attribute(evt_res->ld, current_result, ber)) {
	    char **vals = ldap_get_values(evt_res->ld, current_result, current_attr);
	    if (strcmp(current_attr,"jid") == 0) {
		xmlnode_put_attrib(item,"jid",vals[0]);
	    } else if (strcmp(current_attr,"sn") == 0) {
		xmlnode_put_attrib(item,"name",vals[0]);
	    } else if (strcmp(current_attr,"subscription") == 0) {
		xmlnode_put_attrib(item,"subscription",vals[0]);
	    } else if (strcmp(current_attr,"ask") == 0) {
		xmlnode_put_attrib(item,"ask",vals[0]);
	    } else if (strcmp(current_attr,"server") == 0) {
		xmlnode_put_attrib(item,"server",vals[0]);
	    } else if (strcmp(current_attr,"subscribe") == 0) {
		xmlnode_put_attrib(item,"subscribe",vals[0]);
	    } 
	    ldap_value_free(vals); 
	    ldap_memfree(current_attr);
	}

	xmlnode_insert_tag_node(roster,item);
    }
  
    /* recommended way by C API draft-ietf-... */
    /* by BO, 2001/03/13 */
    if (ber != NULL)
	ber_free(ber,0);
  
    /* don't forget to free the next attribute */
    ldap_memfree(current_attr);
    ldap_msgfree(evt_res->result);  
    free(evt_res);
    free(filter);
    
    log_debug(ZONE,"[xdbldap_roster_get] returning %s", xmlnode2str(roster));
    return roster;
}

int xdbldap_roster_set(XdbLdapDatas *self, XdbLdapConnList *curr_conn, xmlnode data) 
{
    LDAPMod           **ldap_attrs = NULL;
    XdbLdapEvtResult *evt_res;
    pth_event_t evt;
    int j;
    xmlnode temp;
    
    log_debug(ZONE,"[xdbldap_roster_set] received xmlnode %s", xmlnode2str(data));

    /* go through each item node */
    for (temp = xmlnode_get_firstchild(data); temp != NULL;
	 temp = xmlnode_get_nextsibling(temp)) {

	/* to test which attributes are provided */
	int ask, subscription, subscribe, type, server;
	int current_attr_nb, nb_attr = 5; /* at least five attributes */

	char *contact_binddn;
	char *contact_jid = (char *) malloc(sizeof(char)*(strlen(xmlnode_get_attrib(temp,"jid"))+1));
	char *contact_name = (char *) malloc(sizeof(char)*5);

	char *tab_oc[6] = { "top", "person", "organizationalPerson", "inetOrgPerson", "jabbercontact", NULL};
	char *tab_entowner[2]  = { curr_conn->binddn, NULL};

	char *tab_uniqattr[2];
	char *tab_jid[2];
 	char *tab_sn[2];
 	char *tab_ask[2];
 	char *tab_subscription[2];
 	char *tab_server[2];
 	char *tab_subscribe[2];
 	char *tab_type[2];
	
	log_debug(ZONE,"[xdbldap_roster_set] got node %s", xmlnode2str(temp));

	sprintf(contact_jid, "%s", xmlnode_get_attrib(temp,"jid"));
	
	/* prepare the main attributes */
	if (xmlnode_get_attrib(temp,"name") == NULL) {
	    contact_name = (char *) realloc(contact_name, sizeof(char)*strlen(contact_jid));
	    sprintf(contact_name, "%s", strtok(contact_jid, "@"));
	    sprintf(contact_jid, "%s", xmlnode_get_attrib(temp,"jid"));
	} else {
	    contact_name = (char *) realloc(contact_name, sizeof(char)*(strlen(xmlnode_get_attrib(temp,"name"))+1));
	    sprintf(contact_name, "%s", xmlnode_get_attrib(temp,"name"));
	}

	/* set "global" attributes tables */
	tab_uniqattr[0] = contact_name; tab_uniqattr[1] = NULL;
	tab_sn[0] = contact_name; tab_sn[1] = NULL;
	tab_jid[0] = contact_jid; tab_jid[1] = NULL;

	contact_binddn = (char *) malloc(sizeof(char)*(strlen(contact_name)+strlen(self->abook_base)+strlen(self->uniqattr)+strlen(curr_conn->user)+14));
	sprintf(contact_binddn,"%s=%s,abookName=%s,%s", self->uniqattr, contact_name, curr_conn->user, self->abook_base);

	log_debug(ZONE,"[xdbldap_roster_set] creating entry : %s", contact_binddn);
	log_debug(ZONE,"[xdbldap_roster_set] contact_jid : %s / contact_name : %s", contact_jid, contact_name);

	/* prepare "minor" attributes */
	if (xmlnode_get_attrib(temp, "ask") == NULL) {
	    ask = 0;
	} else {
	    ask = 1;
	    tab_ask[0] = xmlnode_get_attrib(temp, "ask");
	    tab_ask[1] = NULL;
	    nb_attr++;
	}
	if (xmlnode_get_attrib(temp, "subscription") == NULL) {
	    subscription = 0;
	} else {
	    subscription = 1;
	    tab_subscription[0] = xmlnode_get_attrib(temp, "subscription");
	    tab_subscription[1] = NULL;
	    nb_attr++;
	}
	if (xmlnode_get_attrib(temp, "server") == NULL) {
	    server = 0;
	} else {
	    server = 1;
	    tab_server[0] = xmlnode_get_attrib(temp, "server");
	    tab_server[1] = NULL;
	    nb_attr++;
	}
	if (xmlnode_get_attrib(temp, "subscribe") == NULL) {
	    subscribe = 0;
	} else {
	    subscribe = 1;
	    tab_subscribe[0] = xmlnode_get_attrib(temp, "subscribe");
	    tab_subscribe[1] = NULL;
	    nb_attr++;
	}
	if (xmlnode_get_attrib(temp, "type") == NULL) {
	    type = 0;
	} else {
	    type = 1;
	    tab_type[0] = xmlnode_get_attrib(temp, "type");
	    tab_type[1] = NULL;
	    nb_attr++;
	}

	ldap_attrs = (LDAPMod **) malloc((nb_attr + 1) * sizeof(LDAPMod *));
	if (ldap_attrs == NULL) { 
	    log_error(ZONE,"[xdbldap_roster_set] unable to allocate memory"); 
	    return -1; 
	} 
	/* allocate all the LDAPMod structures */
	for (j=0; j<nb_attr; j++) {
	    if ((ldap_attrs[j] = (LDAPMod *) malloc(sizeof(LDAPMod))) == NULL) { 
		log_error(ZONE,"[xdbldap_roster_set] unable to allocate memory");
		return -1; 
	    } 
	}

	ldap_attrs[0]->mod_type = "jid";
	ldap_attrs[0]->mod_op = LDAP_MOD_ADD;
	ldap_attrs[0]->mod_values = tab_jid;

	ldap_attrs[1]->mod_type = "entowner";
	ldap_attrs[1]->mod_op = LDAP_MOD_ADD;
	ldap_attrs[1]->mod_values = tab_entowner;

	/* XXX : can we suppose that 'uniqattr' will be the same for both trees ?? */
	ldap_attrs[2]->mod_type = self->uniqattr;
	ldap_attrs[2]->mod_op = LDAP_MOD_ADD;
	ldap_attrs[2]->mod_values = tab_uniqattr;

	ldap_attrs[3]->mod_type = "objectClass";
	ldap_attrs[3]->mod_op = LDAP_MOD_ADD;
	ldap_attrs[3]->mod_values = tab_oc;

	ldap_attrs[4]->mod_type = "sn";
	ldap_attrs[4]->mod_op = LDAP_MOD_ADD;
	ldap_attrs[4]->mod_values = tab_sn;

	current_attr_nb = 5;

	if (ask == 1) {
	    ldap_attrs[current_attr_nb]->mod_type = "ask";
	    ldap_attrs[current_attr_nb]->mod_op = LDAP_MOD_ADD;
	    ldap_attrs[current_attr_nb]->mod_values = tab_ask;
	    current_attr_nb++;
	}

	if (subscription == 1) {
	    ldap_attrs[current_attr_nb]->mod_type = "subscription";
	    ldap_attrs[current_attr_nb]->mod_op = LDAP_MOD_ADD;
	    ldap_attrs[current_attr_nb]->mod_values = tab_subscription;
	    current_attr_nb++;
	}

	if (server == 1) {
	    ldap_attrs[current_attr_nb]->mod_type = "server";
	    ldap_attrs[current_attr_nb]->mod_op = LDAP_MOD_ADD;
	    ldap_attrs[current_attr_nb]->mod_values = tab_server;
	    current_attr_nb++;
	}

	if (subscribe == 1) {
	    ldap_attrs[current_attr_nb]->mod_type = "subscribe";
	    ldap_attrs[current_attr_nb]->mod_op = LDAP_MOD_ADD;
	    ldap_attrs[current_attr_nb]->mod_values = tab_subscribe;
	    current_attr_nb++;
	}

	if (type == 1) {
	    ldap_attrs[current_attr_nb]->mod_type = "type";
	    ldap_attrs[current_attr_nb]->mod_op = LDAP_MOD_ADD;
	    ldap_attrs[current_attr_nb]->mod_values = tab_type;
	    current_attr_nb++;
	}

	ldap_attrs[nb_attr] = NULL;

	/* to wait for the results */
	evt_res = (XdbLdapEvtResult *) malloc(sizeof(XdbLdapEvtResult));
	/* XXX : look at ACLs in slapd.conf to permit user to create entries in abook tree */
	/* by BO, 2002/07/24 */
	/* evt_res->ld = curr_conn->ld; */
	evt_res->ld = self->master_conn->ld;

	/* as we can't know if entry already exist, first delete it ... to be sure */
	if ((evt_res->rc = ldap_delete_ext(evt_res->ld, contact_binddn, 
					   NULL, NULL, &(evt_res->msgid))) != LDAP_SUCCESS) {
	    log_error(ZONE,"[xdbldap_roster_set] modification error : %s",ldap_err2string(evt_res->rc));
	    return 0;
	}
	
	/* wait for the operation to be terminated, poll every second */
	evt = pth_event(PTH_EVENT_FUNC, &xdbldap_wait_result, (void *) evt_res,
			pth_time(1, 0));
	pth_wait(evt);

/* 	log_debug(ZONE,"[xdbldap_roster_set] entry deleted"); */
/* 	log_debug(ZONE,"[xdbldap_roster_set] attributes : "); */
/* 	for (j=0; j<nb_attr; j++) { */
/* 	    log_debug(ZONE,"[xdbldap_roster_set] %s", ldap_attrs[j]->mod_values[0]); */
/* 	} */

	if ((evt_res->rc = ldap_add_ext(evt_res->ld, contact_binddn, 
					ldap_attrs, NULL, NULL, &(evt_res->msgid))) != LDAP_SUCCESS) {
	    log_error(ZONE,"[xdbldap_roster_set] modification error : %s",ldap_err2string(evt_res->rc));
	    return 0;
	}

	/* wait for the operation to be terminated, poll every second */
	evt = pth_event(PTH_EVENT_FUNC, &xdbldap_wait_result, (void *) evt_res,
			pth_time(1, 0));
	pth_wait(evt);

	/* ldap_msgfree(evt_res->result); */
	free(evt_res);
	for (j = 0; j < nb_attr; j++) { 
	    free(ldap_attrs[j]); 
	} 
	free(ldap_attrs); 
	free(contact_jid);
	free(contact_name);

	log_debug(ZONE,"[xdbldap_roster_set] roster entry successfully modified !");
    }

    return 1;
}
