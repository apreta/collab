/**********************************************************************
 *  
 * This code was developped by IDEALX (http://IDEALX.org/) and
 * contributors (their names can be found in the AUTHORS file). It
 * is released under the terms of the JOSL license 
 * (http://www.opensource.org/licenses/jabberpl.html)
 *
 *********************************************************************/

#include "xdb_ldap.h"


static int xdbldap_auth_0k_disable(XdbLdapDatas *self, XdbLdapConnList *curr_conn);
static int xdbldap_auth_0k_set_mod(XdbLdapDatas *self, XdbLdapConnList *curr_conn, 
				   xmlnode data);
static int xdbldap_auth_0k_set_new(XdbLdapDatas *self, XdbLdapConnList *curr_conn, 
				   xmlnode data);

xmlnode xdbldap_auth_0k_get(XdbLdapDatas *self, XdbLdapConnList *curr_conn)
{
    xmlnode data = NULL;    /* return data from this function */
    xmlnode hash,token,sequence;
    LDAPMessage      *e;    /* pointer to LDAP result */
    char **vals;
    int rc, entries_count;
    XdbLdapEvtResult *evt_res;
    pth_event_t evt;
    char *attrs[4] = { "hash", "token", "sequence", NULL};

    /* to wait for the results */
    evt_res = (XdbLdapEvtResult *) malloc(sizeof(XdbLdapEvtResult));
    evt_res->ld = curr_conn->ld;

    /* Search for the entry. */ 
    if ((evt_res->rc = ldap_search_ext(evt_res->ld, self->base, LDAP_SCOPE_ONELEVEL, 
				curr_conn->entry, attrs, 0, NULL, NULL, NULL, 
				LDAP_NO_LIMIT, &(evt_res->msgid))) != LDAP_SUCCESS) { 
      log_error(ZONE,"[xdbldap_auth_0k_get] search error : %s", ldap_err2string(rc)); 
      return NULL; 
    } 

    /* log_debug(ZONE,"[xdbldap_auth_0k_get] async call to search returned : %d", evt_res->rc); */

    /* wait for the operation to be terminated, poll every second */
    evt = pth_event(PTH_EVENT_FUNC, &xdbldap_wait_result, (void *) evt_res,
		    pth_time(1, 0));
    pth_wait(evt);

    entries_count = ldap_count_entries(evt_res->ld,evt_res->result);
    if (entries_count == 0) {
	log_debug(ZONE,"[xdbldap_auth_0k_get] user does not exist");
	return NULL;
    } else if (entries_count > 1) {
	log_warn(ZONE,"[xdbldap_auth_0k_get] more than one user found");
	return NULL;
    }
    e = ldap_first_entry(evt_res->ld, evt_res->result); 
    
    /* TODO : how can I put the three tags on the same level
     *        without a 'father tag' ?
     */
    data = xmlnode_new_tag(""); 
    hash = xmlnode_new_tag("hash");
    token = xmlnode_new_tag("token");
    sequence = xmlnode_new_tag("sequence");

    /* build the result set */
    if ((vals = ldap_get_values(evt_res->ld, e, "hash")) != NULL) {
	xmlnode_insert_cdata(hash, vals[0], -1);
	ldap_value_free(vals);
    } 
    if ((vals = ldap_get_values(evt_res->ld, e, "token")) != NULL) {
	xmlnode_insert_cdata(token, vals[0], -1);
	ldap_value_free(vals);
    } 
    if ((vals = ldap_get_values(evt_res->ld, e, "sequence")) != NULL) {
	xmlnode_insert_cdata(sequence, vals[0], -1);
	ldap_value_free(vals);
    } 
    
    xmlnode_insert_tag_node(data,hash);
    xmlnode_insert_tag_node(data,token);
    xmlnode_insert_tag_node(data,sequence);

    ldap_msgfree(evt_res->result);  
    free(evt_res);

    return data;
} /* end xdbldap_auth_0k_get */


int xdbldap_auth_0k_set_new(XdbLdapDatas *self, XdbLdapConnList *curr_conn, xmlnode data)
{
    LDAPMod           **attrs;
    int                  i,rc;
    XdbLdapEvtResult *evt_res;
    pth_event_t evt;

    char *tab_uniqattr[2] = { curr_conn->user, NULL};
    char *tab_jid[2] = { curr_conn->jid, NULL};
    char *tab_oc[2] = { "jabberuser", NULL};
    char *tab_hash[2] = { xmlnode_get_tag_data(data,"hash"), NULL};
    char *tab_tok[2] = { xmlnode_get_tag_data(data,"token"), NULL};
    char *tab_seq[2] = { xmlnode_get_tag_data(data,"sequence"), NULL};

    /* log_debug(ZONE,"[xdbldap_auth_0k_set_new] adding entry %s", curr_conn->binddn); */

    /* Construct the array of LDAPMod structures representing the attributes 
       of the new entry. */ 
    attrs = (LDAPMod **) malloc(8 * sizeof(LDAPMod *)); 
    if (attrs == NULL) { 
      log_error(ZONE,"[xdb_ldap_auth_0k_set_new] unable to allocate memory"); 
      return -1; 
    } 
    for (i = 0; i < 7; i++) { 
      if ((attrs[i] = (LDAPMod *) malloc(sizeof(LDAPMod))) == NULL) { 
	log_error(ZONE,"[xdb_ldap_auth_0k_set_new] unable to allocate memory");
	return -1; 
      } 
    } 	

    attrs[0]->mod_op = LDAP_MOD_ADD; 
    attrs[0]->mod_type = "hash";
    attrs[0]->mod_values = tab_hash;

    attrs[1]->mod_op = LDAP_MOD_ADD; 
    attrs[1]->mod_type = "token";
    attrs[1]->mod_values = tab_tok;

    attrs[2]->mod_op = LDAP_MOD_ADD; 
    attrs[2]->mod_type = "sequence";
    attrs[2]->mod_values = tab_seq;

    attrs[3]->mod_op = LDAP_MOD_ADD; 
    attrs[3]->mod_type = "objectClass";
    attrs[3]->mod_values = tab_oc;

    attrs[4]->mod_op = LDAP_MOD_ADD; 
    attrs[4]->mod_type = self->uniqattr;
    attrs[4]->mod_values = tab_uniqattr;
	
    attrs[5]->mod_op = LDAP_MOD_ADD; 
    attrs[5]->mod_type = "sn";
    attrs[5]->mod_values = tab_uniqattr;
	
    attrs[6]->mod_op = LDAP_MOD_ADD; 
    attrs[6]->mod_type = "jid";
    attrs[6]->mod_values = tab_jid;

    attrs[7] = NULL;

    /* to wait for the results */
    evt_res = (XdbLdapEvtResult *) malloc(sizeof(XdbLdapEvtResult));
    evt_res->ld = self->master_conn->ld;

    if ((evt_res->rc = ldap_add_ext(evt_res->ld, curr_conn->binddn, 
				attrs, NULL, NULL, &(evt_res->msgid))) != LDAP_SUCCESS) {
	log_error(ZONE,"[xdbldap_auth_0k_set] modification error : %s",ldap_err2string(rc));
	return 0;
    }

    /* wait for the operation to be terminated, poll every second */
    evt = pth_event(PTH_EVENT_FUNC, &xdbldap_wait_result, (void *) evt_res,
		    pth_time(1, 0));
    pth_wait(evt);

    ldap_msgfree(evt_res->result);  
    free(evt_res);
    for (i = 0; i < 7; i++) { 
	free(attrs[i]); 
    } 
    free(attrs); 

    return 1;
}

int xdbldap_auth_0k_set_mod(XdbLdapDatas *self, XdbLdapConnList *curr_conn, xmlnode data)
{
    LDAPMod          **attrs;
    int                rc,i;
    XdbLdapEvtResult  *evt_res;
    pth_event_t        evt;

    char *tab_hash[2] = { xmlnode_get_tag_data(data,"hash"), NULL};
    char *tab_tok[2] = { xmlnode_get_tag_data(data,"token"), NULL};
    char *tab_seq[2] = { xmlnode_get_tag_data(data,"sequence"), NULL};

    /* log_debug(ZONE,"[xdb_ldap_auth_0k_set_mod] setting authentication info for entry : %s", curr_conn->binddn); */

    /* Construct the array of LDAPMod structures representing the attributes 
	   of the new entry. */ 
    attrs = (LDAPMod **) malloc(4 * sizeof(LDAPMod *)); 
    if (attrs == NULL) { 
	log_error(ZONE,"[xdb_ldap_auth_0k_set_mod] unable to allocate memory"); 
	return -1; 
    } 
    for (i = 0; i < 3; i++) { 
	if ((attrs[i] = (LDAPMod *) malloc(sizeof(LDAPMod))) == NULL) { 
	    log_error(ZONE,"[xdb_ldap_auth_0k_set_mod] unable to allocate memory");
	    return -1; 
	} 
    } 	

    attrs[0]->mod_op = LDAP_MOD_REPLACE; 
    attrs[0]->mod_type = "hash";
    attrs[0]->mod_values = tab_hash;

    attrs[1]->mod_op = LDAP_MOD_REPLACE; 
    attrs[1]->mod_type = "token";
    attrs[1]->mod_values = tab_tok;

    attrs[2]->mod_op = LDAP_MOD_REPLACE; 
    attrs[2]->mod_type = "sequence";
    attrs[2]->mod_values = tab_seq;
	
    attrs[3] = NULL;

    /* to wait for the results */
    evt_res = (XdbLdapEvtResult *) malloc(sizeof(XdbLdapEvtResult));
    evt_res->ld = curr_conn->ld;
	
    /* log_debug(ZONE,"[xdbldap_auth_0k_set_mod] modifying dn : %s", curr_conn->binddn); */

    if ((evt_res->rc = ldap_modify_ext(evt_res->ld, curr_conn->binddn, attrs, NULL, NULL, &(evt_res->msgid))) != LDAP_SUCCESS) {
	log_error(ZONE,"[xdbldap_auth_0k_set_mod] modification error : %s",
		  ldap_err2string(rc));
	return 0;
    }

    /* wait for the operation to be terminated, poll every second */
    evt = pth_event(PTH_EVENT_FUNC, &xdbldap_wait_result, (void *) evt_res,
		    pth_time(1, 0));
    pth_wait(evt);
    ldap_msgfree(evt_res->result);  
    free(evt_res);    
    for (i = 0; i < 3; i++) { 
	free(attrs[i]); 
    } 
    free(attrs); 

    log_debug(ZONE,"[xdb_ldap_auth_0k_set_mod] user auth info successfully modified");

    return 1;
}

int xdbldap_auth_0k_disable(XdbLdapDatas *self, XdbLdapConnList *curr_conn)
{
  log_debug(ZONE,"[xdbldap_auth_0k_disable] NOT YET IMPLEMENTED !");
  return 1;
}

int xdbldap_auth_0k_set(XdbLdapDatas *self, XdbLdapConnList *curr_conn, xmlnode data)
{
    if (data) {
	/* Figure out which query we want to use. */
	if (curr_conn->ld == NULL) {
	    /* we did not provide an user ldap conn => user does not exist */
	    /* log_debug(ZONE,"[xdbldap_auth_0k_set] now gonna add an new user in LDAP"); */
	    return xdbldap_auth_0k_set_new(self, curr_conn, data);
	} else {
	    /* log_debug(ZONE,"[xdbldap_auth_0k_set] now gonna mod an user in LDAP"); */
	    return xdbldap_auth_0k_set_mod(self, curr_conn, data);
	}
    }
    else  /* we're removing the authentication info */
      return xdbldap_auth_0k_disable(self,curr_conn);

    return 1;

}


