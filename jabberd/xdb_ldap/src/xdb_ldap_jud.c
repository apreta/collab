/**********************************************************************
 *  
 * This code was developped by IDEALX (http://IDEALX.org/) and
 * contributors (their names can be found in the AUTHORS file). It
 * is released under the terms of the JOSL license 
 * (http://www.opensource.org/licenses/jabberpl.html)
 *
 *********************************************************************/

#include "xdb_ldap.h"

/* mapping of jud fields 
 * change them to what is convenient for your LDAP schema 
 * here, mapping is :
 *     - mail -> email
 *     - givenName -> first
 *     - sn -> name
 *     - initials -> nickname
 *     - cn -> jid
 */
static char *attrs[6] = {"mail", "jid", "sn", "initials", "givenName", NULL};

/* retrieve ALL the users directory data */
/* due to the way jud works ... */
xmlnode xdbldap_jud_get(XdbLdapDatas *self, XdbLdapConnList *curr_conn)
{
  char                  *filter;     /* filter used to retrieve user vcard */
  LDAPMessage   *current_result;
  char            *current_attr;
  BerElement               *ber;
  int         rc, entries_count;
  xmlnode          users = NULL;
  XdbLdapEvtResult     *evt_res;     /* for asynchronous request */
  pth_event_t               evt;

  filter = (char *) malloc((strlen(self->uniqattr)+5)*sizeof(char));
  sprintf(filter,"(%s=*)", self->uniqattr);

  /* to wait for the results */
  evt_res = (XdbLdapEvtResult *) malloc(sizeof(XdbLdapEvtResult));
  evt_res->ld = curr_conn->ld;

  /* get user info from LDAP */
  if ((rc = ldap_search_ext(evt_res->ld, self->base, LDAP_SCOPE_SUBTREE, 
			      filter, attrs, 0, NULL, NULL, NULL, 
			      LDAP_NO_LIMIT, &(evt_res->msgid))) != LDAP_SUCCESS) { 
    log_error(ZONE,"[xdbldap_jud_get] search error : %s", ldap_err2string(rc)); 
    return NULL; 
  } 
    
  /* wait for the operation to be terminated, poll every second */
  evt = pth_event(PTH_EVENT_FUNC, &xdbldap_wait_result, (void *) evt_res,
		  pth_time(1, 0));
  pth_wait(evt);

  if ((entries_count = ldap_count_entries(evt_res->ld,evt_res->result)) == 0) {
    log_error(ZONE,"[xdbldap_jud_get] no entries found !");
    return NULL;
  }

  /* prepare the XML result */
  users = xmlnode_new_tag("query");
  xmlnode_put_attrib(users, "xmlns", "jabber:jud:users");

  for (current_result = ldap_first_entry(evt_res->ld, evt_res->result); 
       current_result != NULL;
       current_result = ldap_next_entry(evt_res->ld, current_result)) {
      xmlnode user = xmlnode_new_tag("item");
      /* retrieve attributes */
      for (current_attr = ldap_first_attribute(evt_res->ld, current_result, &ber);
	   current_attr != NULL; 
	   current_attr = ldap_next_attribute(evt_res->ld, current_result, ber)) {
	  char **vals = ldap_get_values(evt_res->ld, current_result, current_attr);
	  if (strcmp(current_attr,"jid") == 0) {
	      xmlnode_put_attrib(user,"jid",vals[0]);
	  } else if (strcmp(current_attr,"initials") == 0) {
	    xmlnode temp = xmlnode_new_tag("nick");
	    xmlnode_insert_cdata(temp,vals[0],-1);
	    xmlnode_insert_tag_node(user,temp);
	  } else if (strcmp(current_attr,"mail") == 0) {
	    xmlnode temp = xmlnode_new_tag("email");
	    xmlnode_insert_cdata(temp,vals[0],-1);
	    xmlnode_insert_tag_node(user,temp);
	  } else if (strcmp(current_attr,"givenName") == 0) {
	    xmlnode temp = xmlnode_new_tag("first");
	    xmlnode_insert_cdata(temp,vals[0],-1);
	    xmlnode_insert_tag_node(user,temp);
	  } else if (strcmp(current_attr,"sn") == 0) {
	    xmlnode temp = xmlnode_new_tag("name");
	    xmlnode_insert_cdata(temp,vals[0],-1);
	    xmlnode_insert_tag_node(user,temp);
	  } 
	  ldap_value_free(vals); 
	  ldap_memfree(current_attr);
      }

      xmlnode_insert_tag_node(users,user);
  }
  
  /* recommended way by C API draft-ietf-... */
  /* by BO, 2001/03/13 */
  if (ber != NULL)
      ber_free(ber,0);
  
  /* don't forget to free the next attribute */
  ldap_memfree(current_attr);
  ldap_msgfree(evt_res->result);  
  free(evt_res);
  free(filter);

  log_debug(ZONE,"[xdbldap_jud_get] returning %s", xmlnode2str(users));
  return users;
}

int xdbldap_jud_set(XdbLdapDatas *self, XdbLdapConnList *curr_conn, xmlnode data) 
{
    LDAPMod           **ldap_attrs = NULL;
    int                    rc;
    XdbLdapEvtResult *evt_res;
    pth_event_t evt;
    int j,i = 0;
    xmlnode temp;
    char ***tab_attrs_values = NULL;

    log_debug(ZONE,"[xdbldap_jud_set] received xmlnode %s", xmlnode2str(data));
    /* log_debug(ZONE,"[xdbldap_jud_set] binding with %s", curr_conn->binddn); */

    for (temp = xmlnode_get_firstchild(data); temp != NULL;
	 temp = xmlnode_get_nextsibling(temp)) {

	/* "last" field currently unhandled */
	if ((xmlnode_get_data(temp) == NULL) ||
	    (j_strcmp(xmlnode_get_name(temp),"last") == 0))
	    continue;

	i++;
	ldap_attrs = (LDAPMod **) realloc(ldap_attrs, i * sizeof(LDAPMod *));
	if (ldap_attrs == NULL) { 
	    log_error(ZONE,"[xdb_ldap_jud_set] unable to allocate memory"); 
	    return -1; 
	} 
	if ((ldap_attrs[i-1] = (LDAPMod *) malloc(sizeof(LDAPMod))) == NULL) { 
	    log_error(ZONE,"[xdb_ldap_jud_set] unable to allocate memory");
	    return -1; 
	} 

	tab_attrs_values = (char ***) realloc(tab_attrs_values, i * sizeof(char **));
	tab_attrs_values[i-1] = (char **) malloc(2 * sizeof(char *));
	tab_attrs_values[i-1][0] = (char *) malloc(sizeof(char)*strlen(xmlnode_get_data(temp)));
	strcpy(tab_attrs_values[i-1][0],xmlnode_get_data(temp));
	tab_attrs_values[i-1][1] = NULL;

	if (j_strcmp(xmlnode_get_name(temp),"email") == 0) {
	    /* i - 2 has been allocated in the previous round */
	    log_debug(ZONE,"modifying MAIL");
	    ldap_attrs[i-1]->mod_type = "mail";
	    ldap_attrs[i-1]->mod_op = LDAP_MOD_REPLACE;
	    ldap_attrs[i-1]->mod_values = tab_attrs_values[i-1];
	}
	if (j_strcmp(xmlnode_get_name(temp),"nick") == 0) {
	    /* i - 2 has been allocated in the previous round */
	    log_debug(ZONE,"modifying INITIALS");
	    ldap_attrs[i-1]->mod_type = "initials";
	    ldap_attrs[i-1]->mod_op = LDAP_MOD_REPLACE;
	    ldap_attrs[i-1]->mod_values = tab_attrs_values[i-1];
	}
	if (j_strcmp(xmlnode_get_name(temp),"first") == 0) {
	    /* i - 2 has been allocated in the previous round */
	    log_debug(ZONE,"modifying GivenName");
	    ldap_attrs[i-1]->mod_type = "givenName";
	    ldap_attrs[i-1]->mod_op = LDAP_MOD_REPLACE;
	    ldap_attrs[i-1]->mod_values = tab_attrs_values[i-1];
	}
	if (j_strcmp(xmlnode_get_name(temp),"name") == 0) {
	    /* i - 2 has been allocated in the previous round */
	    log_debug(ZONE,"modifying SN");
	    ldap_attrs[i-1]->mod_type = "sn";
	    ldap_attrs[i-1]->mod_op = LDAP_MOD_REPLACE;
	    ldap_attrs[i-1]->mod_values = tab_attrs_values[i-1];
	}
    }

    ldap_attrs = (LDAPMod **) realloc(ldap_attrs, (i+1) * sizeof(LDAPMod *));
    if (ldap_attrs == NULL) { 
	log_error(ZONE,"[xdb_ldap_jud_set] unable to allocate memory"); 
	return -1; 
    } 
    ldap_attrs[i] = NULL;

    /* to wait for the results */
    evt_res = (XdbLdapEvtResult *) malloc(sizeof(XdbLdapEvtResult));
    evt_res->ld = curr_conn->ld;

    if ((evt_res->rc = ldap_modify_ext(evt_res->ld, curr_conn->binddn, 
		    ldap_attrs, NULL, NULL, &(evt_res->msgid))) != LDAP_SUCCESS) {
	log_error(ZONE,"[xdb_ldap_jud_set] modification error : %s",ldap_err2string(rc));
	return 0;
    }

    /* wait for the operation to be terminated, poll every second */
    evt = pth_event(PTH_EVENT_FUNC, &xdbldap_wait_result, (void *) evt_res,
		    pth_time(1, 0));
    pth_wait(evt);

    /* ldap_msgfree(evt_res->result); */
    free(evt_res);
    for (j = 0; j < i; j++) { 
        free(tab_attrs_values[j][0]);
	free(tab_attrs_values[j]);
	free(ldap_attrs[j]); 
    } 
    free(tab_attrs_values);
    free(ldap_attrs); 
    
    log_debug(ZONE,"[xdbldap_jud_set] jud info successfully modified !");

    return 1;
}
