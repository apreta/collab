/**********************************************************************
 *  
 * This code was developped by IDEALX (http://IDEALX.org/) and
 * contributors (their names can be found in the AUTHORS file). It
 * is released under the terms of the JOSL license 
 * (http://www.opensource.org/licenses/jabberpl.html)
 *
 *********************************************************************/

#include "xdb_ldap.h"

static result xdb_ldap_phandler(instance i, dpacket p, void* args);
static short check_attr_value (xmlnode node, char *attr_name, char *attr_value);

static int module_call (XdbLdapDatas *self, XdbLdapConnList *ldap_conn, XdbLdapModule *mod, dpacket p);
static void xdb_ldap_shutdown (void *arg);

/*
 * Setup xdb_ldap, register handler.
 */
void xdb_ldap(instance i, xmlnode x)
{
    xdbcache xc = NULL; /* for config request */
    XdbLdapDatas *self = NULL;

    /* setup internal xdb_ldap structures */
    log_debug(ZONE,"[xdb_ldap] loading xdb_ldap");

    self = pmalloco(i->p,sizeof(*self));
    if (!self)
    {
	log_error(NULL,"[xdb_ldap] memory allocation failed");
	return;
    }
    self->poolref = i->p;

    /* Load config from xdb */
    xc = xdb_cache(i);
    self->config = xdb_get(xc, jid_new(xmlnode_pool(x), "config@-internal"),
			   "jabberd:xdb_ldap:config");

    /* Parse config */
    if (!xdbldap_config_init(self, self->config))
    {
        log_error(NULL,"[xdb_ldap] configuration failed");
	/* do no try to go on with a badly configured ldap component */
	exit(1);
    }

    register_phandler(i, o_DELIVER, xdb_ldap_phandler, (void*) self);
    register_shutdown(xdb_ldap_shutdown, (void*) self);
} /* end xdb_ldap */

static void xdb_ldap_shutdown (void *arg)
{
    XdbLdapDatas *self = (XdbLdapDatas *)arg;  
    /* sasl_conn_t *sasl_c; */

    log_debug(ZONE,"[xdb_ldap_shutdown] xdb_ldap is shutting down !");

    xdbldap_free_allconn();

    /* free admin connection */
    ldap_unbind(self->master_conn->ld);
    pool_free(self->master_conn->p);

	/* not automatically freed by the openldap library */
	/* but not in the external LDAP interface ... */
/*  	sasl_c = (sasl_conn_t *) conn_temp->ld->ld_defconn->lconn_sasl_ctx;  */
/*  	free(sasl_c->external.auth_id); */
}

/*
 * Handle xdb packets from jabberd.
 */
static result xdb_ldap_phandler(instance i, dpacket p, void* args)
{
    char           *user;
    char           *jid;
    char           *namespace;
    char           *passwd;
    const char     *type;
    XdbLdapDatas   *self = (XdbLdapDatas *)args;
    XdbLdapModule  *mod;
    XdbLdapConnList *cur_ldap;
    int                ret;
    int ldap_locally_alloc = 0;
	
    if ((p == NULL) || (p->id == NULL)) {
      log_error(ZONE,"[xdb_ldap_phandler] missing data in xdb_ldap_handler");
      return r_ERR;
    }
   
    user = spools(p->p, p->id->user, p->p);
    /* only used in auth:set requests, maybe move it ... */
    jid = spools(p->p, p->id->user, "@", p->id->server, p->p);
    
    namespace = xmlnode_get_attrib(p->x,"ns");

     /* user can be null when handling a jud request */
    if ((strcmp(namespace,"jabber:jud:users") != 0) && (user == NULL))
	return r_PASS;
    if (namespace == NULL)
	return r_PASS;
 

    log_debug(ZONE, "[xdb_ldap_phandler] got %s (user = %s) (jid = %s) (ns = %s)",
	      xmlnode2str(p->x), user, jid, namespace);

    if (p->x == NULL)
    {
	log_error(p->id->server,"[xdb_ldap_phandler] no xml node in dpacket %p", p);
	return r_ERR;
    }

    type = xmlnode_get_attrib(p->x, "type");
    if (type == NULL)
    {
	log_notice(ZONE, "[xdb_ldap_phandler] boucing unknown packet");
	return r_ERR;
    }
    if (strncmp(type, "error", 5) == 0)
    {
	log_notice(ZONE, "[xdb_ldap_phandler] dropping error packet");
	xmlnode_free(p->x);
	return r_DONE;
    }

    /*
     * retrieve or create an LDAP handle for this request 
     *        and pass it to module_call
     * for request from the jud, we use the master connection
     *        as there is no user ...
     */
    if ((strcmp(namespace,"jabber:jud:users") == 0) && (user == NULL)) {
	pool po = pool_new();

	cur_ldap = pmalloc(po,sizeof(XdbLdapConnList));
	cur_ldap->p = po;
	/* use master conn to LDAP */
	cur_ldap->ld = self->master_conn->ld;
	/* if it is a set, must know which user we have to modify */
	if (strncmp(type, "set", 3) == 0) {
	    char *temp = xmlnode_get_attrib(xmlnode_get_firstchild(p->x),"jid");
	    char *uname = strtok(temp,"@");
	    /* log_debug(ZONE,"got username : %s", uname); */
	    cur_ldap->binddn = pmalloc(cur_ldap->p,sizeof(char)*(strlen(self->base)+strlen(self->uniqattr)+strlen(uname)+3));
	    sprintf(cur_ldap->binddn,"%s=%s,%s",self->uniqattr,uname,self->base);
	}
	cur_ldap->jid = pmalloc(cur_ldap->p,sizeof(char)*strlen(jid));
	strcpy(cur_ldap->jid,jid);
	cur_ldap->exists = 1;
	ldap_locally_alloc = 1;
    } else if ((cur_ldap = xdbldap_get_conn(user)) == NULL) {
	/* no available conn for this user */
	/* we must first get its password then create a new LDAP conn */
	/* log_debug(ZONE, "[xdb_ldap_phandler] no existing conn for user %s", user); */
	passwd = xdb_ldap_getpasswd(self,user);
	if (passwd == NULL) {
	    /* user does not exist */
	    if ((strcmp(namespace,NS_AUTH) != 0)
		&& (strcmp(namespace,NS_AUTH_0K) != 0)) {
		/* if namespace is not auth, it's an error */
		log_error(ZONE,"[xdb_ldap_phandler] invalid namespace for unknown user !");
		return r_ERR;
	    } else if (check_attr_value(p->x,"type","get")) {
		/* if it's auth:0k:get or auth:get , immediately return */
		dpacket dp;

		xmlnode_put_attrib(p->x,"type","result");
		xmlnode_put_attrib(p->x,"to",xmlnode_get_attrib(p->x,"from"));
		xmlnode_put_attrib(p->x,"from",jid_full(p->id));
		dp = dpacket_new(p->x);
		if (dp) {
		    deliver(dp, NULL);
		    return r_DONE;
		} else
		    return r_ERR;

	    } else if (check_attr_value(p->x,"type","set")) {
		/* if it's auth:set, create the entry with the admin privileges */
		/* but we need to know user name ... */
		pool p = pool_new();

		log_debug(ZONE,"[xdb_ldap_phandler] setting an auth entry");

		cur_ldap = pmalloc(p,sizeof(XdbLdapConnList));
		cur_ldap->p = p;
		cur_ldap->ld = NULL;
		cur_ldap->next = NULL;
		cur_ldap->binddn = pmalloc(cur_ldap->p,sizeof(char)*(strlen(self->base)+strlen(self->uniqattr)+strlen(user)+3));
		sprintf(cur_ldap->binddn,"%s=%s,%s",self->uniqattr,user,self->base);
		cur_ldap->user = pmalloc(cur_ldap->p,sizeof(char)*(strlen(user)+1));
		cur_ldap->entry = pmalloc(cur_ldap->p,sizeof(char)*(strlen(self->uniqattr)+strlen(user)+2));
		sprintf(cur_ldap->entry,"%s=%s",self->uniqattr,user);
		strcpy(cur_ldap->user,user);
		cur_ldap->jid = pmalloc(cur_ldap->p,sizeof(char)*strlen(jid));
		strcpy(cur_ldap->jid,jid);
		cur_ldap->exists = 0;
		ldap_locally_alloc = 1;
	    }
	} else {
	    /* user exists, create its own LDAP connection */
	    /* but an user can exist whitout having an userPassword set */
	    /*   so that point must be checked */
	    /* XXX : is it really useful ??? when does it happen ??? */
	    if (strcmp(passwd,"") == 0) {
		/* user exists but has no password */
		pool p = pool_new();

		log_debug(ZONE,"[xdb_ldap_phandler] setting an auth entry");

		cur_ldap = pmalloc(p,sizeof(XdbLdapConnList));
		cur_ldap->p = p;
		cur_ldap->ld = NULL;
		cur_ldap->next = NULL;
		cur_ldap->binddn = pmalloc(cur_ldap->p, sizeof(char)*(strlen(self->base)+strlen(self->uniqattr)+strlen(user)+3));
		sprintf(cur_ldap->binddn,"%s=%s,%s",self->uniqattr,user,self->base);
		cur_ldap->user = pmalloc(cur_ldap->p,sizeof(char)*(strlen(user)+1));
		cur_ldap->entry = pmalloc(cur_ldap->p,sizeof(char)*(strlen(self->uniqattr)+strlen(user)+2));
		sprintf(cur_ldap->entry,"%s=%s",self->uniqattr,user);
		strcpy(cur_ldap->user,user);
		cur_ldap->jid = pmalloc(cur_ldap->p,sizeof(char)*strlen(jid));
		strcpy(cur_ldap->jid,jid);
		cur_ldap->exists = 1;
		ldap_locally_alloc = 1;
	    } else {
		/* if request is an get request on auth namespace
		 * no need to create a conn, we already know the
		 * user password !!
		 * by BO (borihuela@IDEALX.com), 2001/08/30 
		 */
		if ((strcmp(namespace,NS_AUTH) == 0)
		    && (check_attr_value(p->x,"type","get"))) {
		    dpacket dp;
		    /* build the result set */
		    xmlnode data = xmlnode_new_tag("password");

		    log_debug(ZONE,"[xdb_ldap_phandler] ok, it's an auth get request, no need to create a user conn");

		    xmlnode_insert_cdata(data, passwd, -1);

		    xmlnode_put_attrib(p->x,"type","result");
		    xmlnode_put_attrib(p->x,"to",xmlnode_get_attrib(p->x,"from"));
		    xmlnode_put_attrib(p->x,"from",jid_full(p->id));
		    xmlnode_insert_tag_node(p->x, data);
		    xmlnode_free(data);
		    free(passwd);
		    dp = dpacket_new(p->x);
		    if (dp) {
			deliver(dp, NULL);
			return r_DONE;
		    } else
			return r_ERR;
		} else {
		    /* for other requests, create an LDAP connection
		     * as usual ...
		     */
		    char *binddn = (char *) malloc(sizeof(char)*(strlen(self->base)+strlen(self->uniqattr)+strlen(user)+3));
		    sprintf(binddn,"%s=%s,%s",self->uniqattr,user,self->base);

		    cur_ldap = xdbldap_create_conn(self->host,self->port,binddn,user,passwd,1);
		    if (cur_ldap == NULL) {
			log_error(ZONE,"[xdb_ldap_phandler] unable to create a new LDAP conn !");
			free(passwd);
			return r_ERR;
		    }
		    cur_ldap->jid = pmalloc(cur_ldap->p,sizeof(char)*strlen(jid));
		    strcpy(cur_ldap->jid,jid);
		    free(passwd);
		    free(binddn);
		}
	    }
	}
    } 

    /* find module to dispatch according to resource/namespace */
    for (mod = self->modules; mod->namespace != NULL; mod++)
    {
	if (strcmp(namespace, mod->namespace) == 0)
	{
	    log_debug(ZONE, "[xdb_ldap_phandler] dispatching ldap request for (%s, %s)", user, namespace);

	    ret = module_call(self, cur_ldap, mod, p);

	    if (ldap_locally_alloc == 1) {
		pool_free(cur_ldap->p);
	    }

	    return ret;
	}
    }

    log_warn(ZONE, "NOT HANDLED (xdb_ldap) request (user=%s ns=%s) (%s)\n",
	      user, namespace, xmlnode2str(p->x));
    
    return r_PASS;
}

static int module_call(XdbLdapDatas *self, XdbLdapConnList *ldap_conn, XdbLdapModule *mod, dpacket p)
{
    short is_set;
    xmlnode data = NULL;
    dpacket dp;

    is_set = check_attr_value(p->x, "type", "set");

    if (is_set) {
	/*
	 * The xml node is surrounded by an xdb element. Get rid of it,
	 * by taking the first child.
	 */
	xmlnode child = xmlnode_get_firstchild(p->x);
	
	if ((mod->set)(self, ldap_conn, child) == 0)
	    return r_ERR;
    } else {
	data = (mod->get)(self, ldap_conn);
	if (data) {
	    xmlnode_insert_tag_node(p->x, data);
	    xmlnode_free(data);
	}
    }

    /* reply */
    xmlnode_put_attrib(p->x,"type","result");
    xmlnode_put_attrib(p->x,"to",xmlnode_get_attrib(p->x,"from"));
    xmlnode_put_attrib(p->x,"from",jid_full(p->id));

    dp = dpacket_new(p->x);
    if (dp)
    {
	deliver(dp, NULL);
	return r_DONE;
    }
    else
	return r_ERR;
}

static short check_attr_value (xmlnode node, char *attr_name, char *value)
{
    char *attr_value;

    if ((node == NULL) || (attr_name == NULL) || (value == NULL))
	return 0;

    attr_value = xmlnode_get_attrib(node, attr_name);
    if (strcmp(attr_value, value) == 0)
	return 1;
    else
	return 0;
}


