/**********************************************************************
 *  
 * This code was developped by IDEALX (http://IDEALX.org/) and
 * contributors (their names can be found in the AUTHORS file). It
 * is released under the terms of the JOSL license 
 * (http://www.opensource.org/licenses/jabberpl.html)
 *
 *********************************************************************/

#include "xdb_ldap.h"

static int xdbldap_auth_set_new(XdbLdapDatas *self, XdbLdapConnList *curr_conn,	
				char *password);
static int xdbldap_auth_set_mod(XdbLdapDatas *self, XdbLdapConnList *curr_conn, 
				char *password);
static int xdbldap_auth_disable(XdbLdapDatas *self, XdbLdapConnList *curr_conn);

xmlnode xdbldap_auth_get(XdbLdapDatas *self, XdbLdapConnList *curr_conn)
{
    xmlnode data = NULL;    /* return data from this function */
    char *str;
    
    str = xdb_ldap_getpasswd(self, curr_conn->user);

    if (str == NULL) {
	log_error(ZONE,"[xdbldap_auth_get] unable to retrieve user password !");
	return NULL;
    }

    /* build the result set */
    data = xmlnode_new_tag("password");
    xmlnode_insert_cdata(data, str, -1);
    free(str);

    return data;
} /* end xdbldap_auth_get */

int xdbldap_auth_set_mod(XdbLdapDatas *self, XdbLdapConnList *curr_conn, char *password)
{
    LDAPMod           **attrs;
    int                    rc;
    XdbLdapEvtResult *evt_res;
    pth_event_t evt;
    int i;
    char *tab_pass[2] = { password, NULL};

    /* log_debug(ZONE,"[xdbldap_auth_set_mod] modifying userPassword for %s", curr_conn->user); */

    attrs = (LDAPMod **) malloc(2 * sizeof(LDAPMod *)); 
    if (attrs == NULL) { 
	log_error(ZONE,"[xdb_ldap_auth_mod] unable to allocate memory"); 
	return -1; 
    } 
    for (i = 0; i < 1; i++) { 
	if ((attrs[i] = (LDAPMod *) malloc(sizeof(LDAPMod))) == NULL) { 
	    log_error(ZONE,"[xdb_ldap_auth_mod] unable to allocate memory");
	    return -1; 
	} 
    } 	
 
    attrs[0]->mod_type = "userPassword";
    attrs[0]->mod_op = LDAP_MOD_REPLACE;
    attrs[0]->mod_values = tab_pass;

    attrs[1] = NULL;

    /* to wait for the results */
    evt_res = (XdbLdapEvtResult *) malloc(sizeof(XdbLdapEvtResult));
    if (curr_conn->ld == NULL) {
	/* user exists but jabber doesn't yet send its password */
	evt_res->ld = self->master_conn->ld;
    } else {
	evt_res->ld = curr_conn->ld;
    }

    /* log_debug(ZONE,"[xdbldap_auth_0k_set_mod] modifying dn : %s", curr_conn->binddn); */

    if ((evt_res->rc = ldap_modify_ext(evt_res->ld, curr_conn->binddn, 
				attrs, NULL, NULL, &(evt_res->msgid))) != LDAP_SUCCESS) {
	log_error(ZONE,"[xdbldap_auth_set_mod] modification error : %s",ldap_err2string(rc));
	return 0;
    }

    /* wait for the operation to be terminated, poll every second */
    evt = pth_event(PTH_EVENT_FUNC, &xdbldap_wait_result, (void *) evt_res,
		    pth_time(1, 0));
    pth_wait(evt);

    ldap_msgfree(evt_res->result);  
    free(evt_res);
    for (i = 0; i < 1; i++) { 
	free(attrs[i]); 
    } 
    free(attrs); 
    
    log_debug(ZONE,"[xdbldap_auth_set_mod] userPassword successfully modified !");

    return 1;
}

int xdbldap_auth_set_new(XdbLdapDatas *self, XdbLdapConnList *curr_conn, char *password)
{
    LDAPMod          **attrs;
    int                rc,i;
    XdbLdapEvtResult  *evt_res;
    pth_event_t        evt;

    char *tab_pass[2] = {password, NULL};
    char *tab_uniqattr[2] = {curr_conn->user, NULL};
    char *tab_jid[2] = {curr_conn->jid, NULL};
    char *tab_oc[2] = {"jabberuser", NULL};
    char *tab_val[2] = {"1", NULL};

    /* log_debug(ZONE,"[xdb_ldap_auth_set_new] setting cn : %s / password : %s for entry : %s", curr_conn->user, password, curr_conn->binddn); */

    /* Construct the array of LDAPMod structures representing the attributes 
       of the new entry. */ 
    attrs = (LDAPMod **) malloc(7 * sizeof(LDAPMod *)); 
    if (attrs == NULL) { 
	log_error(ZONE,"[xdb_ldap_auth_set_new] unable to allocate memory"); 
	return -1; 
    } 
    for (i = 0; i < 6; i++) { 
	if ((attrs[i] = (LDAPMod *) malloc(sizeof(LDAPMod))) == NULL) { 
	    log_error(ZONE,"[xdb_ldap_auth_set_new] unable to allocate memory");
	    return -1; 
	} 
    } 	

    attrs[0]->mod_op = LDAP_MOD_ADD; 
    attrs[0]->mod_type = "userPassword";
    attrs[0]->mod_values = tab_pass;

    attrs[1]->mod_op = LDAP_MOD_ADD; 
    attrs[1]->mod_type = self->uniqattr;
    attrs[1]->mod_values = tab_uniqattr;

    /* by defaut, set cn = sn as we must provide a sn to create an entry */
    attrs[4]->mod_op = LDAP_MOD_ADD; 
    attrs[4]->mod_type = "sn";
    attrs[4]->mod_values = tab_uniqattr;

    attrs[3]->mod_op = LDAP_MOD_ADD; 
    attrs[3]->mod_type = "objectClass";
    attrs[3]->mod_values = tab_oc;
	
    /* currently unused. should indicate if a account is activated or not */
    attrs[2]->mod_op = LDAP_MOD_ADD; 
    attrs[2]->mod_type = "valid";
    attrs[2]->mod_values = tab_val;
	
    attrs[5]->mod_op = LDAP_MOD_ADD; 
    attrs[5]->mod_type = "jid";
    attrs[5]->mod_values = tab_jid;

    attrs[6] = NULL;

    /* to wait for the results */
    evt_res = (XdbLdapEvtResult *) malloc(sizeof(XdbLdapEvtResult));
    evt_res->ld = self->master_conn->ld;
	
    if ((evt_res->rc = ldap_add_ext(evt_res->ld, curr_conn->binddn, attrs, NULL, NULL, &(evt_res->msgid))) != LDAP_SUCCESS) {
	log_error(ZONE,"[xdbldap_auth_set_new] modification error : %s",
		  ldap_err2string(rc));
	return 0;
    }

    /* wait for the operation to be terminated, poll every second */
    evt = pth_event(PTH_EVENT_FUNC, &xdbldap_wait_result, (void *) evt_res,
		    pth_time(1, 0));
    pth_wait(evt);
    ldap_msgfree(evt_res->result);  
    free(evt_res);    

    for (i = 0; i < 6; i++) { 
	free(attrs[i]); 
    } 
    free(attrs); 
    log_debug(ZONE,"[xdb_ldap_auth_set_new] user successfully added in LDAP");

    return 1;
}

int xdbldap_auth_disable(XdbLdapDatas *self, XdbLdapConnList *curr_conn)
{
  log_debug(ZONE,"[xdbldap_auth_disable] NOT YET IMPLEMENTED !");
  return 1;
}

int xdbldap_auth_set(XdbLdapDatas *self, XdbLdapConnList *curr_conn, xmlnode data)
{
    char *password = NULL;   /* the password set by the user */

    if (data) {
	if (j_strcmp(xmlnode_get_name(data),"password") != 0) { 
	    log_error(NULL,"[xdbldap_auth_set] no <password> found");
	    return 0;
	} else {
	    password = xmlnode_get_data(xmlnode_get_firstchild(data));
	    if (!password)
		return 1;
	}

	/* Figure out which query we want to use. */
	if ((curr_conn->ld == NULL) && (curr_conn->exists == 0)) {
	    /* we did not provide an user ldap conn => user does not exist */
	    log_debug(ZONE,"[xdbldap_auth_set] now gonna add an new user in LDAP"); 
	    return xdbldap_auth_set_new(self, curr_conn, password);
	} else {
	    log_debug(ZONE,"[xdbldap_auth_set] now gonna mod an user in LDAP"); 
	    return xdbldap_auth_set_mod(self, curr_conn, password);
	}
    }
    else  /* we're removing the authentication info */
      return xdbldap_auth_disable(self, curr_conn);

    return 1;
}


