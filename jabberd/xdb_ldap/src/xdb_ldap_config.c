/**********************************************************************
 *  
 * This code was developped by IDEALX (http://IDEALX.org/) and
 * contributors (their names can be found in the AUTHORS file). It
 * is released under the terms of the JOSL license 
 * (http://www.opensource.org/licenses/jabberpl.html)
 *
 *********************************************************************/

#include "xdb_ldap.h"

/*
 * Bind a namespace to set/get functions.
 */
static const XdbLdapModule static_modules[] =
{
    { NS_AUTH,     xdbldap_auth_set,       xdbldap_auth_get     },
    { NS_AUTH_0K,  xdbldap_auth_0k_set,    xdbldap_auth_0k_get  },
    { NS_VCARD,    xdbldap_vcard_set,      xdbldap_vcard_get    },
    { NS_ROSTER,   xdbldap_roster_set,     xdbldap_roster_get   },
    { "jabber:jud:users", xdbldap_jud_set, xdbldap_jud_get      },
    { NULL,        NULL,                   NULL                 }
};

int xdbldap_config_init(XdbLdapDatas *self, xmlnode cfgroot)
{
    xmlnode conn_base, tmp;                          /* pointers into the node tree */
    const char *portstr = NULL;                      /* LDAP port number */
    XdbLdapConnList *ldap_conn;

    if (!cfgroot)
        return 0;  /* configuration not present */

    /* Have a look in the XML configuration data for connection information. */
    conn_base = xmlnode_get_tag(cfgroot,"connection");
    if (conn_base)
    { 
	if ((tmp = xmlnode_get_tag(conn_base,"host")) != NULL) 
	    self->host = (char *) xmlnode_get_data(xmlnode_get_firstchild(tmp));
	else {
	    log_error(ZONE,"[xdb_ldap_config_init] no host specified");
	    return 0;
	}
        if ((tmp = xmlnode_get_tag(conn_base,"port")) != NULL) {
	    portstr = (char *) xmlnode_get_data(xmlnode_get_firstchild(tmp));
	    if (portstr) {
		self->port = atoi(portstr);
	    }
	    else {
	      self->port = LDAP_PORT;
	    }
	} else {
	    self->port = LDAP_PORT;
	}

        if ((tmp = xmlnode_get_tag(conn_base,"suffix")) != NULL)
	    self->base = (char *) xmlnode_get_data(xmlnode_get_firstchild(tmp));
	else {
	    log_error(ZONE,"[xdb_ldap_config_init] no suffix specified");
	    return 0;
	}

        if ((tmp = xmlnode_get_tag(conn_base,"abook_suffix")) != NULL)
	    self->abook_base = (char *) xmlnode_get_data(xmlnode_get_firstchild(tmp));
	else {
	    log_debug(ZONE,"[xdb_ldap_config_init] no address book suffix specified, rosters won't be managed by me");
	}

        if ((tmp = xmlnode_get_tag(conn_base,"uniqattr")) != NULL)
	    self->uniqattr = (char *) xmlnode_get_data(xmlnode_get_firstchild(tmp));
	else {
	    log_error(ZONE,"[xdb_ldap_config_init] no unique attr specified");
	    return 0;
	}

        if ((tmp = xmlnode_get_tag(conn_base,"binddn")) != NULL)
	    self->binddn = (char *) xmlnode_get_data(xmlnode_get_firstchild(tmp));
	else {
	    log_error(ZONE,"[xdb_ldap_config_init] no master dn specified");
	    return 0;
	}

        if ((tmp = xmlnode_get_tag(conn_base,"bindpw")) != NULL)
	    self->bindpw = (char *) xmlnode_get_data(xmlnode_get_firstchild(tmp));
	else {
	    log_error(ZONE,"[xdb_ldap_config_init] no pw specified for master conn");
	    return 0;
	}

     } /* end if */

    /* log_debug(ZONE,"[xdb_ldap_config_init] rootdn set to %s", self->base); */

    self->modules = (struct XdbLdapModule *) pmalloc(self->poolref,
						     sizeof(static_modules));
    memcpy(self->modules, static_modules, sizeof(static_modules));

    /* Get a handle to an LDAP connection. */ 
    log_debug(ZONE,"[xdb_ldap_config_init] hostname : %s / port : %d", 
	      self->host, self->port);

    if ((ldap_conn = xdbldap_create_conn(self->host, self->port, self->binddn, "jabberadmin", self->bindpw, 0)) == NULL) {
	log_error(ZONE,"[xdb_ldap_config_init] unable to create a new LDAP connection");
	return 0;
    }

    self->master_conn = ldap_conn;

    log_debug(ZONE,"[xdb_ldap_config_init] successfull initialisation");

    return 1; 
} /* end xdbldap_config_init */



