/**********************************************************************
 *  
 * This code was developped by IDEALX (http://IDEALX.org/) and
 * contributors (their names can be found in the AUTHORS file). It
 * is released under the terms of the JOSL license 
 * (http://www.opensource.org/licenses/jabberpl.html)
 *
 *********************************************************************/

/*
 * connection management : if an user has no connection, a new one is created
 *    and the binding is done with its dn and userPassword.
 *    then the connection is added in the local hashtable
 *    a monitor thread checks the connections which are older than 10s and 
 *    deletes them. that mainly permits to make the authentication 
 *    process (4 requests) with the same LDAP connection.
 */

#include "xdb_ldap.h"

/* linked list of all currently active LDAP connections */
xht global_conn_list = NULL;

/* unbind and free the connection owned by user */
static void xdbldap_free_conn(xht h, const char *user, void *val);
/* add a connection to the hashtable */
static void xdbldap_add_conn(XdbLdapConnList *ldap_conn);
/* walker function used to unbind and free all the LDAP connections */
static void xdbldap_free_callback(xht h, const char *key, void *val, void *arg);
/* walker function used to unbind and free an expired connection */
static void xdbldap_free_expired(xht h, const char *key, void *val, void *arg);
/* periodic function which expunge expired functions */
static void xdbldap_check_func(void *arg);

XdbLdapConnList *xdbldap_get_conn(const char *user)
{
    XdbLdapConnList *ldap_conn;

    if ((ldap_conn = xhash_get(global_conn_list,user)) == NULL) {
	return NULL;
    } else {
	return ldap_conn;
    }
}

XdbLdapConnList *xdbldap_create_conn(char *host, int port, const char *binddn, 
				     const char *user, const char *passwd, int add_in_list)
{
    LDAP *ld;
    XdbLdapConnList *ldap_conn;
    int version;                                     /* LDAP protocol version */
    int rc;
    char *tmp_str,*ptr;
    pool p;

    log_debug(ZONE,"[xdbldap_create_conn] gonna create a new LDAP handler for %s",binddn);

    if ((ld = ldap_init(host,port)) == NULL ) { 
	log_error(ZONE,"[xdb_ldap_create_conn] unable to init LDAP"); 
	return NULL; 
    } 

    /* Set the LDAP protocol version supported by the client 
     * to 3. (By default, this is set to 2. SASL authentication 
     * is part of version 3 of the LDAP protocol.) */ 
    version = LDAP_VERSION3; 
    ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, &version);

#ifdef WITH_TLS
    if ((rc = ldap_start_tls_s(ld,NULL,NULL)) != LDAP_SUCCESS) {
	log_error(ZONE,"[xdb_ldap_create_conn] error : %s", ldap_err2string(rc));
	return NULL;
    } 
#endif

    /* Bind to the LDAP server. */ 
    /* log_debug(ZONE,"[xdbldap_create_conn] binding with %s/%s", binddn, passwd); */
    if ((rc = ldap_simple_bind_s(ld, binddn, passwd)) != LDAP_SUCCESS) {
	log_error(ZONE,"[xdb_ldap_create_conn] error : %s", ldap_err2string(rc));
	return NULL;
    } 

    /* Add it in the main list */
    p = pool_new();
    ldap_conn = pmalloc(p,sizeof(XdbLdapConnList));
    ldap_conn->p = p;
    ldap_conn->ld = ld;
    ldap_conn->binddn = pmalloc(ldap_conn->p,sizeof(char)*(strlen(binddn)+1));
    strcpy(ldap_conn->binddn,binddn);
    /* tmp_str = pmalloc(ldap_conn->p,sizeof(char)*(strlen(binddn)+1)); */
    tmp_str = (char *) malloc(sizeof(char)*(strlen(binddn)+1));
    strcpy(tmp_str,binddn);
    ptr = strtok(tmp_str,",");
    /* fprintf(stderr,"[xdbldap_create_conn] *** tmp_str : %s",tmp_str); */
    ldap_conn->entry = pmalloc(ldap_conn->p,sizeof(char)*(strlen(tmp_str)+1));
    strcpy(ldap_conn->entry,tmp_str);
    ldap_conn->user = pmalloc(ldap_conn->p,sizeof(char)*(strlen(user)+1));
    strcpy(ldap_conn->user,user);
    ldap_conn->creation_time = time(NULL);
    ldap_conn->exists = 1;
    if (add_in_list == 1)
	xdbldap_add_conn(ldap_conn);

    free(tmp_str);
    return ldap_conn;
}

void xdbldap_free_callback(xht h, const char *key, void *val, void *arg)
{
  xdbldap_free_conn(h,key,val);
}

void xdbldap_free_allconn()
{
  xhash_walk(global_conn_list,xdbldap_free_callback,NULL);
  xhash_free(global_conn_list);
}

static void xdbldap_free_conn(xht h, const char *user, void *val)
{
  XdbLdapConnList *temp_conn = (XdbLdapConnList *) val;

  log_debug(ZONE,"[xdbldap_free_conn] freeing %s LDAP connection", user);

  xhash_zap(h,user);
  ldap_unbind(temp_conn->ld);
  pool_free(temp_conn->p);
}

static void xdbldap_free_expired(xht h, const char *key, void *val, void *arg)
{
    XdbLdapConnList *temp_conn = (XdbLdapConnList *) val;

    /* log_debug(ZONE,"[xdbldap_free_expired] checking %s (%d)",key,(int) (time(NULL) - temp_conn->creation_time)); */

    /* kill connections older than 10s */
    if ((time(NULL) - temp_conn->creation_time) > (time_t) 50) {
	xdbldap_free_conn(h,key,val);
    }
}


static void xdbldap_check_func(void *arg)
{
    log_debug(ZONE,"[xdbldap_check_func] monitor thread enters its main loop");

    while (1) {
      /* special case : when it is called and xdb_ldap is shutting down
       * and that global_conn_list has already been deallocated
       * added by BO, 2001/08/31
       */
      if (global_conn_list != NULL)
	xhash_walk(global_conn_list,xdbldap_free_expired,NULL);
      pth_sleep(10);
    }

}
static void xdbldap_add_conn(XdbLdapConnList *ldap_conn)
{
    pth_attr_t attr;

    /* log_debug(ZONE,"[xdbldap_add_conn] adding LDAP conn for %s", ldap_conn->user); */

    if (global_conn_list == NULL) {

	log_debug(ZONE,"[xdbldap_add_conn] no existing hashtable for conns");

	global_conn_list = xhash_new(509);

	/* spawn the thread which deletes expired connections */
	attr = pth_attr_new();
	pth_attr_set(attr, PTH_ATTR_JOINABLE, FALSE);
	pth_spawn(attr, (void *) xdbldap_check_func, NULL);
	pth_attr_destroy(attr);
    }

    xhash_put(global_conn_list,ldap_conn->user,(void *) ldap_conn);
}
