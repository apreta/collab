/**********************************************************************
 *  
 * This code was developped by IDEALX (http://IDEALX.org/) and
 * contributors (their names can be found in the AUTHORS file). It
 * is released under the terms of the JOSL license 
 * (http://www.opensource.org/licenses/jabberpl.html)
 *
 *********************************************************************/

#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <time.h> 
#include "ldap.h" 

int 
main( int argc, char **argv ) 
{ 
    LDAP        *ld; 
    LDAPMod     mod0; 
    LDAPMod     mod1; 
    LDAPMod     *mods[ 3 ]; 
    char        *vals0[ 2 ]; 
    char        *vals1[ 2 ]; 
    time_t      now; 
    char        buf[ 128 ]; 
    struct berval   cred; 
    struct berval   *servcred; 
    int         version; 

    /* get a handle to an LDAP connection */ 
    if ( (ld = ldap_init( "localhost", 389 )) == NULL ) { 
	perror( "ldap_init" ); 
	return( 1 ); 
    } 

    /* Set the LDAP protocol version supported by the client 
       to 3. (By default, this is set to 2. SASL authentication 
       is part of version 3 of the LDAP protocol.) */ 
    version = LDAP_VERSION3; 
    ldap_set_option( ld, LDAP_OPT_PROTOCOL_VERSION, &version ); 

    /* authenticate */ 
    cred.bv_val = "magic"; 
    cred.bv_len = sizeof( "magic" ) - 1; 
    if ( ldap_sasl_bind_s( ld, "cn=jabberadmin,dc=idx-jabber,dc=org", NULL, &cred, NULL, NULL, &servcred ) != LDAP_SUCCESS ) { 
	ldap_perror( ld, "ldap_sasl_bind_s" ); 
	return( 1 ); 
    } 
    /* get and print the credentials returned by the server */ 
    printf( "Server credentials: %s\n", servcred->bv_val ); 
    /* construct the list of modifications to make */ 
/*      mod0.mod_op = LDAP_MOD_REPLACE;  */
/*      mod0.mod_type = "mail";  */
/*      vals0[0] = "babs@airius.com";  */
/*      vals0[1] = NULL;  */
/*      mod0.mod_values = vals0;  */
/*      mod1.mod_op = LDAP_MOD_ADD;  */
/*      mod1.mod_type = "description";  */
/*      time( &now );  */
/*      sprintf( buf, "This entry was modified with the modattrs program on %s",  */
/*  	     ctime( &now ));  */
    /* Get rid of \n which ctime put on the end of the time string */ 
/*      if ( buf[ strlen( buf ) - 1 ] == '\n' ) {  */
/*  	buf[ strlen( buf ) - 1 ] = '\0';  */
/*      }  */
/*      vals1[ 0 ] = buf;  */
/*      vals1[ 1 ] = NULL;  */
/*      mod1.mod_values = vals1;  */
/*      mods[ 0 ] = &mod0;  */
/*      mods[ 1 ] = &mod1;  */
/*      mods[ 2 ] = NULL;  */
    /* make the change */ 
/*      if ( ldap_modify_s( ld, "uid=bjensen,ou=people,o=airius.com", mods )  */
/*  	 != LDAP_SUCCESS ) {  */
/*  	ldap_perror( ld, "ldap_modify_s" );  */
/*  	return( 1 );  */
/*      }  */
    ldap_unbind( ld ); 
    printf( "modification was successful\n" ); 
    return( 0 ); 
} 

