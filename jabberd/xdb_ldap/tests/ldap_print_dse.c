/**********************************************************************
 *  
 * This code was developped by IDEALX (http://IDEALX.org/) and
 * contributors (their names can be found in the AUTHORS file). It
 * is released under the terms of the JOSL license 
 * (http://www.opensource.org/licenses/jabberpl.html)
 *
 *********************************************************************/

#include <ldap.h>
#include <stdio.h>

int main() 
{ 
    int rc, i; 
    LDAPMessage  *result, *e; 
    BerElement  *ber; 
    LDAP *ld;
    char    *a; 
    char    **vals; 
    char    *attrs[4]; 
    int version;

    /* get a handle to an LDAP connection */ 
    if ( (ld = ldap_init( "localhost", 389 )) == NULL ) { 
	perror( "ldap_init" ); 
	return( 1 ); 
    } 

    /* Set the LDAP protocol version supported by the client 
       to 3. (By default, this is set to 2. SASL authentication 
       is part of version 3 of the LDAP protocol.) */ 
    version = LDAP_VERSION3; 
    ldap_set_option( ld, LDAP_OPT_PROTOCOL_VERSION, &version ); 
    ldap_set_option( ld, LDAP_OPT_REFERRALS, LDAP_OPT_OFF );

    /* Search for the root DSE. */ 
    attrs[0] = "supportedControl"; 
    attrs[1] = "supportedExtension"; 
    attrs[2] = "supportedSASLMechanisms";
    attrs[3] = NULL; 
    rc = ldap_search_ext_s( ld, "cn=jabberadmin,dc=jabber,dc=org", LDAP_SCOPE_BASE, "(objectclass=*)", attrs, 0, NULL, NULL, NULL, 0, &result ); 
    /* Check the search results. */ 
    switch( rc ) { 
	/* If successful, the root DSE was found. */ 
    case LDAP_SUCCESS: 
	break; 
	/* If the root DSE was not found, the server does not comply 
	   with the LDAPv3 protocol. */ 
    case LDAP_PARTIAL_RESULTS: 
    case LDAP_NO_SUCH_OBJECT: 
    case LDAP_OPERATIONS_ERROR: 
    case LDAP_PROTOCOL_ERROR: 
	printf( "LDAP server returned result code %d (%s).\n" 
		"This server does not support the LDAPv3 protocol.\n", 
		rc, ldap_err2string( rc ) ); 
	return( 1 ); 
	/* If any other value is returned, an error must have occurred. */ 
    default: 
	fprintf( stderr, "ldap_search_ext_s: %s\n", ldap_err2string( rc ) ); 
	return( 1 ); 
    } 
    /* Since only one entry should have matched, get that entry. */ 
    e = ldap_first_entry( ld, result ); 
    if ( e == NULL ) { 
	fprintf( stderr, "ldap_search_ext_s: Unable to get root DSE.\n"); 
	ldap_memfree( result ); 
	return( 1 ); 
    } 
          
    /* Iterate through each attribute in the entry. */ 
    for ( a = ldap_first_attribute( ld, e, &ber ); 
	  a != NULL; a = ldap_next_attribute( ld, e, ber ) ) { 
              
	/* Print each value of the attribute. */ 
	if ((vals = ldap_get_values( ld, e, a)) != NULL ) { 
	    for ( i = 0; vals[i] != NULL; i++ ) { 
		printf( "%s: %s\n", a, vals[i] ); 
	    } 
              
	    /* Free memory allocated by ldap_get_values(). */ 
	    ldap_value_free( vals ); 
	} 
            
	/* Free memory allocated by ldap_first_attribute(). */ 
	ldap_memfree( a ); 
    } 
          
    /* Free memory allocated by ldap_first_attribute(). */ 
    if ( ber != NULL ) { 
	ber_free( ber, 0 ); 
    } 
          
    printf( "\n" ); 
    /* Free memory allocated by ldap_search_ext_s(). */ 
    ldap_msgfree( result ); 
    ldap_unbind( ld ); 
    return( 0 ); 
} 
