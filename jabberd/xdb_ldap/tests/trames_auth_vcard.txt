
		Trames � utiliser pour sc�narios de tests
		-----------------------------------------

Authentification
----------------

G : <stream:stream to='localhost' xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams'>
J : <?xml version='1.0'?><stream:stream xmlns:stream='http://etherx.jabber.org/streams' id='3AACA7D8' xmlns='jabber:client' from='localhost'>
G : <iq id='A0' type='get'><query xmlns='jabber:iq:auth'><username>epavausore</username></query></iq>
J : <iq id='A0' type='result'><query xmlns='jabber:iq:auth'><username>epavausore</username><sequence>484</sequence><token>3AA8B3AC</token><resource/></query></iq>
G : <iq id='A1' type='set'><query xmlns='jabber:iq:auth'><username>epavausore</username><resource>ImpLs</resource><hash>43f7a334198c311e0beed06d9f7da79eb6480ffd</hash></query></iq>
J : <iq id='A1' type='result'/>

Interm�diaire utile ...
-----------------------

G : <iq type='get'><query xmlns='jabber:iq:roster'/></iq>
G : <presence/>

VCard
-----

* get

G : <iq id='A2' to='epavausore@localhost' type='get'><vCard prodid='-//HandGen//NONSGML vGen v1.0//EN' version='3.0' xmlns='vcard-temp'/></iq>
J : <iq id='A2' type='result' from='epavausore@localhost/ImpLs'><vCard prodid='-//HandGen//NONSGML vGen v1.0//EN' version='3.0' xmlns='vcard-temp'><N><FAMILY>epavausore</FAMILY><GIVEN>enapas</GIVEN></N><NICKNAME>epavausore</NICKNAME><URL>http://yop.tm.fr</URL><ADR><STREET>pas de rue</STREET><LOCALITY>pas de ville</LOCALITY><REGION>anarchia</REGION><PCODE>54343</PCODE><COUNTRY>espagne</COUNTRY></ADR><TEL>43532523653</TEL><EMAIL>bori@nulpart.fr</EMAIL><FN>epavausoreenapas</FN></vCard></iq>

* set

G : <iq id='A4' type='set'><vCard prodid='-//HandGen//NONSGML vGen v1.0//EN' version='3.0' xmlns='vcard-temp'><FN>epave master</FN><N><FAMILY>master</FAMILY><GIVEN>epave</GIVEN></N><NICKNAME>epavemaster</NICKNAME><URL>http://yop.tm.fr</URL><ADR><STREET>des paiens</STREET><EXTADD></EXTADD><LOCALITY>paris</LOCALITY><REGION>euh</REGION><PCODE>54343</PCODE><COUNTRY>france</COUNTRY></ADR><TEL>43532523653</TEL><EMAIL>bori@nulpart.com</EMAIL><ORG><ORGNAME></ORGNAME><ORGUNIT></ORGUNIT></ORG><TITLE></TITLE><ROLE></ROLE><DESC></DESC><BDAY>2001-03-12</BDAY></vCard></iq>
J : <iq id='A4' type='result' from='epavausore@localhost/ImpLs' to='epavausore@localhost/ImpLs'/>
