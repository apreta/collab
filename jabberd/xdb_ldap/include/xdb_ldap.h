/**********************************************************************
 *  
 * This code was developped by IDEALX (http://IDEALX.org/) and
 * contributors (their names can be found in the AUTHORS file). It
 * is released under the terms of the JOSL license 
 * (http://www.opensource.org/licenses/jabberpl.html)
 *
 *********************************************************************/

#ifndef XDB_LDAP_H
#define XDB_LDAP_H

/* #include <mpatrol.h> */
#include <jabberd.h>
#include <ldap.h>

/* #define WITH_TLS */
#undef WITH_TLS

/*
 * Type definitions
 */

struct XdbLdapDatas;
struct XdbLdapRequest;
struct XdbLdapConnList;

/* prototype of namespaces get/set functions callback */
typedef int (*XdbLdapSetFunc)(struct XdbLdapDatas *self, 
			      struct XdbLdapConnList *curr_conn, xmlnode datas);
typedef xmlnode (*XdbLdapGetFunc)(struct XdbLdapDatas *self, 
				  struct XdbLdapConnList *curr_conn);

/* vcard <-> LDAP mapping functions */
typedef struct XdbLdapRequest* (*XdbVcard2LdapSetFunc)(struct XdbLdapRequest *cur, xmlnode x);
typedef xmlnode (*XdbLdap2VcardSetFunc)(char *attr, char **vals, xmlnode x); 

/* Associate a namespace with functions to set/get datas
 */
typedef struct XdbLdapModule
{
    const char *namespace;
    XdbLdapSetFunc set;
    XdbLdapGetFunc get;
} XdbLdapModule;

/* mapping from Vcard to LDAP */
typedef struct XdbVcard2Ldap
{
    const char *tagname;
    XdbVcard2LdapSetFunc set;
} XdbVcard2Ldap;

/* mapping from LDAP to Vcard */
typedef struct XdbLdap2Vcard
{
    const char *attrname;
    XdbLdap2VcardSetFunc set;
} XdbLdap2Vcard;

/* to retrieve results from asynchronous requests */
typedef struct XdbLdapEvtResult
{
    LDAP *ld;
    int msgid;
    int rc;
    LDAPMessage *result;
} XdbLdapEvtResult;

/* user-oriented connection structs */
typedef struct XdbLdapConnList
{
    pool p;
    LDAP *ld;
    time_t creation_time;
    char *binddn;              /* complete dn */
    char *entry;               /* short dn */
    char *user;                /* username */
    char *jid;                 /* user jid - need to be stored for jud issues */
    /* jabber send xml in strange ways */
    /* so user may be created by an auth:set or auth:0k:set */
    int exists; 

    struct XdbLdapConnList *next;
} XdbLdapConnList;

/* passed to request handler */
/* contains general data about module */
typedef struct XdbLdapDatas
{
    pool                  poolref;     /* reference to the instance pool */
    XdbLdapConnList  *master_conn;     /* root connection to LDAP */
    XdbLdapModule        *modules;     /* modules handled by this ldap */
    xmlnode                config;     /* node from config file */
    char                    *host;     /* LDAP hostname */
    int                      port;     /* LDAP port */
    char                    *base;     /* LDAP root dn */
    char              *abook_base;     /* LDAP address book root dn */
    char                *uniqattr;     /* unique attr used to retrieve/set objects */
    char                  *binddn;     /* dn used for "master" connections */
    char                  *bindpw;     /* pw used for "master" connections */
} XdbLdapDatas;


/*
 * Function prototypes
 */

/* xdb_ldap.c */
extern void xdb_ldap(instance i, xmlnode x);

/* xdb_ldap_common.c */
int xdbldap_wait_result(void *arg); /* a thread waiting for LDAP results */
char *xdb_ldap_getpasswd(XdbLdapDatas *self, char *user); /* get user password */

/* xdb_ldap_config.c */
extern int xdbldap_config_init(XdbLdapDatas *self, xmlnode cfgroot);

/* xdb_ldap_conn.c */
/* create an LDAP connection binded with user/pass */
XdbLdapConnList *xdbldap_create_conn(char *host, int port, const char *binddn, 
	     const char *user, const char *passwd, int add_in_list);
/* get (if it exists) LDAPConn info for this user */
XdbLdapConnList *xdbldap_get_conn(const char *user);
/* free all connections when shutting down the module */
void xdbldap_free_allconn();

/* xdb_ldap_auth.c */
extern xmlnode xdbldap_auth_get(XdbLdapDatas *self, XdbLdapConnList *curr_conn);
extern int xdbldap_auth_set(XdbLdapDatas *self, XdbLdapConnList *curr_conn, xmlnode data);

/* xdb_ldap_auth_0k.c */
extern xmlnode xdbldap_auth_0k_get(XdbLdapDatas *self, XdbLdapConnList *curr_conn);
extern int xdbldap_auth_0k_set(XdbLdapDatas *self, XdbLdapConnList *curr_conn, xmlnode data);

/* xdb_ldap_vcard.c */
extern xmlnode xdbldap_vcard_get(XdbLdapDatas *self, XdbLdapConnList *curr_conn);
extern int xdbldap_vcard_set(XdbLdapDatas *self, XdbLdapConnList *curr_conn, xmlnode data);

/* xdb_ldap_jud.c */
extern xmlnode xdbldap_jud_get(XdbLdapDatas *self, XdbLdapConnList *curr_conn);
extern int xdbldap_jud_set(XdbLdapDatas *self, XdbLdapConnList *curr_conn, xmlnode data);

/* xdb_ldap_roster.c */
extern xmlnode xdbldap_roster_get(XdbLdapDatas *self, XdbLdapConnList *curr_conn);
extern int xdbldap_roster_set(XdbLdapDatas *self, XdbLdapConnList *curr_conn, xmlnode data);

#endif  /* XDB_LDAP_H */
