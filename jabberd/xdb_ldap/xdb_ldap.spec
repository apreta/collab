%define name xdb_ldap 
%define version HEAD
%define release idx
  
Summary: Jabber transport for Jabber
Name: %{name}
Version: %{version}
Release: %{release}
Copyright: GPL
Group: Applications/Internet
Source: xdb_ldap.tgz 
#BuildRoot: /var/tmp/%{name}-%{version}-%{release}
Requires: openldap >= 2.0.7
Requires: jabber
 
%description
This module is an implementation of xdb using LDAP.
 

#%prep
#%setup

#%build
#./configure
#make

#%install
#mkdir -p $RPM_BUILD_ROOT
#make DESTDIR=$RPM_BUILD_ROOT install

#%clean
#rm -rf $RPM_BUILD_ROOT

%post
echo "Voir le répertoire /usr/share/doc/jabber"
echo "pour les informations concernant"
echo "  - l'annuaire xdb_ldap"

#%defattr(-,root,root)
%files
/opt/jabber/xdb_ldap/src/xdb_ldap.so
/usr/share/doc/jabber/README.ldap
/etc/openldap/schema/jabber.schema
/usr/share/doc/jabber/organization.ldif
/usr/share/doc/jabber/user.ldif
/usr/share/doc/jabber/slapd.conf
#/usr/share/doc/jabber/server.pem
