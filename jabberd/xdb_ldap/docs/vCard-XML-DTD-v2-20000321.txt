<?xml version="1.0" encoding="UTF-8"?>
<!-- edited with XML Spy v2.5  - http://www.xmlspy.com -->
<!--Copyright (C) The Internet Society (2000).All Rights Reserved.
This document and translations of it may be copied and furnished to others, and derivative works that comment on or otherwise explain it or assist in its implmentation may be prepared, copied, published and distributed, in whole or in part, without restriction of any kind, provided that the above copyright notice and this paragraph are included on all such copies and derivative works.However, this document itself may not be modified in any way, such as by removing the copyright notice or references to the Internet Society or other Internet organizations, except as needed for the purpose of developing Internet standards in which case the procedures for copyrights defined in the Internet Standards process MUST be followed, or as required to translate it into languages other than English.
The limited permissions granted above are perpetual and will not be revoked by the Internet Society or its successors or assigns.
This document and the information contained herein is provided on an "AS IS" basis and THE INTERNET SOCIETY AND THE INTERNET ENGINEERING TASK FORCE DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTY THAT THE USE OF THE INFORMATION HEREIN WILL NOT INFRINGE ANY RIGHTS OR ANY IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.
-->
<!--Root element and container for one or more vCard objects-->
<!ELEMENT XCARD (VCARD)+>
<!--Individual vCard container-->
<!ELEMENT VCARD ((VERSION, FN, N), (NICKNAME?, PHOTO?, BDAY?, ADR?, LABEL?, TEL?, EMAIL?, MAILER?, TZ?, GEO?, TITLE?, ROLE?,  LOGO?, AGENT?, ORG?, CATEGORIES?, NOTE?, PRODID?, REV?, SORT-STRING?, SOUND?, UID?, URL?, CLASS?, KEY?)*)>
<!--vCard specification version property. This MUST be 2.0, if the document conforms to RFC 2426.-->
<!ELEMENT VERSION (#PCDATA)>
<!--Formatted or display name property-->
<!ELEMENT FN (#PCDATA)>
<!--Structured name property. Name components with multiple values must be specified as a comma separated list of values.-->
<!ELEMENT N ( FAMILY?, GIVEN?, MIDDLE?, PREFIX?, SUFFIX?)>
<!ELEMENT FAMILY (#PCDATA)>
<!ELEMENT GIVEN (#PCDATA)>
<!ELEMENT MIDDLE (#PCDATA)>
<!ELEMENT PREFIX (#PCDATA)>
<!ELEMENT SUFFIX (#PCDATA)>
<!--Nickname property. Multiple nicknames must be specified as a comma separated list value.-->
<!ELEMENT NICKNAME (#PCDATA)>
<!--Photograph property. Value is either a BASE64 encoded binary value or a URI to the external content.-->
<!ELEMENT PHOTO ((TYPE, BINVAL) | EXTVAL)>
<!--Birthday property. Value must be an ISO 8601 formatted date or date/time value.-->
<!ELEMENT BDAY (#PCDATA)>
<!--Structured address property. Address components with multiple values must be specified as a comma separated list of values.-->
<!ELEMENT ADR (HOME?, WORK?, POSTAL?, PARCEL?, (DOM | INTL)?, PREF?, POBOX?, EXTADR?, STREET?, LOCALITY?, REGION?, PCODE?, CTRY?)>
<!ELEMENT POBOX (#PCDATA)>
<!ELEMENT EXTADR (#PCDATA)>
<!ELEMENT STREET (#PCDATA)>
<!ELEMENT LOCALITY (#PCDATA)>
<!ELEMENT REGION (#PCDATA)>
<!ELEMENT PCODE (#PCDATA)>
<!ELEMENT CTRY (#PCDATA)>
<!--Address label property.-->
<!ELEMENT LABEL (HOME?, WORK?, POSTAL?, PARCEL?, (DOM | INTL)?, PREF?, LINE+)>
<!--Individual label lines.-->
<!ELEMENT LINE (#PCDATA)>
<!--Telephone number property.-->
<!ELEMENT TEL (HOME?, WORK?, VOICE?, FAX?, PAGER?, MSG?, CELL?, VIDEO?, BBS?, MODEM?, ISDN?, PCS?, PREF?, NUMBER)>
<!--Phone number value.-->
<!ELEMENT NUMBER (#PCDATA)>
<!--Email address property. Default type is INTERNET.-->
<!ELEMENT EMAIL (HOME?, WORK?, INTERNET?, X400?, USERID)>
<!ELEMENT USERID (#PCDATA)>
<!--Mailer (e.g., Mail User Agent Type) property.-->
<!ELEMENT MAILER (#PCDATA)>
<!--Time zone's Standard Time UTC offset. Value must be an ISO 8601 formatted UTC offset.-->
<!ELEMENT TZ (#PCDATA)>
<!--Geographical position. Values are the decimal degress of LATitude and LONgitude. The value should be specified to six decimal places.-->
<!ELEMENT GEO (LAT, LON)>
<!--Latitude value.-->
<!ELEMENT LAT (#PCDATA)>
<!--Longitude-->
<!ELEMENT LON (#PCDATA)>
<!--Title property.-->
<!ELEMENT TITLE (#PCDATA)>
<!--Role property.-->
<!ELEMENT ROLE (#PCDATA)>
<!--Organization logo property.-->
<!ELEMENT LOGO ((TYPE, BINVAL) | EXTVAL)>
<!--Administrative agent property.-->
<!ELEMENT AGENT (VCARD | EXTVAL)>
<!--Organizational name and units property.-->
<!ELEMENT ORG (ORGNAME, ORGUNIT*)>
<!ELEMENT ORGNAME (#PCDATA)>
<!ELEMENT ORGUNIT (#PCDATA)>
<!--Application specific categories property.-->
<!ELEMENT CATEGORIES (KEYWORD+)>
<!ELEMENT KEYWORD (#PCDATA)>
<!--Commentary note property.-->
<!ELEMENT NOTE (#PCDATA)>
<!--Identifier of product that generated the vCard  property.-->
<!ELEMENT PRODID (#PCDATA)>
<!--Last revised property. The value must be an ISO 8601 formatted UTC date/time.-->
<!ELEMENT REV (#PCDATA)>
<!--Sort string property.-->
<!ELEMENT SORTSTR (#PCDATA)>
<!--Formatted name pronunciation property. The value is either a textual phonetic pronunciation, a Base64 encoded binary digital audio pronunciation or a URI to an external binary digital audio pronunciation.-->
<!ELEMENT SOUND (PHONETIC | BINVAL | EXTVAL)>
<!--Textual phonetic pronunciation.-->
<!ELEMENT PHONETIC (#PCDATA)>
<!--Unique identifier property.-->
<!ELEMENT UID (#PCDATA)>
<!--Directory URL property. -->
<!ELEMENT URL (#PCDATA)>
<!--Privacy classification property.-->
<!ELEMENT CLASS (PUBLIC | PRIVATE | CONFIDENTIAL)>
<!ELEMENT PUBLIC EMPTY>
<!ELEMENT PRIVATE EMPTY>
<!ELEMENT CONFIDENTIAL EMPTY>
<!--Authentication credential or encryption  key property.-->
<!ELEMENT KEY (TYPE?, CRED)>
<!ELEMENT CRED (#PCDATA)>
<!--Commonly used element types.-->
<!--Format type parameter.-->
<!ELEMENT TYPE (#PCDATA)>
<!--Base64 encoded binary value.-->
<!ELEMENT BINVAL (#PCDATA)>
<!--URI to external binary value-->
<!ELEMENT EXTVAL (#PCDATA)>
<!--Addressing type indicators.-->
<!ELEMENT HOME EMPTY>
<!ELEMENT WORK EMPTY>
<!ELEMENT POSTAL EMPTY>
<!ELEMENT PARCEL EMPTY>
<!ELEMENT DOM EMPTY>
<!ELEMENT INTL EMPTY>
<!ELEMENT PREF EMPTY>
<!ELEMENT VOICE EMPTY>
<!ELEMENT FAX EMPTY>
<!ELEMENT PAGER EMPTY>
<!ELEMENT MSG EMPTY>
<!ELEMENT CELL EMPTY>
<!ELEMENT VIDEO EMPTY>
<!ELEMENT BBS EMPTY>
<!ELEMENT MODEM EMPTY>
<!ELEMENT ISDN EMPTY>
<!ELEMENT PCS EMPTY>
<!ELEMENT INTERNET EMPTY>
<!ELEMENT X400 EMPTY>
<!--End of DTD-->
