# some simple command to "initialize" the LDAP server
# if you did not activate the TLS mechanism, 
#          replace -ZZ (start TLS) 
#               by -x  (simple authentication)

# create root of LDAP server
ldapadd -v -W -P 3 -D "cn=jabberadmin,o=idx-jabber, c=fr" -f organization.ldif
# create admin account
ldapadd -v -W -P 3 -D "cn=jabberadmin,o=idx-jabber, c=fr" -f add_jabberadmin.ldif
# add a "common" user in LDAP
#ldapadd -v -W -P 3 -D "cn=jabberadmin,o=idx-jabber, c=fr" -f add_user.ldif