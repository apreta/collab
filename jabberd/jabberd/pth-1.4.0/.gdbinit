##
##  GDB Init Script for use with GNU Pth
##  Copyright (c) 1999-2001 Ralf S. Engelschall <rse@engelschall.com>
##

echo [Established Pth Debugging Environment]\n
set history filename ~/.gdb_history

#   a few adjustments
set print pretty on
set listsize 40

#   do not stop on thread creation (uses SIGUSR1 for trampoline)
handle SIGUSR1 nostop print pass

#   additional DSO support
set auto-solib-add 99
define list-objfiles
  set $obj = object_files
  printf "objfile    bfd        msyms  name\n"
  while $obj != 0
    printf "0x%-8x 0x%-8x %6d %s\n", $obj, $obj->obfd, \
      $obj->minimal_symbol_count, $obj->name
    set var $obj = $obj->next
  end
end
document list-objfiles
Print a table of the current objfiles.
end
define dso
   sharedlibrary
end
document dso
Load DSOs (aka sharedlibrary)
end

