/* --------------------------------------------------------------------------
 *
 * License
 *
 * The contents of this file are subject to the Jabber Open Source License
 * Version 1.0 (the "JOSL").  You may not copy or use this file, in either
 * source code or executable form, except in compliance with the JOSL. You
 * may obtain a copy of the JOSL at http://www.jabber.org/ or at
 * http://www.opensource.org/.  
 *
 * Software distributed under the JOSL is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the JOSL
 * for the specific language governing rights and limitations under the
 * JOSL.
 *
 * Copyrights
 * 
 * Portions created by or assigned to Jabber.com, Inc. are 
 * Copyright (c) 1999-2002 Jabber.com, Inc.  All Rights Reserved.  Contact
 * information for Jabber.com, Inc. is available at http://www.jabber.com/.
 *
 * Portions Copyright (c) 1998-1999 Jeremie Miller.
 * 
 * Acknowledgements
 * 
 * Special thanks to the Jabber Open Source Contributors for their
 * suggestions and support of Jabber.
 * 
 * Alternatively, the contents of this file may be used under the terms of the
 * GNU General Public License Version 2 or later (the "GPL"), in which case
 * the provisions of the GPL are applicable instead of those above.  If you
 * wish to allow use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the JOSL,
 * indicate your decision by deleting the provisions above and replace them
 * with the notice and other provisions required by the GPL.  If you do not
 * delete the provisions above, a recipient may use your version of this file
 * under either the JOSL or the GPL. 
 * 
 * 
 * --------------------------------------------------------------------------*/

#include "jabberd.h"

typedef enum {
    ROTFILE_TIME,
    ROTFILE_SIZE,
} rotfile_type_t;

typedef struct
{
    FILE * fp;
    char * fn;
    int debug;
    rotfile_type_t type;

    // ROTFILE_SIZE
    size_t sz;
    int    len;

    // ROTFILE_TIME
    time_t time;

} conf_t;

static void rotate_if_needed(conf_t * cfg)
{
    char newerfn[MAXPATHLEN];
    char olderfn[MAXPATHLEN];
    long sz;
    int i;

    if (cfg->fp == NULL || cfg->fn == NULL)
        return;

    if (cfg->type == ROTFILE_SIZE)
    {
	fseek(cfg->fp, 0, SEEK_END);
	sz = ftell(cfg->fp);

	if (sz < cfg->sz) return;
    
	fclose(cfg->fp);
	cfg->fp = NULL;
	if (cfg->debug >= 0)
	    set_debug_file(NULL);
	
	sprintf(newerfn, "%s.%u", cfg->fn, cfg->len);
	unlink(newerfn);
	for (i = cfg->len-1; i > 0; i--) {
	    strcpy(olderfn, newerfn);
	    sprintf(newerfn, "%s.%u", cfg->fn, i);
	    if (rename(newerfn, olderfn) < 0) {
		if (errno != ENOENT) {
		    cfg->fp = fopen(cfg->fn, "a");
		    return;
		}
	    }
	}

	strcpy(olderfn, newerfn);
	strcpy(newerfn, cfg->fn);
	if (rename(newerfn, olderfn) < 0) {
	    if (errno != ENOENT) {
		cfg->fp = fopen(cfg->fn, "a");
		return;
	    }
	}
    
	cfg->fp = fopen(cfg->fn, "a");
    }
    else if (cfg->type == ROTFILE_TIME)
    {
	time_t now = time(NULL);
	struct tm now_gmt;

	gmtime_r(&now, &now_gmt);

	if (now > cfg->time)
	{
	    char rot_fn[MAXPATHLEN];
	    char timestamp[34];

	    strftime(timestamp, sizeof timestamp, "%F-%T", &now_gmt);
	    sprintf(rot_fn, "%s+%s", cfg->fn, timestamp);

	    fclose(cfg->fp);
	    rename(rot_fn, cfg->fn);
	    cfg->fp = fopen(cfg->fn, "a");

	    int days_since = (now - cfg->time)/86400;
	    cfg->time += (days_since + 1)*86400;
	}
    }

    if (cfg->debug >= 0)
	set_debug_file(cfg->fp);
}


result base_rotfile_deliver(instance id, dpacket p, void* arg)
{
    conf_t * cfg = (conf_t *)arg;
    char * message = NULL;
    
    message = xmlnode_get_data(p->x);
    if (message == NULL)
    {
        log_debug(ZONE,"base_rotfile_deliver error: no message available to print.\n");
        
        return r_ERR;
    }

    rotate_if_needed(cfg);
    
    if (fprintf(cfg->fp, "%s\n", message) == EOF)
    {
        log_debug(ZONE,"base_rotfile_deliver error: error writing to file(%d).\n", errno);

        return r_ERR;
    }
    fflush(cfg->fp);

    /* Release the packet */
    pool_free(p->p);

    return r_DONE;    
}

void _base_rotfile_shutdown(void *arg)
{
    conf_t * cfg = (conf_t *)arg;
    
    fclose((FILE*)cfg->fp);
    if (cfg->debug >= 0)
        set_debug_file(NULL);
    free((void *)cfg->fn);
    free(arg);
}

result base_rotfile_config(instance id, xmlnode x, void *arg)
{
    conf_t * cfg = malloc(sizeof(conf_t));
    char * fn = xmlnode_get_data(x);
    char * time_str = xmlnode_get_attrib(x, "rotationTime");

    if(id == NULL)
    {
        log_debug(ZONE,"base_rotfile_config validating configuration");

        if (xmlnode_get_data(x) == NULL)
        {
            log_debug(ZONE,"base_rotfile_config error: no filename provided.");
            xmlnode_put_attrib(x,"error","'file' tag must contain a filename to write to");
            return r_ERR;
        }
        return r_PASS;
    }

    log_debug(ZONE,"base_rotfile configuring instance %s",id->id);

    if(id->type != p_LOG)
    {
        log_alert(NULL,"ERROR in instance %s: <rotfile>..</rotfile> element only allowed in log sections", id->id);
        return r_ERR;
    }

    cfg->fn = strdup(fn);
    if (time_str == NULL)
    {
	char * sz_str = xmlnode_get_attrib(x, "rotationSize");
	char * len_str = xmlnode_get_attrib(x, "historyLen");
	char * debug_str = xmlnode_get_attrib(x, "debug");
	
	cfg->type = ROTFILE_SIZE;
	cfg->sz = (sz_str != NULL ? atoi(sz_str) : 5000000);
	cfg->len = (len_str != NULL ? atoi(len_str) : 5);
	cfg->debug = (debug_str != NULL ? atoi(debug_str) : -1);
    }
    else
    {
	time_t now = time(NULL);

	cfg->type = ROTFILE_TIME;
	cfg->time = (int)now + (86400 - (int)now % 86400);  // next midnight (86400 sec/day)
	cfg->time += atoi(time_str) * 3600; // convert hrs past midnight GMT to seconds
	cfg->debug = -1;
    }

    /* Attempt to open/create the file */
    cfg->fp = fopen(cfg->fn, "a");
    if (cfg->fp == NULL)
    {
        log_alert(NULL,
                  "base_rotfile_config error: error opening file (%d): %s",
                  errno, strerror(errno));
        free((void *)cfg->fn);
        free((void *)cfg);
        
        return r_ERR;
    }
    
    /* see if using as debug stream */
    if (cfg->debug >= 0)
    {
        set_debug_flag(cfg->debug);
        set_debug_file(cfg->fp);
    }

    /* Register a handler for this instance... */
    register_phandler(id, o_DELIVER, base_rotfile_deliver, (void*)cfg);
    
    pool_cleanup(id->p, _base_rotfile_shutdown, (void*)cfg);
    
    return r_DONE;
}

void base_rotfile(void)
{
    log_debug(ZONE,"base_rotfile loading...");
    register_config("rotfile",base_rotfile_config,NULL);
}
