/* (C) 2005 SiteScape
 *
 * Authenticate by making a request against a web server.  If request
 * succeeds, user access is allowed.
 */

#include "jsm.h"
#include <curl/curl.h>
#include "anon.h"

typedef struct http_connection_t {
  CURL *http_handle;
  CURLM *multi_handle;
  int open;

  /* input buffer */
  char *memory;
  size_t size;
} http_connection_t;


typedef struct http_config_t {
    char * http_url;
    char * ca_certfile;
    int timeout;

    /* For HTTP auth */
    int use_auth;
    int auth_type;      /* basic, digest, etc. */

    /* For navigating form-based authentication pages */
    int use_post;
    char * user_field;
    char * password_field;
    char * submit_value;
    char * match_pattern;

} http_config_t;


http_config_t g_http;

#define RESULT_POLL_INTERVAL 250    /* 250 ms */


/* Store http result in buffer */
size_t WriteMemoryCallback(void *ptr, size_t size, size_t nmemb, void *data)
{
  int realsize = size * nmemb;
  http_connection_t * mem = (http_connection_t *)data;
  
  mem->memory = (char *)realloc(mem->memory, mem->size + realsize + 1);
  if (mem->memory) {
    memcpy(&(mem->memory[mem->size]), ptr, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;
  }
  return realsize;
}


/* Prepare libcurl connection */
int util_http_open(http_connection_t *c)
{
    if (!c->open) 
    {
        c->http_handle = curl_easy_init();

        curl_easy_setopt(c->http_handle, CURLOPT_URL, g_http.http_url);
        curl_easy_setopt(c->http_handle, CURLOPT_TIMEOUT, g_http.timeout);
        curl_easy_setopt(c->http_handle, CURLOPT_NOSIGNAL, 1);
        curl_easy_setopt(c->http_handle, CURLOPT_NOPROGRESS, 1);
        if (g_http.ca_certfile)
            curl_easy_setopt(c->http_handle, CURLOPT_CAINFO, g_http.ca_certfile);

        curl_easy_setopt(c->http_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
        curl_easy_setopt(c->http_handle, CURLOPT_WRITEDATA, (void *)c);

        /* init a multi stack */
        c->multi_handle = curl_multi_init();

        /* add the individual transfers */
        curl_multi_add_handle(c->multi_handle, c->http_handle);

        c->open = 1;
    }

    return 0;
}


/* Close libcurl connection */
int util_http_close(http_connection_t *c)
{
    if (c->open) 
    {
        curl_multi_cleanup(c->multi_handle);

        curl_easy_cleanup(c->http_handle);

        if (c->memory)
        {
            free(c->memory);
            c->memory = NULL;
        }
        c->open = 0;
    }
    
    return 0;
}


/* Submit test request as defined by config */
int util_http_check_user(http_connection_t *c, char *user, char *password)
{
    char auth[512];
    char fields[512];
    char *escaped_user = NULL;
    char *escaped_pwd = NULL;
    int result = 0;
    int still_running;
    long status;
    
    result = util_http_open(c);
    if (result)
        return result;
    
    /* set up request--may use either or both authentication schemes */
    if (g_http.use_auth)
    {
        snprintf(auth, sizeof(auth), "%s:%s", user, password);
        curl_easy_setopt(c->http_handle, CURLOPT_HTTPAUTH, g_http.auth_type);
        curl_easy_setopt(c->http_handle, CURLOPT_USERPWD, auth);
    }
    
    if (g_http.use_post) 
    {
        escaped_user = curl_escape(user, 0);
        escaped_pwd = curl_escape(password, 0);
        curl_easy_setopt(c->http_handle, CURLOPT_POST, 1);
        if (g_http.submit_value && *g_http.submit_value)
        {
            snprintf(fields, sizeof(fields), "%s=%s&%s=%s&%s",
                g_http.user_field, escaped_user, g_http.password_field, escaped_pwd, 
                g_http.submit_value);
        }
        else
        {
            snprintf(fields, sizeof(fields), "%s=%s&%s=%s",
                g_http.user_field, user, g_http.password_field, password);
        }
        curl_easy_setopt(c->http_handle, CURLOPT_POSTFIELDS, fields);
        log_debug("mod_auth_http", "HTTP posting fields: %s", fields);
    }
    
    /* start request */
    while (CURLM_CALL_MULTI_PERFORM ==
        (result = curl_multi_perform(c->multi_handle, &still_running)));
    
    /* continue processing request when socket events occur */
    while (still_running) 
    {
        struct timeval timeout;
        
        fd_set fdread;
        fd_set fdwrite;
        fd_set fdexcep;
        int maxfd;
        
        FD_ZERO(&fdread);
        FD_ZERO(&fdwrite);
        FD_ZERO(&fdexcep);
        
        /* set a suitable timeout to play around with */
        timeout.tv_sec = 1;
        timeout.tv_usec = 0;
        
        /* get file descriptors from the transfers */
        curl_multi_fdset(c->multi_handle, &fdread, &fdwrite, &fdexcep, &maxfd);
        
        result = pth_select(maxfd+1, &fdread, &fdwrite, &fdexcep, &timeout);
        
        switch (result) {
            case -1:
                /* select error */
                still_running = 0;
                log_error("mod_auth_http", "Error processing HTTP socket events (%d)", errno);
                break;
            case 0:
            default:
                /* timeout or readable/writable sockets */
                while(CURLM_CALL_MULTI_PERFORM ==
                    (result = curl_multi_perform(c->multi_handle, &still_running)));
                result = 0;
                break;
        }
    }
    
    /* check result */
    if (result == 0)
    {
        result = curl_easy_getinfo(c->http_handle, CURLINFO_HTTP_CODE, &status);
        if (result || status < 200 || status > 299)
        {
            log_debug("mod_auth_http", "HTTP request for %s failed (%d, %d)", user, status, result);
            result = -1;
        }
    }
    
    if (result == 0 && g_http.match_pattern && *g_http.match_pattern)
    {
        log_debug("mod_auth_http", "HTTP checking response: %s", c->memory);
        if (strstr(c->memory ,g_http.match_pattern) == NULL)
        {
            log_debug("mod_auth_http", "HTTP match pattern not found in response");
            result = -1;
        }
    }
    
    if (escaped_user)
        curl_free(escaped_user);
    if (escaped_pwd)
        curl_free(escaped_pwd);
    
    util_http_close(c);
    
    return result;
}


static int check_password(jid id, char *password)
{
    http_connection_t http_conn;

    memset(&http_conn, 0, sizeof(http_conn));

    if (!util_http_check_user(&http_conn, id->user, password)) {
        log_debug("mod_auth_http", "User authenticated");
        return 1;
    }

    log_debug("mod_auth_http", "HTTP authentication failed");
    return 0;
}


mreturn mod_auth_http_act(mapi m, void *arg)
{
    char *pass;

    log_debug("mod_auth_http","checking");

    if(jpacket_subtype(m->packet) == JPACKET__GET)
    { /* type=get means we flag that the server can do plain-text auth */
        xmlnode_insert_tag(m->packet->iq,"password");
        return M_PASS;
    }

    if((pass = xmlnode_get_tag_data(m->packet->iq, "password")) == NULL)
        return M_PASS;

    log_debug("mod_auth_http","trying http check");

    /* Allow anonymous with predefined password */
    if (strcmp(m->user->user, ANON_USER) == 0 &&
        strcmp(pass, ANON_PASSWORD) == 0) {
           jutil_iqresult(m->packet->x);
           return M_HANDLED;
    }

    /* Don't handle admin here, let fall back to mod_auth_plain */
    if (strcmp(m->user->user, ADMIN_USER) == 0)
        return M_PASS;

    /* check password via HTTP */
    ++m->user->ref;
    if(check_password(m->user->id, pass))
        jutil_iqresult(m->packet->x);
    else
        jutil_error(m->packet->x, TERROR_AUTH);
    --m->user->ref;

    return M_HANDLED;
}


void mod_auth_http(jsmi si)
{
    xmlnode config;
    char *timeout_str, *auth_str, *use_auth_str, *use_post_str;

    log_debug("mod_auth_http","init");

    memset(&g_http, 0, sizeof(g_http));
    g_http.auth_type = CURLAUTH_ANY;
    g_http.timeout = 5000;

    config = js_config(si, "http");
    if (config == NULL) 
    {
        log_debug("mod_auth_http", "no configuration for mod_auth_http, not initializing");
        return;
    }

    g_http.http_url = xmlnode_get_tag_data(config, "url");
    g_http.ca_certfile = xmlnode_get_tag_data(config, "cacertfile");
    g_http.user_field = xmlnode_get_tag_data(config, "userfield");
    g_http.password_field = xmlnode_get_tag_data(config, "passwordfield");
    g_http.submit_value = xmlnode_get_tag_data(config, "submitvalue");
    g_http.match_pattern = xmlnode_get_tag_data(config, "matchpattern");
    use_auth_str = xmlnode_get_tag_data(config, "useauth");
    if (use_auth_str)
        g_http.use_auth = atoi(use_auth_str);
    use_post_str = xmlnode_get_tag_data(config, "usepost");
    if (use_post_str)
        g_http.use_post = atoi(use_post_str);
    timeout_str = xmlnode_get_tag_data(config, "timeout");
    if (timeout_str)
        g_http.timeout = atoi(timeout_str);
    auth_str = xmlnode_get_tag_data(config, "authtype");
    if (auth_str) {
        if (strcmp(auth_str, "basic") == 0)
            g_http.auth_type = CURLAUTH_BASIC;
        else if (strcmp(auth_str, "digest") == 0) {
            g_http.auth_type = CURLAUTH_DIGEST;
        }
        else if (strcmp(auth_str, "negotiate") == 0) {
            g_http.auth_type = CURLAUTH_GSSNEGOTIATE;
        }
        else if (strcmp(auth_str, "any-safe") == 0) {
            g_http.auth_type = CURLAUTH_ANYSAFE;
        }
        else if (strcmp(auth_str, "any") != 0) {
            log_error("mod_auth_http", "Invalid config option for http authentication type, ignoring");
        }
    }

    if (g_http.http_url == NULL)
    {
        log_error("mod_auth_http", "configuration item missing, not initializing");
        return;
    }

    /* no config, don't enable. */
    if (!*g_http.http_url || (g_http.use_auth == 0 && g_http.use_post == 0))
        return;

    js_mapi_register(si, e_AUTH, mod_auth_http_act, NULL);
}
