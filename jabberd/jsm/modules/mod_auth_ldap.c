/* (C) 2005 SiteScape
 *
 * Adapted in part from Apache LDAP authentication module.
 *
 * Note that user information is still loaded from xdb, this module is only
 * checking the given password by binding against the ldap server.  If a user
 * is not found in xdb first, he will be rejected before reaching this module.
 *
 * Our strategy is:
 *   - bind to directory using predefined DN & password
 *   - search for entries using given screen name
 *   - if entry is found, rebind to directory using its DN & given password.
 *
 * Because we wait for the lookup to complete before continuing (in a libpth
 * friendly way), the number of simultaneous lookups should be limited by the
 * number of threads.
 */

#include "jsm.h"
#include "anon.h"
#include <lber.h>
#include <ldap.h>


typedef struct ldap_config_t {
    char * ldap_url;
    char * bind_dn;
    char * bind_password;
    char * base_dn;
    char * search_filter;
    int search_scope;
    int timeout;
    int retry_time;
    int single_bind;
} ldap_config_t;

typedef struct ldap_connection_t {
    LDAP *ldap;
    int bound;                          /* Flag to indicate whether this connection is bound yet */
    char *reason;
    ldap_config_t *config;
} ldap_connection_t;


#define MAX_SERVERS 32

int g_ldap_count = 0;
ldap_config_t g_ldap[MAX_SERVERS];

#define RESULT_POLL_INTERVAL 250    /* 250 ms */
#define RETRY_INTERVAL 120  /* 120 seconds */


/*
 * Destroys an LDAP connection by unbinding and closing the connection to
 * the LDAP server.
 */
int util_ldap_connection_unbind(ldap_connection_t *ldc)
{

    if (ldc) {
        if (ldc->ldap) {
            ldap_unbind_s(ldc->ldap);
            ldc->ldap = NULL;
        }
        ldc->bound = 0;
    }

    return LDAP_SUCCESS;
}


int util_ldap_wait_result(ldap_connection_t *ldc, int msgid, LDAPMessage **res)
{
    struct timeval timeout = { 0, 0 };
    int result;
    int retries = ldc->config->timeout / RESULT_POLL_INTERVAL;
    LDAPMessage *localres;

    if (msgid < 0) {
        ldc->reason = "LDAP: operation was not started";
        return msgid;
    }

    while (--retries) {
        pth_usleep(RESULT_POLL_INTERVAL * 1000);
        result = ldap_result(ldc->ldap, msgid, 1, &timeout, &localres);
        if (result < 0) {
            ldc->reason = "LDAP: failed to wait for result";
            return result;
        }
        else if (result > 0) {
            if (res)
                *res = localres;
            return ldap_result2error(ldc->ldap, localres, res == NULL);
        }
    }

    return LDAP_TIMEOUT;
}


/*
 * Connect to the LDAP server and binds. Does not connect if already
 * connected (i.e. ldc->ldap is non-NULL.) Does not bind if already bound.
 *
 * Returns LDAP_SUCCESS on success; and an error code on failure
 */
int util_ldap_connection_open(ldap_connection_t *ldc)
{
    int result = 0;
    int version  = LDAP_VERSION3;
    int rc = LDAP_SUCCESS;
    struct timeval timeOut = {2,0};    /* 2 second connection timeout */
    int msgid;

    /* If the connection is already bound, return
    */
    if (ldc->bound)
    {
        return LDAP_SUCCESS;
    }

    /* create the ldap session handle
    */
    if (NULL == ldc->ldap)
    {
        /* clear connection requested */
        result = ldap_initialize(&ldc->ldap, ldc->config->ldap_url);
        if (result == LDAP_SUCCESS)
        {
            if (strncmp(ldc->config->ldap_url, "ldaps:", 6) == 0)
            {
                int SSLmode = LDAP_OPT_X_TLS_HARD;
                result = ldap_set_option(ldc->ldap, LDAP_OPT_X_TLS, &SSLmode);
                if (LDAP_SUCCESS != result)
                {
                    ldap_unbind_s(ldc->ldap);
                    ldc->reason = "LDAP: ldap_set_option - LDAP_OPT_X_TLS_HARD failed";
                    ldc->ldap = NULL;
                }
            }
        }

        if (NULL == ldc->ldap)
        {
            ldc->bound = 0;
            if (NULL == ldc->reason)
                ldc->reason = "LDAP: ldap initialization failed";
            return(-1);
        }

        /* Set the alias dereferencing option */
        //ldap_set_option(ldc->ldap, LDAP_OPT_DEREF, &(ldc->deref));

        /* always default to LDAP V3 */
        ldap_set_option(ldc->ldap, LDAP_OPT_PROTOCOL_VERSION, &version);

        /* set network timeout or initial connect may block */
        if (ldc->config->timeout > 0) {
            timeOut.tv_sec = ldc->config->timeout;
        }

        rc = ldap_set_option(ldc->ldap, LDAP_OPT_NETWORK_TIMEOUT, (void *)&timeOut);
        if (LDAP_SUCCESS != rc) {
            // log "LDAP: Could not set the connection timeout" );
        }
    }

    /* try to connect & bind
     */
    msgid = ldap_simple_bind(ldc->ldap, ldc->config->bind_dn, ldc->config->bind_password);
    result = util_ldap_wait_result(ldc, msgid, NULL);

    /* free the handle if there was an error
    */
    if (LDAP_SUCCESS != result)
    {
        ldap_unbind_s(ldc->ldap);
        ldc->ldap = NULL;
        ldc->bound = 0;
        ldc->reason = "LDAP: bind to directory failed";
    }
    else {
        ldc->bound = 1;
        ldc->reason = "LDAP: connection open";
    }

    return(result);
}


/*
 * Find user in directory, then verify password.
 *
 * Returns LDAP_SUCCESS on success; and an error code on failure
 */
int util_ldap_check_user(ldap_connection_t *ldc, char *user, char *password,
                         char **attrs, const char ***retvals)
{
    char filter[1024];
    char doctoredUser[1024];
    int result = 0;
    LDAPMessage *res, *entry;
    char *dn;
    int count;
    int failures = 0;
    int msgid;

    /*
     * If any LDAP operation fails due to LDAP_SERVER_DOWN, control returns here.
     */
start_over:
    if (failures++ > 10) {
        return result;
    }
    if (LDAP_SUCCESS != (result = util_ldap_connection_open(ldc))) {
        return result;
    }

    /*
     * Undo cheap hack from LDAP sync to allow identification of users by CN, which may
     * have spaces
     */
    int i = 0;
    while(*user) { doctoredUser[i++]= (*user=='+')?' ':*user; user++; }
    doctoredUser[i] ='\0';

    /* construct filter from user name */
    snprintf(filter, sizeof(filter), ldc->config->search_filter, doctoredUser);
    
    /* try do the search */
    result = ldap_search_ext(ldc->ldap,
				    ldc->config->base_dn, ldc->config->search_scope, 
				    filter, attrs, 0, 
				    NULL, NULL, NULL, -1, &msgid);

    result = util_ldap_wait_result(ldc, msgid, &res);
    if (result == LDAP_SERVER_DOWN) {
        ldc->reason = "LDAP: search for user failed with server down";
        util_ldap_connection_unbind(ldc);
        goto start_over;
    }

    /* if there is an error (including LDAP_NO_SUCH_OBJECT) return now */
    if (result != LDAP_SUCCESS) {
        ldc->reason = "LDAP: search for user failed";
        return result;
    }

    /* 
     * We should have found exactly one entry; to find a different
     * number is an error.
     */
    count = ldap_count_entries(ldc->ldap, res);
    if (count != 1) 
    {
        if (count == 0 )
            ldc->reason = "User not found";
        else
            ldc->reason = "User is not unique (search found two or more matches)";
        ldap_msgfree(res);
        return LDAP_NO_SUCH_OBJECT;
    }

    entry = ldap_first_entry(ldc->ldap, res);

    /* Get dn of target user */
    dn = ldap_get_dn(ldc->ldap, entry);

    /* 
     * A bind to the server with an empty password always succeeds, so
     * we check to ensure that the password is not empty. This implies
     * that users who actually do have empty passwords will never be
     * able to authenticate with this module. I don't see this as a big
     * problem.
     */
    if (!password || strlen(password) <= 0) {
        ldap_memfree(dn);
        ldap_msgfree(res);
        ldc->reason = "Empty password not allowed";
        return LDAP_INVALID_CREDENTIALS;
    }

    /* 
     * Attempt to bind with the retrieved dn and the password. If the bind
     * fails, it means that the password is wrong (the dn obviously
     * exists, since we just retrieved it)
     */
    msgid = ldap_simple_bind(ldc->ldap, dn, password);
    if ((result = util_ldap_wait_result(ldc, msgid, NULL)) == 
         LDAP_SERVER_DOWN) {
        ldc->reason = "LDAP: bind to check user credentials failed with server down";
        ldap_memfree(dn);
        ldap_msgfree(res);
        util_ldap_connection_unbind(ldc);
        goto start_over;
    }

    /* Check result... */
    if (result != LDAP_SUCCESS) {
        ldc->reason = "LDAP: bind to check user credentials failed";
    }
    else {
        ldc->reason = "Authentication successful";
    }
    
    /*
     * Get values for the provided attributes.
     */
#if 0
    if (attrs) {
        const char **vals = NULL;
        int k = 0;
        int i = 0;
        while (attrs[k++]);
        vals = apr_pcalloc(r->pool, sizeof(char *) * (k+1));
        numvals = k;
        while (attrs[i]) {
            char **values;
            int j = 0;
            char *str = NULL;
            /* get values */
            values = ldap_get_values(ldc->ldap, entry, attrs[i]);
            while (values && values[j]) {
                str = str ? apr_pstrcat(r->pool, str, "; ", values[j], NULL) : apr_pstrdup(r->pool, values[j]);
                j++;
            }
            ldap_value_free(values);
            vals[i] = str;
            i++;
        }
        *retvals = vals;
    }
#endif

    ldap_memfree(dn);
    ldap_msgfree(res);

    return result;
}


/*
 * Bind to user to verify password.
 *
 * Returns LDAP_SUCCESS on success; and an error code on failure
 */
int util_ldap_bind_user(ldap_connection_t *ldc, char *user, char *password,
                        char **attrs, const char ***retvals)
{
    char filter[512];
    char dn[1024];
    int result = 0;
    LDAPMessage *res, *entry;
    int count;
    int failures = 0;
    int msgid;

    /*
     * If any LDAP operation fails due to LDAP_SERVER_DOWN, control returns here.
     */
start_over:
    if (failures++ > 10) {
        return result;
    }
    if (LDAP_SUCCESS != (result = util_ldap_connection_open(ldc))) {
        return result;
    }

    /* construct filter from user name and base dn */
    snprintf(filter, sizeof(filter), ldc->config->search_filter, user);
    snprintf(dn, sizeof(dn), "%s,%s", filter, ldc->config->base_dn); 
    
    /* 
     * A bind to the server with an empty password always succeeds, so
     * we check to ensure that the password is not empty. This implies
     * that users who actually do have empty passwords will never be
     * able to authenticate with this module. I don't see this as a big
     * problem.
     */
    if (!password || strlen(password) <= 0) {
        ldc->reason = "Empty password not allowed";
        return LDAP_INVALID_CREDENTIALS;
    }

    /* 
     * Attempt to bind with the retrieved dn and the password. If the bind
     * fails, it means that the password is wrong (the dn obviously
     * exists, since we just retrieved it)
     */
    msgid = ldap_simple_bind(ldc->ldap, dn, password);
    if ((result = util_ldap_wait_result(ldc, msgid, NULL)) == 
         LDAP_SERVER_DOWN) {
        ldc->reason = "LDAP: bind to check user credentials failed with server down";
        util_ldap_connection_unbind(ldc);
        goto start_over;
    }

    /* Check result... */
    if (result != LDAP_SUCCESS) {
        ldc->reason = "LDAP: bind to check user credentials failed";
    }
    else {
        ldc->reason = "Authentication successful";
    }
    
    /*
     * Get values for the provided attributes.
     */
#if 0
    if (attrs) {
        const char **vals = NULL;
        int k = 0;
        int i = 0;
        while (attrs[k++]);
        vals = apr_pcalloc(r->pool, sizeof(char *) * (k+1));
        numvals = k;
        while (attrs[i]) {
            char **values;
            int j = 0;
            char *str = NULL;
            /* get values */
            values = ldap_get_values(ldc->ldap, entry, attrs[i]);
            while (values && values[j]) {
                str = str ? apr_pstrcat(r->pool, str, "; ", values[j], NULL) : apr_pstrdup(r->pool, values[j]);
                j++;
            }
            ldap_value_free(values);
            vals[i] = str;
            i++;
        }
        *retvals = vals;
    }
#endif

    return result;
}


static int check_password(jid id, char *password)
{
    ldap_connection_t ldap_conn;
    char *attribs[] = { "distinguishedName", NULL };
    int result;
    int count = 0;

    while (count < g_ldap_count)
    {
        memset(&ldap_conn, 0, sizeof(ldap_conn));
        ldap_conn.config = &g_ldap[count];

        if (g_ldap[count].retry_time && g_ldap[count].retry_time > time(NULL)) {
            log_debug("mod_auth_ldap", "Skipping %s", ldap_conn.config->ldap_url);
            ++count;
            continue;
        }
        g_ldap[count].retry_time = 0;

        log_debug("mod_auth_ldap", "Checking against %s", ldap_conn.config->ldap_url);

        if (ldap_conn.config->single_bind)
            result = util_ldap_bind_user(&ldap_conn, id->user, password, attribs, NULL);
        else
            result = util_ldap_check_user(&ldap_conn, id->user, password, attribs, NULL);
        util_ldap_connection_unbind(&ldap_conn);

        if (result == LDAP_SUCCESS) {
            log_debug("mod_auth_ldap", "User authenticated");
            return 1;
        }

        /* If more then one server and this server appears to be down, skip 
           testing against it a bit */
        if (result == LDAP_SERVER_DOWN && g_ldap_count > 1) {
            g_ldap[count].retry_time = time(NULL) + RETRY_INTERVAL;
            log_debug("mod_auth_ldap", "Server down, will retry later");
        }

        ++count;
    }

    log_debug("mod_auth_ldap", "LDAP error: %d (%s)",result, ldap_conn.reason ? ldap_conn.reason : "");
    return 0;
}


mreturn mod_auth_ldap_act(mapi m, void *arg)
{
    char *pass;

    log_debug("mod_auth_ldap","checking");

    if(jpacket_subtype(m->packet) == JPACKET__GET)
    { /* type=get means we flag that the server can do plain-text auth */
        xmlnode_insert_tag(m->packet->iq,"password");
        return M_PASS;
    }

    if((pass = xmlnode_get_tag_data(m->packet->iq, "password")) == NULL)
        return M_PASS;

    /* Allow anonymous with predefined password */
    if (strcmp(m->user->user, ANON_USER) == 0 &&
        strcmp(pass, ANON_PASSWORD) == 0) {
           jutil_iqresult(m->packet->x);
           return M_HANDLED;
    }
    
    /* Don't handle admin here, let fall back to mod_auth_plain */
    if (strcmp(m->user->user, ADMIN_USER) == 0)
        return M_PASS;

    /* Use "\001\002" to indicate a password we've validated and cached */
    if (m->user->pass != NULL && strncmp(m->user->pass, "\001\002", 2) == 0 &&
        strcmp(m->user->pass + 2, pass) == 0)
    {
       jutil_iqresult(m->packet->x);
       log_debug("mod_auth_ldap", "User authenticated from cache");
       return M_HANDLED;
    }

    /* check password via LDAP */
    ++m->user->ref;
    if(check_password(m->user->id, pass))
    {
        m->user->pass = pmalloc(m->user->p, strlen(pass) + 3);
        strcpy(m->user->pass, "\001\002");
        strcat(m->user->pass, pass);
        jutil_iqresult(m->packet->x);
    }
    else
        jutil_error(m->packet->x, TERROR_AUTH);
    --m->user->ref;

    return M_HANDLED;
}


void mod_auth_ldap(jsmi si)
{
    xmlnode config;
    xmlnode server;
    char *timeout_str, *scope_str, *single_str;
    ldap_config_t *ldap_cfg;
    char *ca_certfile;
    char *ca_certdir;

    log_debug("mod_auth_ldap","init");

    config = js_config(si, "ldap");
    if (config)
        server = xmlnode_get_tag(config, "servers/server");
    if (config == NULL || server == NULL) 
    {
        log_debug("mod_auth_ldap", "no configuration for mod_auth_ldap, not initializing");
        return;
    }

    memset(g_ldap, 0, sizeof(g_ldap));

    ca_certfile = xmlnode_get_tag_data(config, "cacertfile");
    ca_certdir = xmlnode_get_tag_data(config, "cacertdir");

    while (server != NULL && g_ldap_count < MAX_SERVERS)
    {
        if (xmlnode_get_type(server) == NTYPE_TAG)
        {
            ldap_cfg = &g_ldap[g_ldap_count++];
            
            ldap_cfg->search_scope = LDAP_SCOPE_SUBTREE;
            ldap_cfg->timeout = 0;
            ldap_cfg->single_bind = 0;
        
            ldap_cfg->ldap_url = xmlnode_get_tag_data(server, "url");
            ldap_cfg->bind_dn = xmlnode_get_tag_data(server, "binddn");
            ldap_cfg->bind_password = xmlnode_get_tag_data(server, "bindpassword");
            ldap_cfg->base_dn = xmlnode_get_tag_data(server, "basedn");
            ldap_cfg->search_filter = xmlnode_get_tag_data(server, "filter");
            timeout_str = xmlnode_get_tag_data(server, "timeout");
            if (timeout_str)
                ldap_cfg->timeout = atoi(timeout_str);
            scope_str = xmlnode_get_tag_data(server, "scope");
            if (scope_str) {
                if (strcmp(scope_str, "one") == 0)
                    ldap_cfg->search_scope = LDAP_SCOPE_ONELEVEL;
                else if (strcmp(scope_str, "sub") != 0) {
                    log_error("mod_auth_ldap", "Invalid config option for search scope, ignoring");
                }
            }
            single_str = xmlnode_get_tag_data(server, "singlebind");
            if (single_str)
                ldap_cfg->single_bind = atoi(single_str);

            if (ldap_cfg->ldap_url == NULL || ldap_cfg->base_dn == NULL || ldap_cfg->search_filter == NULL ||
				(ldap_cfg->single_bind == 0 && (ldap_cfg->bind_dn == NULL || ldap_cfg->bind_password == NULL)))
            {
                log_error("mod_auth_ldap", "configuration item missing, not initializing");
                return;
            }
            
            log_debug("mod_auth_ldap", "added server %s to config (%s)", ldap_cfg->ldap_url, ldap_cfg->single_bind ? "single bind" : "normal bind");
        }
        
        server = xmlnode_get_nextsibling(server);
    }
    
    /* set ca cert file--this option must be set globally */
    if (ca_certfile)
    {
        int result;
        result = ldap_set_option(NULL, LDAP_OPT_X_TLS_CACERTFILE, ca_certfile);
        if (LDAP_SUCCESS != result)
        {
            log_error("mod_auth_ldap", "Error setting CA cert file");
        }
    }
    if (ca_certdir)
    {
        int result;
        result = ldap_set_option(NULL, LDAP_OPT_X_TLS_CACERTDIR, ca_certdir);
        if (LDAP_SUCCESS != result)
        {
            log_error("mod_auth_ldap", "Error setting CA cert dir");
        }
    }

    js_mapi_register(si, e_AUTH, mod_auth_ldap_act, NULL);
}
