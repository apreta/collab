/*
 * log presence data in real-time to connected external components
 */

#include "jsm.h"

mreturn mod_chatlog_deliver(mapi m, void *arg)
{
    int subtype = jpacket_subtype(m->packet);
    int is_msg = (strcmp(xmlnode_get_name(m->packet->x), "message") == 0);
    xmlnode body = xmlnode_get_tag(m->packet->x, "body");
  
    if (is_msg && body != NULL)
    {
	if ( subtype == JPACKET__CHAT || subtype == JPACKET__GROUPCHAT )
	{
	    if (body)
	    {
		time_t now = time(NULL);
		struct tm now_gmt;
		char now_gmt_str[32];
		gmtime_r(&now, &now_gmt);
		strftime(now_gmt_str, sizeof now_gmt_str, "%FT%T", &now_gmt);
		jpacket ts_pkt = jpacket_new(m->packet->x);

		xmlnode_put_attrib(ts_pkt->x, "time", now_gmt_str);

		logger("chat", NULL, xmlnode2str(ts_pkt->x));
	    }
	}
    }

    return M_PASS;
}

// configuration of 
void mod_chatlog(jsmi si)
{
    extern HASHTABLE instance__ids;

    log_debug("mod_chatlog", "Will log to /var/iic/chatlog");
                      
    js_mapi_register(si, e_DELIVER, mod_chatlog_deliver, NULL);
}

