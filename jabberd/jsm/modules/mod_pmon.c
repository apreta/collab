/*
 * log presence data in real-time to connected external components
 */

#include "jsm.h"

static pth_mutex_t s_cache_lock;
static xht s_p_cache = NULL;

enum {
  PRESBIT_CLIENT=1,
  PRESBIT_VOICE=2,
  // app-share also in controllerd get_presence_*(), but jabber doesn't know about it
  PRESBIT_MEETING=8,
  PRESBIT_AWAY=16,
};

typedef struct pcache_entry_st
{
  int modes;
} _pcache_entry, *pcache_entry;

static int is_client(jid id)
{
  return (strncmp(id->resource, "iic", 3) == 0);
}

static int is_voice(jid id)
{
  return (strncmp(id->resource, "voice", 5) == 0);
}

static int update_modes(int prior_modes, jpacket pkt)
{
  xmlnode c;
  int updated_modes = prior_modes;

  if (is_client(pkt->from))
    {
      int is_away = 0;
        
      log_debug("mod_pmon", "update_modes: client presence");
      for (c = xmlnode_get_firstchild(pkt->x); c != NULL; c = xmlnode_get_nextsibling(c))
        {
	  if (strcmp(xmlnode_get_name(c), "show") == 0)
            {
	      char * data = xmlnode_get_data(c);

	      if (data && strcmp(data, "away") == 0)
                {
		  log_debug("mod_pmon", "update_modes: client is away");
		  is_away = 1;
                }
            }
            
	  if (strcmp(xmlnode_get_name(c), "status") == 0)
            {
	      char * data = xmlnode_get_data(c);
                
	      log_debug("mod_pmon", "update_modes: <status> contains '%s'", data);

	      if (strstr(data, "iic:meeting"))
                {
		  updated_modes |= (PRESBIT_MEETING|PRESBIT_CLIENT);
                }
	      else if (strstr(data, "iic:"))
                {
		  updated_modes |= PRESBIT_CLIENT;
		  updated_modes &= ~PRESBIT_MEETING;
                }
            }
        }
      if (is_away)
        {
	  updated_modes |= PRESBIT_AWAY;
        }
      else
        {
	  if (updated_modes & PRESBIT_AWAY)
	    log_debug("mod_pmon", "update_modes: client is no longer away");

	  updated_modes &= ~PRESBIT_AWAY;
        }
    }
  else if (is_voice(pkt->from))
    {
      log_debug("mod_pmon", "update_modes: voice presence");
      updated_modes |= PRESBIT_VOICE;

      // don't care about children nodes yet
    }
    
  return updated_modes;
}

#define MAX_LISTENERS 10

static pth_mutex_t s_listeners_lock;
static mio s_listeners[MAX_LISTENERS];

static void update_listener(mio m, const char *user, int pres_flags)
{
  char output[64];

  sprintf(output, "%s %d\n", user, pres_flags);
  mio_write(m, NULL, output, strlen(output));
}

static void update_listeners(const char * user, int pres_flags)
{
  int i;

  pth_mutex_acquire(&s_listeners_lock, FALSE /* tryonly*/, NULL);
  for (i = 0; i < MAX_LISTENERS; i++)
    {
      if (s_listeners[i] != NULL)
        {
	  update_listener(s_listeners[i], user, pres_flags);
        }
    }
  pth_mutex_release(&s_listeners_lock);
}

static void update_presence_cache(jpacket pkt, int avail)
{
  const char * from_user;
  pcache_entry from_entry = NULL;
  int prior_modes;
	
  pth_mutex_acquire(&s_cache_lock, FALSE /* tryonly*/, NULL);

  from_user = jid_full(jid_user(pkt->from));
  from_entry = (pcache_entry)xhash_get(s_p_cache, from_user);
	
  if (!avail && from_entry == NULL)
    {
      log_debug(ZONE, "user %s unavail, but wasn't recorded as avail; ignoring...", from_user);
      pth_mutex_release(&s_cache_lock);
      return;
    }
        
  prior_modes = (from_entry != NULL ? from_entry->modes : 0);
    
  if (avail)
    {
      if (from_entry == NULL)
        {
	  from_entry = (pcache_entry)pmalloco(s_p_cache->p, sizeof(_pcache_entry));
	  from_entry->modes = 0;
	  xhash_put(s_p_cache, pstrdup(s_p_cache->p, from_user), from_entry);
        }
      from_entry->modes = update_modes(from_entry->modes, pkt);
    }
  else
    {
      if (is_client(pkt->from))
        {
	  from_entry->modes &= ~(PRESBIT_CLIENT|PRESBIT_MEETING|PRESBIT_AWAY);
        }
      else if (is_voice(pkt->from))
        {
	  from_entry->modes &= ~PRESBIT_VOICE;
        }
    }
	
  if (prior_modes ^ from_entry->modes)
    {
      log_debug("mod_pmon", "user %s is now %d", from_user, from_entry->modes);

      update_listeners(from_user, from_entry->modes);
    }
	
  if (from_entry->modes == 0)
    {
      xhash_zap(s_p_cache, from_user);
    }

  pth_mutex_release(&s_cache_lock);
}

mreturn mod_pmon_deliver(mapi m, void *arg)
{
  int subtype = jpacket_subtype(m->packet);

  switch(subtype)  
    {
    case JPACKET__AVAILABLE:
      update_presence_cache(m->packet, TRUE);
      break;
          
    case JPACKET__UNAVAILABLE:
      update_presence_cache(m->packet, FALSE);
      break;
    }
  
  return M_PASS;
}

static void p_cache_walker(xht h, const char *key, void *val, void *arg)
{
  pcache_entry from_entry = (pcache_entry)val;
  update_listener((mio)arg, key, from_entry->modes);
}

static void initialize_listener(mio m)
{
  pth_mutex_acquire(&s_cache_lock, FALSE /* tryonly*/, NULL);
  xhash_walk(s_p_cache, &p_cache_walker, (void *)m);
  pth_mutex_release(&s_cache_lock);
}

static int s_loser = 0;

static void mio_cb(mio m, int state, void *arg)
{
  int i;
    
  if (state == MIO_NEW)
    {
      pth_mutex_acquire(&s_listeners_lock, FALSE /* tryonly*/, NULL);
      log_debug("mod_pmon", "new connection");
      for (i = 0; i < MAX_LISTENERS; i++)
        {
	  if (s_listeners[i] == NULL)
            {
	      s_listeners[i] = m;
	      initialize_listener(m);
	      log_debug("mod_pmon", "new listener %X", m);
	      break;
            }
        }
      if (i == MAX_LISTENERS)
        {
	  log_error("mod_pmon", "Too many listeners; closing %X", 
		    s_listeners[s_loser]);

	  mio_close(s_listeners[s_loser]);
	  s_listeners[s_loser] = m;
	  initialize_listener(m);

	  if (++s_loser == MAX_LISTENERS)
	    s_loser = 0;
        }
      pth_mutex_release(&s_listeners_lock);
    }
  else if (state == MIO_CLOSED)
    {
      pth_mutex_acquire(&s_listeners_lock, FALSE /* tryonly*/, NULL);
      for (i = 0; i < MAX_LISTENERS; i++)
        {
	  if (s_listeners[i] == m)
            {
	      s_listeners[i] = NULL;
	      log_debug("mod_pmon", "listener %X closed", m);
	      break;
            }
        }
      pth_mutex_release(&s_listeners_lock);
      if (i == MAX_LISTENERS)
        {
	  log_debug("mod_pmon", "listener %X close, but wasn't on listeners list; too many?", m);
        }
    }
}

result mod_pmon_beat(void *arg)
{
  //instance id = (instance)arg;
  log_debug("mod_pmon", "pmon heartbeat");

  update_listeners("ping.service@heartbeat", 0);
   
  return r_DONE;
}

void mod_pmon(jsmi si)
{
  xmlnode cfg = js_config(si, "pmon");
  char *ip = xmlnode_get_tag_data(cfg, "ip");
  int port = j_atoi(xmlnode_get_tag_data(cfg, "port"), -1);
  int i;

  log_debug("mod_pmon", "init ip=%s port=%d", (ip ? ip : "<null>"), port);
                      
  pth_mutex_init(&s_listeners_lock);
  for (i = 0; i < MAX_LISTENERS; i++)
    {
      s_listeners[i] = NULL;
    }

  pth_mutex_init(&s_cache_lock);
  s_p_cache = xhash_new(10007);

  js_mapi_register(si, e_DELIVER, mod_pmon_deliver, NULL);
  if (port > 0 && mio_listen(port, ip, &mio_cb, NULL, NULL, NULL) == NULL)
    {
      log_error("mod_pmon", "Can't listen on %d", port);
    }
    
  register_beat(60, mod_pmon_beat, si->i);

}

