/* (c)2004 imidio */

#include "jsm.h"

#define MAX_HOSTS 2		/* Max hosts defined for service */
#define MAX_SESSION_WATCHERS 5	/* Max number of services receiving session events */

jsmi  mod_router__jsmi = NULL;
char *cluster_name = NULL;
char *node_name = NULL;
char *other_node = NULL;
int sm_heartbeat = 0;
int sm_refresh = 0;

xht node_list = NULL;
xht host_map = NULL;		// service name -> host id
xht service_map = NULL;		// host id -> service name
xht location_cache = NULL;

/* To do:
   Allow for multiple nodes (>2) in cluster
   Send online to online nodes
   Cache user location when we received a forwarded packet
   If user in cache, only send to that node
   Clear cache entries on heartbeat
   Use service location to forward messages
   Seperate services into different modules (presence cache, service location, routing, etc.)

   Issues:
      If same user connects to different servers, one JSM may send offline presence even though user is
      still connected
	  Chat rooms show appear as new users (add room name to resource instead?)
	  On new strategy -- host found while still getting location messages may be invalid
      Packets may get abandoned during exchange

   Config [under jsm element]:
  	  <router>
		<cluster>all.homedns.org</cluster>
		<node>jabber1.homedns.org</node>
		<peer>jabber2.homedns.org</peer>
		<heartbeat>8</heartbeat>
        <refresh>600</refresh>
		<map>
		    <service>
				<name>addressbk</name>
				<host>rpc.iic</host>
			</service>
			<service>
				<name>controller</name>
				<host>master.iic</host>
				<backup>master2.iic</backup>
				<hearbeat>8</heartbeat>
				<builtin>xxx</builtin>
				<type>conference</type>
			</service>
		</map>
	  </router>
	  
   Presence:
	  We cache info about available sessions, so if a jabber server becomes
	  unavailable, we can send out unavailable presence for all affected sessions.

	  When a jabber server comes back online and users connect to it, the server
	  automatically probes for presence info for that user's buddies so cache
	  will get rebuilt.
	  
   Service management:
      On startup, services connect to a JSM and send a location message.  This
	  message is forwarded to peer JSMs.

      Services ping the SM periodically, if we don't detect that ping, we assume
	  service is unavailable and will try to direct traffic elsewhere.

      When multiple hosts provide a service, the one with the lowest epoch is chosen.
      Clock skew may cause different hosts to be chosen for a brief period, but once
      the epoch info is exchanged the correct host is chosen.
*/


/* list of cluster members read at startup */
typedef struct node_entry_st
{
	char *node;
	int epoch;		/* time node started */
	int last;		/* time of last traffic from this node */
	int available;	/* currently available */
	int resend;     /* resend status at this time */
	int usercount;	/* # of local users on this node */
} _node_entry,*node_entry;


typedef struct host_entry_st
{
	struct service_entry_st *service;
	char *host;			/* host name configured for real instance */
	char *current_node;	/* node where this can be found */
	int epoch;
	int last;
	int available;
	int restart;
	char *last_host;	/* Internet host name from where component connected */
} _host_entry,*host_entry;


/* service name to host map read at startup */
typedef struct service_entry_st
{
	char *name;			/* my name for this service */
	int type;
	int heartbeat;
	int max_host;
	int options;		/* service options */
	_host_entry hosts[MAX_HOSTS];
} _service_entry,*service_entry;

enum service_type { normal_service = 0, conference_service };
enum request_type { req_unavailable, req_available };
enum service_options { none = 0, cc_session = 1 };


/* keep track of who presence messages were sent to */
typedef struct cache_entry_st
{
	char *from;
	char *to;
	char *node;			/* node sent from */
	struct cache_entry_st *next;
} _cache_entry, *cache_entry;


/* track presence sent by each user session */
typedef struct session_cache_st
{
	cache_entry head;
} _session_cache, *session_cache;


node_entry this_node = NULL;

/* Keep track of session watchers (services with cc_session option specified) */
int num_session_watchers = 0;
service_entry session_watchers[MAX_SESSION_WATCHERS];

static void send_service_status(host_entry host, char *status);

/*
 * User session & presence caching 
 */

/* find highest priority session with resource that begins with specified
   prefix */
static session find_session(mapi m, char *resource)
{
	session cur, top = NULL;
	int priority = -1;
	
	if (m->user != NULL)
	{
		for(cur = m->user->sessions; cur != NULL; cur = cur->next)
		{
			if(j_strncmp(resource, cur->res, j_strlen(resource)) == 0
			   && cur->priority > priority)
			{
				priority = cur->priority;
				top = cur;
			}
		}
	}
	
	return top;
}


static void log_cache(session_cache cache)
{
	if (get_debug_flag())
	{
		int i=0;
		cache_entry cur = cache->head;
		
		while (cur != NULL)
		{
			log_debug("mod_router", "Cache[%d]: %s on %s", i++, cur->from, cur->node);
			cur = cur->next;
		}
	}
}


/* find matching session from user cache */
static cache_entry find_cache_entry(session_cache cache, char *from, char *to, cache_entry *prev)
{
	cache_entry cur;
	cache_entry last = NULL;
	
	for (cur = cache->head; cur != NULL; last = cur, cur = cur->next)
	{
		if (j_strcmp(cur->from, from) == 0
			&& j_strcmp(cur->to, to) == 0)
		{
			if (prev != NULL)
				*prev = last;
			return cur;
		}
	}
	
	return NULL;
}


/* remove cache entry that's gone unavailable */
static void remove_cache_entry(session_cache cache, cache_entry entry, cache_entry prev)
{
	if (prev)
		prev->next = entry->next;
	else
		cache->head = entry->next;
	
	free(entry->from);
	free(entry->to);
	free(entry->node);
	free(entry);
}


/* send unavailable presence to users in our cache */
static void clean_cache_by_node(xht h, const char *key, void *val, void *arg)
{
	session_cache cache = (session_cache)val;
	char *unavailable_node = (char *)arg;
	cache_entry cur;
	
	cur = cache->head;
	while (cur != NULL)
	{
		log_debug("mod_routed", "Cache: checking %s on %s", cur->from, cur->node);
		
		if (j_strcmp(cur->node, unavailable_node) == 0)
		{
			xmlnode x;
			
			log_debug("mod_router", "Heartbeat: removing unreachable user %s", cur->from);
			
			/* send unavailable presence */
			x = jutil_presnew(JPACKET__UNAVAILABLE, cur->to, NULL);
			xmlnode_put_attrib(x, "from", cur->from);
			xmlnode_put_attrib(x, "routed", cur->node);

			cur = cur->next;

			/* we'll see the offline presence and will remove cache entry then */
			deliver(dpacket_new(x), mod_router__jsmi->i);
		}
		else
		{
			cur = cur->next;
		}
	}		
}


/* we need to track presence packets from all JSMs */
static void cache_presence(jpacket packet, int online)
{
	char *user;
	char *from, *to;
	char *routed;
	char *node;
	session_cache cache;
	
	if (packet->from == NULL 
		|| packet->to == NULL
		|| j_strcmp(packet->from->server, cluster_name) != 0)
		return;

	/* figure out who presence is for & to */
	user = packet->from->user;
	from = jid_full(packet->from);
	to = jid_full(packet->to);
	
	/* figure out which server user is attached to */	
	routed = xmlnode_get_attrib(packet->x, "routed");
	node = routed != NULL ? routed : node_name;

	cache = (session_cache)xhash_get(location_cache, user);
	if (online)
	{		
		cache_entry entry;
		
		if (cache == NULL)
		{
			/* new user, create cache */
			cache = pmalloco(location_cache->p, sizeof(_session_cache));
			cache->head = NULL;
			xhash_put(location_cache, pstrdup(location_cache->p, user), cache);
		}
		
		entry = find_cache_entry(cache, from, to, NULL);
		if (entry == NULL)
		{
			/* create and insert into chain for this user */
			entry = calloc(1, sizeof(_cache_entry));
			entry->next = cache->head ? cache->head : NULL;
			cache->head = entry;

			entry->from = j_strdup(from);
			entry->to = j_strdup(to);
			entry->node = j_strdup(node);
		}
		else
		{
			if (j_strcmp(entry->node, node) != 0)
			{
				/* update entry */		
				free(entry->node);
				entry->node = j_strdup(node);
			}
		}
		
		log_debug("mod_router", "Presence: user %s on node %s", from, node);
		log_cache(cache);
	}
	else /* offline */
	{
		cache_entry entry, prev;

		/* if we can find cache entry, remove it */		
		if (cache != NULL)
		{
			entry = find_cache_entry(cache, from, to, &prev);
			if (entry != NULL)
			{
				remove_cache_entry(cache, entry, prev);
			}
		}
		
		log_debug("mod_router", "Presence: user %s offline", from);
		if (cache != NULL)
			log_cache(cache);
	}
}

/*
 * Service & node tracking
 */

/* find entry for service */
static service_entry find_service(mapi m, char *service_name)
{
	service_entry service;
	service = xhash_get(host_map, service_name);
	return service;
}


/* find active host for service, if any */
static host_entry find_host(service_entry service)
{
	host_entry host = NULL;
	int best = -1;
	int i;

	/* single instance service, return regardless of availability */
	if (service->max_host == 1)
	{
		host = &service->hosts[0];
	}
	else
	{
		/* find connect host with lowest epoch */
		for (i = 0; i < service->max_host; i++)
		{
			if (service->hosts[i].available 
				&& (best == -1 || service->hosts[i].epoch < service->hosts[best].epoch))
				best = i;
		}

		if (best >= 0)
			host = &service->hosts[best];
	}

	if (host != NULL)
	{
		log_debug("mod_router", "Service: using host %s for %s on %s", host->host, service->name,  host->current_node);
	}

	return host;
}


/* we're about to send packet to host, see if it needs to be prepared */
static void use_host(host_entry host)
{
	if (host->restart && j_strcmp(host->current_node, node_name) == 0)
	{
		log_debug("mod_router", "Service: sending start message to %s", host->host);
		host->restart = FALSE;
		send_service_status(host, "started");
	}
}


/* mark host as currently unavailable */
static void set_host_unavailable(host_entry host)
{
	host->available = 0;
	if (host->current_node)
	{
		free(host->current_node);
		host->current_node = NULL;
	}

	log_debug("mod_router", "Service: host %s no longer available", host->host);
}


/* mark host as currently available */
static void set_host_available(host_entry host, char *node, int epoch,
	const char *last_host)
{
	char *use_node = node != NULL ? node : node_name;

    /* if epoch has changed, we know service was restarted */
	if (epoch != 0 && epoch != host->epoch)
	{
		log_debug("mod_router", "Service: host %s was restarted", host->host);
		host->restart = TRUE;
	}

	host->available = TRUE;
	host->epoch = epoch;
	host->last = time(NULL);
	if (j_strcmp(use_node, host->current_node) != 0)
	{
		if (host->current_node)
			free(host->current_node);
		host->current_node = strdup(use_node);
		/* also set restart? */
	}
	if (host->last_host)
	{
	    free(host->last_host);
	    host->last_host = NULL;
	}
	if (last_host)
	  host->last_host = strdup(last_host);
	
	log_debug("mod_router", "Service: host %s available at %s from %s", host->host, host->current_node, host->last_host);
}


/* send heartbeat message to peer node */
void static send_sm_heartbeat()
{
	jid from, to;
	xmlnode x, q;
	char buff[16];
	extern int js__usercount;
	
	log_debug("mod_router", "Heartbeat: sending beat to peer %s", other_node);
	
	if (this_node) this_node->usercount = js__usercount;

    x = jutil_iqnew(JPACKET__SET, "iic:heartbeat");
    from = jid_new(xmlnode_pool(x), node_name);
    to = jid_new(xmlnode_pool(x), other_node);
    jid_set(from, "heartbeat-sm.service", JID_USER);
    jid_set(to, "heartbeat-sm.service", JID_USER);
    
    xmlnode_put_attrib(x, "to", jid_full(to));
    xmlnode_put_attrib(x, "from", jid_full(from));
    xmlnode_put_attrib(x, "routed", node_name);

    q = xmlnode_get_tag(x, "query");
    q = xmlnode_insert_tag(q, "beat");
    
    sprintf(buff, "%d", this_node->epoch);
    xmlnode_put_attrib(q, "epoch", buff);
    sprintf(buff, "%d", js__usercount);
    xmlnode_put_attrib(q, "usercount", buff);
    
    deliver(dpacket_new(x), mod_router__jsmi->i);
}


/* send a service location update message */
static void send_location_message(host_entry host, char *status, char *info)
{
	xmlnode x, q;
	jid from, to;
	char buff[32];

	log_debug("mod_router", "Service: send location %s for %s", status, host->host);

	/* note that we send to cluster address, then pick up message later
	   handle ourselves, and forward to other nodes */	
	x = jutil_iqnew(JPACKET__SET, "iic:location");
	from = jid_new(xmlnode_pool(x), host->host);
	to = jid_new(xmlnode_pool(x), cluster_name);
	jid_set(to, "location.service", JID_USER);
	
	xmlnode_put_attrib(x, "to", jid_full(to));
	xmlnode_put_attrib(x, "from", jid_full(from));

	q = xmlnode_get_tag(x, "query");
	q = xmlnode_insert_tag(q, status);
	xmlnode_put_attrib(q, "host", host->host);
	xmlnode_put_attrib(q, "service", host->service->name);
	if (host->current_node)
		xmlnode_put_attrib(q, "node", host->current_node);
    if (info)
        xmlnode_put_attrib(q, "info", info);
	
	sprintf(buff, "%d", host->epoch);
	xmlnode_put_attrib(q, "epoch", buff);
	xmlnode_put_attrib(q, "lasthost", (host->last_host ? host->last_host : ""));	

	deliver(dpacket_new(x), mod_router__jsmi->i);
}


/* send message to service */
static void send_service_status(host_entry host, char *status)
{
	xmlnode x, q;
	jid from, to;

	log_debug("mod_router", "Service: sending status %s to %s", status, host->host);

	x = jutil_iqnew(JPACKET__SET, "iic:service");
	from = jid_new(xmlnode_pool(x), cluster_name);
	jid_set(from, "location.service", JID_USER);
	to = jid_new(xmlnode_pool(x), host->host);
	
	xmlnode_put_attrib(x, "to", jid_full(to));
	xmlnode_put_attrib(x, "from", jid_full(from));

	q = xmlnode_get_tag(x, "query");
	q = xmlnode_insert_tag(q, status);
	xmlnode_put_attrib(q, "service", host->service->name);
	
	deliver(dpacket_new(x), mod_router__jsmi->i);
}


/* send session message to service */
static void send_session_status(host_entry host, char * user_jid, char * status)
{
	xmlnode x, q;
	jid from, to;

	log_debug("mod_router", "Service: sending session status %s for %s to %s", status, user_jid, host->host);

	x = jutil_iqnew(JPACKET__SET, "iic:session");
	from = jid_new(xmlnode_pool(x), cluster_name);
	jid_set(from, "service", JID_USER);
	to = jid_new(xmlnode_pool(x), host->host);
	
	xmlnode_put_attrib(x, "to", jid_full(to));
	xmlnode_put_attrib(x, "from", jid_full(from));

	q = xmlnode_get_tag(x, "query");
	q = xmlnode_insert_tag(q, status);
	xmlnode_put_attrib(q, "id", user_jid);

	deliver(dpacket_new(x), mod_router__jsmi->i);
}


/* walk list of hosts and send unavailable messages for those that are now disconnected */
static void services_unavailable_by_node(xht h, const char *key, void *val, void *arg)
{
	host_entry host = (host_entry) val;
	char *unavailable_node = (char *)arg;

	if (j_strcmp(host->current_node, unavailable_node) == 0)
	{
		log_debug("mod_router", "Service: host %s is now unreachable", host->host);
		set_host_unavailable(host);        
	}
}


/* walk list of hosts and send available messages for benefit of new sm */
static void services_available_by_node(xht h, const char *key, void *val, void *arg)
{
	host_entry host = (host_entry) val;
	service_entry service = host->service;
	char *node = (char *)arg;

	if (host->available && (j_strcmp(host->current_node, node) == 0))
	{
		log_debug("mod_router", "Service: service %s available here at %s", service->name, host->host);
		send_location_message(host, "available", "refresh");
	}
}


/* reset address from local host to external service address */
static int convert_packet_from(xmlnode pkt, jid fid, int for_forward)
{
	host_entry host;
	static char * service_name = ".service";
	static int service_len = 8;
	static char * conf_name = ".conference";
	static int conf_len = 11;
	
	if (fid->server == NULL)
		return 0;
	
	/* see if was sent to local service */
	host = xhash_get(service_map, fid->server);
	if (host != NULL)
	{
		if (host->service->type == normal_service)
		{
			/* convert back into "name.service@node" */
			char *dest = pmalloco(xmlnode_pool(pkt), strlen(host->service->name) + service_len + 1);
			strcpy(dest, host->service->name);
			strcat(dest, service_name);
			jid_set(fid, dest, JID_USER);
			jid_set(fid, for_forward ? node_name : cluster_name, JID_SERVER);
			log_debug("mod_router", "Resetting packet source to %s", jid_full(fid));
			return 1;
		}
		else if (host->service->type == conference_service)
		{
			/* convert back into "room.conf.conference@node" */
			char *dest;
			if (fid->user)
			{	/* with room name */
				dest = pmalloco(xmlnode_pool(pkt), strlen(host->service->name) +
							    strlen(fid->user) + conf_len + 2);
				strcpy(dest, fid->user);
				strcat(dest, ".");
			}
			else
			{	/* without room name */
				dest = pmalloco(xmlnode_pool(pkt), strlen(host->service->name) + conf_len + 1);
			}
			strcat(dest, host->service->name);
			strcat(dest, conf_name);
			jid_set(fid, dest, JID_USER);
			jid_set(fid, for_forward ? node_name : cluster_name, JID_SERVER);
			log_debug("mod_router", "Resetting packet source to %s", jid_full(fid));
			return 1;
		}
	}
	
	return 0;
}	

/*
 * Mod_router services
 */

/* Don't want offline functionality, but do want messages not directed to a particular user
   session to go through. */
mreturn mod_router_offline(mapi m, void *arg)
{
	session top;
	
    if(m->packet->type == JPACKET_MESSAGE || m->packet->type == JPACKET_IQ) 
	{
		log_debug("mod_router", "checking offline packet for %s", m->user->user);
		
		/* User is on this server, just give it to them */
		if((top = find_session(m, "iic")) != NULL) 
		{
			js_session_to(top,m->packet);
			return M_HANDLED;
		}
        
        return M_PASS;
	}

    /* ignore = we'll never see this kind of packet again */
    return M_IGNORE;
}


/* forward packet to other server */
void mod_router_forward(mapi m, xmlnode pkt, char *to_node)
{
	jid fid, tid;

	xmlnode_put_attrib(pkt, "routed", node_name);

	fid = jid_new(xmlnode_pool(pkt), xmlnode_get_attrib(pkt, "from"));
	if (fid->user != NULL)
	{
		/* convert from cluster name to this node name */
		if (j_strcmp(fid->server, cluster_name) == 0)
			jid_set(fid, node_name, JID_SERVER);
	}
	else
	{
		/* see if was sent to local host, convert to service address */
		convert_packet_from(pkt, fid, TRUE);
	}

	tid = jid_new(xmlnode_pool(pkt), xmlnode_get_attrib(pkt, "to"));
	jid_set(tid, to_node, JID_SERVER);
	
	xmlnode_put_attrib(pkt, "from", jid_full(fid));
	xmlnode_put_attrib(pkt, "to", jid_full(tid));

	log_debug("mod_router", "forwarding to %s", jid_full(tid));

	/* reset the packet and deliver it again */
	jpacket_reset(m->packet);
	js_deliver(m->si, m->packet);
}


/* restore packet forwarded from other server */
void mod_router_restore(mapi m, xmlnode pkt)
{
	jid fid, tid;

	fid = jid_new(xmlnode_pool(pkt), xmlnode_get_attrib(pkt, "from"));
	jid_set(fid, cluster_name, JID_SERVER);

	tid = jid_new(xmlnode_pool(pkt), xmlnode_get_attrib(pkt, "to"));
	jid_set(tid, cluster_name, JID_SERVER);

	xmlnode_put_attrib(pkt, "from", jid_full(fid));
	xmlnode_put_attrib(pkt, "to", jid_full(tid));
	
	log_debug("mod_router", "restoring to %s", xmlnode_get_attrib(pkt, "to"));

	jpacket_reset(m->packet);
	js_deliver(m->si, m->packet);
}


/* force presence updates through all sessions, thanks to 'smarter' presence logic */
void mod_presence_pforce(udata u, jid to, int uflag)
{
    session s;
    xmlnode x;

    log_debug(ZONE,"brute forcing presence updates");

    /* loop through all the sessions */
    for(s = u->sessions; s != NULL; s = s->next)
    {
        if(uflag)
            x = jutil_presnew(JPACKET__UNAVAILABLE,NULL,NULL);
        else
            x = xmlnode_dup(s->presence);
        xmlnode_put_attrib(x,"to",jid_full(to));
        js_session_from(s,jpacket_new(x));
    }
}


/* backdoor for user roster management */
mreturn mod_router_roster(mapi m)
{
	xmlnode info;
	char *command;
	jid user_jid, subscriber_jid;
	udata user;

	/* example:
	   <iq from="some component" to="roster.service@cluster" type="set">
	       <query xmlns="iic:roster">
		       <[un]subscribed user='jid' subscriber='jid/>
		   </query>
	   </iq>
	*/
	
	info = xmlnode_get_tag(m->packet->x, "query");
	if (jpacket_subtype(m->packet) != JPACKET__SET || info == NULL)
		goto bad_request;
                
	info = xmlnode_get_firstchild(info);
	if (info == NULL)
		goto bad_request;
            
	command = xmlnode_get_name(info);
	user_jid = jid_user(jid_new(xmlnode_pool(m->packet->x),xmlnode_get_attrib(info, "user")));
	subscriber_jid = jid_user(jid_new(xmlnode_pool(m->packet->x),xmlnode_get_attrib(info, "subscriber")));
	if (user_jid == NULL || subscriber_jid == NULL)
		goto bad_request;

	user = js_user(mod_router__jsmi, user_jid, NULL);

	if (user != NULL)
	{
		if (j_strcmp(command, "add") == 0)
		{
            ++user->ref;
			jid_append(js_trustees(user),subscriber_jid);
			mod_presence_pforce(user, subscriber_jid, 0);
            --user->ref;
		}
		else if (j_strcmp(command, "remove") == 0)
		{
			jid curr;
            ++user->ref;
			for(curr = js_trustees(user);curr != NULL && jid_cmp(curr->next,subscriber_jid) != 0;curr = curr->next);
			if(curr != NULL && curr->next != NULL)
				curr->next = curr->next->next;
			mod_presence_pforce(user, subscriber_jid, 1);
            --user->ref;
		}
		else
			goto bad_request;
	}
		
	if (other_node != NULL && xmlnode_get_attrib(m->packet->x, "routed") == NULL)
		mod_router_forward(m, m->packet->x, other_node);
	else
		xmlnode_free(m->packet->x);
	return M_HANDLED;
	
bad_request:
	log_debug("mod_router", "Unable to process reset roster");
    jutil_error(m->packet->x,(terror){500,"Invalid reset roster request"});
	return M_HANDLED;
}


/* turn on/off jabber debug logging */
mreturn mod_router_service_loglevel(mapi m)
{
	xmlnode info;
	char *level;
	int flag;
	
	/* example:
	   <iq from="some component" to="loglevel.service@cluster" type="set">
	       <query xmlns="iic:loglevel">
		       <level>1</level>
		   </query>
	   </iq>
	*/
	
	info = xmlnode_get_tag(m->packet->x, "query");
	if (jpacket_subtype(m->packet) != JPACKET__SET || info == NULL)
		goto bad_request;
	level = xmlnode_get_tag_data(info, "level");

	if (level == NULL)
		goto bad_request;
	
	flag = atoi(level);
	
	if (!flag)
		log_debug("mod_router", "Disabling debug logging");
	
	set_debug_flag(flag);

	log_debug("mod_router", "Debug level set to %d", get_debug_flag());
	
	if (other_node != NULL && xmlnode_get_attrib(m->packet->x, "routed") == NULL)
		mod_router_forward(m, m->packet->x, other_node);
	else
		xmlnode_free(m->packet->x);
	return M_HANDLED;
	
bad_request:
	log_debug("mod_router", "Unable to process log level request");
    jutil_error(m->packet->x,(terror){500,"Invalid log level request"});
	return M_HANDLED;
}


static void dump_host_entry_log(xht h, const char *key, void *val, void *arg)
{
    host_entry host = (host_entry) val;
    xmlnode q = (xmlnode) arg;
    char buff[16];

    log_debug("mod_router", "walk_host: key: %s host: %s node: %s avail: %d", key, host->host, host->current_node, host->available, arg);

    if (q != NULL)
    {
        q = xmlnode_insert_tag(q, "service");
        xmlnode_put_attrib(q, "name", host->service->name);
        xmlnode_put_attrib(q, "id", host->host);
        // if no location, host->current_node is NULL so put in empty string
        xmlnode_put_attrib(q, "location", host->current_node == NULL ? "" : host->current_node);
        sprintf(buff, "%d", host->available);
        xmlnode_put_attrib(q, "available", buff);
	    xmlnode_put_attrib(q, "lasthost", (host->last_host ? host->last_host : ""));
    }
}

static void dump_peer_entry_log(xht h, const char *key, void *val, void *arg)
{
    node_entry node = (node_entry)val;
    xmlnode q = (xmlnode) arg;
    char buff[16];
	extern int js__usercount;

    log_debug("mod_router", "walk_node: key: %s node: %s usercount: %d avail: %d",
    		  key, node->node, node->usercount, node->available);


    if (q != NULL)
    {
        if (this_node) this_node->usercount = js__usercount;

        q = xmlnode_insert_tag(q, "peer");
        // if no location, host->current_node is NULL so put in empty string
        xmlnode_put_attrib(q, "location", node->node == NULL ? "" : node->node);
        sprintf(buff, "%d", node->available);
        xmlnode_put_attrib(q, "available", buff);
        sprintf(buff, "%d", node->usercount);
        xmlnode_put_attrib(q, "usercount", buff);
    }
}

/*
static void dump_node_entry_log(xht h, const char *key, void *val, void *arg)
{
    node_entry node = (node_entry)val;

    log_debug("mod_router", "walk_node: key: %s node: %s avail: %d", key, node->node, node->available, arg);
}
*/

static void send_health_message(mapi m, const char *id)
{
    xmlnode x, q;
    jid from, to;

    log_debug("mod_router", "Service: send health: id: %s", id);

    x = jutil_iqnew(JPACKET__SET, "iic:health");

    from = jid_new(xmlnode_pool(x), cluster_name);
    to = jid_new(xmlnode_pool(x), xmlnode_get_attrib(m->packet->x, "from"));

    if (m->packet->to->resource)
        jid_set(to, m->packet->to->resource, JID_RESOURCE);
    
    xmlnode_put_attrib(x, "to", jid_full(to));
    xmlnode_put_attrib(x, "from", jid_full(from));
    xmlnode_put_attrib(x, "id", id);

    q = xmlnode_get_tag(x, "query");
    q = xmlnode_insert_tag(q, "result");
    q = xmlnode_insert_tag(q, "services");

    // walk through the host table and add attributes to each
    xhash_walk(service_map, dump_host_entry_log, q);
    // walk through the peer list and append entries
    xhash_walk(node_list, dump_peer_entry_log, q);

    deliver(dpacket_new(x), mod_router__jsmi->i);
}


/* return status of components on servers */
mreturn mod_router_service_health(mapi m)
{
    xmlnode info;
    
    /* example:
       <iq from="some component" to="health.service@cluster" type="set">
       <query xmlns="iic:health">
       </query>
       </iq>
    */
    
    info = xmlnode_get_tag(m->packet->x, "query");
    if (jpacket_subtype(m->packet) != JPACKET__SET || info == NULL)
        goto bad_request;

    log_debug("mod_router", "Retrieving health for: %s", xmlnode_get_attrib(m->packet->x, "from"));
    
    send_health_message(m, xmlnode_get_attrib(m->packet->x, "id"));

    xmlnode_free(m->packet->x);
    return M_HANDLED;
    
 bad_request:
    log_debug("mod_router", "Unable to process health request");
    jutil_error(m->packet->x,(terror){500,"Invalid health request"});
    return M_HANDLED;
}


/* process service location request -- update map with new node for service */
mreturn mod_router_service_location(mapi m)
{
	host_entry host;
	service_entry service;
	xmlnode info;
	char *request;
	char *current_node;
	char *host_name, *p;
	char *lasthost;
	char *service_name;
	char *host_epoch;
        char *status_info;
	int epoch = 0;
	int type;

	/* example:
	   <iq from="some component" to="location.service@cluster" type="set">
	       <query xmlns="iic:location">
		       <available host="host" service="servicename" node="node" epoch="starttime" lasthost="hostname"/>
		   </query>
	   </iq>
	   other location tag is unavailable
	*/

    if (jpacket_subtype(m->packet) == JPACKET__ERROR)
    {
        info = xmlnode_get_tag(m->packet->x, "error");
        if (info != NULL 
            && j_strcmp(xmlnode_get_attrib(info, "code"), "502") == 0 
            && (this_node->resend == 0 || this_node->resend > time(NULL) + 30))
        {
            this_node->resend = time(NULL) + 30;
            log_debug("mod_router", "Service: status message was bounced, retrying (%d)", this_node->resend);
        }
    }
    
	/* Parse message */
	info = xmlnode_get_tag(m->packet->x, "query");
	if (jpacket_subtype(m->packet) != JPACKET__SET || info == NULL)
		goto bad_request;

	info = xmlnode_get_firstchild(info);
	if (info == NULL)
		goto bad_request;
	request = xmlnode_get_name(info);
	
	if (j_strcmp(request, "available") == 0)
		type = req_available;
	else if (j_strcmp(request, "unavailable") == 0)
		type = req_unavailable;
	else
		goto bad_request;

	host_name = xmlnode_get_attrib(info, "host");
	current_node = xmlnode_get_attrib(info, "node");
	service_name = xmlnode_get_attrib(info, "service");
	host_epoch = xmlnode_get_attrib(info, "epoch");
        status_info = xmlnode_get_attrib(info, "info");
        lasthost = xmlnode_get_attrib(info, "lasthost");

	if (host_epoch)
		epoch = atoi(host_epoch);

	if (host_name)
	{
		p = strchr(host_name, '@');
		if (p) 
			host_name = p+1;
	}
	
	/* Get host name...could use name from packet, but it gets switched to 
	   service name when forwarded */
	host = xhash_get(service_map, host_name);
	if (host == NULL)
		goto bad_request;
	service = host->service;

    /* make sure host name belongs to service */
	if (j_strcmp(service_name, service->name) != 0)
		goto bad_request;

	/* see if message is out-of-date (from earlier connection by this service) */
	if (host->epoch > epoch && j_strcmp(status_info, "refresh") == 0)
	{
		log_debug("mod_router", "Service: out of date location message, ignoring");
		xmlnode_free(m->packet->x);
		return M_HANDLED;

		/*** if service is here, send available message back? */
	}

	/* Store new state info */
	switch (type)
	{
		case req_available:
			set_host_available(host, current_node, epoch, lasthost);
			break;
		case req_unavailable:
			set_host_unavailable(host);
			break;
	}
	
	/* If directed to us, broadcast everywhere */
	if (other_node != NULL && xmlnode_get_attrib(m->packet->x, "routed") == NULL)
	{
		if (current_node == NULL)
			xmlnode_put_attrib(info, "node", node_name);
		mod_router_forward(m, m->packet->x, other_node);
	}
	else
	{
		xmlnode_free(m->packet->x);
	}

	return M_HANDLED;

bad_request:
	log_debug("mod_router", "Unable to process location request");
    jutil_error(m->packet->x,(terror){500,"Invalid service location request"});
	return M_HANDLED;
}


/* process service heartbeat */
mreturn mod_router_service_heartbeat(mapi m)
{
	xmlnode q;
	char *host_name, *p;
	host_entry host;
		
	/* Ping message: 
	   <iq type='set'>
	     <query xmlns='iic:heartbeat'>
	        <beat host='host address'/>
	     </query>
	   </iq>
	*/
	
	if (m->packet->from == NULL)
		goto bad_request;

	/* get host name that sent ping */
	q = xmlnode_get_tag(m->packet->x, "query");
	if (j_strcmp(xmlnode_get_attrib(q, "xmlns"), "iic:heartbeat") != 0)
		goto bad_request;
	q = xmlnode_get_tag(q, "beat");
	host_name = xmlnode_get_attrib(q, "host");
	if (host_name)
	{
		p = strchr(host_name, '@');
		if (p)
			host_name = p + 1;
	}
	
	if (jpacket_subtype(m->packet) == JPACKET__SET)
	{
		host = xhash_get(service_map, host_name);
	
		if (host != NULL)
		{
			log_debug("mod_router", "Heartbeat: Ping from service %s", host_name);
			host->last = time(NULL);
			
            /* we know service is here, make sure everyone else does */
			if (!host->available || j_strcmp(host->current_node, node_name) != 0)
				send_location_message(host, "available", NULL);
		}
	}
		
	xmlnode_free(m->packet->x);
	return M_HANDLED;
	
bad_request:
	log_debug("mod_router", "Heartbeat: invalid ping from service");
	xmlnode_free(m->packet->x);
	return M_HANDLED;
}


/* process jsm heartbeat */
mreturn mod_router_sm_hearbeat(mapi m)
{
	node_entry node;
	char *from_node;
	xmlnode q;
		
	/* Ping from other node 
	   <iq type='set'>
	     <query xmlns='iic:heartbeat'>
	        <beat epoch='local epoch' usercount='local users'/>
	     </query>
	   </iq>
	*/
	
	if (m->packet->from == NULL)
		goto bad_request;
		
	q = xmlnode_get_tag(m->packet->x, "query/beat");
	
	if (jpacket_subtype(m->packet) == JPACKET__SET)
	{
		from_node = xmlnode_get_attrib(m->packet->x, "routed");
		node = xhash_get(node_list, from_node);
	
		/* we got a message from other node, mark it as available */
		if (node != NULL)
		{
			int was_available = node->available && node->epoch;
			char *epoch = xmlnode_get_attrib(q, "epoch");
			char *usercount = xmlnode_get_attrib(q, "usercount");
	
			node->last = time(NULL);
			node->available = 1;
			node->usercount = (usercount ? atoi(usercount) : -1);
			
			log_debug("mod_router", "Heartbeat: beat received from peer %s (%d)", from_node, node->last);
		
			if (epoch != NULL && atoi(epoch) != node->epoch)
			{
				/* epoch change, node must have been restarted */
				node->epoch = atoi(epoch);

				log_debug("mod_router", "Heartbeat: Peer server %s available (%d)", from_node, node->epoch);

				/* epoch changed, but we never detected peer as unavailable, clean up now */
				if (was_available)
				{
					/* walk sessions and generate unavailable presence for users on server */
					xhash_walk(location_cache, clean_cache_by_node, from_node);

					/* also see if any services must be marked unavailable */
					xhash_walk(service_map, services_unavailable_by_node, from_node);
				}

                /* Let other server know who we are */
                send_sm_heartbeat();				
				
				/* send out location messages for benefit of new server */
                log_debug("mod_router", "Service: sending service locations");
                xhash_walk(service_map, services_available_by_node, node_name);
			}
		}			
	}
	
	xmlnode_free(m->packet->x);
	return M_HANDLED;
	
bad_request:
	log_debug("mod_router", "Heartbeat: invalid sm heartbeat");
	xmlnode_free(m->packet->x);
	return M_HANDLED;
}


/* Handle packet addressed to JSM */
mreturn mod_router_service(mapi m, char *service_name, char *sub_service)
{
	service_entry service;
	host_entry host;
	char *routed;
	jid to;
	
	log_debug("mod_router", "server: %s service name: %s", xmlnode2str(m->packet->x), service_name);

	/* check for special "services" first */
	if (j_strcmp(service_name, "location") == 0)
	{
		/* a service is updating it's location */
		return mod_router_service_location(m);
	}
	else if (j_strcmp(service_name, "heartbeat-sm") == 0)
	{
		return mod_router_sm_hearbeat(m);
	}
	else if (j_strcmp(service_name, "heartbeat-service") == 0)
	{
		return mod_router_service_heartbeat(m);
	}
	else if (j_strcmp(service_name, "loglevel") == 0)
	{
		return mod_router_service_loglevel(m);
	}
	else if (j_strcmp(service_name, "health-service") == 0)
	{
		return mod_router_service_health(m);
	}
	else if (j_strcmp(service_name, "roster") == 0)
	{
		return mod_router_roster(m);
	}

	routed = xmlnode_get_attrib(m->packet->x, "routed");

	/* see if sent to registered service name */
	service = find_service(m, service_name);
	if (service != NULL)
	{
		host = find_host(service);
		
		switch (jpacket_subtype(m->packet))
		{
		case JPACKET__AVAILABLE:
		case JPACKET__UNAVAILABLE:
		case JPACKET__GET:
		case JPACKET__SET:
	    case JPACKET__CHAT:
	    case JPACKET__GROUPCHAT:
		case JPACKET__RESULT:
		case JPACKET__ERROR:
			if (j_strcmp(m->packet->to->server, cluster_name) == 0 
				&& host != NULL
				&& j_strcmp(node_name, host->current_node) == 0)
			{
				/* handle locally */
				use_host(host);
				to = jid_new(xmlnode_pool(m->packet->x), host->host);
				if (sub_service)
				{
					jid_set(to, sub_service, JID_USER);
				}
				if (m->packet->to->resource)
					jid_set(to, m->packet->to->resource, JID_RESOURCE);
				log_debug("mod_router", "service here, resetting to %s", jid_full(to));
				xmlnode_put_attrib(m->packet->x, "to", jid_full(to));
				xmlnode_hide_attrib(m->packet->x, "routed");
				jpacket_reset(m->packet);
				js_deliver(m->si, m->packet);
				return M_HANDLED;
			}
	
			/* forward to other node */
			if (other_node != NULL
				&& j_strcmp(m->packet->to->server, cluster_name) == 0 
				&& routed == NULL)
			{
				mod_router_forward(m, m->packet->x, other_node);
				return M_HANDLED;
			}
	
			/* was forwarded to me, restore to/from */
			if (j_strcmp(m->packet->to->server, node_name) == 0 
				&& routed != NULL)
			{
				mod_router_restore(m, m->packet->x);
				return M_HANDLED;
			}
	
			/* not handled here */
			xmlnode_hide_attrib(m->packet->x, "routed");
			return M_PASS;
		}
	}
	
	log_debug("mod_router", "service not defined or not connected");
	jutil_error(m->packet->x,(terror){500,"Unknown service name"});
	return M_HANDLED;
}


/* See if packet needs to be forwarded to another server */
mreturn mod_router_deliver(mapi m, void *arg)
{
	char *routed;
	char *p, *service, *room;

	log_debug("mod_router", "checking %s", xmlnode2str(m->packet->x));

	/* Packet for server */
	if (m->packet->to->user == NULL) 
	{
		log_debug("mod_router", "no to, passing");
		return M_PASS;
	}

	/* See if packet is directed to a mapped service name */
	if (host_map != NULL)
	{
		p = strrchr(m->packet->to->user, '.');
		if (j_strcmp(p, ".service") == 0)
		{
			service = pstrdup(m->packet->p, m->packet->to->user);
			p = strrchr(service, '.');
			*p = '\0';
			
			return mod_router_service(m, service, NULL);
		}
		else if (j_strcmp(p, ".conference") == 0)
		{
			/* handle group chat: <room>.<service>.conference@<cluster> */
			service = pstrdup(m->packet->p, m->packet->to->user);
			p = strrchr(service, '.');
			*p = '\0';
			
			p = strchr(service, '.');
			if (p)
			{
				*p = '\0';
				room = service;
				service = p + 1;
			}
            else
                room = NULL;
			
			return mod_router_service(m, service, room);
		}
	}

	/* check packet type */
    switch(jpacket_subtype(m->packet))
    {
	case JPACKET__AVAILABLE:
		cache_presence(m->packet, TRUE);
		break;
	case JPACKET__UNAVAILABLE:
		cache_presence(m->packet, FALSE);
		break;
	case JPACKET__NONE:
	case JPACKET__PROBE:
    case JPACKET__ERROR:
    case JPACKET__CHAT:
    case JPACKET__GROUPCHAT:
	case JPACKET__GET:
	case JPACKET__SET:
	case JPACKET__RESULT:
		break;
    default:
		log_debug("mod_router", "passing due to packet type");
        return M_PASS;
    }

	/* See if packet will be delivered locally */
	if (m->packet->to->resource != NULL) 
	{
		char *res = m->packet->to->resource;
		session ses;
		
		if (js_session_get(m->user, m->packet->to->resource) != NULL) 
		{
			log_debug("mod_router", "user connected here, passing");
			xmlnode_hide_attrib(m->packet->x, "routed");
			return M_PASS;
		}

		/* reverse normal search: see if packet resource is subset of any session resource */
		if ((ses = find_session(m, res)) != NULL)
		{
			log_debug("mod_router", "user connected here, resetting to %s", jid_full(ses->id));
			xmlnode_put_attrib(m->packet->x, "to", jid_full(ses->id));
			xmlnode_hide_attrib(m->packet->x, "routed");
			jpacket_reset(m->packet);
			js_deliver(m->si, m->packet);
			return M_HANDLED;
		}
	} 
	else 
	{
		/* We let it go through here, will show up later in offline handler */
		if (find_session(m, "iic") != NULL) 
		{
			log_debug("mod_router", "user connected here, passing");
			xmlnode_hide_attrib(m->packet->x, "routed");
			return M_PASS;
		}
	}
	
	routed = xmlnode_get_attrib(m->packet->x, "routed");

	/* forward to other node */
	if (other_node != NULL 
		&& j_strcmp(m->packet->to->server, cluster_name) == 0 
		&& routed == NULL)
	{
		mod_router_forward(m, m->packet->x, other_node);
		return M_HANDLED;
	}

	/* received forwarded, restore from/to */
	if (j_strcmp(m->packet->to->server, node_name) == 0
		&& routed != NULL) 
	{
		mod_router_restore(m, m->packet->x);
		return M_HANDLED;
	}

  	log_debug("mod_router", "packet passed");
	xmlnode_hide_attrib(m->packet->x, "routed");
	return M_PASS;
}


mreturn mod_router_session_end(mapi m, void *arg)
{
	int i;
  	log_debug("mod_router", "%s session end", m->user->user);

	/*** clean cache for this user */

	/* Broadcast session event to interested services */
	for (i = 0; i < num_session_watchers; i++)
	{
		host_entry host = find_host(session_watchers[i]);
		if (host != NULL)
			send_session_status(host, jid_full(m->s->id), "endsession");
	}

	return M_PASS;
}


/* inform cache managers of session begin/ends */
mreturn mod_router_session_begin(mapi m, void *arg)
{
  //js_mapi_session(es_IN, m->s, mod_router_session_in, NULL);
  js_mapi_session(es_END, m->s, mod_router_session_end, NULL);

  log_debug("mod_router", "%s session begin", m->user->user);

  return M_PASS;
}


/* find services attached to us and check for heartbeat message */
static void check_service_heartbeat(xht h, const char *key, void *val, void *arg)
{
	host_entry host = (host_entry)val;

	if (host->available
		&& host->service->heartbeat 
		&& j_strcmp(host->current_node, node_name) == 0)
	{
		log_debug("mod_router", "Heartbeat: checking host %s", host->host);

		if ((host->last + 2*host->service->heartbeat) < time(NULL))
		{
			/* host has not sent heartbeat in required interval */
			log_debug("mod_router", "Heartbeat: host %s unavailable", host->host);

			send_location_message(host, "unavailable", NULL);
		}
	}			
}


/* find other peer SMs and check heartbeat */
static void check_sm_heartbeat(xht h, const char *key, void *val, void *arg)
{
	node_entry node = (node_entry)val;

	if (node->available
		&& sm_heartbeat
		&& j_strcmp(key, node_name) != 0)
	{
        int now = time(NULL);
        
		log_debug("mod_router", "Heartbeat: checking sm %s (%d:%d)", node->node, node->last, now);

		if ((node->last + 4*sm_heartbeat) < now)
		{
			log_debug("mod_router", "Heartbeat: Peer sm %s unavailable", node->node);	
			
			node->available = 0;
			node->epoch = 0;
	
			/* walk sessions and generate unavailable presence for users on server */
			xhash_walk(location_cache, clean_cache_by_node, node->node);
			
			/* also see if any services must be marked unavailable */
			xhash_walk(service_map, services_unavailable_by_node, node->node);

			/* reinform of our services (just in case) */
            log_debug("mod_router", "Service: sending service locations");
			xhash_walk(service_map, services_available_by_node, node_name);
		}
	}
}


result mod_router_beat(void *arg)
{
	instance id = (instance)arg;
    int now;

    if (other_node != NULL)
    {
        /* Send my heartbeat */
        send_sm_heartbeat();
        
        /* Check for other sm */
		xhash_walk(node_list, check_sm_heartbeat, NULL);

		/* Check for services attached to me */
		xhash_walk(service_map, check_service_heartbeat, id);
        
        if (this_node->resend > 0 && (now = time(NULL)) > this_node->resend)
        {
            log_debug("mod_router", "Service: resending service locations (%d)", this_node->resend);
            this_node->resend = sm_refresh > 0 ? now + sm_refresh : 0;
            xhash_walk(service_map, services_available_by_node, node_name);
        }
    }
   
    return r_DONE;
}


/* Restore packet before processing by JSM */
result mod_router_filter(instance id, dpacket p, void *arg)
{
	char *routed;
	jid fid, tid;
	
    log_debug("mod_router", "filtering %s", xmlnode2str(p->x));

	routed = xmlnode_get_attrib(p->x, "routed");
	
	if (j_strcmp(p->id->server, node_name) == 0
		&& routed != NULL)
	{
		fid = jid_new(xmlnode_pool(p->x), xmlnode_get_attrib(p->x, "from"));
		jid_set(fid, cluster_name, JID_SERVER);
	
		tid = jid_new(xmlnode_pool(p->x), xmlnode_get_attrib(p->x, "to"));
		jid_set(tid, cluster_name, JID_SERVER);
	
		xmlnode_put_attrib(p->x, "from", jid_full(fid));
		xmlnode_put_attrib(p->x, "to", jid_full(tid));
		
		log_debug("mod_router", "restoring to %s", xmlnode_get_attrib(p->x, "to"));

		/* Create new dpacket...original should get free'd when this pool is freed */
	    deliver(dpacket_new(p->x), id);

		/* for o_PREDELIVER, r_DONE indicates to stop processing packet */
    	return r_DONE;
	}
	else if (p->type == p_NORM)
	{
		/* see if from address is for local host and convert to service address */
		fid = jid_new(xmlnode_pool(p->x), xmlnode_get_attrib(p->x, "from"));
		if (convert_packet_from(p->x, fid, FALSE))
		{
			xmlnode_put_attrib(p->x, "from", jid_full(fid));
		    deliver(dpacket_new(p->x), id);
			return r_DONE;
		}
	}
    
    return r_PASS;
}


/* read config and set up handlers */
void mod_router(jsmi si)
{
  xmlnode config;
  xmlnode child;
  node_entry node;
  char *heartbeat, *refresh;
  mod_router__jsmi = si;

  /* Use this instead of mod_offline */
  js_mapi_register(si, e_OFFLINE, mod_router_offline, NULL);

  config = js_config(si, "router");
  if (config == NULL) 
  {
	log_debug("mod_router", "no configuration for mod_router, not initializing");
	return;
  }

  /* read cluster config */  
  cluster_name = xmlnode_get_tag_data(config, "cluster");
  node_name = xmlnode_get_tag_data(config, "node");  
  other_node = xmlnode_get_tag_data(config, "peer");  
  heartbeat = xmlnode_get_tag_data(config, "heartbeat");
  if (heartbeat != NULL)
  {
    /* ping is sent every "heartbeat" seconds to peer */
	sm_heartbeat = atoi(heartbeat);
  }
  refresh = xmlnode_get_tag_data(config, "refresh");
  if (refresh != NULL)
  {
    /* our service map is broadcast every "refresh" seconds */
	sm_refresh = atoi(refresh);
  }

  log_debug("mod_router", "config: %s %s %s %d %d", cluster_name, node_name, other_node, 
            sm_heartbeat, sm_refresh);
  
  /* set up node list */
  node_list = xhash_new(13);
  this_node = pmalloco(node_list->p, sizeof(_node_entry));
  this_node->available = 1;
  this_node->epoch = time(NULL);
  this_node->node = node_name;
  this_node->resend = sm_refresh > 0 ? time(NULL) + sm_refresh : 0;
  xhash_put(node_list, node_name, this_node);

  node = pmalloco(node_list->p, sizeof(_node_entry));
  node->node = other_node;
  xhash_put(node_list, other_node, node);
  
  location_cache = xhash_new(10007);
  
  /* read resource to service map */
  child = xmlnode_get_tag(config, "map");
  if (child != NULL)
  {
	  log_debug("mod_router", "processing service map");
	  
	  host_map = xhash_new(31);
	  service_map = xhash_new(31);
	  
	  child = xmlnode_get_firstchild(child);
	  
	  while (child != NULL)
	  {
		char *name, *alias, *host, *heartbeat, *backup, *type, *builtin, *options;
		service_entry p;
		
		name = xmlnode_get_tag_data(child, "name");
		alias = xmlnode_get_tag_data(child, "alias");
		host = xmlnode_get_tag_data(child, "host");
		backup = xmlnode_get_tag_data(child, "backup");
		heartbeat = xmlnode_get_tag_data(child, "heartbeat");
        type = xmlnode_get_tag_data(child, "type");		 
		builtin = xmlnode_get_tag_data(child, "builtin");
		options = xmlnode_get_tag_data(child, "options");
	
		p = pmalloco(si->p, sizeof(_service_entry));
		p->name = pstrdup(si->p, name);
		p->heartbeat = heartbeat ? atoi(heartbeat) : 0;

        if (j_strcmp(type, "conference") == 0)
			p->type = conference_service;
			
		if (options != NULL && strstr(options, "SendSessionEvents") != NULL) 
		{
			if (num_session_watchers >= MAX_SESSION_WATCHERS)
				log_error("mod_router", "Too many session event watchers");
			else
			{
				p->options |= cc_session;
				session_watchers[num_session_watchers++] = p;
			}
		}
		
		if (host != NULL)
		{
			p->hosts[p->max_host].service = p;
			p->hosts[p->max_host].host = pstrdup(si->p, host);
			xhash_put(service_map, host, &p->hosts[p->max_host]);	// host id -> service name
            
			if (j_strcmp(builtin, host) == 0)
			{
				p->hosts[p->max_host].available = 1;
				p->hosts[p->max_host].current_node = strdup(node_name);
				p->hosts[p->max_host].epoch = time(NULL);
			}
			p->max_host++;
		}
		if (backup != NULL)
		{
			p->hosts[p->max_host].service = p;
			p->hosts[p->max_host].host = pstrdup(si->p, backup);
			xhash_put(service_map, backup, &p->hosts[p->max_host]);	// host id -> service name

			if (j_strcmp(builtin, backup) == 0)
			{
				p->hosts[p->max_host].available = 1;
				p->hosts[p->max_host].current_node = strdup(node_name);
				p->hosts[p->max_host].epoch = time(NULL);
			}
			p->max_host++;
		}
		
		xhash_put(host_map, name, p);		// service name -> host id
		if (alias && *alias)
			xhash_put(host_map, alias, p);
		
		child = xmlnode_get_nextsibling(child);
	  }
  }

  /* set up predeliver handler so we can restore cluster address 
     before JSM starts processing it */
  register_phandler(si->i, o_PREDELIVER, mod_router_filter, NULL);

  js_mapi_register(si, e_DELIVER, mod_router_deliver, NULL);
  js_mapi_register(si, e_SESSION, mod_router_session_begin, NULL);

  /* set up heartbeat & queue one for immediate execution */  
  if (sm_heartbeat)
  {
    mtq_send(NULL, si->p, (mtq_callback)mod_router_beat, si->i);
  	register_beat(sm_heartbeat, mod_router_beat, si->i);
  }
  
  log_debug("mod_router", "mod_router initialized (%s, %s)", cluster_name, node_name);
}
