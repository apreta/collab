archive=$1

if [ "$archive" == "" ]; then
  archive=iic-2-messaging.tar
fi

files="./jabberd/jabber.xml ./jabberd/jabberd/jabberd \
./jabberd/dnsrv/dnsrv.so \
./jabberd/xdb_sql/xdb_sql.so ./jabberd/xdb_sql/xdb_sql.xml \
./jabberd/conference-0.4/conference.so \
./jabberd/dialback/dialback.so ./jabberd/jabberd/pth-1.4.0/.libs/libpth.so \
./jabberd/jsm/jsm.so ./jabberd/pthsock/pthsock_client.so"

echo "Creating archive $archive"
cd ..
tar -czf $archive.gz $files

cd jabberd
echo "Done."
