#!/bin/bash

jabhome=.
config=jabber.xml
prog=jabberd/jabberd
pidfile=jabber.pid

while [ "$1" != "" ]; do
    case $1 in
	--jab-home)
	    jabhome=$2
	    shift 2
	    ;;
	--config)
	    config=$2
	    shift 2
	    ;;
	*)     echo $"$0: Usage: [--jab-home <dir>] [--config <file>]"
	    return 1;;
    esac
done

doit() {
cd $jabhome
# algorithm -- if time between restarts is more than an hour we
# reset the count.  otherwise try at most 10 times to restart the
# process
count=0
maxdiff=3600
while [ $count != 10 ]; do
    let count=count+1;
    start=`date +%s`
    rm -f $pidfile
    echo `pwd`
    $prog -H $jabhome -c $config 2>&1
    now=`date +%s`
    let diff=now-start
    if [ $diff -ge $maxdiff ]; then
	count=0
    fi
    if [ ! -f /var/run/$pidfile ]; then
	break;
    fi
done
}

( doit ) < /dev/null > /dev/null 2>&1 &


