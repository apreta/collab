<jabber>
  <service id="sessions">
    <host>share</host>
    <host><jabberd:cmdline flag="h">@@@CURRENT_XMLROUTER_HNAME@@@</jabberd:cmdline></host>

    <jsm xmlns="jabber:config:jsm">
      <internal>
	<secret><![CDATA[@@@SESSION_CREATION_SECRET@@@]]></secret>
      </internal>
	  
      <router>
        <cluster>share</cluster>
        <node>@@@CURRENT_XMLROUTER_HNAME@@@</node>
        <!--peer>@@@CURRENT_PEER_HNAME@@@</peer-->
        <!--heartbeat>8</heartbeat-->
        <map>
          @@@MTGARCHIVER_MAP@@@
          <service>
            <name>sharemgr</name>
            <host>sharemgr.iic</host>
            <alias>controller</alias>
            <options>SendSessionEvents</options>
		  </service>
          <service>
            <name>mailer</name>
            <host>mailer.iic</host>
          </service>
          <service>
            <name>converter</name>
            <host>converter.iic</host>
          </service>
          <service>
            <name>extapi</name>
            <host>extapi.iic</host>
          </service>
          <service>
            <name>extportal</name>
            <host>extportal.iic</host>
          </service>
	</map>
      </router>
      <filter>
          <default/>
          <max_size>100</max_size>
          <allow>
              <conditions>
                  <ns/>          <!-- Matches if the iq's xmlns is the same as the specified namespace -->
                  <unavailable/> <!-- Flag that matches when the reciever is unavailable (offline) -->
                  <from/>        <!-- Matches if the  sender's jid is the specified jid -->
                  <resource/>    <!-- Matches if the sender's resource (anything after the / in a jid) is the specified resource -->
                  <subject/>     <!-- Matches if the message's subject is the specified subject (no regex yet) -->
                  <body/>        <!-- Matches if the message body is the specified body (no regex yet) --> 
                  <show/>        <!-- Matches if the receiver's presence has a show tag that is the same as the specified text -->
                  <type/>        <!-- Matches if the type of the message is the same as the specified text ("normal" is okay) -->
                  <roster/>      <!-- Flag that matches when the sender is in the receiver's roster -->
                  <group/>       <!-- Matches when the sender is in the specified group -->
              </conditions>
              <actions>
                  <error/>       <!-- Sends back an error message to the sender, with the specified text -->
                  <offline/>     <!-- Flag that stores the message offline -->
                  <forward/>     <!-- forwards the message to the specified jid -->
                  <reply/>       <!-- Sends back a reply to the sender with the specified text in the body -->
                  <continue/>    <!-- Flag that continues rule matching, after a rule matches -->
                  <settype/>     <!-- Changes the type of message to the specified type, before delivery to the receiver -->
              </actions>
          </allow>
      </filter>

      <!-- The server vCard -->
      <vCard>
        <FN>Jabber Server</FN>
        <DESC>A Jabber Server!</DESC>
        <URL>http://foo.bar/</URL>
      </vCard>

      <!-- 
      Registration instructions and required fields. The 
      notify attribute will send the server administrator(s)
      a message after each valid registration if it is set
      to "yes".
      -->

      <register notify="yes">
        <instructions>Choose a username and password to register with this server.</instructions>
        <name/>
      </register>

      <!-- 
      A welcome note that is sent to every new user who registers 
      with your server. Comment it out to disable this function.
      -->

      <welcome>
        <subject>Welcome!</subject>
        <body>Welcome to the Jabber server at localhost -- we hope you enjoy this service! For information about how to use Jabber, visit the Jabber User's Guide at http://docs.jabber.org/</body>
      </welcome>

      <!-- 
      IDs with admin access - these people will receive admin 
      messages (any message to="yourhostname" is an admin
      message).  These addresses must be local ids, they cannot
      be remote addresses.

      Note that they can also send announcements to all
      users of the server, or to all online users. To use
      the announcement feature, you need to send raw xml and be
      logged in as one of the admin users. Here is the syntax 
      for sending an announcement to online users:

        <message to="yourhostname/announce/online">
          <body>announcement here</body>
        </message>

        <message to="yourhostname/announce/motd">
          <body>message (of the day) that is sent only once to all users that are logged in and additionally to new ones as they log in</body>
        </message>

      Sending to /announce/motd/delete will remove any existing
      motd, and to /announce/motd/update will only update the motd
      without re-announcing to all logged in users.

      The <reply> will be the message that is automatically
      sent in response to any admin messages.
      -->

      <!--
      <admin>
        <read>support@localhost</read>
        <write>admin@localhost</write>
        <reply>
          <subject>Auto Reply</subject>
          <body>This is a special administrative address.  Your message was received and forwarded to server administrators.</body>
        </reply>
      </admin>
      -->

      <!--
      This is the resource that checks for updated versions 
      of the Jabber server software. Note that you don't lose 
      any functionality if you comment this out. Removing the
      <update/> config is especially a good strategy if your
      server is behind a firewall. If you want to use this 
      feature, change 'localhost' to the hostname or IP address 
      of your server, making sure that it is the same as your 
      entry for <host/> above.
      -->

<!--
      <update><jabberd:cmdline flag="h">localhost</jabberd:cmdline></update>
-->

      <!--
      This enables the server to automatically update the 
      user directory when a vcard is edited.  The update is
      only sent to the first listed jud service below.  It is
      safe to remove this flag if you do not want any users
      automatically added to the directory.
      -->

      <vcard2jud/>

      <!--
      The <browse/> section identifies the transports and other
      services that are available from this server. Note that each
      entity identified here must exist elsewhere or be further 
      defined in its own <service/> section below. These services 
      will appear in the user interface of Jabber clients that
      connect to your server.
      -->

      <browse>

        <!-- 
        This is the default agent for the master Jabber User 
        Directory, a.k.a. "JUD", which is located at jabber.org.
        You can add separate <service/> sections for additional
        directories, e.g., one for a company intranet.
        -->

        <service type="jud" jid="users.jabber.org" name="Jabber User Directory">
          <ns>jabber:iq:search</ns>
          <ns>jabber:iq:register</ns>
        </service>

        <conference type="public" jid="conference.localhost" name="Public Chatrooms"/>
        <conference type="private" jid="private.localhost" name="Private Conferencing"/>
      </browse>
    </jsm>

    <load main="jsm">
      <jsm>./jsm/jsm.so</jsm>
      <mod_router>./jsm/jsm.so</mod_router>
      <mod_auth_digest>./jsm/jsm.so</mod_auth_digest>
      <mod_log>./jsm/jsm.so</mod_log>
      <mod_auth_internal>./jsm/jsm.so</mod_auth_internal>
    </load>
  </service>

  <xdb id="xdb">
    <host/>
    <load>
      <xdb_file>./xdb_file/xdb_file.so</xdb_file>
    </load>
    <xdb_file xmlns="jabber:config:xdb_file">
      <spool><jabberd:cmdline flag='s'>./spool</jabberd:cmdline></spool>
    </xdb_file>
  </xdb>

  <log id='elogger'>
    <host/>
    <logtype/>
    <format>%d: [%t] (%h): %s</format>
    <rotfile rotationSize="@@@ROTATION_SIZE@@@" historyLen="@@@HISTORY_LEN@@@" debug='@@@DEBUG@@@'>/var/log/iic/xmlrouter_error.log</rotfile>
    <!--stderr/-->
  </log>

  <log id='rlogger'>
    <host/>
    <logtype>record</logtype>
    <format>%d %h %s</format>
    <rotfile rotationSize="@@@ROTATION_SIZE@@@" historyLen="@@@HISTORY_LEN@@@">/var/log/iic/xmlrouter_access.log</rotfile>
  </log>

  <service id="dnsrv">
    <host/>
    <load>
      <dnsrv>./dnsrv/dnsrv.so</dnsrv>
    </load>
    <dnsrv xmlns="jabber:config:dnsrv">
    	<resend>s2s</resend>
    </dnsrv>
  </service>

  @@@JADC2S_SERVICES@@@

  <service id="s2s">
    <load>
      <dialback>./dialback/dialback.so</dialback>
    </load>
    <dialback xmlns='jabber:config:dialback'>
      <legacy/>
      <ip port="5269"/>
      <karma>
        <init>128</init>
        <max>128</max>
        <inc>0</inc>
        <dec>0</dec>
        <penalty>0</penalty>
        <restore>128</restore>
      </karma>
    </dialback>
  </service>

  <io>
    <karma>
      <heartbeat>2</heartbeat>
      <init>128</init>
      <max>128</max>
      <inc>6</inc>
      <dec>0</dec>
      <penalty>-3</penalty>
      <restore>128</restore>
    </karma>
    <rate points="50" time="25"/>
  </io>

  <pidfile>/var/iic/run/jabber.pid</pidfile>

  @@@MTGARCHIVER_SERVICES@@@

  <service id="sharemgr">
    <host>sharemgr.iic</host>
    <accept>
      <ip/>
      <port>@@@SHAREMGR_PORT@@@</port>
      <secret><![CDATA[@@@SERVICE_SECRET@@@]]></secret>
    </accept>
  </service>

  <service id="extapi">
    <host>extapi.iic</host>
    <accept>
      <ip/>
      <port>@@@EXTAPI_PORT@@@</port>
      <secret><![CDATA[@@@SERVICE_SECRET@@@]]></secret>
    </accept>
  </service>

  <service id="mailer">
    <host>mailer.iic</host>
    <accept>
      <ip/>
      <port>@@@MAILER_PORT@@@</port>
      <secret><![CDATA[@@@SERVICE_SECRET@@@]]></secret>
    </accept>
  </service>

  <service id="converter">
    <host>converter.iic</host>
    <accept>
      <ip/>
      <port>@@@CONVERTER_PORT@@@</port>
      <secret><![CDATA[@@@SERVICE_SECRET@@@]]></secret>
    </accept>
  </service>

</jabber>
