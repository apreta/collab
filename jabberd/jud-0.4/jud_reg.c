/* --------------------------------------------------------------------------
 *
 * License
 *
 * The contents of this file are subject to the Jabber Open Source License
 * Version 1.0 (the "License").  You may not copy or use this file, in either
 * source code or executable form, except in compliance with the License.  You
 * may obtain a copy of the License at http://www.jabber.com/license/ or at
 * http://www.opensource.org/.  
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyrights
 * 
 * Portions created by or assigned to Jabber.com, Inc. are 
 * Copyright (c) 1999-2000 Jabber.com, Inc.  All Rights Reserved.  Contact
 * information for Jabber.com, Inc. is available at http://www.jabber.com/.
 *
 * Portions Copyright (c) 1998-1999 Jeremie Miller.
 * 
 * Acknowledgements
 * 
 * Special thanks to the Jabber Open Source Contributors for their
 * suggestions and support of Jabber.
 * 
 * --------------------------------------------------------------------------*/

#include "jud.h"

void jud_register(ji j, jpacket p)
{
    xmlnode q, x;
 
    x = xhash_get(j->users, jid_full(jid_user(p->from)));

    if(jpacket_subtype(p) == JPACKET__GET)
    {
        log_debug(ZONE,"handling get from %s",jid_full(p->from));
 
        jutil_iqresult(p->x);

        /* create a new query */
        q = xmlnode_insert_tag(p->x, "query");
        xmlnode_put_attrib(q,"xmlns",NS_REGISTER);
        xmlnode_insert_cdata(xmlnode_insert_tag(q,"instructions"),"Complete the form to submit your searchable attributes in the Jabber User Directory",-1);
 
        /* copy in the registration fields, and any pre-defined data */
        xmlnode_insert_cdata(xmlnode_insert_tag(q,"name"),xmlnode_get_tag_data(x,"name"),-1);
        xmlnode_insert_cdata(xmlnode_insert_tag(q,"first"),xmlnode_get_tag_data(x,"first"),-1);
        xmlnode_insert_cdata(xmlnode_insert_tag(q,"last"),xmlnode_get_tag_data(x,"last"),-1);
        xmlnode_insert_cdata(xmlnode_insert_tag(q,"nick"),xmlnode_get_tag_data(x,"nick"),-1);
        xmlnode_insert_cdata(xmlnode_insert_tag(q,"email"),xmlnode_get_tag_data(x,"email"),-1);

    }else if(jpacket_subtype(p) == JPACKET__SET){

        log_debug(ZONE,"handling set from %s",jid_full(p->from));
        /* store the new registration data */
        q = xmlnode_new_tag("item");
        xmlnode_insert_node(q,xmlnode_get_firstchild(p->iq));
        xmlnode_put_attrib(q,"jid",jid_full(jid_user(p->from))); 
        xdb_act(j->xc, j->id, "jabber:jud:users", "insert", spools(p->p,"?jid=",xmlnode_get_attrib(q,"jid"),p->p), q); /* sync the xdb */
        xmlnode_free(x); /* ditch old dayta */
        xhash_put(j->users,xmlnode_get_attrib(q,"jid"),(void*)q); /* save the new */

        jutil_iqresult(p->x);

    }else{
        xmlnode_free(p->x);
        return;
    }

    deliver(dpacket_new(p->x),NULL);
}
