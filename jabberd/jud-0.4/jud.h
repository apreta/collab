/* --------------------------------------------------------------------------
 *
 * License
 *
 * The contents of this file are subject to the Jabber Open Source License
 * Version 1.0 (the "License").  You may not copy or use this file, in either
 * source code or executable form, except in compliance with the License.  You
 * may obtain a copy of the License at http://www.jabber.com/license/ or at
 * http://www.opensource.org/.  
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyrights
 * 
 * Portions created by or assigned to Jabber.com, Inc. are 
 * Copyright (c) 1999-2000 Jabber.com, Inc.  All Rights Reserved.  Contact
 * information for Jabber.com, Inc. is available at http://www.jabber.com/.
 *
 * Portions Copyright (c) 1998-1999 Jeremie Miller.
 * 
 * Acknowledgements
 * 
 * Special thanks to the Jabber Open Source Contributors for their
 * suggestions and support of Jabber.
 * 
 * --------------------------------------------------------------------------*/

#include "jabberd.h"

#define VERSION "0.4"

/* 
 NOTICE (from jer): 
    this thing is really sick and disgusting right now, 
    if I had more than a few hours to convert it, I'd fully make it hash-indexing/searching!

<service id="jud">
  <load><jud>./jud/jud.so</jud></load>
  <jud xmlns="jabber:config:jud">
    <vCard>
      <FN>Local User Directory</FN>
      <DESC>This service provides a simple user directory service.</DESC>
      <URL>http://foo.bar/</URL>
    </vCard>
  </jud>
</service>

*/

/* jud instance */
typedef struct ji_struct
{
    instance i;
    xdbcache xc;
    xht users;
    jid id;
    int start;
    xmlnode config;
} *ji, _ji;

void jud_search(ji j, jpacket p);
void jud_register(ji j, jpacket p);
