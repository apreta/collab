/* --------------------------------------------------------------------------
 *
 * License
 *
 * The contents of this file are subject to the Jabber Open Source License
 * Version 1.0 (the "License").  You may not copy or use this file, in either
 * source code or executable form, except in compliance with the License.  You
 * may obtain a copy of the License at http://www.jabber.com/license/ or at
 * http://www.opensource.org/.  
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyrights
 * 
 * Portions created by or assigned to Jabber.com, Inc. are 
 * Copyright (c) 1999-2000 Jabber.com, Inc.  All Rights Reserved.  Contact
 * information for Jabber.com, Inc. is available at http://www.jabber.com/.
 *
 * Portions Copyright (c) 1998-1999 Jeremie Miller.
 * 
 * Acknowledgements
 * 
 * Special thanks to the Jabber Open Source Contributors for their
 * suggestions and support of Jabber.
 * 
 * --------------------------------------------------------------------------*/

#include "conference.h"
#include <sys/utsname.h>

void con_server_browsewalk(xht h, const char *key, void *data, void *arg)
{
    cnr r = (cnr)data;
    jpacket jp = (jpacket)arg;
    xmlnode x;
    char str[10];

    /* if we're a private server, we can only show the rooms the user already knows about */
    if(r->c->public == 0 && xhash_get(r->remote,jid_full(jp->to)) == NULL)
        return;

    x = xmlnode_insert_tag(jp->iq, "conference");
    if(r->c->public == 0)
        xmlnode_put_attrib(x,"type","private");
    else
        xmlnode_put_attrib(x,"type","public");
    xmlnode_put_attrib(x,"jid",jid_full(r->id));
    sprintf(str,"%d",r->count);
    xmlnode_put_attrib(x,"name",spools(jp->p,r->name," (",str,")",jp->p));
}

void con_server(cni c, jpacket jp)
{
    char *str;
    struct utsname un;
    xmlnode x;
    int start;
    time_t t;
    char nstr[10];

    log_debug(ZONE,"server packet");

    if(jp->type != JPACKET_IQ)
    {
        xmlnode_free(jp->x);
        return;
    }

    if(jpacket_subtype(jp) != JPACKET__GET)
    {
        jutil_error(jp->x, TERROR_NOTALLOWED);
        deliver(dpacket_new(jp->x),NULL);
        return;
    }

    /* now we're all set */

    if(NSCHECK(jp->iq,NS_TIME))
    {
        jutil_iqresult(jp->x);
        xmlnode_put_attrib(xmlnode_insert_tag(jp->x,"query"),"xmlns",NS_TIME);
        jpacket_reset(jp);
        xmlnode_insert_cdata(xmlnode_insert_tag(jp->iq,"utc"),jutil_timestamp(),-1);
        xmlnode_insert_cdata(xmlnode_insert_tag(jp->iq,"tz"),tzname[0],-1);

        /* create nice display time */
        t = time(NULL);
        str = ctime(&t);
        str[strlen(str) - 1] = '\0'; /* cut off newline */
        xmlnode_insert_cdata(xmlnode_insert_tag(jp->iq,"display"),str,-1);
        deliver(dpacket_new(jp->x),NULL);
        return;
    }

    if(NSCHECK(jp->iq,NS_VERSION))
    {
        jutil_iqresult(jp->x);
        xmlnode_put_attrib(xmlnode_insert_tag(jp->x,"query"),"xmlns",NS_VERSION);
        jpacket_reset(jp);

        xmlnode_insert_cdata(xmlnode_insert_tag(jp->iq,"name"),"conference",-1);
        xmlnode_insert_cdata(xmlnode_insert_tag(jp->iq,"version"),VERSION,-1);

        uname(&un);
        x = xmlnode_insert_tag(jp->iq,"os");
        xmlnode_insert_cdata(x,un.sysname,-1);
        xmlnode_insert_cdata(x," ",1);
        xmlnode_insert_cdata(x,un.release,-1);

        deliver(dpacket_new(jp->x),NULL);
        return;
    }

    if(NSCHECK(jp->iq,NS_BROWSE))
    {
        jutil_iqresult(jp->x);
        xmlnode_put_attrib(xmlnode_insert_tag(jp->x,"conference"),"xmlns",NS_BROWSE);
        jpacket_reset(jp);
        if(c->public == 0)
            xmlnode_put_attrib(jp->iq,"type","private");
        else
            xmlnode_put_attrib(jp->iq,"type","public");
        xmlnode_put_attrib(jp->iq,"name",xmlnode_get_tag_data(c->config,"vCard/FN")); /* pull name from the server vCard */

        xhash_walk(c->rooms, con_server_browsewalk, (void*)jp);

        deliver(dpacket_new(jp->x),NULL);
        return;
    }


    if(NSCHECK(jp->iq,NS_LAST))
    {
        jutil_iqresult(jp->x);
        xmlnode_put_attrib(xmlnode_insert_tag(jp->x,"query"),"xmlns",NS_LAST);
        jpacket_reset(jp);

        start = time(NULL) - c->start;
        sprintf(nstr,"%d",start);
        xmlnode_put_attrib(jp->iq,"seconds",nstr);

        deliver(dpacket_new(jp->x),NULL);
        return;
    }


    if(NSCHECK(jp->iq,NS_VCARD))
    {
        jutil_iqresult(jp->x);
        xmlnode_put_attrib(xmlnode_insert_tag(jp->x,"vCard"),"xmlns",NS_VCARD);
        jpacket_reset(jp);

        xmlnode_insert_node(jp->iq,xmlnode_get_firstchild(xmlnode_get_tag(c->config,"vCard")));

        deliver(dpacket_new(jp->x),NULL);
        return;
    }


    jutil_error(jp->x, TERROR_NOTIMPL);
    deliver(dpacket_new(jp->x),NULL);
}


void _con_packets(void *arg)
{
    jpacket jp = (jpacket)arg;
    cni c = (cni)jp->aux1;
    cnr r;
    cnu u, u2;
    char *s;
    int priority = -1;

    /* first, handle all packets just to the server (browse, vcard, ping, etc) */
    if(jp->to->user == NULL)
    {
        con_server(c, jp);
        return;
    }

    log_debug(ZONE,"processing packet %s",xmlnode2str(jp->x));

    /* any other packets must have an associated room */
    for(s = jp->to->user; *s != '\0'; s++) *s = tolower(*s); /* lowercase the group name */
    if((r = xhash_get(c->rooms, jid_full(jid_user(jp->to)))) == NULL)
    {
        /* well, this might be a room create request! */
        if(jp->type != JPACKET_PRESENCE || jutil_priority(jp->x) < 0)
        {
            if(jpacket_subtype(jp) == JPACKET__ERROR)
            {
                xmlnode_free(jp->x);
            }else{
                jutil_error(jp->x, TERROR_NOTFOUND);
                deliver(dpacket_new(jp->x),NULL);
            }
            return;
        }
        /* make a generic room, this user is the owner */
        r = con_room_new(c, jid_user(jp->to), jp->from, NULL, NULL, 1);
        u = r->owner;
        /* fall through, so the presence goes to the room like normal */
    }else{
        /* get the sending user entry, if any */
        u = xhash_get(r->remote, jid_full(jp->from));
    }

    /* several things use this field below as a flag */
    if(jp->type == JPACKET_PRESENCE)
        priority = jutil_priority(jp->x);

    /* sending available presence will automatically get you a generic user, if you don't have one */
    if(u == NULL && priority >= 0)
        u = con_user_new(r, jp->from);

    /* update tracking stuff */
    r->last = time(NULL);
    r->packets++;
    if(u != NULL)
    {
        u->last = time(NULL);
        u->packets++;
    }

    /* handle the old-style conferencing join/rename */
    if(priority >= 0 && jp->to->resource != NULL)
    {
        u2 = con_room_usernick(r, jp->to->resource); /* existing user w/ this nick? */

        /* it's just us updating our presence, old fashioned way */
        if(u2 == u)
        {
            jp->to = jid_user(jp->to);
            xmlnode_put_attrib(jp->x,"to",jid_full(jp->to));
            con_room_process(r, u, jp);
            return;
        }

        if(u2 != NULL)
        {
            /* XXX do we send a msg too? */
            jutil_error(jp->x, TERROR_CONFLICT);
            deliver(dpacket_new(jp->x),NULL);
            return;
        }

        /* if from an existing user in the room, change the nick */
        if(u->lid != NULL)
        {
            con_user_nick(u,jp->to->resource); /* broadcast nick rename */
        }else if(r->secret == NULL){
            u->legacy = 1;
            con_user_enter(u, jp->to->resource, 0, NULL); /* join the room */
        }else{
            /* XXX do we send a msg too? */
            jutil_error(jp->x, TERROR_NOTALLOWED);
            deliver(dpacket_new(jp->x),NULL);
            return;
        }

        xmlnode_free(jp->x);
        return;
    }

    /* kill any user sending unavailable presence */
    if(jpacket_subtype(jp) == JPACKET__UNAVAILABLE)
    {
        con_user_zap(u);
        xmlnode_free(jp->x);
        return;
    }

    /* handle errors */
    if(jpacket_subtype(jp) == JPACKET__ERROR)
    {
        /* only allow iq errors that are to a resource (direct-chat) */
        if(jp->to->resource == NULL || jp->type != JPACKET_IQ)
            con_user_zap(u);
        xmlnode_free(jp->x);
        return;
    }

    /* not in the room yet? foo */
    if(u == NULL || u->lid == NULL)
    {
        if(jp->to->resource != NULL)
        {
            jutil_error(jp->x, TERROR_NOTFOUND);
            deliver(dpacket_new(jp->x),NULL);
        }else{
            con_room_outsider(r, u, jp); /* non-participants get special treatment */
        }
        return;
    }

    /* packets to a specific resource?  one on one chats, browse lookups, etc */
    if(jp->to->resource != NULL)
    {
        if((u2 = xhash_get(r->local, jp->to->resource)) == NULL && (u2 = con_room_usernick(r, jp->to->resource)) == NULL) /* gotta have a recipient */
        {
            jutil_error(jp->x, TERROR_NOTFOUND);
            deliver(dpacket_new(jp->x),NULL);
        }else{
            con_user_process(u2, u, jp);
        }
        return;
    }

    /* finally, handle packets just to a room from a participant, msgs, pres, iq browse/conferencing, etc */
    con_room_process(r, u, jp);

}


/* phandler callback, send packets to another server */
result con_packets(instance i, dpacket dp, void *arg)
{
    cni c = (cni)arg;
    jpacket jp;

    /* routes are from dnsrv w/ the needed ip */
    if(dp->type == p_ROUTE)
        jp = jpacket_new(xmlnode_get_firstchild(dp->x));
    else
        jp = jpacket_new(dp->x);

    /* if the delivery failed */
    if(jp == NULL)
    {
        deliver_fail(dp,"Illegal Packet");
        return r_DONE;
    }

    /* bad packet??? ick */
    if(jp->type == JPACKET_UNKNOWN || jp->to == NULL)
    {
        jutil_error(jp->x, TERROR_BAD);
        deliver(dpacket_new(jp->x),NULL);
        return r_DONE;
    }

    /* we want things processed in order, and don't like re-entrancy! */
    jp->aux1 = (void*)c;
    mtq_send(c->q, jp->p, _con_packets, (void *)jp);
    return r_DONE;
}

/* callback for walking each user in a room */
void _con_beat_user(xht h, const char *key, void *data, void *arg)
{
    cnu u = (cnu)data;
    int now = (int)arg;

    if(u->lid == NULL && (now - u->last) > 120)
        con_user_zap(u);
}

/* callback for walking each room */
void _con_beat_idle(xht h, const char *key, void *data, void *arg)
{
    cnr r = (cnr)data;
    int now = (int)arg;

    xhash_walk(r->remote,_con_beat_user,arg); /* makes sure nothing stale is in the room */

    if(r->owner == NULL && r->count == 0 && (now - r->last) > 240)
        con_room_zap(r);
}

/* heartbeat checker for timed out idle rooms */
result con_beat_idle(void *arg)
{
    cni c = (cni)arg;

    xhash_walk(c->rooms,_con_beat_idle,(void*)time(NULL));
    return r_DONE;
}

/*** everything starts here ***/
void conference(instance i, xmlnode x)
{
    cni c;
    xmlnode cfg, cur;
    cnr r;
    int priv;
    jid room;

    log_debug(ZONE,"conference loading");

    /* get the config */
    cfg = xdb_get(xdb_cache(i),jid_new(xmlnode_pool(x),"config@-internal"),"jabber:config:conference");

    c = pmalloco(i->p,sizeof(_cni));
    c->rooms = xhash_new(j_atoi(xmlnode_get_tag_data(cfg,"maxrooms"),401));
    c->i = i;
    c->history = j_atoi(xmlnode_get_tag_data(cfg,"history"),10);
    c->q = mtq_new(i->p);
    c->config = cfg;
    if(xmlnode_get_tag(cfg,"public"))
        c->public = 1;

    if((cur = xmlnode_get_tag(cfg,"room")) != NULL && c->public != 0)
        for(;cur != NULL; xmlnode_hide(cur), cur = xmlnode_get_tag(cfg,"room"))
        {
            if((room = jid_new(i->p,xmlnode_get_attrib(cur,"jid"))) == NULL)
                continue;
            if(xmlnode_get_tag(cfg,"privacy") != NULL)
                priv = 1;
            else
                priv = 0;
            r = con_room_new(c, room, NULL, xmlnode_get_tag_data(cur,"name"), xmlnode_get_tag_data(cur,"secret"),priv);
            r->owner = (void*)c; /* flags as pre-configured room */
            /* override defaults if configured */
            if(xmlnode_get_tag(cur,"notice") != NULL)
            {
                r->note_leave = xmlnode_get_tag_data(cur,"notice/leave");
                r->note_join = xmlnode_get_tag_data(cur,"notice/join");
                r->note_rename = xmlnode_get_tag_data(cur,"notice/rename");
            }
        }

    register_phandler(i,o_DELIVER,con_packets,(void*)c);
    register_beat(120, con_beat_idle, (void *)c);

}
