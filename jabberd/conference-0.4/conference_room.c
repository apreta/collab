/* --------------------------------------------------------------------------
 *
 * License
 *
 * The contents of this file are subject to the Jabber Open Source License
 * Version 1.0 (the "License").  You may not copy or use this file, in either
 * source code or executable form, except in compliance with the License.  You
 * may obtain a copy of the License at http://www.jabber.com/license/ or at
 * http://www.opensource.org/.  
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyrights
 * 
 * Portions created by or assigned to Jabber.com, Inc. are 
 * Copyright (c) 1999-2000 Jabber.com, Inc.  All Rights Reserved.  Contact
 * information for Jabber.com, Inc. is available at http://www.jabber.com/.
 *
 * Portions Copyright (c) 1998-1999 Jeremie Miller.
 * 
 * Acknowledgements
 * 
 * Special thanks to the Jabber Open Source Contributors for their
 * suggestions and support of Jabber.
 * 
 * --------------------------------------------------------------------------*/

#include "conference.h"

void _con_room_usernick(xht h, const char *key, void *data, void *arg)
{
    cnu u = (cnu)data;
    xmlnode x = (xmlnode)arg;
    if(j_strcmp(xmlnode_get_data(x),xmlnode_get_data(u->nick)) == 0)
        xmlnode_put_vattrib(x, "u", (void*)u);
}

cnu con_room_usernick(cnr r, char *nick)
{
    cnu u;
    xmlnode x = xmlnode_new_tag("nick");

    log_debug(ZONE,"searching for nick %s in room %s",nick, jid_full(r->id));

    xmlnode_insert_cdata(x, nick, -1);
    xhash_walk(r->local, _con_room_usernick, (void *)x);

    u = (cnu)xmlnode_get_vattrib(x,"u");
    xmlnode_free(x);
    return u;
}

/* returns a valid nick from the list, x is the first <nick>foo</nick> xmlnode, checks siblings */
char *con_room_nick(cnr r, cnu u, xmlnode x)
{
    char *nick = NULL;
    xmlnode cur;
    int count = 1;

    log_debug(ZONE,"looking for valid nick in room %s from starter %s",jid_full(r->id),xmlnode2str(x));

    /* make-up-a-nick phase */
    if(x == NULL)
    {
        nick = pmalloco(u->p, strlen(u->rid->user) + 10);
        sprintf(nick,"%s",u->rid->user);
        while(con_room_usernick(r, nick) != NULL)
            sprintf(nick,"%s%d",u->rid->user,count++);
        return nick;
    }

    /* scan the list */
    for(cur = x; cur != NULL; cur = xmlnode_get_nextsibling(cur))
    {
        if(j_strcmp(xmlnode_get_name(cur),"nick") == 0 && (nick = xmlnode_get_data(cur)) != NULL)
            if(con_room_usernick(r, nick) == NULL)
                break;
    }

    /* XXX apply any further nick restrictions here! */

    return nick;
}

void con_room_browsewalk(xht h, const char *key, void *data, void *arg)
{
    cnu u = (cnu)data;
    xmlnode q = (xmlnode)arg;
    xmlnode x = xmlnode_insert_tag(q,"user");
    xmlnode_put_attrib(x,"name",xmlnode_get_data(u->nick));
    xmlnode_put_attrib(x,"jid",jid_full(u->lid));
}

void con_room_sendwalk(xht h, const char *key, void *data, void *arg)
{
    cnu u = (cnu)data;
    xmlnode x = (xmlnode)arg;
    con_user_send(u, (cnu)xmlnode_get_vattrib(x,"cnu"), xmlnode_dup(x));
}


void con_room_outsider(cnr r, cnu from, jpacket jp)
{
    int privacy = 0;
    char *nick;
    xmlnode q;

    log_debug(ZONE,"handling request from outsider %s to room %s", jid_full(jp->from),jid_full(r->id));

    /* any presence here is just fluff */
    if(jp->type == JPACKET_PRESENCE)
    {
        xmlnode_free(jp->x);
        return;
    }

    if(jp->type == JPACKET_MESSAGE)
    {
        jutil_error(jp->x, TERROR_FORBIDDEN);
        deliver(dpacket_new(jp->x), NULL);
        return;
    }

    /* public iq requests */

    if(jpacket_subtype(jp) == JPACKET__SET)
    {
        /* only handle conference sets! */
        if(!NSCHECK(jp->iq, NS_CONFERENCE) || from == NULL)
        {
            jutil_error(jp->x, TERROR_FORBIDDEN);
            deliver(dpacket_new(jp->x), NULL);
            return;
        }

        /* if a secret, check it first */
        if(r->secret != NULL && j_strcmp(r->secret, xmlnode_get_tag_data(jp->iq, "secret")) != 0)
        {
            jutil_error(jp->x, TERROR_AUTH);
            deliver(dpacket_new(jp->x), NULL);
            return;
        }

        /* get the nick */
        if((nick = con_room_nick(r, from, xmlnode_get_tag(jp->iq,"nick"))) == NULL)
        {
            jutil_error(jp->x, TERROR_CONFLICT);
            deliver(dpacket_new(jp->x), NULL);
            return;
        }

        /* process the join */

        if(r->private && xmlnode_get_tag(jp->iq,"privacy") != NULL)
            privacy = 1;

        /* if we created the room, we can set things */
        if(r->owner == from)
        {
            if(xmlnode_get_tag(jp->iq,"secret") != NULL)
                r->secret =  pstrdup(r->p, xmlnode_get_tag_data(jp->iq, "secret"));
            if(xmlnode_get_tag(jp->iq,"name") != NULL)
                r->name =  pstrdup(r->p, xmlnode_get_tag_data(jp->iq, "name"));
            if(xmlnode_get_tag(jp->iq,"privacy") != NULL)
                r->private = 1;
        }

        con_user_enter(from, nick, privacy, jp->x); /* bit of a hack, in that it returns the iq request */
        return;
    }

    if(NSCHECK(jp->iq,NS_BROWSE))
    {
        jutil_iqresult(jp->x);
        q = xmlnode_insert_tag(jp->x,"conference");
        xmlnode_put_attrib(q,"xmlns",NS_BROWSE);
        xmlnode_put_attrib(q,"name",r->name);
        xmlnode_insert_cdata(xmlnode_insert_tag(q,"ns"),NS_CONFERENCE,-1);
        if(r->c->public)
        {
            xmlnode_put_attrib(q,"type","public");
            xhash_walk(r->local, con_room_browsewalk, (void*)q);
        }else{
            xmlnode_put_attrib(q,"type","private");
        }
        deliver(dpacket_new(jp->x), NULL);
        return;
    }

    if(NSCHECK(jp->iq,NS_CONFERENCE))
    {
        jutil_iqresult(jp->x);
        q = xmlnode_insert_tag(jp->x,"query");
        xmlnode_put_attrib(q,"xmlns",NS_CONFERENCE);
        xmlnode_insert_cdata(xmlnode_insert_tag(q,"name"),r->name,-1);
        xmlnode_insert_tag(q,"nick");
        if(r->secret)
            xmlnode_insert_tag(q,"secret");
        if(r->private)
            xmlnode_insert_tag(q,"privacy");
        deliver(dpacket_new(jp->x), NULL);
        return;
    }

    jutil_error(jp->x, TERROR_NOTIMPL);
    deliver(dpacket_new(jp->x), NULL);

}


void con_room_process(cnr r, cnu from, jpacket jp)
{
    char *nick;
    xmlnode q;
    xmlnode x;
    cnu u2;

    log_debug(ZONE,"handling request from participant %s(%s/%s) to room %s", jid_full(from->rid),from->lid->resource,xmlnode_get_data(from->nick),jid_full(r->id));

    /* presence is just broadcast and cached */
    if(jp->type == JPACKET_PRESENCE)
    {
        xmlnode_put_vattrib(jp->x,"cnu",(void*)from);
        xhash_walk(r->local,con_room_sendwalk,(void*)jp->x);
        xmlnode_free(from->presence);
        from->presence = jp->x;
        return;
    }

    if(jp->type == JPACKET_MESSAGE)
    {
        /* if topic, store it */
        if((x = xmlnode_get_tag(jp->x,"subject")) != NULL)
        {
            xmlnode_free(r->topic);
            r->topic = xmlnode_new_tag("topic");
            xmlnode_put_attrib(r->topic,"subject",xmlnode_get_data(x));
            xmlnode_insert_cdata(r->topic,xmlnode_get_data(from->nick),-1);
            xmlnode_insert_cdata(r->topic," has set the topic to: ",-1);
            xmlnode_insert_cdata(r->topic,xmlnode_get_data(x),-1);
        }

        /* ensure type="groupchat" */
        xmlnode_put_attrib(jp->x,"type","groupchat");

        /* broadcast */
        xmlnode_put_vattrib(jp->x,"cnu",(void*)from);
        xhash_walk(r->local,con_room_sendwalk,(void*)jp->x);

        /* store in history */
        jutil_delay(jp->x,jid_full(r->id));
        if(r->c->history > 0)
        {
            if(++r->hlast == r->c->history)
                r->hlast = 0;
            xmlnode_free(r->history[r->hlast]);
            r->history[r->hlast] = jp->x;
        }else{
            xmlnode_free(jp->x);
        }
        return;
    }

    /* public iq requests */

    if(jpacket_subtype(jp) == JPACKET__SET)
    {
        /* only handle conference sets! */
        if(!NSCHECK(jp->iq, NS_CONFERENCE))
        {
            jutil_error(jp->x, TERROR_FORBIDDEN);
            deliver(dpacket_new(jp->x), NULL);
            return;
        }

        /* nick change request */
        if((nick = xmlnode_get_tag_data(jp->iq,"nick")) != NULL)
        {
            u2 = con_room_usernick(r, nick);
            if(u2 != NULL)
            {
                if(u2 != from)
                {
                    jutil_error(jp->x, TERROR_CONFLICT);
                    deliver(dpacket_new(jp->x), NULL);
                    return;
                }
            }else{
                con_user_nick(from,nick);
            }
        }

        /* if the owner, they can (re)set certian things */
        if((cnu)r->owner == from)
        {
            if(xmlnode_get_tag(jp->iq,"secret") != NULL)
                r->secret =  pstrdup(r->p, xmlnode_get_tag_data(jp->iq, "secret"));
            if(xmlnode_get_tag(jp->iq,"name") != NULL)
                r->name =  pstrdup(r->p, xmlnode_get_tag_data(jp->iq, "name"));
            if(xmlnode_get_tag(jp->iq,"privacy") != NULL)
                from->private = r->private = 1;
        }

        jutil_iqresult(jp->x);
        deliver(dpacket_new(jp->x), NULL);
        return;
    }

    if(NSCHECK(jp->iq,NS_BROWSE))
    {
        jutil_iqresult(jp->x);
        q = xmlnode_insert_tag(jp->x,"conference");
        xmlnode_put_attrib(q,"xmlns",NS_BROWSE);
        xmlnode_put_attrib(q,"name",r->name);
        if(r->c->public)
        {
            xmlnode_put_attrib(q,"type","public");
        }else{
            xmlnode_put_attrib(q,"type","private");
        }
        xhash_walk(r->local, con_room_browsewalk, (void*)q);
        deliver(dpacket_new(jp->x), NULL);
        return;
    }

    if(NSCHECK(jp->iq,NS_CONFERENCE))
    {
        jutil_iqresult(jp->x);
        q = xmlnode_insert_tag(jp->x,"query");
        xmlnode_put_attrib(q,"xmlns",NS_CONFERENCE);
        xmlnode_insert_cdata(xmlnode_insert_tag(q,"name"),r->name,-1);
        xmlnode_insert_cdata(xmlnode_insert_tag(q,"nick"),xmlnode_get_data(from->nick),-1);
        xmlnode_insert_cdata(xmlnode_insert_tag(q,"id"),jid_full(from->lid),-1);
        deliver(dpacket_new(jp->x), NULL);
        return;
    }

    jutil_error(jp->x, TERROR_NOTALLOWED);
    deliver(dpacket_new(jp->x), NULL);

}



cnr con_room_new(cni c, jid room, jid owner, char *name, char *secret, int private)
{
    cnr r;
    pool p;

    p = pool_heap(1024);
    r = pmalloco(p, sizeof(_cnr));
    r->p = p;
    r->c = c;
    r->id = jid_new(p, jid_full(room));
    r->name = pstrdup(p, name);
    r->secret = pstrdup(p, secret);
    r->private = private;
    r->history = pmalloco(p, sizeof(xmlnode) * c->history); /* make array of xmlnodes */
    r->remote = xhash_new(401);
    r->local = xhash_new(401);
    r->note_leave = xmlnode_get_tag_data(c->config,"notice/leave");
    r->note_join = xmlnode_get_tag_data(c->config,"notice/join");
    r->note_rename = xmlnode_get_tag_data(c->config,"notice/rename");
    xhash_put(c->rooms, jid_full(r->id), (void*)r);
    if(owner != NULL)
        r->owner = (void*)con_user_new(r, owner);

    log_debug(ZONE,"new room %s (%s) %s/%s/%d", jid_full(r->id),jid_full(owner),name,secret,private);

    return r;
}

void _con_room_send(xht h, const char *key, void *data, void *arg)
{
    cnu u = (cnu)data;
    xmlnode x = xmlnode_dup((xmlnode)arg);
    xmlnode_put_attrib(x,"to",jid_full(u->rid));
    deliver(dpacket_new(x),NULL);
}


void con_room_send(cnr r, xmlnode x)
{
    log_debug(ZONE,"sending packet from room %s: %s", jid_full(r->id), xmlnode2str(x));

    xmlnode_put_attrib(x,"from",jid_full(r->id));
    xhash_walk(r->local,_con_room_send,(void*)x);
    xmlnode_free(x);
}


void con_room_zap(cnr r)
{
    log_debug(ZONE,"zapping room %s",jid_full(r->id));
    xhash_free(r->remote);
    xhash_free(r->local);
    xhash_zap(r->c->rooms,jid_full(r->id));
    xmlnode_free(r->topic);
    pool_free(r->p);
}












