/* --------------------------------------------------------------------------
 *
 * License
 *
 * The contents of this file are subject to the Jabber Open Source License
 * Version 1.0 (the "License").  You may not copy or use this file, in either
 * source code or executable form, except in compliance with the License.  You
 * may obtain a copy of the License at http://www.jabber.com/license/ or at
 * http://www.opensource.org/.  
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyrights
 * 
 * Portions created by or assigned to Jabber.com, Inc. are 
 * Copyright (c) 1999-2000 Jabber.com, Inc.  All Rights Reserved.  Contact
 * information for Jabber.com, Inc. is available at http://www.jabber.com/.
 *
 * Portions Copyright (c) 1998-1999 Jeremie Miller.
 * 
 * Acknowledgements
 * 
 * Special thanks to the Jabber Open Source Contributors for their
 * suggestions and support of Jabber.
 * 
 * --------------------------------------------------------------------------*/

#include <jabberd.h>

#define CONF_VERSION "0.4"

/*

this component doesn't support virtual hostnames,
it requires that it's hostname be the service id as in:
<service id="conference.localhost">...</service>

we can be configured as a private conferencing server (no browse, all rooms locked up):
  <conference xmlns="jabberd:config:conference">
    <private/>
    <history>30</history>
    <vCard>
      <FN>Private Conferences</FN>
      <DESC>This service is for private conferencing rooms.</DESC>
      <URL>http://foo.bar/</URL>
    </vCard>
  </conferece>

or a public chatroom server:
  <conference xmlns="jabberd:config:conference">
    <public/>
    <vCard>
      <FN>Public Chatrooms</FN>
      <DESC>This service is for public chatrooms.</DESC>
      <URL>http://foo.bar/</URL>
    </vCard>
    <history>20</history>
    <notice>
      <join> has become available</join>
      <leave> has left</leave>
      <rename> is now known as </rename>
    </notice>
    <room jid="help@conference.localhost">
      <name>Assistance Zone</name>
      <privacy/>
    </room>
    <room jid="admin@conference.localhost">
      <name>Adminz only</name>
      <secret>con0r</secret>
      <notice>
        <join> just rocks!</join>
        <leave> gets lost</leave>
        <rename> feels it is more important to be known as </rename>
      </notice>
    </room>
  </conference>

*/

typedef struct cnu_struct *cnu, _cnu;

/* conference instance */
typedef struct cni_struct
{
    instance i;
    xht rooms;
    xmlnode config; /* config data, mostly for public right now */
    int public; /* if we're public or not */
    int history; /* mas history items */
    mtq q;
    int start; /* startup time */
} *cni, _cni;

/* conference room */
typedef struct cnr_struct
{
    pool p;
    cni c;
    jid id; /* room id */
    void *owner; /* creator of the room, either the cnu of the user, NULL, or cni if it's predefined */
    xht remote; /* users associated w/ the room, key is remote jid */
    xht local; /* users associated w/ the room, key is local jid */
    char *name; /* friendly name of the room */
    int last; /* last time there was any traffic to the room */
    int private; /* if private is allowed in this room */
    char *secret; /* if there's a secret */
    xmlnode topic; /* <t i='time(NULL)' from='nick'>Topic Here</t> */
    xmlnode *history; /* an array of history messages (vattrib cnu='') */
    int hlast; /* last history message */
    int packets; /* total packets to this room */
    char *note_leave, *note_join, *note_rename; /* notices */
    int count; /* # of users in the room */
} *cnr, _cnr;

/* conference user */
struct cnu_struct
{
    cnr r;
    pool p;
    jid rid, lid; /* remote and local jids */
    xmlnode nick; /* <n>nickname</n> */
    xmlnode presence; /* cached presence */
    int last; /* last activity to/from user */
    int legacy; /* old style */
    int private; /* private flag */
    int packets; /* number of packets from this dude */
};

cnr con_room_new(cni c, jid room, jid owner, char *name, char *secret, int private);
void con_room_process(cnr r, cnu from, jpacket jp); /* process a packet to a room from a participant */
void con_room_outsider(cnr r, cnu from, jpacket jp); /* process a packet to a room from a non-participant */
cnu con_room_usernick(cnr r, char *nick); /* resolves to to a user based on nickname */
void con_room_send(cnr r, xmlnode x); /* sends a raw packet from="room@host" to all participants */
void con_room_zap(cnr r); /* kills a room */

cnu con_user_new(cnr r, jid id); /* new generic user */
void con_user_nick(cnu u, char *nick); /* broadcast nick change */
void con_user_enter(cnu u, char *nick, int private, xmlnode iq); /* put user in room and announce */
void con_user_send(cnu to, cnu from, xmlnode x); /* send a packet to a user from other user */
void con_user_zap(cnu u); /* clean up the user */
void con_user_process(cnu to, cnu from, jpacket jp); /* process packets betweeen users */
