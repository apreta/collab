/* --------------------------------------------------------------------------
 *
 * License
 *
 * The contents of this file are subject to the Jabber Open Source License
 * Version 1.0 (the "License").  You may not copy or use this file, in either
 * source code or executable form, except in compliance with the License.  You
 * may obtain a copy of the License at http://www.jabber.com/license/ or at
 * http://www.opensource.org/.  
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied.  See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * Copyrights
 * 
 * Portions created by or assigned to Jabber.com, Inc. are 
 * Copyright (c) 1999-2000 Jabber.com, Inc.  All Rights Reserved.  Contact
 * information for Jabber.com, Inc. is available at http://www.jabber.com/.
 *
 * Portions Copyright (c) 1998-1999 Jeremie Miller.
 * 
 * Acknowledgements
 * 
 * Special thanks to the Jabber Open Source Contributors for their
 * suggestions and support of Jabber.
 * 
 * --------------------------------------------------------------------------*/

#include "conference.h"


cnu con_user_new(cnr r, jid id)
{
    pool p;
    cnu u;

    log_debug(ZONE,"adding user %s to room %s",jid_full(id),jid_full(r->id));

    p = pool_heap(1024);
    u = pmalloco(p, sizeof(_cnu));
    u->p = p;
    u->rid = jid_new(p, jid_full(id));
    u->r = r;
    u->presence = jutil_presnew(JPACKET__AVAILABLE, NULL, NULL);
    xhash_put(r->remote, jid_full(u->rid), (void*)u);
    return u;
}

void _con_user_nick(xht h, const char *key, void *data, void *arg)
{
    cnu to = (cnu)data;
    cnu from = (cnu)arg;
    char *old;
    xmlnode x, q;
    jid fid;

    if(to->legacy)
    {
        /* send old style unavail pres w/ old nick */
        if((old = xmlnode_get_attrib(from->nick,"old")) != NULL)
        {
            x = jutil_presnew(JPACKET__UNAVAILABLE, jid_full(to->rid), NULL);
            fid = jid_new(xmlnode_pool(x),jid_full(from->lid));
            jid_set(fid,old,JID_RESOURCE);
            xmlnode_put_attrib(x,"from",jid_full(fid));
            deliver(dpacket_new(x),NULL);
        }

        /* if there's a new nick, broadcast that too */
        if(xmlnode_get_data(from->nick) != NULL)
        {
            x = xmlnode_dup(from->presence);
            xmlnode_put_attrib(x,"to",jid_full(to->rid));
            fid = jid_new(xmlnode_pool(x),jid_full(from->lid));
            jid_set(fid,xmlnode_get_data(from->nick),JID_RESOURCE);
            xmlnode_put_attrib(x,"from",jid_full(fid));
            deliver(dpacket_new(x),NULL);
        }
    }else{
        /* just push new browse set */
        x = xmlnode_new_tag("iq");
        xmlnode_put_attrib(x,"type","set");
        xmlnode_put_attrib(x,"to",jid_full(to->rid));
        xmlnode_put_attrib(x,"from",jid_full(from->r->id));
        q = xmlnode_insert_tag(x, "user");
        xmlnode_put_attrib(q,"xmlns",NS_BROWSE);
        xmlnode_put_attrib(q,"jid",jid_full(from->lid));
        if(xmlnode_get_data(from->nick) != NULL)
            xmlnode_put_attrib(q,"name",xmlnode_get_data(from->nick));
        else
            xmlnode_put_attrib(q,"type","remove");
        deliver(dpacket_new(x),NULL);
    }
}

void con_user_nick(cnu u, char *nick)
{
    xmlnode x;

    log_debug(ZONE,"in room %s changing nick for user %s to %s from %s",jid_full(u->r->id),jid_full(u->rid),nick,xmlnode_get_data(u->nick));

    x = xmlnode_new_tag("n");
    xmlnode_put_attrib(x,"old",xmlnode_get_data(u->nick));
    xmlnode_insert_cdata(x,nick,-1);
    xmlnode_free(u->nick);
    u->nick = x;
    xhash_walk(u->r->local, _con_user_nick, (void*)u);

    /* send nick change notice */
    if(u->r->note_rename != NULL && nick != NULL && xmlnode_get_attrib(x,"old") != NULL)
        con_room_send(u->r,jutil_msgnew("groupchat",NULL,NULL,spools(u->p,xmlnode_get_attrib(x,"old"),u->r->note_rename,nick,u->p)));
}

void _con_user_enter_legacy(xht h, const char *key, void *data, void *arg)
{
    cnu from = (cnu)data;
    cnu to = (cnu)arg;
    xmlnode x;
    jid fid;

    /* mirror */
    if(from == to)
        return;

    x = xmlnode_dup(from->presence);
    xmlnode_put_attrib(x,"to",jid_full(to->rid));
    fid = jid_new(xmlnode_pool(x),jid_full(from->lid));
    jid_set(fid,xmlnode_get_data(from->nick),JID_RESOURCE);
    xmlnode_put_attrib(x,"from",jid_full(fid));
    deliver(dpacket_new(x),NULL);
}

void _con_user_enter(xht h, const char *key, void *data, void *arg)
{
    cnu from = (cnu)data;
    xmlnode b = (xmlnode)arg;

    /* well, no nick during setup means it's *us* */
    if(from->nick == NULL)
        return;

    b = xmlnode_insert_tag(b, "user");
    xmlnode_put_attrib(b, "jid", jid_full(from->lid));
    xmlnode_put_attrib(b, "name", xmlnode_get_data(from->nick));
}

void con_user_enter(cnu u, char *nick, int private, xmlnode iq)
{
    xmlnode x, q;
    int h, tflag = 0;

    u->lid = jid_new(u->p,jid_full(u->r->id));
    jid_set(u->lid,shahash(jid_full(u->rid)),JID_RESOURCE);
    xhash_put(u->r->local,u->lid->resource,(void*)u);
    u->r->count++;

    log_debug(ZONE,"officiating user %s in room %s as %s/%s",jid_full(u->rid),jid_full(u->r->id),nick,u->lid->resource);

    /* first send result */
    if(iq != NULL)
    {
        jutil_iqresult(iq);
        q = xmlnode_insert_tag(iq,"query");
        xmlnode_put_attrib(q, "xmlns", NS_CONFERENCE);
        xmlnode_insert_cdata(xmlnode_insert_tag(q,"nick"),nick,-1);
        xmlnode_insert_cdata(xmlnode_insert_tag(q,"name"),u->r->name,-1);
        xmlnode_insert_cdata(xmlnode_insert_tag(q,"id"),jid_full(u->lid),-1);
        deliver(dpacket_new(iq),NULL);
    }

    if(u->legacy)
    {
        xhash_walk(u->r->local, _con_user_enter_legacy, (void*)u);
    }else{
        x = xmlnode_new_tag("iq");
        xmlnode_put_attrib(x,"type","set");
        xmlnode_put_attrib(x,"to",jid_full(u->rid));
        xmlnode_put_attrib(x,"from",jid_full(u->r->id));
        q = xmlnode_insert_tag(x, "conference");
        xmlnode_put_attrib(q, "xmlns", NS_BROWSE);
        xmlnode_put_attrib(q,"name",u->r->name);
        if(u->r->c->public)
        {
            xmlnode_put_attrib(q,"type","public");
        }else{
            xmlnode_put_attrib(q,"type","private");
        }
        xhash_walk(u->r->local, _con_user_enter, (void*)q);
        deliver(dpacket_new(x), NULL);
    }

    con_user_nick(u, nick); /* pushes to everyone (including ourselves) our entrance */

    /* loop through history and send back */
    if(u->r->c->history == 0)
        return;

    h = u->r->hlast;
    while(1)
    {
        h++;
        if(h == u->r->c->history)
            h = 0;
        con_user_send(u, (cnu)xmlnode_get_vattrib(u->r->history[h],"cnu"), xmlnode_dup(u->r->history[h]));
        if(xmlnode_get_tag(u->r->history[h],"subject") != NULL)
            tflag = 1;
        if(h == u->r->hlast)
            break;
    }

    /* send last know topic */
    if(tflag == 0 && u->r->topic != NULL)
    {
        x = jutil_msgnew("groupchat",jid_full(u->rid),xmlnode_get_attrib(u->r->topic,"subject"),xmlnode_get_data(u->r->topic));
        xmlnode_put_attrib(x,"from",jid_full(u->r->id));
        deliver(dpacket_new(x),NULL);
    }

    /* send entrance notice */
    if(u->r->note_join != NULL)
        con_room_send(u->r,jutil_msgnew("groupchat",NULL,NULL,spools(u->p,nick,u->r->note_join,u->p)));
    
}


void con_user_process(cnu to, cnu from, jpacket jp)
{
    xmlnode q, x;
    char str[10];
    int t;

    /* we handle all iq's for this id, it's *our* id */
    if(jp->type == JPACKET_IQ)
    {
        if(NSCHECK(jp->iq,NS_BROWSE))
        {
            jutil_iqresult(jp->x);
            q = xmlnode_insert_tag(jp->x,"user");
            xmlnode_put_attrib(q,"xmlns",NS_BROWSE);
            xmlnode_put_attrib(q,"name",xmlnode_get_data(to->nick));

            if(to->private == 0)
            {
                x = xmlnode_insert_tag(q,"user");
                xmlnode_put_attrib(x,"jid",jid_full(to->rid));
            }

            deliver(dpacket_new(jp->x), NULL);
            return;
        }

        if(NSCHECK(jp->iq,NS_LAST))
        {
            jutil_iqresult(jp->x);
            q = xmlnode_insert_tag(jp->x,"query");
            xmlnode_put_attrib(q,"xmlns",NS_LAST);
            t = time(NULL) - to->last;
            sprintf(str,"%d",t);
            xmlnode_put_attrib(q,"seconds",str);

            deliver(dpacket_new(jp->x), NULL);
            return;
        }

        /* deny any other iq's if it's private */
        if(to->private == 1)
        {
            jutil_error(jp->x, TERROR_FORBIDDEN);
            deliver(dpacket_new(jp->x), NULL);
            return;
        }

        /* if not, fall through and just forward em on I guess! */
    }

    con_user_send(to, from, jp->x);
}

void con_user_send(cnu to, cnu from, xmlnode x)
{
    jid fid;

    if(to == NULL || from == NULL || x == NULL)
        return;

    log_debug(ZONE,"user send to %s from %s",jid_full(to->rid),jid_full(from->rid));

    fid = jid_new(xmlnode_pool(x),jid_full(from->lid));
    xmlnode_put_attrib(x,"to",jid_full(to->rid));
    if(to->legacy)
        jid_set(fid,xmlnode_get_data(from->nick),JID_RESOURCE);
    xmlnode_put_attrib(x,"from",jid_full(fid));
    deliver(dpacket_new(x),NULL);
}

void con_user_zap(cnu u)
{
    int h, hkill = 0;

    if(u == NULL)
        return;

    log_debug(ZONE,"zapping user %s",jid_full(u->rid));

    if(u->lid != NULL)
    {
        con_user_nick(u, NULL); /* sends unavail/browse-remove */
        xhash_zap(u->r->local,u->lid->resource);
        u->r->count--;
        /* send departure notice */
        con_room_send(u->r,jutil_msgnew("groupchat",NULL,NULL,spools(u->p,xmlnode_get_attrib(u->nick,"old"),u->r->note_leave,u->p)));
        /* chop off history */
        if(u->r->c->history > 0)
        {
            h = u->r->hlast;
            while(1)
            {
                /* if we sent a message in the history, start killing it */
                if((cnu)xmlnode_get_vattrib(u->r->history[h],"cnu") == u)
                    hkill = 1;
                /* kill mode */
                if(hkill)
                {
                    xmlnode_free(u->r->history[h]);
                    u->r->history[h] = NULL;
                }
                /* circle around */
                if(h == 0)
                    h = u->r->c->history;
                /* backtrack down the list */
                h--;
                /* if we're back where we started */
                if(h == u->r->hlast)
                    break;
            }
        }
    }
    if((cnu)u->r->owner == u)
        u->r->owner = NULL;
    xhash_zap(u->r->remote,jid_full(u->rid));
    xmlnode_free(u->presence);
    xmlnode_free(u->nick);
    pool_free(u->p);
}

