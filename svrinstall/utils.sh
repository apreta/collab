
. ./langs.sh 

if [ -f /etc/SuSE-release ]; then
	sles_install="y"
else
	sles_install=""
fi

function update_client_installers_tarball
{
    local -a tarballs=( cluster-prototype/installers_*.tar.gz )
    client_installers_version=$( grep 'client_version=' cluster-prototype/global-config )
    if [ "${tarballs[0]}" != 'cluster-prototype/installers_*.tar.gz' ]; then
	local latest_version=$( \
	    echo ${tarballs[*]} | \
	    sed -e 's+cluster-prototype/installers_++g' -e 's+.tar.gz++g' | \
	    tr ' ' '\n' | \
	    sort -n -k 1 -k 2 -k 3 -k 4 --field-separator=_ | \
	    tail -1 )
	client_installers_tarball=installers_$latest_version.tar.gz
	client_installers_version="client_version=$(echo $latest_version | tr '_' '.')"
    elif [ -e cluster-prototype/installers.tar.gz ]; then
	client_installers_tarball=installers.tar.gz
    fi

    if [ ! -e $cluster/$client_installers_tarball.skip ]; then
	  cp cluster-prototype/$client_installers_tarball $cluster/$client_installers_tarball
    fi

    set_client_hosts $cluster/$client_installers_tarball

    eval "$client_installers_version"
    save_config	# save client_version
}

function resolve_host
{
    local varname=$1
    local host=$2
    local have_ip=$( dig $host in A +short | wc -l )

    echo -n "Checking host in $varname..."
    if [ $have_ip -eq 0 ]; then
	echo "FAILED: global-config $varname value \"$host\" does not resolve to an IP address"
	return 255
    else
	echo "Ok"
	return 0
    fi
}

# maybe use an associative array to keep the ssh from happening for already
# validated/invalidated hosts
function check_host
{
    local host=$1

    if [ "$host" == "localhost" ]; then
	return 0;
    fi

    reported_host=$( ssh -q root@$host "/bin/hostname" );

    if [ "$host" != "$reported_host" ]; then
	return 255;
    else
	return 0;
    fi
}

function check_hosts
{
    local varname=$1
    local ret=0

    shift

    echo -n "Checking hosts in $varname..."
    for h in $* ; do
	if ! check_host $h ; then
	    echo -e "\n\tFAILED: global-config $varname value \"$h\" must be set to \"$reported_host\""
	    ret=-1
	fi
    done
    [ $ret -eq 0 ] && echo "Ok"

    return $ret
}

function check_global_config_hosts
{
    local ret=0
    local raw_hosts=" \
	$db_host \
	$portal_local_hname \
	$extapi_portal_local_hname \
	${lcl_xmlrouter[*]} \
	${controller_host[*]} \
	${voice_host[*]} \
	${mtgarchiver_host[*]} \
	${addressbk_host[*]} \
	${mailer_host[*]} \
	${converter_host[*]} \
	$extapi_portal_local_hname \
	${share_local_host[*]} \
	"

    echo "Checking hosts specified in global config"
    if ! resolve_host "external_hname" $external_hname ; then
	ret=-1
    fi

    if ! check_hosts "db_host" $db_host ; then
	ret=-1
    fi
    
    if ! check_hosts "portal_local_hname" $portal_local_hname ; then
	ret=-1
    fi
    
    if ! check_hosts "extapi_portal_local_hname" $extapi_portal_local_hname ; then
	ret=-1
    fi
    
    if ! check_hosts "lcl_xmlrouter" ${lcl_xmlrouter[*]} ; then
	ret=-1
    fi
    
    if ! check_hosts "controller_host" ${controller_host[*]} ; then
	ret=-1
    fi
    
    if ! check_hosts "voice_host" ${voice_host[*]} ; then
	ret=-1
    fi
    
    if ! check_hosts "mtgarchiver_host" ${mtgarchiver_host[*]} ; then
	ret=-1
    fi
    
    if ! check_hosts "mailer_host" ${mailer_host[*]} ; then
	ret=-1
    fi
    
    if ! check_hosts "converter_host" ${converter_host[*]} ; then
	ret=-1
    fi
    
    if ! check_hosts "extapi_local_hname" $extapi_local_hname ; then
	ret=-1
    fi
    
    if ! check_hosts "share_local_host" ${share_local_host[*]} ; then
	ret=-1
    fi

    echo "Finished checking hosts"

    return $ret
    
}

function push_file
{
    local host=$1
    local file=$2
    local remote_dir=${3-.}

    echo -n "Sending $file to root@$host:$remote_dir..."

    [ "$verify" == "true" ] && return 0

    echo "Sending $file to root@$host:$remote_dir..." >> install-cluster.log
    scp $file root@$host:$remote_dir >> install-cluster.log 2>&1

    return $?
}

function remote_cmd
{
    local host=$1
    local cmd=$2

    echo -n "Executing $cmd as root@$host..."

    [ "$verify" == "true" ] && return 0

    echo "Executing $cmd as root@$host..." >> install-cluster.log
    ssh root@$host "$cmd" >> install-cluster.log 2>&1

    return $?
}

function ctl_services
{
    for s in /etc/rc.d/rc3.d/S*iic* ; do $s $1 ; done
    # for s in /etc/rc.d/rc3.d/S*asterisk* ; do $s $1 ; done
}

function check_cluster_config
{
    local clstr=$1
    local ret=0

    global_config="$clstr/global-config"
    local versioned_tarball="$clstr/installers_$( echo $client_version | tr '.' '_' ).tar.gz"
    if [ -e "$versioned_tarball" ]; then
	client_installers=$versioned_tarball
    else
	client_installers="$clstr/installers.tar.gz"
    fi
    new_mtgarchive_template="$clstr/locale/$locale_setting/emails/new-mtgarchive-template.default"
    meeting_summary_template="$clstr/locale/$locale_setting/emails/meeting-summary-template.default"
    new_user_template="$clstr/locale/$locale_setting/emails/new-user-template.default"
    invitation_template="$clstr/locale/$locale_setting/emails/invitation-template.default"
    dialing_rules="$clstr/dialing.xml"
    voice_config="$clstr/voiceconfig.xml"
    external_bridge="$clstr/extvoiced"
    server_cert="$clstr/server.pem"
    web_server_cert="$clstr/wserver.pem"

    if [ ! -e $global_config ]; then
	echo "ERROR: $global_config not found!"
	ret=-1
    fi

    if [ ! -e "$client_installers" ]; then
	echo "WARN: client installers tarball, $client_installers,  not found; will not be deployed"
    fi

    if [ "$product" == "conferencing" -a ! -e $dialing_rules ]; then
	echo "INFO: $dialing_rules doesn't exist; default will be used"
    fi

    if [ "$product" == "conferencing" -a ! -e $voice_config ]; then
	echo "INFO: $voice_config doesn't exist; default will be used"
    fi

    return $ret
}

# config locations are correct for Red Hat...disabling on other platforms for now.
function define_static_ip_addr
{
    local ip_addr=$1
    local -a interfaces=( $( echo /etc/sysconfig/network-scripts/ifcfg-* | \
	sed -e 's+[^ ]*ifcfg-++g' | \
	tr ' ' '\n' | \
	grep -v ':' | \
	grep -v -w lo ) )
    local interface
    if [ ${#interfaces[*]} -eq 1 ]; then
	interface=${interfaces[0]}
    else
	echo "The following interfaces are configured: ${interfaces[*]}"
	local valid=0
	while [ $valid -eq 0 ]; do
	    until get_strval "Which one to you want to use for $ip_addr?" ${interfaces[0]} ; do continue; done
	    if [ -e /etc/sysconfig/network-scripts/ifcfg-$strval ]; then
		valid=1
	    else
		echo "Interface $strval isn't available"
	    fi
	done
	interface=$strval
    fi
    
    local idx=0
    local defined=0
    while [ $defined -eq 0 ]; do
	if [ ! -e /etc/sysconfig/network-scripts/ifcfg-$interface:$idx ]; then
	    local script="/etc/sysconfig/network-scripts/ifcfg-$interface:$idx"
	    
	    echo "DEVICE=$interface:$idx" > $script
	    echo "ONBOOT=yes" >> $script
	    echo "BOOTPROTO=static" >> $script
	    echo "IPADDR=$ip_addr" >> $script
	    
	    /sbin/ifup $interface:$idx
	    if [ $? -ne 0 ]; then
		return 255
	    fi
	    
	    echo "Defined IP addr in $script"
	    defined=1
	fi
	idx=$(($idx+1))
    done

    return 0
}


function show_configure_now_help
{
    cat<<EOF
The installer can configure a network interface now for the IP address
that is not reachable.
EOF
}

function get_valid_address
{
    local prompt=$1
    local default=$2
    local type=$3
    local help_function=$4
    local check_unreachable=$5
    local no_configure=$6
    local reachable=0
    local addr

    local prompt_default
    if [ -z "$default" ]; then
	prompt_default="<none>"
    else
	prompt_default=$default
    fi

    while [ $reachable -eq 0 ]; do
	echo -n ">>>>> $prompt (\"?\" for help) [default: $prompt_default] "
	read addr
	if [ -z "$addr" ]; then
	    if [ ! -z "$default" ]; then
		addr=$default
	    else
		echo "!!!!! You must enter a value"
		continue
	    fi
	fi

	if [ "$addr" == '|' ]; then
	    local cmd
	    echo -n "Enter a command: "
	    read cmd
	    $cmd
	fi

	if [ "$addr" == '?' ]; then
	    if [ ! -z $help_function ]; then
		$help_function
	    else
		echo "*NH*"
	    fi
	    cat <<EOF

The system will attempt to ping the indicated host to determine if the
given address is resolvable and network services are available on that
host.  Firewalls and known network outages may cause the host to be
unreachable.  Should a host or ip address be unreachable that you know
to be correct, you can choose to continue with the installation
anyway.
EOF
	    continue
	elif [ "$addr" == '|' ]; then
	    local cmd
	    echo -n "Enter a command: "
	    read cmd
	    $cmd
	    continue
	fi

	addr=`echo $addr | tr A-Z a-z`

	if [ "$type" == "ip" ] && ! validate_ip $addr ; then
	    continue
	fi

	if [ -z $check_unreachable ] ; then
	    if ping_address $addr ; then
		valid_address=$addr
		reachable=1
	    else
		if [ "$type" == "ip" -a -z "$sles_install" -a -z "$single_host" -a "$on_single_host" == "yes" -a -z "$no_configure" ]; then
		    until get_yorn "Couldn't reach $addr; configure it now?" "y" show_configure_now_help ; do continue; done;
		    if [ "$yorn" == "yes" ]; then
		    	define_static_ip_addr $addr
		    	[ $? -eq 0 ] && default=$addr
		    fi
		else
		    until get_yorn "Couldn't reach $addr; continue anyway?" "n" show_unreachable_help ; do continue; done
		    if [ "$yorn" == "yes" ]; then
		    	reachable=1
		    	valid_address=$addr
		    fi
	    	fi
	    fi
	else
	    if ! ping_address $addr ; then
		valid_address=$addr
		reachable=1
	    else
		until get_yorn "$addr appears to be in use, continue anyway?" "n" show_unreachable_help ; do continue; done
		if [ "$yorn" == "yes" ]; then
			reachable=1
			valid_address=$addr
		fi
	    fi

	fi
    done
}

function get_yorn
{
    local prompt=$1
    local default=$2
    local display_help=$3
    local resp

    echo -n ">>>>> $prompt (y/n/?) [default: $default] "
    if [ -z "$display_help" ]; then
	echo -n '*NH* '
    fi
    read resp
    if [ -z "$resp" ]; then
	resp=$default
    fi

    if [ "$resp" == "y" ]; then
	yorn="yes"
    elif [ "$resp" == "yes" ]; then
	yorn="yes"
    elif [ "$resp" == "n" ]; then
	yorn="no"
    elif [ "$resp" == "no" ]; then
	yorn="no"
    elif [ "$resp" == '?' ]; then
	$display_help
	return 255
    elif [ "$resp" == '|' ]; then
	local cmd
	echo -n "Enter a command: "
	read cmd
	$cmd
	return 255
    else
	echo "!!!!! Please type y or n"
	return 255
    fi

    return 0
}

function get_strval
{
    local prompt=$1
    local default=$2
    local display_help=$3
    local prompt_default

    if [ -z "$default" ]; then
	prompt_default="<none>"
    else
	prompt_default=$default
    fi

    echo -n ">>>>> $prompt (\"?\" for help) [default: $prompt_default] "
    read resp
    if [ ! -z "$resp" ]; then
	if [ "$resp" == '?' ]; then
	    $display_help
	    return 255
	elif [ "$resp" == '|' ]; then
	    local cmd
	    echo -n "Enter a command: "
	    read cmd
	    $cmd
	    return 255
	else
	    strval="$resp"
	fi
    else
	if [ ! -z "$default" ]; then
	    strval="$default"
	else
	    echo "!!!!! You must enter a value"
	    return 255
	fi
    fi

    return 0
}

function get_passwd
{
    local prompt=$1
    local default=$2
    local display_help=$3
    local prompt_default
    resp="1"
    local resp2="2"

    if [ -z "$default" ]; then
        prompt_default="<none>"
    else
        prompt_default="<current password>"
    fi

    until [ "$resp" == "$resp2" ]
    do
        echo -n ">>>>> $prompt (\"?\" for help) [default: $prompt_default] "
        stty -echo
        read resp
        stty echo
        echo
        echo

        if [ ! -z "$resp" ]; then
            if [ "$resp" == '?' ]; then
                $display_help
                continue
            elif [ "$resp" == '|' ]; then
                local cmd
                echo -n "Enter a command: "
                read cmd
                $cmd
            fi
        else
            if [ ! -z "$default" ]; then
                strval="$default"
            else
                echo "!!!!! You must enter a value"
                return 255
            fi
        fi

        echo -n ">>>>> Re-enter password: "
        stty -echo
        read resp2
        stty echo
        echo
        echo

        if [ "$resp" != "$resp2" ]; then
            echo -n "Passwords did not match - start over."
            echo
        fi
    done

    if [ "$resp" == "$resp2" ]; then
        strval="$resp"
        return 0
    else
        strval=""
        return 255
    fi
}

function get_number
{
    local prompt=$1
    local min=$2
    local max=$3
    local default=$4
    local prompt_default
    local resp
    local filtered

    if [ -z "$default" ]; then
	prompt_default="<none>"
    else
	prompt_default=$default
    fi

    echo -n ">>>>> $prompt [default: ${prompt_default}] "
    read resp
    filtered=$( echo "$resp" | tr -d -C '0123456789' )

    if [ -z "$resp" -a ! -z "$default" ]; then
	numberval=$default;
	return 0;
    fi

    if [ -z "$resp" -o "$filtered" != "$resp" ]; then
	echo "!!!!! Please enter a number between $min and $max"
	return 255;
    fi

    let resp="0+$resp"
    if [ $resp -ge $min -a $resp -le $max ]; then
	numberval=$resp
    else
	echo "!!!!! Please enter a number between $min and $max"
	return 255
    fi

    return 0
}

function get_option
{
    # get_option "Prompt" "A b C"
    #  Prompt for single letter option from space-separated list.  Options are case-insensitive

    local prompt=$1
    local options=$2

    local opts=${2// /}
    opts=$( echo "$opts" | tr '[:lower:]' '[:upper:]' )
    local resp
    local filtered

    echo -n ">>>>> $prompt "
    read resp

    resp=$( echo "$resp" | tr '[:lower:]' '[:upper:]' )
    if [ `expr length "$resp"` -ne 1 -o `expr index "$opts" "$resp"` -eq 0 ]; then
	echo "!!!!! Please enter one of " "$options"
	return 255;
    fi

    strval=$resp

    return 0
}

function get_menuitem
{
    # get_menuitem "Prompt" "A - First menu item" "B - second"
    #  menu items are displayed as-is.  First letter of each item used to (case-insensitively) select item

    local prompt=$1
    local s options
    options=''
    for s in "${@:2}"; do
	options="${options} ${s:0:1}"
	echo "   " $s
    done
    until get_option "$prompt" "$options"; do continue; done
}

function edit_list
{
    local prompt=$1
    local list_heading=$2
    local display_entry=$3
    local edit_entry=$4
    local s i index option

    option=''
    until [ "x$option" == "xF" ]
    do
      echo $list_heading
      i=1
      for s in ${arrayval[@]}; do echo -n "  $i: "; $display_entry $s; let i=$i+1; done
      if [ ${#arrayval[@]} -gt 0 ]; then
	  get_menuitem "Please choose an option " "A - Add" "E - Edit" "D - Delete" "F - Finished"
      else
	  get_menuitem "Please choose an option " "A - Add" "F - Finished"
      fi
      option=$strval
      case "$option" in
	  "A" )
            let index=${#arrayval[@]}
	    $edit_entry ""
	    arrayval[$index]=$strval
	    ;;

	  "E" | "D" )
            until get_number "$prompt" 1 ${#arrayval[@]} ""; do continue; done
	    let index=numberval-1
	    if [ "$option" == "D" ]; then
		until get_yorn "Delete entry $numberval? " "n" ; do continue; done
		if [ "$yorn" == "yes" ]; then
		    while [ $index -lt $((${#arrayval[@]}-1)) ]; do
		      arrayval[$index]=${arrayval[$((index+1))]}
		      let index=index+1
		    done
		    unset arrayval[$index]
		fi
	    else
		$edit_entry ${arrayval[$index]}
		arrayval[$index]=$strval
	    fi
	    ;;
       esac
       done
}

function echo_bash_literal
{
    echo -n "'"
    echo -n "$1" | sed -e "s+'+'\"'\"'+"
    echo -n "'"
}

function save_config
{
    mv $cluster/global-config $cluster/global-config.bak

    echo "## See config.defaults for descriptions of these values" > $cluster/global-config
    echo "config_version=1" >> $cluster/global-config
    echo "product=$product" >> $cluster/global-config
    echo "brand_image=$brand_image" >> $cluster/global-config
    echo "client_version=\"$client_version\"" >> $cluster/global-config
    echo "iic_user=$iic_user" >> $cluster/global-config
    echo "iic_mtg_group=$iic_mtg_group" >> $cluster/global-config
    echo "iic_home=$iic_home" >> $cluster/global-config
    echo "jabber_home=$jabber_home" >> $cluster/global-config
    echo "db_host=$db_host" | tr A-Z a-z >> $cluster/global-config
    echo "db_name=$db_name" >> $cluster/global-config
    echo "db_user=$db_user" >> $cluster/global-config
    echo "db_pswd=$db_pswd" >> $cluster/global-config
    echo "db_ip_addr=( ${db_ip_addr[*]} )" >> $cluster/global-config
    echo "db_netmask=( ${db_netmask[*]} )" >> $cluster/global-config
    echo "svc_base_port=$svc_base_port" >> $cluster/global-config
    echo "multi_ip=$multi_ip" >> $cluster/global-config
    echo "external_hname=$external_hname" | tr A-Z a-z >> $cluster/global-config
    echo "portal_hname=$portal_hname" | tr A-Z a-z >> $cluster/global-config
    echo "portal_ip=( ${portal_ip[*]} )" >> $cluster/global-config
    echo "portal_local_hname=( ${portal_local_hname[*]} )" | tr A-Z a-z >> $cluster/global-config
    echo "extapi_portal_hname=$extapi_portal_hname" | tr A-Z a-z >> $cluster/global-config
    echo "extapi_portal_local_hname=$extapi_portal_local_hname" | tr A-Z a-z >> $cluster/global-config
    echo "extapi_portal_port=$extapi_portal_port" >> $cluster/global-config
    echo "extapi_portal_ip=$extapi_portal_ip" >> $cluster/global-config
    echo "service_secret=\"$service_secret\"" >> $cluster/global-config
    echo "session_creation_secret=\"$session_creation_secret\"" >> $cluster/global-config
    echo "lcl_xmlrouter=( ${lcl_xmlrouter[*]} )" | tr A-Z a-z >> $cluster/global-config
    echo "lcl_xmlrouter_ip=( ${lcl_xmlrouter_ip[*]} )" >> $cluster/global-config
    echo "iptables_port_forwarding=$iptables_port_forwarding" >> $cluster/global-config
    echo "iic_client_port=$iic_client_port"  >> $cluster/global-config
    echo "jadc2s_instances=$jadc2s_instances" >> $cluster/global-config
    echo "jadc2s_port=( ${jadc2s_port[*]} )" >> $cluster/global-config
    echo "extportal_instances=$extportal_instances" >> $cluster/global-config
    echo "extportal_port=( ${extportal_port[*]} )" >> $cluster/global-config
    echo "controller_host=( ${controller_host[*]} )" | tr A-Z a-z >> $cluster/global-config
    echo "controller_port=( ${controller_port[*]} )" >> $cluster/global-config
    echo "voice_host=( ${voice_host[*]} )" | tr A-Z a-z >> $cluster/global-config
    echo "voice_port=( ${voice_port[*]} )" >> $cluster/global-config

    echo -n "voice_phones=( " >> $cluster/global-config
    local idx=0
    while [ $idx -lt ${#voice_phones[*]} ]; do

	echo -n "\"" >> $cluster/global-config
	echo -n $( echo ${voice_phones[$idx]} | sed -e 's+"+\\"+g' ) \
	    >> $cluster/global-config
	echo -n "\" " >> $cluster/global-config

	idx=$(($idx+1))
    done
    echo ")" >> $cluster/global-config

    echo "multi_phone_bridges=$multi_phone_bridges" >> $cluster/global-config
    echo "mtgarchiver_host=( ${mtgarchiver_host[*]} )" | tr A-Z a-z >> $cluster/global-config
    echo "mtgarchiver_voice_host=( ${mtgarchiver_voice_host[*]} )" | tr A-Z a-z >> $cluster/global-config
    echo "mtgarchiver_prio=$mtgarchiver_prio" >> $cluster/global-config
    echo "mtgarchiver_port=( ${mtgarchiver_port[*]} )" >> $cluster/global-config
    echo "addressbk_host=( ${addressbk_host[*]} )" | tr A-Z a-z >> $cluster/global-config
    echo "addressbk_port=( ${addressbk_port[*]} )" >> $cluster/global-config
    echo "mailer_host=( ${mailer_host[*]} )" | tr A-Z a-z >> $cluster/global-config
    echo "mailer_port=( ${mailer_port[*]} )" >> $cluster/global-config
    echo "converter_host=( ${converter_host[*]} )" | tr A-Z a-z >> $cluster/global-config
    echo "converter_port=( ${converter_port[*]} )" >> $cluster/global-config
    echo "extapi_host=( ${extapi_host[*]} )" | tr A-Z a-z >> $cluster/global-config
    echo "extapi_port=( ${extapi_port[*]} )" >> $cluster/global-config
    echo "share_host_equal=$share_host_equal" >> $cluster/global-config
    echo "share_host=( ${share_host[*]} )" | tr A-Z a-z >> $cluster/global-config
    echo "share_local_host=( ${share_local_host[*]} )" | tr A-Z a-z >> $cluster/global-config
    echo "share_ip=( ${share_ip[*]} )" >> $cluster/global-config
    echo "sharemgr_host=$sharemgr_host" | tr A-Z a-z >> $cluster/global-config
    echo "sharemgr_port=$sharemgr_port" >> $cluster/global-config
    echo "sharemgr_control_port=$sharemgr_control_port" >> $cluster/global-config
    echo "sharemgr_rpc_port=$sharemgr_rpc_port" >> $cluster/global-config
    echo "share_port=$share_port" >> $cluster/global-config
    echo "voice_provider=\"$voice_provider\"" >> $cluster/global-config
    echo "wav_port=$wav_port" >> $cluster/global-config
    echo "locale_setting=$locale_setting" >> $cluster/global-config
    echo "iic_prodname=\"$iic_prodname\"" >> $cluster/global-config
    echo "new_user_from_address=$new_user_from_address" >> $cluster/global-config
    echo "new_user_from_display=\"$new_user_from_display\"" >> $cluster/global-config
    echo "new_user_subject=\"$new_user_subject\"" >> $cluster/global-config
    echo "smtp_host=$smtp_host" | tr A-Z a-z >> $cluster/global-config
    echo "smtp_user=$smtp_user" >> $cluster/global-config
    echo "smtp_pswd=\"$smtp_pswd\"" >> $cluster/global-config
    echo "default_from_address=$default_from_address" >> $cluster/global-config
    echo "default_from_display=\"$default_from_display\"" >> $cluster/global-config
    echo "invite_from_address=$invite_from_address" >> $cluster/global-config
    echo "invite_from_display=\"$invite_from_display\"" >> $cluster/global-config
    echo "invite_subject=\"$invite_subject\"" >> $cluster/global-config
    echo "mtgarchiver_from_address=$mtgarchiver_from_address" >> $cluster/global-config
    echo "mtgarchiver_from_display=\"$mtgarchiver_from_display\"" >> $cluster/global-config
    echo "mtgarchiver_subject=\"$mtgarchiver_subject\"" >> $cluster/global-config
    echo "event_rotation_time=\"$event_rotation_time\"" >> $cluster/global-config
    echo "sysadm_email=(${sysadm_email[*]})" >> $cluster/global-config
    echo "db_backup_dir=$db_backup_dir" >> $cluster/global-config
    echo "db_backup_history=$db_backup_history" >> $cluster/global-config
    echo "db_backup_hr=$db_backup_hr" >> $cluster/global-config
    echo "db_backup_min=$db_backup_min" >> $cluster/global-config

    echo "mtgarchive_cleaning_threshold=$mtgarchive_cleaning_threshold" >> $cluster/global-config
    echo "mtgarchive_target_sz=$mtgarchive_target_sz" >> $cluster/global-config
    echo "mtgarchive_cleanup_hr=$mtgarchive_cleanup_hr" >> $cluster/global-config
    echo "mtgarchive_cleanup_min=$mtgarchive_cleanup_min" >> $cluster/global-config
    echo "mtgarchive_cleanup_action=$mtgarchive_cleanup_action" >> $cluster/global-config
    echo "mtgarchive_cleanup_min_age=$mtgarchive_cleanup_min_age" >> $cluster/global-config
    echo "mtgarchive_trash_folder=$mtgarchive_trash_folder" >> $cluster/global-config

    echo "docshare_cleaning_threshold=$docshare_cleaning_threshold" >> $cluster/global-config
    echo "docshare_target_sz=$docshare_target_sz" >> $cluster/global-config
    echo "docshare_cleanup_hr=$docshare_cleanup_hr" >> $cluster/global-config
    echo "docshare_cleanup_min=$docshare_cleanup_min" >> $cluster/global-config
    echo "docshare_cleanup_action=$docshare_cleanup_action" >> $cluster/global-config
    echo "docshare_cleanup_min_age=$docshare_cleanup_min_age" >> $cluster/global-config
    echo "docshare_trash_folder=$docshare_trash_folder" >> $cluster/global-config

    echo "rotation_size=$rotation_size" >> $cluster/global-config
    echo "history_len=$history_len" >> $cluster/global-config
    echo "log_level=$log_level" >> $cluster/global-config

    [ -z "$enable_rt_logs" ] && enable_rt_logs="no"

    echo "enable_rt_logs=$enable_rt_logs" >> $cluster/global-config
    echo "rt_user_port=$rt_user_port"     >> $cluster/global-config
    echo "rt_call_port=$rt_call_port"     >> $cluster/global-config
    echo "rt_rsrv_port=$rt_rsrv_port"     >> $cluster/global-config

    [ -z "$log_chat" ] && log_chat="no"

    echo "log_chat=$log_chat" >> $cluster/global-config
    echo "log_chat_clean_action=$log_chat_clean_action" >> $cluster/global-config
    echo "log_chat_clean_sz=$log_chat_clean_sz" >> $cluster/global-config
    echo "log_chat_trash_folder=$log_chat_trash_folder" >> $cluster/global-config

    echo "enable_security=$enable_security" >> $cluster/global-config
    echo "protocol_prefix=$protocol_prefix" >> $cluster/global-config
    echo "server_certificate=$server_certificate" >> $cluster/global-config
    echo "web_server_certificate=$web_server_certificate" >> $cluster/global-config

    echo "ha_primary=$ha_primary" >> $cluster/global-config
    echo "ha_backup=$ha_backup" >> $cluster/global-config
    echo "install_ha=$install_ha" >> $cluster/global-config
    
    echo "voip_pcphone=$voip_pcphone" >> $cluster/global-config
    echo "voip_ip=( ${voip_ip[*]} )" >> $cluster/global-config
    echo "voip_host=( ${voip_host[*]} )" | tr A-Z a-z >> $cluster/global-config
    echo "nms_base_ip=( ${nms_base_ip[*]} )" >> $cluster/global-config

    echo "ast_main_addr=( ${ast_main_addr[*]} )" >> $cluster/global-config
    echo "ast_main_name=( ${ast_main_name[*]} )" >> $cluster/global-config
    echo "ast_mgr_port=( ${ast_mgr_port[*]} )" >> $cluster/global-config
    echo "ast_main_username=( ${ast_main_username[*]} )" >> $cluster/global-config
    echo "ast_main_secret=( ${ast_main_secret[*]} )" >> $cluster/global-config
    echo "ast_main_max=( ${ast_main_max[*]} )" >> $cluster/global-config

    echo "dial_prefix=$dial_prefix" >> $cluster/global-config
    echo "board_pstn_trunks=$board_pstn_trunks" >> $cluster/global-config
    echo "board_max_calls=$board_max_calls" >> $cluster/global-config
    echo "board_num_recorders=$board_num_recorders" >> $cluster/global-config
    echo "pstn_line_protocol=$pstn_line_protocol" >> $cluster/global-config

    echo "sip_default_domain=$sip_default_domain" >> $cluster/global-config
    echo "sip_outbound_proxy=$sip_outbound_proxy" >> $cluster/global-config
    echo "isdn_operator=$isdn_operator" >> $cluster/global-config
    echo "isdn_country=$isdn_country" >> $cluster/global-config

    echo "enable_ldap_sync=$enable_ldap_sync" >> $cluster/global-config
    echo -n "ldap_servers=( " >> $cluster/global-config
    local server
    for server in ${ldap_servers[@]}; do
	echo_bash_literal "$server" >> $cluster/global-config
	echo -n " " >> $cluster/global-config
    done
    echo ")"  >> $cluster/global-config
    echo "ldap_on_schedule=$ldap_on_schedule" >> $cluster/global-config
    echo "ldap_sync_hour=$ldap_sync_hour" >> $cluster/global-config
    echo "locale_setting=$locale_setting" >> $cluster/global-config
}

function copy_conf_item
{
    local item=$1

    if [ -e $cluster/$item ]; then
	cp $cluster/$item /opt/iic/conf
    else
	echo "Using defaults $item"
    fi
}

function copy_bin_item
{
    local item=$1

    if [ -e $cluster/$item ]; then
	cp $cluster/$item /opt/iic/bin
	chmod +x /opt/iic/bin/$item
    fi
}

function get_unique_hosts
{
    local raw_hosts=" \
	$db_host \
	$portal_local_hname \
	${extapi_host[*]} \
	${lcl_xmlrouter[*]} \
	${controller_host[*]} \
	${voice_host[*]} \
	${mtgarchiver_host[*]} \
	${addressbk_host[*]} \
	${mailer_host[*]} \
	${converter_host[*]} \
	${share_local_host[*]} \
	"
    unique_hosts=( $( echo $raw_hosts | tr ' ' '\n' | sort -u | tr '\n' ' ') )
}

function validate_ip_array
{
    local status=0

    while [ $# -gt 0 ]; do
	if ! validate_ip $1 ; then
	    status=-1
	fi

	if ! ping_address $1 ; then
	    status=-1
	fi

	shift
    done

    return $status
}

function validate_host_array
{
    local status=0

    while [ $# -gt 0 ]; do
	if ! ping_address $1 ; then
	    status=-1
	fi

	shift
    done

    return $status
}

function validate_ip
{
    local in=$1
    local -a comps=( $( echo $in | tr '.' ' ' ) )

    if [ "${#comps[*]}" -ne 4 ]; then
	echo "!!!!! $in does not have 4 dot-separated components"
	return 255
    fi

    if [ ${comps[0]} -ge 0 -a ${comps[0]} -lt 255 ] &&
	[ ${comps[1]} -ge 0 -a ${comps[1]} -lt 255 ] &&
	[ ${comps[2]} -ge 0 -a ${comps[2]} -lt 255 ] &&
	[ ${comps[3]} -ge 0 -a ${comps[3]} -lt 255 ] ; then
	return 0
    else
	echo "!!!!! $in is not well-formed"
	return 255
    fi

    return 0
}

function ping_address
{
    echo -n "Pinging $1 ... "
    ping -c 1 $1 >& /dev/null
    if [ $? -eq 0 ]; then
	echo "ok."
	return 0
    else
	echo "FAILED."
	return 255
    fi
}

function escape_xml
{
    strval=$(echo "$1" | sed -e 's/&/\&amp;/g' | sed -e 's/</\&lt;/g' -e 's/>/\&gt;/g')
}

function unescape_ldap_part
{
    local part=${1//=20/ }
    part=${part//=3B/;}
    strval=${part//=3D/=}
}

function escape_ldap_part
{
    local part=${1//=/=3D}
    part=${part//;/=3B}
    strval=${part// /=20}
}

function explode_ldap_string
{
    local parts=( $(echo $1 | tr ';' ' ' ) )

    unescape_ldap_part ${parts[0]}
    ldap_url=$strval

    unescape_ldap_part ${parts[1]}
    ldap_binddn=$strval

    unescape_ldap_part ${parts[2]}
    ldap_basedn=$strval

    unescape_ldap_part ${parts[3]}
    ldap_import_filter=$strval

    unescape_ldap_part ${parts[4]}
    ldap_auth_filter=$strval

    unescape_ldap_part ${parts[5]}
    ldap_community=$strval

    unescape_ldap_part ${parts[6]}
    ldap_bindpw=$strval
}

function implode_ldap_string
{
    escape_ldap_part "$ldap_url"
    local e_ldap_url=$strval

    escape_ldap_part "$ldap_binddn"
    local e_ldap_binddn=$strval

    escape_ldap_part "$ldap_basedn"
    local e_ldap_basedn=$strval

    escape_ldap_part "$ldap_import_filter"
    local e_ldap_import_filter=$strval

    escape_ldap_part "$ldap_auth_filter"
    local e_ldap_auth_filter=$strval

    escape_ldap_part "$ldap_community"
    local e_ldap_community=$strval

    escape_ldap_part "$ldap_bindpw"
    local e_ldap_bindpw=$strval

    strval="$e_ldap_url;$e_ldap_binddn;$e_ldap_basedn;$e_ldap_import_filter;$e_ldap_auth_filter;$e_ldap_community;$e_ldap_bindpw"
}

function build_ldap_import_xml
{
    explode_ldap_string $1

    escape_xml $ldap_bindpw

    strval="\
<server>\
<url>$ldap_url</url>\
<binddn>$ldap_binddn</binddn>\
<bindpassword>$strval</bindpassword>\
<basedn>$ldap_basedn</basedn>\
<filter>$ldap_import_filter</filter>\
<communityid>$ldap_community</communityid>\
</server>"
}

function local_install
{
    ext_dir=${zon_svr_tarball%.tar.gz}

    if [ $is_new_install == "no" -a -e /opt/iic/conf/control-services.sh ]; then
	/opt/iic/conf/control-services.sh stop
    fi

    cat $zon_svr_tarball | ( cd /root ; tar zxvf - )

    (cd /root/$ext_dir ; ./install.sh --clean )
    copy_conf_item global-config
    (cd /root/$ext_dir ; ./install-bridge.sh $voice_provider )
    copy_conf_item $client_installers_tarball
    cp -r -p $cluster/locale /opt/iic
    copy_conf_item dialing.xml
    copy_conf_item voiceconfig.xml
    copy_bin_item extvoiced
    if [ -e ldap-ca.cer ]; then
	cp ldap-ca.cer /opt/iic/conf
    fi

    if [ -e $cluster/db_upgrade.sh ]; then
	cp $cluster/db_upgrade.sh /opt/iic/conf
    fi

    #  if [ $is_new_install == "yes" -o ! -e $server_certificate ]; then
    if [ ! -e $server_certificate ]; then
      copy_conf_item server.pem
    fi

    #  if [ $is_new_install == "yes" -o ! -e $web_server_certificate ]; then
    if [ ! -e $web_server_certificate ]; then
      copy_conf_item wserver.pem
    fi

    copy_conf_item privkey.pem

    local xml
    if [ "$enable_ldap_sync" == "yes" ]; then
	local server
	xml='<!-- Servers -->'
	for server in ${ldap_servers[@]}; do
	    build_ldap_import_xml $server
	    xml="$xml""$strval"
	done
    else
	xml='<!-- Disabled -->'
    fi
    sed \
        -e "/@@@SERVERS@@@/ c $xml" \
        $cluster/ldap.xml.in > /opt/iic/conf/ldap.xml

    ( cd /opt/iic/conf ; ./configure.sh $initdbarg )

    /opt/iic/conf/control-services.sh start
}

function idx2ord
{
    local idx=$1

    case $idx in
	0)
	    ord="1st"
	    ;;
	1)
	    ord="2nd"
	    ;;
	2)
	    ord="3rd"
	    ;;
	*)
	    ord="$(($idx+1))th"
    esac
}

function install
{
    local prod=$1

    echo "Will install Conferencing clients from $client_installers_tarball version $client_version"

    # find the server tarball with the highest version
    find_svr_tarball $prod

    if [ ${#unique_hosts[*]} -eq 1 -a -z "$single_host" ]; then
	if [ "${unique_hosts[0]}" == "localhost" -o "$this_host" == "${unique_hosts[0]}" ]; then
	    local_install
	elif [ "${unique_hosts[0]}" != "$this_host" ]; then
	    until get_yorn "Remotely install on ${unique_hosts[0]}?" "y" show_remote_install_help ; do continue; done
	    if [ "$yorn" == "no" ]; then
	    # who knows why they don't match, but do a local install
		local_install
	    else
		./check-config.sh --cluster $cluster --single-host ${unique_hosts[0]}
		if [ $? -ne 0 ]; then
		    exit $?
		fi

		if [ $is_new_install == "no" ]; then
		    ./control-cluster --cluster $cluster --single-host ${unique_hosts[0]} stop
		fi

		./install-cluster.sh --cluster $cluster --single-host ${unique_hosts[0]} --clean --tarball $zon_svr_tarball
		if [ $? -ne 0 ]; then
		    exit $?

		fi
		./config-cluster.sh  --cluster $cluster --single_host ${unique_hosts[0]} $initdbarg
		if [ $? -ne 0 ]; then
		    exit $?
		fi

		./control-cluster --cluster $cluster --single-host ${unique_hosts[0]} start
	    fi
	fi
    else
	single_host_arg=""
	if [ ! -z "$single_host" ]; then
	    single_host_arg="--single-host $single_host "
	fi

	./check-config.sh --cluster $cluster $single_host_arg
	if [ $? -ne 0 ]; then
	    exit $?
	fi

	if [ $is_new_install == "no" ]; then
	    ./control-cluster.sh --cluster $cluster $single_host_arg stop

	    if [ "${prior_unique_hosts[*]}" != "${unique_hosts[*]}" ]; then
		uninstall_removed_hosts
	    fi
	else
	    # even though they say
	    ./control-cluster.sh --cluster $cluster $single_host_arg stop >& /dev/null
	fi

	./install-cluster.sh --cluster $cluster $single_host_arg --clean --tarball $zon_svr_tarball
	if [ $? -ne 0 ]; then
	    exit $?
	fi

	./config-cluster.sh  --cluster $cluster $single_host_arg $initdbarg
	if [ $? -ne 0 ]; then
	    exit $?
	fi

	./control-cluster.sh --cluster $cluster $single_host_arg start
    fi
}

function find_svr_tarball
{
    local latest_version=$( ls $product-svr-*.tar.gz | \
	sed -e "s+$product-svr-++" -e 's+.tar.gz++' | \
	tr '.' '_' | \
	sort -n -k 1 -k 2 -k 3 --field-separator=_ | \
	tr '_' '.' | \
	tail -1 )
    zon_svr_tarball="$product-svr-$latest_version.tar.gz"

    echo "Will install Conferencing server components from $zon_svr_tarball"
}

function update_client_version
{
    local -a tarballs=( $cluster/installers_*.tar.gz )
    if [ ! -e $cluster/installers.tar.gz -a ! -e $cluster/installers_${client_version/./_}.tar.gz ]; then
	if [ "${tarballs[0]}" != "$cluster/installers_*.tar.gz" ]; then
	    local latest_version=$( \
		echo ${tarballs[*]} | \
		sed -e 's+.*/installers_++' -e 's+.tar.gz++' | \
		sort -n -k 1 -k 2 -k 3 -k 4 --field-separator=_ | \
		tail -1 | \
		tr '_' '.' )
	    echo "Found installers w/ new version $latest_version ; updating global-config (client version was $client_version)"
	    client_version=$latest_version
	    save_config
	fi
    fi
}

function check_missing_config
{
	if [ -z "$brand_image" ]; then
		brand_image=apreta_logo.png
	fi
	
	if [ -z "$locale_setting" ]; then
		locale_setting="en_US"
	fi	
}
