#!/bin/bash

# OVERVIEW
#
# The cluster configuration is checked for the following problems:
#    - host specified in config does not match return value of /bin/hostname on host
#

. ./utils.sh 

function usage
{
    echo "usage: $0 --cluster <clustername> [--single-host <hostname>]"
}

cluster=""
single_host=""

function parse_cmdline
{
    while [ $# -gt 0 ] ; do
	case $1 in
	    --cluster)
		cluster=$2
		shift 2
		;;
	    --single-host)
		single_host=$2
		shift 2
		;;
	    --help|-h)
		usage
		exit -1
		;;
	    *)
		echo "Unknown argument '$1' seen"
		usage
		exit -1
		;;
	esac
    done
}

parse_cmdline $*

[ -z "$cluster" -a -e cluster.txt ] && cluster=$(cat cluster.txt)
[ -z "$cluster" ] && echo "Set a cluster name using the --cluster argument" && exit -1 
config="$cluster/global-config"
[ ! -e "$config" ] && echo "Cluster configuration $config doesn't exist" && exit -1

. $config

if ! check_global_config_hosts ; then
    exit -1
fi
