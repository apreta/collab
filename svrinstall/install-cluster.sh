#!/bin/bash

# OVERVIEW
#
# The server release tarball is sprayed into the root home directory of all the
# hosts in the cluster.  The non-voice-bridge RPM's contained in the tarball
# are all installed and the voice bridge (stub if no bridge) is installed on
# the voice server.
#
# See config-cluster.sh for cluster configuration
#

. ./utils.sh 

function usage
{
    echo "usage: $0 --cluster <clustername> [--single-host <hostname>] --tarball <server-release-tarball> [--clean] [--verify]"
}

clean="false"
cluster=""
tarball=""
single_host=""
verify="false"

function parse_cmdline
{
    while [ $# -gt 0 ] ; do
	case $1 in
	    --tarball)
		tarball=$2
		echo "$tarball" > tarball.txt
		shift 2
		;;
	    --single-host)
		single_host=$2
		shift 2
		;;
	    --cluster)
		cluster=$2
		echo "$cluster" > cluster.txt
		shift 2
		;;
	    --clean)
		clean="true"
		shift
		;;
	    --verify)
		verify="true"
		shift
		;;
	    --help|-h)
		usage
		exit -1
		;;
	    *)
		echo "Unknown argument '$1' seen"
		usage
		exit -1
		;;
	esac
    done
}

function is_voice_bridge
{
    echo "${voice_host[*]}" | grep --silent $1
    return $?
}

function host_install
{
    local remote_tarball=${tarball##.*/}
    local remote_dist_dir=${remote_tarball%.tar.gz}
    local clean_arg=""

    [ "$clean" == "true" ] && clean_arg=" --clean"

    push_file $1 $tarball && echo "Ok" || echo "FAILED; see install-cluster.log"
    remote_cmd $1 "rm -rf $remote_dist_dir ; tar zxvf $remote_tarball" && echo "Ok" || echo "FAILED; see install-cluster.log"
    remote_cmd $1 "cd $remote_dist_dir ; ./install.sh$clean_arg" && echo "Ok" || echo "FAILED; see install-cluster.log"
    if is_voice_bridge $1 || [ ! -z "$single_host" ]; then
	if [ -e $cluster/extvoiced ]; then
	    push_file $1 $cluster/extvoiced /opt/iic/bin
	    remote_cmd $1 "chmod +x /opt/iic/bin/extvoiced"
	else
	    if [ -e $cluster/appconfvoiced ]; then
		push_file $1 $cluster/appconfvoiced /opt/iic/bin
		remote_cmd $1 "chmod +x /opt/iic/bin/appconfvoiced"
	    fi
	fi
	remote_cmd $1 "cd $remote_dist_dir ; ./install-bridge.sh $voice_provider" && echo "Ok" || echo "FAILED; see install-cluster.log"
    fi
}

function distribute_install
{
    if [ -z "$single_host" ]; then
	local raw_hosts=" \
        $db_host \
	$portal_local_hname \
	$extapi_portal_local_hname \
	${lcl_xmlrouter[*]} \
	${controller_host[*]} \
	${voice_host[*]} \
	${mtgarchiver_host[*]} \
	${addressbk_host[*]} \
	${mailer_host[*]} \
	${converter_host[*]} \
	${extapi_host[*]} \
	${sharemgr_host[*]} \
	${share_local_host[*]} \
	"

	local -a uniq_hosts=( $( echo $raw_hosts | tr ' ' '\n' | sort -u | tr '\n' ' ') )

	for h in ${uniq_hosts[*]} ; do
	    host_install $h
	done
    else
	host_install $single_host
    fi
}

parse_cmdline $*

[ -z "$cluster" -a -e cluster.txt ] && cluster=$(cat cluster.txt)
[ -z "$tarball" -a -e tarball.txt ] && tarball=$(cat tarball.txt)

[ -z "$cluster" ] && echo "Set a cluster name using the --cluster argument" && exit -1 
[ ! -e "$cluster" ] && echo "Create a cluster directory to hold the cluster configuration files" && exit -1
[ -z "$tarball" ] && echo "Specify a distro tarball using the --tarball argument" && exit -1
[ ! -e "$tarball" ] && echo "Tarball $tarball doesn't exist" && exit -1

config="$cluster/global-config"

[ ! -e "$config" ] && echo "Cluster configuration $config doesn't exist" && exit -1

. $config

distribute_install
