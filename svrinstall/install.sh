#!/bin/bash

if [ -f ./settings.sh ]; then
	. ./settings.sh
fi
. ./utils.sh 
. ./ldaputils.sh 

function split_phones
{
    echo \"${1//~/\" \"}\"
}

function show_log_chat_maintenance_help
{
    cat<<EOF
All Conferencing chat activity (in-meeting chat, Conferencing IM and Conferencing chat room) can
be logged to audit files.  If you want this feature, you can choose to
configure it now.
EOF
}

function show_log_chat_clean_sz_help
{
    cat<<EOF
When the chat audit logs reach this size, the system will take action.
Whether that action is to remove audit logs or to send system
administrators e-mail is specified in a later question.
EOF
}

function show_update_bridge_phones_help
{
    cat<<EOF
Choose 'y' if the phone numbers for this voice bridge need to be updated.
EOF
}

function show_update_phone_help
{
    cat<<EOF
Enter the updated phone number.  You may not use the tilde(~) or
double-quote(") characters in a phone number.
EOF
}

function show_action_help
{
    cat<<EOF
To select a phone number to either update or delete enter its line number.
To add a new phone number, enter "a".
EOF
}

function show_update_phone_help
{
    cat<<EOF
If you want to modify this phone number rather than deleting it, choose 'n'.
EOF
}

function update_phone
{
    if [ ! -z "$1" ]; then
	until get_yorn "Do you want to delete $1 (n = modify)?" "n" show_update_phone_help ; do continue ; done
	if [ "$yorn" == "yes" ]; then
	    strval=""
	    return
	fi
    fi

    while true ; do
	until get_strval "What is the new phone number?" "$1" show_update_phone_help ;
	  do continue ; done
	if expr index "$strval" "~\"" ; then
	    echo "You can't use tilde(~) or double quote(\") characters in phone numbers"
	else
	    break;
	fi
    done
}

function update_bridge_phones
{
    local identity
    local upd_default
    local idx=0
    while [ $idx -lt ${#voice_host[*]} ]; do
	if [ "${voice_host[$idx]}" == "localhost" ]; then
	    identity=""
	else
	    identity=" for ${voice_host[$idx]}"
	fi

	if [ -z "${voice_phones[$idx]}" ]; then
	    upd_default="y"
	else
	    upd_default="n"
	fi

	until get_yorn "Do you want to update the bridge phone numbers$identity" $upd_default show_update_bridge_phones_help ; do continue; done
	if [ "$yorn" == "yes" ]; then
	    update_bridge_phones_for $idx
	fi

	(( idx++ ))
    done
}

function update_bridge_phones_for
{
    local idx=$1

    local s=$(split_phones "${voice_phones[idx]}")
    local -a phones
    local n_phones=0

    if [ "$s" != "\"\"" ]; then
	eval `echo "phones=( $s )"`
	n_phones=${#phones[*]}
    fi

    while true ; do
	if [ $n_phones -eq 0 ]; then
	    local header="No phones defined"
	    local prompt="Action (a = add phone, e = end updates)"
	else
	    local header="The following phones are currently defined:"
	    local prompt="Action (1..$n_phones = select, a = add phone, e = end updates) [default: a]?"
	fi

	echo $header
	local pi=0
	while [ $pi -lt $n_phones ]; do
	    echo "$(($pi+1)). ${phones[$pi]}"
	    (( pi++ ))
	done
	until get_strval "$prompt" "a" show_action_help ; do continue; done
	case $strval in
	    a)
		update_phone ""
		if [ ! -z "$strval" ]; then
		    phones[$n_phones]="$strval"
		fi
		;;
	    e)
		break
		;;
	    *)
		local -i intv=$strval
		if [ $intv -gt 0 -a $intv -ge 1 -a $intv -le $n_phones ]; then
		    (( intv-- ))
		    # strval gets reset in update_phone
		    update_phone "${phones[$intv]}"
		    if [ ! -z "$strval" ]; then
			phones[$intv]="$strval"
		    else
			local ii=$intv
			while [ $ii -lt $(( $n_phones-1 )) ]; do
			    phones[$intv]=${phones[$((intv+1))]}
			    (( ii++ ))
			done
			unset phones[$((n_phones-1))]
		    fi
		else
		    echo "Unknown action $strval"
		fi
		;;
	esac
	n_phones=${#phones[*]}
    done

    voice_phones[$idx]="${phones[0]}"
    pi=1
    while [ $pi -lt $n_phones ]; do
	voice_phones[$idx]="${voice_phones[$idx]}~${phones[$pi]}"
	(( pi++ ))
    done
}

function set_client_hosts
{
    local tarball=$1
    local -a installers=( conferencing.exe conferencing.rpm conferencing-64bit.rpm )

    # installer .exe's containe patterns of 255 chars in a row
    local xml_char="S"
    # generate pattern for initial grep
    local xml_pat=$( echo $xml_char | gawk '{ for (i = 0; i < 255; i++) printf "%s\\000", $1 }' )
    # generate sed matching substitution
    local  xml_subst=$( echo $external_hname | gawk '{printf "%-255s\n", $1; }' | sed -e 's+.+\0\\o000+g' )

    local help_url_char="H"
    local help_url_subst=$( echo "$protocol_prefix://$portal_hname/imidio/client-manual/" | gawk '{printf "%-255s\n", $1; }' | sed -e 's+.+\0\\o000+g' )

    local mtg_lookup_url_char="M"
    local mtg_lookup_url_subst=$( echo "$protocol_prefix://$portal_hname/imidio/invite/getstartup.php" | gawk '{printf "%-255s\n", $1; }' | sed -e 's+.+\0\\o000+g' )

    local enable_security_char="E"
    local enable_security_subst=$( echo "$enable_security" | gawk '{printf "%-255s\n", $1; }' | sed -e 's+.+\0\\o000+g' )

    find_svr_tarball $prod
    ext_dir=${zon_svr_tarball%.tar.gz}
    tar -zxf ${zon_svr_tarball} ${ext_dir}/rpminject
    local rpminject="`pwd`/${ext_dir}/rpminject"
        
    mkdir -p /tmp/tarball-$$

    cat $tarball | ( cd /tmp/tarball-$$ ; tar zxf - )

    pushd /tmp/tarball-$$ > /dev/null

    echo -e $xml_pat > xml.pat

    local fn
    for fn in ${installers[*]} ; do
	if expr match "$fn" '.*rpm$' >/dev/nul; then
		# extract post install script and add server info
		$rpminject -q -t RPMTAG_POSTIN $fn >zon-$$.spec
		sed \
			-e "s+@@@SERVER@@@+$external_hname+" \
			-e "s+@@@HELPURL@@@+$protocol_prefix://$portal_hname/imidio/client-manual/+" \
			-e "s+@@@LOOKUPURL@@@+$protocol_prefix://$portal_hname/imidio/invite/getstartup.php+" \
			-e "s+@@@SECURE@@@+$enable_security+" \
			-e "s+RUNNOW=n+RUNNOW=y+" \
			-i \
			zon-$$.spec
		# replace post install script
		cat zon-$$.spec | $rpminject -m -t RPMTAG_POSTIN -v @ $fn
	elif grep -U --silent -f xml.pat $fn ; then
	    sed \
		-e "s+\($xml_char\o000\)\{255\}+$xml_subst+" \
		-e "s+\($help_url_char\o000\)\{255\}+$help_url_subst+" \
		-e "s+\($mtg_lookup_url_char\o000\)\{255\}+$mtg_lookup_url_subst+" \
		-e "s+\($enable_security_char\o000\)\{255\}+$enable_security_subst+" \
		$fn \
		> $fn-$$
	    rm $fn
	    mv $fn-$$ $fn
	else
	    echo "WARN: $fn doesn't have XML server pattern"
	fi
    done

    popd > /dev/null

    tar zcf $tarball -C /tmp/tarball-$$ ${installers[*]} ${otherfiles[*]}

}

function find_svr_tarball
{
    local product=conferencing
    local latest_version=$( ls $product-svr-*.tar.gz | \
	sed -e "s+$product-svr-++" -e 's+.tar.gz++' | \
	tr '.' '_' | \
	sort -n -k 1 -k 2 -k 3 --field-separator=_ | \
	tr '_' '.' | \
	tail -1 )
    zon_svr_tarball="$product-svr-$latest_version.tar.gz"

    echo "Will install Conferencing server components from $zon_svr_tarball"
}

function show_prerequisite_help
{
    cat<<EOF
Enter 'y' if you are satisifed that all prerequisites are completed.
Enter 'n' to exit installation.
EOF
}

function show_new_installation_help
{
    cat<<EOF
If you choose to create a new installation, the installer will create
an initial configuration based on your selections below.  You will
also be asked whether you want to initialize the system database.  You
must initialize a database once before it can be used by the system.
However, initializing a database that is already in use by the system
will result in all of the data being lost.  Only initialize a database
once on a database host.

If you chose to use upgrade/reconfigure a system, you will be prompted
for the name of a prior configuration to upgrade/reconfigure.
EOF
}

function show_use_saved_help
{
    cat<<EOF
A configuration may exist for a new installation if the installation
was exited using CTRL-C.  In this case, you can choose to use the
saved configuration at the time the CTRL-C was pressed.

If you don't use the saved configuration, an intial configuration will
be saved under the name $cluster.

You should CTRL-C now if $cluster is not a new configuration and you
have entered the name of a prior installation inadvertantly.
EOF
}

function summarize_existing_configurations
{
    local alts="$(find . -maxdepth 1 -mindepth 1 -type 'd' -print | grep -v 'cluster-prototype' | grep -v CVS | sed -e "s+^+\t+" | sort )"
    if [ ! -z "$alts" ]; then
    echo
	echo "The following prior installations exist:"
	for ins in $(echo $alts | sed -e "s+\./+\t+g" ) ; do
	    local stripped=$(echo $ins | sed -e 's+\..*++')
	    if [ "$stripped" == "$ins" ]; then
		echo -e "\t$ins"
	    elif [ -e "$stripped" ]; then
		echo -e "\t$ins (backup of $stripped)"
	    fi
	done
    fi
}

function show_this_host_help
{
    cat <<EOF
This installer can either install on this single host or install
remotely on another host of your choosing.  If you want to install
on this host, choose y.  If not, you will be prompted for the
other host for the installation.
EOF
}

function show_multi_ip_help
{
    cat <<EOF
Configuring multiple IP addresses (even if running all services on
a single server) allows better firewall traversal and HTTP connections
from remote clients.
EOF
}

function show_hour_help
{
    cat <<EOF
The system uses a 24 hour clock.  Valid hour values are
0 (i.e. 12 midnight) through 23 (i.e. 11 pm) inclusive.
For example, 3 is 3 am and 15 is 3 pm.
EOF
}

function show_minutes_help
{
    cat <<EOF
The minute of the hour is a value between 0 through 59 inclusive.
EOF
}

function show_config_name_help
{
    cat <<EOF
The configuration for an installation is stored under the configuration
name.  A subdirectory with the configuration name is created in the
current directory to hold the saved configuration for an installation.
EOF
}

function show_upgrade_help
{
    cat <<EOF
If you answer 'n', a new installation will be performed.  Otherwise,
an upgrade will be performed.

The main difference between a new installation and an upgrade is that
for a new installation, you are given the choice to initialize the
database.  For upgrades/reconfigurations you will never be asked
if you want to initialize the database.

If you choose to initialize the database, once the initialization
has started, it cannot be undone.  If you do intialize inadvertantly,
you will need to restore the database from the most recent backup.

The database, however, *must* be initialized once before the Conferencing
product can operate.
EOF
}

function show_modify_help
{
    cat<<EOF
If you choose not to modify a configuration, it will be deleted and
default parameters for all values will be used.  You probably don't
want to do this if you are just re-starting an installation that was
paused with Control-C.
EOF
}

function show_single_vs_multi
{
    cat <<EOF
You can install Conferencing on this host or on multiple hosts.  If you choose
to install on this host only, the installation process is simpler,
however, the Conferencing server will not be as available or handle as high a
load as a multiple host installation.

If you chose to install on multiple hosts, each host must have a
distinct host name (e.g. node1.abc.com, node2.abc.com, etc) for the
installation to function properly.  A 'local' host name entered during
installation must match the value returned by executing 'uname -n' on
a terminal on that host.

With a multiple host install, ssh(1) will be used to send files and
execute commands as root on the hosts.  To ease your installation
burden (typing the root password multiple times), you may wish to
configure ssh(1) to use private/public key authentication rather than
password authentication.  See the appendix in the Conferencing Installation
Manual for details.
EOF
}

function show_db_notes
{
    cat <<EOF
    - The PostgreSQL database server must be installed on one of the
      Conferencing hosts.  A default Enterprise Linux Server installation
      *WILL NOT* install the PostgreSQL database server.  You must
      choose it explicitly.
EOF
}

function show_ip_addr_notes
{
    cat <<EOF
    - You must define at least three IP addresses used by Conferencing clients
      to connect to Conferencing services.  The services that Conferencing clients will
      connect to are the XML router, web portal and
      desktop/app share (see the Conferencing Installation Guide
      for details).  The XML router and web portal each require a
      single IP address and the desktop/app share services need
      an IP address per instance.  If any Conferencing client is outside your
      firewall, the IP addresses must be externally reachable.
    - If using the Conferencing client PC Phone, an additional IP address will be 
      required for each voice bridge instance.
    DNS/HOSTNAME NOTES:
      - The XML router, web portal, desktop/app sharing, and PC Phone
        IP addresses require a hostname resolvable by Conferencing clients.
      - Multiple-machine Conferencing installations require each machine to have
        a distinct host name.  These host names do not need to be resolvable
        outside the local network.
EOF
}

function show_load_balancer_notes
{
    cat<<EOF
    FIREWALL NOTES:
          - The XML router IP address and desktop/app sharing IP
            address must have the following open ports:
	      1270, 443, 80
          - The web portal IP address must have port 80 open
          - If you choose to run Conferencing services in the DMZ and are
	    concerned about database security, you can run the database
	    inside the firewall.  XML routers and the Address Book
	    will connect to the database using port 5432
    LOAD BALANCER NOTES (multi-host installations only):
      - A load balancer is only required when either multiple web
        portal hosts or multiple XML router hosts are used.
      - Conferencing clients keep an open connection while a user is signed on.
      - Connections to the web portal should be sticky.  i.e. if a
        host is assigned to a web portal IP address, that host should
        continue to access that web portal IP address.
      
EOF
      
}

function show_license
{
    if [ ! -e cluster-prototype/EULA.txt ]; then
        echo "Aborting installation, EULA license text not available."
        exit 0
    fi

    if [ "$oss_install" == "y" ]; then
    	more -d <cluster-prototype/EULA.txt
    else
	more -d <cluster-prototype/Enterprise-EULA.txt
    fi
    until get_yorn "Do you accept the terms of this license?" "n" "Please answer yes to accept the license"; do continue; done;
    if [ "$yorn" == "no" ]; then
	exit 0
    fi
}

function show_prerequisites
{
    echo
    cat<<EOF
PREREQUISITES:
    - You must complete the following prerequisites before continuing
      to install.
    - Each Conferencing server host must be running SuSE Linux Enterprise 
      Server 10
    - During the install you will have an option to edit new user and
      invitation templates.  You can do this by editing on a desktop
      computer and importing, or by using emacs on an install host.
      Emacs may not be part of the default Enterprise Linux server
      installation.  You must explicitly install it.
    - All Conferencing hosts must be synchronized to the same time and the
      system timezones should all be consistent.  To maintain accurate
      system time, you can configure reliable timeservers in
      /etc/ntp.conf and /etc/ntp/step-tickers ( see man page for
      ntpd(1) ).
EOF
    show_db_notes
    pause_for_enter "prerequisites"
    show_ip_addr_notes
    pause_for_enter "prerequisites"
    show_load_balancer_notes

    until get_yorn "Are you ready to proceed (n = exit and restart later)?" "y" show_prerequisite_help; do continue; done;
    if [ "$yorn" == "no" ]; then
	exit 0
    fi
}

function show_installation_notes
{
    echo
    echo "INSTALLATION NOTES:"
    if [ $UID -ne 0 ]; then
	cat<<EOF
     - You must log onto the "root" user if you are doing a single-machine
       install on this host.
EOF
	echo "       You are currently logged on as \"$USER\""
    fi

    cat <<EOF
     - If you are unsure about the proper response to a prompt, you can
       enter ? for the prompted value to get detailed information about
       what's being requested.
     - You can press Enter at any prompt to use the displayed default value.
     - If you need to interrupt the installation, you can use Control-C.
       Your configuration settings will be saved and displayed as default
       values when you restart the installation.
EOF
}

function show_unreachable_help
{
    cat<<EOF
This installer attempts to ping hosts and IP addresses to ensure that
they are running and reachable.  If an intervening firewall is
filtering out ICMP messages or the host is intentionally down, it may
not appear to be reachable even though the entered value is valid.
You may choose to continue anyway in this case, but you should be
aware that the installer was not able to validate the value.
EOF
}

function show_db_init_help
{
    cat<<EOF
The installer will initialize the database on the host chosen to run
the database.  This must be performed once for the system to operate.
Initializing a database that is already in use will destroy the data
and would require the database to be restored from the most recent
backup.
EOF
}

function show_sysadmin_email_help
{
    cat<<EOF
You can define a list of system administrator e-mail address that are
used when exceptional events occur (like when the meeting archive
repository exceeds a max size).  An e-mail will be sent to all the
addresses on this list notifying the administrator(s) of the event.
EOF
}

function show_mailer_help
{
    cat<<EOF
The mailer is used to send Conferencing users e-mails notifying them of events
(e.g. invited to a meeting).  By default, the mailer uses the SMTP
service provided on its host.  If you don't want to use the SMTP
service on the mailer host, you can change the SMTP host and
optionally provide user and password authentication for that server.
EOF
}

function show_converter_help
{
    cat<<EOF
The converter is used to prepare uploaded documents/presentations.
EOF
}

function show_database_backup_help
{
    cat<<EOF
Once a day, the system will create a full backup of the database and
store the resulting archive in the given directory.  Prior daily
archives are rotated so the folder does not group w/o bound.

The folder used for the daily backup archives should not reside on the
database host in case of a catastrophic hardware failure.  You should
mount an external filesystem using NFS to hold the daily backup
archives.
EOF
}

function show_meeting_archive_maintenance_help
{
    cat<<EOF
Once a day the system will check the size of the meeting archive
repository.  If that repository exceeds a maximum size, the system can
either just inform the system administrators via e-mail that the
repository is oversized or it can being automatically deleting old
archives.

If the system is configured to automatically delete old archives, the
system will begin deleting archives from oldest to newest.  The system
will stop deleting archives when either the repository size falls
below a configurable target value or if all the archives are too young
to be deleted.
EOF
}

function show_edit_template_help
{
    cat<<EOF
You can choose to edit the e-mail body template in using the emacs
text editor (a short description of the process is below).  If you do
not want to edit the template using emacs, you will be given the
opportunity to import the template from a file.

If you *do* choose to edit the template, emacs will be started with
the template.  When you are finished editing, save the template (using
CTRL-X CTRL-S) and exit emacs (use CTRL-X CTRL-C).  If you are
unfamiliar with emacs, basic editing is described below.

EOF

    pause_for_enter "editing help"
    cat<<EOF
EDITING SUMMARY
---------------

To change the cursor position in emacs, you can use the arrow keys on
your keyboard

To insert text at the current cursor position, just type the text you
want to insert and it will be inserted to the left of the character
under the cursor.

To delete characters under the cursor use CTRL-D.

To move the cursor to the beginning of the line use CTRL-A

To delete from the cursor to the end of a line use CTRL-K (if the
cursor is positioned on an empty line, CTRL-K will delete the empty
line.  Therefore to delete a non-empty line entirely, use CTRL-K
CTRL-K )
EOF
}

function show_import_template_help
{
    cat<<EOF
If you want to import the e-mail body template from a file stored
locally on this host, choose 'y'.
EOF
}

function show_edits_ok_help
{
    cat<<EOF
If you are not satisfied with your changes, they will be discarded and you
will be given an opportunity to edit/import a new template.

If you are satisfied with your changes, the e-mail template will be updated.
EOF
}

function show_no_change_ok_help
{
    cat<<EOF
If you really don't have any changes to make to the e-mail template, you can
answer 'n' to this prompt.  Otherwise, answer 'y' and you will be given the
opportunity to edit/import the template.
EOF
}

function show_secret_key_help
{
    cat<<EOF
You can specify whether or not you wish to specify your own secret key.  If
you wish to have the system randomly generate a secret key, answer 'n'.
Otherwise, answer 'y' and you will be given the opportunity to edit the
current secret key.
EOF

}
function update_template
{
    local template=$1
    local try=1

    echo "You can either import a new template from a file or edit the template"
    while [ $try -eq 1 ]; do
	until get_yorn "Do you want to import the template from a file?" "y" show_import_template_help ; do continue; done
	if [ $yorn == "yes" ]; then
	    until get_strval "What is the full filename for the new template?" "/tmp/template.txt" ; do continue; done
	    if [ -e $strval ]; then
		echo "Importing $strval"
		cp $strval $template
		if [ $? -ne 0 ]; then
		    continue
		fi
		try=0
	    fi
	else
	    until get_yorn "Do you want to edit the template (using emacs)?" "y" show_edit_template_help ; do continue; done
	    if [ $yorn == "yes" ]; then

		cp $template /tmp/template-$$
		if [ $? -ne 0 ]; then
		    continue
		fi

		emacs /tmp/template-$$
		if [ $? -ne 0 ]; then
		    continue
		fi

		until get_yorn "Are you satisfied with your changes?" "y" show_edit_ok_help ; do continue; done
		if [ "$yorn" == "yes" ]; then
		    mv /tmp/template-$$ $template
		    if [ $? -ne 0 ]; then
		    # maybe there's a way to fix the problem; try again
			continue
		    fi
		    try=0
		else
		    echo "Changes discarded."
		    continue
		fi
	    else
		echo "!!!!! You didn't edit the template or import a new one"
		until get_yorn "Do you want to try again?" "y" show_no_change_ok_help ; do continue; done
		if [ $yorn == "no" ]; then
		    try=0
		fi
	    fi
	fi
    done
}

function show_mtgarchive_maintenance_help
{
    cat<<EOF
Once daily, a maintenance process is run that checks to see whether
the meeting archive repository has exceeded a maximum size.  If it
has, you can either have a notification e-mail sent to the system
administators or you can have the oldest archives deleted provided
they are not younger than a configurable limit.
EOF
}

function show_logging_help
{
    cat<<EOF
There are three levels for logging.  From least to most verbose they
are "error", "info" and "debug".  The "error" level only logs error
conditions.  The "info" level logs error conditions and summary info
about what a server is doing.  The "debug" level logs error
conditions, summary info and detailed debugging info.

NOTE: We recommend that the 'error' logging level be the default
    logging level due to the increased CPU and I/O loads associated
    with more verbose logging.

Log files, in general, are written to /var/log/iic except for the web
portal and external API server logs which are written to files in
/var/log/httpd.  Occasionally, some useful information may be
found in /var/log/messages.

NOTE: You may change the logging level on a per-component basis in the
administrator's console while the system up.
EOF
}

function show_port_forwarding_help
{
    cat<<EOF
Conferencing clients will attempt to connect to ports 1270, 443 and 21 when the
primary port (either 5222 for the XML router or 2182 for
desktop/app-share server) are blocked by a firewall.  Connections to
these ports should be forwarded to the primary port.  If you are not
running a load balancer that performs this task, you must forward
these ports locally on the XML router host(s) or the desktop/app-share
server host(s).
EOF
}


function show_security_help
{
    cat<<EOF
In order for a service (e.g. meeting controller, voice bridge, etc.)
to connect to the XML router (see the Conferencing Installation Manual
for a more detailed description of the system architecture), the
service must use the correct authentication key.  If your XML router
allows connections from the internet at large, you should change this
key so that unauthorized services cannot connect to your XML router.

In order for the meeting controller to create user sessions (e.g. for
phone calls), it needs to provide an authentication key.  You should
change this key so that unauthorized meeting controllers can not
create sessions on behalf of your users.
EOF
}

function show_ldap_sync_help
{
    cat<<EOF
The Conferencing server can connect to one or more LDAP servers as a
source of information about users.  To enable LDAP synchronization,
you will need to provide the URL of the LDAP server(s), and for each
server, the distinguished name (DN) and password of an LDAP account
with which the Conferencing server can bind to the server (the "bind
DN"); the DN of the LDAP container in which the user information is
contained (the "base DN"); the LDAP filter used to select user entries
within that container; and the Conferencing community into which to
place users imported from LDAP.
EOF
}

function show_ldap_schedule_help
{
    cat<<EOF
Synchronizing Conferencing with your LDAP server(s) can be scheduled
to happen once a day, at an hour you choose.  The system uses a 24 hour
clock.  Valid hour values are 0 (i.e. 12 midnight) through 23 (i.e. 11 pm)
inclusive.  For example, 3 is 3 am and 15 is 3 pm.

If you choose not to synchronize automatically, you must run the
synchronization program manually to update Conferencing.
EOF
}

function show_locale_help
{
    cat<<EOF
This setting selects which language will be used when generating
e-mail to notify users of certain events.  Possibilities are:
EOF
for lang in $LANGS
do
    echo -e "\t$lang"
done
}

function show_prodname_help
{
    cat<<EOF
This setting accepts the application name which will be used in
the automatically generated e-mails sent to notify users of
certain events.
EOF
}

function show_email_header_help
{
    cat<<EOF
The system will automatically generate e-mail to notify users of
certain events.  You can chose to edit the 'Subject:' and, in some
cases, the From: header fields for these e-mails.
EOF
}

function show_new_user_help
{
    cat<<EOF
The new user e-mail template is used to create the body of the e-mail
sent to users when they are created.  For more information about this
template, see the Conferencing Installation Manual.
EOF
}

function show_invitation_email_help
{
    cat<<EOF
The meeting invitation template is used to create the body of the
e-mail sent to invited participants of a meeting.  The Conferencing
Installation Manual has a section explaining the syntax of an meeting
invitation template.  For more information about this template, see
the Conferencing Installation Manual.
EOF
}

function show_meeting_summary_email_help
{
    cat<<EOF
When a scheduled meeting ends or a recorded instant meeting ends, a
meeting summary is sent to the host of the meeting.  The meeting
summary e-mail template is used to create the body of this message.
For more information about this template, see the Conferencing
Installation Manual.
EOF
}

function show_new_mtgarchive_email_help
{
    cat<<EOF
When a meeting archive is created for an ended meeting, an e-mail is
sent to the host of the meeting.  This template is used to create the
body of this e-mail.  For more information about this template, see
the Conferencing Installation Manual.
EOF
}

function summarize_template
{
    local title=$1
    local template=$2
    local uscore="$( echo $title | sed -e 's+.+-+g' )"
    
    echo $title
    echo $uscore

    if [ -e $template ]; then
	cat $template
    else
	echo "None found; will use default."
    fi
}

function summarize_optional_config
{
    summarize_sysadm_email
    summarize_smtp_config
    summarize_database_backup_config
    pause_for_enter
    summarize_meeting_archive_maintenance_config
    summarize_doc_share_repository_maintenance_config
    summarize_chat_log_maintenance_config
    summarize_component_logging_config
    summarize_security_config
    summarize_ldap_sync_config
    summarize_rt_logs
    pause_for_enter
    summarize_locale_config
    summarize_prodname_config
    summarize_system_email_config
    pause_for_enter

    if [ "$show_new_user_template" == "yes" ]; then
        if [ -e $cluster/locale/$locale_setting/emails/new-user-template.txt ]; then
        	summarize_template "NEW USER E-MAIL BODY TEMPLATE" $cluster/locale/$locale_setting/emails/new-user-template.txt
        else
        	summarize_template "NEW USER E-MAIL BODY TEMPLATE" $cluster/locale/$locale_setting/emails/new-user-template.default
        fi
    	pause_for_enter
    fi

    if [ "$show_invitation_template" == "yes" ]; then
        if [ -e $cluster/locale/$locale_setting/emails/invitation-template.txt ]; then
        	summarize_template "MEETING INVITATION E-MAIL BODY TEMPLATE" $cluster/locale/$locale_setting/emails/invitation-template.txt
        else
        	summarize_template "MEETING INVITATION E-MAIL BODY TEMPLATE" $cluster/locale/$locale_setting/emails/invitation-template.default
        fi
    	pause_for_enter
    fi

    if [ "$show_meeting_summary_template" == "yes" ]; then
        if [ -e $cluster/locale/$locale_setting/emails/meeting-summary-template.txt ]; then
        	summarize_template "MEETING SUMMARY E-MAIL BODY TEMPLATE" $cluster/locale/$locale_setting/emails/meeting-summary-template.txt
        else
        	summarize_template "MEETING SUMMARY E-MAIL BODY TEMPLATE" $cluster/locale/$locale_setting/emails/meeting-summary-template.default
        fi
    	pause_for_enter
    fi

    if [ "$show_new_mtgarchive_template" == "yes" ]; then
        if [ -e $cluster/locale/$locale_setting/emails/new-mtgarchive-template.txt ]; then
        	summarize_template "NEW MEETING ARCHIVE E-MAIL BODY TEMPLATE" $cluster/locale/$locale_setting/emails/new-mtgarchive-template.txt
        else
        	summarize_template "NEW MEETING ARCHIVE E-MAIL BODY TEMPLATE" $cluster/locale/$locale_setting/emails/new-mtgarchive-template.default
        fi
    fi
}

function show_rt_logs_help
{
    cat <<EOF
The system can provide real-time information to third-party components
when users, calls and meeting reservations change.  Records are sent
over a TCP/IP connection to third-party components when user, call or
meeting reservation events occur.  For instance, when a participant is
added or removed from a scheduled meeting or a host starts/stops
recording a meeting.

Details about the records that are written to the real-time interfaces
can be found in the Conferencing Operations Guide.
EOF
}

function summarize_rt_logs
{
    echo "REAL-TIME EVENT LOGS"
    echo "--------------------"
    if [ "$enable_rt_logs" == "no" ]; then
	echo -e "\tNot in use"
    echo

	return 0;
    fi

    echo -e "\tTo access the real-time logs, connect to the following hosts/ports:"
    if [ "${addressbk_host[0]}" == "localhost" ]; then
	if [ ! -z "$single_host" ]; then
	    dpy_host=$single_host
	else
	    dpy_host=$this_host
	fi
    else
	dpy_host=${addressbk_host[0]}
    fi
    
    echo -e "\tUser records.........: $dpy_host:$rt_user_port"
    echo -e "\tReservation records..: $dpy_host:$rt_rsrv_port"

    if [ "${controller_host[0]}" == "localhost" ]; then
	if [ ! -z "$single_host" ]; then
	    dpy_host=$single_host
	else
	    dpy_host=$this_host
	fi
    else
	dpy_host=${controller_host[0]}
    fi
    echo -e "\tCall records.........: $dpy_host:$rt_call_port"
    
    if [ ${#controller_host[*]} -gt 1 ]; then
	if [ "${controller_host[1]}" == "localhost" ]; then
	    if [ ! -z "$single_host" ]; then
		dpy_host=$single_host
	    else
		dpy_host=$this_host
	    fi
	else
	    dpy_host=${controller_host[1]}
	fi
	echo -e "\tCall records.........: $dpy_host:$rt_call_port"
    fi

    echo
}

function update_optional_config
{
    allocate_jabber_service_ports

    echo
    summarize_sysadm_email
    until get_yorn "Do you want to change the system administrator e-mail configuration?" "n" show_sysadmin_email_help ; do continue; done
    if [ "$yorn" == "yes" ]; then
	update_sysadm_email
    fi

    summarize_smtp_config
    until get_yorn "Do you want to change the mailer configuration?" "n" show_mailer_help ; do continue; done
    if [ "$yorn" == "yes" ]; then
	update_smtp_config
    fi

    summarize_database_backup_config
    until get_yorn "Do you want to change the database backup configuration?" "n" show_database_backup_help ; do continue; done
    if [ "$yorn" == "yes" ]; then
	update_database_backup_config
    fi

    summarize_meeting_archive_maintenance_config
    until get_yorn "Do you want to change the meeting archive maintenance configuration?" "n" show_mtgarchive_maintenance_help ; do continue; done
    if [ "$yorn" == "yes" ];  then
	update_meeting_archive_maintenance_config
    fi

    summarize_doc_share_repository_maintenance_config
    until get_yorn "Do you want to change the doc share repository maintenance configuration?" "n" show_doc_share_maintenance_help ; do continue; done
    if [ "$yorn" == "yes" ];  then
	update_doc_share_repository_maintenance_config
    fi

    summarize_chat_log_maintenance_config
    until get_yorn "Do you want to change the chat audit log configuration?" "n" show_log_chat_maintenance_help ; do continue; done
    if [ "$yorn" == "yes" ];  then
	update_chat_log_maintenance_config
    fi

    summarize_component_logging_config
    until get_yorn "Do you want to change the logging configuration?" "n" show_logging_help ; do continue; done
    if [ "$yorn" == "yes" ]; then
	update_component_logging_config
    fi

    local rt_default
    [ -z "$enable_rt_logs" ] && enable_rt_logs=no
    [ "$enable_rt_logs" == "yes" ] && rt_default="y" || rt_default="n"
    summarize_rt_logs
    until get_yorn "Do you want to use the real-time call, user and reservation logs?" "$rt_default" show_rt_logs_help ; do continue; done
    enable_rt_logs=$yorn
    if [ "$yorn" == "yes" ]; then
	allocate_rt_log_ports
    else
	clear_rt_log_ports
    fi
    save_config

    summarize_security_config
    until get_yorn "Do you want to change the security configuration?" "n" show_security_help ; do continue; done
    if [ "$yorn" == "yes" ]; then
	update_security_config
    fi

    summarize_ldap_sync_config
    until get_yorn "Do you want to change the LDAP configuration?" "n" show_ldap_sync_help ; do continue; done
    if [ "$yorn" == "yes" ]; then
	update_ldap_sync_config
    fi

    local rt_default
    [ -z "$enable_rt_logs" ] && enable_rt_logs=no

    summarize_locale_config
    until get_yorn "Do you want to change the current locale setting?" "n" show_locale_help ; do continue; done
    if [ "$yorn" == "yes" ]; then
	update_locale_config
    fi

    summarize_prodname_config
    until get_yorn "Do you want to change the display name for the application?" "n" show_prodname_help ; do continue; done
    if [ "$yorn" == "yes" ]; then
	update_prodname_config
    fi

    summarize_system_email_config
    until get_yorn "Do you want to change any of the system e-mail header configuration?" "n" show_email_header_help ; do continue; done
    if [ "$yorn" == "yes" ]; then
	update_system_email_config
    fi

    if [ -e $cluster/locale/$locale_setting/emails/new-user-template.txt ]; then
        until get_yorn "The new user e-mail template was modified during a prior install.  Do you want to view it now?" "n" show_new_user_help ; do continue; done
        if [ "$yorn" == "yes" ]; then
            summarize_template "Previously modified NEW USER E-MAIL BODY TEMPLATE" $cluster/locale/$locale_setting/emails/new-user-template.txt
        fi
        until get_yorn "Do you want to keep the new user e-mail template that was modified during the previous install?" "y" show_new_user_help ; do continue; done
        if [ "$yorn" == "no" ]; then
            rm -f $cluster/locale/$locale_setting/emails/new-user-template.txt
        else
            until get_yorn "Do you want to change the new user e-mail template from the previous install?" "n" show_new_user_help ; do continue; done
            if [ "$yorn" == "yes" ]; then
                update_template $cluster/locale/$locale_setting/emails/new-user-template.txt
                show_new_user_template="yes"
            else
            	show_new_user_template="no"
            fi
        fi
    fi
    if [ ! -e $cluster/locale/$locale_setting/emails/new-user-template.txt ]; then
        until get_yorn "Do you want to change the new user e-mail template?" "n" show_new_user_help ; do continue; done
        if [ "$yorn" == "yes" ]; then
            cp $cluster/locale/$locale_setting/emails/new-user-template.default $cluster/locale/$locale_setting/emails/new-user-template.txt
            update_template $cluster/locale/$locale_setting/emails/new-user-template.txt
            show_new_user_template="yes"
        else
        	show_new_user_template="no"
        fi
    fi

    if [ -e $cluster/locale/$locale_setting/emails/invitation-template.txt ]; then
        until get_yorn "The meeting invitation template was modified during a prior install.  Do you want to view it now?" "n" show_invitation_email_help ; do continue; done
        if [ "$yorn" == "yes" ]; then
            summarize_template "Previously modified meeting invitation template" $cluster/locale/$locale_setting/emails/invitation-template.txt
        fi
        until get_yorn "Do you want to keep the meeting invitation template that was modified during the previous install?" "y" show_invitation_email_help ; do continue; done
        if [ "$yorn" == "no" ]; then
            rm -f $cluster/locale/$locale_setting/emails/invitation-template.txt
        else
            until get_yorn "Do you want to change the meeting invitation template from the previous install?" "n" show_invitation_email_help ; do continue; done
            if [ "$yorn" == "yes" ]; then
                update_template $cluster/locale/$locale_setting/emails/invitation-template.txt
                show_invitation_template="yes"
            else
            	show_invitation_template="no"
            fi
        fi
    fi
    until get_yorn "Do you want to change the meeting invitation template?" "n" show_invitation_email_help ; do continue; done
    if [ "$yorn" == "yes" ]; then
        cp $cluster/locale/$locale_setting/emails/invitation-template.default $cluster/locale/$locale_setting/emails/invitation-template.txt
    	update_template $cluster/locale/$locale_setting/emails/invitation-template.txt
    	show_invitation_template="yes"
    else
    	show_invitation_template="no"
    fi

    if [ -e $cluster/locale/$locale_setting/emails/meeting-summary-template.txt ]; then
        until get_yorn "The meeting summary e-mail template was modified during a prior install.  Do you want to view it now?" "n" show_meeting_summary_email_help ; do continue; done
        if [ "$yorn" == "yes" ]; then
            summarize_template "Previously modified meeting summary e-mail template" $cluster/locale/$locale_setting/emails/meeting-summary-template.txt
        fi
        until get_yorn "Do you want to keep the meeting summary e-mail template that was modified during the previous install?" "y" show_meeting_summary_email_help ; do continue; done
        if [ "$yorn" == "no" ]; then
            rm -f $cluster/locale/$locale_setting/emails/meeting-summary-template.txt
        else
            until get_yorn "Do you want to change the meeting summary e-mail template from the previous install?" "n" show_meeting_summary_email_help ; do continue; done
            if [ "$yorn" == "yes" ]; then
                update_template $cluster/locale/$locale_setting/emails/meeting-summary-template.txt
                show_meeting_summary_template="yes"
            else
            	show_meeting_summary_template="no"
            fi
        fi
    fi
    until get_yorn "Do you want to change the meeting summary e-mail template?" "n" show_meeting_summary_email_help ; do continue; done
    if [ "$yorn" == "yes" ]; then
        cp $cluster/locale/$locale_setting/emails/meeting-summary-template.default $cluster/locale/$locale_setting/emails/meeting-summary-template.txt
    	update_template $cluster/locale/$locale_setting/emails/meeting-summary-template.txt
    	show_meeting_summary_template="yes"
    else
    	show_meeting_summary_template="no"
    fi

    if [ -e $cluster/locale/$locale_setting/emails/new-mtgarchive-template.txt ]; then
        until get_yorn "The new meeting archive e-mail template was modified during a prior install.  Do you want to view it now?" "n" show_new_mtgarchive_email_help ; do continue; done
        if [ "$yorn" == "yes" ]; then
            summarize_template "Previously modified new meeting archive e-mail template" $cluster/locale/$locale_setting/emails/new-mtgarchive-template.txt
        fi
        until get_yorn "Do you want to keep the new meeting archive e-mail template that was modified during the previous install?" "y" show_new_mtgarchive_email_help ; do continue; done
        if [ "$yorn" == "no" ]; then
            rm -f $cluster/locale/$locale_setting/emails/new-mtgarchive-template.txt
        else
            until get_yorn "Do you want to change the new meeting archive e-mail template from the previous install?" "n" show_new_mtgarchive_email_help ; do continue; done
            if [ "$yorn" == "yes" ]; then
                update_template $cluster/locale/$locale_setting/emails/new-mtgarchive-template.txt
                show_new_mtgarchive_template="yes"
            else
            	show_new_mtgarchive_template="no"
            fi
        fi
    fi
    until get_yorn "Do you want to change the new meeting archive e-mail template?" "n" show_new_mtgarchive_email_help ; do continue; done
    if [ "$yorn" == "yes" ]; then
        cp $cluster/locale/$locale_setting/emails/new-mtgarchive-template.default $cluster/locale/$locale_setting/emails/new-mtgarchive-template.txt
    	update_template $cluster/locale/$locale_setting/emails/new-mtgarchive-template.txt
    	show_new_mtgarchive_template="yes"
    else
    	show_new_mtgarchive_template="no"
    fi
}

function summarize_locale_config
{
    echo "SYSTEM LOCALE SETTINGS"
    echo "----------------------"
    echo -e "\tCURRENT LOCALE SETTING"
    echo -e "\t\t$locale_setting"
    echo
}

function summarize_prodname_config
{
    echo "APPLICATION DISPLAY NAME"
    echo "------------------------"
    echo -e "\tCURRENT APPLICATION DISPLAY NAME"
    echo -e "\t\t$iic_prodname"
    echo
}

function summarize_system_email_config
{
    echo "SYSTEM E-MAIL HEADER CONFIGURATION"
    echo "----------------------------------"
    echo -e "\tNEW USER CREATION E-MAIL"
    echo -e "\t\tFrom: \"$new_user_from_display <$new_user_from_address>\""
    echo -e "\t\tSubject: $new_user_subject"
    echo -e "\tMEETING INVITATION E-MAIL"
    echo -e "\t\tSubject: $invite_subject"
    echo -e "\tNEW MEETING ARCHIVE E-MAIL"
    echo -e "\t\tFrom: \"$mtgarchiver_from_display <$mtgarchiver_from_address>\""
    echo -e "\t\tSubject: $mtgarchiver_subject <meeting title>"
    echo
}

function show_new_user_email_help
{
    cat<<EOF
When a new user is added to the system an e-mail is generated and sent
to that user.

EOF
}

function show_new_user_display_help
{
    show_new_user_email_help
    cat<<EOF
This parameter controls the display name in the From: header field.  For
example, "John Smith" is the display name in the following e-mail
address "John Smith <jsmith@some.com>".
EOF
}

function show_new_user_addr_help
{
    show_new_user_email_help
    cat<<EOF
This parameter controls the e-mail address in the From: header field.  For
example, "jsmith@some.com" is the e-mail address in the following
expanded e-mail address "John Smith <jsmith@some.com>".
EOF
}

function show_new_user_subject_help
{
    show_new_user_email_help
    cat<<EOF
This parameter specifies the text for the 'Subject:' header field for these
e-mails.
EOF
}

function show_meeting_invitation_subject_help
{
    cat<<EOF
When a Conferencing meeting is either scheduled or started, the system can
send meeting invitations via e-mail to the participants.  This parameter
controls the 'Subject:' header field of those e-mails.
EOF
}

function show_new_mtgarchive_email_help
{
    cat<<EOF
When a new meeting archive is produced by the system, an e-mail can be
generated and sent to the host of that meeting.

EOF
}

function show_new_mtgarchive_display_help
{
    show_new_mtgarchive_email_help
    cat<<EOF
This parameter controls the display name in the From: header field.  For
example, "John Smith" is the display name in the following e-mail
address "John Smith <jsmith@some.com>".
EOF
}

function show_new_mtgarchive_addr_help
{
    show_new_mtgarchive_email_help
    cat<<EOF
This parameter controls the e-mail address in the From: header field.  For
example, "jsmith@some.com" is the e-mail address in the following
expanded e-mail address "John Smith <jsmith@some.com>".
EOF
}

function show_new_mtgarchive_subject_help
{
    show_new_mtgarchive_email_help
    cat<<EOF
This parameter specifies the text for the Subject: header field for these
e-mails.
EOF
}

function update_locale_config
{
    ask_for_locale="yes"
    while [ "$ask_for_locale" == "yes" ]; do
        until get_strval "New locale value?" "$locale_setting" show_locale_help ; do continue; done
        for lang in $LANGS
        do
            if [ $lang == $strval ]; then
                ask_for_locale="no"
            fi
        done
        if [ "$ask_for_locale" == "yes" ]; then
            echo
            echo -e "\tUnsupported locale.  Please enter a valid locale."
            echo
        else
            locale_setting=$strval
        fi
    done
    save_config
}

function update_prodname_config
{
    until get_strval "New display name for application?" "$iic_prodname" show_prodname_help ; do continue; done
    iic_prodname=$strval
    save_config
}

function update_system_email_config
{
    until get_strval "New user e-mail 'From:' display name?" "$new_user_from_display" show_new_user_display_help ; do continue; done
    new_user_from_display=$strval
    save_config

    until get_strval "New user e-mail 'From:' e-mail address?" "$new_user_from_address" show_new_user_addr_help ; do continue; done
    new_user_from_address=$strval
    save_config

    until get_strval "New user e-mail 'Subject:'?" "$new_user_subject" show_new_user_subject_help ; do continue; done
    new_user_subject=$strval
    save_config

    until get_strval "Meeting invitation subject 'Subject:'?" "$invite_subject"  show_meeting_invitation_subject_help ; do continue; done
    invite_subject=$strval
    save_config

    until get_strval "New meeting archive e-mail 'From:' display name?" "$mtgarchiver_from_display" show_new_mtgarchive_display_help ; do continue; done
    mtgarchiver_from_display=$strval
    save_config

    until get_strval "New meeting archive e-mail 'From:' e-mail address?" "$mtgarchiver_from_address" show_new_mtgarchive_addr_help ; do continue; done
    mtgarchiver_from_address=$strval
    save_config

    until get_strval "New meeting archive e-mail 'Subject:'?" "$mtgarchiver_subject" show_new_mtgarchive_subject_help ; do continue; done
    mtgarchiver_subject=$strval
    save_config
}

function summarize_smtp_config
{
    echo "MAILER CONFIGURATION"
    echo "--------------------"
    echo -e "\tThe Conferencing mailer will:"
    if [ "$smtp_host" == "localhost" -o -z "$smtp_host" ]; then
	echo -e "\t\t- Send mail using the mail server running on its host"
    else
	echo -e "\t\t- Send mail using the mail server running on $smtp_host"
    fi
    if [ -z "$smtp_user" ]; then
	echo -e "\t\t- Not use any authentication to the mail server"
    else
	echo -e "\t\t- Will authenticate to the mail server using user \"$smtp_user\" and password \"$smtp_pswd\""
    fi
    echo
}

function show_smtp_server_help
{
    echo "Currently the Conferencing mailer will make connections to an SMTP"
    echo "server on $smtp_host.  If you want the Conferencing mailer to use"
    echo "a different host for SMTP service, enter the host here."
    echo ""
}

function show_smtp_auth_help
{
    cat<<EOF
Some SMTP servers require user/password authentication before they
will accept requests.  If your SMTP server requires authentication
enter the user and passwords by choosing 'y'
EOF
}

function update_smtp_config
{
    get_valid_address "What SMTP server will the Conferencing Mailer use?" "$smtp_host" "hname" show_smtp_server_help
    smtp_host=$valid_address
    save_config

    until get_yorn "Use user and password authentication for $smtp_host?" "n" show_smtp_auth_help ; do continue; done
    if [ "$yorn" == "yes" ]; then
	until get_strval "What user should be used for authentication?" "$smtp_user" ; do continue; done
	smtp_user=$strval
	save_config

	until get_strval "What password should be used for authentication?" "$smtp_pswd" ; do continue; done
	smtp_pswd=$strval
	save_config
    else
	smtp_user=""
	smtp_pswd=""
    fi
}

function summarize_database_backup_config
{
    echo "DATABASE BACKUPS"
    echo "----------------"
    echo -e "\t- Backups will be stored in the following directory: $db_backup_dir"
    echo -e "\t\tNOTE:"
    echo -e "\t\t\tWe do not recommend that the database"
    echo -e "\t\t\tbackup directory physically reside on the database"
    echo -e "\t\t\thost in case the database host experiences"
    echo -e "\t\t\ta catastrophic hardware failure.  The backup directory"
    echo -e "\t\t\tshould be an NFS filesystem so that backups can be"
    echo -e "\t\t\tsaved externally."
    if [ $db_backup_min -lt 10 ]; then
	db_backup_min_dpy="0$db_backup_min"
    else
	db_backup_min_dpy=$db_backup_min
    fi
    echo -e "\t- A full backup will be performed daily at $db_backup_hr:$db_backup_min_dpy (24-hr clock)"
    echo -e "\t- Backups will be kept for $db_backup_history days before being deleted"
    echo
}

function show_db_backup_dir_help
{
    cat<<EOF
This directory will contain the database backup archives.  It must be
writeable by the database backup script running under the 'zon' user.
Also, we recommend that the directory be a mounted external
filesystem in case the database host suffers a catastrophic hardware
failure.
EOF
}

function show_db_backup_history_help
{
    cat<<EOF
The contents of the Conferencing database are archived daily for crash recovery
purposes.  This parameter controls the number of days of arhives to
keep.
EOF
}

function update_database_backup_config
{
    until get_strval "What directory will contain database backups?" "$db_backup_dir" show_db_backup_dir_help ; do continue; done
    db_backup_dir=$strval
    save_config

    until get_strval "How many days of backup history are to be kept?" "$db_backup_history" show_db_backup_history_help ; do continue; done
    db_backup_history=$strval
    save_config

    local valid=0
    while [ $valid -eq 0 ]; do
	until get_strval "At what hour of the day [0-23] will backups be performed?" "$db_backup_hr" show_hour_help ; do continue; done
	if [ $strval -ge 0 -a $strval -lt 24 ]; then
	    db_backup_hr=$strval
	    save_config
	    valid=1
	else
	    echo "!!!!! Valid values are 0 through 23"
	fi
    done

    db_backup_min=0
}

function summarize_meeting_archive_maintenance_config
{
    echo "MEETING ARCHIVE MAINTENANCE"
    echo "---------------------------"
    echo -e "\t- When the meeting archives repository size exceeds $mtgarchive_cleaning_threshold Mb,"
    if [ "$mtgarchive_cleanup_action" == "clean" ]; then
	echo -e "\t\tthe oldest archives will be deleted until the repository size falls below $mtgarchive_target_sz Mb."
	if [ ! -z "$mtgarchive_trash_folder" ]; then
	    echo -e "\t\tDeleted files will be put in the following trash folder: $mtgarchive_trash_folder"
	fi
	echo -e "\t\tNOTE:"
	echo -e "\t\t\t- Archives accessed in the past $mtgarchive_cleanup_min_age days old are never deleted."
	echo -e "\t\t\t- If no more archives can be deleted and the repository size *still* exceeds $mtgarchive_target_sz Mb,"
	echo -e "\t\t\t  a warning e-mail is sent to the sys admins"
    fi
    if [ "$mtgarchive_cleanup_action" == "email" ]; then
	echo -e "\t\ta warning e-mail will be sent daily to the sys admins"
    fi
    echo
}

function summarize_doc_share_repository_maintenance_config
{
    echo "DOCUMENT SHARING REPOSITORY MAINTENANCE"
    echo "---------------------------------------"
    echo -e "\t- When the document sharing repository size exceeds $docshare_cleaning_threshold Mb,"
    if [ "$docshare_cleanup_action" == "clean" ]; then
	echo -e "\t\tthe documents with the oldest access time will be deleted until the repository size falls below $docshare_target_sz Mb."
	if [ ! -z "$docshare_trash_folder" ]; then
	    echo -e "\t\tDeleted files will be put in the following trash folder: $docshare_trash_folder"
	fi
	echo -e "\t\tNOTE:"
	echo -e "\t\t\t- Documents accessed in the past $docshare_cleanup_min_age days old are never deleted."
	echo -e "\t\t\t- If no more archives can be deleted and the repository size *still* exceeds $docshare_target_sz Mb,"
	echo -e "\t\t\t  a warning e-mail is sent to the sys admins"
    fi
    if [ "$docshare_cleanup_action" == "email" ]; then
	echo -e "\t\ta warning e-mail will be sent daily to the sys admins"
    fi
    echo
}

function summarize_chat_log_maintenance_config
{
    echo "CHAT AUDIT LOG MAINTENANCE"
    echo "--------------------------"
    if [ "$log_chat" == "no" ]; then
	echo -e "\tNo chat audit logs will be produced."
    else
	echo -e "\t- When chat audit logs exceed $log_chat_clean_sz Mb,"
	if [ "$log_chat_clean_action" == "clean" ]; then
	    echo -n -e "\t\tthe logs with the oldest creation time will be "
	    if [ ! -z "$log_chat_trash_folder" ]; then
		echo "moved to $log_chat_trash_folder"
	    else
		echo "deleted"
	    fi
	elif [ "$log_chat_cleanup_action" == "email" ]; then
	    echo -e "\t\ta warning e-mail will be sent daily to the sys admins"
	fi
    fi
	echo
}

function show_log_chat_cleanup_action_help
{
    cat<<EOF
If the audit logs exceed a configurable maximum size, the system can
delete the old audit logs (optionally moving them to a 'trash'
folder).  Alternatively, it can e-mail the system administrators a
warning.
EOF
}

function show_deleted_help
{
    cat <<EOF
If the repository exceeds a configurable maximum size, the system can delete
the old/inactive items (optionally moving them to a 'trash' folder) or just
e-mail the system administrators a warning.
EOF
}

function show_trash_folder_help
{
    cat<<EOF
You can define a folder that contains the deleted items.
If you define a trash folder, the system will move old items to
the trash folder rather than deleting them.  This trash folder is not
managed by the system, so you will have to provide a maintenance
procedure to keep this folder from exceeding available space.
EOF
}

function show_cleaning_threshold_help
{
    cat<<EOF
Once a respository reaches the cleaning threshold, the configured
maintenance action will be performed.  The maintenance action can
be either to delete old/inactive items or to send an e-mail to the
system administrators informing them of the 
EOF
}

function show_target_sz_help
{
    cat<<EOF
This parameter specifies the size of the repository after the inactive
items are deleted.  This allows the archives to be cleaned
efficiently (i.e. the archives from oldest to youngest need not be
computed frequently).
EOF
}

function show_cleanup_hr_help
{
    cat<<EOF
This parameter defines the hour of the day that repository maintenance
is performed.  While not a terribly resource intensive process, all
things being equal it should be done at a time of low system activity.
The hour is in the local time zone of the servers.
EOF
}

function show_cleanup_min_age_help
{
    cat<<EOF
You may want to provide a guarantee to users that their repository
contents will not be cleaned up if accessed within a given period
of time.  This parameter controls the guaranteed minimum number of
days that an item will remain in the repository.

NOTE: Setting this parameter to a value greater than zero means that
it may not be possible to clean up the repository fully.  An e-mail is
sent to system administrators when this is the case.
EOF
}

function update_meeting_archive_maintenance_config
{
    until get_yorn "Should archives be deleted when the meeting archiver repository size exceeds a maximum? (n = email sys admins)" "y" show_deleted_help ; do continue; done
    if [ "$yorn" == "yes" ]; then
	mtgarchive_cleanup_action="clean"
    else
	mtgarchive_cleanup_action="email"
    fi
    save_config
    
    local action=""
    case $mtgarchive_cleanup_action in
	clean)
	    action="Deleting old archives begins"
	    ;;
	email)
	    action="A warning e-mail is sent"
	    ;;
    esac
    
    until get_strval "$action when repository size exceeds what size (in Mb)?" "$mtgarchive_cleaning_threshold" show_cleaning_threshold_help ; do continue; done
    mtgarchive_cleaning_threshold=$strval
    save_config
    
    if [ "$mtgarchive_cleanup_action" == "clean" ]; then
	until get_strval "Deleting old archives ends when repository size falls below what size (in Mb)?" "$mtgarchive_target_sz" show_target_sz_help ; do continue; done
	mtgarchive_target_sz=$strval
	save_config
	
	until get_strval "Do not delete achives younger than how many days?" "$mtgarchive_cleanup_min_age" show_cleanup_min_age_help ; do continue; done
	mtgarchive_cleanup_min_age=$strval
	save_config

	until get_yorn "Should deleted meeting archives be moved to a trash folder?" "n" show_trash_folder_help ; do continue; done
	if [ "$yorn" == "yes" ]; then
	    until get_strval "What is the trash folder?" "$mtgarchive_trash_folder" show_trash_folder_help ; do continue; done
	    mtgarchive_trash_folder="$strval"
	else
	    mtgarchive_trash_folder=""
	fi
	save_config
    fi

    valid=0
    while [ $valid -eq 0 ]; do
	until get_strval "At what hour of the day (0-23) should the maintenance begin?" "$mtgarchive_cleanup_hr" show_cleanup_hr_help ; do continue; done
	if [ $strval -ge 0 -a $strval -lt 24 ]; then
	    mtgarchive_cleanup_hr=$strval
	    mtgarchive_cleanup_min=0
	    save_config
	    valid=1
	else
	    echo "!!!!! Valid values are 0 through 23"
	fi
    done
}

function update_doc_share_repository_maintenance_config
{
    until get_yorn "Should documents be deleted when the doc share repository size exceeds a maximum? (n = email sys admins)" "y" show_deleted_help ; do continue; done
    if [ "$yorn" == "yes" ]; then
	docshare_cleanup_action="clean"
    else
	docshare_cleanup_action="email"
    fi
    save_config
    
    local action=""
    case $docshare_cleanup_action in
	clean)
	    action="Deleting inactive documents begins"
	    ;;
	email)
	    action="A warning e-mail is sent"
	    ;;
    esac
    
    until get_strval "$action when repository size exceeds what size (in Mb)?" "$docshare_cleaning_threshold" show_cleaning_threshold_help ; do continue; done
    docshare_cleaning_threshold=$strval
    save_config
    
    if [ "$docshare_cleanup_action" == "clean" ]; then
	until get_strval "Deleting inactive documents ends when repository size falls below what size (in Mb)?" "$docshare_target_sz" show_target_sz_help ; do continue; done
	docshare_target_sz=$strval
	save_config
	
	until get_strval "Never delete documents that have been accessed in the past how many days?" "$docshare_cleanup_min_age" show_cleanup_min_age_help ; do continue; done
	docshare_cleanup_min_age=$strval
	save_config

	until get_yorn "Should deleted meeting documents be moved to a trash folder?" "n" show_trash_folder_help ; do continue; done
	if [ "$yorn" == "yes" ]; then
	    until get_strval "What is the trash folder?" "$docshare_trash_folder" show_trash_folder_help ; do continue; done
	    docshare_trash_folder="$strval"
	else
	    docshare_trash_folder=""
	fi
	save_config
    fi

    valid=0
    while [ $valid -eq 0 ]; do
	until get_strval "At what hour of the day (0-23) should the maintenance begin?" "$docshare_cleanup_hr" show_cleanup_hr_help ; do continue; done
	if [ $strval -ge 0 -a $strval -lt 24 ]; then
	    docshare_cleanup_hr=$strval
	    docshare_cleanup_min=0
	    save_config
	    valid=1
	else
	    echo "!!!!! Valid values are 0 through 23"
	fi
    done
}

function update_chat_log_maintenance_config
{
    until get_yorn "Should the system keep audit logs of Conferencing chat activity?" $log_chat show_log_chat_maintenance_help ; do continue; done
    log_chat=$yorn
    save_config

    if [ "$log_chat" == "yes" ]; then
	until get_yorn "When the audit log total size exceeds a limit, should the system delete the oldest daily logs? (n = email sys admins)" "y" show_log_chat_cleanup_action_help ; do continue; done
	if [ "$yorn" == "yes" ]; then
	    log_chat_clean_action="clean"
	else
	    log_chat_clean_action="email"
	fi
	save_config
	
	local action=""
	case $log_chat_clean_action in
	    clean)
		action="Deleting oldest log begins"
		;;
	    email)
		action="A warning e-mail is sent"
		;;
	esac
    
	until get_strval "$action when log total size exceeds what size (in Mb)?" "$log_chat_clean_sz" show_log_chat_clean_sz_help ; do continue; done
	log_chat_clean_sz=$strval
	save_config
	
	if [ "$log_chat_clean_action" == "clean" ]; then
	    until get_yorn "Should deleted chat logs be moved to a trash folder?" "n" show_trash_folder_help ; do continue; done
	    if [ "$yorn" == "yes" ]; then
		until get_strval "What is the trash folder?" "$log_chat_trash_folder" show_trash_folder_help ; do continue; done
		log_chat_trash_folder="$strval"
	    else
		log_chat_trash_folder=""
	    fi
	    save_config
	fi
    fi
}

function summarize_component_logging_config
{
    echo "LOGGING"
    echo "-------"
    echo -e "\t- Default logging level is set to \"$log_level\" (\"error\" recommended)"
    echo -e "\t- Logs will be rotated when they reach $rotation_size bytes long"
    echo -e "\t- $history_len rotated logs will be kept before being deleted"
    echo
}

function show_log_level_help
{
    cat<<EOF
This parameter controls the initial logging level for system
components.

There are three levels for logging.  From least to most verbose they
are "error", "info" and "debug".  The "error" level only logs error
conditions.  The "info" level logs error conditions and summary info
about what a server is doing.  The "debug" level logs error
conditions, summary info and detailed debugging info.

NOTE: You may change the logging level on a per-component basis in the
administrator's console while the system up. We recommend that the
'error' logging level be the default logging level due to the
increased CPU and I/O loads associated with more verbose logging.
EOF
}

function show_rotation_size_help
{
    cat<<EOF
In order too keep an upper bound on log file size, the active log size
is limited to the size specified by this parameter.  When the active
log reaches this size, it is closed and renamed to include a suffix of
".1".  The former log file with an extension of ".1" is renamed to
have an extension of ".2" and so on.
EOF
}


function show_history_len_help
{
    cat<<EOF
In order too keep an upper bound on log file size, The active log for
system components is limited to a maximum size.  When
the active log reaches this size, it is closed and renamed to include a
suffix of ".1".  The former log file with an extension of ".1" is renamed
to have an extension of ".2" and so on.  This parameter controls the
total number of renamed log files that are kept.
EOF
}


function update_component_logging_config
{
    local valid=0
    while [ $valid -eq 0 ]; do
	until get_strval "Default logging level for system components: (error/info/debug)" "$log_level" show_log_level_help ; do continue; done
	case $strval in
	    debug|info|error)
		log_level="$strval"
		save_config
		valid=1
		;;
	    *)
		echo "!!!!! Enter either error, info or debug; $strval is not accepted"
		;;
	esac
    done

    valid=0
    while [ $valid -eq 0 ]; do
	until get_strval "At what size (in bytes) should the logs be rotated?" "$rotation_size" show_rotation_size_help ; do continue; done
	if [ $strval -gt 0 ]; then
	    rotation_size="$strval"
	    valid=1
	    save_config
	else
	    echo "!!!!! Enter a positive integer"
	fi
    done

    valid=0
    while [ $valid -eq 0 ]; do
	until get_strval "What number of rotated logs are kept?" "$history_len"  show_history_len_help ; do continue; done
	if [ $strval -gt 0 ]; then
	    history_len="$strval"
	    valid=1
	    save_config
	else
	    echo "!!!!! Enter a positive integer"
	fi
    done
}

function summarize_security_config
{
    echo "XML ROUTER SECURITY"
    echo "-------------------"
    echo -e "\t- Service connection key: \"$service_secret\""
    echo -e "\t- User sessions creation key: \"$session_creation_secret\""
    echo
}

function update_security_config
{
    
    until get_yorn "Would you like to specify your own secret key for XML router service connections? (n = generate new random key)" "n" show_secret_key_help ; do continue ; done
    if [ "$yorn" = "yes" ]; then
	until get_strval "What secret word should the XML router services use?" "$service_secret" ; do continue; done
	service_secret="$strval"
	save_config
    else
	service_secret=`openssl rand -base64 8 | cut -c -8`
	echo "Generated new service connection secret key: \"$service_secret\""
	save_config
    fi

    until get_yorn "Would you like to specify your own secret key for meeting controller creation of user sessions? (n = generate new random key)" "n" show_secret_key_help ; do continue ; done

    if [ "$yorn" = "yes" ]; then
	until get_strval "What secret word should the meeting controller use to create user sessions?" "$session_creation_secret" ; do continue; done
	session_creation_secret="$strval"
	save_config
    else
	session_creation_secret=`openssl rand -base64 8 | cut -c -8`
	echo "Generated new session creation secret key: \"$session_creation_secret\""
	save_config
    fi
}

function display_ldap_server
{
    explode_ldap_string $1
    echo "URL=$ldap_url, bindDN=$ldap_binddn, bindPW=$ldap_bindpw, baseDN=$ldap_basedn, import filter=$ldap_import_filter, auth filter=$ldap_auth_filter, community=$ldap_community"
}

function summarize_ldap_sync_config
{
    local server

    echo "LDAP SETTINGS"
    echo "----------------"
    if [ "$enable_ldap_sync" == "yes" ]; then
      echo -n -e "\t- LDAP synchronization is enabled"
      if [ ${#ldap_servers[@]} -gt 0 ]; then
	echo " for the following servers:"
	for server in ${ldap_servers[@]}; do
	    echo -n -e "\t\t"
	    display_ldap_server $server
	done
      else
	echo " but no servers are defined!"
      fi
      if [ ! -e 'ldap-ca.cer' ]; then
	  echo -e "\t!! WARNING: ldap-ca.cer file (LDAP server certificate) not found!"
	  echo -e "\t            Without this file, you will be unable to use secure LDAP (ldaps)."
	  echo -e "\t            If you want to use secure LDAP, make sure this file is present"
	  echo -e "\t            before beginning the install process."
      fi
      if [ "$ldap_on_schedule" == "yes" ]; then
	  echo -e "\t - LDAP synchronization will run every day at ${ldap_sync_hour}:00."
      else
	  echo -e "\t - LDAP synchronization must be run manually."
      fi
    else
      echo -e "\t- LDAP synchronization is NOT enabled."
    fi
    echo
}

function edit_ldap_server
{
    local text
    local valid=0

    explode_ldap_string $1

    while [ $valid -eq 0 ]; do
	until get_strval "Input the URL of the LDAP server:" "$ldap_url" show_ldap_sync_help ; do continue; done
	ldap_url=`echo $strval | tr A-Z a-z`

	until get_strval "Input the dn with which to bind to the LDAP server:" "$ldap_binddn" show_ldap_sync_help ; do continue; done
	ldap_binddn=$strval
	
	until get_strval "Input the password for the bind dn:" "$ldap_bindpw" show_ldap_sync_help ; do continue; done
	ldap_bindpw=$strval
	
	until get_strval "Input the base dn under which to search for users:" "$ldap_basedn" show_ldap_sync_help ; do continue; done
	ldap_basedn=$strval

	until get_strval "Input the filter used to select users for import:" "$ldap_import_filter" show_ldap_sync_help ; do continue; done
	ldap_import_filter=$strval

	until get_strval "Input the filter used to lookup users for authentication:" "$ldap_auth_filter" show_ldap_sync_help ; do continue; done
	ldap_auth_filter=$strval

	until get_strval "Input the community into which to place users imported from LDAP:" "$ldap_community" show_ldap_sync_help ; do continue; done
	ldap_community=$strval

	if ! verify_ldap_credentials "$ldap_url" "$ldap_binddn" "$ldap_bindpw" "$ldap_basedn"; then
	    case "$PIPESTATUS" in
		"255" )
                    text="the base DN could not be found in the directory"
		    ;;
		"254" )
                    text="the password given is not correct"
		    ;;
		"253" )
	            text="the bind dn was not found"
		    ;;
		"252" )
                    text="LDAP server did not accept a connection"
		    ;;
		"251" )
		    text="of unanticipated errors"
		    ;;
	    esac

	    echo
	    echo
	    until get_yorn "Can't validate LDAP information because $text.  Continue anyway?" "y" show_ldap_sync_help ; do continue; done
	    if [ "$yorn" = "no" ]; then continue; fi
	fi
	valid=1
    done

    implode_ldap_string
}

function update_ldap_sync_config
{
    until get_yorn "Should the system enable LDAP synchronization?" "$enable_ldap_sync" show_ldap_sync_help ; do continue; done
    enable_ldap_sync=$yorn
    save_config

    if [ "$enable_ldap_sync" == "yes" ]; then
	arrayval=( "${ldap_servers[@]}" )
	edit_list "Enter number of server" "LDAP servers" display_ldap_server edit_ldap_server
	ldap_servers=( "${arrayval[@]}" )
	save_config
	until get_yorn "Should LDAP synchronization be run automatically?" "$ldap_on_schedule" show_ldap_schedule_help; do continue; done
	ldap_on_schedule=$yorn
	if [ "$ldap_on_schedule" == "yes" ]; then
	    until get_number "At what hour of the day (0-23) should LDAP synchronization run?" 0 23 "$ldap_sync_hour"; do continue; done
	    ldap_sync_hour=$numberval
	fi
    fi
}

function summarize_port_forwarding_config
{
    echo "PORT FORWARDING"
    echo "---------------"
    if [ "$iptables_port_forwarding" == "yes" ]; then
	echo -e "\tFallback ports will be forwarded locally on the"
	echo -e "\tXML router and desktop/app-share hosts."
	echo -e "\t\tNOTE:"
	echo -e "\t\t\tOn Conferencing hosts running either an XML router or"
	echo -e "\t\t\tdesktop/app-share server, ports 1270 and 443"
	echo -e "\t\t\tmust be forwarded to the correct service port."
	echo -e "\t\t\tThis is normally done using Linux iptables."
	echo -e "\t\t\tHowever, you may elect to turn this feature off"
	echo -e "\t\t\tand forward the ports using your load balancer instead."
    else
	echo -e "\tFallback ports are *NOT* forwarded locally on the"
	echo -e "\tXML router and desktop/app-share hosts."
	echo -e "\t\tNOTE:"
	echo -e "\t\t\tYou *MUST* have a load balancer capable of forwarding"
	echo -e "\t\t\tports 1270 and 443 to the correct services ports"
	echo -e "\t\t\tfor the XML router and desktop/app-share server"
    fi
    echo
}

function update_port_forwarding_config
{
    until get_yorn "Forward fallback ports using iptables?" "$iptables_port_forwarding" ; do continue; done;
    iptables_port_forwarding=$yorn
    if [ "$yorn" == "no" ]; then
	cat<<EOF
NOTE: Since iptables port forwarding will not be done, you *MUST* have
    a load balancer configured to forward ports 1270 and 443 to the
    correct services ports for the XML routers and desktop/app-share
    servers.

EOF
    fi
    save_config
}

function summarize_sysadm_email
{
    echo "SYSTEM ADMINISTRATOR E-MAILS"
    echo "----------------------------"
    echo -n -e "\t- E-mail about exceptional system events will be sent to "
    if [ ${#sysadm_email[*]} -gt 0 ]; then
	echo "the following addresses:"
	echo -e "\t\t${sysadm_email[*]}"
    else
	echo "NOBODY!"
    fi
    echo
}

function show_email_correct_help
{
cat<<EOF
If the e-mail address information is not correct, you can re-enter
them now by choosing 'n'
EOF
}

function update_sysadm_email
{
    echo "Enter all the system administrator e-mail addresses now."
    echo "When you are finished, hit enter at the 'e-mail:' prompt."
    sysadm_email=()

    local idx=0
    local add=1
    while [ $add -eq 1 ]; do
	echo -n ">>>>> e-mail: "
	read resp
	if [ ! -z "$resp" ]; then
	    echo "Adding $resp"
	    sysadm_email[$idx]="$resp"
	    idx=$(($idx + 1))
	else
	    until get_yorn "Is ${sysadm_email[*]} correct?" "y" show_email_correct_help ; do continue; done
	    if [ "$yorn" == "yes" ]; then
		add=0
		save_config
	    else
		echo "Cleared the e-mail list"
		sysadm_email=()
	    fi
	fi
    done
}

function summarize_client_config
{
    echo "Conferencing CLIENT SUMMARY"
    echo "------------------"
    echo "SSL/TLS security enabled: $enable_security"
    echo "Client version: $client_version"
    echo "XML router: $external_hname"
    echo "Conferencing Client download URL: $protocol_prefix://$portal_hname/imidio/downloads/conferencing.exe"
    echo "Conferencing Client User's Guide URL: $protocol_prefix://$portal_hname/imidio/client-manual/"
    echo
}

function summarize_service_assignment
{
    echo "SERVICE HOST ASSIGNMENTS"
    echo "------------------------"
    if [ ${#unique_hosts[*]} -gt 1 ]; then

	echo -n "Web portal host(s):"
	local idx=0
	while [ $idx -lt ${#portal_local_hname[*]} ]; do
	    echo -n " ${portal_local_hname[$idx]}/${portal_ip[$idx]}"
	    idx=$(($idx + 1))
	done
	echo
	
	echo -n "XML router host(s):"
	idx=0
	while [ $idx -lt ${#lcl_xmlrouter[*]} ]; do
	    echo -n " ${lcl_xmlrouter[$idx]}/${lcl_xmlrouter_ip[$idx]}"
	    idx=$(($idx + 1))
	done
	echo

	echo "Database host: $db_host"
	local access_ctl
	idx=0
	while [ $idx -lt ${#db_ip_addr[*]} ]; do
	    access_ctl=" ${db_ip_addr[$idx]}/${db_netmask[$idx]}"
	    idx=$(($idx + 1))
	done
	echo "Database access: $access_ctl"

	echo -n "Meeting controller host(s):"
	idx=0
	while [ $idx -lt ${#controller_host[*]} ]; do
	    echo -n " ${controller_host[$idx]}"
	    idx=$(($idx + 1))
	done
	echo
	
	if [ "$voice_provider" != "stub" ]; then
	    echo -n "Conferencing voice bridge host(s):"
	    idx=0
	    while [ $idx -lt ${#voice_host[*]} ]; do
		echo -n " ${voice_host[$idx]} tel:${voice_phones[$idx]}"
		idx=$(($idx + 1))
	    done
	    echo
	else
	    echo "No voice bridges configured"
	fi
	
	echo -n "Address book host:"
	idx=0
	while [ $idx -lt ${#controller_host[*]} ]; do
	    echo -n " ${addressbk_host[$idx]}"
	    idx=$(($idx + 1))
	done
	echo

	echo -n "Meeting archiver host(s):"
	idx=0
	while [ $idx -lt ${#mtgarchiver_host[*]} ]; do
	    echo -n " ${mtgarchiver_host[$idx]}"
	    idx=$(($idx + 1))
	done
	echo

	echo "External API portal URL: $protocol_prefix://$extapi_portal_hname:$extapi_portal_port/imidio_api/"

	echo "External API portal host(s): $extapi_portal_local_hname/$extapi_portal_ip"

	echo -n "Mailer/notifier host:"
	idx=0
	while [ $idx -lt ${#mailer_host[*]} ]; do
	    echo -n " ${mailer_host[$idx]}"
	    idx=$(($idx + 1))
	done
	echo

	echo -n "Converter host:"
	idx=0
	while [ $idx -lt ${#converter_host[*]} ]; do
	    echo -n " ${converter_host[$idx]}"
	    idx=$(($idx + 1))
	done
	echo

	echo -n "Desktop/app-share host(s):"
	idx=0
	while [ $idx -lt ${#share_local_host[*]} ]; do
	    echo -n " ${share_local_host[$idx]}/${share_ip[$idx]}"
	    idx=$(($idx + 1))
	done
	echo
    else
	echo "All services running on ${unique_hosts[0]}"
    fi
}

function pause_for_enter
{
    local what=$1

    [ -z "$what" ] && what="configuration"

    echo
    echo ">>>>> Press Enter to continue viewing $what"
    echo
    read resp
}

function summarize_config
{
    local uline="$( echo $cluster | sed -e 's+.+-+g' )"
    echo "$uline-------------"
    echo "$cluster INSTALLATION"
    echo "$uline-------------"
    echo

	summarize_ssl_security_config

    summarize_client_config
    pause_for_enter
    summarize_service_assignment
    pause_for_enter
    summarize_optional_config
}


function show_modify_config_help
{
    cat<<EOF
If you do not want to modify the configuration, an
upgrade/reconfiguration will happen immediately.  If you do want to
modify the configuration you will be prompted for the configuration
information.
EOF
}

function get_cluster
{
    echo
    until get_yorn "Is this a new installation (n = upgrade or reconfigure)?" "n" show_new_installation_help ; do continue; done
    is_new_install=$yorn
    if [ "$yorn" == "no" ]; then
	valid=0
	while [ $valid -eq 0 ]; do
	    summarize_existing_configurations
	    
	    until get_strval "Pick one of the installations above:" $cluster show_config_name_help ; do continue; done
	    cluster=$strval
	    echo $cluster > cluster.txt
	    if [ ! -e $cluster/global-config ]; then
		echo "$cluster doesn't exist; Pick one from the list"
	    else
		valid=1
	    fi
	done

	until get_yorn "Do you want to modify this config (n = start upgrade/reconfig)?" "y" show_modify_config_help ; do continue; done
	do_query_config=$yorn
	if [ ! -e $cluster/server.pem ]; then 
	  cp cluster-prototype/server.pem $cluster
	  cp cluster-prototype/wserver.pem $cluster
	fi
	if [ ! -e $cluster/privkey.pem ]; then 
	  cp cluster-prototype/privkey.pem $cluster
	fi
    else
	until get_strval "Choose a name for this new installation:" $cluster show_config_name_help ; do continue; done
	cluster=$strval
	echo $cluster > cluster.txt
	if [ -e $cluster/global-config ]; then
	    until get_yorn "$cluster exists.  Do you want to use the saved configuration in $cluster?" "y" show_use_saved_help ; do continue; done
	    if [ "$yorn" == "no" ]; then
		rm -rf $cluster
		cp -r cluster-prototype $cluster
	    else
	        if [ ! -e $cluster/server.pem ]; then 
	  	    cp cluster-prototype/server.pem $cluster
		    cp cluster-prototype/wserver.pem $cluster
	        fi
		if [ ! -e $cluster/privkey.pem ]; then 
		    cp cluster-prototype/privkey.pem $cluster
		fi
	    fi
	else
	    echo "The \"$cluster\" configuration will be stored in $(pwd)/$cluster."
	    echo "It is advisable to keep a copy of this directory in a safe place"
	    echo "for backup purposes."
	    echo
	    cp -r cluster-prototype $cluster
	fi
	
	echo "You must initialize the database for the system to operate."
	echo "However, do not initialize a database that is in use.  You will lose all its data."
	until get_yorn "Initialize the database for this new installation?" "y" show_db_init_help ; do continue; done;
	if [ "$yorn" == "yes" ]; then
            echo "Please enter the password for the admin user."
            until get_passwd "Password for admin user?" "" ; do continue; done
            initdbarg="--initdb --password ${strval}"
	    echo $initdbarg > $cluster/initdbarg.txt
	else
	    initdbarg=""
	    rm -f $cluster/initdbarg.txt
	fi

	do_query_config="yes"
    fi
}

function show_remote_install_help
{
    cat<<EOF
The system will use scp(1) and ssh(1) to install on remote systems.
The root user will be used for the installation.  In order to avoid
typing the root password multiple times, you can read the ssh appendix
in the Conferencing Installation Manual about setting up public/private 
key authentication.
EOF
}

function allocate_jabber_service_ports
{
    local -a ports_in_use=( \
	${jadc2s_port[*]} \
	${controller_port[*]} \
	${voice_port[*]} \
	${mtgarchiver_port[*]} \
	${addressbk_port[*]} \
	${addressbk_port[*]} \
	${mailer_port[*]} \
	${extapi_port[*]} \
	${addressbk_port[*]} \
    ${extportal_port[*]} \
    ${converter_port[*]} \
	)
    next_port=$( \
	echo ${ports_in_use[*]} | \
	tr ' ' '\n' | \
	sort -n | \
	tail -1 \
	)

    next_port=$(($next_port+1))

    local idx=0
    while [ $idx -lt ${#controller_host[*]} ]; do
	if [ -z "${controller_port[$idx]}" ]; then
	    controller_port[$idx]=$next_port
	    next_port=$(($next_port+1))
	fi
	idx=$(($idx+1))
    done
    
    idx=0
    while [ $idx -lt ${#voice_host[*]} ]; do
	if [ -z "${voice_port[$idx]}" ]; then
	    voice_port[$idx]=$next_port
	    next_port=$(($next_port+1))
	fi
	idx=$(($idx+1))
    done
    
    idx=0
    while [ $idx -lt ${#mtgarchiver_host[*]} ]; do
	if [ -z "${mtgarchiver_port[$idx]}" ]; then
	    mtgarchiver_port[$idx]=$next_port
	    next_port=$(($next_port+1))
	fi
	idx=$(($idx+1))
    done
    
    if [ -z "${extportal_port[0]}" ]; then
        extportal_port[0]=$next_port
        extportal_instances=1
	    next_port=$(($next_port+1))
    fi
}

function clear_rt_log_ports
{
    rt_user_port=""
    rt_call_port=""
    rt_rsrv_port=""
}

function allocate_rt_log_ports
{
    rt_user_port=$next_port
    next_port=$(($next_port+1))

    rt_call_port=$next_port
    next_port=$(($next_port+1))

    rt_rsrv_port=$next_port
    next_port=$(($next_port+1))
}

function uninstall_removed_hosts
{
    for host in ${prior_unique_hosts[*]} ; do
	if ! $( echo ${unique_hosts[*]} | grep -q $host ) ; then
	    ./uninstall.sh --host $host
	fi
    done
}

function query_config
{
    if [ ${#unique_hosts[*]} -eq 0 ]; then
	on_single_host="y"
    elif [ ${#unique_hosts[*]} -eq 1 ]; then
	if [ "${unique_hosts[0]}" == "localhost" ]; then
	    on_single_host="y"
	else
	    on_single_host="n"
	fi
    else
	on_single_host="n"
    fi

    update_ssl_security_config

    until get_yorn "Are you installing on a single host (n = multiple hosts)?" $on_single_host show_single_vs_multi ; do continue; done
    on_single_host=$yorn
    if [ "$yorn" == "yes" ]; then
	query_single_host_config
    else ## single machine == "no"
	rm -f $cluster/single_host.txt
	query_multi_host_config
    fi ## on_this_host y/n
}

function show_host_array_help
{
    cat<<EOF

The provided host names must agree with the values returned by
executing 'uname -n' on a terminal on the hosts.
EOF
}

function show_ip_array_help
{
    cat<<EOF
The IP addresses provided here must be defined on the hosts previously
entered.
EOF
}

function query_host_array
{
    local type=$1
    local requires_ip=$2
    local max=$3
    local help_function=$4
    local idx=0

    local update_default
    if [ ${#host_array[*]} -gt 0 ]; then
	echo -n "The following $type hosts are defined"
	if [ $requires_ip -ne 0 ]; then
	    echo " (host/IP addr):"
	else
	    echo ":"
	fi
	
	local output="    "
	idx=0
	while [ $idx -lt ${#host_array[*]} ]; do
	    output="$output ${host_array[$idx]}"
	    if [ $requires_ip -ne 0 ]; then
		output="$output/${ip_array[$idx]}"
	    fi
	    idx=$(($idx+1))
	done
	echo "$output"
	update_default="n"
    else
	echo
	echo "There are *no* $type hosts defined."
	update_default="y"
    fi

    until get_yorn "Change the set of hosts that run the $type service?" $update_default $help_function ; do continue; done
    if [ $yorn == "yes" ]; then
	local valid=0
	local -a new_host_array=(  )
	local -a new_ip_array=(  )
	while [ $valid -eq 0 ]; do 
	    until get_strval "Enter the $type hosts (separated by spaces):" "${host_array[*]}" $help_function; do continue; done
	    strval=`echo $strval | tr A-Z a-z`
	    new_host_array=( $strval )
	    if [ $max -gt 0 -a ${#new_host_array[*]} -gt $max ]; then
		echo "!!!!! Too many $type hosts defined.  At most $max $type hosts can be defined"
		continue
	    fi

	    if [ $requires_ip -ne 0 ]; then
		until get_strval "Enter the $type IP addresses (separated by spaces): " "${ip_array[*]}" show_ip_array_help ; do continue; done
		new_ip_array=( $strval )
	    fi

	    if ! validate_host_array ${new_host_array[*]} ; then
		until get_yorn "Can't validate hosts.  Continue anyway?" "n" show_unreachable_help ; do continue; done
		if [ "$yorn" == "no" ]; then
		    continue;
		fi
	    fi

	    if [ $requires_ip -ne 0 ]; then
		if [ ${#new_host_array[*]} -ne ${#new_ip_array[*]} ]; then
		    echo "There must be the same number of hosts as IP addresses (${#new_host_array[*]} hosts, ${#new_ip_array[*]} ip addrs)"
		    continue;
		fi
		if ! validate_ip_array ${new_ip_array[*]} ; then
		    until get_yorn "Can't validate IP addresses.  Continue anyway?" "n" show_unreachable_help ; do continue; done
		    if [ "$yorn" == "no" ]; then
			continue;
		    fi
		fi
	    fi
	    
	    host_array=( ${new_host_array[*]} )
	    if [ $requires_ip -ne 0 ]; then
		ip_array=( ${new_ip_array[*]} )
	    fi
	    
	    valid=1
	done
    fi
}

function show_db_access_help
{
    echo "The XML router, address book and web portal hosts must be able"
    echo "access the database.  By default, all services running on the"
    echo "database host have access to the database.  If any of the"
    echo "above services do not run on the database host, you must"
    echo "define the access control for thos services."
    echo ""
    echo "Access control is based on a range of source IP addresses"
    echo "specified using an IP address and a netmask."
    echo "When entering IP addresses and netmasks, separate the two with"
    echo "a slash.  When entering multiple address ranges, separate each"
    echo "IP address/netmask pair with a space. (e.g."
    echo "\"10.0.1.1/255.255.255.0 10.0.2.1/255.255.255.0\""
    echo ""
    echo "For example:"
    echo "    - If you wanted to allow connections from 10.1.1.*,"
    echo "      you could enter '10.1.1.1/255.255.255.0'."
    echo "    - If you wanted to allow a connection from a specific IP address,"
    echo "      you could enter 'W.X.Y.Z/255.255.255.255'."
    echo ""
}

function show_meeting_controller_help
{
    cat<<EOF
The meeting controller(s) manage all aspects of the lifecycle of
active meetings.

EOF
    show_host_array_help
}

function show_web_portal_help
{
    cat<<EOF
The web portal services HTTP requests for meeting invitations, meeting
archives and the administrator's console.

If you have a load balancer in place, web portal URL's must reference
the load balancer's IP address for the pool of web portal hosts.

Only define more than one web portal host if you have a load balancer
installed and configured.

EOF
}

function show_web_portal_hosts_help
{
    cat<<EOF
The web portal hosts will run the HTTP daemons to handle the web
portal HTTP requests.

Only define more than one web portal host if you have a load balancer
installed and configured to balance the load over the pool of web
portal machines.
EOF
    show_host_array_help
}

function show_xml_router_help
{
    cat<<EOF
The XML router interconnects Conferencing clients and Conferencing services such as the
meeting archiver, voice bridge, etc.

Only define more than one XML router host if you have a load balancer
installed and configured to balance the load over the pool of XML
router machines.
EOF
    show_host_array_help
}

function show_db_host_help
{
    cat<<EOF
You must define a database host to provide the system database.

The host you choose must be accessible to any XML router, address
book, and web portal (for user authentication).
EOF
    show_host_array_help
}

function show_extapi_help
{
    cat<<EOF
The external API is used for processing meeting invitations, to
provide support for the administrators console and to provide a way
for external components to interface with Conferencing.

It is accessed by the web portal hosts and by any non-Conferencing web
applications based on it.  The external API URL must allow for the web
portals (and non-Conferencing components, if any) to connect to the external
API on port 8000.

EOF
}

function show_internal_extapi_help
{
    cat<<EOF
The internal host name for the external API is the host name for the
system as reported by 'uname -n' on a terminal on the host.

It may be valid for the external API internal host name to be the same
as the hostname in the external API URL.  For instance, if there is no
difference between the internal host name and the external API URL
host name (e.g. inside vs. outside firewall host names resolve to the
correct IP addr or if there is no need to resolve the URL outside the
firewall).

EOF
}

function show_internal_extapi_ip_help
{
    cat<<EOF
The internal IP address for the external API is the IP address that
the HTTP server will use to listen for connections on port 8000.

EOF
}

function show_address_book_host_help
{
    cat<<EOF
This is the host name assigned to the system that will run the address
book.  The address book is used to manage meeting schedule and contact
information in the database.

This host name must be the same as the host name returned by executing
'uname -n' on the command line on the address book host.
EOF
    show_host_array_help
}

function show_voice_bridge_help
{
    cat<<EOF
The Conferencing voice bridge interconnects phone calls in a conference.  
It is a purely software-based voice bridge using the Asterisk open source PBX 
as a media server.
EOF
}

function show_voice_bridge_hosts_help
{
    show_voice_bridge_help
    show_host_array_help
}

function show_mailer_host_help {
    cat<<EOF
This is the host name assigned to the system that will run the mailer.
This host name must be the same as the host name returned by executing
'uname -n' on the command line on the address book host.
EOF
    show_host_array_help
}

function show_converter_host_help {
    cat<<EOF
This is the host name assigned to the system that will run the converter.
This host name must be the same as the host name returned by executing
'uname -n' on the command line on the converter host.
EOF
    show_host_array_help
}

function show_appshare_help
{
    cat<<EOF
The desktop/app-share servers distribute the share session data from
the presenter to all the viewers.  You must define one
desktop/app-share server and may define more than one.  The meeting
controller handles balancing the load among the servers so no external
load balancer is required.
EOF
    show_host_array_help
}

function show_share_external_hosts_help
{
    cat<<EOF
If the host names returned by 'uname -n' for the desktop/app-share server
machine are the same as the host names a Conferencing client would use to access
the same servers, you can answer 'y' to this question.  Otherwise, answer
'n' and you will be queried for the host names the Conferencing clients should use.
EOF
}

function show_phone_number_help
{
    cat<<EOF
The voice bridge phone number must agree with the phone number
assigned to Conferencing user communities in the administrator's console.
EOF
}

function show_meeting_archive_help
{
    cat<<EOF
The meeting archiver takes outputs from a completed meeting
(e.g. audio, chat messages, etc.) and creates an archive of the
meeting.

You may define as many meeting archivers as necessary to handle the
archive creation load.  Some considerations are that for meetings with
a recorded voice conference, the meeting archiver will fetch the audio
from the voice bridge (28 Mbyte/hr of meeting).and the recorded
desktop/app share session ( 0.3-8 Mbtye/hr of meeting) and produce a
Shockwave Flash(TM) animation from them (100% of one CPU for 1/20 of
the meeting duration).

EOF
    show_host_array_help
}

function show_xml_router_single_host_help
{
    cat<<EOF
Conferencing clients with use this hostname to connect to the XML router.  If
any Conferencing clients are outside your firewall, the host you provide here
must be able to be resolved to the IP address of the XML router.

NOTE: The XML router, desktop/app-share server and web portal share
    port 80.  So the IP address of the XML router must be distinct
    from those for the desktop/app-share server and web portal.

EOF
}

function show_xml_router_single_host_help
{
    cat<<EOF
The XML router will be accepting connections using this IP address on
the host chosen for this installation.

NOTE: The XML router, desktop/app-share server and web portal share
    port 80.  So the IP address of the XML router must be distinct
    from those for the desktop/app-share server and web portal.

EOF
}

function show_xml_router_help
{
    cat<<EOF
The XML routers interconnect Conferencing clients and services (e.g. voice
bridge, mailer, etc. ) so that they can exchange commands and events.

Only define more than one XML router host if you have a load balancer
configured to balance the load over the pool of XML router machines.
EOF
    show_host_array_help
}

function show_web_portal_ip_single_host_help
{
    cat<<EOF
The web portal will be accepting HTTP requests using this IP address
on the host chosen for this installation.

NOTE: The XML router, desktop/app-share server and web portal share
    port 80.  So the IP address of the web portal must be distinct
    from those for the XML router and desktop/app-share servers.

EOF
}


function show_appshare_ip_single_host_help
{
    cat<<EOF
The desktop/app-share server will be listening for Conferencing client
connections on the IP address you provide here.

NOTE: The XML router, desktop/app-share server and web portal share
    port 80.  So the IP address of the desktop/app-share connections
    must be distinct from those for the XML router and web portal.

EOF
}

function show_web_portal_single_host_help
{
    cat<<EOF
Web browsers on the Conferencing client hosts will be using this host name to
access the web portal for this installation.

NOTE: The XML router, desktop/app-share server and web portal share
    port 80.  So the host name provided must map to an IP address the
    web portal that is distinct from those for the XML router and
    desktop/app-share servers.

EOF
}

function show_web_portal_hostname_help
{
    cat<<EOF
Web browsers on Conferencing client machines will make HTTP requests to the web
portal using the hostname provided here.  If any web browsers are
outside your firewall, you must provide a hostname that will resolve
to an IP address accessible outside the firewall.

EOF
}

function show_db_host_help
{
    cat<<EOF
The database host will be used to run the system database.  You should
choose a host that can accept connections from the XML router hosts,
the address book host and the web portal hosts over port 5432.

EOF
}

function show_multi_phone_help
{
    cat<<EOF
If your bridge supports multiple phone numbers for meeting
participants you can choose to enable this feature.  If you enable
this feature, you can enter the bridge phone number as in:
    US: 800-808-8008 INT'L: 877-777-0001
and meeting invitations will include the string you enter.

If you choose not to enable this feature, enter comma separated phone
numbers for each bridge and one of the numbers will be assigned to
all meeting participants.  The number given to each participant is
assignable per user community.
EOF
}

function show_voip_feature_help
{
    cat<<EOF
The Conferencing voice bridge supports voice-over-IP connections from the Conferencing client and
other VoIP services.  Enter 'y' to enable this feature, 'n' to disable.
EOF
}

function show_voip_ip_help
{
    cat<<EOF
The Conferencing voice bridge will be listening for Conferencing client PC Phone
connections on the IP address you provide here.
EOF
}

function show_voip_host_help
{
    cat<<EOF
The Conferencing client PC Phone will use use this host name to connect to the Conferencing voice
bridge.
EOF
}

function show_ast_ip_help
{
    cat<<EOF
The main Asterisk media server is where all calls from your SIP PBX/provider 
will initially be directed for PIN prompting and entry.  Once the confereee
enters their PIN, the voice service may direct the call to a conference on
another Asterisk media server, if configured.
EOF
}

function show_ast_name_help
{
    cat<<EOF
Every Asterisk media server has a name that is used in debug logging when
referring to the server.  This name can be the hostname of the machine.
EOF
}

function show_ast_mgrport_help
{
    cat<<EOF
The Asterisk Manager Interface accepts connections on a pre-configured port
defined in the manager.conf file on an Asterisk media server.
EOF
}

function show_ast_username_help
{
    cat<<EOF
The voice service communicates with Asterisk media servers via the Asterisk
Manager Interface.  To establish a session with the AMI, a username and secret
are required.  The username and secret are specified in the manager.conf file
on an Asterisk media server.
EOF
}

function show_ast_max_help
{
    cat<<EOF
Specify the maximum number of conference participants you would like to support
on this Asterisk server.  You should select the maximum capacity based on the
computing power available on the media server and the computation requirements
for the types of codecs you expect will most often be used by conferees.
EOF
}

function config_voice_bridge_inst
{
    local inst=$1
    local name=$2

    # TODO: remove
    if [ "$voice_provider" == "nms" ]; then
	if [ "$voip_pcphone" == "y" ]; then
    	    get_valid_address "What is the IP addr for voice bridge ${name}? " "${voip_ip[$inst]}" "ip" show_voip_ip_help
	    voip_ip[$inst]=$valid_address
	    save_config
	    
	    get_valid_address "What is the external hostname for voice bridge ${name}? " "${voip_host[$inst]}" "hname" show_voip_host_help
	    voip_host[$inst]=$valid_address
	    save_config
	    
	    get_valid_address "What is the base IP address for the NMS boards in voice bridge ${name}? " "${nms_base_ip[$inst]}" "ip" show_voip_nms_ip_help "" "no"
	    nms_base_ip[$inst]=$valid_address
	    save_config
	else
	    voip_ip[$inst]="127.0.0.1"
	    voip_host[$inst]="localhost"
	    nms_base_ip[$inst]="0.0.0.0"
	    save_config
	fi
    else
	echo "Multiple instances of the voice bridge service currently only supported with NMS-based bridges"
    fi
}

function config_voice_bridges
{
    if [ -z "$multi_phone_bridges" ]; then
	multi_phone_bridges="no"
    fi

    local mp_default
    if [ "$multi_phone_bridges" == "yes" ]; then
	mp_default="y"
    else
	mp_default="n"
    fi

    # default is voip using compressed codec
    if [ -z $voip_pcphone ]; then
    	voip_pcphone="y";
    fi
    
    until get_yorn "Are you using the Conferencing client PC Phone?" $voip_pcphone show_voip_feature_help ; do continue; done
    if [ "$yorn" == "yes" ]; then
	voip_pcphone="y"
	save_config
    else
	voip_pcphone="n"
	save_config
    fi

    multi_phone_bridges="no"
    until get_yorn "Are you providing meeting participants with multiple bridge phone numbers?" $mp_default show_multi_phone_help ; do continue; done
    multi_phone_bridges=$yorn;
    save_config

    local -a host_array=( ${voice_host[*]} )
    valid=0
    while [ $valid -eq 0 ]; do
	host_array=( ${voice_host[*]} )
	query_host_array "voice bridge" 0 -1 show_voice_bridge_hosts_help
	if [ ${#host_array[*]} -eq 0 ]; then
	    echo
	    echo "!!!!! You must define at least one voice bridge host."
	    echo
	    continue
	fi
	voice_host=( ${host_array[*]} )
	save_config
	valid=1
    done

    # prompt for info for each voice bridge
    local ix=0
    for bridge in ${host_array[*]} ; do
    	config_voice_bridge_inst $ix $bridge
	ix=$(($ix+1))
    done
    
    update_bridge_phones
}

function show_external_bridge_help
{
    cat<<EOF
Kablink provides an external voice bridge development package that
allows installations with existing voice bridges to interface them to
Conferencing.
EOF
}

function query_multi_host_config
{
    get_valid_address "What is the hostname Conferencing clients will use for the XML router? " "$external_hname" "hname" show_xml_router_help
    external_hname=$valid_address
    save_config

    get_valid_address "What is the hostname for web portal URL's? " "$portal_hname" "hname" show_web_portal_hostname_help
    portal_hname=$valid_address
    save_config

    local valid=0
    while [ $valid -eq 0 ]; do
	host_array=( ${portal_local_hname[*]} )
	ip_array=( ${portal_ip[*]} )
	if [ ${#host_array[*]} -eq 0 ]; then
	    echo "You must define at least one host to provide web portal service.  You"
	    echo "may define more than one if you have a load balancer installed and"
	    echo "configured."
	fi
	query_host_array "web portal" 1 -1 show_web_portal_hosts_help
	if [ ${#host_array[*]} -eq 0 ]; then
	    echo
	    echo "!!!!! You must define at least one host to provide web portal service."
	    echo
	    continue
	fi
	portal_local_hname=( ${host_array[*]} )
	portal_ip=( ${ip_array[*]} )
	save_config
	valid=1
    done
	
    valid=0
    while [ $valid -eq 0 ]; do
	host_array=( ${lcl_xmlrouter[*]} )
	ip_array=( ${lcl_xmlrouter_ip[*]} )
	if [ ${#host_array[*]} -eq 0 ]; then
	    echo "You must define at least one host to provide XML router service and"
	    echo "may not define more than two."
	fi
	query_host_array "XML router" 1 2 show_xml_router_help
	if [ ${#host_array[*]} -eq 0 ]; then
	    echo
	    echo "!!!!! You must define at least one host to provide XML router service."
	    echo
	    continue
	fi
	if [ ${#host_array[*]} -gt 2 ]; then
	    echo "You may define at most two hosts that provide XML router service."
	    continue
	fi
	lcl_xmlrouter=( ${host_array[*]} )
	lcl_xmlrouter_ip=( ${ip_array[*]} )
	save_config
	valid=1
    done
	
    get_valid_address "What host runs the database? " "$db_host" "hname" show_db_host_help
    db_host=$valid_address
    save_config
    
    valid=0
    while [ $valid -eq 0 ]; do
	host_array=( ${controller_host[*]} )
	if [ ${#host_array[*]} -eq 0 ]; then
	    echo "You must define at least one host to provide meeting controller service and"
	    echo "may not define more than two."
	fi
	query_host_array "meeting controller" 0 2 show_meeting_controller_help
	if [ ${#host_array[*]} -eq 0 ]; then
	    echo
	    echo "!!!!! You must define at least one meeting controller host"
	    echo
	    continue
	fi
	controller_host=( ${host_array[*]} )
	save_config
	valid=1
    done

    get_valid_address "What is the hostname for external-API URL's? " "$portal_hname" "hname" show_extapi_help
    extapi_portal_hname=$valid_address
    save_config
    
    get_valid_address "What local host will run the external API?" "$extapi_portal_local_hname" "hname" show_internal_extapi_help
    extapi_portal_local_hname=$valid_address
    extapi_host[0]=$valid_address
    save_config

    get_valid_address "what IP address will the external API expect connections?" "$extapi_portal_ip" "ip" show_internal_extapi_ip_help
    extapi_portal_ip=$valid_address
    save_config

    valid=0
    while [ $valid -eq 0 ]; do
	host_array=( ${share_local_host[*]} )
	ip_array=( ${share_ip[*]} )
	if [ ${#host_array[*]} -eq 0 ]; then
	    echo "You must define at least one host to provide desktop/app-share service."
	fi
	query_host_array "desktop/app-share" 1 -1 show_appshare_help
	if [ ${#host_array[*]} -eq 0 ]; then
	    echo
	    echo "!!!!! You must define at least one host to provide desktop/app-share service."
	    echo
	    continue
	fi
	share_local_host=( ${host_array[*]} )
	share_ip=( ${ip_array[*]} )
	save_config
	
	if [ "$share_host_equal" == "yes" -o -z "$share_host_equal" ]; then
	    local equal_default="y"
	else
	    local equal_default="n"
	fi

	until get_yorn "Are the local desktop/app-share hosts given above also global?" $equal_default show_share_external_hosts_help ; do continue; done
	if [ "$yorn" == "yes" ]; then
	    share_host=( ${share_local_host[*]} )
	else
	    host_array=( ${share_host[*]} )
	    query_host_array "Conferencing client desktop/app-share" 0 -1 show_appshare_help
	    share_host=( ${host_array[*]} )
	fi
	save_config
	valid=1
    done

    local vb_default
    if [ "$voice_provider" == "ext" -o "$voice_provider" == "appconf" ]; then
	vb_default="y"
    else
	vb_default="n"
    fi

    until get_yorn "Do you have any Conferencing voice bridges?" $vb_default show_voice_bridge_help ; do continue; done
    if [ "$yorn" == "yes" ]; then
	if [ -e $cluster/extvoiced ]; then
	    echo "Deploying external voice bridge binary from cluster configuration"
	    voice_provider="ext"
	else 
	    if [ -e $cluster/appconfvoiced ]; then
		voice_provider="appconf"
	    else
		voice_provider="stub"
	    fi
	fi
	config_voice_bridges
    else
	voice_provider="stub"
	    # just run the stub voice bridge on the first controller host
	voice_host=( ${controller_host[0]} )
	voice_phones=( "none" )
	multi_phone_bridges="no"
	voip_pcphone="n"
    fi
    save_config

    host_array=( ${mtgarchiver_host[*]} )
    valid=0
    while [ $valid -eq 0 ]; do
	if [ ${#host_array[*]} -eq 0 ]; then
	    echo "You must define at least one host to provide meeting archiver service."
	fi
	query_host_array "meeting archiver" 0 -1 show_meeting_archive_help
	if [ ${#host_array[*]} -eq 0 ]; then
	    echo
	    echo "!!!!! You must define at least one host to provide meeting archiver service."
	    echo
	    continue
	fi
	mtgarchiver_host=( ${host_array[*]} )
	save_config
	valid=1
    done

    get_valid_address "What local host runs the address book? " "${addressbk_host[0]}" "hname"  show_address_book_host_help
    addressbk_host[0]=$valid_address
    save_config

    get_valid_address "What local host runs the mailer/notifier? " "${mailer_host[0]}" "hname"  show_mailer_host_help
    mailer_host[0]=$valid_address
    save_config

    get_valid_address "What local host runs the converter? " "${converter_host[0]}" "hname"  show_converter_host_help
    converter_host[0]=$valid_address
    save_config

    local -a db_client_ip="${lcl_xmlrouter_ip[*]} ${portal_ip[*]} ${addressbk_host[0]}"
    valid=0
    while [ $valid -eq 0 ]; do
	local default_access;
	idx=0
	echo "All of the following hosts/ip addresses must access the database:"
	echo -e "\t${db_client_ip[*]}"
	while [ $idx -lt ${#db_ip_addr[*]} ]; do
	    default_access="$default_access ${db_ip_addr[$idx]}/${db_netmask[$idx]}"
	    idx=$(($idx+1))
	done
	until get_strval "What are the IP address/netmask pair(s) for db access control?" "$default_access" show_db_access_help ; do continue; done
	if [ ! -z "$strval" ]; then
	    strval=`echo $strval | tr A-Z a-z`
	    db_ip_addr=( $(echo $strval | sed -e 's+/[0-9\.]*++g') )
	    db_netmask=( $(echo $strval | sed -e 's+[0-9\.]*/++g') )
	fi

	if [ ${#db_ip_addr[*]} -ne ${#db_netmask[*]} ]; then
	    echo "There are ${#db_ip_addr[*]} ip addresses and ${#db_netmask[*]} netmasks,"
	    echo "but there must be the same number of each."
	    echo "Check '$strval' for errors."
	fi
	save_config
	valid=1
    done

    update_optional_config
    
    get_unique_hosts
}

function show_single_host_help
{
    cat<<EOF
The installer will use scp(1)/ssh(1) to copy files and execute shell
scripts on the host that you provide so that you can install on the
remote host while logged in to this host.  This could be useful if you
are maintaining a beta vs. production installation.
EOF
}

function common_phone_config
{
    until get_yorn "Are you using the Conferencing client PC Phone?" $voip_pcphone show_voip_feature_help ; do continue; done
    if [ "$yorn" == "yes" ]; then
	if [ "$voice_provider" == "appconf" ]; then
	    voip_pcphone="y"
  	    voip_ip=( $ast_main_addr )
	    voip_host=( $ast_main_name )
	    save_config
	else
	    get_valid_address "What is the IP addr for the voice bridge? " "${voip_ip[0]}" "ip" show_voip_ip_help
	    voip_ip=( $valid_address )
	    save_config

	    get_valid_address "What is the hostname for the voice bridge? " "${voip_host[0]}" "hname" show_voip_host_help
	    voip_host=( $valid_address )
	    save_config
	fi
    else
	voip_pcphone="n"
	voip_ip=( 127.0.0.1 )
	voip_host=( localhost )
	nms_base_ip=( 0.0.0.0 )
	save_config
    fi

    multi_phone_bridges="no"
    until get_yorn "Are you providing meeting participants with multiple bridge phone numbers?" $mp_default show_multi_phone_help ; do continue; done
    multi_phone_bridges=$yorn;
    save_config
    
    update_bridge_phones
}

function query_single_host_config
{
    lcl_xmlrouter=( localhost )
    portal_local_hname=( localhost )
    extapi_portal_local_hname=localhost
    db_host=localhost
    db_ip_addr=()
    db_netmask=()
    controller_host=( localhost )
    addressbk_host=( localhost )
    mailer_host=( localhost )
    converter_host=( localhost )
    voice_host=( localhost )
    mtgarchiver_host=( localhost )
    extapi_host=( localhost )
    share_local_host=( localhost )
    extapi_local_hname=localhost

    save_config
	
    local this_host_default
    if [ -n "$single_host" ]; then
	this_host_default="n"
    else
	this_host_default="y"
    fi
    until get_yorn "Are you installing on this host?" $this_host_default show_this_host_help ; do continue; done
    if [ "$yorn" == "no" ]; then
	if [ ! -e $cluster/single_host.txt ]; then
	    single_host="$cluster"
	fi
	until get_valid_address "Which host will be used for the installation: " $single_host "hname" show_single_host_help ; do continue; done
	single_host=$valid_address
	echo $single_host > $cluster/single_host.txt
    else
	if [ $UID -ne 0 ]; then
	    echo "You will not be able to complete this install while logged on as \"$USER\""
	    echo "The installer must now exit to allow you to log in as the \"root\" user"
	    echo "Re-run $0 after you have logged on as \"root\""
	    save_config
	    exit 0
	fi
    fi
    
    until get_yorn "Will you be using multiple IP addresses (allows better firewall traversal and HTTP connections)?" $multi_ip show_multi_ip_help ; do continue; done

    if [ "$yorn" == "no" ]; then
    	multi_ip="n"
    	iptables_port_forwarding="no"
    
	get_valid_address "What is the IP addr for this server?" "${lcl_xmlrouter_ip[0]}" "ip" show_xml_router_single_host_help
	lcl_xmlrouter_ip=( $valid_address )
	portal_ip=( $valid_address )
	extapi_portal_ip=$valid_address
	share_ip=( $valid_address )
	save_config
	    
	get_valid_address "What is the hostname Conferencing clients will use to access this server?" "$external_hname" "hname" show_xml_router_single_host_ip_help
	external_hname=$valid_address
	portal_hname=$valid_address
	extapi_portal_hname=$valid_address
	share_host=( $valid_address )
	save_config
    
    else
    	multi_ip="y"
    	iptables_port_forwarding="yes"
    	
	get_valid_address "What is the IP addr for the XML router?" "${lcl_xmlrouter_ip[0]}" "ip" show_xml_router_single_host_help
	lcl_xmlrouter_ip=( $valid_address )
	save_config
	    
	get_valid_address "What is the hostname Conferencing clients will use to access the XML router?" "$external_hname" "hname" show_xml_router_single_host_ip_help
	external_hname=$valid_address
	save_config
	    
	get_valid_address "What is the IP addr for the web portal? " "${portal_ip[0]}" "ip" show_web_portal_ip_single_host_help
	portal_ip=( $valid_address )
	extapi_portal_ip=$valid_address
	save_config
	    
	get_valid_address "What is the hostname for web portal URL's? " "$portal_hname" "hname" show_web_portal_single_host_help
	portal_hname=$valid_address
	extapi_portal_hname=$valid_address
	save_config
	    
	get_valid_address "What is the IP addr for desktop/app-share? " "$share_ip" "ip" show_appshare_ip_single_host_help
	share_ip=( $valid_address )
	save_config
	   
	get_valid_address "What is the hostname for desktop/app-share? " "${share_host[0]}" "hname" show_appshare_host_single_host_help
	share_host=( $valid_address )
	save_config
    fi	   

    if [ -e $cluster/extvoiced ]; then
	echo "Will deploy with external voice bridge binary"
	voice_provider="ext"
    fi

    local vb_default
    if [ "$voice_provider" == "ext" -o "$voice_provider" == "appconf" ]; then
	vb_default="y"
    else
	vb_default="n"
    fi

    # default is voip; "e" would be used for encrypted voip (not supported for asterisk yet)
    if [ -z $voip_pcphone ]; then
    	voip_pcphone="y";
    fi

    until get_yorn "Are you running a Conferencing voice bridge?" $vb_default show_voice_bridge_help; do continue; done
    if [ "$yorn" == "yes" ]; then
	
	    if [ -e $cluster/extvoiced ]; then
		voice_provider="ext"
	    else
		voice_provider="appconf"
	    fi

	    if [ -z "$multi_phone_bridges" ]; then
		multi_phone_bridges="no"
	    fi
	    
	    local mp_default
	    if [ "$multi_phone_bridges" == "yes" ]; then
		mp_default="y"
	    else
		mp_default="n"
	    fi

	    if [ "$voice_provider" == "appconf" ]; then

		get_valid_address "What is the IP address of the main Asterisk media server?" "${ast_main_addr[0]}" "ip" show_ast_ip_help
		ast_main_addr=( $valid_address )
		save_config

		until get_strval "What is the host name of the main Asterisk media server?" "${ast_main_name[0]}" show_ast_name_help; do continue; done
		ast_main_name=( $strval )
		save_config

		until get_strval "What port is the main Asterisk media server using for Manager API connections?" "${ast_mgr_port[0]}" show_ast_mgrport_help; do continue; done
		ast_mgr_port=( $strval )
		save_config

		until get_strval "What is the Manager API username for the main Asterisk media server?" "${ast_main_username[0]}" show_ast_username_help; do continue; done
		ast_main_username=( $strval )
		save_config

		until get_strval "What is the Manager API secret for the main Asterisk media server?" "${ast_main_secret[0]}" show_ast_username_help; do continue; done
		ast_main_secret=( $strval )
		save_config

		until get_strval "What is the maximum number of conferees supported on the main Asterisk media server?" "${ast_main_max[0]}" show_ast_max_help;  do continue; done
		ast_main_max=( $strval )
		save_config
	    fi

	    common_phone_config
    else
	voice_provider="stub"
	voice_phones=( "none" )
	multi_phone_bridges=no
    fi
    save_config
    
    update_optional_config
    
    get_unique_hosts # should be localhost!
}

function show_enable_security_help
{
    cat<<EOF
If not enabled, then the IM, chat, and other messages are sent as clear text.
However if enabled, you need provide two security certificates, one for XML router,
and one for web server. Conferencing installer come with two, but they should be used only for 
testing purpose.
EOF
}

function summarize_ssl_security_config
{
    echo "SSL/TLS SECURITY SETTINGS"
    echo "----------------"
    if [ "$enable_security" == "yes" ]; then
      echo -e "\t- SSL/TLS security is enabled."
      echo -e "\t- The certificate for XML router is: $server_certificate"
      echo -e "\t- The certificate for web server is: $web_server_certificate"
    else
      echo -e "\t- SSL/TLS security is NOT enabled."
    fi
    echo
}

function update_ssl_security_config
{
    until get_yorn "Should the system enable SSL/TLS security?" "$enable_security" show_enable_security_help ; do continue; done
    enable_security=$yorn
    save_config

    if [ "$enable_security" == "yes" ]; then
		protocol_prefix="https"
		save_config
                local server_cert_default=$server_certificate
                if [ "$server_cert_default" == "" ]; then
                  server_cert_default="/opt/iic/conf/server.pem"
                fi
  		until get_strval "Input the path and name of the certificate for XML router:" "$server_cert_default" show_enable_security_help ; do continue; done
		server_certificate=$strval
		save_config
                local web_server_cert_default=$web_server_certificate
                if [ "$web_server_cert_default" == "" ]; then
                   web_server_cert_default="/opt/iic/conf/wserver.pem"
                fi
  		until get_strval "Input the path and name of the certificate for web server:" "$web_server_cert_default" show_enable_security_help ; do continue; done
		web_server_certificate=$strval
		save_config
	else
		protocol_prefix="http"
		save_config			
    fi
}

function show_are_you_satisfied_help
{
    cat<<EOF
If you are satified with your configuration, the Conferencing server software
will be installed and configured.  If you are not satified, you can
either choose to modify the configuration or exit the installer.  If
you exit the installer your current configuration is saved and will be
used for default values when you next run $0
EOF
}

function show_modify_again_help
{
    cat<<EOF
If you choose to modify your configuration, you will be prompted for
configuration information, except your responses for prior prompts
will be used for default values.

If you choose to *NOT* modify your configuration, the installer will
exit and save your configuration as $cluster.  You can then run $0 at
a later time to update the configuration.
EOF
}

function show_testing_install_help
{
    cat<<EOF


*******************
* GETTING STARTED *
*******************

You can check the status of the server(s) using the administrator's console
using the URL:
EOF
    echo "    $protocol_prefix://$portal_hname/imidio/console/"
cat<<EOF

The initial installation will create the screenname 'admin' with
the password entered during the install.  Use these values when logging 
onto the administrator's console.

After you log on to the console, you should see that all the system
components are online with green status indicators.

You can download the Conferencing client from a client using one of these URLs:

EOF
    echo "    $protocol_prefix://$portal_hname/imidio/downloads/conferencing.exe"
    echo "    $protocol_prefix://$portal_hname/imidio/downloads/conferencing.rpm"
cat <<EOF

A second community for containing normal system users has also been created.
Use screenname 'useradmin' and the admin password to sign on to this community
and create additional users from the Conferencing client.  

EOF
echo "You should also make a copy of the directory $(pwd)/$cluster"
echo "somewhere safe so that you do not lose your server configuration."
}

function main
{
    show_license

    # show_prerequisites
    show_installation_notes

    do_query_config="yes"
    
    if [ -e cluster.txt ]; then
	cluster="$(cat cluster.txt)"
    fi
    if [ -z "$cluster" ]; then
	cluster="ourzon"
    fi

    get_cluster
    
    . $cluster/global-config

    check_missing_config

    get_unique_hosts

    [ -e $cluster/single_host.txt ] && single_host="$(cat $cluster/single_host.txt)"

    if [ $is_new_install == "no" ]; then
	prior_unique_hosts=( ${unique_hosts[*]} )
    else
	service_secret=`openssl rand -base64 8 | cut -c -8`
	session_creation_secret=`openssl rand -base64 8 | cut -c -8`
    fi

    this_host="$(uname -n)"

    while [ "$do_query_config" == "yes" ]; do
	if [ "$do_query_config" == "yes" ]; then
	    if [ $is_new_install == "no" ]; then
		local idx=0
		local bkp_config=$cluster.$idx
		while [ -e $bkp_config ]; do
		    idx=$(($idx+1))
		    bkp_config=$cluster.$idx
		done
		echo "Backing up installation directory '$cluster' to '$bkp_config'"
		cp -r $cluster $bkp_config

        # copy locale sub-tree from cluster-prototype into cluster
        cp -r cluster-prototype/locale $cluster
	    fi
	    query_config
	fi
	
    echo
    echo
	echo '*******************************************************************'
	echo '* You will now be shown a summary of your configuration.          *'
	echo '* Check the values carefully since you will be asked to confirm   *'
	echo '* that you are satisfied with the configuration after it is       *'
	echo '* displayed.                                                      *'
	echo '*******************************************************************'
    echo
	
	summarize_config
	
	echo
	echo
	echo
	until get_yorn "Are you satisfied with your configuration (y = start install)?" "y" show_are_you_satisfied_help ; do continue; done
	if [ "$yorn" == "yes" ]; then
	    do_query_config="no"
	else
	    until get_yorn "Do you want to try modifying it again (n = exit install)?" "y" show_modify_again_help ; do continue; done
	    if [ "$yorn" == "no" ]; then
		echo "The configuration was saved for $cluster"
		echo "You can re-run $0 and modify the configuration for $cluster"
		echo "No software was installed, upgraded or configured"
		exit 0
	    fi
	fi
    done

    update_client_installers_tarball

    install

	if [ $? -ne 0 ]; then
	    exit $?
	fi

    # get rid of any CVS dirs
    rm -rf /opt/iic/locale/CVS
    for lang in $LANGS
    do
        rm -rf /opt/iic/locale/$lang/CVS
        rm -rf /opt/iic/locale/$lang/emails/CVS
        rm -rf /opt/iic/locale/$lang/LC_MESSAGES/CVS
        rm -rf /opt/iic/locale/$lang/web/CVS
        rm -rf /opt/iic/locale/$lang/web/invite/CVS
        if [ ! -e /var/iic/htdocs/imidio/client-manual/$lang ]; then
            ( cd /var/iic/htdocs/imidio/client-manual ; ln --symbolic en_US $lang )
        fi
    done

    touch $cluster/install-done

    show_testing_install_help
}

unset noglob  # make sure globbing is on

main
