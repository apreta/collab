#!/bin/bash

. ./utils.sh 

function usage
{
    echo "usage: $0 [--tarball <svr tarball>] [--version <rpm version>]"
    echo "    Ensure that cluster-prototype contains the correct client installers"
    echo "    Ensure that global-config in cluster-prototype has the correct client version number"
}

brand="apreta"
product="conferencing"
tarball=""
version=""
cluster=""
function parse_cmdline
{
    while [ $# -gt 0 ] ; do
	case $1 in
	    --brand)
		brand="$2"
		shift 2
		;;
	    --product)
		product="$2"
		shift 2
		;;
	    --tarball)
		tarball="$2"
		shift 2
		;;
	    --version)
		version="$2"
		shift 2
		;;
	    --cluster)
		cluster="$2"
		shift 2
		;;
	    --help|-h)
		usage
		exit -1
		;;
	    *)
		echo "Unknown argument '$1' seen"
		usage
		exit -1
		;;
	esac
    done
}

parse_cmdline $*

if [ -z "$tarball" ]; then
    echo "Specify --tarball"
    usage
    exit -1
fi

if [ -z "$version" ]; then
    echo "Specify --version"
    usage
    exit -1
fi

if [ -z "$cluster" ]; then
    if [ "$product" == "conferencing" ]; then
	cluster="cluster-prototype"
    elif [ "$product" == "zon-lite" ]; then
	cluster="zon-lite-prototype"
    fi
fi

. $cluster/global-config

update_client_version

if ! check_cluster_config $cluster ; then
    exit -1
fi

scripts="check-config.sh uninstall.sh install-cluster.sh config-cluster.sh control-cluster.sh langs.sh utils.sh ldaputils.sh"
case $product in
    conferencing)
	scripts="$scripts install.sh settings.sh"
	;;
    zon-lite)
	scripts="$scripts install-lite.sh"
	;;
    *)
	echo "Invalid product \"$product\" given."
	exit -1
esac

if [ -f nmsconfig ]; then
	scripts="$scripts nmsconfig"
fi

now=`date +'%s'`

mkdir -p /tmp/dist-$now/${brand}-$product

case $product in
    conferencing)
	cp -r $cluster /tmp/dist-$now/${brand}-$product/cluster-prototype
	cp -r ../iic/services/locale /tmp/dist-$now/${brand}-$product/cluster-prototype
	;;
    zon-lite)
	cp -r $cluster /tmp/dist-$now/${brand}-$product/zon-lite-prototype
	;;
esac

# get rid of any CVS dirs 
rm -rf /tmp/dist-$now/${brand}-$product/CVS
rm -rf /tmp/dist-$now/${brand}-$product/cluster-prototype/CVS
rm -rf /tmp/dist-$now/${brand}-$product/cluster-prototype/locale/CVS
for lang in $LANGS
do
    rm -rf /tmp/dist-$now/${brand}-$product/cluster-prototype/locale/$lang/CVS
    rm -rf /tmp/dist-$now/${brand}-$product/cluster-prototype/locale/$lang/emails/CVS
    rm -rf /tmp/dist-$now/${brand}-$product/cluster-prototype/locale/$lang/LC_MESSAGES/CVS
    rm -rf /tmp/dist-$now/${brand}-$product/cluster-prototype/locale/$lang/web/CVS
    rm -rf /tmp/dist-$now/${brand}-$product/cluster-prototype/locale/$lang/web/invite/CVS
done

# get rid of any backup files that might be hanging around
rm -f $( find /tmp/dist-$now -name '*~' -print )

cp -r $scripts /tmp/dist-$now/${brand}-$product
cp -r $tarball /tmp/dist-$now/${brand}-$product

( cd /tmp/dist-$now ; tar zcf - ${brand}-$product ) > ~/${brand}-$product-$version.tar.gz

echo "Created ~/${brand}-$product-$version.tar.gz"
