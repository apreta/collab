#!/bin/bash

. ./utils.sh 

function pick_cluster
{
    local alts="$(find . -maxdepth 1 -mindepth 1 -type 'd' -print | grep -v 'cluster-prototype' | grep -v CVS | sed -e "s+^+\t+" )"
    if [ ! -z "$alts" ]; then
	echo "The following prior installations exist:"
	echo $alts | sed -e "s+\./+\t+g" | tr ' ' '\n'
    fi
    
    cluster=""
    [ -e cluster.txt ] && cluster=$(cat cluster.txt)

    until get_strval "Pick one of the installations above (or CTRL-C to exit):" $cluster show_config_name_help ; do continue; done
    cluster=$strval
}

function uninstall_host
{
    echo "Uninstalling Conferencing server on $1"
    remote_cmd $1 "/opt/iic/conf/control-services.sh stop"
    remote_cmd $1 'rpm -e $( cat /opt/iic/zoninst-this.txt | sed -e "s+::.*++" | grep -v notlame | grep -v w3c )'
}

function uninstall
{
    if [ -z "$host" ]; then
	config="$cluster/global-config"
	
	. $config
	
	local raw_hosts=" \
	$db_host \
	$portal_local_hname \
	$extapi_portal_local_hname \
	${lcl_xmlrouter[*]} \
	${controller_host[*]} \
	${voice_host[*]} \
	${mtgarchiver_host[*]} \
	${addressbk_host[*]} \
	${mailer_host[*]} \
	${converter_host[*]} \
	${extapi_host[*]} \
	${share_local_host[*]} \
	"
	local -a uniq_hosts=( $( echo $raw_hosts | tr ' ' '\n' | sort -u | tr '\n' ' ') )

	if [ ${#uniq_hosts[*]} -eq 1 -a "${uniq_hosts[0]}" == "localhost" ]; then
	    if [ -e $cluster/single_host.txt ] ; then
		uninstall_host $(cat $cluster/single_host.txt)
	    else
		[ -e /opt/iic/conf/control-services.sh ] && \
		    /opt/iic/conf/control-services.sh stop
		[ -e /opt/iic/zoninst-this.txt ] && \
		    rpm -e $( cat /opt/iic/zoninst-this.txt | sed -e 's+::.*++' | grep -v notlame | grep -v w3c )
	    fi
	else
	    for h in ${uniq_hosts[*]} ; do
		uninstall_host $h
	    done
	fi
    else
	uninstall_host $host
    fi
}

host=""
[ $# -eq 2 ] && [ $1 == "--host" ] && host=$2
[ -z "$host" ] && pick_cluster
uninstall
