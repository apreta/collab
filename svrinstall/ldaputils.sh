function verify_ldap_credentials
{
    local ldap_url=$1
    local ldap_binddn=$2
    local ldap_bindpw=$3
    local ldap_basedn=$4
    local result
    local command=`which ldapsearch 2>/dev/null`

    if [ -z "$command" ]; then
	return 0;
    fi

    echo "Verifying LDAP connection information..."
    result=`ldapsearch -H "$ldap_url" -D "$ldap_binddn" -w "$ldap_bindpw" -x -b "$ldap_basedn" 2>&1 | grep -E '(ldap_bind|result)' | grep -v '#'`

    if [ "$result" = "result: 0 Success" ]; then
	return 0;
    elif [ "$result" = "result: 32 No such object" ]; then
	return 1;
    elif [ "$result" = "ldap_bind: Invalid credentials (49)" ]; then
	return 2;
    elif [ "$result" = "ldap_bind: No such object (32)" ]; then
	return 3;
    elif [ "$result" = "ldap_bind: Can't contact LDAP server (-1)" ]; then
	return 4;
    fi

    return 5;
}
