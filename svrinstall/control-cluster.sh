#!/bin/bash

. ./utils.sh 

function usage
{
    echo "usage: $0 [--cluster <cluster name>] [--single-host <hostname>] {start|stop|status}"
    echo "   You must specify one of either --cluster or --single-host"
}

cluster=""
operation=""
function parse_cmdline
{
    while [ $# -gt 0 ] ; do
	case $1 in
	    --cluster)
		cluster=$2
		shift 2
		;;
	    --single-host)
		single_host=$2
		shift 2
		;;
	    --help|-h)
		usage
		exit -1
		;;
	    start|stop|status)
		operation=$1
		shift
		;;
	    *)
		echo "Unknown argument '$1' seen"
		usage
		exit -1
		;;
	esac
    done
}

function control_host
{
    remote_cmd $1 "/opt/iic/conf/control-services.sh $operation" && echo "Ok" || echo "FAILED"
}

function distribute_control
{
    if [ -z "$single_host" ]; then
	config="$cluster/global-config"
	
	. $config

	local raw_hosts=" \
	$db_host \
	$portal_local_hname \
	$extapi_portal_local_hname \
	${lcl_xmlrouter[*]} \
	${controller_host[*]} \
	${voice_host[*]} \
	${mtgarchiver_host[*]} \
	${addressbk_host[*]} \
	${mailer_host[*]} \
	${converter_host[*]} \
	${extapi_host[*]} \
	${share_local_host[*]} \
	"
	
	local -a uniq_hosts=( $( echo $raw_hosts | tr ' ' '\n' | sort -u | tr '\n' ' ') )

	for h in ${uniq_hosts[*]} ; do
	    control_host $h
	    sleep 1
	done
    else
	control_host $single_host
    fi
}

parse_cmdline $*

[ -z "$operation" ] && usage && exit -1
[ -z "$single_host" -a -z "$cluster" ] && usage && exit -1

distribute_control
