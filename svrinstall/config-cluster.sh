#!/bin/bash

. ./utils.sh 

function usage
{
    echo "usage: $0 --cluster <cluster name> [--single-host <hostname>] [--initdb] [--verify]"
}

cluster=""
single_host=""
initdb="false"
verify="false"
password="iicadmin"

function parse_cmdline
{
    while [ $# -gt 0 ] ; do
	case $1 in
	    # This is where the site-specific configuration files
	    # must be found
	    --cluster)
		cluster=$2
		echo "$cluster" > cluster.txt
		shift 2
		;;
	    --single-host)
		single_host=$2
		shift 2
		;;
	    --initdb)
		initdb="true"
		shift
		;;
	    --password)
		password=$2
		shift 2
		;;
	    --verify)
		verify="true"
		shift
		;;
	    --help|-h)
		usage
		exit -1
		;;
	    *)
		echo "Unknown argument '$1' seen"
		usage
		exit -1
		;;
	esac
    done
}

function configure_host
{
    local initdbarg=""
    [ "$initdb" == "true" ] && initdbarg="--initdb --password ${password}"

    if [ -e ldap-ca.cer ]; then
	push_file $1 ldap-ca.cer /opt/iic/conf && echo "Ok" || echo "FAILED; see install_cluster.log"
    fi
    
    if [ -e $license ]; then
	push_file $1 $license /opt/iic/conf && echo "Ok" || echo "FAILED; see install_cluster.log"
    fi
    
    if [ -e $config ]; then
	push_file $1 $config /opt/iic/conf && echo "Ok" || echo "FAILED; see install_cluster.log"
    fi

    if [ -e $vconfig ]; then
	push_file $1 $vconfig /opt/iic/conf && echo "Ok" || echo "FAILED; see install_cluster.log"
    fi

    if [ -e $client_installers ]; then
	push_file $1 $client_installers /opt/iic/conf && echo "Ok" || echo "FAILED; see install_cluster.log"
    fi
    
    if [ -e $new_mtgarchive_template ]; then
	push_file $1 $new_mtgarchive_template /opt/iic/conf && echo "Ok" || \
	    echo "FAILED; see install_cluster.log"
    fi

    if [ -e $new_user_template ]; then
	push_file $1 $new_user_template /opt/iic/conf && echo "Ok"  || \
	    echo "FAILED; see install_cluster.log"
    fi

    if [ -e $invitation_template ]; then
	push_file $1 $invitation_template /opt/iic/conf && echo "Ok" || \
	    echo "FAILED; see install_cluster.log"
    fi

    if [ -e $meeting_summary_template ]; then
	push_file $1 $meeting_summary_template /opt/iic/conf && echo "Ok" || \
	    echo "FAILED; see install_cluster.log"
    fi
    
    if [ -e $dialing_rules ]; then
	push_file $1 $dialing_rules /opt/iic/conf && echo "Ok" || \
	    echo "FAILED; see install_cluster.log"
    fi

    if [ -e $external_bridge ]; then
	push_file $1 $external_bridge /opt/iic/bin && echo "Ok" || \
	    echo "FAILED; see install_cluster.log"
	remote_cmd $1 "chmod +x /opt/iic/bin/extvoiced" &&  echo "Ok" || \
	    echo "FAILED; see install_cluster.log"
    fi

    if [ -e $cluster/db_upgrade.sh ]; then
	push_file $1 $cluster/db_upgrade.sh /opt/iic/conf
    fi

	# build and push ldap config
    local xml
    if [ "$enable_ldap_sync" == "yes" ]; then
	local server
	xml='<!-- Servers -->'
	for server in ${ldap_servers[@]}; do
	    build_ldap_import_xml $server
	    xml="$xml""$strval"
	done
    else
	xml='<!-- Disabled -->'
    fi
    sed \
        -e "/@@@SERVERS@@@/ c $xml" \
        $cluster/ldap.xml.in > $cluster/ldap.xml
    push_file $1 $cluster/ldap.xml /opt/iic/conf
        
    # push PEM files to conf...
    if [ "$enable_security" == "yes" ]; then
        if [ -e $server_cert ]; then
            push_file $1 $server_cert /opt/iic/conf && echo "Ok" || echo "FAILED; see install_cluster.log"
        fi
        if [ -e $web_server_cert ]; then
            push_file $1 $web_server_cert /opt/iic/conf && echo "Ok" || echo "FAILED; see install_cluster.log"
        fi
    fi

    remote_cmd $1 "cd /opt/iic/conf ; ./configure.sh $initdbarg" && echo "Ok" || \
	echo "FAILED; see install_cluster.log"
}

function distribute_config
{
    local raw_hosts=" \
	$db_host \
	$portal_local_hname \
	$extapi_portal_local_hname \
	${lcl_xmlrouter[*]} \
	${controller_host[*]} \
	${voice_host[*]} \
	${mtgarchiver_host[*]} \
	${addressbk_host[*]} \
	${mailer_host[*]} \
	${converter_host[*]} \
	${extapi_host[*]} \
	${share_local_host[*]} \
	"

    local -a uniq_hosts=( $( echo $raw_hosts | tr ' ' '\n' | sort -u | tr '\n' ' ') )

    [ -z "$cluster" -a -e cluster.txt ] && cluster=$(cat cluster.txt)

    if [ ! -e $cluster ]; then
	echo "ERROR: cluster configuration directory $cluster does not exist"
	exit -1
    fi

    if ! check_cluster_config $cluster ; then
	exit -1
    fi

    if [ "$initdb" == "true" ]; then
	echo "INFO: database initialization WILL be done; initial clean database will result."
	echo -n "Are you sure you'd like to proceed? (y/n) "
	read resp
	if [ "$resp" != "y" -a "$resp" != "yes" ]; then
	    echo "Configuration ABORTED"
	    exit -1
	fi
    else
	echo "INFO: database initialization will not be done"
    fi

    if [ -z "$single_host" ]; then
	for h in ${uniq_hosts[*]} ; do
	    configure_host $h
	done
    else
	configure_host $single_host
    fi
}

parse_cmdline $*

[ -z "$cluster" ] && echo "Set a cluster name using the --cluster argument" && exit -1 
[ ! -e "$cluster" ] && echo "Create a cluster directory to hold the cluster configuration files" && exit -1
[ ! -e $config ] && echo "$config doesn't exist!" && exit -1

config="$cluster/global-config"
vconfig="$cluster/voiceconfig.xml"
license="license-key.xml"

. $config

distribute_config
