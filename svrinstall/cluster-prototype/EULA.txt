Copyright (c) 2009 apreta.com and its licensors.  All rights reserved.  

This work is governed by the Common Public Attribution License Version 1.0 (the 
"CPAL"); you may not use this file except in compliance with the CPAL. You may 
obtain a copy of the CPAL at http://www.opensource.org/licenses/cpal_1.0. The 
CPAL is based on the Mozilla Public License Version 1.1 but Sections 14 and 15 
have been added to cover use of software over a computer network and provide 
for limited attribution for the Original Developer. In addition, Exhibit A has 
been modified to be consistent with Exhibit B. 
Software distributed under the CPAL is distributed on an _AS IS_ basis, WITHOUT 
WARRANTY OF ANY KIND, either express or implied. See the CPAL for the specific 
language governing rights and limitations under the CPAL.

Powered by Kablink
