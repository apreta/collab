
// Wrapper for C++ blowfish routines, 3/6/07 Larry Simmers
extern "C" {

//#include "vncviewer.h"
#include "stdlib.h"
#include "string.h"
#include "stdio.h"
}

#include "blowfish.h"

CBlowFish blowfish;
bool    BlowfishInitted = false;

extern "C" {

void InitBlowfish(const char * key)
{
	blowfish.SetKey(key);
	blowfish.Initialize(BLOWFISH_KEY, sizeof(BLOWFISH_KEY));
}

void BlowfishEncode(char *lpBuffer, unsigned lSize, unsigned char *pEncrypted, char *pEncrypKey)
{

    if(!BlowfishInitted)
    {
        if( pEncrypKey )
            InitBlowfish( pEncrypKey );
        else
            InitBlowfish( "" );
        BlowfishInitted = true;
    }

    blowfish.Encode((BYTE*)lpBuffer, (BYTE*)pEncrypted, lSize);
}

}	// end extern "C"

