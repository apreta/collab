#ifndef _X11VNC_MACOSXCGS_H
#define _X11VNC_MACOSXCGS_H

/* -- macosxCGS.h -- */

extern void macosxCGS_get_all_windows(void);
extern int macosxCGS_get_qlook(int);
extern void macosxGCS_set_pasteboard(char *str, int len);
extern int macosxCGS_follow_animation_win(int win, int idx, int grow);
extern int macosxCGS_find_index(int w);

extern void macosx_copy_cglion(char *dest, int x, int y, unsigned int w, unsigned int h);
extern void macosx_cglion_init(void);
extern void macosx_cglion_fini(void);
extern int  macosx_cglion_get_height(void);
extern int  macosx_cglion_get_width(void);
extern int  macosx_cglion_get_bpp(void);
extern int  macosx_cglion_get_bps(void);
extern int  macosx_cglion_get_spp(void);


#endif /* _X11VNC_MACOSXCGS_H */
