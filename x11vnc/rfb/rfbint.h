#ifndef _RFB_RFBINT_H
#define _RFB_RFBINT_H 1
#ifndef _GENERATED_STDINT_H
#define _GENERATED_STDINT_H "x11vnc 0.8.4"
/* generated using a gnu compiler version gcc (GCC) 4.1.2 20071124 (Red Hat 4.1.2-42) Copyright (C) 2006 Free Software Foundation, Inc. This is free software; see the source for copying conditions. There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. */

#include <stdint.h>


/* system headers have good uint64_t */
#ifndef _HAVE_UINT64_T
#define _HAVE_UINT64_T
#endif

  /* once */
#endif
#endif
