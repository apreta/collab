// dialog.cpp : Defines the entry point for the application.
//

#include <windows.h>
#include <stdlib.h>
#include "resource.h"
int
CALLBACK
LogonDlgProc(
    HWND        hDlg,
    UINT        Message,
    WPARAM      wParam,
    LPARAM      lParam);
int cmdRemove(LPCTSTR BaseName,LPCTSTR Machine,int argc,TCHAR* argv[]);
int cmdInstall(LPCTSTR BaseName,LPCTSTR Machine,int argc,TCHAR* argv[]);
PWSTR versionText=( unsigned short *)TEXT(" Ultr@VNC Video Drivers Installation Program - v1.0.7");
typedef BOOL (WINAPI* pEnumDisplayDevices)(PVOID,DWORD,PVOID,DWORD);




BOOL newvir,newmir,mirupgrade,virupgrade;
BOOL F_mir_exist,F_vir_exist;

	char driversys1[256];
	char driversys2[256];
	char driverdll1[256];
	char driverdll2[256];
	char driverhdll1[256];
	char driverhdll2[256];
	char buffer[256];
	char command1[256];
	char command2[256];
	char command3[256];
	char command4[256];
	char command5[256];
	char command6[256];
	char command7[256];
	char command8[256];
	char command9[256];
	char command10[256];
	char command11[256];
	char command12[256];


void
Upgrade_mirdrv()
{
	system(command1);
	system(command2);
	system(command3);
}

void
Upgrade_virdrv()
{
	system(command4);
	system(command5);
	system(command6);
}

void
Remove_mirdrv()
{
	system(command7);
	system(command8);
	system(command9);
}

void
Remove_mirdrv2()
{
	TCHAR *cmd[1];
	cmd[0]="Winvnc_hook_display_driver";
	cmdRemove("setupdrv.exe",NULL,1,cmd);
}

void
Remove_virdrv()
{
	system(command10);
	system(command11);
	system(command12);
}

void
Remove_virdrv2()
{
	TCHAR *cmd[1];
	cmd[0]="Virtual_video_driver";
	cmdRemove("setupdrv.exe",NULL,1,cmd);
}


int
OSVersion()
{
	OSVERSIONINFO OSversion;
	
	OSversion.dwOSVersionInfoSize=sizeof(OSVERSIONINFO);

	GetVersionEx(&OSversion);

	switch(OSversion.dwPlatformId)
	{
		case VER_PLATFORM_WIN32_NT:
								  if(OSversion.dwMajorVersion==5 && OSversion.dwMinorVersion==0)
									 return 1;									    
								  if(OSversion.dwMajorVersion==5 && OSversion.dwMinorVersion==1)
									 return 2;
								  if(OSversion.dwMajorVersion==5 && OSversion.dwMinorVersion==2)
									 return 2;	
								  if(OSversion.dwMajorVersion<=4) 	  
								     return 3;
		case VER_PLATFORM_WIN32_WINDOWS:
								return 4;
	}
	return 0;
}

BOOL GetDllProductVersion(char* dllName, char *vBuffer, int size)
{
   char *versionInfo;
   char fileName[MAX_PATH + 1];
   void *lpBuffer;
   unsigned int cBytes;
   DWORD rBuffer;

   if( !dllName || !vBuffer )
      return(FALSE);

   DWORD sName = GetModuleFileName(NULL, fileName, sizeof(fileName));
// FYI only
   DWORD sVersion = GetFileVersionInfoSize(dllName, &rBuffer);
   if (sVersion==0) return (FALSE);
   versionInfo = new char[sVersion];

   BOOL resultVersion = GetFileVersionInfo(dllName,
                                           NULL,
                                           sVersion,
                                           versionInfo);

   BOOL resultValue = VerQueryValue(versionInfo,  

TEXT("\\StringFileInfo\\040904b0\\ProductVersion"), 
                                    &lpBuffer, 
                                    &cBytes);

   if( resultValue )
   {
      strncpy(vBuffer, (char *) lpBuffer, size);
      delete versionInfo;
      return(TRUE);
   }
   else
   {
      *vBuffer = '\0';
      delete versionInfo;
      return(FALSE);
   }
}




VOID CenterWindow(HWND hwnd)
{
  RECT    rect;
  LONG    dx, dy;
  LONG    dxParent, dyParent;
  LONG    Style;

  GetWindowRect(hwnd, &rect);

  dx = rect.right - rect.left;
  dy = rect.bottom - rect.top;

  Style = GetWindowLong(hwnd, GWL_STYLE);
  if ((Style & WS_CHILD) == 0) 
    {
      dxParent = GetSystemMetrics(SM_CXSCREEN);
      dyParent = GetSystemMetrics(SM_CYSCREEN);
    } 
  else 
    {
      HWND    hwndParent;
      RECT    rectParent;

      hwndParent = GetParent(hwnd);
      if (hwndParent == NULL) 
	{
	  hwndParent = GetDesktopWindow();
	}

      GetWindowRect(hwndParent, &rectParent);

      dxParent = rectParent.right - rectParent.left;
      dyParent = rectParent.bottom - rectParent.top;
    }

  rect.left = (dxParent- dx) / 2;
  rect.top  = (dyParent  - dy) / 3;

  SetWindowPos(hwnd, HWND_TOPMOST, rect.left, rect.top, 0, 0, SWP_NOSIZE);

  SetForegroundWindow(hwnd);
}

int CALLBACK LogonDlgProc(HWND hDlg,UINT Message,WPARAM wParam,LPARAM lParam)
{
	HWND hOk = GetDlgItem(hDlg, IDCUPGRADE);
	  EnableWindow(hOk, mirupgrade);

	hOk = GetDlgItem(hDlg, IDOK);
	  EnableWindow(hOk, newmir);

	hOk = GetDlgItem(hDlg, IDC_UPGRADE_VIR);
	  EnableWindow(hOk, virupgrade);

	hOk = GetDlgItem(hDlg, IDC_NEW_VIR);
	  EnableWindow(hOk, newvir);
	if (OSVersion()==1)
	{
	hOk = GetDlgItem(hDlg, IDC_RVIR);
	  EnableWindow(hOk, (!newvir && F_vir_exist));

	hOk = GetDlgItem(hDlg, IDC_RMIR);
	  EnableWindow(hOk, (!newmir && F_mir_exist));

	hOk = GetDlgItem(hDlg, IDC_UNVIR);
	  EnableWindow(hOk, (!F_vir_exist));

	hOk = GetDlgItem(hDlg, IDC_UNMIR);
	  EnableWindow(hOk, (!F_mir_exist));
	}
	else
	{
		hOk = GetDlgItem(hDlg, IDC_RVIR);
	  EnableWindow(hOk, (!newvir));

	hOk = GetDlgItem(hDlg, IDC_RMIR);
	  EnableWindow(hOk, (!newmir ));

	}

  switch (Message)
    {
    case WM_INITDIALOG:
      CenterWindow(hDlg);
      SetWindowText(hDlg,(const char *)versionText);
      SetWindowLong(hDlg, GWL_USERDATA, lParam);
      return(TRUE);

    case WM_COMMAND:
      if (LOWORD(wParam) == IDCANCEL)
			{
			EndDialog(hDlg, 0);
			}
      if (LOWORD(wParam) == IDOK)
			{
			EndDialog(hDlg, 1);
			}
	  if (LOWORD(wParam) == IDCUPGRADE)
			{
			EndDialog(hDlg, 2);
			}
	  if (LOWORD(wParam) == IDC_UPGRADE_VIR)
			{
			EndDialog(hDlg, 4);
			}
	  if (LOWORD(wParam) == IDC_NEW_VIR)
			{
			EndDialog(hDlg, 3);
			}
	  if (LOWORD(wParam) == IDC_RMIR)
			{
			EndDialog(hDlg, 5);
			}
	  if (LOWORD(wParam) == IDC_RVIR)
			{
			EndDialog(hDlg, 6);
			}
	  if (LOWORD(wParam) == IDC_UNMIR)
			{
			EndDialog(hDlg, 7);
			}
	  if (LOWORD(wParam) == IDC_UNVIR)
			{
			EndDialog(hDlg, 8);
			}
      return(TRUE);
    }

  return(FALSE);

}


int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
 
	newvir=false;
	newmir=false;
	mirupgrade=false;
	virupgrade=false;

	strcpy(driversys1,getenv("SystemRoot"));
	strcat(driversys1,"\\System32\\drivers\\vncdrv.sys");

	strcpy(driversys2,getenv("SystemRoot"));
	strcat(driversys2,"\\System32\\drivers\\vncvirdr.sys");

	strcpy(driverdll1,getenv("SystemRoot"));
	strcat(driverdll1,"\\System32\\vncdrv.dll");

	strcpy(driverdll2,getenv("SystemRoot"));
	strcat(driverdll2,"\\System32\\vncvirdr.dll");

	strcpy(driverhdll1,getenv("SystemRoot"));
	strcat(driverhdll1,"\\System32\\vnchelp.dll");

	strcpy(driverhdll2,getenv("SystemRoot"));
	strcat(driverhdll2,"\\System32\\vncmemdr.dll");
	/////////copy driver files
	strcpy(command1,"copy .\\driver\\vncdrv.dll ");
	strcat(command1,driverdll1);
	strcpy(command2,"copy .\\driver\\vncdrv.sys ");
	strcat(command2,driversys1);
	strcpy(command3,"copy .\\driver\\vnchelp.dll ");
	strcat(command3,driverhdll1);
	strcpy(command4,"copy .\\driver\\vncvirdr.dll ");
	strcat(command4,driverdll2);
	strcpy(command5,"copy .\\driver\\vncvirdr.sys ");
	strcat(command5,driversys2);
	strcpy(command6,"copy .\\driver\\vncmemdr.dll ");
	strcat(command6,driverhdll2);
	//////////remove driver files
	strcpy(command7,"del ");
	strcat(command7,driverdll1);
	strcpy(command8,"del ");
	strcat(command8,driversys1);
	strcpy(command9,"del ");
	strcat(command9,driverhdll1);
	strcpy(command10,"del ");
	strcat(command10,driverdll2);
	strcpy(command11,"del ");
	strcat(command11,driversys2);
	strcpy(command12,"del ");
	strcat(command12,driverhdll2);

	



	if (OSVersion()==1 || OSVersion()==2) //win2000 xp
	{
		int returnvalue=10;
		while (returnvalue!=0)
			{

		{ // INSTALLATION MIRROR DRIVER
		pEnumDisplayDevices pd;
		LPSTR driverName = "Winvnc video hook driver";
		DEVMODE devmode;
		FillMemory(&devmode, sizeof(DEVMODE), 0);
		devmode.dmSize = sizeof(DEVMODE);
		devmode.dmDriverExtra = 0;
		BOOL change = EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&devmode);
		devmode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
		pd = (pEnumDisplayDevices)GetProcAddress( LoadLibrary("USER32"), "EnumDisplayDevicesA");
		BOOL result;
		//check dll versions
			if (GetDllProductVersion("vncdrv.dll",buffer,254))
				{
					F_mir_exist=true;
					if (strcmp(buffer,"1.00.7")==NULL) mirupgrade=false;
					else mirupgrade=true;
				}
			else {mirupgrade=false;F_mir_exist=false;}

			if (GetDllProductVersion("vncvirdr.dll",buffer,254))
				{
					F_vir_exist=true;
					if (strcmp(buffer,"1.00.6")==NULL) virupgrade=false;
					else virupgrade=true;
				}
			else {virupgrade=false;F_vir_exist=false;}
		///////////////miror driver
	    if (pd)
	    {
			DISPLAY_DEVICE dd;
			ZeroMemory(&dd, sizeof(dd));
			dd.cb = sizeof(dd);
			LPSTR deviceName = NULL;
			devmode.dmDeviceName[0] = '\0';
			INT devNum = 0;
			while (result = (*pd)(NULL,devNum, &dd,0))
			{
			if (strcmp((const char *)&dd.DeviceString[0], driverName) == 0)
				break;
			   devNum++;
			}
		}
		else return 0;
		
		if (!result) 
			{
				mirupgrade=false;
				newmir=true;
			}
		else
			{
				newmir=false;
			}
		}	// END MIRROR

		{ // INSTALLATION VIRTUAL DRIVER
			pEnumDisplayDevices pd;
			LPSTR driverName = "vncvirdr";
			DEVMODE devmode;
			FillMemory(&devmode, sizeof(DEVMODE), 0);
			devmode.dmSize = sizeof(DEVMODE);
			devmode.dmDriverExtra = 0;
			BOOL change = EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&devmode);
			devmode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
			pd = (pEnumDisplayDevices)GetProcAddress( LoadLibrary("USER32"), "EnumDisplayDevicesA");
			BOOL result;
			if (pd)
			{
				DISPLAY_DEVICE dd;
				ZeroMemory(&dd, sizeof(dd));
				dd.cb = sizeof(dd);
				LPSTR deviceName = NULL;
				devmode.dmDeviceName[0] = '\0';
				INT devNum = 0;
				while (result = (*pd)(NULL,devNum, &dd,0))
				{
				if (strcmp((const char *)&dd.DeviceString[0], driverName) == 0)
					break;
				devNum++;
			}
			}
			else return 0;


			if (!result) 
			{
				virupgrade=false;
				newvir=true;
			}
			else
			{
				newvir=false;
			}

			returnvalue=10;
			if (OSVersion()==1) returnvalue= DialogBoxParam(hInstance,(LPTSTR) MAKEINTRESOURCE(IDD_MIRROR) ,NULL, (DLGPROC) LogonDlgProc, 0);
			if (OSVersion()==2) returnvalue= DialogBoxParam(hInstance,(LPTSTR) MAKEINTRESOURCE(IDD_MIRROR1) ,NULL, (DLGPROC) LogonDlgProc, 0);

			STARTUPINFO si = { sizeof si };
			PROCESS_INFORMATION pi;
			wchar_t szcmd[256];
			
			if (returnvalue==1) 
				{
					TCHAR *cmd[2];
					cmd[0]=".\\driver\\vncdrv.inf";
					cmd[1]="Winvnc_hook_display_driver";
					cmdInstall("setupdrv.exe",NULL,2,cmd);
					//wsprintf( (char *)szcmd, ".//driver//newmrdrv.exe");
					//CreateProcess( NULL, (char*)szcmd, NULL,NULL,FALSE,NULL,NULL,".//driver",&si,&pi);
					newmir=false;
				}
			if (returnvalue==3) 
				{
					TCHAR *cmd[2];
					cmd[0]=".\\driver\\vdacc.inf";
					cmd[1]="Virtual_video_driver";
					cmdInstall("setupdrv.exe",NULL,2,cmd);
					//wsprintf( (char *)szcmd, ".//driver//newvrdrv.exe");
					//CreateProcess( NULL, (char*)szcmd, NULL,NULL,FALSE,NULL,NULL,".//driver",&si,&pi);
					newvir=false;
				}
			
			if (returnvalue==2) 
				{
					Upgrade_mirdrv();
				}
		
			if (returnvalue==4)
				{
					Upgrade_virdrv();
				}

			if (returnvalue==5)
				{
				if (OSVersion()==1) {Remove_mirdrv();MessageBox(NULL, "Reboot needed before Uninstall", "Ultravnc", MB_OK);}
					if (OSVersion()==2) Remove_mirdrv2();
				}

			if (returnvalue==6)
				{
				if (OSVersion()==1) {Remove_virdrv();MessageBox(NULL, "Reboot needed before Uninstall", "Ultravnc", MB_OK);}
					if (OSVersion()==2) Remove_virdrv2();
				}
			if (returnvalue==7)
				{
					Remove_mirdrv2();
				}

			if (returnvalue==8)
				{
					Remove_virdrv2();
				}

			
			}



		}//END VIRTUAL
	return 0;
	}//END XP
	return 0;
}



