/////////////////////////////////////////////////////////////////////////////
//  Copyright (C) 2002 Ultr@VNC Team Members. All Rights Reserved.
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// If the source code for the program is not available from the place from
// which you received this file, check 
// http://ultravnc.sourceforge.net/
//
/////////////////////////////////////////////////////////////////////////////

#include<windows.h>
#include <stdio.h>
#include <process.h>
#define DBG 1


#define MAXCHANGES_BUF 2000

#define CLIP_LIMIT          50

#define IGNORE 0
#define FROM_SCREEN 1
#define FROM_DIB 2
#define TO_SCREEN 3




#define SCREEN_SCREEN 11
#define BLIT 12
#define SOLIDFILL 13
#define BLEND 14
#define TRANS 15
#define PLG 17
#define TEXTOUT 18


#define BMF_1BPP       1L
#define BMF_4BPP       2L
#define BMF_8BPP       3L
#define BMF_16BPP      4L
#define BMF_24BPP      5L
#define BMF_32BPP      6L
#define BMF_4RLE       7L
#define BMF_8RLE       8L
#define BMF_JPEG       9L
#define BMF_PNG       10L


#define NOCACHE 1
#define OLDCACHE 2
#define NEWCACHE 3
#ifndef VIDEODRIVER
#define VIDEODRIVER

#define CDS_UPDATEREGISTRY  0x00000001
#define CDS_TEST            0x00000002
#define CDS_FULLSCREEN      0x00000004
#define CDS_GLOBAL          0x00000008
#define CDS_SET_PRIMARY     0x00000010
#define CDS_RESET           0x40000000
#define CDS_SETRECT         0x20000000
#define CDS_NORESET         0x10000000

typedef BOOL (WINAPI* pEnumDisplayDevices)(PVOID,DWORD,PVOID,DWORD);
typedef LONG (WINAPI* pChangeDisplaySettingsExA)(LPCSTR,LPDEVMODEA,HWND,DWORD,LPVOID);

//*********************************************************************
// DONT TOUCH STRUCTURES/ SHOULD BE EXACTLE THE SAME IN kernel/app/video
//*********************************************************************
//*********************************************************************
//  RINGBUFFER FOR PASSING RECT CHANGES TO VNV
//*********************************************************************
typedef struct _CHANGES_RECORD
{
	ULONG type;  //screen_to_screen, blit, newcache,oldcache
	RECT rect;
	RECT origrect;
	POINT point;
	ULONG color; //number used in cache array
	ULONG refcolor; //slot used to pase btimap data
}CHANGES_RECORD;
typedef CHANGES_RECORD *PCHANGES_RECORD;
typedef struct _CHANGES_BUF
	{
	 ULONG counter;
	 CHANGES_RECORD pointrect[MAXCHANGES_BUF];
	}CHANGES_BUF;
typedef CHANGES_BUF *PCHANGES_BUF;

typedef struct _GETCHANGESBUF
	{
	 PCHANGES_BUF buffer;
	 PVOID Userbuffer;
	}GETCHANGESBUF;
typedef GETCHANGESBUF *PGETCHANGESBUF;

typedef struct _POINTRECT{
	RECT rect;
	POINT point;
	ULONG color;
	ULONG refcolor;
	ULONG type;
}POINTRECT;
typedef POINTRECT *PPOINTRECT;

typedef struct _POINTRECTARRAY {
	long counter;
	POINTRECT pointrect[MAXCHANGES_BUF];
} POINTRECTARRAY, *PPOINTRECTARRAY;

class vncVideoDriver
{

// Fields
public:

// Methods
public:
	// Make the desktop thread & window proc friends

	vncVideoDriver();
	~vncVideoDriver();
	BOOL Activate_video_driver();
	BOOL Activate_video_driver2();
	void DesActivate_video_driver();
	void DesActivate_video_driver2();
	void MapSharedbuffers();
	void UnMapSharedbuffers();
	void MapSharedbuffers2();
	void UnMapSharedbuffers2();
	BOOL ExistMirrorDriver();
	BOOL ExistVirtualDriver();
	BOOL testdriver();
	BOOL testmapped();
	BOOL IsVirtualDriverActive();
	BOOL IsMirrorDriverActive();

	GETCHANGESBUF bufdata;
	ULONG oldaantal;
	ULONG oldaantal2;
	HDC gdc;
	BOOL driver;
	BOOL blocked;
	HDC GetDcMirror();
	
protected:
		
};
#endif