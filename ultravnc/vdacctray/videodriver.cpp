/////////////////////////////////////////////////////////////////////////////
//  Copyright (C) 2002 Ultr@VNC Team Members. All Rights Reserved.
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// If the source code for the program is not available from the place from
// which you received this file, check 
// http://ultravnc.sourceforge.net/
//
/////////////////////////////////////////////////////////////////////////////

#include "videodriver.h"
#include "vncOSVersion.h"
#define MAP1 1030
#define UNMAP1 1031
#define TESTDRIVER 1050
#define TESTMAPPED 1051

#define MAP2 1070
#define UNMAP2 1071
#define TESTDRIVER2 1060
#define TESTMAPPED2 1061

#define DM_POSITION             0x00000020L

vncVideoDriver::vncVideoDriver()
{
	bufdata.buffer=NULL;
	bufdata.Userbuffer=NULL;
	driver=false;
	blocked=false;
}

vncVideoDriver::~vncVideoDriver()
{
	UnMapSharedbuffers();
	UnMapSharedbuffers2();
	if (IsMirrorDriverActive()) DesActivate_video_driver();
	driver=false;
}
void
vncVideoDriver::MapSharedbuffers()
{
	gdc = GetDC(NULL);
	oldaantal=1;
	oldaantal2=1;
	driver=false;
	if (ExtEscape(gdc, MAP1, 0, NULL, sizeof(GETCHANGESBUF), (LPSTR) &bufdata)>0) driver=true;
}

BOOL
vncVideoDriver::testdriver()
{
	gdc = GetDC(NULL);
	BOOL driver=false;
	int returnvalue;
	returnvalue= ExtEscape(gdc, TESTDRIVER, 0, NULL, sizeof(GETCHANGESBUF), (LPSTR) &bufdata);
	if (returnvalue>0) driver=true;
	return driver;
}

BOOL
vncVideoDriver::testmapped()
{
	/*gdc = GetDC(NULL);
	BOOL driver=false;
	int returnvalue;
	returnvalue= ExtEscape(gdc, TESTMAPPED, 0, NULL, sizeof(GETCHANGESBUF), (LPSTR) &bufdata);
	if (returnvalue==1) driver=true;
	if (returnvalue==0) driver=false;*/
	if (IsBadReadPtr(&bufdata,1)) return FALSE;
	if (bufdata.Userbuffer==NULL) return FALSE;
	if (IsBadReadPtr(&bufdata.Userbuffer,10)) return FALSE;
	return TRUE;
}
void
vncVideoDriver::UnMapSharedbuffers()
{
	ExtEscape(gdc, UNMAP1, 0, NULL, sizeof(GETCHANGESBUF), (LPSTR) &bufdata);
	ReleaseDC(NULL, gdc);

}
void
vncVideoDriver::MapSharedbuffers2()
{
	gdc = GetDC(NULL);
	oldaantal=1;
	oldaantal2=1;
	driver=false;
	ExtEscape(gdc, MAP2, 0, NULL, sizeof(GETCHANGESBUF), (LPSTR) &bufdata);
	if (bufdata.buffer && bufdata.Userbuffer) driver=true;
}
void
vncVideoDriver::UnMapSharedbuffers2()
{
	ExtEscape(gdc, UNMAP2, 0, NULL, sizeof(GETCHANGESBUF), (LPSTR) &bufdata);
	bufdata.buffer=NULL;
	bufdata.Userbuffer=NULL;
	ReleaseDC(NULL, gdc);
}



BOOL
vncVideoDriver::Activate_video_driver()
   {
	HDESK   hdeskInput;
    HDESK   hdeskCurrent;
    int j=0;
	pEnumDisplayDevices pd;
	pChangeDisplaySettingsExA pcdse;
	LPSTR driverName = "Winvnc video hook driver";
	DEVMODE devmode;
    FillMemory(&devmode, sizeof(DEVMODE), 0);
    devmode.dmSize = sizeof(DEVMODE);
    devmode.dmDriverExtra = 0;
    BOOL change = EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&devmode);
    devmode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
	pd = (pEnumDisplayDevices)GetProcAddress( LoadLibrary("USER32"), "EnumDisplayDevicesA");
	pcdse = (pChangeDisplaySettingsExA)GetProcAddress( LoadLibrary("USER32"), "ChangeDisplaySettingsExA");
	if (!pcdse) return false;
    if (pd)
    {
        DISPLAY_DEVICE dd;
        ZeroMemory(&dd, sizeof(dd));
        dd.cb = sizeof(dd);
		LPSTR deviceName = NULL;
        devmode.dmDeviceName[0] = '\0';
        INT devNum = 0;
        BOOL result;
        while (result = (*pd)(NULL,devNum, &dd,0))
        {
          if (strcmp((const char *)&dd.DeviceString[0], driverName) == 0)
              break;

           devNum++;
        }
       
        if (!result)
        {
           printf("No '%s' found.\n", driverName);
           return FALSE;
        }

        printf("DevNum:%d\nName:%s\nString:%s\n\n",devNum,&dd.DeviceName[0],&dd.DeviceString[0]);
		printf("Sreen Settings'%i %i %i'\n",devmode.dmPelsWidth,devmode.dmPelsHeight,devmode.dmBitsPerPel);


		CHAR deviceNum[MAX_PATH];
        strcpy(&deviceNum[0], "DEVICE0");

        HKEY hKeyProfileMirror = (HKEY)0;
        if (RegCreateKey(HKEY_LOCAL_MACHINE,
                        ("SYSTEM\\CurrentControlSet\\Hardware Profiles\\Current\\System\\CurrentControlSet\\Services\\vncdrv"),
                         &hKeyProfileMirror) != ERROR_SUCCESS)
        {
           printf("Can't access registry.\n");
           return FALSE;
        }

        HKEY hKeyDevice = (HKEY)0;
        if (RegCreateKey(hKeyProfileMirror,
                         (&deviceNum[0]),
                         &hKeyDevice) != ERROR_SUCCESS)
        {
           printf("Can't access DEVICE# hardware profiles key.\n");
           return FALSE;
        }

        DWORD one = 1;
        if (RegSetValueEx(hKeyDevice,
                          ("Attach.ToDesktop"),
                          0,
                          REG_DWORD,
                          (unsigned char *)&one,
                          4) != ERROR_SUCCESS)
        {
           printf("Can't set Attach.ToDesktop to 0x1\n");
           return FALSE;
        }

        strcpy((LPSTR)&devmode.dmDeviceName[0], "vncdrv");
        deviceName = (LPSTR)&dd.DeviceName[0];
		// save the current desktop
		// save the current desktop

		hdeskCurrent = GetThreadDesktop(GetCurrentThreadId());
		if (hdeskCurrent != NULL)
			{
				hdeskInput = OpenInputDesktop(0, FALSE, MAXIMUM_ALLOWED);
				if (hdeskInput != NULL)
					{
						SetThreadDesktop(hdeskInput);
					}
			}
		if (devmode.dmBitsPerPel==24) devmode.dmBitsPerPel=16;
        // add 'Default.*' settings to the registry under above hKeyProfile\mirror\device
        INT code =
         (*pcdse)(deviceName,
                                &devmode, 
                                NULL,
                                CDS_UPDATEREGISTRY,
                                NULL
                                );
    
        printf("Update Register on device mode: %i\n", code);

        /*code = (*pcdse)(deviceName,
                                &devmode, 
                                NULL,
                                0,
                                NULL
                                );*/


   // reset desktop
   SetThreadDesktop(hdeskCurrent);


   // close the input desktop
   CloseDesktop(hdeskInput);

		DWORD zero = 1;
        if (RegSetValueEx(hKeyDevice,
                          ("Attach.ToDesktop"),
                          0,
                          REG_DWORD,
                          (unsigned char *)&zero,
                          4) != ERROR_SUCCESS)
        {
           printf("Can't set Attach.ToDesktop to 0x0\n");
           return 1;
        }

        RegCloseKey(hKeyProfileMirror);
        RegCloseKey(hKeyDevice);

      }

   return TRUE;
   }


void
vncVideoDriver::DesActivate_video_driver()
   {
	HDESK   hdeskInput;
    HDESK   hdeskCurrent;
    int j=0;
	pEnumDisplayDevices pd;
	pChangeDisplaySettingsExA pcdse;
	LPSTR driverName = "Winvnc video hook driver";
	DEVMODE devmode;
    FillMemory(&devmode, sizeof(DEVMODE), 0);
    devmode.dmSize = sizeof(DEVMODE);
    devmode.dmDriverExtra = 0;
    BOOL change = EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&devmode);
    devmode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
	pd = (pEnumDisplayDevices)GetProcAddress( LoadLibrary("USER32"), "EnumDisplayDevicesA");
	pcdse = (pChangeDisplaySettingsExA)GetProcAddress( LoadLibrary("USER32"), "ChangeDisplaySettingsExA");
	if (!pcdse) return;
    if (pd)
    {
        DISPLAY_DEVICE dd;
        ZeroMemory(&dd, sizeof(dd));
        dd.cb = sizeof(dd);
		LPSTR deviceName = NULL;
        devmode.dmDeviceName[0] = '\0';
        INT devNum = 0;
        BOOL result;
        while (result = (*pd)(NULL,devNum, &dd,0))
        {
          if (strcmp((const char *)&dd.DeviceString[0], driverName) == 0)
              break;

           devNum++;
        }
       
        if (!result)
        {
           printf("No '%s' found.\n", driverName);
           return;
        }

        printf("DevNum:%d\nName:%s\nString:%s\n\n",devNum,&dd.DeviceName[0],&dd.DeviceString[0]);
		printf("Sreen Settings'%i %i %i'\n",devmode.dmPelsWidth,devmode.dmPelsHeight,devmode.dmBitsPerPel);


		CHAR deviceNum[MAX_PATH];
        strcpy(&deviceNum[0], "DEVICE0");

        HKEY hKeyProfileMirror = (HKEY)0;
        if (RegCreateKey(HKEY_LOCAL_MACHINE,
                        ("SYSTEM\\CurrentControlSet\\Hardware Profiles\\Current\\System\\CurrentControlSet\\Services\\vncdrv"),
                         &hKeyProfileMirror) != ERROR_SUCCESS)
        {
           printf("Can't access registry.\n");
           return;
        }

        HKEY hKeyDevice = (HKEY)0;
        if (RegCreateKey(hKeyProfileMirror,
                         (&deviceNum[0]),
                         &hKeyDevice) != ERROR_SUCCESS)
        {
           printf("Can't access DEVICE# hardware profiles key.\n");
           return;
        }

        DWORD one = 0;
        if (RegSetValueEx(hKeyDevice,
                          ("Attach.ToDesktop"),
                          0,
                          REG_DWORD,
                          (unsigned char *)&one,
                          4) != ERROR_SUCCESS)
        {
           printf("Can't set Attach.ToDesktop to 0x1\n");
           return;
        }

        strcpy((LPSTR)&devmode.dmDeviceName[0], "vncdrv");
        deviceName = (LPSTR)&dd.DeviceName[0];
		// save the current desktop
		// save the current desktop

		hdeskCurrent = GetThreadDesktop(GetCurrentThreadId());
		if (hdeskCurrent != NULL)
			{
				hdeskInput = OpenInputDesktop(0, FALSE, MAXIMUM_ALLOWED);
				if (hdeskInput != NULL)
					{
						SetThreadDesktop(hdeskInput);
					}
			}
        // add 'Default.*' settings to the registry under above hKeyProfile\mirror\device
		blocked=true;
        INT code =
         (*pcdse)(deviceName,
                                &devmode, 
                                NULL,
                                CDS_UPDATEREGISTRY,
                                NULL
                                );
    
        printf("Update Register on device mode: %i\n", code);

        code = (*pcdse)(deviceName,
                                &devmode, 
                                NULL,
                                0,
                                NULL
                                );
		blocked=false;


   // reset desktop
   SetThreadDesktop(hdeskCurrent);


   // close the input desktop
   CloseDesktop(hdeskInput);

		DWORD zero = 0;
        if (RegSetValueEx(hKeyDevice,
                          ("Attach.ToDesktop"),
                          0,
                          REG_DWORD,
                          (unsigned char *)&zero,
                          4) != ERROR_SUCCESS)
        {
           printf("Can't set Attach.ToDesktop to 0x0\n");
           return;
        }

        RegCloseKey(hKeyProfileMirror);
        RegCloseKey(hKeyDevice);

      }

   return;
   }




BOOL
vncVideoDriver::ExistMirrorDriver()
{
pEnumDisplayDevices pd;
	LPSTR driverName = "Winvnc video hook driver";
	DEVMODE devmode;
    FillMemory(&devmode, sizeof(DEVMODE), 0);
    devmode.dmSize = sizeof(DEVMODE);
    devmode.dmDriverExtra = 0;
    BOOL change = EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&devmode);
    devmode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
	pd = (pEnumDisplayDevices)GetProcAddress( LoadLibrary("USER32"), "EnumDisplayDevicesA");
	BOOL result;


    if (pd)
    {
        DISPLAY_DEVICE dd;
        ZeroMemory(&dd, sizeof(dd));
        dd.cb = sizeof(dd);
		LPSTR deviceName = NULL;
        devmode.dmDeviceName[0] = '\0';
        INT devNum = 0;
        while (result = (*pd)(NULL,devNum, &dd,0))
        {
          if (strcmp((const char *)&dd.DeviceString[0], driverName) == 0)
              break;

           devNum++;
        }
	return result;
	}
	else return 0;
}

HDC
vncVideoDriver::GetDcMirror()
{
typedef BOOL (WINAPI* pEnumDisplayDevices)(PVOID,DWORD,PVOID,DWORD);
		HDC m_hrootdc;
		pEnumDisplayDevices pd;
		LPSTR driverName = "Winvnc video hook driver";
		BOOL DriverFound;
		DEVMODE devmode;
		FillMemory(&devmode, sizeof(DEVMODE), 0);
		devmode.dmSize = sizeof(DEVMODE);
		devmode.dmDriverExtra = 0;
		BOOL change = EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&devmode);
		devmode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
		pd = (pEnumDisplayDevices)GetProcAddress( LoadLibrary("USER32"), "EnumDisplayDevicesA");
		if (pd)
			{
				LPSTR deviceName=NULL;
				DISPLAY_DEVICE dd;
				ZeroMemory(&dd, sizeof(dd));
				dd.cb = sizeof(dd);
				devmode.dmDeviceName[0] = '\0';
				INT devNum = 0;
				BOOL result;
				DriverFound=false;
				while (result = (*pd)(NULL,devNum, &dd,0))
				{
					if (strcmp((const char *)&dd.DeviceString[0], driverName) == 0)
					{
					DriverFound=true;
					break;
					}
					devNum++;
				}
				if (DriverFound)
				{
				deviceName = (LPSTR)&dd.DeviceName[0];
				m_hrootdc = CreateDC("DISPLAY",deviceName,NULL,NULL);				
				}
			}

		return m_hrootdc;
}

BOOL
vncVideoDriver::ExistVirtualDriver()
{
pEnumDisplayDevices pd;
	LPSTR driverName = "vncvirdrv";
	DEVMODE devmode;
    FillMemory(&devmode, sizeof(DEVMODE), 0);
    devmode.dmSize = sizeof(DEVMODE);
    devmode.dmDriverExtra = 0;
    BOOL change = EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&devmode);
    devmode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
	pd = (pEnumDisplayDevices)GetProcAddress( LoadLibrary("USER32"), "EnumDisplayDevicesA");
	BOOL result;


    if (pd)
    {
        DISPLAY_DEVICE dd;
        ZeroMemory(&dd, sizeof(dd));
        dd.cb = sizeof(dd);
		LPSTR deviceName = NULL;
        devmode.dmDeviceName[0] = '\0';
        INT devNum = 0;
        while (result = (*pd)(NULL,devNum, &dd,0))
        {
          if (strcmp((const char *)&dd.DeviceString[0], driverName) == 0)
              break;

           devNum++;
        }
	return result;
	}
	else return 0;
}

BOOL
vncVideoDriver::IsMirrorDriverActive()
{
pEnumDisplayDevices pd;
	LPSTR driverName = "Winvnc video hook driver";
	DEVMODE devmode;
    FillMemory(&devmode, sizeof(DEVMODE), 0);
    devmode.dmSize = sizeof(DEVMODE);
    devmode.dmDriverExtra = 0;
    BOOL change = EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&devmode);
    devmode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
	pd = (pEnumDisplayDevices)GetProcAddress( LoadLibrary("USER32"), "EnumDisplayDevicesA");
	BOOL result;


    if (pd)
    {
        DISPLAY_DEVICE dd;
        ZeroMemory(&dd, sizeof(dd));
        dd.cb = sizeof(dd);
		LPSTR deviceName = NULL;
        devmode.dmDeviceName[0] = '\0';
        INT devNum = 0;
        while (result = (*pd)(NULL,devNum, &dd,0))
        {
          if (strcmp((const char *)&dd.DeviceString[0], driverName) == 0)
              break;

           devNum++;
        }
	if (dd.StateFlags & DISPLAY_DEVICE_ATTACHED_TO_DESKTOP) return 1;
	else return 0;
	}
	return 0;
}

BOOL
vncVideoDriver::IsVirtualDriverActive()
{
pEnumDisplayDevices pd;
	LPSTR driverName = "vncvirdrv";
	DEVMODE devmode;
    FillMemory(&devmode, sizeof(DEVMODE), 0);
    devmode.dmSize = sizeof(DEVMODE);
    devmode.dmDriverExtra = 0;
    BOOL change = EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&devmode);
    devmode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
	pd = (pEnumDisplayDevices)GetProcAddress( LoadLibrary("USER32"), "EnumDisplayDevicesA");
	BOOL result;


    if (pd)
    {
        DISPLAY_DEVICE dd;
        ZeroMemory(&dd, sizeof(dd));
        dd.cb = sizeof(dd);
		LPSTR deviceName = NULL;
        devmode.dmDeviceName[0] = '\0';
        INT devNum = 0;
        while (result = (*pd)(NULL,devNum, &dd,0))
        {
          if (strcmp((const char *)&dd.DeviceString[0], driverName) == 0)
              break;

           devNum++;
        }
	if (dd.StateFlags & DISPLAY_DEVICE_ATTACHED_TO_DESKTOP) return 1;
	else return 0;
	}
	return 0;
}
void
vncVideoDriver::DesActivate_video_driver2()
   {
	HDESK   hdeskInput;
    HDESK   hdeskCurrent;
    int j=0;
	pEnumDisplayDevices pd = NULL;
	LPSTR driverName = "vncvirdrv";
	DEVMODE devmode,devreset;
    FillMemory(&devmode, sizeof(DEVMODE), 0);
    devmode.dmSize = sizeof(DEVMODE);
    devmode.dmDriverExtra = 0;
	FillMemory(&devreset, sizeof(DEVMODE), 0);
    devreset.dmSize = sizeof(DEVMODE);
    devreset.dmDriverExtra = 0;
	UnMapSharedbuffers2();

	// save the current desktop
   hdeskCurrent = GetThreadDesktop(GetCurrentThreadId());
   if (hdeskCurrent == NULL)
   {
	   
       return ;
   }

   // determine the current input desktop
   hdeskInput = OpenInputDesktop(0, FALSE, MAXIMUM_ALLOWED);
   if (hdeskInput == NULL)
   {
	   
       return ;
   }

   // set the thread desktop
   if (!SetThreadDesktop(hdeskInput))
   {
	   
   }


    BOOL change = EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&devmode);
    devmode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
	pd = (pEnumDisplayDevices)GetProcAddress( LoadLibrary("USER32"), "EnumDisplayDevicesA");
	

	if (pd)
    {
        DISPLAY_DEVICE dd;
        ZeroMemory(&dd, sizeof(dd));
        dd.cb = sizeof(dd);
		LPSTR deviceName = NULL;
        devmode.dmDeviceName[0] = '\0';
        INT devNum = 0;
        BOOL result;
        while (result = (*pd)(NULL,devNum, &dd,0))
        {
          if (strcmp((const char *)&dd.DeviceString[0], driverName) == 0)
              break;

           devNum++;
        }
       
        if (!result)
        {
           printf("No '%s' found.\n", driverName);
           return;
        }

        
		CHAR deviceNum[MAX_PATH];
        strcpy(&deviceNum[0], "DEVICE0");

        strcpy((LPSTR)&devmode.dmDeviceName[0], "vncvirdrv");
        deviceName = (LPSTR)&dd.DeviceName[0];
		devreset.dmBitsPerPel=4;
		devreset.dmPelsHeight=480;
		devreset.dmPelsWidth=640;
		devreset.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT; 
        // add 'Default.*' settings to the registry under above hKeyProfile\mirror\device
        //INT code = ChangeDisplaySettings(&devreset,0);
		INT code = ChangeDisplaySettings(NULL,0);
                                

      }


   // reset desktop
   SetThreadDesktop(hdeskCurrent);
   // close the input desktop
   CloseDesktop(hdeskInput);


   return ;
   
   }


BOOL
vncVideoDriver::Activate_video_driver2()
   {
	HDESK   hdeskInput;
    HDESK   hdeskCurrent;
    int j=0;
	pEnumDisplayDevices pd;
	pChangeDisplaySettingsExA pcdse;
	LPSTR driverName = "vncvirdrv";
	DEVMODE devmode;
    FillMemory(&devmode, sizeof(DEVMODE), 0);
    devmode.dmSize = sizeof(DEVMODE);
    devmode.dmDriverExtra = 0;
    BOOL change = EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&devmode);
    devmode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT|DM_POSITION;
	pd = (pEnumDisplayDevices)GetProcAddress( LoadLibrary("USER32"), "EnumDisplayDevicesA");
    devmode.dmPosition.x = devmode.dmPelsWidth;
    devmode.dmPosition.y = devmode.dmPelsHeight;
	pcdse = (pChangeDisplaySettingsExA)GetProcAddress( LoadLibrary("USER32"), "ChangeDisplaySettingsExA");
	if (!pcdse) return false;

    if (pd)
    {
        DISPLAY_DEVICE dd;
        ZeroMemory(&dd, sizeof(dd));
        dd.cb = sizeof(dd);
		LPSTR deviceName = NULL;
        devmode.dmDeviceName[0] = '\0';
        INT devNum = 0;
        BOOL result;
        while (result = (*pd)(NULL,devNum, &dd,0))
        {
          if (strcmp((const char *)&dd.DeviceString[0], driverName) == 0)
              break;

           devNum++;
        }
       
        if (!result)
        {
           printf("No '%s' found.\n", driverName);
           return FALSE;
        }

        printf("DevNum:%d\nName:%s\nString:%s\n\n",devNum,&dd.DeviceName[0],&dd.DeviceString[0]);
		printf("Sreen Settings'%i %i %i'\n",devmode.dmPelsWidth,devmode.dmPelsHeight,devmode.dmBitsPerPel);


		CHAR deviceNum[MAX_PATH];
        strcpy(&deviceNum[0], "DEVICE0");

        HKEY hKeyProfileMirror = (HKEY)0;
        if (RegCreateKey(HKEY_LOCAL_MACHINE,
                        ("SYSTEM\\CurrentControlSet\\Hardware Profiles\\Current\\System\\CurrentControlSet\\Services\\vncvirdrv"),
                         &hKeyProfileMirror) != ERROR_SUCCESS)
        {
           printf("Can't access registry.\n");
           return FALSE;
        }

        HKEY hKeyDevice = (HKEY)0;
        if (RegCreateKey(hKeyProfileMirror,
                         (&deviceNum[0]),
                         &hKeyDevice) != ERROR_SUCCESS)
        {
           printf("Can't access DEVICE# hardware profiles key.\n");
           return FALSE;
        }

        DWORD one = 1;
        if (RegSetValueEx(hKeyDevice,
                          ("Attach.ToDesktop"),
                          0,
                          REG_DWORD,
                          (unsigned char *)&one,
                          4) != ERROR_SUCCESS)
        {
           printf("Can't set Attach.ToDesktop to 0x1\n");
           return FALSE;
        }

        strcpy((LPSTR)&devmode.dmDeviceName[0], "vncvirdrv");
        deviceName = (LPSTR)&dd.DeviceName[0];
		// save the current desktop
		// save the current desktop
   hdeskCurrent = GetThreadDesktop(GetCurrentThreadId());
   if (hdeskCurrent == NULL)
   {
	   
       return FALSE;
   }

   // determine the current input desktop
   hdeskInput = OpenInputDesktop(0, FALSE, MAXIMUM_ALLOWED);
   if (hdeskInput == NULL)
   {
	   
       return FALSE;
   }

   // set the thread desktop
   if (!SetThreadDesktop(hdeskInput))
   {
	   
       
   }
        // add 'Default.*' settings to the registry under above hKeyProfile\mirror\device
        INT code =
        (*pcdse)(deviceName,
                                &devmode, 
                                NULL,
                                CDS_UPDATEREGISTRY,
                                NULL
                                );
    
        printf("Update Register on device mode: %i\n", code);

        code = (*pcdse)(deviceName,
                                &devmode, 
                                NULL,
                                0,
                                NULL
                                );


   // reset desktop
   SetThreadDesktop(hdeskCurrent);


   // close the input desktop
   CloseDesktop(hdeskInput);

		DWORD zero = 0;
        if (RegSetValueEx(hKeyDevice,
                          ("Attach.ToDesktop"),
                          0,
                          REG_DWORD,
                          (unsigned char *)&zero,
                          4) != ERROR_SUCCESS)
        {
           printf("Can't set Attach.ToDesktop to 0x0\n");
           return 1;
        }

        RegCloseKey(hKeyProfileMirror);
        RegCloseKey(hKeyDevice);

      }
  // MapSharedbuffers2();
   return TRUE;
   }