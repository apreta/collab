/* copyright 2007 SiteScape */

#ifdef WIN32

#include <windows.h>
#include <time.h>
#include <wininet.h>

#else

#define _GNU_SOURCE
#include <stdio.h>          // printf
#include <stdlib.h>
#include <string.h>
#include <glib.h>

#endif      //  #ifdef WIN32

#include "nethttp.h"
#include "http.h"
#include "httpbuffer.h"

#include <assert.h>

#define errorf printf
#define debugf printf

#ifndef WIN32
typedef unsigned long               DWORD;
#endif


/*

Protocol:

 Connect URL: /connect
	Returns <host id>/<check>

 Close URL: /close/<host#>/<check>

 Data exchangel URL: /data/<host#>/<check>/<in-sequence>/<out-sequence>

 in-sequence = position in "up" stream of first byte in this request
 out-sequence = position in "down" stream that client expects to receive
                (= position in "down" stream of last byte in last response + 1)

 If client does not receive a response, it will re-send the same request with
 same sequence numbers again.  The server will ignore the request data if it
 has already sent data to host; and will generate a new response (which may
 include more data if more is pending for client).

Poll strategy:
  If data was received, try again immediately
  If no data, start counter and use fast poll interval
  If no data and counter exceeded, use slow poll interval

Improvements:
  Use async functions to stream data better (might not work well for all proxies)
  Hash sequence values to make protocol harder to spoof (minimally check source address is unchanged)

*/

struct _HTTPINFO
{
#ifdef WIN32
	HINTERNET   hInternetSession;
    HINTERNET   hConnection;
#else
    SoupSession  *hInternetSession;
    SuperMessage *hConnection;
#endif
    long        last;

    BOOL    non_blocking;
    BOOL    main_thread;
    void    (*poll_hook)();
	BOOL    closing;
	int     read_timeout;	/* 0 = no timeout */

    BOOL    sending;
	BOOL    fast_poll;
	int     empty_count;

	char    server[512];
	char    id[32];

	BUFFER  *recv_queue;
	BUFFER  *send_queue;

	long    in_sequence;
	long    out_sequence;

#ifdef WIN32
    CRITICAL_SECTION lock;
#else
    GStaticRecMutex lock;
#endif
};


#ifdef WIN32
static int CheckStatus(HTTPINFO * info, HINTERNET hURL);
static int BufferResult(HTTPINFO * info, HINTERNET hURL);
#else
static int CheckStatus(HTTPINFO * info, SuperMessage *hURL);
static int BufferResult(HTTPINFO * info, SuperMessage *hURL);
#endif

static int SendRequest(HTTPINFO * info);
static BOOL RetryRequest(DWORD error);


//*****************************************************************************
void HttpInit(HTTPINFO ** info)
{
    *info = calloc( sizeof(HTTPINFO), 1 );
#ifdef WIN32
    InitializeCriticalSection( &(*info)->lock );
#else
    g_static_rec_mutex_init(&(*info)->lock);
#endif
}


//*****************************************************************************
void HttpSetNonBlock(HTTPINFO * info, BOOL flag)
{
	info->non_blocking = flag;
}


//*****************************************************************************
void HttpSetTimeout(HTTPINFO * info, int timeout)
{
	info->read_timeout = timeout;
}


//*****************************************************************************
void HttpSetThreadMode(HTTPINFO * info, BOOL main_thread, void (*polling_hook)())
{
    info->main_thread = main_thread;
	info->poll_hook = polling_hook;
}


//*****************************************************************************
// Open HTTP tunnel to server.
// Will block until suceeds or times out.
int HttpConnect(HTTPINFO * info, const char *server)
{
#ifdef WIN32
	HINTERNET   hURL    = NULL;
#else
    SuperMessage *hURL   = NULL;
#endif

    char object[128];
	char out[128];
	DWORD dwError;
	BOOL result;
    int timeout = HTTP_RECV_TIMEOUT/1000;
    INTERNET_BUFFERSA   Output;

    debugf( "Establishing HTTP tunnel to *%s*\r\n", server );

	strcpy(info->server, server);

    info->hInternetSession = netHttpOpen( "ZonClient", timeout, true );

	if (info->hInternetSession == NULL)
	{
        errorf("netHttpOpen failed [%d]\r\n", (int)GetLastError());
		return -1;
	}

	// Open connection to server.
    info->hConnection = netHttpConnect( info->hInternetSession,
                                        server,
                                        server,
                                        INTERNET_DEFAULT_HTTP_PORT,
                                        NULL, NULL );
	if (info->hConnection == NULL)
	{
		errorf("InternetConnect failed [%s, %d]\r\n", server, (int)GetLastError());
		goto failed;
	}

	// Make connection to desired page.
    sprintf(object, "/connect");
    hURL = netHttpOpenRequest( info->hConnection,
                               "POST", object,
                               "HTTP/1.1",
                               INTERNET_FLAG_NO_CACHE_WRITE|INTERNET_FLAG_PRAGMA_NOCACHE|INTERNET_FLAG_RELOAD|INTERNET_FLAG_KEEP_CONNECTION );

	if (hURL == NULL)
	{
        errorf("netHttpOpenRequest( ) failed [%d]\r\n", (int)GetLastError());
		goto failed;
    }

#ifndef WIN32
    sprintf(object, "Connection: Keep-Alive");
    netHttpAddRequestHeaders( hURL, object, strlen( object ) );
    sprintf(object, "Cache-Control: no-cache");
    netHttpAddRequestHeaders( hURL, object, strlen( object ) );
    sprintf(object, "Content-Length: 0");
    netHttpAddRequestHeaders( hURL, object, strlen( object ) );
#endif

	while (TRUE)
	{
        result = netHttpSendRequest( hURL, info->hInternetSession,
                                  NULL, 0,
                                  NULL, 0);
		if (result == FALSE)
		{
            errorf("netHttpSendRequest failed [%d]\r\n", (int)GetLastError());
			goto failed;
		}

        dwError = netInternetErrorDlg( hURL, GetLastError( ) );
		if (dwError == ERROR_INTERNET_FORCE_RETRY)
			continue;

		break;
	}

	// Check status
	if (CheckStatus(info, hURL) < 0)
		goto failed;

	// Get result.
    memset( &Output, 0, sizeof(Output) );
    Output.dwStructSize = sizeof(Output);
    Output.lpvBuffer = out;
    Output.Next = &Output;
    Output.dwBufferLength = sizeof(out);

    result = netInternetReadFileExA( hURL, &Output );
	if (result == FALSE)
	{
        errorf("netInternetReadFileExA( ) failed [%d]\r\n", (int)GetLastError());
		goto failed;
    }
    if( Output.dwBufferLength <= 0 )
	{
        errorf("netInternetReadFileExA( ) no data returned from connection\r\n");
		goto failed;
	}

    strncat(info->id, out, Output.dwBufferLength < sizeof(info->id) ? Output.dwBufferLength : sizeof(info->id));
	errorf("Connect HTTP session %s\r\n", info->id);

    netInternetCloseHandle(hURL);

	return 0;

failed:
	if (hURL != NULL)
        netInternetCloseHandle( hURL );

    netHttpClose( info->hInternetSession, info->hConnection );
    info->hConnection = NULL;
    info->hInternetSession = NULL;

	return -1;
}


//*****************************************************************************
// Send data
// Currently just adds to outgoing buffer list for sending during a receive or
// check readable operation.
int HttpSend(HTTPINFO * info, char * data, int len)
{
	int status;
	BOOL was_empty = info->send_queue == NULL;
    time_t  tm;

#ifdef WIN32
    EnterCriticalSection(&info->lock);
#else
    g_static_rec_mutex_lock(&info->lock);
#endif
	status = FillBuffer(&info->send_queue, data, len);
#ifdef WIN32
	LeaveCriticalSection(&info->lock);
#else
    g_static_rec_mutex_unlock(&info->lock);
#endif

    time( &tm );
    debugf("(%d/%d) Prepared %d bytes\r\n", (int)(_clock()/1000), (int)tm, len );

	if( len > 0  &&  was_empty  &&  !info->fast_poll )
	{
		/* Set to fast poll so receive/check on another thread will send soon */
		info->empty_count = 0;
		info->last = 0; //_clock() / (CLOCKS_PER_SEC/1000);
		info->fast_poll = TRUE;
	}

	return status;
}


//*****************************************************************************
// Receive data
// Non-blocking symantics not quite the same as regular socket as we will
// block for a bit to send a request off to the server.
int HttpRecv(HTTPINFO * info, char * data, int len)
{
	int status;
	int read;

	if (!info->non_blocking)
	{
		// Connection timeout keeps blocking calls from waiting indefinitely.
		int timeout = (info->read_timeout == 0) ? -1 : info->read_timeout/1000;

		status = HttpCheckReadable(info, timeout);
		if (status < 0)
			return status;
	}

	if (info->closing)
		return -1;

	// Check for buffered data
#ifdef WIN32
    EnterCriticalSection(&info->lock);
#else
    g_static_rec_mutex_lock(&info->lock);
#endif
	read = ReadBuffer(&info->recv_queue, data, len);
#ifdef WIN32
    LeaveCriticalSection(&info->lock);
#else
    g_static_rec_mutex_unlock(&info->lock);
#endif
	if (read > 0)
	{
		debugf("Unbuffered %d bytes\r\n", read);
		return read;
	}

	status = SendRequest(info);
	if (status < 0)
		return status;

	// Read buffer.
#ifdef WIN32
    EnterCriticalSection(&info->lock);
#else
    g_static_rec_mutex_lock(&info->lock);
#endif
	read = ReadBuffer(&info->recv_queue, data, len);
#ifdef WIN32
	LeaveCriticalSection(&info->lock);
#else
    g_static_rec_mutex_unlock(&info->lock);
#endif
	debugf("Unbuffered %d bytes\r\n", read);
	return read;
}


//*****************************************************************************
// See if there is data to read.
// If timeout is non-zero, will wait for data to become available.
// -1 waits forever.
int HttpCheckReadable( HTTPINFO *info, int timeout )
{
	BUFFER  *cur;
    long    now         = _clock() / (CLOCKS_PER_SEC/1000);
    long    expires     = now + (timeout*1000) + 1;         // make sure we get through once
	int status;

	/* Already data in queue, return immediately */
	cur = info->recv_queue;
    if( cur != NULL )
    {
        return 1;
    }

    debugf("now = %ld, expires = %ld, last = %ld\n", now, expires, info->last);

    while( !info->closing  &&  (timeout == -1 || now < expires))
	{
		/* Use poll interval to determine when to send request */
        if (info->last == 0 || (now - info->last > (info->fast_poll ? HTTP_FAST_POLL_TIME : HTTP_SLOW_POLL_TIME)))
		{
			status = SendRequest(info);
            if (status < 0)
            {
                return status;
            }

			/* Didn't send all outbound data, send another request */
			if (info->send_queue != NULL)
			{
                info->last = 0;
				continue;
			}
		}
        else
        {
            if (info->poll_hook)
                info->poll_hook();
            sleep_millis( 100 );
        }

		cur = info->recv_queue;
        if( cur  !=  NULL )
        {
            return 1;
        }

		now = _clock() / (CLOCKS_PER_SEC/1000);
	}

	return 0;
}


//*****************************************************************************
// See if we can write data.
// Since we just write into buffer, this always returns true.
int HttpCheckWriteable(HTTPINFO * info, int timeout)
{
	// *** check number bytes waiting?
	return 1;
}


//*****************************************************************************
// Try to perform clean shutdown of tunnel.
// Server will also disconnect us if it hasn't received any recent requests.
int HttpClose(HTTPINFO * info, BOOL clean_shutdown)
{
#ifdef WIN32
    HINTERNET   hURL  = NULL;
#else
    SuperMessage *hURL = NULL;
#endif
	char object[128];
	BUFFER * cur;

	if (info->closing)
		return 0;

	// Not the best logic, but try to force blocking receive to exit.
	info->closing = TRUE;

	if (*info->id && clean_shutdown)
	{

		// Open connection to server.
		if (info->hConnection == NULL)
		{
            info->hConnection = netHttpConnect( info->hInternetSession,
                                                info->server,
                                                info->server,
                                                INTERNET_DEFAULT_HTTP_PORT,
                                                NULL, NULL );
			if (info->hConnection == NULL)
			{
                errorf("netHttpConnect( ) failed [%d]\r\n", (int)GetLastError());
				return -1;
			}
		}

		// Try to cleanly shutdown session.
        sprintf(object, "/close/%s", info->id);
        hURL = netHttpOpenRequest( info->hConnection,
                                    "POST", object,
                                    "HTTP/1.1",
                                    INTERNET_FLAG_NO_CACHE_WRITE|INTERNET_FLAG_PRAGMA_NOCACHE|INTERNET_FLAG_RELOAD|INTERNET_FLAG_KEEP_CONNECTION );
		if (hURL != NULL)
		{
            int count = 5;

            netHttpSendRequest( hURL, info->hInternetSession,
                                NULL, 0, NULL, 0 );

			// Ignore result, but give it a chance to get written
            while (--count)
            {
                netHttpQueryResultsReady( hURL, info->main_thread );
                sleep_millis(50);
            }

            netInternetCloseHandle( hURL );
		}
	}

	// Clean up on this end.
    netHttpClose( info->hInternetSession, info->hConnection );
    info->hConnection = NULL;

#ifdef WIN32
    EnterCriticalSection(&info->lock);
#else
    g_static_rec_mutex_lock(&info->lock);
#endif
	cur = info->recv_queue;
	while( cur  !=  NULL )
	{
		cur = RemoveBuffer( &info->recv_queue );
	}
#ifdef WIN32
	LeaveCriticalSection(&info->lock);

	DeleteCriticalSection(&info->lock);
#else
    g_static_rec_mutex_unlock_full(&info->lock);

    g_static_rec_mutex_free(&info->lock);
#endif

    free( info );

	return 0;
}


//*****************************************************************************
// Send data request to server.
static int SendRequest(HTTPINFO * info)
{
#ifdef WIN32
    HINTERNET   hURL  = NULL;
#else
    SuperMessage *hURL = NULL;
#endif
	char object[128];
	char out[SEND_BUFF_SIZE];
	int len;
	int retries = 0;
	BOOL result;
	static long counter = 0;
    time_t  tm;
    DWORD timeout = HTTP_RECV_TIMEOUT;

	assert(!info->sending);

	// Get any outbound data.
#ifdef WIN32
    EnterCriticalSection(&info->lock);
#else
    g_static_rec_mutex_lock(&info->lock);
#endif
	len = ReadBuffer(&info->send_queue, out, sizeof(out));
#ifdef WIN32
    LeaveCriticalSection(&info->lock);
#else
    g_static_rec_mutex_unlock(&info->lock);
#endif
	if (len < 0)
		len = 0;

    time( &tm );
    debugf("(%d/%d) Sending %d bytes\r\n", (int)(_clock()/1000), (int)tm, len );

retry:
	// Generate request URL from current stream position and unique id.
	// The unique id keeps the response from being cached.
	sprintf(object, "/data/%s/%ld/%ld/%ld", info->id, info->in_sequence, info->out_sequence, ++counter);

	// Close connections on retry
	if (retries++)
	{
        time_t  tm;

		netInternetCloseHandle( hURL );
        netInternetCloseHandle( info->hConnection );
		info->hConnection = NULL;

        time( &tm );
        debugf("(%d/%d) RETRY # %d to send %d bytes\r\n", (int)(_clock()/1000), (int)tm, retries, len );
	}

	// Open connection to server.
	if (info->hConnection == NULL)
	{
        info->hConnection = netHttpConnect( info->hInternetSession,
                                            info->server,
                                            info->server,
                                            INTERNET_DEFAULT_HTTP_PORT,
                                            NULL, NULL );
		if (info->hConnection == NULL)
		{
            errorf("netHttpConnect( ) failed [%d]\r\n", (int)GetLastError());
			return -1;
		}
	}

	// Make connection to desired page.
    hURL = netHttpOpenRequest( info->hConnection,
                                "POST", object,
                                "HTTP/1.1",
                                INTERNET_FLAG_NO_CACHE_WRITE|INTERNET_FLAG_PRAGMA_NOCACHE|INTERNET_FLAG_RELOAD|INTERNET_FLAG_KEEP_CONNECTION );
	if (hURL == NULL)
	{
        errorf("netHttpOpenRequest( b ) failed [%d]\r\n", (int)GetLastError());
		return -1;
	}

	info->sending = TRUE;

#ifdef WIN32

    // Set timeout...may not work on all versions of wininet, but I don't expect
	// remove server to hang, I expect packet loss, in which case we should get
    // a connection error back.
    InternetSetOption(hURL, INTERNET_OPTION_RECEIVE_TIMEOUT, (LPVOID)&timeout, sizeof(DWORD));

#else

    // Explicitly add content-length header if length is 0 so that it gets sent
    if (len == 0)
    {
        char    outBuf[ MAX_PATH ];
        sprintf( outBuf, "Content-Length: %d", len );
        netHttpAddRequestHeaders( hURL, outBuf, strlen( outBuf ) );
    }

#endif

    result = netHttpSendRequest( hURL, info->hInternetSession,
                                 NULL, 0, out, len);
	if (result == FALSE)
	{
		DWORD err = GetLastError();
        errorf("netHttpSendRequest( ) failed [%d]\r\n", (int)err);
		if (retries < HTTP_REQUEST_RETRIES && RetryRequest(err))
        {
            sleep_millis( 500 );
			goto retry;
		}

		goto failed;
	}

	// Check status
	if (CheckStatus(info, hURL) < 0)
		goto failed;

	// Buffer any result
	if (BufferResult(info, hURL) < 0)
	{
		DWORD err = GetLastError();
		if (retries < HTTP_REQUEST_RETRIES && RetryRequest(err))
		{
			goto retry;
		}

		goto failed;
	}

	// Keep track of our position
	info->in_sequence += len;

    netInternetCloseHandle( hURL );

	debugf("Sent %d outgoing bytes\r\n", len);

	info->sending = FALSE;
	return 0;

failed:
	if (hURL)
        netInternetCloseHandle( hURL );
	info->sending = FALSE;
	return -1;
}


//*****************************************************************************
#ifdef WIN32
static void DumpHeaders(HINTERNET hURL)
#else
static void DumpHeaders(SuperMessage *hURL)
#endif
{
	LPVOID lpOutBuffer=NULL;
	DWORD dwSize = 0;

	for (;;)
	{
		// This call will fail on the first pass, because
		// no buffer is allocated.
        if(!netHttpQueryInfo( hURL, HTTP_QUERY_RAW_HEADERS_CRLF,
                              (LPVOID) lpOutBuffer, &dwSize, NULL ) )
		{
			if (GetLastError()==ERROR_HTTP_HEADER_NOT_FOUND)
			{
				// Code to handle the case where the header isn't available.
				debugf("HTTP headers not found\r\n");
				return;
			}
			else
			{
				// Check for an insufficient buffer.
				if (GetLastError()==ERROR_INSUFFICIENT_BUFFER)
				{
					// Allocate the necessary buffer.
					lpOutBuffer = calloc(dwSize,1);
					continue;
				}
				else
				{
					// Error handling code.
					debugf("HTTP headers not fetched\r\n");
					return;
				}
			}
		}
		break;
	}

	debugf("HTTP headers: %s\r\n", (char *)lpOutBuffer);
	free(lpOutBuffer);
}


//*****************************************************************************
// Check status from HTTP response:
//   200 for good connection
//   <anything else> if connection failed
#ifdef WIN32
static int CheckStatus(HTTPINFO * info, HINTERNET hURL)
#else
static int CheckStatus(HTTPINFO * info, SuperMessage *hURL)
#endif
{
	DWORD dwCode, dwSize;
    char    cCode[ MAX_PATH ];
    dwSize = MAX_PATH - 1;

	debugf("Checking status\r\n");

#ifndef WIN32
    int     iLC = 0;
    while( !netHttpQueryResultsReady( hURL, info->main_thread ) )
    {
        if (info->poll_hook)
            info->poll_hook();
        sleep_millis( 50 );
        iLC++;
        if( iLC > (HTTP_RECV_TIMEOUT/50) )
        {
            errorf("    checked failed - looped %d times\r\n", iLC );
            return -1;
        }
    }
    debugf("    check done - looped %d times\r\n", iLC );
#endif

    if (!netHttpQueryInfo( hURL,
                           HTTP_QUERY_STATUS_CODE,
                           cCode, &dwSize, NULL))
	{
        errorf("netHttpQueryInfo failed [%d]\r\n", (int)GetLastError());
		return -1;
	}

    dwCode = atoi( cCode );
	if (dwCode < 200 || dwCode > 299)
	{
		errorf("Invalid status in HTTP response: %d\r\n", (int)dwCode);
		DumpHeaders(hURL);
		return -1;
	}
	if (dwCode != 200)
	{
		debugf("Unexpected HTTP status code %d, continuing\r\n", (int)dwCode);
		DumpHeaders(hURL);
	}

	return 0;
}


//*****************************************************************************
#ifdef WIN32
static int GetLength(HINTERNET hURL)
#else
static int GetLength(SuperMessage *hURL)
#endif
{
	DWORD dwLen, dwSize;
    char    cCode[ MAX_PATH ];
    dwSize = MAX_PATH - 1;

    if (!netHttpQueryInfo( hURL,
                           HTTP_QUERY_CONTENT_LENGTH,
                           cCode, &dwSize, NULL))
	{
        errorf("netHttpQueryInfo for Content-Length failed [%d]\r\n", (int)GetLastError());
		return 0;
	}

    dwLen = atoi( cCode );
	return dwLen;
}


//*****************************************************************************
// Read response data and add to response queue.
#ifdef WIN32
static int BufferResult(HTTPINFO * info, HINTERNET hURL)
#else
static int BufferResult(HTTPINFO * info, SuperMessage *hURL)
#endif
{
	char out[HTTP_BUFF_SIZE];
	DWORD dwBytes = 0;
	BOOL result;
	int numRead = 0;
	int readLen = 0;

	debugf("Reading response\r\n");

	// Read response length (we only handle response with explicit length)
	readLen = GetLength(hURL);

	while (numRead < readLen)
	{
		int remaining = readLen - numRead;

		// Get result.
        INTERNET_BUFFERSA   Output;
        memset( &Output, 0, sizeof(Output) );
        Output.dwStructSize = sizeof(Output);
        Output.lpvBuffer = out;
        Output.Next = &Output;
        Output.dwBufferLength = remaining > sizeof(out) ? sizeof(out) : remaining;

        result = netInternetReadFileExA( hURL, &Output );
		if (result == FALSE)
		{
            errorf("netInternetReadFileExA( ) failed [%d]\r\n", (int)GetLastError());
			return -1;
        }
        dwBytes = Output.dwBufferLength;
		if (dwBytes > 0)
		{
			BUFFER * buff = calloc(1, sizeof(BUFFER));

#ifdef WIN32
			EnterCriticalSection(&info->lock);
#else
            g_static_rec_mutex_lock(&info->lock);
#endif
			memcpy(buff->data, out, dwBytes);
			buff->len = dwBytes;
			InsertBuffer(&info->recv_queue, buff);

#ifdef WIN32
			LeaveCriticalSection(&info->lock);
#else
            g_static_rec_mutex_unlock(&info->lock);
#endif
			// Got data, reset counters
			info->empty_count = 0;
            info->last = 0;
			info->fast_poll = TRUE;

			numRead += dwBytes;

			debugf("Received %d bytes\r\n", (int)dwBytes);
		}
	}

	if (numRead == 0)
	{
		// No data, start counters
        info->last = _clock() / (CLOCKS_PER_SEC/1000);
		if (++info->empty_count >= HTTP_START_SLOW_COUNT)
		{
			info->fast_poll = FALSE;
		}
	}

	info->out_sequence += numRead;

	debugf("Received %d bytes from server\r\n", numRead);

	return 0;
}


//*****************************************************************************
static BOOL RetryRequest(DWORD error)
{
	switch (error)
	{
	case ERROR_INTERNET_TIMEOUT:
	case ERROR_INTERNET_CANNOT_CONNECT:
	case ERROR_INTERNET_CONNECTION_ABORTED:
	case ERROR_INTERNET_CONNECTION_RESET:
		return TRUE;
	}

	return FALSE;
}
