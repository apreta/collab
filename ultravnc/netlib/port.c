/* copyright 2007 SiteScape */

#include <stdio.h>

#include "port.h"

void _set_nonblock(int fd, int act)
{
#ifdef LINUX
	int flags;

	flags = fcntl(fd, F_GETFL, 0);
	if(flags < 0) return;

	if(act)
		flags |= O_NONBLOCK;
	else
		flags &= ~O_NONBLOCK;

	fcntl(fd, F_SETFL, flags);
#else // _WIN32
	unsigned long flag = act;
	ioctlsocket(fd, FIONBIO, &flag);
#endif
}

void _set_nodelay(int fd, int act)
{
    setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (const char *) &act, sizeof(int));
}

int _gettimeofday(struct timeval * tv)
{
#ifdef LINUX
	struct timezone tz;
	return gettimeofday(tv, &tz);
#else
	time_t now;

	time(&now);

	tv->tv_sec = now;
	tv->tv_usec = 0;
	return 0;
#endif
}

#ifdef LINUX
// Return microseconds of elapsed time
long _clock()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * 1000000 + tv.tv_usec;
}

int sleep_millis(int ms)
{
	struct timespec tv = { ms / 1000, ms % 1000 * 1000000 };
	nanosleep(&tv, NULL);
	return 0;
}
#endif

#ifdef _WIN32
int inet_aton(const char * cp, struct in_addr * ip)
{
	unsigned long l_ip;

	l_ip = inet_addr(cp);
	if (l_ip == INADDR_NONE)
		return -1;
	ip->S_un.S_addr = l_ip;
	return 0;
}

const char * local_strerror(int error_code)
{
	static char buffer[1024];

	buffer[0] = '\0';

	FormatMessage(
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		error_code,
		0, // Default language
		(LPTSTR) buffer,
		sizeof(buffer),
		NULL
	);

	return buffer;
}

#endif
