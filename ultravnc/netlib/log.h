/* copyright 2007 SiteScape */

#ifndef LOG_H_INCLUDED
#define LOG_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

void net_init_log(const char *dir, const char *name);
void net_write_stamp(int level, const char *file, int line);
void net_write_log(const char *format, ...);

#define errorf net_write_stamp(0,__FILE__,__LINE__); net_write_log
#define infof net_write_stamp(0,__FILE__,__LINE__); net_write_log
#define debugf net_write_stamp(1,__FILE__,__LINE__); net_write_log

#ifdef __cplusplus
}
#endif

#endif // LOG_H_INCLUDED

