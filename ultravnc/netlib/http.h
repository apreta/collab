/* copyright 2007 SiteScape */

#ifndef _HTTP_H
#define _HTTP_H

#include "port.h"

#define SEND_BUFF_SIZE 1280 /*16384*/
#define HTTP_SLOW_POLL_TIME 4000
#define HTTP_FAST_POLL_TIME 250		/* also 250 ms wait time built into server */
#define HTTP_START_SLOW_COUNT 6
#define HTTP_RECV_TIMEOUT 20000
#define HTTP_REQUEST_RETRIES 2

typedef struct _HTTPINFO HTTPINFO;

void HttpInit(HTTPINFO ** info);
void HttpSetNonBlock(HTTPINFO * info, BOOL flag);
void HttpSetThreadMode(HTTPINFO * info, BOOL main_thread, void (*polling_hook)());
void HttpSetTimeout(HTTPINFO * info, int timeout);
int HttpConnect(HTTPINFO * info, const char *server);
int HttpSend(HTTPINFO * info, char * data, int len);
int HttpRecv(HTTPINFO * info, char * data, int len);
int HttpCheckReadable(HTTPINFO * info, int timeout);
int HttpCheckWriteable(HTTPINFO * info, int timeout);
int HttpClose(HTTPINFO * info, BOOL clean_shutdown);


#endif
