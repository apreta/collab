/* copyright 2007 SiteScape */

// nethttp.h: platform agnostic interface for HTTP commands.
//
//////////////////////////////////////////////////////////////////////

#ifndef _NETHTTP_H
#define _NETHTTP_H

#ifdef __cplusplus
extern "C" {
#endif

#ifdef WIN32

#include <stdio.h>
#include <stdlib.h>

//#include "wininet.h"

#ifndef __cplusplus
#define bool	BOOL
#define true	1
#define false	0
#endif

HINTERNET netHttpOpen( LPCSTR appName, int iTimeout, bool fUseNTLM );
void netHttpClose( HINTERNET hSession, HINTERNET hConnection );
HINTERNET netHttpConnect( HINTERNET hSession, LPCSTR aUrl, LPCSTR aHost, unsigned short aPort, LPCSTR aName,
                          LPCSTR aPass );
HINTERNET netHttpOpenRequest( HINTERNET hConnection, const char *aVerb, const char *aObjName, const char *aVersion, DWORD dwFlags );
bool netHttpSendRequest( HINTERNET hRequest, HINTERNET hSession, const char *aHeaders, DWORD dwHeadersLen, const char *aOptional, DWORD dwOptionalLen );
bool netHttpQueryResultsReady( HINTERNET hRequest, bool main_thread );
bool netHttpQueryInfo( HINTERNET hRequest, DWORD dwFlags, LPVOID pStatusCode, LPDWORD pdwBufferLength, LPDWORD pdwIndex );
bool netInternetReadFileExA( HINTERNET hRequest, INTERNET_BUFFERSA *pOutput );
DWORD netInternetErrorDlg( HINTERNET hRequest, DWORD dwError );
bool netInternetCloseHandle( HINTERNET hRequest );
bool netHttpAddRequestHeaders( HINTERNET hRequest, const char *szHeader, DWORD dwHeaderLen );
bool netHttpEndRequest( HINTERNET hRequest );
bool netInternetGetLastResponseInfo( LPDWORD pdwError, char *szBuf, LPDWORD pdwLength );
bool netHttpSendRequestEx( HINTERNET hRequest, HINTERNET hSession, LPINTERNET_BUFFERS Output, LPINTERNET_BUFFERS Input );
bool netInternetWriteFile( HINTERNET hRequest, LPCVOID pvData, DWORD dwOut, LPDWORD pdwBytesWritten );



#else       //*****************************************************************************


#include <libsoup/soup.h>
#include "httpbuffer.h"

#include <time.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/tcp.h>

#include <stdbool.h>
#include <string.h>


typedef struct _SuperMessage
{
    SoupSession *m_pSession;
    SoupMessage *m_pMsg;
    SoupUri     *m_pUri;
    bool        m_fGotResponse;
    bool        m_fGotStatus;
    gchar       *m_strScreenName;
    gchar       *m_strPassword;
    guint       m_LastStatusCode;
    gchar       *m_LastStatusText;
    BUFFER      *m_pBUFFERRecv;
    int         m_iNumChunksSaved;
    int         m_iNumBytesWritten;
    char        *m_cSpecialPutBuffer;
    unsigned long   m_dwSpecialLength;
    GStaticMutex    m_mutex;
}   SuperMessage;


typedef struct _INTERNET_BUFFERSA
{
    unsigned long       dwStructSize;
    void                *Next;
    char                *lpvBuffer;
    unsigned long       dwBufferLength;
    unsigned long       dwBufferTotal;
}   INTERNET_BUFFERSA, *LPINTERNET_BUFFERSA;


typedef struct _INTERNET_BUFFERS
{
    unsigned long       dwStructSize;
    void                *Next;
    void                *lpvBuffer;
    unsigned long       dwBufferLength;
    unsigned long       dwBufferTotal;
}   INTERNET_BUFFERS, *LPINTERNET_BUFFERS;




#define LPVOID                  void *
#define S_OK                    0
#define E_ABORT                 ((long) 0x80004004L)
#define E_FAIL                  ((unsigned long) 0x80004005L)


#define MAX_PATH                260

#define HTTP_QUERY_CONTENT_LENGTH   5
#define HTTP_QUERY_STATUS_CODE      19
#define HTTP_QUERY_STATUS_TEXT      20
#define HTTP_QUERY_RAW_HEADERS_CRLF 22

#define INTERNET_DEFAULT_HTTP_PORT      80
#define INTERNET_DEFAULT_HTTPS_PORT     443

#define ERROR_INSUFFICIENT_BUFFER   122L
#define ERROR_CANCELLED             1223L


#define INTERNET_FLAG_RELOAD                    0x80000000
#define HTTP_QUERY_FLAG_NUMBER                  0x20000000
#define INTERNET_FLAG_NO_CACHE_WRITE            0x04000000
#define INTERNET_FLAG_DONT_CACHE                INTERNET_FLAG_NO_CACHE_WRITE
#define INTERNET_FLAG_SECURE                    0x00800000
#define INTERNET_FLAG_KEEP_CONNECTION           0x00400000
#define INTERNET_FLAG_IGNORE_CERT_DATE_INVALID  0x00002000
#define INTERNET_FLAG_IGNORE_CERT_CN_INVALID    0x00001000
#define INTERNET_FLAG_PRAGMA_NOCACHE            0x00000100




#define INTERNET_ERROR_BASE                     12000
#define ERROR_INTERNET_OUT_OF_HANDLES           (INTERNET_ERROR_BASE+1)
#define ERROR_INTERNET_TIMEOUT                  (INTERNET_ERROR_BASE+2)
#define ERROR_INTERNET_EXTENDED_ERROR           (INTERNET_ERROR_BASE+3)
#define ERROR_INTERNET_INTERNAL_ERROR           (INTERNET_ERROR_BASE+4)
#define ERROR_INTERNET_INVALID_URL              (INTERNET_ERROR_BASE+5)
#define ERROR_INTERNET_UNRECOGNIZED_SCHEME      (INTERNET_ERROR_BASE+6)
#define ERROR_INTERNET_NAME_NOT_RESOLVED        (INTERNET_ERROR_BASE+7)
#define ERROR_INTERNET_PROTOCOL_NOT_FOUND       (INTERNET_ERROR_BASE+8)
#define ERROR_INTERNET_INVALID_OPTION           (INTERNET_ERROR_BASE+9)
#define ERROR_INTERNET_BAD_OPTION_LENGTH        (INTERNET_ERROR_BASE+10)
#define ERROR_INTERNET_OPTION_NOT_SETTABLE      (INTERNET_ERROR_BASE+11)
#define ERROR_INTERNET_SHUTDOWN                 (INTERNET_ERROR_BASE+12)
#define ERROR_INTERNET_INCORRECT_USER_NAME      (INTERNET_ERROR_BASE+13)
#define ERROR_INTERNET_INCORRECT_PASSWORD       (INTERNET_ERROR_BASE+14)
#define ERROR_INTERNET_LOGIN_FAILURE            (INTERNET_ERROR_BASE+15)
#define ERROR_INTERNET_INVALID_OPERATION        (INTERNET_ERROR_BASE+16)
#define ERROR_INTERNET_OPERATION_CANCELLED      (INTERNET_ERROR_BASE+17)
#define ERROR_INTERNET_INCORRECT_HANDLE_TYPE    (INTERNET_ERROR_BASE+18)
#define ERROR_INTERNET_INCORRECT_HANDLE_STATE   (INTERNET_ERROR_BASE+19)
#define ERROR_INTERNET_NOT_PROXY_REQUEST        (INTERNET_ERROR_BASE+20)
#define ERROR_INTERNET_REGISTRY_VALUE_NOT_FOUND (INTERNET_ERROR_BASE+21)
#define ERROR_INTERNET_BAD_REGISTRY_PARAMETER   (INTERNET_ERROR_BASE+22)
#define ERROR_INTERNET_NO_DIRECT_ACCESS         (INTERNET_ERROR_BASE+23)
#define ERROR_INTERNET_NO_CONTEXT               (INTERNET_ERROR_BASE+24)
#define ERROR_INTERNET_NO_CALLBACK              (INTERNET_ERROR_BASE+25)
#define ERROR_INTERNET_REQUEST_PENDING          (INTERNET_ERROR_BASE+26)
#define ERROR_INTERNET_INCORRECT_FORMAT         (INTERNET_ERROR_BASE+27)
#define ERROR_INTERNET_ITEM_NOT_FOUND           (INTERNET_ERROR_BASE+28)
#define ERROR_INTERNET_CANNOT_CONNECT           (INTERNET_ERROR_BASE+29)
#define ERROR_INTERNET_CONNECTION_ABORTED       (INTERNET_ERROR_BASE+30)
#define ERROR_INTERNET_CONNECTION_RESET         (INTERNET_ERROR_BASE+31)
#define ERROR_INTERNET_FORCE_RETRY              (INTERNET_ERROR_BASE+32)
#define ERROR_INTERNET_INVALID_PROXY_REQUEST    (INTERNET_ERROR_BASE+33)
#define ERROR_INTERNET_NEED_UI                  (INTERNET_ERROR_BASE+34)
#define ERROR_INTERNET_HANDLE_EXISTS            (INTERNET_ERROR_BASE+36)
#define ERROR_INTERNET_SEC_CERT_DATE_INVALID    (INTERNET_ERROR_BASE+37)
#define ERROR_INTERNET_SEC_CERT_CN_INVALID      (INTERNET_ERROR_BASE+38)
#define ERROR_INTERNET_HTTP_TO_HTTPS_ON_REDIR   (INTERNET_ERROR_BASE+39)
#define ERROR_INTERNET_HTTPS_TO_HTTP_ON_REDIR   (INTERNET_ERROR_BASE+40)
#define ERROR_INTERNET_MIXED_SECURITY           (INTERNET_ERROR_BASE+41)
#define ERROR_INTERNET_CHG_POST_IS_NON_SECURE   (INTERNET_ERROR_BASE+42)
#define ERROR_INTERNET_POST_IS_NON_SECURE       (INTERNET_ERROR_BASE+43)
#define ERROR_INTERNET_CLIENT_AUTH_CERT_NEEDED  (INTERNET_ERROR_BASE+44)
#define ERROR_INTERNET_INVALID_CA               (INTERNET_ERROR_BASE+45)
#define ERROR_INTERNET_CLIENT_AUTH_NOT_SETUP    (INTERNET_ERROR_BASE+46)
#define ERROR_INTERNET_ASYNC_THREAD_FAILED      (INTERNET_ERROR_BASE+47)
#define ERROR_INTERNET_REDIRECT_SCHEME_CHANGE   (INTERNET_ERROR_BASE+48)
#define ERROR_HTTP_HEADER_NOT_FOUND             (INTERNET_ERROR_BASE+150)
#define ERROR_INTERNET_SECURITY_CHANNEL_ERROR   (INTERNET_ERROR_BASE+157)


int     GetLastError( );
bool    netInternetGetLastResponseInfo( unsigned long *pdwError, char *szBuf, unsigned long *pdwLength );
bool    netHttpEndRequest( SuperMessage *pMsg );
bool    netInternetCloseHandle( SuperMessage *pMsg );
unsigned long netInternetErrorDlg( SuperMessage *pMsg, unsigned long dwError );
SoupSession     *netHttpOpen( const char *appName, int iTimeout, bool fUseNTLM );
SuperMessage    *netHttpConnect( SoupSession *aSession, const char *aUrl, const char *aHost,
                              unsigned short aPort, const char *aName, const char *aPass );
SuperMessage    *netHttpOpenRequest( SuperMessage *pMsg, const char *aVerb, const char *aObjName, const char *aVersion, unsigned long flags );
bool    netHttpSendRequest( SuperMessage *pMsg, SoupSession *pSession, const char * aHeaders, unsigned long dwHeadersLen,
                         const char *aOptional, unsigned long dwOptionalLen );
bool    netHttpSendRequestEx( SuperMessage *pMsg, SoupSession *pSession, LPINTERNET_BUFFERS Output, LPINTERNET_BUFFERS Input );
bool    netHttpQueryResultsReady( SuperMessage *pMsg, bool main_thread );
bool    netHttpQueryInfo( SuperMessage *pMsg, unsigned long dwFlags, char *pStatusCode, unsigned long *pdwBufferLength, unsigned long *pdwIndex );
bool    netInternetReadFileExA( SuperMessage *pMsg, INTERNET_BUFFERSA *IB );
bool    netHttpAddRequestHeaders( SuperMessage *pMsg, const char *szHeader, unsigned long dwHeaderLen );
bool    netInternetWriteFile( SuperMessage *pMsg, const void *pvData, unsigned long dwOut, unsigned long *pdwBytesWritten );
void    netHttpClose( SoupSession *pSession, SuperMessage *pMsg );

#endif      //  #ifdef WIN32

#ifdef __cplusplus
}
#endif

#endif      //  #ifndef _NETHTTP_H
