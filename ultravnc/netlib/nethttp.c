/* copyright 2007 SiteScape */

// nethttp.c: implementation of our cross-platform HTTP API.
//    The Windows implementation uses WinINet while the Linux
//    implementation uses LibSoup.
//
//////////////////////////////////////////////////////////////////////

#ifdef WIN32
#include <windows.h>
#include <wininet.h>
#endif

#include <stdio.h>

#include "nethttp.h"

#ifndef LINUX

HINTERNET netHttpOpen( LPCSTR appName, int iTimeout, bool fUseNTLM )
{
    return( InternetOpenA( appName, INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0) );
}


void netHttpClose( HINTERNET hSession, HINTERNET hConnection )
{
    if( hConnection )
        InternetCloseHandle( hConnection );
    if( hSession )
        InternetCloseHandle( hSession );
}


HINTERNET netHttpConnect( HINTERNET hSession, LPCSTR aUrl, LPCSTR aHost, unsigned short aPort, LPCSTR aName,
                          LPCSTR aPass )
{
    return( InternetConnectA( hSession, aHost, aPort, aName, aPass, INTERNET_SERVICE_HTTP, 0, 0 ));
}


HINTERNET netHttpOpenRequest( HINTERNET hConnection, const char *aVerb, const char *aObjName, const char *aVersion, DWORD dwFlags )
{
    return( HttpOpenRequestA( hConnection, aVerb, aObjName, aVersion, NULL, NULL, dwFlags, 0) );
}


bool netHttpSendRequest( HINTERNET hRequest, HINTERNET hSession, const char *aHeaders, DWORD dwHeadersLen, const char *aOptional, DWORD dwOptionalLen )
{
    return( HttpSendRequestA( hRequest, aHeaders, dwHeadersLen, (void *) aOptional, dwOptionalLen ) );

}

bool netHttpQueryResultsReady( HINTERNET hRequest, bool main_thread )
{
    return true;
}

bool netHttpQueryInfo( HINTERNET hRequest, DWORD dwFlags, LPVOID pStatusCode, LPDWORD pdwBufferLength, LPDWORD pdwIndex )
{
    return( HttpQueryInfoA( hRequest, dwFlags, pStatusCode, pdwBufferLength, pdwIndex ) );
}

bool netInternetReadFileExA( HINTERNET hRequest, INTERNET_BUFFERSA *pOutput )
{
    DWORD   dwContext = 0;
    return( InternetReadFileExA( hRequest, pOutput, IRF_NO_WAIT, dwContext ) );
}


DWORD netInternetErrorDlg( HINTERNET hRequest, DWORD dwError )
{
    HWND    hWnd    = GetDesktopWindow( );
    DWORD   dwFlags = FLAGS_ERROR_UI_FILTER_FOR_ERRORS | FLAGS_ERROR_UI_FLAGS_GENERATE_DATA | FLAGS_ERROR_UI_FLAGS_CHANGE_OPTIONS;

    return( InternetErrorDlg( hWnd, hRequest, dwError, dwFlags, NULL ) );
}

bool netInternetCloseHandle( HINTERNET hRequest )
{
    if( hRequest )
        return InternetCloseHandle( hRequest );
    else
        return true;
}


bool netHttpAddRequestHeaders( HINTERNET hRequest, const char *szHeader, DWORD dwHeaderLen )
{
    return HttpAddRequestHeadersA( hRequest, szHeader, dwHeaderLen, HTTP_ADDREQ_FLAG_ADD );
}


bool netHttpEndRequest( HINTERNET hRequest )
{
    return HttpEndRequest( hRequest, NULL, 0, 0 );
}

bool netInternetGetLastResponseInfo( LPDWORD pdwError, char *szBuf, LPDWORD pdwLength )
{
    return InternetGetLastResponseInfoA( pdwError, szBuf, pdwLength );
}


bool netHttpSendRequestEx( HINTERNET hRequest, HINTERNET hSession, LPINTERNET_BUFFERS Output, LPINTERNET_BUFFERS Input )
{
    return HttpSendRequestEx( hRequest, Output, Input, HSR_INITIATE, 0 );
}


bool netInternetWriteFile( HINTERNET hRequest, LPCVOID pvData, DWORD dwOut, LPDWORD pdwBytesWritten )
{
    return InternetWriteFile( hRequest, pvData, dwOut, pdwBytesWritten );
}


#else

//#define DEBUG_LOG

#include <errno.h>


typedef struct _PROXY
{
    unsigned        type;		// PROXY, SOCKS
    char            *server;
    unsigned        port;
    struct _PROXY   *next;
} PROXY;

extern  PROXY   *proxy_list;
extern  char    proxy_user[256];
extern  char    proxy_password[256];

int         giMaxBytes2Send      = 1024;
int         giAuthCount;



#define errorf g_print
#define debugf g_print


#ifdef DEBUG_LOG
//*****************************************************************************
static void print_header( gpointer name, gpointer value, gpointer data )
{
    debugf( "  --  header is %s: %s\n", (char *) name, (char *) value );
}


//*****************************************************************************
static void qdump_message( char *aName, SoupMessage *pMsg )
{
    debugf( "\n%s( )  --  method %s,  status code %d,  request/response lens %d/%d,\n",
            aName, pMsg->method, pMsg->status_code, pMsg->request.length, pMsg->response.length );

    debugf( "%s( )  --  status reason ***%s***, SoupMsgStatus %d\n",
            aName, pMsg->reason_phrase, pMsg->status );
}

#endif

//*****************************************************************************
static void dump_message( char *aName, SoupMessage *pMsg )
{
#ifdef DEBUG_LOG
    qdump_message( aName, pMsg );

    debugf( "REQUEST HEADERS:\n" );
    soup_message_foreach_header( pMsg->request_headers, print_header, NULL);

    debugf( "RESPONSE HEADERS:\n" );
    soup_message_foreach_header( pMsg->response_headers, print_header, NULL);
#endif
}


//*****************************************************************************
static void fulldump_message( char *aName, SoupMessage *pMsg )
{
#ifdef DEBUG_LOG
    dump_message( aName, pMsg );

    debugf( "%s( )  --  request encoding %d,  response encoding %d  ",
            aName, pMsg->method, pMsg->status_code, pMsg->request.length, pMsg->response.length );

    debugf( "%s( )  --  request data ***%s***", aName, pMsg->request.body );
    debugf( "  -- 0x%8.8x  --  request data ***%s***\n", pMsg->request.body, pMsg->request.body );

    debugf( "%s( )  --  response data ***%s***", aName, pMsg->response.body );
    debugf( "  -- 0x%8.8x  --  response data ***%s***\n", pMsg->response.body, pMsg->response.body );
#endif
}


//*****************************************************************************
int GetLastError( )
{
    // TODO: implement
    int     iRet = errno;
    debugf( "entering GetLastError( )\n" );

    if( iRet  !=  0 )
    {
        if( iRet  ==  EAGAIN )
            iRet = ERROR_INTERNET_TIMEOUT;
    }

    return 0;
//    return iRet;
}


//*****************************************************************************
bool netInternetGetLastResponseInfo( unsigned long *pdwError, char *szBuf, unsigned long *pdwLength )
{
    // TODO: implement
    debugf( "entering netInternetGetLastResponseInfo( )\n" );
    return false;
}


//*****************************************************************************
bool netHttpEndRequest( SuperMessage *pSMsg )
{
    // TODO: implement
    debugf( "entering netHttpEndRequest( )\n" );
    return true;
}


//*****************************************************************************
unsigned long netInternetErrorDlg( SuperMessage *pSMsg, unsigned long dwError )
{
    // TODO: implement
    debugf( "entering netInternetErrorDlg( )\n" );
    return 0;   //  ERROR_SUCCESS  is zero
}


//*****************************************************************************
bool netInternetCloseHandle( SuperMessage *pSMsg )
{
    //  Do I need to do anything here?
    //  The example code didn't do any unrefs, deletes or closes...
    //  But that doesn't mean they shouldn't actually be done!!
    if( pSMsg  ==  NULL )
    {
#ifdef DEBUG_LOG
        debugf( "entering netInternetCloseHandle( pSMsg = NULL )  --  RETURNING immediately\n" );
#endif
        return true;
    }
    else
    {
#ifdef DEBUG_LOG
        debugf( "entering netInternetCloseHandle( pMsg = 0x%8.8x )\n", pSMsg->m_pMsg );
#endif
    }

    if ( !pSMsg->m_fGotResponse && pSMsg->m_pMsg)
    {
        if (SOUP_MESSAGE_IS_STARTING(pSMsg->m_pMsg) || pSMsg->m_pMsg->status == SOUP_MESSAGE_STATUS_RUNNING)
        {
            debugf( "Cancelling request ox%8.8x\n", pSMsg->m_pMsg );
            soup_session_cancel_message( pSMsg->m_pSession, pSMsg->m_pMsg );
        }
    }

    g_static_mutex_lock( &pSMsg->m_mutex );

    BUFFER  *cur;
    cur = pSMsg->m_pBUFFERRecv;
    while( cur  !=  NULL )
    {
        cur = RemoveBuffer( &pSMsg->m_pBUFFERRecv );
    }

    g_static_mutex_unlock( &pSMsg->m_mutex );
    g_static_mutex_free( &pSMsg->m_mutex );

    // TODO: free other alloc'ed data
    if ( pSMsg->m_strScreenName )
        g_free( pSMsg->m_strScreenName );
    if ( pSMsg->m_strPassword )
        g_free( pSMsg->m_strPassword );
    g_free( pSMsg );

#ifdef DEBUG_LOG
    debugf("Handle closed\n");
#endif

    return true;
}


//*****************************************************************************
static void authenticate( SoupSession *session, SoupMessage *pMsg, const char *auth_type,
                          const char *auth_realm, char **username, char **password, gpointer user_data )
{
    dump_message( "authenticate", pMsg );

    SuperMessage    *pSMsg;
    pSMsg = (SuperMessage *) user_data;

    bool    fUseURI;
    fUseURI = true;
    if( fUseURI )
    {
        *username = g_strdup( pSMsg->m_pUri->user );
        *password = g_strdup( pSMsg->m_pUri->passwd );
    }
    else
    {
        *username = g_strdup( pSMsg->m_strScreenName );
        *password = g_strdup( pSMsg->m_strPassword );
    }

    giAuthCount = 1;

    //  Clear buffer - we aren't interested in this authentication chatter
    g_static_mutex_lock( &pSMsg->m_mutex );

    BUFFER  *cur;
    cur = pSMsg->m_pBUFFERRecv;
    while( cur  !=  NULL )
    {
        cur = RemoveBuffer( &pSMsg->m_pBUFFERRecv );
    }

    g_static_mutex_unlock( &pSMsg->m_mutex );
}


//*****************************************************************************
static void reauthenticate( SoupSession *session, SoupMessage *pMsg, const char *auth_type,
                            const char *auth_realm, char **username, char **password, gpointer user_data )
{
    if( giAuthCount  ==  1 )
        dump_message( "reauthenticate", pMsg );

    SuperMessage    *pSMsg;
    pSMsg = (SuperMessage *) user_data;

    //  If we don't limit the number of times we respond it becomes an endless loop...
    if( giAuthCount++  >  5 )
        return;

    bool    fUseURI;
    fUseURI = false;
    if( fUseURI )
    {
        *username = g_strdup( pSMsg->m_pUri->user );
        *password = g_strdup( pSMsg->m_pUri->passwd );
    }
    else
    {
        *username = g_strdup( pSMsg->m_strScreenName );
        *password = g_strdup( pSMsg->m_strPassword );
    }

    //  Clear buffer - we aren't interested in this authentication chatter
    g_static_mutex_lock( &pSMsg->m_mutex );

    BUFFER  *cur;
    cur = pSMsg->m_pBUFFERRecv;
    while( cur  !=  NULL )
    {
        cur = RemoveBuffer( &pSMsg->m_pBUFFERRecv );
    }

    g_static_mutex_unlock( &pSMsg->m_mutex );
}


//*****************************************************************************
SoupSession * netHttpOpen( const char *appName, int iTimeout, bool fUseNTLM )
{
    SoupSession     *pRet;
    if( iTimeout  >  0 )
    {
        if( proxy_list  !=  NULL )
        {
            SoupUri     *pProxyUri;
            char        serverName[ 255 ];
            serverName[ 0 ] = 0;
            sprintf( serverName, "http://%s:%d", proxy_list->server, proxy_list->port );
            pProxyUri = soup_uri_new( serverName );
            pProxyUri->path = g_strdup( serverName );
            if( fUseNTLM )
                pRet = soup_session_async_new_with_options( SOUP_SESSION_TIMEOUT, iTimeout, SOUP_SESSION_USE_NTLM, fUseNTLM, SOUP_SESSION_PROXY_URI, pProxyUri, NULL );
            else
                pRet = soup_session_async_new_with_options( SOUP_SESSION_TIMEOUT, iTimeout, SOUP_SESSION_PROXY_URI, pProxyUri, NULL );
        }
        else
        {
            if( fUseNTLM )
                pRet = soup_session_async_new_with_options( SOUP_SESSION_TIMEOUT, iTimeout, SOUP_SESSION_USE_NTLM, fUseNTLM, NULL );
            else
                pRet = soup_session_async_new_with_options( SOUP_SESSION_TIMEOUT, iTimeout, NULL );
        }
    }
    else
    {
        if( fUseNTLM )
            pRet = soup_session_async_new_with_options( SOUP_SESSION_USE_NTLM, fUseNTLM, NULL );
        else
            pRet = soup_session_async_new( );
    }
    return pRet;
}


//*****************************************************************************
static SuperMessage * newMessage( SoupSession * aSession )
{
    SuperMessage    *pSMsg  = g_malloc0( sizeof( SuperMessage ) );
    if( pSMsg  ==  NULL )
        return pSMsg;
    pSMsg->m_pSession           = aSession;
    pSMsg->m_pMsg               = NULL;
    pSMsg->m_pUri               = NULL;
    pSMsg->m_fGotResponse       = false;
    pSMsg->m_fGotStatus         = false;
    pSMsg->m_strScreenName      = NULL;
    pSMsg->m_strPassword        = NULL;
    pSMsg->m_LastStatusCode     = 0;
    pSMsg->m_LastStatusText     = NULL;
    pSMsg->m_pBUFFERRecv        = NULL;
    pSMsg->m_iNumChunksSaved    = 0;
    pSMsg->m_iNumBytesWritten   = 0;
    pSMsg->m_cSpecialPutBuffer  = NULL;
    pSMsg->m_dwSpecialLength    = 0;

    g_static_mutex_init( &pSMsg->m_mutex );

    return pSMsg;
}

//*****************************************************************************
SuperMessage *netHttpConnect( SoupSession *aSession, const char *aUrl, const char *aHost,
                              unsigned short aPort, const char *aName, const char *aPass )
{
#ifdef DEBUG_LOG
    debugf("entering netHttpConnect( )  --  aUrl is *%s*  --  aHost is *%s*\n", aUrl, aHost );
#endif

    SuperMessage    *pSMsg  = newMessage( aSession );
    if( pSMsg  ==  NULL )
        return pSMsg;

    pSMsg->m_pUri = soup_uri_new( aUrl );

    if( ( aName && strlen( aName ) > 0 )  &&  ( aPass && strlen( aPass ) > 0 ) )
    {
        pSMsg->m_strScreenName = g_strdup( aName );
        pSMsg->m_strPassword   = g_strdup( aPass );
    }
    else
    {
        pSMsg->m_strScreenName = g_strdup( proxy_user );
        pSMsg->m_strPassword   = g_strdup( proxy_password );
    }

    pSMsg->m_pUri->user    = g_strdup( proxy_user );
    pSMsg->m_pUri->passwd  = g_strdup( proxy_password );

    return pSMsg;
}


//*****************************************************************************
SuperMessage *netHttpOpenRequest( SuperMessage *pSMsg, const char *aVerb, const char *aObjName, const char *aVersion, unsigned long flags )
{
#ifdef DEBUG_LOG
    debugf( "entering netHttpOpenRequest( )  --  aVerb is *%s*  --  aObjName is *%s*\n", aVerb, aObjName );
#endif

    SuperMessage *pNewSMsg = newMessage( pSMsg->m_pSession );

    // Copy other necessary fields
    if( (pSMsg->m_strScreenName && strlen( pSMsg->m_strScreenName ) > 0)  &&  (pSMsg->m_strPassword && strlen( pSMsg->m_strPassword ) > 0 ) )
    {
        pNewSMsg->m_strScreenName = g_strdup( pSMsg->m_strScreenName );
        pNewSMsg->m_strPassword   = g_strdup( pSMsg->m_strPassword );
    }
    else
    {
        pNewSMsg->m_strScreenName = g_strdup( proxy_user );
        pNewSMsg->m_strPassword   = g_strdup( proxy_password );
    }
    pNewSMsg->m_pUri          = pSMsg->m_pUri;
    pNewSMsg->m_pUri->path    = g_strdup( aObjName );

    char        *newVerb;
    newVerb = g_strdup( aVerb );
    pNewSMsg->m_pMsg = soup_message_new_from_uri( newVerb, pNewSMsg->m_pUri );

    g_signal_connect( pSMsg->m_pSession, "authenticate",   G_CALLBACK (authenticate),   pNewSMsg );
    g_signal_connect( pSMsg->m_pSession, "reauthenticate", G_CALLBACK (reauthenticate), pNewSMsg );

    return pNewSMsg;
}


//*****************************************************************************
static void soup_handler_post_request( SoupMessage *pMsg, gpointer user_data )
{
    // * Request was sent
    // NOTE:  this handler is not installed/used currently

    if( strcmp( pMsg->method, SOUP_METHOD_PUT )  ==  0 )
    {
        //  Set up the SOUP data buffer
        dump_message( "entering soup_handler_post_request", pMsg );

        //  Remove the old Content-Length headers.  One probably has a zero length
        soup_message_remove_header( pMsg->response_headers, "Content-Length" );

        //  Add a new Content-Length header with the current length
        char    *pT = g_strdup_printf( "%lu", ((SuperMessage *) user_data)->m_dwSpecialLength );
        soup_message_add_header( pMsg->response_headers, "Content-Length", pT );
        g_free(pT);

        dump_message( "leaving soup_handler_post_request", pMsg );
    }
}


//*****************************************************************************
static void soup_handler_pre_body( SoupMessage *pMsg, gpointer user_data )
{
    // * Headers received from server
    //  NEED TO HANDLE case where  --  method == PUT  &&  status_code == 201 (Resource Created)
    //  Web page says we should look at header "Location" for URI to use
    fulldump_message( "entering soup_handler_pre_body", pMsg );

    if( pMsg->status_code  ==  201  &&  (( strcmp( pMsg->method, SOUP_METHOD_PUT ) == 0 )  ||  ( strcmp( pMsg->method, SOUP_METHOD_POST ) == 0 )))
    {
        debugf("Status code 201\n");
    }
    else if( pMsg->status_code  ==  204  &&  (( strcmp( pMsg->method, SOUP_METHOD_PUT ) == 0 )  ||  ( strcmp( pMsg->method, SOUP_METHOD_POST ) == 0 )))
    {
        dump_message( "entering  --  DANGER HERE  --  soup_handler_pre_body", pMsg );
    }

    SuperMessage    *pSMsg;
    pSMsg = (SuperMessage *) user_data;
    if( pMsg->status_code  ==  200  &&  ( strcmp( pMsg->method, SOUP_METHOD_POST ) == 0 ) )
    {
        int     iBufLen     = 0;
        char    cBuf[ MAX_PATH ];
        char    *pC;
        pC = soup_message_get_header( pMsg->response_headers, "Content-Length" );
        if( pC )
        {
            strcpy( cBuf, pC );
            iBufLen = strlen( cBuf );
            if( iBufLen  >  0 )
            {
                iBufLen = atoi( cBuf );
            }
        }
#ifdef DEBUG_LOG
        debugf( "  --  Content-Length returned was *%s*  --  atoi( ) returned %d\n", cBuf, iBufLen );
#endif

        if( iBufLen )
        {
            pSMsg->m_cSpecialPutBuffer  = g_malloc0( iBufLen );
            pSMsg->m_dwSpecialLength    = iBufLen;

            pMsg->response.owner = SOUP_BUFFER_STATIC;
            pMsg->response.body  = pSMsg->m_cSpecialPutBuffer;
            pMsg->request.length = pSMsg->m_dwSpecialLength;
        }
    }

    pSMsg->m_LastStatusCode = pMsg->status_code;
    if( pSMsg->m_LastStatusText )
        g_free( pSMsg->m_LastStatusText );
    pSMsg->m_LastStatusText = g_strdup( pMsg->reason_phrase );
    pSMsg->m_fGotStatus = true;

    dump_message( "leaving soup_handler_pre_body", pMsg );
}


//*****************************************************************************
static void soup_handler_body_chunk( SoupMessage *pMsg, gpointer user_data )
{
    // * Chunk of response received
    //  NEED TO HANDLE case where  --  method == PUT  &&  status_code == 201 (Resource Created)
    //  Web page says we should look at header "Location" for URI to use
    //  response buffer length returned was 370, rather than the 2048 I had set

    if( pMsg->status_code  ==  201  &&  (( strcmp( pMsg->method, SOUP_METHOD_PUT ) == 0 )  ||  ( strcmp( pMsg->method, SOUP_METHOD_POST ) == 0 )))
    {
        fulldump_message( "entering soup_handler_body_chunk", pMsg );
    }
    else if( pMsg->status_code  ==  204  &&  (( strcmp( pMsg->method, SOUP_METHOD_PUT ) == 0 )  ||  ( strcmp( pMsg->method, SOUP_METHOD_POST ) == 0 )))
    {
        fulldump_message( "entering soup_handler_body_chunk", pMsg );
    }

    SuperMessage    *pSMsg;
    pSMsg = (SuperMessage *) user_data;

    if( strcmp( pMsg->method, SOUP_METHOD_PUT ) != 0 )
    {
        dump_message( "entering soup_handler_body_chunk", pMsg );

        //  We are reading data from the server so save everything to the buffer
        if( pMsg->response.length  >  0 )
        {
            g_static_mutex_lock( &pSMsg->m_mutex );

            FillBuffer( &pSMsg->m_pBUFFERRecv, pMsg->response.body, pMsg->response.length );

            g_static_mutex_unlock( &pSMsg->m_mutex );

            pSMsg->m_iNumBytesWritten += pMsg->response.length;
            pSMsg->m_iNumChunksSaved++;
        }
    }
    else
    {
        //  We are writing data to the server so read from the PUT temp file and send it off
        dump_message( "entering  WHY AM I HERE???  DANGER WILL ROBINSON soup_handler_body_chunk", pMsg );
    }
}


//*****************************************************************************
static void soup_handler_post_body( SoupMessage *pMsg, gpointer user_data )
{
    // * Entire response received
    //  This has been compiled out for a while and it has been working that way...
    dump_message( "entering  --  THIS FUNCTION IS A NOP  --  soup_handler_post_body", pMsg );
}


//*****************************************************************************
static void got_soup_response( SoupMessage *pMsg, gpointer user_data )
{
    SuperMessage    *pSMsg;
    pSMsg = (SuperMessage *) user_data;

    if ( (strcmp( pMsg->method, SOUP_METHOD_PUT ) == 0 )  ||  ( strcmp( pMsg->method, SOUP_METHOD_POST ) == 0 ))
    {
        //  Set up the SOUP data buffer
        fulldump_message( "entering got_soup_response", pMsg );

        if( pSMsg->m_iNumChunksSaved == 0 )
        {
            if( pMsg->response.length  >  0 )
            {
                g_static_mutex_lock( &pSMsg->m_mutex );

                FillBuffer( &pSMsg->m_pBUFFERRecv, pMsg->response.body, pMsg->response.length );

                g_static_mutex_unlock( &pSMsg->m_mutex );

                pSMsg->m_iNumBytesWritten += pMsg->response.length;
                pSMsg->m_iNumChunksSaved++;
            }
        }
    }
    else
        dump_message( "entering got_soup_response [1]", pMsg );

    pSMsg->m_fGotResponse = true;
}


//*****************************************************************************
bool netHttpSendRequest( SuperMessage *pSMsg, SoupSession *pSession, const char * aHeaders, unsigned long dwHeadersLen,
                         const char *aOptional, unsigned long dwOptionalLen )
{
#ifdef DEBUG_LOG
    debugf( "entering netHttpSendRequest( )  --  aHeaders is *%s*\n  --  aOptional is *%s*\n", aHeaders, aOptional );
#endif

    dump_message( "entering netHttpSendRequest", pSMsg->m_pMsg );

    pSMsg->m_iNumChunksSaved = 0;
    pSMsg->m_iNumBytesWritten = 0;

    soup_message_set_request( pSMsg->m_pMsg, "application/octect-stream", SOUP_BUFFER_USER_OWNED, (char *) aOptional, dwOptionalLen );

//    soup_message_add_handler( pSMsg->m_pMsg, SOUP_HANDLER_POST_REQUEST, soup_handler_post_request, pSMsg );
    soup_message_add_handler( pSMsg->m_pMsg, SOUP_HANDLER_PRE_BODY,     soup_handler_pre_body,     pSMsg );
    soup_message_add_handler( pSMsg->m_pMsg, SOUP_HANDLER_BODY_CHUNK,   soup_handler_body_chunk,   pSMsg );
//    soup_message_add_handler( pSMsg->m_pMsg, SOUP_HANDLER_POST_BODY,    soup_handler_post_body,    pSMsg );

    soup_message_set_flags( pSMsg->m_pMsg, SOUP_MESSAGE_OVERWRITE_CHUNKS );

    pSMsg->m_fGotResponse = false;
    pSMsg->m_fGotStatus   = false;

    soup_session_queue_message( pSession, pSMsg->m_pMsg, got_soup_response, pSMsg );

    debugf("Message queued\n");

    return true;
}


//*****************************************************************************
static void handler_wrote_chunk( SoupMessage *pMsg, gpointer data )
{
    dump_message( "entering  --  THIS FUNCTION IS A NOP  --  handler_wrote_chunk( )", pMsg );
}


//*****************************************************************************
bool netHttpSendRequestEx( SuperMessage *pSMsg, SoupSession *pSession, LPINTERNET_BUFFERS Output, LPINTERNET_BUFFERS Input )
{
    dump_message( "entering netHttpSendRequestEx", pSMsg->m_pMsg );

    bool    fRet    = true;

//    g_signal_connect( pSMsg->m_pMsg, "wrote-chunk", G_CALLBACK(handler_wrote_chunk), NULL );

/*    pSMsg->m_cSpecialPutBuffer = (char *) Output->lpvBuffer;
    pSMsg->m_dwSpecialLength   = Output->dwBufferTotal;
*/
    fRet = netHttpSendRequest( pSMsg, pSession, "", 0, Output->lpvBuffer, Output->dwBufferTotal );

    return fRet;
}


//*****************************************************************************
bool netHttpQueryResultsReady( SuperMessage *pSMsg, bool main_thread )
{
    if (main_thread)
        while(g_main_context_iteration(NULL, FALSE))
            ;
    return pSMsg->m_fGotResponse;   // Changed from GotStatus as we loop without exit below if GotResponse is never set
}


//*****************************************************************************
bool netHttpQueryInfo( SuperMessage *pSMsg, unsigned long dwFlags, char *pStatusCode, unsigned long *pdwBufferLength, unsigned long *pdwIndex )
{
    bool    fRet    = false;

    if( dwFlags  ==  HTTP_QUERY_STATUS_CODE )
    {
        sprintf( pStatusCode, "%d", pSMsg->m_LastStatusCode );
        fRet = true;
    }
    else if( dwFlags  ==  HTTP_QUERY_STATUS_TEXT )
    {
        if( pSMsg->m_LastStatusText )
        {
            int iLen    = strlen( pSMsg->m_LastStatusText ) + 1;       //  Don't forget the zero
            iLen = ((iLen >= *pdwBufferLength) ? (*pdwBufferLength - 1) : iLen);
            strncpy( pStatusCode, pSMsg->m_LastStatusText, iLen );
            pStatusCode[ iLen ] = 0;
            if( strlen( pStatusCode )  >  0 )
                fRet = true;
        }
    }
    else if( dwFlags  ==  HTTP_QUERY_CONTENT_LENGTH )
    {
        char    *pT;
        int     i = 0;
        int     j = 0;

        //  We haven't received the entire download yet - wait until gfGotResponse becomes true
        while( !pSMsg->m_fGotResponse )
        {
            i++;
            while( g_main_context_iteration( NULL, false ) )
                j++;
            sleep_millis(100);
        }
#ifdef DEBUG_LOG
        debugf( "entering netHttpQueryInfo( ) -- after looping waiting for data to arrive --  iterations %d  --  TRUE returns %d\n", i, j );
        debugf( "pSMsg->m_fGotResponse = %d  --  pSMsg->m_pBUFFERRecv = 0x%8.8x  --  RecvLen = %d\n", pSMsg->m_fGotResponse, pSMsg->m_pBUFFERRecv,
                              (pSMsg->m_pBUFFERRecv) ? pSMsg->m_pBUFFERRecv->len : -1 );
//        dump_message( "entering netHttpQueryInfo( HTTP_QUERY_CONTENT_LENGTH )", pMsg );
        debugf( "HTTP_QUERY_CONTENT_LENGTH to be returned = %d )\n", pSMsg->m_iNumBytesWritten );
#endif

        //  format number of bytes read and return that
        pT = g_strdup_printf( "%d", pSMsg->m_iNumBytesWritten );
        strcpy( pStatusCode, pT );
        *pdwBufferLength = strlen( pStatusCode );
        if( *pdwBufferLength  >  0 )
            fRet = true;
        g_free( pT );

        //  HERE LIES * * * CRASHES * * * <-- should fix to validate we got what we expected?
//        if( pMsg )
//        {
//            char    *pC;
//            pC = soup_message_get_header( pMsg->response_headers, "Content-Length" );
//            pT = g_strdup_printf( "   soup_message_get_header( response, \"Content-Length\" ) returned  *%s*\n", pC );
//            debugf( pT );
//            g_free( pT );
//            if( pC )
//            {
//                strcpy( pStatusCode, pC );
//                *pdwBufferLength = strlen( pStatusCode );
//                if( *pdwBufferLength  >  0 )
//                    fRet = true;
//            }
//            else
//            {
//                strcpy( pStatusCode, "" );
//                *pdwBufferLength = 0;
//            }
//        }
    }
    return fRet;
}


//*****************************************************************************
bool netInternetReadFileExA( SuperMessage *pSMsg, INTERNET_BUFFERSA *IB )
{
    int     i = 0;
    int     j = 0;

    //  We haven't received the entire download yet - wait until gfGotResponse becomes true
    // TODO: return buffers as we receive them
    while( !pSMsg->m_fGotResponse  &&  !pSMsg->m_pBUFFERRecv )
    {
        i++;
        while( g_main_context_iteration( NULL, false ) )
            j++;
        sleep_millis(100);
    }

#ifdef DEBUG_LOG
    debugf( "after looping waiting for data to arrive --  iterations %d  --  TRUE returns %d\n", i, j );
    debugf( "pSMsg->m_fGotResponse = %d  --  pSMsg->m_pBUFFERRecv = 0x%8.8x  --  RecvLen = %d\n",
            pSMsg->m_fGotResponse, pSMsg->m_pBUFFERRecv,
            (pSMsg->m_pBUFFERRecv) ? pSMsg->m_pBUFFERRecv->len : -1 );
#endif

    g_static_mutex_lock( &pSMsg->m_mutex );

    int     bytesRead;
    bytesRead = ReadBuffer( &pSMsg->m_pBUFFERRecv, IB->lpvBuffer, IB->dwBufferLength );
    IB->dwBufferLength = bytesRead;

    g_static_mutex_unlock( &pSMsg->m_mutex );

    return true;
//    return pSMsg->m_fGotResponse;
}


//*****************************************************************************
bool netHttpAddRequestHeaders( SuperMessage *pSMsg, const char *szHeader, unsigned long dwHeaderLen )
{
    if( dwHeaderLen  ==  0 )
        return false;

    gchar   **splitz;
    gchar   delim[2];
    delim[0] = ':';
    delim[1] = 0;
    splitz = g_strsplit( szHeader, delim, 2 );

    soup_message_add_header( pSMsg->m_pMsg->request_headers, (splitz[0]) ? splitz[0] : "", (splitz[0] && splitz[1]) ? splitz[1] : "" );
    g_strfreev( splitz );

    return true;
}


//*****************************************************************************
bool netInternetWriteFile( SuperMessage *pSMsg, const void *pvData, unsigned long dwOut, unsigned long *pdwBytesWritten )
{
    //  Here's the plan...
    //  See the explanation in the function netHttpSendRequestEx( ).

    *pdwBytesWritten = dwOut;

    return true;
}


//*****************************************************************************
void netHttpClose( SoupSession *pSession, SuperMessage *pSMsg )
{

    if( pSMsg  !=  NULL )
        netInternetCloseHandle( pSMsg );
}

#endif      //  #ifndef __WXMSW__
