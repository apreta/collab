/* copyright 2007 SiteScape */

#ifndef HTTPAUTH_H
#define HTTPAUTH_H

#ifdef USE_SSPI  // Only define for Win32

DWORD SSPIInitialize(LPTSTR pszUserID, LPTSTR pszPassword, LPTSTR pszDomain);
DWORD SSPIAuthenicate(BYTE *pbResponse, BYTE *pbBuffer, DWORD nBufferSize, BOOL *fNeedMoreData);

#endif

#endif
