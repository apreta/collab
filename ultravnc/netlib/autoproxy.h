/* copyright 2007 SiteScape */

// These are missing in Visual Studio .NET and Cygwin/MinGW environments

#ifndef PROXY_AUTO_DETECT_TYPE_DHCP

#define  PROXY_AUTO_DETECT_TYPE_DHCP    1
#define  PROXY_AUTO_DETECT_TYPE_DNS_A   2

struct AutoProxyHelperFunctions;

typedef struct AutoProxyHelperVtbl
{
    BOOL ( __stdcall *IsResolvable )(
            LPSTR   lpszHost);

    DWORD ( __stdcall *GetIPAddress )(
           LPSTR   lpszIPAddress,
           LPDWORD lpdwIPAddressSize);

    DWORD ( __stdcall *ResolveHostName )(
           LPSTR   lpszHostName,
           LPSTR   lpszIPAddress,
           LPDWORD lpdwIPAddressSize);

    BOOL ( __stdcall *IsInNet )(
            LPSTR   lpszIPAddress,
            LPSTR   lpszDest,
            LPSTR   lpszMask);

}AutoProxyHelperVtbl;

typedef struct
{
    DWORD dwStructSize;              // Size of struct
    LPSTR lpszScriptBuffer;          // Buffer to Pass
    DWORD dwScriptBufferSize;        // Size of buffer above
} AUTO_PROXY_SCRIPT_BUFFER, *LPAUTO_PROXY_SCRIPT_BUFFER;

typedef struct AutoProxyHelperFunctions
{
    const struct AutoProxyHelperVtbl * lpVtbl;
} AutoProxyHelperFunctions;


typedef BOOL ( CALLBACK *pfnInternetInitializeAutoProxyDll)(DWORD dwVersion,
                           LPSTR lpszDownloadedTempFile,
                           LPSTR lpszMime,
                           AutoProxyHelperFunctions* lpAutoProxyCallbacks,
                           LPAUTO_PROXY_SCRIPT_BUFFER lpAutoProxyScriptBuffer );

typedef BOOL (CALLBACK *pfnInternetDeInitializeAutoProxyDll)(LPSTR lpszMime,
                           DWORD dwReserved);

typedef BOOL (CALLBACK *pfnInternetGetProxyInfo)(LPCSTR lpszUrl,
                           DWORD dwUrlLength,
                           LPSTR lpszUrlHostName,
                           DWORD dwUrlHostNameLength,
                           LPSTR* lplpszProxyHostName,
                           LPDWORD lpdwProxyHostNameLength);

#endif
