/* copyright 2007 SiteScape */

#include <stdlib.h>
#include <memory.h>

#include "httpbuffer.h"

// Insert new buffer at tail of queue.
void InsertBuffer(BUFFER ** head, BUFFER * new_buff)
{
	BUFFER *cur = *head;

	if (*head == NULL)
	{
		*head = new_buff;
		return;
	}

	while (cur->next != NULL)
		cur = cur->next;

	cur->next = new_buff;
}


// Remove buffer from head of queue, and returns new head.
BUFFER * RemoveBuffer(BUFFER ** head)
{
	BUFFER *cur = *head;
	*head = cur->next;
	free(cur);

	return *head;
}


// Store data in buffer queue.
int FillBuffer(BUFFER ** head, char * data, int len)
{
	BUFFER * cur;
	int copied = 0;
	int remaining_in_buff;

	// Get to last buffer
	cur = *head;
	while (cur != NULL && cur->next != NULL)
		cur = cur->next;

	while (copied < len)
	{
		if (cur == NULL)
		{
			cur = calloc(1, sizeof(BUFFER));
			InsertBuffer(head, cur);
		}

		remaining_in_buff = sizeof(cur->data) - cur->len;
		if (remaining_in_buff > 0)
		{
			int num_to_copy = remaining_in_buff > len - copied ?
							  len - copied : remaining_in_buff;

			memcpy(cur->data + cur->len, data + copied, num_to_copy);
			copied += num_to_copy;
			cur->len += num_to_copy;
		}
		else
			cur = NULL;
	}

	return copied;
}


// Copy buffered data from queue to supplied buffer.
int ReadBuffer(BUFFER ** head, char * data, int len)
{
	BUFFER * cur;
	int copied = 0;

	cur = *head;
	while (cur != NULL && copied < len)
	{
		int remaining_in_buff = cur->len - cur->pos;
		if (remaining_in_buff > 0)
		{
			int num_to_copy = remaining_in_buff > len - copied ?
							  len - copied : remaining_in_buff;

			memcpy(data + copied, cur->data + cur->pos, num_to_copy);
			copied += num_to_copy;
			cur->pos += num_to_copy;

			if (cur->pos == cur->len)
				cur = RemoveBuffer(head);
		}
	}

	return copied;
}


