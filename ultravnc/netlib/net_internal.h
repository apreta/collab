#ifndef NET_INTERNAL_H_INCLUDED
#define NET_INTERNAL_H_INCLUDED

#include "port.h"
#include "http.h"

#ifndef MAX_PATH
#define MAX_PATH 1024
#endif

#define BUFF_SIZE 8*1024        // used to read responses from proxies.
#define STORED_CONFIG_TIME 86400 // only use stored config info for 1 day
#define RETRY_AFTER_CONNECT 90	 // number of secs to retry same port


/* connection status */
typedef enum
{
	CONNECT_NONE = 0,
	CONNECT_GOOD
} CONNECT_STATUS;

/* netlib connection definition */
typedef struct _SOCKETINFO
{
	char *server;
	int port;

	SOCKET sock;
	struct sockaddr addr;
	int len;

	BOOL interrupt;

	int is_http;
	HTTPINFO *http;

} SOCKETINFO;

typedef enum {CONNECT_DIRECT, CONNECT_PROXY, CONNECT_HTTP} connect_types;

typedef struct _HUNTINFO
{
	int port;
	connect_types connect_type;
} HUNTINFO;


#define in_addr_t unsigned long int
#define in_port_t unsigned short int

#endif // NET_INTERNAL_H_INCLUDED
