/* copyright 2007 SiteScape */

#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <string.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <io.h>

char *logdir = NULL;
static int logfd = -1;

// The number of log files to keep before deleting the oldest
static unsigned history_len = 10;

// If the current log is >= kMaxLogFileSize, rotate the current log
// into the stack of historical files and create a new log file.
static int kMaxLogFileSize = 100 * 1024;

#define MAX_LOG_FILENAME    (256)

static void log_rotate_if_needed( const char *logFN, int iAlwaysRotate )
{
    char newerfn[MAX_LOG_FILENAME];
    char olderfn[MAX_LOG_FILENAME];
    int i;
    long    sz      = -1;

    struct stat myStats;
    if( stat( logFN, &myStats )  ==  0 )
        sz = myStats.st_size;

    if( (sz < kMaxLogFileSize)  &&  !iAlwaysRotate )
        return;

    sprintf( newerfn, "%s.%u", logFN, history_len );
    unlink( newerfn );
    for( i = history_len-1; i > 0; i--)
    {
        strcpy( olderfn, newerfn );
        sprintf( newerfn, "%s.%u", logFN, i );
        rename( newerfn, olderfn );
    }
    strcpy(olderfn, newerfn);
    strcpy(newerfn, logFN);
    rename( newerfn, olderfn );
}

void net_write_stamp(int level, const char *file, int line, const char *format,...)
{
    char buff[256];

    if (logfd > 0)
    {
        time_t now = time(NULL);
        struct tm *now_tm = localtime(&now);
        int pos;

        char *f = strrchr(file, '\\');
        if (f == NULL)
            f = strrchr(file, '/');
        if (f == NULL)
            f = file;
        else
            ++f;

        pos = strftime(buff, sizeof(buff), "%m/%d/%y %H:%M:%S", now_tm);
        sprintf(buff + pos, " %s:%d ", f, line);
        write(logfd, buff, strlen(buff));
    }
}

void net_write_log(const char *format, ...)
{
    char buff[1024];

    if (logfd > 0)
    {
        va_list args;

        va_start(args, format);

        _vsnprintf(buff, sizeof(buff), format, args);
        write(logfd, buff, strlen(buff));

        va_end(args);
    }
}

void net_init_log(const char *directory, const char *name)
{
    logdir = calloc(strlen(directory) + 20, 1);

    sprintf(logdir, "%s/%s.log", directory, name);

    log_rotate_if_needed( logdir, 0 );

    logfd = _open(logdir, O_WRONLY|O_CREAT|O_APPEND, _S_IREAD | _S_IWRITE);

    net_write_log("\r\n");
    net_write_log("Opening log\r\n");
}

