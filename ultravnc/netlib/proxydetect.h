/* copyright 2007 SiteScape */

#ifndef PROXYDETECT_H
#define PROXYDETECT_H

#define FAIL -1
#define NOPROXY -2
#define BADURL -3

#ifdef __cplusplus
extern "C" {
#endif

int DetectProxy(char *url, char *lpszProxyServer, unsigned dwNameLen);

#ifdef __cplusplus
}
#endif

#endif
