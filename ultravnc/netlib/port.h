/* copyright 2007 SiteScape */

#ifndef PORT_H
#define PORT_H

#ifdef LINUX
#include <time.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <netinet/tcp.h>
#else
#include <winsock.h>
#include <time.h>
#endif

#ifdef __cplusplus
extern "C"
{
#endif

// Abstractions for some network functions

#ifdef LINUX

#ifndef BOOL
#define BOOL int
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#define SOCKET int
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1

void _set_nonblock(int fd, int act);
void _set_nodelay(int fd, int act);
int _gettimeofday(struct timeval * tv);
int sleep_millis(int ms);
long _clock();

#define _write(f,b,l) write(f,b,l)
#define _read(f,b,l) read(f,b,l)
#define _error errno
#define _close close
#define _strerror strerror

#define CLEAR_ERR (errno = 0)
#define CHECK_ERR errno
#define SET_ERR(_err) errno = _err

#define _EAGAIN EAGAIN
#define _EWOULDBLOCK EWOULDBLOCK  // really the same as EAGAIN
#define _EINTR EINTR
#define _EINPROGRESS EINPROGRESS
#define _ENETUNREACH ENETUNREACH

#endif

#ifdef _WIN32

typedef int socklen_t;

void _set_nonblock(int fd, int act);
void _set_nodelay(int fd, int act);
int _gettimeofday(struct timeval * tv);

#define _write(f,b,l) send(f,b,l,0)
#define _read(f,b,l) recv(f,b,l,0)
#define _error WSAGetLastError()
#define _close closesocket
#define _strerror local_strerror
#define _clock clock

#define CLEAR_ERR
#define CHECK_ERR GetLastError()
#define SET_ERR(_err) SetLastError(_err)

#define _EAGAIN WSAEWOULDBLOCK
#define _EWOULDBLOCK WSAEWOULDBLOCK
#define _EINTR WSAEINTR
#define _EINPROGRESS WSAEINPROGRESS
#define _ENETUNREACH WSAENETUNREACH

extern void __declspec(dllimport) __stdcall Sleep(unsigned long millis);
//#define sleep(sec) Sleep(sec * 1000)
#define sleep_millis(ms) Sleep(ms)
int inet_aton(const char * cp, struct in_addr * ip);
const char * local_strerror(int error_code);

#define strcasecmp stricmp
#define strncasecmp strnicmp

#endif

#ifdef __cplusplus
} // extern "C"
#endif

#endif
