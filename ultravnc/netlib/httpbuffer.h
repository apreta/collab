/* copyright 2007 SiteScape */

#ifndef HTTPBUFFER_H_INCLUDED
#define HTTPBUFFER_H_INCLUDED

#define HTTP_BUFF_SIZE 2048

typedef struct _BUFFER
{
	char data[HTTP_BUFF_SIZE];
	int len;
	int pos;

	struct _BUFFER * next;
} BUFFER;

void InsertBuffer(BUFFER ** head, BUFFER * new_buff);
BUFFER * RemoveBuffer(BUFFER ** head);
int FillBuffer(BUFFER ** head, char * data, int len);
int ReadBuffer(BUFFER ** head, char * data, int len);


#endif // HTTPBUFFER_H_INCLUDED
