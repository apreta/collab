
void RequestDirectX(bool flag=true);
bool UsingDirectX();
void SelectMonitor(char *displayDevice, RECT displayRect);
HRESULT	InitD3D(HWND hWnd);
HRESULT CaptureRectangle(RECT rect, BYTE *scrBuff, UINT scrBuffSize, UINT bytesPerRow, int destx=0, int desty=0);
COLORREF CapturePixel(int x, int y, int bytesPerRow);
HRESULT BufferScreen(RECT rect, int bytesPerRow);
HRESULT BlankRect(RECT rect, DWORD color, BYTE *scrBuff, UINT scrBuffSize, UINT bytesPerRow);
