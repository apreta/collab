//  Copyright (C) 1999 AT&T Laboratories Cambridge. All Rights Reserved.
//
//  This file is part of the VNC system.
//
//  The VNC system is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// If the source code for the VNC system is not available from the place 
// whence you received this file, check http://www.uk.research.att.com/vnc or contact
// the authors on vnc@uk.research.att.com for information on obtaining it.


// WinVNC.cpp

// 24/11/97		WEZ

// WinMain and main WndProc for the new version of WinVNC

////////////////////////////
// System headers
#include "stdhdrs.h"
#include "tchar.h"

////////////////////////////
// Custom headers
#include "VSocket.h"
#include "WinVNC.h"

#include "vncServer.h"
#include "vncMenu.h"
#include "vncInstHandler.h"
#include "vncService.h"
#include "../../netlib/net.h"	// IIC
#include "../../netlib/log.h"	// IIC
#include <shlobj.h> // IIC
#include "directx.h" // IIC
///unload driver
#include "../vdacctray/vncOSVersion.h"
#include "../vdacctray/videodriver.h"
//#define CRASH_ENABLED
#ifdef CRASH_ENABLED
	#ifndef _CRASH_RPT_
	#include "crashrpt.h"
	#pragma comment(lib, "crashrpt.lib")
	#endif
#endif

// Application instance and name
HINSTANCE	hAppInstance;
const char	*szAppName = PRODNAME;
DWORD		mainthreadId;
time_t		start_time = 0;				// IIC
bool		enable_recording = false;	// IIC
bool		share_allinstances = false;	// IIC
char		szDisplayName[256] = { 0 };	// IIC

// IIC
static vncMenu		*menu = NULL;
static vncServer	*server = NULL;

// If the current log is >= kMaxLogFileSize, rotate the current log in the
// stack of historical files and create a new log file.
static size_t kMaxLogFileSize = 10 * 1024;

// WinMain parses the command line and either calls the main App
// routine or, under NT, the main service routine.
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int iCmdShow)
{
    TCHAR szPath[_MAX_PATH + _MAX_FNAME];
	setbuf(stderr, 0);

	// IIC:
	// Set up directory for config files and logger
	SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, SHGFP_TYPE_CURRENT, szPath);
	strcat(szPath, "\\meeting");

	net_initialize(szPath);

	// Configure the log file, in case one is required
	net_init_log(szPath, "mtgshare");

	vnclog.SetMode( VNCLog::ToNetLog );

    errorf("Share server in %s\r\n", szPath);

#ifdef _DEBUG
	{
		// Get current flag
		int tmpFlag = _CrtSetDbgFlag( _CRTDBG_REPORT_FLAG );

		// Turn on leak-checking bit
		tmpFlag |= _CRTDBG_LEAK_CHECK_DF;

		// Set flag to the new value
		_CrtSetDbgFlag( tmpFlag );
	}
#endif

	// Save the application instance and main thread id
	hAppInstance = hInstance;
	mainthreadId = GetCurrentThreadId();

	// Initialise the VSocket system
	VSocketSystem socksys;
	if (!socksys.Initialised())
	{
		MessageBox(NULL, "Failed to initialise the socket system", szAppName, MB_OK);
		return 0;
	}
	vnclog.Print(LL_STATE, VNCLOG("sockets initialised\n"));

	// Make the command-line lowercase and parse it
	int i;
	for (i = 0; i < (int) strlen(szCmdLine); i++)
	{
		szCmdLine[i] = tolower(szCmdLine[i]);
	}

	BOOL argfound = FALSE;
//#ifndef EMBED	// IIC
	for (i = 0; i < (int) strlen(szCmdLine); i++)
	{
		if (szCmdLine[i] <= ' ')
			continue;
		argfound = TRUE;

		// Now check for command-line arguments
		if (strncmp(&szCmdLine[i], winvncRunServiceHelper, strlen(winvncRunServiceHelper)) == 0)
		{
			// NB : This flag MUST be parsed BEFORE "-service", otherwise it will match
			// the wrong option!  (This code should really be replaced with a simple
			// parser machine and parse-table...)

			// Run the WinVNC Service Helper app
			vncService::PostUserHelperMessage();
			return 0;
		}
		if (strncmp(&szCmdLine[i], winvncRunService, strlen(winvncRunService)) == 0)
		{
			// Run WinVNC as a service
			return vncService::WinVNCServiceMain();
		}
// Starts IIC -- handle application sharing
		if (strncmp(&szCmdLine[i], winvncShareApps, strlen(winvncShareApps)) == 0)
		{
			// WinVNC is being run as a user-level program
			return WinVNCAppMain(&szCmdLine[i+strlen(winvncShareApps)+1]);
		}
		if (strncmp(&szCmdLine[i], winvncSharePPT, strlen(winvncSharePPT)) == 0)
		{
			// WinVNC is being run as a user-level program
			return WinVNCAppMain(NULL, TRUE);
		}
		if (strncmp(&szCmdLine[i], winvncRunAsUserApp, strlen(winvncRunAsUserApp)) == 0)
		{
			// WinVNC is being run as a user-level program
			return WinVNCAppMain(NULL);
		}
		if (strncmp(&szCmdLine[i], winvncStartTime, strlen(winvncStartTime)) == 0)
		{
			i+=strlen(winvncStartTime);

			// First, we have to parse the command line to get the filename to use
			int start, end;
			start=i;
			while (szCmdLine[start] <= ' ' && szCmdLine[start] != 0) start++;
			end = start;
			while (szCmdLine[end] > ' ') end++;

			if (end-start > 0)
			{
				char buf[32];
				memset(buf, 0, 32);
				strncpy(buf, &szCmdLine[start], end-start);
				start_time = atoi(buf);
			}

			i = end;
			continue;
		}
		if (strncmp(&szCmdLine[i], winvncRecording, strlen(winvncRecording)) == 0)
		{
			i+=strlen(winvncRecording);

			// First, we have to parse the command line to get the filename to use
			int start, end;
			start=i;
			while (szCmdLine[start] <= ' ' && szCmdLine[start] != 0) start++;
			end = start;
			while (szCmdLine[end] > ' ') end++;

			if (end-start > 0)
			{
				char buf[32];
				memset(buf, 0, 32);
				strncpy(buf, &szCmdLine[start], end-start);
				enable_recording = atoi(buf)>0;
			}

			i = end;
			continue;
		}
		if (strncmp(&szCmdLine[i], winvncDisplay, strlen(winvncDisplay)) == 0)
		{
			i+=strlen(winvncDisplay);

			// First, we have to parse the command line to get the display name
			int start, end;
			start=i;
			while (szCmdLine[start] <= ' ' && szCmdLine[start] != 0) start++;
			end = start;
			while (szCmdLine[end] > ' ') end++;

			if (end-start > 0)
			{
				memset(szDisplayName, 0, sizeof(szDisplayName));
				if (strncmp(&szCmdLine[start], "default", 7))
					strncpy(szDisplayName, &szCmdLine[start], end-start);
			}

			i = end;
			continue;
		}
		if (strncmp(&szCmdLine[i], winvncAllInstances, strlen(winvncAllInstances)) == 0)
		{
			i+=strlen(winvncAllInstances);

			// First, we have to parse the command line to get the filename to use
			int start, end;
			start=i;
			while (szCmdLine[start] <= ' ' && szCmdLine[start] != 0) start++;
			end = start;
			while (szCmdLine[end] > ' ') end++;

			if (end-start > 0)
			{
				char buf[32];
				memset(buf, 0, 32);
				strncpy(buf, &szCmdLine[start], end-start);
				share_allinstances = atoi(buf)>0;
			}

			i = end;
			continue;
		}
		if (strncmp(&szCmdLine[i], winvncDirectX, strlen(winvncDirectX)) == 0)
		{
			RequestDirectX();
			i+=strlen(winvncDirectX);
			continue;
		}
// Ends IIC -- handle application sharing
		if (strncmp(&szCmdLine[i], winvncInstallService, strlen(winvncInstallService)) == 0)
		{
			// Install WinVNC as a service
			vncService::InstallService();
			i+=strlen(winvncInstallService);
			continue;
		}
		if (strncmp(&szCmdLine[i], winvncReinstallService, strlen(winvncReinstallService)) == 0)
		{
			// Silently remove WinVNC, then re-install it
			vncService::ReinstallService();
			i+=strlen(winvncReinstallService);
			continue;
		}
		if (strncmp(&szCmdLine[i], winvncRemoveService, strlen(winvncRemoveService)) == 0)
		{
			// Remove the WinVNC service
			vncService::RemoveService();
			i+=strlen(winvncRemoveService);
			continue;
		}
		if (strncmp(&szCmdLine[i], winvncShowProperties, strlen(winvncShowProperties)) == 0)
		{
			// Show the Properties dialog of an existing instance of WinVNC
			vncService::ShowProperties();
			i+=strlen(winvncShowProperties);
			continue;
		}
		if (strncmp(&szCmdLine[i], winvncShowDefaultProperties, strlen(winvncShowDefaultProperties)) == 0)
		{
			// Show the Properties dialog of an existing instance of WinVNC
			vncService::ShowDefaultProperties();
			i+=strlen(winvncShowDefaultProperties);
			continue;
		}
		if (strncmp(&szCmdLine[i], winvncShowAbout, strlen(winvncShowAbout)) == 0)
		{
			// Show the About dialog of an existing instance of WinVNC
			vncService::ShowAboutBox();
			i+=strlen(winvncShowAbout);
			continue;
		}
		if (strncmp(&szCmdLine[i], winvncKillRunningCopy, strlen(winvncKillRunningCopy)) == 0)
		{
			// Kill any already running copy of WinVNC
			vncService::KillRunningCopy();
			i+=strlen(winvncKillRunningCopy);
			continue;
		}
		if (strncmp(&szCmdLine[i], winvncAddNewClient, strlen(winvncAddNewClient)) == 0)
		{
			// Add a new client to an existing copy of winvnc
			i+=strlen(winvncAddNewClient);

			// First, we have to parse the command line to get the filename to use
			int start, end;
			start=i;
			while (szCmdLine[start] <= ' ' && szCmdLine[start] != 0) start++;
			end = start;
			while (szCmdLine[end] > ' ') end++;

			// Was there a hostname (and optionally a port number) given?
			if (end-start > 0)
			{
				char *name = new char[end-start+1];
				if (name != 0) {
					strncpy(name, &(szCmdLine[start]), end-start);
					name[end-start] = 0;

					int port = INCOMING_PORT_OFFSET;
					char *portp = strchr(name, ':');
					if (portp) {
						*portp++ = '\0';
						if (*portp == ':') {
							port = atoi(++portp);	// Port number after "::"
						} else {
							port += atoi(portp);	// Display number after ":"
						}
					}

					VCard32 address = VSocket::Resolve(name);
					if (address != 0) {
						// Post the IP address to the server
						vncService::PostAddNewClient(address, port);
					}
					delete [] name;
				}
				i=end;
				continue;
			}
			else 
			{
				// Tell the server to show the Add New Client dialog
				vncService::PostAddNewClient(0, 0);
			}
			continue;
		}

		// Either the user gave the -help option or there is something odd on the cmd-line!

		// Show the usage dialog
		MessageBox(NULL, winvncUsageText, "IICServer Usage", MB_OK | MB_ICONINFORMATION);
		break;
	};
//#endif
	// If no arguments were given then just run
	// Starts IIC -- handle application sharing
	if (!argfound)
		return WinVNCAppMain(NULL);
	// Ends IIC -- handle application sharing

	return 0;
}

// IIC
void ReportInternalError(bool closeMenu)
{
	if (menu != NULL)
	{
		menu->SendCallbackMessage(CB_InternalError, 0L);
		if (closeMenu)
			menu->DestroyWindow();
	}
}


// This is the main routine for WinVNC when running as an application
// (under Windows 95 or Windows NT)
// Under NT, WinVNC can also run as a service.  The WinVNCServerMain routine,
// defined in the vncService header, is used instead when running as a service.

int WinVNCAppMain(const char* appsToShare, BOOL aSharePPT)	// IIC -- handle application sharing
{
#ifdef CRASH_ENABLED
	LPVOID lpvState = Install(NULL,  "rudi.de.vos@skynet.be", "UltraVnc RC109-build040703");
#endif
	// Set this process to be the last application to be shut down.
	// Check for previous instances of WinVNC!
	vncInstHandler instancehan;
	if (!instancehan.Init())
	{
		// We don't allow multiple instances!
		MessageBox(NULL, "Another instance of "PRODNAME" is already running", szAppName, MB_OK);
		return 0;
	}

	// CREATE SERVER
	server = new vncServer(appsToShare, start_time, enable_recording, share_allinstances);

	// Set the name and port number
	server->SetDisplayName(szDisplayName); // IIC
	server->SetSharePPT(aSharePPT);	// IIC
	server->SetName(szAppName);
	vnclog.Print(LL_STATE, VNCLOG("server created ok\n"));
	///uninstall driver before cont
	if (OSVersion()==1)
	{
		vncVideoDriver *videodriver;
		videodriver=NULL;
		videodriver=new vncVideoDriver;
		if (videodriver!=NULL)
			{
				videodriver->UnMapSharedbuffers();
				videodriver->UnMapSharedbuffers2();
				videodriver->Activate_video_driver();
				videodriver->DesActivate_video_driver();
				if (videodriver!=NULL) delete videodriver;
				videodriver=NULL;
			}
	}
	

	// Create tray icon & menu if we're running as an app
	menu = new vncMenu(server);
	if (menu == NULL)
	{
		vnclog.Print(LL_INTERR, VNCLOG("failed to create tray menu\n"));
		PostQuitMessage(0);
	}

	// IIC -- vncMenu loads initial values from the registry
	{
		vnclog.SetMode(VNCLog::ToNetLog);	// always write to file

#if 0
		HANDLE hEvent = CreateEvent(NULL, TRUE, FALSE, IICDEBUGSESSIONEVENT);
		if (hEvent!=NULL && WaitForSingleObject(hEvent, 0)==WAIT_OBJECT_0)
		{
			vnclog.SetLevel(LL_INTINFO);
			debugf("Detail log is on for mtgshare.\r\n");
			CloseHandle(hEvent);
		}
#endif

	}

	MSG msg;

	try
	{
		// Now enter the message handling loop until told to quit!
		while (GetMessage(&msg, NULL, 0,0) ) {
			//vnclog.Print(LL_INTINFO, VNCLOG("Message %d received\n"), msg.message);

			TranslateMessage(&msg);  // convert key ups and downs to chars
			DispatchMessage(&msg);
		}
	}
	catch (...)
	{
		menu->SendCallbackMessage(CB_InternalError, 0L);
	}

	try
	{
		vnclog.Print(LL_STATE, VNCLOG("shutting down server\n"));

		if (menu != NULL)
			delete menu;
		if (server != NULL)
			delete server;
	}
	catch (...)
	{
		// Ignore traps while shutting down
	}

	return msg.wParam;
};
