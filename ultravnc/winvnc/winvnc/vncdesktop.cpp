//  Copyright (C) 2002 Ultr@VNC Team Members. All Rights Reserved.
//  Copyright (C) 2000-2002 Const Kaplinsky. All Rights Reserved.
//  Copyright (C) 2002 RealVNC Ltd. All Rights Reserved.
//  Copyright (C) 1999 AT&T Laboratories Cambridge. All Rights Reserved.
//
//  This file is part of the VNC system.
//
//  The VNC system is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// If the source code for the VNC system is not available from the place 
// whence you received this file, check http://www.uk.research.att.com/vnc or contact
// the authors on vnc@uk.research.att.com for information on obtaining it.

// vncDesktop implementation

// System headers
#include <assert.h>
#include "stdhdrs.h"

// Custom headers
//#include <WinAble.h>
#include <omnithread.h>
#include "WinVNC.h"
#include "VNCHooks\VNCHooks.h"
#include "vncServer.h"
#include "vncKeymap.h"
#include "rfbRegion.h"
#include "rfbRect.h"
#include "vncDesktop.h"
#include "vncService.h"
// Modif rdv@2002 - v1.1.x - videodriver
#include "../vdacctray/vncOSVersion.h"
#include "../../netlib/log.h"	// IIC

#include "mmSystem.h" // sf@2002
#include "TextChat.h" // sf@2002

#include "directx.h"	// IIC


// Constants
const UINT RFB_SCREEN_UPDATE = RegisterWindowMessage("IICServer.Update.DrawRect");
const UINT RFB_COPYRECT_UPDATE = RegisterWindowMessage("IICServer.Update.CopyRect");
const UINT RFB_MOUSE_UPDATE = RegisterWindowMessage("IICServer.Update.Mouse");
const char szDesktopSink[] = "IICServer desktop sink";
const UINT RFB_WINMSG_UPDATE = RegisterWindowMessage("IICServer.Update.MessageId");
const UINT RFB_MOUSE_CLICK_MSG = RegisterWindowMessage("IICServer.Update.Mouse.Click");

extern BOOL CALLBACK EnumWndProc(HWND hwnd, LPARAM lParam);

void TRACE(char* msg, ...)
{
#if _DEBUG
	static char lpBuffer[1024];

	va_list args;
	va_start( args, msg );		
	vsprintf(lpBuffer, msg, args);
	va_end( args );				
	OutputDebugString(lpBuffer);
#endif
}


// The desktop handler thread
// This handles the messages posted by RFBLib to the vncDesktop window

class vncDesktopThread : public omni_thread
{
public:
	vncDesktopThread() {m_returnsig = NULL;};
protected:
	~vncDesktopThread() {if (m_returnsig != NULL) delete m_returnsig;};
public:
	virtual BOOL Init(vncDesktop *desktop, vncServer *server);
	virtual void *run_undetached(void *arg);
	void *run_internal(void *arg);
	virtual void ReturnVal(BOOL result);
	void PollWindow(rfb::Region2D &rgn, HWND hwnd);
	// Modif rdv@2002 - v1.1.x - videodriver
	virtual BOOL handle_driver_changes(rfb::Region2D &rgncache,rfb::UpdateTracker &tracker,rfb::Region2D &rgnText,rfb::Region2D &rgnSolid);
	virtual void copy_bitmaps_to_buffer(ULONG i,rfb::Region2D &rgncache,rfb::UpdateTracker &tracker,rfb::Region2D &rgnText,rfb::Region2D &rgnSolid);
	virtual bool ClipRect(int *x, int *y, int *w, int *h,int cx, int cy, int cw, int ch) ;

	//IIC
	BOOL HandleMsg(MSG& msg);


protected:
	vncServer *m_server;
	vncDesktop *m_desktop;

	omni_mutex m_returnLock;
	omni_condition *m_returnsig;
	BOOL m_return;
	BOOL m_returnset;
	bool m_screen_moved;
	bool lastsend;

	// IIC
	rfb::Region2D rgncache;
	BOOL idle_skip;
	ULONG idle_skip_count;
	DWORD dwStartTick;
};

bool
vncDesktopThread::ClipRect(int *x, int *y, int *w, int *h,
	    int cx, int cy, int cw, int ch) {
  if (*x < cx) {
    *w -= (cx-*x);
    *x = cx;
  }
  if (*y < cy) {
    *h -= (cy-*y);
    *y = cy;
  }
  if (*x+*w > cx+cw) {
    *w = (cx+cw)-*x;
  }
  if (*y+*h > cy+ch) {
    *h = (cy+ch)-*y;
  }
  return (*w>0) && (*h>0);
}

// Modif rdv@2002 - v1.1.x - videodriver
void
vncDesktopThread::copy_bitmaps_to_buffer(ULONG i,rfb::Region2D &rgncache,rfb::UpdateTracker &tracker,rfb::Region2D &rgnText,rfb::Region2D &rgnSolid)
{
		rfb::Rect rect;
		rfb::Point point;
		int x = m_desktop->m_videodriver->bufdata.buffer->pointrect[i].rect.left;
		int w = m_desktop->m_videodriver->bufdata.buffer->pointrect[i].rect.right-m_desktop->m_videodriver->bufdata.buffer->pointrect[i].rect.left;
		int y = m_desktop->m_videodriver->bufdata.buffer->pointrect[i].rect.top;
		int h = m_desktop->m_videodriver->bufdata.buffer->pointrect[i].rect.bottom-m_desktop->m_videodriver->bufdata.buffer->pointrect[i].rect.top;
		if (!ClipRect(&x, &y, &w, &h, m_desktop->m_bmrect.tl.x, m_desktop->m_bmrect.tl.y,
			m_desktop->m_bmrect.br.x-m_desktop->m_bmrect.tl.x, m_desktop->m_bmrect.br.y-m_desktop->m_bmrect.tl.y)) return;
		rect.tl.x = x;
		rect.br.x = x+w;
		rect.tl.y = y;
		rect.br.y = y+h;

		/*rect = rect.intersect(m_desktop->m_Cliprect);
		if (rect.is_empty()) return;*/

		
		switch(m_desktop->m_videodriver->bufdata.buffer->pointrect[i].type)
		{
		case SCREEN_SCREEN:
			{
				if (!m_screen_moved)
				{
				int dx=m_desktop->m_videodriver->bufdata.buffer->pointrect[i].point.x-rect.tl.x;
				int dy=m_desktop->m_videodriver->bufdata.buffer->pointrect[i].point.y-rect.tl.y;
				x=x+dx;;
				y=y+dy;;
				if (!ClipRect(&x,&y,&w,&h,m_desktop->m_bmrect.tl.x, m_desktop->m_bmrect.tl.y,
					m_desktop->m_bmrect.br.x-m_desktop->m_bmrect.tl.x, m_desktop->m_bmrect.br.y-m_desktop->m_bmrect.tl.y)) return;
				rect.tl.x=x-dx;
				rect.tl.y=y-dy;
				rect.br.x=x+w-dx;
				rect.br.y=y+h-dy;

				rfb::Point delta = rfb::Point(rect.tl.x-m_desktop->m_videodriver->bufdata.buffer->pointrect[i].point.x,
										      rect.tl.y-m_desktop->m_videodriver->bufdata.buffer->pointrect[i].point.y);
				rgnSolid = rgnSolid.subtract(rect);
				rgnText = rgnText.subtract(rect);
				rgncache=rgncache.union_(rect);
				tracker.add_copied(rect, delta);
				}
				else
				{
					rgnSolid = rgnSolid.subtract(rect);
					rgnText = rgnText.subtract(rect);
					rgncache=rgncache.union_(rect);
				}
				break;
			}
		case SOLIDFILL:
			rgncache = rgncache.subtract(rect);
			rgnText = rgnText.subtract(rect);
			rgnSolid = rgnSolid.union_(rect);
			break;
		case TEXTOUT:
			rgncache = rgncache.subtract(rect);
			rgnSolid = rgnSolid.subtract(rect);
			rgnText = rgnText.union_(rect);
			break;
		case BLEND:
		case TRANS:
		case PLG:
		case BLIT:
			rgnSolid = rgnSolid.subtract(rect);
			rgnText = rgnText.subtract(rect);
			rgncache=rgncache.union_(rect);
			break;
		}
}

// Modif rdv@2002 - v1.1.x - videodriver
BOOL
vncDesktopThread::handle_driver_changes(rfb::Region2D &rgncache,rfb::UpdateTracker &tracker,rfb::Region2D &rgnText,rfb::Region2D &rgnSolid)
{   
	int oldaantal=m_desktop->m_videodriver->oldaantal;
	int counter=m_desktop->m_videodriver->bufdata.buffer->counter;
	if (oldaantal==counter)
					{
						if (lastsend) return FALSE;
						if (!lastsend)
							{
								lastsend=true;
								copy_bitmaps_to_buffer(counter,rgncache,tracker,rgnText,rgnSolid);
								return TRUE;
							}
					}
	lastsend=false;
	m_screen_moved=m_desktop->CalcCopyRects(tracker);
	if (oldaantal<counter)
		{
			for (ULONG i =oldaantal+1; i<counter;i++)
				{
					copy_bitmaps_to_buffer(i,rgncache,tracker,rgnText,rgnSolid);
				}

		}
	else
		{
		    ULONG i = 0;
			for (i =oldaantal+1;i<MAXCHANGES_BUF;i++)
				{
					copy_bitmaps_to_buffer(i,rgncache,tracker,rgnText,rgnSolid);
				}
			for (i=1;i<counter;i++)
				{
					copy_bitmaps_to_buffer(i,rgncache,tracker,rgnText,rgnSolid);
				}
		}
			
	m_desktop->m_videodriver->oldaantal=counter;
	return TRUE;
}

BOOL
vncDesktopThread::Init(vncDesktop *desktop, vncServer *server)
{
	// Save the server pointer
	m_server = server;
	m_desktop = desktop;
	m_returnset = FALSE;
	m_returnsig = new omni_condition(&m_returnLock);

	// Start the thread
	start_undetached();

	// Wait for the thread to let us know if it failed to init
	{	omni_mutex_lock l(m_returnLock);

		while (!m_returnset)
		{
			m_returnsig->wait();
		}
	}
	return m_return;
}

void
vncDesktopThread::ReturnVal(BOOL result)
{
	omni_mutex_lock l(m_returnLock);

	m_returnset = TRUE;
	m_return = result;
	m_returnsig->signal();
}

void
vncDesktopThread::PollWindow(rfb::Region2D &rgn, HWND hwnd)
{
	// Are we set to low-load polling?
	if (m_server->PollOnEventOnly())
	{
		// Yes, so only poll if the remote user has done something
		if (!m_server->RemoteEventReceived()) {
			return;
		}
	}

	// Does the client want us to poll only console windows?
	if (m_desktop->m_server->PollConsoleOnly())
	{
		char classname[20];

		// Yes, so check that this is a console window...
		if (GetClassName(hwnd, classname, sizeof(classname))) {
			if ((strcmp(classname, "tty") != 0) &&
				(strcmp(classname, "ConsoleWindowClass") != 0)) {
				return;
			}
		}
	}

	RECT rect;

	// Get the rectangle
	if (GetWindowRect(hwnd, &rect)) {
		rfb::Rect wrect = rfb::Rect(rect).intersect(m_desktop->m_Cliprect);
		if (!wrect.is_empty()) {
			rgn = rgn.union_(rfb::Region2D(wrect));
		}
	}
}

BOOL CALLBACK EnumChildProc(HWND hWnd, LPARAM lParam) 
{ 
	HDC hDC = GetDC(hWnd);
	RECT rect;
	GetClientRect(hWnd, &rect);
	FillRect(hDC, &rect, (HBRUSH)GetStockObject(WHITE_BRUSH));
	ReleaseDC(hWnd, hDC);

    return TRUE; 
} 

// IIC
BOOL vncDesktopThread::HandleMsg(MSG& msg)
{
	static DWORD dwIdleCount = GetTickCount();

	// in agent mode, if recording is not enabled, don't capture screen updates
	if (m_server->IsAgentMode() && !m_server->EnableRecording())
	{
		if (msg.message == WM_QUIT)
		{
			return FALSE;
		}
		else
		{
			// Process any other messages normally
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			idle_skip = TRUE;
		}
	}
	else
	{
		// IIC
		if (msg.message == RFB_WINMSG_UPDATE)
		{
			dwIdleCount = GetTickCount();	// reset idle count
			if (m_server->AppSharing()||m_server->IsSharingPPT())
			{
				UINT windowMsgID = msg.wParam;
				HWND hWnd = (HWND)msg.lParam;
				if ((windowMsgID==WM_ACTIVATE|| /*windowMsgID==WM_ERASEBKGND||*/
					 windowMsgID==WM_WINDOWPOSCHANGING||windowMsgID==WM_WINDOWPOSCHANGED) && 
					m_desktop->IsSameProcess(hWnd, FALSE)==FALSE &&
					GetForegroundWindow()!=GetAncestor(hWnd, GA_ROOTOWNER))	 // workaround switching windows, but use delay when check process
				{
					if (!dwStartTick)
					{
						debugf("Windows switched, suspending polling\n");
					}
					dwStartTick = GetTickCount();

					idle_skip = TRUE;
				}
			}
		}
		else if (msg.message == RFB_SCREEN_UPDATE)
		{
			dwIdleCount = GetTickCount();	// reset idle count
			// Process an incoming update event
			// An area of the screen has changed
			// ddihook also use RFB_SCREEN_UPDATE
			// to passe the updates
			rfb::Rect rect;
			rect.tl = rfb::Point((SHORT)LOWORD(msg.wParam), (SHORT)HIWORD(msg.wParam));
			rect.br = rfb::Point((SHORT)LOWORD(msg.lParam), (SHORT)HIWORD(msg.lParam));
			rect = rect.intersect(m_desktop->m_Cliprect);
			if (!rect.is_empty())
			{
				rgncache = rgncache.union_(rfb::Region2D(rect));
			}
			idle_skip = TRUE;
		}
		else if (msg.message == RFB_MOUSE_UPDATE)
		{
			dwIdleCount = GetTickCount();	// reset idle count
			// Process an incoming mouse event
			// Save the cursor ID
			m_desktop->SetCursor((HCURSOR) msg.wParam);
			idle_skip = TRUE;
		}
		else if (msg.message == RFB_MOUSE_CLICK_MSG)
		{
			dwIdleCount = GetTickCount();	// reset idle count

			DWORD dwTick = GetTickCount();
			if (m_desktop->m_remote_point.x==-1 && m_desktop->m_remote_point.y==-1 &&
				(dwTick - m_desktop->m_remote_click_time)>200)
			{
				m_server->GrantControl(-1);
			}

			m_desktop->m_remote_point.x = m_desktop->m_remote_point.y = -1;
		}
		else if (msg.message == WM_QUIT)
		{
			return FALSE;
		}
		else
		{
			// Process any other messages normally
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			idle_skip = TRUE;

			if ((GetTickCount() - dwIdleCount) > 5000)	// if have not got any message from the hook dll, try to reset hook
			{
				dwIdleCount = GetTickCount();	// reset idle count

				// Unset hook first
				m_desktop->StartStophookdll(false);

				// Set hook again
				m_desktop->StartStophookdll(true);
			}
		}
	}
	return TRUE;
}

// IIC - trap exceptions and notify containing window
void *
vncDesktopThread::run_undetached(void *arg)
{
	try
	{
		return run_internal(arg);
	}
	catch (...)
	{
		ReportInternalError(true);
	}

	return NULL;
}

void *
vncDesktopThread::run_internal(void *arg)
{
	// Save the thread's "home" desktop, under NT (no effect under 9x)
	HDESK home_desktop = GetThreadDesktop(GetCurrentThreadId());

	// Attempt to initialise and return success or failure
	m_desktop->KillScreenSaver();
	if (!m_desktop->Startup())
	{
		vncService::SelectHDESK(home_desktop);
		ReturnVal(FALSE);
		return NULL;
	}
	
	// Succeeded to initialise ok
	ReturnVal(TRUE);

	m_desktop->InitHookSettings(); // sf@2003 - Done here to take into account if the driver is actually activated

	// START PROCESSING DESKTOP MESSAGES

	// We set a flag inside the desktop handler here, to indicate it's now safe
	// to handle clipboard messages
	m_desktop->SetClipboardActive(TRUE);

	// All changes in the state of the display are stored in a local
	// UpdateTracker object, and are flushed to the vncServer whenever
	// client updates are about to be triggered
	rfb::SimpleUpdateTracker clipped_updates;
	rfb::ClippedUpdateTracker updates(clipped_updates, m_desktop->m_Cliprect);
	clipped_updates.enable_copyrect(true);
	rfb::Region2D rgnSolid;
	rfb::Region2D rgnText;
	rfb::Region2D rgnclearcache;


	// Incoming update messages are collated into a single region cache
	// The region cache areas are checked for changes before an update
	// is triggered, and the changed areas are passed to the UpdateTracker
	rgncache = m_desktop->m_Cliprect;
	m_server->SetScreenOffset(m_desktop->m_ScreenOffsetx,m_desktop->m_ScreenOffsety);

	// The previous cursor position is stored, to allow us to erase the
	// old instance whenever it moves.
	rfb::Point oldcursorpos;
	// The driver gives smaller rectangles to check
	// if Accuracy is 4 you eliminate pointer updates
	if (m_desktop->VideoBuffer() && m_desktop->m_hookdriver)
		m_desktop->m_buffer.SetAccuracy(2);

	// Set the hook thread to a high priority
	// set_priority(omni_thread::PRIORITY_HIGH); // sf@2002

	m_desktop->m_SWSizeChanged=FALSE;
	m_desktop->m_SWtoDesktop=FALSE;
	m_desktop->m_SWmoved=FALSE;
	m_desktop->On_Off_hookdll=true;	// IIC -- don't know why it was commented out, left it uninitialized
	m_desktop->Hookdll_Changed = true;
	m_desktop->m_displaychanged=false;
	m_desktop->m_driverchanged=false;
	m_desktop->m_hookswitch=false;
	m_desktop->m_hookinited = FALSE;
	MSG msg;

	int last_sent = 0;
	
	// IIC
	m_desktop->m_remote_point.x = m_desktop->m_remote_point.y = -1;
	m_desktop->m_remote_click_time = 0;
	dwStartTick = 0;

	idle_skip = FALSE;
	idle_skip_count = 0;

	while (TRUE)
	{
		if (!PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE)) {
			// IIC

			// in agent mode, if recording is not enabled, don't capture screen updates
			static bool log_record = false;
			if (m_server->IsAgentMode() && !m_server->EnableRecording())
			{
				if (!log_record) 
				{
					debugf("Recording not on\n");
					log_record = true;
				}
				Sleep(200);
				continue;
			}
			log_record = false;

			// delay update check a bit to give window just made visible a chance to paint itself
			if (dwStartTick!=0)
			{
				if ((GetTickCount() - dwStartTick) < 850) {
					Sleep(UsingDirectX() ? 40 : 10);
					continue;
				}
				dwStartTick=0;
			}

			//
			// - MESSAGE QUEUE EMPTY
			// Whenever the message queue becomes empty, we check to see whether
			// there are updates to be passed to clients.
			//if (!m_desktop->IsVideoDriverEnabled())
			if (!m_desktop->m_hookdriver)
			{
				if (idle_skip) {
					if (idle_skip_count++ < 4) 
					{
						// IIC - increase polling interval to reduce CPU load
						Sleep(UsingDirectX() ? 40 : 10);
						continue;
					}
					idle_skip = FALSE;
				}
			}
			idle_skip_count = 0;

			// Clear the triggered flag
			m_desktop->m_update_triggered = FALSE;

			if (m_server->IsSharingPPT() && m_server->SingleWindow()==FALSE)
			{
				Sleep(200);
				continue;	// not ready for capturing screen, window handle is not set yet
			}

			// We periodicaly test if there's a slow client connected
			// In this case, we disable Video Hook Driver if it's enabled 
			// and  force System Hook instead
			// It's a temporary change, we don't modify the server properties in the registry
			// Disabled:
		/*	if (m_desktop->m_hookdriver)
			{
				long lTime = timeGetTime();
				if ( lTime - m_desktop->m_lLastSlowClientTestTime > 10000) // 10s
				{
					if (m_server->IsThereASlowClient())
					{
						m_server->Driver(false);
						m_server->Hook(true);
						vnclog.Print(LL_INTERR, VNCLOG("Slow client detected \n"));
						m_desktop->SethookMechanism(true, false);
					}
					m_desktop->m_lLastSlowClientTestTime = lTime;
				}
			}*/
			//*******************************************************
			// SCREEN DISPLAY HAS CHANGED, RESTART DRIVER
			//*******************************************************
			if (m_desktop->m_driverchanged)
			{
				m_desktop->m_videodriver->blocked=true;
				m_desktop->m_videodriver->UnMapSharedbuffers();
				m_desktop->m_videodriver->Activate_video_driver();
				m_desktop->m_videodriver->MapSharedbuffers();
				m_desktop->m_displaychanged = TRUE;
				m_desktop->m_driverchanged = false;
			}
			//*******************************************************
			// HOOKDLL START STOP need to be executed from the thread
			//*******************************************************
			if (m_desktop->Hookdll_Changed && !m_desktop->m_hookswitch)
			{
				m_desktop->StartStophookdll(m_desktop->On_Off_hookdll);
				if (m_desktop->On_Off_hookdll)
					m_desktop->m_hOldcursor = NULL; // Force mouse cursor grabbing if hookdll On
				// Todo: in case of hookdriver Off - Hoodll On -> hookdriver On - Hoodll Off
				// we must send an empty mouse cursor to the clients so they get rid of their local
				// mouse cursor bitmap
				m_desktop->Hookdll_Changed=false;
			}
			//*******************************************************
			// CHECK SCREEN FORMAT SIZE AND SW TO FULL DESKTOP SWITCH
			//*******************************************************
			if ( m_desktop->m_displaychanged || !vncService::InputDesktopSelected() || m_desktop->m_SWtoDesktop || m_desktop->m_hookswitch)
			{
				rfbServerInitMsg oldscrinfo = m_desktop->m_scrinfo;
				
				if (!m_desktop->m_SWtoDesktop)
				{
					// Attempt to close the old hooks
					// shutdown(true) driver is reinstalled without shutdown,(shutdown need a 640x480x8 switch)
					if (!m_desktop->Shutdown(false))
					{
						m_server->KillAuthClients();
						break;
					}					
					
					BOOL fHookDriverWanted = m_desktop->m_hookdriver;

					// Now attempt to re-install them!
					if (!m_desktop->Startup())
					{
						m_server->KillAuthClients();
						break;
					}

					// sf@2003 - After a new Startup(), we check if the required video driver
					// is actually available. If not, we force hookdll
					// No need for m_hookswitch again because the driver is NOT available anyway.
					// All the following cases are now handled:
					// 1. Desktop thread starts with "Video Driver" checked and no video driver available...
					//    -> HookDll forced (handled by the first InitHookSettings() after initial Startup() call
					// 2. Desktop Thread starts without "Video Driver" checked but available driver
					//    then the user checks "Video Driver" -> Video Driver used
					// 3. Desktop thread starts with "Video Driver" and available driver used
					//    Then driver is switched off (-> hookDll) 
					//    Then the driver is switched on again (-> hook driver used again)
					// 4. Desktop thread starts without "Video Driver" checked and no driver available
					//    then the users checks "Video Driver" 
					if (fHookDriverWanted && m_desktop->m_videodriver == NULL)
					{
						vnclog.Print(LL_INTERR, VNCLOG("m_videodriver == NULL \n"));
						m_desktop->SethookMechanism(true, false); 	// InitHookSettings() would work as well;
					}
				}

				m_desktop->m_displaychanged = FALSE;
				m_desktop->m_hookswitch = FALSE;
				m_desktop->Hookdll_Changed = m_desktop->On_Off_hookdll; // Set the hooks again if necessary !
				
				// Check that the screen info hasn't changed
				vnclog.Print(LL_INTINFO, VNCLOG("SCR: old screen format %dx%dx%d\n"),
					oldscrinfo.framebufferWidth,
					oldscrinfo.framebufferHeight,
					oldscrinfo.format.bitsPerPixel);
				vnclog.Print(LL_INTINFO, VNCLOG("SCR: new screen format %dx%dx%d\n"),
					m_desktop->m_scrinfo.framebufferWidth,
					m_desktop->m_scrinfo.framebufferHeight,
					m_desktop->m_scrinfo.format.bitsPerPixel);
				
				//Local pixel format has changed
				if (memcmp(&m_desktop->m_scrinfo.format, &oldscrinfo.format, sizeof(rfbPixelFormat)) != 0)
				{
					m_server->UpdateLocalFormat();
				}
				
				//size desktop has changed or SW to full desktop switch
				if ((m_desktop->m_scrinfo.framebufferWidth != oldscrinfo.framebufferWidth) ||
					(m_desktop->m_scrinfo.framebufferHeight != oldscrinfo.framebufferHeight ||
					m_desktop->m_SWtoDesktop==TRUE ))
				{
					POINT CursorPos;
					m_desktop->SWinit();
					m_desktop->m_SWtoDesktop=FALSE;
					m_server->SetNewSWSize(m_desktop->m_scrinfo.framebufferWidth,m_desktop->m_scrinfo.framebufferHeight,TRUE);
					m_desktop->m_qtrscreen = m_desktop->GetQuarterSize();
					m_server->UpdateLocalFormat();
					rgncache.clear();
					rgnSolid.clear();
					rgnText.clear();
					GetCursorPos(&CursorPos);
					m_desktop->m_cursorpos.tl = CursorPos;
					m_desktop->m_cursorpos.br = rfb::Point(GetSystemMetrics(SM_CXCURSOR),
						GetSystemMetrics(SM_CYCURSOR)).translate(CursorPos);
					//XXX
					// m_server->SetSWOffset(m_desktop->m_SWOffsetx,m_desktop->m_SWOffsety);
					//####################  BEGIN CRITICAL SHARED MEMEORY ACCESS POSSIBLE ############################
					if (m_desktop->m_videodriver!=NULL && !m_desktop->VideoBuffer())
					{
						//This should never happen
						// To prevent a server crash
						vnclog.Print(LL_INTINFO, VNCLOG(" 1:Critical driver state, Clients killed to prevent server crash\n"));
						m_server->KillAuthClients();
						break;
					}
					m_desktop->m_buffer.ClearCache();
					m_desktop->m_buffer.BlackBack();
					//m_desktop->m_buffer.ClearBack();
					//####################  END CRITICAL SHARED MEMEORY ACCESS POSSIBLE ############################
					debugf("Detected desktop size change\n");
				}
				// Adjust the UpdateTracker clip region
				updates.set_clip_region(m_desktop->m_Cliprect);
				// Add a full screen update to all the clients
				//rgncache = rgncache.union_(rfb::Region2D(m_desktop->m_Cliprect));
				m_server->UpdatePalette();
			}
			
			//*******************************************************************
			// SINGLE WINDOW 
			// size SW changed
			// Position change -->change offsets
			//*******************************************************************
			if (m_server->SingleWindow())
			{
				m_desktop->GetQuarterSize();
				// XXX
				// m_server->SetSWOffset(m_desktop->m_SWOffsetx,m_desktop->m_SWOffsety);
				//SW size changed
				if (m_desktop->m_SWSizeChanged)
				{
					rgncache.clear();
					rgnSolid.clear();
					rgnText.clear();
					m_desktop->m_buffer.ClearCache();	// IIC
					m_desktop->m_buffer.BlackBack();
					clipped_updates.clear();
					m_server->SetNewSWSize(m_desktop->m_SWWidth,m_desktop->m_SWHeight,FALSE);	// IIC -- it somehow corrupt the updates
					m_desktop->m_qtrscreen = m_desktop->GetQuarterSize();
					m_desktop->m_SWSizeChanged=FALSE;
					m_desktop->m_SWmoved = FALSE;	//IIC
					m_desktop->m_lGridsList.clear();	// IIC
					rgncache = rgncache.union_(rfb::Region2D(m_desktop->m_Cliprect));
					RECT rect;
					rect.left=m_desktop->m_Cliprect.tl.x;
					rect.top=m_desktop->m_Cliprect.tl.y;
					rect.right=m_desktop->m_Cliprect.br.x;
					rect.bottom=m_desktop->m_Cliprect.br.y;
					updates.set_clip_region(m_desktop->m_Cliprect);
					// XXX
					// m_server->SetSWOffset(m_desktop->m_SWOffsetx,m_desktop->m_SWOffsety);
					//####################  BEGIN CRITICAL SHARED MEMEORY ACCESS POSSIBLE ############################
					if (m_desktop->m_videodriver!=NULL && !m_desktop->VideoBuffer())
					{
						//This should never happen
						// To prevent a server crash
						vnclog.Print(LL_INTINFO, VNCLOG(" 2:Critical driver state, Clients killed to prevent server crash\n"));
						m_server->KillAuthClients();
						break;
					}
					//m_desktop->m_buffer.ClearBack();
					//####################  END CRITICAL SHARED MEMEORY ACCESS POSSIBLE ############################
					debugf("Detected new window size.\n");
//					InvalidateRect(NULL,&rect,TRUE);	// IIC -- avoid double painting, --may cause initial blank screen
					m_desktop->TriggerUpdate();		// So that the new size msg will be sent
//					rgncache = rgncache.union_(rfb::Region2D(m_desktop->m_Cliprect));

					continue;
				}
				//SW position changed
				else if (m_desktop->m_SWmoved)
				{
					rgncache.clear();
					rgnSolid.clear();
					rgnText.clear();
					m_desktop->m_buffer.ClearCache();	// IIC
					m_desktop->m_buffer.BlackBack();

					m_desktop->m_SWmoved=FALSE;
					updates.set_clip_region(m_desktop->m_Cliprect);
					m_server->SetNewSWSize(m_desktop->m_SWWidth,m_desktop->m_SWHeight,FALSE);	// IIC -- it somehow corrupt the updates
//					m_server->SetNewSWSize(GetSystemMetrics(SM_CXFULLSCREEN), GetSystemMetrics(SM_CYFULLSCREEN), FALSE);	// IIC
//IIC					m_server->SetSWOffset(m_desktop->m_SWOffsetx,m_desktop->m_SWOffsety);
					//####################  BEGIN CRITICAL SHARED MEMEORY ACCESS POSSIBLE ############################
					if (m_desktop->m_videodriver!=NULL && !m_desktop->VideoBuffer())
					{
						//This should never happen
						// To prevent a server crash
						vnclog.Print(LL_INTINFO, VNCLOG(" 3:Critical driver state, Clients killed to prevent server crash\n"));
						m_server->KillAuthClients();
						break;
					}
					m_desktop->m_buffer.ClearCache();	// IIC -- if cache remaining, checkregion will not return any changed region
					//####################  END CRITICAL SHARED MEMEORY ACCESS POSSIBLE ############################
					debugf("Detected new window move.\n");
					rgncache = rgncache.union_(rfb::Region2D(m_desktop->m_Cliprect));
					m_desktop->TriggerUpdate();

					continue;
				}
			}
			//***********************************************************************
			// Experimental pseudo driver, if fake dekstop is killed close connection
			//***********************************************************************
			if (m_desktop->DriverType==PSEUDO)
			{
				if (WaitForSingleObject(m_desktop->hAppliProcess, 2)==0)
				{
					m_server->KillAuthClients();
					break;
				}
			}	
			
			//
			// CALCULATE CHANGES
			//
			m_desktop->m_UltraEncoder_used=false;
			m_desktop->m_UltraEncoder_used=m_desktop->m_server->IsThereAUltraEncodingClient();
			if (m_desktop->m_server->UpdateWanted())	// IIC -- why
			{
				// Re-render the mouse's old location if it's moved
				BOOL cursormoved = FALSE;
				POINT cursorpos;
				if (GetCursorPos(&cursorpos) && 
					((cursorpos.x != oldcursorpos.x) ||
					(cursorpos.y != oldcursorpos.y)))
				{
					cursormoved = TRUE;
					oldcursorpos = rfb::Point(cursorpos);
				}
				
				// Disable polling with videodriver
				//if ( !m_desktop->IsVideoDriverEnabled() )
				if (!m_desktop->m_hookdriver)
				{
					//debugf("Polling for changes\n");

					// POLL PROBLEM AREAS
					// We add specific areas of the screen to the region cache,
					// causing them to be fetched for processing.
					
					if (cursormoved)
						m_desktop->m_buffer.SetAccuracy(m_desktop->m_server->TurboMode() ? 4 : 2); // Good Accuracy
					else
						m_desktop->m_buffer.SetAccuracy(2); // Average accuracy
					
					if (m_server->SingleWindow())
					{
						// Only need to poll shared window
						PollWindow(rgncache, m_desktop->m_Single_hWnd);
					}
					else
					{
						if (m_desktop->m_server->PollFullScreen())
						{
							// Don't poll the screen too heavily when the mouse moves
							// -> more time slices for Hooks
							//####################  BEGIN CRITICAL SHARED MEMEORY ACCESS POSSIBLE ############################
							if (m_desktop->m_videodriver!=NULL && !m_desktop->VideoBuffer())
							{
								vnclog.Print(LL_INTINFO, VNCLOG(" 4:Critical driver state,Reinit\n"));
							}
							else
							{
								// IIC - look for changes on selected monitor
								m_desktop->FastDetectChanges(rgncache, m_desktop->GetScreenRect(), 0, !cursormoved); 
							}
							//####################  END CRITICAL SHARED MEMEORY ACCESS POSSIBLE ############################
						}
						
						if (m_desktop->m_server->PollForeground())
						{
							// Get the window rectangle for the currently selected window
							HWND hwnd = GetForegroundWindow();
							if (hwnd != NULL)
							{
								//####################  BEGIN CRITICAL SHARED MEMEORY ACCESS POSSIBLE ############################
								if (m_desktop->m_videodriver!=NULL && !m_desktop->VideoBuffer())
								{
									vnclog.Print(LL_INTINFO, VNCLOG(" 5:Critical driver state,Reinit\n"));
								}
								else PollWindow(rgncache, hwnd);
								//####################  END CRITICAL SHARED MEMEORY ACCESS POSSIBLE ############################
							}
						}
						
						if (m_desktop->m_server->PollUnderCursor())
						{
							// Find the mouse position
							POINT mousepos;
							if (GetCursorPos(&mousepos))
							{
								// Find the window under the mouse
								HWND hwnd = WindowFromPoint(mousepos);
								if (hwnd != NULL)
								{
									//####################  BEGIN CRITICAL SHARED MEMEORY ACCESS POSSIBLE ############################
									if (m_desktop->m_videodriver!=NULL && !m_desktop->VideoBuffer())
									{
										vnclog.Print(LL_INTINFO, VNCLOG(" 6:Critical driver state,Reinit\n"));
									}
									else PollWindow(rgncache, hwnd);
									//####################  END CRITICAL SHARED MEMEORY ACCESS POSSIBLE ############################
								}
							}
						}
					}
				}
				else // Video driver is used
				{
					// long lTime = timeGetTime();
					if (cursormoved)
					{
						// if (lTime - m_desktop->m_lLastMouseUpdateTime < 200)
						// 	continue;
						m_desktop->m_buffer.SetAccuracy(m_desktop->m_server->TurboMode() ? 2 : 1);
						// m_desktop->m_lLastMouseUpdateTime = lTime;
					}
					else
						// 4 is not that bad...but not perfect (especially with tree branchs display)
						m_desktop->m_buffer.SetAccuracy(m_desktop->m_server->TurboMode() ? 4 : 2); 
				}
				
				
				// PROCESS THE MOUSE POINTER
				// Some of the hard work is done in clients, some here
				// This code fetches the desktop under the old pointer position
				// but the client is responsible for actually encoding and sending
				// it when required.
				// This code also renders the pointer and saves the rendered position
				// Clients include this when rendering updates.
				// The code is complicated in this way because we wish to avoid 
				// rendering parts of the screen the mouse moved through between
				// client updates, since in practice they will probably not have changed.
				
				//if (cursormoved && !m_desktop->IsVideoDriverEnabled())
				if (cursormoved && !m_desktop->m_hookdriver)
				{
					if (!m_desktop->m_cursorpos.is_empty())
						rgncache = rgncache.union_(rfb::Region2D(m_desktop->m_cursorpos));
				}
				
				
				{
					// Prevent any clients from accessing the Buffer
					omni_mutex_lock l(m_desktop->m_update_lock);
					
					// CHECK FOR COPYRECTS
					// This actually just checks where the Foreground window is
					//if (!m_desktop->IsVideoDriverEnabled() && !m_server->SingleWindow()) m_desktop->CalcCopyRects(updates);
					if (!m_desktop->m_hookdriver && !m_server->SingleWindow())  // IIC
						m_desktop->CalcCopyRects(updates);
					
					// GRAB THE DISPLAY
					// Fetch data from the display to our display cache.
					// Update the scaled rects when using server side scaling
					// something wrong inithooking again
					//####################  BEGIN CRITICAL SHARED MEMEORY ACCESS POSSIBLE ############################
					if (m_desktop->m_videodriver!=NULL && !m_desktop->VideoBuffer())
					{
						vnclog.Print(LL_INTINFO, VNCLOG(" 7:Critical driver state,Reinit\n"));
					}
					else if (!m_desktop->force_check_screen_format)
					{
						// sf@2002 - Added "&& m_desktop->m_hookdriver"
						// Otherwise we're still getting driver updates (from shared memory buffer)
						// after a m_hookdriver switching from on to off 
						// (and m_hookdll from off to on) that causes mouse cursor garbage,
						// or missing mouse cursor.
						if (m_desktop->VideoBuffer() && m_desktop->m_hookdriver)
						{
							m_desktop->m_buffer.GrabRegion(rgncache,true);
							m_desktop->m_buffer.GrabRegion(rgnText,true);
							m_desktop->m_buffer.GrabRegion(rgnSolid,true);
						}
						else
						{
							m_desktop->m_buffer.GrabRegion(rgncache,false);
							m_desktop->m_buffer.GrabRegion(rgnText,false);
							m_desktop->m_buffer.GrabRegion(rgnSolid,false);
						}

						// Modif rdv@2002 - v1.1.x - videodriver
						if (!m_desktop->m_hookdriver)	// && m_server->HasControl())		// IIC
						{
							// sf@2002 - v1.1.x - Mouse handling
							// If one client, send cursor shapes only when the cursor changes.
							// This is Disabled for now.
							if (m_desktop->m_server->IsXRichCursorEnabled() && !m_desktop->m_UltraEncoder_used)
							{
								if (m_desktop->m_hcursor != m_desktop->m_hOldcursor)
								{
									m_desktop->m_hOldcursor = m_desktop->m_hcursor;
									//  if we use shared memory -->no grabbing, we only need the position
									// if (!m_desktop->VideoBuffer())
										m_desktop->m_buffer.GrabMouse(); // Grab mouse cursor in all cases
									// else
										// m_desktop->CaptureMousePos();
									m_desktop->m_server->UpdateMouse();
									rgncache = rgncache.union_(rfb::Region2D(m_desktop->m_cursorpos));
								}
							}
							else // If several clients, send them all the mouse updates
							{
								// Render the mouse
								//if (!m_desktop->VideoBuffer())
									m_desktop->m_buffer.GrabMouse();
								//else 
								//	m_desktop->CaptureMousePos();
								
								if (cursormoved /*&& !m_desktop->m_buffer.IsCursorUpdatePending()*/) 
								{
									// Inform clients that it has moved
									m_desktop->m_server->UpdateMouse();
									// Get the buffer to fetch the pointer bitmap
									if (!m_desktop->m_cursorpos.is_empty())
										rgncache = rgncache.union_(rfb::Region2D(m_desktop->m_cursorpos));
								}
							}
						}	
						
						
						// SCAN THE CHANGED REGION FOR ACTUAL CHANGES
						// The hooks return hints as to areas that may have changed.
						// We check the suggested areas, and just send the ones that
						// have actually changed.
						// Note that we deliberately don't check the copyrect destination
						// here, to reduce the overhead & the likelihood of corrupting the
						// backbuffer contents.
						rfb::Region2D checkrgn;
						rfb::Region2D changedrgn;
						rfb::Region2D cachedrgn;
						rfb::Region2D Textrgn;
						rfb::Region2D Solidrgn;
						rfb::Region2D changedSolidrgn;
						rfb::Region2D changedTextrgn;
						
						//Update the backbuffer for the copyrect region
						if (!clipped_updates.get_copied_region().is_empty()) 
						{
							rfb::UpdateInfo update_info;
							rfb::RectVector::const_iterator i;
							clipped_updates.get_update(update_info);
							if (!update_info.copied.empty()) {
								for (i=update_info.copied.begin(); i!=update_info.copied.end(); i++) 						
									m_desktop->m_buffer.CopyRect(*i, update_info.copy_delta);
							}
						}
						//Remove the copyrect region from the other updates
						checkrgn = rgncache.subtract(clipped_updates.get_copied_region());
						Textrgn = rgnText.subtract(clipped_updates.get_copied_region());
						Solidrgn = rgnSolid.subtract(clipped_updates.get_copied_region());
						
						//Clear the solid and text regions and make sure the copyrect is checked next update
						rgnText.clear();
						rgnSolid.clear();
						rgncache = clipped_updates.get_copied_region();
						
						//Check all regions for changed and cached parts
						//This is very cpu intensive, only check once for all viewers
						//####################  BEGIN CRITICAL SHARED MEMEORY ACCESS POSSIBLE ############################
						if (m_desktop->m_videodriver!=NULL && !m_desktop->VideoBuffer())
						{
							vnclog.Print(LL_INTINFO, VNCLOG(" 8:Critical driver state,Reinit\n"));
						}
						else if (!m_desktop->force_check_screen_format)
						{
							if (!checkrgn.is_empty())
								m_desktop->m_buffer.CheckRegion(changedrgn,cachedrgn, checkrgn);
							if (!Solidrgn.is_empty())
								m_desktop->m_buffer.CheckRegion(changedSolidrgn,cachedrgn, Solidrgn);
							if (!Textrgn.is_empty())
								m_desktop->m_buffer.CheckRegion(changedTextrgn,cachedrgn, Textrgn);
							updates.add_changed(changedrgn);
							updates.add_cached(cachedrgn);
							updates.add_Solid(changedSolidrgn);
							updates.add_Text(changedTextrgn);
							
							// Copy updates to server object, which copies to
							// connected clients.
							clipped_updates.get_update(m_server->GetUpdateTracker());
						}
						//####################  END CRITICAL SHARED MEMEORY ACCESS POSSIBLE ############################
						}
					}
					//####################  END CRITICAL SHARED MEMEORY ACCESS POSSIBLE ############################
					// Clear the update tracker and region cache an solid
					clipped_updates.clear();
					if (m_desktop->OldPowerOffTimeout!=0)
					{
						SystemParametersInfo(SPI_SETPOWEROFFACTIVE, 1, NULL, 0);
						SendMessage(GetDesktopWindow(),WM_SYSCOMMAND,SC_MONITORPOWER,(LPARAM)2);
					}
			}
			
			// Now wait for more messages to be queued
//			if (!WaitMessage())
			DWORD res = MsgWaitForMultipleObjectsEx(0, NULL, 50, QS_ALLINPUT, MWMO_INPUTAVAILABLE);
			if (res == WAIT_OBJECT_0)
			{
				idle_skip = FALSE;
				continue;
			}
			else if (res != WAIT_TIMEOUT)
			{
				vnclog.Print(LL_INTERR, VNCLOG("WaitMessage() failed\n"));
				break;
			}
			// else we timed out and can poll for updates
		}
		// IIC
		else if (HandleMsg(msg)==FALSE)
			break;

		if (m_desktop->VideoBuffer() && m_desktop->m_hookdriver)
			handle_driver_changes(rgncache,updates,rgnText,rgnSolid);
	}
	
	m_desktop->SetClipboardActive(FALSE);
	
	vnclog.Print(LL_INTINFO, VNCLOG("quitting desktop server thread\n"));
	
	// Clear all the hooks and close windows, etc.
	m_desktop->SetDisableInput(false);
	m_desktop->Shutdown(false);
	m_server->SingleWindow(false);
	// Starts IIC -- handle application sharing
//	leave this, we may restart --- m_server->AppSharing(false);
	// Ends IIC -- handle application sharing
	
	// Clear the shift modifier keys, now that there are no remote clients
	vncKeymap::ClearShiftKeys();
	
	// Switch back into our home desktop, under NT (no effect under 9x)
	vncService::SelectHDESK(home_desktop);
	return NULL;
}


//
// // Modif sf@2002 - v1.1.0 - Optimization
//
// Here we try to speed up the Poll FullScreen function.
// Instead of adding the entire Rect to cached region,
// we divide it in 32/32 pixels blocks and only scan the top-left
// pixel of each block.
// If a pixel has changed, we find the smallest window containing this pixel
// and add the Window rect to the cached region
//
// This function is supposed to be a good compromise between
// speed and efficiency.
// The accuracy has been greatly improved and it now detects very
// small changes on screen that are not detected by the Hooks.
// This new accuracy is obtained by shifting the detection grid 
// in diagonale (the function works with several grids that are tested 
// alternatively). There's probably still room for improvements...
//
// NB: Changes generated by applications using hardware acceleration 
// won't be detected (In order to see a video played on the server using
// Media Player, the vncviewer user must desactivate "Hardware accelation"
// in the Media Player options). 
// 
// 
//
void
vncDesktop::FastDetectChanges(rfb::Region2D &rgn, rfb::Rect &rect, int nZone, bool fTurbo)
{
	RGBPixelList::iterator iPixelColor;
	RGBPixelList *pThePixelGrid;
	bool fInitGrid = false;
	bool fIncCycle = false;
	// For more accuracy, we could use 24 or even 16
	const int PIXEL_BLOCK_SIZE  = 32; // Pixels Grid definition
	const int GRID_OFFSET = 4;  // Pixels Grid shifting during cycle (in pixels)
								// The number of grid per zone is PIXEL_BLOCK_SIZE/GRID_OFFSET (must be int !)
	int x, y;
	int xo, yo;
	// WindowsList lWList;
	WindowsList::iterator iWindow;

	if (UsingDirectX()) 
	{
		RECT bufferRect = {rect.tl.x,rect.tl.y,rect.br.x,rect.br.y};
		BufferScreen(bufferRect, m_bytesPerRow);
	}

	// In turbo mode, we clear the list of windows at each iteration -> Lot of updates because 
	//  the same windows can be detected several times during a complete cycle
	// (To avoid this pb, we must update all the grids at the same coordinate when a pixel of 
	// one grid has changed -> later)
	// Otherwise we only clear it each time the Grid cycle loops -> Less updates, less framerate,
	// less CPU, less bandwidth
	if (fTurbo)
		m_lWList.clear();
	else if (m_nGridCycle == 0)
		m_lWList.clear();

	// Create all the Grids (for all the 5 zones (4 quarter screens + 1 full screen)
	// Actually, the quarter screens are not utilized in v1.1.0 but it does not
	// generate any overhead neither additionnal memory consumption (see below)
	if (m_lGridsList.empty())
	{
		for (int i = 0; i < (5 * PIXEL_BLOCK_SIZE / GRID_OFFSET); i++)
		{
			RGBPixelList *pList = new RGBPixelList;
			if (pList != NULL)
			{
				m_lGridsList.push_back(pList);
			    vnclog.Print(LL_INTINFO, VNCLOG("### PixelsGrid %d created !\n"), i);
			}
		}
	}

	// We test one zone at a time 
	vnclog.Print(LL_INTINFO, VNCLOG("### Polling Grid %d - SubGrid %d\n"), nZone, m_nGridCycle); 
	GridsList::iterator iGrid;
	int nIndex = 0;
	int nGridPos = (nZone * PIXEL_BLOCK_SIZE / GRID_OFFSET) + m_nGridCycle;

	iGrid = m_lGridsList.begin();
	while (nIndex != nGridPos)
	{
		iGrid++;
		nIndex++;
	}

	iPixelColor = ((RGBPixelList*)(*iGrid))->begin();
	pThePixelGrid = (RGBPixelList*) *iGrid;

	if (nZone == 0 || nZone == 4)
	{
	   vnclog.Print(LL_INTINFO, VNCLOG("### IncCycle Please !\n")); 
	   fIncCycle = true;
	}

	if (pThePixelGrid->empty())
	{
		vnclog.Print(LL_INTINFO, VNCLOG("### PixelsGrid Init\n"));
		fInitGrid = true;
	}

	int nOffset = GRID_OFFSET * m_nGridCycle;

	// We walk our way through the Grids
	for (y = rect.tl.y; y < rect.br.y; y += PIXEL_BLOCK_SIZE)
	{
		yo = y + nOffset;

		for (x = rect.tl.x; x < rect.br.x; x += PIXEL_BLOCK_SIZE)
		{
			xo = x + nOffset;

			// If init list
			if (fInitGrid)
			{
			   COLORREF PixelColor = CapturePixel(xo, yo);
			   pThePixelGrid->push_back(PixelColor);
			   vnclog.Print(LL_INTINFO, VNCLOG("### PixelsGrid Init : Pixel xo=%d - yo=%d - C=%ld\n"), xo, yo, (long)PixelColor); 
			   continue;
			}

			// Read the pixel's color on the screen
		    COLORREF PixelColor = CapturePixel(xo, yo);

			// If the pixel has changed
			if (*iPixelColor != PixelColor)
			{
				// Save the new Pixel in the list
				*iPixelColor = PixelColor;

				// Then find the corresponding Window
				POINT point;
				RECT rect;

				point.x = xo;
				point.y = yo;

				// Find the smallest, non-hidden, non-disabled Window containing this pixel
				// REM: We don't use ChildWindowFromPoint because we don't want of hidden windows
				HWND hDeskWnd = GetDesktopWindow();
				HWND hwnd = WindowFromPoint(point);
				bool fAlready = false;

				// Look if we've already detected this window
				for (iWindow = m_lWList.begin(); iWindow != m_lWList.end(); iWindow++)
				{
					if (*iWindow == hwnd)
					{
						fAlready = true;
						break;
					}
				}
				
				// Add the corresponding rect to the cache region 
				if (!fAlready && /*(hwnd != hDeskWnd) &&*/ GetWindowRect(hwnd, &rect))
				{
					rfb::Rect wrect = rfb::Rect(rect).intersect(m_Cliprect);
					if (!wrect.is_empty())
					{
						debugf("Adding region at %d,%d - %d,%d\n", wrect.tl.x, wrect.tl.y, wrect.br.x, wrect.br.y);
						rgn = rgn.union_(rfb::Region2D(wrect));
						m_lWList.push_back(hwnd);
					}
					char szName[64];
					GetWindowText(hwnd, szName, 64);
					vnclog.Print(LL_INTINFO, VNCLOG("### Changed Window : %s (at x=%d - y=%d)\n"), szName, x, y); 
					// return;
				}
			}

			iPixelColor++; // Next PixelColor in the list
		}
	}

	if (fIncCycle)
	{
	   m_nGridCycle = (m_nGridCycle + 1) % (PIXEL_BLOCK_SIZE / GRID_OFFSET);
	}

}




// Implementation

vncDesktop::vncDesktop()
{
	m_thread = NULL;

	m_hwnd = NULL;
	m_timerid = 0;
	m_hnextviewer = NULL;
	m_hcursor = NULL;
	m_hOldcursor = NULL; // sf@2002

	m_displaychanged = FALSE;
	m_update_triggered = FALSE;

	m_hrootdc = NULL;
	m_hmemdc = NULL;
	m_membitmap = NULL;

	m_initialClipBoardSeen = FALSE;

	m_foreground_window = NULL;
	m_foreground_window2 = NULL;	// IIC

	// Vars for Will Dean's DIBsection patch
	m_DIBbits = NULL;
	m_formatmunged = FALSE;

	m_clipboard_active = FALSE;

	m_pollingcycle = 0;

	// Modif sf@2002 - v1.1.0 
	m_lWList.clear();
    m_lGridsList.clear();
	m_nGridCycle = 0;

	// Modif sf@2002 - v1.1.0
	// m_lLastTempo = 0L;
	m_lLastMouseUpdateTime = 0L;
	m_lLastSlowClientTestTime = timeGetTime();

	// sf@2002 - TextChat - No more used for now
	// m_fTextChatRunning = false;
	// m_pCurrentTextChat = NULL;

	// Modif rdv@2002 - v1.1.x - videodriver
	m_videodriver=NULL;
	m_ScreenOffsetx=0;
	m_ScreenOffsety=0;

	OldPowerOffTimeout=0;
	force_check_screen_format=false;

	ddihook = FALSE;

	// IIC
	m_hSourceBitmap = NULL;
	m_hCursor = NULL;
}

vncDesktop::~vncDesktop()
{
	vnclog.Print(LL_INTINFO, VNCLOG("killing screen server\n"));

	// If we created a thread then here we delete it
	// The thread itself does most of the cleanup
	if(m_thread != NULL)
	{
		// Post a close message to quit our message handler thread
		PostMessage(Window(), WM_QUIT, 0, 0);

		// Join with the desktop handler thread
		void *returnval;
		m_thread->join(&returnval);
		m_thread = NULL;
	}
	SetDisableInput(false);
	// Let's call Shutdown just in case something went wrong...
	Shutdown(false);

	// Modif sf@2002
	m_lWList.clear();
	GridsList::iterator iGrid;
	for (iGrid = m_lGridsList.begin(); iGrid != m_lGridsList.end(); iGrid++)
	{
		if (*iGrid)
		{
			// Since we've replaced this:
			// "typedef std::list<RGBPixelList*> GridsList;"
			// with this:
			// "typedef std::list<void*> GridsList;"
			// we must be carefull to avoid memory leaks...
			((RGBPixelList*)(*iGrid))->clear();
			delete ((RGBPixelList*)(*iGrid));
		}
	}
	m_lGridsList.clear();
}

// Tell the desktop hooks to grab & update a particular rectangle
void
vncDesktop::QueueRect(const rfb::Rect &rect)
{
	ULONG vwParam = MAKELONG(rect.tl.x, rect.tl.y);
	ULONG vlParam = MAKELONG(rect.br.x, rect.br.y);

	PostMessage(Window(), RFB_SCREEN_UPDATE, vwParam, vlParam);
}
	
// Kick the desktop hooks to perform an update
void
vncDesktop::TriggerUpdate()
{
	// Note that we should really lock the update lock here,
	// but there are periodic timer updates anyway, so
	// we don't actually need to.  Something to think about.
	if (!m_update_triggered) {
		m_update_triggered = TRUE;
		PostMessage(Window(), WM_TIMER, 0, 0);
	}
}

// Routine to startup and install all the hooks and stuff
BOOL
vncDesktop::Startup()
{
	// Initialise the Desktop object

	// ***
	// vncService::SelectInputWinStation()
	

	if (!InitDesktop())
		return FALSE;

	// Modif rdv@2002 - v1.1.x - videodriver
	if (m_server->Driver())
	{
		if(OSVersion()==1 )
		{
			InitVideoDriver();
			InvalidateRect(NULL,NULL,TRUE);
		}
	}

	if (!InitBitmap())
		return FALSE;

	if (!ThunkBitmapInfo())
		return FALSE;

	EnableOptimisedBlits();

	if (!SetPixFormat())
		return FALSE;

	if (!SetPixShifts())
		return FALSE;
	
	if (!SetPalette())
		return FALSE;
	
	if (!InitWindow())
		return FALSE;

	//Driver 9.x (ddi) and not hooking$
/* Already Done in Sethookmechanism
	if (m_server->Driver() && OSVersion() == 4)
	{
		StartStopddihook(true);
	}//9.x no hooking

	if (m_hookdll)
	{
		StartStophookdll(true);			
	}
*/

	// Start a timer to handle Polling Mode.  The timer will cause
	// an "idle" event once every 1/10 second, which is necessary if Polling
	// Mode is being used, to cause TriggerUpdate to be called.
	m_timerid = SetTimer(m_hwnd, 1, 100, NULL);

	// Initialise the buffer object
	m_buffer.SetDesktop(this);

	// Create the quarter-screen rectangle for polling
	//m_qtrscreen = rfb::Rect(0, 0, m_bmrect.br.x, m_bmrect.br.y/4);
	// N.B. also sets clip rect size
	m_qtrscreen = GetQuarterSize();

	// Everything is ok, so return TRUE
	return TRUE;
}

// Routine to shutdown all the hooks and stuff
BOOL
vncDesktop::Shutdown(BOOL sizechange)
{
	// If we created a timer then kill it
	if (m_timerid != NULL)
		KillTimer(NULL, m_timerid);

	// If we created a window then kill it and the hooks
	if(m_hwnd != NULL)
	{	
		// Remove the system hooks
		UnSetHooks(GetCurrentThreadId());

		// The window is being closed - remove it from the viewer list
		ChangeClipboardChain(m_hwnd, m_hnextviewer);

		// Close the hook window
		DestroyWindow(m_hwnd);
		m_hwnd = NULL;
		m_hnextviewer = NULL;
	}

	// Now free all the bitmap stuff
	if (m_hrootdc != NULL)
	{
		// Release our device context
		// if m_hrootdc was created with createdc, we need to use deletedc to release 
		if (VideoBuffer())
		{
			if (!DeleteDC(m_hrootdc))
				vnclog.Print(LL_INTERR, VNCLOG("failed to DeleteDC hrootdc\n"));
		}
		else
		{
			if(ReleaseDC(NULL, m_hrootdc) == 0)
				{
					vnclog.Print(LL_INTERR, VNCLOG("failed to ReleaseDC\n"));
				}
		}
		m_hrootdc = NULL;
	}
	if (m_hmemdc != NULL)
	{
		// Release our device context
		if (!DeleteDC(m_hmemdc))
		{
			vnclog.Print(LL_INTERR, VNCLOG("failed to DeleteDC hmemdc\n"));
		}
		m_hmemdc = NULL;
	}
	if (m_membitmap != NULL)
	{
		// Release the custom bitmap, if any
		if (!DeleteObject(m_membitmap))
		{
			vnclog.Print(LL_INTERR, VNCLOG("failed to DeleteObject\n"));
		}
		m_membitmap = NULL;
	}

	// Modif rdv@2002 - v1.1.x - videodriver
	if (!sizechange) ShutdownVideoDriver(sizechange);

	// ***
	// vncService::SelectHomeWinStation();

	return TRUE;
}

// Routine to ensure we're on the correct NT desktop

BOOL
vncDesktop::InitDesktop()
{
	if (vncService::InputDesktopSelected())
		return TRUE;

	// Ask for the current input desktop
	return vncService::SelectDesktop(NULL);
}

// Routine used to close the screen saver, if it's active...

BOOL CALLBACK
KillScreenSaverFunc(HWND hwnd, LPARAM lParam)
{
	char buffer[256];

	// - ONLY try to close Screen-saver windows!!!
	if ((GetClassName(hwnd, buffer, 256) != 0) &&
		(strcmp(buffer, "WindowsScreenSaverClass") == 0))
		PostMessage(hwnd, WM_CLOSE, 0, 0);
	return TRUE;
}

void
vncDesktop::KillScreenSaver()
{
	OSVERSIONINFO osversioninfo;
	osversioninfo.dwOSVersionInfoSize = sizeof(osversioninfo);

	// Get the current OS version
	if (!GetVersionEx(&osversioninfo))
		return;

	vnclog.Print(LL_INTINFO, VNCLOG("KillScreenSaver...\n"));

	// How to kill the screen saver depends on the OS
	switch (osversioninfo.dwPlatformId)
	{
	case VER_PLATFORM_WIN32_WINDOWS:
		{
			// Windows 95

			// Fidn the ScreenSaverClass window
			HWND hsswnd = FindWindow ("WindowsScreenSaverClass", NULL);
			if (hsswnd != NULL)
				PostMessage(hsswnd, WM_CLOSE, 0, 0); 
			break;
		} 
	case VER_PLATFORM_WIN32_NT:
		{
			// Windows NT

			// Find the screensaver desktop
			HDESK hDesk = OpenDesktop(
				"Screen-saver",
				0,
				FALSE,
				DESKTOP_READOBJECTS | DESKTOP_WRITEOBJECTS
				);
			if (hDesk != NULL)
			{
				vnclog.Print(LL_INTINFO, VNCLOG("Killing ScreenSaver\n"));

				// Close all windows on the screen saver desktop
				EnumDesktopWindows(hDesk, (WNDENUMPROC) &KillScreenSaverFunc, 0);
				CloseDesktop(hDesk);
				// Pause long enough for the screen-saver to close
				//Sleep(2000);
				// Reset the screen saver so it can run again
				SystemParametersInfo(SPI_SETSCREENSAVEACTIVE, TRUE, 0, SPIF_SENDWININICHANGE); 
			}
			break;
		}
	}
}


//
// Modif sf@2002 - Single Window
//

// Starts IIC -- handle application sharing
BOOL CALLBACK EnumWindowsHnd(HWND hwnd, LPARAM arg)
{
	if (IsWindowVisible(hwnd)==FALSE)
		return TRUE;

	if (((vncDesktop*)arg)->GetServerPointer()->AppSharing())
	{
        if (((vncDesktop*)arg)->IsSameProcess(hwnd, TRUE))		// get current list of shared processes,NoDelay=TRUE
        {
			((vncDesktop*)arg)->m_SharedList.push_back(hwnd);
		}
	}
	else
	{
		int  nRet;
		char buffer[128];
		const char* szNameApps;

		szNameApps = ((vncDesktop*)arg)->GetServerPointer()->GetWindowName();
		nRet = GetWindowText(hwnd, buffer, sizeof(buffer)-2);
		if (nRet > 0)
		{
			((vncDesktop*)arg)->m_Single_hWnd = hwnd;
			return FALSE;
		}
	}

	return TRUE;
}

void vncDesktop::EnumAllSharedAppsWindows()
{
	if (m_server->AppSharing())
	{
		m_SharedList.clear();
		EnumWindows((WNDENUMPROC)EnumWindowsHnd, (LPARAM) this);

		WindowsList::iterator i;
		for (i=m_SharedList.begin(); i!=m_SharedList.end(); i++)
		{
			HWND hwnd = *i;

			if (!IsZoomed(hwnd))
			{
				ShowWindow(hwnd, SW_SHOWNORMAL);
			}

			SetForegroundWindow(hwnd);
		}
	}
}

// Ends IIC -- handle application sharing

// Starts IIC -- handling selected display

BOOL CALLBACK vncDesktop::MonitorEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData)
{
	vncDesktop *desktop = (vncDesktop*)dwData;

	MONITORINFOEX info;
    info.cbSize = sizeof(info);
    
    GetMonitorInfo(hMonitor, &info);

	if (stricmp(info.szDevice, desktop->GetServerPointer()->GetDisplayName()) == 0) 
	{
		debugf("Monitor: %s (%d %d %d %d)\n", info.szDevice, info.rcMonitor.left, info.rcMonitor.top, info.rcMonitor.right, info.rcMonitor.bottom);
	
		// Proceed normally if on the primary display
		if (info.rcMonitor.top == 0 && info.rcMonitor.left == 0)
			return FALSE;

		// This works mostly, but for some reason the cursor image can't be drawn on a DC created for a display device.
		/*desktop->m_hrootdc = CreateDC("DISPLAY", info.szDevice, NULL, NULL);
		desktop->m_ScreenOffsetx = info.rcMonitor.left;
		desktop->m_ScreenOffsety = info.rcMonitor.top;
		*/

		desktop->m_bmrect = rfb::Rect(info.rcMonitor.left, info.rcMonitor.top, info.rcMonitor.right, info.rcMonitor.bottom);

		SelectMonitor(info.szDevice, info.rcMonitor);

		return FALSE;
	}

    return TRUE;
}

void
vncDesktop::InitForDisplay()
{
	debugf("Looking for display %s\n", GetServerPointer()->GetDisplayName());
    EnumDisplayMonitors(NULL, NULL, MonitorEnumProc, (LPARAM)this);
}

// Ends IIC


BOOL
vncDesktop::InitBitmap()
{	
	// Get the device context for the whole screen and find it's size
	DriverType=NONE;
	if (OSVersion()==1) //XP W2k
		{	
			if (VideoBuffer())
				{
				/*	if(Check24bit())*/
						{
							{ //real mirror driver
							pEnumDisplayDevices pd;
							LPSTR driverName = "Winvnc video hook driver";
							BOOL DriverFound;
							DEVMODE devmode;
							FillMemory(&devmode, sizeof(DEVMODE), 0);
							devmode.dmSize = sizeof(DEVMODE);
							devmode.dmDriverExtra = 0;
							BOOL change = EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&devmode);
							devmode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
							pd = (pEnumDisplayDevices)GetProcAddress( LoadLibrary("USER32"), "EnumDisplayDevicesA");
							if (pd)
								{
									LPSTR deviceName=NULL;
									DISPLAY_DEVICE dd;
									ZeroMemory(&dd, sizeof(dd));
									dd.cb = sizeof(dd);
									devmode.dmDeviceName[0] = '\0';
									INT devNum = 0;
									BOOL result;
									DriverFound=false;
									while (result = (*pd)(NULL,devNum, &dd,0))
										{
											if (strcmp((const char *)&dd.DeviceString[0], driverName) == 0)
												{
													DriverFound=true;
													break;
												}
											devNum++;
										}
									if (DriverFound)
										{
											deviceName = (LPSTR)&dd.DeviceName[0];
											m_hrootdc = CreateDC("DISPLAY",deviceName,NULL,NULL);
											m_ScreenOffsetx=devmode.dmPosition.x;
											m_ScreenOffsety=devmode.dmPosition.y;
											if (m_hrootdc) DriverType=MIRROR;
										}
								}
							} //real mirror driver
							if (!m_hrootdc)
								{//perhaps virtual video driver is running
								pEnumDisplayDevices pd;
								LPSTR driverName = "vncvirdr";
								DEVMODE devmode;
								FillMemory(&devmode, sizeof(DEVMODE), 0);
								devmode.dmSize = sizeof(DEVMODE);
								devmode.dmDriverExtra = 0;
								BOOL change = EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&devmode);
								devmode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
								pd = (pEnumDisplayDevices)GetProcAddress( LoadLibrary("USER32"), "EnumDisplayDevicesA");
								if (pd)
									{
										LPSTR deviceName=NULL;
										DISPLAY_DEVICE dd;
										ZeroMemory(&dd, sizeof(dd));
										dd.cb = sizeof(dd);
										devmode.dmDeviceName[0] = '\0';
										INT devNum = 0;
										BOOL result;
										while (result = (*pd)(NULL,devNum, &dd,0))
											{
												if (strcmp((const char *)&dd.DeviceString[0], driverName) == 0) break;
												devNum++;
											}
										deviceName = (LPSTR)&dd.DeviceName[0];
										EnumDisplaySettings(deviceName,ENUM_CURRENT_SETTINGS,&devmode);
										m_hrootdc=NULL;
										m_hrootdc = CreateDC("DISPLAY",deviceName,NULL,NULL);
										m_ScreenOffsetx=devmode.dmPosition.x;
										m_ScreenOffsety=devmode.dmPosition.y;
										if (m_hrootdc) DriverType=PSEUDO;
										//SW and Pseudo driver don't work together
										m_server->SingleWindow(false);
										ActivatePseudoDesktop();
									}

								}//perhaps virtual video driver is running
					}//24bit
				}//VIDEOBUFFER
			}//OS

	// RDV SINGLE WINDOW
	if (m_server->SingleWindow() && m_Single_hWnd==NULL)
	{		
		EnumWindows((WNDENUMPROC)EnumWindowsHnd, (LPARAM) this);
	} 
	// IIC
	else if (m_server->AppSharing())
	{
		EnumAllSharedAppsWindows();
	}

	if (*m_server->GetDisplayName()) {
		InitForDisplay();
	}

	HRESULT stat = InitD3D(m_hwnd);
	if (FAILED(stat)) {
		errorf("Init3D failed");
	}

	if (m_hrootdc == NULL) {
		vnclog.Print(LL_INTERR, VNCLOG("No driver used \n"));
		m_hrootdc = ::GetDC(NULL);
		//m_hrootdc = ::CreateDC("DISPLAY", NULL, NULL, NULL);  // works but slow
		if (m_hrootdc == NULL) {
				vnclog.Print(LL_INTERR, VNCLOG("Failed m_rootdc \n"));
				return FALSE;
		}
	}

	if (m_bmrect.is_empty()) {
		m_bmrect = rfb::Rect(0, 0, 
			GetDeviceCaps(m_hrootdc, HORZRES),
			GetDeviceCaps(m_hrootdc, VERTRES));
	}

	int w = m_bmrect.br.x - m_bmrect.tl.x;
	int h = m_bmrect.br.y - m_bmrect.tl.y;
	vnclog.Print(LL_INTINFO, VNCLOG("bitmap dimensions are %d x %d\n"), w, h);

	// Create a compatible memory DC
	m_hmemdc = CreateCompatibleDC(m_hrootdc);
	if (m_hmemdc == NULL) {
		vnclog.Print(LL_INTERR, VNCLOG("failed to create compatibleDC(%d)\n"), GetLastError());
		return FALSE;
	}

	// Check that the device capabilities are ok
	if ((GetDeviceCaps(m_hrootdc, RASTERCAPS) & RC_BITBLT) == 0)
	{
		MessageBox(
			NULL,
			"root device doesn't support BitBlt\n"
			"IICServer cannot be used with this graphic device driver",
			szAppName,
			MB_ICONSTOP | MB_OK
			);
		return FALSE;
	}
	if ((GetDeviceCaps(m_hmemdc, RASTERCAPS) & RC_DI_BITMAP) == 0)
	{
		MessageBox(
			NULL,
			"memory device doesn't support GetDIBits\n"
			"IICServer cannot be used with this graphics device driver",
			szAppName,
			MB_ICONSTOP | MB_OK
			);
		return FALSE;
	}

	// Create the bitmap to be compatible with the ROOT DC!!!
	m_membitmap = CreateCompatibleBitmap(m_hrootdc, w, h);
	if (m_membitmap == NULL) {
		vnclog.Print(LL_INTERR, VNCLOG("failed to create memory bitmap(%d)\n"), GetLastError());
		return FALSE;
	}
	vnclog.Print(LL_INTINFO, VNCLOG("created memory bitmap\n"));

	// Get the bitmap's format and colour details
	int result;
	memset(&m_bminfo, 0, sizeof(m_bminfo));
	m_bminfo.bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	m_bminfo.bmi.bmiHeader.biBitCount = 0;
	result = ::GetDIBits(m_hmemdc, m_membitmap, 0, 1, NULL, &m_bminfo.bmi, DIB_RGB_COLORS);
	if (result == 0) {
		vnclog.Print(LL_INTERR, VNCLOG("unable to get display format\n"));
		return FALSE;
	}
	debugf("dibits: %d x %d\n", m_bminfo.bmi.bmiHeader.biWidth, m_bminfo.bmi.bmiHeader.biHeight);

	result = ::GetDIBits(m_hmemdc, m_membitmap,  0, 1, NULL, &m_bminfo.bmi, DIB_RGB_COLORS);
	if (result == 0) {
		vnclog.Print(LL_INTERR, VNCLOG("unable to get display colour info\n"));
		return FALSE;
	}
	vnclog.Print(LL_INTINFO, VNCLOG("got bitmap format\n"));

	// Henceforth we want to use a top-down scanning representation
	m_bminfo.bmi.bmiHeader.biHeight = - abs(m_bminfo.bmi.bmiHeader.biHeight);

	// Is the bitmap palette-based or truecolour?
	m_bminfo.truecolour = (GetDeviceCaps(m_hmemdc, RASTERCAPS) & RC_PALETTE) == 0;
	InvalidateRect(NULL,NULL,TRUE);
	
	return TRUE;
}

BOOL
vncDesktop::ThunkBitmapInfo()
{
	// If we leave the pixel format intact, the blist can be optimised (Will Dean's patch)
	m_formatmunged = FALSE;

	// HACK ***.  Optimised blits don't work with palette-based displays, yet
	if (!m_bminfo.truecolour) {
		m_formatmunged = TRUE;
	}

	// Attempt to force the actual format into one we can handle
	// We can handle 8-bit-palette and 16/32-bit-truecolour modes
	switch (m_bminfo.bmi.bmiHeader.biBitCount)
	{
	case 1:
	case 4:
		vnclog.Print(LL_INTINFO, VNCLOG("DBG:used/bits/planes/comp/size = %d/%d/%d/%d/%d\n"),
			(int)m_bminfo.bmi.bmiHeader.biClrUsed,
			(int)m_bminfo.bmi.bmiHeader.biBitCount,
			(int)m_bminfo.bmi.bmiHeader.biPlanes,
			(int)m_bminfo.bmi.bmiHeader.biCompression,
			(int)m_bminfo.bmi.bmiHeader.biSizeImage);
		
		// Correct the BITMAPINFO header to the format we actually want
		m_bminfo.bmi.bmiHeader.biClrUsed = 0;
		m_bminfo.bmi.bmiHeader.biPlanes = 1;
		m_bminfo.bmi.bmiHeader.biCompression = BI_RGB;
		m_bminfo.bmi.bmiHeader.biBitCount = 8;
		m_bminfo.bmi.bmiHeader.biSizeImage =
			abs((m_bminfo.bmi.bmiHeader.biWidth *
				m_bminfo.bmi.bmiHeader.biHeight *
				m_bminfo.bmi.bmiHeader.biBitCount)/ 8);
		m_bminfo.bmi.bmiHeader.biClrImportant = 0;
		m_bminfo.truecolour = FALSE;

		// Display format is non-VNC compatible - use the slow blit method
		m_formatmunged = TRUE;
		break;	
	case 24:
		// Update the bitmapinfo header
		m_bminfo.bmi.bmiHeader.biBitCount = 32;
		m_bminfo.bmi.bmiHeader.biPlanes = 1;
		m_bminfo.bmi.bmiHeader.biCompression = BI_RGB;
		m_bminfo.bmi.bmiHeader.biSizeImage =
			abs((m_bminfo.bmi.bmiHeader.biWidth *
				m_bminfo.bmi.bmiHeader.biHeight *
				m_bminfo.bmi.bmiHeader.biBitCount)/ 8);
		// Display format is non-VNC compatible - use the slow blit method
		m_formatmunged = TRUE;
		break;
/*	case 32: IIC Experiment
		m_bminfo.bmi.bmiHeader.biBitCount = 16;
		m_bminfo.bmi.bmiHeader.biPlanes = 1;
		m_bminfo.bmi.bmiHeader.biCompression = BI_RGB;
		m_bminfo.bmi.bmiHeader.biSizeImage =
			abs((m_bminfo.bmi.bmiHeader.biWidth *
				m_bminfo.bmi.bmiHeader.biHeight *
				m_bminfo.bmi.bmiHeader.biBitCount)/ 8);
		// Display format is non-VNC compatible - use the slow blit method
		m_formatmunged = TRUE;
		break;
*/	}

	return TRUE;
}

BOOL
vncDesktop::SetPixFormat()
{
 // If we are using a memory bitmap then check how many planes it uses
  // The VNC code can only handle formats with a single plane (CHUNKY pixels)
  if (!m_DIBbits) {
	  vnclog.Print(LL_INTINFO, VNCLOG("DBG:display context has %d planes!\n"),
      GetDeviceCaps(m_hrootdc, PLANES));
	  vnclog.Print(LL_INTINFO, VNCLOG("DBG:memory context has %d planes!\n"),
      GetDeviceCaps(m_hmemdc, PLANES));
	  if (GetDeviceCaps(m_hmemdc, PLANES) != 1)
	  {
		  MessageBox(
			  NULL,
			  "current display is PLANAR, not CHUNKY!\n"
			  "IICServer cannot be used with this graphics device driver",
			  szAppName,
			  MB_ICONSTOP | MB_OK
			  );
		  return FALSE;
	  }
  }
	
	
	// Examine the bitmapinfo structure to obtain the current pixel format
	m_scrinfo.format.trueColour = m_bminfo.truecolour;
	m_scrinfo.format.bigEndian = 0;

	// Set up the native buffer width, height and format
	m_scrinfo.framebufferWidth = (CARD16) (m_bmrect.br.x - m_bmrect.tl.x);		// Swap endian before actually sending
	m_scrinfo.framebufferHeight = (CARD16) (m_bmrect.br.y - m_bmrect.tl.y);	// Swap endian before actually sending
	m_scrinfo.format.bitsPerPixel = (CARD8) m_bminfo.bmi.bmiHeader.biBitCount;
	m_scrinfo.format.depth        = (CARD8) m_bminfo.bmi.bmiHeader.biBitCount;

	// Calculate the number of bytes per row
	m_bytesPerRow = m_scrinfo.framebufferWidth * m_scrinfo.format.bitsPerPixel / 8;

	return TRUE;
}

BOOL
vncDesktop::SetPixShifts()
{
	// Sort out the colour shifts, etc.
	DWORD redMask=0, blueMask=0, greenMask = 0;

	switch (m_bminfo.bmi.bmiHeader.biBitCount)
	{
	case 16:
		// Standard 16-bit display
		if (m_bminfo.bmi.bmiHeader.biCompression == BI_RGB)
		{
			// each word single pixel 5-5-5
			redMask = 0x7c00; greenMask = 0x03e0; blueMask = 0x001f;
		}
		else
		{
			if (m_bminfo.bmi.bmiHeader.biCompression == BI_BITFIELDS)
			{
				redMask =   *(DWORD *) &m_bminfo.bmi.bmiColors[0];
				greenMask = *(DWORD *) &m_bminfo.bmi.bmiColors[1];
				blueMask =  *(DWORD *) &m_bminfo.bmi.bmiColors[2];
			}
		}
		break;

	case 32:
		// Standard 24/32 bit displays
		if (m_bminfo.bmi.bmiHeader.biCompression == BI_RGB)
		{
			redMask = 0xff0000; greenMask = 0xff00; blueMask = 0x00ff;
		}
		else
		{
			if (m_bminfo.bmi.bmiHeader.biCompression == BI_BITFIELDS)
			{
				redMask =   *(DWORD *) &m_bminfo.bmi.bmiColors[0];
				greenMask = *(DWORD *) &m_bminfo.bmi.bmiColors[1];
				blueMask =  *(DWORD *) &m_bminfo.bmi.bmiColors[2];
			}
		}
		break;

	default:
		// Other pixel formats are only valid if they're palette-based
		if (m_bminfo.truecolour)
		{
			vnclog.Print(LL_INTERR, "unsupported truecolour pixel format for setpixshifts\n");
			return FALSE;
		}
		return TRUE;
	}

	// Convert the data we just retrieved
	MaskToMaxAndShift(redMask, m_scrinfo.format.redMax, m_scrinfo.format.redShift);
	MaskToMaxAndShift(greenMask, m_scrinfo.format.greenMax, m_scrinfo.format.greenShift);
	MaskToMaxAndShift(blueMask, m_scrinfo.format.blueMax, m_scrinfo.format.blueShift);

	return TRUE;
}

BOOL
vncDesktop::SetPalette()
{
	// Lock the current display palette into the memory DC we're holding
	// *** CHECK THIS FOR LEAKS!
	if (!m_bminfo.truecolour)
	{
		if (!m_DIBbits)
		{
			// - Handle the palette for a non DIB-Section
			
			LOGPALETTE *palette;
			UINT size = sizeof(LOGPALETTE) + (sizeof(PALETTEENTRY) * 256);
			
			palette = (LOGPALETTE *) new char[size];
			if (palette == NULL) {
				vnclog.Print(LL_INTERR, VNCLOG("unable to allocate logical palette\n"));
				return FALSE;
			}
			
			// Initialise the structure
			palette->palVersion = 0x300;
			palette->palNumEntries = 256;
			
			// Get the system colours
			if (GetSystemPaletteEntries(m_hrootdc,
				0, 256, palette->palPalEntry) == 0)
			{
				vnclog.Print(LL_INTERR, VNCLOG("unable to get system palette entries\n"));
				delete [] palette;
				return FALSE;
			}
			
			// Create a palette from those
			HPALETTE pal = CreatePalette(palette);
			if (pal == NULL)
			{
				vnclog.Print(LL_INTERR, VNCLOG("unable to create HPALETTE\n"));
				delete [] palette;
				return FALSE;
			}
			
			// Select the palette into our memory DC
			HPALETTE oldpalette = SelectPalette(m_hmemdc, pal, FALSE);
			if (oldpalette == NULL)
			{
				vnclog.Print(LL_INTERR, VNCLOG("unable to select() HPALETTE\n"));
				delete [] palette;
				DeleteObject(pal);
				return FALSE;
			}
			
			// Worked, so realise the palette
			if (RealizePalette(m_hmemdc) == GDI_ERROR)
				vnclog.Print(LL_INTWARN, VNCLOG("warning - failed to RealizePalette\n"));
			
			// It worked!
			delete [] palette;
			DeleteObject(oldpalette);
			
			vnclog.Print(LL_INTINFO, VNCLOG("initialised palette OK\n"));
			return TRUE;
		}
		else
		{
			// - Handle a DIB-Section's palette
			
			// - Fetch the system palette for the framebuffer
			PALETTEENTRY syspalette[256];
			UINT entries = ::GetSystemPaletteEntries(m_hrootdc, 0, 256, syspalette);
			vnclog.Print(LL_INTERR, VNCLOG("framebuffer has %u palette entries"), entries);
			
			// - Store it and convert it to RGBQUAD format
			RGBQUAD dibpalette[256];
			unsigned int i;
			for (i=0;i<entries;i++) {
				dibpalette[i].rgbRed = syspalette[i].peRed;
				dibpalette[i].rgbGreen = syspalette[i].peGreen;
				dibpalette[i].rgbBlue = syspalette[i].peBlue;
				dibpalette[i].rgbReserved = 0;
			}
			
			// - Set the rest of the palette to something nasty but usable
			for (i=entries;i<256;i++) {
				dibpalette[i].rgbRed = i % 2 ? 255 : 0;
				dibpalette[i].rgbGreen = i/2 % 2 ? 255 : 0;
				dibpalette[i].rgbBlue = i/4 % 2 ? 255 : 0;
				dibpalette[i].rgbReserved = 0;
			}
			
			// - Update the DIB section to use the same palette
			HDC bitmapDC=::CreateCompatibleDC(m_hrootdc);
			if (!bitmapDC) {
				vnclog.Print(LL_INTERR, VNCLOG("unable to create temporary DC"), GetLastError());
				return FALSE;
			}
			HBITMAP old_bitmap = (HBITMAP)::SelectObject(bitmapDC, m_membitmap);
			if (!old_bitmap) {
				vnclog.Print(LL_INTERR, VNCLOG("unable to select DIB section into temporary DC"), GetLastError());
				return FALSE;
			}
			UINT entries_set = ::SetDIBColorTable(bitmapDC, 0, 256, dibpalette);
			if (entries_set == 0) {
				vnclog.Print(LL_INTERR, VNCLOG("unable to set DIB section palette"), GetLastError());
				return FALSE;
			}
			if (!::SelectObject(bitmapDC, old_bitmap)) {
				vnclog.Print(LL_INTERR, VNCLOG("unable to restore temporary DC bitmap"), GetLastError());
				return FALSE;
			}
		}
    }
 
	// Not a palette based local screen - forget it!
	vnclog.Print(LL_INTERR, VNCLOG("no palette data for truecolour display\n"));
	return TRUE;
}

LRESULT CALLBACK DesktopWndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

ATOM m_wndClass = 0;

BOOL
vncDesktop::InitWindow()
{
	if (m_wndClass == 0) {
		// Create the window class
		WNDCLASSEX wndclass;

		wndclass.cbSize			= sizeof(wndclass);
		wndclass.style			= 0;
		wndclass.lpfnWndProc	= &DesktopWndProc;
		wndclass.cbClsExtra		= 0;
		wndclass.cbWndExtra		= 0;
		wndclass.hInstance		= hAppInstance;
		wndclass.hIcon			= NULL;
		wndclass.hCursor		= NULL;
		wndclass.hbrBackground	= (HBRUSH) GetStockObject(WHITE_BRUSH);
		wndclass.lpszMenuName	= (const char *) NULL;
		wndclass.lpszClassName	= szDesktopSink;
		wndclass.hIconSm		= NULL;

		// Register it
		m_wndClass = RegisterClassEx(&wndclass);
		if (!m_wndClass) {
			vnclog.Print(LL_INTERR, VNCLOG("failed to register window class\n"));
			return FALSE;
		}
	}

	// And create a window
	m_hwnd = CreateWindow(szDesktopSink,
				"IICServer",
				WS_OVERLAPPEDWINDOW,
				CW_USEDEFAULT,
				CW_USEDEFAULT,
				400, 200,
				NULL,
				NULL,
				hAppInstance,
				NULL);

	if (m_hwnd == NULL) {
		vnclog.Print(LL_INTERR, VNCLOG("failed to create hook window\n"));
		return FALSE;
	}

	// Set the "this" pointer for the window
	SetWindowLong(m_hwnd, GWL_USERDATA, (long)this);

	// Enable clipboard hooking
	m_hnextviewer = SetClipboardViewer(m_hwnd);

	return TRUE;
}

void
vncDesktop::EnableOptimisedBlits()
{
	vnclog.Print(LL_INTINFO, VNCLOG("attempting to enable DIBsection blits\n"));

	// Create a new DIB section
	HBITMAP tempbitmap = CreateDIBSection(m_hmemdc, &m_bminfo.bmi, DIB_RGB_COLORS, &m_DIBbits, NULL, 0);
	if (tempbitmap == NULL) {
		vnclog.Print(LL_INTINFO, VNCLOG("failed to build DIB section - reverting to slow blits\n"));
		m_DIBbits = NULL;
		return;
	}

	// Delete the old memory bitmap
	if (m_membitmap != NULL) {
		DeleteObject(m_membitmap);
		m_membitmap = NULL;
	}

	// Replace old membitmap with DIB section
	m_membitmap = tempbitmap;

	vnclog.Print(LL_INTINFO, VNCLOG("enabled fast DIBsection blits OK\n"));
}

BOOL
vncDesktop::Init(vncServer *server)
{
	vnclog.Print(LL_INTINFO, VNCLOG("initialising desktop handler\n"));

	// Save the server pointer
	m_server = server;

	// Load in the arrow cursor
	m_hdefcursor = LoadCursor(NULL, IDC_ARROW);
	m_hcursor = m_hdefcursor;
	m_hOldcursor = m_hdefcursor; //sf@2002

	// Spawn a thread to handle that window's message queue
	vncDesktopThread *thread = new vncDesktopThread;
	if (thread == NULL) {
		vnclog.Print(LL_INTERR, VNCLOG("failed to start hook thread\n"));
		return FALSE;
	}
	m_thread = thread;
	//SINGEL WINDOW
	SWinit();
	// InitHookSettings();
	return thread->Init(this, m_server);
}

int
vncDesktop::ScreenBuffSize()
{
	return m_scrinfo.format.bitsPerPixel/8 *
		m_scrinfo.framebufferWidth *
		m_scrinfo.framebufferHeight;
}

void
vncDesktop::FillDisplayInfo(rfbServerInitMsg *scrinfo)
{
	memcpy(scrinfo, &m_scrinfo, sz_rfbServerInitMsg);
}

// IIC - GetPixel was not returning data from desktop on Win7, but bit blitting to 
// memory bitmap seems to work fine.
COLORREF
vncDesktop::CapturePixel(int x, int y)
{
	COLORREF pix = -1;

	if (UsingDirectX())
	{
		// DirectX capture...
		pix = ::CapturePixel(x, y, m_bytesPerRow);
	}
	else 
	{
		// Select the memory bitmap into the memory DC
		HBITMAP oldbitmap;
		if ((oldbitmap = (HBITMAP) SelectObject(m_hmemdc, m_membitmap)) == NULL)
		{
			vnclog.Print(LL_INTERR, VNCLOG("failed to select bitmap (%d)\n"), GetLastError());
			return -1;
		}

		int nFlags = SRCCOPY;
		if (m_server->IsTransparentEnabled()==FALSE)// ||m_server->SingleWindow())
		{
			nFlags |= CAPTUREBLT;
		}

		// Capture screen into bitmap
		BOOL blitok = TRUE;
		blitok = BitBlt(m_hmemdc, 0, 0, 1, 1, m_hrootdc, x, y, nFlags);

		// And grab pixel.
		if (blitok)
			pix = GetPixel(m_hmemdc, 0, 0);
		else {
			errorf("Pixel bitblt failed (%d)\n", GetLastError());
		}

		// Select the old bitmap back into the memory DC
		SelectObject(m_hmemdc, oldbitmap);
	}

	return pix;
}

// Function to capture an area of the screen immediately prior to sending
// an update.
void
vncDesktop::CaptureScreen(const rfb::Rect &rect, BYTE *scrBuff, UINT scrBuffSize)
{
	// IIC - work around the issue when window switch causes hidden window leaks
	HWND foreground = GetForegroundWindow();
	if (m_server->AppSharing() && m_foreground_window2!=NULL && foreground != m_foreground_window2) {
		m_foreground_window2=foreground;
		return;
	}

	if (m_server->IsSharingPPT() && m_server->SingleWindow()==FALSE)
		return;	// not ready for capturing screen, window handle is not set yet

	if (m_server->SingleWindow())
	{
		if (foreground!=m_PPT_Parent_hWnd && 
			foreground!=m_Single_hWnd && 
			IsSameProcess(foreground, FALSE)==FALSE)	// could be floating bar
			return;

		RECT rc;
		GetWindowRect(m_Single_hWnd, &rc);
		HWND hAbove = GetAboveWindow(rc);
		if (hAbove!=m_Single_hWnd)	// there is window above it
			return;
	}

	HBITMAP oldbitmap;

	if (!UsingDirectX())
	{
		// Select the memory bitmap into the memory DC
		if ((oldbitmap = (HBITMAP) SelectObject(m_hmemdc, m_membitmap)) == NULL)
			return;
	}

	// Capture screen into bitmap
	BOOL blitok = TRUE;

	// Starts IIC - handle app sharing
	int nFlags = SRCCOPY;
	if (m_server->IsTransparentEnabled()==FALSE)// ||m_server->SingleWindow())
	{
		nFlags |= CAPTUREBLT;
	}

	if (m_server->AppSharing())
	{
		blitok = CaptureSharedAppsWindows(rect, scrBuff, scrBuffSize);
	}
	else if (m_server->IsSharingPPT())
	// Ends IIC - handle app sharing
	{
		blitok = CapturePPTWindow(rect, scrBuff, scrBuffSize);
	}
	else
	{
		rfb::Rect clipped = rect.intersect(m_bmrect);
		int destx = clipped.tl.x - m_bmrect.tl.x;
		int desty = clipped.tl.y - m_bmrect.tl.y;

		if (UsingDirectX())
		{
			RECT rect = { clipped.tl.x, clipped.tl.y, clipped.br.x, clipped.br.y };
			HRESULT stat = CaptureRectangle(rect, scrBuff, scrBuffSize, m_bytesPerRow); 
		}
		else 
		{
			debugf("Blitting %d,%d - %d,%d to %d,%d\n", clipped.tl.x, clipped.tl.y, clipped.br.x, clipped.br.y, destx, desty);
			blitok = BitBlt(m_hmemdc, destx, desty,
				(clipped.br.x-clipped.tl.x),
				(clipped.br.y-clipped.tl.y),
				m_hrootdc, clipped.tl.x, clipped.tl.y, nFlags);
			if (blitok == 0)
			{	
				errorf("Blit failed (%d)\n", GetLastError()); 
			}
		}
	}

	if (blitok && !UsingDirectX()) 
	{
		// Select the old bitmap back into the memory DC
		SelectObject(m_hmemdc, oldbitmap);

		// Copy the new data to the screen buffer (CopyToBuffer optimises this if possible)
		// IIC - support secondary monitor
		rfb::Rect out = rect.translate(m_bmrect.tl);
		CopyToBuffer(out, scrBuff, scrBuffSize);
	}
}

// Add the mouse pointer to the buffer
void
vncDesktop::CaptureMouse(BYTE *scrBuff, UINT scrBuffSize)
{
	POINT CursorPos;
	ICONINFO IconInfo;

	// If the mouse cursor handle is invalid then forget it
	if (m_hcursor == NULL)
		return;

	// IIC
	if (m_server->IsSharingPPT() && m_server->SingleWindow()==FALSE)
		return;	// not ready for capturing screen, window handle is not set yet

	HWND foreground = GetForegroundWindow();
	if (m_server->SingleWindow())
	{
		if (foreground!=m_PPT_Parent_hWnd && 
			foreground!=m_Single_hWnd && 
			IsSameProcess(foreground, FALSE)==FALSE)	// could be floating bar
			return;

		RECT rc;
		GetWindowRect(m_Single_hWnd, &rc);
		HWND hAbove = GetAboveWindow(rc);
		if (hAbove!=m_Single_hWnd)	// there is window above it
			return;
	}

	// Get the cursor position
	if (!GetCursorPos(&CursorPos))
		return;

	// Translate position for hotspot
	if (GetIconInfo(m_hcursor, &IconInfo))
	{
		CursorPos.x -= ((int) IconInfo.xHotspot);
		CursorPos.y -= ((int) IconInfo.yHotspot);
		if (IconInfo.hbmMask != NULL)
			DeleteObject(IconInfo.hbmMask);
		if (IconInfo.hbmColor != NULL)
			DeleteObject(IconInfo.hbmColor);
	}

	if (m_server->SingleWindow() || m_server->AppSharing()) {	// IIC
		if (CursorPos.x<m_Cliprect.tl.x || CursorPos.y<m_Cliprect.tl.y ||
			(CursorPos.x + GetSystemMetrics(SM_CXCURSOR))>=m_Cliprect.br.x ||
			(CursorPos.y + GetSystemMetrics(SM_CYCURSOR))>=m_Cliprect.br.y)
			return;
	}

	debugf("Cursor position: %d,%d\n", CursorPos.x, CursorPos.y);

	// Select the memory bitmap into the memory DC
	HBITMAP oldbitmap;
	if ((oldbitmap = (HBITMAP) SelectObject(m_hmemdc, m_membitmap)) == NULL)
		return;

	// Draw the cursor
	DrawIconEx(
		m_hmemdc,									// handle to device context 
		CursorPos.x - m_bmrect.tl.x, 
		CursorPos.y - m_bmrect.tl.y,
		m_hcursor,									// handle to icon to draw 
		0,0,										// width of the icon 
		0,											// index of frame in animated cursor 
		NULL,										// handle to background brush 
		DI_NORMAL | DI_COMPAT						// icon-drawing flags 
		);

	// Select the old bitmap back into the memory DC
	SelectObject(m_hmemdc, oldbitmap);

	// Save the bounding rectangle
	m_cursorpos.tl = CursorPos;
	m_cursorpos.br = rfb::Point(GetSystemMetrics(SM_CXCURSOR),
		GetSystemMetrics(SM_CYCURSOR)).translate(CursorPos);

	// Clip the bounding rect to the screen
	// Copy the mouse cursor into the screen buffer, if any of it is visible
	m_cursorpos = m_cursorpos.intersect(m_bmrect);
	if (!m_cursorpos.is_empty()) {
		// IIC -- detect if cursor is hidden
		CURSORINFO info = { sizeof(CURSORINFO) };
		if (GetCursorInfo(&info))
		{
			rfb::Rect bmPos = m_cursorpos.translate(GetBitmapOffset());
			if ((info.flags & CURSOR_SHOWING) == 0)
			{
				debugf("Cursor not showing\n");
				CaptureScreen(bmPos, scrBuff, scrBuffSize);
			}
			else
			{
				debugf("Copying cursor bitmap from %d,%d\n", bmPos.tl.x, bmPos.tl.y);
				CopyToBuffer(bmPos, scrBuff, scrBuffSize);
			}
		}
	}
}

void
vncDesktop::CaptureMousePos()
{
	POINT CursorPos;
	ICONINFO IconInfo;

	// If the mouse cursor handle is invalid then forget it
	if (m_hcursor == NULL)
		return;

	// Get the cursor position
	if (!GetCursorPos(&CursorPos))
		return;

	// Translate position for hotspot
	if (GetIconInfo(m_hcursor, &IconInfo))
	{
		CursorPos.x -= ((int) IconInfo.xHotspot);
		CursorPos.y -= ((int) IconInfo.yHotspot);
		if (IconInfo.hbmMask != NULL)
			DeleteObject(IconInfo.hbmMask);
		if (IconInfo.hbmColor != NULL)
			DeleteObject(IconInfo.hbmColor);
	}

	// Save the bounding rectangle
	m_cursorpos.tl = CursorPos;
	m_cursorpos.br = rfb::Point(GetSystemMetrics(SM_CXCURSOR),
		GetSystemMetrics(SM_CYCURSOR)).translate(CursorPos);

	m_cursorpos = m_cursorpos.intersect(m_bmrect);
}

// CURSOR HANDLING
// Obtain cursor image data in server's local format.
// The length of databuf[] should be at least (width * height * 4).
BOOL
vncDesktop::GetRichCursorData(BYTE *databuf, HCURSOR hcursor, int width, int height)
{
	// Protect the memory bitmap (is it really necessary here?)
	omni_mutex_lock l(m_update_lock);

	// Create bitmap, select it into memory DC
	HBITMAP membitmap = CreateCompatibleBitmap(m_hrootdc, width, height);
	if (membitmap == NULL) {
		return FALSE;
	}
	HBITMAP oldbitmap = (HBITMAP) SelectObject(m_hmemdc, membitmap);
	if (oldbitmap == NULL) {
		DeleteObject(membitmap);
		return FALSE;
	}

	// Draw the cursor
	DrawIconEx(m_hmemdc, 0, 0, hcursor, 0, 0, 0, NULL, DI_IMAGE);
	SelectObject(m_hmemdc, oldbitmap);

	// Prepare BITMAPINFO structure (copy most m_bminfo fields)
	BITMAPINFO *bmi = (BITMAPINFO *)calloc(1, sizeof(BITMAPINFO) + 256 * sizeof(RGBQUAD));
	memcpy(bmi, &m_bminfo.bmi, sizeof(BITMAPINFO) + 256 * sizeof(RGBQUAD));
	bmi->bmiHeader.biWidth = width;
	bmi->bmiHeader.biHeight = -height;

	// Clear data buffer and extract RGB data
	memset(databuf, 0x00, width * height * 4);
	int lines = GetDIBits(m_hmemdc, membitmap, 0, height, databuf, bmi, DIB_RGB_COLORS);

	// Cleanup
	free(bmi);
	DeleteObject(membitmap);

	return (lines != 0);
}

// Return the current mouse pointer position
rfb::Rect
vncDesktop::MouseRect()
{
	return m_cursorpos;
}

void
vncDesktop::SetCursor(HCURSOR cursor)
{
	if (cursor == NULL)
		m_hcursor = m_hdefcursor;
	else
	{
		m_hOldcursor = m_hcursor; // sf@2002
		m_hcursor = cursor;
	}
}

// Manipulation of the clipboard
void vncDesktop::SetClipText(char* rfbStr)
{
  int len = strlen(rfbStr);
  char* winStr = new char[len*2+1];

  int j = 0;
  for (int i = 0; i < len; i++)
  {
    if (rfbStr[i] == 10)
      winStr[j++] = 13;
    winStr[j++] = rfbStr[i];
  }
  winStr[j++] = 0;

  // Open the system clipboard
  if (OpenClipboard(m_hwnd))
  {
    // Empty it
    if (EmptyClipboard())
    {
      HANDLE hMem = GlobalAlloc(GMEM_MOVEABLE | GMEM_DDESHARE, j);

      if (hMem != NULL)
      {
        LPSTR pMem = (char*)GlobalLock(hMem);

        // Get the data
        strcpy(pMem, winStr);

        // Tell the clipboard
        GlobalUnlock(hMem);
        SetClipboardData(CF_TEXT, hMem);
      }
    }
  }

  delete [] winStr;

  // Now close it
  CloseClipboard();
}

// INTERNAL METHODS

inline void
vncDesktop::MaskToMaxAndShift(DWORD mask, CARD16 &max, CARD8 &shift)
{
	for (shift = 0; (mask & 1) == 0; shift++)
		mask >>= 1;
	max = (CARD16) mask;
}

// Copy data from the memory bitmap into a buffer
void
vncDesktop::CopyToBuffer(const rfb::Rect &aRect, BYTE *destbuff, UINT destbuffsize)
{
	rfb::Rect rect = aRect;
	// Finish drawing anything in this thread 
	// Wish we could do this for the whole system - maybe we should
	// do something with LockWindowUpdate here.
	GdiFlush();

	// Are we being asked to blit from the DIBsection to itself?
	if (destbuff == m_DIBbits) {
		// Yes.  Ignore the request!
		return;
	}

	int y_inv;
	BYTE * destbuffpos;

	// Calculate the scanline-ordered y position to copy from
	y_inv = m_scrinfo.framebufferHeight-rect.tl.y-(rect.br.y-rect.tl.y);

	// Calculate where in the output buffer to put the data
	destbuffpos = destbuff + (m_bytesPerRow * rect.tl.y);

	// Set the number of bytes for GetDIBits to actually write
	// NOTE : GetDIBits pads the destination buffer if biSizeImage < no. of bytes required
	m_bminfo.bmi.bmiHeader.biSizeImage = (rect.br.y-rect.tl.y) * m_bytesPerRow;

	// Get the actual bits from the bitmap into the bit buffer
	// If fast (DIBsection) blits are disabled then use the old GetDIBits technique
	if (m_DIBbits == NULL) {
		if (GetDIBits(m_hmemdc, m_membitmap, y_inv,
					(rect.br.y-rect.tl.y), destbuffpos,
					&m_bminfo.bmi, DIB_RGB_COLORS) == 0)
		{
#ifdef _MSC_VER
			_RPT1(_CRT_WARN, "vncDesktop : [1] GetDIBits failed! %d\n", GetLastError());
			_RPT3(_CRT_WARN, "vncDesktop : thread = %d, DC = %d, bitmap = %d\n", omni_thread::self(), m_hmemdc, m_membitmap);
			_RPT2(_CRT_WARN, "vncDesktop : y = %d, height = %d\n", y_inv, (rect.br.y-rect.tl.y));
#endif
		}
	} else {
		// Fast blits are enabled.  [I have a sneaking suspicion this will never get used, unless
		// something weird goes wrong in the code.  It's here to keep the function general, though!]

		int bytesPerPixel = m_scrinfo.format.bitsPerPixel / 8;
		BYTE *srcbuffpos = (BYTE*)m_DIBbits;

		srcbuffpos += (m_bytesPerRow * rect.tl.y) + (bytesPerPixel * rect.tl.x);
		destbuffpos += bytesPerPixel * rect.tl.x;

		int widthBytes = (rect.br.x-rect.tl.x) * bytesPerPixel;

		for(int y = rect.tl.y; y < rect.br.y; y++)
		{
			memcpy(destbuffpos, srcbuffpos, widthBytes);
			srcbuffpos += m_bytesPerRow;
			destbuffpos += m_bytesPerRow;
		}
	}
}

// Routine to find out which windows have moved
// If copyrect detection isn't perfect then this call returns
// the copyrect destination region, to allow the caller to check
// for mistakes
bool
vncDesktop::CalcCopyRects(rfb::UpdateTracker &tracker)
{
	HWND foreground = GetForegroundWindow();
	RECT foreground_rect;

	// Actually, we just compare the new and old foreground window & its position
	if (foreground != m_foreground_window) {
		m_foreground_window=foreground;
		// Is the window invisible or can we not get its rect?
		if (!IsWindowVisible(foreground) ||
			!GetWindowRect(foreground, &foreground_rect)) {
			m_foreground_window_rect.clear();
		} else {
			m_foreground_window_rect = foreground_rect;
		}
	} else {
		// Same window is in the foreground - let's see if it's moved
		RECT destrect;
		rfb::Rect dest;
		rfb::Point source;

		// Get the window rectangle
		if (IsWindowVisible(foreground) && GetWindowRect(foreground, &destrect))
		{
			rfb::Rect old_foreground_window_rect = m_foreground_window_rect;
			source = m_foreground_window_rect.tl;
			m_foreground_window_rect = dest = destrect;
			if (!dest.is_empty() && !old_foreground_window_rect.is_empty() &&	// IIC -- ignore transparent window
				(IsWindowTransparent(foreground)==FALSE||m_server->IsTransparentEnabled()==FALSE)
				)
			{
				// Got the destination position.  Now send to clients!
				if (!source.equals(dest.tl))
				{
					rfb::Point delta;
					delta= rfb::Point(dest.tl.x-source.x, dest.tl.y-source.y);

					// Clip the destination rectangle
					dest = dest.intersect(m_bmrect);
					if (dest.is_empty()) return false;
					m_buffer.ClearCacheRect(dest);
					// Clip the source rectangle
					dest = dest.translate(delta.negate()).intersect(m_bmrect);
					dest = dest.translate(delta);
					if (!dest.is_empty()) {
						// Tell the buffer about the copyrect
						//m_buffer.CopyRect(dest, delta);

						// Notify all clients of the copyrect
						tracker.add_copied(dest, delta);
						return true;
						//m_buffer.ClearCache();
					}
				}
			}
		} else {
			m_foreground_window_rect.clear();
		}
	}
	return false;
}

// Window procedure for the Desktop window
LRESULT CALLBACK
DesktopWndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	vncDesktop *_this = (vncDesktop*)GetWindowLong(hwnd, GWL_USERDATA);

	switch (iMsg)
	{
	///ddihook
	case WM_COPYDATA:
			_this->pMyCDS= (PCOPYDATASTRUCT) lParam;
			if (_this->pMyCDS->dwData==112233)
			{
					DWORD mysize=_this->pMyCDS->cbData;
					char mytext[1024];
					char *myptr;
					char split[4][6];
					strcpy(mytext,(LPCSTR)_this->pMyCDS->lpData);
					myptr=mytext;
					for (int j =0; j<(mysize/20);j++)
					{
						for (int i=0;i<4;i++)
							{
								strcpy(split[i],"     ");
								strncpy(split[i],myptr,4);
								myptr=myptr+5;
							}
						_this->QueueRect(rfb::Rect(atoi(split[0]), atoi(split[1]), atoi(split[2]), atoi(split[3])));
					}
					
			}
			//vnclog.Print(LL_INTINFO, VNCLOG("copydata\n"));	
			return 0;

	// GENERAL

	case WM_DISPLAYCHANGE:
		// The display resolution is changing
		// We must kick off any clients since their screen size will be wrong
		// WE change the clients screensize, if they support it.
		if (_this->m_videodriver != NULL)
		{
			if (!_this->m_videodriver->blocked)
			{
				_this->m_driverchanged = TRUE;
				vnclog.Print(LL_INTERR, VNCLOG("Reinitialized driver lock \n"));
			}
			else
			{
				_this->m_videodriver->blocked=false;
				vnclog.Print(LL_INTERR, VNCLOG("Reinitialized driver unlock\n"));
			}
		}
		else 
		{
			_this->m_displaychanged = TRUE;
			vnclog.Print(LL_INTERR, VNCLOG("Reinitialized no driver\n"));
			// _this->m_videodriver->blocked=false; // _this->m_videodriver = NULL ...
		}
		return 0;

	case WM_SYSCOLORCHANGE:
	case WM_PALETTECHANGED:
		// The palette colours have changed, so tell the server

		// Get the system palette
		if (!_this->SetPalette())
			PostQuitMessage(0);

		// Update any palette-based clients, too
		_this->m_server->UpdatePalette();
		return 0;

		// CLIPBOARD MESSAGES

	case WM_CHANGECBCHAIN:
		// The clipboard chain has changed - check our nextviewer handle
		if ((HWND)wParam == _this->m_hnextviewer)
			_this->m_hnextviewer = (HWND)lParam;
		else
			if (_this->m_hnextviewer != NULL)
				SendMessage(_this->m_hnextviewer,
							WM_CHANGECBCHAIN,
							wParam, lParam);

		return 0;
/*	// IIC	-- disable clipboard
	case WM_DRAWCLIPBOARD:
		// The clipboard contents have changed
		if((GetClipboardOwner() != _this->Window()) &&
		    _this->m_initialClipBoardSeen &&
			_this->m_clipboard_active)
		{
			LPSTR cliptext = NULL;

			// Open the clipboard
			if (OpenClipboard(_this->Window()))
			{
				// Get the clipboard data
				HGLOBAL cliphandle = GetClipboardData(CF_TEXT);
				if (cliphandle != NULL)
				{
					LPSTR clipdata = (LPSTR) GlobalLock(cliphandle);

					// Copy it into a new buffer
					if (clipdata == NULL)
						cliptext = NULL;
					else
						cliptext = strdup(clipdata);

					// Release the buffer and close the clipboard
					GlobalUnlock(cliphandle);
				}

				CloseClipboard();
			}

			if (cliptext != NULL)
			{
				int cliplen = strlen(cliptext);
				LPSTR unixtext = (char *)malloc(cliplen+1);

				// Replace CR-LF with LF - never send CR-LF on the wire,
				// since Unix won't like it
				int unixpos=0;
				for (int x=0; x<cliplen; x++)
				{
					if (cliptext[x] != '\x0d')
					{
						unixtext[unixpos] = cliptext[x];
						unixpos++;
					}
				}
				unixtext[unixpos] = 0;

				// Free the clip text
				free(cliptext);
				cliptext = NULL;

				// Now send the unix text to the server
				_this->m_server->UpdateClipText(unixtext);

				free(unixtext);
			}
		}

		_this->m_initialClipBoardSeen = TRUE;

		if (_this->m_hnextviewer != NULL)
		{
			// Pass the message to the next window in clipboard viewer chain.  
			return SendMessage(_this->m_hnextviewer, WM_DRAWCLIPBOARD, 0,0); 
		}

		return 0;
*/
	default:
		return DefWindowProc(hwnd, iMsg, wParam, lParam);
	}
	return 0;
}


//
//
//
void vncDesktop::ActivatePseudoDesktop()
{
	STARTUPINFO ssi;
	PROCESS_INFORMATION ppi;
    ZeroMemory( &ssi, sizeof(ssi) );
    ssi.cb = sizeof(ssi);
    // Start the child process. 
    if( !CreateProcess( NULL, // No module name (use command line). 
        m_server->GetSuCommand(), // Command line. 
        NULL,             // Process handle not inheritable. 
        NULL,             // Thread handle not inheritable. 
        FALSE,            // Set handle inheritance to FALSE. 
        NULL,                // No creation flags. 
        NULL,             // Use parent's environment block. 
        NULL,             // Use parent's starting directory. 
        &ssi,              // Pointer to STARTUPINFO structure.
        &ppi )             // Pointer to PROCESS_INFORMATION structure.
    ) return;

	WaitForInputIdle(ppi.hProcess, 10000);
	hAppliProcess=ppi.hProcess;
}


// Modif rdv@2002 Dis/enable input
void
vncDesktop::SetDisableInput(bool enabled)
{
	typedef BOOL (WINAPI*  pBlockInput) (BOOL);
	pBlockInput pbi;
	pbi = (pBlockInput)GetProcAddress( LoadLibrary("USER32"), "BlockInput");
	if (pbi) (*pbi)(enabled);

	// Also Turn Off the Monitor if allowed ("Blank Screen", "Blank Monitor")
	if (m_server->BlankMonitorEnabled())
	if (enabled)
	{
		SetProcessShutdownParameters(0x100, 0);
		SystemParametersInfo(SPI_GETPOWEROFFTIMEOUT, 0, &OldPowerOffTimeout, 0);
		SystemParametersInfo(SPI_SETPOWEROFFTIMEOUT, 100, NULL, 0);
		SystemParametersInfo(SPI_SETPOWEROFFACTIVE, 1, NULL, 0);
		SendMessage(GetDesktopWindow(),WM_SYSCOMMAND,SC_MONITORPOWER,(LPARAM)2);
	}
	else // Monitor On
	{
		if (OldPowerOffTimeout!=0)
			SystemParametersInfo(SPI_SETPOWEROFFTIMEOUT, OldPowerOffTimeout, NULL, 0);
		SystemParametersInfo(SPI_SETPOWEROFFACTIVE, 0, NULL, 0);
		SendMessage(GetDesktopWindow(),WM_SYSCOMMAND,SC_MONITORPOWER,(LPARAM)-1);
		OldPowerOffTimeout=0;
	}
}


// SW
void vncDesktop::SetSW(int x,int y)
{
	POINT point;
	point.x=x;
	point.y=y;
	if (x == 1 && y==1) 
		{
			m_server->SingleWindow(false);
			m_displaychanged=true;
			m_SWtoDesktop=TRUE;
	}
	else
	{
	m_Single_hWnd=WindowFromPoint(point);
	if (m_Single_hWnd==NULL) m_server->SingleWindow(false);
	else
		{
		HWND parent;
		while((parent=GetParent(m_Single_hWnd))!=NULL)
			{
				m_Single_hWnd=parent;
			}
		m_server->SingleWindow(true);
		}
	}
}


//
void vncDesktop::SetUpdateTimer(BOOL updown)
{
	int nPeriod = 0;
	if (updown)
		nPeriod = 100;
	else
		nPeriod = 300;

	if (nPeriod != m_timer)
	{
		m_timer = nPeriod;
		if (m_timerid != NULL)
			KillTimer(NULL, m_timerid);
		m_timerid = SetTimer(m_hwnd, 1, m_timer, NULL);
	}

}

/***************************************************************************
* Ultravnc use 2 different software hooking methods 
* hookdll: Hook the messages between the windows (nt,9.x)
* ddihook: Hook the messages to the video buffer (9.x)
* Driver hooking
* Driver hooking is done at kernel level.
* and exist of 2 parts
* update mechanism: rectangles are send from kernel to vnc (shared mem)
* shared video buffer: direct access the memeory without bitblit
*  m_hookdriver: use driver update mechanism (ddihook in case of 9.x)
*  m_hookdll: use software update mechanism
***************************************************************************/

// Modif rdv@2002 - v1.1.x - videodriver

BYTE * vncDesktop::VideoBuffer()
{
	//Always access the shared mememory thru this function
	// Check NULL
	if (m_videodriver==NULL) return NULL;
	if (IsBadReadPtr(m_videodriver,1)) return NULL;
	// If we reach this place, the driver was active
	if (!IsBadReadPtr(m_videodriver->bufdata.Userbuffer,10))
					return (BYTE *)m_videodriver->bufdata.Userbuffer;
	// Driver was active, but shared memeory is no longer available
	// Call a initHooksetting to reinitialize hooking in next vncdesktopthread
	// access
	else
	{
		// InitHookSettings(); // No, otherwise driver/hookdll switching doesn't work well
		SethookMechanism(true, false); // Force HookDll On and Driver Off
		return NULL;
	}
}

// Modif rdv@2002 - v1.1.x - videodriver
BOOL vncDesktop::InitVideoDriver()
{
	omni_mutex_lock l(m_videodriver_lock);
	if(OSVersion()!=1 ) return true; //we need w2k or xp
	vnclog.Print(LL_INTERR, VNCLOG("Driver option is enabled\n"));
	// If m_videodriver exist, the driver was activated.
	// This does not mean he is still active
	// Screen switching disable the driver at kernel level
	// The pointers to the shared memory still exist, but no more memeory
	// associated...This is the biggest risk when using the driver
	//
	if (m_videodriver!=NULL)
	{
		// we can access the shared memeory, no need to reinstall
		if (!IsBadReadPtr(m_videodriver->bufdata.Userbuffer,10)) return true;
	}
	
	m_videodriver=new vncVideoDriver;
	//try to use the virtual/mirror driver if he is still active
	if (m_videodriver->IsVirtualDriverActive())
	{
		vnclog.Print(LL_INTERR, VNCLOG("Use active Virtual driver\n"));
		m_videodriver->UnMapSharedbuffers2();//if still mapped unmap
		m_videodriver->MapSharedbuffers2();
	}
	else if (m_videodriver->IsMirrorDriverActive())
	{
		// This should normal not happen
		vnclog.Print(LL_INTERR, VNCLOG("Use active Mirror driver\n"));
		m_videodriver->UnMapSharedbuffers();
		m_videodriver->MapSharedbuffers();
	}
	else // no drivers where active, so start the mirror driver
	{
		vnclog.Print(LL_INTERR, VNCLOG("Start Mirror driver\n"));
		m_videodriver->Activate_video_driver();
		m_videodriver->MapSharedbuffers();
	}
	// check if driver has mapped the shared memory
	if (!m_videodriver->driver) 
	{
		vnclog.Print(LL_INTERR, VNCLOG("Start Mirror driver Failed\n"));
		vnclog.Print(LL_INTERR, VNCLOG("Using non driver mode\n"));
		if (m_videodriver!=NULL) delete m_videodriver;
		m_videodriver=NULL;
		// If driver selected and fialed to start default to hookdll
		// 
		m_hookdriver=false;
		m_hookdll=true;
		// sf@2002 - Necessary for the following InitHookSettings() call
		m_server->Driver(false);
		m_server->Hook(true);
		return false;
	}
	
	if (m_videodriver->driver)
	{
		vnclog.Print(LL_INTERR, VNCLOG("Driver Used\n"));
		if (!m_videodriver->bufdata.Userbuffer)
		{
			vnclog.Print(LL_INTERR, VNCLOG("Unable to map memory\n"));
			delete m_videodriver;
			m_videodriver=NULL;
			// If driver selected and fialed to start default to hookdll
			// 
			m_hookdriver=false;
			m_hookdll=true;
			return false;
		}
		vnclog.Print(LL_INTERR, VNCLOG("Shared memory mapped\n"));
		return true;
	}
	return true;
}


// Modif rdv@2002 - v1.1.x - videodriver
void vncDesktop::ShutdownVideoDriver(BOOL changesize)
{
	// delete m_videodriver
	// auto desactive the video driver
	omni_mutex_lock l(m_videodriver_lock);
	if(OSVersion()!=1) return;
	if (m_videodriver==NULL) return;
	if (m_videodriver!=NULL) delete m_videodriver;
	m_videodriver=NULL;
}


//
// This proc sets the Desktop update handling params depending
// on tranitted settings and internal constraints.
// It is called from several places (InitHookSettings(), SetHookings()
// and from Desktop thread loop).
// 
void vncDesktop::SethookMechanism(BOOL hookall,BOOL hookdriver)
{
	m_hookswitch=false;
	// Force at least one updates handling method !
	if (!hookall && !hookdriver && !m_server->PollFullScreen())
	{
		hookall = TRUE;
		m_server->PollFullScreen(TRUE);
	}

	// 9,x case 
	vnclog.Print(LL_INTERR, VNCLOG("SethookMechanism called\r\n"));
	if(OSVersion()==4 )
	{
		m_hookdriver=false;//(user driver updates)
		m_hookdll=false;//(use hookdll updates)
		if (hookall || hookdriver) m_hookdll=true;
		//always try to stop the ddihook before starting
		StartStopddihook(false);ddihook=false;
		if (hookdriver) StartStopddihook(true);
		// same for the hookdll
		BOOL old_On_Off_hookdll=On_Off_hookdll;
		if (hookdriver || hookall) On_Off_hookdll=true;
		else On_Off_hookdll=false;
		if (old_On_Off_hookdll!=On_Off_hookdll) Hookdll_Changed=true;
		else Hookdll_Changed=false;
	}
	// W2k-XP case
	else if(OSVersion()==1)
	{
		// sf@2002 - We forbid hoodll and hookdriver at the same time (pointless and high CPU load)
		if (!hookall && !hookdriver) {m_hookdll=false;m_hookdriver=false;}
		if (hookall && hookdriver) {m_hookdll=false;m_hookdriver=true;}
		if (hookall && !hookdriver) {m_hookdll=true;m_hookdriver=false;}
		if (!hookall && hookdriver) {m_hookdll=false;m_hookdriver=true;}

		BOOL old_On_Off_hookdll=On_Off_hookdll;
		if (m_hookdll) On_Off_hookdll=true;
		else On_Off_hookdll=false;
		if (old_On_Off_hookdll!=On_Off_hookdll) Hookdll_Changed=true;
		else Hookdll_Changed=false;
		
		vnclog.Print(LL_INTERR, VNCLOG("Sethook_restart_wanted hook=%d driver=%d \r\n"),m_hookdll,m_hookdriver);
		if (Hookdll_Changed)
			vnclog.Print(LL_INTERR, VNCLOG("Hookdll status changed \r\n"));

		if ((m_hookdriver && !VideoBuffer()) || (!m_hookdriver && VideoBuffer()))
		{
			m_hookswitch=true;
			vnclog.Print(LL_INTERR, VNCLOG("Driver Status changed\r\n"));
		}
	}
	else //NT4
	{
		if (!hookall && !hookdriver) {m_hookdll=false;m_hookdriver=false;}
		if (hookall && hookdriver) {m_hookdll=true;m_hookdriver=false;}
		if (hookall && !hookdriver) {m_hookdll=true;m_hookdriver=false;}
		if (!hookall && hookdriver) {m_hookdll=false;m_hookdriver=false;}
		BOOL old_On_Off_hookdll=On_Off_hookdll;
		if (m_hookdll) On_Off_hookdll=true;
		else On_Off_hookdll=false;
		if (old_On_Off_hookdll!=On_Off_hookdll) Hookdll_Changed=true;
		else Hookdll_Changed=false;
	}

}
void vncDesktop::StartStopddihook(BOOL enabled)
{
	if (enabled)
	{
		STARTUPINFO ssi;
		PROCESS_INFORMATION ppi;
		m_hddihook=NULL;
		char szCurrentDir[MAX_PATH];
		if (GetModuleFileName(NULL, szCurrentDir, MAX_PATH))
		{
			char* p = strrchr(szCurrentDir, '\\');
			if (p == NULL) return;
			*p = '\0';
			strcat (szCurrentDir,"\\16bithlp.exe");
		}
		// Add ddi hook
		ZeroMemory( &ssi, sizeof(ssi) );
		ssi.cb = sizeof(ssi);
		// Start the child process. 
		if( !CreateProcess( NULL,szCurrentDir, NULL,NULL,FALSE,NULL,NULL,NULL,&ssi,&ppi ) ) 
		{
			vnclog.Print(LL_INTERR, VNCLOG("set ddihooks Failed\n"));
			ddihook=false;
		}
		else
		{
			vnclog.Print(LL_INTERR, VNCLOG("set ddihooks OK\n"));
			ddihook=true;
			WaitForInputIdle(ppi.hProcess, 10000);
			m_hddihook=ppi.hProcess;
		}
	}
	else
	{
		if (m_hddihook != NULL)
		{
			TerminateProcess(m_hddihook,0);
			m_hddihook=NULL;
			ddihook=false;
		}
	}

}

void vncDesktop::StartStophookdll(BOOL enabled)
{
	if (enabled)
	{
		if (!SetHooks(
			GetCurrentThreadId(),
			RFB_SCREEN_UPDATE,
			RFB_COPYRECT_UPDATE,
			RFB_MOUSE_UPDATE, ddihook, RFB_WINMSG_UPDATE, RFB_MOUSE_CLICK_MSG
			))
		{
			vnclog.Print(LL_INTERR, VNCLOG("failed to set system hooks\n"));
			// Switch on full screen polling, so they can see something, at least...
			m_server->PollFullScreen(TRUE);
			m_hookinited = FALSE;
		} 
		else 
		{
			vnclog.Print(LL_INTERR, VNCLOG("set hooks OK\n"));
			m_hookinited = TRUE;
		}
		// Start up the keyboard and mouse filters
		SetKeyboardFilterHook(m_server->LocalInputsDisabled());
		SetMouseFilterHook(m_server->LocalInputsDisabled());
	}
	else if (m_hookinited)
	{
		if(!UnSetHooks(GetCurrentThreadId()) )
			vnclog.Print(LL_INTERR, VNCLOG("Unsethooks Failed\n"));
		else vnclog.Print(LL_INTERR, VNCLOG("Unsethooks OK\n"));
		
	}
}


//
//
//
void vncDesktop::InitHookSettings()
{
	SethookMechanism(m_server->Hook(),m_server->Driver());
}
