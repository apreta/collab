/////////////////////////////////////////////////////////////////////////////
//  Copyright (C) 2002 Ultr@VNC Team Members. All Rights Reserved.
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// If the source code for the program is not available from the place from
// which you received this file, check 
// http://ultravnc.sourceforge.net/

// vncPropPath.cpp


#include "stdhdrs.h"
#include "lmcons.h"
#include "vncService.h"

#include "WinVNC.h"
#include "vncPropPath.h"
#include "vncServer.h"
#include "vncPasswd.h"
#include "vncMasterPasswd.h"

const char WINVNC_REGISTRY_KEY [] = "Software\\ORL\\WinVNC3";
const char NO_OVERRIDE_ERR [] = "This machine has been preconfigured with IIC settings, "
								"which cannot be overridden by individual users.  "
								"The preconfigured settings may be modified only by a System Administrator.";
const char NO_CURRENT_USER_ERR [] = "The IIC settings for the current user are unavailable at present.";
const char CANNOT_EDIT_DEFAULT_PREFS [] = "You do not have sufficient priviliges to edit the default local IIC settings.";

// Constructor & Destructor
vncPropPath::vncPropPath()
{
	m_dlgvisible = FALSE;
	m_usersettings = TRUE;
}

vncPropPath::~vncPropPath()
{
}

// Initialization
BOOL
vncPropPath::Init(vncServer *server)
{
	// Save the server pointer
	m_server = server;
	
	// Load the settings from the registry
	Load(TRUE);

	return TRUE;
}

// Dialog box handling functions
void
vncPropPath::Show(BOOL show, BOOL usersettings)
{
	if (show)
	{
		// Verify that we know who is logged on
		if (usersettings) {
			char username[UNLEN+1];
			if (!vncService::CurrentUser(username, sizeof(username)))
				return;
			if (strcmp(username, "") == 0) {
				MessageBox(NULL, NO_CURRENT_USER_ERR, "IIC Error", MB_OK | MB_ICONEXCLAMATION);
				return;
			}
		} else {
			// We're trying to edit the default local settings - verify that we can
			HKEY hkLocal, hkDefault;
			BOOL canEditDefaultPrefs = 1;
			DWORD dw;
			if (RegCreateKeyEx(HKEY_LOCAL_MACHINE,
				WINVNC_REGISTRY_KEY,
				0, REG_NONE, REG_OPTION_NON_VOLATILE,
				KEY_READ, NULL, &hkLocal, &dw) != ERROR_SUCCESS)
				canEditDefaultPrefs = 0;
			else if (RegCreateKeyEx(hkLocal,
				"Default",
				0, REG_NONE, REG_OPTION_NON_VOLATILE,
				KEY_WRITE | KEY_READ, NULL, &hkDefault, &dw) != ERROR_SUCCESS)
				canEditDefaultPrefs = 0;
			if (hkLocal) RegCloseKey(hkLocal);
			if (hkDefault) RegCloseKey(hkDefault);

			if (!canEditDefaultPrefs) {
				MessageBox(NULL, CANNOT_EDIT_DEFAULT_PREFS, "IIC Error", MB_OK | MB_ICONEXCLAMATION);
				return;
			}
		}

		// Now, if the dialog is not already displayed, show it!
		if (!m_dlgvisible)
		{
			if (usersettings)
				vnclog.Print(LL_INTINFO, VNCLOG("show per-user Shared app\n"));
			else
				vnclog.Print(LL_INTINFO, VNCLOG("show default system Shared app\n"));

			// Load in the settings relevant to the user or system
			Load(usersettings);

				m_returncode_valid = FALSE;

				// Do the dialog box
				int result = DialogBoxParam(hAppInstance,
				    MAKEINTRESOURCE(IDD_PATHS), 
				    NULL,
				    (DLGPROC) DialogProc,
				    (LONG) this);

				if (!m_returncode_valid)
				    result = IDCANCEL;

				vnclog.Print(LL_INTINFO, VNCLOG("dialog result = %d\n"), result);

				if (result == -1)
				{
					// Dialog box failed, so quit
					PostQuitMessage(0);
					return;
				}
				
			// Load in all the settings
			Load(TRUE);
		}
	}
}


BOOL CALLBACK
vncPropPath::DialogProc(HWND hwnd,
						  UINT uMsg,
						  WPARAM wParam,
						  LPARAM lParam )
{
	// We use the dialog-box's USERDATA to store a _this pointer
	// This is set only once WM_INITDIALOG has been recieved, though!
	vncPropPath *_this = (vncPropPath *) GetWindowLong(hwnd, GWL_USERDATA);

	switch (uMsg)
	{

	case WM_INITDIALOG:
		{
			// Retrieve the Dialog box parameter and use it as a pointer
			// to the calling vncProperties object
			SetWindowLong(hwnd, GWL_USERDATA, lParam);
			_this = (vncPropPath *) lParam;

		   HWND hNamePath1 = GetDlgItem(hwnd, IDC_Path1);
           if ( _this->m_server->GetNamePath1() != NULL) {
            SetDlgItemText(hwnd, IDC_Path1,_this->m_server->GetNamePath1());
           }
		   EnableWindow(hNamePath1,true);

		   HWND hNamePath2 = GetDlgItem(hwnd, IDC_Path2);
           if ( _this->m_server->GetNamePath2() != NULL) {
            SetDlgItemText(hwnd, IDC_Path2,_this->m_server->GetNamePath2());
           }
		   EnableWindow(hNamePath2,true);

		   HWND hNamePath3 = GetDlgItem(hwnd, IDC_Path3);
           if ( _this->m_server->GetNamePath3() != NULL) {
            SetDlgItemText(hwnd, IDC_Path3,_this->m_server->GetNamePath3());
           }
		   EnableWindow(hNamePath3,true);

		   

			char Path1[MAXPATH];
			strcpy(Path1,_this->m_server->GetNamePath1());
			{
				SetDlgItemText(hwnd, IDC_Path1, Path1);
			}
			char Path2[MAXPATH];
			strcpy(Path2,_this->m_server->GetNamePath2());
			{
				SetDlgItemText(hwnd, IDC_Path2, Path2);
			}
			char Path3[MAXPATH];
			strcpy(Path3,_this->m_server->GetNamePath3());
			{
				SetDlgItemText(hwnd, IDC_Path3, Path3);
			}

			SetForegroundWindow(hwnd);
			_this->m_dlgvisible = TRUE;

			return TRUE;
		}

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{

		case IDOK:
		case IDC_APPLY:
			{
				char Path1[MAXPATH+1];
				if (GetDlgItemText(hwnd, IDC_Path1, (LPSTR) &Path1, MAXPATH+1) == 0) {
				vnclog.Print(LL_INTINFO,VNCLOG("error reading Path1 Pathication %d \n"), GetLastError());
				} else {
				_this->m_server->SetNamePath1(Path1);
				}
				char Path2[MAXPATH+1];
				if (GetDlgItemText(hwnd, IDC_Path2, (LPSTR) &Path2, MAXPATH+1) == 0) {
				vnclog.Print(LL_INTINFO,VNCLOG("error reading Path2 Pathication %d \n"), GetLastError());
				} else {
				_this->m_server->SetNamePath2(Path2);
				}
				char Path3[MAXPATH+1];
				if (GetDlgItemText(hwnd, IDC_Path3, (LPSTR) &Path3, MAXPATH+1) == 0) {
				vnclog.Print(LL_INTINFO,VNCLOG("error reading Path3 Pathication %d \n"), GetLastError());
				} else {
				_this->m_server->SetNamePath3(Path3);
				}
				
				HWND hNamePath1 = GetDlgItem(hwnd, IDC_Path1);
				HWND hNamePath2 = GetDlgItem(hwnd, IDC_Path2);
				HWND hNamePath3 = GetDlgItem(hwnd, IDC_Path3);

				EnableWindow(hNamePath1,true);		   
				EnableWindow(hNamePath2,true);
				EnableWindow(hNamePath3,true);		   
			

				// And to the registry
				_this->Save();

				// Was ok pressed?
				if (LOWORD(wParam) == IDOK)
				{
					// Yes, so close the dialog
					vnclog.Print(LL_INTINFO, VNCLOG("enddialog (OK)\n"));

					_this->m_returncode_valid = TRUE;

					EndDialog(hwnd, IDOK);
					_this->m_dlgvisible = FALSE;
				}

				return TRUE;
			}

		case IDCANCEL:
			vnclog.Print(LL_INTINFO, VNCLOG("enddialog (CANCEL)\n"));

			_this->m_returncode_valid = TRUE;

			EndDialog(hwnd, IDCANCEL);
			_this->m_dlgvisible = FALSE;
			return TRUE;


		case IDC_SETMASTER:
				vncMasterPwd *MasterPwd = new vncMasterPwd();
				if (MasterPwd->Init(_this->m_server))
				{
					MasterPwd->Show(TRUE, _this->m_usersettings,FALSE);
				}
				SetForegroundWindow(hwnd);
				omni_thread::sleep(0, 200000000);
				delete MasterPwd;//CHECKCHECK
			
			return TRUE;

		}

		break;
	}
	return 0;
}

// Functions to load & save the settings
LONG
vncPropPath::LoadInt(HKEY key, LPCSTR valname, LONG defval)
{
	LONG pref;
	ULONG type = REG_DWORD;
	ULONG prefsize = sizeof(pref);

	if (RegQueryValueEx(key,
		valname,
		NULL,
		&type,
		(LPBYTE) &pref,
		&prefsize) != ERROR_SUCCESS)
		return defval;

	if (type != REG_DWORD)
		return defval;

	if (prefsize != sizeof(pref))
		return defval;

	return pref;
}

void
vncPropPath::LoadPath1(HKEY key, char *buffer)
{
	DWORD type = REG_BINARY;
	int slen=MAXPATH;
	char inouttext[MAXPATH];

	// Retrieve the encrypted password
	if (RegQueryValueEx(key,
		"Path1",
		NULL,
		&type,
		(LPBYTE) &inouttext,
		(LPDWORD) &slen) != ERROR_SUCCESS)
		return;

	if (slen > MAXPATH)
		return;

	memcpy(buffer, inouttext, MAXPATH);
}
void
vncPropPath::LoadPath2(HKEY key, char *buffer)
{
	DWORD type = REG_BINARY;
	int slen=MAXPATH;
	char inouttext[MAXPATH];

	// Retrieve the encrypted password
	if (RegQueryValueEx(key,
		"Path2",
		NULL,
		&type,
		(LPBYTE) &inouttext,
		(LPDWORD) &slen) != ERROR_SUCCESS)
		return;

	if (slen > MAXPATH)
		return;

	memcpy(buffer, inouttext, MAXPATH);
}
void
vncPropPath::LoadPath3(HKEY key, char *buffer)
{
	DWORD type = REG_BINARY;
	int slen=MAXPATH;
	char inouttext[MAXPATH];

	// Retrieve the encrypted password
	if (RegQueryValueEx(key,
		"Path3",
		NULL,
		&type,
		(LPBYTE) &inouttext,
		(LPDWORD) &slen) != ERROR_SUCCESS)
		return;

	if (slen > MAXPATH)
		return;

	memcpy(buffer, inouttext, MAXPATH);
}

char *
vncPropPath::LoadString(HKEY key, LPCSTR keyname)
{
	DWORD type = REG_SZ;
	DWORD buflen = 0;
	BYTE *buffer = 0;

	// Get the length of the string
	if (RegQueryValueEx(key,
		keyname,
		NULL,
		&type,
		NULL,
		&buflen) != ERROR_SUCCESS)
		return 0;

	if (type != REG_SZ)
		return 0;
	buffer = new BYTE[buflen];
	if (buffer == 0)
		return 0;

	// Get the string data
	if (RegQueryValueEx(key,
		keyname,
		NULL,
		&type,
		buffer,
		&buflen) != ERROR_SUCCESS) {
		delete [] buffer;
		return 0;
	}

	// Verify the type
	if (type != REG_SZ) {
		delete [] buffer;
		return 0;
	}

	return (char *)buffer;
}

void
vncPropPath::Load(BOOL usersettings)
{
	char username[UNLEN+1];
	HKEY hkLocal, hkLocalUser, hkDefault;
	DWORD dw;

	// Get the user name / service name
	if (!vncService::CurrentUser((char *)&username, sizeof(username)))
		return;

	// If there is no user logged on them default to SYSTEM
	if (strcmp(username, "") == 0)
		strcpy((char *)&username, "SYSTEM");

	// Try to get the machine registry key for WinVNC
	if (RegCreateKeyEx(HKEY_LOCAL_MACHINE,
		WINVNC_REGISTRY_KEY,
		0, REG_NONE, REG_OPTION_NON_VOLATILE,
		KEY_READ, NULL, &hkLocal, &dw) != ERROR_SUCCESS)
		return;

	// Now try to get the per-user local key
	if (RegOpenKeyEx(hkLocal,
		username,
		0, KEY_READ,
		&hkLocalUser) != ERROR_SUCCESS)
		hkLocalUser = NULL;

	// Get the default key
	if (RegCreateKeyEx(hkLocal,
		"Default",
		0, REG_NONE, REG_OPTION_NON_VOLATILE,
		KEY_READ,
		NULL,
		&hkDefault,
		&dw) != ERROR_SUCCESS)
		hkDefault = NULL;

	strcpy(m_pref_Path1,"");
	strcpy(m_pref_Path2,"");
	strcpy(m_pref_Path3,"");

	// Load the local prefs for this user
	if (hkDefault != NULL)
	{
		vnclog.Print(LL_INTINFO, VNCLOG("loading DEFAULT local settings\n"));
		LoadUserPrefs(hkDefault);
	}
	if (hkLocalUser != NULL) RegCloseKey(hkLocalUser);
	if (hkDefault != NULL) RegCloseKey(hkDefault);
	RegCloseKey(hkLocal);

	// Make the loaded settings active..
	ApplyUserPrefs();

	// Note whether we loaded the user settings or just the default system settings
	m_usersettings = usersettings;
}

void
vncPropPath::LoadUserPrefs(HKEY appkey)
{	
	LoadPath1(appkey, m_pref_Path1);
	m_server->SetNamePath1(m_pref_Path1);
	LoadPath2(appkey, m_pref_Path2);
	m_server->SetNamePath2(m_pref_Path2);
	LoadPath3(appkey, m_pref_Path3);
	m_server->SetNamePath3(m_pref_Path3);	
}


void
vncPropPath::ApplyUserPrefs()
{

	m_server->SetNamePath1(m_pref_Path1);	
	m_server->SetNamePath2(m_pref_Path2);	
	m_server->SetNamePath3(m_pref_Path3);
}

void
vncPropPath::SavePath1(HKEY key, char *buffer)
{
	RegSetValueEx(key, "Path1", 0, REG_BINARY, (LPBYTE) buffer, MAXPATH);
}
void
vncPropPath::SavePath2(HKEY key, char *buffer)
{
	RegSetValueEx(key, "Path2", 0, REG_BINARY, (LPBYTE) buffer, MAXPATH);
}
void
vncPropPath::SavePath3(HKEY key, char *buffer)
{
	RegSetValueEx(key, "Path3", 0, REG_BINARY, (LPBYTE) buffer, MAXPATH);
}

void
vncPropPath::Save()
{
	HKEY appkey;
	DWORD dw;

		// Try to get the default local registry key for WinVNC
		HKEY hkLocal;
		if (RegCreateKeyEx(HKEY_LOCAL_MACHINE,
			WINVNC_REGISTRY_KEY,
			0, REG_NONE, REG_OPTION_NON_VOLATILE,
			KEY_READ, NULL, &hkLocal, &dw) != ERROR_SUCCESS) {
			MessageBox(NULL, "MB1", "WVNC", MB_OK);
			return;
		}
		if (RegCreateKeyEx(hkLocal,
			"Default",
			0, REG_NONE, REG_OPTION_NON_VOLATILE,
			KEY_WRITE | KEY_READ, NULL, &appkey, &dw) != ERROR_SUCCESS) {
			RegCloseKey(hkLocal);
			return;
		}
		RegCloseKey(hkLocal);
	

	// SAVE PER-USER PREFS IF ALLOWED
	SaveUserPrefs(appkey);
	RegCloseKey(appkey);	
	RegCloseKey(HKEY_CURRENT_USER);


}

void
vncPropPath::SaveUserPrefs(HKEY appkey)
{
	char Path1[MAXPATH];
	char Path2[MAXPATH];
	char Path3[MAXPATH];
	strcpy(Path1,m_server->GetNamePath1());
	strcpy(Path2,m_server->GetNamePath2());
	strcpy(Path3,m_server->GetNamePath3());
	SavePath1(appkey, Path1);
	SavePath2(appkey, Path2);
	SavePath3(appkey, Path3);
	
}

