//  Copyright (C) 2002 RealVNC Ltd. All Rights Reserved.
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// If the source code for the program is not available from the place from
// which you received this file, check http://www.realvnc.com/ or contact
// the authors on info@realvnc.com for information on obtaining it.

// Log.cpp: implementation of the VNCLog class.
//
//////////////////////////////////////////////////////////////////////

#include "stdhdrs.h"
#include <io.h>
#include "VNCLog.h"
#include "../netlib/log.h"

#include <sys/types.h>
#include <sys/stat.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

const int VNCLog::ToDebug   =  1;
const int VNCLog::ToFile    =  2;
const int VNCLog::ToConsole =  4;
const int VNCLog::ToNetLog	=  8;

const static int LINE_BUFFER_SIZE = 1024;


// The number of log files to keep before deleting the oldest
static unsigned history_len = 10;

# define MAX_LOG_FILENAME    (256)

void VNCLog::log_rotate_if_needed( LPSTR logFN, bool fAlwaysRotate )
{
    char newerfn[MAX_LOG_FILENAME];
    char olderfn[MAX_LOG_FILENAME];
    int i;
    long    sz      = -1;
    DWORD   dwHigh;
    DWORD   errNo;

    if( hlogfile )
        sz = GetFileSize( hlogfile, &dwHigh );
    else
    {
        struct _stat myStats;
        if( _stat( logFN, &myStats )  ==  0 )
            sz = myStats.st_size;
    }

    if( (sz < m_iMaxSize)  &&  !fAlwaysRotate )
        return;

    CloseFile( );

    sprintf( newerfn, "%s.%u", logFN, history_len );
    DeleteFile( newerfn );
    for( i = history_len-1; i > 0; i--)
    {
        strcpy( olderfn, newerfn );
        sprintf( newerfn, "%s.%u", logFN, i );
        if( MoveFile(newerfn, olderfn)  ==  0)
        {
            errNo = GetLastError( );
        }
    }
    strcpy(olderfn, newerfn);
    strcpy(newerfn, logFN);
    if (MoveFile(newerfn, olderfn)  ==  0)
    {
        errNo = GetLastError( );
    }
}


VNCLog::VNCLog()
{
	m_lastLogTime = 0;
	m_filename = NULL;
	m_append = false;
    hlogfile = NULL;
    m_todebug = false;
    m_toconsole = false;
    m_tofile = false;
}

void VNCLog::SetMode(int mode)
{
	m_mode = mode;
    if (mode & ToDebug)
        m_todebug = true;
    else
        m_todebug = false;

    if (mode & ToFile)  {
		if (!m_tofile)
			OpenFile();
	} else {
		CloseFile();
        m_tofile = false;
    }

	if (mode & ToNetLog)
		m_tonetlog = true;
	else
		m_tonetlog = false;
    
    if (mode & ToConsole) {
        if (!m_toconsole) {
            AllocConsole();
            fclose(stdout);
            fclose(stderr);
#ifdef _MSC_VER
            int fh = _open_osfhandle((long)GetStdHandle(STD_OUTPUT_HANDLE), 0);
            _dup2(fh, 1);
            _dup2(fh, 2);
            _fdopen(1, "wt");
            _fdopen(2, "wt");
            printf("fh is %d\n",fh);
            fflush(stdout);
#endif
        }

        m_toconsole = true;

    } else {
        m_toconsole = false;
    }
}


void VNCLog::SetLevel(int level) {
    m_level = level;
}

void VNCLog::SetFile(const char* filename, bool append, int iMaxSize )
{
    if( m_filename != NULL )
		free( m_filename );
	m_filename = strdup( filename );
    m_append = append;
    m_iMaxSize = iMaxSize;
	if( m_tofile )
		OpenFile( );
}

void VNCLog::OpenFile( )
{
	// Is there a file-name?
	if (m_filename == NULL)
	{
        m_todebug = true;
        m_tofile = false;
        Print(0, "Error opening log file\n");
		return;
	}

    m_tofile = true;
    
	// If there's an existing log and we're not appending then move it
	if( !m_append )
    {
        log_rotate_if_needed( m_filename, true );
    }
    else
        log_rotate_if_needed( m_filename, false );

    // If filename is NULL or invalid we should throw an exception here
    hlogfile = CreateFile(
        m_filename,  GENERIC_WRITE, FILE_SHARE_READ, NULL,
        OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL  );
    
    if (hlogfile == INVALID_HANDLE_VALUE) {
        // We should throw an exception here
        m_todebug = true;
        m_tofile = false;
        Print(0, "Error opening log file %s\n", m_filename);
    }
    if (m_append) {
        SetFilePointer( hlogfile, 0, NULL, FILE_END );
    } else {
        SetEndOfFile( hlogfile );
    }
}

// if a log file is open, close it now.
void VNCLog::CloseFile() {
    if (hlogfile != NULL) {
        CloseHandle(hlogfile);
        hlogfile = NULL;
    }
}

inline void VNCLog::ReallyPrintLine(const char* line) 
{
    if (m_todebug) OutputDebugString(line);
    if (m_toconsole) {
        DWORD byteswritten;
        WriteConsole(GetStdHandle(STD_OUTPUT_HANDLE), line, strlen(line), &byteswritten, NULL); 
    };
    if (m_tofile && (hlogfile != NULL)) {
        DWORD byteswritten;
        WriteFile(hlogfile, line, strlen(line), &byteswritten, NULL); 
    }
	if (m_tonetlog) {
		net_write_log("%s", line);
	}
}

void VNCLog::ReallyPrint(const char* format, va_list ap) 
{
	time_t current = time(0);
	if (current != m_lastLogTime) {
		m_lastLogTime = current;
		ReallyPrintLine(ctime(&m_lastLogTime));
	}

	// - Write the log message, safely, limiting the output buffer size
	TCHAR line[LINE_BUFFER_SIZE];
    _vsnprintf(line, LINE_BUFFER_SIZE, format, ap);
	ReallyPrintLine(line);
}

VNCLog::~VNCLog()
{
	if (m_filename != NULL)
		free(m_filename);
    CloseFile();
}
