/////////////////////////////////////////////////////////////////////////////
//  Copyright (C) 2002 Ultr@VNC Team Members. All Rights Reserved.
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// If the source code for the program is not available from the place from
// which you received this file, check 
// http://ultravnc.sourceforge.net/

// vncMasterPwd


class vncMasterPwd;

#if (!defined(_WINVNC_vncMasterPwd))
#define _WINVNC_vncMasterPwd

// Includes
#include "stdhdrs.h"
#include "vncServer.h"

// The vncProperties class itself
class vncMasterPwd
{
public:
	// Constructor/destructor
	vncMasterPwd();
	~vncMasterPwd();

	// Initialisation
	BOOL Init(vncServer *server);

	// The dialog box window proc
	static BOOL CALLBACK DialogProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	//BOOL DoDialog();

	// Display the properties dialog
	// If usersettings is TRUE then the per-user settings come up
	// If usersettings is FALSE then the default system settings come up
	void Show(BOOL show, BOOL usersettings,BOOL check);

	// Loading & saving of preferences
	void Load(BOOL usersettings);
	void Save();

	// Implementation
protected:
	// The server object to which this properties object is attached.
	vncServer *			m_server;

	// Flag to indicate whether the currently loaded settings are for
	// the current user, or are default system settings
	BOOL				m_usersettings;

	void LoadMasterPwd(HKEY k, char *buffer);
	void SaveMasterPwd(HKEY k, char *buffer);

	// Loading/saving all the user prefs
	void LoadUserPrefs(HKEY appkey);
	void SaveUserPrefs(HKEY appkey);

	
	BOOL m_returncode_valid;
	BOOL m_dlgvisible;
	BOOL m_check;
	char m_pref_MasterPwd[MAXPWLEN+1];

};

#endif // _WINVNC_vncMasterPwd
