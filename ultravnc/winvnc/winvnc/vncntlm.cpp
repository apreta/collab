#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

typedef BOOL (*CheckUserGroupPasswordFn)( char * userin,char *password,char *machine);
CheckUserGroupPasswordFn CheckUserGroupPassword = 0;
bool CheckAD()
{
	HMODULE hModule = LoadLibrary("Activeds.dll");
	if (hModule)
	{
		FreeLibrary(hModule);
		return true;
	}
	return false;
}

bool CheckNetapi95()
{
	HMODULE hModule = LoadLibrary("netapi32.dll");
	if (hModule)
	{
		FreeLibrary(hModule);
		return true;
	}
	return false;
}

bool CheckNetApiNT()
{
	HMODULE hModule = LoadLibrary("radmin32.dll");
	if (hModule)
	{
		FreeLibrary(hModule);
		return true;
	}
	return false;
}

BOOL CheckUserGroupPasswordUni(char * userin,char *password,const char *machine)
{
	char clientname[256];
	strcpy(clientname,machine);
	if (!CheckNetapi95() && !CheckNetApiNT())
	{
		return false;
	}
	if (CheckAD())
	{
		char szCurrentDir[MAX_PATH];
		if (GetModuleFileName(NULL, szCurrentDir, MAX_PATH))
		{
			char* p = strrchr(szCurrentDir, '\\');
			if (p == NULL) return false;
			*p = '\0';
			strcat (szCurrentDir,"\\authad.dll");
		}
		HMODULE hModule = LoadLibrary(szCurrentDir);
		if (hModule)
			{
				BOOL result=false;
				CheckUserGroupPassword = (CheckUserGroupPasswordFn) GetProcAddress( hModule, "CUGP" );
				HRESULT hr = CoInitialize(NULL);
				result=CheckUserGroupPassword(userin,password,clientname);
				CoUninitialize();
				FreeLibrary(hModule);
				return result;
			}
		else MessageBox(NULL, "You selected ms-logon, but the authad.dll\nwas not found.Check you installation", "WARNING", MB_OK);
	}
	else
	{
		printf("AD not detected");
		char szCurrentDir[MAX_PATH];
		if (GetModuleFileName(NULL, szCurrentDir, MAX_PATH))
		{
			char* p = strrchr(szCurrentDir, '\\');
			if (p == NULL) return false;
			*p = '\0';
			strcat (szCurrentDir,"\\auth.dll");
		}
		HMODULE hModule = LoadLibrary(szCurrentDir);
		if (hModule)
			{
				BOOL result=false;
				CheckUserGroupPassword = (CheckUserGroupPasswordFn) GetProcAddress( hModule, "CUGP" );
				HRESULT hr = CoInitialize(NULL);
				result=CheckUserGroupPassword(userin,password,clientname);
				CoUninitialize();
				FreeLibrary(hModule);
				return result;
			}
		else MessageBox(NULL, "You selected ms-logon, but the auth.dll\nwas not found.Check you installation", "WARNING", MB_OK);

	}
	return false;
}
