/*
 * translate.c - translate between different pixel formats
 */

/*
 *  Copyright (C) 1999 AT&T Laboratories Cambridge. All Rights Reserved.
 *
 *  This is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this software; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 *  USA.
 */

#include "stdhdrs.h"

#include "translate.h"
#include <stdio.h>
#include <stdlib.h>
#include "rfb.h"
#include "../vdacctray/vncOSVersion.h"

#define CONCAT2(a,b) a##b
#define CONCAT2E(a,b) CONCAT2(a,b)
#define CONCAT4(a,b,c,d) a##b##c##d
#define CONCAT4E(a,b,c,d) CONCAT4(a,b,c,d)

// IIC -- image smooth filter
const int swrows = 3;
const int swcols = 3;
const char UNIFORM[swrows][swcols] = {
	{1,1,1},
	{1,1,1},
	{1,1,1}
};

const int KERNEL[3][3] = {
	{1,2,1},
	{2,4,2},
	{1,2,1}
};

#define OUT 8
#include "tableinittctemplate.cpp"
#include "tableinitcmtemplate.cpp"
#define IN 8
#include "tabletranstemplate.cpp"
#undef IN
#define IN 16
#include "tabletranstemplate.cpp"
#undef IN
#define IN 32
#include "tabletranstemplate.cpp"
#undef IN
#undef OUT

#define OUT 16
#include "tableinittctemplate.cpp"
#include "tableinitcmtemplate.cpp"
#define IN 8
#include "tabletranstemplate.cpp"
#undef IN
#define IN 16
#include "tabletranstemplate.cpp"
#undef IN
#define IN 32
#include "tabletranstemplate.cpp"
#undef IN
#undef OUT

#define OUT 32
#include "tableinittctemplate.cpp"
#include "tableinitcmtemplate.cpp"
#define IN 8
#include "tabletranstemplate.cpp"
#undef IN
#define IN 16
#include "tabletranstemplate.cpp"
#undef IN
#define IN 32
#include "tabletranstemplate.cpp"
#undef IN
#undef OUT


rfbInitTableFnType rfbInitTrueColourSingleTableFns[3] = {
    rfbInitTrueColourSingleTable8,
    rfbInitTrueColourSingleTable16,
    rfbInitTrueColourSingleTable32
};

rfbInitTableFnType rfbInitColourMapSingleTableFns[3] = {
    rfbInitColourMapSingleTable8,
    rfbInitColourMapSingleTable16,
    rfbInitColourMapSingleTable32
};

rfbInitTableFnType rfbInitTrueColourRGBTablesFns[3] = {
    rfbInitTrueColourRGBTables8,
    rfbInitTrueColourRGBTables16,
    rfbInitTrueColourRGBTables32
};

rfbTranslateFnType rfbTranslateWithSingleTableFns[3][3] = {
    { rfbTranslateWithSingleTable8to8,
      rfbTranslateWithSingleTable8to16,
      rfbTranslateWithSingleTable8to32 },
    { rfbTranslateWithSingleTable16to8,
      rfbTranslateWithSingleTable16to16,
      rfbTranslateWithSingleTable16to32 },
    { rfbTranslateWithSingleTable32to8,
      rfbTranslateWithSingleTable32to16,
      rfbTranslateWithSingleTable32to32 }
};

rfbTranslateFnType rfbTranslateWithRGBTablesFns[3][3] = {
    { rfbTranslateWithRGBTables8to8,
      rfbTranslateWithRGBTables8to16,
      rfbTranslateWithRGBTables8to32 },
    { rfbTranslateWithRGBTables16to8,
      rfbTranslateWithRGBTables16to16,
      rfbTranslateWithRGBTables16to32 },
    { rfbTranslateWithRGBTables32to8,
      rfbTranslateWithRGBTables32to16,
      rfbTranslateWithRGBTables32to32 }
};



// rfbTranslateNone is used when no translation is required.

void
rfbTranslateNone(char *table, rfbPixelFormat *in, rfbPixelFormat *out,
		 char *iptr, char *optr, int bytesBetweenInputLines,
		 int width, int height, BOOL reduceColor)
{
    int bytesPerOutputLine = width * (out->bitsPerPixel / 8);

    while (height > 0) {
		if(IsBadReadPtr(iptr,bytesPerOutputLine))  return;
		if(IsBadWritePtr(optr,bytesPerOutputLine))  return;
	memcpy(optr, iptr, bytesPerOutputLine);
	iptr += bytesBetweenInputLines;
	optr += bytesPerOutputLine;
	height--;
    }
}

HDC GetDcMirror()
{
if(OSVersion()==1)
{
	typedef BOOL (WINAPI* pEnumDisplayDevices)(PVOID,DWORD,PVOID,DWORD);
	HDC m_hrootdc;
	pEnumDisplayDevices pd;
	LPSTR driverName = "Winvnc video hook driver";
	BOOL DriverFound;
	DEVMODE devmode;
	FillMemory(&devmode, sizeof(DEVMODE), 0);
	devmode.dmSize = sizeof(DEVMODE);
	devmode.dmDriverExtra = 0;
	BOOL change = EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&devmode);
	devmode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
	pd = (pEnumDisplayDevices)GetProcAddress( LoadLibrary("USER32"), "EnumDisplayDevicesA");
	if (pd)
		{
			LPSTR deviceName=NULL;
			DISPLAY_DEVICE dd;
			ZeroMemory(&dd, sizeof(dd));
			dd.cb = sizeof(dd);
			devmode.dmDeviceName[0] = '\0';
			INT devNum = 0;
			BOOL result;
			DriverFound=false;
			while (result = (*pd)(NULL,devNum, &dd,0))
			{
				if (strcmp((const char *)&dd.DeviceString[0], driverName) == 0)
				{
				DriverFound=true;
				break;
				}
				devNum++;
			}
			if (DriverFound)
			{
			deviceName = (LPSTR)&dd.DeviceName[0];
			m_hrootdc = CreateDC("DISPLAY",deviceName,NULL,NULL);				
			}
		}

		return m_hrootdc;
	}
	else return NULL;
}

int fPaletteCreated=FALSE;
int colorF[216];
int r[216];
int g[216];
int b[216];

///IIC
void CreateWebSafePalette() 
{ 
	int cb[6] = {0xFF, 0xCC, 0x99, 0x66, 0x33, 0}; 
	int cg[6] = {0xFF00, 0xCC00, 0x9900, 0x6600, 0x3300, 0}; 
	int cr[6] = {0xFF0000, 0xCC0000, 0x990000, 0x660000, 0x330000, 0}; 
	
	int jj = 0; 
	for (int ir=0;ir<6; ir++) { 
		for (int ig=0;ig<6; ig++) { 
			for (int ib=0;ib<6; ib++) { 
				colorF[jj] = cr[ir]+cg[ig]+cb[ib]; 
				jj = jj+1; 
			} 
		} 
	} 
	
	for (int i=0;i<216; i++) { 
		r[i] = (colorF[i]>>16) & 0xFF;
		g[i] = (colorF[i]>>8) & 0xFF;
		b[i] = colorF[i] & 0xFF;
	}
}
/*
inline int GetNearestColor(int clr)
{
	if (!fPaletteCreated) {
		CreateWebSafePalette();
		fPaletteCreated = TRUE;
	}

	int r0 = (clr>>16) & 0xFF;
	int g0 = (clr>>8) & 0xFF;
	int b0 = clr & 0xFF;

	int num = 0;
	int k;
	for (int i=0;i<216; i++)
	{
		int x = abs(r[i]-r0);
		int f = (x<<1)+x;
		x = abs(g[i]-g0);
		f += (x<<2) + (x<<1);
		f += abs(b[i]-b0);

//		int f = (int)30*(r[i]-r0)*(r[i]-r0);
//		int x = (int)59*(g[i]-g0)*(g[i]-g0);
//		f += x;
//		x = (int)11*(b[i]-b0)*(b[i]-b0);
//		f += x;

		if (i== 0) {
			k = f;
		}
		if (f<=k){
			k = f;
			num = i;
		}
	}
	
	return colorF[num]; 
}
*/

inline int GetNearestColor(int clr)
{
	static int last_clr=0;
	static int last_nearest_clr=0;
	static int cr[6] = {0xFF0000, 0xCC0000, 0x990000, 0x660000, 0x330000, 0}; 
	static int cg[6] = {0xFF00, 0xCC00, 0x9900, 0x6600, 0x3300, 0}; 
	static int cb[6] = {0xFF, 0xCC, 0x99, 0x66, 0x33, 0};
	int i;

	if (clr==last_clr)
		return last_nearest_clr;

	last_clr = clr;


	int r0 = clr & 0xFF0000;
	int g0 = clr & 0xFF00;
	int b0 = clr & 0xFF;
	
	int r1=0xFF0000, g1=0xFF00, b1=0xFF;

	int k = 0xFF0000;
	for (i=0; i<6; i++)
	{
		int d = abs(cr[i]-r0);
		if (d<k){
			k = d;
			r1=cr[i];
		}
	}
	
	k = 0xFF00;
	for (i=0; i<6; i++)
	{
		int d = abs(cg[i]-g0);
		if (d<k){
			k = d;
			g1=cg[i];
		}
	}

	k = 0xFF;
	for (i=0; i<6; i++)
	{
		int d = abs(cb[i]-b0);
		if (d<k){
			k = d;
			b1=cb[i];
		}
	}

	last_nearest_clr = (r1+g1+b1);
	return last_nearest_clr;
}
