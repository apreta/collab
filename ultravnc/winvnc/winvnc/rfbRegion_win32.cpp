//  Copyright (C) 2002 RealVNC Ltd. All Rights Reserved. 
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// If the source code for the program is not available from the place from
// which you received this file, check http://www.realvnc.com/ or contact
// the authors on info@realvnc.com for information on obtaining it.

// -=- rfbRegion_win32.cpp
// Win32 implementation of the rfb::Region2D classs

#include <assert.h>


#include "stdhdrs.h"
#include "rfbRegion_win32.h"
#include <omnithread.h>

using namespace rfb;

static omni_mutex region_lock;

Region2D::Region2D() {
  omni_mutex_lock l(region_lock);
	hRgn = CreateRectRgn(0, 0, 0, 0);
	assert(hRgn);
}

Region2D::Region2D(int x1, int y1, int x2, int y2) {
  omni_mutex_lock l(region_lock);
	hRgn = CreateRectRgn(x1, y1, x2, y2);
  assert(hRgn);
}

Region2D::Region2D(const Rect &r) {
  omni_mutex_lock l(region_lock);
	hRgn = CreateRectRgn(r.tl.x, r.tl.y, r.br.x, r.br.y);
	assert(hRgn);
}
	
Region2D::Region2D(const Region2D &r) {
  omni_mutex_lock l(region_lock);
	hRgn = CreateRectRgn(0, 0, 1, 1);
	assert(hRgn);
  if (CombineRgn(hRgn, r.hRgn, NULL, RGN_COPY) == ERROR) {
		DWORD cause = GetLastError();
		vnclog.Print(LL_INTERR, "Region2D failure %lu\n", cause);
    abort();
	}
}

Region2D::~Region2D() {
 // omni_mutex_lock l(region_lock);
	assert(hRgn);
  if (!DeleteObject(hRgn)) {
		DWORD cause = GetLastError();
		vnclog.Print(LL_INTERR, "~Region2D failure %lu\n", cause);
    abort();
  }
  hRgn = 0;
}

Region2D& Region2D::operator=(const Region2D &src) {
  omni_mutex_lock l(region_lock);
	assert(hRgn); assert(src.hRgn);
  if (CombineRgn(hRgn, src.hRgn, NULL, RGN_COPY) == ERROR) {
		DWORD cause = GetLastError();
		vnclog.Print(LL_INTERR, "= failure %lu\n", cause);
    abort();
	}
	return *this;
}

Region2D Region2D::intersect(const Region2D &src) const {
  omni_mutex_lock l(region_lock);
	Region2D result;
	assert(result.hRgn); assert(hRgn); assert(src.hRgn);
	if (CombineRgn(result.hRgn, src.hRgn, hRgn, RGN_AND) == ERROR) {
		DWORD cause = GetLastError();
		vnclog.Print(LL_INTERR, "intersect failure %lu\n", cause);
    abort();
	}
	return result;
}
	
Region2D Region2D::union_(const Region2D &src) const {
  omni_mutex_lock l(region_lock);
	Region2D result;
	assert(result.hRgn); assert(hRgn); assert(src.hRgn);
	if (CombineRgn(result.hRgn, src.hRgn, hRgn, RGN_OR) == ERROR) {
		DWORD cause = GetLastError();
		vnclog.Print(LL_INTERR, "union_ failure %lu\n", cause);
    abort();
	}
	return result;
}
	
Region2D Region2D::subtract(const Region2D &src) const {
  omni_mutex_lock l(region_lock);
	Region2D result;
	assert(result.hRgn); assert(hRgn); assert(src.hRgn);
	if (CombineRgn(result.hRgn, hRgn, src.hRgn, RGN_DIFF) == ERROR) {
		DWORD cause = GetLastError();
		vnclog.Print(LL_INTERR, "subtract failure %lu\n", cause);
    abort();
	}
	return result;
}

void Region2D::reset(int x1, int y1, int x2, int y2) {
  omni_mutex_lock l(region_lock);
	assert(hRgn);
  if (!SetRectRgn(hRgn, x1, y1, x2, y2)) {
		DWORD cause = GetLastError();
		vnclog.Print(LL_INTERR, "reset failure %lu\n", cause);
    abort();
	}
  assert(!is_empty());
}

void Region2D::translate(const rfb::Point &p) {
  omni_mutex_lock l(region_lock);
	assert(hRgn);
  if (OffsetRgn(hRgn, p.x, p.y) == ERROR) {
		DWORD cause = GetLastError();
		vnclog.Print(LL_INTERR, "translate failure %lu\n", cause);
    abort();
	}
}

bool Region2D::get_rects(RectVector &rects, bool left2right, bool topdown) const
{
	omni_mutex_lock l(region_lock);
	
	DWORD buffsize = GetRegionData(hRgn, 0, NULL);
	if (!buffsize) {
		DWORD cause = GetLastError();
		vnclog.Print(LL_INTERR, "get_rects1 failure %lu\n", cause);
		abort();
	}
	
	// sf@2002 - No need to allocate a buffer if empty...
	if (is_empty())
	{
		return false;
	}
	
	unsigned char* buffer = new unsigned char[buffsize];
	assert(buffer);	
	assert(hRgn);
	
	/*
	if (is_empty())
	{
	delete [] buffer;
    return false;
	}
	*/
	
	if (GetRegionData(hRgn, buffsize, (LPRGNDATA)buffer) != buffsize)
	{
		DWORD cause = GetLastError();
		vnclog.Print(LL_INTERR, "get_rects2 failure %lu\n", cause);
		delete [] buffer; // sf@2002
		abort();
	}
	
	LPRGNDATA region_data = (LPRGNDATA)buffer;
	DWORD nCount = region_data->rdh.nCount;
	if (topdown) {
		long current_y = INT_MIN;
		long start_i=0, end_i=-1;
		rects.reserve(nCount);
		for (long i=0; i<nCount; i++) {
			Rect rect = ((RECT*)&region_data->Buffer[0])[i];
			if (rect.tl.y == current_y) {
				end_i = i;
			} else {
				if (left2right) {
					for (long j=start_i; j<=end_i; j++) {
						Rect r = ((RECT*)&region_data->Buffer[0])[j];
						rects.push_back(r);
					}
				} else {
					for (long j=end_i; j>=start_i; j--) {
						Rect r = ((RECT*)&region_data->Buffer[0])[j];
						rects.push_back(r);
					}
				}
				start_i = i;
				end_i = i;
				current_y = rect.tl.y;
			}
		}
		if (left2right) {
			for (long j=start_i; j<=end_i; j++) {
				Rect r = ((RECT*)&region_data->Buffer[0])[j];
				rects.push_back(r);
			}
		} else {
			for (long j=end_i; j>=start_i; j--) {
				Rect r = ((RECT*)&region_data->Buffer[0])[j];
				rects.push_back(r);
			}
		}
	} else {
		long current_y = INT_MIN;
		long start_i=nCount, end_i=nCount-1;
		rects.reserve(nCount);
		for (long i=nCount-1; i>=0; i--) {
			Rect rect = ((RECT*)&region_data->Buffer[0])[i];
			if (rect.tl.y == current_y) {
				start_i = i;
			} else {
				if (left2right) {
					for (long j=start_i; j<=end_i; j++) {
						Rect r = ((RECT*)&region_data->Buffer[0])[j];
						rects.push_back(r);
					}
				} else {
					for (long j=end_i; j>=start_i; j--) {
						Rect r = ((RECT*)&region_data->Buffer[0])[j];
						rects.push_back(r);
					}
				}
				end_i = i;
				start_i = i;
				current_y = rect.tl.y;
			}
		}
		if (left2right) {
			for (long j=start_i; j<=end_i; j++) {
				Rect r = ((RECT*)&region_data->Buffer[0])[j];
				rects.push_back(r);
			}
		} else {
			for (long j=end_i; j>=start_i; j--) {
				Rect r = ((RECT*)&region_data->Buffer[0])[j];
				rects.push_back(r);
			}
		}
	}
	
	delete [] buffer;
	assert(!rects.empty());
	return true;
}

Rect Region2D::get_bounding_rect() const {
	omni_mutex_lock l(region_lock);
	RECT result;
	assert(hRgn);
	if (!GetRgnBox(hRgn, &result)) {
		DWORD cause = GetLastError();
		vnclog.Print(LL_INTERR, "get_bounding_rect failure %lu\n", cause);
		abort();
	}
	return result;
}

void Region2D::clear() {
  omni_mutex_lock l(region_lock);
	assert(hRgn);
  if (!SetRectRgn(hRgn, 0, 0, 0, 0)) {
    DWORD cause = GetLastError();
		vnclog.Print(LL_INTERR, "clear failure %lu\n", cause);
    abort();
  }
}

bool Region2D::equals(const Region2D &b) const {
  omni_mutex_lock l(region_lock);
	assert(hRgn); assert(b.hRgn);
	BOOL result = EqualRgn(b.hRgn, hRgn);
  if (result == ERROR) {
    DWORD cause = GetLastError();
		vnclog.Print(LL_INTERR, "equals failure %lu\n", cause);
    abort();
  }
  return result;
}

bool Region2D::is_empty() const {
  omni_mutex_lock l(region_lock);
	RECT result;
	assert(hRgn);
  int kind = GetRgnBox(hRgn, &result);
  if (!kind) {
    DWORD cause = GetLastError();
		vnclog.Print(LL_INTERR, "is_empty failure %lu\n", cause);
    abort();
  }
  return kind == NULLREGION;
}
