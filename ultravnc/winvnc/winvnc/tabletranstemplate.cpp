/*
 * tabletranstemplate.c - template for translation using lookup tables.
 *
 * This file shouldn't be compiled.  It is included multiple times by
 * translate.c, each time with different definitions of the macros IN and OUT.
 *
 * For each pair of values IN and OUT, this file defines two functions for
 * translating a given rectangle of pixel data.  One uses a single lookup
 * table, and the other uses three separate lookup tables for the red, green
 * and blue values.
 *
 * I know this code isn't nice to read because of all the macros, but
 * efficiency is important here.
 */

#if !defined(IN) || !defined(OUT)
#error "This file shouldn't be compiled."
#error "It is included as part of translate.c"
#endif

#define IN_T CONCAT2E(CARD,IN)
#define OUT_T CONCAT2E(CARD,OUT)
#define rfbTranslateWithSingleTableINtoOUT \
				CONCAT4E(rfbTranslateWithSingleTable,IN,to,OUT)
#define rfbTranslateWithRGBTablesINtoOUT \
				CONCAT4E(rfbTranslateWithRGBTables,IN,to,OUT)

/*
 * rfbTranslateWithSingleTableINtoOUT translates a rectangle of pixel data
 * using a single lookup table.
 */

static void
rfbTranslateWithSingleTableINtoOUT (char *table, rfbPixelFormat *in,
				    rfbPixelFormat *out,
				    char *iptr, char *optr,
				    int bytesBetweenInputLines,
				    int width, int height, BOOL reduceColor)
{
    IN_T *ip = (IN_T *)iptr;
    OUT_T *op = (OUT_T *)optr;
    int ipextra = bytesBetweenInputLines / sizeof(IN_T) - width;
    OUT_T *opLineEnd;
    OUT_T *t = (OUT_T *)table;
	int cols = bytesBetweenInputLines / sizeof(IN_T);
#if 0
	int sumval;
	int count;
	// smooth the original image
	for (int row=0;row<height;row++) {
		for (int col=0; col<width; col++) {
			sumval = 0;
			count = 0;
			for (int wrow=-1; wrow<2; wrow++) {
				for (int wcol=-1; wcol<2; wcol++) {
					int xpos = row+wrow;
					int ypos = col+wcol;
					if (xpos>=0 && ypos>=0 && xpos<height && ypos<width) {
						sumval += ip[xpos*cols+ypos] * KERNEL[wrow+1][wcol+1];

						count+=KERNEL[wrow+1][wcol+1];
					}
				}
			}
			op[row*width+col] = t[(OUT_T)(sumval/count)];
		}
	}
#else
    while (height > 0) {
		opLineEnd = op + width;
		while (op < opLineEnd) {
			IN_T c = *(ip++);
			*op = t[c];
#if (OUT == 32)
			if (reduceColor) {
				*op = GetNearestColor(*op);
			}
#endif
			op++;
		}

		ip += ipextra;
		height--;
    }
#endif
}


/*
 * rfbTranslateWithRGBTablesINtoOUT translates a rectangle of pixel data
 * using three separate lookup tables for the red, green and blue values.
 */

static void
rfbTranslateWithRGBTablesINtoOUT (char *table, rfbPixelFormat *in,
				  rfbPixelFormat *out,
				  char *iptr, char *optr,
				  int bytesBetweenInputLines,
				  int width, int height, BOOL reduceColor)
{
    IN_T *ip = (IN_T *)iptr;
    OUT_T *op = (OUT_T *)optr;
    int ipextra = bytesBetweenInputLines / sizeof(IN_T) - width;
    OUT_T *opLineEnd;
    OUT_T *redTable = (OUT_T *)table;
    OUT_T *greenTable = redTable + in->redMax + 1;
    OUT_T *blueTable = greenTable + in->greenMax + 1;

#if 0
	int cols = bytesBetweenInputLines / sizeof(IN_T);
	int sumvalr;
	int sumvalg;
	int sumvalb;
	int count;
	// smooth the original image
	for (int row=0;row<height;row++) {
		for (int col=0; col<width; col++) {
			sumvalr = 0;
			sumvalg = 0;
			sumvalb = 0;
			count = 0;
			for (int wrow=-1; wrow<2; wrow++) {
				for (int wcol=-1; wcol<2; wcol++) {
					int xpos = row+wrow;
					int ypos = col+wcol;
					if (xpos>=0 && ypos>=0 && xpos<height && ypos<width) {
						int pos = xpos*cols+ypos;
						sumvalr += ((ip[pos] >> in->redShift) & in->redMax) * KERNEL[wrow+1][wcol+1];
						sumvalg += ((ip[pos] >> in->greenShift) & in->greenMax) * KERNEL[wrow+1][wcol+1];
						sumvalb += ((ip[pos] >> in->blueShift) & in->blueMax) * KERNEL[wrow+1][wcol+1];
						count+=KERNEL[wrow+1][wcol+1];
					}
				}
			}
			op[row*width+col] = (redTable[sumvalr/count] | greenTable[sumvalg/count] | blueTable[sumvalb/count]);
		}
	}

#else
    while (height > 0) {
		opLineEnd = op + width;

		while (op < opLineEnd) {
			IN_T c = *(ip);
			*op = (redTable[(c >> in->redShift) & in->redMax] |
		       greenTable[(c >> in->greenShift) & in->greenMax] |
		       blueTable[(c >> in->blueShift) & in->blueMax]);
#if (OUT == 32)
			if (reduceColor) {
				*op = GetNearestColor(*op);
			}
#endif
			op++;
			ip++;
		}
		ip += ipextra;
		height--;
    }
#endif
}

#undef IN_T
#undef OUT_T
#undef rfbTranslateWithSingleTableINtoOUT
#undef rfbTranslateWithRGBTablesINtoOUT
