//  Copyright (C) 2002 Ultr@VNC Team Members. All Rights Reserved.
//  Copyright (C) 2000-2002 Const Kaplinsky. All Rights Reserved.
//  Copyright (C) 2002 RealVNC Ltd. All Rights Reserved.
//  Copyright (C) 1999 AT&T Laboratories Cambridge. All Rights Reserved.
//
//  This file is part of the VNC system.
//
//  The VNC system is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// If the source code for the VNC system is not available from the place 
// whence you received this file, check http://www.uk.research.att.com/vnc or contact
// the authors on vnc@uk.research.att.com for information on obtaining it.


// vncClient.cpp

// The per-client object.  This object takes care of all per-client stuff,
// such as socket input and buffering of updates.

// vncClient class handles the following functions:
// - Recieves requests from the connected client and
//   handles them
// - Handles incoming updates properly, using a vncBuffer
//   object to keep track of screen changes
// It uses a vncBuffer and is passed the vncDesktop and
// vncServer to communicate with.

// Includes
#include "stdhdrs.h"
#include <omnithread.h>
#include "resource.h"

// Custom
#include "winvnc.h"
#include "vncServer.h"
#include "vncClient.h"
#include "VSocket.h"
#include "vncDesktop.h"
#include "rfbRegion.h"
#include "vncBuffer.h"
#include "vncService.h"
#include "vncPasswd.h"
#include "vncAcceptDialog.h"
#include "vncKeymap.h"

#include "zlib/zlib.h" // sf@2002
#include "mmSystem.h" // sf@2002

#include "../../netlib/log.h"	// IIC
extern "C" int net_set_next_hunt_port();	// IIC
extern "C" void net_set_connected(); // IIC

// #include "rfb.h"

// vncClient update thread class

class vncClientUpdateThread : public omni_thread
{
public:

	// Init
	BOOL Init(vncClient *client);

	// Kick the thread to send an update
	void Trigger();

	// Kill the thread
	void Kill();

	// Disable/enable updates
	void EnableUpdates(BOOL enable);

	// The main thread function
    virtual void *run_undetached(void *arg);
	void *run_internal(void *arg);

protected:
	virtual ~vncClientUpdateThread();

	// Fields
protected:
	vncClient *m_client;
	omni_condition *m_signal;
	omni_condition *m_sync_sig;
	BOOL m_active;
	BOOL m_enable;
};

BOOL
vncClientUpdateThread::Init(vncClient *client)
{
	vnclog.Print(LL_INTINFO, VNCLOG("init update thread\n"));

	m_client = client;
	omni_mutex_lock l(m_client->GetUpdateLock());
	m_signal = new omni_condition(&m_client->GetUpdateLock());
	m_sync_sig = new omni_condition(&m_client->GetUpdateLock());
	m_active = TRUE;
	m_enable = m_client->m_disable_protocol == 0;
	if (m_signal && m_sync_sig) {
		start_undetached();
		return TRUE;
	}
	m_client->m_hasControl = FALSE;	// IIC
	return FALSE;
}

vncClientUpdateThread::~vncClientUpdateThread()
{
	if (m_signal) delete m_signal;
	if (m_sync_sig) delete m_sync_sig;
	vnclog.Print(LL_INTINFO, VNCLOG("update thread gone\n"));
}

void
vncClientUpdateThread::Trigger()
{
	// ALWAYS lock client UpdateLock before calling this!
	// Only trigger an update if protocol is enabled
	if (m_client->m_disable_protocol == 0) {
		m_signal->signal();
	}
}

void
vncClientUpdateThread::Kill()
{
	vnclog.Print(LL_INTINFO, VNCLOG("kill update thread\n"));

	omni_mutex_lock l(m_client->GetUpdateLock());
	m_active=FALSE;
	m_signal->signal();
}

void
vncClientUpdateThread::EnableUpdates(BOOL enable)
{
	// ALWAYS call this with the UpdateLock held!
	if (enable) {
		vnclog.Print(LL_INTINFO, VNCLOG("enable update thread\n"));
	} else {
		vnclog.Print(LL_INTINFO, VNCLOG("disable update thread\n"));
	}

	m_enable = enable;
	m_signal->signal();
	m_sync_sig->wait();
	vnclog.Print(LL_INTINFO, VNCLOG("enable/disable synced\n"));
}

// IIC - trap exceptions and notify containing window
void* 
vncClientUpdateThread::run_undetached(void *arg)
{
	try
	{
		return run_internal(arg);
	}
	catch (...)
	{
		ReportInternalError(true);
	}

	return NULL;
}


void*
vncClientUpdateThread::run_internal(void *arg)
{
	rfb::SimpleUpdateTracker update;
	rfb::Region2D clipregion;
	char *clipboard_text = 0;
	update.enable_copyrect(true);
	BOOL send_palette = FALSE;
	bool send_noop = false;	// IIC

	unsigned long updates_sent=0;

	vnclog.Print(LL_INTINFO, VNCLOG("starting update thread\n"));

	// Set client update threads to high priority
	// *** set_priority(omni_thread::PRIORITY_HIGH);

	while (1)
	{
		// Block waiting for an update to send
		{
			omni_mutex_lock l(m_client->GetUpdateLock());
			m_client->m_incr_rgn = m_client->m_incr_rgn.union_(clipregion);

			// We block as long as updates are disabled, or the client
			// isn't interested in them, unless this thread is killed.
			while (m_active && (
						!m_enable || (
							m_client->m_update_tracker.get_changed_region().intersect(m_client->m_incr_rgn).is_empty() &&
							m_client->m_update_tracker.get_copied_region().intersect(m_client->m_incr_rgn).is_empty() &&
							m_client->m_update_tracker.get_cached_region().intersect(m_client->m_incr_rgn).is_empty() &&
							m_client->m_update_tracker.get_Text_region().intersect(m_client->m_incr_rgn).is_empty() &&
							m_client->m_update_tracker.get_Solid_region().intersect(m_client->m_incr_rgn).is_empty() &&
							!m_client->m_clipboard_text
							)
						)
					) {
				// Issue the synchronisation signal, to tell other threads
				// where we have got to
				m_sync_sig->broadcast();

				unsigned long time_sec, time_nsec;

				// Wait to be kicked into action
				do {
					omni_thread::get_time(&time_sec, &time_nsec, 120, 0);
					send_noop = m_signal->timedwait(time_sec, time_nsec) == 0;

					// IIC - send no-op if no other traffic is to be sent
					if (send_noop)
					{
						m_client->SendCmdMessage("<internal>noop</internal>");
					}
				} while (send_noop);
			}

			// If the thread is being killed then quit
			if (!m_active) break;

			if (m_client->m_request_recording != m_client->m_enable_recording)
			{
				m_client->m_server->EnableRecording(m_client->m_request_recording);
				m_client->EnableRecording(m_client->m_request_recording);
			}

			// SEND AN UPDATE!
			// The thread is active, updates are enabled, and the
			// client is expecting an update - let's see if there
			// is anything to send.

			// Modif sf@2002 - FileTransfer - Don't send anything if a file transfer is running !
			// if (m_client->m_fFileTransferRunning) return 0;
			// if (m_client->m_pTextChat->m_fTextChatRunning) return 0;

			// sf@2002
			// New scale requested, we do it before sending the next Update
			if (m_client->fNewScale)
			{
				// Send the new framebuffer size to the client
				rfb::Rect ViewerSize = m_client->m_encodemgr.m_buffer->GetViewerSize();
				
				// Copyright (C) 2001 - Harakan software
				if (m_client->m_fPalmVNCScaling)
				{
					rfb::Rect ScreenSize = m_client->m_encodemgr.m_buffer->GetSize();
					rfbPalmVNCReSizeFrameBufferMsg rsfb;
					rsfb.type = rfbPalmVNCReSizeFrameBuffer;
					rsfb.desktop_w = Swap16IfLE(ScreenSize.br.x);
					rsfb.desktop_h = Swap16IfLE(ScreenSize.br.y);
					rsfb.buffer_w = Swap16IfLE(ViewerSize.br.x);
					rsfb.buffer_h = Swap16IfLE(ViewerSize.br.y);
					m_client->m_socket->SendExact((char*)&rsfb,
													sz_rfbPalmVNCReSizeFrameBufferMsg,
													rfbPalmVNCReSizeFrameBuffer);
				}
				else // eSVNC-UltraVNC Scaling
				{
					rfbResizeFrameBufferMsg rsmsg;
					rsmsg.type = rfbResizeFrameBuffer;
					rsmsg.framebufferWidth  = Swap16IfLE(ViewerSize.br.x);
					rsmsg.framebufferHeigth = Swap16IfLE(ViewerSize.br.y);
					m_client->m_socket->SendExact((char*)&rsmsg,
													sz_rfbResizeFrameBufferMsg,
													rfbResizeFrameBuffer);
				}

				m_client->m_encodemgr.m_buffer->ClearCache();
				m_client->fNewScale = false;
				m_client->m_fPalmVNCScaling = false;

				// return 0;
			}

			// Has the palette changed?
			send_palette = m_client->m_palettechanged;
			m_client->m_palettechanged = FALSE;

			// Fetch the incremental region
			clipregion = m_client->m_incr_rgn;
			m_client->m_incr_rgn.clear();

			// Get the clipboard data, if any
			if (m_client->m_clipboard_text) {
				clipboard_text = m_client->m_clipboard_text;
				m_client->m_clipboard_text = 0;
			}
		
			// Get the update details from the update tracker
			m_client->m_update_tracker.flush_update(update, clipregion);

			//if (!m_client->m_encodemgr.m_buffer->m_desktop->IsVideoDriverEnabled())
			if (!m_client->m_encodemgr.m_buffer->m_desktop->m_hookdriver)
			{
				// Render the mouse if required
				m_client->m_cursor_update_pending = m_client->m_encodemgr.IsCursorUpdatePending();
				if (!m_client->m_cursor_update_sent && !m_client->m_cursor_update_pending) 
				{
					if (m_client->m_mousemoved)
					{
						// Re-render its old location
						m_client->m_oldmousepos =
							m_client->m_oldmousepos.intersect(m_client->GetScaledScreen()); // sf@2002 IIC
						if (!m_client->m_oldmousepos.is_empty())
							update.add_changed(m_client->m_oldmousepos);

						// And render its new one
						m_client->m_encodemgr.m_buffer->GetMousePos(m_client->m_oldmousepos);
						m_client->m_oldmousepos =
							m_client->m_oldmousepos.intersect(m_client->GetScaledScreen());	//IIC
						if (!m_client->m_oldmousepos.is_empty())
							update.add_changed(m_client->m_oldmousepos);
		
						m_client->m_mousemoved = FALSE;
					}
				}
			}
			// Get the encode manager to update the client back buffer
		/*	m_client->m_encodemgr.GrabRegion(update.get_changed_region());*/
		
		}

		// SEND THE CLIPBOARD
		// If there is clipboard text to be sent then send it
		if (clipboard_text && !m_client->m_IsLoopback) {
			omni_mutex_lock l(m_client->GetUpdateLock());	// IIC

			rfbServerCutTextMsg message;

			message.length = Swap32IfLE(strlen(clipboard_text));
			if (!m_client->SendRFBMsg(rfbServerCutText,
				(BYTE *) &message, sizeof(message))) {
				m_client->m_socket->Close();
			}
			if (!m_client->m_socket->SendExact(clipboard_text, strlen(clipboard_text)))
			{
				m_client->m_socket->Close();
			}
			free(clipboard_text);
			clipboard_text = 0;
		}

		// SEND AN UPDATE
		// We do this without holding locks, to avoid network problems
		// stalling the server.

		// Update the client palette if necessary
		if (send_palette) {
			omni_mutex_lock l(m_client->GetUpdateLock());	// IIC
			m_client->SendPalette();
		}

		if (m_client->m_NewSWUpdateWaiting) {
			omni_mutex_lock l(m_client->GetUpdateLock());	// IIC
			m_client->SendNewSWSize();
		} else {
			omni_mutex_lock l(m_client->GetUpdateLock());	// IIC
			// Send updates to the client - this implicitly clears
			// the supplied update tracker
			if (m_client->SendUpdate(update)) {
				updates_sent++;
				clipregion.clear();
			}
		}

		// Serialize the sending here, to avoid corrupting updates
//		m_client->SendYieldControl();   // IIC

		yield();
	}

	vnclog.Print(LL_INTINFO, VNCLOG("stopping update thread\n"));
	
	vnclog.Print(LL_INTERR, "client sent %lu updates\n", updates_sent);
	return 0;
}

// vncClient thread class

class vncClientThread : public omni_thread
{
public:

	// Init
	virtual BOOL Init(vncClient *client,
		vncServer *server,
		VSocket *socket,
		BOOL auth,
		BOOL shared);

	// Sub-Init routines
	virtual BOOL InitVersion();
	virtual BOOL InitAuthenticate();

	// The main thread function
	virtual void run(void *arg);
	virtual void run_internal(void *arg);

protected:
	virtual ~vncClientThread();

	// Fields
protected:
	VSocket *m_socket;
	vncServer *m_server;
	vncClient *m_client;
	BOOL m_auth;
	BOOL m_shared;
	BOOL m_ms_logon;
};

vncClientThread::~vncClientThread()
{
	// If we have a client object then delete it
	if (m_client != NULL)
		delete m_client;
}

BOOL
vncClientThread::Init(vncClient *client, vncServer *server, VSocket *socket, BOOL auth, BOOL shared)
{
	// Save the server pointer and window handle
	m_server = server;
	m_socket = socket;
	m_client = client;
	m_auth = auth;
	m_shared = shared;

	// Start the thread
	start();

	return TRUE;
}

BOOL
vncClientThread::InitVersion()
{
	// Generate the server's protocol version
	rfbProtocolVersionMsg protocolMsg;
	sprintf((char *)protocolMsg,
		rfbProtocolVersionFormat,
		rfbProtocolMajorVersion,
		rfbProtocolMinorVersion + (m_server->MSLogonRequired() ? 0 : 2)); // 4: mslogon+FT,
																		 // 6: VNClogon+FT
	// Send the protocol message
	if (!m_socket->SendExact((char *)&protocolMsg, sz_rfbProtocolVersionMsg))
		return FALSE;

	// Now, get the client's protocol version
	rfbProtocolVersionMsg protocol_ver;
	protocol_ver[12] = 0;
	if (!m_socket->ReadExact((char *)&protocol_ver, sz_rfbProtocolVersionMsg))
		return FALSE;

	// Check viewer's the protocol version
	int major, minor;
	sscanf((char *)&protocol_ver, rfbProtocolVersionFormat, &major, &minor);
	if (major != rfbProtocolMajorVersion)
		return FALSE;

	// TODO: Maybe change this UltraVNC specific minor value because
	// TightVNC viewer uses minor = 5 ...
	// For now:
	// UltraViewer always sends minor = 4
	// UltraServer sends minor = 4 or minor = 6
	// m_ms_logon = false; // For all non-UltraVNC logon compatible viewers
	m_ms_logon = m_server->MSLogonRequired();
	if (minor == 4) m_client->SetUltraViewer(true); else m_client->SetUltraViewer(false);

	return TRUE;
}


BOOL
vncClientThread::InitAuthenticate()
{
	// Retrieve the local password
	char password[MAXPWLEN];
	char passwordMs[MAXMSPWLEN];
	m_server->GetPassword(password);
	vncPasswd::ToText plain(password);

	// Verify the peer host name against the AuthHosts string
	vncServer::AcceptQueryReject verified;
	if (m_auth) {
		verified = vncServer::aqrAccept;
	} else {
		verified = m_server->VerifyHost(m_socket->GetPeerName());
	}
	
	// If necessary, query the connection with a timed dialog
	if (verified == vncServer::aqrQuery) {
		vncAcceptDialog *acceptDlg = new vncAcceptDialog(m_server->QueryTimeout(), m_socket->GetPeerName());
		if ((acceptDlg == 0) || (!(acceptDlg->DoDialog())))
			verified = vncServer::aqrReject;
		//CHECKCHECK
//		if (acceptDlg !=NULL) delete acceptDlg;
		//CHECKCHECK
	}
	if (verified == vncServer::aqrReject) {
		CARD32 auth_val = Swap32IfLE(rfbConnFailed);
		char *errmsg = "Your connection has been rejected.";
		CARD32 errlen = Swap32IfLE(strlen(errmsg));
		if (!m_socket->SendExact((char *)&auth_val, sizeof(auth_val)))
			return FALSE;
		if (!m_socket->SendExact((char *)&errlen, sizeof(errlen)))
			return FALSE;
		m_socket->SendExact(errmsg, strlen(errmsg));
		return FALSE;
	}

	// By default we disallow passwordless workstations!
	if ((strlen(plain) == 0) && m_server->AuthRequired())
	{
		vnclog.Print(LL_CONNERR, VNCLOG("no password specified for server - client rejected\n"));

		// Send an error message to the client
		CARD32 auth_val = Swap32IfLE(rfbConnFailed);
		char *errmsg =
			"This server does not have a valid password enabled.  "
			"Until a password is set, incoming connections cannot be accepted.";
		CARD32 errlen = Swap32IfLE(strlen(errmsg));

		if (!m_socket->SendExact((char *)&auth_val, sizeof(auth_val)))
			return FALSE;
		if (!m_socket->SendExact((char *)&errlen, sizeof(errlen)))
			return FALSE;
		m_socket->SendExact(errmsg, strlen(errmsg));

		return FALSE;
	}

	// By default we filter out local loop connections, because they're pointless
	if (!m_server->LoopbackOk())
	{
		char *localname = strdup(m_socket->GetSockName());
		char *remotename = strdup(m_socket->GetPeerName());

		// Check that the local & remote names are different!
		if ((localname != NULL) && (remotename != NULL))
		{
			BOOL ok = strcmp(localname, remotename) != 0;

			if (localname != NULL)
				free(localname);

			if (remotename != NULL)
				free(remotename);

			if (!ok)
			{
				vnclog.Print(LL_CONNERR, VNCLOG("loopback connection attempted - client rejected\n"));
				
				// Send an error message to the client
				CARD32 auth_val = Swap32IfLE(rfbConnFailed);
				char *errmsg = "Local loop-back connections are disabled.";
				CARD32 errlen = Swap32IfLE(strlen(errmsg));

				if (!m_socket->SendExact((char *)&auth_val, sizeof(auth_val)))
					return FALSE;
				if (!m_socket->SendExact((char *)&errlen, sizeof(errlen)))
					return FALSE;
				m_socket->SendExact(errmsg, strlen(errmsg));

				return FALSE;
			}
		}
	}
	else
	{
		char *localname = strdup(m_socket->GetSockName());
		char *remotename = strdup(m_socket->GetPeerName());

		// Check that the local & remote names are different!
		if ((localname != NULL) && (remotename != NULL))
		{
			BOOL ok = strcmp(localname, remotename) != 0;

			if (localname != NULL)
				free(localname);

			if (remotename != NULL)
				free(remotename);

			if (!ok)
			{
				vnclog.Print(LL_CONNERR, VNCLOG("loopback connection attempted - client accepted\n"));
				m_client->m_IsLoopback=true;
			}
		}

	}

	// Authenticate the connection, if required
	if (m_auth || (strlen(plain) == 0))
	{
		// Send no-auth-required message
		CARD32 auth_val = Swap32IfLE(rfbNoAuth);
		if (!m_socket->SendExact((char *)&auth_val, sizeof(auth_val)))
			return FALSE;
	}
	else
	{
		// Send auth-required message
		CARD32 auth_val = Swap32IfLE(rfbVncAuth);
		if (!m_socket->SendExact((char *)&auth_val, sizeof(auth_val)))
			return FALSE;

		BOOL auth_ok = TRUE;
		{
			/*
			// sf@2002 - DSMPlugin
			// Use Plugin only from this point for the moment
			if (m_server->GetDSMPluginPointer()->IsEnabled())
			{
				m_socket->EnableUsePlugin(true);
				// TODO: Make a more secured challenge (with time stamp)
			}
			*/

			// Now create a 16-byte challenge
			char challenge[16];
			char challengems[64];
			char response[16];
			char responsems[64];
			char *plainmsPasswd;
			char user[256];
			char domain[256];
			vncRandomBytes((BYTE *)&challenge);
			vncRandomBytesMs((BYTE *)&challengems);

			// Send the challenge to the client
			// m_ms_logon = false;
			if (m_ms_logon)
			{
				if (!m_socket->SendExact(challengems, sizeof(challengems)))
					return FALSE;
				if (!m_socket->SendExact(challenge, sizeof(challenge)))
						return FALSE;
				if (!m_socket->ReadExact(user, sizeof(char)*256))
					return FALSE;
				if (!m_socket->ReadExact(domain, sizeof(char)*256))
					return FALSE;
				// Read the response
				if (!m_socket->ReadExact(responsems, sizeof(responsems)))
					return FALSE;
				if (!m_socket->ReadExact(response, sizeof(response)))
						return FALSE;
				
				// TODO: Improve this... 
				for (int i = 0; i < 32 ;i++)
				{
					passwordMs[i] = challengems[i]^responsems[i];
				}

				// REM: Instead of the fixedkey, we could use VNC password
				// -> the user needs to enter the VNC password on viewer's side.
				plainmsPasswd = vncDecryptPasswdMs((char *)passwordMs);

				// We let it as is right now for testing purpose.
				if (strlen(user) == 0 && !m_server->MSLogonRequired())
				{
					vncEncryptBytes((BYTE *)&challenge, plain);
	
					// Compare them to the response
					for (int i=0; i<sizeof(challenge); i++)
					{
						if (challenge[i] != response[i])
						{
							auth_ok = FALSE;
							break;
						}
					}
				}
				else if (!CheckUserGroupPasswordUni(user,plainmsPasswd,m_client->GetClientName()))
				// else if (!SSPLogonUser(domain, user, plainmsPasswd)) 
				{
					/*
					char szMes[512];
					sprintf(szMes, "%s - %s - %s", user, plainmsPasswd, domain);
					MessageBox(NULL, szMes, "Unable to process MS logon", MB_OK | MB_ICONEXCLAMATION);
					*/
					auth_ok = FALSE;
				}
				else
				{
					char szMes[512];
					sprintf(szMes, "%s\\\\%s %s", domain,user, plainmsPasswd);
					m_server->SetSuCommand(szMes);
				}
				if (plainmsPasswd) free(plainmsPasswd);
				plainmsPasswd=NULL;
			}
			else
			{
				if (!m_socket->SendExact(challenge, sizeof(challenge)))
					return FALSE;


				// Read the response
				if (!m_socket->ReadExact(response, sizeof(response)))
					return FALSE;
				// Encrypt the challenge bytes
				vncEncryptBytes((BYTE *)&challenge, plain);

				// Compare them to the response
				for (int i=0; i<sizeof(challenge); i++)
				{
					if (challenge[i] != response[i])
					{
						auth_ok = FALSE;
						break;
					}
				}
			}
		}

		// Did the authentication work?
		CARD32 authmsg;
		if (!auth_ok)
		{
			vnclog.Print(LL_CONNERR, VNCLOG("authentication failed\n"));
			//////////////////
			// LOG it also in the event
			//////////////////
			{
				typedef BOOL (*LogeventFn)(char *machine);
				LogeventFn Logevent = 0;
				char szCurrentDir[MAX_PATH];
				if (GetModuleFileName(NULL, szCurrentDir, MAX_PATH))
					{
						char* p = strrchr(szCurrentDir, '\\');
						*p = '\0';
						strcat (szCurrentDir,"\\auth.dll");
					}
				HMODULE hModule = LoadLibrary(szCurrentDir);
				if (hModule)
					{
						BOOL result=false;
						Logevent = (LogeventFn) GetProcAddress( hModule, "LOGFAILED" );
						Logevent((char *)m_client->GetClientName());
						FreeLibrary(hModule);
					}
			}

			authmsg = Swap32IfLE(rfbVncAuthFailed);
			m_socket->SendExact((char *)&authmsg, sizeof(authmsg));
			return FALSE;
		}
		else
		{
			// Tell the client we're ok
			authmsg = Swap32IfLE(rfbVncAuthOK);
			//////////////////
			// LOG it also in the event
			//////////////////
			if (!m_ms_logon){
				typedef BOOL (*LogeventFn)(char *machine);
				LogeventFn Logevent = 0;
				char szCurrentDir[MAX_PATH];
				if (GetModuleFileName(NULL, szCurrentDir, MAX_PATH))
					{
						char* p = strrchr(szCurrentDir, '\\');
						*p = '\0';
						strcat (szCurrentDir,"\\auth.dll");
					}
				HMODULE hModule = LoadLibrary(szCurrentDir);
				if (hModule)
					{
						BOOL result=false;
						Logevent = (LogeventFn) GetProcAddress( hModule, "LOGLOGON" );
						Logevent((char *)m_client->GetClientName());
						FreeLibrary(hModule);
					}
			}
			if (!m_socket->SendExact((char *)&authmsg, sizeof(authmsg)))
				return FALSE;
		}
	}

	// Read the client's initialisation message
	rfbClientInitMsg client_ini;
	if (!m_socket->ReadExact((char *)&client_ini, sz_rfbClientInitMsg))
		return FALSE;

	// If the client wishes to have exclusive access then remove other clients
	if (!client_ini.shared && !m_shared)
	{
		// Which client takes priority, existing or incoming?
		if (m_server->ConnectPriority() < 1)
		{
			// Incoming
			vnclog.Print(LL_INTINFO, VNCLOG("non-shared connection - disconnecting old clients\n"));
			m_server->KillAuthClients();
		} else if (m_server->ConnectPriority() > 1)
		{
			// Existing
			if (m_server->AuthClientCount() > 0)
			{
				vnclog.Print(LL_CLIENTS, VNCLOG("connections already exist - client rejected\n"));
				return FALSE;
			}
		}
	}

	// Tell the server that this client is ok
	return m_server->Authenticated(m_client->GetClientId());
}

void
ClearKeyState(BYTE key)
{
	// This routine is used by the VNC client handler to clear the
	// CAPSLOCK, NUMLOCK and SCROLL-LOCK states.

	BYTE keyState[256];
	
	GetKeyboardState((LPBYTE)&keyState);

	if(keyState[key] & 1)
	{
		// Simulate the key being pressed
		keybd_event(key, 0, KEYEVENTF_EXTENDEDKEY, 0);

		// Simulate it being release
		keybd_event(key, 0, KEYEVENTF_EXTENDEDKEY | KEYEVENTF_KEYUP, 0);
	}
}


// Modif sf@2002
// Get the local ip addresses as a human-readable string.
// If more than one, then with \n between them.
// If not available, then gets a message to that effect.
void GetIPString(char *buffer, int buflen)
{
    char namebuf[256];

    if (gethostname(namebuf, 256) != 0)
	{
		strncpy(buffer, "Host name unavailable", buflen);
		return;
    }

    HOSTENT *ph = gethostbyname(namebuf);
    if (!ph)
	{
		strncpy(buffer, "IP address unavailable", buflen);
		return;
    }

    *buffer = '\0';
    char digtxt[5];
    for (int i = 0; ph->h_addr_list[i]; i++)
	{
    	for (int j = 0; j < ph->h_length; j++)
		{
	    sprintf(digtxt, "%d.", (unsigned char) ph->h_addr_list[i][j]);
	    strncat(buffer, digtxt, (buflen-1)-strlen(buffer));
		}	
		buffer[strlen(buffer)-1] = '\0';
		if (ph->h_addr_list[i+1] != 0)
			strncat(buffer, ", ", (buflen-1)-strlen(buffer));
    }
}

// IIC
HWND GetTopWindowEx(HWND hWnd)
{
	char windowName[MAX_PATH];
	HWND hParent = GetAncestor(hWnd, GA_ROOTOWNER);
	GetWindowText(hParent, windowName, MAX_PATH);

	if (strcmp(windowName, "Program Manager")==0)
		return GetDesktopWindow();
	
	if (strcmp(windowName, "IIC Data Share Control")==0)
		return NULL;

	GetClassName(hParent, windowName, MAX_PATH);
	if (strcmp(windowName, "Program")==0 ||
		strcmp(windowName, "Shell_TrayWnd")==0)
		return NULL;

	return hParent;
}

// IIC - trap exceptions and notify containing window
void
vncClientThread::run(void *arg)
{
	try
	{
		run_internal(arg);
	}
	catch (...)
	{
		ReportInternalError(true);
	}
}

void
vncClientThread::run_internal(void *arg)
{
	// All this thread does is go into a socket-receive loop,
	// waiting for stuff on the given socket

	// IMPORTANT : ALWAYS call RemoveClient on the server before quitting
	// this thread.

	vnclog.Print(LL_CLIENTS, VNCLOG("client connected : %s (%hd)\n"),
								m_client->GetClientName(),
								m_client->GetClientId());

	// rd@2002 - Security logging
	//
	{
		char szMslogonLog[MAX_PATH];
		char texttowrite[512];
		if (GetModuleFileName(NULL, szMslogonLog, MAX_PATH))
		{
			char* p = strrchr(szMslogonLog, '\\');
			if (p != NULL)
			{
				*p = '\0';
				strcat (szMslogonLog,"\\security\\mslogon.log");
			}
		}
		FILE *file;
		file = fopen(szMslogonLog, "a");
		if(file!=NULL) {
			strcpy(texttowrite,m_client->GetClientName());
			fwrite( texttowrite, sizeof( char ), strlen(texttowrite),file);
			fclose(file);	
		}
	}
	// Save the handle to the thread's original desktop
	HDESK home_desktop = GetThreadDesktop(GetCurrentThreadId());
	
	// To avoid people connecting and then halting the connection, set a timeout
	if (!m_socket->SetTimeout(CONNECT_TIMEOUT))
		vnclog.Print(LL_INTERR, VNCLOG("failed to set socket timeout(%d)\n"), GetLastError());

	// sf@2002 - DSM Plugin - Tell the client's socket where to find the DSMPlugin 
	if (m_server->GetDSMPluginPointer() != NULL)
	{
		m_socket->SetDSMPluginPointer(m_server->GetDSMPluginPointer());
		vnclog.Print(LL_INTINFO, VNCLOG("DSMPlugin Pointer to socket OK\n"));
	}
	else
	{
		vnclog.Print(LL_INTINFO, VNCLOG("Invalid DSMPlugin Pointer\n"));
		return;
	}

	// Initially blacklist the client so that excess connections from it get dropped
	m_server->AddAuthHostsBlacklist(m_client->GetClientName());

	// LOCK INITIAL SETUP
	// All clients have the m_protocol_ready flag set to FALSE initially, to prevent
	// updates and suchlike interfering with the initial protocol negotiations.

	// sf@2002 - DSMPlugin
	// Use Plugin only from this point (now BEFORE Protocole handshaking)
	if (m_server->GetDSMPluginPointer()->IsEnabled())
	{
		m_socket->EnableUsePlugin(true);
		// TODO: Make a more secured challenge (with time stamp)
	}

	// IIC -- send init msg first
	rfbOmniInitMsg omni_init;
	if (*m_server->GetSessionToken() == '\0')
	{
		omni_init.type = 0;
		omni_init.version = rfbOmniVersion2;
		omni_init.idLen = Swap16IfLE(strlen(m_server->GetMeetingId()));
		if (!m_socket->SendExact((char *)&omni_init, sizeof(omni_init)))
		{
            errorf("Failed to send init msg.\r\n");
			m_server->RemoveClient(m_client->GetClientId());
			return;
		}

		if (!m_socket->SendExact(m_server->GetMeetingId(), strlen(m_server->GetMeetingId())))
		{
			errorf("Failed to send meeting id.\r\n");
			m_server->RemoveClient(m_client->GetClientId());
			return;
		}
	}
	else 
	{
		char id[512];

		// IIC protocol rev. 2 -- encode participant id and session token after meeting id
		int len = sprintf(id, "%s%c%s%c%s", m_server->GetMeetingId(), 0, m_server->GetParticipantId(), 0, m_server->GetSessionToken());

		omni_init.type = 0;
		omni_init.version = rfbOmniVersion2;
		omni_init.idLen = Swap16IfLE(len);
		if (!m_socket->SendExact((char *)&omni_init, sizeof(omni_init)))
		{
            errorf("Failed to send init msg\r\n");
			m_server->RemoveClient(m_client->GetClientId());
			return;
		}
		if (!m_socket->SendExact(id, len))
		{
            errorf("Failed to send meeting id.\r\n");
			m_server->RemoveClient(m_client->GetClientId());
			return;
		}
	}

	// Actually result from "omni" protocol, value is ignored by server
	rfbClientInitMsg client_ini;
	if (!m_socket->ReadExact((char *)&client_ini, sz_rfbClientInitMsg))
	{
        errorf("Failed to read client init msg.\r\n");
		m_server->RemoveClient(m_client->GetClientId());
		return;
	}

	// GET PROTOCOL VERSION
	if (!InitVersion())
	{
        errorf("Failed to negotiated version info.\r\n");
		m_server->RemoveClient(m_client->GetClientId());
		return;
	}
	vnclog.Print(LL_INTINFO, VNCLOG("negotiated version\n"));

	// IIC - mark this connection as successful.
	net_set_connected();

	// AUTHENTICATE LINK
	if (!InitAuthenticate())
	{
        errorf("Failed to authenticate client.\r\n");
		m_server->RemoveClient(m_client->GetClientId());
		return;
	}

    errorf("Connecting to share service...authentication complete.\r\n");

    // Authenticated OK - remove from blacklist and remove timeout
	m_server->RemAuthHostsBlacklist(m_client->GetClientName());
	m_socket->SetTimeout(m_server->AutoIdleDisconnectTimeout()*1000);
	vnclog.Print(LL_INTINFO, VNCLOG("authenticated connection\n"));

	// Set Client Connect time
	m_client->SetConnectTime(timeGetTime());

	// INIT PIXEL FORMAT

	// Get the screen format
	m_client->m_fullscreen = m_client->m_encodemgr.GetSize();

	// Modif sf@2002 - Scaling
	m_client->m_encodemgr.m_buffer->SetScale(m_server->GetDefaultScale()); // v1.1.2
	m_client->m_ScaledScreen = m_client->m_encodemgr.m_buffer->GetViewerSize();
	m_client->m_nScale = m_client->m_encodemgr.m_buffer->GetScale();

	// Get the name of this desktop
	// sf@2002 - v1.1.x - Complete the computer name with the IP address if necessary
	bool fIP = false;
	char desktopname[MAX_COMPUTERNAME_LENGTH + 1 + 256];
	DWORD desktopnamelen = MAX_COMPUTERNAME_LENGTH + 1 + 256;
	memset((char*)desktopname, 0, sizeof(desktopname));
	if (GetComputerName(desktopname, &desktopnamelen))
	{
		// Make the name lowercase
		for (int x=0; x<strlen(desktopname); x++)
		{
			desktopname[x] = tolower(desktopname[x]);
		}
		// Check for the presence of "." in the string (then it's presumably an IP adr)
		if (strchr(desktopname, '.') != NULL) fIP = true;
	}
	else
	{
		strcpy(desktopname, "IICServer");
	}

	// We add the IP address(es) to the computer name, if possible and necessary
	if (!fIP)
	{
		char szIP[256];
		GetIPString(szIP, sizeof(szIP));
		if (strlen(szIP) > 3 && szIP[0] != 'I' && szIP[0] != 'H') 
		{
			strcat(desktopname, " ( ");
			strcat(desktopname, szIP);
			strcat(desktopname, " )");
		}
	}

	// Send the server format message to the client
	rfbServerInitMsg server_ini;
	server_ini.format = m_client->m_encodemgr.m_buffer->GetLocalFormat();

	// Endian swaps
	// Modif sf@2002 - Scaling
	if (m_server->IsSharingPPT())
	{
		// IIC -- always sending the full desktop size to start (will be resized smaller when window handle is made available)
		// The reflector allocates buffers at share start and may crash if the screen is enlarged later
		server_ini.framebufferWidth = Swap16IfLE(GetSystemMetrics(SM_CXFULLSCREEN));
		server_ini.framebufferHeight = Swap16IfLE(GetSystemMetrics(SM_CYFULLSCREEN));
	}
	else
	{
		server_ini.framebufferWidth = Swap16IfLE(m_client->m_ScaledScreen.br.x - m_client->m_ScaledScreen.tl.x);
		server_ini.framebufferHeight = Swap16IfLE(m_client->m_ScaledScreen.br.y - m_client->m_ScaledScreen.tl.y);
	}

	debugf("Client size: %d x %d\n", m_client->m_ScaledScreen.br.x - m_client->m_ScaledScreen.tl.x, m_client->m_ScaledScreen.br.y - m_client->m_ScaledScreen.tl.y);

	// server_ini.framebufferWidth = Swap16IfLE(m_client->m_fullscreen.br.x-m_client->m_fullscreen.tl.x);
	// server_ini.framebufferHeight = Swap16IfLE(m_client->m_fullscreen.br.y-m_client->m_fullscreen.tl.y);

	server_ini.format.redMax = Swap16IfLE(server_ini.format.redMax);
	server_ini.format.greenMax = Swap16IfLE(server_ini.format.greenMax);
	server_ini.format.blueMax = Swap16IfLE(server_ini.format.blueMax);

	server_ini.nameLength = Swap32IfLE(strlen(desktopname));
	if (!m_socket->SendExact((char *)&server_ini, sizeof(server_ini)))
	{
        errorf("Failed to send desktop info.\r\n");
		m_server->RemoveClient(m_client->GetClientId());
		return;
	}
	if (!m_socket->SendExact(desktopname, strlen(desktopname)))
	{
        errorf("Failed to send desktop name.\r\n");
		m_server->RemoveClient(m_client->GetClientId());
		return;
	}
	vnclog.Print(LL_INTINFO, VNCLOG("sent pixel format to client\n"));

	// UNLOCK INITIAL SETUP
	// Initial negotiation is complete, so set the protocol ready flag
	m_client->EnableProtocol();
	
	// Add a fullscreen update to the client's update list
	// sf@2002 - Scaling
	// m_client->m_update_tracker.add_changed(m_client->m_fullscreen);
	{ // RealVNC 336
		omni_mutex_lock l(m_client->GetUpdateLock());
		m_client->m_update_tracker.add_changed(m_client->m_ScaledScreen);
	}


	// Clear the CapsLock and NumLock keys
	if (m_client->m_keyboardenabled)
	{
		ClearKeyState(VK_CAPITAL);
		// *** JNW - removed because people complain it's wrong
		//ClearKeyState(VK_NUMLOCK);
		ClearKeyState(VK_SCROLL);
	}

	// MAIN LOOP

	// Set the input thread to a high priority
	set_priority(omni_thread::PRIORITY_HIGH);
	
    errorf("Connected to share service.\r\n");

	// IIC	-- send initial recording msg
	m_client->EnableRecording(m_server->EnableRecording());
	m_client->m_request_recording = m_server->EnableRecording();

	BOOL connected = TRUE;
	while (connected)
	{
		rfbClientToServerMsg msg;

		// Ensure that we're running in the correct desktop
		if (!vncService::InputDesktopSelected())
			if (!vncService::SelectDesktop(NULL))
				break;

		// sf@2002 - v1.1.2
		int nTO = 1; // Type offset
		// If DSM Plugin, we must read all the transformed incoming rfb messages (type included)
		if (m_socket->IsUsePluginEnabled() && m_server->GetDSMPluginPointer()->IsEnabled())
		{
			if (!m_socket->ReadExact((char *)&msg.type, sizeof(msg.type)))
			{
				connected = FALSE;
				break;
			}
		    nTO = 0;
		}
		else
		{
			// Try to read a message ID
			if (!m_socket->ReadExact((char *)&msg.type, sizeof(msg.type)))
			{
				connected = FALSE;
				break;
			}
		}

		// What to do is determined by the message id
		switch(msg.type)
		{

		case rfbSetPixelFormat:
			// Read the rest of the message:
			if (!m_socket->ReadExact(((char *) &msg)+nTO, sz_rfbSetPixelFormatMsg-nTO))
			{
				connected = FALSE;
				break;
			}

			// Swap the relevant bits.
			msg.spf.format.redMax = Swap16IfLE(msg.spf.format.redMax);
			msg.spf.format.greenMax = Swap16IfLE(msg.spf.format.greenMax);
			msg.spf.format.blueMax = Swap16IfLE(msg.spf.format.blueMax);

			// Prevent updates while the pixel format is changed
			m_client->DisableProtocol();
				
			// Tell the buffer object of the change			
			if (!m_client->m_encodemgr.SetClientFormat(msg.spf.format))
			{
				vnclog.Print(LL_CONNERR, VNCLOG("remote pixel format invalid\n"));

				connected = FALSE;
			}

			// Set the palette-changed flag, just in case...
			m_client->m_palettechanged = TRUE;

			// Re-enable updates
			m_client->EnableProtocol();
			
			break;

		case rfbSetEncodings:
			// Read the rest of the message:
			if (!m_socket->ReadExact(((char *) &msg)+nTO, sz_rfbSetEncodingsMsg-nTO))
			{
				connected = FALSE;
				break;
			}

			// RDV cache
			m_client->m_encodemgr.EnableCache(FALSE);

	        // RDV XOR and client detection
			m_client->m_encodemgr.AvailableXOR(FALSE);
			m_client->m_encodemgr.AvailableZRLE(FALSE);
			m_client->m_encodemgr.AvailableTight(FALSE);

			// sf@2002 - Tight
			m_client->m_encodemgr.SetQualityLevel(-1);
			m_client->m_encodemgr.SetCompressLevel(9);	// IIC
			m_client->m_encodemgr.EnableLastRect(FALSE);

			// Tight - CURSOR HANDLING
			m_client->m_encodemgr.EnableXCursor(FALSE);
			m_client->m_encodemgr.EnableRichCursor(FALSE);
			m_client->m_server->EnableXRichCursor(FALSE);
			m_client->m_cursor_update_pending = FALSE;
			m_client->m_cursor_update_sent = FALSE;

			// Prevent updates while the encoder is changed
			m_client->DisableProtocol();

			// Read in the preferred encodings
			msg.se.nEncodings = Swap16IfLE(msg.se.nEncodings);
			{
				int x;
				BOOL encoding_set = FALSE;

				// By default, don't use copyrect!
				m_client->m_update_tracker.enable_copyrect(false);

				for (x=0; x<msg.se.nEncodings; x++)
				{
					CARD32 encoding;

					// Read an encoding in
					if (!m_socket->ReadExact((char *)&encoding, sizeof(encoding)))
					{
						connected = FALSE;
						break;
					}

					// Is this the CopyRect encoding (a special case)?
					if (Swap32IfLE(encoding) == rfbEncodingCopyRect)
					{
						m_client->m_update_tracker.enable_copyrect(true);
						continue;
					}

					// Is this a NewFBSize encoding request?
					if (Swap32IfLE(encoding) == rfbEncodingNewFBSize) {
						m_client->m_use_NewSWSize = TRUE;
						vnclog.Print(LL_INTINFO, VNCLOG("NewFBSize protocol extension enabled\n"));
						continue;
					}

					// CACHE RDV
					if (Swap32IfLE(encoding) == rfbEncodingCacheEnable)
					{
						m_client->m_encodemgr.EnableCache(TRUE);
						vnclog.Print(LL_INTINFO, VNCLOG("Cache protocol extension enabled\n"));
						continue;
					}

					// XOR zlib
					if (Swap32IfLE(encoding) == rfbEncodingXOREnable) {
						m_client->m_encodemgr.AvailableXOR(TRUE);
						vnclog.Print(LL_INTINFO, VNCLOG("XOR protocol extension enabled\n"));
						continue;
					}


					// Is this a CompressLevel encoding?
					if ((Swap32IfLE(encoding) >= rfbEncodingCompressLevel0) &&
						(Swap32IfLE(encoding) <= rfbEncodingCompressLevel9))
					{
						// Client specified encoding-specific compression level
						int level = (int)(Swap32IfLE(encoding) - rfbEncodingCompressLevel0);
						m_client->m_encodemgr.SetCompressLevel(level);
						vnclog.Print(LL_INTINFO, VNCLOG("compression level requested: %d\n"), level);
						continue;
					}

					// Is this a QualityLevel encoding?
					if ((Swap32IfLE(encoding) >= rfbEncodingQualityLevel0) &&
						(Swap32IfLE(encoding) <= rfbEncodingQualityLevel9))
					{
						// Client specified image quality level used for JPEG compression
						int level = (int)(Swap32IfLE(encoding) - rfbEncodingQualityLevel0);
						m_client->m_encodemgr.SetQualityLevel(level);
						vnclog.Print(LL_INTINFO, VNCLOG("image quality level requested: %d\n"), level);
						continue;
					}

					// Is this a LastRect encoding request?
					if (Swap32IfLE(encoding) == rfbEncodingLastRect) {
						m_client->m_encodemgr.EnableLastRect(TRUE); // We forbid Last Rect for now 
						vnclog.Print(LL_INTINFO, VNCLOG("LastRect protocol extension enabled\n"));
						continue;
					}

					// Is this an XCursor encoding request?
					if (Swap32IfLE(encoding) == rfbEncodingXCursor) {
						m_client->m_encodemgr.EnableXCursor(TRUE);
						m_client->m_server->EnableXRichCursor(TRUE);
						vnclog.Print(LL_INTINFO, VNCLOG("X-style cursor shape updates enabled\n"));
						continue;
					}

					// Is this a RichCursor encoding request?
					if (Swap32IfLE(encoding) == rfbEncodingRichCursor) {
						m_client->m_encodemgr.EnableRichCursor(TRUE);
						m_client->m_server->EnableXRichCursor(TRUE);
						vnclog.Print(LL_INTINFO, VNCLOG("Full-color cursor shape updates enabled\n"));
						continue;
					}

					// RDV - We try to detect which type of viewer tries to connect
					if (Swap32IfLE(encoding) == rfbEncodingZRLE) {
						m_client->m_encodemgr.AvailableZRLE(TRUE);
						vnclog.Print(LL_INTINFO, VNCLOG("ZRLE found \n"));
						// continue;
					}

					if (Swap32IfLE(encoding) == rfbEncodingTight) {
						m_client->m_encodemgr.AvailableTight(TRUE);
						vnclog.Print(LL_INTINFO, VNCLOG("Tight found\n"));
						// continue;
					}

					// Have we already found a suitable encoding?
					if (!encoding_set)
					{
						// No, so try the buffer to see if this encoding will work...
						if (m_client->m_encodemgr.SetEncoding(Swap32IfLE(encoding),FALSE))
							encoding_set = TRUE;
					}
				}

				// If no encoding worked then default to RAW!
				if (!encoding_set)
				{
					vnclog.Print(LL_INTINFO, VNCLOG("defaulting to raw encoder\n"));

					if (!m_client->m_encodemgr.SetEncoding(Swap32IfLE(rfbEncodingRaw),FALSE))
					{
						vnclog.Print(LL_INTERR, VNCLOG("failed to select raw encoder!\n"));

						connected = FALSE;
					}
				}

				// sf@2002 - For now we disable cache protocole when more than one client are connected
				// (But the cache buffer (if exists) is kept intact (for XORZlib usage))
				if (m_server->AuthClientCount() > 1)
					m_server->DisableCacheForAllClients();

			}

			// Re-enable updates
			m_client->EnableProtocol();

			break;
			
		case rfbFramebufferUpdateRequest:
			// Read the rest of the message:
			if (!m_socket->ReadExact(((char *) &msg)+nTO, sz_rfbFramebufferUpdateRequestMsg-nTO))
			{
				connected = FALSE;
				break;
			}

			{
				rfb::Rect update;

				// Get the specified rectangle as the region to send updates for
				// Modif sf@2002 - Scaling.
				update.tl.x = (Swap16IfLE(msg.fur.x) + m_client->m_SWOffsetx) * m_client->m_nScale;
				update.tl.y = (Swap16IfLE(msg.fur.y) + m_client->m_SWOffsety) * m_client->m_nScale;
				update.br.x = update.tl.x + Swap16IfLE(msg.fur.w) * m_client->m_nScale;
				update.br.y = update.tl.y + Swap16IfLE(msg.fur.h) * m_client->m_nScale;

				// IIC - translate from bitmap to screen
				update = update.translate(m_server->GetDesktopPointer()->GetOffset());
				// XXX update = update.intersect(m_server->GetDesktopPointer()->GetSize());	// IIC -- the viewer rect can be outside screen boundry
				debugf("Update region: %d,%d - %d,%d\n", update.tl.x, update.tl.y, update.br.x, update.br.y);

				rfb::Region2D update_rgn = update;

				// RealVNC 336
				if (update_rgn.is_empty()) {
					vnclog.Print(LL_INTERR, VNCLOG("FATAL! client update region is empty!\n"));
					//	IIC -- don't think this is a critical error, PPT window could be minimized, nothing to send
//					connected = FALSE;
					 // Kick the update thread (and create it if not there already)
					m_client->TriggerUpdateThread();
					break;	// don't want to set m_incr_rgn empty

				}

				{
					omni_mutex_lock l(m_client->GetUpdateLock());

					// Add the requested area to the incremental update cliprect
					m_client->m_incr_rgn = m_client->m_incr_rgn.union_(update_rgn);

					// Is this request for a full update?
					if (!msg.fur.incremental)
					{
						//vnclog.Print(LL_INTINFO, VNCLOG("Full screen update requested\n"));
						debugf("Full screen update requested\n");

						// Yes, so add the region to the update tracker
						m_client->m_update_tracker.add_changed(update_rgn);
						
						// Tell the desktop grabber to fetch the region's latest state
						m_client->m_encodemgr.m_buffer->m_desktop->QueueRect(update);

						// IIC
						// Reset encoder if full screen update is requested.  For tight
						// encoder, resets zlib dictionaries.
						m_client->m_encodemgr.ResetEncoder();
					}
					else 
					{
						debugf("Incremental update requested\n");
					}
					
					/* RealVNC 336 (removed)
					// Check that this client has an update thread
					// The update thread never dissappears, and this is the only
					// thread allowed to create it, so this can be done without locking.
					if (!m_client->m_updatethread)
					{
						m_client->m_updatethread = new vncClientUpdateThread;
						connected = (m_client->m_updatethread && 
							m_client->m_updatethread->Init(m_client));
					}
					*/

					 // Kick the update thread (and create it if not there already)
					m_client->TriggerUpdateThread();
				}
			}
			break;

		case rfbRequestControl:
			// IIC
			if (m_client->m_hasControl==FALSE)
			{
				m_server->GrantControl(m_client->m_id);

				m_client->m_hasControl = TRUE;
			}

			break;

		case rfbRequestRecord:
			// IIC
			if (m_socket->ReadExact(((char *) &msg)+nTO, sz_rfbRecordMsg-nTO))
			{
				omni_mutex_lock l(m_client->GetUpdateLock());
				int enabled = msg.rec.enable != 0;
				if (enabled != m_client->m_enable_recording)
				{
					m_client->m_request_recording = enabled;
					debugf("Recording change requested: %s\n", enabled ? "enabled" : "disabled");
				}
			}
			break;


		case rfbRequestCloseSession:
			// IIC
			m_server->CloseAllClients();
			m_server->EndSession();
			break;

		case rfbKeyEvent:
			// Read the rest of the message:
			if (m_socket->ReadExact(((char *) &msg)+nTO, sz_rfbKeyEventMsg-nTO))
			{				
				if (m_client->m_keyboardenabled && m_client->hasControl())
				{
					msg.ke.key = Swap32IfLE(msg.ke.key);

					// don't allow key event passed to blocking windows
					HWND hWnd = ::GetFocus();
					if (hWnd!=NULL && (((m_server->AppSharing()||m_server->IsSharingPPT()) && m_server->GetDesktopPointer()->IsSameProcess(hWnd, TRUE)==FALSE) ||
						 (m_server->IsTransparentEnabled() && m_server->GetDesktopPointer()->IsWindowTransparent(hWnd)))
						)
					{
						break;
					}

					// Get the keymapper to do the work
					// m_client->m_keymap.DoXkeysym(msg.ke.key, msg.ke.down);
					vncKeymap::keyEvent(msg.ke.key, msg.ke.down!=0);

					m_client->m_remoteevent = TRUE;
				}
			}
			break;

		case rfbPointerEvent:
			// Read the rest of the message:
			if (m_socket->ReadExact(((char *) &msg)+nTO, sz_rfbPointerEventMsg-nTO))
			{
				if (m_client->m_pointerenabled && m_client->hasControl())
				{
					// Convert the coords to Big Endian
					// Modif sf@2002 - Scaling
					msg.pe.x = (Swap16IfLE(msg.pe.x) + m_client->m_SWOffsetx+m_client->m_ScreenOffsetx) * m_client->m_nScale;
					msg.pe.y = (Swap16IfLE(msg.pe.y) + m_client->m_SWOffsety+m_client->m_ScreenOffsety) * m_client->m_nScale;

					// Work out the flags for this event
					DWORD flags = MOUSEEVENTF_ABSOLUTE;

					if (msg.pe.x != m_client->m_ptrevent.x ||
						msg.pe.y != m_client->m_ptrevent.y)
						flags |= MOUSEEVENTF_MOVE;
					if ( (msg.pe.buttonMask & rfbButton1Mask) != 
						(m_client->m_ptrevent.buttonMask & rfbButton1Mask) )
					{
					    if (GetSystemMetrics(SM_SWAPBUTTON))
						flags |= (msg.pe.buttonMask & rfbButton1Mask) 
						    ? MOUSEEVENTF_RIGHTDOWN : MOUSEEVENTF_RIGHTUP;
					    else
						flags |= (msg.pe.buttonMask & rfbButton1Mask) 
						    ? MOUSEEVENTF_LEFTDOWN : MOUSEEVENTF_LEFTUP;
					}
					if ( (msg.pe.buttonMask & rfbButton2Mask) != 
						(m_client->m_ptrevent.buttonMask & rfbButton2Mask) )
					{
						flags |= (msg.pe.buttonMask & rfbButton2Mask) 
						    ? MOUSEEVENTF_MIDDLEDOWN : MOUSEEVENTF_MIDDLEUP;
					}
					if ( (msg.pe.buttonMask & rfbButton3Mask) != 
						(m_client->m_ptrevent.buttonMask & rfbButton3Mask) )
					{
					    if (GetSystemMetrics(SM_SWAPBUTTON))
						flags |= (msg.pe.buttonMask & rfbButton3Mask) 
						    ? MOUSEEVENTF_LEFTDOWN : MOUSEEVENTF_LEFTUP;
					    else
						flags |= (msg.pe.buttonMask & rfbButton3Mask) 
						    ? MOUSEEVENTF_RIGHTDOWN : MOUSEEVENTF_RIGHTUP;
					}

					// Treat buttons 4 and 5 presses as mouse wheel events
					DWORD wheel_movement = 0;
					if (m_client->m_encodemgr.IsMouseWheelTight())
					{
						if ((msg.pe.buttonMask & rfbButton4Mask) != 0 &&
							(m_client->m_ptrevent.buttonMask & rfbButton4Mask) == 0)
						{
							flags |= MOUSEEVENTF_WHEEL;
							wheel_movement = (DWORD)+120;
						}
						else if ((msg.pe.buttonMask & rfbButton5Mask) != 0 &&
								 (m_client->m_ptrevent.buttonMask & rfbButton5Mask) == 0)
						{
							flags |= MOUSEEVENTF_WHEEL;
							wheel_movement = (DWORD)-120;
						}
					}
					else
					{
						// RealVNC 335 Mouse wheel support
						if (msg.pe.buttonMask & rfbWheelUpMask) {
							flags |= MOUSEEVENTF_WHEEL;
							wheel_movement = WHEEL_DELTA;
						}
						if (msg.pe.buttonMask & rfbWheelDownMask) {
							flags |= MOUSEEVENTF_WHEEL;
							wheel_movement = -WHEEL_DELTA;
						}
					}					

					// Generate coordinate values
					// bug fix John Latino
					// offset for multi display
					int screenX, screenY, screenDepth;
					m_server->GetScreenInfo(screenX, screenY, screenDepth);

					unsigned long x = (msg.pe.x *  65535) / (screenX+m_client->m_ScreenOffsetx);
					unsigned long y = (msg.pe.y * 65535) / (screenY+m_client->m_ScreenOffsety);

					debugf("mouse: %d,%d %d,%d\n", msg.pe.x, msg.pe.y, x, y);

					// IIC -- prevent msg being sent to hidden windows
					// IIC -- translate remote point (i.e. captured image coordinate) to desktop coord.
					POINT point;
					point.x = (long)msg.pe.x + m_client->m_server->GetDesktopPointer()->GetOffset().x;
					point.y = (long)msg.pe.y + m_client->m_server->GetDesktopPointer()->GetOffset().y;
					
					if (flags & MOUSEEVENTF_LEFTDOWN || flags & MOUSEEVENTF_RIGHTDOWN)
					{
						// ignore this client click and the subsequence clicks it might generate in 100ms
						m_server->GetDesktopPointer()->m_remote_point.x = point.x;
						m_server->GetDesktopPointer()->m_remote_point.y = point.y;
						m_server->GetDesktopPointer()->m_remote_click_time = GetTickCount();
					}

					// minimize blocking windows if client is trying to click on it
					while (true)
					{
						HWND hWnd = ::WindowFromPoint(point);
						if (hWnd == GetDesktopWindow()) break;
						if (hWnd!=NULL && 
							(((m_server->AppSharing()||m_server->IsSharingPPT()) && 
							  m_server->GetDesktopPointer()->IsSameProcess(hWnd, FALSE)==FALSE) ||
							 (m_server->IsTransparentEnabled() && m_server->GetDesktopPointer()->IsWindowTransparent(hWnd)))
							)
						{
							if (flags & MOUSEEVENTF_LEFTDOWN || flags & MOUSEEVENTF_LEFTUP || 
								flags & MOUSEEVENTF_RIGHTDOWN || flags & MOUSEEVENTF_RIGHTUP)
							{
								HWND hWndTop = GetTopWindowEx(hWnd);
								if (hWndTop == GetDesktopWindow()) break;
								if (hWndTop != NULL)
								{
									DWORD dwStyle = GetWindowLong(hWndTop, GWL_STYLE);
									if ((dwStyle & WS_DISABLED)==0)
									{
										ShowWindow(hWndTop, SW_SHOWMINIMIZED);
										continue;
									}
									else
									{
										FLASHWINFO fi;
										fi.cbSize	= sizeof (FLASHWINFO);
										fi.hwnd		= hWndTop;
										fi.dwFlags	= FLASHW_CAPTION|FLASHW_TRAY;
										fi.uCount	= 6;
										fi.dwTimeout= 100;

										MessageBeep(-1);
										FlashWindowEx(&fi);
									}
								}
								// filter out clicks
								flags = MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE;
							}
						}

						break;
					}
/*
					HWND hWnd = ::WindowFromPoint(point);
					if (hWnd!=NULL && ((m_client->m_server->AppSharing() &&
						 m_client->m_encodemgr.m_buffer->m_desktop->IsSameProcess(hWnd)==FALSE) ||
						 (m_client->m_server->IsTransparentEnabled() && 
						 m_client->m_encodemgr.m_buffer->m_desktop->IsWindowTransparent(hWnd)))
						)
					{
						// filter out clicks
						flags = MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE;
					}
*/
					// IIC - neither mouse_event or SendInput seem to support multiple monitors, so explicitly set
					// cursor to desired position, then use mouse_event for buttons, etc.
					if (!m_client->m_server->GetDesktopPointer()->IsPrimaryMonitor())
					{
						x = y = 0;
						flags &= ~MOUSEEVENTF_MOVE;
						int offx = (int)msg.pe.x + m_client->m_server->GetDesktopPointer()->GetOffset().x;
						int offy = (int)msg.pe.y + m_client->m_server->GetDesktopPointer()->GetOffset().y;
						SetCursorPos(offx, offy);
					}

					// Do the pointer event
					::mouse_event(flags, (DWORD) x, (DWORD) y, wheel_movement, 0);

					// Save the old position
					m_client->m_ptrevent = msg.pe;

					// Flag that a remote event occurred
					m_client->m_remoteevent = TRUE;

					// Tell the desktop hook system to grab the screen...
					m_client->m_encodemgr.m_buffer->m_desktop->TriggerUpdate();
				}
			}
			
			break;

		case rfbClientCutText:
			// Read the rest of the message:
			if (m_socket->ReadExact(((char *) &msg)+nTO, sz_rfbClientCutTextMsg-nTO))
			{
				// Allocate storage for the text
				const UINT length = Swap32IfLE(msg.cct.length);
				char *text = new char [length+1];
				if (text == NULL)
					break;
				text[length] = 0;

				// Read in the text
				if (!m_socket->ReadExact(text, length)) {
					delete [] text;
					break;
				}

				// Get the server to update the local clipboard
				m_server->UpdateLocalClipText(text);

				// Free the clip text we read
				delete [] text;
			}
			break;


		// Modif sf@2002 - Scaling
		// Server Scaling Message received
		case rfbPalmVNCSetScaleFactor:
			m_client->m_fPalmVNCScaling = true;
		case rfbSetScale: // Specific PalmVNC SetScaleFactor
			{
			// m_client->m_fPalmVNCScaling = false;
			// Read the rest of the message 
			if (m_client->m_fPalmVNCScaling)
			{
				if (!m_socket->ReadExact(((char *) &msg) + nTO, sz_rfbPalmVNCSetScaleFactorMsg - nTO))
				{
					connected = FALSE;
					break;
				}
			}
			else
			{
				if (!m_socket->ReadExact(((char *) &msg) + nTO, sz_rfbSetScaleMsg - nTO))
				{
					connected = FALSE;
					break;
				}
			}

			// Only accept reasonable scales...
			if (msg.ssc.scale < 1 || msg.ssc.scale > 9) break;

			m_client->m_nScale = msg.ssc.scale;
			if (!m_client->m_encodemgr.m_buffer->SetScale(msg.ssc.scale))
			{
				connected = FALSE;
				break;
			}

			m_client->fNewScale = true;
			InvalidateRect(NULL,NULL,TRUE);
			m_client->TriggerUpdateThread();

			}
			break;


		// Set Server Input
		case rfbSetServerInput:
			if (!m_socket->ReadExact(((char *) &msg) + nTO, sz_rfbSetServerInputMsg - nTO))
			{
				connected = FALSE;
				break;
			}
			if (msg.sim.status==1) m_client->m_encodemgr.m_buffer->m_desktop->SetDisableInput(true);
			if (msg.sim.status==0) m_client->m_encodemgr.m_buffer->m_desktop->SetDisableInput(false);
			break;


		// Set Single Window
		case rfbSetSW:
			if (!m_socket->ReadExact(((char *) &msg) + nTO, sz_rfbSetSWMsg - nTO))
			{
				connected = FALSE;
				break;
			}
			if (Swap16IfLE(msg.sw.x)==1 && Swap16IfLE(msg.sw.y)==1) 
			{
				m_client->m_encodemgr.m_buffer->m_desktop->SetSW(1,1);
				break;
			}
			m_client->m_encodemgr.m_buffer->m_desktop->SetSW(
				(Swap16IfLE(msg.sw.x) + m_client->m_SWOffsetx+m_client->m_ScreenOffsetx) * m_client->m_nScale,
				(Swap16IfLE(msg.sw.y) + m_client->m_SWOffsety+m_client->m_ScreenOffsety) * m_client->m_nScale);
			break;


		// Modif sf@2002 - TextChat
		case rfbTextChat:
			m_client->m_pTextChat->ProcessTextChatMsg(nTO);
			break;


		// Modif sf@2002 - FileTransfer
		// File Transfer Message
		case rfbFileTransfer:
			// Read the rest of the message:
			m_client->m_fFileTransferRunning = TRUE; 
			if (m_socket->ReadExact(((char *) &msg) + nTO, sz_rfbFileTransferMsg - nTO))
			{
				switch (msg.ft.contentType)
				{
					// A new file is received from the client
					// case rfbFileHeader:
					case rfbFileTransferOffer:
						{
						if (!m_client->m_server->FileTransferEnabled()) break;
						bool fError = false;
						const UINT length = Swap32IfLE(msg.ft.length);
						char *szFullDestName = new char [length + 1 + 32];
						if (szFullDestName == NULL) break;
						memset(szFullDestName, 0, length + 1 + 32);
						// Read in the Name of the file to create
						if (!m_socket->ReadExact(szFullDestName, length)) 
						{
							delete [] szFullDestName;
							break;
						}
						 
						// Parse the FileTime
						char szFileTime[18];
						char *p = strrchr(szFullDestName, ',');
						if (p == NULL)
							szFileTime[0] = '\0';
						else 
						{
							strcpy(szFileTime, p+1);
							*p = '\0';
						}

						// Create Local Dest file
						HANDLE  hDestFile = CreateFile(szFullDestName, 
														GENERIC_WRITE,
														FILE_SHARE_READ | FILE_SHARE_WRITE, 
														NULL,
														CREATE_ALWAYS, 
														FILE_FLAG_SEQUENTIAL_SCAN,
														NULL);

						DWORD dwDstSize = (DWORD)0; // Dummy size, actually a return value
						if (hDestFile == INVALID_HANDLE_VALUE)
							dwDstSize = 0xFFFFFFFF;
						else
							dwDstSize = 0x00;

						// Also check the free space on destination drive
						ULARGE_INTEGER lpFreeBytesAvailable;
						ULARGE_INTEGER lpTotalBytes;		
						ULARGE_INTEGER lpTotalFreeBytes;
						unsigned long dwFreeKBytes;
						char *szDestPath = new char [length + 1 + 32];
						if (szDestPath == NULL) break;
						memset(szDestPath, 0, length + 1 + 32);
						strcpy(szDestPath, szFullDestName);
						*strrchr(szDestPath, '\\') = '\0'; // We don't handle UNCs for now

						if (!GetDiskFreeSpaceEx((LPCTSTR)szDestPath,
												&lpFreeBytesAvailable,
												&lpTotalBytes,
												&lpTotalFreeBytes)) dwDstSize = 0xFFFFFFFF;
						delete [] szDestPath;
						dwFreeKBytes  = (unsigned long) (Int64ShraMod32(lpFreeBytesAvailable.QuadPart, 10));
						if (dwFreeKBytes < (Swap32IfLE(msg.ft.size) / 1000)) dwDstSize = 0xFFFFFFFF;

						rfbFileTransferMsg ft;
						
						ft.type = rfbFileTransfer;
						ft.contentType = rfbFileAcceptHeader;
						ft.size = Swap32IfLE(dwDstSize); // File Size in bytes, 0xFFFFFFFF (-1) means error
						ft.length = Swap32IfLE(strlen(szFullDestName));
						m_socket->SendExact((char *)&ft, sz_rfbFileTransferMsg, rfbFileTransfer);
						m_socket->SendExact((char *)szFullDestName, strlen(szFullDestName));

						if (dwDstSize == 0xFFFFFFFF)
						{
							delete [] szFullDestName;
							break;
						}

						// Todo : replace with more simple alloc
						GLOBALHANDLE hBuff = GlobalAlloc(GMEM_MOVEABLE, sz_rfbBlockSize + 1024);
						if (hBuff == NULL)
						{
							CloseHandle(hDestFile);
							delete [] szFullDestName;
							break;
						}
						LPVOID lpBuff = GlobalLock(hBuff);
						if (lpBuff == NULL)
						{
							CloseHandle(hDestFile);
							delete [] szFullDestName;
							break;
						}

						// Allocate buffer for DeCompression
						// Todo : make a global buffer with CheckBufferSize proc
						char *pCompBuff = new char [sz_rfbBlockSize];
						if (pCompBuff == NULL)
						{
							CloseHandle(hDestFile);
							delete [] szFullDestName;
							break;
						}

						DWORD dwFileSize = Swap32IfLE(msg.ft.size);
						DWORD dwNbPackets = (DWORD)(dwFileSize / sz_rfbBlockSize);
						DWORD dwNbReceivedPackets = 0;
						DWORD dwNbBytesWritten = 0;
						DWORD dwTotalNbBytesWritten = 0;
						bool fCompressed = true;

						// Copy Loop
						for (;;)
						{
							if (m_socket->ReadExact((char *) &msg, sz_rfbFileTransferMsg))
							{
								if (msg.ft.contentType == rfbFilePacket)
								{
									if (m_socket->ReadExact((char *)lpBuff, Swap32IfLE(msg.ft.length)))
									{
										if (Swap32IfLE(msg.ft.size) == 0) fCompressed = false;
										unsigned int nRawBytes = sz_rfbBlockSize;
										
										if (fCompressed)
										{
											// Decompress incoming data
											int nRet = uncompress(	(unsigned char*)pCompBuff,	// Dest 
																	(unsigned long *)&nRawBytes, // Dest len
																	(const unsigned char*)lpBuff,// Src
																	Swap32IfLE(msg.ft.length)	// Src len
																	);							
											if (nRet != 0)
											{
												fError = true;
												break;
											}
										}

										BOOL fRes = WriteFile(hDestFile,
																fCompressed ? pCompBuff : lpBuff,
																fCompressed ? nRawBytes : Swap32IfLE(msg.ft.length),
																&dwNbBytesWritten,
																NULL);
										if (!fRes)
										{
											// TODO : send an explicit error msg to the client...
											fError = true;
											break;
										}
										dwTotalNbBytesWritten += dwNbBytesWritten;
										dwNbReceivedPackets++;
									}

								}
								else if (msg.ft.contentType == rfbEndOfFile)
								{
									// TODO : check dwNbReceivedPackets and dwTotalNbBytesWritten or a checksum
									FlushFileBuffers(hDestFile);
									break;
								}
								else if (msg.ft.contentType == rfbAbortFileTransfer)
								{
									// TODO : Error report
									fError = true;
									FlushFileBuffers(hDestFile);
									break;
								}
							}
						}
						// Set the DestFile Time Stamp
						if (strlen(szFileTime))
						{
							FILETIME DestFileTime;
							SYSTEMTIME FileTime;
							FileTime.wMonth  = atoi(szFileTime);
							FileTime.wDay    = atoi(szFileTime + 3);
							FileTime.wYear   = atoi(szFileTime + 6);
							FileTime.wHour   = atoi(szFileTime + 11);
							FileTime.wMinute = atoi(szFileTime + 14);
							FileTime.wMilliseconds = 0;
							FileTime.wSecond = 0;
							SystemTimeToFileTime(&FileTime, &DestFileTime);
							// ToDo: hook error
							SetFileTime(hDestFile, &DestFileTime, &DestFileTime, &DestFileTime);
						}

						// CleanUp
						CloseHandle(hDestFile);
						if (fError) DeleteFile(szFullDestName);
						delete [] szFullDestName;
						if (pCompBuff != NULL)
							delete [] pCompBuff;
						GlobalUnlock(hBuff);
						GlobalFree(hBuff);
						}
						break;

					// The client requests a File
					case rfbFileTransferRequest:
						{
						if (!m_client->m_server->FileTransferEnabled()) break;
						bool fCompressionEnabled = (Swap32IfLE(msg.ft.size) == 1);
						const UINT length = Swap32IfLE(msg.ft.length);
						char *szSrcFileName = new char [length + 1 + 32];
						if (szSrcFileName == NULL) break;
						memset(szSrcFileName, 0, length + 1 + 32);
						// Read in the Name of the file to create
						if (!m_socket->ReadExact(szSrcFileName, length))
						{
							delete [] szSrcFileName;
							break;
						}
						
						// Open source file
						HANDLE hSrcFile = CreateFile(
											szSrcFileName,		
											GENERIC_READ,		
											FILE_SHARE_READ,	
											NULL,				
											OPEN_EXISTING,		
											FILE_FLAG_SEQUENTIAL_SCAN,	
											NULL);				
						
						DWORD dwSrcSize = (DWORD)0; 
						if (hSrcFile == INVALID_HANDLE_VALUE)
						{
							DWORD TheError = GetLastError();
							dwSrcSize = 0xFFFFFFFF;
						}
						else
						{	
							// Source file size 
							dwSrcSize = GetFileSize(hSrcFile, NULL); 
							if (dwSrcSize == 0xFFFFFFFF)
							{
								CloseHandle(hSrcFile);
							}
							else
							{
								// Add the File Time Stamp to the filename
								FILETIME SrcFileModifTime; 
								BOOL fRes = GetFileTime(hSrcFile, NULL, NULL, &SrcFileModifTime);
								if (fRes)
								{
									char szSrcFileTime[18];
									SYSTEMTIME FileTime;
									FileTimeToSystemTime(&SrcFileModifTime, &FileTime);
									wsprintf(szSrcFileTime,"%2.2d/%2.2d/%4.4d %2.2d:%2.2d",
											FileTime.wMonth,
											FileTime.wDay,
											FileTime.wYear,
											FileTime.wHour,
											FileTime.wMinute
											);
									strcat(szSrcFileName, ",");
									strcat(szSrcFileName, szSrcFileTime);
								}
							}
						}

						// Send the FileTransferMsg with rfbFileHeader
						rfbFileTransferMsg ft;
						
						ft.type = rfbFileTransfer;
						ft.contentType = rfbFileHeader;
						ft.size = Swap32IfLE(dwSrcSize); // File Size in bytes, 0xFFFFFFFF (-1) means error
						ft.length = Swap32IfLE(strlen(szSrcFileName));
						m_socket->SendExact((char *)&ft, sz_rfbFileTransferMsg, rfbFileTransfer);
						m_socket->SendExact((char *)szSrcFileName, strlen(szSrcFileName));
						
						delete [] szSrcFileName;
						if (dwSrcSize == 0xFFFFFFFF) break; // If error, we don't send anything else
						// Check if the file has been created on client side
						if (m_socket->ReadExact((char *) &ft, sz_rfbFileTransferMsg))
							if (ft.contentType == rfbFileHeader)
								if (Swap32IfLE(ft.size) == -1)
								{
									CloseHandle(hSrcFile);
									break;
								}

						GLOBALHANDLE hBuff = GlobalAlloc(GMEM_MOVEABLE, sz_rfbBlockSize);
						if (hBuff == NULL)
						{
							CloseHandle(hSrcFile);
							break;
						}
						LPVOID lpBuff = GlobalLock(hBuff);
						if (lpBuff == NULL)
						{
							CloseHandle(hSrcFile);
							break;
						}
						
						// Allocate buffer for compression
						// Todo : make a global buffer with CheckBufferSize proc
						char *pCompBuff = new char [sz_rfbBlockSize + 1024]; // TODO: Improve this
						if (pCompBuff == NULL)
						{
							CloseHandle(hSrcFile);
							break;
						}

						// Copy Loop
						// int nRet = 1;
						bool fError = false;
						BOOL fEof = FALSE;
						DWORD dwNbBytesRead = 0;
						DWORD dwTotalNbBytesWritten = 0;
						int nCount = 0;
						bool fCompress = fCompressionEnabled;
						
						while ( !fEof )
						{
							int nRes = ReadFile(hSrcFile, lpBuff, sz_rfbBlockSize, &dwNbBytesRead, NULL);
							if (!nRes)
							{
								// nRet = 0;
								fError = true;
								break;
							}
							
							if (dwNbBytesRead == 0)
							{
								fEof = TRUE;
							}
							else
							{
								// Compress the data
								// (Compressed data can be longer if it was already compressed)
								unsigned int nMaxCompSize = sz_rfbBlockSize + 1024; // TODO: Improve this...
								if (fCompress)
								{
									int nRetC = compress((unsigned char*)(pCompBuff),
														(unsigned long *)&nMaxCompSize,	
														(unsigned char*)lpBuff,
														dwNbBytesRead
														);

									if (nRetC != 0)
									{
										vnclog.Print(LL_INTINFO, VNCLOG("Compress returned error in File Send :%d\n"), nRetC);
										// Todo: send data uncompressed instead
										// nRet = 0;
										fError = true;
										break;
									}
								}

								// Test if we have to deal with already compressed data
								if (nMaxCompSize > dwNbBytesRead)
									fCompress = false;

								ft.type = rfbFileTransfer;
								ft.contentType = rfbFilePacket;
								ft.size = fCompress ? Swap32IfLE(1) : Swap32IfLE(0); 
								ft.length = fCompress ? Swap32IfLE(nMaxCompSize) : Swap32IfLE(dwNbBytesRead);
								m_socket->SendExact((char *)&ft, sz_rfbFileTransferMsg);
								if (fCompress)
									m_socket->SendExact((char *)pCompBuff , nMaxCompSize);
								else
									m_socket->SendExact((char *)lpBuff , dwNbBytesRead);
								
								dwTotalNbBytesWritten += dwNbBytesRead;
								// TODO : test on nb of bytes written
							}
							// Every 10 buffers, ask the client if the transfer must continue
							nCount++;
							if (nCount > 10 && !fEof)
							{
								// TODO: Add a timout or something 
								if (m_socket->ReadExact((char *) &msg, sz_rfbFileTransferMsg))
									if (msg.ft.contentType == rfbFilePacket)
										if (Swap32IfLE(msg.ft.size) == -1) 
										{
											// nRet = 0;
											fError = true;
											break;
										}
								nCount = 0;
							}
						}
						
						// File Copy OK
						if ( !fError /*nRet == 1*/)
						{
							ft.type = rfbFileTransfer;
							ft.contentType = rfbEndOfFile;
							m_socket->SendExact((char *)&ft, sz_rfbFileTransferMsg);
						}
						else // Error in file copy
						{
							// TODO : send an error msg to the client...
							ft.type = rfbFileTransfer;
							ft.contentType = rfbAbortFileTransfer;
							m_socket->SendExact((char *)&ft, sz_rfbFileTransferMsg);
						}
						
						CloseHandle(hSrcFile);
						GlobalUnlock(hBuff);
						GlobalFree(hBuff);
						if (pCompBuff != NULL)
							delete [] pCompBuff;
						}
						break;

					// Should never be handled here but within a file transfer loop
					// (after a rfbFileHeader Msg reception)
					case rfbFilePacket:
						if (!m_client->m_server->FileTransferEnabled()) break;
						break;

					// Should never be handled here but within a file transfer loop
					// (after a rfbFileHeader Msg reception)
					case rfbEndOfFile:
						if (!m_client->m_server->FileTransferEnabled()) break;
						break;

					// We use this message for FileTransfer rights
					// The client asks for FileTransfer permission
					case rfbAbortFileTransfer:
						m_client->fFTRequest = true;

						// sf@2002 - DO IT HERE FOR THE MOMENT 
						// FileTransfer permission requested by the client
						if (m_client->fFTRequest)
						{
							rfbFileTransferMsg ft;
							ft.type = rfbFileTransfer;
							ft.contentType = rfbAbortFileTransfer;

							// Ask the client to cancel the transfer
							if (m_client->m_server->FileTransferEnabled())
							   ft.size = Swap32IfLE(1); 
							else // Tell the client it's Ok
							   ft.size = Swap32IfLE(-1); 
							m_client->m_socket->SendExact((char *)&ft, sz_rfbFileTransferMsg, rfbFileTransfer);
							m_client->fFTRequest = false;
						}

						break;

					// The client requests the content of a directory or Drives List
					case rfbDirContentRequest:
						if (!m_client->m_server->FileTransferEnabled()) break;
						switch (msg.ft.contentParam)
						{
							// Client requests the List of Local Drives
							case rfbRDrivesList:
								{
								TCHAR szDrivesList[256]; // Format when filled : "C:\<NULL>D:\<NULL>....Z:\<NULL><NULL>
								DWORD dwLen;
								int nIndex = 0;
								int nType = 0;
								TCHAR szDrive[4];
								dwLen = GetLogicalDriveStrings(256, szDrivesList);

								// We add Drives types to this drive list...
								while (nIndex < dwLen - 3)
								{
									strcpy(szDrive, szDrivesList + nIndex);
									// We replace the "\" char following the drive letter and ":"
									// with a char corresponding to the type of drive
									// We obtain something like "C:l<NULL>D:c<NULL>....Z:n\<NULL><NULL>"
									// Isn't it ugly ?
									nType = GetDriveType(szDrive);
									switch (nType)
									{
									case DRIVE_FIXED:
										szDrivesList[nIndex + 2] = 'l';
										break;
									case DRIVE_REMOVABLE:
										szDrivesList[nIndex + 2] = 'f';
										break;
									case DRIVE_CDROM:
										szDrivesList[nIndex + 2] = 'c';
										break;
									case DRIVE_REMOTE:
										szDrivesList[nIndex + 2] = 'n';
										break;
									}
									nIndex += 4;
								}

								rfbFileTransferMsg ft;
								ft.type = rfbFileTransfer;
								ft.contentType = rfbDirPacket;
								ft.contentParam = rfbADrivesList;
								ft.length = Swap32IfLE((int)dwLen);
								m_socket->SendExact((char *)&ft, sz_rfbFileTransferMsg, rfbFileTransfer);
								m_socket->SendExact((char *)szDrivesList, (int)dwLen);
								}
								break;

							// Client requests the content of a directory
							case rfbRDirContent:
								{
									const UINT length = Swap32IfLE(msg.ft.length);
									char szDir[MAX_PATH];

									// Read in the Name of Dir to explore
									if (!m_socket->ReadExact(szDir, length)) break;
									szDir[length] = 0;
									strcat(szDir, "*");

									WIN32_FIND_DATA fd;
									HANDLE ff;
									BOOL fRet = TRUE;

									rfbFileTransferMsg ft;
									ft.type = rfbFileTransfer;
									ft.contentType = rfbDirPacket;
									ft.contentParam = rfbADirectory; // or rfbAFile...

									SetErrorMode(SEM_FAILCRITICALERRORS); // No popup please !
									ff = FindFirstFile(szDir, &fd);
									SetErrorMode( 0 );
									
									// Case of media not accessible
									if (ff == INVALID_HANDLE_VALUE)
									{
										ft.length = Swap32IfLE(0);										
										m_socket->SendExact((char *)&ft, sz_rfbFileTransferMsg, rfbFileTransfer);
										break;
									}

									ft.length = Swap32IfLE(1);										
									m_socket->SendExact((char *)&ft, sz_rfbFileTransferMsg, rfbFileTransfer);

									while ( fRet )
									{
										if (((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY && strcmp(fd.cFileName, "."))
											||
											(!strcmp(fd.cFileName, "..")))
										{
											// Serialize the interesting part of WIN32_FIND_DATA
											char szFileSpec[sizeof(WIN32_FIND_DATA)];
											int nOptLen = sizeof(WIN32_FIND_DATA) - MAX_PATH - 14 + lstrlen(fd.cFileName);
											memcpy(szFileSpec, &fd, nOptLen);

											ft.length = Swap32IfLE(nOptLen);
											m_socket->SendExact((char *)&ft, sz_rfbFileTransferMsg);
											m_socket->SendExact((char *)szFileSpec, nOptLen);
										}
										else if (strcmp(fd.cFileName, "."))
										{
											// Serialize the interesting part of WIN32_FIND_DATA
											// Get rid of the trailing blanck chars. It makes a BIG
											// difference when there's a lot of files in the dir.
											char szFileSpec[sizeof(WIN32_FIND_DATA)];
											int nOptLen = sizeof(WIN32_FIND_DATA) - MAX_PATH - 14 + lstrlen(fd.cFileName);
											memcpy(szFileSpec, &fd, nOptLen);

											ft.length = Swap32IfLE(nOptLen);
											m_socket->SendExact((char *)&ft, sz_rfbFileTransferMsg);
											m_socket->SendExact((char *)szFileSpec, nOptLen);

										}
										fRet = FindNextFile(ff, &fd);
									}
									FindClose(ff);
									
									// End of the transfer
									ft.contentParam = 0;
									ft.length = Swap32IfLE(0);
									m_socket->SendExact((char *)&ft, sz_rfbFileTransferMsg);
								}
								break;
						}
						break;

					// The client sends a command
					case rfbCommand:
						if (!m_client->m_server->FileTransferEnabled()) break;
						switch (msg.ft.contentParam)
						{
							// Client requests the creation of a directory
							case rfbCDirCreate:
								{
									const UINT length = Swap32IfLE(msg.ft.length);
									char szDir[MAX_PATH];

									// Read in the Name of Dir to explore
									if (!m_socket->ReadExact(szDir, length))
									{
										// todo: manage error !
										break;
									}
									szDir[length] = 0;

									// Create the Dir
									BOOL fRet = CreateDirectory(szDir, NULL);

									rfbFileTransferMsg ft;
									ft.type = rfbFileTransfer;
									ft.contentType = rfbCommandReturn;
									ft.contentParam = rfbADirCreate;
									ft.size = fRet ? 0 : -1;
									ft.length = msg.ft.length;

									m_socket->SendExact((char *)&ft, sz_rfbFileTransferMsg, rfbFileTransfer);
									m_socket->SendExact((char *)szDir, (int)length);
								}
								break;

							// Client requests the deletion of a file
							case rfbCFileDelete:
								{
									const UINT length = Swap32IfLE(msg.ft.length);
									char szFile[MAX_PATH];

									// Read in the Name of the File 
									if (!m_socket->ReadExact(szFile, length))
									{
										// todo: manage error !
										break;
									}
									szFile[length] = 0;

									// Delete the file
									BOOL fRet = DeleteFile(szFile);

									rfbFileTransferMsg ft;
									ft.type = rfbFileTransfer;
									ft.contentType = rfbCommandReturn;
									ft.contentParam = rfbAFileDelete;
									ft.size = fRet ? 0 : -1;
									ft.length = msg.ft.length;

									m_socket->SendExact((char *)&ft, sz_rfbFileTransferMsg, rfbFileTransfer);
									m_socket->SendExact((char *)szFile, (int)length);
								}
								break;

						}  // End of swith
						break;

				} // End of switch

			} // End of if
			m_client->m_fFileTransferRunning = FALSE;
			break;

		default:
			// Unknown message, so fail!
			connected = FALSE;
		}
	}

	// Move into the thread's original desktop
	vncService::SelectHDESK(home_desktop);

	// Quit this thread.  This will automatically delete the thread and the
	// associated client.
	vnclog.Print(LL_CLIENTS, VNCLOG("client disconnected : %s (%hd)\n"),
									m_client->GetClientName(),
									m_client->GetClientId());
	//////////////////
	// LOG it also in the event
	//////////////////
	{
		typedef BOOL (*LogeventFn)(char *machine);
		LogeventFn Logevent = 0;
		char szCurrentDir[MAX_PATH];
		if (GetModuleFileName(NULL, szCurrentDir, MAX_PATH))
		{
			char* p = strrchr(szCurrentDir, '\\');
			*p = '\0';
			strcat (szCurrentDir,"\\auth.dll");
		}
		HMODULE hModule = LoadLibrary(szCurrentDir);
		if (hModule)
			{
				BOOL result=false;
				Logevent = (LogeventFn) GetProcAddress( hModule, "LOGEXIT" );
				Logevent((char *)m_client->GetClientName());
				FreeLibrary(hModule);
			}
	}


	// Disable the protocol to ensure that the update thread
	// is not accessing the desktop and buffer objects
	m_client->DisableProtocol();

	// Finally, it's safe to kill the update thread here
	if (m_client->m_updatethread) {
		m_client->m_updatethread->Kill();
		m_client->m_updatethread->join(NULL);
	}

	// Remove the client from the server
	// This may result in the desktop and buffer being destroyed
	// It also guarantees that the client will not be passed further
	// updates
	m_server->RemoveClient(m_client->GetClientId());

}

// The vncClient itself

vncClient::vncClient()
{
	vnclog.Print(LL_INTINFO, VNCLOG("Client() executing...\n"));

	m_socket = NULL;
	m_client_name = 0;

	// Initialise mouse fields
	m_mousemoved = FALSE;
	m_ptrevent.buttonMask = 0;
	m_ptrevent.x = 0;
	m_ptrevent.y=0;

	// Other misc flags
	m_thread = NULL;
	m_palettechanged = FALSE;

	// Initialise the two update stores
	m_updatethread = NULL;
	m_update_tracker.init(this);

	m_remoteevent = FALSE;

	m_clipboard_text = 0;

	// IMPORTANT: Initially, client is not protocol-ready.
	m_disable_protocol = 1;

	//SINGLE WINDOW
	m_use_NewSWSize = FALSE;
	m_SWOffsetx=0;
	m_SWOffsety=0;
	m_ScreenOffsetx=0;
	m_ScreenOffsety=0;

	// sf@2002 
	fNewScale = false;
	m_fPalmVNCScaling = false;
	fFTRequest = false;

	// Modif sf@2002 - FileTransfer
	m_fFileTransferRunning = FALSE;

	// CURSOR HANDLING
	m_cursor_update_pending = FALSE;
	m_cursor_update_sent = FALSE;

	//cachestats
	totalraw=0;

	m_pRawCacheZipBuf = NULL;
	m_nRawCacheZipBufSize = 0;
	m_pCacheZipBuf = NULL;
	m_nCacheZipBufSize = 0;

	// Modif sf@2002 - Text Chat
	m_pTextChat = new TextChat(this); 	
	m_fUltraViewer = true;
	m_IsLoopback=false;
	m_NewSWUpdateWaiting=false;

	// IIC
	m_SendYieldControlNeeded = false;
	m_sendCloseSession = false;

}

vncClient::~vncClient()
{
	vnclog.Print(LL_INTINFO, VNCLOG("~Client() executing...\n"));

	// Modif sf@2002 - Text Chat
	if (m_pTextChat) 
	{
		delete(m_pTextChat);
		m_pTextChat = NULL;
	}

	// We now know the thread is dead, so we can clean up
	if (m_client_name != 0) {
		free(m_client_name);
		m_client_name = 0;
	}

	// If we have a socket then kill it
	if (m_socket != NULL)
	{
		vnclog.Print(LL_INTINFO, VNCLOG("deleting socket\n"));

		delete m_socket;
		m_socket = NULL;
	}
	if (m_pRawCacheZipBuf != NULL)
		{
			delete [] m_pRawCacheZipBuf;
			m_pRawCacheZipBuf = NULL;
		}
	if (m_pCacheZipBuf != NULL)
		{
			delete [] m_pCacheZipBuf;
			m_pCacheZipBuf = NULL;
		}

	//thos give sometimes errors, hlogfile is already removed at this point
	//vnclog.Print(LL_INTINFO, VNCLOG("cached %d \n"),totalraw);
}

// Init
BOOL
vncClient::Init(vncServer *server,
				VSocket *socket,
				BOOL auth,
				BOOL shared,
				vncClientId newid)
{
	// Save the server id;
	m_server = server;

	// Save the socket
	m_socket = socket;

	// Save the name of the connecting client
	char *name = m_socket->GetPeerName();
	if (name != 0)
		m_client_name = strdup(name);
	else
		m_client_name = strdup("<unknown>");

	// Save the client id
	m_id = newid;
	
	m_hasControl = FALSE;	// IIC

	// Spawn the child thread here
	m_thread = new vncClientThread;
	if (m_thread == NULL)
		return FALSE;
	return ((vncClientThread *)m_thread)->Init(this, m_server, m_socket, auth, shared);

	return FALSE;
}

// IIC
void vncClient::YieldControl()
{
	omni_mutex_lock l(GetUpdateLock());

	m_hasControl = FALSE;
	m_SendYieldControlNeeded = TRUE;

	SendYieldControl();
}

void vncClient::SendYieldControl()
{
	if (m_SendYieldControlNeeded)
	{
		char msg[2] = {rfbGrantControl, 0};
		m_socket->SendExact(msg, 2);
		m_SendYieldControlNeeded = FALSE;
	}
}

void
vncClient::Kill()
{
	// Close the socket
	vnclog.Print(LL_INTERR, VNCLOG("client Kill() called"));
	if (m_socket != NULL)
		m_socket->Close();
}

// IIC
void
vncClient::SetCloseSession(int reason)
{
	m_sendCloseSession = true;

	DisableProtocol();

	rfbCloseSessionMsg msg;
	msg.reason = 0;
	SendRFBMsg(rfbCloseSession, (BYTE*)&msg, sizeof(msg));

	EnableProtocol();
}

// Client manipulation functions for use by the server
void
vncClient::SetBuffer(vncBuffer *buffer)
{
	// Until authenticated, the client object has no access
	// to the screen buffer.  This means that there only need
	// be a buffer when there's at least one authenticated client.
	m_encodemgr.SetBuffer(buffer);
}

void
vncClient::TriggerUpdateThread()
{
	// ALWAYS lock the client UpdateLock before calling this!
	// RealVNC 336	
	// Check that this client has an update thread
	// The update thread never dissappears, and this is the only
	// thread allowed to create it, so this can be done without locking.
	
	if (!m_updatethread)
	{
		m_updatethread = new vncClientUpdateThread;
		if (!m_updatethread || 
			!m_updatethread->Init(this)) {
			Kill();
		}
	}				
	if (m_updatethread)
		m_updatethread->Trigger();
}

void
vncClient::UpdateMouse()
{
	if (!m_mousemoved && !m_cursor_update_sent)
	{
	omni_mutex_lock l(GetUpdateLock());
    m_mousemoved=TRUE;
	}
}

void
vncClient::UpdateClipText(const char* text)
{
	omni_mutex_lock l(GetUpdateLock());
	if (m_clipboard_text) {
		free(m_clipboard_text);
		m_clipboard_text = 0;
	}
	m_clipboard_text = strdup(text);
	TriggerUpdateThread();
}

void
vncClient::UpdatePalette()
{
	omni_mutex_lock l(GetUpdateLock());
	m_palettechanged = TRUE;
}

void
vncClient::UpdateLocalFormat()
{
	DisableProtocol();
	vnclog.Print(LL_INTERR, VNCLOG("updating local pixel format\n"));
	m_encodemgr.SetServerFormat();
	EnableProtocol();
}

// Functions used to set and retrieve the client settings
const char*
vncClient::GetClientName()
{
	return m_client_name;
}

// Enabling and disabling clipboard/GFX updates
void
vncClient::DisableProtocol()
{
	BOOL disable = FALSE;
	{	omni_mutex_lock l(GetUpdateLock());
		if (m_disable_protocol == 0)
			disable = TRUE;
		m_disable_protocol++;
		if (disable && m_updatethread)
			m_updatethread->EnableUpdates(FALSE);
	}
}

void
vncClient::EnableProtocol()
{
	{	omni_mutex_lock l(GetUpdateLock());
		if (m_disable_protocol == 0) {
			vnclog.Print(LL_INTERR, VNCLOG("protocol enabled too many times!\n"));
			m_socket->Close();
			return;
		}
		m_disable_protocol--;
		if ((m_disable_protocol == 0) && m_updatethread)
			m_updatethread->EnableUpdates(TRUE);
	}
}

// Internal methods
BOOL
vncClient::SendRFBMsg(CARD8 type, BYTE *buffer, int buflen)
{
	// Set the message type
	((rfbServerToClientMsg *)buffer)->type = type;

	// Send the message
	if (!m_socket->SendExact((char *) buffer, buflen, type))
	{
		vnclog.Print(LL_CONNERR, VNCLOG("failed to send RFB message to client\n"));

		Kill();
		return FALSE;
	}
	return TRUE;
}


BOOL
vncClient::SendUpdate(rfb::SimpleUpdateTracker &update)
{
	// If there is nothing to send then exit
	if (update.is_empty() /*&& !m_mousemoved RealVNC 336*/ && m_cursor_update_pending)
		return FALSE;

	debugf("Sending Screen Updates\n");

	// Get the update info from the tracker
	rfb::UpdateInfo update_info;
	update.get_update(update_info);
	update.clear();

	// Find out how many rectangles in total will be updated
	// This includes copyrects and changed rectangles split
	// up by codings such as CoRRE.

	int updates = 0;
	int numsubrects = 0;
	updates += update_info.copied.size();
	if (m_encodemgr.IsCacheEnabled())
	{
		if (update_info.cached.size() > 5)
		{
			updates++;
		}
		else updates += update_info.cached.size();
	}
	
	rfb::RectVector::const_iterator i;
	if (updates!= 0xFFFF)
	{
		for ( i=update_info.changed.begin(); i != update_info.changed.end(); i++)
		{
			// Tight specific (lastrect)
			numsubrects = m_encodemgr.GetNumCodedRects(*i);
			
			// Skip rest rectangles if an encoder will use LastRect extension.
			if (numsubrects == 0) {
				updates = 0xFFFF;
				break;
			}
			updates += numsubrects;
		}
	}
	
	if (updates != 0xFFFF)
	{
		for (i=update_info.Text.begin(); i != update_info.Text.end(); i++)
		{
			numsubrects = m_encodemgr.GetNumCodedRects(*i);
			if (numsubrects == 0) {
				updates = 0xFFFF;
				break;
			}
			updates += numsubrects;
		}
	}

	if (updates != 0xFFFF)
	{
		for (i=update_info.Solid.begin(); i != update_info.Solid.end(); i++)
		{
			numsubrects = m_encodemgr.GetNumCodedRects(*i);
			// Skip rest rectangles if an encoder will use LastRect extension.
			if (numsubrects == 0) {
				updates = 0xFFFF;
				break;
			}
			updates += numsubrects;
		}
	}
	
	
	// if no cache is supported by the other viewer
	// We need to send the cache as a normal update
	if (!m_encodemgr.IsCacheEnabled() && updates!= 0xFFFF) 
	{
		for (i=update_info.cached.begin(); i != update_info.cached.end(); i++)
		{
			// Tight specific (lastrect)
			numsubrects = m_encodemgr.GetNumCodedRects(*i);
			
			// Skip rest rectangles if an encoder will use LastRect extension.
			if (numsubrects == 0) {
				updates = 0xFFFF;
				break;
			}	
			updates += numsubrects;
		}
	}
	
	
	// Tight specific (lastrect)
	if (updates != 0xFFFF)
	{
		// Tight - CURSOR HANDLING
		if (m_cursor_update_pending)
			updates++;
		// If there are no updates then return
		if (updates == 0) return FALSE;
	}

	// Otherwise, send <number of rectangles> header
	rfbFramebufferUpdateMsg header;
	header.nRects = Swap16IfLE(updates);
	if (!SendRFBMsg(rfbFramebufferUpdate, (BYTE *) &header, sz_rfbFramebufferUpdateMsg))
		return TRUE;
	

	// CURSOR HANDLING
	if (m_cursor_update_pending) {
		if (!SendCursorShapeUpdate())
			return FALSE;
	}
	
	// Send the copyrect rectangles
	if (!update_info.copied.empty()) {
		rfb::Point to_src_delta = update_info.copy_delta.negate();
		for (i=update_info.copied.begin(); i!=update_info.copied.end(); i++) {
			rfb::Point src = (*i).tl.translate(to_src_delta);
			if (!SendCopyRect(*i, src))
				return FALSE;
		}
	}
	
	if (m_encodemgr.IsCacheEnabled())
	{
		if (update_info.cached.size() > 5)
		{
			if (!SendCacheZip(update_info.cached))
				return FALSE;
		}
		else
		{
			if (!SendCacheRectangles(update_info.cached))
				return FALSE;
		}
	}
	else 
	{
		if (!SendRectangles(update_info.cached))
			return FALSE;
	}
	
	if (!SendRectangles(update_info.changed))
		return FALSE;
	
	
	if (!SendRectangles(update_info.Text))
		return FALSE;
	
	if (!SendRectangles(update_info.Solid))
		return FALSE;
	
	// Tight specific - Send LastRect marker if needed.
	if (updates == 0xFFFF)
	{
		m_encodemgr.LastRect(m_socket);
		if (!SendLastRect())
			return FALSE;
	}

	return TRUE;
}

// Send a set of rectangles
BOOL
vncClient::SendRectangles(const rfb::RectVector &rects)
{
	rfb::Rect rect;
	rfb::RectVector::const_iterator i;

	// Work through the list of rectangles, sending each one
	for (i=rects.begin();i!=rects.end();i++) {
		if (!SendRectangle(*i))
			return FALSE;
	}

	return TRUE;
}

// Tell the encoder to send a single rectangle
BOOL
vncClient::SendRectangle(const rfb::Rect &rect)
{
	// Get the buffer to encode the rectangle
	// Modif sf@2002 - Scaling
	rfb::Rect ScaledRect;
	ScaledRect.tl.y = rect.tl.y / m_nScale;
	ScaledRect.br.y = rect.br.y / m_nScale;
	ScaledRect.tl.x = rect.tl.x / m_nScale;
	ScaledRect.br.x = rect.br.x / m_nScale;

	// IIC - translate from desktop to bitmap
	ScaledRect = ScaledRect.translate(m_encodemgr.m_buffer->m_desktop->GetBitmapOffset());

	debugf("Sending rectangle %dx%d at (%d, %d)\n", ScaledRect.br.x-ScaledRect.tl.x, ScaledRect.br.y-ScaledRect.tl.y, ScaledRect.tl.x, ScaledRect.tl.y);

	// sf@2002 - DSMPlugin
	// Some encoders (Hextile, ZRLE, Raw..) store all the data to send into 
	// m_clientbuffer and return the total size from EncodeRect()
	// Some Encoders (Tight, Zlib, ZlibHex..) send data on the fly and return
	// a partial size from EncodeRect(). 
	// On the viewer side, the data is read piece by piece or in one shot 
	// still depending on the encoding...
	// It is not compatible with DSM: we need to read/write data blocks of same 
	// size on both sides in one shot
	// We create a common method to send the data 
	if (m_socket->IsUsePluginEnabled() && m_server->GetDSMPluginPointer()->IsEnabled())
	{
		// Tell the SendExact() calls to write into the local NetRectBuffer memory buffer
		m_socket->SetWriteToNetRectBuffer(true);
		m_socket->SetNetRectBufOffset(0);
		m_socket->CheckNetRectBufferSize(m_encodemgr.GetClientBuffSize()); 
		UINT bytes = m_encodemgr.EncodeRect(ScaledRect, m_socket);
		m_socket->SetWriteToNetRectBuffer(false);

		BYTE* pDataBuffer = NULL;
		UINT TheSize = 0;

		// If SendExact() was called from inside the encoder
		if (m_socket->GetNetRectBufOffset() > 0)
		{
			TheSize = m_socket->GetNetRectBufOffset();
			m_socket->SetNetRectBufOffset(0);
			pDataBuffer = m_socket->GetNetRectBuf();
			// Add the rest to the data buffer if it exists
			if (bytes > 0)
			{
				memcpy(pDataBuffer + TheSize, m_encodemgr.GetClientBuffer(), bytes);
			}
		}
		else // If all data was stored in m_clientbuffer
		{
			TheSize = bytes;
			bytes = 0;
			pDataBuffer = m_encodemgr.GetClientBuffer();
		}

		// Send the header
		m_socket->SendExact((char *)pDataBuffer, sz_rfbFramebufferUpdateRectHeader);

		// Send the size of the following rects data buffer
		CARD32 Size = (CARD32)(TheSize + bytes - sz_rfbFramebufferUpdateRectHeader);
		CARD32 lSize = Swap32IfLE(Size);
		m_socket->SendExact((char*)&lSize, sizeof(CARD32));

#ifdef BLOWFISH
		m_encodemgr.SendEncrypted(m_socket, (pDataBuffer + sz_rfbFramebufferUpdateRectHeader), Size);
#else
		// Send the data buffer
		m_socket->SendExact(((char *)pDataBuffer + sz_rfbFramebufferUpdateRectHeader), Size);
#endif
	}
	else // Normal case - No DSM - Symetry is not important
	{
		UINT bytes = m_encodemgr.EncodeRect(ScaledRect, m_socket);

#ifdef BLOWFISH
		m_encodemgr.SendEncrypted(m_socket, (BYTE *)(m_encodemgr.GetClientBuffer()), bytes);
#else
		// if (bytes == 0) return false; // From realvnc337. No! Causes viewer disconnections/

		// Send the encoded data
		return m_socket->SendExact((char *)(m_encodemgr.GetClientBuffer()), bytes);
#endif
	}

	return true;
}

// Send a single CopyRect message
BOOL
vncClient::SendCopyRect(const rfb::Rect &dest, const rfb::Point &source)
{
	// Create the message header
	// Modif sf@2002 - Scaling
	rfbFramebufferUpdateRectHeader copyrecthdr;
	copyrecthdr.r.x = Swap16IfLE((dest.tl.x - m_SWOffsetx) / m_nScale );
	copyrecthdr.r.y = Swap16IfLE((dest.tl.y - m_SWOffsety) / m_nScale);
	copyrecthdr.r.w = Swap16IfLE((dest.br.x - dest.tl.x) / m_nScale);
	copyrecthdr.r.h = Swap16IfLE((dest.br.y - dest.tl.y) / m_nScale);
	copyrecthdr.encoding = Swap32IfLE(rfbEncodingCopyRect);

	// Create the CopyRect-specific section
	rfbCopyRect copyrectbody;
	copyrectbody.srcX = Swap16IfLE((source.x- m_SWOffsetx) / m_nScale);
	copyrectbody.srcY = Swap16IfLE((source.y- m_SWOffsety) / m_nScale);

	// Now send the message;
	if (!m_socket->SendExact((char *)&copyrecthdr, sizeof(copyrecthdr)))
		return FALSE;
	if (!m_socket->SendExact((char *)&copyrectbody, sizeof(copyrectbody)))
		return FALSE;

	return TRUE;
}

// Send the encoder-generated palette to the client
// This function only returns FALSE if the SendExact fails - any other
// error is coped with internally...
BOOL
vncClient::SendPalette()
{
	rfbSetColourMapEntriesMsg setcmap;
	RGBQUAD *rgbquad;
	UINT ncolours = 256;

	// Reserve space for the colour data
	rgbquad = new RGBQUAD[ncolours];
	if (rgbquad == NULL)
		return TRUE;
					
	// Get the data
	if (!m_encodemgr.GetPalette(rgbquad, ncolours))
	{
		delete [] rgbquad;
		return TRUE;
	}

	// Compose the message
	setcmap.type = rfbSetColourMapEntries;
	setcmap.firstColour = Swap16IfLE(0);
	setcmap.nColours = Swap16IfLE(ncolours);

	if (!m_socket->SendExact((char *) &setcmap, sz_rfbSetColourMapEntriesMsg, rfbSetColourMapEntries))
	{
		delete [] rgbquad;
		return FALSE;
	}

	// Now send the actual colour data...
	for (int i=0; i<ncolours; i++)
	{
		struct _PIXELDATA {
			CARD16 r, g, b;
		} pixeldata;

		pixeldata.r = Swap16IfLE(((CARD16)rgbquad[i].rgbRed) << 8);
		pixeldata.g = Swap16IfLE(((CARD16)rgbquad[i].rgbGreen) << 8);
		pixeldata.b = Swap16IfLE(((CARD16)rgbquad[i].rgbBlue) << 8);

		if (!m_socket->SendExact((char *) &pixeldata, sizeof(pixeldata)))
		{
			delete [] rgbquad;
			return FALSE;
		}
	}

	// Delete the rgbquad data
	delete [] rgbquad;

	return TRUE;
}

BOOL
vncClient::SetNewSWSize(long w,long h,BOOL Desktop)
{
	omni_mutex_lock l(GetUpdateLock());

	m_update_tracker.clear();

	if (!m_use_NewSWSize) return TRUE;  // IIC -- this will kill the connection if return false
	m_NewSWhdr.r.x = 0;
	m_NewSWhdr.r.y = 0;
	m_NewSWhdr.r.w = Swap16IfLE(w);
	m_NewSWhdr.r.h = Swap16IfLE(h);
	m_NewSWhdr.encoding = Swap32IfLE(rfbEncodingNewFBSize);
	m_NewSWDesktop=Desktop;
	m_NewSWUpdateWaiting=true;

	SendNewSWSize();

	return TRUE;
}

BOOL
vncClient::SendNewSWSize()
{
	m_NewSWUpdateWaiting=false;
	if (!m_use_NewSWSize) TRUE;  // IIC
	rfbFramebufferUpdateMsg header;
	header.nRects = Swap16IfLE(1);
	if (!SendRFBMsg(rfbFramebufferUpdate, (BYTE *)&header,
						sz_rfbFramebufferUpdateMsg))
			return FALSE;

	// Now send the message;
		if (!m_socket->SendExact((char *)&m_NewSWhdr, sizeof(m_NewSWhdr)))
			return FALSE;
	if (m_NewSWDesktop) m_encodemgr.SetEncoding(0,TRUE);//0=dummy
	m_fullscreen = m_encodemgr.GetSize();
	// IIC
	// Reset encoder if full screen update is requested.  For tight
	// encoder, resets zlib dictionaries.
	m_encodemgr.ResetEncoder();
	debugf("Sent New SWSize.\r\n");
	return TRUE;
}

void
vncClient::SetSWOffset(int x,int y)
{
	//if (m_SWOffsetx!=x || m_SWOffsety!=y) m_encodemgr.m_buffer->ClearCache();
	m_SWOffsetx=x;
	m_SWOffsety=y;
	m_encodemgr.SetSWOffset(x,y);
}

void
vncClient::SetScreenOffset(int x,int y)
{
	m_ScreenOffsetx=x;
	m_ScreenOffsety=y;
}

// CACHE RDV
// Send a set of rectangles
BOOL
vncClient::SendCacheRectangles(const rfb::RectVector &rects)
{
	rfb::Rect rect;
	rfb::RectVector::const_iterator i;

	if (rects.size() == 0) return TRUE;
	vnclog.Print(LL_INTINFO, VNCLOG("******** Sending %d Cache Rects \r\n"), rects.size());

	// Work through the list of rectangles, sending each one
	for (i= rects.begin();i != rects.end();i++)
	{
		if (!SendCacheRect(*i))
			return FALSE;
	}

	return TRUE;
}

// Tell the encoder to send a single rectangle
BOOL
vncClient::SendCacheRect(const rfb::Rect &dest)
{
	// Create the message header
	// Modif rdv@2002 - v1.1.x - Application Resize
	// Modif sf@2002 - Scaling
	rfbFramebufferUpdateRectHeader cacherecthdr;
	cacherecthdr.r.x = Swap16IfLE((dest.tl.x - m_SWOffsetx) / m_nScale );
	cacherecthdr.r.y = Swap16IfLE((dest.tl.y - m_SWOffsety) / m_nScale);
	cacherecthdr.r.w = Swap16IfLE((dest.br.x - dest.tl.x) / m_nScale);
	cacherecthdr.r.h = Swap16IfLE((dest.br.y - dest.tl.y) / m_nScale);
	cacherecthdr.encoding = Swap32IfLE(rfbEncodingCache);

	totalraw+=(dest.br.x - dest.tl.x)*(dest.br.y - dest.tl.y)*32 / 8; // 32bit test
	// Create the CopyRect-specific section
	rfbCacheRect cacherectbody;
	cacherectbody.special = Swap16IfLE(9999); //not used dummy

	// Now send the message;
	if (!m_socket->SendExact((char *)&cacherecthdr, sizeof(cacherecthdr)))
		return FALSE;
	if (!m_socket->SendExact((char *)&cacherectbody, sizeof(cacherectbody)))
		return FALSE;
	return TRUE;
}

BOOL
vncClient::SendCursorShapeUpdate()
{
	m_cursor_update_pending = FALSE;

	if (!m_encodemgr.SendCursorShape(m_socket)) {
		m_cursor_update_sent = FALSE;
		return m_encodemgr.SendEmptyCursorShape(m_socket);
	}

	m_cursor_update_sent = TRUE;
	return TRUE;
}

// Tight specific - Send LastRect marker indicating that there are no more rectangles to send
BOOL
vncClient::SendLastRect()
{
	// Create the message header
	rfbFramebufferUpdateRectHeader hdr;
	hdr.r.x = 0;
	hdr.r.y = 0;
	hdr.r.w = 0;
	hdr.r.h = 0;
	hdr.encoding = Swap32IfLE(rfbEncodingLastRect);

	// Now send the message;
	if (!m_socket->SendExact((char *)&hdr, sizeof(hdr)))
		return FALSE;

	return TRUE;
}


//
// sf@2002 - New cache rects transport - Uses Zlib
//
// 
BOOL vncClient::SendCacheZip(const rfb::RectVector &rects)
{
	//int totalCompDataLen = 0;

	int nNbCacheRects = rects.size();
	if (!nNbCacheRects) return true;
	unsigned long rawDataSize = nNbCacheRects * sz_rfbRectangle;
	unsigned long maxCompSize = (rawDataSize + (rawDataSize/100) + 8);

	// Check RawCacheZipBuff
	// create a space big enough for the Zlib encoded cache rects list
	if (m_nRawCacheZipBufSize < rawDataSize)
	{
		if (m_pRawCacheZipBuf != NULL)
		{
			delete [] m_pRawCacheZipBuf;
			m_pRawCacheZipBuf = NULL;
		}
		m_pRawCacheZipBuf = new BYTE [rawDataSize+1];
		if (m_pRawCacheZipBuf == NULL) 
			return false;
		m_nRawCacheZipBufSize = rawDataSize;
	}

	// Copy all the cache rects coordinates into the RawCacheZip Buffer 
	rfbRectangle theRect;
	rfb::RectVector::const_iterator i;
	BYTE* p = m_pRawCacheZipBuf;
	for (i = rects.begin();i != rects.end();i++)
	{
		theRect.x = Swap16IfLE(((*i).tl.x - m_SWOffsetx) / m_nScale );
		theRect.y = Swap16IfLE(((*i).tl.y - m_SWOffsety) / m_nScale);
		theRect.w = Swap16IfLE(((*i).br.x - (*i).tl.x) / m_nScale);
		theRect.h = Swap16IfLE(((*i).br.y - (*i).tl.y) / m_nScale);
		memcpy(p, (BYTE*)&theRect, sz_rfbRectangle);
		p += sz_rfbRectangle;
	}

	// Create a space big enough for the Zlib encoded cache rects list
	if (m_nCacheZipBufSize < maxCompSize)
	{
		if (m_pCacheZipBuf != NULL)
		{
			delete [] m_pCacheZipBuf;
			m_pCacheZipBuf = NULL;
		}
		m_pCacheZipBuf = new BYTE [maxCompSize+1];
		if (m_pCacheZipBuf == NULL) return 0;
		m_nCacheZipBufSize = maxCompSize;
	}

	int nRet = compress((unsigned char*)(m_pCacheZipBuf),
						(unsigned long*)&maxCompSize,
						(unsigned char*)m_pRawCacheZipBuf,
						rawDataSize
						);

	if (nRet != 0)
	{
		return false;
	}

	vnclog.Print(LL_INTINFO, VNCLOG("*** Sending CacheZip Rects=%d Size=%d (%d)\r\n"), nNbCacheRects, maxCompSize, nNbCacheRects * 14);

	// Send the Update Rect header
	rfbFramebufferUpdateRectHeader CacheRectsHeader;
	CacheRectsHeader.r.x = Swap16IfLE(nNbCacheRects);
	CacheRectsHeader.r.y = 0;
	CacheRectsHeader.r.w = 0;
 	CacheRectsHeader.r.h = 0;
	CacheRectsHeader.encoding = Swap32IfLE(rfbEncodingCacheZip);

	// Format the ZlibHeader
	rfbZlibHeader CacheZipHeader;
	CacheZipHeader.nBytes = Swap32IfLE(maxCompSize);

	// Now send the message
	if (!m_socket->SendExact((char *)&CacheRectsHeader, sizeof(CacheRectsHeader)))
		return FALSE;
	if (!m_socket->SendExact((char *)&CacheZipHeader, sizeof(CacheZipHeader)))
		return FALSE;
#ifdef BLOWFISH
	m_encodemgr.SendEncrypted(m_socket, m_pCacheZipBuf, maxCompSize);
#else
	if (!m_socket->SendExact((char *)m_pCacheZipBuf, maxCompSize))
		return FALSE;
#endif
	return TRUE;
}


//
//
//
void vncClient::EnableCache(BOOL enabled)
{
	m_encodemgr.EnableCache(enabled);
}

// IIC
rfb::Rect vncClient::GetScaledScreen()
{
	return m_ScaledScreen;
}

// IIC
void vncClient::EnableRecording(BOOL enable)
{
	omni_mutex_lock l(GetUpdateLock());

	m_enable_recording = enable;

	SendEnableRecording();
}

void vncClient::SendEnableRecording()
{
	if (m_socket)
	{
		debugf("Sending recording: %s\n", m_enable_recording ? "enable" : "disable");

		rfbRecordMsg msg;
		msg.type = rfbEnableRecording;
		msg.enable = m_enable_recording;
		msg.pad = 0;
		msg.start_time = Swap32IfLE(m_server->GetStartTime());
		m_socket->SendExact((char*)&msg, sz_rfbRecordMsg);
	}
}

void vncClient::SetNagleAlgorithm(BOOL enable)
{
	if (m_socket)
	{
		m_socket->SetNagleAlgorithm(enable);
	}
}

void vncClient::SendCmdMessage(const char * cmd)
{
	debugf("Sending command: %s\r\n", cmd);

	rfbCommandStrMsg msg;
	msg.type = rfbCommandStr;
	msg.msg_type = 0;
	msg.length = Swap32IfLE(strlen(cmd));
	m_socket->SendExact((char*)&msg, sz_rfbCommandStrMsg);
	m_socket->SendExact(cmd, strlen(cmd));
}
