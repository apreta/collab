//  Copyright (C) 2002 Ultr@VNC Team Members. All Rights Reserved.
//  Copyright (C) 2002 RealVNC Ltd. All Rights Reserved.
//  Copyright (C) 1999 AT&T Laboratories Cambridge. All Rights Reserved.
//
//  This file is part of the VNC system.
//
//  The VNC system is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// If the source code for the VNC system is not available from the place 
// whence you received this file, check http://www.uk.research.att.com/vnc or contact
// the authors on vnc@uk.research.att.com for information on obtaining it.


// vncMenu

// Implementation of a system tray icon & menu for WinVNC

#include "stdhdrs.h"
#include "WinVNC.h"
#include "vncService.h"
#include "vncConnDialog.h"
#include <lmcons.h>
#include <wininet.h>
#include <shlobj.h>
#include "../vdacctray/vncOSVersion.h"
#include "../../netlib/log.h"	// IIC
#include "directx.h" // IIC

// Header

#include "vncMenu.h"

extern "C" int net_set_next_hunt_port();	// IIC

// Constants
const UINT MENU_PROPERTIES_SHOW = RegisterWindowMessage("IICServer.Properties.User.Show");
const UINT MENU_DEFAULT_PROPERTIES_SHOW = RegisterWindowMessage("IICServer.Properties.Default.Show");
const UINT MENU_ABOUTBOX_SHOW = RegisterWindowMessage("IICServer.AboutBox.Show");
const UINT MENU_SERVICEHELPER_MSG = RegisterWindowMessage("IICServer.ServiceHelper.Message");
const UINT MENU_ADD_CLIENT_MSG = RegisterWindowMessage("IICServer.AddClient.Message");
const UINT MENU_REMOVE_CLIENTS_MSG = RegisterWindowMessage("IICServer.RemoveClients.Message"); // REalVNc 336
const char *MENU_CLASS_NAME = "IICServerTrayIcon";


BOOL g_restore_ActiveDesktop = FALSE;
BOOL g_restore_ui_effects = FALSE;
BOOL g_restore_DWM = FALSE;

HMODULE g_hDWMLib = NULL;

// From Platform SDK, 2000 and better only
#define SPI_GETUIEFFECTS                    0x103E
#define SPI_SETUIEFFECTS                    0x103F

static void
KillActiveDesktop()
{
  vnclog.Print(LL_INTERR, VNCLOG("KillActiveDesktop\n"));

  // Contact Active Desktop if possible
  HRESULT result;
  IActiveDesktop* active_desktop = 0;
  result = CoCreateInstance(CLSID_ActiveDesktop, NULL, CLSCTX_INPROC_SERVER,
    IID_IActiveDesktop, (void**)&active_desktop);
  if (result != S_OK) {
    vnclog.Print(LL_INTERR, VNCLOG("unable to access Active Desktop object:%x\n"), result);
    return;
  }

  // Get Active Desktop options
  COMPONENTSOPT options;
  options.dwSize = sizeof(options);
  result = active_desktop->GetDesktopItemOptions(&options, 0);
  if (result != S_OK) {
    vnclog.Print(LL_INTERR, VNCLOG("unable to fetch Active Desktop options:%x\n"), result);
    active_desktop->Release();
    return;
  }

  // Disable if currently active
  g_restore_ActiveDesktop = options.fActiveDesktop;
  if (options.fActiveDesktop) {
    vnclog.Print(LL_INTINFO, VNCLOG("attempting to disable Active Desktop\n"));
    options.fActiveDesktop = FALSE;
    result = active_desktop->SetDesktopItemOptions(&options, 0);
    if (result != S_OK) {
      vnclog.Print(LL_INTERR, VNCLOG("unable to disable Active Desktop:%x\n"), result);
      active_desktop->Release();
      return;
    }
  } else {
    vnclog.Print(LL_INTINFO, VNCLOG("Active Desktop not enabled - ignoring\n"));
  }

  active_desktop->ApplyChanges(AD_APPLY_REFRESH);
  active_desktop->Release();
}

static void
KillDWM()
{
	if (UsingDirectX())
		return;

	if (g_hDWMLib == NULL)
	{
		g_hDWMLib = LoadLibrary("dwmapi.dll");
	}
	
	if (g_hDWMLib != NULL)
	{
		HRESULT (__stdcall *pDwmEnableComposition)(BOOL fEnable);
		HRESULT (__stdcall *pDwmIsCompositionEnabled)(BOOL *pfEnabled);

		pDwmEnableComposition = (HRESULT (__stdcall *)(BOOL)) GetProcAddress(g_hDWMLib, "DwmEnableComposition");
		pDwmIsCompositionEnabled = (HRESULT (__stdcall *)(BOOL*)) GetProcAddress(g_hDWMLib, "DwmIsCompositionEnabled");

		if (pDwmEnableComposition && pDwmIsCompositionEnabled)
		{
			BOOL flag = FALSE;
			pDwmIsCompositionEnabled(&flag);
			if (flag)
			{
				pDwmEnableComposition(FALSE);
				g_restore_DWM = TRUE;
			}
		}
	}
}

static void
KillWallpaper()
{
  KillActiveDesktop();
  KillDWM();

	// Tell all applications that there is no wallpaper
	// Note that this doesn't change the wallpaper registry setting!
	SystemParametersInfo(SPI_SETDESKWALLPAPER, 0, "", SPIF_SENDCHANGE);

	// Take care of UI effects while we are here
	if (OSVersion() == 1)
	{
		SystemParametersInfo(SPI_GETUIEFFECTS, 0, &g_restore_ui_effects, 0);
		SystemParametersInfo(SPI_SETUIEFFECTS, 0, FALSE, SPIF_SENDCHANGE);
	}
}

static void
RestoreActiveDesktop()
{
  // Contact Active Desktop if possible
  HRESULT result;
  IActiveDesktop* active_desktop = 0;
  result = CoCreateInstance(CLSID_ActiveDesktop, NULL, CLSCTX_INPROC_SERVER,
    IID_IActiveDesktop, (void**)&active_desktop);
  if (result != S_OK) {
    vnclog.Print(LL_INTERR, VNCLOG("unable to access Active Desktop object:%x\n"), result);
    return;
  }

  // Get Active Desktop options
  COMPONENTSOPT options;
  options.dwSize = sizeof(options);
  result = active_desktop->GetDesktopItemOptions(&options, 0);
  if (result != S_OK) {
    vnclog.Print(LL_INTERR, VNCLOG("unable to fetch Active Desktop options:%x\n"), result);
    active_desktop->Release();
    return;
  }

  // Re-enable if previously disabled
  if (g_restore_ActiveDesktop) {
    g_restore_ActiveDesktop = FALSE;
    vnclog.Print(LL_INTINFO, VNCLOG("attempting to re-enable Active Desktop\n"));
    options.fActiveDesktop = TRUE;
    result = active_desktop->SetDesktopItemOptions(&options, 0);
    if (result != S_OK) {
      vnclog.Print(LL_INTERR, VNCLOG("unable to re-enable Active Desktop:%x\n"), result);
      active_desktop->Release();
      return;
    }
  }

  active_desktop->ApplyChanges(AD_APPLY_REFRESH);
  active_desktop->Release();
}

static void
RestoreDWM()
{
	if (UsingDirectX())
		return;

	if (g_hDWMLib != NULL)
	{
		HRESULT (__stdcall *pDwmEnableComposition)(BOOL fEnable);

		if (g_restore_DWM)
		{
			pDwmEnableComposition = (HRESULT (__stdcall *)(BOOL)) GetProcAddress(g_hDWMLib, "DwmEnableComposition");

			if (pDwmEnableComposition)
			{
				pDwmEnableComposition(TRUE);
				g_restore_DWM = FALSE;
			}
		}
	}
}

static void
RestoreWallpaper()
{
  RestoreActiveDesktop();
  RestoreDWM();

	SystemParametersInfo(SPI_SETDESKWALLPAPER, 0, NULL, SPIF_SENDCHANGE);

	// Turn UI effects back on
	if (OSVersion() == 1 && g_restore_ui_effects)
		SystemParametersInfo(SPI_SETUIEFFECTS, 0, (void*)TRUE, SPIF_SENDCHANGE);
}

// Implementation

vncMenu::vncMenu(vncServer *server)
{
    CoInitialize(0);

	// Allow us to receive WM_COPYDATA from low integrity processes (such as browser plugin)
	BOOL (__stdcall *pChangeWindowMessageFilter)(UINT, DWORD);
	HMODULE hModule = LoadLibrary("user32.dll");
	pChangeWindowMessageFilter = (BOOL (__stdcall *)(UINT, DWORD)) GetProcAddress(hModule, "ChangeWindowMessageFilter");
	if (pChangeWindowMessageFilter) {
		pChangeWindowMessageFilter(WM_COPYDATA, 1);
		pChangeWindowMessageFilter(WM_CLOSE, 1);
	}

	// Save the server pointer
	m_server = server;

	// Set the initial user name to something sensible...
	vncService::CurrentUser((char *)&m_username, sizeof(m_username));

	// Create a dummy window to handle tray icon messages
	WNDCLASSEX wndclass;

	wndclass.cbSize			= sizeof(wndclass);
	wndclass.style			= 0;
	wndclass.lpfnWndProc	= vncMenu::WndProc;
	wndclass.cbClsExtra		= 0;
	wndclass.cbWndExtra		= 0;
	wndclass.hInstance		= hAppInstance;
	wndclass.hIcon			= LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground	= (HBRUSH) GetStockObject(WHITE_BRUSH);
	wndclass.lpszMenuName	= (const char *) NULL;
	wndclass.lpszClassName	= MENU_CLASS_NAME;
	wndclass.hIconSm		= LoadIcon(NULL, IDI_APPLICATION);

	RegisterClassEx(&wndclass);

	m_hwnd = CreateWindow(MENU_CLASS_NAME,
				MENU_CLASS_NAME,
				WS_OVERLAPPEDWINDOW,
				CW_USEDEFAULT,
				CW_USEDEFAULT,
				200, 200,
				NULL,
				NULL,
				hAppInstance,
				NULL);
	if (m_hwnd == NULL)
	{
		PostQuitMessage(0);
		return;
	}

	// Only enable the timer if the tray icon will be displayed.
	if ( ! server->GetDisableTrayIcon())
	{
		// Timer to trigger icon updating
		SetTimer(m_hwnd, 1, 5000, NULL);
	}

	// record which client created this window
	SetWindowLong(m_hwnd, GWL_USERDATA, (LONG) this);

	// Ask the server object to notify us of stuff
	server->AddNotify(m_hwnd);

	// Initialise the properties dialog object
	if (!m_properties.Init(m_server))
	{
		PostQuitMessage(0);
		return;
	}

	// sf@2002
	if (!m_ListDlg.Init(m_server))
	{
		PostQuitMessage(0);
		return;
	}

	// Load the icons for the tray
	m_winvnc_icon = LoadIcon(hAppInstance, MAKEINTRESOURCE(IDI_WINVNC));
	m_flash_icon = LoadIcon(hAppInstance, MAKEINTRESOURCE(IDI_FLASH));

	// Load the popup menu
	m_hmenu = LoadMenu(hAppInstance, MAKEINTRESOURCE(IDR_TRAYMENU));

	// Install the tray icon!
	AddTrayIcon();

	// IIC
	m_hCallbackWnd = NULL;
	m_callbackMsg = 0;
	m_proxyAuthName[0] = '\0';
	m_proxyPassword[0] = '\0';
	m_bShutDown = false;
}

vncMenu::~vncMenu()
{
	// Remove the tray icon
	DelTrayIcon();
	
	// Destroy the loaded menu
	if (m_hmenu != NULL)
		DestroyMenu(m_hmenu);

	// Tell the server to stop notifying us!
	if (m_server != NULL)
		m_server->RemNotify(m_hwnd);
}

void
vncMenu::AddTrayIcon()
{
	// If the user name is non-null then we have a user!
	if (strcmp(m_username, "") != 0)
	{
		// Make sure the server has not been configured to
		// suppress the tray icon.
#ifndef EMBED // IIC
		if ( ! m_server->GetDisableTrayIcon())
#endif
		{
			SendTrayMsg(NIM_ADD, FALSE);
		}
	}
}

void
vncMenu::DelTrayIcon()
{
	SendTrayMsg(NIM_DELETE, FALSE);
}

void
vncMenu::FlashTrayIcon(BOOL flash)
{
	SendTrayMsg(NIM_MODIFY, flash);
}

// Get the local ip addresses as a human-readable string.
// If more than one, then with \n between them.
// If not available, then gets a message to that effect.
void GetIPAddrString(char *buffer, int buflen) {
    char namebuf[256];

    if (gethostname(namebuf, 256) != 0) {
		strncpy(buffer, "Host name unavailable", buflen);
		return;
    };

    HOSTENT *ph = gethostbyname(namebuf);
    if (!ph) {
		strncpy(buffer, "IP address unavailable", buflen);
		return;
    };

    *buffer = '\0';
    char digtxt[5];
    for (int i = 0; ph->h_addr_list[i]; i++) {
    	for (int j = 0; j < ph->h_length; j++) {
			sprintf(digtxt, "%d.", (unsigned char) ph->h_addr_list[i][j]);
			strncat(buffer, digtxt, (buflen-1)-strlen(buffer));
		}	
		buffer[strlen(buffer)-1] = '\0';
		if (ph->h_addr_list[i+1] != 0)
			strncat(buffer, ", ", (buflen-1)-strlen(buffer));
    }
}

void
vncMenu::SendTrayMsg(DWORD msg, BOOL flash)
{
	// Create the tray icon message
	m_nid.hWnd = m_hwnd;
	m_nid.cbSize = sizeof(m_nid);
	m_nid.uID = IDI_WINVNC;			// never changes after construction
	m_nid.hIcon = flash ? m_flash_icon : m_winvnc_icon;
	m_nid.uFlags = NIF_ICON | NIF_MESSAGE;
	m_nid.uCallbackMessage = WM_TRAYNOTIFY;
	//vnclog.Print(LL_INTINFO, VNCLOG("SendTRaymesg\n"));

#ifdef ORIG_VNC
	// Use resource string as tip if there is one
	if (LoadString(hAppInstance, IDI_WINVNC, m_nid.szTip, sizeof(m_nid.szTip)))
	{
	    m_nid.uFlags |= NIF_TIP;
	}

	// Try to add the server's IP addresses to the tip string, if possible
	if (m_nid.uFlags & NIF_TIP)
	{
	    strncat(m_nid.szTip, " - ", (sizeof(m_nid.szTip)-1)-strlen(m_nid.szTip));

	    if (m_server->SockConnected())
	    {
		unsigned long tiplen = strlen(m_nid.szTip);
		char *tipptr = ((char *)&m_nid.szTip) + tiplen;

		GetIPAddrString(tipptr, sizeof(m_nid.szTip) - tiplen);
	    }
	    else
	    {
		strncat(m_nid.szTip, "Not listening", (sizeof(m_nid.szTip)-1)-strlen(m_nid.szTip));
	    }
	}
#else
	// IIC
	strcpy(m_nid.szTip, TEXT("Desktop is shared"));
    m_nid.uFlags |= NIF_TIP;

#endif

	// Send the message
	if (Shell_NotifyIcon(msg, &m_nid))
	{
		// Set the enabled/disabled state of the menu items
//		vnclog.Print(LL_INTINFO, VNCLOG("tray icon added ok\n"));
		EnableMenuItem(m_hmenu, ID_PROPERTIES,
			m_properties.AllowProperties() ? MF_ENABLED : MF_GRAYED);
		EnableMenuItem(m_hmenu, ID_CLOSE,
			m_properties.AllowShutdown() ? MF_ENABLED : MF_GRAYED);
    EnableMenuItem(m_hmenu, ID_KILLCLIENTS,
			m_properties.AllowEditClients() ? MF_ENABLED : MF_GRAYED);
    EnableMenuItem(m_hmenu, ID_OUTGOING_CONN,
			m_properties.AllowEditClients() ? MF_ENABLED : MF_GRAYED);
	} 
/*	// IIC -- don't have to exit, or tell user about it
	else {
		if (!vncService::RunningAsService())
		{
			if (msg == NIM_ADD)
			{
				// The tray icon couldn't be created, so use the Properties dialog
				// as the main program window
				vnclog.Print(LL_INTINFO, VNCLOG("opening dialog box\n"));
#ifdef _DEBUG
				m_properties.Show(TRUE, TRUE);
#endif
				PostQuitMessage(0);
			}
		}
	}
*/
}


void vncMenu::connect_thread(void* pvoid)
{
	vncMenu *_this = (vncMenu*)pvoid;
	VSocket *tmpsock = NULL;

	try
	{
		while (tmpsock == NULL && !_this->m_bShutDown)
		{
			_this->SendCallbackMessage(CB_Connecting, 0);

			// Attempt to create a new socket
			tmpsock = new VSocket;
			if (tmpsock) {

				// Connect out to the specified host on the VNCviewer listen port
				tmpsock->Create();
				if (tmpsock->Connect(_this->m_shareServer, _this->m_sharePort, TRUE, 
									 _this->m_proxyAuthName, 
									 _this->m_proxyPassword)) {
					// Add the new client to this server
					_this->m_server->AddClient(tmpsock, TRUE, TRUE);
				} 
				else if (!_this->m_bShutDown)
				{
					delete tmpsock;
					tmpsock = NULL;
					Sleep(2000);
				}
			}
		}
	}
	catch (...)
	{
		// Hopefully rare circumstance of exception in connecting code:
		// make sure we don't get stuck on this port.
		net_set_next_hunt_port(); 

		_this->SendCallbackMessage(CB_InternalError, 0);
		PostQuitMessage(0);
	}
}

// Process window messages
LRESULT CALLBACK vncMenu::WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// This is a static method, so we don't know which instantiation we're 
	// dealing with. We use Allen Hadden's (ahadden@taratec.com) suggestion 
	// from a newsgroup to get the pseudo-this.
	vncMenu *_this = (vncMenu *) GetWindowLong(hwnd, GWL_USERDATA);
	static bool notificationSent = false;

	switch (iMsg)
	{

		// Every five seconds, a timer message causes the icon to update
	case WM_TIMER:
	    // *** HACK for running servicified
		if (vncService::RunningAsService()) {
		    // Attempt to add the icon if it's not already there
		    _this->AddTrayIcon();
		    // Trigger a check of the current user
		    PostMessage(hwnd, WM_USERCHANGED, 0, 0);
		}

		// Update the icon
		_this->FlashTrayIcon(_this->m_server->AuthClientCount() != 0);
		break;

		// DEAL WITH NOTIFICATIONS FROM THE SERVER:
	case WM_SRV_CLIENT_AUTHENTICATED:
	case WM_SRV_CLIENT_DISCONNECT:
		// IIC
		if (_this->m_hCallbackWnd != NULL)
		{
			switch (iMsg)
			{
				case WM_SRV_CLIENT_AUTHENTICATED:
					PostMessage(_this->m_hCallbackWnd, _this->m_callbackMsg, CB_Connected, 0L);
					break;
				case WM_SRV_CLIENT_DISCONNECT:
					PostMessage(_this->m_hCallbackWnd, _this->m_callbackMsg, CB_Disconnected, 0L);
					break;
			}
		}


		// Adjust the icon accordingly
		_this->FlashTrayIcon(_this->m_server->AuthClientCount() != 0);

		if (_this->m_server->AuthClientCount() != 0) {
			if (_this->m_server->RemoveWallpaperEnabled())
				KillWallpaper();
		} else {
			RestoreWallpaper();
		}

		// IIC - restart connection attempts internally
		if (_this->m_hCallbackWnd != NULL && iMsg == WM_SRV_CLIENT_DISCONNECT)
		{
			Sleep(1000);
			_beginthread(connect_thread, 0, _this);
		}

		return 0;

		// STANDARD MESSAGE HANDLING
	case WM_CREATE:
		return 0;

	case WM_COMMAND:
		// User has clicked an item on the tray menu
		switch (LOWORD(wParam))
		{
		case ID_ONLINEHELP:
			ShellExecute(GetDesktopWindow(), "open", "http://ultravnc.sourceforge.net/help.htm", "", 0, SW_SHOWNORMAL);
			break;
		case ID_HOME:
			ShellExecute(GetDesktopWindow(), "open", "http://ultravnc.sourceforge.net/index.html", "", 0, SW_SHOWNORMAL);
			break;

		case ID_DEFAULT_PROPERTIES:
			// Show the default properties dialog, unless it is already displayed
			vnclog.Print(LL_INTINFO, VNCLOG("show default properties requested\n"));
#ifdef _DEBUG
			_this->m_properties.Show(TRUE, FALSE);
#endif
			_this->FlashTrayIcon(_this->m_server->AuthClientCount() != 0);
			break;
		

		case ID_PROPERTIES:
			// Show the properties dialog, unless it is already displayed
			vnclog.Print(LL_INTINFO, VNCLOG("show user properties requested\n"));
#ifdef _DEBUG
			_this->m_properties.Show(TRUE, TRUE);
#endif
			_this->FlashTrayIcon(_this->m_server->AuthClientCount() != 0);
			break;
		
		case ID_OUTGOING_CONN:
			// Connect out to a listening VNC viewer
			{
				vncConnDialog *newconn = new vncConnDialog(_this->m_server);
				if (newconn)
				{
					newconn->DoDialog();
					// delete newconn; // NO ! Already done in vncConnDialog.
				}
			}
			break;

		case ID_KILLCLIENTS:
			// Disconnect all currently connected clients
			_this->m_server->KillAuthClients();
			break;

		// sf@2002
		case ID_LISTCLIENTS:
			_this->m_ListDlg.Display();
			break;

		case ID_ABOUT:
			// Show the About box
			_this->m_about.Show(TRUE);
			break;

		case ID_CLOSE:
			// User selected Close from the tray menu
			_this->m_bShutDown = true;	// IIC
			PostMessage(hwnd, WM_CLOSE, 0, 0);
			break;

		}
		return 0;

	case WM_SRV_SESSION_ENDED:
		if (!notificationSent)
		{
			notificationSent = true;
			PostMessage(_this->m_hCallbackWnd, _this->m_callbackMsg, CB_SessionEnded, 0L);
		}
//		PostMessage(hwnd, WM_CLOSE, 0, 0);
		return 0;

	case WM_TRAYNOTIFY:
		// User has clicked on the tray icon or the menu
		{
			// IIC - ignore tray icon menu
			if (_this->m_server->GetDisableTrayIcon())
				return 0;

			// Get the submenu to use as a pop-up menu
			HMENU submenu = GetSubMenu(_this->m_hmenu, 0);

			// What event are we responding to, RMB click?
			if (lParam==WM_RBUTTONUP)
			{
				if (submenu == NULL)
				{ 
					vnclog.Print(LL_INTERR, VNCLOG("no submenu available\n"));
					return 0;
				}

				// Make the first menu item the default (bold font)
				SetMenuDefaultItem(submenu, 0, TRUE);
				
				// Get the current cursor position, to display the menu at
				POINT mouse;
				GetCursorPos(&mouse);

				// There's a "bug"
				// (Microsoft calls it a feature) in Windows 95 that requires calling
				// SetForegroundWindow. To find out more, search for Q135788 in MSDN.
				//
				SetForegroundWindow(_this->m_nid.hWnd);

				// Display the menu at the desired position
				TrackPopupMenu(submenu,
						0, mouse.x, mouse.y, 0,
						_this->m_nid.hWnd, NULL);

				return 0;
			}
			
			// Or was there a LMB double click?
			if (lParam==WM_LBUTTONDBLCLK)
			{
				// double click: execute first menu item
				SendMessage(_this->m_nid.hWnd,
							WM_COMMAND, 
							GetMenuItemID(submenu, 0),
							0);
			}

			return 0;
		}
		
	case WM_CLOSE:
		_this->m_bShutDown = true;

#ifdef SHARE_LITE
		// IIC: If share lite, close connected client cleanly.
		if (_this->m_server->GetSessionToken() != NULL && *_this->m_server->GetSessionToken() != '\0') 
			// Send shutdown command if connection is open.
			_this->m_server->CloseAllClients();
#endif

		// Only accept WM_CLOSE if the logged on user has AllowShutdown set
		// tnatsni Wallpaper fix
		RestoreWallpaper();
		if (!_this->m_properties.AllowShutdown())
			return 0;
		break;
		
	case WM_DESTROY:
		_this->m_bShutDown = true;	// IIC

		// The user wants WinVNC to quit cleanly...
		vnclog.Print(LL_INTINFO, VNCLOG("quitting from WM_DESTROY\n"));
		PostQuitMessage(0);
		return 0;
		
	case WM_QUERYENDSESSION:
		vnclog.Print(LL_INTERR, VNCLOG("WM_QUERYENDSESSION\n"));
		break;
		
	case WM_ENDSESSION:
		vnclog.Print(LL_INTERR, VNCLOG("WM_ENDSESSION\n"));
		break;


/*
	case WM_ENDSESSION:
		// Are we running as a system service, or shutting the system down?
		if (wParam && (lParam == 0))
		{
			// Shutdown!
			// Try to clean ourselves up nicely, if possible...

			// Firstly, disable incoming CORBA or socket connections
			_this->m_server->SockConnect(FALSE);
			_this->m_server->CORBAConnect(FALSE);

			// Now kill all the server's clients
			_this->m_server->KillAuthClients();
			_this->m_server->KillUnauthClients();
			_this->m_server->WaitUntilAuthEmpty();
			_this->m_server->WaitUntilUnauthEmpty();
		}

		// Tell the OS that we've handled it anyway
		return 0;
		*/

	case WM_USERCHANGED:
		// The current user may have changed.
		{
			char newuser[UNLEN+1];

			if (vncService::CurrentUser((char *) &newuser, sizeof(newuser)))
			{
				vnclog.Print(LL_INTINFO,
//					VNCLOG("usernames : old=\"%s\", new=\"%s\"\n"),
					_this->m_username, newuser);

				// Check whether the user name has changed!
				if (strcmp(newuser, _this->m_username) != 0)
				{
					vnclog.Print(LL_INTINFO,
						VNCLOG("user name has changed\n"));

					// User has changed!
					strcpy(_this->m_username, newuser);

					// Redraw the tray icon and set it's state
					_this->DelTrayIcon();
					_this->AddTrayIcon();
					_this->FlashTrayIcon(_this->m_server->AuthClientCount() != 0);

					// We should load in the prefs for the new user
					_this->m_properties.Load(TRUE);
				}
			}
		}
		return 0;

	// IIC:  remote commands
	case WM_COPYDATA:
		{
			COPYDATASTRUCT * data = (COPYDATASTRUCT *)lParam;
			switch (data->dwData)
			{
			case 0: // Set Password
				char passwd[MAXPWLEN];
				memcpy(passwd, (char *)data->lpData, MAXPWLEN);
				_this->m_server->SetPassword(passwd);
				break;
			case 1: // Set Ports
			{
				struct PortInfo { UINT rfbPort; BOOL httpEnable; UINT httpPort; };

				PortInfo * ports = (PortInfo*)data->lpData;

				_this->m_server->SetAutoPortSelect(FALSE);
				_this->m_server->EnableXDMCPConnect(FALSE);
				_this->m_server->EnableHTTPConnect(ports->httpEnable);
				_this->m_server->SetPorts(ports->rfbPort, ports->httpPort);

				break;
			}
			case 2: // Set callback info
			{
				struct CallbackInfo { 
					  HWND callback; 
					  UINT msg; 
					  char proxyAuthName[64]; 
					  char proxyPassword[32]; 
					  char meetingId[65];
					  char sessionToken[128];
					  char participantId[128];
				};

				CallbackInfo * cbInfo = (CallbackInfo*)data->lpData;

				_this->m_hCallbackWnd = cbInfo->callback;
				_this->m_callbackMsg = cbInfo->msg;
				_this->m_proxyAuthName[0] = '\0';
				strncat(_this->m_proxyAuthName, cbInfo->proxyAuthName, sizeof(_this->m_proxyAuthName));
				_this->m_proxyPassword[0] = '\0';
				strncat(_this->m_proxyPassword, cbInfo->proxyPassword, sizeof(_this->m_proxyPassword));
				_this->m_server->SetMeetingId(cbInfo->meetingId);
				if (data->cbData >= sizeof(CallbackInfo))
				{
					_this->m_server->SetSessionToken(cbInfo->sessionToken);
					_this->m_server->SetParticipantId(cbInfo->participantId);
				}

				break;
			}
			case 3:	// share additional apps
				_this->m_server->AddToProcessIDList((char*)data->lpData);
				break;
			case 4:	// enable/disable transparent
				_this->m_server->EnableTransparent(data->cbData);
				break;
			case 5:	// Set PowerPoint window handle
			{
				struct Handles { HWND hPPT; HWND hMeeting; };

				Handles* pwnds = (Handles*)data->lpData;
				_this->m_server->SetPPTWindowHndl(pwnds->hPPT, pwnds->hMeeting);
				break;
			}
			case 6:	// Set color deduction
			{
				_this->m_server->SetReduceColor((BOOL)data->cbData);
				break;
			}
			case 7:	// Enable disable recording
			{
				_this->m_server->EnableRecording(data->cbData);
				break;
			}
            case 8: // share all instances
			{
				_this->m_server->ShareAllApplicationInstances(data->cbData);
				break;
			}
			case 9: // Add client by name
			{
				struct ClientInfo { USHORT port; char name[256]; char key[256]; };
				ClientInfo * client = (ClientInfo *)data->lpData;

				if (client->port == 0)
					client->port = INCOMING_PORT_OFFSET;

				_this->m_shareServer = strdup(client->name);
				_this->m_sharePort = client->port;
				_this->m_server->SetAppshareKey(client->key);

				_beginthread(connect_thread, 0, _this);
				break;
			}
			case 10: // Set Agent mode
			{
				_this->m_server->SetAgentMode(data->cbData);
				break;
			}
			case 11: // Set Debug mode
			{
				vnclog.SetLevel(data->cbData?LL_INTINFO:LL_INTERR);
// ToDo				set_debug_session(data->cbData);
				errorf("Detail log is %s for mtgshare.\r\n", (data->cbData?"On":"Off"));
				break;
			}
			case 12: // Set Nagle Algorithm
			{
				_this->m_server->SetNagleAlgorithm(data->cbData);
				break;
			}
			case 13: // Close
			{
				PostMessage(hwnd, WM_CLOSE, 0, 0);
			}
			}
		}
		return 0;
	
	default:
		// Deal with any of our custom message types
		if (iMsg == MENU_PROPERTIES_SHOW)
		{
			// External request to show our Properties dialog
			PostMessage(hwnd, WM_COMMAND, MAKELONG(ID_PROPERTIES, 0), 0);
			return 0;
		}
		if (iMsg == MENU_DEFAULT_PROPERTIES_SHOW)
		{
			// External request to show our Properties dialog
			PostMessage(hwnd, WM_COMMAND, MAKELONG(ID_DEFAULT_PROPERTIES, 0), 0);
			return 0;
		}
		if (iMsg == MENU_ABOUTBOX_SHOW)
		{
			// External request to show our About dialog
			PostMessage(hwnd, WM_COMMAND, MAKELONG(ID_ABOUT, 0), 0);
			return 0;
		}
		if (iMsg == MENU_SERVICEHELPER_MSG)
		{
			// External ServiceHelper message.
			// This message holds a process id which we can use to
			// impersonate a specific user.  In doing so, we can load their
			// preferences correctly
			vncService::ProcessUserHelperMessage(wParam, lParam);

			// - Trigger a check of the current user
			PostMessage(hwnd, WM_USERCHANGED, 0, 0);
			return 0;
		}
		if (iMsg == MENU_ADD_CLIENT_MSG)
		{
			// Add Client message.  This message includes an IP address
			// of a listening client, to which we should connect.

			// If there is no IP address then show the connection dialog
			if (!lParam) {
				vncConnDialog *newconn = new vncConnDialog(_this->m_server);
				if (newconn)
					{
						newconn->DoDialog();
						// winvnc -connect fixed
						//CHECH memeory leak
//						delete newconn;
					}
				return 0;
			}

			// Get the IP address stringified
			struct in_addr address;
			address.S_un.S_addr = lParam;
			char *name = inet_ntoa(address);
			if (name == 0)
				return 0;
			_this->m_shareServer = strdup(name);
			if (_this->m_shareServer == 0)
				return 0;

		    // Get the port number
			_this->m_sharePort = (unsigned short)wParam;
			if (_this->m_sharePort == 0)
				_this->m_sharePort = INCOMING_PORT_OFFSET;

			_beginthread(connect_thread, 0, _this);

            return 0;
		}
	}

	// Message not recognised
	return DefWindowProc(hwnd, iMsg, wParam, lParam);
}

// IIC - send message back to container window
void vncMenu::SendCallbackMessage(WPARAM wParam, LPARAM lParam)
{
	if (m_hCallbackWnd != NULL)
		PostMessage(m_hCallbackWnd, m_callbackMsg, wParam, lParam);
}

void vncMenu::DestroyWindow()
{
	::DestroyWindow(m_hwnd);
}
