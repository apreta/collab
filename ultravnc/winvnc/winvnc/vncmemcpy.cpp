/////////////////////////////////////////////////////////////////////////////
//  Copyright (C) 2002 Ultr@VNC Team Members. All Rights Reserved.
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// If the source code for the program is not available from the place from
// which you received this file, check 
// http://ultravnc.sourceforge.net/

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include "vncmemcpy.h"

Ultravncmemcpy::Ultravncmemcpy()
{
	cputype=false;
	Set_memcpu();
}

Ultravncmemcpy::~Ultravncmemcpy()
{
}

inline void
Ultravncmemcpy::Set_memcpu()
{
	if(get_feature_flags() & FEATURE_SSE) cputype=true;
}

bool
Ultravncmemcpy::Save_memcpy(void* dest,void* src,size_t count)
{
	if (src==NULL) return false;
	if (dest==NULL) return false;
	if(IsBadReadPtr(src,count)) 
		{ 
			return false;
		}
	if(IsBadWritePtr(dest,count)) 
		{ 
			return false;
		}
	if (cputype && count>64) memcpySSE(dest, src, count);
	else if (count>32) memcpyMMX(dest, src, count);
	else memcpy(dest, src, count);
	return true;
}

bool
Ultravncmemcpy::Save_memcmp(void* dest,void* src,size_t count)
{
	if (src==NULL) return false;
	if (dest==NULL) return false;
	if(IsBadReadPtr(src,count)) 
		{ 
			return false;
		}
	if(IsBadReadPtr(dest,count)) 
		{ 
			return false;
		}
	return memcmp(dest, src, count);
}

inline void 
Ultravncmemcpy::memcpyMMX(void* dest,void* src,DWORD count)
{
	DWORD	iCount;

	__asm
	{
		// Align Count to a DWORD Boundary
		MOV		ECX,count
		SHR		ECX,2
		SHL		ECX,2
		MOV		iCount,ECX

		// Copy All the DWORDs (32 bits at a Time)
		MOV		ESI,src		// Copy the Source Address to the Register
		MOV		EDI,dest	// Copy the Destination to the Register
		MOV		ECX,iCount	// Copy the Count to the Register
		SHR		ECX,2		// Divide Count by 4 for DWORD Copy
		REP		MOVSD		// Move all the Source DWORDs to the Dest DWORDs

		// Get the Remaining Bytes to Copy
		MOV		ECX,count
		MOV		EAX,iCount
		SUB		ECX,EAX

		// Exit if All Bytes Copied
		JZ		Exit

		// Copy the Remaining BYTEs (8 bits at a Time)
		MOV		ESI,src		// Copy the Source Address to the Register
		ADD		ESI,EAX		// Set the Starting Point of the Copy
		MOV		EDI,dest	// Copy the Destination to the Register
		ADD		EDI,EAX		// Set the Destination Point of the Copy
		REP		MOVSB		// Move all the Source BYTEs to the Dest BYTEs
		Exit:
	}
}

inline void 
Ultravncmemcpy::memcpySSE(void *dest, const void *src, size_t nbytes)
{
	_asm 
	{
			mov esi, src 
			mov edi, dest 
			mov ebx, nbytes 

			// edx takes number of bytes%64
			mov	edx, ebx
			and edx, 63

			// ebx takes number of bytes/64
			shr	ebx, 6
			jz	byteCopy


	loop4k: // flush 4k into temporary buffer 
			push esi 
			mov ecx, ebx
			// copy per block of 64 bytes. Must not override 64*64= 4096 bytes.
			cmp ecx, 64
			jle	skipMiniMize
			mov	ecx, 64
	skipMiniMize:
			// eax takes the number of 64bytes packet for this block.
			mov eax, ecx

	loopMemToL1: 
			prefetchnta 64[ESI] // Prefetch next loop, non-temporal 
			prefetchnta 96[ESI] 

			movq mm1,  0[ESI] // Read in source data 
			movq mm2,  8[ESI] 
			movq mm3, 16[ESI] 
			movq mm4, 24[ESI] 
			movq mm5, 32[ESI] 
			movq mm6, 40[ESI] 
			movq mm7, 48[ESI] 
			movq mm0, 56[ESI] 

			add esi, 64 
			dec ecx 
			jnz loopMemToL1 

			pop esi // Now copy from L1 to system memory 
			mov ecx, eax

	loopL1ToMem: 
			movq mm1, 0[ESI] // Read in source data from L1 
			movq mm2, 8[ESI] 
			movq mm3, 16[ESI] 
			movq mm4, 24[ESI] 
			movq mm5, 32[ESI] 
			movq mm6, 40[ESI] 
			movq mm7, 48[ESI] 
			movq mm0, 56[ESI] 

			movntq 0[EDI], mm1 // Non-temporal stores 
			movntq 8[EDI], mm2 
			movntq 16[EDI], mm3 
			movntq 24[EDI], mm4 
			movntq 32[EDI], mm5 
			movntq 40[EDI], mm6 
			movntq 48[EDI], mm7 
			movntq 56[EDI], mm0 

			add esi, 64 
			add edi, 64 
			dec ecx 
			jnz loopL1ToMem

			// Do next 4k block 
			sub ebx, eax
			jnz loop4k 

			emms

	byteCopy:
			// Do last bytes with std cpy
			mov	ecx, edx
			rep movsb
	}
	return;
}


UINT
Ultravncmemcpy::get_feature_flags(void)
{
    UINT result    = 0;
    UINT signature = 0;
    char vendor[13]        = "UnknownVendr";
    char vendorAMD[13]     = "AuthenticAMD";
    char vendorIntel[13]   = "GenuineIntel";

    __try 
    {
        __asm 
        {
            xor eax, eax
            xor ebx, ebx
            xor ecx, ecx
            xor edx, edx
            cpuid
        }
    }
    __except (EXCEPTION_EXECUTE_HANDLER) 
    {
        return (0);
    }
    
    result |= FEATURE_CPUID;

    _asm 
    {
         
         xor     eax, eax                      
         cpuid                                 
         mov     dword ptr [vendor], ebx       
         mov     dword ptr [vendor+4], edx     
         mov     dword ptr [vendor+8], ecx     
         test    eax, eax                      
         jz      $no_standard_features         
         or      [result], FEATURE_STD_FEATURES

         
         mov     eax, 1                        
         cpuid                                 
         mov     [signature], eax              

         
         mov     ecx, CPUID_STD_TSC            
         and     ecx, edx                      
         neg     ecx                           
         sbb     ecx, ecx                      
         and     ecx, FEATURE_TSC              
         or      [result], ecx                

         
         mov     ecx, CPUID_STD_MMX            
         and     ecx, edx                      
         neg     ecx                           
         sbb     ecx, ecx                      
         and     ecx, FEATURE_MMX              
         or      [result], ecx                 

         
         mov     ecx, CPUID_STD_CMOV           
         and     ecx, edx                      
         neg     ecx                           
         sbb     ecx, ecx                      
         and     ecx, FEATURE_CMOV             
         or      [result], ecx                 
         
         
         mov     ecx, CPUID_STD_MTRR           
         and     ecx, edx                      
         neg     ecx                           
         sbb     ecx, ecx                      
         and     ecx, FEATURE_P6_MTRR          
         or      [result], ecx                

         
         mov     ecx, CPUID_STD_SSE            
         and     ecx, edx                      
         neg     ecx                           
         sbb     ecx, ecx                      
         and     ecx, (FEATURE_MMXEXT+FEATURE_SSEFP+FEATURE_SSE)
                                               
         or      [result], ecx                 

         
         mov     ecx, CPUID_STD_SSE2            
         and     ecx, edx                      
         neg     ecx                           
         sbb     ecx, ecx                      
         and     ecx, (FEATURE_SSE2)           
                                               
         or      [result], ecx                 

         
         mov     eax, 0x80000000               
         cpuid                                 
         cmp     eax, 0x80000000               
         jbe     $no_extended_features         
         or      [result], FEATURE_EXT_FEATURES

         
         mov     eax, 0x80000001               
         cpuid                                
         
         
         mov     ecx, CPUID_EXT_3DNOW         
         and     ecx, edx                     
         neg     ecx                           
         sbb     ecx, ecx                      
         and     ecx, FEATURE_3DNOW           
         or      [result], ecx                 

         lea     esi, vendorAMD                
         lea     edi, vendor                  
         mov     ecx, 12                       
         cld                                   
         repe    cmpsb                         
         jnz     $not_AMD                      

        
         mov     ecx, CPUID_EXT_AMD_3DNOWEXT   
         and     ecx, edx                      
         neg     ecx                           
         sbb     ecx, ecx                     
         and     ecx, FEATURE_3DNOWEXT         
         or      [result], ecx                

         test    [result], FEATURE_MMXEXT     
         jnz     $has_mmxext                   

        

         mov     ecx, CPUID_EXT_AMD_MMXEXT    
         and     ecx, edx                      
         neg     ecx                          
         sbb     ecx, ecx                      
         and     ecx, FEATURE_MMXEXT          
         or      [result], ecx               
        
$has_mmxext:

         
         mov     eax, [signature]   
         and     eax, 0xFFF        
         cmp     eax, 0x588         
         sbb     edx, edx       
         not     edx            
         cmp     eax, 0x600        
         sbb     ecx, ecx       
         and     ecx, edx       
         and     ecx, FEATURE_K6_MTRR  
         or      [result], ecx     
         jmp     $all_done      

$not_AMD:
         
$no_extended_features:
$no_standard_features:
$all_done:
   }

   return (result);
}
