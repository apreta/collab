/////////////////////////////////////////////////////////////////////////////
//  Copyright (C) 2002 Ultr@VNC Team Members. All Rights Reserved.
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// If the source code for the program is not available from the place from
// which you received this file, check 
// http://ultravnc.sourceforge.net/

// vncMasterPwd.cpp


#include "stdhdrs.h"
#include "lmcons.h"
#include "vncService.h"

#include "WinVNC.h"
#include "vncMasterPasswd.h"
#include "vncServer.h"
#include "vncPasswd.h"

const char WINVNC_REGISTRY_KEY [] = "Software\\ORL\\WinVNC3";
const char NO_OVERRIDE_ERR [] = "This machine has been preconfigured with IIC settings, "
								"which cannot be overridden by individual users.  "
								"The preconfigured settings may be modified only by a System Administrator.";
const char NO_CURRENT_USER_ERR [] = "The IIC settings for the current user are unavailable at present.";
const char CANNOT_EDIT_DEFAULT_PREFS [] = "You do not have sufficient priviliges to edit the default local IIC settings.";

// Constructor & Destructor
vncMasterPwd::vncMasterPwd()
{
	m_dlgvisible = FALSE;
	m_usersettings = TRUE;
}

vncMasterPwd::~vncMasterPwd()
{
}

// Initialization
BOOL
vncMasterPwd::Init(vncServer *server)
{
	// Save the server pointer
	m_server = server;
	
	// Load the settings from the registry
	Load(TRUE);

	return TRUE;
}

// Dialog box handling functions
void
vncMasterPwd::Show(BOOL show, BOOL usersettings,BOOL check)
{
	m_check=check;
	if (show)
	{
		// Verify that we know who is logged on
		if (usersettings) {
			char username[UNLEN+1];
			if (!vncService::CurrentUser(username, sizeof(username)))
				return;
			if (strcmp(username, "") == 0) {
				MessageBox(NULL, NO_CURRENT_USER_ERR, "IIC Error", MB_OK | MB_ICONEXCLAMATION);
				return;
			}
		} else {
			// We're trying to edit the default local settings - verify that we can
			HKEY hkLocal, hkDefault;
			BOOL canEditDefaultPrefs = 1;
			DWORD dw;
			if (RegCreateKeyEx(HKEY_LOCAL_MACHINE,
				WINVNC_REGISTRY_KEY,
				0, REG_NONE, REG_OPTION_NON_VOLATILE,
				KEY_READ, NULL, &hkLocal, &dw) != ERROR_SUCCESS)
				canEditDefaultPrefs = 0;
			else if (RegCreateKeyEx(hkLocal,
				"Default",
				0, REG_NONE, REG_OPTION_NON_VOLATILE,
				KEY_WRITE | KEY_READ, NULL, &hkDefault, &dw) != ERROR_SUCCESS)
				canEditDefaultPrefs = 0;
			if (hkLocal) RegCloseKey(hkLocal);
			if (hkDefault) RegCloseKey(hkDefault);

			if (!canEditDefaultPrefs) {
				MessageBox(NULL, CANNOT_EDIT_DEFAULT_PREFS, "IIC Error", MB_OK | MB_ICONEXCLAMATION);
				return;
			}
		}

		// Now, if the dialog is not already displayed, show it!
		if (!m_dlgvisible)
		{
			if (usersettings)
				vnclog.Print(LL_INTINFO, VNCLOG("show per-user Shared app\n"));
			else
				vnclog.Print(LL_INTINFO, VNCLOG("show default system Shared app\n"));

			// Load in the settings relevant to the user or system
			Load(usersettings);

				m_returncode_valid = FALSE;

				// Do the dialog box
				int result = DialogBoxParam(hAppInstance,
				    MAKEINTRESOURCE(IDD_AUTH_DIALOG), 
				    NULL,
				    (DLGPROC) DialogProc,
				    (LONG) this);

				if (!m_returncode_valid)
				    result = IDCANCEL;

				vnclog.Print(LL_INTINFO, VNCLOG("dialog result = %d\n"), result);

				if (result == -1)
				{
					// Dialog box failed, so quit
					PostQuitMessage(0);
					return;
				}
				
			// Load in all the settings
			Load(TRUE);
		}
	}
}


BOOL CALLBACK
vncMasterPwd::DialogProc(HWND hwnd,
						  UINT uMsg,
						  WPARAM wParam,
						  LPARAM lParam )
{
	// We use the dialog-box's USERDATA to store a _this pointer
	// This is set only once WM_INITDIALOG has been recieved, though!
	vncMasterPwd *_this = (vncMasterPwd *) GetWindowLong(hwnd, GWL_USERDATA);

	switch (uMsg)
	{

	case WM_INITDIALOG:
		{
			// Retrieve the Dialog box parameter and use it as a pointer
			// to the calling vncProperties object
			SetWindowLong(hwnd, GWL_USERDATA, lParam);
			_this = (vncMasterPwd *) lParam;

		   HWND hNameMasterPwd = GetDlgItem(hwnd, IDC_PASSWD_EDIT);
           SetDlgItemText(hwnd, IDC_PASSWD_EDIT,"");
		   EnableWindow(hNameMasterPwd,true);
			
			SetForegroundWindow(hwnd);
			_this->m_dlgvisible = TRUE;

			return TRUE;
		}

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{

		case IDOK:
			{
				char passwd[MAXPWLEN+1];
				char *encrpasswd;
				if (_this->m_check)
				{
					_this->m_server->Masterpasswd(false);
					GetDlgItemText(hwnd, IDC_PASSWD_EDIT, (LPSTR) &passwd, MAXPWLEN+1);
					encrpasswd=_this->m_server->GetMasterPwd();
					vncPasswd::ToText current(encrpasswd);
					BOOL password_changed = FALSE;
					int a=strlen(encrpasswd);
					if (a!=0) 
					{
						for (int i=0; i<MAXPWLEN; i++) {
								if (passwd[i] != current[i]) password_changed = TRUE;
							}
						if (!password_changed) _this->m_server->Masterpasswd(true);
					}
					else
					{
						_this->m_server->Masterpasswd(true);
					}
	
					_this->m_returncode_valid = TRUE;
					EndDialog(hwnd, IDOK);
					_this->m_dlgvisible = FALSE;
				}
				else //set new masterpasswd
				{
					GetDlgItemText(hwnd, IDC_PASSWD_EDIT, (LPSTR) &passwd, MAXPWLEN+1);
					vncPasswd::FromText crypt(passwd);
					_this->m_server->SetMasterPwd(crypt);
					_this->Save();
					_this->m_returncode_valid = TRUE;
					EndDialog(hwnd, IDOK);
					_this->m_dlgvisible = FALSE;
				}
				return TRUE;
			}

		case IDCANCEL:
			vnclog.Print(LL_INTINFO, VNCLOG("enddialog (CANCEL)\n"));
			_this->m_server->Masterpasswd(false);

			_this->m_returncode_valid = TRUE;

			EndDialog(hwnd, IDCANCEL);
			_this->m_dlgvisible = FALSE;
			return false;

		}

		break;
	}
	return 0;
}

void
vncMasterPwd::LoadMasterPwd(HKEY key, char *buffer)
{
	DWORD type = REG_BINARY;
	int slen=MAXPWLEN;
	char inouttext[MAXPWLEN];

	// Retrieve the encrypted password
	if (RegQueryValueEx(key,
		"MasterPwd",
		NULL,
		&type,
		(LPBYTE) &inouttext,
		(LPDWORD) &slen) != ERROR_SUCCESS)
		return;

	if (slen > MAXPWLEN)
		return;

	memcpy(buffer, inouttext, MAXPWLEN);
}

void
vncMasterPwd::Load(BOOL usersettings)
{
	char username[UNLEN+1];
	HKEY hkLocal, hkLocalUser, hkDefault;
	DWORD dw;

	// Get the user name / service name
	if (!vncService::CurrentUser((char *)&username, sizeof(username)))
		return;

	// If there is no user logged on them default to SYSTEM
	if (strcmp(username, "") == 0)
		strcpy((char *)&username, "SYSTEM");

	// Try to get the machine registry key for WinVNC
	if (RegCreateKeyEx(HKEY_LOCAL_MACHINE,
		WINVNC_REGISTRY_KEY,
		0, REG_NONE, REG_OPTION_NON_VOLATILE,
		KEY_READ, NULL, &hkLocal, &dw) != ERROR_SUCCESS)
		return;

	// Now try to get the per-user local key
	if (RegOpenKeyEx(hkLocal,
		username,
		0, KEY_READ,
		&hkLocalUser) != ERROR_SUCCESS)
		hkLocalUser = NULL;

	// Get the default key
	if (RegCreateKeyEx(hkLocal,
		"Default",
		0, REG_NONE, REG_OPTION_NON_VOLATILE,
		KEY_READ,
		NULL,
		&hkDefault,
		&dw) != ERROR_SUCCESS)
		hkDefault = NULL;

	strcpy(m_pref_MasterPwd,"");


	// Load the local prefs for this user
	if (hkDefault != NULL)
	{
		vnclog.Print(LL_INTINFO, VNCLOG("loading DEFAULT local settings\n"));
		LoadUserPrefs(hkDefault);
	}
	if (hkLocalUser != NULL) RegCloseKey(hkLocalUser);
	if (hkDefault != NULL) RegCloseKey(hkDefault);
	RegCloseKey(hkLocal);
	m_usersettings = usersettings;
}

void
vncMasterPwd::LoadUserPrefs(HKEY appkey)
{	
	LoadMasterPwd(appkey, m_pref_MasterPwd);
	m_server->SetMasterPwd(m_pref_MasterPwd);

}

void
vncMasterPwd::Save()
{
	HKEY appkey;
	DWORD dw;

		// Try to get the default local registry key for WinVNC
		HKEY hkLocal;
		if (RegCreateKeyEx(HKEY_LOCAL_MACHINE,
			WINVNC_REGISTRY_KEY,
			0, REG_NONE, REG_OPTION_NON_VOLATILE,
			KEY_READ, NULL, &hkLocal, &dw) != ERROR_SUCCESS) {
			MessageBox(NULL, "MB1", "WVNC", MB_OK);
			return;
		}
		if (RegCreateKeyEx(hkLocal,
			"Default",
			0, REG_NONE, REG_OPTION_NON_VOLATILE,
			KEY_WRITE | KEY_READ, NULL, &appkey, &dw) != ERROR_SUCCESS) {
			RegCloseKey(hkLocal);
			return;
		}
		RegCloseKey(hkLocal);
	

	// SAVE PER-USER PREFS IF ALLOWED
	SaveUserPrefs(appkey);
	RegCloseKey(appkey);	
	RegCloseKey(HKEY_CURRENT_USER);


}

void
vncMasterPwd::SaveUserPrefs(HKEY appkey)
{
	char masterpwd[MAXPWLEN];
	strcpy(masterpwd,m_server->GetMasterPwd());
	SaveMasterPwd(appkey, masterpwd);
	
	
}
void
vncMasterPwd::SaveMasterPwd(HKEY key, char *buffer)
{
	RegSetValueEx(key, "MasterPwd", 0, REG_BINARY, (LPBYTE) buffer, MAXPWLEN);
}

