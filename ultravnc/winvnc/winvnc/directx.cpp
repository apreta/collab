// Use DirectX API to capture desktop
//
// (c) 2012 Michael Tinglof, mercuri.ca
//

// System headers
#include <assert.h>
#include "stdhdrs.h"
#include "directx.h"

#include "../../netlib/log.h"	// IIC

#include <d3d9.h>

#define BITSPERPIXEL		32

bool				g_fForceOn=FALSE;

bool				g_fDirectXCapture=FALSE;
IDirect3D9*			g_pD3D=NULL;
IDirect3DDevice9*	g_pd3dDevice=NULL;
IDirect3DSurface9*	g_pSurface=NULL;
BYTE*				g_pBuffer=NULL;

char*				g_deviceName=NULL;
RECT				g_displayRect={0,0,0,0};


static const char* d3d_module_name = "d3d9.dll";
static HMODULE d3d_module = 0;
typedef LPDIRECT3D9 (WINAPI *DIRECT3DCREATE9PROC)(UINT);
static DIRECT3DCREATE9PROC d3d_create = (DIRECT3DCREATE9PROC)NULL;


void RequestDirectX(bool flag)
{
	g_fForceOn = flag;
}

bool UsingDirectX()
{
	return g_fDirectXCapture;
}

void Cleanup()
{
	if(g_pSurface)
	{
		g_pSurface->Release();
		g_pSurface=NULL;
	}
	if(g_pd3dDevice)
	{
		g_pd3dDevice->Release();
		g_pd3dDevice=NULL;
	}
	if(g_pD3D)
	{
		g_pD3D->Release();
		g_pD3D=NULL;
	}
}

void SelectMonitor(char *displayDevice, RECT displayRect)
{
	g_deviceName = _strdup(displayDevice);
	g_displayRect = displayRect;
}

HRESULT	InitD3D(HWND hWnd)
{

	OSVERSIONINFO osversioninfo;
	osversioninfo.dwOSVersionInfoSize = sizeof(osversioninfo);

	// Get the current OS version
	if (!g_fForceOn && 
			(!GetVersionEx(&osversioninfo) || osversioninfo.dwMajorVersion < 6 || 
				(osversioninfo.dwMajorVersion == 6 && osversioninfo.dwMinorVersion < 2)))
		// Pre-windows 8
		return S_OK;	

	d3d_module = LoadLibraryA(d3d_module_name);
	if (d3d_module == NULL) {
		errorf("DirectX: Unable to load %s\n", d3d_module_name);
		return E_FAIL;
	}

	d3d_create = (DIRECT3DCREATE9PROC)GetProcAddress(d3d_module, "Direct3DCreate9");

	D3DDISPLAYMODE	ddm;
	D3DPRESENT_PARAMETERS	d3dpp;

	if((g_pD3D=d3d_create(D3D_SDK_VERSION))==NULL)
	{
		errorf("DirectX: Unable to Create Direct3D\n");
		return E_FAIL;
	}
	
	UINT dispId = D3DADAPTER_DEFAULT;
	if (g_deviceName) 
	{
		D3DADAPTER_IDENTIFIER9 did;
		int adapterCount = g_pD3D->GetAdapterCount();
		for (int i=0; i<adapterCount; i++) {
			g_pD3D->GetAdapterIdentifier(i, 0, &did);
			if (strcmp(did.DeviceName, g_deviceName) == 0) {
				debugf("DirectX: %d - device %s\n", i, did.DeviceName);
				dispId = i;
				break;
			}
		}
	}

	if(FAILED(g_pD3D->GetAdapterDisplayMode(dispId, &ddm)))
	{
		errorf("DirectX: Unable to Get Adapter Display Mode\n");
		return E_FAIL;
	}
	
	/*extern HINSTANCE	hAppInstance;
	HWND hwnd1 = CreateWindowEx( NULL, "STATIC", 
				"win",
				WS_POPUP | WS_VISIBLE,
				0, 0, 200, 200, NULL, NULL, hAppInstance, NULL );
	HWND hwnd2 = CreateWindowEx( NULL, "STATIC", 
				"win",
				WS_POPUP | WS_VISIBLE,
				2000, 0, 200, 200, NULL, NULL, hAppInstance, NULL );
	*/

	ZeroMemory(&d3dpp, sizeof(D3DPRESENT_PARAMETERS));

	d3dpp.Windowed=true;
	d3dpp.Flags=D3DPRESENTFLAG_LOCKABLE_BACKBUFFER;
	d3dpp.BackBufferFormat=ddm.Format;
	d3dpp.BackBufferHeight=ddm.Height;
	d3dpp.BackBufferWidth=ddm.Width;
	d3dpp.MultiSampleType=D3DMULTISAMPLE_NONE;
	d3dpp.SwapEffect=D3DSWAPEFFECT_DISCARD;
	d3dpp.hDeviceWindow=hWnd;
	d3dpp.PresentationInterval=D3DPRESENT_INTERVAL_DEFAULT;
	d3dpp.FullScreen_RefreshRateInHz=D3DPRESENT_RATE_DEFAULT;

	if(FAILED(g_pD3D->CreateDevice(dispId, D3DDEVTYPE_HAL, hWnd, D3DCREATE_HARDWARE_VERTEXPROCESSING/*|D3DCREATE_ADAPTERGROUP_DEVICE*/, &d3dpp, &g_pd3dDevice)))
	{
		errorf("DirectX: Unable to Create Device\n");
		return E_FAIL;
	}

	if(FAILED(g_pd3dDevice->CreateOffscreenPlainSurface(ddm.Width, ddm.Height, D3DFMT_A8R8G8B8, D3DPOOL_SCRATCH, &g_pSurface, NULL)))
	{
		errorf("DirectX: Unable to Create Surface\n");
		return E_FAIL;
	}

	errorf("DirectX: Initialized capture\n");
	g_fDirectXCapture = true;
	return S_OK;
}

HRESULT Reset(HWND hWnd)
{
	D3DDISPLAYMODE	ddm;
	D3DPRESENT_PARAMETERS	d3dpp;

	if(g_pSurface)														//Release the Surface - we need to get the latest surface
	{
		g_pSurface->Release();
		g_pSurface = NULL;
	}

	if(FAILED(g_pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &ddm)))	//User might have changed the mode - Get it afresh
	{
		errorf("Unable to Get Adapter Display Mode");
		return E_FAIL;
	}

	ZeroMemory(&d3dpp, sizeof(D3DPRESENT_PARAMETERS));

	d3dpp.Windowed=true;
	d3dpp.Flags=D3DPRESENTFLAG_LOCKABLE_BACKBUFFER;
	d3dpp.BackBufferFormat=ddm.Format;
	d3dpp.BackBufferHeight=ddm.Height;
	d3dpp.BackBufferWidth=ddm.Width;
	d3dpp.MultiSampleType=D3DMULTISAMPLE_NONE;
	d3dpp.SwapEffect=D3DSWAPEFFECT_DISCARD;
	d3dpp.hDeviceWindow=hWnd;
	d3dpp.PresentationInterval=D3DPRESENT_INTERVAL_DEFAULT;
	d3dpp.FullScreen_RefreshRateInHz=D3DPRESENT_RATE_DEFAULT;

	if(FAILED(g_pd3dDevice->Reset(&d3dpp)))
	{
		errorf("DirectX: Unable to Reset Device\n");
		return E_FAIL;
	}

	if(FAILED(g_pd3dDevice->CreateOffscreenPlainSurface(ddm.Width, ddm.Height, D3DFMT_A8R8G8B8, D3DPOOL_SCRATCH, &g_pSurface, NULL)))
	{
		errorf("DirectX: Unable to Create Surface\n");
		return E_FAIL;
	}

	return S_OK;
}

HRESULT CaptureRectangle(RECT rect, BYTE *scrBuff, UINT scrBuffSize, UINT bytesPerRow, int offsetX, int offsetY)
{
	OffsetRect(&rect, -g_displayRect.left, -g_displayRect.top);

	g_pd3dDevice->GetFrontBufferData(0, g_pSurface);			

	debugf("DirectX: capture %dx%d %dx%d\n", rect.left, rect.top, rect.right, rect.bottom);

	D3DLOCKED_RECT	lockedRect;
	if(FAILED(g_pSurface->LockRect(&lockedRect, &rect, D3DLOCK_NO_DIRTY_UPDATE|D3DLOCK_NOSYSLOCK|D3DLOCK_READONLY)))
	{
		errorf("DirectX: Unable to Lock Front Buffer Surface\n");
		return E_FAIL;
	}

	int height = rect.bottom - rect.top;
	int width = rect.right - rect.left;

	OffsetRect(&rect, offsetX, offsetY);

	for(int i=0;i<height;i++)
	{
		memcpy(scrBuff+(rect.top+i)*bytesPerRow+rect.left*BITSPERPIXEL/8,
			(BYTE*)(lockedRect.pBits)+i*lockedRect.Pitch, 
			width*BITSPERPIXEL/8);
	}
	g_pSurface->UnlockRect();

	return S_OK;
}

COLORREF CapturePixel(int x, int y, int bytesPerRow)
{
	x -= g_displayRect.left;
	y -= g_displayRect.top;

	if (g_pBuffer == NULL)
		return -1;
	
	BYTE *pos = g_pBuffer + y*bytesPerRow + x*BITSPERPIXEL/8;
	return *((DWORD *)pos);
}

HRESULT BufferScreen(RECT rect, int bytesPerRow)
{
	RECT captRect = { rect.left,rect.top,rect.right,rect.bottom };

	const int PIXEL_BLOCK_SIZE  = 32; // Pixels Grid definition
	int size = (rect.right-g_displayRect.left+PIXEL_BLOCK_SIZE)*(rect.bottom-g_displayRect.top+PIXEL_BLOCK_SIZE)*4;

	if (g_pBuffer == NULL)
		g_pBuffer = (BYTE*)malloc(size);
	else
		g_pBuffer = (BYTE*)realloc(g_pBuffer, size);
	CaptureRectangle(captRect, g_pBuffer, size, bytesPerRow);

	return S_OK;
}

HRESULT BlankRect(RECT rect, DWORD color, BYTE *scrBuff, UINT scrBuffSize, UINT bytesPerRow)
{
	int height = rect.bottom - rect.top;
	int width = rect.right - rect.left;
	BYTE * line = scrBuff+(rect.top)*bytesPerRow+rect.left*BITSPERPIXEL/8;

	for(int i=0;i<height;i++)
	{
		DWORD *pos = (DWORD*)line;
		for (int j=0;j<width;j++)
		{
			*pos = color;
			++pos;
		}
		line += bytesPerRow;
	}

	return S_OK;
}

