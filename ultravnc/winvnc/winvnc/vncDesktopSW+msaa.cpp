// System headers
#include <assert.h>
#include "stdhdrs.h"

// Custom headers
#include <omnithread.h>
#include "WinVNC.h"
#include "VNCHooks\VNCHooks.h"
#include "vncServer.h"
#include "rfbRegion.h"
#include "rfbRect.h"
#include "vncDesktop.h"
#include "vncService.h"



typedef struct _importexport
{
	POINT point;
	RECT rect;
	char text[128];
	char text2[128];
} importexport, *pimportexport;

//msaa removed crash debug
int fnPointlib(pimportexport myimport);


BOOL vncDesktop:: CalculateSWrect(RECT &rect)
{
	if (logon==1)
		{
			GetWindowRect(pseudologon->logonhwnd,&rect);
			m_SWHeight=rect.bottom-rect.top;
			m_SWWidth=rect.right-rect.left;
			return TRUE;
		}

	if (logon==3)
			{
				logon=4;
				EndPseudoLogon();
				m_SWOffsetx=0;
				m_SWOffsety=0;
				m_SWWidth=m_scrinfo.framebufferWidth;
				m_SWHeight=m_scrinfo.framebufferHeight;
				m_SWSizeChanged=TRUE;
				if (!m_server->SingleWindow())return TRUE;
			}

	if (!m_Single_hWnd) {m_server->KillAuthClients();return FALSE;}
	if (IsIconic(m_Single_hWnd) && m_OleEnable)
	{
		RECT recttaskbar;
		importexport myexport;
		char testname[128];
		HWND h0=FindWindow("Shell_TrayWnd", NULL );
		HWND h1=FindWindowEx(h0,NULL,"RebarWindow32", NULL);
		HWND h2=FindWindowEx(h1,NULL,"MSTaskSwWClass", NULL);
		HWND h3=FindWindowEx(h2,NULL,"SysTabControl32", NULL);
		GetWindowRect( h3, &recttaskbar );
		GetWindowText(m_Single_hWnd,testname,127);
		for (int j=recttaskbar.top;j<recttaskbar.bottom;j=j+20)
		{
			for (int i=recttaskbar.left;i<recttaskbar.right;i=i+20)
				{
					myexport.point.x=i+1;
					myexport.point.y=j+1;
					strcpy(myexport.text,"");
					//msaa removed crash debug
					fnPointlib(&myexport);
					BOOL result=strcmp(testname,myexport.text);
					if (!result) 
						{
							rect.left=myexport.rect.left;
							rect.right=myexport.rect.right+myexport.rect.left;
							rect.top=myexport.rect.top;
							rect.bottom=myexport.rect.bottom+myexport.rect.top;
							if ((m_SWHeight!=(rect.bottom-rect.top)) || (m_SWWidth!=(rect.right-rect.left)))
								m_SWSizeChanged=TRUE;
							m_SWHeight=rect.bottom-rect.top;
							m_SWWidth=rect.right-rect.left;
							return TRUE;
						}
				
				}
		}
	}
	if ( IsWindowVisible(m_Single_hWnd) && GetWindowRect(m_Single_hWnd,&rect)) 
		{
			rect.top=Max(rect.top,0);
			rect.left=Max(rect.left,0);
			rect.right=Min(rect.right,GetSystemMetrics(SM_CXFULLSCREEN));
			rect.bottom=Min(rect.bottom,GetSystemMetrics(SM_CYFULLSCREEN));
			if ((m_SWHeight!=(rect.bottom-rect.top)) || (m_SWWidth!=(rect.right-rect.left)))
				m_SWSizeChanged=TRUE;
			

			m_SWHeight=rect.bottom-rect.top;
			m_SWWidth=rect.right-rect.left;
			return TRUE;
		}
	
m_server->KillAuthClients();
return FALSE;		
}

void vncDesktop::CheckSWOleacc()
{
	HMODULE  hModule = LoadLibrary(("oleacc.dll"));
		if (!hModule)
		{
			m_OleEnable=FALSE;
		}
		else
		{
			FreeLibrary(hModule);
			m_OleEnable=TRUE;
		}
}

void vncDesktop::SWinit()
{
	m_OleEnable=FALSE;
	CheckSWOleacc();
	m_Single_hWnd=NULL;
	m_Single_hWnd_backup=NULL;
	m_SWHeight=0;
	m_SWWidth=0;
	m_SWSizeChanged=FALSE;
	m_SWOffsetx=0;
	m_SWOffsety=0;
}

rfb::Rect
vncDesktop::GetSize()
{
if (m_server->SingleWindow())
	{
	RECT rect;
	CalculateSWrect(rect);
	m_SWOffsetx=rect.left;
	m_SWOffsety=rect.top;
	return rfb::Rect(rect.left, rect.top, rect.right, rect.bottom);
	}
else 
	{
	m_SWOffsetx=0;
	m_SWOffsety=0;
	return rfb::Rect(0, 0, m_scrinfo.framebufferWidth, m_scrinfo.framebufferHeight);
	}
}

rfb::Rect
vncDesktop::GetQuarterSize()
{
if (m_server->SingleWindow())
	{
	RECT rect;
	CalculateSWrect(rect);
	m_SWOffsetx=rect.left;
	m_SWOffsety=rect.top;
	return rfb::Rect(rect.left, rect.top, rect.right-rect.left, (rect.bottom-rect.top)/4);
	}
else
	{ 
	m_SWOffsetx=0;
	m_SWOffsety=0;
	return rfb::Rect(0, 0, m_bmrect.br.x, m_bmrect.br.y/4);
	}
}

