//  Copyright (C) 2002 Ultr@VNC Team Members. All Rights Reserved.
//  Copyright (C) 2000-2002 Const Kaplinsky. All Rights Reserved.
//  Copyright (C) 2002 RealVNC Ltd. All Rights Reserved.
//  Copyright (C) 1999 AT&T Laboratories Cambridge. All Rights Reserved.
//
//  This file is part of the VNC system.
//
//  The VNC system is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// If the source code for the VNC system is not available from the place 
// whence you received this file, check http://www.uk.research.att.com/vnc or contact
// the authors on vnc@uk.research.att.com for information on obtaining it.


// vncServer.h

// vncServer class handles the following functions:
// - Allowing clients to be dynamically added and removed
// - Propagating updates from the local vncDesktop object
//   to all the connected clients
// - Propagating mouse movements and keyboard events from
//   clients to the local vncDesktop
// It also creates the vncSockConnect and vncCORBAConnect
// servers, which respectively allow connections via sockets
// and via the ORB interface

class vncServer;

#if (!defined(_WINVNC_VNCSERVER))
#define _WINVNC_VNCSERVER

// Custom
#include "vncCORBAConnect.h"
#include "vncSockConnect.h"
#include "vncHTTPConnect.h"
#include "vncClient.h"
#include "rfbRegion.h"
#include "vncPasswd.h"

// Includes
#include "stdhdrs.h"
#include <omnithread.h>
#include <list>
#include <string>

// Define a datatype to handle lists of windows we wish to notify
typedef std::list<HWND> vncNotifyList;
// IIC
typedef std::list<DWORD> vncProcessIDList;

// Some important constants;
const int MAX_CLIENTS = 128;

// The vncServer class itself

class vncServer
{
public:
	// Constructor/destructor
	vncServer(const char* appsToShare, int start_time, bool enable_recording, bool share_allinstances);	// IIC -- handle application sharing
	~vncServer();

	// Client handling functions
	virtual vncClientId AddClient(VSocket *socket, BOOL auth, BOOL shared);
	virtual vncClientId AddClient(VSocket *socket,
		BOOL auth, BOOL shared, BOOL teleport, int capability,
		BOOL keysenabled, BOOL ptrenabled);
	virtual BOOL Authenticated(vncClientId client);
	virtual void KillClient(vncClientId client);
	virtual void KillClient(LPSTR szClientName); // sf@2002
	virtual void TextChatClient(LPSTR szClientName); // sf@2002

	virtual UINT AuthClientCount();
	virtual UINT UnauthClientCount();

	virtual void KillAuthClients();
	virtual void ListAuthClients(HWND hListBox);
	virtual void WaitUntilAuthEmpty();

	virtual void KillUnauthClients();
	virtual void WaitUntilUnauthEmpty();

	// Are any clients ready to send updates?
	virtual BOOL UpdateWanted();

	// Has at least one client had a remote event?
	virtual BOOL RemoteEventReceived();

	// Client info retrieval/setup
	virtual vncClient* GetClient(vncClientId clientid);
	virtual vncClientList ClientList();

	virtual void SetTeleport(vncClientId client, BOOL teleport);
	virtual void SetCapability(vncClientId client, int capability);
	virtual void SetKeyboardEnabled(vncClientId client, BOOL enabled);
	virtual void SetPointerEnabled(vncClientId client, BOOL enabled);

	virtual BOOL IsTeleport(vncClientId client);
	virtual int GetCapability(vncClientId client);
	virtual BOOL GetKeyboardEnabled(vncClientId client);
	virtual BOOL GetPointerEnabled(vncClientId client);
	virtual const char* GetClientName(vncClientId client);

	// Let a client remove itself
	virtual void RemoveClient(vncClientId client);

	// Connect/disconnect notification
	virtual BOOL AddNotify(HWND hwnd);
	virtual BOOL RemNotify(HWND hwnd);

	// Modif sf@2002 - Single Window
	virtual void SingleWindow(BOOL fEnabled) { m_SingleWindow = fEnabled; };
	virtual BOOL SingleWindow() { return m_SingleWindow; };
	virtual void SetSingleWindowName(const char *szName);
	// Starts IIC -- handle application sharing
	virtual const char *GetWindowName() { return m_szWindowName.c_str(); };
	virtual vncProcessIDList* GetProcessIDList() { return &m_ProcessIDList; }
	// Ends IIC -- handle application sharing
	virtual vncDesktop* GetDesktopPointer() {return m_desktop;}
	virtual void SetNewSWSize(long w,long h,BOOL desktop);
	virtual void SetSWOffset(int x,int y);
	virtual void SetScreenOffset(int x,int y);
	// Starts IIC -- handle application sharing
	void AppSharing(BOOL fEnabled) { m_AppSharing = fEnabled; };
	BOOL AppSharing() { return m_AppSharing; };
	void EnableTransparent(BOOL bEnable) { m_EnableTransparent = bEnable; }
	BOOL IsTransparentEnabled() { return m_EnableTransparent; }
	void AddToProcessIDList(const char* aProcIDs);
    void ShareAllApplicationInstances(BOOL aAll);
    BOOL ShareAllApplicationInstances();
	void SetSharePPT(BOOL aSharePPT);
	BOOL IsSharingPPT() { return m_SharePPT; }
	void SetPPTWindowHndl(HWND hWnd, HWND hWndParent);
	char* GetMeetingId() { return m_meeting_id; }
	void SetMeetingId(char* id) { strncpy(m_meeting_id, id, MAX_PATH-1); }
	char* GetSessionToken() { return m_session_token; }
	void SetSessionToken(char* id) { strncpy(m_session_token, id, MAX_PATH-1); }
	char* GetParticipantId() { return m_participant_id; }
	void SetParticipantId(char* id) { strncpy(m_participant_id, id, MAX_PATH-1); }
	void SetReduceColor(BOOL aReduce);
	void SetAppshareKey(const char * key);
	void SetDisplayName(const char * name);
	char* GetDisplayName() { return m_display_name; }
	// IIC
	void CloseAllClients();
	void EndSession();

	// remote control enhancement
	void GrantControl(vncClientId clientid);
	BOOL HasControl() { return m_hasControl; }
	BOOL m_hasControl;
	BOOL m_reduceColor;
	char m_key[256];
	int  m_start_time;
	int  GetStartTime() { return m_start_time; }
	BOOL m_enable_recording;
	BOOL m_agent_mode;
	BOOL m_enable_nagle_algorithm;
	void EnableRecording(BOOL enable);
	BOOL EnableRecording() { return m_enable_recording; }
	void SetAgentMode(BOOL aEnable);
	BOOL IsAgentMode() { return m_agent_mode; }
	BOOL EnableNagleAlgorithm() { return m_enable_nagle_algorithm; }
	void SetNagleAlgorithm(BOOL enable);
	// Ends IIC -- handle application sharing
	
protected:
	// Send a notification message
	virtual void DoNotify(UINT message, WPARAM wparam, LPARAM lparam);

public:
	// Update handling, used by the screen server
	virtual rfb::UpdateTracker &GetUpdateTracker() {return m_update_tracker;};
	virtual void UpdateMouse();
	virtual void UpdateClipText(const char* text);
	virtual void UpdatePalette();
	virtual void UpdateLocalFormat();

	// Polling mode handling
	virtual void PollUnderCursor(BOOL enable) {m_poll_undercursor = enable;};
	virtual BOOL PollUnderCursor() {return m_poll_undercursor;};
	virtual void PollForeground(BOOL enable) {m_poll_foreground = enable;};
	virtual BOOL PollForeground() {return m_poll_foreground;};
	virtual void PollFullScreen(BOOL enable) {m_poll_fullscreen = enable;};
	virtual BOOL PollFullScreen() {return m_poll_fullscreen;};

	virtual void Driver(BOOL enable);
	virtual BOOL Driver() {return m_driver;};
	virtual void Hook(BOOL enable);
	virtual BOOL Hook() {return m_hook;};
	virtual void Virtual(BOOL enable) {m_virtual = enable;};
	virtual BOOL Virtual() {return m_virtual;};
	virtual void SetHookings();

	virtual void PollConsoleOnly(BOOL enable) {m_poll_consoleonly = enable;};
	virtual BOOL PollConsoleOnly() {return m_poll_consoleonly;};
	virtual void PollOnEventOnly(BOOL enable) {m_poll_oneventonly = enable;};
	virtual BOOL PollOnEventOnly() {return m_poll_oneventonly;};

	// Client manipulation of the clipboard
	virtual void UpdateLocalClipText(LPSTR text);

	// Name and port number handling
	// TightVNC 1.2.7
	virtual void SetName(const char * name);
	virtual void SetPorts(const UINT port_rfb, const UINT port_http);
	virtual UINT GetPort() { return m_port; };
	virtual UINT GetHttpPort() { return m_port_http; };
	// RealVNC method
	/*
	virtual void SetPort(const UINT port);
	virtual UINT GetPort();
	*/
	virtual void SetAutoPortSelect(const BOOL autoport) {
	    if (autoport && !m_autoportselect)
	    {
		BOOL sockconnect = SockConnected();
		SockConnect(FALSE);
		m_autoportselect = autoport;
		SockConnect(sockconnect);
	    }
		else
		{
			m_autoportselect = autoport;
		}
	};
	virtual BOOL AutoPortSelect() {return m_autoportselect;};

	// Password set/retrieve.  Note that these functions now handle the encrypted
	// form, not the plaintext form.  The buffer passwed MUST be MAXPWLEN in size.
	virtual void SetPassword(const char *passwd);
	virtual void GetPassword(char *passwd);

	// Remote input handling
	virtual void EnableRemoteInputs(BOOL enable);
	virtual BOOL RemoteInputsEnabled();

	// Local input handling
	virtual void DisableLocalInputs(BOOL disable);
	virtual BOOL LocalInputsDisabled();

	// General connection handling
	virtual void SetConnectPriority(UINT priority) {m_connect_pri = priority;};
	virtual UINT ConnectPriority() {return m_connect_pri;};

	// Socket connection handling
	virtual BOOL SockConnect(BOOL on);
	virtual BOOL SockConnected();
	virtual BOOL SetLoopbackOnly(BOOL loopbackOnly);
	virtual BOOL LoopbackOnly();


	// Tray icon disposition
	virtual BOOL SetDisableTrayIcon(BOOL disableTrayIcon);
	virtual BOOL GetDisableTrayIcon();


	// HTTP daemon handling
	virtual BOOL EnableHTTPConnect(BOOL enable);
	virtual BOOL HTTPConnectEnabled() {return m_enableHttpConn;};
	virtual BOOL EnableXDMCPConnect(BOOL enable);
	virtual BOOL XDMCPConnectEnabled() {return m_enableXdmcpConn;};

	// CORBA connection handling
	virtual BOOL CORBAConnect(BOOL on);
	virtual BOOL CORBAConnected();
	virtual void GetScreenInfo(int &width, int &height, int &depth);

	// Allow connections if no password is set?
	virtual void SetAuthRequired(BOOL reqd) {m_passwd_required = reqd;};
	virtual BOOL AuthRequired() {return m_passwd_required;};

	// Handling of per-client connection authorisation
	virtual void SetAuthHosts(const char *hostlist);
	virtual char *AuthHosts();
	enum AcceptQueryReject {aqrAccept, aqrQuery, aqrReject};
	virtual AcceptQueryReject VerifyHost(const char *hostname);

	// Blacklisting of machines which fail connection attempts too often
	// Such machines will fail VerifyHost for a short period
	virtual void AddAuthHostsBlacklist(const char *machine);
	virtual void RemAuthHostsBlacklist(const char *machine);

	// Connection querying settings
	virtual void SetQuerySetting(const UINT setting) {m_querysetting = setting;};
	virtual UINT QuerySetting() {return m_querysetting;};
	virtual void SetQueryTimeout(const UINT setting) {m_querytimeout = setting;};
	virtual UINT QueryTimeout() {return m_querytimeout;};

	// Whether or not to allow connections from the local machine
	virtual void SetLoopbackOk(BOOL ok) {m_loopback_allowed = ok;};
	virtual BOOL LoopbackOk() {return m_loopback_allowed;};

	// Whether or not to shutdown or logoff when the last client leaves
	virtual void SetLockSettings(int ok) {m_lock_on_exit = ok;};
	virtual int LockSettings() {return m_lock_on_exit;};

	// Timeout for automatic disconnection of idle connections
	virtual void SetAutoIdleDisconnectTimeout(const UINT timeout) {m_idle_timeout = timeout;};
	virtual UINT AutoIdleDisconnectTimeout() {return m_idle_timeout;};

	// Removal of desktop wallpaper, etc
	virtual void EnableRemoveWallpaper(const BOOL enable) {m_remove_wallpaper = enable;};
	virtual BOOL RemoveWallpaperEnabled() {return m_remove_wallpaper;};

	// sf@2002 - v1.1.x - Server Default Scale
	virtual UINT GetDefaultScale();
	virtual BOOL SetDefaultScale(int nScale);
	virtual BOOL FileTransferEnabled();
	virtual BOOL EnableFileTransfer(BOOL fEnable);
	virtual BOOL BlankMonitorEnabled() {return m_fBlankMonitorEnabled;};
	virtual void BlankMonitorEnabled(BOOL fEnable) {m_fBlankMonitorEnabled = fEnable;};
	virtual BOOL MSLogonRequired();
	virtual BOOL RequireMSLogon(BOOL fEnable);

	// sf@2002 - DSM Plugin
	virtual BOOL IsDSMPluginEnabled();
	virtual void EnableDSMPlugin(BOOL fEnable);
	virtual char* GetDSMPluginName();
	virtual void SetDSMPluginName(char* szDSMPlugin);
	virtual BOOL SetDSMPlugin(void);
	virtual CDSMPlugin* GetDSMPluginPointer() { return m_pDSMPlugin;};

	// sf@2002 - Cursor handling
	virtual void EnableXRichCursor(BOOL fEnable);
	virtual BOOL IsXRichCursorEnabled() {return m_fXRichCursor;}; 

	virtual void SetNamePath1(const char *name);
    virtual char *GetNamePath1() { return m_namePath1; };
	virtual void SetNamePath2(const char *name);
    virtual char *GetNamePath2() { return m_namePath2; };
	virtual void SetNamePath3(const char *name);
    virtual char *GetNamePath3() { return m_namePath3; };
	virtual void SetSuCommand(const char *name);
    virtual char *GetSuCommand() { return m_sucommand; };
	virtual void SetMasterPwd(const char *name);
    virtual char *GetMasterPwd() { return m_masterpwd; };
	virtual void Masterpasswd(BOOL enabled){m_master=enabled;};
	virtual BOOL Masterpasswd(){return m_master;};

	// sf@2002
	virtual void DisableCacheForAllClients();
	virtual bool IsThereASlowClient();
	virtual bool IsThereAUltraEncodingClient();

	// sf@2002 - Turbo Mode
	virtual void TurboMode(BOOL fEnabled) { m_TurboMode = fEnabled; };
	virtual BOOL TurboMode() { return m_TurboMode; };

protected:
	// The vncServer UpdateTracker class
	// Behaves like a standard UpdateTracker, but propagates update
	// information to active clients' trackers

	class ServerUpdateTracker : public rfb::UpdateTracker {
	public:
		ServerUpdateTracker() : m_server(0) {};

		virtual void init(vncServer *server) {m_server=server;};

		virtual void add_changed(const rfb::Region2D &region);
		virtual void add_cached(const rfb::Region2D &region);
		virtual void add_Text(const rfb::Region2D &region);
		virtual void add_Solid(const rfb::Region2D &region);
		virtual void add_copied(const rfb::Region2D &dest, const rfb::Point &delta);
	protected:
		vncServer *m_server;
	};

	friend class ServerUpdateTracker;

	ServerUpdateTracker	m_update_tracker;

	// Internal stuffs
protected:
	// Connection servers
	vncSockConnect		*m_socketConn;
	vncCorbaConnect		*m_corbaConn;
	vncHTTPConnect		*m_httpConn;
	HANDLE				m_xdmcpConn;
	BOOL				m_enableHttpConn;
	BOOL				m_enableXdmcpConn;

	// The desktop handler
	vncDesktop			*m_desktop;

	// General preferences
	UINT				m_port;
	UINT				m_port_http; // TightVNC 1.2.7
	BOOL				m_autoportselect;
	char				m_password[MAXPWLEN];
	BOOL				m_passwd_required;
	BOOL				m_loopback_allowed;
	BOOL				m_loopbackOnly;
	char				*m_auth_hosts;
	BOOL				m_enable_remote_inputs;
	BOOL				m_disable_local_inputs;
	int					m_lock_on_exit;
	int					m_connect_pri;
	UINT				m_querysetting;
	UINT				m_querytimeout;
	UINT				m_idle_timeout;

	BOOL				m_remove_wallpaper;
	BOOL				m_disableTrayIcon;

	// Polling preferences
	BOOL				m_poll_fullscreen;
	BOOL				m_poll_foreground;
	BOOL				m_poll_undercursor;

	BOOL				m_poll_oneventonly;
	BOOL				m_poll_consoleonly;

	BOOL				m_driver;
	BOOL				m_hook;
	BOOL				m_virtual;
	BOOL				sethook;

	// Name of this desktop
	char				*m_name;

	// Blacklist structures
	struct BlacklistEntry {
		BlacklistEntry *_next;
		char *_machineName;
		LARGE_INTEGER _lastRefTime;
		UINT _failureCount;
		BOOL _blocked;
	};
	BlacklistEntry		*m_blacklist;
	
	// The client lists - list of clients being authorised and ones
	// already authorised
	vncClientList		m_unauthClients;
	vncClientList		m_authClients;
	vncClient			*m_clientmap[MAX_CLIENTS];
	vncClientId			m_nextid;

	// Lock to protect the client list from concurrency - lock when reading/updating client list
	omni_mutex			m_clientsLock;
	// Lock to protect the desktop object from concurrency - lock when updating client list
	omni_mutex			m_desktopLock;

	// Signal set when a client removes itself
	omni_condition		*m_clientquitsig;

	// Set of windows to send notifications to
	vncNotifyList		m_notifyList;

		// Modif sf@2002 - Single Window
	BOOL    m_SingleWindow;
	// Starts IIC -- handle application sharing
	std::string			m_szWindowName; // to keep the window name
	BOOL				m_AppSharing;
	BOOL				m_EnableTransparent;
	vncProcessIDList	m_ProcessIDList;
    BOOL                m_fShareAllApplicationInstances;
	BOOL				m_SharePPT;
	HWND				m_Single_hWnd;
	HWND				m_PPT_Parent_hWnd;
	char				m_meeting_id[MAX_PATH];
	char				m_session_token[MAX_PATH];
	char				m_participant_id[MAX_PATH];
	char				m_display_name[MAX_PATH];
	// Ends IIC -- handle application sharing

	// TODO: keep a list of app window names;

	// Modif sf@2002
	BOOL    m_TurboMode;

	// Modif sf@2002 - v1.1.x
	// BOOL    m_fQueuingEnabled;
	BOOL    m_fFileTransferEnabled;
	BOOL    m_fBlankMonitorEnabled;
	int     m_nDefaultScale;

	BOOL    m_fMSLogonRequired;

	// sf@2002 - DSMPlugin
	BOOL m_fDSMPluginEnabled;
	char m_szDSMPlugin[128];
	CDSMPlugin *m_pDSMPlugin;

	char    m_namePath1[MAXPATH];
	char    m_namePath2[MAXPATH];
	char    m_namePath3[MAXPATH];
	char	 m_sucommand[MAXPATH*2];
	char	 m_masterpwd[MAXPWLEN];
	BOOL m_master;

	// sf@2002 - Cursor handling
	BOOL m_fXRichCursor; 
};

#endif
