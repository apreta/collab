//  Copyright (C) 2002 Ultr@VNC Team Members. All Rights Reserved.
//  Copyright (C) 2000-2002 Const Kaplinsky. All Rights Reserved.
//  Copyright (C) 2002 RealVNC Ltd. All Rights Reserved.
//  Copyright (C) 1999 AT&T Laboratories Cambridge. All Rights Reserved.
//
//  This file is part of the VNC system.
//
//  The VNC system is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// If the source code for the VNC system is not available from the place 
// whence you received this file, check http://www.uk.research.att.com/vnc or contact
// the authors on vnc@uk.research.att.com for information on obtaining it.


// vncDesktop object

// The vncDesktop object handles retrieval of data from the
// display buffer.  It also uses the RFBLib DLL to supply
// information on mouse movements and screen updates to
// the server

class vncDesktop;

#if !defined(_WINVNC_VNCDESKTOP)
#define _WINVNC_VNCDESKTOP
#pragma once

// Include files
#include "stdhdrs.h"

class vncServer;
#include "rfbRegion.h"
#include "rfbUpdateTracker.h"
#include "vncBuffer.h"
#include "translate.h"
#include <omnithread.h>

// Modif rdv@2002 - v1.1.x - videodriver
#include "../vdacctray/videodriver.h"

// Modif sf@2002 - v1.1.0
#include <list>
#include "textchat.h"

typedef std::list<COLORREF> RGBPixelList;   // List of RGB values (pixels)
// sf@2002 - Generates ClassName lenght warning in debug mode compile.
// typedef std::list<RGBPixelList*> GridsList; // List of Grids of pixels
typedef std::list<void*> GridsList; // List of Grids of pixels
typedef std::list<HWND> WindowsList;		// List of windows handles
typedef std::list<RECT> RECTList;			// List of Rectangles of windows


// Constants
extern const UINT RFB_SCREEN_UPDATE;
extern const UINT RFB_COPYRECT_UPDATE;
extern const UINT RFB_MOUSE_UPDATE;
extern const char szDesktopSink[];
// IIC
extern const UINT RFB_WINMSG_UPDATE;

#define NONE 0
#define MIRROR 1
#define PSEUDO 2

// IIC - capture layered windows; w2k & xp
#define CAPTUREBLT          (DWORD)0x40000000 /* Include layered windows */

// Class definition

class vncDesktop
{

// Fields
public:

// Methods
public:
	// Make the desktop thread & window proc friends
	friend class vncDesktopThread;
	friend LRESULT CALLBACK DesktopWndProc(HWND hwnd, UINT msg, WPARAM wp, LPARAM lp);

	// Create/Destroy methods
	vncDesktop();
	~vncDesktop();

	BOOL Init(vncServer *pSrv);

	// Tell the desktop hooks to grab & update a particular rectangle
	void QueueRect(const rfb::Rect &rect);
	
	// Kick the desktop hooks to perform an update
	void TriggerUpdate();

	// Get a reference to the desktop update lock
	// The lock is held while data is being grabbed and copied
	// to the back buffer, and while changes are being passed to
	// clients
	omni_mutex &GetUpdateLock() {return m_update_lock;};

	// Screen translation, capture, info
	void FillDisplayInfo(rfbServerInitMsg *scrInfo);
	COLORREF CapturePixel(int x, int y);	// IIC
	void CaptureScreen(const rfb::Rect &UpdateArea, BYTE *scrBuff, UINT scrBuffSize);
	int ScreenBuffSize();
	HWND Window() {return m_hwnd;};

	// Mouse related
	void CaptureMouse(BYTE *scrBuff, UINT scrBuffSize);
	void CaptureMousePos();
	rfb::Rect MouseRect();
	void SetCursor(HCURSOR cursor);
	// CURSOR HANDLING
	BOOL GetRichCursorData(BYTE *databuf, HCURSOR hcursor, int width, int height);
	HCURSOR GetCursor() { return m_hcursor; }

	// Clipboard manipulation
	void SetClipText(LPSTR text);

	// Method to obtain the DIBsection buffer if fast blits are enabled
	// If they're disabled, it'll return NULL
	inline VOID *OptimisedBlitBuffer() {return m_DIBbits;};

	BOOL	m_initialClipBoardSeen;

	// Handler for pixel data grabbing and region change checking
	vncBuffer		m_buffer;
		//SINGLE WINDOW
	vncServer		*GetServerPointer() {return m_server;};
	HWND			m_Single_hWnd;
	HWND			m_PPT_Parent_hWnd;	// IIC
	HWND			m_Single_hWnd_backup;
	BOOL			CalculateSWrect(RECT &rect);
	rfb::Rect		GetSize();
	rfb::Rect		GetQuarterSize();
	WindowsList		m_SharedList;	// IIC
	WindowsList		m_AllWindows;	// IIC
	RECT			m_lastPPTRect;	// IIC
	RECTList		m_AllWindowsRect;	// IIC

	// IIC - support secondary monitor
	BOOL			IsPrimaryMonitor() { return m_bmrect.tl.x == 0 && m_bmrect.tl.y == 0; }
	rfb::Rect		GetScreenRect() { return m_bmrect; }

	// Offset of captured image from desktop origin
	rfb::Point		GetOffset() { return rfb::Point(m_bmrect.tl.x, m_bmrect.tl.y); }

	// Offset of desktop origin from captured image
	rfb::Point		GetBitmapOffset() { return rfb::Point(-m_bmrect.tl.x, -m_bmrect.tl.y); }

	// Modif rdv@2002 - v1.1.x - videodriver
	//BOOL IsVideoDriverEnabled();
	BYTE *VideoBuffer();
	int m_ScreenOffsetx;
	int m_ScreenOffsety;
	int DriverType;
	DWORD color[10];
	// Modif rdv@2002 Dis/enable input
	void SetDisableInput(bool enabled);
	void SetSW(int x,int y);
	void SetUpdateTimer(BOOL updown);
	//hook selection
	BOOL m_hookdriver;
	void SethookMechanism(BOOL hookall,BOOL hookdriver);
	bool force_check_screen_format;
	bool m_UltraEncoder_used;

	// IIC	- handle app sharing
	BOOL IsWindowTransparent(HWND hWnd);
	BOOL IsSameProcess(HWND hWnd, BOOL fNoDelay);
	void EnumAllSharedAppsWindows();
	HWND GetAboveWindow(RECT& rect);
	// IIC
	POINT	m_remote_point;
	DWORD	m_remote_click_time;
	HCURSOR LoadBMPCursor(int RESID);
	HBITMAP m_hSourceBitmap;
	HCURSOR m_hCursor;

	// Implementation
protected:
	// Routines to hook and unhook us
	BOOL Startup();
	BOOL Shutdown(BOOL changesize);
	
	// Init routines called by the child thread
	BOOL InitDesktop();
	void KillScreenSaver();
	void KillWallpaper();
	void RestoreWallpaper();
	BOOL InitBitmap();
	BOOL InitWindow();
	BOOL ThunkBitmapInfo();
	BOOL SetPixFormat();
	BOOL SetPixShifts();
	BOOL InitHooks();
	BOOL SetPalette();
	int m_timer;

	// IIC - sharing section of desktop corresponding with a particular display
	static BOOL CALLBACK MonitorEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData);
	void InitForDisplay();

	// Fetching pixel data to a buffer, and handling copyrects
	void CopyToBuffer(const rfb::Rect &rect, BYTE *scrBuff, UINT scrBuffSize);
	bool CalcCopyRects(rfb::UpdateTracker &tracker);

	// Routine to attempt enabling optimised DIBsection blits
	void EnableOptimisedBlits();

	// Convert a bit mask eg. 00111000 to max=7, shift=3
	static void MaskToMaxAndShift(DWORD mask, CARD16 &max, CARD8 &shift);
	
	// Enabling & disabling clipboard handling
	void SetClipboardActive(BOOL active) {m_clipboard_active = active;};

	// Modif sf@2002 - v1.1.0 - FastDetectChanges stuff
	void FastDetectChanges(rfb::Region2D &rgn, rfb::Rect &rect, int nZone, bool fTurbo);
	GridsList    m_lGridsList;   // List of changes detection grids
	WindowsList  m_lWList;		 // List of Windows handles  
	// HDC	         m_hDC;			 // Local Screen Device context to capture our Grid of pixels 
	int          m_nGridCycle;   // Cycle index for grid shifting

	// Modif sf@2002 - v1.1.0
	long         m_lLastMouseUpdateTime;
	long         m_lLastSlowClientTestTime;
	// long			m_lLastTempo;

	// sf@2002 - TextChat - No more used for now
	// bool m_fTextChatRunning;
	// TextChat* m_pCurrentTextChat;

	// DATA

	// Generally useful stuff
	vncServer 		*m_server;
	omni_thread 	*m_thread;
	HWND			m_hwnd;
	UINT			m_timerid;
	HWND			m_hnextviewer;
	BOOL			m_clipboard_active;

	// device contexts for memory and the screen
	HDC				m_hmemdc;
	HDC				m_hrootdc;

	// New and old bitmaps
	HBITMAP			m_membitmap;
	omni_mutex		m_update_lock;

	rfb::Rect		m_bmrect;
	rfb::Rect		m_Cliprect;//the region to check
	struct _BMInfo {
		BOOL			truecolour;
		BITMAPINFO		bmi;
		// Colormap info - comes straight after BITMAPINFO - **HACK**
		RGBQUAD			cmap[256];
	} m_bminfo;

	// Screen info
	rfbServerInitMsg	m_scrinfo;

	// These are the red, green & blue masks for a pixel
	DWORD			m_rMask, m_gMask, m_bMask;

	// This is always handy to have
	int				m_bytesPerRow;

	// Handle of the default cursor
	HCURSOR			m_hcursor;
	HCURSOR			m_hOldcursor; // sf@2002

	// Handle of the basic arrow cursor
	HCURSOR			m_hdefcursor;
	// The current mouse position
	rfb::Rect		m_cursorpos;

	// Boolean flag to indicate when the display resolution has changed
	BOOL			m_displaychanged;
	BOOL			m_driverchanged;

	// Boolean flag to indicate whether or not an update trigger message
	// is already in the desktop thread message queue
	BOOL			m_update_triggered;

	// Extra vars used for the DIBsection optimisation
	VOID			*m_DIBbits;
	BOOL			m_formatmunged;

	// Info used for polling modes
	rfb::Rect		m_qtrscreen;
	UINT			m_pollingcycle;
	// rfb::Rect		m_fullscreen; // sf@2002 - v1.1.0

	// Handling of the foreground window, to produce CopyRects
	HWND			m_foreground_window;
	rfb::Rect		m_foreground_window_rect;

	HWND			m_foreground_window2;	// IIC -- handle app share artifacts

	//SINGLE WINDOW
	void CheckSWOleacc();
	BOOL m_OleEnable;
	void SWinit();
	int m_SWHeight;
	int m_SWWidth;
	BOOL m_SWSizeChanged;
	BOOL m_SWmoved;
	BOOL m_SWtoDesktop;
	int m_SWOffsetx;
	int m_SWOffsety;

	//DDIHOOK
	PCOPYDATASTRUCT pMyCDS;

	// Modif rdv@2002 - v1.1.x - videodriver
	vncVideoDriver *m_videodriver;
	BOOL InitVideoDriver();
 	void ShutdownVideoDriver(BOOL changesize);
	void ActivatePseudoDesktop();
	HANDLE  hAppliProcess;
	omni_mutex		m_videodriver_lock;

	// Modif input dis/enabke
	DWORD m_thread_hooks;
	BOOL ddihook;
	UINT OldPowerOffTimeout;
	//hook selection
	BOOL m_hookdll;
	BOOL On_Off_hookdll;
	BOOL m_hookswitch;
	BOOL Hookdll_Changed;
	BOOL m_hookinited;
	HANDLE m_hddihook;
	void StartStopddihook(BOOL enabled);
	void StartStophookdll(BOOL enabled);
	void InitHookSettings();
	// IIC	- handle app sharing
	BOOL CaptureSharedAppsWindows(const rfb::Rect &rect, BYTE *scrBuff, UINT scrBuffSize);
	HWND HideOverlappedWindows(HWND hWnd, const rfb::Rect &rect, BYTE *scrBuff, UINT scrBuffSize);
	BOOL CapturePPTWindow(const rfb::Rect& rect, BYTE *scrBuff, UINT scrBuffSize);
/*	BOOL Check24bit();*/

    // IIC
    BOOL GetProcName(DWORD aProcID, char* aProcName, int aSize);
};

#endif // _WINVNC_VNCDESKTOP
