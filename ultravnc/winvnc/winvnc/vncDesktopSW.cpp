/////////////////////////////////////////////////////////////////////////////
//  Copyright (C) 2002 Ultr@VNC Team Members. All Rights Reserved.
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// If the source code for the program is not available from the place from
// which you received this file, check 
// http://ultravnc.sourceforge.net/


// System headers
#include <assert.h>
#include "stdhdrs.h"

// Custom headers
#include <omnithread.h>
#include "WinVNC.h"
#include "VNCHooks\VNCHooks.h"
#include "vncServer.h"
#include "rfbRegion.h"
#include "rfbRect.h"
#include "vncDesktop.h"
#include "vncService.h"
#include "psapi.h"
#include "../../netlib/log.h"	// IIC

#include "directx.h"

typedef struct _importexport
{
	POINT point;
	RECT rect;
	char text[128];
	char text2[128];
} importexport, *pimportexport;

//msaa removed crash debug
//fnPointlib(pimportexport myexport);

BOOL vncDesktop:: CalculateSWrect(RECT &rect)
{
	rect.top=0;
	rect.left=0;
	rect.right=0;
	rect.bottom=0;

	if (!m_Single_hWnd)		// IIC -- PPT window handle is not set yet, not to send any updates
		return TRUE;

	if (IsIconic(m_Single_hWnd))
		return TRUE;

	if (IsWindowVisible(m_Single_hWnd) && GetWindowRect(m_Single_hWnd,&rect)) 
	{
		long style = GetWindowLong(m_Single_hWnd, GWL_STYLE);
		if (style & WS_VSCROLL)
			rect.right -= GetSystemMetrics (SM_CXVSCROLL);
		if (style & WS_HSCROLL)
			rect.bottom -= GetSystemMetrics (SM_CYHSCROLL);

		MONITORINFO info = { sizeof(MONITORINFO) };
		HMONITOR hMon = MonitorFromWindow(m_Single_hWnd, MONITOR_DEFAULTTOPRIMARY);
		GetMonitorInfo(hMon, &info);
		RECT workArea = info.rcWork;

		rect.top=Max(rect.top,workArea.top);
		rect.left=Max(rect.left,workArea.left);
		rect.right=Min(rect.right,workArea.right);
		rect.bottom=Min(rect.bottom,workArea.bottom);
		/*if (rect.right < 0 || rect.bottom < 0)	// the powerpoint window size is invalid when it minimized
		{
			rect.left = rect.right = rect.top = rect.bottom = 0;
			return TRUE;
		}*/

		if ((m_SWHeight!=(rect.bottom-rect.top)) || (m_SWWidth!=(rect.right-rect.left)))
			m_SWSizeChanged=TRUE;

		m_SWHeight=rect.bottom-rect.top;
		m_SWWidth=rect.right-rect.left;

		debugf("Single window capture: %d,%d - %d,%d\n", rect.left, rect.top, rect.right, rect.bottom);
	}
	else
	{
		m_server->EndSession();
		return FALSE;
	}

	return TRUE;
}

void vncDesktop::CheckSWOleacc()
{
	HMODULE  hModule = LoadLibrary(("oleacc.dll"));
	if (!hModule)
	{
		m_OleEnable=FALSE;
	}
	else
	{
		FreeLibrary(hModule);
		m_OleEnable=TRUE;
	}
}

void vncDesktop::SWinit()
{
	m_OleEnable=FALSE;
	CheckSWOleacc();
	m_Single_hWnd=NULL;
	m_Single_hWnd_backup=NULL;
	m_SWHeight=0;
	m_SWWidth=0;
	m_SWSizeChanged=FALSE;
	m_SWmoved=FALSE;
	m_SWOffsetx=0;
	m_SWOffsety=0;
}

rfb::Rect
vncDesktop::GetSize()
{
	if (m_server->SingleWindow())
	{
		RECT rect;
		rect.left=m_Cliprect.tl.x;
		rect.top=m_Cliprect.tl.y;
		rect.right=m_Cliprect.br.x;
		rect.bottom=m_Cliprect.br.y;
		return rfb::Rect(0, 0, rect.right - rect.left, rect.bottom - rect.top);
	}
	else
	{
		m_SWOffsetx=0;
		m_SWOffsety=0;
		return rfb::Rect(0, 0, m_scrinfo.framebufferWidth, m_scrinfo.framebufferHeight);
	}
}

rfb::Rect
vncDesktop::GetQuarterSize()
{
	if (m_server->SingleWindow())
	{
		RECT rect;
		if (CalculateSWrect(rect))
		{
			rect.left=rect.left-m_ScreenOffsetx;
			rect.right=rect.right-m_ScreenOffsetx;
			rect.bottom=rect.bottom-m_ScreenOffsety;
			rect.top=rect.top-m_ScreenOffsety;
			if (m_SWOffsetx!=rect.left || m_SWOffsety!=rect.top) m_SWmoved=TRUE;
			m_SWOffsetx=rect.left;
			m_SWOffsety=rect.top;
			m_Cliprect.tl.x=rect.left;
			m_Cliprect.tl.y=rect.top;
			m_Cliprect.br.x=rect.right;
			m_Cliprect.br.y=rect.bottom;
			// XXX
			m_bmrect = m_Cliprect;	
			return rfb::Rect(rect.left, rect.bottom, rect.right-rect.left, (rect.bottom-rect.top)/4);
		}
		else
		{
			m_SWOffsetx=0;
			m_SWOffsety=0;
			m_Cliprect.tl.x=0;
			m_Cliprect.tl.y=0;
			m_Cliprect.br.x=0;	// IIC - PPT window maybe closed by user, avoid sending updates outside of its window size, which might cause recorder to fail. m_bmrect.br.x;
			m_Cliprect.br.y=0;	// IIC - m_bmrect.br.y;
			return rfb::Rect(0, 0, m_bmrect.br.x, m_bmrect.br.y/4);

		}
	}
	else
	{ 
		m_SWOffsetx=0;
		m_SWOffsety=0;
		// IIC - cliprect may be on secondary display
		m_Cliprect.tl.x=m_bmrect.tl.x;
		m_Cliprect.tl.y=m_bmrect.tl.y;
		m_Cliprect.br.x=m_bmrect.br.x;
		m_Cliprect.br.y=m_bmrect.br.y;
		return rfb::Rect(0, 0, m_bmrect.br.x, m_bmrect.br.y/4);
	}
}

static HANDLE hPSAPILibrary;
typedef DWORD (WINAPI * GET_MOD_BASENAME)( HANDLE hProcess, HMODULE hModule, LPSTR lpBaseName, DWORD nSize); 
static GET_MOD_BASENAME pGetModuleBaseName = NULL; 
typedef BOOL (WINAPI * ENUM_PROC_MODULES)( HANDLE hProcess, HMODULE * lphModule, DWORD cb, LPDWORD lpcbNeeded); 
static ENUM_PROC_MODULES pEnumProcModules = NULL; 

BOOL vncDesktop::GetProcName(DWORD aProcID, char* aProcName, int aSize)
{
    if (hPSAPILibrary==NULL)
    {
        hPSAPILibrary = LoadLibrary("PSAPI.DLL");
        if (hPSAPILibrary)
        {
            pGetModuleBaseName = (GET_MOD_BASENAME)GetProcAddress((HINSTANCE)hPSAPILibrary, "GetModuleBaseNameA");
            pEnumProcModules = (ENUM_PROC_MODULES)GetProcAddress((HINSTANCE)hPSAPILibrary, "EnumProcessModules");
        }
    }

    if (pGetModuleBaseName && pEnumProcModules)
    {
        HANDLE hProc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, aProcID);
        if (hProc)
        {
            HMODULE hMod; 
            DWORD cbNeeded; 
            if (pEnumProcModules( hProc, &hMod, sizeof(hMod), &cbNeeded)) 
            {
                pGetModuleBaseName(hProc, hMod, aProcName, aSize);
                CloseHandle(hProc);

                return TRUE;
            }
        }
    }

    return FALSE;
}

//------ Starts IIC - handle application sharing ------//
BOOL vncDesktop::IsSameProcess(HWND hWnd, BOOL fNoDelay)
{
	static DWORD dwTickCount = 0;
	static HWND hwnd = NULL;
	static BOOL bReturn = FALSE;

	if (!fNoDelay && hWnd==hwnd && (GetTickCount() - dwTickCount) < 1000)
	{
		return bReturn;
	}

	dwTickCount = GetTickCount();
	hwnd = hWnd;

    char procName1[MAX_PATH+1], procName2[MAX_PATH+1];
	DWORD dwProcID;
	GetWindowThreadProcessId(hWnd, &dwProcID);

	if (m_server->IsSharingPPT())
	{
		DWORD dwParentProcID;
		GetWindowThreadProcessId(m_PPT_Parent_hWnd, &dwParentProcID);

		HWND hParent = GetAncestor(hWnd, GA_ROOTOWNER);
		if (hWnd == m_Single_hWnd || hParent==m_Single_hWnd ||
			hWnd == m_PPT_Parent_hWnd || hParent==m_PPT_Parent_hWnd ||
			dwProcID == dwParentProcID)
		{
			return (bReturn = TRUE);
		}
		else
		{
			return (bReturn = FALSE);
		}
	}

    if (m_server->ShareAllApplicationInstances())
    {
        memset(procName1, 0, MAX_PATH+1);
        GetProcName(dwProcID, procName1, MAX_PATH);
    }

	vncProcessIDList* list = m_server->GetProcessIDList();
	vncProcessIDList::iterator i;
	for (i=list->begin(); i!=list->end(); i++)
	{
		if (dwProcID == *i)
		{
			return (bReturn = TRUE);
		}

        if (m_server->ShareAllApplicationInstances() && (fNoDelay || (GetTickCount()-dwTickCount)>1000))	// improve some performance
        {
            memset(procName2, 0, MAX_PATH+1);
            if (GetProcName(*i, procName2, MAX_PATH))
            {
                if (strcmp(procName1, procName2)==0)
                {
                    // add the proc id to the list to speed up
                    list->push_back(dwProcID);
                    return (bReturn = TRUE);
                }
            }
        }
	}

	return (bReturn = FALSE);
}

HWND vncDesktop::GetAboveWindow(RECT& rect)
{
	static DWORD dwTickCount = 0;
	static HWND hwnd = NULL;

	if ((GetTickCount() - dwTickCount) < 10)
	{
		dwTickCount = GetTickCount();
		return hwnd;
	}


	dwTickCount = GetTickCount();

	hwnd = m_Single_hWnd;

	POINT point;
	for (point.x=rect.left+1; point.x<rect.right; point.x+=10)
	{
		for (point.y=rect.top+1; point.y<rect.bottom; point.y+=10)
		{
			HWND h = WindowFromPoint(point);
			if (h != m_Single_hWnd && GetParent(h) != m_Single_hWnd && !IsSameProcess(h, FALSE))
			{
				hwnd = h;
				break;
			}
		}
	}

	return hwnd;
}

BOOL CALLBACK EnumWndProc(HWND hwnd, LPARAM lParam)
{
	vncDesktop* desktop = (vncDesktop*) lParam;
	if (IsWindowVisible(hwnd))
	{
		desktop->m_AllWindows.push_front(hwnd);
		RECT rect;
		GetWindowRect(hwnd, &rect);
		desktop->m_AllWindowsRect.push_front(rect);
	}

    return TRUE;
}

BOOL vncDesktop::CaptureSharedAppsWindows(const rfb::Rect &rect,  BYTE *scrBuff, UINT scrBuffSize)
{
	BOOL blitok = TRUE;
	rfb::Rect intersect;
	
//	if (m_SharedList.size()==0) return FALSE;

	rfb::Rect clipped = rect.intersect(m_bmrect);
	RECT rc;
	rc.left		= clipped.tl.x - m_bmrect.tl.x;
	rc.top		= clipped.tl.y - m_bmrect.tl.y;
	rc.right	= clipped.br.x - m_bmrect.tl.x;
	rc.bottom	= clipped.br.y - m_bmrect.tl.y;

	if (UsingDirectX())	{
		BlankRect(rc, 0, scrBuff, scrBuffSize, m_bytesPerRow);
	} else {
		FillRect(m_hmemdc, &rc, (HBRUSH)GetStockObject(BLACK_BRUSH));
	}

	RECT rectPrev;
	RECT lastRect = {0,0,0,0};

	m_AllWindows.clear();
	m_AllWindowsRect.clear();
	::EnumWindows(EnumWndProc, (LPARAM)this);

	while (true)
	{
		HWND hPrevWnd;
		WindowsList::iterator i;
		RECTList::iterator j;
		WindowsList	m_Saved;
		RECTList m_SavedRect;

		for (i=m_AllWindows.begin(),j=m_AllWindowsRect.begin(); i!=m_AllWindows.end(); i++, j++)	
		{
			m_Saved.push_back(*i);
			m_SavedRect.push_back(*j);

			hPrevWnd = *i;

			if (GetWindowRect(hPrevWnd, &rectPrev)==TRUE) 
			{
				if (IsSameProcess(hPrevWnd, FALSE)==TRUE) 
				{
		//			if (rfb::Rect(rectPrev).enclosed_by(lastRect)==FALSE ||
		//				(m_server->IsTransparentEnabled()&&IsWindowTransparent(hPrevWnd)))	// tooltip
					{
						intersect = rect.intersect(rectPrev);
						rfb::Rect clipped = intersect.intersect(m_bmrect);
						if (clipped.is_empty()==FALSE)
						{
							int nFlags = SRCCOPY;
							if (m_server->IsTransparentEnabled()==FALSE)//||IsWindowTransparent(hPrevWnd))	// either sending transparent windows, if shared app window is transparent, won't show it, b/c it may cause artifacts
							{
								nFlags |= CAPTUREBLT;
							}
							if ((nFlags & CAPTUREBLT)!=0 || IsWindowTransparent(hPrevWnd)==FALSE) {
								int destx = clipped.tl.x - m_bmrect.tl.x;
								int desty = clipped.tl.y - m_bmrect.tl.y;

								if (UsingDirectX()) 
								{
									RECT rect = { clipped.tl.x, clipped.tl.y, clipped.br.x, clipped.br.y };
									HRESULT stat = CaptureRectangle(rect, scrBuff, scrBuffSize, m_bytesPerRow); 
								} 
								else
								{
									debugf("Blitting %d,%d - %d,%d to %d,%d\n", clipped.tl.x, clipped.tl.y, clipped.br.x, clipped.br.y, destx, desty);
									blitok = BitBlt(m_hmemdc, destx, desty,
										(clipped.br.x-clipped.tl.x),
										(clipped.br.y-clipped.tl.y),
										m_hrootdc, clipped.tl.x, clipped.tl.y, nFlags);
								}
							}
						}
					}
					lastRect = rectPrev;
				}			
				else if (IsWindowTransparent(hPrevWnd)==FALSE)
				{
					intersect = rfb::Rect(rectPrev).intersect(rect);
					rfb::Rect clipped = intersect.intersect(m_bmrect);
					if (clipped.is_empty()==FALSE)	// reset overlapped buffer
					{
						RECT rc;
						rc.left		= clipped.tl.x - m_bmrect.tl.x;
						rc.top		= clipped.tl.y - m_bmrect.tl.y;
						rc.right	= clipped.br.x - m_bmrect.tl.x;
						rc.bottom	= clipped.br.y - m_bmrect.tl.y;
						
						if (UsingDirectX()) {
							BlankRect(rc, 0, scrBuff, scrBuffSize, m_bytesPerRow);
						} else {
							FillRect(m_hmemdc, &rc, (HBRUSH)GetStockObject(BLACK_BRUSH));
						}
					}
				}
			}
		}

		m_AllWindows.clear();
		m_AllWindowsRect.clear();
		::EnumWindows(EnumWndProc, (LPARAM)this);

		if (m_AllWindows.size() == m_Saved.size())
		{
			WindowsList::iterator m;
			RECTList::iterator n;
			for (i=m_AllWindows.begin(),m=m_Saved.begin(),j=m_AllWindowsRect.begin(),n=m_SavedRect.begin();
			     i!=m_AllWindows.end(); i++, j++, m++, n++)	
			{
				if (*m != *i) break;
				if ((*n).left != (*j).left || 
					(*n).top != (*j).top ||
					(*n).right != (*j).right ||
					(*n).bottom != (*j).bottom) 
					break;
			}

			// not changed
			if (i==m_AllWindows.end())
				break;
		}
	}

	return blitok;
}

// IIC -- detect transparent window
#define WS_EX_LAYERED	0x00080000
BOOL vncDesktop::IsWindowTransparent(HWND hWnd)
{
	HWND hParent = GetAncestor(hWnd, GA_ROOTOWNER);
	DWORD dwExStyle = GetWindowLong(hParent, GWL_EXSTYLE);

	if (dwExStyle & WS_EX_LAYERED)
		return TRUE;

	if (hParent != hWnd)
	{
		dwExStyle = GetWindowLong(hWnd, GWL_EXSTYLE);

		if (dwExStyle & WS_EX_LAYERED)
			return TRUE;
	}

	return FALSE;
}

HWND vncDesktop::HideOverlappedWindows(HWND hWnd, const rfb::Rect& rect, BYTE *scrBuff, UINT scrBuffSize)
{
	HWND hPrevWnd = hWnd;
	
	rfb::Rect intersect;	

	while (hPrevWnd!=NULL)
	{
		RECT rectPrev;
		hPrevWnd = GetNextWindow(hPrevWnd, GW_HWNDPREV);
//		if (hPrevWnd!=NULL && IsSameProcess(hPrevWnd)==TRUE)
//			break;

		if (IsWindowVisible(hPrevWnd)==TRUE && 
			IsSameProcess(hPrevWnd, FALSE)==FALSE && 
			IsWindowTransparent(hPrevWnd)==FALSE &&
			GetWindowRect(hPrevWnd, &rectPrev)==TRUE) 
		{
			intersect = rfb::Rect(rectPrev).intersect(rect);
			rfb::Rect clipped = intersect.intersect(m_Cliprect);
			if (clipped.is_empty()==FALSE)	// reset overlapped buffer
			{
				RECT rc;
				rc.left		= clipped.tl.x - m_Cliprect.tl.x;
				rc.top		= clipped.tl.y - m_Cliprect.tl.y;
				rc.right	= clipped.br.x - m_Cliprect.tl.x;
				rc.bottom	= clipped.br.y - m_Cliprect.tl.y;

				if (UsingDirectX()) {
					BlankRect(rc, 0, scrBuff, scrBuffSize, m_bytesPerRow);
				} else {
					FillRect(m_hmemdc, &rc, (HBRUSH)GetStockObject(BLACK_BRUSH));
				}
			}
		}		
	}

	return hPrevWnd;
}

BOOL vncDesktop::CapturePPTWindow(const rfb::Rect& rect, BYTE *scrBuff, UINT scrBuffSize)
{
	bool tryagain = true;
	BOOL blitok = true;
	int nFlags = SRCCOPY;
	
	if (m_server->IsTransparentEnabled()==FALSE)// ||m_server->SingleWindow())
	{
		nFlags |= CAPTUREBLT;
	}

	rfb::Rect clipped = rect.intersect(m_Cliprect);
	int destx = clipped.tl.x - m_Cliprect.tl.x;
	int desty = clipped.tl.y - m_Cliprect.tl.y;

	if (UsingDirectX()) {
		RECT rect = { clipped.tl.x, clipped.tl.y, clipped.br.x, clipped.br.y };
		HRESULT stat = CaptureRectangle(rect, scrBuff, scrBuffSize, m_bytesPerRow, -m_Cliprect.tl.x, -m_Cliprect.tl.y);
	} else {
		debugf("Blitting %d,%d - %d,%d to %d,%d\n", clipped.tl.x, clipped.tl.y, clipped.br.x, clipped.br.y, destx, desty);
		blitok = BitBlt(m_hmemdc, destx, desty,
			(clipped.br.x-clipped.tl.x),
			(clipped.br.y-clipped.tl.y),
			m_hrootdc, clipped.tl.x, clipped.tl.y, nFlags);
	}

	HideOverlappedWindows(m_PPT_Parent_hWnd, rect, scrBuff, scrBuffSize);

	return TRUE;
}
//------ Ends IIC - handle application sharing ------//
