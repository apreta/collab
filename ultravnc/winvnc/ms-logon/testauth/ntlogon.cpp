#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

typedef BOOL (*CheckUserGroupPasswordFn)( char * userin,char *password);
CheckUserGroupPasswordFn CheckUserGroupPassword = 0;
bool CheckAD()
{
	HMODULE hModule = LoadLibrary("Activeds.dll");
	if (hModule)
	{
		FreeLibrary(hModule);
		return true;
	}
	return false;
}

bool CheckNetapi95()
{
	HMODULE hModule = LoadLibrary("netapi32.dll");
	if (hModule)
	{
		FreeLibrary(hModule);
		return true;
	}
	return false;
}

bool CheckNetApiNT()
{
	HMODULE hModule = LoadLibrary("radmin32.dll");
	if (hModule)
	{
		FreeLibrary(hModule);
		return true;
	}
	return false;
}

int main(int argc, char* argv[])
{
	if (argc<3) 
	{
		printf("usage: testauth.exe user passwd");
		Sleep(5000);
		exit(0);
	}
	if (!CheckNetapi95() && !CheckNetApiNT())
	{
		printf("Netapi not found,radmin32.dll is missing \n");
	}
	if (CheckAD())
	{
		HMODULE hModule = LoadLibrary("authad.dll");
		if (hModule)
			{
				CheckUserGroupPassword = (CheckUserGroupPasswordFn) GetProcAddress( hModule, "CUGP" );
				HRESULT hr = CoInitialize(NULL);
				CheckUserGroupPassword(argv[1],argv[2]);
				CoUninitialize();
				FreeLibrary(hModule);
			}
		else printf("authad.dll not found");
	}
	else
	{
		printf("AD not detected");
		HMODULE hModule = LoadLibrary("auth.dll");
		if (hModule)
			{
				CheckUserGroupPassword = (CheckUserGroupPasswordFn) GetProcAddress( hModule, "CUGP" );
				HRESULT hr = CoInitialize(NULL);
				CheckUserGroupPassword(argv[1],argv[2]);
				CoUninitialize();
				FreeLibrary(hModule);
			}
		else printf("auth.dll not found");
	}
	Sleep(5000);
	return 0;
}
