/////////////////////////////////////////////////////////////////////////////
//  Copyright (C) 2002 Ultr@VNC Team Members. All Rights Reserved.
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// If the source code for the program is not available from the place from
// which you received this file, check 
// http://ultravnc.sourceforge.net/
// /macine-vnc Greg Wood (wood@agressiv.com)
#pragma comment( lib, "netapi32.lib" )
#if defined( UNICODE ) || defined( _UNICODE )
#error Sorry -- please compile as an ANSI program.
#endif
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <tchar.h>
#include <stdio.h>
#include <lm.h>
#include <iads.h>
#include <Adshlp.h>
#include "authad.h"
#define SECURITY_WIN32
#include <sspi.h>
#ifndef SEC_I_COMPLETE_NEEDED
#include <issperr.h>
#endif
#pragma hdrstop

#define MAXLEN 256
#define MAX_PREFERRED_LENGTH    ((DWORD) -1)
#define NERR_Success            0
//#define LG_INCLUDE_INDIRECT 1
#define BUFSIZE 1024

typedef DWORD (__stdcall *NetApiBufferFree_t)( void *buf );
typedef DWORD (__stdcall *NetGetDCNameNT_t)( wchar_t *server, wchar_t *domain, byte **buf );
typedef DWORD (__stdcall *NetGetDCName95_t)( char *domain, byte **buf );
typedef DWORD (__stdcall *NetUserGetGroupsNT_t)( wchar_t *server,wchar_t *user, DWORD level,DWORD flags, byte **buf,DWORD prefmaxlen, DWORD *entriesread, DWORD *totalentries);
typedef DWORD (__stdcall *NetUserGetGroups95_t)( char *server,char *user, DWORD level, byte **buf,DWORD prefmaxlen, DWORD *entriesread, DWORD *totalentries);
typedef DWORD (__stdcall *NetWkstaGetInfoNT_t)( wchar_t *server, DWORD level, byte **buf );
typedef DWORD (__stdcall *NetWkstaGetInfo95_t)( char *domain,DWORD level, byte **buf );
typedef struct _LPLOCALGROUP_USERS_INFO_0_NT
{
	wchar_t *grui0_name;
}LPLOCALGROUP_USERS_INFO_0_NT;
typedef struct _LPLOCALGROUP_USERS_INFO_0_95
{
	char *grui0_name;
}LPLOCALGROUP_USERS_INFO_0_95;
typedef struct _WKSTA_INFO_100_95 {
  DWORD     wki100_platform_id;
  char *    wki100_computername;
  char *    wki100_langroup;
  DWORD     wki100_ver_major;
  DWORD     wki100_ver_minor;
}WKSTA_INFO_100_95;
typedef struct _WKSTA_INFO_100_NT {
  DWORD     wki100_platform_id;
  wchar_t *    wki100_computername;
  wchar_t *    wki100_langroup;
  DWORD     wki100_ver_major;
  DWORD     wki100_ver_minor;
}WKSTA_INFO_100_NT;
typedef struct _AUTH_SEQ {
   BOOL fInitialized;
   BOOL fHaveCredHandle;
   BOOL fHaveCtxtHandle;
   CredHandle hcred;
   struct _SecHandle hctxt;
} AUTH_SEQ, *PAUTH_SEQ;


BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}




// Function pointers
ACCEPT_SECURITY_CONTEXT_FN       _AcceptSecurityContext     = NULL;
ACQUIRE_CREDENTIALS_HANDLE_FN    _AcquireCredentialsHandle  = NULL;
COMPLETE_AUTH_TOKEN_FN           _CompleteAuthToken         = NULL;
DELETE_SECURITY_CONTEXT_FN       _DeleteSecurityContext     = NULL;
FREE_CONTEXT_BUFFER_FN           _FreeContextBuffer         = NULL;
FREE_CREDENTIALS_HANDLE_FN       _FreeCredentialsHandle     = NULL;
INITIALIZE_SECURITY_CONTEXT_FN   _InitializeSecurityContext = NULL;
QUERY_SECURITY_PACKAGE_INFO_FN   _QuerySecurityPackageInfo  = NULL;

bool ad_access=false;


/////////////////////////////////////////////////////////////////////////////// 


void UnloadSecurityDll(HMODULE hModule) {

   if (hModule)
      FreeLibrary(hModule);

   _AcceptSecurityContext      = NULL;
   _AcquireCredentialsHandle   = NULL;
   _CompleteAuthToken          = NULL;
   _DeleteSecurityContext      = NULL;
   _FreeContextBuffer          = NULL;
   _FreeCredentialsHandle      = NULL;
   _InitializeSecurityContext  = NULL;
   _QuerySecurityPackageInfo   = NULL;
}


/////////////////////////////////////////////////////////////////////////////// 
//////////////////////////////////////////////////////////////////////

EventLogging::EventLogging()
{
	// returns a handle that links the source to the registry 
	m_hEventLinker = RegisterEventSource(NULL,"UltraVnc");

}

EventLogging::~EventLogging()
{
	// Releases the handle to the registry
	DeregisterEventSource(m_hEventLinker);
}



void EventLogging::LogIt(WORD CategoryID, DWORD EventID, LPCTSTR *ArrayOfStrings,
						 UINT NumOfArrayStr,LPVOID RawData,DWORD RawDataSize)
{

	// Writes data to the event log
	ReportEvent(m_hEventLinker,EVENTLOG_INFORMATION_TYPE,CategoryID,
		EventID,NULL,1,RawDataSize,ArrayOfStrings,RawData);	

}


void EventLogging::AddEventSourceToRegistry(LPCTSTR lpszSourceName)
{
    HKEY  hk;
    DWORD dwData;
    TCHAR szBuf[MAX_PATH];
    TCHAR szKey[255] =_T("SYSTEM\\CurrentControlSet\\Services\\EventLog\\Application\\");
    TCHAR szServicePath[MAX_PATH];

    lstrcat(szKey, _T("UltraVnc"));

    if(RegCreateKey(HKEY_LOCAL_MACHINE, szKey, &hk) != ERROR_SUCCESS)
    {
        return;
    }

		if (GetModuleFileName(NULL, szServicePath, MAX_PATH))
		{
			char* p = strrchr(szServicePath, '\\');
			if (p == NULL) return;
			*p = '\0';
			strcat (szServicePath,"\\logmessages.dll");
		}
	printf(szServicePath);
    lstrcpy(szBuf, szServicePath);

    // Add the name to the EventMessageFile subkey.
    if(RegSetValueEx(hk,
                     _T("EventMessageFile"),
                     0,
                     REG_EXPAND_SZ,
                     (LPBYTE) szBuf,
                     (lstrlen(szBuf) + 1) * sizeof(TCHAR)) != ERROR_SUCCESS)
    {
        RegCloseKey(hk);
        return;
    }

    dwData = EVENTLOG_ERROR_TYPE | EVENTLOG_WARNING_TYPE |EVENTLOG_INFORMATION_TYPE;
    if(RegSetValueEx(hk,
                     _T("TypesSupported"),
                     0,
                     REG_DWORD,
                     (LPBYTE)&dwData,
                     sizeof(DWORD)) != ERROR_SUCCESS)
    {
        
    } RegCloseKey(hk);
}
////////////////////////////////////////////////


HMODULE LoadSecurityDll() {

   HMODULE hModule;
   BOOL    fAllFunctionsLoaded = FALSE; 
   TCHAR   lpszDLL[MAX_PATH];
   OSVERSIONINFO VerInfo;

   // 
   //  Find out which security DLL to use, depending on
   //  whether we are on NT or Win95 or 2000 or XP or .NET Server
   //  We have to use security.dll on Windows NT 4.0.
   //  All other operating systems, we have to use Secur32.dll
   // 
   VerInfo.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
   if (!GetVersionEx (&VerInfo))   // If this fails, something has gone wrong
   {
      return FALSE;
   }

   if (VerInfo.dwPlatformId == VER_PLATFORM_WIN32_NT &&
      VerInfo.dwMajorVersion == 4 &&
      VerInfo.dwMinorVersion == 0)
   {
      lstrcpy (lpszDLL, _T("security.dll"));
   }
   else
   {
      lstrcpy (lpszDLL, _T("secur32.dll"));
   }


   hModule = LoadLibrary(lpszDLL);
   if (!hModule)
      return NULL;

   __try {

      _AcceptSecurityContext = (ACCEPT_SECURITY_CONTEXT_FN) 
            GetProcAddress(hModule, "AcceptSecurityContext");
      if (!_AcceptSecurityContext)
         __leave;

#ifdef UNICODE
      _AcquireCredentialsHandle = (ACQUIRE_CREDENTIALS_HANDLE_FN)
            GetProcAddress(hModule, "AcquireCredentialsHandleW");
#else
      _AcquireCredentialsHandle = (ACQUIRE_CREDENTIALS_HANDLE_FN)
            GetProcAddress(hModule, "AcquireCredentialsHandleA");
#endif
      if (!_AcquireCredentialsHandle)
         __leave;

      // CompleteAuthToken is not present on Windows 9x Secur32.dll
      // Do not check for the availablity of the function if it is NULL;
      _CompleteAuthToken = (COMPLETE_AUTH_TOKEN_FN) 
            GetProcAddress(hModule, "CompleteAuthToken");

      _DeleteSecurityContext = (DELETE_SECURITY_CONTEXT_FN) 
            GetProcAddress(hModule, "DeleteSecurityContext");
      if (!_DeleteSecurityContext)
         __leave;

      _FreeContextBuffer = (FREE_CONTEXT_BUFFER_FN) 
            GetProcAddress(hModule, "FreeContextBuffer");
      if (!_FreeContextBuffer)
         __leave;

      _FreeCredentialsHandle = (FREE_CREDENTIALS_HANDLE_FN) 
            GetProcAddress(hModule, "FreeCredentialsHandle");
      if (!_FreeCredentialsHandle)
         __leave;

#ifdef UNICODE
      _InitializeSecurityContext = (INITIALIZE_SECURITY_CONTEXT_FN)
            GetProcAddress(hModule, "InitializeSecurityContextW");
#else
      _InitializeSecurityContext = (INITIALIZE_SECURITY_CONTEXT_FN) 
            GetProcAddress(hModule, "InitializeSecurityContextA");
#endif
      if (!_InitializeSecurityContext)
         __leave;

#ifdef UNICODE
      _QuerySecurityPackageInfo = (QUERY_SECURITY_PACKAGE_INFO_FN)
            GetProcAddress(hModule, "QuerySecurityPackageInfoW");
#else
      _QuerySecurityPackageInfo = (QUERY_SECURITY_PACKAGE_INFO_FN)
            GetProcAddress(hModule, "QuerySecurityPackageInfoA");
#endif
      if (!_QuerySecurityPackageInfo)
         __leave;

      fAllFunctionsLoaded = TRUE;

   } __finally {

      if (!fAllFunctionsLoaded) {
         UnloadSecurityDll(hModule);
         hModule = NULL;
      }

   }
   
   return hModule;
}


/////////////////////////////////////////////////////////////////////////////// 


BOOL GenClientContext(PAUTH_SEQ pAS, PSEC_WINNT_AUTH_IDENTITY pAuthIdentity,
      PVOID pIn, DWORD cbIn, PVOID pOut, PDWORD pcbOut, PBOOL pfDone) {

/*++

 Routine Description:

   Optionally takes an input buffer coming from the server and returns
   a buffer of information to send back to the server.  Also returns
   an indication of whether or not the context is complete.

 Return Value:

   Returns TRUE if successful; otherwise FALSE.

--*/ 

   SECURITY_STATUS ss;
   TimeStamp       tsExpiry;
   SecBufferDesc   sbdOut;
   SecBuffer       sbOut;
   SecBufferDesc   sbdIn;
   SecBuffer       sbIn;
   ULONG           fContextAttr;

   if (!pAS->fInitialized) {
      
      ss = _AcquireCredentialsHandle(NULL, _T("NTLM"), 
            SECPKG_CRED_OUTBOUND, NULL, pAuthIdentity, NULL, NULL,
            &pAS->hcred, &tsExpiry);
      if (ss < 0) {
         fprintf(stderr, "AcquireCredentialsHandle failed with %08X\n", ss);
         return FALSE;
      }

      pAS->fHaveCredHandle = TRUE;
   }

   // Prepare output buffer
   sbdOut.ulVersion = 0;
   sbdOut.cBuffers = 1;
   sbdOut.pBuffers = &sbOut;

   sbOut.cbBuffer = *pcbOut;
   sbOut.BufferType = SECBUFFER_TOKEN;
   sbOut.pvBuffer = pOut;

   // Prepare input buffer
   if (pAS->fInitialized)  {
      sbdIn.ulVersion = 0;
      sbdIn.cBuffers = 1;
      sbdIn.pBuffers = &sbIn;

      sbIn.cbBuffer = cbIn;
      sbIn.BufferType = SECBUFFER_TOKEN;
      sbIn.pvBuffer = pIn;
   }

   ss = _InitializeSecurityContext(&pAS->hcred, 
         pAS->fInitialized ? &pAS->hctxt : NULL, NULL, 0, 0, 
         SECURITY_NATIVE_DREP, pAS->fInitialized ? &sbdIn : NULL,
         0, &pAS->hctxt, &sbdOut, &fContextAttr, &tsExpiry);
   if (ss < 0)  { 
      // <winerror.h>
      fprintf(stderr, "InitializeSecurityContext failed with %08X\n", ss);
      return FALSE;
   }

   pAS->fHaveCtxtHandle = TRUE;

   // If necessary, complete token
   if (ss == SEC_I_COMPLETE_NEEDED || ss == SEC_I_COMPLETE_AND_CONTINUE) {

      if (_CompleteAuthToken) {
         ss = _CompleteAuthToken(&pAS->hctxt, &sbdOut);
         if (ss < 0)  {
            fprintf(stderr, "CompleteAuthToken failed with %08X\n", ss);
            return FALSE;
         }
      }
      else {
         fprintf (stderr, "CompleteAuthToken not supported.\n");
         return FALSE;
      }
   }

   *pcbOut = sbOut.cbBuffer;

   if (!pAS->fInitialized)
      pAS->fInitialized = TRUE;

   *pfDone = !(ss == SEC_I_CONTINUE_NEEDED 
         || ss == SEC_I_COMPLETE_AND_CONTINUE );

   return TRUE;
}


/////////////////////////////////////////////////////////////////////////////// 


BOOL GenServerContext(PAUTH_SEQ pAS, PVOID pIn, DWORD cbIn, PVOID pOut,
      PDWORD pcbOut, PBOOL pfDone) {

/*++

 Routine Description:

    Takes an input buffer coming from the client and returns a buffer
    to be sent to the client.  Also returns an indication of whether or
    not the context is complete.

 Return Value:

    Returns TRUE if successful; otherwise FALSE.

--*/ 

   SECURITY_STATUS ss;
   TimeStamp       tsExpiry;
   SecBufferDesc   sbdOut;
   SecBuffer       sbOut;
   SecBufferDesc   sbdIn;
   SecBuffer       sbIn;
   ULONG           fContextAttr;

   if (!pAS->fInitialized)  {
      
      ss = _AcquireCredentialsHandle(NULL, _T("NTLM"), 
            SECPKG_CRED_INBOUND, NULL, NULL, NULL, NULL, &pAS->hcred, 
            &tsExpiry);
      if (ss < 0) {
         fprintf(stderr, "AcquireCredentialsHandle failed with %08X\n", ss);
         return FALSE;
      }

      pAS->fHaveCredHandle = TRUE;
   }

   // Prepare output buffer
   sbdOut.ulVersion = 0;
   sbdOut.cBuffers = 1;
   sbdOut.pBuffers = &sbOut;

   sbOut.cbBuffer = *pcbOut;
   sbOut.BufferType = SECBUFFER_TOKEN;
   sbOut.pvBuffer = pOut;

   // Prepare input buffer
   sbdIn.ulVersion = 0;
   sbdIn.cBuffers = 1;
   sbdIn.pBuffers = &sbIn;

   sbIn.cbBuffer = cbIn;
   sbIn.BufferType = SECBUFFER_TOKEN;
   sbIn.pvBuffer = pIn;

   ss = _AcceptSecurityContext(&pAS->hcred, 
         pAS->fInitialized ? &pAS->hctxt : NULL, &sbdIn, 0, 
         SECURITY_NATIVE_DREP, &pAS->hctxt, &sbdOut, &fContextAttr, 
         &tsExpiry);
   if (ss < 0)  {
      fprintf(stderr, "AcceptSecurityContext failed with %08X\n", ss);
      return FALSE;
   }

   pAS->fHaveCtxtHandle = TRUE;

   // If necessary, complete token
   if (ss == SEC_I_COMPLETE_NEEDED || ss == SEC_I_COMPLETE_AND_CONTINUE) {
      
      if (_CompleteAuthToken) {
         ss = _CompleteAuthToken(&pAS->hctxt, &sbdOut);
         if (ss < 0)  {
            fprintf(stderr, "CompleteAuthToken failed with %08X\n", ss);
            return FALSE;
         }
      }
      else {
         fprintf (stderr, "CompleteAuthToken not supported.\n");
         return FALSE;
      }
   }

   *pcbOut = sbOut.cbBuffer;

   if (!pAS->fInitialized)
      pAS->fInitialized = TRUE;

   *pfDone = !(ss = SEC_I_CONTINUE_NEEDED 
         || ss == SEC_I_COMPLETE_AND_CONTINUE);

   return TRUE;
}

BOOL WINAPI SSPLogonUser(LPTSTR szDomain, LPTSTR szUser, LPTSTR szPassword) {

   AUTH_SEQ    asServer   = {0};
   AUTH_SEQ    asClient   = {0};
   BOOL        fDone      = FALSE;
   BOOL        fResult    = FALSE;
   DWORD       cbOut      = 0;
   DWORD       cbIn       = 0;
   DWORD       cbMaxToken = 0;
   PVOID       pClientBuf = NULL;
   PVOID       pServerBuf = NULL;
   PSecPkgInfo pSPI       = NULL;
   HMODULE     hModule    = NULL;

   SEC_WINNT_AUTH_IDENTITY ai;
   __try {

      hModule = LoadSecurityDll();
      if (!hModule)
         __leave;

      // Get max token size
      _QuerySecurityPackageInfo(_T("NTLM"), &pSPI);
      cbMaxToken = pSPI->cbMaxToken;
      _FreeContextBuffer(pSPI);

      // Allocate buffers for client and server messages
      pClientBuf = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, cbMaxToken);
      pServerBuf = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, cbMaxToken);

      // Initialize auth identity structure
      ZeroMemory(&ai, sizeof(ai));
#if defined(UNICODE) || defined(_UNICODE)
      ai.Domain = szDomain;
      ai.DomainLength = lstrlen(szDomain);
      ai.User = szUser;
      ai.UserLength = lstrlen(szUser);
      ai.Password = szPassword;
      ai.PasswordLength = lstrlen(szPassword);
      ai.Flags = SEC_WINNT_AUTH_IDENTITY_UNICODE;
#else      
      ai.Domain = (unsigned char *)szDomain;
      ai.DomainLength = lstrlen(szDomain);
      ai.User = (unsigned char *)szUser;
      ai.UserLength = lstrlen(szUser);
      ai.Password = (unsigned char *)szPassword;
      ai.PasswordLength = lstrlen(szPassword);
      ai.Flags = SEC_WINNT_AUTH_IDENTITY_ANSI;
#endif

      // Prepare client message (negotiate) .
      cbOut = cbMaxToken;
      if (!GenClientContext(&asClient, &ai, NULL, 0, pClientBuf, &cbOut, &fDone))
         __leave;

      // Prepare server message (challenge) .
      cbIn = cbOut;
      cbOut = cbMaxToken;
      if (!GenServerContext(&asServer, pClientBuf, cbIn, pServerBuf, &cbOut, 
            &fDone))
         __leave;
         // Most likely failure: AcceptServerContext fails with SEC_E_LOGON_DENIED
         // in the case of bad szUser or szPassword.
         // Unexpected Result: Logon will succeed if you pass in a bad szUser and 
         // the guest account is enabled in the specified domain.

      // Prepare client message (authenticate) .
      cbIn = cbOut;
      cbOut = cbMaxToken;
      if (!GenClientContext(&asClient, &ai, pServerBuf, cbIn, pClientBuf, &cbOut,
            &fDone))
         __leave;

      // Prepare server message (authentication) .
      cbIn = cbOut;
      cbOut = cbMaxToken;
      if (!GenServerContext(&asServer, pClientBuf, cbIn, pServerBuf, &cbOut, 
            &fDone))
         __leave;

      fResult = TRUE;

   } __finally {

      // Clean up resources
      if (asClient.fHaveCtxtHandle)
         _DeleteSecurityContext(&asClient.hctxt);

      if (asClient.fHaveCredHandle)
         _FreeCredentialsHandle(&asClient.hcred);

      if (asServer.fHaveCtxtHandle)
         _DeleteSecurityContext(&asServer.hctxt);

      if (asServer.fHaveCredHandle)
         _FreeCredentialsHandle(&asServer.hcred);

      if (hModule)
         UnloadSecurityDll(hModule);

      HeapFree(GetProcessHeap(), 0, pClientBuf);
      HeapFree(GetProcessHeap(), 0, pServerBuf);

   }

   return fResult;
}


HRESULT CheckUserGroups(IADsUser *pUser)
{
   LPTSTR lpszSystemInfo;      
   DWORD cchBuff = 256;        
   TCHAR tchBuffer[BUFSIZE];   
   IADsMembers *pGroups;
   WCHAR mypath1[256];
   HRESULT hr = S_OK;
   hr = pUser->Groups(&pGroups);
   pUser->Release();
   if (FAILED(hr)) return hr;

   lpszSystemInfo = tchBuffer; 
   if( GetComputerName(lpszSystemInfo, &cchBuff) ) 
   strncat( lpszSystemInfo, "-VNC",9 );
   mbstowcs(mypath1,lpszSystemInfo,strlen(lpszSystemInfo));



   IUnknown *pUnk;
   hr = pGroups->get__NewEnum(&pUnk);
   if (FAILED(hr)) return hr;
   pGroups->Release();

   IEnumVARIANT *pEnum;
   hr = pUnk->QueryInterface(IID_IEnumVARIANT,(void**)&pEnum);
   if (FAILED(hr)) return hr;

   pUnk->Release();

   // Now Enumerate
   BSTR bstr;
   VARIANT var;
   IADs *pADs;
   ULONG lFetch;
   IDispatch *pDisp;

   VariantInit(&var);
   hr = pEnum->Next(1, &var, &lFetch);
   while(hr == S_OK)
   {
       if (lFetch == 1)
       {
            pDisp = V_DISPATCH(&var);
            pDisp->QueryInterface(IID_IADs, (void**)&pADs);
            pADs->get_Name(&bstr);
			_wcsupr(bstr);
            printf("Group belonged: %S\n",bstr);
			if (wcsstr(bstr,L"VNCACCESS")!=0) 
				ad_access=TRUE;
			if (wcsstr(bstr,mypath1)!=0) 
				ad_access=TRUE;
			if (wcsstr(bstr,L"ADMINISTRATORS")!=0) ad_access=TRUE;
            SysFreeString(bstr);
            pADs->Release();
       }
       VariantClear(&var);
       pDisp=NULL;
       hr = pEnum->Next(1, &var, &lFetch);
   };
   hr = pEnum->Release();
   return S_OK;
}

IADsUser *GetUserObject(LPWSTR uPath)
{
   IADsUser *pUser;
//   IADs *pObject;
   HRESULT hr = ADsGetObject(uPath,IID_IADsUser,(void**)&pUser);
   if (FAILED(hr)) {return NULL;}
   BSTR bstr;
   hr = pUser->get_FullName(&bstr);
   printf("User: %S\n", bstr);
   SysFreeString(bstr);
   return pUser;
}
AUTHAD_API
BOOL CUGP(char * userin,char *password,char *machine)
{
	LPTSTR lpszSystemInfo;      
	DWORD cchBuff = 256;        
	TCHAR tchBuffer[BUFSIZE];   
	FILE *file;
	bool isNT = true;
	bool access_vnc=FALSE;
	bool laccess_vnc=FALSE;
	bool localdatabase=false;
	HRESULT hr;
	WCHAR mypath[256];
	WCHAR mypath1[256];
	OSVERSIONINFO ovi = { sizeof ovi };

	// determine OS and load appropriate library

	GetVersionEx( &ovi );
	if ( ovi.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS )
		isNT = false;

	HINSTANCE hNet = 0, hLoc = 0;

	if ( isNT )
		{
		hNet = LoadLibrary( "netapi32.dll" );
		}
	else
		{
		hLoc = LoadLibrary( "rlocal32.dll" );
		hNet = LoadLibrary( "radmin32.dll" );
		}

	if ( hNet == 0 )
	{
		printf( "LoadLibrary( %s ) failed, gle = %lu\n",
			isNT? "netapi32.dll": "radmin32.dll", GetLastError() );
		return false;
	}

	// locate needed functions
	NetApiBufferFree_t NetApiBufferFree = 0;
	NetGetDCNameNT_t NetGetDCNameNT = 0;
	NetGetDCName95_t NetGetDCName95 = 0;
	NetUserGetGroupsNT_t NetUserGetGroupsNT = 0;
	NetUserGetGroups95_t NetUserGetGroups95 = 0;
	NetWkstaGetInfoNT_t NetWkstaGetInfoNT = 0;
	NetWkstaGetInfo95_t NetWkstaGetInfo95 = 0;

	NetApiBufferFree = (NetApiBufferFree_t) GetProcAddress( hNet, "NetApiBufferFree" );
	if ( NetApiBufferFree == 0 )
	{
		printf( "Oops! GetProcAddress() failed\n" );
		return false;
	}
	
	if ( isNT )
	{
		NetGetDCNameNT = (NetGetDCNameNT_t) GetProcAddress( hNet, "NetGetDCName" );
		NetUserGetGroupsNT = (NetUserGetGroupsNT_t) GetProcAddress( hNet, "NetUserGetLocalGroups" );
		NetWkstaGetInfoNT = (NetWkstaGetInfoNT_t)GetProcAddress( hNet,"NetWkstaGetInfo" );
		if ( NetGetDCNameNT == 0 || NetUserGetGroupsNT == 0 || NetWkstaGetInfoNT==0)
		{
			printf( "Oops! Some functions not found ...\n" );
			return false;
		}
	}
	else
	{
		NetGetDCName95 = (NetGetDCName95_t) GetProcAddress( hLoc, "LocalNetGetDCName" );
		NetUserGetGroups95 = (NetUserGetGroups95_t) GetProcAddress( hNet, "NetUserGetGroupsA" );
		NetWkstaGetInfo95 = (NetWkstaGetInfo95_t)GetProcAddress( hNet,"NetWkstaGetInfoA" );

		if ( NetGetDCName95 == 0 || NetUserGetGroups95 == 0 ||NetWkstaGetInfo95 == 0)
		{
			printf( "Oops! Some functions not found ...\n" );
			return false;
		}
	}
	// find PDC if necessary; else set up server name for call

	byte *buf = 0;
	DWORD rc,rc2,read, total,i;
	char server[MAXLEN * sizeof(wchar_t)];
	char user[MAXLEN * sizeof(wchar_t)];
	char groupname[MAXLEN];
	char domain[MAXLEN * sizeof(wchar_t)];

	
	if ( isNT )	mbstowcs( (wchar_t *) user, userin, MAXLEN );
	else strcpy( user, userin );
	////////////////////////////////////////////////////////////////////////////////////////////
	////NT
	////////////////////////////////////////////////////////////////////////////////////////////

	if ( isNT )
		{

			printf("------------------------ NT AUTHAD.dll ---------------------------\n");
			///////////////// search DC ///////////////////////////////////
			rc = NetGetDCNameNT( 0, 0, &buf );
			if ( rc ) printf( "NetGetAnyDCNameNT() returned %lu\n", rc );
			if (rc==2453) printf( "No domain controler found\n");
			if (!rc)
			{
			printf("DC found");
			wcscpy( (wchar_t *) server, (wchar_t *) buf );
			wprintf((wchar_t *)server);
			printf("\n-------------------------------------------------------\n");
			}
			printf("---------- Checking Local groups ------------------------\n");

			///////////////// Local groups ///////////////////////////////////
			{
				LOCALGROUP_MEMBERS_INFO_3 *buf, *cur;
				DWORD read, total, resumeh, rc, i;
				wchar_t group1[MAXLEN];
				LPTSTR lpszSystemInfo;
				DWORD cchBuff = 256;        
				TCHAR tchBuffer[BUFSIZE];   

				mbstowcs( group1,  "administrators", MAXLEN );
				printf( " User: %S\n", _wcsupr((wchar_t *)user));
				printf( "Checking ADMINISTRATORS\n");
				resumeh = 0;

				typedef DWORD (__stdcall *netlocalgroupgetmembers_t)(LPWSTR,LPWSTR,DWORD,PBYTE*,DWORD,PDWORD,PDWORD,PDWORD);
				netlocalgroupgetmembers_t netlocalgroupgetmembers;
				HINSTANCE h = LoadLibrary ("netapi32.dll");
				netlocalgroupgetmembers = (netlocalgroupgetmembers_t) GetProcAddress (h, "NetLocalGroupGetMembers");

				do
				{
					buf = NULL;
					rc = netlocalgroupgetmembers( NULL, group1, 3, (BYTE **) &buf,BUFSIZE, &read, &total, &resumeh );
					if ( rc == ERROR_MORE_DATA || rc == ERROR_SUCCESS )
						{
							
							for ( i = 0, cur = buf; i < read; ++ i, ++ cur )
								{
									// Note: the capital S in the format string will expect Unicode
									// strings, as this is a program written/compiled for ANSI.
									_wcsupr(cur->lgrmi3_domainandname);
									printf( "%S\n", cur->lgrmi3_domainandname );
									
									if (wcsstr(_wcsupr(cur->lgrmi3_domainandname), _wcsupr((wchar_t *)user))!=0) 
										{
											laccess_vnc=TRUE;
											printf( "Local: User found in group \n" );
										}
								}
					if ( buf != NULL )
					NetApiBufferFree( buf );
						}
				} while ( rc == ERROR_MORE_DATA );

				mbstowcs( group1,  "vncaccess", MAXLEN );
				printf( "Checking VNCACCESS\n");
				resumeh = 0;
				do
				{
					buf = NULL;
					rc = netlocalgroupgetmembers( NULL, group1, 3, (BYTE **) &buf,BUFSIZE, &read, &total, &resumeh );
					if ( rc == ERROR_MORE_DATA || rc == ERROR_SUCCESS )
						{
							
							for ( i = 0, cur = buf; i < read; ++ i, ++ cur )
								{
									// Note: the capital S in the format string will expect Unicode
									// strings, as this is a program written/compiled for ANSI.
									_wcsupr(cur->lgrmi3_domainandname);
									printf( "%S\n", cur->lgrmi3_domainandname );
									
									if (wcsstr(_wcsupr(cur->lgrmi3_domainandname), _wcsupr((wchar_t *)user))!=0) 
										{
											laccess_vnc=TRUE;
											printf( "Local: User found in group \n" );
										}
								}
					if ( buf != NULL )
					NetApiBufferFree( buf );
						}
				} while ( rc == ERROR_MORE_DATA );

				lpszSystemInfo = tchBuffer; 
				if( GetComputerName(lpszSystemInfo, &cchBuff) ) 
				strncat( lpszSystemInfo, "-VNC",9 );
				mbstowcs(mypath1,lpszSystemInfo,strlen(lpszSystemInfo));
				printf( "Checking name-vnc: %S\n", _wcsupr(mypath1));
				resumeh = 0;
				do
				{
					buf = NULL;
					rc = netlocalgroupgetmembers( NULL, mypath1, 3, (BYTE **) &buf,BUFSIZE, &read, &total, &resumeh );
					if ( rc == ERROR_MORE_DATA || rc == ERROR_SUCCESS )
						{
							
							for ( i = 0, cur = buf; i < read; ++ i, ++ cur )
								{
									// Note: the capital S in the format string will expect Unicode
									// strings, as this is a program written/compiled for ANSI.
									_wcsupr(cur->lgrmi3_domainandname);
									printf( "%S\n", cur->lgrmi3_domainandname );
									
									if (wcsstr(_wcsupr(cur->lgrmi3_domainandname), _wcsupr((wchar_t *)user))!=0) 
										{
											laccess_vnc=TRUE;
											printf( "Local: User found in group \n" );
										}
								}
					if ( buf != NULL )
					NetApiBufferFree( buf );
						}
				} while ( rc == ERROR_MORE_DATA );
					if ( h != 0 )
		FreeLibrary( h);
			}
			///////////////// domain and AD  groups ///////////////////////////////////
			if ( !rc )
			{
				printf("---------- Checking Domain ------------------------\n");
				wcscpy( (wchar_t *) server, (wchar_t *) buf );
				byte *buf2 = 0;
				rc2 = NetWkstaGetInfoNT( 0 , 100 , &buf2 ) ;
				if( rc2 ) printf( "NetWkstaGetInfoA() returned %lu \n", rc2);
				else wcstombs( domain, ((WKSTA_INFO_100_NT *) buf2)->wki100_langroup, MAXLEN );
				NetApiBufferFree( buf2 );
				domain[MAXLEN - 1] = '\0';
				printf("Detected domain = %s\n",domain);
				NetApiBufferFree( buf );
				buf2 = 0;
				buf = 0;


				printf("---------- Checking AD groups ------------------------\n");
				wcscpy(mypath,L"WinNT://");
				int mylen=strlen(domain);
				mbstowcs(mypath1,domain,mylen+1);
				wcsncat(mypath,mypath1,wcslen(mypath1));
				wcscat(mypath,L"/");
				wcscat(mypath,(wchar_t *) user);
				wcscat(mypath,L",user");
				IADsUser *pUser = GetUserObject(mypath);
                     /* L"WinNT://simacbe/rudidv,user");*/
				if(pUser)
					{
						pUser->AddRef();
						hr = CheckUserGroups(pUser);
						pUser->Release();
					}
				if (ad_access==TRUE) printf( "NT: AD group found ...\n" );



				printf("---------- Checking Domain groups ------------------------\n");


				rc = NetUserGetGroupsNT( (wchar_t *) server,(wchar_t *) user, 0, 1,&buf2, MAX_PREFERRED_LENGTH, &read, &total);
				if ( rc == NERR_Success)
					{
						// Added By Greg Wood (wood@agressiv.com)
						lpszSystemInfo = tchBuffer; 
						if( GetComputerName(lpszSystemInfo, &cchBuff) ) 
						strncat( lpszSystemInfo, "-VNC",9 );
						for ( i = 0; i < read; ++ i )
							{
								wcstombs( groupname, ((LPLOCALGROUP_USERS_INFO_0_NT *) buf2)[i].grui0_name, MAXLEN );	
								groupname[MAXLEN - 1] = '\0'; // because strncpy won't do this if overflow
#ifdef _MSC_VER
								_strupr(groupname);
#else
								strupr(groupname);
#endif
								if (strcmp(groupname, lpszSystemInfo)==0) access_vnc=TRUE;
								if (strcmp(groupname, "VNCACCESS")==0) access_vnc=TRUE;
								if (strcmp(groupname, "ADMINISTRATORS")==0) access_vnc=TRUE;
								// End Mod from Greg Wood (wood@agressiv.com)
							}
						if (access_vnc==TRUE) printf( "NT: Domain group found ...\n" );

					}
				NetApiBufferFree( buf2 );
				
			}
		}
	////////////////////////////////////////////////////////////////////////////////////////////
	////9.X
	////////////////////////////////////////////////////////////////////////////////////////////
	if ( !isNT )
		{
			rc = NetGetDCName95( 0, &buf );
			if ( rc ) printf( "NetGetDCName95() returned %lu\n", rc );
			if (rc==2453) 
			{
				printf( "No domain controler found\n");
				return false;
			}
			if ( !rc )
			{
				strcpy( server, (char *) buf );
				byte *buf2 = 0;
				rc = NetWkstaGetInfo95( 0 , 100 , &buf2 ) ;
				if( rc ) printf( "NetWkstaGetInfoA() returned %lu \n", rc);
				else  strncpy( domain, ((WKSTA_INFO_100_95 *) buf2)->wki100_langroup, MAXLEN );
				NetApiBufferFree( buf2 );
				domain[MAXLEN - 1] = '\0';
				NetApiBufferFree( buf );
				buf = 0;
				buf2 = 0;
				//////////////////////////////////////////////
				printf("---------- Checking AD groups ------------------------\n");
				wcscpy(mypath,L"WinNT://");
				int mylen=strlen(domain);
				mbstowcs(mypath1,domain,mylen+1);
				wcsncat(mypath,mypath1,wcslen(mypath1));
				wcscat(mypath,L"/");
				wcscat(mypath,(wchar_t *) user);
				wcscat(mypath,L",user");
				IADsUser *pUser = GetUserObject(mypath);
                     /* L"WinNT://simacbe/rudidv,user");*/
				if(pUser)
					{
						pUser->AddRef();
						hr = CheckUserGroups(pUser);
						pUser->Release();
					}
				if (ad_access==TRUE) printf( "NT: AD group found ...\n" );
				////////////////////////////////////////////
				rc = NetUserGetGroups95( server,user, 0, &buf2, 2024, &read, &total);
				if ( rc == NERR_Success)
					{
						// Added By Greg Wood (wood@agressiv.com)
						lpszSystemInfo = tchBuffer; 
						if( GetComputerName(lpszSystemInfo, &cchBuff) ) 
						strncat( lpszSystemInfo, "-VNC",9 );
						for ( i = 0; i < read; ++ i )
							{ 
								strncpy( groupname, ((LPLOCALGROUP_USERS_INFO_0_95 *) buf2)[i].grui0_name, MAXLEN );
								groupname[MAXLEN - 1] = '\0'; // because strncpy won't do this if overflow
#ifdef _MSC_VER
							_strupr(groupname);
#else
							strupr(groupname);
#endif
								if (strcmp(groupname, lpszSystemInfo)==0) access_vnc=TRUE;
								if (strcmp(groupname, "VNCACCESS")==0) access_vnc=TRUE;
								if (strcmp(groupname, "ADMINISTRATORS")==0) access_vnc=TRUE;
							}
					}
				NetApiBufferFree( buf2 );
				buf2=0;
			}
		}
		
	
	if (buf)NetApiBufferFree( buf );
	if ( hNet != 0 )
		FreeLibrary( hNet );
	if ( hLoc != 0 )
		FreeLibrary( hLoc );
	//check user
	if (isNT)
	{
#if _MSC_VER < 1400
		wcstombs( user, (unsigned short *)user, MAXLEN);	
#else
		wcstombs( user, (const wchar_t *)user, MAXLEN);	
#endif
	}

	BOOL logon_OK=false;
	BOOL passwd_OK=false;
#ifdef _MSC_VER
	_strupr(user);
#else
	strupr(user);
#endif
	passwd_OK=false;
	if (SSPLogonUser(domain,user, password))
		{	passwd_OK=true;
			if (passwd_OK==TRUE) printf( "Domain password check OK \n" );		
		}
	else printf( "Domain password check Failed \n" );
		if (SSPLogonUser(".",user, password))
		{
			passwd_OK=true;
			if (passwd_OK==TRUE) printf( "Local password check OK \n" );
		}
	else printf( "Local password check Failed \n" );
	printf( "Checking Guest account \n" );
	if (SSPLogonUser(domain,"isdiua", "hegbfsa")) {passwd_OK=false;printf( "Guest account block \n" );}
	if (SSPLogonUser(".","isdiua", "hegbfsa")) {passwd_OK=false;printf( "Guest account block \n" );}
	if (access_vnc==TRUE || laccess_vnc==TRUE || ad_access==TRUE)
	{
		if (passwd_OK)
		{
			logon_OK=true;
			printf( "Acces to vnc  OK \n" );
		}
	}
	SYSTEMTIME time;
	GetLocalTime(& time);
	printf("%d/%d/%d  %d:%d", time.wYear, time.wMonth, time.wDay,time.wHour,time.wMinute );
	////////////////////LOGGING///////////////////////////
	{
	char szMslogonLog[MAX_PATH];
	char texttowrite[512];
	if (GetModuleFileName(NULL, szMslogonLog, MAX_PATH))
		{
			char* p = strrchr(szMslogonLog, '\\');
			if (p != NULL)
			{
				*p = '\0';
				strcat (szMslogonLog,"\\mslogon.log");
			}
		}
	printf(szMslogonLog);
	file = fopen(szMslogonLog, "a");
	if(file!=NULL) {
		SYSTEMTIME time;
		GetLocalTime(& time);
		char			szText[256];
		sprintf(szText,"%d/%d/%d %d:%.2d   ", time.wDay,time.wMonth,time.wYear,time.wHour,time.wMinute );
		strcpy(texttowrite,szText);
		if (access_vnc && logon_OK) 
		{
		strcat(texttowrite,"Logon as  ");
		strcat(texttowrite,user);
		strcat(texttowrite," using ");
		strcat(texttowrite,domain);
		
		}
		if (laccess_vnc && logon_OK)
		{
		strcat(texttowrite,"Local as ");
		strcat(texttowrite,user);
		strcat(texttowrite," using local ");
		
		}
		if (ad_access && logon_OK)
		{
		strcat(texttowrite,"Logon as ");
		strcat(texttowrite,user);
		strcat(texttowrite," using Active directory ");
		
		}
		if (!access_vnc && !laccess_vnc && !ad_access)
		{
			strcat(texttowrite,"Logon failed for ");
			strcat(texttowrite,user);
			strcat(texttowrite," belong to incorrect group ");
		}
		if (!logon_OK)
		{
			strcat(texttowrite,"Logon failed for ");
			strcat(texttowrite,user);
			strcat(texttowrite," bad password ");
		}
		strcat(texttowrite," From ");
		strcat(texttowrite,machine);
		strcat(texttowrite,"\n");
		fwrite( texttowrite, sizeof( char ), strlen(texttowrite),file);
		fclose(file);	
		}
		const char* ps[3];
		ps[0] = texttowrite;
		EventLogging log;
		log.AddEventSourceToRegistry(NULL);
		if (logon_OK) log.LogIt(1,0x00640001L, ps,1,NULL,0);
		else log.LogIt(1,0x00640002L, ps,1,NULL,0);
	}
	///////////////////////////////////////////////////////
	return logon_OK;
	
}

AUTHAD_API
void LOGEXIT(char *machine)
{
		const char* ps[3];
		char texttowrite[512];
		SYSTEMTIME time;
		GetLocalTime(& time);
		char			szText[256];
		sprintf(szText,"%d/%d/%d %d:%.2d   ", time.wDay,time.wMonth,time.wYear,time.wHour,time.wMinute );
		strcpy(texttowrite,szText);
		strcat(texttowrite,"Disconect ");
		strcat(texttowrite,machine);
		ps[0] = texttowrite;
	    EventLogging log;
		log.AddEventSourceToRegistry(NULL);
		log.LogIt(1,0x00640001L, ps,1,NULL,0);
}
