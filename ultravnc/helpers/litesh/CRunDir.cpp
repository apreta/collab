/*******************************************************************************
CRunDir

  used to execute all files in a folder

  NOTE: this executes sub-folders, thus opening them in an explorer window.
        is this the desired behaviour?
  
  (c) Robert Moss, 5th February 2002
*******************************************************************************/

#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <windows.h>
#include "CRunDir.h"

CRunDir::CRunDir(void)
{
	;
}

CRunDir::~CRunDir(void)
{
	;
}

bool CRunDir::Execute(char *path)
{
	HANDLE hFile;
	WIN32_FIND_DATA data;
	char *path_buf;
	int i;
	bool bHasSlash = false;

	i = strlen(path);
	if (i == 0)
		return false;

	if (path[i-1] == '\\')
		bHasSlash = true;

	i += 5;
	path_buf = (char *) malloc(sizeof(char) * i);

	if (!path_buf)
		return false;

	//Create path string with appended wildcards
	strcpy(path_buf,path);
	if (bHasSlash)
		strcat(path_buf,"*.*");
	else
		strcat(path_buf,"\\*.*");

	//Search for all files in this directory
	hFile = FindFirstFile(path_buf,&data);

	if (hFile == INVALID_HANDLE_VALUE)
		return false;

	do
	{
		if ( (strcmp(data.cFileName,".")) && (strcmp(data.cFileName,"..")) )
			//Valid file name (not "." or "..")
			ShellExecute(0,NULL,data.cFileName,NULL,path,SW_SHOWDEFAULT);
	}
	while ( FindNextFile(hFile, &data) );

	return (FindClose(hFile) != 0);
}