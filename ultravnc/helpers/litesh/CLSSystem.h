/*******************************************************************************
CLSSystem

  used to provide information about the PC on which LiteShell is running
  
  (c) Robert Moss, 5th February 2002
*******************************************************************************/

#ifndef CLSSYSTEM_H
#define CLSSYSTEM_H

typedef bool (__stdcall *LPDELPROC)(void);
typedef bool (__stdcall *LPSETPROC)(HWND hWin, UINT uMsg, bool bAllMessages);
#define SM_HOOK (WM_USER + 1)

class CLSSystem
{
protected:
	bool bValidNT;
	bool bIsNT;

	bool bLSRunning;

	CStringList cslDrives;
	IMalloc *mal;
	LPDELPROC lpDel;
	LPSETPROC lpSet;
	HMODULE hHook;

	void RunStartupFolders(void);
	void RunStartupKeys(void);

public:
	CLSSystem(void);
	~CLSSystem(void);

	int IsNT(void);
	bool IsLSRunning(void);
	char * GetFolderPath(int ID);
	void FinishedLoading(void);
	POINT ScreenRes(void);
	CStringList *GetDrives(void);
	char *GetMenuDir(void);
	
	void RunStartupApps(void);
	bool LoadHook(HWND hWnd);
	void FreeHook(void);
};

#endif