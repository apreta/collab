/*******************************************************************************
CLSPlugin

  used to handle loading/unloading of plugins
  
  (c) Robert Moss, 5th February 2002
*******************************************************************************/

#ifndef CLSPLUGIN_H
#define CLSPLUGIN_H

typedef HWND (*LPLOADPROC)(HINSTANCE hInstance, HWND hWin, int iPluginID);

#define PM_CLOSE   (WM_USER + 2)
#define PM_EXECUTE (WM_USER + 3)

class CLSPlugin
{
private:
	HMODULE hLib;
	HWND hWnd;
	bool bLoaded;

	LPLOADPROC lpLoad;

	bool Load(char *szLibrary, int iPluginID);
	bool Unload(void);

	static HWND hDesktop;
	static HINSTANCE hInst;
	static bool bLoadedAll;
	static CStringList *pslPlugins; /* used to store pointed to CLSPlugin objects (I know storing them as (char *) isn't perfect) */

public:
	CLSPlugin(void);
	~CLSPlugin(void);

	static void Initialise(HINSTANCE hInstance, HWND hWnd);
	static void LoadPlugins(CRegistry *pcRegs);
	static void UnloadPlugins(void);
	static HWND GetPluginWnd(int iPluginID);
};

#endif