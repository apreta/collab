/*******************************************************************************
CLSSystem

  used to provide information about the PC on which LiteShell is running
  
  (c) Robert Moss, 5th February 2002
*******************************************************************************/

#include <Windows.h>
#include <ShlObj.h>
#include "CStringList.h"
#include "CRegistry.h"
#include "CRunKey.h"
#include "CRunDir.h"
#include "CLSSystem.h"

CLSSystem::CLSSystem(void)
{
	OSVERSIONINFO info;

	//Is another copy of LiteShell already running?
//	CreateMutex(NULL, true, "LiteShell");
//	bLSRunning = (GetLastError() == ERROR_ALREADY_EXISTS);

	//IMalloc
	if (SHGetMalloc(&mal) != NOERROR)
		mal = NULL;

	//Check whether this is Windows NT
	info.dwOSVersionInfoSize = sizeof(info);
	bValidNT = (GetVersionEx(&info) != 0);
	if (!bValidNT)
		return;
	bIsNT = (info.dwPlatformId == VER_PLATFORM_WIN32_NT);

	lpDel = NULL;
	lpSet = NULL;
	hHook = NULL;
}

CLSSystem::~CLSSystem(void)
{
	mal->Release();
}

int CLSSystem::IsNT(void)
{
	if (bValidNT)
		if (bIsNT)
			return 1;
		else
			return 0;
	else
		return -1;
}

bool CLSSystem::IsLSRunning(void)
{
	return bLSRunning;
}

char * CLSSystem::GetFolderPath(int ID)
{
	if (!mal)
		return NULL;

	LPITEMIDLIST pidl;
	char *szBuffer = (char *) malloc((MAX_PATH + 2) * sizeof(char));
	char *szTemp;

	if (!szBuffer)
		return NULL;

	if (SHGetSpecialFolderLocation(0,ID,&pidl) != NOERROR)
	{
		free(szBuffer);
		return NULL;
	}

	if ( !SHGetPathFromIDList(pidl,szBuffer) )
	{
		mal->Free(pidl);
		free(szBuffer);
		return NULL;
	}

	mal->Free(pidl);

	szTemp = szBuffer;
	while (*szTemp)
		szTemp++;

	szTemp--;
	if (*szTemp++ != '\\')
	{
		*szTemp++ = '\\';
		*szTemp = '\0';
	}

	//the caller MUST free this buffer, or there will be a LEAK
	return szBuffer;
}

void CLSSystem::FinishedLoading(void)
{
	SendMessage(GetDesktopWindow(), 0x400, 0, 0);
}

POINT CLSSystem::ScreenRes(void)
{
	POINT p;

	p.x = GetSystemMetrics(SM_CXSCREEN);
	p.y = GetSystemMetrics(SM_CYSCREEN);

	return p;
}

CStringList *CLSSystem::GetDrives(void)
{
	DWORD dwDrives;
	UINT uiType;
	char szDrive[4] = {' ', ':', '\\', '\0'};
	char *szLabel = NULL;
	DWORD dwSize = MAX_PATH + 1;
	int i = 0;

	if ( ! (dwDrives = GetLogicalDrives()) )
		return NULL;

	cslDrives.clear();

	while (dwDrives)
	{
		if (dwDrives % 2)
		{
			szDrive[0] = i + 65;
			uiType = GetDriveType(szDrive);

			if (uiType == DRIVE_NO_ROOT_DIR)
			{
				dwDrives >>= 1;
				i++;
				continue;
			}

			cslDrives.add(szDrive);

			if ((i == 0) && (uiType == DRIVE_REMOVABLE))
				cslDrives.add("3� Floppy (A:)");
			else if ((i == 1) && (uiType == DRIVE_REMOVABLE))
				cslDrives.add("5� Floppy (B:)");
			else
			{
				if (uiType != DRIVE_CDROM)
				{
					szLabel = (char *) malloc(dwSize);
					if ((szLabel) && (( !GetVolumeInformation(szDrive,szLabel,dwSize,NULL,NULL,NULL,NULL,0) ) || ( !strlen(szLabel) )))
					{
						free(szLabel);
						szLabel = NULL;
					}
					else
					{
						szDrive[2] = '\0';
						strcat(szLabel, " (");
						strcat(szLabel, szDrive);
						strcat(szLabel, ")");
						szDrive[2] = '\\';
					}
				}

				if (!szLabel)
				{
					szLabel = (char *) malloc(dwSize);
					szDrive[2] = '\0';

					switch (uiType)
					{
					case DRIVE_REMOVABLE:
						strcpy(szLabel, "Removable Disk (");
						break;

					case DRIVE_FIXED:
						strcpy(szLabel, "Local Disk (");
						break;

					case DRIVE_REMOTE:
						strcpy(szLabel, "Network Disk (");
						break;

					case DRIVE_CDROM:
						strcpy(szLabel, "CD-ROM Drive (");
						break;

					case DRIVE_RAMDISK:
						strcpy(szLabel, "RAM Disk (");
						break;

					case DRIVE_UNKNOWN:
						strcpy(szLabel, "Unknown Disk (");
						break;

					default:
						break;
					}

					strcat(szLabel, szDrive);
					strcat(szLabel, ")");
					szDrive[2] = '\\';
				}

				cslDrives.add(szLabel);
				free(szLabel);
				szLabel = NULL;
			}
		}

		dwDrives >>= 1;
		i++;
	}

	return &cslDrives;
}

char *CLSSystem::GetMenuDir(void)
{
	/*BROWSEINFO biInfo;
	LPITEMIDLIST pilList;
	IMalloc *imMalloc;
	char *szPath;

	if (CoInitialize(NULL) != S_OK)
		return NULL;
	if (SHGetMalloc(&imMalloc) != NOERROR)
	{
		CoUninitialize();
		return NULL;
	}

	biInfo.hwndOwner = NULL;
	biInfo.pidlRoot = NULL;
	biInfo.pszDisplayName = NULL;
	biInfo.lpszTitle = "Choose the directory LiteShell should use to build the menu";
	biInfo.ulFlags = BIF_RETURNONLYFSDIRS;
	biInfo.lpfn = NULL;
	biInfo.lParam = 0;
	biInfo.iImage = 0;

	if (! (pilList = SHBrowseForFolder(&biInfo)) )
	{
		imMalloc->Release();
		CoUninitialize();
		return NULL;
	}

	if (! (szPath = (char *) malloc(MAX_PATH + 1)) )
	{
		imMalloc->Release();
		CoUninitialize();
		return NULL;
	}

	if (! SHGetPathFromIDList(pilList,szPath))
	{
		free(szPath);
		szPath = NULL;
	}

	imMalloc->Free(pilList);
	imMalloc->Release();
	CoUninitialize();
*/
	char *szPath;
	szPath = (char *) malloc(MAX_PATH + 1);
	strcpy(szPath,".\\menu");
	return szPath;
}

void CLSSystem::RunStartupApps(void)
{
	RunStartupKeys();
	if (GetKeyState(VK_SHIFT) >= 0)
		RunStartupFolders();
}

void CLSSystem::RunStartupFolders(void)
{
	CRunDir cRunD;
	char *szBuffer;

	szBuffer = GetFolderPath(CSIDL_STARTUP);
	cRunD.Execute(szBuffer);
	free(szBuffer);

	//Launch NT's startup folder apps
	if ( IsNT() )
	{
		szBuffer = GetFolderPath(CSIDL_COMMON_STARTUP);
		cRunD.Execute(szBuffer);
		free(szBuffer);
	}
}

void CLSSystem::RunStartupKeys(void)
{
	CRunKey		cRunK;
	char *szRegsKeep[4]  = {"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run",
							"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunEx",
							"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServices",
							"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServicesEx"};
	char *szRegsDel[4]   = {"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnce",
							"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunOnceEx",
							"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServicesOnce",
							"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\RunServicesExOnce"};
	HKEY hKey = NULL;
	int i = 0;

	do
	{
		if (!hKey)
			hKey = HKEY_LOCAL_MACHINE;
		else
			hKey = HKEY_CURRENT_USER;

		for (i = 0; i < 4; i++)
			cRunK.Execute(hKey,szRegsKeep[i],false);

		for (i = 0; i < 4; i++)
			cRunK.Execute(hKey,szRegsDel[i],true);
	}
	while (hKey != HKEY_CURRENT_USER);

}

bool CLSSystem::LoadHook(HWND hWnd)
{
	hHook = LoadLibrary("HOOK.DLL");

	if (hHook)
	{
		lpDel = (bool (__stdcall *)(void)) GetProcAddress(hHook,(char *) ((long) 2));
		lpSet = (bool (__stdcall *)(HWND hWin, UINT uMsg, bool bAllMessages)) GetProcAddress(hHook,(char *) ((long) 1));

		if ( ! (lpDel && lpSet))
		{
			if (hHook)
				FreeLibrary(hHook);
			hHook = NULL;
			return false;
		}
		else
			lpSet(hWnd, SM_HOOK, false);
	}
	else
		return false;

	return true;
}

void CLSSystem::FreeHook(void)
{
	if (hHook)
	{
		lpDel();
		FreeLibrary(hHook);
	}
}
