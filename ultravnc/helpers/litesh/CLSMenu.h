/*******************************************************************************
CLSMenu

  manages LiteShell's menu

  (c) Robert Moss, 5th February 2002
*******************************************************************************/

#ifndef CLSMENU_H
#define CLSMENU_H

class CLSMenu
{
protected:
	HMENU hMenu;
	HMENU hTasks, hDocs, hTimeMenu;
	UINT  uTimePos;
	char *szMenu;
	bool bIsNT;
	bool bIsStatic;
	bool bSpecialOrdering;
	CStringList cslItems;
	CLSSystem *pcSysm;

	bool BuildDir(HMENU hMenu, char *szPath);
	bool IsScript(char *szFile, unsigned long *ulPos);
	void FormatCaption(char *szFile);
	void HandleScriptItem(HMENU hMenu, char *szFile, unsigned long ulExclamation);
	HMENU AddToMenu(HMENU hMenu, char *szCaption, enum ItemType itType, bool bDoFormat);

	bool SetPath(char *szPath, CLSSystem *pcSysm);

	void BuildSpecialFolder(HMENU hSubMenu, int ID);
	void BuildTaskList(HMENU hSubMenu);
	void InsertTimeItem(HMENU hMenu, UINT uPos);
	void ClearMenu(HMENU hSubMenu);

	void ConstructSortingName(char *szBuffer, char *szFile, bool bIsDir);
	void DestructSortingName(char *szBuffer, char *szFile);
	bool SortingNameIsDir(char *szFile);
	char *GetCaption(char *szFile);

public:
	CLSMenu(void);
	~CLSMenu(void);

	bool Clear(void);
	void SetSpecialOrdering(bool bSet);
	bool Build(void);
	bool Build(bool bStatic);
	bool Popup(HWND hWnd, int iX, int iY);
	HMENU GetMenu(void);
	char *GetDir(void);
	char *MenuItem(unsigned long ulItem);

	bool SetMenuDir(CRegistry *pcRegs, CLSSystem *pcSysm);
};

#endif