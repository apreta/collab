/*******************************************************************************
CRegistry

  used to interface with the Windows Registry
  
  (c) Robert Moss, 5th February 2002
*******************************************************************************/

#ifndef CREGISTRY_H
#define CREGISTRY_H

class CRegistry
{
protected:
	HKEY hRootKey;
	HKEY hCurrKey;
	bool bOpen;

	unsigned char *GetValue(char *szValueName, DWORD *pdwType);
public:
	CRegistry(void);
	~CRegistry(void);

	bool SetKey(HKEY hKey);
	bool Open(const char *szSubKey);
	bool Close(void);
	bool Delete(char *szSubKey);
	bool DeleteValues(void);

	HKEY GetKey(void);

	unsigned char *ReadString(char *szValueName);
	bool ReadBoolean(char *szValueName, bool bDefault);
	DWORD ReadDWORD(char *szValueName, DWORD dwDefault);

	bool WriteString(char *szValueName, unsigned char *szValue);
	bool WriteBoolean(char *szValueName, bool bValue);
	bool WriteDWORD(char *szValueName, DWORD dwValue);

	bool EnumKeys(CStringList *pslList);
	bool EnumValues(CStringList *pslList, bool bGetValues);
};

#endif