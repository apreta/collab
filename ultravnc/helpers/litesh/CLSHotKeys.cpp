/*******************************************************************************
CLSHotKeys

  used to manage hotkeys under LiteShell
  
  (c) Robert Moss, 5th February 2002
*******************************************************************************/

#include <stdlib.h>
#include <malloc.h>
#include <windows.h>
#include "CStringList.h"
#include "CRegistry.h"
#include "CLSHotKeys.h"

CLSHotKeys::CLSHotKeys(void)
{
	int i = 0, j = 0;

	hHKWnd = 0;

	for ( ; i < CLSHOTKEYS_MAX_ALTS; i++)
		for (j = 0; j < CLSHOTKEYS_MAX_KEYS; j++)
			pcCommands[i][j] = NULL;
}

CLSHotKeys::~CLSHotKeys(void)
{
	Clear();
}

int CLSHotKeys::Add(enum hkModifier hkMod, char cKey, char *szCommand)
{
	unsigned int uMod;
	int iID = -1;
	size_t stLength = strlen(szCommand) + 1;

	if ((!szCommand) || (stLength == 1))
		return -1;

	switch (hkMod)
	{
	case hkm_AltCtrl:
		uMod = 3;
		break;
	case hkm_AltShift:
		uMod = 5;
		break;
	case hkm_CtrlShift:
		uMod = 6;
		break;
	case hkm_AltCtrlShift:
		uMod = 7;
		break;
	case hkm_Win:
		uMod = 8;
		break;

	default:
		return -1;
	}

	cKey = toupper(cKey) - 65;
	if ((cKey < 0) || (cKey > 25))
		return -1;

	iID = (CLSHOTKEYS_MAX_KEYS * hkMod) + cKey;

	pcCommands[hkMod][cKey] = (char *) malloc(stLength);
	if ( ! pcCommands[hkMod][cKey] )
		return -1;

	strcpy(pcCommands[hkMod][cKey], szCommand);

	if ( ! RegisterHotKey(hHKWnd,iID,uMod,cKey + 65) )
	{
		free(pcCommands[hkMod][cKey]);
		pcCommands[hkMod][cKey] = NULL;
		return -1;
	}

	return iID;
}

void CLSHotKeys::Clear(void)
{
	int i = 0, j = 0;

	for (i = 0; i < CLSHOTKEYS_MAX_ALTS; i++)
		for (j = 0; j < CLSHOTKEYS_MAX_KEYS; j++)
			if (pcCommands[i][j])
			{
				UnregisterHotKey(hHKWnd, (CLSHOTKEYS_MAX_KEYS * i) + j);
				free(pcCommands[i][j]);
				pcCommands[i][j] = NULL;
			}
}

HWND CLSHotKeys::SetWnd(HWND hWnd)
{
	HWND hTemp = hHKWnd;

	Clear();
	hHKWnd = hWnd;

	return hTemp;
}

char *CLSHotKeys::Command(int iID)
{
	int iMod, iKey;

	if ((iID < 0) || (iID >= (CLSHOTKEYS_MAX_KEYS * CLSHOTKEYS_MAX_ALTS) ))
		return NULL;

	iMod = iID / CLSHOTKEYS_MAX_KEYS;
	iKey = iID % CLSHOTKEYS_MAX_KEYS;

	if ((iMod < 0) || (iMod >= CLSHOTKEYS_MAX_ALTS))
		return NULL;
	if ((iKey < 0) || (iKey >= CLSHOTKEYS_MAX_KEYS))
		return NULL;

	return pcCommands[iMod][iKey];
}

void CLSHotKeys::LoadHotKeys(bool bClear, HWND hWnd)
{
	CRegistry cRegs;
	char * szKeys[5] = {"SOFTWARE\\robm\\LiteShell\\HotKeys\\Alt+Ctrl",
						"SOFTWARE\\robm\\LiteShell\\HotKeys\\Alt+Ctrl+Shift",
						"SOFTWARE\\robm\\LiteShell\\HotKeys\\Alt+Shift",
						"SOFTWARE\\robm\\LiteShell\\HotKeys\\Ctrl+Shift",
						"SOFTWARE\\robm\\LiteShell\\HotKeys\\Win"};
	enum hkModifier hkMod[5] = {hkm_AltCtrlShift, hkm_AltShift, hkm_AltCtrl, hkm_CtrlShift, hkm_Win};
	char cAlphaKey;
	char *szBuffer;
	char szName[2];
	int iCounter = 0;

	if (bClear)
		Clear();

	if ( !cRegs.SetKey(HKEY_CURRENT_USER) )
		return;

	szName[1] = '\0';
	SetWnd(hWnd);

	for ( ; iCounter < 5; iCounter++)
	{
		if ( !cRegs.Open(szKeys[iCounter]) )
			continue;

		for (cAlphaKey = 'A'; cAlphaKey <= 'Z'; cAlphaKey++)
		{
			szName[0] = cAlphaKey;
			szBuffer = (char *) cRegs.ReadString(szName);
			if (szBuffer)
			{
				Add(hkMod[iCounter],cAlphaKey,szBuffer);
				free(szBuffer);
			}
		}

		cRegs.Close();
	}
}