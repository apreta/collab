/*******************************************************************************
CStringList

  used to maintain a dynamic collection of strings, with the ability to sort the
  collection using qsort and _stricmp for case-insensitive comparisons.

  (c) Robert Moss, 5th February 2002
*******************************************************************************/

#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include "CStringList.h"

#define CSTRINGLIST_BASE_SIZE 8
#define CSTRINGLIST_BASE_INCR 8

CStringList::CStringList(void)
{
	//Initialise data values
	ulCount = 0;
	ulSize = CSTRINGLIST_BASE_SIZE;
	ppcItems = (char **) malloc(sizeof(char *) * CSTRINGLIST_BASE_SIZE);
}

CStringList::~CStringList(void)
{
	unsigned long i = 0;

	//Free all items (if any)
	clear();
}

unsigned long CStringList::count(void)
{
	return ulCount;
}

bool CStringList::add(char *str)
{
	size_t stSize;
	char **ppcTemp;
	char *szBuffer;

	if (!ppcItems)
		return false;

	if (ulSize == ulCount)
	{
		//Need to enlarge the array
		ulSize += CSTRINGLIST_BASE_INCR;
		stSize = ulSize * sizeof(char *);
		if ( ! (ppcTemp = (char **) realloc(ppcItems, stSize)) )
			return false;
		ppcItems = ppcTemp;
	}

	//Add the string to the array
	stSize = strlen(str);
	szBuffer = (char *) malloc(++stSize);
	strcpy(szBuffer, str);
	ppcItems[ulCount++] = szBuffer;

	return true;
}

bool CStringList::del(unsigned long pos)
{
	unsigned long i;

	if (!ppcItems)
		return false;

	if (pos >= ulCount)
		return false;
	
	//Delete the string
	free(ppcItems[pos]);

	//Shift remaining items up one position
	for (i = pos + 1; i < ulCount; i++)
	{
		ppcItems[i - 1] = ppcItems[i];

		if ( !ppcItems[i])
			break;
	}

	ppcItems[ulCount - 1] = NULL;

	return true;
}	

char * CStringList::item(unsigned long pos)
{
	if (!ppcItems)
		//No items
		return NULL;
	else if (pos >= ulCount)
		//No such item
		return NULL;
	else
		//Return this item
		return ppcItems[pos];
}

bool CStringList::sort(bool bCaseSensitive)
{
	if (!ppcItems)
		return false;

	//Sort the string array
	if (bCaseSensitive)
		qsort(ppcItems,ulCount,sizeof(char *),(int (*)(const void *, const void *)) sort_proc_s);
	else
		qsort(ppcItems,ulCount,sizeof(char *),(int (*)(const void *, const void *)) sort_proc_i);

	return true;
}

int CStringList::sort_proc_s(char **item1, char **item2)
{
	//Use strcmp for case-sensitive sorting
	return strcmp(*item1, *item2);
}

int CStringList::sort_proc_i(char **item1, char **item2)
{
	//Use _stricmp for case-insensitive sorting
	return _stricmp(*item1, *item2);
}

bool CStringList::clear(void)
{
	unsigned long i = 0;

	if (!ppcItems)
		return false;

	for (; i < ulCount; i++)
		if (ppcItems[i])
			free(ppcItems[i]);
		else
			break;

	ulCount = 0;

	return true;
}