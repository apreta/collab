/*******************************************************************************
CRunDir

  used to execute all files in a folder

  NOTE: this executes sub-folders, thus opening them in an explorer window.
        is this the desired behaviour?
  
  (c) Robert Moss, 5th February 2002
*******************************************************************************/

#ifndef CRUNDIR_H
#define CRUNDIR_H

class CRunDir
{
public:
	CRunDir(void);
	~CRunDir(void);

	bool Execute(char *path);
};

#endif