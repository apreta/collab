/*******************************************************************************
mp3_controller

  Handles !MP3 Commands through Winamp or CoolPlayer
  (I am yet to test the CoolPlayer functionality personally)

  Adapted from Steve Lawson's WinAmp/CoolPlayer controller and !MP3 commands
  (with permission)

  (c) Robert Moss, 5th Febrary 2002
*******************************************************************************/

void MP3Controller(char *szCommand);