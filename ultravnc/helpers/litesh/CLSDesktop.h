/*******************************************************************************
CLSDesktop

  implements the LiteShell desktop window
  
  (c) Robert Moss, 20th February 2002
*******************************************************************************/

#ifndef CLSDESKTOP_H
#define CLSDESKTOP_H
DECLARE_HANDLE(HMONITOR);
class CLSDesktop
{
protected:
	HWND hDesktop;
	MINIMIZEDMETRICS mmOrig;

public:
	CLSDesktop(void);
	~CLSDesktop(void);

	bool Create(HINSTANCE hInstance, WNDPROC wpProc, POINT pSize, bool bShow, bool bHideMin);
	bool Show(bool bShow);
	bool Resize(POINT pSize);
	HWND GetWnd(void);
	bool Close(void);
	LPSTR deviceName;
	HMONITOR hMonitor;
	int m_ScreenOffsetx,m_ScreenOffsety;
};

#endif