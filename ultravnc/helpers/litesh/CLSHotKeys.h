/*******************************************************************************
CLSHotKeys

  used to manage hotkeys under LiteShell
  
  (c) Robert Moss, 5th February 2002
*******************************************************************************/

#ifndef CLSHOTKEYS_H
#define CLSHOTKEYS_H

#define CLSHOTKEYS_MAX_ALTS 5
#define CLSHOTKEYS_MAX_KEYS 26

enum hkModifier {hkm_AltCtrl, hkm_AltShift, hkm_CtrlShift, hkm_AltCtrlShift, hkm_Win};

class CLSHotKeys
{
protected:
	char (* pcCommands[ CLSHOTKEYS_MAX_ALTS ][ CLSHOTKEYS_MAX_KEYS ]);
	HWND hHKWnd;

public:
	CLSHotKeys(void);
	~CLSHotKeys(void);

	int  Add(enum hkModifier hkMod, char cKey, char *szCommand);
	void Clear(void);
	HWND SetWnd(HWND hWnd);
	char *Command(int iID);
	void LoadHotKeys(bool bClear, HWND hWnd);
};

#endif