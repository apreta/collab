/*******************************************************************************
mp3_controller

  Handles !MP3 Commands through Winamp or CoolPlayer
  (I am yet to test the CoolPlayer functionality personally)

  Adapted from Steve Lawson's WinAmp/CoolPlayer controller and !MP3 commands
  (with permission)

  (c) Robert Moss, 5th Febrary 2002
*******************************************************************************/

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include "mp3_controller.h"

/* Recognised MP3 Players */
enum mp3Player {mppWinamp = 0, mppCoolPlayer = 1};
#define MP3_PLAYER_COUNT 2

/* Recognised MP3 Commands */
enum MP3Command {mpcOpen = 0, mpcPlay, mpcPause, mpcPrev, mpcNext, mpcStop, mpcPlayList, mpcVolUp, mpcVolDown};
#define MP3_COMMAND_COUNT 9

/* Command Array */
int Command[MP3_PLAYER_COUNT][MP3_COMMAND_COUNT] = {
													{40029,40045,40046,40044,40048,40047,40040,40058,40059}, //Winamp 1.x, 2.x
													{40013,40010,40012,40015,40014,40011,40026,40016,40017}  //CoolPlayer
													};

void DoMP3Command(MP3Command mpcCommand)
{
	HWND hPlayer = NULL;
	mp3Player mpPlayer;
	
	if (hPlayer = FindWindow("Winamp v1.x", NULL))
		mpPlayer = mppWinamp;
	else if (hPlayer = FindWindow("CoolPlayer", NULL))
		mpPlayer = mppCoolPlayer;

	if (hPlayer)
		SendMessage(hPlayer, WM_COMMAND, Command[mpPlayer][mpcCommand], 0);
}

void MP3Controller(char *szCmd)
{
	if (! strcmp(szCmd,"OPEN"))
		DoMP3Command(mpcOpen);
	else if (! strcmp(szCmd,"PLAY"))
		DoMP3Command(mpcPlay);
	else if (! strcmp(szCmd,"PAUSE"))
		DoMP3Command(mpcPause);
	else if (! strcmp(szCmd,"PREV"))
		DoMP3Command(mpcPrev);
	else if (! strcmp(szCmd,"NEXT"))
		DoMP3Command(mpcNext);
	else if (! strcmp(szCmd,"STOP"))
		DoMP3Command(mpcStop);
	else if (! strcmp(szCmd,"PLAYLIST"))
		DoMP3Command(mpcPlayList);
	else if (! strcmp(szCmd,"VOLUP"))
		DoMP3Command(mpcVolUp);
	else if (! strcmp(szCmd,"VOLDOWN"))
		DoMP3Command(mpcVolDown);
}