/*******************************************************************************
CRunKey

  used to execute all entires in a registry key

  NOTE: this ignores sub-keys.
        is this the desired behaviour?
  
  (c) Robert Moss, 5th February 2002
*******************************************************************************/

#include <windows.h>
#include "CStringList.h"
#include "CRegistry.h"
#include "CRunKey.h"

CRunKey::CRunKey(void)
{
	;
}

CRunKey::~CRunKey(void)
{
	cslList.clear();
	crReg.Close();
}

bool CRunKey::Execute(HKEY hKey, char *szSubKey, bool bDelete)
{
	unsigned long i = 0;
	HKEY hTemp;

	if ( ! crReg.SetKey(hKey) )
		return false;

	if ( ! crReg.Open(szSubKey) )
		return false;

	hTemp = crReg.GetKey();

	if ( ! crReg.EnumValues(&cslList, true) )
	{
		crReg.Close();
		return false;
	}

	for (i = 1; i < cslList.count(); i += 2)
	{
		//MessageBox(0, cslList.item(i), NULL, 0);
		//ShellExecute(0,NULL,cslList.item(i),NULL,NULL,SW_SHOWDEFAULT);
		Execute(cslList.item(i));

		if (bDelete)
			RegDeleteValue(hTemp, cslList.item(i-1));
	}

	cslList.clear();
	crReg.Close();

	return true;
}

void CRunKey::Execute(char *szCommand)
{
	if (!szCommand)
		return;

	char *szTemp;

	while (isspace(*szCommand))
		szCommand++;

	szTemp = szCommand;

	if (*szTemp == '"')
	{
		szTemp++;
		while (*szTemp && (*szTemp != '"'))
			szTemp++;

		if (*szTemp == '"')
		{
			szTemp++;
			*szTemp = 0;
			szTemp++;
		}
		else
		{
			szCommand++;
			szTemp = NULL;
		}
	}
	else
	{
		while (*szTemp && ( ! isspace(*szTemp)))
			szTemp++;

		if (isspace(*szTemp))
		{
			*szTemp = 0;
			szTemp++;
		}
		else
		{
			szTemp = NULL;
		}
	}

	ShellExecute(0,NULL,szCommand,szTemp,NULL,SW_SHOWDEFAULT);
}