/*******************************************************************************
main

  The entry point for LiteShell.

  (c) Robert Moss, 20th February 2002
*******************************************************************************/

#include <Windows.h>
#include <ShlObj.h>
#include "CStringList.h"
#include "CLSHotKeys.h"
#include "CRunDir.h"
#include "CLSSystem.h"
#include "CRegistry.h"
#include "CRunKey.h"
#include "CLSMenu.h"
#include "CLSDesktop.h"
#include "CLSPlugin.h"
#include "mp3_controller.h"

void Execute(char *szFile, char *szPath);
LRESULT CALLBACK WndProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam);


CRegistry	cRegs;
CLSMenu		cMenu;
CLSHotKeys	cHotK;
CLSDesktop	cDesk;
CLSSystem	cSysm;
bool		bStaticMenu = false;
HWND		hDesktop = NULL;
HRESULT		(__stdcall *hRunDlg)(HWND hwnd, INT x, INT y, CHAR *sz1, CHAR *sz2, INT i) = NULL;
HWND		progHWND = NULL;
DWORD		ThreadId;
DWORD		ProcessId;


/* HERE WE GO ... */
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	MSG mMsg;
	HMODULE hLib = NULL;
	bool bDesk = true;
	bool bHook = false;
	bool bPlugins = false;
	bool bHideMin = false;

	if (cSysm.IsLSRunning())
		return 0;

	cRegs.SetKey(HKEY_CURRENT_USER);
	cRegs.Open("SOFTWARE\\robm\\LiteShell");

	if ( ! cMenu.SetMenuDir(&cRegs, &cSysm) )
	{
		cRegs.Close();
		return 0;
	}

	//if (cRegs.ReadBoolean("LoadApps",true))
	//	cSysm.RunStartupApps();

	/* Added for LiteShell v0.8a */
	//char *strDir = (char *) cRegs.ReadString("StartupDir");
	//if (strDir)
	//{
	//	if (CRunDir *pcRunDir = new CRunDir)
	//	{
	//		pcRunDir->Execute(strDir);
	//		delete pcRunDir;
	//	}
	//	free(strDir);
	//}

	bDesk = cRegs.ReadBoolean("Desktop",true);
	bStaticMenu = cRegs.ReadBoolean("StaticMenu",false);
	bHook = cRegs.ReadBoolean("WinHook",false);
	bPlugins = cRegs.ReadBoolean("Plugins",false);
	bHideMin = cRegs.ReadBoolean("HideMinimisedWindows",false);
	cMenu.SetSpecialOrdering( cRegs.ReadBoolean("AlternateOrdering",false) );
	cRegs.Close();

	if (bStaticMenu)
		cMenu.Build(bStaticMenu);

	cDesk.Create(hInstance,WndProc,cSysm.ScreenRes(),bDesk,bHideMin);
	hDesktop = cDesk.GetWnd();
	cHotK.LoadHotKeys(false, hDesktop);

	if (hLib = LoadLibrary("SHELL32.DLL"))
		hRunDlg = (HRESULT (__stdcall *)(HWND hwnd, INT, INT, CHAR *, CHAR *, INT)) GetProcAddress(hLib,(char *) ((long) 0x3D));

	if (bHook)
		if ( ! (bHook = cSysm.LoadHook(hDesktop)) )
			MessageBox(0,"Could not load hook.dll","LiteShell",0);

	if (bPlugins)
	{
		CLSPlugin::Initialise(hInstance, hDesktop);
		cRegs.Open("SOFTWARE\\robm\\LiteShell\\Plugins");
		CLSPlugin::LoadPlugins(&cRegs);
		cRegs.Close();
	}
	SetTimer(hDesktop,1,5000,NULL);
	cSysm.FinishedLoading();

	while (GetMessage(&mMsg,0,0,0) != 0)
	{
		TranslateMessage(&mMsg);
		DispatchMessage(&mMsg);
	}

	//Cleanup
	if (bPlugins)
		CLSPlugin::UnloadPlugins();
	cDesk.Close();
	if (hLib)
		FreeLibrary(hLib);
	if (bHook)
		cSysm.FreeHook();
	cMenu.Clear();
	cHotK.Clear();

	return 0;
}

BOOL CALLBACK FindMyWindow(HWND hwnd, LPARAM lParam)
{
 DWORD result;

 GetWindowThreadProcessId( hwnd, &result );
 char buffer[300]; 


 if(result == DWORD(lParam))
 {
	HWND parent;
	if (GetParent(hwnd)==NULL)
	{
		progHWND = hwnd;
		if (IsWindowVisible(progHWND) )
		{
		SetParent(progHWND, hDesktop); 
		MoveWindow(progHWND,20,20,800,600,TRUE);
		return FALSE;
		}
	}
 }
 else
 {
  // this isn't the right HWND so keep sending them to us
  return TRUE;
 }
 return TRUE;
}

BOOL CALLBACK FindMyWindow2(HWND hwnd, LPARAM lParam)
{
 DWORD result;

 GetWindowThreadProcessId( hwnd, &result );
 char buffer[300]; 


 if(result == DWORD(lParam))
 {
	HWND parent;
	if (GetParent(hwnd)==NULL)
	{
		progHWND = hwnd;
		if (IsWindowVisible(progHWND) )
		{
		//GetWindowText(hwnd,buffer,15);
		//(MessageBox(NULL, buffer,"ERROR:", MB_OK));
		 LONG testje=GetWindowLong(progHWND,GWL_STYLE);
		 if (testje>0)
		 {
		SetParent(progHWND, hDesktop); 
		MoveWindow(progHWND,20,20,800,600,TRUE);
		//ShowWindow(progHWND,SW_MINIMIZE);
		//ShowWindow(progHWND,SW_SHOWMAXIMIZED);
		//MoveWindow(progHWND,cDesk.m_ScreenOffsetx+20,cDesk.m_ScreenOffsety+20,800,600,TRUE);
		//SetWindowPos(progHWND, HWND_BOTTOM, cDesk.m_ScreenOffsetx+20, cDesk.m_ScreenOffsety+20, 800, 600, 0);
		 }
		return FALSE;
		}
	}
 }
 else
 {
  // this isn't the right HWND so keep sending them to us
  return TRUE;
 }
 return TRUE;
}

void ResolveLink(LPCTSTR szLinkFullPath,TCHAR *buf)
{
   IShellLink* psl;
   CoInitialize(NULL);

   // create a link manager object and request its interface
   HRESULT hr = ::CoCreateInstance(CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER,
                                   IID_IShellLink, (void**)&psl);

   // associate the manager object with the link file in hand
   IPersistFile* ppf; 
   // Get a pointer to the IPersistFile interface. 
   hr = psl->QueryInterface(IID_IPersistFile, (void**)&ppf);

   // full path string must be in Unicode.
   OLECHAR wsz[MAX_PATH];
   ::MultiByteToWideChar(CP_ACP, 0, szLinkFullPath, -1, wsz, MAX_PATH);

   hr = ppf->Load(wsz, STGM_READ);
   hr = psl->Resolve(NULL, SLR_UPDATE);    
   WIN32_FIND_DATA ffd; 
   hr = psl->GetPath(buf, MAX_PATH, &ffd, 0);
   ppf->Release();
   psl->Release(); 
   CoUninitialize();

}

void Execute(char *szFile, char *szPath)
{
	if (!szFile)
		return;
	if (szFile[0] == '!')
	{
		char *szTemp = szFile + 1;
		while (*szTemp)
		{
			*szTemp = toupper(*szTemp);
			szTemp++;
		}
		szFile++;

		if ( ! strcmp(szFile,"BUILDMENU") )
		{
			if (bStaticMenu)
				cMenu.Build();
		}
		else if ( ! strcmp(szFile,"EDITMENU") )
		{
			char *szDir = cMenu.GetDir();
			if (szDir)
			{
				ShellExecute(hDesktop,NULL,szDir,NULL,NULL,SW_SHOWDEFAULT);
				free(szDir);
			}
		}
		else if ( ! strcmp(szFile,"EMPTYDOCS") )
		{
			SHAddToRecentDocs(SHARD_PATH,NULL);
		}
		else if ( ! strcmp(szFile,"EXIT") )
		{
			PostMessage(hDesktop,WM_CLOSE,0,0);
		}
		else if ( ! strcmp(szFile,"HOTREBOOT") )
		{
			ShellExecute(hDesktop,NULL,"rundll.exe","user.exe,exitwindowsexec",NULL,SW_SHOWDEFAULT);
		}
		else if ( ! strcmp(szFile,"LOGOFF") )
		{
			ExitWindowsEx(EWX_LOGOFF,0);
		}
		else if ( ! strcmp(szFile,"MENU") )
		{
			POINT pnt;
			long lPos;
			if (GetCursorPos(&pnt))
			{
				lPos = (pnt.y << 16) + pnt.x;
				PostMessage(hDesktop,WM_RBUTTONDOWN,0,lPos);
			}
		}
		else if ( ! strcmp(szFile,"REBOOT") )
		{
			ExitWindowsEx(EWX_REBOOT,0);
		}
		else if ( ! strcmp(szFile,"RELOADHOTKEYS") )
		{
			cHotK.LoadHotKeys(true, hDesktop);
		}
		else if ( ! strcmp(szFile,"RUN") )
		{
			if (hRunDlg)
				hRunDlg(NULL, NULL, NULL, NULL, NULL, FALSE);
		}
		else if ( ! strcmp(szFile,"SHUTDOWN") )
		{
			ExitWindowsEx(EWX_SHUTDOWN,0);
		}
		else if ( ! strcmp(szFile,"SUSPEND") )
		{
			SetSystemPowerState(true,false);
		}
		else if (! strncmp(szFile,"MP3", 3))
			/* Winamp/CoolPlayer Commands */
			MP3Controller(szFile + 3);
		else if ( ! strncmp(szFile,"SHOW",4) )
		{
			/* Switch focus to another application */
			HWND hWindow = (HWND) atol(&szFile[4]);
			if (IsIconic(hWindow))
				ShowWindowAsync(hWindow,SW_RESTORE);
			SetForegroundWindow(hWindow);
		}

	}
	else
	{
		TCHAR buf[MAX_PATH];
		ResolveLink(szFile,buf);
		PROCESS_INFORMATION ProcessInfo; //This is what we get as an [out] parameter
		STARTUPINFO StartupInfo; //This is an [in] parameter
		ZeroMemory(&StartupInfo, sizeof(StartupInfo));
		StartupInfo.cb = sizeof StartupInfo ; //Only compulsory field
		DWORD pid;
		progHWND = NULL;
		StartupInfo.dwFlags=STARTF_USEPOSITION;
		StartupInfo.dwX=cDesk.m_ScreenOffsetx;
		StartupInfo.dwY=cDesk.m_ScreenOffsety;
		if(CreateProcess(NULL,buf,NULL,NULL,FALSE,0,NULL,szPath,&StartupInfo,&ProcessInfo))
		{ 
		WaitForInputIdle(ProcessInfo.hProcess, 10000);
		WaitForSingleObject(ProcessInfo.hProcess, 500);
		ThreadId=ProcessInfo.dwThreadId;
		ProcessId=ProcessInfo.dwProcessId;
		EnumThreadWindows(ProcessInfo.dwThreadId,(WNDENUMPROC)FindMyWindow,ProcessInfo.dwProcessId);
		CloseHandle(ProcessInfo.hThread);
		CloseHandle(ProcessInfo.hProcess);
		}

	}
}


LRESULT CALLBACK WndProc(HWND hWnd,UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	PAINTSTRUCT psPaint;
	HDC hDC;
	POINT pPoint;
	WINDOWPOS *wpPos;

	switch (uMsg)
	{
	case WM_TIMER:
		EnumThreadWindows(ThreadId,(WNDENUMPROC)FindMyWindow2,ProcessId);
		//MessageBox(NULL, "timer","ERROR:", MB_OK);
		break;
	case PM_EXECUTE:
		/*
		** Plugin Message
		**
		** wParam is ptr to a string (file or !command)
		** lParam is ptr to a string (path)
		*/
		Execute((char *)wParam, (char *)lParam);
		break;

	case WM_PAINT:
		hDC = BeginPaint(hWnd, &psPaint);
		PaintDesktop(hDC);
		EndPaint(hWnd, &psPaint);
		break;

    case WM_SYSCOMMAND:
		if (wParam == SC_CLOSE)
			return 0;
		else
			return DefWindowProc(hWnd,uMsg,wParam,lParam);
		break;

	case WM_COMMAND:
		Execute(cMenu.MenuItem(LOWORD(wParam)), NULL);
		break;

	case SM_HOOK:
	case WM_RBUTTONDOWN:
		cMenu.Popup(hWnd,LOWORD(lParam)+cDesk.m_ScreenOffsetx,HIWORD(lParam)+cDesk.m_ScreenOffsety);
		//cMenu.Popup(hWnd,LOWORD(lParam)+1600,HIWORD(lParam));
		break;

	case WM_CLOSE:
		PostQuitMessage(0);
		break;

	case WM_DISPLAYCHANGE:
		pPoint.x = LOWORD(lParam);
		pPoint.y = HIWORD(lParam);
		cDesk.Resize(pPoint);
		break;

	case WM_HOTKEY:
		Execute(cHotK.Command(wParam),NULL);
		break;

	case WM_WINDOWPOSCHANGING:
		if (wpPos = (WINDOWPOS *)lParam)
		{
			wpPos->hwndInsertAfter = HWND_BOTTOM;
			wpPos->flags |= SWP_NOMOVE | SWP_NOSIZE;
		}
		break;

	default:
		return DefWindowProc(hWnd,uMsg,wParam,lParam);
	}

	return 0;
}
