/*******************************************************************************
CRunKey

  used to execute all entires in a registry key

  NOTE: this ignores sub-keys.
        is this the desired behaviour?
  
  (c) Robert Moss, 5th February 2002
*******************************************************************************/

#ifndef CRUNKEY_H
#define CRUNKRY_H

class CRunKey
{
private:
	void Execute(char *szCommand);

protected:
	CRegistry crReg;
	CStringList cslList;

public:
	CRunKey(void);
	~CRunKey(void);

	bool Execute(HKEY hKey, char *szSubKey, bool bDelete);
};

#endif