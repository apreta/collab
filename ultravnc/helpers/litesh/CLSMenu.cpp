/*******************************************************************************
CLSMenu

  manages LiteShell's menu

  Includes:
	> Steve Lawson's !Time menu command (added a StaticMenu workaround)

  (c) Robert Moss, 5th February 2002
*******************************************************************************/

#include <string.h>
#include <windows.h>
#include <shlobj.h>
#include "CStringList.h"
#include "CLSSystem.h"
#include "CRegistry.h"
#include "CLSMenu.h"

enum ItemType { itPopup, itAddToList, itEnabled, itDisabled };
#define STR_EMPTY ""

CStringList cTasks;

bool IsAppWindow(HWND hWnd)
{
	bool bRes = (IsWindowVisible(hWnd) != 0);
	bRes &= (GetWindow(hWnd,GW_OWNER) == NULL);
	bRes &= ( ! (GetWindowLong(hWnd,GWL_EXSTYLE) & WS_EX_TOOLWINDOW));

	return bRes;
}

BOOL CALLBACK EnumTasks(HWND hWnd, LPARAM lParam)
{
	char *szTitle;
	char szWin[32] = "!SHOW";

	if (IsAppWindow(hWnd))
		if (szTitle = (char *) malloc( 256 * sizeof(char) ))
		{
			SendMessage(hWnd,WM_GETTEXT,255,(LPARAM) szTitle);
			_ltoa((long) hWnd,&szWin[5],10);
			cTasks.add(szWin);
			cTasks.add(szTitle);
			free(szTitle);
		}

	return true;
}

CLSMenu::CLSMenu(void)
{
	hMenu = CreatePopupMenu();
	hDocs = NULL;
	hTasks = NULL;
	hTimeMenu = NULL;
	uTimePos = NULL;
	szMenu = NULL;
	pcSysm = NULL;
	bIsStatic = false;
	bSpecialOrdering = false;
}

CLSMenu::~CLSMenu(void)
{
	if (szMenu)
	{
		free(szMenu);
		szMenu = NULL;
	}

	cslItems.clear();

	if (hMenu)
		DestroyMenu(hMenu);
}

bool CLSMenu::Clear(void)
{
	if (!cslItems.clear())
		return false;

	if (!DestroyMenu(hMenu))
		return false;

	if ( ! (hMenu = CreatePopupMenu()) )
		return false;

	return true;
}

bool CLSMenu::SetPath(char *szPath, CLSSystem *pcSysm)
{
	if (!pcSysm)
		return false;

	size_t stLength = strlen(szPath) + 1;

	this->pcSysm = pcSysm;
	bIsNT = (pcSysm->IsNT() > 0);

	if (szMenu)
	{
		free(szMenu);
		szMenu = NULL;
	}

	if (szPath[stLength - 2] != '\\')
	{
		//Add trailing slash to path
		szMenu = (char *) malloc(++stLength);
		strcpy(szMenu,szPath);
		szMenu[stLength - 2] = '\\';
		szMenu[stLength - 1] = '\0';
	}
	else
	{
		szMenu = (char *) malloc(stLength);
		strcpy(szMenu,szPath);
	}
	
	if (!szMenu)
		return false;

	return true;
}

void CLSMenu::SetSpecialOrdering(bool bSet)
{
	bSpecialOrdering = bSet;
}

bool CLSMenu::Build(void)
{
	unsigned long ulExclamation;
	char *szTemp =(char *) malloc(MAX_PATH + 2);
	if ( ! (szMenu && Clear()) )
		return false;
	ulExclamation=9;
	strcpy(szTemp,"STARTMENU!STARTMENU");
	HandleScriptItem(hMenu, szTemp, ulExclamation);
	ulExclamation=4;
	strcpy(szTemp,"EXIT!EXIT");
	HandleScriptItem(hMenu, szTemp, ulExclamation);
	return BuildDir(hMenu, szMenu);
}

bool CLSMenu::Build(bool bStatic)
{
	bIsStatic = bStatic;
	return Build();
}

bool CLSMenu::BuildDir(HMENU hMenu, char *szPath)
{
	CStringList cslList;
	size_t stLength = strlen(szPath) + 4;
	unsigned long ulPos;
	unsigned long ulMax;
	unsigned long ulCount = 0;
	unsigned long ulExclamation;
	size_t stFileLength;
	char *szSearch = (char *) malloc(stLength);
	char *szBuffer = (char *) malloc(MAX_PATH + 2);
	char *szTemp;
	char *szFileName;
	HANDLE hFile;
	WIN32_FIND_DATA fdData;

/* SANITY CHECK */
	if ((!szSearch) || (!szBuffer) || (!szPath))
	{
		free(szSearch);
		free(szBuffer);
		return false;
	}

/* CONSTRUCT SEARCH PATH */
	strcpy(szSearch, szPath);
	strcat(szSearch, "*.*");

/* BEGIN SEARCH */
	hFile = FindFirstFile(szSearch,&fdData);
	if (hFile == INVALID_HANDLE_VALUE)
	{
		free(szSearch);
		free(szBuffer);
		return false;
	}

	do
	{
		/* Filenames with a length less than two aren't allowed with special ordering */
		if (bSpecialOrdering && (strlen(fdData.cFileName) < 2))
			continue;

		if (fdData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			if ( (strcmp(fdData.cFileName,".")) && (strcmp(fdData.cFileName,"..")) )
			{
				ConstructSortingName(szBuffer, fdData.cFileName, true);
				cslList.add(szBuffer);
			}
		}
		else if ( !(fdData.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN))
		{
			ConstructSortingName(szBuffer, fdData.cFileName, false);
			cslList.add(szBuffer);
		}
	}
	while (FindNextFile(hFile,&fdData));
	FindClose(hFile);
/* END SEARCH */

/* SORT LIST */
	cslList.sort();
	ulMax = cslList.count();
	ulCount = ulMax;
/* BUILD MENU */
	for (ulPos = 0; ulPos < ulMax; ulPos++)
	{
		bool bIsDir;

		szTemp = cslList.item(ulPos);
		bIsDir = SortingNameIsDir(szTemp);

		DestructSortingName(szBuffer, szTemp);
		stFileLength = strlen(szBuffer) + stLength;
		szFileName = (char *) malloc(stFileLength);
		if ( ! szFileName )
			break;

		//contruct file name
		strcpy(szFileName, szPath);
		strcat(szFileName, szBuffer );
		szTemp = GetCaption(szBuffer);

		if ( bIsDir )
		{
			//DIRECTORY
			strcat(szFileName, "\\");
			BuildDir( AddToMenu(hMenu, szTemp, itPopup, false), szFileName);
		}
		else
		{
			if ( IsScript(szTemp, &ulExclamation) )
				//This is a "Caption!Command" item
				HandleScriptItem(hMenu, szTemp, ulExclamation);
			else
				//Normal file
				if (cslItems.add(szFileName))
					AddToMenu(hMenu, szTemp, itAddToList, true);
		}

		free(szFileName);
	}

/* EMPTY MENU? */
	if ( ! ulCount )
		AppendMenu(hMenu,MF_STRING | MF_GRAYED,-1,"(Empty)");

/* CLEANUP */
	cslList.clear();
	free(szSearch);
	free(szBuffer);

	return true;
}

bool CLSMenu::IsScript(char *szFile, unsigned long *ulPos)
{
	int i = 0;

	while (szFile[i] && (szFile[i] != '!'))
		i++;

	*ulPos = i;
	return (szFile[i] == '!');
}

void CLSMenu::FormatCaption(char *szFile)
{
	int i = strlen(szFile);
	int j = -1;
	int k = -1;

	while (++j < i)
	{
		if (szFile[j] == '.')
			k = j;
	}

	if (k > 0)
		szFile[k] = '\0';
}

void CLSMenu::HandleScriptItem(HMENU hMenu, char *szFile, unsigned long ulExclamation)
{
	char *szCommand = &szFile[ulExclamation];
	char *szTemp;
	char *szBuffer;
	HMENU hSub;

	//if the filename is '<filename>!' (ie, no script command)
	if (strlen(szCommand) <= 1)
		return;

	*(szCommand++) = '\0';
	szTemp = szCommand;

	while (*szTemp)
	{
		*szTemp = toupper(*szTemp);
		szTemp++;
	}

	if ( ! strcmp(szCommand,"COMMONPROGRAMS") )
	{
		if ( (bIsNT) && (hSub = CreatePopupMenu()) )
		{
			AppendMenu(hMenu,MF_STRING | MF_POPUP, (UINT) hSub, szFile);
			BuildSpecialFolder(hSub, CSIDL_COMMON_PROGRAMS);
		}
	}
	else if ( ! strcmp(szCommand,"DESKTOP") )
	{
		if (hSub = CreatePopupMenu())
		{
			AppendMenu(hMenu,MF_STRING | MF_POPUP, (UINT) hSub, szFile);
			BuildSpecialFolder(hSub, CSIDL_DESKTOPDIRECTORY);
		}
	}
	else if ( ! strcmp(szCommand,"DOCS") )
	{
		if (hSub = CreatePopupMenu())
		{
			if (bIsStatic)
				hDocs = hSub;

			AppendMenu(hMenu,MF_STRING | MF_POPUP, (UINT) hSub, szFile);
			BuildSpecialFolder(hSub, CSIDL_RECENT);
		}
	}
	else if ( ! strcmp(szCommand,"DRIVES") )
	{
		if (hSub = CreatePopupMenu())
		{
			CStringList *pList = pcSysm->GetDrives();
			unsigned long ulCounter = 0;
			unsigned long ulMax = 0;
			AppendMenu(hMenu,MF_STRING | MF_POPUP, (UINT) hSub, szFile);

			if (pList)
			{
				ulMax = pList->count();
				for ( ; ulCounter < ulMax; )
				{
					cslItems.add(pList->item(ulCounter++));
					AddToMenu(hSub,pList->item(ulCounter++),itAddToList,false);
				}
			}
		}
	}
	else if ( ! strcmp(szCommand,"FAVOURITES") )
	{
		if (hSub = CreatePopupMenu())
		{
			AppendMenu(hMenu,MF_STRING | MF_POPUP, (UINT) hSub, szFile);
			BuildSpecialFolder(hSub, CSIDL_FAVORITES);
		}
	}
	else if ( ! strcmp(szCommand,"MYDOCS") )
	{
		if (hSub = CreatePopupMenu())
		{
			AppendMenu(hMenu,MF_STRING | MF_POPUP, (UINT) hSub, szFile);
			BuildSpecialFolder(hSub, CSIDL_PERSONAL);
		}
	}
	else if ( ! strcmp(szCommand,"PROGRAMS") )
	{
		if (hSub = CreatePopupMenu())
		{
			AppendMenu(hMenu,MF_STRING | MF_POPUP, (UINT) hSub, szFile);
			BuildSpecialFolder(hSub, CSIDL_PROGRAMS);
		}
	}
	else if ( ! strcmp(szCommand,"SEPARATOR") )
	{
		AppendMenu(hMenu,MF_SEPARATOR,0,NULL);
	}
	else if ( ! strcmp(szCommand,"STARTMENU") )
	{
		if (hSub = CreatePopupMenu())
		{
			AppendMenu(hMenu,MF_STRING | MF_POPUP, (UINT) hSub, szFile);
			szBuffer = pcSysm->GetFolderPath(CSIDL_STARTMENU);
			BuildDir(hSub,szBuffer);
			free(szBuffer);
		}
	}
	else if ( ! strcmp(szCommand,"TASKS") )
	{
		if (hSub = CreatePopupMenu())
		{
			if (bIsStatic)
				hTasks = hSub;

			AppendMenu(hMenu,MF_STRING | MF_POPUP, (UINT) hSub, szFile);
			BuildTaskList(hSub);			
		}
	}
	else if(! strcmp(szCommand,"TIME"))
	{
		// Submitted by Steve Lawson
		hTimeMenu = hMenu;
		uTimePos = GetMenuItemCount(hTimeMenu);

		InsertTimeItem(hTimeMenu, uTimePos);
	}
	else
	{
		*(--szCommand) = '!';
		cslItems.add(szCommand);
		*szCommand = '\0';
		AddToMenu(hMenu,szFile,itAddToList,false);
	}
}

HMENU CLSMenu::AddToMenu(HMENU hMenu, char *szCaption, enum ItemType itType, bool bDoFormat)
{
	HMENU hResult = NULL;

	if (!hMenu)
		return hResult;

	if (bDoFormat)
		FormatCaption(szCaption);

	if (itType == itPopup)
	{
		if (hResult = CreatePopupMenu())
			AppendMenu(hMenu,MF_STRING | MF_POPUP,(UINT) hResult,szCaption);
	}
	else if (itType == itAddToList)
		AppendMenu(hMenu,MF_STRING,cslItems.count() - 1,szCaption);
	else if (itType == itEnabled)
		AppendMenu(hMenu,MF_STRING,-1,szCaption);
	else
		AppendMenu(hMenu,MF_STRING | MF_GRAYED,-1,szCaption);

	return hResult;
}

void CLSMenu::BuildSpecialFolder(HMENU hSubMenu, int ID)
{
	char* szBuffer = pcSysm->GetFolderPath(ID);
	BuildDir(hSubMenu,szBuffer);
	free(szBuffer);
}

void CLSMenu::BuildTaskList(HMENU hSubMenu)
{
	unsigned long ulCounter = 0;
	unsigned long ulMax = 0;

	//Build tasks list
	cTasks.clear();
	EnumWindows(EnumTasks, NULL);
	ulMax = cTasks.count();

	for ( ; ulCounter < ulMax; )	
	{
		cslItems.add(cTasks.item(ulCounter++));

		//Ignore windows with no title
		char *szCaption = cTasks.item(ulCounter++);
		if ((szCaption) && (*szCaption))
			AddToMenu(hSubMenu,szCaption,itAddToList,false);
		
	}

	cTasks.clear();

	if (GetMenuItemCount(hSubMenu) == 0)
		AppendMenu(hSubMenu,MF_STRING | MF_GRAYED,0,"(none)");
}

void CLSMenu::InsertTimeItem(HMENU hMenu, UINT uPos)
{
	char str[10];
	char str2[24];

	GetTimeFormat(LOCALE_USER_DEFAULT, 0, 0, "h:mm tt", str, sizeof(str));
	GetDateFormat(LOCALE_USER_DEFAULT, 0, 0, "ddd, dd MMM  ", str2, sizeof(str2));
	strcat(str2, str);

	InsertMenu(hMenu, uPos, MF_BYPOSITION | MF_STRING | MF_DISABLED, -1, str2);
}

void CLSMenu::ClearMenu(HMENU hSubMenu)
{
	int iMax = GetMenuItemCount(hSubMenu);
	int iPos = 0;

	for ( ; iPos < iMax; iPos++)
		DeleteMenu(hSubMenu, 0, MF_BYPOSITION);
}

bool CLSMenu::Popup(HWND hWnd, int iX, int iY)
{
	if (bIsStatic)
	{
		ClearMenu(hDocs);
		ClearMenu(hTasks);
	
		//Rebuild Tasks, Documents and Time
		if (hDocs != NULL)
			BuildSpecialFolder(hDocs, CSIDL_RECENT);

		if (hTasks != NULL)
			BuildTaskList(hTasks);

		if (uTimePos != NULL)
		{
			DeleteMenu(hTimeMenu, uTimePos, MF_BYPOSITION);
			InsertTimeItem(hTimeMenu, uTimePos);
		}
	}
	else
		Build();

	return (TrackPopupMenu(hMenu, TPM_LEFTALIGN | TPM_LEFTBUTTON /*| TPM_RIGHTBUTTON*/, iX, iY, 0, hWnd, NULL) != 0);
}

HMENU CLSMenu::GetMenu(void)
{
	return hMenu;
}

char *CLSMenu::GetDir(void)
{
	size_t stLen = strlen(szMenu) + 1;
	char *szTemp = (char *) malloc(stLen);

	if (szTemp)
		strcpy(szTemp, szMenu);

	return szTemp;
}

char *CLSMenu::MenuItem(unsigned long ulItem)
{
	if (ulItem < cslItems.count())
		return cslItems.item(ulItem);
	else
		return NULL;
}

void CLSMenu::ConstructSortingName(char *szBuffer, char *szFile, bool bIsDir)
{
	char *szTemp = strchr(szFile, '#');
	memset(szBuffer, 0, MAX_PATH + 2);

	if (bSpecialOrdering && szTemp)
	{
		szTemp++[0] = NULL;

		strcpy(szBuffer, szFile);
		if (bIsDir)
			strcat(szBuffer, "#d");
		else
			strcat(szBuffer, "#f");
		strcat(szBuffer, szTemp);
		--szTemp[0] = '#';
	}
	else if (bSpecialOrdering)
	{
		strcpy(szBuffer, szFile);

		if (bIsDir)
			strcat(szBuffer,"d");
		else
			strcat(szBuffer,"f");
	}
	else if (bIsDir)
	{
		strcpy(szBuffer, "*");
		strcat(szBuffer, szFile);
	}
	else
		strcpy(szBuffer, szFile);
}

void CLSMenu::DestructSortingName(char *szBuffer, char *szFile)
{
	char *szTemp = strchr(szFile, '#');
	memset(szBuffer, 0, MAX_PATH + 2);

	if (bSpecialOrdering && szTemp)
	{
		szTemp[1] = NULL;
		szTemp += 2;

		strcpy(szBuffer, szFile);
		strcat(szBuffer, szTemp);
	}
	else if (bSpecialOrdering)
	{
		if (*szFile)
		{
			int iLen = strlen(szFile);

			strcpy(szBuffer, szFile);
			szBuffer[--iLen] = NULL;
		}
		else
			*szBuffer = NULL;
	}
	else if (szFile[0] == '*')
		strcpy(szBuffer, (szFile + 1));
	else
		strcpy(szBuffer, szFile);
}

bool CLSMenu::SortingNameIsDir(char *szFile)
{
	char *szTemp = strchr(szFile, '#');
	int iLen = strlen(szFile);
	if (bSpecialOrdering && szTemp)
		return (szTemp[1] == 'd');
	else if (bSpecialOrdering)		
		return (szFile[--iLen] == 'd');
	else
		return (szFile[0] == '*');
}

char *CLSMenu::GetCaption(char *szFile)
{
	char *szTemp = strchr(szFile, '#');
	if (bSpecialOrdering && szTemp)
		return (szTemp + 1);
	else
		return szFile;
}

bool CLSMenu::SetMenuDir(CRegistry *pcRegs, CLSSystem *pcSysm)
{
	if ((!pcRegs) || (!pcSysm))
		return false;

	char *szBuffer = (char *) pcRegs->ReadString("MenuDir");

	if ( ! szBuffer)
		if ( ! (szBuffer = pcSysm->GetMenuDir()) )
			return false;
		else
		{
			SetPath(szBuffer, pcSysm);
			pcRegs->WriteString("MenuDir",(unsigned char *) szBuffer);
			free(szBuffer);
		}
	else
	{
		SetPath(szBuffer, pcSysm);
		free(szBuffer);
	}

	return true;
}