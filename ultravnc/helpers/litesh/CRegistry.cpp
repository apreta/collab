/*******************************************************************************
CRegistry

  used to interface with the Windows Registry
  
  (c) Robert Moss, 5th February 2002
*******************************************************************************/

#include <windows.h>
#include "CStringList.h"
#include "CRegistry.h"

CRegistry::CRegistry(void)
{
	bOpen = false;
}

CRegistry::~CRegistry(void)
{
	if (bOpen)
		Close();
}

bool CRegistry::SetKey(HKEY hKey)
{
	if (bOpen)
		if ( !Close() )
			return false;

	hRootKey = hKey;
	return true;
}

bool CRegistry::Open(const char *szSubKey)
{
	if (bOpen)
		if ( !Close() )
			return false;

	return (bOpen = ( RegOpenKeyEx(hRootKey, szSubKey, 0, KEY_ALL_ACCESS, &hCurrKey) == ERROR_SUCCESS ));
}

bool CRegistry::Close(void)
{
	if (bOpen)
	{
		bOpen = ! (RegCloseKey(hCurrKey) == ERROR_SUCCESS);
		return !bOpen;
	}
	else
		return true;
}

bool CRegistry::Delete(char *szSubKey)
{
	return (RegDeleteKey(hRootKey, szSubKey) == ERROR_SUCCESS);
}

bool CRegistry::DeleteValues(void)
{
	CStringList cList;
	unsigned long i = 0;
	unsigned long max = 0;

	EnumValues(&cList,false);
	max = cList.count();

	for ( ; i < max; i++)
		if (RegDeleteValue(hCurrKey,cList.item(i)) != ERROR_SUCCESS)
			return false;

	return true;
}

unsigned char *CRegistry::GetValue(char *szValueName, DWORD *pdwType)
{
	unsigned char *szBuffer;
	DWORD dwType;
	DWORD dwSize = 0;

	szBuffer = (unsigned char *) malloc(1);

	if (RegQueryValueEx(hCurrKey, szValueName, 0, &dwType, szBuffer, &dwSize) != ERROR_MORE_DATA)
	{
		free(szBuffer);
		return NULL;
	}

	if (pdwType)
		*pdwType = dwType;

	if (dwSize > 1)
	{
		free(szBuffer);
		szBuffer = (unsigned char *) malloc(dwSize);
		if (!szBuffer)
			return NULL;
	}

	if (RegQueryValueEx(hCurrKey, szValueName, 0, &dwType, szBuffer, &dwSize) != ERROR_SUCCESS)
	{
		free(szBuffer);
		return NULL;
	}

	return szBuffer;
}

unsigned char *CRegistry::ReadString(char *szValueName)
{
	unsigned char *szBuffer;
	DWORD dwType;
	
	szBuffer = GetValue(szValueName, &dwType);
	if (dwType != REG_SZ)
	{
		free(szBuffer);
		return NULL;
	}

	return szBuffer;
}

bool CRegistry::ReadBoolean(char *szValueName, bool bDefault)
{
	unsigned char *szBuffer;
	DWORD dwType;
	
	szBuffer = GetValue(szValueName, &dwType);

	if (dwType == REG_DWORD)
		bDefault = (*szBuffer != 0);

	free(szBuffer);

	return bDefault;
}

DWORD CRegistry::ReadDWORD(char *szValueName, DWORD dwDefault)
{
	unsigned char *szBuffer;
	DWORD dwType;
	
	szBuffer = GetValue(szValueName, &dwType);

	if (dwType == REG_DWORD)
		//convert (unsigned char *) to DWORD manually
		dwDefault = szBuffer[0] + (szBuffer[1] << 8) + (szBuffer[2] << 16) + (szBuffer[3] << 24);

	free(szBuffer);

	return dwDefault;
}

bool CRegistry::WriteString(char *szValueName, unsigned char *szValue)
{
	DWORD dwSize = strlen((const char *) szValue) + 1;

	return (RegSetValueEx(hCurrKey, szValueName, 0, REG_SZ, szValue, dwSize) == ERROR_SUCCESS);
}

bool CRegistry::WriteBoolean(char *szValueName, bool bValue)
{
	DWORD dwValue = (bValue) ? 1 : 0;
	return (RegSetValueEx(hCurrKey, szValueName, 0, REG_DWORD, (const unsigned char *) &dwValue, sizeof(dwValue)) == ERROR_SUCCESS);
}

bool CRegistry::WriteDWORD(char *szValueName, DWORD dwValue)
{
	return (RegSetValueEx(hCurrKey, szValueName, 0, REG_DWORD, (const unsigned char *) &dwValue, sizeof(dwValue)) == ERROR_SUCCESS);
}

bool CRegistry::EnumKeys(CStringList *pslList)
{
	DWORD dwIndex = 0;
	DWORD dwSize = MAX_PATH + 1;
	char *szBuffer;
	LONG lResult;

	if (! pslList)
		return false;

	szBuffer = (char *) malloc(dwSize);
	lResult = RegEnumKeyEx(hCurrKey,dwIndex++,szBuffer,&dwSize,0,NULL,NULL,NULL);

	while (lResult == ERROR_SUCCESS)
	{
		pslList->add(szBuffer);
		free(szBuffer);
		dwSize = MAX_PATH + 1;
		szBuffer = (char *) malloc(dwSize);
		if (!szBuffer)
			return false;
		lResult = RegEnumKeyEx(hCurrKey,dwIndex++,szBuffer,&dwSize,0,NULL,NULL,NULL);
	}

	free(szBuffer);

	return true;
}

bool CRegistry::EnumValues(CStringList *pslList, bool bGetValues)
{
	DWORD dwIndex = 0;
	DWORD dwSize = MAX_PATH + 1;
	DWORD dwValSize = (bGetValues) ? MAX_PATH + 1 : 0;
	char *szBuffer;
	unsigned char *szValue = NULL;
	LONG lResult;

	if (! pslList)
		return false;

	szBuffer = (char *) malloc(dwSize);
	if (bGetValues)
		szValue  = (unsigned char *) malloc(dwSize);

	lResult = RegEnumValue(hCurrKey,dwIndex++,szBuffer,&dwSize,0,NULL,szValue,&dwValSize);

	while (lResult == ERROR_SUCCESS)
	{
		pslList->add(szBuffer);
		free(szBuffer);
		dwSize = MAX_PATH + 1;
		szBuffer = (char *) malloc(dwSize);

		if (bGetValues)
		{
			pslList->add((char *) szValue);
			free(szValue);
			dwValSize = dwSize;
			szValue  = (unsigned char *) malloc(dwValSize);
			if (!szValue)
			{
				if (szBuffer)
					free(szBuffer);
				return false;
			}
		}

		if (!szBuffer)
		{
			if (szValue)
				free(szValue);
			return false;
		}

		lResult = RegEnumValue(hCurrKey,dwIndex++,szBuffer,&dwSize,0,NULL,szValue,&dwValSize);
	}

	free(szValue);
	free(szBuffer);

	return true;
}

HKEY CRegistry::GetKey(void)
{
	return hCurrKey;
}