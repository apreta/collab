/*******************************************************************************
CLSPlugin

  used to handle loading/unloading of plugins
  
  (c) Robert Moss, 5th February 2002
*******************************************************************************/

#include <windows.h>
#include "CStringList.h"
#include "CRegistry.h"
#include "CLSPlugin.h"

#define SZ_LOADPROC_NAME "LS_Plugin_Load"

/* handle static members */
HWND CLSPlugin::hDesktop = NULL;
HINSTANCE CLSPlugin::hInst = NULL;
bool CLSPlugin::bLoadedAll = false;
CStringList *CLSPlugin::pslPlugins = new CStringList;

CLSPlugin::CLSPlugin(void)
{
	bLoaded = false;
	hWnd = NULL;
	hLib = NULL;
	lpLoad = NULL;
}

CLSPlugin::~CLSPlugin(void)
{
	if (bLoaded)
		Unload();
}

bool CLSPlugin::Load(char *szLibrary, int iPluginID)
{
	if (bLoaded || (!szLibrary) || (!hDesktop))
		return false;

	if ( ! (hLib = LoadLibrary(szLibrary)) )
		return false;

	if ( ! (lpLoad = (LPLOADPROC) GetProcAddress(hLib, SZ_LOADPROC_NAME)) )
	{
		FreeLibrary(hLib);
		return false;
	}

	if ( ! (hWnd = lpLoad(hInst, hDesktop, iPluginID)) )
	{
		MessageBox(hDesktop, "Could not load plugin", szLibrary, 0);
		FreeLibrary(hLib);
		return false;
	}

	bLoaded = true;
	return true;
}

bool CLSPlugin::Unload(void)
{
	if (!bLoaded)
		return true;

	/* use PostMessage instead? */
	SendMessage(hWnd, PM_CLOSE, 0, 0);

	return (FreeLibrary(hLib) != 0);
}

void CLSPlugin::Initialise(HINSTANCE hInstance, HWND hWnd)
{
	hDesktop = hWnd;
	hInst = hInstance;
}

void CLSPlugin::LoadPlugins(CRegistry *pcRegs)
{
	if (bLoadedAll || (!pslPlugins) || (!pcRegs))
		return;

	bLoadedAll = true;

	unsigned long ulPos = 0;
	unsigned long ulMax;
	unsigned long ulID;
	CStringList slLibraries;
	CLSPlugin *cpPlugin;

	pcRegs->EnumValues(&slLibraries, false);

	ulMax = slLibraries.count();
	for ( ; ulPos < ulMax; ulPos++)
		if (cpPlugin = new CLSPlugin)
		{
			ulID = pslPlugins->count();
			if (cpPlugin->Load(slLibraries.item(ulPos), ulID))
				pslPlugins->add((char *) cpPlugin);
		}
}

void CLSPlugin::UnloadPlugins(void)
{
	if ((!bLoadedAll) || (!pslPlugins))
		return;

	unsigned long ulPos = 0;
	unsigned long ulMax = pslPlugins->count();
	CLSPlugin *cpPlugin;

	for ( ; ulPos < ulMax; ulPos++)
		if (cpPlugin = (CLSPlugin *) pslPlugins->item(ulPos))
			cpPlugin ->Unload();

	pslPlugins->clear();
}

HWND CLSPlugin::GetPluginWnd(int iPluginID)
{
	// convert iPluginID to unsigned int
	unsigned int uiPluginID = (iPluginID < 0) ? 0 : iPluginID;

	if ((!pslPlugins) || (uiPluginID >= pslPlugins->count()))
		return NULL;
	else
		return ((CLSPlugin *) pslPlugins->item(uiPluginID))->hWnd;
}