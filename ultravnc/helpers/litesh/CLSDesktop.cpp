/*******************************************************************************
CLSDesktop

  implements the LiteShell desktop window
  
  (c) Robert Moss, 20th February 2002
*******************************************************************************/

#include <Windows.h>
#include "CStringList.h"
#include "CLSSystem.h"
#include "CRegistry.h"
#include "CLSMenu.h"
#include "CLSDesktop.h"

#define szAppName "LiteShell"
#define SM_XVIRTUALSCREEN       76
#define SM_YVIRTUALSCREEN       77
#define SM_CXVIRTUALSCREEN      78
#define SM_CYVIRTUALSCREEN      79

/* Used for hiding minimised windows */
#ifndef ARW_HIDE
#define ARW_HIDE 0x0008L
#endif
#define MONITOR_DEFAULTTONULL       0x00000000

CLSDesktop::CLSDesktop(void)
{
	hDesktop = 0;
	deviceName=NULL;
}

CLSDesktop::~CLSDesktop(void)
{
	;
}

bool CLSDesktop::Create(HINSTANCE hInstance, WNDPROC wpProc, POINT pSize, bool bShow, bool bHideMin)
{
///find size off pseudo desktop
	typedef BOOL (WINAPI* pEnumDisplayDevices)(PVOID,DWORD,PVOID,DWORD);
	typedef HMONITOR (WINAPI* pMonitorFromPoint)(POINT, DWORD);
	DWORD  Width; 
	DWORD  Height;
	POINT point;
	pEnumDisplayDevices pd;
	pMonitorFromPoint pm;
	LPSTR driverName = "vncvirdr";
	DEVMODE devmode;
	FillMemory(&devmode, sizeof(DEVMODE), 0);
	devmode.dmSize = sizeof(DEVMODE);
	devmode.dmDriverExtra = 0;
	BOOL change = EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&devmode);
	devmode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
	pd = (pEnumDisplayDevices)GetProcAddress( LoadLibrary("USER32"), "EnumDisplayDevicesA");
	pm = (pMonitorFromPoint)GetProcAddress( LoadLibrary("USER32"), "MonitorFromPoint");
			if (pd)
				{
					DISPLAY_DEVICE dd;
					ZeroMemory(&dd, sizeof(dd));
					dd.cb = sizeof(dd);
					devmode.dmDeviceName[0] = '\0';
					INT devNum = 0;
					BOOL result;
					while (result = (*pd)(NULL,devNum, &dd,0))
					{
						if (strcmp((const char *)&dd.DeviceString[0], driverName) == 0)
						break;
						devNum++;
					}
					deviceName = (LPSTR)&dd.DeviceName[0];
					EnumDisplaySettings(deviceName,ENUM_CURRENT_SETTINGS,&devmode);
					m_ScreenOffsetx=devmode.dmPosition.x;
					m_ScreenOffsety=devmode.dmPosition.y;
					Width=devmode.dmPelsWidth; 
					Height=devmode.dmPelsHeight;
					point.x=devmode.dmPosition.x+5;
					point.y=devmode.dmPosition.y+5;
					hMonitor=(*pm)(point,MONITOR_DEFAULTTONULL);
					point.x=devmode.dmPosition.x-5;
					point.y=devmode.dmPosition.y-5;
					hMonitor=(*pm)(point,MONITOR_DEFAULTTONULL);
				}

	int a=GetSystemMetrics(SM_CXVIRTUALSCREEN);
	int b=GetSystemMetrics(SM_CYVIRTUALSCREEN);
	int c=GetSystemMetrics(SM_CXSCREEN);
	int d=GetSystemMetrics(SM_CYSCREEN);

	WNDCLASSEX wcDesktop;
	MINIMIZEDMETRICS mmNew;

	memset(&wcDesktop,0,sizeof(WNDCLASSEX));
	wcDesktop.cbSize = sizeof(WNDCLASSEX);
	wcDesktop.hInstance = hInstance;
	wcDesktop.hCursor = LoadCursor(0, IDC_ARROW);
	wcDesktop.hbrBackground = (HBRUSH) COLOR_WINDOW;
	wcDesktop.hIcon = LoadIcon(0, IDI_APPLICATION);
	wcDesktop.lpszClassName = szAppName;
	wcDesktop.style = CS_DBLCLKS;
	wcDesktop.lpfnWndProc = wpProc;

	if ( ! RegisterClassEx(&wcDesktop) )
		return false;

	if (! (hDesktop = CreateWindowEx(WS_EX_TOOLWINDOW,szAppName,szAppName,WS_POPUP  | WS_CLIPSIBLINGS | WS_CLIPCHILDREN|WS_BORDER ,m_ScreenOffsetx+10,
		m_ScreenOffsety+10,Width-20,Height-20,GetDesktopWindow(),NULL,hInstance,NULL) ))
		return false;
	HWND hDesk1,hDesk2,hDesk3,hDesk4;
	hDesk1=CreateWindowEx(WS_EX_TOOLWINDOW,szAppName,szAppName,WS_POPUP  | WS_CLIPSIBLINGS | WS_CLIPCHILDREN|WS_BORDER ,m_ScreenOffsetx,
		m_ScreenOffsety,Width,10,GetDesktopWindow(),NULL,hInstance,NULL);
	hDesk2=CreateWindowEx(WS_EX_TOOLWINDOW,szAppName,szAppName,WS_POPUP  | WS_CLIPSIBLINGS | WS_CLIPCHILDREN|WS_BORDER ,m_ScreenOffsetx,
		m_ScreenOffsety,10,Height,GetDesktopWindow(),NULL,hInstance,NULL);
	hDesk3=CreateWindowEx(WS_EX_TOOLWINDOW,szAppName,szAppName,WS_POPUP  | WS_CLIPSIBLINGS | WS_CLIPCHILDREN|WS_BORDER ,m_ScreenOffsetx,
		m_ScreenOffsety+Height-10,Width,10,GetDesktopWindow(),NULL,hInstance,NULL);
	hDesk4=CreateWindowEx(WS_EX_TOOLWINDOW,szAppName,szAppName,WS_POPUP  | WS_CLIPSIBLINGS | WS_CLIPCHILDREN|WS_BORDER ,m_ScreenOffsetx+Width-10,
		m_ScreenOffsety,10,Height,GetDesktopWindow(),NULL,hInstance,NULL);


	SetWindowPos(hDesktop, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
	SetWindowPos(hDesk1, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
	SetWindowPos(hDesk2, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
	SetWindowPos(hDesk3, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
	SetWindowPos(hDesk4, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOSIZE | SWP_NOMOVE);
	ShowWindow(hDesk1,SW_SHOWNORMAL);
	ShowWindow(hDesk2,SW_SHOWNORMAL);
	ShowWindow(hDesk3,SW_SHOWNORMAL);
	ShowWindow(hDesk4,SW_SHOWNORMAL);
	Show(bShow);

	/* Hide minimised windows? */
	if (bHideMin)
	{
		/* Save original metrics */
		mmOrig.cbSize = sizeof(mmOrig);
		SystemParametersInfo(SPI_GETMINIMIZEDMETRICS,sizeof(mmOrig),&mmOrig,NULL);

		/* Hide minimised windows */
		mmNew.cbSize = sizeof(mmNew);
		mmNew.iWidth = 0;
		mmNew.iHorzGap = 0;
		mmNew.iVertGap = 0;
		mmNew.iArrange = ARW_BOTTOMLEFT | ARW_HIDE;
		SystemParametersInfo(SPI_SETMINIMIZEDMETRICS,sizeof(mmNew),&mmNew,SPIF_SENDCHANGE);
	}

	return true;
}

bool CLSDesktop::Show(bool bShow)
{
	if (bShow)
		return (ShowWindow(hDesktop,SW_SHOWNORMAL) != 0);
	else
		return (ShowWindow(hDesktop,SW_HIDE) != 0);
}

bool CLSDesktop::Resize(POINT pSize)
{
	return (SetWindowPos(hDesktop,0,0,0,pSize.x,pSize.y, SWP_NOMOVE | SWP_NOZORDER) != 0);
}

HWND CLSDesktop::GetWnd(void)
{
	return hDesktop;
}

bool CLSDesktop::Close(void)
{
	/* Restore original metrics */
	SystemParametersInfo(SPI_SETMINIMIZEDMETRICS,sizeof(mmOrig),&mmOrig,SPIF_SENDCHANGE);

	return (DestroyWindow(hDesktop) != 0);
}