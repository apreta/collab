/*******************************************************************************
CStringList

  used to maintain a dynamic collection of strings, with the ability to sort the
  collection using qsort and _stricmp for case-insensitive comparisons.

  (c) Robert Moss, 5th February 2002
*******************************************************************************/

#ifndef CSTRINGLIST_H
#define CSTRINGLIST_H

class CStringList
{
protected:
	unsigned long ulCount;
	unsigned long ulSize;
	char ** ppcItems;

	static int sort_proc_s(char **item1, char **item2);
	static int sort_proc_i(char **item1, char **item2);

public:
	CStringList(void);
	~CStringList(void);

	unsigned long count(void);
	bool add(char *str);
	bool del(unsigned long pos);
	char * item(unsigned long pos);

	bool sort(bool bCaseSensitive = false);
	bool clear(void);
};

#endif