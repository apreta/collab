 /////////////////////////////////////////////////////////////
// cmdasuser.cpp
//
// Copyright 1999, Keith Brown
// http://www.develop.com/kbrown
//
// Command line utility that executes a command shell as
// the specified user. Loads the user's profile (creating one
// if necessary), and creates a correct environment block
// for the new process. Demonstrates manipulation of winsta
// and desktop DACLs, granting access to logon session SIDs.
//
// To execute this application, you must be an administrator.
// The principal you specify as a command line argument
// must have the "Log on Locally" privilege on the machine
// (unless you're running on a domain conroller, everyone has
// this privilege by default, so this shouldn't be an issue).
//
// Tested on Windows NT 4.0 SP3, SP4, and Windows 2000 Beta 3.
//
// Usage: cmdasuser authority\principal password
//        cmdasuser localsystem
//
// Ultr@vnc 2002
// changes: use vnc registry  to pass some data
/////////////////////////////////////////////////////////////
#define MAXAWLEN 253
#define _WIN32_WINNT 0x400
#define UNICODE
#include <windows.h>
#include <stdio.h>
#include <aclapi.h>
#include "TempService.h"

// userenv.h can be found in the Windows 2000 beta SDK,
// and you'll need it to build this project...
#include "userenv.h"

// for some reason, winuser.h doesn't define these useful aliases
#ifndef WINSTA_ALL_ACCESS
#define WINSTA_ALL_ACCESS 0x0000037F
#endif

#ifndef DESKTOP_ALL_ACCESS
#define DESKTOP_ALL_ACCESS 0x00001FF
#endif

HWND m_hwnd_appli=NULL;
void
LoadApplic(HKEY key, char *buffer)
{
	DWORD type = REG_BINARY;
	int slen=256;
	char inouttext[256];

	// Retrieve the encrypted password
	if (RegQueryValueEx(key,
		TEXT("Path2"),
		NULL,
		&type,
		(LPBYTE) &inouttext,
		(LPDWORD) &slen) != ERROR_SUCCESS)
		return;

	if (slen > 256)
		return;

	memcpy(buffer, inouttext, MAXAWLEN);
}


//rdv callback for autostart 
BOOL CALLBACK
EnumWindowsHnd(HWND hwnd, LPARAM lParam)
{
  DWORD dwThreadId;
  DWORD dwProcessId;
  PROCESS_INFORMATION* pi = ( PROCESS_INFORMATION* )lParam;

  dwThreadId = GetWindowThreadProcessId(hwnd, &dwProcessId);
  if ( dwProcessId == pi->dwProcessId ) {
	   m_hwnd_appli = hwnd;
       return TRUE;
  }
  return TRUE;
}

static bool m_bDumpErrorsToEventLog = false;

HANDLE g_hProcessHeap = GetProcessHeap();

void Quit( const wchar_t* pszMsg, int nExitCode = 1 )
{
	if ( m_bDumpErrorsToEventLog )
	{
		HANDLE h = RegisterEventSource( 0, L"cmdasuser" );
		ReportEvent( h, EVENTLOG_ERROR_TYPE, 0, 0, 0, 1, 0, &pszMsg, 0 );
		DeregisterEventSource( h );
	}
	else wprintf( L"%s\n", pszMsg );
	exit( nExitCode );
}

void PrintUsageAndQuit()
{
	Quit( L"Usage: cmdasuser authority\\principal password\n       cmdasuser localsystem" );
}

void Err( const wchar_t* pszFcn, DWORD nErr = GetLastError() )
{
	wchar_t szErrMsg[256];
	wchar_t szMsg[512];
	if ( FormatMessage( FORMAT_MESSAGE_FROM_SYSTEM, 0, nErr, 0, szErrMsg, sizeof szErrMsg / sizeof *szErrMsg, 0 ) )
		 swprintf( szMsg, L"%s failed: %s", pszFcn, szErrMsg );
	else swprintf( szMsg, L"%s failed: 0x%08X", pszFcn, nErr );
	Quit( szMsg );
}

bool instr( const wchar_t* psz, wchar_t c )
{
	while ( *psz )
		if ( *psz++ == c )
			return true;
	return false;
}

wchar_t* ParseDomainAndPrincipal( wchar_t* psz, const wchar_t*& pszDomain )
{
	pszDomain = psz;
	while ( '\\' != *psz )
		++psz;
	*psz++ = '\0'; // terminate domain name
	return psz;    // principal name starts here
}

ACL* GetUserObjectDacl( HANDLE h, DWORD cbExtra )
{
	// At least one person complained about GetSecurityInfo failing with
	// E_INVALIDARG, so I changed the code to use the older API
	// and this seemed to fix the problem.
	// But the GetSecurityInfo version was soooo much cleaner :-)
	DWORD cb = 0;
	SECURITY_INFORMATION si = DACL_SECURITY_INFORMATION;
	GetUserObjectSecurity( h, &si, 0, cb, &cb );
	if ( ERROR_INSUFFICIENT_BUFFER != GetLastError() )
		Err( L"GetUserObjectSecurity" );

	void* psd = HeapAlloc( g_hProcessHeap, 0, cb );
	if ( !psd )
		Err( L"HeapAlloc", E_OUTOFMEMORY );

	si = DACL_SECURITY_INFORMATION;
	if ( !GetUserObjectSecurity( h, &si, psd, cb, &cb ) )
		Err( L"GetUserObjectSecurity" );

	BOOL bPresent, bDefaulted;
	ACL* pdaclOld;
	if ( !GetSecurityDescriptorDacl( psd, &bPresent, &pdaclOld, &bDefaulted ) )
		Err( L"GetSecurityDescriptorDacl" );

	// get the size of the existing DACL
	ACL_SIZE_INFORMATION info;
	if ( !GetAclInformation( pdaclOld, &info, sizeof info, AclSizeInformation ) )
		Err( L"GetAclInformation" );

	// allocate enough memory for the existing DACL
	// plus whatever extra space the caller requires
	cb = info.AclBytesInUse + cbExtra;
	ACL* pdaclNew = reinterpret_cast<ACL*>( HeapAlloc( g_hProcessHeap, 0, cb ) );
	if ( !pdaclNew )
		Err( L"HeapAlloc" );

	if ( !InitializeAcl( pdaclNew, cb, ACL_REVISION ) )
		Err( L"InitializeAcl" );
	
	// copy over all the old aces to the new DACL
	for ( DWORD i = 0; i < info.AceCount; ++i )
	{
		ACE_HEADER* pace = 0;
		if ( !GetAce( pdaclOld, i, reinterpret_cast<void**>(&pace) ) )
			Err( L"GetAce" );

		if ( !AddAce( pdaclNew, ACL_REVISION, MAXDWORD, pace, pace->AceSize ) )
			Err( L"AddAce" );
	}
	HeapFree( g_hProcessHeap, 0, psd );
	return pdaclNew;
}

DWORD CalcAceSizeFromSid( void* psid )
{
	return sizeof( ACCESS_ALLOWED_ACE ) - sizeof( DWORD ) + GetLengthSid( psid );
}

#if _WIN32_WINNT < 0x500
// this is such a simple function, it's bizarre that we had to wait 'till W2K to get it
BOOL AddAccessAllowedAceEx( PACL pAcl, DWORD dwAceRevision, DWORD AceFlags, DWORD AccessMask, PSID pSid )
{
	if ( !AddAccessAllowedAce( pAcl, dwAceRevision, AccessMask, pSid ) )
		return FALSE;
	ACL_SIZE_INFORMATION info;
	if ( !GetAclInformation( pAcl, &info, sizeof info, AclSizeInformation ) )
		return FALSE;
	ACE_HEADER* pace = 0;
	if ( !GetAce( pAcl, info.AceCount - 1, reinterpret_cast<void**>(&pace) ) )
		return FALSE;
	pace->AceFlags = static_cast<BYTE>(AceFlags);
	return TRUE;
}
#endif

void DeleteMatchingAces( ACL* pdacl, void* psid )
{
	ACL_SIZE_INFORMATION info;
	if ( !GetAclInformation( pdacl, &info, sizeof info, AclSizeInformation ) )
		Err( L"GetAclInformation" );

	// it's a bit easier to delete aces while iterating backwards
	// so that the iterator doesn't get honked up
	DWORD i = info.AceCount;
	while ( i-- )
	{
		ACCESS_ALLOWED_ACE* pAce = 0;
		if ( !GetAce( pdacl, i, reinterpret_cast<void**>(&pAce) ) )
			Err( L"GetAce" );
		if ( EqualSid( psid, &pAce->SidStart ) )
			DeleteAce( pdacl, i );
	}
}

void GrantSessionSIDAccessToWinstationAndDesktop( HANDLE htok, bool bGrant )
{
	BYTE tgs[4096];
	DWORD cbtgs = sizeof tgs;
	if ( !GetTokenInformation( htok, TokenGroups, tgs, cbtgs, &cbtgs ) )
		Err( L"GetTokenInformation" );
	const TOKEN_GROUPS* ptgs = reinterpret_cast<TOKEN_GROUPS*>(tgs);
	const SID_AND_ATTRIBUTES* it = ptgs->Groups;
	const SID_AND_ATTRIBUTES* end = it + ptgs->GroupCount;
	while ( end != it )
	{
		if ( it->Attributes & SE_GROUP_LOGON_ID )
			break;
		++it;
	}
	if ( end == it )
		Err( L"UNEXPECTED: No Logon SID in TokenGroups" );

	void* psidLogonSession = it->Sid;

	HWINSTA hws = GetProcessWindowStation();
	if ( !hws )
		Err( L"GetProcessWindowStation" );

	{
		ACL* pdacl = 0;
		if ( bGrant )
		{
			// get the existing DACL, with enough extra space for adding a couple more aces
			const DWORD cbExtra = 2 * CalcAceSizeFromSid( psidLogonSession );
			pdacl = GetUserObjectDacl( hws, cbExtra );

			// grant the logon session all access to winsta0
			if ( !AddAccessAllowedAce( pdacl, ACL_REVISION,
										WINSTA_ALL_ACCESS | STANDARD_RIGHTS_REQUIRED,
										psidLogonSession ) )
				Err( L"AddAccessAllowedAce" );

			// grant the logon session all access to any new desktops created in winsta0
			// by adding an inherit-only ace
			if ( !AddAccessAllowedAceEx( pdacl, ACL_REVISION,
										SUB_CONTAINERS_AND_OBJECTS_INHERIT | INHERIT_ONLY,
										DESKTOP_ALL_ACCESS | STANDARD_RIGHTS_REQUIRED,
										psidLogonSession ) )
				Err( L"AddAccessAllowedAceEx" );
		}
		else
		{
			pdacl = GetUserObjectDacl( hws, 0 );
			DeleteMatchingAces( pdacl, psidLogonSession );
		}

		// apply the changes to winsta0
		// unlike GetSecurityInfo, SetSecurityInfo has no troubles on SP3 :-)
		DWORD err = SetSecurityInfo( hws, SE_WINDOW_OBJECT, DACL_SECURITY_INFORMATION, 0, 0, pdacl, 0 );
		if ( err )
			Err( L"SetSecurityInfo", err );

		HeapFree( g_hProcessHeap, 0, pdacl );
	}

	CloseWindowStation( hws );

	HDESK hd = GetThreadDesktop( GetCurrentThreadId() );
	if ( !hd )
		Err( L"GetThreadDesktop" );

	{
		ACL* pdacl = 0;
		if ( bGrant )
		{
			// get the existing DACL, with enough extra space for adding a couple more aces
			const DWORD cbExtra = CalcAceSizeFromSid( psidLogonSession );
			pdacl = GetUserObjectDacl( hd, cbExtra );

			// grant the logon session all access to the default desktop
			if ( !AddAccessAllowedAce( pdacl, ACL_REVISION,
										DESKTOP_ALL_ACCESS | STANDARD_RIGHTS_REQUIRED,
										psidLogonSession ) )
				Err( L"AddAccessAllowedAce" );

		}
		else
		{
			pdacl = GetUserObjectDacl( hd, 0 );
			DeleteMatchingAces( pdacl, psidLogonSession );
		}

		// apply the changes to the default desktop
		// unlike GetSecurityInfo, SetSecurityInfo has no troubles on SP3 :-)
		DWORD err = SetSecurityInfo( hd, SE_WINDOW_OBJECT, DACL_SECURITY_INFORMATION, 0, 0, pdacl, 0 );
		if ( err )
			Err( L"SetSecurityInfo", err );

		HeapFree( g_hProcessHeap, 0, pdacl );
	}

	CloseDesktop( hd );
}

class UserEnvInterface
{
public:
	// signatures of entrypoints into userenv.dll
	typedef BOOL (WINAPI * CREATE_ENVIRONMENT_BLOCK) ( void**, HANDLE, BOOL );
	typedef BOOL (WINAPI * DESTROY_ENVIRONMENT_BLOCK) ( void* );
	typedef BOOL (WINAPI * LOAD_USER_PROFILE) ( HANDLE, PROFILEINFO* );
	typedef BOOL (WINAPI * UNLOAD_USER_PROFILE) ( HANDLE, HANDLE );

	UserEnvInterface()
	{
		m_hModule = LoadLibrary( L"userenv.dll" );
		if ( !m_hModule )
			Err( L"LoadLibrary( userenv.dll )" );

		_getAddr( "CreateEnvironmentBlock",  &m_createEnvironmentBlock );
		_getAddr( "DestroyEnvironmentBlock", &m_destroyEnvironmentBlock );
		_getAddr( "LoadUserProfileW",		 &m_loadUserProfile );
		_getAddr( "UnloadUserProfile",		 &m_unloadUserProfile );
	}
	~UserEnvInterface()	{ FreeLibrary( m_hModule );	}
	template <typename T>
	void _getAddr( const char* psz, T* pProc )
	{
		*pProc = reinterpret_cast<T>( GetProcAddress( m_hModule, psz ) );
		if ( !*pProc )
			Err( L"GetProcAddress" );
	}
	CREATE_ENVIRONMENT_BLOCK	m_createEnvironmentBlock;
	DESTROY_ENVIRONMENT_BLOCK	m_destroyEnvironmentBlock;
	LOAD_USER_PROFILE			m_loadUserProfile;
	UNLOAD_USER_PROFILE			m_unloadUserProfile;
private:
	HMODULE	m_hModule;
};

// STEP 3: we're now inside the "monitor" process which will block
//         until the target process dies, allowing us to clean up
//         the profile we loaded, as well as the winsta/desktop dacls.
void CmdAsUser( wchar_t* pszAccount, const wchar_t* pszPassword )
{
	// called within monitor process to spawn target
	// after loading user profile. When the target app exits,
	// we unload the user profile and exit the monitor app.
	const wchar_t* pszAuthority = L".";
	const wchar_t* pszPrincipal = instr( pszAccount, L'\\' ) ?
		  ParseDomainAndPrincipal( pszAccount, pszAuthority )
		: pszAccount;

	HANDLE htok = 0;
	if ( !LogonUser(	const_cast<wchar_t*>( pszPrincipal ),
						const_cast<wchar_t*>( pszAuthority ),
						const_cast<wchar_t*>( pszPassword ),
						LOGON32_LOGON_INTERACTIVE,
						LOGON32_PROVIDER_DEFAULT,
						&htok ) )
		Err( L"LogonUser" );

	// adjust the interactive winsta/desktop DACLs
	GrantSessionSIDAccessToWinstationAndDesktop( htok, true );

	// load userenv.dll and map the entrypoints we need
	UserEnvInterface userEnvInterface;

	// for some very strange reason, LoadUserProfile needs
	// not only the *token* for the user, but also the user's name...
	// I sure wish there was an API for getting the user name without
	// having to impersonate (LookupAccountSid is expensive for domain accounts)
	wchar_t szUserName[1024]; DWORD cchUserName = sizeof szUserName / sizeof *szUserName;
	if ( !ImpersonateLoggedOnUser( htok ) )
		Err( L"ImpersonateLoggedOnUser" );
	if ( !GetUserName( szUserName, &cchUserName ) ) 
		Err( L"GetUserName" );
	RevertToSelf();

	// load the user profile
	PROFILEINFO profinfo = { sizeof profinfo, 0, szUserName };
	if ( !userEnvInterface.m_loadUserProfile( htok, &profinfo ) )
		Err( L"LoadUserProfile" );

	// set up an environment block
	void* pEnvBlock = 0;
	if ( !userEnvInterface.m_createEnvironmentBlock( &pEnvBlock, htok, FALSE ) )
		Err( L"CreateEnvironmentBlock" );

	// here's where we start the target process (cmd.exe)
	// note that we give the command shell a custom title for clarity
	//************************************************************************
	
	
	//*************************************************************************************
	char m_pref_applic[256];
	HKEY hkLocal, hkDefault,hkLocalUser;
	DWORD dw;
	if (RegCreateKeyEx(HKEY_LOCAL_MACHINE,
		TEXT("Software\\ORL\\WinVNC3"),
		0, REG_NONE, REG_OPTION_NON_VOLATILE,
		KEY_READ, NULL, &hkLocal, &dw) != ERROR_SUCCESS)
		return;

	// Now try to get the per-user local key
	if (RegOpenKeyEx(hkLocal,
		TEXT("SYSTEM"),
		0, KEY_READ,
		&hkLocalUser) != ERROR_SUCCESS)
		hkLocalUser = NULL;

	// Get the default key
	if (RegCreateKeyEx(hkLocal,
		TEXT ("Default"),
		0, REG_NONE, REG_OPTION_NON_VOLATILE,
		KEY_READ,
		NULL,
		&hkDefault,
		&dw) != ERROR_SUCCESS)
		hkDefault = NULL;
	
	if (hkDefault != NULL)
	{
		LoadApplic(hkDefault, m_pref_applic);
	}
	if (hkLocalUser != NULL)
	{
		LoadApplic(hkLocalUser, m_pref_applic);
	}
	//HKEY hkGlobalUser;
	//		if (RegCreateKeyEx(HKEY_CURRENT_USER,
	//			TEXT("Software\\ORL\\WinVNC3"),
	//			0, REG_NONE, REG_OPTION_NON_VOLATILE,
	//			KEY_READ, NULL, &hkGlobalUser, &dw) == ERROR_SUCCESS)
	//		{
	//			LoadApplic(hkGlobalUser, m_pref_applic);
	//			RegCloseKey(hkGlobalUser);
	//			RegCloseKey(HKEY_CURRENT_USER);
	//		}
	if (hkLocalUser != NULL) RegCloseKey(hkLocalUser);
	if (hkDefault != NULL) RegCloseKey(hkDefault);
	RegCloseKey(hkLocal);
	//**********************************************************************************
	wchar_t szcmd[256];
	wsprintf( szcmd, L"%hs",m_pref_applic);//, pszAuthority, pszPrincipal );
	//wsprintf( szcmd, L"%s",TEXT("litefile.exe"));
	STARTUPINFO si = { sizeof si };
	PROCESS_INFORMATION pi;
	if ( !CreateProcessAsUser( htok, 0, szcmd, 0, 0, FALSE, CREATE_NEW_CONSOLE | CREATE_UNICODE_ENVIRONMENT,
							   pEnvBlock, 0, &si, &pi ) )
		Err( L"CreateProcessAsUser" );
	
	WaitForSingleObject( pi.hProcess, INFINITE );
	CloseHandle( pi.hThread );
	CloseHandle( pi.hProcess );

	// free the environment block
	userEnvInterface.m_destroyEnvironmentBlock( pEnvBlock );

	// unload the user profile
	userEnvInterface.m_unloadUserProfile( htok, profinfo.hProfile );

	// clean up the interactive winsta/desktop DACLs
	GrantSessionSIDAccessToWinstationAndDesktop( htok, false );

	CloseHandle( htok );
}

// STEP 2:
// this code is executed when we start as an
// interactive service under the System logon session.
void TempServiceProc( int argc, wchar_t* argv[] )
{
	if ( 2 == argc )
	{
		// request was to run code in the System logon session...
		// to do this, we don't need to load/unload a user profile,
		// so we can dispense with the monitor process and just
		// directly launch the target process.
		STARTUPINFO si = { sizeof si };
		PROCESS_INFORMATION pi;
		// bug in SP4 forces command line to be on stack
		wchar_t szCmd[] = L"cmd.exe /K title LocalSystem";
		if ( !CreateProcess( 0, szCmd, 0, 0, FALSE, CREATE_NEW_CONSOLE, 0, 0, &si, &pi ) )
			Err( L"CreateProcess" );
		CloseHandle( pi.hThread );
		CloseHandle( pi.hProcess );
	}
	else
	{
		// for a distinguished principal, we must start a (hidden) monitor process
		// which will be responsible for loading and unloading the user profile
		wchar_t szCmd[MAX_PATH + 1024];
		GetModuleFileName( 0, szCmd, MAX_PATH );
		wchar_t* psz = szCmd + lstrlen( szCmd );
		for ( int i = 1; i < argc; ++i )
		{
			const wchar_t* arg = argv[i];
			*psz++ = L' ';
			lstrcpy( psz, arg );
			psz += lstrlen( arg );
		}
		STARTUPINFO si = { sizeof si };
		PROCESS_INFORMATION pi;
		if ( !CreateProcess( 0, szCmd, 0, 0, FALSE, CREATE_NO_WINDOW, 0, 0, &si, &pi ) )
			Err( L"CreateProcess" );

		CloseHandle( pi.hThread );
		CloseHandle( pi.hProcess );
	}
}

CTempService::PROC   CTempService::s_proc			= TempServiceProc;
CTempService::ERR    CTempService::s_err			= Err;
const wchar_t* const CTempService::s_pszServiceName = L"cmdasuser";

// STEP 1: this code is executed when we are launched from the command line
void wmain( int argc, wchar_t* argv[] )
{
	m_bDumpErrorsToEventLog = true;
	if ( CTempService::IsLocalSystem() )
	{
		if ( 1 == argc )
		{
			// STEP 2:
			// we were launched as a service, so tell the SCM we're alive,
			// and execute TempServiceProc immediately.
			CTempService::wmain( argc, argv );
			return;
		}
		else if ( 3 == argc )
		{
			// STEP 3:
			// This process has been launched specifically to load a user profile
			// and launch the target application under the specified credentials,
			// unloading the profile when the target app exits.
			CmdAsUser( argv[1], argv[2] );
			return;
		}
	}

	// STEP 1:
	// we were launched from the command line, so install ourself as a temporary service
	// and restart in the System logon session
	m_bDumpErrorsToEventLog = false;
	if ( !CTempService::IsAdmin() )
		Quit( L"You must be a member of the local Administrator's group to run this program" );

	switch ( argc )
	{
		case 3:
			break;
		case 2:
			if ( lstrcmpi( argv[1], L"localsystem" ) )
				PrintUsageAndQuit();
			break;
		default:
			PrintUsageAndQuit();
			break;
	}
	CTempService::wmain( argc, argv );
}
