/////////////////////////////////////////////////////////////////////////////
//  Copyright (C) 2002 Ultr@VNC Team Members. All Rights Reserved.
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// If the source code for the program is not available from the place from
// which you received this file, check 
// http://ultravnc.sourceforge.net/
//
////////////////////////////////////////////////////////////////////////////


// FileTransfer.cpp: implementation of the FileTransfer class.

// sf@2002 - FileTransfer
// This class handles all the FileTransfer messages, events and procs, as well as the
// DialogBox which allows the user to browse Client ans Server disks-directories,
// and select some files to transfer between Client and Server.
//
// The GUI is very basic because I don't want to include MFC Classes in VNC...
// I use only Windows SDK.
//
//
// Modif sf@2002 - v1.1.0
//
// This file has been heavily modified.
//
// The GUI is now quite bearable, but following modifs could be done one day or another
// - Add more columns to FileLists (File type, Attributes...)
// - Add the possibility to sort files by colomns (File Size Order, File Ext order...)
// - Total progress should be based on total files' size instead of total number of files
// - Make the History persistent (file) so it's not lost each time the FileTransfer Win is closed
// - Clean-up the code (duplicated parts, arrays and strings dimensions checks...)
// - Add some commands : Rename File, Move File...
// - Transfer of directories
// - Resume of interrupted transfer, Delta compression
// - Display the total files size in the currently displayed directory
// - Remember the current directories


#include "stdhdrs.h"
#include "vncviewer.h"
#include "FileTransfer.h"
#include "Exception.h"
#include "commctrl.h"
#include "zlib/zlib.h" // sf@2002 - v1.1.2

extern char sz_H1[64];
extern char sz_H2[64];
extern char sz_H3[64];
extern char sz_H4[64];
extern char sz_H5[64];
extern char sz_H6[64];
extern char sz_H7[64];
extern char sz_H8[64];
extern char sz_H9[64];
extern char sz_H10[64];
extern char sz_H11[64];
extern char sz_H12[64];
extern char sz_H13[64];
extern char sz_H14[64];
extern char sz_H15[64];
extern char sz_H16[64];
extern char sz_H17[64];
extern char sz_H18[64];
extern char sz_H19[64];
extern char sz_H20[64];
extern char sz_H21[64];
extern char sz_H22[64];
extern char sz_H23[64];
extern char sz_H24[64];
extern char sz_H25[64];
extern char sz_H26[64];
extern char sz_H27[64];
extern char sz_H28[64];
extern char sz_H29[64];
extern char sz_H30[64];
extern char sz_H31[64];
extern char sz_H32[64];
extern char sz_H33[64];
extern char sz_H34[64];
extern char sz_H35[64];
extern char sz_H36[64];
extern char sz_H37[64];
extern char sz_H38[128];
extern char sz_H39[64];
extern char sz_H40[64];
extern char sz_H41[64];
extern char sz_H42[64];
extern char sz_H43[128];
extern char sz_H44[64];
extern char sz_H45[64];
extern char sz_H46[128];
extern char sz_H47[64];
extern char sz_H48[64];
extern char sz_H49[64];
extern char sz_H50[64];
extern char sz_H51[64];
extern char sz_H52[64];
extern char sz_H53[64];
extern char sz_H54[64];
extern char sz_H55[64];
extern char sz_H56[64];
extern char sz_H57[64];

//
//
//
FileTransfer::FileTransfer(VNCviewerApp *pApp, ClientConnection *pCC)
{
	m_pApp	= pApp;
	m_pCC	= pCC;
	m_fAbort = false;
	m_FilesList.clear();
	m_nFilesToTransfer = 0;
	m_nFilesTransfered = 0;
	m_fFileCommandPending = false;
	m_fFileTransferRunning = false;
	m_fFTAllowed = false;
	m_timer = 0;
}


//
//
//
FileTransfer::~FileTransfer()
{
	m_fFileCommandPending = false;
	m_fFileTransferRunning = false;
	m_FilesList.clear();
}



//
// Simply the classic Windows message processing
//
// I know, doing the filecopy in another thread would be far better, safe, elegant, efficient. 
// But this method is ok for my needs and is faster to implement.
// 
bool PseudoYield(HWND hWnd)
{
	MSG msg;
	while (PeekMessage(&msg, hWnd, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
		if (msg.message == WM_CLOSE) return FALSE;
	}
	return TRUE;
}



//
//	ProcessFileTransferMsg
//
//  Here we process all incoming FileTransferMsg stuff
//  coming from the server.
//  The server only sends FileTransfer data when requested
//  by the client. Possible request are:
//
//  - Send the list of your drives
//  - Send the content of a directory
//  - Send a file
//  - Accept a file
//  - ...
// 
//  We use the main ClientConnection thread and its
//  rfb message reception loop.
//  This function is called by the rfb message processing thread.
//  Thus it's safe to call the ReadExact and ReadString 
//  functions in the functions that are called from here:
//  PopulateRemoteListBox, ReceiveFile
// 
void FileTransfer::ProcessFileTransferMsg(void)
{
	rfbFileTransferMsg ft;
	m_pCC->ReadExact(((char *) &ft) + m_pCC->m_nTO, sz_rfbFileTransferMsg - m_pCC->m_nTO);

	switch (ft.contentType)
	{
	// Response to a rfbDirContentRequest request:
	// some directory data is received from the server
	case rfbDirPacket:
		switch (ft.contentParam)
		{
		// Response to a rfbRDrivesList request
		case rfbADrivesList:
			ListRemoteDrives(hWnd, Swap32IfLE(ft.length));
			m_fFileCommandPending = false;
			break;

		// Response to a rfbRDirContent request 
		case rfbADirectory:
		case rfbAFile:
			PopulateRemoteListBox(hWnd, Swap32IfLE(ft.length));
			m_fFileCommandPending = false;
			break;
		}
		break;

	// In response to a rfbFileTransferRequest request
	// A file is received from the server.
	case rfbFileHeader:
		ReceiveFiles(Swap32IfLE(ft.size), Swap32IfLE(ft.length));
		break;

	// In response to a rfbFileTransferOffer request
	// A ack or nack is received from the server.
	case rfbFileAcceptHeader:
		SendFiles(Swap32IfLE(ft.size), Swap32IfLE(ft.length));
		break;

	// Response to a command
	case rfbCommandReturn:
		switch (ft.contentParam)
		{
		case rfbADirCreate:
			CreateRemoteDirectoryFeedback(Swap32IfLE(ft.size), Swap32IfLE(ft.length));
			m_fFileCommandPending = false;
			break;

		case rfbAFileDelete:
			DeleteRemoteFileFeedback(Swap32IfLE(ft.size), Swap32IfLE(ft.length));
			m_fFileCommandPending = false;
			break;
		}
		break;
		
	// Should never be handled here but in the File Transfer Loop
	case rfbFilePacket:
		break;
	// Should never be handled here but in the File Transfer Loop
	case rfbEndOfFile:
		break;
	// Should never be handled here but in the File Transfer Loop
	// We use it to test if we're allowed to use FileTransfer on the server
	case rfbAbortFileTransfer:
		TestPermission(Swap32IfLE(ft.size));
		break;

	default:
		return;
		break;
	}
}


//
// request file transfer permission 
//
void FileTransfer::RequestPermission()
{
    rfbFileTransferMsg ft;
    ft.type = rfbFileTransfer;
	ft.contentType = rfbAbortFileTransfer; // Have I the right to use FileTransfer ?
    ft.contentParam = 0;
	ft.length = 0;
	ft.size = 0;
    m_pCC->WriteExact((char *)&ft, sz_rfbFileTransferMsg, rfbFileTransfer);
	return;
}


//
// Test if we are allowed to access filetransfer
//
bool FileTransfer::TestPermission(long lSize)
{
	if (lSize == -1)
	{
		m_fFTAllowed = false;
		HWND hWndRemoteList = GetDlgItem(hWnd, IDC_REMOTE_FILELIST);
		SendDlgItemMessage(hWnd, IDC_REMOTE_DRIVECB, LB_RESETCONTENT, 0, 0L);
		ListView_DeleteAllItems(hWndRemoteList);
		SetDlgItemText(hWnd, IDC_CURR_REMOTE, sz_H1);
		SetDlgItemText(hWnd, IDC_REMOTE_STATUS, sz_H2);
		SetStatus(sz_H3);
		DisableButtons(hWnd);
	}
	else
	{
		m_fFTAllowed = true;
		RequestRemoteDrives();
		SetStatus(sz_H4);
	}

	return true;
}


//
// Receive all the files that are referenced in m_FilesList
//
bool FileTransfer::ReceiveFiles(long lSize, int nLen)
{
	// Receive the incoming file
	m_nFilesTransfered++;
	SetGlobalCount();
	ReceiveFile(lSize, nLen);

	SetGlobalCount();

	m_iFile++; // go to next file in the list of files to receive

	HWND hWndLocalList = GetDlgItem(hWnd, IDC_LOCAL_FILELIST);
	HWND hWndRemoteList = GetDlgItem(hWnd, IDC_REMOTE_FILELIST);

	// If one more file has to be received, ask for it !
	if (m_iFile != m_FilesList.end() && !m_fAbort)
	{
		TCHAR szSelectedFile[128];
		TCHAR szDstFile[MAX_PATH];
		memset(szSelectedFile, 0, 128);
		memset(szDstFile, 0, MAX_PATH);

		LVITEM Item;
		Item.mask = LVIF_TEXT;
		Item.iSubItem = 0;
		Item.pszText = szSelectedFile;
		Item.cchTextMax = 128;

		// Get the name file to receive in the remote list
		Item.iItem = *m_iFile;
		ListView_GetItem(hWndRemoteList, &Item);

		GetDlgItemText(hWnd, IDC_CURR_REMOTE, szDstFile, sizeof(szDstFile));
		if (!strlen(szDstFile)) return false; // no destination dir selected - msgbox ?
		strcat(szDstFile, szSelectedFile);

		RequestRemoteFile(szDstFile);
	}
	else // All the files have been processed and hopefully received
	{
		// Refresh the local list so new files are displayed and highlighted
		ListView_DeleteAllItems(hWndLocalList);
		PopulateLocalListBox(hWnd, "");

		if (m_fAbort)
			SetStatus(sz_H5);
		else
			SetStatus(sz_H6);

		EnableButtons(hWnd);

		// Unlock 
		m_fFileCommandPending = false;
	}

	return true;
}


//
// Send all the files that are referenced in m_FilesList
//
bool FileTransfer::SendFiles(long lSize, int nLen)
{
	// Receive the incoming file
	m_nFilesTransfered++;
	SetGlobalCount();
	SendFile(lSize, nLen);

	SetGlobalCount();

	m_iFile++; // go to next file in the list of files to send

	HWND hWndLocalList = GetDlgItem(hWnd, IDC_LOCAL_FILELIST);
	HWND hWndRemoteList = GetDlgItem(hWnd, IDC_REMOTE_FILELIST);

	// If one more file has to be sent, offer it !
	if (m_iFile != m_FilesList.end() && !m_fAbort)
	{
		TCHAR szSelectedFile[128];
		TCHAR szSrcFile[MAX_PATH];
		memset(szSelectedFile, 0, 128);
		memset(szSrcFile, 0, MAX_PATH);

		LVITEM Item;
		Item.mask = LVIF_TEXT;
		Item.iSubItem = 0;
		Item.pszText = szSelectedFile;
		Item.cchTextMax = 128;

		// Get the name of file to send in the local list
		Item.iItem = *m_iFile;
		ListView_GetItem(hWndLocalList, &Item);

		GetDlgItemText(hWnd, IDC_CURR_LOCAL, szSrcFile, sizeof(szSrcFile));
		if (!strlen(szSrcFile)) return false; // no destination dir selected - msgbox ?
		strcat(szSrcFile, szSelectedFile);

		if (!OfferLocalFile(szSrcFile))
		   SendFiles(-1, 0);
	}
	else // All the files have been processed and hopefully received
	{
		// Refresh the remote list so new files are displayed and highlighted
		ListView_DeleteAllItems(hWndRemoteList);
		RequestRemoteDirectoryContent(hWnd, "");

		if (m_fAbort)
			SetStatus(sz_H7);
		else
			SetStatus(sz_H6);

		EnableButtons(hWnd);

		// Unlock 
		m_fFileCommandPending = false;
	}

	return true;
}


//
// Add a file line to a ListView
// 
void FileTransfer::AddFileToFileList(HWND hWnd, int nListId, WIN32_FIND_DATA& fd)
{
	char szFileName[MAX_PATH + 2];
	HWND hWndList = GetDlgItem(hWnd, nListId);

	// If we need to keep more info on the file, we can use LVITEM lParam 
	// (it will be usefull if we want to make comparison between local and remote files
	// for a resume function for instance, or for sorting purposes)
	// 
	// We could also display Files attributes and other Files times...

	if (((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY && strcmp(fd.cFileName, "."))
		||
		(!strcmp(fd.cFileName, ".."))
	   )
	{
		sprintf(szFileName, "(%s)", fd.cFileName);
	
		if (!strcmp(szFileName, "(..)") && nListId == IDC_LOCAL_FILELIST)
		{
			nListId = nListId;
		}
		// Name
		LVITEM Item;
		Item.mask = LVIF_TEXT;
		Item.iItem = 0;
		Item.iSubItem = 0;
		Item.pszText = szFileName;
		int nItem = ListView_InsertItem(hWndList, &Item);
		
		// Type
		Item.mask = LVIF_TEXT;
		Item.iItem = nItem;
		Item.iSubItem = 1;
		Item.pszText = "File Folder";
		ListView_SetItem(hWndList, &Item);

	}
	else if (strcmp(fd.cFileName, ".")) // Test actually Not necessary for remote list
	{
		// Name
		LVITEM Item;
		Item.mask = LVIF_TEXT;
		Item.iItem = 0;
		Item.iSubItem = 0;
		Item.pszText = fd.cFileName;
		int nItem = ListView_InsertItem(hWndList,&Item);
		
		// Size
		// Todo; manage large sizes (> 4Go) 
		unsigned long lSize = ((fd.nFileSizeHigh * (MAXDWORD + 1)) + fd.nFileSizeLow);
		char szText[256];
		szText[0] = '\0';
		if(lSize > (1024*1024))
		{
			unsigned long lRest = (lSize % (1024*1024));
			lSize /= (1024*1024);
			wsprintf(szText,"%u.%2.2lu Mb", lSize, lRest * 100 / 1024 / 1024);
		}
		else if (lSize > 1024)
		{
			unsigned long lRest = lSize % (1024);
			lSize /= 1024;
			wsprintf(szText,"%u.%2.2lu Kb", lSize, lRest * 100 / 1024);
		}
		else
		{
			wsprintf(szText,"%u bytes", lSize);
		}
		Item.mask = LVIF_TEXT;
		Item.iItem = nItem;
		Item.iSubItem = 1;
		Item.pszText = szText;
		ListView_SetItem(hWndList, &Item);
		
		// Last Modif Time
		SYSTEMTIME FileTime;
		FileTimeToSystemTime(&fd.ftLastWriteTime, &FileTime);
		wsprintf(szText,"%2.2d/%2.2d/%4.4d %2.2d:%2.2d",
				FileTime.wMonth,
				FileTime.wDay,
				FileTime.wYear,
				FileTime.wHour,
				FileTime.wMinute
				);

		Item.mask = LVIF_TEXT;
		Item.iItem = nItem;
		Item.iSubItem = 2;
		Item.pszText = szText;
		ListView_SetItem(hWndList,&Item);
	}

}


//
// Select the new transfered files in the dest FileList so the user find them easely
//
void FileTransfer::HighlightTransferedFiles(HWND hSrcList, HWND hDstList)
{
	if (m_FilesList.size() > 0)
	{
		TCHAR szSelectedFile[128];

		LVITEM Item;
		Item.mask = LVIF_TEXT;
		Item.iSubItem = 0;
		Item.pszText = szSelectedFile;
		Item.cchTextMax = 128;

		LVFINDINFO Info;
		Info.flags = LVFI_STRING;
		Info.psz = (LPSTR)szSelectedFile;

		for (m_iFile = m_FilesList.begin();
			m_iFile != m_FilesList.end();
			m_iFile++)
		{
			// Get the name of the file sent
			Item.iItem = *m_iFile;
			ListView_GetItem(hSrcList, &Item);

			// Find this file in the list and highlight it
			int nTheIndex = ListView_FindItem(hDstList, -1, &Info);
			if (nTheIndex > -1)
			{
				ListView_SetItemState(hDstList, nTheIndex, LVIS_SELECTED, LVIS_SELECTED);
				ListView_EnsureVisible(hDstList, nTheIndex, FALSE);
			}
		}	
		m_FilesList.clear();
	}

}


//
// Populate the local machine listbox with files located in szPath
//
void FileTransfer::PopulateLocalListBox(HWND hWnd, LPSTR szPath)
{
	char ofDir[MAX_PATH];
	char ofDirT[MAX_PATH];
	int nSelected = -1;
	int nCount = 0;
	int nFileCount = 0;
	char szLocalStatus[128];

	HWND hWndLocalList = GetDlgItem(hWnd, IDC_LOCAL_FILELIST);
	HWND hWndRemoteList = GetDlgItem(hWnd, IDC_REMOTE_FILELIST);

	ofDir[0] = '\0';
	ofDirT[0] = '\0';

	if (lstrlen(szPath) == 0)
	{
		nCount = ListView_GetItemCount(hWndLocalList);
		for (nSelected = 0; nSelected < nCount; nSelected++)
		{
			if(ListView_GetItemState(hWndLocalList, nSelected, LVIS_SELECTED) & LVIS_SELECTED)
			{
				LVITEM Item;
				Item.mask = LVIF_TEXT;
				Item.iItem = nSelected;
				Item.iSubItem = 0;
				Item.pszText = ofDirT;
				Item.cchTextMax = MAX_PATH;
				ListView_GetItem(hWndLocalList, &Item);
				break;
			}
		}
	}
	else
	{
		// szPath always contains a drive letter (X:) or (..)
		strcpy(ofDirT, szPath);
		// In the case of (..) we keep the current path intact
		if (strcmp(ofDirT, "(..)"))
			SetDlgItemText(hWnd, IDC_CURR_LOCAL, "");
	}

	if (nSelected == nCount || lstrlen(ofDirT) == 0)
	{
		GetDlgItemText(hWnd, IDC_CURR_LOCAL, ofDirT, sizeof(ofDirT));
		if (strlen(ofDirT) == 0) return; 
	}
	else
	{
		if (ofDirT[0] == '(')
		{
			strncpy(ofDir, ofDirT + 1, strlen(ofDirT) - 2); 
			ofDir[strlen(ofDirT) - 2] = '\0';
		}
		else
			return;

		GetDlgItemText(hWnd, IDC_CURR_LOCAL, ofDirT, sizeof(ofDirT));
		if (!stricmp(ofDir, ".."))
		{	
			char* p;
			ofDirT[strlen(ofDirT) - 1] = '\0';
			p = strrchr(ofDirT, '\\');
			if (p == NULL) return;
			*p = '\0';
		}
		else
			strcat(ofDirT, ofDir);
		strcat(ofDirT, "\\");
		SetDlgItemText(hWnd, IDC_CURR_LOCAL, ofDirT);
	}
	strcpy(ofDir, ofDirT);
	strcat(ofDir, "*");

	// Select the good drive in the drives combo box (the first time only)
	int nIndex = SendDlgItemMessage(hWnd, IDC_LOCAL_DRIVECB, CB_GETCURSEL, 0, 0L);
	if (nIndex == LB_ERR)
	{
	    char szDrive[5];
		strcpy(szDrive, "(");
	    strncat(szDrive, ofDir, 2);
	    nIndex = SendDlgItemMessage(hWnd, IDC_LOCAL_DRIVECB, CB_FINDSTRING, -1, (LPARAM)(LPSTR)szDrive); 
	    SendDlgItemMessage(hWnd, IDC_LOCAL_DRIVECB, CB_SETCURSEL, nIndex, 0L);
	}

	sprintf(szLocalStatus, sz_H8); 
	SetDlgItemText(hWnd, IDC_LOCAL_STATUS, szLocalStatus);

	ListView_DeleteAllItems(hWndLocalList);

	WIN32_FIND_DATA fd;
	HANDLE ff;
	int bRet = 1;

	SetErrorMode(SEM_FAILCRITICALERRORS); // No popup please !
	ff = FindFirstFile(ofDir, &fd);
	SetErrorMode( 0 );

	if (ff == INVALID_HANDLE_VALUE)
	{
		sprintf(szLocalStatus, sz_H9); 
		SetDlgItemText(hWnd, IDC_LOCAL_STATUS, szLocalStatus);
		return;
	}

	while (bRet != 0)
	{
		AddFileToFileList(hWnd, IDC_LOCAL_FILELIST, fd);
		nFileCount++;
		if (!PseudoYield(GetParent(hWnd))) return;
		bRet = FindNextFile(ff, &fd);
	}

	FindClose(ff);

	sprintf(szLocalStatus, " > Found: %d File(s) / Directorie(s) ", nFileCount); 
	SetDlgItemText(hWnd, IDC_LOCAL_STATUS, szLocalStatus);

	// If some files have been received 
	HighlightTransferedFiles( hWndRemoteList, hWndLocalList);

}



//
// Request the content of a remote directory
//
void FileTransfer::RequestRemoteDirectoryContent(HWND hWnd, LPSTR szPath)
{
	if (!m_fFTAllowed)
	{
		m_fFileCommandPending = false;
		return;
	}

	char ofDir[MAX_PATH];
	char ofDirT[MAX_PATH];
	int nSelected = -1;
	int nCount;
	HWND hWndRemoteList = GetDlgItem(hWnd, IDC_REMOTE_FILELIST);

	ofDir[0] = '\0';
	ofDirT[0] = '\0';

	if (lstrlen(szPath) == 0)
	{
		nCount = ListView_GetItemCount(hWndRemoteList);
		for (nSelected = 0; nSelected < nCount; nSelected++)
		{
			if(ListView_GetItemState(hWndRemoteList, nSelected, LVIS_SELECTED) & LVIS_SELECTED)
			{
				LVITEM Item;
				Item.mask = LVIF_TEXT;
				Item.iItem = nSelected;
				Item.iSubItem = 0;
				Item.pszText = ofDirT;
				Item.cchTextMax = MAX_PATH;
				ListView_GetItem(hWndRemoteList, &Item);
				break;
			}
		}
	}
	else
	{
		// szPath always contains a drive letter (X:) or (..)
		strcpy(ofDirT, szPath);
		// In the case of (..) we keep the current path intact
		if (strcmp(ofDirT, "(..)"))
			SetDlgItemText(hWnd, IDC_CURR_REMOTE, "");
	}

	if (nSelected == nCount || lstrlen(ofDirT) == 0)
	{
		GetDlgItemText(hWnd, IDC_CURR_REMOTE, ofDirT, sizeof(ofDirT));
		if (strlen(ofDirT) == 0) 
		{
			m_fFileCommandPending = false;
			return; 
		}
	}
	else
	{
		if (ofDirT[0] == '(')
		{
			strncpy(ofDir, ofDirT + 1, strlen(ofDirT) - 2); 
			ofDir[strlen(ofDirT) - 2] = '\0';
		}
		else
		{
			m_fFileCommandPending = false;
			return;
		}

		GetDlgItemText(hWnd, IDC_CURR_REMOTE, ofDirT, sizeof(ofDirT));
		if (!stricmp(ofDir, ".."))
		{	
			char* p;
			ofDirT[strlen(ofDirT) - 1] = '\0';
			p = strrchr(ofDirT, '\\');
			if (p == NULL)
			{
				m_fFileCommandPending = false;
				return;
			}
			*p = '\0';
		}
		else
			strcat(ofDirT, ofDir);
		strcat(ofDirT, "\\");
		SetDlgItemText(hWnd, IDC_CURR_REMOTE, ofDirT);
	}
	strcpy(ofDir, ofDirT);

	// Select the good drive in the drives combo box (the first time only)
	int nIndex = SendDlgItemMessage(hWnd, IDC_REMOTE_DRIVECB, CB_GETCURSEL, 0, 0L);
	if (nIndex == LB_ERR)
	{
	    char szDrive[5];
		strcpy(szDrive, "(");
	    strncat(szDrive, ofDir, 2);
	    nIndex = SendDlgItemMessage(hWnd, IDC_REMOTE_DRIVECB, CB_FINDSTRING, -1, (LPARAM)(LPSTR)szDrive); 
	    SendDlgItemMessage(hWnd, IDC_REMOTE_DRIVECB, CB_SETCURSEL, nIndex, 0L);
	}

	ListView_DeleteAllItems(hWndRemoteList);

    rfbFileTransferMsg ft;
    ft.type = rfbFileTransfer;
	ft.contentType = rfbDirContentRequest;
    ft.contentParam = rfbRDirContent; // Directory content please
	ft.length = Swap32IfLE(strlen(ofDir));
    m_pCC->WriteExact((char *)&ft, sz_rfbFileTransferMsg, rfbFileTransfer);
	m_pCC->WriteExact((char *)ofDir, strlen(ofDir));

	return;
}


//
// Populate the remote machine listbox with files received from server
//
void FileTransfer::PopulateRemoteListBox(HWND hWnd, int nLen)
{
	char szFileSpec[MAX_PATH + 64];
	int bRet = 1;
	int nType = 0;
	int nFileCount = 0;
	char szRemoteStatus[128];
	WIN32_FIND_DATA fd;

	HWND hWndLocalList = GetDlgItem(hWnd, IDC_LOCAL_FILELIST);
	HWND hWndRemoteList = GetDlgItem(hWnd, IDC_REMOTE_FILELIST);

	// If the distant media is not browsable for some reason
	if (nLen == 0)
	{
		sprintf(szRemoteStatus, sz_H10); 
		SetDlgItemText(hWnd, IDC_REMOTE_STATUS, szRemoteStatus);
		return;
	}

	sprintf(szRemoteStatus, sz_H11); 
	SetDlgItemText(hWnd, IDC_REMOTE_STATUS, szRemoteStatus);

    rfbFileTransferMsg ft;

	// The dir in the current packet
	memset(&fd, '\0', sizeof(WIN32_FIND_DATA));

	// Loop until all Directory's files have been received from server
	for(;;)
	{
		// Wait for FileTransfer message
		m_pCC->ReadExact((char *) &ft, sz_rfbFileTransferMsg);
		// Forbid all other type of rfbMessages
		if (ft.type != rfbFileTransfer) continue; 

		// Only consider rfbDirPacket messages
		if (ft.contentType == rfbDirPacket)
		{
			nType = Swap16IfLE(ft.contentParam);
			if (nType == 0) break; // All files have been received (hopefully...)

			// Read the File/Directory full info
			m_pCC->ReadString((char *)szFileSpec, Swap32IfLE(ft.length));
			memset(&fd, '\0', sizeof(WIN32_FIND_DATA));
			memcpy(&fd, szFileSpec, Swap32IfLE(ft.length));
			AddFileToFileList(hWnd, IDC_REMOTE_FILELIST, fd);
			nFileCount++;
			// PseudoYield(pFileTransfer->hWnd);
		}
		if (!PseudoYield(GetParent(hWnd))) return;
	}

	sprintf(szRemoteStatus, " > Found: %d File(s) / Directorie(s)", nFileCount); 
	SetDlgItemText(hWnd, IDC_REMOTE_STATUS, szRemoteStatus);

	// If some files have been sent to remote side highlight them 
	HighlightTransferedFiles(hWndLocalList, hWndRemoteList);
	// UpdateWindow(hWnd);
}


//
// request the list of remote drives 
//
void FileTransfer::RequestRemoteDrives()
{
	if (!m_fFTAllowed) return;

	// TODO : hook error !
    rfbFileTransferMsg ft;
    ft.type = rfbFileTransfer;
	ft.contentType = rfbDirContentRequest;
    ft.contentParam = rfbRDrivesList; // List of Remote Drives please
	ft.length = 0;
    m_pCC->WriteExact((char *)&ft, sz_rfbFileTransferMsg, rfbFileTransfer);

	return;
}


//
// Fill the Remote FilesList and remote drives combo box 
//
void FileTransfer::ListRemoteDrives(HWND hWnd, int nLen)
{
	TCHAR szDrivesList[256]; // Format when filled : "C:t<NULL>D:t<NULL>....Z:t<NULL><NULL>"
	TCHAR szDrive[4];
	TCHAR szTheDrive[32];
	TCHAR szType[16];
	int nIndex = 0;

	m_pCC->ReadString((char *)szDrivesList, nLen);

	HWND hWndRemoteList = GetDlgItem(hWnd, IDC_REMOTE_FILELIST);

	SendDlgItemMessage(hWnd, IDC_REMOTE_DRIVECB, LB_RESETCONTENT, 0, 0L);
	ListView_DeleteAllItems(hWndRemoteList);
	SetDlgItemText(hWnd, IDC_CURR_REMOTE, "");

	// Fill the tree with the remote drives
	while (nIndex < nLen - 3)
	{
		strcpy(szDrive, szDrivesList + nIndex);
		nIndex += 4;

		// Get the type of drive
		switch (szDrive[2])
		{
		case 'l':
			sprintf(szType, "%s", "Local Disk");
			break;
		case 'f':
			sprintf(szType, "%s", "Floppy");
			break;
		case 'c':
			sprintf(szType, "%s", "CD-ROM");
			break;
		case 'n':
			sprintf(szType, "%s", "Network");
			break;
		default:
			sprintf(szType, "%s", "Unknown");
			break;
		}

		szDrive[2] = '\0'; // remove the type char
		sprintf(szTheDrive, "(%s)", szDrive);

		LVITEM Item;
		Item.mask = LVIF_TEXT;
		Item.iItem = 0;
		Item.iSubItem = 0;
		Item.pszText = szTheDrive;
		int nItem = ListView_InsertItem(hWndRemoteList, &Item);
		
		Item.mask = LVIF_TEXT;
		Item.iItem = nItem;
		Item.iSubItem = 1;
		Item.pszText = szType;
		ListView_SetItem(hWndRemoteList, &Item);

		// Prepare it for Combo Box and add it
		strcat(szTheDrive, " - ");
		strcat(szTheDrive, szType);

		SendMessage(GetDlgItem(hWnd, IDC_REMOTE_DRIVECB), CB_ADDSTRING, 0, (LPARAM)szTheDrive); 

	}
	SendMessage(GetDlgItem(hWnd, IDC_REMOTE_DRIVECB), CB_SETCURSEL, -1, 0);
}


//
// List local drives
//
void FileTransfer::ListDrives(HWND hWnd)
{
	TCHAR szDrivesList[256]; // Format when filled : "C:\<NULL>D:\<NULL>....Z:\<NULL><NULL>"
	TCHAR szDrive[4];
	TCHAR szTheDrive[32];
	// TCHAR szName[255];
	TCHAR szType[16];
	UINT nType = 0;
	DWORD dwLen;
	int nIndex = 0;
	dwLen = GetLogicalDriveStrings(256, szDrivesList);

	HWND hWndLocalList = GetDlgItem(hWnd, IDC_LOCAL_FILELIST);
	SendDlgItemMessage(hWnd, IDC_LOCAL_DRIVECB, LB_RESETCONTENT, 0, 0L);

	ListView_DeleteAllItems(hWndLocalList);
	SetDlgItemText(hWnd, IDC_CURR_LOCAL, "");

	// Parse the list of drives
	while (nIndex < dwLen - 3)
	{
		strcpy(szDrive, szDrivesList + nIndex);
		nIndex += 4;
		szDrive[2] = '\0'; // remove the '\'
		sprintf(szTheDrive, "(%s)", szDrive);

		// szName[0] = '\0';
		szType[0] = '\0';

		strcat(szDrive, "\\");

		// GetVolumeInformation(szDrive, szName, sizeof(szName), NULL, NULL, NULL, NULL, NULL);

		// Get infos on the Drive (type and Name)
		nType = GetDriveType(szDrive);
		switch (nType)
		{
        case DRIVE_FIXED:
			sprintf(szType, "%s", "Local Disk");
			break;
		case DRIVE_REMOVABLE:
			sprintf(szType, "%s", "Floppy");
			break;
        case DRIVE_CDROM:
			sprintf(szType, "%s", "CD-ROM");
			break;
        case DRIVE_REMOTE:
			sprintf(szType, "%s", "Network");
			break;
		default:
			sprintf(szType, "%s", "Unknown");
			break;
		}

		// Add it to the ListView
		LVITEM Item;
		Item.mask = LVIF_TEXT;
		Item.iItem = 0;
		Item.iSubItem = 0;
		Item.pszText = szTheDrive;
		int nItem = ListView_InsertItem(hWndLocalList, &Item);
		
		Item.mask = LVIF_TEXT;
		Item.iItem = nItem;
		Item.iSubItem = 1;
		Item.pszText = szType;
		ListView_SetItem(hWndLocalList, &Item);

		// Prepare it for Combo Box and add it
		strcat(szTheDrive, " - ");
		strcat(szTheDrive, szType);

		SendMessage(GetDlgItem(hWnd, IDC_LOCAL_DRIVECB), CB_ADDSTRING, 0, (LPARAM)szTheDrive); 
	}

	SendMessage(GetDlgItem(hWnd, IDC_LOCAL_DRIVECB), CB_SETCURSEL, -1, 0);
}



//
// Set gauge max value
//
void FileTransfer::SetTotalSize(HWND hWnd,DWORD dwTotalSize)
{
	SendDlgItemMessage(hWnd, IDC_PROGRESS, PBM_SETRANGE32, 0, dwTotalSize);
}

//
// Set gauge value
//
void FileTransfer::SetGauge(HWND hWnd,DWORD dwCount)
{
	SendDlgItemMessage(hWnd, IDC_PROGRESS, PBM_SETPOS, dwCount, 0);
}


//
// Display global progress (ratio files transfered/Total Files To transfer)
//
void FileTransfer::SetGlobalCount()
{
	char szGlobal[64];
	
	wsprintf(szGlobal,
			"File %d/%d",
			m_nFilesTransfered,
			m_nFilesToTransfer
			);
	SetDlgItemText(hWnd, IDC_GLOBAL_STATUS, szGlobal);
}


//
// Display current status and add it to the history combo box
//
void FileTransfer::SetStatus(LPSTR szStatus)
{
	// time_t lTime;
	char dbuffer [9];
	char tbuffer [9];

	char szHist[255 + 64];

	SetDlgItemText(hWnd, IDC_STATUS, szStatus);
	_tzset();
	// time(&lTime);
	_strdate(dbuffer);
	_strtime(tbuffer);
	sprintf(szHist, " > %s %s  - %s", dbuffer, tbuffer/*ctime(&lTime)*/, szStatus);
	LRESULT Index = SendMessage(GetDlgItem(hWnd, IDC_HISTORY_CB), CB_ADDSTRING, 0, (LPARAM)szHist); 
	SendMessage(GetDlgItem(hWnd, IDC_HISTORY_CB), CB_SETCURSEL, (WPARAM)Index, (LPARAM)0);		
}


//
// Request a file
//
void FileTransfer::RequestRemoteFile(LPSTR szRemoteFileName)
{
	if (!m_fFTAllowed) return;

	// TODO : hook error !
    rfbFileTransferMsg ft;
    ft.type = rfbFileTransfer;
	ft.contentType = rfbFileTransferRequest;
    ft.contentParam = 0;
	ft.length = Swap32IfLE(strlen(szRemoteFileName));
	ft.size = (m_pCC->kbitsPerSecond > 256) ? Swap32IfLE(0) : Swap32IfLE(1); // 1 means "Enable compression" 
    m_pCC->WriteExact((char *)&ft, sz_rfbFileTransferMsg, rfbFileTransfer);
    m_pCC->WriteExact((char *)szRemoteFileName, strlen(szRemoteFileName));

	return;
}


//
// Receive a file
//
bool FileTransfer::ReceiveFile(long lSize, int nLen)
{
	if (!m_fFTAllowed) return false;

	int fError = false;
	rfbFileTransferMsg ft;

	ft.type = rfbFileTransfer;
	ft.contentType = rfbFileHeader;
	ft.size = Swap32IfLE(0);
	ft.length = Swap32IfLE(0);

	char *szRemoteFileName = new char [nLen+1];
	if (szRemoteFileName == NULL) return false;
	memset(szRemoteFileName, 0, nLen+1);

	// Read in the Name of the file to copy (remote full name !)
	m_pCC->ReadExact(szRemoteFileName, nLen);

	char szStatus[255];

	// If lSize = -1 (0xFFFFFFFF) that means that the Src file on the remote machine
	// could not be opened for some reason (locked, doesn't exits any more...)
	if (lSize == -1)
	{
		sprintf(szStatus, " %s < %s > %s", sz_H12,szRemoteFileName,sz_H13); 
		// SetDlgItemText(pFileTransfer->hWnd, IDC_STATUS, szStatus);
		SetStatus(szStatus);
		delete [] szRemoteFileName;
		return false;
	}

	// Get the current path (destination path)
	char szDestFileName[MAX_PATH + 32];
	GetDlgItemText(hWnd, IDC_CURR_LOCAL, szDestFileName, sizeof(szDestFileName));

	// Check the free space on local destination drive
	bool fErr = false;
	ULARGE_INTEGER lpFreeBytesAvailable;
	ULARGE_INTEGER lpTotalBytes;		
	ULARGE_INTEGER lpTotalFreeBytes;
	unsigned long dwFreeKBytes;
	char *szDestPath = new char [strlen(szDestFileName) + 1];
	memset(szDestPath, 0, strlen(szDestFileName) + 1);
	strcpy(szDestPath, szDestFileName);
	*strrchr(szDestPath, '\\') = '\0'; // We don't handle UNCs for now

	if (!GetDiskFreeSpaceEx((LPCTSTR)szDestPath,
							&lpFreeBytesAvailable,
							&lpTotalBytes,
							&lpTotalFreeBytes)) fErr = true;
	delete [] szDestPath;
	dwFreeKBytes  = (unsigned long) (Int64ShraMod32(lpFreeBytesAvailable.QuadPart, 10));
	if (dwFreeKBytes < (unsigned long)(lSize / 1000)) fErr = true;

	if (fErr)
	{
		sprintf(szStatus, " %s < %s >", sz_H14,szRemoteFileName); 
		SetStatus(szStatus);
		delete [] szRemoteFileName;
		// Tell the server to cancel the transfer
		ft.size = Swap32IfLE(-1);
		m_pCC->WriteExact((char *)&ft, sz_rfbFileTransferMsg/*, rfbFileTransfer*/);
		return false;
	}

	// Parse the FileTime
	char szFileTime[18];
	char *p = strrchr(szRemoteFileName, ',');
	if (p == NULL)
		szFileTime[0] = '\0';
	else 
	{
		strcpy(szFileTime, p+1);
		*p = '\0';
	}
	strcat(szDestFileName, strrchr(szRemoteFileName, '\\') + 1);

	sprintf(szStatus, " %s < %s > (%ld Bytes) <<<",
			sz_H15,szDestFileName , lSize/*, szRemoteFileName*/); 
	SetStatus(szStatus);
	SetTotalSize(hWnd, lSize); // In bytes
	SetGauge(hWnd, 0); // In bytes
	UpdateWindow(hWnd);

	// Create the local Destination file
	HANDLE  hDestFile = CreateFile(szDestFileName, 
									GENERIC_WRITE,
									FILE_SHARE_READ | FILE_SHARE_WRITE, 
									NULL,
									CREATE_ALWAYS, 
									FILE_FLAG_SEQUENTIAL_SCAN,
									NULL);

	if (hDestFile == INVALID_HANDLE_VALUE)
	{
		sprintf(szStatus, " %s < %s > %s", sz_H12,szDestFileName,sz_H16); 
		SetStatus(szStatus);
		CloseHandle(hDestFile);
		delete [] szRemoteFileName;

		// Tell the server to cancel the transfer
		ft.size = Swap32IfLE(-1);
		m_pCC->WriteExact((char *)&ft, sz_rfbFileTransferMsg/*, rfbFileTransfer*/);
		return false;
	}

	delete [] szRemoteFileName;

	// Todo/ replace whith a simpler new BYTES[] alloc
	GLOBALHANDLE hBuff = GlobalAlloc(GMEM_MOVEABLE, sz_rfbBlockSize + 1024);
	if (hBuff == NULL)
	{
		CloseHandle(hDestFile);
		return false;
	}
	LPVOID lpBuff = GlobalLock(hBuff);
	if (lpBuff == NULL)
	{
		CloseHandle(hDestFile);
		return false;
	}

	// Tell the server that the transfer can start
	ft.size = Swap32IfLE(lSize); 
	m_pCC->WriteExact((char *)&ft, sz_rfbFileTransferMsg/*, rfbFileTransfer*/);

	// DWORD dwNbPackets = (DWORD)(lSize / sz_rfbBlockSize);
	DWORD dwNbReceivedPackets = 0;
	DWORD dwNbBytesWritten = 0;
	DWORD dwTotalNbBytesWritten = 0;
	int nCount = 0;
	bool fCompressed = true;

	// Copy Loop
	for (;;)
	{
		m_pCC->ReadExact((char *) &ft, sz_rfbFileTransferMsg);
		{
			if (ft.contentType == rfbFilePacket)
			{
				m_pCC->ReadExact((char *)lpBuff, Swap32IfLE(ft.length));
				{
					if (Swap32IfLE(ft.size) == 0) fCompressed = false;
					unsigned int nRawBytes = sz_rfbBlockSize + 1024;

					if (fCompressed)
					{
						// Decompress incoming data
						m_pCC->CheckZipBufferSize(nRawBytes);
						int nRetU = uncompress(	(unsigned char*)m_pCC->m_zipbuf,// Dest 
												(unsigned long *)&nRawBytes,// Dest len
												(unsigned char*)lpBuff,		// Src
												Swap32IfLE(ft.length)		// Src len
											 );							    

						if (nRetU != 0)
						{
							// vnclog.Print(0, _T("uncompress error in ReceiveFile: %d\n"), nRet);
							fError = true;
							// break;
						}
						Sleep(5);
					}

					BOOL fRes = WriteFile(hDestFile,
											fCompressed ? m_pCC->m_zipbuf : lpBuff,
											fCompressed ? nRawBytes : Swap32IfLE(ft.length),
											&dwNbBytesWritten,
											NULL);
					if (!fRes)
					{
						// TODO: Ask the server to stop the transfer
						fError = true;
						// break;
					}
					dwTotalNbBytesWritten += dwNbBytesWritten;
					dwNbReceivedPackets++;
				}
				// Refresh of the progress bar
				SetGauge(hWnd, dwTotalNbBytesWritten);
				PseudoYield(GetParent(hWnd));

				// Every 10 packets, test if the transfer must be stopped
				nCount++;
				if (nCount > 10)
				{
					if (m_fAbort || fError)
					{
						// Ask the server to stop the transfer
						ft.size = Swap32IfLE(-1);
						m_pCC->WriteExact((char *)&ft, sz_rfbFileTransferMsg/*, rfbFileTransfer*/);
					}
					else
					{
						// Tell the server to continue the transfer
						ft.size = Swap32IfLE(0);
						m_pCC->WriteExact((char *)&ft, sz_rfbFileTransferMsg/*, rfbFileTransfer*/);
					}
					nCount = 0;
				}
			}
			else if (ft.contentType == rfbEndOfFile)
			{
				// TODO : check dwNbReceivedPackets and dwTotalNbBytesWritten or test a checksum
				FlushFileBuffers(hDestFile);
				sprintf(szStatus, " %s < %s > %s", sz_H17,szDestFileName,sz_H18); 
				break;
			}
			else if (ft.contentType == rfbAbortFileTransfer)
			{
				fError = true;
				FlushFileBuffers(hDestFile);
				sprintf(szStatus, " %s < %s > %s", sz_H19,szDestFileName,sz_H20); 
				break;
			}
		}
	}

	// Set the DestFile Time Stamp
	if (strlen(szFileTime))
	{
		FILETIME DestFileTime;
		SYSTEMTIME FileTime;
		FileTime.wMonth  = atoi(szFileTime);
		FileTime.wDay    = atoi(szFileTime + 3);
		FileTime.wYear   = atoi(szFileTime + 6);
		FileTime.wHour   = atoi(szFileTime + 11);
		FileTime.wMinute = atoi(szFileTime + 14);
		FileTime.wMilliseconds = 0;
		FileTime.wSecond = 0;
		SystemTimeToFileTime(&FileTime, &DestFileTime);
		// ToDo: hook error
		SetFileTime(hDestFile, &DestFileTime, &DestFileTime, &DestFileTime);
	}

	CloseHandle(hDestFile);
	if (fError) DeleteFile(szDestFileName);
	GlobalUnlock(hBuff);
	GlobalFree(hBuff);

	SetStatus(szStatus);
	UpdateWindow(hWnd);

	// Sound notif
	MessageBeep(-1);

	return true;
}


//
// Offer a file
//
bool FileTransfer::OfferLocalFile(LPSTR szSrcFileName)
{
	if (!m_fFTAllowed) return false;

	char szStatus[255];

	// Open local src file
	HANDLE hSrcFile = CreateFile(
								szSrcFileName,		
								GENERIC_READ,		
								FILE_SHARE_READ,	
								NULL,				
								OPEN_EXISTING,		
								FILE_FLAG_SEQUENTIAL_SCAN,	
								NULL
								);				

	if (hSrcFile == INVALID_HANDLE_VALUE)
	{
		sprintf(szStatus, " ERROR: Access Problem with file < %s >", szSrcFileName); 
		SetStatus(szStatus);
		DWORD TheError = GetLastError();
		return false;
	}

	// Size of src file
	DWORD dwSrcSize = GetFileSize(hSrcFile, NULL); 
	if (dwSrcSize == -1)
	{
		sprintf(szStatus, " %s < %s >", sz_H21,szSrcFileName);
		SetStatus(szStatus);
		CloseHandle(hSrcFile);
		return false;
	}

	sprintf(szStatus, " %s < %s > (%ld Bytes) >>>",
			sz_H22,szSrcFileName , dwSrcSize/*, szDstFileName*/); 
	SetStatus(szStatus);
	SetTotalSize(hWnd, dwSrcSize); // In bytes
	SetGauge(hWnd, 0); // In bytes
	UpdateWindow(hWnd);

	// Add the File Time Stamp to the filename
	FILETIME SrcFileModifTime; 
	BOOL fRes = GetFileTime(hSrcFile, NULL, NULL, &SrcFileModifTime);
	if (!fRes)
	{
		sprintf(szStatus, " %s < %s >", sz_H23,szSrcFileName); 
		SetStatus(szStatus);
		CloseHandle(hSrcFile);
		return false;
	}

	CloseHandle(hSrcFile);

	TCHAR szDstFileName[MAX_PATH + 32];
	memset(szDstFileName, 0, MAX_PATH + 32);

	GetDlgItemText(hWnd, IDC_CURR_REMOTE, szDstFileName, sizeof(szDstFileName));
	if (!strlen(szDstFileName)) return false; // no destination dir selected - msgbox ?
	strcat(szDstFileName, strrchr(szSrcFileName, '\\') + 1);

	char szSrcFileTime[18];
	SYSTEMTIME FileTime;
	FileTimeToSystemTime(&SrcFileModifTime, &FileTime);
	wsprintf(szSrcFileTime,"%2.2d/%2.2d/%4.4d %2.2d:%2.2d",
			FileTime.wMonth,
			FileTime.wDay,
			FileTime.wYear,
			FileTime.wHour,
			FileTime.wMinute
			);
	strcat(szDstFileName, ",");
	strcat(szDstFileName, szSrcFileTime);

	// Send the FileTransferMsg with rfbFileTransferOffer
	// So the server creates the appropriate new file on the other side
    rfbFileTransferMsg ft;

    ft.type = rfbFileTransfer;
	ft.contentType = rfbFileTransferOffer;
    ft.contentParam = 0;
    ft.size = Swap32IfLE(dwSrcSize); // File Size in bytes
	ft.length = Swap32IfLE(strlen(szDstFileName));
    m_pCC->WriteExact((char *)&ft, sz_rfbFileTransferMsg, rfbFileTransfer);
	m_pCC->WriteExact((char *)szDstFileName, strlen(szDstFileName));

	return true;
}



//
//  SendFile 
// 
bool FileTransfer::SendFile(long lSize, int nLen)
{
	if (!m_fFTAllowed) return false;
	if (nLen == 0) return false; // Used when the local file could no be opened in OfferLocalFile

	char *szRemoteFileName = new char [nLen+1];
	if (szRemoteFileName == NULL) return false;
	memset(szRemoteFileName, 0, nLen+1);

	// Read in the Name of the file to copy (remote full name !)
	m_pCC->ReadExact(szRemoteFileName, nLen);

	char szStatus[255];

	// If lSize = -1 (0xFFFFFFFF) that means that the Dst file on the remote machine
	// could not be created for some reason (locked..)
	if (lSize == -1)
	{
		sprintf(szStatus, " %s < %s > %s",sz_H12, szRemoteFileName,sz_H24); 
		SetStatus(szStatus);
		sprintf(szStatus, " %s < %s >%s", sz_H25,szRemoteFileName,sz_H26); 
		SetStatus(szStatus);

		delete [] szRemoteFileName;
		return false;
	}

	// Build the local file path
	char szSrcFileName[MAX_PATH + 32];
	GetDlgItemText(hWnd, IDC_CURR_LOCAL, szSrcFileName, sizeof(szSrcFileName));
	strcat(szSrcFileName, strrchr(szRemoteFileName, '\\') + 1);

	delete [] szRemoteFileName;

	// Open src file
	HANDLE hSrcFile = CreateFile(
								szSrcFileName,		
								GENERIC_READ,		
								FILE_SHARE_READ,	
								NULL,				
								OPEN_EXISTING,		
								FILE_FLAG_SEQUENTIAL_SCAN,	
								NULL
								);				

	if (hSrcFile == INVALID_HANDLE_VALUE)
	{
		sprintf(szStatus, " %s < %s >", sz_H21,szSrcFileName); 
		SetStatus(szStatus);
		DWORD TheError = GetLastError();
		return false;
	}

	// Todo: replace this buffer allocation with a simpler one (new bytes[])
	GLOBALHANDLE hBuff = GlobalAlloc(GMEM_MOVEABLE, sz_rfbBlockSize);
	if (hBuff == NULL)
	{
		CloseHandle(hSrcFile);
		return false;
	}

	LPVOID lpBuff = GlobalLock(hBuff);
	if (lpBuff == NULL)
	{
		CloseHandle(hSrcFile);
		return false;
	}

	bool fError = false;
	DWORD dwNbBytesRead = 0;
	DWORD dwTotalNbBytesWritten = 0;
	bool fEof = false;
	// If the connection speed is > 256 Kbit/s, no need to compress.
	bool fCompress = (m_pCC->kbitsPerSecond <= 256);

	// Copy Loop
	while ( !fEof )
	{
		int nRes = ReadFile(hSrcFile, lpBuff, sz_rfbBlockSize, &dwNbBytesRead, NULL);
		if (!nRes)
		{
			fError = true;
			break;
		}
		
		if (dwNbBytesRead == 0)
		{
			fEof = true;
		}
		else
		{
			// Compress the data
			// (Compressed data can be longer if it was already compressed)
			unsigned int nMaxCompSize = sz_rfbBlockSize + 1024; // TODO: Improve this...
			if (fCompress)
			{
				m_pCC->CheckZipBufferSize(nMaxCompSize);
				int nRetC = compress((unsigned char*)(m_pCC->m_zipbuf),
											(unsigned long*)&nMaxCompSize,	
											(unsigned char*)lpBuff,
											dwNbBytesRead
											);
				if (nRetC != 0)
				{
					// Todo: send data uncompressed instead
					fError = true;
					break;
				}
				Sleep(5);
			}

			// If data compressed is larger, we're presumably dealing with already compressed data.
			if (nMaxCompSize > dwNbBytesRead)
				fCompress = false;

			// Send the FileTransferMsg with rfbFilePacket
			rfbFileTransferMsg ft;
			ft.type = rfbFileTransfer;
			ft.contentType = rfbFilePacket;
			ft.size = fCompress ? Swap32IfLE(1) : Swap32IfLE(0); 
			ft.length = fCompress ? Swap32IfLE(nMaxCompSize) : Swap32IfLE(dwNbBytesRead);
			m_pCC->WriteExact((char *)&ft, sz_rfbFileTransferMsg/*, rfbFileTransfer*/);
			if (fCompress)
				m_pCC->WriteExact((char *)m_pCC->m_zipbuf, nMaxCompSize);
			else
				m_pCC->WriteExact((char *)lpBuff, dwNbBytesRead);

			dwTotalNbBytesWritten += dwNbBytesRead;

			// Refresh progress bar
			SetGauge(hWnd, dwTotalNbBytesWritten);
			PseudoYield(GetParent(hWnd));
		
			if (m_fAbort)
			{
				fError = true;
				break;
			}
		}
	}

	CloseHandle(hSrcFile);
	GlobalUnlock(hBuff);
	GlobalFree(hBuff);
	
	if ( !fError)
	{
		rfbFileTransferMsg ft;

		ft.type = rfbFileTransfer;
		ft.contentType = rfbEndOfFile;
		m_pCC->WriteExact((char *)&ft, sz_rfbFileTransferMsg/*, rfbFileTransfer*/);
		sprintf(szStatus, " %s < %s > %s",
			sz_H17,szSrcFileName,sz_H27/*, (int)((lTotalComp * 100) / dwTotalNbBytesWritten), fCompress ? "C" : "N"*//*, szDstFileName*/); 
	}
	else // Error during file transfer loop
	{
		rfbFileTransferMsg ft;
		ft.type = rfbFileTransfer;
		ft.contentType = rfbAbortFileTransfer;
		m_pCC->WriteExact((char *)&ft, sz_rfbFileTransferMsg/*, rfbFileTransfer*/);
		sprintf(szStatus, " %s < %s > %s", sz_H19,szSrcFileName,sz_H28); 
	}

	SetStatus(szStatus);
	UpdateWindow(hWnd);

	// Sound notif
	MessageBeep(-1);

	// if (nRet) return true; else return false;
	return !fError;
}



//
// Request the creation of a directory on the remote machine
//
void FileTransfer::CreateRemoteDirectory(LPSTR szDir)
{
    rfbFileTransferMsg ft;
    ft.type = rfbFileTransfer;
	ft.contentType = rfbCommand;
    ft.contentParam = rfbCDirCreate;
	ft.size = 0;
	ft.length = Swap32IfLE(strlen(szDir));
    m_pCC->WriteExact((char *)&ft, sz_rfbFileTransferMsg, rfbFileTransfer);
	m_pCC->WriteExact((char *)szDir, strlen(szDir));
	return;
}


//
// Request the deletion of a file on the remote machine
//
void FileTransfer::DeleteRemoteFile(LPSTR szFile)
{
    rfbFileTransferMsg ft;
    ft.type = rfbFileTransfer;
	ft.contentType = rfbCommand;
    ft.contentParam = rfbCFileDelete;
	ft.size = 0;
	ft.length = Swap32IfLE(strlen(szFile));
    m_pCC->WriteExact((char *)&ft, sz_rfbFileTransferMsg, rfbFileTransfer);
	m_pCC->WriteExact((char *)szFile, strlen(szFile));
	return;
}

//
// Server's response to a directory creation command
//
bool FileTransfer::CreateRemoteDirectoryFeedback(long lSize, int nLen)
{
	char *szRemoteName = new char [nLen+1];
	if (szRemoteName == NULL) return false;
	memset(szRemoteName, 0, nLen+1);
	m_pCC->ReadExact(szRemoteName, nLen);
	char szStatus[255+64];

	if (lSize == -1)
	{
		sprintf(szStatus, "%s < %s > %s", sz_H29,szRemoteName,sz_H30); 
		SetStatus(szStatus);
		delete [] szRemoteName;
		return false;
	}
	sprintf(szStatus, "%s < %s > %s",sz_H31, szRemoteName,sz_H32); 
	SetStatus(szStatus);
	// Refresh the remote list
	ListView_DeleteAllItems(GetDlgItem(hWnd, IDC_REMOTE_FILELIST));
	RequestRemoteDirectoryContent(hWnd, "");

	delete [] szRemoteName;
	return true;
}


//
// Server's response to a File Deletion command
//
bool FileTransfer::DeleteRemoteFileFeedback(long lSize, int nLen)
{
	char *szRemoteName = new char [nLen+1];
	if (szRemoteName == NULL) return false;
	memset(szRemoteName, 0, nLen+1);
	m_pCC->ReadExact(szRemoteName, nLen);
	char szStatus[255+64];

	if (lSize == -1)
	{
		sprintf(szStatus, "%s < %s > %s", sz_H33,szRemoteName,sz_H30); 
		SetStatus(szStatus);
		delete [] szRemoteName;
		return false;
	}
	sprintf(szStatus, "%s < %s > %s", sz_H17,szRemoteName,sz_H34); 
	SetStatus(szStatus);
	// Refresh the remote list
	ListView_DeleteAllItems(GetDlgItem(hWnd, IDC_REMOTE_FILELIST));
	RequestRemoteDirectoryContent(hWnd, "");

	delete [] szRemoteName;
	return true;
}





// It's exceedingly unlikely, but possible, that if two modal dialogs were
// closed at the same time, the static variables used for transfer between 
// window procedure and this method could overwrite each other.
int FileTransfer::DoDialog()
{
 	return DialogBoxParam(pApp->m_instance, DIALOG_MAKEINTRESOURCE(IDD_FILETRANSFER_DLG), 
		NULL, (DLGPROC) FileTransferDlgProc, (LONG) this);
}


//
//
//
BOOL CALLBACK FileTransfer::FileTransferDlgProc(  HWND hWnd,  UINT uMsg,  WPARAM wParam, LPARAM lParam ) {
	// This is a static method, so we don't know which instantiation we're 
	// dealing with. But we can get a pseudo-this from the parameter to 
	// WM_INITDIALOG, which we therafter store with the window and retrieve
	// as follows:
	FileTransfer *_this = (FileTransfer *) GetWindowLong(hWnd, GWL_USERDATA);

	switch (uMsg)
	{
	case WM_TIMER:
		{
			// We have to wait for NetBuf flush
			// !!!: Not necessary anymore - Pb solved on server side.
			DWORD lTime = timeGetTime();
			// DWORD lLastTime = _this->m_pCC->m_lLastRfbRead;
			// DWORD lDelta = abs(timeGetTime() - _this->m_pCC->m_lLastRfbRead);
			if (true/*meGetTime() - _this->m_pCC->m_lLastRfbRead) > 1000 */)
			{
				_this->m_fFileCommandPending = true;
				_this->RequestPermission();
				if (KillTimer(hWnd, _this->m_timer))
					_this->m_timer = 0;
			}
		break;
		}
	case WM_INITDIALOG:
		{
            SetWindowLong(hWnd, GWL_USERDATA, lParam);
            FileTransfer *_this = (FileTransfer *) lParam;
            // CentreWindow(hWnd);
			_this->hWnd = hWnd;

			// Window always on top if Fullscreen On
			if (_this->m_pCC->InFullScreenMode())
			{
				RECT Rect;
				GetWindowRect(hWnd, &Rect);
				SetWindowPos(hWnd, 
							HWND_TOPMOST,
							Rect.left,
							Rect.top,
							Rect.right - Rect.left,
							Rect.bottom - Rect.top,
							SWP_SHOWWINDOW);
			}

			SetForegroundWindow(hWnd);

			// Set the title 
			const long lTitleBufSize=256;			
			char szRemoteName[lTitleBufSize];
			char szTitle[lTitleBufSize];
			if (_snprintf(szRemoteName, 127 ,"%s", _this->m_pCC->m_desktopName) < 0 )
			{
				szRemoteName[128-4]='.';
				szRemoteName[128-3]='.';
				szRemoteName[128-2]='.';
				szRemoteName[128-1]=0x00;
			}	
			_snprintf(szTitle, lTitleBufSize-1," %s <%s>  -  IIC", sz_H35,szRemoteName);
			SetWindowText(hWnd, szTitle);

			// Create all the columns of the Files ListViews
			LVCOLUMN Column;
			Column.mask = LVCF_FMT|LVCF_WIDTH|LVCF_TEXT|LVCF_ORDER|LVCF_SUBITEM;
			Column.fmt = LVCFMT_LEFT;
			Column.cx = 120;
			Column.pszText = "Name";
			Column.iSubItem = 0;
			Column.iOrder = 0;
			HWND hWndList = GetDlgItem(hWnd, IDC_LOCAL_FILELIST);
			ListView_InsertColumn(hWndList, 0, &Column);
			hWndList = GetDlgItem(hWnd, IDC_REMOTE_FILELIST);
			ListView_InsertColumn(hWndList, 0, &Column);

			Column.mask = LVCF_FMT|LVCF_WIDTH|LVCF_TEXT|LVCF_ORDER|LVCF_SUBITEM;
			Column.fmt = LVCFMT_LEFT;
			Column.cx = 70;
			Column.pszText = "Size";
			Column.iSubItem = 1;
			Column.iOrder = 1;
			hWndList = GetDlgItem(hWnd, IDC_LOCAL_FILELIST);
			ListView_InsertColumn(hWndList, 1, &Column);
			hWndList = GetDlgItem(hWnd, IDC_REMOTE_FILELIST);
			ListView_InsertColumn(hWndList, 1, &Column);

			Column.mask = LVCF_FMT|LVCF_WIDTH|LVCF_TEXT|LVCF_ORDER|LVCF_SUBITEM;
			Column.fmt = LVCFMT_LEFT;
			Column.cx = 100;
			Column.pszText = "Modified";
			Column.iSubItem = 2;
			Column.iOrder = 2;
			hWndList = GetDlgItem(hWnd, IDC_LOCAL_FILELIST);
			ListView_InsertColumn(hWndList, 2, &Column);
			hWndList = GetDlgItem(hWnd, IDC_REMOTE_FILELIST);
			ListView_InsertColumn(hWndList, 2, &Column);

			// Create the status bar
			HWND hStatusBar = CreateStatusWindow(WS_VISIBLE|WS_CHILD, sz_H36 , hWnd, IDC_STATUS);

			// Populate the Local listboxes with local drives
			_this->ListDrives(hWnd);

			// Populate the remote listboxes with remote drives if allowed by the server
			if (false/*!_this->m_pCC->m_pEncrypt->IsEncryptionEnabled()*/)
			{
				/*
				if (!_this->m_fFileCommandPending)
				{
					_this->m_fFileCommandPending = true;
					// RequestRemoteDrives(_this);
					RequestPermission(_this);
					SetStatus(_this, " Connecting. Please Wait...");
				}
				*/
			}
			else
			{
				_this->m_timer = SetTimer( hWnd, 3333,  1000, NULL);
				_this->SetStatus(sz_H37);
			}
            return TRUE;
		}
		break;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			return TRUE;

		case IDCANCEL:
			EndDialog(hWnd, FALSE);
			return TRUE;

		// Send selected files
		case IDC_UPLOAD_B:
			{
			if (_this->m_fFileCommandPending) break;

			TCHAR szSelectedFile[128];
			TCHAR szCurrLocal[MAX_PATH];
			TCHAR szDstFile[MAX_PATH + 32];
			memset(szSelectedFile, 0, 128);
			memset(szCurrLocal, 0, MAX_PATH);
			memset(szDstFile, 0, MAX_PATH + 32);
 
			int nSelected = -1;
			int nCount;
			HWND hWndLocalList = GetDlgItem(hWnd, IDC_LOCAL_FILELIST);
			HWND hWndRemoteList = GetDlgItem(hWnd, IDC_REMOTE_FILELIST);

			LVITEM Item;
			Item.mask = LVIF_TEXT;
			Item.iSubItem = 0;
			Item.pszText = szSelectedFile;
			Item.cchTextMax = 128;

			// If no destination is set,nothing to do.
			GetDlgItemText(hWnd, IDC_CURR_REMOTE, szDstFile, sizeof(szDstFile));
			if (!strlen(szDstFile)) break; // no dest dir selected

			// Get all the selected files on check if they already exist on remote side
			// If they already exist, the user is prompted for overwrite
			// Store the indexes of selected files in a list
			_this->m_FilesList.clear();
			nCount = ListView_GetItemCount(hWndLocalList);
			for (nSelected = 0; nSelected < nCount; nSelected++)
			{
				if(ListView_GetItemState(hWndLocalList, nSelected, LVIS_SELECTED) & LVIS_SELECTED)
				{
					Item.iItem = nSelected;
					ListView_GetItem(hWndLocalList, &Item);
					if (szSelectedFile[0] != '(') // Only a file can be transfered
					{ 
						LVFINDINFO Info;
						Info.flags = LVFI_STRING;
						Info.psz = (LPSTR)szSelectedFile;
						int nTheIndex = ListView_FindItem(hWndRemoteList, -1, &Info);
						if (nTheIndex > -1) 
						{
							char szMes[MAX_PATH + 96];
							wsprintf(szMes, "%s < %s > %s", sz_H17,szSelectedFile,sz_H38);
							if (MessageBox(	NULL, _T(szMes), 
											sz_H39, 
											MB_YESNO | MB_ICONQUESTION | MB_SETFOREGROUND | MB_TOPMOST | MB_DEFBUTTON2) == IDNO)
								continue;
						}
						// Add the file to the list
						_this->m_FilesList.push_back(nSelected);
					}
				}
			}
			if (_this->m_FilesList.size() == 0) break;

			// Display Status
			char szLocalStatus[128];
			sprintf(szLocalStatus, " > %d %s", _this->m_FilesList.size(),sz_H40); 
			SetDlgItemText(hWnd, IDC_LOCAL_STATUS, szLocalStatus);
			sprintf(szLocalStatus, "%s %d %s", sz_H41,_this->m_FilesList.size(),sz_H42); 
			_this->SetStatus(szLocalStatus);

			_this->m_nFilesTransfered = 0;
			_this->m_nFilesToTransfer = _this->m_FilesList.size();

			// Disable buttons
			_this->DisableButtons(_this->hWnd);
			ShowWindow(GetDlgItem(hWnd, IDC_ABORT_B), SW_SHOW);

			// Get the fisrt selected file name
			_this->m_iFile = _this->m_FilesList.begin();
			Item.iItem = *_this->m_iFile;
			ListView_GetItem(hWndLocalList, &Item);

			GetDlgItemText(hWnd, IDC_CURR_LOCAL, szCurrLocal, sizeof(szCurrLocal));
			if (!strlen(szCurrLocal)) break; // no src dir selected
			strcat(szCurrLocal, szSelectedFile);

			// Request the first file of the list (-> triggers the transfer of the whole list)
			_this->m_fFileCommandPending = true;
			_this->m_fAbort = false;
			if (!_this->OfferLocalFile(szCurrLocal))
				_this->SendFiles(-1, 0); // If the first file could not be opened try next file

			}
			break;

		// Receive selected files
		case IDC_DOWNLOAD_B:
			{
			if (_this->m_fFileCommandPending) break;
			TCHAR szSelectedFile[128];
			TCHAR szCurrLocal[MAX_PATH];
			TCHAR szDstFile[MAX_PATH + 32];

			memset(szSelectedFile, 0, 128);
			memset(szCurrLocal, 0, MAX_PATH);
			memset(szDstFile, 0, MAX_PATH + 32);

			int nSelected = -1;
			int nCount;
			HWND hWndLocalList = GetDlgItem(hWnd, IDC_LOCAL_FILELIST);
			HWND hWndRemoteList = GetDlgItem(hWnd, IDC_REMOTE_FILELIST);

			LVITEM Item;
			Item.mask = LVIF_TEXT;
			Item.iSubItem = 0;
			Item.pszText = szSelectedFile;
			Item.cchTextMax = 128;

			// If no dst dir is selected, nothing to do
			GetDlgItemText(hWnd, IDC_CURR_LOCAL, szCurrLocal, sizeof(szCurrLocal));
			if (!strlen(szCurrLocal)) break; // no dst dir selected

			// Get all the selected files on check if they already exist on local side
			// If they already exist, the user is prompted for overwrite
			// Store the indexes of selected files in a list
			_this->m_FilesList.clear();
			nCount = ListView_GetItemCount(hWndRemoteList);
			for (nSelected = 0; nSelected < nCount; nSelected++)
			{
				if(ListView_GetItemState(hWndRemoteList, nSelected, LVIS_SELECTED) & LVIS_SELECTED)
				{
					Item.iItem = nSelected;
					ListView_GetItem(hWndRemoteList, &Item);
					if (szSelectedFile[0] != '(') // Only a file can be transfered
					{ 
						LVFINDINFO Info;
						Info.flags = LVFI_STRING;
						Info.psz = (LPSTR)szSelectedFile;
						int nTheIndex = ListView_FindItem(hWndLocalList, -1, &Info);
						if (nTheIndex > -1) 
						{
							char szMes[MAX_PATH + 96];
							wsprintf(szMes, "%s < %s > %s", sz_H17,szSelectedFile,sz_H43);
							if (MessageBox(	NULL, _T(szMes), 
											sz_H39, 
											MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2 | MB_SETFOREGROUND | MB_TOPMOST) == IDNO)
								continue;
						}
						// Add the file to the list
						_this->m_FilesList.push_back(nSelected);
					}
				}
			}
			if (_this->m_FilesList.size() == 0) break;

			// Display Status
			char szRemoteStatus[64];
			sprintf(szRemoteStatus, " > %d %s", _this->m_FilesList.size(),sz_H40); 
			SetDlgItemText(hWnd, IDC_REMOTE_STATUS, szRemoteStatus);
			sprintf(szRemoteStatus, "%s %d File(s) %s ",sz_H44, _this->m_FilesList.size(),sz_H45); 
			_this->SetStatus(szRemoteStatus);

			_this->m_nFilesTransfered = 0;
			_this->m_nFilesToTransfer = _this->m_FilesList.size();

			// Disable buttons
			_this->DisableButtons(_this->hWnd);
			ShowWindow(GetDlgItem(_this->hWnd, IDC_ABORT_B), SW_SHOW);

			// Get the fisrt selected file name
			_this->m_iFile = _this->m_FilesList.begin();
			Item.iItem = *_this->m_iFile;
			ListView_GetItem(hWndRemoteList, &Item);

			GetDlgItemText(hWnd, IDC_CURR_REMOTE, szDstFile, sizeof(szDstFile));
			if (!strlen(szDstFile)) break; // no src dir selected
			strcat(szDstFile, szSelectedFile);

			// Request the first file of the list (-> triggers the transfer of the whole list)
			_this->m_fFileCommandPending = true;
			_this->m_fAbort = false;
			_this->RequestRemoteFile(szDstFile);

			}
			break;

		case IDC_LOCAL_ROOTB:
			{
			char ofDir[MAX_PATH];
			int nSelected = SendMessage(GetDlgItem(hWnd, IDC_LOCAL_DRIVECB), CB_GETCURSEL, 0, 0); 
			if (nSelected == -1) break;
			SendMessage(GetDlgItem(hWnd, IDC_LOCAL_DRIVECB), CB_GETLBTEXT, (WPARAM)nSelected, (LPARAM)ofDir); 
			ofDir[4] = '\0'; // Hum...
			_this->PopulateLocalListBox(hWnd, ofDir);
			}
			break;

		case IDC_REMOTE_ROOTB:
			if (!_this->m_fFileCommandPending)
			{
				_this->m_fFileCommandPending = true;
				char ofDir[MAX_PATH];
				int nSelected = SendMessage(GetDlgItem(hWnd, IDC_REMOTE_DRIVECB), CB_GETCURSEL, 0, 0); 
				if (nSelected == -1) break;
				SendMessage(GetDlgItem(hWnd, IDC_REMOTE_DRIVECB), CB_GETLBTEXT, (WPARAM)nSelected, (LPARAM)ofDir); 
				ofDir[4] = '\0'; // Hum...
				_this->RequestRemoteDirectoryContent(hWnd, ofDir);					
			}
			break;

		case IDC_LOCAL_UPB:
			_this->PopulateLocalListBox(hWnd, "(..)");
			break;

		case IDC_REMOTE_UPB:
			if (!_this->m_fFileCommandPending)
			{
				_this->m_fFileCommandPending = true;
				_this->RequestRemoteDirectoryContent(hWnd, "(..)");					
			}
			break;

		case IDC_ABORT_B:
			_this->m_fAbort = true;
			break;

		case IDC_DELETE_B:
			// Delete Local File
			if (_this->m_fFocusLocal)
			{
				HWND hWndLocalList = GetDlgItem(hWnd, IDC_LOCAL_FILELIST);
				int nCount = ListView_GetSelectedCount(hWndLocalList);
				if (nCount == 0 || nCount > 1)
				{
					MessageBox(	NULL,
								sz_H46, 
								sz_H47, 
								MB_OK | MB_ICONINFORMATION | MB_SETFOREGROUND | MB_TOPMOST);
					break; 
				}

				char szMes[MAX_PATH + 96];
				TCHAR szSelectedFile[128];
				memset(szSelectedFile, 0, 128);
 				int nSelected = -1;

				TCHAR szCurrLocal[MAX_PATH];
				memset(szCurrLocal, 0, MAX_PATH);
				GetDlgItemText(hWnd, IDC_CURR_LOCAL, szCurrLocal, sizeof(szCurrLocal));
				if (!strlen(szCurrLocal)) break; // no dst dir selected

				LVITEM Item;
				Item.mask = LVIF_TEXT;
				Item.iSubItem = 0;
				Item.pszText = szSelectedFile;
				Item.cchTextMax = 128;
				nCount = ListView_GetItemCount(hWndLocalList);
				for (nSelected = 0; nSelected < nCount; nSelected++)
				{
					if(ListView_GetItemState(hWndLocalList, nSelected, LVIS_SELECTED) & LVIS_SELECTED)
					{
						Item.iItem = nSelected;
						ListView_GetItem(hWndLocalList, &Item);
						if (szSelectedFile[0] == '(') continue;// Only a file can be deleted
						if (strlen(szCurrLocal) + strlen(szSelectedFile) > MAX_PATH)
						{
							// TOTO: Display Error
							continue;
						}
						wsprintf(szMes, "%s \n< %s > ?\n", sz_H48,szSelectedFile);
						if (MessageBox(	NULL, _T(szMes), 
										sz_H47, 
										MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2 | MB_SETFOREGROUND | MB_TOPMOST) == IDNO) continue;
						strcat(szCurrLocal, szSelectedFile);
						if (!DeleteFile(szCurrLocal))
						{
							wsprintf(szMes, "%s < %s >", sz_H49,szCurrLocal);
							_this->SetStatus(szMes);
							break;
						}
						wsprintf(szMes, "%s < %s > %s", sz_H17,szCurrLocal,sz_H50);
						_this->SetStatus(szMes);
					}
				}
				// Refresh the Local List view
				ListView_DeleteAllItems(GetDlgItem(hWnd, IDC_LOCAL_FILELIST));
				_this->PopulateLocalListBox(hWnd, "");
			}
			else // Delete remote file
			{
				if (_this->m_fFileCommandPending) break;
				HWND hWndRemoteList = GetDlgItem(hWnd, IDC_REMOTE_FILELIST);
				int nCount = ListView_GetSelectedCount(hWndRemoteList);
				if (nCount == 0 || nCount > 1)
				{
					MessageBox(	NULL,
								sz_H46, 
								sz_H47, 
								MB_OK | MB_ICONINFORMATION | MB_SETFOREGROUND | MB_TOPMOST);
					break; 
				}

				char szMes[MAX_PATH + 96];
				TCHAR szSelectedFile[128];
				memset(szSelectedFile, 0, 128);
 				int nSelected = -1;

				TCHAR szCurrRemote[MAX_PATH];
				memset(szCurrRemote, 0, MAX_PATH);
				GetDlgItemText(hWnd, IDC_CURR_REMOTE, szCurrRemote, sizeof(szCurrRemote));
				if (!strlen(szCurrRemote)) break; // no dst dir selected

				LVITEM Item;
				Item.mask = LVIF_TEXT;
				Item.iSubItem = 0;
				Item.pszText = szSelectedFile;
				Item.cchTextMax = 128;
				nCount = ListView_GetItemCount(hWndRemoteList);
				for (nSelected = 0; nSelected < nCount; nSelected++)
				{
					if(ListView_GetItemState(hWndRemoteList, nSelected, LVIS_SELECTED) & LVIS_SELECTED)
					{
						Item.iItem = nSelected;
						ListView_GetItem(hWndRemoteList, &Item);
						if (szSelectedFile[0] == '(') continue;// Only a file can be deleted
						if (strlen(szCurrRemote) + strlen(szSelectedFile) > MAX_PATH) continue;
						wsprintf(szMes, "%s \n< %s > ?\n", sz_H51,szSelectedFile);
						if (MessageBox(	NULL, _T(szMes), 
										sz_H47, 
										MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2 | MB_SETFOREGROUND | MB_TOPMOST) == IDNO) continue;
						_this->m_fFileCommandPending = true;
						strcat(szCurrRemote, szSelectedFile);
						_this->DeleteRemoteFile(szCurrRemote);
					}
				}
			}
			break;

		case IDC_NEWFOLDER_B:
			// Create Local Folder
			if (_this->m_fFocusLocal)
			{
				char szMes[MAX_PATH + 96];
				TCHAR szCurrLocal[MAX_PATH];
				GetDlgItemText(hWnd, IDC_CURR_LOCAL, szCurrLocal, sizeof(szCurrLocal));
				if (!strlen(szCurrLocal)) break; // no dst dir selected
				
				_this->DoFTParamDialog(sz_H57,sz_H52);
				if (strlen(_this->m_szFTParam) == 0 || (strlen(szCurrLocal) + strlen(_this->m_szFTParam)) > 248) 
				{
					// TODO: Error Message
					break;
				}
				strcat(szCurrLocal, _this->m_szFTParam);
				if (!CreateDirectory(szCurrLocal, NULL))
				{
					// TODO: Error Message
					wsprintf(szMes, "%s < %s >", sz_H53,szCurrLocal);
					_this->SetStatus(szMes);
					break;
				}

				wsprintf(szMes, "%s < %s > %s", sz_H54,szCurrLocal,sz_H55);
				_this->SetStatus(szMes);

				// Refresh the Local List view
				ListView_DeleteAllItems(GetDlgItem(hWnd, IDC_LOCAL_FILELIST));
				_this->PopulateLocalListBox(hWnd, "");

			}
			else // Create Remote Folder
			{
				if (_this->m_fFileCommandPending) break;
				TCHAR szCurrRemote[MAX_PATH];
				GetDlgItemText(hWnd, IDC_CURR_REMOTE, szCurrRemote, sizeof(szCurrRemote));
				if (!strlen(szCurrRemote)) break; // no dst dir selected

				_this->DoFTParamDialog(sz_H57,sz_H56);
				if (strlen(_this->m_szFTParam) == 0 || (strlen(szCurrRemote) + strlen(szCurrRemote)) > 248) 
				{
					// TODO: Error Message
					break;
				}

				_this->m_fFileCommandPending = true;
				strcat(szCurrRemote, _this->m_szFTParam);
				_this->CreateRemoteDirectory(szCurrRemote);
			}
			break;

		case IDC_LOCAL_DRIVECB: 
            switch (HIWORD(wParam)) 
            { 
		        case CBN_SELCHANGE:
					char ofDir[MAX_PATH];
					int nSelected = SendMessage(GetDlgItem(hWnd, IDC_LOCAL_DRIVECB), CB_GETCURSEL, 0, 0); 
					if (nSelected == -1) break;
					SendMessage(GetDlgItem(hWnd, IDC_LOCAL_DRIVECB), CB_GETLBTEXT, (WPARAM)nSelected, (LPARAM)ofDir); 
					ofDir[4] = '\0'; // Hum...
					_this->PopulateLocalListBox(hWnd, ofDir);
					// UpdateWindow(hWnd);
					break;
			}
			break;

		case IDC_REMOTE_DRIVECB:
            switch (HIWORD(wParam)) 
            { 
		        case CBN_SELCHANGE: 
					char ofDir[MAX_PATH];
					int nSelected = SendMessage(GetDlgItem(hWnd, IDC_REMOTE_DRIVECB), CB_GETCURSEL, 0, 0); 
					if (nSelected == -1) break;
					SendMessage(GetDlgItem(hWnd, IDC_REMOTE_DRIVECB), CB_GETLBTEXT, (WPARAM)nSelected, (LPARAM)ofDir); 
					ofDir[4] = '\0'; // Hum...
					_this->RequestRemoteDirectoryContent(hWnd, ofDir);					
					// UpdateWindow(hWnd);
					break;
			}
			break;

		}
		break;

	// Messages from ListViews
	case WM_NOTIFY:
		{
			// int nId = (int) wParam;
			LPNMHDR lpNmh = (LPNMHDR) lParam;

			switch(lpNmh->code)
			{
			case HDN_ITEMCLICK:
				return TRUE;

			case NM_SETFOCUS:
				if (lpNmh->hwndFrom == GetDlgItem(hWnd, IDC_LOCAL_FILELIST))
					_this->m_fFocusLocal = true;

				if (lpNmh->hwndFrom == GetDlgItem(hWnd, IDC_REMOTE_FILELIST))
					_this->m_fFocusLocal = false;

				return TRUE;

			case NM_DBLCLK:
				if (lpNmh->hwndFrom == GetDlgItem(hWnd, IDC_LOCAL_FILELIST))
				{
					_this->PopulateLocalListBox(hWnd, "");
				}

				if (lpNmh->hwndFrom == GetDlgItem(hWnd, IDC_REMOTE_FILELIST))
				{
					if (!_this->m_fFileCommandPending)
					{
						_this->m_fFileCommandPending = true;
						_this->RequestRemoteDirectoryContent(hWnd, "");
					}
				}
				return TRUE;
			}

		}
		break;

	case WM_DESTROY:
		if (_this->m_timer != 0) KillTimer(hWnd, _this->m_timer);
		EndDialog(hWnd, FALSE);
		return TRUE;
	}
	return 0;
}


//
// Params acquisition Dialog Box
// 
int FileTransfer::DoFTParamDialog(LPSTR szTitle, LPSTR szComment)
{
	strcpy(m_szFTParamTitle, szTitle);
	strcpy(m_szFTParamComment, szComment);
	return DialogBoxParam(pApp->m_instance, DIALOG_MAKEINTRESOURCE(IDD_FTPARAM_DLG), NULL, (DLGPROC) FTParamDlgProc, (LONG) this);
}


BOOL CALLBACK FileTransfer::FTParamDlgProc(  HWND hwnd,  UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	FileTransfer *_this = (FileTransfer *) GetWindowLong(hwnd, GWL_USERDATA);

	switch (uMsg)
	{
	case WM_INITDIALOG:
		{
			SetWindowLong(hwnd, GWL_USERDATA, lParam);
			_this = (FileTransfer *) lParam;
			CentreWindow(hwnd);

			// Set Title
			SetWindowText(hwnd, _this->m_szFTParamTitle);
			// Set Comment
			SetDlgItemText(hwnd, IDC_FTPARAMCOMMENT, _this->m_szFTParamComment);

			return TRUE;
		}

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDOK:
			{
				UINT res = GetDlgItemText( hwnd,  IDC_FTPARAM_EDIT, _this->m_szFTParam, 256);
				EndDialog(hwnd, TRUE);
				return TRUE;
			}
		case IDCANCEL:
			strcpy(_this->m_szFTParam, "");
			EndDialog(hwnd, FALSE);
			return TRUE;
		}
		break;
	case WM_DESTROY:
		EndDialog(hwnd, FALSE);
		return TRUE;
	}
	return 0;
}

//
//
//
void FileTransfer::DisableButtons(HWND hWnd)
{
	ShowWindow(GetDlgItem(hWnd, IDC_UPLOAD_B), SW_HIDE);
	ShowWindow(GetDlgItem(hWnd, IDC_DOWNLOAD_B), SW_HIDE);
	ShowWindow(GetDlgItem(hWnd, IDC_DELETE_B), SW_HIDE);
	ShowWindow(GetDlgItem(hWnd, IDC_NEWFOLDER_B), SW_HIDE);
	EnableWindow(GetDlgItem(hWnd, IDC_LOCAL_FILELIST), FALSE);
	EnableWindow(GetDlgItem(hWnd, IDC_LOCAL_DRIVECB), FALSE);
	EnableWindow(GetDlgItem(hWnd, IDC_LOCAL_ROOTB), FALSE);
	EnableWindow(GetDlgItem(hWnd, IDC_LOCAL_UPB), FALSE);
	EnableWindow(GetDlgItem(hWnd, IDC_REMOTE_FILELIST), FALSE);
	EnableWindow(GetDlgItem(hWnd, IDC_REMOTE_DRIVECB), FALSE);
	EnableWindow(GetDlgItem(hWnd, IDC_REMOTE_ROOTB), FALSE);
	EnableWindow(GetDlgItem(hWnd, IDC_REMOTE_UPB), FALSE);
}

//
//
//
void FileTransfer::EnableButtons(HWND hWnd)
{
	ShowWindow(GetDlgItem(hWnd, IDC_ABORT_B), SW_HIDE);
	ShowWindow(GetDlgItem(hWnd, IDC_UPLOAD_B), SW_SHOW);
	ShowWindow(GetDlgItem(hWnd, IDC_DOWNLOAD_B), SW_SHOW);
	ShowWindow(GetDlgItem(hWnd, IDCANCEL), SW_SHOW);
	ShowWindow(GetDlgItem(hWnd, IDC_DELETE_B), SW_SHOW);
	ShowWindow(GetDlgItem(hWnd, IDC_NEWFOLDER_B), SW_SHOW);
	EnableWindow(GetDlgItem(hWnd, IDC_LOCAL_FILELIST), TRUE);
	EnableWindow(GetDlgItem(hWnd, IDC_LOCAL_DRIVECB), TRUE);
	EnableWindow(GetDlgItem(hWnd, IDC_LOCAL_ROOTB), TRUE);
	EnableWindow(GetDlgItem(hWnd, IDC_LOCAL_UPB), TRUE);
	EnableWindow(GetDlgItem(hWnd, IDC_REMOTE_FILELIST), TRUE);
	EnableWindow(GetDlgItem(hWnd, IDC_REMOTE_DRIVECB), TRUE);
	EnableWindow(GetDlgItem(hWnd, IDC_REMOTE_ROOTB), TRUE);
	EnableWindow(GetDlgItem(hWnd, IDC_REMOTE_UPB), TRUE);
}
