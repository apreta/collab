//  Copyright (C) 1999 AT&T Laboratories Cambridge. All Rights Reserved.
//
//  This file is part of the VNC system.
//
//  The VNC system is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// If the source code for the VNC system is not available from the place 
// whence you received this file, check http://www.uk.research.att.com/vnc or 
// contact the authors on vnc@uk.research.att.com for information on obtaining it.
//
// Many thanks to Greg Hewgill <greg@hewgill.com> for providing the basis for 
// the full-screen mode.

#include "stdhdrs.h"
#include "vncviewer.h"
#include "ClientConnection.h"

// Parameters for scrolling in full screen mode
#define BUMPSCROLLBORDER 16
#define BUMPSCROLLAMOUNTX 32
#define BUMPSCROLLAMOUNTY 8
extern char sz_J1[128];
extern char sz_J2[64];

bool ClientConnection::InFullScreenMode() 
{
	return m_opts.m_FullScreen; 
};

// You can explicitly change mode by calling this
void ClientConnection::SetFullScreenMode(bool enable)
{
	if (m_opts.m_FullScreen==true) skipprompt2=true;
	else skipprompt2=false;
	m_opts.m_FullScreen = enable;
	RealiseFullScreenMode();

	// Modif sf@2002 - v1.1.0 - In case of server scaling
	// Clear the Window (in black)
    if (m_opts.m_nServerScale > 1)
	{
		RECT winrect;
		GetWindowRect(m_hwndMain, &winrect);
		int winwidth = winrect.right - winrect.left;
		int winheight = winrect.bottom - winrect.top;
		ObjectSelector b(m_hBitmapDC, m_hBitmap);
		PaletteSelector p(m_hBitmapDC, m_hPalette);
		RECT rect;
		SetRect(&rect, 0,0, winwidth, winheight);
		COLORREF bgcol = RGB(0x0, 0x0, 0x0);
		FillSolidRect(&rect, bgcol);
		// Update the whole screen 
		SendFullFramebufferUpdateRequest();
	}
}

// If the options have been changed other than by calling 
// SetFullScreenMode, you need to call this to make it happen.
void ClientConnection::RealiseFullScreenMode()
{
	LONG style = GetWindowLong(m_hwndMain, GWL_STYLE);
	if (m_opts.m_FullScreen) {

		// A bit crude here - we can skip the prompt on a registry setting.
		// We'll do this properly later.
		HKEY hRegKey;
		DWORD skipprompt = 0;
		if ( RegCreateKey(HKEY_CURRENT_USER, SETTINGS_KEY_NAME, &hRegKey)  != ERROR_SUCCESS ) {
	        hRegKey = NULL;
		} else {
			DWORD skippromptsize = sizeof(skipprompt);
			DWORD valtype;	
			if ( RegQueryValueEx( hRegKey,  "SkipFullScreenPrompt", NULL, &valtype, 
				(LPBYTE) &skipprompt, &skippromptsize) != ERROR_SUCCESS) {
				skipprompt = 0;
			}
			RegCloseKey(hRegKey);
		}

		if (!skipprompt && !skipprompt2)
			MessageBox(m_hwndMain, 
				sz_J1,
				sz_J2,
				MB_OK | MB_ICONINFORMATION | MB_TOPMOST | MB_SETFOREGROUND);

		/* Does not work yet
		// Used by VNCon - Copyright (C) 2001-2003 - Alastair Burr
		//ShowWindow(m_hwnd, SW_HIDE);
		*/
		ShowWindow(m_hwndMain, SW_MAXIMIZE);

		style = GetWindowLong(m_hwndMain, GWL_STYLE);
		style &= ~(WS_DLGFRAME | WS_THICKFRAME);
		SetWindowLong(m_hwndMain, GWL_STYLE, style);
		/* Does not work yet
		// Used by VNCon - Copyright (C) 2001-2003 - Alastair Burr
		// int cx = this->m_hScrollMax;
		// int cy = this->m_vScrollMax;
		*/
		int cx = GetSystemMetrics(SM_CXSCREEN);
		int cy = GetSystemMetrics(SM_CYSCREEN);
		if (m_opts.m_hParentWindow == NULL) {	// IIC
			SetWindowPos(m_hwndMain, HWND_TOPMOST, -1, -1, cx+3, cy+3, SWP_FRAMECHANGED);
		} else {
			SetWindowPos(m_hwndMain, HWND_TOPMOST, 0, m_opts.m_nVerticalOffset, cx-3, cy+3-m_opts.m_nVerticalOffset, SWP_FRAMECHANGED);	// minus splitter bar
		}
		CheckMenuItem(GetSystemMenu(m_hwndMain, FALSE), ID_FULLSCREEN, MF_BYCOMMAND|MF_CHECKED);
		if (m_opts.m_ShowToolbar)
			SetWindowPos(m_hwnd, m_hwndTBwin,0,m_TBr.bottom,m_winwidth, m_winheight, SWP_SHOWWINDOW);
		else 
		{
			if (m_opts.m_hParentWindow == NULL) {	// IIC
				SetWindowPos(m_hwnd, m_hwndTBwin,0,0,cx+3, cy+3, SWP_SHOWWINDOW);
			} else {
				SetWindowPos(m_hwnd, m_hwndTBwin,0,0,cx-3, cy+3, SWP_SHOWWINDOW);	// minus splitter bar
			}
			SetWindowPos(m_hwndTBwin, NULL ,0,0,0, 0, SWP_HIDEWINDOW);
		}

	} else {
		// IIC - don't want frame if going into another window
		if (m_opts.m_hParentWindow == NULL)
		{
			style |= WS_DLGFRAME | WS_THICKFRAME;
			SetWindowLong(m_hwndMain, GWL_STYLE, style);
		}
		SetWindowPos(m_hwndMain, HWND_NOTOPMOST, 0,0,100,100, SWP_NOMOVE | SWP_NOSIZE);
		ShowWindow(m_hwndMain, SW_NORMAL);
		CheckMenuItem(GetSystemMenu(m_hwndMain, FALSE), ID_FULLSCREEN, MF_BYCOMMAND|MF_UNCHECKED);
	}
}

bool ClientConnection::BumpScroll(int x, int y)
{
	// IIC	-- fix the scroll when meeting window is on
	RECT rect;
	GetClientRect(m_hwndMain, &rect);

	int dx = 0;
	int dy = 0;
	// IIC	-- fix the scroll when meeting window is on
	int rightborder = rect.right-BUMPSCROLLBORDER;
	int bottomborder = rect.bottom-BUMPSCROLLBORDER;
//	int rightborder = GetSystemMetrics(SM_CXSCREEN)-BUMPSCROLLBORDER;
//	int bottomborder = GetSystemMetrics(SM_CYSCREEN)-BUMPSCROLLBORDER-(m_TBr.bottom - m_TBr.top);
	if (x < (BUMPSCROLLBORDER * 4))
		dx = -BUMPSCROLLAMOUNTX * m_opts.m_scale_num / m_opts.m_scale_den;
	if (x >= rightborder)
		dx = +BUMPSCROLLAMOUNTX * m_opts.m_scale_num / m_opts.m_scale_den;;
	if (y < (BUMPSCROLLBORDER * 4))
		dy = -BUMPSCROLLAMOUNTY * m_opts.m_scale_num / m_opts.m_scale_den;;
	if (y >= bottomborder)
		dy = +BUMPSCROLLAMOUNTY * m_opts.m_scale_num / m_opts.m_scale_den;;
	if (dx || dy) {
		if (ScrollScreen(dx,dy)) {
			// If we haven't physically moved the cursor, artificially
			// generate another mouse event so we keep scrolling.
			POINT p;
			GetCursorPos(&p);
//			if (p.x == x && p.y == y)
//				SetCursorPos(x,y);
			SetCursorPos(p.x,p.y);
			return true;
		} 
	}
	return false;
}
