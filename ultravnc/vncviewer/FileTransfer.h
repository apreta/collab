/////////////////////////////////////////////////////////////////////////////
//  Copyright (C) 2002 Ultr@VNC Team Members. All Rights Reserved.
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// If the source code for the program is not available from the place from
// which you received this file, check 
// http://ultravnc.sourceforge.net/
//
////////////////////////////////////////////////////////////////////////////

#ifndef FILETRANSFER_H__
#define FILETRANSFER_H__
#pragma once

#include <list>

typedef std::list<int> FilesList; // List of files indexes to be sent or received

class ClientConnection;

class FileTransfer  
{
public:
	// Props
	VNCviewerApp		*m_pApp; 
	ClientConnection	*m_pCC;
	HWND				hWnd;
	bool				m_fAbort;
	FilesList			m_FilesList; // List of files indexes to be sent or received
	FilesList::iterator m_iFile;
	int					m_nFilesToTransfer;
	int					m_nFilesTransfered;
	bool				m_fFileCommandPending;
	bool				m_fFileTransferRunning;
	bool				m_fFTAllowed;
	int                 m_timer;
	bool				m_fFocusLocal;

	char                m_szFTParamTitle[128];
	char                m_szFTParamComment[64];
	char                m_szFTParam[248];

	// Methods
	FileTransfer(VNCviewerApp *pApp, ClientConnection *pCC);
	int DoDialog();
   	virtual ~FileTransfer();
	static BOOL CALLBACK FileTransferDlgProc(HWND hwndDlg,UINT uMsg,WPARAM wParam,LPARAM lParam);
	static BOOL CALLBACK LFBWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	static BOOL CALLBACK RFBWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	int DoFTParamDialog(LPSTR szTitle, LPSTR szComment);
	static BOOL CALLBACK FTParamDlgProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam );
	void DisableButtons(HWND hWnd);
	void EnableButtons(HWND hWnd);

	void ProcessFileTransferMsg(void);
	void RequestPermission();
	bool TestPermission(long lSize);
	void AddFileToFileList(HWND hWnd, int nListId, WIN32_FIND_DATA& fd);
	void RequestRemoteDirectoryContent(HWND hWnd, LPSTR szPath);
	void RequestRemoteDrives();
	void RequestRemoteFile(LPSTR szRemoteFileName);
	bool OfferLocalFile(LPSTR szSrcFileName);
	bool ReceiveFile(long lSize, int nLen);
	bool ReceiveFiles(long lSize, int nLen);
	bool SendFile(long lSize, int nLen);
	bool SendFiles(long lSize, int nLen);
	void ListRemoteDrives(HWND hWnd, int nLen);
	void HighlightTransferedFiles(HWND hSrcList, HWND hDstList);
	void PopulateRemoteListBox(HWND hWnd, int nLen);
	void PopulateLocalListBox(HWND hWnd, LPSTR szPath);
	void ListDrives(HWND hWnd);
	void CreateRemoteDirectory(LPSTR szDir);
	void DeleteRemoteFile(LPSTR szFile);
	bool CreateRemoteDirectoryFeedback(long lSize, int nLen);
	bool DeleteRemoteFileFeedback(long lSize, int nLen);

	void SetTotalSize(HWND hwnd,DWORD dwTotalSize);
	void SetGauge(HWND hwnd,DWORD dwCount);
	void SetGlobalCount();
	void SetStatus(LPSTR szStatus);

};

#endif // FILETRANSFER_H__

