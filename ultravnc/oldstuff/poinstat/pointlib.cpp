/////////////////////////////////////////////////////////////////////////////
//  Copyright (C) 2002 Ultr@VNC Team Members. All Rights Reserved.
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
//  USA.
//
// If the source code for the program is not available from the place from
// which you received this file, check 
// http://ultravnc.sourceforge.net/
//
/////////////////////////////////////////////////////////////////////////////

#include <windows.h>
#include <oleacc.h>
#include <winable.h>

#include "sstr.h"
//#include "objprops.h"
#include "pointlib.h"
#define ARRAYSIZE( a ) (sizeof(a)/sizeof(a[0]))
#define unused( a )

#ifdef DEBUG
#define Assert(f)       if (!(f)) DebugBreak()
#else
#define Assert(f)
#endif

typedef BOOL FN_GetObjProp( IAccessible *, const VARIANT &, StrWrPos &, BOOL );
typedef HRESULT (STDMETHODCALLTYPE IAccessible::*PMTH_IAccGetStr)( VARIANT, BSTR * );

BOOL GetObjProp_Name		( IAccessible * pAcc, const VARIANT & varChild, StrWrPos & str, BOOL bUseInvoke );
BOOL GetObjProp_Role		( IAccessible * pAcc, const VARIANT & varChild, StrWrPos & str, BOOL bUseInvoke );
BOOL GetObjProp_Location	( IAccessible * pAcc, const VARIANT & varChild, StrWrPos & str, BOOL bUseInvoke );



BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}





HINSTANCE           g_hInstance = NULL;
struct PropMapEnt
{
    TCHAR *             m_pName;
    FN_GetObjProp *     m_pfnGetProp;
    TCHAR *             m_pDesc;
};

PropMapEnt g_PropMap [ ] =
{
      {    TEXT("Name"),        GetObjProp_Name,        TEXT("(accName) Name of the object - often the label on or near the object.")                       },
       {    TEXT("Role"),        GetObjProp_Role,        TEXT("(accRole) Type of control (push button, menu item, etc.)")                                    },
       {    TEXT("Location"),    GetObjProp_Location,    TEXT("(accLocation) Position of the object on the screen - {left, top, width, height}")             },
};

BOOL GetObjProp_GenericStr( IAccessible *		pAcc,
						    const VARIANT &		varChild,
							StrWrPos &			str,
							BOOL				bUseInvoke,
					        PMTH_IAccGetStr		pmthGetStrProp,
							DISPID				DispID )
{
    HRESULT hr;
    BSTR bstrName = NULL;

    if( ! bUseInvoke )
    {
        // via IAccessible
        hr = (pAcc->*pmthGetStrProp)( varChild, & bstrName );
    }
    else
    {
        DISPPARAMS  dispp;
        EXCEPINFO   excepInfo;
        UINT        errArg;
        VARIANT     varResult;
		VARIANT		varArg = varChild;

        // via IDispatch::Invoke
        VariantInit( & varResult );

        dispp.cArgs = 1;
        dispp.cNamedArgs = 0;
        dispp.rgvarg = & varArg;
        dispp.rgdispidNamedArgs = NULL;

        FillMemory( & excepInfo, sizeof( excepInfo ), 0 );

        hr = pAcc->Invoke( DispID, IID_NULL, 0, DISPATCH_PROPERTYGET,
            &dispp, &varResult, &excepInfo, &errArg );
        if( SUCCEEDED( hr ) && ( varResult.vt == VT_BSTR ) )
            bstrName = varResult.bstrVal;
        else
            VariantClear( & varResult );
    }

    BOOL fError = FALSE;

    if( hr == S_OK )
    {
        if( bstrName )
        {
		    str << bstrName;
            SysFreeString( bstrName );
        }
        else
        {
            str << TEXT("none [null]");
        }
    }
    else
    {
        if( hr == S_FALSE )
			str << TEXT("none [false]");
		else if( hr == DISP_E_MEMBERNOTFOUND )
			str << TEXT("none [mnfd]");
		else if( hr == E_NOTIMPL )
			str << TEXT("none [nimpl]");
        else if( FAILED( hr ) )
        {
		    str << WriteError( hr, TEXT("getting string") );
            fError = TRUE;
        }

        if( bstrName )
        {
            str << TEXT(" [ BUG? - got non-NULL bstr ]");
            fError = TRUE;
        }
    }

    return ! fError;
}

BOOL GetObjProp_Name( IAccessible * pAcc, const VARIANT & varChild, StrWrPos & str, BOOL bUseInvoke )
{
	return GetObjProp_GenericStr( pAcc, varChild, str, bUseInvoke, & IAccessible::get_accName, DISPID_ACC_NAME );
}




BOOL GetObjProp_RoleOrState( IAccessible * pAcc, const VARIANT & varChild, StrWrPos & str, BOOL bUseInvoke,
							 BOOL IsRole )
{
    HRESULT hr;
    VARIANT varRetVal;

    VariantInit( & varRetVal );

    if( ! bUseInvoke )
    {
        // via IAccessible
		if( IsRole )
			hr = pAcc->get_accRole( varChild, &varRetVal );
		else
			hr = pAcc->get_accState( varChild, &varRetVal );
    }
    else
    {
        DISPPARAMS  dispp;
        EXCEPINFO   excepInfo;
        UINT        errArg;
		VARIANT		varArg = varChild;
        
        // via IDispatch::Invoke
        dispp.cArgs = 1;
        dispp.cNamedArgs = 0;
        dispp.rgvarg = & varArg;
        dispp.rgdispidNamedArgs = NULL;

        FillMemory( & excepInfo, sizeof( excepInfo ), 0 );

        hr = pAcc->Invoke( IsRole ? DISPID_ACC_ROLE : DISPID_ACC_STATE, IID_NULL, 0,
					DISPATCH_PROPERTYGET, &dispp, &varRetVal, &excepInfo, &errArg);
    }

    if( ! SUCCEEDED( hr ) )
	{
        str << WriteError( hr, IsRole ? TEXT("getting role") : TEXT("getting state") );
		return FALSE;
	}

    BOOL fError = FALSE;

    if( varRetVal.vt == VT_BSTR )
    {
		str << TEXT("\"") << varRetVal.bstrVal << TEXT("\" [ BUG? State/Role should not be a string ]");
        fError = TRUE;
    }
    else if( varRetVal.vt == VT_I4 )
    {
		if( IsRole )
		{
			int len = GetRoleText( varRetVal.lVal, str.ptr(), str.left() );
			str.advance( len );
		}
		else
		{
			int   iStateBit;
			DWORD lStateBits;

			BOOL FirstTimeRound = TRUE;

			// Convert state flags to comma separated list.
			for( iStateBit = 0, lStateBits = 1 ;
				 iStateBit < 32 ;
				 iStateBit++, ( lStateBits <<= 1 ) )
			{
				if( varRetVal.lVal & lStateBits )
				{
					if( FirstTimeRound )
                    {
						FirstTimeRound = FALSE;
                    }
					else
                    {
						str << TEXT(",");
                    }

					int len = GetStateText( lStateBits, str.ptr(), str.left() );
					str.advance( len );
				}
			}

			if( ! varRetVal.lVal )
			{
				// State of 0 - ie. "normal"...
				int len = GetStateText( 0, str.ptr(), str.left() );
				str.advance( len );
			}
		}
    }
    else
    {
		str << TEXT("[Error: unknown VARIANT type]");
        fError = TRUE;
    }

    VariantClear( & varRetVal );

    return ! fError;
}


BOOL GetObjProp_Role( IAccessible * pAcc, const VARIANT & varChild, StrWrPos & str, BOOL bUseInvoke )
{
	return GetObjProp_RoleOrState( pAcc, varChild, str, bUseInvoke, TRUE );
}


BOOL GetObjProp_Location( IAccessible * pAcc, const VARIANT & varChild, StrWrPos & str, BOOL bUseInvoke )
{
    HRESULT hr;
    RECT rc;

    SetRectEmpty( &rc );

	// Note - we refer to 'right' and 'bottom' below - but they're *really* 
	// width and height.

    if( ! bUseInvoke )
    {
        // via IAccessible
        hr = pAcc->accLocation( & rc.left, & rc.top, & rc.right, & rc.bottom, varChild );
    }
    else
    {
        // via IDispatch::Invoke
        DISPPARAMS  dispp;
        EXCEPINFO   excepInfo;
        UINT        errArg;
        VARIANT     rgvar[5];

        FillMemory(&excepInfo, sizeof(excepInfo), 0);

        dispp.cArgs = 5;
        dispp.cNamedArgs = 0;
        dispp.rgvarg = rgvar;
        dispp.rgdispidNamedArgs = NULL;

        // Arguments are BACKWARDS
        for( errArg = 4; errArg >= 1; errArg-- )
        {
            VariantInit(&rgvar[errArg]);
            rgvar[ errArg ].vt = VT_I4 | VT_BYREF;
            rgvar[ errArg ].plVal = & ( ( (long *)& rc )[ 4 - errArg ] );
        }

		// ok to do, since this is a simple VT_ type (ie. VT_I4)
        rgvar[ 0 ] = varChild;

        hr = pAcc->Invoke( DISPID_ACC_LOCATION, IID_NULL, 0, DISPATCH_METHOD,
            &dispp, NULL, &excepInfo, &errArg );
    }

    if( ! SUCCEEDED( hr ) )
	{
        str << WriteError( hr, TEXT("getting location") );
        return FALSE;
	}
        
    BOOL fError = FALSE;

    if( ! rc.right || ! rc.bottom )
	{
        str << TEXT("Bad location - ");
        fError = TRUE;
	}

	str << TEXT("{l:") << rc.left
	    << TEXT(", t:") << rc.top
	    << TEXT(", w:") << rc.right
	    << TEXT(", h:") << rc.bottom << TEXT("}");

    return ! fError;
}
// --------------------------------------------------------------------------
//
//  WinMain()
//
// --------------------------------------------------------------------------


int fnPointlib(pimportexport myimport)
{
	IAccessible * pAcc = NULL;
    VARIANT varChild;
	VariantInit( &varChild );
    //POINT pt;
    HRESULT hr = AccessibleObjectFromPoint( myimport->point, &pAcc, &varChild );

    //HWND hWnd = WindowFromPoint( point );

    StrBuf<1024> str;

    if( hr != S_OK || pAcc == NULL )
    {
        
    }
    else
    {
		{
		StrBuf<128> str;
		StrBuf<128> str2;
		SetRectEmpty( &myimport->rect );
		pAcc->accLocation( &myimport->rect.left, & myimport->rect.top, & myimport->rect.right, & myimport->rect.bottom, varChild );
		BOOL fSucceeded = (g_PropMap[ 0 ].m_pfnGetProp)( pAcc, varChild, str, FALSE );
		fSucceeded = (g_PropMap[ 1 ].m_pfnGetProp)( pAcc, varChild, str2, TRUE );
		strcpy(myimport->text,str.str());
		strcpy(myimport->text2,str2.str());
		}
        pAcc->Release();
    }
    return 0;
}


