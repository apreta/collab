//
// sstr - safe string handling utilities.
//
// Copyright (C) 1998 by Microsoft Corporation.  All rights reserved.
//


#include <windows.h>
#include "sstr.h"

void StrWrPos::Add( const TCHAR * pStr )
{
	while( m_CharsLeft && *pStr )
	{
		*m_pWritePos++ = *pStr++;
		m_CharsLeft--;
	}
}

void StrWrPos::Add( const TCHAR * pStr, int len )
{
	while( m_CharsLeft && len && *pStr )
	{
		*m_pWritePos++ = *pStr++;
		m_CharsLeft--;
        len--;
	}
}

void StrWrPos::Add( TCHAR ch )
{
	if( m_CharsLeft )
	{
		*m_pWritePos++ = ch;
	    m_CharsLeft--;
	}
}

void StrWrPos::Add( long i )
{
	TCHAR buf[ 16 ];
	wsprintf( buf, TEXT("%ld"), i );
	Add( buf );
}


void StrWrPos::Add( unsigned long i )
{
	TCHAR buf[ 16 ];
	wsprintf( buf, TEXT("%lu"), i );
	Add( buf );
}



#ifndef UNICODE

void StrWrPos::Add( const WCHAR * pStr )
{
    // Have to be careful not to call WideCharToMultibyte with length of 0:
    // in that case, it returns chars needed, not chars actually copied (which is 0).
    if( left() > 0 )
    {
        int len = WideCharToMultiByte( CP_ACP, 0, pStr, -1, ptr(), left(), NULL, NULL );
	    
        // Len, in this case, includes the terminating NUL - so subtract it, if
	    // we got one...
	    if( len > 0 )
		    len--;

        advance( len );
    }
}

#endif




void WriteHex::WriteToStrWrPos( StrWrPos & str ) const
{
	static const TCHAR * HexChars = TEXT("0123456789ABCDEF");

	//str << TEXT("0x");


	int Digit;
	if( m_Digits == -1 )
	{
		// Work out number of digits...
		Digit = 0;
		DWORD test = m_dw;
		while( test )
		{
			Digit++;
			test >>= 4;
		}

		// Special case for 0 - still want one digit.
		if( Digit == 0 )
			Digit = 1;
	}
	else
		Digit = m_Digits;

	while( Digit )
	{
		Digit--;
		str << HexChars[ ( m_dw >> (Digit * 4) ) & 0x0F ];
	}
}


void WriteError::WriteToStrWrPos( StrWrPos & str ) const
{
	str << TEXT("[Error");
	if( m_pWhere )
		str << TEXT(" ") << m_pWhere;
	str << TEXT(": hr=0x") << WriteHex( m_hr ) << TEXT(" - ");
    if( m_hr == S_FALSE )
    {
        str << TEXT("S_FALSE");
    }
    else
    {
        int len = FormatMessage( 
                FORMAT_MESSAGE_FROM_SYSTEM,
                NULL,
                m_hr,
                MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                str.ptr(),
                str.left(),
                NULL );
	    if( len > 2 )
		    len -= 2; // Ignore trailing /r/n that FmtMsg() adds...
	    str.advance( len );
    }
	str << TEXT("]");
}



StrWrPos & operator << ( StrWrPos & s, const GUID & guid )
{
    s << TEXT("{") << WriteHex( guid.Data1, 8 )  // DWORD
      << TEXT("-") << WriteHex( guid.Data2, 4 )  // WORD
      << TEXT("-") << WriteHex( guid.Data3, 4 )  // WORD
      << TEXT("-")
      << WriteHex( guid.Data4[ 0 ], 2 )
      << WriteHex( guid.Data4[ 1 ], 2 )
      << TEXT("-");

    for( int i = 2 ; i < 8 ; i++ )
    {
        s << WriteHex( guid.Data4[ i ], 2 ); // BYTE
    }
    s << TEXT("}");
    return s;
}

StrWrPos & operator << ( StrWrPos & s, const VARIANT & var )
{
    s << TEXT("[");
    switch( var.vt )
    {
        case VT_EMPTY:
        {
            s << TEXT("VT_EMPTY");
            break;
        }

        case VT_I4:
        {
            s << TEXT("VT_I4=0x");
            s << WriteHex( var.lVal );
            break;
        }

        case VT_I2:
        {
            s << TEXT("VT_I2=0x");
            s << WriteHex( var.iVal );
            break;
        }

        case VT_BOOL:
        {
            s << TEXT("VT_BOOL=");
            if( var.boolVal == VARIANT_TRUE )
                s << TEXT("TRUE");
            else if( var.boolVal == VARIANT_FALSE )
                s << TEXT("FALSE");
            else
                s << TEXT("?") << var.boolVal;
            break;
        }

        case VT_R4:
        {
            float fltval = var.fltVal;
            int x = (int)(fltval * 100);

            s << TEXT("VT_R4=") << x/100 << TEXT(".") 
                                << x/10 % 10 
                                << x % 10;
            break;
        }

        case VT_BSTR:
        {
            s << TEXT("VT_BSTR=\"") << var.bstrVal << TEXT("\"");
            break;
        }

        case VT_UNKNOWN:
        {
            s << TEXT("VT_UNKNOWN=0x") << WriteHex( var.punkVal, 8 );
            break;
        }

        case VT_DISPATCH:
        {
            s << TEXT("VT_DISPATCH=0x") << WriteHex( var.pdispVal, 8 );
            break;
        }

        default:
        {
            s << TEXT("VT_? ") << (long)var.vt;
            break;
        }
    }

    s << TEXT("]");
    return s;
}


StrWrPos & operator << ( StrWrPos & s, const POINT & pt )
{
    s << TEXT("{x:") << pt.x
      << TEXT(" y:") << pt.y
      << TEXT("}");
    return s;
}


StrWrPos & operator << ( StrWrPos & s, const RECT & rc )
{
    s << TEXT("{l:") << rc.left
      << TEXT(" t:") << rc.top
      << TEXT(" r:") << rc.right
      << TEXT(" b:") << rc.bottom
      << TEXT("}");
    return s;
}
