//
// sstr - safe string handling utilities.
//
// Copyright (C) 1998 by Microsoft Corporation.  All rights reserved.
//

///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//
//

//
// StrWrPos - represents a writable position in a string.
//
// Has methods to safely append to the string. Will not overrun buffer,
// truncates if reaches the end.
//
//
// Sample usage:
//
//     void SomeFunc( StrWrPos & str )
//     {
//         int i = 42;
//         str << TEXT("Value is: ") << i;
//     }
//
// Can be used with TCHAR*-style APIs, by using the ptr(), left() and
// advance() members. ptr() returns pointer to current write position,
// left() returns number of chars left, and advance() updates the write
// position.
//
//     void MyGetWindowText( StrWrPos & str )
//     {
//         int len = GetWindowText( hWnd, str.ptr(), str.left() );
//         str.advance( len );
//     }
//

//
// StrBuf - a character buffer.
//
// Use template parameters to specify size.
//
// Use str() method to return as a TCHAR* (for passing to APIs).
//
// Sample usage:
//
//     void Func( StrWrPos & str );
//
//     StrBuf<128> s;
//     s << TEXT("Text added: [");
//     Func( s ); // add more text to string
//     s << TEXT("]");
//
//     SetWindowText( hwnd, s.str() );
//

//
// WriteHex - helper class to output hex values:
//
// Sample usage:
//
//    str << TEXT("Value is:") << WriteHex( hwnd, 8 );
//
// Can optionally specify number of digits to output. (result will be
// 0-padded.)
//

//
// WriteError - helper class to output COM error values:
//
// Sample usage:
//
//    hr = ProcessData();
//    if( hr != S_OK )
//    {
//        str << WriteError( hr, TEXT("in ProcessData()");
//        LogError( str.str() );
//    }
//
// Use of description string is optional.
//

//
// STR - Handy class for temporary formatted strings.
//
// Instead of the long-winded:
//
//     StrBuf<128> str;
//     str << TEXT("value is: ") << val;
//     Log( str.str );
//
// ...you can write the more compact...
//
//     Log( STR() << TEXT("Value is:") << val );
//
// Works in anyplace a const TCHAR * is required.
//
// WARNING: Only use for _temporary_ strings. The string will last only as
// long as the function it is passed to. Once the function returns, the
// temporary string will be destroyed.
//
// For example, the following is incorrect usage:
//
//     TCHAR * pStr = STR() << TEXT("value is: ") << val; // WRONG!
//     // At this point, pStr points to the now-destroyed memory
//     // where the string briefly existed.
//     
// If you need a more permanent string, use StrBuf<> (see above) instead.
//
//
// (By default, uses a 128-char buffer on stack. Can overridden by
// using STRL<nnn> instead; but best to keep with the default in case
// the implementation changes later. May decide at some stage to use
// a process-wide cache of growable buffers instead of stack space.)
//
// Aside for those interested: C++'s "End of expression" rul for temporaties
// guarantees that the temporary STR() will exist until at least after
// after the function is called (and completed) - so we don't have to worry
// about using a destroyed object on the stack while in the function.
//

//
//
///////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////



//
// StrWrPos - represents a writable position in a string.
//
// See top of file for usage notes.
//

class StrWrPos
{
protected:
	// (used by derived StrBuf)
    LPTSTR  m_pWritePos;
	UINT    m_CharsLeft;

	void Reset( TCHAR * pStr, UINT Len )
	{
		m_pWritePos = pStr;
		m_CharsLeft = Len;
	}

public:

	StrWrPos( TCHAR * pStr, UINT Len )
		: m_pWritePos( pStr ),
		  m_CharsLeft( Len )
	{
	    // Do nothing
	}

	void Add( const TCHAR * pStr );
	void Add( const TCHAR * pStr, int len );
	void Add( TCHAR ch );
	void Add( long i );
	void Add( unsigned long i );
	void Add( int i )				{ Add( (long)			i ); }
	void Add( unsigned int i )		{ Add( (unsigned long)	i ); }
#ifndef UNICODE
    // On ansi builds, we still allow unicode strings to be added,
    // converting as we go. Useful for BSTRs and other OLE Unicode strings.
    void Add( const WCHAR * pStr );
#endif // UNICODE

    // short is not supported, since it causes confusion with WCHAR.

	TCHAR * ptr()              {  return m_pWritePos; } 
	const TCHAR * ptr() const  {  return m_pWritePos; } 
	unsigned int left()	const  {  return m_CharsLeft; }
	void advance( unsigned int c )
	{
        // Check for overflow...
        if( c > m_CharsLeft )
            c = m_CharsLeft;
		m_pWritePos += c;
		m_CharsLeft -= c;
	}
};


#define DECLARE_INSERT_OPERATOR( T ) /**/					\
	inline StrWrPos & operator << ( StrWrPos & str, T obj )	\
	{														\
		str.Add( obj );										\
		return str;											\
	}

DECLARE_INSERT_OPERATOR( const TCHAR * )
DECLARE_INSERT_OPERATOR( TCHAR )
DECLARE_INSERT_OPERATOR( long )
DECLARE_INSERT_OPERATOR( unsigned long )
DECLARE_INSERT_OPERATOR( int )
DECLARE_INSERT_OPERATOR( unsigned int )
#ifndef UNICODE
DECLARE_INSERT_OPERATOR( const WCHAR * )
#endif

#undef DECLARE_INSERT_OPERATOR




//
// StrBuf - a character buffer.
//
// See top of file for usage notes.
//


template <unsigned int MaxLen>
class StrBuf: public StrWrPos
{
	// +1 to allow for terminating NUL char
	TCHAR m_Buf[ MaxLen + 1 ];

public:

	StrBuf()
		: StrWrPos( m_Buf, MaxLen )
	{
		// Do nothing
	}

	const TCHAR * pStart() const
	{
		return m_Buf;
	}

	unsigned int Len() const
	{
		return m_pWritePos - m_Buf;
	}

	void Reset()
	{
		StrWrPos::Reset( m_Buf, MaxLen );
	}

	// Truncate string to max len of TruncLen.
	// No effect if string is already less than or equal to TruncLen chars
	void Truncate( unsigned int TruncLen )
	{
		if( Len() > TruncLen )
		{
			StrWrPos::Reset( m_Buf + TruncLen, MaxLen - TruncLen );
		}
	}

	// operator to 'convert' to TCHAR*. Automatically adds
	// terminating NUL char.
	//
	//  eg.
	//
	//  StrBuf<128> b;
	//  b << "Hello world!" ;
	//
	//  MessageBox( NULL, b.str(), "Test", MB_OK );
	//
	const TCHAR * str()
	{
		* m_pWritePos = '\0';
		return m_Buf;
	}
};



//
// WriteHex - helper class to output hex values:
//
// See top of file for usage notes.
//

class WriteHex
{
    DWORD m_dw;
	int   m_Digits;
public:

    // If Digits not specified, uses only as many as needed.
	WriteHex( DWORD dw, int Digits = -1 ) : m_dw( dw ), m_Digits( Digits ) { }

    // For pointer, pads if necessary to get std. ptr size.
    // (sizeof(ptr)*2, since 2 digits per byte in ptr).
	WriteHex( const void * pv, int Digits = sizeof(void*)*2 ) : m_dw( (DWORD)pv ), m_Digits( Digits ) { }

	void WriteToStrWrPos( StrWrPos & ) const;
};

inline
StrWrPos & operator << ( StrWrPos & s, const WriteHex & obj )
{
	obj.WriteToStrWrPos( s );
	return s;
}





//
// WriteError - helper class to output COM error values:
//
// See top of file for usage notes.
//

class WriteError
{
    HRESULT m_hr;
	LPCTSTR m_pWhere;
public:
	WriteError( HRESULT hr, LPCTSTR pWhere = NULL )
		: m_hr( hr ),
		  m_pWhere( pWhere )
	{ }

	void WriteToStrWrPos( StrWrPos & ) const;
};

inline
StrWrPos & operator << ( StrWrPos & s, const WriteError & obj )
{
	obj.WriteToStrWrPos( s );
	return s;
}




// Helper functions to support output of VARIANTs, GUIDs, POINTs and RECTs...

StrWrPos & operator << ( StrWrPos & s, const GUID & guid );
StrWrPos & operator << ( StrWrPos & s, const VARIANT & var );
StrWrPos & operator << ( StrWrPos & s, const POINT & pt );
StrWrPos & operator << ( StrWrPos & s, const RECT & rc );






// Handy classes for formatted string output - instead of writing:
//
// StrBuf<128> str;
// str << TEXT("value is: ") << val;
// Log( str.str );
//
// ... you can write...
//
// Log( STR() << TEXT("Value is:") << val );
//
// Works with any function that's expecting a TCHAR *. 
//
// By default, uses a 128-char buffer on stack. Can overridden by
// using STRL<nnn>:
//
// Log( STRL<64>() << TEXT("Value is:") << val );
//
// (Best to use the default if it works, though. A later implementation
// may use a cache of buffers which grow as necessary.)
//
// Note that C++ does guarantee that the temporary will exist until at least
// after the function is called - so we don't have to worry about using a
// destroyed object on the stack. (C++'s "end of expression" rule for
// temporaries.)
//
// (NB - may not work with macros, since the above may apply differently... ?)

//
// STRL is just StrBuf with a const TCHAR * conversion operator added,
// (so StrBuf's str() no longer needs to be called explicitly.)
//
// STR is STRL with a canned length. (Currently 128)
//
// The overloaded << operator preserves the type in a chain of <<'s
// - without this, using the usual StrWrPos& operator <<, the type
// in use would be StrWrPos &, not STR, after the first <<.
//
// For example, given Log( const TCHAR * pStr ):
//
//     Log( STR() << TEXT("Some text") );
//
// ... would not work without this overload; the type of the expression
// [ STR() << TEXT("some text") ] is StrWrPos& (since that's what that
// << returns), and that has no conversion-to-const-TCHAR* operator
// defined, so it can't work as a parameter to Log(). The compiler will
// complain that it can't convert from StrWrPos& to const TCHAR *.
//
// With the overload, however, the return type of operator << - and
// therefore the type of the expression - is STR(). The old StrWrPos
// operator<< gets called to do the real work, we just change the
// return type. Since STR does have a convertion-to-const-TCHAR* operator
// defined (in base class STRL), the compiler will use that.
//

template < int l >
class STRL: public StrBuf< l >
{
public:
    operator const TCHAR * () 
    {
        return str();
    }
};

template < int l, typename T >
STRL< l > & operator << ( STRL< l > & str, const T & t )
{
    // static_cast to StrWrPos& so that we use the original
    // StrWrPos & operator << ( ... ), instead of calling
    // this one recursively!
    static_cast< StrWrPos & >( str ) << t;
    return str;
}

class STR: public STRL<128>
{
};
