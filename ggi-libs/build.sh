#!/bin/sh

make=0

if uname | grep -i "MINGW32"; then
    prefix=/ggi
elif uname | grep -i "DARWIN"; then
    echo "Yay! A Mac"
    prefix=/opt/conferencing
else
    echo "Linux!"
    SUDO=sudo
    prefix=/opt/conferencing/client
fi

until [ -z "$1" ]
  do
    case $1 in
     --make)  make=1 ;;
     *) echo "Unknown option $1"; exit -1 ;;
    esac
    shift
  done  

if ! echo $PATH | grep "$prefix"; then
    echo "Adding bin directory to path"
    export PATH=$prefix/bin:$PATH
fi

echo "Building libgg"
cd libgg
if (( make == 0 )); then
	autoreconf -fi
    ./configure --prefix=$prefix
fi
if make && $SUDO make install; then
  echo "Ok"
else
  echo "Failed"; exit -1
fi

echo "Building libgii"
cd ../libgii
if (( make == 0 )); then
	autoreconf -fi
    ./configure --prefix=$prefix --disable-x --disable-conffile
fi
if make && $SUDO make install; then
  echo "Ok"
else
  echo "Failed"; exit -1
fi

echo "Building libggi"
cd ../libggi
if (( make == 0 )); then
	autoreconf -fi
    ./configure --prefix=$prefix --disable-x --disable-conffile
fi
if make && $SUDO make install; then
  echo "Ok"
else
  echo "Failed"; exit -1
fi
