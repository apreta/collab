==========================
Design notes for new LibGG
==========================


General purpose utilities
=========================


LibGG provides a few additional tools that are used internally, for
other GGI libraries and that can also be useful to the application
programmer.

data structures
      
      LibGG include a rather large set of C macros that can be used to
      define and manage common data structures: various flavour of
      lists, queues, splay and red-black trees.  These are only
      macro based and do not add any symbol to the library.

      These are largely used throughout GGI for internal data
      structures.


iterator

      Iterators, as defined by libgg, are more a convention to write
      iterators in a readable way than a library infrastructure.  An
      iterator consists in one (or more) initializer(s), an internal
      state, a next function and a cleanup function.  The idea is to
      let user code iterate over internal infrastructure without
      having to expose the implementation details.  The iterator can
      perform some kind of filtering, or give a flattened view of
      hierarchical structures.
      
      A good example of a non-trivial iterator is the target name
      matcher `gg_target_iter`.


observer

      The observer pattern is very useful and commonly found in many
      software packages, even if not explicitly called that way.  The
      problem is that every different software will often use a
      particular implementation of this pattern, depending on a
      specific use-case.  So usually everybody prefers to roll its
      own.  LibGG observers are defined as part of the GG-API (see
      later) infrastructure, as light-weight cooperation model between
      libraries.  The model is very simple:
      
      "struct gg_observable" defines a channel on which observer can
      be registered. An observer is a void* argument and callback
      receiving that argument and a void* API-specific message.  When
      the observable is triggered, all observers' callback will be
      fired.


It should be noted that although these utilities ease many aspects of
application development, they have non-trivial implicit limitations
and gotchas that must be well understood.  For example, it is
incorrect to simply iterate over a list using FOREACH to free all its
elements (the 'next' field would be invalidated).  Iterators can
become invalid if the program somehow modifies the internal data
structures which is being iterated over.  Finally, when designing
observables, extra care should be taken with regard to potential
recursive situations: an event triggers an observable, which notifies
an observer which reacts by doing something that leads to the same
observable being triggered again.

To sum up, these are mainly syntactic sugar for operations whose
implications must be well understood. They do not magically solve
complex problems.


Scopes
======


The scope were introduced in libgii 1.0 (part of GGI 2.2),
as a replacement for the gg*Module api.  A scope can be defined
as namespace with seizable things in it.  The scope interface
was introduced to abstract module loading, allowing dynamic
modules or builtin modules to be used transparently.  The scope
API can be used as a portable dynamic code loader.


Config parsing and module location
==================================


TODO


Realm, Stem, API
================

TODO
