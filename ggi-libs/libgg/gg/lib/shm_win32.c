/* $Id: shm_win32.c,v 1.1 2008/12/12 03:48:12 pekberg Exp $
******************************************************************************

   shm_win32: Very incomplete mapping from unix shm API to Win32.

   Copyright (C) 2004-2005 Peter Rosin		[peda@lysator.liu.se]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include <stdio.h>
#include <windows.h>
#include <ggi/internal/gg_replace.h>

int
shmget(key_t key, int size, int shmflg)
{
	return (int)CreateFileMapping(
		INVALID_HANDLE_VALUE,
		NULL,
		PAGE_READWRITE | SEC_COMMIT,
		0, /* size not larger than 2^32, I hope. */
		size,
		key);
}

void *
shmat(int shmid, const void *shmaddr, int shmflg)
{
	return MapViewOfFile((HANDLE)shmid, FILE_MAP_WRITE, 0, 0, 0);
}

int
shmdt(const void *shmaddr)
{
	return UnmapViewOfFile((LPCVOID)shmaddr);
}

int
shmctl(int shmid, int cmd, void *buf)
{
	return CloseHandle((HANDLE)shmid);
}

key_t
ftok(const char *pathname, int proj_id)
{
	static char object[MAX_PATH];
	char *ptr;
	snprintf(object, MAX_PATH, "gg-shm:%s:%d", pathname, proj_id);
	ptr = object;
	while ((ptr = strchr(ptr, '\\')))
		*ptr++ = '/';
	return object;
}
