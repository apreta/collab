/* $Id: channel.c,v 1.8 2007/04/10 18:29:27 soyt Exp $
******************************************************************************

   LibGG - Channel functions

   Copyright (C) 2006 Eric Faurot	[eric.faurot@gmail.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "plat.h"
#include <stdlib.h>
#include <string.h>
#include <limits.h>

#include <ggi/gg.h>
#include <ggi/gg-queue.h>
#include <ggi/internal/gg_debug.h>


struct gg_observer {
	ggfunc_channel_observe_cb    *cb;
	void                       *arg;
	GG_LIST_ENTRY(gg_observer) _others;
};


struct gg_channel {
	GG_LIST_HEAD(gg_observer_list,  gg_observer)  observers;
#define NHANDLERS 2
	ggfunc_channel_control_cb *handler[NHANDLERS];
	void * arg;
};


static void
ggClearChannel(struct gg_channel *channel)
{
  	struct gg_observer *curr, *next;

	DPRINT_MISC("ggClearChannel(channel=%p)\n", channel);
	
	for (curr = GG_LIST_FIRST(&(channel->observers));
	     curr != NULL;
	     curr = next) {
		next = GG_LIST_NEXT(curr, _others);
		DPRINT_MISC("! observer cb=%p, arg=%p still registered\n",
			    curr->cb, curr->arg);
		GG_LIST_REMOVE(curr, _others);
		curr->cb = NULL;
		curr->arg = NULL;
		free(curr);
	}
}


struct gg_channel*
ggNewChannel(void *arg, ggfunc_channel_control_cb *cb)
{
	struct gg_channel *channel;
	
	if((channel = malloc(sizeof(*channel))) == NULL)
		return NULL;
	
	GG_LIST_INIT(&(channel->observers));
	
	channel->arg = arg;
	channel->handler[0] = cb;
	channel->handler[1] = NULL;
	
	return channel;
	
}


void
ggDelChannel(struct gg_channel *channel)
{
	LIB_ASSERT(channel != NULL, "invalid channel\n");

	ggClearChannel(channel);
	free(channel);	
}


int
ggSetController(struct gg_channel *channel, ggfunc_channel_control_cb *cb)
{
	
	channel->handler[1] = cb;

	return GGI_OK;
}


struct gg_observer *
ggObserve(struct gg_channel *channel, ggfunc_channel_observe_cb *cb, void *arg)
{
	struct gg_observer * observer;
	
	DPRINT_MISC("ggObserve(channel=%p, cb=%p, arg=%p)\n", channel, cb, arg);
	
	observer = calloc(1, sizeof(*observer));
	if (observer == NULL) {
		DPRINT_MISC("! can not alloc mem for observer.\n");
		return NULL;
	}

	observer->arg = arg;
	observer->cb = cb;
	GG_LIST_INSERT_HEAD(&(channel->observers), observer, _others);

	return observer;
}


void
ggDelObserver(struct gg_observer *observer)
{
	LIB_ASSERT(observer != NULL, "invalid observer\n");
	DPRINT_MISC("ggDelObserver(observer=%p)\n", observer);

	GG_LIST_REMOVE(observer, _others);
	observer->cb = NULL;
	observer->arg = NULL;
	free(observer);
}


void
ggBroadcast(struct gg_channel *channel, uint32_t msg, void *data)
{
	struct gg_observer *curr, *next;

	DPRINT_MISC("ggBroadcast(channel=%p, msg=0x%x, data=%p)\n",
		    channel, msg, data);

	for (curr = GG_LIST_FIRST(&(channel->observers));
	     curr != NULL;
	     curr = next) {
		next = GG_LIST_NEXT(curr, _others);
		if (curr->cb(curr->arg, msg, data)) {
			ggDelObserver(curr);
		}
	}
}


int
ggControl(struct gg_channel *channel, uint32_t ctl, void *data)
{
	int err, i;
	
	DPRINT_MISC("ggControl(channel=%p, ctl=0x%x, data=%p)\n",
		    channel, ctl, data);
	
	err = GGI_ENOFUNC;
	
	for(i=0; i < NHANDLERS && err == GGI_ENOFUNC; i++) {
		if (channel->handler[i]) {
			err = channel->handler[i](channel->arg, ctl, data);
		}
	}
	
	return err;
}
