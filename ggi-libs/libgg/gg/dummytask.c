/* $Id: dummytask.c,v 1.1.1.1 2006/03/10 22:18:37 soyt Exp $
******************************************************************************

   LibGG - Configuration handling

   Copyright (C) 2004 Brian S. Julin

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/


#include <ggi/internal/gg.h>


uint32_t ggTimeBase(void) {
	return 0;
}

void _ggTaskExit(void) {
}

int _ggTaskInit(void) {
	return 0;
}

int ggAddTask(struct gg_task *task) {
	return 0;
}


int ggDelTask(struct gg_task *task) {
	return 0;
}


