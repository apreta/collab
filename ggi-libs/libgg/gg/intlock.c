/* $Id: intlock.c,v 1.1.1.1 2006/03/10 22:18:37 soyt Exp $
******************************************************************************

   LibGG - Mutex implementation using locks-on-int

   Copyright (C) 1998 Marcus Sundberg	[marcus@ggi-project.org]
   
   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "plat.h"
#include <stdlib.h>
#include <ggi/internal/gg.h>

#define SLEEP_TIME	1000 /* 1/1000 sec */

void *ggLockCreate(void) {
	int *ret;

	if ((ret = malloc(sizeof(int))) != NULL) {
		*ret = 0;
	}
	return (void *) ret;
}

int ggLockDestroy(void *lock) {
	int *lck = (int *)lock;

	if (*lck != 0) {
		return GGI_EBUSY;
	}
	free(lck);
	return 0;
}

int ggLock(void *lock) {
	int *lck = (int *)lock;

	while (++(*lck) != 1) {
		--(*lck);
		ggUSleep(SLEEP_TIME);
	}
	return 0;
}

int ggUnlock(void *lock) {
	int *lck = (int *)lock;
	
	--(*lck);
	return 0;
}

int ggTryLock(void *lock) {
	int *lck = (int *)lock;

	if (++(*lck) != 1) {
		--(*lck);
		return GGI_EBUSY;
	}
	return 0;
}

int _ggInitLocks(void) {
	return 0;
}

void _ggExitLocks(void) {
	return;
}
