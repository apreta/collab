/* $Id: api.c,v 1.27 2009/08/07 18:41:27 ggibecka Exp $
***************************************************************************

   LibGG - Realms, api and more

   Copyright (C) 2005  Eric Faurot [eric.faurot@gmail.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

***************************************************************************
*/


/*
 *  Notes:
 *
 *    - This implementation is not thread-safe.
 *    - "OUT" mark places where foreign code is called.
 */

#include "plat.h"
#include <stdlib.h>
#include <string.h>

#include <ggi/internal/gg-api.h>
#include <ggi/internal/gg_debug.h>
#include <ggi/errors.h>


#define API_LOCK   ggLock(_api_lock);
#define API_UNLOCK ggUnlock(_api_lock);

#define STEM_LOCK(s)   ggLock(s->lock);
#define STEM_UNLOCK(s) ggLock(s->unlock);


#define FOREACH_API(rlm, a)  GG_LIST_FOREACH(a, &(rlm->_apis), state->_others)
#define REMOVE_API(rlm, a)   GG_LIST_REMOVE(a, state->_others)
#define APPEND_API(rlm, a)   GG_LIST_INSERT_HEAD(&(rlm->_apis), a, state->_others)

#define FOREACH_STEM(rlm, s) GG_LIST_FOREACH(s, &(rlm->_stems), _others)
#define REMOVE_STEM(rlm, s)  GG_LIST_REMOVE(s, _others)
#define APPEND_STEM(rlm, s)  GG_LIST_INSERT_HEAD(&(rlm->_stems), s, _others)


/* Default realm */

static struct gg_realm _realm = GG_REALM_INITIALIZER(sizeof(struct gg_stem),
						     NULL, NULL);
static struct gg_realm *realm = &_realm;


struct gg_api *
ggGetAPIByName(const char *name)
{
	struct gg_api *api;
	
	DPRINT_API("ggGetAPIByName(name=\"%s\")\n", name);
	
	FOREACH_API(realm, api)
		if(!strcmp(api->name, name))
			break;
	
	return api;
}


static struct gg_api *
ggGetAPIByID(int id)
{
	struct gg_api *api;
	
	FOREACH_API(realm, api)
		if(api->id == id)
			break;
	
	return api;
}


int
ggInitAPI(struct gg_api* api)
{
	struct gg_api *tmp;
	int err;

	DPRINT_API("ggInitAPI(api=%p)\n", api);

	if(api->state) {
		/* already installed */
		goto done;
	}
	
	/* ensure that the API is not already installed */
	FOREACH_API(realm, tmp)
		if(!strcmp(tmp->name, api->name)) {
			DPRINT_API("! api named \"%s\" already installed.\n",
				   api->name);
			return GGI_EBUSY;
		}

	err = GGI_EUNKNOWN;
	
	if (ggInit() < 0) {
		DPRINT_API("! Could not initialize libgg.\n");
		goto fail0;
	}
	
	err = GGI_ENOMEM;
	
	api->state = calloc(1, sizeof(*api->state));
	if (api->state == NULL) {
		DPRINT_API("! Could not initialize libgg.\n");
		goto fail1;
	}
	
	GG_LIST_INIT(&api->state->modules);
	
	if((api->channel = ggNewChannel(api, NULL)) == NULL) {
		DPRINT_API("! Could not alloc channel.\n");
		goto fail2;
	}

	api->id = realm->_api_ids++;
	api->state->stemcount = 0;
	api->state->initcount = 0;

	APPEND_API(realm, api);

	err = GGI_EUNKNOWN;
	if (api->ops && api->ops->init)
		if((err = api->ops->init(api))) { /* OUT */
			DPRINT_API("! init failed with error %i.\n",err);
			/*
			 * Here we try to avoid wasting ids. So if the
			 * next id is this one plus one, we can reuse
			 * it. Otherwise it means another API was
			 * added during init
			 */
			if (api->id == realm->_api_ids - 1) 
				realm->_api_ids--;
			goto fail3;
		}
	
	DPRINT_API("- new api \"%s\" registered with id %i.\n",api->name,api->id);
 done:
	return api->state->initcount++;
 fail3:
	REMOVE_API(realm, api);
 fail2:
	free(api->state);
	api->state = NULL;
 fail1:
	ggExit();
 fail0:
	return err;
}


int
ggExitAPI(struct gg_api *api)
{
	int left;
	
	DPRINT_API("ggExitAPI(api=%p)\n", api);
	
	if(api->state == NULL) {
		DPRINT_API("! api \"%s\" is not installed.\n", api->name);
		return GGI_ENOTALLOC;
	}
	
	left = --api->state->initcount;
	if (left) {
		DPRINT_API("- api \"%s\" has %i invocation left.\n",
			   api->name, left);
		return left;
	}

	LIB_ASSERT(api->state->stemcount == 0, "! stemcount is not 0.");

	DPRINT_API("- really closing api \"%s\".\n", api->name);
	
	ggBroadcast(api->channel, GG_MESSAGE_API_EXIT, api);

	if (api->ops && api->ops->exit)
		api->ops->exit(api); /* OUT */

	REMOVE_API(realm, api);
	
	ggDelChannel(api->channel);
	api->channel = NULL;

	free(api->state);
	api->state = NULL;
	
	ggExit();
	
	return GGI_OK;
}


int
ggAttach(struct gg_api *api, struct gg_stem *stem)
{
	struct gg_api_slot * slot;
	struct gg_message_api_stem msg;
	int err;

	DPRINT_API("ggAttach(api=%p, stem=%p)\n", api, stem);

	/*
	 * Make sure the api is properly is initialized.  and claim a
	 * reference to the api.
	 */
	if ((err = ggInitAPI(api)) < 0) {
		DPRINT_API("! could not initialize api.\n");
		return err;
	}

	/* alloc more slots in the api slot vector if needed */

	if (api->id >= stem->_api_num) {
		DPRINT_API("- allocating %i extra slots.\n",
			   api->id + 1 - stem->_api_num);
		if (stem->_api_slots)
			slot = realloc(stem->_api_slots,
				       sizeof(*slot)*(api->id+1));
		else
			slot = malloc(sizeof(*slot)*(api->id+1));

		if (slot == NULL) {
			DPRINT_API("! could not expand api slots.\n");
			ggExitAPI(api);
			return GGI_ENOMEM;
		}
		memset(slot + stem->_api_num, 0,
		       sizeof(*slot)*(api->id + 1 - stem->_api_num));
		stem->_api_slots = slot;
		stem->_api_num   = api->id + 1;
	}

	slot = stem->_api_slots + api->id;

	if (slot->_attachcount) {
		DPRINT_API("- already attached.\n");
		goto skip;
	}

	DPRINT_API("- attaching for the first time.\n");
	/*
	 * XXX actually we probably want slot->priv instead, but we
	 * don't have it yet and we can't guarantee that it will not
	 * change...
	 */
	if((slot->channel = ggNewChannel(stem, NULL)) == NULL) {
		DPRINT_API("! cannot alloc channel.\n");
		ggExitAPI(api);
		return GGI_ENOMEM;
	}

	slot->_attachcount = 0;
	slot->_order = 0;

	if(api->ops && api->ops->attach) {
		if(api->ops->attach(api, stem)) { /* OUT */
			DPRINT_API("! api attach callback failed.\n");
			ggExitAPI(api);
			return GGI_EUNKNOWN;
		}
	}

	api->state->stemcount++;
	
	msg.api = api;
	msg.stem = stem;

	ggBroadcast(stem->channel, GG_MESSAGE_STEM_ATTACH, &msg);
	ggBroadcast(api->channel, GG_MESSAGE_API_DETACH, &msg);

skip:
	return slot->_attachcount++;
}


int
ggDetach(struct gg_api *api, struct gg_stem *stem)
{
	struct gg_api_slot * slot;
	struct gg_message_api_stem msg;
	
	DPRINT_API("ggDetach(api=%p, stem=%p)\n", api, stem);
	
	if (!STEM_HAS_API(stem, api)) {
		DPRINT_API("! api not attached.\n");
		return GGI_ENOTALLOC;
	}

	slot = stem->_api_slots + api->id;
	
	if(--slot->_attachcount > 0)
		goto skip;
	
	DPRINT_API("- really detaching.\n");
	
	msg.api = api;
	msg.stem = stem;
	ggBroadcast(api->channel, GG_MESSAGE_API_DETACH, &msg);
	ggBroadcast(stem->channel, GG_MESSAGE_STEM_DETACH, &msg);
	/*
	 * If we were very very paranoid we could change that all
	 * observers played fair and didn't change msg
	 */

	if(api->ops && api->ops->detach) {
		api->ops->detach(api, stem); /* OUT */
	}
	
	ggDelChannel(slot->channel);
	slot->channel = NULL;

	api->state->stemcount--;

 skip:
	/* Always release our reference to the api. */
	ggExitAPI(api);
	return slot->_attachcount;
}


static struct gg_stem *
alloc_stem(void)
{
	struct gg_stem *stem;
	
	if ((stem = calloc(1, sizeof(*stem) + realm->_stem_alloc)) == NULL) {
		DPRINT_API("! could not alloc stem\n");
		return NULL;
	}
	
	if ((stem->channel = ggNewChannel(stem, NULL)) == NULL) {
		DPRINT_API("! cannot create channel.\n");
		free(stem);
	}
	
	stem->_realm = realm;
	if(realm->_stem_init) {
		if(realm->_stem_init(stem)) { /* OUT */
			DPRINT_API("! init callback failed.\n");
			ggDelChannel(stem->channel);
			free(stem);
			return NULL;
		}
	}
	
	APPEND_STEM(realm, stem);
	return stem;
}


struct gg_stem *
ggNewStem(struct gg_api *api, ...)
{
       va_list apis;
       struct gg_stem *stem;
       int n;

       DPRINT_API("ggNewStem(...)\n");
       
       stem = alloc_stem();
       if (!stem)
               return NULL;

       for (n = 0, va_start(apis, api); api; api = va_arg(apis, void *)) {
	       if (ggInitAPI(api) < 0)
		       break;
#if 0 /* we have only one realm. */
	       if (api->_realm == stem->_realm)
#endif
		       if (STEM_HAS_APIID(stem, api->id)) {
			       /* 
				* The API is already registered and
				* attached on the stem, either because
				* it is found multiple times in the
				* va_list, or because it was
				* indirectly attached by another API.
				* In the former case, we simply ignore
				* it.
			       */
			       if((stem->_api_slots + api->id)->_order)
				       continue;
		       }
               if (ggAttach(api, stem) < 0) {
		       ggExitAPI(api);
		       break;
               }
	       /* needed in ggDelStem. */
	       (stem->_api_slots + api->id)->_order = ++n;
       }
       va_end(apis);

       if (!api)
               return stem;

       /* An error happened, cleanup */
       ggDelStem(stem);
       return NULL;
}


void
ggDelStem(struct gg_stem *stem)
{
	struct gg_api *api;
	struct gg_api_slot *slot;
	int i, latest;

	DPRINT_API("ggDelStem(stem=%p)\n", stem);
		
	LIB_ASSERT(stem != NULL, "invalid stem\n");

	/*

	Here we must correctly detach all APIs still attached on this
	stem.  If the user or any other lib called ggAttach, we can
	consider that they are also responsible for calling ggDetach
	before deleting the stem.  So we assume that all APIs still
	attached on this stem were attached in ggNewStem(), directly
	or indirectly.

	So the cleanup strategy is to detach the APIs that were given
	to ggNewStem in the reverse order.  If they internally
	attached other APIs, then detaching them should (provided that
	they were written correctly) mechanically detach the indirect
	ones, in the right order.
	
	*/
	
	/*  find the latest API attached by ggNewStem  */
	latest = -1;
	for(i = 0; i < stem->_api_num; i++)
		if (STEM_HAS_APIID(stem, i)) {
			slot = stem->_api_slots + i;
			if (slot->_order && slot->_order > latest)
				latest = slot->_order;
		}
	
	/*  now detach all APIs in the reverse order  */
	while(latest > 0) {
		for(i = 0; i < stem->_api_num; i++) {
			if (STEM_HAS_APIID(stem, i)) {
				slot = stem->_api_slots + i;
				if (slot->_order == latest) {
					api = ggGetAPIByID(i);
					ggDetach(api, stem);
					ggExitAPI(api);
					goto next;
				}
			}
		}
		DPRINT_API("! no API found with order \"%i\".\n", latest);
	next:
		latest--;
	}
	
	for(i = 0; i < stem->_api_num; i++) {
		if (STEM_HAS_APIID(stem, i)) {
			api = ggGetAPIByID(i);
			DPRINT_API("! api still attached: %s (%i bounds left)\n", api->name, i);
		}
	}
	
	/*
	 * At this point, no observer should be left, at least if they
	 * were installed by an api.
	 */
	ggDelChannel(stem->channel);
	stem->channel = NULL;

	/* No API can be attached to a deleted stem, so remove teh slots
	 */
	free(stem->_api_slots);
	
	REMOVE_STEM(realm, stem);

	if(realm->_stem_finalize)
		realm->_stem_finalize(stem); /* OUT */
	
	free(stem);
}

#if 0
void
ggDumpStem(struct gg_api *api, struct gg_stem *stem)
{
	int i;
	
	DPRINT_API("ggDumpStem(api=%p, stem=%p)\n", (void*)api, (void*)stem);
	if(api) {
		if(!STEM_HAS_API(stem, api)) {
			DPRINT_API("! api \"%s\" not attached to stem.\n",
				   api->name);
			return;
		}
		if(api->ops == NULL || api->ops->dump == NULL) {
			DPRINT_API("! api \"%s\" does not have a dump call.\n",
				   api->name);
			return;
		}
		api->ops->dump(api, stem);
		return;
	}
	
	if(stem) {
		printf("stem %p, realm=%p\n",(void*)stem,(void*)stem->_realm);
		for(i = 0; i < stem->_api_num; i++) {
			if (!stem->_api_slots[i]._attachcount)
				continue;
			FOREACH_API(stem->_realm, api)
				if(api->id == i)
					break;
			printf("   api \"%s\" id=%i, attached=%i, data=%p\n",
			       api->name, i, stem->_api_slots[i]._attachcount,
			       stem->_api_slots[i]._priv);
		}
		return;
	}
}
#endif

const char *
ggGetEnv(struct gg_api * api, const char *var)
{
	DPRINT_API("ggGetEnv(api=%p, var=\"%s\")\n", api, var);
	
	if (api->ops == NULL || api->ops->getenv == NULL) {
		DPRINT_API("! api \"%s\" has no env.\n", api->name);
		return NULL;
	}
	return api->ops->getenv(api, var);
}


struct gg_module *
ggOpenModule(struct gg_api * api, const char *name)
{
	struct gg_scope *scope;
	struct gg_module *module;
	struct gg_module **modules;
	struct gg_location_iter location;
	ggfunc_dlentry *dlentry;
	void *pass;
	const char * symbol;
	
	DPRINT_API("ggOpenModule(api=%p, name=\"%s\")\n", api, name);
	
	symbol = ggGetEnv(api, "DLSYMBOL");
	module = NULL;
	location.name = name;
	location.config = api->config;
	ggConfigIterLocation(&location);
	GG_ITER_FOREACH(&location) {

		module = NULL;
		
		DPRINT("- trying location \"%s:%s\"\n",
		       location.location, location.symbol);
		if ((scope = ggGetScope(location.location)) == NULL) {
			DPRINT("! cannot open location \"%s\".\n",
			       location.location);
			continue;
		}
		
		if (location.symbol == NULL) {
			if (!symbol) {
				DPRINT("! no default symbol.\n");
				continue;
			}
			location.symbol = symbol;
		}

		dlentry = ggFromScope(scope, location.symbol);
		if (dlentry == NULL) {
			DPRINT("! symbol \"%s\" not found.\n",
			       location.symbol);
			ggDelScope(scope);
			continue;
		}

		pass = &modules;
		if (dlentry(GG_DLENTRY_MODULES, pass) < 0) {
			DPRINT("! location \"%s\" has no module\n",
			       location.symbol);
			ggDelScope(scope);
			continue;
		}

		for(;;) {
			module = *modules;
			if (module == NULL)
				break;
			if(!strcmp(module->name, name))
				break;
			modules++;
		}

		if (module == NULL) {
			DPRINT("! module \"%s\" not found.\n", name);
			ggDelScope(scope);
			continue;
		}
		
		/*
		 * XXX do we allow NULL here? maybe the module must
		 * always set the api correctly.
		 */
		if (module->api == NULL)
			module->api = api;
		
		if (module->api != api) {
			DPRINT("! API mismatch.\n");
			ggDelScope(scope);
			continue;
		}
		
		if (module->state == NULL) {
			/* first time */
			module->state = calloc(1, sizeof(*module->state));
			if(module->state == NULL) {
				DPRINT("! cannot alloc mem");
				break;
			}
			module->state->scope = scope;
			module->state->usecount = 1;
			GG_LIST_INSERT_HEAD(&api->state->modules,
					    module,
					    state->modules);
		} else {
			LIB_ASSERT(module->state->scope == scope,
				   "! scope mismatch");
			module->state->usecount++;
		}
		break;
	}

	GG_ITER_DONE(&location);

	return module;
}


void
ggCloseModule(struct gg_module *module)
{
	struct gg_scope *scope;
	
	module->state->usecount--;
	if(module->state->usecount == 0) {
		scope = module->state->scope;
		GG_LIST_REMOVE(module, state->modules);
		free(module->state);
		module->state = NULL;
		ggDelScope(scope);
	}
}


struct gg_plugin *
ggPlugModule(struct gg_api *api,
	     struct gg_stem *stem,
	     const char *name,
	     const char *argstr,
	     void* argptr)
{
	struct gg_module *module;
	struct gg_plugin *plugin;
	int res;
	
	if(api->ops == NULL ||
	   api->ops->plug == NULL ||
	   api->ops->unplug == NULL) {
		DPRINT_API("! api \"%s\" can not open modules.\n",
			   api->name);
		return NULL;
	}
	
	module = ggOpenModule(api, name);
	if(module == NULL)
		return NULL;
	
	res = api->ops->plug(module->api,
			     module,
			     stem,
			     argstr,
			     argptr,
			     &plugin);
	if(res) {
		ggCloseModule(module);
		return NULL;
	}
	
	LIB_ASSERT(plugin->module == module, "! module mismatch.\n");
	module->state->usecount++;
	
	return plugin;
}

void
ggClosePlugin(struct gg_plugin *plugin)
{
	struct gg_module * module = plugin->module;
	struct gg_api * api = module->api;
	api->ops->unplug(api, plugin);
	module->state->usecount--;
	ggCloseModule(module);
}
