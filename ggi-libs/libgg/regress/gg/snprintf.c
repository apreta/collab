/* $Id: snprintf.c,v 1.1 2009/12/03 11:48:15 pekberg Exp $
******************************************************************************

   snprintf - Regression tests for snprintf

   Copyright (C) 2009 by Peter Rosin

   This software is placed in the public domain and can be used freely
   for any purpose. It comes without any kind of warranty, either
   expressed or implied, including, but not limited to the implied
   warranties of merchantability or fitness for a particular purpose.
   Use it at your own risk. the author is not responsible for any damage
   or consequences raised by use or inability to use this program.

******************************************************************************
*/

#include "config.h"

#include <ggi/gg.h>
#include <ggi/internal/gg_replace.h>

#include "../testsuite.inc.c"


static void
testcase1(const char *desc)
{
	int res;
	char buf[100];
	void *ptr;

	printteststart(__FILE__, __PRETTY_FUNCTION__, EXPECTED2PASS, desc);
	if (dontrun) return;

	snprintf(buf, sizeof(buf), "%p", buf);
	res = sscanf(buf, "%p", &ptr);
	if (res != 1) {
		printfailure("expected return value: 1\n"
			     "actual return value: %d\n", res);
		return;
	}
	if (ptr != buf) {
		printfailure("expected ptr value: %p\n"
			     "actual ptr value: %p\n",
			     buf, ptr);
		return;
	}

	printsuccess();
}


int
main(int argc, char * const argv[])
{
	parseopts(argc, argv);

	printdesc("Regression testsuite for libgg snprintf\n\n");

	testcase1("Check that sprintf %p can be parsed by sscanf.");

	printsummary();

	return 0;
}
