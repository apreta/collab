#include "config.h"
#include <ggi/gg.h>
#include <ggi/gg-api.h>

/*
 * API Example: The Listen API
 */


/*
 *  API specific data that will be found on a stem implementing the
 *  listen API
 */
struct listen_data {
	const char * preferred;
};

/*
 *  The macro to retrieve that structure on a stem.
 */
#define LISTEN_DATA(stem) STEM_API_DATA(stem, (&listen_), struct listen_data*)
#define LISTEN_PRIV(stem) STEM_API_PRIV(stem, (&listen_))

static int  listen_attach(struct gg_api *, struct gg_stem *);
static void listen_detach(struct gg_api *, struct gg_stem *);

static struct gg_api_ops listen_ops = {
	NULL,
	NULL, /* exit */
	listen_attach,
	listen_detach,
	NULL, /* getenv */
	NULL, /* plug */
	NULL /* unplug */
};

static struct gg_api listen_ = GG_API_INIT("listen", 0, 0, &listen_ops);

/*
 * Called the first time the listen API is attached on the stem
 */
static int
listen_attach(struct gg_api *api, struct gg_stem *stem)
{
	printf("I start listening to music.\n");
	if((LISTEN_PRIV(stem) = malloc(sizeof(struct listen_data))) == NULL)
		return GGI_ENOMEM;
	LISTEN_DATA(stem)->preferred = "New Model Army";
	return GGI_OK;
}


/*
 *  Called when the API is really detached from this stem
 */
static void
listen_detach(struct gg_api *api, struct gg_stem *stem)
{
	free(LISTEN_PRIV(stem));
	printf("I don't listen to music anymore.\n");
}


/*
 * Part of the public listen API.
 */
static void
listenMusic(struct gg_stem *stem, const char * music)
{
	if (music == NULL)
		music = LISTEN_DATA(stem)->preferred;
	printf("I listen to %s.\n", music);
}

/*
 * Part of the public listen API.
 */
static void
changeTaste(struct gg_stem *stem, const char *new)
{
	printf("I don't like %s anymore... I prefer %s from now.\n",
	       LISTEN_DATA(stem)->preferred,
	       new);
	LISTEN_DATA(stem)->preferred = new;
}


static int
_apiChange(void *arg, uint32_t msg, void *data)
{
	struct gg_message_api_stem *change;
	
	change = (struct gg_message_api_stem*)data;
	
	printf("API CHANGE: api \"%s\" on stem %p code %"PRIu32"\n",
	       change->api->name,
	       (void *)change->stem,
	       msg);
	return 0;
}


int main(int ac, const char *av[])
{
	struct gg_stem *o1;
	struct gg_observer *obs;
	
	o1 = ggNewStem(NULL);

	printf("-------------------------\n");

	obs = ggObserve(o1->channel, &_apiChange, NULL);
	
	ggAttach(&listen_, o1);
	listenMusic(o1, "Bauhaus");
	listenMusic(o1, NULL);
	changeTaste(o1, "Front Line Assembly");
	listenMusic(o1, NULL);
	ggDetach(&listen_, o1);
	
	ggDelObserver(obs);
	ggDelStem(o1);
	
	printf("-------------------------\n");
	
	o1 = ggNewStem(&listen_, NULL);
	changeTaste(o1, "Nick Cave");
	ggDelStem(o1);
	
	printf("-------------------------\n");

	o1 = ggNewStem(&listen_, NULL);
	ggAttach(&listen_, o1);
	changeTaste(o1, "Nick Cave");
	/*  should complain about listen remaining */
	ggDelStem(o1);
	
	printf("-------------------------\n");

	return 0;
}
