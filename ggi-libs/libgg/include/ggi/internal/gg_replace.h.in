/* $Id: gg_replace.h.in,v 1.4 2008/12/12 03:48:12 pekberg Exp $
******************************************************************************

   LibGG replacements for missing or broken library functions.
 
   Copyright (C) 2004, 2005 Peter Rosin		[peda@lysator.liu.se]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#ifndef _GGI_INTERNAL_GG_REPLACE_H
#define _GGI_INTERNAL_GG_REPLACE_H

#include <ggi/gg-defs.h>


#ifndef intptr_t
#define intptr_t	@INTPTR_T@
#endif

#ifndef uintptr_t
#define uintptr_t	@UINTPTR_T@
#endif


@REPLACE_STRTOUL@

#ifdef _GG_REPLACE_STRTOUL
#define strtoul _gg_strtoul
GGAPIFUNC unsigned long
strtoul(const char *nptr, char **endptr, int base);
#endif

@REPLACE_MEMCMP@

#ifdef _GG_REPLACE_MEMCMP
#define memcmp _gg_memcmp
GGAPIFUNC int
memcmp(const void *s1, const void *s2, size_t n);
#endif

@REPLACE_GETOPT@

#ifdef _GG_REPLACE_GETOPT
#define getopt   _gg_getopt
#define opterr   _gg_opterr
#define optind   _gg_optind
#define optopt   _gg_optopt
#define optreset _gg_optreset
#define optarg   _gg_optarg
GGAPIFUNC int
getopt(int nargc, char * const nargv[], const char *ostr);
GGAPIVAR int opterr;
GGAPIVAR int optind;
GGAPIVAR int optopt;
GGAPIVAR int optreset;
GGAPIVAR char *optarg;
#endif

@REPLACE_SNPRINTF@

#ifdef _GG_REPLACE_SNPRINTF
#define snprintf _gg_snprintf
GGAPIFUNC int
snprintf(char *str, size_t count, const char *fmt, ...);
#endif

@REPLACE_VSNPRINTF@

#ifdef _GG_REPLACE_VSNPRINTF
#include <stdarg.h>	/* for va_list */

#define vsnprintf _gg_vsnprintf
GGAPIFUNC int
vsnprintf(char *str, size_t count, const char *fmt, va_list args);
#endif

@REPLACE_GAI_STRERROR@

#ifdef _GG_REPLACE_GAI_STRERROR
#ifdef gai_strerror
#undef gai_strerror
#endif
#define gai_strerror _gg_gai_strerror
GGAPIFUNC const char *
gai_strerror(int error);
#endif

@REPLACE_STRLCPY@
@REPLACE_STRLCAT@

@REPLACE_STRCASECMP@
@REPLACE_STRNCASECMP@

@REPLACE_POPEN@
@REPLACE_PCLOSE@

@REPLACE_SHM@

#ifdef _GG_REPLACE_SHM
#define _GG_HAVE_SHM
#define IPC_CREAT (1<<0)
#define IPC_RMID  (1<<1)
typedef const char *key_t;
#define shmget _gg_shmget
#define shmat  _gg_shmat
#define shmdt  _gg_shmdt
#define shmctl _gg_shmctl
#define ftok   _gg_ftok
GGAPIFUNC int   shmget(key_t key, int size, int shmflg);
GGAPIFUNC void *shmat(int shmid, const void *shmaddr, int shmflg);
GGAPIFUNC int   shmdt(const void *shmaddr);
GGAPIFUNC int   shmctl(int shmid, int cmd, void *buf);
GGAPIFUNC key_t ftok(const char *pathname, int proj_id);
#endif /* _GG_REPLACE_SHM */

#endif /* _GGI_INTERNAL_GG_REPLACE_H */
