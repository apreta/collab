/* $Id: gg-api.h,v 1.2 2008/03/05 10:49:32 pekberg Exp $
***************************************************************************

   LibGG - API infrastructure header file

   Copyright (C) 2005  Eric Faurot [eric.faurot@gmail.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

***************************************************************************
*/

#ifndef _GGI_INTERNAL_GG_API_H
#define _GGI_INTERNAL_GG_API_H

#include <ggi/gg-api.h>

/*
 * This structure is used internally by libgg to maintain the state of
 * an API. The idea is to be able to change the internal API
 * management while remaining binary compatible with existing APIs.
 * This structure is currently private.
 */
struct gg_api_state {
	int initcount;
	int stemcount;
	GG_LIST_HEAD(_gg_module_dummy, gg_module) modules;
	GG_LIST_ENTRY(gg_api) _others;
};

/*
 * This is managed internally by libgg as it keep tracks of
 * opened modules. It is set to NULL when the module is not
 * opened.  It allows to change internal module handling
 * without changing the size of the gg_module structure in
 * external code.
 */
struct gg_module_state {
	
	/* set by libgg when it opens the module for the first time. */
	int usecount;
	
	/*
	 * A module is found in a scope. This is set by libgg when 
	 */
	struct gg_scope *scope;
	/* other opened modules in this api */
	GG_LIST_ENTRY(gg_module) modules;
};


/*
************************************************************************
**  Realms
************************************************************************
*/


/*
**  A realm is a central authority managing APIs and stems.  Like a
**  stem, an API can only be bound to one realm.  The idea was to
**  allow different APIs to be belong to different realms, so that
**  they become completely orthogonal.  This would also allow a realm
**  to use a specific allocator for extended stems with specific
**  application data or "statically" attached APIs.  Currently this is
**  completely transparent for the user and api writers. There is only
**  one realm internally.
*/

typedef int  (ggfunc_stem_init)(struct gg_stem*);
typedef void (ggfunc_stem_finalize)(struct gg_stem*);

struct gg_realm {
	
	/*  [ ---------- HIDDEN ---------- ]  */
	
	/*  Monotonic API ids. */
	int _api_ids;
	
	/*  Size required for stem. */
	unsigned int          _stem_alloc;
	ggfunc_stem_init     *_stem_init;
	ggfunc_stem_finalize *_stem_finalize;
	
	/*
	**  Not yet clear when that is used.
	*/
	void                  * lock;
	
	GG_LIST_HEAD(gg_stem_list, gg_stem) _stems;
	GG_LIST_HEAD(gg_api_list,  gg_api)  _apis;
};

#define GG_REALM_INITIALIZER(al, in, fi) {0,al,in,fi,NULL,{ NULL },{ NULL }}


#endif /* _GGI_INTERNAL_GG_API_H */
