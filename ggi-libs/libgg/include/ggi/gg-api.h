/* $Id: gg-api.h,v 1.18 2007/06/24 13:35:46 aldot Exp $
***************************************************************************

   LibGG - API infrastructure header file

   Copyright (C) 2005  Eric Faurot [eric.faurot@gmail.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

***************************************************************************
*/

#ifndef _GGI_GG_API_H
#define _GGI_GG_API_H

#include <ggi/gg.h>
#include <ggi/gg-queue.h>

struct gg_stem;
struct gg_api;
struct gg_realm;


struct gg_version {
	uint32_t major;
	uint32_t minor;
};


/*
 * Notification of a change on the stem at the api level.
 * Anybody can know when an API is attached/detached from a stem.
 */
struct gg_message_api_stem {
	struct gg_api *api;
	struct gg_stem *stem;
};

/*
************************************************************************
*  Stem
************************************************************************
*/

/*
 * This structure holds the relevant information regarding the link
 * between an API and a stem.
 */
struct gg_api_slot {
	/*
	 * XXX check if it is used...
	 */
	struct gg_channel *channel;
	
	/*
	 * If non-zero, it means that the API was attached implicitly
	 * when the stem was allocated.  The value is the its rank.
	 */
	int _order;
	
	/*
	 * Tell if this slot is used. This correspond to the number
	 * of times ggAttachAPI(thisstem, thisapi) as been called.
	 * If an API requires another to
	 * be present on a stem to operate correctly, it must
	 * call ggAttachAPI to increment this counter and prevent
	 * the API fom being detached too early.
	 * Of course, it must call ggDetachAPI as soon as the API is
	 * not required anymore.
	 */
	int _attachcount;
	
	/*
	 * Private data added to the stem by the API. 
	 */
	void *_priv;
	
	/*
	 * List of modules bound to this stem for that API
	 */
	GG_LIST_HEAD(gg_module_list,  gg_module_bind)  _modules;
};

#define GG_STEM_API_CHANNEL(s, a) ((s)->_api_slots[(a)->id].channel)

/*
 * This could also be gg_monad, gg_atom, gg_particle, gg_context.  It
 * is an empty object on which apis are attached to give it a specific
 * behavior. APIs attached on the same can "cooperate".
 */
struct gg_stem {

#define GG_MESSAGE_STEM_DELETE 0xff000000 /* stem is going to be deleted */
#define GG_MESSAGE_STEM_ATTACH 0xff000001 /* an api has been attached */
#define GG_MESSAGE_STEM_DETACH 0xff000002 /* an api is going to be detached */

#define GG_CONTROL_STEM_DUMP   0xff000000 /* tell a stem to write its state */

	struct gg_channel *channel;
	
	/*
	 * XXX remove this if it is not used.
	 */
	void *lock;
	
	/*
	 * Array indexed by API id. This is should be considered as
	 * hidden to all. API implementations should use the provided
	 * macros to access API data.
	 */
	struct gg_api_slot *_api_slots;
	/* Size of the array       */
	int _api_num;

	/*
	 *
	 */
	struct gg_realm *_realm;
	GG_LIST_ENTRY(gg_stem) _others;
};


/*
 *  Macros to access API data on a stem.
 */

#define STEM_HAS_APIID(stem, id)  \
       (stem->_api_num>id && stem->_api_slots[id]._attachcount != 0)
#define STEM_APIID_DATA(stem, id, cast) ((cast)(stem->_api_slots[id]._priv))

#define STEM_HAS_API(stem, api)         STEM_HAS_APIID(stem, api->id)
#define STEM_API_DATA(stem, api, cast)  STEM_APIID_DATA(stem, api->id, cast)

#define STEM_API_PRIV(stem, api)        (stem->_api_slots[api->id]._priv)

/*
************************************************************************
**  Modules
************************************************************************
*/


struct gg_module_state;

/*
 * A module is a pluggable piece of code.
 */
struct gg_module {
	/* 
	 * Canonical name of this module.  This is well-known
	 * contractual name which is used to strictly identify the
	 * module.
	 */
	const char *name;
	
	struct gg_version version;
	
	/* 
	 * An API will define several classes of module to be used in
	 * different context.  This value can be used to typecast
	 * gg_module and gg_plugin into more specialzed structures.
	 * It is also used by the internal API implementation to known
	 * what to do when a new instance is created.
	 */
	int klass;
	
	/*
	 * Must be set to the API that will handle this module.  XXX:
	 * can it be NULL, and set when the module is initialized?
	 */
	struct gg_api *api;

	/*
	 * This is managed internally by libgg as it keep tracks of
	 * opened modules. It is set to NULL when the module is not
	 * opened.  It allows to change internal module handling
	 * without changing the size of the gg_module structure in
	 * external code.
	 */
	struct gg_module_state *state;

};


#define GG_MODULE_INIT(name, major, minor, cls) \
              { name, { major, minor }, cls, NULL, NULL }

/*
 * This structure represents an abstract module instance, created by
 * an API and installed on a stem.  A gg_plugin structure is
 * allocated by libgg based on the , but always subclassed (in the C
 * sense). APIs define their own specific instantatable structures
 * that must wrap this one.
 */

#define gg_instance gg_plugin
struct gg_plugin {
	struct gg_module *module;	
	struct gg_channel *channel;
	struct gg_stem *stem;
};

/*
************************************************************************
**  APIs
************************************************************************
*/

typedef int  (ggfunc_api_op_init)(struct gg_api *);
typedef void (ggfunc_api_op_exit)(struct gg_api *);
typedef int  (ggfunc_api_op_attach)(struct gg_api *, struct gg_stem *);
typedef void (ggfunc_api_op_detach)(struct gg_api *, struct gg_stem *);

typedef const char* (ggfunc_api_op_getenv)(struct gg_api *, const char *);

typedef int (ggfunc_api_op_plug)(struct gg_api *,
				 struct gg_module *,
				 struct gg_stem *,
				 const char *,
				 void *,
				 struct gg_plugin **);

typedef int (ggfunc_api_op_unplug)(struct gg_api *,
				   struct gg_plugin *);

/*
 * This structure defines the interface that an API implements.
 */
struct gg_api_ops {
	
	/*
	 * Initialize the API the first time it is used.
	 * Non-zero return values are considered an error,
	 * so the API will not be installed on the realm.
	 * In this case, the exit function will not be called,
	 * so all cleanup must be done in init before returning
	 * the error.
	 */
	ggfunc_api_op_init *init;
	
	/* 
	 * Shut down the API infrastructure. When this function is
	 * called, implementation guarantees the API will not be
	 * attached to any stem, so only internal clean up is
	 * required.
	 */
	ggfunc_api_op_exit *exit;
	
	/* 
	 * Attach the API to a stem. Return GGI_OK on success or an
	 * error code.  This handler is responsible for allocating the
	 * private data structure accessible via STEM_API_PRIV, that
	 * will contain the state of this stem for this API.
	 */
	ggfunc_api_op_attach *attach;
	
	/*
	 * Detach the API from a stem. Implementation guarantees
	 * that the API is actually attached. The handler is
	 * responsible for releasing resources, freeing memory
	 * used in STEM_API_PRIV.
	 */
	ggfunc_api_op_detach *detach;
	
	/*
	 * Function to retrieve api-specific environnement variables.
	 */
	ggfunc_api_op_getenv *getenv;
	
	/*
	 * 
	 * expected to try to open the requested module by calling
	 * the spec.init function that has been located by libgg
	 * according to the API configuration. If the call succeeds,
	 * it must keep a reference to spec.scope to unref it later
	 * when the module is closed.
	 */
	ggfunc_api_op_plug *plug;
	
	/*
	 * Function used to close a plugin.  It must to free
	 * the gg_module.
	 */
	ggfunc_api_op_unplug *unplug;
};

struct gg_api_state;

/*
 *  This defines an api that can be attached to a stem.
 *  It could also be called gg_domain, gg_interface...
 */
struct gg_api {
	
	/* 
	 * Id of this API in the realm.  This id is given by the realm
	 * at registration time.  Anybody can read it but it must not
	 * be changed. This id is used as an offset for storing
	 * API-specific data on any structure that needs this (stems
	 * for example). Ids are monotonic.
	 */
	int id;
	
	/*
	 * Canonical name for this API.  This is the name by which API
	 * can recognize each others, and it may also be used in
	 * configuration files.
	 */
	const char *name;
	
	/*
	 * The version number of the api.
	 */
	struct gg_version version;
	
	/*
	 * This is the channel on which API level events are notified.
	 */
#define GG_MESSAGE_API_ATTACH 0xff000000
#define GG_MESSAGE_API_DETACH 0xff000001
#define GG_MESSAGE_API_EXIT   0xff000002

#define GG_CONTROL_API_DUMP   0xff000000

	struct gg_channel *channel;
	
        /*
	 * Standard api ops (see above). These ops must be filled
	 * by the implementation code after registration succeeded,
	 * and before being attached to any stem.
	 */
	struct gg_api_ops *ops; 
	
	/*
	 * Debugging flags (avoid exporting the variable)
	 */
	uint32_t debug;
	
	/*
	 * Configuration handle.
	 */
	struct gg_config *config;

	/*
	 * Libgg internal
	 */
	struct gg_api_state *state;
};


#define GG_API_INIT(name, major, minor, ops) {           \
	-1,                                              \
	name,                                            \
	{ major, minor },                                \
        NULL,              /* channel */                 \
        ops,                                             \
	0,                                               \
        NULL,                                            \
	NULL                                             \
}

/*
************************************************************************
**  API management functions
************************************************************************
*/

__BEGIN_DECLS

GGAPIFUNC int ggInitAPI(struct gg_api *);

GGAPIFUNC int ggExitAPI(struct gg_api *);

GGAPIFUNC struct gg_api* ggGetAPIByName(const char *);

GGAPIFUNC struct gg_stem* ggNewStem(struct gg_api *, ...);

GGAPIFUNC void ggDelStem(struct gg_stem *);

GGAPIFUNC int ggAttach(struct gg_api *, struct gg_stem *);

GGAPIFUNC int ggDetach(struct gg_api *, struct gg_stem *);

GGAPIFUNC struct gg_module *ggOpenModule(struct gg_api *, const char*);

GGAPIFUNC void ggCloseModule(struct gg_module*);

GGAPIFUNC struct gg_plugin *ggPlugModule(struct gg_api *api,
					 struct gg_stem *stem,
					 const char *name,
					 const char *argstr,
					 void* argptr);

GGAPIFUNC void ggClosePlugin(struct gg_plugin *);

GGAPIFUNC const char *ggGetEnv(struct gg_api *, const char *);

__END_DECLS

#endif /* _GGI_GG_API_H */
