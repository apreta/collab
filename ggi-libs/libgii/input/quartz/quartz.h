/* $Id: quartz.h,v 1.8 2007/02/15 18:47:11 cegger Exp $
******************************************************************************

   Quartz: Input header

   Copyright (C) 2004 Christoph Egger

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/


#include <ApplicationServices/ApplicationServices.h>
#include <Carbon/Carbon.h>


enum quartz_devtype {
	QZ_DEV_KEY = 0,
	QZ_DEV_MOUSE,

	QZ_DEV_MAX
};

typedef struct {
	WindowRef theWindow;
	/* Current modifier state. */
	uint32_t modifiers;
	/* Previous modifier state. Simplifies
	 * detection of what actually happened. */
	uint32_t prev_modifiers;
	uint32_t origin[QZ_DEV_MAX];

	EventHandlerRef windowHandler;
	EventHandlerRef applicationHandler;

	uint32_t titleheight;
	struct gg_publisher *publisher;
	struct gg_observer *observer;
	uint32_t nerve;
} quartz_priv;

#define QUARTZ_PRIV(inp)  ((quartz_priv *)inp->priv)


int QuartzInit(quartz_priv *priv);
int QuartzFinishLaunch(struct gii_source *src);
int QuartzExit(quartz_priv *priv);

int GII_quartz_seteventmask(struct gii_source *src, gii_event_mask evm);
int GII_quartz_geteventmask(struct gii_source *src);
int GII_quartz_eventpoll(struct gii_source *src);

int GII_quartz_getselectfdset(struct gii_source *src, fd_set *readfds);

int QuartzUninitEventHandler(struct gii_source *src);
int QuartzInitEventHandler(struct gii_source *src);

OSStatus DefaultWindowEventHandler(EventHandlerCallRef nextHandler,
		EventRef event, void *userData);

OSStatus DefaultApplicationEventHandler(EventHandlerCallRef nextHandler,
		EventRef event, void *userData);
