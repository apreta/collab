/* $Id: input.c,v 1.23 2008/10/29 22:21:41 pekberg Exp $
******************************************************************************

   Quartz: Input driver

   Copyright (C) 2004 Christoph Egger

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/gii-module.h>
#include <ggi/gii-events.h>
#include <ggi/internal/gii_debug.h>


#include <ggi/input/quartz.h>
#include "quartz.h"


static struct gii_cmddata_devinfo mouse_devinfo =
{
	"Quartz Mouse",				/* long name */
	GII_VENDOR_GGI_PROJECT,
	GII_VENDOR_GGI_PROJECT,
	emPointer,				/* can_generate (targetcan) */
	1,					/* num registers */
	0,					/* num_axes     (only for valuators) */
	3					/* num_buttons  (filled in runtime) */
};

static struct gii_cmddata_devinfo key_devinfo =
{
	"Quartz Keyboard",	/* long name */
	GII_VENDOR_GGI_PROJECT,
	GII_VENDOR_GGI_PROJECT,
	emKey,			/* can_generate (targetcan) */
	1,					/* num registers */
	0,			/* num_axes    (only for valuators) */
	100			/* num_buttons (filled in runtime) */
};



static int update_winparam(struct gii_source *src)
{
	QuartzUninitEventHandler(src);
	QuartzInitEventHandler(src);

	return GGI_OK;
}	/* update_winparam */


static int _gii_quartz_controller(void *arg, uint32_t ctl, void *data)
{
	struct gii_source *src = arg;
	quartz_priv *priv = QUARTZ_PRIV(src);

	if ((ctl & GII_CMDCODE_QZSETPARAM) == GII_CMDCODE_QZSETPARAM) {
		struct gii_quartz_cmddata_setparam param;
		Rect contentRect, winRect;
		DPRINT_MISC("received setparam event\n");

		memcpy(&param, data, sizeof(struct gii_quartz_cmddata_setparam));
		priv->theWindow = param.theWindow;

		GetWindowBounds(priv->theWindow, kWindowContentRgn, &contentRect);
		GetWindowBounds(priv->theWindow, kWindowStructureRgn, &winRect);
		priv->titleheight = contentRect.top - winRect.top;
		DPRINT_MISC("title border height: %i\n", priv->titleheight);

		update_winparam(src);
	}

	if (ctl == GII_CMDCODE_RESIZE) {
		gii_event *ev_ = data;
		gii_event ev;

		DPRINT_MISC("received resize event\n");
		giiEventBlank(&ev, sizeof(gii_cmd_event));
		memcpy(ev.cmd.data, ev_->cmd.data, sizeof(ev.cmd.data));
		ev.any.size = ev_->any.size;
		ev.any.type = ev_->any.type;
		ev.cmd.code = ev_->cmd.code;
		giiPostEvent(src, &ev);
	}

	if (ctl == GII_CMDCODE_INJECT_EVENT) {
		gii_event *ev = data;
		giiEventBlank(ev, 0); /* only set timestamp */
		ev->any.origin = src->origin;
		giiPostEvent(src, ev);
	}

	return 0;
}


static int GII_quartz_send_event(struct gii_source *src, gii_event *ev)
{
	quartz_priv *priv = QUARTZ_PRIV(src);

	if ((ev->any.target & 0xffffff00) != src->origin &&
	    ev->any.target != GII_EV_TARGET_ALL)
	{
		/* not for us */
		return GGI_EEVNOTARGET;
	}

	if (ev->any.type != evCommand) {
		return GGI_EEVUNKNOWN;
	}

	switch (ev->cmd.code) {
	case GII_CMDCODE_QZSETPARAM:
		do {
			int err = GGI_OK;
			struct gii_quartz_cmddata_setparam data;

			/* Assure aligned memory access. */
			memcpy(&data, (struct gii_quartz_cmddata_setparam *)ev->cmd.data,
				sizeof(struct gii_quartz_cmddata_setparam));

			priv->theWindow = data.theWindow;
			err = update_winparam(src);
			return err;
		} while(0);
		break;

	default:
		break;
	}

	return GGI_EEVUNKNOWN;
}	/* GII_quartz_send_event */


static void GII_quartz_close(struct gii_source *src)
{
	quartz_priv *priv = QUARTZ_PRIV(src);

	QuartzExit(priv);
	free(priv);

	DPRINT_LIBS("exit ok.\n");

	return;
}	/* GII_quartz_close */



static int
GII_quartz_init(struct gii_source *src, const char *target,
		const char *args, void *argptr)
{
	int rc = 0;
	gii_inputquartz_arg *quartzarg = argptr;
	quartz_priv *priv;

	DPRINT_MISC("quartz input starting. (args=%s,argptr=%p)\n",
		args, argptr);

	if (quartzarg == NULL) {
		rc = GGI_EARGREQ;
		goto err0;
	}

	priv = calloc(1, sizeof(quartz_priv));
	if (priv == NULL) {
		rc = GGI_ENOMEM;
		goto err0;
	}

	/* Attention: quartzarg->theWindow is invalid at this point.
	 * It becomes valid once a mode has been set up.
	 */
	priv->theWindow = quartzarg->theWindow;
#if 0
	LIB_ASSERT(priv->theWindow != NULL, "quartzarg->theWindow is NULL\n");
#endif


	if (0 == (priv->origin[QZ_DEV_KEY] =
		giiAddDevice(src, &key_devinfo, NULL)))
	{
		rc = GGI_ENODEVICE;
		goto err1;
	}	/* if */

	if (0 == (priv->origin[QZ_DEV_MOUSE] =
		giiAddDevice(src, &mouse_devinfo, NULL)))
	{
		rc = GGI_ENODEVICE;
		goto err1;
	}	/* if */


	if (GGI_OK != QuartzInit(priv)) {
		rc = GGI_ENODEVICE;
		goto err2;
	}


	src->priv = priv;

	src->ops.event = GII_quartz_send_event;
	src->ops.poll = GII_quartz_eventpoll;
	src->ops.close = GII_quartz_close;

	/* Register a tic, as this used to be polled */
	giiSetNerveTick(src, &priv->nerve, 10 /* msec, but ignored */);

	ggSetController(src->instance.channel, _gii_quartz_controller);
	
	if (GGI_OK != QuartzFinishLaunch(src)) {
		rc = GGI_ENODEVICE;
		goto err2;
	}

	DPRINT_MISC("quartz input fully up\n");

	return GGI_OK;

err2:
err1:
	GII_quartz_close(src);
err0:
	return rc;
}	/* GIIdlinit */


struct gii_module_source GII_quartz = {
	GG_MODULE_INIT("input-quartz", 0, 1, GII_MODULE_SOURCE),
	GII_quartz_init
};

static struct gii_module_source *_GIIdl_quartz[] = {
	&GII_quartz,
	NULL
};


EXPORTFUNC
int GIIdl_quartz(int item, void **itemptr);
int GIIdl_quartz(int item, void **itemptr) {
	struct gii_module_source ***modulesptr;
	switch(item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***)itemptr;
		*modulesptr = _GIIdl_quartz;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
