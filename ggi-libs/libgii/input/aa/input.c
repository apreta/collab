/* $Id: input.c,v 1.12 2008/01/18 23:14:37 cegger Exp $
******************************************************************************

   Input-aa: initialization

   Copyright (C) 2006 Peter Rosin      [peda@lysator.liu.se]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"

#include <stdlib.h>
#include <string.h>

#include <ggi/gg.h>
#include <ggi/gii-module.h>
#include <ggi/gii-keyboard.h>
#include <ggi/internal/gii_debug.h>

#include <aalib.h>
#include <ggi/input/aa.h>



struct gii_aa_priv {
	struct aa_context *context;

	int lx, ly, lb;
	unsigned int lastkey, lastkeyticks;
	int haverelease;

	uint32_t origin;
	uint32_t nerve;
};

#define AA_PRIV(inp) ((struct gii_aa_priv *)inp->priv)

/* Multiply screen coordinates by this to get image coordinates */
#define AA_SCRMULT_X	2
#define AA_SCRMULT_Y	2

static int _gii_aa_controller(void *arg, uint32_t flag, void *data)
{
	struct gii_source *src = arg;
	struct gii_aa_cmddata_setparam *setparam;
	struct gii_aa_priv *priv = AA_PRIV(src);

	switch (flag) {
	case GII_CMDCODE_AASETPARAM:
		setparam = (struct gii_aa_cmddata_setparam *) data;
		priv->context = setparam->context;
		break;
	default:
		return GGI_ENOFUNC;
	}

	return GGI_OK;
}


static void add_key_event(struct gii_source *src, unsigned int key,
			  gii_event_mask type)
{
	gii_event giiev;

	giiEventBlank(&giiev, sizeof(gii_key_event));

	if (type == evKeyRelease)
		key &= ~AA_RELEASE;

	switch (key) {
	case AA_UP:
		key = GIIK_Up;
		break;
	case AA_DOWN:
		key = GIIK_Down;
		break;
	case AA_LEFT:
		key = GIIK_Left;
		break;
	case AA_RIGHT:
		key = GIIK_Right;
		break;
	case AA_BACKSPACE:
		key = GIIUC_BackSpace;
		break;
	case AA_ESC:
		key = GIIUC_Escape;
		break;
	default:
#if 0
		key = LATIN_KEY(key);
#endif
		break;
	}

	giiev.any.size = sizeof(gii_key_event);
	giiev.any.type = type;
	giiev.any.origin = src->origin;

	giiev.key.sym = giiev.key.label = giiev.key.button = key;

	giiPostEvent(src, &giiev);
}


static inline gii_event_mask
do_mouse(struct gii_source *src, struct gii_aa_priv *priv)
{
	gii_event_mask ret = 0;
	gii_event giiev;
	int x, y, buttons;

	aa_getmouse(priv->context, &x, &y, &buttons);

	/* Mouse movement is in screen coordinates;
	 * we assume one screen pixel : four image pixels */
	x *= AA_SCRMULT_X;
	y *= AA_SCRMULT_Y;

	if (x != priv->lx || y != priv->ly) {
		giiEventBlank(&giiev, sizeof(gii_pmove_event));
		giiev.any.size = sizeof(gii_pmove_event);
		giiev.any.type = evPtrAbsolute;
		giiev.any.origin = src->origin;

		giiev.pmove.x = x;
		giiev.pmove.y = y;

		ret |= emPtrAbsolute;
		giiPostEvent(src, &giiev);

		priv->lx = x;
		priv->ly = y;
	}

	/* Now check the buttons */
	if (buttons != priv->lb) {
		int change = buttons ^ priv->lb;
		int i;

		for (i = 0; i < 3; i++) {
			int diff = change & (1 << i);

			if (!diff)
				continue;

			giiEventBlank(&giiev, sizeof(gii_pbutton_event));
			giiev.any.size = sizeof(gii_pbutton_event);
			giiev.any.origin = src->origin;

			if ((diff & buttons)) {
				giiev.any.type = evPtrButtonPress;
				ret |= emPtrButtonPress;
			} else {
				giiev.any.type = evPtrButtonRelease;
				ret |= emPtrButtonRelease;
			}
			giiev.pbutton.button = i + 1;

			giiPostEvent(src, &giiev);
		}

		priv->lb = buttons;
	}

	return ret;
}


static int GII_aa_poll(struct gii_source *src)
{
	struct gii_aa_priv *priv = AA_PRIV(src);
	gii_event_mask evmask = 0;
	unsigned int aatype;

	DPRINT_EVENTS("GII_aa_poll started\n");

	if (!priv->context) {
		DPRINT_EVENTS("aa context not valid!\n");
		return 0;
	}

	while ((aatype = aa_getevent(priv->context, 0)) != AA_NONE) {
		DPRINT_EVENTS("got event 0x%x\n", aatype);

		if (aatype == AA_MOUSE) {
			evmask |= do_mouse(src, priv);
		} else if (aatype >= 1 && aatype <= AA_RELEASE) {
			if (priv->lastkey == 0) {
				/* First hit */
				evmask |= emKeyPress;
				add_key_event(src, aatype, evKeyPress);
			} else if (priv->lastkey == aatype) {
				/* Repeated keypress */
				evmask |= emKeyRepeat;
				add_key_event(src, aatype, evKeyRepeat);
			} else {
				if (!priv->haverelease) {
					/* Whoops, different key! We send a
					 * release for the lastkey first.
					 */
					evmask |= emKeyRelease;
					add_key_event(src, priv->lastkey,
						      evKeyRelease);
				}
				evmask |= emKeyPress;
				add_key_event(src, aatype, evKeyPress);
			}
			priv->lastkey = aatype;
		} else if (aatype > AA_RELEASE) {
			/* Release given key.  It should match lastkey, but
			 * if it doesn't, tough luck, we clear it anyway.
			 */
			evmask |= emKeyRelease;
			add_key_event(src, aatype, evKeyRelease);
			priv->lastkey = 0;
			priv->haverelease = 1;
		} else if (aatype == AA_RESIZE) {

			DPRINT("handle resize event\n");

		} else if (aatype == AA_UNKNOWN) {

			DPRINT("unknown event\n");
		}
	}

	if (!priv->haverelease && priv->lastkey) {
		/* No more events. If priv->lastkey != 0, we release that key. */
		evmask |= emKeyRelease;
		add_key_event(src, priv->lastkey, evKeyRelease);
		priv->lastkey = 0;
	}

	return GII_OP_DONE;
}


static void GII_aa_close(struct gii_source *src)
{
	struct gii_aa_priv *priv = AA_PRIV(src);

	free(priv);

	DPRINT("input-aa closed\n");
}


static struct gii_cmddata_devinfo devinfo = {
	"AA Input",		/* long device name */
	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,
	emKey | emPtrAbsolute | emPtrButton,
	1,
	0,			/* all valuators */
	256,			/* buttons */
};


static int
GII_aa_init(struct gii_source *src,
	    const char *target, const char *args, void *argptr)
{
	struct gii_aa_priv *priv;

	DPRINT_LIBS("GIIdl_aa(%p, \"%s\", \"%s\", %p) called\n", src,
		    target, args ? args : "", argptr);

	/* allocate private stuff */
	priv = calloc(1, sizeof(struct gii_aa_priv));
	if (priv == NULL)
		return GGI_ENOMEM;

	priv->origin = giiAddDevice(src, &devinfo, NULL);
	if (priv->origin == 0) {
		free(priv);
		return GGI_ENOMEM;
	}

	src->priv = priv;

	src->ops.poll = GII_aa_poll;
	src->ops.close = GII_aa_close;


	giiSetNerveTick(src, &priv->nerve, 10);

	ggSetController(src->instance.channel, _gii_aa_controller);

	DPRINT_MISC("input-aa fully up\n");

	return 0;
}


struct gii_module_source GII_aa = {
	GG_MODULE_INIT("input-aa", 0, 1, GII_MODULE_SOURCE),
	GII_aa_init
};

static struct gii_module_source *_GIIdl_aa[] = {
	&GII_aa,
	NULL
};


EXPORTFUNC int GIIdl_aa(int item, void **itemptr);
int GIIdl_aa(int item, void **itemptr)
{
	struct gii_module_source ***modulesptr;
	switch (item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***) itemptr;
		*modulesptr = _GIIdl_aa;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
