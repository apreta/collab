/* $Id: input.c,v 1.16 2007/03/13 21:27:24 soyt Exp $
******************************************************************************

   FreeBSD vgl(3) inputlib

   Copyright (C) 2000 Alcove - Nicolas Souchu <nsouch@freebsd.org>

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include <sys/kbio.h>
#include <sys/fbio.h>
#include <vgl.h>

#include "config.h"
#include "input-vgl.h"

static struct gii_cmddata_devinfo devinfo =
{
	"FreeBSD vgl",
	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,
	emAll,				/* can_generate */
	1,
	GII_NUM_UNKNOWN,		/* num_buttons */
	GII_NUM_UNKNOWN		        /* num_axes */
};

#define MAX_INPUTS 64

int
GII_vgl_poll(struct gii_source *src)
{
	int buf[MAX_INPUTS];
	int read_len;
	int ret, i;

	DPRINT_EVENTS("GII_vgl_poll(%p) called\n", src);

	read_len = 0;
	while (read_len < MAX_INPUTS) {
		if (!(buf[read_len] = VGLKeyboardGetCh()))
			break;
		read_len ++;
	}

	/* Dispatch all keys and return queued events. */
	ret = 0;
	for (i = 0; i < read_len; i++) {
		ret |= GII_vgl_key2event(src, buf[i]);
	}

	return GII_OP_DONE;
}

static void
GIIclose(struct gii_source *src)
{
	gii_vgl_priv *priv = VGL_PRIV(src);

	VGLKeyboardEnd();
	free(priv);

	DPRINT_LIBS("FreeBSD vgl closing.\n");
}


static int
GII_vgl_init(struct gii_source *src,
	     const char * target,
	     const char *args,
	     void *argptr)
{
	gii_vgl_priv *priv;
	int error = 0;

	DPRINT_LIBS("GIIdl_vgl(%p, \"%s\", \"%s\", %p) called\n", src,
		    target,
		    args ? args : "",
		    argptr);
	
	/* XXX Only on per link */
	VGLKeyboardInit(VGL_CODEKEYS);

	priv = malloc(sizeof(*priv));
	if (priv == NULL) {
		error = GGI_ENOMEM;
		goto error;
	}
	memset((void *)priv, 0, sizeof(*priv));

	if((priv->origin = giiAddDevice(src,&devinfo,NULL))==0) {
		free(priv);
		error = GGI_ENOMEM;
		goto error;
	}


	/* Retrieve the current keymap */
	if (ioctl(0, GIO_KEYMAP, &priv->kbd_keymap) < 0) {
		free(priv);
		error = GGI_ENODEVICE;
		goto error;
	}

	if (ioctl(0, GIO_DEADKEYMAP, &priv->kbd_accentmap) < 0)
		memset(&priv->kbd_accentmap, 0, sizeof(priv->kbd_accentmap));

	src->ops.close = GIIclose;
	src->ops.poll = GII_vgl_poll;
	
	giiSetNerveTick(src, &priv->nerve, 10);
	
	DPRINT_LIBS("FreeBSD vgl up.\n");

	return 0;

error:
	VGLKeyboardEnd();
	return error;
}


static struct gii_module_source GII_vgl = {
	GG_MODULE_INIT("input-vgl", 0, 1, GII_MODULE_SOURCE),
	GII_vgl_init
};

static struct gii_module_source *GIIdl_vgl[] = {
	&GII_vgl,
	NULL
};


EXPORTFUNC
int GIIdl_vgl(int item, void **itemptr);
int GIIdl_vgl(int item, void **itemptr) {
	struct gii_module_source ***modulesptr;
	switch(item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***)itemptr;
		*modulesptr = _GIIdl_vgl;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
