/* $Id: input.c,v 1.41 2009/12/17 11:28:35 pekberg Exp $
******************************************************************************

   DirectX inputlib

   Copyright (C) 1999-2000 John Fortin		[fortinj@ibm.net]
   Copyright (C) 2000      Marcus Sundberg	[marcus@ggi-project.org]
   Copyright (C) 2004      Peter Ekberg		[peda@lysator.liu.se]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/gg.h>

#include <wctype.h>
#include <ctype.h>

#include "dxinput.h"
#include "dxinputkb.h"

#define GII_DI_LSHIFT (1<<0)
#define GII_DI_RSHIFT (1<<1)
#define GII_DI_LCTRL  (1<<2)
#define GII_DI_RCTRL  (1<<3)
#define GII_DI_LALT   (1<<4)
#define GII_DI_RALT   (1<<5)
#define GII_DI_SHIFT (GII_DI_LSHIFT | GII_DI_RSHIFT)
#define GII_DI_CTRL  (GII_DI_LCTRL  | GII_DI_RCTRL)
#define GII_DI_ALT   (GII_DI_LALT   | GII_DI_RALT)


static void
get_system_keyboard_repeat(gii_di_priv *priv)
{
	int iKeyDelay;
	DWORD dwKeySpeed;

	if(SystemParametersInfo(SPI_GETKEYBOARDDELAY, 0, &iKeyDelay, 0))
		priv->repeat_delay = 250000 * (iKeyDelay + 1);
	else
		/* Default to .5 seconds */
		priv->repeat_delay = 500000;

	if(SystemParametersInfo(SPI_GETKEYBOARDSPEED, 0, &dwKeySpeed, 0))
		priv->repeat_speed = 11825 * (31 - dwKeySpeed) + 33333;
	else
		/* Default to 25 Hz */
		priv->repeat_speed = 40000;

	DPRINT("key delay %d us, key repeat %d us.\n",
	       priv->repeat_delay,
	       priv->repeat_speed);
}

static void
_gii_dx_settings_changed(void *settings_changed_arg)
{
	ggUnlock(settings_changed_arg);
}

/* Caps lock does not seem to work. Why the h*ll not? */

static void
set_key_state(BYTE *keystate, uint32_t modifiers)
{
	keystate[VK_SHIFT]   = (modifiers & GII_MOD_SHIFT) ? 0x80 : 0;
	keystate[VK_CONTROL] = (modifiers & GII_MOD_CTRL)  ? 0x80 : 0;
	keystate[VK_MENU]    = (modifiers & GII_MOD_ALT)   ? 0x80 : 0;
	keystate[VK_CAPITAL] = (modifiers & GII_MOD_CAPS)  ? 0x81 : 0;
}

/* Eliminate UTF-16 surrogate pairs. */

static int
utf16_to_utf32(WCHAR *utf16, int count16, uint32_t *utf32)
{
	int count32 = 0;

	if(count16 <= 0)
		return count16;

	while(count16 > 0) {
		if((*utf16 & 0xf800) != 0xd800)
			*utf32++ = *utf16++;
		else if((*utf16 & 0xfc00) == 0xd800) {
			/* high surrogate */
			*utf32 = (*utf16++ & 0x3ff) << 10;
			if(--count16 < 1)
				/* no low surrogate */
				return 0;
			if((*utf16 & 0xfc00) != 0xdc00)
				/* not a low surrogate */
				return 0;
			*utf32++ |= *utf16++ & 0x3ff;
		}
		else /* if((*utf16 & 0xfc00) == 0xdc00) */
			/* unexpected low surrogate */
			return 0;

		--count16;
		++count32;
	}

	return count32;
}

static gii_event_mask
send_synthetic_key(struct gii_source *src, uint32_t sym)
{
	gii_di_priv *priv = DI_PRIV(src);
	gii_event ev;

	giiEventBlank(&ev, sizeof(gii_key_event));
	ev.any.size      = sizeof(gii_key_event);
	ev.any.origin    = priv->originkey;
	ev.key.modifiers = priv->modifiers;
	ev.key.button    = GII_BUTTON_NIL;
	ev.key.type      = evKeyPress;
	ev.key.sym       = sym;
	ev.key.label     = GIIK_VOID;
	giiPostEvent(src, &ev);

	giiEventBlank(&ev, sizeof(gii_key_event));
	ev.any.size      = sizeof(gii_key_event);
	ev.any.origin    = priv->originkey;
	ev.key.modifiers = priv->modifiers;
	ev.key.button    = GII_BUTTON_NIL;
	ev.key.type      = evKeyRelease;
	ev.key.sym       = sym;
	ev.key.label     = GIIK_VOID;
	giiPostEvent(src, &ev);

	return emKeyPress | emKeyRelease;
}

/* A mess since ToUnicode keeps state with regard to dead keys,
 * and is also used by the windows message translation code.
 */

/* Define MYWAY to get dead keys with unmodified not dead variant
 * in label and dead key in sym, instead of unmodified dead variant
 * in label and GIIK_VOID in sym.
 */
/*#define MYWAY*/

static gii_event_mask
map_key(struct gii_source *src, uint32_t scan, uint32_t *sym, uint32_t *label)
{
	gii_di_priv *priv = DI_PRIV(src);
	UINT vk;
	static BYTE keystate[256];
	WCHAR utf16[6];
	uint32_t utf32[2];
	int res;
	gii_event_mask ret = 0;

	set_key_state(keystate, priv->modifiers);

	vk = MapVirtualKey(scan, /* MAPVK_VSC_TO_VK */ 1);
	res = ToUnicode(vk, scan, keystate, utf16, 5, 0);
	res = utf16_to_utf32(utf16, res, utf32);
	switch(res) {
	case 0: /* fall back to hard coded values... */
		/*printf("no unicode translation\n");fflush(stdout);*/
		*sym = dx_kb[scan];
		*label = dx_kb[scan];
		break;
	case -1: /* We're not in sync. Sync up. */
		res = ToUnicode(vk, scan, keystate, utf16, 5, 0);
		res = utf16_to_utf32(utf16, res, utf32);
		/*if(res <= 0) {
			if(res == 0)
				printf("What? Suddenly no translation\n");
			else
				printf("What? Dead followed by dead...\n");
			fflush(stdout);
		}*/
		/* Should be in sync now. */
		/* Fall through... */
	default: /* We're in sync. Good. */
		if(priv->dead) {
			/* former key was dead. Don't trust initial call. */
			/* Replay the dead key. */
			set_key_state(keystate, priv->dead_mod);
			res = ToUnicode(priv->dead_vk, priv->dead_scan,
					keystate, utf16, 5, 0);
			res = utf16_to_utf32(utf16, res, utf32);
			/*if(res != -1) {
				printf("not dead, racy as hell.\n");
				fflush(stdout);
			}*/

			/* And restore the keystate for this key. */
			set_key_state(keystate, priv->modifiers);
		}
		/* We're in sync, and any former dead key is replayed */
		priv->dead = 0;
		res = ToUnicode(vk, scan, keystate, utf16, 5, 0);
		res = utf16_to_utf32(utf16, res, utf32);
		switch(res) {
		case -1: /* Dead */
			priv->dead = 1;
			priv->dead_vk = vk;
			priv->dead_scan = scan;
			priv->dead_mod = priv->modifiers;
			*sym = GIIK_VOID;
			break;
		case 0: /* Aiee, suddely no translation */
			/*printf("What? Suddely no translation.\n");
			fflush(stdout);*/
			*sym = GIIK_VOID;
			break;
		case 2:
			ret |= send_synthetic_key(src, utf32[0]);
			/* fall through */
		case 1:
			*sym = utf32[res - 1];
			break;
		}
		/* again to get unshifted variant / no dead key variant. */
		set_key_state(keystate, 0);
		res = ToUnicode(vk, scan, keystate, utf16, 5, 0);
		res = utf16_to_utf32(utf16, res, utf32);
		if(res == -1) {
			/* dead, do it once more... */
			res = ToUnicode(vk, scan, keystate, utf16, 5, 0);
			res = utf16_to_utf32(utf16, res, utf32);
		}
		if(res <= 0) {
			/*if(res == 0)
				printf("What? Suddenly no translation...\n");
			else
				printf("What? Dead followed by dead.\n");
			fflush(stdout);*/
			*label = GIIK_VOID;
			break;
		}
		*label = towupper(utf32[res - 1]);

		if(priv->dead) {
#ifdef MYWAY
			*sym = utf32[0];
			switch(*sym)
#else
//			*label = utf32[res - 1];
			switch(*label)
#endif
			{
			case GIIUC_Acute:
			case GIIUC_Cedilla:
			case GIIUC_Circumflex:
			case GIIUC_Diaeresis:
			case GIIUC_Grave:
			case GIIUC_Tilde:
			case GIIUC_Macron:
#ifdef MYWAY
				*sym = GII_KEY(GII_KT_DEAD, GII_KVAL(*sym));
#else
				*label = GII_KEY(GII_KT_DEAD,
						 GII_KVAL(*label));
				*sym = GIIK_VOID;
#endif
				break;
			}
		}
		break;
	}

	/* Don't care whether modifier syms are left or right */
	if(GII_KTYP(*sym) == GII_KT_MOD)
		*sym &= ~GII_KM_RIGHT;

	/* Fix caps lock modifier */
	if(!(priv->modifiers & GII_MOD_CAPS)
	   ^ !(priv->modifiers & GII_MOD_SHIFT))
		*sym = towupper(*sym);

	return ret;
}

static gii_event_mask
send_key(struct gii_source *src, uint32_t scan, uint8_t press)
{
	gii_di_priv *priv = DI_PRIV(src);
	gii_event ev;
	gii_event_mask ret = 0;
	int send_unicode = 0;

	do {
		if (scan != DIK_TAB)
			break;
		if (priv->di_modifiers & (GII_DI_RCTRL | GII_DI_LCTRL))
			break;
		if (!(priv->di_modifiers & (GII_DI_RALT | GII_DI_LALT)))
			break;
		return ret;
	} while(0);

	if (scan == DIK_ADD
		&& (priv->di_modifiers & (GII_DI_RALT | GII_DI_LALT))
		&& press)
	{
		priv->unicode_mode = 1;
		priv->unicode = 0;
		DPRINT_MISC("Unicode input: Entering...\n");
		return ret;
	}
	else if (priv->unicode_mode && press) {
		priv->unicode *= 16;
		switch (scan) {
		case DIK_0: case DIK_NUMPAD0:                      break;
		case DIK_1: case DIK_NUMPAD1: priv->unicode +=  1; break;
		case DIK_2: case DIK_NUMPAD2: priv->unicode +=  2; break;
		case DIK_3: case DIK_NUMPAD3: priv->unicode +=  3; break;
		case DIK_4: case DIK_NUMPAD4: priv->unicode +=  4; break;
		case DIK_5: case DIK_NUMPAD5: priv->unicode +=  5; break;
		case DIK_6: case DIK_NUMPAD6: priv->unicode +=  6; break;
		case DIK_7: case DIK_NUMPAD7: priv->unicode +=  7; break;
		case DIK_8: case DIK_NUMPAD8: priv->unicode +=  8; break;
		case DIK_9: case DIK_NUMPAD9: priv->unicode +=  9; break;
		case DIK_A:                   priv->unicode += 10; break;
		case DIK_B:                   priv->unicode += 11; break;
		case DIK_C:                   priv->unicode += 12; break;
		case DIK_D:                   priv->unicode += 13; break;
		case DIK_E:                   priv->unicode += 14; break;
		case DIK_F:                   priv->unicode += 15; break;
		default:                      priv->unicode /= 16; break;
		}
		DPRINT_MISC("Unicode input: Now at U+%04x\n", priv->unicode);
		return ret;
	}
	else if (priv->unicode_mode) {
		uint32_t modifiers = priv->di_modifiers;
		switch (dx_kb[scan]) {
		case GIIK_AltR: modifiers &= ~GII_DI_RALT; break;
		case GIIK_AltL: modifiers &= ~GII_DI_LALT; break;
		}
		if (modifiers & (GII_DI_RALT | GII_DI_LALT))
			return ret;
		send_unicode = 1;
	}

	giiEventBlank(&ev, sizeof(gii_key_event));

	ev.any.size = sizeof(gii_key_event);
	ev.any.origin = priv->originkey;
	ev.key.modifiers = priv->modifiers;
	ev.key.button = scan;
	if (press) {
		if (press == 2) {
			ev.any.type = evKeyRepeat;
			ret |= emKeyRepeat;
		}
		else {
			ev.any.type = evKeyPress;
			ret |= emKeyPress;
		}
		/* Store sym & label for release event */
		ret |= map_key(src, scan, &ev.key.sym, &ev.key.label);
		priv->symlabel[scan][0] = ev.key.sym;
		priv->symlabel[scan][1] = ev.key.label;

		if(ev.key.label == GIIK_CapsLock)
			priv->modifiers ^= GII_MOD_CAPS;
	} else {
		ev.any.type = evKeyRelease;
		if(priv->symlabel[scan][0] != GIIK_NIL) {
			/* Retrieve stored sym & label */
			ev.key.sym   = priv->symlabel[scan][0];
			ev.key.label = priv->symlabel[scan][1];
			priv->symlabel[scan][0] = GIIK_NIL;
			priv->symlabel[scan][1] = GIIK_NIL;
			ret |= emKeyRelease;
		}
		/* else, Eeek, double release */
	}

	if(ret)
		giiPostEvent(src, &ev);

	if (send_unicode) {
		DPRINT_MISC("Unicode input: Sending U+%04x\n",
			priv->unicode);
		priv->modifiers &= ~GII_MOD_ALT;
		priv->unicode_mode = 0;
		ret |= send_synthetic_key(src, priv->unicode);
	}

	return ret;
}


static gii_event_mask
send_ptr(struct gii_source *src, int relative,
	     uint32_t type,  uint32_t data,
	     uint32_t type2, uint32_t data2)
{
	gii_di_priv *priv = DI_PRIV(src);
	gii_event ev;

	switch (type) {
	case DIMOFS_X:		/* Mouse X movement */
	case DIMOFS_Y:		/* Mouse Y Movement */
	case DIMOFS_Z:		/* Mouse Z Movement (wheel?) */
		giiEventBlank(&ev, sizeof(gii_pmove_event));
		ev.any.size = sizeof(gii_pmove_event);
		ev.any.type = relative ? evPtrRelative : evPtrAbsolute;
		ev.any.origin = priv->originptr;
		if(type == DIMOFS_X)
			ev.pmove.x = data;
		else if(type == DIMOFS_Y)
			ev.pmove.y = data;
		else
			ev.pmove.wheel = data;
		if((type == DIMOFS_X) && (type2 == DIMOFS_Y))
			ev.pmove.y = data2;
		giiPostEvent(src, &ev);
		return relative ? emPtrRelative : emPtrAbsolute;

	case DIMOFS_BUTTON0:	/* Key Press/Release */
	case DIMOFS_BUTTON1:
	case DIMOFS_BUTTON2:
	case DIMOFS_BUTTON3:
#ifdef DIMOFS_BUTTON4
	case DIMOFS_BUTTON4:
	case DIMOFS_BUTTON5:
	case DIMOFS_BUTTON6:
	case DIMOFS_BUTTON7:
#endif /* DIMOFS_BUTTON4 */
		giiEventBlank(&ev, sizeof(gii_pbutton_event));
		ev.any.size = sizeof(gii_pbutton_event);
		ev.any.type = data ? evPtrButtonPress : evPtrButtonRelease;
		ev.any.origin = priv->originptr;
		ev.pbutton.button = GII_PBUTTON_(1 + type - DIMOFS_BUTTON0);
		giiPostEvent(src, &ev);
		return data ? emPtrButtonPress : emPtrButtonRelease;
	}			/* End switch */

	return 0;		/* Should never get here */
}


static gii_event_mask
send_joy(struct gii_source *src, gii_di_priv_dev *dev, uint32_t type, uint32_t data)
{
	gii_event ev;

	if (type >= DIJOFS_BUTTON(0) && type <= DIJOFS_BUTTON(31)) {
		giiEventBlank(&ev, sizeof(gii_key_event));
		ev.any.size = sizeof(gii_key_event);
		ev.any.type = data ? evKeyPress : evKeyRelease;
		ev.any.origin = dev->origin;
		ev.key.modifiers = 0;
		ev.key.button = 1 + type - DIJOFS_BUTTON(0);
		ev.key.sym = GIIK_VOID;
		ev.key.label = GIIK_VOID;
		giiPostEvent(src, &ev);
		return data ? emKeyPress : emKeyRelease;
	}

	if (type >= DIJOFS_POV(0) && type <= DIJOFS_POV(3)) {
		int povnr = (type - DIJOFS_POV(0)) / sizeof(DWORD);
		int povbtn = dev->devinfo.num_buttons - dev->povs + povnr;
		int povaxis = dev->devinfo.num_valuators - dev->povs + povnr;
		if ((data & 0xffff) == 0xffff) {
			/* POV released */
			if (!dev->btn[povbtn])
				return 0;
			dev->btn[povbtn] = 0;
			giiEventBlank(&ev, sizeof(gii_key_event));
			ev.any.size = sizeof(gii_key_event);
			ev.any.type = evKeyRelease;
			ev.any.origin = dev->origin;
			ev.key.modifiers = 0;
			ev.key.button = 1 + povbtn;
			ev.key.sym = GIIK_VOID;
			ev.key.label = GIIK_VOID;
			giiPostEvent(src, &ev);
			return emKeyRelease;
		}
		giiEventBlank(&ev, sizeof(gii_val_event));
		ev.any.size = sizeof(gii_val_event);
		ev.any.type = evValAbsolute;
		ev.any.origin = dev->origin;
		ev.val.first = povaxis;
		ev.val.count = 1;
		ev.val.value[0] = data;
		giiPostEvent(src, &ev);
		if (!dev->btn[povbtn]) {
			/* First POV direction */
			dev->btn[povbtn] = 128;
			giiEventBlank(&ev, sizeof(gii_key_event));
			ev.any.size = sizeof(gii_key_event);
			ev.any.type = evKeyPress;
			ev.any.origin = dev->origin;
			ev.key.modifiers = 0;
			ev.key.button = 1 + povbtn;
			ev.key.sym = GIIK_VOID;
			ev.key.label = GIIK_VOID;
			giiPostEvent(src, &ev);
			return emValAbsolute | emKeyPress;
		}
		return emValAbsolute;
	}

	giiEventBlank(&ev, sizeof(gii_val_event));
	ev.any.size = sizeof(gii_val_event);
	ev.any.type = evValAbsolute;
	ev.any.origin = dev->origin;
	ev.val.first = type;
	ev.val.count = 1;
	ev.val.value[0] = data;
	giiPostEvent(src, &ev);
	return emValAbsolute;
}

/* Don't add more than 1000000 microseconds */
static inline void
tv_add_usec(struct timeval *tv, uint32_t usec)
{
	tv->tv_usec += usec;
	if (tv->tv_usec >= 1000000) {
		++tv->tv_sec;
		tv->tv_usec -= 1000000;
	}
}

/* return 1 if tv1 is later than tv2, -1 if tv2 is later,
 * and 0 if equal
 */
static inline int
tv_compare(struct timeval *tv1, struct timeval *tv2)
{
	if (tv1->tv_sec > tv2->tv_sec)
		return 1;
	if (tv1->tv_sec < tv2->tv_sec)
		return -1;
	if (tv1->tv_usec > tv2->tv_usec)
		return 1;
	if (tv1->tv_usec < tv2->tv_usec)
		return -1;
	return 0;
}

static int
di_ptr_button_is_down(gii_di_priv *priv)
{
	unsigned int i;

	for (i = 0; i < sizeof(priv->ptrbtn) / sizeof(priv->ptrbtn[0]); ++i) {
		if (priv->ptrbtn[i])
			return 1;
	}

	return 0;
}

static void
di_ptr_button_force_up(gii_di_priv *priv)
{
	unsigned int i;

	for (i = 0; i < sizeof(priv->ptrbtn) / sizeof(priv->ptrbtn[0]); ++i)
		priv->ptrbtn[i] = 0;
}

static int
di_poll(struct gii_source *src)
{
	gii_di_priv *priv = DI_PRIV(src);
	gii_di_priv_dev *dev;
	gii_di_priv_obj *obj;
	DWORD KB_Elements;
	DWORD Ptr_Elements;
	DWORD Joy_Elements;
	POINT CurPos;
	RECT WinSize;
	uint32_t i, j;
	uint8_t KB_Buffer[2 * GII_DX_BUFFER_SIZE];
	DWORD Ptr_Buffer[2 * GII_DX_BUFFER_SIZE];
	DWORD Joy_Buffer[2 * GII_DX_BUFFER_SIZE];
	struct timeval now = { 0, 0 };
	int sent_abs;
	int ptrbtn_down;

	if(!ggTryLock(priv->settings_changed_lock))
		get_system_keyboard_repeat(priv);

	_gii_dx_GetKBInput(priv, &KB_Elements, (char *) KB_Buffer);
	if (priv->repeat_key != -1)
		ggCurTime(&now);
	for (i = 0; i < KB_Elements; i++) {
		if (KB_Buffer[2 * i + 1] == 'D') {
			if (now.tv_sec == 0)
				ggCurTime(&now);
			DPRINT("gii_send_key: poll(%p);\n", src);
			send_key(src, KB_Buffer[2 * i], 1);

			priv->repeat_key = -1;
			switch (dx_kb[KB_Buffer[2 * i]]) {
			case GIIK_ShiftR: priv->di_modifiers |= GII_DI_RSHIFT;
				break;
			case GIIK_ShiftL: priv->di_modifiers |= GII_DI_LSHIFT;
				break;
			case GIIK_CtrlR:  priv->di_modifiers |= GII_DI_RCTRL;
				break;
			case GIIK_CtrlL:  priv->di_modifiers |= GII_DI_LCTRL;
				break;
			case GIIK_AltR:   priv->di_modifiers |= GII_DI_RALT;
				break;
			case GIIK_AltL:   priv->di_modifiers |= GII_DI_LALT;
				break;
			}
			if (GII_KTYP(dx_kb[KB_Buffer[2 * i]]) != GII_KT_MOD) {
				priv->repeat_key = KB_Buffer[2 * i];
				priv->repeat_at = now;
				tv_add_usec(&priv->repeat_at,
					priv->repeat_delay);
			}
		} else {
			send_key(src, KB_Buffer[2 * i], 0);

			switch (dx_kb[KB_Buffer[2 * i]]) {
			case GIIK_ShiftR: priv->di_modifiers &= ~GII_DI_RSHIFT;
				break;
			case GIIK_ShiftL: priv->di_modifiers &= ~GII_DI_LSHIFT;
				break;
			case GIIK_CtrlR:  priv->di_modifiers &= ~GII_DI_RCTRL;
				break;
			case GIIK_CtrlL:  priv->di_modifiers &= ~GII_DI_LCTRL;
				break;
			case GIIK_AltR:   priv->di_modifiers &= ~GII_DI_RALT;
				break;
			case GIIK_AltL:   priv->di_modifiers &= ~GII_DI_LALT;
				break;
			}
			if (priv->repeat_key == KB_Buffer[2 * i])
				priv->repeat_key = -1;
		}

		if(priv->di_modifiers & GII_DI_SHIFT)
			priv->modifiers |= GII_MOD_SHIFT;
		else
			priv->modifiers &= ~GII_MOD_SHIFT;
		if(priv->di_modifiers & (GII_DI_CTRL | GII_DI_RALT))
			priv->modifiers |= GII_MOD_CTRL;
		else
			priv->modifiers &= ~GII_MOD_CTRL;
		if(priv->di_modifiers & GII_DI_ALT)
			priv->modifiers |= GII_MOD_ALT;
		else
			priv->modifiers &= ~GII_MOD_ALT;
	}
	if ((src->reqmask & emKeyRepeat) && priv->repeat_key != -1) {
		i = 10;
		while (--i && tv_compare(&now, &priv->repeat_at) >= 0) {
			send_key(src, priv->repeat_key, 2);
			tv_add_usec(&priv->repeat_at, priv->repeat_speed);
		}
		if (!i) {
			/* Don't loop too much here in case someone
			 * changed the system clock or something...
			 */
			priv->repeat_at = now;
			tv_add_usec(&priv->repeat_at, priv->repeat_speed);
		}
	}

	ptrbtn_down = di_ptr_button_is_down(priv);
	_gii_dx_GetPtrInput(priv, &Ptr_Elements, Ptr_Buffer);
	if(Ptr_Elements && (src->reqmask & emPtrAbsolute)) {
		int outside = 0;
		GetCursorPos(&CurPos);
		ScreenToClient(priv->hWnd, &CurPos);
		GetClientRect(priv->hWnd, &WinSize);

		if (CurPos.x < 0) {
			CurPos.x = 0;
			outside = 1;
		}
		if (CurPos.y < 0) {
			CurPos.y = 0;
			outside = 1;
		}
		if (CurPos.x >= WinSize.right) {
			CurPos.x = WinSize.right - 1;
			outside = 1;
		}
		if (CurPos.y >= WinSize.bottom) {
			CurPos.y = WinSize.bottom - 1;
			outside = 1;
		}
		if (outside && !ptrbtn_down) {
			di_ptr_button_force_up(priv);
			Ptr_Elements = 0;
		}
	}
	sent_abs = 0;
	for (i = 0; i < Ptr_Elements; i++) {
		switch (Ptr_Buffer[2 * i]) {
		case DIMOFS_X:
		case DIMOFS_Y:
			if (!sent_abs
			    && (src->reqmask & emPtrAbsolute)) {
				/* abs; must send both x and y */
				send_ptr(src, 0,
					DIMOFS_X, CurPos.x,
					DIMOFS_Y, CurPos.y);
				sent_abs = 1;
				break;
			}

			if(sent_abs || !(src->reqmask & emPtrRelative))
				break;
			if((i+1 < Ptr_Elements)
			   && (Ptr_Buffer[2*i] == DIMOFS_X)
			   && (Ptr_Buffer[2*i+2] == DIMOFS_Y)) {
			   	/* consolidate dx and dy */
				send_ptr(src, 1,
					DIMOFS_X, Ptr_Buffer[2*i+1],
					DIMOFS_Y, Ptr_Buffer[2*i+3]);
				++i;
			}
			else
				send_ptr(src, 1,
					Ptr_Buffer[2*i], Ptr_Buffer[2*i+1],
					DIMOFS_X, 0);
			break;
		case DIMOFS_Z:
			if(src->reqmask & emPtrRelative) {
				send_ptr(src, 1,
					DIMOFS_Z, Ptr_Buffer[2*i+1],
					DIMOFS_X, 0);
			}
			break;
		case DIMOFS_BUTTON0:
		case DIMOFS_BUTTON1:
		case DIMOFS_BUTTON2:
		case DIMOFS_BUTTON3:
#ifdef DIMOFS_BUTTON4
		case DIMOFS_BUTTON4:
		case DIMOFS_BUTTON5:
		case DIMOFS_BUTTON6:
		case DIMOFS_BUTTON7:
#endif /* DIMOFS_BUTTON4 */
			if(Ptr_Buffer[2 * i + 1] & 0x80) {
				if(!(src->reqmask & emPtrButtonPress))
					break;
			}
			else
				if(!(src->reqmask & emPtrButtonRelease))
					break;
			send_ptr(src, 0, Ptr_Buffer[2 * i],
					Ptr_Buffer[2 * i + 1] & 0x80,
					DIMOFS_Z, 0);
			break;
		}
	}

	for(dev = priv->devs; dev; dev = dev->next) {
		_gii_dx_GetJoyInput(dev, &Joy_Elements, Joy_Buffer);
		for (i = 0; i < Joy_Elements; i++) {
			DWORD ofs = Joy_Buffer[2 * i];
			if (ofs >= DIJOFS_BUTTON(0)
			    && ofs <= DIJOFS_BUTTON(31)) {
				/* Button */
				send_joy(src, dev, ofs,
						Joy_Buffer[2 * i + 1] & 0x80);
				continue;
			}
			for (j = 0, obj = dev->objs; obj; ++j, obj = obj->next) {
				if (ofs != obj->ofs)
					continue;
				if (ofs < DIJOFS_POV(0)
				    || ofs > DIJOFS_POV(3))
					ofs = j;
				send_joy(src, dev, ofs,
						Joy_Buffer[2 * i + 1]);
			}
		}
	}

	return GII_OP_DONE;
}


static void
_di_close(gii_di_priv *priv)
{
	gii_di_priv_dev *dev;
	gii_di_priv_obj *obj;
	void *tmp;

	if(!priv)
		return;

	_gii_dx_release(priv);
	for (dev = priv->devs; dev; dev = tmp) {
		if (dev->btn)
			free(dev->btn);
		for (obj = dev->objs; obj; obj = tmp) {
			tmp = obj->next;
			free(obj);
		}
		/* AIEE! The allvalinfos memory is still referred to by the
		 * gii core. Pretty rude/dangerous to free it, but there are
		 * no later notifications so this is the last point at which
		 * it can be freed.
		 */
		if (dev->allvalinfos)
			free(dev->allvalinfos);
		tmp = dev->next;
		free(dev);
	}
	free(priv);
}


static void
di_close(struct gii_source *src)
{
	_di_close(DI_PRIV(src));

	DPRINT("closed\n");
}


static struct gii_cmddata_devinfo di_devinfo_key =
{
	"Direct Input Keyboard",/* long device name */
	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,
	emKey,			/* can_generate */
	GII_NUM_UNKNOWN,
	0,			/* no valuators */
	256			/* 256 pseudo buttons */
};

static struct gii_cmddata_devinfo di_devinfo_ptr =
{
	"Direct Input Mouse",	/* long device name */
	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,
	emPointer,		/* can_generate */
	GII_NUM_UNKNOWN,
	0,			/* no valuators */
	GII_NUM_UNKNOWN		/* number of buttons */
};

static int
send_valinfo(struct gii_source *src, uint32_t origin, gii_di_priv_dev *dev, uint32_t val)
{
	gii_event ev;
	struct gii_cmddata_valinfo *vinfo;
	gii_di_priv_obj *obj;
	uint32_t i;

	if (val == GII_VAL_QUERY_ALL) {
		for (i = 0; i < dev->devinfo.num_valuators; ++i)
			send_valinfo(src, origin, dev, i);
		return 0;
	}

	if (val >= dev->devinfo.num_valuators)
		return GGI_EARGINVAL;

	giiEventBlank(&ev, sizeof(gii_cmd_nodata_event) +
		       sizeof(struct gii_cmddata_valinfo));

	ev.any.size   = sizeof(gii_cmd_nodata_event) +
		         sizeof(struct gii_cmddata_valinfo);
	ev.any.type   = evCommand;
	ev.any.origin = origin;
	ev.cmd.code   = GII_CMDCODE_VALUATOR_INFO;

	vinfo = (struct gii_cmddata_valinfo *) ev.cmd.data;

	obj = dev->objs;
	for (i = 0, obj = dev->objs; obj; ++i, obj = obj->next) {
		if (i == val)
			break;
	}
	if (!obj)
		return GGI_ENOTFOUND;

	*vinfo = obj->valinfo;

	return giiPostEvent(src, &ev);
}


static int
di_event(struct gii_source *src, gii_event *ev)
{
	gii_di_priv *priv = DI_PRIV(src);
	gii_di_priv_dev *dev = NULL;

	do {
#if 0
		if ((ev->any.target & GII_SOURCEMASK) == src->origin)
			break;
#endif
		if (ev->any.target == GII_EV_TARGET_ALL)
			break;
		if (ev->any.target == priv->originkey)
			break;
		if (ev->any.target == priv->originptr)
			break;

		for (dev = priv->devs; dev; dev = dev->next) {
			if(ev->any.target == dev->origin)
				break;
		}
		if (!dev)
			/* not for us */
			return GGI_EEVNOTARGET;
	} while(0);

	if (ev->any.type != evCommand) {
		return GGI_EEVUNKNOWN;
	}
	if (ev->cmd.code == GII_CMDCODE_VALUATOR_INFO) {
		struct gii_cmddata_valinfo *vi;
		vi = (struct gii_cmddata_valinfo *) ev->cmd.data;
		if (ev->any.target == priv->originkey)
			return GGI_EEVNOTARGET;
		if (ev->any.target == priv->originptr)
			return GGI_EEVNOTARGET;
		if (dev)
			return send_valinfo(src,
					dev->origin, dev, vi->number);
		/* GII_EV_TARGET_ALL */
		for (dev = priv->devs; dev; dev = dev->next)
			send_valinfo(src, dev->origin, dev, vi->number);
		return 0;
	}
#if 0
FIXME
	if (ev->cmd.code == GII_CMDCODE_PREFER_ABSPTR) {
		if (ev->any.target == priv->originkey)
			return GGI_EEVNOTARGET;
		if (dev)
			return GGI_EEVNOTARGET;
		/* pointer or GII_EV_TARGET_ALL */
		inp->curreventmask &= ~emPtrRelative;
		inp->curreventmask |= emPtrAbsolute;
		return 0;
	}
	if (ev->cmd.code == GII_CMDCODE_PREFER_RELPTR) {
		if (ev->any.target == priv->originkey)
			return GGI_EEVNOTARGET;
		if (dev)
			return GGI_EEVNOTARGET;
		/* pointer or GII_EV_TARGET_ALL */
		inp->curreventmask &= ~emPtrAbsolute;
		inp->curreventmask |= emPtrRelative;
		return 0;
	}
#endif
	return GGI_EEVUNKNOWN;	/* Unknown command */
}

static int
di_controller(void *arg, uint32_t ctl, void *data)
{
	struct gii_source *src = arg;
	gii_di_priv *priv = DI_PRIV(src);
	gii_event *ev_ = data;
	gii_event ev;
	gii_inputdx_hotkey *hotkey = data;
	int scan;

	switch (ctl) {
	case GII_CMDCODE_RESIZE:
		giiEventBlank(&ev, sizeof(gii_cmd_event));
		memcpy(ev.cmd.data, ev_->cmd.data, sizeof(ev.cmd.data));
		ev.any.size = ev_->any.size;
		ev.any.type = ev_->any.type;
		ev.cmd.code = ev_->cmd.code;
		/* XXX Threadsafe? */
		giiPostEvent(src, &ev);
		break;

	case GII_INPUTDX_HOTKEY:
		DPRINT("Send synthetic hotkey\n");
		giiEventBlank(&ev, sizeof(gii_key_event));
		ev.any.size = sizeof(gii_key_event);
		ev.any.origin = priv->originkey;
		ev.any.type = evKeyPress;
		ev.key.modifiers = priv->modifiers;
		switch (hotkey->vk) {
		case VK_TAB:
			scan = DIK_TAB;
			break;
		case VK_F4:
			scan = DIK_F4;
			break;
		default:
			scan = 0;
			break;
		}
		if (!scan)
			break;
		ev.key.button = scan;
		/* Store sym & label for release event */
		map_key(src, scan, &ev.key.sym, &ev.key.label);
		priv->symlabel[scan][0] = ev.key.sym;
		priv->symlabel[scan][1] = ev.key.label;

		if(ev.key.label == GIIK_CapsLock)
			priv->modifiers ^= GII_MOD_CAPS;

		giiPostEvent(src, &ev);

		giiEventBlank(&ev, sizeof(gii_key_event));
		ev.any.size = sizeof(gii_key_event);
		ev.any.origin = priv->originkey;
		ev.any.type = evKeyRelease;
		ev.key.modifiers = priv->modifiers;
		ev.key.button = scan;
		/* Retrieve stored sym & label */
		ev.key.sym   = priv->symlabel[scan][0];
		ev.key.label = priv->symlabel[scan][1];
		priv->symlabel[scan][0] = GIIK_NIL;
		priv->symlabel[scan][1] = GIIK_NIL;

		giiPostEvent(src, &ev);
		break;

	case GII_CMDCODE_INJECT_EVENT: 
		{
			giiEventBlank(ev_, 0); /* only set timestamp */
			ev_->any.origin = src->origin;
			giiPostEvent(src, ev_);
		}
		break;
	}

	return GGI_OK;
}

static int
GII_directx_init(struct gii_source *src,
		 const char *tgt,
		 const char *args,
		 void *argptr)
{
	gii_di_priv *priv = NULL;
	gii_di_priv_dev *dev;
	gii_inputdx_arg *inputdx = (gii_inputdx_arg *)argptr;
	int own_wnd = 0;
	int ret;
	int i;

#if 0
not enabled yet
	if(!strcasecmp(tgt, "input-directxwin")) {
		DPRINT("Creating my own window\n");
		inputdx = _gii_dx_CreateWindow();
		if (!inputdx) {
			ret = GGI_ENOMEM;
			goto cleanup;
		}
		own_wnd = 1;
	}
#endif

	if (!inputdx) {
		return GGI_EARGINVAL;
		goto cleanup;
	}

	DPRINT("HWND=%ld\n", (long)inputdx->hWnd);

	priv = malloc(sizeof(gii_di_priv));
	if (priv == NULL) {
		ret = GGI_ENOMEM;
		goto cleanup;
	}

	priv->own_wnd = 0;

	priv->settings_changed_lock = ggLockCreate();
	if (!priv->settings_changed_lock) {
		ret = GGI_ENOMEM;
		goto cleanup;
	}
	ggLock(priv->settings_changed_lock);

	inputdx->settings_changed = _gii_dx_settings_changed;
	inputdx->settings_changed_arg = priv->settings_changed_lock; 

	priv->hWnd = inputdx->hWnd;
	priv->own_wnd = own_wnd;

	priv->originkey = giiAddDevice(src, &di_devinfo_key, NULL);
	if(priv->originkey == 0) {
		ret = GGI_ENOMEM;
		goto cleanup;
	}

	di_devinfo_ptr.num_buttons = GetSystemMetrics(SM_CMOUSEBUTTONS);
	priv->originptr = giiAddDevice(src, &di_devinfo_ptr, NULL);
	if(priv->originptr == 0) {
		ret = GGI_ENOMEM;
		goto cleanup;
	}

	priv->hWnd = inputdx->hWnd;
	priv->devs = NULL;
	priv->pDI = NULL;
	priv->pKeyboard = NULL;
	priv->pMouse = NULL;
	priv->modifiers = 0;
	priv->di_modifiers = 0;
	priv->repeat_key = -1;
	for(i = 0; i < 256; ++i) {
		priv->symlabel[i][0] = GIIK_NIL;
		priv->symlabel[i][1] = GIIK_NIL;
	}

	ret = _gii_dx_InitDirectInput(priv, inputdx->hWnd);
	if (ret != 0) {
		DPRINT("Unable to grab keyboard\n");
		DPRINT("HWND=%ld rc=%d\n", (long)inputdx->hWnd, ret);
		ret = GGI_ENODEVICE;
		goto cleanup;
	}

	for (dev = priv->devs; dev; dev = dev->next) {
		gii_di_priv_obj *obj;

		if (dev->objs) {
			dev->allvalinfos = calloc(dev->devinfo.num_valuators,
					sizeof(*dev->allvalinfos));
			if(!dev->allvalinfos) {
				ret = GGI_ENOMEM;
				goto cleanup;
			}
		}

		for (i = 0, obj = dev->objs; obj; ++i, obj = obj->next)
			dev->allvalinfos[i] = obj->valinfo;

		dev->origin = giiAddDevice(src, &dev->devinfo,
						 dev->allvalinfos);
		if (dev->origin == 0) {
			ret = GGI_ENOMEM;
			goto cleanup;
		}
	}

	src->priv = priv;

	src->ops.event = di_event;
	src->ops.poll = di_poll;
	src->ops.close = di_close;

	ggSetController(src->instance.channel, di_controller);

	initkb();

	get_system_keyboard_repeat(priv);

	giiSetNerveTick(src, &priv->nerve, 10);

	if (own_wnd)
		free(inputdx);

	return 0;

cleanup:
	_di_close(priv);

	if (own_wnd) {
		DestroyWindow(inputdx->hWnd);
		free(inputdx);
	}

	return ret;
}


struct gii_module_source GII_directx = {
	GG_MODULE_INIT("input-directx", 0, 1, GII_MODULE_SOURCE),
	GII_directx_init
};

static struct gii_module_source *_GIIdl_directx[] = {
	&GII_directx,
	NULL
};


EXPORTFUNC
int GIIdl_directx(int item, void **itemptr);
int GIIdl_directx(int item, void **itemptr) {
	struct gii_module_source ***modulesptr;
	switch(item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***)itemptr;
		*modulesptr = _GIIdl_directx;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
