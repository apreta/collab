/* $Id: input.c,v 1.27 2009/05/29 15:48:31 pekberg Exp $
******************************************************************************

   Input-vnc: initialization

   Copyright (C) 2008 Peter Rosin      [peda@lysator.liu.se]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"

#include <stdlib.h>
#include <ctype.h>
#ifdef HAVE_STRING_H
#include <string.h>
#endif
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#include <ggi/gg.h>
#include <ggi/gii-module.h>
#include <ggi/gii-keyboard.h>
#include <ggi/internal/gii_debug.h>
#include <ggi/input/vnc.h>


typedef struct origin_t {
	GG_LIST_ENTRY(origin_t) others;
	uint32_t origin;
	int pending;
	struct gii_cmddata_devinfo devinfo;
	struct gii_cmddata_valinfo *valinfo;
} vnc_origin;

typedef struct client_t {
	GG_LIST_ENTRY(client_t) others;
	void *ctx;
	int fd;
	int wfd;
	uint32_t nerve;
	uint32_t wnerve;
	GG_LIST_HEAD(origins, origin_t) origins;
	int gii_version;
} vnc_client;

typedef struct vnc_hook
{
	int sfd;
	GG_LIST_HEAD(clients, client_t) clients;

	gii_vnc_new_client *new_client;
	gii_vnc_client_data *client_data;
	gii_vnc_write_client *write_client;
	gii_vnc_safe_write *safe_write;
	void *usr_ctx;

	uint32_t origin;
	uint32_t server_nerve;

	uint8_t buttons;
} vnc_priv;

#define VNC_PRIV(inp)  ((vnc_priv *) inp->priv)
#define client_FOREACH(client, priv) \
	GG_LIST_FOREACH(client, &(priv)->clients, others)
#define client_FIRST(priv) \
	GG_LIST_FIRST(&(priv)->clients)
#define client_NEXT(client) \
	GG_LIST_NEXT(client, others)
#define client_INSERT(priv, client) \
	GG_LIST_INSERT_HEAD(&(priv)->clients, client, others)
#define client_REMOVE(client) \
	GG_LIST_REMOVE(client, others)

#define origin_FOREACH(origin, client) \
	GG_LIST_FOREACH(origin, &(client)->origins, others)
#define origin_FIRST(client) \
	GG_LIST_FIRST(&(client)->origins)
#define origin_NEXT(origin) \
	GG_LIST_NEXT(origin, others)
#define origin_INSERT(client, origin) \
	GG_LIST_INSERT_HEAD(&(client)->origins, origin, others)
#define origin_REMOVE(origin) \
	GG_LIST_REMOVE(origin, others)


static void
GII_vnc_add_cfd(void *arg, void *ctx, int fd)
{
	struct gii_source *src = arg;
	vnc_priv *priv = VNC_PRIV(src);
	vnc_client *client;

	DPRINT("add_cfd(%d)\n", fd);

	client = malloc(sizeof(*client));
	client->ctx = ctx;
	client->fd = fd;
	client->wnerve = 0;
	GG_LIST_INIT(&client->origins);
	client->gii_version = 0;
	client_INSERT(priv, client);
	giiSetNerveReadFD(src, &client->nerve, fd);
}

static inline vnc_client *
find_client(struct gii_source *src, void *ctx)
{
	vnc_priv *priv = VNC_PRIV(src);
	vnc_client *client;

	client_FOREACH(client, priv) {
		if (client->ctx == ctx)
			break;
	}
	return client;
}

static void
GII_vnc_del_cfd(void *arg, void *ctx, int fd)
{
	struct gii_source *src = arg;
	vnc_client *client;
	vnc_origin *origin;

	DPRINT("del_cfd(%d)\n", fd);

	client = find_client(src, ctx);
	if (!client)
		return;

	while ((origin = origin_FIRST(client))) {
		origin_REMOVE(origin);
		if (!origin->pending)
			giiDelDevice(src, origin->origin);
		if (origin->valinfo)
			free(origin->valinfo);
		free(origin);
	}

	giiCutNerve(src, &client->nerve);
	giiCutNerve(src, &client->wnerve);
	client_REMOVE(client);
	free(client);
}

static void
GII_vnc_add_cwfd(void *arg, void *ctx, int fd)
{
	struct gii_source *src = arg;
	vnc_client *client;

	DPRINT("add_cwfd(%d)\n", fd);

	client = find_client(src, ctx);
	if (!client)
		return;

	client->wfd = fd;
	giiSetNerveFD(src, &client->wnerve, fd, GII_NERVE_FD_WRITE);
}

static void
GII_vnc_del_cwfd(void *arg, void *ctx, int fd)
{
	struct gii_source *src = arg;
	vnc_client *client;

	DPRINT("del_cwfd(%d)\n", fd);

	client = find_client(src, ctx);
	if (!client)
		return;

	giiCutNerve(src, &client->wnerve);
	client->wnerve = 0;
}

static void
GII_vnc_add_crfd(void *arg, void *ctx)
{
	struct gii_source *src = arg;
	vnc_client *client;

	DPRINT("add_crfd()\n");

	client = find_client(src, ctx);
	if (!client)
		return;

	giiSetNerveReadFD(src, &client->nerve, client->fd);
}

static void
GII_vnc_del_crfd(void *arg, void *ctx)
{
	struct gii_source *src = arg;
	vnc_client *client;

	DPRINT("del_crfd()\n");

	client = find_client(src, ctx);
	if (!client)
		return;

	giiCutNerve(src, &client->nerve);
	client->nerve = 0;
}

static uint32_t map_key(uint32_t key)
{
	switch (key) {
	case 0xff08:   key = GIIUC_BackSpace;                          break;
	case 0xff09:   key = GIIUC_Tab;                                break;
	case 0xff0d:   key = GIIUC_Return;                             break;
	case 0xff1b:   key = GIIUC_Escape;                             break;
	case 0xff63:   key = GIIK_Insert;                              break;
	case 0xff67:   key = GIIK_Menu;                                break;
	case 0xffff:   key = GIIUC_Delete;                             break;
	case 0xff50:   key = GIIK_Home;                                break;
	case 0xff57:   key = GIIK_End;                                 break;
	case 0xff55:   key = GIIK_PageUp;                              break;
	case 0xff56:   key = GIIK_PageDown;                            break;
	case 0xff51:   key = GIIK_Left;                                break;
	case 0xff52:   key = GIIK_Up;                                  break;
	case 0xff53:   key = GIIK_Right;                               break;
	case 0xff54:   key = GIIK_Down;                                break;
	case 0xffbe:   key = GIIK_F1;                                  break;
	case 0xffbf:   key = GIIK_F2;                                  break;
	case 0xffc0:   key = GIIK_F3;                                  break;
	case 0xffc1:   key = GIIK_F4;                                  break;
	case 0xffc2:   key = GIIK_F5;                                  break;
	case 0xffc3:   key = GIIK_F6;                                  break;
	case 0xffc4:   key = GIIK_F7;                                  break;
	case 0xffc5:   key = GIIK_F8;                                  break;
	case 0xffc6:   key = GIIK_F9;                                  break;
	case 0xffc7:   key = GIIK_F10;                                 break;
	case 0xffc8:   key = GIIK_F11;                                 break;
	case 0xffc9:   key = GIIK_F12;                                 break;
	case 0xffe1:   key = GIIK_ShiftL;                              break;
	case 0xffe2:   key = GIIK_ShiftR;                              break;
	case 0xffe3:   key = GIIK_CtrlL;                               break;
	case 0xffe4:   key = GIIK_CtrlR;                               break;
	case 0xffe7:   key = GIIK_MetaL;                               break;
	case 0xffe8:   key = GIIK_MetaR;                               break;
	case 0xffe9:   key = GIIK_AltL;                                break;
	case 0xffea:   key = GIIK_AltR;                                break;
	}

	return key;
}

static void
GII_vnc_key(void *arg, int down, uint32_t key)
{
	struct gii_source *src = arg;
	vnc_priv *priv = VNC_PRIV(src);
	gii_event ev;
	uint32_t remapped_key = map_key(key);

	giiEventBlank(&ev, sizeof(gii_key_event));

	ev.any.size      = sizeof(gii_key_event);
	ev.any.type      = down ? evKeyPress : evKeyRelease;
	ev.any.origin    = priv->origin;
	ev.key.modifiers = 0;
	ev.key.sym       = remapped_key;
	ev.key.button    = remapped_key;
	if (!(remapped_key & 0xFFFFFF00))
		remapped_key = toupper(remapped_key);
	ev.key.label     = remapped_key;
	giiPostEvent(src, &ev);
}

static void
GII_vnc_pointer(void *arg, uint8_t mask, uint16_t x, uint16_t y)
{
	struct gii_source *src = arg;
	vnc_priv *priv = VNC_PRIV(src);
	gii_event ev;
	uint8_t same = ~(mask ^ priv->buttons);
	int btn;

	giiEventBlank(&ev, sizeof(gii_pmove_event));

	ev.any.size       = sizeof(gii_pmove_event);
	ev.any.type       = evPtrAbsolute;
	ev.any.origin     = priv->origin;
	ev.pmove.x        = x;
	ev.pmove.y        = y;
	giiPostEvent(src, &ev);

	if (mask & ~same & (3 << 3)) {
		giiEventBlank(&ev, sizeof(gii_pmove_event));
		ev.any.size       = sizeof(gii_pmove_event);
		ev.any.type       = evPtrRelative;
		ev.any.origin     = priv->origin;
		if (mask & ~same & (1 << 3))
			ev.pmove.wheel += 120;
		if (mask & ~same & (1 << 4))
			ev.pmove.wheel -= 120;
		giiPostEvent(src, &ev);
	}

	for (btn = 0; btn < 8; ++btn) {
		if (btn == 3 || btn == 4) /* wheel emulation */
			continue;

		if (same & (1 << btn))
			continue;

		giiEventBlank(&ev, sizeof(gii_pbutton_event));

		ev.any.size       = sizeof(gii_pbutton_event);
		ev.any.type       = (mask & (1 << btn)) ?
			evPtrButtonPress : evPtrButtonRelease;
		ev.any.origin     = priv->origin;
		ev.pbutton.button = btn + 1;
		giiPostEvent(src, &ev);
	}

	priv->buttons = mask;
}

#define VNC_REV16(x) ((x) = (x)<<8 | (x)>>8)
#define VNC_REV32(x)  \
	((x) = (x)<<24 | ((x)&0xff00)<<8 | ((x)&0xff0000)>>8 | (x)>>24)

static inline int
gii_translate_cmd(struct gii_source *src, uint8_t reverse,
	gii_event *gii_ev, const uint8_t *ev)
{
	if (gii_ev->cmd.size < sizeof(gii_cmd_nodata_event))
		return 0;

	memcpy(&gii_ev->cmd.code, ev + 8, 8);
	if (reverse) {
		VNC_REV32(gii_ev->cmd.code);
		VNC_REV32(gii_ev->cmd.seqnum);
	}

	switch (gii_ev->cmd.code) {
	case GII_CMDCODE_EVENTLOST:
	case GII_CMDCODE_DEVICE_CLOSE:
	case GII_CMDCODE_DEVICE_ENABLE:
	case GII_CMDCODE_DEVICE_DISABLE:
		return gii_ev->cmd.size == sizeof(gii_cmd_nodata_event);

	/*
	case GII_CMDCODE_DEVICE_INFO:
	case GII_CMDCODE_SET_REGISTER:
	case GII_CMDCODE_GET_REGISTER:
	case GII_CMDCODE_REGISTER_INFO:
	case GII_CMDCODE_VALUATOR_INFO:
	*/
	}

	return 0;
}

static int
gii_translate_event(struct gii_source *src,
	uint8_t reverse, gii_event *gii_ev, const uint8_t *ev)
{
	uint8_t size = ev[0];

	giiEventBlank(gii_ev, size + sizeof(time_t) + 8);
	memcpy(gii_ev, ev, 8);
	gii_ev->any.size += sizeof(time_t) + 8;
	gii_ev->any.error = 0;
	if (reverse)
		VNC_REV32(gii_ev->any.origin);
	gii_ev->any.target = GII_EV_TARGET_ALL;

	switch(gii_ev->any.type) {
	case evKeyPress:
	case evKeyRelease:
	case evKeyRepeat:
		if (gii_ev->key.size != sizeof(gii_key_event))
			return 0;

		memcpy(&gii_ev->key.modifiers, ev + 8, size - 8);

		if (reverse) {
			VNC_REV32(gii_ev->key.modifiers);
			VNC_REV32(gii_ev->key.sym);
			VNC_REV32(gii_ev->key.label);
			VNC_REV32(gii_ev->key.button);
		}
		return 1;

	case evPtrRelative:
	case evPtrAbsolute:
		if (gii_ev->pmove.size != sizeof(gii_pmove_event))
			return 0;

		memcpy(&gii_ev->pmove.x, ev + 8, size - 8);

		if (reverse) {
			VNC_REV32(gii_ev->pmove.x);
			VNC_REV32(gii_ev->pmove.y);
			VNC_REV32(gii_ev->pmove.z);
			VNC_REV32(gii_ev->pmove.wheel);
		}
		return 1;

	case evPtrButtonPress:
	case evPtrButtonRelease:
		if (gii_ev->pbutton.size != sizeof(gii_pbutton_event))
			return 0;

		memcpy(&gii_ev->pbutton.button, ev + 8, size - 8);

		if (reverse)
			VNC_REV32(gii_ev->pbutton.button);

		return 1;

	case evValRelative:
	case evValAbsolute:
		if (gii_ev->val.size < sizeof(gii_any_event) + 8)
			return 0;

		memcpy(&gii_ev->val.first, ev + 8, size - 8);

		if (reverse) {
			VNC_REV32(gii_ev->val.first);
			VNC_REV32(gii_ev->val.count);
		}

		if (gii_ev->val.size < 28 + 4 * gii_ev->val.count ||
			gii_ev->val.count > 32)
		{
			return 0;
		}

		if (reverse) {
			uint32_t i;
			for (i = 0; i < gii_ev->val.count; ++i)
				VNC_REV32(gii_ev->val.value[i]);
		}

		return 1;

	case evCommand:
		return gii_translate_cmd(src, reverse, gii_ev, ev);
	}

	return 0;
}

static int
gii_check_origin(struct gii_source *src, vnc_client *client, gii_event *ev)
{
	vnc_origin *origin;

	origin_FOREACH(origin, client) {
		if (origin->origin != ev->any.origin)
			continue;
		return !origin->pending;
	}
	return 0;
}

static int
gii_version(struct gii_source *src, vnc_client *client,
	uint8_t reverse, uint16_t count, const uint8_t *buffer, int size)
{
	uint16_t version;

	DPRINT("gii_version\n");

	if (count != 2)
		return -1;

	if (size < 2)
		return 0;

	memcpy(&version, buffer, 2);
	if (reverse)
		VNC_REV16(version);

	if (client->gii_version)
		return -1;

	if (version != 1)
		return -1;

	client->gii_version = version;
	return count;
}

static int
gii_create_device(struct gii_source *src, vnc_client *client,
	uint8_t reverse, uint16_t count, const uint8_t *buffer, int size)
{
	vnc_priv *priv = VNC_PRIV(src);
	vnc_origin *origin;
	struct gii_cmddata_valinfo *val = NULL;
	uint8_t buf[8] = {
		253,
		0x82,
		0, 4
	};
	int res;
	int zap = 0;

	DPRINT("gii_create_device\n");

	origin = origin_FIRST(client);
	if (origin && origin->pending) {
		DPRINT("- working on pending device\n");
		goto valinfo;
	}

	if (count < sizeof(origin->devinfo))
		return -1;

	if (size < sizeof(origin->devinfo))
		return 0;

	DPRINT("- new device\n");
	origin = malloc(sizeof(*origin));

	memcpy(&origin->devinfo, buffer, sizeof(origin->devinfo));
	buffer += sizeof(origin->devinfo);
	size -= sizeof(origin->devinfo);
	zap += sizeof(origin->devinfo);
	if (reverse) {
		VNC_REV32(origin->devinfo.vendor_id);
		VNC_REV32(origin->devinfo.product_id);
		VNC_REV32(origin->devinfo.can_generate);
		VNC_REV32(origin->devinfo.num_registers);
		VNC_REV32(origin->devinfo.num_valuators);
		VNC_REV32(origin->devinfo.num_buttons);
	}

	origin->pending = origin->devinfo.num_valuators;
	origin_INSERT(client, origin);

	if (origin->devinfo.num_valuators) {
		val = malloc(origin->devinfo.num_valuators * sizeof(*val));
		origin->valinfo = val;
	}
	else
		origin->valinfo = NULL;

	if (!origin->pending)
		goto add_device;

valinfo:
	val = origin->valinfo
		+ origin->devinfo.num_valuators - origin->pending;
	while (origin->pending) {
		int32_t phystype;

		if (size < 116) {
			DPRINT("- %d pending valinfo(s)\n", origin->pending);
			return zap;
		}

		memcpy(val, buffer, 96);
		memcpy(&phystype, buffer + 96, 4);
		memcpy(&val->SI_add, buffer + 100, 16);
		if (reverse) {
			VNC_REV32(val->number);
			VNC_REV32(val->range.min);
			VNC_REV32(val->range.center);
			VNC_REV32(val->range.max);
			VNC_REV32(phystype);
			VNC_REV32(val->SI_add);
			VNC_REV32(val->SI_mul);
			VNC_REV32(val->SI_div);
			VNC_REV32(val->SI_shift);
		}
		val->phystype = phystype;
		buffer += 116;
		size -= 116;
		zap += 116;
		--origin->pending;
		++val;
	}

add_device:
	origin->origin = giiAddDevice(src, &origin->devinfo, origin->valinfo);

	buf[4] = origin->origin >> 24;
	buf[5] = origin->origin >> 16;
	buf[6] = origin->origin >> 8;
	buf[7] = origin->origin;

	if (!origin->origin) {
		origin_REMOVE(origin);
		if (origin->valinfo)
			free(origin->valinfo);
		free(origin);
	}

	res = priv->safe_write(client->ctx, buf, sizeof(buf));
	return (res < 0) ? res : zap;
}

static int
gii_delete_device(struct gii_source *src, vnc_client *client,
	uint8_t reverse, uint16_t count, const uint8_t *buffer, int size)
{
	uint32_t device;
	vnc_origin *origin;

	DPRINT("gii_delete_device\n");

	if (count != 4)
		return -1;

	if (size < 4)
		return 0;

	memcpy(&device, buffer, 4);
	if (reverse)
		VNC_REV32(device);

	origin_FOREACH(origin, client) {
		if (origin->pending)
			continue;
		if (origin->origin == device)
			break;
	}
	if (!origin)
		return -1;

	origin_REMOVE(origin);
	giiDelDevice(src, origin->origin);
	if (origin->valinfo)
		free(origin->valinfo);
	free(origin);
	return count;
}

static int
GII_vnc_inject(void *arg, void *ctx,
	uint8_t msg, uint16_t count, const uint8_t *buffer, int size)
{
	struct gii_source *src = arg;
	vnc_client *client;
	uint8_t ev[255];
	gii_event gii_ev;
#ifdef GG_BIG_ENDIAN
	uint8_t reverse = ~msg & 0x80;
#else
	uint8_t reverse = msg & 0x80;
#endif
	int32_t c = count;
	msg &= 0x7f;

	client = find_client(src, ctx);
	if (!client)
		return -1;

	if (!client->gii_version && msg != 1)
		return -1;

	DPRINT("gii inject\n");

	switch (msg) {
	case 0: /* inject event */
		break;
	case 1: /* version */
		return gii_version(src, client, reverse, count, buffer, size);
	case 2: /* create device */
		return gii_create_device(src, client,
			reverse, count, buffer, size);
	case 3: /* delete device */
		return gii_delete_device(src, client,
			reverse, count, buffer, size);
	default: /* unknown */
		return -1;
	}

	DPRINT("inject gii event(s): bytes %d\n", c);

	if (!size)
		return 0;

	while (c > 0) {
		if (*buffer < 8 || *buffer > c)
			return -1; /* bad input */
		if (*buffer > size)
			return count - c;
		memcpy(&ev, buffer, *buffer);
		DPRINT("- event: size %d, type %d\n", ev[0], ev[1]);
		buffer += ev[0];
		size -= ev[0];
		c -= ev[0];
		if (!gii_translate_event(src, reverse, &gii_ev, ev))
			return -1;
		if (!gii_check_origin(src, client, &gii_ev))
			return -1;
		giiPostEvent(src, &gii_ev);
	}

	return count - c;
}

static int
GII_vnc_controller(void *arg, uint32_t ctl, void *data)
{
	struct gii_source *src = arg;

	switch (ctl) {
	case GII_VNC_INJECT_EVENT:
		{
			gii_event *ev = data;
			giiEventBlank(ev, 0); /* only set timestamp */
			ev->any.origin = src->origin;
			giiPostEvent(src, ev);
		}
		break;
	}

	return GGI_OK;
}

static int
GII_vnc_poll(struct gii_source *src)
{
	vnc_priv *priv = VNC_PRIV(src);
	vnc_client *client;

	DPRINT("poll(%p)\n", src);

	/* Be very careful when iterating over the clients.
	 * Calling write_client and client_data might remove
	 * this client and/or other clients from the list.
	 * When the current client has been removed, there
	 * is no record of if the next client has also been
	 * removed. So, be defensive and restart the whole
	 * iteration when the current client has been closed.
	 * Zero out nerves as we go along, so that an active
	 * nerve is only handled once when this happens.
	 */
	client = client_FIRST(priv);
	while (client) {
		if (client->wnerve) {
			client->wnerve = 0;
			if (priv->write_client(client->ctx, client->wfd)) {
				/* client has been closed, don't touch */
				client = client_FIRST(priv);
				continue;
			}
		}
		if (client->nerve) {
			client->nerve = 0;
			if (priv->client_data(client->ctx, client->fd)) {
				/* client has been closed, don't touch */
				client = client_FIRST(priv);
				continue;
			}
		}
		client = client_NEXT(client);
	}

	if (priv->sfd != -1 && priv->server_nerve)
		priv->new_client(priv->usr_ctx);

	DPRINT("poll(%p) exit\n", src);
	return GGI_OK;
}

static void
GII_vnc_close(struct gii_source *src)
{
	vnc_priv *priv = VNC_PRIV(src);

	free(priv);

	DPRINT_MISC("input-vnc: closed\n");
}

static struct gii_cmddata_devinfo devinfo =
{
	"VNC Input",				/* long device name */
	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,
	emKeyPress | emKeyRelease | emPointer,
	1,
	0,		/* all valuators */
	256,		/* buttons */
};


static int
GII_vnc_init(struct gii_source *src,
	     const char *target,
	     const char *args,
	     void *argptr)
{
	vnc_priv *priv;
	gii_vnc_arg *vncarg = argptr;

	DPRINT_LIBS("GIIdl_vnc(%p, \"%s\", \"%s\", %p) called\n", src,
		    target,
		    args ? args : "",
		    argptr);

	if (!vncarg)
		return GGI_EARGREQ;

	/* allocate private stuff */
	if ((priv = malloc(sizeof(vnc_priv))) == NULL)
		return GGI_ENOMEM;

	if((priv->origin = giiAddDevice(src, &devinfo, NULL)) == 0) {
		free(priv);
		return GGI_ENOMEM;
	}

	priv->sfd          = vncarg->sfd;
	GG_LIST_INIT(&priv->clients);

	priv->new_client   = vncarg->new_client;
	priv->client_data  = vncarg->client_data;
	priv->write_client = vncarg->write_client;
	priv->safe_write   = vncarg->safe_write;
	priv->usr_ctx      = vncarg->usr_ctx;

	vncarg->add_cfd  = GII_vnc_add_cfd;
	vncarg->del_cfd  = GII_vnc_del_cfd;
	vncarg->add_cwfd = GII_vnc_add_cwfd;
	vncarg->del_cwfd = GII_vnc_del_cwfd;
	vncarg->add_crfd = GII_vnc_add_crfd;
	vncarg->del_crfd = GII_vnc_del_crfd;
	vncarg->key      = GII_vnc_key;
	vncarg->pointer  = GII_vnc_pointer;
	vncarg->inject   = GII_vnc_inject;
	vncarg->gii_ctx  = src;

	src->priv = priv;

	src->ops.poll  = GII_vnc_poll;
	src->ops.close = GII_vnc_close;

	ggSetController(src->instance.channel, GII_vnc_controller);

	if (priv->sfd != -1)
		giiSetNerveReadFD(src, &priv->server_nerve, priv->sfd);

	DPRINT_MISC("input-vnc fully up\n");

	return 0;
}


struct gii_module_source GII_vnc = {
	GG_MODULE_INIT("input-vnc", 0, 1, GII_MODULE_SOURCE),
	GII_vnc_init
};

static struct gii_module_source *_GIIdl_vnc[] = {
	&GII_vnc,
	NULL
};

EXPORTFUNC
int GIIdl_vnc(int item, void **itemptr);
int GIIdl_vnc(int item, void **itemptr) {
	struct gii_module_source ***modulesptr;
	switch(item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***)itemptr;
		*modulesptr = _GIIdl_vnc;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
