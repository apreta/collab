/* $Id: input.c,v 1.30 2008/01/18 23:46:05 cegger Exp $
******************************************************************************

   Linux_joy: input

   Copyright (C) 1998 Andrew Apted     [andrew@ggi-project.org]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <linux/joystick.h>

#include <ggi/gii-module.h>
#include <ggi/internal/gii_debug.h>
#include <ggi/internal/gg_replace.h>
#include <ggi/gii-keyboard.h>


#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif


#define MAX_NR_JAXES		8
#define MAX_NR_JBUTTONS		32


struct joystick_hook {
	uint32_t origin;
	uint32_t nerve;
	int fd;

	unsigned char num_axes;
	unsigned char num_buttons;

	int axes[MAX_NR_JAXES];
	char buttons[MAX_NR_JBUTTONS];

};

#define JOYSTICK_HOOK(src)	((struct joystick_hook *) src->priv)


/* ---------------------------------------------------------------------- */


static struct gii_cmddata_devinfo linux_joy_devinfo = {
	"Linux Joystick",	/* long device name */
	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,
	emKey | emValuator,	/* can_generate */
	1,			/* num registers */
	0,			/* num_axes (only for valuators) */
	0			/* num_buttons (filled in runtime) */
};

static struct gii_cmddata_valinfo linux_joy_valinfo[MAX_NR_JAXES] = {
	{0,			/* valuator number */
	 "Side to side",	/* long valuator name */
	 "x",			/* shorthand */
	 {-32767, 0, +32767},	/* range */
	 GII_PT_LENGTH,		/* phystype */
	 0, 1, 1, -8		/* SI constants (approx.) */
	 },
	{1,			/* valuator number */
	 "Forward and back",	/* long valuator name */
	 "y",			/* shorthand */
	 {-32767, 0, +32767},	/* range */
	 GII_PT_LENGTH,		/* phystype */
	 0, 1, 1, -8		/* SI constants (approx.) */
	 },

	/* This last one is used for all extra axes */

	{2,			/* changed later */
	 "Axis ",		/* appended to later */
	 "n",			/* appended to later */
	 {-32767, 0, +32767},	/* range */
	 GII_PT_LENGTH,		/* phystype */
	 0, 1, 1, -8		/* SI constants (approx.) */
	 },
};



static int GII_joystick_init(struct gii_source *src, const char *filename)
{
	struct joystick_hook *priv;
	int version;
	int fd;
	char name[128];

	/* open the device file */

	fd = open(filename, O_RDONLY);

	if (fd < 0) {
		perror("Linux_joy: Couldn't open joystick device");
		return GGI_ENODEVICE;
	}

	/* get version and get name */

	if (ioctl(fd, JSIOCGVERSION, &version) < 0) {
		perror("Linux_joy: Couldn't read version:");
		version = 0;
	}

	DPRINT("Linux_joy: Joystick driver version %d.%d.%d\n",
	       (version >> 16) & 0xff, (version >> 8) & 0xff,
	       version & 0xff);

	if (version < 0x010000) {
		fprintf(stderr, "Linux_joy: Sorry, only driver versions "
			">= 1.0.0 supported.\n");
		close(fd);
		return GGI_ENODEVICE;
	}

	if (ioctl(fd, JSIOCGNAME(sizeof(name)), name) < 0) {
		ggstrlcpy(name, "Unknown", sizeof(name));
	}

	DPRINT("Linux_joy: Joystick driver name `%s'.\n", name);

	/* allocate joystick hook */

	priv = calloc(1, sizeof(struct joystick_hook));
	if (priv == NULL) {
		close(fd);
		return GGI_ENOMEM;
	}


	/* get number of axes and buttons */

	if (ioctl(fd, JSIOCGAXES, &priv->num_axes) ||
	    ioctl(fd, JSIOCGBUTTONS, &priv->num_buttons)) {
		perror("Linux_joy: error getting axes/buttons");
		close(fd);
		free(priv);
		return GGI_ENODEVICE;
	}

	DPRINT("Linux_joy: Joystick has %d axes.\n", priv->num_axes);
	DPRINT("Linux_joy: Joystick has %d buttons.\n", priv->num_buttons);

	if (priv->num_axes > MAX_NR_JAXES) {
		priv->num_axes = MAX_NR_JAXES;
	}
	if (priv->num_buttons > MAX_NR_JBUTTONS) {
		priv->num_buttons = MAX_NR_JBUTTONS;
	}

	linux_joy_devinfo.num_valuators = priv->num_axes;
	linux_joy_devinfo.num_buttons = priv->num_buttons;

	priv->fd = fd;
	src->priv = priv;

	DPRINT("Linux_joy: init OK.\n");

	return 0;
}

static void GII_joystick_exit(struct gii_source *src)
{
	struct joystick_hook *priv = JOYSTICK_HOOK(src);

	close(priv->fd);
	priv->fd = -1;

	free(priv);
	src->priv = NULL;

	DPRINT("Linux_joy: exit OK.\n");
}

static inline gii_event_mask
GII_joystick_handle_data(struct gii_source *src)
{
	struct joystick_hook *priv = JOYSTICK_HOOK(src);

	struct js_event js;

	gii_event ev;
	gii_event_mask result = 0;

	unsigned int i;


	/* read the joystick packet */

	if (read(priv->fd, &js, sizeof(js)) != sizeof(js)) {
		perror("Linux_joy: Error reading joystick");
		return 0;
	}

	switch (js.type & ~JS_EVENT_INIT) {

	case JS_EVENT_AXIS:

		if (js.number > priv->num_axes)
			break;
		if (priv->axes[js.number] == js.value)
			break;

		priv->axes[js.number] = js.value;

		DPRINT_EVENTS("JOY-AXIS[%d] -> %d.\n", js.number,
			      js.value);

		giiEventBlank(&ev, sizeof(gii_val_event));

		ev.any.type = evValAbsolute;
		ev.any.size = sizeof(gii_val_event);
		ev.any.origin = src->origin;
		ev.val.first = 0;
		ev.val.count = priv->num_axes;

		for (i = 0; i < ev.val.count; i++) {
			ev.val.value[i] = priv->axes[i];
		}

		giiPostEvent(src, &ev);

		result |= emValAbsolute;
		break;

	case JS_EVENT_BUTTON:

		if (js.number > priv->num_buttons)
			break;
		if (priv->buttons[js.number] == js.value)
			break;

		priv->buttons[js.number] = js.value;

		DPRINT_EVENTS("JOY-BUTTON[%d] -> %d.\n", js.number,
			      js.value);

		giiEventBlank(&ev, sizeof(gii_key_event));

		ev.any.type = js.value ? evKeyPress : evKeyRelease;
		ev.any.size = sizeof(gii_key_event);
		ev.any.origin = src->origin;
		ev.key.modifiers = 0;
		ev.key.button = 1 + js.number;
		ev.key.sym = GIIK_VOID;
		ev.key.label = GIIK_VOID;

		giiPostEvent(src, &ev);

		result |= (1 << ev.any.type);
		break;

	default:
		DPRINT_EVENTS("JOY: unknown event from driver "
			      "(0x%02x)\n", js.type);
		break;
	}

	return result;
}

static int GII_joystick_poll(struct gii_source *src)
{
	DPRINT_EVENTS("GII_joystick_poll(%p, %p) called\n", src);

	GII_joystick_handle_data(src);
	return GII_OP_POLLAGAIN;
}


/* ---------------------------------------------------------------------- */


static void GII_linux_joy_close(struct gii_source *src)
{
	DPRINT_MISC("Linux_joy close\n");

	if (JOYSTICK_HOOK(src)) {
		GII_joystick_exit(src);
	}

	return;
}


static int
GII_linux_joy_init(struct gii_source *src,
		   const char *target, const char *args, void *argptr)
{
	int ret;
	const char *filename = "/dev/js0";
	struct joystick_hook *priv;

	DPRINT_MISC("linux_joy starting.(args=\"%s\",argptr=%p)\n", args,
		    argptr);

	/* Initialize */

	if (args && *args) {
		filename = args;
	}

	/* allocate private stuff */
	priv = calloc(1, sizeof(struct joystick_hook));
	if (priv == NULL) {
		return GGI_ENOMEM;
	}

	priv->origin = giiAddDevice(src, &linux_joy_devinfo,
				    linux_joy_valinfo);
	if (priv->origin == 0) {
		free(priv);
		return GGI_ENOMEM;
	}

	ret = GII_joystick_init(src, filename);
	if (ret < 0) {
		return ret;
	}

	src->ops.poll = GII_joystick_poll;
	src->ops.close = GII_linux_joy_close;

	/*
	   src->devinfo    = &xdevinfo;  FIXME ! Length ! 
	   xdevinfo.origin = src->origin;
	 */

	if (giiSetNerveReadFD(src, &priv->nerve, priv->fd) < 0) {
		GII_linux_joy_close(src);
		return GGI_ENODEVICE;
	}

	DPRINT_MISC("linux_joy fully up\n");

	return 0;
}


struct gii_module_source GII_linux_joy = {
	GG_MODULE_INIT("input-linux-joy", 0, 1, GII_MODULE_SOURCE),
	GII_linux_joy_init
};

static struct gii_module_source *_GIIdl_linux_joy[] = {
	&GII_linux_joy,
	NULL
};


EXPORTFUNC int GIIdl_linux_joy(int item, void **itemptr);
int GIIdl_linux_joy(int item, void **itemptr)
{
	struct gii_module_source ***modulesptr;
	switch (item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***) itemptr;
		*modulesptr = _GIIdl_linux_joy;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
