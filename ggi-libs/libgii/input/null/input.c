/* $Id: input.c,v 1.17 2008/03/05 12:58:33 pekberg Exp $
******************************************************************************

   Input-null: all-in-one-file.
   
   This is a driver for the "null" device. It never generates any event 
   itself. However it might be useful for things that demand to be handed
   a gii_input_t.
   
   Copyright (C) 1998 Andreas Beck      [becka@ggi-project.org]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/errors.h>
#include <ggi/gii-module.h>
#include <ggi/internal/gii_debug.h>
#include <string.h>

static uint32_t nerve;

static int GII_null_poll(struct gii_source *src)
{

	DPRINT_MISC("input-null %p polled\n", (void *) src);

	return GII_OP_DONE;
}

static int
GII_null_init(struct gii_source *src,
	      const char *target, const char *args, void *argptr)
{
	DPRINT_MISC("input-null starting. (args=%s,argptr=%p)\n", args,
		    argptr);

	if (!strcmp(args, "-tick")) {
		src->ops.poll = GII_null_poll;
		giiSetNerveTick(src, &nerve, 100);
	}

	DPRINT_MISC("input-null fully up\n");

	return GGI_OK;
}


struct gii_module_source GII_null = {
	GG_MODULE_INIT("input-null", 0, 1, GII_MODULE_SOURCE),
	GII_null_init
};

static struct gii_module_source *_GIIdl_null[] = {
	&GII_null,
	NULL
};


EXPORTFUNC int GIIdl_null(int item, void **itemptr);
int GIIdl_null(int item, void **itemptr)
{
	struct gii_module_source ***modulesptr;
	switch (item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***) itemptr;
		*modulesptr = _GIIdl_null;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
