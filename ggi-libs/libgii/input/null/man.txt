Null input
~~~~~~~~~~

.. manpage:: 7 input-null

Synopsis
--------

::

  input-null:

 
Description
-----------

This input is a module that does nothing.
