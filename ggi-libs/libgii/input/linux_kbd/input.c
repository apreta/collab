/* $Id: input.c,v 1.25 2008/01/18 23:53:00 cegger Exp $
******************************************************************************

   Linux_kbd: input via linux MEDIUMRAW mode.

   Copyright (C) 1998-1999  Andrew Apted	[andrew@ggi-project.org]
   Copyright (C) 1999       Marcus Sundberg	[marcus@ggi-project.org]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/gg.h>
#include <ggi/gii-module.h>
#include <ggi/internal/gii_debug.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <termios.h>

#include <signal.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/stat.h>

#ifdef HAVE_SYS_KD_H
#include <sys/kd.h>
#else
#include <linux/kd.h>
#endif
#ifdef HAVE_SYS_VT_H
#include <sys/vt.h>
#else
#include <linux/vt.h>
#endif
#include <linux/tty.h>
#include <linux/keyboard.h>


#include "linkey.h"

struct keyboard_hook {
	uint32_t origin;
	uint32_t nerve;

	int fd;
	int eof;

	int old_mode;
	struct termios old_termios;
	char old_kbled;

	uint8_t keydown_buf[128];
	uint32_t keydown_sym[128];
	uint32_t keydown_label[128];

	uint32_t modifiers;
	uint32_t normalmod;
	uint32_t lockedmod;
	uint32_t lockedmod2;

	unsigned char accent;
	struct kbdiacrs accent_table;

	int call_vtswitch;
	int needctrl2switch;
	int ctrlstate;
};

#define LINKBD_PRIV(inp)	((struct keyboard_hook *) inp->priv)

#define LED2MASK(x)	(((x) & LED_CAP ? GII_MOD_CAPS   : 0) | \
			 ((x) & LED_NUM ? GII_MOD_NUM    : 0) | \
			 ((x) & LED_SCR ? GII_MOD_SCROLL : 0))

#define MASK2LED(x)	(((x) & GII_MOD_CAPS   ? LED_CAP : 0) | \
			 ((x) & GII_MOD_NUM    ? LED_NUM : 0) | \
			 ((x) & GII_MOD_SCROLL ? LED_SCR : 0))


/*
******************************************************************************
 Keyboard handling
******************************************************************************
*/

static int got_stopped;
static void sighandler(int unused)
{
	got_stopped = 1;
}

static inline int
GII_keyboard_init(struct gii_source *src, const char *filename)
{
	int fd;
	struct termios tio;
	struct keyboard_hook *priv;
	void (*oldinhandler) (int);
	void (*oldouthandler) (int);

	DPRINT_MISC("Linux-kbd: opening tty\n");

	/* open the tty */
	fd = open(filename, O_RDWR);

	if (fd < 0) {
		perror("Linux-kbd: Couldn't open TTY");
		return GGI_ENODEVICE;
	}

	/* allocate keyboard hook */
	priv = calloc(1, sizeof(struct keyboard_hook));
	if (priv == NULL) {
		close(fd);
		return GGI_ENOMEM;
	}

	DPRINT_MISC("Linux-kbd: calling tcgetattr()\n");

	/* put tty into "straight through" mode.
	 */
	if (tcgetattr(fd, &priv->old_termios) < 0) {
		perror("Linux-kbd: tcgetattr failed");
	}

	tio = priv->old_termios;

	tio.c_lflag &= ~(ICANON | ECHO | ISIG);
	tio.c_iflag &= ~(ISTRIP | IGNCR | ICRNL | INLCR | IXOFF | IXON);
	tio.c_iflag |= IGNBRK;
	tio.c_cc[VMIN] = 0;
	tio.c_cc[VTIME] = 0;

	DPRINT_MISC("Linux-kbd: calling tcsetattr()\n");

	got_stopped = 0;
	oldinhandler = signal(SIGTTIN, sighandler);
	oldouthandler = signal(SIGTTOU, sighandler);
	if (tcsetattr(fd, TCSANOW, &tio) < 0) {
		perror("Linux-kbd: tcsetattr failed");
	}
	signal(SIGTTIN, oldinhandler);
	signal(SIGTTOU, oldouthandler);

	if (got_stopped) {
		/* We're a background process on this tty... */
		fprintf(stderr,
			"Linux-kbd: can't be run in the background!\n");
		free(priv);
		close(fd);
		return GGI_EUNKNOWN;
	}

	/* Put the keyboard into MEDIUMRAW mode.  Despite the name, this
	 * is really "mostly raw", with the kernel just folding long
	 * scancode sequences (e.g. E0 XX) onto single keycodes.
	 */
	DPRINT_MISC("Linux-kbd: going to MEDIUMRAW mode\n");
	if (ioctl(fd, KDGKBMODE, &priv->old_mode) < 0) {
		perror("Linux-kbd: couldn't get mode");
		priv->old_mode = K_XLATE;
	}
	if (ioctl(fd, KDSKBMODE, K_MEDIUMRAW) < 0) {
		perror("Linux-kbd: couldn't set raw mode");
		tcsetattr(fd, TCSANOW, &(priv->old_termios));
		close(fd);
		free(priv);
		return GGI_ENODEVICE;
	}

	priv->fd = fd;
	priv->eof = 0;
	priv->call_vtswitch = 1;
	memset(priv->keydown_buf, 0, sizeof(priv->keydown_buf));

	if (ioctl(fd, KDGKBLED, &priv->old_kbled) != 0) {
		perror("Linux-kbd: unable to get keylock status");
		priv->old_kbled = 0x7f;
		priv->lockedmod = 0;
	} else {
		priv->lockedmod = LED2MASK(priv->old_kbled);
	}
	/* Make sure LEDs match the flags */
	ioctl(priv->fd, KDSETLED, 0x80);

	priv->normalmod = 0;
	priv->modifiers = priv->lockedmod | priv->normalmod;
	priv->lockedmod2 = priv->lockedmod;

	if (ioctl(fd, KDGKBDIACR, &priv->accent_table) != 0) {
		priv->accent_table.kb_cnt = 0;
	} else {
		unsigned int i;
		for (i = 0; i < priv->accent_table.kb_cnt; i++) {
			switch (priv->accent_table.kbdiacr[i].diacr) {
			case '"':
				priv->accent_table.kbdiacr[i].diacr =
				    GIIUC_Diaeresis;
				break;
			case '\'':
				priv->accent_table.kbdiacr[i].diacr =
				    GIIUC_Acute;
				break;
			}
		}
	}
	priv->accent = 0;

	if (getenv("GII_CTRLALT_VTSWITCH")) {
		priv->needctrl2switch = 1;
		priv->ctrlstate = 0;
	} else {
		priv->needctrl2switch = 0;
		priv->ctrlstate = 1;
	}
	src->priv = priv;

	DPRINT_MISC("Linux-kbd: init OK.\n");

	return 0;
}


static inline gii_event_mask
GII_keyboard_flush_keys(struct gii_source *src)
{
	struct keyboard_hook *priv = LINKBD_PRIV(src);
	gii_event_mask rc = 0;
	gii_event ev;
	int code;

	for (code = 0; code < 128; code++) {

		if (!priv->keydown_buf[code])
			continue;
		priv->keydown_buf[code] = 0;

		if (!(src->reqmask & emKeyRelease))
			continue;

		/* Send a key-release event */
		giiEventBlank(&ev, sizeof(gii_key_event));

		ev.any.type = evKeyRelease;
		ev.any.size = sizeof(gii_key_event);
		ev.any.origin = src->origin;

		ev.key.button = code;
		ev.key.sym = priv->keydown_sym[code];
		ev.key.label = priv->keydown_label[code];
		ev.key.modifiers = priv->modifiers;

		DPRINT_EVENTS("Linux-kbd: flushing key: button=0x%02x "
			      "modifiers=0x%02x sym=0x%04x label=0x%04x.\n",
			      ev.key.button, ev.key.modifiers, ev.key.sym,
			      ev.key.label);

		giiPostEvent(src, &ev);

		rc |= emKeyRelease;
	}

	priv->normalmod = 0;
	priv->modifiers = priv->lockedmod;

	if (priv->needctrl2switch) {
		priv->ctrlstate = 0;
	}

	return rc;
}


static inline void
handle_accent(struct keyboard_hook *priv, int symval, gii_event * ev)
{
	unsigned char diacr = priv->accent;
	unsigned int i;

	for (i = 0; i < priv->accent_table.kb_cnt; i++) {
		if (priv->accent_table.kbdiacr[i].diacr == diacr &&
		    priv->accent_table.kbdiacr[i].base == ev->key.sym) {
			ev->key.sym = priv->accent_table.kbdiacr[i].result;
			break;
		}
	}
	if (ev->key.sym == GIIUC_Space)
		ev->key.sym = priv->accent;

	priv->accent = 0;
}

static inline void
handle_modifier(struct keyboard_hook *priv, gii_event * ev)
{
	uint32_t mask, old_label;

	/* Handle AltGr properly */
	if (ev->key.label == GIIK_AltR) {
		if (ev->key.sym == GIIK_VOID) {
			ev->key.sym = GIIK_AltGr;
		}
		mask = 1 << (ev->key.sym & GII_KM_MASK);
	} else {
		mask = 1 << (ev->key.label & GII_KM_MASK);
	}
	/* Handle CapsShift properly: shift keys undo CapsLock */
	if ((ev->key.label == GIIK_ShiftL || ev->key.label == GIIK_ShiftR)
	    && ev->key.sym == GIIK_CapsLock) {
		if (priv->lockedmod & GII_MOD_CAPS) {
			old_label = ev->key.label;
			ev->key.label = GIIK_CapsLock;
			handle_modifier(priv, ev);
			ev->key.label = old_label;
		}
		ev->key.sym = GIIK_Shift;
	}

	if (GII_KVAL(ev->key.label) & GII_KM_LOCK) {
		if (ev->key.type == evKeyPress) {
			if (!(priv->lockedmod & mask)) {
				priv->lockedmod |= mask;
				ioctl(priv->fd, KDSKBLED,
				      MASK2LED(priv->lockedmod));
			} else {
				ev->key.sym = GIIK_VOID;
			}
		} else {
			if ((priv->lockedmod & mask)) {
				if (!(priv->lockedmod2 & mask)) {
					priv->lockedmod2 |= mask;
					ev->key.sym = GIIK_VOID;
				} else {
					priv->lockedmod2 &= ~mask;
					priv->lockedmod &= ~mask;
					ioctl(priv->fd, KDSKBLED,
					      MASK2LED(priv->lockedmod));
				}
			}
		}
	} else {
		if (ev->key.type == evKeyRelease) {
			priv->normalmod &= ~mask;
		} else {
			priv->normalmod |= mask;
		}
	}
	priv->modifiers = priv->lockedmod | priv->normalmod;
}


static inline gii_event_mask
GII_keyboard_handle_data(struct gii_source *src, int code)
{
	struct keyboard_hook *priv = LINKBD_PRIV(src);
	gii_event ev;
	struct kbentry entry;
	int symval, labelval;
	gii_event_mask mask;

	giiEventBlank(&ev, sizeof(gii_key_event));

	if (code & 0x80) {
		code &= 0x7f;
		ev.key.type = evKeyRelease;
		priv->keydown_buf[code] = 0;
	} else if (priv->keydown_buf[code] == 0) {
		ev.key.type = evKeyPress;
		priv->keydown_buf[code] = 1;

	} else {
		ev.key.type = evKeyRepeat;
	}
	ev.key.button = code;
	/* First update modifiers here so linkey.c can use the value */
	ev.key.modifiers = priv->modifiers;

	if (ev.key.type == evKeyRelease &&
	    GII_KTYP(priv->keydown_sym[code]) != GII_KT_MOD &&
	    priv->keydown_sym[code] != GIIK_VOID) {
		/* We can use the cached values */
		ev.key.sym = priv->keydown_sym[code];
		ev.key.label = priv->keydown_label[code];
	} else {
		/* Temporarily switch back to the old mode because
		   unicodes aren't available through table lookup in MEDIUMRAW
		 */
		if (ioctl(priv->fd, KDSKBMODE, priv->old_mode) < 0) {
			DPRINT_MISC
			    ("Linux-kbd: ioctl(KDSKBMODE) failed.\n");
			return 0;
		}
		/* Look up the keysym without modifiers, which will give us
		 * the key label (more or less).
		 */
		entry.kb_table = 0;
		entry.kb_index = code;
		if (ioctl(priv->fd, KDGKBENT, &entry) < 0) {
			DPRINT_MISC
			    ("Linux-kbd: ioctl(KDGKBENT) failed.\n");
			return 0;
		}
		labelval = entry.kb_value;
		if (priv->old_mode == K_UNICODE)
			labelval &= 0x0fff;

		/* Now look up the full keysym in the kernel's table */

		/* calculate kernel-like shift value */
		entry.kb_table =
		    ((priv->
		      modifiers & GII_MOD_SHIFT) ? (1 << KG_SHIFT) : 0) |
		    ((priv->
		      modifiers & GII_MOD_CTRL) ? (1 << KG_CTRL) : 0) |
		    ((priv->
		      modifiers & GII_MOD_ALT) ? (1 << KG_ALT) : 0) |
		    ((priv->
		      modifiers & GII_MOD_ALTGR) ? (1 << KG_ALTGR) : 0) |
		    ((priv->
		      modifiers & GII_MOD_META) ? (1 << KG_ALT) : 0) |
		    ((priv->
		      modifiers & GII_MOD_CAPS) ? (1 << KG_CAPSSHIFT) : 0);

		entry.kb_index = code;
		if (ioctl(priv->fd, KDGKBENT, &entry) < 0) {
			DPRINT_MISC
			    ("Linux-kbd: ioctl(KDGKBENT) failed.\n");
			return 0;
		}

		/* Switch back to MEDIUMRAW */
		if (ioctl(priv->fd, KDSKBMODE, K_MEDIUMRAW) < 0) {
			DPRINT_MISC
			    ("Linux-kbd: ioctl(KDSKBMODE) failed.\n");
			return 0;
		}

		switch (entry.kb_value) {

		case K_HOLE:
			DPRINT_EVENTS("Linux-kbd: NOSUCHKEY\n");
			break;

		case K_NOSUCHMAP:
			DPRINT_EVENTS("Linux-kbd: NOSUCHMAP\n");
			entry.kb_value = K_HOLE;
			break;
		}
		symval = entry.kb_value;
		if (priv->old_mode == K_UNICODE)
			symval &= 0x0fff;

		_gii_linkey_trans(code, labelval, symval, &ev.key);

		if (ev.key.type == evKeyPress) {
			if (priv->accent) {
				if (GII_KTYP(ev.key.sym) != GII_KT_MOD) {
					handle_accent(priv, symval, &ev);
				}
			} else if (KTYP(symval) == KT_DEAD) {
				priv->accent = GII_KVAL(ev.key.sym);
			}
		}
		if (GII_KTYP(ev.key.sym) == GII_KT_DEAD) {
			ev.key.sym = GIIK_VOID;
		}
	}

	/* Keep track of modifier state */
	if (GII_KTYP(ev.key.label) == GII_KT_MOD) {
		/* Modifers don't repeat */
		if (ev.key.type == evKeyRepeat)
			return 0;

		handle_modifier(priv, &ev);
	}
	/* Now update modifiers again to get the value _after_ the current
	   event */
	ev.key.modifiers = priv->modifiers;

	if (ev.any.type == evKeyPress) {
		priv->keydown_sym[code] = ev.key.sym;
		priv->keydown_label[code] = ev.key.label;
	}

	DPRINT_EVENTS("KEY-%s button=0x%02x modifiers=0x%02x "
		      "sym=0x%04x label=0x%04x\n",
		      (ev.key.type == evKeyRelease) ? "UP" :
		      ((ev.key.type == evKeyPress) ? "DN" : "RP"),
		      ev.key.button, ev.key.modifiers,
		      ev.key.sym, ev.key.label);

	if (priv->call_vtswitch) {
		if (ev.key.label == GIIK_CtrlL && priv->needctrl2switch) {
			if (ev.key.type == evKeyRelease) {
				priv->ctrlstate = 0;
			} else if (ev.key.type == evKeyPress) {
				priv->ctrlstate = 1;
			}
		}
		/* Check for console switch.  Unfortunately, the kernel doesn't
		 * recognize KT_CONS when the keyboard is in RAW or MEDIUMRAW
		 * mode, so _we_ have to.  Sigh.
		 */
		if ((ev.key.type == evKeyPress) &&
		    (KTYP(entry.kb_value) == KT_CONS) && priv->ctrlstate) {
			int rc;
			int new_cons = 1 + KVAL(entry.kb_value);

			/* Clear the keydown buffer, since we'll never know
			   what keys the user pressed (or released) while they
			   were away.
			 */
			DPRINT_MISC("Flushing all keys.\n");
			rc = GII_keyboard_flush_keys(src);

			DPRINT_MISC("Switching to console %d.\n",
				    new_cons);

			if (ioctl(priv->fd, VT_ACTIVATE, new_cons) < 0) {
				perror("ioctl(VT_ACTIVATE)");
			}

			return rc;
		}
	}

	mask = (1 << ev.any.type);

	if (!(src->reqmask & mask))
		return 0;

	/* finally queue the key event */
	ev.any.size = sizeof(gii_key_event);
	ev.any.origin = src->origin;

	giiPostEvent(src, &ev);

	return (1 << ev.any.type);
}


static int GII_keyboard_poll(struct gii_source *src)
{
	struct keyboard_hook *priv = LINKBD_PRIV(src);
	unsigned char buf[256];
	gii_event_mask result = 0;
	int readlen, i;

	DPRINT_EVENTS("GII_keyboard_poll(%p, %p) called\n", src);

	if (priv->eof) {
		/* End-of-file, don't do any polling */
		return GII_OP_CLOSE;
	}

	/* Read keyboard data */
	while ((readlen = read(priv->fd, buf, 256)) > 0) {
		for (i = 0; i < readlen; i++) {
			result |= GII_keyboard_handle_data(src, buf[i]);
		}
		if (readlen != 256)
			break;
	}

	if (readlen < 0) {
		/* Error, we try again next time */
		perror("Linux-kbd: Error reading keyboard");
	}

	return GII_OP_DONE;
}


/* ---------------------------------------------------------------------- */

static struct gii_cmddata_devinfo devinfo = {
	"Linux Keyboard",	/* long device name */
	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,
	emKey,			/* can_generate */
	1,			/* num registers */
	0,			/* num_axes */
	128			/* num_buttons */
};

/* ---------------------------------------------------------------------- */


static void GII_lin_kbd_close(struct gii_source *src)
{
	struct keyboard_hook *priv = LINKBD_PRIV(src);

	DPRINT_MISC("Linux-kbd cleanup\n");

	if (priv == NULL) {
		/* Make sure we're only called once */
		return;
	}

	if (priv->old_kbled != 0x7f) {
		ioctl(priv->fd, KDSKBLED, priv->old_kbled);
	}

	if (ioctl(priv->fd, KDSKBMODE, priv->old_mode) < 0) {
		perror("Linux-kbd: couldn't restore mode");
	}

	if (tcsetattr(priv->fd, TCSANOW, &priv->old_termios) < 0) {
		perror("Linux-kbd: tcsetattr failed");
	}

	close(priv->fd);

	free(priv);
	src->priv = NULL;

	ggUnregisterCleanup((ggcleanup_func *) GII_lin_kbd_close, src);

	fputc('\n', stderr);
	fflush(stderr);

	DPRINT("Linux-kbd: exit OK.\n");

	return;
}

static int
GII_linux_kbd_init(struct gii_source *src,
		   const char *target, const char *args, void *argptr)
{
	const char *filename = "/dev/tty";
	struct keyboard_hook *priv;

	DPRINT_LIBS("linux_kbd starting.(args=\"%s\",argptr=%p)\n",
		    args, argptr);

	/* Initialize */
	if (args && *args) {
		filename = args;
	}

	/* allocate private stuff */
	priv = calloc(1, sizeof(struct keyboard_hook));
	if (priv == NULL) {
		return GGI_ENOMEM;
	}

	priv->origin = giiAddDevice(src, &devinfo, NULL);
	if (priv->origin == 0) {
		free(priv);
		return GGI_ENOMEM;
	}

	if (GII_keyboard_init(src, filename) < 0) {
		return GGI_ENODEVICE;
	}

	if (giiSetNerveReadFD(src, &priv->nerve, priv->fd) < 0) {
		GII_lin_kbd_close(src);
		return GGI_ENODEVICE;
	}

	src->ops.poll = GII_keyboard_poll;
	src->ops.close = GII_lin_kbd_close;

	DPRINT_MISC("linux_kbd fully up\n");

	return 0;
}


struct gii_module_source GII_linux_kbd = {
	GG_MODULE_INIT("input-linux-kbd", 0, 1, GII_MODULE_SOURCE),
	GII_linux_kbd_init
};

static struct gii_module_source *_GIIdl_linux_kbd[] = {
	&GII_linux_kbd,
	NULL
};


EXPORTFUNC int GIIdl_linux_kbd(int item, void **itemptr);
int GIIdl_linux_kbd(int item, void **itemptr)
{
	struct gii_module_source ***modulesptr;
	switch (item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***) itemptr;
		*modulesptr = _GIIdl_linux_kbd;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
