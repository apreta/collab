/* $Id: eventparse.c,v 1.1 2008/05/19 18:24:26 ggibecka Exp $
******************************************************************************

   Linux wiimote parser

   Copyright (C) 2008 Andreas Beck	[becka-ggi@bedatec.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include "ggi_cwiid.h"
#include <ggi/gii-keyboard.h>
#include <errno.h>


#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

/* This is a very dumb "forward events from pipe" routine.
 */
int GII_cwiid_poll(struct gii_source *src)
{
	gii_cwiid_priv *priv = src->priv;
	gii_event ev;
	int read_len;

	DPRINT_EVENTS("GII_cwiid_poll(%p) called\n", src);

	/* read events from the read end of the pipefds and post them */
	read_len = read(priv->pipefds[0], &ev, sizeof(ev));

	if (read_len <= 0) {
		/* Error, we try again next time */
		if (errno == EAGAIN) return 0; /* Don't print warning here */
		perror("GII_cwiid_poll: Error reading events");
		return GII_OP_DONE;
	}
	if (read_len != sizeof(gii_event)) {
		perror("GII_cwiid_poll: Error reading events - short read");
		return GII_OP_DONE;
	}

	/* Dispatch event */
	giiPostEvent(src, &ev);

	return GII_OP_DONE;
}
