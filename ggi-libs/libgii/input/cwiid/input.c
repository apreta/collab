/* $Id: input.c,v 1.1 2008/05/19 18:24:28 ggibecka Exp $
******************************************************************************

   Linux wiimote inputlib

   Copyright (C) 2008 Andreas Beck	[becka-ggi@bedatec.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include "ggi_cwiid.h"

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>

#include <ggi/gg-queue.h>

#define WIIMOTE_NUM_REGISTERS 3
/* we do not try to replicate the actual registers but rather a set of
 * pseudo-registers that can be used to control useful features.
 * Reg #0: Reporting mode - bitmask of which parts of the controller to
 *         enable
 * Reg #1: Setting Player LEDs
 * Reg #2: Rumble
 */

/* Valuator enumeration:
 * The wiimote itself features 3 accelerometers
 * I also model the 2 coordinates and the strength indicator 
 * of the IR camera as valuators. One might also consider them pointers, but
 * few apps would expect up to 4 pointers.
 * The nunchuk has a 2 axis stick and 3 accelerometers
 * The classic controller has 2 2 axis sticks plus those 2 l/r things (??)
 * Sums up to a whopping total of 26 valuators. 
 */
#define WIIMOTE_NUM_VALUATORS 26

/* 11 buttons on the Wiimote, 2 on the Nunchuk, 15 on the classic. 
 * However WRT button numbers we set them at 0-10, 16/17 and 32-46
 * so we better report 48 total
 */
#define WIIMOTE_NUM_BUTTONS 48

static struct gii_cmddata_devinfo wiimote_devinfo = {
	"WIImote",		/* device name */
	0x057e,			/* original Wiimote SDP IDs */
	0x0306,
	emKey | emValuator,
	WIIMOTE_NUM_REGISTERS,
	WIIMOTE_NUM_VALUATORS, 
	WIIMOTE_NUM_BUTTONS
};

#define VALNUM_ACCEL_X 0
#define VALNUM_ACCEL_Y 1
#define VALNUM_ACCEL_Z 2

#define VALNUM_IR1_X 3
#define VALNUM_IR1_Y 4
#define VALNUM_IR1_S 5

#define VALNUM_IR_GAPTONEXT 3

#define VALNUM_IR2_X (VALNUM_IR1_X+VALNUM_IR_GAPTONEXT)
#define VALNUM_IR2_Y (VALNUM_IR1_Y+VALNUM_IR_GAPTONEXT)
#define VALNUM_IR2_S (VALNUM_IR1_S+VALNUM_IR_GAPTONEXT)

#define VALNUM_IR3_X (VALNUM_IR2_X+VALNUM_IR_GAPTONEXT)
#define VALNUM_IR3_Y (VALNUM_IR2_Y+VALNUM_IR_GAPTONEXT)
#define VALNUM_IR3_S (VALNUM_IR2_S+VALNUM_IR_GAPTONEXT)

#define VALNUM_IR4_X (VALNUM_IR3_X+VALNUM_IR_GAPTONEXT)
#define VALNUM_IR4_Y (VALNUM_IR3_Y+VALNUM_IR_GAPTONEXT)
#define VALNUM_IR4_S (VALNUM_IR3_S+VALNUM_IR_GAPTONEXT)

#define VALNUM_NUNCHUK_BASE 15
#define VALNUM_NUNCHUK_ACCEL_X (VALNUM_NUNCHUK_BASE+0)
#define VALNUM_NUNCHUK_ACCEL_Y (VALNUM_NUNCHUK_ACCEL_X+1)
#define VALNUM_NUNCHUK_ACCEL_Z (VALNUM_NUNCHUK_ACCEL_X+2)
#define VALNUM_NUNCHUK_STICK_X (VALNUM_NUNCHUK_BASE+3)
#define VALNUM_NUNCHUK_STICK_Y (VALNUM_NUNCHUK_STICK_X+1)

#define VALNUM_CLASSIC_LSTICK_X 20
#define VALNUM_CLASSIC_LSTICK_Y (VALNUM_CLASSIC_LSTICK_X+1)

#define VALNUM_CLASSIC_RSTICK_X (VALNUM_CLASSIC_LSTICK_X+2)
#define VALNUM_CLASSIC_RSTICK_Y (VALNUM_CLASSIC_RSTICK_X+1)

static struct gii_cmddata_valinfo wiimote_valinfo[WIIMOTE_NUM_VALUATORS] = {
	/* First the accelerometers of the wiimote itself */
	{ VALNUM_ACCEL_X,	/* valuator number */
	  "side to side accel",	/* long valuator name */
	  "ax",			/* shorthand */
	  { 0, CWIID_ACC_MAX/2, CWIID_ACC_MAX },	/* range */
	  GII_PT_ACCELERATION,	/* phystype */
	  -CWIID_ACC_MAX/2, 981, 2500, 0	
	  			/* SI constants, defaults to about 
	  			 * 9.81m/s^2 / 25 value units 
	  			 * (changed by calibration data) 
	  			 */
	},
	{ VALNUM_ACCEL_Y,	/* valuator # */
	  "front to back accel",/* longname */
	  "ay",			/* shorthand */
	  { 0, CWIID_ACC_MAX/2, CWIID_ACC_MAX },	/* range */
	  GII_PT_ACCELERATION,	/* phystype */
	  -CWIID_ACC_MAX/2, 981, 2500, 0	/* 9.81m/s^2 / 25 value */
	},
	{ VALNUM_ACCEL_Z,			/* valuator # */
	  "up/down accel",	/* longname */
	  "az",			/* shorthand */
	  { 0, CWIID_ACC_MAX/2, CWIID_ACC_MAX },	/* range */
	  GII_PT_ACCELERATION,	/* phystype */
	  -CWIID_ACC_MAX/2, 981, 2500, 0	/* 9.81m/s^2 / 25 value */
	},
	/* next, the IR camera gives up to 4 triplets of IR source data */
	{ VALNUM_IR1_X,		/* valuator number */
	  "1st IR tracking point x",	/* long valuator name */
	  "ix1",		/* shorthand */
	  { 0, CWIID_IR_X_MAX/2, CWIID_IR_X_MAX },	/* range */
	  GII_PT_LENGTH,	/* phystype - a bit debatable */
	  -CWIID_IR_X_MAX/2, 3, 4*CWIID_IR_X_MAX, 0	
	  			/* SI constants, Model: position on a projection
	  			 * plane at 1m from the receiver. 0/0 at center.
	  			 * about .375m at full tilt. */
	},
	{ VALNUM_IR1_Y,		/* valuator number */
	  "1st IR tracking point y",	/* long valuator name */
	  "iy1",		/* shorthand */
	  { 0, CWIID_IR_Y_MAX/2, CWIID_IR_Y_MAX },	/* range */
	  GII_PT_LENGTH,	/* phystype - a bit debatable */
	  -CWIID_IR_Y_MAX/2, 3*3, 4*4*CWIID_IR_Y_MAX, 0	/* SI constants, Model: see above.
	  			 * about .28m at full tilt. */
	},
	{ VALNUM_IR1_S,		/* valuator number */
	  "1st IR tracking point strength",	/* long valuator name */
	  "is1",		/* shorthand */
	  { -2, 3, 7 },		/* range - FIXME */
	  GII_PT_POWER,		/* phystype - a bit debatable */
	  0, 1, 1, 0		/* SI constants - arbitrary */
	},
	/* second IR tracking point */
	{ VALNUM_IR2_X,		/* valuator number */
	  "2nd IR tracking point x",	/* long valuator name */
	  "ix2",		/* shorthand */
	  { 0, CWIID_IR_X_MAX/2, CWIID_IR_X_MAX },	/* range */
	  GII_PT_LENGTH,	/* phystype */
	  -CWIID_IR_X_MAX/2, 3, 4*CWIID_IR_X_MAX, 0	/* SI constants, see above */
	},
	{ VALNUM_IR2_Y,		/* valuator number */
	  "2nd IR tracking point y",	/* long valuator name */
	  "iy2",		/* shorthand */
	  { 0, CWIID_IR_Y_MAX/2, CWIID_IR_Y_MAX },	/* range */
	  GII_PT_LENGTH,	/* phystype - a bit debatable */
	  -CWIID_IR_Y_MAX/2, 3*3, 4*4*CWIID_IR_Y_MAX, 0	/* SI constants, see above. */
	},
	{ VALNUM_IR2_S,		/* valuator number */
	  "2nd IR tracking point strength",	/* long valuator name */
	  "is2",		/* shorthand */
	  { -2, 3, 7 },		/* range - FIXME */
	  GII_PT_POWER,		/* phystype */
	  0, 1, 1, 0		/* SI constants - arbitrary */
	},

	/* third IR tracking point */
	{ VALNUM_IR3_X,		/* valuator number */
	  "3rd IR tracking point x",	/* long valuator name */
	  "ix3",		/* shorthand */
	  { 0, CWIID_IR_X_MAX/2, CWIID_IR_X_MAX },	/* range */
	  GII_PT_LENGTH,	/* phystype */
	  -CWIID_IR_X_MAX/2, 3, 4*CWIID_IR_X_MAX, 0	/* SI constants, see above */
	},
	{ VALNUM_IR3_Y,		/* valuator number */
	  "3rd IR tracking point y",	/* long valuator name */
	  "iy3",		/* shorthand */
	  { 0, CWIID_IR_Y_MAX/2, CWIID_IR_Y_MAX },	/* range */
	  GII_PT_LENGTH,	/* phystype - a bit debatable */
	  -CWIID_IR_Y_MAX/2, 3*3, 4*4*CWIID_IR_Y_MAX, 0	/* SI constants, see above. */
	},
	{ VALNUM_IR3_S,		/* valuator number */
	  "3rd IR tracking point strength",	/* long valuator name */
	  "is3",		/* shorthand */
	  { -2, 3, 7 },		/* range - FIXME */
	  GII_PT_POWER,		/* phystype */
	  0, 1, 1, 0		/* SI constants - arbitrary */
	},

	/* forth IR tracking point */
	{ VALNUM_IR4_X,		/* valuator number */
	  "4th IR tracking point x",	/* long valuator name */
	  "ix4",		/* shorthand */
	  { 0, CWIID_IR_X_MAX/2, CWIID_IR_X_MAX },	/* range */
	  GII_PT_LENGTH,	/* phystype */
	  -CWIID_IR_X_MAX/2, 3, 4*CWIID_IR_X_MAX, 0	/* SI constants, see above */
	},
	{ VALNUM_IR4_Y,		/* valuator number */
	  "4th IR tracking point y",	/* long valuator name */
	  "iy4",		/* shorthand */
	  { 0, CWIID_IR_Y_MAX/2, CWIID_IR_Y_MAX },	/* range */
	  GII_PT_LENGTH,	/* phystype - a bit debatable */
	  -CWIID_IR_Y_MAX/2, 3*3, 4*4*CWIID_IR_Y_MAX, 0	/* SI constants, see above. */
	},
	{ VALNUM_IR4_S,		/* valuator number */
	  "4th IR tracking point strength",	/* long valuator name */
	  "is4",		/* shorthand */
	  { -2, 3, 7 },		/* range - FIXME */
	  GII_PT_POWER,		/* phystype */
	  0, 1, 1, 0		/* SI constants - arbitrary */
	},

	/* Now for the Nunchuk */
	{ VALNUM_NUNCHUK_ACCEL_X,	/* valuator number */
	  "Nunchuk side to side accel",	/* long valuator name */
	  "nax",		/* shorthand */
	  { 0, CWIID_ACC_MAX/2, CWIID_ACC_MAX },	/* range */
	  GII_PT_ACCELERATION,	/* phystype */
	  -CWIID_ACC_MAX/2, 981, 5000, 0
	  			/* SI constants, defaults to about 
	  			 * 9.81m/s^2 / 50 value units 
	  			 * (changed by calibration data) 
	  			 */
	},
	{ VALNUM_NUNCHUK_ACCEL_Y,	/* valuator # */
	  "Nunchuk front to back accel",/* longname */
	  "nay",		/* shorthand */
	  { 0, CWIID_ACC_MAX/2, CWIID_ACC_MAX },	/* range */
	  GII_PT_ACCELERATION,	/* phystype */
	  -CWIID_ACC_MAX/2, 981, 5000, 0	/* 9.81m/s^2 / 50 value */
	},
	{ VALNUM_NUNCHUK_ACCEL_Z,	/* valuator # */
	  "Nunchuk up/down accel",	/* longname */
	  "naz",		/* shorthand */
	  { 0, CWIID_ACC_MAX/2, CWIID_ACC_MAX },	/* range */
	  GII_PT_ACCELERATION,	/* phystype */
	  -CWIID_ACC_MAX/2, 981, 5000, 0	/* 9.81m/s^2 / 50 value */
	},
	/* Now the joystick part of the Nunchuk */
	{ VALNUM_NUNCHUK_STICK_X,	/* valuator number */
	  "Nunchuk joystick x",	/* long valuator name */
	  "njx",		/* shorthand */
	  { 0, 0x80, 0xff },	/* range */
	  GII_PT_ANGLE,		/* phystype */
	  -0x80, 1, 276, 0	/* SI constants about 22� at full tilt, 
	  			 * which is about 106 values average on 
	  			 * my unit.*/
	},
	{ VALNUM_NUNCHUK_STICK_Y,	/* valuator number */
	  "Nunchuk joystick y",	/* long valuator name */
	  "njy",		/* shorthand */
	  { 0, 0x80, 0xff },	/* range */
	  GII_PT_ANGLE,		/* phystype - a bit debatable */
	  -0x80, 1, 276, 0	/* SI constants, see above. */
	},
	/* Now the classic controller values - left joystick */
	/* FIXME - I do not have one, so calibration values may be grossly 
	 * off. 
	 */
	{ VALNUM_CLASSIC_LSTICK_X,	/* valuator number */
	  "Classic left joystick x",	/* long valuator name */
	  "cljx",		/* shorthand */
	  { 0, CWIID_CLASSIC_L_STICK_MAX/2, CWIID_CLASSIC_L_STICK_MAX },	/* range */
	  GII_PT_ANGLE,		/* phystype */
	  -CWIID_CLASSIC_L_STICK_MAX/2, 1, 1, 0	/* FIXME SI constants � at full tilt */
	},
	{ VALNUM_CLASSIC_LSTICK_Y,	/* valuator number */
	  "Classic left joystick y",	/* long valuator name */
	  "cljy",		/* shorthand */
	  { 0, CWIID_CLASSIC_L_STICK_MAX/2, CWIID_CLASSIC_L_STICK_MAX },	/* range */
	  GII_PT_ANGLE,		/* phystype - a bit debatable */
	  -CWIID_CLASSIC_L_STICK_MAX/2, 1, 1, 0	/* SI constants, see above. */
	},
	/* Classic controller - right joystick */
	{ VALNUM_CLASSIC_RSTICK_X,	/* valuator number */
	  "Classic right joystick x",	/* long valuator name */
	  "crjx",		/* shorthand */
	  { 0, CWIID_CLASSIC_R_STICK_MAX/2, CWIID_CLASSIC_R_STICK_MAX },	/* range */
	  GII_PT_ANGLE,		/* phystype */
	  -CWIID_CLASSIC_R_STICK_MAX/2, 1, 1, 0	/* SI constants � at full tilt */
	},
	{ VALNUM_CLASSIC_RSTICK_Y,	/* valuator number */
	  "Classic right joystick y",	/* long valuator name */
	  "crjy",		/* shorthand */
	  { 0, CWIID_CLASSIC_R_STICK_MAX/2, CWIID_CLASSIC_R_STICK_MAX },	/* range */
	  GII_PT_ANGLE,		/* phystype - a bit debatable */
	  -CWIID_CLASSIC_R_STICK_MAX/2, 1, 1, 0	/* SI constants, see above. */
	},
	/* Classic controller - left/right - what is that? */
	{ 24,			/* valuator number */
	  "Classic right shoulder force",	/* long valuator name */
	  "crv",		/* shorthand */
	  { 0, 0, CWIID_CLASSIC_LR_MAX },	/* range */
	  GII_PT_FORCE,		/* phystype */
	  0, 1, 1, 0	/* SI constants - force? */
	},
	{ 25,			/* valuator number */
	  "Classic left shoulder force",	/* long valuator name */
	  "clv",		/* shorthand */
	  { 0, 0, CWIID_CLASSIC_LR_MAX },	/* range */
	  GII_PT_FORCE,		/* phystype - a bit debatable */
	  0, 1, 1, 0	/* SI constants, see above. */
	}
};

#define SKIPWHITE(str)	while (isspace((uint8_t)*(str))) (str)++

typedef struct wiilist_entry {
	GG_SLIST_ENTRY(wiilist_entry) next;
	cwiid_wiimote_t *wiihandle;
	struct gii_source *src;
	struct gii_cmddata_valinfo wiimote_valinfo[WIIMOTE_NUM_VALUATORS];
} wiilist_entry_t;

GG_SLIST_HEAD(wiilist_head, wiilist_entry) wiiroot = 
	GG_SLIST_HEAD_INITIALIZER(wiiroot);

static wiilist_entry_t *wilist_find_by_wiimote_handle(cwiid_wiimote_t *handle) 
{
	wiilist_entry_t *currmote;

	GG_SLIST_FOREACH(currmote, &wiiroot, next) {
		if (currmote->wiihandle == handle)
			break;
	}

	return currmote;
}

static void make_wii_key(gii_event *ev, struct gii_source *src, int key,
		int button_offset, key2label_func translator) 
{
	gii_cwiid_priv *priv = src->priv;

	giiEventBlank(ev, sizeof(gii_key_event));
	ev->any.type = evKeyPress;
	ev->any.size = sizeof(gii_key_event);
	ev->any.origin = priv->origin;
	ev->key.modifiers = 0;
	ev->key.button    = key + button_offset;
	ev->key.label     = translator(src,(1<<key));
	ev->key.sym       = ev->key.label;
}

static void make_wii_val(gii_event *ev, struct gii_source *src) 
{
	gii_cwiid_priv *priv = src->priv;

	giiEventBlank(ev, sizeof(gii_val_event));
	ev->any.type   = evValAbsolute;
	ev->any.size   = sizeof(gii_val_event);
	ev->any.origin = priv->origin;
}

static void dispatch_key_events(struct gii_source *src, int pipefd, 
		key2label_func translator, int button_offset,
		uint16_t *cachevalue, uint16_t newvalue)
{
	int j;
	uint16_t delta,on,off;
	gii_event ev;

	delta = *cachevalue ^ newvalue;
	on    = delta &  newvalue;
	off   = delta & ~newvalue;
	*cachevalue = newvalue;

	if (on) {
		for(j=0;j<16;j++) {

			if ( ! (on & (1 << j)) )
				continue;

			make_wii_key(&ev, src, j, button_offset, translator);
			write(pipefd, &ev, sizeof(ev));
		}
	}
	if (off) {
		for(j=0;j<16;j++) {

			if ( ! (off & (1 << j)) ) 
				continue;

			make_wii_key(&ev, src, j, button_offset, translator);
			ev.any.type = evKeyRelease;
			write(pipefd, &ev, sizeof(ev));
		}
	}
}

static void dispatch_stick_events(struct gii_source *src, int pipefd,
		int valpos,
		uint8_t *cache, uint8_t *newval) 
{
	gii_event ev;

	if (cache[0] == newval[0] && cache[1] == newval[1]) 
		return;

	cache[0] = newval[0];
	cache[1] = newval[1];

	make_wii_val(&ev, src);
	ev.val.first  = valpos;
	ev.val.count  = 2;
	ev.val.value[0] = newval[0];
	ev.val.value[1] = newval[1];
	write(pipefd, &ev, sizeof(ev));
}

static void dispatch_accel_events(struct gii_source *src, int pipefd,
		int valpos,
		uint8_t *cache, uint8_t *newval) 
{
	gii_event ev;

	if (cache[0] == newval[0] &&
	    cache[1] == newval[1] &&
	    cache[2] == newval[2])
	    	return;

	cache[0] = newval[0];
	cache[1] = newval[1];
	cache[2] = newval[2];

	make_wii_val(&ev, src);

	ev.val.first  = valpos;
	ev.val.count  = 3;
	ev.val.value[0] = newval[0];
	ev.val.value[1] = newval[1];
	ev.val.value[2] = newval[2];

	write(pipefd, &ev, sizeof(ev));
}

static void dispatch_ir_events(struct gii_source *src, int pipefd,
		int valpos,
		gii_cwiid_irstate_priv *cache, 
		uint16_t *newpos, int8_t newsize) 
{
	gii_event ev;

	if (cache->pos[0] == newpos[0] &&
	    cache->pos[1] == newpos[1] &&
	    cache->size   == newsize)
	    	return;

	cache->pos[0] = newpos[0];
	cache->pos[1] = newpos[1];
	cache->size   = newsize;

	make_wii_val(&ev, src);
	ev.val.first = valpos;
	ev.val.count = 3;
	ev.val.value[0] = newpos[0];
	ev.val.value[1] = newpos[1];
	ev.val.value[2] = newsize;

	write(pipefd, &ev, sizeof(ev));
}

static void cwiid_callback(cwiid_wiimote_t *handle, int nummsg,
                  union cwiid_mesg messages[], struct timespec *ts)
{
	int i,j;
	union cwiid_mesg *curmsg;
	wiilist_entry_t *currmote;
	gii_cwiid_priv *priv;

	/* Find GII source for this wiimote
	 */
	currmote = wilist_find_by_wiimote_handle(handle);
	if (!currmote) return;

	priv = currmote->src->priv;

	for(i = 0;i < nummsg;i++) {
		curmsg = &messages[i];
		switch(curmsg->type) {
		case CWIID_MESG_STATUS:
			DPRINT_LIBS("Have wiimote status event.\n");
			break;
		case CWIID_MESG_BTN:
			dispatch_key_events(currmote->src, priv->pipefds[1],
				GII_cwiid_key2label_wiimote, 0,
				&priv->buttonstate.wiimote,
				curmsg->btn_mesg.buttons);
			break;
		case CWIID_MESG_ACC:
			dispatch_accel_events(currmote->src, priv->pipefds[1],
				VALNUM_ACCEL_X,
				priv->as_wiimote.accels,
				curmsg->acc_mesg.acc);
			break;
		case CWIID_MESG_IR:
			for(j = 0;j < CWIID_IR_SRC_COUNT;j++) {
				if (! curmsg->ir_mesg.src[j].valid) {
					/* tag as invalid */
					curmsg->ir_mesg.src[j].size=-2;
					/* center invalid readings */
					curmsg->ir_mesg.src[j].pos[0]=CWIID_IR_X_MAX/2;
					curmsg->ir_mesg.src[j].pos[1]=CWIID_IR_Y_MAX/2;
				}
				dispatch_ir_events(currmote->src, 
					priv->pipefds[1],
					VALNUM_IR1_X+j*VALNUM_IR_GAPTONEXT,
					&priv->is_wiimote[j],
					curmsg->ir_mesg.src[j].pos,
					curmsg->ir_mesg.src[j].size);
			}
			break;
		case CWIID_MESG_NUNCHUK:
			dispatch_key_events(currmote->src, priv->pipefds[1],
				GII_cwiid_key2label_nunchuk, 16,
				&priv->buttonstate.nunchuk,
				curmsg->nunchuk_mesg.buttons);

			dispatch_stick_events(currmote->src, priv->pipefds[1],
				VALNUM_NUNCHUK_STICK_X,
				priv->ss_nunchuk.pos, 
				curmsg->nunchuk_mesg.stick);

			dispatch_accel_events(currmote->src, priv->pipefds[1],
				VALNUM_NUNCHUK_ACCEL_X,
				priv->as_nunchuk.accels,
				curmsg->nunchuk_mesg.acc);
			break;
		case CWIID_MESG_CLASSIC:
			dispatch_key_events(currmote->src, priv->pipefds[1],
				GII_cwiid_key2label_classic, 32,
				&priv->buttonstate.classic,
				curmsg->classic_mesg.buttons);

			dispatch_stick_events(currmote->src, priv->pipefds[1],
				VALNUM_CLASSIC_LSTICK_X,
				priv->ss_classic_left.pos, 
				curmsg->classic_mesg.l_stick);
			dispatch_stick_events(currmote->src, priv->pipefds[1],
				VALNUM_CLASSIC_RSTICK_X,
				priv->ss_classic_right.pos, 
				curmsg->classic_mesg.r_stick);
			/* fixme - add shoulder buttons */
			break;
		case CWIID_MESG_ERROR:
			DPRINT_LIBS("Have wiimote error event.\n");
			break;
		default:
			DPRINT_LIBS("Have unknown wiimote event.\n");
			break;
		}
	}
}


static void calibrate(wiilist_entry_t *entry)
{
	struct acc_cal acc_cal;

	if (0==cwiid_get_acc_cal(entry->wiihandle, CWIID_EXT_NONE, &acc_cal)) {

		entry->wiimote_valinfo[VALNUM_ACCEL_X].range.center = 
			 acc_cal.zero[CWIID_X];
		entry->wiimote_valinfo[VALNUM_ACCEL_X].SI_add =      
			-acc_cal.zero[CWIID_X];
		entry->wiimote_valinfo[VALNUM_ACCEL_X].SI_div = 
			(acc_cal.one[CWIID_X]-acc_cal.zero[CWIID_X])*100;

		entry->wiimote_valinfo[VALNUM_ACCEL_Y].range.center =
			 acc_cal.zero[CWIID_Y];
		entry->wiimote_valinfo[VALNUM_ACCEL_Y].SI_add =
			-acc_cal.zero[CWIID_Y];
		entry->wiimote_valinfo[VALNUM_ACCEL_Y].SI_div = 
			(acc_cal.one[CWIID_Y]-acc_cal.zero[CWIID_Y])*100;

		entry->wiimote_valinfo[VALNUM_ACCEL_Z].range.center = 
			 acc_cal.zero[CWIID_Z];
		entry->wiimote_valinfo[VALNUM_ACCEL_Z].SI_add =      
			-acc_cal.zero[CWIID_Z];
		entry->wiimote_valinfo[VALNUM_ACCEL_Z].SI_div = 
			(acc_cal.one[CWIID_Z]-acc_cal.zero[CWIID_Z])*100;
	}
	if (0==cwiid_get_acc_cal(entry->wiihandle, CWIID_EXT_NUNCHUK, &acc_cal)) {
		entry->wiimote_valinfo[VALNUM_NUNCHUK_ACCEL_X].range.center = 
			 acc_cal.zero[CWIID_X];
		entry->wiimote_valinfo[VALNUM_NUNCHUK_ACCEL_X].SI_add =      
			-acc_cal.zero[CWIID_X];
		entry->wiimote_valinfo[VALNUM_NUNCHUK_ACCEL_X].SI_div = 
			(acc_cal.one[CWIID_X]-acc_cal.zero[CWIID_X])*100;

		entry->wiimote_valinfo[VALNUM_NUNCHUK_ACCEL_Y].range.center =
			 acc_cal.zero[CWIID_Y];
		entry->wiimote_valinfo[VALNUM_NUNCHUK_ACCEL_Y].SI_add =
			-acc_cal.zero[CWIID_Y];
		entry->wiimote_valinfo[VALNUM_NUNCHUK_ACCEL_Y].SI_div = 
			(acc_cal.one[CWIID_Y]-acc_cal.zero[CWIID_Y])*100;

		entry->wiimote_valinfo[VALNUM_NUNCHUK_ACCEL_Z].range.center = 
			 acc_cal.zero[CWIID_Z];
		entry->wiimote_valinfo[VALNUM_NUNCHUK_ACCEL_Z].SI_add =      
			-acc_cal.zero[CWIID_Z];
		entry->wiimote_valinfo[VALNUM_NUNCHUK_ACCEL_Z].SI_div = 
			(acc_cal.one[CWIID_Z]-acc_cal.zero[CWIID_Z])*100;
	}
}


static void GIIclose(struct gii_source *src)
{
	gii_cwiid_priv *priv = src->priv;
	wiilist_entry_t *currmote;

	cwiid_disconnect (priv->wiimote);

	if (priv->pipefds[0] != -1)
		close(priv->pipefds[0]);
	if (priv->pipefds[1] != -1)
		close(priv->pipefds[1]);

	currmote=wilist_find_by_wiimote_handle(priv->wiimote);
	if (currmote) {
		GG_SLIST_REMOVE(&wiiroot, currmote, wiilist_entry, 
			next);
		free(currmote);
	}

	free(priv);

	return;
}

static int
GII_cwiid_init(struct gii_source *src,
		     const char *target,
		     const char *args,
		     void *argptr)
{
	bdaddr_t bdaddr;
	cwiid_wiimote_t *wiimote = 0;
	gii_cwiid_priv *priv;
	wiilist_entry_t *wilientry;
	int i;

	DPRINT_LIBS("Cwiid starting.\n");

	/* Malloc private data area
	 */
	priv = malloc(sizeof(*priv));
	if (priv == NULL) {
		return GGI_ENOMEM;
	}
	
	wilientry = malloc(sizeof(wiilist_entry_t));
	if (wilientry == NULL) {
		free(priv);
		return GGI_ENOMEM;
	}

	bdaddr = *BDADDR_ANY;
	if (args && *args) {
		str2ba (args, &bdaddr);
	}

	DPRINT_LIBS("Press 1+2 on WIImote to make it discoverable.\n");

	if (! (wiimote = cwiid_connect (&bdaddr, 0)))
	{
		DPRINT_LIBS("WIImote not found.\n");
		free(wilientry);
		free(priv);
		return GGI_ENODEVICE;
	}

	DPRINT_LIBS("WIImote connected.\n");
	if (cwiid_set_mesg_callback (wiimote, &cwiid_callback)) {
	    error_out:
		cwiid_disconnect (wiimote);
		free(wilientry);
		free(priv);
		return GGI_ENODEVICE;
	}

	if (cwiid_set_led(wiimote, CWIID_LED1_ON))
		goto error_out;

	if (cwiid_set_rumble(wiimote, 0))
		goto error_out;

	if (cwiid_set_rpt_mode(wiimote, CWIID_RPT_STATUS| 
					CWIID_RPT_BTN |
					CWIID_RPT_ACC |
					CWIID_RPT_IR |
					CWIID_RPT_EXT ))
		goto error_out;

	/* Init shadow button state so we can track changes. */
	priv->buttonstate.wiimote =
	priv->buttonstate.nunchuk =
	priv->buttonstate.classic = 0;
	/* Init accel states so we can suppress surplus reports */
	priv->as_wiimote.accels[0] =
	priv->as_wiimote.accels[1] =
	priv->as_wiimote.accels[2] =
	priv->as_nunchuk.accels[0] =
	priv->as_nunchuk.accels[1] =
	priv->as_nunchuk.accels[2] = 0;
	/* Init stick states so we can suppress surplus reports */
	priv->ss_nunchuk.pos[0] =
	priv->ss_nunchuk.pos[1] =
	priv->ss_classic_left.pos[0] =
	priv->ss_classic_left.pos[1] =
	priv->ss_classic_right.pos[0] =
	priv->ss_classic_right.pos[1] = 0;
	/* Init IR states ... */
	for(i = 0;i < CWIID_IR_SRC_COUNT; i++) {
		priv->is_wiimote[i].pos[0]=
		priv->is_wiimote[i].pos[1]=0;
		priv->is_wiimote[i].size=-2;
	}

	cwiid_set_mesg_callback(wiimote, cwiid_callback);

	cwiid_enable(wiimote, CWIID_FLAG_MESG_IFC);

	priv->wiimote = wiimote;
	priv->pipefds[0] = priv->pipefds[1] = -1;
	if (pipe(priv->pipefds)) {
		cwiid_disconnect (wiimote);
		free(wilientry);
		free(priv);
		return GGI_ENOMEM;
	}

	src->ops.poll = GII_cwiid_poll;
	src->ops.close = GIIclose;
	src->priv = priv;

	/* Prepare the list entry
	 */
	wilientry->wiihandle = wiimote;
	wilientry->src       = src;
	memcpy(wilientry->wiimote_valinfo,
		wiimote_valinfo,
		sizeof(wilientry->wiimote_valinfo[0]) 
		* WIIMOTE_NUM_VALUATORS);
	/* Ask for the accelerometer calibration */
	calibrate(wilientry);

	/* Fixme - the calibration process changes the wiimote_valinfo
	 * This might become a problem once we support multiple WII
	 * inputs at once. So we might want to make copies of the
	 * valinfo array.
	 */
	priv->origin = giiAddDevice(src, &wiimote_devinfo, 
					wilientry->wiimote_valinfo);
	if (priv->origin == 0) {
		close(priv->pipefds[0]);
		close(priv->pipefds[1]);
		cwiid_disconnect (wiimote);
		free(wilientry);
		free(priv);
		return GGI_ENOMEM;
	}

	if (giiSetNerveReadFD(src, &priv->nerve, priv->pipefds[0]) < 0) {
		close(priv->pipefds[0]);
		close(priv->pipefds[1]);
		cwiid_disconnect (wiimote);
		free(wilientry);
		free(priv);
		return GGI_ENODEVICE;
	}

	/* link in the list entry */
	GG_SLIST_INSERT_HEAD(&wiiroot, wilientry, next);
                                
	DPRINT_LIBS("Cwiid up.\n");

	return 0;
}


struct gii_module_source GII_cwiid = {
	GG_MODULE_INIT("input-cwiid", 0, 1, GII_MODULE_SOURCE),
	GII_cwiid_init
};

static struct gii_module_source *_GIIdl_cwiid[] = {
	&GII_cwiid,
	NULL
};


EXPORTFUNC
int GIIdl_cwiid(int item, void **itemptr);
int GIIdl_cwiid(int item, void **itemptr) {
	struct gii_module_source ***modulesptr;
	switch(item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***)itemptr;
		*modulesptr = _GIIdl_cwiid;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
