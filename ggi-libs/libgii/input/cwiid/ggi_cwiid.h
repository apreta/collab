/* $Id: ggi_cwiid.h,v 1.1 2008/05/19 18:24:27 ggibecka Exp $
******************************************************************************

   Linux wiimote inputlib header

   Copyright (C) 2008 Andreas Beck	[becka-ggi@bedatec.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include <ggi/gii-module.h>
#include <ggi/internal/gii_debug.h>

#include <cwiid.h>

typedef struct buttonstate {
	uint16_t wiimote, nunchuk, classic;
} gii_cwiid_buttonstate_priv;

typedef struct accelstatestate {
	uint8_t	accels[3];
} gii_cwiid_accelstate_priv;

typedef struct stickstate {
	uint8_t pos[2];	
} gii_cwiid_stickstate_priv;

typedef struct irstate {
	uint16_t pos[2];
        int8_t size;
} gii_cwiid_irstate_priv;

typedef struct {
	uint32_t origin;
	uint32_t nerve;
	
	gii_cwiid_buttonstate_priv	buttonstate;
	gii_cwiid_accelstate_priv	as_wiimote, as_nunchuk;
	gii_cwiid_stickstate_priv 	ss_nunchuk, 
				     	ss_classic_left, ss_classic_right;
	gii_cwiid_irstate_priv		is_wiimote[CWIID_IR_SRC_COUNT];
	int pipefds[2];
	cwiid_wiimote_t *wiimote;
} gii_cwiid_priv;

#define LWIIMOTE_PRIV(inp)  ((gii_cwiid_priv *) inp->priv)

int GII_cwiid_poll(struct gii_source *src);

typedef uint32_t (*key2label_func)(struct gii_source *src, uint16_t key);
uint32_t GII_cwiid_key2label_wiimote(struct gii_source *src, uint16_t key);
uint32_t GII_cwiid_key2label_nunchuk(struct gii_source *src, uint16_t key);
uint32_t GII_cwiid_key2label_classic(struct gii_source *src, uint16_t key);
