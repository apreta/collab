Linux wiimote input
~~~~~~~~~~~~~~~~~~~

.. manpage:: 7 input-linux-wiimote

Synopsis
--------

::

  input-linux-wiimote: [<bdaddr>]


Description
-----------

Reads from a WIImote connected to a Linux system via bluetooth.

Note, that when the application opens a WIImote, it should notify the 
user to set the WIImote into pairing mode. This is done by pressing
either the 1 and 2 button on the WIImote simultaneaously or by pressing 
the pairing button in the battery compartment.


Options
-------


:p:`bdaddr`
    Specifies the device to use, the default is to accept any wiimote.


Bugs/TODO
---------

- Key repeat messages are not sent, which can annoy users who want
  to use the cursor keys.
