/* $Id: key.c,v 1.1 2008/05/19 18:24:28 ggibecka Exp $
******************************************************************************

   Linux wiimote key mapper

   Copyright (C) 2008 Andreas Beck	[becka-ggi@bedatec.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include "ggi_cwiid.h"
#include <ggi/gii-keyboard.h>


uint32_t
GII_cwiid_key2label_wiimote(struct gii_source *src, uint16_t key)
{
	switch(key) {

	case CWIID_BTN_2:
		return GIIUC_2;
	case CWIID_BTN_1:
		return GIIUC_1;

	case CWIID_BTN_B:
		return GIIUC_B;
	case CWIID_BTN_A:
		return GIIUC_A;

	case CWIID_BTN_MINUS:
		return GIIUC_Minus;
	case CWIID_BTN_PLUS:
		return GIIUC_Plus;
	case CWIID_BTN_HOME:
		return GIIK_Home;

	case CWIID_BTN_LEFT:
		return GIIK_Left;
	case CWIID_BTN_RIGHT:
		return GIIK_Right;
	case CWIID_BTN_DOWN:
		return GIIK_Down;
	case CWIID_BTN_UP:
		return GIIK_Up;
	default:
		DPRINT_EVENTS("GII_cwiid_key2label: Unknown keycode 0x%04x.\n",
			      key);
		break;
	}
	return GIIK_NIL;
}

uint32_t
GII_cwiid_key2label_nunchuk(struct gii_source *src, uint16_t key)
{
	switch(key) {

	case CWIID_NUNCHUK_BTN_Z:
		return GIIUC_Z;
	case CWIID_NUNCHUK_BTN_C:
		return GIIUC_C;

	default:
		DPRINT_EVENTS("GII_cwiid_key2label: Unknown nunchuk keycode 0x%04x.\n",
			      key);
		break;
	}
	return GIIK_NIL;
}

uint32_t
GII_cwiid_key2label_classic(struct gii_source *src, uint16_t key)
{
	switch(key) {
	
	case CWIID_CLASSIC_BTN_ZR:
		return GIIUC_Z;
	case CWIID_CLASSIC_BTN_ZL:
		return GIIUC_z;	/* sigh - should we invent ZR/ZL "labels"? */

	case CWIID_CLASSIC_BTN_R:
		return GIIUC_R;
	case CWIID_CLASSIC_BTN_L:
		return GIIUC_L;

	case CWIID_CLASSIC_BTN_Y:
		return GIIUC_Y;
	case CWIID_CLASSIC_BTN_X:
		return GIIUC_X;
	case CWIID_CLASSIC_BTN_B:
		return GIIUC_B;
	case CWIID_CLASSIC_BTN_A:
		return GIIUC_A;

	case CWIID_CLASSIC_BTN_MINUS:
		return GIIUC_Minus;
	case CWIID_CLASSIC_BTN_PLUS:
		return GIIUC_Plus;
	case CWIID_CLASSIC_BTN_HOME:
		return GIIK_Home;

	case CWIID_CLASSIC_BTN_LEFT:
		return GIIK_Left;
	case CWIID_CLASSIC_BTN_RIGHT:
		return GIIK_Right;
	case CWIID_CLASSIC_BTN_DOWN:
		return GIIK_Down;
	case CWIID_CLASSIC_BTN_UP:
		return GIIK_Up;
	default:
		DPRINT_EVENTS("GII_cwiid_key2label: Unknown classic keycode 0x%04x.\n",
			      key);
		break;
	}
	return GIIK_NIL;
}

