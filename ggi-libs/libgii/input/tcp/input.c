/* $Id: input.c,v 1.24 2008/01/19 01:15:21 cegger Exp $
******************************************************************************

   Input-tcp: Read events from a TCP-socket

   Copyright (C) 2000 Marcus Sundberg	[marcus@ggi-project.org]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"

#include <ggi/gg.h>
#include <ggi/internal/gg_replace.h>
#include <ggi/gii-module.h>
#include <ggi/internal/gii_debug.h>

#include "libtcp.h"

#include <stdlib.h>
#include <string.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifdef HAVE_STRINGS_H
#include <strings.h>
#endif


static struct gii_cmddata_devinfo devinfo = {
	"TCP",			/* long device name */
	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,
	emAll,			/* all event types */
	1,
	~(0U),			/* all valuators */
	~(0U),			/* all buttons */
};

static gii_event_mask handle_packets(struct gii_source *src)
{
	struct gii_tcp_priv *priv = GII_TCP_PRIV(src);
	gii_event *ev;
	int rc = 0;

	ev = (gii_event *) priv->buf;
	while ((priv->count) && (priv->count >= ev->any.size)) {
		if (_gii_tcp_ntohev(ev) == 0) {
			rc |= (1 << ev->any.type);
			DPRINT_EVENTS
			    ("input-tcp: Got event type %d, size %d\n",
			     ev->any.type, ev->any.size);

			ev->any.origin = src->origin;
			giiPostEvent(src, ev);
		} else {
			DPRINT_EVENTS
			    ("input-tcp: Got UNSUPPORTED event type %d, size %d\n",
			     ev->any.type, ev->any.size);
		}

		priv->count -= ev->any.size;
		ev = (gii_event *) ((uint8_t *) ev + ev->any.size);
	}
	if (priv->count) {
		memmove(priv->buf, ev, priv->count);
	}

	return rc;
}


static int GII_tcp_poll(struct gii_source *src)
{
	struct gii_tcp_priv *priv = GII_TCP_PRIV(src);
	size_t read_len;

	DPRINT_EVENTS("GII_tcp_poll(%p) called\n", src);

	if (!priv->state)
		return GGI_OK;

	if (priv->state == GIITCP_LISTEN) {
		if (_gii_tcp_accept(priv) == 0) {
			DPRINT_LIBS("GII_tcp_poll: accepted connection\n");
			giiCutNerve(src, &priv->nerve);
			if (giiSetNerveReadFD(src, &priv->nerve, priv->fd) < 0)
				DPRINT_LIBS("GII_tcp_poll: cannot set new nerve\n");
		} else {
			DPRINT_LIBS("GII_tcp_poll: failed to accept connection\n");
		}
		return GGI_OK;
	}

	read_len = GIITCP_BUFSIZE - priv->count;

	read_len = read(priv->fd, priv->buf + priv->count, read_len);

	if (read_len <= 0) {
		if (read_len == 0) {
			/* Connection has probably been broken. */
			_gii_tcp_close(priv->fd);
			giiCutNerve(src, &priv->nerve);
			if (priv->listenfd != -1) {
				/* Start listening again. */
				priv->state = GIITCP_LISTEN;
				if (giiSetNerveReadFD(src, &priv->nerve, priv->listenfd) < 0)
					DPRINT_MISC("GII_tcp_poll: cannot set new nerve\n");
				fprintf(stderr,
					"input-tcp: starting to listen again\n");
			} else {
				/* Just close the fd. */
				priv->state = GIITCP_NOCONN;
				fprintf(stderr,
					"input-tcp: connection closed\n");
			}
			priv->fd = -1;
		}
		return GGI_OK;
	}

	priv->count += read_len;
	handle_packets(src);

	return GGI_OK;
}


static void GII_tcp_close(struct gii_source *src)
{
	struct gii_tcp_priv *priv = GII_TCP_PRIV(src);

	DPRINT_LIBS("GII_tcp_close(%p) called\n", src);

	if (priv->fd != -1)
		_gii_tcp_close(priv->fd);
	if (priv->listenfd != -1)
		_gii_tcp_close(priv->listenfd);
	if (priv->lock)
		ggLockDestroy(priv->lock);
	free(priv);

	DPRINT_LIBS("GII_tcp_close done\n");
}


#define MAX_HLEN  256


static int
GII_tcp_init(struct gii_source *src,
	     const char *target, const char *args, void *argptr)
{
	char host[MAX_HLEN];
	const char *portstr;
	size_t hlen;
	int port, err, fd;
	struct gii_tcp_priv *priv;

	DPRINT_LIBS("GIIdl_tcp(%p, \"%s\", \"%s\", %p) called\n", src,
		    target, args ? args : "", argptr);

	if (!args || *args == '\0')
		return GGI_EARGREQ;

	portstr = strchr(args, ':');
	if (!portstr)
		return GGI_EARGREQ;

	hlen = portstr - args;
	if (hlen >= MAX_HLEN)
		return GGI_EARGINVAL;
	memcpy(host, args, hlen);
	host[hlen] = '\0';

	portstr++;
	port = strtoul(portstr, NULL, 0);
	if (!port)
		return GGI_EARGINVAL;

	priv = calloc(1, sizeof(*priv));
	if (priv == NULL) {
		return GGI_ENOMEM;
	}

	priv->origin = giiAddDevice(src, &devinfo, NULL);
	if (priv->origin == 0) {
		free(priv);
		return GGI_ENOMEM;
	}

	priv->lock = ggLockCreate();
	if (priv->lock == NULL) {
		free(priv);
		return GGI_ENOMEM;
	}
	priv->state = GIITCP_NOCONN;
	priv->listenfd = priv->fd = -1;
	priv->count = 0;

	if (strcasecmp(host, "listen") == 0) {
		err = _gii_tcp_listen(priv, port);
		fd = priv->listenfd;
	} else {
		err = _gii_tcp_connect(priv, host, port);
		fd = priv->fd;
	}
	if (err)
		return err;

	src->priv = priv;

	if (giiSetNerveReadFD(src, &priv->nerve, fd) < 0) {
		GII_tcp_close(src);
		return GGI_ENODEVICE;
	}

	src->ops.poll = GII_tcp_poll;
	src->ops.close = GII_tcp_close;

	DPRINT_LIBS("input-tcp fully up\n");

	return 0;
}


struct gii_module_source GII_tcp = {
	GG_MODULE_INIT("input-tcp", 0, 1, GII_MODULE_SOURCE),
	GII_tcp_init
};

static struct gii_module_source *_GIIdl_tcp[] = {
	&GII_tcp,
	NULL
};

EXPORTFUNC int GIIdl_tcp(int item, void **itemptr);
int GIIdl_tcp(int item, void **itemptr)
{
	struct gii_module_source ***modulesptr;
	switch (item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***) itemptr;
		*modulesptr = _GIIdl_tcp;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
