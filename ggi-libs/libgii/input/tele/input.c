/* $Id: input.c,v 1.14 2009/05/29 13:26:41 pekberg Exp $
******************************************************************************

   Input-tele: initialization

   Copyright (C) 2006 Christoph Egger

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"

#include <stdlib.h>
#include <string.h>

#include <ggi/gg.h>
#include <ggi/gii-module.h>
#include <ggi/internal/gii_debug.h>

#include "libtele.h"
#include <ggi/input/tele.h>


struct gii_tele_priv {
	TeleClient *client;

	tele_tclient_poll *tclient_poll;
	tele_tclient_read *tclient_read;

	int connected;

	int wait4event;
	TeleEvent wait_event;
	long wait_type;
	long wait_sequence;

	int width;		/* for generating evExpose events */
	int height;

	uint32_t origin;
	uint32_t nerve;

};

#define TELE_PRIV(inp)	((struct gii_tele_priv *) inp->priv)

#define TELE_HANDLE_SHUTDOWN	\
	do {	\
		fprintf(stderr, "display-tele: Server GONE !\n"); \
		exit(2); \
	} while (0);



static int GII_tele_controller(void *arg, uint32_t flag, void *data)
{
	struct gii_source *src = arg;
	struct gii_tele_priv *priv = TELE_PRIV(src);

	if ((flag & GII_CMDCODE_TELE_CONNECT) == GII_CMDCODE_TELE_CONNECT) {
		struct gii_cmddata_connect *connect;

		connect = (struct gii_cmddata_connect *) data;

		priv->connected = connect->connected;
		priv->width = connect->width;
		priv->height = connect->height;
	}

	if ((flag & GII_CMDCODE_TELE_WAIT4EVENT) ==
	    GII_CMDCODE_TELE_WAIT4EVENT) {
		struct gii_cmddata_tele_event *w4ev;

		w4ev = (struct gii_cmddata_tele_event *) data;

		priv->wait_event = w4ev->ev;
		priv->wait_type = w4ev->type;
		priv->wait_sequence = w4ev->sequence;
		priv->wait4event = 1;
	}

	return 0;
}


static void handle_telecmd_event(struct gii_source *src, TeleEvent * ev)
{
	struct gii_tele_priv *priv = TELE_PRIV(src);
	/* handle TELE_CMD_CLOSE ??? */

	if ((priv->wait4event == 0) ||
	    (priv->wait_type != ev->type) ||
	    (priv->wait_sequence != ev->sequence))
	{
		DPRINT_MISC("UNEXPECTED CMD EVENT "
			    "(type=0x%08x seq=0x%08x)\n",
			    ev->type, ev->sequence);
		return;
	}

	DPRINT_EVENTS("GOT REPLY (type = 0x%08lx "
		      "seq = 0x%08lx)\n", ev->type, ev->sequence);

	memcpy(&priv->wait_event, ev, ev->size * sizeof(long));

	do {
		struct gii_cmddata_tele_event reply;

		priv->wait4event = 0;
		memcpy(&reply.ev, &priv->wait_event, sizeof(reply.ev));
		reply.type = ev->type;
		reply.sequence = ev->sequence;

		ggBroadcast(src->instance.channel,
			    GII_CMDCODE_TELE_UNLOCK, &reply);
	} while (0);
}


static int
translate_to_gii(struct gii_source *src, gii_event * ev, TeleEvent * tv)
{
	struct gii_tele_priv *priv = TELE_PRIV(src);

	if ((tv->type & TELE_EVENT_TYPE_MASK) != TELE_INP_BASE) {
		DPRINT_MISC("unrecognised event from "
			    "server (0x%08x).\n", tv->type);
		return GGI_EEVUNKNOWN;
	}

	giiEventBlank(ev, sizeof(gii_event));
	ev->any.sec = tv->time.sec;
	ev->any.usec = tv->time.nsec / 1000;
	ev->any.origin = tv->device;

	switch (tv->type) {
	case TELE_INP_KEY:
	case TELE_INP_KEYUP:
		do {
			TeleInpKeyData *d = (void *) tv->data;

			ev->any.size = sizeof(gii_key_event);
			ev->any.type = (tv->type == TELE_INP_KEY) ?
			    evKeyPress : evKeyRelease;

			ev->key.modifiers = d->modifiers;

			ev->key.sym = d->key;
			ev->key.label = d->label;
			ev->key.button = d->button;

			return 0;
		} while (0);

	case TELE_INP_BUTTON:
	case TELE_INP_BUTTONUP:
		do {
			TeleInpButtonData *d = (void *) tv->data;

			ev->any.size = sizeof(gii_pbutton_event);
			ev->any.type = (tv->type == TELE_INP_BUTTON) ?
			    evPtrButtonPress : evPtrButtonRelease;

			ev->pbutton.button = d->button;

			return 0;
		} while (0);

	case TELE_INP_MOUSE:
	case TELE_INP_TABLET:
		do {
			TeleInpAxisData *d = (void *) tv->data;

			ev->any.size = sizeof(gii_pmove_event);
			ev->any.type = (tv->type == TELE_INP_MOUSE) ?
			    evPtrRelative : evPtrAbsolute;

			ev->pmove.x = (d->count >= 1) ? d->axes[0] : 0;
			ev->pmove.y = (d->count >= 2) ? d->axes[1] : 0;
			ev->pmove.z = (d->count >= 3) ? d->axes[2] : 0;
			ev->pmove.wheel = (d->count >= 4) ? d->axes[3] : 0;

			return 0;
		} while (0);

	case TELE_INP_JOYSTICK:
		do {
			TeleInpAxisData *d = (void *) tv->data;
			int i;

			if (d->count > 32) {
				return GGI_ENOSPACE;
			}

			ev->any.size = sizeof(gii_val_event);
			ev->any.type = evValAbsolute;

			ev->val.first = 0;
			ev->val.count = d->count;

			for (i = 0; i < d->count; i++) {
				ev->val.value[i] = d->axes[i];
			}

			return 0;
		} while (0);

	case TELE_INP_EXPOSE:
		do {
			ev->any.size = sizeof(gii_expose_event);
			ev->any.type = evExpose;

			/* the whole screen */

			ev->expose.x = 0;
			ev->expose.y = 0;
			ev->expose.w = priv->width;
			ev->expose.h = priv->height;

			ggBroadcast(src->instance.channel,
				    GII_CMDCODE_EXPOSE, ev);

			return 0;
		} while (0);
	}

	DPRINT_MISC("unknown input event (0x%08x).\n", tv->type);
	return GGI_EEVUNKNOWN;
}


static int GII_tele_poll(struct gii_source *src)
{
	struct gii_tele_priv *priv = TELE_PRIV(src);

	gii_event_mask evmask = 0;
	gii_event ev;

	TeleEvent th_ev;
	int err;

	DPRINT_EVENTS("poll event.\n");

	if (!priv->connected)
		return 0;

	if (priv->tclient_poll(priv->client)) {

		err = priv->tclient_read(priv->client, &th_ev);
		if (err == TELE_ERROR_SHUTDOWN) {
			TELE_HANDLE_SHUTDOWN;

		} else if (err < 0) {
			DPRINT_EVENTS("tclient_read: ZERO\n");
			return 0;
		}

		DPRINT_EVENTS("got event (type=0x%08x "
			      "seq=0x%08x)\n", (int) th_ev.type,
			      (int) th_ev.sequence);

		if ((th_ev.type & TELE_EVENT_TYPE_MASK) == TELE_CMD_BASE) {
			handle_telecmd_event(src, &th_ev);

		} else if (translate_to_gii(src, &ev, &th_ev) == 0) {
			evmask |= (1 << ev.any.type);
			giiPostEvent(src, &ev);
		}
	}

	return GII_OP_DONE;
}

static void GII_tele_close(struct gii_source *src)
{
	struct gii_tele_priv *priv = TELE_PRIV(src);

	free(priv);

	DPRINT_MISC("input-tele: closed\n");
}

static struct gii_cmddata_devinfo devinfo = {
	"Tele Input",		/* long device name */
	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,
	emKey | emPtrAbsolute | emPtrButton,
	1,
	0,			/* all valuators */
	256,			/* buttons */
};


static int
GII_tele_init(struct gii_source *src,
	      const char *target, const char *args, void *argptr)
{
	struct gii_tele_priv *priv;
	gii_tele_arg *telearg = argptr;

	DPRINT_LIBS("GIIdl_tele(%p, \"%s\", \"%s\", %p) called\n", src,
		    target, args ? args : "", argptr);

	if (!telearg)
		return GGI_EARGREQ;

	/* allocate private stuff */
	priv = calloc(1, sizeof(struct gii_tele_priv));
	if (priv == NULL)
		return GGI_ENOMEM;

	priv->origin = giiAddDevice(src, &devinfo, NULL);
	if (priv->origin == 0) {
		free(priv);
		return GGI_ENOMEM;
	}

	priv->client = telearg->client;
	priv->tclient_poll = telearg->tclient_poll;
	priv->tclient_read = telearg->tclient_read;

	priv->connected = 0;
	priv->wait4event = 0;

	src->priv = priv;

	src->ops.poll = GII_tele_poll;
	src->ops.close = GII_tele_close;

	ggSetController(src->instance.channel, GII_tele_controller);

	giiSetNerveTick(src, &priv->nerve, 10);
	DPRINT_MISC("input-tele fully up\n");

	return 0;
}


struct gii_module_source GII_tele = {
	GG_MODULE_INIT("input-tele", 0, 1, GII_MODULE_SOURCE),
	GII_tele_init
};


static struct gii_module_source *_GIIdl_tele[] = {
	&GII_tele,
	NULL
};

EXPORTFUNC int GIIdl_tele(int item, void **itemptr);
int GIIdl_tele(int item, void **itemptr)
{
	struct gii_module_source ***modulesptr;
	switch (item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***) itemptr;
		*modulesptr = _GIIdl_tele;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
