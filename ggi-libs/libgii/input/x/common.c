/* $Id: common.c,v 1.3 2006/03/10 22:43:42 cegger Exp $
******************************************************************************

   common routines for input-x, input-xwin and input-xf86dga

   Copyright (C) 2006 Christoph Egger

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"

#include <ggi/gg.h>
#include <ggi/gii-module.h>
#include <ggi/internal/gii_debug.h>
#include <ggi/input/xwin.h>

#include "common.h"


Cursor _gii_x_make_cursor(Display *disp, Window win)
{
	char emptybm[] = { 0 };
	Pixmap crsrpix;
	XColor nocol;
	Cursor mycrsr;

	crsrpix = XCreateBitmapFromData(disp, win, emptybm, 1, 1);
	mycrsr = XCreatePixmapCursor(disp, crsrpix, crsrpix,
				     &nocol, &nocol, 0, 0);
	XFreePixmap(disp, crsrpix);

	return mycrsr;
}
