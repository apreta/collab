/* $Id: xwin.c,v 1.38 2009/07/31 07:57:23 pekberg Exp $
******************************************************************************

   Xwin inputlib - use an existing X window as a LibGII input source

   Copyright (C) 1998-1999 Marcus Sundberg	[marcus@ggi-project.org]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>

#include "config.h"
#include <ggi/gg.h>
#include <ggi/gii-module.h>
#include <ggi/internal/gii_debug.h>
#include <ggi/input/xwin.h>
#include "xev.h"

#define RELPTR_KEYS { GIIK_CtrlL, GIIK_AltL, GIIUC_M }
#define RELPTR_KEYINUSE (1 | (1<<1) | (1<<2))
#define RELPTR_NUMKEYS  3

enum {
	XWIN_DEV_KEY = 0,
	XWIN_DEV_MOUSE,

	XWIN_DEV_MAX
};

struct xwin_priv {
	Display *disp;
	Window win;
	Window parentwin;
	XComposeStatus compose_status;
	XIM xim;
	XIC xic;
	Cursor cursor;
	unsigned int oldcode;
	uint32_t symstat[0x60];
	int width, height;
	int frames;
	int oldx, oldy;
	int alwaysrel;
	int relptr;
	uint32_t relptr_keymask;
	void *exposearg;
	void *resizearg;
	gii_inputxwin_lockfunc *lockfunc;
	void *lockarg;
	gii_inputxwin_unlockfunc *unlockfunc;
	void *unlockarg;

	uint32_t origin[XWIN_DEV_MAX];
	uint32_t nerve;
	uint32_t nerve_tick;

	char key_vector[32];

	struct gg_observer *observer;
	int exit_on_delete_window;
	int yoffset;
};

#define XWIN_PRIV(src)	((struct xwin_priv*)((src)->priv))


/* These functions are only called from one place, and broken out of the
   code for readability. We inline them so we don't loose any performance. */

static inline Cursor make_cursor(Display * disp, Window win)
{
	char emptybm[] = { 0 };
	Pixmap crsrpix;
	XColor nocol;
	Cursor mycrsr;

	crsrpix = XCreateBitmapFromData(disp, win, emptybm, 1, 1);
	mycrsr = XCreatePixmapCursor(disp, crsrpix, crsrpix,
				     &nocol, &nocol, 0, 0);
	XFreePixmap(disp, crsrpix);

	return mycrsr;
}

static inline void update_winparam(struct xwin_priv *priv);

static int _gii_xwin_controller(void *arg, uint32_t flag, void *data)
{
	struct gii_source *src = arg;
	struct gii_xwin_cmddata_pointer *xwinpointer;
	struct gii_xwin_cmddata_setparam *setparam;

	struct xwin_priv *priv = XWIN_PRIV(src);

	switch (flag) {

	case GII_CMDCODE_RESIZE:
		{
			gii_event *ev_ = data;
			gii_event ev;
			giiEventBlank(&ev, sizeof(gii_cmd_event));
			memcpy(ev.cmd.data, ev_->cmd.data,
				sizeof(ev.cmd.data));
			ev.any.size = ev_->any.size;
			ev.any.type = ev_->any.type;
			ev.cmd.code = ev_->cmd.code;
			giiPostEvent(src, &ev);
			break;
		}

	case GII_CMDCODE_XWINPOINTER:
		xwinpointer = (struct gii_xwin_cmddata_pointer *) data;
		priv->alwaysrel = xwinpointer->ptralwaysrel;
		break;

	case GII_CMDCODE_XWINSETPARAM:
		setparam = (struct gii_xwin_cmddata_setparam *) data;
		priv->win = setparam->win;
		priv->parentwin = setparam->parentwin;
		priv->frames = setparam->frames;
		update_winparam(priv);
		break;

	case GII_CMDCODE_EXITONCLOSE:
		{
			struct gii_xwin_cmddata_exitonclose *eocparm;
			eocparm =
			    (struct gii_xwin_cmddata_exitonclose *) data;
			priv->exit_on_delete_window = eocparm->exitonclose;
		}
		break;
	case GII_CMDCODE_INJECT_EVENT:
		{
			gii_event *evptr = data;
			giiEventBlank(evptr, 0); /* only set timestamp */
			evptr->any.origin = src->origin;
			giiPostEvent(src, evptr);
		}
		break;
	case GII_CMDCODE_SET_PAGE_OFFSET:
		{
			struct gii_xwin_cmddata_set_page_offset *spoparm;
			spoparm =
			    (struct gii_xwin_cmddata_set_page_offset *)
			    data;
			priv->yoffset = spoparm->yoffset;
		}
		break;

	default:
		return GGI_ENOFUNC;
	}

	return GGI_OK;
}

static void do_grab(struct xwin_priv *priv)
{
	XGrabPointer(priv->disp, priv->win, 1, 0,
		     GrabModeAsync, GrabModeAsync,
		     priv->win, priv->cursor, CurrentTime);
	XWarpPointer(priv->disp, None, priv->win, 0, 0, 0, 0,
		     priv->width / 2, priv->height / 2);
	priv->relptr = 1;
	priv->oldx = priv->width / 2;
	priv->oldy = priv->height / 2;
	DPRINT_EVENTS("GII_xwin: Using relative pointerevents\n");
}


static void do_ungrab(struct xwin_priv *priv)
{
	XUngrabPointer(priv->disp, CurrentTime);
	priv->relptr = 0;
	DPRINT_EVENTS("GII_xwin: Using absolute pointerevents\n");
}


static void handle_relptr(struct xwin_priv *priv)
{
	if (priv->relptr) {
		do_ungrab(priv);
	} else {
		do_grab(priv);
	}
}

static inline void center_pointer(struct xwin_priv *priv)
{
	XEvent event;
	event.type = MotionNotify;
	event.xmotion.display = priv->disp;
	event.xmotion.window = priv->win;
	event.xmotion.x = priv->width / 2;
	event.xmotion.y = priv->height / 2;
	XSendEvent(priv->disp, priv->win, False, PointerMotionMask,
		   &event);
	XWarpPointer(priv->disp, None, priv->win, 0, 0, 0, 0,
		     priv->width / 2, priv->height / 2);
}

static inline void update_winparam(struct xwin_priv *priv)
{
	if (!priv->alwaysrel) {
		Window dummywin;
		unsigned int w, h, dummy;

		if (priv->cursor == None) {
			DPRINT_MISC
			    ("update_winparam: call make_cursor(%p,%i)\n",
			     priv->disp, priv->win);
			priv->cursor = make_cursor(priv->disp, priv->win);
		}
		DPRINT_MISC
		    ("update_winparam: call XGetGeometry with disp=%p, win=%i\n",
		     priv->disp, priv->win);
		XGetGeometry(priv->disp, priv->win, &dummywin,
			     (int *) &dummy, (int *) &dummy, &w, &h,
			     &dummy, &dummy);
		DPRINT_MISC
		    ("update_winparam: XGetGeometry() done, w=%u, h=%u\n",
		     w, h);
		priv->width = w;
		priv->height = h / priv->frames;
		priv->oldx = w / 2;
		/* XXX CHECKME is that correct? */
		priv->oldy = h / 2 + priv->yoffset;
	}
	if (priv->xim) {
		XDestroyIC(priv->xic);
		XCloseIM(priv->xim);
	}
	priv->xim = XOpenIM(priv->disp, NULL, NULL, NULL);
	if (priv->xim) {
		DPRINT_MISC
		    ("update_winparam: call XCreateIC with priv->win = %i\n",
		     priv->win);
		priv->xic =
		    XCreateIC(priv->xim, XNInputStyle,
			      XIMPreeditNothing | XIMStatusNothing,
			      XNClientWindow, priv->win, XNFocusWindow,
			      priv->win, NULL);
		if (!priv->xic) {
			XCloseIM(priv->xim);
			priv->xim = NULL;
		}
	} else {
		priv->xic = NULL;
	}
}

static int GII_xwin_eventpoll(struct gii_source *src)
{
	struct xwin_priv *priv = XWIN_PRIV(src);
	int n, i;
	/* int rc = 0; */
	gii_event releasecache;
	Time releasetime = 0;
	int havecached = 0;

	DPRINT_EVENTS("GII_xwin_eventpoll(%p) called\n", src);

	if (priv->lockfunc)
		priv->lockfunc(priv->lockarg);

	n = 0;
	while (n || (n = XEventsQueued(priv->disp, QueuedAfterReading))) {
		XEvent xev;
		gii_event giiev;
		unsigned int keycode;

		DPRINT_EVENTS
		    ("GII_xwin_eventpoll(%p) handling %d events\n", src,
		     n);

		n--;

		XNextEvent(priv->disp, &xev);
		keycode = xev.xkey.keycode;

		if (XFilterEvent(&xev, None)) {
			priv->oldcode = keycode;
			if (xev.xkey.keycode == 0)
				continue;
		}

		giiEventBlank(&giiev, sizeof(gii_event));

		switch (xev.type) {
		case KeyPress:
			giiev.any.size = sizeof(gii_key_event);
			giiev.any.type = evKeyPress;
			giiev.any.origin = priv->origin[XWIN_DEV_KEY];
			giiev.key.button = keycode - 8;

			priv->key_vector[keycode / 8] |=
			    1 << (keycode & 7);

			if (havecached &&
			    releasecache.key.button == giiev.key.button) {
				if (xev.xkey.time == releasetime) {
					giiev.any.type = evKeyRepeat;
					/* rc |= emKeyRepeat; */
				} else {
					giiPostEvent(src, &releasecache);
					/* rc |= emKeyRelease;
					   rc |= emKeyPress; */
					if (releasecache.key.label < 0x60) {
						priv->symstat[releasecache.
							      key.label] =
						    0;
					}
				}
				havecached = 0;
			}	/* else {
				   rc |= emKeyPress;
				   } */
			_gii_xev_trans(&xev.xkey, &giiev.key,
				       &priv->compose_status, priv->xic,
				       &priv->oldcode);
			if (giiev.any.type == evKeyPress &&
			    giiev.key.label < 0x60) {
				priv->symstat[giiev.key.label] =
				    giiev.key.sym;
			}

			if (!priv->alwaysrel) {
				uint32_t relsyms[RELPTR_NUMKEYS] =
				    RELPTR_KEYS;
				for (i = 0; i < RELPTR_NUMKEYS; i++) {
					if (giiev.key.label == relsyms[i]) {
						priv->relptr_keymask &=
						    ~(1 << i);
						break;
					}
				}
				if (priv->relptr_keymask == 0) {
					handle_relptr(priv);
				}
			}
			DPRINT_EVENTS("GII_xwin_eventpoll: KeyPress\n");
			break;

		case KeyRelease:
			priv->key_vector[keycode / 8] &=
			    ~(1 << (keycode & 7));
			if (havecached) {
				giiPostEvent(src, &releasecache);
				/* rc |= emKeyRelease; */
			}
			giiEventBlank(&releasecache,
				      sizeof(gii_key_event));
			releasecache.any.size = sizeof(gii_key_event);
			releasecache.any.type = evKeyRelease;
			releasecache.any.origin =
			    priv->origin[XWIN_DEV_KEY];
			releasecache.key.button = keycode - 8;

			_gii_xev_trans(&xev.xkey, &releasecache.key,
				       &priv->compose_status, NULL, NULL);
			if (releasecache.key.label < 0x60 &&
			    priv->symstat[releasecache.key.label] != 0)
			{
				releasecache.key.sym
				    = priv->symstat[releasecache.key.label];
			}

			havecached = 1;
			releasetime = xev.xkey.time;

			if (!priv->alwaysrel) {
				uint32_t relsyms[RELPTR_NUMKEYS] =
				    RELPTR_KEYS;
				for (i = 0; i < RELPTR_NUMKEYS; i++) {
					if (releasecache.key.label
					    == relsyms[i])
					{
						priv->relptr_keymask |= (1 << i);
						break;
					}
				}
			}
			DPRINT_EVENTS("GII_xwin_eventpoll: KeyRelease\n");
			break;

		case KeymapNotify:{
				unsigned char delta;
				int j;
				XEvent fake_xev;

				for (i = 1; i < 32; i++) {
					delta =
					    priv->key_vector[i] ^ xev.
					    xkeymap.key_vector[i];
					if (!delta)
						continue;
					for (j = 0; j < 8; j++) {
						if (0 == (delta & (1 << j)))
							continue;
						keycode = i * 8 + j;
						fake_xev.xkey.serial =
						    xev.xkeymap.serial;
						fake_xev.xkey.send_event =
						    xev.xkeymap.send_event;
						fake_xev.xkey.display =
						    xev.xkeymap.display;
						fake_xev.xkey.window =
						    xev.xkeymap.window;
						fake_xev.xkey.root =
						    xev.xkeymap.window;
						fake_xev.xkey.subwindow =
						    xev.xkeymap.window;
						fake_xev.xkey.time = 0;
						fake_xev.xkey.x = 0;
						fake_xev.xkey.y = 0;
						fake_xev.xkey.x_root = 0;
						fake_xev.xkey.y_root = 0;
						fake_xev.xkey.state = 0;
						fake_xev.xkey.keycode =
						    keycode;
						fake_xev.xkey.same_screen =
						    0;

						if (xev.xkeymap.key_vector[i] & (1 << j))
						{
							giiev.any.size =
							    sizeof(gii_key_event);
							giiev.any.type = evKeyPress;
							giiev.any.origin =
							    priv->origin[XWIN_DEV_KEY];
							giiev.key.button = keycode - 8;
							/* rc |= emKeyPress; */
							fake_xev.xkey.type = KeyPress;
							_gii_xev_trans(&fake_xev.
							     xkey,
							     &giiev.key,
							     &priv->compose_status,
							     priv->xic,
							     &priv->oldcode);
							if (giiev.any.type ==
							    evKeyPress
							    && giiev.key.label < 0x60)
							{
								priv->symstat
								    [giiev.key.label]
								    = giiev.key.sym;
							}
							giiPostEvent(src, &giiev);
						} else {
							giiev.any.size =
							    sizeof(gii_key_event);
							giiev.any.type =
							    evKeyRelease;
							giiev.any.origin =
							    priv->origin[XWIN_DEV_KEY];
							giiev.key.button =
							    keycode - 8;
							/* rc |= emKeyRelease; */
							fake_xev.xkey.type = KeyRelease;
							_gii_xev_trans(&fake_xev.xkey,
							     &giiev.key,
							     &priv->compose_status,
							     NULL, NULL);
							if (giiev.key.label < 0x60
							    && priv->symstat[giiev.key.label]
							    != 0) {
								giiev.key.sym =
								    priv->symstat
								    [giiev.key.label];
							}
							giiPostEvent(src, &giiev);
						}
					}
					priv->key_vector[i] =
					    xev.xkeymap.key_vector[i];
				}
			}
			goto dont_queue_this_event;

		case ButtonPress:
			giiev.any.type = evPtrButtonPress;
			giiev.any.origin = priv->origin[XWIN_DEV_MOUSE];
			/* rc |= emPtrButtonPress; */
			DPRINT_EVENTS
			    ("GII_xwin_eventpoll: ButtonPress %x\n",
			     xev.xbutton.state);
			break;

		case ButtonRelease:
			giiev.any.type = evPtrButtonRelease;
			giiev.any.origin = priv->origin[XWIN_DEV_MOUSE];
			/* rc |= emPtrButtonRelease; */
			DPRINT_EVENTS
			    ("GII_xwin_eventpoll: ButtonRelease %x\n",
			     xev.xbutton.state);
			break;

		case MotionNotify:
			if (priv->relptr) {
				int realmove = 0;
				if (!xev.xmotion.send_event) {
					giiev.pmove.x =
					    xev.xmotion.x - priv->oldx;
					giiev.pmove.y =
					    xev.xmotion.y - priv->oldy;
					realmove = 1;
#undef ABS
#define ABS(a) (((int)(a)<0) ? -(a) : (a) )
					if (ABS
					    (priv->width / 2 -
					     xev.xmotion.x)
					    > priv->width / 4
					    || ABS(priv->height / 2 -
						   xev.xmotion.y)
					    > priv->height / 4) {
						center_pointer(priv);
					}
#undef ABS
				}
				priv->oldx = xev.xmotion.x;
				priv->oldy = xev.xmotion.y;
				if (!realmove ||
				    (giiev.pmove.x == 0
				     && giiev.pmove.y == 0))
					goto dont_queue_this_event;
				giiev.any.type = evPtrRelative;
				/* rc |= emPtrRelative; */
			} else {
				if (priv->alwaysrel) {
					giiev.any.type = evPtrRelative;
					/* rc |= emPtrRelative; */
				} else {
					xev.xmotion.y -= priv->yoffset;
#ifdef DEBUG_FRAME_SYNC_WITH_GII
					/* Fixme, in some rare cases, events are queued between changing yoffset
					 * and reading them. Any ideas?
					 */
					if (xev.xmotion.y < 0
					    || xev.xmotion.y >=
					    priv->height) {
						fprintf(stderr,
							"FIXME: %d %d at yoffset=%d ph=%d\n",
							xev.xmotion.x,
							xev.xmotion.y,
							priv->yoffset,
							priv->height);
					}
#endif
					giiev.any.type = evPtrAbsolute;
					/* rc |= emPtrAbsolute; */
				}
				giiev.pmove.x = xev.xmotion.x;
				giiev.pmove.y = xev.xmotion.y;
			}
			giiev.any.size = sizeof(gii_pmove_event);
			giiev.any.origin = priv->origin[XWIN_DEV_MOUSE];
			DPRINT_EVENTS
			    ("GII_xwin_eventpoll: MouseMove %d,%d\n",
			     giiev.pmove.x, giiev.pmove.y);
			break;

		case EnterNotify:
			/* We used to pull focus here using
			 * XSetInputFocus(priv->disp, priv->win,
			 *              RevertToParent, CurrentTime);
			 * We now leave that to the window manager.
			 */
		case LeaveNotify:
			break;

		case FocusIn:
			{
				struct gii_xwin_cmddata_focus focusdata;
				focusdata.has_focus = 1;

				DPRINT_EVENTS
				    ("GII_xwin_eventpoll: FocusIn\n");
				ggBroadcast(src->instance.channel,
					    GII_CMDCODE_FOCUS, &focusdata);
			}
			break;
		case FocusOut:
			{
				struct gii_xwin_cmddata_focus focusdata;
				focusdata.has_focus = 0;

				DPRINT_EVENTS
				    ("GII_xwin_eventpoll: FocusOut\n");
				ggBroadcast(src->instance.channel,
					    GII_CMDCODE_FOCUS, &focusdata);
			}
			break;

		case Expose:
			do {
				struct gii_cmddata_expose expose;

				DPRINT_EVENTS
				    ("GII_xwin_eventpoll: Expose\n");
				expose.x = xev.xexpose.x;
				expose.y = xev.xexpose.y;
				expose.w = xev.xexpose.width;
				expose.h = xev.xexpose.height;
				expose.arg = priv->exposearg;

				ggBroadcast(src->instance.channel,
					    GII_CMDCODE_XWINEXPOSE,
					    &expose);

				giiev.any.size = sizeof(gii_expose_event);
				giiev.any.type = evExpose;
				giiev.any.origin = src->origin;	/* XXX fix this? */
				giiev.expose.x = xev.xexpose.x;
				giiev.expose.y = xev.xexpose.y;
				giiev.expose.w = xev.xexpose.width;
				giiev.expose.h = xev.xexpose.height;
			} while (0);
			break;

		case GraphicsExpose:
			do {
				struct gii_cmddata_expose expose;

				DPRINT_EVENTS
				    ("GII_xwin_eventpoll: Expose dest\n");
				expose.x = xev.xgraphicsexpose.x;
				expose.y = xev.xgraphicsexpose.y;
				expose.w = xev.xgraphicsexpose.width;
				expose.h = xev.xgraphicsexpose.height;
				expose.arg = priv->exposearg;

				ggBroadcast(src->instance.channel,
					    GII_CMDCODE_XWINEXPOSE,
					    &expose);

				giiev.any.size = sizeof(gii_expose_event);
				giiev.any.type = evExpose;
				giiev.any.origin = src->origin;	/* XXX fix this? */
				giiev.expose.x = xev.xgraphicsexpose.x;
				giiev.expose.y = xev.xgraphicsexpose.y;
				giiev.expose.w = xev.xgraphicsexpose.width;
				giiev.expose.h =
				    xev.xgraphicsexpose.height;
			} while (0);
			break;

		case ClientMessage:
			do {
				Atom wm_del_win;
				DPRINT_EVENTS
				    ("GII_xwin_eventpoll: ClientMessage\n");
				wm_del_win =
				    XInternAtom(priv->disp,
						"WM_DELETE_WINDOW", True);
				if (xev.xclient.format == 32
				    && (Atom) xev.xclient.data.l[0] ==
				    wm_del_win) {
					/* We got a WM_DELETE_WINDOW event */
					DPRINT_EVENTS
					    ("GII_xwin_eventpoll: WM_DELETE\n");

					ggBroadcast(src->instance.channel,
						    GII_CMDCODE_CLOSE,
						    NULL);

					if (priv->exit_on_delete_window) {
						exit(0);
					}
				}
			} while (0);
			break;
		case ResizeRequest:
			do {
				struct gii_cmddata_resize resize;

				DPRINT_EVENTS
				    ("GII_xwin_eventpoll: Resize\n");
				resize.width = xev.xresizerequest.width;
				resize.height = xev.xresizerequest.height / priv->frames;
				resize.arg = priv->resizearg;

				ggBroadcast(src->instance.channel,
					    GII_CMDCODE_RESIZE, &resize);

				giiev.any.origin = src->origin;	/* XXX fix this? */
			} while (0);
			break;

		case ConfigureNotify:
			if (priv->parentwin != None &&
				priv->parentwin == xev.xconfigure.window)
			{
				struct gii_cmddata_resize resize;

				DPRINT_EVENTS
				    ("GII_xwin_eventpoll: Configure\n");
				resize.width = xev.xconfigure.width;
				resize.height = xev.xconfigure.height;
				resize.arg = priv->resizearg;

				ggBroadcast(src->instance.channel,
				    GII_CMDCODE_RESIZE, &resize);

				giiev.any.origin = src->origin;	/* XXX fix this? */
			}
			break;

		case SelectionRequest:
			do {
				struct gii_xwin_cmddata_selection selection;

				DPRINT_EVENTS
				   ("GII_xwin_eventpoll: SelectionRequest\n");
				selection.owner = xev.xselectionrequest.owner;
				selection.requestor =
					xev.xselectionrequest.requestor;
				selection.selection =
					xev.xselectionrequest.selection;
				selection.target =
					xev.xselectionrequest.target;
				selection.property =
					xev.xselectionrequest.property;
				selection.time = xev.xselectionrequest.time;

				ggBroadcast(src->instance.channel,
					GII_CMDCODE_SELECTION_REQUEST,
					&selection);
			} while(0);
			break;

		case SelectionClear:
			do {
				struct gii_xwin_cmddata_selection selection;

				DPRINT_EVENTS
				    ("GII_xwin_eventpoll: SelectionClear\n");
				selection.owner = xev.xselectionclear.window;
				selection.selection =
					xev.xselectionclear.selection;
				selection.time = xev.xselectionclear.time;

				ggBroadcast(src->instance.channel,
					GII_CMDCODE_SELECTION_CLEAR,
					&selection);
			} while(0);
			break;

		default:
			DPRINT_EVENTS
			    ("GII_xwin_eventpoll: Other Event (%d)\n",
			     xev.type);
			break;

		}
		switch (giiev.any.type) {
		case evPtrButtonRelease:
			switch (xev.xbutton.button) {
			case Button4:
			case Button5:
				goto dont_queue_this_event;
			}
			/* fall through */
		case evPtrButtonPress:
			switch (xev.xbutton.button) {
			case Button4:
				giiev.any.size = sizeof(gii_pmove_event);
				giiev.any.type = evPtrRelative;
				giiev.pmove.wheel = 120;
				break;
			case Button5:
				giiev.any.size = sizeof(gii_pmove_event);
				giiev.any.type = evPtrRelative;
				giiev.pmove.wheel = -120;
				break;
			default:
				giiev.any.size = sizeof(gii_pbutton_event);
				giiev.pbutton.button =
				    _gii_xev_buttontrans(xev.xbutton.button);
			}
		}
		if (giiev.any.type)
			giiPostEvent(src, &giiev);
	      dont_queue_this_event:
		/* "ANSI C forbids label at end of compound statement"
		   Well, this makes GCC happy at least... */
		while (0) {
		};
	}
	if (priv->unlockfunc)
		priv->unlockfunc(priv->unlockarg);

	if (havecached) {
		giiPostEvent(src, &releasecache);
		/* rc |= emKeyRelease; */
		if (releasecache.key.label < 0x60) {
			priv->symstat[releasecache.key.label] = 0;
		}
	}

	return GGI_OK;
}


static void GII_xwin_close(struct gii_source *src)
{
	struct xwin_priv *priv = XWIN_PRIV(src);

	if (priv->cursor != None)
		XFreeCursor(priv->disp, priv->cursor);

	if (priv->xim) {
		XDestroyIC(priv->xic);
		XCloseIM(priv->xim);
	}

	free(priv);

	DPRINT_MISC("GII_xwin_close(%p) called\n", src);
}


static struct gii_cmddata_devinfo mouse_devinfo = {
	"Xwin Mouse",		/* device name */
	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,
	emPointer,		/* can_generate */
	1,
	0, 0
};

static struct gii_cmddata_devinfo key_devinfo = {
	"Xwin Keyboard",	/* device name */
	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,
	emKey,
	1,
	0, 0
};

static int GIIsendevent(struct gii_source *src, gii_event * ev)
{
	struct xwin_priv *priv = XWIN_PRIV(src);

	switch (ev->cmd.code) {
	case GII_CMDCODE_XWINSETPARAM:
		do {
			struct gii_xwin_cmddata_setparam data;

			/* Assure aligned memory access. Some platforms
			 * (i.e. NetBSD/sparc64) rely on this.
			 */
			memcpy(&data,
			       (struct gii_xwin_cmddata_setparam *) ev->
			       cmd.data,
			       sizeof(struct gii_xwin_cmddata_setparam));

			priv->win = data.win;
			priv->parentwin = data.parentwin;

			update_winparam(priv);
		} while (0);
		return 0;
	case GII_CMDCODE_XWINPOINTER:
		do {
			struct gii_xwin_cmddata_pointer data;

			memcpy(&data,
			       (struct gii_xwin_cmddata_pointer *) ev->cmd.
			       data,
			       sizeof(struct gii_xwin_cmddata_pointer));
			priv->alwaysrel = data.ptralwaysrel;
		} while (0);
		return 0;
	case GII_CMDCODE_PREFER_ABSPTR:
		if (priv->relptr)
			do_ungrab(priv);
		return 0;
	case GII_CMDCODE_PREFER_RELPTR:
		if (!priv->relptr)
			do_grab(priv);
		return 0;
	}

	return GGI_EEVUNKNOWN;
}


static int
GII_xwin_init(struct gii_source *src,
	      const char *target, const char *args, void *argptr)
{
	XComposeStatus dummyxcs = { NULL, 0 };
	struct gii_inputxwin_arg *xwinarg = argptr;
	struct xwin_priv *priv;
	int minkey, maxkey;

	DPRINT_LIBS("GIIdl_xwin(%p, \"%s\", \"%s\", %p) called\n", src,
		    target, args ? args : "", argptr);

	if (xwinarg == NULL || xwinarg->disp == NULL) {
		return GGI_EARGREQ;
	}

	if ((xwinarg->exposearg == NULL)
	    || (xwinarg->resizearg == NULL)) {
		DPRINT
		    ("hmm... do you try to abuse input-xwin as standalone?\n");
		DPRINT
		    ("sorry, that doesn't work. Use input-x(7) for this purpose.\n");
		DPRINT("See input-x(7) manual for more information\n");
		return GGI_EARGREQ;
	}

	priv = calloc(1, sizeof(struct xwin_priv));
	if (priv == NULL) {
		return GGI_ENOMEM;
	}


	/* Attention: priv->win is NOT valid at this
	 * point. It becomes valid once a mode has
	 * been set up by libggi's X-target.
	 */
	priv->disp = xwinarg->disp;
	priv->win = None;
	priv->parentwin = None;
	priv->compose_status = dummyxcs;
	priv->xim = NULL;
	priv->xic = NULL;
	priv->cursor = None;
	priv->oldcode = 0;
	memset(priv->symstat, 0, sizeof(priv->symstat));

	priv->alwaysrel = 0;
	priv->relptr = 0;
	priv->relptr_keymask = RELPTR_KEYINUSE;
	priv->exposearg = xwinarg->exposearg;
	priv->resizearg = xwinarg->resizearg;
	priv->lockfunc = xwinarg->lockfunc;
	priv->lockarg = xwinarg->lockarg;
	priv->unlockfunc = xwinarg->unlockfunc;
	priv->unlockarg = xwinarg->unlockarg;
	priv->exit_on_delete_window = 1;
	priv->frames = 1;
	priv->yoffset = 0;

	memset(priv->key_vector, 0, sizeof(priv->key_vector));

	if (!xwinarg->wait) {
		update_winparam(priv);
	} else {
		priv->cursor = None;
	}

	src->priv = priv;

	src->ops.event = GIIsendevent;
	src->ops.poll = GII_xwin_eventpoll;
	src->ops.close = GII_xwin_close;

	if (0 == (priv->origin[XWIN_DEV_KEY] =
		  giiAddDevice(src, &key_devinfo, NULL))) {
		GII_xwin_close(src);
		return GGI_ENOMEM;
	}

	if (0 == (priv->origin[XWIN_DEV_MOUSE] =
		  giiAddDevice(src, &mouse_devinfo, NULL))) {
		GII_xwin_close(src);
		return GGI_ENOMEM;
	}

	/* XXX add another device for expose events */

	giiSetNerveReadFD(src, &priv->nerve, ConnectionNumber(priv->disp));
	giiSetNerveTick(src, &priv->nerve_tick, 100);

	ggSetController(src->instance.channel, _gii_xwin_controller);

	mouse_devinfo.num_buttons =
	    XGetPointerMapping(priv->disp, NULL, 0);
	XDisplayKeycodes(priv->disp, &minkey, &maxkey);
	key_devinfo.num_buttons = (maxkey - minkey) + 1;

	return 0;
}


EXPORTVAR struct gii_module_source GII_xwin;

struct gii_module_source GII_xwin = {
	GG_MODULE_INIT("input-xwin", 0, 1, GII_MODULE_SOURCE),
	GII_xwin_init
};

static struct gii_module_source *_GIIdl_xwin[] = {
	&GII_xwin,
	NULL
};


EXPORTFUNC int GIIdl_xwin(int item, void **itemptr);
int GIIdl_xwin(int item, void **itemptr)
{
	struct gii_module_source ***modulesptr;
	switch (item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***) itemptr;
		*modulesptr = _GIIdl_xwin;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
