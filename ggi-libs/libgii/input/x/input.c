/* $Id: input.c,v 1.36 2009/05/20 08:38:57 pekberg Exp $
******************************************************************************

   Standalone X inputlib

   Copyright (C) 1998-1999 Marcus Sundberg	[marcus@ggi-project.org]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"

#include <ggi/gg.h>
#include <ggi/gii-module.h>
#include <ggi/internal/gii_debug.h>
#include "xev.h"

#include "common.h"


#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>
#include <ctype.h>

static const gg_option optlist[] = {
	{"nokeyfocus", "no"}
};

#define OPT_NOKEYFOCUS	0

#define NUM_OPTS	(sizeof(optlist)/sizeof(gg_option))


struct x_priv {
	Display *disp;
	Window win;
	XComposeStatus compose_status;
	XIM xim;
	XIC xic;
	unsigned int oldcode;
	uint32_t symstat[0x60];
	int width, height;
	int oldx, oldy;

	uint32_t origin[X_DEV_MAX];
	uint32_t nerve;

	int keyfocus;
};

#define X_PRIV(inp)	((struct x_priv*)((inp)->priv))


static int do_ungrab(Display * disp, Window win)
{
	XUngrabKeyboard(disp, win);
	XUngrabPointer(disp, win);
	return 0;
}

static int do_grab(Display * disp, Window win, Cursor crsr)
{
	if (XGrabKeyboard(disp, win, True, GrabModeAsync, GrabModeAsync,
			  CurrentTime)
	    != GrabSuccess ||
	    XGrabPointer(disp, win, True,
			 PointerMotionMask | ButtonPressMask |
			 ButtonReleaseMask, GrabModeAsync, GrabModeAsync,
			 win, crsr, CurrentTime)
	    != GrabSuccess) {
		DPRINT_LIBS("input-X: Unable to grab input\n");
		return GGI_ENODEVICE;
	}

	return 0;
}

static inline void center_pointer(struct x_priv *priv)
{
	XEvent event;
	event.type = MotionNotify;
	event.xmotion.display = priv->disp;
	event.xmotion.window = priv->win;
	event.xmotion.x = priv->width / 2;
	event.xmotion.y = priv->height / 2;
	XSendEvent(priv->disp, priv->win, False, PointerMotionMask,
		   &event);
	XWarpPointer(priv->disp, None, priv->win, 0, 0, 0, 0,
		     priv->width / 2, priv->height / 2);
}

static int GII_x_eventpoll(struct gii_source *src)
{
	struct x_priv *priv = X_PRIV(src);
	int n;
	/* int rc = 0; */
	gii_event releasecache;
	Time releasetime = 0;
	int havecached = 0;

	n = 0;
	while (n || (n = XPending(priv->disp))) {
		XEvent xev;
		gii_event giiev;
		unsigned int keycode;

		n--;

		XNextEvent(priv->disp, &xev);
		keycode = xev.xkey.keycode;

		if (XFilterEvent(&xev, None)) {
			priv->oldcode = keycode;
			if (xev.xkey.keycode == 0)
				continue;
		}

		giiEventBlank(&giiev, sizeof(gii_event));

		/* Note, X11 keycode range starts from 8,
		 * while GGI's keycode range starts from 0.
		 * So we need to do some transformation.
		 *
		 * XXX: Does the X11 keycode range always
		 *    start from 8 by X11 standard or should we
		 *    read out the value from somewhere?
		 *    (xdpyinfo seems to do so)
		 */

		switch (xev.type) {
		case KeyPress:
			giiev.any.size = sizeof(gii_key_event);
			giiev.any.type = evKeyPress;
			giiev.any.origin = priv->origin[X_DEV_KEY];
			giiev.key.button = keycode - 8;

			if (havecached &&
			    releasecache.key.button == giiev.key.button) {
				if (xev.xkey.time == releasetime) {
					giiev.any.type = evKeyRepeat;
					/* rc |= emKeyRepeat; */
				} else {
					giiPostEvent(src, &releasecache);
					/* rc |= emKeyRelease;
					   rc |= emKeyPress; */
					if (releasecache.key.label < 0x60) {
						priv->symstat[releasecache.key.label] = 0;
					}
				}
				havecached = 0;
			}	/* else {
				   rc |= emKeyPress;
				   } */
			_gii_xev_trans(&xev.xkey, &giiev.key,
				       &priv->compose_status, priv->xic,
				       &priv->oldcode);
			if (giiev.any.type == evKeyPress &&
			    giiev.key.label < 0x60)
			{
				priv->symstat[giiev.key.label] = giiev.key.sym;
			}

			DPRINT_EVENTS("GII_x_eventpoll: KeyPress\n");
			break;

		case KeyRelease:
			if (havecached) {
				giiPostEvent(src, &releasecache);
				/* rc |= emKeyRelease; */
			}

			giiEventBlank(&releasecache,
				      sizeof(gii_key_event));
			releasecache.any.size = sizeof(gii_key_event);
			releasecache.any.type = evKeyRelease;
			releasecache.any.origin = priv->origin[X_DEV_KEY];
			releasecache.key.button = keycode - 8;

			_gii_xev_trans(&xev.xkey, &releasecache.key,
				       &priv->compose_status, NULL, NULL);
			if (releasecache.key.label < 0x60 &&
			    priv->symstat[releasecache.key.label] != 0)
			{
				releasecache.key.sym
				    = priv->symstat[releasecache.key.label];
			}
			havecached = 1;
			releasetime = xev.xkey.time;

			DPRINT_EVENTS("GII_x_eventpoll: KeyRelease\n");
			break;

		case ButtonPress:
			giiev.any.type = evPtrButtonPress;
			giiev.any.origin = priv->origin[X_DEV_MOUSE];
			/* rc |= emPtrButtonPress; */
			DPRINT_EVENTS("GII_x_eventpoll: ButtonPress %x\n",
				      xev.xbutton.state);
			break;

		case ButtonRelease:
			giiev.any.type = evPtrButtonRelease;
			giiev.any.origin = priv->origin[X_DEV_MOUSE];
			/* rc |= emPtrButtonRelease; */
			DPRINT_EVENTS
			    ("GII_x_eventpoll: ButtonRelease %x\n",
			     xev.xbutton.state);
			break;

		case EnterNotify:
			if (priv->keyfocus) {
				/* active window get the key input focus */
				XSetInputFocus(priv->disp, priv->win,
					       RevertToParent,
					       CurrentTime);
			}	/* if */
		case LeaveNotify:
			break;

		case FocusIn:
		case FocusOut:
			break;

		case MotionNotify:
			{
				int realmove = 0;

				if (!xev.xmotion.send_event) {
					giiev.pmove.x =
					    xev.xmotion.x - priv->oldx;
					giiev.pmove.y =
					    xev.xmotion.y - priv->oldy;
					realmove = 1;
#undef ABS
#define ABS(a) (((int)(a)<0) ? -(a) : (a) )
					if (ABS(priv->width / 2 - xev.xmotion.x)
					    > priv->width / 4
					    || ABS(priv->height / 2 - xev.xmotion.y)
					    > priv->height / 4)
					{
#undef ABS
						center_pointer(priv);
					}
				}
				priv->oldx = xev.xmotion.x;
				priv->oldy = xev.xmotion.y;
				if (!realmove
				    || (giiev.pmove.x == 0
					&& giiev.pmove.y == 0))
				{
					goto dont_queue_this_event;
				}
				giiev.any.size = sizeof(gii_pmove_event);
				giiev.any.type = evPtrRelative;
				giiev.any.origin =
				    priv->origin[X_DEV_MOUSE];
				/* rc |= emPtrRelative; */
				DPRINT_EVENTS("GII_x_eventpoll: MouseMove %d,%d\n",
				     giiev.pmove.x, giiev.pmove.y);
				break;
			}
		}		/* switch */
		switch (giiev.any.type) {
		case evPtrButtonRelease:
			switch (xev.xbutton.button) {
			case Button4:
			case Button5:
				goto dont_queue_this_event;
			}
			/* fall through */
		case evPtrButtonPress:
			switch (xev.xbutton.button) {
			case Button4:
				giiev.any.size = sizeof(gii_pmove_event);
				giiev.any.type = evPtrRelative;
				giiev.pmove.wheel = 120;
				break;
			case Button5:
				giiev.any.size = sizeof(gii_pmove_event);
				giiev.any.type = evPtrRelative;
				giiev.pmove.wheel = -120;
				break;
			default:
				giiev.any.size = sizeof(gii_pbutton_event);
				giiev.pbutton.button =
				    _gii_xev_buttontrans(xev.xbutton.button);
			}
		}
		if (giiev.any.type) {
			giiPostEvent(src, &giiev);
		}
	      dont_queue_this_event:
		/* "ANSI C forbids label at end of compound statement"
		   Well, this makes GCC happy at least... */
		while (0) {
		};
	}
	if (havecached) {
		giiPostEvent(src, &releasecache);
		/* rc |= emKeyRelease; */
		if (releasecache.key.label < 0x60) {
			priv->symstat[releasecache.key.label] = 0;
		}
	}

	return GGI_OK;
}

static void GII_x_close(struct gii_source *src)
{
	struct x_priv *priv = X_PRIV(src);

	if (priv->xim) {
		XDestroyIC(priv->xic);
		XCloseIM(priv->xim);
	}

	do_ungrab(priv->disp, priv->win);

	XDestroyWindow(priv->disp, priv->win);
	XCloseDisplay(priv->disp);
	free(priv);
}


static struct gii_cmddata_devinfo mouse_devinfo = {
	"X Mouse",		/* device name */
	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,
	emPointer,		/* can_generate */
	1,
	0, 0
};

static struct gii_cmddata_devinfo key_devinfo = {
	"X Keyboard",		/* device name */
	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,
	emKey,
	1,
	0, 0
};


static int
GII_x_init(struct gii_source *src,
	   const char *target, const char *args, void *argptr)
{
	Display *disp;
	Window win;
	Screen *sc;
	XSetWindowAttributes attr;
	XComposeStatus dummyxcs = { NULL, 0 };
	int scr;
	Cursor crsr;
	XEvent xev;
	struct x_priv *priv;
	int minkey, maxkey;
	gg_option options[NUM_OPTS];

	DPRINT_LIBS("GIIdl_x(%p, \"%s\", \"%s\", %p) called\n", src,
		    target, args ? args : "", argptr);

	/* handle args */
	memcpy(options, optlist, sizeof(options));

	if (args) {
		args = ggParseOptions(args, options, NUM_OPTS);
		if (args == NULL) {
			fprintf(stderr, "input-x: error in "
				"arguments.\n");
			return GGI_EARGINVAL;
		}
	}


	if ((disp = XOpenDisplay(NULL)) == NULL) {
		DPRINT_LIBS("input-X: Unable to open display\n");
		return GGI_ENODEVICE;
	}

	scr = XScreenNumberOfScreen(sc = DefaultScreenOfDisplay(disp));

	attr.event_mask =
	    KeyPressMask | KeyReleaseMask |
	    ButtonPressMask | ButtonReleaseMask | PointerMotionMask |
	    FocusChangeMask;
	win = XCreateWindow(disp, RootWindow(disp, scr), 0, 0,
			    WidthOfScreen(sc) / 2U,
			    HeightOfScreen(sc) / 2U, 0, 0, InputOnly,
			    CopyFromParent, CWEventMask, &attr);

	XMapRaised(disp, win);
	XSync(disp, 0);

	/* Wait until window is mapped */
	XNextEvent(disp, &xev);

	/* Move to top left corner in case the WM uses interactive placement */
	XMoveWindow(disp, win, 0, 0);

	crsr = _gii_x_make_cursor(disp, win);

	if (do_grab(disp, win, crsr) != 0) {
		XDestroyWindow(disp, win);
		XCloseDisplay(disp);
		return GGI_ENODEVICE;
	}

	priv = calloc(1, sizeof(struct x_priv));
	if (priv == NULL) {
		XDestroyWindow(disp, win);
		XCloseDisplay(disp);
		return GGI_ENOMEM;
	}

	priv->disp = disp;
	priv->win = win;
	priv->xim = NULL;
	priv->xic = NULL;
	priv->oldcode = 0;
	priv->compose_status = dummyxcs;
	memset(priv->symstat, 0, sizeof(priv->symstat));

	{
		Window dummywin;
		unsigned int w, h, dummy;

		XGetGeometry(disp, win, &dummywin, (int *) &dummy,
			     (int *) &dummy, &w, &h, &dummy, &dummy);
		priv->width = w;
		priv->height = h;
		priv->oldx = w / 2;
		priv->oldy = h / 2;
		center_pointer(priv);
	}

	priv->xim = XOpenIM(priv->disp, NULL, NULL, NULL);
	if (priv->xim) {
		priv->xic = XCreateIC(priv->xim, XNInputStyle,
				      XIMPreeditNothing | XIMStatusNothing,
				      XNClientWindow, priv->win,
				      XNFocusWindow, priv->win, NULL);
		if (!priv->xic) {
			XCloseIM(priv->xim);
			priv->xim = NULL;
		}
	} else {
		priv->xic = NULL;
	}

	src->priv = priv;

	src->ops.poll = GII_x_eventpoll;
	src->ops.close = GII_x_close;

	if (tolower((uint8_t) options[OPT_NOKEYFOCUS].result[0]) == 'n') {
		priv->keyfocus = 1;
	} else {
		priv->keyfocus = 0;
	}


	if (0 == (priv->origin[X_DEV_KEY] =
		  giiAddDevice(src, &key_devinfo, NULL))) {
		GII_x_close(src);
		return GGI_ENOMEM;

	}

	if (0 == (priv->origin[X_DEV_MOUSE] =
		  giiAddDevice(src, &mouse_devinfo, NULL))) {
		GII_x_close(src);
		return GGI_ENOMEM;
	}

	giiSetNerveReadFD(src, &priv->nerve, ConnectionNumber(disp));

	mouse_devinfo.num_buttons =
	    XGetPointerMapping(priv->disp, NULL, 0);
	XDisplayKeycodes(priv->disp, &minkey, &maxkey);
	key_devinfo.num_buttons = (maxkey - minkey) + 1;

	return 0;
}


struct gii_module_source GII_x = {
	GG_MODULE_INIT("input-x", 0, 1, GII_MODULE_SOURCE),
	GII_x_init
};

static struct gii_module_source *_GIIdl_x[] = {
	&GII_x,
	NULL
};


EXPORTFUNC int GIIdl_x(int item, void **itemptr);
int GIIdl_x(int item, void **itemptr)
{
	struct gii_module_source ***modulesptr;
	switch (item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***) itemptr;
		*modulesptr = _GIIdl_x;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
