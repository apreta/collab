/*
******************************************************************************

   xf86dga inputlib - use DGA input events as a LibGII input source

   Copyright (C) 2005	Joseph Crayne		[oh.hello.joe@gmail.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/extensions/xf86dga.h>

#include "config.h"
#include <ggi/gg.h>
#include <ggi/gii-module.h>
#include <ggi/internal/gii_debug.h>
#include <ggi/input/xf86dga.h>
#include "../x/xev.h"


enum {
	XDGA_DEV_KEY = 0,
	XDGA_DEV_MAX
};

struct xdga_priv {
	Display *disp;
	int screen;
	XComposeStatus compose_status;
	uint32_t origin[XDGA_DEV_MAX];
	char key_vector[32];
	int event_base;
	int error_base;
	uint32_t nerve;
};

#define XDGA_PRIV(inp)	((struct xdga_priv*)((inp)->priv))

static inline void prepare_key_event(struct gii_source *src,
				     gii_event * giiev, int keycode,
				     XDGAEvent * dgaev)
{
	struct xdga_priv *priv = XDGA_PRIV(src);
	XComposeStatus compose_status;
	XKeyEvent xkeyev;
	KeySym xsym;

	giiev->any.size = sizeof(gii_key_event);
	giiev->any.origin = priv->origin[XDGA_DEV_KEY];
	giiev->key.button = keycode - 8;
	XDGAKeyEventToXKeyEvent((XDGAKeyEvent *) dgaev, &xkeyev);
	XLookupString(&xkeyev, NULL, 0, &xsym, &compose_status);
	giiev->key.sym = basic_trans(xsym, 0);
	giiev->key.label = basic_trans(XLookupKeysym(&xkeyev, 0), 1);
	/*
	   _gii_xev_trans( &xkeyev, &giiev.key, &compose_status,
	   0, &oldcode_dummy );
	 */
}


/* Methods for a using an array of char as a packed binary
 * array */
#define SetFlag( packed, index ) packed[index/8]|=1<<(index&7)
#define ClearFlag( packed, index ) packed[index/8]&=~(1<<(index&7));
#define TestFlag( packed, index ) \
	(priv->key_vector[keycode/8] & (1<<(keycode&7)))

static int GII_xdga_eventpoll(struct gii_source *src)
{
	struct xdga_priv *priv = XDGA_PRIV(src);
	XEvent xev;
	XDGAEvent *dgaev = (XDGAEvent *) & xev;
	gii_event giiev;
	int n;
	int rc = 0;
	int dga_event_base = priv->event_base;


	XSync(priv->disp, False);

	for (n = XEventsQueued(priv->disp, QueuedAfterReading); n; n--) {

		int keycode;

		XNextEvent(priv->disp, &xev);
		keycode = dgaev->xkey.keycode;

		giiEventBlank(&giiev, sizeof(gii_event));

		switch (dgaev->type - dga_event_base) {

		case KeyPress:
			prepare_key_event(src, &giiev, keycode, dgaev);
			if (TestFlag(priv->key_vector, keycode)) {
				giiev.any.type = evKeyRepeat;
				rc |= emKeyRepeat;
			} else {
				giiev.any.type = evKeyPress;
				rc |= emKeyPress;
			}
			/*DPRINT("press(%c)\n", giiev.key.sym); */
			SetFlag(priv->key_vector, keycode);
			giiPostEvent(src, &giiev);
			break;

		case KeyRelease:
			prepare_key_event(src, &giiev, keycode, dgaev);
			giiev.any.type = evKeyRelease;
			rc |= emKeyRelease;
			/* DPRINT("release(%c)\n", giiev.key.sym); */
			ClearFlag(priv->key_vector, keycode);
			giiPostEvent(src, &giiev);
			break;
		}

	}

	return GGI_OK;
}


static void GII_xdga_close(struct gii_source *src)
{
	struct xdga_priv *priv = XDGA_PRIV(src);

	DPRINT_MISC("GII_xdga_close(%p) called\n", src);

	free(priv);
}

#if 0
static struct gii_cmddata_devinfo mouse_devinfo = {
	"XFree86-DGA Mouse",	/* device name */
	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,
	emPointer,		/* can_generate */
	1,
	0, 0
};
#endif

static struct gii_cmddata_devinfo key_devinfo = {
	"XFree86-DGA Keyboard",	/* device name */
	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,
	emKey,
	1,
	0, 0
};


static int
GII_xf86dga_init(struct gii_source *src,
		 const char *target, const char *args, void *argptr)
{
	gii_inputxf86dga_arg *xdgaarg = argptr;
	struct xdga_priv *priv;
	int minkey, maxkey;

	DPRINT_LIBS("GIIdl_xf86dga(%p, \"%s\", \"%s\", %p) called\n", src,
		    target, args ? args : "", argptr);

	/* TODO: use XOpenDisplay to get a display if one wasnt
	 * provided? */
	if (xdgaarg == NULL || xdgaarg->disp == NULL) {
		return GGI_EARGREQ;
	}

	priv = calloc(1, sizeof(struct xdga_priv));
	if (priv == NULL) {
		return GGI_ENOMEM;
	}


	priv->disp = xdgaarg->disp;
	priv->screen = xdgaarg->screen;

	memset(priv->key_vector, 0, sizeof(priv->key_vector));

	src->priv = priv;

	src->ops.poll = GII_xdga_eventpoll;
	src->ops.close = GII_xdga_close;

	priv->origin[XDGA_DEV_KEY] = giiAddDevice(src, &key_devinfo, NULL);
	if (priv->origin[XDGA_DEV_KEY] == 0) {
		GII_xdga_close(src);
		return GGI_ENOMEM;
	}

	/*
	   priv->origin[XDGA_DEV_MOUSE] = giiAddDevice(src,&mouse_devinfo,NULL);
	   if (priv->origin[XDGA_DEV_MOUSE] == 0) {
		GII_xdga_close(src);
		return GGI_ENOMEM;
	   }
	 */

	giiSetNerveReadFD(src, &priv->nerve, ConnectionNumber(priv->disp));

	/*
	   mouse_devinfo.num_buttons = XGetPointerMapping(priv->disp, NULL, 0);
	 */
	XDisplayKeycodes(priv->disp, &minkey, &maxkey);
	key_devinfo.num_buttons = (maxkey - minkey) + 1;

	XDGAQueryExtension(priv->disp, &priv->event_base,
			   &priv->error_base);

	XSync(priv->disp, True);
	XDGASelectInput(priv->disp, priv->screen,
			KeyPressMask | KeyReleaseMask);

	return 0;
}


EXPORTVAR struct gii_module_source GII_xf86dga;

struct gii_module_source GII_xf86dga = {
	GG_MODULE_INIT("input-xf86dga", 0, 1, GII_MODULE_SOURCE),
	GII_xf86dga_init
};

static struct gii_module_source *_GIIdl_xf86dga[] = {
	&GII_xf86dga,
	NULL
};


EXPORTFUNC int GIIdl_xf86dga(int item, void **itemptr);
int GIIdl_xf86dga(int item, void **itemptr)
{
	struct gii_module_source ***modulesptr;
	switch (item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***) itemptr;
		*modulesptr = _GIIdl_xf86dga;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
