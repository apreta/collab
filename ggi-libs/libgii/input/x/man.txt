X11 standalone input
~~~~~~~~~~~~~~~~~~~~

.. manpage:: 7 input-x


Synopsis
--------

::

  input-x [-nokeyfocus]

  input-xwin

  input-xf86dga


Description
-----------

input-x and input-xwin, an input target for X11 Environments.
input-x opens a X11 window to receive input from and
works standalone.

input-xwin and input-xf86dga is used by libggi's :man:`display-x(7)`.
They are NOT intended to be directly used by applications.

X11R5 is required.


Options
-------

``-nokeyfocus``
    Disables keyboard focus. See the XSetInputFocus(3X11) manpage for the details
    on keyboard focus, where *focus* is a 'window' and *revert_to* is 'RevertToParent'.


Controls
--------

An `input-x` module understands the following command codes sent by
`ggControl`:

`GII_CMDCODE_XWINPOINTER`
   Data::

	struct gii_xwin_cmddata_pointer {
	       int ptralwaysrel;
	};

`GII_CMDCODE_XWINSETPARAM`
   Data::

	struct gii_xwin_cmddata_setparam {
	       Window	 win;
	       Window	 parentwin;
	       int       frames;
	};


Notifications
-------------

An `input-x` module will report the following events via
`ggBroadcast`:


`GII_CMDCODE_RESIZE`
   Data::

	struct gii_cmddata_resize {
	       int width, height;	/* New width/height */
	       void *arg;
	};

`GII_CMDCODE_XWINEXPOSE`
   Data::

	struct gii_cmddata_expose {
	       int x, y, w, h;
	       void *arg;
	};
