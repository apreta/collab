/* $Id: input.c,v 1.34 2008/01/18 23:30:58 cegger Exp $
******************************************************************************

   fdselect inputlib - send information when filedescriptors become ready

   Copyright (C) 1999 Alexander Peuchert	[peuc@comnets.rwth-aachen.de]
   Copyright (C) 1999 Andreas Beck		[becka@ggi-project.org]
   Copyright (C) 2006 Peter Rosin		[peda@lysator.liu.se]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "config.h"
#include <ggi/gg.h>
#include <ggi/gii-module.h>
#include <ggi/internal/gii_debug.h>
#include <ggi/input/fdselect.h>


#define FD_OPTIONS \
	FD_OPTION(auto,   "")      \
	FD_OPTION(except, "")      \
	FD_OPTION(notify, "fd")    \
	FD_OPTION(read,   "")      \
	FD_OPTION(write,  "")

#define FD_OPTION(name, default) \
	{ #name, default },
static const gg_option optlist[] = {
	FD_OPTIONS
};
#undef FD_OPTION

#define FD_OPTION(name, default) \
	OPT_ ## name,
enum {
	FD_OPTIONS
	NUM_OPTS
};
#undef FD_OPTION

struct fd_entry {
	GG_LIST_ENTRY(fd_entry) others;
	int fd;
	int current;
	int cache;
	uint32_t rnerve;
	uint32_t wnerve;
	uint32_t enerve;
};

#define NOTIFY_SET 0
#define NOTIFY_FD  1

struct fd_priv {
	GG_LIST_HEAD(fds, fd_entry) fds;
	uint32_t origin;
	int auto_reload;
	int notify;
};

#define FD_PRIV(src)  ((struct fd_priv *) src->priv)

/* ---------------------------------------------------------------------- */

static void load_fd(struct gii_source *src, struct fd_entry *fd, int mode)
{
	fd->current |= mode;

	if (mode & GII_FDSELECT_READ)
		giiSetNerveFD(src, &fd->rnerve, fd->fd, GII_NERVE_FD_READ);
	if (mode & GII_FDSELECT_WRITE)
		giiSetNerveFD(src, &fd->wnerve, fd->fd,
			      GII_NERVE_FD_WRITE);
	if (mode & GII_FDSELECT_EXCEPT)
		giiSetNerveFD(src, &fd->enerve, fd->fd,
			      GII_NERVE_FD_EXCEPT);
}

static void add_fd(struct gii_source *src, int new_fd, int mode)
{
	struct fd_priv *priv = FD_PRIV(src);
	struct fd_entry *fd;

	GG_LIST_FOREACH(fd, &priv->fds, others) {
		if (fd->fd == new_fd)
			break;
	}

	if (!fd) {
		fd = malloc(sizeof(*fd));
		fd->fd = new_fd;
		fd->current = 0;
		fd->cache = 0;
		fd->rnerve = 0;
		fd->wnerve = 0;
		fd->enerve = 0;
		GG_LIST_INSERT_HEAD(&priv->fds, fd, others);
	}

	mode &=
	    GII_FDSELECT_READ | GII_FDSELECT_WRITE | GII_FDSELECT_EXCEPT;

	mode &= ~fd->current;
	fd->cache |= mode;

	load_fd(src, fd, mode);
}


static void reload_cache(struct gii_source *src)
{
	struct fd_priv *priv = FD_PRIV(src);
	struct fd_entry *fd;

	GG_LIST_FOREACH(fd, &priv->fds, others)
	    load_fd(src, fd, fd->cache & ~fd->current);
}


static void delete_fd(struct gii_source *src, int old_fd, int mode)
{
	struct fd_priv *priv = FD_PRIV(src);
	struct fd_entry *fd;
	int current;

	GG_LIST_FOREACH(fd, &priv->fds, others) {
		if (fd->fd == old_fd)
			break;
	}

	if (!fd)
		return;

	mode &=
	    GII_FDSELECT_READ | GII_FDSELECT_WRITE | GII_FDSELECT_EXCEPT;
	current = mode & fd->current;
	if (current & GII_FDSELECT_READ) {
		giiCutNerve(src, &fd->rnerve);
		fd->rnerve = 0;
	}
	if (current & GII_FDSELECT_WRITE) {
		giiCutNerve(src, &fd->wnerve);
		fd->wnerve = 0;
	}
	if (current & GII_FDSELECT_EXCEPT) {
		giiCutNerve(src, &fd->enerve);
		fd->enerve = 0;
	}

	fd->current &= ~mode;
	fd->cache &= ~mode;
}

static void delete_all(struct gii_source *src)
{
	struct fd_priv *priv = FD_PRIV(src);
	struct fd_entry *fd;

	GG_LIST_FOREACH(fd, &priv->fds, others) {
		if (fd->current & GII_FDSELECT_READ)
			giiCutNerve(src, &fd->rnerve);
		fd->rnerve = 0;
		if (fd->current & GII_FDSELECT_WRITE)
			giiCutNerve(src, &fd->wnerve);
		fd->wnerve = 0;
		if (fd->current & GII_FDSELECT_EXCEPT)
			giiCutNerve(src, &fd->enerve);
		fd->enerve = 0;
		fd->current = 0;
		fd->cache = 0;
	}
}

static void
set_fdset(struct gii_source *src, struct gii_fdselect_fdset *fdset)
{
	int fd;
	int mode;

	delete_all(src);

	for (fd = 0; fd < fdset->maxfd; ++fd) {
		mode = 0;
		if (fdset->rfd && FD_ISSET(fd, fdset->rfd))
			mode |= GII_FDSELECT_READ;
		if (fdset->wfd && FD_ISSET(fd, fdset->wfd))
			mode |= GII_FDSELECT_WRITE;
		if (fdset->efd && FD_ISSET(fd, fdset->efd))
			mode |= GII_FDSELECT_EXCEPT;
		if (!mode)
			continue;
		add_fd(src, fd, mode);
	}
}

static void
get_fdset(struct gii_source *src, struct gii_fdselect_fdset *fdset)
{
	struct fd_priv *priv = FD_PRIV(src);
	struct fd_entry *fd;
	int mode;

	fdset->maxfd = 0;
	mode = 0;
	if (fdset->rfd) {
		FD_ZERO(fdset->rfd);
		mode |= GII_FDSELECT_READ;
	}
	if (fdset->wfd) {
		FD_ZERO(fdset->wfd);
		mode |= GII_FDSELECT_WRITE;
	}
	if (fdset->efd) {
		FD_ZERO(fdset->efd);
		mode |= GII_FDSELECT_EXCEPT;
	}

	GG_LIST_FOREACH(fd, &priv->fds, others) {
		if (fdset->rfd && fd->current & GII_FDSELECT_READ)
			FD_SET(fd->fd, fdset->rfd);
		if (fdset->wfd && fd->current & GII_FDSELECT_WRITE)
			FD_SET(fd->fd, fdset->wfd);
		if (fdset->efd && fd->current & GII_FDSELECT_EXCEPT)
			FD_SET(fd->fd, fdset->efd);
		if (!(fd->current & mode))
			continue;
		if (fd->fd >= fdset->maxfd)
			fdset->maxfd = fd->fd + 1;
	}
}

static int GII_fd_controller(void *arg, uint32_t flag, void *data)
{
	struct gii_source *src = arg;
	struct gii_fdselect_fd *fd;

	switch (flag) {
	case GII_FDSELECT_RELOAD:
		reload_cache(src);
		break;

	case GII_FDSELECT_ADD:
		fd = data;
		add_fd(src, fd->fd, fd->mode);
		break;

	case GII_FDSELECT_DEL:
		fd = data;
		delete_fd(src, fd->fd, fd->mode);
		break;

	case GII_FDSELECT_DEL_ALL:
		delete_all(src);
		break;

	case GII_FDSELECT_SET_FDSET:
		set_fdset(src, data);
		break;

	case GII_FDSELECT_GET_FDSET:
		get_fdset(src, data);
		break;

	default:
		return GGI_ENOFUNC;
	}

	return GGI_OK;
}


static int post_event(struct gii_source *src, uint32_t flag, int fd)
{
	struct fd_priv *priv = FD_PRIV(src);
	gii_event ev;

	/* at least one of the fds is readable -> send information event */

	giiEventBlank(&ev, sizeof(gii_cmd_nodata_event));

	ev.any.size = sizeof(gii_cmd_nodata_event);
	ev.any.type = evInformation;
	ev.any.origin = priv->origin;
	ev.cmd.code = GII_CMDFLAG_PRIVATE;
	/* fixme - be more precise ! */
	giiPostEvent(src, &ev);

	return GGI_OK;
}


static int GII_fd_poll(struct gii_source *src)
{
	struct fd_priv *priv = FD_PRIV(src);
	struct fd_entry *fd, *next;
	struct gii_fdselect_fd fd_ready;
	int post = 0;
	struct gii_fdselect_fdset fdset;
	fd_set rfd;
	fd_set wfd;
	fd_set efd;

	DPRINT_EVENTS("poll(%p);\n", src);

	if (priv->notify == NOTIFY_SET) {
		memset(&fdset, 0, sizeof(fdset));
		fdset.rfd = &rfd;
		fdset.wfd = &wfd;
		fdset.efd = &efd;
		FD_ZERO(&rfd);
		FD_ZERO(&wfd);
		FD_ZERO(&efd);
	}

	for (fd = GG_LIST_FIRST(&priv->fds); fd; fd = next) {
		next = GG_LIST_NEXT(fd, others);

		if (!fd->cache) {
			GG_LIST_REMOVE(fd, others);
			free(fd);
			continue;
		}

		fd_ready.mode = 0;

		if (fd->rnerve) {
			fd_ready.mode |= GII_FDSELECT_READ;
			if (!priv->auto_reload)
				giiCutNerve(src, &fd->rnerve);
			if (priv->notify == NOTIFY_SET)
				FD_SET(fd->fd, &rfd);
			fd->rnerve = 0;
		}
		if (fd->wnerve) {
			fd_ready.mode |= GII_FDSELECT_WRITE;
			if (!priv->auto_reload)
				giiCutNerve(src, &fd->wnerve);
			if (priv->notify == NOTIFY_SET)
				FD_SET(fd->fd, &wfd);
			fd->wnerve = 0;
		}
		if (fd->enerve) {
			fd_ready.mode |= GII_FDSELECT_EXCEPT;
			if (!priv->auto_reload)
				giiCutNerve(src, &fd->enerve);
			if (priv->notify == NOTIFY_SET)
				FD_SET(fd->fd, &efd);
			fd->enerve = 0;
		}
		if (fd_ready.mode) {
			if (!priv->auto_reload)
				fd->current &= ~fd_ready.mode;
			fd_ready.fd = fd->fd;
			if (priv->notify == NOTIFY_SET) {
				if (fd->fd >= fdset.maxfd)
					fdset.maxfd = fd->fd + 1;
			} else if (priv->notify == NOTIFY_FD)
				ggBroadcast(src->instance.channel,
					    GII_FDSELECT_READY, &fd_ready);
			post = 1;
		}
	}
	if (post) {
		if (priv->notify == NOTIFY_SET)
			ggBroadcast(src->instance.channel,
				    GII_FDSELECT_READY_FDSET, &fdset);
		post_event(src, 0, fd_ready.fd);
	}

	return GGI_OK;
}


static void GII_fd_close(struct gii_source *src)
{
	struct fd_priv *priv = FD_PRIV(src);
	struct fd_entry *fd;

	while (!GG_LIST_EMPTY(&priv->fds)) {
		fd = GG_LIST_FIRST(&priv->fds);
		GG_LIST_REMOVE(fd, others);
		free(fd);
	}

	free(priv);

	DPRINT_MISC("closed\n");

	return;
}


static struct gii_cmddata_devinfo devinfo = {
	"File descriptor input",	/* long device name */
	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,
	emInformation,		/* can_generate */
	0,			/* no registers */
	0,			/* no buttons */
	0			/* no valuators */
};


#define SKIPNONNUM(str)	while(!isdigit((uint8_t)(*str)) && (*str) != '\0'){str++;}
static int dosetup(struct gii_source *src, int mode, const char *result)
{
	SKIPNONNUM(result);

	while (*result != '\0') {
		int curfd;
		char *remain;

		curfd = strtol(result, &remain, 0);
		if (remain == result) {
			/* Parse error */
			return GGI_ENOMATCH;
		}

		add_fd(src, curfd, mode);

		result = remain;
		SKIPNONNUM(result);
	}

	return 0;
}


static int
GII_fdselect_init(struct gii_source *src,
		  const char *target, const char *args, void *argptr)
{
	struct fd_priv *priv;
	gg_option options[NUM_OPTS];

	DPRINT_MISC("starting. (args=\"%s\",argptr=%p)\n", args, argptr);

	/* handle args */
	memcpy(options, optlist, sizeof(options));
	args = ggParseOptions(args, options, NUM_OPTS);
	if (args == NULL) {
		fprintf(stderr, "input-fdselect: error in arguments.\n");
		return GGI_EARGINVAL;
	}

	/* allocate private stuff */
	priv = calloc(1, sizeof(struct fd_priv));
	if (priv == NULL) {
		return GGI_ENOMEM;
	}

	src->priv = priv;
	GG_LIST_INIT(&priv->fds);

	priv->origin = giiAddDevice(src, &devinfo, NULL);
	if (priv->origin == 0) {
		free(priv);
		return GGI_ENOMEM;
	}

	if (dosetup(src, GII_FDSELECT_READ, options[OPT_read].result)) {
		DPRINT_MISC("error parsing -read arguments\n");
	}
	if (dosetup(src, GII_FDSELECT_WRITE, options[OPT_write].result)) {
		DPRINT_MISC("error parsing -write arguments\n");
	}
	if (dosetup(src, GII_FDSELECT_EXCEPT, options[OPT_except].result)) {
		DPRINT_MISC("error parsing -except arguments\n");
	}

	if (options[OPT_auto].result[0] != 'n')
		priv->auto_reload = 1;
	else
		priv->auto_reload = 0;

	if (options[OPT_notify].result[0] != 'f')
		priv->notify = NOTIFY_SET;
	else
		priv->notify = NOTIFY_FD;

	src->ops.poll = GII_fd_poll;
	src->ops.close = GII_fd_close;

	ggSetController(src->instance.channel, GII_fd_controller);

	DPRINT_MISC("fully up\n");

	return 0;
}


struct gii_module_source GII_fdselect = {
	GG_MODULE_INIT("input-fdselect", 0, 1, GII_MODULE_SOURCE),
	GII_fdselect_init
};

static struct gii_module_source *_GIIdl_fdselect[] = {
	&GII_fdselect,
	NULL
};


EXPORTFUNC int GIIdl_fdselect(int item, void **itemptr);
int GIIdl_fdselect(int item, void **itemptr)
{
	struct gii_module_source ***modulesptr;
	switch (item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***) itemptr;
		*modulesptr = _GIIdl_fdselect;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
