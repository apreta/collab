/* $Id: input.c,v 1.22 2008/01/19 00:53:19 cegger Exp $
******************************************************************************

   Mouse inputlib init function

   Copyright (C) 1998-1999 Marcus Sundberg	[marcus@ggi-project.org]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/
#include "config.h"
#include "mouse.h"

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>

#ifdef HAVE_STRINGS_H
#include <strings.h>
#endif

#define SKIPWHITE(str)	while (isspace((uint8_t)*(str))) (str)++

static struct gii_cmddata_devinfo devinfo = {
	"Raw Mouse",		/* long device name */
	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,
	emPointer,		/* can_generate */
	GII_NUM_UNKNOWN,	/* num_registers */
	0,			/* num_valuators (only for valuators) */
	4,			/* num_buttons   (no supported device have more) */
};

static inline struct parser_type *match_mtype(const char *mtype)
{
	int i, j;

	SKIPWHITE(mtype);
	if (*mtype == ',')
		mtype++;
	SKIPWHITE(mtype);

	for (i = 0; _gii_mouse_parsers[i] != NULL; i++) {
		for (j = 0; _gii_mouse_parsers[i]->names[j] != NULL; j++) {
			if (strcasecmp(mtype, _gii_mouse_parsers[i]->names[j]) == 0)
			{
				return _gii_mouse_parsers[i];
			}
		}
	}
	return NULL;
}


static int
GII_mouse_init(struct gii_source *src,
	       const char *target, const char *args, void *argptr)
{
	char *mtype;
	struct parser_type *parser;
	struct mouse_priv *mpriv;
	int fd, fallback_parser = 0;

	DPRINT_LIBS("GIIdl_mouse(%p, \"%s\", \"%s\", %p) called\n", src,
		    target, args ? args : "", argptr);

	if (!(args && *args
	      && (fd = strtol(args, &mtype, 0)) >= 0 && mtype != args
	      /* FIXME - we should allow PnP for the mousetype */
	      && *mtype != '\0'))
	{
		return GGI_EARGREQ;
	}

	parser = match_mtype(mtype);
	if (parser == NULL) {
		return GGI_EARGINVAL;
	}
	if (parser->init_data) {
		if (write(fd, parser->init_data, (unsigned) parser->init_len)
		    != parser->init_len)
		{
			switch (parser->init_type) {
			case GII_MIT_DONTCARE:
				break;
			case GII_MIT_MUST:
				return GGI_ENODEVICE;
			case GII_MIT_FALLBACK:
				fallback_parser = 1;
				break;
			}
		}
	}

	mpriv = calloc(1, sizeof(struct mouse_priv));
	if (mpriv == NULL) {
		return GGI_ENOMEM;
	}

	mpriv->origin = giiAddDevice(src, &devinfo, NULL);
	if (mpriv->origin == 0) {
		free(mpriv);
		return GGI_ENOMEM;
	}

	src->ops.poll = GII_mouse_poll;
	src->ops.close = NULL;

	if (fallback_parser) {
		mpriv->parser = parser->fbparser->parser;
	} else {
		mpriv->parser = parser->parser;
	}
	mpriv->min_packet_len = parser->min_packet_len;
	mpriv->fd = fd;
	mpriv->eof = 0;

	mpriv->parse_state = mpriv->button_state = 0;
	mpriv->packet_len = 0;
	mpriv->sent = 0;

	src->priv = mpriv;

	giiSetNerveReadFD(src, &mpriv->nerve, mpriv->fd);

	DPRINT_LIBS("mouse fully up\n");

	return 0;
}


struct gii_module_source GII_mouse = {
	GG_MODULE_INIT("input-mouse", 0, 1, GII_MODULE_SOURCE),
	GII_mouse_init
};

static struct gii_module_source *_GIIdl_mouse[] = {
	&GII_mouse,
	NULL
};


EXPORTFUNC int GIIdl_mouse(int item, void **itemptr);
int GIIdl_mouse(int item, void **itemptr)
{
	struct gii_module_source ***modulesptr;
	switch (item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***) itemptr;
		*modulesptr = _GIIdl_mouse;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
