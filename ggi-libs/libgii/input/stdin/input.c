/* $Id: input.c,v 1.24 2008/01/19 01:04:24 cegger Exp $
******************************************************************************

   Input-stdin: initialization

   Copyright (C) 1998 Andreas Beck      [becka@ggi-project.org]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "config.h"

#include <ggi/gg.h>
#include <ggi/gii-module.h>
#include <ggi/internal/gii_debug.h>

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifdef HAVE_TERMIOS_H
#include <termios.h>
#define USE_TERMIOS
#endif


static const gg_option optlist[] = {
	{"noraw", "no"},
	{"ansikey", "no"},
};

#define OPT_NORAW	0
#define OPT_ANSIKEY	1

#define NUM_OPTS	(sizeof(optlist)/sizeof(gg_option))


struct stdin_priv {
	uint32_t origin;
	uint32_t nerve;
	int rawmode, ansikey;

#ifdef USE_TERMIOS
	struct termios old_termios;
#endif
};

#define STDIN_PRIV(inp)  ((struct stdin_priv *) inp->priv)

#define STDIN_FD	0


/* ---------------------------------------------------------------------- */


static gii_event_mask GII_send_key(struct gii_source *src, uint32_t sym)
{
	gii_event ev;

	giiEventBlank(&ev, sizeof(gii_key_event));

	ev.any.size = sizeof(gii_key_event);
	ev.any.type = evKeyPress;
	ev.any.origin = STDIN_PRIV(src)->origin;
	ev.key.modifiers = 0;
	ev.key.sym = sym;
	ev.key.label = sym;
	ev.key.button = sym;
	giiPostEvent(src, &ev);

	ev.key.type = evKeyRelease;
	giiPostEvent(src, &ev);

	return emKeyPress | emKeyRelease;
}

static int GII_stdin_poll(struct gii_source *src)
{
	unsigned char buf[6];
	ssize_t nread;

	DPRINT("input-stdin: poll(%p);\n", src);

	while ((nread = read(STDIN_FD, buf, 1))) {
		/* XXX To handle ansi escape, define a return code that ask
		 * libgii to poll again in a very short time.
		 */
		DPRINT("**input-stdin read (%i) : %i\n", nread,
		       (int) (buf[0]));
		GII_send_key(src, (uint32_t) buf[0]);
	}
	return GGI_OK;
}


static void GII_stdin_close(struct gii_source *src)
{
	struct stdin_priv *priv = STDIN_PRIV(src);

#ifdef USE_TERMIOS
	if (priv->rawmode) {
		if (tcsetattr(STDIN_FD, TCSANOW, &priv->old_termios) < 0) {
			perror("input-stdin: tcsetattr failed");
		}
		ggUnregisterCleanup((ggcleanup_func *) GII_stdin_close,
				    (void *) src);
	}
#endif
	free(priv);

	DPRINT_MISC("input-stdin: closed\n");
}


#ifdef USE_TERMIOS
static void GII_stdin_setraw(struct gii_source *src)
{
	struct stdin_priv *priv = STDIN_PRIV(src);

	struct termios newt;

	/* put the tty into "straight through" mode. */
	if (tcgetattr(STDIN_FD, &priv->old_termios) < 0) {
		perror("input-stdin: tcgetattr failed");
	}

	newt = priv->old_termios;

	newt.c_lflag &= ~(ICANON | ECHO | ISIG);
	newt.c_iflag &= ~(ISTRIP | IGNCR | ICRNL | INLCR | IXOFF | IXON);
	newt.c_cc[VMIN] = 0;
	newt.c_cc[VTIME] = 0;

	if (tcsetattr(STDIN_FD, TCSANOW, &newt) < 0) {
		priv->rawmode = 0;
		perror("input-stdin: tcsetattr failed");
	} else {
		ggRegisterCleanup((ggcleanup_func *) GII_stdin_close,
				  (void *) src);
	}
}
#endif

static struct gii_cmddata_devinfo devinfo = {
	"Standard Input",	/* long device name */
	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,
	emKeyPress | emKeyRelease,	/* kdb event */
	1,
	0,			/* all valuators */
	256,			/* buttons */
};


static int
GII_stdin_init(struct gii_source *src,
	       const char *target, const char *args, void *argptr)
{
	const char *str;
	struct stdin_priv *priv;
	gg_option options[NUM_OPTS];

	DPRINT_LIBS("GIIdl_stdin(%p, \"%s\", \"%s\", %p) called\n", src,
		    target, args ? args : "", argptr);

	memcpy(options, optlist, sizeof(options));

	/* handle args */
	str = getenv("GII_STDIN_OPTIONS");
	if (str != NULL) {
		str = ggParseOptions(str, options, NUM_OPTS);
		if (str == NULL) {
			fprintf(stderr, "input-stdin: error in "
				"$GII_STDIN_OPTIONS.\n");
			return GGI_EARGINVAL;
		}
	}

	if (args) {
		args = ggParseOptions(args, options, NUM_OPTS);
		if (args == NULL) {
			fprintf(stderr, "input-stdin: error in "
				"arguments.\n");
			return GGI_EARGINVAL;
		}
	}

	/* allocate private stuff */
	priv = calloc(1, sizeof(struct stdin_priv));
	if (priv == NULL) {
		return GGI_ENOMEM;
	}

	priv->origin = giiAddDevice(src, &devinfo, NULL);
	if (priv->origin == 0) {
		free(priv);
		return GGI_ENOMEM;
	}

	src->priv = priv;

	if (tolower((uint8_t) options[OPT_ANSIKEY].result[0]) != 'n') {
		priv->ansikey = 1;
	} else {
		priv->ansikey = 0;
	}

	priv->rawmode = 0;
#ifdef USE_TERMIOS
	if (tolower((uint8_t) options[OPT_NORAW].result[0]) == 'n') {
		/* turn on `raw' mode (i.e. non-canonical mode) */
		priv->rawmode = 1;
		GII_stdin_setraw(src);
	}
#endif

	if (giiSetNerveReadFD(src, &priv->nerve, STDIN_FD) < 0) {
		GII_stdin_close(src);
		return GGI_ENODEVICE;
	}

	src->ops.poll = GII_stdin_poll;
	src->ops.close = GII_stdin_close;

	DPRINT_MISC("input-stdin fully up\n");

	return 0;
}


struct gii_module_source GII_stdin = {
	GG_MODULE_INIT("input-stdin", 0, 1, GII_MODULE_SOURCE),
	GII_stdin_init
};

static struct gii_module_source *_GIIdl_stdin[] = {
	&GII_stdin,
	NULL
};


EXPORTFUNC int GIIdl_stdin(int item, void **itemptr);
int GIIdl_stdin(int item, void **itemptr)
{
	struct gii_module_source ***modulesptr;
	switch (item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***) itemptr;
		*modulesptr = _GIIdl_stdin;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
