/* $Id: input.c,v 1.22 2009/05/29 13:26:41 pekberg Exp $
******************************************************************************

   Input-file: Read events from a file saved by filter-save

   Copyright (C) 1999 Marcus Sundberg	[marcus@ggi-project.org]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"

#include <ggi/gg.h>
#include <ggi/gii-module.h>
#include <ggi/internal/gg_replace.h>
#include <ggi/internal/gii_debug.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


enum outtype { STDIN, FIL, PIPE };

struct file_priv {
	uint32_t nerve;
	uint32_t origin;
	enum outtype type;
	FILE *fil;
	struct timeval start_here;
	struct timeval start_file;
	gii_event event;
	uint8_t *datastart;
};

#define FILE_PRIV(inp)  ((struct file_priv *) inp->priv)

static inline int read_event(struct file_priv *priv)
{
	if (fread(&priv->event, 1, 1, priv->fil) != 1)
		return 0;
	DPRINT_EVENTS("input-file: got event of size: %d\n",
		      priv->event.size);
	if (fread(priv->datastart, (priv->event.size - 1U), 1, priv->fil)
	    != 1) {
		return 0;
	}

	return 1;
}

static inline int is_event_ready(struct gii_source *src)
{
	struct file_priv *priv = src->priv;
	struct timeval tv;
	long milli_here, milli_file;

	ggCurTime(&tv);

	milli_here = (tv.tv_sec - priv->start_here.tv_sec) * 1000 +
	    (tv.tv_usec - priv->start_here.tv_usec) / 1000;

	milli_file = (priv->event.any.sec -
		      priv->start_file.tv_sec) * 1000 +
	    (priv->event.any.usec -
	     priv->start_file.tv_usec) / 1000;

	if (milli_here >= milli_file) {
		priv->event.any.sec = tv.tv_sec;
		priv->event.any.usec = tv.tv_usec;
		return 1;
	}

	return 0;
}

static int GII_file_poll(struct gii_source *src)
{
	struct file_priv *priv = src->priv;

	DPRINT_EVENTS("GII_file_poll(%p) called\n", src);

	while (is_event_ready(src)) {
		giiPostEvent(src, &priv->event);
		if (!read_event(priv))
			giiCutNerve(src, &priv->nerve);
	}

	return GGI_OK;
}

static void GII_file_close(struct gii_source *src)
{
	struct file_priv *priv = src->priv;

	DPRINT_LIBS("GII_file_close(%p) called\n", src);

	fflush(priv->fil);

	switch (priv->type) {
	case FIL:
		fclose(priv->fil);
		break;
	case PIPE:
		pclose(priv->fil);
		break;
	default:
		break;
	}
	free(priv);

	DPRINT_LIBS("GII_file_close done\n");
}


static struct gii_cmddata_devinfo devinfo = {
	"File",			/* device name */

	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,

	emAll,			/* all event types */

	1,

	~(0U),			/* all valuators */
	~(0U),			/* all buttons */

};

static int
GII_file_init(struct gii_source *src,
	      const char *target, const char *args, void *argptr)
{
	struct timeval tv;
	struct file_priv *priv;

	DPRINT_LIBS("GIIdl_file(%p, \"%s\", \"%s\", %p) called\n", src,
		    target, args ? args : "", argptr);

	priv = malloc(sizeof(struct file_priv));
	if (priv == NULL) {
		return GGI_ENOMEM;
	}

	priv->origin = giiAddDevice(src, &devinfo, NULL);
	if (priv->origin == 0) {
		free(priv);
		return GGI_ENOMEM;
	}

	if (args == NULL || *args == '\0') {
		priv->type = STDIN;
		priv->fil = stdin;
	} else {
		if (*args == '|') {
			DPRINT_LIBS("input-file: pipe\n");
			fflush(stdin);
			priv->fil = popen(args + 1, "rb");
			priv->type = PIPE;
		} else {
			DPRINT_LIBS("input-file: file\n");
			priv->fil = fopen(args, "rb");
			priv->type = FIL;
		}
		if (priv->fil == NULL) {
			free(priv);

			return GGI_ENODEVICE;
		}
	}

	priv->datastart = ((uint8_t *) (&priv->event)) + 1;

	src->priv = priv;

	DPRINT_EVENTS("input-file: reading first event\n");

	if (!read_event(priv)) {
		GII_file_close(src);
		return GGI_ENODEVICE;
	}

	ggCurTime(&tv);

	priv->start_here = tv;
	priv->start_file.tv_sec = priv->event.any.sec;
	priv->start_file.tv_usec = priv->event.any.usec;

	DPRINT_EVENTS("input-file: start_here=(%d,%d) start_file=(%d,%d)",
		      priv->start_here.tv_sec, priv->start_here.tv_usec,
		      priv->start_file.tv_sec, priv->start_file.tv_usec);

	src->ops.close = GII_file_close;
	src->ops.poll = GII_file_poll;

	if (giiSetNerveTick(src, &priv->nerve, 0) < 0) {
		DPRINT_EVENTS("could not register my nerve\n");
		GII_file_close(src);
		return GGI_ENODEVICE;
	}

	DPRINT_LIBS("input-file fully up\n");

	return 0;
}


struct gii_module_source GII_file = {
	GG_MODULE_INIT("input-file", 0, 1, GII_MODULE_SOURCE),
	GII_file_init
};

static struct gii_module_source *_GIIdl_file[] = {
	&GII_file,
	NULL
};


EXPORTFUNC int GIIdl_file(int item, void **itemptr);
int GIIdl_file(int item, void **itemptr)
{
	struct gii_module_source ***modulesptr;
	switch (item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***) itemptr;
		*modulesptr = _GIIdl_file;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
