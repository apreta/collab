/* $Id: input.c,v 1.18 2008/12/12 03:50:04 pekberg Exp $
******************************************************************************

   Input-memory: Input event from buffer in main memory

   Copyright (C) 1995 Andreas Beck      [becka@ggi-project.org]
   Copyright (C) 1997 Jason McMullan    [jmcc@ggi-project.org]
   Copyright (C) 2000 Marcus Sundberg   [marcus@ggi-project.org]
   Copyright (C) 2007 Christoph Egger

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"

#include <ggi/gg.h>
#include <ggi/gii-module.h>
#include <ggi/internal/gg_replace.h>
#include <ggi/internal/gii_debug.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>


static const gg_option optlist[] = {
	{"input", ""},
	{"pointer", ""},
	{"size", "8192"},
};

#define OPT_INPUT	0
#define OPT_POINTER	1
#define OPT_SIZE	2

#define NUM_OPTS	(sizeof(optlist)/sizeof(gg_option))


#ifdef HAVE_SYS_SHM_H
#include <sys/shm.h>
#endif				/* HAVE_SYS_SHM_H */



enum memtype {
	MT_NOTHING,
	MT_EXTERN,		/* only a pointer: use this as input buffer */
	MT_SHMID,		/* "shmid:%i     : map shared memory at shmid */
	MT_SHMKEYFILE,		/* "keyfile:%c%s": create and map shared mem
				   corresponding to keyfile see ftok */
};

struct inpbuffer {
	unsigned int writeoffset;	/* We should lock access to that one... */
	unsigned char buffer[1];	/* This index will be used "overflowing" */
};

struct memory_priv {
	uint32_t nerve;
	uint32_t origin;

	size_t bufsize;
	enum memtype memtype;
	void *memptr;
	unsigned int inputoffset;
	struct inpbuffer *inputbuffer;

#ifdef _GG_HAVE_SHM
	int shmid;
#endif
};

#define MEMORY_PRIV(src)  ((struct memory_priv *) src->priv)

#define MEMINPMAGIC	'M'
#define INPBUFSIZE	8192
#define MINIMUM_BUFSIZE	300	/* Actually this is 262, but for safety... */

static int GII_memory_poll(struct gii_source *src)
{
	struct memory_priv *priv = MEMORY_PRIV(src);
	gii_event ev;

	DPRINT_EVENTS("GII_memory_poll(%p) called\n", src);

	DPRINT_EVENTS("buffer %p inputoffset %x, writeoffset %x\n",
		      priv->inputbuffer->buffer, priv->inputoffset,
		      priv->inputbuffer->writeoffset);
	while (priv->inputoffset != priv->inputbuffer->writeoffset) {
		if (priv->inputbuffer->buffer[priv->inputoffset++] !=
		    MEMINPMAGIC)
		{
			DPRINT_EVENTS("poll: OUT OF SYNC in shm input !\n");
			priv->inputoffset = 0;
			return GII_OP_POLLAGAIN;
		}

		DPRINT_EVENTS("poll: copy event from %p size 0x%x\n",
			      &(priv->inputbuffer->buffer[priv->inputoffset]),
			      (size_t)(priv->inputbuffer->buffer[priv->inputoffset]));
		memcpy(&ev, &(priv->inputbuffer->buffer[priv->inputoffset]),
		       (size_t)(priv->inputbuffer->buffer[priv->inputoffset]));

		giiPostEvent(src, &ev);
		priv->inputoffset += ev.any.size;

		if (priv->inputoffset >= (priv->bufsize - sizeof(gii_event)
			  - sizeof(priv->inputbuffer->writeoffset) - 10))
		{
			DPRINT_EVENTS("poll: reset inputoffset\n");
			priv->inputoffset = 0;
		}

		DPRINT_EVENTS("buffer %p inputoffset %x, writeoffset %x\n",
			      priv->inputbuffer->buffer, priv->inputoffset,
			      priv->inputbuffer->writeoffset);
	}

	DPRINT_EVENTS("poll: done\n");
	return GII_OP_DONE;
}

static int GII_sendevent(struct gii_source *src, gii_event * ev)
{
	struct memory_priv *priv = MEMORY_PRIV(src);
	size_t size = ev->any.size;

	DPRINT_EVENTS("GII_sendevent(%p,%p) called\n", src, ev);

	priv->inputbuffer->buffer[priv->inputbuffer->writeoffset++] =
	    MEMINPMAGIC;
	DPRINT_EVENTS("sendevent: priv->buffer %p, writeoffset 0x%x\n",
		      priv->inputbuffer->buffer,
		      priv->inputbuffer->writeoffset);

	DPRINT_EVENTS("sendevent: copy event to %p size 0x%lx\n",
		      &(priv->inputbuffer->buffer[priv->inputbuffer->writeoffset]), size);
	memcpy(&(priv->inputbuffer->buffer[priv->inputbuffer->writeoffset]),
	       ev, size);
	priv->inputbuffer->writeoffset += size;

	if (priv->inputbuffer->writeoffset >= (priv->bufsize
	       - sizeof(gii_event) - sizeof(priv->inputbuffer->writeoffset) - 10))
	{
		DPRINT_EVENTS("reset priv->writeoffset\n");
		priv->inputbuffer->writeoffset = 0;
	}
	/* "break"-symbol */
	priv->inputbuffer->buffer[priv->inputbuffer->writeoffset] =
	    MEMINPMAGIC - 1;

	return 0;
}

static void GII_memory_close(struct gii_source *src)
{
	struct memory_priv *priv = MEMORY_PRIV(src);

	DPRINT("GII_memory_close(%p) called\n", src);

	switch (priv->memtype) {
	case MT_EXTERN:
		break;
#ifdef _GG_HAVE_SHM
	case MT_SHMID:
		shmdt(priv->memptr);
		break;
	case MT_SHMKEYFILE:
		shmdt(priv->memptr);
		break;
#endif
	default:
		break;
	}

	free(src->priv);

	return;
}


static struct gii_cmddata_devinfo devinfo = {
	"memory",		/* device name */

	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,

	emAll,			/* all event types */
	1,
	~(0U),			/* all valuators */
	~(0U),			/* all buttons */
};


static int
GII_memory_init(struct gii_source *src,
		const char *target, const char *args, void *argptr)
{
	struct memory_priv *priv;
	gg_option options[NUM_OPTS];
	int err = GGI_OK;

	DPRINT_LIBS("GIIdl_memory(%p, \"%s\", \"%s\", %p) called\n", src,
		    target, args ? args : "", argptr);

	/* handle args */
	memcpy(options, optlist, sizeof(options));

	if (args && *args) {
		args = ggParseOptions(args, options, NUM_OPTS);
		if (args == NULL) {
			fprintf(stderr, "input-memory: error in "
				"arguments.\n");
			return GGI_EARGINVAL;
		}
	} else {
		fprintf(stderr, "input-memory: no arguments supplied.\n");
		return GGI_EARGREQ;
	}

	priv = calloc(1, sizeof(struct memory_priv));
	if (priv == NULL) {
		return GGI_ENOMEM;
	}

	priv->memtype = MT_NOTHING;

	priv->origin = giiAddDevice(src, &devinfo, NULL);
	if (priv->origin == 0) {
		free(priv);
		return GGI_ENOMEM;
	}


	if (options[OPT_INPUT].result[0]) {
		DPRINT("parse -input args: \"%s\"\n", args);
#ifdef _GG_HAVE_SHM
		if (strncmp(args, "shmid:", 6) == 0) {
			sscanf(args + 6, "%i", &(priv->shmid));
			DPRINT("input-memory has shmid-arg: %d\n",
			       priv->shmid);
			priv->memptr = shmat(priv->shmid, NULL, 0);
			DPRINT("shmat at %p.\n", priv->memptr);
			if (priv->memptr != (void *) -1) {
				priv->memtype = MT_SHMID;
			}
		} else if (strncmp(args, "keyfile:", 8) == 0) {
			unsigned int size;
			char id;
			char filename[1024];

			sscanf(args + 8, "%u:%c:%s", &size, &id, filename);
			DPRINT("has keyfile-arg: %d:%c:%s.\n",
			       size, id, filename);

			priv->shmid = shmget(ftok(filename, id), size,
					     IPC_CREAT | 0666);
			DPRINT("input-memory has shmid: %d.\n",
			       priv->shmid);

			priv->memptr = shmat(priv->shmid, NULL, 0);
			DPRINT("shmat at %p.\n", priv->memptr);
			if (priv->memptr != (void *) -1) {
				priv->memtype = MT_SHMID;
			}
		}
#endif				/* _GG_HAVE_SHM */
	}

	if (options[OPT_POINTER].result[0]) {
		priv->memptr = argptr;
		DPRINT("input-memory has extern memory %p\n",
		       priv->memptr);
		if (priv->memptr)
			priv->memtype = MT_EXTERN;
	}

	priv->bufsize = strtoul(options[OPT_SIZE].result, NULL, 0);
	if (priv->bufsize == 0 || priv->bufsize == ULONG_MAX) {
		DPRINT("invalid size argument\n");
		err = GGI_EARGINVAL;
		goto out;
	}
	if (priv->bufsize < MINIMUM_BUFSIZE) {
		DPRINT("size too small. see input-memory(7) manpage "
		       "for more information.\n");
		err = GGI_EARGINVAL;
		goto out;
	}

	if (priv->memptr == NULL) {
		DPRINT("no input memory available\n");
		err = GGI_ENOMEM;
		goto out;
	}

	priv->inputbuffer = priv->memptr;
	src->priv = priv;

	src->ops.poll = GII_memory_poll;
	src->ops.event = GII_sendevent;
	src->ops.close = GII_memory_close;

	if (giiSetNerveTick(src, &priv->nerve, 0) < 0) {
		DPRINT_EVENTS("could not register my nerve\n");
		err = GGI_ENODEVICE;
		goto out;
	}

	DPRINT_LIBS("input-memory fully up\n");

	return GGI_OK;

      out:
	GII_memory_close(src);
	return err;
}


struct gii_module_source GII_memory = {
	GG_MODULE_INIT("input-memory", 0, 1, GII_MODULE_SOURCE),
	GII_memory_init
};

static struct gii_module_source *_GIIdl_memory[] = {
	&GII_memory,
	NULL
};


EXPORTFUNC int GIIdl_memory(int item, void **itemptr);
int GIIdl_memory(int item, void **itemptr)
{
	struct gii_module_source ***modulesptr;
	switch (item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***) itemptr;
		*modulesptr = _GIIdl_memory;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
