Input from buffer in main memory
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. manpage:: 7 input-memory

Synopsis
--------

::

  input-memory: [-size=<bufsize>]
		shmid:<sid> | keyfile:<size>:<id>:<fname> | pointer


Description
-----------

Uses an input buffer of `INPBUFSIZE` (default is 8192 bytes)
at the start of the specified memory area.

input-memory is used by libggi's :man:`display-memory(7)` and
:man:`display-ipc(7)`. It is also usable as standalone input-target.

Options
-------

``shmid:<sid>``
    use existing shared memory ID :p:`sid`

    On win32, :p:`sid` is the HANDLE returned by a call to
    ``CreateFileMapping`` in decimal form.

``keyfile:<size>:<id>:<fname>``
    create a new shm segment with id ``ftok(fname,id)`` of size
    :p:`size` (prefered method !). See :man:`ftok(3)`.

    On win32, the newly created shared memory mapping has the object
    name: ``ggi-input-memory-shm:<fname>:<ascid>``, where all
    backslashes have been converted to forward slashes in :p:`fname`
    and :p:`ascid` is the ascii value of :p:`id` in decimal form.
    If this object does already exist (and is a file mapping) it will
    be used, so two apps can share memory by using the same
    ``keyfile`` arguments on win32.

``pointer``
    use the memory pointed to by :p:`argptr` (only available to
    applications calling `giiOpen`).

``size``
    specifies the size of the (shared) memory. If this option
    is not given, then The default size of 8192 bytes are used.
    Minimum size are 300 bytes to be functional.

.. important::

    Be sure the specified memory area is larger or equal the
    given size as no checks can or will be made that a certain
    event fits into it.

