/* $Id: lk201.h,v 1.2 2004/09/18 09:41:22 cegger Exp $
******************************************************************************

   lk201: input

   Copyright (C) 1998 Andrew Apted      [andrew@ggi-project.org]
   Copyright (C) 1999 Marcus Sundberg   [marcus@ggi-project.org]
   Copyright (C) 1999 John Weismiller   [johnweis@home.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/


#ifndef __LK201_H__
#define __LK201_H__

#define LK_POWER_UP	(0x01)
#define LK_CMD_POWER_UP (0xfd)
#define LK_ALLUP	(0xb3)


#endif
