/* $Id: input.c,v 1.29 2009/05/24 17:47:47 antrik Exp $
******************************************************************************

   Input-KII: Input for KII
      
   Copyright (C) 2002 Paul Redmond

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/
#include <string.h>
#include <stdlib.h>

#include <ggi/gg.h>
#include <ggi/gii-module.h>
#include <ggi/gii-keyboard.h>
#include "kii.h"
#include <ggi/internal/gii_debug.h>

#define KIIGII_N 51
static struct { kii_u32_t ksym; uint32_t gsym; int len; } kiigii[KIIGII_N] =
	{
	  /*
	   *    Translation table KII symbols to GII symbols...
	   *
	   *    KGI symbol, corresp GGI label, # sequential entries
	   *    First matching KGI symbol exits the table, else try next row.
	   */
	  	{ K_FIRST_ASCII,	'\0',		0x80 },
		{ K_TYPE_LATIN,		'\0',		0x80 },
		{ K_F21,		GIIK_F21,	44 },
		{ K_F1,			GIIK_F1,        20 },
		{ K_P0,			GIIK_P0,	10 },
		{ K_DOWN,		GIIK_Down,	1 },
		{ K_LEFT,		GIIK_Left,	1 },
		{ K_RIGHT,		GIIK_Right,	1 },
		{ K_UP,			GIIK_Up,	1 },
		{ K_PPLUS,		GIIK_PPlus,	1 },
		{ K_PMINUS,		GIIK_PMinus,	1 },
		{ K_PSTAR,		GIIK_PStar,	1 },
		{ K_PSLASH,		GIIK_PSlash,	1 },
		{ K_PENTER,		GIIK_PEnter,	1 },
		{ K_PCOMMA,		GIIK_PSeparator,1 },
		{ K_PDOT,		GIIK_PDecimal,	1 },
		{ K_PPLUSMINUS,		GIIK_PPlusMinus,1 },
		{ K_PARENL,		GIIK_PParenLeft,2 },
		{ K_NORMAL_SHIFT,	GIIK_Shift,	1 },
		{ K_NORMAL_CTRL,	GIIK_Ctrl,	1 },
		{ K_NORMAL_ALT,		GIIK_Alt,	1 },
		{ K_NORMAL_ALTGR,	GIIK_AltGr,	1 },
		{ K_NORMAL_SHIFTL,	GIIK_ShiftL,	1 },
		{ K_NORMAL_SHIFTR,	GIIK_ShiftR,	1 },
		{ K_NORMAL_CTRLL,	GIIK_CtrlL,	1 },
		{ K_NORMAL_CTRLR,	GIIK_CtrlR,	1 },
		{ K_FIND,		GIIK_Find,	1 },
		{ K_INSERT,		GIIK_Insert,	1 },
		{ K_SELECT,		GIIK_Select,	1 },
		{ K_PGUP,		GIIK_PageUp,	2 },
		{ K_MACRO,		GIIK_Macro,	1 },
		{ K_HELP,		GIIK_Help,	3 },
		{ K_UNDO,		GIIK_Undo,	1 },
		{ K_ENTER,		GIIK_Enter,	1 },
		{ K_BREAK,		GIIK_Break,	1 },
		{ K_CAPS,		GIIK_CapsLock,	3 },
		{ K_BOOT,		GIIK_Boot,	1 },
		{ K_COMPOSE,		GIIK_Compose,	2 },
		{ K_SYSTEM_REQUEST,	GIIK_SysRq,	1 },
		{ K_DGRAVE,		GIIK_DeadGrave, 1 },
		{ K_DACUTE,		GIIK_DeadAcute, 1 },
		{ K_DCIRCM,		GIIK_DeadCircumflex, 1 },
		{ K_DTILDE,		GIIK_DeadTilde, 1 },
		{ K_DDIERE,		GIIK_DeadDiaeresis, 1 },
		{ K_DCEDIL,		GIIK_DeadCedilla, 1 },
		{ K_LOCKED_SHIFT,	GIIK_ShiftLock,	3 },
		{ K_LOCKED_ALTGR,	GIIK_AltGrLock, 1 },
		{ K_LOCKED_SHIFTL,	GIIK_ShiftLock, 1 },
		{ K_LOCKED_SHIFTR,	GIIK_ShiftLock, 1 },
		{ K_LOCKED_CTRLL,	GIIK_CtrlLock,	1 },
		{ K_LOCKED_CTRLR,	GIIK_CtrlLock,	1 }
	};

#define SYMLAB_N 2
static struct { uint32_t sym; uint32_t label; int len; } symlab[SYMLAB_N] = 
	{
	  /*
	   *    Translation table GII symbols to GII labels...
	   *
	   *    GII symbol, corresp GII label, # sequential entries
	   *    First matching GII symbol exits the table, else try next row.
	   */
		{ 'a',/* ucase labels */ GIIUC_A,	26 }, 
		{ 0,  /* CTRL chars */   GIIUC_At,	32 }
	};

static const gg_option optlist[] =
{
	{ "device", "/dev/event,/dev/kgi/event" },
};

typedef struct {
	kii_context_t *ctx;
	uint32_t origin;
	uint32_t nerve;
} kii_priv;

#define KII_PRIV(src) ((kii_priv *)src->priv)
#define KII_CTX(src)  (KII_PRIV(src)->ctx)

static struct gii_cmddata_devinfo devinfo =
{
	"Kernel Input Interface",
	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,
	emAll,				/* can_generate */
	1,
	0, /* GII_NUM_UNKNOWN */	/* num_axes */
	0, /* GII_NUM_UNKNOWN */	/* num_buttons */
};

static void GII_kii_handle_key(struct gii_source *src, gii_event *ge,
	const kii_event_t *ke)
{
	giiEventBlank(ge, sizeof(gii_key_event));
	ge->any.size = sizeof(gii_key_event);
	ge->any.origin = KII_PRIV(src)->origin;
	
	DPRINT_LIBS("sym: 0x%.8x, code: 0x%.8x, effect: 0x%.4x, "
		"normal: 0x%.4x, locked: 0x%.4x, sticky: 0x%.4x\n",
		ke->key.sym, ke->key.code, ke->key.effect,
		ke->key.normal, ke->key.locked, ke->key.sticky);
	
	/* FIXME: What about shift L/R ctrl L/R */
	if (ke->key.effect & KII_MM_SHIFT)
		ge->key.modifiers |= GII_MOD_SHIFT;
	if (ke->key.effect & KII_MM_CTRL)
		ge->key.modifiers |= GII_MOD_CTRL;
	if (ke->key.effect & KII_MM_ALT)
		ge->key.modifiers |= GII_MOD_ALT;
	if (ke->key.effect & KII_MM_ALTGR)
		ge->key.modifiers |= GII_MOD_ALTGR;
	
	/* Event types are the same */
	ge->any.type = ke->any.type;

	/* Button number is the scancode */
	ge->key.button = ke->key.code;

        if (ke->key.code < 0x100) {
                /* Keyboard key. */
		int i;

		for (i = 0; i < KIIGII_N; i++) {
			if (ke->key.sym < kiigii[i].ksym) continue;
			if (ke->key.sym >= kiigii[i].ksym + kiigii[i].len) 
			  continue;
			ge->key.sym = 
			  kiigii[i].gsym + (ke->key.sym - kiigii[i].ksym);
			break;
		}

		if (i >= KIIGII_N) goto voidkey;

		ge->key.label = ge->key.sym;
		for (i = 0; i < SYMLAB_N; i++) {
			if (ge->key.sym < symlab[i].sym) continue;
			if (ge->key.sym >= symlab[i].sym + symlab[i].len)
			  continue;
			ge->key.label =
			  symlab[i].label + (ge->key.sym - symlab[i].sym);
			break;
		}
        } else {
                /* Other button. */
	voidkey:
                ge->key.label = ge->key.sym = GIIK_VOID;
        }
	
	giiPostEvent(src, ge);
}

static void GII_kii_handle_ptr(struct gii_source *src, gii_event *ge,
	const kii_event_t *ke)
{

	if (ke->any.type == KII_EV_PTR_STATE) {
	
		return;
	}

	if (ke->any.type < KII_EV_PTR_BUTTON_PRESS) {

		/* pointer move event */
    		giiEventBlank(ge, sizeof(gii_pmove_event));
		
		switch (ke->any.type) {
		
		case KII_EV_PTR_RELATIVE:
			ge->any.type = evPtrRelative;
			break;
			
		case KII_EV_PTR_ABSOLUTE:
			ge->any.type = evPtrAbsolute;
			break;
		
		default:
			return;
		}
		ge->any.origin = KII_PRIV(src)->origin;
		ge->any.size = sizeof(gii_pmove_event);
		ge->pmove.x = ke->pmove.x;
		ge->pmove.y = ke->pmove.y;
		
		giiPostEvent(src, ge);
	}
	else {
		int i;
		kii_u_t mask = 1;

    		giiEventBlank(ge, sizeof(gii_pbutton_event));
		switch (ke->any.type) {
		
		case KII_EV_PTR_BUTTON_PRESS:
			ge->any.type = evPtrButtonPress; break;
			
		case KII_EV_PTR_BUTTON_RELEASE:
			ge->any.type = evPtrButtonRelease; break;
		
		default:
			return;
		}
		ge->any.origin = src->origin;
		ge->any.size = sizeof(gii_pbutton_event);
		for (i = 1; i <= 32; i++) {
		
			if (ke->pbutton.button & mask) {
			
				ge->pbutton.button = i;
				giiPostEvent(src, ge);
			}
			mask <<= 1;
		}
	}
}

static int GII_kii_poll(struct gii_source *src)
{
	const kii_event_t *ke;
	gii_event ge;

	/* FIXME: Maybe we should handle eof condition? -- ortalo */

	/* We are only polled if the registered fd is ready -- eric */

	/* Now we are sure there is data. kiiEventAvailable() may block */
	if (!kiiEventAvailable(KII_CTX(src))) {
	
		return GII_OP_DONE;
	}
		
	for (ke=kiiNextEvent(KII_CTX(src));ke;ke=kiiNextEvent(KII_CTX(src))) {

		if ((1 << ke->any.type) & KII_EM_KEYBOARD) {
		
			GII_kii_handle_key(src, &ge, ke);
		}
		else if ((1 << ke->any.type) & KII_EM_POINTER) {
		
			GII_kii_handle_ptr(src, &ge, ke);
		}
	}

	return GII_OP_DONE;
}

static void GII_kii_close(struct gii_source *src) 
{
	kii_priv *priv = KII_PRIV(src);
        
	free(KII_CTX(src));
	free(priv);

	DPRINT_MISC("kii: exit OK.\n");
}


static int
GII_kii_init(struct gii_source *src,
	     const char * target,
	     const char *args,
	     void *argptr)
{
	kii_priv *priv;
	gg_option options[KII_NUM_OPTS];

	DPRINT_LIBS("GIIdl_kii(%p, \"%s\", \"%s\", %p) called\n", src,
		    target,
		    args ? args : "",
		    argptr);
	
	if ((priv = src->priv = malloc(sizeof(kii_priv))) == NULL) {

		return GGI_ENOMEM;
	}
	if ((KII_CTX(src) = calloc(1, sizeof(kii_context_t))) == NULL) {
		free(priv);
		return GGI_ENOMEM;
	}
	
	memcpy(options, optlist, sizeof(options));
	if (args) {
		args = ggParseOptions(args, options, KII_NUM_OPTS);
		if (args == NULL) {
			DPRINT_LIBS("Error in arguments\n");
			free(KII_CTX(src));
			free(priv);
			return GGI_EARGINVAL;
		}
	}

	if((priv->origin = giiAddDevice(src, &devinfo, NULL)) == 0) {
		free(KII_CTX(src));
		free(priv);
		return GGI_ENOMEM;
	}

	if (KII_EOK != kiiInit(KII_CTX(src), options)) {
	
		free(KII_CTX(src));
		free(priv);
		return GGI_ENODEVICE;
	}

	if (KII_EOK != kiiMapDevice(KII_CTX(src))) {
	
		/* FIXME: kiiInit now leaks memory */
		free(KII_CTX(src));
		free(priv);	
		return GGI_ENODEVICE;
	}

	src->ops.close = GII_kii_close;
	src->ops.poll = GII_kii_poll;
	
	giiSetNerveReadFD(src, &priv->nerve, kiiEventDeviceFD(KII_CTX(src)));
	
	DPRINT_MISC("kii fully up\n");

	return 0;
}


struct gii_module_source GII_kii = {
	GG_MODULE_INIT("input-kii", 0, 1, GII_MODULE_SOURCE),
	GII_kii_init
};

static struct gii_module_source *_GIIdl_kii[] = {
	&GII_kii,
	NULL
};


EXPORTFUNC
int GIIdl_kii(int item, void **itemptr);
int GIIdl_kii(int item, void **itemptr) {
	struct gii_module_source ***modulesptr;
	switch(item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***)itemptr;
		*modulesptr = _GIIdl_kii;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
