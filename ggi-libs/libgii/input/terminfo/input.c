/* $Id: input.c,v 1.9 2009/04/13 09:05:56 cegger Exp $
******************************************************************************

   Terminfo target

   Copyright (C) 1997 Jason McMullan    [jmcc@ggi-project.org]
   Copyright (C) 1998 MenTaLguY         [mentalg@geocities.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#ifdef __GNUC__
# undef alloca
# define alloca __builtin_alloca
#else
# if HAVE_ALLOCA_H
#  include <alloca.h>
# else
#  ifdef _AIX
#pragma alloca
#  else
#   ifndef alloca		/* predefined by HP cc +Olibcalls */
char *alloca(void);
#   endif
#  endif
# endif
#endif

#ifdef HAVE_NCURSES_H
# include <ncurses.h>
#elif defined(HAVE_NCURSES_NCURSES_H)
# include <ncurses/ncurses.h>
#else
# include <curses.h>
#endif

#include <ggi/input/terminfo.h>
#include <ggi/gg.h>
#include <ggi/gii-module.h>
#include <ggi/gii-keyboard.h>
#include <ggi/internal/gii_debug.h>

struct terminfo_priv {
	func_terminfo_select_screen *term_select_screen;
	func_terminfo_release_screen *term_release_screen;

	uint32_t origin;
	uint32_t nerve;

	SCREEN *scr;
	int mode_dpp_x;
	int mode_dpp_y;
};

#define TERMINFO_PRIV(src)	(struct terminfo_priv *)((src)->priv)


static int translate_key(int key, uint32_t * modifiers)
{
	DPRINT("terminfo: TRANSLATEKEY %04x\n", key);

	*modifiers = 0;

	if ((0x00 <= key) && (key <= 0x7f)) {
		return key;
	}

	if ((KEY_F0 + 1 <= key) && (key <= KEY_F0 + 20)) {
		return GII_KEY(GII_KT_FN, key - 1);
	}

	if ((KEY_F0 + 21 <= key) && (key <= KEY_F0 + 63)) {
		return GII_KEY(GII_KT_FN, key + 9);
	}

	switch (key) {
	case '\015':
		return GIIK_Enter;

	case KEY_BREAK:
		return GIIK_Break;
	case KEY_DOWN:
		return GIIK_Down;
	case KEY_UP:
		return GIIK_Up;
	case KEY_LEFT:
		return GIIK_Left;
	case KEY_RIGHT:
		return GIIK_Right;
	case KEY_BACKSPACE:
		return 8;

	case KEY_DC:
		return GIIK_Clear;
	case KEY_IC:
		return GIIK_Insert;
	case KEY_EIC:
		return GIIK_Insert;	/* ??? */
	case KEY_SF:
		return GIIK_ScrollForw;
	case KEY_SR:
		return GIIK_ScrollBack;
	case KEY_NPAGE:
		return GIIK_PageDown;
	case KEY_PPAGE:
		return GIIK_PageUp;
	case KEY_ENTER:
		return GIIK_Enter;
	case KEY_SRESET:
		return GIIK_SAK;	/* ??? */
	case KEY_RESET:
		return GIIK_Boot;	/* ??? */
	case KEY_A1:
		return GIIK_Home;
	case KEY_A3:
		return GIIK_PageUp;
	case KEY_C1:
		return GIIK_End;
	case KEY_C3:
		return GIIK_PageDown;

	case KEY_END:
		return GIIK_End;
	case KEY_FIND:
		return GIIK_Find;
	case KEY_HELP:
		return GIIK_Help;
	case KEY_UNDO:
		return GIIK_Undo;
	case KEY_NEXT:
		return GIIK_Next;
	case KEY_PREVIOUS:
		return GIIK_Prior;
	case KEY_SELECT:
		return GIIK_Select;
	case KEY_SUSPEND:
		return GIIK_Pause;	/* ??? */

	/*** NOT DONE :

	case KEY_HOME           Home key (upward+left arrow)
	case KEY_DL             Delete line
	case KEY_IL             Insert line
	case KEY_CLEAR:		Clear
	case KEY_EOS:		Clear to end of screen
	case KEY_EOL:		Clear to end of line
	case KEY_STAB:		Set tab
	case KEY_CTAB:		Clear tab
	case KEY_CATAB:		Clear all tabs
	case KEY_PRINT:		Print or copy
	case KEY_LL:		Home down or bottom (lower left).
	case KEY_B2:		Centre of keypad

	case KEY_BTAB:		Back tab key
	case KEY_BEG:		Beg(inning) key
	case KEY_CANCEL		Cancel key
	case KEY_CLOSE:		Close key
	case KEY_COMMAND:	Cmd (command) key
	case KEY_COPY:		Copy key
	case KEY_CREATE:	Create key
	case KEY_EXIT:		Exit key
	case KEY_MARK:		Mark key
	case KEY_MESSAGE:	Message key
	case KEY_MOVE:		Move key
	case KEY_OPEN:		Open key
	case KEY_OPTIONS:	Options key
	case KEY_REDO:		Redo key
	case KEY_REFERENCE:	Ref(erence) key
	case KEY_REFRESH:	Refresh key
	case KEY_REPLACE:	Replace key
	case KEY_RESTART:	Restart key
	case KEY_RESUME:	Resume key
	case KEY_SAVE:		Save key

	case KEY_SBEG:		Shifted beginning key
	case KEY_SCANCEL:	Shifted cancel key
	case KEY_SCOMMAND:	Shifted command key
	case KEY_SCOPY:		Shifted copy key
	case KEY_SCREATE:	Shifted create key
	case KEY_SDC:		Shifted delete char key
	case KEY_SDL:		Shifted delete line key
	case KEY_SEND:		Shifted end key
	case KEY_SEOL:		Shifted clear line key
	case KEY_SEXIT:		Shifted exit key
	case KEY_SFIND:		Shifted find key
	case KEY_SHELP:		Shifted help key
	case KEY_SHOME:		Shifted home key
	case KEY_SIC:		Shifted input key
	case KEY_SLEFT:		Shifted left arrow key
	case KEY_SMESSAGE:	Shifted message key
	case KEY_SMOVE:		Shifted move key
	case KEY_SNEXT:		Shifted next key
	case KEY_SOPTIONS:	Shifted options key
	case KEY_SPREVIOUS:	Shifted prev key
	case KEY_SPRINT:	Shifted print key
	case KEY_SREDO:		Shifted redo key
	case KEY_SREPLACE:	Shifted replace key
	case KEY_SRIGHT:	Shifted right arrow
	case KEY_SRSUME:	Shifted resume key
	case KEY_SSAVE:		Shifted save key
	case KEY_SSUSPEND:	Shifted suspend key
	case KEY_SUNDO:		Shifted undo key

	***/
	}

	return GIIK_VOID;	/* unknown */
}

static int GII_terminfo_eventpoll(struct gii_source *src)
{
	struct terminfo_priv *priv;
	gii_event ev;
	gii_event_mask mask;
	int key;

	priv = TERMINFO_PRIV(src);

	mask = 0;

	priv->term_select_screen(priv->scr);

	key = wgetch(stdscr);
	switch (key) {
	case ERR:
		break;
#if defined(NCURSES_MOUSE_VERSION) && (NCURSES_MOUSE_VERSION == 1)
	case KEY_MOUSE:{
			MEVENT mev;

			getmouse(&mev);
			giiEventBlank(&ev, sizeof(gii_event));

			switch (mev.bstate) {
			case BUTTON1_PRESSED:
			case BUTTON2_PRESSED:
			case BUTTON3_PRESSED:
			case BUTTON4_PRESSED:{
					ev.any.type = evPtrButtonPress;
					ev.any.size =
					    sizeof(gii_pbutton_event);
					mask |= emPtrButtonPress;
					switch (mev.bstate) {
					case BUTTON1_PRESSED:
						ev.pbutton.button = 1;
						break;
					case BUTTON2_PRESSED:
						ev.pbutton.button = 3;
						break;
					case BUTTON3_PRESSED:
						ev.pbutton.button = 2;
						break;
					case BUTTON4_PRESSED:
						ev.pbutton.button = 4;
						break;
					}
				}
				break;
			case BUTTON1_RELEASED:
			case BUTTON2_RELEASED:
			case BUTTON3_RELEASED:
			case BUTTON4_RELEASED:{
					ev.any.type = evPtrButtonRelease;
					ev.any.size =
					    sizeof(gii_pbutton_event);
					mask |= emPtrButtonRelease;
					switch (mev.bstate) {
					case BUTTON1_PRESSED:
						ev.pbutton.button = 1;
						break;
					case BUTTON2_PRESSED:
						ev.pbutton.button = 3;
						break;
					case BUTTON3_PRESSED:
						ev.pbutton.button = 2;
						break;
					case BUTTON4_PRESSED:
						ev.pbutton.button = 4;
						break;
					}
				}
				break;
			default:
				ev.any.type = evPtrAbsolute;
				ev.any.size = sizeof(gii_pmove_event);
				ev.pmove.x = mev.x * priv->mode_dpp_x;
				ev.pmove.y = mev.y * priv->mode_dpp_y;
				mask |= emPtrAbsolute;
			}
			giiPostEvent(src, &ev);
		}
		break;
#endif				/* NCURSES_MOUSE_VERSION */
	case '\033':{
			int temp;
			timeout(1);
			temp = wgetch(stdscr);
			timeout(0);
			if (temp != ERR) {
				giiEventBlank(&ev, sizeof(gii_key_event));
				ev.any.size = sizeof(gii_key_event);
				ev.any.type = evKeyPress;
				ev.key.modifiers = GII_MOD_ALT;
				ev.key.sym =
				    translate_key(key, &ev.key.modifiers);
				ev.key.label = ev.key.sym;	/* FIXME !!! */
				ev.key.button = temp;
				giiPostEvent(src, &ev);

				giiEventBlank(&ev, sizeof(gii_key_event));
				ev.any.size = sizeof(gii_key_event);
				ev.any.type = evKeyRelease;
				ev.key.modifiers = GII_MOD_ALT;
				ev.key.sym =
				    translate_key(key, &ev.key.modifiers);
				ev.key.label = ev.key.sym;	/* FIXME !!! */
				ev.key.button = temp;
				giiPostEvent(src, &ev);

				mask |= emKeyPress | emKeyRelease;

				break;
			}	/* else, normal key, and default... */
		}
	default:{
			giiEventBlank(&ev, sizeof(gii_key_event));
			ev.any.size = sizeof(gii_key_event);
			ev.any.type = evKeyPress;
			ev.key.modifiers = 0;
			ev.key.sym = translate_key(key, &ev.key.modifiers);
			ev.key.label = ev.key.sym;	/* FIXME !!! */
			ev.key.button = key;
			giiPostEvent(src, &ev);

			giiEventBlank(&ev, sizeof(gii_key_event));
			ev.any.size = sizeof(gii_key_event);
			ev.any.type = evKeyRelease;
			ev.key.modifiers = 0;
			ev.key.sym = translate_key(key, &ev.key.modifiers);
			ev.key.label = ev.key.sym;	/* FIXME !!! */
			ev.key.button = key;

			mask |= emKeyPress | emKeyRelease;

			giiPostEvent(src, &ev);
		}
	}
	priv->term_release_screen();

	return mask;
}

static struct gii_cmddata_devinfo devinfo = {
	"Terminfo",		/* long name */
	GII_VENDOR_GGI_PROJECT,
	GII_PRODUCT_GGI_PROJECT,
#if defined(NCURSES_MOUSE_VERSION) && (NCURSES_MOUSE_VERSION == 1)
	emKey | emPtrButton | emPtrAbsolute,	/* can_generate */
	GII_NUM_UNKNOWN,	/* num_registers */
	2,			/* axes */
	4,			/* max 4 buttons */
#else
	emKey,			/* without mouse support */
	GII_NUM_UNKNOWN,	/* num_registers */
	0,
	0
#endif
};

static void GII_terminfo_close(struct gii_source *src)
{
	struct terminfo_priv *priv = TERMINFO_PRIV(src);

	free(priv);

	return;
}


static int GII_terminfo_sendevent(struct gii_source *src, gii_event * ev)
{
	struct terminfo_priv *priv = TERMINFO_PRIV(src);

	if ((ev->any.target != priv->origin)
	    && (ev->any.target != GII_EV_TARGET_ALL)) {
		return -1;	/* not intended for us */
	}

	if (ev->any.type != evCommand) {
		return -1;	/* not interested */
	}

	return GGI_EEVUNKNOWN;
}


static int
GII_terminfo_init(struct gii_source *src,
		  const char *target, const char *args, void *argptr)
{
	struct terminfo_priv *priv;
	gii_terminfo_arg *targ = argptr;

	DPRINT_LIBS("GIIdl_terminfo(%p, \"%s\", \"%s\", %p) called\n",
		    target, args ? args : "", argptr);

	if (!targ)
		return GGI_EARGREQ;

	/* allocate private stuff */
	priv = calloc(1, sizeof(struct terminfo_priv));
	if (priv == NULL)
		return GGI_ENOMEM;

	priv->origin = giiAddDevice(src, &devinfo, NULL);
	if (priv->origin == 0) {
		free(priv);
		return GGI_ENOMEM;
	}

	priv->term_select_screen = targ->select_screen;
	priv->term_release_screen = targ->release_screen;
	priv->scr = targ->scr;

	src->priv = priv;

	src->ops.event = GII_terminfo_sendevent;
	src->ops.poll = GII_terminfo_eventpoll;
	src->ops.close = GII_terminfo_close;

	return 0;
}


struct gii_module_source GII_terminfo = {
	GG_MODULE_INIT("input-terminfo", 0, 1, GII_MODULE_SOURCE),
	GII_terminfo_init
};

static struct gii_module_source *_GIIdl_terminfo[] = {
	&GII_terminfo,
	NULL
};


EXPORTFUNC int GIIdl_terminfo(int item, void **itemptr);
int GIIdl_terminfo(int item, void **itemptr)
{
	struct gii_module_source ***modulesptr;
	switch (item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***) itemptr;
		*modulesptr = _GIIdl_terminfo;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
