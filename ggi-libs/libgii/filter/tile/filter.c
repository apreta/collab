/* $Id: filter.c,v 1.1 2008/01/19 02:36:26 cegger Exp $
******************************************************************************

   tile filterlib

   Copyright (C) 2007 Peter Rosin		[peda@lysator.liu.se]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include <stdlib.h>
#include <string.h>

#include "config.h"
#include <ggi/gg.h>
#include <ggi/gii-module.h>
#include <ggi/internal/gii_debug.h>
#include <ggi/filter/tile.h>


struct rule_entry {
	GG_SLIST_ENTRY(rule_entry) others;
	uint32_t origin;
	int shift_x;
	int shift_y;
};

struct tile_priv {
	GG_SLIST_HEAD(rulez, rule_entry) rules;
};

#define FOREACH_RULE(priv, rule) \
	GG_SLIST_FOREACH(rule, &(priv->rules), others)
#define APPEND_RULE(priv, rule) \
	GG_SLIST_INSERT_HEAD(&(priv->rules), rule, others)
#define REMOVE_RULE(priv, rule) \
	GG_SLIST_REMOVE(&(priv->rules), rule, rule_entry, others)

#define TILE_PRIV(src)  ((struct tile_priv *) src->priv)


/* XXX copied from gii/gii-internal.h */
#define GII_SOURCEMASK	0xffffff00
#define GII_IS_SAME_SOURCE(a,b) ((a&GII_SOURCEMASK) == (b&GII_SOURCEMASK))

/* ---------------------------------------------------------------------- */

static int
add_rule(struct gii_source *src, struct gii_tile_rule *tile_rule)
{
	struct tile_priv *priv = TILE_PRIV(src);
	struct rule_entry *rule;

	DPRINT_MISC("adding rule origin %u shift (%d,%d)\n",
		    tile_rule->origin,
		    tile_rule->shift_x, tile_rule->shift_y);
	rule = malloc(sizeof(*rule));
	if (!rule)
		return GGI_ENOMEM;

	rule->origin = tile_rule->origin;
	rule->shift_x = tile_rule->shift_x;
	rule->shift_y = tile_rule->shift_y;

	APPEND_RULE(priv, rule);

	return GGI_OK;
}


static int tile_controller(void *arg, uint32_t ctl, void *data)
{
	struct gii_source *src = arg;

	switch (ctl) {
	case GII_TILE_ADD_RULE:
		add_rule(src, data);
		break;

	default:
		return GGI_ENOFUNC;
	}

	return GGI_OK;
}


static int tile_filter(struct gii_source *src, gii_event * event)
{
	struct tile_priv *priv = TILE_PRIV(src);
	struct rule_entry *rule;

	if (event->any.type != evPtrAbsolute)
		return GGI_OK;

	FOREACH_RULE(priv, rule) {
		if (GII_IS_SAME_SOURCE(rule->origin, event->any.origin))
			break;
	}

	if (!rule)
		return GGI_OK;

	event->pmove.x += rule->shift_x;
	event->pmove.y += rule->shift_y;

	return GGI_OK;
}


static void tile_close(struct gii_source *src)
{
	struct tile_priv *priv = TILE_PRIV(src);
	struct rule_entry *rule;

	while (!GG_SLIST_EMPTY(&priv->rules)) {
		rule = GG_SLIST_FIRST(&priv->rules);
		REMOVE_RULE(priv, rule);
		free(rule);
	}

	free(priv);

	DPRINT_MISC("closed\n");

	return;
}


static int
tile_init(struct gii_source *src,
	  const char *target, const char *args, void *argptr)
{
	struct tile_priv *priv;

	DPRINT_MISC("starting. (args=\"%s\",argptr=%p)\n", args, argptr);

	/* allocate private stuff */
	priv = malloc(sizeof(*priv));
	if (!priv)
		return GGI_ENOMEM;

	src->priv = priv;
	GG_SLIST_INIT(&priv->rules);

	src->ops.filter = tile_filter;
	src->ops.close = tile_close;

	ggSetController(src->instance.channel, tile_controller);

	DPRINT_MISC("fully up\n");

	return 0;
}


struct gii_module_source GII_filter_tile = {
	GG_MODULE_INIT("filter-tile", 0, 1, GII_MODULE_SOURCE),
	tile_init
};

static struct gii_module_source *_GIIdl_filter_tile[] = {
	&GII_filter_tile,
	NULL
};


EXPORTFUNC int GIIdl_filter_tile(int item, void **itemptr);
int GIIdl_filter_tile(int item, void **itemptr)
{
	struct gii_module_source ***modulesptr;
	switch (item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***) itemptr;
		*modulesptr = _GIIdl_filter_tile;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
