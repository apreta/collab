/* $Id: filter.c,v 1.18 2008/01/19 02:29:55 cegger Exp $
******************************************************************************

   Filter-save - save away an eventstream for later playback

   Copyright (C) 1998 Andreas Beck	[becka@ggi-project.org]
   Copyright (C) 1999 Marcus Sundberg	[marcus@ggi-project.org]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/errors.h>
#include <ggi/internal/gg_replace.h>
#include <ggi/gii-module.h>
#include <ggi/internal/gii_debug.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


enum outtype { STDOUT, FIL, PIPE };

struct save_priv {
	enum outtype type;
	FILE *output;
};


static int GII_save_handler(struct gii_source *src, gii_event * event)
{
	struct save_priv *priv = src->priv;
	DPRINT_LIBS("filter-save handler,priv=%p file=%p\n",
		    priv, priv->output);

	fwrite(event, event->any.size, 1, priv->output);

	return 0;
}


static void GII_save_close(struct gii_source *src)
{
	struct save_priv *priv = src->priv;

	DPRINT_LIBS("GII_save_close(%p) called\n", src);

	fflush(priv->output);

	switch (priv->type) {
	case FIL:
		fclose(priv->output);
		break;
	case PIPE:
		pclose(priv->output);
		break;
	default:
		break;
	}
	free(priv);

	DPRINT_LIBS("GII_save_close done\n");

	return;
}


static int
GII_filter_save_init(struct gii_source *src,
		     const char *target, const char *args, void *argptr)
{
	struct save_priv *priv;
	char *filename;

	DPRINT_LIBS("filter-save init(%p, \"%s\") called\n", src,
		    args ? args : "");

	priv = calloc(1, sizeof(struct save_priv));
	if (priv == NULL)
		return GGI_ENOMEM;

	priv->output = stdout;
	priv->type = STDOUT;

	if (args && *args != '\0') {
		/* XXX the way options are constructed append a ':' at
		 *  the end of the option string.  This module, like
		 *  many others, should use ggParseOptions to get its
		 *  parameters in a consistent way.
		 */
		filename = strdup(args);
		if (filename[strlen(filename) - 1] == ':')
			filename[strlen(filename) - 1] = 0;
		if (*filename == '|') {
			fflush(stdout);
			fflush(stderr);
			priv->output = popen(filename + 1, "wb");
			priv->type = PIPE;
		} else {
			priv->output = fopen(filename, "wb");
			priv->type = FIL;
		}
		free(filename);
		if (priv->output == NULL) {
			fprintf(stderr, "filter-save: unable to open %s\n",
				args);
			free(priv);
			return GGI_ENODEVICE;
		}
	}

	src->priv = priv;
	src->ops.filter = GII_save_handler;
	src->ops.close = GII_save_close;

	DPRINT_LIBS("filter-save fully up, priv=%p file=%p\n",
		    priv, priv->output);

	return 0;
}


struct gii_module_source GII_filter_save = {
	GG_MODULE_INIT("filter-save", 0, 1, GII_MODULE_SOURCE),
	GII_filter_save_init
};

static struct gii_module_source *_GIIdl_filter_save[] = {
	&GII_filter_save,
	NULL
};

EXPORTFUNC int GIIdl_filter_save(int item, void **itemptr);
int GIIdl_filter_save(int item, void **itemptr)
{
	struct gii_module_source ***modulesptr;
	switch (item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gii_module_source ***) itemptr;
		*modulesptr = _GIIdl_filter_save;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}
