/* $Id: gii.h,v 1.19 2007/05/05 07:34:27 cegger Exp $
******************************************************************************

   LibGII API header file

   Copyright (C) 1998      Andreas Beck		[becka@ggi-project.org]
   Copyright (C) 1999-2000 Marcus Sundberg	[marcus@ggi-project.org]
   Copyright (C) 2005      Eric Faurot		[eric.faurot@gmail.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#ifndef _GGI_GII_H
#define _GGI_GII_H

#include <ggi/gg-api.h>
#include <ggi/gii-defs.h>
#include <ggi/gii-events.h>

/*
 *  This header defines the main gii API, as seen by the regular
 *  library user.
 */

__BEGIN_DECLS

/*
************************************************************************
**  API definition
************************************************************************
*/


/*  Enter and leave the library.
 */
GIIAPIFUNC int giiInit(void);
GIIAPIFUNC int giiExit(void);

/*  
 */
GIIAPIFUNC int giiAttach(struct gg_stem*);
GIIAPIFUNC int giiDetach(struct gg_stem*);

/*  The LibGII interface.
*/
GIIAPIVAR struct gg_api *libgii;

/*  A stem on which the gii api is attached.
*/
typedef struct gg_stem *gii_input;

/*  Publishers and events.
*/

/*  Issued after a source join or leave an input
*/
#define GII_OBSERVE_SOURCE_OPENED 0
#define GII_OBSERVE_SOURCE_CLOSED 1

/*
************************************************************************
**  Input manipulation functions
************************************************************************
*/

/*  Add one or more source to an input. return the number of
 *  sources that were added, 0 for none, or an error code.
 */
GIIAPIFUNC int giiOpen(gii_input inp, const char *input, void *argptr);

/*  Remove the given source from the input.
 *  If target is GII_EV_TARGET_ALL, all sources are closed
 *  Returns 0 on success or an error code.
 */
GIIAPIFUNC int giiClose(gii_input inp, uint32_t source);

/*  Transfer a source from an input to another.
 *  `source` is the origin of the source to transfer.
 *  returns the number of transfered inputs.
 */
GIIAPIFUNC int giiTransfer(gii_input src,gii_input dst,uint32_t source);

/* Get/set event mask
 */
GIIAPIFUNC int            giiSetEventMask(gii_input inp,
					  gii_event_mask evm);
GIIAPIFUNC gii_event_mask giiGetEventMask(gii_input inp);

#define                   giiAddEventMask(inp,mask)  \
	   giiSetEventMask((inp), giiGetEventMask((inp)) | (mask))
#define                   giiRemoveEventMask(inp,mask)  \
	   giiSetEventMask((inp), giiGetEventMask((inp)) & ~(mask))

/*
************************************************************************
**  Event handling functions
************************************************************************
*/

GIIAPIFUNC gii_event_mask giiEventPoll(gii_input inp, gii_event_mask mask,
				       struct timeval *t);
GIIAPIFUNC int            giiEventRead(gii_input inp, gii_event *ev,
				       gii_event_mask mask);
GIIAPIFUNC int            giiEventsQueued(gii_input inp,
					  gii_event_mask mask);
GIIAPIFUNC int            giiEventSend(gii_input inp, gii_event *ev);

GIIAPIFUNC int giiKbhit(gii_input inp);

GIIAPIFUNC int giiGetc(gii_input inp);

/*
************************************************************************
**  Input device information
************************************************************************
*/

struct gii_source_iter {
	struct gg_iter iter;

	/* These must be set by the caller before calling
	 * giiIterSource
	 */
	gii_input stem;

	/* Placeholders for results */
	uint32_t origin;
	void *src;

	/* Internal */
	void *_state;
};

struct gii_device_iter {
	struct gg_iter iter;

	/* These must be set by the caller before calling
	 * giiIterDevice
	 */
	gii_input stem;
	uint32_t source_origin;
	void *src;		/* NULL means use source_origin */

	/* Placeholders for results */
	uint32_t origin;
	struct gii_cmddata_devinfo devinfo;

	/* Internal */
	void *_state;
};

#define giiIterFillSources(_iter, _stem)			\
		(_iter)->stem = (_stem);

#define giiIterFillDevices(_iter, _stem, _src, _src_origin)	\
		(_iter)->stem = (_stem);			\
		(_iter)->src = (_src);				\
		(_iter)->source_origin = (_src_origin);

GIIAPIFUNC int giiIterSources(struct gii_source_iter *iter);
GIIAPIFUNC int giiIterDevices(struct gii_device_iter *iter);


GIIAPIFUNC int giiQueryDeviceInfo(gii_input stem,
				  uint32_t origin,
				  struct gii_cmddata_devinfo *info);

GIIAPIFUNC int giiQueryValInfo(gii_input stem,
			       uint32_t origin,
			       uint32_t valnumber,
			       struct gii_cmddata_valinfo *info);



/*
************************************************************************
**  Miscellaneous functions
************************************************************************
*/

/*
************************************************************************
**  Publishers
************************************************************************
*/

__END_DECLS


#endif /*  _GGI_GII_H  */
