/* $Id: gii-events.h,v 1.8 2009/05/29 13:26:41 pekberg Exp $
******************************************************************************

   LibGII event definitions

   Copyright (C) 1995-1997	Steffen Seeger	[seeger@ggi-project.org]
   Copyright (C) 1998		Andrew Apted	[andrew@ggi-project.org]
   Copyright (C) 1998		Andreas Beck	[becka@ggi-project.org]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#ifndef _GGI_GII_EVENTS_H
#define	_GGI_GII_EVENTS_H

#include <ggi/system.h>

#if defined __GNUC__
#define GII_PACK __attribute__((packed))
#elif defined _MSC_VER
#define GII_PACK
#pragma pack(push, 1)
#else
#error You need to pack all structs in this file.
#endif

typedef enum {
	
	evNothing = 0,	/* event is not valid. (must be zero)	*/

	evCommand,	/* report command/do action		*/
	evInformation,	/* notification of new information	*/

	evExpose,	/* exposure event			*/
	/* empty slot */

	evKeyPress=5,	/* key has been pressed			*/
	evKeyRelease,	/* key has been released		*/
	evKeyRepeat,	/* automatically repeated keypress	*/

	evPtrRelative,	/* pointer movements reported relative	*/
	evPtrAbsolute,	/* pointer movements reported absolute	*/
	evPtrButtonPress,	/* pointer button pressed	*/
	evPtrButtonRelease,	/* pointer button released	*/

	evValRelative,	/* valuator change (reported relative)	*/
	evValAbsolute,	/* valuator change (reported absolute)	*/
	
	evFromAPI,	/* notification from certain apis	*/

	evLast		/* must be less than 33			*/

}  gii_event_type;


#define	EVMASK(x)	em ## x = (1 << ev ## x)
typedef enum {

	EVMASK(Nothing),

	EVMASK(Command),
	EVMASK(Information),
	EVMASK(Expose),

	EVMASK(KeyPress),
	EVMASK(KeyRelease),
	EVMASK(KeyRepeat),
	emKey		= emKeyPress | emKeyRelease | emKeyRepeat,
	emKeyboard	= emKey,

	EVMASK(PtrRelative),
	EVMASK(PtrAbsolute),
	EVMASK(PtrButtonPress),
	EVMASK(PtrButtonRelease),
	emPtrMove	= emPtrRelative | emPtrAbsolute,
	emPtrButton	= emPtrButtonPress | emPtrButtonRelease,
	emPointer	= emPtrMove | emPtrButton,

	EVMASK(ValRelative),
	EVMASK(ValAbsolute),
	emValuator	= emValRelative | emValAbsolute,

	EVMASK(FromAPI),

	emZero  = 0,
	emAll	= ((1 << evLast) - 1) & ~emNothing

} gii_event_mask;
#undef EVMASK


#define GII_EV_ORIGIN_NONE	0x00000000	/* Anonymous */
#define GII_EV_ORIGIN_SENDEVENT	0x80000000	/* Event was due to
						   SendEvent call */
#define GII_EV_ORIGIN(id)	(id)		/* Otherwise, it's an id */

#define GII_EV_TARGET_ALL	0x00000000	/* Broadcast event */
#define GII_EV_TARGET_QUEUE	0x80000000	/* Just queue the event */


#define	GII_EV_COMMON_DATA  \
	uint8_t	       size;		/* size of event in bytes	*/\
	uint8_t	       type;		/* type of this event		*/\
	int16_t        error;		/* error (for replies)		*/\
	uint32_t       origin;		/* origin device (etc)		*/\
	uint32_t       target;		/* target device (etc)		*/\
	int32_t        usec;		/* usec part of timestamp	*/\
	time_t         sec		/* sec part of timestamp	*/

/*	This information is reported with all events. Use the <any> field
**	in a gii_event structure to access these fields.
*/
typedef struct {
	
	GII_EV_COMMON_DATA;
	
} GII_PACK gii_any_event;



/*
******************************************************************************
 Command/Information events
******************************************************************************
*/

/* commands not defined by libgii must set this flag */
#define GII_CMDFLAG_PRIVATE     ((uint32_t)(1<<31))

typedef struct {
	
	GII_EV_COMMON_DATA;
	
	uint32_t	code;	               /* command/request code  */
	uint32_t        seqnum;
	
} GII_PACK gii_cmd_nodata_event;


#define GII_CMD_DATA_MAX  (248-sizeof(gii_cmd_nodata_event))
typedef struct {
	GII_EV_COMMON_DATA;
	
	uint32_t	code;			/* command/request code */
	uint32_t        seqnum;
	
	uint8_t	        data[GII_CMD_DATA_MAX];	/* command related data */
} GII_PACK gii_cmd_event;

/*
**  ->  None
**
**  <-  Inform that an event was lost due to queue overflow. No data
*/
#define GII_CMDCODE_EVENTLOST          0x00001

/*
**  ->  Tell sources that they should try to generate ABS/REL pointer
**      events. No data.
**
**  <-  None
*/
#define GII_CMDCODE_PREFER_ABSPTR      0x00002
#define GII_CMDCODE_PREFER_RELPTR      0x00003

/*
**  ->  Request device information. No data.
**
**  <-  Respond to information request, notify new device or device
**      state change. Data is "gii_cmddata_devinfo"
*/
#define GII_CMDCODE_DEVICE_INFO           0x10001
struct gii_cmddata_devinfo {

#       define GII_MAX_DEVNAME   32
	char            devname[GII_MAX_DEVNAME];
	
#       define GII_VENDOR_EXTERNAL  ((uint32_t)(1<<31))
#       define GII_PRODUCT_EXTERNAL ((uint32_t)(1<<31))

#define GII_VENDOR_GGI_PROJECT     0
#define GII_PRODUCT_GGI_PROJECT    0

	uint32_t        vendor_id;
	uint32_t        product_id;
	
	gii_event_mask  can_generate;

#define	GII_NUM_UNKNOWN 0xffffffff
	uint32_t        num_registers;

	uint32_t        num_valuators;
	uint32_t        num_buttons;
} GII_PACK;

/*
**  ->  Tell a device to stop operating (forever). No data
**
**  <-  Report that a device is not available anymore. No data
*/
#define GII_CMDCODE_DEVICE_CLOSE          0x10002

/*
**  ->  Enable/disable a device. No data. A disabled device
**      is still there but does not generate event.
**
**  <-  Confirm device enabling/disabling. No data.
*/
#define GII_CMDCODE_DEVICE_ENABLE         0x10003
#define GII_CMDCODE_DEVICE_DISABLE        0x10004

/*
**  ->  Initiate an io operation on a device register.
**      Data is struct gii_cmddata_ioreg
**
**  <-  Notify that the io operation is completed or that an error
**      occured if ev.any.error is set. Data is struct gii_cmddata_ioreg.
*/
#define GII_CMDCODE_SET_REGISTER   0x10005
#define GII_CMDCODE_GET_REGISTER   0x10006
struct gii_cmddata_ioreg {
	
#       define GIIREG_VENDOR_SPECIFIC  (1<<31)
#       define GIIREG_PRODUCT_SPECIFIC ((1<<31)|(1<<30))
	uint32_t   register_id;
	uint32_t   vendor_id;   /* ignored if the register_id is      */
	uint32_t   product_id;  /* not vendor/product specific        */
	
	/*  for SET_REGISTER, size and address of the buffer
	 *  containing the new data for this register;
	 *  for GET_REGISTER, size and address of the buffer
	 *  where libgii must write the result.
	 */
	uint32_t   size;
	char      *data;
} GII_PACK;

#define GIIREG_DEVICE_LONGNAME  1

/*
**  ->  Request register information.
**      Data is struct gii_cmddata_reginfo with only number set.
**
**  <-  Notify that the io operation is completed or that an error
**      occured if ev.any.error is set. Data is struct gii_cmddata_ioreg.
*/
#define GII_CMDCODE_REGISTER_INFO  0x10007
struct gii_cmddata_reginfo {
	
	uint32_t  number;    /* Number of the register */
#       define GII_REG_QUERY_ALL 0xffffffff
	/*  You may send this to get all registers at once, but it is
	 *  not recommended, as some events might be lost.  The
	 *  correct way is to get the number of registers and query
	 *  them one by one in a gii loop to avoid overflowing the
	 *  queue.
	 */
	
	uint32_t   vendor_id;
	uint32_t   product_id;
	uint32_t   register_id;
	
	/* name of this register */
#       define GII_MAX_REGNAME   32
	char       regname[GII_MAX_REGNAME];
	
	/*  size in bytes of this register */
	uint32_t   size;
	
	/*  if set, the address of a buffer owned by the device that
	 *  may be used by the user to read/write this register. The
	 *  GET/SET command must be used anyway to notify the device
	 *  of the change.
	 */
	char      *data;
	
} GII_PACK;


typedef struct gii_valrange {
	int32_t       	min, center, max;
} GII_PACK gii_valrange;

typedef enum {
	GII_PT_UNKNOWN,			/* unknown */
	GII_PT_TIME,			/* base unit s */
	GII_PT_FREQUENCY,		/* base unit 1/s (Hz) */
	GII_PT_LENGTH,			/* base unit m */
	GII_PT_VELOCITY,		/* base unit m/s */
	GII_PT_ACCELERATION,		/* base unit m/s^2 */
	GII_PT_ANGLE,			/* base unit radian */
	GII_PT_ANGVELOCITY,		/* base unit radian/s */
	GII_PT_ANGACCELERATION,		/* base unit radian/s^2 */
	GII_PT_AREA,			/* base unit m^2 */
	GII_PT_VOLUME,			/* base unit m^3 */
	GII_PT_MASS,			/* base unit kg */
	GII_PT_FORCE,			/* base unit N (kg*m/s^2) */
	GII_PT_PRESSURE,		/* base unit N/m^2 (Pa) */
	GII_PT_TORQUE,			/* base unit Nm */
	GII_PT_ENERGY,			/* base unit Nm, VAs, J */
	GII_PT_POWER,			/* base unit Nm/s, VA, W */
	GII_PT_TEMPERATURE,		/* base unit K */
	GII_PT_CURRENT,			/* base unit A */
	GII_PT_VOLTAGE,			/* base unit V (kg*m^2/(As^3)) */
	GII_PT_RESISTANCE,		/* base unit V/A (Ohm) */
	GII_PT_CAPACITY,		/* base unit As/V (Farad) */
	GII_PT_INDUCTIVITY,		/* base unit Vs/A (Henry) */
	GGI_PT_LAST
} gii_phystype;

/*
**  ->  Request valuator information.
**      Data is struct gii_cmddata_valinfo with only the number field set.
**
**  <-  Respond to valuator information request.
**      Data is struct gii_cmddata_valinfo.
*/
#define GII_CMDCODE_VALUATOR_INFO    0x10008
struct gii_cmddata_valinfo {
	
	uint32_t       	number;		/* Number of the valuator */
#define GII_VAL_QUERY_ALL 0xffffffff
	/*  You may send this to get all valuators at once, but it is
	 *  not recommended, as some events might be lost.  The
	 *  correct way is to get the number of valuators and query
	 *  them one by one in a gii loop to avoid overflowing the
	 *  queue.
	 */
	
	char		longname[75];
	char		shortname[5];
	
	gii_valrange	range;
	gii_phystype	phystype;
	
	/* The SI value can be computed using the values below
	 * and the following formula (unless SI_mul is 0, which
	 * means the device is non-linear or the factor is unknown).
	 *
	 *   float SI;
	 *
	 *   SI = (float) (SI_add + value[n]) * (float) SI_mul
	 *        / (float) SI_div * pow(2.0, SI_shift);
	 */
	
	int32_t 	SI_add,SI_mul,SI_div,SI_shift;
	
} GII_PACK;


/*
******************************************************************************
 Other events
******************************************************************************
*/

/*	Exposure events give rectangles that need to be refreshed.
*/
#define GII_CMDCODE_EXPOSE	0x20001
typedef struct {

	GII_EV_COMMON_DATA;

	uint32_t	x,y;
	uint32_t	h,w;

} GII_PACK gii_expose_event;


/*	key events should be used to report events obtained from keys and
**	other switches.
*/
typedef struct {

	GII_EV_COMMON_DATA;

	uint32_t	modifiers;	/* current modifiers in effect */
	uint32_t	sym;		/* meaning of key	*/
	uint32_t        label;		/* label on key		*/
	uint32_t        button;		/* button number	*/

} GII_PACK gii_key_event;

/*	This is used to report change of pointer position.
**	Depending on the event type, the values are either absolute
**	or relative.
*/
typedef struct {

	GII_EV_COMMON_DATA;

	int32_t	 x, y;		/* absolute/relative position	*/
	int32_t  z, wheel;

} GII_PACK gii_pmove_event;

/*	Button events are sent to report a change in pointer button
**	state.  Depending on the event type, the button is either being
**	pressed or released.
*/
typedef struct {

	GII_EV_COMMON_DATA;

	uint32_t	button; /* This is a number, NOT a mask */

} GII_PACK gii_pbutton_event;

#define GII_PBUTTON_(x)		(x)	/* Generic button access */

#define GII_PBUTTON_NONE	0	/* Dummy */

#define GII_PBUTTON_LEFT	1	/* Left or primary button */
#define GII_PBUTTON_PRIMARY	1
#define GII_PBUTTON_FIRST	1

#define GII_PBUTTON_RIGHT	2	/* Right/Secondary button */
#define GII_PBUTTON_SECONDARY	2
#define GII_PBUTTON_SECOND	2

#define GII_PBUTTON_MIDDLE	3	/* Middle or tertiary  */
#define GII_PBUTTON_TERTIARY	3
#define GII_PBUTTON_THIRD	3

/*	Valuator events report a change of up to 32 of the
**	input device valuators. Only a range of 32 valuators beginning
**	from first can be reported with one event.
*/
typedef struct {

	GII_EV_COMMON_DATA;

	uint32_t	first;		/* first valuator reported	*/
	uint32_t	count;		/* number reported		*/
	int32_t	        value[32];	/* absolute/relative values	*/

} GII_PACK gii_val_event;

/*
******************************************************************************
 Sublibrary Information events
******************************************************************************
*/

typedef struct {

	GII_EV_COMMON_DATA;

	int32_t		api_id;		/* The API that sent out this event */
	uint32_t        code;		/* The type of information sent */

} GII_PACK gii_fromapi_nodata_event;

#define GII_FROMAPI_DATA_MAX  (248-sizeof(gii_fromapi_nodata_event))
typedef struct {
	GII_EV_COMMON_DATA;

	int32_t		api_id;		/* The API that sent out this event */
	uint32_t        code;		/* The type of information sent */

	uint8_t	        data[GII_FROMAPI_DATA_MAX];	/* related data */
} GII_PACK gii_fromapi_event;


typedef union {

	uint8_t			size;		/* size of this event	*/

	gii_any_event		any;		/* access COMMON_DATA	*/
	gii_cmd_event		cmd;		/* command/information	*/
	gii_expose_event	expose;		/* exposure event	*/
	gii_val_event		val;		/* valuator change	*/
	gii_key_event		key;		/* key press/release	*/
	gii_pmove_event		pmove;		/* pointer move		*/
	gii_pbutton_event	pbutton;	/* pointer buttons	*/
	gii_fromapi_event	fromapi;	/* api information	*/

} GII_PACK gii_event;

#if defined __GNUC__
#elif defined _MSC_VER
#pragma pack(pop)
#endif
#undef GII_PACK

#endif	/*  _GGI_GII_EVENTS_H  */
