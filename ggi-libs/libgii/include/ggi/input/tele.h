/* $Id: tele.h,v 1.2 2006/10/02 07:28:18 cegger Exp $
******************************************************************************

   tele inputlib

   Copyright (C) 2006 Christoph Egger 

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#ifndef _GGI_INPUT_TELE_H
#define _GGI_INPUT_TELE_H

#include <ggi/gii-events.h>

typedef int (tele_tclient_poll)(TeleClient *c);
typedef int (tele_tclient_read)(TeleClient *c, TeleEvent *event);

typedef struct {
	TeleClient *client;

	tele_tclient_poll *tclient_poll;
	tele_tclient_read *tclient_read;	

} gii_tele_arg;


#define GII_CMDCODE_TELE_CONNECT	(0x01)
struct gii_cmddata_connect {
	int connected;
	int width, height;
};


#define GII_CMDCODE_TELE_WAIT4EVENT	(0x02)
#define GII_CMDCODE_TELE_UNLOCK		(0x04)
struct gii_cmddata_tele_event {
	TeleEvent ev;
	long type;
	long sequence;
};

#endif /* _GGI_INPUT_TELE_H */
