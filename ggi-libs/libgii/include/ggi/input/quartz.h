/* $Id: quartz.h,v 1.6 2007/05/10 23:31:31 cegger Exp $
******************************************************************************

   Quartz Input Header

   Copyright (C) 2004 Christoph Egger

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#ifndef _GGI_INPUT_QUARTZ_H
#define _GGI_INPUT_QUARTZ_H

#include <ApplicationServices/ApplicationServices.h>
#include <Carbon/Carbon.h>

typedef struct {
	WindowRef theWindow;
	void *gglock;		/* locking mutex */
} gii_inputquartz_arg;


#define GII_CMDCODE_RESIZE		(0x01 | GII_CMDFLAG_PRIVATE)
struct gii_cmddata_resize {
	WindowRef window;
	Rect origRect;		/* Original Bounds */
	Rect prevRect;		/* Previous Bounds */
	Rect curRect;		/* Current Bounds */
	void *arg;
};

#define GII_CMDCODE_QZSETPARAM		(0x04 | GII_CMDFLAG_PRIVATE)
struct gii_quartz_cmddata_setparam {
	WindowRef theWindow;
};

/* GII_CMDCODE_QZWINCLOSE: User clicked on window close, but
 *     but application has chance to react on this.
 */
#define GII_CMDCODE_QZWINCLOSE		(0x08 | GII_CMDFLAG_PRIVATE)
struct gii_quartz_cmddata_winclose {
	WindowRef theWindow;
};

#define GII_CMDCODE_INJECT_EVENT	(0x10 | GII_CMDFLAG_PRIVATE)
/* takes an event to inject */


#endif /* _GGI_INPUT_QUARTZ_H */
