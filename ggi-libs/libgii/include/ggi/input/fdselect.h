/* $Id: fdselect.h,v 1.3 2006/10/18 19:22:10 pekberg Exp $
******************************************************************************

   fdselect inputlib

   Copyright (C) 2006 Peter Rosin	[peda@lysator.liu.se]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#ifndef _GGI_INPUT_FDSELECT_H
#define _GGI_INPUT_FDSELECT_H

#define GII_FDSELECT_RELOAD      (1)
#define GII_FDSELECT_ADD         (2)
#define GII_FDSELECT_DEL         (3)
#define GII_FDSELECT_DEL_ALL     (4)
#define GII_FDSELECT_SET_FDSET   (5)
#define GII_FDSELECT_GET_FDSET   (6)

#define GII_FDSELECT_READY       (0)
#define GII_FDSELECT_READY_FDSET (1)

#define GII_FDSELECT_READ    0x1
#define GII_FDSELECT_WRITE   0x2
#define GII_FDSELECT_EXCEPT  0x4

struct gii_fdselect_fd {
	int fd;
	int mode;
};

struct gii_fdselect_fdset {
	fd_set *rfd;
	fd_set *wfd;
	fd_set *efd;
	int maxfd;
};

#endif /* _GGI_INPUT_FDSELECT_H */
