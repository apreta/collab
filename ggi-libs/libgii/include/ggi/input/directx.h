/* $Id: directx.h,v 1.8 2009/08/07 04:57:02 pekberg Exp $
******************************************************************************

   directx inputlib - use an existing directx window as a LibGII input source

   Copyright (C) 2004 Peter Ekberg	[peda@lysator.liu.se]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#ifndef _GGI_INPUT_DIRECTX_H
#define _GGI_INPUT_DIRECTX_H

#include <ggi/gii-events.h>

typedef void (gii_inputdx_settings_changed)(void *settings_changed_arg);

typedef struct {
	HANDLE hWnd;
	gii_inputdx_settings_changed *settings_changed;
	void *settings_changed_arg;
} gii_inputdx_arg;

typedef struct {
	int id;
	int mod;
	int vk;
} gii_inputdx_hotkey;
	

#define GII_CMDCODE_RESIZE             (0x01 | GII_CMDFLAG_PRIVATE)
#define GII_INPUTDX_HOTKEY             (0x02 | GII_CMDFLAG_PRIVATE)
#define GII_CMDCODE_INJECT_EVENT       (0x03 | GII_CMDFLAG_PRIVATE)

#endif /* _GGI_INPUT_DIRECTX_H */
