/* $Id: xwin.h,v 1.14 2009/08/06 20:46:52 ggibecka Exp $
******************************************************************************

   Xwin inputlib - use an existing X window as a LibGII input source

   Copyright (C) 1998 Marcus Sundberg	[marcus@ggi-project.org]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#ifndef _GGI_INPUT_XWIN_H
#define _GGI_INPUT_XWIN_H

#include <X11/Xlib.h>

/* Returns non-zero if we should queue the expose event */
typedef int (gii_inputxwin_lockfunc)(void *arg);
typedef int (gii_inputxwin_unlockfunc)(void *arg);

struct gii_inputxwin_arg {
	Display *disp;		/* Display to use */
	Window   win;		/* Window to use */
	int	 wait;		/* Wait for SETPARAM event before being
				   usable */
	void *exposearg;
	void *resizearg;
	gii_inputxwin_lockfunc *lockfunc; /* If this is non-null we call
				   the specified function to lock the polling
				   function */
	void	*lockarg;	/* This is the argument we pass to lockfunc */
	gii_inputxwin_unlockfunc *unlockfunc; /* If this is non-null we call
				   the specified function to unlock the polling
				   function */
	void	*unlockarg;	/* This is the argument we pass to unlockfunc*/
};



/* Send this event to change parameters on the fly */

#define GII_CMDCODE_RESIZE		(0x01 | GII_CMDFLAG_PRIVATE)
struct gii_cmddata_resize {
	int width, height;	/* New width/height */
	void *arg;
};

#define GII_CMDCODE_XWINEXPOSE		(0x02 | GII_CMDFLAG_PRIVATE)
struct gii_cmddata_expose {
	int x, y, w, h;
	void *arg;
};

#define GII_CMDCODE_XWINSETPARAM	(0x04 | GII_CMDFLAG_PRIVATE)
struct gii_xwin_cmddata_setparam {
	Window	 win;
	Window	 parentwin;
	int	 frames;
};

#define GII_CMDCODE_XWINPOINTER		(0x08 | GII_CMDFLAG_PRIVATE)
struct gii_xwin_cmddata_pointer {
	int ptralwaysrel;	/* Pointer events are always relative */
};

#define GII_CMDCODE_CLOSE		(0x10 | GII_CMDFLAG_PRIVATE)
/* No data */

#define GII_CMDCODE_EXITONCLOSE		(0x20 | GII_CMDFLAG_PRIVATE)
struct gii_xwin_cmddata_exitonclose {
	int exitonclose;
};

#define GII_CMDCODE_INJECT_EVENT	(0x40 | GII_CMDFLAG_PRIVATE)
/* takes an event to inject */

#define GII_CMDCODE_FOCUS		(0x80 | GII_CMDFLAG_PRIVATE)
struct gii_xwin_cmddata_focus {
	int has_focus;
};

#define GII_CMDCODE_SET_PAGE_OFFSET	(0x100 | GII_CMDFLAG_PRIVATE)
struct gii_xwin_cmddata_set_page_offset {
	int yoffset;
};

#define GII_CMDCODE_SELECTION_REQUEST	(0x200 | GII_CMDFLAG_PRIVATE)
#define GII_CMDCODE_SELECTION_CLEAR	(0x400 | GII_CMDFLAG_PRIVATE)
struct gii_xwin_cmddata_selection {
	Window owner;
	Window requestor;
	Atom selection;
	Atom target;
	Atom property;
	Time time;
};

#endif /* _GGI_INPUT_XWIN_H */
