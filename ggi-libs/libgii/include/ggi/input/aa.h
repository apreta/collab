/* $Id: aa.h,v 1.3 2009/08/07 05:01:00 pekberg Exp $
******************************************************************************

   aa inputlib - use an existing aa context as a LibGII input source

   Copyright (C) 2007 Christoph Egger

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#ifndef _GGI_INPUT_AA_H
#define _GGI_INPUT_AA_H

#define GII_CMDCODE_RESIZE	(0x01 | GII_CMDFLAG_PRIVATE)

#define GII_CMDCODE_AASETPARAM	(0x04 | GII_CMDFLAG_PRIVATE)
struct gii_aa_cmddata_setparam {
	struct aa_context *context;
};

#endif	/* _GGI_INPUT_AA_H */

