/* $Id: vnc.h,v 1.8 2008/09/16 06:53:44 pekberg Exp $
******************************************************************************

   VNC inputlib - use a VNC connection as a LibGII input source

   Copyright (C) 2008 Peter Rosin	[peda@lysator.liu.se]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#ifndef _GGI_INPUT_VNC_H
#define _GGI_INPUT_VNC_H

#include <ggi/gii-events.h>

typedef void (gii_vnc_new_client)(void *arg);
typedef int  (gii_vnc_client_data)(void *arg, int fd);
typedef int  (gii_vnc_write_client)(void *arg, int fd);
typedef int  (gii_vnc_safe_write)(void *arg, const uint8_t *data, int count);
typedef void (gii_vnc_add_cfd)(void *arg, void *ctx, int fd);
typedef void (gii_vnc_del_cfd)(void *arg, void *ctx, int fd);
typedef void (gii_vnc_add_cwfd)(void *arg, void *ctx, int fd);
typedef void (gii_vnc_del_cwfd)(void *arg, void *ctx, int fd);
typedef void (gii_vnc_add_crfd)(void *arg, void *ctx);
typedef void (gii_vnc_del_crfd)(void *arg, void *ctx);
typedef void (gii_vnc_key)(void *arg, int down, uint32_t key);
typedef void (gii_vnc_pointer)(void *arg, uint8_t mask,
	uint16_t x, uint16_t y);
typedef int  (gii_vnc_inject)(void *arg, void *ctx,
	uint8_t msg, uint16_t count, const uint8_t *buffer, int buf_size);

typedef struct {
	int	sfd;

	gii_vnc_new_client *new_client;
	gii_vnc_client_data *client_data;
	gii_vnc_write_client *write_client;
	gii_vnc_safe_write *safe_write;
	void *usr_ctx;

	gii_vnc_add_cfd  *add_cfd;
	gii_vnc_del_cfd  *del_cfd;
	gii_vnc_add_cwfd *add_cwfd;
	gii_vnc_del_cwfd *del_cwfd;
	gii_vnc_add_crfd *add_crfd;
	gii_vnc_del_crfd *del_crfd;
	gii_vnc_key      *key;
	gii_vnc_pointer  *pointer;
	gii_vnc_inject   *inject;
	void *gii_ctx;
} gii_vnc_arg;

#define GII_VNC_INJECT_EVENT         (0x01 | GII_CMDFLAG_PRIVATE)

#endif /* _GGI_INPUT_VNC_H */
