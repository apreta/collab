/* $Id: gii-module.h,v 1.8 2007/03/11 00:44:12 soyt Exp $
******************************************************************************

   LibGII API for input module writer
   
   Copyright (C) 1998 Andreas Beck	[becka@ggi-project.org]
   Copyright (C) 1999 Marcus Sundberg	[marcus@ggi-project.org]
   Copyright (C) 2005 Eric Faurot	[eric.faurot@gmail.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#ifndef _GGI_GII_MODULE_H
#define _GGI_GII_MODULE_H

#include <ggi/gii.h>

/*
 *  This header defines the module API for gii for input/filter module
 *  writers.
 */

__BEGIN_DECLS

/*
************************************************************************
** General purpose functions
************************************************************************
*/

/*  Clears an event and timestamp it. This could actually be part
 *  of the main API.
 */
GIIAPIFUNC void giiEventBlank(gii_event *, size_t size);

/*
************************************************************************
**  Source definition and manipulation
************************************************************************
*/

/*  A source is something that will generate gii events from lowlevel
 *  system events. It is implemented by a gii input module.
 */
struct gii_source;


#define GII_MODULE_SOURCE 0

typedef int (giifunc_src_init)(struct gii_source*,
			       const char*,
			       const char*,
			       void*);

struct gii_module_source {

	struct gg_module module;

	giifunc_src_init *init;
};

/*  Module hook.
 */
#define GII_DLINIT_SYM "GIIdlinit"

/*  Callback prototypes for source operation.
 */
typedef void (giifunc_src_close)  (struct gii_source *);
typedef int  (giifunc_src_poll)   (struct gii_source *);
typedef int  (giifunc_src_filter) (struct gii_source *, gii_event *);
typedef int  (giifunc_src_event)  (struct gii_source *, gii_event *);
typedef int  (giifunc_src_setmask)(struct gii_source *, gii_event_mask);

#define GII_OP_DONE      0 /* normal operation */
#define GII_OP_CLOSE     1 /* request closing  */
#define GII_OP_DISCARD   2 /* discard event (for filters) */
#define GII_OP_POLLAGAIN 3 /* wait a bit and poll again (for inputs) */

struct gii_source_ops {
	giifunc_src_close   *close;   /* close that source            */
	giifunc_src_poll    *poll;    /* try to generate events       */
	giifunc_src_filter  *filter;  /* filter event                 */
	giifunc_src_event   *event;   /* handle events sent to us     */
        giifunc_src_setmask *setmask; /* set desired event mask       */
};

struct gii_source {
	/**  A source is an instance of a module **/
	struct gg_instance    instance;
	
	/**  These must not be changed by the module code itself.  **/
	
	uint32_t              origin;   /* set by libgii, do not change */
	gii_event_mask        reqmask;  /* user requested event mask    */
	gii_event_mask        devmask;  /* mask provided by devices     */
	int                   flags;    /* will be checked after init, it
					   must not be changed afterwards */
	
	/** This can safely change anytime. **/
	void                  *priv;    /* source private data          */
	struct gii_source_ops ops;
	
	/** The rest is implementation specific. **/
	
#ifdef _GII_INTERNAL
	struct gii_source_internal impl;
#endif
};

/*  These functions are used to add/remove logical devices on a source.
 */
GIIAPIFUNC uint32_t giiAddDevice(struct gii_source *,
				 struct gii_cmddata_devinfo *,
				 struct gii_cmddata_valinfo *);

GIIAPIFUNC int      giiDelDevice(struct gii_source *, uint32_t);

/*  This function is used by a source to queue an event.
 */
GIIAPIFUNC int      giiPostEvent(struct gii_source *, gii_event *);


/*
************************************************************************
**  Lowlevel event registration
************************************************************************
*/

enum {
	GII_NERVE_FD = 0,
	GII_NERVE_TIMER,
	GII_NERVE_HANDLE,
	GII_NERVE_TICK,
	
	GII_NERVE_LAST
};

#ifndef __WIN32__
#define HANDLE void *
#endif

#define COMMON int type; uint32_t *nerve;
union gii_nerve {
	struct { COMMON } any;
	struct { COMMON int fd; int mode;   } fd;
	struct { COMMON unsigned int delay; } timer;
	struct { COMMON HANDLE handle;      } handle;
};
#undef COMMON

/*  This function tell libgii what lowlevel event might result in an
 *  gii event. The gii_nerve.any.nerve must be a pointer to a valid
 *  memory location that libgii will alter to notify the source that
 *  the corresponding lowlevel event has occured.
 */
GIIAPIFUNC int giiSetNerve(struct gii_source *, union gii_nerve *);

/*  Tell libgii to ignore the correponding lowlevel event.
 *  The second arg must be the address specified in the corresponding
 *  gii_nerve.any.nerve
 */
GIIAPIFUNC int giiCutNerve(struct gii_source *, uint32_t *);

/*
**  Events based on FD being ready for reading(/writing?)
*/
#define GII_NERVE_FD_READ    0x1
#define GII_NERVE_FD_WRITE   0x2
#define GII_NERVE_FD_EXCEPT  0x4

static inline int giiSetNerveFD(struct gii_source *src, uint32_t *addr, int fd, int mode) {
	union gii_nerve nrv;
	nrv.any.type = GII_NERVE_FD;
	nrv.any.nerve = addr;
	nrv.fd.fd = fd;
	nrv.fd.mode = mode;
	return giiSetNerve(src,&nrv);
}

#define giiSetNerveReadFD(src, addr, fd) giiSetNerveFD(src, addr, fd, GII_NERVE_FD_READ)

/*
**  One-shot timers
*/
static inline int giiSetNerveTimer(struct gii_source * src, uint32_t *addr, unsigned int delay) {
	union gii_nerve nrv;
	nrv.any.type = GII_NERVE_TIMER;
	nrv.any.nerve = addr;
	nrv.timer.delay = delay;
	return giiSetNerve(src,&nrv);
}

/*
**  Win32 HANDLE
*/
static inline int giiSetNerveHandle(struct gii_source *src, uint32_t *addr, HANDLE handle) {
	union gii_nerve nrv;
	nrv.any.type = GII_NERVE_HANDLE;
	nrv.any.nerve = addr;
	nrv.handle.handle = handle;
	return giiSetNerve(src,&nrv);
}

/*
**  poll at regular interval
*/
static inline int giiSetNerveTick(struct gii_source * src, uint32_t *addr, unsigned int freq) {
	union gii_nerve nrv;
	nrv.any.type = GII_NERVE_TICK;
	nrv.any.nerve = addr;
	nrv.timer.delay = freq;
	return giiSetNerve(src,&nrv);
}


__END_DECLS


#endif /*  _GGI_GII_MODULE_H  */
