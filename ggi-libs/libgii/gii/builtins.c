/* $Id: builtins.c,v 1.32 2007/04/24 20:01:25 pekberg Exp $
******************************************************************************

   Libgii builtin targets binding.

   Copyright (C) 2005 Eric Faurot	[eric.faurot@gmail.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/gg.h>
#include <ggi/gg-api.h>
#include <ggi/gii-module.h>
#include <ggi/internal/gii_debug.h>
#include <string.h>


#ifdef BUILTIN_FILTER_KEYTRANS
extern struct gg_module GII_filter_keytrans;
#endif
#ifdef BUILTIN_FILTER_MOUSE
extern struct gg_module GII_filter_mouse;
#endif
#ifdef BUILTIN_FILTER_TCP
extern struct gg_module GII_filter_tcp;
#endif
#ifdef BUILTIN_FILTER_SAVE
extern struct gg_module GII_filter_save;
#endif
#ifdef BUILTIN_FILTER_TILE
extern struct gg_module GII_filter_tile;
#endif
#ifdef BUILTIN_INPUT_AA
extern struct gg_module GII_aa;
#endif
#ifdef BUILTIN_INPUT_COCOA
extern struct gg_module GII_cocoa;
#endif
#ifdef BUILTIN_INPUT_DIRECTX
extern struct gg_module GII_directx;
#endif
#ifdef BUILTIN_INPUT_FDSELECT
extern struct gg_module GII_fdselect;
#endif
#ifdef BUILTIN_INPUT_FILE
extern struct gg_module GII_file;
#endif
#ifdef BUILTIN_INPUT_IPAQ_TS
extern struct gg_module GII_ipaq;
#endif
#ifdef BUILTIN_INPUT_KII
extern struct gg_module GII_kii;
#endif
#ifdef BUILTIN_INPUT_LINUX_EVDEV
extern struct gg_module GII_linux_evdev;
#endif
#ifdef BUILTIN_INPUT_LINUX_KBD
extern struct gg_module GII_linux_kbd;
#endif
#ifdef BUILTIN_INPUT_LINUX_JOY
extern struct gg_module GII_linux_joy;
#endif
#ifdef BUILTIN_INPUT_LINUX_MOUSE
extern struct gg_module GII_linux_mouse;
#endif
#ifdef BUILTIN_INPUT_LK201
extern struct gg_module GII_lk201;
#endif
#ifdef BUILTIN_INPUT_MEMORY
extern struct gg_module GII_memory;
#endif
#ifdef BUILTIN_INPUT_MOUSE
extern struct gg_module GII_mouse;
#endif
#ifdef BUILTIN_INPUT_NULL
extern struct gg_module GII_null;
#endif
#ifdef BUILTIN_INPUT_PCJOY
extern struct gg_module GII_pcjoy;
#endif
#ifdef BUILTIN_INPUT_QUARTZ
extern struct gg_module GII_quartz;
#endif
#ifdef BUILTIN_INPUT_SPACEORB
extern struct gg_module GII_spaceorb;
#endif
#ifdef BUILTIN_INPUT_STDIN
extern struct gg_module GII_stdin;
#endif
#ifdef BUILTIN_INPUT_TCP
extern struct gg_module GII_tcp;
#endif
#ifdef BUILTIN_INPUT_TELE
extern struct gg_module GII_tele;
#endif
#ifdef BUILTIN_INPUT_TERMINFO
extern struct gg_module GII_terminfo;
#endif
#ifdef BUILTIN_INPUT_VGL
extern struct gg_module GII_vgl;
#endif
#ifdef BUILTIN_INPUT_VNC
extern struct gg_module GII_vnc;
#endif
#ifdef BUILTIN_INPUT_X
extern struct gg_module GII_x;
extern struct gg_module GII_xwin;
#ifdef HAVE_XF86DGA
extern struct gg_module GII_xf86dga;
#endif
#endif
#ifdef BUILTIN_INPUT_ZAURUS_TS
extern struct gg_module GII_zaurus;
#endif


static struct gg_module *modules[] = {
#ifdef BUILTIN_FILTER_KEYTRANS
	&GII_filter_keytrans,
#endif
#ifdef BUILTIN_FILTER_MOUSE
	&GII_filter_mouse,
#endif
#ifdef BUILTIN_FILTER_SAVE
	&GII_filter_save,
#endif
#ifdef BUILTIN_FILTER_TCP
	&GII_filter_tcp,
#endif
#ifdef BUILTIN_FILTER_TILE
	&GII_filter_tile,
#endif
#ifdef BUILTIN_INPUT_AA
	&GII_aa,
#endif
#ifdef BUILTIN_INPUT_COCOA
	&GII_cocoa,
#endif
#ifdef BUILTIN_INPUT_DIRECTX
	&GII_directx,
#endif
#ifdef BUILTIN_INPUT_FDSELECT
	&GII_fdselect,
#endif
#ifdef BUILTIN_INPUT_FILE
	&GII_file,
#endif
#ifdef BUILTIN_INPUT_IPAQ_TS
	&GII_ipaq,
#endif
#ifdef BUILTIN_INPUT_KII
	&GII_kii,
#endif
#ifdef BUILTIN_INPUT_LK201
	&GII_lk201,
#endif
#ifdef BUILTIN_INPUT_LINUX_EVDEV
	&GII_linux_evdev,
#endif
#ifdef BUILTIN_INPUT_LINUX_JOY
	&GII_linux_joy,
#endif
#ifdef BUILTIN_INPUT_LINUX_KBD
	&GII_linux_kbd,
#endif
#ifdef BUILTIN_INPUT_LINUX_MOUSE
	&GII_linux_mouse,
#endif
#ifdef BUILTIN_INPUT_MEMORY
	&GII_memory,
#endif
#ifdef BUILTIN_INPUT_MOUSE
	&GII_mouse,
#endif
#ifdef BUILTIN_INPUT_NULL
	&GII_null,
#endif
#ifdef BUILTIN_INPUT_PCJOY
	&GII_pcjoy,
#endif
#ifdef BUILTIN_INPUT_QUARTZ
	&GII_quartz,
#endif
#ifdef BUILTIN_INPUT_SPACEORB
	&GII_spaceorb,
#endif
#ifdef BUILTIN_INPUT_STDIN
	&GII_stdin,
#endif
#ifdef BUILTIN_INPUT_TCP
	&GII_tcp,
#endif
#ifdef BUILTIN_INPUT_TERMINFO
	&GII_terminfo,
#endif
#ifdef BUILTIN_INPUT_VGL
	&GII_vgl,
#endif
#ifdef BUILTIN_INPUT_VNC
	&GII_vnc,
#endif
#ifdef BUILTIN_INPUT_X
        &GII_x,
	&GII_xwin,
#ifdef HAVE_XF86DGA
	&GII_xf86dga,
#endif
#endif
#ifdef BUILTIN_INPUT_ZAURUS_TS
	&GII_zaurus,
#endif
	NULL
};

static int
GIIdl(int item, void **itemptr) {
	struct gg_module ***modulesptr;
	switch(item) {
	case GG_DLENTRY_MODULES:
		modulesptr = (struct gg_module ***)itemptr;
		*modulesptr = modules;
		return GGI_OK;
	default:
		*itemptr = NULL;
	}
	return GGI_ENOTFOUND;
}

static void * _builtins_get(void *_, const char *symbol) {
	if(!strcmp(symbol, GII_DLINIT_SYM))
		return GIIdl;
	DPRINT_LIBS("! builtin symbol '%s' not found\n",symbol);
	return NULL;
}

static struct gg_scope *_builtins;

void _giiInitBuiltins(void);
void _giiExitBuiltins(void);

void _giiInitBuiltins(void)
{
	_builtins = ggNewScope("@libgii", NULL, &_builtins_get,  NULL);
}

void _giiExitBuiltins(void)
{
	ggDelScope(_builtins);
}

#ifndef HAVE_CONFFILE
char const *_giibuiltinconf[] = {
#include "builtins.inc"
	NULL
};
#endif /* HAVE_CONFFILE */
