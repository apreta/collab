/* $Id: gii-internal.h,v 1.8 2007/06/21 18:03:07 pekberg Exp $
******************************************************************************

   LibGII implementation specific headers

   Copyright (C) 1998 Andreas Beck	[becka@ggi-project.org]
   Copyright (C) 1999 Marcus Sundberg	[marcus@ggi-project.org]
   Copyright (C) 2005 Eric Faurot	[eric.faurot@gmail.com]
   
   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#define _GII_INTERNAL

#include <ggi/gg.h>
#include <ggi/gg-api.h>
#include <ggi/gg-queue.h>
#include <ggi/gii-events.h>

/*
************************************************************************
**  Event management
************************************************************************
*/

#define GII_MAXSOURCES  0x7fffff00
#define GII_MAXDEVICES	256

#define GII_DEVICEMASK	0x000000ff
#define GII_SOURCEMASK	0xffffff00

#define GII_IS_SAME_SOURCE(a,b) ((a&GII_SOURCEMASK) == (b&GII_SOURCEMASK))

#define GII_QUEUE_SIZE      8192
#define GII_Q_THRESHOLD     (GII_QUEUE_SIZE - (int)sizeof(gii_event) - 1)

/* The threshold the point where events wrap back to the start of the
 * buffer.  Events are stored in the buffer in whole; they do not get
 * split in half at the end of the buffer.  Instead, the event that
 * crosses the threshold will physically be the last, and the next event
 * has offset 0 in the buffer.
 *
 * Corollary: head is always <= Q_THRESHOLD.
 */
struct gii_ev_queue {
	int     count;	/* number of events in the buffer */
	int     head;	/* offset into buf[] where next event is put */
	int     tail;	/* offset into buf[] of last event */
	uint8_t buf[GII_QUEUE_SIZE];
};

struct gii_ev_queue_set {
	gii_event_mask	    seen;
	struct gii_ev_queue *queues[evLast];
	void		    *mutex;
};

/*
************************************************************************
**  Internal input implementation
************************************************************************
*/

struct gii_input;
struct gii_source;
union  gii_nerve;

GG_LIST_HEAD(gii_sources, gii_source);

/*  An input holds a queue of events, organized per event type.
 *  It also holds a doubly linked list of underlying sources that
 *  generate events and post them on the queue.
 */
struct gii_input {
	void                   *mutex;     /* lock for this input    */
	struct gii_ev_queue_set	queue;     /* event queue            */
	gii_event_mask          reqmask;   /* desired events         */
	gii_event_mask          canmask;   /* possible events        */
	struct gii_sources      sources;   /* list of opened sources */
	void *                  reactor;   /* reactor specific data  */
};

#define input_INIT_SOURCES(inp)     GG_LIST_INIT(&inp->sources)
#define input_FIRST_SOURCE(inp)     GG_LIST_FIRST(&inp->sources)
#define input_NEXT_SOURCE(inp,v)    GG_LIST_NEXT(v,impl.le0)
#define input_FOREACH_SOURCE(inp,v) GG_LIST_FOREACH(v,&inp->sources,impl.le0)
#define input_ADD_SOURCE(inp,v)     GG_LIST_INSERT_HEAD(&inp->sources,v,impl.le0)
#define input_REM_SOURCE(inp,v)     GG_LIST_REMOVE(v,impl.le0)

/*
************************************************************************
**  Internal source implementation
************************************************************************
*/

GG_LIST_HEAD(gii_devices, gii_device);

struct gii_device {
	uint32_t                    origin;
	struct gii_cmddata_devinfo *devinfo;
	struct gii_cmddata_valinfo *valinfo;
	GG_LIST_ENTRY(gii_device)   devices;
};

/*  In case we want to define flags on sources. It is always useful...
 */
#define GII_SRCFLAG_PUBLIC   0x0000ffff
#define GII_SRCFLAG_PRIVATE  0xffff0000

#define GII_SRCFLAG_OPENING  0x00010000
#define GII_SRCFLAG_CLOSING  0x00020000


struct gii_source_internal {
	struct gii_input         *inp;       /* the input we belong to    */
	uint32_t                  next_origin;
	
	GG_LIST_ENTRY(gii_source) le0;   /* other sources on that input   */
	GG_LIST_ENTRY(gii_source) le1;   /* spare list entry              */
	
        struct gii_devices        devices;   /* device list               */
        struct gii_devices        origins;   /* free origin list          */
	void                     *reactor;   /* reactor specific data     */
};

#define source_INIT_DEVICES(src)     GG_LIST_INIT(&src->impl.devices)
#define source_FIRST_DEVICE(src)     GG_LIST_FIRST(&src->impl.devices)
#define source_NEXT_DEVICE(src,v)    GG_LIST_NEXT(v,devices)
#define source_FOREACH_DEVICE(src,v) GG_LIST_FOREACH(v,&src->impl.devices,devices)
#define source_ADD_DEVICE(src,v)     GG_LIST_INSERT_HEAD(&src->impl.devices,v,devices)
#define source_REM_DEVICE(src,v)     GG_LIST_REMOVE(v,devices)

#define source_INIT_ORIGINS(src)     GG_LIST_INIT(&src->impl.origins)
#define source_FIRST_ORIGIN(src)     GG_LIST_FIRST(&src->impl.origins)
#define source_NEXT_ORIGIN(src,v)    GG_LIST_NEXT(v,devices)
#define source_FOREACH_ORIGIN(src,v) GG_LIST_FOREACH(v,&src->impl.origins,devices)
#define source_ADD_ORIGIN(src,v)     GG_LIST_INSERT_HEAD(&src->impl.origins,v,devices)
#define source_REM_ORIGIN(src,v)     GG_LIST_REMOVE(v,devices)
#define source_IAFTER_ORIGIN(src,v1,v2) GG_LIST_INSERT_AFTER(v1,v2,devices)


/*
************************************************************************
**  Internal functions
************************************************************************
*/

/* Builtins module initialization */

void _giiInitBuiltins(void);

void _giiExitBuiltins(void);

int giiCloseSource(struct gii_source *);

/*
************************************************************************
**  Reactor
************************************************************************
*/

/*
 *  The ops structure is the (one way) opaque interface through which
 *  libgii delegates lowlevel event handling to a specific reactor.
 *  The idea is to have reactor optimized on different platforms,
 *  but still use the same core input implementation.
 */

/* The following functions may return GGI_OK or an errror code */

typedef int(giifunc_reactor_init)(struct gii_input *);
typedef void(giifunc_reactor_exit)(struct gii_input *);
typedef int(giifunc_reactor_poll)(struct gii_input *, struct timeval *);
typedef int(giifunc_reactor_bind)(struct gii_source *);
typedef void(giifunc_reactor_unbind)(struct gii_source *);
typedef int(giifunc_reactor_set_nerve)(struct gii_source *, union gii_nerve *);
typedef int(giifunc_reactor_cut_nerve)(struct gii_source *, uint32_t *);
typedef int(giifunc_reactor_transfer)(struct gii_source *, struct gii_input*);

struct gii_reactor {
	giifunc_reactor_init      *init;
	giifunc_reactor_exit      *exit;
	giifunc_reactor_bind      *bind;
	giifunc_reactor_unbind    *unbind;
	giifunc_reactor_poll      *poll;
	giifunc_reactor_cut_nerve *cut_nerve;
	giifunc_reactor_set_nerve *set_nerve;
	giifunc_reactor_transfer  *transfer;
};

/* Reactor subsystem initialization */
struct gii_reactor * _giiInitReactor(void);

void                 _giiExitReactor(struct gii_reactor *);
