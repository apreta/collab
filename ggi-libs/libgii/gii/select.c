/* $Id: select.c,v 1.25 2009/08/10 09:15:28 pekberg Exp $
******************************************************************************

   LibGII - select(2)-based reactor implementation
   
   Copyright (C) 2005 Eric Faurot	[eric.faurot@gmail.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"

#include "gii-internal.h"
#include <ggi/gii-module.h>
#include <ggi/internal/gii_debug.h>

#include <sys/types.h>
#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#include <string.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <errno.h>

struct gii_reaction {
	
	int                 type;
	struct gii_source  *source;
	uint32_t           *nerve; /* user slot for event notification */
	
	GG_LIST_ENTRY(gii_reaction) le0; /* input and pool */
	GG_LIST_ENTRY(gii_reaction) le1; /* source */
	
	union {
		struct { int fd;   int mode;} fd;
		struct { struct timeval at; } timer;
	} _;
};

GG_LIST_HEAD(gii_reactions, gii_reaction);

struct gii_reactor_inp {
	struct gii_reactions fds;
	struct gii_reactions timers;
	struct gii_reactions ticks;

	struct gii_reaction * tick; /* the tick installed on this input */
	
	/*
	  XXX possible improvement:
	  
	  The always keep fd sets for read/write file descriptiors.
	  When a fd reaction is added, simply add it to the set.
	  When a fd recation is cut, mark the fdsets as dirty.
	  
	  At poll time, check if the fd sets are dirty. If so iterate over
	  the registered fd reactions and rebuild the fd set.
	  Its avoid setting the fds all the time, and it is likely to
	  be more efficient if fds are not often removed.
	*/
};

struct gii_reactor_src {
	
	struct gii_reactions  rcts;  /* list for this source */
	
	int seen; /* number of reactions that were triggered */
};

#define SOURCE_REACTOR(src)   ((struct gii_reactor_src*)(src->impl.reactor))

#define SOURCE_REACTIONS(src)      (&(SOURCE_REACTOR(src)->rcts))
#define SOURCE_FIRST_REACTION(src) GG_LIST_FIRST(SOURCE_REACTIONS(src))
#define SOURCE_FOREACH_REACTION(src, rea)                                    \
        GG_LIST_FOREACH(rea,SOURCE_REACTIONS(src),le1)
#define SOURCE_ADD_REACTION(src, rea)                                        \
        GG_LIST_INSERT_HEAD(SOURCE_REACTIONS(src),rea,le1)
#define SOURCE_REM_REACTION(src, rea) GG_LIST_REMOVE(rea,le1)


#define INPUT_REACTOR(inp) ((struct gii_reactor_inp*)(inp->reactor))

#define INPUT_FDS(inp)     (&(INPUT_REACTOR(inp)->fds))
#define INPUT_FOREACH_FD(inp, rea) GG_LIST_FOREACH(rea,INPUT_FDS(inp),le0)
#define INPUT_ADD_FD(inp, rea) GG_LIST_INSERT_HEAD(INPUT_FDS(inp),rea,le0)
#define INPUT_REM_FD(inp, rea) GG_LIST_REMOVE(rea,le0)

#define INPUT_TIMERS(inp)            (&(INPUT_REACTOR(inp)->timers))
#define INPUT_FOREACH_TIMER(inp,rea) GG_LIST_FOREACH(rea,INPUT_TIMERS(inp),le0)
#define INPUT_FIRST_TIMER(inp)       GG_LIST_FIRST(INPUT_TIMERS(inp))
#define INPUT_REM_TIMER(inp, rea)    GG_LIST_REMOVE(rea,le0)
#define INPUT_NEXT_TIMER(rea)        GG_LIST_NEXT(rea,le0)

#define INPUT_TICKS(inp)            (&(INPUT_REACTOR(inp)->ticks))
#define INPUT_HAS_TICK(inp)         GG_LIST_FIRST(INPUT_TICKS(inp))
#define INPUT_FOREACH_TICK(inp,rea) GG_LIST_FOREACH(rea,INPUT_TICKS(inp),le0)
#define INPUT_ADD_TICK(inp,rea)   GG_LIST_INSERT_HEAD(INPUT_TICKS(inp),rea,le0)
#define INPUT_REM_TICK(inp,rea)   GG_LIST_REMOVE(rea,le0)

/* a > b  -> -1 
 * a = b  ->  1
 * a < b  ->  0
 */
static inline int ggCmpTime(struct timeval *a, struct timeval *b) {
	if (a->tv_sec  > b->tv_sec)  return -1;
	if (a->tv_sec  < b->tv_sec)  return 1;
	if (a->tv_usec > b->tv_usec) return -1;
	if (a->tv_usec < b->tv_usec) return 1;
	return 0;
}

#define ggTimeGT(a,b)                                                  \
        (((a)->tv_sec > (b)->tv_sec) ||                                \
        (((a)->tv_sec == (b)->tv_sec)&&(a)->tv_usec > (b)->tv_usec))
#define ggTimeGE(a,b)                                                  \
        (((a)->tv_sec > (b)->tv_sec) ||                                \
        (((a)->tv_sec == (b)->tv_sec)&&(a)->tv_usec >= (b)->tv_usec))

/* c = a - b (a > b _must_ hold) */
#define ggTimeSub(a, b, c) do {                                        \
	(c)->tv_sec = (a)->tv_sec - (b)->tv_sec;                       \
	if((b)->tv_usec > (a)->tv_usec) {                              \
		(c)->tv_usec = 1000000 - (b)->tv_usec + (a)->tv_usec;  \
		(c)->tv_sec--;                                         \
	} else {                                                       \
		(c)->tv_usec = (a)->tv_usec - (b)->tv_usec;            \
	} } while(0)

#define ggTimeAdd(a, b, c) do {                                        \
	(c)->tv_sec = (a)->tv_sec + (b)->tv_sec;                       \
	(c)->tv_usec = (a)->tv_usec + (b)->tv_usec;                    \
	if ((c)->tv_usec > 999999) {                                   \
		(c)->tv_sec++;                                         \
		(c)->tv_usec %= 1000000;                               \
	} } while(0)

static struct gii_reactions reaction_pool;
static int reaction_pool_size;

static struct gii_reaction*
new_reaction(struct gii_source *src, int type, uint32_t *nrv)
{
	struct gii_reaction *rea;
	
	if ((rea = GG_LIST_FIRST(&reaction_pool)) == NULL) {
		if((rea = calloc(1, sizeof(*rea))) == NULL)
			return NULL;
		reaction_pool_size++;
	} else {
		GG_LIST_REMOVE(rea, le0);
	}
	
	rea->type   = type;
	rea->nerve  = nrv;
	rea->source = src;
	
	/* Normal reactions have a nerve. */
	if(rea->nerve)
		*rea->nerve = 0;
	if(src)
		SOURCE_ADD_REACTION(src, rea);
	
	return rea;
}

#define DEL_REACTION(rea) GG_LIST_INSERT_HEAD(&reaction_pool, rea, le0)


static int
new_fd(struct gii_source *src, uint32_t *nrv, int  fd, int mode)
{
	struct gii_reaction *rea;

	DPRINT_REACTOR("- new fd src=%p, nrv=%p, fd=%i mode=%i\n", src, nrv, fd, mode);
	
	if ((mode & (GII_NERVE_FD_READ | GII_NERVE_FD_WRITE)) == 0) {
		DPRINT_REACTOR("! invalid mode for fd event.\n");
		return GGI_EARGINVAL;
	}
	
	if((rea = new_reaction(src, GII_NERVE_FD, nrv)) == NULL) {
		DPRINT_REACTOR("! cannot allocate reaction.\n");
		return GGI_ENOMEM;
	}
	
	rea->_.fd.fd   = fd;
	rea->_.fd.mode = mode;
	
	INPUT_ADD_FD(src->impl.inp, rea);

	return GGI_OK;
}

/* timers are always sorted */
static inline void add_timer(struct gii_input *inp,
			     struct gii_reaction *rea) {

#define ADD_FIRST(inp,rea)      GG_LIST_INSERT_HEAD(INPUT_TIMERS(inp),rea,le0)
#define ADD_AFTER(inp,rea,ref)  GG_LIST_INSERT_AFTER(ref,rea,le0)
#define ADD_BEFORE(inp,rea,ref) GG_LIST_INSERT_BEFORE(ref,rea,le0)

	struct gii_reaction *tmp;
	/* XXX: not a nice loop, can probably be improved */
	INPUT_FOREACH_TIMER(inp, tmp) {
		if(ggTimeGT(&tmp->_.timer.at,&rea->_.timer.at)) {
			ADD_BEFORE(inp,tmp,rea);
			return;
		}
		if(INPUT_NEXT_TIMER(tmp) == NULL) {
			ADD_AFTER(inp,tmp,rea);
			return;
		}
	}
	ADD_FIRST(inp, rea);
}

static int
new_timer(struct gii_source *src, uint32_t *nrv, unsigned int delay)
{
	struct gii_reaction *rea;
	struct timeval tv;
	
	DPRINT_REACTOR("- new timer src=%p, nrv=%p, delay=%i\n", src, nrv, delay);
	
	if((rea = new_reaction(src, GII_NERVE_TIMER, nrv)) == NULL)
		return GGI_ENOMEM;
	
	tv.tv_sec  = delay / 1000;
	tv.tv_usec = delay % 1000;
	ggCurTime(&rea->_.timer.at);
	ggTimeAdd(&rea->_.timer.at, &tv, &rea->_.timer.at);
	add_timer(rea->source->impl.inp, rea);
	
	return GGI_OK;
}

static int
new_tick(struct gii_source *src, uint32_t *nrv, unsigned int freq)
{
	struct gii_reaction *rea, *tick;
	struct timeval tv;
	
	DPRINT_REACTOR("- new tick src=%p, nrv=%p, freq=%i\n", src, nrv, freq);
	
	if((rea = new_reaction(src, GII_NERVE_TICK, nrv)) == NULL)
		return GGI_ENOMEM;
	
	if (INPUT_HAS_TICK(rea->source->impl.inp) == NULL) {
		/* We are the first to use ticks, so we need to schedule the
		 * fake tick reaction (100ms). Freq is not used right now.
		 */
		tick = INPUT_REACTOR(rea->source->impl.inp)->tick;
		tv.tv_sec  = 0;
		tv.tv_usec = 100000;
		ggCurTime(&tick->_.timer.at);
		ggTimeAdd(&tick->_.timer.at, &tv, &tick->_.timer.at);
		add_timer(rea->source->impl.inp, tick);
	}
	
	INPUT_ADD_TICK(rea->source->impl.inp, rea);
	
	return GGI_OK;
}


/*
 *  Count all exhausted timers and add the associated sources to the given poll list.
 *  The timers reactions are deleted. Returns the number of exhausted timers.
 */
static inline int
check_timers(struct gii_input* inp, struct gii_sources * to_poll, struct timeval *now)
{
	struct gii_reaction *rea, *tmp;
	struct timeval tv;
	int timers;
	int reschedule = 0;
	timers = 0;
	
	DPRINT_REACTOR("- checking timers at %i.%06i.\n", now->tv_sec, now->tv_usec);

	while((rea = INPUT_FIRST_TIMER(inp))) {
		/*
		 * This work because time is strictly monotonic, so
		 * the timer cannot be scheduled earlier than an already 
		 * exhausted one.
		 */
		if (!ggTimeGE(now, &rea->_.timer.at))
			break;

		/* Intercept our tick */
		if(rea == INPUT_REACTOR(inp)->tick) {
			DPRINT_REACTOR("- tick!\n");
			INPUT_FOREACH_TICK(inp, tmp) {
				(*tmp->nerve)++;
				timers++;
				if(SOURCE_REACTOR(tmp->source)->seen++ == 0) {
					DPRINT_REACTOR("- adding source %p to poll list\n", (void*)tmp->source);
					GG_LIST_INSERT_HEAD(to_poll, tmp->source, impl.le1);
				}
			}
			/* We will reschedule it later if needed */
			INPUT_REM_TIMER(inp,rea);
			reschedule = 1;
			continue;
		}
		
		DPRINT_REACTOR("- timer exhausted at %i.%06i for source %s.\n",rea->_.timer.at.tv_sec, rea->_.timer.at.tv_usec, rea->source);
		timers++;
		(*rea->nerve)++;
		
		if(SOURCE_REACTOR(rea->source)->seen++ == 0) {
			DPRINT_REACTOR("- adding source %p to poll list\n", (void*)rea->source);
			GG_LIST_INSERT_HEAD(to_poll, rea->source, impl.le1);
		}
		/* consume this timer event */
		GG_LIST_REMOVE(rea,le0);
		GG_LIST_REMOVE(rea,le1);
		DEL_REACTION(rea);
	}
	
	if (reschedule && INPUT_HAS_TICK(inp)) {
		/* Schedule the next tick */
		tv.tv_sec = 0;
		tv.tv_usec = 100000;
		ggTimeAdd(now, &tv, &(INPUT_REACTOR(inp)->tick->_.timer.at));
		add_timer(inp, INPUT_REACTOR(inp)->tick);
	}
	
	return timers;
}


static int
select_poll(struct gii_input* inp, struct timeval *tv)
{
	struct gii_reaction *rea;
	struct gii_source *src, *src2;
	struct timeval ttv, now, *ptv, start;
	int ret, evts, maxfd;
	fd_set rfds, wfds;
	
	/* list of sources to poll */
	struct gii_sources to_poll;

	if(tv)
		DPRINT_REACTOR("- select_poll inp=%p, tv=%i.%06is\n",inp, tv->tv_sec, tv->tv_usec);
	else
		DPRINT_REACTOR("- select_poll inp=%p, tv=%p\n",inp, tv);
	
	GG_LIST_INIT(&to_poll);
	
	/*
	  The idea is very simple:
	  
	  1. For all reactions installed on that input, check wether
	     the underlying lowelevel event occured. If yes, mark its
	     nerve, and add the associated source to the poll list
	     (only once).
	  
	  2. Poll all sources that have been added in to the poll list.
	*/
	/*
	  XXX see comments line 481	
start_over:
	*/

	/* By default, we will be using our timeval */
	ptv = &ttv;
	
	/* First, collect exhausted timers */
	ggCurTime(&start);
	evts = check_timers(inp, &to_poll, &start);
	
	/* Then, choose a timeout for the select call */
	if(evts) {
		DPRINT_REACTOR("- %i timers exhausted\n", evts);
		/* At least a timeout occured, so no need to wait. We
		 * will just poll the fd without timeout.
		 */
		ttv.tv_sec  = 0;
		ttv.tv_usec = 0;
		goto select_fds;
	}
	
	DPRINT_REACTOR("- no timer exhausted\n");
	/* There was no timeout, so we will wait forever or until the
	 * next scheduled timer.
	 */
	if((rea = INPUT_FIRST_TIMER(inp)) == NULL) {
		if(tv == NULL) {/* No scheduled timer and no given timeout, so wait forever */
			ptv = NULL;
			DPRINT_REACTOR("- no scheduled timer\n");
		}
		else {/* No scheduled timer, so use the given timeout */
			DPRINT_REACTOR("- using the given timeout\n");
			ttv = *tv;
		}
		goto select_fds;
	}
	/* Compute the delay until next scheduled timer. a > b holds,
	 * because otherwise the timer would have been triggered
	 * already. use it by default. */
	ggTimeSub(&rea->_.timer.at, &start, &ttv);
	if(tv == NULL) { /* No timeout given, so use our scheduled timer. */
		DPRINT_REACTOR("- using the next scheduled timeout\n");
		goto select_fds;
	}
	if(ggTimeGT(tv, &ttv)) { /* Scheduled timer will happen before... */
		DPRINT_REACTOR("- next scheduled timeout will happen before the given one\n");
		goto select_fds;
	}
	DPRINT_REACTOR("- using provided timeout\n");
	ttv = *tv; /* Use the provided timeout... */
	
select_fds:
	if (ptv) {
		DPRINT_REACTOR("- will select() for %i.%06is\n",ptv->tv_sec, ptv->tv_usec);
	} else {
		DPRINT_REACTOR("- will select() forever.\n");
	}
	/*

	Prepare the file descriptor vectors
	
	XXX: This can be cached in the input reactor
	
	*/
	maxfd = 0;
	FD_ZERO(&rfds);
	FD_ZERO(&wfds);
	INPUT_FOREACH_FD(inp, rea) {
		if((rea->_.fd.mode & GII_NERVE_FD_READ)) {
			if(rea->_.fd.fd >= maxfd) maxfd = rea->_.fd.fd+1;
			FD_SET(rea->_.fd.fd, &rfds);
			DPRINT_REACTOR("- select fd %i for reading\n", rea->_.fd.fd);
		}
		if((rea->_.fd.mode & GII_NERVE_FD_WRITE)) {
			if(rea->_.fd.fd >= maxfd) maxfd = rea->_.fd.fd+1;
			FD_SET(rea->_.fd.fd, &wfds);
			DPRINT_REACTOR("- select fd %i for writing\n", rea->_.fd.fd);
		}
	}
	
	if(maxfd == 0 && ptv == NULL) {
		/*
		  
		If there is no timeout, only fds can generate
		events. So if there is not fds to wait for, return -1
		immediatly, instead of sleeping forever.
		
		*/
		DPRINT_REACTOR("- hang condition. returning -1.\n");
		return -1;
	}
	
#if defined(__WIN32__) && !defined(__CYGWIN__)
	if (maxfd == 0) {
		ggUSleep(ptv->tv_sec * 1000000UL + ptv->tv_usec);
		ret = 0;
	}
	else
#endif
	ret = select(maxfd, &rfds, &wfds, NULL, ptv);
	
	if(ret < 0) {
		if (errno == EINTR) {
			DPRINT_REACTOR("! system call interrupted.\n");
			/*
			  
			XXX: Here we might want to startover. In this case, we must
			take the ellepased time into account and decrease the original timeout
			accordingly.
			
			goto start_over;
			*/
			
		} else {
			DPRINT_REACTOR("! error %i in select.\n", errno);
		}
	} else if(ret > 0) {
		DPRINT_REACTOR("- seen %i fd(s).\n", ret);
		/* At least one fd is ready. */
		INPUT_FOREACH_FD(inp, rea) {
			if(((rea->_.fd.mode & GII_NERVE_FD_READ) &&
			    FD_ISSET(rea->_.fd.fd, &rfds)) ||
			   ((rea->_.fd.mode & GII_NERVE_FD_WRITE) &&
			    FD_ISSET(rea->_.fd.fd, &wfds))) {
				DPRINT_REACTOR("- seen fd %i for source %p.\n",rea->_.fd.fd, (void*)rea->source);
				(*rea->nerve)++;
				if(SOURCE_REACTOR(rea->source)->seen++ == 0) {
					DPRINT_REACTOR("- adding source %p to poll list\n", (void*)rea->source);
					GG_LIST_INSERT_HEAD(&to_poll, rea->source, impl.le1);
				}
				evts++;
			}
		}
	} else {
		DPRINT_REACTOR("- no fd was seen. checking timers again.\n", ret);
		/* A timeout occured, so we collect the potential events. */
		if (INPUT_FIRST_TIMER(inp)) {
			ggCurTime(&now);
			evts += check_timers(inp, &to_poll, &now);
		}
	};
	
poll_candidates:

	DPRINT_REACTOR("- polling all candidates...\n");

	/* Notify sources that have been selected for poll.  They can
	 * return GII_OP_POLLAGAIN to be polled again with a very
	 * short delay, without redoing a whole loop. This can be
	 * useful in some cases for inputs that can not decide yet
	 * what event to generate (eg: detecting a very quick key
	 * sequence).
	 */
	GG_LIST_FOREACH(src, &to_poll, impl.le1) {
	skip:
		if (src->ops.poll == NULL) {
			DPRINT_REACTOR("! source %p has no poll function.\n",src);
			GG_LIST_REMOVE(src, impl.le1);
			SOURCE_FOREACH_REACTION(src, rea)
				*rea->nerve = 0;
			continue;
		}
		
		ret = src->ops.poll(src);
		SOURCE_REACTOR(src)->seen = 0;
		
		if(ret == GII_OP_POLLAGAIN) {
			DPRINT_REACTOR("- source wants to be polled again.\n",src);
			continue; /* Leave the source in the poll list */
		}
		/* We will not poll this source anymore */
		GG_LIST_REMOVE(src, impl.le1); /* 'next' is still valid */
		
		if(ret == GII_OP_DONE) {
			DPRINT_REACTOR("- normal return code.\n",src);
			/* Reset the nerves for this source */
			SOURCE_FOREACH_REACTION(src, rea)
				*rea->nerve = 0;
			continue;
		}
		
		if(ret == GII_OP_CLOSE) {
			DPRINT_REACTOR("- source want to be closed.\n",src);
			/* Tricky, 'next' will be invalidated when source is freed */
			src2 = src;
			src = src->impl.le1.le_next;
			
			/* Close the source */
			giiCloseSource(src2);

			if(!src)
				break;
			goto skip;
		}
		DPRINT_REACTOR("! invalid return code %i.\n",src);
		SOURCE_FOREACH_REACTION(src, rea)
			*rea->nerve = 0;
	}
	
	if(GG_LIST_FIRST(&to_poll)) {
		/* some sources asked to be polled again after a short delay */
#define POLL_AGAIN_DELAY 100000 /* 100 milliseconds */
		DPRINT_REACTOR("- some source asked for delayed poll, sleeping 0.1 seconds\n",src);
		ggUSleep(POLL_AGAIN_DELAY);
		goto poll_candidates;
	}

	DPRINT_REACTOR("- poll finished with %i event(s) triggered\n", evts);

	/*
	 
	If a timeout was given, adjust it
	
	XXX can we do better?
	
	*/
	if(tv) {
		ggCurTime(&now);
		/* tv =  max(0, (tv - (now - start)) */
		ggTimeSub(&now, &start, &ttv);
		if(ggTimeGT(&ttv, tv)) {
			tv->tv_sec = tv->tv_usec =  0;
		} else {
			ggTimeSub(tv, &ttv, tv);
		}
		
	}
	return evts;
}


static int
select_set_nerve(struct gii_source *src, union gii_nerve *nrv)
{
	switch(nrv->any.type) {
	case GII_NERVE_FD:
		return new_fd(src,nrv->any.nerve,nrv->fd.fd,nrv->fd.mode);
	case GII_NERVE_TIMER:
		return new_timer(src,nrv->any.nerve,nrv->timer.delay);
	case GII_NERVE_TICK:
		return new_tick(src,nrv->any.nerve,nrv->timer.delay);
	default:
		DPRINT_REACTOR("! nerve type %i not supported.\n", nrv->any.type);
	}
	return GGI_ENOFUNC;
}

static int
select_cut_nerve(struct gii_source *src, uint32_t *nrv)
{
	struct gii_reaction *rea;
	int found = 0;

	SOURCE_FOREACH_REACTION(src, rea)
		if(rea->nerve == nrv) {
			DPRINT_REACTOR("- deleting reaction %p\n", rea);
			found ++;
			GG_LIST_REMOVE(rea,le0);
			GG_LIST_REMOVE(rea,le1); /* 'next' is still valid */
			DEL_REACTION(rea);
			if (rea->type == GII_NERVE_TICK) {
				struct gii_input *inp = rea->source->impl.inp;
				if (INPUT_HAS_TICK(inp))
					continue;
				/* we are the last to use ticks, so we need
				 * to remove the fake tick reaction
				 */
				GG_LIST_REMOVE(INPUT_REACTOR(inp)->tick, le0);
			}
		}
	if (found)
		return GGI_OK;
	return GGI_ENOTFOUND;
}

static int
select_bind(struct gii_source * src)
{
	struct gii_reactor_src * r;

	if ((r = calloc(1, sizeof(*r))) == NULL)
		return GGI_ENOMEM;
	
	src->impl.reactor = (void*)r;
	
	return GGI_OK;
}

static void
select_unbind(struct gii_source * src)
{
	struct gii_reaction * rea;
	
	while((rea = SOURCE_FIRST_REACTION(src)))
		select_cut_nerve(src, rea->nerve);
	
	free(src->impl.reactor);
	src->impl.reactor = NULL;
}

static int
select_init(struct gii_input * inp)
{
	struct gii_reactor_inp * r;

	if ((r = calloc(1, sizeof(*r))) == NULL)
		return GGI_ENOMEM;

	if((r->tick = new_reaction(NULL, -1, NULL)) == NULL) {
		free(r);
		return GGI_ENOMEM;
	}

	inp->reactor = (void*)r;

	return GGI_OK;
}

static void
select_exit(struct gii_input * inp)
{
	DEL_REACTION(INPUT_REACTOR(inp)->tick);
	free(inp->reactor);
	inp->reactor = NULL;
}

static int
select_transfer(struct gii_source *src, struct gii_input *inp) {
	struct gii_reaction * rea;
	
	SOURCE_FOREACH_REACTION(src, rea) {
		GG_LIST_REMOVE(rea, le0);
		switch(rea->type) {
		case GII_NERVE_FD:
			INPUT_ADD_FD(inp, rea);
			break;
		case GII_NERVE_TIMER:
			add_timer(inp, rea);
			break;
		case GII_NERVE_TICK:
			if (INPUT_HAS_TICK(inp) == NULL) {
				/* We are the first to use ticks, so we need to schedule the
				 * fake tick reaction (100ms). Freq is not used right now.
				 */
				struct gii_reaction *tick;
				struct timeval tv;
				tick = INPUT_REACTOR(inp)->tick;
				tv.tv_sec  = 0;
				tv.tv_usec = 100000;
				ggCurTime(&tick->_.timer.at);
				ggTimeAdd(&tick->_.timer.at, &tv, &tick->_.timer.at);
				add_timer(inp, tick);
			}

			INPUT_ADD_TICK(inp, rea);
			break;
		default:
			DPRINT_REACTOR("! unknown reaction type %i\n", rea->type);
		}
	}
	
	return GGI_OK;
}

static struct gii_reactor select_reactor = {
	&select_init,
	&select_exit,
	&select_bind,
	&select_unbind,
	&select_poll,
	&select_cut_nerve,
	&select_set_nerve,
	&select_transfer
};

struct gii_reactor *
_giiInitReactor(void)
{
	DPRINT_REACTOR("_giiReactorInit()\n");
	
	DPRINT_REACTOR("- using reactor \"select\" at %p\n", &select_reactor);
	
	GG_LIST_INIT(&reaction_pool);
	reaction_pool_size = 0;
	
	return &select_reactor;
}

void
_giiExitReactor(struct gii_reactor * reactor)
{
	struct gii_reaction * rea;
	
	DPRINT_REACTOR("_giiReactorExit(reactor=%p)\n");
	
	while((rea = GG_LIST_FIRST(&reaction_pool))) {
		GG_LIST_REMOVE(rea, le0);
		free(rea);
	}
}
