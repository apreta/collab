/* $Id: gii.c,v 1.82 2009/12/17 19:33:01 pekberg Exp $
******************************************************************************

   new LibGII implementation
   
   Copyright (C) 1997 Jason McMullan	[jmcc@ggi-project.org]
   Copyright (C) 1998 Andreas Beck	[becka@ggi-project.org]
   Copyright (C) 1998 Jim Ursetto	[jim.ursetto@ggi-project.org]
   Copyright (C) 1998 Andrew Apted	[andrew.apted@ggi-project.org]
   Copyright (C) 1999 Marcus Sundberg	[marcus@ggi-project.org]
   Copyright (C) 2005 Eric Faurot	[eric.faurot@gmail.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"

#include "gii-internal.h"

#include <ggi/gii-module.h>
#include <ggi/gii.h>

#include <ggi/gg.h>
#include <ggi/internal/gg_replace.h>
#include <ggi/gg-api.h>
#include <ggi/internal/gii_debug.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>

#ifdef HAVE_STRINGS_H
#include <strings.h>
#endif

#ifdef HAVE_SIGNAL_H
#include <signal.h>
#endif

/*
******************************************************************************
**  Static variables
******************************************************************************
*/

static struct gii_reactor * reactor = NULL;

static uint32_t _origins          = GII_MAXDEVICES; /* next available */
static int      _gii_threadsafe   = 0; /* XXX always 0 for now. */


static ggfunc_api_op_init gii_init;
static ggfunc_api_op_exit gii_exit;
static ggfunc_api_op_attach gii_attach;
static ggfunc_api_op_detach gii_detach;
static ggfunc_api_op_getenv gii_getenv;
static ggfunc_api_op_plug gii_plug;
static ggfunc_api_op_unplug gii_unplug;

static struct gg_api_ops gii_ops = {
	gii_init,
	gii_exit,
	gii_attach,
	gii_detach,
	gii_getenv,
	gii_plug,
	gii_unplug
};

static struct gg_api gii = GG_API_INIT("gii", 3, 0, &gii_ops);

struct gg_api * libgii = &gii;

#define GII_INPUT(stem) STEM_API_DATA(stem,(&gii), struct gii_input*)
#define GII_PRIV(stem) STEM_API_PRIV(stem,(&gii))

#ifndef HAVE_CONFFILE
extern const char const *_giibuiltinconf[];
#endif

/*
******************************************************************************
**  Init/Exit
******************************************************************************
*/

int
giiInit(void)
{
	return ggInitAPI(libgii);
}

int
giiExit(void)
{
	return ggExitAPI(libgii);
}

int
giiAttach(struct gg_stem *s)
{
	return ggAttach(libgii, s);
}

int
giiDetach(struct gg_stem *s)
{
	return ggDetach(libgii, s);
}


/*
******************************************************************************
**  Internal helpers
******************************************************************************
*/

/* Make event timestamps strictly monotonic.
 */
static inline void
_giiEvTimestamp(gii_event *ev)
{
	static struct timeval last_event = { 0, 0 };
	struct timeval tv;
	
	ggCurTime(&tv);
	ev->any.sec = tv.tv_sec;
	ev->any.usec = tv.tv_usec;
	if((ev->any.sec > last_event.tv_sec)
	   || ((ev->any.sec == last_event.tv_sec)
	       && (ev->any.usec > last_event.tv_usec))) {
		last_event.tv_sec = ev->any.sec;
		last_event.tv_usec = ev->any.usec;
	}
	else {
		if(++last_event.tv_usec >= 1000000) {
			last_event.tv_usec -= 1000000;
			++last_event.tv_sec;
		}
		ev->any.sec = last_event.tv_sec;
		ev->any.usec = last_event.tv_usec;
	}
}

/*
******************************************************************************
**  EvQueue mechanisms
******************************************************************************
*/

/* Add event to a queue
 */
static inline int
_giiAddEvent(struct gii_ev_queue *qp, gii_event *ev)
{
	if (qp->head < qp->tail) {
		if ((unsigned)(qp->tail - qp->head - 1) < ev->size) {
			DPRINT_EVENTS("! event queue overflow\n");
			return GGI_EEVOVERFLOW;
		}
	} else if (qp->head > qp->tail) {
		if ((qp->head + ev->size) > GII_Q_THRESHOLD) {
			/* Event crosses threshold, thus we need space
			   at start of buffer to put head (tail may be
			   at 0, but head == tail means an empty buffer).
			*/
			if (qp->tail == 0) {
				DPRINT_EVENTS("! event queue overflow (threshold)\n");
				return GGI_EEVOVERFLOW;
			}
		}
	}
	
	/* Add the event, and mark that we've seen it.. */
	memcpy(qp->buf + qp->head, ev, ev->size);
	
	qp->count++;
	qp->head += ev->size;
	
	if (qp->head > GII_Q_THRESHOLD)
		qp->head = 0;
	
	return GGI_OK;
}

/* Peek at event at tail of queue
 * WARNING: The returned pointer may be misaligned.
 *   Use it in conjunction with memcpy(3) to ensure
 *   aligned memory access.
 *   Some platforms (i.e. NetBSD/sparc64) rely on this.
 */
static inline gii_event *
_giiPeekEvent(struct gii_ev_queue *qp)
{
	return (gii_event*)(qp->buf + qp->tail);
}

/* Delete event at tail of queue
 */
static inline void
_giiDeleteEvent(struct gii_ev_queue *qp)
{
	qp->count--;
	qp->tail += qp->buf[qp->tail];
	
	if (qp->tail > GII_Q_THRESHOLD) {
		qp->tail = 0;
	}
}	

/* Get event at tail of queue
 */
static inline void
_giiGetEvent(struct gii_ev_queue *qp, gii_event *ev)
{
	uint8_t size = qp->buf[qp->tail];
	
	/* Pull event out of queue.. */
	memcpy(ev, qp->buf + qp->tail, size);
	
	qp->count--;
	qp->tail += size;
	
	if (qp->tail > GII_Q_THRESHOLD) {
		qp->tail = 0;
	}
}

/* Put an event on an input.
 */
static int
_giiEvQueueAdd(struct gii_input *inp, gii_event *ev)
{
	struct gii_ev_queue *qp;
	struct gii_source *src;
	int ret;
	
	DPRINT_EVENTS("_giiEvQueueAdd(%p, %p)\n", inp, ev);
	
	/* Check if type is in range */
	if (ev->any.type >= evLast) {
		DPRINT_EVENTS("! bad event type: 0x%x\n", ev->any.type );
		return GGI_EARGINVAL;
	}
	
	/* Tell all the filters */
	input_FOREACH_SOURCE(inp, src)
		if (src->ops.filter)
			/*  the source is not allowed to close the input
			 *  during the callback, but that should never append
			 *  anyway.
			 */
			if (src->ops.filter(src, ev)) {
				/* Event eaten by filter */
				DPRINT_EVENTS("- event blocked by filter %p",
					      src);
				return GGI_OK;
			}
	
	if (_gii_threadsafe)
		ggLock(inp->queue.mutex);
	
	if (inp->queue.queues[ev->any.type])
		qp = inp->queue.queues[ev->any.type];
	else {
		if ((qp = calloc(1, sizeof(*qp))) == NULL) {
			DPRINT_EVENTS("! cannot allocate event queue.\n");
			if (_gii_threadsafe)
				ggUnlock(inp->queue.mutex);
			return GGI_ENOMEM;
		}
		inp->queue.queues[ev->any.type] = qp;
	}
	
	DPRINT_EVENTS("- adding event type %d, size %d at pos %d\n",
		      ev->any.type, ev->size, qp->count);
	
	ret = _giiAddEvent(qp, ev);
	if (ret != 0) {
		if (_gii_threadsafe)
			ggUnlock(inp->queue.mutex);
		return ret;
	}
	
	inp->queue.seen |= (1 << ev->any.type);
	
	if (_gii_threadsafe) {
		/* Notify any other threads which may be blocking on
		   this input */
		/* _giiAsyncNotify(inp); */
		ggUnlock(inp->queue.mutex);
	}
	
	return GGI_OK;
}

/* Release an event from the queue set. Select the earliest one.
 * Returns event-size. 0 = fail.
 */
static int
_giiEvQueueRelease(struct gii_input *inp,
		   gii_event *ev,
		   gii_event_mask mask)
{
	struct gii_ev_queue *qp = NULL;
	gii_event_mask evm;
	struct timeval t_min;
	int queue;
	
	DPRINT_EVENTS("_giiEvQueueRelease(%p, %p, 0x%x)\n", inp, ev, mask);
	
	if (_gii_threadsafe)
		ggLock(inp->queue.mutex);
	
	evm = mask & inp->queue.seen;
	
	/* Got nothing.. */
	if (evm == 0) {
		if (_gii_threadsafe)
			ggUnlock(inp->queue.mutex);
		return 0;
	}
	
	/* Max timestamp.. */
	t_min.tv_sec  = 0x7FFFFFFF;
	t_min.tv_usec = 0x7FFFFFFF;
	
	/* Get the specified event out of the queue.  If the user asks
	 * for more than one event type, return the one that has been
	 * waiting the longest.
	 */
	for (queue=0; queue < evLast; queue++) {
		struct gii_ev_queue *qp_tmp;
		
		DPRINT2_EVENTS("- queue = %d, queue->queues = %p, "
			       "queue->queues[queue] = %p\n",
			       queue, inp->queue.queues,
			       inp->queue.queues[queue]);
		qp_tmp = inp->queue.queues[queue];
		if (qp_tmp && qp_tmp->count && (evm & (1 << queue))) {
			uint8_t *e_tmp = (uint8_t *)_giiPeekEvent(qp_tmp);
			gii_event t_tmp;
			
			/* Assure aligned memory access. Some platforms
			 * (i.e. NetBSD/sparc64) rely on this.
			 */
			memcpy(&t_tmp, e_tmp, sizeof(gii_any_event));
			
			if (t_tmp.any.sec < t_min.tv_sec || 
			    (t_tmp.any.sec == t_min.tv_sec &&
                             t_tmp.any.usec < t_min.tv_usec)) {
				DPRINT2_EVENTS("- plausible found.\n");
				qp = qp_tmp;
				t_min.tv_sec = t_tmp.any.sec;
				t_min.tv_usec = t_tmp.any.usec;
			}
		}
	}
	
	/* Shouldn't happen.. */
	LIB_ASSERT(qp != NULL, "! Arrgghh!! Nothing plausible");
	if (qp == NULL) {
		if (_gii_threadsafe)
			ggUnlock(inp->queue.mutex);
		return 0;
	}
	
	_giiGetEvent(qp, ev);
	
	if (qp->count == 0)
		inp->queue.seen &= ~(1 << ev->any.type);
	
	if (_gii_threadsafe)
		ggUnlock(inp->queue.mutex);
	
	DPRINT_EVENTS("- retrieved event type %d, size %d.\n", 
		      ev->any.type, ev->size);
	
	return ev->size;
}

/*
******************************************************************************
**  Internal source operations
******************************************************************************
*/

/* Default devinfo sender
 * Most input targets implement the devinfo sender in this way.
 * It is static, and will be set by default on the source,
 * If the source wants to change it but also needs the default
 * as fall back, it must keep a reference to it.
 */
static int
_giiSendDevInfo(struct gii_source *src, struct gii_device *dev)
{
	gii_event ev;
	struct gii_cmddata_devinfo *dinfo;
	
#define EVSIZE sizeof(gii_cmd_nodata_event)+sizeof(struct gii_cmddata_devinfo)
	giiEventBlank(&ev, EVSIZE);
	
	ev.any.size	= EVSIZE;
	ev.any.type	= evCommand;
	ev.any.origin	= dev->origin;
	ev.cmd.code	= GII_CMDCODE_DEVICE_INFO;
	
	dinfo = (struct gii_cmddata_devinfo *) ev.cmd.data;
	*dinfo = *dev->devinfo;
#undef EVSIZE	
	return _giiEvQueueAdd(src->impl.inp, &ev);
}

/* Default valuator sender
 * Most input targets implement the valuator sender in this way.
 */
static int
_giiStdSendValEvent(struct gii_source *src, struct gii_device *dev,
		    struct gii_cmddata_valinfo *vi,
		    uint32_t val,
		    uint32_t maxval)
{
	struct gii_cmddata_valinfo *VI = NULL;
	gii_event ev;
	size_t size;
	
	DPRINT_EVENTS("_giiStdSendValEvent(%p, %p, %i, %i)\n",
		      src, VI, val, maxval);
	
	if (val >= maxval) return GGI_EARGINVAL;
	
	size = sizeof(gii_cmd_nodata_event)+sizeof(struct gii_cmddata_valinfo);
	
	giiEventBlank(&ev, size);
	
	ev.any.size	= size;
	ev.any.type	= evCommand;
	ev.any.origin	= dev->origin;
	ev.cmd.code	= GII_CMDCODE_VALUATOR_INFO;
	
	VI = (struct gii_cmddata_valinfo *) ev.cmd.data;
	
	*VI = vi[val];
	
	return _giiEvQueueAdd(src->impl.inp, &ev);
}

static int
_giiStdSendValInfo(struct gii_source *src, struct gii_device *dev,
		   struct gii_cmddata_valinfo *vi,
		   uint32_t val,
		   uint32_t maxval)
{
	if (val == GII_VAL_QUERY_ALL) {
		uint32_t i;
		for (i = 0; i < maxval; ++i)
			_giiStdSendValEvent(src, dev, vi, i, maxval);
		return 0;
	}

	return _giiStdSendValEvent(src, dev, vi, val, maxval);
}

static int
_giiEventIntercept(struct gii_source *src, gii_event *ev)
{
	struct gii_device *dev;
	struct gii_cmddata_valinfo *vi;

	if (ev->any.type != evCommand)
		return GGI_EEVUNKNOWN;

	switch (ev->cmd.code) {
	case GII_CMDCODE_DEVICE_INFO:
		if (ev->cmd.target & GII_DEVICEMASK) {
			source_FOREACH_DEVICE(src, dev)
				if (dev->origin == ev->cmd.target)
					break;
			if (!dev)
				return 0;
			return _giiSendDevInfo(src, dev);
		}
		source_FOREACH_DEVICE(src, dev)
			_giiSendDevInfo(src, dev);
		return 0;

	case GII_CMDCODE_VALUATOR_INFO:
		vi = (struct gii_cmddata_valinfo *)ev->cmd.data;
		if (ev->cmd.target & GII_DEVICEMASK) {
			source_FOREACH_DEVICE(src, dev)
				if (dev->origin == ev->cmd.target)
					break;
			if (!dev)
				return 0;
			return _giiStdSendValInfo(src, dev, dev->valinfo,
				vi->number, dev->devinfo->num_valuators);
		}
		source_FOREACH_DEVICE(src, dev)
			_giiStdSendValInfo(src, dev, dev->valinfo,
				vi->number, dev->devinfo->num_valuators);
		return 0;
	}

	return GGI_EEVUNKNOWN;
}

/*
******************************************************************************
**  Internal input operations
******************************************************************************
*/

/*
 *  XXX possible improvement
 *
 *  It is not sure that the canmask is really useful.  We might want
 *  to drop this feature. In this adding and removing devices would be
 *  more direct.
 */
static inline void _updateMask(struct gii_input *inp) {
	struct gii_source *src;
	inp->canmask = emNothing;
	input_FOREACH_SOURCE(inp, src)
		inp->canmask |= src->devmask;
}


/* Find a device by its origin
 */
static struct gii_device *
_giiFindDevice(struct gii_input *inp, uint32_t origin)
{
	struct gii_source *src;
	struct gii_device *dev;
	
	input_FOREACH_SOURCE(inp, src) {
		if (!GII_IS_SAME_SOURCE(src->origin,origin))
			continue;
		source_FOREACH_DEVICE(src, dev)
			if (dev->origin == origin)
				return dev;
	}
	
	return NULL;
}

/*
******************************************************************************
**  Module API
******************************************************************************
*/

/* Set up an event
 */
void
giiEventBlank(gii_event *ev, size_t size)
{
	memset(ev, 0, size);
	
	/*
	ev->any.error  = 0;
	ev->any.origin = GII_EV_ORIGIN_NONE;
	ev->any.target = GII_EV_TARGET_ALL;
	*/
	
	_giiEvTimestamp(ev);
}

/* Add a new device to an input source and return its origin, or 0 on error.
 */
uint32_t
giiAddDevice(struct gii_source *src,
	     struct gii_cmddata_devinfo *devinfo,
	     struct gii_cmddata_valinfo *valinfo)
{
	struct gii_device *dev;
		
	DPRINT("giiAddDevice(%p, %p, %p)\n",src, devinfo, valinfo);
	
	if(src->flags & GII_SRCFLAG_CLOSING) {
		DPRINT_CORE("- source is being closed.");
		return 0;
	}
	
	if((src->impl.next_origin & GII_DEVICEMASK) != GII_DEVICEMASK ) {
		if((dev = calloc(1, sizeof(*dev))) == NULL) {
			DPRINT_CORE("! mem error.");
			return 0;
		}
		dev->origin = ++src->impl.next_origin;
	}
	else if ((dev = source_FIRST_ORIGIN(src)))
		source_REM_ORIGIN(src, dev);
	else {
		DPRINT_CORE("! origin overflowed.");
		return 0;
	}

	dev->devinfo = devinfo;
	dev->valinfo = valinfo;

	source_ADD_DEVICE(src, dev);
	
	src->devmask |= devinfo->can_generate;
	src->impl.inp->canmask |= src->devmask;
	
	/*  If the device was added during the source initialization,
	 *  we do not want to send the device info right now in case
	 *  initialisation fails later. Instead, we wait for
	 *  initialization completion to send all device at once.
	 */
	if(!(src->flags & GII_SRCFLAG_OPENING))
		_giiSendDevInfo(src, dev);
	
	
	return dev->origin;
}

/*
 * Remove a device given by its origin.
 * 
 * return GGI_OK, or GGI_ENOTFOUND if no device match the origin.
 */
int
giiDelDevice(struct gii_source *src, uint32_t origin)
{
	struct gii_device *dev, *tmp;
	gii_cmd_nodata_event ev;
	
	gii_event_mask mask;
	
	DPRINT("giiDelDevice(%p, 0x%x)\n", src, origin);
	
	tmp = NULL;
	mask = emNothing;
	
	source_FOREACH_DEVICE(src, dev)
		if (dev->origin == origin)
			tmp = dev;
		else
			mask |= dev->devinfo->can_generate;
	
	if (tmp == NULL) {
		return GGI_ENOTFOUND;
	}
	
	/*  Report if removed outside of the source initialization. */
	if(!(src->flags & GII_SRCFLAG_OPENING)) {
		giiEventBlank((gii_event*)&ev, sizeof(ev));
		ev.size   = sizeof(ev);
		ev.origin = tmp->origin;
		ev.type   = evCommand;
		ev.code   = GII_CMDCODE_DEVICE_CLOSE;
		giiPostEvent(src, (gii_event*)&ev);
	}
	
	source_REM_DEVICE(src, tmp);
	tmp->devinfo = NULL;
	tmp->valinfo = NULL;
	source_FOREACH_ORIGIN(src, dev)
		if (!source_NEXT_ORIGIN(src, dev))
			break;
	if (dev)
		source_IAFTER_ORIGIN(src, dev, tmp);
	else
		source_ADD_ORIGIN(src, tmp);
	
	if(src->devmask != mask) {
		src->devmask = mask;
		_updateMask(src->impl.inp);
	}
	
	return GGI_OK;
	
}

int
giiPostEvent(struct gii_source *src, gii_event *ev)
{
	DPRINT_EVENTS("giiPostEvent(%p, %p)\n", src, ev);
	
	/* block event if not in the current event mask */
	if(!(src->impl.inp->reqmask & (1 << ev->any.type))) {
		DPRINT_EVENTS("! blocking event type=%i origin=0x%08x\n",
			     ev->any.type, ev->any.origin);
		return GGI_EARGINVAL;
	}
	
	return _giiEvQueueAdd(src->impl.inp, ev);
}

int
giiSetNerve(struct gii_source *src, union gii_nerve *nrv)
{
	DPRINT_MISC("giiSetNerve(%p, %p)\n",src,nrv);
	
	if(src->flags & GII_SRCFLAG_CLOSING) {
		DPRINT_CORE("! source is being closed.");
		return GGI_EBUSY;
	}
	
	if(nrv->any.type >= GII_NERVE_LAST) {
		DPRINT_CORE("! invalid nerve type %i.", nrv->any.type);
		return GGI_EARGINVAL;
	}
	
	return reactor->set_nerve(src, nrv);
}

int
giiCutNerve(struct gii_source *src, uint32_t *nrv)
{
	
	DPRINT_MISC("giiCutNerve(%p, %p)\n",src,nrv);
	
	return reactor->cut_nerve(src, nrv);
}

/*
******************************************************************************
** User API
******************************************************************************
*/

int
giiOpen(gii_input stem, const char *input, void *argptr)
{
	int count;
	struct gg_target_iter   target;
	
	DPRINT_CORE("giiOpen(%p, \"%s\", %p)\n", stem, input, argptr);
	
	if(input == NULL)
		if((input = getenv("GII_INPUT")) == NULL)
			input = "input-default";
	
	count = 0;
	target.input  = input;
	target.config = libgii->config;
	ggConfigIterTarget(&target);
	GG_ITER_FOREACH(&target) {
		DPRINT("- adding \"%s\", \"%s\", %p\n",
		       target.target, target.options, argptr);
		if (ggPlugModule(libgii,
				 stem,
				 target.target,
				 target.options,
				 argptr))
			count += 1;
	}
	GG_ITER_DONE(&target);
	
	return count;
}

int
giiClose(gii_input stem, uint32_t target)
{
	struct gii_source *src;
	struct gii_input  *inp;
	
	DPRINT_CORE("giiClose(%p, 0x%x)\n", stem, target);
	
	inp = GII_INPUT(stem);
	
	if(target == GII_EV_TARGET_ALL) {
		while((src = input_FIRST_SOURCE(inp))) {
			giiCloseSource(src);
		}
		inp->canmask = emNothing;
		return GGI_OK;
	}
	
	target &= GII_SOURCEMASK;
	
	input_FOREACH_SOURCE(inp, src)
		if(GII_IS_SAME_SOURCE(src->origin, target)) {
			giiCloseSource(src);
			_updateMask(inp);
			return GGI_OK;
		}
	
	return GGI_ENOTFOUND;
}

int
giiTransfer(gii_input s1, gii_input s2, uint32_t target)
{
	struct gii_input  *inp1, *inp2;
	struct gii_source *src;
	struct gii_device *dev;
	int res;
	
	DPRINT_MISC("giiTransfer(%p, %p, 0x%x)\n", s1, s2, target);
	
	inp1 = GII_INPUT(s1);
	inp2 = GII_INPUT(s2);
	
	input_FOREACH_SOURCE(inp1, src)
		if(GII_IS_SAME_SOURCE(src->origin,target))
			break;
	
	if (src == NULL)
		return GGI_ENOTFOUND;
	
	if ((res = reactor->transfer(src, inp2)) != GGI_OK) {
		DPRINT_CORE("! reactor couldn't transfer source\n");
		return res;
	}
	
	input_REM_SOURCE(inp1, src);
	input_ADD_SOURCE(inp2, src);
	
	/* update the inputs mask */
	inp2->canmask |= src->devmask;
	if (src->ops.setmask)
		src->ops.setmask(src, inp1->reqmask);
	src->reqmask = inp1->reqmask;
	
	_updateMask(inp1);

	src->impl.inp = inp2;

	source_FOREACH_DEVICE(src, dev)
		_giiSendDevInfo(src, dev);

	return GGI_OK;
}

int
giiEventsQueued(gii_input stem, gii_event_mask mask)
{
	int count = 0;
	int i;
	struct gii_input *inp;
	
	DPRINT_EVENTS("giiEventsQueued(%p, 0x%x)\n", stem, mask);
	
	inp = GII_INPUT(stem);
	
	if (_gii_threadsafe)
		ggLock(inp->queue.mutex);
	
	mask &= inp->queue.seen;
	for (i = 0; mask; i++, mask >>= 1)
		if ((mask & 1))
			count += inp->queue.queues[i]->count;
	
	if (_gii_threadsafe)
		ggUnlock(inp->queue.mutex);
	
	return count;
}

gii_event_mask
giiEventPoll(gii_input stem, gii_event_mask mask, struct timeval *timeout)
{
	struct gii_input  *inp;
	gii_event_mask tmpmask;
	
	DPRINT_EVENTS("giiEventPoll(%p, 0x%x, %p)\n", stem, mask, timeout);
	
	inp = GII_INPUT(stem);
	
	if (mask & inp->queue.seen) {
		struct timeval zero;
		zero.tv_sec = zero.tv_usec = 0;
		if (reactor->poll(inp, &zero) < 0)
			return 0;
		return mask & inp->queue.seen;
	}

	for (;;) {
		if (reactor->poll(inp, timeout) < 0)
			return 0;
		if ((tmpmask = (mask & inp->queue.seen)))
			break;
		/* Nothing so far, should we wait longer? */
		if (!timeout)
			continue;
		if (!timeout->tv_usec && !timeout->tv_sec)
			break;
	}
	
	return tmpmask;
}

int
giiEventRead(gii_input stem, gii_event *ev, gii_event_mask mask)
{
	struct gii_input  *inp;
	
	DPRINT_EVENTS("giiEventRead(%p, %p, 0x%x)\n", stem, ev, mask);
	
	inp = GII_INPUT(stem);
	
	if (!(mask & inp->queue.seen)) {
		/*  block until an event comes in.  */
		if (!giiEventPoll(stem, mask, NULL))
			return 0;
	}
	
	return _giiEvQueueRelease(inp, ev, mask);
}

int
giiSetEventMask(gii_input stem, gii_event_mask evm)
{
	struct gii_source *src;
	int i;
	struct gii_input  *inp;
	
	DPRINT_EVENTS("giiSetEventMask(%p, 0x%x)\n", stem, evm);
	
	inp = GII_INPUT(stem);
	
	inp->reqmask = evm;
	
	input_FOREACH_SOURCE(inp, src) {
		if(src->ops.setmask)
			/*  This might result in devices to be
			 *  closed or brought back, as well as
			 *  lowlevel event (un)registrations.
			 */
			src->ops.setmask(src, evm);
		/* change after, so that the sources can know the old one */
		src->reqmask = evm;
	}
	
	/* Flush any events whose bit is 0 in the new mask.
	 * Is that really desireable ? Andy
	 */
	if (_gii_threadsafe)
		ggLock(inp->queue.mutex);
	
	for (i=0; i < evLast; i++)
		if (((evm & (1 << i)) == 0) && inp->queue.queues[i]) {
			inp->queue.queues[i]->head  = 0;
			inp->queue.queues[i]->tail  = 0;
			inp->queue.queues[i]->count = 0;
			inp->queue.seen&=~(1 << i);	/* Not seen ! */
		}
	
	if (_gii_threadsafe)
		ggUnlock(inp->queue.mutex);
	
	return GGI_OK;
}

gii_event_mask
giiGetEventMask(gii_input stem)
{
	struct gii_input *inp;
	
	DPRINT_EVENTS("giiGetEventMask(%p)\n", stem);
	
	inp = GII_INPUT(stem);
	
	return inp->reqmask;
}

int
giiEventSend(gii_input stem, gii_event *ev)
{
	struct gii_source *src;
	struct gii_input *inp;
	int intercept = 0;

	DPRINT_EVENTS("giiEventSend(%p, %p)\n", stem, ev);
	
	inp = GII_INPUT(stem);
	
	_giiEvTimestamp(ev);
	
	/* XXX FIXME! We should allow this to be or-ed in */
	ev->any.origin = GII_EV_ORIGIN_SENDEVENT;
	
	if (ev->any.target == GII_EV_TARGET_QUEUE)
		return _giiEvQueueAdd(inp, ev);
	
	/* XXX Intercept protocol events (handled by libgii) like devinfo
	 * requests etc. only other events are passed to the sources.
	 */
	if (ev->any.type == evCommand) {
		switch (ev->cmd.code) {
		case GII_CMDCODE_DEVICE_INFO:
		case GII_CMDCODE_VALUATOR_INFO:
			intercept = 1;
		}
	}

	/* XXX Should a source have a chance to override the core handler? */
	if (intercept) {
		if(ev->any.target == GII_EV_TARGET_ALL) {
			input_FOREACH_SOURCE(inp, src)
				_giiEventIntercept(src, ev);
			return 0;
		}

		input_FOREACH_SOURCE(inp, src)
			if (GII_IS_SAME_SOURCE(src->origin, ev->any.target))
				return _giiEventIntercept(src, ev);

		return GGI_EEVNOTARGET;
	}

	if(ev->any.target == GII_EV_TARGET_ALL) {
		input_FOREACH_SOURCE(inp, src)
			if(src->ops.event)
				src->ops.event(src, ev);
		return 0;
	}

	input_FOREACH_SOURCE(inp, src)
		if(GII_IS_SAME_SOURCE(src->origin,ev->any.target))
			if(src->ops.event)
				return src->ops.event(src, ev);
	
	return GGI_EEVNOTARGET;
}

int
giiKbhit(gii_input inp)
{
	struct timeval t = { 0, 0 };

	return (giiEventPoll(inp, emKeyPress | emKeyRepeat, &t)
		!= emZero);
}

int
giiGetc(gii_input inp)
{
	gii_event ev;

	/* Block until we get a key. */
	giiEventRead(inp, &ev, emKeyPress | emKeyRepeat);

	return ev.key.sym;
}

static int _source_next(struct gii_source_iter *iter)
{
	struct gii_source *src;

	src = (struct gii_source *)iter->_state;

	while (src) {
		iter->origin = src->origin;
		iter->src = iter->_state;
		iter->_state = input_NEXT_SOURCE(iter->stem, src);
		return ITER_YIELD;
	}
	return ITER_DONE;
}

int giiIterSources(struct gii_source_iter *iter)
{
	struct gii_input *inp;

	DPRINT("giiIterSource(%p)\n", iter);

	inp = GII_INPUT(iter->stem);
	GG_ITER_PREPARE(iter, _source_next, NULL);
	iter->src = NULL; 
	iter->_state = input_FIRST_SOURCE(inp);
	return GGI_OK;
}

static int _device_next(struct gii_device_iter *iter)
{
	struct gii_device *dev;

	dev = (struct gii_device *)iter->_state;

	while (dev) {
		iter->origin = dev->origin;
		iter->devinfo = *dev->devinfo;
		iter->_state = source_NEXT_DEVICE(iter->stem, dev);
		return ITER_YIELD;
	}
	return ITER_DONE;
}

int giiIterDevices(struct gii_device_iter *iter)
{
	struct gii_device *dev = NULL;
	struct gii_source *src = NULL;
	struct gii_input *inp;

	DPRINT("giiIterDevice(%p)\n", iter);

	inp = GII_INPUT(iter->stem);
	if (iter->src == NULL) {
		input_FOREACH_SOURCE(inp, src) {
			if (!GII_IS_SAME_SOURCE(src->origin, iter->source_origin))
				continue;
			dev = source_FIRST_DEVICE(src);
			break;
		}
	} else {
		src = iter->src;
		dev = source_FIRST_DEVICE(src);
	}

	GG_ITER_PREPARE(iter, _device_next, NULL);
	iter->_state = dev;
	if (dev) {
		iter->devinfo = *dev->devinfo;
	} else {
		memset(&iter->devinfo, 0,
			sizeof(struct gii_cmddata_devinfo));
	}
	return GGI_OK;
}

int
giiQueryDeviceInfo(gii_input stem, uint32_t origin,
		   struct gii_cmddata_devinfo *info)
{
	struct gii_device *dev;
	struct gii_input  *inp;
	
	DPRINT_EVENTS("giiQueryDeviceInfo(%p, 0x%x, %p)\n",stem,origin,info);
	
	inp = GII_INPUT(stem);
	
	dev = _giiFindDevice(inp, origin);
	if (dev) {
		*info = *dev->devinfo;
		return 0;
	}
	return GGI_ENOMATCH;
}

int
giiQueryValInfo(gii_input stem,
		uint32_t origin,
		uint32_t valnumber,
		struct gii_cmddata_valinfo *info)
{
	struct gii_device *dev;
	struct gii_input  *inp;
	
	DPRINT_EVENTS("giiQueryValInfo(%p, 0x%x, %i, %p)\n",
		      stem,origin,valnumber,info);
	
	inp = GII_INPUT(stem);
	
	dev = _giiFindDevice(inp,origin);
	if (dev) {
		if (valnumber >= dev->devinfo->num_valuators)
			return GGI_ENOSPACE;
		*info = dev->valinfo[valnumber];
		return 0;
	}
	return GGI_ENOMATCH;
}

static void
_giiShowDevice(struct gii_device *dev) {
	struct gii_cmddata_devinfo *DI;
	
	DI = dev->devinfo;
	
	printf("        [device] origin=0x%08"PRIx32"\n"
	       "                 name=\"%s\"\n"
	       "                 vendor=0x%08"PRIx32" product=0x%08"PRIx32
					" generate=0x%08x\n"
	       "                 registers=%"PRId32" buttons=%"PRId32
					" valuators=%"PRId32"\n",
	       dev->origin,
	       DI->devname,
	       DI->vendor_id, DI->product_id, DI->can_generate,
	       DI->num_registers, DI->num_buttons, DI->num_valuators);
}

static void
_giiShowSource(struct gii_source *src) {
	struct gii_device *dev;
	
	printf("    [source] origin=0x%08"PRIx32"\n"
	       "             module=%s v%"PRId32".%"PRId32"\n"
	       "             reqmask=0x%08x devmask=0x%08x flags=0x%08x\n"
	       "             priv=%p",
	       src->origin,
	       src->instance.module->name,
	       src->instance.module->version.major,
	       src->instance.module->version.minor,
	       src->reqmask, src->devmask, src->flags,
	       src->priv);
	
	source_FOREACH_DEVICE(src, dev)
		_giiShowDevice(dev);
}

static void
_giiShowInput(struct gii_input *inp) {
	struct gii_source *src;
	
	printf("[input] %p\n"
	       "        reqmask=0x%08x canmask=0x%08x\n",
	       (void*)inp,
	       inp->reqmask, inp->canmask);
	
	input_FOREACH_SOURCE(inp, src)
		_giiShowSource(src);
}

static void
_giiDump(struct gg_api *api, struct gg_stem *stem) {
	_giiShowInput(GII_INPUT(stem));
}


static int
_giiController(void *arg, uint32_t cmd, void *data)
{
#if 0
	struct gg_api * api = arg;

	/* XXX Finish this. */
	switch(cmd) {
		
		
	case GII_CTL_DUMP:
		break;
	case GII_CTL_GETENV:
		break;
		
	default:
		return GGI_ENOFUNC;
	}
	
	return 0;
#endif

	return GGI_ENOFUNC;
}


int
giiCloseSource(struct gii_source *src)
{
	struct gii_device *dev;
	gii_cmd_nodata_event ev;

	DPRINT_CORE("giiCloseSource(%p)\n", src);

	if(src->flags & GII_SRCFLAG_CLOSING) {
		DPRINT_CORE("! source is already being closed.\n");
		return GGI_EBUSY;
	}
	
	/* forbid further registration on this source */
	src->flags |= GII_SRCFLAG_CLOSING;
	
	/* notify the observers */
	ggBroadcast(libgii->channel, GII_OBSERVE_SOURCE_CLOSED, src);
	ggBroadcast(src->instance.channel, GII_OBSERVE_SOURCE_CLOSED, src);
	
	/* free all devices */
	while((dev = source_FIRST_DEVICE(src))) {
		if(!(src->flags & GII_SRCFLAG_OPENING)) {
			giiEventBlank((gii_event*)&ev, sizeof(ev));
			ev.size   = sizeof(ev);
			ev.origin = dev->origin;
			ev.type   = evCommand;
			ev.code   = GII_CMDCODE_DEVICE_CLOSE;
			giiPostEvent(src, (gii_event*)&ev);
		}
		source_REM_DEVICE(src, dev);
		free(dev);
	}
	while((dev = source_FIRST_ORIGIN(src))) {
		source_REM_ORIGIN(src, dev);
		free(dev);
	}
	
	reactor->unbind(src);
	
	if(src->ops.close)
		src->ops.close(src);
	
	ggDelChannel(src->instance.channel);
	src->instance.channel = NULL;
	
	/* remove the source from the input */
	input_REM_SOURCE(inp, src);

	/* free the module */
	free(src);

	return GGI_OK;
}


/***********************************************************************
API ops
***********************************************************************/


static int
gii_init(struct gg_api* api)
{
	char *str;
	const char * confdir;
	int l;

	LIB_ASSERT(api == libgii, "API mismatch!");
	
	ggSetController(api->channel, _giiController);
	
	str = getenv("GII_DEBUGSYNC");
	if (str != NULL)
		api->debug |= DEBUG_SYNC;
	
	str = getenv("GII_DEBUG");
	if (str != NULL) {
		api->debug |= atoi(str) & DEBUG_ALL;
		DPRINT_CORE("- %s debugging=%d\n",
			    DEBUG_ISSYNC ? "sync":"async",
			    api->debug);
	}

#ifdef HAVE_CONFFILE
	/* if GII_CONFIG is set, use that */
	str = getenv("GII_CONFIG");
	if(str)
		if(ggLoadConfig(str, &api->config) == GGI_OK)
			goto have_config;
	
	/* if $HOME/.ggi/libgii.conf exists, use that */
	l = strlen(getenv("HOME")) + 6 + strlen(GIICONFFILE) + 1;
	str = malloc(l);
	if (str) {
		ggstrlcpy(str, getenv("HOME"), l);
		ggstrlcat(str, "/.ggi/", l);
		ggstrlcat(str, GIICONFFILE, l);
		l = ggLoadConfig(str, &api->config);
		free(str);
		if(l == GGI_OK)
			goto have_config;
	}
	/* try default location */
	str = NULL;
	confdir = ggGetEnv(libgii, "CONFDIR");
	if (confdir != NULL) {
		l = strlen(confdir) + 1 + strlen(GIICONFFILE) + 1;
		str = malloc(l);
	}
	if (str) {
		ggstrlcpy(str, confdir, l);
		ggstrlcat(str, "/", l);
		ggstrlcat(str, GIICONFFILE, l);
		l = ggLoadConfig(str, &api->config);
		free(str);
		if(l == GGI_OK)
			goto have_config;
	}
	
	DPRINT_CORE("! fatal error - could not find libgii config\n");
	return GGI_ENOFILE;
#else
	{
		char arrayconf[40];
		snprintf(arrayconf, sizeof(arrayconf),
			"array@%p", (const void *)_giibuiltinconf);
		l = ggLoadConfig(arrayconf, &api->config);
		if (l == GGI_OK)
			goto have_config;

		DPRINT_CORE("! fatal error - "
			"could not load builtin config\n");
		return l;
	}
#endif
	
have_config:
	
	if((reactor = _giiInitReactor()) == NULL) {
		DPRINT_CORE("! fatal error - could not install reactor\n");
		ggFreeConfig(api->config);
		return GGI_EUNKNOWN;
	}
	
	_giiInitBuiltins();
	
	return GGI_OK;
}


static void
gii_exit(struct gg_api* api)
{
	LIB_ASSERT(api == libgii, "API mismatch!");

	DPRINT_CORE("- really destroying.\n");

	_giiExitReactor(reactor);
	
	_giiExitBuiltins();
	
	ggFreeConfig(api->config);
	api->config = NULL;
	
	DPRINT_CORE("- done!\n");
}

/*
 * Allocate memory for an input descriptor and initialize it.
 * It also allocates an event queue.
 */
static int
gii_attach(struct gg_api* api, struct gg_stem *stem)
{
	struct gii_input *inp;
	int err;

	LIB_ASSERT(api == libgii, "API mismatch!");
	
	if((inp = calloc(1, sizeof(struct gii_input))) == NULL) {
		DPRINT_CORE("! could not allocate input struct.\n");
		return GGI_ENOMEM;
	}
	
	GII_PRIV(stem) = inp;
	
	input_INIT_SOURCES(inp);
	
	if ((inp->queue.mutex = ggLockCreate()) == NULL) {
		DPRINT_CORE("! could not create input queue mutex.\n");
		return GGI_EUNKNOWN;
	}
	
	if ((err = reactor->init(inp)) != GGI_OK) {
		DPRINT_CORE("! could not install reactor on input.\n");
		if (inp->queue.mutex)
			ggLockDestroy(inp->queue.mutex);
		return err;
	}
	
	/* By default, we listen to all events */
	inp->canmask = emNothing;
	inp->reqmask = emAll;
	
	if (_gii_threadsafe)
		inp->mutex = ggLockCreate();
	
	return GGI_OK;
	/* gcc warning */
	api = NULL;
}

/* Frees an input */
static void
gii_detach(struct gg_api* api, struct gg_stem *stem)
{
	struct gii_input *inp;
	int queue;

	LIB_ASSERT(api == libgii, "API mismatch!");
	
	inp = GII_INPUT(stem);
	
	/* close all sources */
	giiClose(stem, GII_EV_TARGET_ALL);
	
	/* destroy all queues */
	for (queue = 0; queue < evLast; queue++)
		if (inp->queue.queues[queue])
			free(inp->queue.queues[queue]);
	
	/* destroy queue lock */
	if (inp->queue.mutex)
		ggLockDestroy(inp->queue.mutex);
	
	/* destroy reactor */
	reactor->exit(inp);
	
	/* destroy input lock */
	if (inp->mutex) 
		ggLockDestroy(inp->mutex);
	
	free(inp);
	
	/* gcc warning */
	api = NULL;
}


static const char *
gii_getenv(struct gg_api * api, const char *var)
{
	LIB_ASSERT(api == libgii, "API mismatch!");

	if (!strcmp(var, "CONFDIR")) {
		static char giiconfdir[512] = GIICONFDIR;
		return giiconfdir + GIITAGLEN;
	}
	if (!strcmp(var, "DLSYMBOL")) {
		return "GIIdlinit";
	}
	return NULL;
}

static int
gii_plug(struct gg_api * api,
	 struct gg_module *_module,
	 struct gg_stem *stem,
	 const char * argstr,
	 void *argptr,
	 struct gg_instance **res)
{
	/*
	**  XXX closing a module may require to call updateMask,
	**  or perform other input state adaption. But we do not
	**  know if this must always be done. In some cases, we will
	**  be closing several module, and adapt the state after.
	**  so we must probably add flags to the input that specify
	**  if certain action must be taken when closing a module.
	*/
	struct gii_input *inp;
	struct gii_source *src;
	struct gii_device *dev;
	
	struct gii_module_source *module;

	LIB_ASSERT(api == libgii, "API mismatch!");
	LIB_ASSERT(_module->klass == GII_MODULE_SOURCE,
		   "module is not a source!");
	
	module = (struct gii_module_source*)_module;
	
	inp = GII_INPUT(stem);
	
	if((src = calloc(1, sizeof(*src))) == NULL) {
		DPRINT_CORE("! calloc failed\n");
		return GGI_ENOMEM;
	}
	
	src->instance.stem = stem;
	src->instance.module = _module;
	
	source_INIT_DEVICES(src);
	source_INIT_ORIGINS(src);
	
	src->devmask = emNothing;
	src->reqmask = inp->reqmask;
	src->impl.inp = inp;
	src->origin = _origins;
	src->impl.next_origin = _origins;
	src->flags = GII_SRCFLAG_OPENING;
	if((src->instance.channel = ggNewChannel(src, NULL)) == NULL) {
		DPRINT_CORE("! could not alloc channel\n");
		free(src);
		return GGI_ENOMEM;
	}
	
	if(reactor->bind(src) < 0) {
		DPRINT_CORE("! could not bind reactor to source\n");
		ggDelChannel(src->instance.channel);
		free(src);
		return GGI_EUNKNOWN;
	}
	
	input_ADD_SOURCE(inp, src);
	
	if(module->init(src, _module->name, argstr, argptr) < 0) {
		DPRINT_CORE("! init failed\n");
		/*
		  if init failed, close must not be called,
		  so we set it to NULL.further more, we need to
		  bypass ggCloseModule because the scope will be
		  deleted by ggOpenModule. Technically, the module
		  is not opened until ggOpenModule returns, so
		  it cannot be closed by ggCloseModule.
		*/
		src->ops.close = NULL;
		gii_unplug(libgii, (struct gg_instance *)src);
		return GGI_EUNKNOWN;
	}
	
	DPRINT_CORE("- source attached: \"%s:%s\",%p -> origin 0x%x\n",
		    _module->name, argstr, argptr, src->origin);
	
	_origins += GII_MAXDEVICES;
	
	/* now that the source is up, send all devices event */
	source_FOREACH_DEVICE(src, dev)
		_giiSendDevInfo(src, dev);
	
	src->flags &= ~GII_SRCFLAG_OPENING;
		
	/* notify the observers */
	ggBroadcast(api->channel,
		    GII_OBSERVE_SOURCE_OPENED, src);
	ggBroadcast(GG_STEM_API_CHANNEL(stem, api),
		    GII_OBSERVE_SOURCE_OPENED, src);
	
	*res = &(src->instance);
	
	return GGI_OK;
}


static int
gii_unplug(struct gg_api * api, struct gg_instance *instance)
{
	LIB_ASSERT(api == libgii, "API mismatch!");

	return giiCloseSource((struct gii_source *)instance);
}
