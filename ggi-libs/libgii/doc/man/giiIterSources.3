.TH "giiIterSources" 3 "2008-10-23" "libgii-current" GGI
.SH NAME
\fBgiiIterSources\fR, \fBgiiIterFillSources\fR, \fBgiiIterDevices\fR, \fBgiiIterFillDevices\fR : Iterate over input sources and devices
.SH SYNOPSIS
.nb
.nf
#include <ggi/gg.h>
#include <ggi/gii.h>

struct gii_source_iter {
      struct gg_iter iter;

      /* These must be set by the caller before calling
       * giiIterSource
       */
      gii_input stem;

      /* Placeholders for results */
      uint32_t origin;
      void *src;

      /* Internal */
      void *_state;
};

#define giiIterFillSources(_iter, _stem)                        \e
              (_iter)->stem = (_stem);

int giiIterSources(struct gii_source_iter *iter);


struct gii_device_iter {
      struct gg_iter iter;

      /* These must be set by the caller before calling
       * giiIterDevice
       */
      gii_input stem;
      uint32_t source_origin;
      void *src;              /* NULL means use source_origin */

      /* Placeholders for results */
      uint32_t origin;
      struct gii_cmddata_devinfo devinfo;

      /* Internal */
      void *_state;
};

#define giiIterFillDevices(_iter, _stem, _src, _src_origin)   \e
              (_iter)->stem = (_stem);                        \e
              (_iter)->src = (_src);                          \e
              (_iter)->source_origin = (_src_origin);

int giiIterDevices(struct gii_device_iter *iter);
.fi

.SH DESCRIPTION
These functions provide a way to iterate over input sources and devices.

\fIinput sources\fR are per-target. A target can provide one or more input devices.
Therefore, an input source can contain multiple input devices.

\fBgiiIterSources\fR allows to iterate over all input sources. This function will
prepare the \fIiter\fR structure to be used as an iterator. \fIiter\fR is a
pointer to a gii_source_iter structure owned by the caller. This user part of
the structure must be set by the caller before calling \fBgiiIterSources\fR. The
\fIstem\fR field must be filled with the stem associated with the input sources.
\fIorigin\fR and \fIsrc\fR are placeholders in which the results will be found
along the iteration.  \fIorigin\fR contains the number of the input source and
\fIsrc\fR is a pointer to the source. The resulting pointer belongs to libgii
and \fBmust not\fR be freed or altered, and they may be invalidated if not used
during the iteration process.  They must be copied if needed later. \fI_state\fR
is an internal opaque pointer that keeps track of the iterator state. Its value
must never be touched by the caller.

\fBgiiIterDevices\fR allows to iterate over all input devices of a certain input
source. This function will prepare the \fIiter\fR structure to be used as an
iterator. \fIiter\fR is a pointer to a gii_device_iter structure owned by the
caller.  This user part of the structure must be set by the caller before
calling \fBgiiIterDevices\fR. The \fIstem\fR field must be filled with the stem
associated with the input sources and devices. The \fIsource_origin\fR and
\fIsrc\fR fields determine the input source to iterate over. \fIsrc\fR can only
be filled with the \fIsrc\fR result pointer from the gii_source_iter structure
or must be set to NULL. If \fIsrc\fR is NULL, \fIsource_origin\fR specifies the
origin of an input source. It may be the \fIorigin\fR from the gii_device_iter
structure.  When \fIsrc\fR is non-NULL, then \fIsource_origin\fR value is not
used, so setting both fields has the same effect as setting the \fIsrc\fR field
only.  Specifying invalid values that don't cover any specified cases above
results in undefed behaviour. The \fIorigin\fR and \fIdevinfo\fR fields are
placeholders in which the results will be found along the iteration.
\fIorigin\fR contains the origin number of the input device and \fIdevinfo\fR
contains the \f(CWgii_cmddata_devinfo(3)\fR structure of the device. The
\fIorigin\fR and \fIdevinfo\fR fields are copies from the libgii internals and
change during iteration. So changes to them have no effect and must be copied
if needed later.  \fI_state\fR is an internal opaque pointer that keeps track of
the iterator state.  Its value must never be touched by the caller.

\fBgiiIterFillSources\fR is a helper macro that may be used to fill out the
\fBgii_source_iter\fR structure.

\fBgiiIterFillDevices\fR is a helper macro that may be used to fill out the
\fBgii_device_iter\fR structure.
.SH RETURN VALUE
\fBgiiIterFillSources\fR and \fBgiiIterFillDevices\fR are helper macros. Their
return type is void.

\fBgiiIterSources\fR and \fBgiiIterDevices\fR return GGI_OK on success
or a \fBgg-error(3)\fR error code otherwise.
.SH EXAMPLE
This code demonstrates how to iterate over all input sources and
input devices:

.nb
.nf
struct gii_source_iter src_iter;
struct gii_device_iter dev_iter;
uint32_t dev_origin;
struct gii_cmddata_devinfo *devinfo;

giiIterFillSources(&src_iter, stem);
giiIterSources(&src_iter);
GG_ITER_FOREACH(&src_iter) {
      giiIterFillDevices(&dev_iter, stem, src_iter.src, src_iter.origin);
      giiIterDevices(&dev_iter);
      GG_ITER_FOREACH(&dev_iter) {
              ...
              /* device origin */
              dev_origin = dev_iter.origin;
              /* device info */
              devinfo = &dev_iter.devinfo;

              ...

              /* do something with the device data */

              ...

      }
      GG_ITER_DONE(&dev_iter);
}
GG_ITER_DONE(&src_iter);
.fi

.SH SEE ALSO
\f(CWgiiQueryDeviceInfo(3)\fR, \f(CWgiiQueryValInfo(3)\fR
