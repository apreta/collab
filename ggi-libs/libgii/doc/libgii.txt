====================
LibGII Documentation
====================

LibGII Library
==============


A flexible library for input handling
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. manpage:: 7 libgii

Description
-----------

LibGII is an input library developed by the `GGI Project`_.  Its
design philosophy is similar to LibGGI, which deals with graphics
output.

.. _GGI Project: http://www.ggi-project.org

LibGII is based on the concept of input streams, which virtualize
access to the underlying input drivers. Events from various input
devices are abstracted into easy-to-use structures. LibGII also allows
the application to join streams together, receiving input from an
arbitrary combination of devices.


LibGII is a separate component from LibGGI, although LibGGI depends on
LibGII for input purposes.  (LibGGI's input functions are simply
wrappers for LibGII functions.)


Environment Variables
---------------------


The following outlines the environment variables, intended for the
user, which affect the behaviour of LibGII:

`GII_INPUT`
    The default input source(s) and/or filter(s) to use when giiOpen()
    is called with a NULL argument.  The format is: ``input:inputargs``.
    Multiple inputs can be specified by this notation: ``(i1):(i2) ...``.

    .. note::

        This setting does not affect LibGGI visuals automatically
	opening inputs. Use the ``GGI_INPUTx`` variable instead.

`GII_DEBUG`
    The debug level for LibGII:

    - 0 or unset : debug output is off; debugging is off
    - 255 : all debug output is on

    You may also bitwise 'or' any of the following together:

    - 2   : debug core
    - 32  : misc debugging output
    - 64  : debug dynamic library handling
    - 128 : debug event handling

`GII_DEBUGSYNC`
    Turn on synchronous debug output, flushing the output buffers
    before returning from `DPRINT` calls.

`GII_CONFDIR`
    Override compiled-in path to global config files (Win32 only,
    but not Cygwin).

Other environment variables specific to filters and inputs are
documented in the corresponding manual page.


See Also
--------

:man:`libgii.conf(5)`, :man:`filter-keytrans(7)`, :man:`filter-mouse(7)`,
:man:`filter-save(7)`, :man:`filter-tcp(7)`, :man:`input-directx(7)`, 
:man:`input-linux-kbd(7)`, :man:`input-mouse(7)`, :man:`input-file(7)`, 
:man:`input-linux-mouse(7)`, :man:`input-tcp(7)`,
:man:`input-linux-evdev(7)`, :man:`input-lk201(7)`.



LibGII configuration file format
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. manpage:: 5 libgii.conf


Description
-----------

``/etc/ggi/libgii.conf`` is the configuration file that defines what
input module are available and where libgii is supposed to find them.
It consists of lines defining target locations (mapping a target name
a function name) and target aliases (fake targets that actually calls
other target with a specific set of parameters).

The format is common to all GGI libraries. It is defined by libgg.
See :man:`ggLoadConfig(3)` for additional information on file
inclusions and other generic options.


Examples
--------

These examples show how to use the generic configuration mechanism
proposed by ``LibGG`` with ``LibGII``.


The first example defines three input modules (or targets) for which
initialization function is found in three different dynamic libraries
(.so files), under the default LibGII input symbol: ``GIIdlinit``::

  input-stdin			input/stdin.so
  input-x			input/x.so
  input-xwin			input/xwin.so


In the second example, the two inputs are implemented in a single
dynamic library, but they each have their own initialization functions
in this library. Their name is separated from the path by a ``:``.

  input-x			input/x.so:GIIdl_x
  input-xwin			input/x.so:GIIdl_xwin


The third example defines only one real target ``input-x``, located in
shared object ``input/x.so`` under the symbol ``GIIdl_x``.
``input-xwin`` is an alias that will resolve to target ``input-x``
with the option string ``-be-xwin`` to be passed to the target
function (``GIIdl_x`` in ``input/x.so``).

  input-x			input/x.so:GIIdl_x
  alias		input-xwin	input-x:-be-xwin


The last examples defines two inputs, with two possible location for
their implementation.  The first two lines are the same as in example
2. The other two states that these two inputs can also be found (if
the previous fail) as a built-in modules. The ``/gii-builtins`` path
points to the ``LibGII`` built-in symbol namespace.  In this case both
input would be found in this namespace under the default symbol
``GIIdlinit``.  The initialization function will be given the
requested target name to know which implementation to use.

input-x				input/x.so:GIIdl_x
input-xwin			input/x.so:GIIdl_xwin

input-x				/gii-builtins
input-xwin			/gii-builtins


See also
--------

:man:`ggLoadConfig(3)`


.. include:: libgii-functions.txt

.. include:: libgii-structures.txt

.. include:: libgii-libraries.txt
