LibGII types and structures
============================


LibGII event structures
~~~~~~~~~~~~~~~~~~~~~~~

.. manpage:: 3 gii_event gii_any_event gii_event_type gii_event_mask

Synopsis
--------

::

  #include <ggi/gii-events.h>

  typedef union gii_event {

      uint8_t                 size;

      gii_any_event           any;
      gii_cmd_event           cmd;
      gii_expose_event        expose;
      gii_val_event           val;
      gii_key_event           key;
      gii_pmove_event         pmove;
      gii_pbutton_event       pbutton;
      gii_fromapi_event       fromapi;
  } gii_event;

  #define COMMON_DATA  \
  uint8_t   size;           /* size of event in bytes       */\
  uint8_t   type;           /* type of this event           */\
  int16_t   error;          /* error (for replies)          */\
  uint32_t  origin;         /* origin device (etc)          */\
  uint32_t  target;         /* target device (etc)          */\
  int32_t   usec;           /* usec part of timestamp       */\
  time_t    sec             /* sec part of timestamp        */

  typedef struct {

      COMMON_DATA;

  } gii_any_event;



Description
-----------

Events are of type `gii_event`. It is an union of all of the
structures for each specific type of event.


Structure Members
-----------------


All of the event structures contains housekeeping information at the
beginning, as defined by `COMMON_DATA`.


Thus, by analyzing the contents of `any.type`, you can determine what
the given event is, and select the appropriate member of the
`gii_event` union to access to get at the event 

The common fields found in any event structure are:

size
    Specifies the size of the given event (in bytes).

type
    An enumeration of the possible types of LibGII events (see next section).

error
    Mainly there to round things up to a 32-bit boundary, but could
    be used to signal an error in a send-reply sequence.
    
origin
    A device handle: it distinguishes one input device from another.
    Other than that there's no real meaning to the number.

target
    Also a device handle, but for distinguishes input devices when
    sending events **to** an input device via :man:`giiEventSend(3)`.

sec, usec
    Indicates when the event in question has been generated.


Event types
-----------

The different types of events are defined as an enumeration of type
`gii_event_type`. The possible values are:

- `evNothing`          : event is not valid.
- `evCommand`          : report command or do action.
- `evInformation`      : notification of new information.
- `evExposure`         : exposure event.
- `evKeyPress`         : a key has been pressed.
- `evKeyRelease`       : a key has been released.
- `evKeyRepeat`        : automatically repeated keypress.
- `evPtrRelative`      : pointer movement reported in relative coordinates.
- `evPtrAbsolute`      : pointer movement reported in absolute coordinates.
- `evPtrButtonPress`   : a pointer button has been pressed.
- `evPtrButtonRelease` : a pointer button has been released.
- `evValRelative`      : valuator change reported as a relative value.
- `evValAbsolute`      : valuator change reported as an absolute value.
- `evFromAPI`          : notification from a certain API

Event masks
-----------

`gii_event_mask` is passed to various event handling functions to
indicate which types of events the program is interested in. The list
below sums the available event masks:

- `emCommand`        : `evCommand`
- `emInformation`    : `evInformation`
- `emExpose`         : `evExpose`
- `emKeyPress`       : `evKeyPress`
- `emKeyRelease`     : `evKeyRelease`
- `emKeyRepeat`      : `evKeyRepeat`
- `emKey`            : Any of `evKeyPress`, `evKeyRelease` or `evKeyRepeat`
- `emPtrRelative`    : `evPtrRelative`
- `emPtrAbsolute`    : `evPtrAbsolute`
- `emPtrButtonPress` : `evPtrButtonPress`
- `emButtonRelease`  : `evButtonRelease`
- `emPtrMove`        : Any of `evPtrRelative` or `evPtrAbsolute`
- `emPtrButton`      : Any of `evPtrButtonPress` or `evPtrButtonRelease`
- `emPointer`        : All pointer events
- `emValRelative`    : `evValRelative`
- `emValAbsolute`    : `evValAbsolute`
- `emValuator`       : Any of `evValRelative` or `evValAbsolute`
- `emAll`            : Any event type
- `emNothing`        : Matches no event type
- `emFromAPI`        : Notification from a certain API


See Also
--------

:man:`gii_key_event(3)`, :man:`gii_pmove_event(3)`,
:man:`gii_pbutton_event(3)`, :man:`gii_cmd_event(3)`,
:man:`gii_val_event(3)`, :man:`gii_expose_event(3)`,
:man:`gii_fromapi_event(3)`






LibGII key events
~~~~~~~~~~~~~~~~~

.. manpage:: 3 gii_key_event

Synopsis
--------

::

  #include <ggi/gii-events.h>

  typedef struct {

      GII_EV_COMMON_DATA;
      
      uint32_t  modifiers;
      uint32_t  sym;
      uint32_t  label;
      uint32_t  button;

  } gii_key_event;


Description
-----------

The `gii_key_event` structure represents key/button events from
keyboards and other devices.


Generated Events
----------------

`evKeyPress`
    The key specified in the structure is pressed. Not
    repeatedly produced while holding down the key.

`evKeyRelease`
    A key specified in the structure is released.

`evKeyRepeat`
    Makes sense when dealing with character input. A key is being
    held down and the character should be processed at intervals
    when the key is held down.

    The key repeat rate and the delay before repeat is unspecified
    and depends on the user's environment.


Structure Members
-----------------

`modifiers`
    Result of bitwise-or of the following flags, indicating certain shift
    states:
    
    - `GII_MOD_SHIFT`
    - `GII_MOD_CTRL`
    - `GII_MOD_ALT`
    - `GII_MOD_META`
    - `GII_MOD_SUPER`
    - `GII_MOD_HYPER`
    - `GII_MOD_ALTGR`
    - `GII_MOD_CAPS`
    - `GII_MOD_NUM`
    - `GII_MOD_SCROLL`

`sym`
    The *symbol* of the key, which is the resultant character produced
    by the key. This is roughly a transformation of the `label` with
    the current `modifiers`. It also depends on the user's key
    configuration.
    
`label`
    The actual label visible on the key in question.  This is either
    the symbol produced when there are no modifiers or it is the *most
    prominent* symbol on that key.  For example:

    
    - The numeric keys on top of the letter keys on a standard PC
      keyboard have `label` values which are the digit characters in
      ASCII.

    - The English letter keys on a keyboard are represented by ``A``
      through ``Z`` in `label`. Although in their unshifted state
      these keys produce lowercase letters, the keycaps are printed
      with uppercase by convention, so this is what LibGII returns.

    `label` can be used as a generalized, portable keycode or scancode
    of the key (That is, if the documentation for an applications says
    that something is is mapped to key ``y``, it is, even for German
    keyboard, where ``y`` and ``z`` are swapped)
    
`button`
    The button number distinguishing between the different buttons on
    the device.  For example, on a keyboard it is a number from 0 to
    127 (i.e. a scancode), on a joystick it might be 1 to 4, and on a
    spaceorb it will be 1 to 8.


If an application is interested in what physical keys are pressed and
released (most games for example), read the `label` field. Usually the
modifiers in effect are irrelevant for these applications (however,
for non-alphanumeric symbols like ``+`` and ``-``, it is wise to check
the `sym` field instead of `label` as they are accessed using shifts
on some keyboard configurations).


If an application wants a stream of characters (for text input), it
should read the `sym` field.

GGI keysym system
-----------------

In GGI, key values are defined in `ggi/gii-keyboard.h`. They are
basically Unicode characters with some extensions:


Keys which are not represented by codepoints in the Unicode standard
(such as the numeric keypad keys) are assigned codepoints in the
private range. Applications should use the `GIIK_` #defines for
representing these keys.  These codepoints are used in `label`, but
they can also occur in `sym` when the symbol is not any character
(e.g. arrow keys, function keys).


The `GIIUC_` #defines represent normal Unicode characters. The
#defines are interchangeable with their corresponding codepoint
scalars and their use is optional. However, it is recommended to use
them for the symbols `GIIUC_BackSpace`, `GIIUC_Return`, `GIIUC_Escape`
and `GIIUC_Delete`.

.. important::
    In the LibGII system, no key is guaranteed to exist; the key
    values are for identification only.  Particularly, applications
    should **not** rely on their presence.  Also, because not all
    keyboards are configured in the same way, applications are
    encouraged to allow user configuration of the keys used and not
    hard-code their values.

See Also
--------

:man:`gii_event(3)`






LibGII pointer movement events
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. manpage:: 3 gii_pmove_event

Synopsis
--------

::

  #include <ggi/gii-events.h>

  typedef struct {

      GII_EV_COMMON_DATA;

      int32_t  x, y;           /* absolute/relative position   */
      int32_t  z, wheel;

  } gii_pmove_event;


Description
-----------

The `gii_pmove_event` structure is used to report change of pointer
position. Depending on the event type, the values are either absolute
or relative.


Generated Events
----------------

This structure is used for `evPtrRelative` and `evPtrAbsolute` events.


Structure Members
-----------------

The `gii_pmove_event` structure describes pointer (mice, etc.) motion
in terms of the `x`, `y`, `z` coordinates and the `wheel` values of
the device.  The motion described may be relative (offset from the
current location) or absolute (a specific location on the 'screen'),
depending on whether the event is of type `evPtrRelative` or
`evPtrAbsolute`, respectively.


LibGII does not attempt to interpolate or keep track of the *current*
pointer position.  (This is the application's responsibility.)  LibGII
may also report both relative and absolute pointer motion for the same
input, which may happen when the mouse is being emulated using a
keyboard on an input/target that is normally reports absolute motion.


Examples
--------

Input handling for applications expecting absolute pointer position::

  {
      static int mousex,mousey;

      giiEventPoll(stem, emKey|emPointer, NULL);
      events = giiEventsQueued(stem, emKey|emPointer);

      while (events--) {
          giiEventRead(stem, &event, emKey|emPointer);

	  switch(event.any.type) {
	  case evPtrButtonPress:
	          switch(event.pbutton.button) {
		  case GII_PBUTTON_FIRST:
		      do_something_as_appropriate(mousex,mousey);
		      break;
		  case GII_PBUTTON_SECOND:
		      /* ... */
		  }
		  break;
	  case evPtrButtonRelease:
	      /* ... if needed ... */
	      break;
	  case evPtrAbsolute:
	      mousex = event.pmove.x;
	      mousey = event.pmove.y;
	      break;
	  case evPtrRelative:
	      mousex += event.pmove.x;
	      mousey += event.pmove.y;
	      break;
	  }

	  /* Constrain mouse in any case */
	  if (mousex < 0) mousex = 0;
	  if (mousey < 0) mousey = 0;
	  if (mousex > xmax) mousex = xmax;
	  if (mousey > ymax) mousey = ymax;

      } /* while */
  }


See Also
--------

:man:`gii_event(3)`






LibGII pointer button events
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. manpage:: 3 gii_pbutton_event

Synopsis
--------

::

  #include <ggi/gii-events.h>

  typedef struct {

      GII_EV_COMMON_DATA;

      uint32_t  button;

  } gii_pbutton_event;


Description
-----------

Button events are sent to report a change in pointer button
state. Depending on the event type, the button is either being pressed
or released.


Generated Events
----------------

This structure is used for `evPtrButtonPress` and `evPtrButtonRelease`
events.


Structure Members
-----------------

`gii_pbutton_event` simply specifies that the `button` is pressed or
released.


Pointer buttons are specified in order of common usage, with 1 being
the primary button. The following values are defined:


- `GII_PBUTTON_LEFT`, `GII_PBUTTON_PRIMARY`, `GGI_PBUTTON_FIRST` equal 1.
- `GII_PBUTTON_RIGHT`, `GII_PBUTTON_SECONDARY`, `GGI_PBUTTON_SECOND` equal 2.
- `GII_PBUTTON_MIDDLE`, `GII_PBUTTON_TERTIARY`, `GGI_PBUTTON_THIRD` equal 3.

Of course, applications should avoid hardcoding mouse button values.


See Also
--------

:man:`gii_event(3)`






LibGII commands and information events
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. manpage:: 3 gii_cmd_event gii_cmd_nodata_event

Synopsis
--------

::

  #include <ggi/gii-events.h>

  typedef struct {

      GII_EV_COMMON_DATA;

      uint32_t	code;
      uint32_t  seqnum;

  } gii_cmd_nodata_event;

  #define GII_CMD_DATA_MAX  (248-sizeof(gii_cmd_nodata_event))

  typedef struct {

      GII_EV_COMMON_DATA;

      uint32_t  code;
      uint32_t  seqnum;
      uint8_t   data[GII_CMD_DATA_MAX];

  } gii_cmd_event;


Description
-----------

These are used internally either to the application or the kernel. The
same event is used for both Command and Information events.


Generated Events
----------------

`gii_cmd_event` is the basic structure for `evCommand` and
`evInformation` events.  It may need to be casted to some other
structure (depending on `code`) to access the data.


Structure Members
-----------------

`code`
    The command or request code.

`seqnum`
    The sequence number of the packet.

`data`
    Provides raw access to the device and/or command specific
    data. The recipient must not store references to the data. If the
    data information is needed afterwards, copy it!


Device information
------------------

The use of `evCommand` is to convey information on a GII device.
The command codes are:

`GII_CMDCODE_EVENTLOST`
  * Input: None.
  * Output: Inform that an event was lost due to queue overflow. No data.

`GII_CMDCODE_PREFER_ABSPTR` / `GII_CMDCODE_PREFER_RELPTR`
  * Input: Tell sources that they should try to generate the prefered ABS/REL
    pointer events.
  * Output: None.

`GII_CMDCODE_DEVICE_INFO`
  * Input: Request device information/capabilities.
  * Output: Responds to information request, notify new device or device state
    change. Data is :man:`gii_cmddata_devinfo(3)`

`GII_CMDCODE_DEVICE_CLOSE`
  * Input: Tell a device to stop operating (forever). No data.
  * Output: Report a device is not available anymore. No data.

`GII_CMDCODE_DEVICE_ENABLE` / `GII_CMDCODE_DEVICE_DISABLE`
  * Input: Enable/disable a device. No data. A disabled device is still
    there but does not generate events.
  * Output: Confirm device enabling/disabling. No data.

`GII_CMDCODE_SET_REGISTER` / `GII_CMDCODE_GET_REGISTER`
  * Input: Initiate an io operation on a device register.
    Data is :man:`gii_cmddata_ioreg(3)`
  * Output: Notify that the io operation is completed or that an error
    occured if ev.any.error is set. Data is :man:`gii_cmddata_ioreg(3)`

`GII_CMDCODE_REGISTER_INFO`
  * Input: Request register information. Data is :man:`gii_cmddata_reginfo(3)`
    with only number set.
  * Output: Notify that the io operation is completed or that an error
    occured if ev.any.error is set. Data is :man:`gii_cmddata_reginfo(3)`.

`GII_CMDCODE_VALUATOR_INFO`
  * Input: Request valuator information. Data is :man:`gii_cmddata_valinfo(3)`
    with only number set.
  * Output: Respond to valuator information request.
    Data is :man:`gii_cmddata_valinfo(3)`.


See Also
--------

:man:`gii_event(3)`, :man:`gii_cmddata_devinfo(3)`, :man:`gii_cmddata_ioreg(3)`, :man:`gii_cmddata_reginfo(3)`, :man:`gii_cmddata_valinfo(3)`




LibGII valuator events
~~~~~~~~~~~~~~~~~~~~~~

.. manpage:: 3 gii_val_event

Synopsis
--------

::

  #include <ggi/gii-events.h>

  typedef struct {

      GII_EV_COMMON_DATA;

      uint32_t  first;          /* first valuator reported      */
      uint32_t  count;          /* number reported              */
      int32_t   value[32];      /* absolute/relative values     */

  } gii_val_event;


Description
-----------
  
A valuator is a representation of the state of an input device in
terms of a physical quantity such as length.  A valuator event, of
type `gii_val_event`, reports a change in any of those quantities.


Generated Events
----------------


This structure is used for the `evValRelative` and `evValAbsolute`
events.


Structure Members
-----------------

A valuator may be either absolute or relative, like pointer motion
events.


To interpret the numbers, query valuator device info with the
`GII_CMDCODE_VALUATOR_INFO` command code.




See Also
--------

:man:`gii_event(3)`, :man:`gii_cmddata_valinfo(3)`






LibGII expose events
~~~~~~~~~~~~~~~~~~~~

.. manpage:: 3 gii_expose_event

Synopsis
--------

::

  #include <ggi/gii-events.h>

  typedef struct {

      GII_EV_COMMON_DATA;

      uint32_t  x,y;
      uint32_t  h,w;

  } gii_expose_event;


Description
-----------

If an application loses the focus and is not physically displayed
(e.g. console switching, iconifying), it may be stopped. Some targets
may implement a backbuffer and allow continuing, though.

After reactivation, the application will receive a redraw event,
`evExpose`.


Generated Events
----------------


The `gii_expose_event` is used for the `evExpose` event.


Structure Members
-----------------


The fields describe the region which needs to be redrawn.


Examples
--------

`evExpose` handling skeleton::

  ggi_visual_t vis;
  ggi_event ev;

  /* ... wait and get the event... */

  if (ev.any.type == evExpose) {
      /* We might not be able to render partially ... */
      render_screen(vis);
                
      /* but flush only the region needed */
      ggiFlushRegion(vis, ev.expose.x, ev.expose.y,
			  ev.expose.w, ev.expose.h);
  }

  /* ... etc ... */


See Also
--------

:man:`gii_event(3)`


Notification from certain API
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. manpage:: 3 gii_fromapi_event


Synopsis
--------

::

  #include <ggi/gii-events.h>

  typedef struct {
	GII_EV_COMMON_DATA;

	int32_t api_id;
	uint32_t code;
  } gii_fromapi_nodata_event;

  typedef struct {
	GII_EV_COMMON_DATA;

	int32_t api_id;
	uint32_t code;

        uint8_t data[GII_FROMAPI_DATA_MAX];
  } gii_fromapi_event;


Description
-----------

TODO

Generated Events
----------------

The `gii_fromapi_event` is used for the `evFromAPI` event.


Structure Members
-----------------

`api_id`
     TODO

`code`
     TODO

`data`
     TODO

Example
-------

TODO

See Also
--------

:man:`gii_event(3)`


GII device capabilities description
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. manpage:: 3 gii_cmddata_devinfo


Synopsis
--------

::

  #include <ggi/gii-events.h>

  struct gii_cmddata_devinfo {
  #define GII_MAX_DEVNMAME	  32
      char		devname[GII_MAX_DEVNAME];
  #define GII_VENDOR_GGI_PROJECT  0
  #define GII_PRODUCT_GGI_PROJECT 0
      uint32_t		vendor_id;
      uint32_t		product_id;

      gii_event_mask	can_generate;

  #define GII_NUM_UNKNOWN	  0xffffffff
      uint32_t		num_registers;
      uint32_t		num_valuators;
      uint32_t		num_button;
  };


Description
-----------

This structure describe an input device. It is used for notifying
new devices or device state changes.


Structure Members
-----------------

`devname`
    A human-readable NULL terminated string identifying the device.

`vendor_id`
    Vendor of the input device.

`product_id`
    Product id of the input device.

`can_generate`
    This mask tells what kind of events this device can generate.

`num_registers`
    The number of users.  `GII_NUM_UNKNOWN` mean that the number
    of registers is not known. This is the usual initialization
    value.

`num_valuators`
    The number of valuators this device has.  `GII_NUM_UNKNOWN`
    means that the number of valuators is not known.

`num_buttons`
    The number of button this device has.  `GII_NUM_UNKNOWN`
    means that the number of button is not known.


See Also
--------

:man:`giiQueryDeviceInfo(3)`


GII io operation description
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. manpage:: 3 gii_cmddata_ioreg


Synopsis
--------

::

  #include <ggi/gii-events.h>

  struct gii_cmddata_ioreg {

  #define GIIREG_VENDOR_SPECIFIC	(1<<31U)
  #define GIIREG_PRODUCT_SPECIFIC	((1<<31U)|(1<<30U))
	uint32_t register_id;
	uint32_t vendor_id;	/* ignored if the register_id is */
	uint32_t product_id;	/* not vendor/product specific */

	/* for SET_REGISTER, size and address of the buffer
	 * containing the new data for this register;
	 * for GET_REGISTER, size and address of the buffer
	 * where libgii must write the result.
	 */
	uint32_t size;
	char *data;
  };

  #define GIIREG_DEVICE_LONGNAME	1

Description
-----------

TODO

Structure Members
-----------------

TODO

See Also
--------

:man:`gii_cmd_event(3)`


GII register description
~~~~~~~~~~~~~~~~~~~~~~~~~

.. manpage:: 3 gii_cmddata_reginfo


Synopsis
--------

::

  #include <ggi/gii-events.h>

  struct gii_cmddata_reginfo {
        
        uint32_t  number;    /* Number of the register */
  #define GII_REG_QUERY_ALL 0xffffffff
        
        uint32_t   vendor_id;
        uint32_t   product_id;
        uint32_t   register_id;
        
        /* name of this register */
  #define GII_MAX_REGNAME   32
        char       regname[GII_MAX_REGNAME];
        /*  size in bytes of this register */
        uint32_t   size;
        
        char      *data;
  };


Description
-----------

TODO

Structure Members
-----------------

TODO

See Also
--------

:man:`gii_cmd_event(3)`



GII valuators description
~~~~~~~~~~~~~~~~~~~~~~~~~

.. manpage:: 3 gii_cmddata_valinfo gii_phystype gii_valrange


Synopsis
--------

::

  #include <ggi/gii-events.h>

  typedef struct gii_valrange {
      int32_t		min, center, max;
  } gii_valrange;

  typedef enum {
        GII_PT_UNKNOWN,                 /* unknown */
        GII_PT_TIME,                    /* base unit s */
        GII_PT_FREQUENCY,               /* base unit 1/s (Hz) */
        GII_PT_LENGTH,                  /* base unit m */
        GII_PT_VELOCITY,                /* base unit m/s */
        GII_PT_ACCELERATION,            /* base unit m/s^2 */
        GII_PT_ANGLE,                   /* base unit radian */
        GII_PT_ANGVELOCITY,             /* base unit radian/s */
        GII_PT_ANGACCELERATION,         /* base unit radian/s^2 */
        GII_PT_AREA,                    /* base unit m^2 */
        GII_PT_VOLUME,                  /* base unit m^3 */
        GII_PT_MASS,                    /* base unit kg */
        GII_PT_FORCE,                   /* base unit N (kg*m/s^2) */
        GII_PT_PRESSURE,                /* base unit N/m^2 (Pa) */
        GII_PT_TORQUE,                  /* base unit Nm */
        GII_PT_ENERGY,                  /* base unit Nm, VAs, J */
        GII_PT_POWER,                   /* base unit Nm/s, VA, W */
        GII_PT_TEMPERATURE,             /* base unit K */
        GII_PT_CURRENT,                 /* base unit A */
        GII_PT_VOLTAGE,                 /* base unit V (kg*m^2/(As^3)) */
        GII_PT_RESISTANCE,              /* base unit V/A (Ohm) */
        GII_PT_CAPACITY,                /* base unit As/V (Farad) */
        GII_PT_INDUCTIVITY,             /* base unit Vs/A (Henry) */
        GGI_PT_LAST
  } gii_phystype;


  struct gii_cmddata_valinfo {
      uint32_t		number;
      char		longname[75];
      char		shortname[5];
      gii_valrange	range;
      gii_phystype	phystype;
      int32_t		SI_add,SI_mul,SI_div,SI_shift;
  };


Description
-----------

This structure is used to describe the values reported by a specific
valuator.


Structure Members
-----------------

`gii_cmddata_getvalinfo` fields are defined as follow:

`number`
    Number of the queried valuator.

`longname`
    A human-readable NULL terminated string identifying the valuator.

`shortname`
    A NULL terminated abbreviated name for this valuator.

`range`
    Contains the minimum, center and maximum values for this valuator. Note
    that this range may change on some devices due to calibration, but it is
    usually not expected that a device recalibrates while in use. You should
    react gracefully to values received from the valuator that are outside the
    specified range, though.

`phystype`
    Gives the physical quantity the device measures. The idea is to report the
    thing the user actually controls. I.e. a Joystick actually measures
    resistance, but should report `GII_PT_ANGLE` or `GII_PT_FORCE`, as that is
    what the user does to the stick and what results in the changed resistance.
    
`SI_add`, `SI_mul`, `SI_div`, `SI_shift`
     Using these values, it is possible to give calibration data to the
     application or to compute the actual floating point value (in the unit
     expressed in `phystype`) reported by the valuator with the following
     formula::

       (float)(SI_add + value) * (float)SI_mul / (float)SI_div * pow(2.0, SI_shift);



Physical units
--------------

The following physical units are defined for `gii_phystype`:

- `GII_PT_UNKNOWN`         : unknown
- `GII_PT_TIME`            : base unit s (second)
- `GII_PT_FREQUENCY`       : base unit 1/s (Hz)
- `GII_PT_LENGTH`          : base unit m (meter)
- `GII_PT_VELOCITY`        : base unit m/s
- `GII_PT_ACCELERATION`    : base unit m/s^2
- `GII_PT_ANGLE`           : base unit radian
- `GII_PT_ANGVELOCITY`     : base unit radian/s
- `GII_PT_ANGACCELERATION` : base unit radian/s^2
- `GII_PT_AREA`            : base unit m^2
- `GII_PT_VOLUME`          : base unit m^3
- `GII_PT_MASS`            : base unit kg
- `GII_PT_FORCE`           : base unit N (kg*m/s^2)
- `GII_PT_PRESSURE`        : base unit N/m^2 (Pa)
- `GII_PT_TORQUE`          : base unit Nm
- `GII_PT_ENERGY`          : base unit Nm, VAs, J
- `GII_PT_POWER`           : base unit Nm/s, VA, W
- `GII_PT_TEMPERATURE`     : base unit K
- `GII_PT_CURRENT`         : base unit A
- `GII_PT_VOLTAGE`         : base unit V (kg*m^2/(As^3))
- `GII_PT_RESISTANCE`      : base unit V/A (Ohm)
- `GII_PT_CAPACITY`        : base unit As/V (Farad)
- `GII_PT_INDUCTIVITY`     : base unit Vs/A (Henry)


See Also
--------

:man:`giiQueryValInfo(3)`


GII input
~~~~~~~~~

.. manpage:: 3 gii_input


Synopsis
--------

::

  #include <ggi/gii.h>

  /* A stem on which the gii api is attached.
   */
  typedef struct gg_stem *gii_input;

Description
-----------

The `gii_input` is an alias for `struct gg_stem`. It is inherited from
GGI 2.2.x and still exists for simple migration from GGI 2.2.x.

See Also
--------

:man:`gg_stem(3)`
