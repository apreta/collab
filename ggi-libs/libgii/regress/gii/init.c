/* $Id: init.c,v 1.7 2006/09/23 08:31:33 cegger Exp $
******************************************************************************

   init - Regression tests for init/exit handling

   Copyright (C) 2005 by Christoph Egger

   This software is placed in the public domain and can be used freely
   for any purpose. It comes without any kind of warranty, either
   expressed or implied, including, but not limited to the implied
   warranties of merchantability or fitness for a particular purpose.
   Use it at your own risk. the author is not responsible for any damage
   or consequences raised by use or inability to use this program.

******************************************************************************
*/

#include "config.h"

#include <ggi/gg.h>
#include <ggi/gg-api.h>
#include <ggi/gii.h>

#include "../testsuite.inc.c"


static void testcase1(const char *desc)
{
	int err;
	
	printteststart(__FILE__, __PRETTY_FUNCTION__, EXPECTED2PASS, desc);
	if (dontrun) return;


	err = giiInit();
	if (err != GGI_OK) {
		printfailure("expected return value: 0\n"
			"actual return value: %i\n", err);
		return;
	}

	err = giiInit();
	if (err != 1) {
		printfailure("expected return value: 0\n"
			"actual return value: %i\n", err);
		return;
	}


	printsuccess();
	return;
}


static void testcase2(const char *desc)
{
	int err;
	
	printteststart(__FILE__, __PRETTY_FUNCTION__, EXPECTED2PASS, desc);
	if (dontrun) return;


	err = giiExit();
	if (err != 1) {
		printfailure("expected return value: 1\n"
			"actual return value: %i\n", err);
		return;
	}

	err = giiExit();
	if (err != GGI_OK) {
		printfailure("expected return value: 0\n"
			"actual return value: %i\n", err);
		return;
	}


	printsuccess();
	return;
}


static void testcase3(const char *desc)
{
	int err;
	gii_input inp;
	int success = 1;
	
	printteststart(__FILE__, __PRETTY_FUNCTION__, EXPECTED2PASS, desc);
	if (dontrun) return;


	err = giiInit();
	if (err != GGI_OK) {
		printfailure("giiInit: expected return value: 0\n"
			     "actual return value: %i\n", err);
		success = 0;
		goto do_return;
	}
	
	if ((inp = ggNewStem(NULL)) == NULL) {
		printfailure("ggNewStem: returned NULL\n");
		success = 0;
		goto do_exit;
	}

	err = giiAttach(inp);
	if(err != 0) {
		printfailure("giiAttach: expected return value: 0\n"
			     "actual return value: %i\n", err);
		success = 0;
		goto do_del_stem;
	}
	
	err = giiOpen(inp, NULL, NULL);
	if (err <= 0) {
		printfailure("giiOpen: expected return value > 0\n"
			     "actual return value: %i\n",
			     err);
		success = 0;
		goto do_detach;
	}
	
	err = giiClose(inp, GII_EV_TARGET_ALL);
	if (err != GGI_OK) {
		printfailure("giiClose: expected return value: %i\n"
			     "actual return value: %i\n",
			     GGI_OK, err);
		success = 0;
	}

 do_detach:
	err = giiDetach(inp);
	if(err != 0) {
		printfailure("giiDetach: expected return value: 0\n"
			     "actual return value: %i\n", err);
		success = 0;
	}
	
 do_del_stem:
	ggDelStem(inp);
	
 do_exit:
	err = giiExit();
	if (err != GGI_OK) {
		printfailure("giiExit: expected return value: 0\n"
			     "actual return value: %i\n", err);
		success = 0;
		goto do_return;
	}
	
 do_return:
	if (success)
		printsuccess();
	return;
}



int main(int argc, char * const argv[])
{
	parseopts(argc, argv);

	printdesc("Regression testsuite for libgii init/exit handling\n\n");

	testcase1("Check that giiInit() behaves as documented.");
	testcase2("Check that giiExit() behaves as documented.");
	testcase3("Check giiOpen()/giiClose() to open/close a default input.");
	
	printsummary();

	return 0;
}
