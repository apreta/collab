/* $Id: res_ops.c,v 1.12 2005/12/03 17:26:42 cegger Exp $
******************************************************************************

   LibGalloc: functions for _single_ resource manipulation

   Copyright (C) 2001 Brian S. Julin	[bri@tull.umassp.edu]
   Copyright (C) 2001-2003 Christoph Egger   [Christoph_Egger@t-online.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/internal/galloc.h>
#include <ggi/internal/galloc_debug.h>

#include <ggi/internal/galloc_int.h>

#include <string.h>

/* Docs for functions listed here are in the ggi/internal/galloc.h or
 * ggi/internal/galloc_ops.h headers, or in the API docs.
 */


int ggiGACopyProps(ggiGA_resource_handle from_handle,
		   ggiGA_resource_handle to_handle)
{
	LIB_ASSERT(from_handle != NULL, "from_handle == NULL");
	LIB_ASSERT(to_handle != NULL, "to_handle == NULL");
	LIB_ASSERT(from_handle->props != NULL, "from_handle has no properties to copy");
	LIB_ASSERT(!ggiGAIsTagHead(from_handle), "from_handle may not be a tag head");
	LIB_ASSERT(!ggiGAIsTagHead(to_handle), "to_handle may not be a tag head");

	if (from_handle->props == NULL) return GGI_EARGINVAL;

	if (to_handle->props == NULL) {
		to_handle->props = malloc(sizeof(struct ggiGA_resource_props));
		if (!to_handle->props) return GGI_ENOMEM;
	}	/* if */

	memcpy(to_handle->props, from_handle->props,
		sizeof(struct ggiGA_resource_props));
	return GALLOC_OK;
}	/* ggiGACopyProps */


int ggiGACopyPriv(ggiGA_resource_handle from_handle,
		  ggiGA_resource_handle to_handle)
{
	LIB_ASSERT(from_handle != NULL, "from_handle == NULL");
	LIB_ASSERT(to_handle != NULL, "to_handle == NULL");
	LIB_ASSERT(from_handle->priv != NULL, "from_handle has no properties to copy");
	LIB_ASSERT(!ggiGAIsTagHead(from_handle), "from_handle may not be a tag head");
	LIB_ASSERT(!ggiGAIsTagHead(to_handle), "to_handle may not be a tag head");

	if (from_handle->priv == NULL) return GGI_EARGINVAL;

	if (to_handle->priv == NULL) {
		to_handle->priv = malloc(from_handle->priv_size);
		if (!to_handle->priv) return GGI_ENOMEM;	
		to_handle->priv_size = from_handle->priv_size;
	}	/* if */
	if (to_handle->priv_size != from_handle->priv_size) {
		to_handle->priv = realloc(from_handle->priv,
					  from_handle->priv_size);
		if (!to_handle->priv) return GGI_ENOMEM;
		to_handle->priv_size = from_handle->priv_size;
	}	/* if */

	LIB_ASSERT(from_handle->priv_size == to_handle->priv_size,
		"properties of from_handle and to_handle have different sizes");
	memcpy(to_handle->priv, from_handle->priv, from_handle->priv_size);

	return GALLOC_OK;
}	/* ggiGACopyPriv */


int ggiGAClone(const ggiGA_resource_handle in, ggiGA_resource_handle *out)
{
	int rc = GALLOC_OK;
	ggiGA_resource_handle tmp = NULL;

	if (out == NULL) return GGI_EARGINVAL;

	if (in == out[0]) {
		/* don't copy yourself */
		return GALLOC_OK;
	}	/* if */

	if (ggiGAIsTagHead(in)) {
		DPRINT_CORE("calling ggiGATagHeadClone(%p, %p)\n",
			(void *)in, (void *)out);
		return ggiGATagHeadClone(in, out);
	}

	out[0] = tmp = calloc((size_t)1, sizeof(struct ggiGA_resource));
	if (!tmp) {
		rc = GGI_ENOMEM;
		goto err0;
	}	/* if */

	memcpy(tmp, in, sizeof(struct ggiGA_resource));
	RESLIST_NEXT(tmp) = NULL;
	tmp->priv = NULL;
	tmp->props = NULL;

	if (in->priv != NULL) {
		rc = ggiGACopyPriv(in, tmp);
		LIB_ASSERT(rc == GALLOC_OK, "Could not copy resource");
		if (rc != GALLOC_OK) goto err1;
	}	/* if */

	if (in->props != NULL) {
		rc = ggiGACopyProps(in, tmp);
		LIB_ASSERT(rc == GALLOC_OK, "Could not copy properties");
		if (rc != GALLOC_OK) goto err2;
	}	/* if */


	LIB_ASSERT(rc == GALLOC_OK, "we return success, although something went wrong");
	return rc;

err2:
	if (tmp->priv)  free(tmp->priv);
err1:
	free(tmp);
	out[0] = NULL;
err0:
	return rc;
}	/* ggiGAClone */


int ggiGACloneNoProps(const ggiGA_resource_handle in, ggiGA_resource_handle *out)
{
	ggiGA_resource_handle tmp = NULL;

	if (out == NULL) return GGI_EARGINVAL;

	if (in == out[0]) {
		/* Copying a resource into itself is a noop. */
		return GALLOC_OK;
	}	/* if */

	if (ggiGAIsTagHead(in)) {
		DPRINT_CORE("calling ggiGATagHeadClone(%p, %p)\n",
			(void *)in, (void *)out);
		return ggiGATagHeadClone(in, out);
	}

	out[0] = tmp = calloc((size_t)1, sizeof(struct ggiGA_resource));
	if (!tmp) return GGI_ENOMEM;

	memcpy(tmp, in, sizeof(struct ggiGA_resource));

	RESLIST_NEXT(tmp) = NULL;
	tmp->props = NULL;
	tmp->priv  = NULL;

	return GALLOC_OK;
}	/* ggiGACloneNoProps */


int ggiGACopy(const ggiGA_resource_handle in, ggiGA_resource_handle out)
{
	int rc = GALLOC_OK;
	ggiGA_resource_handle next = NULL;

	if (out == NULL) return GALLOC_EFAILED;

	if (in == out) {
		/* Copying a resource back into itself is a noop. */
		return GALLOC_OK;
	}	/* if */

	if (ggiGAIsTagHead(in)) {
		DPRINT_CORE("calling ggiGATagHeadCopy(%p, %p)\n",
			(void *)in, (void *)out);
		return ggiGATagHeadCopy(in, out);
	}

	/* Save pointer to next handle */
	next = RESLIST_NEXT(out);
	memcpy(out, in, sizeof(struct ggiGA_resource));

	/* Restore pointer to next handle */
	RESLIST_NEXT(out) = next;

	if (in->priv != NULL) {
		rc = ggiGACopyPriv(in, out);
		LIB_ASSERT(rc == GALLOC_OK, "Could not copy resource");
		if (rc != GALLOC_OK) goto err0;
	}	/* if */

	if (in->props != NULL) {
		rc = ggiGACopyProps(in, out);
		LIB_ASSERT(rc == GALLOC_OK, "Could not copy properties");
		if (rc != GALLOC_OK) goto err1;
	}	/* if */

	return GALLOC_OK;

err1:
	if (out->priv)  free(out->priv);
err0:
	return GGI_ENOMEM;
}	/* ggiGACopy */


/* Functions on resource + cap */

/* Check that the values in a GA_RT_FRAME resource are not ridiculous,
   including comparing to those in its resource cap if present. */
int ggiGAModeCapSanity(const ggiGA_resource_handle handle)
{
	LIB_ASSERT(handle != NULL, "handle == NULL");

	LIB_ASSERT (handle->priv != NULL, "A mode without a description");
	if (handle->priv == NULL) {
		return GALLOC_ESPECIFY;
	}	/* if */

	/* Do we have a cap? */
	if (!ggiGAIsCap(RESLIST_NEXT(handle))) {
		/* Nope. */
		return GALLOC_OK;
	}	/* if */

	LIB_ASSERT(RESLIST_NEXT(handle)->priv != NULL,
		"RESLIST_NEXT(handley)->priv == NULL");
	if (RESLIST_NEXT(handle)->priv == NULL) {
		return GALLOC_ESPECIFY;
	}	/* if */

	do {
		struct ggiGA_mode *res, *cap;

		res = (struct ggiGA_mode *)(handle->priv);
		cap = (struct ggiGA_mode *)(RESLIST_NEXT(handle)->priv);
		GA_CAPCMP_ASSERT(mode.visible.x, >=);
		GA_CAPCMP_ASSERT(mode.visible.y, >=);
		GA_CAPCMP_ASSERT(mode.virt.x, >=);
		GA_CAPCMP_ASSERT(mode.virt.y, >=);
		GA_CAPCMP_ASSERT(mode.size.x, >=);
		GA_CAPCMP_ASSERT(mode.size.y, >=);
		GA_CAPCMP_ASSERT(mode.frames, >=);

#ifdef DEBUG
		fprintf(stderr, "fixme: %s:%s:%d: TODO: db.*\n",
			DEBUG_INFO);
#endif
	} while(0);

	return GALLOC_OK;
}	/* ggiGAModeCapSanity */


/* Check that the values in a motor resource are not ridiculous,
 * including comparing to those in its resource cap if present. */
int ggiGAMotorCapSanity(const ggiGA_resource_handle res)
{
	ggiGA_resource_handle cap = NULL;

	/* If this was called on a tank, then we presume this is a
	 * motor-only resource, and the caller will want to return
	 * GALLOC_EUNAVAILABLE rather than GALLOC_EFAILED */
	LIB_ASSERT (ggiGAIsMotor(res), "resource must be a motor type");
	if (!ggiGAIsMotor(res)) return GALLOC_EUNAVAILABLE;

	LIB_ASSERT (res->props != NULL, "a resource without properties");
	if (res->props == NULL) return GALLOC_ESPECIFY;
	/* More to check here?  Fields that cannot be GGI_AUTO? */

	/* Now, do we have a cap? */
	if (!ggiGAIsCap(RESLIST_NEXT(res))) {
		/* Nope. */
		return GALLOC_OK;
	}	/* if */

	cap = RESLIST_NEXT(res);
	LIB_ASSERT (ggiGAIsMotor(cap), "cap resource must be a motor type");
	if (!ggiGAIsMotor(cap)) return GALLOC_EUNAVAILABLE;
	LIB_ASSERT (cap->props != NULL, "cap resource must have properties");
	if (cap->props == NULL) return GALLOC_ESPECIFY;
	
	LIB_ASSERT((res->priv == NULL) || (cap->priv != NULL),
		"both motor and caps must have target areas");
	if ((res->priv != NULL) && (cap->priv != NULL)) return GALLOC_EFAILED;
	
	GA_CAPCMP_ASSERT(props->qty, >=);
	GA_CAPCMP_ASSERT(props->sub.motor.pos_snap.x, <=);
	GA_CAPCMP_ASSERT(props->sub.motor.pos_snap.y, <=);
	GA_CAPCMP_ASSERT(props->sub.motor.size_snap.x, <=);
	GA_CAPCMP_ASSERT(props->sub.motor.size_snap.y, <=);
	GA_CAPCMP_ASSERT(props->sub.motor.grid_start.x, <=);
	GA_CAPCMP_ASSERT(props->sub.motor.grid_start.y, <=);
	GA_CAPCMP_ASSERT(props->sub.motor.grid_size.x, >=);
	GA_CAPCMP_ASSERT(props->sub.motor.grid_size.y, >=);
	GA_CAPCMP_ASSERT(props->sub.motor.mul_min.x, <=);
	GA_CAPCMP_ASSERT(props->sub.motor.mul_min.y, <=);
	GA_CAPCMP_ASSERT(props->sub.motor.mul_max.x, >=);
	GA_CAPCMP_ASSERT(props->sub.motor.mul_max.y, >=);
	GA_CAPCMP_ASSERT(props->sub.motor.div_min.x, <=);
	GA_CAPCMP_ASSERT(props->sub.motor.div_min.y, <=);
	GA_CAPCMP_ASSERT(props->sub.motor.div_max.x, >=);
	GA_CAPCMP_ASSERT(props->sub.motor.div_max.y, >=);
	return GALLOC_OK;
	
}	/* ggiGAMotorCapSanity */


/* Check that the values in a tank resource are not ridiculous,
 * including comparing to those in its resource cap if present.
 */
int ggiGATankCapSanity(const ggiGA_resource_handle res)
{
#ifdef DEBUG
	fprintf(stderr, "fixme: %s:%s:%d: not yet implemented\n",
		DEBUG_INFO);
#endif
	return GALLOC_OK;
}	/* ggiGATankCapSanity */


/* Check that the values in a carb resource are not ridiculous,
 * including comparing to those in its resource cap if present.
 */
int ggiGACarbCapSanity(const ggiGA_resource_handle res)
{
#ifdef DEBUG
	fprintf(stderr, "fixme: %s:%s:%d: not yet implemented\n",
		DEBUG_INFO);
#endif
	return GALLOC_OK;
}	/* ggiGACarbCapSanity */
