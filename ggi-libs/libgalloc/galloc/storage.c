/* $Id: storage.c,v 1.8 2007/12/20 23:43:06 cegger Exp $
******************************************************************************

   LibGAlloc: utility functions for storage related operations

   Copyright (C) 2001 Brian S. Julin	[bri@tull.umassp.edu]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/internal/galloc.h>
#include <ggi/internal/galloc_debug.h>
#include <ggi/internal/galloc_int.h>

#include <string.h>

/* Docs for functions listed here are in the ggi/internal/galloc.h or
 * ggi/internal/galloc_ops.h headers, or in the API docs.
 */


/* Functions on single resources. */


/* Fill out a mmutil rm_range_t based on a ggi_mode.   All frames
 * will be aligned at falign.  All lines will be aligned at lalign.
 * (note lalign is currently ignored until we have an rm that can 
 * deal with two levels of stride.)
 */
void ggiGA_mode2rm (ggi_mode *resmode, rm_range_t *rm, int falign, int lalign) 
{
	ggi_mode mode;

	memcpy(&mode, resmode, sizeof(ggi_mode));
	memset(rm, 0, sizeof(rm));

	/* We do not need to worry about math overflow;
	 * That's a higher layer's responsibility.
	 */
	rm->attribute.width = mode.virt.y * mode.virt.x;
	rm->attribute.width *= (GT_SIZE(mode.graphtype));
	rm->attribute.width += 7;
	rm->attribute.width /= 8;
	rm->attribute.width *= 8;

	rm->attribute.stride = rm->attribute.width;
	rm->attribute.height = mode.frames;

	rm->constraint.stride_align = falign;
	rm->constraint.start_align = falign;

	rm->constraint.start_min = 0;
	rm->constraint.start_max = 0;
}	/* ggiGA_mode2rm */


/* Fill out a mmutil rm_range_t based on an active ggi visual that
 * has directbuffer.  Note this only handles the common case of plb
 * buffers where inter-frame stride is constant and read==write.  
 * Anything more complex and the target will have to do this on its own.
 */
void ggiGA_visdb2rm (struct ggi_visual *vis, rm_range_t *rm, char *fb)
{
	memset(rm, 0, sizeof(rm));

	rm->attribute.width = LIBGGI_FB_SIZE(vis->mode);

	ggiFPrintMode (stderr, vis->mode);
	fprintf(stderr, "\n");

	LIB_ASSERT(LIBGGI_MODE(vis)->frames > 0, "1 frame is minimum");
	if (vis->mode->frames == 1) {
		rm->attribute.stride = rm->attribute.width;
	} else {
		rm->attribute.stride =
		  (char *)(LIBGGI_APPBUFS(vis)[1]->write) -
		  (char *)(LIBGGI_APPBUFS(vis)[0]->write);
	}	/* if */
	rm->attribute.height = vis->mode->frames;

	rm->constraint.stride_align = 1;
	rm->constraint.start_align = 1;

	rm->constraint.start_min = 
	  (char *)(LIBGGI_APPBUFS(vis)[0]->write) - fb;
	rm->constraint.start_max = rm->constraint.start_min;
        rm->attribute.flags = RANGE_START_MIN | RANGE_START_MAX;
}	/* ggiGA_visdb2rm */
