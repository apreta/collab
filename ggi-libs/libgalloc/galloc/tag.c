/* $Id: tag.c,v 1.13 2005/10/10 18:08:31 cegger Exp $
******************************************************************************

   LibGalloc: utility functions for tagged groups manipulation

   Copyright (C) 2001 Brian S. Julin	[bri@tull.umassp.edu]
   Copyright (C) 2001 Christoph Egger   [Christoph_Egger@t-online.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/internal/galloc.h>
#include <ggi/internal/galloc_int.h>
#include <ggi/internal/galloc_debug.h>

int ggiGASetTag(ggiGA_resource_handle handle, tag_t tag)
{
	LIB_ASSERT(handle != NULL, "handle == NULL");
	LIB_ASSERT((tag & GA_STATE_TAG_MASK) == tag, "invalid tag id");
	LIB_ASSERT(tag >= 0, "non-negative tag id is not allowed");

	handle->res_state |= (tag & GA_STATE_TAG_MASK);
	return GALLOC_OK;
}	/* ggiGASetTag */

int ggiGAGetTag(ggiGA_resource_handle handle)
{
	LIB_ASSERT(handle != NULL, "handle == NULL");

	return ggiGA_TAG(handle);
}	/* ggiGAGetTag */

tag_t ggiGAGetTagTail(ggiGA_resource_handle handle)
{
	LIB_ASSERT(handle != NULL, "handle == NULL");
	LIB_ASSERT(ggiGAIsTagHead(handle), "resource must be a tag head");

	return ((ggiGA_taghead_t) handle)->tag2;
}	/* ggiGAGetTag */

int ggiGASetTagTail(ggiGA_resource_handle handle, tag_t tag)
{
	LIB_ASSERT(handle != NULL, "handle == NULL");
	LIB_ASSERT(ggiGAIsTagHead(handle), "resource must be a tag head");
	LIB_ASSERT((tag & GA_STATE_TAG_MASK) == tag, "invalid tag id");
	LIB_ASSERT(tag >= 0, "non-negative tag id not allowed");

	((ggiGA_taghead_t) handle)->tag2 = tag;
	return GALLOC_OK;
}	/* ggiGAGetTag */

/* Returns non-zero when tags match. */
int ggiGACompareTag(ggiGA_resource_handle handle1,
		ggiGA_resource_handle handle2)
{
	if (handle1 == NULL)
		return 0;
	if (handle2 == NULL)
		return 0;

	if (ggiGA_TAG(handle1) != ggiGA_TAG(handle2))
		return 0;

	return 1;
}	/* ggiGACompareTag */


int ggiGASetAutoTag(ggiGA_resource_list reslist,
			ggiGA_resource_handle handle)
{
	ggiGA_resource_handle current = NULL;
	tag_t tag;
	int rc;

	LIB_ASSERT(handle != NULL, "handle == NULL");
	LIB_ASSERT(reslist != NULL, "reslist == NULL");

	rc = ggiGAFind(reslist, handle);
	if (rc != GALLOC_OK)
		return rc;

	rc = ggiGAGetTag(handle);
	if (rc)
		return rc;

	for (tag = 1; ((tag & GA_STATE_TAG_MASK) == tag); tag++) {
		RESLIST_FOREACH(current, reslist) {
			if (tag == ggiGA_TAG(current))
				break;
		}	/* foreach */
		if (current == NULL)
			break;
	}	/* for */

	if ((tag & GA_STATE_TAG_MASK) != tag) {
		/* no more tags available */
		return 0;
	}	/* if */
	rc = ggiGASetTag(handle, tag);
	LIB_ASSERT(rc == GALLOC_OK,
		"we return success, although ggiGASetTag() failed");

	return tag;
}	/* ggiGASetAutoTag */


/*
 * Attaches one resource to another using a tag group.  If onto is not
 * already in a tag group, a new tag group is autocreated to contain both
 * onto and handle.
 */
int ggiGATagOnto(ggiGA_resource_list reslist,
		 ggiGA_resource_handle onto,
		 ggiGA_resource_handle handle)
{
	int rc = GALLOC_OK;
	tag_t tag;

	tag = ggiGAGetTag(onto);

	/*
	 * The attached resource should not have a positive tag value, but if
	 * it does, we just override it.
	 */

	if (tag == 0) {
		tag = ggiGASetAutoTag(reslist, onto);
		LIB_ASSERT(tag > 0, "tag id must be positive");
		if (tag == 0) {
			/* no more tag-numbers available */
			rc = GALLOC_EFAILED;
			goto exit;
		}	/* if */
	}	/* if */
	rc = ggiGASetTag(handle, tag);
	goto exit;

exit:
	return rc;
}	/* ggiGATagOnto */


/*
 * Find last resource before "handle" in "reslist" with the same tag as
 * "handle" has.  This function has asserts that flag suspicious calls from
 * API functions, so if you need it for other purposes, dup it.
 */
ggiGA_resource_handle ggiGAFindPrevTag(ggiGA_resource_list reslist,
					ggiGA_resource_handle handle)
{
	ggiGA_resource_handle current = NULL;
	ggiGA_resource_handle result = NULL;

	LIB_ASSERT(handle != NULL, "handle == NULL");
	LIB_ASSERT(ggiGAFind(reslist, handle), "handle with same tag id not in resource list");

	RESLIST_FOREACH(current, reslist) {

		if (current == handle)
			break;

		if (ggiGACompareTag(current, handle)) {
			result = current;

			/*
			 * In order to have gotten this far, previous should
			 * have succeeded.
			 */
			LIB_ASSERT(!ggiGAIsFailed(result), "uh oh... something nasty happened");
		}	/* if */
	}	/* foreach */

	return result;

}	/* ggiGAFindPrevTag */


ggiGA_resource_handle ggiGAFindNextTag(ggiGA_resource_list reslist,
					ggiGA_resource_handle handle)
{
	ggiGA_resource_handle current = NULL;

	LIB_ASSERT(ggiGAFind(reslist, handle) == GALLOC_OK, "handle not in resource list");

	RESLIST_FOREACH(current, reslist) {
		if (current == handle)
			break;
	}	/* foreach */

	LIB_ASSERT(current != NULL, "handle not in resource list");
	if (current == NULL)
		return NULL;

	while (current != NULL) {
		current = RESLIST_NEXT(current);

		if (ggiGACompareTag(current, handle)) {
			LIB_ASSERT(!ggiGAIsFailed(current), "tag ids do not match");
			break;
		}	/* if */
	}	/* while */

	/*
	 * if there are no more tagged resources, then current == NULL here
	 */

	return current;
}	/* ggiGAFindNextTag */



ggiGA_resource_handle ggiGAFindFirstTag(ggiGA_resource_list reslist,
					ggiGA_resource_handle handle)
{
	ggiGA_resource_handle current = NULL;
	ggiGA_resource_handle first = NULL;

	if (reslist == NULL)
		return NULL;
	if (handle == NULL)
		return NULL;

	LIB_ASSERT(ggiGAFind(reslist, handle) == GALLOC_OK,
		"handle not in resource list");

	RESLIST_FOREACH(current, reslist) {
		if (ggiGAIsFailed(current))
			continue;

		if (ggiGACompareTag(current, handle)) {
			first = current;
			goto finish;
		}	/* if */
	}	/* foreach */

	LIB_ASSERT(current == NULL, "nothing found");
	LIB_ASSERT(first == NULL, "nothing found");
	LIB_ASSERT(!ggiGAIsFailed(first), "something failed");

finish:
	return first;
}	/* ggiGAFindFirstTag */


ggiGA_resource_handle ggiGAFindLastTag(ggiGA_resource_list reslist,
					ggiGA_resource_handle handle)
{
	ggiGA_resource_handle current = NULL;
	ggiGA_resource_handle last = NULL;

	if (reslist == NULL)
		return NULL;
	if (handle == NULL)
		return NULL;

	LIB_ASSERT(ggiGAFind(reslist, handle) == GALLOC_OK, "handle not in resource list");

	current = RESLIST_FIRST(reslist);
	while (current != NULL) {
		last = current;

		current = ggiGAFindNextTag(reslist, current);
	}	/* if */

	LIB_ASSERT(current == NULL, "nothing found");

	LIB_ASSERT(!ggiGAIsFailed(last), "something failed");
	return last;
}	/* ggiGAFindLastTag */


/*
 * Find the compound resource to which the first resource with the same type
 * and tag belongs.
 */
ggiGA_resource_handle ggiGAFindCompoundByTag(ggiGA_resource_list reslist,
						ggiGA_resource_handle handle)
{
	ggiGA_resource_handle current = NULL;
	ggiGA_resource_handle compound = NULL;

	LIB_ASSERT(reslist != NULL, "reslist == NULL");
	LIB_ASSERT(handle != NULL, "handle == NULL");

	RESLIST_FOREACH(current, reslist) {

		if (ggiGAIsCompoundHead(current)) {
			compound = current;

			/*
			 * It would be invalid for first resource not to be
			 * this.
			 */
			LIB_ASSERT(compound != NULL, "must be a compound head");
		}	/* if */
		if (ggiGACompareTag(current, handle)) {
			LIB_ASSERT(compound != NULL, "tag id do not match");
			break;
		}	/* if */
	}	/* while */

	return compound;

}	/* ggiGAFindCompoundByTag */
