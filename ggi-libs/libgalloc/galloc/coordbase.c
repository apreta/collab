/* $Id: coordbase.c,v 1.5 2007/12/20 23:43:06 cegger Exp $
******************************************************************************

   LibGalloc: Coordbase conversion functions

   Copyright (C) 2001-2003 Christoph Egger   [Christoph_Egger@t-online.de]
   Copyright (C) 2001 Brian S. Julin    [bri@calyx.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/internal/galloc.h>
#include <ggi/internal/galloc_debug.h>


/*******************************************************
 * "gray area" API
 */

int ggiGASubUnit2Unit(ggiGA_resource_handle handle,
                      ggi_coord subunit, ggi_coord *unit)
{
	ggi_coord div_max;

	LIB_ASSERT(handle != NULL, "handle == NULL");
	LIB_ASSERT(unit != NULL, "unit == NULL");

	if (!ggiGAIsMotor(handle)) return GALLOC_EFAILED;

	LIB_ASSERT(handle->props->sub.motor.div_max.x != 0, "Division by zero");
	LIB_ASSERT(handle->props->sub.motor.div_max.y != 0, "Division by zero");

	div_max = handle->props->sub.motor.div_max;

	unit->x = subunit.x / div_max.x;
	unit->y = subunit.y / div_max.y;

	return GALLOC_OK;
}	/* ggiGASubUnit2Unit */


int ggiGAUnit2SubUnit(ggiGA_resource_handle handle,
                      ggi_coord unit, ggi_coord *subunit)
{
	ggi_coord div_max;

	LIB_ASSERT(handle != NULL, "handle == NULL");
	LIB_ASSERT(subunit != NULL, "subunit == NULL");

	if (!ggiGAIsMotor(handle)) return GALLOC_EFAILED;

	LIB_ASSERT(handle->props->sub.motor.div_max.x != 0, "Division by zero");
	LIB_ASSERT(handle->props->sub.motor.div_max.y != 0, "Division by zero");

	div_max = handle->props->sub.motor.div_max;

	subunit->x = unit.x * div_max.x;
	subunit->y = unit.y * div_max.y;

	return GALLOC_OK;
}	/* ggiGAUnit2SubUnit */



int ggiGAPixel2Dot(struct gg_stem *stem, ggi_coord pixel, ggi_coord *dot)
{
	struct ggi_visual *vis;

	LIB_ASSERT(stem != NULL, "stem == NULL");
	LIB_ASSERT(dot != NULL, "dot == NULL");

	vis = GGI_VISUAL(stem);

	LIB_ASSERT(LIBGGI_MODE(vis)->dpp.x != 0, "Division by zero");
	LIB_ASSERT(LIBGGI_MODE(vis)->dpp.y != 0, "Division by zero");

	dot->x = pixel.x / LIBGGI_MODE(vis)->dpp.x;
	dot->y = pixel.y / LIBGGI_MODE(vis)->dpp.y;

	return GALLOC_OK;
}	/* ggiGAPixel2Dot */


int ggiGADot2Pixel(struct gg_stem *stem, ggi_coord dot, ggi_coord *pixel)
{
	struct ggi_visual *vis;

	LIB_ASSERT(stem != NULL, "stem == NULL");
	LIB_ASSERT(pixel != NULL, "pixel == NULL");

	vis = GGI_VISUAL(stem);

	LIB_ASSERT(LIBGGI_MODE(vis)->dpp.x != 0, "Division by zero");
	LIB_ASSERT(LIBGGI_MODE(vis)->dpp.y != 0, "Division by zero");

	pixel->x = LIBGGI_MODE(vis)->dpp.x * dot.x;
	pixel->y = LIBGGI_MODE(vis)->dpp.y * dot.y;

	return GALLOC_OK;
}	/* ggiGADot2Pixel */


/* Convert any nature measure to pixels and vice versa.
 */
int ggiGAConv2Pixel(struct gg_stem *stem, enum ggi_ll_coordbase cb,
		    ggi_coord coord, ggi_coord *pixel)
{
	struct ggi_visual *vis;
	float mmpp_x;	/* mm per pixel */
	float mmpp_y;	/* mm per pixel */
	ggi_coord size;

	LIB_ASSERT(stem != NULL, "stem == NULL");
	LIB_ASSERT(pixel != NULL, "pixel == NULL");

	vis = GGI_VISUAL(stem);

	/* We know, this size is always given in mm */
	size = LIBGGI_MODE(vis)->size;

	if ((size.x == 0) || (size.y == 0)) {
		/* Ooops! Unknown or invalid physical size!
		 * Can't support natural maesures. We fail!
		 */

		return GALLOC_EFAILED;
	}	/* if */

	mmpp_x = (float)(size.x / LIBGGI_MODE(vis)->visible.x);
	mmpp_y = (float)(size.y / LIBGGI_MODE(vis)->visible.y);


	switch (cb) {
	case LL_COORDBASE_MILLIMETER:
		pixel->x = (int)(mmpp_x * coord.x);
		pixel->y = (int)(mmpp_y * coord.y);
		break;

	case LL_COORDBASE_MICROMETER:
		{
		float tmp_x, tmp_y;

		/* convert um to mm */
		tmp_x = (float)(coord.x / 1000);
		tmp_y = (float)(coord.y / 1000);

		pixel->x = (int)(mmpp_x * tmp_x);
		pixel->y = (int)(mmpp_y * tmp_y);
		break;
		}

	case LL_COORDBASE_1_64_INCH:
		{
		float tmp_x, tmp_y;

		/* convert 1/64th inch to mm */
		tmp_x = (float)(coord.x / 0.396875);
		tmp_y = (float)(coord.y / 0.396875);

		pixel->x = (int)(mmpp_x * tmp_x);
		pixel->y = (int)(mmpp_y * tmp_y);
		break;
		}

	case LL_COORDBASE_DOT:

		return ggiGADot2Pixel(stem, coord, pixel);

	case LL_COORDBASE_PIXEL:

		pixel[0] = coord;
		break;

	default:
		/* Sorry, no supported meassure */
		return GALLOC_EFAILED;
	}	/* switch */

	return GALLOC_OK;
}	/* ggiGAConv2Pixel */


int ggiGAConvPixel2(struct gg_stem *stem, enum ggi_ll_coordbase cb,
		    ggi_coord pixel, ggi_coord *coord)
{
	struct ggi_visual *vis;
	float mmpp_x;	/* mm per pixel */
	float mmpp_y;	/* mm per pixel */
	ggi_coord size;

	LIB_ASSERT(stem != NULL, "stem == NULL");
	LIB_ASSERT(coord != NULL, "coord == NULL");

	vis = GGI_VISUAL(stem);

	/* We know, this size is always given in mm */
	size = LIBGGI_MODE(vis)->size;

	if ((size.x == 0) || (size.y == 0)) {
		/* Ooops! Unknown or invalid physical size!
		 * Can't support natural maesures. We fail!
		 */

		return GALLOC_EFAILED;
	}	/* if */

	mmpp_x = (float)(LIBGGI_MODE(vis)->visible.x / size.x);
	mmpp_y = (float)(LIBGGI_MODE(vis)->visible.y / size.y);


	switch (cb) {
	case LL_COORDBASE_MILLIMETER:
		coord->x = (int)(mmpp_x * pixel.x);
		coord->y = (int)(mmpp_y * pixel.y);
		break;

	case LL_COORDBASE_MICROMETER:
		coord->x = (int)(mmpp_x * pixel.x);
		coord->y = (int)(mmpp_y * pixel.y);

		/* convert mm to um */
		coord->x *= 1000;
		coord->y *= 1000;
		break;

	case LL_COORDBASE_1_64_INCH:
		{
		float tmp_x, tmp_y;

		/* convert mm to 1/64th inch */
		tmp_x = (float)(pixel.x * 0.396875);
		tmp_y = (float)(pixel.y * 0.396875);

		coord->x = (int)(mmpp_x * tmp_x);
		coord->y = (int)(mmpp_y * tmp_y);
		break;
		}

	case LL_COORDBASE_DOT:

		return ggiGAPixel2Dot(stem, pixel, coord);

	case LL_COORDBASE_PIXEL:

		coord[0] = pixel;
		break;

	default:
		/* Sorry, no natural meassure */
		return GALLOC_EFAILED;
	}	/* switch */

	return GALLOC_OK;
}	/* ggiGAConvPixel2 */


/* Convert any nature measure to pixels and vice versa.
 */
int ggiGAConv2Dot(ggi_visual_t vis, enum ggi_ll_coordbase cb,
		  ggi_coord coord, ggi_coord *dot)
{
	int rc;
	ggi_coord pixel;

	LIB_ASSERT(vis != NULL, "vis == NULL");
	LIB_ASSERT(dot != NULL, "dot == NULL");

	rc = ggiGAConv2Pixel(vis, cb, coord, &pixel);
	LIB_ASSERT(rc != GALLOC_OK, "Conversion to pixel failed");
	if (rc != GALLOC_OK) goto exit;

	rc = ggiGAPixel2Dot(vis, pixel, dot);
	LIB_ASSERT(rc != GALLOC_OK, "Conversion from pixel to dot failed");

exit:
	return rc;
}	/* ggiGAConv2Dot */


int ggiGAConvDot2(ggi_visual_t vis, enum ggi_ll_coordbase cb,
		  ggi_coord dot, ggi_coord *coord)
{
	int rc;
	ggi_coord pixel;

	LIB_ASSERT(vis != NULL, "vis == NULL");
	LIB_ASSERT(coord != NULL, "coord == NULL");

	rc = ggiGADot2Pixel(vis, dot, &pixel);
	LIB_ASSERT(rc != GALLOC_OK, "Conversion from dot to pixel failed");

	rc = ggiGAConvPixel2(vis, cb, pixel, coord);
	LIB_ASSERT(rc != GALLOC_OK, "Conversion from pixel failed");

	return rc;
}	/* ggiGAConvDot2 */
