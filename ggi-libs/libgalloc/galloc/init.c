/* $Id: init.c,v 1.38 2007/12/20 23:43:06 cegger Exp $
******************************************************************************

   LibGAlloc: extension initialization.

   Copyright (C) 2001 Christoph Egger   [Christoph_Egger@t-online.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include <stdio.h>
#include <string.h>

#include "config.h"
#include <ggi/internal/galloc.h>
#include <ggi/internal/galloc_debug.h>
#include <ggi/gg.h>

#include <ggi/internal/gg_replace.h>	/* for snprintf() */

/* libgalloc api */

static ggfunc_api_op_init ggiGA_init;
static ggfunc_api_op_exit ggiGA_exit;
static ggfunc_api_op_attach ggiGA_attach;
static ggfunc_api_op_detach ggiGA_detach;
/* static ggfunc_api_op_getenv ggiGA_getenv; */
static ggfunc_api_op_plug ggiGA_plug;
static ggfunc_api_op_unplug ggiGA_unplug;

static struct gg_api_ops ggiGA_ops = {
	ggiGA_init,
	ggiGA_exit,
	ggiGA_attach,
	ggiGA_detach,
	NULL /* ggiGA_getenv */,
	ggiGA_plug,
	ggiGA_unplug
};

static struct gg_api ggiGA = GG_API_INIT("GAlloc", 1, 0, &ggiGA_ops);

struct gg_api * libgalloc = &ggiGA;

#ifdef HAVE_CONFFILE
static char      _galloc_confstub[512]	= GALLOCCONFDIR;
static char     *_galloc_confdir	= _galloc_confstub + GALLOCTAGLEN;
#else
extern const char const *_ggiGAbuiltinconf[];
#endif /* HAVE_CONFFILE */

static struct gg_observer *observer;

#ifdef PIC
/* Dynamic version */
#define CONF_OFFSET (0)
#define CONF_SIZE (strlen(confdir) + 1 + strlen(GALLOCCONFFILE) + 1)
#define CONF_FORMAT "%s/%s"
#else /* PIC */
/* Static version, with a config stub to disable dynamic modules */
#define CONF_STUB ".set ignore-dynamic-modules\n.require "
#define CONF_OFFSET (40)
#define CONF_SIZE (CONF_OFFSET + strlen(CONF_STUB) + \
                   strlen(confdir) + 1 + strlen(GALLOCCONFFILE) + 2)
#define CONF_FORMAT CONF_STUB "%s/%s\n"
#endif /* PIC */

/*
 * Returns the directory where global config files are kept
 */

const char *ggiGAGetConfDir(void)
{
#ifdef HAVE_CONFFILE
#if defined(__WIN32__) && !defined(__CYGWIN__)
	/* On Win32 we allow overriding of the compiled in path. */
	const char *envdir = getenv("GGI_CONFDIR");
	if (envdir) return envdir;
#endif
	return _gallocconfdir;
#else /* HAVE_CONFFILE */
	return NULL;
#endif /* HAVE_CONFFILE */
}

/* Dummy function which returns GGI_ENOFUNC 
   We use this to reset the function pointers */
static int dummyfunc(void)
{
	DPRINT_CORE("dummyfunc() of LibGAlloc called\n");
	return GGI_ENOFUNC;
}	/* dummyfunc */

static struct galloc_ops ga_noop = {
	(gallocfunc_check *)dummyfunc,
	(gallocfunc_set *)dummyfunc,
	(gallocfunc_release *)dummyfunc,
	(gallocfunc_anprintf *)dummyfunc,
	(gallocfunc_mode *)dummyfunc,
	(gallocfunc_checkIfShareable *)dummyfunc,

	(ggiGA_reset_checkstate *)dummyfunc,
	(ggiGA_migrate_checkstate *)dummyfunc,

	{ (ggiGA_reslist_callback *)dummyfunc,
	  (ggiGA_reslist_callback *)dummyfunc,
	  (ggiGA_reslist_callback *)dummyfunc,
	  (ggiGA_reslist_callback *)dummyfunc,
	  (ggiGA_reslist_callback *)dummyfunc,
	  (ggiGA_reslist_callback *)dummyfunc,
	  (ggiGA_reslist_callback *)dummyfunc,
	  (ggiGA_reslist_callback *)dummyfunc,
	  (ggiGA_reslist_callback *)dummyfunc
	},
};

static int
_galloc_reconfigure(struct galloc_context *ctxt)
{
	int i;
	char api[GGI_MAX_APILEN], args[GGI_MAX_APILEN];
	struct gg_stem *stem;
	struct gg_plugin *self;

	DPRINT_CORE("_galloc_reconfigure ctxt=%p\n", ctxt);

	stem = ctxt->plugin.stem;

	if (ctxt->plugin.stem) {
		/* we already have an implementation */
		DPRINT("Closing current implementation: %s\n",
			ctxt->plugin.module->name);
		ggClosePlugin(&ctxt->plugin);
	}

	for (i = 0; ggiGetAPI(stem, i, api, args) == 0; i++) {
		ggstrlcat(api, "-galloc", sizeof(api));
		DPRINT_LIBS("Trying #%d: %s(%s)\n", i, api, args);

		self = ggPlugModule(libgalloc, stem, api, args, NULL);
		if (self) {
			LIB_ASSERT(self = &ctxt->plugin, "plugin mismatch\n");
			return GGI_OK;
		}
	}

	DPRINT_CORE("! _galloc_reconfigure ctxt=%p FAILED!\n", ctxt);
	return GGI_ENOFUNC;
}

static int
_galloc_setup_for_ggi_visual(struct galloc_context *ctxt, struct ggi_visual *vis)
{
	DPRINT_CORE("_galloc_setup_for_ggi_visual ctxt=%p vis=%p\n", ctxt, vis);
	LIB_ASSERT(ctxt != NULL, "NULL context\n");
	LIB_ASSERT(vis != NULL, "NULL visual\n");
	LIB_ASSERT(ctxt->vis == NULL, "GAlloc context already bound to a visual\n");
	ctxt->vis = vis;
	return GGI_OK;
}

/* */
static int
ggiGA_attach(struct gg_api *api, struct gg_stem *stem)
{
	struct gg_api *_api;
	struct galloc_context *ctxt;

	GALLOC_PRIV(stem) = ctxt = calloc(1, sizeof(*ctxt));
	if (!ctxt)
		return GGI_ENOMEM;

	/*
	 * We are not really a plugin yet, until we are plugged to an
	 * implementation.
	 */

	ctxt->plugin.channel = ggNewChannel(ctxt, NULL);
	ctxt->plugin.stem = stem;
	ctxt->ops = &ga_noop;

	/*
	 * XXX Should libgalloc not attaich ggi itself, but it
	 * should just react on ggi attachment and mode changes.
	 */
#if 0
	rc = ggiAttach(stem);
	if (rc < 0) {
		ggDelChannel(ctxt->plugin.channel);
		GALLOC_PRIV(stem) = NULL;
		free(ctxt);
		return rc;
	}
#endif

	_api = ggGetAPIByName("ggi");
	if (_api != NULL && STEM_HAS_API(stem, _api) &&
	   GGI_VISUAL(stem) != NULL)
	{
		/*
		 * Libggi is already attached when libgalloc was attached.
		 * If the visual is already opened, we must (re)open the
		 * galloc implementation.
		 */
		DPRINT("GGI already attached and opened\n");
		/* XXX what if that fails? do we report it? */
		_galloc_setup_for_ggi_visual(ctxt, GGI_VISUAL(stem));
		_galloc_reconfigure(ctxt);
	}

	return GGI_OK;
}


static void
_galloc_tear_down_ggi_visual(struct galloc_context *ctxt, struct ggi_visual *vis)
{
	DPRINT_CORE("_galloc_tear_down_ggi_visual ctxt=%p vis=%p\n", ctxt, vis);

	LIB_ASSERT(ctxt != NULL, "NULL context\n");
	LIB_ASSERT(ctxt->vis == vis, "wrong visual\n");

	if (vis == NULL)
		return;

	if (ctxt->plugin.module)
		ggClosePlugin(&ctxt->plugin);

	ctxt->vis = NULL;
}

/* */
static void
ggiGA_detach(struct gg_api *api, struct gg_stem *stem)
{
	struct galloc_context *ctxt;

	DPRINT_CORE("ggiGA_detach(%p, %p)\n", api, stem);

	ctxt = GALLOC_CONTEXT(stem);

	if (ctxt->vis)
		_galloc_tear_down_ggi_visual(ctxt, ctxt->vis);

	free(ctxt);

	GALLOC_PRIV(stem) = NULL;
}

static int
_galloc_observe_visual(void *arg, uint32_t msg, void *data)
{
	struct galloc_context *ctxt;
	struct ggi_visual *vis;

	ctxt = arg;

	switch (msg) {
	case GGI_OBSERVE_VISUAL_OPENED:
		DPRINT("_galloc_observe_visual: visual opened\n");
		vis = data;
		_galloc_setup_for_ggi_visual(ctxt, vis);
		_galloc_reconfigure(ctxt);
		break;

	case GGI_OBSERVE_VISUAL_CLOSED:
		DPRINT("_galloc_observe_visual: visual closed %p\n", data);
		vis = data;
		_galloc_tear_down_ggi_visual(ctxt, vis);
		break;

	case GGI_OBSERVE_VISUAL_APILIST:
		DPRINT("_galloc_observe_visual: visual apilist change\n");
		_galloc_reconfigure(ctxt);
		break;
	}
	return 0;
}


static int
_galloc_observe_libggi(void *arg, uint32_t msg, void *data)
{
	struct gg_message_api_stem *apistem;
	struct ggi_visual *vis;

	switch (msg) {
	case GG_MESSAGE_API_ATTACH:
		apistem = data;
		/*
		 * apistem->api is necessarily libggi, since the observer
		 * was registered on libggi->channel
		 */
		DPRINT("_galloc_observe_libggi: \"%s\" attached to stem %p\n",
			apistem->api->name,
			apistem->stem);
		if (STEM_HAS_API(apistem->stem, libgalloc)) {
			_galloc_setup_for_ggi_visual(
				GALLOC_CONTEXT(apistem->stem),
				GGI_VISUAL(apistem->stem));
			_galloc_reconfigure(GALLOC_CONTEXT(apistem->stem));
		}
		break;

	case GG_MESSAGE_API_DETACH:
		apistem = data;
		DPRINT("_galloc_observe_libggi: \"%s\" detached from stem %p\n",
			apistem->api->name,
			apistem->stem);
		if (STEM_HAS_API(apistem->stem, libgalloc)) {
			_galloc_tear_down_ggi_visual(
				GALLOC_CONTEXT(apistem->stem),
				GGI_VISUAL(apistem->stem));
		}
		break;

	case GG_MESSAGE_API_EXIT:
		/* We don't care because we must have been detached already */
		break;

	case GGI_OBSERVE_VISUAL_OPENED:
	case GGI_OBSERVE_VISUAL_CLOSED:
	case GGI_OBSERVE_VISUAL_APILIST:
		/*
		 * XXX Should these messages really be posted to this
		 * channel?  I think not; clarify this in libggi. For
		 * now, we do as if the observer was installed on the
		 * ggi attachment.
		 */
		vis = data;
		if (STEM_HAS_API(vis->instance.stem, libgalloc))
			_galloc_observe_visual(
				GALLOC_CONTEXT(vis->instance.stem),
				msg, vis);
		break;
	}

	return GGI_OK;
}

static int
ggiGA_unplug(struct gg_api *api, struct gg_plugin *plugin)
{
	struct galloc_context *ctxt;
	struct galloc_module_context *module;

	DPRINT_CORE("ggiGA_unplug(%p)\n", plugin);

	LIB_ASSERT(plugin->module->klass == GALLOC_MODULE_CONTEXT, "bad module!");
	module = (struct galloc_module_context *)plugin->module;

	ctxt = (struct galloc_context *)plugin;

	if (ctxt->flags & GALLOC_CONTEXTFLAG_CLOSING) {
		DPRINT_CORE("! plugin is already being closed.\n");
		return GGI_EBUSY;
	}

	/* forbid further registration on this module */
	ctxt->flags |= GALLOC_CONTEXTFLAG_CLOSING;

	if (module->exit)
		module->exit(ctxt);

	ctxt->ops = &ga_noop;
	ctxt->plugin.module = NULL;

	return GGI_OK;
}

/* */
static int
ggiGA_plug(struct gg_api *api, struct gg_module *_module,
	    struct gg_stem *stem, 
	    const char *argstr,
	    void *argptr,
	    struct gg_instance **res)
{
	struct galloc_module_context *module;
	struct galloc_context *ctxt;
	int err;

	LIB_ASSERT(_module->klass = GALLOC_MODULE_CONTEXT, "already plugged!");
	module = (struct galloc_module_context *)_module;

	ctxt = GALLOC_CONTEXT(stem);

	LIB_ASSERT(ctxt->plugin.module == NULL, "already plugged!");

	ctxt->plugin.module = _module;
	err = module->init(ctxt, _module->name, argstr, argptr);
	if (err < 0) {
		DPRINT_CORE("! init failed\n");
		ctxt->ops = &ga_noop;
		ctxt->plugin.module = NULL;
		return err;
	}
	*res = &ctxt->plugin;

	return GGI_OK;
}


void _ggigallocInitBuiltins(void);
void _ggigallocExitBuiltins(void);

/* Initialize the extension
 */
int ggiGAInit(void)
{
	return ggInitAPI(libgalloc);
}


/* Interface initialization */
static int
ggiGA_init(struct gg_api *api)
{
	int err;
	const char *str;
#ifdef HAVE_CONFFILE
	const char *confdir;
	char *conffile;
#endif

	str = getenv("GA_DEBUGSYNC");
	if (str == NULL) str = getenv("GALLOC_DEBUGSYNC");
	if (str != NULL) {
		_gallocDebug |= DEBUG_SYNC;
	}	/* if */

	str = getenv("GA_DEBUG");
	if (str == NULL) str = getenv("GALLOC_DEBUG");
	if (str != NULL) {
		_gallocDebug |= atoi(str) & DEBUG_ALL;
		DPRINT_CORE("%s Debugging=%d\n",
				DEBUG_ISSYNC ? "sync" : "async",
				_gallocDebug);
	}	/* if */

#ifdef HAVE_CONFFILE
	confdir = ggiGAGetConfDir();
	
	/* two extra bytes needed. One for the slash and one for the
	 * terminator (\0)
	 */
	conffile = malloc(CONF_SIZE);
	if (!conffile) {
		fprintf(stderr, "LibGAlloc: unable to allocate memory for config filename.\n");
		err = GGI_ENOMEM;
		goto err0;
	}
#ifndef PIC
	snprintf(conffile, CONF_OFFSET, "string@%p", conffile + CONF_OFFSET);
#endif
	snprintf(conffile + CONF_OFFSET, CONF_SIZE - CONF_OFFSET,
		CONF_FORMAT, confdir, GALLOCCONFFILE);

	err = ggLoadConfig(conffile, &api->config);
	free(conffile);
	if (err != GGI_OK) {
		fprintf(stderr,"LibGAlloc: couldn't open %s.\n",
			conffile);
		goto err0;
	}
#else /* HAVE_CONFFILE */
	do {
		char arrayconf[40];
		snprintf(arrayconf, sizeof(arrayconf),
			"array@%p", (const void *)_ggiGAbuiltinconf);
		err = ggLoadConfig(arrayconf, &api->config);
		if (err != GGI_OK) {
			fprintf(stderr, "LibGAlloc: fatal error - "
					"could not load builtin config\n");
			goto err1;
		}
	} while (0);
#endif /* HAVE_CONFFILE */

	ggiInit();
	observer = ggObserve(libggi->channel, _galloc_observe_libggi, NULL);

	_ggigallocInitBuiltins();

	return GGI_OK;

#ifndef HAVE_CONFFILE
err1:
	ggFreeConfig(api->config);	
	api->config = NULL;
#else
err0:
#endif
	return err;
} /* ggiGA_init */

int ggiGAExit(void)
{
	return ggExitAPI(libgalloc);
}

/* Bring down interface */
static void
ggiGA_exit(struct gg_api *api)
{
	_ggigallocExitBuiltins();

	ggDelObserver(observer);
	ggiExit();
	ggFreeConfig(api->config);

	api->config = NULL;
	return;
}

/* Attach the extension to a visual
 */
int ggiGAAttach(struct gg_stem *stem)
{
	int rc;

	DPRINT_CORE("ggiGAAttach(%p) called\n", stem);

	rc = ggAttach(libgalloc, stem);
	if (rc < 0) {
		DPRINT("ggAttach() failed with error: %i!\n", rc);
		goto err0;
	}	/* if */

	if (rc == 0) {
		/* We are actually creating the primary instance. */
#if 0
		memset(LIBGGI_GALLOCEXT(vis), 0, sizeof(gallocpriv));
#endif
		GALLOC_CONTEXT(stem)->dummy = (void *)dummyfunc;
		GALLOC_CONTEXT(stem)->priv = NULL;
		GA_RESLIST(stem) = NULL;
	}	/* if */

err0:
	return rc;
}

/* Detach the extension from a visual
 */
int ggiGADetach(struct gg_stem *stem)
{
	int rc;
	ggiGA_resource_list cur_reslist = NULL;

	DPRINT_CORE("ggiGADetach(%p) called\n", stem);

	cur_reslist = GA_RESLIST(stem);

	rc = ggDetach(libgalloc, stem);
	if (rc != 0) return rc;

	/* Free the currently active request structure
	 * if not already.
	 */
	if (cur_reslist != NULL) {
		ggiGADestroyList(&cur_reslist);
	}	/* if */

	return rc;
}


/*
static const char*
ggiGA_getenv(struct gg_api *, const char *)
{}
*/

#define COMPILING_GALLOC_C
#include <ggi/ga_prop.h>
