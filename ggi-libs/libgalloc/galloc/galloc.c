/* $Id: galloc.c,v 1.68 2005/10/12 10:23:43 cegger Exp $
******************************************************************************

   LibGAlloc: plain functions

   Copyright (C) 2001 Christoph Egger   [Christoph_Egger@t-online.de]
   Copyright (C) 2001 Brian S. Julin    [bri@calyx.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/internal/galloc.h>
#include <ggi/internal/galloc_debug.h>
#include <ggi/internal/galloc_int.h>

#include <string.h>

/* "external" API */

int ggiGAAdd(ggiGA_resource_list *reqlist, 
	     struct ggiGA_resource_props *props, 
	     ggiGA_resource_type rt, ggiGA_resource_handle *handle)
{
	int rc = GALLOC_OK;
	ggiGA_resource_handle result;

	if (reqlist == NULL) return GGI_EARGINVAL;
	if (props == NULL) return GGI_EARGINVAL;

	APP_ASSERT(reqlist[0] != NULL, "request list has invalid list header");

	result = ggiGACopyIntoResource(props, NULL, (size_t)0);
	if (result == NULL) goto err1;

	result->res_type = rt;	

	RESLIST_NEXT(result) = NULL;
	rc = ggiGAAppend(result, reqlist, 0);

	if (handle != NULL) handle[0] = result;

	LIB_ASSERT(rc == GALLOC_OK, "Could not create result list");

	return GALLOC_OK;

 err1:
	if (handle != NULL) handle[0] = NULL;
	return GGI_ENOMEM;
}	/* ggiGAAdd */


int ggiGAAddMode(ggi_visual_t vis, ggiGA_resource_list *reqlist, 
		 ggi_mode *mode, ggi_directbuffer *db,
		 ggiGA_resource_handle *handle)
{
	int rc = GALLOC_OK;
	ggiGA_resource_handle result;

	if (reqlist == NULL) return GGI_EARGINVAL;
	if (mode == NULL) return GGI_EARGINVAL;


	if (reqlist[0] == NULL) {
		reqlist[0] = ggiGACreateList();
		if (reqlist[0] == NULL) return GGI_ENOMEM;
	}
	LIB_ASSERT(reqlist[0] != NULL, "request list has invalid list header");

	result = calloc((size_t)1, sizeof(struct ggiGA_resource));
	if (result == NULL) goto err0;

	result->priv = calloc((size_t)1, sizeof(struct ggiGA_mode));
	if (result == NULL) goto err1;

	result->priv_size = sizeof(struct ggiGA_mode);
	RESLIST_NEXT(result) = NULL;

	/* GA_RT_FRAME resources don't have a properties structure. */
	result->props = NULL;

	memcpy(&(((struct ggiGA_mode *)result->priv)->mode), mode, 
		sizeof(ggi_mode));

	if (db != NULL) {
		memcpy(&(((struct ggiGA_mode *)result->priv)->db), db, 
			sizeof(ggi_directbuffer));
	}	/* if */

	ggiGA_Mode(vis, &result);

	rc = ggiGAAppend(result, reqlist, 0);
	LIB_ASSERT(rc == GALLOC_OK, "Could not create result list");

	if (handle != NULL) handle[0] = result;

	return rc;

err1:
	free(result);
err0:
	if (handle != NULL) handle[0] = NULL;
	return GGI_ENOMEM;
}	/* ggiGAAddMode */



/* These are just rips from the corresponding GGI core Set* functions. */
int ggiGAAddTextMode(ggi_visual_t vis, ggiGA_resource_list *reqlist,
		     int cols,int rows,
		     int vcols,int vrows,
		     int fontsizex,int fontsizey,
		     ggi_graphtype type,
		     ggiGA_resource_handle *handle) 
{
	ggi_mode mode;

	mode.frames	= GGI_AUTO;
	mode.visible.x	= cols;
	mode.visible.y	= rows;
	mode.virt.x	= vcols;
	mode.virt.y	= vrows;
	mode.size.x	= mode.size.y = GGI_AUTO;
	mode.graphtype	= type;
	mode.dpp.x	= fontsizex;
	mode.dpp.y	= fontsizey;
        
	return ggiGAAddMode(vis, reqlist, &mode, NULL, handle);
}	/* ggiGAAddTextMode */


int ggiGAAddGraphMode(ggi_visual_t vis, ggiGA_resource_list *reqlist,
		     int xsize,int ysize,
		     int xvirtual,int yvirtual,ggi_graphtype type,
		     ggiGA_resource_handle *handle)
{
	ggi_mode mode;
        
	mode.frames	= GGI_AUTO;
	mode.visible.x	= xsize;
	mode.visible.y	= ysize;
	mode.virt.x	= xvirtual;
	mode.virt.y	= yvirtual;
	mode.size.x	= mode.size.y = GGI_AUTO;
	mode.graphtype	= type;
	mode.dpp.x	= mode.dpp.y = GGI_AUTO;

	return ggiGAAddMode(vis, reqlist, &mode, NULL, handle);
}	/* ggiGAAddGraphMode */


int ggiGAAddSimpleMode(ggi_visual_t vis, ggiGA_resource_list *reqlist,
		       int xsize, int ysize, 
		       int frames,
		       ggi_graphtype type,
		       ggiGA_resource_handle *handle) 
{
	ggi_mode mode;
        
	mode.frames	= frames;
	mode.visible.x	= xsize;
	mode.visible.y	= ysize;
	mode.virt.x	= mode.virt.y = GGI_AUTO;
	mode.size.x	= mode.size.y = GGI_AUTO;
	mode.graphtype	= type;
	mode.dpp.x	= mode.dpp.y = GGI_AUTO;

	return ggiGAAddMode(vis, reqlist, &mode, NULL, handle);
}	/* ggiGAAddSimpleMode */


int ggiGAClearMotorProperties(struct ggiGA_resource_props *prop)
{
	if (prop == NULL) return GGI_EARGINVAL;

	memset(prop, 0, sizeof(*prop));

	/* Set useful default flags here */
	GA_FLAG(prop) = (LL_COORDBASE_UNITS & GA_FLAG_UNIT_MASK);
	GA_FLAG(prop) |= (LL_COORDBASE_PIXEL & GA_FLAG_COORDBASE_MASK);

	/* Set useful default flags here */
	prop->qty = 1;
	prop->sub.motor.mul_max.x = 1;
	prop->sub.motor.mul_min.x = 1;
	prop->sub.motor.div_max.x = 1;
	prop->sub.motor.div_min.x = 1;
	prop->sub.motor.mul_max.y = 1;
	prop->sub.motor.mul_min.y = 1;
	prop->sub.motor.div_max.y = 1;
	prop->sub.motor.div_min.y = 1;

	return GALLOC_OK;
}	/* ggiGAClearMotorProperties */


int ggiGAClearCarbProperties(struct ggiGA_resource_props *prop)
{
	if (prop == NULL) return GGI_EARGINVAL;

	memset(prop, 0, sizeof(*prop));

	/* Set useful default flags here */
	prop->qty = 1;

	return GALLOC_OK;
}	/* ggiGAClearCarbProperties */


int ggiGAClearTankProperties(struct ggiGA_resource_props *prop)
{
	if (prop == NULL) return GGI_EARGINVAL;

	memset(prop, 0, sizeof(*prop));

	/* Set useful default flags here */
	GA_FLAG(prop) = (GA_FLAG_STEREO_NOT);

	prop->qty = 1;

	prop->storage_share |= GA_SHARE_COPYONWRITE;

	return GALLOC_OK;
}	/* ggiGAClearTankProperties */


struct ggiGA_resource_props *ggiGAGetProperties(ggiGA_resource_handle handle)
{
	if (handle == NULL) {
		return NULL;
	}	/* if */

	return handle->props;
}	/* ggiGAGetProperties */


int ggiGAIsFailed(ggiGA_resource_handle handle)
{
	int state;

	if (handle == NULL) return GGI_EARGINVAL;

	state = (handle->res_state & GA_STATE_FAILED);

	return ((state == GA_STATE_FAILED) ? 1 : 0);
}	/* ggiGAIsFailed */


int ggiGAIsModified(ggiGA_resource_handle handle)
{
	int state;

	if (handle == NULL) return GGI_EARGINVAL;

	state = (handle->res_state & GA_STATE_MODIFIED);

	return ((state == GA_STATE_MODIFIED) ? 1 : 0);
}	/* ggiGAIsModified */


int ggiGAIsMotor(ggiGA_resource_handle handle)
{
	unsigned int type;

	if (handle == NULL) return GGI_EARGINVAL;

	type = (handle->res_type & GA_RT_MOTOR);

	return ((type == GA_RT_MOTOR) ? 1 : 0);
}	/* ggiGAIsMotor */


int ggiGAIsCarb(ggiGA_resource_handle handle)
{
	unsigned int type;

	if (handle == NULL) return GGI_EARGINVAL;

	type = (handle->res_type & GA_RT_CARB);

	return ((type == GA_RT_CARB) ? 1 : 0);
}	/* ggiGAIsCarb */


int ggiGAIsTank(ggiGA_resource_handle handle)
{
	unsigned int type;

	if (handle == NULL) return GGI_EARGINVAL;

	type = (handle->res_type & (GA_RT_MOTOR | GA_RT_CARB));

	return ((type == 0) ? 1 : 0);
}	/* ggiGAIsTank */


ggi_visual_t ggiGAGetVisual(ggiGA_resource_handle handle)
{
	if (handle == NULL) {
		return NULL;
	}	/* if */

	/* When this assertion fails, then
	 * there is either a bug or that resource failed
	 * or whatever...
	 */
	LIB_ASSERT(handle->vis != NULL, "Here's a bug or a resource failed...");
	return handle->vis;
}	/* ggiGAGetVisual */




ggiGA_resource_handle ggiGAMotorGetTank(ggiGA_resource_list list,
				ggiGA_resource_handle motor,
				ggiGA_resource_handle tank)
{
	int tag_m, tag_c;
	ggiGA_resource_handle current = NULL;
	ggiGA_resource_handle carb = NULL;


	LIB_ASSERT(list != NULL, "list == NULL");
	LIB_ASSERT(motor != NULL, "motor == NULL");

	tag_m = ggiGAGetTag(motor);
	/* if this fails, then the motor isn't tagged, which don't have to be */
	LIB_ASSERT(tag_m != 0, "motor is not tagged, which do not have to be!");

	/* A motor is always before the tank */
	current = RESLIST_NEXT(motor);
	while (current != NULL) {

		LIB_ASSERT(ggiGAIsTank(current), "This must be a tank resource");

		carb = ggiGATankGetCarb(list, current, NULL);
		if (carb != NULL) {
			tag_c = ggiGAGetTag(carb);

			if (tag_c == tag_m) return current;
		}	/* if */

		current = RESLIST_NEXT(current);
	}	/* while */


	/* motor has no tanks */

	return NULL;
}	/* ggiGAMotorGetTank */



ggiGA_resource_handle ggiGATankGetCarb(ggiGA_resource_list list,
				ggiGA_resource_handle tank,
				ggiGA_resource_handle carb)
{
	ggiGA_resource_handle tmp = NULL;

	LIB_ASSERT(list != NULL, "list == NULL");
	LIB_ASSERT(tank != NULL, "tank == NULL");

	if (carb == NULL) {
		tmp = ggiGAGetNextSkipCap(tank);
	} else {
		LIB_ASSERT(ggiGAIsCarb(carb), "this resource must be a carb type");

		tmp = RESLIST_NEXT(carb);
		if (ggiGAIsCarb(tmp)) {
			return tmp;
		}	/* if */
	}	/* if */

	return NULL;
}	/* ggiGATankGetCarb */


ggi_mode *ggiGAGetGGIMode(ggiGA_resource_handle handle)
{
	if (handle == NULL) {
		/* handle not valid! */
		return NULL;
	}	/* if */

	if (ggiGA_TYPE(handle) != GA_RT_FRAME) {
		return NULL;
	}	/* if */

	LIB_ASSERT (handle->priv, "handle->priv not allocated");
	LIB_ASSERT (handle->priv_size == sizeof(struct ggiGA_mode), "handle->priv has invalid size");

	return (&(((struct ggiGA_mode *)(handle->priv))->mode));
}	/* ggiGAGetGGIMode */


ggi_directbuffer *ggiGAGetGGIDB(ggiGA_resource_handle handle)
{
	if (handle == NULL) {
		/* handle not valid! */
		return NULL;
	}	/* if */

	if (ggiGA_TYPE(handle) != GA_RT_FRAME) {
		return NULL;
	}	/* if */

	LIB_ASSERT (handle->priv, "handle->priv not allocated");
	LIB_ASSERT (handle->priv_size == sizeof(struct ggiGA_mode), "handle->priv has invalid size");

	return (&(((struct ggiGA_mode *)(handle->priv))->db));
}	/* ggiGAGetGGIDB */


ggiGA_resource_handle ggiGAHandle(ggiGA_resource_list reslist,
				  ggiGA_resource_list reqlist,
				  ggiGA_resource_handle reqhandle)
{
	ggiGA_resource_handle reqcurrent;
	ggiGA_resource_handle rescurrent;

	if (reqlist == NULL) return NULL;
	if (reslist == NULL) return NULL;
	if (reqhandle == NULL) return NULL;


	reqcurrent = RESLIST_FIRST(reqlist);
	rescurrent = RESLIST_FIRST(reslist);
	while (reqcurrent != NULL) {
		if (reqcurrent == reqhandle) return rescurrent;

		reqcurrent = RESLIST_NEXT(reqcurrent);
		rescurrent = RESLIST_NEXT(rescurrent);
		if (rescurrent == NULL && reqcurrent != NULL) {
			return NULL;
		}	/* if */
	}	/* while */

	/* There was no such resource */
	return NULL;
}	/* ggiGAHandle */


int ggiGAReleaseVis(ggi_visual_t vis,
		ggiGA_resource_list *list,
		ggiGA_resource_handle *handle)
{
	if (vis == NULL) return GGI_EARGINVAL;
	if (list == NULL) return GGI_EARGINVAL;
	if (handle == NULL) return GGI_EARGINVAL;

	if (vis != ggiGAGetVisual(handle[0])) return GALLOC_EFAILED;

	return ggiGARelease(vis, list, handle);
}	/* ggiGAReleaseVis */


int ggiGAReleaseList(ggi_visual_t vis, ggiGA_resource_list *list,
		     ggiGA_resource_handle *handle)
{
	int rc;
	ggiGA_resource_handle startfrom = NULL;
	ggiGA_resource_handle lastmode = NULL;
	ggiGA_resource_handle next_res = NULL;
	ggiGA_resource_handle current = NULL;
	struct ggiGA_resource_listhead list2 = GG_SIMPLEQ_HEAD_INITIALIZER(list2);

	if (list == NULL) return GGI_EARGINVAL;

	if (list[0] == NULL) return GALLOC_OK; /* That was easy.  :-) */

	if (handle == NULL) startfrom = RESLIST_FIRST(list[0]);
	else if (handle[0] != NULL) startfrom = handle[0];
	else return GGI_EARGINVAL;

	rc = ggiGAFind(list[0], startfrom);
	if (rc != GALLOC_OK) return GGI_EARGINVAL;

	current = ggiGAGetPrevSkipCap(list[0], startfrom);
	if (current != NULL) startfrom = current;
	LIB_ASSERT(startfrom != NULL, "startfrom == NULL");

	/* Releasing tail-first results in less failures.  Note
	 * failures are not fatal, and can sometimes be expected, 
	 * so they should not be noisy.
	 */
	do {
		int success;

		success = 0;
		current = ggiGAGetPrevSkipCap(list[0], NULL);
		while (current != startfrom) {
			
			rc = ggiGARelease(vis, list, &current);
			if (rc == GALLOC_OK) {
				success = 1; 
				break;
			}	/* if */
			current = ggiGAGetPrevSkipCap(list[0], current);
		}	/* while */
		if (!success) break;
	} while (1);

	if (rc) return rc;

	/* The list should now be clean, with the only time a resource
	 * was not freed when asked being GA_RT_FRAMEs, but those should
	 * have been silently ignored without errors from ggiGARelease.
	 * Now by hand we Remove all the GA_RT_FRAME resources except
	 * lastmode and its cap.
	 */

	LIB_ASSERT(startfrom != NULL, "startfrom == NULL");

	RESLIST_INSERT_HEAD(&list2, startfrom);

	lastmode = ggiGAFindLastMode(&list2, NULL);
	LIB_ASSERT(lastmode != NULL, "there is no GA_RT_FRAME resource!");

	current = ggiGAGetNextSkipCap(startfrom);
	while (current != NULL) {
		next_res = RESLIST_NEXT(current);

		LIB_ASSERT(ggiGA_TYPE(current) == GA_RT_FRAME, "This must be a GA_RT_FRAME resource");
		if (current == lastmode) break;

		rc = ggiGARemove(list, &current);
		LIB_ASSERT(rc == GALLOC_OK, "Could not remove a resource");

		current = next_res;
	}	/* while */

	return GALLOC_OK;

}	/* ggiGAReleaseList */




/*******************************************************
 * "gray area" API
 */

ggiGA_resource_handle ggiGACopyIntoResource(struct ggiGA_resource_props *props,
					void *priv, size_t privlen)
{
	ggiGA_resource_handle handle;

	LIB_ASSERT(props != NULL, "props == NULL");

	handle = calloc((size_t)1, sizeof(handle[0]));
	if (handle == NULL) goto err0;

	handle->props = calloc((size_t)1, sizeof(struct ggiGA_resource_props));
	if (handle->props == NULL) goto err1;
	memcpy(handle->props, props, sizeof(struct ggiGA_resource_props));

	handle->storage_share = (props->storage_share & GA_SHARE_REQUEST_MASK);
	handle->storage_ok = props->storage_ok;

	handle->priv = RESLIST_NEXT(handle) = NULL;
	return(handle);

 err1:
	free(handle);
 err0:
	return NULL;
}	/* ggiGACopyIntoResource */


ggiGA_resource_handle ggiGAWrapIntoResource(struct ggiGA_resource_props *props,
					void *priv, size_t privlen) 
{
	ggiGA_resource_handle handle;


	LIB_ASSERT(priv != NULL, "priv == NULL");
	LIB_ASSERT(props != NULL, "props == NULL");


	handle = calloc((size_t)1, sizeof(handle[0]));
	if (handle == NULL) goto err0;

	handle->props = props;
	handle->priv = priv;
	handle->priv_size = privlen;
	RESLIST_NEXT(handle) = NULL;

	handle->storage_share = (props->storage_share & GA_SHARE_REQUEST_MASK);
	handle->storage_ok = props->storage_ok;

	return handle;

 err0:
	return NULL;
}	/* ggiGAWrapIntoResource */


void *ggiGAGetPriv(ggiGA_resource_handle handle)
{
	if (handle == NULL) {
		DPRINT("NULL passed to GetPriv\n"); 
		return(NULL);
	}	/* if */

	return handle->priv;
}	/* ggiGAGetPriv */


#if 0
int ggiGASetPriv(ggiGA_resource_handle handle, void *priv, size_t size)
{
	if (handle == NULL) return GGI_EARGINVAL;
	if (priv == NULL && size == 0) {
		if (handle->priv != NULL) free (handle->priv);
		handle->priv = NULL;
		return GALLOC_OK;
	}	/* if */

	if (priv == NULL) return GGI_EARGINVAL;
	if (size == 0) return GGI_EARGINVAL;

	if (handle->priv != NULL) free(handle->priv);
	handle->priv_size = 0;

	handle->priv = malloc(size);
	if (handle->priv == NULL) return GGI_ENOMEM;

	handle->priv_size = size;
	memcpy(handle->priv, priv, size);

	return GALLOC_OK;
}	/* ggiGASetPriv */
#endif

ggiGA_resource_type ggiGAGetType(ggiGA_resource_handle handle) 
{
	return (handle->res_type & GA_RT_FULLTYPE_MASK);
}	/* ggiGAGetType */


ggiGA_resource_type ggiGASetType(ggiGA_resource_handle handle, 
				   ggiGA_resource_type rt)
{
	ggiGA_resource_type tmp;

	tmp = handle->res_type;
	handle->res_type &= ~GA_RT_FULLTYPE_MASK;
	handle->res_type |= rt;
	return (tmp & GA_RT_FULLTYPE_MASK);
}	/* ggiGASetType */


ggiGA_resource_state ggiGAGetState(ggiGA_resource_handle handle) 
{
	LIB_ASSERT(handle != NULL, "handle == NULL");

	return (handle->res_state & GA_STATE_RESPONSE_MASK);
}	/* ggiGAGetState */


ggiGA_resource_state ggiGASetState(ggiGA_resource_handle handle, 
				ggiGA_resource_state rs)
{
	ggiGA_resource_state tmp;

	tmp = (handle->res_state & GA_STATE_RESPONSE_MASK);
	rs &= GA_STATE_REQUEST_MASK;
	rs |= (handle->res_state & ~(unsigned)(GA_STATE_REQUEST_MASK));

	handle->res_state = rs;
	return tmp;
}	/* ggiGASetState */


unsigned int ggiGAGetCoordBase(ggiGA_resource_handle handle)
{
	return handle->cb;
}	/* ggiGAGetCoordBase */


ggiGA_storage_type ggiGAGetStorage(ggiGA_resource_handle handle)
{
	return (handle->storage_ok);
}	/* ggiGAGetStorage */


ggiGA_storage_share ggiGAGetStorageShareState(ggiGA_resource_handle handle)
{
	return (handle->storage_share & GA_SHARE_RESPONSE_MASK);
}	/* ggiGAGetStorageShareState */


ggiGA_storage_share ggiGASetStorageShareState(ggiGA_resource_handle handle,
						ggiGA_storage_share ss)
{
	ggiGA_storage_share tmp;

	tmp = (handle->storage_share & GA_SHARE_RESPONSE_MASK);
	ss &= GA_SHARE_REQUEST_MASK;
	ss |= (handle->storage_share & ~(unsigned)(GA_SHARE_REQUEST_MASK));

	handle->storage_share = ss;
	return tmp;
}	/* ggiGASetStorageShareState */


int ggiGAIsShared(ggiGA_resource_handle handle)
{
	return ((handle->storage_share & GA_SHARE_SHARED) ? 1 : 0);
}	/* ggiGAIsShared */


int ggiGAIsShareable(ggiGA_resource_handle handle)
{
	return ((handle->storage_share & GA_SHARE_SHAREABLE) ? 1 : 0);
}	/* ggiGAIsShareable */


int ggiGAHasCopyOnWrite(ggiGA_resource_handle handle)
{
	return ((handle->storage_share & GA_SHARE_COPYONWRITE) ? 1 : 0);
}	/* ggiGAHasCopyOnWrite */


int ggiGAIsReadOnly(ggiGA_resource_handle handle)
{
	return ((handle->storage_ok & GA_STORAGE_WRITE) ? 0 : 1);
}	/* ggiGAIsReadOnly */


#if 0

int ggiGACopy2Write(ggiGA_resource_handle *handle)
{
	ggiGA_resource_list list = NULL;
	ggiGA_resource_list list2 = NULL;
	ggiGA_resource_handle handle2 = NULL;
	void *handle_data = NULL;

	int rc = GALLOC_OK;


	DPRINT_SHARE("%s:%s:%i: entered\n", DEBUG_INFO);

	if (handle == NULL) return GGI_EARGINVAL;
	if (handle[0] == NULL) return GGI_EARGINVAL;

	if (!ggiGAIsShared(handle[0]) || !ggiGAHasCopyOnWrite(handle[0])) {
		DPRINT_SHARE("%s:%s:%i: handle not shared or no copy2write-flag\n",
				DEBUG_INFO);
		rc = GALLOC_EFAILED;
		goto err0;
	}	/* if */

	DPRINT_SHARE("%s:%s:%i: make backup\n", DEBUG_INFO);

	/* Backup handle in case of failure */
	handle_data = malloc(sizeof(handle[0]));
	if (!handle_data) {
		rc = GGI_ENOMEM;
		goto err0;
	}	/* if */
	memcpy(handle_data, handle[0], sizeof(handle[0]));


	rc = ggiGAGet(handle[0]->vis, &list);
	LIB_ASSERT(rc == GALLOC_OK, "ggiGAGet() failed");
	if (rc != GALLOC_OK) goto err0;

	LIB_ASSERT(ggiGAFind(list, handle[0]), "resource to share not there");

	DPRINT_SHARE("%s:%s:%i: unshare\n", DEBUG_INFO);

	rc = ggiGAUndoShare(list, handle[0]);
	LIB_ASSERT(rc == GALLOC_OK, "ggiGAUndoShare() failed");
	if (rc != GALLOC_OK) goto err1;

	LIB_ASSERT(handle[0]->priv == NULL, "resource not shared?");

	/* We assume here, that all list-members are flagged
	 * with (GA_STATE_NOCHANGE | GA_STATE_NORESET)
	 * for speed. If this is not the case, then there is
	 * a bug anywhere.
	 */

	handle[0]->res_state &= ~GA_STATE_NORESET;

	DPRINT_SHARE("%s:%s:%i: recheck\n", DEBUG_INFO);

	rc = ggiGACheck(handle[0]->vis, list, NULL);
	LIB_ASSERT(rc == GALLOC_OK, "ggiGACheck() failed");
	if (rc != GALLOC_OK) goto err2;

	LIB_ASSERT(handle[0]->priv != NULL, "handle has no private area");

	DPRINT_SHARE("%s:%s:%i: reset\n", DEBUG_INFO);

	rc = ggiGASet(handle[0]->vis, list, &list2);
	LIB_ASSERT(rc == GALLOC_OK, "ggiGASet() failed");
	if (rc != GALLOC_OK) goto err2;

	LIB_ASSERT(handle[0]->priv != NULL, "handle has no private area");

	DPRINT_SHARE("%s:%s:%i: update handle\n", DEBUG_INFO);

	handle2 = ggiGAHandle(list2, list, handle[0]);
	LIB_ASSERT(handle2 != NULL, "ggiGAHandle() failed");
	if (handle2 == NULL) goto err2;

	handle[0] = handle2;
	rc = GALLOC_OK;

	ggiGADestroyList(&list2);
	ggiGADestroyList(&list);
	free(handle_data);
	DPRINT_SHARE("%s:%s:%i: exit successful\n", DEBUG_INFO);
	return rc;

err2:
	ggiGADestroyList(&list2);
err1:
	ggiGADestroyList(&list);
	memcpy(handle[0], handle_data, sizeof(handle[0]));
	free(handle_data);
err0:
	DPRINT_SHARE("%s:%s:%i: exit with error: %i\n",
				DEBUG_INFO, rc);
	return rc;
}	/* ggiGACopy2Write */

#endif
