/* $Id: taghead.c,v 1.4 2007/12/20 23:43:06 cegger Exp $
******************************************************************************

   LibGalloc: utility functions for taghead manipulation

   Copyright (C) 2005 Christoph Egger

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/internal/galloc.h>
#include <ggi/internal/galloc_int.h>
#include <ggi/internal/galloc_debug.h>

#include <string.h>


#define ggiGATagHeadGetSize(size)	\
	do {									\
		size = sizeof(struct ggiGA_taghead);				\
		if (sizeof(struct ggiGA_resource) > sizeof(struct ggiGA_taghead)) {	\
			size = sizeof(struct ggiGA_resource);			\
		}								\
	} while(0)


ggiGA_taghead_t ggiGACreateTagHead(void)
{
	size_t size;
	ggiGA_taghead_t taghead;

	ggiGATagHeadGetSize(size);

	DPRINT("create taghead, allocating %lu bytes\n",
		(unsigned long)size);

	taghead = calloc(1, size);
	return taghead;
}


int ggiGADestroyTagHead(ggiGA_taghead_t *taghead)
{
	if (taghead == NULL) return GGI_EARGINVAL;

	LIB_ASSERT(taghead[0] != NULL, "invalid pointer");

	free(taghead[0]);
	taghead[0] = NULL;
	return 0;
}


int ggiGATagHeadClone(ggiGA_resource_taghandle in, ggiGA_resource_taghandle *out)
{
	size_t size;
	ggiGA_taghead_t tmp = NULL;

	DPRINT("entered ggiGATagHeadClone()\n");
	LIB_ASSERT(ggiGAIsTagHead(in), "in must be a taghead");

	if (out == NULL) return GGI_EARGINVAL;

	if (in == out[0]) {
		/* don't copy yourself */
		return GALLOC_OK;
	}

	tmp = ggiGACreateTagHead();
	if (tmp == NULL) return GGI_ENOMEM;

	out[0] = (ggiGA_resource_taghandle)tmp;

	ggiGATagHeadGetSize(size);
	memcpy(tmp, in, size);

	tmp->left = NULL;
	tmp->right = NULL;

	tmp->left2 = NULL;
	tmp->right2 = NULL;
	return GALLOC_OK;
}


int ggiGATagHeadCopy(ggiGA_resource_taghandle in, ggiGA_resource_taghandle out)
{
	size_t size;
	ggiGA_resource_handle next = NULL;
	ggiGA_resource_taghandle left = NULL, right = NULL;
	ggiGA_resource_taghandle left2 = NULL, right2 = NULL;
	ggiGA_taghead_t th_in, th_out;

	DPRINT("entered ggiGATagHeadCopy()\n");
	LIB_ASSERT(ggiGAIsTagHead(in), "in must be a taghead");

	if (out == NULL) return GALLOC_EFAILED;

	if (in == out) {
		/* Copying a resource back into itself is a noop. */
		return GALLOC_OK;
	}	/* if */

	LIB_ASSERT(ggiGAIsTagHead(out), "out must be a taghead");

	th_in = ggiGATagGetHead(in);
	th_out = ggiGATagGetHead(out);

	/* Save list pointers */
	next = RESLIST_NEXT(th_out);
	left = th_out->left;
	right = th_out->right;
	left2 = th_out->left2;
	right2 = th_out->right2;

	ggiGATagHeadGetSize(size);
	memcpy(th_out, th_in, size);

	/* Restore list pointers */
	RESLIST_NEXT(th_out) = next;
	th_out->left = left;
	th_out->right = right;
	th_out->left2 = left2;
	th_out->right2 = right2;

	return GALLOC_OK;
}
