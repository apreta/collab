/* $Id: reslist_ops.c,v 1.20 2005/12/02 23:11:27 cegger Exp $
******************************************************************************

   LibGalloc: functions for resource list manipulation

   Copyright (C) 2003 Christoph Egger   [Christoph_Egger@t-online.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/internal/galloc.h>
#include <ggi/internal/galloc_debug.h>
#include <ggi/internal/galloc_int.h>	/* declares ggiGAIsTagHead() */

#include <string.h>

/* Docs for functions listed here are in the ggi/internal/galloc.h or
 * ggi/internal/galloc_ops.h headers, or in the API docs.
 */


int ggiGAFind(ggiGA_resource_list list, ggiGA_resource_handle handle)
{
	ggiGA_resource_handle current = NULL;

	if (list == NULL) return GGI_EARGINVAL;
	if (handle == NULL) return GGI_EARGINVAL;

	RESLIST_FOREACH(current, list) {
		if (current == handle) return GALLOC_OK;
	}	/* foreach */

	/* There was no such resource */
	return GALLOC_EUNAVAILABLE;
}	/* ggiGAFind */


ggiGA_resource_handle ggiGAPropsHandle(const ggiGA_resource_list list, 
				       ggiGA_resource_handle handle)
{
	ggiGA_resource_handle current = NULL;

	LIB_ASSERT(list != NULL, "list == NULL");

	if (handle == NULL) return NULL;

	if (ggiGAIsTagHead(handle)) return handle;
	if ((handle->props == NULL) && (handle->priv == NULL)) return handle;

	RESLIST_FOREACH(current, list) {
		if ((handle->props != NULL) && 
		    (current->props == handle->props)) break;
		if ((handle->priv != NULL) &&
		    (current->priv  == handle->priv)) break;
	}	/* foreach */

	return current;
}	/* ggiGAPropsHandle */


int ggiGAFreeDetachedHandle(const ggiGA_resource_list list, 
			     ggiGA_resource_handle *handle) 
{
	int rc = 0;

	if (ggiGAPropsHandle(list, handle[0]) == NULL) {
		if (handle[0]->props) {
			free(handle[0]->props);
			rc |= 1;
		}	/* if */

		if (handle[0]->priv) {
			free(handle[0]->priv);
			rc |= 2;
		}	/* if */
	}	/* if */

	free(handle[0]);
	handle[0] = NULL;

	return rc;
}	/* ggiGAFreeDetachedHandle */


ggiGA_resource_list ggiGACreateList(void)
{
	ggiGA_resource_list list = NULL;

	list = calloc((size_t)1, sizeof(struct ggiGA_resource_listhead));
	RESLIST_INIT(list);

	return list;
}

int ggiGADestroyList(ggiGA_resource_list *list)
{
	if (list == NULL) return GGI_EARGINVAL;
	if (list[0] == NULL) return GGI_EARGINVAL;

	ggiGAEmptyList(list);
	free(list[0]);
	list[0] = NULL;

	return GALLOC_OK;
}


int ggiGATruncateList(ggiGA_resource_list list, ggiGA_resource_handle *handle)
{
	ggiGA_resource_handle current = NULL;
	int rc;

	if (handle == NULL) return GGI_EARGINVAL;
	if (handle[0] == NULL) return GGI_EARGINVAL;

	/* ggiGAFind will check that list != NULL */
	rc = ggiGAFind(list, handle[0]);
	if (rc != GALLOC_OK) return rc;

	/* First free properties from non-compound
	 * resources. Note, that this can't be done
	 * in one loop, because the properties
	 * are shared.
	 */
	current = handle[0];
	while (current != NULL) {
		if ((!ggiGAIsTagHead(current))
		  && (ggiGAPropsHandle(list, current) == current))
		{
			if (current->props) free(current->props);
			if (current->priv) free(current->priv);
		}	/* if */

		LIB_ASSERT(current != RESLIST_NEXT(current),
			"current == current->next => bogus list");
		current = RESLIST_NEXT(current);
	}	/* while */

	current = handle[0];
	while (current != NULL) {
		ggiGA_resource_handle next;

		next = RESLIST_NEXT(current);
		RESLIST_REMOVE(list, current);
		free(current);
		current = next;
	}	/* while */

	handle[0] = NULL;

	if (RESLIST_EMPTY(list)) RESLIST_INIT(list);

	return GALLOC_OK;
}	/* ggiGATruncateList */


int ggiGAEmptyList(ggiGA_resource_list *list)
{
	if (list == NULL) return 0; /* Already an empty list */
	return ggiGATruncateList(list[0], &RESLIST_FIRST(list[0]));
}	/* ggiGAEmptyList */


int ggiGAEmptyFailures(ggiGA_resource_list *list)
{
	int rc = GALLOC_OK;
	ggiGA_resource_handle current = NULL;

	if (list == NULL) return GGI_EARGINVAL;
	if (list[0] == NULL) return GGI_EARGINVAL;

	if (ggiGAIsFailed(RESLIST_FIRST(list[0]))) {
		rc = ggiGAEmptyList(list);
		goto done;
	}	/* if */

	RESLIST_FOREACH(current, list[0]) {
		if (ggiGAIsFailed(RESLIST_NEXT(current))) {
			rc = ggiGATruncateList(list[0], &(RESLIST_NEXT(current)));
			goto done;
		}	/* if */
	}	/* foreach */

 done:
	return rc;
}	/* ggiGAEmptyFailures */




int ggiGAAddMutual(ggiGA_resource_list list, ggiGA_resource_handle *handle)
{
	int rc = GALLOC_OK;
	ggiGA_resource_handle result = NULL;

	/* tagheads are never mutual resources */
	LIB_ASSERT(!ggiGAIsTagHead(handle[0]), "handle may not be a taghead");

	if (ggiGAFind(list, handle[0]) != GALLOC_OK) {
		return GGI_EARGINVAL;
	}	/* if */

	rc = ggiGACloneNoProps(handle[0], &result);
	if (rc != GALLOC_OK) goto err1;

	result->props = handle[0]->props;
	result->priv = handle[0]->priv;

	RESLIST_NEXT(result) = NULL;

	/* Indicate that this resource has not been checked yet,
	 * even though it may be flagged active.
	 */
	ggiGAFlagFailed(result);

	/* We know that &list will not be changed, because we know
	 * It is not empty.  (As long as noone changes AppendList's behavior.)
	 */
	rc = ggiGAAppend(result, &list, 0);

	handle[0] = result;

	LIB_ASSERT(rc == GALLOC_OK, "we return success, although something went wrong!");

	return GALLOC_OK;

 err1:
	if (handle != NULL) handle[0] = NULL;
	return GGI_ENOMEM;
}	/* ggiGAAddMutual */


int ggiGAAppend(ggiGA_resource_handle handle, ggiGA_resource_list *list,
		int copy)
{
	ggiGA_resource_handle tmp = NULL;

	if (list == NULL) return GGI_EARGINVAL;

	LIB_ASSERT(handle != NULL, "handle == NULL");

	if (list[0] == NULL) {
		list[0] = ggiGACreateList();
		if (list[0] == NULL) return GGI_ENOMEM;
	}
	LIB_ASSERT(list[0] != NULL, "list: invalid header");

	if (copy) {
		ggiGAClone(handle, &tmp);
		RESLIST_NEXT(tmp) = NULL;
	} else {
		tmp = handle;
	}

	if (RESLIST_EMPTY(list[0])) {
		/* Oops! - The first element in this list! */
		RESLIST_INSERT_HEAD(list[0], tmp);
	} else {
		RESLIST_INSERT_TAIL(list[0], tmp);
	}	/* if */

	return GALLOC_OK;
}	/* ggiGAAppend */


/* Used by extensions for their Create* lazy API, to keep the global
 * reqlist in sync without altering the existing handles in it.
 */
int ggiGASpliceList(ggiGA_resource_list *onto, ggiGA_resource_list *list)
{
	ggiGA_resource_handle onto_h;

	if (list == NULL) return GGI_EARGINVAL;
	if (onto == NULL) return GGI_EARGINVAL;

	if (RESLIST_EMPTY(onto[0])) {
		onto[0] = list[0];
		list[0] = NULL;
		return GALLOC_OK;
	}

	onto_h = RESLIST_FIRST(onto[0]);
	do {	
		if (RESLIST_EMPTY(list[0])) return GALLOC_EFAILED;
		/* Remove first resource and advance list[0] to next one. */
		ggiGARemove(list, &RESLIST_FIRST(list[0])); 
		if (RESLIST_NEXT(onto_h) == NULL) break;
		onto_h = RESLIST_NEXT(onto_h);
	} while (1);

	RESLIST_NEXT(onto_h) = RESLIST_FIRST(list[0]);
	return GALLOC_OK;
}

int ggiGARemove(ggiGA_resource_list *list, ggiGA_resource_handle *handle)
{
	int rc = GALLOC_OK;
	ggiGA_resource_handle curr = NULL;
	ggiGA_resource_handle res;
	void **tmp;

	if (list == NULL) return GGI_EARGINVAL;
	if (handle == NULL) return GGI_EARGINVAL;

	rc = ggiGAFind(list[0], handle[0]);
	if (rc != GALLOC_OK) {
		/* not present in list */
		return GALLOC_EUNAVAILABLE;
	}	/* if */

	res = handle[0];
	curr = RESLIST_FIRST(list[0]);
	if (curr == res) {
		tmp = (void **)&RESLIST_FIRST(list[0]);
	} else {
		RESLIST_FOREACH(curr, list[0]) {
			if (res == RESLIST_NEXT(curr)) goto cont;
		}
		LIB_ASSERT(curr == NULL,
			"not found. Should have been catched by ggiGAFind");

cont:
		tmp = (void **)&RESLIST_NEXT(curr);
	}


	/* Unlink from list... */
	RESLIST_REMOVE(list[0], res);

	/* Empty next pointer */
	RESLIST_NEXT(res) = NULL;

	/* Flushhh. */
	ggiGAFreeDetachedHandle(list[0], &res);

	if ((void **)handle != tmp) handle[0] = NULL;


	return GALLOC_OK;
}	/* ggiGARemove */


int ggiGARemoveWithCap(ggiGA_resource_list *list,
			ggiGA_resource_handle *handle)
{
	int rc = GALLOC_OK;

	LIB_ASSERT(list != NULL, "list == NULL");
	LIB_ASSERT(list[0] != NULL, "list[0] == NULL");
	LIB_ASSERT(handle != NULL, "handle == NULL");
	LIB_ASSERT(handle[0] != NULL, "handle[0] == NULL");

	if (list == NULL) return GGI_EARGINVAL;
	if (handle == NULL) return GGI_EARGINVAL;

	/* Obviously, you cannot remove anything from empty list */
	if (list[0] == NULL) return GALLOC_EUNAVAILABLE;

	rc = ggiGAFind(list[0], handle[0]);
	if (rc != GALLOC_OK) {
		/* not present in list */
		rc = GALLOC_EUNAVAILABLE;
		goto exit;
	}	/* if */

	if (ggiGAIsCap(RESLIST_NEXT(handle[0]))) {
		rc = ggiGARemove(list, &(RESLIST_NEXT(handle[0])));
		LIB_ASSERT(rc == GALLOC_OK, "Could not remove a cap");
	}	/* if */

	rc = ggiGARemove(list, handle);
	LIB_ASSERT(rc == GALLOC_OK, "Could not remove a resource");

exit:
	return rc;
}	/* ggiGARemoveWithCap */


int ggiGACopyList(const ggiGA_resource_list in, ggiGA_resource_list *out)
{
	int rc = GALLOC_OK;

	ggiGA_resource_handle res = NULL;
	ggiGA_resource_handle tail = NULL;


	LIB_ASSERT(in != NULL, "invalid 'in' listheader");

	if (out == NULL) return GGI_EARGINVAL;

	if (out[0] == NULL) {
		out[0] = ggiGACreateList();
		if (out[0] == NULL) return GGI_ENOMEM;
	}

	LIB_ASSERT(out[0] != NULL, "invalid 'out' list header");

	if (!RESLIST_EMPTY(out[0])) {
		rc = ggiGAEmptyList(out);
		LIB_ASSERT(rc == GALLOC_OK, "Could not make resource list empty");
	}	/* if */
	LIB_ASSERT(RESLIST_EMPTY(out[0]), "List not empty");

	/* Return Empty list (NULL) if given empty list. */
	LIB_ASSERT(in != NULL, "invalid 'in' listheader");
	if (RESLIST_EMPTY(in)) goto exit;

	RESLIST_FOREACH(res, in) {
		ggiGA_resource_handle tmp1 = NULL;

		tail = NULL;

		if (res == RESLIST_FIRST(in)) {
			/* Do the first one */
			rc = ggiGAClone(RESLIST_FIRST(in), &tail);
			if (rc != GALLOC_OK) goto err1;
			LIB_ASSERT(RESLIST_NEXT(tail) == NULL,
				"resource cloning failed");

			RESLIST_INSERT_HEAD(out[0], tail);
			LIB_ASSERT(tail != NULL, "tail == NULL");
			continue;
		}

		/* Detect mutual properties and duplicate them appropriately */
		tmp1 = ggiGAPropsHandle(in, res);
		LIB_ASSERT(tmp1 != NULL, "There are no mutual properties");

		if (tmp1 != res) {
			ggiGA_resource_handle tmp2;

			tmp2 = ggiGAHandle(out[0], in, tmp1);
			LIB_ASSERT(tmp2 != NULL, "Cound not find handle of property");

			rc = ggiGACloneNoProps(res, &tail);
			if (rc != GALLOC_OK) goto err2;
			tail->props = tmp2->props;
			tail->priv =  tmp2->priv;

		} else {
			rc = ggiGAClone(res, &tail);
			if (rc != GALLOC_OK) goto err2;

		}	/* if */

		RESLIST_INSERT_TAIL(out[0], tail);
	}	/* foreach */

 exit:
	return GALLOC_OK;

 err2:
	ggiGADestroyList(out);

 err1:
	return GGI_ENOMEM;
}	/* ggiGACopyList */


int ggiGAAppendList(const ggiGA_resource_list this, ggiGA_resource_list *tothis)
{
	if (tothis == NULL) return GGI_EARGINVAL;

	if (tothis[0] == NULL) {
		tothis[0] = this;
		goto exit;
	}	/* if */
	LIB_ASSERT(tothis[0] != NULL, "invalid list header");

	LIB_ASSERT(*tothis[0]->sqh_last == NULL,
		"bogus header: last pointer doesn't show to last next element");

	if (RESLIST_EMPTY(this)) {
		/* There's nothing to append, so just return */
		goto exit;
	}	/* if */


	if (RESLIST_EMPTY(tothis[0])) {
		RESLIST_INSERT_HEAD(tothis[0], RESLIST_FIRST(this));
		tothis[0]->sqh_last = this->sqh_last;
	} else {
		*tothis[0]->sqh_last = RESLIST_FIRST(this);
		tothis[0]->sqh_last = this->sqh_last;
	}	/* if */

 exit:
	return GALLOC_OK;
}	/* ggiGAAppendList */


/* Finds and returns the handle of the first (successful) GA_RT_FRAME
 * resource. If the returned "handle" is NULL, then there is _no_
 * GA_RT_FRAME in the list, which should never happen.
 */
ggiGA_resource_handle ggiGAFindFirstMode(const ggiGA_resource_list reslist)
{
	ggiGA_resource_handle current = NULL;

	LIB_ASSERT(reslist != NULL, "reslist == NULL");

	RESLIST_FOREACH(current, reslist) {

		if (ggiGA_TYPE(current) != GA_RT_FRAME) continue;

		/* GA_RT_FRAMEs don't come in "seeabove" flavor */
		LIB_ASSERT(!(current->res_state & GA_STATE_SEEABOVE),
			"GA_RT_FRAMEs do not come in \"seeabove\" flavor");

		/* GA_RT_FRAME might be a CAP, when the previous one failed */
		if (ggiGAIsCap(current)) continue;
		if (ggiGAIsFailed(current)) continue;

		break;
	}	/* foreach */

	/* Should never fail. */
	LIB_ASSERT(current != NULL, "no GA_RT_FRAME resource handle found");

	return current;
}	/* ggiGAFindFirstMode */



/* Find the last successful GA_RT_FRAME resource before "handle" in
 * "reslist", or if "handle" is NULL the last in the entire list.  By "last
 * successful" we mean that if a failure is found before "handle", the search
 * returns NULL, not the last one that would have succeeded.
 */
ggiGA_resource_handle ggiGAFindLastMode(const ggiGA_resource_list reslist,
					const ggiGA_resource_handle handle)
{
	ggiGA_resource_handle current = NULL;
	ggiGA_resource_handle result = NULL;

	LIB_ASSERT(reslist != NULL, "reslist == NULL");

	RESLIST_FOREACH(current, reslist) {

		if (current == handle) goto finish;
		if (ggiGAIsFailed(current)) return NULL;
		if (ggiGA_TYPE(current) != GA_RT_FRAME) continue;

		/* GA_RT_FRAMEs don't come in "seeabove" flavor */
		LIB_ASSERT(!(current->res_state & GA_STATE_SEEABOVE),
			"GA_RT_FRAMEs do not come in \"seeabove\" flavor");

		if (ggiGAIsCap(current)) continue;

		result = current;
	}	/* foreach */

finish:
	return result;
}	/* ggiGAFindLastMode */

