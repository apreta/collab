/* $Id: reslist_nav.c,v 1.1 2005/10/10 13:59:09 cegger Exp $
******************************************************************************

   LibGalloc: functions for resource list navigation

   Copyright (C) 2005 Christoph Egger

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/internal/galloc.h>
#include <ggi/internal/galloc_debug.h>




/* Returns the pointer of the first member of the list
 */
ggiGA_resource_handle ggiGAGetFirst(ggiGA_resource_list list)
{
	if (list == NULL) return NULL;

	return RESLIST_FIRST(list);
}


/* Returns the pointer of the next member of the list
 */
ggiGA_resource_handle ggiGAGetNext(ggiGA_resource_handle handle)
{
	if (handle == NULL) return NULL;

	return RESLIST_NEXT(handle);
}	/* ggiGAGetNext */


/* As above, but skips the cap, if present
 */
ggiGA_resource_handle ggiGAGetNextSkipCap(ggiGA_resource_handle handle)
{
	if (handle == NULL) return NULL;

	handle = RESLIST_NEXT(handle);
	if (ggiGAIsCap(handle)) handle = RESLIST_NEXT(handle);

	return handle;
}	/* ggiGAGetNextSkipCap */


ggiGA_resource_handle ggiGAGetPrev(ggiGA_resource_list list, 
				   ggiGA_resource_handle handle)
{
	ggiGA_resource_handle tmp;

	/* Note: If (handle == NULL) we want to return the last item. */

	/* If handle is the first element in the list, we return NULL 
	 * since there is nothing before it.  Note this also correctly
	 * handles the case where handle == list == NULL.
	 */
	if (RESLIST_FIRST(list) == handle) return NULL;

	RESLIST_FOREACH(tmp, list) {
		if (tmp == handle) return tmp;
	}	/* foreach */

	return NULL;

}	/* ggiGAGetPrev */


ggiGA_resource_handle ggiGAGetPrevSkipCap(ggiGA_resource_list list, 
					   ggiGA_resource_handle handle)
{
	ggiGA_resource_handle tmp;

	/* Note: If (handle == NULL) we want to return the last item. */

	/* If handle is the first element in the list, we return NULL 
	 * since there is nothing before it.  Note this also correctly
	 * handles the case where handle == list == NULL.
	 */
	if (RESLIST_FIRST(list) == handle) return NULL;

	for (tmp = RESLIST_FIRST(list); tmp; ) {
		if (ggiGAGetNextSkipCap(tmp) == handle) return tmp;
		tmp = ggiGAGetNextSkipCap(tmp);
	}

	return NULL;
}	/* ggiGAGetPrevSkipCap */

