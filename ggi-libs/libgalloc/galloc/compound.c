/* $Id: compound.c,v 1.16 2005/12/03 16:43:34 cegger Exp $
******************************************************************************

   LibGAlloc: utility functions for manipulation of compound resources

   Copyright (C) 2001 Brian S. Julin	[bri@tull.umassp.edu]
   Copyright (C) 2001 Christoph Egger   [Christoph_Egger@t-online.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/internal/galloc.h>
#include <ggi/internal/galloc_debug.h>

#include <ggi/internal/galloc_int.h>	/* for ggiGAIsTagHead */


int ggiGAAddSeeabove(ggiGA_resource_list *reqlist, 
		     struct ggiGA_resource_props *props, 
		     ggiGA_resource_type rt, 
		     ggiGA_resource_handle *handle)
{
	int rc;
	ggiGA_resource_handle tmp;

	rc = ggiGAAdd(reqlist, props, rt, &tmp);
	if (handle != NULL) handle[0] = tmp;
	if (rc != GALLOC_OK) return rc;

	tmp->res_state |= GA_STATE_SEEABOVE;
	return rc;
}	/* ggiGAAddSeeabove */


/* Determine if the resource is a cap.
 * Note: returns 0 on error, or 1 if the resource is a cap.
 */
int ggiGAIsCap(const ggiGA_resource_handle handle)
{
	int state;

	if (handle == NULL) return 0;

	state = (handle->res_state & GA_STATE_CAP);

	return ((state != 0) ? 1 : 0);
}	/* ggiGAIsCap */


/* Determine if the resource is a subordinate member of a compound.
 * Note: returns 0 on error, or 1 if the resource is attached to a compound.
 */
int ggiGAIsSeeabove(const ggiGA_resource_handle handle)
{
	int state;

	if (handle == NULL) return 0;

	state = (handle->res_state & GA_STATE_SEEABOVE);

	return ((state != 0) ? 1 : 0);
}	/* ggiGAIsSeeabove */


/* Determine if the resource is a compound head.
 * Note: returns 0 on error, or 1 if the resource is a compound head.
 */
int ggiGAIsCompoundHead(const ggiGA_resource_handle handle)
{
	int state;

	if (handle == NULL) return 0;

	state = (handle->res_state & (GA_STATE_SEEABOVE | GA_STATE_CAP));

	return ((state == 0) ? 1 : 0);
}	/* ggiGAIsCompoundHead */


/* Determine if the resource has members below it in a compound resource.
 * Note: returns 0 on error, or 1 if the resource has members attached to it.
 */
int ggiGAHasAttachedSeeAboves(const ggiGA_resource_list list,
			      ggiGA_resource_handle handle)
{
	int rc;

	if (handle == NULL) return 0;
	rc = ggiGAFind(list, handle);
	LIB_ASSERT(rc == GALLOC_OK, "member not found below a compound resource");
	if (rc != GALLOC_OK) return 0;

	if (RESLIST_NEXT(handle) == NULL) return 0;
	if (ggiGAIsCap(RESLIST_NEXT(handle))) {
		handle = RESLIST_NEXT(handle);
		if (RESLIST_NEXT(handle) == NULL) return 0;
	}	/* if */
	if (ggiGAIsSeeabove(RESLIST_NEXT(handle))) return 1;

	return 0;
}	/* ggiGAHasAttachedSeeAboves */


/* Finds the head if the compound of the resource pointed to by handle */
ggiGA_resource_handle ggiGAFindCompoundHead(const ggiGA_resource_list list,
					    const ggiGA_resource_handle handle)
{
	int rc;
	ggiGA_resource_handle cur, comp;

	rc = ggiGAFind(list, handle);
	if (rc != GALLOC_OK) return NULL;

	comp = RESLIST_FIRST(list);
	RESLIST_FOREACH(cur, list) {
		if (cur == handle) break;
		if (ggiGAIsCompoundHead(cur)) comp = cur;
	}	/* foreach */

	if (ggiGAIsCompoundHead(cur)) comp = cur;
	return comp;
}	/* ggiGAFindCompoundHead */


/* Finds the last member of a compound resource */
ggiGA_resource_handle ggiGAFindLastSeeabove(const ggiGA_resource_list list,
					    ggiGA_resource_handle handle)
{
	if (handle == NULL) return NULL;
	if (ggiGAIsCap(handle)) return NULL;

	for (;;) {
		/* ggiGAIsCap also checks for the NULL case */
		if (ggiGAIsCap(RESLIST_NEXT(handle))) {

			/* ggiGAIsSeeabove also checks for the NULL case. */
			if (!ggiGAIsSeeabove(RESLIST_NEXT(RESLIST_NEXT(handle)))) {
				return handle;
			}	/* if */
			handle = RESLIST_NEXT(RESLIST_NEXT(handle));

		} else {
			/* ggiGAIsSeeabove also checks for the NULL case. */
			if (!ggiGAIsSeeabove(RESLIST_NEXT(handle))) {
				return handle;
			}	/* if */

			handle = RESLIST_NEXT(handle);
		}	/* if */
	}	/* for */

	/* Yes, we do not use the "list" parameter, we know. */

}	/* ggiGAFindLastSeeabove */


/* Replicate an entire compound resource onto the end of the list 
 * using mutual resources.
 */
int ggiGAAddMutualCompound(ggiGA_resource_list list,
			   ggiGA_resource_handle *handle)
{
	ggiGA_resource_handle current;
	int rc;

	if (handle == NULL) return GALLOC_EFAILED;

	/* tagheads are never mutual resources */
	LIB_ASSERT(!ggiGAIsTagHead(handle[0]), "handle may not be a taghead");

	rc = ggiGAFind(list, handle[0]);
	if (rc != GALLOC_OK) return rc;
	if (!ggiGAIsCompoundHead(handle[0])) return GALLOC_EFAILED;

	current = handle[0];
	handle[0] = NULL;

	if (!ggiGAHasAttachedSeeAboves(list, current)) {
		ggiGA_resource_handle tmp;

		tmp = current;
		rc = ggiGAAddMutual(list, &tmp);
		LIB_ASSERT(rc == GALLOC_OK, "Could not add a mutual resource");
		if (rc != GALLOC_OK) return rc; /* limit damage. */

		handle[0] = tmp;
		tmp = RESLIST_NEXT(tmp);
		if (!ggiGAIsCap(tmp)) return 0;

		rc = ggiGAAddMutual(list, &tmp);
		LIB_ASSERT(rc == GALLOC_OK, "Could not add a mutual resource");
		return rc;
	}	/* if */

	while (ggiGAHasAttachedSeeAboves(list, current)) {
		ggiGA_resource_handle tmp;

		tmp = current;
		rc = ggiGAAddMutual(list, &tmp);
		LIB_ASSERT(rc == GALLOC_OK, "Could not add a mutual resource");
		if (rc != GALLOC_OK) return GALLOC_EFAILED; /* limit damage. */

		current = RESLIST_NEXT(current);
		if (handle[0] == NULL) handle[0] = tmp;
	}	/* while */

	return GALLOC_OK;
}	/* ggiGAAddMutualCompound */
