/* $Id: templates.c,v 1.89 2007/12/20 23:43:06 cegger Exp $
******************************************************************************

   LibGAlloc: Templates for use by simpler extensions.

   Copyright (C) 2001-2003 Christoph Egger	[Christoph_Egger@t-online.de]
   Copyright (C) 2001 Brian S. Julin	[bri@calyx.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/internal/galloc.h>
#include <ggi/internal/galloc_debug.h>
#include <ggi/internal/galloc_int.h>
#include <ggi/ga_prop.h>
#include <stdio.h>
#include <string.h>

/* This function parses through a list, linking resources to one-another
 * in a manner that represents the tag groups found.
 */
int ggiGA_basic_parse_list(ggiGA_resource_list list)
{
	ggiGA_resource_handle cur, last, grep;

	last = NULL;
	LIB_ASSERT(list != NULL, "invalid list");


	RESLIST_FOREACH(cur, list) {
		int variant_cur;

		if (ggiGAIsCap(cur)) goto nextres1;

		/* Initialize resource by making it a 1-member list.
		 * Also points an eventual following cap to its owner.
		 */
		ggiGATagGroupInit(cur);

		variant_cur = ggiGAGetResourceVariant(cur);

		/* Pass # 1: Make double linked lists,
		 * one per variant, per tag, e.g. all motors
		 * with tag == 2 are put in a list, all carbs
		 * with tag == 2 are put in another list, etc.
		 */
		if (!ggiGA_TAG(cur)) goto taghead;
		grep = RESLIST_FIRST(list);
		while ((grep != NULL) && (grep != cur)) {
			int variant_grep;

			if (ggiGAIsCap(grep)) goto nextgrep1;
			variant_grep = ggiGAGetResourceVariant(grep);

			if (variant_cur != variant_grep) goto nextgrep1;

			/* Tag group heads link into the doubly linked list
			 * via their second set of pointers if their second
			 * tag matches.
			 */
			if ((ggiGAIsTagHead(grep)) &&
			    (ggiGAInTagGroup(cur, ggiGAGetTagTail(grep))))
			{
				ggiGATagGroupAddHead(cur, grep);
				break;
			}	/* if */

			if (ggiGACompareTag(cur, grep)) {
				ggiGATagGroupAdd(cur, grep);
				break;
			}	/* if */
		nextgrep1:
			grep = RESLIST_NEXT(grep);
		}	/* while */

	taghead:
		/* More or less repeat of the above code using the
		 * second link in the tag head as the starting point.
		 */
		if (!ggiGAIsTagHead(cur)) goto nextres1;

		grep = RESLIST_FIRST(list);
		while ((grep != NULL) && (grep != cur)) {
			int variant_grep;

			if (ggiGAIsCap(grep)) goto nextgrep2;
			variant_grep = ggiGAGetResourceVariant(grep);

			if (variant_cur != variant_grep) goto nextgrep2;

			if (ggiGAInTagGroup(grep, ggiGAGetTagTail(cur))) {
				ggiGATagGroupAddHead(grep, cur);
				break;
			}	/* if */
		nextgrep2:
			grep = RESLIST_NEXT(grep);
		}	/* while */
	nextres1:
		last = cur;
	}	/* foreach */


	/* Pass 2: Find rings of carbs which have no Tag Group Head
	 * in them.  Link them to the ring of motors with the same 
	 * tag.  If the motor ring contains a tag group head, use that as
	 * the linking point.
	 */
	RESLIST_FOREACH(cur, list) {
		int found;

		if (!ggiGAIsCarb(cur)) continue;

		/* Check if this carb has already been unlinked */
		if (!ggiGAIsCarb(cur->left)) continue;

		/* Check if there is a tag group head for this
		 * ring (Note: "for this ring", not "in this ring").
		 * What we are looking for is a tag group head 
		 * which is linked into the ring via it's second set of 
		 * pointers. Those linked in by the first set are just
		 * regular members of this tag group which happen to have
		 * a subordinate tag group, so they don't count.
		 */
		grep = cur;
		found = 0;
		do {
			if (grep->right->left != grep) {
				ggiGA_taghead_t th;

				th = (ggiGA_taghead_t)grep->right;
				LIB_ASSERT(ggiGAIsTagHead(grep->right), "must be a tag head");
				LIB_ASSERT(th->left2 == grep, "no match");
				LIB_ASSERT(th->tag2 == ggiGA_TAG(cur), "tag ids do not match");

				/* XXX: Shouldn't this assertion be inverted? */
				LIB_ASSERT(!ggiGACompareTag(grep, cur), "tags match");

				found = 1;
				break;
			}	/* if */
			grep = grep->right;
		} while (grep != cur);

		/* A tag group head was found for this ring, so we leave
		 * it alone. 
		 */
		if (found) continue;

		/* Now look for a matching motor ring */
		found = 0;
		RESLIST_FOREACH(grep, list) {
			if (ggiGAIsMotor(grep)
			  && (ggiGACompareTag(grep, cur)))
			{
				found = 1;
				break;
			}	/* if */
		}	/* foreach */
		if (!found) continue;

		/* Look for a tag group head in the motor ring */
		found = 0;
		last = grep;
		do {
			if (grep->right->left != grep) {
				ggiGA_taghead_t th;

				th = (ggiGA_taghead_t)grep->right;
				LIB_ASSERT(ggiGAIsTagHead(grep->right), "must be a tag head");
				LIB_ASSERT(th->left2 == grep, "no match");
				LIB_ASSERT(th->tag2 == ggiGA_TAG(last), "tag ids do not match");

				/* XXX: Shouldn't this assertion be inverted? */
				LIB_ASSERT(!ggiGACompareTag(grep, last), "tags match");

				found = 1;
				break;
			}	/* if */
			grep = grep->right;
		} while (grep != last);
		last = grep;

		/* Point all the left hand pointers in the carb ring to
		 * the motor ring.  We know there are no tag group heads
		 * to worry about.
		 */
		grep = cur;
		do {
			grep->left = last;
			grep = grep->right;
		} while (grep != cur);

	}	/* foreach */

	return GALLOC_OK;
}	/* ggiGA_basic_parse_list */



/* When we need to "start from the top" we copy the master haslist
 * back into a *pre-existing* copy of the haslist, without altering
 * the parsed list structure and keeping the pointers to the master
 * resources intact.
 */
static inline void reset_haslist(ggi_visual_t vis, 
				 ggiGA_resource_list haslist)
{
	struct ggiGA_resource res;
	ggiGA_resource_handle master;
	ggiGA_resource_handle slave;

	slave = RESLIST_FIRST(haslist);
	RESLIST_FOREACH(master, GA_HASLIST(vis)) {
		memcpy(&res, slave, sizeof(struct ggiGA_resource));
		if (slave->props != NULL) {
			LIB_ASSERT(master->props != NULL,
				"master resource has no properties. No target loaded?\n");
			memcpy(slave->props, master->props,
			       sizeof(struct ggiGA_resource_props));
		}	/* if */
		if (slave->priv != NULL) {
			LIB_ASSERT(master->priv != NULL,
				"master resource has no private data. No target loaded?\n");
			memcpy(slave->priv, master->priv,
			       slave->priv_size);
		}	/* if */

		memcpy(slave, master, sizeof(struct ggiGA_resource));
		slave->master = master;
		slave->props  = res.props;
		slave->priv   = res.priv;
		slave->left   = res.left;
		slave->right  = res.right;

		if (ggiGAIsTagHead(&res)) {
			ggiGA_taghead_t th_slave, th_res;
			th_slave = ggiGATagGetHead(slave);
			th_res = ggiGATagGetHead(&res);

			th_slave->left2 = th_res->left2;
			th_slave->right2 = th_res->right2;
		}	/* if */
		RESLIST_NEXT(slave) = RESLIST_NEXT(&res);

		slave = RESLIST_NEXT(slave);
	}	/* foreach */
}	/* reset_haslist */


/* Approximately as above, for the reslist.
 */
static inline void reset_reqlist(ggiGA_resource_list origreq, 
				 ggiGA_resource_list reqlist)
{
	struct ggiGA_resource res;
	ggiGA_resource_handle tmp_origreq, tmp_reqlist;

	tmp_reqlist = RESLIST_FIRST(reqlist);
	RESLIST_FOREACH(tmp_origreq, origreq) {
		memcpy(&res, tmp_reqlist, sizeof(struct ggiGA_resource));
		if (tmp_reqlist->props != NULL) {
			LIB_ASSERT(tmp_origreq->props != NULL,
				"request has no properties?\n");
			memcpy(tmp_reqlist->props, tmp_origreq->props,
			       sizeof(struct ggiGA_resource_props));
		}	/* if */
		if (tmp_reqlist->priv != NULL) {
			LIB_ASSERT(tmp_origreq->priv != NULL,
				"request has no private data?\n");
			memcpy(tmp_reqlist->priv, tmp_origreq->priv,
			       tmp_reqlist->priv_size);
		}	/* if */

		memcpy(tmp_reqlist, tmp_origreq, sizeof(struct ggiGA_resource));
		tmp_reqlist->master = res.master;
		tmp_reqlist->props  = res.props;
		tmp_reqlist->priv   = res.priv;
		tmp_reqlist->left   = res.left;
		tmp_reqlist->right  = res.right;

		if (ggiGAIsTagHead(&res)) {
			ggiGA_taghead_t th_reqlist, th_res;
			th_reqlist = ggiGATagGetHead(tmp_reqlist);
			th_res = ggiGATagGetHead(&res);

			th_reqlist->left2 = th_res->left2;
			th_reqlist->right2 = th_res->right2;
		}	/* if */
		RESLIST_NEXT(tmp_reqlist) = RESLIST_NEXT(&res);
		ggiGAFlagFailed(tmp_reqlist);

		tmp_reqlist = RESLIST_NEXT(tmp_reqlist);
	}	/* foreach */
}	/* reset_reqlist */


/* As above, but only do so for the named resource (in reqlist) */
static inline void reset_reqres(ggiGA_resource_list origreqlist, 
				ggiGA_resource_list reqlist,
				ggiGA_resource_handle req)
{
	struct ggiGA_resource res, *origreq;

	LIB_ASSERT(origreqlist != NULL, "origreqlist == NULL");
	LIB_ASSERT(reqlist != NULL, "reqlist == NULL");
	LIB_ASSERT(req != NULL, "req == NULL");

	origreq = ggiGAHandle(origreqlist, reqlist, req);
	LIB_ASSERT(origreq != NULL, "origreq == NULL");

	memcpy(&res, req, sizeof(struct ggiGA_resource));
	if (req->props != NULL) {
		LIB_ASSERT(origreq->props != NULL,
			"request has no properties?\n");
		memcpy(req->props, origreq->props, 
			sizeof(struct ggiGA_resource_props));
	}	/* if */
	if (req->priv != NULL) {
		LIB_ASSERT(origreq->priv != NULL,
			"request has no private data?\n");
		memcpy(req->priv, origreq->priv, req->priv_size);
	}	/* if */

	memcpy(req, origreq, sizeof(struct ggiGA_resource));
	req->master = res.master;
	req->props  = res.props;
	req->priv   = res.priv;
	req->left   = res.left;
	req->right  = res.right;

	if (ggiGAIsTagHead(&res)) {
		ggiGA_taghead_t th_req, th_res;
		th_req = ggiGATagGetHead(req);
		th_res = ggiGATagGetHead(&res);

		th_req->left2 = th_res->left2;
		th_req->right2 = th_res->right2;
	}	/* if */

	RESLIST_NEXT(req) = RESLIST_NEXT(&res);
	ggiGAFlagFailed(req);
}	/* reset_reqres */


/* The state and target-private state also need to be reset when the
 * list is about to be retraversed.
 */
static void reset_state(struct galloc_context *ctxt,
			struct ggiGA_template_state *state,
			void *privstate)
{
	if (ctxt->ops->reset_privstate
	   && ctxt->ops->reset_privstate(ctxt, state, privstate))
	{
		return;
	}	/* if */

	state->mode = NULL;
	return;
}	/* reset_state */


static void migrate_state(struct galloc_context *ctxt,
			  struct ggiGA_template_state *state,
			  void *privstate, ggiGA_resource_list from,
			  ggiGA_resource_list to)
{
	if (ctxt->ops->migrate_privstate
	   && !(ctxt->ops->migrate_privstate(ctxt, state, privstate,from,to)))
	{
		return;
	}	/* if */
}	/* migrate_state */


#define find_taghead(list, cthis) ggiGAFindTagHeadInList(list, cthis)
#define find_compound(list,res) ggiGAFindCompoundHead(list, res)

/* This function checks to see if has in haslist, which is a proposed 
 * master resource for res in reslist, would place res in the "correct"
 * haslist tag group/compound as determined by resources which res 
 * is attached to.  The rules defining "correct" are a bit complex:
 */
static inline int do_fit(ggiGA_resource_list reqlist,
			 ggiGA_resource_handle req,
			 ggiGA_resource_list haslist,
			 ggiGA_resource_handle has)
{
	ggiGA_resource_handle has_tmp, req_tmp;

	/* All resources before req (e.g. resources which
	 * have already been checked) which are in req's 
	 * compound must have masters in the corresponding
         * compound in haslist (that is, the compound which
	 * has belongs to.)
	 *
	 * Since resources prior to this have succeeded
	 * this test already, it is only necessary to 
	 * do the following check:
	 */
	req_tmp = find_compound(reqlist, req);
	has_tmp = find_compound(haslist, has);

	if ((req_tmp != req) &&
	    (find_compound(haslist, req_tmp->master) != has_tmp)) return -1;

	/* All resources before and including req which are in 
	 * the same ring as req must have masters in the corresponding
         * ring in haslist (that is, the ring that has belongs to.)
	 *
	 * Since resources prior to this have succeeded
	 * this test already, it is only necessary to 
	 * check one such resource.  But first we must find it.
	 * (It cannot be the tag group head.)
	 */
	req_tmp = req;
	do {
		/* Skip past the tag group head of the ring. */
		if (!ggiGACompareTag(req_tmp, req)) {
			LIB_ASSERT(ggiGAIsTagHead(req_tmp), "must be a tag head");
			req_tmp = ggiGATagHeadGetRight(req_tmp);
			continue;
		}	/* if */
		req_tmp = req_tmp->right;
		if (!ggiGAIsFailed(req_tmp)) break;
	} while (req_tmp != req);

	if (req_tmp == req) goto alone;
	has_tmp = has;
	do {
		if (req_tmp->master == has_tmp) break;

		/* Skip past the tag group head of the ring. */
		if (!ggiGACompareTag(has_tmp, has)) {
			LIB_ASSERT(ggiGAIsTagHead(has_tmp), "must be a tag head");
			has_tmp = ggiGATagHeadGetRight(has_tmp);
			continue;
		}	/* if */
		has_tmp = has_tmp->right;
	} while (has_tmp != has);
	if (req_tmp->master != has_tmp) return -1;

	/* If the req's left pointer is pointing out if its ring, 
	 * we check to make sure that ring it is hooked to corresponds
	 * to the ring that has's left pointer points to.  (That is, 
	 * all the resources in that ring have masters which are in the 
	 * corresponding ring in haslist ring.
	 *
	 * Again, this test only applies to resources which have
	 * already passed a check; unchecked resources are ignored.
	 */
 alone:
	req_tmp = req->left;
	do {
		if (req_tmp == req) return GALLOC_OK; /* ring is 2-way */

		/* Skip past the tag group head of the ring. */
		if (!ggiGACompareTag(req_tmp, req)) {
			LIB_ASSERT(ggiGAIsTagHead(req_tmp), "must be a tag head");
			req_tmp = ggiGATagHeadGetRight(req_tmp);
			continue;
		}	/* if */
		req_tmp = req_tmp->right;
	} while (req_tmp != req->left);

	/* Find a pointer in the ring that has already been checked */
	if (!ggiGAIsFailed(req->left)) goto skip;
	req_tmp = req->left;
	do {
		if (!ggiGAIsFailed(req_tmp)) break;

		/* Skip past the tag group head of the ring. */
		if (!ggiGACompareTag(req_tmp, req)) {
			LIB_ASSERT(ggiGAIsTagHead(req_tmp), "must be a tag head");
			req_tmp = ggiGATagHeadGetRight(req_tmp);
			continue;
		}	/* if */
		req_tmp = req_tmp->right;
	} while (req_tmp != req->left);

	/* If there are no successful resources yet in that ring, we will
	 * give the requested resource the benefit of the doubt.
	 *
	 * However, right now this case is not handled elsewhere in the code, 
	 * so we complain.
	 */
	LIB_ASSERT(req_tmp != req->left, "beware: unchecked ring!");
	if (req_tmp == req->left) return GALLOC_OK;
 skip:
	/* Finally, we look for its master in the haslist */
	has_tmp = has->left;
	do {
		if (req_tmp->master == has_tmp) break; 

		/* Simply traverse through the tag group heads of the ring. */
		if (!ggiGACompareTag(has_tmp, has)) {
			LIB_ASSERT(ggiGAIsTagHead(has_tmp), "must be a tag head");
			has_tmp = ggiGATagHeadGetRight(has_tmp);
			continue;
		}	/* if */
		has_tmp = has_tmp->right;
	} while (has_tmp != has->left);
	if (req_tmp->master != has_tmp) return -1;

	return GALLOC_OK;
}	/* do_fit */



#define CALLBACK_ARGS \
struct galloc_context *ctxt,		\
ggiGA_resource_list reslist,		\
enum ggiGA_callback_command command,	\
ggiGA_resource_handle res,		\
ggiGA_resource_handle head,		\
ggiGA_taghead_t taghead,		\
ggiGA_resource_handle has,		\
struct ggiGA_template_state *state,	\
void *privstate

#define CALLBACK_PASSARGS \
ctxt, reslist, command, res, head, taghead, has, state, privstate

/* Callback return values: 
 * 0  = OK
 * bit 0 set  = storage/state allocations mangled, need to be reset/"rechecked"
 * bit 1 set  = resource props have been altered and need reset.
 * any other bits set = some other error also occurred.
 *
 * Some nuances:
 * GA_CB_MATCH should never return with bit 0 or 1 set.
 * GA_CB_PRECHECK and GA_CB_CHECK should never return with bit 0 set.
 * GA_CB_POSTCHECK, GA_CB_SET, GA_CB_RECHECK should normally return 0.
 */
static inline int do_callback(CALLBACK_ARGS) 
{
	ggiGA_reslist_callback *cb;
	cb = NULL;

	LIB_ASSERT(has != NULL, "has == NULL");
	if (has->cbflags1 & command) {
		cb = has->cb1;
	} else if (has->cbflags2 & command) {
		cb = has->cb2;
	} else {
		int i = 0;

		do {
			if ((command >> i) & 1) break;
		} while (++i < 32);
		if (i >= 32) return GGI_EARGINVAL;
		cb = ctxt->ops->default_callbacks[i];
	}	/* if */

	if (cb == NULL) return 4;
	return cb(CALLBACK_PASSARGS);
}	/* do_callback */




int ggiGACheck_template (ggi_visual_t vis, 
			 ggiGA_resource_list request,
			 ggiGA_resource_list *result, 
			 struct ggiGA_template_state *state,
			 void *privstate)
{
	ggiGA_resource_list haslist = NULL;
	ggiGA_resource_list reqlist = NULL;

	ggiGA_resource_handle tmp1 = NULL;
	ggiGA_resource_handle tmp2 = NULL;
	ggiGA_resource_list reslist = NULL;
	ggiGA_resource_handle farthest = NULL;
	int at_farthest = 1;
	int need_reset = 0;

	int rc = GALLOC_OK;


	if (request == NULL || RESLIST_EMPTY(request)) {

		/* ggiGAGet -- begin */

		if (result == NULL) return GGI_EARGINVAL;

		if (GA_RESLIST(vis) == NULL) {
			/* Visual's mode either unset or ggiSetMode used */
			ggi_mode mode;

			DPRINT("Adding implicite GA_RT_FRAME\n");

			/* This is where the mode resource magically appears */
			rc = ggiGetMode(vis, &mode);
			LIB_ASSERT(rc == GALLOC_OK, "ggiGetMode() failed");

			tmp1 = NULL;
			rc = ggiGAAddMode(vis, &(GA_RESLIST(vis)),
					  &mode, NULL, &tmp1);
			LIB_ASSERT(rc == GALLOC_OK, "Could not add a mode");
			if (rc != GALLOC_OK) return GALLOC_EFAILED;

			ggiGAFlagActive(tmp1);
			tmp1->vis = vis;

			ggiGA_basic_parse_list(GA_RESLIST(vis));

		}	/* if */

		rc = ggiGACopyList(GA_RESLIST(vis), result);
		LIB_ASSERT(rc == GALLOC_OK, "Could not copy result list");
		return rc;

		/* ggiGAGet -- end */

	}	/* if */

	/* Make a copy of the haslist, with links back to the master copy. */
	rc = ggiGACopyList(GA_HASLIST(vis), &haslist);
	LIB_ASSERT(rc == GALLOC_OK, "Could not copy haslist");
	if (rc != GALLOC_OK) return rc;

	ggiGA_basic_parse_list(haslist);
	reset_haslist(vis, haslist);
	reset_state(GALLOC_CONTEXT(vis), state, privstate);

	state->haslist = haslist;

	/* Make a copy of the request, with cleared master links. */
	rc = ggiGACopyList(request, &reqlist);
	LIB_ASSERT(rc == GALLOC_OK, "Could not copy request list");

	if (rc != GALLOC_OK) return rc;
	ggiGA_basic_parse_list(reqlist);
	RESLIST_FOREACH(tmp1, reqlist) {
		tmp1->master = NULL;
	}	/* foreach */

#if 0
	reset_reqlist(request, reqlist);
	do {
		char *foo = NULL;

		ggiGAanprintf(vis, haslist, (size_t)9000, "", &foo);
		fprintf(stderr, "haslist = \n%s\n", foo);
		free(foo);
		foo = NULL;
		ggiGAanprintf(vis, reqlist, (size_t)9000, "", &foo);
		fprintf(stderr, "reqlist = \n%s\n", foo);
		free(foo);
	} while (0);
#endif

#define CALLBACK_REQ(_cbflag) do_callback(GALLOC_CONTEXT(vis), reqlist, _cbflag, \
	tmp2, compound, taghead, tmp2->master, state, privstate)

#define CALLBACK_HAS(_cbflag) do_callback(GALLOC_CONTEXT(vis), haslist, _cbflag, \
	tmp1, compound, taghead, tmp2, state, privstate)

	tmp1 = RESLIST_FIRST(reqlist);
	reset_reqlist(request, reqlist);
	reset_haslist(vis, haslist);

	while (tmp1 != NULL) {
		ggiGA_taghead_t taghead = NULL;
		ggiGA_resource_handle compound = NULL;

		if (need_reset & 2) {
			reset_reqlist(request, reqlist);
			reset_haslist(vis, haslist);
			reset_state(GALLOC_CONTEXT(vis), state, privstate);
			tmp2 = RESLIST_FIRST(reqlist);

			while (tmp2 != tmp1) {
				taghead = find_taghead(reqlist, tmp2);
				compound = find_compound(reqlist, tmp2);

				if (tmp2->res_state & GA_STATE_NORESET) {
					goto nocheck1;
				}	/* if */

				rc = CALLBACK_REQ(GA_CB_MATCH);
				LIB_ASSERT(rc == GALLOC_OK, "Callback match failed");
				rc = CALLBACK_REQ(GA_CB_PRECHECK);
				LIB_ASSERT(rc == GALLOC_OK, "Callback pre-check failed");
				rc = CALLBACK_REQ(GA_CB_CHECK);
				LIB_ASSERT(rc == GALLOC_OK, "Callback check failed");

			nocheck1:
				ggiGAFlagSuccess(tmp2);
				if (ggiGAIsCap(RESLIST_NEXT(tmp2))) {
					ggiGAFlagSuccess(tmp2);
				}	/* if */

				rc = CALLBACK_REQ(GA_CB_POSTCHECK);
				LIB_ASSERT(rc == GALLOC_OK, "Callback post-check failed");

				tmp2 = ggiGAGetNextSkipCap(tmp2);
			}	/* while */
			need_reset &= ~3;
		}	/* if */

		if (need_reset & 1) {
			reset_state(GALLOC_CONTEXT(vis), state, privstate);
			tmp2 = RESLIST_FIRST(reqlist);

			while (tmp2 != tmp1) {
				taghead = find_taghead(reqlist, tmp2);
				compound = find_compound(reqlist, tmp2);
				rc = CALLBACK_REQ(GA_CB_RECHECK);
				LIB_ASSERT(rc == GALLOC_OK, "Callback re-check failed");

				tmp2 = ggiGAGetNextSkipCap(tmp2);
			}	/* while */
			need_reset &= ~1;
		}	/* if */

		taghead = find_taghead(reqlist, tmp1);
		compound = find_compound(reqlist, tmp1);
		tmp2 = (tmp1->master == NULL) ?
			RESLIST_FIRST(haslist) : RESLIST_NEXT(tmp1->master);
		rc = 0;
		while (tmp2 != NULL) {
			if (rc & 2) {
				if (at_farthest) {
					/* Take a snapshot for the result */
					ggiGACopyList(reqlist, &reslist);
				}	/* if */
				reset_reqres(request, reqlist, tmp1);
				at_farthest = 2;
			}	/* if */
			if (rc & 1) {
				ggiGA_resource_handle tmp3;
				reset_state(GALLOC_CONTEXT(vis), state, privstate);
				tmp3 = RESLIST_FIRST(reqlist);
				while (tmp3 != tmp1) {
					taghead = find_taghead(reqlist,tmp3);
					compound = find_compound(reqlist,tmp3);
					rc = CALLBACK_REQ(GA_CB_RECHECK);
					LIB_ASSERT(rc == GALLOC_OK, "Callback re-check failed");

					tmp3 = ggiGAGetNextSkipCap(tmp3);
				}	/* while */
			}	/* if */

			rc = 0;
			if (CALLBACK_HAS(GA_CB_MATCH)) goto nexthas;
			if (tmp2->res_state & GA_STATE_NORESET) {
				goto nocheck2;
			}	/* if */

			rc = CALLBACK_HAS(GA_CB_PRECHECK);
			if (rc) goto nexthas;
			if (do_fit(reqlist, tmp1, haslist, tmp2)) {
				goto nexthas;
			}	/* if */

			rc = CALLBACK_HAS(GA_CB_CHECK);
			if (rc) goto nexthas;

		nocheck2:
			rc = CALLBACK_HAS(GA_CB_POSTCHECK);
			if (rc) goto nexthas;
			ggiGAFlagSuccess(tmp1);
			if (ggiGAIsCap(RESLIST_NEXT(tmp1))) {
				ggiGAFlagSuccess(RESLIST_NEXT(tmp1));
			}	/* if */
			tmp1->master = tmp2;
			break;

		nexthas:
			tmp2 = ggiGAGetNextSkipCap(tmp2);
		}	/* while */

		if (tmp2 == NULL) {	/* No match */
			if (at_farthest == 1) ggiGACopyList(reqlist, &reslist);
			if (rc & 2) {
				if (at_farthest) {
					/* Take a snapshot for the result */
					ggiGACopyList(reqlist, &reslist);
				}	/* if */
				reset_reqres(request, reqlist, tmp1);
				at_farthest = 2;
			}	/* if */

			if (rc & 1) {
				reset_state(GALLOC_CONTEXT(vis), state, privstate);
				tmp2 = RESLIST_FIRST(reqlist);
				while (tmp2 != tmp1) {
					taghead = find_taghead(reqlist,tmp2);
					compound = find_compound(reqlist,tmp2);
					rc = CALLBACK_REQ(GA_CB_RECHECK);
					LIB_ASSERT(rc == GALLOC_OK, "Callback re-check failed");

					tmp2 = ggiGAGetNextSkipCap(tmp2);
				}	/* while */
			}	/* if */

			if (tmp1 != RESLIST_FIRST(reqlist)) {
				tmp1->master = NULL;
				tmp2 = RESLIST_FIRST(reqlist);
				while (RESLIST_NEXT(tmp2) != tmp1 &&
					(!ggiGAIsCap(RESLIST_NEXT(tmp2)) ||
					RESLIST_NEXT(RESLIST_NEXT(tmp2)) != tmp1))
				{
					tmp2 = RESLIST_NEXT(tmp2);
				}	/* while */
			}	/* if */


			fprintf(stderr, "rolling back to %p\n", (void *)tmp2);
			if (at_farthest)  {
				/* Take a snapshot for the result */
				ggiGACopyList(reqlist, &reslist);
			}	/* if */

			tmp1 = tmp2;
			at_farthest = 0;
			if (tmp2 != NULL) {
				ggiGAFlagFailed(tmp2);
				taghead = find_taghead(reqlist, tmp1);
				compound = find_compound(reqlist, tmp1);
				tmp2 = RESLIST_NEXT(tmp2);
				while (tmp2 != NULL) {
					tmp2->master = NULL;
					tmp2 = RESLIST_NEXT(tmp2);
				}	/* while */

				LIB_ASSERT(tmp1->master != NULL, "must be a master");
				need_reset = do_callback(GALLOC_CONTEXT(vis),
							 reqlist, 
							 GA_CB_UNCHECK,
							 tmp1, compound, 
							 taghead, 
							 tmp1->master, 
							 state, privstate);
				if (tmp1->res_state & GA_STATE_MODIFIED) {
					reset_reqres(request, reqlist, tmp1);
				}	/* if */
			}	/* if */
		} else {
			if (at_farthest) {
				farthest = tmp1;
				/* Take a snapshot for the result */
				ggiGACopyList(reqlist, &reslist);
			} else if (tmp1 == farthest) {
				at_farthest = 1;
			}	/* if */

			tmp1 = ggiGAGetNextSkipCap(tmp1);
		}	/* if-else */
	}	/* while */
	if (tmp2 == NULL) rc = GALLOC_EUNAVAILABLE;

	/* Done with the copy of the request */
	migrate_state(GALLOC_CONTEXT(vis), state, privstate, reqlist, reslist);
	ggiGADestroyList(&reqlist);

	/* Now we build the result: reslist contains the list
	 * at the point that the most resources were successful.
	 * but all the resources in it have been altered, even if
	 * they had the GGI_GA_NOCHANGE flag set.  We copy the
	 * original reqlist and then copy the contents of the
	 * resources that should change into this list, and return
	 * it.
	 */
	if (result == NULL) goto out_noresult;

	/* Check for the handle-preserving special case. */
	if (request != result[0]) {
		ggiGACopyList(request, result);
	}	/* if */

	tmp2 = RESLIST_FIRST(result[0]);
	RESLIST_FOREACH(tmp1, reslist) {
		struct ggiGA_resource_props *props;
		ggiGA_resource_handle next;
		void *priv;

		/* The resource part is always copied */
		props = tmp2->props;
		priv  = tmp2->priv;
		next  = RESLIST_NEXT(tmp2);
		memcpy (tmp2, tmp1, sizeof(struct ggiGA_resource));
		tmp2->props  = props;
		tmp2->priv   = priv;
		RESLIST_NEXT(tmp2) = next;
		tmp2->right  = NULL;
		tmp2->left   = NULL;

		/* Master links provided for ggiGASet -- could be cleared 
		 * by target before handing back to user if we want to 
		 * be more opaque.
		 */
		if (tmp2->master) tmp2->master = tmp2->master->master;
		if (ggiGAIsTagHead(tmp2)) {
			ggiGA_taghead_t th_tmp2;
			th_tmp2 = ggiGATagGetHead(tmp2);

			th_tmp2->left2 = NULL;
			th_tmp2->right2 = NULL;
		}	/* if */
		if (tmp2->res_state & GA_STATE_NOCHANGE) goto nochange;
		if (props) memcpy(tmp2->props, tmp1->props, 
				  sizeof(struct ggiGA_resource_props));
		if (priv) memcpy(tmp2->priv, tmp1->priv, tmp2->priv_size);

	nochange:
		if (ggiGAIsFailed(tmp1)) break; /* Quit after first failure */
		tmp2 = RESLIST_NEXT(tmp2);
	}	/* foreach */

 out_noresult:
	do {
		char *foo = NULL;
		ggiGAanprintf(vis, reslist, (size_t)9000, "", &foo);
#ifdef DEBUG
		fprintf(stderr, "internal res(ult)list = \n%s\n", foo);
#endif
		free(foo);
	} while (0);

	if (result && result[0]) {
		do {
			char *foo = NULL;
			ggiGAanprintf(vis, result[0], (size_t)9000, "", &foo);
#ifdef DEBUG
	 		fprintf(stderr, "result = \n%s\n", foo);
#endif
			free(foo);
		} while (0);
	}	/* if */

	/* In case we are being called by the Set template, we need
	 * to allow any handles in the state to be translated. 
	 */
	if (result && result[0]) {
		migrate_state(GALLOC_CONTEXT(vis),
				state, privstate, reslist, result[0]);
	}	/* if */

	ggiGADestroyList(&reslist);

	return rc;
}	/* ggiGACheck_template */


int ggiGASet_template(ggi_visual_t vis,
		      ggiGA_resource_list request,
		      ggiGA_resource_list *result,
		      struct ggiGA_template_state *state,
		      void *privstate)
{
	ggiGA_resource_list reqlist = NULL;
	ggiGA_resource_list haslist = NULL;
	ggiGA_resource_list resultlist = NULL;
	int rc = GALLOC_OK;
	ggiGA_resource_list active = NULL;
	ggiGA_resource_handle tmp, compound, appendat;
	ggiGA_taghead_t taghead;

	APP_ASSERT(request != NULL, "request == NULL");
	if (request == NULL) return GALLOC_ESPECIFY;

	if (result != NULL) {
		resultlist = result[0];
	}	/* if */

	/* Decide if handle-preserving special case or not */
	if (request == resultlist) {
		reqlist = request; /* Work directly with user handles. */
	} else {
		ggiGACopyList(request, &reqlist); /* Work with a copy */
	}	/* if */


	/* Ignore/turn off GA_STATE_NOCHANGE_FLAGS */
	RESLIST_FOREACH(tmp, reqlist) {
		tmp->res_state &= ~GA_STATE_NOCHANGE;
	}	/* foreach */

	rc = ggiGACheck_template(vis, reqlist, &resultlist, state, privstate);

	if ((result != NULL) && (result[0] == NULL)) {
		result[0] = resultlist;
	}	/* if */

	/* Don't need this anymore (but don't empty result in special case.) */
	if (reqlist != request) ggiGADestroyList(&reqlist);

	/* Just returning here gives identical behavior to ggiGACheck,
	 * except for handling of immutable resources.  No fuss, no muss.
	 */
	if (rc != GALLOC_OK) {
		goto err0;
	}

	/* request was cleaned of parsing results, so recreate them. */
	ggiGA_basic_parse_list(resultlist);

	/* We need the masters referred to by the resources in result */
	haslist = state->haslist;

	/* Nuance: currently when adding resources to the active
	 * list, the new resources must be at the end of the list,
	 * and the list must contain a copy of the active resources
	 * up to the point where the new resources begin.
	 *
	 * Even with these restrictions it is a bit tricky to
	 * figure out where the old resources end and the new
	 * ones begin, because new mutual resources will be flagged
	 * active also.  They can be told apart because their properties
	 * are at the same memory location.
	 */
	active = GA_RESLIST(vis);
	if (active != NULL) {
		appendat = RESLIST_FIRST(active);
	} else {
		appendat = NULL;
	}
	tmp = RESLIST_FIRST(resultlist);
	while ((tmp != NULL) && (appendat != NULL)) {
		ggiGA_resource_handle mutual;

		if (!(tmp->res_state & GA_STATE_NORESET)) break;

		mutual = ggiGAPropsHandle(resultlist, tmp);

		if ((mutual != NULL) && (mutual != tmp) &&
		    (ggiGAPropsHandle(active, appendat)
		     != ggiGAHandle(active, resultlist, mutual)))
		{
			break;
		}

		tmp = RESLIST_NEXT(tmp);
		appendat = RESLIST_NEXT(appendat);
	};

	/* The above code block has advanced appendat and tmp to the 
	 * point at which we want to splice the lists together.  The 
	 * resources before appendat (in active) will be kept.  The resources
	 * after and including tmp (in resultlist) will be pasted onto
	 * the end of active.  But before we do that, we need to release
	 * any resources that remain after appendat.
	 */
	if (appendat != NULL) {
		ggiGAReleaseList(vis, &GA_RESLIST(vis), &appendat);
	}	/* if */

	active = GA_RESLIST(vis);
	if (active != NULL) {
		appendat = RESLIST_FIRST(active);
	} else {
		appendat = NULL;
	}

	RESLIST_FOREACH(tmp, resultlist) {
		if (ggiGAIsCap(tmp)) goto setact;

		taghead = find_taghead(resultlist, tmp);
		compound = find_compound(resultlist, tmp);

		LIB_ASSERT(tmp->master != NULL, "must be a master");
		rc = do_callback(GALLOC_CONTEXT(vis), resultlist, GA_CB_SET,
				  tmp, compound, taghead,
				  tmp->master, state, privstate);
		DPRINT("%s:%s:%d: do_callback() returned with rc = %i\n",
			     DEBUG_INFO, rc);
		if (rc != GALLOC_OK) {
			ggiGAFlagFailed(tmp);
			break;
		}	/* if */
setact:
		tmp->vis = vis;
		if (appendat == NULL) {		/* New resource. */
			ggiGA_resource_handle tmp2;
			ggiGAFlagActive(tmp);

			tmp2 = ggiGAPropsHandle(resultlist, tmp);
			if (tmp2 == tmp) {
				ggiGAAppend(tmp, &active, 1);
			} else {
				tmp2 = ggiGAHandle(active, resultlist, tmp2);
				ggiGAAddMutual(active, &tmp2);
				/* The new mutual is flagged failed. */
				ggiGAFlagSuccess(tmp2);
			}	/* if */
		}	/* if */

		if (appendat) appendat = RESLIST_NEXT(appendat);
	}	/* foreach */


	if (rc != GALLOC_OK) {
		goto err0;
	}

	ggiGA_basic_parse_list(active);	/* Keep this parsed for Release. */
	GA_RESLIST(vis) = active;

#ifdef DEBUG
	do {
		char *foo = NULL;
		ggiGAanprintf(vis, GA_RESLIST(vis), (size_t)9000, "", &foo);
		fprintf(stderr, "setresult = \n%s\n", foo);
		free(foo);
	} while (0);
#endif

	/* Clean up the result list for use by the caller, by removing parsed 
	 * state and master links which they have no business touching.
	 */
	RESLIST_FOREACH(tmp, resultlist) {
		struct ggiGA_resource_props *props;

		props = tmp->props;
		tmp->right  = NULL;
		tmp->left   = NULL;
		tmp->master = NULL;

		if (ggiGAIsTagHead(tmp)) {
			ggiGA_taghead_t th_tmp;
			th_tmp = ggiGATagGetHead(tmp);

			th_tmp->left2 = NULL;
			th_tmp->right2 = NULL;
		}	/* if */
	}	/* foreach */

	DPRINT("%s:%s:%d: rc = %i\n", DEBUG_INFO, rc);
	if (rc != GALLOC_OK) {
		goto err0;
	}	/* if */

	if (result == NULL) {
		/* also happens in normal case */
		ggiGADestroyList(&resultlist);
	}

	return rc;

err0:
	if (result == NULL) {
		ggiGADestroyList(&resultlist);
	}	/* if */

	return rc;
}	/* ggiGASet_template */



int ggiGARelease_template(ggi_visual_t vis, ggiGA_resource_list *reqlist,
			  ggiGA_resource_handle *handle, 
			  struct ggiGA_template_state *state,
			  void *privstate)
{
	ggiGA_resource_handle res = NULL;

	ggiGA_resource_handle tmp1 = NULL;

	int rc = GALLOC_OK;
	
	if (reqlist == NULL) return GGI_EARGINVAL;
	if (handle == NULL) return GGI_EARGINVAL;
	if (handle[0] == NULL) return GALLOC_ESPECIFY;

	/* ggiGAHandle takes care of verifying handle is in reqlist. 
	 * It also takes care of the case where GA_RESLIST(vis) is NULL,
	 * (which is an error by the application.)
	 */
	res = ggiGAHandle(GA_RESLIST(vis), reqlist[0], handle[0]);
	if (res == NULL) return GGI_EARGINVAL;

	/* res now points to the actual resource we want to free. */

	/* GA_RESLIST(vis) is already parsed.  So all we need to do
	 * is check to see if the resource is OK to remove. 
	 */

	/* It is not OK to remove a resource when something outside of 
	 * its ring is pointing to it.  This will only happen with .left.
	 */
	tmp1 = RESLIST_FIRST(GA_RESLIST(vis));
	while (tmp1 != NULL) {
		if ((tmp1 != res)
		  && (!ggiGAInTagGroup(tmp1, ggiGA_TAG(res)))
		  && (tmp1->left == res))
		{
			break;
		}
		tmp1 = ggiGAGetNextSkipCap(tmp1);
	}	/* while */
	if (tmp1 != NULL) return GGI_EBUSY;

	/* Tag Heads cannot be released unless they are the only
	 * member of their subordinate tag group.
	 */
	if (ggiGAIsTagHead(res)) {
		if (ggiGATagHeadGetLeft(res) != res || 
		    ggiGATagHeadGetRight(res) != res) return GGI_EBUSY;
	}	/* if */

	/* Other than the above, there are no hard and fast rules, so
	 * we must leave it up to the target as to whether the resource
	 * may be released.
	 */
	LIB_ASSERT(res->master != NULL, "there or this must be a master");
	rc = do_callback(GALLOC_CONTEXT(vis), GA_RESLIST(vis), GA_CB_RELEASE,
			 res, ggiGAFindCompoundHead(GA_RESLIST(vis), res),
			 ggiGAFindTagHeadInList(GA_RESLIST(vis), res),
			 res->master, state, privstate);
	if (rc) return rc;

	/* We have been given approval to remove this resource from the
	 * lists. 
	 */
        rc = ggiGARemoveWithCap(&(GA_RESLIST(vis)), &res);
	LIB_ASSERT(rc == GALLOC_OK, "ggiGARemoveWithCap() failed");
        if (rc) ggiGARemoveWithCap(reqlist, handle);
	else rc = ggiGARemoveWithCap(reqlist, handle);
	LIB_ASSERT(rc == GALLOC_OK, "ggiGARemoveWithCap() failed");

	ggiGA_basic_parse_list(GA_RESLIST(vis)); /* Keep this parsed. */

	return(rc);

}	/* ggiGARelease_template */






/* Default functions for callbacks implementing lowest common denominator */

/* The match function performs a basic type/bounds checking of the resource,
 * to see if it is just plain incompatible.  It does not alter the resource,
 * so it does not check any fields that feed back suggestions.
 */
int ggiGA_default_match(CALLBACK_ARGS)
{
	/* Don't process NULLs or Caps */
	if (res == NULL || has == NULL) return 4;
	if (ggiGAIsCap(res) || ggiGAIsCap(has)) return 4;

	/* Check resource variant */
	if (((ggiGAIsCarb(res) << 1) | ggiGAIsMotor(res)) !=
	    ((ggiGAIsCarb(has) << 1) | ggiGAIsMotor(has))) return 4;

	/* Check resource type */
	if (ggiGA_TYPE(res) != ggiGA_TYPE(has)) return 4;
	if (ggiGA_SUBTYPE(res) && 
	    (ggiGA_SUBTYPE(res) != ggiGA_SUBTYPE(has))) return 4;

	/* Anything else to check at this stage? */

	return 0;
}	/* ggiGA_default_match */


#define _GA_TEMPLATES_INITVARS(subtype)			\
	res_c = RESLIST_NEXT(res);			\
	has_c = RESLIST_NEXT(has);			\
	res_c_p = res_p = res->props;			\
	res_c_sp = res_sp = &(res_p->sub.subtype);	\
	has_c_p = has_p = has->props;			\
	has_c_sp = has_sp = &(has_p->sub.subtype);	\
	if (ggiGAIsCap(RESLIST_NEXT(res))) {		\
		res_c = RESLIST_NEXT(res);		\
		res_c_p = res_c->props;			\
		res_c_sp = &(res_c_p->sub.subtype);	\
	}						\
	if (ggiGAIsCap(RESLIST_NEXT(has))) {		\
		has_c = RESLIST_NEXT(has);		\
		has_c_p = has_c->props;			\
		has_c_sp = &(has_c_p->sub.subtype);	\
	}

/* The precheck callback checks values for basic compatibility,
 * and does send back suggestions... so, it picks up where the
 * match callback left off.  We know that any resource that gets 
 * to the precheck callback has succeeded the match callback.
 * However, we are still only checking for basic incompatibility,
 * so the availability of storage (or other allocatable quantities) 
 * are not checked.
 */

static int ggiGA_default_motor_precheck (CALLBACK_ARGS)
{
	ggiGA_resource_handle res_c = NULL;
	ggiGA_resource_handle has_c = NULL;
	struct ggiGA_resource_props *res_p, *has_p, *res_c_p, *has_c_p;
	struct ggiGA_motor_props *res_sp, *has_sp, *res_c_sp, *has_c_sp;
	int rc;

	if (ggiGAMotorCapSanity(res) != GALLOC_OK) {
		return GALLOC_EFAILED;
	}	/* if */

	_GA_TEMPLATES_INITVARS(motor);

	rc = 0;

	rc = _ga_cmp_qty(res_p, res_c_p, has_p, has_c_p, rc);
	rc = _ga_cmp_storage(res_p, res_c_p, has_p, has_c_p, rc);

	return rc;
}	/* ggiGA_default_motor_precheck */


static int ggiGA_default_carb_precheck(CALLBACK_ARGS)
{
	ggiGA_resource_handle res_c = NULL;
	ggiGA_resource_handle has_c = NULL;
	struct ggiGA_resource_props *res_p, *has_p, *res_c_p, *has_c_p;
	struct ggiGA_carb_props *res_sp, *has_sp, *res_c_sp, *has_c_sp;

	if (ggiGACarbCapSanity(res) != GALLOC_OK) {
		return GALLOC_EFAILED;
	}	/* if */

	_GA_TEMPLATES_INITVARS(carb);

        return 0;
}	/* ggiGA_default_carb_precheck */


static int ggiGA_default_tank_precheck(CALLBACK_ARGS)
{
	ggiGA_resource_handle res_c = NULL;
	ggiGA_resource_handle has_c = NULL;
	struct ggiGA_resource_props *res_p, *has_p, *res_c_p, *has_c_p;
	struct ggiGA_tank_props *res_sp, *has_sp, *res_c_sp, *has_c_sp;

	if (ggiGATankCapSanity(res) != GALLOC_OK) {
		return GALLOC_EFAILED;
	}	/* if */
        
	if (ggiGAIsCap(RESLIST_NEXT(res))) {
		if (ggiGATankCapSanity(res) != GALLOC_OK) {
			return 4;
		}	/* if */

		res_c = RESLIST_NEXT(res);
		res_c_p = res_c->props; res_c_sp = &(res_c_p->sub.tank);
	}	/* if */

	_GA_TEMPLATES_INITVARS(tank);

	return 0;
}	/* ggiGA_default_tank_precheck */


static int ggiGA_default_mode_precheck (CALLBACK_ARGS)
{
	/* This can be used by targets to provide alternative resource
	 * trees for modes of different schemes/resolutions.
	 *
	 * Implementation of this can wait until such targets exist.
	 */

#ifdef DEBUG
	fprintf(stderr, "fixme: %s:%s:%d: not yet implemented\n",
		DEBUG_INFO);
#endif
	return 0;
}	/* ggiGA_default_mode_precheck */


int ggiGA_default_precheck (CALLBACK_ARGS)
{
	if (ggiGAIsTagHead(res)) {
		fprintf(stderr, "Tag group heads not yet implemented\n");
		return -1;
	}	/* if */

	if (ggiGAIsMotor(res)) {
		return ggiGA_default_motor_precheck(CALLBACK_PASSARGS);
	}	/* if */

	if (ggiGAIsCarb(res)) {
		return ggiGA_default_carb_precheck(CALLBACK_PASSARGS);
	}	/* if */

	if (ggiGA_FULLTYPE(res) == GA_RT_FRAME) {
		return ggiGA_default_mode_precheck(CALLBACK_PASSARGS);
	}	/* if */

	return ggiGA_default_tank_precheck(CALLBACK_PASSARGS);
}	/* ggiGA_default_precheck */


/* The check callback checks whether storage (or other allocatable 
 * quantities) is actually available, and sends back suggestions.
 * It does not actually mark the checked quantity as allocated.
 * We know that any resource that makes it to the check callback
 * has succeeded the precheck callback.
 *
 * The postcheck callback actually marks regions of storage as 
 * used (for the duration of the call to ggiGACheck, at least.)
 *
 * The recheck callback re-marks regions as used after
 * they have been temporarily "unchecked" by the uncheck callback,
 * or if the whole list is being rechecked from the top to
 * roll back to a previous state.  In the former case, the resource
 * will be marked as failed.
 *
 * These three callbacks will often share the same actual callback 
 * function, as they do here.
 *
 * It is known that the resource has passed precheck, and it has
 * passed the do_fit function before these callbacks are called.
 * 
 * NOTE: In-service resources *will* be passed to these callbacks, 
 * so they should be anticipated (and they can be told apart by
 * the GA_STATE_NORESET state flag).
 */
int ggiGA_default_motor_check (CALLBACK_ARGS)
{
	/* For now, we should not be getting any rechecks. */
	LIB_ASSERT (command != GA_CB_RECHECK, "may not perform a re-check");

#ifdef DEBUG
	fprintf(stderr, "fixme: %s:%s:%d: not yet implemented\n",
		DEBUG_INFO);
#endif

	return 0;
}	/* ggiGA_default_motor_check */


int ggiGA_default_carb_check(CALLBACK_ARGS)
{
	/* For now, we should not be getting any rechecks. */
	LIB_ASSERT (command != GA_CB_RECHECK, "may not perform a re-check");

#ifdef DEBUG
	fprintf(stderr, "fixme: %s:%s:%d: not yet implemented\n",
		DEBUG_INFO);
#endif

	return 0;
}	/* ggiGA_default_carb_check */


int ggiGA_default_tank_check (CALLBACK_ARGS)
{
	/* For now, we should not be getting any rechecks. */
	LIB_ASSERT (command != GA_CB_RECHECK, "may not perform a re-check");

	/* Here we will be checking with the storage handlers.
	 * If the resource in in service, we must choose the
	 * same exact storage location as it is using in the
	 * in-service storage state.
	 */

#ifdef DEBUG
	fprintf(stderr, "fixme: %s:%s:%d: not yet implemented\n",
		DEBUG_INFO);
#endif

	if (command != GA_CB_POSTCHECK) return GALLOC_OK;

	/* Here we will be reserving with the storage handlers.
	 * (Not in the actual in-service storage, rather in our
	 * hypothetical storage)
	 */

#ifdef DEBUG
	fprintf(stderr, "fixme: %s:%s:%d: not yet implemented\n",
		DEBUG_INFO);
#endif

	return 0;
}	/* ggiGA_default_tank_check */


int ggiGA_default_check (CALLBACK_ARGS)
{
	if (ggiGAIsTagHead(res)) {
		fprintf(stderr, "fixme: %s:%s:%d: Tag group heads not yet implemented\n",
			DEBUG_INFO);
		return -1;
	}	/* if */

	if (ggiGAIsMotor(res)) {
		return ggiGA_default_motor_check(CALLBACK_PASSARGS);
	}	/* if */

	if (ggiGAIsCarb(res)) {
		return ggiGA_default_carb_check(CALLBACK_PASSARGS);
	}	/* if */

	return ggiGA_default_tank_check(CALLBACK_PASSARGS);
}	/* ggiGA_default_check */


/* The uncheck callback removes any pre-allocation of storage that
 * the given resource caused.  The default is to force a full reset
 * and renegotiation of the previous resources in the request list,
 * so targets should flag and handle this case on their own.  This
 * default behavior is subject to change, so doubly so.
 */
int ggiGA_default_motor_uncheck(CALLBACK_ARGS)
{
	return 2;	/* Force full rerun */
}	/* ggiGA_default_motor_uncheck */
        
                
int ggiGA_default_carb_uncheck(CALLBACK_ARGS)
{
	return 2;	/* Force full rerun */
}	/* ggiGA_default_carb_uncheck */

 
int ggiGA_default_tank_uncheck(CALLBACK_ARGS)
{
	return 2;	/* Force full rerun */
}	/* ggiGA_default_tank_uncheck */


int ggiGA_default_uncheck (CALLBACK_ARGS)
{
#ifdef DEBUG
	fprintf(stderr, "default uncheck %p\n", (void *)res);
#endif

	if (ggiGAIsTagHead(res)) {
		fprintf(stderr, "fixme: %s:%s:%d: Tag group heads not yet implemented\n",
			DEBUG_INFO);
	}	/* if */

	if (ggiGAIsMotor(res)) {
		return ggiGA_default_motor_uncheck(CALLBACK_PASSARGS);
	}	/* if */
        
	if (ggiGAIsCarb(res)) {
		return ggiGA_default_carb_uncheck(CALLBACK_PASSARGS);
	}	/* if */

	return ggiGA_default_tank_uncheck(CALLBACK_PASSARGS);

}	/* ggiGA_default_uncheck */


/* The set function cements the allocation of resources, and
 * may alter global hardware state (that is, state which affects
 * the basic hardware layout scheme of resources but is not specific
 * to a certain resource.)
 */
int ggiGA_default_motor_set (CALLBACK_ARGS)
{
#ifdef DEBUG
	fprintf(stderr, "fixme: %s:%s:%d: not yet implemented\n",
		DEBUG_INFO);
#endif
	return 0;
}	/* ggiGA_default_motor_set */


int ggiGA_default_carb_set(CALLBACK_ARGS)
{
#ifdef DEBUG
	fprintf(stderr, "fixme: %s:%s:%d: not yet implemented\n",
		DEBUG_INFO);
#endif
	return 0;
}	/* ggiGA_default_carb_set */


int ggiGA_default_tank_set (CALLBACK_ARGS)
{
	struct ggiGA_resource_props *res_p = NULL;
	struct ggiGA_tank_props *res_sp = NULL;

	/* See if the resource has already been set. */
	if (res->res_state & GA_STATE_NORESET) return 0;

	LIB_ASSERT(!ggiGAIsFailed(res), "resource may not failed before");
	if (ggiGAIsFailed(res)) return 4;
	res_p = res->props;
	if (res_p == NULL) return 4;
	res_sp = &(res_p->sub.tank);

	/* Here we would use the storage managers to reserve storage in the
	 * in-service storage state.  Until then, we will handle RAM resources
	 * by simply allocating them.
	 */
	if (res_p->storage_ok != GA_STORAGE_SWAP) return 4;
	res_sp->read = res_sp->write =
		calloc((size_t)(res_p->size.area.x * res_p->size.area.y),
			(size_t)(GT_ByPP(res_sp->gt)));

        if (res_sp->read == NULL) return 2;
	ggiGAFlagModified(res);

	return 0;
}	/* ggiGA_default_tank_set */


int ggiGA_default_set(CALLBACK_ARGS)
{
	if (ggiGAIsTagHead(res)) {
		fprintf(stderr, "fixme: %s:%s:%d: Tag group heads not yet implemented\n",
			DEBUG_INFO);
		return 2;
	}	/* if */
	
	if (ggiGAIsMotor(res)) {
		return ggiGA_default_motor_set(CALLBACK_PASSARGS);
	}	/* if */
	if (ggiGAIsCarb(res)) {
		return ggiGA_default_carb_set(CALLBACK_PASSARGS);
	}	/* if */

	return ggiGA_default_tank_set(CALLBACK_PASSARGS);
}	/* ggiGA_default_set */


/* The unset callback removes any global hardware state.
 */
int ggiGA_default_unset(CALLBACK_ARGS)
{
#ifdef DEBUG
	fprintf(stderr, "fixme: %s:%s:%d: Default unset not implemented\n",
		DEBUG_INFO);
#endif
	return 0;
}	/* ggiGA_default_unset */


/* The release callback deallocates storage.
 */
int ggiGA_default_release(CALLBACK_ARGS)
{
#ifdef DEBUG
	fprintf(stderr, "fixme: %s:%s:%d: Default release not implemented\n",
		DEBUG_INFO);
#endif
	return 0;
}	/* ggiGA_default_release */
