/* $Id: taggroup.c,v 1.16 2005/11/30 16:38:33 cegger Exp $
******************************************************************************

   LibGalloc: utility functions for nested taggroups manipulation

   Copyright (C) 2003 Christoph Egger   [Christoph_Egger@t-online.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/internal/galloc.h>
#include <ggi/internal/galloc_debug.h>
#include <ggi/internal/galloc_int.h>


/* Tag Group property functions
 */


/* Returns zero, if no taghead */
int ggiGAIsTagHead(ggiGA_resource_taghandle handle)
{
	int rc;
	LIB_ASSERT(handle != NULL, "handle == NULL");

	rc = ((ggiGA_FULLTYPE(handle)
		== GA_RT_RESLIST_TAGHEAD) ? 1 : 0);

	/* Attention: Don't use ggiGAGetTagTail() here, because
	 *	ggiGAGetTagTail() calls ggiGAIsTagHead() within an LIB_ASSERT!!
	 */
	LIB_ASSERT( (!rc) || (rc && (((ggiGA_taghead_t)(handle))->tag2 > 0)),
		"malicious taghead: It must have a tag it is the head of");

	return rc;
}	/* ggiGAIsTagHead */


int ggiGAInTagGroup(ggiGA_resource_taghandle handle, tag_t tag)
{
	tag_t tmp;

	LIB_ASSERT(handle != NULL, "handle == NULL");

	tmp = ggiGAGetTag(handle);

	return ((tmp == tag) ? 1 : 0);
}	/* ggiGAInTagGroup */



/* Tag Group manipulation functions
 */

int ggiGATagGroupInit(ggiGA_resource_taghandle handle)
{
	ggiGA_taghead_t th;
	ggiGA_resource_taghandle cap;

	LIB_ASSERT(handle != NULL, "handle == NULL");
	LIB_ASSERT(!ggiGAIsCap(handle), "handle may not be a cap");

	/* initialize tag group by making
	 * a 1 member ring list.
	 */
	handle->left = handle->right = handle;

	/* Also initialize second link in Tag Group Heads */
	if (ggiGAIsTagHead(handle)) {
		th = ggiGATagGetHead(handle);
		th->left2 = th->right2 = handle;
	}	/* if */

	/* Caps get pointed to their owners */
	cap = RESLIST_NEXT(handle);
	if (ggiGAIsCap(cap)) {
		cap->left = cap->right = handle;
	}	/* if */

	return GALLOC_OK;
}	/* ggiGATagGroupInit */


int ggiGATagGroupAdd(ggiGA_resource_taggroup list,
			ggiGA_resource_handle resource)
{
	ggiGA_resource_handle cap = NULL;

	LIB_ASSERT(list != NULL, "list == NULL");
	LIB_ASSERT(resource != NULL, "resource == NULL");

	LIB_ASSERT(ggiGA_TAG(list) > 0, "Tag Id must be positive");
	LIB_ASSERT(ggiGA_TAG(resource) > 0, "Tag Id must be positive");

	/* note, tagheads may also be added into
	 * a ring list.
	 */
	if (ggiGAIsCap(resource)) return GALLOC_OK;

	if (!ggiGAInTagGroup(list, ggiGA_TAG(resource))) {
		return GALLOC_EFAILED;
	}	/* if */

	resource->left = list;
	resource->right = list->right;
	list->right->left = resource;
	list->right = resource;

	/* Caps get pointed to their owners. */
	cap = RESLIST_NEXT(resource);
	if (cap && ggiGAIsCap(cap)) {
		cap->left = cap->right = resource;
	}	/* if */

	return GALLOC_OK;
}	/* ggiGATagGroupAdd */


int ggiGATagGroupRemove(ggiGA_resource_taggroup list,
			ggiGA_resource_handle resource)
{
	LIB_ASSERT(list != NULL, "list == NULL");
	LIB_ASSERT(remove != NULL, "remove == NULL");

	LIB_ASSERT(ggiGA_TAG(list) > 0, "tag id must be positive");
	LIB_ASSERT(ggiGA_TAG(resource) > 0, "tag id must be positive");
	LIB_ASSERT(!ggiGAIsTagHead(list), "list must have a tag head");

	if (ggiGAIsCap(resource)) return GALLOC_OK;

	list->right = resource->right;
	list->right->left = resource->left;
	resource->left = resource->right = resource;

	return GALLOC_OK;
}	/* ggiGATagGroupRemove */


/* Use this to add Tagheads
 */
int ggiGATagGroupAddHead(ggiGA_resource_taggroup list,
			ggiGA_resource_taghandle taghead_handle)
{
	int variant_list;
	int variant_taghead;
	ggiGA_taghead_t th;

	variant_list = ggiGAGetResourceVariant(list);
	variant_taghead = ggiGAGetResourceVariant(taghead_handle);

	th = ggiGATagGetHead(taghead_handle);
	if (variant_list != variant_taghead) return GALLOC_EFAILED;

	if (!ggiGAInTagGroup(list, th->tag2)) return GALLOC_EFAILED;

	th->left2 = list;
	th->right2 = list->right;
	list->right->left = taghead_handle;
	list->right = taghead_handle;

	return GALLOC_OK;
}	/* ggiGATagGroupAddHead */


/* Use this to remove Tagheads
 */
int ggiGATagGroupRemoveHead(ggiGA_resource_taggroup list,
			ggiGA_resource_taghandle taghead_handle)
{
	ggiGA_taghead_t th;

	th = ggiGATagGetHead(taghead_handle);

	list->right = th->right2;
	list->right->left = th->left2;

	th->left2 = th->right2 = taghead_handle;

	return GALLOC_OK;
}	/* ggiGATagGroupRemoveHead */




/* Tag Group Navigation routines
 */

/* Finds the taghead of res, (but only if it occurs before cthis
 * in the list).  Returns NULL when nothing found.
 */
ggiGA_taghead_t ggiGAFindTagHeadInList(ggiGA_resource_list list,
				ggiGA_resource_handle cthis)
{
	ggiGA_resource_handle tmp2;
	ggiGA_taghead_t taghead;

	if (!ggiGA_TAG(cthis)) return NULL;

	taghead = NULL;

	tmp2 = RESLIST_FIRST(list);
	while (tmp2 != cthis) {
		if (ggiGAIsTagHead(tmp2)) {
			taghead = ggiGATagGetHead(tmp2);
			if (ggiGAInTagGroup(cthis, taghead->tag2)) {
				return taghead;
			}	/* if */
		}	/* if */

		tmp2 = RESLIST_NEXT(tmp2);
	}	/* while */

	return NULL;
}	/* ggiGAFindTagHeadInList */


ggiGA_taghead_t ggiGAFindTagHeadInGroup(ggiGA_resource_taggroup list)
{
	ggiGA_resource_taggroup idx;

	LIB_ASSERT(list != NULL, "list == NULL");

	idx = ggiGATagGetRight(list);
	while (idx != list) {
		if (ggiGAIsTagHead(idx)) {
			return ggiGATagGetHead(idx);
		}

		idx = ggiGATagGetRight(idx);
	}	/* while */

	return NULL;
}	/* ggiGAFindTagHeadInGroup */


int ggiGAHasNestedTagGroup(ggiGA_resource_taggroup list)
{
	ggiGA_taghead_t taghead;

	taghead = ggiGAFindTagHeadInGroup(list);
	if (taghead == NULL) {
		/* This taggroup has no taghead at all */
		return -1;
	}	/* if */

	if (ggiGAInTagGroup(list, taghead->tag2)) {
		/* the taghead is in our own taggroup
		 * => we have a nested taggroup
		 */
		return 1;
	}	/* if */

	/* The taghead does NOT belong to our taggroup
	 * => We are the nested taggroup!! :)
	 */
	return 0;
}	/* ggiGAHasNestedTagGroup */
