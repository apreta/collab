/* $Id: stubs.c,v 1.11 2007/12/20 23:43:06 cegger Exp $
******************************************************************************

   LibGalloc: extension stubs

   Copyright (C) 2001 Christoph Egger   [Christoph_Egger@t-online.de]
   Copyright (C) 2001 Brian S. Julin    [bri@calyx.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/internal/galloc.h>
#include <ggi/internal/galloc_int.h>
#include <ggi/internal/galloc_debug.h>
#include <ggi/ga_prop.h>
#include <stdio.h>
#include <string.h>


int ggiGACheck(ggi_visual_t vis, ggiGA_resource_list request,
               ggiGA_resource_list *result)
{
	struct galloc_context *ctxt = GALLOC_CONTEXT(vis);
	return ctxt->ops->check(ctxt, request, result);
}	/* ggiGACheck */


int ggiGASet(ggi_visual_t vis, ggiGA_resource_list request,
             ggiGA_resource_list *result)
{
	struct galloc_context *ctxt = GALLOC_CONTEXT(vis);
	return ctxt->ops->set(ctxt, request, result);
}	/* ggiGASet */


int ggiGARelease(ggi_visual_t vis, ggiGA_resource_list *list,
                 ggiGA_resource_handle *handle)
{
	struct galloc_context *ctxt = GALLOC_CONTEXT(vis);
	return ctxt->ops->release(ctxt, list, handle);
}	/* ggiGARelease */

int ggiGAanprintf(ggi_visual_t vis, ggiGA_resource_list request, 
		  size_t size, const char *format, char **out)
{
	struct galloc_context *ctxt = GALLOC_CONTEXT(vis);
	return ctxt->ops->anprintf(ctxt,request,size,format,out);
}	/* ggiGAanprintf */



/* Internal use only */
int ggiGA_Mode(ggi_visual_t vis, ggiGA_resource_handle *out)
{
	struct galloc_context *ctxt = GALLOC_CONTEXT(vis);
	return ctxt->ops->mode(ctxt, out);
}	/* ggiGA_Mode */


#if 0	/* Disabled */
int ggiGACheckIfShareable(ggi_visual_t vis, 
		    ggiGA_resource_handle reshandle,
		    ggiGA_resource_handle tocompare)
{
	struct galloc_context *ctxt = GALLOC_CONTEXT(vis);
	return ctxt->ops->checkifshareable(ctxt, reshandle, tocompare);
}	/* ggiGACheckIfShareable */
#endif
