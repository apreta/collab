/* $Id: share.c,v 1.3 2004/11/27 19:10:18 soyt Exp $ */

/* Not sure what to do with this code yet.   Moved here for preservation. */

#if 0

int ggiGACompare(ggiGA_resource_handle handle1, ggiGA_resource_handle handle2)
{
  	void *priv_h1, *priv_h2;
  	struct ggiGA_resource_props *props_h1, *props_h2;
	int rc;

	if (handle1 == NULL) return GGI_EARGINVAL;
	if (handle2 == NULL) return GGI_EARGINVAL;

	priv_h1 = handle1->priv;	
	priv_h2 = handle2->priv;
	props_h1 = handle1->props;
	props_h2 = handle2->props;

	if ((props_h1 == NULL) && (props_h2 != NULL)) goto fail;
	if ((props_h2 == NULL) && (props_h1 != NULL)) goto fail;
	if (!((props_h1 == NULL) && (props_h2 == NULL)) && 
	    memcmp(props_h1, props_h2, sizeof(struct ggiGA_resource_props)))
		goto fail;

	if (handle1->priv_size != handle2->priv_size) goto fail;
	if ((priv_h1 == NULL) && (priv_h2 != NULL)) goto fail;
	if ((priv_h2 == NULL) && (priv_h1 != NULL)) goto fail;
	if (!((priv_h1 == NULL) && (priv_h2 == NULL)) && 
	    memcmp(priv_h1, priv_h2, handle1->priv_size))
		goto fail;

	rc = GALLOC_OK;
	handle1->priv = handle2->priv = handle1->props = handle2->props = NULL;
	if (memcmp(handle1, handle2, sizeof(struct ggiGA_resource)) != 0)
		rc = GALLOC_EFAILED;	

	handle1->priv = priv_h1;
	handle2->priv = priv_h2;
	handle1->props = props_h1;
	handle2->props = props_h2;

	if (rc != GALLOC_OK) goto fail;

	return GALLOC_OK;

fail:
	return GALLOC_EFAILED;
}	/* ggiGACompare */



/* ACTUALLY INTERNAL STUFF - not even useable by extensions
 *
 */


/* Search for a matching resource that can share with the given one.
 * ret_handle is NULL on failure.
 * If return value is > 0, then we have to allocate
 * ret-val * colordepth bytes to be able to share with.
 * Note, that the allocation process may fail (i.e. when there is
 * not enough space).
 */
int ggiGASearchShareable(ggi_visual_t vis, ggiGA_resource_list reslist,
			 ggiGA_resource_handle handle,
			 ggiGA_resource_handle *ret_handle)
{
	int rc;
	ggiGA_resource_handle res_current = NULL;


	int storage_unmask = 0;
	int state_unmask = 0;

	LIB_ASSERT(reslist != NULL);
	LIB_ASSERT(handle != NULL);
	LIB_ASSERT(ret_handle != NULL);

	if (reslist == NULL) return GGI_EARGINVAL;
	if (handle == NULL) return GGI_EARGINVAL;
	if (ret_handle == NULL) return GGI_EARGINVAL;

	DPRINT_SHARE("%s:%s:%i: entered\n", DEBUG_INFO);

	LIB_ASSERT(!ggiGAIsShared(handle));

	if (!ggiGAIsShareable(handle)
	   || ggiGAIsCap(handle) || ggiGAIsCarb(handle))
	{
		DPRINT_SHARE("%s:%s:%i: handle is notshare, a cap or a carb\n", DEBUG_INFO);
		goto exit;
	}	/* if */

	if (handle->storage_share & GA_SHARE_FORCEDONTSHARE) {
		DPRINT_SHARE("%s:%s:%i: handle has GA_SHARE_FORCEDONTSHARE\n", DEBUG_INFO);
		goto exit;
	}	/* if */


	/* Note: You can't use ggiGAHandle() here */

	/* Note: the "handle" -resource is in reqlist, but not in reslist
	 *	 It will be added to reslist by ggiGASet(), _after_ we
	 *	 finished here.
	 */

	/* Init... */
	res_current = reslist;

	state_unmask |= GA_STATE_NORESET;
	state_unmask |= GA_STATE_NOCHANGE;
	state_unmask |= GA_STATE_MODIFIED;
	state_unmask |= GA_STATE_SEEABOVE;

	storage_unmask |= GA_STORAGE_READONLY;
	storage_unmask |= GA_STORAGE_TRANSFER;
	storage_unmask |= GA_STORAGE_DIRECT;


	/* Go through the _reslist_
	 */
	while (res_current != NULL) {
		int storage1, storage2;
		int state1, state2;

		LIB_ASSERT(res_current != NULL);

		/* Attention:
		 *	Don't use ggiGACompare() to compare!
		 *	Some storage-bits may differ.
		 */

		if (!ggiGAIsShareable(res_current)
		   || ggiGAIsFailed(res_current))
		{
			goto cont;
		}	/* if */

		if (res_current == handle) {
			goto cont;
		}	/* if */


		DPRINT_SHARE("%s:%s:%i: compare res_state: res_current: 0x%X, handle: 0x%X\n",
				DEBUG_INFO, res_current->res_state,
				handle->res_state);

		/* compare state */
		state1 = handle->res_state;
		state2 = res_current->res_state;

		state1 &= ~state_unmask;
		state2 &= ~state_unmask;

		if (state1 != state2) {
			goto cont;
		}	/* if */


		DPRINT_SHARE("%s:%s:%i: compare storage_ok: res_current: 0x%X, handle: 0x%X\n",
				DEBUG_INFO, res_current->storage_ok,
				handle->storage_ok);

		/* compare storage */
		storage1 = handle->storage_ok;
		storage2 = res_current->storage_ok;

		/* unmask all storage-types, which doesn't matter
		 */
		storage1 &= ~storage_unmask;
		storage2 &= ~storage_unmask;

		if (storage1 != storage2) {
			goto cont;
		}	/* if */

		DPRINT_SHARE("%s:%s:%i: call ggiGACheckIfShareable\n", DEBUG_INFO);

		/* Compare target and resource specific stuff */
		rc = ggiGACheckIfShareable(vis, handle, res_current);
		DPRINT_SHARE("%s:%s:%i: return code of ggiGACheckIfShareable: %i\n", DEBUG_INFO, rc);
		if (rc < GALLOC_OK) goto cont;

		/* Success! */
		DPRINT_SHARE("%s:%s:%i: exit successfully\n", DEBUG_INFO);
		ret_handle[0] = res_current;
		return rc;

cont:
		res_current = res_current->next;
	}	/* while */

exit:
	/* No proper resource */
	DPRINT_SHARE("%s:%s:%i: exit - no proper shareable found\n", DEBUG_INFO);
	ret_handle[0] = NULL;
	return GALLOC_EFAILED;
}	/* ggiGASearchShareable */



/* Let the "toshare" -resource share with the "shareable" one.
 * Returns GALLOC_OK, when successful.
 */
int ggiGADoShare(ggiGA_resource_handle shareable,
		 ggiGA_resource_handle toshare)
{
	int rc = GALLOC_EFAILED;

	if (shareable == NULL) return GGI_EARGINVAL;
	if (toshare == NULL) return GGI_EARGINVAL;

	DPRINT_SHARE("%s:%s:%i: entered\n", DEBUG_INFO);

	LIB_ASSERT(ggiGAIsShareable(shareable));
	LIB_ASSERT(ggiGAIsShareable(toshare));
	LIB_ASSERT(!(toshare->storage_share & GA_SHARE_FORCEDONTSHARE));
	LIB_ASSERT(!ggiGAIsFailed(toshare));


	DPRINT_SHARE("%s:%s:%i: before sharing: toshare (%p): props (%p), priv (%p), priv_size (%i), vis: (%p)\n",
			DEBUG_INFO, toshare, toshare->props, toshare->priv,
			toshare->priv_size, toshare->vis);
	DPRINT_SHARE("%s:%s:%i: shareable (%p): props (%p), priv (%p), priv_size (%i), vis: (%p)\n",
			DEBUG_INFO, shareable, shareable->props, shareable->priv,
			shareable->priv_size, shareable->vis);

	/* Free old data */
	free(toshare->props);
	free(toshare->priv);
	toshare->priv = toshare->props = NULL;
	toshare->priv_size = 0;

	/* We have to take care of some flags and other
	 * property stuff like the storage_ok -flag, we
	 * don't have to share blindy!
	 */
	toshare->props = shareable->props;
	toshare->priv = shareable->priv;
	toshare->priv_size = shareable->priv_size;
	toshare->vis = shareable->vis;

	/* We do this in case of GA_RT_*_DONTCARE */
	toshare->storage_ok |= (shareable->storage_ok & GA_STORAGE_STORAGE_MASK);

	DPRINT_SHARE("%s:%s:%i: after sharing: toshare (%p): props (%p), priv (%p), priv_size (%i), vis: (%p)\n",
			DEBUG_INFO, toshare, toshare->props, toshare->priv,
			toshare->priv_size, toshare->vis);
	DPRINT_SHARE("%s:%s:%i: shareable (%p): props (%p), priv (%p), priv_size (%i), vis: (%p)\n",
			DEBUG_INFO, shareable, shareable->props, shareable->priv,
			shareable->priv_size, shareable->vis);


	/* Never force a copy-on-write! Maybe the user wanna change
	 * all shared sprites at once, for example!
	 *
	 * The user has to set GA_SHARE_COPYONWRITE.
	 * Maybe it is set as default by ggiGAClearProperties()
	 */
	toshare->storage_share |= GA_SHARE_SHARED;
	toshare->res_count++;

	shareable->storage_share |= GA_SHARE_SHARED;
	shareable->res_count++;

	LIB_ASSERT(toshare->res_count >= 1);
	LIB_ASSERT(shareable->res_count > 1);

	DPRINT_SHARE("%s:%s:%i: res_count: toshare (%i), shareable (%i)\n",
			DEBUG_INFO, toshare->res_count, shareable->res_count);


	rc = GALLOC_OK;
	DPRINT_SHARE("%s:%s:%i: exit successfully\n", DEBUG_INFO);
	return rc;
}	/* ggiGADoShare */


/* Unshares the resource, so that it can be released as usual.
 */
int ggiGAUndoShare(ggiGA_resource_list list,
		   ggiGA_resource_handle handle)
{
	ggiGA_resource_handle current = NULL;
	int rc = GALLOC_EFAILED;

	if (list == NULL) return GGI_EARGINVAL;
	if (handle == NULL) return GGI_EARGINVAL;

	/* not shared? */
	if (!ggiGAIsShared(handle)) return GALLOC_OK;
	LIB_ASSERT(ggiGAIsShared(handle));

	DPRINT_SHARE("%s:%s:%i: entered\n", DEBUG_INFO);

	current = list;
	while (current != NULL) {
		struct ggiGA_resource_props *props = NULL;

		if (!ggiGAIsShared(current)) {
			goto cont;
		}	/* if */

		/* shared resource found
		 * now check, if it is shared with "handle"
		 */
		if ((current->priv != handle->priv)
		   || (current->props != handle->props))
		{
			goto cont;
		}	/* if */


		/* unshare properties */
		props = calloc(1, sizeof(struct ggiGA_resource_props));
		if (!props) return GGI_ENOMEM;

		DPRINT_SHARE("%s:%s:%i: unshare here\n", DEBUG_INFO);
		memcpy(props, current->props,
			sizeof(struct ggiGA_resource_props));
		handle->props = props;

		/* This is necessary for the case of "copy-on-write"
		 */
		handle->storage_share &= ~GA_SHARE_SHARED;
		handle->props->storage_ok = handle->storage_ok;
		handle->props->storage_need = handle->storage_ok;
		handle->props->storage_share = handle->storage_share;

		/* When this cofuses ggiGARelease(), then we
		 * have to fix ggiGARelease()
		 */
		handle->priv = NULL;
		handle->priv_size = 0;
		handle->res_count = 1;

		DPRINT_SHARE("%s:%s:%i: handle: storage_share: 0x%X,
				DEBUG_INFO, props (%p), priv: (%s),
				priv_size: (%i), res_count: (%i)\n",
				handle->storage_share, handle->props,
				handle->priv, handle->priv_size,
				handle->res_count);
		goto exit;

cont:
		current = current->next;
	}	/* while */

	/* No such resource! Should NEVER happen! */
	LIB_ASSERT(current != NULL);

	rc = GALLOC_EFAILED;

exit:
	DPRINT_SHARE("%s:%s:%i: exit with return code: %i\n",
			DEBUG_INFO, rc);
	return rc;
}	/* ggiGAUndoShare */


/* Compares the two lists. Note, that list1 may have more list-members
 * than list2. Returns GALLOC_OK, if successfull.
 */
int ggiGACompareList(ggiGA_resource_list list1, ggiGA_resource_list list2)
{
	int rc = GALLOC_EFAILED;
	ggiGA_resource_handle current_list1 = NULL;
	ggiGA_resource_handle current_list2 = NULL;


	if (list1 == NULL) return GGI_EARGINVAL;

	if (list2 == NULL) {
		/* we have to assume, that the list2 has no elements yet
		 */
		return GALLOC_OK;
	}	/* if */

	current_list1 = list1;
	current_list2 = list2;
	while (current_list1 != NULL) {
		rc = ggiGACompare(current_list1, current_list2);
		LIB_ASSERT(rc == GALLOC_OK);
		if (rc != GALLOC_OK) return rc;

		current_list1 = current_list1->next;
		current_list2 = current_list2->next;

		if (current_list2 == NULL) break;
	}	/* while */


	return GALLOC_OK;
}	/* ggiGACompareList */

#endif
