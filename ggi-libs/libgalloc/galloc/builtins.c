/* $Id: builtins.c,v 1.2 2007/03/10 11:56:18 soyt Exp $
******************************************************************************

   LibGAlloc builtin target bindings

   Copyright (C) 2005 Christoph Egger

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/


#include "config.h"
#include <ggi/gg.h>	/* We use LibGG to manage config files */
#include <ggi/internal/galloc_debug.h>
#include <string.h>


typedef int (ggigallocfunc_dlinit)(int, void **);

#ifdef BUILTIN_DISPLAY
ggigallocfunc_dlinit GALLOCdl_stubs_galloc;
#endif
#ifdef BUILTIN_DISPLAY_FBDEV
ggigallocfunc_dlinit GALLOCdl_fbdev_galloc;
#endif
#ifdef BUILTIN_DISPLAY_KGI
ggigallocfunc_dlinit GALLOCdl_kgi_Radeon_galloc;
#endif
#ifdef BUILTIN_DISPLAY_LIBKGI
ggigallocfunc_dlinit GALLOCdl_libkgi_galloc;
#endif
#ifdef BUILTIN_DISPLAY_TERMINFO
ggigallocfunc_dlinit GALLOCdl_terminfo_galloc;
#endif
#ifdef BUILTIN_DISPLAY_X
ggigallocfunc_dlinit GALLOCdl_X_galloc;
#endif


struct target {
	const char *target;
	ggigallocfunc_dlinit *func;
};

static struct target _targets[] = {

#ifdef BUILTIN_DISPLAY
	{ "GALLOCdl_stubs_galloc", &GALLOCdl_stubs_galloc },
#endif
#ifdef BUILTIN_DISPLAY_FBDEV
	{ "GALLOCdl_fbdev_galloc", &GALLOCdl_fbdev_galloc },
#endif
#ifdef BUILTIN_DISPLAY_KGI
	{ "GALLOCdl_kgi_Radeon_galloc", &GALLOCdl_kgi_Radeon_galloc },
#endif
#ifdef BUILTIN_DISPLAY_LIBKGI
	{ "GALLOCdl_libkgi_galloc", &GALLOCdl_libkgi_galloc },
#endif
#ifdef BUILTIN_DISPLAY_TERMINFO
	{ "GALLOCdl_terminfo_galloc", &GALLOCdl_terminfo_galloc },
#endif
#ifdef BUILTIN_DISPLAY_X
	{ "GALLOCdl_X_galloc", &GALLOCdl_X_galloc },
#endif
	{ NULL, NULL }

};


static void *_builtins_get(void * _, const char *symbol)
{
	struct target *t;

	for (t = _targets; t->target != NULL; t++) {
		if (!strcmp(t->target, symbol)) {
			DPRINT_LIBS("builtin symbol found: '%s'\n", symbol);
			return t->func;
		}
	}
	DPRINT_LIBS("builtin symbol '%s' not found\n", symbol);
	return NULL;
}

static struct gg_scope *_builtins;

void _ggigallocInitBuiltins(void);
void _ggigallocExitBuiltins(void);

void _ggigallocInitBuiltins(void)
{
	_builtins = ggNewScope("@libggigalloc", NULL, &_builtins_get, NULL);
}

void _ggigallocExitBuiltins(void)
{
	ggDelScope(_builtins);
}

