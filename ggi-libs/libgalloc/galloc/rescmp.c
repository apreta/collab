/* $Id: rescmp.c,v 1.11 2005/10/12 10:23:43 cegger Exp $
******************************************************************************

   LibGAlloc: Functions to compare reqlist resources with haslist resources.

   Copyright (C) 2002 Brian S. Julin    [bri@tull.umassp.edu]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies of substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/internal/galloc.h>
#include <ggi/internal/galloc_debug.h>
#include <ggi/ga_prop.h>
#include <stdio.h>
#include <string.h>


#if 0	/* Defined, but unused */
/* Helper math function: Euclidian Reduction by GCD. */
static void _ga_factor_euclid(unsigned long *n, unsigned long *p)
{
	unsigned long v1, v2;
	if (*n > *p) {
		v1 = *n; v2 = *p;
	} else {
		v2 = *n; v1 = *p;
	}	/* if */

	while (v2) {
		unsigned long tmp;
		tmp = v1 % v2;
		v1 = v2;
		v2 = tmp;
	}	/* while */

	*n /= v1;
	*p /= v1;

	return;
}	/* _ga_factor_euclid */


/* Find a ratio of one resource's coordbase to another.
 * This does not include subunits, as that's motor specific.
 * Different ratios exist for horizontal vs. vertical units.
 * (Essentially these functions convert the coordbases to 
 * micrometers and then reduce the results by their GCD.)
 */
static void _ga_find_cb_hfactors(struct ggiGA_resource_props *res,
			  struct ggiGA_resource_props *has,
			  ggi_mode *mode,
			  unsigned long *rmul, 
			  unsigned long *hmul)
{
	enum ggi_ll_coordbase rescb, hascb;

	rescb = 
	  (res->flags & GA_FLAG_COORDBASE_MASK) >> GA_FLAGSHIFT_COORDBASE;
	hascb = 
	  (has->flags & GA_FLAG_COORDBASE_MASK) >> GA_FLAGSHIFT_COORDBASE;

	/* Not really necessary, just for clarity */
	rescb &= LL_COORDBASE_MASK; 
	hascb &= LL_COORDBASE_MASK;

	*rmul = *hmul = 1;
	if (rescb == LL_COORDBASE_DOTS) *rmul = mode->dpp.x;
	if (hascb == LL_COORDBASE_DOTS) *hmul = mode->dpp.x;
	_ga_factor_euclid(rmul, hmul);

	if (rescb == LL_COORDBASE_DOTS || rescb == LL_COORDBASE_PIXELS) {
		*rmul *= mode->size.x * 1000;
		*hmul *= mode->visible.x;
		_ga_factor_euclid(rmul, hmul);
	}	/* if */
	if (hascb == LL_COORDBASE_DOTS || hascb == LL_COORDBASE_PIXELS) {
		*hmul *= mode->size.x * 1000;
		*rmul *= mode->visible.x;
		_ga_factor_euclid(rmul, hmul);
	}	/* if */

	if (rescb == LL_COORDBASE_MILLIMETER) *rmul *= 1000;
	if (hascb == LL_COORDBASE_MILLIMETER) *hmul *= 1000;
	_ga_factor_euclid(rmul, hmul);

	if (rescb == LL_COORDBASE_1_64_INCH) *rmul = 39688;
	if (hascb == LL_COORDBASE_1_64_INCH) *hmul = 39688;
	_ga_factor_euclid(rmul, hmul);
}	/* _ga_find_cb_hfactors */
#endif


#if 0	/* Defined, but unused */
static void _ga_find_cb_vfactors(struct ggiGA_resource_props *res,
			  struct ggiGA_resource_props *has,
			  ggi_mode *mode,
			  unsigned long *rmul, 
			  unsigned long *hmul)
{
	enum ggi_ll_coordbase rescb, hascb;

	rescb = 
	  (res->flags & GA_FLAG_COORDBASE_MASK) >> GA_FLAGSHIFT_COORDBASE;
	hascb = 
	  (has->flags & GA_FLAG_COORDBASE_MASK) >> GA_FLAGSHIFT_COORDBASE;

	/* Not really necessary, just for clarity */
	rescb &= LL_COORDBASE_MASK; 
	hascb &= LL_COORDBASE_MASK;

	*rmul = *hmul = 1;
	if (rescb == LL_COORDBASE_DOTS) *rmul = mode->dpp.y;
	if (hascb == LL_COORDBASE_DOTS) *hmul = mode->dpp.y;
	_ga_factor_euclid(rmul, hmul);

	if (rescb == LL_COORDBASE_DOTS || rescb == LL_COORDBASE_PIXELS) {
		*rmul *= mode->size.y * 1000;
		*hmul *= mode->visible.y;
		_ga_factor_euclid(rmul, hmul);
	}	/* if */
	if (hascb == LL_COORDBASE_DOTS || hascb == LL_COORDBASE_PIXELS) {
		*hmul *= mode->size.y * 1000;
		*rmul *= mode->visible.y;
		_ga_factor_euclid(rmul, hmul);
	}	/* if */

	if (rescb == LL_COORDBASE_MILLIMETER) *rmul *= 1000;
	if (hascb == LL_COORDBASE_MILLIMETER) *hmul *= 1000;
	_ga_factor_euclid(rmul, hmul);

	if (rescb == LL_COORDBASE_1_64_INCH) *rmul = 39688;
	if (hascb == LL_COORDBASE_1_64_INCH) *hmul = 39688;
	_ga_factor_euclid(rmul, hmul);
}	/* _ga_find_cb_vfactors */
#endif

/* All functions below must define "res", "rescap", "has", and
 * "hascap" as pointers to some type of structure, and "rc" as an integer.
 *
 * Note in any of the following functions/macros, if "res" 
 * has no cap, then "rescap" == "res", and if "has" has no 
 * cap, then "hascap" == "has".  Care must be taken of this.
 *
 * _GA_CHECKMEMBER compares a member in a pair of resources (possibly 
 * with caps), handling all GGI_AUTO cases and compensating for 
 * factors which scale the resource/cap values relative to each other.
 * (Set rmul,hmul to 1,1 for a normal compare.)
 *
 * TODO: this macro really isn't written to do the right thing
 * when rounding error conditions occur.
 */
#define _GA_CHECKMEMBER(mem, jump, rmul, hmul) \
do {									\
	LIB_ASSERT(hascap->mem >= has->mem, "not enough memory");	\
	LIB_ASSERT(rescap->mem >= res->mem, "not enough memory");	\
									\
	if (res->mem == GGI_AUTO) {					\
		res->mem = rescap->mem;					\
                /* If hascap->mem == GGI_AUTO so does has->mem */	\
		if (hascap->mem == GGI_AUTO) goto jump;			\
		if (res->mem * (rmul) > hascap->mem * (hmul))		\
			res->mem = hascap->mem * (hmul) / (rmul);	\
	        if (has->mem * (hmul) > res->mem * (rmul))		\
			res->mem = has->mem * (hmul) / (rmul);		\
		break;							\
	}								\
	if ((hascap->mem != GGI_AUTO) &&				\
	    (res->mem * (rmul) > hascap->mem * (hmul))) rc |= 2;	\
	if ((has->mem * (hmul) > rescap->mem * (rmul))) rc |= 2;	\
	res->mem = (hascap->mem * (hmul) > rescap->mem * (rmul)) ?	\
		rescap->mem : (hascap->mem * (hmul) / (rmul));		\
	if (has->mem * (hmul) > res->mem * (rmul))			\
		res->mem = has->mem * (hmul)/(rmul);			\
	if (rc) goto jump;						\
} while (0);


int _ga_cmp_qty(struct ggiGA_resource_props *res,
		struct ggiGA_resource_props *rescap,
		struct ggiGA_resource_props *has,
		struct ggiGA_resource_props *hascap,
		int rc)
{
	_GA_CHECKMEMBER(qty, out, 1, 1);
 out:
	return rc;
}	/* _ga_cmp_qty */



int _ga_cmp_factors(struct ggiGA_resource_props *res,
		struct ggiGA_resource_props *rescap,
		struct ggiGA_resource_props *has,
		struct ggiGA_resource_props *hascap,
		int rc)
{
#ifdef DEBUG
	fprintf(stderr, "fixme: %s:%s:%i: not yet implemented!\n",
		DEBUG_INFO);
#endif

	return rc;
}	/* _ga_cmp_factors */





#if 0	/* Defined, but unused */
/* Motors only. */
static int ga_cmp_scaling(ggiGA_resource_handle *res,
		   ggiGA_resource_handle *rescap,
		   ggiGA_resource_handle *has,
		   ggiGA_resource_handle *hascap,
		   ggi_mode *mode,
		   int rc)
{
	unsigned long (rmul), (hmul);
	enum ggi_ll_coordbase rescb, hascb;

	APP_ASSERT(ggiGAIsMotor(res[0]), "res must of a motor resource");

	/* res and has are expected to be type
	 * of ggiGA_resource_props
	 */
	fprintf(stderr, "fixme: %s:%s:%d: res and has are used"
			"from incompatible pointer type",
			DEBUG_INFO);
	_ga_find_cb_hfactors(res, has, mode, &(rmul), &(hmul));

	/* FIXME: return value missing! */
}	/* ga_cmp_scaling */
#endif


int _ga_cmp_storage(struct ggiGA_resource_props *res,
		    struct ggiGA_resource_props *rescap,
		    struct ggiGA_resource_props *has,
		    struct ggiGA_resource_props *hascap,
		    int rc)
{
	ggiGA_storage_type storage_mask_res, storage_mask_has;
	ggiGA_storage_type storage_mask;

	LIB_ASSERT(has->storage_ok == hascap->storage_ok,
			"assertion \"has->storage_ok == hascap->storage_ok\" failed");
	LIB_ASSERT(has->storage_need == hascap->storage_need,
			"assertion \"has->storage_need == hascap->storage_need\" failed");

	if (res->storage_ok != rescap->storage_ok || 
	    res->storage_need != rescap->storage_need)
	{
		fprintf(stderr,  "LibGAlloc: Behavior when cap storage "
			"!= resource storage is undefined. \n");
	}	/* if */

	/* Compare storage type with haslist */
	storage_mask_has = ~(has->storage_ok ^ has->storage_need);
	if (res->storage_ok == GA_STORAGE_DONTCARE) {
		/* Nuance: In has, storage_ok:n = 0, storage_need:n = 1 never 
		 * happens (forbidden combination.)
		 */
		res->storage_ok    = has->storage_ok;
		res->storage_need &= ~storage_mask_has;
		res->storage_need |= storage_mask_has & has->storage_need;
		res->storage_ok   ^= res->storage_need & ~storage_mask_has;
	}	/* if */

	storage_mask_res = ~(res->storage_ok ^ res->storage_need);
	storage_mask = storage_mask_res & storage_mask_has;
	if ((storage_mask & has->storage_ok) !=
	    (storage_mask & res->storage_ok))
	{
		if (rc == 1) {
			res->storage_ok   = has->storage_ok;
			res->storage_need = has->storage_need;
		}	/* if */
		rc |= 2;
	} else {
		storage_mask = ~storage_mask_res & storage_mask_has;
		res->storage_ok   &= ~storage_mask;
		res->storage_need &= ~storage_mask;
		res->storage_ok   |= storage_mask & has->storage_ok;
		res->storage_need |= storage_mask & has->storage_ok;
	}	/* if-else */

	return rc;
}	/* _ga_cmp_storage */
