<book id="libmmutils">
<title/mmutils mini-library documentation/
<titleabbrev/mmutils minilib/

<bookinfo>
<abstract>
<para>
This document describes the mmutils mini-library, a codebase
developed originally for LibGAlloc/LibGGI which is potentially
too generally useful to deny the world by keeping it GGI-specific.
</para>
</abstract>
</bookinfo>

<preface id="libmmutils-preface">
<title/Preface/

<para> 
If you find any errors in this documentation, please notify the
documentation maintainer(s).  If you find anything in this documentation
that is misleading or could be improved, also notify the maintainers :)
</para>

<para>
This documentation is written in DocBook.  The SGML source can be used to
generate Unix manpages, HTML and text formats automatically.
</para>

<para>
For other developers: if you want to change this manual and do not know
DocBook/SGML, just mail me the changes in plain text and I 
(<email/bri@calyx.com/) will gladly format and include it for you.
</para>

</preface>


<chapter id="libmmutils-chapter-rm">
<title>Range Managers</title>
<para>
   A range manager tackles the job of fitting as many data items
   into a limited memory area as it can, and doing so in such a 
   way that when items are freed, fragmentation of the free areas
   is minimized.
</para>
<para>
   The simple case of a range manager is to manage "1D" ranges.
   These are ranges that have a start location and use a contiguous,
   known amount of memory.  The more advanced case is a "2D" range
   manager meant to manage areas of memory that have a width and a 
   height -- however, these areas may have different strides so 
   in effect, a 2D manager manages "dashed lines" of memory.
</para>
<para>
   This may seem simple, but it is not, especially when you add in the
   fact that several of the memory allocations that the range manager
   must manage require two or even three levels of memory alignment
   (it being the case that the range manager is being developed primarily
   to manage graphics card video RAM.)
</para>

<!--
<para>
   There is currently a braindead 1D range manager implementation 
   in the mmutils library, and a demo program that shows it in motion;
   however the actual internal structure and api is by no means definite,
   so it is no use at this point in time to document it further.  Run
   the demo, read the code comments, and you should get the general
   idea.
</para>
-->

<sect1><title>2D range manager</title>
<para>
   The mmutils provides a general purpose 2D range manager. Actually
   'general' might not be that true since it is a bit biased by its
   primary purpose.
</para>

<para>
   So, how does it work? Basically, you define a "range list" which
   holds the ranges managed by a given area. At first it is empty.
   To allocate ranges in this area, you first need to define a
   "request list", populated with the set of candidate ranges that must be
   allocated. Each range is defined by a set of attributes, mainly its
   dimensions, and constraints which restrict the way it can be allocated.
   Then you ask the range manager to find the best fit.
</para>

<sect2><title>Constructing the request list</title>

<para>
The request list is initialized by a call to
<function>rm_list_init(rm_range_list_t ** rl)</>. A range list is
defined as follow:
<programlisting>
typedef struct rm_range_list {
	rm_range_t * range;
	int count;            /* number of used ranges */
	int allocated;        /* number of slots   */
} rm_range_list_t;
</programlisting>
<structfield/range/ points to an array of ranges, and <structfield/count/
is the number of ranges currently managed by this list.
</para>

<para>
Then we need to add ranges to this list. This is done with
the <function/rm_list_add(rm_range_list_t * rl, rm_range_t * range)/
function, which will extend the array in the range list if necessary,
and add a new entry by copying the content of <parameter/range/.
Let's have look at how to define a range.

<programlisting>
typedef struct rm_range_attribute {

	/* Set by the caller. */
	
	size_t width;
	size_t height;
	
	void    * owner;
	
	rangeFlags flags;

	/* These are set by the range manager algorithm. */
	
	size_t start;
	size_t stride;

} rm_range_attribute_t;


typedef struct rm_range_constraint {
	size_t start_min;
	size_t start_max;
	size_t start_align;
	size_t stride_align;
} rm_range_constraint_t;


typedef struct rm_range {
	rm_range_attribute_t  attribute;
	rm_range_constraint_t constraint;
} rm_range_t;
</programlisting>
</para>
<para>
The attributes are described as follows:
<variablelist>

<varlistentry><term><structfield/width/,<structfield/height/</term>
<listitem>
<para>Self-explanatory. These must be set by the caller</para>
</listitem>
</varlistentry>

<varlistentry><term><structfield/owner/</term>
<listitem>
<para>This is a user data that can be used to later identify the
ressource this range refers to. The range manager don't ever touch it.
</para>
</listitem>
</varlistentry>

<varlistentry><term><structfield/flags/</term>
<listitem><para>
The range manager and user communicate through these flags.
Part of the bits are written to by the user and read by the range
manager, and the other way round.
<programlisting>
typedef enum {
	
	RANGE_REQUEST_MASK    = 0x0000ffff,
	RANGE_ANSWER_MASK     = 0xffff0000,

	/* user flags, may be OR'd */
	    
	   /* misc */

	RANGE_RELOCATABLE     = 0x00000001, /* set if the range can be
					   moved around */

	   /* constraint */

	RANGE_CONSTRAINT_MASK = 0x0000ff00,


	RANGE_START_MIN       = 0x00000100,
	RANGE_START_MAX       = 0x00000200,
	RANGE_START_ALIGN     = 0x00000400,

	RANGE_STRIDE_ALIGN    = 0x00001000,
	RANGE_STRIDE_FIXED    = 0x00002000,  /* the stride can't be changed */

	/* range manager response */

	RANGE_OK_MASK         = 0x000f0000,
	RANGE_ALLOCATED       = 0x00010000, /* For newly allocated ranges.           */
	RANGE_RELOCATED       = 0x00020000, /* Existing range has been moved around. */

	RANGE_KO_MASK         = 0x00f00000,
	RANGE_INVALID         = 0x00100000, /* e.g. start_max < start_min            */
	RANGE_NO_FIT          = 0x00200000, /* Couldn't find a space.                */

} rangeFlags;
</programlisting>
The user may set any of the bit covered by
<symbol/RANGE_REQUEST_MASK/. The other ones will be cleared anyway.
The constraint bits must be set for the manager to take them into
account. <symbol/RANGE_STRIDE_FIXED/ tells the range manager to use
the value set in <structfield/stride/ as the fixed stride for this range.
</para></listitem>
</varlistentry>

<varlistentry><term><structfield/start/,<structfield/stride/</term>
<listitem>
<para>Setting these values has no effect, unless the stride is
fixed. It is normally the range manager very purpose to do this.
</para>
</listitem>
</varlistentry>

</variablelist>
</para>

<para>
The following constraints may apply to the range:
<variablelist>

<varlistentry><term><structfield/start_min/</term>
<listitem>
<para>The minimum offset where this range may be allocated.</para>
</listitem>
</varlistentry>

<varlistentry><term><structfield/start_max/</term>
<listitem><para>The maximum offest where this range may be
allocated. Note that if <structfield/start_min/ equals <structfield/start_max/,
the range has a fixed starting offset.
</para></listitem>
</varlistentry>

<varlistentry><term><structfield/start_align/</term>
<listitem><para>
This is the alignement factor for the start offset of this range.
Setting it to 2 means that the range must be allocated on even address.
</para></listitem>
</varlistentry>

<varlistentry><term><structfield/stride_align/</term>
<listitem><para>
This is the alignement factor for the stride of this range.
</para></listitem>
</varlistentry>

</variablelist>
</para>

<example><title>A sample request</title>
<programlisting>
        rm_range_list_t * rm1;
	rm_range_t r;

	/* initialize the list */
	rm_list_init(&amp;rm1);

	r.attribute.width=100;
	r.attribute.height=50;
	r.constraint.stride_align=17;
	r.attribute.flags=RANGE_STRIDE_ALIGN;
	rm_list_add(rm1,&amp;r);
	
	r.attribute.width=70;
	r.attribute.height=20;
	r.attribute.flags=0;
	rm_list_add(rm1,&amp;r);
	
	r.attribute.width=200;
	r.attribute.height=20;
	r.attribute.flags=0;
	rm_list_add(rm1,&amp;r);
	
	r.attribute.width=40;
	r.attribute.height=30;
	r.constraint.start_min=0;
	r.constraint.start_max=0;
	r.constraint.stride_align=3;
	r.attribute.flags= RANGE_START_MIN | RANGE_START_MAX | RANGE_STRIDE_ALIGN ;
	rm_list_add(rm1,&amp;r);
	
</programlisting>
</example>
</sect2>

<sect2><title>Allocation</title>
<para>
Once the request is ready, <function/rm_list_fit(rm_range_list_t *
rl,rm_range_list_t * req,  size_t size)/ is called.
It will try to fit the ranges in the request list <parameter/req/>
into the area managed by the list <parameter/rl/. <parameter/size/
is the size of the area managed by list.
</para>

<para>
When the call returns, the ranges in <parameter>req</parameter>
have their attributes altered to reflect the range_manager's choice.
The user should loop through the resulting <parameter/req/ list
and check the <symbol/RM_RESULT_MASK/ flags to see which ranges
were succesfully placed. For those ranges, <structfield/start/ and
<structfield/stride/ are set.
</para>

<para>A set of macros are defined to easily access the attributes of
the ranges in a list. Some can only be read.
<programlisting>
#define RM_FLAGS(r,i)             (r->range[i].attribute.flags)
#define RM_SET_FLAGS(r,i,f)       (RM_FLAGS(r,i)|=f)
#define RM_UNSET_FLAGS(r,i,f)     (RM_FLAGS(r,i)&amp;=~f)
#define RM_CHECK_ALL_FLAGS(r,i,f) (RM_FLAGS(r,i)&amp;f==f)
#define RM_CHECK_FLAGS(r,i,f)     (RM_FLAGS(r,i)&amp;f)

#define RM_OWNER(r,i)        (r->range[i].attribute.owner)
#define RM_WIDTH(r,i)        (r->range[i].attribute.width)
#define RM_HEIGHT(r,i)       (r->range[i].attribute.height)

#define RM_START(r,i)        (r->range[i].attribute.start)
#define RM_STRIDE(r,i)       (r->range[i].attribute.stride)

#define RM_PADDING(r,i)      (RM_STRIDE(r,i)-RM_WIDTH(r,i))
#define RM_SURFACE(r,i)      (RM_STRIDE(r,i)*RM_HEIGHT(r,i)-RM_PADDING(r,i))
#define RM_END(r,i)          (RM_START(r,i) + RM_SURFACE(r,i) - 1)

#define RM_CSTR_START_MIN(r,i) (r->range[i].constraint.start_min)
#define RM_CSTR_START_MAX(r,i) (r->range[i].constraint.start_max)
#define RM_CSTR_START_GAP(r,i) (RM_CSTR_START_MAX(r,i)-RM_CSTR_START_MIN(r,i))
#define RM_CSTR_START_ALIGN(r,i) (r->range[i].constraint.start_align)

#define RM_CSTR_STRIDE_ALIGN(r,i) (r->range[i].constraint.stride_align)
</programlisting>
</para>

<para>Note that the main list <parameter/rl/ is not altered by this
call. It means that <function/rm_list_fit/ can be called multiple
times with different request lists.
When a request list is choosen for allocation,
<function/rm_list_alloc(rm_range_list_t * rl, rm_range_list_t * req)/
is called. The request list must not be changed between the call to
<function/rm_range_fit/ and this one, unless you really know what you
are doing. Otherwise, for the believers, the anger of God will fall
upon you; for the other ones, your memory might be badly messed up.
A harmless operation is to remove a range from a list with
<function/rm_list_remove(rm_range_list_t * rl, int idx)/. This is also
the only way to desallocate a range from a managed area.
</para>

<para>
It is a citizen's duty to collect one's garbage. So don't forget
to call <function/rm_list_exit(rm_range_list_t ** rl)/ on the request
lists you don't need.
</para>

</sect2>

<sect2><title>Notes</title>
<para>
The range manager is not yet able to shuffle allocated ranges.
This will be done by adding ranges to the request list, with owner
set to the index of the relocated chunks. This is in my opinion a
nice way to tell the user what will actually happen, without touching
anything.
</para>

<para>
Another missing feature is the <function/rm_list_defrag/, which
would relocated ranges at best to reduce fragmentation after the
removal of ranges.
</para>

<para>
Finally, customizable allocation policy could be introduced.
</para>

</sect2>

</sect1>

<sect1><title>Chunk list</title>
<para>
Internally, the range manager makes use of ordered chunk lists to keep
track of remaining memory chunks. It won't be described in details here,
but the header should be clear enough.
</para>
</sect1>

</chapter>
<chapter id="libmmutils-chapter-bo">
<title>The Versatile Batchop</title>
<para>
   A "batchop" is essentially a format descriptor for data which is
   organized in arrays. The mmutils library defines batchops and ways to
   operate on the data contained in these arrays.
</para>
<sect1 id="bo-overview">
<title>A bird's eye view of batchops</title>
<para>
   The batchop structure is a holder for an array of "batchparm"
   structures. It contains a "numparms" member which says how many
   batchparm structures are in the array, which is located through the
   pointer member "parms". If you consider the batchop to be like a
   spreadsheet full of values, a batchparm is like a column header.
</para>
<para>   
   The batchop structure also contains two indexes: one for writing input
   into the data areas which the batchop manages and one for reading
   output from the data areas. Thinking again in spreadsheet terms, they
   can be thought of as referring to a row in the spreadsheet. They are
   used to contain the state of a transfer between two batchops. Since
   this state is contained inside the batchop, no batchop can be involved
   in more than one output or input operation at the same time. However,
   it is possible to transfer data into a batchop's area at the same time
   as data is being transferred out of the batchop's data area -- this
   must be done with care that access to the data does not collide.
</para>
<para>  
   The high level functions can be explained without going into further
   detail. Let us assume that you have two batchops names bsrc and bdest,
   which have already been created for you (we will cover how batchops
   are created later.) Assume also that bsrc is managing some data areas
   that are already full of data, and bdest is managing areas that you
   want to transfer the data into.
</para>
<para>   
   The first step in transferring data is to call the macro
   bo_match(bsrc, bdest). This will study the two batchops and fill out
   some internal values in bdest that will speed up the data transfer.
   Because this state is kept in bdest, it is possible for you to do
   this:
</para>
<programlisting>
bo_match(bdest, bsrc);
bo_match(bdest2, bsrc);
</programlisting>
<para>
   ...now bdest and bdest2 are both prepared to receive data from bsrc.
   But it does not work the other way around -- you cannot do this:
</para>
<programlisting>
bo_match(bdest, bsrc);
bo_match(bdest, bsrc2); /* Wrong! */
</programlisting>
<para>
   ...because after this, <parameter/bdest/ will be prepared to receive data from <parameter/bsrc2/, not from <parameter/bsrc/.
   
   The second step is to set the read and write indices on your batchops.
   The function bo_set_in_idx() will set the destination's write index to point
   to any given "row", and the function bo_set_out_idx() will set the read
   index on the source:
</para>
<programlisting>
/* Copy starting from row 40 of bsrc to bdest starting at row 5 */
bo_set_in_idx(bdest, 5);
bo_set_out_idx(bsrc, 40);
</programlisting>
<para>
   Row values start at zero. There are also bo_inc_*_idx functions which 
   will move to the next row in the batchop, but you will rarely have a use 
   for those.  Make sure to set the index of a batchop before use: doing
   so ensures that the internal state is consistant.
</para>
<para>   
   Now we are ready to copy data. This is as simple as calling the macro
   bo_go:
</para>
<programlisting>
bo_go(bdest, bsrc, 6); /* Copy 6 "rows" of data from bdest to bsrc */
</programlisting>
<para>
   If you are in a threaded program, you can check the progress of the
   copy from another thread with the macro bo_at(bsrc).
</para>
<para>
   Simple, eh? But what's really going on, and why is it useful? Read
   on...
</para>
</sect1>
<sect1 id="bo-closer">
<title>A closer look</title>
<para>
   Take a look at Figure 1, at the green lines in the center (my
   apologies to the color-blind.) The figure depicts bsrc and bdest and
   shows the batchparms which they contain. Each batchparm has a
   "parmtype". When the bo_match macro is called, the parmtypes are
   compared and source batchparms are mapped to destination batchparms.
   So, as long as the bsrc contains at least one batchparm of the same
   parmtype as any given batchparm in bdest, it will receive values from
   the matching batchparm in bsrc.
</para>   
   <graphic FileRef="batchop1.png">
<para>
   There are some rules as to how this matching takes place.
</para>
<itemizedlist>
<listitem><para>
       Look at batchparm 1d. It has parmtype 4, but there are no
       batchparms in bdest that have parmtype 4. So the values managed by
       batchparm 1d are ignored, and will not be copied into the data
       area managed by bdest.
</para></listitem>
<listitem><para>
       Now look at batchparm 1c. It has parmtype 3, and so do batchparms
       2b and 2e. In this case, the data managed by batchparm 1c is
       duplicated into the data areas managed by batchparms 2b and 2e.
</para></listitem>
<listitem><para>
       If no match is found for a batchparm in bdest, then no values are
       copied into the data area managed by that batchparm.
</para></listitem>
<listitem><para>
       If more than one match occurs for a batchparm in bdest, the 
       first one found in bsrc is used and any further ones are ignored.
</para></listitem>
<listitem><para>
       The order in which the data is copied is the same as the order of
       the batchparms in bdest. In Figure1, first a value is copied from
       the column managed by batchparm 1b into the column managed by
       batchparm 2a. Then data is copied from 1c to 2b, then 1a to 2c,
       then 1e to 2d, then 1c to 2e.
</para></listitem>
</itemizedlist>
<para>      
   Now let us take a look at the black and blue arrows in Figure1. They
   show how a batchparm points to data in the managed data areas. In this
   example, bsrc is managing two blocks of data. Block 1 contains data
   columns managed by batchparms 1a, 1c, and 1d, and Block 2 contains
   data columns managed by batchparms 1b and 1e. The batchop bdest is
   only managing one data area, Block 3. Block1 and Block2 can be located
   anywhere in memory, and Block3 could even, if it were structured
   right, overlap with Block1 and Block2. Batchops are thus a handy way
   to merge arrays.
</para>
<para>  
   The "block" fields in the batchparms point to a data area, and the
   "offset" fields point to the first value in the column that the
   batchparm represents. A field which is not shown in the diagram, the
   "datasize" field, tells how many bits the values in the row contain.  
   It is easy to guess that from the width of the columns in the diagram.
</para>
<para>
   The "step" feild tells the distance between rows in this column. Note
   the batchparm 2a in the diagram. This has a different step value than
   the rest of the batchparms in bdest, and as a result, the data it
   represents is distributed differently inside Block 3. This would
   actually be an unusual batchop, but it does demonstrate the
   flexibility of batchops.
</para>
<para>  
   Now note the batchparm 1c. This has a "step" value of zero, which is a
   special case that designates a constant. Because of this, the columns
   managed by batchparms 2b and 2e will be filled with the value from
   that one memory location. (The beauty of that is that it is not
   actually a "special case" at all, because the step value can be added
   normally between rows and, being 0, ends up pointing to the same
   place.)
</para>
</sect1>
<sect1 id="bo-moredetailed">
<title>Adjustments, translation, and the real story of mapping.</title>
<para>
   You may have already noted in the diagram in the last section that the
   sizes of some of the columns do not match (for example, the 8 bit
   column managed by batchparm 1a is mapped to the 16 bit column managed
   by batchparm 2c.) This isn't a mistake. Batchops are meant to provide
   not only raw data copy operations, but also to do very basic translation
   between different data formats. The available translations supported 
   (so far) are:
</para>
<itemizedlist>
<listitem><para>   
    right or left justified integer resizing
</para></listitem>
<listitem><para>
    bitwise inversion
</para></listitem>
<listitem><para>
    integer negation
</para></listitem>
<listitem><para>
    integer test (< , <=, ==, =>, >, !=)
</para></listitem>
<listitem><para>
    endian and bitswap translations of the block
</para></listitem>
<listitem><para>
    endian and bitswap translations of the field
</para></listitem>
</itemizedlist>
<para>       
   In addition, though the diagram depicts only byte-aligned values of
   size 8, 16, and 32bits, the batchparm "step" and "size" values are
   actually given in bits, not in bytes, so bitwise operations are also 
   supported.
</para>
<para>
   Now for a confession: what was said earlier about how parmtypes are
   matched was a bit misleading. In fact, it is not merely the parmtype
   that is compared -- the data sizes, bit ordering, and sense of the
   parameters is compared to find the "best match" (the batchparm that 
   loses the least accuracy, and in the event of a tie on that criterion,
   the one that requires the least number of operations to perform the
   required transfer/conversions is chosen even if it is below another 
   batchparm in the source batchop.)
</para>
</sect1>
<sect1 id="bo-opt">
<title>Batchop optimization</title>
<para>
   Hold on, you say, now that you bring up efficiency, the code to bit
   shift values from a column, decide whether to perform the various
   conversions, deal with endianness issues, and then bit-shift and mask 
   it back into the destination column will either be a morass of flow 
   control statements, or horribly suboptimal. We'd get more speed by 
   just writing optimized code for the simpler case, right?
</para>
<para>   
   Well, we do optimize, but instead of optimizing off-the-cuff, we
   write interchangable, reusable optimizations.  Looking back at how 
   batchops are set in motion we mentioned the macro bo_go().  This 
   macro is defined as follows:
</para>
<programlisting>
#define bo_go(src, dest, n) \
((dest->go == NULL) ? bo_def_go(src, dest, n) : dest->go(src, dest, n))
</programlisting>
<para>
   Calling the bo_match() macro not only matches the batchparms to each
   other, it also examines both the source and the destination batchop
   and loads the best available bo_go implementation for the given combination
   of batchops onto the destination batchop's "go" hook. So if, for
   example, the two batchops contain only byte-aligned little endian
   values, they are processed by a slimmed down, optimized implementation
   that does not sacrifice speed for flexibility. (And yes, the default
   "handle anything" function, bo_def_go, is indeed a suboptimal morass
   of flow control statements, but at least it is there as a fallback.)
   
   Surprise: it does not end there. Look at the definition of the
   bo_match() macro:
</para>
<programlisting>
#define bo_match(src, dest) \
((dest->match == NULL) ? bo_def_match(src, dest) : dest->match(src, dest))
</programlisting>
<para>
   This is the capper which allows three extremely powerful things to
   happen. One is for optimized implementations to be mixed in from other
   codebases -- application and library developers are not limited to the
   generic set of implementations available in the mmutils mini-library.
   Another is that specialized translations can be added -- for example,
   translation of x,y,w,h values to x1,y1,x2,y2 coordinates on the
   fly. Finally, it allows for pseudo-batchops which do not store the
   values written to them, but rather use them as commands and parameters
   to perform operations as each row is written.
</para>
</sect1>
<sect1 id="bo-transient">
<title>A final detail: transient batchparms</title>
<para>
   What do you do if the destination batchop's managed area is
   write-only? Consider what happens if you are trying to load two
   four-bit batchparms from bsrc to the same write-only byte in bdest. If
   it is done naively, then the first value to be written will be zeroed
   out or otherwise mangled by the second value, or worse -- the attempt
   to read the old value out of the destination area in order to merge
   the unaffected bits could cause unforseen results like segmentation
   violations, corruption, or system hangs if directly accessing a
   hardware MMIO region.
</para>
<para>      
   To cope with this eventuality, a batchop may contain a special
   batchparm called a "transient".  A transient batchparm provides
   a "transient block" where other batchparms can read/write their 
   data, instead of reading directly from a batchparm in the source 
   batchop or writing directly to a batchparm in the destination
   batchop. For example, instead of having batchparms "3a" and
   "3b" which put interleaved 4-bit values directly into one of the
   "real" data blocks, the batchparm is given step 0 and pointed at
   location in the transient block, and later, when the transient
   batchparm itself is reached, the 8-bit combined value is pulled 
   from the transient block and put into the real data block.
</para>
<para>   
   Use of transient batchparms is not just for write-only memory. They
   may also to aid in optimization of the batchop "go" operation, to
   avoid redundant shifting and comparison of the data occupying the same
   bytes as the transferred data. As such, transient batchparms may
   contain a flag that causes the current contents of the final
   destination byte(s) to be read into the transient block before any
   values are transferred to/from the transient block. Using them is
   encouraged.  This flag must be set if the batchop in question will
   be acting as a source in addition to a destination.
</para>
<para>   
   Naturally, a transient batchparm should be positioned lower in the
   batchop than any batchparms that refer to it, such that it is written
   last, after the other batchparms have written into the transient
   block.
</para>
</sect1>
<sect1 id="bo-howtosource">
<title>Coding a source batchop</title>
<para>
   There are two levels at which to code with batchops.  The first is
   when you simply want to create a source batchop.  In this case, all
   you have to do is call bo_calloc to allocate a new batchop.  It takes
   as arguments the number of batchparms the batchop should contain, 
   and a default value for the .block member of the batchparms.  It returns
   a pointer to your new batchop, or NULL if there was a failure to allocate
   memory.
</para>
<para>
   Once you have this, you should start by setting the .block and .offset
   of the first item in a data column into the batchparm that will represent
   the column.  Note that the value in .block is a normal C pointer, and 
   must be aligned to a 64 bit boundary.  The value in offset is given in 
   bits, and should not be greater than BO_BP_OFFSET_MAX or less than 
   BO_BP_OFFSET_MIN; if it is, then you will have to point .block closer 
   to the beginning of the column.
</para>
<para>
   Now, set the datasize of the batchparm to say how many bits wide
   the values in your column are and the step designating the number of
   bits between values (between the first bits in two consecutive values, 
   that is, e.g. it would be 8 if you were indexing a "row" of consecutive 
   characters.)  The step must be no more than BO_BP_STEP_MAX and no less
   than BO_BP_STEP_MIN (which is negative, so you can step backwards through
   memory)  Once you have set your blocks, offsets, sizes and steps 
   on all your batchparms, you should call bo_set_in_idx and bo_set_out_idx 
   to get the internal fields of the batchop correctly initialized.
</para>
<para>
   Now assign the batchparm a type: types are application specific and 
   will be defined either by you for use with a destination batchop you
   are writing, or in the headers of a library which is using mmutils
   (e.g. various extensions to LibGGI, for which mmutils was originally
   developed.).
</para>
<para>
   There are all sorts of flags available to modify the parmtype. If 
   the value is signed, OR the parmtype with the BO_PT_SINT flag value.
   If the value should be left justified when moved to a larger 
   data size, set the BO_PT_JUSTIFY_LEFT (this will also affect the
   truncation behavior if the data is moved to a smaller datasize.)
   You may set the BO_PT_INVERT flag to have the value bitwise inverted,
   e.g. for an active-low bit.
</para>
<para>
   In addition, there are operations that may be performed on the 
   batchop that take a constant argument, represented by the 64-bit
   integer batchparm member .arg, which is available in signed (.arg.u64),
   and unsigned(.arg.s64) flavors, depending on whether you set the
   BO_PT_SINT flag.  These operations only occur on the parameter when 
   it is being read out of a source batchparm.  You may choose from 
   several "adjuster" operation values: to add .arg to the value, use
   BO_PT_ADD, or compare it (e.g. with BO_PT_CMP_LTE) to produce a 
   boolean result, or you may negate the value by placing the value 
   1 in the .arg member and or'ing the value BO_PT_NEGATE.
</para>
<para>
   Finally, we get down to the lurid business of endianness.  There is
   a .endian member in the batchparm containing two sets of flags to 
   deal with the horrible mess that is endianness.  You may not have 
   to touch it.  Basically you do not have to touch it (that is, you
   should leave it set to 0) if all your batchparms are either 8, 16, 32, 
   or 64-bit values aligned on 8, 16, 32, or 64 bit boundaries respectively.
</para>
<para>
   The first set of flags applies to the block the data is stored in.
   If this block needs to be reordered you can use the BP_EB_RE_* flags
   to reverse the bit or byte ordering. Note that each of those
   flags means only to swab chunks of that particular size, so, for
   example, translating from 32-bit little endian to 32-bit big-endian
   or visa versa is not done by setting the flag BO_EB_RE_32,
   but rather by setting the flags of the chunk sizes to be swapped:
   BO_PT_RE_16 | BO_PT_RE_8.  Make sure to get the block endian flags right,
   or your data will be horribly munged -- and, when you set them, make
   sure that the area pointed to by your data block is accessible when
   padded out to align with the first and last possible location of 
   your column values.
</para>
<para>
   The second set of flags pertains to the feild in the block, after
   the block itself has been un-munged.  If you were to memcpy/bitshift 
   the data in your feild into the smallest C integer data type that would
   contain it, would you then have to swab any nybbles/bytes/words to
   get the value contained in the C integer to match the value the field
   is supposed to contain?  In that case, set the appropriate BP_EP_RE_* 
   flags on this batchparm.
</para>
<para>
   Finally, if the block has restrictions on the data access widths
   used to read it, you will want to set the BP_ACCESS_* flags for each
   of the supported access widths.  (The default value, 0, is a special
   case that means that all access widths are supported _and_ that 
   it is OK to copy bytes which do not contain part of your column, 
   if it is more efficient to do so than to skirt around those bytes.
   This is the appropriate setting for normal system RAM.)
</para>
<para>
   Great, so now you can go create a hairy bastard 2 and 4-bit endian 
   reversed 17-bit-wide 25-bit-step column of data that gets the value 15 added
   to it, with no cares, right?  Well, keep in mind that someone is
   writing the source batchop and its associated match/go functions, and they
   are probably optizing more for the common cases.  If you do something 
   contorted, you'll probably be shunted through a less optimized
   "go" function, and won't get the speed you were hoping for.  Use
   common sense when generating data to be fed through a batchop; don't
   rely on them to do work you can do when first filling your block with
   data.  Or, in some cases, you don't have to depend entirely on common 
   sense: if you have been provided the handle to, or copy of, your destination
   batchop it is considered fair usage to look inside and customize the
   data you generate to be easily convertable to the values which the
   destination batchop is expecting.
</para>
</sect1>
<sect1 id="bo-howtodest">
<title>Coding a destination batchop</title>
<para>
   This section of the docs is not complete, as the codebase is still
   in the process of converging (been back to square one a few times
   so far.)
</para>
<para>
   A "destination batchop" is usually more than just a batchop structure
   as described above... unless you are satisfied with a pre-existing 
   set of match/go functions you will want to write your own.  The mmutils
   library contains numerous conveniences to help with that task.
</para>
<para>
   Probably the most useful tool in mmutils is the bo_summarize function.  
   This can be run on both the source and destination batchops to give a rough
   idea of how simple the batchparms contained inside them are.  The
   bo_summarize function expects a pointer to a bo_summary structure,
   which you will have to allocate.  See the comments in the mmutils.h
   file to understand what the returned values mean.  There is also a 
   bo_summarize_pair function which returns some additional information
   about any given combination of source/destination batchops.
</para>
</sect1>

</chapter>

</book>
