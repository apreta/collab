<chapter id="libgalloc-chapter-faq">
<title/LibGAlloc Frequently Asked Questions/

<!-- LibGAlloc FAQ -->

<qandaset>

<qandadiv>
<title/LibGAlloc general topics/


<!-- ==================================================================== -->
<qandaentry>
<question><para>What is LibGAlloc?</para></question>

<answer>
<para>
See <link linkend="libgalloc-chapter-intro">the introduction to LibGAlloc</link>.
</para>

<para>
For a bit of background:
</para>

<para>
LibGAlloc is being developed to access the graphics functionality KGI (the
GGI kernel code) will provide, once it is finished.  In the meantime it
will cut its teeth on currently functional LibGGI target systems. At its
core, LibGAlloc is really a protocol more than anything else. It provides
a central place for requests for graphics features to be sent by LibGGI
extension libraries, or even directly from the application level.  Think
of it as an easily-expandable, fully-featured, extension-friendly
ggiSetMode().
</para>

<para>
LibGAlloc is primarily meant to be used by those who write extensions to
LibGGI.  See the LibGGI Extension HOWTO to get a full understanding of
what an extension is.  Further, LibGAlloc is primarily concerned with
extensions that provide an API for the use of graphics hardware features.
</para>
</answer>

</qandaentry>

<!-- ==================================================================== -->
<qandaentry>
<question><para>What is a hardware feature?</para></question>

<answer>

<para>Short Answer: What isn't?</para>

<para>
Long Answer: A feature is hardware support for doing some sort of graphics
operation.  The following paragraphs offer many examples if features, as
well as a bit of a history of their evolution.
</para>

<para>
The IBM VGA chip standard has some capabilities that are not supported by
the base LibGGI, instead they are supported by the LibGGIMisc extension.
Hardware split-line, rastor position detection, hardware textmode font
cells, and plane bitmasks all qualify as features.
</para>

<para>
The competing Commodore line of computers had at the same time a new
invention: the Sprite.  This feature went on to be implemented in several
IBM compatible graphics cards, most of the time to provide a mouse cursor
that did not interfere with the data in the framebuffer.
</para>

<para>
Early Apple computers had a graphics co-processor affectionately known as
the "Copper."  Since this co-processor would obey instructions it was
sent, it could be used to create many interesting effects by transforming
the data displayed on the screen mathematically.  This was the predecessor
the ROP feature in graphics chips to follow.
</para>

<para>
As the PC became dominant in the marketplace, various new video standards
evolved and have been carried forward into modern graphics hardware.  The
IBM 8514 standard introduced BLTs, which allowed fast copies of system RAM
into the graphics chip. In some implementations the copy could be done
without tying up the main CPU.  BLTs also came with a ROP feature, that
would allow mathematical transformation of the copied data without CPU
assistance.
</para>

<para>
3D chipsets introduced us to the "texture", which is like a BLT, but could
be twisted and morphed in a way that made it easy to draw 3d objects.  
Also introduced were Z and alpha buffers, which allowed 3d
lighting/shading effects and melding of different images to be handled
without the main CPU doing the arithmetic.
</para>

<para>
Many chipsets started to become available that would display motion video
from a TV tuner.  These were later followed by chips that would decode
compressed video.
</para>

<para>
In the years since, and in the years to come, features accumulate with
each innovation in graphics hardware.  LibGAlloc is designed for future
compatibility, so that new features will be able to use LibGAlloc without
major surgery.
</para>

</answer>

</qandaentry>


<!-- ==================================================================== -->
<qandaentry>
<question><para>That's a lot of stuff to support -- won't it be huge?</para>
</question>

<answer>

<para>
The LibGGI API aimed at reducing the set of operations it supported to the
lowest common denominator in order to be small and portable -- it supports
drawing operations that will work on almost anything that was meant to
display a screen to a user.
</para>

<para>
LibGAlloc is the opposite -- it supports any conceivable hardware feature
no matter how obscure and rare it may be.  But, unlike LibGGI, it does not
provide full support or a user-level API for these functions -- it only
provides a shopping guide that tells you where to find the features, and
the calculator you use to balance your graphics programming checkbook.  
The real support for each feature is provided by a LibGGI extension, and
since extensions are independent of each other, you can install only the
extensions which you want to use.
</para>

<para>
LibGAlloc is also modular like LibGGI, so it will have separate modules to
handle the back-end "targets", and you only need to install the targets
you want to use.  As such, LibGAlloc will, like LibGGI, be a leightweight
library suitable for use in environments with tight memory requirements.
</para>

</answer>

</qandaentry>

<!-- ==================================================================== -->

<qandaentry>
<question><para>Why Gerbils?</para>
</question>

<answer>

<para>
<graphic FileRef="rennmaus.gif">
Could you say no to a face like this?
</para>

<para>
"Degas" was a typo.  The release was really supposed to be called 
"degus".
</para>

<para>"I know, when the cats (=masters) are away, then the mice start
dancing on the tables..." -- Andreas Beck, LibGGI master wizard, before
going on vacation.
</para>

<para>
We really think video hardware manufacturers should stop wasting their time
with improving 3D technology and give the consumers what they want:
hardware accelerated dancing gerbils.
</para>

</answer>

</qandaentry>

<!-- ==================================================================== -->

</qandadiv>

</qandaset>

</chapter>


