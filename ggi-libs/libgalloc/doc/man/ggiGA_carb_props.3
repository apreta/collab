.TH "ggiGA_carb_props" 3 "2007-06-24" "libgalloc-current" GGI
.SH NAME
\fBggiGA_carb_props\fR : Carb resource properties
.nb
.nf
#include <ggi/ga_prop.h>

struct ggiGA_carb_props {

      struct ggi_ll_math_gc can, must;
      void **block;
      int numblock;
};
.fi

.SH DESCRIPTION
A carb resource represents a translation, alteration, or mixing of 
data from tank(s) before a motor positions the data.  This kind of
resource is used to represent \fIraster operations\fR, special 
feature-specific pallette entries or look-up tables, and data
format (endian, depth) translations.  The carb-specific resource
properties are accessible under the union member sub.carb.  For
carb resources, the common properties members qty, size, and flags
have meanings that vary based on the type of the carb.  The carb
is used most commonly in LibBlt, so refer to that library's 
documentation for a description of how \f(CWGA_RT_BOB\fR carbs use these 
fields.
.SH ROPS (RASTER OPERATIONS)
A 32-bit set of flags and bitmasks is defined in the enumerated type
\fBggi_ll_rop\fR.  These represent the types of operations that can be
performed by a \fIROP\fR.  For our purposes a ROP is an operation that
takes two tanks as \fIoperands\fR, which are compared either bit-by-bit or
pixel-by-pixel, and some sort of result is produced by taking each
pair of bits (or pixels) as arguments to a function which produces one
bit (or pixel) of the result.  Since some hardware uses \fIunary\fR (or
\fInon-associative\fR) operators, there are cases where a certain ROP is
represented by two bits, one for each operand.

A note on notation: earlier hardware implementations of ROPs assume 
that the two tanks of data being operated on were a \fIsource\fR tank, and 
the data which was to be replaced by the result of the operation.  
Later implementations allow operations between two tanks to be performed 
without the data to be replaced by the result being one of them.  As such, 
many of the symbols in the \fBggi_ll_rop\fR type have two names; the
former being a \f(CW*_SOURCE\fR and \f(CW*_DEST\fR pair and the latter being an 
\f(CW*_OPERAND1\fR, \f(CW*_OPERAND2\fR pair.  These symbols are equivalent and are
merely to allow code clarity in either context.  When we refer to
a symbol in the below descriptions, we will omit the suffixes and
may be refer to a pair of bits by their common root e.g. \f(CWLL_ROP_KEYVAL\fR
refers to the two bits \f(CWLL_ROP_KEYVAL_SOURCE\fR (a.k.a. \f(CWLL_ROP_KEYVAL_OPERAND1\fR)
and \f(CWLL_ROP_KEYVAL_DEST\fR (a.k.a. \f(CWLL_ROP_KEYVAL_OPERAND2\fR).

The least signifigant 16 bits of the \fBggi_ll_rop\fR type are used to
supply one bit for each of the \fIboolean\fR ROPs.  These rops compare the
two operands bit-by-bit and the resulting value is a logical operation
between the two bit values.  There are 16 \fItruth tables\fR possible,
some of which have, in addition to a name like
\f(CWLL_ROP_TRUTHTABLE_0001\fR, a commonly used name like \f(CWLL_ROP_NOR\fR.

The \f(CWLL_ROP_KEVAL\fR bits represent the ability to perform \fIkeying\fR,
which is sometimes imprecisely referred to \fIcomparing\fR.  To key is to
apply the ROP only when the data in one or the other operand matches,
or does not match, a specific value.  Further, the \f(CWLL_ROP_KEYMASK\fR
bits designate the ability to exempt certain bits from the key
comparison, such that these bits are \fIdon't care\fR bits.

The \f(CWLL_ROP_COEFF\fR bits represent the ability to scale the value of
each pixel by a constant coefficient.  This is not multiplication
unless the tank is grayscale -- each of the color channels is
separately scaled, and may be adapted for gamma correction.  Further,
when the tank contains an Alpha channel, the coefficient used is not a
constant, but rather the contents of the alpha channel in that pixel.
When used in this manner, the alternative \f(CWLL_ROP_ALPHA\fR symbol is
clearer to say.

The \f(CWLL_ROP_THRESH\fR bits represent the ability of the ROP to only be
performed when the pixel data is above (or below) a certain threshold
value.  This value represents the pixel's intensity as calculated by
weighting the values in each channel, not a simple integer value
derived from the whole pixel.  Further, when the tank contains a Z
channel, the comparison is assumed to be between Z values, rather than
by intensity.  When used in this manner, the alternative
\f(CWLL_ROP_DEPTH\fR symbol is clearer to say.

The \f(CWLL_ROP_ADD\fR bit represents the ability to \fIadd\fR the two pixels
together.  This is not always plain addition of the pixel's bit
values, it is addition of colors and uses a formula appropriate to the
tanks' color spaces.  Further the \f(CWLL_ROP_KEY_CMP\fR bit designates
the ability to \fIkey\fR on the result of this \fIaddition\fR.

The \f(CWLL_ROP_TESSELATE\fR, \f(CWLL_ROP_XWRAP\fR, and \f(CWLL_ROP_YWRAP\fR values
are used for \fIpattern fills\fR.  They apply only to the source, or
operand1.  \f(CWLL_ROP_TESSELATE\fR indicates that the ROP can take a
pattern inside a tank and repeat it many times to fill a larger region
with that pattern.  The others indicate that the pattern can be
\fIpinned\fR, such that the resulting fill begins at some offset inside
the pattern, rather than at the pattern's top left corner.

The \f(CWLL_ROP_AUX\fR bits indicate that this ROP feeds into another ROP
before being passed into the motor.  If \f(CWLL_ROP_OPERAND1_AUX\fR is
set, then the result from this rop becomes the first operand of
another ROP.  If \f(CWLL_ROP_OPERAND2_AUX\fR is set, then the result from
this rop becomes the second operand of another ROP.  IF both are set
(\f(CWLL_ROP_AUX\fR) this is a special case indicating a \fIside-by-side\fR
rop, which is most often used such that two tanks can be compared for
keying purposes, deciding whether or not to perform a different ROP on
a corresponding pixel in two other tanks.

As mentioned above, Z and Alpha operations are implicit when certain
ROP flags are set and the set of tanks being processed contain Z or
Alpha values.  There are two common ways in which Z and ALPHA
operations are performed.  In one mode, most common for Alpha and rare
for Z, the Z or Alpha data is contained in a channel alongside the
data for each color channel in the pixel itself.  In another mode,
most common for Z and rare for Alpha, the Z or Alpha data is kept in a
separate tank of the same geometry as the tank containing the color
data.  In the latter case \f(CWLL_ROP_AUX bits\fR are both set as
described in the last paragraph.

The \f(CWLL_ROP_ALT\fR bit is there to deal with unforseen or quirky ROPs that 
may need to be added to this API at a later date.  If it is set, assume
you know nothing about the meaning of the \fBggi_ll_rop\fR value in question.
A \fBggi_ll_rop\fR item with a value of \f(CWLL_ROP_NOP\fR does absolutely nothing.
.SH ADJUSTERS
Adjusters represent the ability of a carb to adjust data between
various pixel formats.

The \f(CWLL_ADJ_ENDIAN*\fR bit flags represent the ability of the hardware
to compensate for differences between the endiannes (in 4, 8, 16, or
32 bit endian slices) of tanks.  The \f(CWLL_ADJ_BACKASS_WORDS\fR bit
represents the ability to reassemble the pixel with the bits in
reverse order, as a format that carries the bits backwards was
unfortunately popularized by an certain notorious operating system
whose name should not be spoken.

The \f(CWLL_ADJ_UPSIZE\fR and \f(CWLL_ADJ_DOWNSIZE\fR indicate an ability to
translate pixels between different bit-depths.

The \f(CWLL_ADJ_USE_OWN_CLUT\fR flag indicates that an internal CLUT
separate from CLUTs of the involved tanks is available for use.  The
\f(CWLL_ADJ_COLORSPACE\fR flag indicates the ability to gamma correct or
adjust between colorspaces (YUV/RGB/CMYK).
.SH ORGANIZATION OF THE CARB RESOURCE
The carb properties members named \fBmust\fR and \fBcan\fR contain each a
\fBggi_ll_rop\fR named \fBrop\fR, a \fBggi_ll_adjuster\fR named \fBadj\fR, and a
\fBggiGA_storage_type(3)\fR named \fBtostorage\fR.  The former are described
above; the latter refer to the type of storage of the destination
resource, which the carb and the motor that owns it has been created
to send the data to.  This does \fBnot\fR mean that normal
\fBstorage_need\fR and \fBstorage_ok\fR properties members in the carb
resource refer to the storage type of the source data (the source data
is a tank resource and has it's own \fBstorage_ok\fR, \fBstorage_need\fR
members).  Naturally, the values in the structure named must are
required for the request to succeed, and the ones in the structure
named can are just wishlist requests.  If the carb resource is not
flagged \f(CWGA_STATE_NOCHANGE\fR, or is being returned by \fBggiGASet(3)\fR,
then the values returned in the structure named must are always
active, and cannot be disabled, while the ones returned in the
structure named can may be turned on or off through some mechanism
provided by a higher level extension target.

The \fBtotype\fR carb properties member designates what types of resources
are valid candidates to receive the data from this carb (via its
motor).  This can be a specific type/subtype, or can be a whole
resource type family by using one of the \f(CWGA_RT_*_DONTCARE\fR values.
Usually, this value is a tank.  The \fBtogt\fR carb properties member is
similar, it defines the valid graphics scheme/depth which are eligible
to receive data from this carb (via its motor), and can contain the
various auto/dontcare values accepted by \fBggiSetMode(3)\fR.

Several pointers (\fBblock1\fR, \fBblock2\fR, \fBblock3\fR) are available and are
used to offer hints which allow extension targets to find resources
which are used to control the carb.  For \f(CWGA_RT_BOB\fR resources,
these are used to hand back addresses of \fIbatchop blocks\fR.  Another
possible use is as a handoff for the addresses of internal
CLUT/ILUT/TLUT tables stored in VRAM.
.SH SEE ALSO
\f(CWggiGA_resource_props(3)\fR
