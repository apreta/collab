.TH "ggiGA_resource_state" 3 "2007-06-24" "libgalloc-current" GGI
.SH NAME
\fBggiGA_resource_state\fR : The Resource States
.SH SYNOPSIS
.nb
.nf
#include <ggi/ga_types.h>

enum ggiGA_resource_state;
.fi

.SH DESCRIPTION
The resource state is a variable that contains bitwise flags that
represent the status of a resource during negotiation. Some of these flags
are meant to be set only by the user, some of them are meant to be set
only by LibGAlloc, and some of them are bi-directional.  You can find out
which is which by masking with the values \f(CWGA_STATE_REQUEST_MASK\fR
and \f(CWGA_STATE_RESPONSE_MASK\fR.
.SH THE "FAILURE" FLAG MEANS THE RESOURCE USED TOO MUCH SPACE
The \f(CWRS_STATE_FAILED\fR flag is set by LibGAlloc; it will also be
cleared by LibGAlloc, so there is no need to worry what state it is in
when you send a resource to LibGAlloc.  This flag indicates that this
resource is the point in the list where LibGAlloc decided that the feature
list did not fit.
.SH LIBGALLOC SETS THE "MODIFIED" FLAG IF IT MAKES A SUGGESTION
The \f(CWRS_STATE_MODIFIED\fR flag is only set by LibGAlloc.  When it has
been set it means LibGAlloc has altered the valus of the sizes of the
resource.  This can be one of two cases -- if the resource contained
\f(CWGGI_AUTO\fR sizes, then the \f(CWGGI_AUTO\fRs have been replaced by
the largest sizes that would fit.  The other case, which you can tell by
the \f(CWRS_STATE_FAILED\fR flag returned by LibGAlloc, is that the
resource failed.  In this case, the sizes have been replaced with the
largest sizes that would succeed, whether or not they contained
\f(CWGGI_AUTO\fR.
.SH THE "NOCHANGE" FLAG TELLS LIBGALLOC NOT TO MAKE SUGGESTIONS
The \f(CWRS_STATE_NOCHANGE\fR flag is only set by the creator of a
resource. (In rare cases, this can be LibGAlloc.)  It is not altered after
the resource is created.  This flag tells Galloc not to suggest values for
the sizes of this resource if it fails during a ggiGACheck.  Normally, if 
a resource fails, LibGAlloc will set \f(CWRS_STATE_MODIFIED\fR flag, 
and replace the sizes in the object with smaller sizes that would succeed.  
This is like the optional suggestion parameter passed back by ggiCheckmode.
Note that this flag is only honored by \fBggiGACheck(3)\fR, and is ignored 
by ggiGASet, so, the values in the reslist returned from ggiGASet may have 
been altered regardless.
.SH THE "SEE ABOVE" FLAGS DESIGNATES A REFERRING RESOURCE
The \f(CWRS_STATE_SEEABOVE\fR flag is only set by the creator of a
resource. (In rare cases, this can be LibGAlloc.)  It is not altered after
the resource is created.  This flag designates the resource to be a
"referer resource" as described above.  Many of the properties fields in
this resource might be ignored in favor of the values in the first resource
in the current compound resource, (exactly which values are overridden
depends on the types of both resources.)
.SH THE "CAP" FLAGS DESIGNATES A RESOURCE CAP
The \f(CWRS_STATE_CAP\fR flag is only set by the creator of a resource.
(In rare cases, this can be LibGAlloc.)  It is not altered after the
resource is created.  This flag designates the resource to be a "resource
cap" as described above.  The sizes in this resource must be greater than
the values in the resource before it in the list, (\f(CWGGI_AUTO\fR
should be considered infinity for this purpose.)  When budgeting space,
LibGAlloc will make the resource use at least as much space as the
resource before it, but no more than asked for by this resource.
.SH THE "NORESET" FLAGS DESIGNATES AN IN-SERVICE FEATURE
The \f(CWRS_STATE_NORESET\fR flag is only set by the creator of a
resource. (In rare cases, this can be LibGAlloc.)  It is not altered after
the resource is created.  This flag designates the resource to represent a
feature that is currently being used, and cannot be reallocated.  Any
feature anywhere in the request list which would force this resource to be
reallocated or use of it to be interrupted in any way will fail.
.SH THE "SHARED" FLAG DESIGNATES A SHARABLE RESOURCE
The \f(CWRS_STATE_SHARED\fR flag is only set by LibGAlloc.  When it has
been set it means the resource properties structure can be/is shared.
Certain types of negotiations require a resource to appear multiple
times in the request list; sharing the properties structure is done in
these cases for efficiency and simplicity of internal code.
.SH THE "TAG" FIELD DEFINES ADVANCED LIST STRUCTURE
Resources are considered to be in a \fItag\fR group if the bits masked by
\f(CWGA_STATE_TAG_MASK\fR are non-zero.  There are two types of tag
groups, one which has tanks in it, the other which has motors and
carbs; and they work a bit differently.

Tag groups containing tanks are processed as follows: each time a tank
resource of that tag group is encountered, it, and the resources
attached to it, are negotiated as they normally would be, but it is
assumed that they will be reusing the same storage areas that previous
resources in the tag group used (if more space is needed for this
resource than for any of the previous resources in the tag group, a
larger area is requested).  The user is responsible at this point for
not trying to use the resources simultaneously, and for initializing
the storage areas after another resource has been using them.  Note
that \f(CWGA_RT_FRAME\fR resources are implicitly in a tag group with any
other \f(CWGA_RT_FRAME\fR resources.  Now, it is easy to see that the
process of \fIupsizing\fR a resource which was described earlier is
nothing more than requesting a second, bigger resource that shares the
same storage area, and simply not using the smaller resource.

In a tank tag group, compound resources interact with the tag group as
follows: any resources marked as \fIseeabove\fR and in the same compound
must exist simultaneously with each other and so they do not share
storage area with each other even though they share the same tag; they
only share storage with resources in other compound resources.  Any
resources in the compound which are not marked as part of the tag
group cause allocation of new storage areas, even if they exist in
previous compounds in that tag group... so be sure to join your
seeaboves into the tag group, as well.

Note also that if you wish to upsize a resource and it's attached
seeaboves, e.g. Z-Buffers, they must appear in both the original
compound resource, and in the second \fIupsize\fR resource -- do not
assume that because you have requested a Z-buffer above in the list
that it is implicitly requested for other resources in the same tag
group; it is not.

Now, for tag groups containing motors and carbs, the handling is
slightly different.  Generally these consist of one or more motors and
a lot of carbs.  The carbs are attached to tank compound resources as
seeabove, and this is how the tank-carb-motor pipeline is glued
together.  If negotiation of a carb succeeds, it means that the tank
it is attached to has a valid path (the carb) through which its data
can be sent to any of the motors which are in the same tag group as
the carb.

See \f(CWggiGA_resource_type(3)\fR section "Advanced list structure with tags"
for situations that are solved with tagheads.
.SH SEE ALSO
\f(CWggiGAGetState(3)\fR
