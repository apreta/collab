.TH "ggiGA_resource_props" 3 "2007-06-24" "libgalloc-current" GGI
.SH NAME
\fBggiGA_resource_props\fR, \fBggiGA_storage_type\fR, \fBggiGA_mode\fR : LibGAlloc resource properties
.SH SYNOPSIS
.nb
.nf
#include <ggi/ga_prop.h>

#define GA_FLAG(props)        props->flags

struct ggiGA_resource_props {

      enum ggiGA_storage_type storage_need;
      enum ggiGA_storage_type storage_ok;

      /* bits to control sharing by the user */
      enum ggiGA_storage_share storage_share;

      /* Meaning of qty and flags vary depending on type of resource. 
         For the latter, see GA_FLAGS* defs in ga_types.h */
      uint16_t qty;
      uint32_t flags;

      /* On the way into a request, this is the desired size, 
       * when returned back from gallocation, this is the
       * maximum size.  Units for tanks are in pixels; Units for motors
       * are in same units as snap grid (see GA_COORDBASE* values in .flags) 
       */
      union {
              ggi_coord area;
              uint32_t linear;
      } size;

      /* Possible variants */
      union {
              struct ggiGA_motor_props motor;
              struct ggiGA_carb_props carb;
              struct ggiGA_tank_props tank;
      } sub;
};

struct ggiGA_mode {
      ggi_mode mode;
      ggi_directbuffer db;
};
.fi

.SH DESCRIPTION
Most LibGalloc resources will contain a structure called the 
properties, which is set when the resource is added to a list
with \fBggiGAAdd(3)\fR, and a pointer to which can be retrieved with 
\fBggiGAGetProperties(3)\fR.

The properties structure contains commonly used parameters for the
negotiation of a resource.
.SH FIELDS COMMON TO MOST RESOURCES
If a resource has a props structure, then it will probably use some
of the following members.  An additional union member, sub, contains
members specific to the three resource variants: \fItank\fR, \fImotor\fR, and 
\fIcarb\fR resources, and is described in the next three sections.

The \fBqty\fR member is a generic field for storing an integer attribute.
For example, a \f(CWGA_RT_SPRITE_SPRITE\fR motor resource would use this
field in negotiation of how many sprites it can display on the screen
simultaneuously.

The \fBflags\fR field is a generic variable for holding flags.  For example, 
most motor resources will use the two flags \f(CWGA_PIXEL_MODE\fR or 
\f(CWGA_DOT_MODE\fR to define what units the snap grid parameters are in
(see below).

The \fBsize member\fR is used to define the size of the negotiated resource,
if it has one.  \fBGGI_AUTO\fR (0) is considered to mean automatically
find the maximum size possible for this resource.  The size member
can be accessed as size.x,size.y for two dimensional objects, or
size.linear for linear allocations.
.SH THE STORAGE TYPE
The two members \fBstorage_ok\fR and \fBstorage_need\fR check the capability
to use certain types of data storage for the resource.  The meaning of
\fBstorage_ok\fR and \fBstorage_need\fR are described below; Let us look at
the values these fields may contain.  First, the special value
\f(CWGA_STORAGE_DONTCARE\fR (which equals zero) means any type of storage
is acceptable.

The flag values \f(CWGA_STORAGE_READ\fR and \f(CWGA_STORAGE_WRITE\fR designate
that the storage is readable/writeable.  For convenience, the value
\f(CWGA_STORAGE_RW\fR (equal to \f(CWGA_STORAGE_READ | GA_STORAGE_WRITE\fR) is
also defined.  The flag value \f(CWGA_STORAGE_DIRECT\fR indicates that the
storage may be accessed as per normal system memory (even if it is
not.)  Unless it is set, special methods are needed to access the
storage.  The value \f(CWGA_STORAGE_NORMAL\fR (equal to \f(CWGA_STORAGE_RW |
GA_STORAGE_DIRECT\fR) is provided for convenience as well.

The flag value \f(CWGA_STORAGE_ONBOARD\fR indicates that storage is
located on-board the graphics device, and does not need to be
transferred across the system bus in order to be used in a graphics
operation (however, on the down side, this means that actually loading
content into the storage from regular system memory will require the
data to be passed across the system bus.)  The value
\f(CWGA_STORAGE_VRAM\fR (equal to \f(CWGA_STORAGE_NORMAL | GA_STORAGE_ONBOARD\fR)
is more convenient to use.

The flag value \f(CWGA_STORAGE_RAM\fR indicates that the storage is normal
system memory which is stored permanently in RAM, that is, it will not
be swapped out to secondary storage.  The value \f(CWGA_STORAGE_REALRAM\fR
(equal to \f(CWGA_STORAGE_NORMAL\fR | \f(CWGA_STORAGE_RAM\fR) is more
convenient to use.

The flag value \f(CWGA_STORAGE_SECONDARY\fR indicates that the storage is
located on a physical medium which is slower than system RAM, usually
a disk drive.  Likewise, the flag value \f(CWGA_STORAGE_TERTIARY\fR
indicates the storage is located on a medium which may have large
latency or very slow speed -- this usually indicates the storage is on
a network server which lacks service guarantees.

The flag value \f(CWGA_STORAGE_TRANSFER\fR indicates that the graphics
system is capable of using the storage without CPU assistance.  
For example, user-space DMA-capable buffers can be gotten using
the convenient value \f(CWGA_STORAGE_DMA\fR, which is equal to 
\f(CWGA_STORAGE_REALRAM | GA_STORAGE_TRANSFER\fR.

The flag value \f(CWGA_STORAGE_VIRT\fR indicates that an automatic caching
system (transparent to the user) is in effect which makes slower
storage appear to be interchangable with faster storage.  For example,
the convenient value \f(CWGA_STORAGE_SWAP\fR represents virtual RAM which
may be stored in either real ram, or in \fIvirtual memory\fR on a hard
drive.  Accordingly, \f(CWGA_STORAGE_SWAP\fR is equal to
\f(CWGA_STORAGE_REALRAM | GA_STORAGE_SECONDARY | GA_STORAGE_VIRT\fR.
Another convenience value \f(CWGA_STORAGE_GART\fR is provided and equals
\f(CWGA_STORAGE_REALRAM | GA_STORAGE_ONBOARD | GA_STORAGE_VIRT\fR.

The flag value \f(CWGA_STORAGE_HOTPIPE\fR indicates that read and write
operations to the storage do more than alter the contents of the
storage, and may in fact trigger graphics operations.

The \fBstorage_need\fR and \fBstorage_ok\fR members work together to provide a
flexible negotiation system. When a request is submitted to LibGAlloc
via ggiGASet, each bit flag position is checked in both positions, and
the following meanings apply depending on the values found:
.TP
\f(CWstorage_ok = 0\fR, \f(CWstorage_need = 0\fR
This type of storage is not acceptable.  The resource will fail if 
it is only available with this type of storage.

.TP
\f(CWstorage_ok = 0\fR, \f(CWstorage_need = 1\fR
This type of storage is desirable, but is not entirely necessary.
If this type of storage is not available, the resource will still
succeed, but if it is available, LibGAlloc will try to allocate it
before any other type of storage.

.TP
\f(CWstorage_ok = 1\fR, \f(CWstorage_need = 0\fR
This type of storage is undesirable, but is acceptable.  If this
type of storage is the only kind available, the resource will
still succeed, but if other storage is available, LibGAlloc will
try to allocate it first.

.TP
\f(CWstorage_ok = 1\fR, \f(CWstorage_need = 1\fR
This type of storage is necessary.  The resource will fail unless
it is available.

.PP
LibGAlloc also returns values in these fields.  The values in these
fields will be always be altered by \fBggiGASet(3)\fR.  When \fBggiGACheck(3)\fR
is called the values will not be altered if the resource is flagged with 
\f(CWGA_STATE_NOCHANGE\fR (see the section on resource state).  The meanings of 
the returned flags are as follows when a call to \fBggiGACheck(3)\fR or
\fBggiGASet(3)\fR fails:
.TP
\f(CWstorage_ok = 0\fR, \f(CWstorage_need = 0\fR
Any requests for this resource that have this bit set will fail.
This type of storage is not compatible with this resource.

.TP
\f(CWstorage_ok = 0\fR, \f(CWstorage_need = 1\fR
Requests for this resource will be more likely to succeed if this
bit is set to 1.

.TP
\f(CWstorage_ok = 1\fR, \f(CWstorage_need = 0\fR
Requests for this resource will be more likely to succeed if this
bit is set to 0.

.TP
\f(CWstorage_ok = 1\fR, \f(CWstorage_need = 1\fR
Any requests for this resource will fail unless this bit is set.
Storage of this type is the only kind compatible with this resource.

.PP
Note that downsizing the resource may help in the second and third
situation above depending on the available amounts of different types
of storage.  In this case, suggested sizes are supplied in the
returned resource.  In the first and fourth situations, altering
characteristics of the resource may change the values returned
entirely, by changing the types of storage that the resource is
compatible with.  Clues as to minimal alterations needed to make the
resource work with the originally requested storage type may have been
placed by LibGAlloc in various members of the returned resource.

When \fBggiGASet(3)\fR (or \fBggiGACheck(3)\fR) is successful, both members will
contain the same value, which will have bit flags set accordingly to
represent the type of storage which has actually been (or would be)
obtained and allocated for use by this resource.
.SH SEE ALSO
\f(CWggiGA_tank_props(3)\fR,
\f(CWggiGA_motor_props(3)\fR,
\f(CWggiGA_carb_props(3)\fR
