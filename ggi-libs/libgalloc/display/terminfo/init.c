/* $Id: init.c,v 1.6 2006/02/04 22:15:09 soyt Exp $
******************************************************************************

   LibGAlloc implementation for "terminfo" target -- Initialization.

   
  
   Copyright (c) Mon Mar  5 2001 by: 
	Brian S. Julin		bri@calyx.com

  
   
This software may be redistributed under the terms of the GNU Public
License -- see the file COPYING in the parent directory of the 
source code.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.




******************************************************************************
*/

#include "terminfogalloc.h"

static int GGIopen(ggi_visual *vis, struct ggi_dlhandle *dlh,
		   const char *args, void *argptr, uint32_t *dlret)
{
  struct terminfogalloc_priv *priv;
  
  GGIDPRINT("LibGAlloc: GGIopen(%p, %p, %s, %p, %p)" 
	    " called for terminfo_GAlloc sublib\n",
	    vis, dlh, args ? args : "(NULL)", argptr, dlret);

  /*-* Initialize target-private data structure */

  priv = calloc(1, sizeof(struct terminfogalloc_priv));
  if (priv == NULL) return GGI_ENOMEM;
  TERMINFOGALLOC_PRIV(vis) = priv;

  /*-* Hook in target functions */

  LIBGGI_GALLOCEXT(vis)->set = GALLOC_terminfo_Set;
  LIBGGI_GALLOCEXT(vis)->release = GALLOC_terminfo_Release;
  LIBGGI_GALLOCEXT(vis)->check = GALLOC_Terminfo_Check;
  
  /*-* Apply initial values */
  priv->vgasoftcursor = 1;
  
  *dlret = GGI_DL_EXTENSION | GGI_DL_OPDISPLAY;
  return 0;
}

static int GGIclose(ggi_visual *vis, struct ggi_dlhandle *dlh)
{

  /* Restore the cursor -- should rather be done by core terminfo target */

  /*-* Free target-private structure */
  free(TERMINFOGALLOC_PRIV(vis));

  return 0;
}


EXPORTFUNC
int GALLOCdl_terminfo_galloc(int func, void **funcptr);

int GALLOCdl_terminfo_galloc(int func, void **funcptr)
{
      switch (func) {
      case GGIFUNC_open:
	*funcptr = GGIopen;
	return 0;
      case GGIFUNC_exit:
	*funcptr = NULL;
	return 0;
      case GGIFUNC_close:
	*funcptr = GGIclose;
	return 0;
      default:
	*funcptr = NULL;
      }
      
      return GGI_ENOTFOUND;
}
