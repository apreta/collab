/* $Id: funcs.c,v 1.7 2004/11/27 19:10:17 soyt Exp $
******************************************************************************

   Galloc implementation for X target - functions.

   Copyright (C) 2001	Christoph Egger	[Christoph_Egger@t-online.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "xgalloc.h"

int GALLOC_terminfo_check_callback(ggi_visual_t vis,
			    ggiGA_resource_handle handle,
			    ggiGA_resource_handle compound,
			    ggiGA_resource_handle previous,
			    ggiGA_resource_handle lastmode,
			    void **checkstate)
{
	int rc = GALLOC_EUNAVAILABLE;
	int res_state;
	struct terminfogalloc_container_t *checkstate_t = NULL;

	DPRINT("%s:%s:%i: called\n", DEBUG_INFO);

	/* This demonstrates how to hold state between callbacks. */
	if (handle == NULL) {
		LIB_ASSERT(compound == NULL);
		LIB_ASSERT(previous == NULL);
		LIB_ASSERT(lastmode == NULL);
                if (checkstate[0] == NULL) {
                        checkstate[0] = malloc(sizeof(struct terminfogalloc_container_t));
			if (checkstate[0] == NULL) {
				rc = GGI_ENOMEM;
				ggiGAFlagFailed(handle);
				return rc;
			}	/* if */

			checkstate_t = checkstate[0];
			checkstate_t->state = 1;
		} else {
			free(checkstate[0]);
			checkstate[0] = NULL;
		}	/* if */
		return GALLOC_OK;
	}	/* if */

	LIB_ASSERT(compound != NULL);
	LIB_ASSERT(handle != NULL);

	/* We should never be called directly on a cap. */
	LIB_ASSERT(!ggiGAIsCap(handle));

	res_state = (handle->res_state & GA_STATE_REQUEST_MASK);

        /* Resources which are already active have already been checked. */
	if ((res_state & GA_STATE_NORESET) == GA_STATE_NORESET) {
		rc = GALLOC_OK;
		return rc;
	}	/* if */

	switch (handle->res_type & GA_RT_TYPE_MASK) {
	case GA_RT_FRAME:
	  /* Sanity */

		break;

	case GA_RT_SPRITE:

	  /* Sanity */

		break;

	default:
		break;
	}	/* switch */

	return rc;
}	/* GALLOC_terminfo_check_callback */




int GALLOC_terminfo_set_callback(ggi_visual_t vis,
			  ggiGA_resource_handle handle,
			  ggiGA_resource_handle previous,
			  ggiGA_resource_handle lastmode,
			  void **setstate)
{
	int rc = GALLOC_EFAILED;
	int res_state;

	LIB_ASSERT(handle != NULL);

	if (handle == lastmode) {
		if (*setstate == NULL) {
			/* This is the first callback. */
			*setstate = malloc(sizeof(int));
			*((int *)*setstate) = 1;
		} else {
			/* This is the last callback. */
			free(*setstate);
			*setstate = NULL;
			/* terminfo target sets on the first callback. */
			return GALLOC_OK;
		}	/* if */
	}


	DPRINT("%s:%s:%i: called\n", DEBUG_INFO);
	res_state = (handle->res_state & GA_STATE_REQUEST_MASK);

	if (res_state & GA_STATE_NORESET) {
		rc = GALLOC_OK;
		return rc;
	}	/* if */

	if (handle->res_count > 0) {
		/* already in use */
		handle->res_count++;
		rc = GALLOC_OK;
		return rc;
	}	/* if */


	switch (handle->res_type & GA_RT_TYPE_MASK) {
	case (GA_RT_FRAME & GA_RT_TYPE_MASK):

		break;

	case (GA_RT_SPRITE & GA_RT_TYPE_MASK):

		break;

	}	/* switch */

	if (rc == GALLOC_OK) {
		/* Increase usage-counter */
		handle->res_count++;
		handle->vis = vis;
		handle->res_state |= GA_STATE_NORESET;
	}	/* if */

	return rc;
}	/* GALLOC_terminfo_set_callback */




int GALLOC_terminfo_release_callback(ggi_visual_t vis,
			      ggiGA_resource_handle handle)
{
	int rc = GALLOC_EFAILED;


	DPRINT("%s:%s:%i: called\n", DEBUG_INFO);

	LIB_ASSERT(handle != NULL);

	if (handle->res_state & GA_STATE_FAILED) {
		/* allocation failed! */
		rc = GALLOC_OK;
		return rc;
	}	/* if */

	handle->res_count--;
	if (handle->res_count < 0) {
		/* Never allocated! */
		handle->res_count = 0;
		rc = GALLOC_OK;
		return rc;
	}	/* if */

	LIB_ASSERT(handle->res_count >= 0);
	DPRINT("%s:%s:%i: handle->res_count: %i\n",
		DEBUG_INFO, handle->res_count);

	if (handle->res_count > 0) {
		/* already in use */
		rc = GALLOC_OK;
		return rc;
	}	/* if */


	switch (handle->res_type & GA_RT_TYPE_MASK) {
	case (GA_RT_FRAME & GA_RT_TYPE_MASK):

		break;

	case (GA_RT_SPRITE & GA_RT_TYPE_MASK):

		break;

	default:
	  break;

	}	/* switch */

	return rc;
}	/* GALLOC_terminfo_release_callback */


int GALLOC_terminfo_check(ggi_visual_t vis, ggiGA_resource_list request,
		   ggiGA_resource_list *result)
{
	return ggiGACheck_template(vis, request, result,
				   &GALLOC_terminfo_check_callback);
}       /* ggiGACheck */


int GALLOC_terminfo_set(ggi_visual_t vis, ggiGA_resource_list request,
		 ggiGA_resource_list *result)
{
	return ggiGASet_template(vis, request, result,
				 &GALLOC_terminfo_set_callback);
}       /* ggiGASet */


int GALLOC_terminfo_release(ggi_visual_t vis, ggiGA_resource_list *list,
		     ggiGA_resource_handle *handle)
{
	return ggiGARelease_template(vis, list, handle,
				     &GALLOC_terminfo_release_callback);
}       /* ggiGARelease */

