/* $Id: terminfogalloc.h,v 1.4 2004/09/18 13:00:12 cegger Exp $
******************************************************************************

   Galloc implementation for terminfo target - header.

   Copyright (C) 2001	Brian S. Julin [bri@calyx.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#ifndef _GGI_GALLOC_terminfo_H
#define _GGI_GALLOC_terminfo_H


#include <ggi/internal/galloc.h>
#include <ggi/internal/galloc_debug.h>

#ifdef HAVE_NCURSES_H
#include <ncurses.h>
#elif defined(HAVE_NCURSES_NCURSES_H)
#include <ncurses/ncurses.h>
#else
#include <curses.h>
#endif

/*
 *	"exported" Function prototypes
 */
GACheck		GALLOC_terminfo_check;
GASet		GALLOC_terminfo_set;
GARelease	GALLOC_terminfo_release;
GA_Mode		GALLOC_terminfo__mode;
GAIsShareable	GALLOC_terminfo_isshareable;

struct TIhooks {
	SCREEN *scr;
	FILE *f_in, *f_out;
	int f_private;
	struct { int x, y; } origin;
	int splitline;
	int virgin;
	chtype color16_table[256];
	chtype charmap[256];
	ggi_visual *vis;
};

/* Structure holding private data.
 */
struct terminfogalloc_priv {
	void *dummy;
};


#define TERMINFOGALLOC_PRIV(vis) ((struct terminfogalloc_priv *)(LIBGGI_GALLOCEXT(vis)->priv))


struct terminfogalloc_container_t {
	int state;
	ggi_mode mode;
};

#endif	/* _GGI_GALLOC_terminfo_H*/
