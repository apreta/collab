/* $Id: anprintf.inc,v 1.2 2005/10/12 10:23:43 cegger Exp $
******************************************************************************

   Common helpers for targets ggiGAanprintf() implementation

   Copyright (C) 2005  Christoph Egger
   
   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/




/* Used to look up index of a pointer in the list, for more
 * human readable view of the inter-resource relationships.  
 * If found, a string with the index of the resource in the
 * list is returned, else a string containing the pointer value
 * is returned. 
 */
static int idx_str(char *buf, int size, ggiGA_resource_list list, 
		   ggiGA_resource_handle cthis) {
	int i;
	ggiGA_resource_handle tmp;

	if (ggiGAFind(list, cthis)) {
		return snprintf(buf, size, "<%p>", (void *)cthis);
	}	/* if */

	i = 0;
	RESLIST_FOREACH(tmp, list) {
		i++;
		if (tmp == cthis) break;
	}
	return snprintf(buf, size, "(%d)", i);
}

/* Generates a string for the common attributes of a resource, minus the 
 * inter-resource relationships.
 */
static int common_str(char *buf, int size, ggiGA_resource_handle res,
		      ggi_visual_t vis)
{
	ggiGA_resource_state s;

	s = res->res_state;
	return snprintf(buf, size, " %6s %s, %s "
			"Usage: %i (Handle = %p)\n"
			"     State:%s%s%s%s%s%s\n",
			ggiGAIsMotor(res) ? "motor" : 
			(ggiGAIsCarb(res) ? "carb" : "tank"),
			GA_RTSTRING(vis, res->res_type),
			GA_RTSUBSTRING(vis, res->res_type),
			res->res_count,
			(void *)res,
			(s & GA_STATE_NORESET)	? " -resetable"	 : "",
			(s & GA_STATE_NOCHANGE)	? " -negotiable" : "",
			(s & GA_STATE_MODIFIED)	? " +modified"	 : "",
			(s & GA_STATE_FAILED)	? " !failure"	 : "",
			ggiGAIsShareable(res)	? " +shareable"	 : "",
			(s & GA_STATE_CAP)	? " +cap"	 : "");
}

static int storage_str(char *buf, int size, ggiGA_storage_type st)
{
	return snprintf(buf, size, "%s%s%s%s%s%s%s%s%s%s%s",
			(st == GA_STORAGE_DONTCARE)	? " +any"	: "",
			(st & GA_STORAGE_SECONDARY)	? " +secondary"	: "",
			(st & GA_STORAGE_TERTIARY)	? " +tertiary"	: "",
			(st & GA_STORAGE_VIRT)		? " +virtual"	: "",
			(st & GA_STORAGE_RAM)		? " +ram"	: "",
			(st & GA_STORAGE_TRANSFER)	? " +transfer"	: "",
			(st & GA_STORAGE_HOTPIPE)	? " +hotpipe"	: "",
			(st & GA_STORAGE_DIRECT)	? " +direct"	: "",
			(st & GA_STORAGE_ONBOARD)	? " +onboard"   : "",
			((st & GA_STORAGE_WRITE) || 
			 (!st == GA_STORAGE_DONTCARE))	? ""   : " -write",
			(st & GA_STORAGE_READ) || 
			 (!st == GA_STORAGE_DONTCARE)   ? ""	: " -read");
}

static int motor_str(char *buf, int size, struct ggiGA_resource_props *props)
{
	struct ggiGA_motor_props *m;
	m = &(props->sub.motor);
	return snprintf(buf, size, 
			"     +Props:  Available ROPs: %i\n"
			"              Positioning Dotgrid: "
			"corner %i,%i (snap %i,%i), "
			"size %i,%i (snap %i,%i)\n"
			"              "
			"Dot ratio X: Min %i/%i max %i/%i, "
			"Dot ratio Y: Min %i/%i max %i/%i\n",
			m->rops,
			m->grid_start.x, m->grid_start.y,
			m->pos_snap.x, m->pos_snap.y,
			m->grid_size.x, m->grid_size.y,
			m->size_snap.x, m->size_snap.y,
			m->mul_min.x, m->div_max.x,
			m->mul_max.x, m->div_min.x,
			m->mul_min.y, m->div_max.y,
			m->mul_max.y, m->div_min.y
			);
}

static int carb_str(char *buf, int size, struct ggiGA_resource_props *props)
{
	struct ggiGA_carb_props *c;
	c = &(props->sub.carb);
	return snprintf(buf, size, 
			"     +Props:  \n"
			"     %i blocks",
			c->numblock
			);
}

static int tank_str(char *buf, int size, struct ggiGA_resource_props *props)
{
	struct ggiGA_tank_props *t;
	t = &(props->sub.tank);
	return snprintf(buf, size, 
			"     +Props:  Graphtype %x"
			" Advanced Buffer Format %p\n"
			"              Buffer Read: %p Write: %p"
			" Snap: 2D(%i,%i) or 1D(%i)\n",
			t->gt, (void *)t->db, 
			t->read, t->write, 
			t->buf_snap.area.x, t->buf_snap.area.y, 
			t->buf_snap.linear
			);
}


static inline int empty_str(ggiGA_resource_list list,
				size_t *size, char **out)
{
	char *result;

	if (*size == 0) {
		/* This is a silly request, but for consistency... */
		*out = malloc((size_t)1);
		**out = '\0';
		return 1;
	}	/* if */

	if (list == NULL || RESLIST_EMPTY(list)) {
		/* This resource list is empty. */
		if (list == NULL) {
			if (*size > 17) *size = 17;
			result = calloc(*size + 1, sizeof(char));
			strncpy(result, "<No list header>\n", *size);
		} else {
			if (*size > 13) *size = 13;
			result = calloc(*size + 1, sizeof(char));
			strncpy(result, "<Empty List>\n", *size);
		}
		*out = result;
		return 1;
	}	/* if */

	return 0;
}


