/* $Id: init.c,v 1.8 2006/02/04 22:15:09 soyt Exp $
******************************************************************************

   LibGAlloc implementation for LibKGI target -- Initialization.

   Copyright (c)2001  Brian S. Julin [bri@tull.umassp.edu]
   
   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   The above copyright notice applies to all files in this package, unless 
   explicitly stated otherwise in the file itself or in a file named COPYING 
   in the same directory as the file.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.





******************************************************************************
*/

#include "libkgigalloc.h"

static int GGIopen(ggi_visual *vis, struct ggi_dlhandle *dlh,
		   const char *args, void *argptr, uint32_t *dlret)
{
	gallocpriv *ga_priv;
	ggi_libkgi_priv *kgi_priv;

	DPRINT_LIBS("GGIopen(%p, %p, %s, %p, %p) called for "
			  "libkgi_galloc sublib\n", vis, dlh, 
			  args ? args : "(NULL)", argptr, dlret);

	ga_priv = LIBGGI_GALLOCEXT(vis);
	kgi_priv = LIBKGI_PRIV(vis);

	/*-* Initialize target-private data structure */

	/*-* Hook in target functions */

	ga_priv->set = GALLOC_LibKGI_Set;
	ga_priv->check = GALLOC_LibKGI_Check;
	ga_priv->release = GALLOC_LibKGI_Release;
	ga_priv->_mode = GALLOC_LibKGI__Mode;

        /*-* Apply initial values */

	*dlret = GGI_DL_EXTENSION | GGI_DL_OPDISPLAY;
	return 0;
}	/* GGIopen */


static int GGIclose(ggi_visual *vis, struct ggi_dlhandle *dlh)
{
	DPRINT_LIBS("GGIdlclose(%p, %p) called for"
			  "LibKGIGAlloc sublib\n", vis, dlh);

	/*-* Free target-private structure */


	return 0;
}	/* GGIclose */


EXPORTFUNC
int GALLOCdl_libkgi_galloc(int func, void **funcptr);

int GALLOCdl_libkgi_galloc(int func, void **funcptr)
{
	switch (func) {
	case GGIFUNC_open:
		*funcptr = GGIopen;
		return 0;
	case GGIFUNC_exit:
		*funcptr = NULL;
		return 0;
	case GGIFUNC_close:
		*funcptr = GGIclose;
		return 0;
	default:
		*funcptr = NULL;
	}	/* switch */
      
	return GGI_ENOTFOUND;
}	/* GALLOCdl_libkgi_galloc */
