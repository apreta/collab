/* $Id: funcs.c,v 1.5 2005/10/06 07:51:22 cegger Exp $
******************************************************************************

   LibGAlloc implementation for LibKGI target -- API functions.
  
   Copyright (c) 2001 Brian S. Julin [bri@tull.umassp.edu]
   
   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   The above copyright notice applies to all files in this package, unless 
   explicitly stated otherwise in the file itself or in a file named COPYING 
   in the same directory as the file.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


******************************************************************************
*/


#include "libkgigalloc.h"

/* _All_ non-local (ie not declared 'static') functions and variables _must_
   be prefixed with the extension name, and for sublibs also with a unique
   sublib identifier. This is to keep the namespace clean on systems where
   all symbols are exported by default.
*/

/*-* API Implementation */

/* These are the same as stubs right now, but copied anyway in case
 * they change and to isolate debugging statements to this target during
 * development.
 */

int GALLOC_LibKGI_Check(ggi_visual_t vis, ggiGA_resource_list request,
		       ggiGA_resource_list *result)
{
	struct ggiGA_template_state state;
	int rc;

	state.mode = NULL;
	state.haslist = NULL;

	rc = ggiGACheck_template(vis, request, result, &state, NULL);
	ggiGADestroyList(&(state.haslist));
	return rc;
}	/* GALLOC_LibKGI_Check */


int GALLOC_LibKGI_Set(ggi_visual_t vis, ggiGA_resource_list request,
		     ggiGA_resource_list *result)
{
	struct ggiGA_template_state state;
	int rc;

	state.mode = NULL;
	state.haslist = NULL;

	rc = ggiGASet_template(vis, request, result, &state, NULL);
	ggiGADestroyList(&(state.haslist));
	return rc;
}	/* GALLOC_LibKGI_Set */


int GALLOC_LibKGI_Release(ggi_visual_t vis, ggiGA_resource_list *list,
			 ggiGA_resource_handle *handle)
{
	struct ggiGA_template_state state;
	return ggiGARelease_template(vis, list, handle, &state, NULL);
}	/* GALLOC_LibKGI_Release */


int GALLOC_LibKGI_CheckIfShareable(ggi_visual *vis,
				  ggiGA_resource_handle handle, 
				  ggiGA_resource_handle tocompare)
{
	/* Silence compiler warnings */
	vis = vis;
	handle = handle;
	tocompare = tocompare;

	return (GALLOC_EFAILED);
}	/* GALLOC_LibKGI_CheckIfShareable */


int GALLOC_LibKGI__Mode(ggi_visual *vis, ggiGA_resource_handle *out)
{
	DPRINT("Function GALLOC_LibKGI__Mode (%p, %p) called.\n", 
		     vis, out);

	/* Silence compiler warnings */
	vis = vis;

	LIB_ASSERT(out != NULL); /* People who call this should know better. */

	if (out[0] == NULL) {
		/* Create new resource and make GGI_AUTO */
		ggi_mode *tmp;

		out[0] = calloc(1, sizeof(struct ggiGA_resource));
		if (out[0] == NULL) goto err0;

		out[0]->priv = calloc(1, sizeof(struct ggiGA_mode));
		if (out[0]->priv == NULL) goto err1;

		out[0]->priv_size = sizeof(struct ggiGA_mode);
		tmp = &(((struct ggiGA_mode *)(out[0]->priv))->mode);
		tmp->frames    = GGI_AUTO;
		tmp->visible.x = GGI_AUTO;
		tmp->visible.y = GGI_AUTO;
		tmp->virt.x    = tmp->virt.y = GGI_AUTO;
		tmp->size.x    = tmp->size.y = GGI_AUTO;
		tmp->graphtype = GGI_AUTO;
		tmp->dpp.x     = tmp->dpp.y = GGI_AUTO;
	}	/* if */

	out[0]->res_type = GA_RT_FRAME;
	/* Set up db. */
	return(GALLOC_OK);
 err1:
	free(out[0]);
	out[0] = NULL;
 err0:
	return(GALLOC_EUNAVAILABLE);
}	/* GALLOC_LibKGI__Mode */
