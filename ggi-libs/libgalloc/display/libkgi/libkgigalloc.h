/* $Id: libkgigalloc.h,v 1.3 2005/07/31 15:29:56 soyt Exp $
******************************************************************************

   LibGAlloc implementation for LibKGI target -- header.



   Copyright (c) Thu Mar 22 2001 by:
	Brian S. Julin		bri@calyx.com



   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   The above copyright notice applies to all files in this package, unless
   explicitly stated otherwise in the file itself or in a file named COPYING
   in the same directory as the file.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.





******************************************************************************
*/

#include <ggi/internal/galloc.h>
#include <ggi/internal/galloc_debug.h>
#include <ggi/display/libkgi.h>

/*-* Function prototypes */

GASet			GALLOC_LibKGI_Set;
GARelease		GALLOC_LibKGI_Release;
GACheckIfShareable	GALLOC_LibKGI_CheckIfShareable;
GAanprintf		GALLOC_LibKGI_anprintf;
GACheck			GALLOC_LibKGI_Check;
GA_Mode			GALLOC_LibKGI__Mode;

ggiGA_reslist_callback libkgi_galloc_check_mode;
ggiGA_reslist_callback libkgi_galloc_set_mode;

/*-* Structure holding private data. */
