/* $Id: callback.c,v 1.2 2004/09/18 13:00:11 cegger Exp $
******************************************************************************

   LibGAlloc implementation for LibKGI target -- default callbacks
  
   Copyright (c) 2001 Brian S. Julin		[bri@tull.umassp.edu]
   
   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   The above copyright notice applies to all files in this package, unless 
   explicitly stated otherwise in the file itself or in a file named COPYING 
   in the same directory as the file.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "libkgigalloc.h"

int libkgi_galloc_check_mode (ggi_visual_t vis,
			     ggiGA_resource_list reslist,
			     enum ggiGA_callback_command command,
			     ggiGA_resource_handle res,
			     ggiGA_resource_handle head,
			     ggiGA_taghead_t taghead,
			     ggiGA_resource_handle has,
			     struct ggiGA_template_state *state,
			     void *privstate) {
	ggi_mode mode;
	int rc;

	if (command & GA_CB_UNCHECK) goto uncheck;

	fprintf(stderr, "libkgi_galloc_check_mode called\n");
	
	return 0;
uncheck:
	return 2;
}


int libkgi_galloc_set_mode (ggi_visual_t vis,
			   ggiGA_resource_list reslist,
			   enum ggiGA_callback_command command,
			   ggiGA_resource_handle res,
			   ggiGA_resource_handle head,
			   ggiGA_taghead_t taghead,
			   ggiGA_resource_handle has,
			   struct ggiGA_template_state *state,
			   void *privstate) {
	fprintf(stderr, "libkgi_galloc_set_mode called\n");

	if (res->res_state & GA_STATE_NORESET)
	  return GALLOC_OK;	/* Already set, nothing to do. */
	return GALLOC_OK;
}	/* libkgi_galloc_set_mode */

