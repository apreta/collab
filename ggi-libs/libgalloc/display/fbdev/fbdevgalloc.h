/* $Id: fbdevgalloc.h,v 1.5 2005/07/31 15:29:56 soyt Exp $
******************************************************************************

   LibGAlloc implementation for "fbdev" target -- header.

   Copyright (c) Mon Mar  5 2001 by:
	Brian S. Julin		bri@calyx.com

This software may be redistributed under the terms of the GNU Public
License -- see the file COPYING in the parent directory of the
source code.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#ifndef _GGI_GALLOC_DISPLAY_FBDEV_H
#define _GGI_GALLOC_DISPLAY_FBDEV_H

#include <ggi/internal/galloc.h>
#include <ggi/internal/galloc_debug.h>
#include <ggi/display/fbdev.h>

/*-* Function prototypes */

GASet			GALLOC_Fbdev_Set;
GARelease		GALLOC_Fbdev_Release;
GAanprintf		GALLOC_Fbdev_anprintf;
GACheck			GALLOC_Fbdev_Check;
GA_Mode			GALLOC_Fbdev__Mode;
GACheckIfShareable	GALLOC_Fbdev_CheckIfShareable;


/*-* Structure holding private data. */

struct galloc_fbdev_features {

	/* Splitline feature usage counter.   */
	int splitline_count;

	/* Ray Position feature availability flag. */
	int raypos_flag;

	/* Ray trigger set feature usage counter. */
	int raytrig_set_count;

	/* Ray trigger pallette feature usage counter. */
	int raytrig_cmap_count;

	/* Hardware text cursor "sprite" counter. */
	int tcursor_count;
};	/* struct galloc_fbdev_features */


struct fbdevgalloc_priv {
	struct galloc_fbdev_features avail, curr;

	/* place to store cursor state/info */
	struct fb_fix_cursorinfo	tcursor_fix;
	struct fb_var_cursorinfo	tcursor_origvar;
	struct fb_cursorstate		tcursor_origstate;
	struct ggiGA_motor_props	tcursor_motor_props;
};


#define FBDEVGALLOC_PRIV(vis) \
((struct fbdevgalloc_priv*)(LIBGGI_GALLOCEXT(vis)->priv))

#endif	/* defined _GGI_GALLOC_DISPLAY_FBDEV_H */
