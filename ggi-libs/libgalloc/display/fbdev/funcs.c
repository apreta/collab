/* $Id: funcs.c,v 1.11 2008/01/21 23:33:46 cegger Exp $
******************************************************************************

   LibGAlloc implementation for "fbdev" target -- API functions.
  
   Copyright (c) Mon Mar  5 2001 by: 
	Brian S. Julin		bri@calyx.com
   
This software may be redistributed under the terms of the GNU Public
License -- see the file COPYING in the parent directory of the 
source code.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include <stdio.h>
#include "fbdevgalloc.h"


/* _All_ non-local (ie not declared 'static') functions and variables _must_
   be prefixed with the extension name, and for sublibs also with a unique
   sublib identifier. This is to keep the namespace clean on systems where
   all symbols are exported by default.
*/

				  
/* Find and ggiCheckMode the /etc/fb.modes timings which are 
   compatible with the given GA_RT_FRAME resource/cap, in order
   of most screen "volume" to least. */
static int findbestmode(ggi_visual_t vis, 
			ggiGA_resource_handle res, 
			ggiGA_resource_handle cap,
			ggi_mode *mode) 
{
  ggi_fbdev_timing *timings, *tlist;
  unsigned long *vlist, vmax;

  int num_timings, idx, rc;
  
  timings = FBDEV_PRIV(vis)->timings;
  if (timings == NULL) return GALLOC_EFAILED;
  num_timings = 1;
  while (timings->next != NULL) {
    num_timings++;
    timings = timings->next;
  }
  tlist = calloc(sizeof(*tlist), num_timings);
  vlist = calloc(sizeof(*vlist), num_timings);
  
  timings = FBDEV_PRIV(vis)->timings;
  idx = 0;
  vmax = 0;
  while (timings) {
    
#warning TODO: handle mode.frames
    
    /* Reject timings that are too small */
    if (GA_AUTOCMP(timings->xres, <, ggiGAGetGGIMode(res)->visible.x) ||
	GA_AUTOCMP(timings->xres_virtual, <, ggiGAGetGGIMode(res)->virt.x) ||
	GA_AUTOCMP(timings->yres, <, ggiGAGetGGIMode(res)->visible.y) ||
	GA_AUTOCMP(timings->yres_virtual, <, ggiGAGetGGIMode(res)->virt.y)) 
      goto skip;

    /* Reject timings that are too large */
    if (GA_AUTOCMP(timings->xres, >, ggiGAGetGGIMode(cap)->visible.x) ||
	GA_AUTOCMP(timings->xres_virtual, >, ggiGAGetGGIMode(cap)->virt.x) ||
	GA_AUTOCMP(timings->yres, >, ggiGAGetGGIMode(cap)->visible.y) ||
	GA_AUTOCMP(timings->yres_virtual, >, ggiGAGetGGIMode(cap)->virt.y)) 
      goto skip;
    
    /* Reject graphmodes if textmode needed */
    if ((GT_SCHEME(mode->graphtype) == GT_TEXT) && 
	(timings->bits_per_pixel == 0)) goto skip;
    if ((GT_SCHEME(ggiGAGetGGIMode(res)->graphtype) == GT_TEXT) && 
	(timings->bits_per_pixel == 0)) goto skip;
    
    /* Reject incompatible bit depths */
    if (!(ggiGAGetGGIMode(res)->graphtype == GT_AUTO) &&
	timings->bits_per_pixel < GT_DEPTH(ggiGAGetGGIMode(res)->graphtype))
      goto skip;
    
    if (!(ggiGAGetGGIMode(cap)->graphtype == GT_AUTO) &&
	timings->bits_per_pixel > GT_DEPTH(ggiGAGetGGIMode(cap)->graphtype))
      goto skip;
    
    DPRINT("Accepted timing %ix%i %ix%i %x\n", 
		 timings->xres, timings->yres, 
		 timings->xres_virtual, timings->yres_virtual, 
		 timings->bits_per_pixel);
    
    tlist[idx] = *timings;
    vlist[idx] = abs(timings->xres) * abs(timings->yres) *
      abs((timings->bits_per_pixel == 0) ? timings->bits_per_pixel : 1);
    if (vlist[idx] > vmax) vmax = vlist[idx];
    idx++;
  skip:
    timings = timings->next;
  }
  
  num_timings = idx + 1;
  rc = GALLOC_EFAILED;
  while (1) {
    unsigned long vlower;
    vlower = 0;
    for (idx = 0; idx < num_timings; idx++) {
      if ((vlist[idx] > vlower) && (vlist[idx] < vmax)) vlower = vlist[idx]; 
      if (vlist[idx] != vmax) continue;
      mode->visible.x = (tlist + idx)->xres;
      mode->visible.y = (tlist + idx)->yres;
      mode->virt.x    = (tlist + idx)->xres_virtual;
      mode->virt.y    = (tlist + idx)->yres_virtual;
      if ((tlist + idx)->bits_per_pixel == 0)
	mode->graphtype = GT_TEXT16;
      else if ((tlist + idx)->bits_per_pixel <= 16)
	mode->graphtype = 
	  GT_CONSTRUCT((tlist + idx)->bits_per_pixel, GT_PALETTE,
		       (tlist + idx)->bits_per_pixel);
      else 
	mode->graphtype = 
	  GT_CONSTRUCT((tlist + idx)->bits_per_pixel, GT_TRUECOLOR,
		       (tlist + idx)->bits_per_pixel);
      if (ggiCheckMode(vis, mode) == GGI_OK) {
	rc = GALLOC_OK;
	goto done;
      }
    }
    if (vlower == vmax) goto done;
    vmax = vlower;
  }
 done:
  free(vlist);
  free(tlist);
  return(rc);
}

static int adjust_tcurs_props(ggiGA_resource_handle lastmode,
			      ggiGA_resource_handle lasttcurs,
			      ggiGA_resource_handle origtcurs) {
  /* lasttcurs is the resource as it has been negotiated. */
  /* origtcurs is the request without any GGI_AUTOs adjusted. */
  int rc = GALLOC_OK;
  ggi_mode *mode;
  
  mode = ggiGAGetGGIMode(lastmode);
  /* Note: GT_TEXT is assumed at this point. */
  
  memcpy(lasttcurs->props, origtcurs->props, sizeof(*(lasttcurs->props)));
  
  GA_AUTOLIMIT (lasttcurs, props->qty, !=, 1);
  
  /* No sub-dot accuracy */
  GA_AUTOLIMIT(lasttcurs, props->sub.motor.div_min.x, !=, 1);
  GA_AUTOLIMIT(lasttcurs, props->sub.motor.div_min.y, !=, 1);
  GA_AUTOLIMIT(lasttcurs, props->sub.motor.div_max.x, !=, 1);
  GA_AUTOLIMIT(lasttcurs, props->sub.motor.div_max.y, !=, 1);
  
  /* The text cursor is always as wide as the pixel (character cell) */
  GA_AUTOLIMIT(lasttcurs, props->size.area.x, !=, mode->dpp.x);
  GA_AUTOLIMIT(lasttcurs, props->sub.motor.mul_min.x, !=, mode->dpp.x);
  GA_AUTOLIMIT(lasttcurs, props->sub.motor.mul_max.x, !=, mode->dpp.x);
  GA_AUTOLIMIT(lasttcurs, props->sub.motor.pos_snap.x, !=, mode->dpp.x);
  GA_AUTOLIMIT(lasttcurs, props->sub.motor.size_snap.x, !=, mode->dpp.x);
  GA_LIMIT(lasttcurs, props->sub.motor.grid_start.x, <, 0);
  GA_LIMIT(lasttcurs, props->sub.motor.grid_start.x, >, 
	   (mode->visible.x * mode->dpp.x));
  GA_AUTOLIMIT(lasttcurs, props->sub.motor.grid_size.x, >,
	       (mode->visible.x * mode->dpp.x));
  
  /* fbdev interface to hardware cursor is not well documented, except
     for the VC escape sequence stuff which we cannot use.  For now,
     we will limit ourselves to using a cursor which is the full character
     cell height.  linux-console target will be a bit better. */
  GA_AUTOLIMIT(lasttcurs, props->size.area.y, !=, mode->dpp.y);
  GA_AUTOLIMIT(lasttcurs, props->sub.motor.mul_min.y, !=, mode->dpp.y);
  GA_AUTOLIMIT(lasttcurs, props->sub.motor.mul_max.y, !=, mode->dpp.y);
  GA_AUTOLIMIT(lasttcurs, props->sub.motor.pos_snap.y, !=, mode->dpp.y);
  GA_AUTOLIMIT(lasttcurs, props->sub.motor.size_snap.y, !=, mode->dpp.y);
  GA_LIMIT(lasttcurs, props->sub.motor.grid_start.y, <, 0);
  GA_LIMIT(lasttcurs, props->sub.motor.grid_start.y, >, 
	   (mode->visible.y * mode->dpp.y));
  GA_AUTOLIMIT (lasttcurs, props->sub.motor.grid_size.y, >,
		(mode->visible.y * mode->dpp.y));
  
  return(rc);
  
}

static int reconcile_features(ggi_visual_t vis,
			      ggiGA_resource_list reqlist,
			      ggiGA_resource_list orig_reqlist,
			      ggiGA_resource_handle current, 
			      ggiGA_resource_handle lastmode, 
			      ggiGA_resource_handle lasttcurs,
			      ggiGA_resource_handle lastsplit, 
			      ggiGA_resource_handle lastraypos, 
			      ggiGA_resource_handle lastraytrig_pal, 
			      ggiGA_resource_handle lastraytrig_set) {
  
  struct ggiGA_mode *gamode, newmode;
  ggi_fbdev_priv *priv;
  int rc;
  
  priv = FBDEV_PRIV(vis);
  gamode = NULL;
  if (lastmode != NULL) gamode = (struct ggiGA_mode *)(lastmode->priv);
  
  rc = GALLOC_OK;
  
  if (current == lasttcurs) {
    /* Text cursor was (re)negotiated. */
    
    if (gamode != NULL) {
      /* Make sure that the mode is a textmode. */
      if (GT_SCHEME(gamode->mode.graphtype) != GT_TEXT) 
	return GALLOC_EFAILED;
      /* Ensure the new props match the current mode. */
      rc = adjust_tcurs_props(lastmode, lasttcurs, lasttcurs);
    }
    /* text cursor does not rely/affect anything else. */
    return (rc);
  }
  
  if (current == lastsplit) {
    /* Splitline was (re)negotiated. */
    
    GA_AUTOLIMIT (current, props->sub.motor.mul_min.x, !=, 1);
    GA_AUTOLIMIT (current, props->sub.motor.mul_min.y, !=, 1);
    GA_AUTOLIMIT (current, props->sub.motor.mul_max.x, !=, 1);
    GA_AUTOLIMIT (current, props->sub.motor.mul_max.y, !=, 1);
    GA_AUTOLIMIT (current, props->sub.motor.div_min.x, !=, 1);
    GA_AUTOLIMIT (current, props->sub.motor.div_min.y, !=, 1);
    GA_AUTOLIMIT (current, props->sub.motor.div_max.x, !=, 1);
    GA_AUTOLIMIT (current, props->sub.motor.div_max.y, !=, 1);
    
    if (gamode != NULL) {
      /* We silently ignore x values here. */
      GA_LIMIT (current, props->sub.motor.grid_start.y, <, 0);
      GA_LIMIT (current, 
		props->sub.motor.grid_start.y, >, 
		gamode->mode.visible.y);
      GA_AUTOLIMIT (current,
		    props->sub.motor.grid_size.y, >,
		    gamode->mode.visible.y);
    }
    /* In fbdev API ywrap is considered to be part of the "fixed
       screen info", which there is no way to check without setting
       the mode first (FB_ACTIVATE_TEST only applies to "var  
       screen info").  So we just hope that it is truely mode
       independent, and look at the values we got when the fbdev 
       target initialized. */
    
    /* This case should have been taken care of in init.c */
    LIB_ASSERT(priv->orig_fix.ywrapstep != 0);
    
    GA_AUTOLIMIT (current, 
		  props->sub.motor.pos_snap.y, <, 
		  priv->orig_fix.ywrapstep);
  }
  
  if (current == lastraypos) {
    /* raypos feature was (re)negotiated. */
    /* fbdev FBIOGET_VBLANK interface is not well defined.
       We assume for non-target-specific case that 
       units will be in dots. */
    GA_AUTOLIMIT(current, props->sub.motor.mul_min.x, !=, 1);
    GA_AUTOLIMIT(current, props->sub.motor.mul_min.y, !=, 1);
    GA_AUTOLIMIT(current, props->sub.motor.mul_max.x, !=, 1);
    GA_AUTOLIMIT(current, props->sub.motor.mul_max.y, !=, 1);
    GA_AUTOLIMIT(current, props->sub.motor.div_min.x, !=, 1);
    GA_AUTOLIMIT(current, props->sub.motor.div_min.y, !=, 1);
    GA_AUTOLIMIT(current, props->sub.motor.div_max.x, !=, 1);
    GA_AUTOLIMIT(current, props->sub.motor.div_max.y, !=, 1);
  }
  
  if (current == lastmode) {
    /* Prepare us some scribble space for suggestions. */
    memcpy(&newmode, gamode, sizeof(newmode));
    
    /* Check if a text cursor has been promised, if so force textmode. */
    if (lasttcurs != NULL) newmode.mode.graphtype = GT_TEXT;
    
    /* Check if there is a cap on this mode. */
    if (current->next && (current->next->res_state & GA_STATE_CAP))
      rc = findbestmode(vis, current, current->next, &(newmode.mode));
    else rc = findbestmode(vis, current, current, &(newmode.mode));
    
    if (rc != GALLOC_OK) return (rc); /* Failed to find a compatible mode */

    /* Adjust the text cursor props to the new mode. */
    if (lasttcurs != NULL)
      rc = adjust_tcurs_props(lastmode, lasttcurs, 
			      ggiGAHandle(orig_reqlist, reqlist, lasttcurs));
    if (rc != GALLOC_OK) return (rc); /* Mode incompatible with tcurs. */
    
    /* Put the suggested mode into current, unless asked not to. */
    if (!(current->res_state & GA_STATE_NOCHANGE)) 
      memcpy(ggiGAGetGGIMode(current), &newmode, sizeof(struct ggiGA_mode));
    
    /* Flag as successful resource(s) */
    current->res_state &= ~GA_STATE_FAILED;
    if (current->next && (current->next->res_state & GA_STATE_CAP))
      current->next->res_state &= ~GA_STATE_FAILED;
  }
  
  return(rc);
}


/*-* API Implementation */


int GALLOC_Fbdev_Check(ggi_visual *vis, ggiGA_resource_list request, 
		       ggiGA_resource_list *result) 
{
  ggiGA_resource_list reqlist_tmp = NULL, reslist_tmp = NULL;
  ggiGA_resource_handle current = NULL, compound = NULL;
  ggiGA_resource_handle lastmode = NULL, lasttcurs = NULL, lastsplit = NULL,
    lastraypos = NULL, lastraytrig_pal = NULL, lastraytrig_set = NULL;
  
  struct fbdevgalloc_priv *priv;
  struct galloc_fbdev_features newfeat;
  int rc = GALLOC_OK, rc_stat = GALLOC_OK;
  
  priv = FBDEVGALLOC_PRIV(vis);
  
  if (request == NULL) {
    
    /* ggiGAGet -- begin */
    
    if (result == NULL) return GGI_EARGINVAL;
    
    if (GA_RESLIST(vis) == NULL) {
      /* Visual's mode either unset or ggiSetMode used */
      ggi_mode mode;
      
      /* This is where the mode resource magically appears */
      rc = ggiGetMode(vis, &mode);
      LIB_ASSERT(rc == GALLOC_OK);
      
      rc = ggiGAAddMode(vis, &(GA_RESLIST(vis))),
			&mode, NULL, &current);
      LIB_ASSERT(rc == GALLOC_OK);
      if (rc != GALLOC_OK) rc_stat = rc;
    }       /* if */
    rc = ggiGACopyList(GA_RESLIST(vis), result);
    
    if (rc != GALLOC_OK) rc_stat = rc;
    LIB_ASSERT(rc == GALLOC_OK);
    goto finish;
    
    /* ggiGAGet -- end */
    
  }       /* if */
  
  /* Work with a copy */
  rc = ggiGACopyList(request, &reqlist_tmp);
  LIB_ASSERT(rc == GALLOC_OK);
  
  lastmode = ggiGAFindLastMode(reqlist_tmp, NULL);
  /* Lists without any GA_RT_FRAMEs are considered too vague. */
  LIB_ASSERT(lastmode != NULL);
  if (lastmode == NULL) {
    ggiGADestroyList(&reqlist_tmp);
    return GALLOC_ESPECIFY;
  }       /* if */
  
  DPRINT("Looks like a good reqlist, proceeding with check\n");
  
  /* We copy a "blank slate" from the "available" resources. */
  memcpy(&newfeat, &(priv->avail), sizeof(priv->avail));
  
  current = reqlist_tmp;
  lastmode = NULL;
  while (current != NULL) {
    
    /* Any failures mean we skip checking the rest of the list,
       but we still must copy the rest of it to the results. */
    if (rc_stat != GALLOC_OK) goto skip;

    if (ggiGAIsCap(current)) {
      /* Resource caps will have been lookahead processed.
       * Append them to the result and continue. */
      LIB_ASSERT(current != reqlist_tmp);
      goto skip;
    }       /* if */

    /* Now that caps are eliminated -- find we a new compound? */
    if (ggiGAIsCompoundHead(current)) compound = current;
    
    /* It would be invalid for a list to begin with a seeabove. */
    LIB_ASSERT(compound != NULL);
    
    current->res_state &= ~GA_STATE_FAILED;
    rc = GALLOC_OK;
    
    /* Basic sanity check. */
    switch(current->res_type) {
      /* Basic sanity for motor-only resources. */
    case GA_RT_SPRITE_CURSOR:         /* hw textmode cursor is weird. */
    case GA_RT_SPRITE_DONTCARE:       /* as it has no motor. */
    case GA_RT_MISC_SPLITLINE:
    case GA_RT_MISC_RAYPOS:
    case GA_RT_MISC_RAYTRIG_PAL:      /* May have a tank someday, not now. */
    case GA_RT_MISC_RAYTRIG_SET:
      rc = ggiGAMotorCapSanity(current);
      break;
      /* Basic sainity check for GA_RT_FRAMEs */
    case GA_RT_FRAME:
      rc = ggiGAModeCapSanity(current);
      break;
    default:
      rc = GALLOC_EUNAVAILABLE;
      break;
    } /* switch */
    
    if (rc != GALLOC_OK) {
      ggiGAFlagFailed(current);
      rc_stat = rc;
      goto skip;
    }
    
    /* Check for prior tag group activity. */
    if (!(current->res_state & GA_STATE_TAG_MASK) ||
	(ggiGAFindPrevTag(reqlist_tmp, current) == NULL)) {

      /* GA_RT_FRAMES should not have tags. */
      LIB_ASSERT(!((current->res_state & GA_STATE_TAG_MASK) && 
		   (ggiGA_TYPE(current) == GA_RT_FRAME)));
      
      /* None.  So check availability of requested feature. */
      
      switch(current->res_type) {
      case GA_RT_SPRITE_CURSOR:
      case GA_RT_SPRITE_DONTCARE:
	if (newfeat.tcursor_count <= 0) rc = GALLOC_EUNAVAILABLE;
	else newfeat.tcursor_count--;
	break;
      case GA_RT_MISC_SPLITLINE:
	if (current->res_type & GA_RT_MOTOR) rc = GALLOC_EUNAVAILABLE;
	else newfeat.splitline_count--;
	break;
      case GA_RT_MISC_RAYPOS:
	if (newfeat.raypos_flag < 0) rc = GALLOC_EUNAVAILABLE;
	break;
      case GA_RT_MISC_RAYTRIG_PAL:
	if (newfeat.raytrig_cmap_count < 0) rc = GALLOC_EUNAVAILABLE;
	else newfeat.raytrig_cmap_count--;
	break;
      case GA_RT_MISC_RAYTRIG_SET:
	if (newfeat.raytrig_set_count < 0) rc = GALLOC_EUNAVAILABLE;
	else newfeat.raytrig_set_count--;
	break;
      case GA_RT_FRAME:
	break;
      default:
	DPRINT("res_type shouldn't == %x here.\n", current->res_type); 
	rc = GALLOC_EFAILED;
	break;
      }; /* switch */
    } else {  /* This resource is tagged to something prior. */
      
      /* Some list structure sanity checks. */
      LIB_ASSERT(ggiGAFindCompoundByTag(reqlist_tmp,current) != NULL);      
      LIB_ASSERT(ggiGA_TYPE(current) != GA_RT_FRAME);
    }

    /* Did basic availability check fail? */
    if (rc != GALLOC_OK) {
      ggiGAFlagFailed(current);
      rc_stat = rc;
      goto skip;
    }
    
    /* Regular check. */
    switch(current->res_type) {
      
    case GA_RT_FRAME:
      lastmode = current;
      break;
    case GA_RT_SPRITE_CURSOR:
    case GA_RT_SPRITE_DONTCARE:
      lasttcurs = current;
      break;
    case GA_RT_MISC_SPLITLINE:
      lastsplit = current;
      break;
    case GA_RT_MISC_RAYPOS:
      lastraypos = current;
      break;
    case GA_RT_MISC_RAYTRIG_PAL:
      lastraytrig_pal = current;
      break;
    case GA_RT_MISC_RAYTRIG_SET:
      lastraytrig_set = current;
      break;
    default:
      DPRINT("res_type shouldn't == %x here.\n", current->res_type); 
      break;
    } /* switch */
    
    rc = reconcile_features(vis, request, reqlist_tmp, current, 
			    lastmode, lasttcurs, lastsplit, 
			    lastraypos, lastraytrig_pal, lastraytrig_set);
    
    /* Did basic availability check fail? */
    if (rc != GALLOC_OK) {
      ggiGAFlagFailed(current);
      rc_stat = rc;
      goto skip;
    }
    
  skip:
    
    rc = ggiGAAppend(current, &reslist_tmp);
    LIB_ASSERT(rc == GALLOC_OK);
    
    current = current->next;
    
  } /* while */
  
  
  /* The rest of the processing is only needed if result is desired. */
  if (result != NULL) {
    if (request == result[0]) {
      rc = ggiGADestroyList(result);
      LIB_ASSERT(rc == GALLOC_OK);
    }       /* if */
    rc = ggiGACopyList(reslist_tmp, result);
    LIB_ASSERT(rc == GALLOC_OK);
  } else {    
    rc = ggiGADestroyList(&reslist_tmp);
    LIB_ASSERT(rc == GALLOC_OK);
  }       /* if */
  
 finish:
  rc = rc_stat;
  return rc;
  
}	/* GALLOC_Fbdev_Check */


int GALLOC_Fbdev_Set(ggi_visual *vis, ggiGA_resource_list request, 
		     ggiGA_resource_list *result) 
{
  ggiGA_resource_list reqlist_tmp = NULL, reslist_tmp = NULL;
  
  ggiGA_resource_handle current = NULL, compound = NULL, lastmode = NULL;
  
  struct galloc_fbdev_features newfeat;
  int rc = GALLOC_OK, rc_stat = GALLOC_OK;
  
  LIB_ASSERT(request != NULL);
  if (request == NULL) {
    /* no resources specified */
    return GALLOC_ESPECIFY;
  }       /* if */
  
  /* Making sure that the mode checks makes this function _much_ simpler;
     Check is the only truly hairy function. */
  rc = ggiGACheck(vis, request, &reqlist_tmp);
  LIB_ASSERT(rc == GALLOC_OK);
  if (rc != GALLOC_OK) {
    /* User didn't bother to ggiGACheck().
     *  Behave like ggiGACheck(). */
    if (result != NULL) {
      if (request == *result) ggiGADestroyList(result);
      *result = reqlist_tmp;
    }
    else ggiGADestroyList(&reqlist_tmp);
    return(rc);
  }	/* if */
  
  /* All resources in tmp have succeeded.  All GGI_AUTOs have been
     replaced with modified values, and we only have to pay 
     attention to the last framebuffer resource (possibly a
     compound resource) in the list. */
  lastmode = ggiGAFindLastMode(reqlist_tmp, NULL);
  LIB_ASSERT(lastmode != NULL);
  
  /* We are fortunate enough to be able to use the regular ggiSetMode,
     because this target is simple enough not to need it's own code
     to do that.  We can even do that now without interfering with
     the allocation of other features, which helps, because it fills
     out the FBDEV_PRIV area, which we need information from. */
  LIB_ASSERT(ggiCheckMode(vis, ggiGAGetGGIMode(lastmode)) == GGI_OK);
  if (!(lastmode->res_state & GA_STATE_NORESET)) 
    rc = ggiSetMode(vis, ggiGAGetGGIMode(lastmode)); 
  LIB_ASSERT(rc == GGI_OK);
  
  /* We copy a "blank slate" from the "available" resources. */
  memcpy(&newfeat, &(FBDEVGALLOC_PRIV(vis)->avail), 
	 sizeof(struct galloc_fbdev_features));
  
  /* Now we traverse the list.  Since the mode has been checked,
     everything is all set up already.  fbdev really doesn't have
     anything besides the SetMode that needs pre-setup before
     the extension initializes it.  This loop could just as
     easily not be here, but it is here for the purpose
     of sanity checking. */
  current = reqlist_tmp;
  while (current != NULL) {
    /* Mark resource as active and unchangable. */
    current->res_state |= GA_STATE_NORESET | GA_STATE_NOCHANGE;
    switch(current->res_type) {
    case GA_RT_SPRITE_CURSOR:
      newfeat.tcursor_count--;
      LIB_ASSERT(GT_SCHEME(LIBGGI_GT(vis)) == GT_TEXT);
      LIB_ASSERT(newfeat.tcursor_count >= 0);
      break;
    case GA_RT_MISC_RAYPOS:
      LIB_ASSERT(newfeat.raypos_flag);
      break;
    case GA_RT_MISC_RAYTRIG_SET:
      newfeat.raytrig_set_count--;
      LIB_ASSERT(newfeat.raytrig_set_count >= 0);
      break;
    case GA_RT_MISC_RAYTRIG_PAL:
      newfeat.raytrig_cmap_count--;
      LIB_ASSERT(newfeat.raytrig_cmap_count >= 0);
      break;
    case GA_RT_MISC_SPLITLINE:
      newfeat.splitline_count--;
      LIB_ASSERT(newfeat.splitline_count >= 0);
      break;
    case GA_RT_BUFFER:
      LIB_ASSERT(current->props);
      /* Here we really have to have a 1D range allocator/deallocator, for now 
         we just do something for the heck of it. */;
      break;
    case GA_RT_FRAME:
      /* We did this already */
      break;
    default:
      GGIDPRINT("Unsupported resource penetrated ggiGASet\n");
      LIB_ASSERT(0);
      break;
    }
    current = current->next;
  }

  ggiGADestroyList(&(GA_RESLIST(vis)));
  GA_RESLIST(vis) = reqlist_tmp;
  if (result != NULL) {
    printf( "result before empty = %p\n", *result);
    ggiGADestroyList(result);
    printf( "result after empty = %p\n", *result);
    ggiGACopyList(reqlist_tmp, result);
    printf( "result after copy = %p\n", *result);
  }
  memcpy(&(FBDEVGALLOC_PRIV(vis)->curr),
	 &newfeat, sizeof(struct galloc_fbdev_features));
  return rc;
}	/* GALLOC_Fbdev_Set */



int GALLOC_Fbdev_Release(ggi_visual_t vis, ggiGA_resource_list *list,
			 ggiGA_resource_handle *handle)
{
	ggiGA_resource_list reslist = NULL;
	ggiGA_resource_list reqlist = NULL;
	ggiGA_resource_handle reqcurrent = NULL, reqlast = NULL;
	ggiGA_resource_handle rescurrent = NULL, reslast = NULL;
	ggiGA_resource_handle lastmode = NULL, req = NULL;
	struct galloc_fbdev_features newfeat;
	int rc = GALLOC_OK;
	struct fbdevgalloc_priv *priv;

	priv = FBDEVGALLOC_PRIV(vis);

	memcpy(&newfeat, &(priv->curr), sizeof(struct galloc_fbdev_features));

	if (list == NULL) return GGI_EARGINVAL;
	if (handle == NULL) return GGI_EARGINVAL;
	if (*handle == NULL) return GALLOC_ESPECIFY;
	
	rc = ggiGAGet(vis, &reslist);
	LIB_ASSERT(rc == GALLOC_OK);

	DPRINT("reslist = %p\n", reslist);

	/* find the last mode, such that we can refuse to free it. */
	lastmode = ggiGAFindLastMode(reslist, NULL);
	LIB_ASSERT(lastmode != NULL);

	DPRINT("lastmode = %p\n", lastmode);

	reqlist = list[0];
	req = handle[0];
	rc = ggiGAFind(reqlist, req);
	if (rc != GALLOC_OK) {
		/* not in this list! */
		return GALLOC_EUNAVAILABLE;
	}	/* if */

	reqcurrent = reqlist;
	rescurrent = reslist;
	reslast = reqlast = NULL;
	while (reqcurrent != NULL) {
		/* Note: soon this needs to support compound resources. */
	        if (rescurrent == NULL) {
			/* That happens, when releasing a
			 * failed resource...
			 */
			rc = GALLOC_EFAILED;
			goto cleanup;
		}	/* if */

		if (reqcurrent != req) {
			reqlast = reqcurrent;
			reslast = rescurrent;
			reqcurrent = reqcurrent->next;
			rescurrent = rescurrent->next;
		} else break; /* if */
	} /* while */

	/* We silently ignore requests to free lastmode */ 
	if (rescurrent == lastmode) goto cleanup;

	/* Decrease usage-counter */
	rescurrent->res_count--;
	LIB_ASSERT(rescurrent->res_count >= 0);
	if (rescurrent->res_count < 0) {
		/* Never allocated...?? */
		rescurrent->res_count = 0;
	}	/* if */

	/* Don't release if something is still using this. */
	if (rescurrent->res_count) goto install;

	switch(rescurrent->res_type) {
	case GA_RT_SPRITE_CURSOR:
		newfeat.tcursor_count++;
		LIB_ASSERT(newfeat.tcursor_count <= priv->avail.tcursor_count);
		break;
	case GA_RT_MISC_RAYPOS:
		LIB_ASSERT(newfeat.raypos_flag);
		LIB_ASSERT(priv->avail.raypos_flag);
		break;
	case GA_RT_MISC_RAYTRIG_SET:
		newfeat.raytrig_set_count++;
		LIB_ASSERT(newfeat.raytrig_set_count <= 
			   priv->avail.raytrig_set_count);
		break;
	case GA_RT_MISC_RAYTRIG_PAL:
		newfeat.raytrig_cmap_count++;
		LIB_ASSERT(newfeat.raytrig_cmap_count <= 
			   priv->avail.raytrig_cmap_count);
		break;
	case GA_RT_MISC_SPLITLINE:
        	newfeat.splitline_count++;
		LIB_ASSERT(newfeat.splitline_count <= 
			   priv->avail.splitline_count);
		break;
	case GA_RT_BUFFER:
		LIB_ASSERT(rescurrent->props);
		/* Here we have to keep a 1D range allocator/deallocator */;
		break;
	case GA_RT_FRAME:
		/* We just delete these. */
		break;      
	default:
		GGIDPRINT("Unsupported resource penetrated ggiGARelease\n");
		LIB_ASSERT(0);
		break;
	} /* switch */
	
	if ((reslast == NULL) || (reqlast == NULL) ) {
	  LIB_ASSERT((reslast == NULL) && (reqlast == NULL));
	  /* This is the list head.  */
	  reqlist = reqlist->next; 
	  reslist = reslist->next;
	} else {
	  reqlast->next = reqcurrent->next;
	  reslast->next = rescurrent->next;
	}	/* if */
	reqcurrent->next = NULL; ggiGADestroyList(&reqcurrent);
	rescurrent->next = NULL; ggiGADestroyList(&rescurrent);
	list[0] = reqlist;

 install:
	memcpy(&(priv->curr), &newfeat, sizeof(struct galloc_fbdev_features));
	rc = ggiGADestroyList(&(GA_RESLIST(vis)));
	LIB_ASSERT(rc == GALLOC_OK);
	
	rc = ggiGACopyList(reslist, &(GA_RESLIST(vis)));
	LIB_ASSERT(rc == GALLOC_OK);

	return rc;

 cleanup:
	ggiGADestroyList(&reslist);
	return rc;

}	/* GALLOC_Fbdev_Release */





int GALLOC_Fbdev__Mode(ggi_visual *vis, ggiGA_resource_handle *out) {

	printf("Function GALLOC_Fbdev__Mode called.\n");

	LIB_ASSERT(out != NULL); /* People who call this should know better. */
	if (out[0] == NULL) {      /* Create new resource and make GGI_AUTO */
	  ggi_mode *tmp;
	  out[0] = calloc(1, sizeof(struct ggiGA_resource));
	  if(out[0] == NULL) goto err0;
	  out[0]->priv = calloc(1, sizeof(struct ggiGA_mode));
	  if(out[0]->priv == NULL) goto err1;
	  out[0]->priv_size = sizeof(struct ggiGA_mode);
	  tmp = &(((struct ggiGA_mode *)(out[0]->priv))->mode);
	  tmp->frames    = GGI_AUTO;
	  tmp->visible.x = GGI_AUTO;
	  tmp->visible.y = GGI_AUTO;
	  tmp->virt.x    = tmp->virt.y = GGI_AUTO;
	  tmp->size.x    = tmp->size.y = GGI_AUTO;
	  tmp->graphtype = GGI_AUTO;
	  tmp->dpp.x     = tmp->dpp.y = GGI_AUTO;
	}
	out[0]->res_type = GA_RT_FRAME;
	/* Set up db. */
	return(GALLOC_OK);
 err1:
	free(out[0]);
 err0:
	return(GALLOC_EUNAVAILABLE);
}	/* Galloc_Fbdev__Mode */


int GALLOC_Fbdev_CheckIfShareable(ggi_visual_t vis,
                            ggiGA_resource_handle reshandle,
                            ggiGA_resource_handle tocompare)
{
	return (GALLOC_EUNAVAILABLE);
}	/* GALLOC_Fbdev_CheckIfShareable */
