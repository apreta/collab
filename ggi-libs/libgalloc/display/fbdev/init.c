/* $Id: init.c,v 1.7 2006/02/04 22:15:09 soyt Exp $
******************************************************************************

   LibGAlloc implementation for "fbdev" target -- Initialization.

   
  
   Copyright (c) Mon Mar  5 2001 by: 
	Brian S. Julin		bri@calyx.com

  
   
This software may be redistributed under the terms of the GNU Public
License -- see the file COPYING in the parent directory of the 
source code.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.




******************************************************************************
*/

#warning Chris -- dont bother reformatting this code yet, it will change fast.

#include "fbdevgalloc.h"

static int GGIopen(ggi_visual *vis, struct ggi_dlhandle *dlh,
		   const char *args, void *argptr, uint32_t *dlret)
{
#ifdef FBIOGET_VBLANK
  struct fb_vblank vblank;
#endif
  struct fbdevgalloc_priv *priv;
  
  GGIDPRINT("LibGAlloc: GGIopen(%p, %p, %s, %p, %p)" 
	    " called for fbdev_GAlloc sublib\n",
	    vis, dlh, args ? args : "(NULL)", argptr, dlret);

  /*-* Initialize target-private data structure */

  priv = calloc(1, sizeof(struct fbdevgalloc_priv));
  if (priv == NULL) return GGI_ENOMEM;
  FBDEVGALLOC_PRIV(vis) = priv;

  /*-* Hook in target functions */

  LIBGGI_GALLOCEXT(vis)->set = GALLOC_Fbdev_Set;
  LIBGGI_GALLOCEXT(vis)->release = GALLOC_Fbdev_Release;
#if 0
  LIBGGI_GALLOCEXT(vis)->anprintf = GALLOC_Fbdev_anprintf;
#endif
  LIBGGI_GALLOCEXT(vis)->check = GALLOC_Fbdev_Check;
  LIBGGI_GALLOCEXT(vis)->_mode = GALLOC_Fbdev__Mode;
  LIBGGI_GALLOCEXT(vis)->checkifshareable = GALLOC_Fbdev_CheckIfShareable;
  
  /*-* Apply initial values */
  
  /* Figure out if there is splitline support -- this one
     is easy.  Core fbdev target has already read for us
     the "fixed" info, and hopefully (?) it also stores/restores
     the value contained before the fb was open. */
  priv->avail.splitline_count = (FBDEV_PRIV(vis)->orig_fix.ypanstep) ? 1 : -1;

  /* Get what info we can from the fbdev about the cursor. */
  /* We *should* only have to do like above but fbdev target
     has not caught up to fbdev yet. */
  priv->avail.tcursor_count = 
    ioctl(LIBGGI_FD(vis), FBIOGET_FCURSORINFO, &(priv->tcursor_fix)) ? -1 : 1;
  ioctl(LIBGGI_FD(vis), FBIOGET_VCURSORINFO, &(priv->tcursor_origvar));
  ioctl(LIBGGI_FD(vis), FBIOGET_CURSORSTATE, &(priv->tcursor_origstate));

#ifdef FBIOGET_VBLANK
  /* This one it is right for us to be handling: */
  priv->avail.raypos_flag = 
    ioctl(LIBGGI_FD(vis), FBIOGET_VBLANK, &vblank) ? -1 : 1;
  if ((priv->avail.raypos_flag == 1) &&
      (vblank.flags & 
       (FB_VBLANK_HAVE_VBLANK | FB_VBLANK_HAVE_HBLANK | 
	FB_VBLANK_HAVE_COUNT  | FB_VBLANK_HAVE_VCOUNT |
	FB_VBLANK_HAVE_HCOUNT | FB_VBLANK_HAVE_VSYNC   ))) 
    priv->avail.raypos_flag = 1;
  else
#endif
    priv->avail.raypos_flag = 0;      /* Not there */


  /* Figure out if there is ray trigger functionality */
  /* I don't see any fbcon drivers actually implementing
     the _VBL stuff yet, so I cannot tell how to test for this.
     So we chalk these up as unavailable. */
  priv->avail.raytrig_set_count = -1;
  priv->avail.raytrig_cmap_count = -1;

  
  *dlret = ( GGI_DL_EXTENSION | GGI_DL_OPDISPLAY );
  return 0;
}

static int GGIclose(ggi_visual *vis, struct ggi_dlhandle *dlh)
{
  printf("LibGAlloc: GGIdlclose(%p, %p)"
	 " called for FbdevGAlloc sublib\n", vis, dlh);

  /* Restore the cursor -- should rather be done by core fbdev target */
  ioctl(LIBGGI_FD(vis), FBIOPUT_CURSORSTATE, 
	&(FBDEVGALLOC_PRIV(vis)->tcursor_origstate));
  ioctl(LIBGGI_FD(vis), FBIOPUT_VCURSORINFO,  
	&(FBDEVGALLOC_PRIV(vis)->tcursor_origvar));

  /*-* Free target-private structure */
  free(FBDEVGALLOC_PRIV(vis));

  return 0;
}


EXPORTFUNC
int GALLOCdl_fbdev_galloc(int func, void **funcptr);

int GALLOCdl_fbdev_galloc(int func, void **funcptr)
{
      switch (func) {
      case GGIFUNC_open:
	*funcptr = GGIopen;
	return 0;
      case GGIFUNC_exit:
	*funcptr = NULL;
	return 0;
      case GGIFUNC_close:
	*funcptr = GGIclose;
	return 0;
      default:
	*funcptr = NULL;
      }
      
      return GGI_ENOTFOUND;
}
