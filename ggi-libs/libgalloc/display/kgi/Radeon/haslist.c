/* $Id: haslist.c,v 1.7 2005/10/11 14:50:34 cegger Exp $
******************************************************************************

   LibGAlloc implementation for KGI Radeon target -- 
   				Master resource list and associated callbacks
  
   Copyright (c) 2002 Brian S. Julin		[bri@tull.umassp.edu]
   
   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   The above copyright notice applies to all files in this package, unless 
   explicitly stated otherwise in the file itself or in a file named COPYING 
   in the same directory as the file.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "kgi_Radeon_galloc.h"
#include <string.h>


/******************************** callbacks *********************************/

int kgi_Radeon_galloc_check_mode (ggi_visual_t vis,
				  ggiGA_resource_list reslist,
				  enum ggiGA_callback_command command,
				  ggiGA_resource_handle res,
				  ggiGA_resource_handle head,
				  ggiGA_taghead_t taghead,
				  ggiGA_resource_handle has,
				  struct ggiGA_template_state *state,
				  void *privstate) {
	ggi_mode mode;
	rm_range_list_t *rlist, *newlist;
	rm_range_t fbrange, swrange, *tmprange;
	ggi_kgi_priv *kpriv;
	int rc, idx;
	struct kgi_radeon_galloc_container_t *pstate;

	pstate = privstate;
	kpriv = LIBGGI_PRIVATE(vis);
	rc = 0;

	if (command == GA_CB_PRECHECK) return 0;

	if (command == GA_CB_POSTCHECK ||
	    command == GA_CB_UNCHECK ||
	    command == GA_CB_RECHECK) goto dostorage;

	/* No need to validate an active resource. */
	if (res->res_state & GA_STATE_NORESET) return 0;

	/* If we have a cap, try it first */
	if (!ggiGAIsCap(res->next)) goto nocap;

	memcpy(&mode, ggiGAGetGGIMode(res->next), sizeof(ggi_mode));
	rc = ggiCheckMode(vis, &mode);

        /* Now we try again, using the adjusted values that came 
	 * back from ggiCheckMode. 
	 */
        if (rc != GALLOC_OK) rc = ggiCheckMode(vis, &mode);

	if (rc == GALLOC_OK) {
		ggi_mode *minmode;
		/* The cap succeeded.  But did it get a mode above the 
		 * minimum requirements in res?
		 */
		minmode = ggiGAGetGGIMode(res);
		rc |= (minmode->visible.x > mode.visible.x);
		rc |= (minmode->visible.y > mode.visible.y);
		rc |= (minmode->virt.x > mode.virt.x);
		rc |= (minmode->virt.y > mode.virt.y);
		rc |= (GT_DEPTH(minmode->graphtype) > 
		       GT_DEPTH(mode.graphtype));
		rc |= (GT_SCHEME(minmode->graphtype) != 
		       GT_SCHEME(mode.graphtype));
                return 4;
        }       /* if */

	if (rc == GALLOC_OK) goto done;

 nocap:
	/* cap failed, so let's try the original resource. */
	memcpy(&mode, ggiGAGetGGIMode(res), sizeof(ggi_mode));
        rc = ggiCheckMode(vis, &mode);

	if (rc == GALLOC_OK) {
		ggiGAFlagModified(res);
		memcpy(ggiGAGetGGIMode(res), &mode, sizeof(ggi_mode));
		if (!ggiGAIsCap(res->next)) goto done;
		ggiGAFlagModified(res->next);
		memcpy(ggiGAGetGGIMode(res->next), &mode, sizeof(ggi_mode));
	        goto done;
	}
	
        /* Now we try again, using the adjusted values that came 
	 * back from ggiCheckMode. 
	 */
	rc = ggiCheckMode(vis, &mode);
	if (rc != GALLOC_OK) {
		mode.visible.x = GGI_AUTO;
		mode.visible.y = GGI_AUTO;
		mode.virt.x = GGI_AUTO;
		mode.virt.y = GGI_AUTO;
		mode.graphtype = GT_AUTO;
		rc = ggiCheckMode(vis, &mode);
	}

	if (rc == GALLOC_OK) {
		ggi_mode *minmode, *maxmode;

		/* We got something to succeed -- question is, what? */
		minmode = ggiGAGetGGIMode(res);
		rc |= (minmode->visible.x > mode.visible.x);
		rc |= (minmode->visible.y > mode.visible.y);
		rc |= (minmode->virt.x > mode.virt.x);
		rc |= (minmode->virt.y > mode.virt.y);
		rc |= (GT_DEPTH(minmode->graphtype) > 
		       GT_DEPTH(mode.graphtype));
		rc |= (GT_SCHEME(minmode->graphtype) != 
		       GT_SCHEME(mode.graphtype));

		if (!ggiGAIsCap(res->next)) goto done;
		maxmode = ggiGAGetGGIMode(res->next);
		rc |= (maxmode->visible.x < mode.visible.x);
		rc |= (maxmode->visible.y < mode.visible.y);
		rc |= (maxmode->virt.x < mode.virt.x);
		rc |= (maxmode->virt.y < mode.virt.y);
		rc |= (GT_DEPTH(maxmode->graphtype) <
		       GT_DEPTH(mode.graphtype));
	}

 done:	  
	ggiGAFlagModified(res);
	memcpy(ggiGAGetGGIMode(res), &mode, sizeof(ggi_mode));
	if (!ggiGAIsCap(res->next)) goto dostorage;
	ggiGAFlagModified(res->next);
	memcpy(ggiGAGetGGIMode(res->next), &mode, sizeof(ggi_mode));

 dostorage:

	if (!(res->res_state & GA_STATE_NORESET)) goto dostorage_new;
	/* Make a range to represent the required allocation */
	/* We know kgi uses equally spaced frames, so we can use the 
	 * default single-range function 
	 */
	ggiGA_visdb2rm(vis, &fbrange, kpriv->fb);

	fbrange.attribute.owner = res;

        memset(&swrange, 0, sizeof(swrange));
	swrange.attribute.width = kpriv->swatch_size;
        swrange.attribute.stride = swrange.attribute.width;
        swrange.attribute.height = 1;
        swrange.constraint.stride_align = 1;
        swrange.constraint.start_align = 1;
        swrange.constraint.start_min = (size_t)kpriv->swatch_gp;
	swrange.constraint.start_max =  fbrange.constraint.start_min;
	swrange.attribute.flags = RANGE_START_MIN | RANGE_START_MAX;

	goto dostorage2;

 dostorage_new:

	/* Make a range to represent the required allocation */
	ggiGA_mode2rm(ggiGAGetGGIMode(res), &fbrange, 4096, 32);
	fbrange.attribute.owner = res;
	fbrange.attribute.flags = RANGE_START_MIN | RANGE_START_MAX;

        memset(&swrange, 0, sizeof(swrange));
	/* TODO: this must predict exactly what kgi setmode will do,
	 * but only implementing common case now because that itself needs
	 * a rework.
	 */
	swrange.attribute.width = (kpriv->swatch_size > 0) ?
	  kpriv->swatch_size : (fbrange.attribute.width/2 + GGI_KGI_FONTSIZE);
        swrange.attribute.stride = swrange.attribute.width;
        swrange.attribute.height = 1;
        swrange.constraint.stride_align = 1;
        swrange.constraint.start_align = 1;
        swrange.constraint.start_min =  fbrange.attribute.width;
	swrange.constraint.start_min += fbrange.constraint.stride_align - 1;
	swrange.constraint.start_min /= fbrange.constraint.stride_align;
	swrange.constraint.start_min *= fbrange.constraint.stride_align;
	swrange.constraint.start_min *= fbrange.attribute.height;
	swrange.constraint.start_max =  fbrange.constraint.start_min;
	swrange.attribute.flags = RANGE_START_MIN | RANGE_START_MAX;

 dostorage2:

	/* Look to see if we are growing an existing frame. */
	/* TODO: grow swatch with frame */
	for (idx = 0; idx < pstate->vram->count; idx++) {
		ggiGA_resource_handle allocres;

		tmprange = pstate->vram->range + idx;
		allocres = tmprange->attribute.owner;
		if (ggiGA_FULLTYPE(allocres) != GA_RT_FRAME) continue;

		LIB_ASSERT(tmprange->attribute.start == 0);
		if (tmprange->attribute.width > fbrange.attribute.width)
			fbrange.attribute.width = tmprange->attribute.width;
		fbrange.attribute.owner = allocres;
	}	

	/* Initialize new range list and fill it with all the ranges
	 * so far allocated, except for the framebuffer-owned ones.
	 */
	rm_list_init(&rlist);
	for (idx = 0; idx < pstate->vram->count; idx++) {
		ggiGA_resource_handle allocres;

		tmprange = pstate->vram->range + idx;
		allocres = tmprange->attribute.owner;
		if (ggiGA_FULLTYPE(allocres) == GA_RT_FRAME) continue;
		rm_list_add(rlist, tmprange);
	}
	rm_list_add(rlist, &fbrange);
	if (kpriv->use3d) rm_list_add(rlist, &swrange);
	
	if (command == GA_CB_POSTCHECK) goto postcheck;
	if (command == GA_CB_UNCHECK)	goto uncheck;
	if (command == GA_CB_RECHECK)	goto recheck;

	/* Check storage availability, but do not allocate storage. */
	rm_list_init(&newlist);
	rm_list_fit(newlist, rlist, kpriv->fb_size);
	while (idx > 0) { 
		if (rlist->range[idx - 1].attribute.flags & RANGE_NO_FIT) 
			break;
		idx--;
	}
	if (idx) rc = 1;
	rm_list_exit(&newlist);
	rm_list_exit(&rlist);
	if (rc) rc |= 2;
	return rc;

 postcheck:
 recheck:

	rm_list_init(&newlist);
	rm_list_fit(newlist, rlist, kpriv->fb_size);
	idx = rlist->count;
	while (idx > 0) { 
		if (rlist->range[idx - 1].attribute.flags & RANGE_NO_FIT) 
			break;
		idx--;
	}
	if (idx) {
		rc = 1;
		rm_list_exit(&newlist);
	}
	else {
		rm_list_alloc(newlist, rlist);
		rm_list_exit(&pstate->vram);
		pstate->vram = newlist;
	}
	rm_list_exit(&rlist);
	return rc;
	
 uncheck:
	/* No way to efficiently uncheck storage, must force a full flush. */
	return 3;
}


int kgi_Radeon_galloc_set_mode (ggi_visual_t vis,
				ggiGA_resource_list reslist,
				enum ggiGA_callback_command command,
				ggiGA_resource_handle res,
				ggiGA_resource_handle head,
				ggiGA_taghead_t taghead,
				ggiGA_resource_handle has,
				struct ggiGA_template_state *state,
				void *privstate) {

	if (command == GA_CB_UNSET ||
	    command == GA_CB_RELEASE) {
	  fprintf(stderr, "Implement UNSET/RELEASE!!\n");
	  return -1;
	}

	if (!(res->res_state & GA_STATE_NORESET))
		return ggiSetMode(vis, ggiGAGetGGIMode(res));
	return GALLOC_OK;	/* Already set, nothing to do. */
}	/* kgi_Radeon_galloc_set_mode */


/* Check hardware Z buffer resource.
 */
int kgi_Radeon_galloc_check_hwz (ggi_visual_t vis,
				 ggiGA_resource_list reslist,
				 enum ggiGA_callback_command command,
				 ggiGA_resource_handle res,
				 ggiGA_resource_handle head,
				 ggiGA_taghead_t taghead,
				 ggiGA_resource_handle has,
				 struct ggiGA_template_state *state,
				 void *privstate) {

        struct ggiGA_resource_props *res_p;
	ggi_mode *mode;
	rm_range_list_t *rlist, *newlist;
	rm_range_t hwzrange, *tmprange;
	ggi_kgi_priv *kpriv;
	int rc, idx;
	struct kgi_radeon_galloc_container_t *pstate;

	DPRINT("kgi_radeon_galloc_check_hwz reslist callback called\n");

	pstate = privstate;
	kpriv = LIBGGI_PRIVATE(vis);
	rc = 0;

	LIB_ASSERT(ggiGA_TYPE(head) == GA_RT_FRAME);
	LIB_ASSERT(head->priv != NULL);

	if (res == head) {
		fprintf(stderr, "No support for floaters yet.\n");
		return 4;
	}

        res_p = res->props;
        mode = ggiGAGetGGIMode(head);

	/* PRECHECK is handled by default callback. */

	if (command == GA_CB_POSTCHECK ||
	    command == GA_CB_UNCHECK ||
	    command == GA_CB_RECHECK) goto dostorage;

	/* No need to validate an active resource. */
	if (res->res_state & GA_STATE_NORESET) return 0;

 dostorage:

	/* Note much of the below should be encapsulated by mmutil range 
	 * manager and LibGAlloc convenience functions.
	 */
	/* Make a range to represent the required allocation */
	mode = ggiGAGetGGIMode(head);
	memset(&hwzrange, 0, sizeof(hwzrange));
	hwzrange.attribute.width = hwzrange.attribute.stride = mode->frames * 
	  (GT_SIZE(res_p->sub.tank.gt)/8) * mode->virt.x * mode->virt.y;
	hwzrange.attribute.height = 1;
	hwzrange.attribute.owner = res;
	hwzrange.constraint.stride_align = 1;
	hwzrange.constraint.start_align = 16;
	hwzrange.attribute.flags = RANGE_START_ALIGN;

	/* Look to see if we are sharing storage via a tag group */
	for (idx = 0; idx < pstate->vram->count; idx++) {
		ggiGA_resource_handle allocres;

		tmprange = pstate->vram->range + idx;
		allocres = tmprange->attribute.owner;
		if (!ggiGACompareTag(allocres, res)) continue;
		/* Currently no sharing with strided resources. */
		LIB_ASSERT(tmprange->attribute.height == 1);
		if (tmprange->attribute.width > hwzrange.attribute.width)
			hwzrange.attribute.width = tmprange->attribute.width;
		hwzrange.attribute.owner = allocres;
	}

	/* Initialize new range list and fill it with all the ranges
	 * so far allocated, except for members of our tag group.
	 */
	rm_list_init(&rlist);
	for (idx = 0; idx < pstate->vram->count; idx++) {
		ggiGA_resource_handle allocres;

		tmprange = pstate->vram->range + idx;
		allocres = tmprange->attribute.owner;
		if (ggiGACompareTag(allocres, res)) continue;
		rm_list_add(rlist, tmprange);
	}
	rm_list_add(rlist, &hwzrange);
	
	if (command == GA_CB_POSTCHECK) goto postcheck;
	if (command == GA_CB_UNCHECK)	goto uncheck;
	if (command == GA_CB_RECHECK)	goto recheck;

	/* Check storage availability, but do not allocate storage. */
	rm_list_init(&newlist);
	rm_list_fit(newlist, rlist, kpriv->fb_size);
	while (idx > 0) { 
		if (rlist->range[idx - 1].attribute.flags & RANGE_NO_FIT) 
			break;
		idx--;
	}
	if (idx) rc = 1;
	rm_list_exit(&newlist);
	rm_list_exit(&rlist);
	if (rc) rc |= 2;
	return rc;

 postcheck:
 recheck:
	rm_list_init(&newlist);
	rm_list_fit(newlist, rlist, kpriv->fb_size);
	idx = rlist->count;
	while (idx > 0) { 
		if (rlist->range[idx - 1].attribute.flags & RANGE_NO_FIT) 
			break;
		idx--;
	}
	if (idx) {
		rc = 1;
		rm_list_exit(&newlist);
	}
	else {
		rm_list_alloc(newlist, rlist);
		rm_list_exit(&pstate->vram);
		pstate->vram = newlist;
	}
	rm_list_exit(&rlist);
	return rc;
	
 uncheck:
	/* No way to efficiently uncheck storage, must force a full flush. */
	return 3;
}


/* Check for normal RAM side-buffers that match the main mode.
 */
int kgi_Radeon_galloc_check_cladbuf (ggi_visual_t vis,
				     ggiGA_resource_list reslist,
				     enum ggiGA_callback_command command,
				     ggiGA_resource_handle res,
				     ggiGA_resource_handle head,
				     ggiGA_taghead_t taghead,
				     ggiGA_resource_handle has,
				     struct ggiGA_template_state *state,
				     void *privstate) {

        struct ggiGA_resource_props *res_p;
	ggi_mode *mode;

	if (res == head) {
		fprintf(stderr, "No support for floaters yet.\n");
		return 4;
	}

	/* We probably don't even need these asserts, remove later. */
	LIB_ASSERT(ggiGA_TYPE(head) == GA_RT_FRAME);
	LIB_ASSERT(head->priv != NULL);

        mode = ggiGAGetGGIMode(head);
        res_p = res->props;


	switch(command) {
	  /* GA_CB_PRECHECK is handled by default template callback */
	case GA_CB_CHECK:
		/* precheck is sufficient. */
		return 0;
	case GA_CB_POSTCHECK:
	case GA_CB_RECHECK:
	case GA_CB_UNCHECK:
		/* As there is no storage management for RAM yet: */
		return 0;
	default:
		LIB_ASSERT(0 /* This case should not occur */);
		return -1;
	}
}

/* We need to calculate the address of the Z buffer. */
int kgi_Radeon_galloc_set_hwz(ggi_visual_t vis,
			      ggiGA_resource_list reslist,
			      enum ggiGA_callback_command command,
			      ggiGA_resource_handle res,
			      ggiGA_resource_handle head,
			      ggiGA_taghead_t taghead,
			      ggiGA_resource_handle has,
			      struct ggiGA_template_state *state,
			      void *privstate) {
	ggi_mode *mode;
	ggi_kgi_priv *kpriv;
	rm_range_t *tmprange;
	int idx;
	size_t hwzaddr;
	struct kgi_radeon_galloc_container_t *pstate;

	kpriv = LIBGGI_PRIVATE(vis);
	pstate = privstate;

        mode = ggiGAGetGGIMode(head);

	switch(command) {
	case GA_CB_UNSET:
		LIB_ASSERT(0 /* unsetting hwz not implemented */);
		return 0;
	case GA_CB_RELEASE:
		LIB_ASSERT(0 /* releasing hwz not implemented yet */);
		return 0;
	case GA_CB_SET:
		tmprange = NULL;
		fprintf(stdout, "migrated list follows:\n");
		rm_list_print(pstate->vram, kpriv->fb_size);
		for (idx = 0; idx < pstate->vram->count; idx++) {
			ggiGA_resource_handle allocres;

			tmprange = pstate->vram->range + idx;
			allocres = tmprange->attribute.owner;
			if (allocres != res) continue;
			goto setaddr;
		}
		LIB_ASSERT(0 /* Foul! There was no allocated storage. */);
		return 4;
	default:
		LIB_ASSERT(0 /* This case should not occur */);
		return 4;
	}

 setaddr:

	hwzaddr = tmprange->attribute.start;
	if (hwzaddr & 16) hwzaddr += 16 - (hwzaddr % 16);
	ggiGAFlagModified(res);
	res->props->sub.tank.read = res->props->sub.tank.write =
	  (unsigned char *)kpriv->fb + hwzaddr;

	return 0;
}

/********************************* haslist **********************************/

ggiGA_resource_list kgi_Radeon_create_master_haslist(ggi_visual_t vis) {
	ggiGA_resource_list ret, haslist;
	struct ggiGA_resource *res;
	struct ggiGA_resource_props *propslist, *props;
	struct ggiGA_mode *gamodelist, *gamode;
	struct ggiGA_tank_props *tank;
	struct ggiGA_carb_props *carb;
	struct ggiGA_motor_props *motor;
	int i;
	
	ret = haslist = calloc(9, sizeof(struct ggiGA_resource));
	propslist = calloc(7, sizeof(struct ggiGA_resource_props));
	gamodelist = calloc(2, sizeof(struct ggiGA_resource_props));
	if (!(haslist && propslist && gamodelist)) goto err;
	for (i = 0; i < 8; i++) haslist[i].next = haslist + i + 1;

	/* Tag group usage:

	   Group 1 and 2: used on the motor/carb level to separate
	   software-rendered from hardware-rendered resources, respectively.
	   LibBuf does not support mixing the two yet.

	   Group 3: used to designate that software-rendered Z and alpha 
	   tanks may share storage.

	   Group 4: used to designate that hardware-rendered Z and alpha 
	   tanks may share storage.

	*/
	
	/* Add drawops resource for visuals without hardware Z/alpha. */
	res = haslist; haslist++; 
	props = res->props = propslist; propslist++;
	motor = &(props->sub.motor);
	res->res_type = GA_RT_RENDERER_DRAWOPS | GA_RT_MOTOR;
	res->res_state = 1;	/* Tag group */
	props->qty = 1;
	motor->mul_min.x = motor->mul_max.x = 1;
	motor->mul_min.y = motor->mul_max.y = 1;
	motor->div_min.x = motor->div_max.x = 1;
	motor->div_min.y = motor->mul_max.y = 1;

	/* Add drawops resource for visuals with hardware Z/alpha. */
	res = haslist; haslist++; 
	props = res->props = propslist; propslist++;
	motor = &(props->sub.motor);
	res->res_type = GA_RT_RENDERER_DRAWOPS | GA_RT_MOTOR;
	res->res_state = 2;	/* Tag group */
	props->qty = 1;
	motor->mul_min.x = motor->mul_max.x = 1;
	motor->mul_min.y = motor->mul_max.y = 1;
	motor->div_min.x = motor->div_max.x = 1;
	motor->div_min.y = motor->mul_max.y = 1;

	/* Add a frame resource for visuals without hardware Z/Alpha */
	res = haslist; haslist++; 
	gamode = res->priv = gamodelist; gamodelist++;
	res->priv_size = sizeof(struct ggiGA_mode);
	res->res_type = GA_RT_FRAME;
        res->cbflags1 = GA_CB_PRECHECK | 
	  GA_CB_CHECK | GA_CB_POSTCHECK | GA_CB_UNCHECK | GA_CB_RECHECK;
	res->cb1 = &kgi_Radeon_galloc_check_mode;
	res->cbflags2 = GA_CB_SET | GA_CB_UNSET | GA_CB_RELEASE;
	res->cb2 = &kgi_Radeon_galloc_set_mode;
	/* gamode is set to all GGI_AUTOS by virtue of calloc. */


	/* To the frame we attach a resource representing the 
	 * software-emulated alpha and Z side-buffer implemented by LibBuf's
	 * default renderers.  First the alpha buffer:
	 */
	res = haslist; haslist++; 
	props = res->props = propslist; propslist++;
	tank = &(props->sub.tank);
	res->res_type = GA_RT_BUFFER_ABUFFER;
	res->res_state = GA_STATE_SEEABOVE | 3;
	res->cbflags1 = 
	  GA_CB_CHECK | GA_CB_POSTCHECK | GA_CB_UNCHECK | GA_CB_RECHECK;
	res->cb1 = &kgi_Radeon_galloc_check_cladbuf;
	props->storage_ok = GA_STORAGE_SWAP;
	props->storage_need = GA_STORAGE_REALRAM;
	/* Since aligning the Z buffer may help SWAR implementations, 
	 * we tell the user we expectthe Z origin to be aligned to a
	 * 128 byte boundary.
	 */
	tank->buf_snap.linear = 128;
	/* Currently LibBuf only supports 8-bit grayscale (that is, 
	 * one value affecting all channels) alpha buffers. 
	 */
	tank->gt = GT_CONSTRUCT(8, GT_GREYSCALE, 8);

	/* Now the software emulated Z buffer */
	res = haslist; haslist++; 
	props = res->props = propslist; propslist++;
	tank = &(props->sub.tank);
	res->res_type = GA_RT_BUFFER_ZBUFFER;
	res->res_state = GA_STATE_SEEABOVE | 3;
	res->cbflags1 =
	  GA_CB_CHECK | GA_CB_POSTCHECK | GA_CB_UNCHECK | GA_CB_RECHECK;
	res->cb1 = &kgi_Radeon_galloc_check_cladbuf;
	props->storage_ok = GA_STORAGE_SWAP;
	props->storage_need = GA_STORAGE_REALRAM;
	/* Since aligning the Z buffer may help SWAR implementations, 
	 * we tell the user we expectthe Z origin to be aligned to a
	 * 128 byte boundary.
	 */
	tank->buf_snap.linear = 128;
	/* Currently LibBuf only supports 8-bit grayscale (that is, 
	 * one value affecting all channels) alpha buffers. 
	 */
	tank->gt = GT_CONSTRUCT(8, GT_GREYSCALE, 8);

	/* We attach a carb representing the link between the available
	 * Z and alpha buffers to the LibBuf default drawops resource. 
	 */
	res = haslist; haslist++; 
	props = res->props = propslist; propslist++;
	carb = &(props->sub.carb);
	res->res_type =	GA_RT_RENDERER_DRAWOPS | GA_RT_CARB;
	res->res_state = GA_STATE_SEEABOVE | 1;
	carb->can.rop = LL_ROP_ALPHA_AUX | LL_ROP_Z;
	carb->can.math = 0xffffffff;  /* All math functions supported. */
	carb->must.rop = LL_ROP_AUX;  /* side-buffers only */

	/* Add a frame resource for visuals with hardware Z/Alpha */
	res = haslist; haslist++; 
	gamode = res->priv = gamodelist; gamodelist++;
	res->priv_size = sizeof(struct ggiGA_mode);
	res->res_type = GA_RT_FRAME;
        res->cbflags1 = GA_CB_PRECHECK |
	  GA_CB_CHECK | GA_CB_POSTCHECK | GA_CB_UNCHECK | GA_CB_RECHECK;
	res->cb1 = &kgi_Radeon_galloc_check_mode;
	res->cbflags2 = GA_CB_SET | GA_CB_UNSET | GA_CB_RELEASE;
	res->cb2 = &kgi_Radeon_galloc_set_mode;
	/* gamode is set to all GGI_AUTOS by virtue of calloc. */


	/* To the frame we attach a resource representing the 
	 * hardware alpha and Z side-buffer implemented by LibBuf's
	 * Radeon-specific renderers.  (Currently only Z.)
	 */
	res = haslist; haslist++; 
	props = res->props = propslist; propslist++;
	tank = &(props->sub.tank);
	res->res_type = GA_RT_BUFFER_ZBUFFER;
	res->res_state = GA_STATE_SEEABOVE | 4;
	res->cbflags1 = 
	  GA_CB_CHECK | GA_CB_POSTCHECK | GA_CB_UNCHECK | GA_CB_RECHECK;
	res->cb1 = &kgi_Radeon_galloc_check_hwz;
	res->cbflags2 = GA_CB_SET;
	res->cb2 = &kgi_Radeon_galloc_set_hwz;
	props->storage_ok = GA_STORAGE_VRAM;
	props->storage_need = GA_STORAGE_VRAM;
	/* Radeon Z origin must be 16-byte aligned. */
	tank->buf_snap.linear = 16;
	tank->gt = GT_CONSTRUCT(16, GT_GREYSCALE, 16);

	/* We attach a carb representing the link between the available
	 * Z and alpha buffers to the LibBuf Radeon drawops resource. 
	 */
	res = haslist; haslist++; 
	props = res->props = propslist; propslist++;
	carb = &(props->sub.carb);
	res->res_type =	GA_RT_RENDERER_DRAWOPS | GA_RT_CARB;
	res->res_state = GA_STATE_SEEABOVE | 2;
	carb->can.rop = LL_ROP_Z;
	carb->can.math = LL_MATH_CMP_SIGNED |
	  LL_MATH_CMP_GT | LL_MATH_CMP_LT | LL_MATH_CMP_GTE | LL_MATH_CMP_LTE;

	return ret;

 err:
	if (haslist)	free(haslist);
	if (propslist)	free(propslist);
	if (gamodelist)	free(gamodelist);
	return NULL;
}

/* Since the above was allocated chunks we need to free the chunks. */
void kgi_Radeon_destroy_master_haslist(ggi_visual_t vis) {
	free(LIBGGI_GALLOCEXT(vis)->haslist[2].priv);
	free(LIBGGI_GALLOCEXT(vis)->haslist[0].props);
	free(LIBGGI_GALLOCEXT(vis)->haslist);
	LIBGGI_GALLOCEXT(vis)->haslist = NULL;
}
