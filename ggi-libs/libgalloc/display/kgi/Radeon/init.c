/* $Id: init.c,v 1.10 2006/02/04 22:15:09 soyt Exp $
******************************************************************************

   LibGAlloc implementation for KGI Radeon target -- Initialization.

   Copyright (c) Thu Dec 12 23:48:08 EST 2002 by: 
	Brian S. Julin		bri@calyx.com
   
   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   The above copyright notice applies to all files in this package, unless 
   explicitly stated otherwise in the file itself or in a file named COPYING 
   in the same directory as the file.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


******************************************************************************
*/

#include "kgi_Radeon_galloc.h"

static int GGIopen(ggi_visual *vis, struct ggi_dlhandle *dlh,
		   const char *args, void *argptr, uint32_t *dlret)
{
	gallocpriv *gapriv;
	DPRINT_LIBS("GGIopen(%p, %p, %s, %p, %p)"
			  " called for Kgi_Radeon_GAlloc sublib\n",
			  vis, dlh, args ? args : "(NULL)", argptr, dlret);

	gapriv = LIBGGI_GALLOCEXT(vis);

	/*-* Initialize target-private data structure */

	/* Done by stubs target. */

	/*-* Hook in target functions */
	gapriv->check = GALLOC_Kgi_Radeon_Check;
	gapriv->set = GALLOC_Kgi_Radeon_Set;
	gapriv->reset_privstate = kgi_Radeon_galloc_reset_checkstate;
	gapriv->migrate_privstate = kgi_Radeon_galloc_migrate_checkstate;

	/* Stubs target functions are fine for the rest. */

        /*-* Apply initial values */
	gapriv->haslist = kgi_Radeon_create_master_haslist(vis);
	if (!gapriv->haslist) return -1;

	/* Stubs target will load default callbacks for us. */

	*dlret = GGI_DL_EXTENSION | GGI_DL_OPDISPLAY;
	return 0;
}	/* GGIopen */


static int GGIclose(ggi_visual *vis, struct ggi_dlhandle *dlh)
{
	DPRINT_LIBS("GGIdlclose(%p, %p) called for "
			  "Kgi_Radeon_GAlloc sublib\n",
			  vis, dlh);

	/*-* Free target-private structure */
	kgi_Radeon_destroy_master_haslist(vis);

	return 0;
}	/* GGIclose */


EXPORTFUNC
int GALLOCdl_kgi_Radeon_galloc(int func, void **funcptr);

int GALLOCdl_kgi_Radeon_galloc(int func, void **funcptr)
{
	switch (func) {
	case GGIFUNC_open:
		*funcptr = GGIopen;
		return 0;
	case GGIFUNC_exit:
		*funcptr = NULL;
		return 0;
	case GGIFUNC_close:
		*funcptr = GGIclose;
		return 0;
	default:
		*funcptr = NULL;
	}	/* switch */
      
	return GGI_ENOTFOUND;
}	/* GALLOCdl_kgi_Radeon_galloc */
