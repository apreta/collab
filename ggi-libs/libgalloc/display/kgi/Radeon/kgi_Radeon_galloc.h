/* $Id: kgi_Radeon_galloc.h,v 1.6 2005/07/31 15:29:56 soyt Exp $
******************************************************************************

   LibGAlloc implementation for KGI_Radeon target -- header.

   Copyright (c) Thu Dec 12 23:51:39 EST 2002 by:
	Brian S. Julin		bri@calyx.com

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   The above copyright notice applies to all files in this package, unless
   explicitly stated otherwise in the file itself or in a file named COPYING
   in the same directory as the file.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


******************************************************************************
*/

#include <ggi/internal/galloc.h>
#include <ggi/internal/galloc_debug.h>
#include <kgi/config.h>
#include <ggi/display/kgi.h>
#include <ggi/mmutil.h>

/*-* Function prototypes */

GACheck			GALLOC_Kgi_Radeon_Check;
GASet			GALLOC_Kgi_Radeon_Set;

extern ggiGA_resource_list kgi_Radeon_haslist;

void kgi_Radeon_destroy_master_haslist(ggi_visual_t vis);
ggiGA_resource_list kgi_Radeon_create_master_haslist(ggi_visual_t vis);

ggiGA_reslist_callback kgi_Radeon_galloc_check_mode;
ggiGA_reslist_callback kgi_Radeon_galloc_check_cladbuf;
ggiGA_reslist_callback kgi_Radeon_galloc_check_hwz;
ggiGA_reslist_callback kgi_Radeon_galloc_set_mode;
ggiGA_reslist_callback kgi_Radeon_galloc_set_hwz;
ggiGA_reset_checkstate kgi_Radeon_galloc_reset_checkstate;
ggiGA_migrate_checkstate kgi_Radeon_galloc_migrate_checkstate;

struct kgi_radeon_galloc_container_t {
  	rm_range_list_t *vram;
};

/*-* Structure holding private data. */
