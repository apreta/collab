/* $Id: funcs.c,v 1.5 2005/10/06 07:51:22 cegger Exp $
******************************************************************************

   LibGAlloc implementation for KGI Radeon target -- API functions.

   Copyright (c) Thu Dec 12 23:46:20 EST 2002 by: 
	Brian S. Julin		bri@calyx.com  
   
   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   The above copyright notice applies to all files in this package, unless 
   explicitly stated otherwise in the file itself or in a file named COPYING 
   in the same directory as the file.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


******************************************************************************
*/


#define COMPILING_GALLOC_STUB_FUNCTIONS
#define COMPILING_GALLOC_C
#include <ggi/internal/galloc.h>
#include <ggi/ga_prop.h>
#include <stdio.h>
#include <string.h>
#include "kgi_Radeon_galloc.h"


/* _All_ non-local (ie not declared 'static') functions and variables _must_
   be prefixed with the extension name, and for sublibs also with a unique
   sublib identifier. This is to keep the namespace clean on systems where
   all symbols are exported by default.
*/

/*-* API Implementation */

int kgi_Radeon_galloc_reset_checkstate(ggi_visual_t vis, 
				       struct ggiGA_template_state *state,
				       void *privstate) {
	struct kgi_radeon_galloc_container_t *pstate;
	ggi_kgi_priv *kpriv;

	pstate = privstate;
	kpriv = LIBGGI_PRIVATE(vis);

	state->mode = NULL;
	state->haslist = NULL;
	if(pstate->vram) rm_list_exit(&(pstate->vram));
	rm_list_init(&(pstate->vram));

	return GALLOC_OK;
}

int kgi_Radeon_galloc_migrate_checkstate(ggi_visual_t vis, 
					 struct ggiGA_template_state *state,
					 void *privstate,
					 ggiGA_resource_list from,
					 ggiGA_resource_list to) {
	struct kgi_radeon_galloc_container_t *pstate;
	int idx;

	pstate = privstate;

	fprintf(stderr, "privstate = %p\n", pstate);
	fprintf(stderr, "migrating from %p to %p (rl is %p)\n",
		from, to, pstate->vram);

	if (!pstate->vram) goto skip;

	for (idx = 0; idx < pstate->vram->count; idx++) {
		rm_range_t *r;
		r = pstate->vram->range + idx;
		r->attribute.owner = 
			ggiGAHandle(to, from, r->attribute.owner);
	}
	fprintf(stderr, "migrated %i owners\n", idx);

 skip:
	return GALLOC_OK;
}


int GALLOC_Kgi_Radeon_Check(ggi_visual_t vis, ggiGA_resource_list request, 
			    ggiGA_resource_list *result)
{
	struct ggiGA_template_state state;
	struct kgi_radeon_galloc_container_t pstate;
	int rc;

	pstate.vram = NULL;
	kgi_Radeon_galloc_reset_checkstate(vis, &state, &pstate);

	rc = ggiGACheck_template(vis, request, result, &state, &pstate);
	ggiGADestroyList(&(state.haslist));
	return rc;
}	/* GALLOC_Kgi_Radeon_Check */


int GALLOC_Kgi_Radeon_Set(ggi_visual_t vis, ggiGA_resource_list request, 
			  ggiGA_resource_list *result)
{
	struct ggiGA_template_state state;
	struct kgi_radeon_galloc_container_t pstate;
	int rc;

	pstate.vram = NULL;
	kgi_Radeon_galloc_reset_checkstate(vis, &state, &pstate);

	rc = ggiGASet_template(vis, request, result, &state, &pstate);
	ggiGADestroyList(&(state.haslist));
	return rc;
}	/* GALLOC_Kgi_Radeon_Set */

