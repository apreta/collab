/* $Id: renderer.c,v 1.6 2005/10/12 12:44:45 cegger Exp $
******************************************************************************

   Galloc implementation for X target - renderer functions.

   Copyright (C) 2001	Christoph Egger	[Christoph_Egger@t-online.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include <stdio.h>

#include "xgalloc.h"



/******************************************************
 *	Check Renderer
 */



int check_renderer_motor(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* check_renderer_motor */


int check_renderer_carb(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* check_renderer_carb */




int check_renderer_tank(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	ggiGA_resource_handle cap = NULL;
	ggi_xwin_common *priv = NULL;
	struct GA_x_renderer *renderer = NULL;

	int rc = GALLOC_EUNAVAILABLE;

	int res_state;
	int storage_need;

	res_state = (handle->res_state & GA_STATE_REQUEST_MASK);
	storage_need = (handle->props->storage_need & GA_STORAGE_REQUEST_MASK);


	LIB_ASSERT(handle != NULL, "handle == NULL");
	priv = LIBGGI_PRIVATE(vis);


	switch (handle->res_type & GA_RT_SUBTYPE_MASK) {
	case (GA_RT_RENDERER_DONTCARE & GA_RT_SUBTYPE_MASK):

		/* Not specified enough, what the user want to do.
		 */
		rc = GALLOC_ESPECIFY;
		goto err0;

	case (GA_RT_RENDERER_DRAWOPS & GA_RT_SUBTYPE_MASK):
		if (handle->priv == NULL) {
			renderer = malloc(sizeof(struct GA_x_renderer));
			if (!renderer) {
				rc = GGI_ENOMEM;
				ggiGAFlagFailed(handle);
				break;
			}	/* if */
			handle->priv = (void *)renderer;
			handle->priv_size = sizeof(struct GA_x_renderer);
		} else {
			renderer = (struct GA_x_renderer *)handle->priv;
		}	/* if */



		/* Set coordbase */
		handle->cb = (GA_COORDBASE_UNIT & GA_FLAG_UNIT_MASK);
		handle->cb |= (GA_COORDBASE_PIXEL & GA_FLAG_COORDBASE_MASK);

                /* Get maximum size the resource can have
                 */
#if 0
		if (lastmode != NULL) {
			struct ggiGA_mode *ga_mode = NULL;

			ga_mode = (struct ggiGA_mode *)lastmode->priv;

			maxwidth = ga_mode->mode.visible.x;
			maxheight = ga_mode->mode.visible.y;
		} else {
			/* maxsize */
			maxwidth = priv->defsize.x;
			maxheight = priv->defsize.y;
		}	/* if */

		/* Is there a CAP ?
		 */
		if (ggiGAIsCap(handle->next)) {
			/* Check CAP props */
			cap = handle->next;

			GA_CHECK_CAPAUTO_2D(cap,
				maxwidth, maxheight, maxdepth);
		}	/* if */

		/* Get requested size
		 */
		width = &(handle->props->size.area.x);
		height = &(handle->props->size.area.y);
		depth = &(handle->props->sub.tank.gt);

		/* Check GGI_AUTO's */
		GA_CHECK_GGIAUTO_2D(handle, maxwidth, maxheight, maxdepth);
				


		if (width[0] > maxwidth) {
			width[0] = maxwidth;
			ggiGAFlagModified(handle);
		}	/* if */
		if (height[0] > maxheight) {
			height[0] = maxheight;
			ggiGAFlagModified(handle);
		}	/* if */
		if (depth[0] > maxdepth) {
			depth[0] = maxdepth;
			ggiGAFlagModified(handle);
		}	/* if */
#endif

		goto renderer_drawops_exit;





renderer_drawops_exit:
		LIB_ASSERT(renderer != NULL, "renderer == NULL");
		handle->storage_ok = (GA_STORAGE_VIRT | GA_STORAGE_VRAM);
		goto exit;

renderer_drawops_err:
		goto err0;



	}	/* switch */


exit:
	handle->props->storage_ok = handle->storage_ok;
	LIB_ASSERT(handle->priv != NULL, "handle has no private data");
	rc = GALLOC_OK;
	return rc;

err0:
	handle->res_state &= ~GA_STATE_MODIFIED;
	ggiGAFlagFailed(handle);
	return rc;
}	/* check_renderer_tank */



int check_renderer(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	if (ggiGAIsMotor(handle)) {
		return check_renderer_motor(vis, handle, compound, lastmode);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return check_renderer_carb(vis, handle, compound, lastmode);
	}	/* if */

	
	return check_renderer_tank(vis, handle, compound, lastmode);
}	/* check_renderer */












/******************************************************
 *	Set Renderer
 */


int set_renderer_motor(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* set_renderer_motor */


int set_renderer_carb(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* set_renderer_carb */




int set_renderer_tank(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	ggi_xwin_common *priv;
	struct GA_x_renderer *renderer;

	int rc = GALLOC_EUNAVAILABLE;

	LIB_ASSERT(handle != NULL, "handle == NULL");
	LIB_ASSERT(handle->priv != NULL, "handle has no priv data");
	priv = LIBGGI_PRIVATE(vis);

	renderer = (struct GA_x_renderer *)handle->priv;


	switch (handle->res_type & GA_RT_SUBTYPE_MASK) {
	case (GA_RT_RENDERER_DONTCARE & GA_RT_SUBTYPE_MASK):

		/* don't know, what you exactly want */
		rc = GALLOC_ESPECIFY;
		break;

	case (GA_RT_RENDERER_DRAWOPS & GA_RT_SUBTYPE_MASK):

		/* basic renderers are always available */
		rc = GALLOC_OK;
		break;

	}	/* switch */

	return rc;
}	/* set_renderer_tank */



int set_renderer(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	if (ggiGAIsMotor(handle)) {
		return set_renderer_motor(vis, handle, compound, lastmode);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return set_renderer_carb(vis, handle, compound, lastmode);
	}	/* if */

	
	return set_renderer_tank(vis, handle, compound, lastmode);
}	/* set_renderer */














/******************************************************
 *	Release Renderer
 */


int release_renderer_motor(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* release_renderer_motor */


int release_renderer_carb(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* release_renderer_carb */



int release_renderer_tank(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	ggi_xwin_common *priv;
	struct GA_x_renderer *renderer;

	int rc = GALLOC_OK;

	LIB_ASSERT(handle != NULL, "handle == NULL");

	priv = LIBGGI_PRIVATE(handle->vis);
	renderer = (struct GA_x_renderer *)handle->priv;


	switch (handle->res_type & GA_RT_SUBTYPE_MASK) {
	case (GA_RT_RENDERER_DONTCARE & GA_RT_SUBTYPE_MASK):
		break;

	case (GA_RT_RENDERER_DRAWOPS & GA_RT_SUBTYPE_MASK):

		free(renderer);
		renderer = NULL;
		break;

	}	/* switch */

	return rc;
}	/* release_renderer_tank */


int release_renderer(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	if (ggiGAIsMotor(handle)) {
		return release_renderer_motor(vis, handle);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return release_renderer_carb(vis, handle);
	}	/* if */

	
	return release_renderer_tank(vis, handle);
}	/* release_renderer */
