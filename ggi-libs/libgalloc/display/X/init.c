/* $Id: init.c,v 1.16 2007/09/04 01:26:27 cegger Exp $
******************************************************************************

   LibGAlloc implementation for "X" target -- Initialization.

   Copyright (c) Thu Mar 22 2002 by: 
	Brian S. Julin		bri@calyx.com
   
   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies of substantial portions of the Software.

   The above copyright notice applies to all files in this package, unless 
   explicitly stated otherwise in the file itself or in a file named COPYING 
   in the same directory as the file.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.





******************************************************************************
*/

#include "xgalloc.h"

static const char *rtstrings[GA_RTNUM]			= {GA_RTSTRINGS};
static const char *rtunsetstrings[1]			= { "no subtype" };
static const char *rtframestrings[GA_RTFRAMENUM]	= {GA_RTFRAMESTRINGS};
static const char *rtbufferstrings[GA_RTBUFFERNUM]	= {GA_RTBUFFERSTRINGS};
static const char *rtbobstrings[GA_RTBOBNUM]		= {GA_RTBOBSTRINGS};
static const char *rtspritestrings[GA_RTSPRITENUM]	= {GA_RTSPRITESTRINGS};
static const char *rtvideostrings[GA_RTVIDEONUM]	= {GA_RTVIDEOSTRINGS};
static const char *rtwindowstrings[GA_RTWINDOWNUM]	= {GA_RTWINDOWSTRINGS};
static const char *rtmiscstrings[GA_RTMISCNUM]		= {GA_RTMISCSTRINGS};
static const char *rtrendererstrings[GA_RTRENDERERNUM]	= {GA_RTRENDERERSTRINGS};
static const char *rtresliststrings[GA_RTRESLISTNUM]	= {GA_RTRESLISTSTRINGS};

static int GGIopen(ggi_visual *vis, struct ggi_dlhandle *dlh,
		   const char *args, void *argptr, uint32_t *dlret)
{
	gallocpriv *priv;
	ggi_x_priv *xpriv;
	int i, screen;
	ggiGA_resource_handle hand;

	DPRINT_LIBS("GGIopen(%p, %p, %s, %p, %p)"
			  " called for X_GAlloc sublib\n",
			  vis, dlh, args ? args : "(NULL)", argptr, dlret);

	priv = LIBGGI_GALLOCEXT(vis);
	xpriv = GGIX_PRIV(vis);

	/* Right now we do not support multiple screens; it 
	 * adds complexity as the screen must be chosen by the
	 * highest priority resources.  In the future this support
	 * will be added.
	 */
	screen = -1;
	for (i = 0; i < xpriv->nvisuals; i++) {
		if (xpriv->vilist[i].flags && GGI_X_VI_NON_FB) continue;
		if (screen == -1) {
			screen = xpriv->vilist[i].vi->screen; 
			continue;
		}	/* if */
		if (screen != xpriv->vilist[i].vi->screen) {
			fprintf(stderr, "LibGAlloc: Multiple screen support"
				" not implemented yet.\n");
			return -1;
		}	/* if */
	}	/* for */
	LIB_ASSERT(screen >= 0, "no screen found");

	/*-* Initialize target-private data structure */

	/*-* Hook in target functions */

	priv->set = GALLOC_X_Set;
	priv->release = GALLOC_X_Release;
	priv->checkifshareable = GALLOC_X_CheckIfShareable;
	priv->anprintf = GALLOC_X_anprintf;
	priv->check = GALLOC_X_Check;
	priv->_mode = GALLOC_X__Mode;


	x_galloc_InitHaslist(priv);

	/* Find the maximum size of the mouse cursor and put
	 * it in the motor and tank caps.   The rest of the resources
	 * are trained to look at the motor cap for maximum size.
	 */
	RESLIST_FOREACH(hand, priv->haslist) {
		if (ggiGAGetType(hand) == GA_RT_SPRITE_POINTER 
		    && !ggiGAIsCarb(hand) && ggiGAIsCap(hand)){
			unsigned int w, h;
			XQueryBestCursor(xpriv->disp, 
					 RootWindow(xpriv->disp, screen),
					 65535, 65535, &w, &h);
			hand->props->size.area.x = w;
			hand->props->size.area.y = h;
		}	/* if */
	}	/* foreach */

	priv->rtstrings = rtstrings;
	priv->rtsubstrings = calloc(GA_RTNUM, sizeof(char **));
	priv->rtsubstrings[0] = rtunsetstrings;
	priv->rtsubstrings[1] = rtframestrings;
	priv->rtsubstrings[2] = rtbufferstrings;
	priv->rtsubstrings[3] = rtbobstrings;
	priv->rtsubstrings[4] = rtspritestrings;
	priv->rtsubstrings[5] = rtvideostrings;
	priv->rtsubstrings[6] = rtwindowstrings;
	priv->rtsubstrings[7] = rtmiscstrings;
	priv->rtsubstrings[8] = rtrendererstrings;
	priv->rtsubstrings[9] = rtresliststrings;

#if 0
	priv->default_callbacks[0] = ga_x_match;
	priv->default_callbacks[1] = ga_x_precheck;
	priv->default_callbacks[2] = ga_x_check;
	priv->default_callbacks[3] = ga_x_check;
	priv->default_callbacks[4] = ga_x_uncheck;
	priv->default_callbacks[5] = ga_x_check;
	priv->default_callbacks[6] = ga_x_set;
	priv->default_callbacks[7] = ga_x_unset;
	priv->default_callbacks[8] = ga_x_release;
#endif
	
	*dlret = (GGI_DL_EXTENSION | GGI_DL_OPDISPLAY);
	return 0;
}	/* GGIopen */


static int GGIclose(ggi_visual *vis, struct ggi_dlhandle *dlh)
{
	DPRINT_LIBS("GGIclose(%p, %p) called for X_galloc sublib\n",
	       vis, dlh);


	/*-* Free target-private structure */


	return 0;
}	/* GGIclose */


EXPORTFUNC
int GALLOCdl_X_galloc(int func, void **funcptr);

int GALLOCdl_X_galloc(int func, void **funcptr)
{
	switch (func) {
	case GGIFUNC_open:
		*funcptr = GGIopen;
		return 0;
	case GGIFUNC_exit:
		*funcptr = NULL;
		return 0;
	case GGIFUNC_close:
		*funcptr = GGIclose;
		return 0;
	default:
		*funcptr = NULL;
	}	/* switch */
      
	return GGI_ENOTFOUND;
}	/* GALLOCdl_x_galloc */
