/* $Id: sprite.c,v 1.24 2005/10/12 12:44:45 cegger Exp $
******************************************************************************

   Galloc implementation for X target - sprite functions.

   Copyright (C) 2001	Christoph Egger	[Christoph_Egger@t-online.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include <stdio.h>

#include "xgalloc.h"



/******************************************************
 *	Check Sprite
 */



int check_sprite_motor(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* check_sprite_motor */


int check_sprite_carb(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* check_sprite_carb */




int check_sprite_tank(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	ggiGA_resource_handle cap = NULL;
	ggi_xwin_common *priv = NULL;
	struct GA_x_sprite *sprite = NULL;

	int rc = GALLOC_EUNAVAILABLE;

	int res_state;
	int storage_need;

	res_state = (handle->res_state & GA_STATE_REQUEST_MASK);
	storage_need = (handle->props->storage_need & GA_STORAGE_REQUEST_MASK);


	LIB_ASSERT(handle != NULL, "handle == NULL");
	priv = LIBGGI_PRIVATE(vis);


	switch (handle->res_type & GA_RT_SUBTYPE_MASK) {
	case (GA_RT_SPRITE_DONTCARE & GA_RT_SUBTYPE_MASK):
		/* Oops! We know, X can handle two kind of sprites:
		 * X-mouse-cursor-sprite and X-text-cursor-sprite.
		 * But nothing is specified to allocate, so we fail here.
		 */
		rc = GALLOC_ESPECIFY;
		goto err0;

	case (GA_RT_SPRITE_POINTER & GA_RT_SUBTYPE_MASK):
	case (GA_RT_SPRITE_CURSOR & GA_RT_SUBTYPE_MASK):
		{
		int16_t *width, *height;
		ggi_graphtype *depth;
		int ret_width, ret_height;
		int maxwidth, maxheight, maxdepth;


		if (handle->priv == NULL) {
			sprite = malloc(sizeof(struct GA_x_sprite));
			if (!sprite) {
				rc = GGI_ENOMEM;
				ggiGAFlagFailed(handle);
				break;
			}	/* if */
			handle->priv = (void *)sprite;
			handle->priv_size = sizeof(struct GA_x_sprite);
		} else {
			sprite = (struct GA_x_sprite *)handle->priv;
		}	/* if */


		if  ( (storage_need & GA_STORAGE_NOVIRT)
		   && (storage_need & GA_STORAGE_NOVRAM))
		{
			rc = GALLOC_EFAILED;
			goto sprite_cursor_err;
		}	/* if */

	
		if (  !(storage_need & GA_STORAGE_VIRT)
		   && !(storage_need & GA_STORAGE_VRAM)
		   && ((storage_need & GA_STORAGE_STORAGE_MASK)
			!= GA_STORAGE_DONTCARE) )
		{
			rc = GALLOC_EFAILED;
			goto sprite_cursor_err;
		}	/* if */


		if ((GA_FLAG(handle->props) & GA_FLAG_AUTOTRACK) == GGI_AUTO) {

			/* Auto-Track is always required, because X
			 * forces us to bind the mouse to the sprite-pointer
			 * and to bind the keyboard to the sprite-cursor.
			 */
			GA_FLAG(handle->props) |= GA_FLAG_AUTOTRACK;
		}	/* if */


		/* Set coordbase */
		handle->cb = (GA_COORDBASE_UNIT & GA_FLAG_UNIT_MASK);
		handle->cb |= (GA_COORDBASE_PIXEL & GA_FLAG_COORDBASE_MASK);


                /* Get maximum size the resource can have
                 */
		if (lastmode != NULL) {
			struct ggiGA_mode *ga_mode = NULL;

			ga_mode = (struct ggiGA_mode *)lastmode->priv;

			maxwidth = ga_mode->mode.visible.x;
			maxheight = ga_mode->mode.visible.y;
		} else {
			/* maxsize */
			maxwidth = priv->defsize.x;
			maxheight = priv->defsize.y;
		}	/* if */

		XQueryBestCursor(priv->x.display, priv->window,
				 maxwidth, maxheight,
				 &ret_width, &ret_height);
		maxwidth = ret_width;
		maxheight = ret_height;
		maxdepth = GT_1BIT;


		/* Is there a CAP ?
		 */
		if (ggiGAIsCap(handle->next)) {
			/* Check CAP props */
			cap = handle->next;

			GA_CHECK_CAPAUTO_2D(cap,
				maxwidth, maxheight, maxdepth);
		}	/* if */


		/* Get requested size
		 */
		width = &(handle->props->size.area.x);
		height = &(handle->props->size.area.y);
		depth = &(handle->props->sub.tank.gt);

		/* Check GGI_AUTO's */
		GA_CHECK_GGIAUTO_2D(handle, maxwidth, maxheight, maxdepth);
				


		if (width[0] > maxwidth) {
			width[0] = maxwidth;
			ggiGAFlagModified(handle);
		}	/* if */
		if (height[0] > maxheight) {
			height[0] = maxheight;
			ggiGAFlagModified(handle);
		}	/* if */
		if (depth[0] > maxdepth) {
			depth[0] = maxdepth;
			ggiGAFlagModified(handle);
		}	/* if */


#define GA_CHECK_SPRITE(exit, error)				\
								\
XQueryBestCursor(priv->x.display, priv->window,			\
		 width[0], height[0], &ret_width, &ret_height);	\
if ((width[0] == ret_width) && (height[0] == ret_height)) {	\
	rc = GALLOC_OK;						\
} else {							\
	rc = GALLOC_EFAILED;					\
}	/* if */


		GA_CHECK_SPRITE(sprite_cursor_exit, sprite_cursor_err);

#undef GA_CHECK_SPRITE

		if (rc < GALLOC_OK) goto sprite_cursor_err;
		goto sprite_cursor_exit;





sprite_cursor_exit:
		LIB_ASSERT(sprite != NULL, "sprite == NULL");
		handle->storage_ok = (GA_STORAGE_VIRT | GA_STORAGE_VRAM);
		goto exit;

sprite_cursor_err:
		goto err0;
		}

	case (GA_RT_SPRITE_SPRITE & GA_RT_SUBTYPE_MASK):

		/* X doesn't support hardware-sprites, so fail! */
		rc = GALLOC_EUNAVAILABLE;
		goto err0;

	}	/* switch */


exit:
	handle->props->storage_ok = handle->storage_ok;
	LIB_ASSERT(handle->priv != NULL, "handle has no private data");
	rc = GALLOC_OK;
	return rc;

err0:
	handle->res_state &= ~GA_STATE_MODIFIED;
	ggiGAFlagFailed(handle);
	return rc;
}	/* check_sprite_tank */



int check_sprite(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	if (ggiGAIsMotor(handle)) {
		return check_sprite_motor(vis, handle, compound, lastmode);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return check_sprite_carb(vis, handle, compound, lastmode);
	}	/* if */

	
	return check_sprite_tank(vis, handle, compound, lastmode);
}	/* check_sprite */












/******************************************************
 *	Set Sprite
 */


int set_sprite_motor(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* set_sprite_motor */


int set_sprite_carb(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* set_sprite_carb */




int set_sprite_tank(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	ggi_xwin_common *priv;
	struct GA_x_sprite *sprite;

	int rc = GALLOC_EUNAVAILABLE;

	LIB_ASSERT(handle != NULL, "handle == NULL");
	LIB_ASSERT(handle->priv != NULL, "no private data");
	priv = LIBGGI_PRIVATE(vis);

	sprite = (struct GA_x_sprite *)handle->priv;


	switch (handle->res_type & GA_RT_SUBTYPE_MASK) {
	case (GA_RT_SPRITE_DONTCARE & GA_RT_SUBTYPE_MASK):
		rc = GALLOC_EUNAVAILABLE;
		break;

	case (GA_RT_SPRITE_POINTER & GA_RT_SUBTYPE_MASK):
		{
		XColor fg, bg;
		ggi_color fg_col, bg_col;
		Pixmap source, mask;

		void *buf = NULL;
		int width, height;



		width = handle->props->size.area.x;
		height = handle->props->size.area.y;

		LIB_ASSERT(width != GGI_AUTO, "width == GGI_AUTO");
		LIB_ASSERT(height != GGI_AUTO, "height == GGI_AUTO");

		DPRINT_TARGET("%s:%s:%i: sprite-size (%i,%i,%i)\n",
			DEBUG_INFO, width, height, priv->visual.depth);


		/* we set up a black cursor */
		buf = calloc( width * height, sizeof(signed char));
		if (!buf) {
			rc = GGI_ENOMEM;
			break;
		}	/* if */


		fg.pixel = LIBGGI_GC_FGCOLOR(vis);
		bg.pixel = LIBGGI_GC_BGCOLOR(vis);

#if 1		/* Do we really need this? */
		ggiUnmapPixel(vis, fg.pixel, &fg_col);
		ggiUnmapPixel(vis, bg.pixel, &bg_col);

		fg.red = fg_col.r;
		fg.green = fg_col.g;
		fg.blue = fg_col.b;
		fg.flags = DoRed | DoGreen | DoBlue;

		bg.red = bg_col.r;
		bg.green = bg_col.g;
		bg.blue = bg_col.b;
		bg.flags = DoRed | DoGreen | DoBlue;
#endif

		source = XCreatePixmapFromBitmapData(
				priv->x.display,
				priv->window,
				(signed char *)buf,
				width, height,
				fg.pixel, bg.pixel,
				priv->visual.depth);
		mask = None;

		sprite->cursor = XCreatePixmapCursor(
			priv->x.display,
			source, mask,
			&fg, &bg, 0, 0);

		if (mask != None) {
			XFreePixmap(priv->x.display, mask);
		}	/* if */

		XFreePixmap(priv->x.display, source);
		free(buf);
		rc = GALLOC_OK;

		}
		break;

	case (GA_RT_SPRITE_CURSOR & GA_RT_SUBTYPE_MASK):

#warning Should we use XCreateGlyphCursor() or XCreateFontCursor() for creating a text-cursor?

		/* I suppose we should use XCreateGlyphCursor() here, but
		 * I used XCreateFontCursor() just for testing purpose
		 */

		sprite->cursor = XCreateFontCursor(priv->x.display,
						CursorShape);
		if (sprite->cursor != BadAlloc) {
			rc = GALLOC_OK;
		}	/* if */
		break;

	case (GA_RT_SPRITE_SPRITE & GA_RT_SUBTYPE_MASK):

		/* X doesn't support hardware-sprites, so fail! */
		rc = GALLOC_EUNAVAILABLE;
		break;

	}	/* switch */

	return rc;
}	/* set_sprite_tank */



int set_sprite(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	if (ggiGAIsMotor(handle)) {
		return set_sprite_motor(vis, handle, compound, lastmode);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return set_sprite_carb(vis, handle, compound, lastmode);
	}	/* if */

	
	return set_sprite_tank(vis, handle, compound, lastmode);
}	/* set_sprite */














/******************************************************
 *	Release Sprite
 */


int release_sprite_motor(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* release_sprite_motor */


int release_sprite_carb(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* release_sprite_carb */



int release_sprite_tank(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	ggi_xwin_common *priv;
	struct GA_x_sprite *sprite;

	int rc = GALLOC_OK;

	LIB_ASSERT(handle != NULL, "handle == NULL");

	priv = LIBGGI_PRIVATE(handle->vis);
	sprite = (struct GA_x_sprite *)handle->priv;


	switch (handle->res_type & GA_RT_SUBTYPE_MASK) {
	case (GA_RT_SPRITE_DONTCARE & GA_RT_SUBTYPE_MASK):
		break;

	case (GA_RT_SPRITE_POINTER & GA_RT_SUBTYPE_MASK):

		/* the caller makes sure, that the cursor
		 * is already undefined, as only the caller knows
		 * if the sprite is shown or not
		 */

		XFreeCursor(priv->x.display, sprite->cursor);
		free(sprite);
		sprite = NULL;
		break;

	case (GA_RT_SPRITE_CURSOR & GA_RT_SUBTYPE_MASK):

		/* the caller makes sure, that the cursor
		 * is already undefined, as only the caller knows
		 * if the sprite is shown or not
		 */

		XFreeCursor(priv->x.display, sprite->cursor);
		free(sprite);
		sprite = NULL;
		break;

	case (GA_RT_SPRITE_SPRITE & GA_RT_SUBTYPE_MASK):

		/* X doesn't support hardware-sprites, so fail! */
		rc = GALLOC_EUNAVAILABLE;
		break;

	}	/* switch */

	return rc;
}	/* release_sprite_tank */


int release_sprite(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	if (ggiGAIsMotor(handle)) {
		return release_sprite_motor(vis, handle);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return release_sprite_carb(vis, handle);
	}	/* if */

	
	return release_sprite_tank(vis, handle);
}	/* release_sprite */

