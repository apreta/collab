/* $Id: xgalloc.h,v 1.12 2005/10/11 21:34:38 cegger Exp $
******************************************************************************

   LibGAlloc implementation for "X" target -- header.
  
   Copyright (c) Thu Mar 22 2001 by: 
	Brian S. Julin		bri@calyx.com
	Christoph Egger		Christoph_Egger@t-online.de
   
   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies of substantial portions of the Software.

   The above copyright notice applies to all files in this package, unless 
   explicitly stated otherwise in the file itself or in a file named COPYING 
   in the same directory as the file.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.





******************************************************************************
*/

#include <ggi/internal/galloc.h>
#include <ggi/internal/galloc_debug.h>
#include <ggi/display/x.h>

/* size of carb rows for stubs-blt */
#define X_GALLOC_BOB_CARB_ROWSIZE 4096

/*-* Function prototypes */

GASet			GALLOC_X_Set;
GARelease		GALLOC_X_Release;
GACheckIfShareable	GALLOC_X_CheckIfShareable;
GAanprintf		GALLOC_X_anprintf;
GACheck			GALLOC_X_Check;
GA_Mode			GALLOC_X__Mode;

int x_galloc_InitHaslist(gallocpriv *priv);

ggiGA_reslist_callback x_galloc_check_mode;
ggiGA_reslist_callback x_galloc_check_cladbuf;
ggiGA_reslist_callback x_galloc_set_mode;

struct galloc_x_state {
	ggi_mode minmode, maxmode;
};

/*-* Structure holding private data. */
