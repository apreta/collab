/*
******************************************************************************

   LibGAlloc implementation for "X" target -- 
   				Master resource list and associated callbacks
  
   Copyright (c) 2002 Brian S. Julin		[bri@tull.umassp.edu]
   
   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   The above copyright notice applies to all files in this package, unless 
   explicitly stated otherwise in the file itself or in a file named COPYING 
   in the same directory as the file.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "xgalloc.h"

#include <string.h>


static int x_galloc_check_ptrmotor (ggi_visual_t vis,
			     ggiGA_resource_list reslist,
			     enum ggiGA_callback_command command,
			     ggiGA_resource_handle res,
			     ggiGA_resource_handle head,
			     ggiGA_taghead_t taghead,
			     ggiGA_resource_handle has,
			     struct ggiGA_template_state *state,
			     void *privstate)
{
	ggi_x_priv *xpriv;
	ggiGA_resource_handle rcap = NULL;
        ggiGA_resource_handle hcap = NULL;
        struct ggiGA_resource_props *rp, *hp, *rcp, *hcp;
        struct ggiGA_motor_props *rmp, *hmp, *rcmp, *hcmp;
        int rc;

	xpriv = GGIX_PRIV(vis);

	if (ggiGAMotorCapSanity(res) != GALLOC_OK) 
		return GALLOC_EFAILED;

	rcap = res;
	hcap = RESLIST_NEXT(has);
        hp = has->props; hmp = &(hp->sub.motor);
	hcp = hcap->props; hcmp = &(hcp->sub.motor);

        rp = rcp = res->props; rmp = rcmp = &(rp->sub.motor);
        if (ggiGAIsCap(RESLIST_NEXT(res))) {
                rcap = RESLIST_NEXT(res);
                rcp = rcap->props; rcmp = &(rcp->sub.motor);
        }

        rc = 0;

	/* X mouse always autotracks and is always over fb. */
	if ((((rp->flags | rcp->flags) & 
	      (GA_FLAG_AUTOTRACK | GA_FLAG_ALWAYS_ON_TOP)) !=
	     (GA_FLAG_AUTOTRACK | GA_FLAG_ALWAYS_ON_TOP)) &&
	    (rc = GALLOC_EFAILED))
	{
		rp->flags |= GA_FLAG_AUTOTRACK | GA_FLAG_ALWAYS_ON_TOP;
	}

	/* We only have one "active" slot. */
	if (_ga_cmp_qty(rp, rcp, hp, hcp, rc)) rc = GALLOC_EFAILED;

	/* No scaling allowed */
	if (_ga_cmp_factors(rp, rcp, hp, hcp, rc)) rc = GALLOC_EFAILED;

	return rc;

}


/* This resource represents the ability of the X display target
 * to make bits in the mouse pointer transparent.
 */
static struct ggiGA_resource_props ptr_carb_props = {
  .qty = 1,
  .storage_ok =   GA_STORAGE_SWAP,
  .storage_need = GA_STORAGE_SWAP_RO,
  .sub = {
    .carb = {
      .can = {
	.rop =	(LL_ROP_SOURCE | LL_ROP_ALPHA_SOURCE),
	.math = (LL_MATH_ALPHA_OVER | LL_MATH_CMP_GT),
	.adj =  0
      },
      .must = {
	.rop =  LL_ROP_SOURCE,
	.math = 0,
	.adj =  0
      }
    }
  }
};

static struct ggiGA_resource ptr_carb_res = {
  .res_type =	(GA_RT_SPRITE_POINTER | GA_RT_CARB),
  .res_state =	(GA_STATE_SEEABOVE | 2),	/* Tag group 2 */
  .props = &ptr_carb_props,
};

/* This resource represents the transparancy mask data for
 * the mouse cursor pointer.
 */
static struct ggiGA_resource_props ptr_abuf_cap_props = {
  .qty = 0,
  .size = { .area = {1,1} },
  .storage_ok =   GA_STORAGE_SWAP,
  .storage_need = GA_STORAGE_SWAP_RO,
};

static struct ggiGA_resource ptr_abuf_cap = {
  .res_type = GA_RT_BUFFER_ABUFFER,
  .res_state = GA_STATE_CAP | GA_STATE_SEEABOVE,
  .props = &ptr_abuf_cap_props,
};

static struct ggiGA_resource_props ptr_abuf_props = {
  .qty = 0,
  .size = { .area = {1,1} },
  .storage_ok =   GA_STORAGE_SWAP,
  .storage_need = GA_STORAGE_SWAP_RO,
};

static struct ggiGA_resource ptr_abuf_res = {
  .res_type = GA_RT_BUFFER_ABUFFER,
  .res_state = GA_STATE_SEEABOVE,
  .props = &ptr_abuf_props,
};

/* This resource represents the pixel data for the mouse cursor pointer.
 */
static struct ggiGA_resource_props ptr_buf_cap_props = {
  .qty = 1,
  .storage_ok =   GA_STORAGE_SWAP,
  .storage_need = GA_STORAGE_SWAP_RO,
};

static struct ggiGA_resource ptr_buf_cap = {
  .res_type = GA_RT_SPRITE_POINTER,
  .res_state = GA_STATE_CAP | GA_STATE_SEEABOVE,
  .props = &ptr_buf_cap_props,
};

static struct ggiGA_resource_props ptr_buf_props = {
  .qty = 1,
  .storage_ok =   GA_STORAGE_SWAP,
  .storage_need = GA_STORAGE_SWAP_RO,
};

static struct ggiGA_resource ptr_buf_res = {
  .res_type = GA_RT_SPRITE_POINTER,
  .props = &ptr_buf_props,
};

/* This resource represents a) the palette functionalities 
 * of display-X target for the main framebuffer, and b) the ability of 
 * the target to be enhanced by LibBuf to mix in alpha and Z sources
 * in drawing operations by the main framebuffer.
 */
static struct ggiGA_resource_props drawcarb_props = {
  .storage_ok =   GA_STORAGE_SWAP,
  .storage_need = GA_STORAGE_SWAP,
  .sub = {
    .carb = {
      .can = {
	.rop =	LL_ROP_ALPHA_AUX | LL_ROP_Z,
	.math =   0xffffffff,  /* All math functions supported. */
	.adj =    0
      },
      .must = {
	.rop =    LL_ROP_AUX,  /* side-buffers only */
	.math =   0,           /* No math is mandatory. */
	.adj =    0
      }
    }
  }
};

static struct ggiGA_resource drawcarb_res = {
  .res_type =	GA_RT_RENDERER_DRAWOPS | GA_RT_CARB,
  .res_state =	GA_STATE_SEEABOVE | 1,
  .props = &drawcarb_props,
};

/* This resource represents a software Z buffer, e.g. that which
 * would be used by LibBuf's stub renderer.
 */
static struct ggiGA_resource_props zbuf_props = {
  .storage_ok =   GA_STORAGE_SWAP,
  .storage_need = GA_STORAGE_SWAP,
};

static struct ggiGA_resource zbuf_res = {
  .res_type = GA_RT_BUFFER_ZBUFFER,
  .res_state = GA_STATE_SEEABOVE,
  .props = &zbuf_props,
  .cbflags1 = GA_CB_CHECK | GA_CB_RECHECK | GA_CB_POSTCHECK,
  .cb1 = &x_galloc_check_cladbuf
};

/* This resource represents a software Alpha buffer, e.g. that which
 * would be used by LibBuf's stub renderer.
 */
static struct ggiGA_resource_props alphabuf_props = {
  .storage_ok =   GA_STORAGE_SWAP,
  .storage_need = GA_STORAGE_SWAP,
};

static struct ggiGA_resource alphabuf_res = {
  .res_type = GA_RT_BUFFER_ABUFFER,
  .res_state = GA_STATE_SEEABOVE,
  .props = &alphabuf_props,
  .cbflags1 = GA_CB_CHECK | GA_CB_RECHECK | GA_CB_POSTCHECK,
  .cb1 = &x_galloc_check_cladbuf
};

/* This resource represents the data in the main framebuffer.
 */
static struct ggiGA_mode frame_priv = {
  .mode = {
    .frames =	GGI_AUTO,
    .visible =	{ GGI_AUTO, GGI_AUTO },
    .virt =	{ GGI_AUTO, GGI_AUTO },
    .size =	{ GGI_AUTO, GGI_AUTO },
    .graphtype =	GT_AUTO,
    .dpp =	{ GGI_AUTO, GGI_AUTO },
  },
};

static struct ggiGA_resource frame_res = {
  .res_type = GA_RT_FRAME,
  .priv = &frame_priv,
  .priv_size = sizeof(struct ggiGA_mode),
  .cbflags1 = GA_CB_PRECHECK | GA_CB_CHECK | GA_CB_UNCHECK | GA_CB_RECHECK,
  .cb1 = x_galloc_check_mode,
  .cbflags2 = GA_CB_SET | GA_CB_UNSET | GA_CB_RELEASE,
  .cb2 = x_galloc_set_mode
};


/* This resource represents the ability of the X display target
 * to place a mouse pointer cursor on the screen.
 */
static struct ggiGA_resource_props ptr_motor_cap_props = {
  .qty = 1,	/* X supports only one mouse pointer cursor. */
  .flags = GA_FLAG_AUTOTRACK | GA_FLAG_ALWAYS_ON_TOP,
  .sub = {
    .motor = {	/* No scaling/pixeldoubling supported. */
      .mul_min = {1, 1},
      .mul_max = {1, 1},
      .div_min = {1, 1},
      .div_max = {1, 1},
      .rops = LL_ROP_SOURCE | LL_ROP_ALPHA_SOURCE
    }
  }
};

static struct ggiGA_resource ptr_motor_cap = {
  .res_type = GA_RT_SPRITE_POINTER | GA_RT_MOTOR,
  .res_state = GA_STATE_CAP | GA_STATE_SEEABOVE | 2,	/* Tag group 2 */
  .props = &ptr_motor_cap_props
};

static struct ggiGA_resource_props ptr_motor_props = {
  .qty = 1,	/* X supports only one mouse pointer cursor. */
  .flags = GA_FLAG_AUTOTRACK | GA_FLAG_ALWAYS_ON_TOP,
  .size = { .area = {1,1} },
  .sub = {
    .motor = {	/* No scaling/pixeldoubling supported. */
      .mul_min = {1, 1},
      .mul_max = {1, 1},
      .div_min = {1, 1},
      .div_max = {1, 1},
      .rops = LL_ROP_SOURCE | LL_ROP_ALPHA_SOURCE
    }
  },
};

static struct ggiGA_resource ptr_motor_res = {
  .res_type = GA_RT_SPRITE_POINTER | GA_RT_MOTOR,
  .res_state = 2,		/* Tag group 2 */
  .cbflags1 = GA_CB_PRECHECK,
  .cb1 = x_galloc_check_ptrmotor,
  .props = &ptr_motor_props
};

/* This resource represents the ability of LibGGI to draw primitives
 * on the main framebuffer.
 */
static struct ggiGA_resource_props drawops_motor_props = {
  .sub = {
    .motor = {	/* No scaling/pixeldoubling supported. */
      .mul_min = {1, 1},
      .mul_max = {1, 1},
      .div_min = {1, 1},
      .div_max = {1, 1},
    }
  },
};

static struct ggiGA_resource drawops_motor_res = {
  .res_type = GA_RT_RENDERER_DRAWOPS | GA_RT_MOTOR,
  .res_state = 1,		/* Tag group 1 */
  .props = &drawops_motor_props
};


static struct ggiGA_resource_listhead ga_x_haslist;


int x_galloc_InitHaslist(gallocpriv *priv)
{
	ggiGA_resource_list list = &ga_x_haslist;

	RESLIST_INIT(list);

	RESLIST_INSERT_HEAD(list, &drawops_motor_res);
	RESLIST_INSERT_TAIL(list, &ptr_motor_res);
	RESLIST_INSERT_TAIL(list, &ptr_motor_cap);
	RESLIST_INSERT_TAIL(list, &frame_res);
	RESLIST_INSERT_TAIL(list, &alphabuf_res);
	RESLIST_INSERT_TAIL(list, &zbuf_res);
	RESLIST_INSERT_TAIL(list, &drawcarb_res);
	RESLIST_INSERT_TAIL(list, &ptr_buf_res);
	RESLIST_INSERT_TAIL(list, &ptr_buf_cap);
	RESLIST_INSERT_TAIL(list, &ptr_abuf_res);
	RESLIST_INSERT_TAIL(list, &ptr_abuf_cap);
	RESLIST_INSERT_TAIL(list, &ptr_carb_res);


	/* Apply initial values */
	priv->haslist = list;

	return 0;
}


/******************************** callbacks *********************************/

int x_galloc_check_mode (ggi_visual_t vis,
			 ggiGA_resource_list reslist,
			 enum ggiGA_callback_command command,
			 ggiGA_resource_handle res,
			 ggiGA_resource_handle head,
			 ggiGA_taghead_t taghead,
			 ggiGA_resource_handle has,
			 struct ggiGA_template_state *state,
			 void *privstate) {
	ggiGA_resource_handle cap;
	ggi_mode mode;
	int rc;

	rc = GALLOC_OK;
	cap = NULL;
	if (ggiGAIsCap(RESLIST_NEXT(res))) cap = RESLIST_NEXT(res);

	fprintf(stderr, "x_galloc_check_mode called\n");

	if (command == GA_CB_UNCHECK) goto uncheck;

	if (command == GA_CB_PRECHECK) {
		ggi_mode *resmode, *maxmode, *minmode;
		maxmode = &(((struct galloc_x_state *)privstate)->maxmode);
		minmode = &(((struct galloc_x_state *)privstate)->minmode);

		printf("Mode res:   ");
		ggiPrintMode(ggiGAGetGGIMode(res));
		if (cap) {
		  printf(" cap ");
		  ggiPrintMode(ggiGAGetGGIMode(cap));
		}
		printf("\n");
		printf("Mode state:");
		ggiPrintMode(minmode);
		printf(" max ");
		ggiPrintMode(maxmode);
		printf("\n");

		resmode = (cap == NULL) ? 
		  ggiGAGetGGIMode(res) : ggiGAGetGGIMode(cap);

		/* We know pstate->maxmode won't have any GGI_AUTO values. */
		GA_AUTOLIMIT_M(resmode, >, maxmode, visible.x);
		GA_AUTOLIMIT_M(resmode, >, maxmode, visible.y);
		GA_AUTOLIMIT_M(resmode, >, maxmode, virt.x);
		GA_AUTOLIMIT_M(resmode, >, maxmode, virt.y);
		GA_AUTOLIMIT_M(resmode, >, maxmode, size.x);
		GA_AUTOLIMIT_M(resmode, >, maxmode, size.y);
		GA_AUTOLIMIT_M(resmode, >, maxmode, frames);
		if ((GT_DEPTH(resmode->graphtype) == GT_AUTO) ||
		    ((GT_DEPTH(resmode->graphtype) > 
		     GT_DEPTH(maxmode->graphtype)) &&
		     (rc = GALLOC_EFAILED))) {
			resmode->graphtype &= ~GT_DEPTH_MASK;
			resmode->graphtype |= GT_DEPTH(maxmode->graphtype);
		}

		resmode = ggiGAGetGGIMode(res);

		/* We know pstate->minmode < pstate->maxmode in all fields.
		 * Further, we know GGI_AUTO == 0 and that resmode no
		 * longer contains any GGI_AUTOs.
		 */
		GA_AUTOLIMIT_M(resmode, <, minmode, visible.x);
		GA_AUTOLIMIT_M(resmode, <, minmode, visible.y);
		GA_AUTOLIMIT_M(resmode, <, minmode, virt.x);
		GA_AUTOLIMIT_M(resmode, <, minmode, virt.y);
		GA_AUTOLIMIT_M(resmode, <, minmode, size.x);
		GA_AUTOLIMIT_M(resmode, <, minmode, size.y);
		GA_AUTOLIMIT_M(resmode, <, minmode, frames);
		if ((GT_DEPTH(resmode->graphtype) == GT_AUTO) ||
		    ((GT_DEPTH(resmode->graphtype) <
		     GT_DEPTH(minmode->graphtype)) &&
		     (rc = GALLOC_EFAILED))) {
			resmode->graphtype &= ~GT_DEPTH_MASK;
			resmode->graphtype |= GT_DEPTH(minmode->graphtype);
		}

		if (ggiGAModeCapSanity(res)) rc = GALLOC_EFAILED;

		printf("Mode res:   ");
		ggiPrintMode(ggiGAGetGGIMode(res));
		if (cap) {
		  printf(" cap ");
		  ggiPrintMode(ggiGAGetGGIMode(cap));
		}
		printf(" %s\n", rc ? "failed" : "passed");

		if (rc) DPRINT("Failed mode precheck\n");
		if (rc) return rc;
		goto newstate;
	}

	if (command == GA_CB_POSTCHECK) return 0;

	/* check and recheck are the same for us. */

	/* If we have a cap, try it first */
	if (!ggiGAIsCap(RESLIST_NEXT(res))) goto nocap;

	memcpy(&mode, ggiGAGetGGIMode(RESLIST_NEXT(res)), sizeof(ggi_mode));
	rc = ggiCheckMode(vis, &mode);

        /* Now we try again, using the adjusted values that came 
	 * back from ggiCheckMode. 
	 */
        if (rc != GALLOC_OK) rc = ggiCheckMode(vis, &mode);

	if (rc == GALLOC_OK) {
		ggi_mode *minmode;
		/* The cap succeeded.  But did it get a mode above the 
		 * minimum requirements in res?
		 */
		minmode = ggiGAGetGGIMode(res);
		rc |= (minmode->visible.x > mode.visible.x);
		rc |= (minmode->visible.y > mode.visible.y);
		rc |= (minmode->virt.x > mode.virt.x);
		rc |= (minmode->virt.y > mode.virt.y);
		rc |= (minmode->size.x > mode.size.x);
		rc |= (minmode->size.y > mode.size.y);
		rc |= (minmode->frames > mode.frames);
		rc |= (GT_DEPTH(minmode->graphtype) > 
		       GT_DEPTH(mode.graphtype));
		rc |= (GT_SCHEME(minmode->graphtype) != 
		       GT_SCHEME(mode.graphtype));
		if (rc != GALLOC_OK) rc = GALLOC_EFAILED;
                return rc;
        }       /* if */

	if (rc == GALLOC_OK) goto done;

 nocap:
	/* cap failed, so let's try the original resource. */
	memcpy(&mode, ggiGAGetGGIMode(res), sizeof(ggi_mode));
        rc = ggiCheckMode(vis, &mode);

	if (rc == GALLOC_OK) {
		ggiGAFlagModified(res);
		memcpy(ggiGAGetGGIMode(res), &mode, sizeof(ggi_mode));
		if (!ggiGAIsCap(RESLIST_NEXT(res))) return rc;
		ggiGAFlagModified(RESLIST_NEXT(res));
		memcpy(ggiGAGetGGIMode(RESLIST_NEXT(res)), &mode, sizeof(ggi_mode));
		return rc;
	}
	
        /* Now we try again, using the adjusted values that came 
	 * back from ggiCheckMode. 
	 */
	rc = ggiCheckMode(vis, &mode);
	if (rc != GALLOC_OK) {
		mode.visible.x = GGI_AUTO;
		mode.visible.y = GGI_AUTO;
		mode.virt.x = GGI_AUTO;
		mode.virt.y = GGI_AUTO;
		mode.frames = GGI_AUTO;
		mode.size.x = GGI_AUTO;
		mode.size.y = GGI_AUTO;
		mode.graphtype = GT_AUTO;
		rc = ggiCheckMode(vis, &mode);
	}


	if (rc == GALLOC_OK) {
		ggi_mode *minmode, *maxmode;

		/* We got something to succeed -- question is, what? */
		minmode = ggiGAGetGGIMode(res);
		rc |= (minmode->visible.x > mode.visible.x);
		rc |= (minmode->visible.y > mode.visible.y);
		rc |= (minmode->virt.x > mode.virt.x);
		rc |= (minmode->virt.y > mode.virt.y);
		rc |= (minmode->size.x > mode.size.x);
		rc |= (minmode->size.y > mode.size.y);
		rc |= (minmode->frames > mode.frames);
		rc |= (GT_DEPTH(minmode->graphtype) > 
		       GT_DEPTH(mode.graphtype));
		rc |= (GT_SCHEME(minmode->graphtype) != 
		       GT_SCHEME(mode.graphtype));

		if (!ggiGAIsCap(RESLIST_NEXT(res))) goto done;
		maxmode = ggiGAGetGGIMode(RESLIST_NEXT(res));
		rc |= (maxmode->visible.x < mode.visible.x);
		rc |= (maxmode->visible.y < mode.visible.y);
		rc |= (maxmode->virt.x < mode.virt.x);
		rc |= (maxmode->virt.y < mode.virt.y);
		rc |= (maxmode->size.x < mode.size.x);
		rc |= (maxmode->size.y < mode.size.y);
		rc |= (maxmode->frames < mode.frames);
		rc |= (GT_DEPTH(maxmode->graphtype) <
		       GT_DEPTH(mode.graphtype));
	}

 done:	  
	ggiGAFlagModified(res);
	memcpy(ggiGAGetGGIMode(res), &mode, sizeof(ggi_mode));
	if (!ggiGAIsCap(RESLIST_NEXT(res))) return rc;
	ggiGAFlagModified(RESLIST_NEXT(res));
	memcpy(ggiGAGetGGIMode(RESLIST_NEXT(res)), &mode, sizeof(ggi_mode));
	return rc;
	
 uncheck:
	return 2;

 newstate:
	state->mode = res;
	return GALLOC_OK;
}


int x_galloc_set_mode (ggi_visual_t vis,
			   ggiGA_resource_list reslist,
			   enum ggiGA_callback_command command,
			   ggiGA_resource_handle res,
			   ggiGA_resource_handle head,
			   ggiGA_taghead_t taghead,
			   ggiGA_resource_handle has,
			   struct ggiGA_template_state *state,
			   void *privstate)
{
	fprintf(stderr, "x_galloc_set_mode called\n");
	fprintf(stderr, "res_state = %x\n", res->res_state);
	if (!(res->res_state & GA_STATE_NORESET))
		return ggiSetMode(vis, ggiGAGetGGIMode(res));
	return GALLOC_OK;	/* Already set, nothing to do. */
}	/* x_galloc_set_mode */


/* Check for buffers that clad the visual -- they match the 
 * main mode, which is always their compound head right now, until
 * we start supporting floaters.
 */
int x_galloc_check_cladbuf (ggi_visual_t vis,
				ggiGA_resource_list reslist,
				enum ggiGA_callback_command command,
				ggiGA_resource_handle res,
				ggiGA_resource_handle head,
				ggiGA_taghead_t taghead,
				ggiGA_resource_handle has,
				struct ggiGA_template_state *state,
				void *privstate)
{

        struct ggiGA_resource_props *res_p;
	ggi_mode *mode;

	/* We probably don't even need these asserts, remove later. */
	LIB_ASSERT(ggiGA_TYPE(head) == GA_RT_FRAME,
		"must be frame resource");
	LIB_ASSERT(head->priv != NULL,
		"invalid priv data");

	/* For now, we should not be getting any rechecks. */
	LIB_ASSERT(command != GA_CB_RECHECK,
		"invalid command");

	/* Storage type checks were handled by precheck */

	if (res == head) {
		fprintf(stderr, "No support for floaters yet.\n");
		goto fail;
	}

	res_p = res->props;

	mode = ggiGAGetGGIMode(head);

	if ((res_p->size.area.x != GGI_AUTO) &&
	    (res_p->size.area.x != mode->virt.x)) goto fail;
	if ((res_p->size.area.y != GGI_AUTO) &&
	    (res_p->size.area.y != mode->virt.y)) goto fail;

	res_p->size.area.x = mode->virt.x;
	res_p->size.area.y = mode->virt.y;

	if (command != GA_CB_POSTCHECK) goto nomark;

	/* Here we would use the storage manager */

 nomark:
	return GALLOC_OK;
 fail:
	return GALLOC_EFAILED;
}	/* x_galloc_check_cladbuf */


