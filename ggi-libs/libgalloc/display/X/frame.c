/* $Id: frame.c,v 1.14 2005/10/12 12:44:45 cegger Exp $
******************************************************************************

   Galloc implementation for X target - frame functions.

   Copyright (C) 2001	Christoph Egger	[Christoph_Egger@t-online.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include <stdio.h>
#include <string.h>

#include "xgalloc.h"


/******************************************************
 *      Check FRAME
 */



int check_frame_motor(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* check_frame_motor */


int check_frame_carb(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* check_frame_carb */



int check_frame_tank(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	ggiGA_resource_handle cap = NULL;
	int rc = GALLOC_EFAILED;
	struct ggiGA_mode frame;
	struct ggiGA_mode *frame_p = NULL;

	int res_state;

	res_state = (handle->res_state & GA_STATE_REQUEST_MASK);


	/* Set coordbase */
	handle->cb = (GA_COORDBASE_UNIT & GA_FLAG_UNIT_MASK);
	handle->cb |= (GA_COORDBASE_PIXEL & GA_FLAG_COORDBASE_MASK);


	LIB_ASSERT(handle->priv != NULL, "no private data");
	LIB_ASSERT(handle->props == NULL, "invalid properties");
	frame_p = (struct ggiGA_mode *)handle->priv;

	LIB_ASSERT(frame_p != NULL, "frame invalid");

	/* Is there a cap? */
	if (!ggiGAIsCap(handle->next)) {
		rc = ggiCheckMode(vis, &(frame_p->mode));

		if (rc != GGI_OK) {
			ggiGAFlagModified(handle);
			rc = GALLOC_OK;
		}	/* if */

		goto exit;
	}	/* if */

	cap = handle->next;
	memcpy(&frame, cap->priv, sizeof(struct ggiGA_mode));
	if (ggiCheckMode(vis, &(frame.mode)) == GGI_OK) {
		/* Lucky us. */
		rc = GALLOC_OK;
		goto finish;
	}	/* if */

	/* Now we try again, using the adjusted values that
	 * came back from ggiCheckMode.
	 */
	if (ggiCheckMode(vis, &(frame.mode)) == GGI_OK) {
		/* Lucky us. */
		rc = GALLOC_OK;
		goto finish;
	}	/* if */

	/* TODO: Now what to try?? */

finish:
	/* If any of these assertions fail for a target, we should
	 * consider doing a better job above.
	 */
	frame_p = (struct ggiGA_mode *)cap->priv;

	LIB_ASSERT(frame.mode.visible.x <= frame_p->mode.visible.x ||
		   frame_p->mode.visible.x == GGI_AUTO,
		"mode.visible.x out of range");
	LIB_ASSERT(frame.mode.visible.y <= frame_p->mode.visible.y ||
		   frame_p->mode.visible.y == GGI_AUTO,
		"mode.visible.y out of range");
	LIB_ASSERT(frame.mode.virt.x <= frame_p->mode.virt.x ||
		   frame_p->mode.virt.x == GGI_AUTO,
		"mode.virt.x out of range");
	LIB_ASSERT(frame.mode.virt.y <= frame_p->mode.virt.y ||
		   frame_p->mode.virt.y == GGI_AUTO,
		"mode.virt.y out of range");
	LIB_ASSERT(GT_DEPTH(frame.mode.graphtype) <=
		   GT_DEPTH(frame_p->mode.graphtype) ||
		   frame_p->mode.graphtype == GT_AUTO,
		"graphtype out of range");

	frame_p = (struct ggiGA_mode *)handle->priv;
	LIB_ASSERT(frame.mode.visible.x >= frame_p->mode.visible.x ||
		   frame_p->mode.visible.x == GGI_AUTO,
		"mode.visible.x out of range");
	LIB_ASSERT(frame.mode.visible.y >= frame_p->mode.visible.y ||
		   frame_p->mode.visible.y == GGI_AUTO,
		"mode.visible.y out of range");
	LIB_ASSERT(frame.mode.virt.x >= frame_p->mode.virt.x ||
		   frame_p->mode.virt.x == GGI_AUTO,
		"mode.virt.x out of range");
	LIB_ASSERT(frame.mode.virt.y >= frame_p->mode.virt.y ||
		   frame_p->mode.virt.y == GGI_AUTO,
		"mode.virt.y out of range");
	LIB_ASSERT(GT_DEPTH(frame.mode.graphtype) >=
		   GT_DEPTH(frame_p->mode.graphtype) ||
		   frame_p->mode.graphtype == GT_AUTO,
		"graphtype out of range");

	memcpy(handle->priv, &frame, sizeof(frame));
	ggiGAFlagModified(handle);


exit:
	LIB_ASSERT(handle->priv != NULL, "invalid private structure");
	rc = GALLOC_OK;
	return rc;

err0:
	handle->res_state &= ~GA_STATE_MODIFIED;
	ggiGAFlagFailed(handle);
	LIB_ASSERT(handle->priv != NULL, "invalid private structure");

	return rc;
}	/* check_frame_tank */


int check_frame(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	if (ggiGAIsMotor(handle)) {
		return check_frame_motor(vis, handle, compound, lastmode);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return check_frame_carb(vis, handle, compound, lastmode);
	}	/* if */


	return check_frame_tank(vis, handle, compound, lastmode);
}	/* check_frame */













/******************************************************
 *      Set FRAME
 */

int set_frame_motor(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* set_frame_motor */


int set_frame_carb(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* set_frame_carb */



int set_frame_tank(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	struct ggiGA_mode *frame;
	int rc = GALLOC_EFAILED;


	frame = (struct ggiGA_mode *)handle->priv;

	rc = ggiSetMode(vis, &(frame->mode));
	if (rc != GALLOC_OK) {
		ggiGAFlagFailed(handle);
		rc = GALLOC_EFAILED;
		goto exit;
	}	/* if */
	rc = GALLOC_OK;

exit:
	return rc;
}	/* set_frame_tank */


int set_frame(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	if (ggiGAIsMotor(handle)) {
		return set_frame_motor(vis, handle, compound, lastmode);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return set_frame_carb(vis, handle, compound, lastmode);
	}	/* if */


	return set_frame_tank(vis, handle, compound, lastmode);
}	/* set_frame */















/******************************************************
 *      Release FRAME
 */


int release_frame_motor(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* release_frame_motor */


int release_frame_carb(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* release_frame_carb */


int release_frame_tank(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	/* nothing todo here!
	 * The caller has to use ggiClose() instead.
	 */

	return GALLOC_OK;
}	/* release_frame_tank */



int release_frame(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	if (ggiGAIsMotor(handle)) {
		return release_frame_motor(vis, handle);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return release_frame_carb(vis, handle);
	}	/* if */


	return release_frame_tank(vis, handle);
}	/* release_frame */

