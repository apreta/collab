/* $Id: misc.c,v 1.9 2005/10/11 19:26:43 cegger Exp $
******************************************************************************

   Galloc implementation for X target - misc functions.

   Copyright (C) 2001	Christoph Egger	[Christoph_Egger@t-online.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include <stdio.h>

#include "xgalloc.h"


/******************************************************
 *      Check MISC
 */



int check_misc_motor(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* check_misc_motor */


int check_misc_carb(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* check_misc_carb */



int check_misc_tank(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	int rc = GALLOC_EUNAVAILABLE;

	LIB_ASSERT(handle != NULL, "handle == NULL");


	switch (handle->res_type & GA_RT_SUBTYPE_MASK) {
	case (GA_RT_MISC_DONTCARE & GA_RT_SUBTYPE_MASK):

		/* Oops! Don't know, what to do here. So fail!
		 */
		rc = GALLOC_EUNAVAILABLE;
		ggiGAFlagFailed(handle);
		break;

	case (GA_RT_MISC_RAYPOS & GA_RT_SUBTYPE_MASK):

		/* Not supported by X. So fail!
		 */
		rc = GALLOC_EUNAVAILABLE;
		ggiGAFlagFailed(handle);
		break;

	case (GA_RT_MISC_SPLITLINE & GA_RT_SUBTYPE_MASK):

		/* Set coordbase */
		handle->cb = (GA_COORDBASE_UNIT & GA_FLAG_UNIT_MASK);
		handle->cb |= (GA_COORDBASE_PIXEL & GA_FLAG_COORDBASE_MASK);

		/* Lets do libggimisc the rest */

		rc = GALLOC_OK;
		ggiGAFlagActive(handle);
		break;

	case (GA_RT_MISC_FONTCELL & GA_RT_SUBTYPE_MASK):

		/* FIXME: Feel free to change this, when you
		 * know, that X supports this!
		 */

#if 0
		/* Feel free to correct this */

		/* Set coordbase */
		handle->cb = (GA_COORDBASE_UNIT & GA_FLAG_UNIT_MASK);
		handle->cb |= (GA_COORDBASE_PIXEL & GA_FLAG_COORDBASE_MASK);
#endif

		rc = GALLOC_EUNAVAILABLE;
		ggiGAFlagFailed(handle);
		break;

	}	/* switch */

	return rc;
}	/* check_misc_tank */


int check_misc(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	if (ggiGAIsMotor(handle)) {
		return check_misc_motor(vis, handle, compound, lastmode);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return check_misc_carb(vis, handle, compound, lastmode);
	}	/* if */


	return check_misc_tank(vis, handle, compound, lastmode);
}	/* check_misc */













/******************************************************
 *      Set MISC
 */

int set_misc_motor(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* set_misc_motor */


int set_misc_carb(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* set_misc_carb */



int set_misc_tank(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	int rc = GALLOC_EFAILED;

	LIB_ASSERT(handle != NULL, "handle == NULL");


	switch (handle->res_type & GA_RT_SUBTYPE_MASK) {
	case (GA_RT_MISC_DONTCARE & GA_RT_SUBTYPE_MASK):

		/* Oops! Don't know, what to do here. So fail!
		 */
		rc = GALLOC_EUNAVAILABLE;
		ggiGAFlagFailed(handle);
		break;

	case (GA_RT_MISC_RAYPOS & GA_RT_SUBTYPE_MASK):

		/* Not supported by X. So fail!
		 */
		rc = GALLOC_EUNAVAILABLE;
		ggiGAFlagFailed(handle);
		break;

	case (GA_RT_MISC_SPLITLINE & GA_RT_SUBTYPE_MASK):

		/* Nothing todo here. Just
		 * let's handle it libggmisc
		 */
		rc = GALLOC_OK;
		ggiGAFlagActive(handle);
		break;

	case (GA_RT_MISC_FONTCELL & GA_RT_SUBTYPE_MASK):

		/* FIXME: Feel free to change this, when you
		 * know, that X supports this!
		 */
		rc = GALLOC_EUNAVAILABLE;
		ggiGAFlagFailed(handle);
		break;

	}	/* switch */

	return rc;
}	/* set_misc_tank */


int set_misc(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	if (ggiGAIsMotor(handle)) {
		return set_misc_motor(vis, handle, compound, lastmode);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return set_misc_carb(vis, handle, compound, lastmode);
	}	/* if */


	return set_misc_tank(vis, handle, compound, lastmode);
}	/* set_misc */















/******************************************************
 *      Release MISC
 */


int release_misc_motor(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* release_misc_motor */


int release_misc_carb(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* release_misc_carb */



int release_misc_tank(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	int rc = GALLOC_OK;

	LIB_ASSERT(handle != NULL, "handle == NULL");


	switch (handle->res_type & GA_RT_SUBTYPE_MASK) {
	case (GA_RT_MISC_DONTCARE & GA_RT_SUBTYPE_MASK):

		/* Oops! Don't know, what to do here. So fail!
		 */
		rc = GALLOC_EUNAVAILABLE;
		ggiGAFlagFailed(handle);
		break;

	case (GA_RT_MISC_RAYPOS & GA_RT_SUBTYPE_MASK):

		/* Not supported by X. So fail!
		 */
		rc = GALLOC_EUNAVAILABLE;
		ggiGAFlagFailed(handle);
		break;

	case (GA_RT_MISC_SPLITLINE & GA_RT_SUBTYPE_MASK):

		/* Nothing todo here. Just
		 * let's handle it libggmisc
		 */
		rc = GALLOC_OK;
		ggiGAFlagActive(handle);
		break;

	case (GA_RT_MISC_FONTCELL & GA_RT_SUBTYPE_MASK):

		/* FIXME: Feel free to change this, when you
		 * know, that X supports this!
		 */
		rc = GALLOC_EUNAVAILABLE;
		ggiGAFlagFailed(handle);
		break;

	}	/* switch */

	return rc;
}	/* release_misc_tank */


int release_misc(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	if (ggiGAIsMotor(handle)) {
		return release_misc_motor(vis, handle);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return release_misc_carb(vis, handle);
	}	/* if */


	return release_misc_tank(vis, handle);
}	/* release_misc */
