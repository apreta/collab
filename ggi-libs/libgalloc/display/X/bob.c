/* $Id: bob.c,v 1.21 2005/10/12 12:44:45 cegger Exp $
******************************************************************************

   Galloc implementation for X target - bob functions.

   Copyright (C) 2001	Christoph Egger	[Christoph_Egger@t-online.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include <stdio.h>

#include "xgalloc.h"


/******************************************************
 *      Check BOB
 */



int check_bob_motor(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}       /* check_bob_motor */


int check_bob_carb(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* check_bob_carb */




int check_bob_tank(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	ggiGA_resource_handle cap = NULL;
	ggi_xwin_common *priv = NULL;
	struct GA_x_bob *bob = NULL;

	int rc = GALLOC_EUNAVAILABLE;
	int res_state;
	int storage_need;

	int16_t *handle_width, *handle_height;
	ggi_graphtype *handle_depth;
	int maxwidth, maxheight, maxdepth;


	LIB_ASSERT(handle != NULL, "handle == NULL");

	priv = LIBGGI_PRIVATE(vis);

	res_state = (handle->res_state & GA_STATE_REQUEST_MASK);
	storage_need = (handle->props->storage_need & GA_STORAGE_STORAGE_MASK);


	if (handle->priv == NULL) {
		LIB_ASSERT(sizeof(struct GA_x_bob) > 0, "bogus structure");
		bob = calloc(1, sizeof(struct GA_x_bob));
		if (!bob) {
			rc = GGI_ENOMEM;
			goto err0;
		}	/* if */

		handle->priv = (void *)bob;
		handle->priv_size = sizeof(struct GA_x_bob);
	} else {
		bob = (struct GA_x_bob *)handle->priv;
	}	/* if */

	LIB_ASSERT(bob != NULL, "couldn't allocate bob");


	if ( (storage_need & GA_STORAGE_NOVIRT) ||
	   ( !(storage_need & GA_STORAGE_VIRT)
	   && (storage_need & ~GA_STORAGE_DONTCARE) != GA_STORAGE_DONTCARE))
	{
		rc = GALLOC_EFAILED;
		goto err0;
	}	/* if */

	/* Set coordbase */
	handle->cb = (GA_COORDBASE_UNIT & GA_FLAG_UNIT_MASK);
	handle->cb |= (GA_COORDBASE_PIXEL & GA_FLAG_COORDBASE_MASK);


	/* Get maximum size the resource can have
	 */
	if (lastmode != NULL) {
		struct ggiGA_mode *ga_mode = NULL;

		ga_mode = (struct ggiGA_mode *)lastmode->priv;

		maxwidth = ga_mode->mode.visible.x;
		maxheight = ga_mode->mode.visible.y;
		maxdepth = ga_mode->mode.graphtype;

		LIB_ASSERT(maxwidth != GGI_AUTO, "maxwidth == GGI_AUTO");
		LIB_ASSERT(maxheight != GGI_AUTO, "maxheight == GGI_AUTO");
		LIB_ASSERT(maxdepth != GT_AUTO, "maxdepth == GT_AUTO");

		LIB_ASSERT((maxwidth * maxheight * GT_ByPP(maxdepth)) > 0,
			"maxwidth * maxheight * GT_ByPP(maxdepth) == 0");

		DPRINT_TARGET("%s:%s:%i: maxwidth, maxheight, maxdepth (%i,%i,%i)\n",
			DEBUG_INFO, maxwidth, maxheight, GT_ByPP(maxdepth));
	} else {
		/* maxsize */
		maxwidth = priv->defsize.x;
		maxheight = priv->defsize.y;

		/* FIXME: Is "visual.depth" in the same format as
		 * "mode.graphtype" ?
		 * If not, then we must convert it, otherwise you
		 * get a wrong behaviour here.
		 */
		/* FIXME: Handle GT_TEXT16 and GT_TEXT32 here as well */
		switch (priv->visual.depth) {
		case 1:
			maxdepth = GT_1BIT;
			break;
		case 2:
			maxdepth = GT_2BIT;
			break;
		case 4:
			maxdepth = GT_4BIT;
			break;
		case 8:
			maxdepth = GT_8BIT;
			break;
		case 15:
			maxdepth = GT_15BIT;
			break;
		case 16:
			maxdepth = GT_16BIT;
			break;
		case 24:
			maxdepth = GT_24BIT;
			break;
		case 32:
			maxdepth = GT_32BIT;
			break;
		default:
			maxdepth = 0;
			rc = GALLOC_EFAILED;
			goto err0;
		}	/* switch */

		LIB_ASSERT(maxwidth != GGI_AUTO, "maxwidth == GGI_AUTO");
		LIB_ASSERT(maxheight != GGI_AUTO, "maxheight == GGI_AUTO");
		LIB_ASSERT(maxdepth != GT_AUTO, "maxdepth == GT_AUTO");

		LIB_ASSERT((maxwidth * maxheight * GT_SIZE(maxdepth)) > 0,
			"maxwidth * maxheight * GT_SIZE(maxdepth) == 0");
		LIB_ASSERT((maxwidth * maxheight * GT_ByPP(maxdepth)) > 0,
			"maxwidth * maxheight * GT_ByPP(maxdepth)");

		DPRINT_TARGET("%s:%s:%i: maxwidth, maxheight, maxdepth (%i,%i,%i)\n",
			DEBUG_INFO, maxwidth, maxheight, GT_SIZE(maxdepth));
	}	/* if */



	/* Is there a CAP ?
	 */
	if (ggiGAIsCap(handle->next)) {
		cap = handle->next;

		GA_CHECK_CAPAUTO_2D(cap, maxwidth, maxheight, maxdepth);
	}	/* if */




	/* Get requested size
	 */
	handle_width = &(handle->props->size.area.x);
	handle_height = &(handle->props->size.area.y);
	handle_depth = &(handle->props->sub.tank.gt);


	/* Check GGI_AUTO's */
	GA_CHECK_GGIAUTO_2D(handle, maxwidth, maxheight, maxdepth);



	if (handle_width[0] > maxwidth) {
		handle_width[0] = maxwidth;
		ggiGAFlagModified(handle);
	}	/* if */
	if (handle_height[0] > maxheight) {
		handle_height[0] = maxheight;
		ggiGAFlagModified(handle);
	}	/* if */
	if (handle_depth[0] > maxdepth) {
		handle_depth[0] = maxdepth;
		ggiGAFlagModified(handle);
	}	/* if */

	LIB_ASSERT((handle_width[0] * handle_height[0] *
		GT_ByPP(handle_depth[0])) > 0,
		"handle_width * handle_height * handle_depth == 0");




#define GA_CHECK_BOB(exit, error)					\
									\
bob->bobimage = malloc(handle_width[0] * handle_height[0] *		\
			GT_ByPP(handle_depth[0]));			\
if (!bob->bobimage) {							\
	rc = GGI_ENOMEM;						\
} else {								\
	free(bob->bobimage);						\
	bob->bobimage = NULL;						\
	rc = GALLOC_OK;							\
}	/* if */

	GA_CHECK_BOB(exit, err0);

#undef GA_CHECK_BOB


	if (rc < GALLOC_OK) goto err0;
	goto exit;




exit:
	handle->storage_ok = GA_STORAGE_VIRT;
	handle->storage_share |= GA_SHARE_SHAREABLE;
	handle->props->storage_ok = handle->storage_ok;
	LIB_ASSERT(handle->priv != NULL, "handle has no private structure");
	rc = GALLOC_OK;
	return rc;

err0:
	handle->res_state &= ~GA_STATE_MODIFIED;
	ggiGAFlagFailed(handle);
	LIB_ASSERT(handle->priv != NULL, "handle has no private structure");
	return rc;
}	/* check_bob_tank */


int check_bob(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	if (ggiGAIsMotor(handle)) {
		return check_bob_motor(vis, handle, compound, lastmode);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return check_bob_carb(vis, handle, compound, lastmode);
	}	/* if */

	return check_bob_tank(vis, handle, compound, lastmode);
}	/* check_bob */












/******************************************************
 *      Set BOB
 */

int set_bob_motor(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}       /* set_bob_motor */


int set_bob_carb(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* set_bob_carb */




int set_bob_tank(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	ggi_xwin_common *priv = NULL;
	struct GA_x_bob *bob = NULL;
	int rc = GALLOC_EFAILED;

	int width, height, depth;

	LIB_ASSERT(handle != NULL, "handle == NULL");
	LIB_ASSERT(handle->priv != NULL, "handle has no private data");


	priv = LIBGGI_PRIVATE(vis);

	if (handle->priv == NULL) {
		goto err0;
	}	/* if */

	width = handle->props->size.area.x;
	height = handle->props->size.area.y;
	depth = GT_ByPP(handle->props->sub.tank.gt);

	LIB_ASSERT((width * height * depth) > 0,
		"width * height * depth == 0");

	DPRINT_TARGET("%s:%s:%i: width (%i), height (%i), depth (%i, %i)\n",
		DEBUG_INFO, width, height,
		GT_SIZE(handle->props->sub.tank.gt), depth);

	bob = (struct GA_x_bob *)handle->priv;
	LIB_ASSERT(bob != NULL, "invalid bob");

	bob->bobimage = calloc(1, width * height * depth);
	if (!bob->bobimage) goto err0;

	DPRINT_TARGET("%s:%s:%i: bob %p, bobimage %p\n",
		DEBUG_INFO, bob, bob->bobimage);

#if 0
	/* This belongs into Xlib!! */
	bobaddr = calloc(1, (width * height * depth);
	if (!bobaddr) goto err0;

	bob->bobimage = XCreateImage(priv->x.display,
				  priv->visual.visual,
				  priv->visual.depth,
				  ZPixmap,	/* format */
				  0,		/* offset */
				  bobaddr,	/* data */
				  width, height,	/* size */
				  8,		/* bitmap_pad */
				  0);		/* bytes_per_line */
#ifdef GGI_LITTLE_ENDIAN
	bob->bobimage->byte_order = LSBFirst;
	bob->bobimage->bitmap_bit_order = LSBFirst;
#else
	bob->bobimage->byte_order = MSBFirst;
	bob->bobimage->bitmap_bit_order = MSBFirst;
#endif

#endif


	rc = GALLOC_OK;


	if (rc != GALLOC_OK) {
		goto err1;
	}	/* if */

	return rc;

err1:
	free(bob->bobimage);
	free(bob);
	handle->priv_size = 0;
	handle->priv = NULL;
err0:
	rc = GALLOC_EFAILED;
	ggiGAFlagFailed(handle);
	return rc;
}	/* set_bob_tank */


int set_bob(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	if (ggiGAIsMotor(handle)) {
		return set_bob_motor(vis, handle, compound, lastmode);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return set_bob_carb(vis, handle, compound, lastmode);
	}	/* if */

	return set_bob_tank(vis, handle, compound, lastmode);
}	/* set_bob */












/******************************************************
 *	Release BOB
 */

int release_bob_motor(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* release_bob_motor */


int release_bob_carb(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* release_bob_carb */



int release_bob_tank(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	struct GA_x_bob *bob = NULL;
	int rc = GALLOC_OK;

	LIB_ASSERT(handle != NULL, "handle == NULL");

	if (handle->priv == NULL) {
		/* never allocated */
		rc = GALLOC_OK;
		goto exit;
	}	/* if */

	bob = (struct GA_x_bob *)handle->priv;

	free(bob->bobimage);
	free(bob);
	handle->priv_size = 0;
	handle->priv = NULL;

exit:
	return rc;
}	/* release_bob_tank */


int release_bob(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	if (ggiGAIsMotor(handle)) {
		return release_bob_motor(vis, handle);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return release_bob_carb(vis, handle);
	}	/* if */


	return release_bob_tank(vis, handle);
}	/* release_bob */

