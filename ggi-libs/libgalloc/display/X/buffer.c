/* $Id: buffer.c,v 1.29 2005/10/12 12:44:45 cegger Exp $
******************************************************************************

   Galloc implementation for X target - buffer functions.

   Copyright (C) 2001	Christoph Egger	[Christoph_Egger@t-online.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#include <stdio.h>
#include <string.h>

#include "xgalloc.h"



/******************************************************
 *      Check Sprite
 */



int check_buffer_motor(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* check_buffer_motor */


int check_buffer_carb(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* check_buffer_carb */




int check_buffer_tank(ggi_visual_t vis,
		 ggiGA_resource_handle handle,
		 ggiGA_resource_handle compound,
		 ggiGA_resource_handle lastmode)
{
	ggiGA_resource_handle cap = NULL;
	ggi_xwin_common *priv = NULL;
	struct GA_x_buffer *buffer = NULL;
	ggi_mode mode;

	int16_t *width = NULL, *height = NULL;
	uint32_t *linear_size = NULL;
	ggi_graphtype *depth = NULL;
	int maxwidth, maxheight, maxsize, maxdepth;

	int dbnum;
	int res_state;
	int storage_need, storage_ok;

	int rc = GALLOC_EFAILED;


	LIB_ASSERT(handle != NULL, "handle == NULL");
	priv = LIBGGI_PRIVATE(vis);

	res_state = (handle->res_state & GA_STATE_REQUEST_MASK);
	storage_need = (handle->props->storage_need & GA_STORAGE_REQUEST_MASK);
	storage_ok = (handle->props->storage_ok & GA_STORAGE_REQUEST_MASK);


	if (handle->priv == NULL) {
		LIB_ASSERT(sizeof(struct GA_x_buffer) > 0, "bogus structure");
		buffer = malloc(sizeof(struct GA_x_buffer));
		if (!buffer) {
			/* not enough memory available */
			goto err0;
		}	/* if */
		handle->priv = (void *)buffer;
		handle->priv_size = sizeof(struct GA_x_buffer);
	} else {
		buffer = (struct GA_x_buffer *)handle->priv;
	}	/* if */

	LIB_ASSERT(buffer != NULL, "couldn't allocate buffer");


	/* Set coordbase */
	handle->cb = (GA_COORDBASE_UNIT & GA_FLAG_UNIT_MASK);
	handle->cb |= (GA_COORDBASE_PIXEL & GA_FLAG_COORDBASE_MASK);


	switch (handle->res_type & GA_RT_SUBTYPE_MASK) {
	case (GA_RT_BUFFER_DONTCARE & GA_RT_SUBTYPE_MASK):

	case (GA_RT_BUFFER_RAW & GA_RT_SUBTYPE_MASK):

	/* X seems to handle all buffer-types as raw-buffers.
	 * That's good as we don't have to care about them.
	 */
	case (GA_RT_BUFFER_ZBUFFER & GA_RT_SUBTYPE_MASK):
	case (GA_RT_BUFFER_ABUFFER & GA_RT_SUBTYPE_MASK):
	case (GA_RT_BUFFER_SBUFFER & GA_RT_SUBTYPE_MASK):
	case (GA_RT_BUFFER_TBUFFER & GA_RT_SUBTYPE_MASK):

		if (storage_need & GA_STORAGE_RAM) goto buffer_err;

		/* Get maximum size the resource can have
		 */
		if (lastmode != NULL) {
			struct ggiGA_mode *ga_mode;

			ga_mode = ((struct ggiGA_mode *)lastmode->priv);
			memcpy(&mode, &ga_mode->mode, sizeof(ggi_mode));

			maxwidth = mode.virt.x;
			maxheight = mode.virt.y;

		} else {
			ggiGetMode(vis, &mode);
			maxwidth = mode.virt.x;
			maxheight = mode.virt.y;
		}	/* if */

		/* The supported storages GA_STORAGE_SECONDARY,
		 * GA_STORAGE_VIRT and GA_STORAGE_VRAM supports
		 * GT_32BIT, so we can hardcode this here.
		 */
		maxdepth = GT_32BIT;



		/* Is there a CAP ?
		 */
		if (ggiGAIsCap(handle->next)) {
			cap = handle->next;

			GA_CHECK_CAPAUTO_2D(cap,
				maxwidth, maxheight, maxdepth);
		}	/* if */



		/* Get requested size
		 */
		width = &(handle->props->size.area.x);
		height = &(handle->props->size.area.y);
		depth = &(handle->props->sub.tank.gt);

		if ((GA_FLAG(handle->props) & GA_FLAG_COORDBASE_MASK)
		   == GA_COORDBASE_DOTS)
		{
			ggi_coord dot, pixel;

			dot.x = width[0]; dot.y = height[0];
			ggiGADot2Pixel(vis, dot, &pixel);

#warning Perform snapping to grid here

			width[0] = pixel.x; height[0] = pixel.y;
		}	/* if */

		LIB_ASSERT(width[0] >= 0, "width < 0");
		LIB_ASSERT(height[0] >= 0, "height < 0");
		LIB_ASSERT(depth[0] >= 0, "depth < 0");

		DPRINT_TARGET("%s:%s:%i: "
			"handle->props->size.area.x (%i),\n"
			"handle->props->size.area.y (%i),\n"
			"handle->props->size.linear (%i),\n"
			"handle->props->sub.tank.gt (%i,%i)\n",
			DEBUG_INFO,
			handle->props->size.area.x,
			handle->props->size.area.y,
			handle->props->size.linear,
			GT_SIZE(handle->props->sub.tank.gt),
			GT_ByPP(handle->props->sub.tank.gt));

		if ((width[0] < 0)
		   || (height[0] < 0)
		   || (GT_ByPP(depth[0]) < 0))
		{
			/* either user error or overflow bug */
			goto buffer_err;
		}	/* if */

		LIB_ASSERT(width[0] >= 0, "width < 0");
		LIB_ASSERT(height[0] >= 0, "height < 0");
		LIB_ASSERT(depth[0] >= 0, "depth < 0");

		/* Check GGI_AUTO's */
		GA_CHECK_GGIAUTO_2D(handle, maxwidth, maxheight, maxdepth);

		LIB_ASSERT(width[0] > 0, "width <= 0");
		LIB_ASSERT(height[0] > 0, "height <= 0");
		LIB_ASSERT(depth[0] > 0, "depth <= 0");




		if (width[0] > maxwidth) {
			width[0] = maxwidth;
			ggiGAFlagModified(handle);
		}	/* if */
		if (height[0] > maxheight) {
			height[0] = maxheight;
			ggiGAFlagModified(handle);
		}	/* if */
		if (depth[0] > maxdepth) {
			depth[0] = maxdepth;
			ggiGAFlagModified(handle);
		}	/* if */

		LIB_ASSERT((width[0] * height[0] *
			GT_ByPP(depth[0])) > 0,
			"width * height * depth == 0");




#define GA_CHECK_BUFFER(exit, error)				\
								\
{								\
int try = 0;							\
								\
if (storage_need != GA_STORAGE_DONTCARE) try = 2;		\
								\
for (; try <= 2; try++) {					\
								\
if (  (((storage_need & GA_STORAGE_STORAGE_MASK)		\
	== GA_STORAGE_DONTCARE) && (try == 0))			\
   || (storage_need & GA_STORAGE_VRAM) )			\
{								\
	if (storage_need == GA_STORAGE_NOVRAM) {		\
		if ((storage_need & GA_STORAGE_STORAGE_MASK)	\
			== GA_STORAGE_DONTCARE)			\
		{						\
			continue;				\
		} else {					\
			rc = GALLOC_EFAILED;			\
			goto error;				\
		}	/* if */				\
	}	/* if */					\
								\
	DPRINT_TARGET("%s:%s:%i: try GA_STORAGE_VRAM\n",	\
			DEBUG_INFO);				\
	dbnum = ggiDBGetNumBuffers(vis);			\
	if (dbnum == 0) {					\
		DPRINT_TARGET("%s:%s:%i: no directbuffer available\n",	\
			DEBUG_INFO);				\
		/* no directbuffer available */			\
		if ((storage_need & GA_STORAGE_STORAGE_MASK)	\
		   == GA_STORAGE_DONTCARE)			\
		{						\
			continue;	/* Try next */		\
		}	/* if */				\
								\
		rc = GALLOC_EFAILED;				\
		goto error;					\
	}	/* if */					\
								\
	/* Directbuffer has always the same size as		\
	 * the video-mode.					\
	 */							\
	if (width[0] != maxwidth) rc = GALLOC_EFAILED;		\
	if (height[0] != maxheight) rc = GALLOC_EFAILED;	\
								\
	if (rc < GALLOC_OK) {					\
		DPRINT_TARGET("%s:%s:%i: directbuffer alloation failed\n",	\
			DEBUG_INFO);				\
		if ((storage_need & GA_STORAGE_STORAGE_MASK)	\
			== GA_STORAGE_DONTCARE)			\
		{						\
			continue;	/* Try next */		\
		}	/* if */				\
								\
		goto error;					\
	}	/* if */					\
								\
	handle->storage_share |= GA_SHARE_SHAREABLE;		\
	handle->storage_ok =					\
			(GA_STORAGE_DIRECT | GA_STORAGE_VRAM);	\
	goto exit;						\
}	/* if */						\
								\
								\
if ( (((storage_need & GA_STORAGE_STORAGE_MASK)			\
	== GA_STORAGE_DONTCARE) && (try == 1))			\
   || (storage_need & GA_STORAGE_VIRT) )			\
{								\
	if (storage_need == GA_STORAGE_NOVIRT) {		\
		if ((storage_need & GA_STORAGE_STORAGE_MASK)	\
			== GA_STORAGE_DONTCARE)			\
		{						\
			continue;				\
		} else {					\
			rc = GALLOC_EFAILED;			\
			goto error;				\
		}	/* if */				\
	}	/* if */					\
								\
	DPRINT_TARGET("%s:%s:%i: try GA_STORAGE_VIRT - size %ix%ix%i\n",	\
		DEBUG_INFO, width[0], height[0], GT_ByPP(depth[0]));	\
	buffer->buf = malloc(width[0] * height[0] *		\
			 GT_ByPP(depth[0]));			\
	if (!buffer->buf) {					\
		DPRINT_TARGET("%s:%s:%i: not enough memory\n",	\
			DEBUG_INFO);				\
								\
		/* not enough memory available */		\
		if ((storage_need & GA_STORAGE_STORAGE_MASK)	\
			== GA_STORAGE_DONTCARE)			\
		{						\
			buffer->buf = NULL;			\
			continue;	/* Try next */		\
		}	/* if */				\
								\
		rc = GGI_ENOMEM;				\
		goto error;					\
	}	/* if */					\
								\
	free(buffer->buf);					\
	buffer->buf = NULL;					\
								\
	rc = GALLOC_OK;						\
	handle->storage_share |= GA_SHARE_SHAREABLE;		\
	handle->storage_ok = GA_STORAGE_VIRT;			\
	goto exit;						\
}	/* if */						\
								\
								\
if ( (((storage_need & GA_STORAGE_STORAGE_MASK)			\
	== GA_STORAGE_DONTCARE) && (try == 2))			\
   || (storage_need & GA_STORAGE_SECONDARY) )			\
{								\
	/* int fd; */						\
								\
	if (storage_need == GA_STORAGE_NOSECONDARY) {		\
		if ((storage_need & GA_STORAGE_STORAGE_MASK)	\
			== GA_STORAGE_DONTCARE)			\
		{						\
			continue;				\
		} else {					\
			rc = GALLOC_EFAILED;			\
			goto error;				\
		}	/* if */				\
	}	/* if */					\
								\
	DPRINT_TARGET("%s:%s:%i: try GA_STORAGE_SECONDARY\n",	\
			DEBUG_INFO);				\
	/* Should we emulate MMIO-access by using mmap()	\
	 * or should we use a filedescriptor directly?		\
	 */							\
	/* if (!buffer->filename) goto error; */		\
	/*							\
	fd = open(buffer->filename,				\
		O_CREAT | O_TRUNC | O_RDWR, 0600);		\
	if (fd < 0) goto fail_dontcare;				\
								\
	close(fd);						\
								\
	rc = GALLOC_OK;						\
	handle->storage_ok = GA_STORAGE_SECONDARY;		\
	goto exit;						\
	*/							\
	DPRINT_TARGET("%s:%s:%i: try GA_STORAGE_SECONDARY not implemented\n",	\
			DEBUG_INFO);				\
	rc = GALLOC_EFAILED;					\
}	/* if */						\
								\
}	/* for */						\
}



		GA_CHECK_BUFFER(buffer_exit, buffer_err);

#undef GA_CHECK_BUFFER


		if (rc < GALLOC_OK) goto buffer_err;
		goto buffer_exit;





buffer_exit:
		DPRINT_TARGET("%s:%s:%i: success\n", DEBUG_INFO);
		goto exit;

buffer_err:
		DPRINT_TARGET("%s:%s:%i: failure\n", DEBUG_INFO);
		goto err0;




	case (GA_RT_BUFFER_SWATCH & GA_RT_SUBTYPE_MASK):

		if (storage_need & GA_STORAGE_RAM) goto swatch_buffer_err;

		/* Get maximum size the resource can have
		 */
		if (lastmode != NULL) {
			struct ggiGA_mode *ga_mode;

			ga_mode = ((struct ggiGA_mode *)lastmode->priv);
			memcpy(&mode, &ga_mode->mode, sizeof(ggi_mode));

			maxwidth = mode.virt.x;
			maxheight = mode.virt.y;
			maxdepth = mode.graphtype;
		} else {
			ggiGetMode(vis, &mode);
			maxwidth = mode.virt.x;
			maxheight = mode.virt.y;
			maxdepth = mode.graphtype;
		}	/* if */

		/* Swatch buffers have always the same pixelformat
		 * as the main mode.
		 */
		maxsize = maxwidth * maxheight;


		/* Is there a CAP ?
		 */
		if (ggiGAIsCap(handle->next)) {
			cap = handle->next;

			GA_CHECK_CAPAUTO_1D(cap, maxsize, maxdepth);
		}	/* if */





		/* Get requested size
		 */
		linear_size = &(handle->props->size.linear);
		depth = &(handle->props->sub.tank.gt);

		if ((GA_FLAG(handle->props) & GA_FLAG_COORDBASE_MASK)
		  == GA_COORDBASE_DOTS)
		{
			ggi_coord dot, pixel;

			dot.x = linear_size[0]; dot.y = linear_size[0];
			ggiGADot2Pixel(vis, dot, &pixel);

#warning Perform snapping to grid here

			linear_size[0] = pixel.x;
		}	/* if */



		LIB_ASSERT(linear_size[0] >= 0, "linear_size < 0");
		LIB_ASSERT(depth[0] >= 0, "depth < 0");

		DPRINT_TARGET("%s:%s:%i: "
			"handle->props->size.area.x (%i),\n"
			"handle->props->size.area.y (%i),\n"
			"handle->props->size.linear (%i),\n"
			"handle->props->sub.tank.gt (%i,%i)\n",
			DEBUG_INFO,
			handle->props->size.area.x,
			handle->props->size.area.y,
			handle->props->size.linear,
			GT_SIZE(handle->props->sub.tank.gt),
			GT_ByPP(handle->props->sub.tank.gt));

		if ((linear_size[0] < 0) || (GT_ByPP(depth[0]) < 0)) {
			/* either user error or overflow bug */
			goto swatch_buffer_err;
		}	/* if */

		LIB_ASSERT(linear_size[0] >= 0, "linear_size < 0");
		LIB_ASSERT(depth[0] >= 0, "depth < 0");

		/* Check GGI_AUTO's */
		GA_CHECK_GGIAUTO_1D(handle, maxsize, maxdepth);

		LIB_ASSERT(linear_size[0] > 0, "linear_size < 0");
		LIB_ASSERT(depth[0] > 0, "depth < 0");



		if (linear_size[0] > maxsize) {
			linear_size[0] = maxsize;
			ggiGAFlagModified(handle);
		}	/* if */
		if (depth[0] != maxdepth) {
			depth[0] = maxdepth;
			ggiGAFlagModified(handle);
		}	/* if */

		LIB_ASSERT((linear_size[0] * GT_ByPP(depth[0])) > 0,
			linear_size * depth == 0);





#define GA_CHECK_BUFFER(exit, error)				\
								\
{								\
int try = 0;							\
								\
if (storage_need != GA_STORAGE_DONTCARE) try = 2;		\
								\
for (; try <= 2; try++) {					\
								\
if (  (((storage_need & GA_STORAGE_STORAGE_MASK)		\
	== GA_STORAGE_DONTCARE) && (try == 0))			\
   || (storage_need & GA_STORAGE_VRAM) )			\
{								\
	if (storage_need == GA_STORAGE_NOVRAM) {		\
		if ((storage_need & GA_STORAGE_STORAGE_MASK)	\
			== GA_STORAGE_DONTCARE)			\
		{						\
			continue;				\
		} else {					\
			rc = GALLOC_EFAILED;			\
			goto error;				\
		}	/* if */				\
	}	/* if */					\
								\
	DPRINT_TARGET("%s:%s:%i: try GA_STORAGE_VRAM\n",	\
			DEBUG_INFO);				\
	dbnum = ggiDBGetNumBuffers(vis);			\
	if (dbnum == 0) {					\
		DPRINT_TARGET("%s:%s:%i: no directbuffer available\n",	\
			DEBUG_INFO);				\
		/* no directbuffer available */			\
		if ((storage_need & GA_STORAGE_STORAGE_MASK)	\
			== GA_STORAGE_DONTCARE)			\
		{						\
			continue;	/* Try next */		\
		}	/* if */				\
								\
		rc = GALLOC_EFAILED;				\
		goto error;					\
	}	/* if */					\
								\
	/* Directbuffer has always the same size as		\
	 * the video-mode.					\
	 */							\
	if (width[0] != maxwidth) rc = GALLOC_EFAILED;		\
	if (height[0] != maxheight) rc = GALLOC_EFAILED;	\
								\
	if (rc < GALLOC_OK) {					\
		DPRINT_TARGET("%s:%s:%i: directbuffer alloation failed\n",	\
			DEBUG_INFO);				\
		if ((storage_need & GA_STORAGE_STORAGE_MASK)	\
			== GA_STORAGE_DONTCARE)			\
		{						\
			continue;	/* Try next */		\
		}	/* if */				\
								\
		goto error;					\
	}	/* if */					\
								\
	handle->storage_share |= GA_SHARE_SHAREABLE;		\
	handle->storage_ok =					\
			(GA_STORAGE_DIRECT | GA_STORAGE_VRAM);	\
	goto exit;						\
}	/* if */						\
								\
								\
if ( (((storage_need & GA_STORAGE_STORAGE_MASK)			\
	== GA_STORAGE_DONTCARE) && (try == 1))			\
   || (storage_need & GA_STORAGE_VIRT) )			\
{								\
	if (storage_need == GA_STORAGE_VIRT) {			\
		if ((storage_need & GA_STORAGE_STORAGE_MASK)	\
			== GA_STORAGE_DONTCARE)			\
		{						\
			continue;				\
		} else {					\
			rc = GALLOC_EFAILED;			\
			goto error;				\
		}	/* if */				\
	}	/* if */					\
								\
	DPRINT_TARGET("%s:%s:%i: try GA_STORAGE_VIRT - size %ix%i\n",	\
		DEBUG_INFO, linear_size[0], GT_ByPP(depth[0]));	\
	buffer->buf = malloc(linear_size[0] * GT_ByPP(depth[0]));	\
	if (!buffer->buf) {					\
		DPRINT_TARGET("%s:%s:%i: not enough memory\n",	\
			DEBUG_INFO);				\
								\
		/* not enough memory available */		\
		if ((storage_need & GA_STORAGE_STORAGE_MASK)	\
			== GA_STORAGE_DONTCARE)			\
		{						\
			buffer->buf = NULL;			\
			continue;	/* Try next */		\
		}	/* if */				\
								\
		rc = GGI_ENOMEM;				\
		goto error;					\
	}	/* if */					\
								\
	free(buffer->buf);					\
	buffer->buf = NULL;					\
								\
	rc = GALLOC_OK;						\
	handle->storage_share |= GA_SHARE_SHAREABLE;		\
	handle->storage_ok = GA_STORAGE_VIRT;			\
	goto exit;						\
}	/* if */						\
								\
								\
if ( (((storage_need & GA_STORAGE_STORAGE_MASK)			\
	== GA_STORAGE_DONTCARE) && (try == 2))			\
   || (storage_need & GA_STORAGE_SECONDARY) )			\
{								\
	/* int fd; */						\
								\
	if (storage_need == GA_STORAGE_NOSECONDARY) {		\
		if ((storage_need & GA_STORAGE_STORAGE_MASK)	\
			== GA_STORAGE_DONTCARE)			\
		{						\
			continue;				\
		} else {					\
			rc = GALLOC_EFAILED;			\
			goto error;				\
		}	/* if */				\
	}	/* if */					\
								\
	DPRINT_TARGET("%s:%s:%i: try GA_STORAGE_SECONDARY\n",	\
			DEBUG_INFO);				\
	/* Should we emulate MMIO-access by using mmap()	\
	 * or should we use a filedescriptor directly?		\
	 */							\
	/* if (!buffer->filename) goto error; */		\
	/*							\
	fd = open(buffer->filename,				\
		O_CREAT | O_TRUNC | O_RDWR, 0600);		\
	if (fd < 0) goto fail_dontcare;				\
								\
	close(fd);						\
								\
	rc = GALLOC_OK;						\
	handle->storage_ok = GA_STORAGE_SECONDARY;		\
	goto exit;						\
	*/							\
	DPRINT_TARGET("%s:%s:%i: try GA_STORAGE_SECONDARY not implemented\n",	\
			DEBUG_INFO);				\
	rc = GALLOC_EFAILED;					\
}	/* if */						\
								\
}	/* for */						\
}


		GA_CHECK_BUFFER(swatch_buffer_exit, swatch_buffer_err);

#undef GA_CHECK_BUFFER


		if (rc < GALLOC_OK) goto swatch_buffer_err;
		goto swatch_buffer_exit;





swatch_buffer_exit:
		DPRINT_TARGET("%s:%s:%i: success\n", DEBUG_INFO);
		goto exit;

swatch_buffer_err:
		DPRINT_TARGET("%s:%s:%i: failure\n", DEBUG_INFO);
		goto err0;

	}	/* switch */


exit:
	handle->props->storage_ok = handle->storage_ok;
	LIB_ASSERT(handle->priv != NULL, "handle has no priv data");
	rc = GALLOC_OK;
	return rc;

err0:
	handle->res_state &= ~GA_STATE_MODIFIED;
	ggiGAFlagFailed(handle);
	LIB_ASSERT(handle->priv != NULL, "handle has no priv data");
	return rc;
}	/* check_buffer_tank */


int check_buffer(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	if (ggiGAIsMotor(handle)) {
		return check_buffer_motor(vis, handle, compound, lastmode);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return check_buffer_carb(vis, handle, compound, lastmode);
	}	/* if */


	return check_buffer_tank(vis, handle, compound, lastmode);
}	/* check_buffer */












/******************************************************
 *      Set Buffer
 */


int set_buffer_motor(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* set_sprite_motor */



int set_buffer_carb(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* set_buffer_carb */



int set_buffer_tank(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	ggi_xwin_common *priv;
	struct GA_x_buffer *buffer;

	int rc = GALLOC_EUNAVAILABLE;

	int dbnum;
	int storage_ok;

	LIB_ASSERT(handle != NULL, "handle == NULL");
	priv = LIBGGI_PRIVATE(vis);


	buffer = (struct GA_x_buffer *)handle->priv;

	storage_ok = handle->storage_ok;


	switch (handle->res_type & GA_RT_SUBTYPE_MASK) {
	case (GA_RT_BUFFER_DONTCARE & GA_RT_SUBTYPE_MASK):

	case (GA_RT_BUFFER_RAW & GA_RT_SUBTYPE_MASK):

	/* X seems to handle all buffer-types as raw-buffers.
	 * That's good as we don't have to care about them.
	 */
	case (GA_RT_BUFFER_ZBUFFER & GA_RT_SUBTYPE_MASK):
	case (GA_RT_BUFFER_ABUFFER & GA_RT_SUBTYPE_MASK):
	case (GA_RT_BUFFER_SBUFFER & GA_RT_SUBTYPE_MASK):
	case (GA_RT_BUFFER_TBUFFER & GA_RT_SUBTYPE_MASK):

		if (storage_ok & GA_STORAGE_VRAM) {
			int actype;

			dbnum = ggiDBGetNumBuffers(vis);
			if (dbnum == 0) {
				goto buffer_fail;
			}	/* if */

			buffer->db = ggiDBGetBuffer(vis, dbnum);
			LIB_ASSERT(buffer->db != NULL, "got no buffer");

#warning Should not we support all types of dbs?
			if (!(buffer->db->type & GGI_DB_SIMPLE_PLB)) {
				goto buffer_fail;
			}	/* if */

			if (handle->props->storage_need & GA_STORAGE_READONLY) {
				actype = GGI_ACTYPE_READ;
			} else {
				actype = (GGI_ACTYPE_READ | GGI_ACTYPE_WRITE);
			}	/* if */

			rc = ggiResourceAcquire(buffer->db->resource, actype);
			LIB_ASSERT(rc == GALLOC_OK, "couldn't acquire buffer");
			if (rc != GALLOC_OK) {
				goto buffer_fail;
			}	/* if */

			buffer->buf = NULL;
			buffer->buf_size = 0;
			buffer->read = (buffer->db->read);
			buffer->write = (buffer->db->write);

			rc = GALLOC_OK;
			goto buffer_exit;
		}	/* if */


		if (storage_ok & GA_STORAGE_VIRT) {
			size_t size;

			size = handle->props->size.area.x *
				handle->props->size.area.y *
				GT_ByPP(handle->props->sub.tank.gt);

			buffer->buf = malloc(size);
			if (!buffer->buf) {
				goto buffer_fail;
			}	/* if */

			buffer->buf_size = size;
			buffer->read = (buffer->buf);
			buffer->write = (buffer->buf);

			rc = GALLOC_OK;
			goto buffer_exit;
		}	/* if */


		if (storage_ok & GA_STORAGE_SECONDARY) {
#if 0
			int fd;
			int prot, flags;
			int filesize;

			if (!buffer->filename) goto buffer_fail;
			fd = open(buffer->filename,
					O_CREAT | O_TRUNC | O_RDWR, 0600);
			if (fd < 0) goto buffer_fail;


			flags = MAP_FIXED | MAP_PRIVATE;
			prot = PROT_READ;
			if (!(storage_ok & GA_STORAGE_READONLY)) {
				prot |= PROT_WRITE;
			}	/* if */

#warning Set the filesize here

			buffer->buf = mmap(0, filesize, prot, flags, fd, 0);

			close(fd);

			if (buffer->buf == MAP_FAILED) {
				buffer->buf = NULL;
				goto buffer_fail;
			}	/* if */

			rc = GALLOC_OK;
			goto buffer_exit;
#else
			goto buffer_fail;
#endif
		}	/* if */


buffer_exit:
		DPRINT_TARGET("%s:%s:%i: point EXIT\n",
				DEBUG_INFO);
		goto exit;

buffer_fail:
		DPRINT_TARGET("%s:%s:%i: point FAIL\n",
				DEBUG_INFO);
		goto fail;



	case (GA_RT_BUFFER_SWATCH & GA_RT_SUBTYPE_MASK):

		if (storage_ok & GA_STORAGE_VRAM) {
			int actype;

			dbnum = ggiDBGetNumBuffers(vis);
			if (dbnum == 0) {
				goto swatch_buffer_fail;
			}	/* if */

			buffer->db = ggiDBGetBuffer(vis, dbnum);
			LIB_ASSERT(buffer->db != NULL, "couldn't get buffer");

#warning Should not we support all types of dbs?
			if (!(buffer->db->type & GGI_DB_SIMPLE_PLB)) {
				goto swatch_buffer_fail;
			}	/* if */

			if (handle->props->storage_need & GA_STORAGE_READONLY) {
				actype = GGI_ACTYPE_READ;
			} else {
				actype = (GGI_ACTYPE_READ | GGI_ACTYPE_WRITE);
			}	/* if */

			rc = ggiResourceAcquire(buffer->db->resource, actype);
			LIB_ASSERT(rc == GALLOC_OK, "couldn't acquire buffer");
			if (rc != GALLOC_OK) {
				goto swatch_buffer_fail;
			}	/* if */

			buffer->buf = NULL;
			buffer->buf_size = 0;
			buffer->read = (buffer->db->read);
			buffer->write = (buffer->db->write);

#warning Check here, if swatch has same pixelformat as the mode

			rc = GALLOC_OK;
			goto swatch_buffer_exit;
		}	/* if */


		if (storage_ok & GA_STORAGE_VIRT) {
			size_t size;

			size = handle->props->size.linear *
				GT_ByPP(handle->props->sub.tank.gt);

			buffer->buf = malloc(size);
			if (!buffer->buf) {
				goto swatch_buffer_fail;
			}	/* if */

			buffer->buf_size = size;
			buffer->read = (buffer->buf);
			buffer->write = (buffer->buf);

#warning Check here, if swatch has same pixelformat as the mode

			rc = GALLOC_OK;
			goto swatch_buffer_exit;
		}	/* if */


		if (storage_ok & GA_STORAGE_SECONDARY) {
#if 0
			int fd;
			int prot, flags;
			int filesize;

			if (!buffer->filename) goto swatch_buffer_fail;
			fd = open(buffer->filename,
					O_CREAT | O_TRUNC | O_RDWR, 0600);
			if (fd < 0) goto swatch_buffer_fail;


			flags = MAP_FIXED | MAP_PRIVATE;
			prot = PROT_READ;
			if (!(storage_ok & GA_STORAGE_READONLY)) {
				prot |= PROT_WRITE;
			}	/* if */

#warning Set the filesize here

			buffer->buf = mmap(0, filesize, prot, flags, fd, 0);

			close(fd);

			if (buffer->buf == MAP_FAILED) {
				buffer->buf = NULL;
				goto swatch_buffer_fail;
			}	/* if */

#warning Check here, if swatch has same pixelformat as the mode

			rc = GALLOC_OK;
			goto swatch_buffer_exit;
#else
			goto swatch_buffer_fail;
#endif
		}	/* if */


swatch_buffer_exit:
		DPRINT_TARGET("%s:%s:%i: point swatch EXIT\n",
				DEBUG_INFO);
		goto exit;

swatch_buffer_fail:
		DPRINT_TARGET("%s:%s:%i: point swatch FAIL\n",
				DEBUG_INFO);
		goto fail;


	}	/* switch */

exit:
	return rc;

fail:
	rc = GALLOC_EUNAVAILABLE;
	ggiGAFlagFailed(handle);
	return rc;
}	/* set_buffer_tank */


int set_buffer(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	if (ggiGAIsMotor(handle)) {
		return set_buffer_motor(vis, handle, compound, lastmode);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return set_buffer_carb(vis, handle, compound, lastmode);
	}	/* if */


	return set_buffer_tank(vis, handle, compound, lastmode);
}	/* set_buffer */














/******************************************************
 *      Release Buffer
 */


int release_buffer_motor(ggi_visual_t vis, ggiGA_resource_handle handle)
{
        DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* release_buffer_motor */


int release_buffer_carb(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* release_buffer_carb */



int release_buffer_tank(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	ggi_xwin_common *priv;
	struct GA_x_buffer *buffer;

	int rc = GALLOC_OK;

	int storage_ok;

	LIB_ASSERT(handle != NULL, "handle == NULL");
	priv = LIBGGI_PRIVATE(handle->vis);

	buffer = (struct GA_x_buffer *)handle->priv;
	storage_ok = handle->storage_ok;


	switch (handle->res_type & GA_RT_SUBTYPE_MASK) {
	case (GA_RT_BUFFER_DONTCARE & GA_RT_SUBTYPE_MASK):

	case (GA_RT_BUFFER_RAW & GA_RT_SUBTYPE_MASK):

	/* X seems to handle all buffer-types as raw-buffers.
	 * That's good as we don't have to care about them.
	 */
	case (GA_RT_BUFFER_ZBUFFER & GA_RT_SUBTYPE_MASK):
	case (GA_RT_BUFFER_ABUFFER & GA_RT_SUBTYPE_MASK):
	case (GA_RT_BUFFER_SBUFFER & GA_RT_SUBTYPE_MASK):
	case (GA_RT_BUFFER_TBUFFER & GA_RT_SUBTYPE_MASK):

		if (storage_ok & GA_STORAGE_VRAM) {
			if (buffer->db == NULL) {
				/* never allocated */
				rc = GALLOC_OK;
				goto buffer_exit;
			}	/* if */

			rc = ggiResourceRelease(buffer->db->resource);
			LIB_ASSERT(rc == GALLOC_OK, "couldn't acquire buffer");

			buffer->db = NULL;
			buffer->read = NULL;
			buffer->write = NULL;

			rc = GALLOC_OK;
			goto buffer_exit;
		}	/* if */


		if (storage_ok & GA_STORAGE_VIRT) {
			LIB_ASSERT(buffer->buf != NULL, "invalid buffer");

			free(buffer->buf);

			buffer->buf = NULL;
			buffer->buf_size = 0;
			buffer->read = NULL;
			buffer->write = NULL;

			rc = GALLOC_OK;
			goto buffer_exit;
		}	/* if */


		if (storage_ok & GA_STORAGE_SECONDARY) {
			LIB_ASSERT(buffer->buf != NULL, "invalid buffer");

			rc = munmap(buffer->buf, buffer->buf_size);
			if (rc < 0) {
				goto buffer_fail;
			}	/* if */

			rc = GALLOC_OK;
			goto buffer_exit;
		}	/* if */

buffer_exit:
		goto fail;

buffer_fail:
		goto fail;



	case (GA_RT_BUFFER_SWATCH & GA_RT_SUBTYPE_MASK):

		if (storage_ok & GA_STORAGE_VRAM) {
			if (buffer->db == NULL) {
				/* never allocated */
				rc = GALLOC_OK;
				goto swatch_buffer_exit;
			}	/* if */

			rc = ggiResourceRelease(buffer->db->resource);
			LIB_ASSERT(rc == GALLOC_OK, "couldn't release resource");

			buffer->db = NULL;
			buffer->read = NULL;
			buffer->write = NULL;

			rc = GALLOC_OK;
			goto swatch_buffer_exit;
		}	/* if */


		if (storage_ok & GA_STORAGE_VIRT) {
			LIB_ASSERT(buffer->buf != NULL, "invalid buffer");

			free(buffer->buf);

			buffer->buf = NULL;
			buffer->buf_size = 0;
			buffer->read = NULL;
			buffer->write = NULL;

			rc = GALLOC_OK;
			goto swatch_buffer_exit;
		}	/* if */


		if (storage_ok & GA_STORAGE_SECONDARY) {
			LIB_ASSERT(buffer->buf != NULL, "invalid buffer");

			rc = munmap(buffer->buf, buffer->buf_size);
			if (rc < 0) {
				goto swatch_buffer_fail;
			}	/* if */

			rc = GALLOC_OK;
			goto swatch_buffer_exit;
		}	/* if */


swatch_buffer_exit:
		goto exit;

swatch_buffer_fail:
		goto fail;

	}	/* switch */

exit:
	return rc;

fail:
	rc = GALLOC_EFAILED;
	ggiGAFlagFailed(handle);
	return rc;
}	/* release_buffer_tank */


int release_buffer(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	if (ggiGAIsMotor(handle)) {
		return release_buffer_motor(vis, handle);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return release_buffer_carb(vis, handle);
	}	/* if */


	return release_buffer_tank(vis, handle);
}	/* release_buffer */
