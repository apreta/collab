/* $Id: window.c,v 1.20 2005/10/12 12:44:45 cegger Exp $
******************************************************************************

   Galloc implementation for X target - window functions.

   Copyright (C) 2001	Christoph Egger	[Christoph_Egger@t-online.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include <stdio.h>
#include <string.h>

#include "xgalloc.h"



/******************************************************
 *      Check WINDOW
 */



int check_window_motor(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* check_window_motor */


int check_window_carb(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* check_window_carb */



int check_window_tank(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	ggiGA_resource_handle cap = NULL;
        struct GA_x_window *window = NULL;
	int rc = GALLOC_EUNAVAILABLE;

	int res_state;
	int storage_need;

	LIB_ASSERT(handle != NULL, "handle == NULL");


	res_state = (handle->res_state & GA_STATE_REQUEST_MASK);
	storage_need = (handle->props->storage_need & GA_STORAGE_STORAGE_MASK);

	switch (handle->res_type & GA_RT_SUBTYPE_MASK) {
	case (GA_RT_WINDOW_DONTCARE & GA_RT_SUBTYPE_MASK):

		/* Oops! Don't know, what to do here. So fail!
		 */
		rc = GALLOC_ESPECIFY;
		goto err0;

	case (GA_RT_WINDOW_YUV & GA_RT_SUBTYPE_MASK):

		/* not implemented yet */
		rc = GALLOC_EUNAVAILABLE;
		goto err0;

	case (GA_RT_WINDOW_MM & GA_RT_SUBTYPE_MASK):
		{
	        ggi_visual_t subvis = NULL;
	        ggi_mode maxmode, mode;


		if (handle->priv == NULL) {
			window = calloc(1, sizeof(struct GA_x_window));
			if (!window) {
				rc = GGI_ENOMEM;
				ggiGAFlagFailed(handle);
				goto win_mm_err;
			}	/* if */

			handle->priv = (void *)window;
			handle->priv_size = sizeof(struct GA_x_window);
		} else {
			window = (struct GA_x_window *)handle->priv;
		}	/* if */

		LIB_ASSERT(window != NULL, "window == NULL");

		if (storage_need & GA_STORAGE_NOVIRT) {
			rc = GALLOC_EFAILED;
			goto win_mm_err;
		}	/* if */


		if ( !(storage_need & GA_STORAGE_VIRT)
		   && (storage_need & ~GA_STORAGE_DONTCARE)
			!= GA_STORAGE_DONTCARE)
		{
			rc = GALLOC_EFAILED;
			goto win_mm_err;
		}	/* if */


		/* Set coordbase */
		handle->cb = (GA_COORDBASE_UNIT & GA_FLAG_UNIT_MASK);
		handle->cb |= (GA_COORDBASE_PIXEL & GA_FLAG_COORDBASE_MASK);


		/* Get maximum size the resource can have
		 */
		if (lastmode != NULL) {
			struct ggiGA_mode *ga_mode = NULL;

			ga_mode = (struct ggiGA_mode *)lastmode->priv;
			memcpy(&maxmode, &(ga_mode->mode), sizeof(ggi_mode));
		} else {
			ggiGetMode(vis, &maxmode);
		}	/* if */



		/* Is there a CAP ?
		 */
		if (ggiGAIsCap(handle->next)) {
			cap = handle->next;

			GA_CHECK_CAPAUTO_2D(cap,
				maxmode.virt.x,
				maxmode.virt.y,
				maxmode.graphtype);
		}	/* if */




		/* Get requested size
		 */
		/* top left corner */
		mode.visible.x = handle->props->sub.motor.grid_start.x;
		mode.visible.y = handle->props->sub.motor.grid_start.y;

		/* size */
		mode.virt.x = handle->props->size.area.x;
		mode.virt.y = handle->props->size.area.y;

		if ((GA_FLAG(handle->props) & GA_FLAG_COORDBASE_MASK)
		   == GA_COORDBASE_DOTS)
		{
			ggiGADot2Pixel(vis, mode.visible, &mode.visible);
			ggiGADot2Pixel(vis, mode.virt, &mode.virt);

#warning Perform snapping to grid here
		}	/* if */


		subvis = ggiOpen("display-sub", vis, NULL);
		if (!subvis) {
			rc = GALLOC_EUNAVAILABLE;
			ggiGAFlagFailed(handle);
			goto win_mm_err;
		}	/* if */


		/* Check GGI_AUTO's */

		/* top left corner */
		if (handle->props->sub.motor.grid_start.x == 0)
			handle->props->sub.motor.grid_start.x = 1;
		if (handle->props->sub.motor.grid_start.y == 0)
			handle->props->sub.motor.grid_start.y = 1;

		/* size */
		GA_CHECK_GGIAUTO_2D(handle,
				maxmode.virt.x, maxmode.virt.y,
				maxmode.graphtype);


		if (mode.virt.x > maxmode.virt.x) {
			mode.virt.x = maxmode.virt.x;
			ggiGAFlagModified(handle);
		}	/* if */
		if (mode.virt.y > maxmode.virt.y) {
			mode.virt.y = maxmode.virt.y;
			ggiGAFlagModified(handle);
		}	/* if */
		if (mode.graphtype > maxmode.graphtype) {
			mode.graphtype = maxmode.graphtype;
			ggiGAFlagModified(handle);
		}	/* if */






#define GA_CHECK_WINDOW_MM(exit, error)			\
							\
rc = ggiCheckMode(subvis, &mode);			\
if (rc != GGI_OK) {					\
	rc = GALLOC_EFAILED;				\
} else {						\
	rc = GALLOC_OK;					\
}	/* if */



		GA_CHECK_WINDOW_MM(win_mm_exit, win_mm_err);

#undef GA_CHECK_WINDOW_MM


		if (rc < GALLOC_OK) goto win_mm_err;
		goto win_mm_exit;





win_mm_exit:
		LIB_ASSERT(window->vis == NULL, "window has invalid visual");
		window->vis = subvis;
		memcpy(&(window->mode), &mode, sizeof(ggi_mode));

		handle->storage_ok = GA_STORAGE_VIRT;
		goto exit;

win_mm_err:
		ggiClose(subvis);
		goto err0;
		}

	}	/* switch */

exit:
	LIB_ASSERT(handle->priv != NULL, "handle has no private data");
	rc = GALLOC_OK;
	return rc;

err0:
	handle->res_state &= ~GA_STATE_MODIFIED;
	ggiGAFlagFailed(handle);
	LIB_ASSERT(handle->priv != NULL, "handle has no private data");
	return rc;
}	/* check_window_tank */


int check_window(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	if (ggiGAIsMotor(handle)) {
		return check_window_motor(vis, handle, compound, lastmode);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return check_window_carb(vis, handle, compound, lastmode);
	}	/* if */


	return check_window_tank(vis, handle, compound, lastmode);
}	/* check_window */













/******************************************************
 *      Set WINDOW
 */

int set_window_motor(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* set_window_motor */


int set_window_carb(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* set_window_carb */



int set_window_tank(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	struct GA_x_window *window;
	int rc = GALLOC_EFAILED;

	LIB_ASSERT(handle != NULL, "handle == NULL");

	if (handle->priv == NULL) {
		/* not allocated before */
		rc = GALLOC_EFAILED;
		ggiGAFlagFailed(handle);
		goto err0;
	}	/* if */


	switch (handle->res_type & GA_RT_SUBTYPE_MASK) {
	case (GA_RT_WINDOW_DONTCARE & GA_RT_SUBTYPE_MASK):

		/* Oops! Don't know, what to do here. So fail!
		 */
		rc = GALLOC_EUNAVAILABLE;
		ggiGAFlagFailed(handle);
		break;

	case (GA_RT_WINDOW_YUV & GA_RT_SUBTYPE_MASK):

		/* not implemented yet */
		rc = GALLOC_EUNAVAILABLE;
		ggiGAFlagFailed(handle);
		break;

	case (GA_RT_WINDOW_MM & GA_RT_SUBTYPE_MASK):
		window = (struct GA_x_window *)handle->priv;

		rc = ggiSetMode(window->vis, &(window->mode));
		LIB_ASSERT(rc == GALLOC_OK, "couldn't set a mode");

		if (rc != GALLOC_OK) {
			rc = GALLOC_EFAILED;
			ggiGAFlagFailed(handle);
			goto win_mm_err;
		}       /* if */

		break;

win_mm_err:
		ggiClose(window->vis);
		free(window);
		handle->priv_size = 0;
		handle->priv = NULL;
		break;

	}	/* switch */

err0:
	return rc;
}	/* set_window_tank */



int set_window(ggi_visual_t vis,
		ggiGA_resource_handle handle,
		ggiGA_resource_handle compound,
		ggiGA_resource_handle lastmode)
{
	if (ggiGAIsMotor(handle)) {
		return set_window_motor(vis, handle, compound, lastmode);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return set_window_carb(vis, handle, compound, lastmode);
	}	/* if */


	return set_window_tank(vis, handle, compound, lastmode);
}	/* set_window */















/******************************************************
 *	Release WINDOW
 */

int release_window_motor(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement motor handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* release_window_motor */



int release_window_carb(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	DPRINT_TARGET("%s:%s:%i: called\n", DEBUG_INFO);

#warning implement carb handling here

#if 1
	return GALLOC_OK;
#else
	return GALLOC_EFAILED;
#endif
}	/* release_window_carb */



int release_window_tank(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	struct GA_x_window *window;
	int rc = GALLOC_OK;

	LIB_ASSERT(handle != NULL, "handle == NULL");

	if (handle->priv == NULL) {
		/* never allocated */
		rc = GALLOC_OK;
		goto exit;
	}	/* if */


	switch (handle->res_type & GA_RT_SUBTYPE_MASK) {
	case (GA_RT_WINDOW_DONTCARE & GA_RT_SUBTYPE_MASK):

		/* Oops! Don't know, what to do here. So fail!
		 */
		rc = GALLOC_EUNAVAILABLE;
		ggiGAFlagFailed(handle);
		break;

	case (GA_RT_WINDOW_YUV & GA_RT_SUBTYPE_MASK):

		/* Not implemented yet! */
		rc = GALLOC_EUNAVAILABLE;
		ggiGAFlagFailed(handle);
		break;

	case (GA_RT_WINDOW_MM & GA_RT_SUBTYPE_MASK):
		window = (struct GA_x_window *)handle->priv;

		ggiClose(window->vis);
		free(window);
		handle->priv_size = 0;
		handle->priv = NULL;

		rc = GALLOC_OK;
		break;

	}	/* switch */


exit:
	return rc;
}	/* release_window_tank */


int release_window(ggi_visual_t vis, ggiGA_resource_handle handle)
{
	if (ggiGAIsMotor(handle)) {
		return release_window_motor(vis, handle);
	}	/* if */


	if (ggiGAIsCarb(handle)) {
		return release_window_carb(vis, handle);
	}	/* if */


	return release_window_tank(vis, handle);
}	/* release_frame */

