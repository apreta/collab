/* $Id: haslist.c,v 1.21 2007/12/21 09:30:47 cegger Exp $
******************************************************************************

   LibGAlloc implementation for "stubs" target -- 
   				Master resource list and associated callbacks
  
   Copyright (c) 2001 Brian S. Julin		[bri@tull.umassp.edu]
   
   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   The above copyright notice applies to all files in this package, unless 
   explicitly stated otherwise in the file itself or in a file named COPYING 
   in the same directory as the file.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "stubsgalloc.h"
#include <string.h>


static struct ggiGA_resource_props drawcarb_props = {
	.storage_ok =   GA_STORAGE_SWAP,
	.storage_need = GA_STORAGE_REALRAM,
	.sub = {
		.carb = {
			.can = {
				.rop = LL_ROP_ALPHA_AUX | LL_ROP_Z,
				.math = 0xffffffff,  /* All math functions supported. */
				.adj = 0
			},
			.must = {
				.rop = LL_ROP_AUX,     /* side-buffers only */
				.math = 0,           /* No math is mandatory. */
				.adj = 0
			}
		}
	}
};


static struct ggiGA_resource drawcarb_res = {
	.res_type =	GA_RT_RENDERER_DRAWOPS | GA_RT_CARB,
	.res_state =	GA_STATE_SEEABOVE | 1,
	.props = &drawcarb_props,
};


static struct ggiGA_resource_props zbuf_props = {
	.storage_ok =	GA_STORAGE_SWAP,
	.storage_need =	GA_STORAGE_REALRAM,
};

static struct ggiGA_resource zbuf_res = {
	.res_type = GA_RT_BUFFER_ZBUFFER,
	.res_state = GA_STATE_SEEABOVE,
	.props = &zbuf_props,
	.cbflags1 = GA_CB_CHECK | GA_CB_RECHECK | GA_CB_POSTCHECK,
	.cb1 = &stubs_galloc_check_cladbuf
};

static struct ggiGA_resource_props alphabuf_props = {
	.storage_ok =   GA_STORAGE_SWAP,
	.storage_need = GA_STORAGE_REALRAM,
};

static struct ggiGA_resource alphabuf_res = {
	.res_type = GA_RT_BUFFER_ABUFFER,
	.res_state = GA_STATE_SEEABOVE,
	.props = &alphabuf_props,
	.cbflags1 = GA_CB_CHECK | GA_CB_RECHECK | GA_CB_POSTCHECK,
	.cb1 = &stubs_galloc_check_cladbuf
};

static struct ggiGA_mode frame_priv = {
	.mode = {
		.frames =	GGI_AUTO,
		.visible =	{ GGI_AUTO, GGI_AUTO },
		.virt =	{ GGI_AUTO, GGI_AUTO },
		.size =	{ GGI_AUTO, GGI_AUTO },
		.graphtype =	GT_AUTO,
		.dpp =	{ GGI_AUTO, GGI_AUTO },
	},
};

static struct ggiGA_resource frame_res = {
	.res_type = GA_RT_FRAME,
	.priv = &frame_priv,
	.priv_size = sizeof(struct ggiGA_mode),
	.cbflags1 = GA_CB_PRECHECK | GA_CB_CHECK | GA_CB_UNCHECK | GA_CB_RECHECK,
	.cb1 = stubs_galloc_check_mode,
	.cbflags2 = GA_CB_SET | GA_CB_UNSET | GA_CB_RELEASE,
	.cb2 = stubs_galloc_set_mode
};

static struct ggiGA_resource_props drawops_motor_props = {
	.sub = {
		.motor = {	/* No scaling/pixeldoubling supported. */
			.mul_min = {1, 1},
			.mul_max = {1, 1},
			.div_min = {1, 1},
			.div_max = {1, 1},
		}
	},
};

static struct ggiGA_resource drawops_motor_res = {
	.res_type = GA_RT_RENDERER_DRAWOPS | GA_RT_MOTOR,
	.res_state = 1,		/* Tag group 1 */
	.props = &drawops_motor_props
};


static struct ggiGA_resource_listhead stubs_haslist;


int stubs_galloc_InitHaslist(struct galloc_context *ctxt)
{
	ggiGA_resource_list list = &stubs_haslist;

	RESLIST_INIT(list);

	RESLIST_INSERT_HEAD(list, &drawops_motor_res);
	RESLIST_INSERT_TAIL(list, &frame_res);
	RESLIST_INSERT_TAIL(list, &alphabuf_res);
	RESLIST_INSERT_TAIL(list, &zbuf_res);
	RESLIST_INSERT_TAIL(list, &drawcarb_res);

	/* Apply initial values */
	if (ctxt->haslist == ctxt->dummy) {
		ctxt->haslist = list;
	}

	return 0;
}


/******************************** callbacks *********************************/

int stubs_galloc_check_mode (struct galloc_context *ctxt,
			     ggiGA_resource_list reslist,
			     enum ggiGA_callback_command command,
			     ggiGA_resource_handle res,
			     ggiGA_resource_handle head,
			     ggiGA_taghead_t taghead,
			     ggiGA_resource_handle has,
			     struct ggiGA_template_state *state,
			     void *privstate)
{
	ggi_mode mode;
	int rc;

#ifdef DEBUG
	fprintf(stderr, "stubs_galloc_check_mode called\n");
#endif

	/* TODO: even if active process storage on POSTCHECK */
	if (res->res_state & GA_STATE_NORESET) return GALLOC_OK;

	if (command == GA_CB_UNCHECK) goto uncheck;

	if (command == GA_CB_PRECHECK) {
		/* TODO: basic comparison of res vs. has including caps. */
		return 0;
	}

	if (command == GA_CB_POSTCHECK) return 0;

	/* check and recheck are the same for us. */

	/* If we have a cap, try it first */
	if (!ggiGAIsCap(RESLIST_NEXT(res))) goto nocap;

	memcpy(&mode, ggiGAGetGGIMode( RESLIST_NEXT(res)), sizeof(ggi_mode));
	rc = ggiCheckMode(vis, &mode);

        /* Now we try again, using the adjusted values that came 
	 * back from ggiCheckMode. 
	 */
        if (rc != GALLOC_OK) rc = ggiCheckMode(vis, &mode);

	if (rc == GALLOC_OK) {
		ggi_mode *minmode;

		/* The cap succeeded.  But did it get a mode above the 
		 * minimum requirements in res?
		 */
		minmode = ggiGAGetGGIMode(res);
		rc |= (minmode->visible.x > mode.visible.x);
		rc |= (minmode->visible.y > mode.visible.y);
		rc |= (minmode->virt.x > mode.virt.x);
		rc |= (minmode->virt.y > mode.virt.y);
		rc |= (GT_DEPTH(minmode->graphtype) > 
		       GT_DEPTH(mode.graphtype));
		rc |= (GT_SCHEME(minmode->graphtype) != 
		       GT_SCHEME(mode.graphtype));
	}	/* if */

	if (rc == GALLOC_OK) goto done;

 nocap:
	/* cap failed, so let's try the original resource. */
	memcpy(&mode, ggiGAGetGGIMode(res), sizeof(ggi_mode));
	rc = ggiCheckMode(vis, &mode);

	if (rc == GALLOC_OK) {
		ggiGAFlagModified(res);
		memcpy(ggiGAGetGGIMode(res), &mode, sizeof(ggi_mode));
		if (!ggiGAIsCap(RESLIST_NEXT(res))) return 0;
		ggiGAFlagModified(RESLIST_NEXT(res));
		memcpy(ggiGAGetGGIMode(RESLIST_NEXT(res)), &mode, sizeof(ggi_mode));
		return 0;
	}	/* if */
	
	/* Now we try again, using the adjusted values that came
	 * back from ggiCheckMode.
	 */
	rc = ggiCheckMode(vis, &mode);
	if (rc != GALLOC_OK) {
		mode.visible.x = GGI_AUTO;
		mode.visible.y = GGI_AUTO;
		mode.virt.x = GGI_AUTO;
		mode.virt.y = GGI_AUTO;
		mode.graphtype = GT_AUTO;
		rc = ggiCheckMode(vis, &mode);
	}	/* if */


	if (rc == GALLOC_OK) {
		ggi_mode *minmode, *maxmode;

		/* We got something to succeed -- question is, what? */
		minmode = ggiGAGetGGIMode(res);
		rc |= (minmode->visible.x > mode.visible.x);
		rc |= (minmode->visible.y > mode.visible.y);
		rc |= (minmode->virt.x > mode.virt.x);
		rc |= (minmode->virt.y > mode.virt.y);
		rc |= (GT_DEPTH(minmode->graphtype) > 
		       GT_DEPTH(mode.graphtype));
		rc |= (GT_SCHEME(minmode->graphtype) != 
		       GT_SCHEME(mode.graphtype));

		if (!ggiGAIsCap(RESLIST_NEXT(res))) goto done;
		maxmode = ggiGAGetGGIMode(RESLIST_NEXT(res));
		rc |= (maxmode->visible.x < mode.visible.x);
		rc |= (maxmode->visible.y < mode.visible.y);
		rc |= (maxmode->virt.x < mode.virt.x);
		rc |= (maxmode->virt.y < mode.virt.y);
		rc |= (GT_DEPTH(maxmode->graphtype) <
		       GT_DEPTH(mode.graphtype));
	}	/* if */

 done:
	ggiGAFlagModified(res);
	memcpy(ggiGAGetGGIMode(res), &mode, sizeof(ggi_mode));
	if (!ggiGAIsCap(RESLIST_NEXT(res))) return (rc ? 0 : 2);
	ggiGAFlagModified(RESLIST_NEXT(res));
	memcpy(ggiGAGetGGIMode(RESLIST_NEXT(res)), &mode, sizeof(ggi_mode));
	return (rc ? 0 : 2);
	
uncheck:
	return 0;
}	/* stubs_galloc_check_mode */


int stubs_galloc_set_mode (ggi_visual_t vis,
			   ggiGA_resource_list reslist,
			   enum ggiGA_callback_command command,
			   ggiGA_resource_handle res,
			   ggiGA_resource_handle head,
			   ggiGA_taghead_t taghead,
			   ggiGA_resource_handle has,
			   struct ggiGA_template_state *state,
			   void *privstate)
{
#ifdef DEBUG
	fprintf(stderr, "stubs_galloc_set_mode called\n");
	fprintf(stderr, "res_state = %x\n", res->res_state);
#endif
	if (!(res->res_state & GA_STATE_NORESET))
		return ggiSetMode(vis, ggiGAGetGGIMode(res));
	return GALLOC_OK;	/* Already set, nothing to do. */
}	/* stubs_galloc_set_mode */


/* Check for buffers that clad the visual -- they match the 
 * main mode, which is always their compound head right now, until
 * we start supporting floaters.
 */
int stubs_galloc_check_cladbuf (ggi_visual_t vis,
				ggiGA_resource_list reslist,
				enum ggiGA_callback_command command,
				ggiGA_resource_handle res,
				ggiGA_resource_handle head,
				ggiGA_taghead_t taghead,
				ggiGA_resource_handle has,
				struct ggiGA_template_state *state,
				void *privstate)
{
	ggi_mode *mode;
	struct ggiGA_resource_props *res_p;

	/* We probably don't even need these asserts, remove later. */
	LIB_ASSERT(ggiGA_TYPE(head) == GA_RT_FRAME, "may not be a frame resource");
	LIB_ASSERT(head->priv != NULL, "no priv area allocated");

	if (command == GA_CB_POSTCHECK) return 0;
	if (command == GA_CB_UNCHECK) return 0;
	if (command == GA_CB_RECHECK) return 0;

	/* Storage type checks were handled by precheck */

	if (res == head) {
		fprintf(stderr, "No support for floaters yet.\n");
		goto fail;
	}	/* if */

	res_p = res->props;

	mode = ggiGAGetGGIMode(head);

	if ((res_p->size.area.x != GGI_AUTO) &&
	    (res_p->size.area.x != mode->virt.x)) goto fail;
	if ((res_p->size.area.y != GGI_AUTO) &&
	    (res_p->size.area.y != mode->virt.y)) goto fail;

	res_p->size.area.x = mode->virt.x;
	res_p->size.area.y = mode->virt.y;
	ggiGAFlagModified(res);

	return 0;
 fail:
	return 4;
}	/* stubs_galloc_check_cladbuf */
