/* $Id: funcs.c,v 1.34 2005/10/11 23:18:39 cegger Exp $
******************************************************************************

   LibGAlloc implementation for "stubs" target -- API functions.

   
  
   Copyright (c) Thu Mar 22 2001 by: 
	Brian S. Julin		bri@calyx.com

  
   
   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   The above copyright notice applies to all files in this package, unless 
   explicitly stated otherwise in the file itself or in a file named COPYING 
   in the same directory as the file.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.





******************************************************************************
*/


#define COMPILING_GALLOC_STUB_FUNCTIONS
#define COMPILING_GALLOC_C

#include "stubsgalloc.h"

#include <ggi/ga_prop.h>
#include <stdio.h>
#include <string.h>


/* _All_ non-local (ie not declared 'static') functions and variables _must_
   be prefixed with the extension name, and for sublibs also with a unique
   sublib identifier. This is to keep the namespace clean on systems where
   all symbols are exported by default.
*/

/*-* API Implementation */

int GALLOC_Stubs_Check(ggi_visual_t vis, ggiGA_resource_list request,
		       ggiGA_resource_list *result)
{
	struct ggiGA_template_state state;
	int rc;

	state.mode = NULL;
	state.haslist = NULL;

	rc = ggiGACheck_template(vis, request, result, &state, NULL);
	ggiGADestroyList(&(state.haslist));
	return rc;
}	/* GALLOC_Stubs_Check */


int GALLOC_Stubs_Set(ggi_visual_t vis, ggiGA_resource_list request,
		     ggiGA_resource_list *result)
{
	struct ggiGA_template_state state;
	int rc;

	state.mode = NULL;
	state.haslist = NULL;

	rc = ggiGASet_template(vis, request, result, &state, NULL);
	ggiGADestroyList(&(state.haslist));
	return rc;
}	/* GALLOC_Stubs_Set */


int GALLOC_Stubs_Release(ggi_visual_t vis, ggiGA_resource_list *list,
			 ggiGA_resource_handle *handle)
{
	struct ggiGA_template_state state;
	return ggiGARelease_template(vis, list, handle, &state, NULL);
}	/* GALLOC_Stubs_Release */


int GALLOC_Stubs_CheckIfShareable(ggi_visual *vis,
				  ggiGA_resource_handle handle, 
				  ggiGA_resource_handle tocompare)
{
	/* Silence compiler warnings */
	vis = vis;
	handle = handle;
	tocompare = tocompare;

	return (GALLOC_EFAILED);
}	/* GALLOC_Stubs_CheckIfShareable */



/* Helper functions for ggiGAanprintf() implementation */
#include "../common/anprintf.inc"


int GALLOC_Stubs_anprintf(ggi_visual *vis, ggiGA_resource_list request, 
			  size_t size, const char *format, char **out)
{
	ggiGA_resource_handle ptr;
	char *result, *curr;
	int count;
	int xit;

	/* Silence compiler warnings */
        vis = vis;
	format = format;


	xit = empty_str(request, &size, out);
	if (xit) return size;


	/* This is the lazy person's implementation. */
	curr = result = calloc((size + 1), sizeof(char));
	count = 0;

	RESLIST_FOREACH(ptr, request) {
		int tsize;

#define POST_SNPRINTF \
		size -= tsize;   /* we know tsize <= size */ \
		if (size <= 0) break;			     \
		curr += tsize

		if (ptr->res_state & GA_STATE_SEEABOVE) {
			tsize = snprintf(curr, size, "^ %3i)", count++);
		} else {
			tsize = snprintf(curr, size, "* %3i)", count++);
		}
		POST_SNPRINTF;

		tsize = common_str(curr, size, ptr, vis);
		POST_SNPRINTF;

		tsize = snprintf(curr, size, "     TAG: %i",
				 ptr->res_state & GA_STATE_TAG_MASK);
		POST_SNPRINTF;

		if (ptr->master != NULL) {
			tsize = snprintf(curr, size, " master: %p",
					 (void *)ptr->master);
			POST_SNPRINTF;
		}
		if (ptr->left != NULL) {
			tsize = snprintf(curr, size, " left: ");
			POST_SNPRINTF;
			tsize = idx_str(curr, size, request, ptr->left);
			POST_SNPRINTF;
		}
		if (ptr->right != NULL) {
			tsize = snprintf(curr, size, " right: ");
			POST_SNPRINTF;
			tsize = idx_str(curr, size, request, ptr->right);
			POST_SNPRINTF;
		}
		tsize = snprintf(curr, size, "\n");
		POST_SNPRINTF;

		if ((ptr->props != NULL) && ggiGAIsMotor(ptr)) {
			tsize = motor_str(curr, size, ptr->props);
			POST_SNPRINTF;
		}
		if ((ptr->props != NULL) && ggiGAIsCarb(ptr)) {
			tsize = carb_str(curr, size, ptr->props);
			POST_SNPRINTF;
		}
		if ((ptr->props != NULL) && ggiGAIsTank(ptr)) {
			tsize = tank_str(curr, size, ptr->props);
			POST_SNPRINTF;
		}
		if (ptr->props != NULL) {
		  	tsize = snprintf(curr, size, 
					 "        Size: 2D(%i,%i) or 1D(%i)\n"
					 "        Quantity: %i  Flags %x\n"
					 "       Need:",
					 ptr->props->size.area.x, 
					 ptr->props->size.area.y, 
					 ptr->props->size.linear,
					 ptr->props->qty,
					 ptr->props->flags
					 );
			POST_SNPRINTF;
			tsize = storage_str(curr, size, 
					    ptr->props->storage_need);
 			POST_SNPRINTF;

			tsize = snprintf(curr, size, "\n       OK:   ");
			POST_SNPRINTF;
			tsize = storage_str(curr, size,
					    ptr->props->storage_ok);
 			POST_SNPRINTF;
			tsize = snprintf(curr, size, "\n");
 			POST_SNPRINTF;
		}	/* if */
		if (ptr->priv != NULL)	{
			tsize = snprintf(curr, size, 
					 "     +Private (size %lu)\n", 
					 (unsigned long int)ptr->priv_size);
			POST_SNPRINTF;
		}
		if ((ptr->res_type == GA_RT_FRAME) &&
		    (ptr->priv != NULL))
		{
			tsize = snprintf(curr, size,"     +Mode: ");
			POST_SNPRINTF;
			if (size > 128) {
			/* Dangerous... 128 is just a guess */
				ggiSPrintMode(curr,
					&(((struct ggiGA_mode *)
					(ptr->priv))->mode));
				tsize = strlen(curr);
				curr += tsize;
				size -= tsize;
				snprintf(curr, (size_t)2, "\n");
				curr++;
				size--;
			}	/* if */
			else break;
		}	/* if */
	}	/* foreach */

	*out = result;
	return (strlen(result));
}	/* GALLOC_Stubs_anprintf */


int GALLOC_Stubs__Mode(ggi_visual *vis, ggiGA_resource_handle *out)
{
	DPRINT("Function GALLOC_Stubs__Mode (%p, %p) called.\n", 
		     vis, out);

	/* Silence compiler warnings */
	vis = vis;

	/* People who call this should know better. */
	APP_ASSERT(out != NULL, "out == NULL");

	if (out[0] == NULL) {
		/* Create new resource and make GGI_AUTO */
		ggi_mode *tmp;

		out[0] = calloc((size_t)1, sizeof(struct ggiGA_resource));
		if (out[0] == NULL) goto err0;

		out[0]->priv = calloc((size_t)1, sizeof(struct ggiGA_mode));
		if (out[0]->priv == NULL) goto err1;

		out[0]->priv_size = sizeof(struct ggiGA_mode);
		tmp = &(((struct ggiGA_mode *)(out[0]->priv))->mode);
		tmp->frames    = GGI_AUTO;
		tmp->visible.x = GGI_AUTO;
		tmp->visible.y = GGI_AUTO;
		tmp->virt.x    = tmp->virt.y = GGI_AUTO;
		tmp->size.x    = tmp->size.y = GGI_AUTO;
		tmp->graphtype = GGI_AUTO;
		tmp->dpp.x     = tmp->dpp.y = GGI_AUTO;
	}	/* if */

	out[0]->res_type = GA_RT_FRAME;
	/* Set up db. */
	return GALLOC_OK;
 err1:
	free(out[0]);
	out[0] = NULL;
 err0:
	return(GALLOC_EUNAVAILABLE);
}	/* Galloc_Stubs__Mode */
