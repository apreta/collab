/* $Id: stubsgalloc.h,v 1.15 2007/12/21 09:30:47 cegger Exp $
******************************************************************************

   LibGAlloc implementation for "stubs" target -- header.



   Copyright (c) Thu Mar 22 2001 by:
	Brian S. Julin		bri@calyx.com



   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   The above copyright notice applies to all files in this package, unless
   explicitly stated otherwise in the file itself or in a file named COPYING
   in the same directory as the file.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.





******************************************************************************
*/

#include "config.h"
#include <ggi/internal/ggi-dl.h>
#include <ggi/internal/galloc.h>
#include <ggi/internal/galloc_debug.h>

/* size of carb rows for stubs-blt */
#define STUBS_GALLOC_BOB_CARB_ROWSIZE 4096

/*-* Function prototypes */

gallocfunc_set		GALLOC_Stubs_Set;
gallocfunc_release	GALLOC_Stubs_Release;
gallocfunc_checkIfShareable	GALLOC_Stubs_CheckIfShareable;
gallocfunc_anprintf	GALLOC_Stubs_anprintf;
gallocfunc_check	GALLOC_Stubs_Check;
gallocfunc_mode		GALLOC_Stubs__Mode;


int stubs_galloc_InitHaslist(struct galloc_context *ctxt);


ggiGA_reslist_callback stubs_galloc_check_mode;
ggiGA_reslist_callback stubs_galloc_check_cladbuf;
ggiGA_reslist_callback stubs_galloc_set_mode;

struct stubsgalloc_container_t {
	int state;
	ggi_mode mode;
};

/*-* Structure holding private data. */
