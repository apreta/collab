/* $Id: init.c,v 1.24 2009/06/09 07:35:15 cegger Exp $
******************************************************************************

   LibGAlloc implementation for "stubs" target -- Initialization.

   Copyright (c) Thu Mar 22 2001 by: 
	Brian S. Julin		bri@calyx.com
   
   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   The above copyright notice applies to all files in this package, unless 
   explicitly stated otherwise in the file itself or in a file named COPYING 
   in the same directory as the file.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.





******************************************************************************
*/

#include "stubsgalloc.h"

static const char *rtstrings[GA_RTNUM] = {GA_RTSTRINGS};
static const char *rtunsetstrings[1] = { "no subtype" };
static const char *rtframestrings[GA_RTFRAMENUM] = {GA_RTFRAMESTRINGS};
static const char *rtbufferstrings[GA_RTBUFFERNUM] = {GA_RTBUFFERSTRINGS};
static const char *rtbobstrings[GA_RTBOBNUM] = {GA_RTBOBSTRINGS};
static const char *rtspritestrings[GA_RTSPRITENUM] = {GA_RTSPRITESTRINGS};
static const char *rtvideostrings[GA_RTVIDEONUM] = {GA_RTVIDEOSTRINGS};
static const char *rtwindowstrings[GA_RTWINDOWNUM] = {GA_RTWINDOWSTRINGS};
static const char *rtmiscstrings[GA_RTMISCNUM] = {GA_RTMISCSTRINGS};
static const char *rtrendererstrings[GA_RTRENDERERNUM] = {GA_RTRENDERERSTRINGS};
static const char *rtresliststrings[GA_RTRESLISTNUM] = {GA_RTRESLISTSTRINGS};

static int GGIopen(struct ggi_visual *vis, struct ggi_dlhandle *dlh,
		   const char *args, void *argptr, uint32_t *dlret)
{
	struct gallocpriv *priv;
	DPRINT_LIBS("GGIopen(%p, %p, %s, %p, %p)"
			  " called for Stubs_GAlloc sublib\n",
			  vis, dlh, args ? args : "(NULL)", argptr, dlret);

	priv = LIBGGI_GALLOCEXT(vis);

	/*-* Initialize target-private data structure */

	/*-* Hook in target functions */

	if (priv->set == priv->dummy)
		priv->set = GALLOC_Stubs_Set;

	if (priv->release == priv->dummy)
		priv->release = GALLOC_Stubs_Release;

	if (priv->checkifshareable == priv->dummy)
		priv->checkifshareable = GALLOC_Stubs_CheckIfShareable;

	if (priv->anprintf == priv->dummy)
		priv->anprintf = GALLOC_Stubs_anprintf;

	if (priv->check == priv->dummy)
		priv->check = GALLOC_Stubs_Check;

	if (priv->_mode == priv->dummy)
		priv->_mode = GALLOC_Stubs__Mode;

	stubs_galloc_InitHaslist(priv);

	priv->rtstrings = rtstrings;
	priv->rtsubstrings = calloc(GA_RTNUM, sizeof(char **));
	priv->rtsubstrings[0] = rtunsetstrings;
	priv->rtsubstrings[1] = rtframestrings;
	priv->rtsubstrings[2] = rtbufferstrings;
	priv->rtsubstrings[3] = rtbobstrings;
	priv->rtsubstrings[4] = rtspritestrings;
	priv->rtsubstrings[5] = rtvideostrings;
	priv->rtsubstrings[6] = rtwindowstrings;
	priv->rtsubstrings[7] = rtmiscstrings;
	priv->rtsubstrings[8] = rtrendererstrings;
	priv->rtsubstrings[9] = rtresliststrings;

	if (priv->default_callbacks[0] == priv->dummy)
		priv->default_callbacks[0] = ggiGA_default_match;
	if (priv->default_callbacks[1] == priv->dummy)
		priv->default_callbacks[1] = ggiGA_default_precheck;
	if (priv->default_callbacks[2] == priv->dummy)
		priv->default_callbacks[2] = ggiGA_default_check;
	if (priv->default_callbacks[3] == priv->dummy)
		priv->default_callbacks[3] = ggiGA_default_check;
	if (priv->default_callbacks[4] == priv->dummy)
		priv->default_callbacks[4] = ggiGA_default_uncheck;
	if (priv->default_callbacks[5] == priv->dummy)
		priv->default_callbacks[5] = ggiGA_default_check;
	if (priv->default_callbacks[6] == priv->dummy)
		priv->default_callbacks[6] = ggiGA_default_set;
	if (priv->default_callbacks[7] == priv->dummy)
		priv->default_callbacks[7] = ggiGA_default_unset;
	if (priv->default_callbacks[8] == priv->dummy)
		priv->default_callbacks[8] = ggiGA_default_release;

	*dlret = GGI_DL_EXTENSION;
	return 0;
}	/* GGIopen */


static int GGIclose(struct ggi_visual *vis, struct ggi_dlhandle *dlh)
{
	DPRINT_LIBS("GGIdlclose(%p, %p) called for StubsGAlloc sublib\n",
			  vis, dlh);

	/*-* Free target-private structure */


	return 0;
}	/* GGIclose */


EXPORTFUNC
int GALLOCdl_stubs_galloc(int func, void **funcptr);

int GALLOCdl_stubs_galloc(int func, void **funcptr)
{
	ggifunc_open **openptr;
	ggifunc_close **closeptr;

	switch (func) {
	case GGIFUNC_open:
		openptr = (ggifunc_open **)funcptr;
		*openptr = GGIopen;
		return 0;
	case GGIFUNC_exit:
		*funcptr = NULL;
		return 0;
	case GGIFUNC_close:
		closeptr = (ggifunc_close **)funcptr;
		*closeptr = GGIclose;
		return 0;
	default:
		*funcptr = NULL;
	}	/* switch */
      
	return GGI_ENOTFOUND;
}	/* GALLOCdl_stubs_galloc */
