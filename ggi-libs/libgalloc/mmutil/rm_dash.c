/* $Id: rm_dash.c,v 1.4 2007/06/24 13:59:11 aldot Exp $
******************************************************************************

   Core algorithms for management of "2D" range regions.

   Copyright (C) 2003 Brian S. Julin    [bri@calyx.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

/* Define this to make this file a standalone program to test algorithm
 * consistency.
 */
#undef CHECK


/* You can perhaps visualize these functions most easily as answering 
 * the following question: "Will a dashed line segment starting at s2 with 
 * h2+1 dashes that are w2+1 units long occurring every p2 units fit inside 
 * a dashed line segment starting at s1 with h1+1 dashes that are w1+1 units 
 * long occurring every p1 units?"
 *
 * The actual application we use it for is graphics memory management where
 * the dashed lines represent buffers in VRAM with start-points, width, 
 * height, and pitch.  The algorithm can also probably find a use in 
 * real-time scheduling and operations research (cutting and packing problems).
 *
 * This should iterate at most as many times as Euclid's Algorithm, 
 * (noting that Euclid's Algorithm is concurrently performed and when 
 * finished terminates the recursion) which for 32-bit numbers works out to 
 * I think somewhere under 26 iterations, if I did the math right.  So say 
 * 32 to be safe from my horrible math.
 *
 * What follows, commented out, is a crude rolled-up recursive 
 * implementation which is easier to understand than the actual 
 * implementation further below, which is unrolled and takes pains 
 * to prevent numerical overflow.
 *
 */


#if 0

unsigned int lev;

unsigned int dash_reduce(unsigned int s2, unsigned int p2, 
			 unsigned int w, unsigned int p1) {
	unsigned int dashes;

        lev++;
	if (s2 > w) return 1;
	if (!p2) return 0;
	if (p2 == 1) return (w - s2 + 2);
	if (p1 > p2 + w) return ((w - s2)/p2 + 2);
	dashes = dash_reduce((w - s2) % p2, p1 % p2, p2 + w - p1, p2);
	if (dashes == 0) return 0;
	return (((dashes-1) * p1 + w - s2)/p2 + 2);
}

int optimal_dash(unsigned int s1, unsigned int p1, 
		 unsigned int w1, unsigned int h1, 
		 unsigned int s2, unsigned int p2, 
		 unsigned int w2, unsigned int h2, int *ev) {
	unsigned int w, len;

	/* Handle some trivial cases */
	if (s2 < s1) return -1;
	if (s2 + p2 * h2 + w2 > s1 + p1 * h1 + w1) return -1;
	if (p1 <= w1 + 1) return 0;
	if (w2 > w1) return -1;
	if (p2 < 2) {
	  if (s2 + p2 * h2 + w2 <= s1 + w1) return 0;
	  return -1;
	}

	lev = 0;
	len = dash_reduce(s2 - ((s2 - s1)/p1 * p1), p2, w1 - w2, p1);
	*ev = lev;

	if (!len) return 0;
	if (len > h2 + 1) return 0;
	return -1;
}

#endif

int mm_dashfit_raw(unsigned int s1, unsigned int p1, 
		   unsigned int w1, unsigned int h1, 
		   unsigned int s2, unsigned int p2, 
		   unsigned int w2, unsigned int h2) {
	unsigned int i, tmp, w;
	unsigned int r[33],s[32];

	/* WARNING:
	 * It is assumed here that all values have been checked
	 * to prevent overflow.  See mm_dashfit below. 
	 */

	/* Handle some trivial cases */
	if (s2 < s1) return -1;
	if (s2 + p2 * h2 + w2 > s1 + p1 * h1 + w1) return -1;
	if (!(w1 + 1)) return 0;
	if (p1 <= w1 + 1) return 0; /* hence w1 <= max int - 2 */
	if (w2 > w1) return -1;
	if (p2 < 2) {
		s2 = s1 + ((s2 - s1) % p1);
		if (s2 + p2 * h2 + w2 <= w1) return 0;
		return -1;
	}

	s[0] = s1 + ((s2 - s1) % p1);
	r[0] = p1;
	r[1] = p2 % p1;
	w = w1 - w2;

	i = 0;
	do {
		unsigned int r1;
		r1 = r[i+1];

		/* FYI: w == w1 - w2 - p1 + r[i] */
		if (s[i] > w) { 
			tmp = 0;
			goto finish;
		}
		if (!r1) return 0;
		if (r1 == 1) {
			tmp = (w - s[i] + 1);
			goto finish;
		}
		if (r[i] - r1 > w) {
			tmp = ((w - s[i])/r1 + 1);
			goto finish;
		}
		s[i+1] = (w - s[i]) % r1;
		r[i+2] = r[i] % r1;
		w = w + r1 - r[i];
		
	} while (++i < 32);
	
	return -1;


 finish:
	w = p1 - w1 + w2;
	while(i--) {
		unsigned int tmp2;
		tmp2 = (tmp + 1) * r[i];
		if (tmp + 1 != tmp2 / r[i]) return 0;
		tmp = (tmp2 - w - s[i])/r[i+1] + 1;
	}

	/* Note, tmp contains now the number of dashes that fit, 
	 * except note that in certain cases we returned 0 above
	 * when the first "collision point" was further than 
	 * the maximum interger value.  So a function that gives a 
	 * more detailed return code would be possible if needed.
	 */
	if (tmp >= h2 + 1) return 0;	
	return -1;
}

int mm_dashfit(unsigned int s1, unsigned int p1, 
	       unsigned int w1, unsigned int h1, 
	       unsigned int s2, unsigned int p2, 
	       unsigned int w2, unsigned int h2) {

	unsigned int tmp;

	/* Check for conditions that would cause overflow */
	if (s2 + w2 < s2) return -1;
	if (s1 + w1 < s1) return -1;
	tmp = p1 * h1;
	if (tmp && (tmp/h1 != p1)) return -1;
	if (s1 + w1 + tmp < s1 + w1) return -1;
	tmp = p2 * h2;
	if (tmp && (tmp/h2 != p2)) return -1;
	if (s2 + w2 + tmp < s2 + w2) return -1;

	return mm_dashfit_raw(s1, p1, w1, h1, s2, p2, w2, h2);
}

#ifdef CHECK 

#include <stdio.h>
#include <stdlib.h>	/* needed for exit() */

#ifndef MAXSZ
#define MAXSZ 30
#endif

/* The brain-dead inefficient solution, for testing consistency. */
int trivial_dash(unsigned int s1, unsigned int p1, 
		 unsigned int w1, unsigned int h1, 
		 unsigned int s2, unsigned int p2, 
		 unsigned int w2, unsigned int h2) {

	unsigned int tmp;

	/* Check for conditions that would cause overflow */
	if (s2 + w2 < s2) return -1;
	if (s1 + w1 < s1) return -1;
	tmp = p1 * h1;
	if (tmp && (tmp/h1 != p1)) return -1;
	if (s1 + w1 + tmp < s1 + w1) return -1;
	tmp = p2 * h2;
	if (tmp && (tmp/h2 != p2)) return -1;
	if (s2 + w2 + tmp < s2 + w2) return -1;

	if (s2 < s1) return -1;
	if (s2 + p2 * h2 + w2 > s1 + p1 * h1 + w1) return -1;
	if (p1 <= w1 + 1) return 0;
	do { 
	  while (s2 + w2 > s1 + w1) {
	    s1 += p1;
	    h1--;
	    if (!(h1 + 1)) return -1;
	  }
	  if (s2 < s1) return -1;
	  s2 += p2;
	} while(h2--);
	return 0;
}

/* Testing code -- tries values at low and high end to check for overflow. */

int main (void) {

  unsigned int s1, p1, w1, h1;
  unsigned int s2, p2, w2, h2;
  int maxeval = 0;

  for (p1 = 0; p1 + 1 > 0; p1++) {
    if (p1 == MAXSZ) p1 = 0 - MAXSZ;

    for (w1 = 0; w1 + 1 > 0; w1++) {
      if (w1 == MAXSZ) w1 = 0 - MAXSZ;

      for (h1 = 0; h1 * p1 < MAXSZ ; h1++) {

	for (s2 = 0; s2 + 1 > 0; s2++) {
	  if (s2 == MAXSZ) s2 = 0 - MAXSZ;

	  for (p2 = 0; p2 + 1 > 0; p2++) {
	    if (p2 == MAXSZ) p2 = 0 - MAXSZ;

	    for (h2 = 0; h2 * p2 < MAXSZ; h2++) {
	      int t, o, ev;
	      ev = 0;
	      o = mm_dashfit(0,p1,w1,h1,s2,p2,0,h2);
	      t = trivial_dash(0,p1,w1,h1,s2,p2,0,h2);
	      if (ev > maxeval) maxeval = ev;
	      if (t != o) {
		fprintf(stderr, "failed at %u %u %u %u, %u %u %u %u (trivial %s, optimal %s)\n",
			0,p1,w1+1,h1+1,s2,p2,1,h2+1,
			t ? "no" : "yes", o ? "no" : "yes");
		
		exit(-1);
	      }
	      if (p2 == 0) break;
	    }
	  }
	}
	if (p1 == 0) break;  
      }
    }
  }
  fprintf(stderr, "maxeval was %i\n", maxeval);
  return 0;
}

#endif

/* 

This is a graphical representation of how the core algorithm works.  It
does not care about how many dashes each line contains, that is
sorted out after the core has determined how many will fit.


** Recursion level 0 **
Starting with these two lines, which do not fit (at the 
eighth dash of line 2):
_____________  _____________  _____________  ____________
 __      __      __      __      __      __      __      __   

Convert to an equivalent problem where the second line dashes
are only one unit wide:
____________   ____________   ____________   ____________
 _       _       _       _       _       _       _       _   

** Recursion level 1 **
Swap and invert the two lines.  We are now looking to see how
many gaps in the first line above fit inside gaps in the second
line above -- which if you ponder it for a bit you will realize that
this tells us which gap the non-fitting condition occurs in.  We also 
discard the partial gap at the start of line 2 above:

  _______ _______ _______ _______ _______ _______ _______
            ___            ___            ___            ___ 

Convert to an equivalent problem where the second line dashes
are only one unit wide:
  _____   _____   _____   _____   _____   _____   _____  
            _              _              _              _  

Convert to an equivalent problem where the inter-dash spacing
in the second line is smaller than that of the first line.
(We did not have to do this above because this condition was
already met.)
  _____   _____   _____   
    _      _      _      _

** Recursion level 2 **
Swap and invert the two lines again as above.
     ______ ______ ______ ______                                         
       ___     ___     ___                                               

Convert to an equivalent problem where the second line dashes
are only one unit wide:
     ____   ____   ____   ____                                           
       _       _       _                                                 

Convert to an equivalent problem where the inter-dash spacing
in the second line is smaller than that of the first line:                                                                         
     ____   ____   ____   ____                                
       ___

When the second dashed line has 0 space between its dashes, we 
have reached our termination point.  The above steps might have been 
repeated many times if the line had not reduced so quickly.  It is
a simple subtraction to calculate that above, two dashes fit and 
the non-fit condition is at the third dash.  We feed this back up 
to the next level, and it is figured out there how many dashes occur
before the "failure gap", which is possible now that we know 
which gap it is.  Then this is passed upward and so on, until
out of the top pops the final answer.

*/

