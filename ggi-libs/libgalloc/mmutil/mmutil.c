/* $Id: mmutil.c,v 1.25 2005/07/30 12:14:20 cegger Exp $
******************************************************************************

   mmutils.c:  A reusable mini-library with some pretty handy utilities:

   1) a generic 2D memory range manager (No 2D yet, just 1D)
   2) a "batch operation" core

   Copyright (C) 2001 Christoph Egger   [Christoph_Egger@t-online.de]
   Copyright (C) 2001 Brian S. Julin    [bri@calyx.com]
   Copyright (C) 2001 Eric Faurot       [eric.faurot@gmail.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/mmutil.h>
#include <string.h>


/*****************************************************************************
 * Range Manager
 */
#if 0
#define MYREALLOC(a,b) realloc(a,b);  printf("reallocated : %d\n",b);
#else
#define MYREALLOC(a,b) realloc(a,b);
#endif

#if 0
#define DEBUG_CHUNK_USE
#define DEBUG_CHUNK_INSERT
#endif

#define RM_DEBUG

/*****************************************************************************
 * Chunks
 */

#define MIN(a,b) (a<b?a:b)
#define MAX(a,b) (a>b?a:b)

int 
rm_chunk_init(rm_chunk_list_t ** cl, size_t size)
{

	*cl = calloc((size_t)1, sizeof(**cl));

	if (*cl == NULL)
		return RM_ENOMEM;

	(*cl)->chunk =
		calloc((size_t)RM_ALLOC_STEP, sizeof(*((*cl)->chunk)));

	if ((*cl)->chunk == NULL) {
		free(*cl);
		*cl = NULL;
		return RM_ENOMEM;
	}
	(*cl)->allocated = RM_ALLOC_STEP;
	(*cl)->count = 1;
	(*cl)->chunk[0].offset = 0;
	(*cl)->chunk[0].length = size;
	(*cl)->size = size;

	return RM_OK;
}

int 
rm_chunk_exit(rm_chunk_list_t ** cl)
{
	free((*cl)->chunk);
	free(*cl);
	*cl = NULL;

	return RM_OK;
}

/**
 * Inserts a chunk at index idx.
 */
static int 
rm_chunk_insert(rm_chunk_list_t * cl, int idx, size_t s, size_t w)
{
#ifdef DEBUG_CHUNK_INSERT
	printf("inserting chunk (%i,%i) at index %i. %i chunk(s) moved forward.\n",
	       s, w, idx, cl->count - idx);
#endif
	if (cl->allocated == cl->count) {
		rm_chunk_t     *ptr;
		ptr = MYREALLOC(cl->chunk,
		      sizeof(*cl->chunk) * (cl->allocated + RM_ALLOC_STEP));
		if (ptr == NULL)
			return RM_ENOMEM;
		cl->chunk = ptr;
		cl->allocated += RM_ALLOC_STEP;
	}
	memmove(&(cl->chunk[idx + 1]), &(cl->chunk[idx]), sizeof(*(cl->chunk)) * (cl->count - idx));
	CHK_START(cl, idx) = s;
	CHK_WIDTH(cl, idx) = w;
	cl->count++;
	return 0;
}

/**
 * Remove chunk at index idx;
 */
static int 
rm_chunk_remove(rm_chunk_list_t * cl, int idx)
{
	cl->count--;
#ifdef DEBUG_CHUNK_INSERT
	printf("removing chunk at index %i. %i chunk(s) moved back.\n", idx, cl->count - idx);
#endif
	memmove(&cl->chunk[idx], &cl->chunk[idx + 1], sizeof(*cl->chunk) * (cl->count - idx));
	if (cl->allocated == cl->count + RM_ALLOC_STEP && cl->count != 0) {
		rm_chunk_t     *ptr;
		ptr = MYREALLOC(cl->chunk,
		      sizeof(*cl->chunk) * (cl->allocated - RM_ALLOC_STEP));
		if (ptr == NULL)
			return RM_ENOMEM;
		cl->chunk = ptr;
		cl->allocated -= RM_ALLOC_STEP;
	}
	return 0;
}

/* Frees an allocated chunk. The chunk must be used in the current rl */
int 
rm_chunk_free(rm_chunk_list_t * rl, size_t start, size_t width)
{
	int             op;
	int             i;

	/*
	 * cases: - a new free chunk is created.          ( +1 ) - an existing
	 * free chunk gets bigger.   ( +0 ) - 2 free chunks are merged.
	 * ( -1 )
	 * 
	 */

#ifdef DEBUG_CHUNK_USE
	printf("CHUNK_LIST %p : freeing mem at offset %i, width %i.\n", (void *)rl, start, width);
#endif
	op = 0;

	for (i = 0; i < rl->count; i++) {
		if (CHK_BEFORE(rl, i, start))
			break;
	}

	if (i < rl->count) {	/* there is a free chunk after this one */
#ifdef DEBUG_CHUNK_USE
		printf(" - before chunk %i : start = %i, width = %i\n", i, CHK_START(rl, i), CHK_WIDTH(rl, i));
#endif

		/* the chunk must finish before the next free chunk */
		if (!CHK_BEFORE(rl, i, start + width)) {
#ifdef DEBUG_CHUNK_USE
			printf(" * the chunk must finish before the next free chunk (%i / %i)\n", start + width, CHK_END(rl, i));
#endif

			return -1;
		}
		/* we can extend the next chunk */
		if (CHK_START(rl, i) == start + width)
			op += 2;

	} else {		/* there is no free chunk after this one */

		if (start + width > rl->size) {

#ifdef DEBUG_CHUNK_USE
			printf(" * the chunk is to big : %i / %i\n", start + width, rl->size);
#endif

			return -1;
		}
	}

	if (i > 0) {		/* there is a chunk before */
		if (CHK_END(rl, i) + 1 == start) {	/* we just extend the
							 * previous chunk */
			op += 1;
		}
	}
	switch (op) {
	case 0:
		rm_chunk_insert(rl, i, start, width);
		break;
	case 1:
		CHK_WIDTH(rl, i - 1) += width;
		break;
	case 2:
		CHK_WIDTH(rl, i) += width;
		CHK_START(rl, i) = start;
		break;
	case 3:
		CHK_WIDTH(rl, i - 1) += width + CHK_WIDTH(rl, i);
		rm_chunk_remove(rl, i);
		break;
	}
	return 0;
}

/* Removes a chunk from rl. The chunk must be free in the current rl */
int 
rm_chunk_use(rm_chunk_list_t * rl, size_t start, size_t width)
{
	size_t          s1, s2, w1, w2;
	int             i;

	/*
	 * cases: - a free chunk is split in two.        ( +1 ) - an existing
	 * free chunk gets smaller. ( +0 ) - an existing free chunk disapears
	 * ( -1 )
	 * 
	 */


#ifdef DEBUG_CHUNK_USE
	printf("CHUNK_LIST %p : using mem at offset %i, width %i (end at %i).\n",
			(void *)rl, start, width, start + width - 1);
#endif
	/* find the first index concerned. */
	for (i = 0; i < rl->count; i++) {
		if (CHK_IN(rl, i, start))
			break;
	}

	if (i == rl->count) {
#ifdef DEBUG_CHUNK_USE
		printf(" * there should be a free chunk containing start\n");
#endif
		return -1;
	}
#ifdef DEBUG_CHUNK_USE
	printf(" - using chunk %i : start = %i, width = %i\n", i, CHK_START(rl, i), CHK_WIDTH(rl, i));
#endif

	if (!CHK_IN(rl, i, start + width - 1)) {
#ifdef DEBUG_CHUNK_USE
		printf(" * the chunk should also contain the end (%i / %i)\n", start + width - 1, CHK_END(rl, i));
#endif
		return -1;
	}
	if (CHK_WIDTH(rl, i) == width) {
		/* case 3 : the chunk is simply removed */
		rm_chunk_remove(rl, i);
		return 0;
	}
	if (CHK_START(rl, i) == start) {
		CHK_WIDTH(rl, i) -= width;
		CHK_START(rl, i) += width;
		return 0;
	}
	if (CHK_END(rl, i) == start + width - 1) {
		CHK_WIDTH(rl, i) -= width;
		return 0;
	}
	/* we have to split the chunk in two chunks. */

	s1 = CHK_START(rl, i);
	w1 = start - s1;

	s2 = start + width;
	w2 = CHK_END(rl, i) - s2 + 1;

	CHK_WIDTH(rl, i) = w2;
	CHK_START(rl, i) = s2;
	return rm_chunk_insert(rl, i, s1, w1);
}

void 
rm_chunk_print(rm_chunk_list_t * cl)
{
	size_t          remaining;
	int             i;
	remaining = 0;
	printf("RM_CHUNK %p\n", (void *)cl);
	printf("chunks : %i / %i\n", cl->count, cl->allocated);
	printf("--------------------------------------\n");
	for (i = 0; i < cl->count; i++) {
		printf("[ %lu - %lu (%lu) ]\n",
			(unsigned long int)CHK_START(cl, i),
			(unsigned long int)CHK_END(cl, i),
			(unsigned long int)CHK_WIDTH(cl, i));
		remaining += CHK_WIDTH(cl, i);
	}
	printf("--------------------------------------\n");
	printf("Free memory : %lu / %lu\n",
		(unsigned long int)remaining,
		(unsigned long int)cl->size);
}

int 
rm_chunk_allocate_range(rm_chunk_list_t * cl, rm_range_t * r, size_t relpad)
{
	size_t          i;
	size_t          offset;
	if (r->attribute.stride == r->attribute.width) {	/* padding is null */
		return rm_chunk_use(cl, r->attribute.start, r->attribute.width * r->attribute.height);
	}
	/* non zero padding */
	if (r->attribute.stride - r->attribute.width < relpad) {
		/* if the padding is not relevant, allocate as a whole */
		return rm_chunk_use(cl, r->attribute.start, (r->attribute.stride * r->attribute.height) - (r->attribute.stride - r->attribute.width));
	}
	/* else allocates normally */
	offset = r->attribute.start;
	for (i = 0; i < r->attribute.height; i++) {
		if (rm_chunk_use(cl, offset, r->attribute.width) < 0)
			return -1;
		offset += r->attribute.stride;
	}
	return 0;
}

int 
rm_chunk_free_range(rm_chunk_list_t * cl, rm_range_t * r, size_t relpad)
{
	size_t          i;
	size_t          offset;
	if (r->attribute.stride == r->attribute.width) {
		return rm_chunk_free(cl, r->attribute.start, r->attribute.width * r->attribute.height);
	}
	/* non zero padding */
	if (r->attribute.stride - r->attribute.width < relpad) {
		/* if the padding is not relevant, frees as a whole */
		return rm_chunk_free(cl, r->attribute.start, (r->attribute.stride * r->attribute.height) - (r->attribute.stride - r->attribute.width));
	}
	/* else frees normally */
	offset = r->attribute.start;
	for (i = 0; i < r->attribute.height; i++) {
		if (rm_chunk_free(cl, offset, r->attribute.width) < 0)
			return -1;
		offset += r->attribute.stride;
	}
	return 0;
}

/*****************************************************************************
 * Range Manager
 */
void 
rm_range_print(rm_range_list_t * r, int i)
{
	printf("---------------------------\n");
	printf("- dimensions  : %lux%lu\n",
		(unsigned long int)RM_WIDTH(r, i),
		(unsigned long int)RM_HEIGHT(r, i));
	if (RM_CHECK_FLAGS(r, i, RANGE_RELOCATABLE)) {
		printf("- relocatable\n");
	} else {
		printf("- not relocatable\n");
	}
	printf("- constraints :");
	if (RM_CHECK_FLAGS(r, i, RANGE_START_MIN)) {
		printf(" start_min=%lu",
			(unsigned long int)RM_CSTR_START_MIN(r, i));
	}
	if (RM_CHECK_FLAGS(r, i, RANGE_START_MAX)) {
		printf(" start_max=%lu",
			(unsigned long int)RM_CSTR_START_MAX(r, i));
	}
	if (RM_CHECK_FLAGS(r, i, RANGE_START_ALIGN)) {
		printf(" start_align=%lu",
			(unsigned long int)RM_CSTR_START_ALIGN(r, i));
	}
	if (RM_CHECK_FLAGS(r, i, RANGE_STRIDE_ALIGN)) {
		printf(" stride_align=%lu",
			(unsigned long int)RM_CSTR_STRIDE_ALIGN(r, i));
	}
	printf("\n");
	if (RM_CHECK_FLAGS(r, i, RANGE_OK_MASK)) {
		printf("- located at offset %lu with stride %lu, end at %lu.\n",
			(unsigned long int)RM_START(r, i),
			(unsigned long int)RM_STRIDE(r, i),
			(unsigned long int)RM_END(r, i));
	} else {
		printf("- not allocated\n");
	}
	printf("\n");
}

int
rm_list_init(rm_range_list_t ** rl)
{

	*rl = calloc((size_t)1, sizeof(**rl));

	if (*rl == NULL)
		return RM_ENOMEM;

	(*rl)->range =
		calloc((size_t)RM_ALLOC_STEP, sizeof(*((*rl)->range)));

	if ((*rl)->range == NULL) {
		free(*rl);
		*rl = NULL;
		return RM_ENOMEM;
	}
	(*rl)->allocated = RM_ALLOC_STEP;
	(*rl)->count = 0;
	return RM_OK;
}


int 
rm_list_exit(rm_range_list_t ** rl)
{
	free((*rl)->range);
	free(*rl);
	*rl = NULL;

	return RM_OK;
}

int 
rm_list_remove(rm_range_list_t * rl, int idx)
{
	if (idx >= rl->count)
		return RM_EINDEX;
	memmove(&rl->range[idx + 1], &rl->range[idx], (idx + 1 - rl->count) * sizeof(*rl->range));
	if ((--rl->count) + RM_ALLOC_STEP > rl->allocated) {
		rm_range_t     *ptr;
		ptr = MYREALLOC(rl->range,
		      sizeof(*rl->range) * (rl->allocated - RM_ALLOC_STEP));
		if (ptr != NULL) {
			rl->range = ptr;
			rl->allocated -= RM_ALLOC_STEP;
		}
	}
	return RM_OK;
}

/* add range to a range list */
int 
rm_list_add(rm_range_list_t * rl, rm_range_t * rm)
{
	if (rl->count == rl->allocated) {
		rm_range_t     *ptr;
		ptr = MYREALLOC(rl->range,
		      sizeof(*rl->range) * (rl->allocated + RM_ALLOC_STEP));
		if (ptr != NULL) {
			rl->range = ptr;
			rl->allocated += RM_ALLOC_STEP;
		} else {
			return RM_ENOMEM;
		}
	}
	rl->range[rl->count] = *rm;
	return rl->count++;	/* return the index */
}

void 
rm_list_print(rm_range_list_t * rl, size_t size)
{
	size_t          remaining;
	int             i;
	remaining = size;
	printf("Memory chunk %p\n", (void *)rl);
	printf("Ranges : %i / %i\n", rl->count, rl->allocated);
	printf("--------------------------------------\n");
	for (i = 0; i < rl->count; i++) {
		remaining -= RM_WIDTH(rl, i) * RM_HEIGHT(rl, i);
		rm_range_print(rl, i);
	}
	printf("Free memory : %lu / %lu\n",
		(unsigned long int)remaining,
		(unsigned long int)size);
}

static int 
compare_setfirst(const void *aa, const void *bb)
{
	const rm_range_t     *a, *b;
	size_t          starta, startb;
	size_t          sizea, sizeb;

	a = (const rm_range_t *) aa;
	b = (const rm_range_t *) bb;

	/* a < b => -1  */

	/*
	
	  Invalid ranges are always bigger
	
	*/
	if (a->attribute.flags & RANGE_KO_MASK) {
		if (b->attribute.flags & RANGE_KO_MASK)
			return 0;
		return 1;
	}
	if (b->attribute.flags & RANGE_KO_MASK)
		return -1;

	starta = a->constraint.start_max - a->constraint.start_min;
	startb = b->constraint.start_max - b->constraint.start_min;
	sizea = a->attribute.stride * a->attribute.height - (a->attribute.stride - a->attribute.width);
	sizeb = b->attribute.stride * b->attribute.height - (b->attribute.stride - b->attribute.width);

	/*

	  Now decide which should be set first.

	 */
	if (starta == startb) {	/* same start gap */
		if (sizea > sizeb)
			return -1;	/* a is bigger, should go first. */
		if (sizea < sizeb)
			return 1;	/* a is bigger, should go first. */

		return 0;
	}
	if (starta < startb)
		return -1;	/* 'a' start gap is more restricted, should
				 * go first. */
	return 1;
}

int 
rm_list_alloc(rm_range_list_t * rl, rm_range_list_t * req)
{
	int             i;

	for (i = 0; i < req->count; i++) {
		if (RM_CHECK_FLAGS(req, i, RANGE_ALLOCATED)) {
			rm_list_add(rl, &req->range[i]);
		}
	}
	return 0;
}

int 
rm_list_fit(rm_range_list_t * rl, rm_range_list_t * req, size_t max)
{
	int             i, j;
	rm_chunk_list_t *cl;
	size_t          currelpad, marea;
	size_t          start_min, start_max;
	size_t          waste_left, waste_right;

#ifdef RM_DEBUG
	const char           *debug_waste_pol;
	size_t          debug_waste;

	printf("Starting rm_list_fit.\n");

#endif

	/*
	   At first get rid of the ones that will not fit anyway.
	*/

	currelpad = RM_WIDTH(req, 0);
	for (i = 0; i < req->count; i++) {

		/*

		  Make sure that the result flags are clean

		 */

		RM_UNSET_FLAGS(req, i, RANGE_ANSWER_MASK);

		/*
		
		  For uniformity in following treatments, it might be a good choice to set
		  the non-constrained attributes with the most tolerant values.
		  -> min_start : 0 max_end : size, align: 0 (means any, 1 too actually...).
		
		 */

		if (!RM_CHECK_FLAGS(req, i, RANGE_STRIDE_ALIGN))
			RM_CSTR_STRIDE_ALIGN(req, i) = 1;
		if (!RM_CHECK_FLAGS(req, i, RANGE_START_ALIGN))
			RM_CSTR_START_ALIGN(req, i) = 1;
		if (!RM_CHECK_FLAGS(req, i, RANGE_START_MIN))
			RM_CSTR_START_MIN(req, i) = 0;
		if (!RM_CHECK_FLAGS(req, i, RANGE_START_MAX))
			RM_CSTR_START_MAX(req, i) = (max - 1) - ((max - 1) % RM_CSTR_START_ALIGN(req, i));

		/*
		
		  If the stride is negotiatable, it is set to it's minimum
		
		*/

		if (!RM_CHECK_FLAGS(req, i, RANGE_STRIDE_FIXED)) {
			RM_STRIDE(req, i) = RM_WIDTH(req, i);
			if (RM_WIDTH(req, i) % RM_CSTR_STRIDE_ALIGN(req, i))
				RM_STRIDE(req, i) += RM_CSTR_STRIDE_ALIGN(req, i) -
					(RM_WIDTH(req, i) % RM_CSTR_STRIDE_ALIGN(req, i));
		}
		/*
		
		  The validity of the range should be checked

		*/

		if (RM_CSTR_START_MAX(req, i) < RM_CSTR_START_MIN(req, i)) {
#ifdef RM_DEBUG
			printf("range %i is invalid.\n", i);
#endif
			RM_SET_FLAGS(req, i, RANGE_INVALID);
			continue;
		}
		/*
		
		  Test if it's not to big. Probably useless sice it will be detected later.
		
		*/

		if (RM_SURFACE(req, i) + RM_CSTR_START_MIN(req, i) > max) {
#ifdef RM_DEBUG
			printf("range %i can't fit in the given memory : %lu / %lu\n", i,
				(unsigned long int)(RM_SURFACE(req, i) + RM_CSTR_START_MIN(req, i)),
				(unsigned long int)max);
#endif
			RM_SET_FLAGS(req, i, RANGE_NO_FIT);
			continue;
		}
		/*
		
		  See if we need to refine the currrelpad.
		
		*/

		if (RM_WIDTH(req, i) < currelpad)
			currelpad = RM_WIDTH(req, i);

		/*
		  The following code could be inserted to invalidate the ranges that
		  collide with a non-relocatable chunk.
		*/
		/*
		for(j=0;j<rl->count;j++) {
			if(RM_CHECK_FLAGS(rl,j,RANGE_RELOCATABLE)) {
				if(rm_range_compatible(cl,j,req,j)==0) {
					RM_SET_FLAGS(res,i,FLAG_NOFIT);
					break;
				}
			}
		}
		*/

	}			/* for */

	/*
	
	  First, compute the free chunks.

	*/

	rm_chunk_init(&cl, max);
	for (j = 0; j < rl->count; j++) {
		rm_chunk_allocate_range(cl, &rl->range[j], currelpad - 1);
	}

	/*
	
	  Sort the ranges. The most constrained ones come first.
	
	*/

	qsort(req->range, (size_t)req->count, sizeof(*req->range),
		compare_setfirst);

	/*
	 * starting from first range in the request list, try to pack it as
	 * much as possible to the right.
	 */

#ifdef RM_DEBUG
	printf("Ranges sorted.\n");
#endif

	for (i = 0; i < req->count; i++) {

		/* The invalid ranges are in the end of the queue */
		if (RM_CHECK_FLAGS(req, i, RANGE_KO_MASK))
			break;

		/*
		
		  The minimum stride is already set.
		  Compute the minimum area required by this range.
		
		*/
#ifdef RM_DEBUG
		printf("Fitting range %i.\n", i);
		rm_range_print(req, i);
#endif

		marea = RM_SURFACE(req, i);

		/*
		
		  Starting at the right most free chunk, try to allocate the range.
		
		*/

		for (j = cl->count - 1; j >= 0; j--) {

			/* First if it has a chance to fit */

			if (CHK_WIDTH(cl, j) < marea)
				continue;

			/*
			 * First, compute the start boundary for this
			 * chunk...
			 */

			start_min = MAX(RM_CSTR_START_MIN(req, i), CHK_START(cl, j));
			start_max = MIN(RM_CSTR_START_MAX(req, i), CHK_END(cl, j) - (marea - 1));

			/*
			 * ... and adjust for the start alignement and area
			 * requirement
			 */

			if (start_min % RM_CSTR_START_ALIGN(req, i))
				start_min += RM_CSTR_START_ALIGN(req, i)
					- (start_min % RM_CSTR_START_ALIGN(req, i));

			start_max -= start_max % RM_CSTR_START_ALIGN(req, i);

			/*
			  Now it fails if :
			  - min is greater than max
			  - the new start min is over the range max
			  - the new start max is below the range min
			  - the new start min is over the chunk end
			  - the new start max is below the chunk start
			*/

			if (start_min > start_max)
				continue;
			if ((start_min > RM_CSTR_START_MAX(req, i)) ||
			    (start_min > CHK_END(cl, j)) ||
			    (start_max < RM_CSTR_START_MIN(req, i)) ||
			    (start_max < CHK_START(cl, j)))
				continue;

			/*
			 * We found a free space. We try to allocate it.
			 * Compute the space wasted for the right most and
			 * left most allocation, then choose the smallest
			 * one, or right if equal (right most policy)
			 */

			waste_left = start_min - CHK_START(cl, j);
			waste_right = CHK_END(cl, j) - (start_max + marea - 1);
#ifdef RM_DEBUG
			printf("start_min: %lu, end: %lu, waste %lu - start_max: %lu, end: %lu, waste :%lu.\n",
				(unsigned long int)start_min,
				(unsigned long int)(start_min + marea - 1),
				(unsigned long int)waste_left,
				(unsigned long int)start_max,
				(unsigned long int)start_max + marea - 1, 
				(unsigned long int)waste_right);
			debug_waste_pol = "left";
			debug_waste = waste_left;
#endif
			if (waste_left < waste_right) {
#ifdef RM_DEBUG
				debug_waste_pol = "left";
				debug_waste = waste_left;
#endif
				RM_START(req, i) = start_min;
			} else {
#ifdef RM_DEBUG
				debug_waste_pol = "right";
				debug_waste = waste_right;
#endif
				RM_START(req, i) = start_max;
			}

#ifdef RM_DEBUG
			printf("Range %p allocated at offset %lu stride %lu (%s-most, waste %lu).\n",
				(void *)&req->range[i],
				(unsigned long int)RM_START(req, i), 
				(unsigned long int)RM_STRIDE(req, i),
				debug_waste_pol,
				(unsigned long int)debug_waste);
#endif

			if (rm_chunk_allocate_range(cl, &req->range[i], currelpad) < 0) {
				/*
				  For some unclear reason (lack of memory probably),
				  the range couldn't be allocated in the chunk list.
				*/
				RM_SET_FLAGS(req, i, RANGE_NO_FIT);
#ifdef RM_DEBUG
				printf("Unexpected error ( as if errors were to be expected! ) while allocating range... Mark as 'no fit'.\n");
#endif

			} else {
				RM_SET_FLAGS(req, i, RANGE_ALLOCATED);
			}
			break;	/* for(j==cl-... */
		}
		if (j < 0) {
			/*
			
			  The range couldn't fit.
			
			*/
#ifdef RM_DEBUG
			printf("Range %p couldn't be allocated.\n", (void *)&req->range[i]);
#endif
			RM_SET_FLAGS(req, i, RANGE_NO_FIT);
		}
	}

	/*
	  clean up;
	*/
	rm_chunk_exit(&cl);

#ifdef RM_DEBUG
	printf("Done with rm_list_fit\n");
#endif
	return 0;
}


/*****************************************************************************
 * Batchop administrative functions
 */

batchop_t 
bo_calloc(size_t numparms, uint64_t * defblock)
{
	batchop_t       res;
	int             idx;

	res = calloc((size_t)1, sizeof(struct batchop));
	if (res == NULL)
		return NULL;

	res->parms = calloc(numparms, sizeof(struct batchparm));
	if (res->parms == NULL) {
		free(res);
		return NULL;
	}
	res->numparms = numparms;
	for (idx = 0; idx < res->numparms; idx++)
		res->parms[idx].block = defblock;

	return res;
}

void 
bo_free(batchop_t this)
{
	if (this != NULL) {
		if (this->parms != NULL)
			free(this->parms);
		free(this);
	}
}
