/* $Id: bo_go.c,v 1.17 2005/07/30 12:14:20 cegger Exp $
******************************************************************************

   bo_def_go function implementation

   Copyright (C) 2001 Christoph Egger   [Christoph_Egger@t-online.de]
   Copyright (C) 2001 Brian S. Julin    [bri@calyx.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <string.h>
#include <ggi/mmutil.h>

/* Listen, my little batchlings, to a story of fear and terror.  This
   is where bad little batchops go when they cannot be matched to
   a normal go function.  Here they spend what seems an eternity
   having their tonsils examined for endianness infections, 
   and their molars drilled.  Waboogaboogaboogaboogabooga!
*/

int bo_def_go(batchop_t src, batchop_t dest, int num)
{
	int idx, boidx;
	for (boidx = 0; boidx < num; boidx++) {
		for (idx = 0; idx < dest->numparms; idx++) {
			uint64_t inbuf[2], outbuf[2], mask[2];
			int bM;
			batchparm_t inbp, outbp;
			if (dest->parms[idx].match.src == -1) continue;
			outbp = &(dest->parms[idx]);
			inbp = &(src->parms[dest->parms[idx].match.src]);
#define bP inbp
#define bB inbuf
#define bDir in

#include "ggi/internal/mmutil/def_load_64.inc"
#include "ggi/internal/mmutil/def_splice_64.inc"
#include "ggi/internal/mmutil/def_in_ops_64.inc"

#undef bP
#define bP outbp
#include "ggi/internal/mmutil/def_out_ops_64.inc"

#define WANT_MSB \
(src->parms[dest->parms[idx].match.src].parmtype & BO_PT_JUSTIFY_MS)
#include "ggi/internal/mmutil/def_unsplice_64.inc"

			mask[0] = mask[1] = 0xffffffffffffffffLL;

#undef bB
#define bB mask
#include "ggi/internal/mmutil/def_unsplice_64.inc"

#undef WANT_MSB
#undef bB
#define bB outbuf
#undef bDir
#define bDir out

#include "ggi/internal/mmutil/def_load_64.inc"

#undef bDir

			outbuf[0] &= ~(mask[0]);
			outbuf[1] &= ~(mask[1]);
			outbuf[0] |= inbuf[0];
			outbuf[1] |= inbuf[1];

#include "ggi/internal/mmutil/def_store_64.inc"

#undef bP
#undef bB			
		}	/* for */
		bo_inc_in_idx(src);
		bo_inc_out_idx(dest);
	}	/* for */
	return BO_OK;

}	/* bo_def_go */
