/* $Id: bo_match.c,v 1.7 2004/09/18 13:00:16 cegger Exp $
******************************************************************************

   Functions for matching bsrc and bdest

   Copyright (C) 2001 Christoph Egger   [Christoph_Egger@t-online.de]
   Copyright (C) 2001 Brian S. Julin    [bri@calyx.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/mmutil.h>

static int _bo_def_pt_callback (int srcpt, int dstpt) {
	if ((srcpt & BO_PT_TYPE_MASK) != (dstpt & BO_PT_TYPE_MASK))
		return 0;
	if ((srcpt & BO_PT_SUBTYPE_MASK) != (dstpt & BO_PT_SUBTYPE_MASK))
		return 1;
	return 2;
}	/* _bo_def_pt_callback */

static void _bo_match_parm(batchop_t s, batchparm_t p, bo_pt_callback *cmp) {
	int idx = -1;
	p->match.src = -1;

	while (++idx < s->numparms) {
		batchparm_t try;
		batchparm_t cur;
		int try_res, cur_res;

		try = &(s->parms[idx]);

		try_res = cmp(try->parmtype, p->parmtype);
                if (!try_res) continue;
		if (p->match.src == -1) {
			p->match.src = idx;
			continue;
                }

		cur = &(s->parms[p->match.src]);
		cur_res = cmp(cur->parmtype, p->parmtype);

                if (cur_res < try_res) {
			p->match.src = idx;
			continue;
                }
		else if (cur_res > try_res) continue;
		
		if ((try->datasize > cur->datasize) &&
		    (cur->datasize < p->datasize)) {
			p->match.src = idx;
			continue;
                }
		if ((try->datasize < cur->datasize) &&
		    (cur->datasize <= p->datasize))
			continue;
		if ((try->datasize < cur->datasize) &&
		    (try->datasize < p->datasize))
			continue;
		/* TODO: be more thorough */
        }
}


int bo_def_match(batchop_t s, batchop_t d, bo_pt_callback *cmp)
{
	int idx;

	if (cmp == NULL) cmp = _bo_def_pt_callback;

	for (idx = 0; idx < d->numparms; idx++)
		_bo_match_parm(s, &(d->parms[idx]), cmp);

	d->lastmatch = s;

	d->go = &bo_def_go;

	return BO_OK;
}	/* bo_def_match */

