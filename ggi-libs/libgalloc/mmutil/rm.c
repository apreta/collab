/* $Id: rm.c,v 1.6 2004/09/18 13:00:16 cegger Exp $
******************************************************************************

   rm.c:  Glue for the mmutils range manager for LibGalloc use.

   1) a generic 2D memory range manager (No 2D yet, just 1D)
   2) a batch operation core

   Copyright (C) 2001 Christoph Egger   [Christoph_Egger@t-online.de]
   Copyright (C) 2001 Brian S. Julin    [bri@calyx.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/internal/mmutil/rm.h>
#include <ggi/internal/galloc_debug.h>

/* The function will fit nochange and juggle into the spare space left
 * over from frame, without moving any of the ranges in nochange, but 
 * it is free to move the ones in juggle.
 *
 * w, h, stride, align, address are all adjusted before calling so that 
 * all the entries are using the same units, be they pixels, bytes, 
 * words, etc.  Usually we will store the start address in the resource 
 * and throw the rest away, after using it to set some of the properties, 
 * (it can be deduced back from the properties.)
 * 
 * Really, this does special processing for frame, and for the data
 * structure, and then calls rm_squish, which is the meat of the
 * algorithm.
 */

#if 0
int ggiGArm_check(rm_rangelist frame, 
		  rm_rangelist nochange,
		  rm_rangelist juggle)
{
	size_t max, goal, best;
	int idx = 0, res = 0;

	idx = 0;
	while (RM_START(juggle,idx) != 0) idx++;
	if (RM_OWNER(juggle,idx) == NULL) {
		/* We went to the end without finding a "new" range, 
		 * so it is the frame that has changed.
		 */
		res = -1;
	} else {
		res = idx;
	}	/* if */


	/* Find the upper limit. */
	idx = 0;
	while (RM_OWNER(frame,idx) != NULL) {
		if (RM_END(frame, idx) == SIZE_MAX) {
			/* This is the range that blocks off the area
			 * beyond the end of the MMIO area.
			 */
			max = RM_START(frame, idx);
		}	/* if */
		idx++;
	}	/* while */

	/* The nochange resources MUST be under that limit. */
	idx = 0;
	while (RM_OWNER(nochange,idx) != NULL) {
		LIB_ASSERT(RM_END(nochange,idx) <= max);
		if (RM_END(nochange,idx) > max) return -2;
		idx++;
	}	/* while */

	/* We could sort the nochange list (except for the "new" resources
	 * in juggle) here in order to improve efficiency of rm_squish.
	 */
	goal = 0;
	if (res == -1) {
		/* If the frame is the new resource, we try to maximize
		 * it by setting the goal to the lowest start point in
		 * nochange.
		 */
		idx = 0;
		while (RM_START(nochange,idx) != 0) {
			if ((goal == 0) || (goal > RM_START(nochange,idx))) {
				goal = RM_START(nochange,idx);
			}	/* if */
			idx++;
		}	/* while */

	} else {
		/* Otherwise we set the goal to the end of the frame, which 
		 * may result in better fragmentation-proofing of the squish,
		 * once we have an implementation of rm_squish that is worth
		 * a hill of beans, that is.
		 */
		idx = 0;
		while (RM_OWNER(frame,idx) != NULL) {
			if (RM_START(frame,idx) == 0) {
				/* This is the range that defines the main fb
				 * area from 0 to the end of the virtual
				 * drawing area.
				 */
				goal = RM_W(frame,idx);
			}	/* if */
			idx++;
		}	/* while */
	}	/* if */  

	/* All righty.  Now we have a max, and a goal below which
	 * squish will try not to go.  So now the problem reduces to 
	 * pure math, more or less.
	 */
	goal = rm_squish(goal, max, nochange, juggle);

	/* Now we deal with the results. */
	if (res == -1) {
		idx = 0;
		while (RM_OWNER(frame,idx) != NULL) {
			if (RM_START(frame,idx) == 0) {
				/* This is the range that defines the
				 * main fb area from 0 to the end of the
				 * virtual drawing area.
				 */
				if (RM_W(frame,idx) > goal)
					RM_W(frame,idx) = goal;
			}	/* if */
			idx++;
		}	/* while */
	}	/* if */

	return res;
}	/* rm_check */

#endif
