/* $Id: memdebug.c,v 1.10 2005/07/30 12:14:20 cegger Exp $
******************************************************************************

   Functions to debug and test the range manager and batchops 

   Copyright (C) 2001 Christoph Egger   [Christoph_Egger@t-online.de]
   Copyright (C) 2001 Brian S. Julin    [bri@calyx.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

/*
 Put in useful stuff here
 */

#include <ggi/mmutil.h>

int main (void)
{
	uint64_t inblock[2], outblock[2];
	batchop_t inbo, outbo;
	batchparm_t bp;

	inbo = bo_calloc(1, inblock);
	outbo = bo_calloc(1, outblock);

	inblock[0] = 0x789abcdeffffffff;
	inblock[1] = 0xffffffffffffffff;

	outblock[0] = 0x123498765fab6789;
	outblock[1] = 0x48732abdef2837a8;

	bp = &(inbo->parms[0]);

	bp->offset = 32;
	bp->datasize = 32;
	bp->step = 64;
	bp->access = 0;
	bp->parmtype = BO_PT_JUSTIFY_MS | 1;
	bp->arg.s64 = 1;

	bp = &(outbo->parms[0]);

	bp->offset = 50;
	bp->datasize = 16;
	bp->step = 16;
	bp->access = 0;
	bp->parmtype = 1;
	bp->arg.s64 = 1;

	printf("Copying %16.16llx %16.16llx:%i-%i\n", 
		inblock[0], inblock[1], inbo->parms[0].offset, 
		inbo->parms[0].offset + inbo->parms[0].datasize);

	printf("...into %16.16llx %16.16llx:%i-%i\n", 
		outblock[0], outblock[1], outbo->parms[0].offset, 
		outbo->parms[0].offset + outbo->parms[0].datasize);

	bo_match(inbo, outbo, NULL);
	bo_set_in_idx(inbo, 0);
	bo_set_out_idx(outbo, 0);
	bo_go(inbo, outbo, 1);

	printf("yeilds  %16.16llx %16.16llx.\n", outblock[0], outblock[1]);

#if BYTE_ORDER != BIG_ENDIAN
	printf("That might not be obvious to the naked eye, as you are not\n"
		"running on a big-endian machine.\n");
#endif
}	/* main */
