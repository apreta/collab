dnl libggi (un)installed path


AC_DEFUN([GGI_INST_PATH],
[

AC_ARG_WITH([ggi],
[  --with-ggi=DIR          use the LibGGI installed with prefix DIR
                           (can be used to specify location of LibGII too)],
AM_CPPFLAGS="$AM_CPPFLAGS -I$withval/include"
  AM_LDFLAGS="$AM_LDFLAGS -L$withval/lib"
  CPPFLAGS="$CPPFLAGS -I$withval/include"
  LDFLAGS="$LDFLAGS -L$withval/lib"
  DISTCHECK_CONFIGURE_FLAGS="$DISTCHECK_CONFIGURE_FLAGS --with-ggi=$withval",
AM_CPPFLAGS="$AM_CPPFLAGS -I$prefix/include"
  AM_LDFLAGS="$AM_LDFLAGS -L$prefix/lib"
  CPPFLAGS="$CPPFLAGS -I$prefix/include"
  LDFLAGS="$LDFLAGS -L$prefix/lib"
  DISTCHECK_CONFIGURE_FLAGS="$DISTCHECK_CONFIGURE_FLAGS --with-ggi=$prefix")

])

# This is for building against an uninstalled libggi for
# both $(top_builddir) == $(top_srcdir)
# and $(top_builddir) != $(top_srcdir)
AC_DEFUN([GGI_UNINST_PATH],
[

AC_ARG_WITH([uninst-ggi],
[  --with-uninst-ggi=DIR   use uninstalled copy of LibGGI found in DIR],
[[if test -d "$withval" ; then
     ggi_top_builddir="`cd "$withval" ; pwd`"
     ggi_top_srcdir="$gii_top_builddir/`cd "$withval" ; sed -n -e 's/^top_srcdir[       ]*=[    ]*//p' Makefile`"
     ggi_top_srcdir="`cd $gii_top_srcdir ; pwd`"
     DISTCHECK_CONFIGURE_FLAGS="$DISTCHECK_CONFIGURE_FLAGS --with-uninst-ggi=$ggi_top_builddir"
  fi
  if test -d "$ggi_top_builddir" -a -d "$ggi_top_srcdir" ; then
    if test "$ggi_top_builddir" = "$ggi_top_srcdir" ; then
      AM_CPPFLAGS="-I$ggi_top_builddir/include $AM_CPPFLAGS"
      CPPFLAGS="-I$ggi_top_builddir/include $CPPFLAGS"
    else
      AM_CPPFLAGS="-I$ggi_top_srcdir/include -I$ggi_top_builddir/include $AM_CPPFLAGS"
      CPPFLAGS="-I$ggi_top_srcdir/include -I$ggi_top_builddir/include $CPPFLAGS"
    fi
    AM_LDFLAGS="-L$ggi_top_builddir/ggi $AM_LDFLAGS"
    LDFLAGS="-L$ggi_top_builddir/ggi $LDFLAGS"
  fi
]])

])


AC_DEFUN([GGI_CHECKLIB],
[

AC_CHECK_HEADERS([ggi/ggi.h])

ggi_missing_part=no

dnl Testing libs should use libtool but doesn't - grrrr!
dnl This isn't a problem during the build because libtool is always used.
dnl Wish I could use $LIBTOOL, but it is written for Makefile use.
save_CC="$CC"
CC="$SHELL ./libtool --mode=link $CC"
AC_CHECK_LIB(ggi,ggiInit,foo=bar,ggi_missing_part=yes)
CC="$save_CC"

if test "$ac_cv_header_ggi_ggi_h" != "yes" -o \
	"$ggi_missing_part" = "yes" ; then
  AC_MSG_ERROR([Either wrong include path is used or LibGGI is
                not properly installed on the system. You need LibGGI for
                building $1. Please compile LibGGI first.])
fi

])
