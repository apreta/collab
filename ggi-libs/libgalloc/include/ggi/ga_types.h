/* $Id: ga_types.h,v 1.19 2005/10/12 10:23:44 cegger Exp $
******************************************************************************

   LibGalloc: extension types header file

   Copyright (C) 2001 Christoph Egger   [Christoph_Egger@t-online.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


******************************************************************************
*/

#ifndef _GGI_GALLOC_TYPE_H
#define _GGI_GALLOC_TYPE_H

#include <ggi/ggi.h>
#include <ggi/ll.h>

__BEGIN_DECLS


/* Datatype used for tag numbers.
 */
typedef unsigned int tag_t;


/* Why we don't use enum's:
 * The C standard says, sizeof(enum) is compiler-defined,
 * but must be large enough to represent all values in the enum.
 * The enum identifier themself must be representable in an
 * signed int. This limits sizeof(enum) to signed int but
 * can be smaller (i.e. signed char).
 * To not risk breaking the (binary) API when adding new members
 * or changing a value (if needed) in the future.
 */



/* Resource types.

   Note, if these are changed, the AnPrintf strings in internal/galloc.h
   should be changed accordingly.

*/

typedef uint32_t ggiGA_resource_type;

	/* defined by extension */
#define GA_RT_SUBTYPE_MASK	0x0000ffff
#define GA_RT_TYPE_MASK		0x00ff0000
#define GA_RT_FULLTYPE_MASK	(GA_RT_SUBTYPE_MASK | GA_RT_TYPE_MASK)
#define GA_RTSHIFT		16

	/* base frame */
#define GA_RT_FRAME		0x00010000

	/* hardware buffers mask */
#define GA_RT_BUFFER		0x00020000

	/* blitting-object */
#define GA_RT_BOB		0x00030000

	/* Sprite/hw cursors */
#define GA_RT_SPRITE		0x00040000

	/* video */
#define GA_RT_VIDEO		0x00050000

	/* any window */
#define GA_RT_WINDOW		0x00060000

	/* Hard to categorize resources */
#define GA_RT_MISC		0x00070000

	/* Renderers */
#define GA_RT_RENDERER		0x00080000

	/* Pseudo resources used for list structure. */
#define GA_RT_RESLIST		0x00090000

	/* Number of types.  Be sure to adjust. */
#define GA_RTNUM		10


	/* Frame type has no subtypes. */
#define GA_RT_FRAME_DONTCARE	0x00010000
#define GA_RTFRAMENUM		1


	/* Subtypes of (direct)buffer types */
#define GA_RT_BUFFER_DONTCARE	0x00020000

	/* raw buffer */
#define GA_RT_BUFFER_RAW	0x00020001

	/* zbuffer */
#define GA_RT_BUFFER_ZBUFFER	0x00020002

	/* alpha-buffer */
#define GA_RT_BUFFER_ABUFFER	0x00020003

	/* stencil-buffer */
#define GA_RT_BUFFER_SBUFFER	0x00020004

	/* texture-buffer */
#define GA_RT_BUFFER_TBUFFER	0x00020005

	/* generic drawing area in same format as the main fb */
#define GA_RT_BUFFER_SWATCH	0x00020006

	/* Number of buffer subtypes.
           Be sure to adjust. */
#define GA_RTBUFFERNUM		7


	/* BOBs -- no subtypes */
#define GA_RT_BOB_DONTCARE	0x00030000

	/* Number of BOB subtypes.
           Be sure to adjust. */
#define GA_RTBOBNUM		1


	/* Subtypes for sprite features */
#define GA_RT_SPRITE_DONTCARE	0x00040000

	/* Hardware pointer cursor */
#define GA_RT_SPRITE_POINTER	0x00040001

	/* Hardware text cursor */
#define GA_RT_SPRITE_CURSOR	0x00040002

	/* True sprite (a-la C-64) */
#define GA_RT_SPRITE_SPRITE	0x00040003

	/* Number of sprite subtypes.
           Be sure to adjust. */
#define GA_RTSPRITENUM		4


	/* Subtypes for video features */
#define GA_RT_VIDEO_DONTCARE	0x00050000

	/* motion video */
#define GA_RT_VIDEO_MOTION	0x00050001

	/* streaming video */
#define GA_RT_VIDEO_STREAMING	0x00050002

	/* Number of sprite subtypes.
           Be sure to adjust. */
#define GA_RTVIDEONUM		3


	/* Subtypes for window features */
#define GA_RT_WINDOW_DONTCARE	0x00060000

	/* YUV-Viewport */
#define GA_RT_WINDOW_YUV	0x00060001

        /* Hardware address translation port (like display-sub) */
#define GA_RT_WINDOW_MM		0x00060002

	/* Number of sprite subtypes.
           Be sure to adjust. */
#define GA_RTWINDOWNUM		3


	/* Subtypes for miscellaneous features */
#define GA_RT_MISC_DONTCARE	0x00070000

	/* CRT ray position */
#define GA_RT_MISC_RAYPOS	0x00070001

	/* CRT ray position auto-flip */
#define GA_RT_MISC_RAYTRIG_SET	0x00070002

	/* CRT ray position auto-palette */
#define GA_RT_MISC_RAYTRIG_PAL	0x00070003

	/* VGA Splitline */
#define GA_RT_MISC_SPLITLINE	0x00070004

	/* Hardware textmode font data */
#define GA_RT_MISC_FONTCELL	0x00070005

	/* Number of misc subtypes.
           Be sure to adjust. */
#define GA_RTMISCNUM		6


	/* Subtypes for Renderer features */
#define GA_RT_RENDERER_DONTCARE	0x00080000

	/* Basic GGI style drawops */
#define GA_RT_RENDERER_DRAWOPS	0x00080001

	/* Number of renderer subtypes.
           Be sure to adjust. */
#define GA_RTRENDERERNUM	2

	/* These don't do anything, just waste space. */
#define GA_RT_RESLIST_NOOP	0x00090000

	/* Tag group head. */
#define GA_RT_RESLIST_TAGHEAD	0x00090001

	/* Number of resource list subtypes.
           Be sure to adjust. */
#define GA_RTRESLISTNUM		2


	/* Flag -- if set this is a motor resource,
	   otherwise it is a tank or carb resource. */
#define	GA_RT_MOTOR		0x80000000

	/* Flag -- if set this is a carb resource,
	   otherwise it is a tank or motor resource. */
#define GA_RT_CARB		0x40000000

/* ggiGA_resource_type */


/* See the LibGAlloc documentation for a full description
 * of storage values and flags.
 */
typedef uint32_t ggiGA_storage_type;

	/* Special values */
#define GA_STORAGE_DONTCARE	0x00000000

	/* Flags */
#define GA_STORAGE_READ		0x00000001
#define GA_STORAGE_WRITE	0x00000002
#define GA_STORAGE_DIRECT	0x00000004
#define GA_STORAGE_ONBOARD	0x00000010
#define GA_STORAGE_RAM		0x00000020
#define GA_STORAGE_SECONDARY	0x00000040
#define GA_STORAGE_TERTIARY	0x00000080
#define GA_STORAGE_VIRT		0x00000100
#define GA_STORAGE_TRANSFER	0x00000200
#define GA_STORAGE_HOTPIPE	0x00000400

	/* Convenience values */
#define GA_STORAGE_RW		(GA_STORAGE_READ     | GA_STORAGE_WRITE)
#define GA_STORAGE_NORMAL	(GA_STORAGE_DIRECT   | GA_STORAGE_RW)
#define GA_STORAGE_REALRAM	(GA_STORAGE_NORMAL   | GA_STORAGE_RAM)
#define GA_STORAGE_REALRAM_RO	(GA_STORAGE_REALRAM  & ~GA_STORAGE_READ)
#define GA_STORAGE_VRAM		(GA_STORAGE_NORMAL   | GA_STORAGE_ONBOARD)
#define GA_STORAGE_VRAM_RO	(GA_STORAGE_VRAM     & ~GA_STORAGE_READ)
#define GA_STORAGE_SWAP		(GA_STORAGE_REALRAM  | \
				GA_STORAGE_SECONDARY | GA_STORAGE_VIRT)
#define GA_STORAGE_SWAP_RO	(GA_STORAGE_SWAP     & ~GA_STORAGE_READ)
#define GA_STORAGE_DMA		(GA_STORAGE_REALRAM  | GA_STORAGE_TRANSFER)
	/* This one is for AGP */
#define GA_STORAGE_GART		(GA_STORAGE_REALRAM  | \
				GA_STORAGE_VRAM      | GA_STORAGE_VIRT)
/* ggiGA_storage_type */


/* Flag value registry */
typedef uint32_t ggiGA_flags;

	/* Common flags */


	/* MOTORS */

	/* These flags apply to all motors, but not to tanks/carbs. */
#define GA_FLAGSHIFT_COORDBASE	0
#define GA_FLAG_UNIT_MASK	0x00000001	/* See GA_COORDBASE* */
#define GA_FLAG_COORDBASE_MASK	0x0000000E	/* See GA_COORDBASE* */
#define GA_FLAG_STAY_IN_GRID	0x00000010	/* x+w,y+h must be inside grid */

	/* CARBS */

	/* TANKS */

	/* These flags apply to all tanks, but not to carbs. */
#define GA_FLAG_STEREO_NOT	0x00000000
#define GA_FLAG_STEREO_LEFT	0x00000001
#define GA_FLAG_STEREO_RIGHT	0x00000002

	/* Buffer specific flags (libbuf) */

	/* MOTORS */

	/* CARBS */

	/* These flags apply to certain carbs, mostly buffers */


	/* TANKS */


	/* Overlay specific flags (libovl) */

	/* Overlay MOTORS */

	/* This flag applies only to GA_RT_SPRITE_POINTER/CURSOR motors */
#define GA_FLAG_AUTOTRACK	0x00000100

	/* priority flag (motors only) */
#define GA_FLAG_ALWAYS_ON_TOP	0x00000200

	/* video formats */
#define GA_FLAG_VIDEO_MPEG_1	0x00001000
#define GA_FLAG_VIDEO_MPEG_2	0x00002000
#define GA_FLAG_VIDEO_MPEG_4	0x00003000
#define GA_FLAG_VIDEO_AVI	0x00004000
#define GA_FLAG_VIDEO_QUICKTIME	0x00005000
#define GA_FLAG_VIDEO_H261	0x00006000
#define GA_FLAG_VIDEO_H263	0x00007000

	/* operations during display */
#define GA_FLAG_VIDEO_INTERPOLATE	0x00010000
#define GA_FLAG_VIDEO_DECOMPRESS	0x00020000

	/* framerate */
#define GA_FLAG_VIDEO_CURFRAMERATE	0x00040000	/* current framerate */
#define GA_FLAG_VIDEO_AVGFRAMERATE	0x00080000	/* average framerate */

	/* CARBS */

	/* TANKS */

	/* Bob specific flags (libblt) */

	/* MOTORS */

	/* CARBS */

	/* TANKS */

	/* Renderer specific flags */
#define GA_FLAG_RENDERER_HW	0x00000020	/* Accellerated */

/* ggiGA_flags */


/* Everything below here is for internal use.  API functions are provided to
 * access any needed flags/values in the below.
 */

typedef uint32_t ggiGA_storage_share;

#define GA_SHARE_REQUEST_MASK	0x0000000F
#define GA_SHARE_RESPONSE_MASK	0xFFFFFFFF

	/* Force resource never to share the storage */
#define GA_SHARE_FORCEDONTSHARE	0x00000001

	/* The resource has to be copyied before writing. This is i.e.
	 * necessary, when you share the resource with an read-only one.
	 */
#define GA_SHARE_COPYONWRITE	0x00000002

	/* This resource is capable to be shared.
	 * NEVER set it - only ggiGACheck() does this!
	 */
#define GA_SHARE_SHAREABLE	0x00000010

	/* Set by ggiGASet(), if resource is shared
	 * and it doesn't allow any modifications in the
	 * size, place and layout of storage.
	 * This flag combines the meaning, that the resource _is_
	 * shared and being the GA_STATE_NOCHANGE flag.
	 */
#define GA_SHARE_SHARED		0x00000020

/* ggiGA_storage_share */


#define ggiGA_TYPE(r)		(r->res_type & GA_RT_TYPE_MASK)
#define ggiGA_SUBTYPE(r)	(r->res_type & GA_RT_SUBTYPE_MASK)
#define ggiGA_FULLTYPE(r)	(r->res_type & GA_RT_FULLTYPE_MASK)
#define ggiGA_TAG(r)		(r->res_state & GA_STATE_TAG_MASK)

/* Behavior control information between Galloc/higher level. */
typedef uint32_t ggiGA_resource_state;

	/* Bits that are used to talk to Galloc */
#define GA_STATE_REQUEST_MASK	0x00003300

	/* Bits Galloc uses to talk back to caller. */
#define	GA_STATE_RESPONSE_MASK	0xFFFFFF00

	/* Tag for complex resources */
#define GA_STATE_TAG_MASK	0x000000FF


	/* This resource is in-service and must not be disturbed. */
#define GA_STATE_NORESET	0x00000100

	/* Don't bother modifying this resource, if
	 * it fails as is, it fails.
	 */
#define GA_STATE_NOCHANGE	0x00000200



	/* This resource request was altered due to
	 * failure or GGI_AUTO
	 */
#define GA_STATE_MODIFIED	0x00000400

	/* This resource request failed. */
#define GA_STATE_FAILED		0x00000800



	/* This resource modifies or refers to the last
	 * resource in the list that did not have this flag set.
	 */
#define GA_STATE_SEEABOVE	0x00001000

	/* Upper bounds cap resource */
#define GA_STATE_CAP		0x00002000

/* ggiGA_resource_state */


__END_DECLS


#endif	/* _GGI_GALLOC_TYPE_H */
