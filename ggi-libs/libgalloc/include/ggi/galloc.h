/* $Id: galloc.h,v 1.56 2007/12/20 23:38:24 cegger Exp $
******************************************************************************

   LibGalloc: extension API header file

   Copyright (C) 2001 Christoph Egger   [Christoph_Egger@t-online.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


******************************************************************************
*/

#ifndef _GGI_GALLOC_H
#define _GGI_GALLOC_H

#include <ggi/ggi.h>
#include <ggi/galloc-defs.h>
#include <ggi/gg-queue.h>


__BEGIN_DECLS



/* Get the master config dir
 */
GGIGAAPIFUNC const char *ggiGAGetConfDir(void);

/* Extension management
 */
GGIGAAPIFUNC int ggiGAInit(void);
GGIGAAPIFUNC int ggiGAExit(void);

GGIGAAPIFUNC int ggiGAAttach(ggi_visual_t vis);
GGIGAAPIFUNC int ggiGADetach(ggi_visual_t vis);


/* Structure:
 *
 * ggiGA_resource_handle: A pointer to a single resource
 * ggiGA_resource_list: A pointer to a resource list
 * ggiGA_resource_taghandle: A pointer to a single tagged resource
 * ggiGA_resource_taggroup: A pointer to a tagged resource ring list
 *	building a group.
 */

#ifdef HAVE_STRUCT_GALLOC
GG_SIMPLEQ_HEAD(ggiGA_resource_listhead, ggiGA_resource);
typedef struct ggiGA_resource_listhead *ggiGA_resource_list;
typedef struct ggiGA_resource *ggiGA_resource_handle;
typedef struct ggiGA_resource *ggiGA_resource_taggroup;
typedef struct ggiGA_resource *ggiGA_resource_taghandle;
#else
struct ggiGA_resource { int dummy; };

GG_SIMPLEQ_HEAD(ggiGA_resource_listhead, ggiGA_resource);
typedef struct ggiGA_resource_listhead *ggiGA_resource_list;
typedef struct ggiGA_resource *ggiGA_resource_handle;
typedef struct ggiGA_resource *ggiGA_resource_taggroup;
typedef struct ggiGA_resource *ggiGA_resource_taghandle;
#endif


/* GAlloc error-codes */

#include <ggi/errors.h>

#define GALLOC_OK		GGI_OK

/* LibGAlloc sometimes return LibGGI error codes. (See ggi/errors.h)
 * The following error codes are used for LibGAlloc-specific circumstances.
 */

/* This occurs when a request cannot be honored due to lack of resources.
 */
#define GALLOC_EUNAVAILABLE	-1

/* This occurs when there is some sort of normal failure condition.
 */
#define GALLOC_EFAILED		-2

/* This means that a request was too vague, resulting in an ambiguity.
 * This will not happen if the caller is specific enough.
 */
#define GALLOC_ESPECIFY		-3



/**********************************************************************
 API functions
 **********************************************************************/


/* See manual page ggiGAGet(3) */
GGIGAAPIFUNC int ggiGACheck(ggi_visual_t vis, ggiGA_resource_list request,
	       ggiGA_resource_list *result);

GGIGAAPIFUNC int ggiGASet(ggi_visual_t vis, ggiGA_resource_list request,
	     ggiGA_resource_list *result);

#define ggiGAGet(vis, result)	ggiGACheck(vis, NULL, result);

GGIGAAPIFUNC ggiGA_resource_handle ggiGAHandle(ggiGA_resource_list reslist,
					    ggiGA_resource_list reqlist,
					    ggiGA_resource_handle reqhandle);



/* See manual page ggiGARelease */
GGIGAAPIFUNC int ggiGARelease(ggi_visual_t vis, ggiGA_resource_list *list,
		 ggiGA_resource_handle *handle);

int ggiGAReleaseVis(ggi_visual_t vis, ggiGA_resource_list *list,
		ggiGA_resource_handle *handle);

GGIGAAPIFUNC int ggiGAReleaseList(ggi_visual_t vis, ggiGA_resource_list *list,
		ggiGA_resource_handle *handle);



/* See manual page ggiGAanprintf(3) */
GGIGAAPIFUNC int ggiGAanprintf(ggi_visual_t vis, ggiGA_resource_list list,
		  size_t size, const char *format, char **out);


#include <ggi/ga_types.h>
#include <ggi/ga_prop.h>


/* See manual page ggiGACreateList(3) */
GGIGAAPIFUNC ggiGA_resource_list ggiGACreateList(void);
GGIGAAPIFUNC int ggiGADestroyList(ggiGA_resource_list *list);

/* See manual page ggiGAEmptyList(3) */
int ggiGATruncateList(ggiGA_resource_list list, ggiGA_resource_handle *handle);
GGIGAAPIFUNC int ggiGAEmptyList(ggiGA_resource_list *list);

int ggiGAEmptyFailures(ggiGA_resource_list *list);


/* See manual page ggiGAAdd(3) */
GGIGAAPIFUNC int ggiGAAdd(ggiGA_resource_list *reqlist,
	struct ggiGA_resource_props *props,
	ggiGA_resource_type t, ggiGA_resource_handle *handle);

int ggiGAAddMode(ggi_visual_t vis, ggiGA_resource_list *reqlist,
		 ggi_mode *mode, ggi_directbuffer *db,
		 ggiGA_resource_handle *handle);


/* See manual page ggiGAGetProperties(3) */
GGIGAAPIFUNC int ggiGAClearMotorProperties(struct ggiGA_resource_props *prop);
GGIGAAPIFUNC int ggiGAClearCarbProperties(struct ggiGA_resource_props *prop);
GGIGAAPIFUNC int ggiGAClearTankProperties(struct ggiGA_resource_props *prop);

GGIGAAPIFUNC struct ggiGA_resource_props *ggiGAGetProperties(
	ggiGA_resource_handle handle);



GGIGAAPIFUNC ggi_mode *ggiGAGetGGIMode(ggiGA_resource_handle handle);
ggi_directbuffer *ggiGAGetGGIDB(ggiGA_resource_handle handle);



/* See manual page ggiGAIsFailed(3) */
GGIGAAPIFUNC int ggiGAIsFailed(ggiGA_resource_handle handle);
int ggiGAIsModified(ggiGA_resource_handle handle);

GGIGAAPIFUNC int ggiGAIsMotor(ggiGA_resource_handle handle);
GGIGAAPIFUNC int ggiGAIsTank(ggiGA_resource_handle handle);
GGIGAAPIFUNC int ggiGAIsCarb(ggiGA_resource_handle handle);

GGIGAAPIFUNC ggi_visual_t ggiGAGetVisual(ggiGA_resource_handle handle);


/* returns the first tank after tank. If tank == NULL, then it returns the
 * first tank, it'll find.
 */
GGIGAAPIFUNC ggiGA_resource_handle ggiGAMotorGetTank(ggiGA_resource_list list,
				ggiGA_resource_handle motor,
				ggiGA_resource_handle tank);


ggiGA_resource_handle ggiGATankGetCarb(ggiGA_resource_list list,
				ggiGA_resource_handle tank,
				ggiGA_resource_handle carb);


GGIGAAPIFUNC int ggiGATagOnto(ggiGA_resource_list reslist,
			   ggiGA_resource_handle motor,
			   ggiGA_resource_handle handle);


/* See manual page ggiGAAddSimpleMode(3) */
GGIGAAPIFUNC int ggiGAAddSimpleMode(ggi_visual_t vis,
		ggiGA_resource_list *reqlist,
		int xsize, int ysize,
		int frames,
		ggi_graphtype type,
		ggiGA_resource_handle *handle);

int ggiGAAddTextMode(ggi_visual_t vis, ggiGA_resource_list *reqlist,
		     int cols,int rows,
		     int vcols,int vrows,
		     int fontsizex,int fontsizey,
		     ggi_graphtype type,
		     ggiGA_resource_handle *handle);

int ggiGAAddGraphMode(ggi_visual_t vis, ggiGA_resource_list *reqlist,
                      int xsize,int ysize,
                      int xvirtual,int yvirtual,ggi_graphtype type,
                      ggiGA_resource_handle *handle);


/**********************************************************************
   Gray Ops API

   The "external" API encourages people to use "properties" to define
   resources, and will mask the extension users away from working directly
   with resources and resource lists.  The following functions are "gray
   ops" functions.  They are mainly for use by extension authors who have
   included also the internal utility functions in internal/galloc.h in
   order to do some fancy managing of request lists, but they are not so
   obscure that others may not have occasion to use them, and can be used
   without internal/galloc.h.  See ga_prop.h.
 **********************************************************************/

/* How to get an llobj's resources (if it has them) which is a NULL
 * terminated list of ggiGA_resource_handle *'s
 */
#define GA_LL_SET(obj) \
(obj)->ptrs[((obj)->map>>GGI_LL_GA_SHIFT)&GGI_LL_ATTRIB_MASK]
#define GA_LL(obj) (ggiGA_resource_handle *)GA_LL_SET(obj)


/* Unit conversion. Unit can be either (sub-)pixels or (sub-)dots
 */

/* Converts subunit 2 unit and vice versa. (Sub-)Unit can be either
 * (sub-)pixel or (sub-)dot.
 */
int ggiGASubUnit2Unit(ggiGA_resource_handle handle,
		      ggi_coord subunit, ggi_coord *unit);
int ggiGAUnit2SubUnit(ggiGA_resource_handle handle,
		      ggi_coord unit, ggi_coord *subunit);


/* Converts pixel to dots and vice versa.
 * Make sure, that you don't use subpixels or subdots.
 */
int ggiGAPixel2Dot(struct gg_stem *stem, ggi_coord pixel, ggi_coord *dot);
int ggiGADot2Pixel(struct gg_stem *stem, ggi_coord dot, ggi_coord *pixel);


/* Convert any nature measure to pixels and vice versa.
 */
int ggiGAConv2Pixel(struct gg_stem *stem, enum ggi_ll_coordbase cb,
		    ggi_coord coord, ggi_coord *pixel);
int ggiGAConvPixel2(struct gg_stem *stem, enum ggi_ll_coordbase cb,
		    ggi_coord pixel, ggi_coord *coord);


/* Convert any nature measure to pixels and vice versa.
 */
int ggiGAConv2Dot(ggi_visual_t vis, enum ggi_ll_coordbase cb,
		    ggi_coord coord, ggi_coord *dot);
int ggiGAConvDot2(ggi_visual_t vis, enum ggi_ll_coordbase cb,
		    ggi_coord dot, ggi_coord *coord);


/* return-value GALLOC_OK indicates the present in the reslist.
 * return-value of != 0 indicates an error.
 */
GGIGAAPIFUNC int ggiGAFind(ggiGA_resource_list list,
	ggiGA_resource_handle handle);


/* This function allows a new resource management structure to be
 * created from "properties" and "private data", without adding it
 * to a request list. The structure content is copied, so it may be reused
 * or freed without affecting the newly created resource.
 */
GGIGAAPIFUNC ggiGA_resource_handle ggiGACopyIntoResource(
	struct ggiGA_resource_props *props,
	void *priv, size_t privlen);


/* This function allows a new resource management structure to be
 * created from "properties" and "private data", without adding it
 * to a request list. The structure content is wrapped, so it can NOT
 * be reused or freed without affecting the newly created resource.
 */
ggiGA_resource_handle ggiGAWrapIntoResource(struct ggiGA_resource_props *props,
					    void *priv, size_t privlen);



/* Get the private structure, if any, out of a resource.  What is returned
   is a copy of the private struct, not a pointer to it.  There is no way to
   tell the size of the structure returned because, if you do not already
   know it, you should not be touching it (hint: if your extension has
   dynamically sized private structures, it should keep its own internal size
   field.)
 */
void *ggiGAGetPriv(ggiGA_resource_handle handle);



/* True if list is empty
 */
#define ggiGAIsListEmpty(list)	\
		(ggiGAGetFirst(list) == NULL)

/* Return the pointer of the first member of the list
 */
GGIGAAPIFUNC ggiGA_resource_handle ggiGAGetFirst(ggiGA_resource_list list);

/* Returns the pointer of the next member of the list
 */
GGIGAAPIFUNC ggiGA_resource_handle ggiGAGetNext(ggiGA_resource_handle handle);

/* As ggiGAGetNext, but skips the cap, if present
 */
GGIGAAPIFUNC ggiGA_resource_handle ggiGAGetNextSkipCap(ggiGA_resource_handle handle);

/* As above, but returns the previous member (or last member if handle == NULL)
 */
ggiGA_resource_handle ggiGAGetPrev(ggiGA_resource_list list,
					ggiGA_resource_handle handle);
ggiGA_resource_handle ggiGAGetPrevSkipCap(ggiGA_resource_list list,
					ggiGA_resource_handle handle);



/* Set/Get a tag number to the handle.
 */
GGIGAAPIFUNC int ggiGASetTag(ggiGA_resource_handle handle, tag_t tag);
int ggiGAGetTag(ggiGA_resource_handle handle);

/* If the given handle has a nonzero tag value, this does nothing.
 * Otherwise, searches for an unused nonzero tag number in "reslist", then
 * sets the tag-number of the resource pointed to by "handle" to that.
 * It returns 0, when no more tag-numbers are available and < 0 when an
 * error occured.
 */
GGIGAAPIFUNC int ggiGASetAutoTag(ggiGA_resource_list reslist,
	ggiGA_resource_handle handle);

/* As above, but can only be called on compound heads.  Applies the
 * newly acquired tag group to all the resources in the compound.  Also
 * differs in that if the compound head has a nonzero tag, this tag is
 * indeed applied to the rest of the resources in the compound.
 */
int ggiGASetAutoTagCompound(ggiGA_resource_list reslist,
			    ggiGA_resource_handle handle);

/* Like ggiGAAdd, but instead of adding a copy of a resource+props+priv
 * to a resource list, it duplicates one resource into "mutual resources",
 * both of which use the same props/priv structure.  These are mostly used to
 * propagate changes forward in a tag group during negotiation, but this
 * function is provided in order to allow them to be used outside of a
 * tag group, if that is desired.
 */
int ggiGAAddMutual(ggiGA_resource_list list,
		   ggiGA_resource_handle *handle);

/* As above, but duplicates a whole compound.  Naturally, the first resource
 * in the new compound and the first on the old one are "mutual resources",
 * and+ * the second resource (if any) in the new compound and the second one in the
 * old one are "mutual resources" and so on.
 */
GGIGAAPIFUNC int ggiGAAddMutualCompound(ggiGA_resource_list list,
					ggiGA_resource_handle *handle);

#if 0
/* Set the private structure in a resource. Note: It frees, allocates, and
   fills the private structure in the resource, so use ggiGAGetPriv if you
   wanted to save the old values.  The size is mandatory and must be the
   number of bytes to copy from the provided pointer.
 */
int ggiGASetPriv(ggiGA_resource_handle handle, void *priv, size_t size);
#endif

/* See manual page ggiGAGetType(3) */
GGIGAAPIFUNC ggiGA_resource_type ggiGAGetType(
	ggiGA_resource_handle handle);
GGIGAAPIFUNC ggiGA_resource_type ggiGASetType(
	ggiGA_resource_handle handle,
	ggiGA_resource_type rt);

/* See manual page ggiGAGetState(3) */
GGIGAAPIFUNC ggiGA_resource_state ggiGAGetState(
	ggiGA_resource_handle handle);
GGIGAAPIFUNC ggiGA_resource_state ggiGASetState(
	ggiGA_resource_handle handle,
	ggiGA_resource_state rs);

#define ggiGAAddStateFlag(handle, flag)	\
	ggiGASetState(handle, ggiGAGetState(handle) | (flag))
#define ggiGARemoveStateFlag(handle, flag)	\
	ggiGASetState(handle, ggiGAGetState(handle) & ~(flag))

GGIGAAPIFUNC unsigned int ggiGAGetCoordBase(ggiGA_resource_handle handle);


ggiGA_storage_type ggiGAGetStorage(ggiGA_resource_handle handle);
ggiGA_storage_share ggiGAGetStorageShareState(ggiGA_resource_handle handle);
ggiGA_storage_share ggiGASetStorageShareState(ggiGA_resource_handle handle,
						ggiGA_storage_share ss);

#define ggiGAAddShareFlag(handle, flag)		\
	ggiGASetStorageShareState(handle,	\
			ggiGAGetStorageShareState(handle) | (flag))
#define ggiGARemoveShareFlag(handle, flag)	\
	ggiGASetStorageShareState(handle,	\
			ggiGAGetStorageShareState(handle) & ~(flag))


/* checks, if the given resource is shared and returns 0 for non-shared
 * and != 0 for shared.
 */
int ggiGAIsShared(ggiGA_resource_handle handle);
GGIGAAPIFUNC int ggiGAIsShareable(ggiGA_resource_handle handle);
int ggiGAHasCopyOnWrite(ggiGA_resource_handle handle);
int ggiGAIsReadOnly(ggiGA_resource_handle handle);

#if 0
/* copies a shared resource. It fails, if it is NOT shared or
 * is NOT flagged with GA_SHARE_COPYONWRITE.
 * The caller has to use it, _before_ doing write-access to it,
 * except the caller KNOWS what he does!
 */
int ggiGACopy2Write(ggiGA_resource_handle *handle);
#endif

__END_DECLS

#endif /* _GGI_GALLOC_H */
