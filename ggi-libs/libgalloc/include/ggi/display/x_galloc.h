/* $Id: x_galloc.h,v 1.5 2005/07/30 12:14:19 cegger Exp $
******************************************************************************

   Galloc X target - header for extensions.

   Copyright (C) 2001	Christoph Egger	[Christoph_Egger@t-online.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#ifndef _GGI_DISPLAY_X_GALLOC_H
#define _GGI_DISPLAY_X_GALLOC_H


#include <ggi/display/xcommon.h>
#include <ggi/display/x.h>

#include <X11/X.h>
#include <X11/Xlib.h>

#include <stdio.h>


struct GA_x_buffer {

	/* pointer to directbuffer */
	const ggi_directbuffer *db;

	/* pointer to system RAM buffer */
	void *buf;

	/* size of buffer */
	size_t buf_size;

	/* pointers for MMIO access */
	void *read;
	void *write;

	/* buffers with the GA_STORAGE_SECONDARY flag needs it,
	 * otherwise they always fails.
	 */
	char *filename;
};


struct GA_x_bob {
	void *bobimage;
};


struct GA_x_renderer {
	int dummy;
};

struct GA_x_sprite {
	Pixmap pixmap;
	Cursor cursor;

	uint32_t res_count;
};


struct GA_x_window {
	/* subvisual for hardware-address translation */
	ggi_visual_t vis;

	ggi_mode mode;
};



#endif /* _GGI_DISPLAY_X_GALLOC_H */
