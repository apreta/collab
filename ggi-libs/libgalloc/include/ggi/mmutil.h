/* $Id: mmutil.h,v 1.34 2005/10/12 10:23:44 cegger Exp $
******************************************************************************

   mmutils.h: Header file for the mmutils mini-library

   Copyright (C) 2001 Christoph Egger   [Christoph_Egger@t-online.de]
   Copyright (C) 2001 Brian S. Julin    [bri@calyx.com]
   Copyright (C) 2001 Eric Faurot       [eric.faurot@gmail.com]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#ifndef _GGI_MMUTIL_H
#define _GGI_MMUTIL_H

#include <ggi/mmutil-defs.h>

#ifdef MMUTIL_STANDALONE
#include <sys/types.h>

#ifdef MMUTIL_USE_ANSI_TYPES
typedef u_int8_t  uint8_t;
typedef u_int16_t uint16_t;
typedef u_int32_t uint32_t;
typedef u_int64_t uint64_t;
typedef s_int8_t  int8_t;
typedef s_int16_t int16_t;
typedef s_int32_t int32_t;
typedef s_int64_t int64_t;
#endif

#else
#include <ggi/system.h>
#endif

#include <stdlib.h>
#include <stdio.h>


/* Solaris doesn't have this!
 */
#ifdef HAVE_STDINT_H
#include <stdint.h>
#endif


/*******************************************
 * 2D RANGE MANAGER
 */

#ifndef RM_ALLOC_STEP
#define RM_ALLOC_STEP 16
#endif

#define RM_OK		0
#define RM_ENOMEM	-1
#define RM_EINDEX       -2

/*
  These are OR'ed to set the range properties
  when adding in a range list. The user should only use
  the 'user flags'
 */
typedef uint32_t rangeFlags;


#define RANGE_REQUEST_MASK	0x0000ffff
#define RANGE_ANSWER_MASK	0xffff0000

/* user flags, may be OR'd */

	/* misc */

#define RANGE_RELOCATABLE	0x00000001 /* set if the range can be
						moved around */

	/* constraint */

#define RANGE_CONSTRAINT_MASK	0x0000ff00

#define RANGE_START_MIN		0x00000100
#define RANGE_START_MAX		0x00000200
#define RANGE_START_ALIGN	0x00000400

#define RANGE_STRIDE_ALIGN	0x00001000
#define RANGE_STRIDE_FIXED	0x00002000 /* the stride can't be changed */

/* range manager response */

#define RANGE_OK_MASK		0x000f0000
#define RANGE_ALLOCATED		0x00010000 /* For newly allocated ranges.           */
#define RANGE_RELOCATED		0x00020000 /* Existing range has been moved around. */

#define RANGE_KO_MASK		0x00f00000
#define RANGE_INVALID		0x00100000 /* e.g. start_max < start_min            */
#define RANGE_NO_FIT		0x00200000 /* Couldn't find a space.                */

/* rangeFlags */

typedef struct rm_range_attribute {

	/* Set by the caller. */

	size_t width;
	size_t height;

	void    * owner;

	rangeFlags flags;

	/* These are set by the range manager algorithm. */

	size_t start;
	size_t stride;

} rm_range_attribute_t;


typedef struct rm_range_constraint {
	size_t start_min;
	size_t start_max;
	size_t start_align;
	size_t stride_align;
} rm_range_constraint_t;


typedef struct rm_range {
	rm_range_attribute_t  attribute;
	rm_range_constraint_t constraint;
} rm_range_t;


typedef struct rm_range_list {
	rm_range_t * range;
	int count;            /* number of used ranges */
	int allocated;        /* number of slots   */
} rm_range_list_t;

/* A set of convienience macros */

#define RM_FLAGS(r,i)             (r->range[i].attribute.flags)
#define RM_SET_FLAGS(r,i,f)       (RM_FLAGS(r,i) |= f)
#define RM_UNSET_FLAGS(r,i,f)     (RM_FLAGS(r,i) &= ~f)
#define RM_CHECK_ALL_FLAGS(r,i,f) (RM_FLAGS(r,i) & f == f)
#define RM_CHECK_FLAGS(r,i,f)     (RM_FLAGS(r,i) & f)

#define RM_OWNER(r,i)        (r->range[i].attribute.owner)
#define RM_WIDTH(r,i)        (r->range[i].attribute.width)
#define RM_HEIGHT(r,i)       (r->range[i].attribute.height)

#define RM_START(r,i)        (r->range[i].attribute.start)
#define RM_STRIDE(r,i)       (r->range[i].attribute.stride)

#define RM_PADDING(r,i)      (RM_STRIDE(r,i)-RM_WIDTH(r,i))
#define RM_SURFACE(r,i)      (RM_STRIDE(r,i)*RM_HEIGHT(r,i)-RM_PADDING(r,i))
#define RM_END(r,i)          (RM_START(r,i) + RM_SURFACE(r,i) - 1)

#define RM_CSTR_START_MIN(r,i) (r->range[i].constraint.start_min)
#define RM_CSTR_START_MAX(r,i) (r->range[i].constraint.start_max)
#define RM_CSTR_START_GAP(r,i) (RM_CSTR_START_MAX(r,i)-RM_CSTR_START_MIN(r,i))
#define RM_CSTR_START_ALIGN(r,i) (r->range[i].constraint.start_align)

#define RM_CSTR_STRIDE_ALIGN(r,i) (r->range[i].constraint.stride_align)


/* Allocates/frees a range list. */
MMUTILAPIFUNC int rm_list_init(rm_range_list_t ** rl);
MMUTILAPIFUNC int rm_list_exit(rm_range_list_t ** rl);

/*
  Adds a new range in the list (by copy). return the index;
*/
MMUTILAPIFUNC int rm_list_add(rm_range_list_t * rl, rm_range_t * range);

/* Removes the range at the given index */
MMUTILAPIFUNC int rm_list_remove(rm_range_list_t * rl, int idx);

/* Tries to fit the ranges in the request list into rl.
   The req->range[*] attributes will be set accordingly.
   'size' is the size of the range. The req->range[*]
   might be shuffled.
*/
MMUTILAPIFUNC int rm_list_fit(rm_range_list_t * rl,
			      rm_range_list_t * req,
			      size_t size);

/*
  Allocate in rl the valid ranges found in req.
  It must follow a call to rm_list_fit.
 */
MMUTILAPIFUNC int rm_list_alloc(rm_range_list_t * rl,
				rm_range_list_t * req);

/* Print out debugging info */
MMUTILAPIFUNC void rm_range_print(rm_range_list_t * r, int i);
MMUTILAPIFUNC void rm_list_print(rm_range_list_t * rl,size_t i);


/**********************************************************
 * Memory chunks, used to keep track of free chunks.
 */

#define CHK_START(l,i) (l->chunk[i].offset)
#define CHK_WIDTH(l,i) (l->chunk[i].length)

#define CHK_END(l,i) (CHK_START(l,i)+CHK_WIDTH(l,i)-1)

#define CHK_BEFORE(l,i,v) (CHK_START(l,i)>v)
#define CHK_AFTER(l,i,v) (CHK_END(l,i)<v)
#define CHK_IN(l,i,v) (CHK_START(l,i)<=v && CHK_END(l,i)>=v)

typedef struct rm_chunk {
	size_t offset;
	size_t length;
} rm_chunk_t;

typedef struct rm_chunk_list {
	rm_chunk_t * chunk;
	int count;
	int allocated;
	size_t size;
	/* maybe :
	   size_t free; // remaining memory.
	   size_t wasted; // marked as used but actually free
	   (relevant padding)
	 */
} rm_chunk_list_t;

int rm_chunk_init(rm_chunk_list_t ** cl, size_t size);
int rm_chunk_exit(rm_chunk_list_t ** cl);

/* Marks a simple area as used */
int rm_chunk_use(rm_chunk_list_t * cl, size_t start, size_t length);

/* Frees a used area */
int rm_chunk_free(rm_chunk_list_t * cl, size_t start, size_t length);

/* Marks the area covered by a range as used.
   The relpad is the 'relevant padding'. If the range padding <=
   relpadd, then the surface is marked as a single chunk. (saves
   time and memory for small padding). */
int rm_chunk_allocate_range(rm_chunk_list_t * cl, rm_range_t * r,
			    size_t relpad);

/* Frees a range. be careful that the relpad arg should be same
   or bigger than the one used for allocation. */
int rm_chunk_free_range(rm_chunk_list_t * cl, rm_range_t * r,
			size_t relpad);

/* Prints out debugging info */
void rm_chunk_print(rm_chunk_list_t * cl);


/*******************************************
 *        BATCH OPERATION
 */

#define BO_OK 0


/***************** Generic endian/bitorder swapping macros *******************
 * This being several millenia AFTER mankind invented the wheel,
 * one would THINK there would be a standard (or at least popular)
 * header for this type of thing, but damned if I can find one...
 */


#define BO_SWAB64_32(v) ((v<<32) | (v>>32))
#define BO_SWAB64_16(v) \
(((v<<16) & 0xffff0000ffff0000LL) | ((v>>16) & 0x0000ffff0000ffffLL))

#define BO_SWAB64_8(v)  \
(((v<<8)  & 0xff00ff00ff00ff00LL) | ((v>>8)  & 0x00ff00ff00ff00ffLL))
#define BO_SWAB64_4(v)  \
(((v<<4)  & 0xf0f0f0f0f0f0f0f0LL) | ((v>>4)  & 0x0f0f0f0f0f0f0f0fLL))
#define BO_SWAB64_2(v)  \
(((v<<2)  & 0xccccccccccccccccLL) | ((v>>2)  & 0x3333333333333333LL))
#define BO_SWAB64_1(v)  \
(((v<<1)  & 0xaaaaaaaaaaaaaaaaLL) | ((v>>1)  & 0x5555555555555555LL))
#define BO_RE64(v) BO_SWAB64_8(BO_SWAB64_16(BO_SWAB64_32(v)))
#define BO_BACKASS64(v) BO_SWAB64_1(BO_SWAB64_2(BO_SWAB64_4(BO_RE64(v))))

#define BO_SWAB32_16(v) ((v<<16) | (v>>16))
#define BO_SWAB32_8(v) (((v<<8) & 0xff00ff00) | ((v>>8) & 0x00ff00ff))
#define BO_SWAB32_4(v) (((v<<4) & 0xf0f0f0f0) | ((v>>4) & 0x0f0f0f0f))
#define BO_SWAB32_2(v) (((v<<2) & 0xcccccccc) | ((v>>2) & 0x33333333))
#define BO_SWAB32_1(v) (((v<<1) & 0xaaaaaaaa) | ((v>>1) & 0x55555555))
#define BO_RE32(v) BO_SWAB32_8(BO_SWAB32_16(v))
#define BO_BACKASS32(v) BO_SWAB32_1(BO_SWAB32_2(BO_SWAB32_4(BO_RE32_8(v))))

#define BO_SWAB16_8(v) ((v<<8) | (v>>8))
#define BO_SWAB16_4(v) (((v<<4) & 0xf0f0) | ((v>>4) & 0x0f0f))
#define BO_SWAB16_2(v) (((v<<2) & 0xcccc) | ((v>>2) & 0x3333))
#define BO_SWAB16_1(v) (((v<<1) & 0xaaaa) | ((v>>1) & 0x5555))
#define BO_RE16(v) BO_SWAB16_8(v)
#define BO_BACKASS16(v) BO_SWAB16_1(BO_SWAB16_2(BO_SWAB16_4(BO_SWAB16_8(v))))

#define BO_SWAB8_4(v) ((v<<4) | (v>>4))
#define BO_SWAB8_2(v) (((v<<2) & 0xcc) | ((v>>2) & 0x33))
#define BO_SWAB8_1(v) (((v<<1) & 0xaa) | ((v>>1) & 0x55))
#define BO_BACKASS8(v) BO_SWAB8_1(BO_SWAB8_2(BO_SWAB8_4(v)))

/* These defines apply to the parmtype members in struct batchparm.
 */

#define BO_PT_TYPE_MASK		0x0000ff00
#define BO_PT_SUBTYPE_MASK	0x0000ffff
#define BO_PT_SINT              0x40000000 /* field, arg are signed 2s comp */
#define BO_PT_JUSTIFY_MS	0x20000000 /* Keep most signifigant */
#define BO_PT_INVERT		0x10000000 /* Active low bits */
#define BO_PT_IN_ARG_MASK       0x00f00000 /* Inbound op with arg member */
#define BO_PT_IN_CMP_EQ         0x00100000
#define BO_PT_IN_CMP_LT         0x00200000
#define BO_PT_IN_CMP_LTE        0x00300000
#define BO_PT_IN_CMP_GT         0x00400000
#define BO_PT_IN_CMP_GTE        0x00500000
#define BO_PT_IN_CMP_NE         0x00600000
#define BO_PT_IN_ADD            0x00800000
#define BO_PT_IN_NEGATE         0x50800000 /* arg must contain +1 for this */
#define BO_PT_OUT_ARG_MASK      0x000f0000 /* Outbound op with arg member */
#define BO_PT_OUT_CMP_EQ        0x00010000
#define BO_PT_OUT_CMP_LT        0x00020000
#define BO_PT_OUT_CMP_LTE       0x00030000
#define BO_PT_OUT_CMP_GT        0x00040000
#define BO_PT_OUT_CMP_GTE       0x00050000
#define BO_PT_OUT_CMP_NE        0x00060000
#define BO_PT_OUT_ADD           0x00080000
#define BO_PT_OUT_NEGATE        0x50080000 /* arg must contain +1 for this */

/* The following flags refer to the endian member of the batchparm.
 * BO_EB* pertain to the block in which the fields are stored
 * BO_EP* pertain to the field after any block endianness has been removed
 */
#define BO_EB_MASK		0xfc000000 /* Endianness of block */
#define BO_EB_RE_32		0x80000000 /* Swap 32 bit chunks */
#define BO_EB_RE_16		0x40000000 /* Swap 16 bit chunks */
#define BO_EB_RE_8 		0x20000000 /* Swap bytes */
#define BO_EB_RE_4		0x10000000 /* Swap nybbles */
#define BO_EB_RE_2		0x08000000 /* Swap 2 bit chunks */
#define BO_EB_RE_1		0x04000000 /* Swap bits */
#define BO_AW_MASK		0x00f00000 /* Access widths of block */
#define BO_AW_8			0x00800000 /* 64-bit access OK */
#define BO_AW_4			0x00400000 /* 32-bit access OK */
#define BO_AW_2			0x00200000 /* 16-bit access OK */
#define BO_AW_1			0x00100000 /* 8-bit access OK */
#define BO_AW_ANY		0x00000000 /* access unused OK, any width */
#define BO_EP_MASK              0x0000fc00 /* Endianness of field */
#define BO_EP_RE_32		0x00008000 /* Swap 32 bit chunks */
#define BO_EP_RE_16		0x00004000 /* Swap 16 bit chunks */
#define BO_EP_RE_8 		0x00002000 /* Swap bytes */
#define BO_EP_RE_4		0x00001000 /* Swap nybbles */
#define BO_EP_RE_2		0x00000800 /* Swap 2 bit chunks */
#define BO_EP_RE_1		0x00000400 /* Swap bits */
#define BO_EP_BACKASS_WORDS	0x00003c00 /* full Reverse bit order words */
#define BO_EP_BACKASS_BYTES	0x00001c00 /* full Reverse bit order bytes */

/* These operations are used to adjust endianness when
   reading/writing batchparm data. */

#ifndef BYTE_ORDER
#	ifdef __BYTE_ORDER
#		define BYTE_ORDER	__BYTE_ORDER
#	endif
#endif


#if !defined(BYTE_ORDER) || \
	(BYTE_ORDER != BIG_ENDIAN && BYTE_ORDER != LITTLE_ENDIAN && \
	BYTE_ORDER != PDP_ENDIAN)
	/*
	 * you must determine what the correct bit order is for
	 * your compiler - the next line is an intentional error
	 * which will force your compiles to bomb until you fix
	 * the above macros.
	 */

#ifndef LITTLE_ENDIAN
#define LITTLE_ENDIAN	1234
#endif
#ifndef BIG_ENDIAN
#define BIG_ENDIAN	4321
#endif
#ifndef PDP_ENDIAN
#define PDP_ENDIAN	3412
#endif

#include <ggi/system.h>
#if defined(GGI_BIG_ENDIAN)
#	define BYTE_ORDER	BIG_ENDIAN
#elif defined(GGI_LITTLE_ENDIAN)
#	define BYTE_ORDER	LITTLE_ENDIAN
#else
#	error "Undefined or invalid BYTE_ORDER";
#endif
#endif



#if BYTE_ORDER == LITTLE_ENDIAN

#define BO_DO_BLOCKSWAB64(p, v) \
	do {								\
		if (!(p->access & BO_EB_RE_32)) v = BO_SWAB64_32(v);	\
		if (!(p->access & BO_EB_RE_16)) v = BO_SWAB64_16(v);	\
		if (!(p->access & BO_EB_RE_8))  v = BO_SWAB64_8(v);	\
		if (p->access & BO_EB_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EB_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EB_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);

#define BO_DO_BLOCKSWAB32(p, v) \
	do {								\
		if (!(p->access & BO_EB_RE_16)) v = BO_SWAB64_16(v);	\
		if (!(p->access & BO_EB_RE_8))  v = BO_SWAB64_8(v);	\
		if (p->access & BO_EB_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EB_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EB_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);

#define BO_DO_BLOCKSWAB16(p, v) \
	do {								\
		if (!(p->access & BO_EB_RE_8))  v = BO_SWAB64_8(v);	\
		if (p->access & BO_EB_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EB_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EB_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);

#define BO_DO_BLOCKSWAB8(p, v) \
	do {								\
		if (p->access & BO_EB_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EB_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EB_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);


#define BO_DO_PARMSWAB64(p, v) \
	do {								\
		if (!(p->access & BO_EP_RE_32)) v = BO_SWAB64_32(v);	\
		if (!(p->access & BO_EP_RE_16)) v = BO_SWAB64_16(v);	\
		if (!(p->access & BO_EP_RE_8))  v = BO_SWAB64_8(v);	\
		if (p->access & BO_EP_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EP_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EP_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);

#define BO_DO_PARMSWAB32(p, v) \
	do {								\
		if (!(p->access & BO_EP_RE_16)) v = BO_SWAB64_16(v);	\
		if (!(p->access & BO_EP_RE_8))  v = BO_SWAB64_8(v);	\
		if (p->access & BO_EP_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EP_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EP_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);

#define BO_DO_PARMSWAB16(p, v) \
	do {								\
		if (!(p->access & BO_EP_RE_8))  v = BO_SWAB64_8(v);	\
		if (p->access & BO_EP_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EP_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EP_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);

#define BO_DO_PARMSWAB8(p, v) \
	do {								\
		if (p->access & BO_EP_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EP_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EP_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);

#define BO_ENDIANNESS_IMPLEMENTED
#endif

#if BYTE_ORDER == BIG_ENDIAN
#define BO_DO_BLOCKSWAB64(p, v) \
	do {								\
		if (p->access & BO_EB_RE_32) v = BO_SWAB64_32(v);	\
		if (p->access & BO_EB_RE_16) v = BO_SWAB64_16(v);	\
		if (p->access & BO_EB_RE_8)  v = BO_SWAB64_8(v);	\
		if (p->access & BO_EB_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EB_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EB_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);

#define BO_DO_BLOCKSWAB32(p, v) \
	do {								\
		if (p->access & BO_EB_RE_16) v = BO_SWAB64_16(v);	\
		if (p->access & BO_EB_RE_8)  v = BO_SWAB64_8(v);	\
		if (p->access & BO_EB_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EB_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EB_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);

#define BO_DO_BLOCKSWAB16(p, v) \
	do {								\
		if (p->access & BO_EB_RE_8)  v = BO_SWAB64_8(v);	\
		if (p->access & BO_EB_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EB_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EB_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);

#define BO_DO_BLOCKSWAB8(p, v) \
	do {								\
		if (p->access & BO_EB_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EB_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EB_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);


#define BO_DO_PARMSWAB64(p, v) \
	do {								\
		if (p->access & BO_EP_RE_32) v = BO_SWAB64_32(v);	\
		if (p->access & BO_EP_RE_16) v = BO_SWAB64_16(v);	\
		if (p->access & BO_EP_RE_8)  v = BO_SWAB64_8(v);	\
		if (p->access & BO_EP_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EP_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EP_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);

#define BO_DO_PARMSWAB32(p, v) \
	do {								\
		if (p->access & BO_EP_RE_16) v = BO_SWAB64_16(v);	\
		if (p->access & BO_EP_RE_8)  v = BO_SWAB64_8(v);	\
		if (p->access & BO_EP_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EP_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EP_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);

#define BO_DO_PARMSWAB16(p, v) \
	do {								\
		if (p->access & BO_EP_RE_8)  v = BO_SWAB64_8(v);	\
		if (p->access & BO_EP_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EP_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EP_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);

#define BO_DO_PARMSWAB8(p, v) \
	do {								\
		if (p->access & BO_EP_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EP_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EP_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);
#define BO_ENDIANNESS_IMPLEMENTED
#endif

#if BYTE_ORDER == PDP_ENDIAN
#define BO_DO_BLOCKSWAB64(p, v) \
	do {								\
		if (p->access & BO_EB_RE_32) v = BO_SWAB64_32(v);	\
		if (!(p->access & BO_EB_RE_16)) v = BO_SWAB64_16(v);	\
		if (p->access & BO_EB_RE_8)  v = BO_SWAB64_8(v);	\
		if (p->access & BO_EB_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EB_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EB_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);

#define BO_DO_BLOCKSWAB32(p, v) \
	do {								\
		if (!(p->access & BO_EB_RE_16)) v = BO_SWAB64_16(v);	\
		if (p->access & BO_EB_RE_8)  v = BO_SWAB64_8(v);	\
		if (p->access & BO_EB_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EB_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EB_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);

#define BO_DO_BLOCKSWAB16(p, v) \
	do {								\
		if (p->access & BO_EB_RE_8)  v = BO_SWAB64_8(v);	\
		if (p->access & BO_EB_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EB_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EB_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);

#define BO_DO_BLOCKSWAB8(p, v) \
	do {								\
		if (p->access & BO_EB_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EB_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EB_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);


#define BO_DO_PARMSWAB64(p, v) \
	do {								\
		if (p->access & BO_EP_RE_32) v = BO_SWAB64_32(v);	\
		if (!(p->access & BO_EP_RE_16)) v = BO_SWAB64_16(v);	\
		if (p->access & BO_EP_RE_8)  v = BO_SWAB64_8(v);	\
		if (p->access & BO_EP_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EP_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EP_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);

#define BO_DO_PARMSWAB32(p, v) \
	do {								\
		if (!(p->access & BO_EP_RE_16)) v = BO_SWAB64_16(v);	\
		if (p->access & BO_EP_RE_8)  v = BO_SWAB64_8(v);	\
		if (p->access & BO_EP_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EP_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EP_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);

#define BO_DO_PARMSWAB16(p, v) \
	do {								\
		if (p->access & BO_EP_RE_8)  v = BO_SWAB64_8(v);	\
		if (p->access & BO_EP_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EP_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EP_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);

#define BO_DO_PARMSWAB8(p, v) \
	do {								\
		if (p->access & BO_EP_RE_4)  v = BO_SWAB64_4(v);	\
		if (p->access & BO_EP_RE_2)  v = BO_SWAB64_2(v);	\
		if (p->access & BO_EP_RE_1)  v = BO_SWAB64_1(v);	\
	} while (0);
#define BO_ENDIANNESS_IMPLEMENTED
#endif

#ifndef BO_ENDIANNESS_IMPLEMENTED
#error Code not available for your endianness
#endif


/* The following flags apply to the in.flags/out.flags members */
#define BO_TRANSIENT_IS	    0x80000000 /* This parm is a transient. */
#define BO_TRANSIENT_LOADED 0x40000000 /* transient initial val loaded */
#define BO_TRANSIENT_LINKED 0x20000000 /* parm loads to/from a transient */

/**************** batchparm and batchop structures ***************************/

struct batchparm {
	uint64_t *block;	/* 64bit aligned address of block data */
	int offset;		/* Offset in block in bits of first value */
#define BO_BP_OFFSET_MAX 32767
#define BO_BP_OFFSET_MIN -32768
	int step;		/* Stride in bits to next value */
#define BO_BP_STEP_MAX   32767
#define BO_BP_STEP_MIN   -32768
	int datasize;		/* Data item size */
#define BO_BP_DATASIZE_MAX 64
	int parmtype;		/* Data item purpose and adjuster flags. */
	int access;		/* Describes endianness of block/parm field */
	int transient;		/* Flags for handling transients */
	union {			/* Argument to adjuster */
		uint64_t u64;
		int64_t s64;
	} arg;
	struct { /* in, out pointers value in to current batchop "row" */
		union {		/*  pointer to current value */
			uint64_t *u64; /* 64bit-aligned */
			uint32_t *u32; /* 32bit-aligned */
			uint16_t *u16; /* 16bit-aligned */
			uint8_t  *u8;  /* 8bit-aligned  */
			int64_t *s64; /* 64bit-aligned signed */
			int32_t *s32; /* 32bit-aligned signed */
			int16_t *s16; /* 16bit-aligned signed */
			int8_t  *s8;  /* 8bit-aligned  signed */
			void   *v;
		} ptr;
                int boff; 		/* Bit offset from ptr */
		int flags;		/* Other transfer state */
        } in, out;
        struct { /* match structure used to tie batchops together */
                int src;	/* Which source parm other batchop to use */
                int parmtype;   /* Cached data adjustment */
                int size;       /* Cached size adjustment */
        } match;
};


typedef struct batchparm *batchparm_t;
typedef struct batchop *batchop_t;
typedef int (bo_pt_callback)(int srcpt, int destpt);
typedef int (bo_match_func)(batchop_t src, batchop_t dest,
			    bo_pt_callback *cmp);
typedef int (bo_go_func)(batchop_t src, batchop_t dest, int numrows);


struct batchop {

	batchparm_t parms;	/* Pointer to batchparms contained here. */
	int numparms;		/* Number of batchparms contained here. */
	bo_match_func *match;	/* Function to match to a source batchop */
	batchop_t lastmatch;	/* Last source batchop we matched to */
	bo_go_func *go;		/* Function to transfer data from lastmatch */

	struct {		/* Current row of transfer from lastmatch */
		int idx;
	} in, out;
#define BO_IDX_MAX 32767
#define BO_IDX_MIN 0

};

batchop_t bo_calloc(size_t numparms, uint64_t *defblock);
void bo_free(batchop_t this);
MMUTILAPIFUNC bo_match_func	bo_def_match;
MMUTILAPIFUNC bo_go_func	bo_def_go;

/* Use of ternary gives us return code and sanity check. */
#define bo_match(src, dest, cmp) \
((dest->match == NULL) ? \
bo_def_match(src, dest, cmp) : dest->match(src, dest, cmp))

#define bo_go(src, dest, n) \
((dest->go == NULL)    ? bo_def_go(src, dest, n) : dest->go(src, dest, n))


/* Now for lots of building-block macros... oh, if only inline
 * was pervasive.  I hate debugging code with macros like this... sigh.
 */

#define _bo_initparms(bo) \
        do {                                                            \
                int _bo_initparms_lcv;                                  \
                for (_bo_initparms_lcv = 0;				\
		     _bo_initparms_lcv < bo->numparms;			\
		     _bo_initparms_lcv++) {				\
                        bo->parms[_bo_initparms_lcv].match.src = -1;    \
                        bo->parms[_bo_initparms_lcv].transient &=	\
				~BO_TRANSIENT_LINKED;     		\
                }                                                       \
        } while(0);

#define _bo_setparmidx(p, dir, idx) \
        do {                                                            \
		int64_t _bo_setparmidx_tmp;				\
		if (p->step > BO_BP_STEP_MAX)				\
			_bo_setparmidx_tmp = BO_BP_STEP_MAX;		\
		else if (p->step < BO_BP_STEP_MIN)			\
			_bo_setparmidx_tmp = BO_BP_STEP_MIN;		\
		else _bo_setparmidx_tmp = p->step;			\
									\
		if (idx > BO_IDX_MAX)					\
			_bo_setparmidx_tmp *= BO_IDX_MAX;		\
		else if (idx < BO_IDX_MIN)				\
			_bo_setparmidx_tmp *= BO_IDX_MIN;		\
		else _bo_setparmidx_tmp *= idx;				\
									\
		if (p->offset > BO_BP_OFFSET_MAX)			\
			_bo_setparmidx_tmp += BO_BP_OFFSET_MAX;		\
		else if (p->offset < BO_BP_OFFSET_MIN)			\
			_bo_setparmidx_tmp += BO_BP_OFFSET_MIN;		\
		else _bo_setparmidx_tmp += p->offset;			\
									\
		p->dir.ptr.u64 = p->block + _bo_setparmidx_tmp/64;	\
		if (_bo_setparmidx_tmp < 0) p->dir.ptr.u64--;		\
		p->dir.boff = _bo_setparmidx_tmp % 64;			\
        } while(0);

#define _bo_setidx(bo, dir, newidx) \
        do {								\
                int _bo_setidx_lcv;					\
                bo->dir.idx = newidx;					\
		if (bo->dir.idx > BO_IDX_MAX) bo->dir.idx = BO_IDX_MAX;	\
		if (bo->dir.idx < BO_IDX_MIN) bo->dir.idx = BO_IDX_MIN;	\
                for (_bo_setidx_lcv = 0;				\
		     _bo_setidx_lcv < bo->numparms;			\
		     _bo_setidx_lcv++)					\
                  _bo_setparmidx((bo->parms + _bo_setidx_lcv),		\
				 dir, bo->dir.idx);			\
        } while(0);

#define bo_set_out_idx(bo, idx) _bo_setidx(bo, out, idx)
#define bo_set_in_idx(bo, idx)  _bo_setidx(bo, in,  idx)

#define _bo_incparmidx(p, dir) \
        do {                                                            \
		if (p->step > BO_BP_STEP_MAX)				\
			p->dir.boff += BO_BP_STEP_MAX;			\
		else if (p->step < BO_BP_STEP_MIN)			\
			p->dir.boff += BO_BP_STEP_MIN;			\
		else p->dir.boff += p->step;				\
                p->dir.ptr.u64 += p->dir.boff/64;   			\
                if (p->dir.boff < 0) p->dir.ptr.u64--;   		\
                p->dir.boff %= 64;					\
        } while (0);

#define _bo_incidx(bo, dir) \
        do {                                                            \
                int _bo_incidx_lcv;					\
		if (bo->dir.idx < BO_IDX_MAX)				\
                	for (_bo_incidx_lcv = 0;			\
			     _bo_incidx_lcv < bo->numparms;		\
			     _bo_incidx_lcv++)				\
                   _bo_incparmidx((bo->parms + _bo_incidx_lcv), dir);   \
                bo->dir.idx++;                                          \
        } while(0);

#define bo_inc_out_idx(bo) _bo_incidx(bo, out)
#define bo_inc_in_idx(bo)  _bo_incidx(bo, in)

#endif	/* _GGI_MMUTIL_H */
