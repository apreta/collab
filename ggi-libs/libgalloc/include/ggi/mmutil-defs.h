/* $Id: mmutil-defs.h,v 1.2 2005/07/31 15:29:57 soyt Exp $
******************************************************************************

   mmutils mini-library API header file

   Copyright (C) 2004      Peter Ekberg		[peda@lysator.liu.se]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#ifndef _GGI_MMUTIL_DEFS_H
#define _GGI_MMUTIL_DEFS_H

#ifndef MMUTIL_STANDALONE
#include <ggi/system.h>
#else

#ifdef __CYGWIN32__
# ifndef __CYGWIN__
#  define __CYGWIN__	1
# endif
#endif

#ifndef __WIN32__
# if defined(__CYGWIN__)  || \
    defined(__MINGW32__) || \
    defined(__MINGW__)   || \
    defined(_WIN32)
# define __WIN32__
# endif
#endif /* ! __WIN32__ */

#ifdef __WIN32__
# define EXPORTVAR	extern __declspec(dllexport)
# define IMPORTVAR	extern __declspec(dllimport)
# define EXPORTFUNC	__declspec(dllexport)
# define IMPORTFUNC	extern __declspec(dllimport)
#else
# define EXPORTVAR	extern
# define IMPORTVAR	extern
# define EXPORTFUNC	/* empty */
# define IMPORTFUNC	extern
#endif
#endif /* MMUTIL_STANDALONE */

#ifdef BUILDING_LIBMMUTILS
# define MMUTILAPIFUNC EXPORTFUNC
# define MMUTILAPIVAR  EXPORTVAR
#else
# define MMUTILAPIFUNC IMPORTFUNC
# define MMUTILAPIVAR  IMPORTVAR
#endif

#endif /* _GGI_MMUTIL_DEFS_H */
