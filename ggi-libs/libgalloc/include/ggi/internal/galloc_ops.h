/* $Id: galloc_ops.h,v 1.41 2007/06/26 20:10:45 mooz Exp $
******************************************************************************

   LibGalloc: Useful stuff for high wizards

   Copyright (C) 2002 Christoph Egger   [Christoph_Egger@t-online.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#ifndef _GGI_INTERNAL_GALLOC_OPS_H
#define _GGI_INTERNAL_GALLOC_OPS_H


#include <ggi/internal/ggi.h>

/* "black-ops" section.  People who use these functions drive cars with
 * tinted windows and duct-tape lethal weapons to their thigh.
 */


/* Convenient list macros
 */
#define RESLIST_FOREACH(n, head)	GG_SIMPLEQ_FOREACH(n,head,listentry)
#define RESLIST_FIRST(head)		GG_SIMPLEQ_FIRST(head)
#define RESLIST_NEXT(elm)		GG_SIMPLEQ_NEXT(elm,listentry)
#define RESLIST_EMPTY(head)		GG_SIMPLEQ_EMPTY(head)
#define RESLIST_INIT(head)		GG_SIMPLEQ_INIT(head)
#define RESLIST_INSERT_HEAD(head,elm)	GG_SIMPLEQ_INSERT_HEAD(head,elm,listentry)
#define RESLIST_INSERT_TAIL(head,elm)	GG_SIMPLEQ_INSERT_TAIL(head,elm,listentry)
#define RESLIST_REMOVE(head,elm)	GG_SIMPLEQ_REMOVE(head,elm,ggiGA_resource,listentry)



/* Some simple macros to reduce code size/improve legibility.  nuff said. */
#define ggiGAFlagActive(res) \
	res->res_state |= GA_STATE_NORESET | GA_STATE_NOCHANGE;
#define ggiGAFlagFailed(res) res->res_state |= GA_STATE_FAILED
#define ggiGAFlagSuccess(res) res->res_state &= ~GA_STATE_FAILED
#define ggiGAFlagModified(res) res->res_state |= GA_STATE_MODIFIED

/* Copy properties of a resource structure. Properties of the destination
 * resource structure is overwritten then. So you are warned.
 */
int ggiGACopyProps(ggiGA_resource_handle from_handle,
		   ggiGA_resource_handle to_handle);

/* Copy private data of a resource structure. The private data of the
 * destination resource structure is overwritten. So you are warned.
 */
int ggiGACopyPriv(ggiGA_resource_handle from_handle,
		  ggiGA_resource_handle to_handle);

/* Create a copy of a resource structure and any associated
 * private data.
 */
int ggiGAClone(const ggiGA_resource_handle in, ggiGA_resource_handle *out);

/* Copy of a resource structure and any associated
 * private data. If out[0] == NULL, then it fails. This behaviour
 * allows you to make sure to not cause any kind of damage.
 */
int ggiGACopy(const ggiGA_resource_handle in, ggiGA_resource_handle out);

/* Makes a copy of a resource structure but does not copy the props/priv
 * like ggiGACopy does, instead, sets them to NULL pointers;
 */
int ggiGACloneNoProps(const ggiGA_resource_handle in, ggiGA_resource_handle *out);

/* Prepend a copy of the resource at "handle" to the list "*list".
 * On success, "list" has a different value assigned to it.
 */
int ggiGAPrepend(ggiGA_resource_handle handle, ggiGA_resource_list *list);

/* Append a copy of the resource at "handle" to the list "*list".
 * If "copy" is false, it links the "handle" to the end of the list "*list".
 */
GGIGAAPIFUNC int ggiGAAppend(
	ggiGA_resource_handle handle,
	ggiGA_resource_list *list, int copy);


/* Removes (and frees) the resource pointed to by "handle" out of the
 * resource-list pointed to by "*list". if present, "list" is passed by
 * reference, as this may change the pointer to the
 * list (e.g. to NULL to indicate that the list is now empty.)
 */
GGIGAAPIFUNC int ggiGARemove(
	ggiGA_resource_list *list,
	ggiGA_resource_handle *handle);


/* Removes (and frees) the resource pointed to by "handle" out of the
 * resource-list pointed to by "*list", INCLUDING the cap, if present.
 * "list" is passed by reference, as this may change the pointer to the
 * list (e.g. to NULL to indicate that the list is now empty.)
 */
int ggiGARemoveWithCap(ggiGA_resource_list *list,
		       ggiGA_resource_handle *handle);

/* Frees a handle, setting the handle to NULL when done, but takes
 * care not to free any mutual props.  The handle should of course
 * NOT be present in the list at the time.
 */
int ggiGAFreeDetachedHandle(const ggiGA_resource_list list,
			    ggiGA_resource_handle *handle);


/* Create a copy of a linked list of resource structures and any associated
 * private data.
 */
GGIGAAPIFUNC int ggiGACopyList(
	const ggiGA_resource_list in,
	ggiGA_resource_list *out);

/* Link the list "this" to the end of the list "tothis".  No copying is done.
 * "tothis" is passed by reference, as this may change the pointer to that
 * list (e.g. from NULL to a real value if the list was empty.)
 */
GGIGAAPIFUNC int ggiGAAppendList(
	const ggiGA_resource_list this,
	ggiGA_resource_list *tothis);

/* Take any extra resources from the end of "list" and put them on the end
 * of "onto", then empty the previous resources in "list".  This is used
 * by extensions in their lazy-API Create functions to keep the global
 * request list in sync without changing existing handles.
 */
GGIGAAPIFUNC int ggiGASpliceList(
	ggiGA_resource_list *onto,
	ggiGA_resource_list *list);

/* Link "tothis" to the end of "this" (syntax sugar).  Note swapped types. */
#define ggiGAPrependList(this, tothis)      \
			ggiGAAppendList(tothis, this);

/* Finds the first resource in the list which points to the
 * same properties as "handle".  This is used by ggiGACopyList and
 * ggiGAFreeDetachedHandle to detect mutual resources.  You know a resource
 * is a mutual resource if the "handle" is in the list and ggiGAPropsHandle
 * does not return the same value given in "handle".  (But you do NOT
 * know the converse.)  This is a very internal function you must know
 * what you are doing to use correctly.
 */
ggiGA_resource_handle ggiGAPropsHandle(const ggiGA_resource_list list,
				       ggiGA_resource_handle handle);

/* Find the last resource in the compound resource to which "handle"
 * belongs.  The "list" parameter is not needed now, but may be if we
 * change the list structure.  "handle" should not be a CAP, and CAPs
 * are not returned, rather the resource before them is.
 */
GGIGAAPIFUNC ggiGA_resource_handle ggiGAFindLastSeeabove(
					const ggiGA_resource_list list,
					ggiGA_resource_handle handle);


/* Finds and returns the handle of the first (successful) GA_RT_FRAME
 * resource. If the returned "handle" is NULL, then there is _no_
 * GA_RT_FRAME in the list, which should never happen.
 */
ggiGA_resource_handle ggiGAFindFirstMode(const ggiGA_resource_list reslist);


/* Find the last successful GA_RT_FRAME resource before "handle" in
 * "reslist", or if "handle" is NULL the last in the entire list.  By "last
 * successful" we mean that if a failure is found before "handle", the search
 * returns NULL, not the last one that would have succeeded.
 */
GGIGAAPIFUNC ggiGA_resource_handle ggiGAFindLastMode(
			const ggiGA_resource_list reslist,
			const ggiGA_resource_handle handle);

ggiGA_resource_list ggiGAReverseList(ggiGA_resource_list list);


/* Black ops - section: Utils for compound resource manipulation
 */


GGIGAAPIFUNC int ggiGAAddSeeabove(ggiGA_resource_list *reqlist,
                     struct ggiGA_resource_props *props,
                     ggiGA_resource_type rt,
                     ggiGA_resource_handle *handle);

int ggiGAIsCompoundHead(const ggiGA_resource_handle handle);

GGIGAAPIFUNC int ggiGAIsCap(const ggiGA_resource_handle handle);

GGIGAAPIFUNC ggiGA_resource_handle ggiGAFindCompoundHead(
			const ggiGA_resource_list list,
			const ggiGA_resource_handle handle);

int ggiGAIsSeeabove(const ggiGA_resource_handle handle);

int ggiGAHasAttachedSeeAboves(const ggiGA_resource_list list,
			      const ggiGA_resource_handle handle);


/* Basic sanity check functions for resources and their caps. */
int ggiGAModeCapSanity(const ggiGA_resource_handle mode);
int ggiGAMotorCapSanity(const ggiGA_resource_handle res);
int ggiGATankCapSanity(const ggiGA_resource_handle res);
int ggiGACarbCapSanity(const ggiGA_resource_handle res);

/* Some handy macros for dealing with caps/GGI_AUTO */

/* Check if x is GGI_AUTO or x <op> y is true. */
#define GA_AUTOCMP(x,op,y) ((x != GGI_AUTO) && (x op y))


/* Check if either x or y are set to GGI_AUTO, or x <op> y is true. */
#define GA_AUTO2CMP(x,op,y) \
((x == GGI_AUTO) || (y == GGI_AUTO) || (x op y))


/* Compare members in (struct *) "res" to those in (struct *) "cap".
 * NOTE: Variable names "res" and "cap" must be used and must be of
 * correct type in order to use this.
 */
#define GA_CAPCMP(member, op) \
if (!GA_AUTO2CMP(cap->member, op, res->member)) return GALLOC_EFAILED;

/* As above, but throw in an assertion. */
#define GA_CAPCMP_ASSERT(member, op) \
LIB_ASSERT(GA_AUTO2CMP(cap->member, op, res->member), "cap member does not match");	\
if (!GA_AUTO2CMP(cap->member, op, res->member)) return GALLOC_EFAILED;


#define GA_LIMIT(r, x, op, y)	\
if (r->x op y) {		\
	r->x = y;               \
	rc = GALLOC_EFAILED;	\
}
/* Same comparing member _m in identical structures referenced by _a and _b. */
#define GA_LIMIT_M(_a, _op, _b, _m)	\
if (_a->_m _op _b->_m) {		\
	_a->_m = _b->_m;               	\
	rc = GALLOC_EFAILED;		\
}

/* If _a is GGI_AUTO or _a _op _b, set _a to _b.
 * Set rc if _a was not GGI_AUTO.  rc must be defined. 
 */
#define GA_AUTOLIMIT(_a, _op, _b)				\
if ((_a == GGI_AUTO) || ((_a _op _b) && (rc = GALLOC_EFAILED))) \
	_a = _b;
/* Same comparing member _m in identical structures referenced by _a and _b. */
#define GA_AUTOLIMIT_M(_a, _op, _b, _m)				\
if ((_a->_m == GGI_AUTO) || ((_a->_m _op _b->_m) && (rc = GALLOC_EFAILED))) \
	_a->_m = _b->_m;


#define GA_CHECK_CAPAUTO_2D(cap, maxwidth, maxheight, maxdepth)		\
									\
{									\
int cap_width = cap->props->size.area.x;				\
int cap_height = cap->props->size.area.y;				\
int cap_depth = cap->props->sub.tank.gt;				\
									\
if ((cap_width == GGI_AUTO) || (cap_width > maxwidth)) {		\
	cap_width = maxwidth;						\
}	/* if */							\
if ((cap_height == GGI_AUTO) || (cap_height > maxheight)) {		\
	cap_height = maxheight;						\
}	/* if */							\
if ((cap_depth == GGI_AUTO) || (cap_depth > maxdepth)) {		\
	cap_depth = maxdepth;						\
}	/* if */							\
									\
maxwidth = cap_width;							\
maxheight = cap_height;							\
maxdepth = cap_depth;							\
}

#endif	/* _GGI_INTERNAL_GALLOC_OPS_H */
