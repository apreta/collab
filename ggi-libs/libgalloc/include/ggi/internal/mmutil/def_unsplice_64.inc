

/* This code snip moves the data in a 64-bit value around
   such that it appears at the right bit locations for an output batchparm.

   It is inefficient, because it handles all the bells and whistles.
   If you don't have non-byte-aligned steps/widths and access width 
   restrictions, don't use it!  There are/will be/should be more optimized 
   versions for those situations.

   This version is intended for machines with all access operations from
        8 to 64 bits.

   You should supply the following as macros/variables:

   bB is a pointer to a 16-byte buffer, aligned on a 64-bit boundary,
        containing the data to be munged.
   bP is a pointer to the batchparm where the data is to end up, and 
        must be of type (batchparm *)
   WANT_MSB is an expression saying whether to prefer the most signifigant
        bytes.  If true, the least signifigant bytes will be discarded.
	If false, the most signifigant will be discarded.
*/

  /* Apply the field's endianness.  Note, we do NOT check for invalid
     combinations of datasize and endianness. (e.g. a 6-bit field can only
     be BO_EP_RE_1.)  Such combinations will produce spurious results. */
switch ((bP->datasize + 7) / 8) {
case 1:
	BO_DO_PARMSWAB8(bP, *((uint64_t *) bB));
	break;
case 2:
	BO_DO_PARMSWAB16(bP, *((uint64_t *) bB));
	break;
case 3:
case 4:
	BO_DO_PARMSWAB32(bP, *((uint64_t *) bB));
	break;
case 5:
case 6:
case 7:
case 8:
	BO_DO_PARMSWAB64(bP, *((uint64_t *) bB));
	break;
}

  /* Split across two uint64_t as per the bit offset/datasize */
if (WANT_MSB) {
	if (bP->out.boff + bP->datasize > 64)
		*((uint64_t *) bB + 1) =
		    (0xffffffffffffffffLL <<
		     (128 -
		      (bP->out.boff +
		       bP->datasize))) & (*((uint64_t *) bB) << (64 -
							       bP->out.
							       boff));
	*((uint64_t *) bB) >>= bP->out.boff;
	if (bP->out.boff + bP->datasize < 64)
		*((uint64_t *) bB) &=
		    ~(0xffffffffffffffffLL >>
		      (bP->out.boff + bP->datasize));
} else {
	if ((bP->out.boff + bP->datasize) < 64) {
		*((uint64_t *) bB + 1) =
		    *((uint64_t *) bB) << (128 -
					 (bP->out.boff + bP->datasize));
		*((uint64_t *) bB) >>= (bP->out.boff + bP->datasize) - 64;
		*((uint64_t *) bB) &= 0xffffffffffffffffLL >> (bP->out.boff);
	} else {
		*((uint64_t *) bB) <<= (bP->out.boff + bP->datasize) - 64;
		*((uint64_t *) bB) &= 0xffffffffffffffffLL >> bP->out.boff;
	}
}

  /* Adapt to the endianness of the block */
BO_DO_BLOCKSWAB64(bP, *((uint64_t *) bB));
BO_DO_BLOCKSWAB64(bP, *((uint64_t *) bB + 1));
