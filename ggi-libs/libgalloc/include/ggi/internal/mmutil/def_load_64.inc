



/*
 * This is the fallback method by which data is read out of a batchparm. It
 * is inefficient, because it handles all the bells and whistles. If you
 * don't have non-byte-aligned steps/widths and access width restrictions,
 * don't use it!  There are/will be/should be more optimized versions for
 * those situations.
 * 
 * This version is intended for machines with all access operations from 8 to 64
 * bits.
 * 
 * You should supply the following as macros:
 * 
 * bB is a pointer to a 16-byte buffer, aligned on a 64-bit boundary. it can be
 * any type of pointer, as it will be cast as needed. Note it is NOT
 * automatically cleared so unused bytes may contain spurious data
 * afterwards. bP is a pointer to the batchparm being processed, and must be
 * of type (batchparm *) bM should be an unsigned integer at least 16-bit.
 * It will be assigned, in the least signifigant 16 bits, a mask which tells
 * which bytes in the area pointed to by bB contain batchparm data. bDir
 * should be defined to "in" or "out" depending on whether you are loading an
 * input parameter or preloading an output parameter.
 */


bM = (0xffff << (16 - (bP->bDir.boff + bP->datasize + 7) / 8))
    & (0xffff >> (bP->bDir.boff / 8));
if (bP->access & BO_EB_RE_32)
	bM = (((bM << 4) & 0xf0f0) | ((bM >> 4) & 0x0f0f));
if (bP->access & BO_EB_RE_16)
	bM = (((bM << 2) & 0xcccc) | ((bM >> 2) & 0x3333));
if (bP->access & BO_EB_RE_8)
	bM = (((bM << 1) & 0xaaaa) | ((bM >> 1) & 0x5555));

switch (bP->access & BO_AW_MASK) {
case BO_AW_ANY:
	do {
		/* Way to do this better? */
		size_t i, s, e;
		s = i = 0;
		while (!((bM << i++) & 0x8000))
			s++;
		e = 16;
		i = 0;
		while (!((bM >> i++) & 0x0001))
			e--;
		memcpy(((uint8_t *) bB + s), bP->bDir.ptr.u8 + s, e - s);
	} while (0);
	break;
case BO_AW_1:
	if (bM & 0x8000)
		*((uint8_t *) bB) = *(bP->bDir.ptr.u8);
	if (bM & 0x4000)
		*(((uint8_t *) bB) + 1) = *(bP->bDir.ptr.u8 + 1);
	if (bM & 0x2000)
		*(((uint8_t *) bB) + 2) = *(bP->bDir.ptr.u8 + 2);
	if (bM & 0x1000)
		*(((uint8_t *) bB) + 3) = *(bP->bDir.ptr.u8 + 3);
	if (bM & 0x0800)
		*(((uint8_t *) bB) + 4) = *(bP->bDir.ptr.u8 + 4);
	if (bM & 0x0400)
		*(((uint8_t *) bB) + 5) = *(bP->bDir.ptr.u8 + 5);
	if (bM & 0x0200)
		*(((uint8_t *) bB) + 6) = *(bP->bDir.ptr.u8 + 6);
	if (bM & 0x0100)
		*(((uint8_t *) bB) + 7) = *(bP->bDir.ptr.u8 + 7);
	if (bM & 0x0080)
		*(((uint8_t *) bB) + 8) = *(bP->bDir.ptr.u8 + 8);
	if (bM & 0x0040)
		*(((uint8_t *) bB) + 9) = *(bP->bDir.ptr.u8 + 9);
	if (bM & 0x0020)
		*(((uint8_t *) bB) + 10) = *(bP->bDir.ptr.u8 + 10);
	if (bM & 0x0010)
		*(((uint8_t *) bB) + 11) = *(bP->bDir.ptr.u8 + 11);
	if (bM & 0x0008)
		*(((uint8_t *) bB) + 12) = *(bP->bDir.ptr.u8 + 12);
	if (bM & 0x0004)
		*(((uint8_t *) bB) + 13) = *(bP->bDir.ptr.u8 + 13);
	if (bM & 0x0002)
		*(((uint8_t *) bB) + 14) = *(bP->bDir.ptr.u8 + 14);
	if (bM & 0x0001)
		*(((uint8_t *) bB) + 15) = *(bP->bDir.ptr.u8 + 15);
	break;
case BO_AW_2:
	if (bM & 0xc000)
		*((uint16_t *) bB) = *(bP->bDir.ptr.u16);
	if (bM & 0x3000)
		*(((uint16_t *) bB) + 1) = *(bP->bDir.ptr.u16 + 1);
	if (bM & 0x0c00)
		*(((uint16_t *) bB) + 2) = *(bP->bDir.ptr.u16 + 2);
	if (bM & 0x0300)
		*(((uint16_t *) bB) + 3) = *(bP->bDir.ptr.u16 + 3);
	if (bM & 0x00c0)
		*(((uint16_t *) bB) + 4) = *(bP->bDir.ptr.u16 + 4);
	if (bM & 0x0030)
		*(((uint16_t *) bB) + 5) = *(bP->bDir.ptr.u16 + 5);
	if (bM & 0x000c)
		*(((uint16_t *) bB) + 6) = *(bP->bDir.ptr.u16 + 6);
	if (bM & 0x0003)
		*(((uint16_t *) bB) + 7) = *(bP->bDir.ptr.u16 + 7);
	break;
case BO_AW_2 | BO_AW_1:
	if ((bM & 0xc000) == 0xc000)
		*((uint16_t *) bB) = *(bP->bDir.ptr.u16);
	else {
		if (bM & 0x8000)
			*((uint8_t *) bB) = *(bP->bDir.ptr.u8);
		if (bM & 0x4000)
			*(((uint8_t *) bB) + 1) = *(bP->bDir.ptr.u8 + 1);
	}
	if ((bM & 0x3000) == 0x3000)
		*(((uint16_t *) bB) + 1) = *(bP->bDir.ptr.u16 + 1);
	else {
		if (bM & 0x2000)
			*(((uint8_t *) bB) + 2) = *(bP->bDir.ptr.u8 + 2);
		if (bM & 0x1000)
			*(((uint8_t *) bB) + 3) = *(bP->bDir.ptr.u8 + 3);
	}
	if ((bM & 0x0c00) == 0x0c00) {
		*(((uint16_t *) bB) + 2) = *(bP->bDir.ptr.u16 + 2);
	} else {
		if (bM & 0x0800)
			*(((uint8_t *) bB) + 4) = *(bP->bDir.ptr.u8 + 4);
		if (bM & 0x0400)
			*(((uint8_t *) bB) + 5) = *(bP->bDir.ptr.u8 + 5);
	}
	if ((bM & 0x0300) == 0x0300)
		*(((uint16_t *) bB) + 3) = *(bP->bDir.ptr.u16 + 3);
	else {
		if (bM & 0x0200)
			*(((uint8_t *) bB) + 6) = *(bP->bDir.ptr.u8 + 6);
		if (bM & 0x0100)
			*(((uint8_t *) bB) + 7) = *(bP->bDir.ptr.u8 + 7);
	}
	if ((bM & 0x00c0) == 0x00c0)
		*(((uint16_t *) bB) + 4) = *(bP->bDir.ptr.u16 + 4);
	else {
		if (bM & 0x0080)
			*(((uint8_t *) bB) + 8) = *(bP->bDir.ptr.u8 + 8);
		if (bM & 0x0040)
			*(((uint8_t *) bB) + 9) = *(bP->bDir.ptr.u8 + 9);
	}
	if ((bM & 0x0030) == 0x0030)
		*(((uint16_t *) bB) + 5) = *(bP->bDir.ptr.u16 + 5);
	else {
		if (bM & 0x0020)
			*(((uint8_t *) bB) + 10) = *(bP->bDir.ptr.u8 + 10);
		if (bM & 0x0010)
			*(((uint8_t *) bB) + 11) = *(bP->bDir.ptr.u8 + 11);
	}
	if ((bM & 0x000c) == 0x000c)
		*(((uint16_t *) bB) + 6) = *(bP->bDir.ptr.u16 + 6);
	else {
		if (bM & 0x0008)
			*(((uint8_t *) bB) + 12) = *(bP->bDir.ptr.u8 + 12);
		if (bM & 0x0004)
			*(((uint8_t *) bB) + 13) = *(bP->bDir.ptr.u8 + 13);
	}
	if ((bM & 0x0003) == 0x0003)
		*(((uint16_t *) bB) + 7) = *(bP->bDir.ptr.u16 + 7);
	else {
		if (bM & 0x0002)
			*(((uint8_t *) bB) + 14) = *(bP->bDir.ptr.u8 + 14);
		if (bM & 0x0001)
			*(((uint8_t *) bB) + 15) = *(bP->bDir.ptr.u8 + 15);
	}
	break;
case BO_AW_4:
	if (bM & 0xf000)
		*((uint32_t *) bB) = *(bP->bDir.ptr.u32);
	if (bM & 0x0f00)
		*(((uint32_t *) bB) + 1) = *(bP->bDir.ptr.u32 + 1);
	if (bM & 0x00f0)
		*(((uint32_t *) bB) + 2) = *(bP->bDir.ptr.u32 + 2);
	if (bM & 0x000f)
		*(((uint32_t *) bB) + 3) = *(bP->bDir.ptr.u32 + 3);
	break;
case BO_AW_4 | BO_AW_1:
	if ((bM & 0xf000) == 0xf000)
		*((uint32_t *) bB) = *(bP->bDir.ptr.u32);
	else {
		if (bM & 0x8000)
			*((uint8_t *) bB) = *(bP->bDir.ptr.u8);
		if (bM & 0x4000)
			*(((uint8_t *) bB) + 1) = *(bP->bDir.ptr.u8 + 1);
		if (bM & 0x2000)
			*(((uint8_t *) bB) + 2) = *(bP->bDir.ptr.u8 + 2);
		if (bM & 0x1000)
			*(((uint8_t *) bB) + 3) = *(bP->bDir.ptr.u8 + 3);
	}
	if ((bM & 0x0f00) == 0x0f00)
		*(((uint32_t *) bB) + 1) = *(bP->bDir.ptr.u32 + 1);
	else {
		if (bM & 0x0800)
			*(((uint8_t *) bB) + 4) = *(bP->bDir.ptr.u8 + 4);
		if (bM & 0x0400)
			*(((uint8_t *) bB) + 5) = *(bP->bDir.ptr.u8 + 5);
		if (bM & 0x0200)
			*(((uint8_t *) bB) + 6) = *(bP->bDir.ptr.u8 + 6);
		if (bM & 0x0100)
			*(((uint8_t *) bB) + 7) = *(bP->bDir.ptr.u8 + 7);
	}
	if ((bM & 0x00f0) == 0x00f0)
		*(((uint32_t *) bB) + 2) = *(bP->bDir.ptr.u32 + 2);
	else {
		if (bM & 0x0080)
			*(((uint8_t *) bB) + 8) = *(bP->bDir.ptr.u8 + 8);
		if (bM & 0x0040)
			*(((uint8_t *) bB) + 9) = *(bP->bDir.ptr.u8 + 9);
		if (bM & 0x0020)
			*(((uint8_t *) bB) + 10) = *(bP->bDir.ptr.u8 + 10);
		if (bM & 0x0010)
			*(((uint8_t *) bB) + 11) = *(bP->bDir.ptr.u8 + 11);
	}
	if ((bM & 0x000f) == 0x000f)
		*(((uint32_t *) bB) + 3) = *(bP->bDir.ptr.u32 + 3);
	else {
		if (bM & 0x0008)
			*(((uint8_t *) bB) + 12) = *(bP->bDir.ptr.u8 + 12);
		if (bM & 0x0004)
			*(((uint8_t *) bB) + 13) = *(bP->bDir.ptr.u8 + 13);
		if (bM & 0x0002)
			*(((uint8_t *) bB) + 14) = *(bP->bDir.ptr.u8 + 14);
		if (bM & 0x0001)
			*(((uint8_t *) bB) + 15) = *(bP->bDir.ptr.u8 + 15);
	}
	break;
case BO_AW_4 | BO_AW_2:
	if ((bM & 0xc000) && (bM & 0x3000))
		*((uint32_t *) bB) = *(bP->bDir.ptr.u32);
	else {
		if (bM & 0xc000)
			*((uint16_t *) bB) = *(bP->bDir.ptr.u16);
		if (bM & 0x3000)
			*(((uint16_t *) bB) + 1) = *(bP->bDir.ptr.u16 + 1);
	}
	if ((bM & 0x0c00) && (bM & 0x0300))
		*(((uint32_t *) bB) + 1) = *(bP->bDir.ptr.u32 + 1);
	else {
		if (bM & 0x0c00)
			*(((uint16_t *) bB) + 2) = *(bP->bDir.ptr.u16 + 2);
		if (bM & 0x0300)
			*(((uint16_t *) bB) + 3) = *(bP->bDir.ptr.u16 + 3);
	}
	if ((bM & 0x00c0) && (bM & 0x0030))
		*(((uint32_t *) bB) + 2) = *(bP->bDir.ptr.u32 + 2);
	else {
		if (bM & 0x00c0)
			*(((uint16_t *) bB) + 4) = *(bP->bDir.ptr.u16 + 4);
		if (bM & 0x0030)
			*(((uint16_t *) bB) + 5) = *(bP->bDir.ptr.u16 + 5);
	}
	if ((bM & 0x000c) && (bM & 0x0003))
		*(((uint32_t *) bB) + 3) = *(bP->bDir.ptr.u32 + 3);
	else {
		if (bM & 0x000c)
			*(((uint16_t *) bB) + 6) = *(bP->bDir.ptr.u16 + 6);
		if (bM & 0x0003)
			*(((uint16_t *) bB) + 7) = *(bP->bDir.ptr.u16 + 7);
	}
	break;
case BO_AW_4 | BO_AW_2 | BO_AW_1:
	if ((bM & 0xc000) && (bM & 0x3000))
		*((uint32_t *) bB) = *(bP->bDir.ptr.u32);
	else {
		if ((bM & 0xc000) == 0xc000)
			*((uint16_t *) bB) = *(bP->bDir.ptr.u16);
		else {
			if (bM & 0x8000)
				*((uint8_t *) bB) = *(bP->bDir.ptr.u8);
			if (bM & 0x4000)
				*(((uint8_t *) bB) + 1) =
				    *(bP->bDir.ptr.u8 + 1);
		}
		if ((bM & 0x3000) == 0x3000)
			*(((uint16_t *) bB) + 1) = *(bP->bDir.ptr.u16 + 1);
		else {
			if (bM & 0x2000)
				*(((uint8_t *) bB) + 2) =
				    *(bP->bDir.ptr.u8 + 2);
			if (bM & 0x1000)
				*(((uint8_t *) bB) + 3) =
				    *(bP->bDir.ptr.u8 + 3);
		}
	}
	if ((bM & 0x0c00) && (bM & 0x0300))
		*(((uint32_t *) bB) + 1) = *(bP->bDir.ptr.u32 + 1);
	else {
		if ((bM & 0x0c00) == 0xc00)
			*(((uint16_t *) bB) + 2) = *(bP->bDir.ptr.u16 + 2);
		else {
			if (bM & 0x0800)
				*(((uint8_t *) bB) + 4) =
				    *(bP->bDir.ptr.u8 + 4);
			if (bM & 0x0400)
				*(((uint8_t *) bB) + 5) =
				    *(bP->bDir.ptr.u8 + 5);
		}
		if ((bM & 0x0300) == 0x300)
			*(((uint16_t *) bB) + 3) = *(bP->bDir.ptr.u16 + 3);
		else {
			if (bM & 0x0200)
				*(((uint8_t *) bB) + 6) =
				    *(bP->bDir.ptr.u8 + 6);
			if (bM & 0x0100)
				*(((uint8_t *) bB) + 7) =
				    *(bP->bDir.ptr.u8 + 7);
		}
	}
	if ((bM & 0x00c0) && (bM & 0x0030))
		*(((uint32_t *) bB) + 2) = *(bP->bDir.ptr.u32 + 2);
	else {
		if ((bM & 0x00c0) == 0x00c0)
			*(((uint16_t *) bB) + 4) = *(bP->bDir.ptr.u16 + 4);
		else {
			if (bM & 0x0080)
				*(((uint8_t *) bB) + 8) =
				    *(bP->bDir.ptr.u8 + 8);
			if (bM & 0x0040)
				*(((uint8_t *) bB) + 9) =
				    *(bP->bDir.ptr.u8 + 9);
		}
		if ((bM & 0x0030) == 0x0030)
			*(((uint16_t *) bB) + 5) = *(bP->bDir.ptr.u16 + 5);
		else {
			if (bM & 0x0020)
				*(((uint8_t *) bB) + 10) =
				    *(bP->bDir.ptr.u8 + 10);
			if (bM & 0x0010)
				*(((uint8_t *) bB) + 11) =
				    *(bP->bDir.ptr.u8 + 11);
		}
	}
	if ((bM & 0x000c) && (bM & 0x0003))
		*(((uint32_t *) bB) + 3) = *(bP->bDir.ptr.u32 + 3);
	else {
		if ((bM & 0x000c) == 0x000c)
			*(((uint16_t *) bB) + 6) = *(bP->bDir.ptr.u16 + 6);
		else {
			if (bM & 0x0008)
				*(((uint8_t *) bB) + 12) =
				    *(bP->bDir.ptr.u8 + 12);
			if (bM & 0x0004)
				*(((uint8_t *) bB) + 13) =
				    *(bP->bDir.ptr.u8 + 13);
		}
		if ((bM & 0x0003) == 0x0003)
			*(((uint16_t *) bB) + 7) = *(bP->bDir.ptr.u16 + 7);
		else {
			if (bM & 0x0002)
				*(((uint8_t *) bB) + 14) =
				    *(bP->bDir.ptr.u8 + 14);
			if (bM & 0x0001)
				*(((uint8_t *) bB) + 15) =
				    *(bP->bDir.ptr.u8 + 15);
		}
	}
	break;
case BO_AW_8:
	if (bM & 0xff00)
		*((uint64_t *) bB) = *(bP->bDir.ptr.u64);
	if (bM & 0x00ff)
		*((uint64_t *) bB + 1) = *(bP->bDir.ptr.u64 + 1);
	break;
case BO_AW_8 | BO_AW_1:
	if ((bM & 0xff00) == 0xff00)
		*((uint64_t *) bB) = *(bP->bDir.ptr.u64);
	else {
		if (bM & 0x8000)
			*((uint8_t *) bB) = *(bP->bDir.ptr.u8);
		if (bM & 0x4000)
			*(((uint8_t *) bB) + 1) = *(bP->bDir.ptr.u8 + 1);
		if (bM & 0x2000)
			*(((uint8_t *) bB) + 2) = *(bP->bDir.ptr.u8 + 2);
		if (bM & 0x1000)
			*(((uint8_t *) bB) + 3) = *(bP->bDir.ptr.u8 + 3);
		if (bM & 0x0800)
			*(((uint8_t *) bB) + 4) = *(bP->bDir.ptr.u8 + 4);
		if (bM & 0x0400)
			*(((uint8_t *) bB) + 5) = *(bP->bDir.ptr.u8 + 5);
		if (bM & 0x0200)
			*(((uint8_t *) bB) + 6) = *(bP->bDir.ptr.u8 + 6);
		if (bM & 0x0100)
			*(((uint8_t *) bB) + 7) = *(bP->bDir.ptr.u8 + 7);
	}
	if ((bM & 0x00ff) == 0x00ff)
		*((uint64_t *) bB + 1) = *(bP->bDir.ptr.u64 + 1);
	else {
		if (bM & 0x0080)
			*(((uint8_t *) bB) + 8) = *(bP->bDir.ptr.u8 + 8);
		if (bM & 0x0040)
			*(((uint8_t *) bB) + 9) = *(bP->bDir.ptr.u8 + 9);
		if (bM & 0x0020)
			*(((uint8_t *) bB) + 10) = *(bP->bDir.ptr.u8 + 10);
		if (bM & 0x0010)
			*(((uint8_t *) bB) + 11) = *(bP->bDir.ptr.u8 + 11);
		if (bM & 0x0008)
			*(((uint8_t *) bB) + 12) = *(bP->bDir.ptr.u8 + 12);
		if (bM & 0x0004)
			*(((uint8_t *) bB) + 13) = *(bP->bDir.ptr.u8 + 13);
		if (bM & 0x0002)
			*(((uint8_t *) bB) + 14) = *(bP->bDir.ptr.u8 + 14);
		if (bM & 0x0001)
			*(((uint8_t *) bB) + 15) = *(bP->bDir.ptr.u8 + 15);
	}
	break;
case BO_AW_8 | BO_AW_2:
	if ((bM & 0xff00) == 0xff00)
		*((uint64_t *) bB) = *(bP->bDir.ptr.u64);
	else {
		if (bM & 0xc000)
			*((uint16_t *) bB) = *(bP->bDir.ptr.u16);
		if (bM & 0x3000)
			*(((uint16_t *) bB) + 1) = *(bP->bDir.ptr.u16 + 1);
		if (bM & 0x0c00)
			*(((uint16_t *) bB) + 2) = *(bP->bDir.ptr.u16 + 2);
		if (bM & 0x0300)
			*(((uint16_t *) bB) + 3) = *(bP->bDir.ptr.u16 + 3);
	}
	if ((bM & 0x00ff) == 0x00ff)
		*((uint64_t *) bB + 1) = *(bP->bDir.ptr.u64 + 1);
	else {
		if (bM & 0x00c0)
			*(((uint16_t *) bB) + 4) = *(bP->bDir.ptr.u16 + 4);
		if (bM & 0x0030)
			*(((uint16_t *) bB) + 5) = *(bP->bDir.ptr.u16 + 5);
		if (bM & 0x000c)
			*(((uint16_t *) bB) + 6) = *(bP->bDir.ptr.u16 + 6);
		if (bM & 0x0003)
			*(((uint16_t *) bB) + 7) = *(bP->bDir.ptr.u16 + 7);
	}
	break;
case BO_AW_8 | BO_AW_2 | BO_AW_1:
	if ((bM & 0xff00) == 0xff00)
		*((uint64_t *) bB) = *(bP->bDir.ptr.u64);
	else {
		if ((bM & 0xc000) == 0xc000)
			*((uint16_t *) bB) = *(bP->bDir.ptr.u16);
		else {
			if (bM & 0x8000)
				*((uint8_t *) bB) = *(bP->bDir.ptr.u8);
			if (bM & 0x4000)
				*(((uint8_t *) bB) + 1) =
				    *(bP->bDir.ptr.u8 + 1);
		}
		if ((bM & 0x3000) == 0x3000)
			*(((uint16_t *) bB) + 1) = *(bP->bDir.ptr.u16 + 1);
		else {
			if (bM & 0x2000)
				*(((uint8_t *) bB) + 2) =
				    *(bP->bDir.ptr.u8 + 2);
			if (bM & 0x1000)
				*(((uint8_t *) bB) + 3) =
				    *(bP->bDir.ptr.u8 + 3);
		}
		if ((bM & 0x0c00) == 0x0c00)
			*(((uint16_t *) bB) + 2) = *(bP->bDir.ptr.u16 + 2);
		else {
			if (bM & 0x0800)
				*(((uint8_t *) bB) + 4) =
				    *(bP->bDir.ptr.u8 + 4);
			if (bM & 0x0400)
				*(((uint8_t *) bB) + 5) =
				    *(bP->bDir.ptr.u8 + 5);
		}
		if ((bM & 0x0300) == 0x0300)
			*(((uint16_t *) bB) + 3) = *(bP->bDir.ptr.u16 + 3);
		else {
			if (bM & 0x0200)
				*(((uint8_t *) bB) + 6) =
				    *(bP->bDir.ptr.u8 + 6);
			if (bM & 0x0100)
				*(((uint8_t *) bB) + 7) =
				    *(bP->bDir.ptr.u8 + 7);
		}
	}
	if ((bM & 0x00ff) == 0x00ff)
		*((uint64_t *) bB + 1) = *(bP->bDir.ptr.u64 + 1);
	else {
		if ((bM & 0x00c0) == 0x00c0)
			*(((uint16_t *) bB) + 4) = *(bP->bDir.ptr.u16 + 4);
		else {
			if (bM & 0x0080)
				*(((uint8_t *) bB) + 8) =
				    *(bP->bDir.ptr.u8 + 8);
			if (bM & 0x0040)
				*(((uint8_t *) bB) + 9) =
				    *(bP->bDir.ptr.u8 + 9);
		}
		if ((bM & 0x0030) == 0x0030)
			*(((uint16_t *) bB) + 5) = *(bP->bDir.ptr.u16 + 5);
		else {
			if (bM & 0x0020)
				*(((uint8_t *) bB) + 10) =
				    *(bP->bDir.ptr.u8 + 10);
			if (bM & 0x0010)
				*(((uint8_t *) bB) + 11) =
				    *(bP->bDir.ptr.u8 + 11);
		}
		if ((bM & 0x000c) == 0x000c)
			*(((uint16_t *) bB) + 6) = *(bP->bDir.ptr.u16 + 6);
		else {
			if (bM & 0x0008)
				*(((uint8_t *) bB) + 12) =
				    *(bP->bDir.ptr.u8 + 12);
			if (bM & 0x0004)
				*(((uint8_t *) bB) + 13) =
				    *(bP->bDir.ptr.u8 + 13);
		}
		if ((bM & 0x0003) == 0x0003)
			*(((uint16_t *) bB) + 7) = *(bP->bDir.ptr.u16 + 7);
		else {
			if (bM & 0x0002)
				*(((uint8_t *) bB) + 14) =
				    *(bP->bDir.ptr.u8 + 14);
			if (bM & 0x0001)
				*(((uint8_t *) bB) + 15) =
				    *(bP->bDir.ptr.u8 + 15);
		}
	}
	break;
case BO_AW_8 | BO_AW_4:
	if ((bM & 0xff00) == 0xff00)
		*((uint64_t *) bB) = *(bP->bDir.ptr.u64);
	else {
		if (bM & 0xf000)
			*((uint32_t *) bB) = *(bP->bDir.ptr.u32);
		if (bM & 0x0f00)
			*(((uint32_t *) bB) + 1) = *(bP->bDir.ptr.u32 + 1);
	}
	if ((bM & 0x00ff) == 0x00ff)
		*((uint64_t *) bB + 1) = *(bP->bDir.ptr.u64 + 1);
	else {
		if (bM & 0x00f0)
			*(((uint32_t *) bB) + 2) = *(bP->bDir.ptr.u32 + 2);
		if (bM & 0x000f)
			*(((uint32_t *) bB) + 3) = *(bP->bDir.ptr.u32 + 3);
	}
	break;
case BO_AW_8 | BO_AW_4 | BO_AW_1:
	if ((bM & 0xff00) == 0xff00)
		*((uint64_t *) bB) = *(bP->bDir.ptr.u64);
	else {
		if ((bM & 0xf000) == 0xf000)
			*((uint32_t *) bB) = *(bP->bDir.ptr.u32);
		else {
			if (bM & 0x8000)
				*((uint8_t *) bB) = *(bP->bDir.ptr.u8);
			if (bM & 0x4000)
				*(((uint8_t *) bB) + 1) =
				    *(bP->bDir.ptr.u8 + 1);
			if (bM & 0x2000)
				*(((uint8_t *) bB) + 2) =
				    *(bP->bDir.ptr.u8 + 2);
			if (bM & 0x1000)
				*(((uint8_t *) bB) + 3) =
				    *(bP->bDir.ptr.u8 + 3);
		}
		if ((bM & 0x0f00) == 0x0f00)
			*(((uint32_t *) bB) + 1) = *(bP->bDir.ptr.u32 + 1);
		else {
			if (bM & 0x0800)
				*(((uint8_t *) bB) + 4) =
				    *(bP->bDir.ptr.u8 + 4);
			if (bM & 0x0400)
				*(((uint8_t *) bB) + 5) =
				    *(bP->bDir.ptr.u8 + 5);
			if (bM & 0x0200)
				*(((uint8_t *) bB) + 6) =
				    *(bP->bDir.ptr.u8 + 6);
			if (bM & 0x0100)
				*(((uint8_t *) bB) + 7) =
				    *(bP->bDir.ptr.u8 + 7);
		}
	}
	if ((bM & 0x00ff) == 0x00ff)
		*((uint64_t *) bB + 1) = *(bP->bDir.ptr.u64 + 1);
	else {
		if ((bM & 0x00f0) == 0x00f0)
			*(((uint32_t *) bB) + 2) = *(bP->bDir.ptr.u32 + 2);
		else {
			if (bM & 0x0080)
				*(((uint8_t *) bB) + 8) =
				    *(bP->bDir.ptr.u8 + 8);
			if (bM & 0x0040)
				*(((uint8_t *) bB) + 9) =
				    *(bP->bDir.ptr.u8 + 9);
			if (bM & 0x0020)
				*(((uint8_t *) bB) + 10) =
				    *(bP->bDir.ptr.u8 + 10);
			if (bM & 0x0010)
				*(((uint8_t *) bB) + 11) =
				    *(bP->bDir.ptr.u8 + 11);
		}
		if ((bM & 0x000f) == 0x000f)
			*(((uint32_t *) bB) + 3) = *(bP->bDir.ptr.u32 + 3);
		else {
			if (bM & 0x0008)
				*(((uint8_t *) bB) + 12) =
				    *(bP->bDir.ptr.u8 + 12);
			if (bM & 0x0004)
				*(((uint8_t *) bB) + 13) =
				    *(bP->bDir.ptr.u8 + 13);
			if (bM & 0x0002)
				*(((uint8_t *) bB) + 14) =
				    *(bP->bDir.ptr.u8 + 14);
			if (bM & 0x0001)
				*(((uint8_t *) bB) + 15) =
				    *(bP->bDir.ptr.u8 + 15);
		}
	}
	break;
case BO_AW_8 | BO_AW_4 | BO_AW_2:
	if ((bM & 0xff00) == 0xff00)
		*((uint64_t *) bB) = *(bP->bDir.ptr.u64);
	else {
		if ((bM & 0xc000) && (bM & 0x3000))
			*((uint32_t *) bB) = *(bP->bDir.ptr.u32);
		else {
			if (bM & 0xc000)
				*((uint16_t *) bB) = *(bP->bDir.ptr.u16);
			if (bM & 0x3000)
				*(((uint16_t *) bB) + 1) =
				    *(bP->bDir.ptr.u16 + 1);
		}
		if ((bM & 0x0c00) && (bM & 0x0300))
			*(((uint32_t *) bB) + 1) = *(bP->bDir.ptr.u32 + 1);
		else {
			if (bM & 0x0c00)
				*(((uint16_t *) bB) + 2) =
				    *(bP->bDir.ptr.u16 + 2);
			if (bM & 0x0300)
				*(((uint16_t *) bB) + 3) =
				    *(bP->bDir.ptr.u16 + 3);
		}
	}
	if ((bM & 0x00ff) == 0x00ff)
		*((uint64_t *) bB + 1) = *(bP->bDir.ptr.u64 + 1);
	else {
		if ((bM & 0x00c0) && (bM & 0x0030))
			*(((uint32_t *) bB) + 2) = *(bP->bDir.ptr.u32 + 2);
		else {
			if (bM & 0x00c0)
				*(((uint16_t *) bB) + 4) =
				    *(bP->bDir.ptr.u16 + 4);
			if (bM & 0x0030)
				*(((uint16_t *) bB) + 5) =
				    *(bP->bDir.ptr.u16 + 5);
		}
		if ((bM & 0x000c) && (bM & 0x0003))
			*(((uint32_t *) bB) + 3) = *(bP->bDir.ptr.u32 + 3);
		else {
			if (bM & 0x000c)
				*(((uint16_t *) bB) + 6) =
				    *(bP->bDir.ptr.u16 + 6);
			if (bM & 0x0003)
				*(((uint16_t *) bB) + 7) =
				    *(bP->bDir.ptr.u16 + 7);
		}
	}
	break;
case BO_AW_8 | BO_AW_4 | BO_AW_2 | BO_AW_1:
	if ((bM & 0xff00) == 0xff00)
		*((uint64_t *) bB) = *(bP->bDir.ptr.u64);
	else {
		if ((bM & 0xc000) && (bM & 0x3000))
			*((uint32_t *) bB) = *(bP->bDir.ptr.u32);
		else {
			if ((bM & 0xc000) == 0xc000)
				*((uint16_t *) bB) = *(bP->bDir.ptr.u16);
			else {
				if (bM & 0x8000)
					*((uint8_t *) bB) =
					    *(bP->bDir.ptr.u8);
				if (bM & 0x4000)
					*(((uint8_t *) bB) + 1) =
					    *(bP->bDir.ptr.u8 + 1);
			}
			if ((bM & 0x3000) == 0x3000)
				*(((uint16_t *) bB) + 1) =
				    *(bP->bDir.ptr.u16 + 1);
			else {
				if (bM & 0x2000)
					*(((uint8_t *) bB) + 2) =
					    *(bP->bDir.ptr.u8 + 2);
				if (bM & 0x1000)
					*(((uint8_t *) bB) + 3) =
					    *(bP->bDir.ptr.u8 + 3);
			}
		}
		if ((bM & 0x0c00) && (bM & 0x0300))
			*(((uint32_t *) bB) + 1) = *(bP->bDir.ptr.u32 + 1);
		else {
			if ((bM & 0x0c00) == 0xc00)
				*(((uint16_t *) bB) + 2) =
				    *(bP->bDir.ptr.u16 + 2);
			else {
				if (bM & 0x0800)
					*(((uint8_t *) bB) + 4) =
					    *(bP->bDir.ptr.u8 + 4);
				if (bM & 0x0400)
					*(((uint8_t *) bB) + 5) =
					    *(bP->bDir.ptr.u8 + 5);
			}
			if ((bM & 0x0300) == 0x300)
				*(((uint16_t *) bB) + 3) =
				    *(bP->bDir.ptr.u16 + 3);
			else {
				if (bM & 0x0200)
					*(((uint8_t *) bB) + 6) =
					    *(bP->bDir.ptr.u8 + 6);
				if (bM & 0x0100)
					*(((uint8_t *) bB) + 7) =
					    *(bP->bDir.ptr.u8 + 7);
			}
		}
	}
	if ((bM & 0x00ff) == 0x00ff)
		*((uint64_t *) bB + 1) = *(bP->bDir.ptr.u64 + 1);
	else {
		if ((bM & 0x00c0) && (bM & 0x0030))
			*(((uint32_t *) bB) + 2) = *(bP->bDir.ptr.u32 + 2);
		else {
			if ((bM & 0x00c0) == 0x00c0)
				*(((uint16_t *) bB) + 4) =
				    *(bP->bDir.ptr.u16 + 4);
			else {
				if (bM & 0x0080)
					*(((uint8_t *) bB) + 8) =
					    *(bP->bDir.ptr.u8 + 8);
				if (bM & 0x0040)
					*(((uint8_t *) bB) + 9) =
					    *(bP->bDir.ptr.u8 + 9);
			}
			if ((bM & 0x0030) == 0x0030)
				*(((uint16_t *) bB) + 5) =
				    *(bP->bDir.ptr.u16 + 5);
			else {
				if (bM & 0x0020)
					*(((uint8_t *) bB) + 10) =
					    *(bP->bDir.ptr.u8 + 10);
				if (bM & 0x0010)
					*(((uint8_t *) bB) + 11) =
					    *(bP->bDir.ptr.u8 + 11);
			}
		}
		if ((bM & 0x000c) && (bM & 0x0003))
			*(((uint32_t *) bB) + 3) = *(bP->bDir.ptr.u32 + 3);
		else {
			if ((bM & 0x000c) == 0x000c)
				*(((uint16_t *) bB) + 6) =
				    *(bP->bDir.ptr.u16 + 6);
			else {
				if (bM & 0x0008)
					*(((uint8_t *) bB) + 12) =
					    *(bP->bDir.ptr.u8 + 12);
				if (bM & 0x0004)
					*(((uint8_t *) bB) + 13) =
					    *(bP->bDir.ptr.u8 + 13);
			}
			if ((bM & 0x0003) == 0x0003)
				*(((uint16_t *) bB) + 7) =
				    *(bP->bDir.ptr.u16 + 7);
			else {
				if (bM & 0x0002)
					*(((uint8_t *) bB) + 14) =
					    *(bP->bDir.ptr.u8 + 14);
				if (bM & 0x0001)
					*(((uint8_t *) bB) + 15) =
					    *(bP->bDir.ptr.u8 + 15);
			}
		}
	}
	break;
}
