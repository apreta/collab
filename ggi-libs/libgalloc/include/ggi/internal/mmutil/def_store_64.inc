



/*
 * This is the fallback method by which data is written to a batchparm. It is
 * inefficient, because it handles all the bells and whistles. If you don't
 * have non-byte-aligned steps/widths and access width restrictions, don't
 * use it!  There are/will be/should be more optimized versions for those
 * situations.
 * 
 * This version is intended for machines with all access operations from 8 to 64
 * bits.
 * 
 * You should supply the following as macros:
 * 
 * bB is a pointer to a 16-byte buffer, aligned on a 64-bit boundary, containing
 * the data to store, which must be prechewed. bP is a pointer to the
 * batchparm data is being stored to, and must be of type (batchparm *) bM
 * should be an unsigned integer at least 16-bit.  It will be assigned, in
 * the least signifigant 16 bits, a mask which tells which bytes in the area
 * pointed to by bB contain batchparm data.
 */

bM = (0xffff << (16 - (bP->out.boff + bP->datasize + 7) / 8))
    & (0xffff >> (bP->out.boff / 8));
if (bP->access & BO_EB_RE_32)
	bM = (((bM << 4) & 0xf0f0) | ((bM >> 4) & 0x0f0f));
if (bP->access & BO_EB_RE_16)
	bM = (((bM << 2) & 0xcccc) | ((bM >> 2) & 0x3333));
if (bP->access & BO_EB_RE_8)
	bM = (((bM << 1) & 0xaaaa) | ((bM >> 1) & 0x5555));

switch (bP->access & BO_AW_MASK) {
case BO_AW_ANY:
	do {
		/* Way to do this better? */
		size_t i, s, e;
		s = i = 0;
		while (!((bM << i++) & 0x8000))
			s++;
		e = 16;
		i = 0;
		while (!((bM >> i++) & 0x0001))
			e--;
		memcpy(bP->out.ptr.u8 + s, ((uint8_t *) bB + s), e - s);
	} while (0);
	break;
case BO_AW_1:
	if (bM & 0x8000)
		*(bP->out.ptr.u8) = *((uint8_t *) bB);
	if (bM & 0x4000)
		*(bP->out.ptr.u8 + 1) = *(((uint8_t *) bB) + 1);
	if (bM & 0x2000)
		*(bP->out.ptr.u8 + 2) = *(((uint8_t *) bB) + 2);
	if (bM & 0x1000)
		*(bP->out.ptr.u8 + 3) = *(((uint8_t *) bB) + 3);
	if (bM & 0x0800)
		*(bP->out.ptr.u8 + 4) = *(((uint8_t *) bB) + 4);
	if (bM & 0x0400)
		*(bP->out.ptr.u8 + 5) = *(((uint8_t *) bB) + 5);
	if (bM & 0x0200)
		*(bP->out.ptr.u8 + 6) = *(((uint8_t *) bB) + 6);
	if (bM & 0x0100)
		*(bP->out.ptr.u8 + 7) = *(((uint8_t *) bB) + 7);
	if (bM & 0x0080)
		*(bP->out.ptr.u8 + 8) = *(((uint8_t *) bB) + 8);
	if (bM & 0x0040)
		*(bP->out.ptr.u8 + 9) = *(((uint8_t *) bB) + 9);
	if (bM & 0x0020)
		*(bP->out.ptr.u8 + 10) = *(((uint8_t *) bB) + 10);
	if (bM & 0x0010)
		*(bP->out.ptr.u8 + 11) = *(((uint8_t *) bB) + 11);
	if (bM & 0x0008)
		*(bP->out.ptr.u8 + 12) = *(((uint8_t *) bB) + 12);
	if (bM & 0x0004)
		*(bP->out.ptr.u8 + 13) = *(((uint8_t *) bB) + 13);
	if (bM & 0x0002)
		*(bP->out.ptr.u8 + 14) = *(((uint8_t *) bB) + 14);
	if (bM & 0x0001)
		*(bP->out.ptr.u8 + 15) = *(((uint8_t *) bB) + 15);
	break;
case BO_AW_2:
	if (bM & 0xc000)
		*(bP->out.ptr.u16) = *((uint16_t *) bB);
	if (bM & 0x3000)
		*(bP->out.ptr.u16 + 1) = *(((uint16_t *) bB) + 1);
	if (bM & 0x0c00)
		*(bP->out.ptr.u16 + 2) = *(((uint16_t *) bB) + 2);
	if (bM & 0x0300)
		*(bP->out.ptr.u16 + 3) = *(((uint16_t *) bB) + 3);
	if (bM & 0x00c0)
		*(bP->out.ptr.u16 + 4) = *(((uint16_t *) bB) + 4);
	if (bM & 0x0030)
		*(bP->out.ptr.u16 + 5) = *(((uint16_t *) bB) + 5);
	if (bM & 0x000c)
		*(bP->out.ptr.u16 + 6) = *(((uint16_t *) bB) + 6);
	if (bM & 0x0003)
		*(bP->out.ptr.u16 + 7) = *(((uint16_t *) bB) + 7);
	break;
case BO_AW_2 | BO_AW_1:
	if ((bM & 0xc000) == 0xc000)
		*(bP->out.ptr.u16) = *((uint16_t *) bB);
	else {
		if (bM & 0x8000)
			*(bP->out.ptr.u8) = *((uint8_t *) bB);
		if (bM & 0x4000)
			*(bP->out.ptr.u8 + 1) = *(((uint8_t *) bB) + 1);
	}
	if ((bM & 0x3000) == 0x3000)
		*(bP->out.ptr.u16 + 1) = *(((uint16_t *) bB) + 1);
	else {
		if (bM & 0x2000)
			*(bP->out.ptr.u8 + 2) = *(((uint8_t *) bB) + 2);
		if (bM & 0x1000)
			*(bP->out.ptr.u8 + 3) = *(((uint8_t *) bB) + 3);
	}
	if ((bM & 0x0c00) == 0x0c00) {
		*(bP->out.ptr.u16 + 2) = *(((uint16_t *) bB) + 2);
	} else {
		if (bM & 0x0800)
			*(bP->out.ptr.u8 + 4) = *(((uint8_t *) bB) + 4);
		if (bM & 0x0400)
			*(bP->out.ptr.u8 + 5) = *(((uint8_t *) bB) + 5);
	}
	if ((bM & 0x0300) == 0x0300)
		*(bP->out.ptr.u16 + 3) = *(((uint16_t *) bB) + 3);
	else {
		if (bM & 0x0200)
			*(bP->out.ptr.u8 + 6) = *(((uint8_t *) bB) + 6);
		if (bM & 0x0100)
			*(bP->out.ptr.u8 + 7) = *(((uint8_t *) bB) + 7);
	}
	if ((bM & 0x00c0) == 0x00c0)
		*(bP->out.ptr.u16 + 4) = *(((uint16_t *) bB) + 4);
	else {
		if (bM & 0x0080)
			*(bP->out.ptr.u8 + 8) = *(((uint8_t *) bB) + 8);
		if (bM & 0x0040)
			*(bP->out.ptr.u8 + 9) = *(((uint8_t *) bB) + 9);
	}
	if ((bM & 0x0030) == 0x0030)
		*(bP->out.ptr.u16 + 5) = *(((uint16_t *) bB) + 5);
	else {
		if (bM & 0x0020)
			*(bP->out.ptr.u8 + 10) = *(((uint8_t *) bB) + 10);
		if (bM & 0x0010)
			*(bP->out.ptr.u8 + 11) = *(((uint8_t *) bB) + 11);
	}
	if ((bM & 0x000c) == 0x000c)
		*(bP->out.ptr.u16 + 6) = *(((uint16_t *) bB) + 6);
	else {
		if (bM & 0x0008)
			*(bP->out.ptr.u8 + 12) = *(((uint8_t *) bB) + 12);
		if (bM & 0x0004)
			*(bP->out.ptr.u8 + 13) = *(((uint8_t *) bB) + 13);
	}
	if ((bM & 0x0003) == 0x0003)
		*(bP->out.ptr.u16 + 7) = *(((uint16_t *) bB) + 7);
	else {
		if (bM & 0x0002)
			*(bP->out.ptr.u8 + 14) = *(((uint8_t *) bB) + 14);
		if (bM & 0x0001)
			*(bP->out.ptr.u8 + 15) = *(((uint8_t *) bB) + 15);
	}
	break;
case BO_AW_4:
	if (bM & 0xf000)
		*(bP->out.ptr.u32) = *((uint32_t *) bB);
	if (bM & 0x0f00)
		*(bP->out.ptr.u32 + 1) = *(((uint32_t *) bB) + 1);
	if (bM & 0x00f0)
		*(bP->out.ptr.u32 + 2) = *(((uint32_t *) bB) + 2);
	if (bM & 0x000f)
		*(bP->out.ptr.u32 + 3) = *(((uint32_t *) bB) + 3);
	break;
case BO_AW_4 | BO_AW_1:
	if ((bM & 0xf000) == 0xf000)
		*(bP->out.ptr.u32) = *((uint32_t *) bB);
	else {
		if (bM & 0x8000)
			*(bP->out.ptr.u8) = *((uint8_t *) bB);
		if (bM & 0x4000)
			*(bP->out.ptr.u8 + 1) = *(((uint8_t *) bB) + 1);
		if (bM & 0x2000)
			*(bP->out.ptr.u8 + 2) = *(((uint8_t *) bB) + 2);
		if (bM & 0x1000)
			*(bP->out.ptr.u8 + 3) = *(((uint8_t *) bB) + 3);
	}
	if ((bM & 0x0f00) == 0x0f00)
		*(bP->out.ptr.u32 + 1) = *(((uint32_t *) bB) + 1);
	else {
		if (bM & 0x0800)
			*(bP->out.ptr.u8 + 4) = *(((uint8_t *) bB) + 4);
		if (bM & 0x0400)
			*(bP->out.ptr.u8 + 5) = *(((uint8_t *) bB) + 5);
		if (bM & 0x0200)
			*(bP->out.ptr.u8 + 6) = *(((uint8_t *) bB) + 6);
		if (bM & 0x0100)
			*(bP->out.ptr.u8 + 7) = *(((uint8_t *) bB) + 7);
	}
	if ((bM & 0x00f0) == 0x00f0)
		*(bP->out.ptr.u32 + 2) = *(((uint32_t *) bB) + 2);
	else {
		if (bM & 0x0080)
			*(bP->out.ptr.u8 + 8) = *(((uint8_t *) bB) + 8);
		if (bM & 0x0040)
			*(bP->out.ptr.u8 + 9) = *(((uint8_t *) bB) + 9);
		if (bM & 0x0020)
			*(bP->out.ptr.u8 + 10) = *(((uint8_t *) bB) + 10);
		if (bM & 0x0010)
			*(bP->out.ptr.u8 + 11) = *(((uint8_t *) bB) + 11);
	}
	if ((bM & 0x000f) == 0x000f)
		*(bP->out.ptr.u32 + 3) = *(((uint32_t *) bB) + 3);
	else {
		if (bM & 0x0008)
			*(bP->out.ptr.u8 + 12) = *(((uint8_t *) bB) + 12);
		if (bM & 0x0004)
			*(bP->out.ptr.u8 + 13) = *(((uint8_t *) bB) + 13);
		if (bM & 0x0002)
			*(bP->out.ptr.u8 + 14) = *(((uint8_t *) bB) + 14);
		if (bM & 0x0001)
			*(bP->out.ptr.u8 + 15) = *(((uint8_t *) bB) + 15);
	}
	break;
case BO_AW_4 | BO_AW_2:
	if ((bM & 0xc000) && (bM & 0x3000))
		*(bP->out.ptr.u32) = *((uint32_t *) bB);
	else {
		if (bM & 0xc000)
			*(bP->out.ptr.u16) = *((uint16_t *) bB);
		if (bM & 0x3000)
			*(bP->out.ptr.u16 + 1) = *(((uint16_t *) bB) + 1);
	}
	if ((bM & 0x0c00) && (bM & 0x0300))
		*(bP->out.ptr.u32 + 1) = *(((uint32_t *) bB) + 1);
	else {
		if (bM & 0x0c00)
			*(bP->out.ptr.u16 + 2) = *(((uint16_t *) bB) + 2);
		if (bM & 0x0300)
			*(bP->out.ptr.u16 + 3) = *(((uint16_t *) bB) + 3);
	}
	if ((bM & 0x00c0) && (bM & 0x0030))
		*(bP->out.ptr.u32 + 2) = *(((uint32_t *) bB) + 2);
	else {
		if (bM & 0x00c0)
			*(bP->out.ptr.u16 + 4) = *(((uint16_t *) bB) + 4);
		if (bM & 0x0030)
			*(bP->out.ptr.u16 + 5) = *(((uint16_t *) bB) + 5);
	}
	if ((bM & 0x000c) && (bM & 0x0003))
		*(bP->out.ptr.u32 + 3) = *(((uint32_t *) bB) + 3);
	else {
		if (bM & 0x000c)
			*(bP->out.ptr.u16 + 6) = *(((uint16_t *) bB) + 6);
		if (bM & 0x0003)
			*(bP->out.ptr.u16 + 7) = *(((uint16_t *) bB) + 7);
	}
	break;
case BO_AW_4 | BO_AW_2 | BO_AW_1:
	if ((bM & 0xc000) && (bM & 0x3000))
		*(bP->out.ptr.u32) = *((uint32_t *) bB);
	else {
		if ((bM & 0xc000) == 0xc000)
			*(bP->out.ptr.u16) = *((uint16_t *) bB);
		else {
			if (bM & 0x8000)
				*(bP->out.ptr.u8) = *((uint8_t *) bB);
			if (bM & 0x4000)
				*(bP->out.ptr.u8 + 1) =
				    *(((uint8_t *) bB) + 1);
		}
		if ((bM & 0x3000) == 0x3000)
			*(bP->out.ptr.u16 + 1) = *(((uint16_t *) bB) + 1);
		else {
			if (bM & 0x2000)
				*(bP->out.ptr.u8 + 2) =
				    *(((uint8_t *) bB) + 2);
			if (bM & 0x1000)
				*(bP->out.ptr.u8 + 3) =
				    *(((uint8_t *) bB) + 3);
		}
	}
	if ((bM & 0x0c00) && (bM & 0x0300))
		*(bP->out.ptr.u32 + 1) = *(((uint32_t *) bB) + 1);
	else {
		if ((bM & 0x0c00) == 0xc00)
			*(bP->out.ptr.u16 + 2) = *(((uint16_t *) bB) + 2);
		else {
			if (bM & 0x0800)
				*(bP->out.ptr.u8 + 4) =
				    *(((uint8_t *) bB) + 4);
			if (bM & 0x0400)
				*(bP->out.ptr.u8 + 5) =
				    *(((uint8_t *) bB) + 5);
		}
		if ((bM & 0x0300) == 0x300)
			*(bP->out.ptr.u16 + 3) = *(((uint16_t *) bB) + 3);
		else {
			if (bM & 0x0200)
				*(bP->out.ptr.u8 + 6) =
				    *(((uint8_t *) bB) + 6);
			if (bM & 0x0100)
				*(bP->out.ptr.u8 + 7) =
				    *(((uint8_t *) bB) + 7);
		}
	}
	if ((bM & 0x00c0) && (bM & 0x0030))
		*(bP->out.ptr.u32 + 2) = *(((uint32_t *) bB) + 2);
	else {
		if ((bM & 0x00c0) == 0x00c0)
			*(bP->out.ptr.u16 + 4) = *(((uint16_t *) bB) + 4);
		else {
			if (bM & 0x0080)
				*(bP->out.ptr.u8 + 8) =
				    *(((uint8_t *) bB) + 8);
			if (bM & 0x0040)
				*(bP->out.ptr.u8 + 9) =
				    *(((uint8_t *) bB) + 9);
		}
		if ((bM & 0x0030) == 0x0030)
			*(bP->out.ptr.u16 + 5) = *(((uint16_t *) bB) + 5);
		else {
			if (bM & 0x0020)
				*(bP->out.ptr.u8 + 10) =
				    *(((uint8_t *) bB) + 10);
			if (bM & 0x0010)
				*(bP->out.ptr.u8 + 11) =
				    *(((uint8_t *) bB) + 11);
		}
	}
	if ((bM & 0x000c) && (bM & 0x0003))
		*(bP->out.ptr.u32 + 3) = *(((uint32_t *) bB) + 3);
	else {
		if ((bM & 0x000c) == 0x000c)
			*(bP->out.ptr.u16 + 6) = *(((uint16_t *) bB) + 6);
		else {
			if (bM & 0x0008)
				*(bP->out.ptr.u8 + 12) =
				    *(((uint8_t *) bB) + 12);
			if (bM & 0x0004)
				*(bP->out.ptr.u8 + 13) =
				    *(((uint8_t *) bB) + 13);
		}
		if ((bM & 0x0003) == 0x0003)
			*(bP->out.ptr.u16 + 7) = *(((uint16_t *) bB) + 7);
		else {
			if (bM & 0x0002)
				*(bP->out.ptr.u8 + 14) =
				    *(((uint8_t *) bB) + 14);
			if (bM & 0x0001)
				*(bP->out.ptr.u8 + 15) =
				    *(((uint8_t *) bB) + 15);
		}
	}
	break;
case BO_AW_8:
	if (bM & 0xff00)
		*(bP->out.ptr.u64) = *((uint64_t *) bB);
	if (bM & 0x00ff)
		*(bP->out.ptr.u64 + 1) = *((uint64_t *) bB + 1);
	break;
case BO_AW_8 | BO_AW_1:
	if ((bM & 0xff00) == 0xff00)
		*(bP->out.ptr.u64) = *((uint64_t *) bB);
	else {
		if (bM & 0x8000)
			*(bP->out.ptr.u8) = *((uint8_t *) bB);
		if (bM & 0x4000)
			*(bP->out.ptr.u8 + 1) = *(((uint8_t *) bB) + 1);
		if (bM & 0x2000)
			*(bP->out.ptr.u8 + 2) = *(((uint8_t *) bB) + 2);
		if (bM & 0x1000)
			*(bP->out.ptr.u8 + 3) = *(((uint8_t *) bB) + 3);
		if (bM & 0x0800)
			*(bP->out.ptr.u8 + 4) = *(((uint8_t *) bB) + 4);
		if (bM & 0x0400)
			*(bP->out.ptr.u8 + 5) = *(((uint8_t *) bB) + 5);
		if (bM & 0x0200)
			*(bP->out.ptr.u8 + 6) = *(((uint8_t *) bB) + 6);
		if (bM & 0x0100)
			*(bP->out.ptr.u8 + 7) = *(((uint8_t *) bB) + 7);
	}
	if ((bM & 0x00ff) == 0x00ff)
		*(bP->out.ptr.u64 + 1) = *((uint64_t *) bB + 1);
	else {
		if (bM & 0x0080)
			*(bP->out.ptr.u8 + 8) = *(((uint8_t *) bB) + 8);
		if (bM & 0x0040)
			*(bP->out.ptr.u8 + 9) = *(((uint8_t *) bB) + 9);
		if (bM & 0x0020)
			*(bP->out.ptr.u8 + 10) = *(((uint8_t *) bB) + 10);
		if (bM & 0x0010)
			*(bP->out.ptr.u8 + 11) = *(((uint8_t *) bB) + 11);
		if (bM & 0x0008)
			*(bP->out.ptr.u8 + 12) = *(((uint8_t *) bB) + 12);
		if (bM & 0x0004)
			*(bP->out.ptr.u8 + 13) = *(((uint8_t *) bB) + 13);
		if (bM & 0x0002)
			*(bP->out.ptr.u8 + 14) = *(((uint8_t *) bB) + 14);
		if (bM & 0x0001)
			*(bP->out.ptr.u8 + 15) = *(((uint8_t *) bB) + 15);
	}
	break;
case BO_AW_8 | BO_AW_2:
	if ((bM & 0xff00) == 0xff00)
		*(bP->out.ptr.u64) = *((uint64_t *) bB);
	else {
		if (bM & 0xc000)
			*(bP->out.ptr.u16) = *((uint16_t *) bB);
		if (bM & 0x3000)
			*(bP->out.ptr.u16 + 1) = *(((uint16_t *) bB) + 1);
		if (bM & 0x0c00)
			*(bP->out.ptr.u16 + 2) = *(((uint16_t *) bB) + 2);
		if (bM & 0x0300)
			*(bP->out.ptr.u16 + 3) = *(((uint16_t *) bB) + 3);
	}
	if ((bM & 0x00ff) == 0x00ff)
		*(bP->out.ptr.u64 + 1) = *((uint64_t *) bB + 1);
	else {
		if (bM & 0x00c0)
			*(bP->out.ptr.u16 + 4) = *(((uint16_t *) bB) + 4);
		if (bM & 0x0030)
			*(bP->out.ptr.u16 + 5) = *(((uint16_t *) bB) + 5);
		if (bM & 0x000c)
			*(bP->out.ptr.u16 + 6) = *(((uint16_t *) bB) + 6);
		if (bM & 0x0003)
			*(bP->out.ptr.u16 + 7) = *(((uint16_t *) bB) + 7);
	}
	break;
case BO_AW_8 | BO_AW_2 | BO_AW_1:
	if ((bM & 0xff00) == 0xff00)
		*(bP->out.ptr.u64) = *((uint64_t *) bB);
	else {
		if ((bM & 0xc000) == 0xc000)
			*(bP->out.ptr.u16) = *((uint16_t *) bB);
		else {
			if (bM & 0x8000)
				*(bP->out.ptr.u8) = *((uint8_t *) bB);
			if (bM & 0x4000)
				*(bP->out.ptr.u8 + 1) =
				    *(((uint8_t *) bB) + 1);
		}
		if ((bM & 0x3000) == 0x3000)
			*(bP->out.ptr.u16 + 1) = *(((uint16_t *) bB) + 1);
		else {
			if (bM & 0x2000)
				*(bP->out.ptr.u8 + 2) =
				    *(((uint8_t *) bB) + 2);
			if (bM & 0x1000)
				*(bP->out.ptr.u8 + 3) =
				    *(((uint8_t *) bB) + 3);
		}
		if ((bM & 0x0c00) == 0x0c00)
			*(bP->out.ptr.u16 + 2) = *(((uint16_t *) bB) + 2);
		else {
			if (bM & 0x0800)
				*(bP->out.ptr.u8 + 4) =
				    *(((uint8_t *) bB) + 4);
			if (bM & 0x0400)
				*(bP->out.ptr.u8 + 5) =
				    *(((uint8_t *) bB) + 5);
		}
		if ((bM & 0x0300) == 0x0300)
			*(bP->out.ptr.u16 + 3) = *(((uint16_t *) bB) + 3);
		else {
			if (bM & 0x0200)
				*(bP->out.ptr.u8 + 6) =
				    *(((uint8_t *) bB) + 6);
			if (bM & 0x0100)
				*(bP->out.ptr.u8 + 7) =
				    *(((uint8_t *) bB) + 7);
		}
	}
	if ((bM & 0x00ff) == 0x00ff)
		*(bP->out.ptr.u64 + 1) = *((uint64_t *) bB + 1);
	else {
		if ((bM & 0x00c0) == 0x00c0)
			*(bP->out.ptr.u16 + 4) = *(((uint16_t *) bB) + 4);
		else {
			if (bM & 0x0080)
				*(bP->out.ptr.u8 + 8) =
				    *(((uint8_t *) bB) + 8);
			if (bM & 0x0040)
				*(bP->out.ptr.u8 + 9) =
				    *(((uint8_t *) bB) + 9);
		}
		if ((bM & 0x0030) == 0x0030)
			*(bP->out.ptr.u16 + 5) = *(((uint16_t *) bB) + 5);
		else {
			if (bM & 0x0020)
				*(bP->out.ptr.u8 + 10) =
				    *(((uint8_t *) bB) + 10);
			if (bM & 0x0010)
				*(bP->out.ptr.u8 + 11) =
				    *(((uint8_t *) bB) + 11);
		}
		if ((bM & 0x000c) == 0x000c)
			*(bP->out.ptr.u16 + 6) = *(((uint16_t *) bB) + 6);
		else {
			if (bM & 0x0008)
				*(bP->out.ptr.u8 + 12) =
				    *(((uint8_t *) bB) + 12);
			if (bM & 0x0004)
				*(bP->out.ptr.u8 + 13) =
				    *(((uint8_t *) bB) + 13);
		}
		if ((bM & 0x0003) == 0x0003)
			*(bP->out.ptr.u16 + 7) = *(((uint16_t *) bB) + 7);
		else {
			if (bM & 0x0002)
				*(bP->out.ptr.u8 + 14) =
				    *(((uint8_t *) bB) + 14);
			if (bM & 0x0001)
				*(bP->out.ptr.u8 + 15) =
				    *(((uint8_t *) bB) + 15);
		}
	}
	break;
case BO_AW_8 | BO_AW_4:
	if ((bM & 0xff00) == 0xff00)
		*(bP->out.ptr.u64) = *((uint64_t *) bB);
	else {
		if (bM & 0xf000)
			*(bP->out.ptr.u32) = *((uint32_t *) bB);
		if (bM & 0x0f00)
			*(bP->out.ptr.u32 + 1) = *(((uint32_t *) bB) + 1);
	}
	if ((bM & 0x00ff) == 0x00ff)
		*(bP->out.ptr.u64 + 1) = *((uint64_t *) bB + 1);
	else {
		if (bM & 0x00f0)
			*(bP->out.ptr.u32 + 2) = *(((uint32_t *) bB) + 2);
		if (bM & 0x000f)
			*(bP->out.ptr.u32 + 3) = *(((uint32_t *) bB) + 3);
	}
	break;
case BO_AW_8 | BO_AW_4 | BO_AW_1:
	if ((bM & 0xff00) == 0xff00)
		*(bP->out.ptr.u64) = *((uint64_t *) bB);
	else {
		if ((bM & 0xf000) == 0xf000)
			*(bP->out.ptr.u32) = *((uint32_t *) bB);
		else {
			if (bM & 0x8000)
				*(bP->out.ptr.u8) = *((uint8_t *) bB);
			if (bM & 0x4000)
				*(bP->out.ptr.u8 + 1) =
				    *(((uint8_t *) bB) + 1);
			if (bM & 0x2000)
				*(bP->out.ptr.u8 + 2) =
				    *(((uint8_t *) bB) + 2);
			if (bM & 0x1000)
				*(bP->out.ptr.u8 + 3) =
				    *(((uint8_t *) bB) + 3);
		}
		if ((bM & 0x0f00) == 0x0f00)
			*(bP->out.ptr.u32 + 1) = *(((uint32_t *) bB) + 1);
		else {
			if (bM & 0x0800)
				*(bP->out.ptr.u8 + 4) =
				    *(((uint8_t *) bB) + 4);
			if (bM & 0x0400)
				*(bP->out.ptr.u8 + 5) =
				    *(((uint8_t *) bB) + 5);
			if (bM & 0x0200)
				*(bP->out.ptr.u8 + 6) =
				    *(((uint8_t *) bB) + 6);
			if (bM & 0x0100)
				*(bP->out.ptr.u8 + 7) =
				    *(((uint8_t *) bB) + 7);
		}
	}
	if ((bM & 0x00ff) == 0x00ff)
		*(bP->out.ptr.u64 + 1) = *((uint64_t *) bB + 1);
	else {
		if ((bM & 0x00f0) == 0x00f0)
			*(bP->out.ptr.u32 + 2) = *(((uint32_t *) bB) + 2);
		else {
			if (bM & 0x0080)
				*(bP->out.ptr.u8 + 8) =
				    *(((uint8_t *) bB) + 8);
			if (bM & 0x0040)
				*(bP->out.ptr.u8 + 9) =
				    *(((uint8_t *) bB) + 9);
			if (bM & 0x0020)
				*(bP->out.ptr.u8 + 10) =
				    *(((uint8_t *) bB) + 10);
			if (bM & 0x0010)
				*(bP->out.ptr.u8 + 11) =
				    *(((uint8_t *) bB) + 11);
		}
		if ((bM & 0x000f) == 0x000f)
			*(bP->out.ptr.u32 + 3) = *(((uint32_t *) bB) + 3);
		else {
			if (bM & 0x0008)
				*(bP->out.ptr.u8 + 12) =
				    *(((uint8_t *) bB) + 12);
			if (bM & 0x0004)
				*(bP->out.ptr.u8 + 13) =
				    *(((uint8_t *) bB) + 13);
			if (bM & 0x0002)
				*(bP->out.ptr.u8 + 14) =
				    *(((uint8_t *) bB) + 14);
			if (bM & 0x0001)
				*(bP->out.ptr.u8 + 15) =
				    *(((uint8_t *) bB) + 15);
		}
	}
	break;
case BO_AW_8 | BO_AW_4 | BO_AW_2:
	if ((bM & 0xff00) == 0xff00)
		*(bP->out.ptr.u64) = *((uint64_t *) bB);
	else {
		if ((bM & 0xc000) && (bM & 0x3000))
			*(bP->out.ptr.u32) = *((uint32_t *) bB);
		else {
			if (bM & 0xc000)
				*(bP->out.ptr.u16) = *((uint16_t *) bB);
			if (bM & 0x3000)
				*(bP->out.ptr.u16 + 1) =
				    *(((uint16_t *) bB) + 1);
		}
		if ((bM & 0x0c00) && (bM & 0x0300))
			*(bP->out.ptr.u32 + 1) = *(((uint32_t *) bB) + 1);
		else {
			if (bM & 0x0c00)
				*(bP->out.ptr.u16 + 2) =
				    *(((uint16_t *) bB) + 2);
			if (bM & 0x0300)
				*(bP->out.ptr.u16 + 3) =
				    *(((uint16_t *) bB) + 3);
		}
	}
	if ((bM & 0x00ff) == 0x00ff)
		*(bP->out.ptr.u64 + 1) = *((uint64_t *) bB + 1);
	else {
		if ((bM & 0x00c0) && (bM & 0x0030))
			*(bP->out.ptr.u32 + 2) = *(((uint32_t *) bB) + 2);
		else {
			if (bM & 0x00c0)
				*(bP->out.ptr.u16 + 4) =
				    *(((uint16_t *) bB) + 4);
			if (bM & 0x0030)
				*(bP->out.ptr.u16 + 5) =
				    *(((uint16_t *) bB) + 5);
		}
		if ((bM & 0x000c) && (bM & 0x0003))
			*(bP->out.ptr.u32 + 3) = *(((uint32_t *) bB) + 3);
		else {
			if (bM & 0x000c)
				*(bP->out.ptr.u16 + 6) =
				    *(((uint16_t *) bB) + 6);
			if (bM & 0x0003)
				*(bP->out.ptr.u16 + 7) =
				    *(((uint16_t *) bB) + 7);
		}
	}
	break;
case BO_AW_8 | BO_AW_4 | BO_AW_2 | BO_AW_1:
	if ((bM & 0xff00) == 0xff00)
		*(bP->out.ptr.u64) = *((uint64_t *) bB);
	else {
		if ((bM & 0xc000) && (bM & 0x3000))
			*(bP->out.ptr.u32) = *((uint32_t *) bB);
		else {
			if ((bM & 0xc000) == 0xc000)
				*(bP->out.ptr.u16) = *((uint16_t *) bB);
			else {
				if (bM & 0x8000)
					*(bP->out.ptr.u8) =
					    *((uint8_t *) bB);
				if (bM & 0x4000)
					*(bP->out.ptr.u8 + 1) =
					    *(((uint8_t *) bB) + 1);
			}
			if ((bM & 0x3000) == 0x3000)
				*(bP->out.ptr.u16 + 1) =
				    *(((uint16_t *) bB) + 1);
			else {
				if (bM & 0x2000)
					*(bP->out.ptr.u8 + 2) =
					    *(((uint8_t *) bB) + 2);
				if (bM & 0x1000)
					*(bP->out.ptr.u8 + 3) =
					    *(((uint8_t *) bB) + 3);
			}
		}
		if ((bM & 0x0c00) && (bM & 0x0300))
			*(bP->out.ptr.u32 + 1) = *(((uint32_t *) bB) + 1);
		else {
			if ((bM & 0x0c00) == 0xc00)
				*(bP->out.ptr.u16 + 2) =
				    *(((uint16_t *) bB) + 2);
			else {
				if (bM & 0x0800)
					*(bP->out.ptr.u8 + 4) =
					    *(((uint8_t *) bB) + 4);
				if (bM & 0x0400)
					*(bP->out.ptr.u8 + 5) =
					    *(((uint8_t *) bB) + 5);
			}
			if ((bM & 0x0300) == 0x300)
				*(bP->out.ptr.u16 + 3) =
				    *(((uint16_t *) bB) + 3);
			else {
				if (bM & 0x0200)
					*(bP->out.ptr.u8 + 6) =
					    *(((uint8_t *) bB) + 6);
				if (bM & 0x0100)
					*(bP->out.ptr.u8 + 7) =
					    *(((uint8_t *) bB) + 7);
			}
		}
	}
	if ((bM & 0x00ff) == 0x00ff)
		*(bP->out.ptr.u64 + 1) = *((uint64_t *) bB + 1);
	else {
		if ((bM & 0x00c0) && (bM & 0x0030))
			*(bP->out.ptr.u32 + 2) = *(((uint32_t *) bB) + 2);
		else {
			if ((bM & 0x00c0) == 0x00c0)
				*(bP->out.ptr.u16 + 4) =
				    *(((uint16_t *) bB) + 4);
			else {
				if (bM & 0x0080)
					*(bP->out.ptr.u8 + 8) =
					    *(((uint8_t *) bB) + 8);
				if (bM & 0x0040)
					*(bP->out.ptr.u8 + 9) =
					    *(((uint8_t *) bB) + 9);
			}
			if ((bM & 0x0030) == 0x0030)
				*(bP->out.ptr.u16 + 5) =
				    *(((uint16_t *) bB) + 5);
			else {
				if (bM & 0x0020)
					*(bP->out.ptr.u8 + 10) =
					    *(((uint8_t *) bB) + 10);
				if (bM & 0x0010)
					*(bP->out.ptr.u8 + 11) =
					    *(((uint8_t *) bB) + 11);
			}
		}
		if ((bM & 0x000c) && (bM & 0x0003))
			*(bP->out.ptr.u32 + 3) = *(((uint32_t *) bB) + 3);
		else {
			if ((bM & 0x000c) == 0x000c)
				*(bP->out.ptr.u16 + 6) =
				    *(((uint16_t *) bB) + 6);
			else {
				if (bM & 0x0008)
					*(bP->out.ptr.u8 + 12) =
					    *(((uint8_t *) bB) + 12);
				if (bM & 0x0004)
					*(bP->out.ptr.u8 + 13) =
					    *(((uint8_t *) bB) + 13);
			}
			if ((bM & 0x0003) == 0x0003)
				*(bP->out.ptr.u16 + 7) =
				    *(((uint16_t *) bB) + 7);
			else {
				if (bM & 0x0002)
					*(bP->out.ptr.u8 + 14) =
					    *(((uint8_t *) bB) + 14);
				if (bM & 0x0001)
					*(bP->out.ptr.u8 + 15) =
					    *(((uint8_t *) bB) + 15);
			}
		}
	}
	break;
}
