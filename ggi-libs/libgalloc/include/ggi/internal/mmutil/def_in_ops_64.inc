
/* This code processes any input operations

   This version is intended for machines with all access operations from
        8 to 64 bits.

   You should supply the following as macros/variables:

   bB is a pointer to an 8-byte buffer, aligned on a 64-bit boundary,
	containing either a uint64_t or int64_t
   bP is a pointer to the batchparm being processed, and must be of 
        type (batchparm *)
*/


if (bP->parmtype & BO_PT_INVERT)
	*((uint64_t *) bB) = ~*((uint64_t *) bB);
if (bP->parmtype & BO_PT_SINT) {
	switch (bP->parmtype & BO_PT_IN_ARG_MASK) {
	case 0:
		break;
	case BO_PT_IN_CMP_EQ:
		*((int64_t *) bB) = (*((int64_t *) bB) == bP->arg.s64);
		break;
	case BO_PT_IN_CMP_LT:
		*((int64_t *) bB) = (*((int64_t *) bB) < bP->arg.s64);
		break;
	case BO_PT_IN_CMP_LTE:
		*((int64_t *) bB) = (*((int64_t *) bB) <= bP->arg.s64);
		break;
	case BO_PT_IN_CMP_GT:
		*((int64_t *) bB) = (*((int64_t *) bB) > bP->arg.s64);
		break;
	case BO_PT_IN_CMP_GTE:
		*((int64_t *) bB) = (*((int64_t *) bB) >= bP->arg.s64);
		break;
	case BO_PT_IN_CMP_NE:
		*((int64_t *) bB) = (*((int64_t *) bB) != bP->arg.s64);
		break;
	case BO_PT_IN_ADD:
		*((int64_t *) bB) += bP->arg.s64;
		break;
	}
} else {
	switch (bP->parmtype & BO_PT_IN_ARG_MASK) {
	case 0:
		break;
	case BO_PT_IN_CMP_EQ:
		*((uint64_t *) bB) = (*((uint64_t *) bB) == bP->arg.u64);
		break;
	case BO_PT_IN_CMP_LT:
		*((uint64_t *) bB) = (*((uint64_t *) bB) < bP->arg.u64);
		break;
	case BO_PT_IN_CMP_LTE:
		*((uint64_t *) bB) = (*((uint64_t *) bB) <= bP->arg.u64);
		break;
	case BO_PT_IN_CMP_GT:
		*((uint64_t *) bB) = (*((uint64_t *) bB) > bP->arg.u64);
		break;
	case BO_PT_IN_CMP_GTE:
		*((uint64_t *) bB) = (*((uint64_t *) bB) >= bP->arg.u64);
		break;
	case BO_PT_IN_CMP_NE:
		*((uint64_t *) bB) = (*((uint64_t *) bB) != bP->arg.u64);
		break;
	case BO_PT_IN_ADD:
		*((uint64_t *) bB) += bP->arg.u64;
		break;
	}
}
