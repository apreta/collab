

/* This code snip reassembles the bytes retrieved from an input
   batchparm, including padding and justification and signedness,
   into a 64-bit value in native endian format.

   It is inefficient, because it handles all the bells and whistles.
   If you don't have non-byte-aligned steps/widths and access width 
   restrictions, don't use it!  There are/will be/should be more optimized 
   versions for those situations.

   This version is intended for machines with all access operations from
        8 to 64 bits.

   You should supply the following as macros/variables:

   bB is a pointer to a 16-byte buffer, aligned on a 64-bit boundary,
        containing the unspliced data (e.g. what comes from running
	it through the def_load_64.inc code)
   bP is a pointer to the batchparm being processed, and must be of 
        type (batchparm *)
*/

  /* Make it native-endian so we can shift the bitfield around correctly */
BO_DO_BLOCKSWAB64(bP, *((uint64_t *) bB));
BO_DO_BLOCKSWAB64(bP, *((uint64_t *) bB + 1));

  /* Get both halves of the bitfield into the same uint64_t */
if (bP->parmtype & BO_PT_JUSTIFY_MS) {
	*((uint64_t *) bB) <<= bP->in.boff;
	if (bP->in.boff + bP->datasize > 64)
		*((uint64_t *) bB) |=
		    *((uint64_t *) bB + 1) >> (64 - bP->in.boff);

	/* Get rid of cruft from remainder of byte (i.e. when datasize %8 != 0). */
	*((uint64_t *) bB) &= 0xffffffffffffffffLL << (64 - bP->datasize);

	/* Undo the field's endianness.  Note, we do NOT check for invalid
	   combinations of datasize and endianness. (e.g. a 6-bit field can only
	   be BO_EP_RE_1.)  Such combinations will produce spurious results. */
	switch ((bP->datasize + 7) / 8) {
	case 1:
		BO_DO_PARMSWAB8(bP, *((uint64_t *) bB));
		break;
	case 2:
		BO_DO_PARMSWAB16(bP, *((uint64_t *) bB));
		break;
	case 3:
	case 4:
		BO_DO_PARMSWAB32(bP, *((uint64_t *) bB));
		break;
	case 5:
	case 6:
	case 7:
	case 8:
		BO_DO_PARMSWAB64(bP, *((uint64_t *) bB));
		break;
	}
	/* Fill out least signifigant bits. */
	if (bP->datasize < 64)
		*((uint64_t *) bB) |= *((uint64_t *) bB) >> bP->datasize;
	if (bP->datasize < 32)
		*((uint64_t *) bB) |= *((uint64_t *) bB) >> (bP->datasize * 2);
	if (bP->datasize < 16)
		*((uint64_t *) bB) |= *((uint64_t *) bB) >> (bP->datasize * 4);
	if (bP->datasize < 8)
		*((uint64_t *) bB) |= *((uint64_t *) bB) >> (bP->datasize * 8);
	if (bP->datasize < 4)
		*((uint64_t *) bB) |=
		    *((uint64_t *) bB) >> (bP->datasize * 16);
	if (bP->datasize < 2)
		*((uint64_t *) bB) |= *((uint64_t *) bB) >> 32;
} else {
	if ((bP->in.boff + bP->datasize) < 64)
		*((uint64_t *) bB) >>= (64 - (bP->in.boff + bP->datasize));
	else
		*((uint64_t *) bB) <<= ((bP->in.boff + bP->datasize) - 64);

	if (bP->in.boff + bP->datasize > 64)
		*((uint64_t *) bB) |=
		    *((uint64_t *) bB + 1) >> (128 -
					     (bP->in.boff + bP->datasize));

	/* Undo the field's endianness.  Note, we do NOT check for invalid
	   combinations of datasize and endianness. (e.g. a 6-bit field can only
	   be BO_EP_RE_1, not BO_EP_RE_2.)  Such combinations will produce 
	   spurious results. */
	switch ((bP->datasize + 7) / 8) {
	case 1:
		BO_DO_PARMSWAB8(bP, *((uint64_t *) bB));
		break;
	case 2:
		BO_DO_PARMSWAB16(bP, *((uint64_t *) bB));
		break;
	case 3:
	case 4:
		BO_DO_PARMSWAB32(bP, *((uint64_t *) bB));
		break;
	case 5:
	case 6:
	case 7:
	case 8:
		BO_DO_PARMSWAB64(bP, *((uint64_t *) bB));
		break;
	}
	if ((bP->parmtype & BO_PT_SINT) &&
	    (*((uint64_t *) bB) & 0x1 << (bP->datasize - 1))) {
		*((uint64_t *) bB) |= 0xffffffffffffffffLL << (bP->datasize);
	} else {
		*((uint64_t *) bB) &=
		    0xffffffffffffffffLL >> (64 - bP->datasize);
	}
}
