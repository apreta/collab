/* $Id: galloc.h,v 1.53 2007/12/20 23:38:24 cegger Exp $
******************************************************************************

   LibGalloc: extension internals

   Copyright (C) 2001 Christoph Egger   [Christoph_Egger@t-online.de]
   Copyright (C) 2002 Brian S. Julin    [bri@tull.umassp.edu]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#ifndef _GGI_INTERNAL_GALLOC_H
#define _GGI_INTERNAL_GALLOC_H

#include <ggi/internal/ggi.h>

#define HAVE_STRUCT_GALLOC
#include <ggi/galloc.h>

__BEGIN_DECLS

/* Exported variables */
GGIGAAPIVAR struct gg_api *libgalloc;

struct ggiGA_template_state {
	ggiGA_resource_handle	mode;
	ggiGA_resource_list	haslist;
};

GGIGAAPIFUNC int ggiGACheck_template(ggi_visual_t vis,
	ggiGA_resource_list request,
	ggiGA_resource_list *result,
	struct ggiGA_template_state *state, void *privstate);

GGIGAAPIFUNC int ggiGASet_template(ggi_visual_t vis,
	ggiGA_resource_list request,
	ggiGA_resource_list *result,
	struct ggiGA_template_state *state, void *privstate);

GGIGAAPIFUNC int ggiGARelease_template(ggi_visual_t vis,
	ggiGA_resource_list *list,
	ggiGA_resource_handle *handle,
	struct ggiGA_template_state *state, void *privstate);

/* Template callback flags, used to decide whether to call a
 * callback function, and inside a callback function to figure out
 * why/when it has been called.  See the do_callback inline function
 * in templates.c for an understanding of the control flow.
 */
enum ggiGA_callback_command {

  /* This enum is only internally used and thus can't break
   * the API */

  GA_CB_MATCH =     0x00000001, /* Call when doing rough match           */
  GA_CB_PRECHECK =  0x00000002, /* Call before checking storage          */
  GA_CB_CHECK =     0x00000004, /* Call to check storage                 */
  GA_CB_POSTCHECK = 0x00000008, /* Call after checking storage           */
  GA_CB_UNCHECK =   0x00000010, /* Call to temporarily release storage   */
  GA_CB_RECHECK =   0x00000020, /* Call to restore unchecked storage     */
  GA_CB_SET =       0x00000040, /* Call to activate resources            */
  GA_CB_UNSET =     0x00000080, /* Call to deactivate resources          */
  GA_CB_RELEASE =   0x00000100  /* Call to permanently release resources */
};

struct ggiGA_taghead;                           /* needed for prototype. */
typedef struct ggiGA_taghead *ggiGA_taghead_t;	/* needed for prototype. */

struct galloc_context;

typedef int (ggiGA_reset_checkstate)(struct galloc_context *ctxt,
				     struct ggiGA_template_state *state,
				     void *privstate);

typedef int (ggiGA_migrate_checkstate)(struct galloc_context *ctxt,
				       struct ggiGA_template_state *state,
				       void *privstate,
				       ggiGA_resource_list from,
				       ggiGA_resource_list to);

typedef int (ggiGA_reslist_callback)(struct galloc_context *ctxt,
				     ggiGA_resource_list reslist,
				     enum ggiGA_callback_command command,
				     ggiGA_resource_handle res,
				     ggiGA_resource_handle head,
				     ggiGA_taghead_t taghead,
				     ggiGA_resource_handle has,
				     struct ggiGA_template_state *state,
				     void *privstate);


/* Prototypes for functions available in templates.c:
 */

/* Builds rings (linked lists) based on tag groups. */
int ggiGA_basic_parse_list(ggiGA_resource_list list);

GGIGAAPIFUNC ggiGA_reslist_callback ggiGA_default_match;
GGIGAAPIFUNC ggiGA_reslist_callback ggiGA_default_precheck;
             ggiGA_reslist_callback ggiGA_default_motor_check;
             ggiGA_reslist_callback ggiGA_default_carb_check;
             ggiGA_reslist_callback ggiGA_default_tank_check;
GGIGAAPIFUNC ggiGA_reslist_callback ggiGA_default_check;
             ggiGA_reslist_callback ggiGA_default_motor_uncheck;
             ggiGA_reslist_callback ggiGA_default_carb_uncheck;
             ggiGA_reslist_callback ggiGA_default_tank_uncheck;
GGIGAAPIFUNC ggiGA_reslist_callback ggiGA_default_uncheck;
             ggiGA_reslist_callback ggiGA_default_motor_set;
             ggiGA_reslist_callback ggiGA_default_carb_set;
             ggiGA_reslist_callback ggiGA_default_tank_set;
GGIGAAPIFUNC ggiGA_reslist_callback ggiGA_default_set;
GGIGAAPIFUNC ggiGA_reslist_callback ggiGA_default_unset;
GGIGAAPIFUNC ggiGA_reslist_callback ggiGA_default_release;


/* Internal data type.  Nothing but LibGAlloc sublibs may manipulate these
 * structures directly.  Any other access to these structures must be
 * through LibGAlloc API functions.  This keeps us from losing our minds.
 */
#define GA_RESOURCE_COMMON_MEMBERS \
	int dummy;	/* prevents user from overriding internal states */ \
	ggiGA_resource_type res_type;		/* See ggiGAGetType */      \
	ggiGA_resource_state res_state;		/* See ggiGAGetState */     \
	/* The following are used internally to contain parsed structure. */\
	ggiGA_resource_handle master;					    \
	ggiGA_resource_taghandle left, right; 				    \
	/* next resource in list -- see typedef in ggi/galloc.h */	    \
	GG_SIMPLEQ_ENTRY(ggiGA_resource) listentry;			    \
	/* These are used to provide target-specific hooks */		    \
	enum ggiGA_callback_command cbflags1;				    \
	ggiGA_reslist_callback *cb1;					    \
	enum ggiGA_callback_command cbflags2;				    \
	ggiGA_reslist_callback *cb2


struct ggiGA_resource {
	/* As above. */
	GA_RESOURCE_COMMON_MEMBERS;

	/* Storage type actually allocated. */
	ggiGA_storage_type storage_ok;

	/* Flags describing the sharing of the storage with other resources */
	ggiGA_storage_share storage_share;

	/* The visual this resource contains to */
	ggi_visual_t vis;

	/* usage-counter for this instance of the structure.  Structures
	 * should not be freed unless it is 0.
	 */
	int res_count;

	/* Either (sub-)pixel or (sub-)dot. This is the coordbase
	 * this resource is stored and handled internally through
	 * all extensions.
	 */
	enum ggi_ll_coordbase cb;

	/* the properties of this resource */
	struct ggiGA_resource_props *props;

	/* Target or resource-type-specific properties can be put here.  This
	 * area is respected by all the list manipulation functions and treated
	 * identically to the props area.  Note GA_RT_FRAME resources use this
	 * area, so it is not available for re-use in GA_RT_FRAME resources.
	 */
	void *priv;
	size_t priv_size;
};

/* Alternative view of contents for special internal list structure
 * resources (tag group heads).  Please never alloc sizeof(*ggiGA_taghead_t),
 * always alloc sizeof(struct ggiGA_resource) which is the larger of the two.
 */
struct ggiGA_taghead {

	GA_RESOURCE_COMMON_MEMBERS;

	/* The tag group which this is the head of. */
	tag_t tag2;

	/* Links into the ring formed when tag group tag2 is parsed. */
	ggiGA_resource_taghandle left2, right2;

	/* Flags defining the semantic of the tag group head */
	unsigned int flags;

	/* If GA_TAGHEAD_EXCLUSIVE is set, the resources must still be
	 * use the same type of storage as they must in a normal tag group,
	 * such they can be shared.  However, storage should not overlap
	 * among resources in this tag group.
	 */
#define GA_TAGHEAD_EXCLUSIVE	0x0001
};


/* Stuff that is potentially useful to get non-LibGAlloc sublibs out
 * of sticky situations is separated into a separate include file.
 */
#include <ggi/internal/galloc_ops.h>


/* Functions in rescmp.c are useful for building custom target callbacks */

typedef int (_ga_cmp_func)(struct ggiGA_resource_props *res,
			struct ggiGA_resource_props *rescap,
			struct ggiGA_resource_props *has,
			struct ggiGA_resource_props *hascap,
			int rc);

_ga_cmp_func	_ga_cmp_storage;
_ga_cmp_func	_ga_cmp_qty;
_ga_cmp_func	_ga_cmp_factors;


/* Default strings for ggiGAanprintf */

#define GA_RTSTRINGS \
	"Unspecified Type",         /* GA_RT_DONTCARE  */     \
	"Main Framebuffer",         /* GA_RT_FRAME     */     \
	"Anciliary Buffer",         /* GA_RT_BUFFER    */     \
	"Blitting Object",	    /* GA_RT_BOB       */     \
	"Sprite",                   /* GA_RT_SPRITE    */     \
	"Video",                    /* GA_RT_VIDEO     */     \
	"Translation Window",       /* GA_RT_WINDOW    */     \
	"Miscellaneous",            /* GA_RT_MISC      */     \
	"Renderer",                 /* GA_RT_MISC      */     \
	"Reslist Internal"          /* GA_RT_MISC      */

#define GA_RTFRAMESTRINGS \
	"Unspecified"               /* GA_RT_FRAME_DONTCARE */

#define GA_RTBUFFERSTRINGS \
	"Unspecified",              /* GA_RT_BUFFER_DONTCARE */   \
	"Raw",			    /* GA_RT_BUFFER_RAW	     */   \
	"Z",                        /* GA_RT_BUFFER_ZBUFFER  */   \
	"Alpha",                    /* GA_RT_BUFFER_ABUFFER  */   \
	"Stencil",                  /* GA_RT_BUFFER_SBUFFER  */   \
	"Texture",                  /* GA_RT_BUFFER_TBUFFER  */   \
	"Spare Drawing Space",      /* GA_RT_BUFFER_SWATCH   */

#define GA_RTBOBSTRINGS \
	"Unspecified"               /* GA_RT_BOB_DONTCARE */

#define GA_RTSPRITESTRINGS \
	"Unspecified",              /* GA_RT_SPRITE_DONTCARE */   \
	"Hardware Mouse Cursor",    /* GA_RT_SPRITE_POINTER  */   \
	"Hardware Text Cursor",     /* GA_RT_SPRITE_CURSOR   */   \
	"Basic Sprite"              /* GA_RT_SPRITE_SPRITE   */

#define GA_RTVIDEOSTRINGS \
	"Unspecified",              /* GA_RT_VIDEO_DONTCARE  */   \
	"Motion Source",            /* GA_RT_VIDEO_MOTION    */   \
	"Streaming Source",         /* GA_RT_VIDEO_STREAMING */

#define GA_RTWINDOWSTRINGS \
	"Unspecified",              /* GA_RT_WINDOW_DONTCARE */   \
	"YUV Viewport",             /* GA_RT_WINDOW_YUV      */   \
	"Address Translation Port"  /* GA_RT_WINDOW_MM       */

#define GA_RTMISCSTRINGS \
	"Unspecified",              /* GA_RT_MISC_DONTCARE   */     \
	"Ray Position",             /* GA_RT_MISC_RAYPOS     */     \
	"VBlank AutoFlip",          /* GA_RT_MISC_RAYTRIG_SET*/     \
	"VBlank AutoPalette",       /* GA_RT_MISC_RAYTRIG_PAL*/     \
	"Hardware Splitline",       /* GA_RT_MISC_SPLITLINE  */     \
	"Font Data"                 /* GA_RT_MISC_FONTCELL   */

#define GA_RTRENDERERSTRINGS \
	"Unspecified",              /* GA_RT_RENDERER_DONTCARE */   \
	"GGI drawop renderer"       /* GA_RT_RENDERER_DRAWOPS  */

#define GA_RTRESLISTSTRINGS \
	"NOOP",                     /* GA_RT_RESLIST_DONTCARE */   \
	"Tag Group Head"            /* GA_RT_RESLIST_DRAWOPS  */

#define GA_RTSTRING(vis, r) LIBGGI_GALLOCEXT(vis)->\
rtstrings[(r & GA_RT_TYPE_MASK) >> GA_RTSHIFT]
#define GA_RTSUBSTRING(vis, r) LIBGGI_GALLOCEXT(vis)->\
rtsubstrings[(r & GA_RT_TYPE_MASK) >> GA_RTSHIFT][r & GA_RT_SUBTYPE_MASK]


struct ggiGA_sharemap {
	struct ggiGA_resource ***sharemap;

	uint32_t have_vis_space;
	uint32_t used_vis_space;
	uint32_t have_tag_space;
	uint32_t used_tag_space;
	uint32_t have_shared_space;
	uint32_t used_shared_space;
};

#define GA_TYPE_UNKNOWN	(1 << 0)
#define GA_TYPE_1D	(1 << 1)
#define GA_TYPE_2D	(1 << 2)

struct ggiGA_shareable_types {
	/* the type can shared with... */
	ggiGA_resource_type type1;

	/* ... this type */
	ggiGA_resource_type type2;

	/* 1D- or 2D-resource */
	int dimension1, dimension2;
};

//---------------------------------------------------------------------

#define GALLOC_CONTEXTFLAG_PUBLIC   0x0000ffff
#define GALLOC_CONTEXTFLAG_PRIVATE  0xffff0000

#define GALLOC_CONTEXTFLAG_OPENING  0x00010000
#define GALLOC_CONTEXTFLAG_CLOSING  0x00020000

typedef int (gallocfunc_check)(struct galloc_context *ga_res,
		ggiGA_resource_list request,
		ggiGA_resource_list *result);
		      
typedef int (gallocfunc_set)(struct galloc_context *ga_res,
		ggiGA_resource_list request,
		ggiGA_resource_list *result);

typedef int (gallocfunc_release)(struct galloc_context *ga_res,
		ggiGA_resource_list *list,
		ggiGA_resource_handle *handle);

typedef int (gallocfunc_anprintf)(struct galloc_context *ga_res,
		ggiGA_resource_list request,
		size_t size, const char *format, char **out);

/* This one is private, for use by ggiGAAddMode */
typedef int (gallocfunc_mode)(struct galloc_context *ga_res, ggiGA_resource_handle *out);

/* This one is private, for use by  ggiGASearchShareable */
typedef int (gallocfunc_checkIfShareable)(struct galloc_context *ga_res,
		 ggiGA_resource_handle reshandle,
		 ggiGA_resource_handle tocompare);

struct galloc_ops
{
	gallocfunc_check		*check;
	gallocfunc_set			*set;
	gallocfunc_release		*release;
	gallocfunc_anprintf		*anprintf;
	gallocfunc_mode			*mode;
	gallocfunc_checkIfShareable	*checkIfShareable;

	/* callback functions used by default on this visual. */
	ggiGA_reset_checkstate *reset_privstate;
	ggiGA_migrate_checkstate *migrate_privstate;
	ggiGA_reslist_callback *default_callbacks[9];
};

/*
 * A resource is a visual augmented with the galloc API 
 */
struct galloc_context {

	struct gg_plugin plugin;
	
	uint32_t flags;

	/* The visual that we manage */
	struct ggi_visual *vis;
	
	/* */
	struct galloc_ops *ops;
	
	/* The list of currently allocated resources in this visual */
	ggiGA_resource_list current_reslist;

	/* This contains a copy of the last successful request list
	 * to ggiGASet, in case the user loses it.
	 */
	ggiGA_resource_list current_reqlist;

	/* This is the master copy of the list representing resources
	 * available on the visual.
	 */
	ggiGA_resource_list haslist;

#if 0
	/* callback functions used by default on this visual. */
	ggiGA_reset_checkstate *reset_privstate;
	ggiGA_migrate_checkstate *migrate_privstate;
	ggiGA_reslist_callback *default_callbacks[9];
#endif

	/* pointer to the dummyfunc() used for selective overloading. */
	void *dummy;

	/* pointers to strings used by ggiGAanprintf */
	char **rtstrings;
	char ***rtsubstrings;
	/* plugin private data */
	void *priv;
};

/*
 *  MODULE
 *
 * A galloc_module_context is a module that defines an initializer for a
 * galloc_context.
 */
typedef int (gallocfunc_context_init)(struct galloc_context *,
					const char *,
					const char *,
					void *);
typedef int (gallocfunc_context_exit)(struct galloc_context *);

#define GALLOC_MODULE_CONTEXT	0
struct galloc_module_context {
	
	struct gg_module module;
	gallocfunc_context_init *init; /* alloc priv and set up resource->ops */
	gallocfunc_context_exit *exit; /* free priv */
};

#define GALLOC_PRIV(stem) STEM_API_PRIV(stem, libgalloc)
#define GALLOC_CONTEXT(stem) STEM_API_DATA(stem, libgalloc, struct galloc_context*)

#define GA_RESLIST(stem)	GALLOC_CONTEXT(stem)->current_reslist
#define GA_REQLIST(stem)	GALLOC_CONTEXT(stem)->current_reqlist
#define GA_HASLIST(stem)	GALLOC_CONTEXT(stem)->haslist
#define GA_PRIV(stem)		GALLOC_CONTEXT(stem)->priv

#if 0

/* Search for a matching resource that can share with the given one.
 * ret_handle is NULL on failure.
 * If return value is > 0, then we have to allocate
 * ret-val * colordepth bytes to be able to share with.
 * Note, that the allocation process may fail (i.e. when there is
 * not enough space).
 */
int ggiGASearchShareable(ggi_visual_t vis, ggiGA_resource_list reslist,
			 ggiGA_resource_handle handle,
			 ggiGA_resource_handle *ret_handle);


/* Let the "toshare" -resource share with the "shareable" one.
 * Returns GALLOC_OK, when successful.
 */
int ggiGADoShare(ggiGA_resource_handle shareable,
		 ggiGA_resource_handle toshare);


/* Unshares the resource, so that it can be released as usual.
 */
int ggiGAUndoShare(ggiGA_resource_list list,
		   ggiGA_resource_handle handle);

#endif

__END_DECLS

#endif /* _GGI_INTERNAL_GALLOC_H */
