/* $Id: galloc_int.h,v 1.20 2007/12/20 23:38:25 cegger Exp $
******************************************************************************

   LibGalloc: extension internals

   Copyright (C) 2003 Christoph Egger   [Christoph_Egger@t-online.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#ifndef _GGI_INTERNAL_GALLOC_INT_H
#define _GGI_INTERNAL_GALLOC_INT_H


#include <ggi/internal/ggi.h>
#include <ggi/internal/galloc_debug.h>	/* macro requires LIB_ASSERT */
#include <ggi/mmutil.h>			/* storage requires rm_range_t */


/* Functions for libgalloc internal use only!
 * Note, this file must be included explicitely by
 * libgalloc core files.
 */


/* tag.c
 */

tag_t ggiGAGetTagTail(ggiGA_resource_handle handle);

int ggiGASetTagTail(ggiGA_resource_handle handle, tag_t tag);

int ggiGACompareTag(ggiGA_resource_handle handle1,
		    ggiGA_resource_handle handle2);

ggiGA_resource_handle ggiGAFindNextTag(ggiGA_resource_list reslist,
				       ggiGA_resource_handle handle);

ggiGA_resource_handle ggiGAFindFirstTag(ggiGA_resource_list reslist,
					ggiGA_resource_handle handle);

ggiGA_resource_handle ggiGAFindLastTag(ggiGA_resource_list reslist,
				       ggiGA_resource_handle handle);


/* Find last resource before "handle" in "reslist" with the same type and
 * tag as "handle" has.  Or, if "handle" is not in "reslist", finds the
 * last one in the entire list instead.  This function has asserts that
 * flag suspicious calls from API functions, so if you need it for other
 * purposes, dup it.
 */
ggiGA_resource_handle ggiGAFindPrevTag(ggiGA_resource_list reslist,
				       ggiGA_resource_handle handle);

/* Find the compound resource to which the first resource with the
 * same type and tag belongs.
 */
ggiGA_resource_handle ggiGAFindCompoundByTag(ggiGA_resource_list reslist,
					     ggiGA_resource_handle handle);



/* taggroup.c
 */

GGIGAAPIFUNC int ggiGAIsTagHead(ggiGA_resource_taghandle handle);

int ggiGAInTagGroup(ggiGA_resource_taghandle handle, tag_t tag);

int ggiGATagGroupInit(ggiGA_resource_taghandle handle);

int ggiGATagGroupAdd(ggiGA_resource_taggroup list,
			ggiGA_resource_handle resource);

int ggiGATagGroupRemove(ggiGA_resource_taggroup list,
			ggiGA_resource_handle resource);

int ggiGATagGroupAddHead(ggiGA_resource_taggroup list,
			ggiGA_resource_taghandle taghead);

int ggiGATagGroupRemoveHead(ggiGA_resource_taggroup list,
			ggiGA_resource_taghandle taghead);

ggiGA_taghead_t ggiGAFindTagHeadInList(ggiGA_resource_list list,
			ggiGA_resource_handle cthis);

ggiGA_taghead_t ggiGAFindTagHeadInGroup(ggiGA_resource_taggroup list);

int ggiGAHasNestedTagGroup(ggiGA_resource_taggroup list);


#define ggiGAGetResourceVariant(handle)	\
	ggiGAIsMotor(handle) ? 1 : (ggiGAIsCarb(handle) ? 2 : 0)


static inline
ggiGA_taghead_t ggiGATagGetHead(ggiGA_resource_taghandle handle)
{
	LIB_ASSERT(ggiGAIsTagHead(handle), "handle must be a taghead");
	return ((ggiGA_taghead_t)handle);
}	/* ggiGATagGetHead */


static inline
ggiGA_resource_taghandle ggiGATagGetLeft(ggiGA_resource_taghandle handle)
{
	/* A ring list has NEVER an empty member */
	LIB_ASSERT(handle->left != NULL, "A ring list has NEVER an empty member");
	return handle->left;
}	/* ggiGATagGetLeft */


static inline
ggiGA_resource_taghandle ggiGATagGetRight(ggiGA_resource_taghandle handle)
{
	/* A ring list has NEVER an empty member */
	LIB_ASSERT(handle->right != NULL, "A ring list has NEVER an empty member");
	return handle->right;
}	/* ggiGATagGetRight */


static inline
ggiGA_resource_taghandle ggiGATagHeadGetLeft(ggiGA_resource_taghandle handle)
{
	ggiGA_taghead_t th;

	LIB_ASSERT(handle != NULL, "handle == NULL");

	if (!ggiGAIsTagHead(handle)) return NULL;
	th = ggiGATagGetHead(handle);

	/* A ring list has NEVER an empty member */
	LIB_ASSERT(th->left2 != NULL, "A ring list has NEVER an empty member");

	return th->left2;
}	/* ggiGATagHeadGetLeft */


static inline
ggiGA_resource_taghandle ggiGATagHeadGetRight(ggiGA_resource_taghandle handle)
{
	ggiGA_taghead_t th;

	LIB_ASSERT(handle != NULL, "handle == NULL");

	if (!ggiGAIsTagHead(handle)) return NULL;
	th = ggiGATagGetHead(handle);

	/* A ring list has NEVER an empty member */
	LIB_ASSERT(th->right2 != NULL, "A ring list has NEVER an empty member");

	return th->right2;
}	/* ggiGATagHeadGetRight */


/* taghead.c */
ggiGA_taghead_t ggiGACreateTagHead(void);

int ggiGADestroyTagHead(ggiGA_taghead_t *taghead);

int ggiGATagHeadClone(ggiGA_resource_taghandle in, ggiGA_resource_taghandle *out);

int ggiGATagHeadCopy(ggiGA_resource_taghandle in, ggiGA_resource_taghandle out);


/* storage.c
 */

void ggiGA_mode2rm (ggi_mode *resmode, rm_range_t *rm, int falign, int lalign);
void ggiGA_visdb2rm (struct ggi_visual *vis, rm_range_t *rm, char *fb);


/* stubs.c
 */
GGIGAAPIFUNC int ggiGA_Mode(ggi_visual_t vis, ggiGA_resource_handle *out);


/* share.c
 */

#if 0
int ggiGACheckIfShareable(ggi_visual_t vis,
		     ggiGA_resource_handle reshandle,
		     ggiGA_resource_handle tocompare);

/* Compares handle1 and handle2, if their props and priv-fields are
 * identical, even their own pointer is different...
 * Returns GALLOC_OK, if true.
 */
int ggiGACompare(ggiGA_resource_handle handle1, ggiGA_resource_handle handle2);


/* Compares the two lists. Note, that list1 may have more list-members
 * than list2. Returns GALLOC_OK, if successfull.
 */
int ggiGACompareList(ggiGA_resource_list list1, ggiGA_resource_list list2);

#endif

#endif	/* _GGI_INTERNAL_GALLOC_INT_H */
