/* $Id: ga_prop.h,v 1.29 2005/10/12 10:23:44 cegger Exp $
******************************************************************************

   LibGalloc: extension properties header file

   Copyright (C) 2001 Christoph Egger   [Christoph_Egger@t-online.de]

   Permission is hereby granted, free of charge, to any person obtaining a
   copy of this software and associated documentation files (the "Software"),
   to deal in the Software without restriction, including without limitation
   the rights to use, copy, modify, merge, publish, distribute, sublicense,
   and/or sell copies of the Software, and to permit persons to whom the
   Software is furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


******************************************************************************
*/

#ifndef _GGI_GALLOC_PROP_H
#define _GGI_GALLOC_PROP_H

#include <ggi/ggi.h>
#include <ggi/ll.h>

__BEGIN_DECLS


#include <ggi/ga_types.h>


struct ggiGA_tank_props {

	union {
		ggi_coord area;
		uint32_t linear;
	} buf_snap;

	/* A vague description of the buffer pixel format */
	ggi_graphtype gt;

	/* Pointer to a *shared* detailed description of the pixel format */
	ggi_directbuffer *db;

	/* Pointers to access buffer data.
	   These are surrogates to the pointers in the shared *db */
	void *read, *write;
};


struct ggiGA_motor_props {

	/* All units as per the GA_COORDBASE* bits set in
	 * the common .flags member.
	 */

	/* When positioned and sized on the screen, some features must
	 * be positioned and sized on a snap grid.
	 */
	ggi_coord pos_snap;	/* Size of grid for positioning */
	ggi_coord size_snap;	/* Size of grid for sizing */
	ggi_coord grid_start;	/* Upper left of the snap grid */
	ggi_coord grid_size;	/* Size of the snap grid */

	/* For resources that support pixeldoubling/scaling or for
	 * overlay windows that drive the DAC directly, these
	 * define the available scaling factors.
	 */
	ggi_coord mul_min, mul_max;	/* dot double, triple, etc. */
	ggi_coord div_min, div_max;	/* Sub-dot resolution (scan windows) */

	/* Available raster operations e.g. XOR with current screen data. */
	uint32_t rops;
};


struct ggiGA_carb_props {

	struct ggi_ll_math_gc can, must;
	void **block;
	int numblock;
};


#define GA_FLAG(props)	props->flags

struct ggiGA_resource_props {

	ggiGA_storage_type storage_need;
	ggiGA_storage_type storage_ok;

	/* bits to control sharing by the user */
	ggiGA_storage_share storage_share;

	/* Meaning of qty and flags vary depending on type of resource.
	   For the latter, see GA_FLAGS* defs in ga_types.h */
	uint16_t qty;
	uint32_t flags;

	/* On the way into a request, this is the desired size,
	 * when returned back from gallocation, this is the
	 * maximum size.  Units for tanks are in pixels; Units for motors
	 * are in same units as snap grid (see GA_COORDBASE* values in .flags)
	 */
	union {
		ggi_coord area;
		uint32_t linear;
	} size;

	/* Possible variants */
	union {
		struct ggiGA_motor_props motor;
		struct ggiGA_carb_props carb;
		struct ggiGA_tank_props tank;
	} sub;
};

struct ggiGA_mode {
	ggi_mode mode;
	ggi_directbuffer db;
};

__END_DECLS


#endif	/* _GGI_GALLOC_PROP_H */
