/* $Id: target_check.c,v 1.25 2005/10/08 11:06:14 cegger Exp $
******************************************************************************

   Demo of the Galloc extension.

   Authors: 2001 Christoph Egger	[Christoph_Egger@t-online.de]

   This code is placed in the public domain and may be used freely for any
   purpose.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "config.h"
#include <ggi/gg.h>
#include <ggi/galloc.h>


static int sweat2theoldies(ggi_visual_t vis, char *newlist)
{
	char *ptr;
	ggiGA_resource_list reqlist = NULL;
	ggiGA_resource_handle handle = NULL;
	struct ggiGA_resource_props p;
	int rc;

	reqlist = NULL;
	ptr = newlist;

	while (*ptr != '\0') {
		switch(*ptr) {
		case 'M':	/* Add a GA_RT_FRAME */
			printf("Adding a mode with ggiGAAddSimpleMode...");
			fflush(stdout);
			rc = ggiGAAddSimpleMode(vis, &reqlist,
						GGI_AUTO, GGI_AUTO,
						GGI_AUTO, GT_AUTO, NULL);
			printf("%s\n",
				(rc == GALLOC_OK) ? "passed" : "failed");
			fflush(stdout);
			break;

		case 'm':	/* Add a GA_RT_FRAME CAP */
			printf("Adding a mode cap with ggiGAAddSimpleMode...");
			fflush(stdout);
			rc = ggiGAAddSimpleMode(vis, &reqlist,
						810, 620, GGI_AUTO, GT_AUTO,
						&handle);
			ggiGAAddStateFlag(handle, GA_STATE_CAP);
			printf("%s\n",
				(rc == GALLOC_OK) ? "passed" : "failed");
			fflush(stdout);
			break;

		case 'S':	/* Add a GA_RT_SPRITE */
			printf("Add sprite-pointer...");
			fflush(stdout);
			ggiGAClearTankProperties(&p);
			rc = ggiGAAdd(&reqlist, &p,
					GA_RT_SPRITE_POINTER, NULL);
			printf("%s\n",
				(rc == GALLOC_OK) ? "passed" : "failed");
			fflush(stdout);
			break;

		case 's':	/* Add a GA_RT_SPRITE CAP */
			printf("Add sprite-pointer cap...");
			fflush(stdout);
			ggiGAClearTankProperties(&p);
			rc = ggiGAAdd(&reqlist, &p,
					GA_RT_SPRITE_POINTER, &handle);
			ggiGAAddStateFlag(handle, GA_STATE_CAP);
			printf("%s\n",
				(rc == GALLOC_OK) ? "passed" : "failed");
			fflush(stdout);
			break;

		case 'T':	/* Add a GA_RT_BUFFER_TBUFFER */
			printf("Add texture-buffer...");
			fflush(stdout);
			ggiGAClearTankProperties(&p);
			rc = ggiGAAdd(&reqlist, &p, GA_RT_BUFFER_TBUFFER, NULL);
			printf("%s\n",
				(rc == GALLOC_OK) ? "passed" : "failed");
			fflush(stdout);
			break;

		case 't':	/* Add a GA_RT_BUFFER_TBUFFER */
			printf("Add texture-buffer cap...");
			fflush(stdout);
			ggiGAClearTankProperties(&p);
			rc = ggiGAAdd(&reqlist, &p,
					GA_RT_BUFFER_TBUFFER, &handle);
			ggiGAAddStateFlag(handle, GA_STATE_CAP);
			printf("%s\n",
				(rc == GALLOC_OK) ? "passed" : "failed");
			fflush(stdout);
			break;

		case '1':	/* Tag the last resource "1" */
			handle = ggiGAGetFirst(reqlist);
			while (ggiGAGetNext(handle) != NULL)
				handle = ggiGAGetNext(handle);
			ggiGASetTag(handle, 1);
			break;

		case '2':	/* Tag the last resource "2" */
			handle = ggiGAGetFirst(reqlist);
			while (ggiGAGetNext(handle) != NULL)
				handle = ggiGAGetNext(handle);
			ggiGASetTag(handle, 2);
			break;

		case '^':	/* Make the last resource a seeabove */
			handle = ggiGAGetFirst(reqlist);
			while (ggiGAGetNext(handle) != NULL)
				handle = ggiGAGetNext(handle);
			ggiGAAddStateFlag(handle, GA_STATE_SEEABOVE);
			break;

		default:
			fprintf(stderr, "huh?\n");
		}	/* switch */
		ptr++;
	}	/* while */

	printf("Checking...");
	fflush(stdout);
	rc = ggiGACheck(vis, reqlist, NULL);
	printf("%s\n", (rc == GALLOC_OK) ? "passed" : "failed");
	fflush(stdout);

	printf("Setting...");
	fflush(stdout);
	rc = ggiGASet(vis, reqlist, NULL);
	printf("%s\n", (rc == GALLOC_OK) ? "passed" : "failed");
	fflush(stdout);

	ggiGADestroyList(&reqlist);

	printf("Getting...");
	ggiGAGet(vis, &reqlist);

	handle = ggiGAGetFirst(reqlist);
	ptr = newlist;

	while (handle != NULL) {
		if ((ggiGAGetType(handle) & GA_RT_TYPE_MASK) == GA_RT_FRAME) 
			*(ptr++) = (ggiGAGetState(handle) & GA_STATE_CAP)
				 ? 'm' : 'M';
		if ((ggiGAGetType(handle) & GA_RT_TYPE_MASK) == GA_RT_SPRITE) 
			*(ptr++) = (ggiGAGetState(handle) & GA_STATE_CAP)
				 ? 's' : 'S';
		if ((ggiGAGetType(handle) & GA_RT_TYPE_MASK) == GA_RT_BUFFER) 
			*(ptr++) = (ggiGAGetState(handle) & GA_STATE_CAP)
				 ? 't' : 'T';

		if (ggiGAGetState(handle) & GA_STATE_SEEABOVE)
			*(ptr++) = '^';
		if ((ggiGAGetState(handle) & GA_STATE_TAG_MASK) == 1)
			*(ptr++) = '1';
		if ((ggiGAGetState(handle) & GA_STATE_TAG_MASK) == 2)
			*(ptr++) = '2';

		handle = ggiGAGetNext(handle);
	}	/* while */

	ggiGADestroyList(&reqlist);
	*ptr = '\0';
	return rc;
}	/* sweat2theoldies */


#if 0	/* Defined, but unused */
static int andstretch (ggi_visual_t vis, int torelease)
{
#if 0
	ggiGA_resource_list reslist;
	ggiGA_resource_handle rel;
	int rc;

	ggiGAGet(vis, &reslist);
	rel = ggiGAGetFirst(reslist);
	while (torelease--) {
		if (rel == NULL) return GALLOC_EFAILED;
		rel = ggiGAGetNext(rel);
	}	/* while */
	rc = ggiGARelease(vis, &reslist, &rel);
	ggiGADestroyList(&reslist);
#else
	return 0;
#endif
}	/* andstretch */
#endif

#if 0	/* Defined, but unused */
static int andbreath (ggi_visual_t vis, char *addlist)
{
	return 0;
}	/* andbreath */
#endif

static int do_demo(ggi_visual_t vis)
{
	int rc = 0;
	char *pr;
	ggiGA_resource_list reslist = NULL;

	pr = calloc((size_t)1024, sizeof(char));

#define TRY(s)	printf("trying \"" s "\"...");		\
		fflush(stdout);				\
		ggstrlcpy(pr, s, 1024);			\
		rc = sweat2theoldies(vis, pr);		\
		printf("got %s (rc = %i)\n", pr, rc);	\
		fflush(stdout);				\
		ggUSleep(1000);


#define RELEASE	ggiGAGet(vis, &reslist);		\
		ggiGAReleaseList(vis, &reslist, NULL);	\
		ggiGADestroyList(&reslist);

	TRY("M");
	RELEASE;
	TRY("Mm");
	RELEASE;
	TRY("MmS");
	RELEASE;
	TRY("MmSs");
	RELEASE;
	TRY("MmT^");
	RELEASE;
	TRY("MmT^T");
	RELEASE;

	free(pr);
	pr = NULL;

	for (;;) {
		ggi_event ev;

		struct timeval timeout={0,1000};

		ggiEventPoll(vis, emAll, &timeout);
		ggUSleep(timeout.tv_usec);

		while (ggiEventsQueued(vis, emAll)) {
			ggiEventRead(vis, &ev, emAll);

			switch(ev.any.type) {
			case evKeyPress:
			case evKeyRepeat:

				switch(ev.key.sym) {
				case GIIK_Enter:
					break;

				case GIIUC_Escape:
					goto cleanup;

				}	/* switch */
			}	/* switch */
		}	/* while */

		ggiFlush(vis);
	}	/* for */

 cleanup:
	return rc;
}	/* do_demo */



int main(void)
{
	ggi_visual_t vis;

	/* Initialize LibGGI */
	if (ggiInit() != 0) {
		fprintf(stderr, "Unable to initialize LibGGI\n");
		exit(1);
	}	/* if */
	
	/* Initialize Galloc extension */
	if (ggiGAInit() != 0) {
		ggiPanic("Unable to initialize LibGalloc extension\n");
	}	/* if */

	/* Open the default visual */
	if ((vis = ggiOpen(NULL)) == NULL) {
		ggiPanic("Unable to open default visual\n");
	}	/* if */

	/* Turn on asynchronous mode (which should always be used) */
	ggiSetFlags(vis, GGIFLAG_ASYNC);

	/* Attach the Galloc extension to the visual */
	if (ggiGAAttach(vis) < 0) {
		ggiPanic("Unable to attach Galloc extension to visual\n");
	}	/* if */



	/* Try the API functions */
	do_demo(vis);


	/* Detach extension from visual */
	ggiGADetach(vis);

	/* Close visual */
	ggiClose(vis);

	/* Deinitialize Galloc extension */
	ggiGAExit();

	/* Deinitialize LibGGI */
	ggiExit();

	return 0;
}	/* main */
