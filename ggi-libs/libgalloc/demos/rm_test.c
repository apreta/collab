/* $Id: rm_test.c,v 1.12 2005/10/01 09:48:45 cegger Exp $
******************************************************************************

   Test for the Range Manager.

   Authors:     2001 Eric Faurot     [eric.faurot@gmail.com]

   This code is placed in the public domain and may be used freely for any
   purpose.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include "config.h"
#include <ggi/ggi.h>
#include <ggi/mmutil.h>

ggi_visual_t vis;
ggi_mode mode;

ggi_pixel black;
ggi_pixel grey;
ggi_pixel white;

ggi_pixel red;
ggi_pixel green;
ggi_pixel blue;

#define LINEHEIGHT 12

#define MIN(a,b) (a<b?a:b)

static void draw_block(size_t x, size_t l) {
	int sx,sy,w,remaining;
	sy=(x/mode.visible.x)*LINEHEIGHT;
	sx=x%mode.visible.x;
	remaining=l;
	while(remaining) {
		w=MIN(mode.visible.x-sx,remaining);
		ggiDrawBox(vis,sx,sy+1,w,LINEHEIGHT-1);
		sy+=LINEHEIGHT;
		remaining-=w;
		sx=0;
	}
}

static void draw_range(rm_range_list_t * rm, int idx) {
	unsigned int i;
	size_t off;

	off=RM_START(rm,idx);
	ggiSetGCForeground(vis,*((ggi_pixel *)RM_OWNER(rm,idx)));
	for(i=0;i<RM_HEIGHT(rm,idx);i++) {
		draw_block(off,RM_WIDTH(rm,idx));
		off+=RM_STRIDE(rm,idx);
	}
}

#if 0	/* Defined, but unused */
static void display_chunk(rm_chunk_list_t * rl, ggi_pixel color) {
	int i;
	ggiSetGCForeground(vis,color);
	for(i=0;i<rl->count;i++) {
		draw_block(CHK_START(rl,i),CHK_WIDTH(rl,i));
	}
}
#endif

static void display_clean(void) {
	ggiSetGCForeground(vis,black);
	ggiFillscreen(vis);
}

static void display_wait(void) {
	ggiFlush(vis);
	ggiGetc(vis);
}

static void display_grid(ggi_pixel color, size_t max) {
	int x,y, i;
	int count;

	ggiSetGCForeground(vis,color);
	
	count=max;
	x = MIN(mode.visible.x, 0);
	y=0;
	while(count>0) {
		y+=LINEHEIGHT;
		x=MIN(mode.visible.x,count);
		ggiDrawHLine(vis,0,y,x);
		count-=mode.visible.x;
		for(i=0;i<x/5;i++) {
			ggiDrawVLine(vis,(i+1)*5,y-(LINEHEIGHT/(4-(2*(i%2))))+1,LINEHEIGHT/(4-(2*(i%2))));
		}
	}
	ggiDrawVLine(vis,x,y-LINEHEIGHT+1,LINEHEIGHT);
}

static void display_list(rm_range_list_t * rm) {
	int i;
	for(i=0;i<rm->count;i++) {
		draw_range(rm,i);
	}
}

static int display_init(void) {
	ggi_color c;
	ggiInit();
	vis=ggiOpen(NULL);
	ggiSetGraphMode(vis,640,480,640,480,GGI_AUTO);
	ggiSetFlags(vis,GGIFLAG_ASYNC);
	ggiSetEventMask(vis,emKey);
	ggiGetMode(vis,&mode);
	c.r=0x0; c.g=0x0; c.b=0x0;          black = ggiMapColor(vis, &c);
	c.r=0x8f8f; c.g=0x8f8f; c.b=0x8f8f; grey  = ggiMapColor(vis, &c);
	c.r=0xffff; c.g=0xffff; c.b=0xffff; white = ggiMapColor(vis, &c);
	c.r=0xffff; c.g=0x0000; c.b=0x0000; red =   ggiMapColor(vis, &c);
	c.r=0x0000; c.g=0xffff; c.b=0x0000; green = ggiMapColor(vis, &c);
	c.r=0x0000; c.g=0x0000; c.b=0xffff; blue =  ggiMapColor(vis, &c);
	return 0;
}

static int display_exit(void) {
	ggiClose(vis);
	return ggiExit();
}

int main(void) {
	size_t size=20000;
	rm_range_list_t * rm1;
	rm_range_list_t * list;
	rm_range_t r;

	display_init();

	rm_list_init(&rm1);
	rm_list_init(&list);
	rm_list_print(rm1,size);
	
	display_clean();
	display_list(rm1);
	display_grid(grey,size);
	display_wait();



	r.attribute.width=100;
	r.attribute.height=50;
	r.attribute.owner=&white;
	r.constraint.stride_align=17;
	r.attribute.flags=RANGE_STRIDE_ALIGN;
	rm_list_add(rm1,&r);

	r.attribute.width=70;
	r.attribute.height=20;
	r.attribute.owner=&red;
	r.attribute.flags=0;
	rm_list_add(rm1,&r);

	r.attribute.width=200;
	r.attribute.height=20;
	r.attribute.owner=&blue;
	r.attribute.flags=0;
	rm_list_add(rm1,&r);

	r.attribute.width=40;
	r.attribute.height=30;
	r.attribute.owner=&green;
	r.constraint.start_min=0;
	r.constraint.start_max=0;
	r.constraint.stride_align=3;
	r.attribute.flags= RANGE_START_MIN | RANGE_START_MAX | RANGE_STRIDE_ALIGN ;
	rm_list_add(rm1,&r);


	rm_list_fit(list,rm1,size);
	rm_list_alloc(list,rm1);

	rm_list_print(list,size);

	display_clean();
	display_list(list);
	display_grid(grey,size);
	display_wait();

	rm_list_exit(&rm1);
	rm_list_exit(&list);

	display_exit();

	return 0;
}
