/* $Id: galloctest.c,v 1.22 2005/10/06 12:51:22 cegger Exp $
******************************************************************************

   Demo of the LibGAlloc extension.

   Authors:	2001 Christoph Egger	[Christoph_Egger@t-online.de]

   This code is placed in the public domain and may be used freely for any
   purpose.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
   THE AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

******************************************************************************
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include <ggi/gg.h>
#include <ggi/ggi.h>
#include <ggi/galloc.h>


/* This code uses some functions NOT in the external API:
 */
extern int ggiGARemove(ggiGA_resource_list *list, ggiGA_resource_handle *handle);
extern int ggiGACopyList(ggiGA_resource_list in, ggiGA_resource_list *out);
extern int ggiGAAppendList(ggiGA_resource_list this, ggiGA_resource_list *tothis);
extern int ggiGAAppend(ggiGA_resource_handle handle, ggiGA_resource_list *list, int copy);


static int do_demo(ggi_visual_t vis)
{
	int rc = 0;
	char *pr;
	ggiGA_resource_list r1, r2;
	struct ggiGA_resource_props p;

	ggiGA_resource_handle frame_res;
	ggiGA_resource_handle res1;
	ggiGA_resource_handle res2;
	ggiGA_resource_handle res3;
	ggiGA_resource_handle res4;
	ggiGA_resource_handle res5;

	memset(&p, 0, sizeof(p));

	r1 = ggiGACreateList();
	r2 = ggiGACreateList();

	/* Do some calisthenics with the internal list operations */
	printf("ggiGAanprintf of an empty list:\n");
	fflush(stdout);
	ggiGAanprintf(vis, r1, (size_t)8092, NULL, &pr);
	printf("%s", pr);
	fflush(stdout);
	free(pr);

	/* Make a 1-element list, using the "gray area" functions. */
	res1 = ggiGACopyIntoResource(&p, NULL, (size_t)0);
	ggiGAAppend(res1, &r1, 0);
	ggiGASetType(res1, GA_RT_BUFFER_SBUFFER);
	printf("ggiGAanprintf of a one element list:\n");
	fflush(stdout);
	ggiGAanprintf(vis, r1, (size_t)8092, NULL, &pr);
	printf("%s", pr);
	fflush(stdout);
	free(pr);

	/* Add 5 elements using the regular API. */
	ggiGAAdd(&r1, &p, GA_RT_WINDOW_YUV, &res1);
	ggiGAAdd(&r1, &p, GA_RT_MOTOR | GA_RT_VIDEO_MOTION, &res2);
	ggiGAAdd(&r1, &p, GA_RT_SPRITE_SPRITE, &res3);
	ggiGAAdd(&r1, &p, GA_RT_SPRITE_POINTER, &res4);
	ggiGAAdd(&r1, &p, GA_RT_MOTOR | GA_RT_MISC_RAYPOS, &res5);
	printf("ggiGAanprintf of a six element list:\n");
	fflush(stdout);
	ggiGAanprintf(vis, r1, (size_t)8192, NULL, &pr);
	printf("%s", pr);
	fflush(stdout);
	free(pr);

	/* Delete the second element */
	ggiGARemove(&r1, &res1);
	printf("Same list with second resource ggiGARemove()d:\n");
	fflush(stdout);
	ggiGAanprintf(vis, r1, (size_t)8092, NULL, &pr);
	printf("%s", pr);
	fflush(stdout);
	free(pr);

	/* Delete the first element */
	res1 = ggiGAGetFirst(r1);
	ggiGARemove(&r1, &res1);
	printf("Same list with first resource ggiGARemove()d:\n");
	fflush(stdout);
	ggiGAanprintf(vis, r1, (size_t)8092, NULL, &pr);
	printf("%s", pr);
	fflush(stdout);
	free(pr);

	/* Delete the last element */
	ggiGARemove(&r1, &res5);
	printf("Same list with last resource ggiGARemove()d:\n");
	fflush(stdout);
	ggiGAanprintf(vis, r1, (size_t)8092, NULL, &pr);
	printf("%s", pr);
	fflush(stdout);
	free(pr);

	/* Test CopyList */
	GG_SIMPLEQ_INIT(r2);
	ggiGACopyList(r1, &r2);
	printf("A ggiGACopyList of that last list\n");
	fflush(stdout);
	ggiGAanprintf(vis, r2, (size_t)8092, NULL, &pr);
	printf("%s", pr);
	fflush(stdout);
	free(pr);

	/* Test AppendList */
	ggiGAAppendList(r2, &r1);
	printf("ggiGAAppendList of those last two\n");
	fflush(stdout);
	ggiGAanprintf(vis, r1, (size_t)8092, NULL, &pr);
	printf("%s", pr);
	fflush(stdout);
	free(pr);

	/* Test EmptyList */
	ggiGAEmptyList(&r1);
	printf("Destroying the list (1); This should print an empty list:\n");
	fflush(stdout);
	ggiGAanprintf(vis, r1, (size_t)8092, NULL, &pr);
	printf("%s", pr);
	fflush(stdout);
	free(pr);

	/* Test DestroyList */
	ggiGADestroyList(&r1);
	printf("Destroying the list (2); This should print an empty header:\n");
	fflush(stdout);
	ggiGAanprintf(vis, r1, (size_t)8092, NULL, &pr);
	printf("%s", pr);
	fflush(stdout);
	free(pr);


	GG_SIMPLEQ_INIT(r2);
	res1 = res2 = res3 = res4 = res5 = NULL;

	/* From here down external API functions only should be used. */

	/* Check Addmode */
	printf("Adding a mode to the list with ggiGAAddSimpleMode\n");
	fflush(stdout);
	frame_res = NULL;
	rc = ggiGAAddSimpleMode(vis, &r1,
				GGI_AUTO, GGI_AUTO, GGI_AUTO, GT_AUTO, 
				&frame_res);
	printf("galloctest: done - rc: %i\n", rc);
	fflush(stdout);

	printf("galloctest: call ggiGAanprintf\n");
	fflush(stdout);
	ggiGAanprintf(vis, r1, (size_t)8092, NULL, &pr);
	printf("galloctest: done - pr: \n%s\n", pr);
	fflush(stdout);
	free(pr);

	ggiGADestroyList(&r2);

	printf("galloctest: call ggiGACheck\n");
	fflush(stdout);
	rc = ggiGACheck(vis, r1, &r1);
	printf("galloctest: done - rc: %i\n", rc);
	fflush(stdout);

	printf("galloctest: call ggiGAanprintf\n");
	fflush(stdout);
	ggiGAanprintf(vis, r1, (size_t)8092, NULL, &pr);
	printf("galloctest: done - pr: \n%s\n", pr);
	fflush(stdout);
	free(pr);

	printf("galloctest: call ggiGASet\n");
	fflush(stdout);
	rc = ggiGASet(vis, r1, &r1);
	printf("galloctest: done - rc: %i\n", rc);
	fflush(stdout);

	printf("galloctest: call ggiGAanprintf\n");
	fflush(stdout);
	ggiGAanprintf(vis, r1, (size_t)8092, NULL, &pr);
	printf("galloctest: done - pr: \n%s\n", pr);
	fflush(stdout);
	free(pr);

	if (rc != GALLOC_OK) goto cleanup;

	for (;;) {

		ggi_event ev;

		struct timeval timeout={0,1000};

		ggiEventPoll(vis, emAll, &timeout);
		ggUSleep(timeout.tv_usec);

		while (ggiEventsQueued(vis, emAll)) {
			ggiEventRead(vis, &ev, emAll);

			switch(ev.any.type) {
			case evKeyPress:
			case evKeyRepeat:

				switch(ev.key.sym) {
				case GIIK_Enter:
					break;

				case GIIUC_Escape:
					goto cleanup;

				}	/* switch */
			}	/* switch */	
		}	/* while */

		ggiFlush(vis);
	}	/* for */

cleanup:

	printf("galloctest: call ggiGAReleaseList\n");
	fflush(stdout);
	rc = ggiGAReleaseList(vis, &r1, NULL);

	printf("galloctest: done - rc: %i\n", rc);
	fflush(stdout);

#if 1
	printf("galloctest: call ggiGAanprintf\n");
	fflush(stdout);
	ggiGAanprintf(vis, r1, (size_t)8092, NULL, &pr);
	printf("galloctest: done - pr: \n%s\n", pr);
	fflush(stdout);
	free(pr);
#endif

	ggiGADestroyList(&r1);
	ggiGADestroyList(&r2);

	return rc;
}


int main(void)
{
	ggi_visual_t vis;

	/* Initialize LibGGI */
	if (ggiInit() != 0) {
		fprintf(stderr, "Unable to initialize LibGGI\n");
		exit(1);
	}
	
	/* Initialize Galloc extension */
	if (ggiGAInit() != 0) {
		ggiPanic("Unable to initialize LibGalloc extension\n");
	}

	/* Open the default visual */
	if ((vis = ggiOpen(NULL)) == NULL) {
		ggiPanic("Unable to open default visual\n");
	}

	/* Turn on asynchronous mode (which should always be used) */
	ggiSetFlags(vis, GGIFLAG_ASYNC);

	/* Attach the Galloc extension to the visual */
	if (ggiGAAttach(vis) < 0) {
		ggiPanic("Unable to attach Galloc extension to visual\n");
	}



	/* Try the API functions */
	do_demo(vis);


	/* Detach extension from visual */
	ggiGADetach(vis);

	/* Close visual */
	ggiClose(vis);

	/* Deinitialize Galloc extension */
	ggiGAExit();

	/* Deinitialize LibGGI */
	ggiExit();

	return 0;
}
